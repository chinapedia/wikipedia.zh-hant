[Un_Chau_Street_Municipal_Services_Building.jpg](https://zh.wikipedia.org/wiki/File:Un_Chau_Street_Municipal_Services_Building.jpg "fig:Un_Chau_Street_Municipal_Services_Building.jpg")
[Un_Chau_Street_2009.jpg](https://zh.wikipedia.org/wiki/File:Un_Chau_Street_2009.jpg "fig:Un_Chau_Street_2009.jpg")
**元州街**（）\[1\]，是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[深水埗區的街道](../Page/深水埗區.md "wikilink")，由[深水埗](../Page/深水埗.md "wikilink")[石硤尾街近](../Page/石硤尾街.md "wikilink")[大埔道交界起](../Page/大埔道.md "wikilink")，向西北方延伸，至[長沙灣](../Page/長沙灣.md "wikilink")[青山道止](../Page/青山道.md "wikilink")，與區內的[福榮街](../Page/福榮街.md "wikilink")、[福華街](../Page/福華街.md "wikilink")、[長沙灣道等街道平行](../Page/長沙灣道.md "wikilink")。元州街原名「**元洲街**」，起源於[元洲之名](../Page/元洲.md "wikilink")。本街道為單程路，大部份路段設有三條行車線，街道盡頭是著名的[青山道](../Page/青山道.md "wikilink")[公共小巴總站](../Page/公共小巴.md "wikilink")。

由於元州街在[第二次世界大戰前已發展為](../Page/第二次世界大戰.md "wikilink")[住宅區](../Page/住宅區.md "wikilink")，沿線佔大多數是6層高的[唐樓及舊式店舖](../Page/唐樓.md "wikilink")。因此，從上世紀80年代起，部份樓宇重建成較新式的住宅。

2004年，[房協公布](../Page/房協.md "wikilink")「深水埗
K20-23」4項重建項目，包括福榮街、元州街、昌華街、興華街、青山道等多幢大廈於2009年起拆卸，大部分居民已在2006年遷出，在2007年8月只剩下六十多個零星的商戶和居民堅持留下，百年歷史的醬醋舖
「劉成和醬園」、開業50多年的「蘇記茶莊」，及多間經營汽車維修店、報紙檔、舊式士多陸續在2007年11月結業。

## 交匯道路

  - [石硤尾街](../Page/石硤尾街.md "wikilink")
  - [南昌街](../Page/南昌街.md "wikilink")
  - [北河街](../Page/北河街.md "wikilink")
  - [桂林街](../Page/桂林街.md "wikilink")
  - [欽州街](../Page/欽州街.md "wikilink")
  - [九江街](../Page/九江街.md "wikilink")
  - [營盤街](../Page/營盤街.md "wikilink")
  - [東沙島街](../Page/東沙島街.md "wikilink")
  - [東京街](../Page/東京街.md "wikilink")
  - [永隆街](../Page/永隆街.md "wikilink")
  - [發祥街](../Page/發祥街.md "wikilink")
  - [長發街](../Page/長發街.md "wikilink")
  - [興華街](../Page/興華街.md "wikilink")
  - [昌華街](../Page/昌華街.md "wikilink")
  - [青山道](../Page/青山道.md "wikilink")

## 沿線建築物

  - [元州街市政大廈](../Page/元州街市政大廈.md "wikilink")
  - [寶血醫院](../Page/寶血醫院.md "wikilink")
  - [美居中心](../Page/美居中心.md "wikilink")
  - [東京街明渠](../Page/東京街明渠.md "wikilink")
  - [元州邨](../Page/元州邨.md "wikilink")
  - [元州商場](../Page/元州商場.md "wikilink")

## 途經之公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

[HK_Shum_Shui_Po_元州街_300_Un_Chau_Street_night_view_shop_neon_signs_Oct-2013_西岸商場_West_Coast_Shopping_Centre_天然居_Restaurant.JPG](https://zh.wikipedia.org/wiki/File:HK_Shum_Shui_Po_元州街_300_Un_Chau_Street_night_view_shop_neon_signs_Oct-2013_西岸商場_West_Coast_Shopping_Centre_天然居_Restaurant.JPG "fig:HK_Shum_Shui_Po_元州街_300_Un_Chau_Street_night_view_shop_neon_signs_Oct-2013_西岸商場_West_Coast_Shopping_Centre_天然居_Restaurant.JPG")

## 參考資料

## 相關

  - [元州街公共圖書館](../Page/元州街公共圖書館.md "wikilink")
    [1](https://web.archive.org/web/20071219191317/http://www.hkpl.gov.hk/tc_chi/ext_act/ext_act_la/ext_act_la_18dla/ext_act_la_18dla_ucs.html)
  - [元州街市政大廈](../Page/元州街市政大廈.md "wikilink")
  - [元州邨](../Page/元州邨.md "wikilink")
  - [永旺百貨](../Page/永旺百貨.md "wikilink")

[Category:深水埗街道](../Category/深水埗街道.md "wikilink")
[Category:長沙灣街道](../Category/長沙灣街道.md "wikilink")

1.