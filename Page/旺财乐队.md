**旺财乐队**，[中国大陆的一支](../Page/中国大陆.md "wikilink")[摇滚乐队](../Page/摇滚.md "wikilink")，独创了“喜剧摇滚”风格。

## 乐队历史

**旺财乐队**组建于[2000年九月](../Page/2000年九月.md "wikilink")，又名[板砖与片刀](../Page/板砖与片刀.md "wikilink")

乐队成员包括[梁涛](../Page/梁涛.md "wikilink")，[崔旭东](../Page/崔旭东.md "wikilink")（同年就读于[河北省艺术学校](../Page/河北省艺术学校.md "wikilink")，现[NICO乐队](../Page/NICO乐队.md "wikilink")[吉他兼](../Page/吉他.md "wikilink")[主唱](../Page/主唱.md "wikilink")）[旬亮](../Page/旬亮.md "wikilink")（同年就读于河北省艺术学校，现不明）组成最初阵容。组件原因为近期有演出，梁涛、崔旭东两人闲来无事，于是拉上旬亮组队。曾用名旺财乐队、来福乐队、建国乐队。最终确定为旺财乐队。

乐队早期作品有：《马老墩》、《奶奶SAY》、《小二黑结婚》、《友善的狗》

近期由于诸多原因，崔旭东与梁涛已经离队，只剩下梁涛孤军奋战。

## 乐队成员

  - 梁涛：[吉他](../Page/吉他.md "wikilink")、[贝司](../Page/贝司.md "wikilink")、[主唱](../Page/主唱.md "wikilink")、[合声](../Page/合声.md "wikilink")、[词曲](../Page/词曲.md "wikilink")（代号：史蒂芬\*梁）
  - 杨海军：[吉他](../Page/吉他.md "wikilink")、[主唱](../Page/主唱.md "wikilink")、[合声](../Page/合声.md "wikilink")、[词曲](../Page/词曲.md "wikilink")（代号：托马斯·杆中杆）
  - 毕志峰：[吉他](../Page/吉他.md "wikilink")、[键盘](../Page/键盘.md "wikilink")、鼓、[合声](../Page/合声.md "wikilink")（代号：小B）
  - 苏雷：[吉他](../Page/吉他.md "wikilink")、[RY9鼓机](../Page/RY9鼓机.md "wikilink")、[键盘](../Page/键盘.md "wikilink")（代号：苏苏）
  - 曹迪：[吉他](../Page/吉他.md "wikilink")、[合声](../Page/合声.md "wikilink")、词、现场演员（代号：包子曹）
  - 崔旭东：[吉他](../Page/吉他.md "wikilink")、[合声](../Page/合声.md "wikilink")、[词曲](../Page/词曲.md "wikilink")、临时演员、全活等（代号：皮搋子）
  - 赵亚楠：[吉他](../Page/吉他.md "wikilink")、[合声](../Page/合声.md "wikilink")、曲、现场演员（代号：扁子楠）
  - XX：[吉他](../Page/吉他.md "wikilink")、[主唱](../Page/主唱.md "wikilink")、[词曲](../Page/词曲.md "wikilink")、[合声](../Page/合声.md "wikilink")（隐藏人物，暂时保密）
  - 临时客串、演员等等。

## 专辑

### 《运用你的想象力Vol.3》

2003年4月1日推出首张专辑**《运用你的想象力Vol.3》**〔《运用你的想象力·第三辑》 〕。

专辑名称：运用你的想象力Vol.3〔《运用你的想象力·第三辑》 〕

艺术家：旺财乐队

厂牌：[SOROCK](../Page/SOROCK.md "wikilink")（我爱摇滚乐）

CD编号：CD-12

卡带编号：CS-12

风格：[喜剧摇滚](../Page/喜剧摇滚.md "wikilink")（应该是没有风格的风格）

标题：一支简称W.C.的乐队的惊险搞笑之旅

规格：增强型CD，内附“马老敦”Flash MV

曲目列表：

01.奶奶Say

02.马老敦

03.工程师

04.乱码

05.BoomtreeMan

06.杆儿中杆儿

07.友善的狗

08.随想

09.和弦大叔

10.上下五千年

### 《痛苦的》

2006年推出第二张专辑**《痛苦的》**

曲目列表：

01.菩提树下（第一版）

02.小二黑结婚（硬版）

03.来福（绝版）

04.友善的狗（ReMix版）

05.一壶白酒（二胡版）

06.杆中杆（星战版）

07.马老蹲（FUNK版）

08.硬奶（硬摇滚版奶奶SAY）

09.和弦大叔（98年屎版）

10.随想（98年屎版）

11.如果我变成一道阳光从城市上空下降那么一定会把你刺伤（纯净版）

12.小狄到了以后（请慎重欣赏）

13.上边儿人听的东西儿（请慎重欣赏）

14.这你妈是给人听的东西哦？（请慎重欣赏）

## 外部链接

[《运用你的想象力Vol.3》乐评](http://www.douban.com/subject/1784507/)

[category:中国摇滚乐队](../Page/category:中国摇滚乐队.md "wikilink")