[LeungKingStation_PassengerCenter.jpg](https://zh.wikipedia.org/wiki/File:LeungKingStation_PassengerCenter.jpg "fig:LeungKingStation_PassengerCenter.jpg")
[LeungKingStation_OldPassengerCenter.jpg](https://zh.wikipedia.org/wiki/File:LeungKingStation_OldPassengerCenter.jpg "fig:LeungKingStation_OldPassengerCenter.jpg")

**良景站**（英文：**Leung King
Stop**）是[港鐵](../Page/港鐵.md "wikilink")[輕鐵車站](../Page/香港輕鐵.md "wikilink")。代號150，屬單程車票[第3收費區](../Page/輕鐵第3收費區.md "wikilink")，共有2個月台。車站位於田景路近[良景邨](../Page/良景邨.md "wikilink")，在良景邨和[田景邨之間](../Page/田景邨.md "wikilink")，良景商場地下。

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p>-</p></td>
<td><p>商場</p></td>
<td><p>良景商場</p></td>
</tr>
<tr class="even">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p>良景商場、<a href="../Page/良景邨.md" title="wikilink">良景邨</a></p></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 往<a href="../Page/田景站.md" title="wikilink">田景</a>（總站） {{!}}  往<a href="../Page/元朗站_(輕鐵).md" title="wikilink">元朗</a>（<a href="../Page/田景站.md" title="wikilink">田景</a>）|}}</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> 往<a href="../Page/三聖站.md" title="wikilink">三聖</a> {{!}}  往<a href="../Page/屯門碼頭站.md" title="wikilink">屯門碼頭</a>（<a href="../Page/新圍站.md" title="wikilink">新圍</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>輕鐵客務中心、良景商場</p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站月台

良景站位於田景路近[良景邨](../Page/良景邨.md "wikilink")，在[良景邨和](../Page/良景邨.md "wikilink")[田景邨之間](../Page/田景邨.md "wikilink")，良景商場地下，路軌與行車道路完全分隔，只與一條行人道相交。

良景站2號月台方設有[輕鐵客務中心](../Page/輕鐵客務中心.md "wikilink")。

## 附近地點

  - 良景商場
  - 良田體育館
  - [田景邨](../Page/田景邨.md "wikilink")
  - [良景邨](../Page/良景邨.md "wikilink")

## 利用狀況

由於本站鄰近多個公共屋邨和屋苑，也有數間中小學設在良景邨，因此使用量處於中上水平。

逢星期一至五（公眾假期除外）上午7時30分至8時30分，到達本站的列車均須停在指定位置上落客。

  - 於1號月台，[505線](../Page/香港輕鐵505綫.md "wikilink")、[615線及](../Page/香港輕鐵615綫.md "wikilink")[615P線會於前半部分停泊](../Page/香港輕鐵615P綫.md "wikilink")；[507線則會於後半部分停泊](../Page/香港輕鐵507綫.md "wikilink")。
  - 於2號月台，[507線](../Page/香港輕鐵507綫.md "wikilink")、[615線及](../Page/香港輕鐵615綫.md "wikilink")[615P線會於前半部分停泊](../Page/香港輕鐵615P綫.md "wikilink")；[505線則會於後半部分停泊](../Page/香港輕鐵505綫.md "wikilink")。

## 鄰近車站

## 參見

  - [香港輕鐵](../Page/香港輕鐵.md "wikilink")

## 外部連結

  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵街道圖](http://www.mtr.com.hk/archive/ch/services/maps/07.gif)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[Category:屯門](../Category/屯門.md "wikilink")
[Category:以屋苑命名的香港輕鐵車站](../Category/以屋苑命名的香港輕鐵車站.md "wikilink")
[Category:1988年启用的铁路车站](../Category/1988年启用的铁路车站.md "wikilink")
[Category:屯門區鐵路車站](../Category/屯門區鐵路車站.md "wikilink")