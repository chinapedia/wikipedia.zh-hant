[Piatnitzkysaurus_skull_1.jpg](https://zh.wikipedia.org/wiki/File:Piatnitzkysaurus_skull_1.jpg "fig:Piatnitzkysaurus_skull_1.jpg")
[Piatnitzkysaurus.jpg](https://zh.wikipedia.org/wiki/File:Piatnitzkysaurus.jpg "fig:Piatnitzkysaurus.jpg")
**皮亞尼茲基龍屬**（[屬名](../Page/屬.md "wikilink")：）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[侏羅紀中期的](../Page/侏羅紀.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。

皮亞尼茲基龍的[化石是兩個破碎的頭顱骨](../Page/化石.md "wikilink")，以及部份的顱後[骨骼](../Page/骨骼.md "wikilink")。化石發現於[阿根廷的Cañadon](../Page/阿根廷.md "wikilink")
Asfalto地層，地質年代屬於[侏羅紀中期的](../Page/侏羅紀.md "wikilink")[卡洛維階](../Page/卡洛維階.md "wikilink")。根據不同的研究，皮亞尼茲基龍被認為屬於基礎[肉食龍下目](../Page/肉食龍下目.md "wikilink")\[1\]，或是[斑龍超科動物](../Page/斑龍超科.md "wikilink")\[2\]。皮亞尼茲基龍是種中型、二足[肉食性恐龍](../Page/肉食性.md "wikilink")，有粗壯的前肢，身長約4.3公尺。體重估計約275公斤\[3\]或450公斤，後者是根據[正模標本是亞成年體而推論的結果](../Page/正模標本.md "wikilink")\[4\]。

[腸骨長](../Page/腸骨.md "wikilink")42.3公分\[5\]。腦殼類似其他斑龍超科、[斑龍科的](../Page/斑龍科.md "wikilink")[皮爾逖龍](../Page/皮爾逖龍.md "wikilink")（發現於[法國](../Page/法國.md "wikilink")）\[6\]。

[模式種是](../Page/模式種.md "wikilink")**弗氏皮亞尼茲基龍**（*P.
floresi*），是由[約瑟·波拿巴](../Page/約瑟·波拿巴.md "wikilink")（Jose
Bonaparte）於1979年描述、命名的，屬名是紀念阿根廷地質學家Alejandro Mateievich Piatnitzky。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - <https://web.archive.org/web/20090627142913/http://www.thescelosaurus.com/tetanurae.htm>
  - <https://web.archive.org/web/20080723135333/http://kids.yahoo.com/dinosaurs/68--Piatnitzkysaurus>

[Category:斑龍超科](../Category/斑龍超科.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.  Bonaparte, 1979. Dinosaurs: A Jurassic assemblage from Patagonia.
    Science. 205, 1377-1379.

2.
3.

4.  Mazzetta, G. V., Fariiia, R. A., & Vizcaino, S. F. 2000. On the
    palaeobiology of the South American homed theropod Carnotaurus
    sastrei Bonaparte. In: B. Perez-Moreno, T.R. Holtz Jr., J.L. Sanz, &
    J.J. Moratalla (eds.), Aspects of Theropod Paleobiology, Special
    Volume - Gaia 15, 185-192. Lisbon.

5.  Brusatte, S. L., Benson, R. B. J., and Xu, X. 2010. The evolution of
    large-bodied theropod dinosaurs during the Mesozoic in Asia. Journal
    of Iberian Geology, 36, 275-296

6.  Rauhut, 2004. Braincase structure of the Middle Jurassic theropod
    dinosaur *Piatnitzkysaurus*. Canadian Journal of Earth Science. 41,
    1109-1122.