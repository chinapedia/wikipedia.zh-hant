[MacauBOCBuilding_SAR10thAnniversary.jpg](https://zh.wikipedia.org/wiki/File:MacauBOCBuilding_SAR10thAnniversary.jpg "fig:MacauBOCBuilding_SAR10thAnniversary.jpg")
**中國銀行澳門分行大廈**（）是[中國銀行澳門分行的總辦事處](../Page/中國銀行澳門分行.md "wikilink")，位於[蘇亞利斯博士大馬路](../Page/蘇亞利斯博士大馬路.md "wikilink")，於1991年底落成使用，樓高38層，在[澳門旅遊塔落成前曾經是澳門最高的建築物](../Page/澳門旅遊塔.md "wikilink")。

## 歷史

大廈位置原為[利宵中學舊址的一部份](../Page/利宵中學.md "wikilink")，1986年利宵中學遷到新口岸後，在原址東南端的一幅地段用作興建這座大廈\[1\]。1991年落成後[中國銀行澳門分行的總辦事處從](../Page/中國銀行澳門分行.md "wikilink")[亞美打利庇盧大馬路](../Page/亞美打利庇盧大馬路.md "wikilink")[南通商業大廈遷入本大廈](../Page/南通商業大廈.md "wikilink")。

## 大廈結構

整幢大廈樓高160多[公尺](../Page/公尺.md "wikilink")，另地庫3層，建築面積總計達50萬平方[呎](../Page/呎.md "wikilink")。是澳門目前最高及設備最現代化的甲級商廈。大廈由[香港](../Page/香港.md "wikilink")[巴馬丹拿瑞士裔建築師李華武設計](../Page/巴馬丹拿.md "wikilink")（造型與同一人設計的[中環](../Page/中環.md "wikilink")[渣打銀行大廈相同](../Page/渣打銀行大廈.md "wikilink")），大廈位於澳門[南灣區](../Page/南灣_\(澳門\).md "wikilink")[金融及](../Page/金融.md "wikilink")[商業中心地帶](../Page/商業.md "wikilink")。大廈底部為裙樓4層，以上為[塔樓](../Page/塔樓.md "wikilink")，縱觀如[火箭](../Page/火箭.md "wikilink")，橫觀呈[扇形](../Page/扇形.md "wikilink")。外牆砌粉紅色[花崗石](../Page/花崗石.md "wikilink")，並配以銀色反光玻璃幕牆。設有可容納300多人之多功能宴會廳；200多人之演講廳及多功能會議室，並附設員工[俱樂部等](../Page/俱樂部.md "wikilink")。

## 用途

大廈目前是中國銀行澳門分行總辦事處。而中國銀行澳門分行，前身是[澳門南通銀行](../Page/澳門南通銀行.md "wikilink")，成立於1950年，最初只提供[僑匯](../Page/僑匯.md "wikilink")、[結匯和簡單](../Page/結匯.md "wikilink")[貸款業務](../Page/貸款.md "wikilink")，1970年代轉為商業銀行，1987年易名為中國銀行澳門分行，成為[中國銀行第九家海外分行](../Page/中國銀行.md "wikilink")。1995年由當時的澳門貨幣暨匯兌監理處（即今[澳門金融管理局](../Page/澳門金融管理局.md "wikilink")）授權成為發鈔銀行，2000年成為[澳門政府公庫其中一家代理銀行](../Page/澳門政府公庫.md "wikilink")。\[2\]

另外，[葡萄牙商業銀行](../Page/葡萄牙商業銀行.md "wikilink")、[畢馬威會計師事務所](../Page/畢馬威.md "wikilink")（24樓）澳門總辦事處也設於該大廈內。\[3\]

## 參考文獻

<div class="references-small">

<references />

  - 中銀澳門分行，[大廈簡介](https://web.archive.org/web/20010222060056/http://www.bocmacau.com/b5/aboutus/2d1_index.htm)，2009年4月6日
    (一) 17:44 (UTC)查閱，

</div>

[Category:澳門商廈](../Category/澳門商廈.md "wikilink")
[Category:中國銀行](../Category/中國銀行.md "wikilink")

1.
2.
3.