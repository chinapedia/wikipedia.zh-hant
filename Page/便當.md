**便当**（）即是**盒装餐食**，在中國大陸大部分地方被称为**盒-{}-饭**，在香港和廣東等粵語區被稱为**飯-{}-盒**，在台湾被称为**便-{}-当**或**飯-{}-包**（台灣話：Pn̄g-pau），主要使用[盒子盛裝](../Page/盒子.md "wikilink")[飯](../Page/糧食.md "wikilink")[菜或面条等各種](../Page/菜.md "wikilink")[食物以便於攜帶](../Page/食物.md "wikilink")。主要通行於[亞洲以](../Page/亞洲.md "wikilink")[稻米作為](../Page/稻米.md "wikilink")[主食的地區](../Page/主食.md "wikilink")。

## 概說

[Lunches_of_Seven-Eleven.jpg](https://zh.wikipedia.org/wiki/File:Lunches_of_Seven-Eleven.jpg "fig:Lunches_of_Seven-Eleven.jpg")便利店的微波爐便當\]\]
“**便-{}-当**”一词最早源于[南宋时期的俗语](../Page/南宋.md "wikilink")，意思是「便利的东西、方便、顺利」。传入日本後，曾以「」、「」、「」等[借字](../Page/借字.md "wikilink")（）表记\[1\]。「便-{}-当」一詞後來反传入中国是源于[日语的](../Page/日语.md "wikilink")「」（新字体：，[旧字体](../Page/旧字体.md "wikilink")：）。

便當通常用于[午餐與](../Page/午餐.md "wikilink")[晚餐](../Page/晚餐.md "wikilink")，非家裡開伙。又不在[餐廳用膳](../Page/餐廳.md "wikilink")，即是[外送](../Page/外送.md "wikilink")、[野餐等](../Page/野餐.md "wikilink")，為了節省時間以及省卻煮飯、洗餐具等工作。可到店裡買現成的便當，或打電話找[外賣店](../Page/外賣店.md "wikilink")，或者到[自助餐店裡](../Page/自助餐.md "wikilink")，選取自己喜歡的配菜。便當也是[便利商店販售的即食商品之一](../Page/便利商店.md "wikilink")。

在台灣，各級[學校的](../Page/學校.md "wikilink")[營養午餐普及之前](../Page/營養午餐.md "wikilink")，不少學生把家裡製作好的便當帶到學校去。通常是在早上放進蒸飯機，中午再去取回蒸好的便當食用。

在日本，無論是家庭自製或外賣的便當大多以冷便當為主，少數外賣的便當附有加熱器利用化學放熱以加熱便當。

## 製作方式

製作便當時，必須注意很重要的一點：避免[食物中毒](../Page/食物中毒.md "wikilink")。特別是[夏季尤其需要注意](../Page/夏季.md "wikilink")。有幾個重要的預防措施。首先，食物必須充分加熱。並且，把熱的飯菜裝進便當盒裡之前必須先冷卻。另外，最好把便當放在涼爽而溼氣不重的地方。

## 便當盒物料

  - 紙（紙盒）
  - [聚苯乙烯](../Page/聚苯乙烯.md "wikilink")（PS）
  - [聚丙烯](../Page/聚丙烯.md "wikilink")（PP）
  - [聚乳酸](../Page/聚乳酸.md "wikilink")（PLA）
  - [軟木](../Page/軟木.md "wikilink")
  - [合金](../Page/合金.md "wikilink")
  - [不锈鋼](../Page/不锈鋼.md "wikilink")

## 各式便当

<File:Bento_at_Hanabishi,_Koyasan.jpg>|[炸蝦便當](../Page/炸蝦.md "wikilink")
<File:Bento> de luxe.jpg|日式[鮭魚便當](../Page/鮭魚.md "wikilink") <File:Japan>
Pic 1 Bento.jpg|日本[鐵路便當](../Page/鐵路便當.md "wikilink") <File:Orizume>
bentō SETSUGEKKA served by Ningyocho Imahan Co,. Ltd.
01.jpg|日本折詰便當(三層紙盒) <File:Orizume> bentō SETSUGEKKA
served by Ningyocho Imahan Co,. Ltd. 02.jpg|日本折詰便當 <File:Ekiben> in
Mainland China.jpg|中國火車[盒飯](../Page/盒飯.md "wikilink")
[File:Bento0001.jpg|肉丸便當](File:Bento0001.jpg%7C肉丸便當) <File:Hanami> bento
by Blue Lotus.jpg|[花見便當](../Page/花見.md "wikilink")
<File:Sushi_bento.jpg>|[壽司便當](../Page/壽司.md "wikilink") <File:Tōge> no
Kamameshi 02.jpg|陶碗便當 <File:Checheng> wood bucket bento
(Taiwan).jpg|以軟木製造的木桶便當 <File:Schoolboys> taking lunch in
Maruyama Park (11809979553).jpg|家庭便當（[學校旅行](../Page/學校旅行.md "wikilink")）
<File:台鐵排骨便當> 20070814.jpg|thumb|[台鐵排骨便當](../Page/台鐵.md "wikilink")
<File:Fulong> Station Ekiben 02.jpg|缩略图|福隆便當，攝影於2007年 <File:31274819654>
65e18d3408 yuann.tw.jpg|thumb|台東池上便當
[File:雞腿飯包.jpg|250px|right|thumb](File:雞腿飯包.jpg%7C250px%7Cright%7Cthumb)|[臺灣傳統雞腿](../Page/臺灣.md "wikilink")[便當](../Page/便當.md "wikilink")
<File:4119200387> adkskladlj.jpg|thumb|爌肉飯便當 <File:1210077955>
sdfk;sfkd;k;k.jpg|thumb|炸蝦捲便當

## 參見

  - [鐵路便當](../Page/鐵路便當.md "wikilink")

  - [微波爐便當](../Page/微波爐便當.md "wikilink")

  - [領便當](../Page/領便當.md "wikilink")

  -
## 参考文献

## 外部連結

{{-}}

[Category:米饭](../Category/米饭.md "wikilink")
[Category:餐](../Category/餐.md "wikilink")
[Category:生活用品](../Category/生活用品.md "wikilink")
[BD](../Category/日語借詞.md "wikilink")

1.  [弁当（べんとう）- 語源由来辞典](http://gogen-allguide.com/he/bentou.html)