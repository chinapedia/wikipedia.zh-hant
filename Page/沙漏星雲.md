{{ Planetary nebula | image =
[MyCn18-crop.png](https://zh.wikipedia.org/wiki/File:MyCn18-crop.png "fig:MyCn18-crop.png")
沙漏星雲 | name =沙漏星雲 | type = ??? | epoch = J2000.0 | ra = 13<sup>h</sup>
39<sup>m</sup> 30.0<sup>s</sup> | dec = -67° 22′ 06″ | dist_ly = 8,000
[ly](../Page/光年.md "wikilink") | appmag_v = ??? | size_v = ???
[角分](../Page/角分.md "wikilink") | constellation =
[蒼蠅座](../Page/蒼蠅座.md "wikilink") | radius_ly = - |
absmag_v = - | notes = - | names = MyCn18 Mayall/Cannon}}
**沙漏星雲**（也稱為**MyCn18**）是一個年輕的[行星狀星雲](../Page/行星狀星雲.md "wikilink")，位於南天的[蒼蠅座](../Page/蒼蠅座.md "wikilink")，距離[地球](../Page/地球.md "wikilink")8,000光年遠，是[安妮J.坎農和](../Page/安妮J.坎農.md "wikilink")[瑪格麗特W.梅歐在擴編](../Page/瑪格麗特W.梅歐.md "wikilink")[亨利德雷珀目錄時發現的](../Page/亨利德雷珀目錄.md "wikilink")。當時只簡單的標示是一個行星狀星雲，直到望遠鏡和影像處理的技術獲得改進之後，[美国宇航局](../Page/美国宇航局.md "wikilink")-{zh-hans:[喷气推进实验室](../Page/喷气推进实验室.md "wikilink");
zh-hant:[噴射推進實驗室](../Page/噴射推進實驗室.md "wikilink");}-的[Raghvendra
Sahai和](../Page/Raghvendra_Sahai.md "wikilink")[John
Trauger才在](../Page/John_Trauger.md "wikilink")1995年7月30日發現它的形狀像一個沙漏。猜想MyCn18的會成為沙漏形狀的原因是在低速膨脹的雲氣之內有高速膨脹的-{zh-hans:[星风](../Page/星风.md "wikilink");
zh-hant:[恆星風](../Page/恆星風.md "wikilink");}-，而雲氣在赤道的密度又比兩極高。

沙漏星雲的照片是由-{zh-hans:[哈勃太空望远镜](../Page/哈勃太空望远镜.md "wikilink");
zh-hant:[哈伯太空望遠鏡](../Page/哈伯太空望遠鏡.md "wikilink");}-上的[第二代廣域和行星照相機拍攝的](../Page/第二代廣域和行星照相機.md "wikilink")。

要注意，在明亮的[礁湖星雲中也有一個像沙漏的星雲](../Page/礁湖星雲.md "wikilink")，不要將兩者混淆了。[1](https://web.archive.org/web/19961225160716/http://seds.lpl.arizona.edu/messier/more/m008_det.html)[2](https://web.archive.org/web/20060927055307/http://www.seds.org/messier/m-names.html)。

## 大眾文化中的沙漏星雲

  - 沙漏星雲曾經被[珍珠果醬樂團在](../Page/珍珠果醬樂團.md "wikilink")2000年出版的專輯（[Binaural](../Page/Binaural（album）.md "wikilink")）中做為封面。
  - 沙漏星雲曾在劇情片[哈洛上尉：無盡的冒險之旅突顯其特色](../Page/哈洛上尉：無盡的冒險之旅.md "wikilink")。

## 參考

<div class="references-small">

1.  STScI. [Hubble Finds an Hourglass Nebula around a Dying
    Star](http://hubblesite.org/newscenter/newsdesk/archive/releases/1996/07/).
    Press release: *Space Telescope Science Institute*. January 16,
    1996.

</div>

[Category:行星狀星雲](../Category/行星狀星雲.md "wikilink")
[Category:苍蝇座](../Category/苍蝇座.md "wikilink")