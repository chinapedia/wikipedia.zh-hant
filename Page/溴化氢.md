**溴化氢**是化学式为[H](../Page/氢.md "wikilink")[Br的](../Page/溴.md "wikilink")[二元化合物](../Page/二元化合物.md "wikilink")，标准情况下为[气体](../Page/气体.md "wikilink")。溴化氢溶于水得到[氢溴酸](../Page/氢溴酸.md "wikilink")，氢溴酸中加入[失水剂也可得到溴化氢](../Page/失水剂.md "wikilink")。

## 性质

标准情况下，溴化氫是不可燃气体，带有酸味，在[潮湿](../Page/潮湿.md "wikilink")[空气中发烟](../Page/空气.md "wikilink")。溴化氫可溶于水生成[氢溴酸](../Page/氢溴酸.md "wikilink")，室温下[饱和溶液的浓度为](../Page/飽和_\(化學\).md "wikilink")68.85%（质量比），[恒沸混合物中含](../Page/恒沸混合物.md "wikilink")47.38%质量的溴化氫，恒沸混合物沸点126°C。氢溴酸几乎完全离解为H<sup>+</sup>和Br<sup>−</sup>。

## 用途

HBr在化学合成中有很多应用。例如，可由它与[醇反应来制取](../Page/醇.md "wikilink")[溴代烃](../Page/溴代烃.md "wikilink")：

  -

      -
        ROH + HBr → R<sup>+</sup>OH<sub>2</sub> + Br<sup>–</sup> → RBr +
        H<sub>2</sub>O

HBr与[烯烃加成得到溴代烃](../Page/烯烃.md "wikilink")：

  -

      -
        RCH=CH<sub>2</sub> + HBr → RCH(Br)–CH<sub>3</sub>

HBr与[炔烃加成得到溴代烯烃](../Page/炔烃.md "wikilink")，产物为反式：

  -
    RC≡CH + HBr → RCH(Br)=CH<sub>2</sub>

继续反应得到[偕二溴代烷](../Page/偕二卤.md "wikilink")，遵循[马氏规则](../Page/马氏规则.md "wikilink")：

  -

      -
        RC(Br)=CH<sub>2</sub> + HBr → RC(Br<sub>2</sub>)–CH<sub>3</sub>

此外，HBr也可用于[环氧化合物和](../Page/环氧化合物.md "wikilink")[内酯的开环合成溴缩醛](../Page/内酯.md "wikilink")，以及很多有机反应的催化剂。\[1\]\[2\]\[3\]\[4\]

## 工业制取

与[氯化氢和](../Page/氯化氢.md "wikilink")[盐酸不同的是](../Page/盐酸.md "wikilink")，工业上，溴化氢与氢溴酸并非大量需求的化学品，并且是通过200-400°C和[铂或](../Page/铂.md "wikilink")[石棉催化下](../Page/石棉.md "wikilink")，[氢气与](../Page/氢气.md "wikilink")[溴反应制取的](../Page/溴.md "wikilink")。\[5\]\[6\]

## 实验室制备

实验室中可通过很多方法制备HBr。一种是用[硫酸和](../Page/硫酸.md "wikilink")[NaBr反应](../Page/溴化钠.md "wikilink")：\[7\]

  -

      -
        NaBr<sub>(s)</sub> + H<sub>2</sub>SO<sub>4(aq)</sub> →
        NaHSO<sub>4(s)</sub> + HBr<sub>(g)</sub>

但该法产率不高，生成的HBr会被硫酸氧化为溴：

  -

      -
        2HBr<sub>(g)</sub> + H<sub>2</sub>SO<sub>4(aq)</sub> →
        Br<sub>2(g)</sub> + SO<sub>2(g)</sub> +
        2H<sub>2</sub>O<sub>(l)</sub>

因此常选用非氧化性酸，如[磷酸或](../Page/磷酸.md "wikilink")[乙酸作为反应物](../Page/乙酸.md "wikilink")。

或者可以通过[四氢化萘的溴化来制备](../Page/四氢化萘.md "wikilink")：\[8\]

  -

      -
        C<sub>10</sub>H<sub>12</sub> + 4Br<sub>2</sub> →
        C<sub>10</sub>H<sub>8</sub>Br<sub>4</sub> + 4HBr<sub>(g)</sub>

亦用[苯酚与浓](../Page/苯酚.md "wikilink")[溴水反应制取](../Page/溴.md "wikilink"):

C<sub>6</sub>H<sub>5</sub>OH+3Br<sub>2</sub>→C<sub>6</sub>H<sub>2</sub>Br<sub>3</sub>OH(三溴苯酚)+3HBr

干燥的HBr气体可以方便的由有机羧酸同LiBr在高温条件(\>150
<sup>o</sup>C)下反应得到\[9\]，然后通过氮气流将HBr带出。

但需要注意的是，LiBr极易吸水，需要使用无水LiBr。如

PhCOOH + LiBr → PhCOOLi + HBr↑

纯净氢气与溴在铂催化下反应：\[10\]

  -

      -
        Br<sub>2</sub> + H<sub>2</sub> → 2HBr<sub>(g)</sub>

用[亚磷酸还原溴](../Page/亚磷酸.md "wikilink")：\[11\]

  -

      -
        Br<sub>2</sub> + H<sub>3</sub>PO<sub>3</sub> + H<sub>2</sub>O →
        H<sub>3</sub>PO<sub>4(s)</sub> + 2HBr<sub>(g)</sub>

少量无水溴化氢还可由[溴化三苯基鏻在](../Page/溴化三苯基鏻.md "wikilink")[二甲苯中回流分解制得](../Page/二甲苯.md "wikilink")。\[12\]

以上方法得到的HBr常含有Br<sub>2</sub>杂质，可通过使其与Cu或[苯酚反应除去](../Page/苯酚.md "wikilink")。\[13\]

## 参考资料

[Category:氢化合物](../Category/氢化合物.md "wikilink")
[Category:溴化物](../Category/溴化物.md "wikilink")
[Category:非金属卤化物](../Category/非金属卤化物.md "wikilink")

1.  Hercouet, A.;LeCorre, M. (1988) Triphenylphosphonium bromide: A
    convenient and quantitative source of gaseous hydrogen bromide.
    Synthesis, 157-158.

2.  Greenwood, N. N.; Earnshaw, A. Chemistry of the Elements;
    Butterworth-Heineman: Oxford, Great Britain; 1997; pp. 809-812.

3.  Carlin, William W. [U.S.
    Patent 4,147,601](http://patft.uspto.gov/netacgi/nph-Parser?patentnumber=4147601),
    April 3, 1979

4.  Vollhardt, K. P. C.; Schore, N. E. Organic Chemistry: Structure and
    Function; 4th Ed.; W. H. Freeman and Company: New York, NY; 2003.

5.
6.
7.  [WebElements: Hydrogen
    Bromide](http://www.webelements.com/webelements/compounds/text/H/Br1H1-10035106.html)

8.
9.  Suzuki, S, Francisco, S. California Research Corporation, US
    3199953, 1965.

10.
11.
12.
13. Ruhoff, J. R.; Burnett, R. E.; Reid, E. E. "Hydrogen Bromide
    (Anhydrous)" Organic Syntheses, Vol. 15, p.35 (Coll. Vol. 2, p.338).