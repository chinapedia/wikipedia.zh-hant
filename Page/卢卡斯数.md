**卢卡斯数**是一个以数学家[爱德华·卢卡斯命名的](../Page/爱德华·卢卡斯.md "wikilink")[整数序列](../Page/整数序列.md "wikilink")，他既研究了这个数列，也研究了有密切关系的[斐波那契数](../Page/斐波那契数.md "wikilink")。与[斐波那契数一样](../Page/斐波那契数.md "wikilink")，每一个卢卡斯数都定义为前两项之和，也就是说，它是一个[斐波那契整数序列](../Page/斐波那契整数序列.md "wikilink")。两个相邻的卢卡斯数之比收敛于[黄金分割比](../Page/黄金分割比.md "wikilink")。

但是，最初两个卢卡斯数是*L*<sub>0</sub> = 2和*L*<sub>1</sub> =
1，而不是0和1。所以，卢卡斯数的性质与斐波那契数的性质有些不同。

卢卡斯数可以定义如下：

\[L_n = L(n)=
  \begin{cases}
    2             & \mbox{if } n = 0; \\
    1             & \mbox{if } n = 1; \\
    L(n-1)+L(n-2) & \mbox{if } n > 1. \\
   \end{cases}\]

前几个卢卡斯数是：

  -
    [2](../Page/2.md "wikilink"), [1](../Page/1.md "wikilink"),
    [3](../Page/3.md "wikilink"), [4](../Page/4.md "wikilink"),
    [7](../Page/7.md "wikilink"), [11](../Page/11.md "wikilink"),
    [18](../Page/18.md "wikilink"), [29](../Page/29.md "wikilink"),
    [47](../Page/47.md "wikilink"), [76](../Page/76.md "wikilink"),
    [123](../Page/123.md "wikilink"), ...

## 延伸到负数

用L<sub>n-2</sub> = L<sub>n</sub> -
L<sub>n-1</sub>的公式，我们可以把卢卡斯数延伸到负数。这样我们得到以下数列：

(... -11, 7, -4, 3, -1, 2, 1, 3, 4, 7, 11, ...)

一般地，我们有

  - \(L_{-n}=(-1)^nL_n.\!\)

## 与斐波那契数的关系

卢卡斯数与斐波那契数有以下关系：

  - \(\,L_n = F_{n-1}+F_{n+1}\)
  - \(\,L_n^2 = 5 F_n^2 + 4 (-1)^n\)，因此，当\(n\,\)趋近于[无穷大时](../Page/无穷大.md "wikilink")，\(L_n \over F_n\,\)趋近于\(\sqrt{5}\,\)。
  - \(\,F_{2n} = L_n F_n\)
  - \(\,F_n = {L_{n-1}+L_{n+1} \over 5}\)

[通项公式为](../Page/通项公式.md "wikilink")：

\[L_n = \varphi^n + (1-\varphi)^{n} = \varphi^n + (- \varphi)^{- n}=\left({ 1+ \sqrt{5} \over 2}\right)^n + \left({ 1- \sqrt{5} \over 2}\right)^n\, ,\]

其中\(\varphi\)是[黄金分割比](../Page/黄金分割比.md "wikilink")。

## 同余关系

如果n是素数，则L<sub>n</sub>被n除余1，但某些合数也具有这个性质。

## 卢卡斯素数

卢卡斯素数就是既是卢卡斯数又是素数的整数。最小的几个卢卡斯素数为：

2, 3, 7, 11, 29, 47, 199, 521, 2207, 3571, 9349, ...

除了*n* =
0、4、8、16的情况外，如果*L<sub>n</sub>*是素数，则*n*是素数。但是，它的[逆命题不成立](../Page/逆命题.md "wikilink")。

## 參考

  - [卢卡斯数列](../Page/卢卡斯数列.md "wikilink")

## 参考文献

  - Hoggatt, V. E. Jr. *The Fibonacci and Lucas numbers*. Boston, MA:
    Houghton Mifflin, 1969.
  - Hrant Arakelian. *Mathematics and History of the Golden Section*,
    Logos 2014, 404 p. ISBN 978-5-98704-663-0 (rus.).

## 外部链接

  - [MathWorld](http://mathworld.wolfram.com/LucasNumber.html)
  - [Dr Ron
    Knott](https://web.archive.org/web/20051126021243/http://www.mcs.surrey.ac.uk/Personal/R.Knott/Fibonacci/lucasNbs.html)
  - [卢卡斯数与黄金分割](https://web.archive.org/web/20051030021553/http://milan.milanovic.org/math/english/lucas/lucas.html)
  - [卢卡斯数计算器](https://web.archive.org/web/20070216024906/http://www.plenilune.pwp.blueyonder.co.uk/fibonacci-calculator.asp)

[L](../Category/整数数列.md "wikilink")
[Category:斐波那契数](../Category/斐波那契数.md "wikilink")