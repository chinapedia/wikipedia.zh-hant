**艾拔圖·阿古蘭尼**（**Alberto
Aquilani**，），[意大利足球運動員](../Page/意大利.md "wikilink")，司職[中場](../Page/中場.md "wikilink")，最後效力[西乙球會](../Page/西乙.md "wikilink")[拉斯彭马斯](../Page/拉斯帕尔马斯体育联盟.md "wikilink")
。
阿古蘭尼出身自意大利首都球會[羅馬](../Page/羅馬體育會.md "wikilink")，曾多次代表[意大利國家隊比賽](../Page/意大利國家足球隊.md "wikilink")。

## 球會生涯

### 羅馬

阿古蘭尼出生於意大利首都羅馬市郊一個富裕家庭，自年少已加入羅馬足球會。他於2003年5月10日一場對[拖連奴的聯賽中上陣](../Page/拖連奴.md "wikilink")，時僅19歲。其後他被外借至意大利[第里雅斯特的小型球會](../Page/第里雅斯特.md "wikilink")[特尼斯天拿一季](../Page/特里斯蒂納足球俱樂部.md "wikilink")，至2004-05年球季返回羅馬。他於2006年2月26日一場打比大戰中取得入球，助球會獲勝，打破了球會在[意甲的連勝紀錄](../Page/意甲.md "wikilink")。

2007-08年球季阿古蘭尼於首兩場意甲聯賽入了兩球，包括第一周對[巴勒莫時一記](../Page/巴勒莫.md "wikilink")30米外的「腳趾尾拉西」及第二周對[錫耶納時第一時間的禁區頂撞射](../Page/錫耶納.md "wikilink")。之後他受到國內一些大球會的斟介，如[國際米蘭](../Page/國際米蘭.md "wikilink")，但遭到羅馬拒絕。而他也公開稱只會忠心於羅馬。\[1\]

### 利物浦

阿古蘭尼於2009年8月7日與利物浦簽下為期5年的合約\[2\]，轉會費為2000萬歐元，以替代[哈維·阿隆索轉會留下的空缺](../Page/哈維·阿隆索.md "wikilink")。阿古蘭尼加入利物浦後穿起離隊轉投[利華古遜的](../Page/利華古遜.md "wikilink")[希比亞的](../Page/希比亞.md "wikilink")4號球衣。

在2009年10月21日與新特蘭後備隊比賽當中，阿古蘭尼以利物浦後備隊身份於最後15分鐘重上賽場，最終後備隊以2：0勝出，是次為阿古蘭尼第一次穿起利物浦球衣上場。
及後於10月28日於[英格蘭聯賽杯第四輪作客](../Page/英格蘭聯賽杯.md "wikilink")[阿仙奴的賽事當中正式代表利物浦出賽](../Page/阿仙奴.md "wikilink")，阿古蘭尼於77分鐘入替[皮利斯斯](../Page/皮利斯斯.md "wikilink")（Plessis），比賽中阿古蘭尼表演出色，其傳球給[菲臘·迪根的長傳及其門前倒掛令人驚訝](../Page/菲臘·迪根.md "wikilink")。

而阿古蘭尼終於在11月9日對[伯明翰的賽事中以後備入替首次亮相於英超](../Page/伯明翰足球會.md "wikilink")。阿古蘭尼於12月9日歐聯分組賽在主場對[費倫天拿的賽事中獲得首發的機會](../Page/費倫天拿.md "wikilink")，惜利物浦最終以2比1，不敵對手。而阿古蘭尼在英超的首發出場則等到12月26日對[狼隊的賽事](../Page/狼隊.md "wikilink")，阿古蘭尼於84分鐘由柏捷高（Daniel
Pacheco）入替，他離場時受到利物浦球迷站立鼓掌支持。

2011年，阿古蘭尼外借結束後回到利物浦，他在季前熱身賽表現出色並表示樂意為利物浦效力。

### 祖雲達斯

2010年8月21日，祖雲達斯官方宣布租借阿古蘭尼1年，賽季結束回到利物浦，成為利物浦領隊[杜格利殊陣中的一員](../Page/杜格利殊.md "wikilink")。

### AC米蘭

2011年8月，AC米蘭官方宣布與利物浦就阿古蘭尼達成租借協議並擁有優先收購權。如果他在2011-2012赛季出场超过25次（含25次），AC米兰就必须用600万欧元将其正式买下。不过600万是分三年分期付款。

### 費倫天拿

2012年夏天，由於阿古蘭尼效力AC米蘭僅上陣24.5場（替補算半場）未達25場，因此無法轉會AC米蘭。也因為這樣在外借結束後回到利物浦。同年，8月份正式轉投意甲費倫天拿。

加盟首季表現出色，攻入7球協助球隊奪得意甲第4名。

## 國家隊生涯

阿古蘭尼早於2004年已入選國家隊，迄今入了20次。在2006-07年球季，他獲得首次代表意大利國家隊之紀錄。不幸及後他因傷而要休養數月，幸而他及時復出出席了2007年於[荷蘭舉行](../Page/荷蘭.md "wikilink")[欧洲U-21足球锦标赛](../Page/2007年欧洲U-21足球锦标赛.md "wikilink")，在對捷克的比賽中取得了入球。雖然意大利及後成績平平，但阿古蘭尼仍獲評為大賽中最佳球員之一，並協助意大利拿到了2008年[北京奥运会入場券](../Page/北京奥运会.md "wikilink")。

## 职业生涯统计

\[3\]

### 俱乐部

| 俱乐部表现                                                  | 联赛                                             | 杯赛                                    | 欧洲赛事<sup>1</sup> | 其它<sup>2</sup> | 总计 |
| ------------------------------------------------------ | ---------------------------------------------- | ------------------------------------- | ---------------- | -------------- | -- |
| 赛季                                                     | 俱乐部                                            | 联赛                                    | 出场               | 进球             | 出场 |
| [2002–03](../Page/2002年至2003年意大利足球甲级联赛.md "wikilink")  | [罗马](../Page/罗马足球俱乐部.md "wikilink")            | [意甲](../Page/意大利足球甲级联赛.md "wikilink") | 1                | 0              | 1  |
| [2003–04](../Page/2003年至2004年意大利足球乙级联赛.md "wikilink")  | [特里斯蒂纳](../Page/特里斯蒂纳足球俱乐部.md "wikilink") (租借) | [意乙](../Page/意大利足球乙级联赛.md "wikilink") | 41               | 4              | —  |
| [2004–05](../Page/2004年至2005年意大利足球甲级联赛.md "wikilink")  | [罗马](../Page/罗马足球俱乐部.md "wikilink")            | [意甲](../Page/意大利足球甲级联赛.md "wikilink") | 29               | 0              | 4  |
| [2005–06](../Page/2005年至2006年意大利足球甲级联赛.md "wikilink")  | 24                                             | 3                                     | 2                | 2              | 8  |
| [2006–07](../Page/2006年至2007年_意大利足球甲级联赛.md "wikilink") | 13                                             | 1                                     | 3                | 0              | 5  |
| [2007–08](../Page/2007年至2008年意大利足球甲级联赛.md "wikilink")  | 21                                             | 3                                     | 4                | 1              | 5  |
| [2008–09](../Page/2008年至2009年意大利足球甲级联赛.md "wikilink")  | 14                                             | 2                                     | 1                | 0              | 4  |
| [2009–10](../Page/2009年至2010年英格兰足球超级联赛.md "wikilink")  | [利物浦](../Page/利物浦足球俱乐部.md "wikilink")          | [英超](../Page/英格兰足球超级联赛.md "wikilink") | 18               | 1              | 2  |
| [2010–11](../Page/2010年至2011年英格兰足球超级联赛.md "wikilink")  | 0                                              | 0                                     | 0                | 0              | 2  |
| [2010–11](../Page/2010年至2011年意大利足球甲级联赛.md "wikilink")  | [尤文图斯](../Page/尤文图斯足球俱乐部.md "wikilink") (租借)   | [意甲](../Page/意大利足球甲级联赛.md "wikilink") | 33               | 2              | 1  |
| [2011–12](../Page/2011年至2012年意大利足球甲级联赛.md "wikilink")  | [AC米兰](../Page/AC米兰.md "wikilink") (租借)        | 18                                    | 1                | 1              | 0  |
| 总计                                                     | 意大利                                            | 193                                   | 16               | 17             | 3  |
| 英格兰                                                    | 18                                             | 1                                     | 2                | 0              | 7  |
| 总计                                                     | 211                                            | 17                                    | 19               | 3              | 40 |

<sup>1</sup><small>欧洲赛事包括[欧洲足球冠军联赛](../Page/欧洲冠军联赛.md "wikilink"),
[欧洲联盟杯和](../Page/欧洲联盟杯.md "wikilink")[欧罗巴联赛](../Page/欧足联欧洲联赛.md "wikilink")</small>
<sup>2</sup><small>其它赛事包括[意大利超级杯和](../Page/意大利超级杯.md "wikilink")[英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")</small>

### 国际比赛进球

<table>
<thead>
<tr class="header">
<th><p>进球</p></th>
<th><p>日期</p></th>
<th><p>场地</p></th>
<th><p>对阵</p></th>
<th><p>进球比分</p></th>
<th><p>最终比分</p></th>
<th><p>赛事</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td><p>2008-10-15</p></td>
<td><p>意大利<a href="../Page/莱切.md" title="wikilink">莱切</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/2010年世界杯外围赛_(欧洲区).md" title="wikilink">2010年世界杯预选赛</a></p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td><p><strong>2</strong>–1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td><p>2011-8-10</p></td>
<td><p>意大利<a href="../Page/巴里.md" title="wikilink">巴里</a></p></td>
<td></td>
<td><p><strong>2</strong>–1</p></td>
<td><p>2–1</p></td>
<td><p>友谊赛</p></td>
</tr>
</tbody>
</table>

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  - [FIFA.com档案](http://www.fifa.com/worldfootball/statisticsandrecords/players/player=215623/index.html)

  - [FIGC National Team
    Archive](http://www.figc.it/nazionali/DettaglioConvocato?codiceConvocato=2400&squadra=1)


  - [罗马俱乐部时期档案](http://www.asroma.it/LaSquadraDoc.aspx?Numero=2793)

  - [利物浦时期档案](https://web.archive.org/web/20110831085907/http://www.liverpoolfc.tv/team/first-team/player/4-alberto-aquilani)

  -
[Category:意大利足球運動員](../Category/意大利足球運動員.md "wikilink")
[Category:義大利國家足球隊球員](../Category/義大利國家足球隊球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:羅馬球員](../Category/羅馬球員.md "wikilink")
[Category:祖雲達斯球員](../Category/祖雲達斯球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:費倫天拿球員](../Category/費倫天拿球員.md "wikilink")
[Category:士砵亭球員](../Category/士砵亭球員.md "wikilink")
[Category:佩斯卡拉球員](../Category/佩斯卡拉球員.md "wikilink")
[Category:萨索罗球员](../Category/萨索罗球员.md "wikilink")
[Category:拉斯彭馬斯球員](../Category/拉斯彭馬斯球員.md "wikilink")
[Category:義大利旅外足球運動員](../Category/義大利旅外足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:葡萄牙外籍足球運動員](../Category/葡萄牙外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2013年洲際國家盃球員](../Category/2013年洲際國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")

1.
2.
3.