**北燕文成帝冯跋**（），[十六國時期](../Page/十六國.md "wikilink")[北燕君主](../Page/北燕.md "wikilink")，字**文起**，小名**乞直伐**，是胡化的[漢族人](../Page/漢族人.md "wikilink")，[长乐](../Page/长乐.md "wikilink")[信都](../Page/信都.md "wikilink")（今[河北](../Page/河北.md "wikilink")[冀州市](../Page/冀州市.md "wikilink")）人。

## 生平

冯跋是[馮和之孫](../Page/馮和.md "wikilink")，其父[馮安曾任](../Page/馮安.md "wikilink")[西燕將軍](../Page/西燕.md "wikilink")。西燕亡，馮跋東遷[後燕](../Page/後燕.md "wikilink")，於後燕帝[慕容寶在位時被任命為中卫将军](../Page/慕容寶.md "wikilink")。

馮跋與其弟[馮素弗先前曾因事獲罪於後燕帝](../Page/馮素弗.md "wikilink")[慕容熙](../Page/慕容熙.md "wikilink")，因此慕容熙有殺馮跋兄弟之意，馮跋兄弟遂逃匿深山。馮跋兄弟商量說：「熙今昏虐，兼忌吾兄弟，既還首無路，不可坐受誅滅。當及時而起，立公侯之業。事若不成，死其晚乎！」於是與從兄萬泥等二十二人合謀。後燕[建始元年](../Page/建始.md "wikilink")（407年）馮跋兄弟乘車，由婦人禦，潛入都城和龙（今[辽宁](../Page/辽宁.md "wikilink")[朝阳](../Page/朝阳.md "wikilink")），匿於[北部司馬](../Page/北部司馬.md "wikilink")[孫護家](../Page/孫護.md "wikilink")。趁慕容熙送葬-{[苻后](../Page/苻訓英.md "wikilink")}-之際起事，推[高雲](../Page/高雲.md "wikilink")（慕容雲）為燕王，改元[正始](../Page/正始_\(北燕\).md "wikilink")，不久擒殺慕容熙。高雲登位後以馮跋為[侍中](../Page/侍中.md "wikilink")、-{征}-北大將軍、開府儀同三司，封武邑公，政事皆決於馮跋兄弟。

正始三年（409年），高雲為寵臣[離班](../Page/離班.md "wikilink")、[桃仁所殺](../Page/桃仁.md "wikilink")，亂事平定後，眾人推馮跋為主，馮跋遂即天王位，改元[太平](../Page/太平_\(北燕\).md "wikilink")。馮跋勤於政事，獎勵農桑，輕薄徭役，因此人民喜悅，雖外有強大的[北魏相逼](../Page/北魏.md "wikilink")，也維持20餘年的安定。

北燕太平二十二年（430年），馮跋病重，命[太子](../Page/太子.md "wikilink")[馮翼攝理國家大事](../Page/馮翼.md "wikilink")，未料宋夫人有為其子[馮受居圖謀王位之意](../Page/馮受居.md "wikilink")，馮跋之弟[馮弘於是帶兵進宮平變](../Page/馮弘.md "wikilink")，倉促間馮跋於驚懼中去世。後被[諡文成皇帝](../Page/諡號.md "wikilink")，廟號太祖。冯弘篡位，将包括冯翼在内的冯跋之子一百余人一并杀死。

## 大臣

  - 弟[馮素弗为](../Page/馮素弗.md "wikilink")[侍中](../Page/侍中.md "wikilink")、车骑大将军、[录尚书事](../Page/录尚书事.md "wikilink")
  - 弟[馮弘为侍中](../Page/馮弘.md "wikilink")、征东大将军、[尚书右仆射](../Page/尚书右仆射.md "wikilink")、汲郡公
  - 从兄[冯万泥为骠骑大将军](../Page/冯万泥.md "wikilink")、幽平二州牧
  - [务银提为上大将军](../Page/务银提.md "wikilink")、辽东太守
  - [孙护为侍中](../Page/孙护.md "wikilink")、[尚书令](../Page/尚书令.md "wikilink")、阳平公
  - [张兴为卫将军](../Page/张兴_\(十六国\).md "wikilink")、[尚书左仆射](../Page/尚书左仆射.md "wikilink")、永宁公
  - [郭生为镇东大将军](../Page/郭生.md "wikilink")、领右卫将军、陈留公
  - 从兄子[冯乳陈为征西大将军](../Page/冯乳陈.md "wikilink")、并青二州牧、上谷公
  - 姚昭为镇南大将军、司隶校尉、上党公
  - 马弗勤为吏部尚书、广宗公
  - 王难为侍中、抚军将军、颍川公

## 家族

### 祖父

  - [冯和](../Page/冯和.md "wikilink")

### 父

  - [冯安](../Page/冯安.md "wikilink")，馮和的兒子

### 兄弟

  - [冯素弗](../Page/冯素弗.md "wikilink")，馮安的兒子，馮跋的長弟。
  - [冯弘](../Page/冯弘.md "wikilink")，馮安的兒子，馮跋之弟。
  - [冯丕](../Page/冯丕.md "wikilink")，馮安的兒子，馮跋之弟。
  - [冯萬泥](../Page/冯萬泥.md "wikilink")，馮跋的從兄。
  - [冯乳陈](../Page/冯乳陈.md "wikilink")，馮跋的從兄子。

### 妻妾

  - [孙王后](../Page/孙王后.md "wikilink")
  - [宋夫人](../Page/宋夫人.md "wikilink")

### 子女

  - [冯永](../Page/冯永.md "wikilink")，冯跋的长子，409年立为太子，426年卒。
  - [冯翼](../Page/冯翼.md "wikilink")，冯跋的兒子，430年被冯弘所杀。
  - [冯受居](../Page/冯受居.md "wikilink")，冯跋的兒子，430年被冯弘所杀。
  - [乐浪公主](../Page/乐浪公主_\(北燕\).md "wikilink")。

### 后人

  - 冯于，[北齐威](../Page/北齐.md "wikilink")、檀二州刺史
      - 冯长，[隋朝](../Page/隋朝.md "wikilink")[平州卢龙县令](../Page/平州.md "wikilink")
          - 冯才，唐初[深州录事参军](../Page/深州.md "wikilink")
              - 冯庆，[唐高宗年间文林郎](../Page/唐高宗.md "wikilink")，[咸亨四年](../Page/咸亨.md "wikilink")（673年）五月卒

## 相關作品

  - [廣開土太王 (電視劇)中的馮跋](../Page/廣開土太王_\(電視劇\).md "wikilink")。

## 参考资料

  - 《[魏书](../Page/魏书.md "wikilink")》列传第八十五
  - 《[资治通鉴](../Page/资治通鉴.md "wikilink")》卷一百一十五至一百二十一

[category:北燕皇帝](../Page/category:北燕皇帝.md "wikilink")
[category:天王](../Page/category:天王.md "wikilink")
[B](../Page/category:馮姓.md "wikilink")