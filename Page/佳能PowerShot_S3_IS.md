**[佳能](../Page/佳能.md "wikilink") PowerShot S3 IS**
这部600万像素的[数码相机首次亮相于](../Page/数码相机.md "wikilink")[2006年2月并且在](../Page/2006年2月.md "wikilink")[当年五月正式发布](../Page/2006年5月.md "wikilink")。它是佳能第三款长焦数码相机，拥有12x
[变焦镜头和IS影像稳定系统](../Page/变焦镜头.md "wikilink")。它的前任型号是拥有12x
变焦镜头，[DIGIC处理器和IS影像稳定系统但是只有](../Page/DIGIC.md "wikilink")500万像素的[Canon
PowerShot S2 IS](../Page/Canon_PowerShot_S2_IS.md "wikilink")。

## Features

  - 600万有效像素
  - 1/2.5 英寸 (10 mm) CCD 感光元件
  - 12x 光学变焦，4x 數碼变焦（总共大约 48x 变焦）（35 mm 等效）
  - 光学影像稳定系统 (Shift Type)
  - 超声波马达
  - 超长的VGA分辨率有声短片拍摄功能
  - 带有iSAPS功能的佳能DIGIC II处理器
  - 支援[PictBridge协议以及佳能直接打印功能](../Page/PictBridge.md "wikilink") -
    无需个人电脑
  - 20 种拍摄模式（包括快門先決，光圈先決，全手動模式）
  - 光圈范围：F2.7/F3.5 to F8
      - 微距范围：10 至 50 cm（最宽视角）; 30 至 50 cm（略微变焦）; 90 cm 至无穷远（大于三倍变焦）
  - 超级微距功能：0 到 10 cm（在最大视角模式下聚焦）
  - 最短快门：1/3200 s，最大快门：15 s
  - 640 x 480 15/30 帧 无限制立体声短片（最大限制1GB大小和），或者 320 x 240 下 60/30/15 帧每秒
  - 电子取景器（11.5万像素）
  - 连拍能力：2.3 张每秒
  - 2.0"（翻转式）LCD（11.5万像素）
  - 尺寸：113.4 x 78 x 75.5 mm (4.5 x 3.1 x 3.0 in)
  - 重量：410 g

## 外部链接

  - [Canon Inc.](http://www.canon.com)
  - [Canon's S3 IS product information
    page](http://web.canon.jp/Imaging/pss3is/index-e.html)
  - [Canon S3 review - very
    detailed](http://www.dpreview.com/reviews/canons3is/)

|- |colspan="3" style="text-align:center;"|**佳能 "长焦" 消费数码相机** |-

[en:Canon PowerShot S3
IS](../Page/en:Canon_PowerShot_S3_IS.md "wikilink") [pl:Canon PowerShot
S3 IS](../Page/pl:Canon_PowerShot_S3_IS.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")
[Category:2006年面世的相機](../Category/2006年面世的相機.md "wikilink")