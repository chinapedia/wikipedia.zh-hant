**重庆特钢厂事件**，是2005年在[中国](../Page/中国.md "wikilink")[重庆发生的一起](../Page/重庆.md "wikilink")[群体事件](../Page/群体事件.md "wikilink")，最终酿成了流血惨剧。

中国重庆特钢厂是中国的大型国营企业，但因为近年来经营不善，官员腐败严重，造成巨大亏损。2005年7月4日，重庆市第一中级人民法院宣布其[破产](../Page/破产.md "wikilink")。[工人被迫](../Page/工人.md "wikilink")[下岗](../Page/下岗.md "wikilink")，生活艰难。

由于[上访没有回音](../Page/上访.md "wikilink")，8月初工人开始采取各种[维权行动](../Page/维权.md "wikilink")，他们自发组织数千人，在厂房附近、[212国道及](../Page/212国道.md "wikilink")[重庆市政府大楼外等地](../Page/重庆市政府大楼.md "wikilink")[静坐](../Page/静坐.md "wikilink")[请愿](../Page/请愿.md "wikilink")。

10月7日，数千工人在重庆市政府附近集会，重庆政府出动数以千计的[警力](../Page/警力.md "wikilink")，抓捕工人代表，随即发生严重的冲突，许多工人被殴打，不少围观者也未能幸免，目前已经造成3人死亡（死者中包括两名妇女，一名70岁，一名50岁，另一名是7岁的儿童），许多人受伤，9名工人代表被抓。据目击者称，现场有[警车被人掀翻](../Page/警车.md "wikilink")，但有工人认为这并非工人所为。

10月11日，第五届[亚太城市市长峰会在](../Page/亚太城市市长峰会.md "wikilink")[重庆举行](../Page/重庆.md "wikilink")。

## 注释

1.  原厂约有1.8万职工（连退休职工一起则约为4万），破产后只剩下1800多“留守人员”维持简单生产；下岗职工“买断工龄”，一年工龄只有四百多元，另一说是八百元左右。

2.  根据[中国新闻社的报道](http://www.people.com.cn/GB/channel1/11/20000928/253993.html)，重庆特钢集团原董事长兼党委书记、总经理陈宝荣已经被查处，原因是“提供虚假财务报告，隐匿企业巨额亏损，给国家和企业造成巨大损失案”，但该案还没有宣判；

3.  根据[重庆特钢工人的一次现场讲演侧记](https://web.archive.org/web/20060219153735/http://curvesoft.cndev.org/archive/2005/10/11/43971.aspx)，该厂职工离婚率达65%以上、生病不住院的比率达90%以上，职工由于绝望而自杀的事件也发生过多起；

## 外部链接

1.  重庆特钢破产“不死”奇迹终结，http://chanye.finance.sina.com.cn/yj/2005-07-14/256638.shtml
2.  谁为国企破产负责？ <http://www.nfcmag.com/ReadNews.asp?NewsID=3275>
3.  BBC中文网：[重庆工人举行抗议遭警方暴力镇压](https://web.archive.org/web/20051016005210/http://news8.thdo.bbc.co.uk/chinese/simp/hi/newsid_4340000/newsid_4342800/4342828.stm)
4.  重特钢见闻，http://www.zggr.org/index.php?entry=entry050916-020417
5.  [重庆市近两千名国企腐败分子受党纪政纪处分](http://www.people.com.cn/GB/channel1/11/20000928/253993.html)
6.  [扶持8年无力挽回 重庆特钢破产进入倒计时](http://news.qq.com/a/20050729/001311.htm)
7.  [重庆特钢工人八月起持续上街抗议](http://eapple.com.cn/bbs/dispbbs.asp?boardid=18&ID=259968)
8.  [重庆市特钢厂和中国十八冶破产工人联合拦街示威](http://forum.itdoor.net/right.php?aid=422760)
9.  重庆特钢的几张照片，http://www.xinji.org/read.php?tid=13727
10. 黄奇帆迫重特钢破产,遭遇工人强烈不满及抗议\!
    <http://bbs.swqx.com/dispbbs.asp?boardid=63&id=1903>
11. [重庆为峰会的准备](https://web.archive.org/web/20070930081824/http://www.mfzq.com.cn/Blog/Diary.aspx?cid=1000&Data=S&Tid=390040)

[Category:中华人民共和国工人运动](../Category/中华人民共和国工人运动.md "wikilink")
[Category:中华人民共和国警民冲突](../Category/中华人民共和国警民冲突.md "wikilink")
[Category:中华人民共和国重庆市群体性事件](../Category/中华人民共和国重庆市群体性事件.md "wikilink")
[Category:2005年中国群体性事件](../Category/2005年中国群体性事件.md "wikilink")
[Category:2005年重庆](../Category/2005年重庆.md "wikilink")
[Category:2005年10月](../Category/2005年10月.md "wikilink")