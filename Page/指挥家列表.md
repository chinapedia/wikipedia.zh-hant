这是世界上知名**[指挥家](../Page/指挥家.md "wikilink")**的**列表**：

## 亞洲

### 中国

  - [叶咏诗](../Page/叶咏诗.md "wikilink")（1960-）
  - [叶惠康](../Page/叶惠康.md "wikilink")
  - [石信之](../Page/石信之.md "wikilink")
  - [余隆](../Page/余隆.md "wikilink")
  - [陈佐湟](../Page/陈佐湟.md "wikilink")
  - [阎惠昌](../Page/阎惠昌.md "wikilink") （1954）
  - [李心草](../Page/李心草.md "wikilink")
  - [彭家鹏](../Page/彭家鹏.md "wikilink")
  - [朱亦兵](../Page/朱亦兵.md "wikilink")
  - [郑小瑛](../Page/郑小瑛.md "wikilink")
  - [傅人长](../Page/傅人长.md "wikilink")
  - [麦家乐](../Page/麦家乐.md "wikilink")

### 台灣

  - [廖年賦](../Page/廖年賦.md "wikilink")(1932生)
  - [亨利·梅哲](../Page/亨利·梅哲.md "wikilink")（1918-2002）
  - [江靖波](../Page/江靖波.md "wikilink")（1970-）
  - [邱君強](../Page/邱君強.md "wikilink")
  - [陳秋盛](../Page/陳秋盛.md "wikilink")
  - [張佳韻](../Page/張佳韻.md "wikilink")
  - [黃東漢](../Page/黃東漢.md "wikilink")
  - [溫以仁](../Page/溫以仁.md "wikilink")
  - [李秀文](../Page/李秀文.md "wikilink")
  - [謝建得](../Page/謝建得.md "wikilink")
  - [王雅蕙](../Page/王雅蕙.md "wikilink")
  - [簡文彬](../Page/簡文彬.md "wikilink")（1967-）
  - [呂紹嘉](../Page/呂紹嘉.md "wikilink")
  - [林望傑](../Page/林望傑.md "wikilink")
  - [葉樹涵](../Page/葉樹涵.md "wikilink")
  - [林昱丞](../Page/林昱丞.md "wikilink")
  - [吳尚倫](../Page/吳尚倫.md "wikilink")
  - [林家慶](../Page/林家慶.md "wikilink")

### 日本

  - [小泽征尔](../Page/小泽征尔.md "wikilink")（Seiji Ozawa, 1935-）
  - [朝比奈隆](../Page/朝比奈隆.md "wikilink")（Takashi Asahina, 1908-2001）
  - [菅野茂](../Page/菅野茂.md "wikilink")
  - [西本智實](../Page/西本智實.md "wikilink")
  - [久石讓](../Page/久石讓.md "wikilink")
  - [若杉弘](../Page/若杉弘.md "wikilink")
  - [近卫秀麿](../Page/近卫秀麿.md "wikilink")
  - [山田一雄](../Page/山田一雄.md "wikilink")
  - [宇野功芳](../Page/宇野功芳.md "wikilink")
  - [小林研一郎](../Page/小林研一郎.md "wikilink")
  - [宇宿允人](../Page/宇宿允人.md "wikilink")
  - [大植英次](../Page/大植英次.md "wikilink")

### 韩国

  - [郑明勋](../Page/郑明勋.md "wikilink")（Myung-Whun Chung, 1953-）

### 印度

  - [祖宾·梅塔](../Page/祖宾·梅塔.md "wikilink")（Zubin Mehta, 1936-）

## 西歐

### 英国

  - [西蒙·拉特](../Page/西蒙·拉特.md "wikilink")（Sir Simon Rattle, 1955-）
  - [科林·戴维斯](../Page/科林·戴维斯.md "wikilink")（Sir Colin Davis, 1927-2013）
  - [内维尔·马里纳](../Page/内维尔·马里纳.md "wikilink")（Sir Neville Marriner,
    1924-）
  - [汤玛斯·比彻姆](../Page/汤玛斯·比彻姆.md "wikilink")
  - [约翰·巴比罗利](../Page/约翰·巴比罗利.md "wikilink")（Sir John Barbirolli,
    1899-1970）
  - [约翰·艾略特·加德纳](../Page/约翰·艾略特·加德纳.md "wikilink")（Sir John Elliot
    Gardiner, 1943）
  - [克利斯朵夫·霍格伍德](../Page/克利斯朵夫·霍格伍德.md "wikilink")（Christopher Hogwood,
    1941-）
  - [罗格·诺宁顿](../Page/罗格·诺宁顿.md "wikilink")（Sir Roger Norrington, 1934-）

### 法国

  - [皮埃尔·蒙特](../Page/皮埃尔·蒙特.md "wikilink")
  - [皮埃尔·布列兹](../Page/皮埃尔·布列兹.md "wikilink")
  - [米歇尔·普拉松](../Page/米歇尔·普拉松.md "wikilink")
  - [喬治·普雷特](../Page/喬治·普雷特.md "wikilink")

### 德国、奥地利

  - [理查·史特勞斯](../Page/理查·史特勞斯.md "wikilink")（, 1864-1949）
  - [卡爾·貝姆](../Page/卡爾·貝姆.md "wikilink")（, 1894-1981）
  - [赫伯特·冯·卡拉扬](../Page/赫伯特·冯·卡拉扬.md "wikilink")（, 1908-1989）
  - [艾利希·克莱伯](../Page/艾利希·克莱伯.md "wikilink")（, 1890-1956）
  - [卡洛斯·克莱伯](../Page/卡洛斯·克莱伯.md "wikilink")（, 1930-2004）
  - [尼古劳斯·哈农库特](../Page/尼古劳斯·哈农库特.md "wikilink")（, 1929-2016）
  - [威尔海姆·福特萬格勒](../Page/威尔海姆·福特萬格勒.md "wikilink")（, 1886-1954）
  - [库特·马舒尔](../Page/库特·马舒尔.md "wikilink")（, 1927-）
  - [卡尔·穆辛格尔](../Page/卡尔·穆辛格尔.md "wikilink")
  - [汉斯·冯·彪罗](../Page/汉斯·冯·彪罗.md "wikilink")
  - [鲁道夫·肯普](../Page/鲁道夫·肯普.md "wikilink")
  - [布鲁诺·瓦尔特](../Page/布鲁诺·瓦尔特.md "wikilink")
  - [奥托·克伦佩勒](../Page/奥托·克伦佩勒.md "wikilink")
  - [欧根·约夫姆](../Page/欧根·约夫姆.md "wikilink")（, 1902-1987）
  - [安德列·普列文](../Page/安德列·普列文.md "wikilink")（, 1929-）
  - [汉斯·卡纳匹兹布什](../Page/汉斯·卡纳匹兹布什.md "wikilink")
  - [威利·博斯科夫斯基](../Page/威利·博斯科夫斯基.md "wikilink")
  - [沃尔夫冈·萨瓦利希](../Page/沃尔夫冈·萨瓦利希.md "wikilink")
  - [赫尔曼·舍尔兴](../Page/赫尔曼·舍尔兴.md "wikilink")
  - [卡尔·李希特](../Page/卡尔·李希特.md "wikilink")
  - [卡尔·舒李希特](../Page/卡尔·舒李希特.md "wikilink")
  - [克里斯蒂安·蒂勒曼](../Page/克里斯蒂安·蒂勒曼.md "wikilink")
  - [弗朗兹·威尔瑟-莫斯特](../Page/弗朗兹·威尔瑟-莫斯特.md "wikilink")

### 瑞士

  - [恩奈斯特·安塞美](../Page/恩奈斯特·安塞美.md "wikilink")
  - [夏尔·迪图瓦](../Page/夏尔·迪图瓦.md "wikilink")
  - [菲利浦·約丹](../Page/菲利浦·約丹.md "wikilink")（Philippe Jordan, 1974-）

### 義大利

  - [克劳迪奥·阿巴多](../Page/克劳迪奥·阿巴多.md "wikilink")（Claudio Abbado,
    1933-2014）
  - [里卡多·穆蒂](../Page/里卡多·穆蒂.md "wikilink")（Riccardo Muti, 1941-）
  - [基賽普·辛諾波里](../Page/基賽普·辛諾波里.md "wikilink")（Giuseppe Sinopoli,
    1946-2001）
  - [阿尔图罗·托斯卡尼尼](../Page/阿尔图罗·托斯卡尼尼.md "wikilink")（Arturo Toscanini,
    1867-1957）
  - [卡尔罗·马里亚·朱里尼](../Page/卡尔罗·马里亚·朱里尼.md "wikilink")

### 荷蘭

  - [艾度·迪華特](../Page/艾度·迪華特.md "wikilink")（Edo de Waart, 1941-）

## 東歐

### 俄罗斯

  - [叶夫根尼·穆拉文斯基](../Page/叶夫根尼·穆拉文斯基.md "wikilink")
  - [米哈伊尔·普雷特涅夫](../Page/米哈伊尔·普雷特涅夫.md "wikilink")
  - [姆斯蒂斯拉夫·罗斯特罗波维奇](../Page/姆斯蒂斯拉夫·罗斯特罗波维奇.md "wikilink")（Mstislav
    Rostropovich, 1927-2007）
  - [鲁道夫·巴尔沙](../Page/鲁道夫·巴尔沙.md "wikilink")（Rudolf Barshai, 1924-2010）
  - [瓦列里·格吉耶夫](../Page/瓦列里·格吉耶夫.md "wikilink")
  - [弗拉基米尔·达维多维奇·阿什肯纳齐](../Page/弗拉基米尔·达维多维奇·阿什肯纳齐.md "wikilink")（Vladimir
    Ashkenazy, 1937-）

### 捷克

  - [约瑟夫·苏克](../Page/约瑟夫·苏克.md "wikilink")
  - [瓦茨拉夫·塔里赫](../Page/瓦茨拉夫·塔里赫.md "wikilink")
  - [拉法埃尔·库贝里克](../Page/拉法埃尔·库贝里克.md "wikilink")
  - [吉里·贝洛拉维克](../Page/吉里·贝洛拉维克.md "wikilink")
  - [雅各布·胡鲁萨](../Page/雅各布·胡鲁萨.md "wikilink")

### 波兰

  - [克里斯托弗·潘德列茨基](../Page/克里斯托弗·潘德列茨基.md "wikilink")
  - [斯坦尼斯拉夫·斯克洛瓦切夫斯基](../Page/斯坦尼斯拉夫·斯克洛瓦切夫斯基.md "wikilink")

### 匈牙利

  - [乔治·索尔蒂](../Page/乔治·索尔蒂.md "wikilink")（Sir George Solti, 1912-1997）
  - [费伦茨·弗里乔伊](../Page/费伦茨·弗里乔伊.md "wikilink")
  - [阿尔图·尼基什](../Page/阿尔图·尼基什.md "wikilink")
  - [弗里兹·莱纳](../Page/弗里兹·莱纳.md "wikilink")

### 拉脱维亚

  - [马里斯·扬颂斯](../Page/马里斯·扬颂斯.md "wikilink") (Mariss Jansons, 1943-)
  - [安德雷斯·尼爾森斯](../Page/安德雷斯·尼爾森斯.md "wikilink") (Andris Nelsons, 1978-)

### 罗马尼亚

  - [切利比达奇](../Page/切利比达奇.md "wikilink")

### 保加利亚

  - [埃米尔·查卡罗夫](../Page/埃米尔·查卡罗夫.md "wikilink")

### 希腊

  - [德米特里·米特罗波洛斯](../Page/德米特里·米特罗波洛斯.md "wikilink")

### 委內瑞拉

  - [古斯塔夫·杜達美](../Page/古斯塔夫·杜達美.md "wikilink")（Gustavo Dudamel, 1981-）

## 美国

  - [伦纳德·伯恩斯坦](../Page/伦纳德·伯恩斯坦.md "wikilink")（Leonard Bernstein,
    1918-1990）
  - [洛林·马泽尔](../Page/洛林·马泽尔.md "wikilink")（Lorin Maazel, 1930-）
  - [詹姆士·列文](../Page/詹姆士·列文.md "wikilink")（James Levine, 1943-）
  - [乔治·塞尔](../Page/乔治·塞尔.md "wikilink")
  - [列奥波德·斯托科夫斯基](../Page/列奥波德·斯托科夫斯基.md "wikilink")
  - [邁可·提爾森·湯瑪斯](../Page/邁可·提爾森·湯瑪斯.md "wikilink")（Michael Tilson
    Thomas, 1944）

## 澳大利亞

  - [理查德·波寧吉](../Page/理查德·波寧吉.md "wikilink")（Richard Bonynge, 1930-）

[Category:指挥家](../Category/指挥家.md "wikilink")
[Category:音樂家列表](../Category/音樂家列表.md "wikilink")