教宗聖**雷定一世**（，），原名不詳，於422年至432年4月6日為[教宗](../Page/教宗.md "wikilink")。

教宗雷定一世是一个[罗马人](../Page/古罗马.md "wikilink")，他可能是[羅馬皇帝](../Page/羅馬皇帝列表.md "wikilink")[瓦伦提尼安三世的近亲](../Page/瓦伦提尼安三世.md "wikilink")。他的早年生活不明，他的父亲可能叫普利斯库斯。据称他曾在[米兰与圣](../Page/米兰.md "wikilink")[安波羅修生活过一段时间](../Page/安波羅修.md "wikilink")。最早可以考证的关于他的记载是416年[教宗諾森一世提到他是一名祭司](../Page/教宗依诺增爵一世.md "wikilink")。

据传说一些[礼拜仪式是他创立的](../Page/礼拜仪式.md "wikilink")，但是并没有说明哪些。431年他派代表参加将[景教判为异端的](../Page/景教.md "wikilink")[以弗所公會議](../Page/以弗所公會議.md "wikilink")。四封他于431年3月15日因此写给[非洲](../Page/非洲.md "wikilink")、[伊利里亚](../Page/伊利里亚.md "wikilink")、[塞萨洛尼基和](../Page/塞萨洛尼基.md "wikilink")[纳尔榜的主教的信件被保存下来了](../Page/纳博讷.md "wikilink")，不过是从[希腊语翻译过来的](../Page/希腊语.md "wikilink")，[拉丁语原文失落了](../Page/拉丁语.md "wikilink")。

他热衷教条，積極打擊[白拉奇主义](../Page/白拉奇主义.md "wikilink")。431年他派[帕拉第阿斯赴](../Page/帕拉第阿斯.md "wikilink")[爱尔兰任主教](../Page/爱尔兰.md "wikilink")。后来[聖博德继续了帕拉第阿斯的传教工作](../Page/聖博德.md "wikilink")。他还打擊罗马市的[诺洼天派](../Page/诺洼天主义.md "wikilink")，收押他们的主教，禁止他们的崇拜。他强烈反对对他的前任所制定的规则进行任何修改。罗马天主教会将他尊为圣人。

他于432年4月6日逝世，一开始他被葬在[撒拉里亚大道上的圣普里西拉墓地上](../Page/撒拉里亚大道.md "wikilink")，后来他的遗体被改藏在[圣普拉赛德教堂内](../Page/圣巴西德圣殿.md "wikilink")。

在图像中教宗雷定一世的代表往往为一只鸽子、一条龙和火焰。

## 譯名列表

  - 策肋定：[梵蒂冈广播电台](http://www.radiovaticana.org/cinesegb/santuari/37catacombe.html)作策肋定。
  - 雷定：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作雷定。

[C](../Category/教宗.md "wikilink") [C](../Category/羅馬人.md "wikilink")
[C](../Category/基督教聖人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")