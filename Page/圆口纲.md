**圆口纲**（[学名](../Page/学名.md "wikilink")：）是在[动物分类学上提出的一種分類](../Page/动物分类学.md "wikilink")，與已經滅絕的[甲冑魚類](../Page/甲冑魚類.md "wikilink")（Ostracoderms）同位於[無頷總綱](../Page/無頷總綱.md "wikilink")（Agnatha）之下。現存代表動物為[七鰓鰻](../Page/七鰓鰻.md "wikilink")（又名：[八目鰻](../Page/八目鰻.md "wikilink")）。

## 特徵

圆口纲無真正上下頜。口為吸盤型，故稱之無頜類。無成對的附肢。（脊椎動物門中唯一沒有附肢的動物。）終生保留脊索，沒有真正的脊椎骨，只有一些軟骨小弧片直立於脊索上方及神經管的兩側，是脊椎的雛形。（脊椎動物笫一階段進化的代表特徵。）具有獨特的呼吸器官──鰓囊，囊壁為由內胚層來源的褶皺狀鰓絲，遍佈大量的微血管，可供氣體交換。

## 分類

现存种类包括：

  - [七鳃鳗亚纲](../Page/七鳃鳗亚纲.md "wikilink") Hyperoartia
  - [盲鳗亚纲](../Page/盲鳗亚纲.md "wikilink") Myxini

這一理論存在爭議。根據各種[遺傳學](../Page/遺傳學.md "wikilink")、[古生物學研究](../Page/古生物學.md "wikilink")，[盲鰻被認爲與](../Page/盲鰻.md "wikilink")[脊椎動物距離較遠](../Page/脊椎動物.md "wikilink")，而被劃到[有頭動物之下](../Page/有頭動物.md "wikilink")\[1\]，但另一些研究卻顯示[盲鰻與](../Page/盲鰻.md "wikilink")[七鰓鰻的關聯較與](../Page/七鰓鰻.md "wikilink")[有頜類的關聯更密切](../Page/有頜類.md "wikilink")，有可能構成一個[單系群](../Page/單系群.md "wikilink")\[2\]\[3\]，而七鰓鰻與[頭甲魚的相似特徵則被認爲可能是](../Page/頭甲魚綱.md "wikilink")[趨同演化的結果](../Page/趨同演化.md "wikilink")。

## 參見

  - [有頭動物](../Page/有頭動物.md "wikilink")(Craniata)
  - [有頜類](../Page/有頜類.md "wikilink")(Gnathostomata)
  - [頭甲魚綱](../Page/頭甲魚綱.md "wikilink")(Cephalaspidomorphi)
  - [鰭甲魚綱](../Page/鰭甲魚綱.md "wikilink")(Pteraspidomorphi)

## 參考文獻

[Category:無頜總綱](../Category/無頜總綱.md "wikilink")

1.
2.
3.