[PRC_1_cent_(1987).png](https://zh.wikipedia.org/wiki/File:PRC_1_cent_\(1987\).png "fig:PRC_1_cent_(1987).png")
**分**，[香港又稱做](../Page/香港.md "wikilink")**-{仙}-**、**厘**；在[台灣](../Page/台灣.md "wikilink")，年長者則稱其為**-{仙}-**、**針**。**分**是一種[貨幣](../Page/流通貨幣.md "wikilink")[單位](../Page/单位_\(度量衡\).md "wikilink")，而一分即百分之一[元](../Page/銀圓.md "wikilink")。在[英國和一些使用](../Page/英国.md "wikilink")[英語的國家](../Page/英语.md "wikilink")，人們一般都叫分做[便士](../Page/便士.md "wikilink")。

**-{仙}-**為英文「」的音譯，為香港港幣單位的稱呼，“-{分}-”字並不常用。而香港口語詞語「神沙」（通常指硬幣，同義詞為“斗零”“碎銀”等）亦有學者認為是英文「」的音譯。

「」的字源則是[拉丁文](../Page/拉丁语.md "wikilink")「」和[希臘文](../Page/希腊语.md "wikilink")「」。這兩個字都是[一百或](../Page/百.md "wikilink")“-{分}-為一百份”的意思。

分的貨幣符號是¢或c，例如5¢或5c即是代表5分。前者常用於[美國和](../Page/美國.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")，而後者則流行於[澳洲](../Page/澳大利亚.md "wikilink")、[紐西蘭和](../Page/新西兰.md "wikilink")[歐元區](../Page/欧元区.md "wikilink")。[南非則只用後者](../Page/南非.md "wikilink")。

## 現時使用分或其相關單位的一些貨幣

  - [阿根廷比索](../Page/阿根廷比索.md "wikilink")
  - [澳大利亞元](../Page/澳大利亚元.md "wikilink")（現時硬幣的最小面值為5仙）
  - [加拿大元](../Page/加拿大元.md "wikilink")
  - [巴西黑奧](../Page/黑奧.md "wikilink")
  - [歐元](../Page/欧元.md "wikilink")（現時部份歐洲國家的歐元，硬幣的最小面值為1欧分）
  - [港元](../Page/港元.md "wikilink")（現時硬幣的最小面值為10仙，稱為[一毫](../Page/香港一毫硬幣.md "wikilink")，英語仍稱10
    cents）
  - [澳門幣](../Page/澳門幣.md "wikilink")（現時硬幣的最小面值為10仙，稱為[一毫](../Page/一毫.md "wikilink")，葡文仍稱10
    avos）
  - [中國大陸人民幣](../Page/人民幣.md "wikilink")（所有分币已停止发行新币，紙分幣已於2007年4月1日停用。目前仍有1分、2分、5分三种硬分币处于法理流通状态，但基本已无人使用，商家通常以角或元作为结算单位）
  - [新台幣](../Page/新台幣.md "wikilink")（現時硬幣的最小面值為50仙，而且在台灣不常使用.）(正式說法
    5角<伍角>)
  - [立陶宛立特](../Page/立陶宛立特.md "wikilink")
  - [馬來西亞令吉](../Page/令吉.md "wikilink")(現時硬幣的最小面值為5仙。)
  - [馬爾他里拉](../Page/馬爾他里拉.md "wikilink")
  - [墨西哥比索](../Page/墨西哥比索.md "wikilink")
  - [新西蘭元](../Page/新西蘭元.md "wikilink")（現時硬幣的最小面值為10仙）
  - [菲律賓比索](../Page/菲律賓比索.md "wikilink")
  - [新加坡元](../Page/新加坡元.md "wikilink")
  - [南非蘭特](../Page/南非蘭特.md "wikilink")
  - [西班牙比塞塔](../Page/比塞塔.md "wikilink")
  - [斯里蘭卡盧比](../Page/斯里蘭卡盧比.md "wikilink")
  - [美元](../Page/美元.md "wikilink")（稱作[美分](../Page/美分.md "wikilink")）
  - [俄羅斯盧布](../Page/俄羅斯盧布.md "wikilink")（稱作）（Копейка）
  - [白俄羅斯盧布](../Page/白俄羅斯盧布.md "wikilink")（稱作戈比）（Копейка）
  - [烏克蘭](../Page/烏克蘭.md "wikilink")[赫里夫尼亞](../Page/赫里夫尼亞.md "wikilink")（稱作戈比）（Копейка）

## 曾經使用分或其相關單位的一些貨幣

  - [日元](../Page/日圓.md "wikilink")（現時沒有比元更小的單位）
  - [智利比索](../Page/智利比索.md "wikilink")（現時沒有比[比索更小的單位](../Page/比索_\(货币\).md "wikilink")）
  - [科威特丁那](../Page/科威特丁那.md "wikilink")（一[丁那分為](../Page/丁那.md "wikilink")1000「」）
  - [Mauritanian
    ouguiya](../Page/Mauritanian_ouguiya.md "wikilink")（一「」分為5「」）
  - [阿里亞里](../Page/阿里亞里.md "wikilink")（一[亞里分為](../Page/亞里.md "wikilink")5「」）
  - [韩元](../Page/韩元.md "wikilink")

[Category:分币](../Category/分币.md "wikilink")