《**Hello\!
Morning**》（）是[日本](../Page/日本.md "wikilink")[東京電視台](../Page/東京電視台.md "wikilink")（TV
Tokyo）於2000年4月9日至2007年4月1日期間，逢[星期日上午](../Page/星期日.md "wikilink")11時30分（[日本時間](../Page/日本時間.md "wikilink")）播放有關[早安家族的](../Page/早安家族.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")，常被人簡稱為《》（Haromoni）以及《HM》。該節目[台灣播映權是由](../Page/台灣.md "wikilink")[國興衛視取得](../Page/國興衛視.md "wikilink")，國興衛視譯為《新早安少女組》。

這節目以“[早安少女組。](../Page/早安少女組。.md "wikilink")”為固定班底，有時也會邀請[早安家族裡的其他成員](../Page/早安家族.md "wikilink")，以及其他名人嘉賓參與節目。

節目旁白一開始由[青柳秀侑擔任](../Page/青柳秀侑.md "wikilink")(2000年4月9日至2001年9月30日)，後來由知名[聲優](../Page/聲優.md "wikilink")[諏訪部順一接手至節目收掉](../Page/諏訪部順一.md "wikilink")(2001年10月7日至2007年4月1日)。

起初節目頗受歡迎，[收視率也達](../Page/收視率.md "wikilink")4至5%左右；但其後因早安少女組的人氣下降，收視率也慢慢減少，最低跌至1%左右。至2007年4月8日起，節目改以新名稱《[Haromoni@](../Page/Haromoni@.md "wikilink")》（）重新推出，並縮短每集播放時間至半小時，以及換上新旁白[池田昌子](../Page/池田昌子.md "wikilink")。在台灣地區，國興衛視只播出Hello\!
Morning，並未播出Haromoni@

## 參見

  - [Haromoni@](../Page/Haromoni@.md "wikilink")

## 節目的變遷

[Category:早安少女組。](../Category/早安少女組。.md "wikilink")
[Category:東京電視台綜藝節目](../Category/東京電視台綜藝節目.md "wikilink")
[Category:2000年日本電視節目](../Category/2000年日本電視節目.md "wikilink")
[Category:國興衛視電視節目](../Category/國興衛視電視節目.md "wikilink")