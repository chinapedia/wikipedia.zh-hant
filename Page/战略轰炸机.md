[B-1A_Orthographic.PNG](https://zh.wikipedia.org/wiki/File:B-1A_Orthographic.PNG "fig:B-1A_Orthographic.PNG")
[Simulated_Napalm_Airstrike.jpg](https://zh.wikipedia.org/wiki/File:Simulated_Napalm_Airstrike.jpg "fig:Simulated_Napalm_Airstrike.jpg")
**战略轰炸机**（**Strategic
Bomber**）係[轟炸機的一种](../Page/轟炸機.md "wikilink")，從高空對地面进行远程投弹的大型[軍用飛機](../Page/軍用飛機.md "wikilink")。与[战术轰炸机被用于對某个交战区内的军队和军事设备轟炸不同](../Page/战术轰炸机.md "wikilink")，战略轰炸机的用途是執行遠程轟炸和[战略轰炸](../Page/战略轰炸.md "wikilink")，運載長射程、高威力的[空對地飛彈或核武器](../Page/空對地飛彈.md "wikilink")，對敌方心臟地區的战略目标如主要军事设施、工厂和城市等进行打击，一舉達到大幅削弱敵方的戰爭能力。当然，战略轰炸机也可用于[战术轰炸](../Page/战术轰炸.md "wikilink")。

现代战略轰炸机的定义，根据1990年6月1日签署的苏美《第一阶段削减进攻性武器条约》，满足下述2种条件之一即被视为“战略轰炸机”：

1.  航程大于8000千米。对于空射导弹而言，航程是指标准设计模式下飞行至燃料耗尽条件下可以达到的最大地表弧长。对于弹道导弹而言，航程是发射点与再入器落点之间的飞行轨迹所投射在地表的弧长。对于飞机而言，航程是搭载7500千克军械不进行空中加油最经济模式可以飞行的最大距离，着陆后内油低于燃料箱最大容量5%。
2.  搭载“长射程空射核战斗部巡航导弹”。其中，射程即上一条的空射导弹的航程，并达到600千米。

据此，5个系列轰炸机被称为战略轰炸机：

  - 美国：[B-1B](../Page/B-1槍騎兵戰略轟炸機.md "wikilink")，[B-2A](../Page/B-2幽灵战略轰炸机.md "wikilink")，[B-52G/H](../Page/B-52同溫層堡壘轟炸機.md "wikilink")
  - 苏联：[Tu-95系列](../Page/Tu-95.md "wikilink")，[Tu-160](../Page/Tu-160.md "wikilink")

对于可搭载600千米级核战斗部反舰导弹的[Tu-16和](../Page/Tu-16.md "wikilink")[Tu-22M3系列](../Page/Tu-22M.md "wikilink")，停止使用并销毁核武装，不列为战略轰炸机。B-1B也根据条约拆解了AGM-86B导弹发射装置，不再列入战略轰炸机队伍。

如果轰炸机属于“战略轰炸机”，但解除了“长射程空射核巡航导弹”装备，则视为“改装型战略轰炸机”，即“具有与负责核打击任务的战略轰炸机基本设计相同的飞机”，如[图-142](../Page/图-142.md "wikilink")。可在缔约国联合约定同意的前提下，在“战略轰炸机”特定军事基地距离大于310英里之外的基地驻扎。

## 世界各國主要战略轰炸机

### 第一次世界大战

  - [伊利亞·穆羅梅茨](../Page/伊利亞·穆羅梅茨.md "wikilink")，1914年1月26日首飞的双翼四发，世界上第一种重型轰炸机。
  - [齐柏林飞船](../Page/齐柏林飞船.md "wikilink")
  - [Gotha](../Page/Gotha_G.md "wikilink")
  - [Handley Page V/1500](../Page/Handley_Page_V/1500.md "wikilink")

### [第二次世界大战](../Page/第二次世界大战.md "wikilink")

[Boeing_B-29A,_44-62207,_To_Each_His_Own,_98th_BW,_344th_BS.jpg](https://zh.wikipedia.org/wiki/File:Boeing_B-29A,_44-62207,_To_Each_His_Own,_98th_BW,_344th_BS.jpg "fig:Boeing_B-29A,_44-62207,_To_Each_His_Own,_98th_BW,_344th_BS.jpg")

  - [蘭開斯特轟炸機](../Page/蘭開斯特轟炸機.md "wikilink")
  - [哈利法克斯轟炸機](../Page/哈利法克斯轟炸機.md "wikilink")
  - [斯特林轟炸機](../Page/斯特林轟炸機.md "wikilink")
  - [B-17飞行堡垒](../Page/B-17轟炸機.md "wikilink")
  - [B-24解放者](../Page/B-24轟炸機.md "wikilink")
  - [B-29超级堡垒](../Page/B-29超級堡壘轟炸機.md "wikilink")
  - [F.222式轰炸机](../Page/F.222式轰炸机.md "wikilink")
  - [Il-4轟炸機](../Page/Il-4轟炸機.md "wikilink")
  - [pe-8轟炸機](../Page/pe-8轟炸機.md "wikilink")
  - [烏拉轟炸機](../Page/烏拉轟炸機.md "wikilink")（Ural bomber）
  - [連山轟炸機](../Page/連山轟炸機.md "wikilink")
  - [深山轟炸機](../Page/深山轟炸機.md "wikilink")
  - [富嶽轟炸機](../Page/富嶽轟炸機.md "wikilink")（計劃）

### [冷战時期](../Page/冷战.md "wikilink")

[B-52_&_Tu-95.jpg](https://zh.wikipedia.org/wiki/File:B-52_&_Tu-95.jpg "fig:B-52_&_Tu-95.jpg")\]\]
[Энгельс_Ту-160_02_фото_3.jpg](https://zh.wikipedia.org/wiki/File:Энгельс_Ту-160_02_фото_3.jpg "fig:Энгельс_Ту-160_02_фото_3.jpg")
冷战中曾拥有战略轰炸机的国家不少，第三世界国家基本都是靠苏联输入或是接收[苏联解体财产](../Page/苏联解体.md "wikilink")（靠着俄制轰炸机主要是[图-16拥有过此种飞机的国家有亚美尼亚](../Page/图-16.md "wikilink")、阿塞拜疆、白俄罗斯、乌克兰、格鲁吉亚、印度尼西亚、埃及、伊拉克，印度有图-95但到今天全部都是反潜巡逻机改型），在20世纪它们基本都退役了，只有美俄和中国还拥有战略轰炸机（严格来说埃及是在2000年退役了所有图-16和轰-6，[伊拉克是在](../Page/伊拉克.md "wikilink")2003年的战事中被摧毁了海湾战争中幸存的最后一架轰-6\[1\]）。

#### 美国

  - [B-36和平締造者](../Page/B-36和平締造者轟炸機.md "wikilink")
  - [B-47同温层喷气](../Page/B-47轟炸機.md "wikilink")
  - [B-52同温层堡垒](../Page/B-52同温层堡垒.md "wikilink")
  - [B-58盗贼](../Page/B-58盗贼.md "wikilink")
  - [XB-70女武神式轟炸機](../Page/XB-70女武神式轟炸機.md "wikilink")
  - [FB-111](../Page/FB-111.md "wikilink")

#### 苏联

  - [Tu-4](../Page/圖-4.md "wikilink")，B-29的苏联版
  - [米亚-4](../Page/米亚-4.md "wikilink")
  - [Tu-16](../Page/Tu-16轟炸機.md "wikilink")，它的出现标志着苏联进入了喷气战略轰炸机时代。\[2\]
  - [Tu-95](../Page/Tu-95轟炸機.md "wikilink")
  - [Tu-22M](../Page/Tu-22M.md "wikilink")
  - [Tu-160](../Page/Tu-160.md "wikilink")

#### 中国

  - [图-4](../Page/图-4.md "wikilink")，引进自苏联，它是中国拥有的第一种战略轰炸机。\[3\]
  - [轰-6](../Page/轰-6.md "wikilink")

#### 英国

  - [Avro Lincoln](../Page/Avro_Lincoln.md "wikilink")
  - [Vickers Valiant](../Page/Vickers_Valiant.md "wikilink")
  - [Avro Vulcan](../Page/火神式轰炸机.md "wikilink")
  - [胜利者式轰炸机](../Page/胜利者式轰炸机.md "wikilink")（数款中最后的胜利者式于1993年退役，从此英国无战略轰炸机。）

#### 法国

  - [达索](../Page/达索.md "wikilink")
    [幻影IV式轰炸机](../Page/幻影IV式轰炸机.md "wikilink")（1963年开始生产，1996年轰炸版本退役，2005年侦察版退役）

### 21世纪

进入21世纪後，全球共有四个拥有战略轰炸机的国家：美国、俄罗斯、中国、印度。

#### 美国

  - [B-2幽灵](../Page/B-2精神式战略轰炸机.md "wikilink")－由[麻省理工學院和](../Page/麻省理工學院.md "wikilink")[諾斯羅普·格魯門公司一起為](../Page/諾斯羅普·格魯門.md "wikilink")[美國空軍研製生產](../Page/美國空軍.md "wikilink")。
  - [B-1槍騎兵](../Page/B-1槍騎兵戰略轟炸機.md "wikilink")
  - [B-52同温层堡垒](../Page/B-52同溫層堡壘轟炸機.md "wikilink")
  - [B-21](../Page/B-21戰略轟炸機.md "wikilink")（研發中）
  - [2037](../Page/2037轟炸機.md "wikilink")（計劃中）

#### 俄罗斯

  - [Tu-160海盗旗](../Page/Tu-160海盗旗战略轰炸机.md "wikilink")
  - [Tu-22M逆火](../Page/Tu-22M轟炸機.md "wikilink")
  - [Tu-95熊式](../Page/Tu-95轟炸機.md "wikilink")
  - [PAK-DA](../Page/PAK-DA戰略轟炸機.md "wikilink")（研發中）

#### 中国

  - [轰-6](../Page/轰-6.md "wikilink")
  - [轰-20](../Page/轰-20.md "wikilink")（研发中）

#### 印度

  - [Tu-22M逆火](../Page/Tu-22M轟炸機.md "wikilink") \[4\]

## 現役戰略轟炸機性能一覽表

<table>
<thead>
<tr class="header">
<th><p>型號</p></th>
<th><p>俄<a href="../Page/Tu-160海盗旗战略轰炸机.md" title="wikilink">Tu-160海盗旗战略轰炸机</a></p></th>
<th><p>俄<a href="../Page/Tu-95轟炸機.md" title="wikilink">Tu-95熊式轟炸機</a></p></th>
<th><p>美<a href="../Page/B-1槍騎兵戰略轟炸機.md" title="wikilink">B-1槍騎兵戰略轟炸機</a></p></th>
<th><p>美<a href="../Page/B-2幽灵战略轰炸机.md" title="wikilink">B-2幽灵戰略轟炸機</a></p></th>
<th><p>美<a href="../Page/B-52同溫層堡壘轟炸機.md" title="wikilink">B-52同溫層保壘轟炸機</a></p></th>
<th><p><a href="../Page/轰-6.md" title="wikilink">轰-6战神轰炸机</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>外觀</p></td>
<td><p><a href="../Page/file:Kremlin_Tupolev_Tu-160.jpg.md" title="wikilink">135px</a></p></td>
<td><p><a href="../Page/file:Tu-142M-1988.jpg.md" title="wikilink">135px</a></p></td>
<td><p><a href="../Page/file:B-1b_fly-by_2004.jpg.md" title="wikilink">135px</a></p></td>
<td><p><a href="../Page/file:B-2_Spirit_original.jpg.md" title="wikilink">140px</a></p></td>
<td><p><a href="../Page/file:Usaf.Boeing_B-52.jpg.md" title="wikilink">140px</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xian_H-6M.jpg" title="fig:Xian_H-6M.jpg">Xian_H-6M.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>最大起飛重量（公噸）</p></td>
<td><p><strong>275</strong></p></td>
<td><p>172[5]</p></td>
<td><p>216.3</p></td>
<td><p>171</p></td>
<td><p>229[6]</p></td>
<td><p>75.8[7] [8]</p></td>
</tr>
<tr class="odd">
<td><p>載彈量與戰鬥部重量（公噸）</p></td>
<td><p>45</p></td>
<td><p>20</p></td>
<td><p>34[9][10][11]<br />
(+22,7 加上外部掛架)</p></td>
<td><p>27</p></td>
<td><p>22,7</p></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td><p>最高速度（公里/每小時）</p></td>
<td><p>2 230</p></td>
<td><p>882</p></td>
<td><p>1 335[12]</p></td>
<td><p>1,010</p></td>
<td><p>957</p></td>
<td><p>1,050<sup>不确定</sup></p></td>
</tr>
<tr class="odd">
<td><p>作戰半徑（公里）</p></td>
<td><p>6 000</p></td>
<td><p>6 500</p></td>
<td><p>5 543</p></td>
<td><p>5 300</p></td>
<td><p><strong>7 210</strong>[13]</p></td>
<td><p>3500</p></td>
</tr>
<tr class="even">
<td><p>負載狀態下單趟飛行航程（公里）</p></td>
<td><p>10 500</p></td>
<td><p><strong>12 100</strong></p></td>
<td><p>12 000</p></td>
<td><p>不適用</p></td>
<td><p>不適用</p></td>
<td><p>无相关资料</p></td>
</tr>
<tr class="odd">
<td><p>最大航程（公里）</p></td>
<td><p>13 950[14]</p></td>
<td><p>15000</p></td>
<td><p>13 500[15]</p></td>
<td><p>11 100</p></td>
<td><p><strong>16 090</strong>[16]</p></td>
<td><p>9000</p></td>
</tr>
<tr class="even">
<td><p>最大升限（公尺）</p></td>
<td><p>21 765</p></td>
<td><p>18 000</p></td>
<td><p><strong>23 600</strong></p></td>
<td><p>18 290</p></td>
<td><p>16 765</p></td>
<td><p>12,800<sup>不确定</sup></p></td>
</tr>
<tr class="odd">
<td><p>發動機推力（克力）</p></td>
<td><p><strong>100 000</strong></p></td>
<td><p>48 000</p></td>
<td><p>76 200</p></td>
<td><p>31 300</p></td>
<td><p>61 680</p></td>
<td><p>无相关资料</p></td>
</tr>
<tr class="even">
<td><p>縮小雷達反射（RCS）處理</p></td>
<td><p>部分隱身處理</p></td>
<td><p>無</p></td>
<td><p>部份隱身處理</p></td>
<td><p>全隱身處理</p></td>
<td><p>無</p></td>
<td><p>無</p></td>
</tr>
<tr class="odd">
<td><p>服役數量</p></td>
<td><p>16<ref name='militarybalance2010'>{{книга</p></td>
<td><p>автор = The International Institute For Strategic Studies IISS</p></td>
<td><p>заглавие = The Military Balance 2010</p></td>
<td><p>издательство = Nuffield Press</p></td>
<td><p>год = 2010</p></td>
<td><p>страниц = 492</p></td>
</tr>
</tbody>
</table>

## 参见

  - [战略轰炸](../Page/战略轰炸.md "wikilink")
  - [战术轰炸机](../Page/战术轰炸机.md "wikilink")
  - [一次打击](../Page/一次打击.md "wikilink")
  - [二次打击](../Page/二次打击.md "wikilink")

## 注释

[Category:军用飞机](../Category/军用飞机.md "wikilink")
[Category:战略轰炸](../Category/战略轰炸.md "wikilink")
[Category:战略轰炸机](../Category/战略轰炸机.md "wikilink")

1.

2.  One Hundred Years of World Military Aircraft P369

3.  The Military-Industrial Complex and American Society P31

4.

5.  [Летно-технические характеристики
    Ту-95](http://www.airwar.ru/enc/bomber/tu95.html)

6.  Ильин В. Е., Левин М. А. Бомбардировщики.-М.:Виктория,АСТ.1996.-272
    с. Том 1. стр. 68 ISBN 5-89327-004-5

7.  <http://www.leitingcn.com/news/201504/128322.html>

8.  <http://www.bjnews.com.cn/feature/2015/09/05/376486.html>

9.

10.

11. [Rockwell B-1 Lancer](../Page/Rockwell_B-1_Lancer.md "wikilink")

12. [Национальный музей ВВС США: B-1B LANCER — Страница
    фактов.](https://archive.is/20121212202213/http://www.af.mil/information/factsheets/factsheet.asp?fsID=81)

13.
14. [ХАРАКТЕРИСТИКИ БОМБАРДИРОВЩИКА
    Ту-160](http://www.airforce.ru/aircraft/tupolev/tu-160/book/page_2_6.htm)

15. [B-1B](http://www.internetlooks.com/b1b.html)

16.