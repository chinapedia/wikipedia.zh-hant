**弗格斯縣**（）是[美國](../Page/美國.md "wikilink")[蒙大拿州的一個縣](../Page/蒙大拿州.md "wikilink")，位於該州的地理中心，面積11,267平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口11,586人。本縣的治所為[劉易斯敦](../Page/劉易斯敦_\(蒙大拿州\).md "wikilink")。

弗格斯縣成立於1885年，縣名紀念早期殖民者安德魯·弗格斯的父親。\[1\]

## 地理

[Lewistown_MT_Fergus_County_Courthouse.jpg](https://zh.wikipedia.org/wiki/File:Lewistown_MT_Fergus_County_Courthouse.jpg "fig:Lewistown_MT_Fergus_County_Courthouse.jpg")的弗格斯縣法院\]\]
[Missouri_River_breaks.jpg](https://zh.wikipedia.org/wiki/File:Missouri_River_breaks.jpg "fig:Missouri_River_breaks.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，弗格斯縣
的總面積為，其中有，即99.74%為陸地；，即0.26%為水域。\[2\]本縣既是新墨西哥州面積第八大的縣份，亦是[美國面積第91大的縣份](../Page/美國面積最大縣份列表.md "wikilink")。

### 毗鄰縣

所有弗格斯縣的毗鄰縣皆為蒙大拿州的縣。

  - [舒托縣](../Page/舒托縣_\(蒙大拿州\).md "wikilink")：西北方
  - [布萊恩縣](../Page/布萊恩縣_\(蒙大拿州\).md "wikilink")：北方
  - [菲臘斯縣](../Page/菲臘斯縣_\(蒙大拿州\).md "wikilink")：東北方
  - [石油縣](../Page/石油縣_\(蒙大拿州\).md "wikilink")：東方
  - [馬瑟爾謝爾縣](../Page/馬瑟爾謝爾縣_\(蒙大拿州\).md "wikilink")：東南方
  - [戈爾登瓦利縣](../Page/戈爾登瓦利縣_\(蒙大拿州\).md "wikilink")：南方
  - [惠特蘭縣](../Page/惠特蘭縣_\(蒙大拿州\).md "wikilink")：西南方
  - [朱迪斯盆地縣](../Page/朱迪斯盆地縣_\(蒙大拿州\).md "wikilink")：西方

### 國家保護區

  - [查爾斯·羅素國家野生動物保護區](../Page/查爾斯·羅素國家野生動物保護區.md "wikilink")（部分）
  - [劉易斯和克拉克國家森林公園](../Page/劉易斯和克拉克國家森林公園.md "wikilink")（部分）
  - [上部密蘇里河斷層國家紀念區](../Page/上部密蘇里河斷層國家紀念區.md "wikilink")（部分）

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，弗格斯縣擁有11,893居民、4,860住戶和3,197家庭。\[3\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")3居民（每平方公里2居民）。\[4\]本縣擁有5,558間房屋单位，其密度為每平方英里1間（每平方公里0.5間）。\[5\]而人口是由97.1%[白人](../Page/歐裔美國人.md "wikilink")、0.08%[黑人](../Page/非裔美國人.md "wikilink")、1.18%[土著](../Page/美國土著.md "wikilink")、0.19%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.29%其他[種族和](../Page/種族.md "wikilink")1.16%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")0.81%。在97.1%白人中，有24.5%為[德國人](../Page/德國人.md "wikilink")、13%為[挪威人](../Page/挪威人.md "wikilink")，9.3%為
[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")、以及9%為[英國人](../Page/英國人.md "wikilink")。97.1%人口的母語為[英語](../Page/英語.md "wikilink")、1.2%為[德語以及](../Page/德語.md "wikilink")1.1%為[西班牙語](../Page/西班牙語.md "wikilink")。\[6\]

在4,860住户中，有28.7%擁有一個或以上的兒童（18歲以下）、56.1%為夫妻、6.7%為單親家庭、34.2%為非家庭、30.5%為獨居、13.9%住戶有同居長者。平均每戶有2.33人，而平均每個家庭則有2.91人。在11,893居民中，有24.5%為18歲以下、6.1%為18至24歲、23.6%為25至44歲、25.8%為45至64歲以及19.9%為65歲以上。人口的年齡中位數為42歲，女子對男子的性別比為100：94.8。成年人的性別比則為100：93.4。\[7\]

本縣的住戶收入中位數為$30,409，而家庭收入中位數則為$36,609。男性的收入中位數為$27,260，而女性的收入中位數則為$18,138，[人均收入為](../Page/人均收入.md "wikilink")$15,808。約10.6%家庭和15.4%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括19.4%兒童（18歲以下）及12.2%長者（65歲以上）。\[8\]

## 參考文獻

[F](../Category/蒙大拿州行政區劃.md "wikilink")

1.  [Welcome to Fergus County](http://www.co.fergus.mt.us/)

2.

3.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

4.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

5.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

6.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

7.  [American FactFinder](http://factfinder.census.gov/)

8.