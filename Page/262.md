**262**是[261與](../Page/261.md "wikilink")[263之間的](../Page/263.md "wikilink")[自然數](../Page/自然數.md "wikilink")。

## 在數學中

  - [迴文數](../Page/迴文數.md "wikilink")
  - [快樂數](../Page/快樂數.md "wikilink")
  - [無平方數因數的數](../Page/無平方數因數的數.md "wikilink")

## 在人類文化中

  - [Me 262戰鬥機](../Page/Me_262戰鬥機.md "wikilink")
  - [猛獅NL262](../Page/猛獅NL262.md "wikilink")，[德國](../Page/德國.md "wikilink")[猛獅出產的巴士車款](../Page/猛獅.md "wikilink")
  - [宜蘭縣](../Page/宜蘭縣.md "wikilink")[礁溪鄉的](../Page/礁溪鄉.md "wikilink")[郵遞區號為](../Page/郵遞區號.md "wikilink")262
  - [留尼旺的](../Page/留尼旺.md "wikilink")[國際電話區號為](../Page/國際電話區號.md "wikilink")262
  - [威斯康辛州的部分地區北美洲](../Page/威斯康辛州.md "wikilink")[電話區號為](../Page/電話區號.md "wikilink")262
  - [瑪歌酒庄是位於](../Page/瑪歌酒庄.md "wikilink")[法國](../Page/法國.md "wikilink")[梅多克](../Page/梅多克.md "wikilink")（Médoc）地區，佔地達262[公頃的](../Page/公頃.md "wikilink")[葡萄酒](../Page/葡萄酒.md "wikilink")[莊園](../Page/莊園.md "wikilink")
  - 《[金匱要略](../Page/金匱要略.md "wikilink")》載方262首
  - 《[基地邊緣](../Page/基地邊緣.md "wikilink")》是[以撒·艾西莫夫在](../Page/以撒·艾西莫夫.md "wikilink")44年的寫作生涯中，寫了262本書之後，首次進入[紐約時報暢銷書排行榜的作品](../Page/紐約時報.md "wikilink")
  - [錦衣衛至](../Page/錦衣衛.md "wikilink")[李自成](../Page/李自成.md "wikilink")[起義滅明後](../Page/起義.md "wikilink")，正式結束長達262年的歷史
  - [约翰逊县
    (肯塔基州)的陸地面積為](../Page/约翰逊县_\(肯塔基州\).md "wikilink")264[平方英里](../Page/平方英里.md "wikilink")
  - 《[天才總動員](../Page/天才總動員.md "wikilink")》於[1991年](../Page/1991年.md "wikilink")[7月1日起](../Page/7月1日.md "wikilink")，至[1992年](../Page/1992年.md "wikilink")[6月30日止](../Page/6月30日.md "wikilink")，共262集
  - 《[摩訶般若波羅蜜多心經](../Page/摩訶般若波羅蜜多心經.md "wikilink")》日本通行的漢譯本共262字

## 在科學中

  - [鐒](../Page/鐒.md "wikilink")-262是鐒中最穩定的[同位素](../Page/同位素.md "wikilink")，它的[半衰期有](../Page/半衰期.md "wikilink")216分鐘

## 在天文中

  - [NGC 262](../Page/NGC_262.md "wikilink")
    是[仙女座的一個](../Page/仙女座.md "wikilink")[星系](../Page/星系.md "wikilink")
  - [天琴座RR](../Page/天琴座RR.md "wikilink")
    是在[天琴座的一顆](../Page/天琴座.md "wikilink")[變星](../Page/變星.md "wikilink")，距離為262[秒差距](../Page/秒差距.md "wikilink")

## 在其他領域中

  - 西元[262年和](../Page/262年.md "wikilink")[前262年](../Page/前262年.md "wikilink")
  - [H.262是](../Page/H.262.md "wikilink")[ITU-T的一個](../Page/ITU-T.md "wikilink")[數位影像編碼標準](../Page/數位影像.md "wikilink")，屬於[視訊編解碼器](../Page/視訊編解碼器.md "wikilink")
  - [極地衛星運載火箭的助推器](../Page/極地衛星運載火箭.md "wikilink")[比沖為](../Page/比沖.md "wikilink")262秒
  - [地球同步衛星運載火箭的助推器比沖為](../Page/地球同步衛星運載火箭.md "wikilink")262秒
  - [音符C的近似頻率為](../Page/音符.md "wikilink")262[Hz](../Page/Hz.md "wikilink")

[Category:整數](../Category/整數.md "wikilink")