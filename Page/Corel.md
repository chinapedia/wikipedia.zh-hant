**科-{}-立爾數位科技**，英文名为**Corel**，中国大陆名为**科亿尔数码科技**（），是一間[電腦](../Page/電腦.md "wikilink")[軟件公司](../Page/軟件.md "wikilink")，總部在[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")[渥太華](../Page/渥太華.md "wikilink")。主要产品包括[CorelDRAW](../Page/CorelDRAW.md "wikilink")、[绘画](../Page/绘画.md "wikilink")、[会声会影](../Page/会声会影.md "wikilink")、[PaintShop
Photo](../Page/Corel_PaintShop_Pro.md "wikilink")、[WinZip和](../Page/WinZip.md "wikilink")[WordPerfect等](../Page/WordPerfect.md "wikilink")。

## 历史

1985年由創辦，早期原希望成為研究實驗室。Corel是"**Co**wpland **Re**search
**L**aboratory"的簡稱。1990年代以[CorelDRAW得到很大的成功](../Page/CorelDRAW.md "wikilink")，並曾經是加拿大最大的軟件公司。1996年收購[WordPerfect文件編輯器與](../Page/WordPerfect.md "wikilink")[微软對抗](../Page/微软.md "wikilink")，但計劃失敗，最終需要大幅[裁員](../Page/裁員.md "wikilink")。

2001年年底收购涉足图形图像领域。

2003年首次在[纳斯达克和](../Page/纳斯达克.md "wikilink")[多伦多证券交易所上市](../Page/多伦多证券交易所.md "wikilink")。

2004年收购，将[PaintShop Pro收入囊中](../Page/Corel_PaintShop_Pro.md "wikilink")。

2006年再次在[纳斯达克上市](../Page/纳斯达克.md "wikilink")，并收购压缩软件开发商[WinZip](../Page/WinZip.md "wikilink")，斥资1.96億美元收购多媒体软件巨头，完成了英特维和[友立资讯的合并](../Page/友立资讯.md "wikilink")。

## 產品

  - 图像處理類：
      - [CorelDRAW](../Page/CorelDRAW.md "wikilink")/[CorelDRAW](../Page/CorelDRAW.md "wikilink")（[向量繪圖軟件](../Page/向量圖形.md "wikilink")）
      - [Corel DESIGNER](../Page/Corel_DESIGNER.md "wikilink")/[Corel
        DESIGNER Technical
        Suite](../Page/Corel_DESIGNER.md "wikilink")（[工业绘图軟件](../Page/工业绘图.md "wikilink")）
      - [Corel Painter](../Page/Corel_Painter.md "wikilink")/[Corel
        Painter](../Page/Corel_Painter.md "wikilink")/[Corel
        Painter](../Page/Corel_Painter.md "wikilink")
      - [Corel PaintShop
        Pro](../Page/Corel_PaintShop_Photo.md "wikilink")（原[Corel
        PaintShop Pro](../Page/Corel_PaintShop_Pro.md "wikilink")）
      - [Corel PaintShop Photo
        Express](../Page/Corel_PaintShop_Photo.md "wikilink")
      - [Corel Paint it\!](../Page/Corel_Paint_it!.md "wikilink")
      - [CorelCAD](../Page/CorelCAD.md "wikilink")

<!-- end list -->

  - 多媒体類：
      - [Corel Digital
        Studio](../Page/Corel_Digital_Studio.md "wikilink")（中文名稱：[影音寶典](../Page/影音寶典.md "wikilink")，原）

      - [Corel PaintShop Photo
        Express](../Page/Corel_PaintShop_Photo.md "wikilink")

      - [會聲會影](../Page/會聲會影.md "wikilink")（中文名稱：[會聲會影](../Page/會聲會影.md "wikilink")）

      - （中文名稱：[DVD錄錄燒](../Page/DVD錄錄燒.md "wikilink")）

      - [WinDVD](../Page/WinDVD.md "wikilink")

      - [Corel DVD
        Factory](../Page/Corel_DVD_Factory.md "wikilink")（原[Corel
        DVD Copy](../Page/DVD_Copy.md "wikilink")）

      - [Corel MotionStudio
        3D](../Page/Corel_MotionStudio_3D.md "wikilink")

<!-- end list -->

  - 办公類：
      - [WordPerfect](../Page/WordPerfect.md "wikilink")/[WordPerfect](../Page/WordPerfect.md "wikilink")
      - [WinZip](../Page/WinZip.md "wikilink")
      - [WinZip Courier](../Page/WinZip.md "wikilink")
      - [iGrafx](../Page/iGrafx.md "wikilink")
      - [Corel PDF Fusion](../Page/Corel_PDF_Fusion.md "wikilink")
      - [Corel Perfect
        Authority](../Page/Corel_Perfect_Authority.md "wikilink")

## 參考資料

## 外部連結

  - [Corel Corporation](http://www.corel.com/)
  - [科亿尔数码科技](http://www.corel.com.cn/)（中国大陆）
  - [科立爾數位科技](http://www.corel.com.tw/)（台湾）
  - [科立爾台灣分公司
    裁員50人](https://web.archive.org/web/20080918082114/http://www.libertytimes.com.tw/2008/new/sep/15/today-e7.htm)

[Category:加拿大公司](../Category/加拿大公司.md "wikilink")
[Category:跨国软件公司](../Category/跨国软件公司.md "wikilink")
[Category:渥太华经济](../Category/渥太华经济.md "wikilink")