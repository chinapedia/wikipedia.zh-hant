**芬纳恩夫人阿斯特丽公主**（，），[挪威公主](../Page/挪威.md "wikilink")，是挪威王储奥拉夫（后来的挪威国王[奥拉夫五世](../Page/奥拉夫五世.md "wikilink")）和挪威王储妃[玛塔公主的第二个女儿](../Page/玛塔公主_\(瑞典\).md "wikilink")。

## 生平

从她的母亲1954年去世到她父亲1968年8月再婚之间，阿斯特丽公主一直担任挪威的第一夫人。1961年1月12日，公主和（1927年7月22 -
）[奥斯陆郊外的阿斯克教堂结婚](../Page/奥斯陆.md "wikilink")。他们育有五个孩子：

  - 凯瑟琳·芬纳恩（Cathrine Ferner，1962年出生）
  - 本尼迪克特·芬纳恩（Benedikte Ferner，1963年出生）
  - 亚历山大·芬纳恩（Alexander Ferner，1965年出生）
  - 伊丽莎白·芬纳恩（Elisabeth Ferner，1969年出生）
  - 卡尔-克里斯蒂安·芬纳恩（Carl-Christian Ferber，1972年出生）

## 祖先

<center>

</center>

[A](../Category/挪威公主.md "wikilink")
[A](../Category/格呂克斯堡王朝.md "wikilink")
[A](../Category/挪威君主女儿.md "wikilink")