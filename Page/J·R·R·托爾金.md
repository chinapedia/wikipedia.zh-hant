**約翰·羅納德·魯埃爾·托爾金**，[CBE](../Page/CBE.md "wikilink")（，常縮寫為，），[英國](../Page/英國人.md "wikilink")[作家](../Page/英國文學.md "wikilink")、[詩人](../Page/詩.md "wikilink")、[語言學家及大學教授](../Page/語言學.md "wikilink")，以創作經典[古典奇幻作品](../Page/古典奇幻.md "wikilink")《[霍比特人](../Page/霍比特人.md "wikilink")》、《[魔戒](../Page/魔戒.md "wikilink")》與《[精灵宝钻](../Page/精灵宝钻.md "wikilink")》而聞名於世。

托爾金曾於1925年至1945年間，在[牛津大學](../Page/牛津大學.md "wikilink")[彭布罗克学院擔任](../Page/牛津大学彭布罗克学院.md "wikilink")[盎格魯-撒克遜語的](../Page/古英語.md "wikilink")，並於1945年至1959年間轉至[牛津大學默頓學院任](../Page/牛津大學默頓學院.md "wikilink")[英國語言與](../Page/英語.md "wikilink")[文學的](../Page/英國文學.md "wikilink")[默顿教授](../Page/默顿教授.md "wikilink")\[1\]。他是[C·S·路易斯的密友](../Page/C·S·路易斯.md "wikilink")——兩人皆為非正式文學讨论社「[迹象](../Page/迹象文学社.md "wikilink")」的社員。1972年3月28日，托爾金獲英國女王[伊莉莎白二世頒授的](../Page/伊莉莎白二世.md "wikilink")[大英帝國司令勳章](../Page/大英帝國勳章.md "wikilink")。

托尔金的儿子[克里斯托夫在父亲去世后](../Page/克里斯托夫·托爾金.md "wikilink")，整理了大量的笔记和未发表的手稿，出版了一系列作品，包括《[精灵宝钻](../Page/精灵宝钻.md "wikilink")》、《[未完成的故事](../Page/未完成的故事.md "wikilink")》、《》等。这些作品与《[霍比特人](../Page/霍比特人.md "wikilink")》和《[魔戒](../Page/魔戒.md "wikilink")》一起构成了有联系的共同体，由传说、诗歌、虚构历史、人造语言和短文所组成，关于一个名叫[阿尔达](../Page/阿尔达.md "wikilink")（及其中的[中土大陆](../Page/中土大陆.md "wikilink")）的奇幻世界。1951至1955年间，托尔金用“[传说故事集](../Page/托爾金的傳說故事集.md "wikilink")”（）一词来称呼大部分作品。\[2\]尽管在他之前有很多其他作者的奇幻作品问世\[3\]，但《[霍比特人](../Page/霍比特人.md "wikilink")》和《[魔戒](../Page/魔戒.md "wikilink")》的巨大成功造成了该文学类型的流行与复兴。因此托尔金被大众公认为“现代奇幻文学之父”\[4\]\[5\]——或更准确地说，是对于[古典奇幻](../Page/古典奇幻.md "wikilink")。\[6\]

2008年，《[泰晤士报](../Page/泰晤士报.md "wikilink")》把他列为“1945年后50位最伟大的英国作家”榜单上的第6名。\[7\]
2009年，他在《[福布斯](../Page/福布斯.md "wikilink")》收入最高的“已故名人”榜单中列第5名。\[8\]

## 生平

### 家族起源

托尔金父系家族的祖先大部分是工匠，原居于[德国的](../Page/德国.md "wikilink")[下萨克森](../Page/下萨克森.md "wikilink")。18世纪後移居到英格兰，很快地變得非常英國化。\[9\]托尔金（Tolkien）这个姓氏可能係自德语词*Tollkiehn*，意为“愚勇」。\[10\]一些以托尔金或其变种为姓氏的家族现今居住在[德国西北部](../Page/德国.md "wikilink")，主要分佈在[下萨克森州和](../Page/下萨克森州.md "wikilink")[汉堡市](../Page/汉堡市.md "wikilink")。\[11\]\[12\]有一位德国作者提出这个名字更有可能是源自於[东普鲁士](../Page/东普鲁士.md "wikilink")[肯琴的村庄](../Page/肯琴.md "wikilink")“Tołkiny”（现位于[波兰东北部](../Page/波兰.md "wikilink")），虽然那个村庄离下萨克森很远；它的名称源于现已灭绝的[普魯士語](../Page/普魯士語.md "wikilink")。\[13\]\[14\]

托尔金的外公外婆萨菲尔德（*Suffield*）夫妇居住在[伯明翰](../Page/伯明翰.md "wikilink")，并在市中心拥有一间商店。萨菲尔德家族从1812年起在一座名叫Lamb
House的房子中開始经营书籍、文具、衣袜等生意。

### 童年

[Mabel_Suffield_Christmas_Card.jpg](https://zh.wikipedia.org/wiki/File:Mabel_Suffield_Christmas_Card.jpg "fig:Mabel_Suffield_Christmas_Card.jpg")的彩照，寄往[英格兰](../Page/英格兰.md "wikilink")[伯明翰的亲戚](../Page/伯明翰.md "wikilink")。\]\]
托尔金于1892年1月3日出生于现[南非](../Page/南非.md "wikilink")[奥兰治自由邦的](../Page/奥兰治自由邦.md "wikilink")[布隆方丹](../Page/布隆方丹.md "wikilink")，父亲亚瑟·鲁埃尔·托尔金（Arthur
Reuel Tolkien, 1857-1896）是一位银行经理，母亲梅布尔，原姓萨菲尔德（Mabel, née Suffield,
1870-1904）。亚瑟升职为布隆方丹分部银行的主管，这对夫妇才离开了[英格兰](../Page/英格兰.md "wikilink")。托尔金有一个弟弟希拉里·亚瑟·鲁埃尔·托尔金（Hilary
Arthur Reuel Tolkien），出生于1894年2月17日。\[15\]

童年时期，他在花园被一只大咬伤，某些人认为此事在他的故事中必然有所对应。但是托尔金成年后宣称自己对此事毫无记忆，也对蜘蛛没有特别的憎恶。還有一件托尔金絕對不會知道的事，當托尔金還是婴儿時，有一位年轻的家仆觉得托尔金長得非常个漂亮，就把他带到自己的驻地炫耀，直到第二天早晨才把他还回来。\[16\]

3岁时，母亲帶著托尔金跟弟弟回[英格兰探亲](../Page/英格兰.md "wikilink")，這也是一次漫长旅程，托尔金的父亲原本打算在其母子走后才动身，但是却在离开前死于风湿热。\[17\]頓時家庭没有了收入，托尔金的母亲只好带着他們到[伯明翰](../Page/伯明翰.md "wikilink")，和外祖父母住在一起。不久后，在1896年，他们再搬家到，当时這只是一个[伍斯特郡的小村庄](../Page/伍斯特郡.md "wikilink")，後來才被[伯明翰兼并](../Page/伯明翰.md "wikilink")。\[18\]托尔金非常喜欢探索、和周边山丘群。这些地点和伍斯特郡的小镇村庄，以及他阿姨简的农场袋底洞，后来都成为他书中场景的灵感来源，而袋底洞这一名字也被用在他的作品中。\[19\]

[BirminghamOratoryDome.jpg](https://zh.wikipedia.org/wiki/File:BirminghamOratoryDome.jpg "fig:BirminghamOratoryDome.jpg")
母亲梅布尔·托尔金親自教育两个孩子，小托尔金從小在家中就有好学的名声。\[20\]母亲教了他很多[植物学知识](../Page/植物学.md "wikilink")，启发了他对于植物的审美情趣。小托尔金喜爱绘画风景和树木，但他最喜欢的课程却与语言息息相关，母亲很早就教了他[拉丁语基础](../Page/拉丁语.md "wikilink")。\[21\]

托尔金四岁起就学会了阅读，而且很快能熟练书写，母亲也允许他看很多书。但是托尔金不喜欢《[金银岛](../Page/金银岛.md "wikilink")》和《[花衣魔笛手](../Page/花衣魔笛手.md "wikilink")》，他认为[路易斯·卡罗的](../Page/路易斯·卡罗.md "wikilink")《[爱丽丝梦游仙境](../Page/爱丽丝梦游仙境.md "wikilink")》“有趣但令人不安”。他喜欢印第安人的故事和[乔治·麦克唐纳的奇幻作品](../Page/喬治·麥克唐納.md "wikilink")。\[22\]另外，[安德鲁·朗格的童话书系列对他而言尤为重要](../Page/安德鲁·朗格.md "wikilink")，并且在他日后的一些作品中产生了明显的影响。\[23\]

[KES_Free_Grammar_School_Charles_Barry.jpg](https://zh.wikipedia.org/wiki/File:KES_Free_Grammar_School_Charles_Barry.jpg "fig:KES_Free_Grammar_School_Charles_Barry.jpg")的爱德华国王学校，托尔金曾經在這裡就讀。(1900–1902,
1903–1911)\[24\]\]\]
尽管属于[浸礼教的家人强烈反对](../Page/浸礼教.md "wikilink")，托尔金的母親梅布尔·托尔金于1900年皈依了[罗马天主教](../Page/罗马天主教.md "wikilink")，家人遂中斷對她們的经济支援。\[25\]
1904年，托尔金12岁时，他的母亲在租的小屋中因急性[糖尿病去世](../Page/糖尿病.md "wikilink")。她死时大约34岁，与[1型糖尿病病人在不治疗的情况下能存活的寿命大致吻合](../Page/1型糖尿病.md "wikilink")——醫學上的[胰岛素則遲至二十多年后才被发现](../Page/胰岛素.md "wikilink")。梅布尔·萨菲尔德·托尔金在[伍斯特郡的圣彼得罗马天主教堂火葬](../Page/伍斯特郡.md "wikilink")。\[26\]九年后，托尔金写道：“我自己亲爱的母亲是一个真正的殉道者，不是每个人都像（弟弟）希拉里和我一样收到上帝给予的伟大礼物，祂给了我们一个因劳累与困难而死来确保我们维持信仰的母亲。”\[27\]

梅布尔在死前把两个儿子的监护权委托给好友伯明翰一所教堂（Birmingham
Oratory）的神父，期許神父教育他們成为好的天主教徒，並抚养他们长大成人。在1965年给儿子迈克尔的一封信中，托尔金回忆监护人给他的影响：“摩根是一位威尔士-西班牙裔的保守党人，对一些人而言他似乎是闲混的爱说长道短的老头。他是，也不是。我从他那第一次学到博爱和宽恕，这些的光亮甚至穿透了我曾经处于的‘自由主义’的黑暗，我曾对‘[血腥瑪莉](../Page/血腥瑪莉.md "wikilink")’的了解比对[耶稣之母玛丽还多](../Page/玛利亚_\(耶稣母亲\).md "wikilink")，后者除了作为罗马教徒的邪恶崇拜对象外从没被提及过。”\[28\]

母亲死后，托尔金在伯明翰的地区长大，就学于，以及之后的。1903年，他赢得了一份奖学金后回到了爱德华国王学校。這期间，托尔金成为学校[军官训练团](../Page/军官训练团.md "wikilink")(OTC)的一员，在1910年[乔治五世](../Page/乔治五世_\(英国\).md "wikilink")[加冕游行中帮助调整游行队伍](../Page/加冕.md "wikilink")。托尔金的位置被安排在[白金汉宫大门前](../Page/白金汉宫.md "wikilink")。\[29\]

在艾吉巴斯顿期間，托尔金住在[佩羅特的荒謬高塔和](../Page/佩羅特的荒謬高塔.md "wikilink")[维多利亚时期的](../Page/维多利亚时期.md "wikilink")[艾吉巴斯頓水廠附近](../Page/艾吉巴斯頓水廠.md "wikilink")，讀者們推測，也许這個經驗影响了他作品中黑暗双塔的想象。\[30\]\[31\]另外一个重要影响是[爱德华·伯纳-琼斯和](../Page/爱德华·伯纳-琼斯.md "wikilink")[前拉斐尔派的画作](../Page/前拉斐尔派.md "wikilink")，[伯明翰博物館和美術館有大量藏品向公众展出](../Page/伯明翰博物館和美術館.md "wikilink")。\[32\]

### 青年

早年，托尔金第一次遇到一种[人工语言Animalic语](../Page/人工语言.md "wikilink")，由他的表亲玛丽和马乔丽·英克莱顿发明。那时，他在学习[拉丁语和](../Page/拉丁语.md "wikilink")[盎格魯-撒克遜語](../Page/盎格魯-撒克遜語.md "wikilink")。对这一语言的兴趣很快褪去了，但玛丽和其他人，包括托尔金自己，发明了一种更复杂的语言叫做Nevbosh语。接着他一个人创造了Naffarin语。\[33\]\[34\]

1911年，在爱德华国王学校就学时，托尔金和三位朋友，罗伯·吉尔森，乔弗里·巴彻·史密斯和克里斯托弗·维斯曼创建了一个半秘密社团名叫T.C.B.S。名字是“茶俱乐部和巴罗社团”（Tea
Club and Barrovian
Society）的首字母缩写，暗指他们喜欢在学校附近的巴罗商店以及偷偷在校图书馆喝茶。\[35\]\[36\]
毕业后，社团成员仍然保持联系。1914年12月，他们在伦敦维斯曼的家中举行了一次“参议会”，这次聚会让托尔金献身于诗歌创作。

1911年，英格兰和威尔士的人口普查显示托尔金（职业：学生）与他的弟弟希拉里（职业：五金商人的店员）居住在艾吉巴斯顿的海菲尔德4号。\[37\]

同年，托尔金到[瑞士度过暑假](../Page/瑞士.md "wikilink")，他在1968年的一封信中生动回忆了这次旅行\[38\]，写道[比尔博跨越](../Page/比爾博·巴金斯.md "wikilink")[迷雾山脉的旅途是直接基于他们一行](../Page/迷雾山脉.md "wikilink")12人从[因特拉肯徒步到](../Page/因特拉肯.md "wikilink")[卢达本纳](../Page/卢达本纳.md "wikilink")，最终到[米伦外](../Page/米伦_\(瑞士\).md "wikilink")[冰碛上营地的冒险](../Page/冰碛.md "wikilink")。他们通过“”到[格林德瓦镇](../Page/格林德瓦.md "wikilink")，然后走过到达[迈林根](../Page/迈林根.md "wikilink")。他们接着走过，通过[瓦莱州北部到](../Page/瓦莱州.md "wikilink")[布里格](../Page/布里格.md "wikilink")，到[阿莱奇冰川和](../Page/阿莱奇冰川.md "wikilink")[采尔马特](../Page/采尔马特.md "wikilink")。\[39\]

同年10月，托尔金开始在[牛津大学埃克塞特学院的学业](../Page/牛津大学埃克塞特学院.md "wikilink")。他一开始学习[古典学但在](../Page/西洋古典學.md "wikilink")1913年转修英语语言和文学，并于1915年以一等荣誉毕业。\[40\]

### 恋爱和婚姻

16岁的托尔金和弟弟搬进宿舍时，初遇比他年长三岁的伊迪丝·玛丽·布拉特。据[汉弗莱·卡彭特的记叙](../Page/汉弗莱·卡彭特.md "wikilink")：

他的监护人摩根神父，认为伊迪丝分散了托尔金对学业的注意力，还惊恐于他与一个[新教徒女孩认真交往](../Page/新教徒.md "wikilink")，因此禁止他见她，和她说话，甚至禁止和她写信，直到他21岁。\[41\]他早年违反了一次，导致摩根神父威胁如果再犯就中断他的大学生涯，此后他严格地服从了这个禁令。\[42\]

在他21岁生日的夜晚，托尔金写信给了伊迪丝。他表白了自己的爱情并且向她求婚。伊迪丝回信说她已经答应了另一个男人的求婚，不过那是因为她以为托尔金已经忘了她。两人在一个铁路桥下见面重续旧情。伊迪丝归还了订婚戒指，声明她会与托尔金结婚。\[43\]
他们订婚后，伊迪丝不情愿地在托尔金的坚持下宣布自己皈依罗马天主教。她的房东，一位坚定的新教徒，对此暴怒并驱逐她，令她一找到其他住处就离开。\[44\]
1913年1月，伊迪丝和罗纳德在伯明翰正式订婚，然后于1916年3月22日在圣贞女玛丽罗马天主教堂结婚。\[45\]

### 一战

1914年，英国加入[第一次世界大战](../Page/第一次世界大战.md "wikilink")。托尔金的亲戚非常震惊于他不马上志愿加入[英国陆军的决定](../Page/英国陆军.md "wikilink")。相反，托尔金开始了一个学业项目，直到1915年取得学位后才入伍。他被委任为[少尉](../Page/少尉.md "wikilink")。\[46\]他和第13预备营在[斯塔福德郡接受了](../Page/斯塔福德郡.md "wikilink")11个月的训练。在一封给伊迪丝的信中，托尔金抱怨道：“上级长官中很难看到绅士，说实在的，他们甚至少有人性。”\[47\]托尔金之后转到第11服务营，随[英国远征军于](../Page/英国远征军.md "wikilink")1916年6月4日到达[法国](../Page/法国.md "wikilink")。在运兵船上离开英格兰的经历启发他写了一首诗《孤独的岛屿》（The
Lonely Isle）。\[48\] 他后来写道：“下级军官被杀光了，每分钟都有将士横死沙场。离开我的妻子……像是一次死亡。”\[49\]

训练为通讯员后，托尔金到达[索姆河](../Page/索姆河战役.md "wikilink")。在战线后的[布赞库尔](../Page/布赞库尔.md "wikilink")，托尔金参与了对和莱比锡突前的进攻。根据的英国[圣公会本堂神父](../Page/圣公会.md "wikilink")，尊敬的梅尔文·S·艾夫斯的回忆录：

托尔金在军中的时间对艾迪丝是可怕的压力，她害怕每一次敲门可能带来丈夫死去的消息。为了避开[英国陆军的信件审查](../Page/英国陆军.md "wikilink")，夫妇俩发展了一种密码，用于托尔金的家信。由此艾迪丝能够在[西线的地图上跟踪到丈夫的动向](../Page/西方战线_\(第一次世界大战\).md "wikilink")。

1916年10月27日，他的营进攻时，托尔金患上[戰壕熱](../Page/戰壕熱.md "wikilink")，一种流行于战壕，由[虱子传染的疾病](../Page/虱子.md "wikilink")。1916年11月8日，托尔金负伤回到英格兰。\[50\]
很多他学生时代的密友都战死了。其中包括T.C.B.S的社员罗伯·吉尔森，死于索姆河战役首日的[博蒙阿梅尔进攻](../Page/博蒙阿梅尔.md "wikilink")。另一社员乔弗里·史密斯死于同一场战役，一枚德军炮弹击中了急救站。托尔金回到英格兰后他所在的营几乎全军覆没。

因为遭受健康问题几次被迫从战斗中撤离，不然托尔金也很可能已经战死。\[51\]

根据约翰·加斯的叙述：

在以后的岁月中，托尔金愤慨地表明那些在他作品中寻找和二战相照应的部分的人完全弄错了：

  - 大后方

病弱消瘦的托尔金在医院和守备部队中度过了剩余的战争时期，他的健康状况被认定为不适合上战场。\[52\]\[53\]

在[斯塔福德郡](../Page/斯塔福德郡.md "wikilink")的养病期间，托尔金开始写作《[失落的故事集](../Page/失落的故事集.md "wikilink")》(The
Book of Lost Tales)，由《[贡多林的陷落](../Page/贡多林.md "wikilink")》(Fall of
Gondolin)开始。1917到1918年，他的病情反复发作，但他已康复到可以在各个军营作后方服役，并被提拔为中尉。就在此时，伊迪丝怀了他们的第一个孩子——约翰·弗兰西斯·鲁埃尔·托尔金。

当他被派驻在[赫尔河畔金斯顿时](../Page/赫尔河畔金斯顿.md "wikilink")，他和伊迪丝在附近[罗斯的树林中散步](../Page/罗斯.md "wikilink")，伊迪丝开始在开花的铁杉林中的空地上为他跳舞。在妻子于1971年死后，托尔金回忆道：

这个事件是恋人[贝伦和](../Page/贝伦.md "wikilink")[露西安相遇情节的灵感来源](../Page/露西安.md "wikilink")。\[54\]

### 学术和写作生涯

[2_Darnley_Road,_the_former_home_of_J.R.R._Tolkien_in_West_Park,_Leeds.jpg](https://zh.wikipedia.org/wiki/File:2_Darnley_Road,_the_former_home_of_J.R.R._Tolkien_in_West_Park,_Leeds.jpg "fig:2_Darnley_Road,_the_former_home_of_J.R.R._Tolkien_in_West_Park,_Leeds.jpg")
[20_Northmoor_Road,_Oxford.JPG](https://zh.wikipedia.org/wiki/File:20_Northmoor_Road,_Oxford.JPG "fig:20_Northmoor_Road,_Oxford.JPG")[20号](../Page/北沼路20號.md "wikilink")，托尔金在北牛津的故居。\]\]
一战后，托尔金的第一份平民工作是编纂《[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")》，他主要负责W开头的源自日耳曼语词汇的历史和语源学。\[55\]
1920年，他接下[利兹大学英语语言的教职](../Page/利兹大学.md "wikilink")（头衔为Reader），并成为那里最年轻的教授。\[56\]在利兹期间，他制作了《中古英语词汇表》，与[E·V·戈登合作完成了](../Page/E·V·戈登.md "wikilink")《[高文爵士与绿骑士](../Page/高文爵士与绿骑士.md "wikilink")》定本，两本书都成为之后几十年的学术标准。他还翻译了《高文爵士》、诗歌《[珍珠](../Page/珍珠.md "wikilink")》和叙事诗《》。1925年，他作为中古英语的劳林森与博斯沃思教授回到[牛津大学](../Page/牛津大学.md "wikilink")，在[彭布罗克学院任职](../Page/牛津大学彭布罗克学院.md "wikilink")。

在[彭布罗克学院期间](../Page/牛津大学彭布罗克学院.md "wikilink")，托尔金创作了《[霍比特人](../Page/霍比特人.md "wikilink")》和《[魔戒](../Page/魔戒.md "wikilink")》的前二卷，同时居住在的[诺斯摩尔路](../Page/北沼路.md "wikilink")[20号](../Page/北沼路20號.md "wikilink")（2002年，一个[藍色牌匾被安置在那里以作纪念](../Page/藍色牌匾.md "wikilink")）。在1928年在[格洛斯特郡](../Page/格洛斯特郡.md "wikilink")出土了一座崇拜该神的罗马神庙之后，他在1931年发表了一篇关于凯尔特神祇“[诺登斯](../Page/诺登斯.md "wikilink")”名字的语言学随笔。\[57\]

1933年秋季學期，由牛津大學一些志同道合的學者、教授組成的文學社團「[迹象文学社](../Page/迹象文学社.md "wikilink")」正式成立，托爾金和[C·S·路易斯皆是其中的成員之一](../Page/C·S·路易斯.md "wikilink")\[58\]，他都會參與迹象文学社的定期聚會，週一上午在[老鷹與小孩酒吧](../Page/老鷹與小孩.md "wikilink")、週四下午則在學校宿舍裡碰面\[59\]，在聚會時各成員之間互相討論北歐的神話和史詩，以及各人近來的作品\[60\]。包括托爾金的《哈比人歷險記》與《魔戒》的一些章節就曾在迹象文学社聚會上被朗讀\[61\]。

托爾金亦與C·S·路易斯成為至交好友，他曾這樣說：「同路易斯的友誼給了我很多東西，除了不斷的歡樂和安慰之外，我還從這個誠實、勇敢、聰明的人身上獲益良多」\[62\]。他們在聚會閒聊時相互約定要各自撰寫一個給兒童看的文學作品，托爾金預定寫的[時間題材](../Page/時間.md "wikilink")、而路易斯則是[空間題材](../Page/空間.md "wikilink")。寫作過程中路易斯一直扮演著激勵與催促托爾金的角色，並提供意見，最終路易斯寫出了《[納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")》系列，而托爾金則是《魔戒》\[63\]\[64\]。不過在創作《魔戒》的過程中，兩人的關係逐漸疏離，直至1957年路易斯同[海倫·喬依·戴維德曼成婚後](../Page/海倫·喬依·戴維德曼.md "wikilink")，托爾金就較少與其來往\[65\]。[1950年代期間](../Page/1950年代.md "wikilink")，托爾金亦慢慢淡出了迹象文学社的活動\[66\]。

#### 《贝奥武甫》

托尔金1936年的讲座“《[贝奥武甫](../Page/贝奥武甫.md "wikilink")》：怪兽和评论”对《[贝奥武甫](../Page/贝奥武甫.md "wikilink")》的研究有长久的影响。\[67\]里维斯·E·尼克尔森说托尔金写的关于《贝奥武甫》的文章是“被广泛承认为对该作品文学批评的转折点”，指出托尔金建立了该作品作为诗歌本质的优先权，相对于其纯语言学元素。\[68\]
在那时，学者们一致轻视《贝奥武甫》，因为其中讲述的是与怪兽的幼稚战斗，而不是现实中部落间的战争。托尔金主张《贝奥武甫》的作者是在笼统地表达人类命运的主题，不被特定的部落政治斗争所限制，因此怪兽们对此诗歌是必不可少的。\[69\]《贝奥武甫》中涉及具体部落斗争的地方，例如在，托尔金坚定地反对计入奇幻元素。\[70\]
在文章中，托尔金表示他对《贝奥武甫》的高看：“《贝奥武甫》是我认为价值最高的材料之一”，这一影响在他[中土世界的](../Page/中土世界.md "wikilink")[传说故事集中有所体现](../Page/托爾金的傳說故事集.md "wikilink")。\[71\]

根据[汉弗莱·卡彭特的记叙](../Page/汉弗莱·卡彭特.md "wikilink")，托尔金用巧妙的方式来开始他关于《贝奥武甫》的系列讲座：

几十年后，著名诗人[威斯坦·休·奥登写给他曾经的教授](../Page/威斯坦·休·奥登.md "wikilink")：

2003年，托尔金手写的《贝奥武甫》翻译和评论，达到大约2000页，在牛津大学[博德利圖書館的档案中被发现](../Page/博德利圖書館.md "wikilink")。\[72\]

#### 二战

[Merton_College_and_chapel_from_St_Marys.JPG](https://zh.wikipedia.org/wiki/File:Merton_College_and_chapel_from_St_Marys.JPG "fig:Merton_College_and_chapel_from_St_Marys.JPG")，托尔金在此担任英语语言及文学教授。
（1945–1959）\]\]

在第二次世界大戰爆發的前期，英國軍方在軍備階段，就指定托尔金为电码译员。\[73\]\[74\]
並於1939年1月，询问托尔金是否准备好在国家的非常时刻效力于外交部的[密码分析部门](../Page/密码分析.md "wikilink")。\[75\]\[76\]
托尔金當時的回应是肯定的，英國軍方遂於3月27日起，指派托尔金在政府编码与译码学校（GC\&CS）的伦敦部接受指导课程。\[77\]\[78\]然而，尽管他热心地\[79\]想成为电码译员，卻在十月被告知此时不需要他的服务。\[80\]\[81\]最终他从没有做过这个职务。\[82\]\[83\]
2009年，根據《[每日电讯报](../Page/每日电讯报.md "wikilink")》報導，托尔金當時回绝了一份年薪₤500的工作，出于未知原因想要成为全职候选人。\[84\]

1939年之後\[85\]，托尔金被編入了家園防衛隊，負責夜間巡邏等任務，因此他可以藉這個機會觀察星空；1940年某日晚間他發現北方突現一片火光，次日才知道那是[考文垂遭到](../Page/考文垂.md "wikilink")[納粹德軍](../Page/納粹德軍.md "wikilink")[的轟炸](../Page/考文垂大轟炸.md "wikilink")。

1942年期間，托尔金因為任務的關係，得以專注於觀察[太陽和](../Page/太陽.md "wikilink")[月亮升起](../Page/月亮.md "wikilink")、沉落時的壯麗景象，後來托尔金撰寫《魔戒》時，就把這些情節詳細地寫入書中。\[86\]

1945年，托尔金搬到[牛津大學默頓學院](../Page/牛津大學默頓學院.md "wikilink")，成为英语语言及文学的默顿教授\[87\]，並在此一直工作到1959年退休。他多年来担任[都柏林大学的客座考官](../Page/都柏林大学.md "wikilink")。1954年，[爱尔兰国立大学](../Page/爱尔兰国立大学.md "wikilink")（都柏林大学曾是其一部分）授予托尔金荣誉学位。托尔金于1948年完成了《魔戒》，已是开始动笔的近十年后。

托尔金还为《》翻译了《[約拿書](../Page/約拿書.md "wikilink")》，並於1966年出版。\[88\]

### 家庭

托尔金夫妇有四个孩子：

  - 约翰·弗兰西斯·鲁埃尔·托尔金 （1917年11月17日 – 2003年1月22日）
  - 迈克尔·希拉里·鲁埃尔·托尔金 （1920年10月22日 – 1984年2月27日）
  - [克里斯托夫·约翰·鲁埃尔·托尔金](../Page/克里斯托夫·托爾金.md "wikilink") （1924年11月21日 - ）
  - 普里西拉·玛丽·安妮·鲁埃尔·托尔金（1929年6月18日 - ）

托尔金为孩子们投入很多精力，孩子小的时候给他们寄带插图的来自[圣诞老人的信](../Page/圣诞老人.md "wikilink")（后集结出版为《[聖誕老爸的來信](../Page/聖誕老爸的來信.md "wikilink")》）。每年会加入新角色，比如[北极熊](../Page/北极熊.md "wikilink")（圣诞老人的助手），[雪人](../Page/雪人.md "wikilink")（他的园丁），精灵伊尔贝雷斯（他的秘书）等等。主要人物会讲述圣诞老人对骑[蝙蝠的](../Page/蝙蝠.md "wikilink")[哥布林的战役](../Page/哥布林.md "wikilink")，还有北极熊的各个[恶作剧故事](../Page/恶作剧.md "wikilink")。\[89\]

### 退休与晚年

[[牛津大学埃克塞特学院小教堂中的托尔金半身像](../Page/牛津大学埃克塞特学院.md "wikilink")。|thumb](https://zh.wikipedia.org/wiki/File:Oxford_Tolkien.JPG "fig:牛津大学埃克塞特学院小教堂中的托尔金半身像。|thumb")
1959年退休到1973年去世期間，托尔金的公众关注度和文学名气不断增长。1961年，他甚至被朋友C·S·路易斯提名[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")\[90\]，但終落選\[91\]。

托尔金作品的销售带来很大利润，以至于他后悔没有早点[退休](../Page/退休.md "wikilink")。\[92\]一开始，托尔金兴致很高地回复读者的提问，但他对1960年代的[反主流文化运动使他的书一下子流行感到非常不高兴](../Page/反主流文化.md "wikilink")。\[93\]在1972年的信中，他强烈反对自己成为了，但也承认“就算是最谦逊偶像的鼻子也不可能完全抵挡美妙熏香的味道！”\[94\]

书迷的狂热让托尔金不得不把自己的[电话号码从公开](../Page/电话号码.md "wikilink")[电话簿上去除](../Page/电话簿.md "wikilink")\[95\]，最终他和伊迪丝搬家到[伯恩茅斯](../Page/伯恩茅斯.md "wikilink")，一个服务于中上层阶级的海滨胜地。托尔金作为畅销书作家的身份给了他们轻松进入上流社会的可能，但他深深怀念“迹象文学社”的同伴们。相反，伊迪丝为能扮演社交界女主人的角色而欣喜若狂，这也是托尔金最初选择了伯恩茅斯的原因。

根据[汉弗莱·卡彭特的记叙](../Page/汉弗莱·卡彭特.md "wikilink")：

[Tolkien's_grave,_Wolvercote_Cemetery.jpg](https://zh.wikipedia.org/wiki/File:Tolkien's_grave,_Wolvercote_Cemetery.jpg "fig:Tolkien's_grave,_Wolvercote_Cemetery.jpg")。\]\]

伊迪丝·托尔金于1971年11月29日去世，享壽82岁。根据孙子[西蒙·托尔金的记叙](../Page/托尔金家族.md "wikilink")：

1972年，托尔金获英国女王[伊丽莎白二世颁授的](../Page/伊丽莎白二世.md "wikilink")[大英帝国司令勋章](../Page/大英帝国勋章.md "wikilink")。\[96\]
同年，[牛津大学授予他荣誉](../Page/牛津大学.md "wikilink")[文学博士学位](../Page/文学博士.md "wikilink")。\[97\]\[98\]

托尔金在伊迪丝位于牛津的墓碑上刻上“[露西安](../Page/露西安.md "wikilink")”的名字。21个月后，托尔金患上了嚴重的[胃潰瘍出血](../Page/胃潰瘍.md "wikilink")，導致胸部感染，而於1973年9月2日去世，享壽81岁。\[99\]\[100\]他与妻子合葬，名字旁刻有“[贝伦](../Page/贝伦.md "wikilink")”。墓碑显示如下：

在托尔金的[中土世界](../Page/中土世界.md "wikilink")[传说故事集中](../Page/托爾金的傳說故事集.md "wikilink")，[露西安是神的儿女中最美丽的那位](../Page/露西安.md "wikilink")，因爱上凡人武士[贝伦而放弃](../Page/贝伦.md "wikilink")[永生](../Page/永生.md "wikilink")。

## 观点

### 自然

[链接=<https://zh.wikipedia.org/wiki/File:Tolkien's_Favorite_Tree,_Oxford_Botanical_Garden.jpg>](https://zh.wikipedia.org/wiki/File:Tolkien's_Favorite_Tree,_Oxford_Botanical_Garden.jpg "fig:链接=https://zh.wikipedia.org/wiki/File:Tolkien's_Favorite_Tree,_Oxford_Botanical_Garden.jpg")
托爾金非常熱愛大自然，這一點可以從他的插圖和信件看出來。他特別喜愛[樹木](../Page/樹木.md "wikilink")，自認是樹木的捍衛者\[101\]，堅決反對對樹木的肆意破壞\[102\]\[103\]。托爾金去世前的最後一張照片，就是在[牛津大學植物園跟他](../Page/牛津大學植物園.md "wikilink")[最喜愛的一棵黑松樹合影的](../Page/托爾金樹.md "wikilink")\[104\]。對大自然的熱愛亦明顯地表現在托爾金的作品中，例如《精靈寶鑽》中照亮[維林諾的](../Page/維林諾.md "wikilink")[雙聖樹](../Page/雙聖樹.md "wikilink")，以及《魔戒》裡[樹人聯合起來去攻打破壞自然的](../Page/樹人.md "wikilink")[薩魯曼的情節](../Page/薩魯曼.md "wikilink")\[105\]。

此外托爾金也非常厭惡[工業化的痕跡入侵了鄉村](../Page/工業化.md "wikilink")\[106\]。托爾金的朋友喬治·塞爾在回憶他與托爾金的一次[度假時表示](../Page/度假.md "wikilink")：「當我們看到[工業污染的痕跡時](../Page/工業污染.md "wikilink")，他（托爾金）會談起[半獸人或半獸人族](../Page/半獸人.md "wikilink")」\[107\]。

## 文学作品

托尔金的一些构想在他的[传说故事集中连续出现](../Page/托爾金的傳說故事集.md "wikilink")，由《》开头，该作品是他在[索姆河战役中染疾后养病时所作](../Page/索姆河战役.md "wikilink")。

### 所受影响

#### 英国冒险故事

领导[藝術與工藝運動的](../Page/藝術與工藝運動.md "wikilink")[博学家](../Page/博学家.md "wikilink")[威廉·莫里斯是托尔金最大的影响来源之一](../Page/威廉·莫里斯.md "wikilink")。托尔金希望模仿他的散文和浪漫诗歌。\[108\]《[魔戒](../Page/魔戒.md "wikilink")》中的一些命名如[死亡沼泽](../Page/死亡沼泽.md "wikilink")\[109\]
和[幽暗密林](../Page/幽暗密林.md "wikilink")\[110\]，以及一些笼统的写作方式都来源于莫里斯的作品。

英国作者的儿童小说《》中有“桌子般高”的主人公，强烈地影响了《[霍比特人](../Page/霍比特人.md "wikilink")》中[比尔博所属种族的描述和设定](../Page/比爾博·巴金斯.md "wikilink")。\[111\]

在一次电话采访中，托尔金提到了[亨利·萊特·哈葛德的冒险小说](../Page/亨利·萊特·哈葛德.md "wikilink")《[三千年豔屍記](../Page/三千年豔屍記.md "wikilink")》：“作为一个小男孩，《[三千年豔屍記](../Page/三千年豔屍記.md "wikilink")》中的所有东西都引起了我的兴趣——像阿蒙纳塔斯的碎片。”小说中有一块陶片的复制品，上面有古代的铭文，被翻译出来后便可以带着主人公去往其中的古老王国。评论家把这个道具与《魔戒》中“[埃西鐸的遗嘱](../Page/埃西鐸.md "wikilink")”相比较。\[112\]评论家已找到哈葛德的传奇和托尔金的相似之处。\[113\]\[114\]\[115\]

托尔金小时候对的历史小说《黑色道格拉斯》（The Black
Douglas）以及反派[吉尔·德·莱斯男爵的](../Page/吉尔·德·莱斯.md "wikilink")[招魂術印象深刻](../Page/招魂術.md "wikilink")。《[霍比特人](../Page/哈比人歷險記.md "wikilink")》和《[魔戒](../Page/魔戒.md "wikilink")》中的事件在叙事和风格上都有与之相似之处\[116\]，其总体的意象据说对托尔金也有一定影响。\[117\]

#### 欧洲神话

早期[日耳曼文化](../Page/日耳曼人.md "wikilink")（尤其是[古英语](../Page/古英语.md "wikilink")）的文学、[诗歌和](../Page/诗歌.md "wikilink")[神话给予了托尔金很多灵感](../Page/神话.md "wikilink")，这也是他自己选择并热爱的专业领域。灵感来源包括[古英语文学如](../Page/古英语.md "wikilink")《[贝奥武甫](../Page/贝奥武甫.md "wikilink")》，北欧[萨迦如](../Page/萨迦_\(文学\).md "wikilink")《》（Völsungasaga）、《》（Hervarar
saga ok
Heiðreks）\[118\]、《[老埃達](../Page/老埃達.md "wikilink")》、《[散文埃達](../Page/散文埃達.md "wikilink")》以及古德语的《[尼伯龙根之歌](../Page/尼伯龙根之歌.md "wikilink")》等等。\[119\]尽管托尔金的作品与《》和《[尼伯龙根之歌](../Page/尼伯龙根之歌.md "wikilink")》有诸多相似之处（基于后者[理查德·瓦格纳创作了歌剧](../Page/理查德·瓦格纳.md "wikilink")《[尼伯龙根的指环](../Page/尼伯龙根的指环.md "wikilink")》），但他反对评论家把他的作品与瓦格纳的做直接比较，对出版商说：“两枚戒指都是圆环状的，除了这个就没其他共同点了。”然而，一些评论家\[120\]\[121\]\[122\]认为托尔金应当感激瓦格纳构想的一些元素，例如“能给予统治世界力量的戒指”这一概念。\[123\]
[至尊魔戒的两个特征](../Page/至尊魔戒.md "wikilink")，即其本质的邪恶和对心灵的腐蚀力量，在各神话中没有出现，却是瓦格纳歌剧的主要情节。

托尔金也确认了几个非日耳曼文化的影响。他引述了[古希腊作家](../Page/古希腊.md "wikilink")[索福克勒斯的悲剧](../Page/索福克勒斯.md "wikilink")《[俄狄浦斯王](../Page/俄狄浦斯王.md "wikilink")》作为《[精灵宝钻](../Page/精灵宝钻.md "wikilink")》和《》的灵感来源。在爱德华国王学校就学时，托尔金第一次阅读了[芬兰](../Page/芬兰.md "wikilink")《[卡勒瓦拉](../Page/卡勒瓦拉.md "wikilink")》的英译本。其主角[维纳莫宁据托尔金自己所说是](../Page/维纳莫宁.md "wikilink")[甘道夫的原型之一](../Page/甘道夫.md "wikilink")。其中的[反英雄角色](../Page/反英雄.md "wikilink")（Kullervo）是[圖林·圖倫拔的原型](../Page/圖林·圖倫拔.md "wikilink")。\[124\]许多研究托尔金的杰出学者认为托尔金也从[凯尔特](../Page/凱爾特神話.md "wikilink")（爱尔兰、苏格兰和威尔士）的历史和传说中汲取灵感。\[125\]\[126\]《[精灵宝钻](../Page/精灵宝钻.md "wikilink")》当年被退稿的一部分原因是其中复杂难读的凯尔特名字。然而，托尔金否认了它们的起源：

#### 天主教

[天主教的](../Page/天主教.md "wikilink")[神学和意象在塑造托尔金的想象世界中起了一定作用](../Page/神学.md "wikilink")，因他非常虔诚的心而弥漫在他的作品中。\[127\]\[128\]托尔金自己承认了这点：

学者尤其指出，托尔金描述的“恶”是正统基督教式的，即“善”的缺失。他用《魔戒》举例，如[索伦](../Page/索倫_\(魔戒\).md "wikilink")“[无眼脸的眼睛](../Page/索倫之眼.md "wikilink")”，“瞳孔是黑色裂缝，里面空无一物”。科赫认为托尔金的影响来自神学家[托马斯·阿奎那](../Page/托马斯·阿奎那.md "wikilink")，有理由相信托尔金作为研究中世纪的学者和[天主教徒对他很了解](../Page/天主教徒.md "wikilink")。\[129\]另一学者同意此观点，但认为托尔金的影响来自[阿尔弗雷德大帝对于古罗马哲学家](../Page/阿尔弗雷德大帝.md "wikilink")[波伊提烏著作](../Page/波伊提烏.md "wikilink")《》的古英语译本。雪培认为[波伊提烏对基督教看法下的](../Page/波伊提烏.md "wikilink")“恶”表达得最清楚：“邪恶是虚无。”托尔金推论出邪恶无法创造，就像弗罗多所说“这阴影……只能模仿，不能产生它自己的新东西”，[樹鬍和](../Page/樹鬍.md "wikilink")[愛隆也有类似评论](../Page/愛隆.md "wikilink")。\[130\]他接着论述，在《魔戒》中邪恶似乎是一种独立的力量，不仅仅是“善”的缺失（虽然不是[摩尼教异端所指的那种](../Page/摩尼教.md "wikilink")“独立”），并指出[阿尔弗雷德大帝的译本增注也许启发了这种观点](../Page/阿尔弗雷德大帝.md "wikilink")。\[131\]

另一个有趣的观点是斯塔福德·卡尔德克特对魔戒和其意义的神学角度诠释：“至尊魔戒代表堕落灵魂的[黑魔法](../Page/黑魔法.md "wikilink")，对上帝的拒绝服从。它表面上给人自由，但真正的功能是使持戒者成为[堕落天使的奴隶](../Page/堕落天使.md "wikilink")。它腐蚀持戒者作为人的意志，让他越来越‘消瘦’和不真实；隐身的功能象征它摧毁人所有的自然人际关系和自我认知。你可以说魔戒就是罪孽
（sin） 本身，一开始吸引人而且似乎没有危害，长久来看越来越难以放弃，使人堕落。”\[132\]

## 语言学

托爾金的學術生涯和他的文學創作兩者，和他所鍾愛的[語言和](../Page/語言.md "wikilink")[哲學是不可分割的](../Page/哲學.md "wikilink")。他在大學時代專攻[古希臘哲學](../Page/古希臘.md "wikilink")，並自1918年開始投入[牛津英語辭典的編寫工作](../Page/牛津英語辭典.md "wikilink")，其中被認為曾研究“W”為首字的一些詞彙，包括*walrus*「[海象](../Page/海象.md "wikilink")」此字的字源——一個他曾頭痛許久的詞彙\[133\]。1920年，托爾金赴[利茲擔任英語高級講師](../Page/利茲.md "wikilink")，並聲稱是因為他，而使得[語言學學生的數量由](../Page/語言學.md "wikilink")5位增加至20位。他曾教授[古英語](../Page/古英語.md "wikilink")[英雄詩體](../Page/英雄詩體.md "wikilink")、[英語歷史](../Page/英語.md "wikilink")、許多古英語及[中古英語文本](../Page/中古英語.md "wikilink")、古英語及中古英語哲學、[日耳曼哲學介紹](../Page/日耳曼語言.md "wikilink")、[哥德語](../Page/哥德語.md "wikilink")、[古冰島語及](../Page/古冰島語.md "wikilink")[中古威爾斯語](../Page/中古威爾斯語.md "wikilink")。1925年托爾金33歲那年，他成功申請到盎格魯撒克遜的Rawlinson及Bosworth教授職缺，並以其在利茲的日耳曼哲學的學生曾組成一個「維京俱樂部」（Viking
Club）為傲——在俱樂部裡，大學生可以自由地閱讀古斯堪的納維亞的[冒險故事及喝啤酒](../Page/萨迦_\(文学\).md "wikilink")\[134\]。

私底下，托爾金被任何有關種族及語言學意含的事物吸引，並且思考著一種與生俱來的語言品味，一種他在1955年「[英語與威爾斯語](../Page/英語與威爾斯語.md "wikilink")」這場演講中所提到的「母語」，相對於自幼時所學的語言。他認為[西密德蘭的中古英語是他所擁有的](../Page/西密德蘭.md "wikilink")「母語」，就如同他在1955年給大詩人[奧登的書信中所提到](../Page/奧登.md "wikilink")：「在血緣上，我是一位西密德蘭人（並且早先在我第一眼看到西密德蘭的中古英語時，我就已視他為已知的語言）。」\[135\]

另一項與托爾金身為哲學家的專業成就並駕齊驅，有時甚至超越它而使得他的學術產出相形渺小的，就是其對[人造語言創作的熱愛](../Page/人造語言.md "wikilink")。其中發展得最成熟的就是[昆雅和](../Page/昆雅.md "wikilink")[辛達林](../Page/辛達林.md "wikilink")，此兩種人造語言的語源關係構成了大部份托爾金傳說故事集系列的核心。對於托爾金來說，語言及[文法](../Page/文法.md "wikilink")，是攸關[美感及](../Page/美感.md "wikilink")[諧音的重要元素](../Page/諧音.md "wikilink")，而昆雅更特別是以「語音美感」（phonaesthetic）的考量去設計；它的創造概念為「精靈拉丁語」，在語音上也是以[拉丁語](../Page/拉丁語.md "wikilink")（Elvenlatin）為基礎，並以[芬蘭語及](../Page/芬蘭語.md "wikilink")[希臘語為靈感來源](../Page/希臘語.md "wikilink")\[136\]。

托爾金認為：「語言及與之關聯的神話傳說是不可分割的」，因此他終究對[國際輔助語言持悲觀的態度](../Page/國際輔助語言.md "wikilink")：他在1930年一場演講「[秘密的罪行](../Page/秘密的罪行.md "wikilink")」中對一群[世界語的使用者說](../Page/世界語.md "wikilink")：「你們的語言將產生一部神話」，然而在1956年他卻下一個結論：「[沃拉普克語](../Page/沃拉普克語.md "wikilink")、世界語、[伊多語及](../Page/伊多語.md "wikilink")[諾維亞語等諸如此類的語言均是枯萎](../Page/諾維亞語.md "wikilink")、死亡的語言，遠比現已不被使用的遠古語言死得還要徹底，因為他們的創作者從未創作任何以其為基礎的傳奇故事。」\[137\]

托爾金著作的流行對語言的使用，尤其是在奇幻文學中，已經產生微小但深遠的影響，甚至是（矮人之複數）和（精靈的、精靈語）這兩個原本自19世紀中或更早便不再被使用的拼法，也在托爾金的復興下廣為現今的字典所接受，分別取代19世紀後及的拼法。其他如他所創造的新詞*eucatastrophe*（救贖的時刻，或直譯為「善戰勝惡」）也在與托爾金著作相關的文章中經常地被使用。

## 作品索引

[缩略图](https://zh.wikipedia.org/wiki/File:El_Señor_de_los_Anillos_lectura.jpg "fig:缩略图")
**生前出版**

  - 1937年 《[哈比人歷險記](../Page/哈比人歷險記.md "wikilink")》 The Hobbit（\*
    中文繁體版，2012年12月，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")-{[《哈比人（全新修訂譯本）》](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=184251)}-，[朱學恆譯](../Page/朱學恆.md "wikilink")）
  - 1945年 《[尼格爾的叶子](../Page/尼格爾的叶子.md "wikilink")》 Leaf by Niggle（短篇）
  - 1947年 《[树与叶](../Page/树与叶.md "wikilink")》Tree and Leaf（散文）
  - 1949年 《[哈莫的農夫吉列斯](../Page/哈莫的農夫吉列斯.md "wikilink")》 Farmer Giles of
    Ham
  - 1954年
    《[魔戒現身](../Page/魔戒現身.md "wikilink")》，《[魔戒](../Page/魔戒.md "wikilink")》首部曲
    The Lord of the Rings：The Fellowship of the Ring（\*
    中文繁體版，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")-[ISBN：9789570841008](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=18425201)）
  - 1954年
    《[雙城奇謀](../Page/雙城奇謀.md "wikilink")》，《[魔戒](../Page/魔戒.md "wikilink")》二部曲
    The Lord of the Rings：The Two Towers（\*
    中文繁體版，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")-[ISBN：9789570841015](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=18425202)）
  - 1955年
    《[王者再臨](../Page/王者再臨.md "wikilink")》，《[魔戒](../Page/魔戒.md "wikilink")》三部曲
    The Lord of the Rings：The Return of the King（\*
    中文繁體版，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")-[ISBN：9789570841022](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=18425203)）
  - 1962年 《[汤姆·邦巴迪爾历险记](../Page/汤姆·邦巴迪爾历险记.md "wikilink")》 The
    Adventures of Tom Bombadil and Other Verses from the Red Book
  - 1964年 《树与葉·续篇》 Tree and Leaf（神话诗篇 On Fairy-Stories and Leaf by
    Niggle in book form）
  - 1967年 《[大伍頓的铁匠](../Page/大伍頓的铁匠.md "wikilink")》Smith of Wootton Major

**去世以后出版**

托爾金去世後，由其子[克里斯托夫·托爾金整理出版的其他作品](../Page/克里斯托夫·托爾金.md "wikilink")：

  - 1976年 《[聖誕老爸的來信](../Page/聖誕老爸的來信.md "wikilink")》（The Father
    Christmas Letters）
  - 1977年 《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》（The Silmarillion*）*（\*
    中文繁體版，2002年，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")-{[ISBN：9789570825244](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=87015)）
  - 1981年 《[J·R·R·托尔金的信件](../Page/J·R·R·托尔金的信件.md "wikilink")》（The
    Letters of J. R. R. Tolkien）
  - 1982年 《[幸福先生](../Page/幸福先生.md "wikilink")》（Mr. Bliss）（\*
    中文繁體版，2013年12月，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")
    [ISBN：9789570843040](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=1100549)）
  - 1983年 The Monsters and the Critics
  - 1998年 《[羅佛蘭登](../Page/羅佛蘭登.md "wikilink")》（Roverandom）
  - 2007年 《》（The Children of Hurin）\[138\]（\*
    中文繁體版，2008年05月，台灣[聯經出版公司出版](../Page/聯經出版.md "wikilink")
    [ISBN：9789570832662](https://www.linkingbooks.com.tw/LNB/book/Book.aspx?ID=187019)）
  - 2017年《[貝倫與露西安](../Page/貝倫與露西安.md "wikilink")》（Beren and
    Luthien）\[139\]\[140\]
  - 2018年《[貢多林的陷落](../Page/貢多林的陷落.md "wikilink")》（The Fall of
    Gondolin），將於八月出版\[141\]\[142\]\[143\]\[144\]

以下12卷统称为 《[中土世界的历史](../Page/中土世界的历史.md "wikilink")》〔The History of
Middle-earth〕

  - 1983年 The Book of Lost Tales 1
  - 1984年 The Book of Lost Tales 2
  - 1985年 The Lays of Beleriand
  - 1986年 The Shaping of Middle-Earth
  - 1987年 The Lost Road and Other Writings
  - 1988年 The Return of the Shadow (The History of LotR, Part 1)
  - 1989年 The Treason of Isengard (The History of LotR, Part 2)
  - 1990年 The War of the Ring (The History of LotR, Part 3)
  - 1992年 Sauron Defeated (The History of LotR, Part 4)
  - 1993年 Morgoth's Ring (The Later Silmarillion, Part 1)
  - 1994年 The War of the Jewels (The Later Silmarillion, Part 2)
  - 1996年 The Peoples of Middle-earth

## 藝術

除作家、語言學家身分外，托爾金亦擅長於作畫，其繪畫技巧主要靠自學而來\[145\]。他著有兒童[繪本](../Page/繪本.md "wikilink")《幸福先生》和《聖誕老爸的來信》，這兩本書皆是在他過世後整理出版的\[146\]。此外托爾金也常為自己的文學作品創作插畫，如1937年版本《哈比人歷險記》的封面插圖即是他本人親自創作的。托爾金的插畫作品多以花草樹木等大自然環境為主題\[147\]。在他身後總共留下两百多張[水彩](../Page/水彩.md "wikilink")、[速寫](../Page/速寫.md "wikilink")、[墨水畫作品](../Page/墨水畫.md "wikilink")，後來這些畫作被與結集成《[J·R·R·托爾金：藝術家與插畫家](../Page/J·R·R·托爾金：藝術家與插畫家.md "wikilink")》與《霍比特人的艺术》出版\[148\]\[149\]\[150\]\[151\]。

## 身後影響

### 對文學的影響

托爾金被譽為「現代奇幻文學之父」\[152\]，他的作品（尤其是《魔戒》三部曲）影響和啟發了後世諸多的奇幻文學作品。媒體人在《[紐約時報](../Page/紐約時報.md "wikilink")》書評專欄上表示：「沒有托爾金，我們無法想像會有[哈利波特的熱潮](../Page/哈利波特.md "wikilink")。」[臺灣](../Page/臺灣.md "wikilink")《魔戒》譯者[朱學恆亦指出](../Page/朱學恆.md "wikilink")：「在美國每年出版的兩億本平裝小說中，有超過四分之一的作品直接或間接的和托爾金有關聯」\[153\]。

不少著名作家皆十分推崇托爾金的地位，並且承認自己的文學作品有部分是受到了托爾金小說的啟發。[J·K·羅琳表示自己](../Page/J·K·羅琳.md "wikilink")14歲時就開始閱讀《魔戒》，並說：「到《哈利波特》寫到最後一卷時，我仍然堅持認為自己不會超越托爾金」。[史蒂芬·金也承認他的作品](../Page/史蒂芬·金.md "wikilink")《[黑塔](../Page/黑塔系列小說.md "wikilink")》有受到了《魔戒》影響\[154\]。而[喬治·R·R·馬丁更表示](../Page/喬治·R·R·馬丁.md "wikilink")：「拿到托爾金的书时，天啊，我絕望了，我怎麼努力也達不到他的成就，甚至连接近也做不到！他是真正的大師。」\[155\]，他還指出《魔戒》裡甘道夫之死的情節深深影響了他的寫作\[156\]。

## 紀念

在1982年4月發現的一顆[小行星被命名為](../Page/小行星.md "wikilink")「[2675托爾金](../Page/小行星2675.md "wikilink")」，用以紀念J·R·R·托爾金\[157\]\[158\]。

自2003年以來，發起了「[托爾金閱讀日](../Page/托爾金閱讀日.md "wikilink")」的活動，鼓勵大家閱讀托爾金的文學作品\[159\]。

### 傳記電影

2013年[福斯探照燈影業和](../Page/福斯探照燈影業.md "wikilink")[徹寧娛樂公司正籌備一部以托爾金為主題的](../Page/徹寧娛樂公司.md "wikilink")[傳記電影](../Page/傳記電影.md "wikilink")\[160\]。2017年，這部名為《[托爾金](../Page/托爾金_\(電影\).md "wikilink")》、由[芬蘭籍導演](../Page/芬蘭.md "wikilink")[多梅·卡魯科斯基執導的電影開始拍攝](../Page/多梅·卡魯科斯基.md "wikilink")，並於同年結束拍攝，由[尼古拉斯·霍爾特飾演J](../Page/尼古拉斯·霍尔特.md "wikilink")·R·R·托爾金\[161\]\[162\]。該片的上映日期为美国时间5月10号。

## 条目注释

## 參考资料

注：*Biography*表示参考资料来自 ；*Letters*表示参考资料来自

## 參見

  - [迹象文学社](../Page/迹象文学社.md "wikilink")
  - [C·S·路易斯](../Page/C·S·路易斯.md "wikilink")
  - [翁贝托·埃可](../Page/翁贝托·埃可.md "wikilink")

## 外部連結

  - [Talking About
    Tolkien](https://web.archive.org/web/20040615104906/http://www.talkingabouttolkien.com/index.html)

  - [The Tolkien Estate Website](http://www.tolkienestate.com/home/)

  - [Journal of Inklings Studies](http://www.inklings-studies.com/)
    牛津上经过[同行评审的关于托尔金及奇幻与神话文学界的文章](../Page/同行评审.md "wikilink")

  - [Arda百科 - 马克菲舍尔的礼品站 托尔金作品](http://www.glyphweb.com/arda/default.htm)

  - [魔戒魔幻区 -
    托尔金社区](https://web.archive.org/web/20060810173110/http://www.lotrplaza.com/)

  - [托尔金协会](http://www.tolkiensociety.org/tolkien/)

  - [托尔金维基协会](http://www.thetolkienwiki.org/)

  - [TheOneRing.net - 托尔金粉丝制作](http://www.theonering.net/)

  -
  -
  -
  -
  -
[J·R·R·托爾金](../Category/J·R·R·托爾金.md "wikilink")
[Category:牛津大学埃克塞特学院校友](../Category/牛津大学埃克塞特学院校友.md "wikilink")
[Category:英国作家](../Category/英国作家.md "wikilink")
[Category:20世纪作家](../Category/20世纪作家.md "wikilink")
[Category:奇幻小說家](../Category/奇幻小說家.md "wikilink")
[Category:英格兰儿童文学作家](../Category/英格兰儿童文学作家.md "wikilink")
[Category:罗马天主教作家](../Category/罗马天主教作家.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:牛津大学教授](../Category/牛津大学教授.md "wikilink")
[Category:牛津大学荣誉博士](../Category/牛津大学荣誉博士.md "wikilink")
[Category:語言創造者](../Category/語言創造者.md "wikilink")
[Category:文字發明者](../Category/文字發明者.md "wikilink")
[Category:英國天主教徒](../Category/英國天主教徒.md "wikilink")
[Category:德國裔英格蘭人](../Category/德國裔英格蘭人.md "wikilink")
[Category:英國第一次世界大戰人物](../Category/英國第一次世界大戰人物.md "wikilink")
[Category:英国语言学家](../Category/英国语言学家.md "wikilink")
[Category:皇家文学学会院士](../Category/皇家文学学会院士.md "wikilink")
[Category:利兹大学教师](../Category/利兹大学教师.md "wikilink")

1.  *Biography*, pp. 111, 200, 266.

2.  *Letters*, nos. 131, 153, 154, 163.

3.
    作者强调这影响不仅来自托尔金，也来自[威廉·莫里斯](../Page/威廉·莫里斯.md "wikilink")、[喬治·麥克唐納](../Page/喬治·麥克唐納.md "wikilink")、[罗伯特·欧文·霍华德和](../Page/罗伯特·欧文·霍华德.md "wikilink")[艾立克·洛克·艾丁生](../Page/艾立克·洛克·艾丁生.md "wikilink").

4.

5.  The *Oxford companion to English Literature* calls him "*the
    greatest influence within the fantasy genre*. (Sixth edition, 2000,
    page 352. Ed. Margaret Drabble.)

6.

7.

8.

9.  *Letters*, no. 165.

10.  Tolkien jokingly inserted himself as a "cameo" into ** under the
    name Rashbold, a literal translation of *tollkühn*.

11.

12.

13. Georg Gerullis: *Die altpreußischen Ortsnamen*, o.v., Berlin/Leipzig
    1922, p. 184.

14. Max Mechow: *Deutsche Familiennamen prussischer Herkunft*,
    Tolkemita, Dieburg 1994, S. 99.

15. *Biography*, p. 14.

16. *Biography*, p. 13. Both the spider incident and the visit to a
    kraal are covered here.

17. *Biography*, p. 24.

18. *Biography*, p. 27.

19. *Biography*, p. 113.

20. *Biography*, p. 29.

21.

22. *Biography*, p. 22.

23. *Biography*, p. 30.

24. Carpenter, *Biography*, pages 24–51.

25. *Biography*, p. 31.

26. [Find A
    Grave](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=10841494)
    Retrieved on 25 December 2012.

27. Carpenter, *Biography*, page 31.

28. Carpenter, *Letters of J.R.R. Tolkien*, letter 267.

29. *Letters*, no. 306.

30. [J.R.R. Tolkien](http://www.birminghamheritage.org.uk/tolkien.html),
    Birmingham Heritage Forum. Retrieved on 27 April 2009.

31. [J. R. R. Tolkien](http://www.birmingham.gov.uk/tolkien), Archives
    and Heritage Service, Birmingham City Council. Updated 7 January
    2009. Retrieved on 28 April 2009.

32. Saler, Michael T. (2012) *As If: Modern Enchantment and the Literary
    Pre-History of Virtual Reality*, Oxford University Press, ISBN
    978-0-19-534316-8, page 181

33. [Tolkien's Not-So-Secret Vice](http://folk.uib.no/hnohf/vice.htm)

34. [Tolkien's Languages | The Tongues of
    Middle-Earth](https://web.archive.org/web/20131224110153/http://lordfingulfin.webs.com/earlierlanguages.htm)

35. *Biography*, pp. 53–54.

36. *Tolkien and the Great War*, page 6.

37. Reference RG14PN17925 RG78PN1093 RD383 SD3 ED15 SN118.

38.
39. [dab](http://maps.google.de/maps/user?uid=112792823367225913506&hl=de&gl=de)
    , [Roots of
    Romance](http://maps.google.de/maps/ms?ie=UTF8&hl=de&om=1&msa=0&msid=108471754903242205397.00000112a38e113bc84bd&ll=46.363988,8.055725&spn=0.843415,1.683655&t=k&z=9)
    (zoomed in on 1911 trail), hosted on [Google
    Maps](../Page/Google_Maps.md "wikilink"). Retrieved 28 April 2009.

40.

41.

42. *Biography*, p. 43.

43. *Biography*, pp. 67–69.

44. *Biography*, p. 73.

45. *Biography*, p. 86.

46. *Biography*, pp. 77–85.

47. *Tolkien and the Great War*, page 94.

48. Garth, John. *Tolkien and the Great War*, Boston, Houghton Mifflin
    2003, pp. 89, 138, 147.

49. Quoted in John Garth, *Tolkien and the Great War*, p. 138.

50. *Biography*, p. 93.

51. Carpenter, Humphrey. J.R.R. Tolkien A Biography. Houghton Mifflin,
    1977. Print.

52. Garth, John. *Tolkien and the Great War*, Boston, Houghton Mifflin
    2003, pp. 207 *et seq.*

53. Tolkien's [Webley .455](../Page/Webley_.455.md "wikilink") service
    revolver was put on display in 2006 as part of a [Battle of the
    Somme](../Page/Battle_of_the_Somme.md "wikilink") exhibition in the
    [Imperial War Museum](../Page/Imperial_War_Museum.md "wikilink"),
    London. (See  and ) Several of his service records, mostly dealing
    with his health problems, can be seen at the National Archives. ()

54.

55.

56.

57. See *The Name Nodens* (1932) in the bibliographical listing. For the
    etymology, see
    [Nodens\#Etymology](../Page/Nodens#Etymology.md "wikilink").

58.

59.

60.

61.

62.

63.

64.

65.
66.

67. *Biography*, p. 143.

68.

69. Tolkien: **. Chiefly, p.4 in the Introduction by .

70. Tolkien: *Finn and Hengest*, the discussion of *Eotena*, *passim*.

71.

72. "The Data File: 'New' Tolkien Book,"
    [Locus](../Page/Locus.md "wikilink"), February 2003, p.85.

73. *Letters*, no. 35 (see also editorial note).

74.

75.
76.
77.
78.
79.

80.
81.
82.
83.
84.
85.
86.

87.

88. Rogerson, John. *The Oxford Illustrated History of the Bible*, 2001.

89. J. R. R. Tolkien,  (1976)

90.

91.

92.
93.

94. *Letters*, no. 336.

95. *Letters*, no. 332.

96. *Letters*, no. 334 (editorial note).

97.
98.

99.

100.
101.

102.

103.

104.

105.
106.

107.
108. *Letters*, no. 1.

109. *Letters*, no. 226.

110. Anderson, Douglas A. *The Annotated Hobbit*, Boston, Houghton
     Mifflin 1988, p. 183, note 10.

111. Anderson, Douglas A. *The Annotated Hobbit*, Boston, Houghton
     Mifflin 1988, pp. 6–7.

112.

113.

114.

115.

116. Anderson, Douglas A. *The Annotated Hobbit*, Boston, Houghton
     Mifflin 1988, p. 150.

117. Lobdell, Jared C. *The World of the Rings: Language, Religion, and
     Adventure in Tolkien*, pp. 6–7.

118. As described by Christopher Tolkien in *Hervarar Saga ok Heidreks
     Konung* (Oxford University, Trinity College). B. Litt. thesis.
     1953/4. \[Year uncertain\], *The Battle of the Goths and the Huns*,
     in: Saga-Book (University College, London, for the Viking Society
     for Northern Research) 14, part 3 (1955–6)
     [1](http://www.tolkiensociety.org/tolkien/bibl4.html)

119.

120. Alex Ross, ["The Ring and The Rings: Wagner vs.
     Tolkien"](http://www.newyorker.com/archive/2003/12/22/031222crat_atlarge),
     *The New Yorker*, 22 December 2003. Retrieved on 2 December 2011.

121. , [The 'Ring' and the remnants of the
     West](http://www.atimes.com/atimes/Front_Page/EA11Aa02.html), *Asia
     Times*, 11 January 2003. Retrieved on 27 April 2009.

122. Spengler, [Tolkien's Christianity and the pagan
     tragedy](http://www.atimes.com/atimes/Front_Page/ID24Aa01.html),
     *Asia Times*, 11 January 2003. Retrieved on 27 April 2009.

123. [Tolkien's Ring and Der Ring des
     Nibelungen](http://tolkienonline.de/etep/1ring5.html), Chapter 5 in
     Harvey, David (1995). *[One Ring to Rule them
     All](http://tolkienonline.de/etep/ring_toc.html)*. Updated 20
     October 1995. Retrieved on 27 April 2009.

124.

125.

126.

127.
128.

129.

130.

131. *Road*, pp. 141–145.

132.

133. Winchester, Simon (2003). *The Meaning of Everything: The Story of
     the Oxford English Dictionary.* Oxford University Press. ISBN
     978-0-9654996-3-7

134. (Letter dated 27 June 1925 to the Electors of the Rawlinson and
     Bosworth Professorship of Anglo-Saxon, University of Oxford,

135.

136.

137.

138.

139.

140.

141.

142.

143.

144.

145.

146.

147.

148.

149.

150.

151.

152.

153. [朱學恆](../Page/朱學恆.md "wikilink")〈旅程開始之前〉（源自：）

154.
155.

156.

157.

158.

159.

160.

161.

162.