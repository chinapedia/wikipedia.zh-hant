[thumb](../Page/F:Erotic_asphyxiation.jpg.md "wikilink")
**窒息式性爱**（、）是指为了获得[性快感](../Page/性刺激.md "wikilink")，故意限制大脑供[氧的行为](../Page/氧.md "wikilink")。该做法利用人體在[缺氧](../Page/缺氧.md "wikilink")、[窒息的瞬間](../Page/窒息.md "wikilink")，局部器官的高度收縮，而使自己或對方從中得到性快感和[性高潮](../Page/性高潮.md "wikilink")，但常有不小心而令人死亡的事件發生。如果是[自慰时进行此行为](../Page/自慰.md "wikilink")，则会被称为**自慰性窒息**（）。在[美國精神醫學學會的](../Page/美國精神醫學學會.md "wikilink")《[精神疾病診斷與統計手冊](../Page/精神疾病診斷與統計手冊.md "wikilink")》中，窒息式性爱被归于[性偏離的一种](../Page/性偏離.md "wikilink")。

## 生理

约翰·库拉（）写道，“脖子两侧的[總頸動脈负责将含有](../Page/總頸動脈.md "wikilink")[氧的血液从心脏运输到大脑中](../Page/氧.md "wikilink")。当因吊颈而使得该血管受到挤压时，大脑会突然缺氧，[二氧化碳也开始堆积](../Page/二氧化碳.md "wikilink")，此时会有头晕目眩的感觉并伴有愉悦感，这些会使得自慰带来的快感度升高。”\[1\]乔治·舒曼（）将该影响描写为“当大脑供氧不足时，它会进入到被称为[缺氧的状态](../Page/缺氧.md "wikilink")，此状态下人会保持清醒，但却处在半致幻状态。该感觉与[性高潮结合](../Page/性高潮.md "wikilink")，带来的快感不比[可卡因差](../Page/可卡因.md "wikilink")，也非常容易上瘾。”\[2\]

关于窒息式性爱因缺氧所带来的幻觉，E·劳埃德（）指出，这种幻觉可能与身处在高海拔地区的攀岩者感受到的幻觉类似。他还指出，飞机在高海拔处突然减压并不会带来这种幻觉，因此这一感觉可能并不是单纯因缺氧而起。在回顾了有关缺氧方面的文献后，他发现“[神经递质](../Page/神经递质.md "wikilink")、[多巴胺](../Page/多巴胺.md "wikilink")、[血清素和](../Page/血清素.md "wikilink")，它们会互相影响，并导致大脑神经化学的异常。这种异常变化在所有的致幻情形中都有出现。”\[3\]

## 历史

自慰性窒息从17世纪便有所记载，起先是用来治疗[勃起功能障碍的](../Page/勃起功能障碍.md "wikilink")。\[4\]这种疗法很可能源自被[绞刑者](../Page/绞刑.md "wikilink")，人们注意到男性受刑者出现了[勃起的现象](../Page/勃起.md "wikilink")，有时能够持续到死亡之后（即[死亡勃起](../Page/死亡勃起.md "wikilink")），偶尔亦会伴有[射精](../Page/射精.md "wikilink")。\[5\]

## 方式

悬挂、用塑料袋闷住头部、自己住自己（比如使用[绳结](../Page/結紮.md "wikilink")）、利用气体或易挥发溶质、挤压胸腔等不同方式（或对这些方式加以结合）都能达到窒息式性愛所需要的耗氧量\[6\]，也有的人会使用一些更为复杂的装置来达到此目的\[7\]。哪怕小心谨慎地进行，窒息式性爱也很危险，已造成大量的意外身亡事故。尤瓦（）1995年称，“据估计，自慰性窒息会造成美国每年250至1000不等的死亡人数。”\[8\][斯堪的纳维亚](../Page/斯堪的纳维亚.md "wikilink")\[9\]和[德国](../Page/德国.md "wikilink")\[10\]\[11\]亦有类似报告。自慰性窒息常常被误判为自杀，也是青少年的常见死亡原因之一。\[12\]

## 意外死亡

在窒息式性爱中，部分缺氧的状况常常会导致人失去意识，从而失去对悬挂绳索的控制，并进一步导致缺氧，最终死亡。尽管有窒息癖的人们经常会和别人一起进行性爱活动，但对方通常因过于投入而忽略了被窒息者可能处在的危险境地。\[13\]

在部分死亡案例中，窒息癖死者的尸体被发现时全裸或手握性器，身旁有情色材料、性玩具，有死前高潮过的迹象。\[14\]这些意外死亡现场常常会有死者生前曾进行其他[性偏離活动的迹象](../Page/性偏離.md "wikilink")，\[15\]如[女装癖](../Page/異裝.md "wikilink")、受虐癖。\[16\]如果死者是在家中居住的青少年，家属通常会“打扫”现场，将这些性偏离的证据清理掉，结果破坏了现场，导致死者看上去像是故意自杀而非意外身亡。\[17\]

窒息式性爱导致的死者多为男性；在1974年至1987年、加拿大[安大略省和](../Page/安大略省.md "wikilink")[艾伯塔省所有因窒息式性爱身亡的](../Page/艾伯塔省.md "wikilink")117名死者中，只有一名是女性。\[18\]窒息式性爱导致的意外死亡主要发生在25岁前后，\[19\]\[20\]但也存在青少年\[21\]\[22\]\[23\]和70岁男性的死亡案例。\[24\]\[25\]

因为律师和保险公司的缘故，这些窒息式性爱的案例引起了临床医生的注意——有的保险可以给意外身亡赔付，但自杀是不行的。\[26\]\[27\]\[28\]

### 著名案例

  - 1990年代“海尔采格诉《好色客》案”中，黛安·海尔采格（Diane
    Herceg）指控《[好色客](../Page/好色客.md "wikilink")》杂志导致了她14岁儿子
    Troy D. 的死亡，因为她的儿子在读了杂志上关于窒息式性爱的介绍后打算尝试一下，结果发生了意外。\[29\]

  - 1994年，，英国政治家、[伊斯特利](../Page/伊斯特利.md "wikilink")[保守黨议员](../Page/保守黨_\(英國\).md "wikilink")，因自慰性窒息和[自我綁縛身亡](../Page/自我綁縛.md "wikilink")。\[30\]

  - 1996年，，音乐家兼作曲家，因明显自慰性窒息而身亡。\[31\]

  - ，澳大利亚歌手兼作曲人，隶属于摇滚乐队 。
    他死于1997年，[死因裁判官认定他是自杀](../Page/死因裁判官.md "wikilink")，但其亲人、伴侣都认为是自慰性窒息。\[32\]

  - 演员[大衛·卡拉定死于](../Page/大衛·卡拉定.md "wikilink")2009年6月4日。为他作不公开尸检的检查者认定此事件属于意外窒息。\[33\]\[34\]被发现时，其尸体位于他在泰国的房间的柜子里，用绳子吊着<ref name="abc">

</ref>\[35\]，有迹象表明他死前曾高潮过。\[36\]经过两次尸检，人们得出结论认为不是自杀，一名验过尸体的泰国法医认为他的死因有可能是自慰性窒息。\[37\]\[38\]卡拉定的两名前妻，盖尔·詹森（）\[39\]\[40\]和玛丽娜·安德森（）\[41\]\[42\]都公开表示自我綁縛是他的性趣之一。

  - [重庆红衣男孩事件中](../Page/重庆红衣男孩事件.md "wikilink")，一名身着红衣、双手悬梁、双脚绑秤砣的男孩身亡，法医鉴定结果为体位性性窒息性死亡。\[43\]

## 虚构作品

有关自慰性窒息的话题本身就颇为敏感，因此经常会成为[都市傳說的素材](../Page/都市傳說.md "wikilink")，也在虚构作品中屡次出现。

  - 在[薩德侯爵著名的小说](../Page/薩德侯爵.md "wikilink")《》中，贾斯汀被人怀疑有窒息癖的倾向，但她挺过了这些怀疑。
  - [恰克·帕拉尼克所著小说](../Page/恰克·帕拉尼克.md "wikilink")《》中的小短篇《肠子》（）中，一名角色谈到了发现自己儿子意外死于自慰性窒息的父母们。他们据说会在警察或死因裁判官到来之前清理现场，以防给家里蒙羞。
  - 在小说及其同名改编电影《》中，被谋杀者被认为可能死于窒息式性爱，剧中亦对此有过一定的介绍。
  - 电影《》中，主角还处于青春期的儿子因窒息性自慰而不慎让自己窒息死亡。主角将儿子的死亡包装为一起自杀事件，但却增加了因而背负骂名的可能性。
  - 电影《[天地無倫](../Page/天地無倫.md "wikilink")》中，一个名为泰特（Tate）的角色曾尝试过自慰性窒息。
  - 《[-{zh-cn:六英尺下;zh-tw:六呎風雲;zh-hk:身前身後;}-](../Page/六呎風雲.md "wikilink")》在《返回园中》（）一集的[冷开场中出现了自慰性窒息](../Page/冷开场.md "wikilink")。
  - 《[南方公園](../Page/南方公園.md "wikilink")》的《》一集中，[肯尼·麦克康米克在身着](../Page/肯尼·麦克康米克.md "wikilink")[蝙蝠俠服装](../Page/蝙蝠俠.md "wikilink")、尝试自慰性窒息时死亡。
  - 《[加州靡情](../Page/加州靡情.md "wikilink")》第四季《偷鸡摸狗》（）一集中，Zig Semetauer
    被发现因自慰性窒息导致的缺氧死在了自己的浴室中。
  - 电影《[-{zh-cn:一夜大肚; zh-tw:好孕臨門;
    zh-hk:弊傢伙…搞大咗;}-](../Page/弊傢伙…搞大咗.md "wikilink")》中有个角色负责监督他的朋友，防止其进行自慰性窒息的行为。
  - 由[阿部定事件改编的电影](../Page/阿部定事件.md "wikilink")《[感官世界](../Page/感官世界.md "wikilink")》中，男主角在窒息式性爱中意外死亡。

## 参见

  -
  -
  - [窒息游戏](../Page/窒息游戏.md "wikilink")

  - [颜面骑乘](../Page/颜面骑乘.md "wikilink")

  - [缺氧](../Page/缺氧.md "wikilink")

## 参考资料

## 拓展阅读

  - Robert R. Hazelwood, Park Elliot Dietz, Ann Wolbert Burgess:
    *Autoerotic Fatalities*. Lexington, Mass.: LexingtonBooks, 1983.
  - Sergey Sheleg, Edwin Ehrlich: *Autoerotic Asphyxiation: Forensic,
    Medical, and Social Aspects*, Wheatmark (August 15, 2006), trade
    paperback, 208 pages
  - John Money, Gordon Wainwright and David Hingsburger: *The Breathless
    Orgasm: A Lovemap Biography of Asphyxiophilia*. Buffalo, New York:
    Prometheus Books, 1991.

## 外部链接

  - [Well Hung: Death By
    Orgasm](https://web.archive.org/web/20080311153542/http://www.altpenis.com/penis_news/autoerotic_asphyxia.shtml)
  - [Turvey B. "An Objective Overview of Autoerotic
    Fatalities"](http://www.corpus-delicti.com/auto.html)
  - [The Medical Realities of Breath Control Play By Jay
    Wiseman](http://www.evilmonk.org/A/breath.cfm)
  - [Is there a safe way to perform autoerotic asphyxiation? - Slate
    Magazine](http://www.slate.com/articles/news_and_politics/explainer/2009/06/strangle_with_care.html)

[Category:BDSM术语](../Category/BDSM术语.md "wikilink")
[Category:呼吸](../Category/呼吸.md "wikilink")
[Category:BDSM](../Category/BDSM.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.

9.

10.

11.

12.

13. [Autoerotic
    Asphyxiophilia](http://www.soc.ucsb.edu/sexinfo/article/autoerotic-asphyxiophilia)
     on 'Sexinfo' website, University of Santa Barbara, Ca.

14.
15.

16.

17. Downs, Martin. [The Highest Price for
    Pleasure](http://www.medicinenet.com/script/main/art.asp?articlekey=51776)
    , featured by WebMD

18.
19.
20.

21.

22.

23.

24.
25.
26.

27.

28.

29.

30. "[Police probe MP's suspicious
    death](http://news.bbc.co.uk/onthisday/hi/dates/stories/february/8/newsid_2538000/2538165.stm)
    ". *BBC News*，1994-02-08

31.

32. [Michael Hutchence found dead in
    hotel](http://news.bbc.co.uk/onthisday/hi/dates/stories/november/22/newsid_4006000/4006205.stm)
     BBC News, 1997-11-22

33. Orloff, Brian. ["David Carradine Died of Accidental
    Asphyxiation"](http://www.people.com/people/article/0,,20289267,00.html)
    . *People*, 2009-07-02

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.