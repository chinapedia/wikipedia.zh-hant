**林培瑞**（英文名：，音譯**小尤金·培瑞·林克**；），生于[纽约州](../Page/纽约州.md "wikilink")，最高學歷為[哈佛大学](../Page/哈佛大学.md "wikilink")[哲学博士](../Page/哲学.md "wikilink")（1976年），汉学家。曾担任[普林斯顿大學东亚系研究讲师](../Page/普林斯顿大學.md "wikilink")，现任[加利福尼亚大学河濱分校校長特聘講座教授](../Page/加利福尼亚大学河濱分校.md "wikilink")。

## 家庭背景

父亲是[纽约州立大学历史教授](../Page/纽约州立大学.md "wikilink")。

## 簡介

林培瑞，生于[纽约州](../Page/纽约州.md "wikilink")，1966年获[哈佛大学](../Page/哈佛大学.md "wikilink")[文学学士](../Page/文学学士.md "wikilink")，1969年获[哈佛大学](../Page/哈佛大学.md "wikilink")[文学硕士](../Page/文学硕士.md "wikilink")，1976年获[哈佛大学](../Page/哈佛大学.md "wikilink")[哲学博士](../Page/哲学.md "wikilink")（博士论文题为：《上海传统风格的通俗小说（1910\~1930）》（'''Traditional
style popular fiction in Shanghai , 1910\~1930
'''））。学术涉猎广泛，主要研究中国现代文学、社会史、大众文化、20世纪初中国的通俗小说、[毛泽东时代以后的](../Page/毛泽东.md "wikilink")[中国文学](../Page/中国文学.md "wikilink")。精通英文、中文、法文、日文。

林培瑞是美国[汉学家中与](../Page/汉学家.md "wikilink")[中国社会联系最为密切的一位](../Page/中国.md "wikilink")“中国通”之一。1972年，在有着“[乒乓外交](../Page/乒乓外交.md "wikilink")”之称的中国乒乓球代表团访美时，林担任中文翻译，不久，他到中国访问一个月，据他本人讲，这一个月的消费只有550美元。1970年代末，重返中国并开始关注和研究[中国当代文学](../Page/中国当代文学.md "wikilink")，结识[刘宾雁等作家](../Page/刘宾雁.md "wikilink")。1989年
“[六四](../Page/六四.md "wikilink")”期间，林培瑞担任[美国科学院中国办事处主任](../Page/美国科学院.md "wikilink")。六月五日凌晨，他把方励之夫妇带入美国大使馆。他们二人一直住在里面，大约半年后经过日本促谈，在美国解除对华经济制裁的前提下，中国政府允许他们赴美。他是《[中国“六四”真相](../Page/中国“六四”真相.md "wikilink")》英文版的编辑之一。

林培瑞將[零八宪章譯成英文](../Page/零八宪章.md "wikilink")，西方媒體於2009年1月刊出\[1\]。

林培瑞曾经和一个女孩谈论中国政改，女孩说現在民主制度不适合中国，要等幾十年。林培瑞反问她，如果政府突然宣布政改，你是否會站出來反對？女孩最終沒有回答他的问题\[2\]。

## 經歷

1973年，担任[普林斯顿东亚系研究讲师](../Page/普林斯顿.md "wikilink")，現任[加利福尼亚大学河濱分校校長特聘講座教授](../Page/加利福尼亚大学河濱分校.md "wikilink")。

## 主要著作

  - [李希凡论现代中国文学](../Page/李希凡.md "wikilink")（Li Hsifan on Modern Chinese
    Literature）,China Quart,June 1974

  - 鸳鸯蝴蝶派：20世纪初中国城市的[通俗小说](../Page/通俗小说.md "wikilink")

  - 同[包天笑的一次会见](../Page/包天笑.md "wikilink")

  - 邓小平中国文化改革的范围

  -
  -
  -
  -
## 榮譽

[美国](../Page/美国.md "wikilink")[普林斯顿大学东亚研究系榮譽教授](../Page/普林斯顿大学.md "wikilink")\[3\]。

## 参考来源

<references/>

## 外部链接

  -
  - [南都周刊对林培瑞的专访](https://web.archive.org/web/20160304122720/http://www.nbweekly.com/magazine/cont.aspx?artiID=11213)

[L](../Category/紐約州人.md "wikilink") [L](../Category/美國漢學家.md "wikilink")
[L](../Category/哈佛大學校友.md "wikilink")
[Category:普林斯顿大学教授](../Category/普林斯顿大学教授.md "wikilink")
[Category:河濱加州大學教師](../Category/河濱加州大學教師.md "wikilink")
[Category:中文—英文翻译家](../Category/中文—英文翻译家.md "wikilink")

1.
2.  [南都周刊-2010-南都周刊对林培瑞专访](http://www.nbweekly.com/magazine/cont.aspx?artiID=11213)

3.