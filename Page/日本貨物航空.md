**日本貨物航空**（，簡稱**NCA**）是[日本一家專營](../Page/日本.md "wikilink")[國際航線的](../Page/國際航線.md "wikilink")[貨運航空公司](../Page/貨運航空公司.md "wikilink")，為[日本郵船附屬子公司](../Page/日本郵船.md "wikilink")。

## 歷史

日本貨物航空在1978年9月27日開始營運，現時網絡已覆蓋全球三大洲不少主要城市。

最初，日本貨物航空是[日本郵船和](../Page/日本郵船.md "wikilink")[全日空聯合營運](../Page/全日空.md "wikilink")，所以日本貨物航空的飛機塗裝，和全日空十分相似。到了2005年7月，全日空把手上的日本貨物航空股份售予日本郵船。同時，日本貨物航空的塗裝也開始統一起來。[1](http://www.mutantfrog.com/2005/10/21/japans-own-fedex-continuing-the-airspace-oligarchy/#more-691)

2010年起，因日本航空的波音747-400型飛機陸續退役，[日本航空自衛隊所操作的兩架](../Page/日本航空自衛隊.md "wikilink")[日本國政府專用機改由擁有同型機的日本貨物航空負責保養與維修工作](../Page/日本國政府專用機.md "wikilink")。但日本貨物航空計劃於2019年將旗下的747-400完全退役，日本政府已於2014年選擇採購波音777-300ER取代，預定於2019年度啟用新機\[1\]。

## 機隊資料

[Nca1.jpg](https://zh.wikipedia.org/wiki/File:Nca1.jpg "fig:Nca1.jpg")
[NCA_B747-8F.jpg](https://zh.wikipedia.org/wiki/File:NCA_B747-8F.jpg "fig:NCA_B747-8F.jpg")

截至2018年9月統計，日本貨物航空擁有1架[波音747-400貨機](../Page/波音747.md "wikilink")（原有8架，其中7架已退役）與8架[波音747-8貨機](../Page/波音747.md "wikilink")。

日本貨物航空也會是[波音747-8貨機的](../Page/波音747.md "wikilink")[首批客戶](../Page/啟始客戶.md "wikilink")，他們總共訂了8架這類型的航機；不過由於種種原因，結果首架量產機〔JA13KZ〕要到2012年8月初才得到正式交付。至2014年10月，已接收了8架747-8F。到2019年之前，機隊將簡化為全747-8F，現有的747-400F將逐年退役。

2008年3月28日，最後一架747-200F（[編號](../Page/航空器註冊編號.md "wikilink")：JA8181）退役\[2\]。

## 目的地

日本貨物航空定期營運由日本往亞洲其他地方的路線，或經[安克拉治往歐美地方](../Page/安克拉治.md "wikilink")。

### 亞洲

  - \-
    [曼谷](../Page/曼谷.md "wikilink")[蘇凡納布機場](../Page/蘇凡納布機場.md "wikilink")

  - \-
    [名古屋](../Page/名古屋.md "wikilink")[中部國際機場](../Page/中部國際機場.md "wikilink")

  - \-
    [大阪](../Page/大阪.md "wikilink")[關西國際機場](../Page/關西國際機場.md "wikilink")

  - \-
    [首爾](../Page/首爾.md "wikilink")[仁川國際機場](../Page/仁川國際機場.md "wikilink")

  - \-
    [上海](../Page/上海.md "wikilink")[浦東國際機場](../Page/浦東國際機場.md "wikilink")

  - \-
    [台北](../Page/台北.md "wikilink")[桃園國際機場](../Page/桃園國際機場.md "wikilink")

  - \- [香港國際機場](../Page/香港國際機場.md "wikilink")

  - \- [新加坡樟宜機場](../Page/新加坡樟宜機場.md "wikilink")

  - \-
    [東京](../Page/東京.md "wikilink")[成田國際機場](../Page/成田國際機場.md "wikilink")

### 歐洲

  - \-
    [阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[史基浦機場](../Page/阿姆斯特丹史基浦機場.md "wikilink")

  - \- [米蘭－馬爾彭薩機場](../Page/米蘭－馬爾彭薩機場.md "wikilink")

### 北美洲

  - \-
    [安克拉治](../Page/安克拉治.md "wikilink")[泰德·史蒂文斯國際機場](../Page/泰德·史蒂文斯安克雷奇國際機場.md "wikilink")

  - \-
    [芝加哥](../Page/芝加哥.md "wikilink")[奧黑爾國際機場](../Page/芝加哥奧黑爾國際機場.md "wikilink")

  - \-
    [達拉斯](../Page/達拉斯.md "wikilink")[達拉斯/沃斯堡國際機場](../Page/達拉斯-沃斯堡國際機場.md "wikilink")

  - \- [洛杉磯國際機場](../Page/洛杉磯國際機場.md "wikilink")

  - \-
    [紐約](../Page/紐約.md "wikilink")[甘迺迪國際機場](../Page/甘迺迪國際機場.md "wikilink")

  - \- [三藩市國際機場](../Page/三藩市國際機場.md "wikilink")

## 參考文獻

## 外部連結

  - [官方網站](http://www.nca.aero/)
  - [機隊資料](http://www.plane-spotters.net/Airline/Nippon-Cargo-Airlines)

[Category:日本航空公司](../Category/日本航空公司.md "wikilink")
[Category:貨運航空公司](../Category/貨運航空公司.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:日本郵船](../Category/日本郵船.md "wikilink")
[Category:1978年成立的航空公司](../Category/1978年成立的航空公司.md "wikilink")

1.
2.  <http://www.nca.aero/news/2008/news_20080331.html>