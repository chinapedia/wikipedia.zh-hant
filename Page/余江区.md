**余江区**是[中国](../Page/中国.md "wikilink")[江西省](../Page/江西省.md "wikilink")[鹰潭市的一个市辖区](../Page/鹰潭市.md "wikilink")。总面积为937平方公里，2003年人口为34.8万。

## 历史沿革

[晋](../Page/晋.md "wikilink")[元康元年](../Page/元康.md "wikilink")（291）置晋兴县，后改兴安县，不久撤县并入余汗县。南朝陈[元嘉年间设安仁县](../Page/元嘉.md "wikilink")。隋并入余干县。宋[端拱元年](../Page/端拱.md "wikilink")（988）又置安仁县。
民国3年（1914）易名余江县。

1949年后隶属贵溪专署领导，后又改隶上饶专署。1983年隶属鹰潭市。

2018年5月2日撤销余江县设立鹰潭市余江区。\[1\]\[2\]

## 行政区划

余江县辖黄庄、春涛、平定、杨溪、洪湖5乡，邓埠、锦江、画桥、潢溪、中童、马荃6镇，高公寨林场、水产场、大桥良种场、张公桥农场、塘潮源林场、青年农场、刘垦场7农垦场。共有23个居委会，116个行政村，1个管委会。
　　

## 自然地理

余江县位于江西省东北部，信江中下游。南北长达75公里，东西宽28.65公里，最狭蜂腰地段仅17.5公里。

## 历史名人

  - [汤汉](../Page/汤汉.md "wikilink")
  - [李存](../Page/李存.md "wikilink")
  - [桂萼](../Page/桂萼.md "wikilink")
  - [邹韬奋](../Page/邹韬奋.md "wikilink")
  - [吴迈](../Page/吴迈.md "wikilink")
  - [熊梦帷](../Page/熊梦帷.md "wikilink")

## 风景名胜

  - [马祖岩](../Page/马祖岩.md "wikilink")
  - [香炉峰](../Page/香炉峰.md "wikilink")
  - 天鹅湖
  - 洪五湖
  - 樟树谷
  - 马岗岭
  - 锦江古镇
  - 县衙门
  - 财神庙
  - 孔庙
  - 古代书院（龙溪书院和龙门书院）
  - 安仁城垣
  - [锦江天主教堂](../Page/锦江天主教堂.md "wikilink")（江西省文物保护单位）
  - [邹韬奋故居](../Page/邹韬奋故居.md "wikilink")

## 外部链接

  - [余江县人民政府网站](https://web.archive.org/web/20080617195518/http://www.yujiang.gov.cn/yjzf/Default.htm)
  - [鹰潭市余江县简介](https://web.archive.org/web/20060519113410/http://www.yingtan.gov.cn/node2/node173/userobject6ai1026.html)

[余江县](../Page/category:余江县.md "wikilink")
[县](../Page/category:鹰潭区县市.md "wikilink")
[鹰潭](../Page/category:江西省县份.md "wikilink")

1.
2.