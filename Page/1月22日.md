## 大事记

### 8世紀

  - [754年](../Page/754年.md "wikilink")：[鉴真东渡到达](../Page/鉴真.md "wikilink")[日本](../Page/日本.md "wikilink")。

### 16世紀

  - [1506年](../Page/1506年.md "wikilink")：受[教宗](../Page/教宗.md "wikilink")[儒略二世的请求](../Page/儒略二世.md "wikilink")，首支150名士兵组成的[瑞士护卫队进驻](../Page/瑞士护卫队.md "wikilink")[梵蒂冈](../Page/梵蒂冈.md "wikilink")。

### 19世紀

  - [1879年](../Page/1879年.md "wikilink")：[祖鲁王国的军队在](../Page/祖鲁王国.md "wikilink")[伊散德尔瓦纳战役中击败](../Page/伊散德尔瓦纳战役.md "wikilink")[英国军队](../Page/英国军队.md "wikilink")。

### 20世紀

  - [1901年](../Page/1901年.md "wikilink")：[英国](../Page/英国.md "wikilink")[维多利亚女王去世](../Page/维多利亚女王.md "wikilink")，其长子[爱德华七世继承王位](../Page/爱德华七世_\(英国\).md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：
    [镇压反革命运动](../Page/镇压反革命运动.md "wikilink")：毛泽东电告[中共中央华南分局广东方面负责人称](../Page/中共中央华南分局.md "wikilink")：“你们已杀了3700多，这很好，再杀三四千人”，“今年可以杀八九千人为目标”。\[1\]
  - [1973年](../Page/1973年.md "wikilink")：[冰島](../Page/冰島.md "wikilink")[赫馬島](../Page/赫馬島.md "wikilink")[火山爆發](../Page/火山.md "wikilink")。<ref>[1](http://www.geo.mtu.edu/volcanoes/boris/mirror/mirrored_html/Heimaey.html)，Heimaey
    island, Vestmannaeyjar, Iceland

The 1973 eruption。</ref>

  - [1973年](../Page/1973年.md "wikilink")：[美国最高法院对](../Page/美国最高法院.md "wikilink")[罗诉韦德案进行裁决](../Page/罗诉韦德案.md "wikilink")，确认妇女决定是否继续[妊娠的权力受](../Page/妊娠.md "wikilink")[宪法保护](../Page/美国宪法.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[苏联](../Page/苏联.md "wikilink")[原子物理学家](../Page/原子物理学.md "wikilink")、[诺贝尔和平奖得主](../Page/诺贝尔和平奖.md "wikilink")[萨哈罗夫因示威抗议](../Page/安德烈·德米特里耶维奇·萨哈罗夫.md "wikilink")[苏联入侵阿富汗而被捕并被流放到](../Page/阿富汗战争_\(1979年\).md "wikilink")[高尔基市](../Page/高尔基市.md "wikilink")。
  - [1996年](../Page/1996年.md "wikilink")：[黎巴嫩](../Page/黎巴嫩.md "wikilink")[貝魯特股票交易市場重開](../Page/貝魯特.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：[纳粹大屠殺倖存者與](../Page/纳粹.md "wikilink")[瑞士一些](../Page/瑞士.md "wikilink")[銀行達成協議](../Page/銀行.md "wikilink")，銀行支付12.5億[美元](../Page/美元.md "wikilink")，作为戰後未歸還大屠殺時期資產的賠償。

### 21世紀

  - [2011年](../Page/2011年.md "wikilink")：[日本](../Page/日本.md "wikilink")[HTV-2货运飞船发射](../Page/H-II運載飛船.md "wikilink")。
  - [2012年大陸動漫熊出没播出](../Page/2012年.md "wikilink")。

## 出生

  - [1561年](../Page/1561年.md "wikilink")：[弗兰西斯·培根](../Page/弗兰西斯·培根.md "wikilink")，[英國](../Page/英國.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")（逝於[1626年](../Page/1626年.md "wikilink")）
  - [1729年](../Page/1729年.md "wikilink")：[戈特霍尔德·埃夫莱姆·莱辛](../Page/戈特霍尔德·埃夫莱姆·莱辛.md "wikilink")，[德國劇作家](../Page/德國.md "wikilink")（逝於[1781年](../Page/1781年.md "wikilink")）
  - [1737年](../Page/1737年.md "wikilink")：[劉躍雲](../Page/劉躍雲.md "wikilink")，[清朝政治人物](../Page/清朝.md "wikilink")（逝於[1808年](../Page/1808年.md "wikilink")）
  - [1788年](../Page/1788年.md "wikilink")：[拜伦](../Page/拜伦.md "wikilink")，英國[詩人](../Page/詩人.md "wikilink")（逝於[1824年](../Page/1824年.md "wikilink")）
  - [1858年](../Page/1858年.md "wikilink")：[盧吉](../Page/盧吉.md "wikilink")，[香港](../Page/香港.md "wikilink")[總督](../Page/香港總督.md "wikilink")（逝於[1945年](../Page/1945年.md "wikilink")）
  - [1865年](../Page/1865年.md "wikilink")：[弗裡德里希·帕邢](../Page/弗裡德里希·帕邢.md "wikilink")，德國物理學家（逝於[1947年](../Page/1947年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[大衛·格里菲斯](../Page/大衛·格里菲斯.md "wikilink")，[美國導演](../Page/美國.md "wikilink")（逝於[1948年](../Page/1948年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[谢尔盖·米哈依洛维奇·爱森斯坦](../Page/谢尔盖·米哈依洛维奇·爱森斯坦.md "wikilink")，電影理論家（逝於[1948年](../Page/1948年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[列夫·朗道](../Page/列夫·朗道.md "wikilink")，[苏联](../Page/苏联.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")（逝於[1968年](../Page/1968年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[張奧偉](../Page/張奧偉.md "wikilink")，香港政界人物（逝於[2003年](../Page/2003年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[余英時](../Page/余英時.md "wikilink")，[中國](../Page/中國.md "wikilink")[歷史學家](../Page/歷史學家.md "wikilink")
  - [1931年](../Page/1931年.md "wikilink")：[聖嚴法師](../Page/聖嚴法師.md "wikilink")，[台灣佛教法师](../Page/台灣.md "wikilink")（逝於[2009年](../Page/2009年.md "wikilink")）
  - [1946年](../Page/1946年.md "wikilink")：[張成澤](../Page/張成澤.md "wikilink")，曾任[朝鮮黨政軍多項要職](../Page/朝鮮.md "wikilink")，現任領導人[金正恩的](../Page/金正恩.md "wikilink")[姑丈](../Page/姑丈.md "wikilink")，遭[處決](../Page/處決.md "wikilink")（逝於[2013年](../Page/2013年.md "wikilink")）
  - [1949年](../Page/1949年.md "wikilink")：[陳鑑林](../Page/陳鑑林.md "wikilink")，香港政界人物
  - [1951年](../Page/1951年.md "wikilink")：[能條純一](../Page/能條純一.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")
  - [1953年](../Page/1953年.md "wikilink")：[吉姆·賈木許](../Page/吉姆·賈木許.md "wikilink")，美國[電影導演](../Page/電影導演.md "wikilink")
  - [1965年](../Page/1965年.md "wikilink")：[黃德森](../Page/黃德森.md "wikilink")，香港[滑浪風帆運動員](../Page/滑浪風帆.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[弗兰克·勒伯夫](../Page/弗兰克·勒伯夫.md "wikilink")，[法國](../Page/法國.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[森江博](../Page/Heath.md "wikilink")，日本貝斯手
  - [1972年](../Page/1972年.md "wikilink")：[朴璐美](../Page/朴璐美.md "wikilink")，日本女性[聲優](../Page/聲優.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[約瑟夫·穆斯卡特](../Page/約瑟夫·穆斯卡特.md "wikilink")，[馬耳他政治家](../Page/馬耳他.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[中田英壽](../Page/中田英壽.md "wikilink")，日本足球運動員
  - [1979年](../Page/1979年.md "wikilink")：[陳致中](../Page/陳致中.md "wikilink")，前中華民國總統陳水扁長子，曾任[高雄市議會第](../Page/高雄市議會.md "wikilink")1屆議員，現任高雄市議會第3屆議員
  - [1979年](../Page/1979年.md "wikilink")：[林威助](../Page/林威助.md "wikilink")，台灣旅日棒球選手
  - [1981年](../Page/1981年.md "wikilink")：[柯有綸](../Page/柯有綸.md "wikilink")，台灣歌手
  - [1983年](../Page/1983年.md "wikilink")：[陽耀勳](../Page/陽耀勳.md "wikilink")，台灣旅日棒球選手
  - [1986年](../Page/1986年.md "wikilink")：[李國毅](../Page/李國毅.md "wikilink")，台灣演員
  - [1987年](../Page/1987年.md "wikilink")：[原田明繪](../Page/原田明繪.md "wikilink")，日本[AV女優](../Page/AV女優.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[蕭亮](../Page/蕭亮_\(足球運動員\).md "wikilink")，[香港](../Page/香港.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[新內真衣](../Page/新內真衣.md "wikilink")，日本女子偶像團[乃木坂46成員](../Page/乃木坂46.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[劉浩霖](../Page/劉浩霖.md "wikilink")，[香港](../Page/香港.md "wikilink")[足球運動員](../Page/足球.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[永瀨匡](../Page/永瀨匡.md "wikilink")，日本演員
  - [1997年](../Page/1997年.md "wikilink")：[生田繪梨花](../Page/生田繪梨花.md "wikilink")，日本女子偶像团体[乃木坂46成員](../Page/乃木坂46.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[李濬榮](../Page/李濬榮.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[男子團體](../Page/男子團體.md "wikilink")[U-KISS](../Page/U-KISS.md "wikilink")、[UNB成員](../Page/UNB.md "wikilink")
  - [青山由香里](../Page/青山由香里.md "wikilink")，日本女性聲優
  - 1999年：[柳貞安](../Page/데이지_\(가수\).md "wikilink")，韓國女子團體[Momoland成員](../Page/MOMOLAND.md "wikilink")

## 逝世

  - [906年](../Page/906年.md "wikilink")：[何太后](../Page/积善太后.md "wikilink")，[唐朝](../Page/唐朝.md "wikilink")[皇太后](../Page/皇太后.md "wikilink")
  - [1170年](../Page/1170年.md "wikilink")：[王重阳](../Page/王重阳.md "wikilink")，[道教](../Page/道教.md "wikilink")[全真派的创始人](../Page/全真派.md "wikilink")（[1113年出生](../Page/1113年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[維多利亞](../Page/維多利亞_\(英國君主\).md "wikilink")，[英國女王](../Page/英國女王.md "wikilink")、[印度女皇](../Page/印度女皇.md "wikilink")（[1819年出生](../Page/1819年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[卡爾·拉森](../Page/卡爾·拉森.md "wikilink")，[瑞典畫家](../Page/瑞典.md "wikilink")（[1853年出生](../Page/1853年.md "wikilink")）
  - [1942年](../Page/1942年.md "wikilink")：[萧红](../Page/萧红.md "wikilink")，中国女作家（[1911年出生](../Page/1911年.md "wikilink")）
  - [1945年](../Page/1945年.md "wikilink")：[盧吉](../Page/盧吉.md "wikilink")，[香港](../Page/香港.md "wikilink")[總督](../Page/香港總督.md "wikilink")（[1858年出生](../Page/1858年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[蕭蔚雲](../Page/蕭蔚雲.md "wikilink")，中國法學家、前[香港基本法起草委員會委員](../Page/香港基本法起草委員會.md "wikilink")（[1924年出生](../Page/1924年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[希斯·萊傑](../Page/希斯·萊傑.md "wikilink")，[澳洲電影演員](../Page/澳洲電影.md "wikilink")（[1979年出生](../Page/1979年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[鍾錦](../Page/鍾錦.md "wikilink")，香港企業家、飲食界人士（[1942年出生](../Page/1942年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[梁羽生](../Page/梁羽生.md "wikilink")，[中國著名](../Page/中國.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")[作家](../Page/作家.md "wikilink")（[1924年出生](../Page/1924年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[黃龍](../Page/黃龍.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[歌仔戲](../Page/歌仔戲.md "wikilink")[演員](../Page/演員.md "wikilink")（[1951年出生](../Page/1951年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[溫德爾·漢普頓·福特](../Page/溫德爾·H·福特.md "wikilink")，[美國](../Page/美國.md "wikilink")[肯塔基州已故](../Page/肯塔基州.md "wikilink")[政治家](../Page/政治家.md "wikilink")（[1924年出生](../Page/1924年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[陳玉梅](../Page/陳玉梅_\(政治人物\).md "wikilink")，[台灣臺北市議會第](../Page/台灣.md "wikilink")7至第11屆議員([1966年出生](../Page/1966年.md "wikilink"))

## 节假日和习俗

  - [2012年](../Page/2012年.md "wikilink")[农历](../Page/农历.md "wikilink")[除夕](../Page/除夕.md "wikilink")

## 參考資料

1.