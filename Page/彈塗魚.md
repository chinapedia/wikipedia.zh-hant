**彈塗魚**是**弹涂鱼族**（[学名](../Page/学名.md "wikilink")：）鱼类的通称，属[虾虎鱼目](../Page/虾虎鱼目.md "wikilink")[背眼虾虎鱼科](../Page/背眼虾虎鱼科.md "wikilink")[背眼虾虎鱼亚科](../Page/背眼虾虎鱼亚科.md "wikilink")，大多是兩棲魚類。

常見彈塗魚包括[廣東彈塗魚](../Page/廣東彈塗魚.md "wikilink")（**），又名**泥猴**、**石貼仔**、**跳跳魚**，身長可達10厘米；另一種稱為[薄氏大彈塗魚](../Page/薄氏大彈塗魚.md "wikilink")（*Boleophthalmus
boddarti*），又稱大彈塗魚，成長後身長約20厘米；牠不及廣東彈塗魚那樣適應陸上生活，牠只能停留在地面上很短時間。

它们全身似泥澤色調的灰褐色，並佈滿深色的斑紋，特大的胸鰭肉質化，適於泥澤的爬行，身體修長，尾部扁平。

常見於[香港](../Page/香港.md "wikilink")、[台灣和](../Page/台灣.md "wikilink")[東南亞等地的](../Page/東南亞.md "wikilink")[紅樹林](../Page/紅樹林.md "wikilink")[濕地地區](../Page/濕地.md "wikilink")。

## 习性

彈塗魚能用濕潤的[皮膚和](../Page/皮膚.md "wikilink")[鰓室中的水分呼吸](../Page/鰓室.md "wikilink")，能夠適應半水半陸的潮間帶環境，棲息於河口水域、[沼澤及泥灘](../Page/沼澤.md "wikilink")，在泥下建造由兩條垂直地道構成的洞內居住。在繁殖季節時，雄性的彈塗魚會豎起背鰭，吸引雌魚，成功後會鑽入地道。

例如，[廣東彈塗魚以](../Page/廣東彈塗魚.md "wikilink")[螃蟹](../Page/螃蟹.md "wikilink")、[昆蟲為主食](../Page/昆蟲.md "wikilink")，多留守於近岸紅樹林內，其體型較小，滿布深色斑紋，會依附在石頭或紅樹樹幹等垂直面上；回到水中，彈塗魚亦可像魚類遊動自如。一般漲滿潮至剛退潮之間是觀察良時。

[大彈塗魚以泥灘上的藻類和碎屑為主食](../Page/大彈塗魚.md "wikilink")，體型較大，身上有螢光藍點，大彈塗的胸鰭在中間接合及前彎，有如枴杖，有助彈跳或爬行；當潮退後，大彈塗魚會其從洞內爬出。

## 分类

### 系统发生学

[背眼虾虎鱼亚科各](../Page/背眼虾虎鱼亚科.md "wikilink")[属的](../Page/属.md "wikilink")[演化关系如下](../Page/演化.md "wikilink")\[1\]：

## 相關新聞報道

[Mudskipper.jpg](https://zh.wikipedia.org/wiki/File:Mudskipper.jpg "fig:Mudskipper.jpg")
2007年3月，廣東發生多人進食雲斑櫛蝦虎魚而發生中毒事件，該含[河魨毒素的魚種和彈塗魚外形相似](../Page/河魨毒素.md "wikilink")，因此曾被錯認為誤食含[雪卡魚毒的彈塗魚而中毒](../Page/雪卡魚毒.md "wikilink")。

2007年11月15日[世界自然基金會](../Page/世界自然基金會.md "wikilink")[香港分會公佈](../Page/香港.md "wikilink")「我最喜愛海洋10寶」公眾網上投票結果，此投票為期4個月選出最受歡迎十種本地海洋生物。彈塗魚得二百六十九票榮獲「我最喜愛海洋10寶」第五位。\[2\]\[3\]\[4\]

## 參看

  - [肺魚](../Page/肺魚.md "wikilink")

## 參考文献

  - [彈塗魚介紹](http://www.hknature.net/chi/ecology/mudskipper.html)
    香港政府漁農護理署　香港自然網
  - [海洋十寶:彈塗魚](https://web.archive.org/web/20070106062021/http://www.wwf.org.hk/oceans10/chi/ocean10/mudskipper/)
    世界自然（香港）基金會

## 扩展阅读

[\*](../Category/弹涂鱼族.md "wikilink")
[P](../Category/香港原生物種.md "wikilink")
[P](../Category/中國魚類.md "wikilink")
[P](../Category/虾虎鱼科.md "wikilink")

1.
2.  [世界自然基金會揭示: 綠海龜、馬蹄蟹和黃唇魚徘徊生存警界線 香港市民選出「我最喜愛的海洋十寶」
    (2007年11月15日)](http://www.wwf.org.hk/chi/pressreleases/20071115.php)
     世界自然基金會新聞稿，2007年11月15日。

3.  [瀕危海洋生物面臨絕種
    團體促政府推行全面保護措施](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20071116&sec_id=4104&subsec_id=11867&art_id=10426143)
    《[蘋果日報 (香港)](../Page/蘋果日報_\(香港\).md "wikilink")》2007年11月16日A14版。

4.  \[ 調查顯示:中華白海豚是港人最喜愛的海洋生物\]新浪新聞
    原文載於《[新華網](../Page/新華網.md "wikilink")》2007年11月16日