**查理·喬治·戈登**（，1833年1月28日－1885年1月26日），[英國陸軍](../Page/英國陸軍.md "wikilink")[少將](../Page/少將.md "wikilink")。因在[中国指挥雇佣](../Page/中国.md "wikilink")“[常勝軍](../Page/常勝軍.md "wikilink")”协助[李鸿章及](../Page/李鸿章.md "wikilink")[刘铭传](../Page/刘铭传.md "wikilink")[淮军與](../Page/淮军.md "wikilink")[太平軍作战](../Page/太平軍.md "wikilink")，获得[兩宮太后封為](../Page/兩宮太后.md "wikilink")[提督](../Page/提督.md "wikilink")、赏穿[黃馬褂而得到](../Page/黃馬褂.md "wikilink")“[中国人戈登](../Page/中国人.md "wikilink")”（Chinese
Gordon）之[綽號](../Page/綽號.md "wikilink")。英國賜之「[巴斯勳章](../Page/巴斯勳章.md "wikilink")」，後將其調至[蘇丹任](../Page/蘇丹.md "wikilink")[總督](../Page/總督.md "wikilink")，人稱「戈登[帕夏](../Page/帕夏.md "wikilink")」\[1\]，最後於任內爆發的[馬赫迪戰爭中](../Page/馬赫迪戰爭.md "wikilink")[陣亡](../Page/陣亡.md "wikilink")。

## 早年生活

戈登出生在[倫敦的伍爾維奇](../Page/倫敦.md "wikilink")，是皇家砲兵（Royal Artillery）的H. W.
Gordon將軍的第4個兒子。他在陶頓學校（Taunton
School）受教育，後來在1848年進入[皇家軍事學院](../Page/皇家軍事學院.md "wikilink")（Royal
Military Academy）。1852年，他任官為皇家工兵（Royal
Engineers）少尉，在查塔姆的皇家工兵學校繼續完成他的訓練，並在1854年晉升到中尉。

一開始他奉派建造威爾斯的[米爾福德港的防禦工事](../Page/米爾福德港.md "wikilink")。但由於[克里米亞戰爭的爆發](../Page/克里米亞戰爭.md "wikilink")，他在1855年1月被指派到巴拉克拉瓦（Balaklava，現屬烏克蘭）。6月18日到9月8日他參與了對[塞瓦斯托波爾的攻擊](../Page/塞瓦斯托波爾.md "wikilink")。\[2\]

另外他也參與了英國與[帝俄的金伯恩战役](../Page/帝俄.md "wikilink")，並在衝突結束後回到塞瓦斯托波爾。戈登伴隨一個國際和平委員會劃定[俄國和](../Page/俄國.md "wikilink")[土耳其在](../Page/土耳其.md "wikilink")[比薩拉比亞的新國界](../Page/比薩拉比亞.md "wikilink")。他繼續他在[小亞細亞的調查工作](../Page/小亞細亞.md "wikilink")，並在1858年底回到英國，出任工兵學校教師，而且在1859年晉升為上尉。\[3\]

## 中國

1860年，[第二次鴉片戰爭爆發](../Page/第二次鴉片戰爭.md "wikilink")，戈登随[英法联军来到](../Page/英法联军.md "wikilink")[中国](../Page/中国.md "wikilink")，\[4\]在9月抵達[天津](../Page/天津.md "wikilink")，其后参与了佔領[北京与](../Page/北京.md "wikilink")[火燒圆明园的行動](../Page/火燒圆明园.md "wikilink")。10月《[北京条约](../Page/北京条约.md "wikilink")》签订，增加天津为通商口岸，并开辟英法美租界。天津英租界首先开辟，时任[工兵上尉的戈登是划定界限和规划道路的主要设计者](../Page/工兵.md "wikilink")。

1850年起，[太平天国在](../Page/太平天国.md "wikilink")[兩廣](../Page/兩廣.md "wikilink")、[湖廣等省连战皆捷](../Page/湖廣.md "wikilink")，並于1853年佔領[金陵](../Page/金陵.md "wikilink")（今日[江蘇](../Page/江蘇.md "wikilink")[南京](../Page/南京.md "wikilink")），改稱[天京並定都於此](../Page/天京.md "wikilink")。清朝[蘇松太道](../Page/蘇松太道.md "wikilink")[道尹](../Page/道尹.md "wikilink")[吳煦出面](../Page/吳煦.md "wikilink")，富商[楊坊出資](../Page/楊坊.md "wikilink")，居于上海的[歐美移民也協助成立洋枪队以防御城池](../Page/歐美.md "wikilink")。洋枪队由美國人[華飛烈指揮](../Page/華飛烈.md "wikilink")，在上海西郊与太平军多次發生零星战斗，情势逐渐不利。1862年英將[士迪佛立率军抵达](../Page/士迪佛立.md "wikilink")，配合華飛烈和另一支小型法軍抵抗太平軍势力，1862年底夺回原本被太平軍佔領的[嘉定](../Page/嘉定.md "wikilink")、[青浦等地](../Page/青浦.md "wikilink")，同年也改名為[常勝軍](../Page/常勝軍.md "wikilink")。\[5\]\[6\]

[華飛烈](../Page/華飛烈.md "wikilink")[陣亡于](../Page/陣亡.md "wikilink")[慈溪之战](../Page/慈溪.md "wikilink")，由[美國人](../Page/美國人.md "wikilink")[白聚文繼任](../Page/白聚文.md "wikilink")，但因劫掠清军餉四萬[銀元](../Page/銀元.md "wikilink")，被撤职。[江蘇](../Page/江蘇.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")[李鴻章要求士迪佛立另指派英國軍官來指揮洋槍隊](../Page/李鴻章.md "wikilink")。士迪佛立選擇了时任[少校的戈登](../Page/少校.md "wikilink")。1863年3月，戈登在[松江接任指揮](../Page/松江.md "wikilink")，与華飛烈不同的是，戈登严禁[常勝軍士兵劫掠](../Page/常勝軍.md "wikilink")，在军中採以严刑峻法，軍紀嚴明。戈登重整軍隊並支援[常熟成功](../Page/常熟.md "wikilink")，很快得到士兵們的尊敬。李鸿章的淮军与戈登率领的洋枪队密切配合，在江苏的战场上频频得手。戈登得知守将纳王[郜永宽等与主帅](../Page/郜永宽.md "wikilink")[谭绍光不和](../Page/谭绍光.md "wikilink")，有投降之意，便与李鸿章商量诱降郜永宽，李鴻章命部將[程學啟與郜永宽定下降約](../Page/程學啟.md "wikilink")，由戈登居中做[保人](../Page/保人.md "wikilink")。

11月，[蘇州被常胜军和李鸿章的](../Page/蘇州.md "wikilink")[淮军合力攻下](../Page/淮军.md "wikilink")。1864年5月，太平軍在天京（今[南京](../Page/南京.md "wikilink")）外圍最後一個堡壘[常州府失陷](../Page/常州.md "wikilink")，[常勝軍的聲望也達到最高峰](../Page/常勝軍.md "wikilink")。同治二年十月二十六日（1863年12月5日），郜永宽、伍贵文、汪安均等八名太平军降将到淮军营中见李鸿章，程学启率百余人突入午宴，将八名降将杀死。程学启提着八人首级入城，称“八人反侧，已[伏诛矣](../Page/伏诛.md "wikilink")！”戈登對殺降一事不滿，痛罵程學啟不講道義，与之断交，又提着[火枪要找李鸿章算帐](../Page/火枪.md "wikilink")，逼得李鸿章四处躲藏。由於找不到李鸿章，戈登發出了[最後通牒](../Page/最後通牒.md "wikilink")，要求李鸿章下台，否则他就率“常胜军”进攻[淮军](../Page/淮军.md "wikilink")。后他率军返回[昆山](../Page/昆山.md "wikilink")，同時要求英國駐華公使[卜魯斯](../Page/卜魯斯.md "wikilink")，希望[英國政府能夠協助](../Page/英國政府.md "wikilink")，迫使李鴻章下台。李鸿章急忙向英国人[赫德和](../Page/赫德.md "wikilink")[馬格里](../Page/馬格里.md "wikilink")（郜永寬的女婿）求援，请他们代为调解；李鸿章在给朝廷的《骈诛八降酋片》中说：“惟洋人性情反覆，罔知事体，如臣构昧，恐难驾驭合宜。设英公使与总理衙门过于争执，惟有请旨将臣严议治罪以折服其心。”[曾国藩竟然称赞这种行径说](../Page/曾国藩.md "wikilink")：“此间近事，惟李少荃在苏州杀降王八人最快人意”，“殊为眼明手辣”。\[7\]

1864年，與李鴻章合作攻陷[常州之後](../Page/常州.md "wikilink")，常勝軍解散，李鴻章奏請[兩宮太后](../Page/兩宮太后.md "wikilink")，授與戈登清代最高武职[提督稱號](../Page/提督.md "wikilink")、賞穿[黃馬褂](../Page/黃馬褂.md "wikilink")；另外，英國也晉升他為[中校並賜](../Page/中校.md "wikilink")“[巴斯勳章](../Page/巴斯勳章.md "wikilink")”。

## 非洲

戰後，戈登短暫回到英國，並負責指揮皇家工兵在[泰晤士河南岸](../Page/泰晤士河.md "wikilink")[格雷夫森附近的防禦要塞](../Page/格雷夫森德_\(肯特郡\).md "wikilink")。1871年10月，戈登代表英國到**多瑙河委員會**去維持[多瑙河河口的](../Page/多瑙河.md "wikilink")[加拉茨的船隻航行](../Page/加拉茨.md "wikilink")。

1872年戈登被派去[克里米亞檢閱當地的英軍公墓](../Page/克里米亞.md "wikilink")，當他經過[伊斯坦堡時](../Page/伊斯坦堡.md "wikilink")，認識了當時的[埃及總理](../Page/埃及總理.md "wikilink")[努巴尔帕夏](../Page/努巴尔帕夏.md "wikilink")，他和戈登商議在[土耳其駐埃及總督](../Page/土耳其.md "wikilink")[伊斯梅尔帕夏手下工作](../Page/伊斯梅尔帕夏.md "wikilink")。1873年，戈登在[英國政府的同意下接受了總督的邀請](../Page/英國政府.md "wikilink")，並在1874年初到達[埃及](../Page/埃及.md "wikilink")，戈登在那裏成為埃及軍的上校。

埃及政府從1820年代開始往南擴張，並在英國探險家[山繆·貝克](../Page/山繆·貝克.md "wikilink")（Samuel
Baker）的領導下，在1870年2月到達[尼羅河上游的](../Page/尼羅河.md "wikilink")[喀土穆](../Page/喀土穆.md "wikilink")，1871年6月到達[剛多卡洛](../Page/剛多卡洛.md "wikilink")。貝克在當地遭遇了極大的困難，所以總督要求戈登去處理當地事務。在[開羅待一小段時間後](../Page/開羅.md "wikilink")，戈登經由[薩瓦金](../Page/薩瓦金.md "wikilink")（[蘇丹海港](../Page/苏丹共和国.md "wikilink")）和[柏柏爾](../Page/柏柏爾.md "wikilink")（蘇丹北部城鎮）到喀土穆，從喀土穆他又繼續沿著[白尼羅河到了剛多卡洛](../Page/白尼羅河.md "wikilink")。

戈登留在[剛多卡洛省直到](../Page/剛多卡洛省.md "wikilink")1876年10月。他建立從[索伯特河](../Page/索伯特河.md "wikilink")（Sobat）到[烏干達前線的哨站](../Page/烏干達.md "wikilink")，以及打擊奴隸交易活動。\[8\]然而戈登開始和埃及官員和[蘇丹發生衝突](../Page/蘇丹_\(稱謂\).md "wikilink")，這導致戈登回到倫敦並通知總督他不會回到蘇丹。總督寫信給他，戈登同意回到埃及，但堅持他必須成為全蘇丹的總督。在總督同意一些討論後，戈登成為全蘇丹的總督。

成為總督後，戈登開始處理一些更廣的問題。其中一個是改善埃及和[阿比西尼亞](../Page/衣索比亞帝國.md "wikilink")（今[衣索匹亞](../Page/衣索匹亞.md "wikilink")）的關係，主要是對Bogos地區的爭議。戰爭在1875年爆發，埃及軍隊被擊敗，於是戈登到Bogos與阿比西尼亞談判，不過並沒有得到回復。不久，Darfur又爆發起義，叛軍相當龐大，而戈登認為外交途徑是比較好的解決方法。這次戈登只帶一名翻譯員到敵營談判。這個大膽的行動被認為是成功的，一些叛軍加入他，另一些則撤回南方。1878年3月戈登被召到[開羅](../Page/開羅.md "wikilink")，他被指派為委員會主席。[埃及總督伊斯梅爾在](../Page/埃及總督.md "wikilink")1879年辭職，並把位子傳給他的兒子。

戈登回到南方，到阿比西尼亞南方的Harrar，他發現當地官僚相當腐敗，於是他將當地官員免職。然後他又繼續打擊Darfur的奴隸交易活動。他的部屬Gessi
Pasha成功結束Bahr-el-Ghazal地區的暴動。他又試著與阿比西尼亞簽訂和約，不過沒有成功。他回到[開羅並辭去總督的職務](../Page/開羅.md "wikilink")，他對於經年累月繁雜的工作感到筋疲力盡。

1880年戈登在[布魯塞爾拜訪](../Page/布魯塞爾.md "wikilink")[比利時國王](../Page/比利時國王.md "wikilink")[利奧波德二世](../Page/利奧波德二世_\(比利時\).md "wikilink")，並應邀幫忙負責[剛果自由邦的事務](../Page/剛果自由邦.md "wikilink")。4月，[好望角殖民政府提供他一個地方軍隊的指揮職務](../Page/好望角.md "wikilink")。5月，即將到[印度擔任總督的Ripon侯爵要求戈登當他的私人祕書](../Page/印度.md "wikilink")。戈登同意最後的這個請求，但在他到印度不久便辭職。辞职后，他在7月再度来到中國北京，和這時已是[直隶总督兼](../Page/直隶总督.md "wikilink")[北洋大臣的李鴻章會面](../Page/北洋.md "wikilink")，李告訴他中國和俄國因为[伊犁事件有戰爭的危機](../Page/伊犁.md "wikilink")，於是戈登便用他的影響力來確保和平。戈登回到英國，但在1881年4月又離開到[模里西斯指揮當地的皇家工兵](../Page/模里西斯.md "wikilink")。他留在模里西斯直到1882年3月，當他晉升為[少將時](../Page/少將.md "wikilink")。調派到好望角去增援[巴苏陀兰](../Page/巴苏陀兰.md "wikilink")（今[賴索托](../Page/賴索托.md "wikilink")）。數月後，他回到英國。不久後，戈登又前往[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")，一個他一直渴望拜訪的地方，並在那裡待一年。[比利時](../Page/比利時.md "wikilink")[國王再次邀請他管理剛果自由邦的事務](../Page/國王.md "wikilink")，他接受並且回到倫敦準備。但在他到達英國不久，英國政府要求他立刻去蘇丹，那裡的情況在他離開後變得相當糟糕，[穆罕默德·艾哈邁德·馬赫迪自稱是](../Page/穆罕默德·艾哈邁德·馬赫迪.md "wikilink")[馬赫迪](../Page/馬赫迪.md "wikilink")（「導師」），[馬赫迪戰爭在他的領導下爆發](../Page/馬赫迪戰爭.md "wikilink")。

蘇丹的埃及軍隊無法抵擋這場暴動，埃及政府則正忙於陣壓另一場暴動。1882年9月後，蘇丹處於一個瀕臨毀滅的位置。1883年12月，英國政府命令埃及放棄蘇丹，但撤離行動是困難的，數千名埃及士兵、平民和他們的家眷仍然困在蘇丹。英國政府於是要求戈登前往喀土穆處理並撤離人員。

1884年1月，戈登到達開羅；在開羅，他再度出任為總督，踏上前往喀土穆的不歸路。2月18日，他抵達喀土穆。戈登開始著手遣返婦孺和傷者回到埃及，約2500人在救主軍封鎖道路前被撤離。戈登希望有影響力的地方領導人Zobeir能接管蘇丹，不過英國政府以他曾經是奴隸贩卖商的身份拒絕。

叛軍在喀土穆和蘇丹東部附近不斷進攻，而薩瓦金的埃及軍隊則不斷的被擊敗。一支由[葛雷漢](../Page/葛雷漢.md "wikilink")（Gerald
Graham）將軍的英國部隊曾派遣到薩瓦金，並迫使叛軍撤離當地。戈登曾強烈主張從薩瓦金到柏柏爾的道路應重新開放，不過這要求被倫敦政府所回絕。4月，葛雷漢將軍和他的軍隊撤離。5月，柏柏爾的守軍投降，喀土穆被完全隔離。

1884年3月18日，[喀土穆圍城戰開始](../Page/喀土穆圍城戰.md "wikilink")。雖然英國政府決定放棄蘇丹，但民間仍有許多人呼籲派兵拯救戈登。直到8月英國政府才決定拯救戈登，但直到11月英國救援隊才準備開始行動。

12月底，部隊到達Korti。1885年1月20日，他們到達Metemma，在那裡他們發現戈登四個月前派來的砲艇，準備用來把他們運往尼羅河上遊。24日，兩艘汽船出發前往喀土穆，但當他們在28日抵達時，發現喀土穆已經淪陷，戈登已戰死兩天，後遭敵人[斬首](../Page/斬首.md "wikilink")，\[9\]並且[梟首示眾](../Page/梟首.md "wikilink")。

## 影響

戈登的死讓當時的[英國首相](../Page/英國首相.md "wikilink")[威廉·尤尔特·格莱斯顿被逼下台](../Page/威廉·尤尔特·格莱斯顿.md "wikilink")，並導致[自由黨政府垮台](../Page/英國自由黨.md "wikilink")，開始[保守黨的長期執政](../Page/保守黨_\(英國\).md "wikilink")。另一方面，增加英國吞併蘇丹的決心；1898年，英軍擊敗[救主軍](../Page/救主軍.md "wikilink")，蘇丹從此成為[英國与埃及共治的殖民地](../Page/英埃蘇丹.md "wikilink")，直到1956年獨立。

1890年，[天津英租界的](../Page/天津英租界.md "wikilink")[工部局大楼落成](../Page/工部局.md "wikilink")，为了纪念戈登在开辟和规划该租界方面的贡献，这座大楼命名为[戈登堂](../Page/戈登堂.md "wikilink")。在落成典礼上，戈登的巨幅照片挂在会场的中央。[直隶总督兼](../Page/直隶总督.md "wikilink")[北洋大臣](../Page/北洋大臣.md "wikilink")[李鸿章在盛赞戈登的军事指挥才能后](../Page/李鸿章.md "wikilink")，宣布大楼正式开放。\[10\]1897年[天津英租界第一次扩充后](../Page/天津英租界.md "wikilink")，将新筑的一条马路命名为戈登道（今湖北路）。\[11\]

在戈登曾经活动过的上海，1900年，[上海公共租界也将西区新筑的一条重要道路命名为](../Page/上海公共租界.md "wikilink")[戈登路](../Page/戈登路.md "wikilink")（Gordon
Road，今[江宁路](../Page/江宁路_\(上海\).md "wikilink")）。\[12\]\[13\]

2010年，[天津市在海河南岸重建](../Page/天津市.md "wikilink")[戈登堂](../Page/戈登堂.md "wikilink")，作为[中国银行业监督管理委员会天津监管局的场所](../Page/中国银行业监督管理委员会.md "wikilink")。

## 名言

戈登嘗言：“中國人民耐勞易使，果能教練，可轉弱為強。”又曰：“中國海軍利於守，船砲之製，大不如小。”當時稱其將略-{云}-。\[14\]

## 相關作品

  - General Gordon's Last Stand－1885年由George William
    Joy所繪，現藏於里茲市立藝廊（Leeds City Art Gallery）
  - 英國電影《[-{zh-hans:喀土穆;zh-hk:沙漠龍虎會;zh-tw:戰國春秋;}-](../Page/戰國春秋.md "wikilink")》（英語：Khartoum）

## 参考文献

## 参见

  - [太平天國](../Page/太平天國.md "wikilink")

[分類:倫敦人](../Page/分類:倫敦人.md "wikilink")

[G](../Category/1833年出生.md "wikilink")
[G](../Category/1885年逝世.md "wikilink")
[G](../Category/英國陸軍少將.md "wikilink")
[G](../Category/英国战争身亡者.md "wikilink")
[Category:清朝外籍軍官](../Category/清朝外籍軍官.md "wikilink")

1.  [惨死非洲的“中国人”查尔斯·戈登](https://m.ifeng.com/shareNews?fromType=vampire&aid=sub_37449840)

2.

3.
4.  [Ch'ing China: The Taiping
    Rebellion](http://www.wsu.edu/~dee/CHING/TAIPING.HTM)

5.  [士迪佛立（Charles W.
    Staveley，1817～1896）](http://www.shtong.gov.cn/newsite/node2/node2247/node4603/node79844/node79846/userobject1ai102337.html).
    上海市地方志办公室

6.  [Sir Charles William Dunbar
    Staveley](http://www.staveley-genealogy.com/cwd_staveley.htm)

7.

8.  [Slave trade in the Sudan in the nineteenth century and its
    suppression in the
    years 1877-80.](http://www.encyclopedia.com/doc/1G1-20649477.html)

9.  Neufeld 1899, Appendix II, p. 332-337

10. [1939年天津水灾中的戈登堂](http://www.tjdag.gov.cn/tjdag/wwwroot/root/template/main/ztda_article.shtml?id=204&typeid=10).
    天津市档案局

11. [舊日天津英租界道路名稱好洋氣，街區格局延續百年基本未變](https://kknews.cc/history/8bg6le.html)

12. [附录
    二、新旧路名对照](http://www.shtong.gov.cn/newsite/node2/node2245/node63852/node63965/node64504/index.html).
    上海租界志

13. [第一篇区域人口 第一章区域 第四节
    越界筑路](http://www.shtong.gov.cn/newsite/node2/node2245/node63852/node63857/node63871/node64463/userobject1ai57957.html).
    上海租界志

14.