**2月19日**是[公历一年中的第](../Page/公历.md "wikilink")50天，离全年结束还有315天（[闰年则还有](../Page/闰年.md "wikilink")316天）。

## 大事记

### 16世紀

  - [1594年](../Page/1594年.md "wikilink")：[波兰立陶宛联邦国王](../Page/波兰立陶宛联邦.md "wikilink")[齐格蒙特三世在](../Page/齐格蒙特三世.md "wikilink")[乌普萨拉加冕成为](../Page/乌普萨拉.md "wikilink")[瑞典国王](../Page/瑞典国王.md "wikilink")。
  - [1600年](../Page/1600年.md "wikilink")：[祕魯的](../Page/秘鲁.md "wikilink")[於埃納普蒂納火山發生南美洲有史以來最大的火山爆發](../Page/于埃納普蒂納火山.md "wikilink")。

### 19世紀

  - [1878年](../Page/1878年.md "wikilink")：[保加利亚王国独立](../Page/保加利亚王国.md "wikilink")。
  - [1878年](../Page/1878年.md "wikilink")：[-{zh-hant:湯瑪斯·愛迪生;
    zh-hans:托马斯·爱迪生;}-獲得留聲機的專利權](../Page/愛迪生.md "wikilink")。
  - [1881年](../Page/1881年.md "wikilink")：[堪薩斯州成為美國第一個禁止販售酒精產品的州](../Page/堪薩斯州.md "wikilink")。

### 20世紀

  - [1907年](../Page/1907年.md "wikilink")：[黄冈起义失败](../Page/黄冈起义.md "wikilink")。
  - [1916年](../Page/1916年.md "wikilink")：[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，[德軍向](../Page/德軍.md "wikilink")[法國城市](../Page/法國.md "wikilink")[凡爾登發動猛烈攻擊](../Page/凡爾登.md "wikilink")。
  - [1934年](../Page/1934年.md "wikilink")：[中華民國](../Page/中華民國.md "wikilink")[國民政府軍事委員會委員長](../Page/國民政府軍事委員會.md "wikilink")[蒋中正在](../Page/蒋中正.md "wikilink")[南昌行营发表演说](../Page/南昌.md "wikilink")，发起「[新生活運動](../Page/新生活運動.md "wikilink")」。
  - [1942年](../Page/1942年.md "wikilink")：[日本海军航空兵对](../Page/日本.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")[达尔文港发动](../Page/达尔文港.md "wikilink")[空袭](../Page/达尔文空袭.md "wikilink")，这是日本飞机对澳大利亚大陆的首次攻击。
  - 1942年：[美國總統](../Page/美國總統.md "wikilink")[富蘭克林·德拉諾·羅斯福簽署](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")[9066號行政命令](../Page/第9066號行政命令.md "wikilink")，授權將112,000多名居住在美國的日裔美國人和日本人[強制遣送到拘留營中](../Page/日裔美國人囚禁.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[美国海军陆战队在](../Page/美国海军陆战队.md "wikilink")[硫磺岛登陆](../Page/硫磺岛.md "wikilink")，向[日本守军发动进攻](../Page/日本.md "wikilink")，[硫磺岛战役爆发](../Page/硫磺岛战役.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：。
  - [1972年](../Page/1972年.md "wikilink")：[日本](../Page/日本.md "wikilink")[淺間山莊事件](../Page/淺間山莊事件.md "wikilink")：[聯合赤軍進入](../Page/日本赤軍.md "wikilink")[輕井澤淺間山莊](../Page/輕井澤.md "wikilink")，並挾持人質。
  - [1979年](../Page/1979年.md "wikilink")：中国[联合国教科文组织全国委员会成立](../Page/联合国教科文组织.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[美國再次表示坚决抵制](../Page/美國.md "wikilink")[莫斯科奥运会](../Page/1980年夏季奥林匹克运动会.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[蘇聯](../Page/蘇聯.md "wikilink")[和平號太空站發射升空](../Page/和平號太空站.md "wikilink")，成为人类首个可以进行长期研究工作的[太空站](../Page/太空站.md "wikilink")。

### 21世紀

  - [2008年](../Page/2008年.md "wikilink")：日本[東芝公司正式宣佈將終止](../Page/東芝.md "wikilink")[HD
    DVD事業](../Page/HD_DVD.md "wikilink")。

## 出生

  - [703年](../Page/703年.md "wikilink")：[安禄山](../Page/安禄山.md "wikilink")，本姓康，名軋犖山，[安史之亂的主要發動人之一](../Page/安史之亂.md "wikilink")，并建立燕政权，年号聖武。（逝于[757年](../Page/757年.md "wikilink")）
  - [1473年](../Page/1473年.md "wikilink")：[尼古拉·哥白尼](../Page/尼古拉·哥白尼.md "wikilink")，[波兰天文学家](../Page/波兰.md "wikilink")（逝於[1543年](../Page/1543年.md "wikilink")）
  - [1859年](../Page/1859年.md "wikilink")：[阿伦尼乌斯](../Page/阿伦尼乌斯.md "wikilink")，[瑞典物理學家](../Page/瑞典.md "wikilink")，[1903年](../Page/1903年.md "wikilink")[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")（逝於[1927年](../Page/1927年.md "wikilink")）
  - [1865年](../Page/1865年.md "wikilink")：[斯文·赫定](../Page/斯文·赫定.md "wikilink")，瑞典探險家，[樓蘭古城發現者](../Page/樓蘭.md "wikilink")（逝於[1952年](../Page/1952年.md "wikilink")）
  - [1899年](../Page/1899年.md "wikilink")：[卢齐欧·封塔纳](../Page/卢齐欧·封塔纳.md "wikilink")，[意大利畫家](../Page/意大利.md "wikilink")、雕塑家（逝於[1968年](../Page/1968年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[俞大绂](../Page/俞大绂.md "wikilink")，植物病理学家（逝於[1993年](../Page/1993年.md "wikilink")）
  - [1942年](../Page/1942年.md "wikilink")：[霍华德·斯金格](../Page/霍华德·斯金格.md "wikilink")，[英國企業家](../Page/英國.md "wikilink")，前[日本](../Page/日本.md "wikilink")[索尼董事長](../Page/Sony.md "wikilink")
  - [1943年](../Page/1943年.md "wikilink")：[侯默·海堪](../Page/侯默·海堪.md "wikilink")，
    [美國](../Page/美國.md "wikilink")[NASA工程師及作家](../Page/NASA.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[林懷民](../Page/林懷民.md "wikilink")，[台灣舞蹈家](../Page/台灣.md "wikilink")，[云门舞集创办人](../Page/云门舞集.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[丁佩](../Page/丁佩.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[村上龍](../Page/村上龍.md "wikilink")，[日本小說家](../Page/日本.md "wikilink")、電影導演
  - [1953年](../Page/1953年.md "wikilink")：[克里斯蒂娜·费尔南德斯·德·基什内尔](../Page/克里斯蒂娜·费尔南德斯·德·基什内尔.md "wikilink")，[阿根廷第](../Page/阿根廷.md "wikilink")55任[總統](../Page/阿根廷總統列表.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[法爾可](../Page/法爾可.md "wikilink")，[奧地利](../Page/奧地利.md "wikilink")[饒舌歌手](../Page/饒舌.md "wikilink")（逝於[1998年](../Page/1998年.md "wikilink")）
  - [1977年](../Page/1977年.md "wikilink")：[黃騰浩](../Page/黃騰浩.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[藍鈞天](../Page/藍鈞天.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[陳紹誠](../Page/陳紹誠.md "wikilink")，[台灣時尚F](../Page/台灣.md "wikilink")4，[陳立夫之孫](../Page/陳立夫.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[卡卡](../Page/卡路士·伊度亞度·費拉利.md "wikilink")，[香港](../Page/香港.md "wikilink")[巴西籍足球運動員](../Page/巴西.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[马琳](../Page/马琳.md "wikilink")，[中國男子](../Page/中國.md "wikilink")[乒乓球運動員](../Page/乒乓球.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[簡佩堅](../Page/簡佩堅.md "wikilink")，香港艺人、2000年香港小姐競選亞軍
  - [1981年](../Page/1981年.md "wikilink")：[Vitas](../Page/Vitas.md "wikilink")，[俄罗斯男高音歌手](../Page/俄罗斯.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[中島美嘉](../Page/中島美嘉.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[郭采潔](../Page/郭采潔.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")、演員
  - [1986年](../Page/1986年.md "wikilink")：[久紗野水萌](../Page/久紗野水萌.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")、寫真集模特兒
  - [1987年](../Page/1987年.md "wikilink")：[岑潔儀](../Page/岑潔儀.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[許怡才](../Page/許怡才.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[入野自由](../Page/入野自由.md "wikilink")，[日本男演員](../Page/日本.md "wikilink")、聲優
  - [1988年](../Page/1988年.md "wikilink")：[潘時七](../Page/潘時七.md "wikilink")，[中國內地女演員](../Page/中國.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[楊善諭](../Page/楊善諭.md "wikilink")，[香港女配音員](../Page/香港.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[克里斯托夫·克拉默](../Page/克里斯托夫·克拉默.md "wikilink")，[德國國家足球隊球員](../Page/德國國家足球隊.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[泱泱](../Page/吳泱潾.md "wikilink")，[台灣女藝人](../Page/台灣.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[維多利亞·嘉絲蒂](../Page/維多利亞·嘉絲蒂.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")、創作行歌手、舞蹈家

## 逝世

  - [1938年](../Page/1938年.md "wikilink")：[张廷玉](../Page/张廷玉.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")（[1902年出生](../Page/1902年.md "wikilink")）
  - [1951年](../Page/1951年.md "wikilink")：[纪德](../Page/纪德.md "wikilink")，小说家、[1947年](../Page/1947年.md "wikilink")[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")（[1869年出生](../Page/1869年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[莫哈末·韩查](../Page/莫哈末·韩查.md "wikilink"),
    [马来西亚国旗设计者](../Page/马来西亚国旗.md "wikilink")
    （[1918年出生](../Page/1918年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[邓小平](../Page/邓小平.md "wikilink")，[中國政治家](../Page/中國.md "wikilink")、軍事家、[改革開放和社會主義現代化建設總設計師](../Page/改革開放.md "wikilink")（[1904年出生](../Page/1904年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[許財利](../Page/許財利.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[基隆市前市長](../Page/基隆市.md "wikilink")（[1947年出生](../Page/1947年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[沈殿霞](../Page/沈殿霞.md "wikilink")，[香港資深電視藝人](../Page/香港.md "wikilink")（[1945年出生](../Page/1945年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[吳木金](../Page/吳木金.md "wikilink")，香港女工（[1917年出生](../Page/1917年.md "wikilink")）
  - [2010年](../Page/2010年.md "wikilink")：[凌禮文](../Page/凌禮文.md "wikilink")，[香港電視藝人](../Page/香港.md "wikilink")（[1941年出生](../Page/1941年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[袁雪芬](../Page/袁雪芬.md "wikilink")，[越劇表演藝術家](../Page/越劇.md "wikilink")（[1922年出生](../Page/1922年.md "wikilink")）
  - [2019年](../Page/2019年.md "wikilink")：[卡爾·拉格斐](../Page/卡爾·拉格斐.md "wikilink")，人称“老佛爷”，德国设计师，法国时尚品牌[香奈儿领衔设计师兼创意总监](../Page/香奈儿.md "wikilink")（[1933年出生](../Page/1933年.md "wikilink")）

## 节日、风俗习惯

  - ：[炬光節](../Page/炬光節.md "wikilink")（1977年起）

  - ：[新生活運動紀念日](../Page/新生活運動紀念日.md "wikilink")