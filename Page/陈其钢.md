**陈其钢**（），[法籍华人](../Page/法籍华人.md "wikilink")，[作曲家](../Page/作曲家.md "wikilink")。

## 作品

陈的代表作有：[芭蕾舞剧](../Page/芭蕾舞剧.md "wikilink")《[大红灯笼高高挂](../Page/大红灯笼高高挂.md "wikilink")》、[大提琴](../Page/大提琴.md "wikilink")[协奏曲](../Page/协奏曲.md "wikilink")《逝去的时光》、[管弦乐组曲](../Page/管弦乐.md "wikilink")《五行》、双乐队协奏曲《蝶恋花》、[钢琴](../Page/钢琴.md "wikilink")[协奏曲](../Page/协奏曲.md "wikilink")《二黄》等。\[1\][2008年夏季奧林匹克運動會開幕式的主题曲](../Page/2008年夏季奧林匹克運動會開幕式.md "wikilink")《[我和你](../Page/我和你_\(歌曲\).md "wikilink")》是由他作词作曲，并且由他担任开幕式的音乐总监。

## 个人生活

陈其钢与旅法钢琴家黎耘是夫妻，音乐制作人[陈黎雨是他们的儿子](../Page/陈黎雨.md "wikilink")。

## 争议

作曲家[王西麟曾经批评陈其钢等第五代中国作曲家作品内容肤浅](../Page/王西麟.md "wikilink")，多涉风花雪月，“没有回答历史对他们的深刻要求，也没有思想家批判历史这样的深度，没有思想批判的深度”\[2\][郭文景对此的回应是](../Page/郭文景.md "wikilink")：作曲家有“选择自己熟悉、喜爱、擅长的题材来进行创作的自由”。\[3\]

## 电影配乐

  - 2010年《[山楂树之恋](../Page/山楂树之恋_\(电影\).md "wikilink")》
  - 2011年《[金陵十三钗](../Page/金陵十三钗.md "wikilink")》，获得[第六届亚洲电影大奖最佳原创音乐提名](../Page/第六届亚洲电影大奖.md "wikilink")
  - 2013年《[归来](../Page/归来_\(电影\).md "wikilink")》[第51届金马奖最佳原创电影音乐](../Page/第51届金马奖.md "wikilink")、[第16届华表奖优秀音乐](../Page/第16届华表奖.md "wikilink")

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")

## 参考资料

<div class="references-small">

<references />

</div>

## 外部链接

  -
  - [陈其钢访谈](http://humanities.cn/modules/article/view.article.php?category=3&article=403&page=0)

  - [陈其钢：音乐，玩不得半点虚假](http://humanities.cn/modules/article/view.article.php?396)

  - [陈其钢和张艺谋的争吵与默契](http://www.youngchina.cn/star/20140731/34465.html)

[Category:法国作曲家](../Category/法国作曲家.md "wikilink")
[Category:中华人民共和国作曲家](../Category/中华人民共和国作曲家.md "wikilink")
[Category:归化法国公民的中华人民共和国人](../Category/归化法国公民的中华人民共和国人.md "wikilink")
[Category:20世纪作曲家](../Category/20世纪作曲家.md "wikilink")
[Category:21世纪作曲家](../Category/21世纪作曲家.md "wikilink")
[Category:中央音乐学院校友](../Category/中央音乐学院校友.md "wikilink")
[Category:上海艺术家](../Category/上海艺术家.md "wikilink")
[Qigang](../Category/陈姓.md "wikilink")
[Category:电影配乐家](../Category/电影配乐家.md "wikilink")
[Category:金馬獎最佳電影音樂獲獎者](../Category/金馬獎最佳電影音樂獲獎者.md "wikilink")
[Category:华表奖得主](../Category/华表奖得主.md "wikilink")

1.  [陈其钢简历](http://humanities.cn/modules/article/view.article.php?category=3&article=396&page=1)
2.  [王西麟：他们的批判精神丧失了](http://humanities.cn/modules/article/view.article.php?397/c0)

3.  [郭文景：与王西麟先生对话](http://humanities.cn/modules/article/view.article.php?398)