**OQO**是一個的小型[UMPC製造商](../Page/UMPC.md "wikilink")，其製品名為ultra Personal
Computer
(uPC)，是一種擁有[平板電腦機能](../Page/平板電腦.md "wikilink")、尺寸外型比[PDA略大的電腦](../Page/PDA.md "wikilink")。根據[金氏世界紀錄大全](../Page/金氏世界紀錄大全.md "wikilink")"OQO"是最小型的全功能[個人電腦](../Page/個人電腦.md "wikilink")。OQO第一個小型筆記型電腦產品為OQO
model 01。

初時OQO model
01於[原型機亮相多年前發表以致很多人稱其為](../Page/原型.md "wikilink")[霧件直至最終於](../Page/霧件.md "wikilink")2004年後期發售。該電腦預載[Windows
XP](../Page/Windows_XP.md "wikilink")（Home Edition或Professional版，Tablet
PC Edition於model 01+發售後才能選擇）、擁有1
[GHz](../Page/赫茲_\(單位\).md "wikilink")[全美達處理器](../Page/全美達處理器.md "wikilink")、20
[GB硬碟及](../Page/Gigabyte.md "wikilink")256
[MB](../Page/Megabyte.md "wikilink")
[RAM](../Page/隨機存取記憶體.md "wikilink")，包含了[USB](../Page/USB.md "wikilink")
1.1、[FireWire
400](../Page/FireWire.md "wikilink")、耳機接口、內置麥克風、整合[802.11b無線連接與](../Page/802.11b.md "wikilink")[藍芽](../Page/藍芽.md "wikilink")，使用配合磁性筆的800x480磁性輕觸式[Transflective](../Page/Transflective.md "wikilink")
LCD屏幕，零售運送於2004年10月14日開始。

OQO model 01+於2005年12月27日發表與發售，作為model 01的加強版OQO model
01+擁有30[GB](../Page/Gigabyte.md "wikilink")[硬碟](../Page/硬碟.md "wikilink")、512[MB](../Page/Megabyte.md "wikilink")
[RAM](../Page/隨機存取記憶體.md "wikilink")、[USB](../Page/USB.md "wikilink")
2.0與內置揚聲器，同時加入支援[自畫像顯示模式](../Page/自畫像.md "wikilink")（480x800）並重新設計屏幕斜面以增加輕觸屏幕的準確性。

運行Microsoft Windows XP Tablet PC Edition 2005的OQO model
01+於2006年1月4日推出市面，充份利用了Tablet PC
Edition的[手寫識別與導航功能](../Page/手寫識別.md "wikilink")。

最近它被用作與[UMPC平台比較](../Page/超級移動電腦.md "wikilink")，儘管它比UMPC發售早得多。

## 已發布的產品列表

  - [OQO Model 01](../Page/OQO_Model_01.md "wikilink")
  - OQO Model 01+
  - [OQO Model 02](../Page/OQO_Model_02.md "wikilink")
  - OQO Model 03

## 批評

*PC World*雜誌將OQO Model
01評為25件最差科技產品中的19位，以下為其說明：「你需要一個放大鏡才能在其5吋乘3吋的屏幕上看得清文字與圖示，其隱藏的鍵盤太小甚至不能容納兩根成人的手指。同時Model
1的運行時温度非常高，而其$1900元以上的價格亦會將你的錢包燒出一個洞。好東西通常來自小包裝（Good things often come
in small packages，OQO宣傳口號），但這次不是。」

## 鍵盤指令

  - Fn-2 – 進入BIOS設定（開機期間）
  - Fn-B – 切換滑動屏幕喚醒睡眠模式
  - Fn-C – 改變"引力滑鼠"的靈敏度
  - Fn-J – 切換自由落體感應（無聲=開啓，卡嗒聲=關閉）
  - Fn-K – 切換按鍵音開關
  - Fn-L – 切換內或外置屏幕顯示
  - Fn-M – 屏幕反轉180度
  - Fn-N – 切換滾輪的方向
  - Fn-V – 切換蓋子開／關感應器
  - Fn-Z – 切換TrackStik按一下以選擇

## 外部連結

  - [官方網站](https://web.archive.org/web/20090213192632/http://www.oqo.com/)
      - [OQO驅動程式](http://support.oqo.com/cgi-bin/oqo.cfg/php/enduser/std_adp.php?p_faqid=305&p_created=1132113176)
      - [OQO智識庫](https://web.archive.org/web/20080517124632/http://support.oqo.com/cgi-bin/oqo.cfg/php/enduser/entry.php)
  - OQO創辦人[Jory
    Bell專訪](http://www.npost.com/interview.jsp?intID=INT00132)
  - [Handtops.com](https://web.archive.org/web/20061212113018/http://www.handtops.com/forum/432/0/OQO_OS_Optimization.html)
    – 性能優化

### 非官方論壇

  - [Handtops
    OQO](https://web.archive.org/web/20061216070404/http://www.handtops.com/show/forum/3)
  - [OqoTalk](http://www.oqotalk.com)

### 評論與批論

  - PC World –
    [評論](http://www.pcworld.com/reviews/article/0,aid,118150,00.asp)
  - PC World –
    [25年最差科技產品](http://www.pcworld.com/reviews/article/0,aid,125772,pg,5,00.asp)

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:筆記型電腦](../Category/筆記型電腦.md "wikilink")
[Category:2000年加利福尼亞州建立](../Category/2000年加利福尼亞州建立.md "wikilink")