**金本位**是一種[贵金屬](../Page/贵金屬.md "wikilink")[貨幣制度](../Page/貨幣.md "wikilink")，於19世紀中期開始盛行。在金本位制度下，每單位的貨幣價值等同於若干含重量的[黃金](../Page/黃金.md "wikilink")（即貨幣含金量）；當不同[國家使用金本位時](../Page/國家.md "wikilink")，國家之間的[匯率由它們各自貨幣的含金量之比值](../Page/匯率.md "wikilink")－金平價（Gold
Parity）來決定。

金本位制總共有三種實現形式：**金幣本位制**、**金塊本位制**和**金兌匯本位制**，其中[金幣本位制最具有金本位制的代表性](../Page/金幣.md "wikilink")。

## 金幣本位制

金幣本位制將黃金鑄幣作為法定[本位幣](../Page/本位幣.md "wikilink")，而銀幣則退居[輔幣地位](../Page/輔幣.md "wikilink")，它的自由鑄造和無限法償能力受到限制。

黃金作為本位幣就有[無限法償能力](../Page/無限法償.md "wikilink")，[銀行券可以自由兌換成金幣](../Page/銀行券.md "wikilink")，任何人都可以向國家造幣廠申請將其所有的黃金鑄造成金幣或將金幣熔成金屬塊。黃金的輸入和輸出在國與國之間可以自由進行轉移。金幣本位制的統治地位長達100年，其間對[資本主義的生產起到過一定的促進作用](../Page/資本主義.md "wikilink")。然而由於世界黃金產地分佈不均勻，金幣磨損等原因，金幣本位制在[第一次世界大戰後逐漸被金塊本位制和金兌匯本位制所取代](../Page/第一次世界大戰.md "wikilink")。

## 金塊本位制及金兌匯本位制

在金塊本位制中，可以兌換黃金的銀行券代替金幣，任何人都可以將所持有的銀行券兌換成等價的黃金。雖設有兌換上限，但一般人的財富很少會超出此限，這種制度可以舒緩黃金產量不足和損耗等問題。

金兌匯本位制也稱做虛金本位制，當一個國家實行這種貨幣制度時，需要將黃金存放在另一個國家或[地區](../Page/地區.md "wikilink")，本國貨幣與之實行固定比例率，只有當此國貨幣兌換成黃金存放國的貨幣時，才能再兌換成黃金，這是一種[殖民地性質的貨幣制度](../Page/殖民地.md "wikilink")。

上述兩種貨幣制度基本上到1970年代已逐漸消失。

## 金本位制崩潰歷程

### 布雷頓森林體系時期

在[第二次世界大戰之後](../Page/第二次世界大戰.md "wikilink")，類似於金兌本位制的體系在[美國的主導下建立](../Page/美國.md "wikilink")。在該體系下，各國貨幣與[美元掛鉤](../Page/美元.md "wikilink")，美元與黃金掛鉤，明訂一[盎司黃金價格固定等同於](../Page/盎司.md "wikilink")35美元。60和70年代發生的數次美元危機使該體系發生動搖，最終[美國總統](../Page/美國總統.md "wikilink")[尼克森於](../Page/尼克森.md "wikilink")1971年宣佈停止美元與黃金的兌換，布雷頓森林體系終結。

## 儲備黃金的意義

黃金至今依然被視為一種「**準貨幣**」，被國際所接受。類似于[外匯和](../Page/外匯.md "wikilink")[國債](../Page/國債.md "wikilink")，黃金儲備在各國財政儲備中均有重要地位，一方面是出於對本國匯率的保障，另一方面則是據此[對沖由美元貶值帶來的損失](../Page/對沖.md "wikilink")。而在流動性市場上，黃金依然有著廣泛的需求，依然被視為儲藏財富的一種方式。

## 金本位下的汇率

### 含金量

  - 一[英镑等於](../Page/英镑.md "wikilink")7.32238克黄金
  - 1[法郎](../Page/法郎.md "wikilink")=0.2903225克黄金（1803年3月28日法律确立，称为“芽月法郎”。[一战后法郎贬值](../Page/一战.md "wikilink")，芽月法郎被称为**金法郎**，作为[万国邮政联盟](../Page/万国邮政联盟.md "wikilink")、[国际清算银行的结算货币](../Page/国际清算银行.md "wikilink")，质量为10/31克，含金量0.900）
  - 1[美元](../Page/美元.md "wikilink")=1.50463克黄金
  - 1德国金[马克](../Page/马克.md "wikilink")=10德国马克=3.58425克黄金
  - 1[卢布](../Page/卢布.md "wikilink")=0.774234克黄金
  - 1[日元](../Page/日元.md "wikilink")=0.75克黄金
  - 1[库平两白银](../Page/库平两.md "wikilink")=37.3克=（1894年时金银比价1：20）1.865克黄金=（1900年后金银比价1：35）1.065克黄金
  - 1[比索](../Page/比索.md "wikilink")=0.507816克黄金

### 近似汇率

以下近似等值：

  - 1英镑
  - 25法郎
  - 5美元
  - 2德国金马克=20德国马克
  - 9卢布
  - 10日元
  - 4库平两（1894年前）=7库平两（1900后）
  - 14比索

## 参考文献

## 参见

  - [银本位](../Page/银本位.md "wikilink")
  - [金銀複本位](../Page/金銀複本位.md "wikilink")

{{-}}

[金本位](../Category/金本位.md "wikilink")
[Category:貨幣理論](../Category/貨幣理論.md "wikilink")
[Category:宏觀經濟學](../Category/宏觀經濟學.md "wikilink")
[Category:金融史](../Category/金融史.md "wikilink")
[Category:银行业历史](../Category/银行业历史.md "wikilink")
[Category:经济学](../Category/经济学.md "wikilink")