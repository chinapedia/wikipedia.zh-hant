[Urostigma_and_Fuzhou_20140126.jpg](https://zh.wikipedia.org/wiki/File:Urostigma_and_Fuzhou_20140126.jpg "fig:Urostigma_and_Fuzhou_20140126.jpg")
**榕亞屬**（[学名](../Page/学名.md "wikilink")：），是[桑科](../Page/桑科.md "wikilink")[榕属的](../Page/榕属.md "wikilink")[亞屬之一](../Page/亞屬.md "wikilink")，由於多為[常綠喬木及細節分冶不易被人分辨](../Page/常綠植物.md "wikilink")，因此也統稱**榕樹**。有“正榕”、“鳥榕”、“老公鬚”或“戲葉榕樹”等別名。原產於[中國](../Page/中國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[印度](../Page/印度.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")。

榕樹為常綠大[喬木](../Page/喬木.md "wikilink")，[氣根多數](../Page/氣根.md "wikilink")；葉互生，倒卵形或[橢圓形](../Page/橢圓.md "wikilink")，表面深[綠色](../Page/綠色.md "wikilink")；隱花果，無柄單生或對生於葉腋，成熟時呈[黃褐色](../Page/黄色.md "wikilink")、[紅褐色或](../Page/红色.md "wikilink")[黑](../Page/黑色.md "wikilink")[紫色](../Page/紫色.md "wikilink")。

北宋治平元年（1064年），太守[张伯玉移知福州](../Page/张伯玉.md "wikilink")，夏天酷热难耐，遂令编户浚沟七尺，種植榕樹，後來“绿荫满城，暑不张盖”，故福州又有“榕城”的美称\[1\]。全世界现有1000多种榕树，多集中在[热带雨林地区](../Page/热带雨林.md "wikilink")，[热带植物中最大的木本树种之一](../Page/热带.md "wikilink")，常高达20米。由於耐污性好、易栽植，也是常見的[行道樹](../Page/行道樹.md "wikilink")。

## 榕樹的名字

榕樹的[拉丁文為banyan](../Page/拉丁文.md "wikilink")，在西方這個名字多指來自印度及[孟加拉等地的](../Page/孟加拉.md "wikilink")[孟加拉榕](../Page/孟加拉榕.md "wikilink")（**）；但在中國及亞洲等地則多指[細葉榕](../Page/細葉榕.md "wikilink")（**）。由於它們同屬榕亞屬，並且有相近的生活周期及[氣根等特徵](../Page/氣根.md "wikilink")，因此榕樹現多作為桑科榕屬下榕亞屬的簡稱。

## 注釋

## 外部連結

  - [榕樹
    Rongshu](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00543)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[榕亚属](../Category/榕亚属.md "wikilink")

1.  《太平寰宇记》载，“榕……其大十围，凌冬不凋，郡城中独盛，故号榕城"。