**磷酸酯酶与张力蛋白同源物**（，简称为**PTEN**）是一种在人体中由*PTEN*[基因编码的](../Page/基因.md "wikilink")[蛋白质](../Page/蛋白质.md "wikilink")\[1\]。该基因的突变是多种[癌症进展过程的环节之一](../Page/癌症.md "wikilink")。

*PTEN*通过其[磷酸酯酶蛋白产物而行使一种](../Page/磷酸酯酶.md "wikilink")[抑癌基因的作用](../Page/抑癌基因.md "wikilink")。这一磷酸酯酶参与了[细胞周期的调节](../Page/细胞周期.md "wikilink")，阻止细胞过快地生长与分裂\[2\]。其为[癌微RNA](../Page/癌微RNA.md "wikilink")[MIRN21的靶之一](../Page/MIRN21.md "wikilink")。

该基因被鉴定为一种肿瘤抑制物，在多种癌症中往往处于变异状态。该基因编码的蛋白质是一种磷脂酰肌醇-3,4,5-三磷酸3-磷酸酯酶。该蛋白同时含有一[张力蛋白样结构域及一催化结构域](../Page/TNS1.md "wikilink")，这与双特异性[蛋白酪氨酸磷酸酶很相似](../Page/蛋白酪氨酸磷酸酶.md "wikilink")。但与大部分蛋白质酪氨酸磷酸酶不同的是，该蛋白偏好脱去[磷酸肌醇底物上的磷酸](../Page/磷酸肌醇.md "wikilink")。该蛋白负性调控胞内[磷脂酰肌醇-3,4,5-三磷酸的水平](../Page/磷脂酰肌醇\(3,4,5\)-三磷酸.md "wikilink")，并通过负性调控[Akt/PKB信号通道发挥抑癌基因的作用](../Page/Akt/PKB信号通道.md "wikilink")\[3\]。

## 參考文献

## 深入阅读

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部链接

  - [GeneReviews/NCBI/NIH/UW entry on PTEN Hamartoma Tumor Syndrome
    (PHTS)](http://www.ncbi.nlm.nih.gov/bookshelf/br.fcgi?book=gene&part=phts)

  -
  -
  -
  -
  - [Research shows gene defect's role in autism-like
    behavior](http://www.ucdmc.ucdavis.edu/publish/news/mindinstitute/6866)

[Category:人类蛋白质](../Category/人类蛋白质.md "wikilink")
[Category:肿瘤抑制基因](../Category/肿瘤抑制基因.md "wikilink")
[Category:外周膜蛋白](../Category/外周膜蛋白.md "wikilink") [Category:EC
3.1.3](../Category/EC_3.1.3.md "wikilink")

1.
2.
3.