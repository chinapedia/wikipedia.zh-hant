擴展實數線由[實數線](../Page/實數線.md "wikilink")\(\R\)加上\(+\infty\)和\(-\infty\)得到（注意\(+\infty\)和\(-\infty\)并不是实数），写作\(\overline\R\)或\(\left[ -\infty, +\infty \right]\)。扩展的實數線在研究[数学分析](../Page/数学分析.md "wikilink")，特别是[积分时非常有用](../Page/积分.md "wikilink")。

## 扩展

对任意[实数](../Page/实数.md "wikilink")\(a\)，定义\(-\infty \le a \le +\infty\)，扩展的实数轴就成了一个[全序集](../Page/全序集.md "wikilink")。这种集合有种非常好的性质，就是其所有[子集都有](../Page/子集.md "wikilink")[上确界和](../Page/上确界.md "wikilink")[下确界](../Page/下确界.md "wikilink")：这是一个[完备格](../Page/完备格.md "wikilink")。全序关系在\(\overline\R\)上引入了[拓扑](../Page/拓扑学.md "wikilink")。在这个拓扑中，集合\(U\)是\(+\infty\)的[邻域](../Page/邻域.md "wikilink")，当且仅当它包含集合\(\left\{ x : x > a \right\}\)，这里\(a\)是某个实数。\(-\infty\)的邻域类似。\(\overline\R\)是个[紧致的](../Page/紧致.md "wikilink")[豪斯多夫空间](../Page/豪斯多夫空间.md "wikilink")，与单位[区间](../Page/区间.md "wikilink")\(\left[ 0, 1 \right]\)[同胚](../Page/同胚.md "wikilink")。

\(\R\)上的算术运算可以部分地扩展到\(\overline\R\)，如下：

\[\begin{array}{l}
a + \infty = +\infty + a = +\infty & a \neq -\infty \\
a - \infty = -\infty + a = -\infty & a \neq +\infty \\
a \cdot \left( \pm\infty \right) = \pm\infty \cdot a = \pm\infty & a \in \left( 0, +\infty \right] \\
a \cdot \left( \pm\infty \right) = \pm\infty \cdot a = \mp\infty & a \in \left[ -\infty, 0 \right) \\
\dfrac{a}{\pm\infty} = 0 & a \in \R \\
\dfrac{\pm\infty}{a} = \pm\infty & a \in \left( 0, +\infty \right) \\
\dfrac{\pm\infty}{a} = \mp\infty & a \in \left( -\infty, 0 \right)
\end{array}\]

通常不定义\(\infty - \infty, 0 \cdot \left( \pm\infty \right), \frac{\pm\infty}{\pm\infty}\)。同时\(\frac{1}{0}\)也不定义为\(+\infty\)（因為這樣忽視了\(-\infty\)），这些规则是根据无穷[极限的性质确定的](../Page/极限\(数学\).md "wikilink")。

注意在这些定义下，\(\overline\R\)不是[域](../Page/域_\(数学\).md "wikilink")，也不是[环](../Page/环_\(代数\).md "wikilink")。

## 性质

经过上述定义，扩展的实数轴仍有很多实数的性质：

  - \(a + \left( b + c \right)\)和\(\left( a + b \right) + c\)相等或同时没有定义。
  - \(a + b\)和\(b + a\)相等或同时没有定义。
  - \(a \cdot \left( b \cdot c \right)\)和\(\left( a \cdot b \right) \cdot c\)相等或同时没有定义。
  - \(a \cdot b\)和\(b \cdot a\)相等或同时没有定义。
  - \(a \cdot \left( b + c \right)\)和\(\left( a \cdot b \right) + \left( a \cdot c \right)\)若都有定义则相等。
  - 若\(a \le b\)且\(a + c\)和\(b + c\)都有定义，则\(a + c \le b + c\)。
  - 若\(a \le b\)且\(c>0\)且\(a \cdot c\)和\(b \cdot c\)都有定义，则\(a \cdot c \le b \cdot c\)。

通常只要表达式都有定义，所有算术性质在\(\overline\R\)上都成立。

使用极限，一些[函数可以自然地扩展到](../Page/函数.md "wikilink")\(\overline\R\)。例如可以定义\(\rm{e}^{-\infty} = 0, \rm{e}^{+\infty} = +\infty, \ln{0} = -\infty, \ln{\left( +\infty \right)} = +\infty\)等。

## 参见

  - [扩展的复平面](../Page/扩展的复平面.md "wikilink")

[Category:实分析](../Category/实分析.md "wikilink")
[Category:无穷](../Category/无穷.md "wikilink")