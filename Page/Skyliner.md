**Skyliner**（）是一列往返於[東京](../Page/東京都.md "wikilink")[上野與](../Page/上野.md "wikilink")[成田國際機場之間的](../Page/成田國際機場.md "wikilink")[特急列車](../Page/特急列車.md "wikilink")，由[京成電鐵營運](../Page/京成電鐵.md "wikilink")，主要行駛[成田機場線](../Page/成田機場線.md "wikilink")（）。

## 服務

Skyliner約每隔40分鐘開出一班列車來往於[京成上野站及](../Page/京成上野站.md "wikilink")[成田機場站之間](../Page/成田機場站.md "wikilink")，中途只停靠[日暮里及](../Page/日暮里站.md "wikilink")[空港第2大樓站](../Page/空港第2大樓站.md "wikilink")。雖然京成上野站有多條其他路線轉乘，但不少旅客選擇在日暮里站轉乘，這主要是因為日暮里站有[山手線等主要幹線通過](../Page/山手線.md "wikilink")，且在日暮里站轉乘時所需的步行距離也較短之故。

由成田機場至日暮里站行車時間約為36分鐘，成田機場至上野全程則為44分鐘。票價為單程2,470[日圓](../Page/日圓.md "wikilink")（其中包括1,240日圓基本運費與1,230日圓的[特急料金價格](../Page/特急料金.md "wikilink")）。

Skyliner的主要競爭對手是由[JR東日本所經營的](../Page/JR東日本.md "wikilink")[成田特快](../Page/成田特快.md "wikilink")（Narita
Express，N'EX），後者價格較為高昂但可直接到達多個主要車站。2010年時[成田機場線](../Page/成田機場線.md "wikilink")（或又暱稱為「成田Sky
Access線」）通車，Skyliner的行駛路線也從原本的[京成本線轉移到新通車的路線](../Page/京成本線.md "wikilink")。由於路線的更改，由日暮里至[機場第2大樓的行車時間也從原本的](../Page/機場第2大樓車站.md "wikilink")51分鐘大幅縮短至36分鐘。至於Skyliner原本所行駛的京成本線京成上野至成田機場間的路段，則新設特急列車「City
Liner」（）來負擔運輸任務。除此之外在早上與傍晚的通勤尖峰時段，尚設有Morning Liner（）與Evening
Liner（）兩種特急列車，其行駛路線與City
Liner重複，但配合通勤的需求停靠的車站數量較多。Cityliner最後因需求低，於2015年12月5日停止服務。
[Skyliner_行車終點顯示器.jpg](https://zh.wikipedia.org/wiki/File:Skyliner_行車終點顯示器.jpg "fig:Skyliner_行車終點顯示器.jpg")

## 停靠車站

  - Skyliner
    [京成上野](../Page/京成上野車站.md "wikilink") -
    [日暮里](../Page/日暮里車站.md "wikilink") -（行經成田Sky
    Access線）- [機場第2大樓](../Page/機場第2大樓車站.md "wikilink") -
    [成田機場](../Page/成田機場車站.md "wikilink")（位於第一航廈）
  - Morning Liner與Evening Liner
    京成上野 - 日暮里 - [青砥](../Page/青砥站.md "wikilink") -
    [京成船橋](../Page/京成船橋車站.md "wikilink") -
    [八千代台](../Page/八千代台站.md "wikilink") -
    [京成佐倉](../Page/京成佐倉站.md "wikilink") - 京成成田 -
    機場第2大樓 - 成田機場

<!-- end list -->

  -
    <small>註：部分車班由京成成田發車</small>

[Skyliner_車頭上的_Skyliner_營運五周年紀念標誌1.jpg](https://zh.wikipedia.org/wiki/File:Skyliner_車頭上的_Skyliner_營運五周年紀念標誌1.jpg "fig:Skyliner_車頭上的_Skyliner_營運五周年紀念標誌1.jpg")
[Skyliner_車廂上的_Skyliner_營運五周年紀念標誌2.jpg](https://zh.wikipedia.org/wiki/File:Skyliner_車廂上的_Skyliner_營運五周年紀念標誌2.jpg "fig:Skyliner_車廂上的_Skyliner_營運五周年紀念標誌2.jpg")

## 使用車輛

### 現役車輛

  - [京成AE型電聯車
    (二代)](../Page/京成AE型電聯車_\(二代\).md "wikilink")（Skyliner、Morning
    Liner與Evening Liner）

### 退役車輛

[Keisei-ae.jpg](https://zh.wikipedia.org/wiki/File:Keisei-ae.jpg "fig:Keisei-ae.jpg")
[Keisei_Electric_Railway_AE100.jpg](https://zh.wikipedia.org/wiki/File:Keisei_Electric_Railway_AE100.jpg "fig:Keisei_Electric_Railway_AE100.jpg")

  - [京成AE型電聯車](../Page/京成AE型電聯車.md "wikilink")
  - [京成AE100型電聯車](../Page/京成AE100型電聯車.md "wikilink")（City Liner）

## 外部連結

  - [Skyliner網站](http://www.keisei.co.jp/keisei/tetudou/skyliner/jp/index.html)
      - [中文版網站](http://www.keisei.co.jp/keisei/tetudou/skyliner/cn/)
      - [繁體中文版網站](http://www.keisei.co.jp/keisei/tetudou/skyliner/tc/index.php)

[Category:機場聯絡軌道系統](../Category/機場聯絡軌道系統.md "wikilink")
[Category:京成電鐵](../Category/京成電鐵.md "wikilink")
[Category:日本特急列車](../Category/日本特急列車.md "wikilink")
[Category:東京都鐵路](../Category/東京都鐵路.md "wikilink")
[Category:千葉縣鐵路](../Category/千葉縣鐵路.md "wikilink")
[Category:成田國際機場](../Category/成田國際機場.md "wikilink")