**希恩**（）是[挪威](../Page/挪威.md "wikilink")[泰勒马克郡的城市](../Page/泰勒马克.md "wikilink")。

## 名人

  - [亨利·易卜生](../Page/亨利·易卜生.md "wikilink")，作家
  - [西奥马尔·约翰森](../Page/西奥马尔·约翰森.md "wikilink")（），[北极](../Page/北极.md "wikilink")[探险家](../Page/探险家.md "wikilink")[体操运动员](../Page/体操.md "wikilink")
  - [克里斯廷·哈尔沃森](../Page/克里斯廷·哈尔沃森.md "wikilink")（），政治家，[社会主义左翼党党员](../Page/社会主义左翼党.md "wikilink")，现任财政部部长

## 姊妹城市

  - [芬兰](../Page/芬兰.md "wikilink")[洛马亚](../Page/洛马亚.md "wikilink")（）

  - [瑞典](../Page/瑞典.md "wikilink")[乌德瓦拉](../Page/乌德瓦拉.md "wikilink")（）

  - [丹麦](../Page/丹麦.md "wikilink")[蒂斯德特](../Page/蒂斯德特.md "wikilink")（）

  - [美国](../Page/美国.md "wikilink")[米諾特](../Page/米諾特.md "wikilink")（）

[Category:泰勒馬克郡自治區](../Category/泰勒馬克郡自治區.md "wikilink")
[Category:挪威城市](../Category/挪威城市.md "wikilink")