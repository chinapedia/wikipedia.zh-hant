[Taipei_Railway_Workshop_front_overview.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Railway_Workshop_front_overview.jpg "fig:Taipei_Railway_Workshop_front_overview.jpg")

**臺北機廠**，簡稱**北廠**，是[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")（簡稱臺鐵）[機務處下屬單位之一](../Page/机务段.md "wikilink")，為臺鐵主要的[車輛維修與改裝基地](../Page/鐵路車輛.md "wikilink")。該廠原位於[臺北市](../Page/臺北市.md "wikilink")[信義區](../Page/信義區_\(臺北市\).md "wikilink")[市民大道五段](../Page/市民大道.md "wikilink")，前身為[日治時代的](../Page/台灣日治時期.md "wikilink")「臺北鐵道工場」；2013年1月10日遷至位於[桃園市](../Page/桃園市.md "wikilink")[楊梅區的](../Page/楊梅區.md "wikilink")[富岡車輛基地](../Page/臺鐵富岡車輛基地.md "wikilink")，舊址則在關心鐵道文化保存的人士奔走之下，將以全區保留的方式設置鐵路博物館。2015年3月15日，臺北機廠舊址經[中華民國文化部認定為](../Page/中華民國文化部.md "wikilink")[國定古蹟](../Page/國定古蹟.md "wikilink")。

## 簡介

[View_of_Taipei_Railway_Workshop_in_1930s.jpg](https://zh.wikipedia.org/wiki/File:View_of_Taipei_Railway_Workshop_in_1930s.jpg "fig:View_of_Taipei_Railway_Workshop_in_1930s.jpg")
[TRAW-railcar-entrace.jpg](https://zh.wikipedia.org/wiki/File:TRAW-railcar-entrace.jpg "fig:TRAW-railcar-entrace.jpg")
[Taipei_Railway_Workshop_02.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Railway_Workshop_02.jpg "fig:Taipei_Railway_Workshop_02.jpg")

### 地理位置

臺北機廠廠區位於臺北市[信義區](../Page/信義區_\(臺北市\).md "wikilink")[新仁里近](../Page/新仁里.md "wikilink")[松山區交界處](../Page/松山區_\(臺北市\).md "wikilink")；北臨[市民大道與](../Page/市民大道.md "wikilink")[京華城相對面](../Page/京華城.md "wikilink")；南臨[台北市東西向快速道路高架道與](../Page/台北市東西向快速道路.md "wikilink")[松山菸廠相隔](../Page/松山菸廠.md "wikilink")。

### 歷史

據交通部臺灣鐵路管理局出品之《北廠80-見證臺灣鐵道工業的黃金年代》所述。早在1885年劉銘傳選擇大稻埕作為鐵路開工的起點時，他也在此興建了「臺灣機器局」，地點就位於今臺北市北門塔城街附近，主要業務為製造兵器和鐵路機器，兼鑄造貨幣、修理船舶等。日本統治臺灣之後，接收清軍撤退時尚未破壞之鐵路相關機具，1895年10月並將機器局改稱為「臨時臺北兵器修理所」，作為砲兵工廠使用。1899年「臺灣總督府鐵道部」成立，計畫全面興建臺灣西部縱貫鐵路，並借用砲兵工廠部份空間作為「車輛工廠」，之後又改稱為「臺北工場」，此即為「臺北機廠」之前身。隨著鐵路興築進程延伸至全省，以及急速成長的鐵路運輸量，腹地有限的臺北工場，已無法滿足日漸增加的鐵路維修需求，
考量機廠佔地面積大，而且不能與鐵路站體設施距離太遠，因此便選擇了位於臺北華山車站與松山車站間鐵路線上的興雅庄（今信義區），作為新廠的設置地點，而為了有別於舊廠，新廠也暫時命名為「新臺北鐵道工場」。1931年，臺北工場的新建工程終於動工，廠房配置由日本鐵道工程師速水和彥所規劃設計，其規模之浩大，當時號稱是東亞最大的鐵路機廠遷建計畫。興建工程期長達4年，至1935年完工，全廠總面積為193,912平方公尺，廠房面積達57,200平方公尺，完工啟用之後定名為「臺北鐵道工場」，也成為全臺灣最具規模的鐵路車輛維修後勤中心。

[戰後](../Page/戰後台灣.md "wikilink")，該廠區成為[臺灣鐵路管理局首要](../Page/臺灣鐵路管理局.md "wikilink")[車輛基地](../Page/車輛基地.md "wikilink")，直至交接任務與[富岡基地前](../Page/富岡基地.md "wikilink")。

### 業務

該廠區之主要業務保養及維修[電力機車](../Page/電力機車.md "wikilink")、[電聯車](../Page/電聯車.md "wikilink")、[柴電機車](../Page/柴電機車.md "wikilink")。有時，廠區也會負責進行車輛之改造更新工程（如將[平快車改造為](../Page/平快車.md "wikilink")[冷氣平快車](../Page/冷氣平快車.md "wikilink")，又如[太魯閣號之內部裝設整理](../Page/太魯閣號.md "wikilink")）。早年，該廠區曾一度生產[不銹鋼製](../Page/不銹鋼.md "wikilink")[守車出口](../Page/守車.md "wikilink")[泰國](../Page/泰國.md "wikilink")。

### 景觀

廠區內設有一座自落成時留存至今、設計風格受[現代主義影響的員工澡堂](../Page/現代主義.md "wikilink")；該地曾為電影《[天台](../Page/天台_\(電影\).md "wikilink")》拍攝場景之一，也是廠區第一個被指定為臺北市[市定古蹟的建築](../Page/市定古蹟.md "wikilink")。\[1\]
法國導演[盧貝松電影](../Page/盧貝松.md "wikilink")《[露西](../Page/露西_\(電影\).md "wikilink")》也曾在機廠周邊取景。

### 組織編制

臺北機廠原設有人事室、會計室、政風室、總務室、勞工安全衛生室，並另有技術組、工作組、材料組與廠內數座工場\[2\]。大多組織人員現已搬至富岡基地，僅留少數[保全人員](../Page/保安人員.md "wikilink")。

### 鐵路運轉

在[縱貫線](../Page/縱貫線_\(北段\).md "wikilink")[臺北](../Page/台北車站.md "wikilink")＝[松山區間地面四線時期](../Page/松山車站_\(台灣\).md "wikilink")，其最南側一線為[華山站管理之站外](../Page/華山車站.md "wikilink")[側線](../Page/侧线_\(铁路\).md "wikilink")，並被稱為臺北機廠側線；華山站主要站外專用側線都是由此線分出。

在廠區外圍設有員工車站（無正式站名，內部稱呼**臺北機廠前**），平常工作日有兩班[通勤電聯車供員工上下班搭乘](../Page/台鐵通勤電聯車.md "wikilink")\[3\]；離開機廠的列車也需在此等候調度；往松山站方向則進入地下段路線，可經由路線出土段至松山站，由松山行車室管制發車。

在2010年代初期，廠區路線為縱貫線[汐科](../Page/汐科車站.md "wikilink")（不含）＝[板橋地下化區間中唯一地面軌道區間](../Page/板橋車站_\(臺灣\).md "wikilink")，並於至臺北站方向設有[折返線](../Page/折返式路線.md "wikilink")，以供列車折返進入廠區（由於設有此一折返線，一列[推拉式自強號列車維修或改裝需分二至三次完成](../Page/推拉式自強號.md "wikilink")）

2012年1月31日，臺北機廠與松山車站間的連絡線（機廠側線）正式停用並拆除。

### 公車站牌

過往，[臺北聯營公車在](../Page/臺北聯營公車.md "wikilink")[八德路上設有](../Page/八德路_\(臺北市\).md "wikilink")「鐵路機廠」站牌。在[京華城興建後](../Page/京華城.md "wikilink")，[市民大道上新設站位被命名為](../Page/市民大道.md "wikilink")「京華城」；「鐵路機廠」站位在京華城前門，「京華城站」位在京華城後門與廠區前門。

站牌英文名稱依臺鐵命名應為，但臺北聯營公車業者[英譯為](../Page/翻译.md "wikilink")，[臺北捷運公司則譯為](../Page/臺北捷運公司.md "wikilink")。

後來，行經鐵路機廠站位的所有路線陸續將站名更新為「京華城」，不同的英譯站名才消失\[4\]。臺北捷運公司在更新地圖時也予以更正。

## 機廠搬遷

[鐵路局台北機廠澡堂_1.jpg](https://zh.wikipedia.org/wiki/File:鐵路局台北機廠澡堂_1.jpg "fig:鐵路局台北機廠澡堂_1.jpg")
[鐵路局台北機廠澡堂_2.jpg](https://zh.wikipedia.org/wiki/File:鐵路局台北機廠澡堂_2.jpg "fig:鐵路局台北機廠澡堂_2.jpg")

  - 由於機廠出土段路線已確定於2011年6月前須交與[台灣高速鐵路作](../Page/台灣高速鐵路.md "wikilink")「[臺北](../Page/臺北車站.md "wikilink")─[松山](../Page/松山車站_\(台灣\).md "wikilink")」地下段南隧道形成[平面交叉](../Page/平面交叉.md "wikilink")，故廠區須準時終止運作遷出。最初，相關單位計畫將廠區業務遷至臺中市[大肚區大肚](../Page/大肚區.md "wikilink")[調車場](../Page/調車場.md "wikilink")，後亦曾計畫將之轉移至甫新建完工之[七堵調車場或](../Page/七堵調車場.md "wikilink")[池上車站附近](../Page/池上車站_\(臺灣\).md "wikilink")，不過如此計畫均未被付諸實行。
  - 廠區業務原被規劃分割為三個部份，並分別遷至[富岡車站附近](../Page/富岡車站.md "wikilink")（包含[北湖車站計畫](../Page/北湖車站.md "wikilink")，2009年3月31日動工）、[蘇澳新站附近以及潮州調車場](../Page/蘇澳新站.md "wikilink")；廠區原址則計畫開發為[商業中心](../Page/商業中心區.md "wikilink")、[商務旅館和](../Page/商務旅遊.md "wikilink")[住宅區](../Page/住宅.md "wikilink")。\[5\]\[6\]
  - 2011年8月，所有待[三級檢修](../Page/三级检查.md "wikilink")(全檢)車輛的階段性檢修作業完成，部份設備也已移至富岡基地。
  - 2012年1月31日，臺北機廠與[松山車站間之連絡線](../Page/松山車站_\(臺灣\).md "wikilink")（機廠[側線](../Page/侧线_\(铁路\).md "wikilink")）正式停用並拆除，機廠員工上下班[通勤列車及臺北市區僅存地面](../Page/通勤列车.md "wikilink")[月台宣告走入歷史](../Page/車站月台.md "wikilink")。
  - 在過渡期間，列車車輛的三級檢修（全檢）作業，暫改所屬機務段負責；機廠僅負責引擎、馬達等由列車卸下之大型組件維修工作（由鐵路運送至[七堵調車場後](../Page/七堵調車場.md "wikilink")，再放上卡車經[公路運送](../Page/公路.md "wikilink")）。
  - 2012年6月，完成遷出並終止運作。但是，隨著2008年9月21日臺北市臺鐵路線完全地下化，加上廠區擁有之歷史文化價值，已有要求大規模保留之倡議。\[7\]

## 機廠保留

  - 2011年5月18日，[臺北市議員](../Page/臺北市議員.md "wikilink")[簡余晏](../Page/簡余晏.md "wikilink")、[李慶鋒及立法委員](../Page/李慶鋒.md "wikilink")[王幸男前往臺北機廠會勘](../Page/王幸男.md "wikilink")，提出臺北機廠完整保存之訴求，並認為[臺北市政府僅將機廠澡堂列入](../Page/臺北市政府.md "wikilink")[古蹟乃](../Page/古蹟.md "wikilink")「本末倒置」之舉。\[8\]
    [文化資產專家及相關](../Page/文化資產.md "wikilink")[學者呼籲當局務必保留整座廠區](../Page/學者.md "wikilink")。\[9\]除[鐵道文化協會外](../Page/中華民國鐵道文化協會.md "wikilink")，國外鐵道團體亦投入聲援；各種建議紛紛出爐，如就地改為鐵道[博物館](../Page/博物館.md "wikilink")\[10\]\[11\]\[12\]、建設「臺版[奧賽館](../Page/奧賽博物館.md "wikilink")」\[13\]等等。
  - 2013年1月，各界向臺鐵請願；1月14日，臺北市政府將廠區組立工場、鍛冶工場、原動室列為市定古蹟，並預定於當年[暑假於廠區舉辦活動](../Page/暑假.md "wikilink")。\[14\]

<!-- end list -->

  - 2014年1月13日，在立法委員[鄭麗君提案要求下](../Page/鄭麗君.md "wikilink")，[文化部文化資產局召開](../Page/文化部文化資產局.md "wikilink")「臺北機廠之保存與再發展」公聽會\[15\]；9月3日，臺北市市長參選人[柯文哲在立法委員](../Page/柯文哲.md "wikilink")[管碧玲邀請下參訪臺北機廠](../Page/管碧玲.md "wikilink")，由鐵道文化協會代表[古庭維與管碧玲國會辦公室副主任陳林頌偕同導覽](../Page/古庭維.md "wikilink")；會後，柯文哲表示「臺北機廠應全區保存、整體規劃、分期投資、開發不得破壞歷史紋理」。\[16\]
  - 2014年9月24日，管碧玲在[立法院第](../Page/立法院.md "wikilink")8屆第6會期交通委員會第2次全體委員會議提案並經全體表決無異議通過，要求[交通部於一週內發函](../Page/中華民國交通部.md "wikilink")[文化部提出臺北機廠全區指定為](../Page/中華民國文化部.md "wikilink")[國定古蹟](../Page/國定古蹟.md "wikilink")，與將製造於1889年的蒸汽動力鎚等機器登錄為[國寶與](../Page/國寶.md "wikilink")[重要古物](../Page/重要古物.md "wikilink")。\[17\]
  - 2014年9月25日，[臺北市政府都市計畫委員會通過相關開發案件](../Page/臺北市政府都市計畫委員會.md "wikilink")。
  - 2014年10月14日，管碧玲再度於立法院內政委員會提案並經全體表決無異議通過，要求[內政部於臺北機廠定古蹟審議定案後始得辦理相關](../Page/中華民國內政部.md "wikilink")[都市計畫審議案](../Page/都市計畫.md "wikilink")；但是，[臺灣鐵路管理局也對此提出法律異議](../Page/臺灣鐵路管理局.md "wikilink")，導致文化部陷入國定古蹟審議是否適法之相關問題，並決議暫不啟動國定古蹟審議。
  - 2015年1月19日，柯文哲在當選臺北市長後再度由管碧玲、[許陽明](../Page/許陽明.md "wikilink")、[陳林頌](../Page/陳林頌.md "wikilink")、[吳思瑤等人安排陪同參訪](../Page/吳思瑤.md "wikilink")，並於臺北市政府與臺北市副市長[林欽榮](../Page/林欽榮.md "wikilink")、[臺北市政府都市發展局局長](../Page/臺北市政府都市發展局.md "wikilink")[林洲民](../Page/林洲民.md "wikilink")、[臺北市政府文化局局長](../Page/臺北市政府文化局.md "wikilink")[倪重華等人共同研商臺北機廠保存活化並達成全區保存指定國定古蹟之共識](../Page/倪重華.md "wikilink")。
  - 2015年1月21日，管碧玲在立法院一百零四年度國家總預算審查時提出國家總預算[主決議](../Page/主動議.md "wikilink")，要求[臺灣鐵路管理局應行文表明遵照立法院交通委員會之決議](../Page/臺灣鐵路管理局.md "wikilink")，且應於公告後一週內向文化部提供國定古蹟審議所需要之表冊資料。此一主決議最後經[朝野協商](../Page/朝野協商.md "wikilink")[三讀通過後交付](../Page/三讀.md "wikilink")[總統公告](../Page/中華民國總統.md "wikilink")；由於此決議經立院三讀通過具有法律效力及強制約束力，因此成為文化部正式啟動國定古蹟審查關鍵之一。
  - 2015年3月15日，在[民間團體](../Page/社会团体.md "wikilink")、[學者](../Page/學者.md "wikilink")、[民代長期強力呼籲下](../Page/民意代表.md "wikilink")，文化部終於依「古蹟指定及廢止審查辦法」第3條第1項第1款前往臺北機廠進行現場勘查，並依前述辦法第3條第1項第2款開始進行審議。除[中華民國鐵道文化協會會長](../Page/中華民國鐵道文化協會.md "wikilink")[任恆毅](../Page/任恆毅.md "wikilink")、[洪致文](../Page/洪致文.md "wikilink")、[林奎妙](../Page/林奎妙.md "wikilink")（臺北機廠文史守護聯盟代表）、[黃立品列席發言爭取指定國定古蹟外](../Page/黃立品.md "wikilink")，[李宜修](../Page/李宜修.md "wikilink")（鄭麗君委員國會辦公室代表）與[許陽明及陳林頌](../Page/許陽明.md "wikilink")（管碧玲委員國會辦公室代表）
    列席審議委員會說明國定古蹟臺北機廠提案內容及與臺北市政府協商之結論。最後，委員會經出席14位委員一致同意依《[文化資產保存法](../Page/文化資產保存法.md "wikilink")》第14條及前述準則與辦法之規定審議通過，將「臺北機廠」指定為[國定古蹟](../Page/國定古蹟.md "wikilink")。\[18\]
    此後，[臺灣鐵路管理局因期望透過土地開發償還巨額](../Page/臺灣鐵路管理局.md "wikilink")[債務之計畫遭此決議干預](../Page/債務.md "wikilink")，其相關人士紛紛提出異議。此外，臺北機廠的維護經費來源與責任歸屬也仍屬未明，並由相關當局互相協調解決。\[19\]\[20\]
  - 2015年7月9日，[行政院指示文化部建置國家級鐵道博物館](../Page/行政院.md "wikilink")，並由文化部擬具「臺北機廠活化轉型國家鐵道博物館園區實施計畫」及相關委託計畫。\[21\]
  - 2017年8月1日，於日本埼玉市[鐵道博物館舉辦](../Page/鐵道博物館_\(日本\).md "wikilink")2節（MOHANE582-106及MOHANE583-106）捐贈[國立台灣博物館簽約儀式](../Page/國立台灣博物館.md "wikilink")。（出席者為[駐日代表](../Page/臺北駐日經濟文化代表處.md "wikilink")[謝長廷](../Page/謝長廷.md "wikilink")、文化部部長[鄭麗君](../Page/鄭麗君.md "wikilink")、國立臺灣博物館館長洪世佑\[22\]。

[File:Mohane582-106.jpg|MOHANE（モハネ）582-106](File:Mohane582-106.jpg%7CMOHANE（モハネ）582-106)
[File:Mohane583-106.jpg|MOHANE（モハネ）583-106](File:Mohane583-106.jpg%7CMOHANE（モハネ）583-106)
[File:臺北機廠-鐵道博物館外觀.jpg|臺北機廠鐵道博物館園區籌備小組辦公室大門](File:臺北機廠-鐵道博物館外觀.jpg%7C臺北機廠鐵道博物館園區籌備小組辦公室大門)

## 鄰近車站

  - **廢止營運模式**

## 引用、附註及參考

## 相關條目

  - [臺灣鐵路管理局組織編制](../Page/臺灣鐵路管理局組織編制.md "wikilink")
  - [高雄機廠](../Page/高雄機廠.md "wikilink")
  - [花蓮機廠](../Page/花蓮機廠.md "wikilink")
  - [臺鐵富岡車輛基地](../Page/臺鐵富岡車輛基地.md "wikilink")
  - [彰化扇形車庫](../Page/彰化扇形車庫.md "wikilink")

## 外部鍵結

  - [臺北機廠鐵道博物館園區](https://trw.moc.gov.tw)
  - [《鐵路局臺北機廠歷史與其鐵道產業文化資產之基礎研究》](https://www.boch.gov.tw/information_192_49303.html)
  - [臺北機廠遷建建設計畫 -
    交通部臺灣鐵路管理局](http://www.railway.gov.tw/tw/cp.aspx?sn=3660)
  - [台北機廠文史守護聯盟網站](http://railway.net.tw)

[Category:臺鐵組織編制](../Category/臺鐵組織編制.md "wikilink")
[Category:台北市古蹟](../Category/台北市古蹟.md "wikilink")
[Category:台灣日治時期交通建築](../Category/台灣日治時期交通建築.md "wikilink")
[Category:信義區 (臺北市)](../Category/信義區_\(臺北市\).md "wikilink")
[Category:1939年台灣建立](../Category/1939年台灣建立.md "wikilink")
[北](../Category/臺灣鐵路文化資產.md "wikilink")

1.  [廠區建築介紹](https://trw.moc.gov.tw/Building)臺北機廠鐵道博物館園區預定地
2.  [交通部臺灣鐵路管理局臺北機廠辦事細則](http://law.moj.gov.tw/LawClass/LawAll.aspx?PCode=K0010071)
3.  本線直通的北廠員工專列最後為上午2506B-2511A次，及下午2720B-2539A次，之後改為單組EMU100或三組EMU300擔任
4.  鐵路機廠站位的站牌，最後更名為「京華城」的是[臺北客運營運的臺北聯營公車](../Page/臺北客運.md "wikilink")667線。不過，在舊款木製旗桿式站牌尚未全數更換前，仍有舊路線圖使用此名。
5.
6.
7.  洪致文⊙鐵道時光‧氣象時間：[用文化創意保存台北機廠](http://www.wretch.cc/blog/hungchihwen/10639938)
8.
9.
10.
11.
12.
13.
14.
15. 文化部 -
    最新公告：[「臺北機廠之保存與再發展」公聽會](http://www.moc.gov.tw/information_253_21339.html)

16.
17.
18.
19.
20.
21.
22.