**梁芷珊**（****，），香港[企業家](../Page/企業家.md "wikilink")、[市場推廣](../Page/市場推廣.md "wikilink")[專家](../Page/專家.md "wikilink")、[填詞人](../Page/填詞人.md "wikilink")、[作家](../Page/作家.md "wikilink")、人物專訪[記者](../Page/記者.md "wikilink")、[電影及](../Page/電影.md "wikilink")[廣播劇](../Page/廣播劇.md "wikilink")[編劇](../Page/編劇.md "wikilink")、[電視及](../Page/電視.md "wikilink")[電台](../Page/電台.md "wikilink")[節目主持](../Page/節目主持.md "wikilink")，現時在[Maxi
House
Limited擔任行政總裁](../Page/Maxi_House_Limited.md "wikilink")，兼任[香港足球總會董事及市務傳訊委員會主席](../Page/香港足球總會.md "wikilink")；被譽為香港「四大才女」之一。

[Maxi House
Limited專營國際性市場推廣項目](../Page/Maxi_House_Limited.md "wikilink")，亦管理運動專才、作家、米芝蓮廚師。

## 簡歷

梁芷珊曾就讀[聖士提反堂中學](../Page/聖士提反堂中學.md "wikilink")，先後畢業於[庇理羅士女子中學及](../Page/庇理羅士女子中學.md "wikilink")[香港理工大學](../Page/香港理工大學.md "wikilink")。她於[學生時期兼任](../Page/學生.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")，於1990年起為[流行歌曲](../Page/流行歌曲.md "wikilink")[填詞](../Page/填詞.md "wikilink")。大學畢業後於知名時裝品牌擔任市場推廣工作，位至管理層，並且繼續以[業餘身份填詞](../Page/業餘.md "wikilink")，至今作品逾400首，代表作品包括《唯獨你是不可取替》《假使有日能忘記》《一生愛你一個》《取消資格》等經典情歌；與此同時，她亦編寫[廣播劇和小說](../Page/廣播劇.md "wikilink")，並且擔任電影[編劇](../Page/編劇.md "wikilink")，包括《[愛情白麵包](../Page/愛情白麵包.md "wikilink")》及《[四葉草](../Page/四葉草.md "wikilink")》系列。1998年，她推出3本[長篇小說及一本](../Page/長篇小說.md "wikilink")[自傳](../Page/自傳.md "wikilink")[散文](../Page/散文.md "wikilink")，於1999年起全職[寫作](../Page/寫作.md "wikilink")，並且開始在[香港報章及雜誌上撰寫專欄](../Page/香港報章.md "wikilink")，包括《蘋果日報》、《東方日報》、《太陽報》、《經濟日報》、《Cosmopolitan》及《Elle》等，其後又主持[電視及電台節目](../Page/電視節目.md "wikilink")。

2006年，梁芷珊為其前夫為足球部主任的[南華足球隊統籌市場策劃及市場推廣工作](../Page/南華足球隊.md "wikilink")，協助該足球隊於短短一年內，吸引逾20個國際著名品牌贊助。亦由於此，梁芷珊於翌年獲得[香港管理專業協會頒發](../Page/香港管理專業協會.md "wikilink")「2007
HKMA/[TVB傑出市場策劃獎](../Page/TVB.md "wikilink")」。

2008年，梁芷珊成為於該年成立的[天水圍飛馬足球會的創會董事之一](../Page/天水圍飛馬足球會.md "wikilink")\[1\]，被球迷指為[陳瑤琴後另外一位熱心推動香港足球發展的女士](../Page/陳瑤琴.md "wikilink")。

2009年，梁芷珊獲得中國百名傑出女企業家獎，在[中國大陸](../Page/中國大陸.md "wikilink")[北京](../Page/北京.md "wikilink")[人民大會堂領獎](../Page/人民大會堂.md "wikilink")。同年，她出任[保良局總理至今](../Page/保良局.md "wikilink")，參與保良局董事會會議，就保良局各項重要政策及服務的推展作出決定；於擔任的同年，她參與成立保良局足球發展基金，推動本地青少年足球發展。

2015-16球季起，擔任[香港飛馬足球會主席](../Page/香港飛馬足球會.md "wikilink")、2018離任。

2015年起擔任香港足球總會董事，香港足球總會市務及傳訊委員會主席。

## 個人生活

2002年羅傑承與梁芷珊結婚。十五年來二人一直是圈中公認的模範夫婦，而婚後梁芷珊為協助丈夫事業上的大小事務，減少文壇創作。

2017年，二人辦妥離婚手續，低調離婚。

2018年，梁芷珊有了新的開始，與法籍三星米芝蓮廚師Olivier Elzer拍拖\[2\]。

2018年，梁芷珊女兒成為IGCSE 11A狀元。<ref>
[1](https://www.youtube.com/watch?v=jOy1edEoUxA)

離婚後開展新一頁，梁芷珊50歲創業，創立萬事屋�。<ref>
[2](https://bka.mpweekly.com/focus/local/%E5%B0%81%E9%9D%A2%E6%95%85%E4%BA%8B/20180728-124774)

## 作品

### 小說／散文集

#### 香港出版

1.  214個夢（長篇小說）
2.  我愛的人愛我的人（長篇小說）
3.  交換28磅快樂（長篇小說）
4.  其實我介意（長篇小說）　
5.  我的六個身份（散文）
6.  生命白麵包（長篇小說）
7.  單身女人心情色（散文）
8.  單身生活感言（散文）
9.  想得美（美容散文）
10. 尋找最後一個（長篇小說）
11. 愛情號外（e小說系列）
12. 愛情是死去‧活來（散文）
13. 粉紅色套裝（長篇小說）
14. 美麗魔法書（美容散文）
15. 愛沒有如果（長篇小說）　
16. 最後答案：愛情（e小說系列）
17. 換句話講我愛你（愛情金句）
18. 換句話講我不愛你（愛情金句）
19. 換句話講我掛念你（愛情金句）
20. 米白色套裝（長篇小說）
21. cm日記交流 I（交流系列）
22. cm日記交流 II（交流系列）
23. cm日記交流 III（交流系列）
24. cm日記交流 IV（交流系列）
25. 好想好想你（愛情金句）
26. 喜有貓你（漫畫）
27. 日記生情（交流系列）
28. 好狠愛情（散文）
29. 日記茜珊下（交流系列）
30. 換句話講我愛你（二）（愛情金句）
31. 換句話講我掛念你（二）（愛情金句）
32. 日記茜珊下（2）（交流系列）
33. 最好愛情（e小說系列）
34. 深藍色套裝（長篇小說）　
35. 錯愛都市（長篇小說）
36. 愛情‧生活可以很美麗（散文）
37. [當四葉草碰上劍尖時](../Page/當四葉草碰上劍尖時.md "wikilink")（電視劇集改編小說）
38. 當四葉草遇上劍尖時――我們的紀念冊（散文）
39. 每個人都有一棵屬於自己的咖啡樹（長篇小說）
40. 200個愛和不愛的理由（散文）
41. [赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")（[TVB電視劇集同名小說](../Page/TVB.md "wikilink")）
42. 赤沙印記@四葉草.2 漫畫版（上）（（[TVB電視劇集同名漫畫](../Page/TVB.md "wikilink")）
43. 赤沙印記@四葉草.2 漫畫版（下）（（[TVB電視劇集同名漫畫](../Page/TVB.md "wikilink")）
44. 我是不是屬於你的那顆星星（長篇小說）
45. 愛是藍色的火（小說食譜）
46. 天幕下的戀人（[TVB電視劇集同名小說](../Page/TVB.md "wikilink")）
47. 100個有生之年必須擁有的愛情感覺（散文）
48. 十萬個愛甚麼（散文）
49. 十萬個愛甚麼 甜蜜‧繪本（繪本）
50. 十萬個戀甚麼（散文）
51. My desktop（散文）
52. 我的魔鬼上司（散文）

#### 台灣出版

1.  我愛的人‧愛我的人（長篇小說）
2.  交換28磅快樂（長篇小說）
3.  幸運草的約定（電視劇集小說）
4.  每個人都有一棵屬於自己的咖啡樹（長篇小說）
5.  我是不是屬於你的那顆星星（長篇小說）

#### 簡體版

1.  十萬個愛甚麼（散文）

#### 其他

[談一場不後悔的戀愛](../Page/談一場不後悔的戀愛.md "wikilink")（與趙學而的唱片《Inside》隨碟附送）

### 填詞作品

#### 1991年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/梁漢文.md" title="wikilink">梁漢文</a> - 想著你等著你<strong>（第一首發表作品）</strong></li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 不來的季節</li>
</ul></td>
<td><ul>
<li>鄭秀文 - 告別溫室</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1992年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/李家明_(香港).md" title="wikilink">李家明</a> - 不老的情人</li>
<li>李家明 - 單戀的不幸</li>
<li><a href="../Page/張立基.md" title="wikilink">張立基</a> - 夢中情人</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 徹夜纏綿</li>
<li>許志安 - 容我欣賞妳</li>
<li>許志安 - 一個答案</li>
</ul></td>
<td><ul>
<li><a href="../Page/劉小慧.md" title="wikilink">劉小慧</a> - 飄遠飄近</li>
<li><a href="../Page/蔡濟文.md" title="wikilink">蔡濟文</a> - Say Yes</li>
<li>蔡濟文 - 誰曾愛我</li>
<li><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a>、<a href="../Page/吳君如.md" title="wikilink">吳君如</a> - 只因你心醉</li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - It's Too Late</li>
<li><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a> - 我不灑脫</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1993年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/Girl_Friends.md" title="wikilink">Girl Friends</a> - 彩色世界</li>
<li>Girl Friends - 十三點</li>
<li><a href="../Page/王菲.md" title="wikilink">王　菲</a> - 夜半醉</li>
<li><a href="../Page/王馨平.md" title="wikilink">王馨平</a> - 難忘初戀</li>
<li>王馨平 - 祈禱</li>
<li>王馨平 - 愛一生也不夠</li>
<li><a href="../Page/吳君如.md" title="wikilink">吳君如</a> - 不可輕看</li>
<li><a href="../Page/李國祥_(歌手).md" title="wikilink">李國祥</a> - 俏佳人</li>
<li><a href="../Page/周慧敏.md" title="wikilink">周慧敏</a> - Invite Me To Dance</li>
<li><a href="../Page/李家明_(香港).md" title="wikilink">李家明</a> - 傷心多一次</li>
<li><a href="../Page/徐鎮東.md" title="wikilink">徐鎮東</a> - 流星的傳說</li>
<li>徐鎮東 - 一個願望</li>
</ul></td>
<td><ul>
<li><a href="../Page/莫文蔚.md" title="wikilink">莫文蔚</a> - 來吧</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 從沒這麼愛戀過</li>
<li>許志安 - 綑妳在情網</li>
<li>許志安 - 唯獨你是不可取替</li>
<li>許志安 - 雨天晴天</li>
<li><a href="../Page/彭羚.md" title="wikilink">彭　羚</a> - 魔鬼天使拉鋸戰</li>
<li><a href="../Page/葉蘊儀.md" title="wikilink">葉蘊儀</a> - 我是否很傻</li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 天生妒忌</li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 絕情斜陽</li>
<li>鄭秀文 - 總算為情認真過（與張美賢合填）</li>
<li><a href="../Page/黎明詩.md" title="wikilink">黎明詩</a> - 真愛</li>
<li><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a> - 割愛</li>
<li>蘇永康 - 愛是這樣微妙</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1994年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/王馨平.md" title="wikilink">王馨平</a> - 冰火</li>
<li>王馨平 - 理想情人</li>
<li><a href="../Page/江希文.md" title="wikilink">江希文</a> - 一觸即發</li>
<li>江希文 - 輕的感覺</li>
<li><a href="../Page/吳奇隆.md" title="wikilink">吳奇隆</a> - 戀愛探險</li>
<li>吳奇隆 - 怎平靜</li>
<li><a href="../Page/李克勤.md" title="wikilink">李克勤</a> - I Love You</li>
<li>李克勤 - 從未忘記你</li>
<li><a href="../Page/張智霖.md" title="wikilink">張智霖</a> - 幸運兒</li>
<li><a href="../Page/張衛健.md" title="wikilink">張衛健</a> - 傾心、傾意、傾神</li>
<li><a href="../Page/梁朝偉.md" title="wikilink">梁朝偉</a> - 原來不懂得戀愛</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 讓我待妳好一點</li>
<li>許志安 - 不嗟不怨</li>
<li>許志安 - 一見如故</li>
<li><a href="../Page/陳山蔥.md" title="wikilink">陳山蔥</a> - 攻佔我的心</li>
</ul></td>
<td><ul>
<li><a href="../Page/陳雅倫.md" title="wikilink">陳雅倫</a> - 心思思</li>
<li>陳雅倫 - -{吓}-!咩話?</li>
<li>陳雅倫 - 我行我素</li>
<li><a href="../Page/蔡濟文.md" title="wikilink">蔡濟文</a> - 時間沖不淡思念</li>
<li><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a> - 一生愛你一個</li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 愛的天際</li>
<li><a href="../Page/黎姿.md" title="wikilink">黎　姿</a> - 白恤衫</li>
<li>黎　姿 - 私奔</li>
<li><a href="../Page/黎瑞蓮.md" title="wikilink">黎瑞蓮</a> - 沒法失去你</li>
<li>黎瑞蓮 - 愛上你太失敗</li>
<li><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a> - 達爾大冒險</li>
<li>蘇永康 - 緣份遊戲</li>
<li>蘇永康 - 上帝製造您給我</li>
<li>蘇永康 - 燈火欄柵處</li>
<li>蘇永康 - 看海的日子</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1995年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/王翠玲.md" title="wikilink">王翠玲</a> - 平平凡凡</li>
<li>王翠玲 - 回味</li>
<li><a href="../Page/古巨基.md" title="wikilink">古巨基</a> - 愛是憑直覺</li>
<li><a href="../Page/孫耀威.md" title="wikilink">孫耀威</a> - 壞作風</li>
<li><a href="../Page/張智霖.md" title="wikilink">張智霖</a> - 無聊地</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 雨天晴天</li>
<li>許志安 - 最完美的畫面</li>
</ul></td>
<td><ul>
<li><a href="../Page/劉小慧.md" title="wikilink">劉小慧</a> - 不可告退</li>
<li><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a> - 因為你</li>
<li><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a> - 一切從年青開始</li>
<li>蘇永康 - 假使有日能忘記</li>
<li>蘇永康 - I Like You</li>
<li><a href="../Page/蘇有朋.md" title="wikilink">蘇有朋</a> - 夏日炎炎</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1996年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/王傑_(歌手).md" title="wikilink">王　傑</a> - 始終我是我</li>
<li><a href="../Page/李樂詩_(歌手).md" title="wikilink">李樂詩</a> - 看不到的愛</li>
<li><a href="../Page/趙學而.md" title="wikilink">趙學而</a> - 忽然戀愛</li>
<li><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a> - 末世男女</li>
</ul></td>
<td><ul>
<li><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a> - 從來未發生</li>
<li>蘇永康 - 放肆</li>
<li>蘇永康 - 早抖</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1997年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/紀炎炎.md" title="wikilink">紀炎炎</a> - 無重狀態</li>
<li><a href="../Page/胡諾言.md" title="wikilink">胡諾言</a> - 錯誤引導</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 抱歉</li>
</ul></td>
<td><ul>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 情變</li>
<li><a href="../Page/黎駿.md" title="wikilink">黎　駿</a> - 浪子心情</li>
<li><a href="../Page/譚小環.md" title="wikilink">譚小環</a> - 被愛</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1998年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/流氓樂團.md" title="wikilink">流氓樂團</a> - 流氓</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 一家一減你</li>
<li>許志安 - 真心真意</li>
<li><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳</a> - 深呼吸</li>
<li><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a> -怕黑</li>
</ul></td>
<td><ul>
<li><a href="../Page/謝霆鋒.md" title="wikilink">謝霆鋒</a> - 早知</li>
<li><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a> - 不想獨自快樂</li>
<li>蘇永康 - 開玩笑</li>
</ul></td>
</tr>
</tbody>
</table>

#### 1999年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/何嘉莉.md" title="wikilink">何嘉莉</a> - 非份之想</li>
<li>何嘉莉 - I Believe</li>
<li><a href="../Page/吳奇隆.md" title="wikilink">吳奇隆</a> - 你不識愛我</li>
<li>吳奇隆 - 我心不死</li>
<li><a href="../Page/張栢芝.md" title="wikilink">張栢芝</a> - 一齣短片</li>
<li><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳</a> - 多開心</li>
<li>陳慧琳 - 愛怎麼了斷</li>
<li>陳慧琳 - 都是你的錯</li>
<li>陳慧琳、<a href="../Page/鄭中基.md" title="wikilink">鄭中基</a> - 都是你的錯（合唱版）</li>
<li><a href="../Page/陳慧嫻.md" title="wikilink">陳慧嫻</a> - 甚麼都想要</li>
</ul></td>
<td><ul>
<li>陳慧嫻 - 靈犀</li>
<li>陳慧嫻 - 我也有懦弱的時候</li>
<li><a href="../Page/陳曉東_(藝人).md" title="wikilink">陳曉東</a> - 這就是生活</li>
<li>陳曉東 - 簡約主義</li>
<li>陳曉東 - 東芝月色</li>
<li><a href="../Page/趙學而.md" title="wikilink">趙學而</a> - 自欺欺人</li>
<li><a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a> - 唯獨你一個</li>
<li>鄭秀文 - 宿命主義</li>
<li>鄭秀文 - 不可多得</li>
<li>林心如 - 心跳</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2000年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/B2_(組合).md" title="wikilink">B2</a> - 要求太高</li>
<li><a href="../Page/小雪_(香港).md" title="wikilink">小　雪</a> - 有你是榮幸</li>
<li>小　雪 - 這分鐘不愛我</li>
<li><a href="../Page/王傑_(歌手).md" title="wikilink">王　傑</a> - 若要人不知</li>
<li>王　傑 - 退燒</li>
<li>王　傑 - 泡沬情感</li>
<li>王　傑 - 還原</li>
<li>王　傑 - 想你知道</li>
<li>王　傑 - 頭上一片天</li>
<li><a href="../Page/何潤東.md" title="wikilink">何潤東</a> - 醉生夢</li>
<li>何潤東 - 我偏心</li>
<li><a href="../Page/林漢洋.md" title="wikilink">林漢洋</a> - 怪你</li>
<li>林漢洋 - 戀愛</li>
<li><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a> - Goodbye</li>
<li>容祖兒 - 穿花蝴蝶</li>
<li>容祖兒 - 敬請留步</li>
</ul></td>
<td><ul>
<li>容祖兒 - 好女孩</li>
<li>容祖兒 - 撈針</li>
<li>容祖兒、<a href="../Page/梁奕倫.md" title="wikilink">梁奕倫</a> - 雙腳著地</li>
<li><a href="../Page/張家輝.md" title="wikilink">張家輝</a> - 愛得起</li>
<li>張家輝 - 下意識</li>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 好男不與女鬥</li>
<li><a href="../Page/陳慧嫻.md" title="wikilink">陳慧嫻</a> - 睡公主</li>
<li><a href="../Page/彭羚.md" title="wikilink">彭　羚</a> - 相對論</li>
<li><a href="../Page/葉佩雯.md" title="wikilink">葉佩雯</a> - 大食懶加菲</li>
<li><a href="../Page/謝霆鋒.md" title="wikilink">謝霆鋒</a> - 一擊即中</li>
<li>謝霆鋒 - 蒸發</li>
<li>謝霆鋒 - 罪人</li>
<li>謝霆鋒 - 別來無恙</li>
<li>謝霆鋒 - 魔鬼的主意</li>
<li><a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a> - 火花</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2001年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/B2_(組合).md" title="wikilink">B2</a> - 型男索女</li>
<li>B2 - 愛情速遞</li>
<li><a href="../Page/Twins.md" title="wikilink">Twins</a> - 盲頭烏蠅</li>
<li><a href="../Page/王傑_(歌手).md" title="wikilink">王　傑</a> - 情已經失去了</li>
<li><a href="../Page/林漢洋.md" title="wikilink">林漢洋</a> - 一時忘記</li>
<li>林漢洋、<a href="../Page/小雪_(香港).md" title="wikilink">小　雪</a> - 其實我介意</li>
<li><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a> - 擁抱天下</li>
<li><a href="../Page/馬浚偉.md" title="wikilink">馬浚偉</a> - 我最關心</li>
<li>馬浚偉 - 重視你</li>
<li><a href="../Page/張家輝.md" title="wikilink">張家輝</a> - 學會忘記</li>
<li><a href="../Page/張德豪.md" title="wikilink">張德豪</a> - 順其自然</li>
<li><a href="../Page/陳小春.md" title="wikilink">陳小春</a> - 取消資格</li>
<li><a href="../Page/陳冠希.md" title="wikilink">陳冠希</a> - 雙手插袋</li>
</ul></td>
<td><ul>
<li>陳冠希 - 心中最愛</li>
<li>陳冠希 - 緋聞</li>
<li>陳冠希 - 生化戀</li>
<li>陳冠希 - 寒冬</li>
<li><a href="../Page/陳潔儀_(新加坡).md" title="wikilink">陳潔儀</a> - 不要</li>
<li>陳潔儀、<a href="../Page/林保怡.md" title="wikilink">林保怡</a> - 不知不覺</li>
<li><a href="../Page/葉佩雯.md" title="wikilink">葉佩雯</a> - 沒有心情哭</li>
<li>葉佩雯 - 拿得起 放不低</li>
<li><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a> - 老實說話</li>
<li><a href="../Page/鍾兆康.md" title="wikilink">鍾兆康</a>、<a href="../Page/汪敏.md" title="wikilink">汪　敏</a>、<a href="../Page/張佳佳.md" title="wikilink">張佳佳</a> - 天馬行空</li>
<li><a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a> - 腹語</li>
<li>羅嘉良 - 心中有數</li>
<li>羅嘉良、<a href="../Page/趙學而.md" title="wikilink">趙學而</a> - 愛多錯多</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2002年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/B2_(組合).md" title="wikilink">B2</a> - 愛得不清醒</li>
<li>B2 - 自己知自己事</li>
<li>B2 - Kissy Kissy</li>
<li><a href="../Page/小雪_(香港).md" title="wikilink">小　雪</a> - 心甘情願</li>
<li>小　雪 - 糊塗夢醒</li>
<li>小　雪、<a href="../Page/葉文輝.md" title="wikilink">葉文輝</a> - 方寸大亂</li>
<li><a href="../Page/王傑_(歌手).md" title="wikilink">王　傑</a> - 愛不起</li>
<li>王　傑 - 找一個地方</li>
<li><a href="../Page/王嘉明_(香港).md" title="wikilink">王嘉明</a> - 差一分鐘</li>
<li><a href="../Page/陳冠希.md" title="wikilink">陳冠希</a> - 信者得救</li>
</ul></td>
<td><ul>
<li>陳冠希 - 愛沒有罪</li>
<li>陳冠希 - 誰讓你哭了</li>
<li><a href="../Page/陳奕迅.md" title="wikilink">陳奕迅</a>、<a href="../Page/容祖兒.md" title="wikilink">容祖兒</a> - 美好世界</li>
<li><a href="../Page/關心妍.md" title="wikilink">關心妍</a> - 負擔不起</li>
<li>關心妍 - 心安理得</li>
<li>關心妍 - Hope it is not too late</li>
<li>關心妍 - 手下敗將</li>
<li>關心妍 - 辛苦大家</li>
<li>關心妍 - 音樂女神</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2003年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/Cookies_(組合).md" title="wikilink">Cookies</a> - 眼淺</li>
<li><a href="../Page/傅珮嘉.md" title="wikilink">傅珮嘉</a> - 垃圾筒</li>
<li><a href="../Page/關心妍.md" title="wikilink">關心妍</a> - 關心…心妍</li>
<li>關心妍 - 優先訂座</li>
<li>關心妍 - 戰爭不是答案</li>
<li>關心妍 - 我太強</li>
<li>關心妍 - Peace No War</li>
</ul></td>
<td><ul>
<li>關心妍 - 接受現實（與關心妍合填）</li>
<li>關心妍 - 劍尖碰上我的心</li>
<li>關心妍 - 寶馬</li>
<li>關心妍 - 無罪釋放</li>
<li>關心妍 - 秘密花園</li>
<li>關心妍 - 收復失地</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2004年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/傅珮嘉.md" title="wikilink">傅珮嘉</a> - 一夜成名（與傅珮嘉合填）</li>
</ul></td>
<td><ul>
<li><a href="../Page/關心妍.md" title="wikilink">關心妍</a> - 無痛失戀（與傅珮嘉合填）</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2005年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/吳卓羲.md" title="wikilink">吳卓羲</a> - 愛馬仕小姐</li>
<li>吳卓羲 - 過三關</li>
<li>吳卓羲 - 別人問起</li>
</ul></td>
<td><ul>
<li>吳卓羲 - 1+1=2</li>
<li><a href="../Page/關心妍.md" title="wikilink">關心妍</a> - 高竇</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2006年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/周麗淇.md" title="wikilink">周麗淇</a> - 上海人</li>
</ul></td>
<td><ul>
<li><a href="../Page/關心妍.md" title="wikilink">關心妍</a> - 粉筆字</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2007年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/王友良.md" title="wikilink">王友良</a> - 合格</li>
<li>王友良 - 談何容易</li>
</ul></td>
<td><ul>
<li><a href="../Page/謝文雅.md" title="wikilink">謝文雅</a> - 我要成名</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2008年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/V_(組合).md" title="wikilink">V</a> - 南華歌（與簡俊毅合填）</li>
</ul></td>
<td><ul>
<li><a href="../Page/關心妍.md" title="wikilink">關心妍</a> - 如有雷同</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2011年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/紅色力量.md" title="wikilink">紅色力量</a> - 正選</li>
</ul></td>
<td><ul>
<li>紅色力量、<a href="../Page/周國賢.md" title="wikilink">周國賢</a> - We Are The Best</li>
</ul></td>
</tr>
</tbody>
</table>

#### 2014年

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/許志安.md" title="wikilink">許志安</a> - 晚安</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 《[他來自江湖](../Page/他來自江湖.md "wikilink")》飾 林美琪（Maggie）（1989）

### 演出MV

  - [黎　明](../Page/黎明.md "wikilink") - OH\!夜（1991）
  - [鄭伊健](../Page/鄭伊健.md "wikilink") - Love is All（1992）
  - [張學友](../Page/張學友.md "wikilink") - 來來回回（1994）
  - [張學友](../Page/張學友.md "wikilink") - 你知不知道（1994）

## 相關

  - [企業公關](../Page/公共關係.md "wikilink")

## 外部参考

  - LINKEDIN[3](https://www.linkedin.com/in/cannyleungchishan/?originalSubdomain=hk)
  - [博美娛樂集團](../Page/博美娛樂集團.md "wikilink")

## 参考资料

## 参考文献

  - [2019
    蘋果日報－蘋人誌－余家強訪問梁芷珊－6種身份 2種男人](http://s.nextmedia.com/apple/a.php?i=20190409&sec_id=4104&s=0&a=20651781)

  -
  -
  -
  - [梁芷珊 Roadshow blog
    對得住自己](http://www.roadshow.hk/blog-spotting/living/entry/2013-11-02-17-19-06.html)

  - \[<http://hk.apple.nextmedia.com/supplement/culture/art/20130416/18229072>　梁芷珊
    蘋果日報 登高見博\]

  - [百年傑出女企業家＿梁芷珊](http://blog.sina.cn/dpool/blog/s/blog_659abccf0100hyup.html?tj=1)

  - [bma.com.hk](http://www.bma.com.hk/)

  -
  -
  -
[category:保良局己丑年總理](../Page/category:保良局己丑年總理.md "wikilink")

[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港小說家](../Category/香港小說家.md "wikilink")
[Category:香港填詞人](../Category/香港填詞人.md "wikilink")
[Category:香港女性编剧](../Category/香港女性编剧.md "wikilink")
[Category:香港记者](../Category/香港记者.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港理工大學校友](../Category/香港理工大學校友.md "wikilink")
[Category:庇理羅士女子中學校友](../Category/庇理羅士女子中學校友.md "wikilink")
[Category:聖士提反堂中學校友](../Category/聖士提反堂中學校友.md "wikilink")
[Z](../Category/梁姓.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港足球界人士](../Category/香港足球界人士.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")

1.  <http://www.tswpegasus.com/zh/club_d.php?nid=16&id=19> 天水圍飛馬董事會
2.  [【低調離婚】曾因婚姻問題暴瘦
    梁芷珊速戀法籍三星廚](https://bka.mpweekly.com/focus/local/%E5%B0%81%E9%9D%A2%E6%95%85%E4%BA%8B/20180214-102108)