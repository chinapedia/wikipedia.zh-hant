**TV戰士**（**てれび戦士**，てれびせんし）是[NHK教育台受歡迎的節目](../Page/NHK教育台.md "wikilink")「[天才兒童MAX](../Page/天才兒童MAX.md "wikilink")」中作為主角的青少年童星。

本項目為了方便起見，也把TV戰士以外的經常性演出者一併記述在此。

## TV戰士

對象是從小學2年級～國中2年級。NHK再三表示過、**並不開放一般人參加TV戰士的甄選**。全部都是藝能事務所旗下的童星，也就是所謂的專業人員。（因此，並沒有進行過所謂的公開甄選會。而且因為是由NHK的人選來決定的關係，新成員的加入・畢業・殘留都完全取決於NHK的意見）。

### 男性

  - [栗山祐哉](../Page/栗山祐哉.md "wikilink")（初代 1993～1996）
  - [清野努](../Page/清野努.md "wikilink")（初代 1993～1995）
  - [小林一裕](../Page/小林一裕.md "wikilink")（初代 1993～1995）
  - [中條慎太郎](../Page/中條慎太郎.md "wikilink")（初代 1993～1995）
  - [小林三義](../Page/小林三義.md "wikilink")（2代目 1995）
  - [斉藤拓実](../Page/斉藤拓実.md "wikilink")（2代目 1995）
  - [Wentz瑛士](../Page/Wentz瑛士.md "wikilink")（ウエンツ瑛士）（2代目 1995～1999）
  - [相ヶ瀬龍史](../Page/相ヶ瀬龍史.md "wikilink")（2代目 1995～1997）
  - [佐藤北斗](../Page/佐藤北斗.md "wikilink")（3代目 1996）
  - [生田斗真](../Page/生田斗真.md "wikilink")（3代目 1996～1997）
  - [坂田慎一郎](../Page/坂田慎一郎.md "wikilink")（3代目 1996）
  - [伊東亮輔](../Page/伊東亮輔.md "wikilink")（3代目 1996～1997）
  - [伊藤俊輔](../Page/伊藤俊輔_\(talent\).md "wikilink")（4代目 1997～2000）
  - [海道亮平](../Page/海道亮平.md "wikilink")（4代目 1997～1998）
  - [松浦顕一郎](../Page/松浦顕一郎.md "wikilink")（4代目 1997～1998）
  - [橋田紘緒](../Page/橋田紘緒.md "wikilink")（4代目 1997～2000）
  - [ジェームス・マーティン](../Page/ジミーMackey.md "wikilink")（5代目 1998～1999）
  - [安藤奏](../Page/安藤奏.md "wikilink")（5代目 1998）
  - [山元竜一](../Page/山元竜一.md "wikilink")（5代目 1998～2003）
  - [福田亮太](../Page/福田亮太.md "wikilink")（6代目 1999～2000）
  - [須田泰大](../Page/須田泰大.md "wikilink")（6代目 1999～2000）
  - [エバンス太郎](../Page/エバンス太郎.md "wikilink")（7代目 2000～2001）
  - [熊木翔](../Page/熊木翔.md "wikilink")（7代目 2000～2002）
  - [松下博昭](../Page/松下博昭.md "wikilink")（7代目 2000）
  - [ローブリィ翔](../Page/ローブリィ翔.md "wikilink")（7代目 2000～2002）
  - [竪山隼太](../Page/竪山隼太.md "wikilink")（8代目 2001）
  - [松井蘭丸](../Page/松井蘭丸.md "wikilink")（8代目 2001～2002）
  - [井出卓也](../Page/井出卓也.md "wikilink")（8代目 2001～2004）
  - [八木俊彦](../Page/八木俊彦.md "wikilink")（8代目 2001～2003）
  - [ブライアン・ウォルターズ](../Page/ブライアン・ウォルターズ.md "wikilink")（9代目 2002～2003）
  - [堀江幸生](../Page/堀江幸生.md "wikilink")（9代目 2002～2004）
  - [マイケル・メンツァー](../Page/Michael.md "wikilink")（9代目 2002～2003）
  - [前田公輝](../Page/前田公輝.md "wikilink")（10代目 2003～2005）
  - [de・Lencquesaing望](../Page/de・Lencquesaing望.md "wikilink")（10代目
    2003～2005）
  - [張沢紫星](../Page/張沢紫星.md "wikilink")（10代目 2003～2004）
  - [洸太Lacey](../Page/洸太Lacey.md "wikilink")（洸太レイシー）（11代目 2004～2006）
  - [Barnes勇氣](../Page/Barnes勇氣.md "wikilink")（バーンズ勇気）（11代目 2004～2006）
  - [高橋郁哉](../Page/高橋郁哉.md "wikilink")（11代目 2004～2006）
  - [千秋Lacey](../Page/千秋Lacey.md "wikilink")（11代目 2004～2007）
  - [永島謙二郎](../Page/永島謙二郎.md "wikilink")（12代目 2005～2006）
  - [木村遼希](../Page/木村遼希.md "wikilink")（12代目 2005～2007）
  - [笠原拓巳](../Page/笠原拓巳.md "wikilink")（12代目 2005～）
  - [Ryan](../Page/藤田Ryan.md "wikilink")（藤田ライアン）（12代目 2005～2007）
  - [日向滉一](../Page/日向滉一.md "wikilink")（13代目 2006～2007）
  - [小關裕太](../Page/小關裕太.md "wikilink")（小関裕太）（13代目 2006～2008）
  - [千葉一磨](../Page/千葉一磨.md "wikilink")（13代目 2006～）
  - [渡邉聖斗](../Page/渡邉聖斗.md "wikilink")（14代目 2007～2008）
  - [吉野翔太](../Page/吉野翔太.md "wikilink")（14代目 2007～2008）
  - [荒木次元](../Page/荒木次元.md "wikilink")（14代目 2007～）
  - [丸山瀨南](../Page/丸山瀨南.md "wikilink")（丸山瀬南）（14代目 2007～2008）
  - [Benjamin](../Page/Mitchell_Benjamin.md "wikilink")（ミッチェル・ベンジャミン|ベンジャミン）
  - [武田聖夜](../Page/武田聖夜.md "wikilink")（15代目 2008～）
  - [島田翼](../Page/島田翼.md "wikilink")（15代目 2008）
  - [伊藤元太](../Page/伊藤元太.md "wikilink")（15代目 2008～）
  - [田中理來](../Page/田中理來.md "wikilink")（田中理来）（15代目 2008）
  - [木村遼](../Page/木村遼.md "wikilink")（15代目 2008～）
  - [齊藤稜駿](../Page/齊藤稜駿.md "wikilink")（16代目 2009～）
  - [上妻成吾](../Page/上妻成吾.md "wikilink")（16代目 2009～）
  - [鈴木純一朗](../Page/鈴木純一朗.md "wikilink")（16代目 2009～）
  - [長江崚行](../Page/長江崚行.md "wikilink")（16代目 2009～）

### 女性

  - [クリスティー](../Page/クリスティー_\(タレント\).md "wikilink")（クリスティー・夏実・コーサル，初代
    1993～1995）
  - [須山彩](../Page/須山彩.md "wikilink")（初代 1993～1995）
  - [山口美沙](../Page/山口美沙.md "wikilink")（初代 1993～1995
    ※2006年度現場直播「**快樂星期四\!**」主持人）
  - [ジェニファー・ハースト](../Page/ジェニファー・ハースト.md "wikilink")（初代 1993～1995）
  - [田原加奈子](../Page/田原加奈子.md "wikilink")（初代 1993～1995）
  - [三星眞奈美](../Page/三星眞奈美.md "wikilink")（2代目 1995）
  - [漆野友美](../Page/漆野友美.md "wikilink")（2代目 1995）
  - [阿部七絵](../Page/阿部七絵.md "wikilink")（2代目 1995～1997）
  - [渋谷桃子](../Page/渋谷桃子.md "wikilink")（2代目 1995）
  - [鈴木愛可](../Page/鈴木愛可.md "wikilink")（3代目 1996）
  - [保里優紀](../Page/保里優紀.md "wikilink")（3代目 1996）
  - [大塚清華](../Page/大塚清華.md "wikilink")（3代目 1996）
  - [ジェニファー・ペリマン](../Page/ジェニファー・ペリマン.md "wikilink")（3代目 1996～1997）
  - [篠原麻里](../Page/篠原麻里.md "wikilink")（3代目 1996～1997）
  - [前田亞季](../Page/前田亞季.md "wikilink")（前田亜季）（3代目 1996～1997）
  - [ジャスミン・アレン](../Page/ジャスミン・アレン.md "wikilink")（3代目 1996～1999）
  - [棚橋由希](../Page/棚橋由希.md "wikilink")（3代目 1996～1999）
  - [饗場詩野](../Page/饗場詩野.md "wikilink")（3代目 1996～2000）
  - [松川佳以](../Page/松川佳以.md "wikilink")（4代目 1997～1998）
  - [田中樹里](../Page/田中樹里.md "wikilink")（4代目 1997～1998）
  - [中田亞好美(中田あすみ)](../Page/中田亞好美\(中田あすみ\).md "wikilink")（5代目 1998～2001）
  - [佐久間信子](../Page/佐久間信子.md "wikilink")（5代目 1998～2000）
  - [有紗](../Page/有紗.md "wikilink")（ダーブロウ有紗，5代目 1998～2001）
  - [石部里紗](../Page/矢嶋里紗.md "wikilink")（6代目 1999）
  - [大沢あかね](../Page/大沢あかね.md "wikilink")（6代目 1999）
  - [モニーク・ローズ](../Page/モニーク・ローズ.md "wikilink")（6代目 1999～2001）
  - [徐桑安](../Page/徐桑安.md "wikilink")（6代目 1999～2000）
  - [安斎舞](../Page/安齊舞.md "wikilink")（6代目 1999～2002）
  - [石田比奈子](../Page/石田比奈子.md "wikilink")（7代目 2000）
  - [逵優希](../Page/逵優希.md "wikilink")（7代目 2000）
  - [俵有希子](../Page/俵有希子.md "wikilink")（7代目 2000～2002）
  - [村田千宏](../Page/村田Chihiro.md "wikilink")（村田ちひろ）（7代目
    2000～2005　※從2001年開始以**村田Chihiro**作為別名，但從2002年起統一稱為村田Chihiro）
  - [村上東奈](../Page/村上東奈.md "wikilink")（8代目 2001～2002）
  - [岩井七世](../Page/岩井七世.md "wikilink")（8代目 2001～2003）
  - [俵小百合](../Page/俵小百合.md "wikilink")（8代目 2001～2003）
  - [中村有沙](../Page/中村有沙.md "wikilink")（8代目 2001～2004）
  - [豕瀬志穗](../Page/豕瀬志穗.md "wikilink")（8代目 2001～2003）
  - [白木杏奈](../Page/杏奈_\(タレント\).md "wikilink")（9代目 2002～2004）
  - [飯田里穗](../Page/飯田里穗.md "wikilink")（9代目 2002～2005）
  - [ジョアン・ヤマザキ](../Page/ジョアン_\(タレント\).md "wikilink")（9代目 2002～2004）
  - [堀口美咲](../Page/堀口美咲.md "wikilink")（10代目 2003）
  - [桜井結花](../Page/桜井結花.md "wikilink")（10代目 2003）
  - [近藤Emma](../Page/近藤Emma.md "wikilink")（近藤エマ）（10代目 2003～2005）
  - [樹音](../Page/樹音.md "wikilink")（川崎樹音，10代目 2003～2008）
  - [篠原愛實](../Page/篠原愛實.md "wikilink")（篠原愛実）（11代目 2004～2006）
  - [橋本甜歌](../Page/橋本甜歌.md "wikilink")（11代目 2004～）
  - [伊倉愛美](../Page/伊倉愛美.md "wikilink")（11代目 2004～）
  - [淺野優梨愛](../Page/淺野優梨愛.md "wikilink")（浅野優梨愛）（11代目 2004～2005）
  - [木內江莉](../Page/木內江莉.md "wikilink")（）（12代目 2005～）
  - [一木有海](../Page/一木有海.md "wikilink")（12代目 2005～2007）
  - [木內梨生奈](../Page/木內梨生奈.md "wikilink")（）（12代目 2005～2007）
  - [藤本七海](../Page/藤本七海.md "wikilink")（12代目 2005～2006）
  - [大木梓彩](../Page/大木梓彩.md "wikilink")（13代目 2006）
  - [細川藍](../Page/細川藍.md "wikilink")（13代目 2006～2007）
  - [渡邊Elly](../Page/渡邊Elly.md "wikilink")（渡邊エリー）（13代目 2006～2007）
  - [細田羅夢](../Page/細田羅夢.md "wikilink")（13代目 2006～2008）
  - [加藤Gina](../Page/加藤Gina.md "wikilink")（加藤ジーナ）（13代目 2006～）
  - [長谷川明里](../Page/長谷川明里.md "wikilink")（長谷川あかり）（14代目 2007～）
  - [藤井千帆](../Page/藤井千帆.md "wikilink")（14代目 2007～）
  - [鍋本帆乃香](../Page/鍋本帆乃香.md "wikilink")（14代目 2007～）
  - [Melody
    Chuback](../Page/Melody_Chuback.md "wikilink")（メロディー・チューバック）（14代目
    2007～）
  - [松尾瑠璃](../Page/松尾瑠璃.md "wikilink")（14代目 2007）
  - [重本小鳥](../Page/重本小鳥.md "wikilink")（重本ことり）（15代目 2008～）
  - [山田樹里亞](../Page/山田樹里亞.md "wikilink")（15代目 2008）
  - [中村彩乃](../Page/中村彩乃.md "wikilink")（中村あやの）（15代目 2008～）
  - [水本凛](../Page/水本凛.md "wikilink")（15代目 2008～）
  - [鈴木美知代](../Page/鈴木美知代.md "wikilink")（16代目 2009～）
  - [脇菜菜香](../Page/脇菜菜香.md "wikilink")（脇菜々香）（16代目 2009～）
  - [平田真優香](../Page/平田真優香.md "wikilink")（16代目 2009～）
  - [渡邊青萊](../Page/渡邊青萊.md "wikilink")（16代目 2009～）
  - [鎮西壽壽歌](../Page/鎮西壽壽歌.md "wikilink")（鎮西寿々歌）（16代目 2009～）
  - [白坂奈奈](../Page/白坂奈奈.md "wikilink")（白坂奈々）（16代目 2009～）

<!-- end list -->

1.  生田斗真、伊東亮輔在演出當時是[傑尼斯Jr.成員](../Page/傑尼斯Jr..md "wikilink")。（生田現在也是屬於[傑尼斯Jr.](../Page/傑尼斯Jr..md "wikilink")）
2.  マイケル・メンツァー在節目途中把藝名改成現在的Mikey。
3.  兄弟姊妹也是TV戰士
    1.  俵有希子（姐）・俵小百合（妹）
    2.  洸太Lacey（兄）・千秋Lacey（弟）
4.  節目中的別名如下。
    1.  クリスティー=クリスティー・夏実・コーサル
    2.  有紗=ダーブロウ有紗
    3.  ジョアン=ジョアン・ヤマザキ
    4.  樹音=川崎樹音
    5.  コウタ・レイシー=洸太Lacey（洸太レイシー）
    6.  チアキ・レイシー=千秋Lacey（千秋レイシー）
5.  篠原麻里是在前年度的*'ポコ・ア・ポコ*裡的固定班底。在*'天ドラ*裡也客串演出。
6.  前田亞季是在前年度的**悪夢の王**裡的固定班底。是在**ミステリーの館**裡固定班底[前田愛的妹妹](../Page/前田愛_\(女演員\).md "wikilink")。
7.  相ヶ瀬龍史畢業後、曾在**ザ・ゴーストカンパニー**裡客串演出。
8.  伊藤俊輔、石部里紗（矢嶋里紗）在畢業後曾在**天ドラ**裡客串演出。
9.  安斎舞在2000年度途中改名為安齊舞。
10. 村田千宏在2001年度以村田Chihiro（村田ちひろ）為別名（在2002年度正式改名）。
11. 白木杏奈在2004年度末改名為杏奈。
12. 岩井七世在畢業後在姊妹節目[天才ビットくん裡演出生活科教師的角色](../Page/天才ビットくん.md "wikilink")。
13. [山口美沙在](../Page/山口美沙.md "wikilink")2006年度現場直播**快樂星期四\!**中擔任主持人。
14. Ryan（ライアン）把藤田Ryan（藤田ライアン）作為別名，但是在2006年度正式改名。

## 歴代經常性參與演出者

### 主持人

  - 初代 -
    [鴕鳥俱樂部](../Page/鴕鳥俱樂部.md "wikilink")（[搞笑三人組](../Page/搞笑藝人.md "wikilink")、1993年4月播出～）
      - [バカルディ](../Page/さまぁ～ず.md "wikilink")（1994－1995年星期五版**クイズ電脳バトラー**）
  - 2代目 -
    [キャイ～ン](../Page/キャイ～ン.md "wikilink")（[搞笑搭檔](../Page/搞笑搭檔.md "wikilink")、1996年4月－）
  - 3代目 -
    [山崎邦正](../Page/山崎邦正.md "wikilink")（[搞笑藝人](../Page/搞笑藝人.md "wikilink")）、[リサ・ステッグマイヤー](../Page/リサ・ステッグマイヤー.md "wikilink")（[talent](../Page/talent.md "wikilink")）（1998年4月－）
  - 4代目 -
    [角田信朗](../Page/角田信朗.md "wikilink")（[talent](../Page/talent.md "wikilink")、當時是[格闘家](../Page/格闘家.md "wikilink")）、[山川惠里佳](../Page/山川惠里佳.md "wikilink")（[talent](../Page/talent.md "wikilink")）（2001年4月－）
  - 5代目 -
    [極楽とんぼ](../Page/極楽とんぼ.md "wikilink")（[搞笑搭檔](../Page/搞笑搭檔.md "wikilink")、2002年4月－）
  - 6代目 -
    [TIM](../Page/TIM.md "wikilink")（[搞笑搭檔](../Page/搞笑搭檔.md "wikilink")、2003年4月－2007年3月）
  - 7代目 -
    [ハリセンボン](../Page/ハリセンボン.md "wikilink")（[搞笑搭檔](../Page/搞笑搭檔.md "wikilink")、2007年4月－現在）
      - [井上Ma-](../Page/井上Ma-.md "wikilink")、[山口美沙](../Page/山口美沙.md "wikilink")（2006年星期四現場直播**快樂星期四\!**）

<!-- end list -->

1.  山川惠里佳降格為平Regular、在2002年**SF (?) スペース新喜劇
    ラフィン★スター**、2003年**こちらHK学園笑芸部\!**裡演出。

### 播報員

  - [大澤幹朗](../Page/大澤幹朗.md "wikilink")（2004～）

### 採訪者

  - [ペコちゃん](../Page/オオタスセリ.md "wikilink")（1993～1995）
  - [早瀬ちさと](../Page/早瀬ちさと.md "wikilink")（1996～1997）

### 講師

  - [タケカワユキヒデ](../Page/タケカワユキヒデ.md "wikilink")（1994～1995音樂）
  - [陸川章](../Page/陸川章.md "wikilink")（1996籃球）
  - [渋谷浩](../Page/渋谷浩.md "wikilink")（1998桌球）
  - [T-ASADA](../Page/T-ASADA.md "wikilink")（1998～舞蹈・節目的舞蹈編舞，指導師）
  - [夏井一季](../Page/夏井一季.md "wikilink")（夏井いつき）（1999俳句，2006天TV任務的一部分）
  - [清水ミチコ](../Page/清水ミチコ.md "wikilink")（1999～2000,2003）
  - [広瀬香美](../Page/広瀬香美.md "wikilink")（2000音楽）
  - [種ともこ](../Page/種ともこ.md "wikilink")（2001）

### 其他

#### 男性

  - [安藤一志](../Page/安藤一志.md "wikilink")（1994）
  - [大沼勇](../Page/南イサム.md "wikilink")（1994）
  - [明石健太郎](../Page/明石健太郎.md "wikilink")（1994）
  - [新井健](../Page/新井健.md "wikilink")（1994～1995）
  - [杉山丈二](../Page/杉山丈二.md "wikilink")（1994～1995）
  - [柴田綾太](../Page/柴田綾太.md "wikilink")（1994）
  - [雨宮章郎](../Page/雨宮章郎.md "wikilink")（1994）
  - [立沢貴明](../Page/立沢貴明.md "wikilink")（1994）
  - [便辺弁](../Page/便辺弁.md "wikilink")（1994）
  - [翠敏夫](../Page/翠敏夫.md "wikilink")（1994）
  - [高田瑞紀](../Page/高田瑞紀.md "wikilink")（1994）
  - [岡本康行](../Page/岡本康行.md "wikilink")（1994～1995）
  - [伊集院光](../Page/伊集院光.md "wikilink")（1994）
  - [藤間宇宙](../Page/藤間宇宙.md "wikilink")（1995）
  - [樋浦勉](../Page/樋浦勉.md "wikilink")（1995）
  - [げんしじん](../Page/げんしじん.md "wikilink")（1996～2002）
  - [レイパー佐藤](../Page/レイパー佐藤.md "wikilink")（1997）
  - [牟田将士](../Page/牟田将士.md "wikilink")（1997）
  - [木村高人](../Page/木村高人.md "wikilink")（1997）
  - [大滝貴也](../Page/大滝貴也.md "wikilink")（1997）
  - [寺床秀太](../Page/羽柴秀也.md "wikilink")（1997）
  - [入沢大聖](../Page/入沢大聖.md "wikilink")（1998）
  - [立澤真明](../Page/立澤真明.md "wikilink")（1998）
  - [藤村俊二](../Page/藤村俊二.md "wikilink")（1998）
  - [二瓶正也](../Page/二瓶正也.md "wikilink")（1998）
  - [なすび](../Page/なすび_\(talent\).md "wikilink")（2000～2002）
  - [伊達直斗](../Page/伊達直斗.md "wikilink")（2000～2001）
  - [簗瀬憲光](../Page/簗瀬憲光.md "wikilink")（2002,2004）
  - [菅谷智春](../Page/菅谷智春.md "wikilink")（2002）
  - [石田靖](../Page/石田靖.md "wikilink")（2003）
  - [井上Ma-](../Page/井上Ma-.md "wikilink")（2006～）
  - [楠本柊生](../Page/楠本柊生.md "wikilink")（2006）

#### 女性

  - [中村友里子](../Page/中村友里子.md "wikilink")（1993～1994）
  - [石川磨依](../Page/石川磨依.md "wikilink")（1993～1994）
  - [鈴木加奈枝](../Page/鈴木加奈枝.md "wikilink")（1993～1994）
  - [細谷麻衣](../Page/細谷麻衣.md "wikilink")（1993）
  - [金子麻里](../Page/金子麻里.md "wikilink")（1993）
  - [草村麻利子](../Page/草村麻利子.md "wikilink")（1993）
  - [倉沢桃子](../Page/倉沢桃子.md "wikilink")（1993）
  - [石井明日香](../Page/石井明日香.md "wikilink")（1993）
  - [井上愛香](../Page/井上愛香.md "wikilink")（1993）
  - [岡沢千穗梨](../Page/岡沢千穗梨.md "wikilink")（1993）
  - [小泉真由美](../Page/小泉真由美.md "wikilink")（1993）
  - [糀本季央](../Page/糀本季央.md "wikilink")（1993）
  - [斎藤恵里奈](../Page/斎藤恵里奈.md "wikilink")（1993）
  - [菅原彩](../Page/菅原彩.md "wikilink")（1993）
  - [寺嶋あゆみ](../Page/寺嶋あゆみ.md "wikilink")（1993）
  - [富樫麻里](../Page/富樫麻里.md "wikilink")（1993）
  - [西村有香](../Page/西村有香.md "wikilink")（1993）
  - [平野春花](../Page/平野春花.md "wikilink")（1993）
  - [藤田あゆ](../Page/藤田あゆ.md "wikilink")（1993）
  - [藤村陽子](../Page/藤村陽子.md "wikilink")（1993）
  - [本多沙由里](../Page/本多沙由里.md "wikilink")（1993）
  - [三浦遊](../Page/三浦遊.md "wikilink")（1993）
  - [鹿島かんな](../Page/鹿島かんな.md "wikilink")（1994～1995）
  - [藁科みき](../Page/藁科みき.md "wikilink")（1994）
  - [清水真実](../Page/清水真実.md "wikilink")（1994）
  - [金沢明蘭](../Page/金沢明蘭.md "wikilink")（1994）
  - [小野智子](../Page/小野智子.md "wikilink")（1994）
  - [前田愛](../Page/前田愛_\(女演員\).md "wikilink")（1994）
  - [西坂やすよ](../Page/西坂やすよ.md "wikilink")（1994）
  - [篠原麻里](../Page/篠原麻里.md "wikilink")（1995）
  - [冨貴塚桂香](../Page/冨貴塚桂香.md "wikilink")（1995）
  - [星野涼子](../Page/星野涼子.md "wikilink")（1995）
  - [前田亞季](../Page/前田亞季.md "wikilink")（1995）
  - [渡邉まゆみ](../Page/渡邉まゆみ.md "wikilink")（1995）
  - [三浦聡子](../Page/高取茉南.md "wikilink")（1995）
  - [前田倫子](../Page/前田倫子.md "wikilink")（1995）
  - [安室満樹子](../Page/安室満樹子.md "wikilink")（1995）
  - [藤本かをる](../Page/藤本かをる.md "wikilink")（1995）
  - [井上知香](../Page/井上知香.md "wikilink")（1997）
  - [玉城真由美](../Page/玉城真由美.md "wikilink")（1997）
  - [新保はる奈](../Page/新保はる奈.md "wikilink")（1997）
  - [山口明寿香](../Page/あすか_\(女演員\).md "wikilink")（1997）
  - [杉本文乃](../Page/杉本文乃.md "wikilink")（1998）
  - [中村美佳](../Page/中村美佳.md "wikilink")（1998）
  - [谷口紗耶香](../Page/谷口紗耶香.md "wikilink")（1998）
  - [前泊理花](../Page/前泊理花.md "wikilink")（1998）
  - [穴井夕子](../Page/穴井夕子.md "wikilink")（1998）
  - [くまいもとこ](../Page/くまいもとこ.md "wikilink")（1999）
  - [せがわきり](../Page/せがわきり.md "wikilink")（1999）
  - [清水ミチコ](../Page/清水ミチコ.md "wikilink")（1999～2000,2002～2003）
  - [谷口絵梨](../Page/谷口絵梨.md "wikilink")（2000）
  - [金澤あかね](../Page/藤沢かりん.md "wikilink")（2001）
  - [井端珠里](../Page/井端珠里.md "wikilink")（2002）
  - [秋山恵](../Page/秋山恵.md "wikilink")（2004）
  - [加藤夏希](../Page/加藤夏希.md "wikilink")（2004）
  - [伊藤真奈美](../Page/伊藤真奈美.md "wikilink")（2006～）

#### 團體

  - [デンジャラス](../Page/デンジャラス.md "wikilink")（1993）
  - [バカルディ](../Page/さまぁ～ず.md "wikilink")（1994～1995）
  - [どーよ](../Page/どーよ.md "wikilink")（2001～2005）

<!-- end list -->

1.  安藤一志、井上知香在連續劇**ザ・ゴーストカンパニー**裡客串演出。
2.  篠原麻里、前田亞季在翌年度昇格為**TV戰士**。
3.  レイパー佐藤=さとうふみのり
4.  なすび在2004年度夏季公演裡演出。
5.  井端珠里以いはたじゅり的名義在動畫**スージーちゃんとマービー**裡演唱主題曲。

## 歴代CG角色

  - 1993年度－1994年度・1996年度「てっちゃん」（聲優：[千葉繁](../Page/千葉繁.md "wikilink")）
  - 1995年度「玉三郎」（聲優：[梅津秀行](../Page/梅津秀行.md "wikilink")）
  - 1997年度「てつまろ」（聲優：[中尾隆聖](../Page/中尾隆聖.md "wikilink")）
  - 1998年度－1999年度「TKくん」（聲優：[千葉繁](../Page/千葉繁.md "wikilink")）
  - 2000年度「エバラン」（聲優：[江原正士](../Page/江原正士.md "wikilink")）
  - 2001年度「モンゴ」（聲優：[中尾隆聖](../Page/中尾隆聖.md "wikilink")）
  - 2002年度「ブッチョー部長」（聲優：[坂口候一](../Page/坂口候一.md "wikilink")）
  - 2003年度「タマ部長」（聲優：[坂口候一](../Page/坂口候一.md "wikilink")）
      - 2003年度木曜ゲームゾーン：「エトワール」（聲優：[高田由美](../Page/高田由美.md "wikilink")）
  - 2004年度「ラビ零号・ラビ4.8号・ラビ55号・鬼ラビ」（聲優：[坂口候一](../Page/坂口候一.md "wikilink")）、「ラビ88号」（聲優：[高田由美](../Page/高田由美.md "wikilink")）、「ラビロボ」（聲優：？、根據單元不同登場的角色也不同）、魔王（聲優：[加藤夏希](../Page/加藤夏希.md "wikilink")）
  - 2005年度「おんつくん」（聲優：[堀本等](../Page/堀本等.md "wikilink")）
  - 2006年度「おんつ8世」（聲優：[久嶋志帆](../Page/久嶋志帆.md "wikilink")、前年度的角色稱他為「おんつくん」）、「みみぃ」、「びびん★」（聲優：[成田紗矢香](../Page/成田紗矢香.md "wikilink")）、「∞（無限）」（聲優：[千葉繁](../Page/千葉繁.md "wikilink")）

## 關連項目

  - [天才兒童MAX](../Page/天才兒童MAX.md "wikilink")

## 外部連結

  - [天才兒童MAX](http://www3.nhk.or.jp/tvkun)（日本語）

[\*](../Category/TV戰士.md "wikilink")