| [Lalacalle2.jpg](https://zh.wikipedia.org/wiki/File:Lalacalle2.jpg "fig:Lalacalle2.jpg") |
| :--------------------------------------------------------------------------------------: |
|                                           任期:                                            |
|                                           前任:                                            |
|                                           继任:                                            |
|                                           副总统:                                           |
|                                           出生:                                            |
|                                           出生地:                                           |
|                                           职业:                                            |
|                                           党派:                                            |

**路易斯·阿尔贝托·拉卡列**

**路易斯·阿尔贝托·拉卡列·埃雷拉**(**Luis Alberto Lacalle
Herrera**)，1941年7月13日－），[乌拉圭政治家](../Page/乌拉圭.md "wikilink")、曾任[乌拉圭总统](../Page/乌拉圭总统.md "wikilink")。

## 背景

拉卡列生于[乌拉圭](../Page/乌拉圭.md "wikilink")[蒙得维的亚市](../Page/蒙得维的亚.md "wikilink")。

他从乌拉圭共和国大学法律和社会科学系毕业后自1961年成为一位新闻工作者，拉卡列夫人玛丽亚·布里托·德尔皮诺(María Julia Pou
Brito del Pino)(1946-)。他们有三位孩子：Pilar, Luis Alberto和Juan
José。拉卡列在17岁参加国家党(白党)（）。Luis Alberto

1984年当民主恢复，他被选举为参议员和副总统。

## 总统

拉卡列1989年当选总统，在2004年再次竞选总统时失败，败于[塔瓦雷·巴斯克斯](../Page/塔瓦雷·巴斯克斯.md "wikilink")。

[L](../Category/1941年出生.md "wikilink")
[L](../Category/乌拉圭总统.md "wikilink")