[Trident_II_missile_image.jpg](https://zh.wikipedia.org/wiki/File:Trident_II_missile_image.jpg "fig:Trident_II_missile_image.jpg")
[SPb_railway_BShRK_06.JPG](https://zh.wikipedia.org/wiki/File:SPb_railway_BShRK_06.JPG "fig:SPb_railway_BShRK_06.JPG")用[裝甲列車機動發射的弹道导弹](../Page/裝甲列車.md "wikilink")\]\]
**彈道飛彈**是一种[飛彈](../Page/飛彈.md "wikilink")，通常没有翼，在烧完燃料后只能保持预定的航向，不可改变，其后的航向由[弹道学法则支配](../Page/弹道学.md "wikilink")。为了覆盖广大的距离，弹道导弹必需發射很高，进入空中或太空，进行[亚轨道宇宙飞行](../Page/亚轨道.md "wikilink")；对于洲际导弹，中途高度大约为1200公里。当在太空时，不提供推力，只作[自由落体运动](../Page/自由落体.md "wikilink")。

## 結構特性

与弹道导弹相对的概念是[巡航导弹](../Page/巡航导弹.md "wikilink")，后者可以控制自身的飞行轨道。

中远程弹道导弹通常被用于投掷[核弹头](../Page/核弹头.md "wikilink")，因为它们可携带的有效载荷很难保证[常规武器有效摧毁目标](../Page/常规武器.md "wikilink")，而弹头落入大气层时产生的高热往往也会使[生化武器失效](../Page/生化武器.md "wikilink")。

许多先进的弹道导弹由多级[火箭推进](../Page/火箭.md "wikilink")，它们的轨道也能在一定范围内进行调整。

弹道导弹的射程和用途有很大区别，一般来说，弹道导弹根据射程的不同进行分类。

[Minuteman_III_MIRV_path.svg](https://zh.wikipedia.org/wiki/File:Minuteman_III_MIRV_path.svg "fig:Minuteman_III_MIRV_path.svg")

美国的分类方式：

  - **[洲际弹道导弹](../Page/洲际弹道导弹.md "wikilink")（ICBM）**: 射程在5500km以上
  - **[远程弹道导弹](../Page/远程弹道导弹.md "wikilink")（IRBM）**: 射程在3000km至5500km之间
  - **[中程弹道导弹](../Page/中程弹道导弹.md "wikilink")（MRBM）**: 射程在1000km至3000km之间
  - **[短程弹道导弹](../Page/短程弹道导弹.md "wikilink")（SRBM）**: 射程在1000km以下。

另外還有以發射模式和作戰用途區分的[潜射弹道导弹](../Page/潜射弹道导弹.md "wikilink")（SLBM，多屬洲际弹道导弹）和[戰區彈道飛彈](../Page/戰區彈道飛彈.md "wikilink")（TBM，多屬中短程弹道导弹）。

## 歷史

  - 世界上第一种弹道导弹是[纳粹德国研制的](../Page/纳粹德国.md "wikilink")[V2火箭](../Page/V2火箭.md "wikilink")，它也是第一种投入实际使用的弹道导弹。在二战末期，纳粹曾用V-2攻击[英国的城市](../Page/英国.md "wikilink")。
  - 1962年爆發了[古巴飛彈危機](../Page/古巴飛彈危機.md "wikilink")，卡斯楚企圖提供蘇聯飛彈基地以攻擊美國，但蘇聯的飛彈運輸船和護航艦隊被美軍偵察機發現，蘇聯艦隊一度與前來迎擊的美軍艦隊在古巴海域對峙；最後蘇聯運輸船長為防觸發全面核戰，不惜抗命將飛彈運輸船調頭，同時美蘇領導人透過[美蘇熱線](../Page/美蘇熱線.md "wikilink")，化解了古巴飛彈危機。
  - 在古巴飛彈危機發生的同時，美軍也在[西德和](../Page/西德.md "wikilink")[土耳其部署了彈道飛彈對準了](../Page/土耳其.md "wikilink")[東歐](../Page/東歐.md "wikilink")、[高加索等地的蘇聯據點](../Page/高加索.md "wikilink")，企圖與蘇聯相互威脅。
  - 1969年中蘇之間發生[珍寶島戰役](../Page/珍寶島戰役.md "wikilink")，之後中蘇雙方曾將大量的彈道飛彈擺到邊界上相互威脅。
  - 1980年代發生了[兩伊戰爭](../Page/兩伊戰爭.md "wikilink")，[伊朗和](../Page/伊朗.md "wikilink")[伊拉克雙方都使用了彈道飛彈](../Page/伊拉克.md "wikilink")，企圖攻擊各自的首都[德黑蘭和](../Page/德黑蘭.md "wikilink")[巴格達](../Page/巴格達.md "wikilink")；而兩伊戰爭為世界上首次使用配備生化彈頭的彈道飛彈的一場戰爭，伊拉克方面在飛毛腿飛彈上配備了[沙林毒氣造成伊朗軍隊慘重傷亡](../Page/沙林毒氣.md "wikilink")。
  - 1991年的[波斯灣戰爭](../Page/波斯灣戰爭.md "wikilink")，伊拉克廣泛使用了[飛毛腿飛彈](../Page/飛毛腿飛彈.md "wikilink")，企圖攻擊[以色列](../Page/以色列.md "wikilink")、[沙烏地阿拉伯等伊拉克周邊的美國盟邦和前線據點](../Page/沙烏地阿拉伯.md "wikilink")，而美軍、沙軍和以軍則以[愛國者飛彈迎擊](../Page/愛國者飛彈.md "wikilink")。
  - 1996年的[台海飛彈危機](../Page/台海飛彈危機.md "wikilink")，解放軍假借在[福建沿海演習的名義對台灣](../Page/福建.md "wikilink")[基隆和](../Page/基隆.md "wikilink")[高雄外海發射了M族飛彈](../Page/高雄.md "wikilink")，牽動美中台三邊敏感神經，美國遂派遣[第七艦隊從日本南下介入兩岸緊張局勢](../Page/第七艦隊.md "wikilink")，企圖緩和兩岸領導人[江澤民和](../Page/江澤民.md "wikilink")[李登輝間的對立態勢](../Page/李登輝.md "wikilink")。此後，解放軍針對台灣的各型彈道飛彈數量遽增，並在21世紀初擁有千餘枚飛彈，當作「以武促統」的籌碼，尤其在2004年大陸制定針對台灣[陳水扁政府的](../Page/陳水扁政府.md "wikilink")《[反分裂國家法](../Page/反分裂國家法.md "wikilink")》後達到高峰。
  - 2000年代的[朝核問題](../Page/朝核問題.md "wikilink")，北韓不顧國際制裁逕自開發彈道飛彈並實施地下核爆；而北韓在2012年為了慶祝[金日成百歲冥誕](../Page/金日成.md "wikilink")「[太陽節](../Page/太陽節.md "wikilink")」，試射了由「大浦洞」和「蘆洞」彈道飛彈為設計基礎的火箭卻發射失敗。在此同時，南韓的因應之道亦以自製彈道飛彈與北韓抗衡，以[勝利女神飛彈為基礎開發了](../Page/勝利女神飛彈.md "wikilink")[玄武三型彈道飛彈](../Page/玄武三型彈道飛彈.md "wikilink")。北韓方面在2012年底正式宣布成功發射長程火箭，並載運衛星[光明星3號升空](../Page/光明星3號_\(2期\).md "wikilink")。
  - 2000年代印巴雙方不約而同地發展了核子武器和彈道飛彈，其中印度的[烈火五型彈道飛彈射程達五千公里](../Page/烈火五型彈道飛彈.md "wikilink")，理論上具有攻擊中國全境的能力，但政治意義的成分居多。
  - 2010年代伊朗和以色列的意識形態衝突加劇，雙方宣稱要「[相互保證毀滅](../Page/相互保證毀滅.md "wikilink")」；而以色列的耶利哥飛彈和伊朗的流星飛彈都有攻擊對方領土的能力，加劇了中東緊張情勢。

## 指揮權

大部分國家的陸上彈道飛彈都歸由[陸軍管轄](../Page/陸軍.md "wikilink")，而[潛射彈道飛彈則由](../Page/潛射彈道飛彈.md "wikilink")[海軍管轄](../Page/海軍.md "wikilink")，部分國家有建立使用彈道飛彈的專門軍種。

美軍的彈道飛彈是由[美國空軍和](../Page/美國空軍.md "wikilink")[北美防空司令部](../Page/北美防空司令部.md "wikilink")（NORAD）管轄，俄軍的彈道飛彈是由[戰略火箭軍管轄](../Page/俄羅斯戰略火箭軍.md "wikilink")，而中國大陆的彈道飛彈則是由[火箭军管轄](../Page/中国人民解放军火箭军.md "wikilink")。有部分配備核生化武器的彈道飛彈，核生化彈道飛彈的發射指令是由[國家元首](../Page/國家元首.md "wikilink")、[政府首腦](../Page/政府首腦.md "wikilink")（如[英國首相](../Page/英國首相.md "wikilink")、[伊朗最高領袖](../Page/伊朗最高領袖.md "wikilink")）或[中央軍委](../Page/中央軍委.md "wikilink")（如中國大陆、北韓、古巴）下令授權。

## 典型的弹道导弹

[Miss_launch_veh.jpg](https://zh.wikipedia.org/wiki/File:Miss_launch_veh.jpg "fig:Miss_launch_veh.jpg")
[Peacekeeper-missile-testing.jpg](https://zh.wikipedia.org/wiki/File:Peacekeeper-missile-testing.jpg "fig:Peacekeeper-missile-testing.jpg")(MX)[洲際彈道飛彈的多目標重返大氣層載具重返大氣層時的景象](../Page/洲際彈道飛彈.md "wikilink")(遠拍)\]\]
[Agni-II_missile_(Republic_Day_Parade_2004).jpeg](https://zh.wikipedia.org/wiki/File:Agni-II_missile_\(Republic_Day_Parade_2004\).jpeg "fig:Agni-II_missile_(Republic_Day_Parade_2004).jpeg")\]\]

  -

<!-- end list -->

  - [紅石飛彈](../Page/紅石飛彈.md "wikilink")(已退役)
  - [義勇兵洲際彈道飛彈](../Page/LGM-30義勇兵洲際彈道飛彈.md "wikilink")
  - [和平卫士洲际彈道导弹](../Page/LGM-118A和平守護者飛彈.md "wikilink")（已退役）
  - [北极星潜射弹道导弹](../Page/UGM-27北极星导弹.md "wikilink")（已退役）
  - [UGM-73海神导弹](../Page/UGM-73海神导弹.md "wikilink")（已退役）
  - [三叉戟导弹](../Page/三叉戟导弹.md "wikilink")
  - [MGM-134侏儒洲際彈道飛彈](../Page/MGM-134侏儒洲際彈道飛彈.md "wikilink")(未部署)

※美國方面在簽署了《[削減戰略武器條約](../Page/削減戰略武器條約.md "wikilink")》後，依照條約廢止了陸上中短程彈道飛彈和巡弋飛彈的運用。

  - ／

<!-- end list -->

  - R-11/R-300 Elbrus
    （美國代號SS-1，北約代號Scud，即[飞毛腿导弹](../Page/飞毛腿导弹.md "wikilink")）
  - [R-36M](../Page/R-36.md "wikilink") (SS-18，Satan)
  - [RT-23 Molodets](../Page/RT-23_Molodets\(飛彈\).md "wikilink")
    (SS-24，Scalpel，已退役)
  - [白杨](../Page/RT-2PM白楊飛彈.md "wikilink")(SS-25)
  - [RT-2UTTH 白杨-M](../Page/白杨-M洲际弹道导弹.md "wikilink") (SS-27)
  - [RS24亞爾斯洲際彈道飛彈](../Page/RS-24洲際彈道飛彈.md "wikilink") (SS-27-2)

<!-- end list -->

  -

<!-- end list -->

  - [東風-1短程彈道飛彈](../Page/東風-1短程彈道飛彈.md "wikilink")（已退役）
  - [東風-2中程彈道飛彈](../Page/東風-2中程彈道飛彈.md "wikilink")（已退役）
  - [东风-3遠程弹道导弹](../Page/东风-3遠程弹道导弹.md "wikilink")（已退役）
  - [东风-5型洲際弹道导弹](../Page/东风-5型洲際弹道导弹.md "wikilink")
  - [東風-15短程彈道飛彈](../Page/東風-15短程彈道飛彈.md "wikilink")
  - [東風-16中程彈道飛彈](../Page/東風-16中程彈道飛彈.md "wikilink")
  - [東風-21中程彈道飛彈](../Page/東風-21中程彈道飛彈.md "wikilink")
  - [東風-31洲際彈道導彈](../Page/東風-31洲際彈道導彈.md "wikilink")
  - [东风-41型洲際弹道导弹](../Page/东风-41型洲際弹道导弹.md "wikilink")
  - [巨浪1型潜射弹道导弹](../Page/巨浪-1.md "wikilink")
  - [巨浪2型潜射弹道导弹](../Page/巨浪2型潜射弹道导弹.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [哈迪斯式战术导弹](../Page/哈迪斯式战术导弹.md "wikilink")（已退役）
  - [M45潜射弹道导弹](../Page/M45潜射弹道导弹.md "wikilink")
  - [M51潜射弹道导弹](../Page/M51潜射弹道导弹.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [大地式地对地导弹](../Page/大地式地对地导弹.md "wikilink")（[印度](../Page/印度.md "wikilink")）
  - [烈火-3彈道飛彈](../Page/烈火-3彈道飛彈.md "wikilink")（印度）
  - [烈火-4彈道飛彈](../Page/烈火-4彈道飛彈.md "wikilink")（印度）
  - [烈火-5彈道飛彈](../Page/烈火-5彈道飛彈.md "wikilink")（印度）

<!-- end list -->

  - 其他國家

<!-- end list -->

  - [青鋒飛彈](../Page/青鋒飛彈.md "wikilink")（[中華民國](../Page/中華民國.md "wikilink")）
  - [天馬飛彈](../Page/天馬飛彈.md "wikilink")（[中華民國](../Page/中華民國.md "wikilink")）
  - [高里式地对地导弹](../Page/高里式地对地导弹.md "wikilink")（[巴基斯坦](../Page/巴基斯坦.md "wikilink")）
  - [劳动-1](../Page/劳动-1.md "wikilink")（[朝鲜](../Page/朝鲜.md "wikilink")）
  - [杰里科式地对地导弹](../Page/杰里科式地对地导弹.md "wikilink")（[以色列](../Page/以色列.md "wikilink")）
  - [流星3型飛彈](../Page/流星3型飛彈.md "wikilink")（[伊朗](../Page/伊朗.md "wikilink")）
  - [玄武一型彈道飛彈](../Page/玄武一型彈道飛彈.md "wikilink")（[韓國](../Page/韓國.md "wikilink")）
  - [玄武二型彈道飛彈](../Page/玄武二型彈道飛彈.md "wikilink")（[韓國](../Page/韓國.md "wikilink")）

具有发射弹道导弹能力的[潜艇称为](../Page/潜艇.md "wikilink")[弹道导弹潜艇](../Page/弹道导弹潜艇.md "wikilink")。因其发射的导弹主要是战略弹道导弹，故也常称此类潜艇为战略潜艇。

## 主要涉及弹道导弹的国际条约

  - 《[第一阶段削减战略武器条约](../Page/第一阶段削减战略武器条约.md "wikilink")》（START-1）
  - 《[第二阶段削减战略武器条约](../Page/第二阶段削减战略武器条约.md "wikilink")》（START-2）
  - 《[反弹道导弹条约](../Page/反弹道导弹条约.md "wikilink")》
  - 《[苏联和美国清除两国中程和中短程导弹条约](../Page/苏联和美国清除两国中程和中短程导弹条约.md "wikilink")》（《中导条约》）

## 参考文献

## 外部链接

  - [弹道导弹](http://www.fas.org/nuke/intro/missile/index.html)

  - [世界上的弹道导弹](https://web.archive.org/web/20050407232400/http://www.missilethreat.com/missiles/)

## 参见

  - [圓機率誤差](../Page/圓機率誤差.md "wikilink")（CEP）
  - [巡航导弹](../Page/巡航导弹.md "wikilink")
  - [反弹道导弹](../Page/反弹道导弹.md "wikilink")
  - [潜射弹道导弹](../Page/潜射弹道导弹.md "wikilink")（SLBM）
  - [戰區彈道飛彈](../Page/戰區彈道飛彈.md "wikilink")（TBM）
  - [彈道飛彈防禦](../Page/彈道飛彈防禦.md "wikilink")（Ballistic Missile Defense,
    BMD）
  - [戰區飛彈防禦系統](../Page/戰區飛彈防禦系統.md "wikilink")（Theater Missile Defense
    System, TMD）

{{-}}

[Category:导弹类型](../Category/导弹类型.md "wikilink")
[弹道导弹](../Category/弹道导弹.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")