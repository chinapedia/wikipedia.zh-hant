**哈里发**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****
；拉丁化：Caliph），為[哈里發國](../Page/哈里發國.md "wikilink")（khilāfa，意為哈里發統治之地）與[烏瑪的統治者](../Page/烏瑪.md "wikilink")，是[伊斯兰教的宗教及世俗的最高统治者的称号](../Page/伊斯兰教.md "wikilink")。最早源自於，意為繼承者，指先知[穆罕默德的繼承者](../Page/穆罕默德.md "wikilink")。在穆罕默德死後，其弟子以Khalifat
Rasul
Allah（安拉使者的繼承者）為名號，繼續領導伊斯蘭教，隨後被簡化為哈里發，所謂的[四大哈里發為](../Page/四大哈里發.md "wikilink")[巴克爾](../Page/阿布·伯克尔.md "wikilink")、[歐瑪爾](../Page/欧麦尔·本·赫塔卜.md "wikilink")、[奧斯曼和](../Page/奥斯曼·本·阿凡.md "wikilink")[阿里](../Page/阿里·本·阿比·塔利卜.md "wikilink")。

哈里發本為[阿拉伯帝国最高的統治者稱號](../Page/阿拉伯帝国.md "wikilink")，相當於一般所說的[皇帝](../Page/皇帝.md "wikilink")，但同時兼有統治所有[遜尼派](../Page/遜尼派.md "wikilink")[穆斯林的精神領袖的意味](../Page/穆斯林.md "wikilink")，故又可以同時起了類似[天主教的](../Page/天主教.md "wikilink")[教宗的作用](../Page/教宗.md "wikilink")。

在阿拉伯帝国鼎盛时期，哈里发拥有最高权威，管理着庞大的[伊斯兰帝国](../Page/伊斯兰.md "wikilink")。阿拉伯帝国灭亡之后，“哈里发”的头衔，僅作为伊斯兰教宗教领袖的名称，一直保存了下来，[傀儡哈里发处于](../Page/傀儡.md "wikilink")[开罗的](../Page/开罗.md "wikilink")[马木留克王朝的控制之下](../Page/马木留克王朝.md "wikilink")。

1517年，[鄂圖曼帝国](../Page/鄂圖曼帝国.md "wikilink")[苏丹](../Page/苏丹_\(称谓\).md "wikilink")[塞利姆一世征服了](../Page/塞利姆一世.md "wikilink")[埃及](../Page/埃及.md "wikilink")，时任哈里发的[穆塔瓦基勒三世也被俘](../Page/穆塔瓦基勒三世.md "wikilink")。塞利姆一世宣布自己继承哈里发的职位。之後哈里發稱號便由鄂圖曼帝國蘇丹世襲，但1517年之後數百年歷代蘇丹均不常使用該稱號，直到19世紀末鄂圖曼帝國瀕臨解體，蘇丹[阿卜杜勒-哈米德二世才重新強調自己的哈里發身分](../Page/阿卜杜勒-哈米德二世.md "wikilink")。1924年，哈里发制度被[土耳其總統](../Page/土耳其總統.md "wikilink")[凯末尔废除](../Page/凯末尔.md "wikilink")\[1\]，末任哈里发[阿卜杜勒-迈吉德二世被剥夺哈里发头衔并被放逐到海外](../Page/阿卜杜勒-迈吉德二世.md "wikilink")。

2014年6月29日，[極端組織](../Page/極端組織.md "wikilink")“[伊斯兰国](../Page/伊斯兰国.md "wikilink")”領袖[阿布·贝克尔·巴格达迪自立為哈里發](../Page/阿布·贝克尔·巴格达迪.md "wikilink")，稱**易卜拉欣**\[2\]，但未受大部分穆斯林承認。

## 其他

在伊斯蘭教[蘇非派](../Page/蘇非派.md "wikilink")[納格什班迪教團中](../Page/納格什班迪教團.md "wikilink")，与[和卓最親近的學生也被稱為哈里發](../Page/和卓.md "wikilink")。

## 参考文献

## 参见

  - [哈里發國](../Page/哈里發國.md "wikilink")
  - [哈里发列表](../Page/哈里发列表.md "wikilink")
  - [苏丹](../Page/苏丹_\(称谓\).md "wikilink")
  - [两圣地监护人](../Page/两圣地监护人.md "wikilink")
  - [伊玛目](../Page/伊玛目.md "wikilink")
  - [马赫迪](../Page/马赫迪.md "wikilink")

{{-}}

[Category:伊斯兰教阿拉伯语头衔](../Category/伊斯兰教阿拉伯语头衔.md "wikilink")
[哈里發](../Category/哈里發.md "wikilink")
[Category:君主称谓](../Category/君主称谓.md "wikilink")
[Category:伊斯蘭教術語](../Category/伊斯蘭教術語.md "wikilink")

1.
2.