**vBulletin**（有时简称为**vB**，**vBB**）是一个商业[论坛程序](../Page/討論區.md "wikilink")。由Internet
Brands及vBulletin
Solutions開發及銷售，它使用[PHP脚本语言编写](../Page/PHP.md "wikilink")，并以[MySQL為](../Page/MySQL.md "wikilink")[資料庫](../Page/資料庫.md "wikilink")。

## 版本

vBulletin目前主要分成三種版本：

  - SUITE版：包含討論區、[內容管理系統](../Page/內容管理系統.md "wikilink")（CMS）及[網誌等三大產品](../Page/網誌.md "wikilink")，售價285美元。
  - FORUM版：僅有討論區，售價195美元。
  - MOBILE版：產生供[行動裝置瀏覽的網頁程式](../Page/行動裝置.md "wikilink")，需有上述兩版本之一搭配使用，售價199美元。

以上金額為永久使用權，並提供一年內版本更新。

## 參見

  - [論壇](../Page/論壇.md "wikilink")
  - [PHPwind](../Page/PHPwind.md "wikilink")
  - [Discuz\!](../Page/Discuz!.md "wikilink")
  - [PhpBB](../Page/PhpBB.md "wikilink")

## 外部連結

  - [vBulletin.com](http://www.vbulletin.com/) 官方网站

  - [vBulletin.org](http://www.vbulletin.org/) 官方代码及风格模版修改站

[Category:PHP](../Category/PHP.md "wikilink")
[Category:網絡討論區軟體](../Category/網絡討論區軟體.md "wikilink")
[Category:2000年面世的產品](../Category/2000年面世的產品.md "wikilink")