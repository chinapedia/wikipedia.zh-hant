**奧德雷恩县**（**Audrain County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州東部的一個縣](../Page/密蘇里州.md "wikilink")。面積1,805平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口25,853人。縣治[墨西哥](../Page/墨西哥_\(密蘇里州\).md "wikilink")
（Mexico）。

成立於1831年1月12日，1836年縣政府成立。縣名可能紀念當地第一個殖民者薩穋爾·奧德雷恩或[州議員詹姆斯](../Page/密蘇里州州議會.md "wikilink")·H·奧德雷恩
（James H. Audrain）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[A](../Category/密蘇里州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.