《**星空的Spica**》（）是[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")[田村由香里的第](../Page/田村由香里.md "wikilink")12張[單曲](../Page/單曲.md "wikilink")，由[KING
RECORDS於](../Page/KING_RECORDS.md "wikilink")2007年5月9日發行，商品編號為KICM-1210。

## 概要

  - 初回限定版採用Digipak形式的包裝。
  - 這是田村由香里的唱片合約自[Konami轉往KING](../Page/Konami.md "wikilink")
    RECORDS後首張發行單曲\[1\]。
  - 和其他單曲不同的是，此單曲只拍攝宣傳用的硬照和短片，沒有拍攝完整的音樂錄影帶。

## 收錄曲目

1.  \[4:45\]

      - 作詞：椎名可憐／作曲、編曲：太田雅友
      - 動畫《[魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")》前期片尾曲

2.  Sensitive Venus \[4:20\]

      - 作詞：渡邊美佳／作曲、編曲：Dux
      - [Oh\!sama
        TV](../Page/Oh!sama_TV.md "wikilink")「」開首曲（2007年5月25日－2008年2月）

3.  Melody \[4:06\]

      - 作詞、作曲、編曲：田中隼人
      - [文化放送系電台節目](../Page/文化放送.md "wikilink")「」開首曲（2007年4月－2008年1月）
      - Oh\!sama TV「」結尾曲（2007年5月25日－2008年2月）

## 註解

<div class="references-small">

<references />

</div>

[en:Hoshizora no Spica](../Page/en:Hoshizora_no_Spica.md "wikilink")

[Category:2007年單曲](../Category/2007年單曲.md "wikilink")
[Category:田村由香里單曲](../Category/田村由香里單曲.md "wikilink")
[Category:魔法少女奈葉歌曲](../Category/魔法少女奈葉歌曲.md "wikilink")
[Category:電視動畫主題曲](../Category/電視動畫主題曲.md "wikilink")

1.  但田村由香里在Konami的時候，她的唱片製作均由KING RECORDS的VC製作部製作，並由KING RECORDS發行。