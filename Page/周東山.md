**周東山**博士(**CHOW
Tung-Shan**，)，畢業於香港大學，在英國牛津大學進修，及在東京八王子研修院深造。擔任香港政府首長級職位多年，曾出席大量國際會議，接觸無數國內外政、經及學術界高層人士。他曾於[香港特區政府工作](../Page/香港特區政府.md "wikilink")30多年，退休前為[勞工處助理處長](../Page/勞工處.md "wikilink")，\[[https://web.archive.org/web/20050908061538/http://www.qesaausa.org/chow_tungshan.htm\]任內協助](https://web.archive.org/web/20050908061538/http://www.qesaausa.org/chow_tungshan.htm%5D任內協助)[香港](../Page/香港.md "wikilink")[僱員追討](../Page/僱員.md "wikilink")[欠薪](../Page/欠薪.md "wikilink")，對手包括[華懋](../Page/華懋.md "wikilink")[創辦人](../Page/創辦人.md "wikilink")[王德輝](../Page/王德輝.md "wikilink")。[1](http://the-sun.on.cc/channels/news/20070408/20070408023801_0000.html)

周東山於 92 年創立『僱員再培訓局』， 94 年『國際經濟合作發展組織』譽為全球最富創意及最具成本效益的再培訓計劃， 96
年加入aTV，獲『亞洲亞洲管理大獎』。 2004
年提前退休，創立『東山語言中心』，教授包括[普通話](../Page/普通話.md "wikilink")、[英語](../Page/英語.md "wikilink")、[日語](../Page/日語.md "wikilink")、[法語](../Page/法語.md "wikilink")、[德文及](../Page/德文.md "wikilink")[西班牙語](../Page/西班牙語.md "wikilink")[2](https://web.archive.org/web/20071213024241/http://hk.news.yahoo.com/071204/60/2kpxt.html)
，已婚，育有兩名[兒子](../Page/兒子.md "wikilink")。其『東山語言中心』員工要經過八場以上的面試，每一位員工都經過漫長的歷練，漫長的試練，才能成為中心的一份子。比起前僱主香港政府請[AO更嚴格](../Page/AO.md "wikilink")。近年，致力推行老有所依計劃，於其下兩間分校，安排大量長者在前線工作，好讓他們更貼近社會。

2008年，有份客串電視劇《東山飄雨西關晴》「東山儲蓄銀行」執行董事、財政部委員一角。 a

## 参考文献

<div class="references-small">

<references />

</div>

[T](../Category/周姓.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[C](../Category/牛津大學校友.md "wikilink")