[The_Cenotaph_2014.jpg](https://zh.wikipedia.org/wiki/File:The_Cenotaph_2014.jpg "fig:The_Cenotaph_2014.jpg")

**和平紀念碑**是[香港一座](../Page/香港.md "wikilink")[紀念碑](../Page/紀念碑.md "wikilink")，位於[香港的香港島](../Page/香港的香港島.md "wikilink")[中環](../Page/中環.md "wikilink")[遮打道](../Page/遮打道.md "wikilink")，於1923年豎立，原為紀念於[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")[殉職的](../Page/殉職.md "wikilink")[軍人](../Page/軍人.md "wikilink")。[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，紀念碑用以紀念在兩次大戰中殉職的軍人。和平紀念碑屬於[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")\[1\]。

## 設計及歷史

和平紀念碑於1923年5月24日由當時[港督](../Page/港督.md "wikilink")[司徒拔揭幕](../Page/司徒拔.md "wikilink")\[2\]。[和平紀念碑仿傚自](../Page/和平紀念碑.md "wikilink")1920年建成，由[埃德溫·魯琴斯爵士設計的](../Page/埃德溫·魯琴斯.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[白廳](../Page/白廳.md "wikilink")[和平紀念碑](../Page/和平紀念碑_\(倫敦\).md "wikilink")，而[百慕達及](../Page/百慕達.md "wikilink")[奧克蘭的同類紀念碑亦採用同樣設計](../Page/奧克蘭_\(新西蘭\).md "wikilink")。

和平紀念碑原先是為紀念[第一次世界大戰的死難者而建](../Page/第一次世界大戰.md "wikilink")，最初只刻上英文「The
Glorious
Dead」（意即光榮殉難者）字樣及一戰年份「1914-1918」，其後為紀念[第二次世界大戰的死難者](../Page/第二次世界大戰.md "wikilink")，加刻「1939-1945」的年份，直到1981年紀念碑側面加刻「英魂不朽　浩氣長存」八個中文字，以表示和平紀念碑為紀念所有死難者，特別是為港捐軀的陣亡將士而建\[3\]。石碑上方刻有「MCMXIX」[羅馬數字](../Page/羅馬數字.md "wikilink")，代表1919年——《[凡爾賽和約](../Page/凡爾賽和約.md "wikilink")》簽訂的年份。

和平紀念碑屬於古典復興建築風格，外觀設計勻稱，位於階式長方形花崗石地台中央位置，經由四條呈十字形的行人徑到達，四周為平時封閉的草坪，突顯紀念碑的莊嚴肅穆。紀念碑的基座呈階梯形，上面部分採用壁階設計，逐漸向上縮窄，頂部為一長方形石棺，上面飾有石製花圈\[4\]。和平紀念碑是香港惟一正式為紀念第一次世界大戰死難者而建的紀念碑，至今仍然保留原貌\[5\]。

2013年11月22日，古物事務監督根據《古物及古蹟條例》將和平紀念碑列為法定古蹟\[6\]。

[File:HK_Cenotaph_Side.JPG|香港和平紀念碑側面](File:HK_Cenotaph_Side.JPG%7C香港和平紀念碑側面)
[File:Cenotaph.jpg|倫敦白廳的和平紀念碑](File:Cenotaph.jpg%7C倫敦白廳的和平紀念碑)
<File:Rembrance> Day Parade Bermuda.jpg|百慕達的和平紀念碑 <File:AucklandMuseum>
edit gobeirne.jpg|新西蘭奧克蘭的和平紀念碑

## 紀念活動

[1945_liberation_of_Hong_Kong_at_Cenotaph.jpg](https://zh.wikipedia.org/wiki/File:1945_liberation_of_Hong_Kong_at_Cenotaph.jpg "fig:1945_liberation_of_Hong_Kong_at_Cenotaph.jpg")

在[第二次世界大戰前](../Page/第二次世界大戰.md "wikilink")，政府將每年的11月11日訂為[和平紀念日](../Page/和平紀念日_\(香港\).md "wikilink")（即《[康邊停戰協定](../Page/康邊停戰協定.md "wikilink")》簽訂日）港督與殖民地官員每年均出席大型悼念及閱兵儀式。[香港重光後](../Page/香港重光.md "wikilink")，香港政府把「和平紀念日」訂於每年11月第二個星期日，1946年起將8月30日另訂為[香港重光紀念日](../Page/重光紀念日.md "wikilink")，以紀念英國在二次大戰後正式重新管治香港，當天更列為公眾假期。1968年起，重光紀念日改為8月的最後一個星期一。[香港主權移交後](../Page/香港主權移交.md "wikilink")，易名為[抗日戰爭勝利紀念日](../Page/抗日戰爭勝利紀念日.md "wikilink")，日期改為8月15日，即[日本宣佈無條件投降的日子](../Page/日本投降.md "wikilink")。1998年後由於特區政府將[佛誕列為公眾假期](../Page/佛誕.md "wikilink")，抗日戰爭勝利紀念日不再是公眾假期，但每年的兩個紀念日仍會在和平紀念碑進行紀念活動。

以往香港每年均在「和平紀念日」在[香港動植物公園](../Page/香港動植物公園.md "wikilink")、[皇后像廣場及](../Page/皇后像廣場.md "wikilink")[聖約翰座堂三處舉行紀念儀式](../Page/聖約翰座堂.md "wikilink")，但1981年起合併為每年一次在和平紀念碑進行官方儀式，以悼念兩次大戰的死難者。1998年起，官方紀念儀式改於每年[重陽節在](../Page/重陽節.md "wikilink")[香港大會堂紀念龕舉行](../Page/香港大會堂.md "wikilink")，由行政長官及高級官員出席。至於[香港退伍軍人協會](../Page/香港退伍軍人協會.md "wikilink")、[香港前戰俘協會](../Page/香港前戰俘協會.md "wikilink")、[香港義勇軍協會等團體](../Page/香港義勇軍協會.md "wikilink")，則繼續在和平紀念碑舉行悼念儀式\[7\]。近年形式由風笛手奏樂帶領青少年制服團體進場，號角手在[香港會所大廈面向和平紀念碑的露台上](../Page/香港會所大廈.md "wikilink")，吹奏軍樂《[最後崗位](../Page/最後崗位.md "wikilink")》，全場默哀兩分鐘。其後，[政務司司長代表](../Page/政務司司長.md "wikilink")[香港特區政府致上花圈](../Page/香港特區政府.md "wikilink")，悼念為保衛香港犧牲的英魂。其後先後為[保安局局長](../Page/保安局.md "wikilink")、[立法會議員代表](../Page/香港立法會.md "wikilink")、退伍軍人代表、制服團體代表等，步往紀念碑前致送花圈。紀念碑台階四角由儀杖隊站崗，並低頭垂下步槍以示哀悼。出席的嘉賓都會佩戴紅色虞美人花，代表紀念陣亡將士。

## 升旗儀式

[HKClub_1928.jpeg](https://zh.wikipedia.org/wiki/File:HKClub_1928.jpeg "fig:HKClub_1928.jpeg")（攝於1928年）\]\]

在香港主權移交前，[香港軍事服務團每日早上](../Page/香港軍事服務團.md "wikilink")8時及下午6時在和平紀念碑進行升降旗儀式。1997年，升降旗任務改為英軍[黑衛士兵團](../Page/黑衛士兵團.md "wikilink")（Black
Watch）負責。

升旗儀式由三名旗手執行，旗手首先會在紀念碑向[立法局大樓一面由左至右升起](../Page/香港立法會大樓.md "wikilink")[英國皇家空軍軍旗](../Page/英國皇家空軍.md "wikilink")，[英國國旗及英國](../Page/英國國旗.md "wikilink")[紅船旗](../Page/紅船旗.md "wikilink")；然後走到紀念碑向[香港大會堂高座一面](../Page/香港大會堂.md "wikilink")，由左至右升起[英國皇家海軍軍旗](../Page/英國皇家海軍.md "wikilink")，英國國旗及英國[藍船旗](../Page/藍船旗.md "wikilink")，這樣的排列是與白廳的和平紀念碑一致。

1997年4月24日，一名黑衛士兵團士兵執行降旗任務時，大風掀起了其[蘇格蘭裙](../Page/蘇格蘭裙.md "wikilink")，被在場媒體拍攝發現他沒有穿著[內褲](../Page/內褲.md "wikilink")。事實上，英軍軍團並沒有穿著內褲的嚴格規定，而傳統上蘇格蘭裙一般是不穿內褲的\[8\]。陳果的[香港電影](../Page/香港電影.md "wikilink")《[去年煙花特別多](../Page/去年煙花特別多.md "wikilink")》更以此作為影片結幕。

香港主權移交後，官方每日的升降旗儀式改於[金紫荊廣場舉行](../Page/金紫荊廣場.md "wikilink")，由香港警察儀仗隊執行。

## 參見

  - [香港保衛戰](../Page/香港保衛戰.md "wikilink")
  - [香港日佔時期](../Page/香港日佔時期.md "wikilink")
  - [香港重光](../Page/香港重光.md "wikilink")
  - [和平紀念日 (香港)](../Page/和平紀念日_\(香港\).md "wikilink")

<!-- end list -->

  - 香港其他戰爭紀念設施：

<!-- end list -->

  - [忠靈塔](../Page/香港忠靈塔.md "wikilink")（ 已被炸毀 ）
  - [西灣國殤紀念墳場](../Page/西灣國殤紀念墳場.md "wikilink")
  - [赤柱軍人墳場](../Page/赤柱軍人墳場.md "wikilink")
  - [香港動植物公園紀念牌坊](../Page/香港動植物公園.md "wikilink")
  - [香港大會堂紀念花園](../Page/香港大會堂紀念花園.md "wikilink")
  - [香港海防博物館](../Page/香港海防博物館.md "wikilink")
  - [深水埗公園紀念碑及軍營界石](../Page/深水埗公園.md "wikilink")
  - [香港公園紀念銅像](../Page/香港公園.md "wikilink")
  - [烏蛟騰抗日英烈紀念碑](../Page/烏蛟騰抗日英烈紀念碑.md "wikilink")

## 注释

## 参考文献

## 外部連結

  - [主權移交前和平紀念碑升旗儀式的照片](https://www.webcitation.org/query?id=1256590903446662&url=www.geocities.com/jeffyam/c_m_turnover.htm)
  - [黑衛士兵團士兵執行降旗任務的「走光」照片](http://www.scottishwebcamslive.com/humour.htm)

[category:香港地標](../Page/category:香港地標.md "wikilink")

[Category:中環](../Category/中環.md "wikilink")
[Category:香港紀念碑](../Category/香港紀念碑.md "wikilink")
[Category:香港法定古蹟](../Category/香港法定古蹟.md "wikilink")

1.

2.  [香港中環皇后像廣場和平紀念碑文物價值評估報告](http://www.aab.gov.hk/form/160meeting/AAB%2046%202011-12%20\(Annex%20A\)chi.pdf)，古物諮詢委員會討論文件，2012年12月

3.
4.  [法定古蹟「新貴」：和平紀念碑
    伯大尼修院](http://orientaldaily.on.cc/cnt/news/20121218/00176_047.html)
    《東方日報》 2012年12月18日

5.
6.  [新法定古蹟](http://orientaldaily.on.cc/cnt/news/20131123/00176_024.html)
    《東方日報》 2013年11月23日

7.
8.  [Howcast: How to wear a
    Kilt](http://www.howcast.com/guides/1572-How-To-Wear-a-Kilt)
    第七項注明，嚴格傳統上蘇格蘭裙一般是不穿內褲的。