**芳香草科**（[学名](../Page/学名.md "wikilink")：）又名**聚星草科**，为[单子叶植物](../Page/单子叶植物.md "wikilink")[天门冬目的一科](../Page/天门冬目.md "wikilink")，共有4[属](../Page/属.md "wikilink")39[种](../Page/种.md "wikilink")，有[乔木](../Page/乔木.md "wikilink")、[灌木和](../Page/灌木.md "wikilink")[草本植物](../Page/草本.md "wikilink")，主要分布在南半球。

## 分类

在以前的分类法中，各属分别属于[百合科](../Page/百合科.md "wikilink")、[龙舌兰科等](../Page/龙舌兰科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将这些属单独分为一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，隶属于[天门冬目下面](../Page/天门冬目.md "wikilink")。

## 参考文献

## 外部链接

:\* 在[L. Watson和M.J. Dallwitz
(1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[芳香草科](http://delta-intkey.com/angio/www/asteliac.htm)

:\*
[NCBI分类中的芳香草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=51510&lvl=3&lin=f&keep=1&srchmode=1&unlock)

:\* [links at
CSDL分类中的芳香草科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Asteliaceae)

[Category:芳香草科](../Category/芳香草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")
[Category:天門冬目](../Category/天門冬目.md "wikilink")