**NGC
346**是一個周圍被星雲環繞的[疏散星團](../Page/疏散星團.md "wikilink")，位於[小麥哲倫星系內](../Page/小麥哲倫星系.md "wikilink")，天球上位於[杜鵑座](../Page/杜鵑座.md "wikilink")。該星團內的恆星[HD
5980是小麥哲倫星系內亮度最高的恆星](../Page/HD_5980.md "wikilink")。

該星團由[詹姆士·敦洛普發現於](../Page/詹姆士·敦洛普.md "wikilink")1826年。

## 圖擊

|  |                                                                                              |
|  | -------------------------------------------------------------------------------------------- |
|  | [ESO-NGC346.jpg](https://zh.wikipedia.org/wiki/File:ESO-NGC346.jpg "fig:ESO-NGC346.jpg")\]\] |

## 參考資料

  - [**ESA**](http://www.esa.int/esaSC/SEMLKL4N0MF_index_0.html)
  - [**Hubble Heritage site** Hubble picture and information on NGC
    346](http://heritage.stsci.edu/2005/35/fast_facts.html)
  - [**ESO** Beautiful Image of a Cosmic
    Sculpture](http://www.eso.org/public/news/eso1008/)

[Category:疏散星团](../Category/疏散星团.md "wikilink")
[0346](../Category/杜鵑座NGC天體.md "wikilink")
[Category:小麥哲倫星系](../Category/小麥哲倫星系.md "wikilink")
[Category:電離氫區](../Category/電離氫區.md "wikilink")