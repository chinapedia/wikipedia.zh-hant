**亚甲蓝**（Methylene blue），又称亚甲基蓝、次甲基蓝、次甲蓝、美蓝、品蓝、甲烯蓝、瑞士藍（Swiss
blue），[國際非專利藥品名稱](../Page/國際非專利藥品名稱.md "wikilink")（INN）為methylthioninium
chloride。是一种[芳香](../Page/芳香性.md "wikilink")[杂环化合物](../Page/杂环化合物.md "wikilink")。被用作化学指示剂、染料、生物染色剂和药物使用。亚甲蓝的水溶液在氧化性环境中蓝色，但遇锌、氨水等还原剂会被还原成无色形态。也可以說是一種酸鹼指示劑。
亞甲藍與弱[酸性](../Page/酸性.md "wikilink")[染料](../Page/染料.md "wikilink")[甲基藍](../Page/甲基藍.md "wikilink")（Methyl
blue）或[pH指示劑](../Page/pH指示劑.md "wikilink")[甲基紫](../Page/甲基紫.md "wikilink")（methyl
violet，又稱「龍膽紫」）不同，應注意勿混用。

## 用途

### 化学

[分析化学中用亚甲蓝作为氧化还原反应](../Page/分析化学.md "wikilink")[滴定的指示剂](../Page/滴定.md "wikilink")。[蓝瓶实验也是以亚甲蓝的变色为基础的](../Page/蓝瓶实验.md "wikilink")。可以用来检验水的溶氧量，反应是会令亚甲蓝液更蓝。

### 染料

亚甲蓝可以给[麻](../Page/麻.md "wikilink")、[丝绸](../Page/丝绸.md "wikilink")、[纸张](../Page/纸张.md "wikilink")[染色](../Page/染色.md "wikilink")。生物学上也被用做细菌或细胞[染色剂](../Page/染色剂.md "wikilink")。

### 医药

亚甲蓝因为有还原性，其注射液被用来治疗[正鐵血紅蛋白血症](../Page/正鐵血紅蛋白血症.md "wikilink")。也用于解救[硝基苯](../Page/硝基苯.md "wikilink")、[亚硝酸盐和](../Page/亚硝酸盐.md "wikilink")[氰化物中毒等](../Page/氰化物.md "wikilink")。

因为其杀菌消毒的作用，口服亚甲蓝或用其溶液冲洗可以治疗[膀胱炎和](../Page/膀胱炎.md "wikilink")[尿道炎](../Page/尿道炎.md "wikilink")。另外，亚甲蓝在进入人体30分钟（注射）至几小时（口服）内会从尿中排出，导致尿液暂时呈蓝色，因此也用来作肾功能测定。

[观赏鱼养殖中](../Page/观赏鱼.md "wikilink")，0.1-0.2[ppm的亚甲蓝溶液也被用作消毒](../Page/ppm.md "wikilink")，或治疗[白点病等疾病](../Page/白点病.md "wikilink")。

0.3ppm的亚甲蓝溶液可以防止水霉菌的发生。

1ppm的亚甲蓝溶液用於一般体外虫的防治。

2ppm的亚甲蓝溶液用於体外虫的治療。

### 疟疾

亚甲蓝的发明人[保罗·埃尔利希在](../Page/保罗·埃尔利希.md "wikilink")1891年就发现它会对[疟原虫染色](../Page/疟原虫.md "wikilink")，因此推断其有可能治疗[疟疾](../Page/疟疾.md "wikilink")。\[1\]
[太平洋战争中](../Page/太平洋战争.md "wikilink")，日军与美军都使用亚甲蓝作为抗疟药。但因為其暫時性副作用--服药者会出现尿液与[巩膜](../Page/巩膜.md "wikilink")（即眼白）变蓝色或绿色（低剂量时）--不受士兵們喜歡而在戰後淡出舞台。近来研究证实了亚甲蓝抗疟的确切性，特别是其廉价性。\[2\]
\[3\]

## 参考文献

## 外部連結

  - [NIH - Methylene blue
    test](http://www.nlm.nih.gov/medlineplus/ency/article/003412.htm)
  - [Methylene blue](http://stainsfile.info/StainsFile/dyes/52015.htm)
    at stainsfile

{{-}}

[Category:解毒剂](../Category/解毒剂.md "wikilink")
[Category:組織學](../Category/組織學.md "wikilink")
[Category:氧化还原指示剂](../Category/氧化还原指示剂.md "wikilink")
[Category:噻嗪染料](../Category/噻嗪染料.md "wikilink")
[Category:活体染料](../Category/活体染料.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")
[Category:单胺氧化酶抑制剂](../Category/单胺氧化酶抑制剂.md "wikilink")
[Category:酚噻嗪](../Category/酚噻嗪.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")

1.  Guttmann, P. and Ehrlich. P. (1891) ["Über die Wirkung des
    Methylenblau bei
    Malaria"](https://books.google.com/books?id=8EYcAQAAMAAJ&pg=PA953#v=onepage&q&f=false)
    (On the effect of methylene blue on malaria), *Berliner Klinische
    Wochenschrift*, **28** : 953-956.
2.
3.