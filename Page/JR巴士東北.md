[JRBusTohoku_647-8972.jpg](https://zh.wikipedia.org/wiki/File:JRBusTohoku_647-8972.jpg "fig:JRBusTohoku_647-8972.jpg")
**JR巴士東北**（）是在日本[東北地方](../Page/東北地方.md "wikilink")（不含[福島縣白河](../Page/福島縣.md "wikilink")、棚倉地區）經營的[巴士業者](../Page/巴士.md "wikilink")，屬於[JR東日本集團旗下獨資](../Page/JR東日本.md "wikilink")[子公司](../Page/子公司.md "wikilink")。公司成立于1988年（昭和63年）3月5日。

## 支店・營業所

  - [青森支店](../Page/JR巴士東北青森支店.md "wikilink")
  - [大湊營業所](../Page/下北線.md "wikilink")
  - [盛岡支店](../Page/JR巴士東北盛岡支店.md "wikilink")
      - [大釜車庫](../Page/JR巴士東北盛岡支店.md "wikilink")
  - [久慈營業所](../Page/JR巴士東北久慈營業所.md "wikilink")
      - [葛卷車庫](../Page/JR巴士東北葛卷車庫.md "wikilink")
  - [二戶營業所](../Page/JR巴士東北二戶營業所.md "wikilink")
  - [秋田支店](../Page/JR巴士東北秋田支店.md "wikilink")
  - [仙台支店](../Page/JR巴士東北仙台支店.md "wikilink")
      - [白澤事業所](../Page/白澤事業所.md "wikilink")
  - [古川營業所](../Page/JR巴士東北古川營業所.md "wikilink")
  - [福島支店](../Page/JR巴士東北福島支店.md "wikilink")
      - [川俣車庫](../Page/JR巴士東北川俣車庫.md "wikilink")

## 外部連結

  - [JR巴士東北](http://www.jrbustohoku.co.jp/)

[Category:日本巴士公司](../Category/日本巴士公司.md "wikilink")
[Category:JR東日本集團](../Category/JR東日本集團.md "wikilink")