**Google有限公司**（；中文：**谷-{}-歌**\[1\]）是源自[美国的](../Page/美国.md "wikilink")[跨國](../Page/跨國公司.md "wikilink")[科技公司](../Page/科技公司.md "wikilink")，為[Alphabet
Inc.的子公司](../Page/Alphabet_Inc..md "wikilink")，业务范围涵盖[互联网广告](../Page/網際網路.md "wikilink")、[互联网搜索](../Page/搜索引擎.md "wikilink")、[云计算等领域](../Page/云计算.md "wikilink")，开发并提供大量基于互联网的产品与服务\[2\]，其主要利润来自于等广告服务\[3\]\[4\]。Google由在[斯坦福大学攻读](../Page/斯坦福大学.md "wikilink")[理工](../Page/理工.md "wikilink")[博士的](../Page/博士.md "wikilink")[拉里·佩奇和](../Page/拉里·佩奇.md "wikilink")[谢尔盖·布林共同创建](../Page/谢尔盖·布林.md "wikilink")，因此两人也被称为“”\[5\]\[6\]\[7\]。

1998年9月4日，Google以私营公司的形式创立，目的是设计并管理互联网搜索引擎“[Google搜索](../Page/Google搜索.md "wikilink")”。2004年8月19日，Google公司在[纳斯达克](../Page/纳斯达克.md "wikilink")[上市](../Page/上市公司.md "wikilink")，后来被称为“三驾马车”的公司两位共同创始人与出任[首席执行官的](../Page/首席执行官.md "wikilink")[埃里克·施密特在此时承诺](../Page/埃里克·施密特.md "wikilink")：共同在Google工作至少二十年，即至2024年止\[8\]。Google的宗旨是“-{zh-cn:整合全球信息，供大众使用，使人人受益;zh-tw:匯整全球資訊，供大眾使用，使人人受惠}-”（**）\[9\]；而非正式的口号则为“[不作恶](../Page/不作恶.md "wikilink")”（**），由工程师阿米特·帕特尔（）所创\[10\]，并得到了[保罗·布赫海特的支持](../Page/保罗·布赫海特.md "wikilink")\[11\]\[12\]。Google公司的总部称为“”，位于美国[加州](../Page/加州.md "wikilink")[圣克拉拉县的](../Page/圣克拉拉县.md "wikilink")[山景城](../Page/山景城_\(加利福尼亚州\).md "wikilink")。2011年4月，佩奇接替施密特擔任首席执行官\[13\]。在2015年8月，Google宣布進行资产重组。重组後，Google划归新成立的Alphabet底下。同时，此舉把Google旗下的核心搜索和廣告業務與[Google無人車等新兴业务分離開來](../Page/Google_Driverless_Car.md "wikilink")\[14\]。

据估计，Google在全世界的数据中心内运营着上百万台的[服务器](../Page/服务器.md "wikilink")，\[15\]每天处理数以亿计的搜索请求\[16\]和约二十四[PB用户生成的数据](../Page/拍字节.md "wikilink")。\[17\]\[18\]\[19\]\[20\]
Google自创立起开始的快速成长同时也带动了一系列的产品研发、并购事项与合作关系，而不仅仅是公司核心的网络搜索业务。Google公司提供丰富的线上软件服务，如[雲端硬碟](../Page/Google_Drive.md "wikilink")、[电子邮件](../Page/电子邮件.md "wikilink")，包括、以及在内的[社交网络服务](../Page/社交网络服务.md "wikilink")。Google的产品同时也以[应用软件的形式进入用户桌面](../Page/应用软件.md "wikilink")，例如[网页浏览器](../Page/网页浏览器.md "wikilink")、图片整理与编辑软件、[即时通讯工具等](../Page/即时通讯工具.md "wikilink")。另外，Google还进行了移动设备的[操作系统以及](../Page/操作系统.md "wikilink")操作系统的开发。\[21\]

\-{zh-cn:信息; zh-tw:資訊;
zh-hk:資訊;}-分析网站[Alexa数据显示](../Page/Alexa_Internet.md "wikilink")，Google的主域名google.com是全世界访问量最高的站点，[Google搜索在其他国家或地区域名下的多个站点](../Page/Google搜索.md "wikilink")（google.co.in、google.de、google.com.hk等等），及旗下的、、等的访问量都在前一百名之内。\[22\]其中，[社交网络服务](../Page/社交网络.md "wikilink")[Orkut于](../Page/Orkut.md "wikilink")2014年9月关闭。\[23\]

## 历史

### 建立

[Google1998.png](https://zh.wikipedia.org/wiki/File:Google1998.png "fig:Google1998.png")方面并不是很有经验\[24\]。点击[這裡](https://web.archive.org/web/19990125084553/http://alpha.google.com/)查看Google
1999年时的外觀\]\]
1996年1月，加州斯坦福大学理学博士生的[拉里·佩奇和](../Page/拉里·佩奇.md "wikilink")[谢尔盖·布林在学校开始研究一项关于搜索的研究项目](../Page/谢尔盖·布林.md "wikilink")。\[25\]区别于传统搜索根据关键字在页面中出现次数来进行结果排序的方法，两人开发了一个对网站之间的关系做精确分析的搜寻引擎。\[26\]这个名为[PageRank的引擎通过检查网页中的](../Page/PageRank.md "wikilink")[反向链接以评估站点的重要性](../Page/反向链接.md "wikilink")，此引擎的精确度胜于当时的基本搜索技术。\[27\]\[28\]最初，佩奇和布林将这个搜索引擎命名为「BackRub」，直到后来改为「Google」。\[29\]\[30\]\[31\]这个新名字来源于一个数学大数[googol](../Page/googol.md "wikilink")（数字1後有100个0，即[自然数](../Page/自然数.md "wikilink")10<sup>100</sup>）单字错误的拼写方式，\[32\]\[33\]\[34\]象征着为人们提供搜索海量优质信息的决心。\[35\]
Google搜索引擎在斯坦福大学的网站上启用，域名为google.stanford.edu。\[36\]

1997年9月15日，两人注册了Google[域名](../Page/域名.md "wikilink")。\[37\]1998年9月4日，佩奇和布林在加州[门洛帕克一位朋友家的车库内建立了Google公司](../Page/门洛帕克_\(加利福尼亚州\).md "wikilink")，克雷格·西尔弗斯坦（Craig
Silverstein）——同为斯坦福大学的博士生——是公司的首位雇员。\[38\]\[39\]\[40\]

### 上市

[Google’s_First_Production_Server.jpg](https://zh.wikipedia.org/wiki/File:Google’s_First_Production_Server.jpg "fig:Google’s_First_Production_Server.jpg")
佩奇和布林在Google项目上最早获得投资是在1998年8月，[Sun
Microsystems的联合创始人](../Page/Sun_Microsystems.md "wikilink")[安迪·贝托尔斯海姆给了两人一张十万](../Page/安迪·贝托尔斯海姆.md "wikilink")[美元的支票来用于搜索引擎的开发和运营](../Page/美元.md "wikilink")，当时Google公司还尚未成立。\[41\]到1999年，由于搜索引擎的开发花费了太多的学习时间，佩奇和布林甚至考虑将其出售。两人找到公司CEO乔治·贝尔（George
Bell）提出一百万美元的收购价。尽管Excite的风险投资人[维诺德·科斯拉在与Google的两位创始人谈判后将价格降低到](../Page/维诺德·科斯拉.md "wikilink")75万美元，但仍被贝尔拒绝了。1999年6月7日，包括[Kleiner
Perkins公司和](../Page/Kleiner_Perkins公司.md "wikilink")[红杉资本在内的投资者为Google注资两千五百万美元](../Page/红杉资本.md "wikilink")。\[42\]\[43\]

Google在2004年8月19日进行了[首次公开募股](../Page/首次公开募股.md "wikilink")，公司发行了19,605,052份每股价值85美元的[股票](../Page/股票.md "wikilink")。\[44\]\[45\]股票由[摩根士丹利和](../Page/摩根士丹利.md "wikilink")[瑞士信贷集团承销](../Page/瑞士信贷集团.md "wikilink")，以网上拍卖的形式发售。\[46\]\[47\]\[48\]
IPO后，Google公司的[市值迅速上涨到超过](../Page/市值.md "wikilink")230亿美元，而Google仍然控制着27182万（2.718281828和数学常数e有关）股票中的大多数股份，许多Google公司的雇员也在瞬间变成[百万富翁](../Page/富豪.md "wikilink")。而作为竞争对手的[雅虎](../Page/雅虎.md "wikilink")，也因为在Google上市前持有840万股票而受益。\[49\]

有人怀疑Google公司的企业文化在上市后，会由于董事会施压或高管们的暴富而不可避免地被改变。\[50\]两位创始人拉里·佩奇和谢尔盖·布林则在一份报告中承诺，上市不会影响Google的公司文化，以作为对潜在投资者们的怀疑所做的回应。\[51\]2005年，[纽约时报等媒体撰文指出Google已经丧失了原来不作恶的哲学](../Page/纽约时报.md "wikilink")。\[52\]\[53\]\[54\]在此之下，为了保持与众不同的企业文化，Google专门指派了一位首席文化官，来为Google构建和维护企业内部广泛协助的扁平化组织，及其所产生的核心价值。\[55\]
Google也面临前员工[性别歧视和](../Page/性别歧视.md "wikilink")[年龄歧视的指控](../Page/年龄歧视.md "wikilink")。\[56\]\[57\]

首次公开募股之后，Google的股票形势良好，2007年10月31日，受益于在网络广告市场的强势盈利，\[58\]股价首次超过700美元。\[59\]相比较大型机构投资和[共同基金](../Page/共同基金.md "wikilink")，Google股票的股价的变动更多地是由个人投资者们所影响。\[60\]在2015年第四季起，Google的上市地位由母公司[Alphabet
Inc.取代](../Page/Alphabet_Inc..md "wikilink")（[股票代号分别为](../Page/股票代号.md "wikilink")、）。

### 成长

1999年3月，Google公司将办公场所搬至加州的[帕罗奥多](../Page/帕罗奥图_\(加利福尼亚州\).md "wikilink")，这里是众多知名的[硅谷初创公司所在的地方](../Page/硅谷.md "wikilink")。\[61\]翌年，Google开始以出售搜索关键词的广告，\[62\]但这一做法与佩奇和布林以广告赞助搜索的意愿相违背。\[63\]为了保持页面简洁的设计，提高搜索速度，广告只会以基于文本的形式出现。关键词的出售结合点击次数和价格的竞标，竞标起价为每次点击5[美分](../Page/美分.md "wikilink")。\[64\]这种出售广告关键词的模式最早来源于Goto.com——一个由[比尔·葛罗斯的](../Page/比尔·葛罗斯.md "wikilink")企业孵化器派生的网站。\[65\]\[66\]之后Google受到了这家改名为Overture
Services的公司对于次广告出售专利技术侵权的指控。2003年，Overture
Services被雅虎收购，并被改名为[雅虎搜索营销](../Page/雅虎搜索营销.md "wikilink")。最终，雅虎与Google在庭外达成和解：Google用[普通股股份换取此专利的永久授权](../Page/普通股.md "wikilink")。\[67\]

2001年，Google获得了[PageRank的专利权](../Page/PageRank.md "wikilink")，\[68\]这项专利被正式颁与斯坦福大学，劳伦斯·佩奇（即[拉里·佩奇](../Page/拉里·佩奇.md "wikilink")）为专利发明人。2003年，在发展了两处办公地点以后，公司又向[硅谷图形公司租赁了位于](../Page/硅谷图形公司.md "wikilink")[山景城目前所在的综合办公楼](../Page/山景城.md "wikilink")。\[69\]这处办公地点被戏称为「-{[Googleplex](../Page/Googleplex.md "wikilink")}-」，数学大数[古戈尔普勒克斯](../Page/古戈尔普勒克斯.md "wikilink")（googolplex）单词的变体。三年后，Google以三亿一千九百万的价额向硅谷图形公司买下了这里的产权。\[70\]在这段时间内，google这个单词逐渐进入各类语言当中，也使得「google」作为动词被收入至[梅里亚姆-韦伯斯特词典和](../Page/梅里亚姆-韦伯斯特.md "wikilink")[牛津英语词典内](../Page/牛津英语词典.md "wikilink")，释义为“使用Google搜索引擎在互联网上获取信息”（*to
use the Google search engine to obtain information on the
Internet*）。\[71\]\[72\]

2011年5月，Google的月独立访客数量首次超过十亿，与一年前同期的9亿3100万相比增长8.4%。\[73\]
Google也是首个取得该数据里程碑的网站。\[74\]

### 并购与合作

[Schmidt-Brin-Page-20080520.jpg](https://zh.wikipedia.org/wiki/File:Schmidt-Brin-Page-20080520.jpg "fig:Schmidt-Brin-Page-20080520.jpg")、[拉里·佩奇和](../Page/拉里·佩奇.md "wikilink")[谢尔盖·布林](../Page/谢尔盖·布林.md "wikilink")\]\]
自2001年始，Google已收购了许多企业，其中尤以小型风投公司为主。2004年7月13日，Google收购照片整理与编辑软件[Picasa](../Page/Picasa.md "wikilink")，\[75\]同年10月又吞并了[Keyhole公司](../Page/Google地球.md "wikilink")。\[76\]这家初创公司开发出一个名为Earth
Viewer的产品，供使用者以[3D的视角观察地球](../Page/3D.md "wikilink")。一年后，Google将此服务改名为[Google地球](../Page/Google地球.md "wikilink")。2005年，成立仅22个月的高科技企业[Android被Google相中](../Page/Android.md "wikilink")，被收购并成为Google麾下的移动设备操作系统。2006年10月，Google宣布以16.5亿美元的股票收购在线视频分享网站[YouTube](../Page/YouTube.md "wikilink")，并于11月敲定。\[77\]2007年4月13日，Google与[DoubleClick达成协议](../Page/DoubleClick.md "wikilink")，DoubleClick以31亿美元的价格被Google收购，并成为后者旗下的一家网络出版与广告商。\[78\]同年，Google以5千万美元并购了GrandCentral，后来将其改造为[Google
Voice](../Page/Google_Voice.md "wikilink")。\[79\]2009年8月5日，Google以1亿650万美元买断视频软件制造公司[On2
Technologies](../Page/On2_Technologies.md "wikilink")。\[80\]2010年，Google将纳入旗下，出价为5千万美元，并在内部博客中写道：“我们很期待将其整合进我们的产品中”。\[81\]同年，Google又大举并购了、、[Like.com等](../Page/Like.com.md "wikilink")。\[82\]\[83\]\[84\]\[85\]2011年，Google继续大笔的收购之路，业已将\[86\]等十余家美国和[欧洲的企业或团队招致麾下](../Page/欧洲.md "wikilink")。2011年8月15日，Google官方宣布將以每股40美元現金，總額約125億美元收購[摩托羅拉移動](../Page/摩托羅拉移動.md "wikilink")。\[87\]

除了收购的方式以外，Google也积极地与其他公司或组织进行合作，从科研到商业广告，都是开展合作所涉及的领域。2005年，Google与[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")合作建设了用于囊括了大规模数据管理、纳米技术、分布式计算、航空航天产业等科研项目的场所，占地约93000平方米。\[88\]
2005年10月，Google与[Sun
Microsystems交换并分享了各自的技术](../Page/Sun_Microsystems.md "wikilink")。\[89\]
Google也和[AOL及](../Page/AOL.md "wikilink")[时代华纳达成商业伙伴关系](../Page/时代华纳.md "wikilink")，以增强各自视频搜索服务的质量。\[90\]
2005年，Google与[微软](../Page/微软.md "wikilink")、[诺基亚](../Page/诺基亚.md "wikilink")、[爱立信联手](../Page/爱立信.md "wikilink")，为[顶级域名](../Page/顶级域名.md "wikilink")[.mobi融资](../Page/.mobi.md "wikilink")。\[91\]而后Google启动了Adsense
for
Mobile，开拓移动广告市场。2006年，[新闻集团旗下的](../Page/新闻集团.md "wikilink")[福克斯互动媒体同Google达成一份](../Page/福克斯互动媒体.md "wikilink")9亿美元的协议，Google为[社交网站](../Page/社交网络服务.md "wikilink")[MySpace提供搜索与广告业务](../Page/MySpace.md "wikilink")。\[92\]
2007年，Google与34家手机制造商、芯片制造商、软件开发商和电信运营商一同创建了[开放手机联盟](../Page/开放手机联盟.md "wikilink")，开发[Android系统与](../Page/Android.md "wikilink")[苹果](../Page/苹果.md "wikilink")[iOS](../Page/iOS.md "wikilink")、[微软](../Page/微软.md "wikilink")[Windows
Phone等移动平台竞争](../Page/Windows_Phone.md "wikilink")。\[93\]

2013年9月11日Google將夥拍哈佛大學和麻省理工（MIT）組成的網上教育平台edX，發展網上教學課程，任何人都可將課程放入網路中。

### Alphabet

在2015年8月10日，Google宣佈要組織重整為[控股公司Alphabet](../Page/控股公司.md "wikilink")，Google則會是Alphabet公司旗下最大的子公司。重整完成以後，[桑德爾·皮蔡會接任成為Google的執行長](../Page/桑德爾·皮蔡.md "wikilink")。

## 产品与服务

### 广告

[Google_add.JPG](https://zh.wikipedia.org/wiki/File:Google_add.JPG "fig:Google_add.JPG")广告\]\]
Google九成以上的营收来自其[广告系统](../Page/广告.md "wikilink")。\[94\]如2006年财政年度，公司财报显示104.92亿美元的收入中只有1.12亿来自于非广告收入。\[95\]为了保持在网络广告代理市场中执牛耳的地位，Google不断实施各种新的手段。例如Google在收购[DoubleClick后获取技术](../Page/DoubleClick.md "wikilink")，得以获取用户兴趣和确定广告目标。\[96\]\[97\][Google
Analytics可以让网站站长们查看并追踪访问者在何时](../Page/Google_Analytics.md "wikilink")，以何种方式访问自己的网站，如检查某一页面上所有[链接的点击等](../Page/超連結.md "wikilink")。\[98\]
Google的广告系统由两个部分组成，通过第三方网站来放置。[AdWords为广告客户提供在网络中展示广告的服务](../Page/AdWords.md "wikilink")，按点击或显示收费；AdWords的姊妹项目[AdSense](../Page/AdSense.md "wikilink")，允许网站站长在自己的网页中提供广告展示的位置，并参与分成。\[99\]

但Google也由于在网络广告系统中反管理的欠缺而受到诟病——当一个并不是出于对广告中产品感兴趣的访客或一个自动脚本故意点击该则广告后，会引起广告客户过多支付广告费用。2006年的行业报告指出，约有14%至20%的点击属于欺诈点击或无效点击。\[100\]另一个对Google广告业务的批评来自于对广告客户的审查，有些被怀疑违反[数字千年版权法](../Page/数字千年版权法.md "wikilink")。2003年2月，Google停止了对一个名为的非营利组织的（抗议大型油轮不合理的污水处理）广告展示。Google引用其编辑政策解释：“Google不接受广告或网站内容为宣扬反对其他个人、团体或组织的广告。”\[101\]该政策却在随后即被更改。\[102\]
2008年6月，在[美国司法部鉴于](../Page/美国司法部.md "wikilink")[反托拉斯的促使之下](../Page/反托拉斯法.md "wikilink")，Google与[雅虎两大巨头达成广告协议](../Page/雅虎.md "wikilink")，雅虎在自己的网站页面上显示Google广告。但最终Google又在2008年11月撤出该协议。\[103\]\[104\]

为了推销自己的产品，Google启动了一个名为[Demo
Slam](https://web.archive.org/web/20101116175709/http://demoslam.com/)的网站以展示自有产品的。\[105\]每一周都会有两个团队竞相把Google的新产品公布到网站上。《搜索引擎杂志》撰文写道：Demo
Slam是“创作者和技术娴熟的人们制作视频来使世界上其他人类了解最新最伟大技术的地方”。\[106\]

### 搜索引擎

[Google_web_search.png](https://zh.wikipedia.org/wiki/File:Google_web_search.png "fig:Google_web_search.png")
Google搜索是Google公司重要也是最普及的一项功能，是多个国家内使用率最高的互联网搜索引擎。根据2009年11月公布的市场统计，Google在美国搜索引擎市场上占有率为65.6%。\[107\]
Google抓取数十亿的[互联网网页](../Page/互联网.md "wikilink")，因此用户能通过搜索关键词等操作较为轻松地获取想要搜寻的信息。\[108\]除了最基本的文字搜索功能之外，Google搜索还提供至少22种特殊功能，\[109\]\[110\]如[同义词](../Page/同义词.md "wikilink")、[天气预报](../Page/天气预报.md "wikilink")、[时区](../Page/时区.md "wikilink")、[股价](../Page/股价.md "wikilink")、[地图](../Page/地图.md "wikilink")、[地震数据](../Page/地震.md "wikilink")、电影放映时间、[机场](../Page/机场.md "wikilink")、体育赛事比分等。Google搜索在搜索与数字相关的信息时又会有另一些特殊功能：如单位换算、货币换算、数字运算、包裹追踪、地区代码。\[111\]同时，Google也为搜索页面提供语言[翻译功能](../Page/翻译.md "wikilink")。2011年，Google先后推出语音搜索和图片搜索。\[112\]

尽管Google搜索很受大众喜爱，但也有组织对它发出批评的声音。2003年，[纽约时报声称Google对其网站抓取与形成的](../Page/纽约时报.md "wikilink")[缓存侵害了网站内容的版权](../Page/缓存.md "wikilink")。\[113\]在这个问题上，[内华达](../Page/内华达.md "wikilink")[地方法院审理了相关的](../Page/地方法院.md "wikilink")和Parker
v.
Google两个案件，Google成功地为自己作了[辩护](../Page/辩护.md "wikilink")，推翻了指控。\[114\]\[115\]

### 工作工具

除了强大的搜索引擎以外，Google也研发了许多在线的工作辅助工具。[Gmail是Google开发的一个免费](../Page/Gmail.md "wikilink")[电子邮箱服务](../Page/电子邮箱.md "wikilink")，于2004年4月1日进入[Beta阶段](../Page/软件版本周期.md "wikilink")，\[116\]
2007年2月7日正式面向大众开放。\[117\]直到2009年7月7日，Gmail才结束其漫长的Beta阶段，\[118\]当时已有1.64亿用户使用。\[119\]
Gmail是首个拥有1[GB存储空间的网络电子邮箱](../Page/吉字节.md "wikilink")，也是第一个以单线程会话保存邮件的的电邮，这与网络论坛相似。\[120\]目前，Gmail提供给每个免费用户15GB的邮箱容量，付费用户可缴纳每年每GB
0.25美元的费用来将容量提升至20GB至16[TB](../Page/太字节.md "wikilink")。\[121\]
Gmail也是[AJAX技术开拓的先行者](../Page/AJAX.md "wikilink")，该技术使人们不用手动刷新浏览器显示就能看到新的网页信息。\[122\]

Google工作套件的另一部分——[Google文件](../Page/Google文件.md "wikilink")，与[微软的](../Page/微软.md "wikilink")[Microsoft
Word不同](../Page/Microsoft_Word.md "wikilink")，它允许用户在在线环境下新建、编辑或协同编辑文件。这项服务原名为Writely，但在2006年3月9日被Google收购。\[123\]6月6日，Google创建了一个实验性的电子表格编辑项目，\[124\]在随后的10月10日被整合进Google文件中。\[125\]
2007年9月17日，编辑演示文稿的功能也被加入Google文件。\[126\]
2009年7月7日，包括三项功能在内的Google文件与Gmail、[Google日历一起组成](../Page/Google日历.md "wikilink")[Google
Apps工作套件](../Page/Google_Apps.md "wikilink")。\[127\]

### 企业产品

[Google_Appliance.jpg](https://zh.wikipedia.org/wiki/File:Google_Appliance.jpg "fig:Google_Appliance.jpg")

2002年2月，Google向企业市场推出一款名为[Google Search
Appliance的设备](../Page/Google_Search_Appliance.md "wikilink")，针对大型组织机构提供搜索技术。\[128\]三年后Google又推出Google
Mini以适应中小型企业。2006年，又推出子自定义搜索商务版，2008年改名为[Google协作平台](../Page/Google协作平台.md "wikilink")。\[129\]

另一个Google的企业产品是[Google
Apps专业版](../Page/Google_Apps.md "wikilink")。Google
Apps专业版和标准版及教育版同为Google
Apps的不同版本，为企业、学校和其他组织提供自身域名下的Gmail、Google文件等在线应用服务。与标准版相比，专业版有着更多的功能，例如更多的磁盘空间、[API准入和高级支持](../Page/API.md "wikilink")，每个账户收费为每年50美元。Google较早在[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")[桑德贝的](../Page/桑德贝.md "wikilink")[湖首大学推广Google](../Page/湖首大学.md "wikilink")
Apps业务，有超过3万8千名用户。在推出Google
Apps的同年，Google收购了[Postini公司](../Page/Postini.md "wikilink")，\[130\]得以将其原有的安全技术以Google
Postini Services之名整合进Google Apps中。\[131\]\[132\]

### 社交网络服务

Google曾多次试图进入[SNS领域但多次碰壁](../Page/SNS.md "wikilink")，失败的例子不乏[Google
Wave](../Page/Google_Wave.md "wikilink")、[Google
Buzz](../Page/Google_Buzz.md "wikilink")，\[133\]而[Orkut只在](../Page/Orkut.md "wikilink")[印度和](../Page/印度.md "wikilink")[巴西获得了较大市场](../Page/巴西.md "wikilink")。\[134\]在经历多次挫折之后，2011年，Google发布新的社交网站[Google+](../Page/Google+.md "wikilink")。\[135\]这个新型的社交网站有着社交“圈子”、多人视频聊天、“灵感话题”等多种新型的特色，\[136\]在测试阶段用户数量便突飞猛进，仅2个星期就超越了1000万。\[137\]\[138\]相比较之下，[Twitter和](../Page/Twitter.md "wikilink")[Facebook过去分别用了](../Page/Facebook.md "wikilink")780天和852天才达到这个数字。\[139\]推出以来的第三周，Google+的用户数量旋即又翻至2000万。\[140\]但即便如此，Google仍宣布2019年4月2日他們將會關閉消費者（個人用戶）版本的Google+，代表Google再次於進入SNS領域中嚐到挫敗。\[141\]

2003年，Google收购了创建的[博客服务网站](../Page/博客.md "wikilink")[Blogger](../Page/Blogger.md "wikilink")。收购完成后，Google依旧保留了Blogger这一独立的品牌，并在blogspot.com主机下为用户提供免费博客发布与存放服务。\[142\]
2007年5月，Blogger的数据全部搬至Google的服务器中，并在当年的独立访客流量50强域名中排在第16位。\[143\]在Google较为成功的运营下，Blogger一直保持着全球最大的博客网站的头衔。\[144\]
2011年7月，有传闻透露，作为重命名或取消非Google品牌计划的一部分，Google公司有意将「Blogger」名称改成「Google
Blogger」。\[145\]

2005年2月，三位前[PayPal员工创建了](../Page/PayPal.md "wikilink")[视频分享网站](../Page/视频分享网站.md "wikilink")[YouTube](../Page/YouTube.md "wikilink")。\[146\]在显露出自身价值后，YouTube被Google以16.5亿美元收购，成为后者旗下的子公司。YouTube公司地址位于加州的[圣布鲁](../Page/圣布鲁.md "wikilink")，以[Flash
Video和](../Page/Flash_Video.md "wikilink")[HTML5技术记录播放用户上传的电影](../Page/HTML5.md "wikilink")、电视剧剪辑和音乐视频，以及用户原创制作的业余视频作品。大部分发布在YouTube网站上的视频都是由个人所上传的，但也有[哥伦比亚广播公司](../Page/哥伦比亚广播公司.md "wikilink")、[英国广播公司](../Page/英国广播公司.md "wikilink")、[VEVO](../Page/VEVO.md "wikilink")、[Hulu等作为合作伙伴的媒体企业在YouTube网站上发布视频](../Page/Hulu.md "wikilink")。\[147\]
YouTube的模式取得巨大的成功，目前仍是世界上最大和最热门的视频分享网站。\[148\]

### 浏览器

[Google
Chrome是Google以](../Page/Google_Chrome.md "wikilink")[WebKit](../Page/WebKit.md "wikilink")[排版引擎开发的一款开源的网页浏览器](../Page/排版引擎.md "wikilink")，从Chrome
28开始使用[Blink引擎](../Page/Blink.md "wikilink")。2008年，Google公司发布了该浏览器大部分的[源代码](../Page/源代码.md "wikilink")，包括[V8
JavaScript引擎的开源项目](../Page/V8_\(JavaScript引擎\).md "wikilink")[Chromium](../Page/Chromium_\(浏览器\).md "wikilink")。\[149\]\[150\]这一举动使得第三方的开发者们能够获取底层源代码，促使浏览器与[Mac
OS
X或](../Page/Mac_OS_X.md "wikilink")[Linux操作系统的端口对接](../Page/Linux.md "wikilink")。Google官方同时也表达了希望其他浏览器能够采用V8引擎以提升web应用程序的性能。\[151\]
Google在Chromium的创作上按照[BSD许可证条款](../Page/BSD许可证.md "wikilink")，\[152\]使得这部分内容几乎属于公有领域，有着很高的相容性。\[153\]而其他部分的内容则遵循一系列的开源许可证。\[154\]
Chromium的特性与Chrome基本相似，但没有内置的自动更新功能。另外，Chromium也没有Google商标的印记，与Chrome四色的Logo最明显的区别是其蓝色的标识。\[155\]\[156\]

2008年9月2日，Google推出在[Windows上Beta版的](../Page/Microsoft_Windows.md "wikilink")[Google
Chrome](../Page/Google_Chrome.md "wikilink")，稳定版本则在12月11日推出。\[157\]
2011年，根据[StatCounter的统计](../Page/StatCounter.md "wikilink")，Chrome以20.65%的[浏览器市场份额排在](../Page/网页浏览器的使用分布.md "wikilink")[IE和](../Page/Internet_Explorer.md "wikilink")[火狐之后跃至第三位](../Page/Mozilla_Firefox.md "wikilink")。\[158\]\[159\]2011年7月11日，Chrome在英国的市场份额达到22.12%，而[Firefox仅占](../Page/Firefox.md "wikilink")21.65%，这是Chrome首次在地区的统计中超越[Firefox](../Page/Firefox.md "wikilink")。\[160\]2012年5月21日，Chrome在全球范围内占有率达到33%，超越IE位居首位，\[161\]直到2018年7月，Chrome在全球範圍内占有率已達到66%。

### 操作系统

[Android_robot.svg](https://zh.wikipedia.org/wiki/File:Android_robot.svg "fig:Android_robot.svg")的[商标为一个绿色的机器小人的造型](../Page/商标.md "wikilink")\]\]
2007年，Google正计划开发自己的移动设备与[苹果的](../Page/苹果公司.md "wikilink")[iPhone竞争的报道逐渐浮出水面](../Page/iPhone.md "wikilink")。\[162\]\[163\]\[164\]后来证实该项目名为[Android](../Page/Android.md "wikilink")，并非手机的名称，而是一款移动设备的[操作系统](../Page/操作系统.md "wikilink")。Android于2005年被Google收购，并发展为[Apache许可证下的](../Page/Apache许可证.md "wikilink")[开源项目](../Page/开源.md "wikilink")，\[165\]同时Google为开发者们提供了[软件开发工具包以便开发出在Android手机上运行的应用](../Page/软件开发工具包.md "wikilink")，并拉拢了世界上多家手机制造商、电信运营商、软件开发商等，组建了[开放手机联盟以共同开发Android系统](../Page/开放手机联盟.md "wikilink")。\[166\]
2008年9月，[T-Mobile发布第一款运行Android系统的手机](../Page/T-Mobile.md "wikilink")[G1](../Page/T-Mobile_G1.md "wikilink")（HTC
dream的T-Mobile定制版）。\[167\] 2010年1月5日，Google公司发布自有的一款手机，名为[Nexus
One](../Page/Nexus_One.md "wikilink")。\[168\]在2010年第四季度的分析报告中，Android被列为全世界最畅销的[智能手机操作系统](../Page/智能手机.md "wikilink")。\[169\]\[170\]

[Google_Android.jpg](https://zh.wikipedia.org/wiki/File:Google_Android.jpg "fig:Google_Android.jpg")是市占率最高的[移动操作系统](../Page/移动操作系统.md "wikilink")\]\]
Android有着数目庞大的[应用软件](../Page/应用软件.md "wikilink")，得以大大扩充其系统功能。目前，能在Android上运行的应用软件超过了75万种，\[171\]</ref>\[172\]并仍处于高速增加的过程中。[Google
Play是Google运营的电子应用软件商店](../Page/Google_Play.md "wikilink")，但用户也可以从第三方下载与安装软件。开发者们主要用[Java语言编写软件](../Page/Java.md "wikilink")，并通过Google开发的Java库来使应用软件适配系统。\[173\]

2009年7月7日，Google宣布正在开发基于[Linux的操作系统](../Page/Linux.md "wikilink")[-{Google
Chrome
OS}-](../Page/Google_Chrome_OS.md "wikilink")，\[174\]\[175\]该操作系统运行于只含有网页浏览器的[上网本上](../Page/上网本.md "wikilink")。\[176\]\[177\]11月19日，Google公司开始发布-{Google}-
Chrome操作系统的开发版本，名为[Chromium
OS](../Page/Chromium_OS.md "wikilink")。Chromium
OS与Chromium浏览器类似，遵照[BSD许可证的条款](../Page/BSD许可证.md "wikilink")。\[178\]

与Chromium OS从下载的源代码可被编译不同，Chrome
OS只适配搭载在Google的合作生产商的硬件上。与[Chrome浏览器相似](../Page/Google_Chrome.md "wikilink")，Chrome
OS的[用户界面很简洁化](../Page/用户界面.md "wikilink")。这款操作系统的目标是将用户用在[电脑上的时间尽量的分配在](../Page/电脑.md "wikilink")[互联网之上](../Page/互联网.md "wikilink")，因此搭载Chrome
OS系统的设备上，附带应用程序数量比较少，且都与网页浏览器相关。\[179\]\[180\]\[181\]\[182\]\[183\]

2011年5月[Google
I/O大会上宣布](../Page/Google_I/O.md "wikilink")，最先上市的[Chromebook上网本是由](../Page/Chromebook.md "wikilink")[宏碁和](../Page/宏碁.md "wikilink")[三星在同年的](../Page/三星電子.md "wikilink")7月15日推出。\[184\]2013年7月24日Google推出更薄、更輕、更快的第二代[Nexus
7平板電腦](../Page/Nexus_7.md "wikilink")，以及可作Wi-Fi串流在電視播放平板電腦畫面的「電視棒」[Chromecast](../Page/Chromecast.md "wikilink")。

### 电子商务

[Google
Checkout是一项简化在线购物支付的服务](../Page/Google_Checkout.md "wikilink")。\[185\]2006年6月28日和翌年的4月13日分别在美国和英国开放。2011年，Google公司宣布[团购业务](../Page/团购.md "wikilink")，与[Groupon等网站展开正面竞争](../Page/Groupon.md "wikilink")。\[186\]4月21日，美国[俄勒冈州的](../Page/俄勒冈州.md "wikilink")[波特兰成为该团购业务于测试阶段的首个开放城市](../Page/波特兰_\(俄勒冈州\).md "wikilink")，\[187\]7月12日Google又将服务范围扩张到[纽约商业区和](../Page/纽约.md "wikilink")[旧金山湾区](../Page/旧金山.md "wikilink")。\[188\]9月，Google在中国开通团购网站“[谷-{}-歌时惠](../Page/谷歌时惠.md "wikilink")”；在欧洲团购市场，Google收购了德国团购网站DailyDeal。Google又推出[Google
Wallet手机应用](../Page/Google_Wallet.md "wikilink")，\[189\]涉足无线支付和[近场通讯领域](../Page/近场通讯.md "wikilink")。

### 其他产品

[EMachinesM5405Laptop.JPG](https://zh.wikipedia.org/wiki/File:EMachinesM5405Laptop.JPG "fig:EMachinesM5405Laptop.JPG")連上Google\]\]
[Google翻译是一个服务器端的](../Page/Google翻译.md "wikilink")[机器翻译服务](../Page/机器翻译.md "wikilink")，可以翻译103种不同的语言。\[190\][浏览器通过安装相应扩展即能实现在浏览器中使用Google翻译](../Page/浏览器.md "wikilink")。Google翻译的软件使用[语料库语言学技术](../Page/语料库语言学.md "wikilink")，程序会向专业的翻译文件资料“学习”，例如参考[联合国或](../Page/联合国.md "wikilink")[欧洲议会的会议事项记录](../Page/欧洲议会.md "wikilink")。\[191\]

2002年，Google推出[Google新闻服务](../Page/Google新闻.md "wikilink")。当时，Google宣布制造了一个“极不寻常的”网站，可以“在只靠电脑算法而没有人工干预的情况下编选和提供新闻，Google并没有雇佣任何编辑。”\[192\]

2008年5月，Google在中国大陆收购了上网导航服务[265导航](../Page/265导航.md "wikilink")，进一步拓展了其在中国大陆的业务。

2011年10月6日，Google推出[Google云平台](../Page/Google云平台.md "wikilink")(Google
Cloud Platform)。该平台是一项使用了Google核心基础架构、数据分析和机器学习技术的云计算服务。

2012年7月，Google正式推出光纖寬頻服務[Google光纖](../Page/Google光纖.md "wikilink")，率先在美國[堪萨斯城試驗一套超高速網絡系統](../Page/堪萨斯城.md "wikilink")，可以同時提供上網及有線電視服務。\[193\]

## 企业事务和文化

Google公司一直以其轻松随意的企业文化著称于世。在2007年、2008、2012年\[194\]的《[财富](../Page/财富_\(杂志\).md "wikilink")》杂志最适合工作的企业排行榜中位列榜首，\[195\]\[196\]
2009年和2010年则位居第四位。\[197\]\[198\]同时，Google在Universum的通信人才的吸引力指数中被列为世界上大学毕业生最向往的雇主。\[199\]
Google的网站声称，公司的企业哲学在其一些看上去漫不经心的信条中展现得淋漓尽致，如“無需作惡，也可賺錢”、“無需西裝革履，也可認真執著”\[200\]、“工作充满着挑战而挑战充满欢乐”等等。\[201\]

### 雇员

[Noogler.png](https://zh.wikipedia.org/wiki/File:Noogler.png "fig:Noogler.png")
自[上市后Google股票的良好涨势使许多早期员工获得丰厚的回报](../Page/上市.md "wikilink")。\[202\]
IPO之后，创始人[拉里·佩奇](../Page/拉里·佩奇.md "wikilink")、[谢尔盖·布林和前CEO](../Page/谢尔盖·布林.md "wikilink")[埃里克·施密特主动要求将他们自己的基本薪水降至](../Page/埃里克·施密特.md "wikilink")1美分，随后公司将他们的资薪调低，而他们的收入主要来自于持有的Google股票。2004年以前，施密特每年收入为25万美元，佩奇和布林则为15万。\[203\]

2007年至2008年初，有多位Google高管离任。2007年10月，[YouTube前](../Page/YouTube.md "wikilink")[首席财务官吉迪昂](../Page/首席财务官.md "wikilink")·余同高级工程师本杰明·林一道跳槽至[Facebook](../Page/Facebook.md "wikilink")。\[204\]\[205\]
2008年3月，[雪莉·桑德伯格](../Page/雪莉·桑德伯格.md "wikilink")，当时的全球网络销售与运营副总裁，也离开Google，在Facebook擔任首席财务官一职。\[206\]另外，阿什·艾尔迪佛莱威——前品牌广告负责人前往Netshops公司（一家网络零售企业，在2009年更名为）擔任[首席营销官](../Page/首席营销官.md "wikilink")。\[207\]
2011年4月4日，拉里·佩奇重任[首席执行官](../Page/首席执行官.md "wikilink")（Chief Executive
Officer），施密特改任执行[董事长](../Page/董事长.md "wikilink")（Executive
Chairman）。\[208\]

Google有一个称为「创意休息時間」（Innovation Time
Off）的政策，允许工程师花20%的工作时间做自己想做的事情，以此来激励更多、更广泛的创新，而这项政策也确确实实激发出了[Gmail](../Page/Gmail.md "wikilink")、[Google新闻](../Page/Google新闻.md "wikilink")、[Orkut](../Page/Orkut.md "wikilink")、[AdSense等产品的创意](../Page/AdSense.md "wikilink")。\[209\]
Google的搜索产品和用户体验副总裁[玛丽莎·梅耶尔曾在一次斯坦福大学的访谈中展示过当时半数来源于](../Page/玛丽莎·梅耶尔.md "wikilink")「创意休息時間」的产品。\[210\]

2011年3月，Universum的问卷调查报告显示，Google以接近25%的选择率成为年轻人最嚮往的雇主。\[211\]

### 办公地点

[Googleplexsouthsidesecondangle.jpg](https://zh.wikipedia.org/wiki/File:Googleplexsouthsidesecondangle.jpg "fig:Googleplexsouthsidesecondangle.jpg")，Google公司的总部\]\]
Google在[美国](../Page/美国.md "wikilink")[加州](../Page/加州.md "wikilink")[山景城的总部被昵称为](../Page/山景城.md "wikilink")「Googleplex」，数学大数[古戈尔普勒克斯](../Page/古戈尔普勒克斯.md "wikilink")（googolplex，数字1後有10<sup>100</sup>个0，即[自然数](../Page/自然数.md "wikilink")10<sup>10<sup>100</sup></sup>=10<sup>googol</sup>）单词的变体。总部本身也是一栋错综复杂的综合性建筑。大厅用一架[钢琴](../Page/钢琴.md "wikilink")、[熔岩灯](../Page/熔岩灯.md "wikilink")、老旧的服务器装饰，墙上还有一副搜索请求的投影。门厅则布满了健身球和自行车。建筑内还有娱乐中心供公司员工休闲娱乐，健身房、更衣室、洗衣干衣机、按摩室、配套[游戏机](../Page/游戏机.md "wikilink")、[桌上足球](../Page/桌上足球.md "wikilink")、小型三角钢琴、台球桌、乒乓球桌等等日常生活或娱乐设施设备散布在大楼内部。除此以外，Googleplex有着一个填满食物和饮料的餐厅，食物都以重视营养为重点。\[212\]餐厅和自动售卖机每周七天，每天二十四小时免费向员工开放。\[213\]

[PONYA_Inland_Term_1_jeh.JPG](https://zh.wikipedia.org/wiki/File:PONYA_Inland_Term_1_jeh.JPG "fig:PONYA_Inland_Term_1_jeh.JPG")的办公大楼\]\]
2003年，为了包括[Google地图等项目](../Page/Google地图.md "wikilink")，Google增加了一支在[纽约的工程师队伍](../Page/纽约.md "wikilink")。2006年，Google在纽约市的办公地址搬进了位于[曼哈顿第八大道的面积为](../Page/曼哈顿.md "wikilink")28900平米的新大楼，目前容纳着公司最大的广告销售团队。\[214\]而这处办公大楼的设计与功能也与Googleplex有着相似之处，甚至同样有着桌上足球、桌上冰球、乒乓球台和电子游戏机。2006年11月，公司在[匹兹堡](../Page/匹兹堡.md "wikilink")[卡内基梅隆大学校区的办公场所开张](../Page/卡内基梅隆大学.md "wikilink")，Google在这里的工作主要是购物广告代码和[智能手机应用](../Page/智能手机.md "wikilink")。\[215\]\[216\]
06年底，Google又在[密歇根州的](../Page/密歇根州.md "wikilink")[安娜堡为](../Page/安娜堡.md "wikilink")[AdWords添置了一处新的总部](../Page/AdWords.md "wikilink")。\[217\]此外，Google的办公场所遍布全世界，仅在美国就包括[亚特兰大](../Page/亚特兰大.md "wikilink")、[奧斯汀](../Page/奧斯汀.md "wikilink")、[旧金山](../Page/旧金山.md "wikilink")、[西雅图](../Page/西雅图.md "wikilink")、[华盛顿等多处](../Page/华盛顿.md "wikilink")。

Google对环境保护抱有相当高的热忱。2006年10月，Google公司宣布投资建造太阳能发电项目的计划，建成后可为Googleplex提供产生1.6[兆瓦的电力](../Page/兆瓦.md "wikilink")，满足接近30%的用电需求。\[218\]这套发电系统成为了全美最大的企业发电系统，也跻身世界最大的企业发电系统之一。\[219\]在2009年，Google租赁了一群[山羊来为Googleplex周围的草地除草](../Page/山羊.md "wikilink")。这个方法既能防止季节性的丛林火灾，也可以大大减少大面积除草所产生的碳排放。\[220\]\[221\]据透露这个用山羊修剪草坪的点子来自于一位名叫R·J·韦勒的[美国国家半导体有限公司的工程师](../Page/美国国家半导体有限公司.md "wikilink")。\[222\]尽管如此，Google依旧遭到了[《哈泼斯》杂志对于能源极端过度使用的指责](../Page/哈泼斯.md "wikilink")，同时被指控打着“不作恶”口号和公开举行节能活动的幌子来掩盖企业所运行的服务器实际对能源的海量需求。\[223\]

### 資料中心

Google公司因應全球使用者的需求，在美國及世界各地建立[資料中心](../Page/資料中心.md "wikilink")。至2013年底，Google公司已在美國（6處）、[智利](../Page/智利.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")、[新加坡及](../Page/新加坡.md "wikilink")[台灣設有共](../Page/台灣.md "wikilink")12處資料中心，另还有其他資料中心籌建中\[224\]。

其中[台灣資料中心是Google在亞洲最大的資料中心](../Page/Google台灣資料中心.md "wikilink")\[225\]，以進軍亞洲的[雲端產業](../Page/雲端產業.md "wikilink")\[226\]，而該資料中心有機器人為主題\[227\]。

大多數資料中心的業主基於[資訊安全考量](../Page/資訊安全.md "wikilink")，極少透露其資料中心的資訊及內部情形。不過Google公司曾於2012年在其網站设立专栏，公開了各地資料中心的內部照片及部份街景圖，讓外界一窺其神秘面紗。\[228\]

### 愚人节玩笑和复活节彩蛋

[Google_toilet_control.jpg](https://zh.wikipedia.org/wiki/File:Google_toilet_control.jpg "fig:Google_toilet_control.jpg")
Google在每年的[愚人节有制造玩笑的传统](../Page/愚人节.md "wikilink")。2000年愚人节，Google推出MentalPlex（意念搜索），号称能以思维意念的力量来进行网络搜索。\[229\]
2007年，Google则宣传发布一套名为TiSP（厕所互联网服务提供者）的免费服务，用户可以凭借将Google所提供的[光导纤维电缆的一头冲到厕所下水管道之内来获取宽带服务](../Page/光导纤维.md "wikilink")。\[230\]同在2007年，Google在[Gmail页面上展示即将推出Gmail](../Page/Gmail.md "wikilink")
Paper服务的提示，称将可以满足替用户把邮件打印到纸上并运送给收件人的要求。\[231\]
2008年，Google说Gmail的用户将可以更改已发送邮件的发送时间。\[232\]
2010年的愚人节，为了回应[堪萨斯首府](../Page/堪萨斯.md "wikilink")[托皮卡城市长之前为了申请](../Page/托皮卡.md "wikilink")[Google光纤而颁布法令将城市名暂时改为Google](../Page/Google光纤.md "wikilink")，Google戏剧性地把公司名改为Topeka以纪念該城市。\[233\]\[234\]
2011年的玩笑则名为Gmail运动，号称用户可以通过[摄像头前做肢体运动来控制Gmail或计算机](../Page/摄像头.md "wikilink")。\[235\]
2013年的愚人節則推出[Google嗅覺](../Page/Google嗅覺.md "wikilink")，號稱為一個測試版的香味[資料庫](../Page/資料庫.md "wikilink")，使用戶可以透過大部分的螢幕聞到各種味道。\[236\]

除了在愚人节会开一些玩笑之外，制作[彩蛋也是Google的保留项目](../Page/复活节彩蛋.md "wikilink")。例如Google在搜索引擎的语言选择上非常滑稽的加入了提线木偶剧中的“Bork
bork
bork”语言、[儿童黑话](../Page/儿童黑话.md "wikilink")、黑客语、卡通人物的语言、《[星际迷航](../Page/星际迷航.md "wikilink")》中的[克林贡语等等](../Page/克林贡语.md "wikilink")。\[237\]
Google搜索还能够计算并提供[生命、宇宙以及任何事情的终极答案](../Page/生命、宇宙以及任何事情的终极答案.md "wikilink")（*The
answer to life, the universe, and
everything*，出自于[道格拉斯·亚当斯的](../Page/道格拉斯·亚当斯.md "wikilink")[科幻小说](../Page/科幻小说.md "wikilink")《[银河系漫游指南](../Page/银河系漫游指南.md "wikilink")》）。\[238\]此外，当一个人搜索单词「recursion」（递归）时，系统中的拼写检查会给你一个“纠错”链接，显示的建议依旧是「recursion」这个单词然后一直如此循环。\[239\]类似的彩蛋还有，当搜索单词「anagram」（一种颠倒字母顺序的字谜）时，Google提供的建议会显示“你是不是在找nag
a
ram？”\[240\]如果[Google地图中在间隔大片水域的两个地点之间搜寻路线](../Page/Google地图.md "wikilink")，比如从[洛杉矶到](../Page/洛杉矶.md "wikilink")[东京](../Page/东京.md "wikilink")，搜索结果会建议“划[独木舟穿越](../Page/独木舟.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")”。在[2010年世界杯足球赛期间](../Page/2010年世界杯足球赛.md "wikilink")，搜索关键词若是“[世界杯](../Page/世界杯.md "wikilink")”、“[FIFA](../Page/FIFA.md "wikilink")”等，就会导致Google页面底部原为「Goooo...gle」的页面指标变成「Goooo...al\!」。\[241\]

### Doodles

自从BackRub时代起，Google已经有过许多品牌[标识](../Page/标识.md "wikilink")，现今Google公司的官方标志是以[Catull设计一个文字](../Page/Catull.md "wikilink")[商标](../Page/商标.md "wikilink")\[242\]。除了这个主要的商标之外，Google经常会在节日、名人诞辰日或重大事件（如[奥运会](../Page/奥运会.md "wikilink")）发生的时候把标志作修改后替代原商标放在网站上\[243\]。这些特殊的标志被称为「Google
Doodles」（“谷-{}-歌涂鸦”）。截至目前，Google已经制作和更换了超过1000个doodles\[244\]。

2010年5月21日，为了纪念[街机游戏](../Page/街机游戏.md "wikilink")[吃豆子](../Page/吃豆人.md "wikilink")30周年，Google在与[南梦宫交流之后将标志改成了可以玩吃豆子游戏的迷宫地图](../Page/南梦宫.md "wikilink")。\[245\]这个小游戏还有着与原版一样的音效。“-{zh-hant:好手氣;
zh-hans:手气不错}-”（*I'm Feeling Lucky*）键则被“投入硬币”（*Insert
Coin*）键所暂时取代。按下这个键可以加入第二位玩家——“”，用键盘上的W、S、A、D键操控。两天后的5月23日，吃豆子游戏的doodle被移除。但没过几天，为了满足用户们的要求，Google把这个doodle永久放置在了一个子域名上。\[246\]\[247\]

2011年6月9日，为了纪念[电吉他之父](../Page/电吉他.md "wikilink")[莱斯·保罗诞辰](../Page/莱斯·保罗.md "wikilink")96周年，Google在主页放置了一个可互动的电吉他式doodle。除了能用悬停的鼠标指针来弹奏该电吉他之外，用户也可以用键盘按键来进行弹奏。用户可以在弹奏一段30秒以内的旋律，并用[URL的方式发送给他人](../Page/URL.md "wikilink")。同样由于热门，这个电吉他式的doodle被额外增加了一天的展示时间，之后亦被放置在一个永久的域名下。\[248\]\[249\]

除了官方团队会制作Doodles以外，Google也鼓励人们参与到创造的行列中。Google每年举办[「Doodle 4
Google」大赛](../Page/Doodle4Google大赛.md "wikilink")，吸收参赛者的创意并嘉奖入选者。\[250\]

### 慈善事业

2004年，Google建立了非营利的慈善机构Google.org，起始基金为10亿美元。\[251\]组织的使命是建立公众对[气候变化](../Page/气候变化.md "wikilink")、全球公共卫生、全球贫困等问题的意识。它的首批计划之一是开发一款可行的每[加仑燃料行驶](../Page/加仑.md "wikilink")100英里的[混合动力](../Page/插电式混合动力汽车.md "wikilink")[电动载具](../Page/电动载具.md "wikilink")。2004年Google聘请了为该项目的执行经理，\[252\]目前的执行经理则为梅根·史密斯。\[253\]

2008年，Google宣布“创想10<sup>100</sup>项目”，激励人们想出如何帮助人类并从中精选投票。\[254\]经过两年创意投稿的时间之后，\[255\]
Google最终揭示了投票结果并把一千万美元的资金投入到从提高[非洲的教育条件到建设免费互联网在线教学等等各式各样的创意的实践中](../Page/非洲.md "wikilink")。\[256\]

自2008年起，Google在中国大陆发起举办“‘益暖中华’——谷-{}-歌杯中国大学生公益创意大赛”，征集公益创意，并对优胜项目予以奖励，以此来倡导大学生投身社会公益事业。截至2011年，大赛举办了四届。\[257\]

2011年，Google公司为[国际奥林匹克数学竞赛捐款一百万](../Page/国际奥林匹克数学竞赛.md "wikilink")[欧元](../Page/欧元.md "wikilink")，以支持该竞赛2011年至2015年的举办。\[258\]

2012年，Google創立全球影響力大獎\[259\]，鼓勵運用科技和創新解決方案來應付人類當前挑戰的機構與組織。Google投入了2300萬美元來獎勵七個組織\[260\]，包括[世界自然基金会](../Page/世界自然基金会.md "wikilink")。

### 网络中立性

Google是著名的[网络中立性的支持者](../Page/网络中立性.md "wikilink")。2006年2月7日，[文顿·瑟夫](../Page/文顿·瑟夫.md "wikilink")，[网际协议](../Page/网际协议.md "wikilink")（IP）的发明人之一，同时也是Google的副总裁和“首席互联网专员”在[国会中说道](../Page/国会.md "wikilink")：“允许宽带运营商控制人们在互联网上看什么或做什么从根本上破坏现已给互联网带来如此之成功的网络中立性原则。”\[261\]

但也有报道认为，Google也是網路中立性的違反者。\[262\]

### 隐私

前任首席执行官[埃里克·施密特在](../Page/埃里克·施密特.md "wikilink")2007年接受[金融时报采访时说](../Page/金融时报.md "wikilink")：“我们的目标是让Google的用户可以得到诸如‘我明天该做什么？’和‘我应该参加什么工作’之类问题的答案”。\[263\]
2010年，在接受[华尔街日报的采访时他再一次重申](../Page/华尔街日报.md "wikilink")：“我想事实上大多数人并不渴望Google回答他们的问题，他们想让Google告诉他们接下去该做什么。”\[264\]

2009年12月，施密特在一些人表示了对隐私权的担忧之后声明：“如果有些东西你不想让别人知道，或许首先你不应该做这件事。如果你真正需要某种隐私权，事实上是搜索引擎——包括Google在内——都会在一段时间内保留有这些信息，而且这很重要。举个例子，我们美国人都得遵守《[爱国者法案](../Page/爱国者法案.md "wikilink")》，因而我们所有人的信息都被掌控在当局的手裡。”\[265\]评价Google“敌对隐私权”，并在报告中将其排在最后一位。\[266\]\[267\]\[268\]

在2010年的Techonomy大会上，施密特预测“实名而非匿名”是引领因特网前进的要素：“在一个充满不同步威胁的世界裡，若没有方法可以辨认你是谁将会很危险。我们需要为人们提供一个实名验证的服务。政府也需要它。”“如果我看了足够多关于你的消息和所在位置，我们就能用人工智能预测你要去哪里。给我们你的14张照片，我们就能辨别出你。你想你没有14张照片在互联网上？实际上在你的[Facebook照片裡就有](../Page/Facebook.md "wikilink")！”\[269\]

[Googlecar.png](https://zh.wikipedia.org/wiki/File:Googlecar.png "fig:Googlecar.png")的汽车\]\]
一个名为“公众信息研究”（Public Information Research）的非营利组织开设了「Google
Watch」网站，标语为“一份对Google垄断、算法和隐私问题的观察”。\[270\]\[271\]这个网站针对有着存储[Cookie向Google提出了一些问题](../Page/Cookie.md "wikilink")。\[272\]
Google也因推出社交网络服务[Google
Buzz而遭受诟病](../Page/Google_Buzz.md "wikilink")，在Google
Buzz的默认设置中，[Gmail用户的联系人列表会自动公开](../Page/Gmail.md "wikilink")。\[273\]由于在一些国家和地区对某些网站实行审查制度，Google也受到了批评。一直到2010年3月[退出中国为止](../Page/谷歌退出中国事件.md "wikilink")，Google一直在坚持[中国大陆的网络审查制度](../Page/中华人民共和国网络审查.md "wikilink")，被强制以俗称为“[防火长城](../Page/防火长城.md "wikilink")”的过滤器来执行网络审查。\[274\]

尽管在国家和地区政治事务上很有影响力，但Google并不透露其用于政治的开支。2010年8月，纽约市公益维护官[白思豪开展了一场全国运动](../Page/白思豪.md "wikilink")，敦促公司透露所有的政治开支情况。\[275\]

2006年至2010年间，拍摄[Google街景的小车经过三十多个国家](../Page/Google街景.md "wikilink")，从未加密的公共或私人[Wi-Fi网络收集了约](../Page/Wi-Fi.md "wikilink")600[GB的数据](../Page/吉字节.md "wikilink")。但是Google并没有给予受影响的人Wi-Fi信号的主人以隐私权处理的解释。一位Google的代表声称他们在接到[德国监管机构的质询之前](../Page/德国.md "wikilink")，并没有意识到收集数据带来的问题，并解释说收集到的信息也不会用于搜索或其他服务。为了避免可能会导致的法律制裁，Google称其不会销毁数据，直到获得政府相关机构的许可。\[276\]\[277\]

2013年6月，Google被指參與[稜鏡計劃](../Page/稜鏡計劃.md "wikilink")，\[278\]\[279\]隨後Google對媒體宣告，\[280\]並在其官方部落格上發表了由該公司時任執行長和時任首席法務官聯合署名的文章。\[281\]同时，彭博社曝出Google等公司与美国国家安全部门紧密合作，向其提供敏感信息，同时获得机密情报的新聞。\[282\]

2014年5月，中华人民共和国国务院新闻办互联网新闻研究中心发布了一份名为《美国全球监听行动纪录》的报告，聲稱美国的棱镜计划对华窃密，“微软Google均有配合”。\[283\]

2018年9月，曝出Gmail 未经用户同意违规扫描邮件信息而涉嫌泄露用户个人信息，美国将在9月26日举行听证会。Google方面表示不予置评。

### 互聯網自由

#### 关闭Google.cn搜索服务

[Google_China_headquarter_in_Beijing.jpg](https://zh.wikipedia.org/wiki/File:Google_China_headquarter_in_Beijing.jpg "fig:Google_China_headquarter_in_Beijing.jpg")[中关村](../Page/中关村.md "wikilink")[清华科技园内的总部](../Page/清华科技园.md "wikilink")\]\]
2010年1月12日，Google公司发表声明称受到来自[中国大陆的](../Page/中华人民共和国.md "wikilink")[黑客攻击](../Page/黑客.md "wikilink")，并将与[中华人民共和国政府谈判](../Page/中华人民共和国政府.md "wikilink")，要求取消[谷-{}-歌中国](../Page/谷歌中国.md "wikilink")[搜索引擎的过度](../Page/搜索引擎.md "wikilink")[内容审查](../Page/中华人民共和国网络审查.md "wikilink")，否则谷-{}-歌中国会退出中国大陆市场。事件引起了关于[网络自由](../Page/网络自由.md "wikilink")、[言论自由](../Page/言论自由.md "wikilink")，以及事件背后真正原因的争论。两个多月后，从[北京时间](../Page/北京时间.md "wikilink")（[UTC+8](../Page/UTC+8.md "wikilink")）3月23日凌晨起，Google公司决定将原有谷-{}-歌中国的两个域名（google.cn和g.cn）重定向至[Google香港的域名](../Page/Google香港.md "wikilink")（google.com.hk），并使用通过其在[香港或海外的](../Page/香港.md "wikilink")[服务器实现未经审查过滤的搜索引擎服务](../Page/服务器.md "wikilink")\[284\]，但中国大陆用户搜索[敏感词汇会因防火长城而导致链接被重置](../Page/关键词过滤.md "wikilink")。北京时间2010年4月3日，Google香港网站Logo由“Google谷-{}-歌”更名为“Google中国”，有人认为这意味着使用四年的“谷-{}-歌”名称可能遭弃用。\[285\]但在第二天，2010年4月4日（[UTC+8](../Page/UTC+8.md "wikilink")）Google香港网站Logo又由“Google中国”改回“Google谷-{}-歌”。\[286\]

谷\-{}-歌不屈服於中國當局網路控制的行動，引發全球關注和人權組織的廣泛讚揚。被外界視為中國第一家人權網站的[六四天網創辦人](../Page/六四天網.md "wikilink")[黃琦表示很讚賞谷](../Page/黃琦.md "wikilink")-{}-歌公司堅持[普世價值的骨氣](../Page/普世價值.md "wikilink")，面對中國的新聞封鎖和[網路封鎖](../Page/中華人民共和國網路審查.md "wikilink")，一直不妥協。這種做法理應受到全世界企業的效倣。\[287\]

但是，此次Google结束的仅仅是中国大陆的搜索业务，而[Google翻译](../Page/Google翻译.md "wikilink")、[Google地图等Google在中国大陆的无关互联网搜索的业务照常营运](../Page/Google地图.md "wikilink")。Google在中国大陆的官方[博客](../Page/博客.md "wikilink")“Google黑板报”也照常更新\[288\]。

自从2010年[Google中国将搜索业务迁移至香港以来](../Page/Google中国.md "wikilink")，Google香港域名便时常遭到中华人民共和国政府的关键词过滤和全站屏蔽，致使中国大陆网民无法正常使用Google搜索。

2011年，根据不完全统计，中国大陆顶级互联网网站前5名中，谷-{}-歌香港的域名“Google.com.hk”排在第五名。\[289\]

2013年11月，谷-{}-歌公司執行主席埃里克·施密特呼籲中國[開放網際網路](../Page/中華人民共和國網路審查.md "wikilink")、尊重民眾的[言論自由](../Page/言論自由.md "wikilink")。他說中國大陆的新聞檢查問題明顯惡化，他對中华人民共和国當局加緊打壓網路言論、對轉發所謂“網路謠言”500次入罪的司法解釋感到擔憂，並表示如果中國不改變目前的[新聞檢查制度](../Page/中華人民共和國新聞自由.md "wikilink")，谷-{}-歌就不會重返中國。\[290\]

2014年5月29日起，Google所有服務（包括Gmail服務）在中國大陆被[防火長城](../Page/防火長城.md "wikilink")[大規模干擾至基本無法使用](../Page/2014年中国大陆屏蔽谷歌服务事件.md "wikilink")，重定向至[Google香港的域名也遭到屏蔽](../Page/Google香港.md "wikilink")，Google指中國大陆的網絡仍然可局部使用其服務，如未迁移至香港的[地图](../Page/Google地图.md "wikilink")、[翻译等](../Page/Google翻译.md "wikilink")\[291\]\[292\]\[293\]\[294\]\[295\]\[296\]。直至2014年9月，Google.cn域名下的大部分服务恢复正常。

2015年六四前夕，长城防火墙加强对Google的封锁，众多IP地址也被封锁，此后除了Google.cn域名的翻译、地图服务（除街景外）、[网站导航服务及广告等业务可以部分使用之外](../Page/265上网导航.md "wikilink")，其他所有Google服务均无法在大陆使用\[297\]。

#### 反對管轄互聯網

2012年12月3日舉行的國際電信大會討論中華人民共和國和[俄羅斯等国支持的國際電信規則修訂方案](../Page/俄羅斯.md "wikilink")，修訂方案建議將互聯網包含在[聯合國下屬機構](../Page/聯合國.md "wikilink")[國際電信聯盟的管轄範圍之內](../Page/國際電信聯盟.md "wikilink")。Google為此發起了反擊，呼籲[網民支持自由和開放的互聯網](../Page/網民.md "wikilink")。

Google建立了一個參與行動的網站，並聲明「互聯網將全球20多億人聯繫在一起。一些國家/地區的政府想要在12月份召開秘密會議，以期就網絡內容審查制度和網絡管制達成共識。我們應該團結起來，維護互聯網的自由與開放。」，「自由和開放的世界取決於自由和開放的網絡。各國政府不應該單獨決定互聯網的前路。全球數以十億計使用互聯網的人，以至建立和維護互聯網的專家，都應該出一分力。」\[298\]

由于[中國互聯網絡信息中心的合作方MCS](../Page/中國互聯網絡信息中心.md "wikilink")
Holdings签发多个Google域名的不当证书，2015年4月2日，Google宣佈不再信任该中心發佈的[安全證書](../Page/安全證書.md "wikilink")，网站若使用该中心颁发的证书，用户在[Google
Chrome中访问时將受到谷歌的安全警告](../Page/Google_Chrome.md "wikilink")\[299\]\[300\]。

## 爭議

  - 2014年8月，Google向[美國警方報警](../Page/美國.md "wikilink")，表示其用戶[Gmail內有大量兒童色情照片](../Page/Gmail.md "wikilink")，隨後該男子遭逮捕，此事一出也引發許多用戶對隱私的擔憂。
  - 2017年，有伊莉版主因上傳霹靂布袋戲至Google雲端硬碟而被檢方向台灣Google資料處理中心調閱資料，獲美國Google同意。全案偵查起訴。\[301\]
  - 2018年4月11日臉書洩密風波，相關資訊單位如蘋果、google等受到檢查，尤其大型搜尋引擎google也被批評管理不當而導致其用戶資料洩漏。\[302\]
  - 2017年10月12日一個使用[Google Home
    mini的語音助理產品其用戶](../Page/Google_Home_mini.md "wikilink")，被發現其裝置在聽到"特定詞彙"會自動產生錄音檔並進行雲端上傳，變相形成了竊聽私人用戶的用具引起爭議，雖然google事後單方面宣稱已經更新，但是並公開未提出書面解釋或是實際的產品檢討報告，所以最後問題不了了之\[303\]
  - 2017年8月7日一名底下公司工程師在網上發表了3000多字關於女性不適合當工程師的言論，相關爭議的言論讓[女性主義的群體群體激憤](../Page/女性主義.md "wikilink")，並引發google對其底下人員管理不當的潛在隱憂。\[304\]
  - 不當監控消費者生活資訊，根據美國[彭博社](../Page/彭博社.md "wikilink")2019年2月12日報導，Google要求合作的裝置廠商將使用者的行為資料持續傳送給他們，2018年開始Google要求裝置廠商修改程式碼，不再被動由消費者下指令才取得資訊，而是要求廠商主動持續回報裝置狀態，例如消費者看了那些電視頻道、家中電燈及門鎖狀態。報導引述[羅技等業者](../Page/羅技.md "wikilink")，這些廠商以侵犯用戶隱私為由試圖拒絕兩家大廠的要求，但遭Google回絕。\[305\]

## 故障

2013年8月17日06:50至06:55，谷-{}-歌遭遇了故障，包含[Google搜尋](../Page/Google搜尋.md "wikilink")、[YouTube](../Page/YouTube.md "wikilink")、[Google
Drive和](../Page/Google_Drive.md "wikilink")[Gmail等服务都受到了影响](../Page/Gmail.md "wikilink")。此事導致全球网络流量暴跌40%\[306\]。

2014年1月25日，Google和旗下其他許多服務疑因軟體出包故障，據信影響全球數億民眾\[307\]。

2019年3月13日上午10時左右，發生全球大當機問題，Gmail出現無法發送郵件、無法下載附件檔案等情形，Google雲端硬碟也傳出有大lag或是檔案無法下載的狀況。直到14時13分，Google才在官網公告已經解決Gmail和Google雲端硬碟的問題，表示對於造成用戶不便感到抱歉，但是並未提到造成當機的原因。\[308\]

## 註解

<references group="註" />

## 參考文獻

  -
## 參考來源

## 外部链接

  - \[//www.google.com/ncr Google 首页\] 不进行国家跳转

  - \[//www.google.com/webhp?hl=zh-CN Google 简体中文国际版\]

  - \[//www.google.com/webhp?hl=zh-TW Google 繁體中文國際版\]

  - \[//www.google.com.hk/ Google 香港\]

  - \[//www.google.com.tw/ Google 台湾\]

  - \[//www.google.com/about/company/ Google 企業資訊\]

  - [Google 官方博客](https://blog.google/)

  - \[//www.google.org/ Google 公益\]

  -
## 參見

  - [Alphabet Inc.](../Page/Alphabet_Inc..md "wikilink")
  - [谷-{}-歌中国](../Page/谷歌中国.md "wikilink")
  - [Google 搜索](../Page/Google搜索.md "wikilink")
  - [Google 档案系統](../Page/Google档案系統.md "wikilink")
  - [MapReduce](../Page/MapReduce.md "wikilink")
  - [-{zh-cn:谷歌;zh-tw:Google }-退-{}-出中国事件](../Page/谷歌退出中国事件.md "wikilink")
  - [Google 产品列表](../Page/Google产品列表.md "wikilink")
  - [Google 公司收购列表](../Page/Google公司收购列表.md "wikilink")
  - [Googleplex](../Page/Googleplex.md "wikilink")
  - [實習大叔](../Page/實習大叔.md "wikilink")
  - [網路主權](../Page/網路主權.md "wikilink")

[Google](../Category/Google.md "wikilink")
[Category:美國互联网公司](../Category/美國互联网公司.md "wikilink")
[Category:總部在美國的跨國公司](../Category/總部在美國的跨國公司.md "wikilink")
[Category:山景城公司](../Category/山景城公司.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")
[Category:全球資訊網](../Category/全球資訊網.md "wikilink")
[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")
[Category:2004年IPO](../Category/2004年IPO.md "wikilink")

1.

2.  参见：[Google产品列表](../Page/Google产品列表.md "wikilink").

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21. \[//www.google.com/chromebook chromebook\]

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33. [2010-03-27
    archive](http://web.archive.org/web/*/http://www.stanforddaily.com/2003/02/12/from-googol-to-google/)

34.

35.

36.

37.

38.
39.

40.

41.

42.
43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58.

59.

60.
61.

62.
63.

64.
65.

66.

67.

68.

69.

70.

71.

72.

73. [Google’s new record, 1 billion visitors in
    May](http://itsalltech.com/2011/06/22/googles-new-record-1-billion-visitors-in-may/)


74.

75.

76.

77.

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.

88.

89.

90.

91.

92.

93.

94. Google Annual Report, Feb. 15, 2008

95.

96.

97.

98.

99.

100. Mills, Elinor. "[Google to offer advertisers click fraud
     stats](http://news.cnet.com/Google-to-offer-advertisers-click-fraud-stats/2100-1024_3-6098469.html)."
     *[c net](http://www.cnet.com/).* July 25, 2006. Retrieved July 29,
     2006.

101.

102.

103.

104.

105.

106.

107.

108.

109.

110.

111.
112.

113.

114.

115.

116.

117.

118.

119.

120.
121.

122.

123.

124.

125.

126.

127.
128.
129.

130.

131.

132.

133.

134.

135. Parr, Ben. ["Google Launches Google+ To Battle Facebook
     \[PICS](http://www.mashable.com/2011/06/28/google-plus)"\],
     *Mashable.com*, June 28, 2011, accessed June 28, 2011

136.

137.

138.

139.

140. [Google+ hits 20 million mark in three
     weeks](http://news.cnet.com/8301-1023_3-20081650-93/google-hits-20-million-mark-in-three-weeks/?part=rss&subj=news&tag=2547-1_3-0-20)
     cnet 21 July 2011

141.

142.

143. ["Top 50 Domains - www.TheLifeMovie.blogspot.com Ranked by Unique
     Visitors"”](http://blog.compete.com/2007/10/30/top-50-websites-domains-digg-youtube-flickr-facebook/)
     September, 2007.

144. [全球最大博客网站迎来10岁生日](http://w010w.com.cn/htm/chuizhiwangzhanlianmeng/wangzhanpinpaijingying/2011/0420/21135.html)
     ，[2012-07-15
     archive](https://archive.is/http://w010w.com.cn/htm/chuizhiwangzhanlianmeng/wangzhanpinpaijingying/2011/0420/21135.html)、[2011-09-01
     archive](http://web.archive.org/web/*/http://w010w.com.cn/htm/chuizhiwangzhanlianmeng/wangzhanpinpaijingying/2011/0420/21135.html)

145.

146.

147.

148. [YouTube是全球最大的视频分享社区](http://adwords.google.com/support/aw/bin/answer.py?hl=zh-Hans&answer=172554)
     ，[2012-07-20
     archive](https://archive.is/http://adwords.google.com/support/aw/bin/answer.py?hl=zh-Hans&answer=172554)、[2011-08-13
     archive](http://web.archive.org/web/*/http://adwords.google.com/support/aw/bin/answer.py?hl=zh-Hans&answer=172554)

149.

150. "Google Chrome is built with open source code from Chromium."
     Retrieved from
     [Chromium.org](http://dev.chromium.org/developers/how-tos/getting-started)，[2008-09-13
     archive](https://archive.is/http://dev.chromium.org/developers/how-tos/getting-started)、[2008-09-06
     archive](http://web.archive.org/web/*/http://dev.chromium.org/developers/how-tos/getting-started)

151. *"During a press briefing today, Google expressed hope that other
     browsers..."*, Retrieved from: [Google unveils Chrome source code
     and Linux
     port](http://arstechnica.com/open-source/news/2008/09/google-unveils-chrome-source-code-and-linux-port.ars),
     Ars Technica

152.

153. *"Google has made the Chrome source available under a permissive
     BSD license so that..."*, Retrieved from: [Google unveils Chrome
     source code and Linux
     port](http://arstechnica.com/open-source/news/2008/09/google-unveils-chrome-source-code-and-linux-port.ars),
     Ars Technica

154. ，、[2008-09-04
     archive](http://web.archive.org/web/*/http://code.google.com/chromium/terms.html)

155.

156.

157.

158.

159. 参见：[网页浏览器的使用分布](../Page/网页浏览器的使用分布.md "wikilink")

160.

161. [Top 5
     Browsers](http://gs.statcounter.com/#browser-ww-weekly-201120-201220),
     From Week 20 2011 to Week 20 2012.

162.

163.

164.

165.

166.

167.

168.

169.

170.

171.
172.

173.

174.

175.

176.

177.

178.

179.
180.

181.

182.

183.

184.

185. [Google unveils UK payments
     system](http://news.bbc.co.uk/1/hi/business/6549643.stm) BBC News

186.

187.

188.

189. TARA SIEGEL BERNARD, The New York Times. "[Google Unveils App for
     Paying With
     Phone](http://www.nytimes.com/2011/05/27/technology/27google.html?_r=1&ref=technology)."
     May 26, 2011. Retrieved May 30, 2011.

190.

191.

192.

193.

194.

195.

196.

197.

198.

199.

200. \[<http://www.google.com/about/corporate/company/tenthings.html谷->{}-歌：我們的信條\]

201.

202.

203.

204.

205.

206.

207.

208.

209.

210.

211.

212. "\[//www.google.com/corporate/culture.html About the Googleplex\]."
     *Google*. Retrieved March 5, 2008.

213.

214. Reardon, Marguerite. "[Google takes a bigger bite of Big
     Apple](http://news.cnet.com/2100-1024_3-6121970.html)." *[c
     net](http://www.cnet.com/).* October 2, 2006. Retrieved October 9,
     2006.

215.

216.

217.

218. Richmond, Riva. "[Google plans to build huge solar energy system
     for
     headquarters](http://www.post1.net/lowem/entry/google_plans_to_build_huge)
     ." *[MarketWatch](http://www.marketwatch.com/).* October 17, 2006.
     Retrieved October 17, 2006.

219.
220.

221.

222.

223. Strand, Ginger. "[Keyword:
     Evil](http://www.harpers.org/media/slideshow/annot/2008-03/index.html)
     ." Retrieved April 9, 2008.

224.

225.

226. Chiu Yu-Tzu, [Google's largest Asian data center in Taiwan to boost
     cloud
     sector](http://www.zdnet.com/googles-largest-asian-data-center-in-taiwan-to-boost-cloud-sector-7000024174/)
     ZDNET, December 12, 2013

227. [Google opens first data centres in
     Asia](http://www.bbc.co.uk/news/technology-25328932)11 December
     2013

228.

229.

230.

231.

232.

233.

234.

235.

236.

237.

238.

239.

240.

241.

242.

243.

244.

245.

246.

247. 详见\[//www.google.com/pacman/ Google Doodle - 小精靈遊戲上世 30 週年\]

248.

249. 详见\[//www.google.com/logos/2011/lespaul.html Google Doodle - Les
     Paul's 96th Birthday\]

250.

251.

252.

253.

254.

255.

256.

257.

258.

259.

260.
261.

262.

263. [Google’s goal: to organise your daily
     life](http://www.ft.com/cms/s/2/c3e49548-088e-11dc-b11e-000b5df10621,dwp_uuid=e8477cc4-c820-11db-b0dc-000b5df10621.html)
     Financial Times

264. [Google and the Search for the
     Future](http://online.wsj.com/article/SB10001424052748704901104575423294099527212.html)
     Wall Street Journal

265.

266.

267. [Google ranked 'worst' on
     privacy](http://news.bbc.co.uk/2/hi/technology/6740075.stm) BBC
     News, June 2007

268. Delichatsios, Stefanie Alki; Sonuyi, Temitope, ["Get to Know
     Google...Because They Know
     You"](http://groups.csail.mit.edu/mac/classes/6.805/student-papers/fall05-papers/google.pdf),
     MIT, Ethics and Law on the Electronic Frontier, 6.805, December 14,
     2005

269.

270.

271.

272.

273.

274.

275.

276.

277.

278.

279.

280.

281.

282.

283.

284.

285. {{ cite web | url = <http://www.cnbeta.com/articles/107759.htm> |
     title = 谷-{}-歌时代结束 - Google中国名称已经改回 | publisher =
     [cnBeta](../Page/cnBeta.md "wikilink") | date = 2010-04-02 |
     accessdate = 2010-04-03 }}

286.

287. [谷-{}-歌：中國不改言論審查就不會重返中國](http://www.voachinese.com/content/google-cheif-describe-china-internet-control-20131105/1783683.html)，[美國之音](../Page/美國之音.md "wikilink"),
     11.05.2013

288. [Google（谷-{}-歌）中国的博客网志](http://www.google.cn/ggblog/googlechinablog/)


289.

290.
291.

292.

293.

294.

295.

296.

297.

298.

299.

300.

301.

302. [臉書洩密風波](https://udn.com/news/story/7086/3079814)臉書洩密風波恐遭罰7.1兆美元
     蘋果、Google等可能面臨更嚴格監管 2018-04-11 08:45楊又肇

303. [谷歌新智慧裝置爆「竊聽疑慮」](https://tw.appledaily.com/new/realtime/20171012/1220876/)
     設計缺失？谷歌新智慧裝置爆 竊聽疑慮 時間：2017/10/12 谷歌宣稱已經更新了軟體版本，不用再擔心這個問題。

304. [女人無法當科技主管](https://tw.news.yahoo.com/%E5%A5%B3%E4%BA%BA%E7%84%A1%E6%B3%95%E7%95%B6%E7%A7%91%E6%8A%80%E4%B8%BB%E7%AE%A1-%E8%B0%B7%E6%AD%8C%E5%B7%A5%E7%A8%8B%E5%B8%AB%E7%99%BC%E6%96%87-%E6%80%A7%E5%88%A5%E5%B7%AE%E8%B7%9D%E4%B8%8D%E6%98%AF%E6%AD%A7%E8%A6%96-040000723.html)「女人無法當科技主管」
     谷歌工程師發文：性別差距不是歧視上報快訊 2017年8月7日 下午12:00

305.

306.

307.

308.  熱門話題 {{\!}}
     產業|url=[https://money.udn.com/money/story/5648/3696010|work=經濟日報|accessdate=2019-03-14|language=zh-Hant-TW|last=經濟日報](https://money.udn.com/money/story/5648/3696010%7Cwork=經濟日報%7Caccessdate=2019-03-14%7Clanguage=zh-Hant-TW%7Clast=經濟日報)}}