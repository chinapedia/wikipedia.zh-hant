|enname= |number= |symbol= |left=[無}}}](../Page/{{{left.md "wikilink")
|right=[無}}}](../Page/{{{right.md "wikilink")
|above=[無}}}](../Page/{{{up.md "wikilink")
|below=[無}}}](../Page/{{{down.md "wikilink") |series= |series comment=
|group= |period=}}} |block= |series color= |phase color= |atomic
mass=\[\][u](../Page/原子量单位.md "wikilink") |atomic mass 2= |atomic mass
comment= |electron configuration= |electrons per shell= |color=未知；可能是；
|phase= |phase comment= |density gplstp= |density gpcm3nrt= |oxidation
states= }}
}}**是{{\#ifexpr:=1|一種尚未成功合成的|一种人工合成的}}[放射性化学](../Page/放射性.md "wikilink")[元素](../Page/元素.md "wikilink")，它的[化学符号是](../Page/化学符号.md "wikilink")“”，[原子序数是](../Page/原子序数.md "wikilink"){{\#ifexpr:=1|。由於此元素尚未成功合成，所以是未來的研究方向。|{{\#ifexpr:{{\#expr:\>=2
or
{{\#expr:\<=-1}}}}|<span style="color:red"><big>**參數錯誤：Not-found布林值不得為'''</big></span>|。}}
}}}}}

}}在[元素周期表中位于](../Page/元素周期表.md "wikilink")\[\[第族，属于[未知}}}之一](../Page/{{{series.md "wikilink")。其[相对原子质量约为](../Page/相对原子质量.md "wikilink")[u](../Page/原子量单位.md "wikilink")。}}}

{{\#ifexpr:=1|}}|{{\#ifexpr:{{\#expr:\>=2 or
{{\#expr:\<=-1}}}}|<span style="color:red"><big>**參數錯誤：ele-stub布林值不得為**</big></span>|}}}}

{{\#ifexpr:=1||{{\#ifexpr:{{\#expr:\>=2 or
{{\#expr:\<=-1}}}}|<span style="color:red"><big>**參數錯誤：Category布林值不得為**</big></span>|}}}}
<noinclude>  </noinclude>

[7}}}周期元素](../Category/第{{{cyc.md "wikilink")