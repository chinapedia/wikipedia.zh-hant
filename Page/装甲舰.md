**装甲舰**（，此词也指[铁甲舰](../Page/铁甲舰.md "wikilink")）是[德国海军在](../Page/德国海军.md "wikilink")[一战后提出的一种新式舰种](../Page/一战.md "wikilink")，在当时被各国[海军称作](../Page/海军.md "wikilink")**“[袖珍战列舰](../Page/德国级装甲舰.md "wikilink")”或“口袋战列舰”**。
[Panzerschiff_Deutschland.jpg](https://zh.wikipedia.org/wiki/File:Panzerschiff_Deutschland.jpg "fig:Panzerschiff_Deutschland.jpg")
[Admiral_Scheer_in_Gibraltar.jpg](https://zh.wikipedia.org/wiki/File:Admiral_Scheer_in_Gibraltar.jpg "fig:Admiral_Scheer_in_Gibraltar.jpg")
[Bundesarchiv_DVM_10_Bild-23-63-64,_Panzerschiff_"Admiral_Scheer".jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_DVM_10_Bild-23-63-64,_Panzerschiff_"Admiral_Scheer".jpg "fig:Bundesarchiv_DVM_10_Bild-23-63-64,_Panzerschiff_\"Admiral_Scheer\".jpg")
[Bundesarchiv_Bild_183-2008-0421-500,_Panzerschiff_"Deutschland",_Adolf_Hitler.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-2008-0421-500,_Panzerschiff_"Deutschland",_Adolf_Hitler.jpg "fig:Bundesarchiv_Bild_183-2008-0421-500,_Panzerschiff_\"Deutschland\",_Adolf_Hitler.jpg")

## 诞生

[一战之后](../Page/一战.md "wikilink")，由于[凡尔赛和约对于德国海军的限制](../Page/凡尔赛和约.md "wikilink")，德国处于没有[海防的状态](../Page/海防.md "wikilink")。但是后来的[华盛顿海军条约却给了德国希望](../Page/华盛顿海军条约.md "wikilink")，因为条约规定：用来进行战斗的非航空母舰和战列舰以外的艦艇，[排水量不得超过一万吨](../Page/排水量.md "wikilink")（10160[公吨](../Page/公吨.md "wikilink")）或[主炮](../Page/艦砲.md "wikilink")[口径不得超过八英寸](../Page/口径.md "wikilink")（20.3[公分](../Page/公分.md "wikilink")），而和约允许列國建造排水量不超过一万吨且可以携带11英寸口径舰炮的军舰，于是这种思想初步诞生。

## 理念

装甲舰的核心理念是**“火力要比巡洋舰猛，速度要比战列舰快”**。首艘装甲舰“德意志号”装备了6门11英寸火炮，远比当时重巡洋舰普遍使用的8寸炮的火力要强；而且航速达28节，很容易得就能摆脱战列舰的追击。装甲舰设计时首要任务是[近海防御](../Page/近海防御.md "wikilink")，而到了后来则变成了攻击[商船队的利器](../Page/商船.md "wikilink")。而在Z计划中，“P级”已经作为升级版的德意志级，具备更强的远洋能力，或者说是专门设计在[大西洋上作为破交战的战舰](../Page/大西洋.md "wikilink")，以及作为炮战中辅助战舰。

值得一提的是，其核心理念与[战列巡洋舰颇为相像](../Page/战列巡洋舰.md "wikilink")，但实际上两者还是有很大的区别的。装甲舰一般仅执行破交舰的任务，并不是用于传统海战的武器，而战列巡洋舰的主要任务则是在海战中打击敌方的巡洋舰。

## 战舰列表以及装备

由于装甲舰是在德国受[凡尔赛和约](../Page/凡尔赛和约.md "wikilink")，以及后来的[华盛顿海军条约的限制而产生的舰种](../Page/华盛顿海军条约.md "wikilink")。所以到了后来，[希特勒撕毁](../Page/希特勒.md "wikilink")[凡尔赛和约](../Page/凡尔赛和约.md "wikilink")，拒绝在[伦敦海军条约签字后](../Page/伦敦海军条约.md "wikilink")，德国的装甲舰也就停止了建造和研发，取而代之的是战列舰和其他大型战舰的建造计划。建造完成的装甲舰为3艘，均属德意志级，同级的D和E号也因为上述事件而重新设计成了[沙恩霍斯特级战列巡洋舰](../Page/沙恩霍斯特级战列巡洋舰.md "wikilink")；而在[Z计划中制定的](../Page/Z计划.md "wikilink")“P级”远洋装甲舰，与其说是装甲舰，不如说是小型[战列巡洋舰](../Page/战列巡洋舰.md "wikilink")，最终其计划亦被取消。

### 战舰列表

  - [德意志级装甲舰](../Page/德国级装甲舰.md "wikilink")
      - **德意志**号（Deutschland）后改名为吕佐夫号，因为[希特勒担心一旦该舰沉没会影响士气](../Page/阿道夫·希特勒.md "wikilink")。该舰1931年下水服役，1945年5月为避免被英国俘获而自沉。
      - **希爾上將**号（Admiral
        Scheer）于1934年下水服役，1945年4月年在[基尔被皇家空军击沉](../Page/基尔.md "wikilink")。
      - **斯佩伯爵海军上将**号（Admiral Graf
        Spee）于1936年下水服役，1939年在[拉普拉塔河口海战后被困于](../Page/拉普拉塔河口海战.md "wikilink")[蒙得维的亚](../Page/蒙得维的亚.md "wikilink")，随后被凿沉。
  - [P級裝甲艦](../Page/P級裝甲艦.md "wikilink")
      - P-1至P-12 （全部取消建造）

### 装备

德意志级为了节省吨位，采用了[柴油机作为轮机系统](../Page/柴油机.md "wikilink")，不仅提高了航速，而且增大了[续航力和消耗量](../Page/续航力.md "wikilink")。在“P级”远洋装甲舰的设计中也提到了使用[柴油机](../Page/柴油机.md "wikilink")。而在火炮上面，德意志级也与“P级”相似，主炮使用6座11英寸火炮。但不同的是，德意志号和舍尔号的副炮使用的是单联装5.9英寸火炮，而在施佩号上装备的却是SK/C
33高平两用炮。到了“P级”则出现了装备重型高射炮的方案。

## 对于德国海军的影响

德意志级是德国在“海军假日”中自行建造的少数重型战舰，代表了[德国海军的复兴](../Page/德国海军.md "wikilink")。甚至在当时德国人的眼中，“德国三舰”被视为了德国的标志。德意志级的设计和研发对于后来德国战舰的研制产生了深远的影响，乃至对二战后[苏联海军的炮舰和导弹舰的设计亦产生了相当的影响](../Page/苏联海军.md "wikilink")。

## 相關條目

  - [戰鬥巡洋艦](../Page/戰鬥巡洋艦.md "wikilink")
  - [狮级战列巡洋舰](../Page/狮级战列巡洋舰.md "wikilink")

[Z](../Category/德国海军.md "wikilink")
[Category:德國海軍艦艇](../Category/德國海軍艦艇.md "wikilink")
[Z](../Category/军舰.md "wikilink")