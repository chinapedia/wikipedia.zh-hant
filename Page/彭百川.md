[`Aappointment_of_Nationalist_Government_for_Peng_Baichuan_1929.jpg`](https://zh.wikipedia.org/wiki/File:Aappointment_of_Nationalist_Government_for_Peng_Baichuan_1929.jpg "fig:Aappointment_of_Nationalist_Government_for_Peng_Baichuan_1929.jpg")

**彭百川**（），[中华民国大陆时期教育人物](../Page/中华民国大陆时期.md "wikilink")、政府官员。

## 生平

彭百川出生于江西省永新县，父亲为县衙工作人员，母亲务农。1921年-1928年赴美求学。1921年考取政府官费赴美留学生，每省2名，当年江西省考取的有[吴有训](../Page/吴有训.md "wikilink")、彭百川，国民政府负责一切学费、生活费。先入[斯坦福大学教育系](../Page/斯坦福大学.md "wikilink")，获得硕士学位；后就读[哥伦比亚大学语言学院](../Page/哥伦比亚大学.md "wikilink")，取得博士学位。1928年加入[国立山东大学筹委会](../Page/国立山东大学.md "wikilink")。1929年4月12日，[国民政府主席](../Page/国民政府主席.md "wikilink")[蒋中正与](../Page/蒋中正.md "wikilink")[行政院院长](../Page/行政院院长.md "wikilink")[谭延闿任命为](../Page/谭延闿.md "wikilink")[山东省政府教育厅科长](../Page/山东省政府.md "wikilink")。1937年-
1945年随国民政府迁都重庆，在教育部任秘书长。1945年-1949年随政府返回南京生活，作为[国立中央大学教授](../Page/国立中央大学_\(南京\).md "wikilink")，兼中央大学师范学院附中校长。\[1\]
1949年[中华人民共和国建立后](../Page/中华人民共和国.md "wikilink")，作为原国立中央大学（现南京大学）教授接受改造学习。1953年受不公正待遇，在狱中病逝。

## 个人功绩

### 参与创立山东大学

1928年8月，南京国民政府教育部根据山东省政府教育厅的报告，下令在省立山东大学的基础上筹建国立山东大学。并由[何思源](../Page/何思源.md "wikilink")、魏宗晋、陈雪南、[赵畸](../Page/赵畸.md "wikilink")、王近信、彭百川、[杨亮功](../Page/杨亮功.md "wikilink")、[杨振声](../Page/杨振声_\(教育家\).md "wikilink")、杜光埙、[傅斯年](../Page/傅斯年.md "wikilink")、孙学悟等11人组成国立山东大学筹备委员会，着手筹备工作。在筹备过程中，[蔡元培力主将国立山东大学设在青岛](../Page/蔡元培.md "wikilink")，取得教育部长[蒋梦麟的同意](../Page/蒋梦麟.md "wikilink")。1929年6月，国立山东大学筹备委员会奉令改为国立青岛大学筹备委员会，除接收省立山东大学外，并将私立青岛大学校产收用，筹备国立青岛大学。\[2\]
1930年4月，国民政府任命[杨振声为国立青岛大学校长](../Page/杨振声_\(教育家\).md "wikilink")。9月21日，国立青岛大学举行开学典礼，正式成立。1932年，行政院议决，将国立青岛大学改为国立山东大学，任命赵太侔（[赵畸](../Page/赵畸.md "wikilink")）接任校长。

### 推行贷金制度

  - 1938年后任教育部秘书长（部长为[陈立夫](../Page/陈立夫.md "wikilink")，1938-1944）。参与推行贷金制度。该制度主要用以对青年的救济和训练，这个办法来资助那些来自战区的没有经济来源的学生。据统计，战时由中学至大学毕业，全部靠贷金或公费完成学业的，大约有12万多人，包括像[李政道](../Page/李政道.md "wikilink")、[杨振宁这些后来的知名学者](../Page/杨振宁.md "wikilink")，顺利地念完西南联大，皆得益于贷金制度，这项制度，为中国培养出大批的优秀高级人才起了很大的作用。

### 支持革命

  - 彭百川在教育部与中央大学附中工作期间，多次保护进步学生与[中国共产党党员](../Page/中国共产党.md "wikilink")，资助多位青年赴[延安参加革命](../Page/延安.md "wikilink")。

## 家庭情况

  - 彭百川与杨英民结为夫妻。杨英民出身永新县书香门第，贤良淑德。育三女，均为高级知识分子。

## 参考文献

[Category:中華民國大陸時期中央政府官員](../Category/中華民國大陸時期中央政府官員.md "wikilink")
[Category:中国留美学生](../Category/中国留美学生.md "wikilink")
[Category:山东教育史](../Category/山东教育史.md "wikilink")
[Category:國立中央大學教授](../Category/國立中央大學教授.md "wikilink")
[Category:山东大学](../Category/山东大学.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:永新人](../Category/永新人.md "wikilink")
[百川](../Category/彭姓.md "wikilink")

1.  [彭校长下聘书](http://book.kongfz.com/item_pic_15045_188641006/)
2.