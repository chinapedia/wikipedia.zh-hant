**2Be3**是1990年代法国男子流行音乐组合，由三个成员组成：[Filip
Nikolic](../Page/Filip_Nikolic.md "wikilink")、[Abdel
Kachermi](../Page/Abdel_Kachermi.md "wikilink")、[Frank
Delay](../Page/Frank_Delay.md "wikilink")。该组合没有相对应的官方中文译名，在中国最为人所熟悉的是他们的英文歌曲Even
if。

## 概要

1996年，2Be3发行了首张专辑*Partir un jour*，一炮而红，总共售出80万张。 1999年，2Be3爆发丑闻，主唱[Filip
Nikolic淋浴时展露下体被临时演员偷拍](../Page/Filip_Nikolic.md "wikilink")，随后被杂志[Entrevue披露](../Page/Entrevue.md "wikilink")。
2001年，2Be3的英文专辑*Even if*销量不佳，随后该团体解散。

## 唱片列表

1996年首张专辑《某天离去》 *Partir un jour* (1996):

1.  *Partir un jour*
2.  *Toujours la pour toi*
3.  *Donne*
4.  *L'été bouge*
5.  *2 Be 3*
6.  *Rendez-vous*
7.  *Elle rêvait*
8.  *Des mots pour toi*
9.  *Souviens-toi (L'été dernier)*
10. *Naviguer sans phare*
11. *Partir un jour*

## 团体名称

  - 2Be3曾考虑过其它的名称To be three, To be free等

## 言论

  - Filip曾说，“歌星给歌迷签名的笔很危险，就像一把把匕首”。

## 出典

  - [Stars-Oubliées.com, le panthéon des stars francophones Stars
    oubliées](http://www.stars-oubliees.com/)

<!-- end list -->

  - \[<http://2be3.zikforum.com/>, Forum sur les 2be3\]

[Category:男子演唱團體](../Category/男子演唱團體.md "wikilink")
[Category:法国歌手](../Category/法国歌手.md "wikilink")
[Category:已解散的男子演唱團體](../Category/已解散的男子演唱團體.md "wikilink")
[Category:歐美男子演唱團體](../Category/歐美男子演唱團體.md "wikilink")