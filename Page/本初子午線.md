[Primemeridian.jpg](https://zh.wikipedia.org/wiki/File:Primemeridian.jpg "fig:Primemeridian.jpg")
[赤道、本初子午線及東亞部分都市位置圖.jpg](https://zh.wikipedia.org/wiki/File:赤道、本初子午線及東亞部分都市位置圖.jpg "fig:赤道、本初子午線及東亞部分都市位置圖.jpg")部分都市位置圖（主要文字為[中文地區](../Page/中文.md "wikilink")）\]\]
[Prime_meridian.jpg](https://zh.wikipedia.org/wiki/File:Prime_meridian.jpg "fig:Prime_meridian.jpg")的本初子午線\]\]

**本初子午線**（），即0度[經線](../Page/經度.md "wikilink")，亦稱**[格林威治子午線](../Page/格林威治.md "wikilink")**或**本初經線**，是經過[英國](../Page/英國.md "wikilink")[格林尼治天文台的一條](../Page/格林尼治天文台.md "wikilink")[經線](../Page/經線.md "wikilink")（亦稱**子午線**）。本初子午線的東西兩邊分別定為[東經和](../Page/東經.md "wikilink")[西經](../Page/西經.md "wikilink")，於180度相遇。

不像[緯度起點](../Page/緯度.md "wikilink")（即[赤道](../Page/赤道.md "wikilink")）可由[地球自轉軸決定](../Page/地球自轉.md "wikilink")，理論上任何一條經線均可定為本初子午線，故此在歷史上曾對此線有不同定位。1851年御用天文學家[艾里（Sir
George
Airy）在格林威治天文台設置](../Page/喬治·比德爾·艾里.md "wikilink")[中星儀](../Page/子午儀.md "wikilink")，並以此確定格林威治子午線。因為當時超過三分之二的船隻已使用該線為參考子午線，在1884年於[美國](../Page/美國.md "wikilink")[華盛頓特區舉行的](../Page/華盛頓特區.md "wikilink")上正式定之為經度的起點。來自25個國家共41位代表參與了會議，但[法國代表在投票時棄權](../Page/法國.md "wikilink")，在1911年之前法國仍以[巴黎子午線做為經度起點](../Page/巴黎子午線.md "wikilink")。

從[北極開始](../Page/北極.md "wikilink")，本初子午線經過[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")、[馬利](../Page/馬利.md "wikilink")、[布吉納法索](../Page/布吉納法索.md "wikilink")、[多哥和](../Page/多哥.md "wikilink")[迦納共](../Page/迦納.md "wikilink")8個國家，然後直至[南極](../Page/南極.md "wikilink")。

除了定義經度，格林尼治子午線亦曾作為世界時間標準使用。理論上來說，[格林尼治標準時間的正午是指當太陽橫穿格林尼治子午線時的時間](../Page/格林尼治標準時間.md "wikilink")。然而因為地球自轉速度並不規則，現在的世界標準時間基準已由[協調世界時取代](../Page/協調世界時.md "wikilink")。

## 其它參考經線

格林尼治子午線並非唯一的參考子午線，歷史上和現代亦有其它作為經度起點之用的經線。

  - [WGS84](../Page/WGS84.md "wikilink")
    0度經線，位於格林威治子午線以東102.5米\[1\]。該線所形成的平面穿過地球中心，而非以[天頂為參考點](../Page/天頂.md "wikilink")。因為格林尼治子午線隨著[歐亞大陸板塊移動](../Page/歐亞大陸板塊.md "wikilink")，兩者正以每年約一厘米的速度接近。
  - [哥本哈根](../Page/哥本哈根.md "wikilink")，格林威治以東12°34'32.25"。
  - [耶羅島](../Page/耶羅島.md "wikilink")（El Hierro），格林尼治以西17°39' 46"。
  - [耶路撒冷](../Page/耶路撒冷.md "wikilink")，格林威治以東35°13'
    46"，[聖墓教堂的大圓頂](../Page/聖墓教堂.md "wikilink")。
  - [馬德里](../Page/馬德里.md "wikilink")，格林威治以西3°41' 16.48"
  - [奧斯陸](../Page/奧斯陸.md "wikilink")，格林威治以東10°43' 22.5"
  - [巴黎](../Page/巴黎.md "wikilink")，格林威治以東2°20'
    14"，詳見[巴黎子午線](../Page/巴黎子午線.md "wikilink")。
  - [羅馬](../Page/羅馬.md "wikilink")，格林威治以東12°27' 8.04"
  - [聖彼德堡](../Page/聖彼德堡.md "wikilink")，格林威治以東30°19'
    42.09"，詳見[普爾科沃子午線](../Page/普爾科沃子午線.md "wikilink")。
  - [印度](../Page/印度.md "wikilink")[烏賈因](../Page/烏賈因.md "wikilink")，格林威治以東75°47'
  - [華盛頓特區](../Page/華盛頓特區.md "wikilink")，格林威治以西77°3'
    2.3"，詳見[華盛頓子午線](../Page/華盛頓子午線.md "wikilink")。
  - [北京](../Page/北京.md "wikilink")\[2\]，格林威治以东116°23'。

## 界线所经

从[北极点到](../Page/北极点.md "wikilink")[南极点](../Page/南极点.md "wikilink")，本初子午線依次经过：

<table>
<thead>
<tr class="header">
<th><p>坐標</p></th>
<th><p>國家、領土、海洋</p></th>
<th><p>注釋</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/北冰洋.md" title="wikilink">北冰洋</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/格陵蘭海.md" title="wikilink">格陵蘭海</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/挪威海.md" title="wikilink">挪威海</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/北海_(大西洋).md" title="wikilink">北海</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>從到，途中穿越<a href="../Page/格林威治.md" title="wikilink">格林威治</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/英吉利海峽.md" title="wikilink">英吉利海峽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>從<a href="../Page/濱海維萊爾_(卡爾瓦多斯省).md" title="wikilink">濱海維萊爾到</a><a href="../Page/加瓦爾涅.md" title="wikilink">加瓦爾涅</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>從<a href="../Page/西林德羅峰.md" title="wikilink">西林德羅峰到</a><a href="../Page/卡斯特利翁-德拉普拉納.md" title="wikilink">卡斯特利翁-德拉普拉納</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/地中海.md" title="wikilink">地中海</a></p></td>
<td><p><a href="../Page/瓦倫西亞灣.md" title="wikilink">瓦倫西亞灣</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>從<a href="../Page/埃爾韋爾赫爾.md" title="wikilink">埃爾韋爾赫爾到</a><a href="../Page/卡爾佩.md" title="wikilink">卡爾佩</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/地中海.md" title="wikilink">地中海</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>從到附近的阿爾及利亞－馬利國界。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>約600公尺。</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>約16公里。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>約39公里。</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>從附近的多哥－加納國界到<a href="../Page/特馬.md" title="wikilink">特馬</a>。<br />
穿越<a href="../Page/沃爾特水庫.md" title="wikilink">沃爾特水庫</a>。（）</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大西洋.md" title="wikilink">大西洋</a></p></td>
<td><p>與<a href="../Page/赤道.md" title="wikilink">赤道相交於</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/南冰洋.md" title="wikilink">南冰洋</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/南極洲.md" title="wikilink">南極洲</a></p></td>
<td><p><a href="../Page/南極領地.md" title="wikilink">聲稱擁有的</a><a href="../Page/毛德皇后地.md" title="wikilink">毛德皇后地</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 其它星體

人類亦為[太陽系其它星體定義本初子午線](../Page/太陽系.md "wikilink")。和[地球的一樣](../Page/地球.md "wikilink")，這些本初子午線並沒有特殊的物理意義。

  - [月球的本初子午線是月球對著地球一面的中線](../Page/月球.md "wikilink")。
  - [火星的本初子午線為經過](../Page/火星.md "wikilink")[隕石坑](../Page/隕石坑.md "wikilink")[Airy-0的經線](../Page/Airy-0.md "wikilink")。

## 參見

  - [東經1度線](../Page/東經1度線.md "wikilink")
  - [西經1度線](../Page/西經1度線.md "wikilink")
  - [國際換日線](../Page/國際換日線.md "wikilink")

## 參考網站

<references/>

## 外部連結

  -
  - [1884年國際本初子午線大會會議紀錄](http://www.ucolick.org/~sla/leapsecs/scans-meridian.html)

  - [1880年代各國使用的本初子午線](https://web.archive.org/web/20061029234822/http://wwp.greenwichmeantime.com/info/prime-meridian.htm)

[Category:地球坐標系統](../Category/地球坐標系統.md "wikilink")
[Category:時區](../Category/時區.md "wikilink")
[Category:经线](../Category/经线.md "wikilink")

1.  [History of the Prime Meridian -Past and
    Present](http://gpsinformation.net/main/greenwich.htm)
2.  [1](http://news.eastday.com/epublish/gb/paper148/20010726/class014800009/hwz447203.htm)