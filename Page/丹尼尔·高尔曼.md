**丹尼尔·高尔曼**（Daniel Goleman'，1946年-），美国著名作家兼心理学家。

## 学术成就

  - 情商（EQ）——人类最重要的生存能力

## 主要著作

  - 《EQ》（Emotional Intelligence），1995年
  - 《Focus》，2014年

[G](../Category/1946年出生.md "wikilink")
[G](../Category/美国管理学家.md "wikilink")
[G](../Category/TED演讲人.md "wikilink")
[G](../Category/阿默斯特學院校友.md "wikilink")
[G](../Category/哈佛大學校友.md "wikilink")