**亞洲電影大獎**（，縮寫-{「}--{」}-）是[亞洲](../Page/亞洲.md "wikilink")[電影的一個獎項](../Page/電影.md "wikilink")。

亞洲電影大獎由[亞洲電影大獎學院主辦](../Page/亞洲電影大獎學院.md "wikilink")，創辦於2007年（首七屆由[香港國際電影節協會主辦](../Page/香港國際電影節協會.md "wikilink")），每年舉行一次，為每年[香港國際電影節及](../Page/香港國際電影節.md "wikilink")[香港影視娛樂博覽的其中一项重大節目](../Page/香港影視娛樂博覽.md "wikilink")。主要表揚對亞洲電影業界之傑出成就，並對全亞洲不同類型的電影作品及電影精英予以肯定。香港国际电影节协会每年组织由17位國際殿堂級電影專家組成評審團，配合大會就過往一年所有亞洲區製作之電影建議得出提名名單，再經第二輪投票選出得獎者。頒獎典禮初時以[英語進行](../Page/英語.md "wikilink")，後來改以粵語進行，配合有其他語言（例如：日語、韓語）的即時傳譯。但在電視直播時只播出粵語、英語及普通話，而當嘉賓使用其他語言時會改播粵語即時傳譯。

亞洲電影大獎頒發的獎項主要包括：最佳電影、最佳導演、最佳男演員、最佳女演員、最佳編劇、最佳攝影、最佳美術指導、最佳原創音樂、最佳剪接、最佳視覺效果十項大獎。[2007年亞洲電影大獎頒獎禮於](../Page/2007年亞洲電影大獎.md "wikilink")2007年3月20日假[香港會議展覽中心舉行](../Page/香港會議展覽中心.md "wikilink")，是第一屆亞洲電影大獎。[2008年亞洲電影大獎起](../Page/2008年亞洲電影大獎.md "wikilink")，增設最佳男配角、最佳女配角，故取消最佳男演員、最佳女演員，分為最佳男主角、最佳女主角，至十二項大獎。[2009年亞洲電影大獎](../Page/2009年亞洲電影大獎.md "wikilink")，更增設最佳新演員，以表揚去年出色的影圈新血。

## 歷届主要獎項

  - 紀錄
      - 日本男星**[淺野忠信](../Page/淺野忠信.md "wikilink")**是唯一獲得過**「最佳男主角」**和**「最佳男配角」**的演員，為男演員之冠。
      - 南韓導演**[李滄東](../Page/李滄東.md "wikilink")**是獲獎最多的導演，先後兩度獲得**「最佳導演」**
      - 伊朗導演**[阿斯哈·法哈蒂](../Page/阿斯哈·法哈蒂.md "wikilink")**是獲獎最多的編劇，兩次奪獲得**「最佳編劇」**。
      - 過往只有兩位導演同年獲得**「最佳編劇」**，一位是《生命之詩》的**[李滄東](../Page/李滄東.md "wikilink")**，另一位是執導《[伊朗式分居](../Page/伊朗式分居.md "wikilink")》的**[阿斯哈·法哈蒂](../Page/阿斯哈·法哈蒂.md "wikilink")**。

<table>
<tbody>
<tr class="odd">
<td><p><strong>屆</strong></p></td>
<td><p>|<strong>年度</strong></p></td>
<td><p>|<strong>最佳電影</strong></p></td>
<td><p>|<strong>最佳導演</strong></p></td>
<td><p>|<strong>最佳編劇</strong></p></td>
<td><p>|<strong>最佳男主角</strong></p></td>
<td><p>|<strong>最佳女主角</strong></p></td>
<td><p>|<strong>最佳男配角</strong></p></td>
<td><p>|<strong>最佳女配角</strong></p></td>
<td><p>|<strong>最佳新演員</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第1屆亞洲電影大獎.md" title="wikilink">1</a></p></td>
<td><p>2007</p></td>
<td><p>《<a href="../Page/駭人怪物.md" title="wikilink">駭人怪物</a>》</p></td>
<td><p><a href="../Page/賈樟柯.md" title="wikilink">賈樟柯</a><br />
《<a href="../Page/三峽好人.md" title="wikilink">三峽好人</a>》</p></td>
<td><p><a href="../Page/曼尼．夏希希.md" title="wikilink">曼尼．夏希希</a><br />
《男人做嘢》</p></td>
<td><p><a href="../Page/宋康昊.md" title="wikilink">宋康昊</a><br />
《<a href="../Page/駭人怪物.md" title="wikilink">駭人怪物</a>》</p></td>
<td><p><a href="../Page/中谷美紀.md" title="wikilink">中谷美紀</a><br />
《<a href="../Page/花樣奇緣.md" title="wikilink">花樣奇緣</a>》</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第2屆亞洲電影大獎.md" title="wikilink">2</a></p></td>
<td><p>2008</p></td>
<td><p>《<a href="../Page/密陽_(電影).md" title="wikilink">密陽</a>》</p></td>
<td><p><a href="../Page/李滄東.md" title="wikilink">李滄東</a><br />
《密陽》</p></td>
<td><p><a href="../Page/韋家輝.md" title="wikilink">韋家輝</a>、<a href="../Page/歐健兒.md" title="wikilink">歐健兒</a><br />
《<a href="../Page/神探.md" title="wikilink">神探</a>》</p></td>
<td><p><a href="../Page/梁朝偉.md" title="wikilink">梁朝偉</a><br />
《<a href="../Page/色戒.md" title="wikilink">色戒</a>》</p></td>
<td><p><a href="../Page/全道嬿.md" title="wikilink">全道嬿</a><br />
《密陽》</p></td>
<td><p><a href="../Page/孫紅雷.md" title="wikilink">孫紅雷</a><br />
《鐵木真》</p></td>
<td><p><a href="../Page/陳冲_(演員).md" title="wikilink">陳沖</a><br />
《<a href="../Page/太陽照常升起.md" title="wikilink">太陽照常升起</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第3屆亞洲電影大獎.md" title="wikilink">3</a></p></td>
<td><p>2009</p></td>
<td><p>《<a href="../Page/東京奏鳴曲.md" title="wikilink">東京奏鳴曲</a>》</p></td>
<td><p><a href="../Page/是枝裕和.md" title="wikilink">是枝裕和</a><br />
《<a href="../Page/橫山家之味.md" title="wikilink">橫山家之味</a>》</p></td>
<td><p><a href="../Page/黑澤清.md" title="wikilink">黑澤清</a><br />
《<a href="../Page/東京奏鳴曲.md" title="wikilink">東京奏鳴曲</a>》</p></td>
<td><p><a href="../Page/本木雅弘.md" title="wikilink">本木雅弘</a><br />
《<a href="../Page/禮儀師之奏鳴曲.md" title="wikilink">禮儀師之奏鳴曲</a>》</p></td>
<td><p><a href="../Page/周迅.md" title="wikilink">周迅</a><br />
《<a href="../Page/李米的猜想.md" title="wikilink">李米的猜想</a>》</p></td>
<td><p><a href="../Page/鄭雨盛.md" title="wikilink">鄭雨盛</a><br />
《》</p></td>
<td><p><a href="../Page/珍娜·芭蓮露.md" title="wikilink">珍娜·芭蓮露</a><br />
《<a href="../Page/我們這一家戲院‧‧‧服務周到.md" title="wikilink">我們這一家戲院‧‧‧服務周到</a>》</p></td>
<td><p><a href="../Page/余少群.md" title="wikilink">余少群</a><br />
《<a href="../Page/梅蘭芳_(電影).md" title="wikilink">梅蘭芳</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第4屆亞洲電影大獎.md" title="wikilink">4</a></p></td>
<td><p>2010</p></td>
<td><p>《<a href="../Page/骨肉同謀.md" title="wikilink">骨肉同謀</a>》</p></td>
<td><p><a href="../Page/陸川.md" title="wikilink">陸川</a><br />
《<a href="../Page/南京！南京！.md" title="wikilink">南京！南京！</a>》</p></td>
<td><p><a href="../Page/奉俊昊.md" title="wikilink">奉俊昊</a><br />
《<a href="../Page/骨肉同謀.md" title="wikilink">骨肉同謀</a>》</p></td>
<td><p><a href="../Page/王學圻.md" title="wikilink">王學圻</a><br />
《<a href="../Page/十月圍城.md" title="wikilink">十月圍城</a>》</p></td>
<td><p><a href="../Page/金惠子.md" title="wikilink">金惠子</a><br />
《<a href="../Page/骨肉同謀.md" title="wikilink">骨肉同謀</a>》</p></td>
<td><p><a href="../Page/謝霆鋒.md" title="wikilink">謝霆鋒</a><br />
《<a href="../Page/十月圍城.md" title="wikilink">十月圍城</a>》</p></td>
<td><p><a href="../Page/惠英紅.md" title="wikilink">惠英紅</a><br />
《<a href="../Page/心魔_(2009年電影).md" title="wikilink">心魔</a>》</p></td>
<td><p><a href="../Page/黃明慧.md" title="wikilink">黃明慧</a><br />
《<a href="../Page/心魔_(2009年電影).md" title="wikilink">心魔</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第5屆亞洲電影大獎.md" title="wikilink">5</a></p></td>
<td><p>2011</p></td>
<td><p>《<a href="../Page/波米叔叔的前世今生.md" title="wikilink">波米叔叔的前世今生</a>》</p></td>
<td><p><a href="../Page/李滄東.md" title="wikilink">李滄東</a><br />
《》</p></td>
<td><p><a href="../Page/李滄東.md" title="wikilink">李滄東</a><br />
《》</p></td>
<td><p><a href="../Page/河正宇.md" title="wikilink">河正宇</a><br />
《<a href="../Page/黃海_(電影).md" title="wikilink">黃海 (電影)</a>》</p></td>
<td><p><a href="../Page/徐帆.md" title="wikilink">徐帆</a><br />
《<a href="../Page/唐山大地震_(電影).md" title="wikilink">唐山大地震</a>》</p></td>
<td><p><a href="../Page/洪金寶.md" title="wikilink">洪金寶</a><br />
《<a href="../Page/葉問2.md" title="wikilink">葉問2</a>》</p></td>
<td><p><a href="../Page/尹汝貞.md" title="wikilink">尹汝貞</a><br />
《<a href="../Page/下女_(2010年電影).md" title="wikilink">下女</a>》</p></td>
<td><p><a href="../Page/趙又廷.md" title="wikilink">趙又廷</a><br />
《<a href="../Page/艋舺_(電影).md" title="wikilink">艋舺</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第6屆亞洲電影大獎.md" title="wikilink">6</a></p></td>
<td><p>2012</p></td>
<td><p>《<a href="../Page/伊朗式分居.md" title="wikilink">伊朗式分居</a>》</p></td>
<td><p><a href="../Page/阿斯哈·法哈蒂.md" title="wikilink">阿斯哈·法哈蒂</a><br />
《<a href="../Page/伊朗式分居.md" title="wikilink">伊朗式分居</a>》</p></td>
<td><p><a href="../Page/阿斯哈·法哈蒂.md" title="wikilink">阿斯哈·法哈蒂</a><br />
《<a href="../Page/伊朗式分居.md" title="wikilink">伊朗式分居</a>》</p></td>
<td><p><a href="../Page/Donny_DAMARA.md" title="wikilink">Donny DAMARA</a><br />
《<a href="../Page/爸爸離家企街去.md" title="wikilink">爸爸離家企街去</a>》</p></td>
<td><p><a href="../Page/葉德嫻.md" title="wikilink">葉德嫻</a><br />
《<a href="../Page/桃姐.md" title="wikilink">桃姐</a>》</p></td>
<td><p><a href="../Page/柯宇綸.md" title="wikilink">柯宇綸</a><br />
《<a href="../Page/翻滾吧！阿信.md" title="wikilink">翻滾吧！阿信</a>》</p></td>
<td><p><a href="../Page/Shamaine_BUENCAMINO.md" title="wikilink">Shamaine BUENCAMINO</a><br />
《<a href="../Page/連奴.md" title="wikilink">連奴</a>》</p></td>
<td><p><a href="../Page/倪妮.md" title="wikilink">倪妮</a><br />
《<a href="../Page/金陵十三釵.md" title="wikilink">金陵十三釵</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第7屆亞洲電影大獎.md" title="wikilink">7</a></p></td>
<td><p>2013</p></td>
<td><p>《<a href="../Page/浮城謎事.md" title="wikilink">浮城謎事</a>》</p></td>
<td><p><a href="../Page/北野武.md" title="wikilink">北野武</a><br />
《》</p></td>
<td><p><a href="../Page/梅峰.md" title="wikilink">梅峰</a>、<a href="../Page/於帆.md" title="wikilink">-{于}-帆</a>、<a href="../Page/婁燁.md" title="wikilink">婁燁</a><br />
《<a href="../Page/浮城謎事.md" title="wikilink">浮城謎事</a>》</p></td>
<td><p><a href="../Page/艾迪·加西亞.md" title="wikilink">艾迪·加西亞</a><br />
《<a href="../Page/怪老頭與公主狗.md" title="wikilink">怪老頭與公主狗</a>》</p></td>
<td><p><br />
《》</p></td>
<td><p><a href="../Page/納華薩甸·薛迪奇.md" title="wikilink">納華薩甸·薛迪奇</a><br />
《<a href="../Page/潛藏真相.md" title="wikilink">潛藏真相</a>》</p></td>
<td><p><a href="../Page/渡邊真起子.md" title="wikilink">渡邊真起子</a><br />
《》</p></td>
<td><p><a href="../Page/齊溪.md" title="wikilink">齊溪</a><br />
《<a href="../Page/浮城謎事.md" title="wikilink">浮城謎事</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第8屆亞洲電影大獎.md" title="wikilink">8</a></p></td>
<td><p>2014</p></td>
<td><p>《<a href="../Page/一代宗師.md" title="wikilink">一代宗師</a>》</p></td>
<td><p><a href="../Page/王家衛.md" title="wikilink">王家衛</a><br />
《<a href="../Page/一代宗師.md" title="wikilink">一代宗師</a>》</p></td>
<td><p><br />
《<a href="../Page/美味情書.md" title="wikilink">美味情書</a>》</p></td>
<td><p><a href="../Page/伊凡·卡漢.md" title="wikilink">伊凡·卡漢</a><br />
《<a href="../Page/美味情書.md" title="wikilink">美味情書</a>》</p></td>
<td><p><a href="../Page/章子怡.md" title="wikilink">章子怡</a><br />
《<a href="../Page/一代宗師.md" title="wikilink">一代宗師</a>》</p></td>
<td><p><a href="../Page/黃渤.md" title="wikilink">黃渤</a><br />
《<a href="../Page/无人区_(电影).md" title="wikilink">無人區</a>》</p></td>
<td><p><a href="../Page/楊雁雁.md" title="wikilink">楊雁雁</a><br />
《<a href="../Page/爸媽不在家.md" title="wikilink">爸媽不在家</a>》</p></td>
<td><p><a href="../Page/江疏影.md" title="wikilink">江疏影</a><br />
《<a href="../Page/致我們終將逝去的青春_(電影).md" title="wikilink">致我們終將逝去的青春</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第9屆亞洲電影大獎.md" title="wikilink">9</a></p></td>
<td><p>2015</p></td>
<td><p>《<a href="../Page/推拿_(電影).md" title="wikilink">推拿</a>》</p></td>
<td><p><a href="../Page/許鞍華.md" title="wikilink">許鞍華</a><br />
《<a href="../Page/黃金時代_(2014年電影).md" title="wikilink">黃金時代</a>》</p></td>
<td><p><a href="../Page/刁亦男.md" title="wikilink">刁亦男</a><br />
《<a href="../Page/白日焰火.md" title="wikilink">白日焰火</a>》</p></td>
<td><p><a href="../Page/廖凡.md" title="wikilink">廖凡</a><br />
《<a href="../Page/白日焰火.md" title="wikilink">白日焰火</a>》</p></td>
<td><p><a href="../Page/裴斗娜.md" title="wikilink">裴斗娜</a><br />
《》</p></td>
<td><p><a href="../Page/王志文.md" title="wikilink">王志文</a><br />
《<a href="../Page/黃金時代_(2014年電影).md" title="wikilink">黃金時代</a>》</p></td>
<td><p><a href="../Page/池脇千鶴.md" title="wikilink">池脇千鶴</a><br />
《》</p></td>
<td><p><a href="../Page/張慧雯_(中國演員).md" title="wikilink">張慧雯</a><br />
《<a href="../Page/歸來_(電影).md" title="wikilink">歸來</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第10屆亞洲電影大獎.md" title="wikilink">10</a></p></td>
<td><p>2016</p></td>
<td><p>《<a href="../Page/刺客聶隱娘.md" title="wikilink">刺客聶隱娘</a>》</p></td>
<td><p><a href="../Page/侯孝賢.md" title="wikilink">侯孝賢</a><br />
《<a href="../Page/刺客聶隱娘.md" title="wikilink">刺客聶隱娘</a>》</p></td>
<td><p><a href="../Page/賈樟柯.md" title="wikilink">賈樟柯</a><br />
《<a href="../Page/山河故人.md" title="wikilink">山河故人</a>》</p></td>
<td><p><a href="../Page/李炳憲.md" title="wikilink">李炳憲</a><br />
《<a href="../Page/臥底_(電影).md" title="wikilink">臥底</a>》</p></td>
<td><p><a href="../Page/舒淇.md" title="wikilink">舒淇</a><br />
《<a href="../Page/刺客聶隱娘.md" title="wikilink">刺客聶隱娘</a>》</p></td>
<td><p><a href="../Page/淺野忠信.md" title="wikilink">淺野忠信</a><br />
《<a href="../Page/岸邊之旅#電影.md" title="wikilink">身後戀事</a>》</p></td>
<td><p><a href="../Page/周韵.md" title="wikilink">周韵</a><br />
《<a href="../Page/刺客聶隱娘.md" title="wikilink">刺客聶隱娘</a>》</p></td>
<td><p><a href="../Page/春夏.md" title="wikilink">春夏</a><br />
《<a href="../Page/踏血尋梅.md" title="wikilink">踏血尋梅</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第11屆亞洲電影大獎.md" title="wikilink">11</a></p></td>
<td><p>2017</p></td>
<td><p>《<a href="../Page/我不是潘金莲_(电影).md" title="wikilink">我不是潘金蓮</a>》</p></td>
<td><p><a href="../Page/羅宏鎮.md" title="wikilink">羅泓軫</a><br />
《<a href="../Page/哭聲_(電影).md" title="wikilink">哭聲</a>》</p></td>
<td><p><a href="../Page/阿斯哈·法哈蒂.md" title="wikilink">阿斯哈·法哈蒂</a><br />
《<a href="../Page/新居風暴.md" title="wikilink">新居風暴</a>》</p></td>
<td><p><a href="../Page/淺野忠信.md" title="wikilink">淺野忠信</a><br />
《》</p></td>
<td><p><a href="../Page/范冰冰.md" title="wikilink">范冰冰</a><br />
《<a href="../Page/我不是潘金莲_(电影).md" title="wikilink">我不是潘金蓮</a>》</p></td>
<td><p><a href="../Page/林雪.md" title="wikilink">林雪</a><br />
《<a href="../Page/樹大招風.md" title="wikilink">樹大招風</a>》</p></td>
<td><p><a href="../Page/文素利.md" title="wikilink">文素利</a><br />
《<a href="../Page/下女的誘惑.md" title="wikilink">下女的誘惑</a>》</p></td>
<td><p><a href="../Page/金泰梨.md" title="wikilink">金泰梨</a><br />
《<a href="../Page/下女的誘惑.md" title="wikilink">下女的誘惑</a>》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第12屆亞洲電影大獎.md" title="wikilink">12</a></p></td>
<td><p>2018</p></td>
<td><p>《<a href="../Page/芳華.md" title="wikilink">芳華</a>》</p></td>
<td><p><a href="../Page/石井裕也.md" title="wikilink">石井裕也</a><br />
《》</p></td>
<td><p><a href="../Page/Amit_V_Masurkar.md" title="wikilink">Amit V Masurkar</a>、<a href="../Page/Mayank_Tewari.md" title="wikilink">Mayank Tewari</a><br />
《》</p></td>
<td><p><a href="../Page/古天樂.md" title="wikilink">古天樂</a><br />
《<a href="../Page/殺破狼·貪狼.md" title="wikilink">殺破狼·貪狼</a>》</p></td>
<td><p><a href="../Page/張艾嘉.md" title="wikilink">張艾嘉</a><br />
《<a href="../Page/相愛相親.md" title="wikilink">相愛相親</a>》</p></td>
<td><p><a href="../Page/梁益準.md" title="wikilink">梁益準</a><br />
《》</p></td>
<td><p><a href="../Page/張雨綺.md" title="wikilink">張雨綺</a><br />
《<a href="../Page/妖貓傳.md" title="wikilink">妖貓傳</a>》</p></td>
<td><p><a href="../Page/茱蒂蒙·瓊查容蘇因.md" title="wikilink">茱蒂蒙·瓊查容蘇因</a><br />
《<a href="../Page/模犯生.md" title="wikilink">模犯生</a>》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第13屆亞洲電影大獎.md" title="wikilink">13</a></p></td>
<td><p>2019</p></td>
<td><p>《<a href="../Page/小偷家族.md" title="wikilink">小偷家族</a>》</p></td>
<td><p><a href="../Page/李滄東.md" title="wikilink">李滄東</a><br />
《<a href="../Page/燃燒烈愛.md" title="wikilink">-{zh-tw:燃燒烈愛; zh-hk:燒失樂園; zh-cn:燃燒; }-</a>》</p></td>
<td><p><a href="../Page/賈樟柯.md" title="wikilink">賈樟柯</a><br />
《<a href="../Page/江湖兒女.md" title="wikilink">江湖兒女</a>》</p></td>
<td><p><a href="../Page/役所廣司.md" title="wikilink">役所廣司</a><br />
《<a href="../Page/孤狼之血.md" title="wikilink">孤狼之血</a>》</p></td>
<td><p><a href="../Page/莎瑪‧葉斯利亞莫娃.md" title="wikilink">莎瑪‧葉斯利亞莫娃</a><br />
《<a href="../Page/小傢伙_(2018年電影).md" title="wikilink">小傢伙</a>》</p></td>
<td><p><a href="../Page/章宇.md" title="wikilink">章宇</a><br />
《<a href="../Page/我不是藥神.md" title="wikilink">我不是藥神</a>》</p></td>
<td><p><a href="../Page/惠英紅.md" title="wikilink">惠英紅</a><br />
《<a href="../Page/翠絲.md" title="wikilink">翠絲</a>》</p></td>
<td><p><a href="../Page/黃景瑜.md" title="wikilink">黃景瑜</a><br />
《<a href="../Page/紅海行動.md" title="wikilink">紅海行動</a>》</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歷年表演

  - 2007 - [林憶蓮](../Page/林憶蓮.md "wikilink") - I
    Believe（《[我的野蠻女友](../Page/我的野蠻女友.md "wikilink")》主題曲
    - 韓）、伴隨著你（《[天空之城](../Page/天空之城.md "wikilink")》主題曲 -
    日），追（《[金枝玉葉](../Page/金枝玉葉.md "wikilink")》主題曲 -
    粵）
  - 2008 - [孫燕姿](../Page/孫燕姿.md "wikilink") -
    遇見（《[向左走向右走](../Page/向左走向右走.md "wikilink")》插曲
    - 國）、歡樂今宵（粵）、不了情（《[忘不了](../Page/忘不了.md "wikilink")》主題曲 -
    國）、終身美麗（《[瘦身男女](../Page/瘦身男女.md "wikilink")》主題曲
    - 粵）、Maria（《[醜女大翻身](../Page/醜女大翻身.md "wikilink")》主題曲 - 韓）
  - 2009 - [側田](../Page/側田.md "wikilink") - 命硬、Thirty Days、男人KTV
  - 2010 - [李宇春](../Page/李宇春.md "wikilink") -
    粉末（《[十月围城](../Page/十月围城.md "wikilink")》主题曲 -
    國）
  - 2019 - [金在中](../Page/金在中.md "wikilink") -
    化妝（《[化妝](../Page/化妝.md "wikilink")》中島美雪(1978)）
  - 2019 - [杜德偉](../Page/杜德偉.md "wikilink") -
    如泣如訴（《[金枝玉葉](../Page/金枝玉葉.md "wikilink")》插曲）

## 參看

  - [香港国际电影节协会](../Page/香港国际电影节协会.md "wikilink")
  - [香港国际电影节](../Page/香港国际电影节.md "wikilink")
  - [香港影視娛樂博覽](../Page/香港影視娛樂博覽.md "wikilink")
  - [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

## 参考资料

## 外部連結

  - [亞洲電影大獎官方網站](http://www.afa-academy.com)

  -
[category:香港年度事件](../Page/category:香港年度事件.md "wikilink")

[亞洲電影大獎](../Category/亞洲電影大獎.md "wikilink")
[Category:香港國際電影節](../Category/香港國際電影節.md "wikilink")
[Category:2007年建立的獎項](../Category/2007年建立的獎項.md "wikilink")
[Category:2007年香港建立](../Category/2007年香港建立.md "wikilink")