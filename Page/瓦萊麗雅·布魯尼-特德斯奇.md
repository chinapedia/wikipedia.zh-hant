**瓦萊麗雅·布魯尼·特德斯奇**（英语：Valeria Bruni
Tedeschi，1964年11月16日--），生於[義大利](../Page/義大利.md "wikilink")[都靈](../Page/都靈.md "wikilink")，意大利女[演員](../Page/演員.md "wikilink")，[萨科齐夫人](../Page/萨科齐.md "wikilink")[卡拉·布魯尼同母異父的](../Page/卡拉·布魯尼.md "wikilink")[姊姊](../Page/姊姊.md "wikilink")。

## 電影作品

  - 美好的一年 A Good Year (2006)

  - [慕尼黑](../Page/慕尼黑_\(電影\).md "wikilink") Munich (2005)

  - [愛情賞味期](../Page/愛情賞味期.md "wikilink") 5x2 (2004)

  - 謊言的顏色 The Color of Lies、Au coeur du mensonge (1999)

  - [瑪歌皇后](../Page/瑪歌皇后_\(電影\).md "wikilink") Queen Margot、 La Reine
    Margot (1994)

  -
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:1964年出生](../Category/1964年出生.md "wikilink")
[意大利电影演员](../Category/意大利电影演员.md "wikilink")
[Category:法国电影女演员](../Category/法国电影女演员.md "wikilink")
[Category:意大利导演](../Category/意大利导演.md "wikilink")
[Category:意大利裔法国人](../Category/意大利裔法国人.md "wikilink")
[Category:移民法国的意大利人](../Category/移民法国的意大利人.md "wikilink")