**卡雷尔·波波斯基**（****，）是一名已退役的[捷克职业足球运动员](../Page/捷克.md "wikilink")，技术出众。

2004年8月18日，波波斯基参加了捷克与[希腊的比赛](../Page/希腊国家足球队.md "wikilink")，从而成为第一位代表捷克国家队出场100次的球员。1994年2月23日，波波斯基参加了捷克与[土耳其的比赛](../Page/土耳其国家足球队.md "wikilink")，这是他代表捷克国家队的第一场国际比赛，也是捷克国家队从[捷克斯洛伐克中独立后参加的第一场国际赛事](../Page/捷克斯洛伐克国家足球队.md "wikilink")。

在[1996年欧洲足球锦标赛四分之一决赛中](../Page/1996年欧洲足球锦标赛.md "wikilink")，波波斯基在大禁区线一脚精妙的挑射使他一战成名，帮助球队1-0战胜葡萄牙晋级半决赛。

波波斯基先后效力于[布拉格斯拉维亚](../Page/布拉格斯拉维亚足球俱乐部.md "wikilink")、[曼联](../Page/曼联.md "wikilink")、[本菲卡](../Page/本菲卡.md "wikilink")、[-{zh-hans:拉齐奥;zh-hk:拉素;zh-tw:拉齊奧;}-](../Page/拉素體育會.md "wikilink")、[布拉格斯巴达以及](../Page/布拉格斯巴达足球俱乐部.md "wikilink")[布約維奇足球俱乐部](../Page/布約維奇足球俱乐部.md "wikilink")（Sk
České Budějovice），于2007年宣布退役。

波波斯基代表国家队参加过[1996年](../Page/1996年欧洲足球锦标赛.md "wikilink")、[2000年](../Page/2000年欧洲足球锦标赛.md "wikilink")、[2004年欧洲足球锦标赛和](../Page/2004年欧洲足球锦标赛.md "wikilink")[2006年世界杯](../Page/2006年世界杯.md "wikilink")。退役后，他出任了捷克国家队的技术指导，随队参加了[2008年欧洲足球锦标赛](../Page/2008年欧洲足球锦标赛.md "wikilink")。

[Category:1972年出生](../Category/1972年出生.md "wikilink")
[Category:捷克足球运动员](../Category/捷克足球运动员.md "wikilink")
[Category:布拉格斯拉維亞球員](../Category/布拉格斯拉維亞球員.md "wikilink")
[Category:曼聯球員](../Category/曼聯球員.md "wikilink")
[Category:賓菲加球員](../Category/賓菲加球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:布拉格斯巴達球員](../Category/布拉格斯巴達球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1997年洲際國家盃球員](../Category/1997年洲際國家盃球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")