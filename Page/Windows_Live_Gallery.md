**Windows Live Gallery**
可以下載[微軟小工具提供的額外功能及服務](../Page/微軟小工具.md "wikilink")，例如氣象、新聞等。

## 附加價值

Windows Live Gallery下載的小工具可以在以下地方使用。

  - [Gadgets](../Page/Microsoft_Gadgets.md "wikilink")
      - 在[Windows Vista或](../Page/Windows_Vista.md "wikilink")[Windows
        7中的](../Page/Windows_7.md "wikilink")[Windows資訊看板使用](../Page/Windows資訊看板.md "wikilink")
      - 在[Live.com使用](../Page/Live.com.md "wikilink")
      - 提供[Windows Live
        Spaces](../Page/Windows_Live_Spaces.md "wikilink")[部落格使用](../Page/部落格.md "wikilink")

## 方式

Windows Live Gallery可以提供所有擁有[Windows Live
ID的人上傳自己製作的小工具](../Page/Windows_Live_ID.md "wikilink")，或是下載小工具，下載時會顯示是提供給何處使用，目前大多都提供給[Live.com作為首頁小工具](../Page/Live.com.md "wikilink")。

## 外部链接

  - [Windows Live Gallery](http://gallery.live.com/)

[Category:Windows Live](../Category/Windows_Live.md "wikilink")