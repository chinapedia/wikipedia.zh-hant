**羅林斯县**（**Rawlins County,
Kansas**，簡稱**RA**）是[美國](../Page/美國.md "wikilink")[堪薩斯州西北部的一個縣](../Page/堪薩斯州.md "wikilink")，北鄰[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")。面積2,771平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口2,672人。縣治[阿特伍德](../Page/阿特伍德_\(堪薩斯州\).md "wikilink")（Atwood）。

成立於1873年3月6日，1881年3月11日縣政府成立。縣名紀念[戰爭部長](../Page/美國戰爭部長.md "wikilink")[約翰·A·羅林斯](../Page/約翰·A·羅林斯.md "wikilink")
（John Aaron Rawlins）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[R](../Category/堪薩斯州行政區劃.md "wikilink")
[羅林斯縣_(堪薩斯州)](../Category/羅林斯縣_\(堪薩斯州\).md "wikilink")
[Category:1873年堪薩斯州建立](../Category/1873年堪薩斯州建立.md "wikilink")
[Category:1873年建立的聚居地](../Category/1873年建立的聚居地.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.