是由[光榮的ω](../Page/光榮.md "wikilink")-force開發，由光榮發行的[動作遊戲](../Page/動作遊戲.md "wikilink")。遊戲[PlayStation
2版在](../Page/PlayStation_2.md "wikilink")2007年3月21日\[1\]在日本正式發行，而美版則在2007年9月18日\[2\]發行，歐洲在2007年9月21日\[3\]發行，澳洲在2007年9月27日\[4\]發行，以及紐西蘭在2007年9月28日\[5\]發行。此外遊戲也在之後發行了[Xbox
360版](../Page/Xbox_360.md "wikilink")\[6\]和[PSP版](../Page/PlayStation_Portable.md "wikilink")\[7\]，并且在2008年3月20日發行[PC版](../Page/PC.md "wikilink")\[8\]。

在2008年4月3日，[光榮正式在日本推出續集](../Page/光榮.md "wikilink")[蛇魔再臨](../Page/無雙OROCHI_蛇魔再臨.md "wikilink")，歐版和美版則在2008年9月正式發行。

此遊戲的第三作[無雙OROCHI
Z於](../Page/無雙OROCHI_Z.md "wikilink")2009年3月12日在日本於[PS3平臺正式發行](../Page/PlayStation_3.md "wikilink")。光榮宣布美版和歐版將不再發行。

2018年，《[无双大蛇3](../Page/无双大蛇3.md "wikilink")》发行。

## 特色

該作的特色將會是[真·三國無雙系列及](../Page/真·三國無雙系列.md "wikilink")[戰國無雙系列角色首次正式在無雙系列融合](../Page/戰國無雙系列.md "wikilink")，此外，曾在《戰國無雙2》沒有登場的角色亦會在該作品登場。本作可以允許在同一關卡中操控3個武將，只要在戰鬥中途按一個按鈕就可以轉換人物。

本作的關卡是沿用了《真·三國無雙4》及《戰國無雙2》的56個關卡地圖，加上四傳共用的最終章原創地圖「古志城」，共60關。當中的戰鬥情況和原先的會有所分別，多數關卡的參戰人物已大大改變。有一些甚至會稍微修改地形。

## 故事

故事講述魔王[遠呂智嘗試尋找世界真正的強者](../Page/八岐大蛇.md "wikilink")，於是將時空扭曲，將[中國](../Page/中國.md "wikilink")[三國時代及](../Page/三國時代.md "wikilink")[日本](../Page/日本.md "wikilink")[戰國時代的人物困在一個異世界共同登場](../Page/战国时代_\(日本\).md "wikilink")。而各個勢力有不同的難處，有些暫時加入遠呂智軍，希望尋找失蹤的主君。有些則直接與遠呂智軍對抗，稱為反亂軍。

### 蜀

蜀軍與遠呂智軍交戰潰敗，多名重要人物失蹤，軍師[諸葛亮和](../Page/諸葛亮.md "wikilink")[劉備義兄弟](../Page/劉備.md "wikilink")[關羽和](../Page/關羽.md "wikilink")[張飛更加入遠呂智軍](../Page/張飛.md "wikilink")。被監禁在[上田城的](../Page/上田城.md "wikilink")[趙雲得到神仙](../Page/趙雲.md "wikilink")[左慈幫助下開始逃獄](../Page/左慈.md "wikilink")，與同伴[星彩及](../Page/星彩.md "wikilink")[島津義弘一起尋找失散的蜀國主君](../Page/島津義弘.md "wikilink")[劉備](../Page/劉備.md "wikilink")，在與遠呂智軍交戰時，遇到了[真田幸村](../Page/真田幸村.md "wikilink")，最後兩人合力對抗遠呂智。雖然蜀軍的主力已經潰敗，但是有[黃忠等拒降武將駐守小城](../Page/黃忠.md "wikilink")，游擊交戰。

### 魏

[曹操在與遠呂智交戰時失蹤](../Page/曹操.md "wikilink")，部分武將也因為戰敗而紛紛消失蹤跡。其子[曹丕因曹操失蹤暫時繼承主君一職](../Page/曹丕.md "wikilink")，在無力與遠呂智抗衡的的情況下，被迫與部下[張遼](../Page/張遼.md "wikilink")、[徐晃一同加入遠呂智軍](../Page/徐晃.md "wikilink")。最後與同被遠呂智收為部下的[石田三成聯合起兵反抗遠呂智](../Page/石田三成.md "wikilink")。

### 吳

吳軍在與遠呂智交戰時多名武將被捕獲。當中[孫堅更被遠呂智捕獲](../Page/孫堅.md "wikilink")，長子[孫策暫時加入遠呂智軍](../Page/孫策.md "wikilink")，監視行動。遠呂智答應：只要完成一樣任務就釋放一名戰俘，有同樣遭遇的[德川家康也在這個章節中與孫策共同戰鬥](../Page/德川家康.md "wikilink")。

### 戰國

[織田信長](../Page/織田信長.md "wikilink")、[武田信玄和](../Page/武田信玄.md "wikilink")[上杉谦信三位战国](../Page/上杉谦信.md "wikilink")[大名在遠呂智的猛烈攻擊下保持著與原本差不多的戰力](../Page/大名.md "wikilink")，雖然多位武將因為戰敗已經多數投降、少數失去蹤跡，但還是舉起反遠呂智軍隊的旗幟，開始在各地與遠呂智軍交戰。

## 系統

本作是首次引用三人小隊系統，在戰場上按一個按鈕後，可以在戰場上更換角色，而被更換的角色，其生命值及無雙值會慢慢地回復。每名角色在每一個關卡滿足一個條件後，可以學習新的特殊技能（已學習的話會升級）。

本作也是無雙系列首次引入武器合成系統，玩家可以選擇武將的不同武器合成，以增強裝備能力。

## 登場人物

所有在[真·三國無雙系列及](../Page/真·三國無雙系列.md "wikilink")[戰國無雙系列的人物悉數登場](../Page/戰國無雙系列.md "wikilink")，詳細請參看相關條目。此外，[商朝歷史人人物](../Page/商朝.md "wikilink")[妲己以及原創人物遠呂智會以敵軍大將身份登場](../Page/妲己.md "wikilink")（滿足指定條件後可以被玩家使用）。

**蜀國**

| 人物名稱                             | 聲優                                   | 武器（Lv4武器）                                | 道具                                     |
| -------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- |
| [趙雲](../Page/趙雲.md "wikilink")   | [小野坂昌也](../Page/小野坂昌也.md "wikilink") | [直槍](../Page/槍.md "wikilink")（豪龍膽）       | 孔明之錦囊                                  |
| [關羽](../Page/關羽.md "wikilink")   | [增谷康紀](../Page/增谷康紀.md "wikilink")   | [偃月刀](../Page/偃月刀.md "wikilink")（黃龍偃月刀）  | [春秋左氏傳](../Page/春秋左氏傳.md "wikilink")   |
| [張飛](../Page/張飛.md "wikilink")   | [掛川裕彥](../Page/掛川裕彥.md "wikilink")   | [鐵矛](../Page/蛇矛.md "wikilink")（破軍蛇矛）     | 桃園之酒                                   |
| [諸葛亮](../Page/諸葛亮.md "wikilink") | [小野坂昌也](../Page/小野坂昌也.md "wikilink") | [羽扇](../Page/羽扇.md "wikilink")（朱雀羽扇）     | [出師表](../Page/出師表.md "wikilink")       |
| [劉備](../Page/劉備.md "wikilink")   | [遠藤守哉](../Page/遠藤守哉.md "wikilink")   | [尖劍](../Page/劍.md "wikilink")（真黃龍劍）      | 敬母之草鞋                                  |
| [馬超](../Page/馬超.md "wikilink")   | [服卷浩司](../Page/服卷浩司.md "wikilink")   | [直槍](../Page/槍.md "wikilink")（龍騎尖）       | 西涼錦之手綱                                 |
| [黃忠](../Page/黃忠.md "wikilink")   | [川津泰彥](../Page/川津泰彥.md "wikilink")   | [盤刀](../Page/刀.md "wikilink")（破邪旋風斬）     | 黄忠弓                                    |
| [姜維](../Page/姜維.md "wikilink")   | [菅沼久義](../Page/菅沼久義.md "wikilink")   | [三尖槍](../Page/三尖兩刃刀.md "wikilink")（昂龍顎閃） | [兵法二十四編](../Page/兵法二十四編.md "wikilink") |
| [魏延](../Page/魏延.md "wikilink")   | [增谷康紀](../Page/增谷康紀.md "wikilink")   | [長柄雙刀](../Page/大刀.md "wikilink")（雙極滅星）   | 反骨相之面                                  |
| [龐統](../Page/龐統.md "wikilink")   | [河內孝博](../Page/河內孝博.md "wikilink")   | [幻杖](../Page/杖.md "wikilink")（豪風神杖）      | 水鏡絹布                                   |
| [月英](../Page/黃夫人.md "wikilink")  | [笠原留美](../Page/笠原留美.md "wikilink")   | [戰戈](../Page/戈.md "wikilink")（湖底蒼月）      | 絡繰人形                                   |
| [關平](../Page/關平.md "wikilink")   | [中尾良平](../Page/中尾良平.md "wikilink")   | [斬馬刀](../Page/斬馬刀.md "wikilink")（神龍昇天刀）  | 雲長鉢巻                                   |
| [星彩](../Page/敬哀皇后.md "wikilink") | [野田順子](../Page/野田順子.md "wikilink")   | [叉突矛](../Page/矛.md "wikilink")（煌天）       | 翼徳之首飾                                  |

**魏國**

| 人物名稱                             | 聲優                                   | 武器（Lv4武器）                              | 道具                                 |
| -------------------------------- | ------------------------------------ | -------------------------------------- | ---------------------------------- |
| [夏侯惇](../Page/夏侯惇.md "wikilink") | [中井和哉](../Page/中井和哉.md "wikilink")   | [朴刀](../Page/朴刀.md "wikilink")（滅麒麟牙）   | 不臣之眼帶                              |
| [典韋](../Page/典韋.md "wikilink")   | [中井和哉](../Page/中井和哉.md "wikilink")   | [戰斧](../Page/斧.md "wikilink")（真極牛頭）    | 牙門之旗                               |
| [許褚](../Page/許褚.md "wikilink")   | [吉水孝宏](../Page/吉水孝宏.md "wikilink")   | [碎棒](../Page/棒.md "wikilink")（蚩尤瀑布碎）   | 牛尾之首布                              |
| [曹操](../Page/曹操.md "wikilink")   | [岸野幸正](../Page/岸野幸正.md "wikilink")   | [將劍](../Page/劍.md "wikilink")（倚天之奸劍）   | [孟德新書](../Page/孟德新書.md "wikilink") |
| [夏侯淵](../Page/夏侯淵.md "wikilink") | [德山靖彥](../Page/德山靖彥.md "wikilink")   | [碎棍](../Page/鞭.md "wikilink")（金剛九天斷）   | 夏侯弓                                |
| [張遼](../Page/張遼.md "wikilink")   | [田中大文](../Page/田中大文.md "wikilink")   | [鉤鐮刀](../Page/大刀.md "wikilink")（黃龍鉤鐮刀） | 白馬篇                                |
| [司馬懿](../Page/司馬懿.md "wikilink") | [瀧下毅](../Page/瀧下毅.md "wikilink")     | [燕扇](../Page/羽扇.md "wikilink")（窮奇羽扇）   | [司馬法](../Page/司馬法.md "wikilink")   |
| [徐晃](../Page/徐晃.md "wikilink")   | [山本圭一郎](../Page/山本圭一郎.md "wikilink") | [大斧](../Page/大斧.md "wikilink")（白虎牙斷）   | 武技白布                               |
| [張郃](../Page/張郃.md "wikilink")   | [幸野善之](../Page/幸野善之.md "wikilink")   | 鐵鉤（[朱雀虹](../Page/朱雀.md "wikilink")）    | 戰美之薔薇                              |
| [甄姬](../Page/甄姬.md "wikilink")   | [住友優子](../Page/住友優子.md "wikilink")   | [鐵笛](../Page/笛.md "wikilink")（月妖日狂）    | 妖姬之古琴                              |
| [曹仁](../Page/曹仁.md "wikilink")   | [江川央生](../Page/江川央生.md "wikilink")   | [牙壁](../Page/複合盾.md "wikilink")（鳳嘴凰翼）  | [尉繚子](../Page/尉繚子.md "wikilink")   |
| [曹丕](../Page/曹丕.md "wikilink")   | [神奈延年](../Page/神奈延年.md "wikilink")   | 雙刃[劍](../Page/劍.md "wikilink")（無奏）     | [典論](../Page/典論.md "wikilink")     |
| [龐德](../Page/龐德.md "wikilink")   | [森田成一](../Page/森田成一.md "wikilink")   | 雙[戟](../Page/戟.md "wikilink")（驚天動地）    | 決死之棺桶                              |

**吳國**

| 人物名稱                             | 聲優                                   | 武器（Lv4武器）                              | 道具                                 |
| -------------------------------- | ------------------------------------ | -------------------------------------- | ---------------------------------- |
| [周瑜](../Page/周瑜.md "wikilink")   | [吉水孝宏](../Page/吉水孝宏.md "wikilink")   | [鐵劍](../Page/劍.md "wikilink")（古錠刀真打）   | 天下二分書                              |
| [陸遜](../Page/陸遜.md "wikilink")   | [野島健兒](../Page/野島健兒.md "wikilink")   | 雙[劍](../Page/劍.md "wikilink")（閃飛燕）     | [六韜](../Page/六韜.md "wikilink")     |
| [太史慈](../Page/太史慈.md "wikilink") | [掛川裕彥](../Page/掛川裕彥.md "wikilink")   | [雙鞭](../Page/雙鞭.md "wikilink")（虎撲毆狼改）  | 六十日誓書                              |
| [孫尚香](../Page/孫尚香.md "wikilink") | [宇和川惠美](../Page/宇和川惠美.md "wikilink") | 夏圈（日月[乾坤圈](../Page/乾坤圈.md "wikilink")） | 玄德腕輪                               |
| [孫堅](../Page/孫堅.md "wikilink")   | [德山靖彥](../Page/德山靖彥.md "wikilink")   | [牙劍](../Page/劍.md "wikilink")（真天狼劍）    | [玉璽](../Page/玉璽.md "wikilink")     |
| [孫權](../Page/孫權.md "wikilink")   | [菅沼久義](../Page/菅沼久義.md "wikilink")   | 積刃[劍](../Page/劍.md "wikilink")（白炎皇狼劍）  | 孫氏之兵法                              |
| [呂蒙](../Page/呂蒙.md "wikilink")   | [堀之紀](../Page/堀之紀.md "wikilink")     | 斷[戟](../Page/戟.md "wikilink")（白虎顎）     | [戰國策](../Page/戰國策.md "wikilink")   |
| [甘寧](../Page/甘寧.md "wikilink")   | [三浦祥郎](../Page/三浦祥郎.md "wikilink")   | [甲刀](../Page/刀.md "wikilink")（霸海）      | 鵞鳥之羽                               |
| [黃蓋](../Page/黃蓋.md "wikilink")   | [稻田徹](../Page/稻田徹.md "wikilink")     | [鐵鞭](../Page/鐵鞭.md "wikilink")（斷海鞭）    | 孫吳之酒                               |
| [孫策](../Page/孫策.md "wikilink")   | [河內孝博](../Page/河內孝博.md "wikilink")   | [旋棍](../Page/拐.md "wikilink")（霸王）      | [孫臏兵法](../Page/孫臏兵法.md "wikilink") |
| [大喬](../Page/大喬.md "wikilink")   | [島方純子](../Page/島方純子.md "wikilink")   | 櫻[扇](../Page/扇.md "wikilink")（喬美麗）     | 月光花之髮飾                             |
| [小喬](../Page/小喬.md "wikilink")   | [島方純子](../Page/島方純子.md "wikilink")   | 桃[扇](../Page/扇.md "wikilink")（喬佳麗）     | 陽光花之髮飾                             |
| [周泰](../Page/周泰.md "wikilink")   | [石川英郎](../Page/石川英郎.md "wikilink")   | [弧刀](../Page/日本刀.md "wikilink")（宵）     | 青傘蓋                                |
| [凌統](../Page/凌統.md "wikilink")   | [松野太紀](../Page/松野太紀.md "wikilink")   | [雙節棍](../Page/雙節棍.md "wikilink")（怒濤）   | 仲謀上衣                               |

**他國**

| 人物名稱                              | 聲優                                   | 武器（Lv4武器）                                                           | 道具                                 |
| --------------------------------- | ------------------------------------ | ------------------------------------------------------------------- | ---------------------------------- |
| [貂蟬](../Page/貂蟬.md "wikilink")    | [小松里歌](../Page/小松里歌.md "wikilink")   | 雙[鎚](../Page/鎚.md "wikilink")（金麗玉鎚）                                 | 奉先簪                                |
| [呂布](../Page/呂布.md "wikilink")    | [稻田徹](../Page/稻田徹.md "wikilink")     | [鐵戟](../Page/方天戟.md "wikilink")（無雙[方天戟](../Page/方天戟.md "wikilink")） | 飛將鎧                                |
| [董卓](../Page/董卓.md "wikilink")    | [堀之紀](../Page/堀之紀.md "wikilink")     | 獄[刀](../Page/刀.md "wikilink")（[阿修羅](../Page/阿修羅.md "wikilink")）     | [九錫](../Page/九錫.md "wikilink")     |
| [袁紹](../Page/袁紹.md "wikilink")    | [龍谷修武](../Page/龍谷修武.md "wikilink")   | 寶[劍](../Page/劍.md "wikilink")（真霸道劍）                                 | 霸道鎧                                |
| [張角](../Page/張角.md "wikilink")    | [川津泰彥](../Page/川津泰彥.md "wikilink")   | 妖杖（轟火神杖）                                                            | [太平要術](../Page/太平要術.md "wikilink") |
| [孟獲](../Page/孟獲.md "wikilink")    | [幸野善之](../Page/幸野善之.md "wikilink")   | 蠻拳（百獸王）                                                             | 南中王之王冠                             |
| [祝融](../Page/祝融夫人.md "wikilink")  | [米本千珠](../Page/米本千珠.md "wikilink")   | [投弧刃](../Page/飛去來器.md "wikilink")（業火）                               | 火神之羽                               |
| [左慈](../Page/左慈.md "wikilink")    | [佐藤正治](../Page/佐藤正治.md "wikilink")   | 呪符（冥天照符）                                                            | 遁甲天書                               |
| [遠呂智](../Page/八岐大蛇.md "wikilink") | [置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink") | 鐮刀（無間）                                                              | 神變鬼毒酒                              |
| [妲己](../Page/妲己.md "wikilink")    | [金月真美](../Page/金月真美.md "wikilink")   | 妖玉（崩國）                                                              | [殺生石](../Page/殺生石.md "wikilink")   |

**戰國1**

| 人物名稱                                   | 聲優                                      | 武器（Lv4武器）                                                          | 道具                                 |
| -------------------------------------- | --------------------------------------- | ------------------------------------------------------------------ | ---------------------------------- |
| [真田幸村](../Page/真田幸村.md "wikilink")     | [草尾毅](../Page/草尾毅.md "wikilink")        | 十文字槍（[炎槍素戔鳴](../Page/炎槍素戔鳴.md "wikilink")）                         | 六文錢                                |
| [前田慶次](../Page/前田慶次.md "wikilink")     | [上田祐司](../Page/上田祐司.md "wikilink")      | [二又矛](../Page/二又矛.md "wikilink")（天之瓊鉾）                             | 慶次道中日記                             |
| [織田信長](../Page/織田信長.md "wikilink")     | [小杉十郎太](../Page/小杉十郎太.md "wikilink")    | [鬼切](../Page/鬼切.md "wikilink")（[蛇之麁正](../Page/天羽羽斬.md "wikilink")） | [敦盛繪卷](../Page/敦盛繪卷.md "wikilink") |
| [明智光秀](../Page/明智光秀.md "wikilink")     | [綠川光](../Page/綠川光.md "wikilink")        | 業物（靈劍[布都御魂](../Page/布都御魂.md "wikilink")）                           | 愛宕百韻                               |
| [石川五右衛門](../Page/石川五右衛門.md "wikilink") | [江川央生](../Page/江川央生.md "wikilink")      | [石割](../Page/石割.md "wikilink")（魁伐折羅）                               | [千鳥香爐](../Page/千鳥香爐.md "wikilink") |
| [上杉謙信](../Page/上杉謙信.md "wikilink")     | [中田讓治](../Page/中田讓治.md "wikilink")      | [七握刀](../Page/七握刀.md "wikilink")（[天叢雲](../Page/天叢雲.md "wikilink")） | 馬上杯                                |
| [阿市](../Page/阿市.md "wikilink")         | [前田愛](../Page/前田愛_\(聲優\).md "wikilink") | [劍玉.梅](../Page/劍玉.梅.md "wikilink")（木花開耶．菊）                         | 小豆袋                                |
| [阿國](../Page/阿國.md "wikilink")         | [山崎和佳奈](../Page/山崎和佳奈.md "wikilink")    | 番[傘](../Page/傘.md "wikilink")（日向天鈿女）                               | 勸進帳                                |
| [くのいち](../Page/女忍者.md "wikilink")      | [永島由子](../Page/永島由子.md "wikilink")      | [穴苦無](../Page/穴苦無.md "wikilink")（絕不知火）                             | 黃熟香                                |
| [雜賀孫市](../Page/雜賀孫市.md "wikilink")     | [磯部弘](../Page/磯部弘.md "wikilink")        | 種子島（焰獄火具土）                                                         | 雜賀胴衣                               |
| [武田信玄](../Page/武田信玄.md "wikilink")     | [鄉里大輔](../Page/鄉里大輔.md "wikilink")      | [將配](../Page/將配.md "wikilink")（天孫降臨）                               | 武田軍旗                               |
| [伊達政宗](../Page/伊達政宗.md "wikilink")     | [檜山修之](../Page/檜山修之.md "wikilink")      | [陣太刀](../Page/陣太刀.md "wikilink")（大覇狩）                              | 獨眼龍之煙管                             |
| [濃姬](../Page/濃姬.md "wikilink")         | [鈴木麻里子](../Page/鈴木麻里子.md "wikilink")    | [蜘蛛](../Page/蜘蛛.md "wikilink")（蛭虸）                                 | 道三之小刀                              |
| [服部半藏](../Page/服部半藏.md "wikilink")     | [黑田崇矢](../Page/黑田崇矢.md "wikilink")      | [鎖鎌](../Page/鎖鎌.md "wikilink")（闇牙黃泉津）                              | [伊賀流忍術書](../Page/伊賀.md "wikilink") |
| [森蘭丸](../Page/森蘭丸.md "wikilink")       | [進藤尚美](../Page/進藤尚美.md "wikilink")      | [野太刀](../Page/野太刀.md "wikilink")（神劍カムド）                            | 善隣國寶記                              |

**戰國2**

| 人物名稱                                 | 聲優                                         | 武器（Lv4武器）                                | 道具                               |
| ------------------------------------ | ------------------------------------------ | ---------------------------------------- | -------------------------------- |
| [豐臣秀吉](../Page/豐臣秀吉.md "wikilink")   | [石川英郎](../Page/石川英郎.md "wikilink")         | [三節棍](../Page/三節棍.md "wikilink")（三貴宇津皇子） | 信長之草履                            |
| [今川義元](../Page/今川義元.md "wikilink")   | [河內孝博](../Page/河內孝博.md "wikilink")         | [蹴鞠](../Page/蹴鞠.md "wikilink")（意富加牟豆美）   | 枕草子                              |
| [本多忠勝](../Page/本多忠勝.md "wikilink")   | [大塚明夫](../Page/大塚明夫.md "wikilink")         | [豪槍](../Page/豪槍.md "wikilink")（闘尖荒覇吐）    | 鹿角脅立兜                            |
| [稻姬](../Page/本多小松.md "wikilink")     | [大本真基子](../Page/大本真基子.md "wikilink")       | [長弓](../Page/長弓.md "wikilink")（天之麻迦古弓）   | 白鉢巻                              |
| [德川家康](../Page/德川家康.md "wikilink")   | [中田讓治](../Page/中田讓治.md "wikilink")         | 筒槍（煌刃獲加武）                                | 貞觀政要                             |
| [石田三成](../Page/石田三成.md "wikilink")   | [竹本英史](../Page/竹本英史.md "wikilink")         | [義扇](../Page/義扇.md "wikilink")（志那都神扇）    | 三獻茶                              |
| [淺井長政](../Page/淺井長政.md "wikilink")   | [神谷浩史](../Page/神谷浩史.md "wikilink")         | [椎矛](../Page/椎矛.md "wikilink")（倭王八千戟）    | 小谷之賞牌                            |
| [島左近](../Page/島左近.md "wikilink")     | [山田真一](../Page/山田真一_\(配音員\).md "wikilink") | 戰場刀（猛壬那刀）                                | 影目錄                              |
| [島津義弘](../Page/島津義弘.md "wikilink")   | [江川央生](../Page/江川央生.md "wikilink")         | [丸太槌](../Page/丸太槌.md "wikilink")（大槌伊武岐）  | 花札豬鹿蝶                            |
| [立花誾千代](../Page/立花誾千代.md "wikilink") | [進藤尚美](../Page/進藤尚美.md "wikilink")         | [稻妻刀](../Page/稻妻刀.md "wikilink")（雷鋼須世理）  | 道雪具足                             |
| [直江兼續](../Page/直江兼續.md "wikilink")   | [高塚正也](../Page/高塚正也.md "wikilink")         | [寶劍](../Page/寶劍.md "wikilink")（神直毘御劍）    | 直江狀                              |
| [寧寧](../Page/寧寧.md "wikilink")       | [山崎和佳奈](../Page/山崎和佳奈.md "wikilink")       | [飛刀](../Page/飛刀.md "wikilink")（豐玉翔小太刀）   | 太閤記                              |
| [風魔小太郎](../Page/風魔小太郎.md "wikilink") | [檜山修之](../Page/檜山修之.md "wikilink")         | [鐵籠手](../Page/鐵籠手.md "wikilink")（闇御津破）   | 風魔血判狀                            |
| [宮本武藏](../Page/宮本武藏.md "wikilink")   | [金子英彥](../Page/金子英彥.md "wikilink")         | [雙劍](../Page/雙劍.md "wikilink")（石裂岩刀劍）    | [五輪書](../Page/五輪書.md "wikilink") |
|                                      |                                            |                                          |                                  |
|                                      |                                            |                                          |                                  |

## PSP版

無雙蛇魔移植PSP版後，仍維持大部份遊戲內容。無雙系列首次在攜帶機上以一個戰場一個畫面進行遊戲。不過大部份動畫改以cut-in事件進行。聲音部份保留cut-in及戰鬥時擊退敵武將的聲音。此外，PSP版亦修正了PS2版的BUG，呂布個人物品的取得難度亦有所減低。

## 續作

## 參考資料

## 外部連結

  - [《無雙OROCHI 蛇魔》官方網站](http://www.gamecity.ne.jp/orochi/)

  - [《無雙OROCHI 蛇魔》官方網站](http://www.gamecity.com.tw/orochi/)

[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:无双大蛇系列](../Category/无双大蛇系列.md "wikilink")
[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:Xbox 360遊戲](../Category/Xbox_360遊戲.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.