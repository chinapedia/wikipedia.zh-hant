**2007年NBA全明星赛**在2007年2月18日於[内华达州](../Page/内华达州.md "wikilink")[拉斯维加斯市](../Page/拉斯维加斯.md "wikilink")[托马斯和马克中心球馆舉行](../Page/托马斯和马克中心.md "wikilink")，这是史上首次在没有NBA球队的城市举办NBA全明星赛。西區全明星和東區全明星展開了一場精彩的賽事，雙方球員都有出色的表現，雙方得分都打破了歷年的紀錄，最後西區全明星隊以153比132擊敗東區全明星隊。[科比·布莱恩特憑著他出色的表現](../Page/科比·布莱恩特.md "wikilink")（31分、5個籃板、6次助攻）而成為本次全明星賽的最有價值球員。

## 球隊陣容

### 教練

NBA全明星隊的教練是由該賽區表現最佳的球隊的教練擔任。根據規定，一個教練不能連續兩年執教NBA全明星隊。雖然[達拉斯小牛在該賽區領先](../Page/達拉斯小牛.md "wikilink")，但是[-{zh-hans:埃弗里·约翰逊;
zh-hant:阿弗里·詹森;}-不能擔任](../Page/阿弗里·詹森.md "wikilink")，他的位置就由[鳳凰城太陽的](../Page/鳳凰城太陽.md "wikilink")[迈克·德安东尼補上](../Page/迈克·德安东尼.md "wikilink")。另外，東區的教練是[华盛顿-{zh-hans:奇才;zh-hk:巫師;zh-tw:巫師;}-的](../Page/華盛頓巫師.md "wikilink")[埃迪·乔丹](../Page/埃迪·乔丹.md "wikilink")。

### 球員

本次NBA全明星赛是歷年來最多球員因傷不能上陣，包括東區的[贾森·-{zh-hans:基德;
zh-hk:傑特;}-](../Page/贾森·基德.md "wikilink")、西區的[-{zh-hans:卡罗斯·布泽尔;
zh-hk:卡路士·布沙;}-](../Page/卡罗斯·布泽尔.md "wikilink")、[史蒂夫·纳什](../Page/史蒂夫·纳什.md "wikilink")、[-{zh-hans:阿伦·艾弗森;
zh-hk:阿倫·艾佛遜;}-和](../Page/阿伦·艾弗森.md "wikilink")[姚明](../Page/姚明.md "wikilink")。

| 位置       | 球員                                                                  | 球隊                                                                       |
| -------- | ------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| 先發球員     |                                                                     |                                                                          |
| 控球後衛     | [吉尔伯特·阿里纳斯](../Page/吉尔伯特·阿里纳斯.md "wikilink")                        | [-{zh-hans:华盛顿奇才队; zh-hk:華盛頓巫師隊;}-](../Page/华盛顿奇才队.md "wikilink")        |
| 得分後衛     | [德韦恩·韦德](../Page/德韦恩韦德.md "wikilink")                               | [迈阿密热火队](../Page/迈阿密热火队.md "wikilink")                                   |
| 小前鋒      | [-{zh-hans:勒布朗·詹姆斯; zh-hk:勒邦·占士;}-](../Page/勒布朗·詹姆斯.md "wikilink")  | [克里夫兰骑士队](../Page/克里夫兰骑士队.md "wikilink")                                 |
| 大前鋒／中鋒   | [克里斯·波什](../Page/克里斯·波什.md "wikilink")                              | [多伦多猛龙队](../Page/多伦多猛龙队.md "wikilink")                                   |
| 中鋒       | [沙奎尔·奥尼尔](../Page/沙奎尔·奥尼尔.md "wikilink")                            | [迈阿密热火队](../Page/迈阿密热火队.md "wikilink")                                   |
| 後備球員     |                                                                     |                                                                          |
| 控球後衛     | [昌西·比卢普斯](../Page/昌西·比卢普斯.md "wikilink")                            | [底特律活塞队](../Page/底特律活塞队.md "wikilink")                                   |
| 小前鋒      | [卡隆·巴特勒](../Page/卡隆·巴特勒.md "wikilink")                              | [華盛頓奇才隊](../Page/華盛頓奇才隊.md "wikilink")                                   |
| 得分後衛／小前鋒 | [贾森·-{zh-hans:基德; zh-hk:傑特;}-](../Page/贾森·基德.md "wikilink")\*       | [紐澤西籃網隊](../Page/紐澤西籃網隊.md "wikilink")                                   |
| 得分後衛     | [理查德·汉密尔顿](../Page/理查德·汉密尔顿.md "wikilink")                          | [底特律活塞队](../Page/底特律活塞队.md "wikilink")                                   |
| 大前鋒／中鋒   | [德懷特·霍華德](../Page/德懷特·霍華德.md "wikilink")                            | [奧蘭多魔術隊](../Page/奧蘭多魔術隊.md "wikilink")                                   |
| 得分後衛／小前鋒 | [-{zh-hans:乔·约翰逊; zh-hk:祖·莊遜;}-](../Page/乔·约翰逊.md "wikilink")       | [亞特蘭大鷹隊](../Page/亞特蘭大鷹隊.md "wikilink")                                   |
| 得分後衛     | [-{zh-hans:文斯·卡特; zh-hk:文森·卡特;}-](../Page/文斯·卡特.md "wikilink")      | [-{zh-hans:新泽西网队; zh-hant:紐澤西籃網隊;}-](../Page/新泽西网队.md "wikilink")        |
| 大前鋒／中鋒   | [-{zh-hans:杰梅因·奥尼尔; zh-hk:謝美·奧尼爾;}-](../Page/杰梅因·奥尼尔.md "wikilink") | [-{zh-hans:印第安那步行者队; zh-hant:印第安那溜馬隊;}-](../Page/印第安那步行者队.md "wikilink") |

**
東區全明星**

| 位置        | 球員                                                                                                      | 球隊                                                                               |
| --------- | ------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| 先發球員      |                                                                                                         |                                                                                  |
| 控球後衛      | [-{zh-hans:特雷西·麦克格雷迪; zh-hk:特雷西·麥基迪;}-](../Page/特雷西·麦克格雷迪.md "wikilink")                                | [休斯敦火箭队](../Page/休斯敦火箭队.md "wikilink")                                           |
| 得分後衛      | [-{zh-hans:科比;zh-hk:高比;zh-tw:科比;}-·-{zh-hans:布莱恩特;zh-hk:拜仁;zh-tw:布莱恩;}-](../Page/科比·布莱恩特.md "wikilink") | [洛杉矶湖人队](../Page/洛杉矶湖人队.md "wikilink")                                           |
| 小前鋒       | [-{zh-hans:凯文·加内特; zh-hk:奇雲·加納特;}-](../Page/奇雲·加納特.md "wikilink")                                       | [-{zh-hans:明尼苏达森林狼队; zh-hant:; zh-hk:明尼蘇達木狼隊;}-](../Page/明尼苏达森林狼队.md "wikilink") |
| 大前鋒       | [蒂姆·邓肯](../Page/蒂姆·邓肯.md "wikilink")                                                                    | [圣安东尼奥马刺队](../Page/圣安东尼奥马刺队.md "wikilink")                                       |
| 中鋒        | [姚明](../Page/姚明.md "wikilink")\*                                                                        | [休斯敦火箭队](../Page/休斯敦火箭队.md "wikilink")                                           |
| 後備球員      |                                                                                                         |                                                                                  |
| 得分後衛      | [雷·阿伦](../Page/雷·阿伦.md "wikilink")                                                                      | [西雅图超音速队](../Page/西雅图超音速队.md "wikilink")                                         |
| 小前鋒       | [卡梅隆·安东尼](../Page/卡梅隆·安东尼.md "wikilink")                                                                | [丹佛掘金队](../Page/丹佛掘金队.md "wikilink")                                             |
| 大前鋒       | [-{zh-hans:卡罗斯·布泽尔; zh-hk:卡路士·布沙;}-](../Page/卡罗斯·布泽尔.md "wikilink")\*                                   | [猶他爵士隊](../Page/猶他爵士隊.md "wikilink")                                             |
| 小前鋒       | [約什·霍華德](../Page/約什·霍華德.md "wikilink")                                                                  | [达拉斯小牛队](../Page/达拉斯小牛队.md "wikilink")                                           |
| 控球後衛／得分後衛 | [-{zh-hans:阿伦·艾弗森; zh-hk:阿倫·艾佛遜;}-](../Page/艾倫·艾弗森.md "wikilink")\*                                     | [丹佛掘金队](../Page/丹佛掘金队.md "wikilink")                                             |
| 小前鋒       | [肖恩·马里昂](../Page/肖恩·马里昂.md "wikilink")                                                                  | [-{zh-hans:菲尼克斯太阳队; zh-hant:鳳凰城太陽隊;}-](../Page/菲尼克斯太阳队.md "wikilink")            |
| 控球後衛      | [史蒂夫·奈許](../Page/史蒂夫·奈許.md "wikilink")\*                                                                | [-{zh-hans:菲尼克斯太阳队; zh-hant:鳳凰城太陽隊;}-](../Page/菲尼克斯太阳队.md "wikilink")            |
| 大前鋒／中鋒    | [-{zh-hans:德克·诺维茨基; zh-hant:德克·奴域斯基;}-](../Page/德克·诺维茨基.md "wikilink")                                  | [达拉斯小牛队](../Page/达拉斯小牛队.md "wikilink")                                           |
| 大前鋒／中鋒    | [梅米特·奥库](../Page/梅米特·奥库.md "wikilink")                                                                  | [猶他爵士隊](../Page/猶他爵士隊.md "wikilink")                                             |
| 控球後衛      | [-{zh-hans:托尼·帕克; zh-hk:東尼·帕加;}-](../Page/托尼·帕克.md "wikilink")                                          | [圣安东尼奥马刺队](../Page/圣安东尼奥马刺队.md "wikilink")                                       |
| 大前鋒／中鋒    | [阿玛雷·斯塔德迈尔](../Page/阿玛雷·斯塔德迈尔.md "wikilink")                                                            | [-{zh-hans:菲尼克斯太阳队; zh-hant:鳳凰城太陽隊;}-](../Page/菲尼克斯太阳队.md "wikilink")            |

**
西區全明星**

\* 因傷不能上陣

### 每節比分

  -
    {| class="wikitable"

|+ \!align=right|隊伍 \!|1 \!|2 \!|3 \!|4 \!|合計

|- style="text-align:center;" |align=left|**東區** |31||28||29||44 |132 |-
style="text-align:center;" |align=left|**西區** |39||40||40||34 |**153**
|- style="text-align:center;" |}

## 參見

  - [NBA全明星赛](../Page/NBA全明星赛.md "wikilink")

## 外部連結

  -
  -
[Category:NBA全明星賽](../Category/NBA全明星賽.md "wikilink")
[N](../Category/2007年体育.md "wikilink")