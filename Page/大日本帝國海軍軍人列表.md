**大日本帝國海軍軍人列表**為[大日本帝國海軍](../Page/大日本帝國海軍.md "wikilink")[軍人各階級](../Page/軍人.md "wikilink")・晉階年月日列表（含逝後追授階級）。

## 大元帥（陸海軍大將）

  - [明治天皇](../Page/明治天皇.md "wikilink")
  - [大正天皇](../Page/大正天皇.md "wikilink")
  - [昭和天皇](../Page/昭和天皇.md "wikilink")

## 兵科・機關科（[昭和](../Page/昭和.md "wikilink")17年11月1日兵科統合）

### 元帥海軍大將

  - [西鄉從道](../Page/西鄉從道.md "wikilink")
  - [伊東祐亨](../Page/伊東祐亨.md "wikilink")
  - [井上良馨](../Page/井上良馨.md "wikilink")
  - [東鄉平八郎](../Page/東鄉平八郎.md "wikilink")
  - [有栖川宮威仁親王](../Page/有栖川宮威仁親王.md "wikilink")
  - [伊集院五郎](../Page/伊集院五郎.md "wikilink")
  - [東伏見宮依仁親王](../Page/東伏見宮依仁親王.md "wikilink")
  - [島村速雄](../Page/島村速雄.md "wikilink")
  - [加藤友三郎](../Page/加藤友三郎.md "wikilink")
  - [伏見宮博恭王](../Page/伏見宮博恭王.md "wikilink")
  - [山本五十六](../Page/山本五十六.md "wikilink")
  - [永野修身](../Page/永野修身.md "wikilink")
  - [古賀峯一](../Page/古賀峯一.md "wikilink")

### 海軍大將

### 海軍（機關）中將

  - [榎本武揚](../Page/榎本武揚.md "wikilink")
  - [仁禮景範](../Page/仁禮景範.md "wikilink")
  - [有地品之允](../Page/有地品之允.md "wikilink")
  - [河原要一](../Page/河原要一.md "wikilink")
  - [新井有貫](../Page/新井有貫.md "wikilink")
  - [坂本俊篤](../Page/坂本俊篤.md "wikilink")
  - [富岡定恭](../Page/富岡定恭.md "wikilink")
  - [松本和](../Page/松本和.md "wikilink")
  - [釜屋忠道](../Page/釜屋忠道.md "wikilink")
  - [上泉德彌](../Page/上泉德彌.md "wikilink")
  - [佐藤鐵太郎](../Page/佐藤鐵太郎.md "wikilink")
  - [秋山真之](../Page/秋山真之.md "wikilink")
  - [小笠原長生](../Page/小笠原長生.md "wikilink")
  - [山路一善](../Page/山路一善.md "wikilink")
  - [佐藤皐藏](../Page/佐藤皐藏.md "wikilink")
  - [山岡豊一](../Page/山岡豊一.md "wikilink")
  - [鳥巢玉樹](../Page/鳥巢玉樹.md "wikilink")
  - [左近司政三](../Page/左近司政三.md "wikilink")
  - [飯田延太郎](../Page/飯田延太郎.md "wikilink")
  - [八角三郎](../Page/八角三郎.md "wikilink")
  - [荒城二郎](../Page/荒城二郎.md "wikilink")
  - [松山茂](../Page/松山茂.md "wikilink")
  - [寺島健](../Page/寺島健.md "wikilink")
  - [堀悌吉](../Page/堀悌吉.md "wikilink")
  - [坂野常善](../Page/坂野常善.md "wikilink")
  - [下村正助](../Page/下村正助.md "wikilink")
  - [谷本馬太郎](../Page/谷本馬太郎.md "wikilink")
  - [佐藤市郎](../Page/佐藤市郎.md "wikilink")
  - [新見政一](../Page/新見政一.md "wikilink")
  - [杉政人](../Page/杉政人.md "wikilink")
  - [清水光美](../Page/清水光美.md "wikilink")
  - [細萱戊子郎](../Page/細萱戊子郎.md "wikilink")
  - [高橋伊望](../Page/高橋伊望.md "wikilink")
  - [岩村清一](../Page/岩村清一.md "wikilink")
  - [浮田秀彦](../Page/浮田秀彦.md "wikilink")
  - [大川内傳七](../Page/大川内傳七.md "wikilink")
  - [小澤治三郎](../Page/小澤治三郎.md "wikilink")
  - [草鹿任一](../Page/草鹿任一.md "wikilink")
  - [鮫島具重](../Page/鮫島具重.md "wikilink")
  - [松浦永次郎](../Page/松浦永次郎.md "wikilink")
  - [小松輝久](../Page/小松輝久.md "wikilink")
  - [石井常次郎](../Page/石井常次郎.md "wikilink")
  - [桑折英三郎](../Page/桑折英三郎.md "wikilink")
  - [小林仁](../Page/小林仁_\(海軍軍人\).md "wikilink")
  - [稻垣生起](../Page/稻垣生起.md "wikilink")
  - [五藤存知](../Page/五藤存知.md "wikilink")
  - [須賀彦次郎](../Page/須賀彦次郎.md "wikilink")
  - [栗田健男](../Page/栗田健男.md "wikilink")
  - [戸塚道太郎](../Page/戸塚道太郎.md "wikilink")
  - [福田良三](../Page/福田良三.md "wikilink")
  - [三川軍一](../Page/三川軍一.md "wikilink")
  - [阿部弘毅](../Page/阿部弘毅.md "wikilink")
  - [岡敬純](../Page/岡敬純.md "wikilink")
  - [角田覺治](../Page/角田覺治.md "wikilink")
  - [田結穰](../Page/田結穰.md "wikilink")
  - [原忠一](../Page/原忠一.md "wikilink")
  - [宇垣纏](../Page/宇垣纏.md "wikilink")
  - [岡新](../Page/岡新.md "wikilink")
  - [大西瀧治郎](../Page/大西瀧治郎.md "wikilink")
  - [吉良俊一](../Page/吉良俊一.md "wikilink")
  - [左近允尚正](../Page/左近允尚正.md "wikilink")
  - [醍醐忠重](../Page/醍醐忠重.md "wikilink")
  - [福留繁](../Page/福留繁.md "wikilink")
  - [山口多聞](../Page/山口多聞.md "wikilink")
  - [志摩清英](../Page/志摩清英.md "wikilink")
  - [西村祥治](../Page/西村祥治.md "wikilink")
  - [大森仙太郎](../Page/大森仙太郎.md "wikilink")
  - [大田實](../Page/大田實.md "wikilink")
  - [木村昌福](../Page/木村昌福.md "wikilink")
  - [田中頼三](../Page/田中頼三.md "wikilink")　　
  - [草鹿龍之介](../Page/草鹿龍之介.md "wikilink")
  - [多田武雄](../Page/多田武雄.md "wikilink")
  - [保科善四郎](../Page/保科善四郎.md "wikilink")
  - [大西新藏](../Page/大西新藏.md "wikilink")
  - [前田稔](../Page/前田稔.md "wikilink")
  - [佐藤康夫](../Page/佐藤康夫.md "wikilink")
  - [小柳富次](../Page/小柳富次.md "wikilink")
  - [有馬正文](../Page/有馬正文.md "wikilink")
  - [柴崎惠次](../Page/柴崎惠次.md "wikilink")
  - [矢野英雄](../Page/矢野英雄.md "wikilink")
  - [伊集院松治](../Page/伊集院松治.md "wikilink")
  - [岩淵三次](../Page/岩淵三次.md "wikilink")
  - [中澤佑](../Page/中澤佑.md "wikilink")
  - [橋本信太郎](../Page/橋本信太郎.md "wikilink")
  - [矢野志加三](../Page/矢野志加三.md "wikilink")
  - [早川幹夫](../Page/早川幹夫.md "wikilink")
  - [有賀幸作](../Page/有賀幸作.md "wikilink")
  - [高柳儀八](../Page/高柳儀八.md "wikilink")
  - [猪口敏平](../Page/猪口敏平.md "wikilink")
  - [松永貞市](../Page/松永貞市.md "wikilink")
  - [坪井航三](../Page/坪井航三.md "wikilink")

### 海軍（機關）少將

  - [寺岡平吾](../Page/寺岡平吾.md "wikilink")
  - [岩下保太郎](../Page/岩下保太郎.md "wikilink")
  - [井原美岐雄](../Page/井原美岐雄.md "wikilink")
  - [三木守男](../Page/三木守男.md "wikilink")
  - [加來止男](../Page/加來止男.md "wikilink")
  - [横井忠雄](../Page/横井忠雄.md "wikilink")
  - [小川貫爾](../Page/小川貫爾.md "wikilink")
  - [上阪香苗](../Page/上阪香苗.md "wikilink")
  - [大林末雄](../Page/大林末雄.md "wikilink")
  - [高木惣吉](../Page/高木惣吉.md "wikilink")
  - [橋本象造](../Page/橋本象造.md "wikilink")
  - [黒木剛一](../Page/黒木剛一.md "wikilink")
  - [小島齊志](../Page/小島齊志.md "wikilink")
  - [木下三雄](../Page/木下三雄.md "wikilink")
  - [小田為清](../Page/小田為清.md "wikilink")
  - [三好輝彦](../Page/三好輝彦.md "wikilink")
  - [吉見信一](../Page/吉見信一.md "wikilink")
  - [湊慶讓](../Page/湊慶讓.md "wikilink")
  - [栗原悦藏](../Page/栗原悦藏.md "wikilink")
  - [黒島龜人](../Page/黒島龜人.md "wikilink")
  - [小島秀雄](../Page/小島秀雄.md "wikilink")
  - [柳本柳作](../Page/柳本柳作.md "wikilink")
  - [大野竹二](../Page/大野竹二.md "wikilink")
  - [松田千秋](../Page/松田千秋.md "wikilink")
  - [中村勝平](../Page/中村勝平.md "wikilink")
  - [平出英夫](../Page/平出英夫.md "wikilink")
  - [中瀬泝](../Page/中瀬泝.md "wikilink")
  - [山本親雄](../Page/山本親雄.md "wikilink")
  - [柳澤藏之助](../Page/柳澤藏之助.md "wikilink")
  - [竹内馨](../Page/竹内馨.md "wikilink")
  - [矢牧章](../Page/矢牧章.md "wikilink")
  - [光延東洋](../Page/光延東洋.md "wikilink")
  - [城英一郎](../Page/城英一郎.md "wikilink")
  - [山本善雄](../Page/山本善雄.md "wikilink")
  - [横山一郎](../Page/横山一郎.md "wikilink")
  - [川井巖](../Page/川井巖.md "wikilink")
  - [神重德](../Page/神重德.md "wikilink")
  - [鹿岡円平](../Page/鹿岡円平.md "wikilink")
  - [有泉龍之助](../Page/有泉龍之助.md "wikilink")
  - [古村啓藏](../Page/古村啓藏.md "wikilink")
  - [入佐俊家](../Page/入佐俊家.md "wikilink")
  - [木梨鷹一](../Page/木梨鷹一.md "wikilink")
  - [阿部俊雄](../Page/阿部俊雄.md "wikilink")
  - [長井滿](../Page/長井滿.md "wikilink")
  - [島津忠重](../Page/島津忠重.md "wikilink")

### 海軍（機關）大佐

  - [高松宮宣仁親王](../Page/高松宮宣仁親王.md "wikilink")
  - [沖野亦男](../Page/沖野亦男.md "wikilink")
  - [岡田貞外茂](../Page/岡田貞外茂.md "wikilink")
  - [渡名喜守定](../Page/渡名喜守定.md "wikilink")
  - [堀内豊秋](../Page/堀内豊秋.md "wikilink")
  - [源田實](../Page/源田實.md "wikilink")
  - [淵田美津雄](../Page/淵田美津雄.md "wikilink")
  - [樋端久利雄](../Page/樋端久利雄.md "wikilink")
  - [柴田武雄](../Page/柴田武雄.md "wikilink")
  - [吉田英三](../Page/吉田英三.md "wikilink")
  - [大井篤](../Page/大井篤.md "wikilink")
  - [井浦祥二郎](../Page/井浦祥二郎.md "wikilink")
  - [扇一登](../Page/扇一登.md "wikilink")
  - [實松讓](../Page/實松讓.md "wikilink")
  - [小園安名](../Page/小園安名.md "wikilink")
  - [豊田隈雄](../Page/豊田隈雄.md "wikilink")
  - [原道男](../Page/原道男.md "wikilink")
  - [渡邊安次](../Page/渡邊安次.md "wikilink")
  - [高橋赫一](../Page/高橋赫一.md "wikilink")
  - [江草隆繁](../Page/江草隆繁.md "wikilink")
  - [村田重治](../Page/村田重治.md "wikilink")
  - [島村信政](../Page/島村信政.md "wikilink")
  - [島津信夫](../Page/島津信夫.md "wikilink")
  - [多田野佐七郎](../Page/多田野佐七郎.md "wikilink")
  - [岡村基春](../Page/岡村基春.md "wikilink")
  - [野中五郎](../Page/野中五郎.md "wikilink")

### 海軍（機關）中佐

  - [廣瀬武夫](../Page/廣瀬武夫.md "wikilink")
  - [下村忠助](../Page/下村忠助.md "wikilink")
  - [加世田哲彦](../Page/加世田哲彦.md "wikilink")
  - [中山定義](../Page/中山定義.md "wikilink")
  - [中島親孝](../Page/中島親孝.md "wikilink")
  - [土肥一夫](../Page/土肥一夫.md "wikilink")
  - [寺内正道](../Page/寺内正道.md "wikilink")
  - [馬越喜七](../Page/馬越喜七.md "wikilink")
  - [大谷藤之助](../Page/大谷藤之助.md "wikilink")
  - [友永丈市](../Page/友永丈市.md "wikilink")
  - [板谷茂](../Page/板谷茂.md "wikilink")
  - [橋本以行](../Page/橋本以行.md "wikilink")
  - [工藤俊作](../Page/工藤俊作_\(海軍軍人\).md "wikilink")
  - [吉田俊雄](../Page/吉田俊雄.md "wikilink")
  - [板倉光馬](../Page/板倉光馬.md "wikilink")
  - [内田一臣](../Page/内田一臣.md "wikilink")
  - [岩佐直治](../Page/岩佐直治.md "wikilink")
  - [岡村德長](../Page/岡村德長.md "wikilink")

### 海軍（機關）少佐

  - [黒澤丈夫](../Page/黒澤丈夫.md "wikilink")
  - [黒木博司](../Page/黒木博司.md "wikilink")
  - [仁科關夫](../Page/仁科關夫.md "wikilink")

### 海軍（機關）大尉

  - [阿川弘之](../Page/阿川弘之.md "wikilink")
  - [中島知久平](../Page/中島知久平.md "wikilink")
  - [野村實](../Page/野村實.md "wikilink")
  - [樋口直](../Page/樋口直.md "wikilink")

### 海軍（機關）中尉

  - [三上卓](../Page/三上卓.md "wikilink")
  - [梅林孝次](../Page/梅林孝次.md "wikilink")

### 海軍（機關）少尉

  - [酒巻和男](../Page/酒巻和男.md "wikilink")
  - [有栖川宮栽仁王](../Page/栽仁王.md "wikilink")

### （機關）少尉候補生

### 特務（機關）士官

  - [岩本徹三](../Page/岩本徹三.md "wikilink")
  - [西澤廣義](../Page/西澤廣義.md "wikilink")
  - [坂井三郎](../Page/坂井三郎.md "wikilink")
  - [武藤金義](../Page/武藤金義.md "wikilink")
  - [黒田秀雄](../Page/黒田秀雄.md "wikilink")

### 海軍准士官

  - [稻垣清](../Page/稻垣清.md "wikilink")

### 海軍兵曹

  - [甲木清實](../Page/甲木清實.md "wikilink")
  - [小平義雄](../Page/小平義雄.md "wikilink")
  - [興津貴](../Page/興津貴.md "wikilink")
  - [中嶋七五三雄](../Page/中嶋七五三雄.md "wikilink")

### 海軍水兵

## 主計科

### 海軍主計中將

  - [武井大助](../Page/武井大助.md "wikilink")

### 海軍主計少將

  - [等松農夫藏](../Page/等松農夫藏.md "wikilink")

### 海軍主計大佐

  - [伏下哲夫](../Page/伏下哲夫.md "wikilink")
  - [青木大吉](../Page/青木大吉.md "wikilink")

### 海軍主計少佐

  - [中曾根康弘](../Page/中曾根康弘.md "wikilink")
  - [岡田貞寛](../Page/岡田貞寛.md "wikilink")

## 法務科

### 海軍法務大尉

  - [栗栖弘臣](../Page/栗栖弘臣.md "wikilink")

## 技術（造船、造兵、造機、水路、施設）科

### 海軍技術（造船、造兵、造機、水路、施設）中將

  - [平賀讓](../Page/平賀讓.md "wikilink")
  - [德川武定](../Page/德川武定.md "wikilink")

### 海軍技術（造船、造兵、造機、水路、施設）少將

  - [藤本喜久雄](../Page/藤本喜久雄.md "wikilink")

### 海軍技術（造船、造兵、造機、水路、施設）大佐

  - [牧野茂](../Page/牧野茂_\(海軍技術將校\).md "wikilink")
  - [松本喜太郎](../Page/松本喜太郎.md "wikilink")

### 海軍技術（造船、造兵、造機、水路、施設）少佐

  - [橋口義男](../Page/橋口義男.md "wikilink")
  - [福井靜夫](../Page/福井靜夫.md "wikilink")

### 海軍技術（造船、造兵、造機、水路、施設）大尉

  - [内藤初稻穗](../Page/内藤初稻穗.md "wikilink")
  - [宇治家彦](../Page/宇治家彦.md "wikilink")

## 軍醫科

### 海軍軍醫總監

  - [戸塚文海](../Page/戸塚文海.md "wikilink")
  - [高木兼寛](../Page/高木兼寛.md "wikilink")
  - [宮井積](../Page/宮井積.md "wikilink") 少將: 二戰時期 海南島 三亞 日本海軍病院院長

## 衛生（看護）科

## 藥劑科

## 齒科醫科

## 軍樂科

### 海軍軍樂長

  - [瀬戸口藤吉](../Page/瀬戸口藤吉.md "wikilink")
  - [江口源吾](../Page/江口源吾.md "wikilink")

## 關連項目

  - [大日本帝國陸軍軍人列表](../Page/大日本帝國陸軍軍人列表.md "wikilink")　
  - [海軍兵學校](../Page/海軍兵學校.md "wikilink")
  - [大日本帝國軍航空將兵列表](../Page/航空兵#大日本帝國軍航空將兵.md "wikilink")
  - [日本海军大将列表](../Page/日本海军大将列表.md "wikilink")

## 參考文獻

  - 『日本海軍指揮官総覧』（[新人物往來社](../Page/新人物往來社.md "wikilink")・1995年8月）　ISBN
    4-404-02246-8
  - 福川秀樹『日本海軍将官辞典』（芙蓉書房出版・2000年12月）　ISBN 4-8295-0272-X
  - 太平洋戦争研究会『帝国海軍将官総覧』（[KKベストセラーズワニの本](../Page/KKベストセラーズ.md "wikilink")・2002年7月）　ISBN
    4-584-01073-0
  - 戸高一成『日本海軍士官総覧』（[柏書房](../Page/柏書房.md "wikilink")・2003年2月）　ISBN
    4-7601-2340-7

[Category:日本海軍軍人](../Category/日本海軍軍人.md "wikilink")
[Category:日本军事列表](../Category/日本军事列表.md "wikilink")
[Category:日本人列表](../Category/日本人列表.md "wikilink")