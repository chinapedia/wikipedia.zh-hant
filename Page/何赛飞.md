**何賽飛**（），[中國大陆](../Page/中國大陆.md "wikilink")[越剧戏剧女演员](../Page/越剧.md "wikilink")，資深[女演員](../Page/女演員.md "wikilink")。籍贯[浙江省](../Page/浙江省.md "wikilink")[舟山市](../Page/舟山市.md "wikilink")[岱山县](../Page/岱山县.md "wikilink")。曾演出多部電視劇，何賽飛多數在古裝劇里飾演太皇太后、太后，民國劇中扮演[姨太太等](../Page/姨太太.md "wikilink")。

## 作品列表

### 電影

| 上映年份  | 電影名                                                                     | 角色      | 备注                                                  |
| ----- | ----------------------------------------------------------------------- | ------- | --------------------------------------------------- |
| 1984年 | 《[五女拜壽](../Page/五女拜壽.md "wikilink")》                                    | 翠雲      | 越劇電影                                                |
| 1987年 | 《[兩宮皇太后](../Page/兩宮皇太后.md "wikilink")》                                  | 紫鵑      |                                                     |
| 1988年 | 《[唐伯虎](../Page/唐伯虎.md "wikilink")》                                      | 沈九娘     | 越劇電影                                                |
| 1988年 | 《[戰爭見證人](../Page/戰爭見證人.md "wikilink")》                                  | 蘇浣      |                                                     |
| 1989年 | 《[紅樓夢](../Page/红楼梦.md "wikilink")》                                      | 妙玉      |                                                     |
| 1991年 | 《[大紅燈籠高高掛](../Page/大紅燈籠高高掛.md "wikilink")》                              | 梅珊（三姨太） |                                                     |
| 1993年 | 《[雙槍假面人](../Page/雙槍假面人.md "wikilink")》                                  | 賽雲霞     |                                                     |
| 1995年 | 《[敵後武工隊](../Page/敵後武工隊.md "wikilink")》                                  | 小紅雲     | 获得第十九届[大眾電影百花獎最佳女配角](../Page/大眾電影百花獎.md "wikilink") |
| 1995年 | 《[紅粉](../Page/紅粉.md "wikilink")》                                        | 小萼      |                                                     |
| 1995年 | 《[天涯歌女](../Page/天涯歌女.md "wikilink")》                                    | 小紅      |                                                     |
| 1996年 | 《[風月](../Page/風月.md "wikilink")》                                        | 林秀儀     |                                                     |
| 1999年 | 《[說出你的秘密](../Page/說出你的秘密.md "wikilink")》                                | 企業职員    |                                                     |
| 2000年 | 《[河流時光](../Page/河流時光.md "wikilink")》                                    |         |                                                     |
| 2001年 | 《[女足九號](../Page/女足九號.md "wikilink")》                                    | 朱茵      |                                                     |
| 2005年 | 《[金瓶少女](../Page/金瓶少女.md "wikilink")》/《[珍珠衫](../Page/珍珠衫.md "wikilink")》 | 平氏      |                                                     |
| 2007年 | 《[我媽是富婆](../Page/我媽是富婆.md "wikilink")》                                  | 李桂芬     |                                                     |
| 2008年 | 《[色·戒](../Page/色·戒.md "wikilink")》                                      | 蕭太      |                                                     |
| 2011年 | 《[少年鄧恩銘](../Page/少年鄧恩銘.md "wikilink")》                                  |         |                                                     |
| 2012年 | 《[遠方的路還要繼續走](../Page/遠方的路還要繼續走.md "wikilink")》                          | 李芳儀     |                                                     |
| 2016年 | 《[超级快递](../Page/超级快递.md "wikilink")》                                    |         |                                                     |
| 2017年 | 《[上海王](../Page/上海王.md "wikilink")》                                      |         | 客串                                                  |
| 2018年 | 《[那些女人](../Page/那些女人.md "wikilink")》                                    |         |                                                     |
|       |                                                                         |         |                                                     |

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>上映年份</p></th>
<th><p>中文名</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1992年</p></td>
<td><p>《<a href="../Page/喂，菲亞特.md" title="wikilink">喂，菲亞特</a>》</p></td>
<td><p>葉葉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p>《<a href="../Page/宋慈斷獄.md" title="wikilink">宋慈斷獄</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/國際列車從這里經過.md" title="wikilink">國際列車從這里經過</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/墜子皇后.md" title="wikilink">墜子皇后</a>》</p></td>
<td><p>墜子皇后</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/情系國脉.md" title="wikilink">情系國脉</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/漢口往事.md" title="wikilink">漢口往事</a>》</p></td>
<td><p>張秀梅（梅子）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/昨日留言.md" title="wikilink">昨日留言</a>》</p></td>
<td><p>惠潔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/闖上海.md" title="wikilink">闖上海</a>》</p></td>
<td><p>唐玉芬</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/女人三十.md" title="wikilink">女人三十</a>》</p></td>
<td><p>潘悅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/糊涂衙門.md" title="wikilink">糊涂衙門</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/大風堂.md" title="wikilink">大風堂</a>》</p></td>
<td><p>望春</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/寇老西兒.md" title="wikilink">寇老西兒</a>》</p></td>
<td><p>潘妃</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/花木兰.md" title="wikilink">花木兰</a>》</p></td>
<td><p><a href="../Page/花木兰.md" title="wikilink">花木兰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/千秋國家夢.md" title="wikilink">千秋國家夢</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/走出凱旋門.md" title="wikilink">走出凱旋門</a>》</p></td>
<td><p>張夢惠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p>《<a href="../Page/情迷海上花.md" title="wikilink">情迷海上花</a>》</p></td>
<td><p>蘇悅卿</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p>《<a href="../Page/東方欲曉.md" title="wikilink">東方欲曉</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p>《<a href="../Page/財神到.md" title="wikilink">財神到</a>》</p></td>
<td><p>蘇玉環</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p>《<a href="../Page/大宅門.md" title="wikilink">大宅門</a>》</p></td>
<td><p>楊九紅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p>《<a href="../Page/紅粉须眉.md" title="wikilink">紅粉须眉</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p>《<a href="../Page/星夢戀人.md" title="wikilink">星夢戀人</a>》</p></td>
<td><p>陳愛琳</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p>《<a href="../Page/完全婚姻手册之老公自家好.md" title="wikilink">完全婚姻手册之老公自家好</a>》</p></td>
<td><p>許茗雨</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p>《<a href="../Page/太陽不落山.md" title="wikilink">太陽不落山</a>》</p></td>
<td><p>馬翠蓮</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/愛是陽光.md" title="wikilink">愛是陽光</a>》</p></td>
<td><p>趙雅馨</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/選妃記.md" title="wikilink">選妃記</a>》</p></td>
<td><p>皇后</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/與羊共舞.md" title="wikilink">與羊共舞</a>》</p></td>
<td><p>葉小萌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/魂斷秦淮.md" title="wikilink">魂斷秦淮</a>》</p></td>
<td><p>馬湘蘭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/大宅門.md" title="wikilink">大宅門</a>2》</p></td>
<td><p>楊九紅</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/孝庄秘史.md" title="wikilink">孝庄秘史</a>》</p></td>
<td><p><a href="../Page/海兰珠.md" title="wikilink">海蘭珠</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/生死卧底.md" title="wikilink">生死卧底</a>》</p></td>
<td><p>江曼雲（三把鎖）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/藍色馬蹄蓮.md" title="wikilink">藍色馬蹄蓮</a>》</p></td>
<td><p>趙永紅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/分流.md" title="wikilink">分流</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/七武士.md" title="wikilink">七武士</a>》</p></td>
<td><p>蕭藏刀</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/愛比恨多一點.md" title="wikilink">愛比恨多一點</a>》</p></td>
<td><p>安虹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/少年天子_(电视剧).md" title="wikilink">少年天子</a>》</p></td>
<td><p><a href="../Page/懿靖大贵妃.md" title="wikilink">太妃</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/至高利益.md" title="wikilink">至高利益</a>》</p></td>
<td><p>劉璐璐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/不可触摸的真情.md" title="wikilink">不可触摸的真情</a>》</p></td>
<td><p>蓮姨</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/天下奇谋.md" title="wikilink">天下奇谋</a>》</p></td>
<td><p>聞如錦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/愛上天使.md" title="wikilink">愛上天使</a>》</p></td>
<td><p>利雅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/非常使命.md" title="wikilink">非常使命</a>》</p></td>
<td><p>鍾曉虹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/南宋傳奇之蟋蟀宰相.md" title="wikilink">南宋傳奇之蟋蟀宰相</a>》</p></td>
<td><p><a href="../Page/谢道清.md" title="wikilink">谢道清</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/絕不放棄.md" title="wikilink">絕不放棄</a>》</p></td>
<td><p>趙霞</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/月影风荷.md" title="wikilink">月影风荷</a>》</p></td>
<td><p>季文月</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/永乐英雄儿女.md" title="wikilink">永乐英雄儿女</a>》</p></td>
<td><p>紀妃</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/婆婆.md" title="wikilink">婆婆</a>》</p></td>
<td><p>譚超然</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/誠信.md" title="wikilink">誠信</a>》/《誓不罢休》</p></td>
<td><p>吳茵</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/白鹭謠.md" title="wikilink">白鹭謠</a>》</p></td>
<td><p>玉秀蓮</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/黃浦風雲云.md" title="wikilink">黃浦風雲云</a>》/《<a href="../Page/飛花如蝶.md" title="wikilink">飛花如蝶</a>》</p></td>
<td><p>于小蘭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/紫玉金砂.md" title="wikilink">紫玉金砂</a>》</p></td>
<td><p>柳叶</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/一生倾情.md" title="wikilink">一生倾情</a>》</p></td>
<td><p>区丽萍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/母爱十三宗.md" title="wikilink">母爱十三宗</a>》</p></td>
<td><p>李桂芬</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/明德绣庄.md" title="wikilink">明德绣庄</a>》</p></td>
<td><p>明姑</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/家门.md" title="wikilink">家门</a>》</p></td>
<td><p>江萍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/婆媳过招.md" title="wikilink">婆媳过招</a>》</p></td>
<td><p>冯雪梅</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/爱在来时.md" title="wikilink">爱在来时</a>/错爱天使》</p></td>
<td><p>苏拉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/血色残阳.md" title="wikilink">血色残阳</a>》</p></td>
<td><p>雅芝（三太太）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/最好的时光.md" title="wikilink">最好的时光</a>/一世情缘》</p></td>
<td><p>許心梅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/舞台姐妹.md" title="wikilink">舞台姐妹</a>》</p></td>
<td><p>商水花</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/红幡.md" title="wikilink">红幡</a>》</p></td>
<td><p>吴美云</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/梦里花落知多少.md" title="wikilink">梦里花落知多少</a>》</p></td>
<td><p>林母</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/江湖兄弟.md" title="wikilink">江湖兄弟</a>》</p></td>
<td><p>王美菊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/人怕出名.md" title="wikilink">人怕出名</a>》</p></td>
<td><p>金晓燕</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/纸醉金迷.md" title="wikilink">纸醉金迷</a>》</p></td>
<td><p>朱四奶奶</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/祈望.md" title="wikilink">祈望</a>》</p></td>
<td><p>冯丽萍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/嫁衣.md" title="wikilink">嫁衣</a>》</p></td>
<td><p>王月</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/娘家的故事.md" title="wikilink">娘家的故事</a>》</p></td>
<td><p>李秋萍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/侬本多情.md" title="wikilink">侬本多情</a>》</p></td>
<td><p>胡瑛</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/同龄子.md" title="wikilink">同龄子</a>》</p></td>
<td><p>王芸英</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/故梦.md" title="wikilink">故梦</a>》</p></td>
<td><p>太妃</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/杨贵妃秘史.md" title="wikilink">楊貴妃秘史</a>》</p></td>
<td><p><a href="../Page/武惠妃.md" title="wikilink">武惠妃</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/侦探小说.md" title="wikilink">侦探小说</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/枪声背后.md" title="wikilink">枪声背后</a>》</p></td>
<td><p>刘月娥（林素贞）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/电视台的故事.md" title="wikilink">电视台的故事</a>》</p></td>
<td><p>许萌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/那个年代.md" title="wikilink">那个年代</a>》</p></td>
<td><p>余莉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/老马家的幸福往事.md" title="wikilink">老马家的幸福往事</a>》</p></td>
<td><p>胡根娣</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/國色天香.md" title="wikilink">國色天香</a>》</p></td>
<td><p>香夫人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/後宮_(電視劇).md" title="wikilink">後宮</a>》</p></td>
<td><p><a href="../Page/孝肃皇后.md" title="wikilink">周太后</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/唐宮美人天下.md" title="wikilink">唐宮美人天下</a>》</p></td>
<td><p>林雪儀</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/背后.md" title="wikilink">背后</a>》</p></td>
<td><p>盧嬸</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/娘家的故事.md" title="wikilink">娘家的故事</a>3》</p></td>
<td><p>李秋萍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p>《<a href="../Page/新女婿時代.md" title="wikilink">新女婿時代</a>》</p></td>
<td><p>桂蘭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/唐宮燕.md" title="wikilink">唐宮燕</a>》</p></td>
<td><p><a href="../Page/韋皇后_(唐中宗).md" title="wikilink">韋　后</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/那金花和她的女婿.md" title="wikilink">那金花和她的女婿</a>》</p></td>
<td><p>那金花</p></td>
<td><p>第一女主角<br />
又名《刁蠻岳母鬥女婿》</p></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/保衛孫子.md" title="wikilink">保衛孫子</a>》</p></td>
<td><p>張小寧</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/金玉滿堂_(中國電視劇).md" title="wikilink">金玉滿堂</a>》</p></td>
<td><p>趙寧麗</p></td>
<td><p>友情客串</p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p>《<a href="../Page/女人淚.md" title="wikilink">女人淚</a>》</p></td>
<td><p>吳麗芳</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>《<a href="../Page/刀客家族的女人.md" title="wikilink">刀客家族的女人</a>》</p></td>
<td><p>張夫人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p>《<a href="../Page/我们的青春1977.md" title="wikilink">我们的青春1977</a>》</p></td>
<td><p>沈玉梅</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>《<a href="../Page/假如幸福臨來.md" title="wikilink">假如幸福臨來</a>》</p></td>
<td><p>蒋新慧</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>《我们家的微幸福生活》</p></td>
<td><p>虞美人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p>《<a href="../Page/活法.md" title="wikilink">活法</a>》</p></td>
<td><p>王丽茹</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>《<a href="../Page/等你爱我.md" title="wikilink">等你爱我</a>》</p></td>
<td><p>贺母</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p>《<a href="../Page/娘家的故事4之爱的陪伴.md" title="wikilink">娘家的故事4之爱的陪伴</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>《<a href="../Page/再见，老婆大人.md" title="wikilink">再见，老婆大人</a>》</p></td>
<td><p>甄妈妈</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>《<a href="../Page/少林问道.md" title="wikilink">少林问道</a>》</p></td>
<td><p>梅姑</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>《<a href="../Page/老妈的桃花运.md" title="wikilink">老妈的桃花运</a>》</p></td>
<td><p>丁香香</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p>《<a href="../Page/玄武_(电视剧).md" title="wikilink">玄武</a>》</p></td>
<td><p>小玫瑰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p>《<a href="../Page/青恋.md" title="wikilink">青恋</a>》</p></td>
<td><p>春霞</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p>《<a href="../Page/最亲最爱的人.md" title="wikilink">最亲最爱的人</a>》</p></td>
<td><p>张锦屏</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p>《<a href="../Page/面具背后.md" title="wikilink">面具背后</a>》</p></td>
<td><p>吴玉琴</p></td>
<td><p>2014年拍攝</p></td>
</tr>
<tr class="odd">
<td><p>待播</p></td>
<td><p>《<a href="../Page/二毛驴传奇.md" title="wikilink">二毛驴传奇</a>》</p></td>
<td><p>孟母</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>待播</p></td>
<td><p>《<a href="../Page/王小笛驯夫记.md" title="wikilink">王小笛驯夫记</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>待播</p></td>
<td><p>《<a href="../Page/亲爱的设计师.md" title="wikilink">亲爱的设计师</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>待播</p></td>
<td><p>《<a href="../Page/甜蜜.md" title="wikilink">甜蜜</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>待播</p></td>
<td><p>《<a href="../Page/山海蓝图.md" title="wikilink">山海蓝图</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>待播</p></td>
<td><p>《<a href="../Page/中国天眼.md" title="wikilink">中国天眼</a>》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 舞台演出（越劇 / 滑稽戏）

| 上映年份             | 类型                                          | 中文名                                      | 角色                                      | 备注      |
| ---------------- | ------------------------------------------- | ---------------------------------------- | --------------------------------------- | ------- |
| 1983年起(浙江小百花越剧团) | [越　剧](../Page/越剧.md "wikilink")             | [白蛇传](../Page/白蛇传.md "wikilink")         | [白娘子](../Page/白娘子.md "wikilink")        |         |
| 越　剧              | [春草闯堂](../Page/春草闯堂.md "wikilink")          | 春草                                       |                                         |         |
| 越　剧              | [大观园](../Page/大观园.md "wikilink")            | [林黛玉](../Page/林黛玉.md "wikilink")         |                                         |         |
| 越　剧              | [貂禅](../Page/貂禅.md "wikilink")              | 貂禅                                       |                                         |         |
| 越　剧              | [汉武之恋](../Page/汉武之恋.md "wikilink")          | [陈阿娇](../Page/陈皇后_\(汉武帝\).md "wikilink") |                                         |         |
| 越　剧              | [汉宫怨](../Page/汉宫怨.md "wikilink")            | [许平君或琼瑛](../Page/许平君.md "wikilink")      |                                         |         |
| 越　剧              | [红丝错](../Page/红丝错.md "wikilink")            | 章榴月                                      |                                         |         |
| 越　剧              | [红楼梦](../Page/紅樓夢_\(越劇\).md "wikilink")     | [林黛玉](../Page/林黛玉.md "wikilink")         |                                         |         |
| 越　剧              | [孔雀东南飞](../Page/孔雀东南飞.md "wikilink")        | 刘兰芝                                      |                                         |         |
| 越　剧              | [盘夫索夫](../Page/盘夫索夫.md "wikilink")          |                                          |                                         |         |
| 越　剧              | [沙漠王子](../Page/沙漠王子.md "wikilink")          | 伊丽                                       |                                         |         |
| 越　剧              | [三弟审兄](../Page/三弟审兄.md "wikilink")—迷汤       | 柳春莺                                      |                                         |         |
| 越　剧              | [双珠凤](../Page/双珠凤.md "wikilink")（送花楼会）\[1\] | 霍定金                                      |                                         |         |
| 越　剧              | [唐伯虎落第](../Page/唐伯虎落第.md "wikilink")        | [沈九娘](../Page/沈九娘.md "wikilink")         |                                         |         |
| 越　剧              | [新潮西厢](../Page/新潮西厢.md "wikilink")          | [崔莺莺](../Page/崔莺莺.md "wikilink")         |                                         |         |
| 越　剧              | [祥林嫂](../Page/祥林嫂.md "wikilink")            |                                          |                                         |         |
| 越　剧              | [一缕麻](../Page/一缕麻.md "wikilink")            |                                          |                                         |         |
| 越　剧              | [卓文君](../Page/卓文君.md "wikilink")（白头吟）       | [卓文君](../Page/卓文君.md "wikilink")         |                                         |         |
| 越　剧              | [终生大事](../Page/终生大事.md "wikilink")          | 金梅儿                                      |                                         |         |
| 越　剧              | [五女拜寿](../Page/五女拜寿.md "wikilink")          | 翠云                                       |                                         |         |
| 2003年            | [上海滑稽剧团](../Page/上海滑稽剧团.md "wikilink")      | [滑稽戏](../Page/滑稽戏.md "wikilink")         | [啼笑因缘](../Page/啼笑因缘.md "wikilink")\[2\] |         |
| 2005年9月          | 上海滑稽剧团                                      | 滑稽戏                                      | [太太万岁](../Page/太太万岁.md "wikilink")\[3\] |         |
| 2005年            | 越　剧                                         | [玉卿嫂](../Page/玉卿嫂.md "wikilink")         | 玉卿嫂                                     | 辞演\[4\] |
|                  |                                             |                                          |                                         |         |

## 獎項

| 年份    | 頒獎典禮                                         | 獎項           | 作品                                     |
| ----- | -------------------------------------------- | ------------ | -------------------------------------- |
| 1995年 |                                              | 中國電影表演藝術學會獎  |                                        |
| 1996年 | 第十九屆[大眾電影百花獎](../Page/大众电影百花奖.md "wikilink") | 最佳女配角        | 《[敵後武工隊](../Page/敵後武工隊.md "wikilink")》 |
| 2001年 | 中央電視台美菱杯                                     | 观众最喜爱的十佳女演员奖 | 《[大宅門](../Page/大宅門.md "wikilink")》     |

## 外部連結

  -
  -
  -
## 注释

<references/>

[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:岱山人](../Category/岱山人.md "wikilink")
[Category:浙江艺术职业学院校友](../Category/浙江艺术职业学院校友.md "wikilink")
[Category:浙江演员](../Category/浙江演员.md "wikilink")
[Category:越剧演员](../Category/越剧演员.md "wikilink")
[S赛](../Category/何姓.md "wikilink")

1.

2.

3.
4.