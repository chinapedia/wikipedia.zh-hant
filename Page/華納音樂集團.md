**華納音樂集團**（英文：**Warner
Music**）是美国的一家总部位于[纽约市唱片公司](../Page/纽约市.md "wikilink")。华纳音乐集团亦是全球最大的美国独资音乐集团，全球三大唱片公司之一。[時代華納](../Page/時代華納.md "wikilink")（**Time
Warner**）曾經為母公司，2004年脫離。[華納兄弟為拍片時可使用廉價音樂](../Page/華納兄弟.md "wikilink")，於1929年曾創立「音樂出版有限公司」（MPHC），購入音樂版權。1930年購入Brunswick唱片公司而進軍唱片業。

1967年，華納兄弟影片公司收購[大西洋唱片](../Page/大西洋唱片.md "wikilink")，1972年再購入Elektra唱片與其子公司Nonesuch唱片，合組Warner-Atlantic-Elektra(WEA)公司。隨後數十年，華納兄弟繼續透過各國收購，發展成全球最大的唱片公司之一，倂購對象包括法國Erato公司，德國Teldec公司，芬蘭Finlandia公司。

2011年7月，[時代華納出售華納音樂集團予Access](../Page/時代華納.md "wikilink") Industries。

2013年7月，華納音樂集團向環球唱片收購EMI旗下[Parlophone](../Page/Parlophone.md "wikilink")
Label Group。

## 台灣地區發展

1991年，華納音樂集團來台合資收購[飛碟唱片的部分股權](../Page/飛碟唱片.md "wikilink")，並於1996年完全收購[飛碟唱片](../Page/飛碟唱片.md "wikilink")。

2014年4月，華納音樂集團收購[金牌大風](../Page/金牌大風.md "wikilink")，保留品牌，亦成為子公司。

## 合作藝人

### 香港

### 台灣

### 日本

### 中國

  - [劉鳳瑤](../Page/劉鳳瑤.md "wikilink")
  - [李榮浩](../Page/李榮浩.md "wikilink")（同時與台灣華納簽約）
  - [關喆](../Page/關喆.md "wikilink")
  - 王矜霖
  - [袁婭維](../Page/袁婭維.md "wikilink")
  - [婁藝瀟](../Page/婁藝瀟.md "wikilink")
  - 李汶翰

### 韓國

以下列表以藝人作品首次代理發行的時序排列。

**韓國華納音樂**— 獲授權在[亞洲地區進行代理的旗下藝人](../Page/亞洲.md "wikilink")

  -
  - REX.D

  - [TAHITI](../Page/TAHITI.md "wikilink")

  - JACE (Band JACE)

  - SIXBOMB

  -
  - Gru:vi

#### 代理藝人

**[YG娛樂](../Page/YG娛樂.md "wikilink") 授權代理**

  - [BIGBANG](../Page/BIGBANG.md "wikilink") 韓語作品
      - [G-DRAGON](../Page/權志龍.md "wikilink") 韓語作品
      - [T.O.P](../Page/T.O.P..md "wikilink")
      - [TAEYANG (太陽)](../Page/太陽_\(歌手\).md "wikilink") 韓語作品
      - [DAESUNG (大聲)](../Page/大聲.md "wikilink") 韓語及日語作品
      - [SEUNGRI (勝利)](../Page/勝利_\(歌手\).md "wikilink")
      - [GD\&TOP](../Page/GD&TOP.md "wikilink")
      - [GD X TAEYANG](../Page/GD_X_TAEYANG.md "wikilink")
  - [CL](../Page/李彩麟.md "wikilink") 韓語及英語作品
  - [LEE HI (李遐怡)](../Page/李遐怡.md "wikilink")
  - [樂童音樂家 (AKMU)](../Page/樂童音樂家.md "wikilink")
  - [WINNER](../Page/WINNER.md "wikilink") 韓語作品
      - [姜昇潤](../Page/姜昇潤.md "wikilink") (前譯：姜勝允)
      - [MINO](../Page/宋閔浩.md "wikilink")
  - [EPIK HIGH](../Page/Epik_High.md "wikilink")
      - [Tablo](../Page/Tablo.md "wikilink")
  - [HI SUHYUN](../Page/HI秀賢.md "wikilink") (LEE HI + 樂童音樂家 李秀賢)
  - [JINUSEAN](../Page/Jinusean.md "wikilink")
  - [iKON](../Page/iKON.md "wikilink") 韓語作品
      - [BOBBY](../Page/金知元_\(饒舌者\).md "wikilink")
  - [BLACKPINK](../Page/BLACKPINK.md "wikilink") 韓語作品
  - [MOBB](../Page/MOBB.md "wikilink") (WINNER MINO + iKON BOBBY) 韓語作品
  - [SECHSKIES (水晶男孩)](../Page/水晶男孩.md "wikilink")

**The Black Label**

  - [Zion.T](../Page/Zion.T.md "wikilink")

**[HIGHGRND](../Page/HIGHGRND.md "wikilink")**

  - [hyukoh](../Page/hyukoh.md "wikilink")

**PSYG**

  - [PSY](../Page/PSY.md "wikilink")


**[FNC娛樂](../Page/FNC娛樂.md "wikilink") 授權代理**

  - [FTISLAND](../Page/FTIsland.md "wikilink") 韓語及日語作品
      - [李洪基](../Page/李洪基.md "wikilink") 韓語及日語作品
  - [CNBLUE](../Page/CNBLUE.md "wikilink") 韓語及日語作品
      - [鄭容和](../Page/鄭容和.md "wikilink")
      - [李宗泫](../Page/李宗泫.md "wikilink") 日語作品
  - [AOA](../Page/AOA_\(韓國\).md "wikilink") 韓語作品 —
    只限[中國地區](../Page/中國.md "wikilink") (其他亞洲地區由[Sony
    Music](../Page/台灣索尼音樂娛樂.md "wikilink")、[寰亞唱片代理](../Page/寰亞唱片.md "wikilink"))﻿
      - [AOA Cream](../Page/AOA_Cream.md "wikilink")
      - [智珉](../Page/申智珉.md "wikilink")
  - JIMIN N J.DON (AOA 智珉 + J.DON (N.Flying 李承協))
  - [N.Flying](../Page/N.Flying.md "wikilink") 韓語作品
  - [SF9](../Page/SF9.md "wikilink") 韓語及日語作品
  - [HONEYST](../Page/HONEYST.md "wikilink")


**[BRANDNEW MUSIC](../Page/Brand_New_Music.md "wikilink") 數位授權代理**

  - [San E](../Page/San_E.md "wikilink")

  -
  -
  - 梁多一 (Yang Da Il)

  -
  - CANDLE

  -
  -
  -
  - (於2017年停止活動)

  -
  -
  - [Sanchez](../Page/Sanchez.md "wikilink")

  - Kanto

  - BRANDNEW BOYS (暫名)

      - [MXM](../Page/MXM_\(男子音樂組合\).md "wikilink")


**其他授權代理藝人**

  - [吳元斌](../Page/吳元斌.md "wikilink") 韓語及日語作品
  - [ZE:A帝國之子](../Page/ZE:A.md "wikilink")
  - [金賢重](../Page/金賢重.md "wikilink") 韓語作品
  - [B1A4](../Page/B1A4.md "wikilink") 韓語作品
  - [ROY KIM](../Page/羅伊·金.md "wikilink")
  - [g.o.d](../Page/g.o.d.md "wikilink")
  - [B.A.P](../Page/B.A.P.md "wikilink") 日語作品
  - [IU](../Page/IU_\(艺人\).md "wikilink") — 2016年精選輯《SMASH
    HITS》，限[台灣地區](../Page/台灣.md "wikilink")
  - [Eric Nam](../Page/Eric_Nam.md "wikilink")
  - [SEVENTEEN](../Page/Seventeen_\(組合\).md "wikilink")
  - [Block B](../Page/Block_B.md "wikilink") 日語作品
  - [CLC](../Page/CLC.md "wikilink") —
    只限[中國地區](../Page/中國.md "wikilink")
    (其他亞洲地區由[環球音樂代理](../Page/環球音樂_\(台灣\).md "wikilink"))
  - [Wanna One](../Page/Wanna_One.md "wikilink")
  - [少女時代](../Page/少女時代.md "wikilink") —
    只限[中國地區](../Page/中國.md "wikilink")
    (其他亞洲地區由[環球音樂](../Page/環球音樂_\(台灣\).md "wikilink")、[Avex](../Page/Avex.md "wikilink")
    Asia、[Sony
    Music](../Page/台灣索尼音樂娛樂.md "wikilink")、[寰亞唱片代理](../Page/寰亞唱片.md "wikilink"))﻿
  - [Lovelyz](../Page/Lovelyz.md "wikilink") —
    只限[中國地區](../Page/中國.md "wikilink")
    (其他亞洲地區由[環球唱片](../Page/環球唱片.md "wikilink")、[Sony
    Music](../Page/台灣索尼音樂娛樂.md "wikilink")、[寰亞唱片代理](../Page/寰亞唱片.md "wikilink"))
  - [LUCENTE](../Page/LUCENTE.md "wikilink")

### 新加坡

  - [陳偉聯](../Page/陳偉聯.md "wikilink")
  - [石欣卉](../Page/石欣卉.md "wikilink")
  - [陳世維](../Page/陳世維.md "wikilink")
  - [向洋](../Page/向洋.md "wikilink")

### 馬來西亞

  - Atilia
  - Hujan
  - Faizal Tahir
  - Bella Nazari
  - Pesawat
  - Tysha Tiar
  - Caliph Buskers
  - Alyah
  - Azlan & The Typewriter
  - Noh Salleh
  - Andi Bernadee
  - MonoloQue
  - Aizat Amdan
  - Malique
  - Oceanlight
  - Tuah SAJA
  - Elizabeth Tan
  - Nazim
  - Cassidy
  - Bittersweet
  - Dewi Seriestha
  - Ammar Hamdan
  - Farawahida
  - Maya Karin
  - Awi Rafael
  - Yasin
  - Payslip
  - Aziz Harun
  - Harris Baba
  - Nastia
  - Dr. Katalina
  - Kaka Azraff
  - Diana Danielle
  - Liyana Jasmay\[1\]

### 爱尔兰

  - [恩雅](../Page/恩雅.md "wikilink")
  - [灰烬合唱团](../Page/灰烬合唱团.md "wikilink")

### 德国

  - [罗伯汤玛斯](../Page/罗伯汤玛斯.md "wikilink")

### 法国

  - [悲剧演唱组](../Page/悲剧演唱组.md "wikilink")

### 澳大利亚

  - [圣洁凡蕾丝](../Page/圣洁凡蕾丝.md "wikilink")
  - [科迪辛普森](../Page/科迪辛普森.md "wikilink")

### 挪威

  - [M2M](../Page/M2M_\(組合\).md "wikilink")

### 加拿大

  - [琼妮蜜雪儿](../Page/琼妮蜜雪儿.md "wikilink")
  - [丹尼尔](../Page/丹尼尔.md "wikilink")
  - [简单计划](../Page/简单计划.md "wikilink")
  - [发烧乐团](../Page/发烧乐团.md "wikilink")
  - [麦可布雷](../Page/麦可布雷.md "wikilink")
  - [Scott Helman](../Page/Scott_Helman.md "wikilink")

### 英国

  - [艾德·希蘭](../Page/艾德·希蘭.md "wikilink")
  - [菲尔柯林斯](../Page/菲尔柯林斯.md "wikilink")
  - [新秩序合唱团](../Page/新秩序合唱团.md "wikilink")
  - [詹姆仕·布朗特](../Page/詹姆仕·布朗特.md "wikilink")
  - [起笑蛙](../Page/起笑蛙.md "wikilink")
  - [缪斯乐队](../Page/缪斯乐队.md "wikilink")
  - [席尔](../Page/席尔.md "wikilink")
  - [黑暗旋风合唱团](../Page/黑暗旋风合唱团.md "wikilink")
  - [喷射机合唱团](../Page/喷射机合唱团.md "wikilink")

## 曾签艺人

### 韓國

  - [T-ara](../Page/T-ara.md "wikilink") (→上點音樂→龍楨文化→香蕉計劃)

**曾代理藝人**

  - [SHINHWA (神話)](../Page/神话_\(组合\).md "wikilink") (→環球唱片→索尼音樂娛樂)
      - [李玟雨](../Page/李玟雨.md "wikilink")
      - [申彗星](../Page/申彗星.md "wikilink")
  - [Rain](../Page/Rain.md "wikilink") (→環球唱片→索尼音樂娛樂)
  - [AFTERSCHOOL](../Page/After_School.md "wikilink") (→愛貝克思)
      - [橙子焦糖 (Orange Caramel)](../Page/橙子焦糖.md "wikilink") (→愛貝克思)
      - [嘉熙](../Page/嘉熙_\(歌手\).md "wikilink")
  - [JYJ](../Page/JYJ.md "wikilink")
  - [2NE1](../Page/2NE1.md "wikilink") (已解散)
  - [KARA](../Page/Kara.md "wikilink") (已解散)
  - [蘇志燮](../Page/蘇志燮.md "wikilink") (→大田出版)
  - [SE7EN](../Page/崔東昱.md "wikilink") (→YS Entertainment)
  - [INFINITE](../Page/INFINITE.md "wikilink") (→環球唱片)
  - [NU'EST](../Page/NU'EST.md "wikilink")
    (授權到期，改由[韓國](../Page/韓國.md "wikilink")[LOEN娛樂作全球數位發行](../Page/LOEN娛樂.md "wikilink"))
  - [TEAM H (張根碩 X BIG BROTHER)](../Page/張根碩.md "wikilink") (→海蝶音樂)
  - [JUNIEL](../Page/Juniel.md "wikilink")
  - [MASTA WU](../Page/Masta_Wu.md "wikilink")

### 香港

  - [郭富城](../Page/郭富城.md "wikilink")
  - [林子祥](../Page/林子祥.md "wikilink")
  - [刘锡明](../Page/刘锡明.md "wikilink")
  - [刘德华](../Page/刘德华.md "wikilink")
  - [张卫健](../Page/张卫健.md "wikilink")
  - [杜德伟](../Page/杜德伟.md "wikilink")
  - [杜德智](../Page/杜德智.md "wikilink")
  - [周启生](../Page/周启生.md "wikilink")
  - [钟镇涛](../Page/钟镇涛.md "wikilink")
  - [陈百强](../Page/陈百强.md "wikilink")
  - [古巨基](../Page/古巨基.md "wikilink")
  - [吕方](../Page/吕方.md "wikilink")
  - [黄子华](../Page/黄子华.md "wikilink")
  - [周国贤](../Page/周国贤.md "wikilink")
  - [林忆莲](../Page/林忆莲.md "wikilink")
  - [黎姿](../Page/黎姿.md "wikilink")
  - [马怡静](../Page/马怡静.md "wikilink")
  - [郑秀文](../Page/郑秀文.md "wikilink")
  - [纪炎炎](../Page/纪炎炎.md "wikilink")
  - [罗敏庄](../Page/罗敏庄.md "wikilink")
  - [谷祖琳](../Page/谷祖琳.md "wikilink")
  - [戴辛尉](../Page/戴辛尉.md "wikilink")
  - [梁咏琪](../Page/梁咏琪.md "wikilink")
  - [Beyond](../Page/Beyond.md "wikilink")
  - [太极乐队](../Page/太极乐队.md "wikilink")
  - [VRF](../Page/VRF.md "wikilink")
  - [LMF](../Page/LMF.md "wikilink")
  - [Shine](../Page/Shine.md "wikilink")
  - [Zarahn](../Page/Zarahn.md "wikilink")
  - [薛凱琪](../Page/薛凱琪.md "wikilink")
  - [方大同](../Page/方大同.md "wikilink")
  - [衛蘭](../Page/衛蘭.md "wikilink")

### 台灣

  - [王傑](../Page/王傑_\(歌手\).md "wikilink")
  - [曾庆瑜](../Page/曾庆瑜.md "wikilink")
  - [张凤凤](../Page/张凤凤.md "wikilink")
  - [张惠妹](../Page/张惠妹.md "wikilink")
  - [萧亚轩](../Page/萧亚轩.md "wikilink")
  - [5566](../Page/5566.md "wikilink")
  - [辛晓琪](../Page/辛晓琪.md "wikilink")
  - [Choc7](../Page/Choc7.md "wikilink")
  - [郭品超](../Page/郭品超.md "wikilink")
  - [费玉清](../Page/费玉清.md "wikilink")
  - [杨宗纬](../Page/杨宗纬.md "wikilink")
  - [舞思愛](../Page/舞思愛.md "wikilink")
  - [黄立行](../Page/黄立行.md "wikilink")
  - [增山裕纪](../Page/增山裕纪.md "wikilink")
  - [ZIP 发射乐团](../Page/ZIP_发射乐团.md "wikilink")
  - [周汤豪](../Page/周汤豪.md "wikilink")
  - [孙翠凤](../Page/孙翠凤.md "wikilink")
  - [蕭敬騰](../Page/蕭敬騰.md "wikilink")

### 新加坡及马来西亚

  - [蔡健雅](../Page/蔡健雅.md "wikilink")
  - [蔡淳佳](../Page/蔡淳佳.md "wikilink")
  - [林俊傑](../Page/林俊傑.md "wikilink")
  - [郭美美](../Page/郭美美.md "wikilink")
  - [孙燕姿](../Page/孙燕姿.md "wikilink")
  - [黃靖倫](../Page/黃靖倫.md "wikilink")
  - [Buddy](../Page/Buddy.md "wikilink")
  - [曹格](../Page/曹格.md "wikilink")
  - [張智成](../Page/張智成.md "wikilink")
  - [何維健](../Page/何維健.md "wikilink")

### 日本

  - [加藤纪子](../Page/加藤纪子.md "wikilink")
  - [さだまさし](../Page/さだまさし.md "wikilink")
  - [恋爱信号](../Page/恋爱信号.md "wikilink")
  - [三浦理恵子](../Page/三浦理恵子.md "wikilink")
  - [笠原弘子](../Page/笠原弘子.md "wikilink")
  - カルロス・トシキ&オメガトライブ
  - [荒木真树彦](../Page/荒木真树彦.md "wikilink")
  - [中森明菜](../Page/中森明菜.md "wikilink")
  - [MEG](../Page/MEG.md "wikilink")
  - [小松政夫](../Page/小松政夫.md "wikilink")
  - サラ・ブライトマン
  - [冈村孝子](../Page/冈村孝子.md "wikilink")
  - 武田久美子

## 外部連結

  - [華納音樂集團](http://www.wmg.com/)

  -
## 参考文献

[Category:美國唱片公司](../Category/美國唱片公司.md "wikilink")
[Category:華納媒體](../Category/華納媒體.md "wikilink")
[Category:華納音樂集團](../Category/華納音樂集團.md "wikilink")
[Category:国际唱片业协会成员](../Category/国际唱片业协会成员.md "wikilink")
[Category:贝恩资本公司](../Category/贝恩资本公司.md "wikilink")
[Category:1958年成立的公司](../Category/1958年成立的公司.md "wikilink")

1.  [1](http://warnermusic.com.my/warner-music-malaysia-artist/)