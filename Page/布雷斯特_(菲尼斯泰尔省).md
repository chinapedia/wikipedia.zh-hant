<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><big><strong>布雷斯特<br />
Brest</strong></big></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Dessinblasonbrest.jpeg" title="fig:Dessinblasonbrest.jpeg">Dessinblasonbrest.jpeg</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Brest_dot.png" title="fig:Brest_dot.png">Brest_dot.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>基本资料</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>大区</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>省</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>面积</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>人口</strong><br />
- 总人口<br />
- <a href="../Page/人口密度.md" title="wikilink">人口密度</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>坐标</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>网站</strong></p></td>
</tr>
</tbody>
</table>

[Le_Port_de_Brest_(une_prise_de_la_mâture)-Louis-Nicolas_Van_Blarenberghe_mg_8234.jpg](https://zh.wikipedia.org/wiki/File:Le_Port_de_Brest_\(une_prise_de_la_mâture\)-Louis-Nicolas_Van_Blarenberghe_mg_8234.jpg "fig:Le_Port_de_Brest_(une_prise_de_la_mâture)-Louis-Nicolas_Van_Blarenberghe_mg_8234.jpg")
**布雷斯特**（[法语](../Page/法语.md "wikilink")：**Brest**）位于[法国](../Page/法国.md "wikilink")[布列塔尼半岛西端](../Page/布列塔尼.md "wikilink")，是[布列塔尼大区](../Page/布列塔尼大区.md "wikilink")[菲尼斯泰尔省的城市](../Page/菲尼斯泰尔省.md "wikilink")，也是重要的[港口和](../Page/港口.md "wikilink")[海军基地](../Page/海军基地.md "wikilink")。

位居半島尖端、濱海的布雷斯特是早年法國與[美洲之間最主要的吞吐港](../Page/美洲.md "wikilink")，並進而興盛了當地的[造船工業](../Page/造船.md "wikilink")。具有官方色彩的[船舶建造局](../Page/法國船舶建造局.md "wikilink")（Direction
des constructions
navales）就在此地設有造船廠，生產過大小各級軍艦與潛艇，包括目前[法國海軍的旗艦](../Page/法國海軍.md "wikilink")[夏尔·戴高乐号航空母舰](../Page/戴高乐号航空母舰.md "wikilink")。

## 歷史

1240年之前，關於布雷斯特的準確文獻記載數量極少。1342年，John de
Montfort將布雷斯特交給了英國，自此此地留在英國人手中直至1397年。中世紀時布雷斯特地位十分重要，以至於有“非布雷斯特領主者算不上真正的布列塔尼大公”的說法。

布雷斯特的海港優勢首先由黎塞留樞機認識到。他於1631年在此興建了木埠港口，該港很快就成為了法國海軍基地。

二戰時，德軍在布雷斯特設有大型潛艇基地。1944年夏，此市於[布雷斯特戰役幾乎盡毀於兵燹](../Page/布雷斯特戰役.md "wikilink")（僅三棟建築未坍）。戰後，西德政府向無家可歸的布雷斯特市民支付了數十億美元的賠款。

1972年，法國在布雷斯特港錨地的Île Longue(中文直譯為“長島”)設立了核潛艇防禦基地。

## 交通

[布雷斯特站是法国本土最西端的铁路车站](../Page/布雷斯特站.md "wikilink")，每天开行多趟直达巴黎的[TGV列车](../Page/TGV.md "wikilink")。

## 名勝古跡

布雷斯特因光復橋（一架大型開合橋）、軍火庫和暹羅街而聞名。布雷斯特城堡和唐居伊塔（Tour Tanguy）是布雷斯特最古老的標誌性建築。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a><a href="../Page/加的斯.md" title="wikilink">加的斯</a></p></li>
<li><p><a href="../Page/罗马尼亚.md" title="wikilink">罗马尼亚</a><a href="../Page/康斯坦察.md" title="wikilink">康斯坦察</a></p></li>
<li><p><a href="../Page/美国.md" title="wikilink">美国</a><a href="../Page/丹佛_(科罗拉多州).md" title="wikilink">丹佛</a></p></li>
<li><p><a href="../Page/爱尔兰.md" title="wikilink">爱尔兰</a><a href="../Page/Dún_Laoghaire.md" title="wikilink">Dún Laoghaire</a></p></li>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/基尔.md" title="wikilink">基尔</a></p></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><p><a href="../Page/英格兰.md" title="wikilink">英格兰</a><a href="../Page/普利茅斯.md" title="wikilink">普利茅斯</a></p></li>
<li><p><a href="../Page/布基纳法索.md" title="wikilink">布基纳法索</a><a href="../Page/Saponé.md" title="wikilink">Saponé</a></p></li>
<li><p><a href="../Page/意大利.md" title="wikilink">意大利</a><a href="../Page/塔兰托.md" title="wikilink">塔兰托</a></p></li>
<li><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/横须贺.md" title="wikilink">横须贺</a></p></li>
<li><p><a href="../Page/中国.md" title="wikilink">中国</a><a href="../Page/青岛.md" title="wikilink">青岛</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

[B](../Category/菲尼斯泰尔省市镇.md "wikilink")