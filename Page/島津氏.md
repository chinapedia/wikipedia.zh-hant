**島津氏**是日本的[氏族之一](../Page/氏族.md "wikilink")。在[鎌倉時代到](../Page/鎌倉時代.md "wikilink")[江戶時代期間是](../Page/江戶時代.md "wikilink")[大名](../Page/大名_\(稱謂\).md "wikilink")，另外其家族亦有不少分支。

## 概要

從[守護職成為](../Page/守護.md "wikilink")[戰國大名](../Page/戰國大名.md "wikilink")，也是[江戶時代的](../Page/江戶時代.md "wikilink")[外樣大名](../Page/外樣大名.md "wikilink")（[薩摩藩藩主](../Page/薩摩藩.md "wikilink")）。家族全盛期時以[薩摩國為中心統治](../Page/薩摩國.md "wikilink")[南九州](../Page/南九州.md "wikilink")。初代當主[島津忠久是](../Page/島津忠久.md "wikilink")[薩摩國](../Page/薩摩國.md "wikilink")、[大隅国及](../Page/大隅国.md "wikilink")[日向国](../Page/日向国.md "wikilink")3国[守護](../Page/守護.md "wikilink")、亦出任[越前国守護](../Page/越前国.md "wikilink")。鎌倉時代起家族開始發展，在[越前](../Page/越前国.md "wikilink")、[信濃](../Page/信濃国.md "wikilink")、[駿河](../Page/駿河國.md "wikilink")、[若狹及](../Page/若狹國.md "wikilink")[近江出現了島津氏的支流](../Page/近江国.md "wikilink")，他們被稱為[越前島津氏](../Page/越前島津氏.md "wikilink")、[信濃島津氏](../Page/信濃島津氏.md "wikilink")、[河州島津氏](../Page/河州島津氏.md "wikilink")、[若狹島津氏及](../Page/若狹島津氏.md "wikilink")[江州島津氏](../Page/江州島津氏.md "wikilink")。

此外，條目以後所記述的戰國時代的宗家是一個庶流[伊作家](../Page/伊作家.md "wikilink")（他們同樣是[相州家的當主](../Page/相州家.md "wikilink")），他們成為了當主。江戶時代薩摩藩主的島津氏的伊作家（相州家）的系統。

## 出自：島津莊的莊官

[KoyaSatsumaShimazuKeNoHaka.jpg](https://zh.wikipedia.org/wiki/File:KoyaSatsumaShimazuKeNoHaka.jpg "fig:KoyaSatsumaShimazuKeNoHaka.jpg")
島津氏的祖先是[秦氏子孫](../Page/日本秦氏.md "wikilink")[惟宗氏後代](../Page/惟宗氏.md "wikilink")。[惟宗基言之子](../Page/惟宗基言.md "wikilink")[惟宗廣言的主君是](../Page/惟宗廣言.md "wikilink")[藤原](../Page/藤原氏.md "wikilink")[攝關家筆頭](../Page/攝關家.md "wikilink")[近衛家日向國島津莊](../Page/近衛家.md "wikilink")（現[宮崎縣](../Page/宮崎縣.md "wikilink")[都城市](../Page/都城市.md "wikilink")）的[莊官](../Page/莊官.md "wikilink")（下司）前往九州、兒子[惟宗忠久在](../Page/島津忠久.md "wikilink")[源賴朝時期出任該地](../Page/源賴朝.md "wikilink")[地頭時開始稱為島津氏](../Page/地頭.md "wikilink")，都城市被說是島津家的發源地。

### 被源賴朝重用背景

到底惟宗忠久（島津忠久）是否為惟宗廣言的兒子則無從考究。另外關於「[攝津](../Page/攝津國.md "wikilink")[大阪](../Page/大阪.md "wikilink")[住吉大社境內誕下忠久的](../Page/住吉大社.md "wikilink")[丹後局是](../Page/丹後局.md "wikilink")[源賴朝側室](../Page/源賴朝.md "wikilink")，忠久是賴朝私生子」在『島津国史』及『島津氏正統系圖』等資料記亦有所記載，學會對於島津氏是賴朝的私生子的說法是否定的。
現在關於島津氏忠久時期以前家譜沒有明確的家譜。

同樣是九州[守護一職的](../Page/守護.md "wikilink")[大友能直](../Page/大友能直.md "wikilink")，他與島津忠久的共通點是後來被稱為九州一族的祖先，他們的出自均自不明，並傳出是「母親是賴朝的妻妾，受到賴朝的擁戴」。正確來說，忠久是源自惟宗氏，能直並非出自[近藤氏](../Page/近藤氏.md "wikilink")。

## 南北朝時代

[鎌倉時間開始](../Page/鎌倉幕府.md "wikilink")，當主以幕府[御家人身份在鎌倉居住](../Page/御家人.md "wikilink")，由守護職及地頭職等一族人士負責開發領土。[久經時期](../Page/島津久經.md "wikilink")，[元軍入侵日本](../Page/元朝.md "wikilink")，氏族的居地有向南遷的趨勢。

1333年（[元弘](../Page/元弘.md "wikilink")3年）[島津貞久參與由](../Page/島津貞久.md "wikilink")[後醍醐天皇發動的鎌倉幕府討幕運動](../Page/後醍醐天皇.md "wikilink")。鎌倉幕府滅亡後後醍醐天皇開始實行[建武新政](../Page/建武新政.md "wikilink")，後醍醐親政謀反的[足利尊氏在攝津國戰敗而逃往九州](../Page/足利尊氏.md "wikilink")，與少貳氏共同協助足利尊氏，在筑前国的[多多良濱](../Page/多多良濱之戰.md "wikilink")(今福岡縣福岡市)與[菊池氏率領後醍醐宮方軍交戰](../Page/菊池氏.md "wikilink")。

1342年，南朝的征西將軍[懷良親王進入](../Page/懷良親王.md "wikilink")[南九州](../Page/南九州.md "wikilink")，有一段時間成為了南朝派的人物。1340年貞久嫡男[島津宗久過身後](../Page/島津宗久.md "wikilink")，與守護国的[島津氏久](../Page/島津氏久.md "wikilink")（[奧州家](../Page/奧州家.md "wikilink")）及[島津師久](../Page/島津師久.md "wikilink")（[總州家](../Page/總州家.md "wikilink")）兩家分裂。

## 戰國時代

進入戰國時代後，與領土內各地的[国人及其他島津一族展開了鬥爭](../Page/国人.md "wikilink")，宗家因而逐漸衰落。庶流[伊作忠良](../Page/伊作忠良.md "wikilink")（[伊作島津家](../Page/伊作家.md "wikilink")）勢力漸漸增大，壓倒了其他家系，長男[島津貴久以宗家的養子成為島津家家督](../Page/島津貴久.md "wikilink")。貴久之後將家督讓給長男[島津義久](../Page/島津義久.md "wikilink")，1578年[耳川之戰當中大敗](../Page/耳川之戰.md "wikilink")[大友氏](../Page/大友氏.md "wikilink")，加上1584年[沖田畷之戰重創](../Page/沖田畷之戰.md "wikilink")[龍造寺氏](../Page/龍造寺氏.md "wikilink")、島津氏成為了「三州的太守」。此外，當時義久還擁有三個優秀弟弟（[島津義弘](../Page/島津義弘.md "wikilink")、[歲久及](../Page/島津歲久.md "wikilink")[家久](../Page/島津家久.md "wikilink")），因而被稱為戰國最強的薩摩武士，他們嘗試率領大軍統一九州南部三州及九州。1587年[秀吉發動](../Page/豐臣秀吉.md "wikilink")[九州戰役](../Page/九州征伐.md "wikilink")，島津氏向秀吉投降，最終保住了三州的領土。

## 江戶時期以後

[關原之戰爆發期間](../Page/關原之戰.md "wikilink")，島津家支持西軍，與德川家陷入敵對關係。戰後於義弘退位下，成功保住了原有領土。江戶時代早期島津家入侵[琉球](../Page/琉球.md "wikilink")，佔領[奄美諸島](../Page/奄美諸島.md "wikilink")，並支配琉球王朝。[幕藩體制之下在](../Page/幕藩體制.md "wikilink")[寶曆治水當中是被幕府的弱體化政策受壓的一方](../Page/寶曆治水事件.md "wikilink")。[德川綱吉養女](../Page/德川綱吉.md "wikilink")[竹姫為](../Page/淨岸院.md "wikilink")[島津繼豐的後妻之後](../Page/島津繼豐.md "wikilink")，與[德川氏女兒進行婚姻](../Page/德川氏.md "wikilink")，因而加深了與幕府的關係。幸運的是，他們的當家相當長命，並沒出現子嗣繼承問題需要幕府介入，因此與幕府的關係比較穩定。

踏入[幕末](../Page/幕末.md "wikilink")，對抗西方帝國主義情緒高漲，第28代當家[島津齊彬時期組成了以西式製鐵](../Page/島津齊彬.md "wikilink")、造船、紡織為中心的近代產業（[集成館事業](../Page/集成館事業.md "wikilink")），開始出現了新的發展。另外幕府進入了崩壞期，薩摩藩內出現不少尊皇倒幕的人物，他們與長洲藩共同為主要倒幕力量。明治時代[島津忠義本家與父親](../Page/島津忠義.md "wikilink")[島津久光在明治維新過後各自分成](../Page/島津久光.md "wikilink")[玉里家共兩家](../Page/玉里家.md "wikilink")，被授予[公爵](../Page/公爵.md "wikilink")。其他有實力的分家包括了[昭和天皇第五皇女子](../Page/昭和天皇.md "wikilink")[清宮貴子內親王降嫁的日向佐土原島津家](../Page/島津貴子.md "wikilink")（幕末時是領有2万7千石[伯爵](../Page/伯爵.md "wikilink")）。其餘的被任命為[男爵](../Page/男爵.md "wikilink")。

[今上天皇的外祖母是島津忠義七女](../Page/明仁.md "wikilink")[島津俔子](../Page/邦彥王妃俔子.md "wikilink")，因此現時的島津氏一門與[皇室有血緣關係](../Page/日本皇室.md "wikilink")。[今上天皇的妹妹](../Page/今上天皇.md "wikilink")[貴子內親王嫁給](../Page/貴子內親王.md "wikilink")[島津久永](../Page/島津久永.md "wikilink")。兒子[島津禎久島津家與皇室有著根深蒂固的親戚關係](../Page/島津禎久.md "wikilink")

## 系譜

粗體字是當主、實線是親子、虛線是養子
       　　　

## 一族

数字是当主繼承先後次序

1.  [島津忠久](../Page/島津忠久.md "wikilink")
2.  [島津忠時](../Page/島津忠時.md "wikilink")
3.  [島津久經](../Page/島津久經.md "wikilink")
4.  [島津忠宗](../Page/島津忠宗.md "wikilink")
5.  [島津貞久](../Page/島津貞久.md "wikilink")
6.  [島津師久](../Page/島津師久.md "wikilink")‐貞久三男、統治[薩摩國](../Page/薩摩國.md "wikilink")。
7.  [島津氏久](../Page/島津氏久.md "wikilink")‐貞久四男、統治[大隅国](../Page/大隅国.md "wikilink")。
8.  [島津伊久](../Page/島津伊久.md "wikilink")‐師久長男、統治[薩摩國](../Page/薩摩國.md "wikilink")。
9.  [島津元久](../Page/島津元久.md "wikilink")‐氏久長男、統治[大隅国](../Page/大隅国.md "wikilink")。
10. [島津久豐](../Page/島津久豐.md "wikilink")‐氏久二男。
11. [島津忠國](../Page/島津忠國.md "wikilink")
12. [島津立久](../Page/島津立久.md "wikilink")
13. [島津忠昌](../Page/島津忠昌.md "wikilink")
14. [島津忠治](../Page/島津忠治.md "wikilink")‐忠昌長男。
15. [島津忠隆](../Page/島津忠隆.md "wikilink")‐忠昌二男。
16. [島津勝久](../Page/島津勝久.md "wikilink")‐忠昌三男。
17. [島津貴久](../Page/島津貴久.md "wikilink")‐勝久養子。[島津忠良嫡子](../Page/島津忠良.md "wikilink")。
18. [島津義久](../Page/島津義久.md "wikilink")
19. [島津義弘](../Page/島津義弘.md "wikilink")
20. [島津忠恒](../Page/島津忠恒.md "wikilink")‐義久養子。義弘三男。
21. [島津光久](../Page/島津光久.md "wikilink")
22. [島津綱貴](../Page/島津綱貴.md "wikilink")
23. [島津吉貴](../Page/島津吉貴.md "wikilink")
24. [島津繼豐](../Page/島津繼豐.md "wikilink")
25. [島津宗信](../Page/島津宗信.md "wikilink")
26. [島津重年](../Page/島津重年.md "wikilink")‐繼豐次男、宗信弟。
27. [島津重豪](../Page/島津重豪.md "wikilink")
28. [島津齊宣](../Page/島津齊宣.md "wikilink")
29. [島津齊興](../Page/島津齊興.md "wikilink")
30. [島津齊彬](../Page/島津齊彬.md "wikilink")‐齊興長男。
31. [島津久光](../Page/島津久光.md "wikilink")‐齊興三男。
32. [島津忠義](../Page/島津忠義.md "wikilink")‐久光長男。
33. [島津忠重](../Page/島津忠重.md "wikilink")

## 島津氏族

島津氏的系統以下多個。現時的島津宗家是伊作家出身的。

  - 庶流（江戶時代以前）
      - [越前家](../Page/越前島津氏.md "wikilink")
      - [伊作家](../Page/伊作家.md "wikilink")
      - [奧州家](../Page/奧州家.md "wikilink")
      - [薩州家](../Page/薩州家.md "wikilink")
      - [總州家](../Page/總州家.md "wikilink")
      - [相州家](../Page/相州家.md "wikilink")
      - [豐州家](../Page/豐州家.md "wikilink")

<!-- end list -->

  - 庶流（江戶時期以後）
      - [加治木家](../Page/加治木家.md "wikilink")

      - [垂水家](../Page/垂水家.md "wikilink")

      - [重富家](../Page/重富家.md "wikilink")

      - [玉里家](../Page/玉里島津家.md "wikilink")

      - [今和泉家](../Page/今和泉家.md "wikilink")

      - [宮之城家](../Page/宮之城家.md "wikilink")

      - [永吉家](../Page/永吉家.md "wikilink")

      - [日置家](../Page/日置家.md "wikilink")

      -
<!-- end list -->

  - 庶家
      - [伊集院氏](../Page/伊集院氏.md "wikilink")
      - [樺山氏](../Page/樺山氏.md "wikilink")
      - [川上氏](../Page/川上氏.md "wikilink")
      - [北鄉氏](../Page/北鄉氏.md "wikilink")
      - [新納氏](../Page/新納氏.md "wikilink")
      - [町田氏](../Page/町田氏.md "wikilink")
      - [山田氏](../Page/山田氏.md "wikilink")
      - [和泉氏](../Page/和泉氏.md "wikilink")
      - [義岡氏](../Page/義岡氏.md "wikilink")
      - [桂氏](../Page/桂氏.md "wikilink")
      - [喜入氏](../Page/喜入氏.md "wikilink")
      - [佐多氏](../Page/佐多氏.md "wikilink")
      - [入來院氏](../Page/入來院氏.md "wikilink")

## 正式簽名所見的「姓」

官方文書簽名是鎌倉時代初期的「惟宗朝臣○○」，由德川家賜姓「[松平](../Page/松平氏.md "wikilink")」名字改用「松平朝臣○○」而簽署。自此以後，幕府公式文書等以「松平<u>薩摩守</u>（有變化）○○」作書寫。此外江戶時代中期以後、内部公式文書等就以「源朝臣○○」作簽名\[1\]。

## 相關書籍

  - 『』（東京大学史料編纂所）
  - 『』（[人物往來社戦国史叢書](../Page/新人物往來社.md "wikilink")6・1966年）
  - 三木靖『薩摩島津氏』（人物往来社戦国史叢書10・1972年）
  - 『』（[学研歴史群像シリーズ](../Page/學習研究社.md "wikilink")12・1989年6月） ISBN
    4051051498
  - 『』（学研歴史群像シリーズ戦国セレクション6・2001年8月） ISBN 4056025959
  - 吉永正春『』（海鳥社・2006年7月）ISBN 4874155863

## 其他參考資料

  - [戰國大名興亡史──島津氏](https://web.archive.org/web/20071026001549/http://se-n-go-ku.org/topic02-04.html)

## 參看

  - [秦氏](../Page/秦氏.md "wikilink")

## 註釋

## 外部連結

  - [島津氏系譜](http://www2.harimaya.com/sengoku/html/simazu.html)

[島津氏](../Category/島津氏.md "wikilink")
[Category:九州地方姓氏](../Category/九州地方姓氏.md "wikilink")
[Category:薩摩國](../Category/薩摩國.md "wikilink")
[Category:鹿兒島市](../Category/鹿兒島市.md "wikilink")
[Category:日本氏族](../Category/日本氏族.md "wikilink")

1.  不過「源」的簽名使用方式是[室町時代中期左是右藤原姓與平行相等](../Page/室町時代.md "wikilink")。