[Tomdickson.jpg](https://zh.wikipedia.org/wiki/File:Tomdickson.jpg "fig:Tomdickson.jpg")
***Will It
Blend?***是一系列30至60秒展示[Blendtec產品線攪拌機能力的](../Page/Blendtec.md "wikilink")[資訊型廣告](../Page/資訊型廣告.md "wikilink")，目標為使用Blendtec攪拌機攪碎不同物件。該節目由Blendtec的創辦人、行政總裁兼總工程師湯姆·狄克生（Tom
Dickson）主持\[1\]。由於正常家庭都不會拿示範中的東西來攪碎（如玩具、掌上電腦等），在展示攪拌機性能的一方更多程度上是以被攪碎物的荒唐性吸引大眾的注意，因此*Will
It
Blend?*被視為[病毒式行銷的一種](../Page/病毒营销.md "wikilink")。儘管許多人並不贊同Blendtec這種形式的廣告，可是自該系列廣告推出後，公司的產品銷量便激增500%。\[2\]

## 簡介

Blendtec股份有限公司與湯姆·狄克生決定展示他們的攪拌機，而方法就是拍攝一系列資訊型廣告放到[YouTube與](../Page/YouTube.md "wikilink")*Will
It Blend?*網站。

## 結構

該節目通常會以湯姆·狄克生說「Will it Blend? That is the
question.」（它將會攪碎嗎？這是個問題。）作為開場，一段短促的音樂後湯姆會以一個「背景故事」帶出接下來要攪碎的物品，該物品會放入攪拌機內然後以特定的攪拌設定開始進行攪拌，同時會出現字幕說明該物品是否适合在家中自行嘗試攪拌。例如[立方氧化鋯](../Page/立方氧化鋯.md "wikilink")、[高爾夫球與](../Page/高爾夫球.md "wikilink")[彈珠是不安全的](../Page/彈珠.md "wikilink")，字幕会提示“不要在家中尝试。”另一方面，24張[信用卡](../Page/信用卡.md "wikilink")\[3\]、[牛油果](../Page/鱷梨.md "wikilink")、冰塊與[麥當勞](../Page/麥當勞.md "wikilink")[巨無霸超值套餐](../Page/巨無霸.md "wikilink")\[4\]是安全的，这时字幕会提示“请在家中尝试。”通常他会在说“我将要按「物品」按钮”后启动搅拌机。在搅拌的过程中湯姆·狄克生会微笑着等待。最後攪碎的物品會放到一張厚紙板或玻璃杯上，有些物品攪碎後的灰塵可能會有害健康時，湯姆·狄克生還會提醒不要去聞它，最後通常會以「YES,
IT BLENDS\!」（是的，這玩意兒能被攪碎！）作為結尾。

### 支持者的等待

該系列的支持者都在等待攪碎另一個攪拌器或一支[鐵撬](../Page/鐵撬.md "wikilink")，這兩樣物品（特別是鐵撬）很可能永遠不會嘗試在節目中進行攪拌；Will
It
Blend?網站上有一套看起來要攪碎鐵撬的實驗視頻，但實際上影片在鐵橇開始攪拌前被手提電話的響聲打斷，而狄克生以拿走鐵橇改為在攪拌機中放滿手提電話並攪碎它們作為回應。\[5\]

### 失敗的Blend

除了上文的鐵撬外，湯姆·狄克生曾經公開嘗試把一個[耙攪碎](../Page/耙.md "wikilink")，但是耙的手柄依然沒有被攪碎，弄得狄克生十分尷尬\[6\]。而在[福特Fiesta一集中](../Page/福特Fiesta.md "wikilink")，嘗試把[硼](../Page/硼.md "wikilink")[鋼攪碎](../Page/鋼.md "wikilink")，但硼鋼仍完好無缺\[7\]。

## 部份曾經被攪碎的物品

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>高爾夫球棒的手柄</li>
<li><a href="../Page/高爾夫球.md" title="wikilink">高爾夫球</a></li>
<li><a href="../Page/棒球.md" title="wikilink">棒球</a></li>
<li>50顆<a href="../Page/彈珠.md" title="wikilink">彈珠</a></li>
<li>玩具車</li>
<li>12支<a href="../Page/螢光棒.md" title="wikilink">螢光棒</a></li>
<li>耙的手柄</li>
<li><a href="../Page/人造鑽石.md" title="wikilink">人造鑽石</a>（<a href="../Page/立方氧化鋯.md" title="wikilink">立方氧化鋯</a>）</li>
<li><a href="../Page/攝影機.md" title="wikilink">攝影機</a> [8]</li>
<li><a href="../Page/Chrome_Notebook.md" title="wikilink">Chrome Notebook</a>(Cr-48)</li>
<li><a href="../Page/三星_Galaxy_S_III.md" title="wikilink">三星 Galaxy S III</a></li>
<li><a href="../Page/三星_Galaxy_S_IV_Active.md" title="wikilink">三星 Galaxy S IV Active</a></li>
<li><a href="../Page/微軟.md" title="wikilink">微軟</a><a href="../Page/Kinect.md" title="wikilink">Kinect</a></li>
<li><a href="../Page/Amazon_Kindle_Fire.md" title="wikilink">Amazon Kindle Fire HD</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/手機.md" title="wikilink">手機</a></li>
<li>多張<a href="../Page/信用卡.md" title="wikilink">信用卡</a></li>
<li><a href="../Page/嗚嗚祖拉.md" title="wikilink">嗚嗚祖拉</a>（Vuvuzela）</li>
<li><a href="../Page/iPod.md" title="wikilink">iPod</a></li>
<li><a href="../Page/iPhone.md" title="wikilink">iPhone</a>(2G, 3G, 4, 4S, 5, 5C, 5S, 6 plus, SE) [9][10]</li>
<li><a href="../Page/足球.md" title="wikilink">足球</a></li>
<li><a href="../Page/磁石.md" title="wikilink">磁石</a></li>
<li>伸縮拉尺</li>
<li><a href="../Page/燈泡.md" title="wikilink">燈泡</a></li>
<li><a href="../Page/錄音帶.md" title="wikilink">錄音帶</a></li>
<li>40支<a href="../Page/原子筆.md" title="wikilink">原子筆</a></li>
<li>《<a href="../Page/吸血新世紀.md" title="wikilink">吸血新世紀</a>》影碟及相關產品</li>
<li><a href="../Page/Google_Nexus_7.md" title="wikilink">Google Nexus 7</a></li>
<li><a href="../Page/Nokia_3310.md" title="wikilink">Nokia 3310</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/非洲鯽.md" title="wikilink">非洲鯽</a></li>
<li><a href="../Page/美式足球.md" title="wikilink">美式足球</a></li>
<li>一堆<a href="../Page/化妝品.md" title="wikilink">化妝品</a></li>
<li><a href="../Page/生蠔.md" title="wikilink">生蠔</a>（連殼）</li>
<li>可樂雞(Cochicken) （<a href="../Page/可樂.md" title="wikilink">可樂和</a><a href="../Page/雞肉.md" title="wikilink">雞肉的混合</a>）</li>
<li><a href="../Page/午餐肉.md" title="wikilink">午餐肉</a>（連罐）</li>
<li><a href="../Page/打火機.md" title="wikilink">打火機</a></li>
<li><a href="../Page/魔方.md" title="wikilink">魔術方塊</a></li>
<li><a href="../Page/iPad.md" title="wikilink">iPad</a>[11]、<a href="../Page/iPad_2.md" title="wikilink">iPad 2</a>、<a href="../Page/iPad_mini.md" title="wikilink">iPad mini</a></li>
<li><a href="../Page/小賈斯汀.md" title="wikilink">小賈斯汀</a>（Justin Bieber）的相關產品[12]</li>
<li><a href="../Page/鍵盤.md" title="wikilink">鍵盤</a></li>
<li><a href="../Page/冰.md" title="wikilink">冰</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 《[Minecraft](../Page/Minecraft.md "wikilink")》中的Steve
  - 《[冰雪奇缘](../Page/冰雪奇缘.md "wikilink")》相关产品
  - [XBox 360 Kinect](../Page/XBox_360_Kinect.md "wikilink")

## YouTube上的成功

2006年11月初期數集*Will It
Blend?*於[YouTube上刊登後立即成為焦點](../Page/YouTube.md "wikilink")，高爾夫球一集擁有超過1,500萬次點擊率，其他較舊的數集分別擁有30萬
– 50萬次點擊率。\[13\]

2006年聖誕期間Blendtec因攪拌iPod的一集*Will It
Blend?*\[14\]再次登上YouTube首頁，被攪碎的iPod放到拍賣網站eBay用作慈善拍賣。\[15\]

## 參考

## 外部連結

  - [*Will It Blend?*官方網站](http://www.willitblend.com) – 包括各集*Will It
    Blend?*、*Will It Blog?*網誌與一張希望在未來集數攪碎物品的申請表格。

[Category:網路文化](../Category/網路文化.md "wikilink")

1.  [Blendtec.com](http://www.blendtec.com/aboutus.aspx)
2.  [Will it blend ? - Interview
    VO](http://www.onirik.net/spip.php?article3859)
3.
4.
5.  [Will It
    Blend?網站上的攪拌鐵撬視頻](http://www.willitblend.com/videos/crowbar)
6.  [Blendtec Blendtec Will It Blend? - Sometimes It
    Won't](https://www.youtube.com/watch?v=6a78-aqY_FU)
7.  [1](https://www.youtube.com/watch?v=ulFAT66wtXU)
8.  [2](http://www.youtube.com/watch?v=fY8MqWBIHvo)
9.
10. [3](http://hk.apple.nextmedia.com/news/art/20131004/18449786)
11. [4](http://www.youtube.com/watch?v=lAl28d6tbko)
12.
13. [Youtube.com](http://www.youtube.com/profile_videos?user=Blendtec)
14. [Will It Blend?網站上的攪拌ipod視頻](http://www.willitblend.com/videos/ipod)
15. [Ebay.com](http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&item=300061186098)