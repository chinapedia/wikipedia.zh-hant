**金正鉉**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。2009年11月7日，與小7歲的記者女友金宥珠結婚。

## 演出作品

### 電視劇

  - 1995年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[沙漏](../Page/沙漏_\(電視劇\).md "wikilink")》
  - 1997年：[KBS](../Page/韓國放送公社.md "wikilink")《[奔跑](../Page/奔跑_\(電視劇\).md "wikilink")》
  - 1998年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[六个孩子](../Page/六个孩子.md "wikilink")》飾演
    金宗哲
  - 1999年：SBS《[KAIST](../Page/KAIST_\(電視劇\).md "wikilink")》
  - 1999年：SBS《[現在戀愛的時候](../Page/現在戀愛的時候.md "wikilink")》
  - 2000年：KBS《[天定情緣](../Page/天定情緣.md "wikilink")》
  - 2001年：SBS《[沒有離別的清晨](../Page/沒有離別的清晨.md "wikilink")》
  - 2003年：MBC《[男人的香氣](../Page/男人的香氣.md "wikilink")》
  - 2003年：MBC《[我要跑](../Page/我要跑.md "wikilink")》
  - 2004年：SBS《[大小姐們](../Page/大小姐們.md "wikilink")》
  - 2004年：SBS《[洪所長的秋天](../Page/洪所長的秋天.md "wikilink")》
  - 2006年：SBS《[我也要去](../Page/我也要去.md "wikilink")》飾演 尚民
  - 2006年：KBS《[大祚榮](../Page/大祚榮_\(電視劇\).md "wikilink")》飾演 미모사
  - 2006年：KBS《[白雲階梯](../Page/白雲階梯.md "wikilink")》飾演 金道憲
  - 2008年：KBS《[媽媽發怒了](../Page/媽媽發怒了.md "wikilink")》飾演 羅英一
  - 2009年：MBC《[善德女王](../Page/善德女王_\(韓國電視劇\).md "wikilink")》飾演 夏宗
  - 2010年：SBS《[Giant](../Page/巨人_\(韓國電視劇\).md "wikilink")》飾演 黃政植
  - 2011年：KBS《[廣開土太王](../Page/廣開土太王_\(電視劇\).md "wikilink")》飾演 乭飛守
  - 2011年：MBC《[危險的女人](../Page/危險的女人.md "wikilink")》飾演 金志元
  - 2012年：SBS《[我的愛蝴蝶夫人](../Page/我的愛蝴蝶夫人.md "wikilink")》飾演 金燦基
  - 2013年：MBC《[奇皇后](../Page/奇皇后_\(電視劇\).md "wikilink")》飾演 唐其勢
  - 2015年：SBS《[媽媽我媳婦](../Page/媽媽我媳婦.md "wikilink")》飾演 張泰誠
  - 2019年：SBS《[VAGABOND](../Page/VAGABOND_\(電視劇\).md "wikilink")》飾演 洪勝範

### 電影

  - 1987年：《[The Secret Diary](../Page/The_Secret_Diary.md "wikilink")》
  - 1994年：《[荷里活小子](../Page/荷里活小子.md "wikilink")》
  - 1996年：《[Come to me](../Page/Come_to_me.md "wikilink")》
  - 1997年：《[He Asked Me If I Knew
    Zither](../Page/He_Asked_Me_If_I_Knew_Zither.md "wikilink")》
  - 2000年：《[青春](../Page/青春_\(電影\).md "wikilink")》
  - 2000年：《[Interview](../Page/Interview.md "wikilink")》
  - 2002年：《Plastic Tree》

### MV

  - 2011年：Namolla Family JW《我們分手吧》

## 外部連結

  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%EA%B9%80%EC%A0%95%ED%98%84&sm=tab_txc&ie=utf8&key=PeopleService&os=96346)

[K](../Category/韓國電視演員.md "wikilink")
[K](../Category/韓國電影演員.md "wikilink")
[K](../Category/首爾藝術大學校友.md "wikilink")
[J](../Category/金姓.md "wikilink")