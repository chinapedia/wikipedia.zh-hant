**侯活·梅爾頓·韋比**（，）\[1\]是一名[英格蘭](../Page/英格蘭.md "wikilink")[職業](../Page/職業.md "wikilink")[足球](../Page/足球.md "wikilink")[球證](../Page/球證.md "wikilink")，現在主要執法[英格蘭超級聯賽賽事](../Page/英格蘭超級聯賽.md "wikilink")，自2005年起他亦是一名國際球證。他居住在[南約克郡](../Page/南約克郡.md "wikilink")[羅瑟漢姆](../Page/羅瑟漢姆.md "wikilink")。\[2\]韦伯被足球界与媒体公认为史上少有的高水平裁判员。

2014年8月6日，英超官网宣布霍华德·韦伯宣布从裁判界退役，正式结束了长达25年的裁判生涯。\[3\]

## 執法生涯

韋比於1989年首次於羅瑟漢姆地區聯賽開始擔任球證，1993年開始擔任北部聯賽的助理裁判，兩年後升為主球證。1996年為[英格蘭足球聯賽擔任助理裁判](../Page/英格蘭足球聯賽.md "wikilink")，在1998年提拔為[足球議會聯賽的球證](../Page/英格蘭足球議會聯賽.md "wikilink")\[4\]。

2000年他進入了[英格蘭足球聯賽球證名單](../Page/英格蘭足球聯賽.md "wikilink")，可以執法較高級別的賽事，3年後再被提升入[英超球證名單](../Page/英超.md "wikilink")，可以執法英格蘭最高級別聯賽－[英格蘭超級聯賽](../Page/英格蘭超級聯賽.md "wikilink")\[5\]。2003年10月18日，他首次執法英超賽事，該場[富咸對](../Page/富咸足球會.md "wikilink")[狼隊的賽事以](../Page/狼隊.md "wikilink")0-0打平，共發出4面黃牌\[6\]。2005年，他被[國際足協認可為國際球證](../Page/國際足協.md "wikilink")，可以執法世界大型賽事\[7\]
。

韋伯首個執法的國際賽是於2005年11月舉行的友誼賽，[北愛爾蘭對](../Page/北愛爾蘭.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")，賽果為1-1。隨後執法了2006年[歐洲U-21足球錦標賽兩場分組賽和一場準決賽](../Page/歐洲U-21足球錦標賽.md "wikilink")。同年9月26日亦執法了他首場[歐洲冠軍聯賽](../Page/歐洲冠軍聯賽.md "wikilink")，賽事為[布加勒斯特星隊對](../Page/布加勒斯特星足球俱樂部.md "wikilink")[里昂](../Page/奧林匹克里昂.md "wikilink")。在2007年的英格蘭聯賽盃決賽中，由韋比執法，為[阿森納對](../Page/兵工廠足球俱樂部.md "wikilink")[切爾西](../Page/切爾西足球俱樂部.md "wikilink")，最終切爾西以2-1擊敗阿森納。韋比在這場比賽中出示三面紅牌，是聯賽盃首次出現的\[8\]。

他亦是[2008年歐洲國家盃](../Page/2008年歐洲國家盃.md "wikilink")12位球證之一，執法主辦國[奧地利對](../Page/奧地利國家足球隊.md "wikilink")[波蘭的賽事](../Page/波蘭國家足球隊.md "wikilink")。2009年，他成為[2009年洲際國家盃的裁判](../Page/2009年洲際國家盃.md "wikilink")，執法了兩場賽事。2010年，韋比成為[當屆世界盃的主球證之一](../Page/2010年世界盃足球賽.md "wikilink")，並判法了4場賽事，當中包括[決賽](../Page/2010年世界盃足球賽決賽.md "wikilink")，乃史上首位執法過歐聯決賽及世界盃決賽的主球證。

2014年8月6日，英格兰足球超级联赛官方网站宣布，韦伯退役，从此结束25年的裁判生涯。\[9\]

## 爭議性判罰

2012年2月5日，[波亞斯帶領的](../Page/安德烈·維拉斯-波亞斯.md "wikilink")[切爾西主場迎戰曼聯](../Page/切爾西足球俱樂部.md "wikilink")，出現令人爭議的判決，曼聯在落後三球的情況下，追成3-3。弗格森表示比賽初期[卡希爾在禁區邊緣對](../Page/加里·卡希爾.md "wikilink")[維爾貝克的战术犯規時逃過一劫](../Page/丹尼·維爾貝克.md "wikilink")，而卡希爾亦承認他幸運地逃過處分\[10\]。

## 數據

### 紅黃牌紀錄

| 賽季            | 執法場次 | [Yellow_card.svg](https://zh.wikipedia.org/wiki/File:Yellow_card.svg "fig:Yellow_card.svg") 黃牌總數 (張) | [Yellow_card.svg](https://zh.wikipedia.org/wiki/File:Yellow_card.svg "fig:Yellow_card.svg") 平均每場黃牌 (張) | [Red_card.svg](https://zh.wikipedia.org/wiki/File:Red_card.svg "fig:Red_card.svg") 紅牌總數 (張) | [Red_card.svg](https://zh.wikipedia.org/wiki/File:Red_card.svg "fig:Red_card.svg") 平均每場紅牌 (張) |
| ------------- | ---- | ----------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- |
| **2000/2001** | 26   | 58                                                                                                    | *2.23*                                                                                                  | 1                                                                                            | *0.04*                                                                                         |
| **2001/2002** | 32   | 69                                                                                                    | *2.16*                                                                                                  | 5                                                                                            | *0.16*                                                                                         |
| **2002/2003** | 39   | 145                                                                                                   | *3.72*                                                                                                  | 4                                                                                            | *0.10*                                                                                         |
| **2003/2004** | 34   | 92                                                                                                    | *2.94*                                                                                                  | 9                                                                                            | *0.26*                                                                                         |
| **2004/2005** | 34   | 100                                                                                                   | *2.94*                                                                                                  | 2                                                                                            | *0.06*                                                                                         |
| **2005/2006** | 47   | 117                                                                                                   | *2.49*                                                                                                  | 7                                                                                            | *0.15*                                                                                         |
| **2006/2007** | 43   | 151                                                                                                   | *3.51*                                                                                                  | 9                                                                                            | *0.20*                                                                                         |
| **2007/2008** | 38   | 128                                                                                                   | *3.36*                                                                                                  | 2                                                                                            | *0.05*                                                                                         |
| **2008/2009** | 40   | 132                                                                                                   | *3.30*                                                                                                  | 6                                                                                            | *0.15*                                                                                         |
| **2009/2010** | 35   | 124                                                                                                   | *3.54*                                                                                                  | 4                                                                                            | *0.11*                                                                                         |
|               |      |                                                                                                       |                                                                                                         |                                                                                              |                                                                                                |

## 個人生活

韋比曾經是一名[警長](../Page/警長.md "wikilink")\[11\] 。他在2011年新年因為其在足球上的貢獻而被受勳員佐勳章
（MBE）。\[12\]\[13\]

## 參考資料

## 外部連結

  - [soccerbase.com
    韋比執法數據](https://web.archive.org/web/20090428085722/http://www.soccerbase.com/refs2.sd?refid=434)
  - [2008歐錦賽球證評分](http://www.ratetheref.net/ref/view/69/howard-webb)

[Category:英格蘭足球裁判](../Category/英格蘭足球裁判.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2014年世界盃足球賽裁判](../Category/2014年世界盃足球賽裁判.md "wikilink")
[Category:世界盃足球賽決賽裁判](../Category/世界盃足球賽決賽裁判.md "wikilink")
[Category:MBE勳銜](../Category/MBE勳銜.md "wikilink")
[Category:欧洲足球锦标赛裁判](../Category/欧洲足球锦标赛裁判.md "wikilink")
[Category:欧洲冠军联赛裁判](../Category/欧洲冠军联赛裁判.md "wikilink")
[Category:欧洲联赛裁判](../Category/欧洲联赛裁判.md "wikilink")

1.  [Birthdate and biographical
    detail](http://www.football-league.premiumtv.co.uk/page/RefereeProfilesDetail/0,,10794~628215,00.html)
    : Football League website.

2.
3.

4.  [Detailed profile](http://ynwa.tv/news/index.php/2006/November/2246)
    at the *YNWA.TV* website.

5.  [Detailed profile](http://ynwa.tv/news/index.php/2006/November/2246)
    at the *YNWA.TV* website.

6.  [韋比首場執法英超
    soccerbase.com](http://www.soccerbase.com/results3.sd?gameid=404273)

7.  [[FIFA官方網頁](../Page/FIFA.md "wikilink")](http://www.fifa.com/en/regulations/referees/singlem/0,1576,MENG,00.html)

8.  [Carling Cup
    Final](http://news.bbc.co.uk/sport1/hi/football/league_cup/6371613.stm),
    2007: Match report at BBC.co.uk website.

9.

10.

11. [2008年歐洲足球錦標賽
    傳媒資料](http://www.euro2008.uefa.com/MultimediaFiles/Download/PressConference/Competitions/MediaServices/68/51/03/685103_DOWNLOAD.pdf)


12.

13. [New Year Honours for Lennox, Suchet, Hancock and
    Webb](http://www.bbc.co.uk/news/uk-12090365)