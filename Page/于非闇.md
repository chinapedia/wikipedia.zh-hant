**-{于}-非-{闇}-**（），[满族](../Page/满族.md "wikilink")，原名-{于}-照，字非廠，别署**非-{闇}-**，又号**闲人**。生於[北京](../Page/北京.md "wikilink")，是[中华民国和](../Page/中华民国.md "wikilink")[中华人民共和国工笔花鸟画家](../Page/中华人民共和国.md "wikilink")、书法家\[1\]。

## 生平

于非闇生于北京。幼读[私塾](../Page/私塾.md "wikilink")。1908年进入满蒙高等学堂学习。1912年进入北京师范学校学习。1913年任教于北京市第二小学，同时随一位王姓民间画师学画。后来在《北京晨报》艺圃美术周刊当记者。1930年代任[古物陈列所国画研究馆导师](../Page/古物陈列所.md "wikilink")，并任教于北京师范学校、京华美专、华北大学、[北平艺专](../Page/国立北平艺术专科学校.md "wikilink")。1949年起，历任北京中国画研究会副会长、[中央美术学院民族美术研究所研究员](../Page/中央美术学院.md "wikilink")、北京市文联常务理事、[北京中国画院副院长等职务](../Page/北京画院.md "wikilink")\[2\]。

于非闇自幼受到家庭和周边环境影响，喜爱书画、诗文，种植花草，钓鱼养鸟，直到40余岁时才将精力集中到工笔花鸟画上。从[白描入手](../Page/白描.md "wikilink")，先学的是[赵子固](../Page/赵孟坚.md "wikilink")、[陈老莲的双勾花卉](../Page/陈老莲.md "wikilink")，随后上溯[宋朝](../Page/宋朝.md "wikilink")、[五代](../Page/五代.md "wikilink")，从[赵佶](../Page/赵佶.md "wikilink")[瘦金体书法中悟笔致](../Page/瘦金体.md "wikilink")。1936年在[中山公园第一次举办个人画展](../Page/北京中山公园.md "wikilink")。他为[院体画注入了新活力](../Page/院体画.md "wikilink")，通过对花鸟的观察写生，对民间绘画、[缂丝](../Page/缂丝.md "wikilink")、[刺绣的借鉴](../Page/刺绣.md "wikilink")，使其艺术不断获得新生命。1940年代后，风格更加成熟，有“南陈北-{于}-”（陈指[陈之佛](../Page/陈之佛.md "wikilink")）之称。他对当代工笔花鸟画产生了重要影响\[3\]。

## 著作

  - 《中国画颜色的研究》
  - 《都门豢鸽记》
  - 《都门艺菊记》
  - 《我怎样画工笔花鸟画》

## 参考文献

[Category:中华民国画家](../Category/中华民国画家.md "wikilink")
[Category:中华民国书法家](../Category/中华民国书法家.md "wikilink")
[Category:中华人民共和国画家](../Category/中华人民共和国画家.md "wikilink")
[Category:中华人民共和国书法家](../Category/中华人民共和国书法家.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:满族人](../Category/满族人.md "wikilink")
[F](../Category/于姓.md "wikilink")

1.

2.
3.