**湯盈盈**（****，），香港女演员，現為[香港](../Page/香港.md "wikilink")[無綫電視經理人合約女藝員](../Page/無綫電視.md "wikilink")，丈夫是香港藝人[錢嘉樂](../Page/錢嘉樂.md "wikilink")。2005年憑[無綫電視劇集](../Page/無綫電視.md "wikilink")《[阿旺新傳](../Page/阿旺新傳.md "wikilink")》[李笑好角色獲頒發](../Page/李笑好.md "wikilink")《[萬千星輝頒獎典禮2005](../Page/2005年度萬千星輝賀台慶.md "wikilink")》「[最佳女配角](../Page/萬千星輝頒獎典禮最佳女配角.md "wikilink")」
，及以配角身份勇奪全民投票《[壹電視大獎2006](../Page/2006年度壹電視大獎得獎名單.md "wikilink")》「十大電視藝人
- 第一位」。　

## 簡歷

湯盈盈祖籍廣東佛山順德\[1\]，在[加拿大](../Page/加拿大.md "wikilink")[滿地可長大](../Page/滿地可.md "wikilink")，中學畢業後入讀[協和大學](../Page/協和大學_\(加拿大\).md "wikilink")。

1994年參選加拿大蒙特利爾小姐奪冠，隨後在1995年到港參選[國際中華小姐競選](../Page/國際中華小姐競選.md "wikilink")，雖然未能奪得冠軍，但旋即與無綫電視簽約成為藝員，之後在同年加入[香港無綫電視第八期藝員進修班](../Page/香港無綫電視.md "wikilink")，同期學員還包括[姚瑩瑩和](../Page/姚瑩瑩.md "wikilink")[滕麗名](../Page/滕麗名.md "wikilink")。值得一提的是，其伯父是已故[香港道教聯合會主席](../Page/香港道教聯合會.md "wikilink")、[圓玄學院值理主席](../Page/圓玄學院.md "wikilink")[湯國華](../Page/湯國華.md "wikilink")，現任道教聯合會主席[湯偉奇是她堂兄](../Page/湯偉奇.md "wikilink")。她在家中排行老幺，有兩個哥哥，其父於飲食業經商，在加拿大擁有多間連鎖式自助餐館(其中一間是位於[蒙特利爾的](../Page/蒙特利尔.md "wikilink")
 )，家境富裕。

她在加入[香港無綫電視後](../Page/香港無綫電視.md "wikilink")，曾參與多部電視劇集的演出，但由於她的形象比較性感，所以時常被委派飾演一些[狐狸精的角色](../Page/狐狸精.md "wikilink")。直至2005年，她在《[阿旺新傳](../Page/阿旺新傳.md "wikilink")》一劇作出一個突破性的演出，飾演[李笑好](../Page/李笑好.md "wikilink")（一個臉上有很多雀斑，而且又哨牙，卻歌唱動聽的女生），大受觀眾歡迎，獲無綫電視頒發《[萬千星輝頒獎典禮2005](../Page/2005年度萬千星輝賀台慶.md "wikilink")》「[最佳女配角](../Page/萬千星輝頒獎典禮最佳女配角.md "wikilink")」
，及以配角身份獲《[壹電視大獎2006](../Page/2006年度壹電視大獎得獎名單.md "wikilink")》「十大電視藝人 -
第一位」。2007年在劇集《[兩妻時代](../Page/兩妻時代.md "wikilink")》首度擔正第一女主角。

2008年1月20日，在[灣仔](../Page/灣仔.md "wikilink")[春園街被一個無業男子](../Page/春園街.md "wikilink")[掌摑](../Page/掌摑.md "wikilink")，令湯受驚。5月5日，[疑犯被控普通襲擊罪](../Page/疑犯.md "wikilink")，判160小時[社會服務令](../Page/社會服務令.md "wikilink")。同年12月2日，湯盈盈涉嫌[醉駕傷人](../Page/醉駕.md "wikilink")。2009年3月16日，她被判160小時社會服務令，停牌一年及罰款1200港元。

另外，她是無綫電視監製[羅鎮岳的常用演員](../Page/羅鎮岳.md "wikilink")。2017年在處境劇《[愛·回家之開心速遞](../Page/愛·回家之開心速遞.md "wikilink")》再度擔正第一女主角。

## 家庭

2012年11月13日，與拍拖五年的香港男藝人[錢嘉樂結婚](../Page/錢嘉樂.md "wikilink")，註冊儀式在[香港海洋公園大熊貓園內的熊貓](../Page/香港海洋公園.md "wikilink")[盈盈](../Page/盈盈.md "wikilink")、[樂樂見證下完成](../Page/樂樂.md "wikilink")（恰巧二人名字含「樂」及「盈」字）。二人更是全港首對在熊貓館舉行婚禮的新人。\[2\]11月15日於[香港洲際酒店筵開](../Page/香港洲際酒店.md "wikilink")55席招待親友，更於席間以切蛋糕方式揭曉懷有女嬰。兄弟姐妹團陣容鼎盛，有[王祖藍](../Page/王祖藍.md "wikilink")、[謝天華](../Page/謝天華.md "wikilink")、[鄭伊健](../Page/鄭伊健.md "wikilink")、[錢國偉](../Page/錢國偉.md "wikilink")、[鄭敬基](../Page/鄭敬基.md "wikilink")、[洪天明](../Page/洪天明.md "wikilink")、[金剛](../Page/金剛.md "wikilink")、[朱永棠](../Page/朱永棠.md "wikilink")、[阮兆祥](../Page/阮兆祥.md "wikilink")、[關寶慧](../Page/關寶慧.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")、[鍾麗淇](../Page/鍾麗淇.md "wikilink")、[佘詩曼](../Page/佘詩曼.md "wikilink")、[周家蔚](../Page/周家蔚.md "wikilink")、[文頌嫻](../Page/文頌嫻.md "wikilink")、[姚樂怡](../Page/姚樂怡.md "wikilink")。

2013年3月10日順利誕下大女兒錢凱晴〈Alyssa〉。2015年再度懷孕，同年9月23日順利誕下二女兒錢凱琪〈Kassidy〉。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/天地男兒.md" title="wikilink">天地男兒</a></p></td>
<td><p>護　士</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/真情.md" title="wikilink">真情</a></p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/900重案追兇.md" title="wikilink">900重案追兇</a></p></td>
<td><p>Betty</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西遊記.md" title="wikilink">西遊記</a></p></td>
<td><p>蛇　妖</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/刑事偵緝檔案III.md" title="wikilink">刑事偵緝檔案III</a></p></td>
<td><p>何小麗（Erica）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/醉打金枝.md" title="wikilink">醉打金枝</a></p></td>
<td><p>宮　女</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/樂壇插班生.md" title="wikilink">樂壇插班生</a></p></td>
<td><p>模仿比賽的參賽者</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陀槍師姐.md" title="wikilink">陀槍師姐</a></p></td>
<td><p>小　丁</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/緣來沒法擋.md" title="wikilink">緣來沒法擋</a></p></td>
<td><p>Susan</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/外父唔怕做.md" title="wikilink">外父唔怕做</a></p></td>
<td><p>Sandy</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/妙手仁心.md" title="wikilink">妙手仁心</a></p></td>
<td><p>周淑茵</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/鑑證實錄II.md" title="wikilink">鑑證實錄II</a></p></td>
<td><p>鍾翠屏</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/全院滿座.md" title="wikilink">全院滿座</a></p></td>
<td><p>湯賢淑</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/先生貴性.md" title="wikilink">先生貴性</a></p></td>
<td><p>蘇麗盈（Sophia）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/布袋和尚_(電視劇).md" title="wikilink">布袋和尚</a></p></td>
<td><p>文瑞公主</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FM701.md" title="wikilink">FM701</a></p></td>
<td><p>方詠心（CoCo）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金裝四大才子.md" title="wikilink">金裝四大才子</a></p></td>
<td><p>華府丫環</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/娛樂反斗星.md" title="wikilink">娛樂反斗星</a></p></td>
<td><p>顧寶玲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/封神榜_(2001年電視劇).md" title="wikilink">封神榜</a></p></td>
<td><p>柳琵琶</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/騎呢大狀.md" title="wikilink">騎呢大狀</a></p></td>
<td><p>玫　瑰</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/駁命老公追老婆.md" title="wikilink">駁命老公追老婆</a></p></td>
<td><p>方美斯（Macy）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/七號差館_(電視劇).md" title="wikilink">七號差館</a></p></td>
<td><p>楊芷君</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戇夫成龍.md" title="wikilink">戇夫成龍</a></p></td>
<td><p>包金枝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/十萬噸情緣.md" title="wikilink">十萬噸情緣</a></p></td>
<td><p>陳寶兒（Pauline）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金牌冰人.md" title="wikilink">金牌冰人</a></p></td>
<td><p>屈寶如</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/俗世情真.md" title="wikilink">俗世情真</a></p></td>
<td><p>方　菲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/非常外父.md" title="wikilink">非常外父</a></p></td>
<td><p>王麗莎</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Monique</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/牛郎織女_(2002年電視劇).md" title="wikilink">牛郎織女</a></p></td>
<td><p>江　寧</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楚漢驕雄.md" title="wikilink">楚漢驕雄</a></p></td>
<td><p><a href="../Page/薄姬.md" title="wikilink">薄　姬</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜</a></p></td>
<td><p>Grace Ho</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/我師傅係黃飛鴻.md" title="wikilink">我師傅係黃飛鴻</a></p></td>
<td><p>張玉如</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開心賓館.md" title="wikilink">開心賓館</a></p></td>
<td><p>陳翹楚</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a></p></td>
<td><p><a href="../Page/李笑好.md" title="wikilink">李笑好</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/御用閒人.md" title="wikilink">御用閒人</a></p></td>
<td><p>瑞貴人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/窈窕熟女.md" title="wikilink">窈窕熟女</a></p></td>
<td><p>莊瑪莉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/天幕下的戀人.md" title="wikilink">天幕下的戀人</a></p></td>
<td><p>Suki（神婆）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/潮爆大狀.md" title="wikilink">潮爆大狀</a></p></td>
<td><p>余樂施</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛情全保.md" title="wikilink">愛情全保</a></p></td>
<td><p>蕭　雲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/同事三分親.md" title="wikilink">同事三分親</a></p></td>
<td><p>明　亮</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凶城計中計.md" title="wikilink">凶城計中計</a></p></td>
<td><p>盤霽月</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歲月風雲.md" title="wikilink">歲月風雲</a></p></td>
<td><p>芝　芝（GiGi）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/兩妻時代.md" title="wikilink">兩妻時代</a></p></td>
<td><p>繆　鈴（Meow Meow）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/師奶股神.md" title="wikilink">師奶股神</a></p></td>
<td><p>蔣如寶</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/疑情別戀.md" title="wikilink">疑情別戀</a></p></td>
<td><p>方卓玲（Bonnie）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/蔡鍔與小鳳仙.md" title="wikilink">蔡鍔與小鳳仙</a></p></td>
<td><p>洪麗媚（小麗媚）</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/鐵馬尋橋.md" title="wikilink">鐵馬尋橋</a></p></td>
<td><p>周芳芳</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/讀心神探.md" title="wikilink">讀心神探</a></p></td>
<td><p>玄靈子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/居家兵團.md" title="wikilink">居家兵團</a></p></td>
<td><p>華晶叻</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/結·分@謊情式.md" title="wikilink">結·分@謊情式</a></p></td>
<td><p>-{姜}-蓉蓉（Ginger）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法證先鋒III.md" title="wikilink">法證先鋒III</a></p></td>
<td><p>Rebecca</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天與地_(無綫電視劇).md" title="wikilink">天與地</a></p></td>
<td><p>楊雪薇（Shirley）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/換樂無窮.md" title="wikilink">換樂無窮</a></p></td>
<td><p>顧家琴（Kim）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/耀舞長安.md" title="wikilink">耀舞長安</a></p></td>
<td><p>朱攬月（朱上師）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/老表，你好嘢！_(電視劇).md" title="wikilink">老表, 你好嘢!</a></p></td>
<td><p>司馬淑貞（導遊貞）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/衝上雲霄II.md" title="wikilink">衝上雲霄II</a></p></td>
<td><p>神　婆</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/新抱喜相逢.md" title="wikilink">新抱喜相逢</a></p></td>
<td><p>文彩菱</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/倩女喜相逢.md" title="wikilink">倩女喜相逢</a></p></td>
<td><p>燕赤霞</p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/愛·回家之八時入席.md" title="wikilink">愛·回家之八時入席</a></p></td>
<td><p>趙麗虹（阿虹、虹姐）</p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/愛·回家之開心速遞.md" title="wikilink">愛·回家之開心速遞</a></p></td>
<td><p><strong>高栢菲（David）</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/老表，畢業喇！.md" title="wikilink">老表，畢業喇！</a></p></td>
<td><p>練敏敏</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視電影

|        |                                          |        |        |
| ------ | ---------------------------------------- | ------ | ------ |
| **首播** | **片名**                                   | **角色** | **備註** |
| 2001年  | [天降橫財心驚驚](../Page/天降橫財心驚驚.md "wikilink") | 李香琴    | （TVM）  |
|        |                                          |        |        |

### 電影

|        |                                            |        |
| ------ | ------------------------------------------ | ------ |
| **首播** | **片名**                                     | **角色** |
| 1998   | 生化壽屍                                       |        |
| 1998   | 亞李·爸爸：兩個大盜                                 |        |
| 2002   | [嫁個有錢人](../Page/嫁個有錢人.md "wikilink")       |        |
| 2002   | [嚦咕嚦咕新年財](../Page/嚦咕嚦咕新年財.md "wikilink")   |        |
| 2011   | [我愛HK開心萬歲](../Page/我愛HK開心萬歲.md "wikilink") |        |
| 2011   | [白蛇傳說](../Page/白蛇傳說.md "wikilink")         | 貓妖     |
| 2016   | [刑警兄弟](../Page/刑警兄弟.md "wikilink")         | 女同事    |

### 音樂錄影帶

  - 囍宴　　　　　主唱：[李蕙敏](../Page/李蕙敏.md "wikilink")（1996）
  - 衣櫃裏的男人　主唱：[梁漢文](../Page/梁漢文.md "wikilink")（1996）
  - 有我必須有你　主唱：[DRY](../Page/DRY.md "wikilink")（1997）

### 節目主持([無綫電視](../Page/無綫電視.md "wikilink"))

|        |                                      |
| ------ | ------------------------------------ |
| **年份** | **節目**                               |
| 2006   | [笑口常開](../Page/笑口常開.md "wikilink")   |
| 2007   | [咁樣認第一](../Page/咁樣認第一.md "wikilink") |
| 2011   | [明珠生活](../Page/明珠生活.md "wikilink")   |
| 2011   | [知法守法](../Page/知法守法.md "wikilink")   |
|        |                                      |

## 音樂作品(歌曲)

|               |                                          |                                      |        |
| ------------- | ---------------------------------------- | ------------------------------------ | ------ |
| **歌曲**        | **動畫**                                   | **電視劇**                              | **性質** |
| We are family | 動畫《[我們這一家](../Page/我們這一家.md "wikilink")》 |                                      | 主題曲    |
| 沒完沒了          |                                          | 《[兩妻時代](../Page/兩妻時代.md "wikilink")》 | 主題曲    |
|               |                                          |                                      |        |

## 獎項殊榮

### 電視獎項

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>機構</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>劇集</strong></p></td>
<td><p><strong>角色/歌曲</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/2005年度萬千星輝賀台慶.md" title="wikilink">2005年度萬千星輝賀台慶</a>》</p></td>
<td><p>最佳女配角</p></td>
<td><p>阿旺新傳</p></td>
<td><p><a href="../Page/李笑好.md" title="wikilink">李笑好</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/新城勁爆電視大獎頒獎禮.md" title="wikilink">新城勁爆電視大獎頒獎禮</a>》</p></td>
<td><p>新城勁爆女配角</p></td>
<td><p><a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a></p></td>
<td><p><a href="../Page/李笑好.md" title="wikilink">李笑好</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/2006年度壹電視大獎得獎名單.md" title="wikilink">壹電視大獎2006</a>》</p></td>
<td><p>十大電視藝人（第一位）</p></td>
<td><p>/</p></td>
<td><p>/</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/2006年萬千星輝頒獎典禮.md" title="wikilink">2006年萬千星輝頒獎典禮</a>》</p></td>
<td><p>最佳女配角</p></td>
<td><p><a href="../Page/愛情全保.md" title="wikilink">愛情全保</a></p></td>
<td><p>蕭雲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/Astro華麗台電視劇大獎2007.md" title="wikilink">Astro華麗台電視劇大獎2007</a>》</p></td>
<td><p>我的至愛極品造型</p></td>
<td><p><a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a></p></td>
<td><p><a href="../Page/李笑好.md" title="wikilink">李笑好</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/香港貿易發展局.md" title="wikilink">香港貿易發展局</a></p></td>
<td><p>最受歡迎香港女電視藝人（六名之一）</p></td>
<td><p>/</p></td>
<td><p>/</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/2009年萬千星輝頒獎典禮.md" title="wikilink">2009年萬千星輝頒獎典禮</a>》</p></td>
<td><p>最佳女配角</p></td>
<td><p><a href="../Page/蔡鍔與小鳳仙.md" title="wikilink">蔡鍔與小鳳仙</a></p></td>
<td><p>洪麗媚</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p>《<a href="../Page/萬千星輝頒獎典禮2012.md" title="wikilink">萬千星輝頒獎典禮2012</a>》</p></td>
<td><p>最佳劇集</p></td>
<td><p><a href="../Page/天與地_(無綫電視劇).md" title="wikilink">天與地</a></p></td>
<td><p>楊雪薇（Shirley）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/萬千星輝頒獎典禮2013.md" title="wikilink">萬千星輝頒獎典禮2013</a>》</p></td>
<td><p>最佳女配角</p></td>
<td><p><a href="../Page/老表，你好嘢！.md" title="wikilink">老表，你好嘢！</a></p></td>
<td><p>司馬淑貞</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>《<a href="../Page/萬千星輝頒獎典禮2014.md" title="wikilink">萬千星輝頒獎典禮2014</a>》</p></td>
<td><p>最受歡迎劇集歌曲</p></td>
<td><p><a href="../Page/新抱喜相逢.md" title="wikilink">新抱喜相逢</a></p></td>
<td><p>喜氣洋洋</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>《<a href="../Page/萬千星輝頒獎典禮2015.md" title="wikilink">萬千星輝頒獎典禮2015</a>》</p></td>
<td><p>最受歡迎電視女角色</p></td>
<td><p><a href="../Page/倩女喜相逢.md" title="wikilink">倩女喜相逢</a></p></td>
<td><p>燕赤霞</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>《<a href="../Page/萬千星輝頒獎典禮2016.md" title="wikilink">萬千星輝頒獎典禮2016</a>》</p></td>
<td><p>最受歡迎電視女角色</p></td>
<td><p><a href="../Page/愛·回家之八時入席.md" title="wikilink">愛·回家之八時入席</a></p></td>
<td><p>趙麗虹</p></td>
<td></td>
</tr>
</tbody>
</table>

### 兒歌獎項

|        |                                                              |                              |
| ------ | ------------------------------------------------------------ | ---------------------------- |
| **年份** | **機構**                                                       | **獎項**                       |
| 2007年  | 《[2007年度兒歌金曲頒獎典禮](../Page/2007年度兒歌金曲頒獎典禮得獎名單.md "wikilink")》 | 十大兒歌金曲 - We are family       |
| 2007年  | 《2007年度兒歌金曲頒獎典禮》                                             | 最受爹D媽咪歡迎兒歌大獎 - We are family |
|        |                                                              |                              |

## 参考文献

## 外部連結

  - [湯盈盈 TVB Blog](http://blog.tvb.com/angelatong)

  -
  -
  -
[category:香港電影演員](../Page/category:香港電影演員.md "wikilink")
[category:香港女歌手](../Page/category:香港女歌手.md "wikilink")
[category:华裔加拿大人](../Page/category:华裔加拿大人.md "wikilink")
[Y](../Page/category:湯姓.md "wikilink")

[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:蒙特婁人](../Category/蒙特婁人.md "wikilink")
[Category:香港順德人](../Category/香港順德人.md "wikilink")
[Category:康考迪亞大學校友](../Category/康考迪亞大學校友.md "wikilink")
[Category:酒醉駕駛](../Category/酒醉駕駛.md "wikilink")

1.  TVB資訊節目《[回鄉](../Page/回鄉.md "wikilink")》
2.