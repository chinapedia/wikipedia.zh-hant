**高半胱氨酸**（，或稱為**同型半胱氨酸**或**同半胱氨酸**）是[氨基酸](../Page/氨基酸.md "wikilink")[半胱氨酸的異種](../Page/半胱氨酸.md "wikilink")，在[旁鏈部份](../Page/旁鏈.md "wikilink")[硫醇基](../Page/硫醇基.md "wikilink")（-SH）前包含一個額外的[亞甲基](../Page/亞甲基.md "wikilink")（-CH<sub>2</sub>-）。

## 化學

[Methionine_pathway_(zh-cn).svg](https://zh.wikipedia.org/wiki/File:Methionine_pathway_\(zh-cn\).svg "fig:Methionine_pathway_(zh-cn).svg")
高半胱氨酸的額外[亞甲基使](../Page/亞甲基.md "wikilink")[硫醇基更接近](../Page/硫醇基.md "wikilink")[羧基](../Page/羧基.md "wikilink")，令它能起動[化學反應形成一個五元環](../Page/化學反應.md "wikilink")，稱為[高半胱氨酸硫內酯](../Page/高半胱氨酸硫內酯.md "wikilink")。當[氨基酸正常地與它的毗鄰形成一個](../Page/氨基酸.md "wikilink")[肽鍵就會產生這種反應](../Page/肽鍵.md "wikilink")。高半胱氨酸所以是不適合與[蛋白質混合](../Page/蛋白質.md "wikilink")，這是因含有高半胱氨酸的蛋白質會自行分解。

高半胱氨酸是從[S-腺苷基蛋氨酸透過兩個反應步驟途徑形成](../Page/S-腺苷基蛋氨酸.md "wikilink")。並能回轉成[蛋氨酸](../Page/蛋氨酸.md "wikilink")，或經過[轉硫途徑而回轉為](../Page/轉硫途徑.md "wikilink")[半胱氨酸或](../Page/半胱氨酸.md "wikilink")[牛磺酸](../Page/牛磺酸.md "wikilink")。雖然高半胱氨酸可以回轉為半胱氨酸，但沒有證據顯示人類食用高半胱氨酸可取代半胱氨酸的攝取。\[1\]

## 高半胱氨酸水平上升

缺乏[維他命如](../Page/維他命.md "wikilink")[葉酸](../Page/葉酸.md "wikilink")（B<sub>9</sub>）、[吡哆醇](../Page/吡哆醇.md "wikilink")（B<sub>6</sub>）或[鈷胺素](../Page/鈷胺素.md "wikilink")（B<sub>12</sub>），作為[生物化學反應的結果](../Page/生物化學.md "wikilink")，高半胱氨酸水平都會上升。\[2\]
補充吡哆醇、葉酸、鈷胺素或[三甲基甘氨酸會減少](../Page/三甲基甘氨酸.md "wikilink")[血液內的高半胱氨酸的濃度](../Page/血液.md "wikilink")。\[3\]\[4\]高水平的高半胱氨酸會與[內皮細胞的](../Page/內皮細胞.md "wikilink")[非對稱性二甲基精氨酸的高水平有所關連](../Page/非對稱性二甲基精氨酸.md "wikilink")。

高半胱氨酸的攀升在少見的遺傳病[高胱胺酸尿症及缺乏](../Page/高胱胺酸尿症.md "wikilink")[亞甲基四氫葉酸還原酶](../Page/亞甲基四氫葉酸還原酶.md "wikilink")。後者是較普遍而不常發現，而高半胱氨酸較高的人會容易患上[血栓症及](../Page/血栓症.md "wikilink")[心血管疾病](../Page/心血管疾病.md "wikilink")。

高半胱氨酸在高濃度的[多酚抗氧化劑下會調降](../Page/多酚抗氧化劑.md "wikilink")，而多酚抗氧化劑被認為是對[心血管系統及](../Page/心血管系統.md "wikilink")[免疫系統有某些健康益處](../Page/免疫系統.md "wikilink")。多酚抗氧化劑可以調降心血管疾病的重要化合物[活性氧的形成](../Page/活性氧.md "wikilink")。透過高半胱氨酸的自動氧化成活性氧會導致生物損害。

## 心血管風險

[血清內高半胱氨酸的高水平是潛在](../Page/血清.md "wikilink")[心血管疾病的標記](../Page/心血管疾病.md "wikilink")，它即是這種疾病及[中風的風險因素](../Page/中風.md "wikilink")。現時正研究是否高半胱氨酸的高水平本身就是一個問題或是現存問題的指標。\[5\]

2006年有一項研究指攝取[維他命以減低高半胱氨酸水平並不能即時帶來效用](../Page/維他命.md "wikilink")，但是明顯在中風個案中下降了25%。\[6\]縱然整體死亡率並沒有明顯改變，但亦會為患有嚴重[動脈衰竭的病人帶來幫助](../Page/動脈.md "wikilink")。減低高半胱氨酸並非快速回復動脈結構破壞的方法。但是，[生物化學強烈支持高半胱氨酸會使動脈的三個主要結構](../Page/生物化學.md "wikilink")（[膠原蛋白](../Page/膠原蛋白.md "wikilink")、[彈性蛋白及](../Page/彈性蛋白.md "wikilink")[多醣蛋白](../Page/多醣蛋白.md "wikilink")）衰退及阻止增生。高半胱氨酸永久性令[半胱氨酸衰退及](../Page/半胱氨酸.md "wikilink")[蛋白質內的](../Page/蛋白質.md "wikilink")[賴氨酸](../Page/賴氨酸.md "wikilink")，及逐漸影響其功能及結構。簡單來說，高半胱氨酸是一種腐蝕長久的膠原蛋白及彈性蛋白，或達一生之久的[結締組織蛋白質的物質](../Page/結締組織蛋白質.md "wikilink")。這種長遠的影響在臨床研究上是很難發現的。減低高半胱氨酸的目的是要防止，而在治療上只有很慢的進展。\[7\]\[8\]\[9\]

## 骨軟弱症

高半胱氨酸的高水平增加了老人的[骨折](../Page/骨折.md "wikilink")。\[10\]\[11\]高半胱氨酸不會影響骨質密度。它反而是妨礙[膠原蛋白纖維與組織的連接](../Page/膠原蛋白.md "wikilink")，來影響膠原蛋白。

[維他命補充劑可以在膠原蛋白上作出效用](../Page/維他命.md "wikilink")。由於老人並不能有效地從食物中吸收[鈷胺素](../Page/鈷胺素.md "wikilink")（B<sub>12</sub>），他們可以以補充劑的方式獲得療效。

## 參考

<references/>

## 外部連結

  - [高同型半胱氨酸血症](https://web.archive.org/web/20071008054923/http://www.muscle-nerve.com/ReadNews.asp?NewsID=313)
  - [同型半胱氨酸---心脑血管疾病的独立危险因子](http://www.sdccl.com.cn/ltxxlr.asp?id=1253)
  - [Methionine and
    Homocysteine](https://web.archive.org/web/20061111044539/http://www.thorne.com/altmedrev/fulltext/meth1-4.html)
  - [Structure, Properties, and metabolism of
    Homocysteine](https://web.archive.org/web/20060822023746/http://www.diabetesforum.net/cgi-bin/display_engine.pl?category_id=8&content_id=232)

[分類:α-胺基酸](../Page/分類:α-胺基酸.md "wikilink")
[分類:含硫氨基酸](../Page/分類:含硫氨基酸.md "wikilink")
[分類:硫醇](../Page/分類:硫醇.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.  [原整文章](http://content.nejm.org/cgi/content/full/350/20/2042)

11.  [原整文章](http://content.nejm.org/cgi/content/full/350/20/2033)