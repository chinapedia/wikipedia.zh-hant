## [阿根廷](../Page/阿根廷国家足球队.md "wikilink")

主教练：[-{zh-hans:马塞洛·贝尔萨;
zh-hant:比爾沙;}-](../Page/马塞洛·贝尔萨.md "wikilink")（Marcelo
Bielsa）

| \# | 姓名                                                                                              | 位置 | 年龄 | 出场次数 | 所属球队                                                                                                                                                               |
| -- | ----------------------------------------------------------------------------------------------- | -- | -- | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1  | [-{zh-hans:赫尔曼·博格斯; zh-hant:貝高斯;}-](../Page/赫尔曼·博格斯.md "wikilink")（Germán Burgos）               | 门将 | 33 | 35   | [-{zh-hans:马德里竞技; zh-hant:馬德里體育會;}-](../Page/馬德里體育會.md "wikilink")（Atlético Madrid，西班牙）                                                                            |
| 2  | [-{zh-hans:罗伯托·阿亚拉; zh-hant:艾耶拉;}-](../Page/罗伯托·阿亚拉.md "wikilink")（Roberto Ayala）               | 后卫 | 29 | 74   | [巴伦西亚](../Page/巴伦西亚足球俱乐部.md "wikilink")（Valencia，西班牙）                                                                                                              |
| 3  | [-{zh-hans:胡安·巴勃罗·索林; zh-hant:蘇連;}-](../Page/胡安·巴勃罗·索林.md "wikilink")（Juan Pablo Sorín）         | 中场 | 26 | 35   | [-{zh-hans:克鲁塞罗; zh-hant:高士路;}-](../Page/克鲁塞罗足球俱乐部.md "wikilink")（Cruzeiro，巴西）                                                                                     |
| 4  | [-{zh-hans:毛里西奥·波切蒂诺; zh-hant:普捷天奴;}-](../Page/毛里西奥·波切蒂诺.md "wikilink")（Mauricio Pochettino）    | 后卫 | 30 | 16   | [-{zh-hans:巴黎圣日耳曼; zh-hant:巴黎聖日爾門;}-](../Page/巴黎圣日耳曼.md "wikilink")（Paris Saint-Germain，法国）                                                                        |
| 5  | [-{zh-hans:阿尔梅达;zh-hk:艾美達;}-](../Page/馬迪亞斯·艾美達.md "wikilink")（Matías Almeyda）                   | 中场 | 29 | 33   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [帕尔马](../Page/帕爾瑪足球俱樂部.md "wikilink")                          |
| 6  | [-{zh-hans:沃尔特·萨穆埃尔; zh-hant:華特·森美爾;}-](../Page/沃尔特·萨穆埃尔.md "wikilink")（Walter Samuel）          | 后卫 | 24 | 30   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [罗马](../Page/羅馬足球會.md "wikilink")                              |
| 7  | [-{zh-hans:克劳迪奥·洛佩斯; zh-hant:葛迪奧·盧比斯;}-](../Page/克劳迪奥·洛佩斯.md "wikilink")（Claudio López）         | 前锋 | 28 | 49   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [-{zh-hans:拉齐奥; zh-hant:拉素;}-](../Page/拉素足球俱樂部.md "wikilink")  |
| 8  | [-{zh-hans:哈维尔·萨内蒂; zh-hant:查維·辛尼迪;}-](../Page/哈维尔·萨内蒂.md "wikilink")（Javier Zanetti）           | 中场 | 28 | 66   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [国际米兰](../Page/国际米兰.md "wikilink")                             |
| 9  | [-{zh-hans:加夫列尔·巴蒂斯图塔; zh-hant:巴迪斯圖達;}-](../Page/加夫列尔·巴蒂斯图塔.md "wikilink")（Gabriel Batistuta）   | 前锋 | 33 | 75   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [罗马](../Page/羅馬足球會.md "wikilink")                              |
| 10 | [-{zh-hans:奥尔特加;zh-hk:奧迪加;zh-tw:奧爾特加;}-](../Page/阿列尔·奥尔特加.md "wikilink")（Ariel Ortega）          | 中场 | 28 | 81   | [河床](../Page/河床足球俱乐部.md "wikilink")（River Plate，阿根廷）                                                                                                               |
| 11 | [-{zh-hans:胡安·塞瓦斯蒂安·贝隆; zh-hant:華朗;}-](../Page/胡安·塞瓦斯蒂安·贝隆.md "wikilink")（Juan Sebastián Verón） | 中场 | 27 | 47   | [-{zh-hans:曼彻斯特联; zh-hant:曼聯;}-](../Page/曼彻斯特联足球俱乐部.md "wikilink")（Manchester United，英格兰）                                                                          |
| 12 | [-{zh-hans:巴勃罗·卡巴列罗; zh-hant:卡華拿路;}-](../Page/巴勃罗·卡巴列罗.md "wikilink")（Pablo Cavallero）          | 门将 | 29 | 8    | [维戈塞尔塔](../Page/维戈塞尔塔.md "wikilink")（Celta Vigo，西班牙）                                                                                                               |
| 13 | [-{zh-hans:迭戈·普拉森特; zh-hant:柏拉辛迪;}-](../Page/迭戈·普拉森特.md "wikilink")（Diego Placente）             | 后卫 | 25 | 6    | [-{zh-hans:勒沃库森; zh-hant:利華古遜;}-](../Page/勒沃库森足球俱乐部.md "wikilink")（Bayer Leverkusen，德国）                                                                            |
| 14 | [-{zh-hans:迭戈·西蒙尼; zh-hant:施蒙尼;}-](../Page/迭戈·西蒙尼.md "wikilink")（Diego Simeone）                 | 中场 | 32 | 104  | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [-{zh-hans:拉齐奥; zh-hant:拉素;}-](../Page/拉齐奥足球俱乐部.md "wikilink") |
| 15 | [-{zh-hans:克劳迪奥·候塞因; zh-hant:侯辛;}-](../Page/克劳迪奥·候塞因.md "wikilink")（Claudio Husaín）             | 中场 | 27 | 14   | [河床](../Page/河床足球俱乐部.md "wikilink")（River Plate，阿根廷）                                                                                                               |
| 16 | [-{zh-hans:巴勃罗·艾马尔; zh-hant:艾馬;}-](../Page/巴勃罗·艾马尔.md "wikilink")（Pablo Aimar）                  | 中场 | 22 | 18   | [巴伦西亚](../Page/巴伦西亚足球俱乐部.md "wikilink")（Valencia，西班牙）                                                                                                              |
| 17 | [-{zh-hans:古斯塔沃·洛佩斯; zh-hant:古斯達禾·盧比斯;}-](../Page/古斯塔沃·洛佩斯.md "wikilink")（Gustavo López）        | 前锋 | 29 | 31   | [-{zh-hans:维戈塞尔塔; zh-hant:切爾達;}-](../Page/维戈塞尔塔.md "wikilink")（Celta Vigo，西班牙）                                                                                     |
| 18 | [-{zh-hans:基利·冈萨雷斯; zh-hant:基利·干沙里斯;}-](../Page/基利·冈萨雷斯.md "wikilink")（Kily González）           | 前锋 | 27 | 30   | [巴伦西亚](../Page/巴伦西亚足球俱乐部.md "wikilink")（Valencia，西班牙）                                                                                                              |
| 19 | [-{zh-hans:埃尔南·克雷斯波; zh-hant:基斯普;}-](../Page/埃尔南·克雷斯波.md "wikilink")（Hernán Crespo）             | 前锋 | 26 | 33   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [-{zh-hans:拉齐奥; zh-hant:拉素;}-](../Page/拉素足球俱樂部.md "wikilink")  |
| 20 | [-{zh-hk:加拿度;zh-hans:加拉多;}-](../Page/馬些路·丹尼爾·加拿度.md "wikilink")（Marcelo Gallardo）               | 中场 | 26 | 42   | [摩纳哥](../Page/摩纳哥足球俱乐部.md "wikilink")（AS Monaco，法国）                                                                                                                |
| 21 | [-{zh-hans:克劳迪奥·卡尼吉亚; zh-hant:肯尼基亞;}-](../Page/克劳迪奥·卡尼吉亚.md "wikilink")（Claudio Caniggia）       | 前锋 | 35 | 50   | [-{zh-hans:格拉斯哥流浪者; zh-hant:格拉斯哥流浪;}-](../Page/格拉斯哥流浪.md "wikilink")（Rangers，苏格兰）                                                                                  |
| 22 | [-{zh-hans:查莫特;zh-hk:查莫特;zh-tw:查莫特;}-](../Page/荷西·查莫特.md "wikilink")（José Chamot）               | 后卫 | 33 | 42   | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [AC米兰](../Page/AC米兰.md "wikilink")                             |
| 23 | [-{zh-hans:罗伯托·博纳诺; zh-hant:羅拔圖·保蘭奴;}-](../Page/罗伯托·博纳诺.md "wikilink")（Roberto Bonano）          | 门将 | 32 | 13   | [-{zh-hans:巴塞罗那; zh-hant:巴塞隆拿;}-](../Page/巴塞罗那足球俱乐部.md "wikilink")（Barcelona，西班牙）                                                                                  |

## [英格蘭](../Page/英格蘭國家足球隊.md "wikilink")

主教练：[-{zh-hans:斯文·戈兰·埃里克松;
zh-hant:艾歷臣;}-](../Page/斯文·戈兰·埃里克松.md "wikilink")（Sven-Göran
Eriksson）

| 編號 | 位置                               | 球員名稱                                                                                       | 出生日期／年齡       | 代表次數 | 效力球隊                                                                                                                                                                     |
| -- | -------------------------------- | ------------------------------------------------------------------------------------------ | ------------- | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1  | [門將](../Page/足球位置.md "wikilink") | [-{zh-hans:大卫·西曼; zh-hant:施文;}-](../Page/大卫·西曼.md "wikilink")（David Seaman）                | 1963.09.19/38 | 68   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [阿森纳](../Page/阿仙奴.md "wikilink")                               |
| 2  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:丹尼·米尔斯; zh-hant:米斯;}-](../Page/丹尼·米尔斯.md "wikilink")（Danny Mills）               | 1977.05.18/25 | 6    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:利兹联; zh-hant:列斯聯;}-](../Page/列斯聯.md "wikilink")     |
| 3  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:阿什利·科尔; zh-hant:艾殊利·高爾;}-](../Page/阿什利·科尔.md "wikilink")（Ashley Cole）           | 1980.12.20/21 | 8    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:阿森纳; zh-hant:阿仙奴;}-](../Page/阿仙奴.md "wikilink")     |
| 4  | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:特莱沃·辛克莱尔; zh-hant:冼佳亞;}-](../Page/特莱沃·辛克莱尔.md "wikilink")（Trevor Sinclair）      | 1973.03.02/29 | 4    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:南安普顿; zh-hant:韋斯咸;}-](../Page/韋斯咸.md "wikilink")    |
| 5  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:里奥·费迪南德; zh-hant:里奧·費迪南;}-](../Page/里奥·费迪南德.md "wikilink")（Rio Ferdinand）       | 1978.11.07/23 | 21   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:利兹联; zh-hant:列斯聯;}-](../Page/列斯聯.md "wikilink")     |
| 6  | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:索尔·坎贝尔; zh-hant:蘇金保;}-](../Page/索尔·坎贝尔.md "wikilink")（Sol Campbell）             | 1974.09.19/27 | 45   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:阿森纳; zh-hant:阿仙奴;}-](../Page/阿仙奴.md "wikilink")     |
| 7  | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:大卫·贝克汉姆; zh-hant:碧咸;}-](../Page/大卫·贝克汉姆.md "wikilink")（David Beckham）           | 1975.05.02/27 | 49   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [曼联](../Page/曼联.md "wikilink")                                 |
| 8  | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:保罗·斯科尔斯; zh-hant:史高斯;}-](../Page/保罗·斯科尔斯.md "wikilink")（Paul Scholes）           | 1974.11.16/27 | 43   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [曼联](../Page/曼联.md "wikilink")                                 |
| 9  | [前鋒](../Page/足球位置.md "wikilink") | [-{zh-hans:罗比·福勒; zh-hant:科拿;}-](../Page/罗比·福勒.md "wikilink")（Robbie Fowler）               | 1975.04.09/27 | 24   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:利兹联; zh-hant:列斯聯;}-](../Page/列斯聯.md "wikilink")     |
| 10 | [前鋒](../Page/足球位置.md "wikilink") | [-{zh-hans:迈克尔·欧文; zh-hant:奧雲;}-](../Page/迈克尔·欧文.md "wikilink")（Michael Owen）              | 1979.12.14/22 | 35   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [利物浦](../Page/利物浦足球會.md "wikilink")                            |
| 11 | [前鋒](../Page/足球位置.md "wikilink") | [-{zh-hans:埃米尔·赫斯基; zh-hant:希斯基;}-](../Page/埃米尔·赫斯基.md "wikilink")（Emile Heskey）           | 1978.01.11/24 | 23   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [利物浦](../Page/利物浦足球會.md "wikilink")                            |
| 12 | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:维斯·布朗; zh-hant:布朗;}-](../Page/维斯·布朗.md "wikilink")（Wes Brown）                   | 1979.10.13/22 | 5    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [曼联](../Page/曼联.md "wikilink")                                 |
| 13 | [門將](../Page/足球位置.md "wikilink") | [-{zh-hans:奈杰尔·马丁; zh-hant:馬田;}-](../Page/奈杰尔·马丁.md "wikilink")（Nigel Martyn）              | 1966.08.11/35 | 22   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:利兹联; zh-hant:列斯聯;}-](../Page/列斯聯.md "wikilink")     |
| 14 | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:韦恩·布里奇; zh-hant:布歷治;}-](../Page/布歷治.md "wikilink")（Wayne Bridge）                | 1980.08.05/21 | 4    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:南安普顿; zh-hant:韋斯咸;}-](../Page/韋斯咸.md "wikilink")    |
| 15 | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:马丁·基翁; zh-hant:基昂;}-](../Page/马丁·基翁.md "wikilink")（Martin Keown）                | 1966.07.24/36 | 42   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:阿森纳; zh-hant:阿仙奴;}-](../Page/阿仙奴.md "wikilink")     |
| 16 | [後衛](../Page/足球位置.md "wikilink") | [-{zh-hans:盖雷斯·索斯盖特; zh-hant:格里夫·修夫基;}-](../Page/盖雷斯·索斯盖特.md "wikilink")（Gareth Southgate） | 1970.09.03/31 | 48   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:米德尔斯堡; zh-hant:米杜士堡;}-](../Page/米杜士堡.md "wikilink") |
| 17 | [前鋒](../Page/足球位置.md "wikilink") | [-{zh-hans:特迪·谢林汉姆; zh-hant:舒靈咸;}-](../Page/特迪·谢林汉姆.md "wikilink")（Teddy Sheringham）       | 1966.04.02/36 | 46   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [熱刺](../Page/熱刺.md "wikilink")                                 |
| 18 | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:欧文·哈格里夫斯; zh-hant:夏格維斯;}-](../Page/欧文·哈格里夫斯.md "wikilink")（Owen Hargreaves）     | 1981.01.20/21 | 5    | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")                           |
| 19 | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:乔·科尔; zh-hant:祖·高爾;}-](../Page/祖高爾.md "wikilink")（Joe Cole）                     | 1981.11.08/20 | 5    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:南安普顿; zh-hant:韋斯咸;}-](../Page/韋斯咸.md "wikilink")    |
| 20 | [前鋒](../Page/足球位置.md "wikilink") | [-{zh-hans:达里乌斯·瓦塞尔; zh-hant:華素爾;}-](../Page/达里乌斯·瓦塞尔.md "wikilink")（Darius Vassell）       | 1980.08.13/22 | 4    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [阿士東维拉](../Page/阿士東维拉.md "wikilink")                           |
| 21 | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:尼基·巴特; zh-hant:畢特;}-](../Page/尼基·巴特.md "wikilink")（Nicky Butt）                  | 1975.01.21/27 | 18   | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [曼联](../Page/曼联.md "wikilink")                                 |
| 22 | [門將](../Page/足球位置.md "wikilink") | [-{zh-hans:大卫·詹姆斯; zh-hant:占士;}-](../Page/大卫·詹姆斯.md "wikilink")（David James）               | 1970.08.01/31 | 8    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [-{zh-hans:南安普顿; zh-hant:韋斯咸;}-](../Page/韋斯咸.md "wikilink")    |
| 23 | [中場](../Page/足球位置.md "wikilink") | [-{zh-hans:基隆·代尔; zh-hant:戴亞;}-](../Page/基隆·代尔.md "wikilink")（Kieron Dyer）                 | 1978.12.29/23 | 9    | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [紐卡素](../Page/紐卡素.md "wikilink")                               |

## [尼日利亚](../Page/尼日利亚国家足球队.md "wikilink")

主教练：[费斯塔斯·奥尼宾德](../Page/费斯塔斯·奥尼宾德.md "wikilink")（Festus Onigbinde）

| \# | 姓名                                                                  | 位置 | 年龄 | 出场次数 | 所属球队                                                                         |
| -- | ------------------------------------------------------------------- | -- | -- | ---- | ---------------------------------------------------------------------------- |
| 1  | [伊克·肖伦穆](../Page/伊克·肖伦穆.md "wikilink")（Ike Shorunmu）                | 门将 | 34 | 34   | [Lausanne](../Page/Lausanne_Sports.md "wikilink") (Switzerland)              |
| 2  | [约瑟夫·雅博](../Page/约瑟夫·雅博.md "wikilink")（Joseph Yobo）                 | 中场 | 21 | 14   | [Marseille](../Page/Olympique_de_Marseille.md "wikilink") (France)           |
| 3  | [塞莱斯汀·巴巴亚罗](../Page/塞莱斯汀·巴巴亚罗.md "wikilink")（Celestine Babayaro）    | 后卫 | 23 | 24   | [Chelsea](../Page/Chelsea_F.C..md "wikilink") (England)                      |
| 4  | [恩万科沃·卡努](../Page/恩万科沃·卡努.md "wikilink")（Nwankwo Kanu）              | 前锋 | 26 | 33   | [Arsenal](../Page/Arsenal_F.C..md "wikilink") (England)                      |
| 5  | [伊萨克·奥科隆科沃](../Page/伊萨克·奥科隆科沃.md "wikilink")（Isaac Okoronkwo）       | 后卫 | 24 | 12   | [Shakhtar Donetsk](../Page/Shakhtar_Donetsk.md "wikilink") (Ukraine)         |
| 6  | [塔里博·威斯特](../Page/塔里博·威斯特.md "wikilink")（Taribo West）               | 后卫 | 28 | 38   | [Kaiserslautern](../Page/1._FC_Kaiserslautern.md "wikilink") (Germany)       |
| 7  | [皮乌斯·伊克迪亚](../Page/皮乌斯·伊克迪亚.md "wikilink")（Pius Ikedia）             | 前锋 | 21 | 9    | [Ajax](../Page/Ajax_Amsterdam.md "wikilink") (Netherlands)                   |
| 8  | [穆蒂尤·阿德波尤](../Page/穆蒂尤·阿德波尤.md "wikilink")（Mutiu Adepoju）           | 中场 | 21 | 49   | [Salamanca](../Page/Union_Deportiva_Salamanca.md "wikilink") (Spain)         |
| 9  | [巴托洛缪·奥贝彻](../Page/巴托洛缪·奥贝彻.md "wikilink")（Bartholomew Ogbeche）     | 前锋 | 18 | 4    | [Paris Saint-Germain](../Page/Paris_Saint-Germain.md "wikilink") (France)    |
| 10 | [杰伊·杰伊·奥科查](../Page/杰伊·杰伊·奥科查.md "wikilink")（Jay-Jay Okocha）        | 中场 | 28 | 56   | [Paris Saint-Germain](../Page/Paris_Saint-Germain.md "wikilink") (France)    |
| 11 | [加尔巴·拉瓦尔](../Page/加尔巴·拉瓦尔.md "wikilink")（Garba Lawal）               | 中场 | 28 | 34   | [Roda JC](../Page/Roda_JC.md "wikilink") (Netherlands)                       |
| 12 | [奥斯汀·埃吉德](../Page/奥斯汀·埃吉德.md "wikilink")（Austin Ejide）              | 门将 | 17 | 3    | [Gabros International](../Page/Gabros_International.md "wikilink") (Nigeria) |
| 13 | [拉比尤·阿福拉比](../Page/拉比尤·阿福拉比.md "wikilink")（Rabiu Afolabi）           | 后卫 | 22 | 5    | [Standard Liège](../Page/Standard_Liège.md "wikilink") (Belgium)             |
| 14 | [伊费亚尼·乌德泽](../Page/伊费亚尼·乌德泽.md "wikilink")（Ifeanyi Udeze）           | 后卫 | 21 | 15   | [PAOK](../Page/PAOK_FC.md "wikilink") (Greece)                               |
| 15 | [贾斯蒂斯·克里斯托弗](../Page/贾斯蒂斯·克里斯托弗.md "wikilink")（Justice Christopher） | 中场 | 20 | 7    | [Royal Antwerp](../Page/Royal_Antwerp.md "wikilink") (Belgium)               |
| 16 | [埃费托波尔·索杰](../Page/埃费托波尔·索杰.md "wikilink")（Efetobore Sodhe）         | 后卫 | 30 | 7    | [Crewe Alexandra](../Page/Crewe_Alexandra_F.C..md "wikilink") (England)      |
| 17 | [朱利叶斯·阿加霍瓦](../Page/朱利叶斯·阿加霍瓦.md "wikilink")（Julius Aghahowa）       | 前锋 | 19 | 17   | [Shakhtar Donetsk](../Page/Shakhtar_Donetsk.md "wikilink") (Ukraine)         |
| 18 | [本尼迪克特·阿格布](../Page/本尼迪克特·阿格布.md "wikilink")（Benedict Akwuegbu）     | 前锋 | 28 | 16   | [Shenyang Haishi](../Page/Shenyang_Haishi.md "wikilink") (China)             |
| 19 | [埃里克·埃吉奥弗尔](../Page/埃里克·埃吉奥弗尔.md "wikilink")（Eric Ejiofor）          | 后卫 | 22 | 12   | [Maccabi Haifa](../Page/Maccabi_Haifa_F.C..md "wikilink") (Israel)           |
| 20 | [詹姆斯·奥比奥拉](../Page/詹姆斯·奥比奥拉.md "wikilink")（James Obiorah）           | 中场 | 23 | 2    | [Lokomotiv Moscow](../Page/Lokomotiv_Moscow.md "wikilink") (Russia)          |
| 21 | [约翰·乌塔卡](../Page/约翰·乌塔卡.md "wikilink")（John Utaka）                  | 前锋 | 19 | 4    | [Al Saad](../Page/Al_Saad.md "wikilink") (Qatar)                             |
| 22 | [文森特·恩亚马](../Page/文森特·恩亚马.md "wikilink")（Vincent Enyeama）           | 门将 | 19 | 2    | [Enyimba](../Page/Enyimba.md "wikilink") (Nigeria)                           |
| 23 | [费米·奥帕班米](../Page/费米·奥帕班米.md "wikilink")（Femi Opabunmi）             | 前锋 | 17 | 2    | [3SC Ibadan](../Page/3SC_Ibadan.md "wikilink") (Nigeria)                     |

## [瑞典](../Page/瑞典国家足球队.md "wikilink")

Head coaches: [拿格碧克](../Page/拿格碧克.md "wikilink") and [Tommy
Söderberg](../Page/Tommy_Söderberg.md "wikilink")

| \# | Name                                                                       | Position | Age | Caps | Team                                                                 |
| -- | -------------------------------------------------------------------------- | -------- | --- | ---- | -------------------------------------------------------------------- |
| 1  | [Magnus Hedman海德曼](../Page/Magnus_Hedman海德曼.md "wikilink")                 | GK       | 30  | 44   | [Coventry City](../Page/Coventry_City_F.C..md "wikilink") (England)  |
| 2  | [Olof Mellberg梅尔贝里](../Page/Olof_Mellberg梅尔贝里.md "wikilink")               | D        | 24  | 21   | [Aston Villa](../Page/Aston_Villa_F.C..md "wikilink") (England)      |
| 3  | [Patrik Andersson帕特里克·安德森](../Page/Patrik_Andersson帕特里克·安德森.md "wikilink") | D        | 30  | 95   | [Barcelona](../Page/FC_Barcelona.md "wikilink") (Spain)              |
| 4  | [Johan Mjällby](../Page/Johan_Mjällby.md "wikilink")                       | D        | 31  | 35   | [Celtic](../Page/Celtic_F.C..md "wikilink") (Scotland)               |
| 5  | [米高·史雲遜](../Page/米高·史雲遜.md "wikilink")                                     | D        | 26  | 11   | [Troyes](../Page/Troyes_AC.md "wikilink") (France)                   |
| 6  | [連達羅夫](../Page/連達羅夫.md "wikilink")                                         | M        | 23  | 19   | [Everton](../Page/Everton_F.C..md "wikilink") (England)              |
| 7  | [阿歷山達臣](../Page/阿歷山達臣.md "wikilink")                                       | M        | 30  | 58   | [Everton](../Page/Everton_F.C..md "wikilink") (England)              |
| 8  | [安達斯·史雲遜](../Page/安達斯·史雲遜.md "wikilink")                                   | M        | 25  | 24   | [Southampton](../Page/Southampton_F.C..md "wikilink") (England)      |
| 9  | [龍格保](../Page/龍格保.md "wikilink")                                           | M        | 25  | 31   | [Arsenal](../Page/Arsenal_F.C..md "wikilink") (England)              |
| 10 | [柯碧克](../Page/柯碧克.md "wikilink")                                           | F        | 28  | 18   | [Heerenveen](../Page/SC_Heerenveen.md "wikilink") (Netherlands)      |
| 11 | [軒歷·拿臣](../Page/軒歷·拿臣.md "wikilink")                                       | F        | 30  | 67   | [Celtic](../Page/Celtic_F.C..md "wikilink") (Scotland)               |
| 12 | [Magnus Kihlstedt](../Page/Magnus_Kihlstedt.md "wikilink")                 | GK       | 30  | 12   | [Copenhagen](../Page/FC_København.md "wikilink") (Denmark)           |
| 13 | [Tomas Antonelius](../Page/Tomas_Antonelius.md "wikilink")                 | D        | 29  | 6    | [Copenhagen](../Page/FC_København.md "wikilink") (Denmark)           |
| 14 | [艾特文](../Page/艾特文.md "wikilink")                                           | D        | 23  | 5    | [Heerenveen](../Page/SC_Heerenveen.md "wikilink") (Netherlands)      |
| 15 | [Andreas Jakobsson](../Page/Andreas_Jakobsson.md "wikilink")               | D        | 29  | 12   | [Hansa Rostock](../Page/Hansa_Rostock.md "wikilink") (Germany)       |
| 16 | [Teddy Lucic](../Page/Teddy_Lucic.md "wikilink")                           | D        | 29  | 41   | [AIK Solna](../Page/AIK_Fotboll.md "wikilink") (Sweden)              |
| 17 | [Magnus Svensson](../Page/Magnus_Svensson.md "wikilink")                   | M        | 33  | 24   | [Brondby](../Page/Brøndby_I.F..md "wikilink") (Denmark)              |
| 18 | [Mattias Jonson](../Page/Mattias_Jonson.md "wikilink")                     | M        | 28  | 23   | [Brondby](../Page/Brøndby_I.F..md "wikilink") (Denmark)              |
| 19 | [Pontus Farnerud](../Page/Pontus_Farnerud.md "wikilink")                   | M        | 22  | 2    | [AS Monaco](../Page/AS_Monaco_FC.md "wikilink") (France)             |
| 20 | [Daniel Andersson](../Page/Daniel_Andersson.md "wikilink")                 | M        | 24  | 38   | [Venezia](../Page/Venezia_A.C..md "wikilink") (Italy)                |
| 21 | [伊卜拉希莫维奇](../Page/伊卜拉希莫维奇.md "wikilink")                                   | F        | 20  | 9    | [Ajax](../Page/Ajax_Amsterdam.md "wikilink") (Netherlands)           |
| 22 | [安達斯·安達臣](../Page/安達斯·安達臣.md "wikilink")                                   | F        | 28  | 32   | [AIK Solna](../Page/AIK_Fotboll.md "wikilink") (Sweden)              |
| 23 | [伊薩臣](../Page/伊薩臣.md "wikilink")                                           | GK       | 20  | 1    | [Djurgaarden](../Page/Djurgårdens_IF_Fotboll.md "wikilink") (Sweden) |

[Category:2002年世界杯足球赛](../Category/2002年世界杯足球赛.md "wikilink")