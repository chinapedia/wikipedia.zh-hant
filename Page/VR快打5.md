《**VR快打5**》（）是[SEGA](../Page/SEGA.md "wikilink")《[VR快打](../Page/VR快打.md "wikilink")》系列遊戲的最新作品。2006年7月12日VR快打5在日本的[大型電玩登場](../Page/街机.md "wikilink")，2007年2月VR快打5在PlayStation
3上發售，隨後同年也在[Xbox 360上發售此作](../Page/Xbox_360.md "wikilink")\[1\]。\[2\]

## 故事

受歡迎的第5屆世界格鬥大賽發送邀請函給世界上17位頂尖的格鬥家，讓他們開始最後的準備階段，他們必須在此時優先了解自己的缺失與強化自己在每一方面的心智、肉體、靈魂，以不讓任何錯誤在這屆大賽中發生，但他們不知道的是，這屆比賽的資助組織－J6，有著別有用心的邪惡目的，那就是組織的最高機密－已在進行中的Dural程式。

J6為了控制世界，組織裡的科學家創造了一個擁有人類特徵的終極戰鬥武器，但第一個模型在第4屆世界格鬥大賽中被擊敗，這迫使他們去綁架凡妮莎，但在他們奪得凡妮莎的戰鬥資料，並將之轉移至V-Dural（新的Dural模型）前，她已在內部人士的幫助下脫逃了。稍後，V-Dural開發完成，J6決定舉辦第5屆世界格鬥大賽，將V-Dural投入大賽中，此外，還要趁這場大賽找出這名放走凡妮莎的背叛組織者。

而第5屆世界格鬥大賽也就此揭開了序幕。

## 登場角色

  - 結城 晶（[:en:Akira Yuki](../Page/:en:Akira_Yuki.md "wikilink")）
  - 陳佩（[:en:Pai Chan](../Page/:en:Pai_Chan.md "wikilink")）
  - 陳洛（[:en:Lau Chan](../Page/:en:Lau_Chan.md "wikilink")）
  - 梅小路 葵（[:en:Aoi
    Umenokouji](../Page/:en:Aoi_Umenokouji.md "wikilink")）
  - Brad Burns（[:en:Brad Burns](../Page/:en:Brad_Burns.md "wikilink")）
  - Dural（[en:Dural](../Page/:en:Dural_\(Virtua_Fighter\).md "wikilink")）
  - 日守 剛（[:en:Goh Hinogami](../Page/:en:Goh_Hinogami.md "wikilink")）
  - 莎菈·布萊恩（[Sarah
    Bryant](../Page/:en:Sarah_Bryant_\(computer_game_character\).md "wikilink")）
  - 傑克·布萊恩（[:en:Jacky Bryant](../Page/:en:Jacky_Bryant.md "wikilink")）
  - Jeffry McWild（[:en:Jeffry
    McWild](../Page/:en:Jeffry_McWild.md "wikilink")）
  - 影丸（[:en:Kage-Maru](../Page/:en:Kage-Maru.md "wikilink")）
  - 雷飛（[:en:Lei-Fei](../Page/:en:Lei-Fei.md "wikilink")）
  - 里昂（[:en:Lion Rafale](../Page/:en:Lion_Rafale.md "wikilink")）
  - 舜帝（[:en:Shun Di](../Page/:en:Shun_Di.md "wikilink")）
  - 凡妮莎·李維絲（[:en:Vanessa
    Lewis](../Page/:en:Vanessa_Lewis.md "wikilink")）
  - 沃爾夫（[:en:Wolf Hawkfield](../Page/:en:Wolf_Hawkfield.md "wikilink")）
  - 愛琳（[Eileen](../Page/:en:Eileen_\(Virtua_Fighter\).md "wikilink")）
  - 艾爾·-{布}-雷茲（[:en:El Blaze](../Page/:en:El_Blaze.md "wikilink")）

## 新增要素（與過去系列作品比較）

### 角色

VR快打5有兩個新角色，第一個角色愛琳是一個年輕女孩，她使用，加入比賽是為了追尋她的偶像－陳佩（之所以會崇拜陳佩，是因為曾看過她展示[武術](../Page/武術.md "wikilink")）。第二個角色艾爾·-{布}-雷茲是一個[自由式摔角手](../Page/自由式摔角.md "wikilink")，參加比賽的目的是為了擊敗沃爾夫。

## 家用機原創要素（與大型電玩版比較）

### PlayStation 3版

  - 訓練模式\[3\]（日文為DOJO モード）：可進行操縱角色招式的輸入練習等。
  - 任務模式\[4\](日文為QUEST モード)：可挑戰眾多虛擬角色，並進行道具搶奪與提昇段位等事項。
  - VF.TV模式\[5\](日文為VF.TV モード)：收錄了大型電玩版的對戰影片，讓遊玩者了解其他人的對戰方式。

## 評分

### PlayStation 3版

  - [Fami通給了](../Page/Fami通.md "wikilink")35分的評分(滿分40分)。[1](https://web.archive.org/web/20160311004748/http://gamebrink.com/playstation-3/2260-virtua_fighter_5-info.html).
  - [Electronic Gaming
    Monthly](../Page/Electronic_Gaming_Monthly.md "wikilink")([:en:Electronic
    Gaming
    Monthly](../Page/:en:Electronic_Gaming_Monthly.md "wikilink"))給了9.5、10、9.5的評分。[2](http://www.1up.com/do/newsStory?cId=3156901)
  - [Computer & Video Games
    (雜誌)](../Page/Computer_&_Video_Games_\(雜誌\).md "wikilink")([:en:Computer
    and Video Games
    (magazine)](../Page/:en:Computer_and_Video_Games_\(magazine\).md "wikilink"))給了9.1分的評分(滿分10分)。[3](https://web.archive.org/web/20071025085436/http://www.gamerankings.com/itemrankings/launchreview.asp?reviewid=761005)
  - [Game Brink](../Page/Game_Brink.md "wikilink")([:en:Game
    Brink](../Page/:en:Game_Brink.md "wikilink"))給了88分的評分(滿分100分)。[4](http://www.gamerankings.com/itemrankings/launchreview.asp?reviewid=760491)
  - [Gamespy給了](../Page/Gamespy.md "wikilink")5顆星的評價(最佳評價為5顆星)[5](http://ps3.gamespy.com/playstation-3/virtua-fighter-5/765787p1.html)
  - [1UP.com給了](../Page/1UP.com.md "wikilink")9.5分的評分(滿分10分)。[6](https://web.archive.org/web/20070927203016/http://www.1up.com/do/reviewPage?cId=3157287&sec=REVIEWS)
  - [Gamespot給了](../Page/Gamespot.md "wikilink")8.1分的評分(滿分10分)。[7](http://www.gamespot.com/ps3/action/virtuafighter5/review.html?sid=6166151&tag=topslot;title;1)
  - [Game Informer](../Page/Game_Informer.md "wikilink")([:en:Game
    Informer](../Page/:en:Game_Informer.md "wikilink"))給了8分的評分(滿分10分)。[8](https://web.archive.org/web/20070304105426/http://www.gameinformer.com/Games/Review/200703/R07.0220.1104.36312.htm)

## 銷售量

  - 日本：PlayStation
    3版的VR快打5首日銷售量有32000套\[6\]，而截至2007年2月18日止，推估已有65835套賣出\[7\]。

## 影响

在2016年由世嘉发售的动作冒险游戏《[如龙6
生命诗篇。](../Page/如龙6_生命诗篇。.md "wikilink")》中，《VR战士5》作为一个在游戏中的街机厅里可以游玩的街机游戏被收录其中。\[8\]

## 參考資源

<references/>

## 外部連結

  - 官方網站

<!-- end list -->

  - [北美網站](https://web.archive.org/web/20080509062557/http://www.sega.com/gamesite/vf5/index.html)
  - [日本網站（大型電玩版）](http://www.virtuafighter.jp/)
  - [日本網站（PlayStation 3版）](http://virtuafighter5.jp/)

<!-- end list -->

  - 一般資源

<!-- end list -->

  - [存放於Game
    Sector網站的VR快打5截圖](http://game-sector.co.uk/gallery/ps3/virtua_fighter_5)
  - [1UP.com網站的VR快打5專區](https://web.archive.org/web/20070927195340/http://www.1up.com/do/gameOverview?cId=3150467)
  - [PS3版（美版）發售日期](http://ps3.ign.com/articles/751/751474p1.html)

[Category:世嘉遊戲](../Category/世嘉遊戲.md "wikilink")
[Category:街機遊戲](../Category/街機遊戲.md "wikilink")
[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:Xbox 360遊戲](../Category/Xbox_360遊戲.md "wikilink")
[Category:格鬥遊戲](../Category/格鬥遊戲.md "wikilink")

1.

2.

3.

4.
5.
6.

7.

8.  [你能够在《如龙6》里玩到《VR战士5
    最终决战》了](http://www.vgtime.com/topic/8879.jhtml).游戏时光.2016-09-07.\[2017-09-21\].