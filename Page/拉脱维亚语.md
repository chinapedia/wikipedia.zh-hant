**拉脱维亚语**（）是[拉脱维亚的官方语言](../Page/拉脱维亚.md "wikilink")。拉脱维亚语在英语中曾被称为**Lettish**，而在大部分[日耳曼语族的语言中仍然是这样称呼](../Page/日耳曼语族.md "wikilink")。在拉脱维亚有大约130万人以其为母语，而在海外有约10万。总共有200万，也即80%的拉脱维亚人口使用拉脱维亚语。\[1\]而在其中有116万，也即约56%的人在国内将其当作主要语言。\[2\]在日常生活中使用拉脱维亚语的比例在多个地区均有上升趋势。\[3\]

拉脱维亚语是现存的两种[波罗的语族之一](../Page/波罗的语族.md "wikilink")（另一种是[立陶宛语](../Page/立陶宛语.md "wikilink")，与其相近）。波罗的语族隶属于[印欧语系](../Page/印欧语系.md "wikilink")。另外有争议质疑能够和拉脱维亚语相互沟通的[拉特加莱语和](../Page/拉特加莱语.md "wikilink")[克罗尼语是否应该被看作不同的独立语言](../Page/克罗尼语.md "wikilink")。

拉脱维亚语最早在16世纪中期出现在西方出版物中，在[塞巴斯丁·缪斯特的](../Page/塞巴斯丁·缪斯特.md "wikilink")《世界志》（1544）中对[主祷文进行了拉脱维亚语的翻译](../Page/主祷文.md "wikilink")。

## 發音

### 子音

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p><a href="../Page/雙唇音.md" title="wikilink">雙唇音</a></p></th>
<th><p><a href="../Page/齒齦音.md" title="wikilink">齒齦音</a></p></th>
<th><p><a href="../Page/齦後音.md" title="wikilink">齦後音</a></p></th>
<th><p><a href="../Page/硬顎音.md" title="wikilink">硬顎音</a></p></th>
<th><p><a href="../Page/軟顎音.md" title="wikilink">軟顎音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td><p>  </p></td>
<td><p>  </p></td>
<td><p> </p></td>
<td><p>  </p></td>
<td><p>  </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td><p> </p></td>
<td><p>  </p></td>
<td><p>  </p></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td><p>()  </p></td>
<td><p>  </p></td>
<td><p>  </p></td>
<td></td>
<td><p>()</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/近音.md" title="wikilink">近音</a></p></td>
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/邊音.md" title="wikilink">邊音</a></p></td>
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/顫音.md" title="wikilink">顫音</a></p></td>
<td><p> </p></td>
<td></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

### 母音

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p><a href="../Page/前母音.md" title="wikilink">前</a></p></th>
<th><p><a href="../Page/央母音.md" title="wikilink">央</a></p></th>
<th><p><a href="../Page/後母音.md" title="wikilink">後</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>短</p></td>
<td><p>長</p></td>
<td><p>短</p></td>
<td><p>長</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/閉母音.md" title="wikilink">閉母音</a></p></td>
<td></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/半開母音.md" title="wikilink">半開母音</a></p></td>
<td></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開母音.md" title="wikilink">開母音</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 拼写法

拉脫維亞語共33個字母：

  - 具[長音符號的代表](../Page/長音符號.md "wikilink")[長母音](../Page/長母音.md "wikilink")

## 參考資料

## 外部链接

  - [Letonika](http://www.letonika.lv/)
  - [English-Latvian/Latvian-English
    dictionary](http://www.letonika.lv/dictionary/)
  - [Latvian English
    Dictionary](https://web.archive.org/web/20070916092434/http://www.websters-online-dictionary.org/definition/Latvian-english/)
    from [Webster's Online
    Dictionary](https://web.archive.org/web/20120223164907/http://www.websters-online-dictionary.org/)
    - the Rosetta Edition
  - [Russian-Latvian/Latvian-Russian
    dictionary](http://www.multitran.ru)
  - [National Agency for Latvian Language
    Training](http://www.lvavp.lv/)
  - [The Latvian
    Alphabet](http://www.languagehelpers.com/Latvia/TheLatvianAlphabet.html)
  - [Examples of Latvian words and phrases (with
    sound)](http://www.languagehelpers.com)
  - [Bilingual (Latvian and English)
    e-zine](http://www.torontozinas.com)



[Category:波羅的語族](../Category/波羅的語族.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")
[Category:拉脱维亚语言](../Category/拉脱维亚语言.md "wikilink")

1.  [Dažādu tautu valodu
    prasme](http://www.vvk.lv/index.php?sadala=129&id=389)
2.  [RESIDENT POPULATION ON MARCH 1, 2011 BY LANGUAGE MOSTLY SPOKEN AT
    HOME, GENDER AND AGE
    GROUP](http://data.csb.gov.lv/pxweb/lv/tautassk_11/tautassk_11__tsk2011/TSG11-071.px/?rxid=c6397dfa-547d-4de0-afac-3791d8f0957b)
3.  [Krievvalodīgie arvien vairāk runā
    latviski](http://www.delfi.lv/archive/article.php?id=20135719)