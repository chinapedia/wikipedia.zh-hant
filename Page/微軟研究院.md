**微軟研究院**（，[縮寫为MSR](../Page/縮寫.md "wikilink")）是[微軟公司在](../Page/微軟公司.md "wikilink")1991年創立硏究不同的[计算机科学主題與問題的分部](../Page/计算机科学.md "wikilink")。微軟研究院其中一個固定的目標為「支持長期的電腦科學硏究而不受產品週期所限」\[1\]。

微軟研究院是目前世界頂尖的研究中心之一，有很多在電腦科學、[物理學](../Page/物理學.md "wikilink")、[數學受到高度公認的專家及許多著名科學獎項得主在微軟研究院](../Page/數學.md "wikilink")，包括[圖靈獎得主](../Page/圖靈獎.md "wikilink")[東尼·霍爾](../Page/東尼·霍爾.md "wikilink")、[詹姆斯·尼古拉·格雷](../Page/詹姆斯·尼古拉·格雷.md "wikilink")，[菲爾茲獎得主Michael](../Page/菲爾茲獎.md "wikilink")
Freedman，[沃爾夫獎得主Laszlo](../Page/沃爾夫獎.md "wikilink") Lovasz，MacArthur
Fellow獎金得主Jim Blinn與Dijkstra Prize獎得主Leslie Lamport。

## 研究領域

微軟研究院的研究範疇可以被歸類為10大項：\[2\]

1.  [演算法與理論](../Page/演算法.md "wikilink")
2.  硬體發展
3.  [人機互動](../Page/人機互動.md "wikilink")
4.  機械的學習、適應與智慧
5.  [多媒體與影像技術](../Page/多媒體.md "wikilink")
6.  資料搜索、擷取與[知識管理](../Page/知識管理.md "wikilink")
7.  資訊安全與加密技術
8.  社會計算
9.  軟體發展
10. 系統、[计算机体系结构](../Page/计算机体系结构.md "wikilink")、可攜系統與網路

微軟研究院有提供被稱為「微軟學者」的人才培育專案，微軟研究院每年會提供微軟學者獎學金與在國際、研究院的學習機會給學生。

## 全球各地的研究院

微軟研究院於[班加羅爾](../Page/班加羅爾.md "wikilink")、[北京](../Page/北京.md "wikilink")、[劍橋](../Page/劍橋.md "wikilink")、[矽谷](../Page/矽谷.md "wikilink")、[雷德蒙德與](../Page/雷德蒙德_\(華盛頓州\).md "wikilink")[舊金山擁有實驗室](../Page/舊金山.md "wikilink")。\[3\]

  - **微軟雷德蒙德研究院**：1991年設立於美國[華盛頓州微軟雷德蒙德園區](../Page/華盛頓州.md "wikilink")（Redmond，微軟總公司所在地），雷德蒙德研究院常與來自英國劍橋、中國北京、舊金山的研究員合作交流。
  - **[微軟亞洲研究院](../Page/微軟亞洲研究院.md "wikilink")**（MSRA）：1998年在北京成立，有上百位來自全球的研究人員與科學家，還有200多位客座研究員與學生支援研究，主要研究改良的[使用者介面](../Page/使用者介面.md "wikilink")、網路與[無線通訊](../Page/無線通訊.md "wikilink")，和新世代多媒體與亞洲訊息處理技術等。
  - **微軟英國劍橋研究院**：1997年成立於英國劍橋，有超過百名的研究人員，主要研究[機械學習](../Page/機械學習.md "wikilink")、[資訊安全](../Page/資訊安全.md "wikilink")、[資料探勘](../Page/資料探勘.md "wikilink")、[作業系統](../Page/作業系統.md "wikilink")、編程與網路技術等。微軟英國研究所也與[劍橋大學的計算機研究室有密切的聯繫](../Page/劍橋大學.md "wikilink")。
  - **微軟矽谷研究院**：2001年成立於[矽谷](../Page/矽谷.md "wikilink")。研究人員約25人，主要研究[分布式運算](../Page/分布式運算.md "wikilink")，還有包括資訊安全、[可靠性](../Page/可靠性.md "wikilink")、協定、[資源定址器等研究](../Page/資源定址器.md "wikilink")。2006年8月與微軟灣區研究中心（BARC）合併。
  - **微軟印度研究院**：2005年1月成立於[班加羅爾](../Page/班加羅爾.md "wikilink")，研究人員約50人，並有大批實習生支援研究。主要再進行多領域的基礎研究及應用研究，並與多所學校與研究機構有密切交流。

## 參考文獻

## 外部連結

  - [官方網站](http://research.microsoft.com/)

## 參見

  - [微軟亞洲研究院](../Page/微軟亞洲研究院.md "wikilink")
  - [微軟歐洲科學獎](../Page/微軟歐洲科學獎.md "wikilink")

{{-}}

[Category:微软的部门与子公司](../Category/微软的部门与子公司.md "wikilink")
[Category:计算机科学研究机构](../Category/计算机科学研究机构.md "wikilink")
[Category:信息技术研究机构](../Category/信息技术研究机构.md "wikilink")
[Category:软件研究机构](../Category/软件研究机构.md "wikilink")
[Category:1991年建立的组织](../Category/1991年建立的组织.md "wikilink")
[Category:1991年美國建立](../Category/1991年美國建立.md "wikilink")

1.  <http://research.microsoft.com/aboutmsr/overview/default.aspx>
2.  <http://research.microsoft.com/research/default.aspx>
3.  <http://research.microsoft.com/en-us/research/default.aspx>