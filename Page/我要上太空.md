《**我要上太空**》（）是[柳沼行原作的](../Page/柳沼行.md "wikilink")[日本漫畫作品](../Page/日本漫畫.md "wikilink")。其後被[動畫化](../Page/日本動畫.md "wikilink")，並於[日本放送協會的BS](../Page/日本放送協會.md "wikilink")2頻道首播。首播時間為2003年11月1日至2004年3月27日，全20集。香港方面，動畫版由[無綫電視翡翠台於](../Page/無綫電視.md "wikilink")2005年4月9日至6月18日首播，劇集版於2009年6月18日開始在[NHK播放](../Page/NHK.md "wikilink")，全7集。動畫版和劇集版兩者有些內容會不同。

## 劇情簡介

載人宇宙探測[火箭](../Page/火箭.md "wikilink")「獅子號」失事墜毀，故事主人公鴨川明日美的母親鴨川今日子被捲入事故中，意外身亡，當時明日美只有一歲。五年後，明日美與一位自稱為「拉岸」的[宇航員的](../Page/宇航員.md "wikilink")[靈魂相遇](../Page/靈魂.md "wikilink")。自此以後，明日美便對宇宙產生了興趣，並立志成為一位“[火箭駕駛員](../Page/宇航員.md "wikilink")”。

## 主要人物

  - 鴨川明日美　（[配音員](../Page/配音員.md "wikilink")：[矢島晶子](../Page/矢島晶子.md "wikilink")
    / [林元春](../Page/林元春.md "wikilink") /
    演員：[樱庭奈奈美](../Page/樱庭奈奈美.md "wikilink")）
      -
        夢想成為火箭的司機(宇宙飛機駕駛員)的及以其作為目標而來到東京宇宙學校。純粹積極的女孩子。左撇子。身長約143厘米身材短小，不過跑得很快，英語以外有取滿分的實力(第六集單行本)，幾乎背著星的位置和距離(第一集單行本)。母親鴨川今日子是獅子號墜落事件的犧牲者(受災後5年以植物人狀態生存著)。
  - 拉岸 （為日語英文：Mr.
    Lion，故香港電視台譯作"拉岸")，配音員：[子安武人](../Page/子安武人.md "wikilink")
    / [陳卓智](../Page/陳卓智.md "wikilink")）

<!-- end list -->

  -
    獅子號飛行員的幽靈。明日美叫他做拉岸哥哥(台譯獅子先生)，原姓**高野**，是明日美小學時的老師-鈴成由子的原戀人。跟宇喜多萬里香很相似的少女(其實是2024年的宇喜多所複製的對象，在遺傳學上的姐姐)的初戀對象。他頭上戴著獅子公仔頭，如果拿起來的，他就會升天（是在柳沼的第一篇漫畫，2015年綻放的煙火中的獅子先生說的，而未知是否與我要上太空中的設定一樣）。在漫畫裡，明日美小學的時候，到去東京宇宙學校讀書的初期，都留在唯濱(明日美的家鄉，獅子號墜落的地方)，直至到鈴成由子在唯濱結婚為止。之後，他就跟隨明日美到東京，住在明日美宿舍裡的雜物房。平時，除了明日美及一些快要離開世界的人(例如：島津武-見"明美的櫻花-單行本3，就沒有其他人看見過他
    拉岸的名字，沒有看過日本動畫或慬日文的粵語人士可能不明白，因為日本人讀的英語不太像英文，故此，拉岸可以當作為日語英文的音譯多於純正英文的音譯。
    在動畫版中，最終回，宇喜多万里香、府中野新之介、近江圭、鈴木秋也看到獅子先生。
    而高野死後，採用獅子的形象，而在現世中出現，是有解釋。一、高野所乘坐的大空船為**獅子號**。二、高野的少年時代、與在唯濱療養的宇喜多万里香（遺傳學上的姐姐）一起玩耍時，高野扮演**美女與野獸**中的野獸(實為王子)與宇喜多一起跳舞，野獸形象是高野用紙做獅子樣子的面具。故有高野=獅子的意義。

<!-- end list -->

  - 宇喜多萬里香　（配音員：[木村亞希子](../Page/木村亞希子.md "wikilink") /
    [陸惠玲](../Page/陸惠玲.md "wikilink") /
    演員：[足立梨花](../Page/足立梨花.md "wikilink")）
  - 府中野新之介　（配音員：[豊永利行](../Page/豊永利行.md "wikilink") /
    [梁偉德](../Page/梁偉德.md "wikilink") /
    演員：[大東駿介](../Page/大東駿介.md "wikilink")）
  - 近江圭　（配音員：[大浦冬華](../Page/大浦冬華.md "wikilink") /
    [曾秀清](../Page/曾秀清.md "wikilink") /
    演員：[高山侑子](../Page/高山侑子.md "wikilink")）
  - 鈴木秋　（配音員：[甲斐田幸](../Page/甲斐田幸.md "wikilink") /
    [鄺樹培](../Page/鄺樹培.md "wikilink") /
    演員：[中村優一](../Page/中村優一.md "wikilink")）

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p><a href="../Page/Media_Factory.md" title="wikilink">Media Factory</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2002年1月31日</p></td>
<td><p>ISBN 4-8401-0428-X</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2002年4月30日</p></td>
<td><p>ISBN 4-8401-0440-9</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2002年11月30日</p></td>
<td><p>ISBN 4-8401-0468-9</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2003年5月31日</p></td>
<td><p>ISBN 4-8401-0490-5</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2003年10月31日</p></td>
<td><p>ISBN 4-8401-0906-0</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2004年4月30日</p></td>
<td><p>ISBN 4-8401-0944-3</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2004年12月31日</p></td>
<td><p>ISBN 4-8401-0984-2</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2005年5月31日</p></td>
<td><p>ISBN 4-8401-1307-6</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>2005年12月31日</p></td>
<td><p>ISBN 4-8401-1349-1</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2006年3月31日</p></td>
<td><p>ISBN 4-8401-1377-7</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>2006年11月30日</p></td>
<td><p>ISBN 4-8401-1635-0</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2007年3月23日</p></td>
<td><p>ISBN 978-4-8401-1687-9</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2007年12月22日</p></td>
<td><p>ISBN 978-4-8401-1984-9</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2008年3月22日</p></td>
<td><p>ISBN 978-4-8401-2205-4</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>2009年6月23日</p></td>
<td><p>ISBN 978-4-8401-2576-5</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>2009年10月23日</p></td>
<td><p>ISBN 978-4-8401-2923-7</p></td>
</tr>
</tbody>
</table>

  - 小說
    小說版由動畫導演望月智充執筆撰寫。和原作設定有很大的差異。由原作者柳沼行負責插畫。

<!-- end list -->

  - 宇宙へのいちばん星
      - 上卷 - 2004年4月19日發售、ISBN 4-8401-1062-X
      - 下卷 - 2004年4月30日發售、ISBN 4-8401-1080-8

<!-- end list -->

  - 其他書籍

<!-- end list -->

  - オフィシャルブック ふたつのスピカ アニメ設定資料集 - 2004年4月19日發售、ISBN 4-8401-1041-7
  - ふたつのスピカ イラストブック - 2006年3月31日發售、ISBN 4-8401-1380-7

## 電視動畫

### 製作團隊

  - 原作 - 柳沼行
  - 監督、系列構成 - [望月智充](../Page/望月智充.md "wikilink")
  - 人物設計 - 後藤真砂子
  - 設定考察、概念設計 - [小倉信也](../Page/小倉信也.md "wikilink")
  - 美術監督 - 込山明日香
  - 色彩設定 - 一瀬美代子
  - 數位合成（攝影監督） - 伊藤正弘
  - 音響監督 - [鄉田穗積](../Page/鄉田穗積.md "wikilink")
  - 音樂 - [三宅一德](../Page/三宅一德.md "wikilink")
  - 動畫製作 - [Group TAC](../Page/Group_TAC.md "wikilink")
  - 動畫製作人 - 桜井宏、橋本淳至
  - 製作人 - 森千加代、近藤栄三
  - 製作統籌 - 柏木敦子、植原智幸
  - 製作、著作 - [NHK](../Page/日本放送協会.md "wikilink")
  - 共同製作 - [NHK ENTERPRISE
    21](../Page/NHK_ENTERPRISES.md "wikilink")、[綜合Vision](../Page/綜合Vision.md "wikilink")

### 主題曲

  - 片頭曲「[Venus Say](../Page/鯨_\(歌曲\).md "wikilink")」
    作詞 - [新藤晴一](../Page/新藤晴一.md "wikilink") / 作曲・編曲 -
    [本間昭光](../Page/本間昭光.md "wikilink") / 歌 -
    [Buzy](../Page/Buzy.md "wikilink")
      - 正式曲名為「Venus Say…」。
  - 片尾曲「[見上げてごらん夜の星を](../Page/見上げてごらん夜の星を.md "wikilink")」
    作詞 - [永六輔](../Page/永六輔.md "wikilink") / 作曲 -  / 編曲・歌 -
    [BEGIN](../Page/BEGIN.md "wikilink")

### 各話列表

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>標題</p></th>
<th><p>腳本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
<th><p>放送日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td></td>
<td><p><a href="../Page/望月智充.md" title="wikilink">望月智充</a></p></td>
<td><p>佐佐木和宏</p></td>
<td><p>窪敏</p></td>
<td><p><strong>2003年</strong><br />
11月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第2話</p></td>
<td></td>
<td><p><a href="../Page/中瀬理香.md" title="wikilink">中瀬理香</a></p></td>
<td><p>安芸紀子</p></td>
<td><p><a href="../Page/佐佐木勝利.md" title="wikilink">佐佐木勝利</a></p></td>
<td><p>我妻宏</p></td>
<td><p>11月8日</p></td>
</tr>
<tr class="odd">
<td><p>第3話</p></td>
<td></td>
<td><p><a href="../Page/佐藤英一.md" title="wikilink">佐藤英一</a></p></td>
<td><p><a href="../Page/山本裕介.md" title="wikilink">山本裕介</a></p></td>
<td><p>古田誠</p></td>
<td><p>11月15日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4話</p></td>
<td></td>
<td><p><a href="../Page/青山弘.md" title="wikilink">河合夢男</a></p></td>
<td><p>柳瀬雄之</p></td>
<td><p>11月22日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5話</p></td>
<td></td>
<td><p>望月智充</p></td>
<td><p>萩原露光</p></td>
<td><p>松田芳明</p></td>
<td><p>11月29日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6話</p></td>
<td></td>
<td><p>中瀬理香</p></td>
<td><p>山崎たかし</p></td>
<td><p>木鈴芳</p></td>
<td><p>山口保則</p></td>
<td><p>12月6日</p></td>
</tr>
<tr class="odd">
<td><p>第7話</p></td>
<td></td>
<td><p>洪憲杓</p></td>
<td><p>窪敏<br />
長谷部敦志</p></td>
<td><p>12月13日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8話</p></td>
<td></td>
<td><p>安芸紀子</p></td>
<td><p>佐佐木勝利</p></td>
<td><p>吾妻宏</p></td>
<td><p>12月20日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第9話</p></td>
<td></td>
<td><p>望月智充</p></td>
<td><p><a href="../Page/大沼心.md" title="wikilink">大沼心</a></p></td>
<td><p>古田誠</p></td>
<td><p><strong>2004年</strong><br />
1月10日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第10話</p></td>
<td></td>
<td><p>中瀬理香</p></td>
<td><p>山本裕介</p></td>
<td><p>加藤顕</p></td>
<td><p>柳瀬雄之</p></td>
<td><p>1月17日</p></td>
</tr>
<tr class="odd">
<td><p>第11話</p></td>
<td></td>
<td><p>萩原露光</p></td>
<td><p>川口弘明</p></td>
<td><p>1月24日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第12話</p></td>
<td></td>
<td><p>望月智充</p></td>
<td><p>洪憲杓</p></td>
<td><p>窪敏</p></td>
<td><p>1月31日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第13話</p></td>
<td></td>
<td><p>中瀬理香</p></td>
<td><p><a href="../Page/高柳哲司.md" title="wikilink">高柳哲司</a></p></td>
<td><p>木鈴芳</p></td>
<td><p>山口保則</p></td>
<td><p>2月7日</p></td>
</tr>
<tr class="even">
<td><p>第14話</p></td>
<td></td>
<td><p>安芸紀子</p></td>
<td><p>佐々木勝利</p></td>
<td><p>権允姫</p></td>
<td><p>2月14日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第15話</p></td>
<td></td>
<td><p>山崎たかし</p></td>
<td><p>太田知章</p></td>
<td><p>粉川剛</p></td>
<td><p>2月21日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第16話</p></td>
<td></td>
<td><p>望月智充</p></td>
<td><p>山本秀世</p></td>
<td><p>桜井正明</p></td>
<td><p>2月28日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第17話</p></td>
<td></td>
<td><p>中瀬理香</p></td>
<td><p>萩原露光</p></td>
<td><p>川口弘明</p></td>
<td><p>3月6日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第18話</p></td>
<td></td>
<td><p><a href="../Page/康村諒.md" title="wikilink">康村諒</a></p></td>
<td><p>木鈴芳</p></td>
<td><p>山口保則</p></td>
<td><p>3月13日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第19話</p></td>
<td></td>
<td><p>安芸紀子</p></td>
<td><p>洪憲杓</p></td>
<td><p>窪敏</p></td>
<td><p>3月20日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第20話</p></td>
<td></td>
<td><p>望月智充</p></td>
<td><p>後藤真砂子</p></td>
<td><p>3月27日</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 播放期間

  - 1回目 - 2003年11月1日 -
    2004年3月27日、[NHK](../Page/日本放送協会.md "wikilink")-[BS2](../Page/NHK衛星第2頻道.md "wikilink")、星期六
    9時00分 - 9時25分 （[衛星動畫劇場](../Page/衛星動畫劇場.md "wikilink") 第3頻道）
  - 2回目 - 2005年2月3日 - 6月16日、[NHK教育](../Page/NHK教育電視台.md "wikilink")、星期四
    0時25分 - 0時50分
  - 3回目 - 2006年1月13日 -
    3月17日、NHK[數位教育](../Page/NHK教育電視台.md "wikilink")3ch、星期五
    22時00分 - 22時50分 （2話連續×10回）
  - 4回目 - 2006年10月16日 - 10月27日、NHK-BS2、平日 5時00分 - 5時50分 （2話連續×10回）
  - 5回目 - 2009年6月30日 - 、NHK-BShi、星期二 19時25分 - 19時50分

## 電視劇

  - NHK綜合台「Drama 8」於2009年6月18日起，逢星期四20:00 - 20:45（日本時間）播出。

### 演員

  - 鴨川明日美：[櫻庭奈奈美](../Page/櫻庭奈奈美.md "wikilink")（兒時：[大森絢香](../Page/大森絢香.md "wikilink")）
  - 鈴木秋：[中村優一](../Page/中村優一.md "wikilink")
  - 府中野新之介：[大東俊介](../Page/大東俊介.md "wikilink")（兒時：[柴田洸翔](../Page/柴田洸翔.md "wikilink")）
  - 宇喜多萬里香：[足立梨花](../Page/足立梨花.md "wikilink")
  - 近江圭：[高山侑子](../Page/高山侑子.md "wikilink")
  - 拝島涼子：
  - 桐生春樹：[向井理](../Page/向井理.md "wikilink")
  - 宇喜多千里：[RIKACO](../Page/RIKACO.md "wikilink")
  - 大西耕次郎：[Golgo松本](../Page/Golgo松本.md "wikilink")（[TIM](../Page/TIM_\(日本搞笑搭檔\).md "wikilink")）
  - 秋葉吾朗：
  - 藤井ルミ：[霧島麗香](../Page/霧島麗香.md "wikilink")
  - 鴨川今日子：[原史奈](../Page/原史奈.md "wikilink")
  - 鴨川友朗：[高嶋政宏](../Page/高嶋政宏.md "wikilink")
  - 塩見敏子：
  - 佐野貴仁：[田邊誠一](../Page/田邊誠一.md "wikilink")

### 製作團隊

  - 製作統籌 - [橘康仁](../Page/橘康仁.md "wikilink")、加賀田透、遠藤理史
  - 道具製作人 - 竹内敬明
  - 腳本 -
    [荒井修子](../Page/荒井修子.md "wikilink")、[松居大悟](../Page/松居大悟.md "wikilink")
  - 演出 - 山本剛義、[塚原亞由子](../Page/塚原亞由子.md "wikilink")
  - 音樂 - [梅堀淳](../Page/梅堀淳.md "wikilink")
  - 攝影協力 - JAXA（[宇宙航空研究開發機構](../Page/宇宙航空研究開發機構.md "wikilink")）
  - 機器協力 - [千葉工業大學](../Page/千葉工業大學.md "wikilink") 未來機器人技術研究中心
  - 映像提供 - JAXA、[NASA](../Page/NASA.md "wikilink")
  - 製作電視台 - [NHK ENTERPRISE](../Page/NHK_ENTERPRISE.md "wikilink")
  - 製作、著作 -
    [NHK](../Page/日本放送協會.md "wikilink")、[DREAMAX電視台](../Page/DREAMAX電視台.md "wikilink")

### 主題曲

  - [ORANGE RANGE](../Page/ORANGE_RANGE.md "wikilink")
    『[瞳の先に](../Page/瞳の先に.md "wikilink")』

### 播放集數

<table>
<thead>
<tr class="header">
<th><p>各話</p></th>
<th><p>播放日期</p></th>
<th><p>標題</p></th>
<th><p>腳本</p></th>
<th><p>演出</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1回</p></td>
<td><p>6月18日</p></td>
<td><p>!</p></td>
<td><p>荒井修子</p></td>
<td><p>山本剛義</p></td>
<td><p>3.4%</p></td>
</tr>
<tr class="even">
<td><p>第2回</p></td>
<td><p>6月25日</p></td>
<td></td>
<td><p>塚原あゆ子</p></td>
<td><p>3.0%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第3回</p></td>
<td><p>7月2日</p></td>
<td></td>
<td><p>松居大悟</p></td>
<td><p>3.1%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4回</p></td>
<td><p>7月9日</p></td>
<td></td>
<td><p>山本剛義</p></td>
<td><p>3.2%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5回</p></td>
<td><p>7月16日</p></td>
<td></td>
<td><p>3.5%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6回</p></td>
<td><p>7月23日</p></td>
<td></td>
<td><p>荒井修子</p></td>
<td><p>塚原あゆ子</p></td>
<td><p>3.5%</p></td>
</tr>
<tr class="odd">
<td><p>最終回</p></td>
<td><p>7月30日</p></td>
<td></td>
<td><p>山本剛義</p></td>
<td><p>3.1%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>平均收視率 3.2%（收視率參考<a href="../Page/關東地方.md" title="wikilink">關東地區</a>、由<a href="../Page/Video_Research.md" title="wikilink">Video Research進行社調</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 作品評價

## 外部連結

  - [美國國家航空暨太空總署（美國）](http://www.nasa.gov/home/index.html)（英文）
  - [中国航天科技集团公司（中國）](http://www.spacechina.com/n25/index.html)（中文）
    \[國家航天局\]
  - [宇宙航空研究開発機構（日本）](http://www.jaxa.jp/)（日文,英文）
  - [韓國航空宇宙研究院（南韓）](https://web.archive.org/web/20110113174954/http://www.kari.re.kr/)（韓文,英文）
  - [國家太空中心（臺灣）](http://www.nspo.narl.org.tw/tw/)（中文,英文）
  - [我要上太空（NHK官方網頁）](https://web.archive.org/web/20050826111343/http://www3.nhk.or.jp/anime/spica/index.html)（日文,動畫）
  - [我要上太空（NHK官方網頁）](https://web.archive.org/web/20091004094437/http://www.nhk.or.jp/drama8/spica/)（日文,電視劇集）

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:校園漫畫](../Category/校園漫畫.md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:青年漫畫](../Category/青年漫畫.md "wikilink")
[Category:2003年日本電視動畫](../Category/2003年日本電視動畫.md "wikilink")
[Category:NHK動畫](../Category/NHK動畫.md "wikilink")
[Category:衛星動畫劇場](../Category/衛星動畫劇場.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:科幻動畫](../Category/科幻動畫.md "wikilink")
[Category:校園動畫](../Category/校園動畫.md "wikilink")
[Category:2009年日本電視劇集](../Category/2009年日本電視劇集.md "wikilink")
[Category:電視劇8](../Category/電視劇8.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:日本漫畫改編日本電視劇](../Category/日本漫畫改編日本電視劇.md "wikilink")
[Category:日本科幻劇](../Category/日本科幻劇.md "wikilink")
[Category:日本校園劇](../Category/日本校園劇.md "wikilink")
[Category:天文題材作品](../Category/天文題材作品.md "wikilink")