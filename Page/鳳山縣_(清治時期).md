**鳳山縣**（；1684年－1895年），為[臺灣清治時期之行政區劃](../Page/臺灣清治時期.md "wikilink")。

1683年（[清](../Page/清.md "wikilink")[康熙二十二年](../Page/康熙.md "wikilink")，[南明](../Page/南明.md "wikilink")[永曆三十七年](../Page/永曆.md "wikilink")），[明鄭滅亡](../Page/明鄭.md "wikilink")，經[施琅力陳](../Page/施琅.md "wikilink")，次年4月正式設立[臺灣府](../Page/臺灣府.md "wikilink")，隸屬於[福建省](../Page/福建省.md "wikilink")，並將明鄭時期[承天府](../Page/承天府.md "wikilink")、[天興州](../Page/天興州.md "wikilink")、[萬年州的行政區域略加調整](../Page/萬年州.md "wikilink")，改為[諸羅](../Page/諸羅縣.md "wikilink")、[臺灣](../Page/臺灣縣.md "wikilink")、鳳山三縣，鳳山縣即承繼了原萬年州的永寧里、新昌里、依仁里、長治里、維新里、嘉祥里、仁壽里。鳳山縣治設在興隆庄（今[高雄市](../Page/高雄市.md "wikilink")[左營區](../Page/左營區.md "wikilink")），典史署在縣治之西，巡檢司署在下淡水里東港。行政範圍包括：東至[下淡水溪](../Page/高屏溪.md "wikilink")，西至[打鼓山港](../Page/壽山_\(高雄市\).md "wikilink")，南至[沙馬磯頭](../Page/墾丁國家公園#.E8.B2.93.E9.BC.BB.E9.A0.AD.md "wikilink")，北至[台灣縣文賢里](../Page/台灣縣.md "wikilink")，二贊行溪（二層行溪，今[二仁溪](../Page/二仁溪.md "wikilink")），另有溪北緊臨的[依仁里](../Page/依仁里.md "wikilink")，以及文賢里北部的飛地（永寧里、新昌里、安平鎮）。

1734年（[雍正十二年](../Page/雍正.md "wikilink")），原屬鳳山縣的永寧里、新昌里、依仁里、[效忠里](../Page/效忠里.md "wikilink")（原安平鎮，康熙六十一年改名）、土墼埕保、羅漢門庄併入[臺灣縣](../Page/臺灣縣.md "wikilink")。1786年（[乾隆五十一年](../Page/乾隆.md "wikilink")）發生[林爽文事件](../Page/林爽文事件.md "wikilink")，縣城兩度毀於兵災，遂將鳳山縣治遷往埤頭街（今[高雄市](../Page/高雄市.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")）。1875年（[光緒元年](../Page/光緒.md "wikilink")），拆分[率芒溪以南之地另置](../Page/率芒溪.md "wikilink")[恆春縣](../Page/恆春縣.md "wikilink")。1887年（光緒十二年），上級台灣府改稱[台南府](../Page/台南府.md "wikilink")。1893年時人口約394,000人。

1895年（光緒二十一年）臺灣改隸[日本](../Page/日本.md "wikilink")，此行政區劃持續至[臺灣總督府將](../Page/臺灣總督府.md "wikilink")[臺南府下各縣合併成](../Page/臺南府.md "wikilink")[臺南縣為止](../Page/臺南縣_\(日治時期\).md "wikilink")。

## 行政區劃

1685年（[康熙二十四年](../Page/康熙.md "wikilink")），鳳山縣下轄里七、庄二、鎮一、社十二\[1\]：

  - 里：[依仁里](../Page/依仁里.md "wikilink")、[永寧里](../Page/永寧里_\(堡里\).md "wikilink")、[新昌里](../Page/新昌里_\(堡里\).md "wikilink")、[長治里](../Page/長治里.md "wikilink")、[嘉祥里](../Page/嘉祥里.md "wikilink")、[維新里](../Page/維新里_\(堡里\).md "wikilink")、[仁壽里](../Page/仁壽里_\(高雄市堡里\).md "wikilink")
  - 庄：[觀音-{庄}-](../Page/觀音庄.md "wikilink")、[鳳山-{庄}-](../Page/鳳山庄.md "wikilink")
  - 鎮：[安平鎮](../Page/安平鎮.md "wikilink")
  - 社：[力力社](../Page/力力社.md "wikilink")、[茄藤社](../Page/茄藤社.md "wikilink")、[放索社](../Page/放索社.md "wikilink")、[下淡水社](../Page/下淡水社.md "wikilink")、[上淡水社](../Page/上淡水社.md "wikilink")、[阿猴社](../Page/阿猴社.md "wikilink")、[搭樓社](../Page/搭樓社.md "wikilink")、[大澤磯社](../Page/大澤磯社.md "wikilink")、[郎嬌社](../Page/郎嬌社.md "wikilink")、[琉球社](../Page/琉球社.md "wikilink")、[南覔社](../Page/南覔社.md "wikilink")、[加六堂社](../Page/加六堂社.md "wikilink")

1694年（康熙三十三年），鳳山縣下轄里十、保三、莊五、社十二\[2\]：

  - 里：新昌里、永寧里、依仁里、長治里、維新里、仁壽里、嘉祥里、[觀音山里](../Page/觀音山里.md "wikilink")、[淡水港東里](../Page/淡水港東里.md "wikilink")、[淡水港西里](../Page/淡水港西里.md "wikilink")
  - 保：[土墼埕保](../Page/土墼埕保.md "wikilink")、[安平鎮保](../Page/安平鎮保.md "wikilink")、[喜樹仔保](../Page/喜樹仔保.md "wikilink")
  - 莊：[半屏山莊](../Page/半屏山莊.md "wikilink")、[興隆莊](../Page/興隆莊.md "wikilink")、[赤山莊](../Page/赤山莊.md "wikilink")、[大小竹橋莊](../Page/大小竹橋莊.md "wikilink")、[鳳山莊](../Page/鳳山莊.md "wikilink")
  - 社：上淡水社、阿猴社、搭樓社、大澤機社、下淡水社、力力社、茄藤社、放索社、[小琉球社](../Page/小琉球社.md "wikilink")、加六堂社、郎嬌社、[卑南覔社](../Page/卑南覔社.md "wikilink")

1712年（康熙五十一年），鳳山縣轄區不變，僅「-{莊}-」改「-{庄}-」\[3\]。

1734年（[雍正十二年](../Page/雍正.md "wikilink")），原屬鳳山縣的永寧里、新昌里、依仁里、[效忠里](../Page/效忠里.md "wikilink")（原安平鎮，康熙六十一年改名）、土墼埕保、羅漢門庄併入[臺灣縣](../Page/臺灣縣.md "wikilink")。至1740年（[乾隆五年](../Page/乾隆.md "wikilink")），鳳山縣下轄八里、七庄\[4\]：

  -
  - 庄：興隆庄、半屏山-{庄}-、觀音山-{庄}-、赤山-{庄}-、[大竹橋庄](../Page/大竹橋庄.md "wikilink")、[小竹橋庄](../Page/小竹橋庄.md "wikilink")、鳳山-{庄}-

## 相關條目

  - [鳳山縣舊城](../Page/鳳山縣舊城.md "wikilink")
  - [鳳山縣新城](../Page/鳳山縣新城.md "wikilink")
  - [舊城城隍廟](../Page/舊城城隍廟.md "wikilink")
  - [鳳山城隍廟](../Page/鳳山城隍廟.md "wikilink")
  - [鳳山舊城孔子廟崇聖祠](../Page/鳳山舊城孔子廟崇聖祠.md "wikilink")
  - [鳳山八景](../Page/鳳山八景.md "wikilink")

## 參考文獻

## 外部連結

  - [鳳山縣-行政院文建會](http://nrch.culture.tw/twpedia.aspx?id=1350&Keyword=%E9%B3%B3%E5%B1%B1%E7%B8%A3)

[龍巖冽泉](../Page/壽山.md "wikilink"){{.w}}[淡溪秋月](../Page/高屏溪.md "wikilink"){{.w}}[球嶼曉霞](../Page/小琉球.md "wikilink"){{.w}}[岡山樹色](../Page/大崗山.md "wikilink"){{.w}}[泮水荷香](../Page/蓮池潭.md "wikilink"){{.w}}[翠屏夕照](../Page/觀音山_\(高雄市\).md "wikilink"){{.w}}[丹渡晴帆](../Page/打狗港.md "wikilink")
| belowstyle = | below = }}<noinclude>

[鳳山縣_(台灣)](../Category/鳳山縣_\(台灣\).md "wikilink")
[Category:台灣古代縣份](../Category/台灣古代縣份.md "wikilink")
[Category:1684年台灣建立](../Category/1684年台灣建立.md "wikilink")
[Category:1895年台灣廢除](../Category/1895年台灣廢除.md "wikilink")

1.  蔣毓英，《臺灣府志》，卷一封隅，1685年
2.  高拱乾，《臺灣府志》，卷二規制志：坊里，1694年
3.  高拱乾、周元文，《臺灣府志》，卷二規制志：坊里，1712年
4.  劉良璧，《重修福建臺灣府志》，卷之五城池：坊里，1740年