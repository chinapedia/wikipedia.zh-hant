****是历史上第九十次航天飞机任务，也是[发现号航天飞机的第二十四次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[查尔斯·普里克特](../Page/查尔斯·普里克特.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[多米尼克·普德维尔·格里](../Page/多米尼克·普德维尔·格里.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[温迪·劳伦斯](../Page/温迪·劳伦斯.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[张福林](../Page/張福林.md "wikilink")**（，曾执行、、、、、以及任务），任务专家
  - **[珍妮特·卡万迪](../Page/珍妮特·卡万迪.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[瓦列里·留明](../Page/瓦列里·留明.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行[联盟25号](../Page/联盟25号.md "wikilink")、[联盟32号](../Page/联盟32号.md "wikilink")、[联盟35号以及](../Page/联盟35号.md "wikilink")任务），任务专家

### 从和平号空间站返回

  - **[安德鲁·托马斯](../Page/安德鲁·托马斯.md "wikilink")**（，曾执行、、[和平号](../Page/和平號太空站.md "wikilink")、、以及任务），任务专家

[Category:1998年美国](../Category/1998年美国.md "wikilink")
[Category:1998年科學](../Category/1998年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1998年6月](../Category/1998年6月.md "wikilink")