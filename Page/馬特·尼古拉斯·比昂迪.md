**馬休（馬特）·尼古拉斯·比昂迪**（**Matthew "Matt" Nicholas
Biondi**，）是一位[美國著名的](../Page/美國.md "wikilink")[游泳運動員](../Page/游泳.md "wikilink")。比昂迪曾在[漢城奧運上奪得](../Page/1988年夏季奧林匹克運動會.md "wikilink")7塊獎牌（其中包括5面金牌），追平了另一位美國游泳運動員[馬克·史畢茲創下的奧運紀錄](../Page/馬克·史畢茲.md "wikilink")，這個紀錄直到2004年[雅典奧運才被](../Page/雅典奧運.md "wikilink")[邁克爾·弗雷德·菲爾普斯打破](../Page/邁克爾·弗雷德·菲爾普斯.md "wikilink")，菲爾普斯於該屆奧運奪得8枚獎牌\[1\]。

## 生平

比昂迪出生在[加利福尼亞州的](../Page/加利福尼亞州.md "wikilink")[帕羅奧圖](../Page/帕羅奧圖_\(加利福尼亞州\).md "wikilink")，然後他在[蒙拉加](../Page/蒙拉加.md "wikilink")（Moraga）以游泳及[水球選手開始運動員生涯](../Page/水球.md "wikilink")。隨著比昂迪進入青少年時期，他在游泳上驚人的能力便開始顯露出來，特別是在短距離上。雖然他直到進入Campolindo高中後才練習游泳一整年。比昂迪在高中時期成為美國頂尖的學生運動員，並創下50碼自由泳20.40秒的美國紀錄。比昂迪後來獎學金[柏克萊加利福尼亞大學的獎學金來游泳與練習水球](../Page/柏克萊加利福尼亞大學.md "wikilink")，後來在1983年進入該校就讀。在進入大學頭一年中，比昂迪開始在柏克萊NCAA的水球隊中出賽。

比昂迪於1984年入選美國奧運游泳代表隊，並在[洛杉磯奧運中幫助美國隊獲得](../Page/洛杉磯奧運.md "wikilink")4x100公尺自由泳接力金牌，並且打破世界紀錄。回到柏克萊之後，比昂迪在1985年秋季參加該校的NCAA水球隊，並在1985年冬季奪得8項的NCAA游泳冠軍。他也是1985年、1986年與1987年的NCAA年度游泳運動員，並且在這段時期創下幾次新的世界紀錄。

比昂迪在1985年首次獨自創下世界紀錄，直到退休為止，他總共打破12次世界紀錄。他也是弟一位在100公尺自由泳中游進49秒之內的運動員，等到1988年時，這個項目最快的十次紀錄已經被比昂迪完全佔據了。比昂迪總共在50公尺、100公尺、200公尺自由泳及100公尺蝶泳這些項目獲得24次美國錦標賽的冠軍。比昂迪在1988年首爾奧運中奪得7面獎牌，並在50公尺、100公尺自由泳、4x100公尺混合泳接力、4x100公尺自由泳接力與4x100公尺自由泳接力獲得金牌。他也在1986年與1991年這兩次[世界游泳錦標賽中獲得](../Page/世界游泳錦標賽.md "wikilink")11面獎牌，其中包括6面金牌。比昂迪曾經入圍[詹姆斯·E·蘇利文獎](../Page/詹姆斯·E·蘇利文獎.md "wikilink")（James
E. Sullivan
Award）、也獲選為[合眾國際社年度運動員](../Page/合眾國際社.md "wikilink")、[美國奧運委員會年度運動員及](../Page/美國奧運委員會.md "wikilink")2次《[游泳世界雜誌](../Page/游泳世界雜誌.md "wikilink")》（*Swimming
World*）的年度男子運動員（1986年與1988年）。比昂迪也是[美國奧運名人堂](../Page/美國奧運名人堂.md "wikilink")（United
States Olympic Hall of
Fame）與[國際游泳名人堂](../Page/國際游泳名人堂.md "wikilink")（International
Swimming Hall of Fame）\[2\]的成員。

比昂迪於1998年以[工業化](../Page/工業化.md "wikilink")[社會的](../Page/社會.md "wikilink")[政治](../Page/政治.md "wikilink")[經濟](../Page/經濟.md "wikilink")（PEIS）學位畢業於柏克萊大學。目前他居住在[夏威夷的](../Page/夏威夷.md "wikilink")[威美亞](../Page/威美亞.md "wikilink")（Waimea），並且在學校中教授[數學](../Page/數學.md "wikilink")、美國[歷史](../Page/歷史.md "wikilink")、個人經驗與游泳。比昂迪與妻子克莉斯汀在1995年結婚，他們的第一個孩子於1998年出世。后与妻子离婚，和孩子移居洛杉矶，在塞拉峡谷中学教（Sierra
Canyon Upper School）数学兼游泳队教练。

## 紀錄

### 世界記錄

比昂迪總共打破12次世界紀錄，其中6次為獨立完成。

### 奧運紀錄

比昂迪總共獨立打破2次奧運紀錄。

| 賽事                                                     | 地點                             | 項目       | 名次 | 成績    |
| ------------------------------------------------------ | ------------------------------ | -------- | -- | ----- |
| [1988年夏季奧林匹克運動會](../Page/1988年夏季奧林匹克運動會.md "wikilink") | [首爾](../Page/首爾.md "wikilink") | 50公尺自由泳  | 金牌 | 22秒14 |
| [1988年夏季奧林匹克運動會](../Page/1988年夏季奧林匹克運動會.md "wikilink") | [首爾](../Page/首爾.md "wikilink") | 100公尺自由泳 | 金牌 | 48秒63 |

## 參考資料

  - [美國奧運委員會中關於比昂迪的運動員簡介](http://www.usolympicteam.com/26_37838.htm)
  - [國際游泳名人堂關於比昂迪的介紹](https://web.archive.org/web/20071221210124/http://www.ishof.org/honorees/97/97mbiondi.html)

[Category:美國游泳運動員](../Category/美國游泳運動員.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:美國奧林匹克運動會銀牌得主](../Category/美國奧林匹克運動會銀牌得主.md "wikilink")
[Category:美國奧林匹克運動會銅牌得主](../Category/美國奧林匹克運動會銅牌得主.md "wikilink")
[Category:1984年夏季奧林匹克運動會獎牌得主](../Category/1984年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會獎牌得主](../Category/1988年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:男子自由式游泳運動員](../Category/男子自由式游泳運動員.md "wikilink")
[Category:義大利裔美國人](../Category/義大利裔美國人.md "wikilink")
[Category:奧林匹克運動會游泳金牌得主](../Category/奧林匹克運動會游泳金牌得主.md "wikilink")
[Category:奧林匹克運動會游泳銀牌得主](../Category/奧林匹克運動會游泳銀牌得主.md "wikilink")
[Category:奥林匹克运动会游泳铜牌得主](../Category/奥林匹克运动会游泳铜牌得主.md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")
[Category:國際游泳名人堂成員](../Category/國際游泳名人堂成員.md "wikilink")
[Category:前游泳世界纪录保持者](../Category/前游泳世界纪录保持者.md "wikilink")
[Category:1984年夏季奧林匹克運動會游泳運動員](../Category/1984年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:1988年夏季奧林匹克運動會游泳運動員](../Category/1988年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:1992年夏季奧林匹克運動會游泳運動員](../Category/1992年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")

1.
2.