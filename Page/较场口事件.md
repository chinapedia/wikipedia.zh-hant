**较场口事件**，又名**较场口惨案**，指1946年2月10日在[重庆](../Page/重庆.md "wikilink")[较场口举行的](../Page/较场口.md "wikilink")“陪都各界庆祝[政治协商会议成功大会](../Page/政治协商会议.md "wikilink")”的暴力流血事件。\[1\]\[2\]

## 背景

1945年10月10日，[中国共产党和](../Page/中国共产党.md "wikilink")[中國國民黨经过会谈](../Page/中國國民黨.md "wikilink")，签定了《[双十协定](../Page/双十协定.md "wikilink")》，其中规定要召开各团体参加的[政治协商会议](../Page/政治协商会议.md "wikilink")。1946年1月10日至31日，政治协商会议正式召开。其间，重庆各界组成了“[政治协商会议陪都各界协进会](../Page/政治协商会议陪都各界协进会.md "wikilink")”，从1月12日到27日召开了八次各界民众大会，有三千多人参加。

1946年2月10日，[政治协商会议陪都各界协进会召开](../Page/政治协商会议陪都各界协进会.md "wikilink")「陪都各界慶祝政治協商會議成功大會」。與會的團體有：「[新華日報](../Page/新华日报.md "wikilink")」、「[全國郵務總工會](../Page/全國郵務總工會.md "wikilink")」、「[中國農業協進會](../Page/中國農業協進會.md "wikilink")」、「[中國經濟建設協會](../Page/中國經濟建設協會.md "wikilink")」、「[中國勞動協會](../Page/中國勞動協會.md "wikilink")」、「[陪都青年聯誼會](../Page/陪都青年聯誼會.md "wikilink")」等；主席團為：[周恩來](../Page/周恩來.md "wikilink")、[郭沫若](../Page/郭沫若.md "wikilink")、[沈鈞儒](../Page/沈鈞儒.md "wikilink")、[羅隆基](../Page/羅隆基.md "wikilink")、[馬寅初](../Page/馬寅初.md "wikilink")、[李公樸等](../Page/李公樸.md "wikilink")。\[3\]

## 经过

### 中共描述的经过

从1月16日起，[中国国民党重庆市党部派出特务跟踪威胁参加集会的代表](../Page/中国国民党重庆市党部.md "wikilink")。

2月2日，由协进会发起，定于2月10日上午9时，在重庆较场口广场举行庆祝政协成功大会，并推选[郭沫若](../Page/郭沫若.md "wikilink")、[马寅初](../Page/马寅初.md "wikilink")、[李公朴](../Page/李公朴.md "wikilink")、[施复亮](../Page/施复亮.md "wikilink")、[章乃器等二十余人组成大会主席团](../Page/章乃器.md "wikilink")，[李德全](../Page/李德全.md "wikilink")（国民党将领[冯玉祥夫人](../Page/冯玉祥.md "wikilink")）为总主席，李公朴作总指挥。国民党则密谋破坏。

2月10日上午，[中国国民党另外组织了一个所谓的](../Page/中国国民党.md "wikilink")“主席团”，其成员有[吴人初](../Page/吴人初.md "wikilink")（[重庆市教育会理事长](../Page/重庆市教育会.md "wikilink")）、[谭泽森](../Page/谭泽森.md "wikilink")（[重庆市工会理事长](../Page/重庆市工会.md "wikilink")）、[刘野樵](../Page/刘野樵.md "wikilink")（[重庆市农会常务理事](../Page/重庆市农会.md "wikilink")）、[周德侯](../Page/周德侯.md "wikilink")（[重庆市商会理事](../Page/重庆市商会.md "wikilink")）、[庞仪山](../Page/庞仪山.md "wikilink")（[中国国民党重庆市党部宣传科长](../Page/中国国民党重庆市党部.md "wikilink")）等，早早抢占了主席台，会场两侧布满了特务打手，并有雇佣而来冒充各会会员的“八百壮汉”提前入场。[周德侯叫嚷](../Page/周德侯.md "wikilink")：“我们选占中国人口百分之八十的农会代表[刘野樵担任总主席](../Page/刘野樵.md "wikilink")！”并悍然宣布开会。李公朴、施复亮上前阻拦遭到殴打。[郭沫若](../Page/郭沫若.md "wikilink")、[陶行知](../Page/陶行知.md "wikilink")、[章乃器和新闻记者及劳协会员等六十余人也被打伤](../Page/章乃器.md "wikilink")。当[中国共产党代表](../Page/中国共产党.md "wikilink")[周恩来](../Page/周恩来.md "wikilink")、[王若飞等和](../Page/王若飞.md "wikilink")[冯玉祥赶到会场时](../Page/冯玉祥.md "wikilink")，特务们才四散离去。

### 国民党描述的经过

2月10日上午，重庆各界在较场口庆祝政协会议圆满成功。开会前，原筹备委员会推[李德全为大会总主席](../Page/李德全.md "wikilink")。開會時，部分人員不遵開會程序，逕擁[李公樸登台任大會主席](../Page/李公樸.md "wikilink")，群情大嘩，時醫師公會全國聯合會秘書長[覃勤在台下高呼](../Page/覃勤.md "wikilink")：“請按集會程序進行”，約集各團體代表數十名，推舉重慶市農會代表[刘野樵为大會主席](../Page/刘野樵.md "wikilink")。刘氏上台报告，筹委会成员[李公朴不满](../Page/李公朴.md "wikilink")，上前推搡，夺去扩音器。\[4\]随后台上发生斗殴，台下大乱，筹委会之[李公朴](../Page/李公朴.md "wikilink")、[郭沫若](../Page/郭沫若.md "wikilink")、[施复亮等人被群眾毆傷](../Page/施复亮.md "wikilink")\[5\]。

## 事件处理和影响

第二天，重庆市农会常务理事[刘野樵向](../Page/刘野樵.md "wikilink")[重庆地方法院控告](../Page/重庆地方法院.md "wikilink")[李公朴](../Page/李公朴.md "wikilink")、[章乃器](../Page/章乃器.md "wikilink")、[朱学范](../Page/朱学范.md "wikilink")、[陶行知](../Page/陶行知.md "wikilink")、[施复亮等五人公然扰乱集会并伤害他人身体](../Page/施复亮.md "wikilink")。经过法庭辩论，因为李公朴等人明显受伤，案件只好无果而终。

当晚，[中國民主同盟召集会议](../Page/中國民主同盟.md "wikilink")，推[周恩来](../Page/周恩来.md "wikilink")、[張君勱等人赴](../Page/張君勱.md "wikilink")[蒋中正处陈情](../Page/蒋中正.md "wikilink")，然蒋已赴[上海](../Page/上海.md "wikilink")。故前往国民党秘书长[吴铁城处](../Page/吴铁城.md "wikilink")，要求彻底追查事件真相。这一事件是国共两党关系恶化表面化之开始。

## 后续事件

  - [李公樸于](../Page/李公樸.md "wikilink")1946年7月11日在[昆明大兴街学院坡被国民党特务暗杀](../Page/昆明.md "wikilink")。

## 纪念

  - [较场口事件纪念碑](../Page/较场口事件纪念碑.md "wikilink")-位于[重庆](../Page/重庆.md "wikilink")[较场口](../Page/较场口.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [会场照片](http://images.qianlong.com/mmsource/images/2004/09/02/jiaochankouxuean.jpg)

{{-}}

[Category:1946年重庆](../Category/1946年重庆.md "wikilink")
[Category:中華民國大陸時期的騷亂或暴動](../Category/中華民國大陸時期的騷亂或暴動.md "wikilink")
[Category:1946年国共内战](../Category/1946年国共内战.md "wikilink")
[Category:重庆民国时期政治事件](../Category/重庆民国时期政治事件.md "wikilink")
[Category:1946年2月](../Category/1946年2月.md "wikilink")

1.  张宪文. 中华民国史·第四卷(1945\~1949年). 南京大学出版社. 2005年. 第54页
2.  黄立人. 较场口事件述略\[J\]. 历史档案,1988,(4).
3.  俞諧．中共史略．正中書局．台北．1976年.第144頁
4.  「陪都较场口事件报告书」，民国三十五年二月十九日重庆市党部主任委员方治呈，《中华民国重要史料初编》第七编，战后中国，页270-273
5.  「覃勤先生事略」，《湖南文獻》第9卷第4期，1981-10-15