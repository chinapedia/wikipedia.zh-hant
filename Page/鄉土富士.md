[Yotei-zan-from-hirafu.jpg](https://zh.wikipedia.org/wiki/File:Yotei-zan-from-hirafu.jpg "fig:Yotei-zan-from-hirafu.jpg")（蝦夷富士）\]\]
[NikkoNantaisan.jpg](https://zh.wikipedia.org/wiki/File:NikkoNantaisan.jpg "fig:NikkoNantaisan.jpg")（下野富士、日光富士）\]\]
[Iinoyama(sanukihuji).jpg](https://zh.wikipedia.org/wiki/File:Iinoyama\(sanukihuji\).jpg "fig:Iinoyama(sanukihuji).jpg")（讚岐富士）\]\]

**鄉土富士**（きょうどふじ），遍布[日本全國各地帶有](../Page/日本.md "wikilink")「富士」名稱的山。大部分是因山形狀似[富士山或與富士山有某些關聯而得名](../Page/富士山.md "wikilink")。也可能是代表當地的山或單純為了宣傳而稱為「富士」。可能不是[火山或與富士山同類型的](../Page/火山.md "wikilink")[成層火山](../Page/複式火山.md "wikilink")。此外，在[關東地方等地也有稱為](../Page/關東地方.md "wikilink")「[富士塚](../Page/富士塚.md "wikilink")」的人工小山丘。

## 鄉土富士一覽

### 北海道

  - 阿寒富士（あかんふじ）（[阿寒富士](../Page/阿寒富士.md "wikilink")） -
    [釧路市](../Page/釧路市.md "wikilink")
  - 蝦夷富士（えぞふじ）（[羊蹄山](../Page/羊蹄山.md "wikilink")） -
    [虻田郡](../Page/虻田郡.md "wikilink")[俱知安町](../Page/俱知安町.md "wikilink")・[京極町](../Page/京極町.md "wikilink")・[喜茂別町](../Page/喜茂別町.md "wikilink")・[真狩村](../Page/真狩村.md "wikilink")・[二世古町](../Page/二世古町.md "wikilink")
  - 渡島富士（おしまふじ）（[駒岳](../Page/北海道駒岳.md "wikilink")） -
    [茅部郡](../Page/茅部郡.md "wikilink")[鹿部町](../Page/鹿部町.md "wikilink")・[森町](../Page/森町_\(北海道\).md "wikilink")、[龜田郡](../Page/龜田郡.md "wikilink")[七飯町](../Page/七飯町.md "wikilink")
  - 音威富士（おといふじ）（[音威富士](../Page/:ja:音威富士.md "wikilink")） -
    [中川郡](../Page/中川郡_\(上川支廳\).md "wikilink")[音威子府村](../Page/音威子府村.md "wikilink")
  - 鄂霍次克富士（オホーツクふじ）・斜里富士（しゃりふじ）（[斜里岳](../Page/斜里岳.md "wikilink")） -
    [斜里郡](../Page/斜里郡.md "wikilink")[清里町](../Page/清里町.md "wikilink")
  - 溫泉富士（おんせんふじ）（[溫泉富士](../Page/:ja:温泉富士.md "wikilink")） -
    [標津郡](../Page/標津郡.md "wikilink")[中標津町](../Page/中標津町.md "wikilink")
  - 釜谷富士（かまやふじ）（[釜谷富士](../Page/釜谷富士.md "wikilink")） -
    [函館市](../Page/函館市.md "wikilink")
  - 北見富士（きたみふじ）（[北見富士 (紋別市)](../Page/北見富士_\(紋別市\).md "wikilink")） -
    [紋別市](../Page/紋別市.md "wikilink")、[紋別郡](../Page/紋別郡.md "wikilink")[瀧上町](../Page/瀧上町.md "wikilink")・[遠輕町](../Page/遠輕町.md "wikilink")
  - 北見富士（きたみふじ）（[北見富士 (北見市)](../Page/北見富士_\(北見市\).md "wikilink")） -
    [北見市](../Page/北見市.md "wikilink")
  - 黄金富士（こがねふじ）・濱益富士（はまますふじ）（[黄金山](../Page/黄金山_\(石狩市\).md "wikilink")） -
    [石狩市](../Page/石狩市.md "wikilink")
  - 知床富士（しれとこふじ）（[羅臼岳](../Page/羅臼岳.md "wikilink")） -
    [目梨郡](../Page/目梨郡.md "wikilink")[羅臼町](../Page/羅臼町.md "wikilink")、斜里郡[斜里町](../Page/斜里町.md "wikilink")
  - 美瑛富士（びえいふじ）（[美瑛富士](../Page/:ja:美瑛富士.md "wikilink")） -
    [上川郡](../Page/上川郡_\(石狩國\).md "wikilink")[美瑛町](../Page/美瑛町.md "wikilink")
  - 富士形山（ふじかたやま）（[富士形山](../Page/富士形山.md "wikilink")） -
    [樺戶郡](../Page/樺戶郡.md "wikilink")[新十津川町](../Page/新十津川町.md "wikilink")
  - 母戀富士（ぼこいふじ）（[母戀富士](../Page/:ja:母恋富士.md "wikilink")） -
    [室蘭市](../Page/室蘭市.md "wikilink")
  - 增毛富士（ましけふじ）（[暑寒別岳](../Page/:ja:暑寒別岳.md "wikilink")） -
    [增毛郡](../Page/增毛郡.md "wikilink")[增毛町](../Page/增毛町.md "wikilink")、[雨龍郡](../Page/雨龍郡.md "wikilink")[雨龍町](../Page/雨龍町.md "wikilink")・[北龍町](../Page/北龍町.md "wikilink")、[樺戶郡](../Page/樺戶郡.md "wikilink")[新十津川町](../Page/新十津川町.md "wikilink")
  - 利尻富士（りしりふじ）（[利尻山](../Page/利尻山.md "wikilink")） -
    [利尻郡](../Page/利尻郡.md "wikilink")[利尻富士町](../Page/利尻富士町.md "wikilink")・[利尻町](../Page/利尻町.md "wikilink")

<File:Mount> Yotei from Mount
Shiribetsu.jpg|[羊蹄山](../Page/羊蹄山.md "wikilink")（蝦夷富士）
<File:Rausu-dake> 02.JPG|[羅臼岳](../Page/羅臼岳.md "wikilink")（知床富士）
<File:利尻岳> 洋上から.JPG|[利尻山](../Page/利尻山.md "wikilink")（利尻富士）
<File:Onsen> fuji.jpg|[溫泉富士](../Page/:ja:温泉富士.md "wikilink") <File:Mt>
meakann akannfuji\&lake onnneto.jpg|[阿寒富士](../Page/阿寒富士.md "wikilink")
<File:Ōnuma-Kōen01.jpg>|[北海道駒岳](../Page/北海道駒岳.md "wikilink")（渡島富士）

### 東北地方

#### 青森縣

  - 津輕富士（つがるふじ）（[岩木山](../Page/岩木山.md "wikilink")） -
    [弘前市](../Page/弘前市.md "wikilink")、[西津輕郡](../Page/西津輕郡.md "wikilink")[鰺澤町](../Page/鰺澤町.md "wikilink")
  - 南部小富士（なんぶこふじ）（[名久井岳](../Page/:ja:名久井岳.md "wikilink")）-
    [三戶郡](../Page/三戶郡.md "wikilink")[南部町](../Page/南部町_\(青森縣\).md "wikilink")、[三戶町](../Page/三戶町.md "wikilink")

#### 岩手縣

  - 岩手富士（いわてふじ）・南部片富士（なんぶかたふじ）（[岩手山](../Page/岩手山.md "wikilink")） -
    [八幡平市](../Page/八幡平市.md "wikilink")、[瀧澤市](../Page/瀧澤市.md "wikilink")、[岩手郡](../Page/岩手郡.md "wikilink")[雫石町](../Page/雫石町.md "wikilink")
  - 遠野小富士（とおのこふじ）（[六角牛山](../Page/:ja:六角牛山.md "wikilink")） -
    [遠野市](../Page/遠野市.md "wikilink")
  - 室根小富士（むろねこふじ）（[室根山](../Page/:ja:室根山.md "wikilink")） -
    [一關市](../Page/一關市.md "wikilink")
  - 野田富士（のだふじ）（[和左羅比山](../Page/和左羅比山.md "wikilink")） -
    [九戶郡](../Page/九戶郡.md "wikilink")[野田村](../Page/野田村.md "wikilink")
  - 綾里富士（りょうりふじ）（[大森山](../Page/大森山.md "wikilink")） -
    [大船渡市](../Page/大船渡市.md "wikilink")
  - 富士之根山（ふじのねやま）（[富士之根山](../Page/富士之根山.md "wikilink")） -
    [奥州市](../Page/奥州市.md "wikilink")

#### 宮城縣

  - 加美富士（かみふじ）（[藥萊山](../Page/藥萊山.md "wikilink")） -
    [加美郡](../Page/加美郡.md "wikilink")[加美町](../Page/加美町.md "wikilink")
  - 小富士山（おふんつぁん）（[小富士山](../Page/小富士山.md "wikilink")） -
    [石卷市](../Page/石卷市.md "wikilink")

#### 秋田縣

  - 出羽富士（でわふじ）（[鳥海山](../Page/鳥海山.md "wikilink")） -
    [仁賀保市](../Page/仁賀保市.md "wikilink")、[由利本莊市](../Page/由利本莊市.md "wikilink")
  - [明田富士](../Page/:ja:富士山_\(秋田県\).md "wikilink")（みょうでんふじ、35m。-{[日本山岳會](../Page/日本山岳會.md "wikilink")}-認定之「日本第一低富士山」）
    - [秋田市](../Page/秋田市.md "wikilink")
  - [大潟富士](../Page/大潟富士.md "wikilink")（おおがたふじ） -
    [南秋田郡](../Page/南秋田郡.md "wikilink")[大潟村](../Page/大潟村.md "wikilink")

#### 山形縣

  - 鳥海富士（ちょうかい）（[鳥海山](../Page/鳥海山.md "wikilink")） -
    [酒田市](../Page/酒田市.md "wikilink")、[飽海郡](../Page/飽海郡.md "wikilink")[遊佐町](../Page/遊佐町.md "wikilink")

#### 福島縣

  - 吾妻富士（あづまふじ）（[吾妻小富士](../Page/吾妻小富士.md "wikilink")） -
    [福島市](../Page/福島市.md "wikilink")
  - 會津富士（あいづふじ）（[磐梯山](../Page/磐梯山.md "wikilink")） -
    [耶麻郡](../Page/耶麻郡.md "wikilink")[豬苗代町](../Page/豬苗代町.md "wikilink")、[磐梯町](../Page/磐梯町.md "wikilink")、[北鹽原村](../Page/北鹽原村.md "wikilink")

<File:Iwakisan> 02.jpg|[岩木山](../Page/岩木山.md "wikilink")（津輕富士） <File:Mt>.
Iwate and Morioka.jpg|[岩手山](../Page/岩手山.md "wikilink")（南部片富士） <File:Mt>
Chokai from Kawauti.jpg|[鳥海山](../Page/鳥海山.md "wikilink")（出羽富士）
<File:Mt.Bandai> 01.jpg|[磐梯山](../Page/磐梯山.md "wikilink")（會津富士）
<File:Mount> Azuma-kofuji-1, May
2007.JPG|[吾妻小富士](../Page/吾妻小富士.md "wikilink")

### 關東地方

#### 茨城縣

  - 真壁富士（まかべふじ）（[權現山](../Page/權現山_\(櫻川市\).md "wikilink")） -
    [櫻川市](../Page/櫻川市.md "wikilink")
  - 柿岡富士（かきおかふじ）（[富士山](../Page/富士山_\(石岡市\).md "wikilink")） -
    [石岡市](../Page/石岡市.md "wikilink")

#### 栃木縣

  - 富士山（ふじやま） - [那須鹽原市](../Page/那須鹽原市.md "wikilink")
  - 下野富士、日光富士（[男體山](../Page/男體山.md "wikilink")） -
    [日光市](../Page/日光市.md "wikilink")
  - 芳賀富士（大平山） -
    [芳賀郡](../Page/芳賀郡.md "wikilink")[益子町](../Page/益子町.md "wikilink")・[茂木町](../Page/茂木町.md "wikilink")
  - 中村富士（京路戶山（きょうろどさん）） - [佐野市](../Page/佐野市.md "wikilink")
  - 富士山（ふじやま） - [鹿沼市](../Page/鹿沼市.md "wikilink")
  - 足利富士（上淺間山・男淺間山） - [足利市](../Page/足利市.md "wikilink")

#### 群馬縣

  - 榛名富士（はるなふじ）（[榛名山](../Page/榛名山.md "wikilink")） -
    [高崎市](../Page/高崎市.md "wikilink")
  - [富士山](../Page/:ja:富士山_\(群馬県\).md "wikilink")（ふじやま） -
    [澀川市](../Page/澀川市.md "wikilink")

#### 埼玉縣

  - 富士山（ふじやま） - [日高市](../Page/日高市.md "wikilink")
  - 荒幡富士（あらはたふじ） - [所澤市](../Page/所澤市.md "wikilink")
  - 弟富士（おとふじ） - [秩父市](../Page/秩父市.md "wikilink")

#### 千葉縣

  - 上總富士、大坂富士（富士山） - [君津市](../Page/君津市.md "wikilink")

#### 東京都

  - 八丈富士（西山）-
    [八丈町](../Page/八丈町.md "wikilink")（[八丈島](../Page/八丈島.md "wikilink")）
  - オフジサマ（丸山）-[青島村](../Page/青島村_\(日本\).md "wikilink")（[青島](../Page/:ja:青ヶ島.md "wikilink")）
  - 小富士 - [小笠原村](../Page/小笠原村.md "wikilink")
    ([母島](../Page/母島.md "wikilink"))

#### 神奈川縣

  - [川和富士](../Page/:ja:川和富士公園.md "wikilink") -
    [横濱市](../Page/横濱市.md "wikilink")[都築區](../Page/都築區.md "wikilink")
  - 山田富士 - 横濱市都築區
  - 羽澤富士 - 横濱市[神奈川區](../Page/神奈川區.md "wikilink")
  - 菅田富士 - 横濱市神奈川區
  - 三浦富士（183m） - [横須賀市](../Page/横須賀市.md "wikilink")
  - 半原富士（はんばらふじ）（[佛果山](../Page/:ja:仏果山.md "wikilink")、747m） -
    [愛甲郡](../Page/愛甲郡.md "wikilink")[清川村](../Page/清川村.md "wikilink")、[愛川町](../Page/愛川町.md "wikilink")
  - 荻野富士（おぎのふじ）（[經岳](../Page/:ja:経ヶ岳_\(神奈川県\).md "wikilink")） -
    愛甲郡清川村、同郡愛川町、[厚木市](../Page/厚木市.md "wikilink")

<File:Mount> nantai and lake chuzenji.jpg
|[男體山](../Page/男體山.md "wikilink")（下野富士、日光富士）
<File:Haruna00.JPG>|[榛名山](../Page/榛名山.md "wikilink")（榛名富士）
<File:Hachijo-jima.JPG>|[八丈西山](../Page/八丈島.md "wikilink")（八丈富士）
<File:Aogasima>
maruyama.jpg|[青島](../Page/:ja:青ヶ島.md "wikilink")・丸山（オフジサマ）
<File:Mt-Kohuji.jpg>|[母島](../Page/母島.md "wikilink")・小富士
<File:Kawawahuji.JPG>|[川和富士](../Page/:ja:川和富士公園.md "wikilink")
<File:Mt.Bukka.JPG>|[佛果山](../Page/:ja:仏果山.md "wikilink")（半原富士）

### 中部地方

#### 山梨縣

  - 黑富士（くろふじ）（[黑富士](../Page/:ja:黒富士.md "wikilink")、1635m）

#### 長野縣

  - 諏訪富士（すわふじ）（[蓼科山](../Page/蓼科山.md "wikilink")、2530m）
  - 信濃富士（しなのふじ）（[黑姬山](../Page/黑姬山.md "wikilink")、2053m）
  - 信濃富士（しなのふじ）（[有明山](../Page/:ja:有明山_\(長野県\).md "wikilink")、2268m） -
    安曇富士（あづみふじ）、有明富士（ありあけふじ）
  - 高井富士（たかいふじ）（[高社山](../Page/:ja:高社山.md "wikilink")、1351m）
  - 伊那富士（いなふじ）（[戶倉山](../Page/戶倉山.md "wikilink")、1681m）

#### 新潟縣

  - 越後富士（えちごふじ）（[妙高山](../Page/妙高山.md "wikilink")、2454m）-
    [妙高市](../Page/妙高市.md "wikilink")
  - 兩尾富士（もろおふじ）（宇賀神山、107m） - [佐渡市](../Page/佐渡市.md "wikilink")
  - 上田富士（うえだふじ）（[飯士山](../Page/:ja:飯士山.md "wikilink")、1112m） -
    [南魚沼市](../Page/南魚沼市.md "wikilink")、[南魚沼郡](../Page/南魚沼郡.md "wikilink")[湯澤町](../Page/湯澤町.md "wikilink")

#### 富山縣

  - 富士之折立（ふじのおりたて）（[富士之折立](../Page/立山_\(日本\).md "wikilink")、2999m）

#### 石川縣

  - 能登富士（のとふじ）（[高爪山](../Page/:ja:高爪山.md "wikilink")、341m） -
    [羽咋郡](../Page/羽咋郡.md "wikilink")[志賀町](../Page/志賀町.md "wikilink")、[輪島市](../Page/輪島市.md "wikilink")
  - 加賀富士（かがふじ）（[大門山](../Page/:ja:大門山.md "wikilink")、1572m） -
    [金澤市](../Page/金澤市.md "wikilink")、[富山縣](../Page/富山縣.md "wikilink")[南砺市](../Page/南砺市.md "wikilink")
  - 加賀富士（かがふじ）（[白山](../Page/白山_\(日本\).md "wikilink")、2702m）
  - 富士寫岳（ふじしゃがだけ）(富士寫岳、942m） - [加賀市](../Page/加賀市.md "wikilink")

#### 福井縣

  - 大野富士（おおのふじ）（[荒島岳](../Page/荒島岳.md "wikilink")、1524m） -
    [大野市](../Page/大野市.md "wikilink")
  - 越前富士（えちぜんふじ）（[日野山](../Page/:ja:日野山.md "wikilink")、795m） -
    [越前市](../Page/越前市.md "wikilink")
  - 若狭富士（わかさふじ）（青葉山、693m） -
    [大飯郡](../Page/大飯郡.md "wikilink")[高濱町](../Page/高濱町.md "wikilink")、[京都府](../Page/京都府.md "wikilink")[舞鶴市](../Page/舞鶴市.md "wikilink")
  - 敦賀富士（つるがふじ）（[野坂岳](../Page/:ja:野坂岳.md "wikilink")、914m） -
    [敦賀市](../Page/敦賀市.md "wikilink")

#### 岐阜縣

  - 日和田富士（ひわだふじ）　（[繼子岳](../Page/御嶽山.md "wikilink")、2859m）
  - 蒲田富士（がまだふじ）　（[蒲田富士](../Page/穗高岳.md "wikilink")、2742m）
  - 飛驒富士（ひだふじ）（船山、1479m）
  - 德山富士（とくやまふじ）（925m）
  - 下呂富士（げろふじ）（[中根山](../Page/:ja:中根山.md "wikilink")、782m）

#### 靜岡縣

  - 下田富士（しもだふじ）（本鄉富士、187m）

#### 愛知縣

  - 三河富士（みかわふじ）（[本宮山](../Page/:ja:本宮山_\(愛知県\).md "wikilink")、789m）
  - [尾張富士](../Page/尾張富士.md "wikilink")
  - 三谷富士（みやふじ）（砥神山、252m）

<File:Tateshinayama-fuyu.JPG>|[蓼科山](../Page/蓼科山.md "wikilink")（諏訪富士）
<File:Mount> Kurohime from Mount Hiuchi
1996-6-29.jpg|[黑姬山](../Page/黑姬山.md "wikilink")（信濃富士）
<File:Ontakesan> from Hachimoriyama
2008-4-25.JPG|[繼子岳](../Page/御嶽山.md "wikilink")（日和田富士）
<File:Mt> Myoko from
Northeast.JPG|[妙高山](../Page/妙高山.md "wikilink")（越後富士）
<File:Mt.Kosha.jpg>|[高社山](../Page/:ja:高社山.md "wikilink")（高井富士）
<File:Mount> Haku from Aburazaka
1997-7-19.jpg|[白山](../Page/白山_\(日本\).md "wikilink")（加賀富士）
<File:Daimonsan.jpg>|[大門山](../Page/:ja:大門山.md "wikilink")（加賀富士）
<File:Mountains> of Japan
HINOSAN.jpg|[日野山](../Page/:ja:日野山.md "wikilink")（越前富士）
<File:Mt> Aobasan
1.JPG|[青葉山](../Page/:ja:青葉山_\(京都府・福井県\).md "wikilink")（若狭富士）
<File:Arashimadake> from Ohno
2002-6-10.jpg|[荒島岳](../Page/荒島岳.md "wikilink")（大野富士）
<File:Gero-onsen01s3200.jpg>|[中根山](../Page/:ja:中根山.md "wikilink")（下呂富士）
<File:Mount> Owarifujij and Meiji Mura from Iruka Pond
2008-05-15.JPG|[尾張富士](../Page/尾張富士.md "wikilink")

### 近畿地方

#### 三重縣

  - 伊賀富士（いがふじ）（尼岳、958m） -
    [伊賀市](../Page/伊賀市.md "wikilink")、[津市](../Page/津市.md "wikilink")
  - 伊勢富士（いせふじ）（堀坂山、757m） - [松阪市](../Page/松阪市.md "wikilink")
  - 五所富士（ごかしょふじ）（五所淺間山、172m） -
    [度會郡](../Page/度會郡.md "wikilink")[南伊勢町](../Page/南伊勢町.md "wikilink")

#### 滋賀縣

  - 近江富士（おうみふじ）（[三上山](../Page/三上山.md "wikilink")、432m） -
    [野洲市](../Page/野洲市.md "wikilink")

#### 京都府

  - 丹後富士（たんごふじ）
      - 高龍寺ヶ岳（697m） -
        [京丹後市](../Page/京丹後市.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")[豐岡市](../Page/豐岡市.md "wikilink")
      - 建部山（316m） - [舞鶴市](../Page/舞鶴市.md "wikilink")
      - 由良岳（640m） - [宮津市](../Page/宮津市.md "wikilink")、舞鶴市
  - 丹波富士（たんばふじ）
      - 牛松山（636m） - [龜岡市](../Page/龜岡市.md "wikilink")
      - 烏帽子山（513m） -
        [福知山市](../Page/福知山市.md "wikilink")、兵庫縣[丹波市](../Page/丹波市.md "wikilink")
      - 砥石山（536m） -
        [船井郡](../Page/船井郡.md "wikilink")[京丹波町](../Page/京丹波町.md "wikilink")
      - 彌仙山（664m） - [綾部市](../Page/綾部市.md "wikilink")
  - 都富士（みやこふじ）（[比叡山](../Page/比叡山.md "wikilink")、848m） -
    [京都市](../Page/京都市.md "wikilink")、[滋賀縣](../Page/滋賀縣.md "wikilink")[大津市](../Page/大津市.md "wikilink")

#### 大阪府

  - 天見富士（あまみふじ）（[旗尾岳](../Page/:ja:旗尾岳.md "wikilink")、548m） -
    [河内長野市](../Page/河内長野市.md "wikilink")
  - 泉州小富士（せんしゅうこふじ）（[小富士山](../Page/:ja:小富士山.md "wikilink")、260m） -
    [泉佐野市](../Page/泉佐野市.md "wikilink")

#### 兵庫縣

  - [有馬富士](../Page/:ja:有馬富士.md "wikilink")（ありまふじ）（374m） -
    [三田市](../Page/三田市.md "wikilink")
  - 淡路富士（あわじふじ）（[先山](../Page/:ja:先山.md "wikilink")、448m） -
    [洲本市](../Page/洲本市.md "wikilink")
  - 但馬富士（たじまふじ）（三開山、202m） - [豐岡市](../Page/豐岡市.md "wikilink")
  - 丹波富士（たんばふじ）
      - 大箕山（626m） - [丹波市](../Page/丹波市.md "wikilink")
      - 白髪岳（722m） - [篠山市](../Page/篠山市.md "wikilink")
      - 高城山（459m） - 篠山市
  - 播磨富士（はりまふじ）
      - [笠形山](../Page/:ja:笠形山.md "wikilink")（939.4m） -
        [神崎郡](../Page/神崎郡.md "wikilink")[神河町](../Page/神河町.md "wikilink")、[市川町](../Page/市川町.md "wikilink")、[多可郡](../Page/多可郡.md "wikilink")[多可町](../Page/多可町.md "wikilink")
      - [高御位山](../Page/:ja:高御位山.md "wikilink")（304m） -
        [加古川市](../Page/加古川市.md "wikilink")、[高砂市](../Page/高砂市.md "wikilink")
      - [明神山](../Page/:ja:明神山_\(兵庫県\).md "wikilink")（667m） -
        [姫路市](../Page/姫路市.md "wikilink")
      - 雌岡山（250m）・雄岡山（241m） - [神戶市](../Page/神戶市.md "wikilink")
  - [小富士山](../Page/小富士山.md "wikilink")（173m） - 姫路市
  - [長富士](../Page/長富士.md "wikilink")（227・6m） -
    [加西市](../Page/加西市.md "wikilink")
  - 八千種富士
      - [飯盛山](../Page/:ja:飯盛山_\(福崎町\).md "wikilink")（197・9m） -
        神崎郡[福崎町](../Page/福崎町.md "wikilink")

#### 奈良縣

  - 朝倉富士（あさくらふじ）（[外鎌山](../Page/外鎌山.md "wikilink")、293m ） -
    [櫻井市](../Page/櫻井市.md "wikilink")
  - 伊賀富士（いがふじ）（[俱留尊山](../Page/:ja:俱留尊山.md "wikilink")、1038m） -
    [宇陀郡](../Page/宇陀郡.md "wikilink")[曾爾村](../Page/曾爾村.md "wikilink")、[三重縣](../Page/三重縣.md "wikilink")[津市](../Page/津市.md "wikilink")
  - 都介野富士（つげのふじ）（[都介野岳](../Page/都介野岳.md "wikilink")、631.2m） -
    [奈良市](../Page/奈良市.md "wikilink")
  - 大和富士（やまとふじ）（[額井岳](../Page/:ja:額井岳.md "wikilink")、813m） -
    [宇陀市](../Page/宇陀市.md "wikilink")、奈良市

#### 和歌山縣

  - 紀州富士（きしゅうふじ）（[龍門山](../Page/龍門山_\(和歌山縣\).md "wikilink")、756m） -
    [紀之川市](../Page/紀之川市.md "wikilink")
  - 日高富士（ひだかふじ）（[真妻山](../Page/真妻山.md "wikilink")、523m） -
    [日高郡](../Page/日高郡_\(和歌山縣\).md "wikilink")[日高川町](../Page/日高川町.md "wikilink")、[印南町](../Page/印南町.md "wikilink")

<File:Mikamiyama.jpg>|[三上山](../Page/三上山.md "wikilink")（近江富士）
<File:Sakura> MtHiei.jpg|[比叡山](../Page/比叡山.md "wikilink")（都富士）
<File:Hataodake1.jpg>|[旗尾岳](../Page/:ja:旗尾岳.md "wikilink")（天見富士）
<File:Bodai> temple03 hyogo.JPG|[有馬富士](../Page/:ja:有馬富士.md "wikilink")
<File:SUMOTO-IC.JPG>|[先山](../Page/:ja:先山.md "wikilink")（淡路富士）
<File:Takamikura1.jpg>|[高御位山](../Page/:ja:高御位山.md "wikilink")（播磨富士）
<File:Ryūmon-Zan.jpg>|[龍門山](../Page/龍門山_\(和歌山縣\).md "wikilink")（紀州富士）

### 中國地方

#### 鳥取縣

  - 伯耆富士（ほうきふじ）（[大山](../Page/大山_\(鳥取縣\).md "wikilink")）

#### 島根縣

  - 出雲富士（いずもふじ）（[伯耆大山](../Page/伯耆大山.md "wikilink")）
  - 石見富士（いわみふじ）（[三瓶山](../Page/三瓶山.md "wikilink")）
  - 淺利富士（あさりふじ）（[室神山](../Page/室神山.md "wikilink")）
  - 隱岐富士（おきふじ）（[大滿寺山](../Page/:ja:大満寺山.md "wikilink")）

#### 岡山縣

  - 備前富士（びぜんふじ）（芥子山（けしごやま））- [岡山市](../Page/岡山市.md "wikilink")
  - 兒島富士（こじまふじ）（常山（つねやま）） - 岡山市
  - 美作富士（みまさかふじ）（日名倉山（ひなくらやま）） - [美作市](../Page/美作市.md "wikilink")

#### 廣島縣

  - 安藝之小富士（あきのこふじ）（[似島](../Page/:ja:似島.md "wikilink")） -
    [廣島市](../Page/廣島市.md "wikilink")[南區](../Page/南區_\(廣島市\).md "wikilink")
  - 備後富士（びんごふじ）（[蛇圓山](../Page/蛇圓山.md "wikilink")） -
    [福山市](../Page/福山市.md "wikilink")
  - 峠富士（とうげふじ）（[峠空山](../Page/峠空山.md "wikilink")） -
    [廿日市市](../Page/廿日市市.md "wikilink")

#### 山口縣

  - 周防富士
      - 四熊岳（504 m） - [周南市](../Page/周南市.md "wikilink")
      - 矢筈岳（461 m） - [防府市](../Page/防府市.md "wikilink")
  - 周防之富士
      - 氷室岳（563 m） -
        [柳井市](../Page/柳井市.md "wikilink")、[岩國市](../Page/岩國市.md "wikilink")
  - 西之富士
      - 石城山（352 m） -
        [光市](../Page/光市.md "wikilink")、[熊毛郡](../Page/熊毛郡_\(山口縣\).md "wikilink")[田布施町](../Page/田布施町.md "wikilink")
  - 長門富士（ながとふじ）
      - 青山（288 m） - [下關市](../Page/下關市.md "wikilink")
      - 十種峰（989 m） - [山口市](../Page/山口市.md "wikilink")

など

<File:Mt> Daisen Full View.jpg|[伯耆大山](../Page/伯耆大山.md "wikilink")（伯耆富士）
<File:Mt>. Sanbe and Ukinuno
pond.JPG|[三瓶山](../Page/三瓶山.md "wikilink")（石見富士）
<File:Mt>.
Tsuneyama.JPG|[常山](../Page/:ja:常山_\(岡山県\).md "wikilink")（児島富士）
<File:Aki> no kofuji.jpg|[似島](../Page/:ja:似島.md "wikilink")（安芸之小富士）

### 四國地方

#### 德島縣

  - 阿波富士（あわふじ） （[高越山](../Page/:ja:高越山.md "wikilink")） -
    [吉野川市](../Page/吉野川市.md "wikilink")

#### 香川縣

  - [讚岐七富士](../Page/讚岐七富士.md "wikilink")
      - 讚岐富士（さぬきふじ）（[飯野山](../Page/飯野山.md "wikilink")、421.9m） -
        [丸龜市](../Page/丸龜市.md "wikilink")・[坂出市](../Page/坂出市.md "wikilink")
      - 三木富士（みきふじ）（白山、202.7m） - 三木町
      - 御厩富士（みまやふじ）（六目山、317m） - [高松市](../Page/高松市.md "wikilink")
      - 羽床富士（はゆかふじ）（堤山、201.6m） - 丸龜市・[綾川町](../Page/綾川町.md "wikilink")
      - 綾上富士（あやかみふじ）（高鉢山、512.0m） - 綾川町
      - 高瀨富士（たかせふじ）（爺神山（とかみやま）、約210m） - [三豐市](../Page/三豐市.md "wikilink")
      - 有明富士（ありあけふじ）（江甫草山（江甫山・九十九山、つくもやま）、153.1m） -
        [觀音寺市](../Page/觀音寺市.md "wikilink")

#### 愛媛縣

  - [伊予富士](../Page/伊予富士.md "wikilink")（いよふじ）（1756m） -
    [西條市](../Page/西條市.md "wikilink")
  - [伊予小富士](../Page/:ja:伊予小富士.md "wikilink")（いよこふじ） -
    [松山市](../Page/松山市.md "wikilink")・[興居島](../Page/興居島.md "wikilink")
  - 大洲富士（おおずふじ）（[富士山](../Page/:ja:冨士山.md "wikilink")（とみすやま）） -
    [大洲市](../Page/大洲市.md "wikilink")
  - 小倉富士（おぐわふじ） -
    [北宇和郡](../Page/北宇和郡.md "wikilink")[鬼北町](../Page/鬼北町.md "wikilink")

#### 高知縣

  - 土佐富士（とさふじ）（高之森、299.5m） - [高知市](../Page/高知市.md "wikilink")

<File:Mt.Iino.jpg>|[飯野山](../Page/飯野山.md "wikilink")（讃岐富士）
<File:Mt.Iyofuji.jpg>|[伊予富士](../Page/伊予富士.md "wikilink")

### 九州地方

#### 福岡縣

  - 筑紫富士 （可也山） - [糸島市](../Page/糸島市.md "wikilink")
  - 企救富士 （貫山） -
    [北九州市](../Page/北九州市.md "wikilink")[小倉南區](../Page/小倉南區.md "wikilink")
  - 小富士 - [朝倉市](../Page/朝倉市.md "wikilink")

#### 佐賀縣

  - 松浦富士（まつらふじ）（腰岳） - [伊萬里市](../Page/伊萬里市.md "wikilink")
  - 肥前小富士（ひぜんこふじ）（唐泉山） - [嬉野市](../Page/嬉野市.md "wikilink")

#### 長崎縣

  - 相浦富士（あいのうらふじ） （[愛宕山](../Page/:ja:愛宕山_\(佐世保市\).md "wikilink")） -
    [佐世保市](../Page/佐世保市.md "wikilink")
  - 佐世保富士（させぼふじ） （[烏帽子岳](../Page/:ja:烏帽子岳_\(佐世保市\).md "wikilink")） -
    佐世保市
  - [富士山](../Page/富士山_\(南島原市\).md "wikilink")（ふじさん） -
    [南島原市](../Page/南島原市.md "wikilink")

#### 熊本縣

  - 小國富士（おぐにふじ）（[涌蓋山](../Page/涌蓋山.md "wikilink")（わいたさん）） -
    [阿蘇郡](../Page/阿蘇郡.md "wikilink")[小國町](../Page/小國町_\(熊本縣\).md "wikilink")

#### 大分縣

  - 豐後富士（ぶんごふじ）（[由布岳](../Page/由布岳.md "wikilink")） -
    [由布市](../Page/由布市.md "wikilink")
  - 日田富士（ひたふじ）（[月出山岳](../Page/:ja:月出山岳.md "wikilink")（かんとうだけ）） -
    [日田市](../Page/日田市.md "wikilink")
  - 玖珠富士（くすふじ）（[涌蓋山](../Page/涌蓋山.md "wikilink")（わいたさん）） -
    [玖珠郡](../Page/玖珠郡.md "wikilink")[九重町](../Page/九重町.md "wikilink")

#### 宮崎縣

  - 生駒富士（いこまふじ）（[夷守岳](../Page/夷守岳.md "wikilink")） -
    [小林市](../Page/小林市.md "wikilink")

#### 鹿兒島縣

  - 筑紫富士（つくしふじ）（[櫻島御岳](../Page/櫻島.md "wikilink")） -
    [鹿兒島市](../Page/鹿兒島市.md "wikilink")
  - 薩摩富士（さつまふじ）（[開聞岳](../Page/開聞岳.md "wikilink")） -
    [指宿市](../Page/指宿市.md "wikilink")
  - 藺牟田富士（いむたふじ）（飯盛山） - [薩摩川内市](../Page/薩摩川内市.md "wikilink")
  - トカラ富士（吐噶喇富士）（とからふじ）（御岳） -
    [中之島](../Page/:ja:中之島_\(鹿児島県\).md "wikilink")

#### 沖繩縣

  - 本部富士（もとぶふじ）（目良山） -
    [國頭郡](../Page/國頭郡.md "wikilink")[本部町](../Page/本部町.md "wikilink")
  - 塩屋富士（しおやふじ） - 國頭郡[大宜味村](../Page/大宜味村.md "wikilink")
  - 與那國富士（よなぐにふじ）（宇良部岳） -
    [八重山郡](../Page/八重山郡.md "wikilink")[與那國町](../Page/與那國町.md "wikilink")

<File:Ainoura> River and
MtAtago.jpg|[愛宕山](../Page/:ja:愛宕山_\(佐世保市\).md "wikilink")（相浦富士）
<File:はげの湯温泉1.JPG>|[涌蓋山](../Page/涌蓋山.md "wikilink")（小国富士、玖珠富士）
<File:Hinamoridake> Miyazaki.jpg|[夷守岳](../Page/夷守岳.md "wikilink")（生駒富士）
<File:Kaimondake> 2005 3 19.jpg|[開聞岳](../Page/開聞岳.md "wikilink")（薩摩富士）
<File:Sakurajima21.JPG>|[櫻島御岳](../Page/櫻島.md "wikilink")（筑紫富士）
<File:Dawn> in
Nakanoshima.jpg|[中之島](../Page/:ja:中之島_\(鹿児島県\).md "wikilink")（トカラ富士）

### 日本國外

#### 舊日本領

  - [得撫富士](../Page/得撫富士.md "wikilink")（うるっぷふじ） -
    [千島列島](../Page/千島列島.md "wikilink")[得撫島](../Page/得撫島.md "wikilink")
  - 松輪富士（まつわふじ）（[芙蓉山](../Page/萨雷切夫火山.md "wikilink")） -
    千島列島[松輪島](../Page/松輪島.md "wikilink")
  - 阿賴度富士（あらいどふじ）（[阿賴度山](../Page/:ja:阿頼度山.md "wikilink")） -
    千島列島[阿賴度島](../Page/阿賴度島.md "wikilink")
  - 樫保三富士（かしほみつふじ）（[樫保山](../Page/:ja:樫保山.md "wikilink")） -
    [樺太](../Page/樺太.md "wikilink")
  - 台灣富士（たいわんふじ）（[玉山](../Page/玉山.md "wikilink")） -
    [台灣](../Page/台灣.md "wikilink")
  - 淡水富士（たんすいふじ）（-{[占山](../Page/占山.md "wikilink")}-、尖山） - 台灣
  - 平溪富士（へいけいふじ）（[薯榔尖](../Page/薯榔尖.md "wikilink")、平溪富士山） - 台灣
  - 礁溪富士（しょうけいふじ）（[鵲子山](../Page/鵲子山.md "wikilink")、礁溪富士山） - 台灣
  - 基隆富士（キールンふじ）（[基隆山](../Page/基隆山.md "wikilink")） - 台灣
  - 台灣富士（たいわんふじ）（[加里山](../Page/加里山.md "wikilink")、台灣富士山\[1\]） - 台灣
  - 苗栗富士（びょうりつふじ）（[雙峰山](../Page/雙峰山.md "wikilink")） - 台灣
  - 茄苳富士（かたんふじ）（[茄苳富士山](../Page/茄苳富士山.md "wikilink")） - 台灣
  - 魚池富士（ぎょちふじ）（[魚池尖](../Page/魚池尖.md "wikilink")、魚池富山、魚池富士山） - 台灣
  - マヘボ富士（マヘボふじ）（[馬海僕富士山](../Page/馬海僕富士山.md "wikilink")） - 台灣
  - ホーゴー富士（ホーゴーふじ）（[荷戈富士山](../Page/荷戈富士山.md "wikilink")、花岡山） - 台灣
  - 阿緱富士（あこうふじ）（[井歩山](../Page/井歩山.md "wikilink")、阿猴富士山） - 台灣
  - 牡丹富士（ぼたんふじ）（[牡丹富士山](../Page/牡丹富士山.md "wikilink")） - 台灣
  - 台東富士（たいとうふじ）（[都蘭山](../Page/都蘭山.md "wikilink")、台東富士山） - 台灣
  - 貓公富士（ばこうふじ）（[八里灣山](../Page/八里灣山.md "wikilink")、貓公富士山） - 台灣
  - ターフン富士（ターフンふじ）（[大分富士山](../Page/大分富士山.md "wikilink")） - 台灣
  - 富士岳（[富士岳](../Page/富士岳.md "wikilink")） - 台灣
  - 富士山（[母安山](../Page/母安山.md "wikilink")） - 台灣
  - 小富士山（[南面掛嶼](../Page/南面掛嶼.md "wikilink")、澎湖富士山） - 台灣

<File:Matua.jpg>|[芙蓉山](../Page/萨雷切夫火山.md "wikilink")（松輪富士）
<File:Atlasovisland.jpg>|[阿頼度山](../Page/:ja:阿頼度山.md "wikilink")（阿頼度富士）
<File:MaHaiPuMountain.jpg>|[馬海僕富士山](../Page/馬海僕富士山.md "wikilink")（マヘボ富士）

#### 其他地區

  - 塔科馬富士（タコマふじ）（[瑞尼爾山](../Page/瑞尼爾山.md "wikilink")） -
    [美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")
  - 奧勒岡富士（オレゴン富士）（[胡德山](../Page/胡德山.md "wikilink")） -
    美國[奧勒岡州](../Page/奧勒岡州.md "wikilink")
  - 呂宋富士 （ルソンふじ）（[馬榮火山](../Page/馬榮火山.md "wikilink")） -
    [菲律賓](../Page/菲律賓.md "wikilink")
  - 南洋富士（なんようふじ）（[塔拉納基山](../Page/塔拉納基山.md "wikilink")、艾格蒙特山） -
    [紐西蘭](../Page/紐西蘭.md "wikilink")
  - 智利富士（チリふじ）（[奧索爾諾火山](../Page/奧索爾諾火山.md "wikilink")） -
    [智利](../Page/智利.md "wikilink")

<File:Mt>. Rainer-Reflection
Lake.JPG|[瑞尼爾山](../Page/瑞尼爾山.md "wikilink")（塔科馬富士）
<File:Mount> Hood reflected in Mirror Lake, Oregon.jpg
|[胡德山](../Page/胡德山.md "wikilink")（奧勒岡富士）
<File:Mayon.jpg>|[馬榮火山](../Page/馬榮火山.md "wikilink")（呂宋富士）
<File:Taranaki> daybreak.jpg|[塔拉納基山](../Page/塔拉納基山.md "wikilink")（南洋富士）
<File:Volcan> Osorno.jpg|[奧索爾諾火山](../Page/奧索爾諾火山.md "wikilink")（智利富士）

## 脚注

## 外部連結

  - [ふるさとの富士山大集合](https://web.archive.org/web/20110516153926/http://www.city.fuji.shizuoka.jp/hp/menu000003200/hpg000003127.htm)

[Category:富士山](../Category/富士山.md "wikilink")
[Category:日本山峰](../Category/日本山峰.md "wikilink")
[鄉土富士](../Category/鄉土富士.md "wikilink")

1.  [加里山 -
    氣象局](http://www.cwb.gov.tw/V7/forecast/entertainment/other/D128.htm)