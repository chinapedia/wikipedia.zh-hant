[Oberösterreich_in_Austria.svg](https://zh.wikipedia.org/wiki/File:Oberösterreich_in_Austria.svg "fig:Oberösterreich_in_Austria.svg")

**上奧地利州**（[德語](../Page/德語.md "wikilink")：Oberösterreich）是[奧地利面積第四大的州](../Page/奧地利.md "wikilink")（11,980平方公里）和人口第三多的州（1,376,797）。首府[林茨](../Page/林茨.md "wikilink")。与[捷克共和国相连](../Page/捷克共和国.md "wikilink")。

## 行政区划

上奥地利州分为15县3市。分别为：

  - [乌尔法尔城郊县](../Page/乌尔法尔城郊县.md "wikilink")
  - [佩尔格县](../Page/佩尔格县.md "wikilink")
  - [克雷姆斯河畔基希多夫县](../Page/克雷姆斯河畔基希多夫县.md "wikilink")
  - [因克赖斯地区里德县](../Page/因克赖斯地区里德县.md "wikilink")
  - [因河畔布劳瑙县](../Page/因河畔布劳瑙县.md "wikilink")
  - [埃费尔丁县](../Page/埃费尔丁县.md "wikilink")
  - [弗克拉布鲁克县](../Page/弗克拉布鲁克县.md "wikilink")
  - [弗赖施塔特县](../Page/弗赖施塔特县.md "wikilink")
  - [施泰尔兰县](../Page/施泰尔兰县.md "wikilink")
  - [林茨兰县](../Page/林茨兰县.md "wikilink")
  - [格蒙登县](../Page/格蒙登县.md "wikilink")
  - [格里斯基尔兴县](../Page/格里斯基尔兴县.md "wikilink")
  - [罗巴赫县](../Page/罗巴赫县.md "wikilink")
  - [谢尔丁县](../Page/谢尔丁县.md "wikilink")
  - [韦尔斯兰县](../Page/韦尔斯兰县.md "wikilink")

<!-- end list -->

  - [施泰尔](../Page/施泰尔.md "wikilink")（法定市）
  - [韦尔斯](../Page/韦尔斯_\(上奥地利州\).md "wikilink")（法定市）
  - [林茨](../Page/林茨.md "wikilink")（法定市）

## 参考文献

## 参见

  - [上奥地利州市镇列表](../Page/上奥地利州市镇列表.md "wikilink")

{{-}}

[O](../Category/奥地利联邦州.md "wikilink")
[上奧地利州](../Category/上奧地利州.md "wikilink")