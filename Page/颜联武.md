**顏聯武**（，），現任[D100大戲台台長](../Page/D100.md "wikilink")，[D100香港台](../Page/D100.md "wikilink")《[霎時衝動](../Page/霎時衝動.md "wikilink")》，《[寬講心靈](../Page/寬講心靈.md "wikilink")》節目主持人，[D100香港台](../Page/D100.md "wikilink")《[D100
Keep On
Singing](../Page/D100_Keep_On_Singing.md "wikilink")》節目主持人，《[停不了的金曲
iPod Music
Choice](../Page/停不了的金曲_iPod_Music_Choice.md "wikilink")》節目監製。他於1971年畢業於[嶺南大學香港同學會小學](../Page/嶺南大學香港同學會小學.md "wikilink")\[1\]，曾參與[麗的電視主辦的](../Page/麗的電視.md "wikilink")「一九八O年度亞洲歌唱比賽」，並於比賽中獲得香港區選拔賽冠軍\[2\]。

顏聯武是前[新城娛樂台音樂總監及節目策略師及](../Page/新城娛樂台.md "wikilink")[商業電台節目主持人](../Page/香港商業電台.md "wikilink")。在商台任職時憑藉磁性的聲線迅速走紅，他於[張承襄主政時加盟新城](../Page/張承襄.md "wikilink")，曾主持電台節目《今夜陽光燦爛》。亦不時參與廣告旁白。但[新城電台於](../Page/新城電台.md "wikilink")2007年1月改組，[新城娛樂台亦改組為](../Page/新城娛樂台.md "wikilink")[新城知訊台](../Page/新城知訊台.md "wikilink")。顏聯武的電台節目《[今夜陽光燦爛](../Page/今夜陽光燦爛.md "wikilink")》由[新城娛樂台改為於](../Page/新城娛樂台.md "wikilink")[新城財經台播放的同時改名為](../Page/新城財經台.md "wikilink")《[顏色生活](../Page/顏色生活.md "wikilink")》。2011年轉投[香港數碼廣播有限公司](../Page/香港數碼廣播有限公司.md "wikilink")，於[數碼大戲台主持節目](../Page/數碼大戲台.md "wikilink")《[霎時衝動](../Page/霎時衝動.md "wikilink")》。

## 現主持節目

### 電台

  - [D100](../Page/D100.md "wikilink")
    [香港台](../Page/D100_香港台.md "wikilink")《[霎時衝動](../Page/霎時衝動.md "wikilink")》，逢星期一至四
    21:00 - 00:00 集合中外60年金曲 顏聯武與你分享不同音樂選擇
  - [D100](../Page/D100.md "wikilink")
    [香港台](../Page/D100_香港台.md "wikilink")《[寬講心靈](../Page/寬講心靈.md "wikilink")》，逢星期一
    22:00 - 23:00 霎時衝動節目內播出 主持：顏聯武 蓮花少東金剛上師 (由普明佛學會贊助播出)
  - [D100](../Page/D100.md "wikilink")
    [PBS台](../Page/D100_PBS台.md "wikilink")《[寬講心靈再接再厲重溫版](../Page/寬講心靈.md "wikilink")》，逢星期五
    16:00 - 17:00 主持：顏聯武 蓮花少東金剛上師
  - [D100](../Page/D100.md "wikilink")
    [香港台](../Page/D100_香港台.md "wikilink") 《[D100 Keep On
    Singing](../Page/D100_Keep_On_Singing.md "wikilink")》，逢星期六 21:00 -
    00:00 主持：顏聯武 嘉賓主持：[韋綺姗](../Page/韋綺姗.md "wikilink")
  - [D100](../Page/D100.md "wikilink")
    [香港台](../Page/D100_香港台.md "wikilink") 監製：《[停不了的金曲 iPod
    Music
    Choice](../Page/停不了的金曲_iPod_Music_Choice.md "wikilink")》，逢星期六凌晨
    00:00 - 06:00, 逢星期日凌晨 00:00 - 05:00

## 曾主持節目

### 電台

  - [D100](../Page/D100.md "wikilink")《[霎時衝動(週日版)](../Page/霎時衝動\(週日版\).md "wikilink")》，[逢星期日
    晚上9時至12時](../Page/逢星期日_晚上9時至12時.md "wikilink")
  - [DBC 香港數碼廣播有限公司](../Page/DBC_香港數碼廣播有限公司.md "wikilink")，[DBC 7台
    數碼大戲台](../Page/DBC_7台_數碼大戲台.md "wikilink")《[霎時衝動](../Page/霎時衝動.md "wikilink")》
  - [DBC 香港數碼廣播有限公司](../Page/DBC_香港數碼廣播有限公司.md "wikilink")，[DBC 7台
    數碼大戲台](../Page/DBC_7台_數碼大戲台.md "wikilink")《[寬講心靈](../Page/寬講心靈.md "wikilink")》
  - [DBC 香港數碼廣播有限公司](../Page/DBC_香港數碼廣播有限公司.md "wikilink")，[DBC 1台
    數碼大聲台](../Page/DBC_1台_數碼大聲台.md "wikilink")《[紛婚中需要美](../Page/紛婚中需要美.md "wikilink")》
  - [新城財經台](../Page/新城財經台.md "wikilink")《[顏色生活](../Page/顏色生活.md "wikilink")》
  - [新城娛樂台](../Page/新城娛樂台.md "wikilink")《[今夜陽光燦爛](../Page/今夜陽光燦爛.md "wikilink")》——是顏聯武加盟[新城電台的首個電台節目](../Page/新城電台.md "wikilink")，從九十年代到[新城財經台成立前](../Page/新城財經台.md "wikilink")，即使[新城電台經歷多次](../Page/新城電台.md "wikilink")「大地震」，但節目仍然以該名字繼續。
  - [新城知訊台和](../Page/新城知訊台.md "wikilink")[新城財經台聯播深夜音樂節目](../Page/新城財經台.md "wikilink")《[流行直播室](../Page/流行直播室.md "wikilink")》
  - [商業一台](../Page/商業一台.md "wikilink")[雷霆881](../Page/雷霆881.md "wikilink")《[霎時衝動](../Page/霎時衝動.md "wikilink")》——1990年，《霎時衝動》是當年顏聯武接手[周美茵主持的同名節目](../Page/周美茵.md "wikilink")，由於傾聽聽眾心事的電台節目在當年算是新穎的節目題材，而顏聯武在這方面也做得得心應手，所以為晚上七時這個冷淡電台時段注入新氣象。直至1994年，由於受到當時電視節目「龍門陣」的論政熱潮影響，聽眾轉為關心新聞及公共事務，最後該時段被黃毓民《毓民七鐘敍》取代。
  - [商業一台](../Page/商業一台.md "wikilink")[雷霆881](../Page/雷霆881.md "wikilink")《[活力大都會](../Page/活力大都會.md "wikilink")》——約在1986年左右，商業一台進行節目大改革，原本下午時段的長壽節目《[空中俱樂部](../Page/空中俱樂部.md "wikilink")》結束，由該節目取代。當時商台起用多位新晉[唱片騎師如利達英](../Page/唱片騎師.md "wikilink")﹑李少慧等。顏聯武仍由這時從深宵節目調出主持下午節目。
  - [商業一台](../Page/商業一台.md "wikilink")[雷霆881](../Page/雷霆881.md "wikilink")《[深宵樂悠揚](../Page/深宵樂悠揚.md "wikilink")》——由八十年代初，顏聯武便由星期一至星期日主持這個商台由凌晨二時至六時的深宵節目。

### 電視

#### 亞洲電視

  - 外購紀錄片《哭泣的森林》及以後同類型節目（旁述）
  - 電視劇 《還君明珠》 電視劇開始時的介紹
  - 2011年-古董資訊節目《[藏．寶](../Page/藏．寶.md "wikilink")》（旁述）

#### [亞洲電視數碼媒體](../Page/亞洲電視數碼媒體.md "wikilink")

  - 2018年《今晚見》(嘉賓）

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th><p>發行年份</p></th>
<th><p>專輯名稱</p></th>
<th><p>歌曲目錄</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991</p></td>
<td><p>昨日情懷 今日感覺</p></td>
<td><p>01. 序<br />
02. (思念)明月千里寄相思<br />
03. (寄望)明日之歌<br />
04. (痛心)心里有風<br />
05. (情愁)情人的眼淚<br />
06. (漂泊)船<br />
07. (寄語)問白雲<br />
08. (等)痴痴的等<br />
09. (迷惘)藍與黑<br />
10. (再會)等著你回來</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>璀璨人間</p></td>
<td><p>01. 兩個字<br />
02. 璀璨<br />
03. 其實很想親近你<br />
04. 友情<br />
05. 何價<br />
06. 相依為命<br />
07. 半生<br />
08. 執著<br />
09. 一陣雨<br />
10. 月未亮<br />
11. 星閃閃<br />
12. 活著<br />
13. 劫後餘生</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p>聯想</p></td>
<td><p>01. 你怎麼捨得我難過<br />
02. 大約在冬季<br />
03. 夢醒時份<br />
04. 彎彎的月亮<br />
05. 寂寞難耐<br />
06. 把悲傷留給自己<br />
07. 最真的夢<br />
08. 其實你不懂我的心<br />
09. 猜心<br />
10. 想念你</p></td>
</tr>
</tbody>
</table>

### 其他歌曲

<table>
<thead>
<tr class="header">
<th><p>歌曲名稱</p></th>
<th><p>收錄專輯</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>歲月忘情</p></td>
<td><p>我和春天有個約會</p></td>
<td><p>亞洲電視劇集《<a href="../Page/我和春天有個約會.md" title="wikilink">我和春天有個約會</a>》插曲</p></td>
</tr>
<tr class="even">
<td><p>變色龍</p></td>
<td><p>驛動男人心2動情</p></td>
<td><p>翻唱自<a href="../Page/關正傑.md" title="wikilink">關正傑同名歌曲</a><br />
亞洲電視劇集《<a href="../Page/97變色龍.md" title="wikilink">97變色龍</a>》主題曲</p></td>
</tr>
<tr class="odd">
<td><p>#Jungle</p></td>
<td><p>#Jungle</p></td>
<td><p>演繹開頭旁白，由<a href="../Page/MastaMic.md" title="wikilink">MastaMic主唱</a></p></td>
</tr>
</tbody>
</table>

## 著作

| 出版年份        | 書名      | 出版       | 備註    |
| ----------- | ------- | -------- | ----- |
| 1993年7月22日  | 霎時衝動    | 自由文庫     |       |
| 1994年11月30日 | 霎時衝動（二） | 自由文庫     |       |
| 1995年7月20日  | 衷口而出    | 商台出版有限公司 | 語錄    |
| 1996年7月     | 一武傾情    | 商台出版有限公司 | 散文小說集 |

## 参考

## 外部連結

  - [Gary Ngan 顏聯武 Facebook 專頁](http://www.facebook.com/garynlm)
  - [霎時衝動 Facebook
    專頁](https://web.archive.org/web/20170123151519/https://www.facebook.com/D100SuddenImpulse)
  - [D100 Keep On Singing Facebook
    專頁](http://www.facebook.com/D100Singing)
  - [寬講心靈 Facebook
    專頁](http://www.facebook.com/pages/%E5%AF%AC%E8%AC%9B%E5%BF%83%E9%9D%88/317439231642711)

[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:香港作家](../Category/香港作家.md "wikilink")
[Category:嶺南大學香港同學會小學校友](../Category/嶺南大學香港同學會小學校友.md "wikilink")

1.
2.