[Ipod4g.JPG](https://zh.wikipedia.org/wiki/File:Ipod4g.JPG "fig:Ipod4g.JPG")
**iPod
Photo**是[蘋果電腦公司旗下](../Page/蘋果電腦公司.md "wikilink")[iPod數碼音樂播放器系列第一款備有彩色顯視屏並開始使用](../Page/iPod.md "wikilink")[Adobe公司的](../Page/Adobe.md "wikilink")[Myriad字體的iPod](../Page/Myriad.md "wikilink")。iPod
Photo 擁有一個兩寸65,536色[LCD顯視屏](../Page/LCD.md "wikilink")，解像度為220x176。iPod
Photo由2004年10月24日起開始發售，初時只有40[GB及](../Page/GB.md "wikilink")60[GB兩款型號](../Page/GB.md "wikilink")。iPod
Photo於2005年6月28日取代舊式黑白屏iPod成為標準iPod。
iPod
Photo的最大賣點是能作為一本隨身相簿。使用者能透過iPod附屬程式[iTunes匯入照片到iPod](../Page/iTunes.md "wikilink")
Photo中，支援格式包括[JPEG](../Page/JPEG.md "wikilink")，[BMP](../Page/BMP.md "wikilink")，[GIF](../Page/GIF.md "wikilink")，[TIFF及](../Page/TIFF.md "wikilink")[PNG](../Page/PNG.md "wikilink")。iPod
Photo亦能連接到[電視播放照片集](../Page/電視.md "wikilink")。[蘋果公司於其廣告中聲稱iPod](../Page/蘋果公司.md "wikilink")
Photo能持續播放十五個小時的音樂。

## 型號

<table>
<thead>
<tr class="header">
<th><p>世代</p></th>
<th><p>容量</p></th>
<th><p>顏色</p></th>
<th><p>連接</p></th>
<th><p>發行日期</p></th>
<th><p>操作系統</p></th>
<th><p>電池壽命<small>（小時）</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><small>原始</small></p></td>
<td><p>40 GB</p></td>
<td><p>White</p></td>
<td><p><a href="../Page/FireWire.md" title="wikilink">FireWire</a> or <a href="../Page/USB.md" title="wikilink">USB</a></p></td>
<td><p>October 26, 2004</p></td>
<td><p><small>Mac: <a href="../Page/Mac_OS_X_v10.2.md" title="wikilink">10.2.8</a><br />
Win: <a href="../Page/Windows_2000.md" title="wikilink">2000</a></small></p></td>
<td><p><em>audio</em>: 15<br />
<em>slideshow</em>: 5</p></td>
</tr>
<tr class="even">
<td><p>60 GB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Premium spin-off of 4G iPod with color screen and picture viewing.</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><small>改良第一版</small></p></td>
<td><p>30 GB</p></td>
<td><p>White</p></td>
<td><p><a href="../Page/FireWire.md" title="wikilink">FireWire</a> or <a href="../Page/USB.md" title="wikilink">USB</a></p></td>
<td><p>February 23, 2005</p></td>
<td><p><small>Mac: <a href="../Page/Mac_OS_X_v10.2.md" title="wikilink">10.2.8</a><br />
Win: <a href="../Page/Windows_2000.md" title="wikilink">2000</a></small></p></td>
<td><p><em>audio</em>: 15<br />
<em>slideshow</em>: 5</p></td>
</tr>
<tr class="odd">
<td><p>60 GB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Pack-ins and price reduced. Images directly viewable via optional iPod Camera Connector.</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [iPod](../Page/iPod.md "wikilink")
  - [iPod classic](../Page/iPod_classic.md "wikilink")
  - [iPod mini](../Page/iPod_mini.md "wikilink")
  - [iPod shuffle](../Page/iPod_shuffle.md "wikilink")
  - [蘋果公司](../Page/蘋果公司.md "wikilink")

[Category:IPod](../Category/IPod.md "wikilink")