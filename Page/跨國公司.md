[Chart-Biggest_Transnational_Companies_2011.png](https://zh.wikipedia.org/wiki/File:Chart-Biggest_Transnational_Companies_2011.png "fig:Chart-Biggest_Transnational_Companies_2011.png")
**跨國公司**、**多國公司**（，**MNC**），經常被稱為**跨國企業**（，**MNE**），是在多个[国家有业务](../Page/国家.md "wikilink")，通常规模很大的[公司](../Page/公司.md "wikilink")。这些公司在不同的国家或地區设有办事处、[工厂或](../Page/工厂.md "wikilink")[分公司](../Page/分公司.md "wikilink")，通常还有一个[总部用来协调全球的](../Page/总部.md "wikilink")[管理工作](../Page/管理.md "wikilink")。亦有**超國家公司**（**t**rans**n**ational
**c**orporation，**TNC**）、國際公司（international corporation）、「世界公司」\[1\]
\[2\]等稱呼。跨国公司通常利用承包商来製造特殊的商品，[外包的經營手法也經常被使用](../Page/外包.md "wikilink")。

大型的跨国公司其预算甚至超過許多國家的[政府預算](../Page/政府預算.md "wikilink")。它們對全球政治具有很大的[外交影響力](../Page/外交.md "wikilink")，這不僅是因為它們直接影響到許多政治人物選區的經濟以外，也是因為它們在公關與政治遊說上提供了資金來源。一個國家中的地區之間以及國家與國家之間都會彼此競爭以爭取跨國公司來到它們的地方設點（以及隨之而來的稅收、工作機會和經濟提升）。國家與地區[政府通常會提供优惠条件来吸引跨国公司](../Page/政府.md "wikilink")，比如財税优惠、承諾給予政府協助或较低的环境标准。也因此，類似的[國際投資雖然多少帶著社會意識](../Page/國際投資.md "wikilink")，其短期內能獲得巨大利益且四處流動的特性，經常給世人一種「是否逐步剝奪各國在經濟與社會方面的權力，甚至一步步掌控[全球](../Page/全球.md "wikilink")」的擔憂與批評\[3\]。第33任[美國副總統](../Page/美國副總統.md "wikilink")[亨利·阿加德·華萊士評論](../Page/亨利·阿加德·華萊士.md "wikilink")[法西斯主義時說](../Page/法西斯主義.md "wikilink")，與[主權國家的政府比起來](../Page/主權國家.md "wikilink")，真正掌控全球權力的是跨國公司，而且以[美國的跨國公司為主](../Page/美國.md "wikilink")：「如果將法西斯的內涵定義在一種『以金錢與權力最大化為終極目標，並不惜利用各種手段達到它』，那美國已有成千上萬個法西斯主義者了。（If
we define an American fascist as one who in case of conflict puts money
and power ahead of human beings, then there are undoubtedly several
million fascists in the United
States.）」2016年1月22日，[工人國際委員會台灣表示](../Page/工人國際委員會台灣.md "wikilink")，以美國為首的[跨太平洋戰略經濟夥伴關係協議](../Page/跨太平洋戰略經濟夥伴關係協議.md "wikilink")（TPP）會讓跨國公司剝奪國家主權\[4\]。

世界上最早的跨国公司是1600年成立的[英國东印度公司和](../Page/英國东印度公司.md "wikilink")1602年成立的[荷兰东印度公司](../Page/荷兰东印度公司.md "wikilink")\[5\]。

## 現實中

### 建筑机械行业

  - 美国[卡特彼勒公司](../Page/卡特彼勒.md "wikilink")
  - 日本[小松制作所](../Page/小松制作所.md "wikilink")
  - 日本[日立公司](../Page/日立.md "wikilink")
  - 德国[利勃海尔公司](../Page/利勃海尔.md "wikilink")
  - 中国大陸[中联重科](../Page/中联重科.md "wikilink")
  - 中国大陸[三一重工](../Page/三一重工.md "wikilink")
  - 韩国[斗山集团](../Page/斗山集团.md "wikilink")

### 制药行业

  - 美国[辉瑞公司](../Page/辉瑞.md "wikilink")
  - 瑞士[诺华公司](../Page/诺华.md "wikilink")
  - 瑞士[罗氏公司](../Page/罗氏.md "wikilink")
  - 法国[赛诺菲公司](../Page/赛诺菲.md "wikilink")
  - 英国[葛兰素史克公司](../Page/葛兰素史克.md "wikilink")
  - 美国[嬌生公司](../Page/嬌生公司.md "wikilink")

### 医疗设备行业

  - 美国[强生](../Page/强生.md "wikilink")
  - 美国[通用电气](../Page/通用电气.md "wikilink")
  - 美国[美敦力](../Page/美敦力.md "wikilink")
  - 德国[西门子](../Page/西门子.md "wikilink")
  - 荷兰[飞利浦](../Page/飞利浦.md "wikilink")
  - 瑞士[诺华](../Page/诺华.md "wikilink")

### 電子代工行业

  - 台灣[鴻海](../Page/鴻海.md "wikilink")

### 电视机行业

  - 韩国[三星](../Page/三星.md "wikilink")
  - 韩国[LG](../Page/LG.md "wikilink")
  - 中国[TCL](../Page/TCL.md "wikilink")
  - 日本[索尼](../Page/索尼.md "wikilink")
  - 中国[海信](../Page/海信.md "wikilink")
  - 中国[创维](../Page/创维.md "wikilink")
  - 台灣[鴻海夏普](../Page/鴻海夏普.md "wikilink")

### [液晶面板行业](../Page/液晶.md "wikilink")

  - 韩国[LG](../Page/LG.md "wikilink")
  - 韩国[三星](../Page/三星.md "wikilink")
  - 中国大陆[京东方](../Page/京东方.md "wikilink")
  - 台湾[群创](../Page/群创.md "wikilink")
  - 台湾[友达](../Page/友达.md "wikilink")

### [个人电脑行业](../Page/个人电脑.md "wikilink")

  - 中国[联想](../Page/联想集团.md "wikilink")
  - 美国[惠普](../Page/惠普.md "wikilink")
  - 美国[戴尔](../Page/戴尔.md "wikilink")
  - 台湾[宏碁](../Page/宏碁.md "wikilink")
  - 台湾[华硕](../Page/华硕.md "wikilink")
  - 美国[苹果](../Page/苹果电脑.md "wikilink")
  - 日本[东芝](../Page/东芝.md "wikilink")

### [平板电脑行业](../Page/平板电脑.md "wikilink")

  - 美国[苹果](../Page/苹果公司.md "wikilink")
  - 韩国[三星](../Page/三星.md "wikilink")
  - 中国[联想](../Page/联想.md "wikilink")
  - 中国[华为](../Page/华为.md "wikilink")
  - 台灣[華碩](../Page/華碩.md "wikilink")

### 手机行业

  - 韩国[三星](../Page/三星.md "wikilink")
  - 美国[苹果](../Page/苹果.md "wikilink")
  - 中国[华为](../Page/华为.md "wikilink")
  - 中国[小米](../Page/小米.md "wikilink")
  - 中国[OPPO](../Page/OPPO.md "wikilink")
  - 韩国[LG](../Page/LG.md "wikilink")
  - 台灣[HTC](../Page/HTC.md "wikilink")

### 汽车行业

  - 日本[丰田](../Page/丰田.md "wikilink")
  - 德国[大众](../Page/大众.md "wikilink")
  - 美国[通用](../Page/通用.md "wikilink")
  - 日本[日产](../Page/日产.md "wikilink")
  - 韩国[现代](../Page/现代汽车集团.md "wikilink")
  - 美国[福特](../Page/福特汽车.md "wikilink")
  - 美国[克莱斯勒](../Page/克莱斯勒.md "wikilink")
  - 日本[本田](../Page/本田.md "wikilink")
  - 法国[雪铁龙](../Page/雪铁龙.md "wikilink")
  - 日本[铃木](../Page/铃木.md "wikilink")

### 货车行业

  - 德国[戴姆勒](../Page/戴姆勒.md "wikilink")
  - 中国[东风](../Page/东风汽车公司.md "wikilink")
  - 德国[大众汽车](../Page/大众汽车.md "wikilink")
  - 瑞典[沃尔沃](../Page/沃尔沃.md "wikilink")
  - 印度[塔塔](../Page/塔塔汽車.md "wikilink")

### 机车行业

  - 日本[本田](../Page/本田.md "wikilink")
  - 日本[雅马哈](../Page/雅马哈.md "wikilink")
  - 日本[铃木](../Page/铃木.md "wikilink")
  - 台灣[光陽](../Page/光陽.md "wikilink")
  - 台灣[三陽](../Page/三陽.md "wikilink")

### 小型飞机行业

  - 加拿大[庞巴迪宇航公司](../Page/庞巴迪.md "wikilink")
  - [巴西航空工业公司](../Page/巴西航空工业公司.md "wikilink")
  - 意大利&法国[ATR](../Page/ATR.md "wikilink")

### 中大型飞机行业

  - 美国[波音](../Page/波音.md "wikilink")
  - 法国[空客](../Page/空客.md "wikilink")

### [半导体行业](../Page/半导体.md "wikilink")

  - 美国[英特尔](../Page/英特尔.md "wikilink")
  - 韩国[三星](../Page/三星.md "wikilink")
  - 美国[高通](../Page/高通.md "wikilink")
  - 美国[美光](../Page/美光.md "wikilink")
  - 韩国[鲜京](../Page/鲜京.md "wikilink")
  - 日本[东芝](../Page/东芝.md "wikilink")
  - 台灣[台積電](../Page/台積電.md "wikilink")
  - 中国[华为](../Page/华为.md "wikilink")

### [自动化行业](../Page/自动化.md "wikilink")

  - 德国[西门子](../Page/西门子公司.md "wikilink")
  - 瑞士[ABB](../Page/ABB.md "wikilink")
  - 日本[三菱](../Page/三菱.md "wikilink")
  - 法国[施耐德](../Page/施耐德电气.md "wikilink")
  - 日本[欧姆龙](../Page/欧姆龙.md "wikilink")

### 照相机行业

  - 日本[佳能](../Page/佳能.md "wikilink")
  - 日本[尼康](../Page/尼康.md "wikilink")
  - 日本[索尼](../Page/索尼.md "wikilink")
  - 韩国[三星](../Page/三星.md "wikilink")
  - 日本[富士](../Page/富士.md "wikilink")
  - 德國[徠卡](../Page/徠卡.md "wikilink")
  - 瑞典[哈蘇](../Page/哈蘇.md "wikilink")

### 摄影機行業

  - 美国[gopro](../Page/gopro.md "wikilink")
  - 日本[佳能](../Page/佳能.md "wikilink")
  - 日本[松下](../Page/松下.md "wikilink")

### 硬盘行业

  - 美国[西部数据](../Page/西部数据.md "wikilink")
  - 美国[希捷](../Page/希捷.md "wikilink")
  - 日本[东芝](../Page/东芝.md "wikilink")

### 电池行业

  - 美国[金霸王](../Page/金霸王.md "wikilink")
  - 美国[劲量](../Page/劲量.md "wikilink")
  - 中国[南孚](../Page/南孚.md "wikilink")

### 打印机行业

  - 美国[惠普](../Page/惠普.md "wikilink")
  - 日本[佳能](../Page/佳能.md "wikilink")
  - 日本[爱普生](../Page/爱普生.md "wikilink")

### 空调行业

  - 中国[格力](../Page/格力.md "wikilink")
  - 中国[海尔](../Page/海尔.md "wikilink")
  - 中国[美的](../Page/美的.md "wikilink")
  - 中国[海信](../Page/海信.md "wikilink")

### 钟表行业

  - 瑞士[劳力士](../Page/劳力士.md "wikilink")
  - 日本[西鐵城](../Page/西鐵城.md "wikilink")
  - 日本[精工](../Page/精工.md "wikilink")
  - 瑞士[百达翡丽](../Page/百达翡丽.md "wikilink")
  - 日本[卡西欧](../Page/卡西欧.md "wikilink")
  - 瑞士[伯爵](../Page/伯爵.md "wikilink")

### 汽车轮胎行业

  - 日本[普利司通](../Page/普利司通.md "wikilink")
  - 法国[米其林](../Page/米其林.md "wikilink")
  - 美国[固特异](../Page/固特异.md "wikilink")
  - 德国[大陆集团](../Page/大陆集团.md "wikilink")
  - 意大利[倍耐力](../Page/倍耐力.md "wikilink")
  - 日本[住友](../Page/住友.md "wikilink")
  - 台灣[正新](../Page/正新.md "wikilink")
  - 韩国[韩泰](../Page/韩泰.md "wikilink")

### 电梯行业

  - 美国[奥的斯](../Page/奥的斯.md "wikilink")
  - 瑞士[迅达](../Page/迅达.md "wikilink")
  - 芬兰[通力](../Page/通力.md "wikilink")
  - 德国[蒂森克虏伯](../Page/蒂森克虏伯.md "wikilink")
  - 日本[三菱](../Page/三菱.md "wikilink")

## 参见

  - [福布斯500强](../Page/福布斯500强.md "wikilink")
  - [全球化](../Page/全球化.md "wikilink")
  - [走出去戰略](../Page/走出去戰略.md "wikilink")

## 備註、注釋

<references group="注"/>

<div class="references-small">

</div>

## 資料來源

<div class="references-small">

</div>

<references/>

[\*](../Category/跨国公司.md "wikilink")

1.  ，裝訂：平裝，2nd edtion。
2.  此稱呼應衍生自[翻譯問題](../Page/翻譯.md "wikilink")，[譯者刻意以此稱呼凸顯效應以及較能使初學者明白其意義](../Page/譯者.md "wikilink")。
3.  ，裝訂：平裝，2nd edtion。
4.
5.   "GlobalInc. An Atlas of The Multinational Corporation" Medard Gabel
    & Henry Bruner, New York: The New Press , 2003. ISBN
    978-1-56584-727-9