**楠屬**（[学名](../Page/学名.md "wikilink")：），[樟目](../Page/樟目.md "wikilink")[樟科的一属](../Page/樟科.md "wikilink")。

## 分类

本属已知至少有50种，部份如下：

  - [沼楠](../Page/沼楠.md "wikilink") *Phoebe angustifolia*
  - [闽楠](../Page/闽楠.md "wikilink") *Phoebe bournei*
  - [短序楠](../Page/短序楠.md "wikilink") *Phoebe brachythyrsa*
  - [浙江楠](../Page/浙江楠.md "wikilink") *Phoebe chekiangensis*
  - [山楠](../Page/山楠.md "wikilink") *Phoebe chinensis*
  - [竹叶楠](../Page/竹叶楠.md "wikilink") *Phoebe faberi*
  - [台楠](../Page/台楠.md "wikilink") *Phoebe formosana*
  - [长毛楠](../Page/长毛楠.md "wikilink") *Phoebe forrestii*
  - [粉叶楠](../Page/粉叶楠.md "wikilink") *Phoebe glaucophylla*
  - [茶槁楠](../Page/茶槁楠.md "wikilink") *Phoebe hainanensis*
  - [细叶楠](../Page/细叶楠.md "wikilink") *Phoebe hui*
  - [湘楠](../Page/湘楠.md "wikilink") *Phoebe hunanensis*
  - [红毛山楠](../Page/红毛山楠.md "wikilink") *Phoebe hungmaoensis*
  - [桂楠](../Page/桂楠.md "wikilink") *Phoebe kwangsiensis*
  - [披针叶楠](../Page/披针叶楠.md "wikilink") *Phoebe lanceolata*
  - [雅砻江楠](../Page/雅砻江楠.md "wikilink") *Phoebe legendrei*
  - [利川楠](../Page/利川楠.md "wikilink") *Phoebe lichuanensis*
  - [大果楠](../Page/大果楠.md "wikilink") *Phoebe macrocarpa*
  - [大萼楠](../Page/大萼楠.md "wikilink") *Phoebe megacalyx*
  - [小叶楠](../Page/小叶楠.md "wikilink") *Phoebe microphylla*
  - [小花楠](../Page/小花楠.md "wikilink") *Phoebe minutiflora*
  - [墨脱楠](../Page/墨脱楠.md "wikilink") *Phoebe motuonan*
  - [滇楠](../Page/滇楠.md "wikilink") *Phoebe nanmu*
  - [白楠](../Page/白楠.md "wikilink") *Phoebe neurantha*
      - [短叶楠](../Page/短叶楠.md "wikilink") *Phoebe neurantha* var.
        *brevifolia*
      - [兴义楠](../Page/兴义楠.md "wikilink") *Phoebe neurantha* var.
        *cavaleriei*
  - [光枝楠](../Page/光枝楠.md "wikilink") *Phoebe neuranthoides*
  - [黑叶楠](../Page/黑叶楠.md "wikilink") *Phoebe nigrifolia*
  - [琴叶楠](../Page/琴叶楠.md "wikilink") *Phoebe pandurata*
  - [普文楠](../Page/普文楠.md "wikilink") *Phoebe puwenensis*
  - [红梗楠](../Page/红梗楠.md "wikilink") *Phoebe rufescens*
  - [紫楠](../Page/紫楠.md "wikilink") *Phoebe sheareri*
      - [峨眉楠](../Page/峨眉楠.md "wikilink") *Phoebe sheareri* var.
        *omeiensis*
  - [乌心楠](../Page/乌心楠.md "wikilink") *Phoebe tavoyana*
  - [崖楠](../Page/崖楠.md "wikilink") *Phoebe yaiensis*
  - [景东楠](../Page/景东楠.md "wikilink") *Phoebe yunnanensis*
  - [楠木](../Page/楠木.md "wikilink") *Phoebe zhennan*

## 参考文献

[Category:樟科](../Category/樟科.md "wikilink")
[楠屬](../Category/楠屬.md "wikilink")