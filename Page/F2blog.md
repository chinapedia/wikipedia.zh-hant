**F2blog**是[PHP語言的中文](../Page/PHP.md "wikilink")[部落格平台](../Page/部落格.md "wikilink")，以[Tatter
Tools為藍本設計](../Page/Tatter_Tools.md "wikilink")，同時也參考了[WordPress等部落格](../Page/WordPress.md "wikilink")，把多個平台的優點整合而成。支援建構在Linux和Windows
server的PHP+[MySQL](../Page/MySQL.md "wikilink")，目前最新版本是 1.2 build 03.01
版。

開發理念：F2BLOG 自由誌，一班為興趣而開發的BLOG程式，我們不是為了獲利，也不收分毫，只想給大家知道華人也有好的程序。

## 功能

  - 內建[TinyMCE和](../Page/TinyMCE.md "wikilink")[FCKeditor兩種](../Page/FCKeditor.md "wikilink")[所見即所得編輯器和使用](../Page/所見即所得.md "wikilink")[UBB語法的](../Page/BBCode.md "wikilink")[PHPWind編輯器](../Page/PHPWind.md "wikilink")
  - 可開放讓會員共同編輯
  - 支援自行增加、調整版面與外掛模組
  - [繁體中文和](../Page/繁體中文.md "wikilink")[簡體中文相互轉換功能](../Page/簡體中文.md "wikilink")
  - 提供文章的分類和個別的[RSS](../Page/RSS.md "wikilink")
  - 完整備份功能
  - 生成靜態網頁功能
  - 私密文章功能限定某些文章只供內部人員瀏覽
  - [標籤](../Page/標籤.md "wikilink")（Tags）功能便於閱讀熱門重要的文章

### 模板、主題

F2blog使用和[pjblog共通的模版](../Page/pjblog.md "wikilink")，並且可藉由修改[CSS來控制](../Page/CSS.md "wikilink")。

### 外掛

完整版內建四十多個插件，使用者可以自行在後端控制台開啟或關閉。所有插件由PHP撰寫，也可以自行開發。

### 語言

F2blog使用[UTF-8編碼](../Page/UTF-8.md "wikilink")，相容於中日韓等[雙位元字元](../Page/CJKV.md "wikilink")，內建有[英文](../Page/英文.md "wikilink")、[繁體中文](../Page/繁體中文.md "wikilink")、[簡體中文三種介面](../Page/簡體中文.md "wikilink")。

### 運作環境

  - PHP 4 以上
  - MySQL 4.0以上

## 歷史

F2blog開發團隊創立於2006年5月12日，成員由[香港和](../Page/香港.md "wikilink")[中國人組成](../Page/中國.md "wikilink")。

2006年7月發佈了F2blog的第一個版本，到2007年3月1日的1.2版，共有兩次改版。

在1.2版發佈之後，F2blog開發團隊就沒有再發表任何更新的版本，只有在公式論壇（現已關閉）上零星公開安全性修正檔。

2008年9月，F2blog公式網站無法訪問。

2008年11月20日，F2blog開發團隊的Terry表示所有成員都無法繼續進行開發\[1\]，F2blog開發團隊形同解散。

### F2blog.cont

2008年11月15日，由中國、香港、台灣三地有志的F2blog使用者自己組成F2.CONT團隊，為了延續F2blog的開發\[2\]。

## 版本

  - 1.0
    2006年7月發佈，是F2blog的第一個版本。

<!-- end list -->

  - 1.1beta
    2006年11月公開的測試版，隨後並有數次改版。

<!-- end list -->

  - 1.2
    2007年3月1日發表1.2正式版。

### F2blog.cont

以下為F2blog.cont之後的版本

  - 1.0
    2008年11月21日，F2.CONT團隊發表第一個版本，內容是F2blog
    1.2版之後所有公開過的修正，和數個bug修正，本身和F2blog
    1.2版沒有太大差異。

<!-- end list -->

  - 未來
    F2.CONT目前維護人為Wenchi0920,不過自2009年9月2號後,就再也沒有任何更新\[3\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [F2Blog.Cont](http://www.f2cont.com/)
  - [F2blog簡易操作說明](http://www.blog101.url.tw/)
  - [F2Blog教學網頁（影音版）](https://archive.is/20071015160615/http://163.32.161.9/maylike/teach/F2Blog/)

[Category:網誌軟體](../Category/網誌軟體.md "wikilink")

1.  <http://www.chong.com.hk/read-692.html>
2.
3.  <http://code.google.com/p/f2cont/source/list>