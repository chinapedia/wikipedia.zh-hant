**太元**（376年-396年）是[東晋皇帝](../Page/東晋.md "wikilink")[晋孝武帝司马曜的第二个](../Page/晋孝武帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计21年。

太元二十一年九月[晋安帝即位沿用](../Page/晋安帝.md "wikilink")，次年改元[隆安元年](../Page/隆安_\(年號\).md "wikilink")。

期间较为重要的事件是太元八年（[383年](../Page/383年.md "wikilink")）的[淝水之战](../Page/淝水之战.md "wikilink")。

## 纪年

| 太元                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 376年                           | 377年                           | 378年                           | 379年                           | 380年                           | 381年                           | 382年                           | 383年                           | 384年                           | 385年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") |
| 太元                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            | 十八年                            | 十九年                            | 二十年                            |
| [公元](../Page/公元纪年.md "wikilink") | 386年                           | 387年                           | 388年                           | 389年                           | 390年                           | 391年                           | 392年                           | 393年                           | 394年                           | 395年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") |
| 太元                               | 二十一年                           |                                |                                |                                |                                |                                |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 396年                           |                                |                                |                                |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [丙申](../Page/丙申.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |                                |                                |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[太元年号](../Page/太元.md "wikilink")
  - 同期存在的其他政权年号
      - [升平](../Page/升平.md "wikilink")：[前凉年号](../Page/前凉.md "wikilink")
      - [凤凰](../Page/凤凰.md "wikilink")（386年二月-十一月）：[张大豫自立年号](../Page/张大豫.md "wikilink")
      - [建元](../Page/建元.md "wikilink")（365年-385年七月）：[前秦政权](../Page/前秦.md "wikilink")[苻坚年号](../Page/苻坚.md "wikilink")
      - [太安](../Page/太安.md "wikilink")（385年八月-386年十月）：[前秦政权](../Page/前秦.md "wikilink")[苻丕年号](../Page/苻丕.md "wikilink")
      - [太初](../Page/太初.md "wikilink")（386年十一月-394年六月）：[前秦政权](../Page/前秦.md "wikilink")[苻登年号](../Page/苻登.md "wikilink")
      - [延初](../Page/延初.md "wikilink")（394年七月-394年十月）：[前秦政权](../Page/前秦.md "wikilink")[苻崇年号](../Page/苻崇.md "wikilink")
      - [元光](../Page/元光.md "wikilink")（393年六月-394年七月）：[窦冲自立年号](../Page/窦冲.md "wikilink")
      - [白雀](../Page/白雀.md "wikilink")（384年四月-386年四月）：[后秦政权](../Page/后秦.md "wikilink")[姚苌年号](../Page/姚苌.md "wikilink")
      - [建初](../Page/建初.md "wikilink")（386年四月-394年四月）：[后秦政权](../Page/后秦.md "wikilink")[姚苌年号](../Page/姚苌.md "wikilink")
      - [皇初](../Page/皇初.md "wikilink")（394年五月-399年九月）：[后秦政权](../Page/后秦.md "wikilink")[姚兴年号](../Page/姚兴.md "wikilink")
      - [燕元](../Page/燕元.md "wikilink")（384年-386年二月）：[后燕政权](../Page/后燕.md "wikilink")[慕容垂年号](../Page/慕容垂.md "wikilink")
      - [建兴](../Page/建兴.md "wikilink")（386年二月-396年四月）：[后燕政权](../Page/后燕.md "wikilink")[慕容垂年号](../Page/慕容垂.md "wikilink")
      - [永康](../Page/永康_\(後燕\).md "wikilink")（396年四月-398年四月）：[后燕政权](../Page/后燕.md "wikilink")[慕容宝年号](../Page/慕容宝.md "wikilink")
      - [建光](../Page/建光.md "wikilink")（388年二月-391年十月）：[翟辽自立年号](../Page/翟辽.md "wikilink")
      - [定鼎](../Page/定鼎.md "wikilink")（391年十月-392年六月）：翟辽子[翟钊年号](../Page/翟钊.md "wikilink")
      - [燕兴](../Page/燕兴.md "wikilink")（384年四月-十二月）：[西燕政权](../Page/西燕.md "wikilink")[慕容泓年号](../Page/慕容泓.md "wikilink")
      - [更始](../Page/更始.md "wikilink")（385年-386年二月）：[西燕政权](../Page/西燕.md "wikilink")[慕容冲年号](../Page/慕容冲.md "wikilink")
      - [昌平](../Page/昌平.md "wikilink")（386年二月-三月）：[西燕政权](../Page/西燕.md "wikilink")[段随年号](../Page/段随.md "wikilink")
      - [建明](../Page/建明.md "wikilink")（386年三月）：[西燕政权](../Page/西燕.md "wikilink")[慕容顗年号](../Page/慕容顗.md "wikilink")
      - [建平](../Page/建平.md "wikilink")（386年三月）：[西燕政权](../Page/西燕.md "wikilink")[慕容瑶年号](../Page/慕容瑶.md "wikilink")
      - [建武](../Page/建武.md "wikilink")（386年三月-九月）：[西燕政权](../Page/西燕.md "wikilink")[慕容忠年号](../Page/慕容忠.md "wikilink")
      - [中兴](../Page/中兴.md "wikilink")（386年十月-394年八月）：[西燕政权](../Page/西燕.md "wikilink")[慕容永年号](../Page/慕容永.md "wikilink")
      - [建义](../Page/建义.md "wikilink")（385年九月-388年六月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏国仁年号](../Page/乞伏国仁.md "wikilink")
      - [太初](../Page/太初_\(西秦\).md "wikilink")（388年六月-400年七月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏-{乾}-歸年号](../Page/乞伏乾歸.md "wikilink")
      - [太安](../Page/太安.md "wikilink")（386年十月-389年正月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [麟嘉](../Page/麟嘉.md "wikilink")（389年二月-396年六月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [龙飞](../Page/龍飛_\(後涼\).md "wikilink")（396年六月-399年）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [建国](../Page/建国.md "wikilink")（338年十一月-376年）：[代政权](../Page/代国.md "wikilink")[拓跋什翼犍年号](../Page/拓跋什翼犍.md "wikilink")
      - [登国](../Page/登国.md "wikilink")（386年-396年六月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")
      - [皇始](../Page/皇始.md "wikilink")（396年七月-398年）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:东晋年号](../Category/东晋年号.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:370年代中国政治](../Category/370年代中国政治.md "wikilink")
[Category:380年代中国政治](../Category/380年代中国政治.md "wikilink")
[Category:390年代中国政治](../Category/390年代中国政治.md "wikilink")