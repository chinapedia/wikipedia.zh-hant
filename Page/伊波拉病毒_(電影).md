是一部1996年的[香港](../Page/香港電影.md "wikilink")[III級](../Page/香港電影分級制度.md "wikilink")[剝削電影](../Page/剝削電影.md "wikilink")，由[王晶監製](../Page/王晶_\(導演\).md "wikilink")，[邱禮濤執導](../Page/邱禮濤.md "wikilink")，金像影帝[黃秋生主演](../Page/黃秋生.md "wikilink")。本片製作時正值黃秋生憑《[八仙飯店之人肉叉燒包](../Page/八仙飯店之人肉叉燒包.md "wikilink")》贏得[金像影帝](../Page/香港電影金像獎.md "wikilink")，與此同時又適逢非洲國家[盧旺達爆發](../Page/盧旺達.md "wikilink")[伊波拉病毒](../Page/伊波拉病毒.md "wikilink")，故以相關新聞製作類似《[八仙飯店之人肉叉燒包](../Page/八仙飯店之人肉叉燒包.md "wikilink")》的電影。

此片裡黃秋生飾演一個變態人物，拍下了大量[恐怖](../Page/恐怖.md "wikilink")、[變態](../Page/變態.md "wikilink")、[血腥之鏡頭](../Page/血腥.md "wikilink")，可說是[港產片中的典型](../Page/港產片.md "wikilink")[邪典電影](../Page/邪典電影.md "wikilink")。

## 劇情

古惑仔阿雞〔[黃秋生飾](../Page/黃秋生.md "wikilink")〕因與大哥之妻子[通姦](../Page/通姦.md "wikilink")，被大哥（[成奎安飾](../Page/成奎安.md "wikilink")）發現。大哥本欲把阿雞生殖器官割除，卻反被阿雞殺死，阿雞並殺死大嫂及其手下，本欲燒死其女兒，卻被撞見而失敗逃跑。阿雞因謀殺案逃到[南非一家餐館工作](../Page/南非.md "wikilink")，其間姦殺了一名昏迷的[黑人少女](../Page/黑人.md "wikilink")，但死者本身已有[伊波拉病毒](../Page/伊波拉病毒.md "wikilink")，阿雞感染了病毒卻沒死，反而成了帶原者。不久阿雞連餐館東主夫婦及表哥也殺死，並把三人起肉拆骨，製成「非洲叉燒包(漢堡包)」出售。再取得餐館東主的財產後，阿雞又跑回香港，其身上[病毒因而蔓延至很多人](../Page/病毒.md "wikilink")，而阿雞卻因而變得喪心病狂......
最后阿雞被一名身著防護衣的警察用火燒死。

## 演員

|                                  |        |                                        |                                      |
| -------------------------------- | ------ | -------------------------------------- | ------------------------------------ |
| **演員**                           | **角色** | **粵語配音**                               | **關係**                               |
| [黃秋生](../Page/黃秋生.md "wikilink") | 阿雞     | 黃秋生                                    | 古惑仔，原為餐館員工，阿霞前男友，後感染伊波拉病毒，不过本身却有免疫力。 |
| [陳妙瑛](../Page/陳妙瑛.md "wikilink") | 阿霞     | [林姍姍](../Page/林姍姍.md "wikilink")       | 阿雞前女友，後感染伊波拉病毒。                      |
| [成奎安](../Page/成奎安.md "wikilink") | 大佬     | 成奎安                                    | 阿雞大佬，被阿雞殺死。                          |
| [曾燕](../Page/曾燕.md "wikilink")   | 大佬老婆   | [陸惠玲](../Page/陸惠玲.md "wikilink")       | 被阿雞剪舌而死。                             |
| [羅莽](../Page/羅莽.md "wikilink")   | 餐館老闆   | [張炳強](../Page/張炳強.md "wikilink")       | 被伙計阿雞殺死，最后被弄成汉堡肉。                    |
| [張璐](../Page/張璐.md "wikilink")   | 餐館老板娘  |                                        | 被阿雞先姦后殺，最后被弄成汉堡肉。                    |
| [尹揚明](../Page/尹揚明.md "wikilink") | 楊國立    | [陳欣](../Page/陳欣_\(配音員\).md "wikilink") | 督察，用火把阿雞烧死，最后感染伊波拉病毒。                |
| [王翠玲](../Page/王翠玲.md "wikilink") | 周小麗    |                                        | 大佬之女（成年），後感染伊波拉病毒                    |
| [吳瑞庭](../Page/吳瑞庭.md "wikilink") |        | [潘文柏](../Page/潘文柏.md "wikilink")       | 小麗男友                                 |
| [敖志君](../Page/敖志君.md "wikilink") | 馬先生    | [郭立文](../Page/郭立文.md "wikilink")       | 阿霞丈夫，道友，後感染伊波拉病毒                     |

## 外部連結

  - {{@movies|fehk50116163|伊波拉病毒}}

  -
  -
  -
  -
  -
[分类:邱礼涛电影](../Page/分类:邱礼涛电影.md "wikilink")

[6](../Category/1990年代香港電影作品.md "wikilink")
[Category:香港恐怖片](../Category/香港恐怖片.md "wikilink")
[Category:香港驚悚片](../Category/香港驚悚片.md "wikilink")
[Category:香港災難片](../Category/香港災難片.md "wikilink")
[Category:1990年代恐怖片](../Category/1990年代恐怖片.md "wikilink")
[Category:嘉禾電影](../Category/嘉禾電影.md "wikilink")
[Category:香港背景电影](../Category/香港背景电影.md "wikilink")
[Category:南非背景電影](../Category/南非背景電影.md "wikilink")
[Category:病毒題材作品](../Category/病毒題材作品.md "wikilink")
[Category:人吃人題材電影](../Category/人吃人題材電影.md "wikilink")