**進階電視系統委員會**（ATSC，Advanced Television Systems
Committee）是1982年成立的委員會，訂立用於[數碼電視的](../Page/數碼電視.md "wikilink")**進階電視系統委員會標準**（ATSC，Advanced
Television Systems Committee standards）

ATSC系統原為取代[北美洲最常用的](../Page/北美洲.md "wikilink")[NTSC制式](../Page/NTSC.md "wikilink")。在ATSC規範之下，[高畫質電視可產生高達](../Page/高畫質電視.md "wikilink")1920×1080[像素的](../Page/像素.md "wikilink")[寬螢幕](../Page/寬螢幕.md "wikilink")[16:9畫面尺寸](../Page/16:9.md "wikilink")──超過早前標準[顯示解析度的六倍](../Page/顯示解析度.md "wikilink")。然而，許多不同尺寸的畫面也有支援，所以高達六個標準解析的子頻道可被播送在一個現存的[電視台](../Page/電視台.md "wikilink")6[兆赫](../Page/兆赫.md "wikilink")[頻道](../Page/頻道.md "wikilink")。

ATSC也傲於其劇院質素聲音，因其採用[杜比數碼](../Page/杜比數碼.md "wikilink")[AC-3格式](../Page/AC-3.md "wikilink")，提供5.1[環迴立體聲](../Page/環迴立體聲.md "wikilink")。眾多的輔助數據傳輸廣播的服務可能也可提供。

如同ATSC系統所需佔用一整個頻道，當使用ATSC的[電視台也想保留類比訊號時](../Page/電視台.md "wikilink")，必須播送在兩個分離的頻道。

## 解析度

ATSC支援多種不同的[顯示解析度](../Page/顯示解析度.md "wikilink")、[長寬比及](../Page/長寬比_\(影像\).md "wikilink")[幀率](../Page/幀率.md "wikilink")。

  - 640x480（4:3，正方形像素）
      - [隔行掃描](../Page/隔行掃描.md "wikilink")
          - 29.97（59.94場/每秒）
          - 30（60場/每秒）
      - [逐行掃描](../Page/逐行掃描.md "wikilink")
          - 23.976
          - 24
          - 29.97
          - 30
          - 59.94
          - 60
  - 704x480（4:3或16:9，非正方形像素）
      - [隔行掃描](../Page/隔行掃描.md "wikilink")
          - 29.97（59.94場/每秒）
          - 30（60場/每秒）
      - [逐行掃描](../Page/逐行掃描.md "wikilink")
          - 23.976
          - 24
          - 29.97
          - 30
          - 59.94
          - 60
  - 1280x720（16:9，正方形像素）
      - [逐行掃描](../Page/逐行掃描.md "wikilink")
          - 23.976
          - 24
          - 29.97
          - 30
          - 59.94
          - 60
  - 1920x1080（16:9，正方形像素）
      - [隔行掃描](../Page/隔行掃描.md "wikilink")
          - 29.97（59.94場/每秒）
          - 30（60場/每秒）
      - [逐行掃描](../Page/逐行掃描.md "wikilink")
          - 23.976
          - 24
          - 29.97
          - 30

不同的解析度能運作在[逐行掃描或](../Page/逐行掃描.md "wikilink")[隔行掃描模式](../Page/隔行掃描.md "wikilink")，雖然最高解析度1080線系統無法顯示在59.94幀／秒或60幀／每秒的逐行掃瞄畫面（當時認為這種技術太先進，增加畫面品質被認為太欠缺考量能被傳遞的資料量）。6MHz無線廣播頻道可傳送每秒19.39[Mb](../Page/Mb.md "wikilink")（[百萬位元](../Page/百萬位元.md "wikilink")）的資料量；比照[DVD標準最大允許](../Page/DVD.md "wikilink")[位元率為每秒](../Page/位元率.md "wikilink")10.08Mb。

ATSC有三種基本的顯示尺寸。基本和增強型NTSC和PAL畫面尺寸是屬於低階的480或576掃瞄線。中型尺寸畫面有720解析度的掃瞄線和960或1280像素寬（[4:3](../Page/4:3.md "wikilink")，傳統型，和[16:9](../Page/16:9.md "wikilink")，
寬螢幕型，分別的長寬比）。
最高階的有1080掃瞄線1440或者1920像素寬（這也是，[4:3和](../Page/4:3.md "wikilink")[16:9分別的長寬比](../Page/16:9.md "wikilink")）。1080掃瞄線影像是實際以1920×1088像素幀編碼，但是最後8條掃瞄線省略顯示。這是因為MPEG-2影像格式的限制，要求被編碼的亮度採樣數（即像素）要可被16整除。

## 模塊化和傳輸

ATSC訊號是設計為使用與[NTSC電視頻道相同的](../Page/NTSC.md "wikilink")6MHz[頻寬](../Page/頻寬.md "wikilink")。

## 使用情況

美國、[加拿大](../Page/加拿大.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[韓國和](../Page/韓國.md "wikilink")[洪都拉斯正在使用](../Page/洪都拉斯.md "wikilink")。

[台灣曾短暫測試ATSC](../Page/台灣.md "wikilink")，[台視在](../Page/台視.md "wikilink")2000年曾用該系統HDTV轉播[中華民國第十任總統就職典禮](../Page/中華民國.md "wikilink")，但僅限[陽明山](../Page/陽明山.md "wikilink")[竹子湖發射站收訊範圍](../Page/竹子湖.md "wikilink")，後因行動接收的收訊不理想，改為使用[DVB-T](../Page/DVB-T.md "wikilink")。

## 其他系統

ATSC與[DVB-T標準和](../Page/DVB-T.md "wikilink")[日本導入的](../Page/日本.md "wikilink")[ISDB-T](../Page/ISDB-T.md "wikilink")（ISDB模組同為[巴西](../Page/巴西.md "wikilink")[SBTVD-T標準的基礎](../Page/SBTVD-T.md "wikilink")）並存。
另有一個稱為[ADTB的類似系統](../Page/ADTB.md "wikilink")，該系統是作為[中國新](../Page/中華人民共和國.md "wikilink")[DMB-T/H雙系統的一部分而研發的](../Page/DMB-T/H.md "wikilink")。雖然[中國的官方做法是採用雙制式系統](../Page/中華人民共和國.md "wikilink")，
但目前並無要求須讓接受端可在雙系統下運作，而且也沒有播送端設備與接收器製造廠有支援ADTB調變。

### 比較

儘管曾有批評指導入及使用ATSC系統既複雜且昂貴， 但如今播送端和接受端設備的價格已與DVB相去不遠。

## 電視解析度總覽

<div id="TV resolution">

</div>

## 外部連結

  - [ATSC website](http://www.atsc.org)
  - [The common ATSC formats represented in different
    criteria](http://www.paradiso-design.net/videostandards_en.html#atsc)
  - [What exactly is
    ATSC?](http://www.hdtvprimer.com/ISSUES/what_is_ATSC.html)

[\*](../Category/ATSC.md "wikilink")
[Category:MPEG](../Category/MPEG.md "wikilink")
[Category:數碼電視](../Category/數碼電視.md "wikilink")
[Category:高清晰度电视](../Category/高清晰度电视.md "wikilink")
[Category:電視科技](../Category/電視科技.md "wikilink")