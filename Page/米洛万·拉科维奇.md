**米洛万·拉科维奇**（，），出生于[前南斯拉夫塞尔维亚地区的](../Page/前南斯拉夫.md "wikilink")[乌日策](../Page/乌日策.md "wikilink")，[塞尔维亚职业篮球运动员](../Page/塞尔维亚.md "wikilink")，司职[中锋](../Page/中锋.md "wikilink")，身高207公分，体重235磅，效力于赛黑联赛的Mega
Ishrana俱乐部，在2007年NBA选秀中被达拉斯小牛在第二轮选中。

## 欧洲赛场

拉科维奇曾在2001年随南斯拉夫U-16青年队取得过欧洲锦标赛的冠军，之后也加入了塞尔维亚和黑山U-20国家队。他在2005-06赛季后期脚部曾受过一次伤。2006-07赛季在联赛中拉科维奇取得每场比赛上场20.4分钟、场均13.7分、篮板4.0个、高达64.7的命中率以及作为中锋来说十分精准78.2%的罚球命中率。

## NBA选秀

据NBA选秀报告称，拉科维奇是喜欢接触低位的一类球员，可以在球场上把握住自己的位置，不惧怕高大强壮的防守者，属于彪悍型中锋，他主要的进攻方式是底线上篮、扣篮以及勾手，拥有良好的爆发力和身体素质，进攻防守转换较为迅速，可以发展成为一位不错的角色球员。但拉科维奇的缺点是进攻方式较为单一创造性不够，控球能力较为缺乏，传球能力和意识也不强，一旦被对手包夹防守就会遇到麻烦。

2007年6月28日在[2007年NBA选秀中第二轮末位被](../Page/2007年NBA选秀.md "wikilink")[达拉斯小牛队选中](../Page/达拉斯小牛队.md "wikilink")，之后达拉斯小牛队随即用他加上现金补偿和[奥兰多魔术交换了](../Page/奥兰多魔术.md "wikilink")[雷肖恩·特里](../Page/雷肖恩·特里.md "wikilink")。

## 参考资料

## 外部链接

  - [Milovan Raković career
    stats''](http://www.fibaeurope.com/cid_f43ulKJBGLcVnbH-aqLVu2.teamID_390.compID_YUjW-7-FJ,kK9s431Lyr41.season_2001.roundID_2297.playerID_38628.html)
  - [米洛万-拉科维奇_新浪竞技风暴](http://sports.sina.com.cn/star/MilovanRakovic/index.shtml)

[Category:塞尔维亚男子篮球运动员](../Category/塞尔维亚男子篮球运动员.md "wikilink")
[Category:奥兰多魔术队球员](../Category/奥兰多魔术队球员.md "wikilink")