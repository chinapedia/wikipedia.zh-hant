[Rubiksmagic.jpg](https://zh.wikipedia.org/wiki/File:Rubiksmagic.jpg "fig:Rubiksmagic.jpg")

**魔板**（**Rubik's
Magic**），是由[扭計骰的发明人](../Page/魔方.md "wikilink")、[匈牙利雕刻家](../Page/匈牙利.md "wikilink")、建筑学教授[厄尔诺·鲁比克教授发明的一种智力玩具](../Page/厄尔诺·鲁比克.md "wikilink")，最早在1980年代由Matchbox公司制作。

魔板由8个呈2×4排列的正方形组成，在其对角线的槽中有细线将每个正方形连起来。所有的正方形都能层叠摆放，并且从两个方向展开。魔板的正面图案是三个无联系的圆环，背面则是打乱、连接在一起的三个圆环。游戏的目的就是将原来长方形的魔板经过层叠和展开变成一个心形，并且将背面的3个圆环图案复原。

[Rubiksmagic_solved.jpg](https://zh.wikipedia.org/wiki/File:Rubiksmagic_solved.jpg "fig:Rubiksmagic_solved.jpg")

解决这个谜题的方法有很多，熟练的玩家能在5秒内完成问题的解决，事实上，魔板的世界纪录为0.69秒。其他的关于魔板的玩法有将魔板折叠成给出的形状等。

1987年，一款名叫“[大师版本](../Page/大師魔板.md "wikilink")”的魔板发布，它包含的呈2×6排列的12个正方形都为银色，上面会有5个相互联系的圆环，游戏的目的则是通过变换将图案变成一个“[W](../Page/W.md "wikilink")”形。1990年代后期，最初版本的魔板经过Oddzon改造后重新发布，这次的魔板的图案颜色发生了变化。也有未经过授权的2×8版本的魔板投入了生产。还有魔板迷自己制作了2×12版本的魔板。目前也已經出現2×20版本的魔板。

鲁比克教授拥有魔板在[匈牙利的专利权](../Page/匈牙利.md "wikilink")（HU
1211/85，1985年3月19日授权），和[美国的专利权](../Page/美国.md "wikilink")（US
4,685,680，1987年8月11日授权）。

2013年，WCA取消了魔板比赛项目。\[1\]

## 复原方法

## 參見

  - [智力遊戲](../Page/智力遊戲.md "wikilink")

## 注解

## 外部链接

  - [香港第一個扭計骰組織--好骰同盟](https://web.archive.org/web/20080305234308/http://www.hkcubeunion.net/)
  - [各种魔板](https://web.archive.org/web/20061127072734/http://twistypuzzles.com/cgi-bin/pdb-search.cgi?act=sim&key=magic&key2=&field=Mechanism&field2=&type=All&type2=&off=0)
  - [魔板纪念堂](http://www.doyouremember.co.uk/memory.asp?memID=2098)

[Category:魔術方塊](../Category/魔術方塊.md "wikilink")
[Category:玩具](../Category/玩具.md "wikilink")
[Category:1987年面世的產品](../Category/1987年面世的產品.md "wikilink")

1.  [WCA在2013年取消魔板项目](http://page.renren.com/601446694/note/865418118?op=next&curTime=1344670825000)