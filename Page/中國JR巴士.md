  -
    <small>本文標題的公司名稱與中國大陸無關。</small>

[JR_bus_chugoku_tuyama_daikou.jpg](https://zh.wikipedia.org/wiki/File:JR_bus_chugoku_tuyama_daikou.jpg "fig:JR_bus_chugoku_tuyama_daikou.jpg")
[JRbus_744-2901-C.jpg](https://zh.wikipedia.org/wiki/File:JRbus_744-2901-C.jpg "fig:JRbus_744-2901-C.jpg")
[Chugoku_JR_bus01.jpg](https://zh.wikipedia.org/wiki/File:Chugoku_JR_bus01.jpg "fig:Chugoku_JR_bus01.jpg")
**中國JR巴士**（日：、英：**Chugoku JR Bus
Company**）是於[日本](../Page/日本.md "wikilink")[中國地方](../Page/中國地方.md "wikilink")[島根縣](../Page/島根縣.md "wikilink")、[岡山縣](../Page/岡山縣.md "wikilink")、[廣島縣](../Page/廣島縣.md "wikilink")、[山口縣地區經營](../Page/山口縣.md "wikilink")[巴士業務的](../Page/巴士.md "wikilink")[JR西日本子公司](../Page/西日本旅客鐵道.md "wikilink")。總社位於[廣島縣](../Page/廣島縣.md "wikilink")[廣島市](../Page/廣島市.md "wikilink")[南區松原町](../Page/南區_\(廣島市\).md "wikilink")1番6號。

## 历史

  - 1986年12月4日　[日本國有鐵道改革法施行](../Page/日本國有鐵道改革法.md "wikilink")。旅客自動車運送事業由各旅客會社繼承、原則是獨立經營。
  - 1987年4月1日
    [國鐵分割民營化](../Page/日本國有鐵道.md "wikilink")、[JR西日本成立](../Page/西日本旅客鐵道.md "wikilink")。國鐵中國地方自動車局所經營的巴士事業由JR西日本中國自動車事業部繼承。
  - 1988年3月2日　中國JR巴士株式會社成立。
  - 1988年4月1日　營業開始。
  - 1998年2月3日　廣島營業所・海田市營業所統合、成為新・廣島營業所。
  - 1998年5月　廢除秋吉派出所。
  - 1999年1月12日
    由於[井原鐵道井原線開業](../Page/井原鐵道井原線.md "wikilink")、與其並行的路線（倉敷～清音～矢掛）讓與[井笠鐵道](../Page/井笠鐵道.md "wikilink")。廢除矢掛派出所。
  - 2000年4月1日　組織改正。
  - 2000年6月30日　廢除岩國支所。
  - 2003年4月1日
    島根縣・岡山縣的全部（岡山縣的清心學園學校巴士除外）、廣島縣的部份一般路線巴士取消、路線讓與[谷本ハイヤー](../Page/谷本ハイヤー.md "wikilink")・吉田村營巴士（現・[雲南市民巴士](../Page/雲南市民巴士.md "wikilink")）・[石見交通](../Page/石見交通.md "wikilink")・[岡山電氣軌道](../Page/岡山電氣軌道.md "wikilink")・[兩備巴士](../Page/兩備巴士.md "wikilink")・[下津井電鐵](../Page/下津井電鐵.md "wikilink")・[中鐵巴士](../Page/中鐵巴士.md "wikilink")・[廣交観光](../Page/廣交観光.md "wikilink")・[廣島電鐵](../Page/廣島電鐵.md "wikilink")・[藝陽巴士](../Page/藝陽巴士.md "wikilink")・[備北交通](../Page/備北交通.md "wikilink")。廢除赤名支所・川本支所・矢上在勤・海田市支所。
  - 2005年7月1日　廢除川本線・川本北線（廢除特急銀山號、特急江之川號）。

## 主要停車站

### 鳥取縣

  - [米子站](../Page/米子站.md "wikilink")（只限高速巴士）

### 島根縣

  - [松江站](../Page/松江站_\(日本\).md "wikilink")（只限高速巴士）
  - [出雲市站](../Page/出雲市站.md "wikilink")（只限高速巴士）
  - [江津站](../Page/江津站.md "wikilink")（只限高速巴士）
  - [濱田站](../Page/濱田站.md "wikilink")（只限高速巴士）
  - [益田站](../Page/益田站_\(日本\).md "wikilink")（只限高速巴士）
  - [津和野站](../Page/津和野站.md "wikilink")（只限高速巴士）

### 岡山縣

  - [岡山站前](../Page/岡山站.md "wikilink")（只限高速巴士）
  - [天滿屋巴士中心](../Page/天滿屋巴士中心.md "wikilink")（只限高速巴士）
  - [中庄站](../Page/中庄站.md "wikilink")（只限清心學園學校巴士）
  - [倉敷站北口](../Page/倉敷站.md "wikilink")（只限高速巴士）

### 廣島縣

#### 廣島地區

  - [西條站](../Page/西條站_\(廣島縣\).md "wikilink")
  - [東廣島站](../Page/東廣島站.md "wikilink")
  - [廣島大學](../Page/廣島大學.md "wikilink")
  - [廣島國際大學](../Page/廣島國際大學.md "wikilink")
  - [吳大學](../Page/吳大學.md "wikilink")
  - [廣站](../Page/廣站.md "wikilink")
  - [新廣站](../Page/新廣站.md "wikilink")
  - [阿賀站前](../Page/安藝阿賀站.md "wikilink")
  - [吳站](../Page/吳站.md "wikilink")
  - [廣島站](../Page/廣島站.md "wikilink")
  - [廣島巴士中心](../Page/廣島巴士中心.md "wikilink")
  - [橫川站](../Page/橫川站_\(廣島縣\).md "wikilink")
  - [不動院前站](../Page/不動院前站.md "wikilink")
  - [中筋站](../Page/中筋站.md "wikilink")
  - [大塚站](../Page/大塚站_\(廣島縣\).md "wikilink")
  - [玖村站](../Page/玖村站.md "wikilink")
  - [下深川站](../Page/下深川站.md "wikilink")
  - [中島站](../Page/中島站_\(廣島縣\).md "wikilink")
  - [可部站](../Page/可部站.md "wikilink")
  - 飯室
  - [大朝站](../Page/大朝站.md "wikilink")

#### 福山地區

  - [福山站](../Page/福山站.md "wikilink")（只限高速巴士）
  - [廣島空港](../Page/廣島空港.md "wikilink")（只限高速（空港Limousine）巴士）

### 山口縣

  - [大畠站](../Page/大畠站.md "wikilink")
  - [光站](../Page/光站.md "wikilink")
  - [防府站](../Page/防府站.md "wikilink")
  - [新山口站](../Page/新山口站.md "wikilink")
  - [山口站](../Page/山口站_\(山口縣\).md "wikilink")
  - [美祢站](../Page/美祢站.md "wikilink")
  - [東萩站](../Page/東萩站.md "wikilink")
  - [萩巴士中心](../Page/萩巴士中心.md "wikilink")
  - [萩站](../Page/萩站.md "wikilink")
  - [宇部中央](../Page/宇部中央.md "wikilink")（只限高速巴士）
  - [小野田站](../Page/小野田站.md "wikilink")（只限高速巴士）
  - [下關站](../Page/下關站.md "wikilink")（只限高速巴士）

## 路線

詳細資料可參看[\#外部連結的中國JR巴士網頁](../Page/#外部連結.md "wikilink")。

## 車輛

### 車輛稱號

依照國鐵巴士附例法則來命名。

## 關連會社

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
  - [西日本JR巴士](../Page/西日本JR巴士.md "wikilink")
  - [西日本巴士網絡服務](../Page/西日本巴士網絡服務.md "wikilink")

## 外部連結

  - [中國JR巴士](http://www.chugoku-jrbus.co.jp/)

[Category:日本巴士公司](../Category/日本巴士公司.md "wikilink")
[Category:西日本旅客鐵道](../Category/西日本旅客鐵道.md "wikilink")
[Category:廣島縣公司](../Category/廣島縣公司.md "wikilink")