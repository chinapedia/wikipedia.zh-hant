[Winzip-logo.png](https://zh.wikipedia.org/wiki/File:Winzip-logo.png "fig:Winzip-logo.png")

**WinZip**是一个由WinZip Computing（前Nico Mak Computing）开发，適用於[Microsoft
Windows作業系統的](../Page/Microsoft_Windows.md "wikilink")[数据压缩软件](../Page/数据压缩.md "wikilink")。它原先的基礎是使用[PKZIP](../Page/PKZIP.md "wikilink")，但也不同程度地支持其它压缩格式。

WinZip最初在1990年代早期出现，是一个[共享软件](../Page/共享软件.md "wikilink")，PKZIP的[图形用户界面前台程序](../Page/图形用户界面.md "wikilink")。1996年前后，WinZip加入来自[Info-ZIP计划的代码](../Page/Info-ZIP.md "wikilink")，从此不需要PKZIP程序以执行。

从版本6.0到9.0，注册用户可以下载软件的最新版本，只需输入购买时获得的序列号就能获得一次免费升级。但从版本10.0起後，这项升级功能被取消。\[1\]WinZip大致上分為标准版（Standard）和专业版（Professional）。

後來，[微軟從](../Page/微軟.md "wikilink")[Windows
Me開始後的作業系統](../Page/Windows_Me.md "wikilink")，包括[Windows
Vista以及](../Page/Windows_Vista.md "wikilink")[Windows
7皆内建開啟壓縮檔的功能](../Page/Windows_7.md "wikilink")，一定程度上減少WinZip的销售量。

2006年5月，[科立爾數位科技收购WinZip](../Page/科立爾數位科技.md "wikilink") Computing。\[2\]

WinZip的官方網站上提供為期30天的免费试用版本。然而，一些舊的版本仍然可以在试用期之后继续使用。

## 功能

  - 创建或加入文件到ZIP压缩包、解压[ZIP压缩包](../Page/ZIP格式.md "wikilink")，还可解压部分其他格式压缩包。
  - 可配置的[Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
    Shell整合。
  - 128位和256位[AES](../Page/AES.md "wikilink")[加密](../Page/加密.md "wikilink")。\[3\]这种加密方式替代了较早版本较不安全的PKZIP
    2.0加密方式。加密的执行使用了[Brian
    Gladman的代码](../Page/Brian_Gladman.md "wikilink")，2003年3月27日被FIPS-197认证。\[4\]
    版本9加入了64位PKZIP文件格式，消灭了压缩文件个数65,535的[上限和](../Page/16位元.md "wikilink")4GB的压缩文件／被压缩文件大小[上限](../Page/32位元.md "wikilink")。
  - 支持[bzip2](../Page/bzip2.md "wikilink")（9.0），[PPMd](../Page/PPM_compression_algorithm.md "wikilink")（10.0），[WavPack](../Page/WavPack.md "wikilink")（11.0），使压缩文件能更小，但压缩/解压时间会更长（特别是当采用PPMd时）。
  - 支持解压.bz2和.[rar文件](../Page/rar.md "wikilink")。
  - 当合适的外部程序被安装时，WinZip可以支持[ARC](../Page/.arc.md "wikilink")，[ARJ](../Page/ARJ.md "wikilink")，[LHA压缩文件](../Page/LHA_\(文件格式\).md "wikilink")。
  - 直接向CD/DVD写入[ZIP](../Page/ZIP_\(文件格式\).md "wikilink")。
  - 自动备份功能
  - 整合[FTP上传](../Page/FTP.md "wikilink")
  - 电邮[ZIP压缩文件](../Page/ZIP_\(文件格式\).md "wikilink")
  - 使用[Unicode来确保正确显示Zip内带有国际字符的文件名](../Page/Unicode.md "wikilink")。（在版本11.2之前，WinZip不支持带有[Unicode的文件名](../Page/Unicode.md "wikilink")。\[5\]
    \[6\]在试图添加这些文件到压缩文件中时会弹出"Warning: Could not open for reading: ..."）
  - 内建支持读写LHA和LZH压缩文件

## 历史

ZIP文件格式（PKzip）在1989年由[Phil
Katz和他的PKWare公司在](../Page/Phil_Katz.md "wikilink")[MS-DOS上所发明](../Page/MS-DOS.md "wikilink")。\[7\]

因为PKWare没有为Zip注册商标和申请算法专利，并且没有意识到Windows将会统治操作系统市场，Nico
Mak（当时效力于Mansfield Software Group,
Inc.）抓住了这个时机并为Windows平台开发了WinZip。

据称一些被PKWARE使用的算法其实抄袭了System Enhancement Associates的Thom
Henderson发明的[ARC](../Page/ARC.md "wikilink")，这可能是PKWARE不能为PKZIP申请专利的原因。\[8\]

WinZip 8.0是最后一个官方支援[Windows 95的版本](../Page/Windows_95.md "wikilink")。

WinZip
9.0支持AES加密ZIP，[BZip2算法压缩的ZIP压缩文件](../Page/BZip2.md "wikilink")（仅仅解压）。

WinZip
10加入了创建／解压[PPMD算法压缩的ZIP压缩文件](../Page/PPMD.md "wikilink")。这是最后一个官方支持[Windows
98以及](../Page/Windows_98.md "wikilink")[Windows
ME的版本](../Page/Windows_ME.md "wikilink")。WinZip专业版（Pro）在这个版本中被引入，加入了自动操作和任务计划功能。

WinZip
11加入了创建／解压[WavPack压缩的ZIP压缩文件的功能](../Page/WavPack.md "wikilink")。安装程序仍然可以在[Windows
98](../Page/Windows_98.md "wikilink")、[ME上安装](../Page/Windows_ME.md "wikilink")，但不被官方支持。WinZip专业版加入了被动FTP支持，FTP传输，e-mail通知，自定义工作选择，完整大小的图片浏览器等新功能。

WinZip 11.1获得[Windows
Vista认证](../Page/Windows_Vista.md "wikilink")，并將介面融入Windows
Vista的主题，並開始支援[64位元作業系統](../Page/64位元.md "wikilink")。

WinZip 11.2可以独立创建[LHA压缩文件](../Page/LHA.md "wikilink")。加入Unicode文件名支持。

WinZip
12.0针对照片和图形文件的压缩进行了改进，可以直接将相机内的照片自动打包出来，可以在ZIP里面直接查看照片的缩略图。增加了对[ISO](../Page/ISO.md "wikilink")、[IMZ等新格式的支持](../Page/IMZ.md "wikilink")。改进了加密算法和用户界面。

WinZip
12.1推出了全新的压缩文件格式[ZIPX](../Page/ZIPX.md "wikilink")。并添加了可以对通过WinZip直接发送的电子邮件中的图片大小进行调整这一新功能。

WinZip 14.5開始支援[Windows
7](../Page/Windows_7.md "wikilink")，並將使用介面改為和[Microsoft
Office
2007以及](../Page/Microsoft_Office_2007.md "wikilink")[2010相同的](../Page/Microsoft_Office_2010.md "wikilink")[Ribbon介面](../Page/Ribbon.md "wikilink")。

## 参见

  - [压缩软件比较](../Page/压缩软件比较.md "wikilink")

## 注释

## 外部链接

  - [WinZip主页](http://www.winzip.com/)
  - [WinZip正體中文首頁](http://www.corel.com/servlet/Satellite/tw/ct/Product/1207834997200#versionTabview=tab1&tabview=tab0)
  - [MaximumCompression.com](http://www.maximumcompression.com/)，压缩软件基准性能比较

[Category:数据压缩软件](../Category/数据压缩软件.md "wikilink")
[Category:专有软件](../Category/专有软件.md "wikilink")
[Category:共享软件](../Category/共享软件.md "wikilink")

1.  [为什么我的序列号不能在WinZip 11中工作?](http://www.winzip.com/xregno.htm)
2.  [Corel Corporation收购WinZip
    Computing](http://investor.corel.com/releasedetail.cfm?ReleaseID=194952)

3.  , [AES Encryption Information: Encryption Specification AE-1 and
    AE-2](http://www.winzip.com/aes_info.htm)
4.  见Brian
    Gladman的[AES代码网页](http://fp.gladman.plus.com/cryptography_technology/fileencrypt/)
    和官方[AES认证列表](http://csrc.nist.gov/groups/STM/cavp/documents/aes/aesval.html)
5.
6.
7.  [Computing
    Encyclopedia](http://www.smartcomputing.com/editorial/dictionary/detail.asp?&searchtype=0&DicID=19632&RefType=Encyclopedia%7CSmart)
    - 关于WinZip的一篇文章
8.  [1](http://en.wikipedia.org/wiki/Phil_Katz) -
    关于Phil_Katz的文章，PKZIP作者