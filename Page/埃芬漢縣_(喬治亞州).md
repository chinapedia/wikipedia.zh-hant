**埃芬漢縣**（**Effingham County,
Georgia**）是[美國](../Page/美國.md "wikilink")[喬治亞州東部的一個縣](../Page/喬治亞州.md "wikilink")，東鄰[南卡羅萊納州](../Page/南卡羅萊納州.md "wikilink")。面積1,250平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口37,535人。縣治[春田市](../Page/春田市_\(喬治亞州\).md "wikilink")
(Springfield)。

成立於1777年2月5日，是該州最早成立的七個縣之一。縣名紀念因為反對攻打殖民地居民而辭職的[湯瑪斯·霍華德，第三代埃芬漢伯爵](../Page/湯瑪斯·霍華德，第三代埃芬漢伯爵.md "wikilink")\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[E](../Category/佐治亚州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.