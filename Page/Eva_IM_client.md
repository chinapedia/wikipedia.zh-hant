**Eva**是一款[KDE下的](../Page/KDE.md "wikilink")[QQ客户端](../Page/QQ.md "wikilink")，基于[Qt库开发](../Page/Qt.md "wikilink")，遵照[GNU
GPL第二版发布](../Page/GNU_GPL.md "wikilink")，是[自由软件](../Page/自由软件.md "wikilink")。

目前的最新版是0.4.1

## 功能

  - 文字聊天
  - 显示QQ常见的表情头像
  - 查看好友个人信息
  - 修改自己的个人信息
  - 上传、下载好友分组
  - 新建群，进行群聊天
  - 登录方式可选择[TCP或者](../Page/TCP.md "wikilink")[UDP](../Page/UDP.md "wikilink")，并且可以选用[HTTP代理](../Page/HTTP.md "wikilink")
  - 自动回复
  - 支持文件传输，好友发送的自定义表情，也是通过文件传输来实现
  - 查找好友、添加好友、好友添加验证

## 参见

  - [QQ](../Page/QQ.md "wikilink")
  - [LumaQQ](../Page/LumaQQ.md "wikilink")
  - [即时通讯软件列表](../Page/即时通讯软件列表.md "wikilink")
  - [即时通讯软件比较](../Page/即时通讯软件比较.md "wikilink")

## 外部链接

  - [Eva首页](http://sourceforge.net/projects/evaq/)
  - [EvaQQ论坛](https://web.archive.org/web/20071120104802/http://www.myswear.net/forum/forumdisplay.php?fid=3)

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:自由的即时通讯软件](../Category/自由的即时通讯软件.md "wikilink")
[Category:KDE](../Category/KDE.md "wikilink")