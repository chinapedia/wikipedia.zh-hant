**威廉·H·苏厄德**（**William Henry
Seward**，），[美国律师](../Page/美国.md "wikilink")、地产經紀人、政治家，曾任[美国国务卿](../Page/美国国务卿.md "wikilink")、[美國參議員和](../Page/美國參議員.md "wikilink")[纽约州州长](../Page/纽约州州长.md "wikilink")。

## 生平

西華德曾於1849－1861年擔任兩屆[美國參議員](../Page/美國參議員.md "wikilink")。他於[1860年美國總統選舉與](../Page/1860年美國總統選舉.md "wikilink")[亞伯拉罕·林肯競逐共和黨初選](../Page/亞伯拉罕·林肯.md "wikilink")，最終敗給林肯。林肯于1861年就任總統後，讓他出任國務卿。

1865年4月14日，[美國內戰結束後的僅五天](../Page/美國內戰.md "wikilink")，總統林肯遇刺身亡。行刺主謀[约翰·威尔克斯·布斯另與](../Page/约翰·威尔克斯·布斯.md "wikilink")合謀，讓他當天晚上到西華德的家中進行刺殺，西華德臉部被刀重創，但沒有危及生命。\[1\]林肯遇刺後，副總統[安德魯·詹森繼任總統](../Page/安德魯·詹森.md "wikilink")，西華德仍任國務卿，他保持對總統詹森的忠誠，維持共和黨在內閣的影響力。

## 外交

他在國務卿任內，從[沙俄手上買下了](../Page/沙俄.md "wikilink")[俄屬北美](../Page/俄屬北美.md "wikilink")，即今日的[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")。

## 資料來源

<div class="references-small">

<references>

\[2\]

</references>

</div>

[Category:阿拉斯加州歷史](../Category/阿拉斯加州歷史.md "wikilink")
[Category:美俄關係](../Category/美俄關係.md "wikilink")
[Category:美国国务卿](../Category/美国国务卿.md "wikilink")
[Category:美国共和党联邦参议员](../Category/美国共和党联邦参议员.md "wikilink")
[Category:纽约州州长](../Category/纽约州州长.md "wikilink")
[Category:美国民兵将领](../Category/美国民兵将领.md "wikilink")
[Category:纽约州律师](../Category/纽约州律师.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")
[Category:纽约州南北战争人物](../Category/纽约州南北战争人物.md "wikilink")
[Category:暗殺未遂倖存者](../Category/暗殺未遂倖存者.md "wikilink")
[Category:美国辉格党州长](../Category/美国辉格党州长.md "wikilink")

1.
2.  [美国历史上最黑暗时刻：失意演员刺杀林肯总统
    搜狐新聞](http://news.sohu.com/20070924/n252327811.shtml)