**民主勞動黨**（[韩语](../Page/韩语.md "wikilink")：민주노동당）是[韩国的一個已不存在的](../Page/韩国.md "wikilink")[左翼政黨](../Page/左翼.md "wikilink")。该党成立于2000年1月，2008年陷入分裂，党内的“人民民主派”退出另组[新进步党](../Page/新进步党.md "wikilink")，剩下的民族解放派在2011年11月并入[统合进步党](../Page/统合进步党.md "wikilink")。

## 歷史

民主勞動黨的設立是為了建立與全國民主勞動組合總連盟，全國敎職員勞動組合的政治連線，全國民主勞動組合總連盟在[韩国兩個總](../Page/韩国.md "wikilink")[工會中](../Page/工會.md "wikilink")（另一個是[韓國勞動組合總連盟](../Page/韓國勞動組合總連盟.md "wikilink"))是較為偏向[左翼且獨立的](../Page/左翼.md "wikilink")。

民主劳动党在[2004年韩国国会选举](../Page/2004年韩国国会选举.md "wikilink")（[대한민국 제17대
총선](../Page/:en:South_Korean_parliamentary_election,_2004.md "wikilink")）中，地區得票率為4.3%，政黨得票率為13%，获得10个席位，首次參選國會即成為當時國會的第三大黨\[1\]，該黨支持[盧武鉉領導的執政](../Page/盧武鉉.md "wikilink")[開放國民黨施政](../Page/開放國民黨.md "wikilink")。

而[2007年韓國總統選舉中](../Page/2007年韓國總統選舉.md "wikilink")，民主勞動黨推出的候選人[權永吉得票率](../Page/權永吉.md "wikilink")3.0%\[2\]。

該黨內部大約分成[民族解放派與](../Page/韓國民主勞動黨民族解放派.md "wikilink")[人民民主派兩派](../Page/韓國民主勞動黨人民民主派.md "wikilink")，前者被稱為親[朝鲜劳动党的派系](../Page/朝鲜劳动党.md "wikilink")，支持[朝鮮半島統一](../Page/朝鮮半島統一.md "wikilink")，曾經有傳言認為在[2002年大韓民國總統選舉時](../Page/2002年大韓民國總統選舉.md "wikilink")，部分「北派」支持[盧武鉉而非該黨提名的](../Page/盧武鉉.md "wikilink")[權永吉](../Page/權永吉.md "wikilink")\[3\]。

2006年10月，該黨前現職黨幹部（包括前任中央委員[李楨勳與現任副秘書長](../Page/李楨勳.md "wikilink")[崔基榮](../Page/崔基榮.md "wikilink")）涉嫌「386朝鲜間諜團」（一心會）而遭到[韓國國家情報院以間諜罪名逮捕](../Page/韓國國家情報院.md "wikilink")，一名民主勞動黨黨員被查獲企圖暗殺不同意見的政治人物\[4\]\[5\]\[6\]。2007年4月16日，[李楨勳](../Page/李楨勳.md "wikilink")（남양동）與[崔基榮](../Page/崔基榮.md "wikilink")（최기영）皆被[首爾地方法院判處有罪](../Page/首爾.md "wikilink")，分別為6年與4年[徒刑](../Page/徒刑.md "wikilink")\[7\]。

民主勞動黨由黨主席[文成賢帶領的訪問團於](../Page/文成賢.md "wikilink")2006年10月31日受[朝鮮勞動黨之邀前往](../Page/朝鮮勞動黨.md "wikilink")[朝鲜訪問](../Page/朝鲜.md "wikilink")。

## 參考文獻

<references />

## 延伸閱讀

  - 楊偉中，〈[韓國民主勞動黨的奮進與前途](https://web.archive.org/web/20070820094919/http://linkage.ngo.org.tw/international%20soliadarity/korea/korea040820.htm)〉
  - 汪亭友、董向荣，〈[韩国民主劳动党的纲领和政策](https://web.archive.org/web/20071212003143/http://myy.cass.cn/file/2007082728390.html)〉
  - 陶文昭，〈[韩国民主劳动党的兴起](https://web.archive.org/web/20071224025759/http://boxun.com/hero/2007/voacnn/4_1.shtml)〉

## 外部連結

  - [政黨官方網站韓文版](http://www.kdlp.org/)

  - [政黨官方網站英文版](http://inter.kdlp.org/)

[Category:韓國已解散政黨](../Category/韓國已解散政黨.md "wikilink")
[Category:2000年建立的政黨](../Category/2000年建立的政黨.md "wikilink")
[Category:社會主義政黨](../Category/社會主義政黨.md "wikilink")

1.

2.

3.

4.
5.

6.

7.