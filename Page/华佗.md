[HuaTuo.jpg](https://zh.wikipedia.org/wiki/File:HuaTuo.jpg "fig:HuaTuo.jpg")《三国演义》绣像中的華佗\]\]
[Hua_Tuo_Hefei.jpg](https://zh.wikipedia.org/wiki/File:Hua_Tuo_Hefei.jpg "fig:Hua_Tuo_Hefei.jpg")[合肥的](../Page/合肥.md "wikilink")[安徽中医大學的華佗塑像](../Page/安徽中医大學.md "wikilink")\]\]
**華佗**（），字**元化**，幼名**旉**，沛国谯县（今[安徽](../Page/安徽.md "wikilink")[亳州市](../Page/亳州市.md "wikilink")）人，[东汉末年的](../Page/东汉.md "wikilink")[方士](../Page/方士.md "wikilink")、[醫師](../Page/醫師.md "wikilink")，其事跡见于《[後漢書](../Page/後漢書.md "wikilink")·[方術列傳下](../Page/方術.md "wikilink")》、《[三國志·方技傳](../Page/三國志.md "wikilink")》及《[华佗别传](../Page/华佗别传.md "wikilink")》。華佗與[董奉和](../Page/董奉.md "wikilink")[張仲景被并稱為](../Page/張仲景.md "wikilink")「建安三神醫」。与[扁鹊](../Page/扁鹊.md "wikilink")、[张仲景及](../Page/张仲景.md "wikilink")[李时珍并称中国古代四大名医](../Page/李时珍.md "wikilink")。

## 生平

《[後漢書](../Page/後漢書.md "wikilink")》及《[三國志](../Page/三國志.md "wikilink")》二書都說華佗年近六十，但亦保持壯容，《後漢書》甚至說「時人以為仙」。

華佗早年[遊學](../Page/遊學.md "wikilink")[徐州](../Page/徐州.md "wikilink")，兼通數部[經書](../Page/六經.md "wikilink")。[沛國相](../Page/沛國.md "wikilink")[陳-{珪}-舉其為](../Page/陳珪_\(東漢\).md "wikilink")[孝廉后](../Page/孝廉.md "wikilink")[太尉](../Page/太尉.md "wikilink")[黃琬辟舉为太医不为所动](../Page/黃琬.md "wikilink")。于鄉村行醫因醫術精湛名氣漸大因而求醫之人甚众。

據《[三國志](../Page/三國志.md "wikilink")·魏書二十九·方技傳》，廣陵太守[陳登因喜食](../Page/陳登.md "wikilink")[魚膾](../Page/生魚片.md "wikilink")（生鱼片），胃中有大量[寄生虫而重病](../Page/寄生虫.md "wikilink")。虽经華佗医治痊癒，但華佗提醒他此病三年後會復發，需要有良醫在側。三年後，果然復發，此時華佗不在，陳登便因病死去。

[曹操頭風病嚴重](../Page/曹操.md "wikilink")，時常[頭痛欲裂](../Page/頭痛.md "wikilink")。知其醫術了得，特封其為侍醫。但當時「醫」為一學問，非為職業，華佗本為[士人](../Page/士大夫.md "wikilink")，所以他被曹操召到左右，甚為不快；加上離家太久思念親人，便說得到家書，方向曹操請假。回家後又不想回到曹操身邊，便稱妻子患病，過期不返。

曹操多次書信召華佗，又要求[郡](../Page/郡.md "wikilink")[縣](../Page/縣.md "wikilink")[長官將之遣回](../Page/官吏.md "wikilink")，但華佗認為自己的醫術能養活自己，所以不肯回去。曹操大怒，派人前去考察，發現華佗之妻原來是詐病，便將華佗[禁錮獄中](../Page/禁錮.md "wikilink")。[荀彧向曹操求情](../Page/荀彧.md "wikilink")，曹操不許。

華佗受到獄卒的厚待，於是取出耗費畢生心力的[醫學名著](../Page/醫學.md "wikilink")《[青囊書](../Page/青囊書.md "wikilink")》贈給獄卒，以報答獄卒的照護之恩。然而獄卒因懼得罪曹操，只好婉拒，華佗也不強求，黯然將它燒毀，最後被殺。日後曹操最為疼愛的兒子[曹沖病重時](../Page/曹沖.md "wikilink")，曹操曾後悔當初不應該[處死華佗](../Page/處死.md "wikilink")。

華佗有兩名徒弟[吳普與](../Page/吳普.md "wikilink")[樊阿頗得真傳](../Page/樊阿.md "wikilink")；樊阿更善於[針灸以華佗所教的](../Page/針灸.md "wikilink")「漆葉青黏散」保身年亦高達百多歲而頭髮不白，但已失傳。\[1\]

## 医术

[A_man_playing_go_(a_board_game)_Wellcome_L0033024.jpg](https://zh.wikipedia.org/wiki/File:A_man_playing_go_\(a_board_game\)_Wellcome_L0033024.jpg "fig:A_man_playing_go_(a_board_game)_Wellcome_L0033024.jpg")浮世繪「刮骨療毒」，左側為華佗\]\]
华佗一生行醫濟世，精通[內科](../Page/內科.md "wikilink")、[外科](../Page/外科.md "wikilink")、[婦科](../Page/婦科.md "wikilink")、[兒科](../Page/兒科.md "wikilink")、[針灸等](../Page/針灸.md "wikilink")。特點是用藥少，只用幾味藥而已；執藥隨手抓出，不用稱量。針灸也只是針一兩處。下針前對病人說：「當引某許，若至，語人」（針感會到某個部位，若你感覺到了就告訴我），病人說：「已到」，便拔針，不久病便會好。

如針藥都不能醫治，就給病人用酒服[麻沸散](../Page/麻沸散.md "wikilink")，飲後有如麻醉，然後施手術，再縫合傷口，擦下藥膏，四、五日後創愈，一月就已平復。但麻沸散與外科手法已经失传。華佗是醫史上公認第一位使用[麻醉藥來](../Page/麻醉藥.md "wikilink")[麻醉病人](../Page/麻醉.md "wikilink")，然後進行[外科手術的](../Page/外科手術.md "wikilink")[醫師](../Page/醫師.md "wikilink")，同時也是中國第一位將手術小刀組在使用之前用火來[殺菌](../Page/殺菌.md "wikilink")[消毒](../Page/消毒.md "wikilink")，平時不用時浸泡在[酒水裡的](../Page/酒水.md "wikilink")[醫生](../Page/醫生.md "wikilink")。

華佗也曉得養性之術，年紀雖大，但仍有壯容。他模仿[虎](../Page/虎.md "wikilink")、[鹿](../Page/鹿科.md "wikilink")、[熊](../Page/熊.md "wikilink")、[猿](../Page/猿.md "wikilink")、[鸟的动作](../Page/鸟.md "wikilink")，创造了[五禽戏](../Page/五禽戏.md "wikilink")。他认为“人体欲得劳动，……血脉不通，病不得生，譬如户枢，终不朽也”。他的學徒吳普一直學習，年至九十多歲，耳目仍然聰敏，牙齒完整。

## 病例

在《[三国志](../Page/三国志.md "wikilink")》中有十六则病例（包括《[後漢書](../Page/後漢書.md "wikilink")》中所載七則），《[华佗别传](../Page/华佗别传.md "wikilink")》中有五则，其他文献中五则，共二十六则病例。

華佗的行醫事蹟中，以關羽「刮骨療毒」最為膾炙人口。源於《[三國演義](../Page/三國演義.md "wikilink")》，描述[關羽跟](../Page/關羽.md "wikilink")[曹操麾下將領](../Page/曹操.md "wikilink")[曹仁交戰時被毒箭射中](../Page/曹仁.md "wikilink")、必須把入骨的劇[毒用刀刮除](../Page/毒.md "wikilink")；描寫刮骨過程中，關羽談笑自若地與[馬良](../Page/馬良.md "wikilink")[下棋](../Page/下棋.md "wikilink")。《[三國志](../Page/三國志.md "wikilink")》中確有關羽手臂中箭，刮骨療毒的記載\[2\]；但實際上，當時（219年）華佗早已被曹操所殺，操刀者是另外一名[軍醫](../Page/軍醫.md "wikilink")\[3\]。

## 評價

[Hua_Tuo_figurine.JPG](https://zh.wikipedia.org/wiki/File:Hua_Tuo_figurine.JPG "fig:Hua_Tuo_figurine.JPG")
《[三國志](../Page/三國志.md "wikilink")》評曰:「華佗之醫診，[杜夔之聲樂](../Page/杜夔.md "wikilink")，[朱建平之相術](../Page/朱建平.md "wikilink")，[周宣之相夢](../Page/周宣.md "wikilink")，[管輅之術筮](../Page/管輅.md "wikilink")，誠皆玄妙之殊巧，非常之絕技矣。昔史遷著[扁鵲](../Page/扁鵲.md "wikilink")、[倉公](../Page/倉公.md "wikilink")、[日者之傳](../Page/日者.md "wikilink")，所以廣異聞而表奇事也。故存錄-{云}-爾。」\[4\]

《[後漢書](../Page/後漢書.md "wikilink")》记载[荀彧曾说](../Page/荀彧.md "wikilink")：「佗方術實工，人命所懸，宜加全宥。」

近代人們多用神醫華佗稱呼他，又以「華佗再世」、「元化重生」稱譽有傑出醫術的[醫師](../Page/醫師.md "wikilink")。在[臺灣有許多](../Page/臺灣.md "wikilink")[廟宇奉祀華佗](../Page/廟宇.md "wikilink")，如知名的[艋舺龍山寺](../Page/艋舺龍山寺.md "wikilink")。

## 質疑

漢代之後的中醫著作家也很少提到這個人。[宋朝甚至有人質疑](../Page/宋朝.md "wikilink")《神農本草經》為華佗偽造\[5\]。

[陳寅恪認為](../Page/陳寅恪.md "wikilink")，華佗本身就是個[神話故事](../Page/神話.md "wikilink")，華佗二字的[上古音](../Page/上古漢語.md "wikilink")\[\]與印度藥神「[阿伽陀](../Page/阿伽陀.md "wikilink")」音近。故「當時民間比附印度神話故事，因稱為華佗，實以藥神目之。」而且他的病例原型來自於[印度](../Page/印度.md "wikilink")[佛教傳說](../Page/佛教.md "wikilink")，「其有神話色彩，似無可疑。」\[6\]認為這個故事與「[曹沖稱象](../Page/曹沖#質疑.md "wikilink")」一樣，都是印度的舶來品。華佗這個人可能真有其人，但他的醫學傳奇是虛構的。\[7\]

## 後世紀念

### 典故

  - 「華佗再世」：華佗以醫術高超聞名，後世讚人醫術高明，有如華佗再度來到人世。也作「華佗再生」。
  - 「元化重生」：同上。

### 安奉

  - 墓塜

世傳華佗墓於[安徽](../Page/安徽.md "wikilink")[亳州](../Page/亳州.md "wikilink")、[江蘇](../Page/江蘇.md "wikilink")[徐州和](../Page/徐州.md "wikilink")[河南](../Page/河南.md "wikilink")[許昌各有一座](../Page/許昌.md "wikilink")\[8\]，其中有[衣冠塜者](../Page/衣冠塜.md "wikilink")。[亳州為華佗故鄉](../Page/亳州.md "wikilink")；[徐州為華佗本籍](../Page/徐州.md "wikilink")(原彭城)及遊學、行醫之地；[許昌為華佗喪生之地](../Page/許昌.md "wikilink")，許昌之墓於1993年經[許昌市人民政府列為市級文物保護單位](../Page/許昌市.md "wikilink")。

  - 廟祠

[徐州華佗墓曾於](../Page/徐州.md "wikilink")[明](../Page/明朝.md "wikilink")[永樂年間重修](../Page/永樂.md "wikilink")，同時於墓北建造「華佗庵」供奉華佗神像。信徒建有華佗廟，或配祀華佗像，[道教尊為](../Page/道教.md "wikilink")「青囊濟世華真人」\[9\]、「神功妙手華真人」\[10\]，信眾稱「華佗仙師」、「華佗仙翁」、「華佗神醫」等。

  - 紀念館

[安徽](../Page/安徽.md "wikilink")[亳州永安街中段路北建有華祖庵](../Page/亳州.md "wikilink")，1962年增設「華佗紀念館」，由[郭沫若題寫館名](../Page/郭沫若.md "wikilink")。1980年經維修，次年(1981)正式列為省級重點文物保護單位。該紀念館介紹華佗一生事跡，並特別展示了華佗施行外科手術之工具及過程資料。

## 藝術形象

### 藝文作品

  - [三國志系列](../Page/三國志系列.md "wikilink")（電玩）
  - 少年华佗（動畫）
  - 1983年电影《[华佗与曹操](../Page/华佗与曹操.md "wikilink")》：[郑乾龙饰演华佗](../Page/郑乾龙.md "wikilink")
  - 1994年电视剧《[三国演义](../Page/三国演义_\(电视剧\).md "wikilink")》：[王忠信饰演华佗](../Page/王忠信.md "wikilink")
  - 2000年电视剧《[医神华佗](../Page/医神华佗.md "wikilink")》：[林文龍饰演华佗](../Page/林文龍.md "wikilink")
  - 2004年电视剧《[神医华佗](../Page/神医华佗.md "wikilink")》：[庹宗华饰演华佗](../Page/庹宗华.md "wikilink")
  - 2009年电视剧《[终极三国](../Page/终极三国.md "wikilink")》: 曾子余饰演华佗
  - 2010年电视剧《[三国](../Page/三国_\(电视剧\).md "wikilink")》：[蔡军饰演华佗](../Page/蔡军.md "wikilink")
  - 2015年电视剧《[半為蒼生半美人](../Page/半為蒼生半美人.md "wikilink")》：[何晟铭饰演华佗](../Page/何晟铭.md "wikilink")
  - 2017年电视剧《[军师联盟](../Page/军师联盟.md "wikilink")》：[许还山饰演华佗](../Page/许还山.md "wikilink")
  - 2017年網路剧《[终极三国2017](../Page/終極三國_\(2017年\).md "wikilink")》:
    [夏志远饰演华佗](../Page/夏志远.md "wikilink")

## 参考文献

## 外部連結

  - [古代名醫 華佗傳](http://bbs.xdxdxd.com/viewthread.php?tid=72362)
  - [中國古代名醫之華佗](http://news.epochtimes.com/b5/4/3/25/n491977.htm)

[H华](../Category/漢朝醫學家.md "wikilink")
[H华](../Category/東汉人.md "wikilink")
[T佗](../Category/华姓.md "wikilink")
[H华](../Category/亳州人.md "wikilink")
[H](../Category/人物神.md "wikilink")
[H](../Category/中國民間信仰.md "wikilink")
[H](../Category/醫藥之神.md "wikilink")

1.  《[後漢書·卷八十二下·方技傳](../Page/:s:後漢書/卷82下.md "wikilink")》
2.  《[三國志·蜀書·關羽傳](../Page/:s:三國志/卷36#關羽.md "wikilink")》：羽嘗為流矢所中，貫其左臂，後創雖愈，每至陰雨，骨常疼痛，醫曰：「矢鏃有毒，毒入於骨，當破臂作創，刮骨去毒，然後此患乃除耳。」羽便伸臂令醫劈之。時羽適請諸將飲食相對，臂血流離，盈於盤器，而羽割炙引酒，言笑自若。
3.
4.  《[三国志](../Page/:s:三国志.md "wikilink")·魏书·方技传》
5.  [《本草綱目》唱得活像現代中醫](http://www.xys.org/xys/ebooks/others/science/dajia10/zhongyi2240.txt)[張功耀](../Page/張功耀.md "wikilink")，[新語絲](../Page/新語絲.md "wikilink")，2009-3-2。
6.  《[清華學報](../Page/清華學報.md "wikilink")》六卷一期所刊「三國志曹沖華佗傳與佛教故事」，1930年
7.  [再提爭論：華佗到底是波斯人還是印度人(圖)](http://tech.sina.com.cn/d/2004-12-02/0934468776.shtml)
8.  [華夏經緯網-華佗在許昌](http://big5.huaxia.com/ytsc/zywh/dlhn/2009/07/1485599.html)
9.  《元始天尊說北方北帝伏魔法懺》
10. 上清天醫院十三科天醫真人