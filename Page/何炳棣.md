**何炳棣**（），[浙江](../Page/浙江.md "wikilink")[金華人](../Page/金華縣.md "wikilink")\[1\]，出生于天津，旅美華裔史學家。

## 生平

少年時曾念過[南开中学](../Page/南开中学.md "wikilink")，后求学于[山东大学](../Page/山东大学.md "wikilink")，一年后复考入[清华大学](../Page/清华大学.md "wikilink")，后因抗战爆发而在上海[光华大学](../Page/光华大学.md "wikilink")（今[华东师范大学](../Page/华东师范大学.md "wikilink")）借读而得以毕业。1938年畢業於[國立清華大學歷史系](../Page/國立清華大學.md "wikilink")。對日[抗戰時執教于](../Page/抗戰.md "wikilink")[西南聯大](../Page/西南聯大.md "wikilink")，通過[教育部第六屆公費留學西洋史學門考試](../Page/教育部.md "wikilink")，赴美留學，學習18、19世紀英國史。於美國[哥倫比亞大學取得](../Page/哥倫比亞大學.md "wikilink")[歷史學博士](../Page/歷史學.md "wikilink")，曾先後任教於[加拿大](../Page/加拿大.md "wikilink")[英屬哥倫比亞大學](../Page/英屬哥倫比亞大學.md "wikilink")、美國[芝加哥大學](../Page/芝加哥大學.md "wikilink")。

何炳棣就讀清華大學化學系一年級時，修讀[劉崇鋐教授西洋通史課程](../Page/劉崇鋐.md "wikilink")，向劉請教讀西洋史相關問題。劉崇鋐勸其三思，復以學成後如何做研究，和西方人競爭等等問題討論。轉入歷史系後，在系主任[蔣廷黻建議下](../Page/蔣廷黻.md "wikilink")，先以近代外交史作為修課主要方向，同時自修西洋史知識。在清華大學兼容並包，以社會科學方法、人文通識課程設計基礎下，打好準備留學考試根基。1944年考取第六屆庚款留美公費，並於次年赴美，入哥倫比亞大學，專攻英國史及及西歐史。1952年以《英國土地問題與土地政策1870-1914》獲得博士學位後，正式轉入中國史研究。何氏自言，西洋史的訓練，讓他一看到問題，就進行中、西比較，應用到不同的工具，就得出與前人、時人不同的論點。

1966年，何炳棣當選[中央研究院第六屆人文組院士](../Page/中央研究院.md "wikilink")，時年49歲。其在美國人文社科學界地位亦甚高，曾在1975年出任美國[亞洲研究學會會長](../Page/亞洲研究學會.md "wikilink")，為首任亞裔會長，自被國際視為大師級學者。（專攻印度史的[西雅圖華盛頓大學講座教授兼該校賈克遜國際關係學院院長](../Page/西雅圖華盛頓大學.md "wikilink")[楊雅南](../Page/楊雅南.md "wikilink")（Anand
A.Yang），於2006年四月初出任亞洲研究學會會長，他雖是學會六十五年會史上的第二位華裔會長，但之前已有過一位印度裔學者和兩位日裔學者做過會長，楊雅南是第五位亞裔會長。）

何炳棣認為，治學不可被似有創意而數據不足的[社會科學](../Page/社會科學.md "wikilink")[理論所迷惑](../Page/理論.md "wikilink")，必須從大量多種[史料的考訂](../Page/史料.md "wikilink")、詮釋、控制入手，並指出堅實的史料根據乃為史家養命之源，撰寫The
Ladder of Success in Imperial
China時，遍檢北美各大圖書館所藏近四千種方志。另外何氏亦不贊同青年史家在入門階段即進入思想史的工作，何氏指出：“如果自青年即專攻[思想史](../Page/思想史.md "wikilink")，一生對史料的類型及範疇可能都缺乏至少必要的了解，以致長期的研究寫作都空懸於[政治](../Page/政治.md "wikilink")、[社會](../Page/社會.md "wikilink")、[經濟制度之上而不能著地](../Page/經濟.md "wikilink")。”

何炳棣雖然在美國，但在[中國大陸](../Page/中國大陸.md "wikilink")、[台灣學界亦享有大名](../Page/台灣.md "wikilink")。但為人孤芳自賞，有稜有角，給人一種距離感，甚至何氏亦自承「終身脾氣急躁」、「往往與中外學人不能和諧共處」，但亦反映出何氏有足夠的自信心與安全感。

何先生雖然個性強，但其實待人處世有其真性情的一面，近史所的諸多先生們多以「tiger」暱稱先生。

2001年何炳棣提出「[老子辯證思維源於](../Page/老子.md "wikilink")[孫子兵法](../Page/孫子兵法.md "wikilink")」的說法。

何炳棣近年寫有《讀史閱世六十年》的回憶錄，裡面不少附件記錄了他的一生，同时也是[中國現代史的珍貴材料](../Page/中國現代史.md "wikilink")。

2012年上半年，何先生因夜間起床時，飲水稍有不慎，誤入氣管，身體不適而送醫。 然在醫院接受治療時，又受感染，而病況漸重。

2012年美国西部时间6月7日，何炳棣病逝于美国加州，享年95岁。

追思會上，中研院諸位先生的回憶言談，多半懷念先生為人處世的點點滴滴，先生為學不好使用助理，喜歡自己動手找材料，勤於研究的精神，讓人敬佩。

## 家族

堂兄[何炳松](../Page/何炳松.md "wikilink")，長期任[商務印書館編譯所所長](../Page/商務印書館.md "wikilink")、協理和[暨南大學校長](../Page/暨南大學.md "wikilink")。

## 著作

  - 《明初以降人口及其相關問題(1368--1953)》,葛劍雄 譯, 北京: 三聯書店, 2000。上海古籍出版社於1990曾出版:
    1368-1953中國人口研究 (原名: Studies on the Population of China, 1368- 1953)
  - 《明清社會史論》遠流遲遲不出，而[徐泓教授已陸續以譯註形式刊載在](../Page/徐泓_\(歷史學家\).md "wikilink")《東吳歷史學報》中。本書已於2013年12月由臺北:
    聯經出版, 徐泓 譯注。(原名: The Ladder of Success in Imperial China: Aspects of
    Social Mobility, 1368- 1911, 美國哥倫比亞大學, 1962 初版, 1967 修訂二版)
  - 《東方的搖籃:紀元5000至10000年華夏技術及理念本土起源的探索》無中文版。(原名: Cradle of the East: An
    Enquiry into the Indigenous Origins of Techniques and Ideas of
    Neolithic and Early Historic China, 5000-10000 B.C., Chinese
    University of Hong Kong, 1975 )
  - 《中國會館史論》1966學生書局
  - 《中國歷代土地數字考實》1995聯經出版。北京 : 中國社會科學出版社, 1988 出版 "中國古今土地數字的考譯和評價 ",
    為同書異名。
  - 《黃土與中國農業的起源》1969, 2001香港中文大學
  - 《有關《孫子》《老子》的三篇考證》2002.8中研院近史所
  - 《讀史閱世六十年》2004.5允晨文化；香港商務印書館, 2004;2005廣西師範大學, 2012中華書局

近作

  - 《從莊子天下篇首解析先秦思想中的基本關懷》2007.3台大文史學報
  - 《國史上的大事因緣解謎-從重建秦墨史實入手》2008.6.17中研院講演題目

## 评价

[陳來](../Page/陳來.md "wikilink")：我對這位老先生也抱“敬而遠之”的態度，因為90年代初期他在《二十一世紀》上批評[杜維明先生](../Page/杜維明.md "wikilink")“克己復禮”詮釋的文章，作哲學史、思想史的學者都對他的批評不以為然。我跟杜先生思想較為一致，所以也就不敢跟他親近，主要是怕他對我們研究思想史的學者（特別是研究儒學的學者）懷有偏見。\[2\]

[葛劍雄](../Page/葛劍雄.md "wikilink")：何炳棣先生到了晚年更加自負，我給他翻譯的書，把翻好的稿子都給他看了，他要我根據他后來的觀點修改裡面的文字，我不接受，我說：何先生，你可以修改，我是翻譯的人，我若給你改了，我要怎麼跟你的讀者交代？我堅持用加注的方法，不同意改動，他不高興。后來他對吳承明先生說我的翻譯很糟糕，我知道后問他：“我當時不都給你看過的嗎？”“你的稿子是橫寫的簡體字，我看不慣，其實我沒看。”沒辦法。\[3\]

[李敖赞赏何炳棣](../Page/李敖.md "wikilink")\[4\]：

## 参考文献

## 参考书目

  - 《李敖大全集》，李敖，中国友谊出版公司，ISBN 9787505727526。

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中華民國歷史學家](../Category/中華民國歷史學家.md "wikilink")
[Category:芝加哥大學教師](../Category/芝加哥大學教師.md "wikilink")
[Category:不列顛哥倫比亞大學教師](../Category/不列顛哥倫比亞大學教師.md "wikilink")
[Category:國立西南聯合大學教授](../Category/國立西南聯合大學教授.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:南開中學校友](../Category/南開中學校友.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:亚洲研究协会会长](../Category/亚洲研究协会会长.md "wikilink")
[Category:庚子赔款留学生](../Category/庚子赔款留学生.md "wikilink")
[Category:金华人](../Category/金华人.md "wikilink")
[Bing炳棣](../Category/何姓.md "wikilink")

1.  [明清社會史論](http://mingching.sinica.edu.tw/Publish_book_detail/86)，中央研究院
    明清研究推動委員會
2.  陳來，[小憶何炳棣先生](http://blog.sina.com.cn/s/blog_4a03de990101boz1.html#rd)，2012-11-20
3.  葛劍雄，2015年08月18日[葛劍雄：做歷史研究，特別要關注現實](http://epaper.ynet.com/html/2015-08/18/content_149515.htm#rd)
    ，北京青年報，B07
4.  [李敖大全集](../Page/李敖大全集.md "wikilink") 32 李敖書啟集 p.149-150