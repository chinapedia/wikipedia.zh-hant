**[羅馬尼亞國旗](../Page/羅馬尼亞.md "wikilink")**是一面藍、黃、紅的[三色旗](../Page/三色旗.md "wikilink")（象徵[川西凡尼亞](../Page/川西凡尼亞.md "wikilink")、[瓦拉幾亞和](../Page/瓦拉幾亞.md "wikilink")[摩爾多瓦](../Page/摩爾多瓦.md "wikilink")），比例為2:3。

根據第75號法律，現行的國旗於1994年7月16日啟用\[1\]。

## 设计

[Romaniaflag1.jpg](https://zh.wikipedia.org/wiki/File:Romaniaflag1.jpg "fig:Romaniaflag1.jpg")
[Flag_of_Romania_(construction).png](https://zh.wikipedia.org/wiki/File:Flag_of_Romania_\(construction\).png "fig:Flag_of_Romania_(construction).png")

| Scheme                             | 蓝色         | 黄色         | 红色        |
| ---------------------------------- | ---------- | ---------- | --------- |
| [彩通](../Page/彩通.md "wikilink")     | 280c       | 116c       | 186c      |
| [CMYK](../Page/CMYK.md "wikilink") | 99-86-1-00 | 2-18-95-0  | 4-99-94-0 |
| [RGB](../Page/RGB.md "wikilink")   | 0-43-127   | 252-209-22 | 206-17-38 |
| [网页颜色](../Page/网页颜色.md "wikilink") | \#002B7F   | \#FCD116   | \#CE1126  |

## 历史沿革

1881年，罗马尼亚大公[卡罗尔一世建立了](../Page/卡罗尔一世.md "wikilink")[罗马尼亚王国](../Page/罗马尼亚王国.md "wikilink")，当时使用的国旗就是和现在样式相同的蓝黄红[三色旗](../Page/三色旗.md "wikilink")。

1945年，[第二次世界大战之后](../Page/第二次世界大战.md "wikilink")，[苏联陆续扶持建立](../Page/苏联.md "wikilink")[社会主义政权](../Page/社会主义.md "wikilink")，[罗马尼亚也包括其中](../Page/罗马尼亚.md "wikilink")，[格奥尔基·乔治乌-德治成立](../Page/格奥尔基·乔治乌-德治.md "wikilink")[罗马尼亚人民共和国](../Page/罗马尼亚人民共和国.md "wikilink")。这期间在三色旗上添加了国徽。该时期总共出现了四种样式的罗马尼亚国旗。1952年以后的国徽上添加了[五星](../Page/红色五角星.md "wikilink")，国旗也相应改变。1965年，[格奥尔基·乔治乌-德治逝世后](../Page/格奥尔基·乔治乌-德治.md "wikilink")，[齐奥塞斯库上台](../Page/齐奥塞斯库.md "wikilink")，把国号改为“**[罗马尼亚社会主义共和国](../Page/罗马尼亚社会主义共和国.md "wikilink")**”，并修改了国徽图案，国旗再次变动，这是罗马尼亚共产政权最后一版国旗。

1989年12月21日，罗马尼亚发生[大规模反共示威](../Page/1989年罗马尼亚革命.md "wikilink")，事件最后演变为流血事件，导致[齐奥塞斯库政权的倒台](../Page/齐奥塞斯库.md "wikilink")，[齐奥塞斯库夫妇双双处死](../Page/齐奥塞斯库.md "wikilink")。示威者仿照[匈牙利十月事件](../Page/匈牙利十月事件.md "wikilink")，将国旗上的旧国徽剪掉，剩下的部分作为反抗旗帜。

1989年12月27日，罗马尼亚在共产主义倒台之后，确认去掉旧国徽的蓝、黄、红垂直三色旗为现在的罗马尼亚国旗。

## 历代国旗列表

<table>
<thead>
<tr class="header">
<th><p>國旗</p></th>
<th><p>國家</p></th>
<th><p>使用</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Principalities_of_Wallachia_and_Moldavia_(1859_-_1862).svg" title="fig:Flag_of_the_United_Principalities_of_Wallachia_and_Moldavia_(1859_-_1862).svg">Flag_of_the_United_Principalities_of_Wallachia_and_Moldavia_(1859_-_1862).svg</a></p></td>
<td><p><a href="../Page/罗马尼亚联合公国.md" title="wikilink">罗马尼亚联合公国</a></p></td>
<td><p>(1859-1862)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Principalities_of_Romania_(1862_-_1866).svg" title="fig:Flag_of_the_United_Principalities_of_Romania_(1862_-_1866).svg">Flag_of_the_United_Principalities_of_Romania_(1862_-_1866).svg</a></p></td>
<td><p><a href="../Page/罗马尼亚联合公国.md" title="wikilink">罗马尼亚联合公国</a></p></td>
<td><p>(1862-1866)</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania.svg" title="fig:Flag_of_Romania.svg">Flag_of_Romania.svg</a></p></td>
<td><p><a href="../Page/罗马尼亚联合公国.md" title="wikilink">罗马尼亚联合公国</a></p></td>
<td><p>(1867-1881)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Romanian_Army_Flag_-_1867_official_model.svg" title="fig:Romanian_Army_Flag_-_1867_official_model.svg">Romanian_Army_Flag_-_1867_official_model.svg</a></p></td>
<td><p><a href="../Page/罗马尼亚联合公国.md" title="wikilink">罗马尼亚联合公国</a></p></td>
<td><p>(1867-1872)</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Romanian_Army_Flag_-_1872_official_model.svg" title="fig:Romanian_Army_Flag_-_1872_official_model.svg">Romanian_Army_Flag_-_1872_official_model.svg</a></p></td>
<td><p><a href="../Page/罗马尼亚联合公国.md" title="wikilink">罗马尼亚联合公国</a></p></td>
<td><p>(1872-1921)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania.svg" title="fig:Flag_of_Romania.svg">Flag_of_Romania.svg</a></p></td>
<td><p><a href="../Page/罗马尼亚王国.md" title="wikilink">罗马尼亚王国</a></p></td>
<td><p>(1881-1947)</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Romanian_Army_Flag_-_1872_official_model.svg" title="fig:Romanian_Army_Flag_-_1872_official_model.svg">Romanian_Army_Flag_-_1872_official_model.svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞王國.md" title="wikilink">羅馬尼亞王國</a></p></td>
<td><p>(1872-1921)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Romanian_Army_Flag_-_1921_official_model.svg" title="fig:Romanian_Army_Flag_-_1921_official_model.svg">Romanian_Army_Flag_-_1921_official_model.svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞王國.md" title="wikilink">羅馬尼亞王國</a></p></td>
<td><p>(1921-1947)</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania_(January-March_1948).svg" title="fig:Flag_of_Romania_(January-March_1948).svg">Flag_of_Romania_(January-March_1948).svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞人民共和國.md" title="wikilink">羅馬尼亞人民共和國</a></p></td>
<td><p>(1948)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania_(1948-1952).svg" title="fig:Flag_of_Romania_(1948-1952).svg">Flag_of_Romania_(1948-1952).svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞人民共和國.md" title="wikilink">羅馬尼亞人民共和國</a></p></td>
<td><p>(1948-1952)</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania_(1952-1965).svg" title="fig:Flag_of_Romania_(1952-1965).svg">Flag_of_Romania_(1952-1965).svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞人民共和國.md" title="wikilink">羅馬尼亞人民共和國</a></p></td>
<td><p>(1952-1965)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania_(1965-1989).svg" title="fig:Flag_of_Romania_(1965-1989).svg">Flag_of_Romania_(1965-1989).svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞社會主義共和國.md" title="wikilink">羅馬尼亞社會主義共和國</a></p></td>
<td><p>（1965-1989）</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Romania_flag_1989_revolution.svg" title="fig:Romania_flag_1989_revolution.svg">Romania_flag_1989_revolution.svg</a></p></td>
<td><p><a href="../Page/羅馬尼亞.md" title="wikilink">羅馬尼亞</a></p></td>
<td><p><a href="../Page/1989年羅馬尼亞革命.md" title="wikilink">羅馬尼亞革命旗</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Romania.svg" title="fig:Flag_of_Romania.svg">Flag_of_Romania.svg</a></p></td>
<td><p><a href="../Page/罗马尼亚.md" title="wikilink">罗马尼亚</a></p></td>
<td><p>羅馬尼亞國旗(1989至今)</p></td>
</tr>
</tbody>
</table>

## 其他旗帜

ROU Bucharest Flag.svg|[布加勒斯特市旗](../Page/布加勒斯特.md "wikilink")

## 类似旗帜

Flag of Romania.svg|[罗马尼亚国旗](../Page/罗马尼亚国旗.md "wikilink") Flag of
Chad.svg|[乍得国旗](../Page/乍得国旗.md "wikilink") Flag of
Andorra.svg|[安道尔国旗](../Page/安道尔国旗.md "wikilink") Flag of
Moldova.svg|[摩尔多瓦国旗](../Page/摩尔多瓦国旗.md "wikilink")
Sanshokuki.svg|創價學會三色旗

## 注释与参考文献

{{-}}

[L](../Category/各国国旗.md "wikilink")
[Category:罗马尼亚国家象征](../Category/罗马尼亚国家象征.md "wikilink")
[Category:罗马尼亚旗帜](../Category/罗马尼亚旗帜.md "wikilink")
[Category:垂直三色旗](../Category/垂直三色旗.md "wikilink")

1.  [Law
    no. 75](http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=14530)
    (羅馬尼亞語) ([英文版](http://www.crwflags.com/fotw/flags/ro-law.html))