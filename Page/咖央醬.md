**咖央醬**（；；又名**咖椰**、**加椰**、**咖吔**、**加椰醬**、**咖椰醬**），是一種[東南亞常见的](../Page/東南亞.md "wikilink")[甜点材料](../Page/甜点.md "wikilink")，用上[椰漿](../Page/椰漿.md "wikilink")、[鴨蛋或](../Page/鴨蛋.md "wikilink")[雞蛋](../Page/雞蛋.md "wikilink")、[砂糖和](../Page/砂糖.md "wikilink")[牛油等材料隔水加熱撈勻做成](../Page/牛油.md "wikilink")。咖央醬常用来涂面包、制作蛋糕、制作夹心面包等的馅料，如：咖央[西多士或咖央](../Page/西多士.md "wikilink")[多士](../Page/多士.md "wikilink")\[1\]。咖央醬在[東南亞是廣受歡迎的](../Page/東南亞.md "wikilink")，主要表現在[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、以及[印尼](../Page/印尼.md "wikilink")[巨港](../Page/巨港.md "wikilink")\[2\]。

## 制作方法

材料：椰浆1升、斑斓叶4片、椰糖1kg、蛋黄10颗

做法：

1.  斑斓叶洗净，备用。
2.  把鸡蛋和幼糖一起搅拌至糖溶化，加入椰奶搅拌均匀，过滤。
3.  隔水煮，一直搅拌煮至浓稠即可，不停搅伴它，才会煮得顺滑。
4.  收入冰箱里，随时都可以从冰箱里拿出来沾上吃用。

## 参考

[Yosrisekaya1.jpg](https://zh.wikipedia.org/wiki/File:Yosrisekaya1.jpg "fig:Yosrisekaya1.jpg")

[Category:新加坡食品](../Category/新加坡食品.md "wikilink")
[Category:東南亞食品](../Category/東南亞食品.md "wikilink")
[Category:調味料](../Category/調味料.md "wikilink")
[Category:马来西亚食品](../Category/马来西亚食品.md "wikilink")

1.
2.