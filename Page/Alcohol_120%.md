**Alcohol 120%**是[Alcohol
Soft的軟件產品之一](../Page/Alcohol_Soft.md "wikilink")，集合光碟[燒錄及模擬](../Page/燒錄.md "wikilink")[映像檔功能](../Page/映像檔.md "wikilink")，可燒錄[CD](../Page/CD.md "wikilink")/[DVD及虛擬多台光碟機](../Page/DVD.md "wikilink")，支援ISO等多種光碟映像格式。

## 簡介

這套軟件可用作光碟資料備份，透過複製或製作映像檔方式進行。它可同一時間燒錄多片光碟，能突破一些防拷保護技術，支援MDS、CCD、BIN/CUE、ISO、CDI、BWT、BWI、BWS、BWA、等映像檔，同時也能輕易複製[PlayStation及](../Page/PlayStation_\(遊戲機\).md "wikilink")[PS2遊戲光碟](../Page/PlayStation_2.md "wikilink")，但某些光碟的防拷技術則需特定有支援該技術的燒錄器，方能燒錄。由於受法律限制，Alcohol不支援備份受版權保護的DVD電影光碟。

Alcohol也不支援製作自訂映像檔功能，製作映像檔的來源需要光碟進行。此外由於其功能可用作侵權，一些軟件商已把Alcohol列入黑名單，使用此等軟體需先把Alcohol移除，但一些軟件已給第三者破解，例如在[Windows可透過修改](../Page/Windows.md "wikilink")[登錄檔進行](../Page/註冊表.md "wikilink")。

另有一套名為「Alcohol
52%」的軟件，僅提供虛擬光碟功能，可把光碟的映像檔複製備份，及支援最多31個虛擬光碟機。受法律所限，該軟件不支援使用[內容擾亂系統](../Page/內容擾亂系統.md "wikilink")（CSS,
Content Scramble System）保護的DVD光碟。免費版Alcohol
52%內含[廣告軟體](../Page/廣告軟體.md "wikilink") Smart File
Advisor。

## 免費版

Alcohol 120％ 免費版\[1\]僅可使用最多 2個虛擬光碟，且同時只能寫入 1個實體光碟，且沒有複製保護模擬選項。

## 参考资料

## 外部連結

  - [Alcohol Soft官網](http://www.alcohol-soft.com)

  - [Alcohol 120％
    免費版](http://www.filefacts.com/alcohol-120-free-edition-info)

  - [Alcohol 120％ 便攜版](http://www.filefacts.com/alcohol-portable-info)

[Category:光盤創作軟件](../Category/光盤創作軟件.md "wikilink")
[Category:虛擬光碟軟體](../Category/虛擬光碟軟體.md "wikilink")

1.  <http://www.filefacts.com/alcohol-120-free-edition-info>