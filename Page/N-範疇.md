在[數學中](../Page/數學.md "wikilink")，**n-範疇**是[範疇在高階情形的推廣](../Page/範疇論.md "wikilink")。（小）n-範疇組成的範疇
**n-Cat** 以下述方式遞迴定義：

  - **0-Cat**是[集合範疇](../Page/集合範疇.md "wikilink") \(\mathbf{Set}\)
  - **(n+1)-範疇**是全體在 **n-Cat**
    上[濃化的範疇組合的範疇](../Page/濃化範疇.md "wikilink")，其[張量範疇之結構由合成導出](../Page/張量範疇.md "wikilink")。

特例是[小範疇及其間](../Page/小範疇.md "wikilink")[函子組成的範疇](../Page/函子.md "wikilink")
**1-Cat**。

## 參見

  - [2-範疇](../Page/2-範疇.md "wikilink")
  - [弱 n-範疇](../Page/弱_n-範疇.md "wikilink")

## 文獻

  -
  -
[Category:高階範疇論](../Category/高階範疇論.md "wikilink")