**Avex trax**
（）是[日本](../Page/日本.md "wikilink")[愛貝克思集團屬下的一間](../Page/愛貝克思集團.md "wikilink")[唱片公司](../Page/唱片公司.md "wikilink")，成立於1990年，是愛貝克思的首間唱片公司。名字的trax取自tracks（[音軌](../Page/音軌.md "wikilink")）。\[1\]

## 歷史

在日本以外的國家協助發行[錄音室專輯後兩年](../Page/錄音室專輯.md "wikilink")，他與愛貝克思創辦人等人決定成立新的唱片廠牌與[日本古倫美亞](../Page/日本古倫美亞.md "wikilink")、[日本索尼音樂娛樂和](../Page/日本索尼音樂娛樂.md "wikilink")[環球唱片等經驗較多的公司競爭](../Page/環球唱片.md "wikilink")，Avex
Trax因此誕生。

Avex Trax的首位簽約藝人是樂團[TRF](../Page/TRF.md "wikilink")，由於他們發展成功，使Avex
Trax搖身一變成為藝人的收容所，其中包括一些與舊東家結束合約的藝人（例：[安室奈美惠](../Page/安室奈美惠.md "wikilink")）、希望更換唱片公司的藝人（例：[Gackt](../Page/Gackt.md "wikilink")）、以及被舊東家棄用的藝人（例：[濱崎步](../Page/濱崎步.md "wikilink")）。

## 概略

現在為愛貝克思目前藝人人數最多的音樂廠牌。

[規格編號](../Page/規格編號.md "wikilink")[CD為](../Page/CD.md "wikilink")「AVCD」開頭、[DVD為](../Page/DVD.md "wikilink")「AVBD」開頭、[Blu-ray
Disc為](../Page/Blu-ray_Disc.md "wikilink")「AVXD」開頭，以此作為編號標準。

## 藝人

### 現在所屬主要藝人

#### 以五十音排序

  - [安室奈美惠](../Page/安室奈美惠.md "wikilink")（[日本EMI音樂移籍至此](../Page/日本EMI音樂.md "wikilink")）

  - [大島麻衣](../Page/大島麻衣.md "wikilink")

  - [大塚愛](../Page/大塚愛.md "wikilink")

  - [乙三](../Page/乙三.md "wikilink")

  - [上木彩矢](../Page/上木彩矢.md "wikilink")（[GIZA
    studio移籍至此](../Page/GIZA_studio.md "wikilink")）

  - [氣志團](../Page/氣志團.md "wikilink")（[日本EMI音樂移籍至此](../Page/日本EMI音樂.md "wikilink")）

  - [北乃紀伊](../Page/北乃紀伊.md "wikilink")

  - [金亨俊](../Page/金亨俊.md "wikilink")（[SS501](../Page/SS501.md "wikilink")）

  - [黒夢](../Page/黒夢.md "wikilink")

  - [後藤真希](../Page/後藤真希.md "wikilink")（PICCOLO TOWN→[rhythm
    zone移籍至此](../Page/rhythm_zone.md "wikilink")）

  - [紗羅マリー](../Page/紗羅マリー.md "wikilink")

  - [島谷瞳](../Page/島谷瞳.md "wikilink")

  - [シン・スンフン](../Page/シン・スンフン.md "wikilink")

  - [鈴木亞美](../Page/鈴木亞美.md "wikilink")（[日本索尼音樂娛樂移籍至此](../Page/日本索尼音樂娛樂.md "wikilink")）

  - [スティーヴィー・ホアン](../Page/スティーヴィー・ホアン.md "wikilink")

  - [ストロボ](../Page/ストロボ_\(バンド\).md "wikilink")

  -
  - [瀧與翼](../Page/瀧與翼.md "wikilink")

      - [瀧澤秀明](../Page/瀧澤秀明.md "wikilink")
      - [今井翼](../Page/今井翼.md "wikilink")

  - [倖田來未](../Page/倖田來未.md "wikilink")

  - [東京女子流](../Page/東京女子流.md "wikilink")

  - [東方神起](../Page/東方神起.md "wikilink")（rhythm zone移籍至此）\[2\]

  -
  - [所喬治](../Page/所喬治.md "wikilink")（佳音唱片（現：[波麗佳音](../Page/波麗佳音.md "wikilink")）→Epic
    Records（現：[Epic Records Japan
    Inc.](../Page/Epic_Records_Japan_Inc..md "wikilink")）→[VAPより移籍](../Page/VAP.md "wikilink")）

  - [初音](../Page/奥村初音.md "wikilink")（奥村初音）

  - [濱崎步](../Page/濱崎步.md "wikilink")

  - [玉木宏](../Page/玉木宏.md "wikilink")

#### 英文字母排序

  - [AAA](../Page/AAA_\(團體\).md "wikilink")
  - [alan](../Page/alan.md "wikilink")
  - [BoA](../Page/BoA.md "wikilink")\[3\]
  - [Da-iCE](../Page/Da-iCE.md "wikilink")
  - [DiVA](../Page/DiVA.md "wikilink")
  - [Do As Infinity](../Page/Do_As_Infinity.md "wikilink")
      - [伴都美子](../Page/伴都美子.md "wikilink")
  - [Dorothy Little Happy](../Page/DOROTHY_LITTLE_HAPPY.md "wikilink")
  - [Droog](../Page/Droog.md "wikilink")
  - Dream5
  - [Every Little Thing](../Page/Every_Little_Thing.md "wikilink")
      - [持田香織](../Page/持田香織.md "wikilink")
      - [伊藤一朗](../Page/伊藤一朗.md "wikilink")
  - [FPM](../Page/FPM.md "wikilink")（Fantastic Plastic Machine）
  - [GIRL NEXT DOOR](../Page/GIRL_NEXT_DOOR.md "wikilink")
  - [Honey L Days](../Page/Honey_L_Days.md "wikilink")
  - [JURIAN BEAT
    CRISIS](../Page/JURIAN_BEAT_CRISIS.md "wikilink")（川上ジュリア）
  - [Kis-My-Ft2](../Page/Kis-My-Ft2.md "wikilink")
  - [lol](../Page/Lol_\(團體\).md "wikilink")
  - [May J.](../Page/May_J..md "wikilink")
  - [m.c.A・T](../Page/m.c.A・T.md "wikilink")
  - [misono](../Page/misono.md "wikilink")（[day after
    tomorrow](../Page/day_after_tomorrow.md "wikilink")）
  - [moumoon](../Page/moumoon.md "wikilink")
  - ROOT FIVE(即√5，譯:根號五)
  - [SKE48](../Page/SKE48.md "wikilink")（[日本クラウン移籍至此](../Page/日本クラウン.md "wikilink")）
  - [SHINee](../Page/SHINee.md "wikilink")\[4\]
  - [SHU-I](../Page/SHU-I.md "wikilink")
  - [SOULHEAD](../Page/SOULHEAD.md "wikilink")
  - [Super Junior](../Page/Super_Junior.md "wikilink")\[5\]
  - [the pillows](../Page/the_pillows.md "wikilink")
  - [Tourbillon](../Page/Tourbillon_\(バンド\).md "wikilink")
  - [TRF](../Page/TRF.md "wikilink")
  - [teranoid\&MCnatsack](../Page/teranoid&MCnatsack.md "wikilink")
  - [U-KISS](../Page/U-KISS.md "wikilink")
  - [V6](../Page/V6_\(樂團\).md "wikilink")
      - [20th Century](../Page/V6_\(樂團\).md "wikilink")
      - [Coming Century](../Page/V6_\(樂團\).md "wikilink")

## 參考資料

## 外部連結

  - [Avex Trax官方網站](http://www.avexnet.jp/)

  - [愛貝克思官方網站](http://www.avex.co.jp/)

  -
  -
[Category:1990年成立的公司](../Category/1990年成立的公司.md "wikilink")
[Category:日本唱片公司](../Category/日本唱片公司.md "wikilink")
[Category:愛貝克思集團](../Category/愛貝克思集團.md "wikilink")

1.   About Avex Group {{\!}}
    History|work=[愛貝克思](../Page/愛貝克思.md "wikilink")|accessdate=2010-05-22}}

2.  使用[SM
    Entertainment專屬藝人的](../Page/SM_Entertainment.md "wikilink")[規格編號](../Page/規格編號.md "wikilink")"AV\*K-79\*\*\*"

3.
4.
5.