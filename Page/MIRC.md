**mIRC**是一個[Windows系統上的](../Page/Microsoft_Windows.md "wikilink")[即時通訊共享軟件](../Page/即時通訊軟件.md "wikilink")，由Khaled
Mardam-Bey開發並在1995年發佈。現在它的[mIRC語言已经在原来的基础上得到了极大的拓展](../Page/mIRC語言.md "wikilink")。其主要附加功能包括：

  - 頻道[IRC自動管理](../Page/IRC.md "wikilink")
  - 支持多人遊戲[伺服器](../Page/伺服器.md "wikilink")
  - 内置MP3播放器
  - 網頁parser（多數用於觀看搜尋結果或標題）
  - DCC、[HTTP及IRC伺服器](../Page/HTTP.md "wikilink")
  - 遊戲平台（這些遊戲多被稱為[mIRC遊戲](../Page/mIRC遊戲.md "wikilink")）

它是一個很知名的軟件，曾在2004年內於CNET的[download.com](http://www.download.com/)上被下載七百萬次。而在[Neilsen網上排名](http://www.internetnews.com/stats/article.php/3096631)調查中，mIRC是2003年最受欢迎的10大网络软件之一。它的成名可能是很多mIRC的用戶會誤以為他們所用的協議與軟件的名稱相同，所以他們會想到連到“mIRC伺服器”或“加入mIRC頻道”。

## 主要功能

  - 同時連接至多個伺服器（新增在版本6.0）
  - 高智能的事項性及指令性的程式語言
  - \-{zh-tw:支援;zh-cn:支持}-基本[CTCP](../Page/CTCP.md "wikilink")
  - \-{zh-tw:支援;zh-cn:支持}-CTCP的音訊（可播放[MP3](../Page/MP3.md "wikilink")、[WAV和](../Page/WAV.md "wikilink")[MIDI](../Page/MIDI.md "wikilink")）
  - \-{zh-tw:支援;zh-cn:支持}-[DCC的交談及檔案發送](../Page/DCC.md "wikilink")
  - 能停止下载隱藏的木馬
  - 檔案伺服器（由DCC交談控制）允許用戶瀏覽和下載指定的檔案
  - 支持ANSI-風格和mIRC-風格的文字
  - 可安裝外掛以使用語音辨識技术及發聲功能

## 常見問題

  - mIRC語言允許鬧事者欺騙其他用戶執行危險命令程式。
  - Khaled
    Mardam-Bey沒有咨詢其他mIRC开发者，而选择了一种备受指责的颜色格式；這也許是为什么一些IRC程序作者不對mIRC进行支持。
  - 利用mIRC可以傳送一個不引人注意的命令而令電腦关機，这个问题将会迫使用户升级。（6.1X版的mIRC已经修正了这个bug，更新后的用户已经不必再担心这个问题了）

## 復活節彩蛋

  - 在關於mIRC的對話框中按滑鼠右鍵，在上面mIRC的"I"上會出現一跳動的小點。
  - 在左邊mIRC的圖示按滑鼠左鍵，會變成舊的mIRC圖示。
  - 在右邊mIRC的作者鼻子上按滑鼠左鍵，會出現娃娃的叫聲。
  - 輸入英文arnie，mIRC的作者會變成一隻恐龍。
  - 在工具列最後一個"關於"圖示上按滑鼠右鍵，會出現一張笑臉。
  - 重複上述任何動作，就會恢復原狀。

## 外部連結

  - [官方網頁](http://www.mirc.com/)（英文）

[Category:IRC客户端](../Category/IRC客户端.md "wikilink")