**单于**\[1\]（[上古漢語擬音](../Page/上古漢語.md "wikilink")：；拼音([叶音](../Page/叶音.md "wikilink"))：；注音
ㄔㄢˊ
ㄩˊ），曾作**-{善于}-**\[2\]，是[匈奴人對他們部落联盟的首领的專稱](../Page/匈奴.md "wikilink")，意為廣大之貌。单于始創於匈奴著名的[冒頓单于的父親](../Page/冒顿.md "wikilink")[頭曼单于](../Page/头曼.md "wikilink")，之後這個稱號一直繼承下去，直到匈奴滅亡為止。而[東漢](../Page/东汉.md "wikilink")[三國之際](../Page/三国.md "wikilink")，有[烏丸](../Page/乌桓.md "wikilink")、[鮮卑的部落使用](../Page/鮮卑.md "wikilink")**单于**這個稱號。至[兩晉](../Page/晋朝.md "wikilink")[十六國](../Page/五胡十六国.md "wikilink")，皆改稱為**大单于**的稱號，但地位已不如以前。

至中世紀時，這個稱號被[可汗取代](../Page/可汗.md "wikilink")。而单于一词之后被转译为达干、达官、答刺罕、答儿罕、达儿罕、打儿汉等\[3\]。

## 字源

“單于”，据《[史记](../Page/史记.md "wikilink")·[匈奴列传](../Page/匈奴列传.md "wikilink")》，其称号全稱為“撐犁孤塗單于”，之後被簡化為單于。在迄今为止的研究中，[匈奴语属](../Page/匈奴语.md "wikilink")[古代突厥语](../Page/古代突厥語.md "wikilink")，“撐犁”意为“天”，与[突厥语系](../Page/突厥语族.md "wikilink")[蒙古语](../Page/蒙古语.md "wikilink")“Tengri（天）”同源。\[4\]“孤塗”意为“子”，“单于”意为“广大”，本義有[天子](../Page/天子.md "wikilink")、「如天一般廣大的上天之子」的意思。\[5\]\[6\]。

《[廣韻](../Page/廣韻.md "wikilink")》中記載，單分别有“都寒切”（中古汉语拟音：/tɑn/，现代汉语：dān）、“市连切”（中古汉语拟音：/d͡ʑiɛn/，现代汉语：chán）、“常演切”（中古汉语拟音：/d͡ʑiɛnX/，现代汉语：shàn）和“时战切”（中古汉语拟音：/d͡ʑiɛnH/，现代汉语：shàn）。

至漢文帝時，匈奴單于又稱大單于\[7\]。

## 單于繼位制度

匈奴单于的继承制度，大体可分为二种情况，[父死子继](../Page/父死子继.md "wikilink")、[兄终弟及和远亲袭位](../Page/兄终弟及.md "wikilink")。单于的传位可分三段时期，公元前209年至前127年，[冒顿](../Page/冒顿单于.md "wikilink")、[老上](../Page/老上单于.md "wikilink")、[军臣三单于持续了三代的父死子继](../Page/军臣单于.md "wikilink")；公元前127年至公元前31年，开始以父子相继和兄终弟及二种形式的交替出现，其中[呴犁湖和](../Page/呴犁湖單于.md "wikilink")[握衍朐提二单于为远亲袭位](../Page/握衍朐提單于.md "wikilink")；公元前20年至公元46年，由于呼韩邪单于“约令传国与弟”，规定了兄终弟及的方式，命其子死后传位给弟，[搜谐若鞮](../Page/搜谐若鞮单于.md "wikilink")、[车牙若鞮](../Page/车牙若鞮单于.md "wikilink")、[乌珠留若鞮](../Page/乌珠留若鞮单于.md "wikilink")、[乌累若鞮](../Page/乌累若鞮单于.md "wikilink")、[呼都而尸道皋若鞮五单于持续了五代的兄终弟及](../Page/呼都而尸道皋若鞮单于.md "wikilink")，一直延续到匈奴分裂前的第二年。\[8\]

## 各族单于列表

### 匈奴单于

### 烏丸单于

  - [蹋頓](../Page/蹋頓.md "wikilink")
  - 峭王[蘇僕延](../Page/蘇僕延.md "wikilink")
  - 汗魯王[烏延](../Page/烏延.md "wikilink")
  - [樓班](../Page/樓班.md "wikilink")

### 鮮卑单于

### 大单于

十六国时代的大单于一般是君主没有当天王或皇帝时担任的，他们当皇帝後，就将大单于的位置交给太子担任

  - [刘渊](../Page/刘渊.md "wikilink")
  - [刘聪](../Page/刘聪.md "wikilink")
  - [刘乂](../Page/刘乂.md "wikilink")
  - [刘粲](../Page/刘粲.md "wikilink")
  - [刘胤](../Page/刘胤.md "wikilink")
  - [石勒](../Page/石勒.md "wikilink")
  - [石弘](../Page/石弘.md "wikilink")
  - [石虎](../Page/石虎.md "wikilink")
  - [石宣](../Page/石宣.md "wikilink")
  - [冉胤](../Page/冉胤.md "wikilink")
  - [慕容廆](../Page/慕容廆.md "wikilink")
  - [慕容皝](../Page/慕容皝.md "wikilink")
  - [慕容儁](../Page/慕容儁.md "wikilink")
  - [苻洪](../Page/苻洪.md "wikilink")
  - [苻健](../Page/苻健.md "wikilink")
  - [苻苌](../Page/苻苌.md "wikilink")
  - [姚弋仲](../Page/姚弋仲.md "wikilink")
  - [姚襄](../Page/姚襄.md "wikilink")
  - [姚苌](../Page/姚苌.md "wikilink")
  - [慕容宝](../Page/慕容宝.md "wikilink")
  - [兰汗](../Page/兰汗.md "wikilink")
  - [乞伏国仁](../Page/乞伏国仁.md "wikilink")
  - [乞伏乾归](../Page/乞伏乾归.md "wikilink")
  - [冯永](../Page/冯永.md "wikilink")
  - [赫连勃勃](../Page/赫连勃勃.md "wikilink")
  - [烏紇提](../Page/烏紇提.md "wikilink")
  - [慕容樹洛干](../Page/慕容樹洛干.md "wikilink")

## 参阅

  - [阏氏](../Page/阏氏.md "wikilink")（）
  - [可汗](../Page/可汗.md "wikilink")（）

## 註釋

[Category:君主称谓](../Category/君主称谓.md "wikilink")
[Category:匈奴单于](../Category/匈奴单于.md "wikilink")
[C](../Category/汉语外来词.md "wikilink")
[Category:繁簡體文字應用衝突](../Category/繁簡體文字應用衝突.md "wikilink")

1.  不可寫作「單-{於}-」。
2.  《漢書·匈奴傳下》：「 天鳳二年…… 咸等至，多遺單于金珍，因諭說改其號，號匈奴曰『 恭奴』，單于曰『善於』，賜印綬。」
3.  [蒲立本](../Page/蒲立本.md "wikilink")《[上古汉语的辅音系统](../Page/上古汉语.md "wikilink")》
4.
5.  《漢書》卷94〈匈奴傳〉：「單于，姓攣鞮氏，其國稱之曰「撐犁孤塗單于」。匈奴謂天為「撐犁」，謂子為「孤塗」，單于者，廣大之貌也，言其象天單于然也。」
6.  《史記》〈匈奴列傳〉索隱引《玄晏春秋》：「士安讀《漢書》，不詳此言，有胡奴在側，言之曰：『此胡所謂天子』，與古書所說附會也。」
7.  《史記》〈匈奴列傳〉：「漢遺單于書，牘以尺一寸，辭曰：『皇帝敬問匈奴大單于無恙』，所遺物及言語云云。中行說令單于遺漢書以尺二寸牘，及印封皆令廣大長，倨傲其辭曰：『天地所生日月所置匈奴大單于敬問漢皇帝無恙』，所以遺物言語亦-{云云}-。」
8.  李晓敏，单于继承与匈奴政局，内蒙古师范大学报(哲学社会科学版)，1999年第1期。