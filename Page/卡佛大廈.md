[HK_Crawford_House.jpg](https://zh.wikipedia.org/wiki/File:HK_Crawford_House.jpg "fig:HK_Crawford_House.jpg")
[H\&M_Crawford_House_Store.jpg](https://zh.wikipedia.org/wiki/File:H&M_Crawford_House_Store.jpg "fig:H&M_Crawford_House_Store.jpg")
[Crawford_House_ZARA_Store_201510.jpg](https://zh.wikipedia.org/wiki/File:Crawford_House_ZARA_Store_201510.jpg "fig:Crawford_House_ZARA_Store_201510.jpg")\]\]
**卡佛大廈**（Crawford
House），是[香港島](../Page/香港島.md "wikilink")[中環的一座](../Page/中環.md "wikilink")[商業](../Page/商業.md "wikilink")[寫字樓](../Page/寫字樓.md "wikilink")[大廈](../Page/大廈.md "wikilink")，位於[皇后大道中](../Page/皇后大道中.md "wikilink")68號，樓高24層。主要租戶為醫務所、旅行社、金融及珠寶公司辦事處。

該大廈原名**連卡佛大廈**（Lane Crawford
House），但其最大租戶[連卡佛](../Page/連卡佛.md "wikilink")[百貨公司已於](../Page/百貨公司.md "wikilink")2004年遷址至[香港國際金融中心](../Page/香港國際金融中心.md "wikilink")，所以連卡佛大廈已更名為卡佛大厦。同時連卡佛大廈由連卡佛同系公司[會德豐擁有](../Page/會德豐.md "wikilink")，集團曾於2005年公開招標出售大廈全幢。\[1\]2014年8月11日，[會德豐的子公司](../Page/會德豐.md "wikilink")[九龍倉集團向母公司以](../Page/九龍倉集團.md "wikilink")26.88億港元現金購入卡佛大厦的業權。\[2\]

現時卡佛大廈最大的商戶是[ZARA](../Page/ZARA.md "wikilink")，佔地55000平方呎，上一個租戶為香港第一家[H\&M之旗艦店](../Page/H&M.md "wikilink")，於2007年3月至2013年運作。

## 鄰近

  - [陸海通大廈](../Page/陸海通大廈.md "wikilink")
  - [豐盛創建大廈](../Page/豐盛創建大廈.md "wikilink")

## 參見

  - [連卡佛](../Page/連卡佛.md "wikilink")
  - [卡佛大廈](http://www.whoms.net/servlet/print.jsp?series=24&article=9763)

## 資料來源

<references />

[Category:中西區寫字樓 (香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:香港中環寫字樓](../Category/香港中環寫字樓.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:九龍倉置業物業](../Category/九龍倉置業物業.md "wikilink")

1.  <http://www.wheelockcompany.com/cwheelock/wpl/c20050801153750.doc>
2.  [香港電台：九倉以近27億元向會德豐購入中環卡佛大廈](http://www.rthk.org.hk/rthk/news/expressnews/20140811/news_20140811_55_1028813.htm)