**Copy Control** 是一种于2001至2006年间由[EMI与](../Page/EMI.md "wikilink")[Sony
BMG采用并应用于数个地区](../Page/Sony_BMG.md "wikilink")（欧洲、加拿大、美国、澳洲等）推出之部分数位音訊光碟的防拷贝系统。注意：勿将其与[Microcosm公司于](../Page/Microcosm.md "wikilink")1989年发明的同名防拷贝系统混为一谈。

有数种形式的保护存在。基本上，Copy
Control的目的在于使[CD受防拷保护](../Page/CD.md "wikilink")。然而，由于它们不符合[红皮书的规范](../Page/红皮书_\(音讯CD\).md "wikilink")，Copy
Control光碟实际上不能被认为是CD。此一防拷系统是为了防止数位音訊抽取（即所谓的"ripping"），并借此限制其非法散布。Copy
Control使用了下列技术：

  - 包含多区段（[蓝皮书](../Page/蓝皮书.md "wikilink")）信息，使音频轨能在大部分[CD-ROM光驱上被隐藏](../Page/CD-ROM.md "wikilink")。
  - 损坏的[纠错码](../Page/纠错.md "wikilink")，使抽取出来的音频出现可被听见的缺陷。
  - 数据区通常包括含[DRM限制的音频数据](../Page/数字版权管理.md "wikilink")。它们与某些[操作系统并不兼容](../Page/操作系统.md "wikilink")。

在[荷兰](../Page/荷兰.md "wikilink")，[Sony与](../Page/Sony.md "wikilink")[环球音乐曾经实验过Copy](../Page/环球音乐.md "wikilink")
Control至2004年为止。[EMI则继续使用至](../Page/EMI.md "wikilink")2006年6月。

在美国，[环球唱片于](../Page/环球唱片.md "wikilink")2001至2002年间在某些产品上试用Copy
Control，但随后放弃。[华纳音乐集团仅在欧洲出版的](../Page/华纳音乐集团.md "wikilink")[呛辣红椒的](../Page/呛辣红椒.md "wikilink")
*Greatest Hits*等专辑上试用过。至2006年9月，Copy
Control所采用的由[Macrovision提供的](../Page/Macrovision.md "wikilink")[CDS技术不再被列于Macrovision的网站上](../Page/Cactus_Data_Shield.md "wikilink")，并已于澳洲等国被完全放弃。\[1\]

[Billboard杂志于](../Page/告示牌_\(雜誌\).md "wikilink")2006年12月的一篇文章中指出，EMI已决定于全球停用Copy
Control。但由于无官方正式文书的佐证，至今仍无法确认EMI是否已全面放弃Copy Control。

在[日本](../Page/日本.md "wikilink")，[avex
trax在](../Page/avex_trax.md "wikilink")2002年開始使用Copy
Control，其後多間音樂發行商都加入行列。但發行期間，因電腦技術都能破解Copy
Control、mp3音樂播放機的盛行、甚至國內有樂迷爭論Copy
Control影響音質及[最終擁有權等等問題及原因之下](../Page/最終擁有權.md "wikilink")，在2004年下半年，avex
trax及其他音樂發行商都宣佈放棄Copy Control的商業發行。而今Copy Control技術只限於宣傳用的CD。

在[香港及](../Page/香港.md "wikilink")[台灣](../Page/台灣.md "wikilink")，也曾經在2003年都嘗試出Copy
Control，但後來也無疾而終。

## 背景

Copy
Control是为了应对近年来日益严重的盗拷与[档案分享行为而制定的保护措施](../Page/档案分享.md "wikilink")。上述盗版行为据称已造成音乐产业的钜额损失。实际上，由于CD规格在1980年代初被制定时尚未有此类问题，因此，与[DVD相较](../Page/DVD.md "wikilink")，CD标准先天缺乏提供保护或其它[数字版权管理能力](../Page/数字版权管理.md "wikilink")。Copy
Control是一种试图在CD标准上应用防拷保护的方法。然而，由于它只是基于这种缺乏限制的格式之微小改变，且必须要让大部份的CD播放机能正常播放，其保护效力并不彰。

[CD-AUDIO_logo.png](https://zh.wikipedia.org/wiki/File:CD-AUDIO_logo.png "fig:CD-AUDIO_logo.png")
由于Copy
Control光盘不符合[音讯CD规范](../Page/红皮书_\(音讯CD\).md "wikilink")，它们不能打上由[飞利浦注册的CDDA图示](../Page/飞利浦.md "wikilink")。一场2003年在[法国的法律诉讼案中](../Page/法国.md "wikilink")，不能在车用CD音响中正常播放的
Copy Control "CD"被认定为"瑕疵品"。后期的Copy
Control光盘皆有显眼的说明文字表明其兼容性问题，诸如"可能无法在某些机器上正常播放，例如：车用CD音响"
。"然而，在口语及贩售介绍时仍然经常将Copy Control光盘等同视为CD或视为"受保护的CD"。

## 规避保护

[Nero-CC.jpg](https://zh.wikipedia.org/wiki/File:Nero-CC.jpg "fig:Nero-CC.jpg")

Copy Control光盘会被判读为音频、数据混合光盘。在Windows下，放入Copy
Control光盘通常会[自动启动CD内附的播放软件](../Page/autorun.md "wikilink")，其能播放存于该光盘中之受DRM保护的音频。在放入光盘时按住[shift键或禁用自动播放可以跳过这个步骤](../Page/shift键.md "wikilink")。

抽取音频轨的能力则决定在使用的驱动器。第一道难关是"伪造的"内容表（Table of
Contents，ToC）其目的是使CD-ROM驱动器无法读取音频轨。另一方面，CD-R/RW驱动器等设备则通常能存取光盘上的所有区段，因此能正常读取音频轨。亦有人称该保护可透过以签字笔涂黑光盘内圈约2\~4厘米的范围达成破解。然而，在Copy
Control技术的增进下，此方法可能不再有效。

另一个主要的难关则是不兼容（技术上被认为是损坏的）的纠错码。其影响力仍然决定在光驱的选用。有些光驱可以正常读取并撷取音频，另一些则会在撷取出的音频中产生明显的爆音。由此引发的一个相关问题是，这种光盘可能更怕刮伤。

Copy
Control亦无法防止用电脑声卡录制音频的行为，儘管这会导致微小的音质损失及需要更长的复制时间（若使用数码方式连接则能使音质无损失）。由于许多盗版商以此种方法撷取音频，这对音乐产业形成严峻考验。

通常CD-R/RW驱动器可以播放光盘，但会有偶然的中断情形（约每10秒发生一次），而DVD-R/RW驱动器则可以正常读取并撷取音频至电脑上。CD-ROM及DVD-ROM驱动器则通常需要透过光盘内附的播放器才能播放受保护的数据。

然而，Windows以外的操作系统可以轻易播放Copy
Control光盘，并被显示为两个分开的部份－"音讯CD"与数据部份－通常显示的为制造商所给的名称。由于内附的播放软件与自动播放功能通常是设计给Windows使用的，因此几乎没有办法可以阻止非Windows用户从Copy
Control光盘上撷取音频（然而此操作可能需较长时间）。

在[Linux中](../Page/Linux.md "wikilink")，Copy
Control光盘可透过[cdparanoia或](../Page/cdparanoia.md "wikilink")[KDE的](../Page/KDE.md "wikilink")
"audiocd:/" 服务进行存取。

在[Mac OS
X](../Page/Mac_OS_X.md "wikilink")，这些光盘可以轻易被[iTunes和](../Page/iTunes.md "wikilink")[Quicktime存取](../Page/Quicktime.md "wikilink")（当CDDA音轨被拖曳至该CD以外的目录，Quicktime会自动将该音轨转换成[AIFF无损格式](../Page/AIFF.md "wikilink")。）。仅管某些Copy
Control提供Mac OS版的软件，但并不常见。

由于其他技术的进步，这项技术已日渐无用。几乎所有新的电脑都能以撷取无任何保护之CD的相同方法撷取Copy
Control光盘的音轨，这使得这项技术更无用武之地。

## 音轨区以外的数据

  - [CDS-100 or CDS-200](../Page/Cactus_Data_Shield.md "wikilink")
    播放器与一个媒体文件数据库（一份以Windows Media格式存储的音频副本）。该播放器只能播放该音频副本。
  - [CDS-300](../Page/Cactus_Data_Shield.md "wikilink")
    播放器与防拷程序。防拷程序会在放入光碟时未经同意下透过Autorun程序自动安装到Windows系统(不论按下"I agree"或"I
    disagree")，防拷程序会干扰所有程序(Windows Media
    Player,iTunes,Nero等)播放或读取CD音讯，而光碟内的播放器可以忽略防拷程序并直接读取音频轨。该播放器可以让用户播放音频轨并将音频抽取成受DRM保护的WMA文件，并可以刻录成CD
    3次。（播放器会将音频撷取成320 kbit/s WMA文件，然后将其刻录于CD-R。请注意：刻录出来的副本音量较小，且音质较差）

## 参见

  - [Analog hole](../Page/Analog_hole.md "wikilink")
  - [Cactus Data Shield](../Page/Cactus_Data_Shield.md "wikilink")
  - [CDS
    100/200/300](../Page/Macrovision#Cactus_Data_Shield.md "wikilink")
  - [Totalplay](../Page/Macrovision#Cactus_Data_Shield.md "wikilink")
  - [Copy protection for audio
    CDs](../Page/Copy_protection#Copy_protection_for_audio_CDs.md "wikilink")
  - [:Category:Copy Control
    albums](../Category/Copy_Control_albums.md "wikilink") — overview of
    Copy Control releases
  - [MediaMax CD-3](../Page/MediaMax_CD-3.md "wikilink")
  - [Key2Audio](../Page/Key2Audio.md "wikilink")
  - [Extended Copy
    Protection](../Page/Extended_Copy_Protection.md "wikilink")
  - [Exact Audio Copy](../Page/Exact_Audio_Copy.md "wikilink")

## 来源

## 外部链接

  - [EMI](https://web.archive.org/web/20070929200640/http://www.emimusic.info/)
      - [Copy
        Control资讯](https://web.archive.org/web/20070929200659/http://www.emimusic.info/us_EN/sect4.html)（英文）
      - [光盘播放问题](https://web.archive.org/web/20070929200711/http://www.emimusic.info/us_EN/sect5.html)
        - 问与答（英文）
  - [消费者反制Copy Control（英文）](http://cacc.drmowinckel.com/)
  - [IFPI Press
    Release](https://web.archive.org/web/20050405202506/http://www.ifpi.org/site-content/press/20020614.html)
    - [IFPI](../Page/IFPI.md "wikilink") issues labeling guidelines for
    Copy Control discs
  - [Evaluating New Copy-Prevention Techniques for Audio
    CDs](https://web.archive.org/web/20051015015043/http://www.cs.princeton.edu/~jhalderm/papers/drm2002.pdf)（[PDF](../Page/Portable_Document_Format.md "wikilink")
    format）, by J. Alex Halderman
  - [padawan.info: Protected CD judged abusive in
    France](http://padawan.info/ip/protected_cd_judged_abusive_in_france.html)
  - [Guide to Copying Copy-Protected Music
    discs](https://web.archive.org/web/20051024013926/http://www.dsg.cs.tcd.ie/~haahrm/copying-protected-cds/)
  - [EMI Music & Sony/BMG Anti-Copy Control
    Information](http://www.fedge.net/emi/)

[\*](../Category/Copy_Control.md "wikilink")
[Category:數位版權管理](../Category/數位版權管理.md "wikilink")

1.  \[