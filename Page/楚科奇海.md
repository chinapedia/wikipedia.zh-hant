[Chukchi_Sea.png](https://zh.wikipedia.org/wiki/File:Chukchi_Sea.png "fig:Chukchi_Sea.png")

**楚科奇海**（[俄语](../Page/俄语.md "wikilink")：**Чуко́тское
мо́ре**）是[北冰洋的一个](../Page/北冰洋.md "wikilink")[边缘海](../Page/边缘海.md "wikilink")，位于[楚科奇半岛和](../Page/楚科奇半岛.md "wikilink")[阿拉斯加之间](../Page/阿拉斯加.md "wikilink")。其西界是[弗兰格尔岛边的](../Page/弗兰格尔岛.md "wikilink")[德朗海峡](../Page/德朗海峡.md "wikilink")，东界是将其与[波弗特海分开的阿拉斯加的](../Page/波弗特海.md "wikilink")[巴罗角](../Page/巴罗角.md "wikilink")，通过南面的[白令海峡连接](../Page/白令海峡.md "wikilink")[白令海和](../Page/白令海.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")。楚科奇海的主要港口为俄国的[楚科奇自治区的](../Page/楚科奇自治区.md "wikilink")。[国际日期变更线由西北到东南向东倾斜穿过楚科奇海](../Page/国际日期变更线.md "wikilink")，避免穿过弗兰格尔岛和俄罗斯本土的楚科奇自治区。

## 地理

楚科奇海的面积约595,000[平方公里](../Page/平方公里.md "wikilink")，56%面积的水深浅於50[公尺](../Page/公尺.md "wikilink")，一年之中只有四个月可以航行。楚科奇海底的主要地理特征为700公里长的霍普海盆，边缘为赫勒尔德拱起。整个海域的56%深度低于50米。与其他北极海域相比楚科奇海域内只有很少的岛屿。[弗兰格尔岛位于楚科奇海的西北边界处](../Page/弗兰格尔岛.md "wikilink")；[赫勒尔德岛位于海域最北端](../Page/赫勒尔德岛.md "wikilink")；余的就是[西伯利亚和](../Page/西伯利亚.md "wikilink")[阿拉斯加海岸线边的一些小岛](../Page/阿拉斯加州.md "wikilink")。“楚科奇”得名于住在海边和[楚科奇半岛上的](../Page/楚科奇半岛.md "wikilink")[楚科奇人](../Page/楚科奇人.md "wikilink")。住在海边的楚科奇人传统上以捕鱼、捕鲸和在寒冷的海上捕猎[海象为生](../Page/海象.md "wikilink")。

西伯利亚海岸线附近的地方有: Cape Billings、Cape
Schmidt、[阿姆古埃马河](../Page/阿姆古埃马河.md "wikilink")、Cape
Vankarem、大[科柳钦湾](../Page/科柳钦湾.md "wikilink")、Neskynpil'gyn
Lagoon、心石角、[艾奴尔米诺](../Page/艾奴尔米诺.md "wikilink")、[切吉京河](../Page/切吉京河.md "wikilink")、Inchoun、和[杰日尼奥夫角](../Page/杰日尼奥夫角.md "wikilink").

由阿拉斯加注入该海域的河流有Kivalina、Kobuk、Kokolik、Kukpowruk、Kukpuk、Noatak、Utukok、Pitmegea、Wulik等等.
由西伯利亚注入楚科奇海的河中，最主要的是[阿姆古埃马河](../Page/阿姆古埃马河.md "wikilink")、[伊奥尼韦姆河和](../Page/伊奥尼韦姆河.md "wikilink")[切吉京河](../Page/切吉京河.md "wikilink")。

**范围**

国际航道测量组织将楚科奇海的边界定义为如下\[1\]

西缘： [东西伯利亚海的最东边](../Page/东西伯利亚海.md "wikilink")，从弗兰格尔岛最北边通过该岛直至Cape
Blossom再到大陆上的Cape Yakan（176°40'E）。

北缘： 由阿拉斯加的巴罗角（71°20′N 156°20′W）到弗兰格尔岛最北端(179°30'W)的一条直线。

南缘： 西伯利亚和阿拉斯加之间的北极圈（66°33'39"N），即白令海的北缘。

习惯上会将一直到白令海峡最窄部分，即北纬66度线，的北段都算作楚科奇海域。

## 歷史

1648年，俄羅斯人[Semyon
Dezhnyov從](../Page/:en:Semyon_Dezhnyov.md "wikilink")[科雷馬河出海口航行到太平洋](../Page/科雷馬河.md "wikilink")[阿纳德尔河](../Page/阿纳德尔河.md "wikilink")，但他的路線不切合實際，隨後的200年並沒有再被使用。

1728年，一位俄羅斯海軍丹麥裔軍官[維他斯·白令從太平洋海面進入這個海域](../Page/維他斯·白令.md "wikilink")。

1779年，[庫克船長從太平洋海面也進入這個海域](../Page/詹姆斯·庫克.md "wikilink")。

## 地理

[Chukchi_Sea.JPG](https://zh.wikipedia.org/wiki/File:Chukchi_Sea.JPG "fig:Chukchi_Sea.JPG")

楚科奇海面积约有595,000平方公里（約合230,000平方英里）一年之中仅有四个月左右可以航行。

[Tsjoektsjenzee.PNG](https://zh.wikipedia.org/wiki/File:Tsjoektsjenzee.PNG "fig:Tsjoektsjenzee.PNG")

## 參考資料

  - [Ecological
    assessment](http://webarchive.loc.gov/all/20060930222354/http://www.unep.org/dewa/giwa/areas/reports/r1a/assessment_giwa_r1a.pdf)
  - [Audubon Alaska's Arctic Marine Synthesis: Atlas of the Chukchi and
    Beaufort
    Seas](https://web.archive.org/web/20100908163821/http://ak.audubon.org/birds-science-education/arctic-marine-synthesis-atlas-chukchi-and-beaufort-seas)

[\*](../Category/楚科奇海.md "wikilink")
[Category:北冰洋陸緣海](../Category/北冰洋陸緣海.md "wikilink")
[Category:阿拉斯加地理](../Category/阿拉斯加地理.md "wikilink")
[Чуко́тское](../Category/楚科奇自治區地理.md "wikilink")
[Category:俄羅斯-美國邊界](../Category/俄羅斯-美國邊界.md "wikilink")