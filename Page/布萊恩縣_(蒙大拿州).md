**布萊恩縣**（）是[美國](../Page/美國.md "wikilink")[蒙大拿州北部的一個縣](../Page/蒙大拿州.md "wikilink")，北鄰[加拿大](../Page/加拿大.md "wikilink")[沙士吉萬](../Page/沙士吉萬.md "wikilink")，面積10,979平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口6,491人。本縣縣治為[希努克](../Page/希努克_\(蒙大拿州\).md "wikilink")（Chinook）。

本縣於1912年2月29日成立，縣名紀念前[美國國務卿](../Page/美國國務卿.md "wikilink")[詹姆斯·G·布萊恩](../Page/詹姆斯·G·布萊恩.md "wikilink")。

## 地理

[Blaine_County_Courthouse.JPG](https://zh.wikipedia.org/wiki/File:Blaine_County_Courthouse.JPG "fig:Blaine_County_Courthouse.JPG")
[Bear_Paw_Battlefield.jpg](https://zh.wikipedia.org/wiki/File:Bear_Paw_Battlefield.jpg "fig:Bear_Paw_Battlefield.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，布萊恩縣的總面積為，其中有，即99.7%為陸地；，即0.3%為水域。\[1\]本縣既是懷俄明州面積第九大的縣份，亦是[美國面積第95大的縣份](../Page/美國面積最大縣份列表.md "wikilink")。大部份[貝克納普堡印第安人保留地](../Page/貝克納普堡印第安人保留地.md "wikilink")（Fort
Belknap Indian Reservation）土地皆位於本縣內。\[2\]

### 毗鄰縣和直轄市

  - [蒙大拿州](../Page/蒙大拿州.md "wikilink")
    [希爾縣](../Page/希爾縣_\(蒙大拿州\).md "wikilink")：西方
  - 蒙大拿州 [舒托縣](../Page/舒托縣_\(蒙大拿州\).md "wikilink")：西南方
  - 蒙大拿州 [弗格斯縣](../Page/弗格斯縣_\(蒙大拿州\).md "wikilink")：南方
  - 蒙大拿州 [菲臘斯縣](../Page/菲臘斯縣_\(蒙大拿州\).md "wikilink")：東方
  - [薩斯喀徹溫省](../Page/薩斯喀徹溫省.md "wikilink")
    [第51號里諾](../Page/第51號里諾.md "wikilink")：北方
  - 薩斯喀徹溫省 [第19號邊疆](../Page/第19號邊疆.md "wikilink")：北方
  - 薩斯喀徹溫省 [第18號隆娜特里](../Page/第18號隆娜特里.md "wikilink")：北方

### 國家保護區

  - [黑古力國家野生動物保護區](../Page/黑古力國家野生動物保護區.md "wikilink")
  - [內茲佩爾塞國家歷史公園](../Page/內茲佩爾塞國家歷史公園.md "wikilink")（部分）
  - [密蘇里河處游斷層國家紀念碑](../Page/密蘇里河處游斷層國家紀念碑.md "wikilink")（部分）

## 經濟

布萊恩縣的主要產業為農業。\[3\]而在印第安人保留地中，最大僱主則為[格羅斯文特雷人](../Page/格羅斯文特雷人.md "wikilink")（Gros
Ventre people）和[阿西尼博因人](../Page/阿西尼博因人.md "wikilink")（Assiniboine
people）部落。\[4\]

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，布萊恩縣擁有7,009居民、2,501住戶和1,793家庭。\[5\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")2居民（每平方公里1居民）。\[6\]本縣擁有2,947間房屋单位，其密度為每平方英里1間（每平方公里1間）。\[7\]而人口是由52.58%[白人](../Page/歐裔美國人.md "wikilink")、0.17%[黑人](../Page/非裔美國人.md "wikilink")、45.37%[土著](../Page/美國土著.md "wikilink")、0.09%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.03%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、0.23%其他[種族和](../Page/種族.md "wikilink")1.54%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")1%。在52.58%白人中，有18.8%是[德國人以及](../Page/德國人.md "wikilink")8.1%是[挪威人](../Page/挪威人.md "wikilink")。91.7%人口的母語為[英語](../Page/英語.md "wikilink")、3.8%
[German](../Page/German_language.md "wikilink") and 2.0%
[Dakota](../Page/Dakota_language.md "wikilink") as their first
language.\[8\]

在2,501住户中，有36%擁有一個或以上的兒童（18歲以下）、52.3%為夫妻、14.4%為單親家庭、28.3%為非家庭、26%為獨居、10.8%住戶有同居長者。平均每戶有2.78人，而平均每個家庭則有3.36人。在7,009居民中，有32.6%為18歲以下、8%為18至24歲、24.8%為25至44歲、21.6%為45至64歲以及12.9%為65歲以上。人口的年齡中位數為34歲，女子對男子的性別比為100：97.5。成年人的性別比則為100：96.3。\[9\]

本縣的住戶收入中位數為$25,247，而家庭收入中位數則為$30,616。男性的收入中位數為$23,627，而女性的收入中位數則為$20,469，[人均收入為](../Page/人均收入.md "wikilink")$12,101。約23.4%家庭和28.1%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括36.5%兒童（18歲以下）及19.9%長者（65歲以上）。\[10\]

## 參考文獻

[B](../Category/蒙大拿州行政區劃.md "wikilink")

1.

2.

3.

4.

5.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

6.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

7.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

8.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

9.  [American FactFinder](http://factfinder.census.gov/)

10.