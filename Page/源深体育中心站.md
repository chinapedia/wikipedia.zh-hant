**源深体育中心站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[张杨路](../Page/张杨路.md "wikilink")[源深路](../Page/源深路.md "wikilink")，为[上海轨道交通6号线的地下](../Page/上海轨道交通6号线.md "wikilink")[侧式车站](../Page/侧式站台.md "wikilink")，于2007年12月29日启用。车站以浅绿色作為佈置的主要色彩。

## 周邊

  - [源深体育中心](../Page/源深体育中心.md "wikilink")

## 公交换乘

130、169、181、339、609、736、775、783、785、790、791、961、975、977

## 车站出口

  - 1号口：张扬路北侧，源深路东（远离源深路一侧）
  - 2号口：张杨路南侧，源深路东（远离源深路一侧）
  - 3号口：张杨路南侧，源深路东（靠近源深路一侧）
  - 4号口：张杨路北侧，源深路东（靠近源深路一侧）

## 图片

[File:YSSNorthBoundPlatform.jpg|港城路站方向站台](File:YSSNorthBoundPlatform.jpg%7C港城路站方向站台)
[File:YSSExit2.jpg|2号出口（远处为3号出口](File:YSSExit2.jpg%7C2号出口（远处为3号出口)）

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")