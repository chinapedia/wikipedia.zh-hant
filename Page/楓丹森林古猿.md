**楓丹森林古猿**（*Dryopithecus
fontani*），又名**楓丹林猿**，是於19世紀在[法國發現已](../Page/法國.md "wikilink")[滅絕的](../Page/滅絕.md "wikilink")[猿](../Page/猿.md "wikilink")，可以追溯至[中新世中期](../Page/中新世.md "wikilink")。牠被認為是古[靈長目的首個](../Page/靈長目.md "wikilink")[演化證據](../Page/演化.md "wikilink")，是[歐洲最重要的](../Page/歐洲.md "wikilink")[化石之一](../Page/化石.md "wikilink")。

## 形態

楓丹森林古猿的上下顎[齒式都是](../Page/齒式.md "wikilink")2:1:2:3。牠的[門齒較其他古猿](../Page/門齒.md "wikilink")（如[原康修爾猿](../Page/原康修爾猿.md "wikilink")）較窄及少像竹片。上[臼齒有發展了部份的](../Page/臼齒.md "wikilink")[舌側](../Page/舌側.md "wikilink")。上[前臼齒相對較長](../Page/前臼齒.md "wikilink")，而下前臼齒則較闊。下臼齒有5個低而圓的牙尖，表面有[琺瑯質包裹](../Page/琺瑯質.md "wikilink")。[犬齒較為纖細](../Page/犬齒.md "wikilink")，[前上顎骨較短](../Page/前上顎骨.md "wikilink")。[下頜骨有下橫突](../Page/下頜骨.md "wikilink")，但卻沒有上橫突。

楓丹森林古猿的前肢有縮小了的[鷹嘴突](../Page/鷹嘴突.md "wikilink")，深的[肱骨滑車](../Page/肱骨.md "wikilink")，並沒有了[內上髁孔](../Page/內上髁孔.md "wikilink")。估計牠的體重約為35公斤。

## 分佈

楓丹森林古猿分佈在[歐洲](../Page/歐洲.md "wikilink")，如[西班牙及](../Page/西班牙.md "wikilink")[匈牙利](../Page/匈牙利.md "wikilink")。

## 食性

根據楓丹森林古猿的[牙齒](../Page/牙齒.md "wikilink")，估計牠是吃水果的。

## 外部連結

  - <http://members.tripod.com/cacajao/dryopithecus_fontani.html>
  - <http://johnhawks.net/weblog/fossils/apes/dryopithecus/dryopithecus_overview.html>

[Category:森林古猿](../Category/森林古猿.md "wikilink")