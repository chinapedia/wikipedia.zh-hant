**舍利寺**（）是[越南](../Page/越南.md "wikilink")[胡志明市最大的佛寺](../Page/胡志明市.md "wikilink")，建於1956年，是越南南部的佛教中心。該寺位於胡志明市第三郡，佔地2500平方米。

1963年8月21日，南越天主教徒總統[吳廷琰之弟](../Page/吳廷琰.md "wikilink")[吳廷瑈指示](../Page/吳廷瑈.md "wikilink")[南越軍隊查抄並破壞寺院](../Page/南越軍隊.md "wikilink")（[查抄舍利寺](../Page/查抄舍利寺.md "wikilink")），該寺在國外因而著稱。

1981年以前，該寺是[越南佛教協會總部](../Page/越南佛教協會.md "wikilink")，且至1993年5月為止是該會的第二辦公室。

[ChuaXaLoi002.jpg](https://zh.wikipedia.org/wiki/File:ChuaXaLoi002.jpg "fig:ChuaXaLoi002.jpg")
該寺鐘樓建於1961年，高7層、32米，是越南最高的鐘樓，且在頂層有重達2噸的大鐘。

## 参考文献

  -
  -
  -
[Category:越南佛寺](../Category/越南佛寺.md "wikilink")
[Category:胡志明市建築物](../Category/胡志明市建築物.md "wikilink")
[Category:越南傳統建築](../Category/越南傳統建築.md "wikilink")