**馬豪輝**（，），[回族人](../Page/回族.md "wikilink")，[穆斯林](../Page/穆斯林.md "wikilink")，籍貫[寧夏](../Page/寧夏.md "wikilink")\[1\]，[東院道官立小學](../Page/東院道官立小學.md "wikilink")、[聖保羅書院校友](../Page/聖保羅書院.md "wikilink")，[香港執業](../Page/香港.md "wikilink")[律師](../Page/律師.md "wikilink")，現任中華人民共和國[香港特別行政區](../Page/香港特別行政區.md "wikilink")[第十二屆全國人民代表大會代表](../Page/第十二屆全國人民代表大會.md "wikilink")、[胡關李羅律師行高級合夥人](../Page/胡關李羅律師行.md "wikilink")、[香港基本法研究中心副主席](../Page/香港基本法研究中心.md "wikilink")、[僱員補償援助基金管理局主席](../Page/僱員補償援助基金管理局.md "wikilink")、[中華回教博愛社顧問](../Page/中華回教博愛社.md "wikilink")、[破產欠薪保障基金委員會委員](../Page/破產欠薪保障基金委員會.md "wikilink")、[選舉委員會委員](../Page/選舉委員會.md "wikilink")、[地產代理監管局委員](../Page/地產代理監管局.md "wikilink")、[空運牌照局成員](../Page/空運牌照局.md "wikilink")、[香港旅遊業議會理事](../Page/香港旅遊業議會.md "wikilink")。曾任[粵劇發展諮詢委員會成員](../Page/粵劇發展諮詢委員會.md "wikilink")、[土地及建設諮詢委員會委員](../Page/土地及建設諮詢委員會.md "wikilink")、[香港房屋委員會委員和](../Page/香港房屋委員會.md "wikilink")[大舜政策研究中心顧問](../Page/大舜政策研究中心.md "wikilink")\[2\]、[伊斯蘭脫維善紀念中學校董](../Page/伊斯蘭脫維善紀念中學.md "wikilink")。

## 家庭狀況

馬豪輝已婚，其妻為馬廖千睿，並育有女兒馬嘉烯。

## 馬主生涯

馬豪輝與馬廖千睿夫婦於1998/1999年度馬季開始成為馬主，並擁有馬匹「得意非凡」（由何良訓練），在港出賽47次，取得3冠4亞2季成績，於2004年退役。

## 榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（2005年）
  - [銀紫荊星章](../Page/銀紫荊星章.md "wikilink")（2007年）
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")（2017年）\[3\]

## 參考文獻

<references/>

[分類:第十三屆港區全國人大代表](../Page/分類:第十三屆港區全國人大代表.md "wikilink")

[H豪](../Category/馬姓.md "wikilink")
[Category:寧夏人](../Category/寧夏人.md "wikilink")
[Category:回族人](../Category/回族人.md "wikilink")
[Category:香港穆斯林](../Category/香港穆斯林.md "wikilink")
[Category:東院道官立小學校友](../Category/東院道官立小學校友.md "wikilink")
[Category:聖保羅書院校友](../Category/聖保羅書院校友.md "wikilink")
[Category:香港法律界人士](../Category/香港法律界人士.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:第十一屆港區全國人大代表](../Category/第十一屆港區全國人大代表.md "wikilink")
[Category:第十二屆港區全國人大代表](../Category/第十二屆港區全國人大代表.md "wikilink")
[M](../Category/香港馬主.md "wikilink")

1.
2.
3.