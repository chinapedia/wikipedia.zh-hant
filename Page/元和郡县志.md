《**元和郡縣圖志**》，[唐代](../Page/唐代.md "wikilink")[地理著作](../Page/地理.md "wikilink")，[李吉甫撰](../Page/李吉甫.md "wikilink")，完成於[元和八年](../Page/元和.md "wikilink")（813年）。是[中國現存最早](../Page/中國.md "wikilink")、較完整的[地理總志](../Page/地理.md "wikilink")。

## 概述

《元和郡縣圖志》原本四十卷，目錄二卷，共四十二卷。內容以[貞觀時期的](../Page/貞觀.md "wikilink")[十道](../Page/十道.md "wikilink")（[关内道](../Page/关内道.md "wikilink")、[河南道](../Page/河南道.md "wikilink")、[河东道](../Page/河东道.md "wikilink")、[河北道](../Page/河北道.md "wikilink")、[山南道](../Page/山南道.md "wikilink")、[陇右道](../Page/陇右道.md "wikilink")、[淮南道](../Page/淮南道.md "wikilink")、[江南道](../Page/江南道.md "wikilink")、[剑南道](../Page/剑南道.md "wikilink")、[岭南道](../Page/岭南道.md "wikilink")）為綱，配以[唐憲宗時的實際上存在的四十七個](../Page/唐憲宗.md "wikilink")[觀察使](../Page/觀察使.md "wikilink")、[節度使轄區](../Page/節度使.md "wikilink")（當時也同樣稱為"道"），每道繪有圖，記載各鎮之地理沿革、户口、山川、贡赋等，內容可上溯到三代或《[禹贡](../Page/禹贡.md "wikilink")》的记载。

李吉甫亦在書中於[廣德二年](../Page/廣德_\(唐朝\).md "wikilink")\[1\]、元和三年\[2\]、元和七年\[3\]、元和八年\[4\]表達自己的意見。

[南宋以后图已亡佚](../Page/南宋.md "wikilink")，亦略称为《**元和郡县志**》，文字也有残缺，第十九、二十、二十三、二十四、二十六、三十六诸卷已佚，第十八卷和第二十五卷缺一小部份。《[四庫總目提要](../Page/四庫總目提要.md "wikilink")》說：“輿地圖經，隋唐誌所著錄者，率散佚無存；其傳於今者，惟此書為最古，其體例亦為最善，後來雖遞相損益，無能出其範圍。”

## 補注

清代[严观著有](../Page/严观.md "wikilink")《元和郡县补志》，[缪荃荪有](../Page/缪荃荪.md "wikilink")《元和郡县志缺卷逸文》。近代[贺次君为其注解](../Page/贺次君.md "wikilink")，是目前最佳的版本，[中华书局收錄於](../Page/中华书局.md "wikilink")《中國古代地理總志叢刊》。

## 目錄

  - 一至四：關內道（[京兆府](../Page/京兆府.md "wikilink")・[鳳翔節度使](../Page/鳳翔節度使.md "wikilink")・[涇原節度使](../Page/涇原節度使.md "wikilink")・[邠寧節度使](../Page/邠寧節度使.md "wikilink")・[鄜坊觀察使](../Page/鄜坊觀察使.md "wikilink")・[靈武節度使](../Page/靈武節度使.md "wikilink")・[夏綏節度使](../Page/夏綏節度使.md "wikilink")・[振武節度使](../Page/振武節度使.md "wikilink")・[豐州都防禦使](../Page/天德軍.md "wikilink")）
  - 五至十一：河南道（[河南府](../Page/河南府.md "wikilink")・[陝虢觀察使](../Page/陝虢觀察使.md "wikilink")・[汴宋節度使](../Page/汴宋節度使.md "wikilink")・[鄭滑節度使](../Page/鄭滑節度使.md "wikilink")・[陳許節度使](../Page/陳許節度使.md "wikilink")・[徐泗節度使](../Page/徐泗節度使.md "wikilink")・[蔡州節度使](../Page/蔡州節度使.md "wikilink")・[淄青節度使](../Page/淄青節度使.md "wikilink")）
  - 十二至十五：河東道（[河中節度使](../Page/河中節度使.md "wikilink")・[河東節度使](../Page/河東節度使.md "wikilink")・[澤潞節度使](../Page/澤潞節度使.md "wikilink")）
  - 十六至十九：河北道（[河陽三城懷州節度使](../Page/河陽節度使.md "wikilink")・[魏博節度使](../Page/魏博節度使.md "wikilink")・[恒冀節度使](../Page/恒冀節度使.md "wikilink")・[易定節度使](../Page/易定節度使.md "wikilink")・[滄景節度使](../Page/滄景節度使.md "wikilink")）
  - 二十至二十三：山南道（[襄陽節度使](../Page/襄陽節度使.md "wikilink")・[山南西道節度](../Page/山南西道節度.md "wikilink")）
  - 二十四：淮南道
  - 二十五至三十：江南道（[浙西觀察使](../Page/浙西觀察使.md "wikilink")・[浙東觀察使](../Page/浙東觀察使.md "wikilink")・[鄂岳觀察使](../Page/鄂岳觀察使.md "wikilink")・[江南西道觀察使](../Page/江南西道觀察使.md "wikilink")・[宣歙觀察使](../Page/宣歙觀察使.md "wikilink")・[湖南觀察使](../Page/湖南觀察使.md "wikilink")・[福建觀察使](../Page/福建觀察使.md "wikilink")・[黔州觀察使](../Page/黔州觀察使.md "wikilink")）
  - 三十一至三十三：劍南道（[成都府](../Page/成都府.md "wikilink")・[西川節度使](../Page/西川節度使.md "wikilink")・[東川節度使](../Page/東川節度使.md "wikilink")）
  - 三十四至三十八：嶺南道（[嶺南節度使](../Page/嶺南節度使.md "wikilink")・[桂管經略使](../Page/桂管經略使.md "wikilink")・[邕管經略使](../Page/邕管經略使.md "wikilink")・[安南都護府](../Page/安南都護府.md "wikilink")）
  - 三十九至四十：隴右道

## 參考資料

  - [唐朝行政區劃](../Page/唐朝行政區劃.md "wikilink")
  - 《[舊唐書](../Page/舊唐書.md "wikilink")·經籍志》
  - 《[新唐書](../Page/新唐書.md "wikilink")·藝文志》

[Category:唐朝典籍](../Category/唐朝典籍.md "wikilink")
[Category:地理總志](../Category/地理總志.md "wikilink")
[Category:地理書籍](../Category/地理書籍.md "wikilink")

1.  見卷一〈雲陽縣〉條
2.  見卷三十〈涪州〉條
3.  見卷十四〈蔚州·三河冶〉條
4.  見卷四〈新宥州〉〈天德軍〉條