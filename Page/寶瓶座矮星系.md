{{ Galaxy | | image =
[Aquarius_Dwarf_Hubble_WikiSky.jpg](https://zh.wikipedia.org/wiki/File:Aquarius_Dwarf_Hubble_WikiSky.jpg "fig:Aquarius_Dwarf_Hubble_WikiSky.jpg")
<small>[哈伯太空望遠鏡拍攝的寶瓶座矮星系](../Page/哈伯太空望遠鏡.md "wikilink")</small> | name
= 寶瓶座矮星系 | epoch = [J2000](../Page/J2000.md "wikilink") | type =
IB(s)m\[1\] | ra = \[2\] | dec = \[3\] | dist_ly = 3.10 ±
0.16[Mly](../Page/光年.md "wikilink") (950 ± 50
[kpc](../Page/秒差距.md "wikilink"))\[4\] | z = -141 ± 2
[公里](../Page/公里.md "wikilink")/秒\[5\] | appmag_v = 14.0\[6\] |
size_v = 2′.2 × 1′.1\[7\] | constellation name =
[寶瓶座](../Page/寶瓶座.md "wikilink") | notes =
呈現出[藍移](../Page/藍移.md "wikilink") | names =
[DDO](../Page/David_Dunlap_Observatory_Catalogue.md "wikilink")
210,\[8\] AqrDIG,\[9\]
[PGC](../Page/Principal_Galaxies_Catalogue.md "wikilink") 65367\[10\] }}

**寶瓶座矮星系** (AqrDIG)
是位於[寶瓶座內的一個](../Page/寶瓶座.md "wikilink")[矮不規則星系](../Page/矮不規則星系.md "wikilink")，最初是在1966年被登錄在[DDO巡天的目錄上](../Page/DDO巡天.md "wikilink")。它最特別的特徵是少數顯示出[藍移的星系之一](../Page/藍移.md "wikilink")，以137 [公里](../Page/公里.md "wikilink")/秒朝向[銀河系運動](../Page/銀河系.md "wikilink")。

李等人在1999年明確的認定AqrDIG是[本星系群的成員並且使用](../Page/本星系群.md "wikilink")[紅巨星分支技術得知其距離為](../Page/紅巨星分支技術.md "wikilink")950
± 50 千秒差距。它距離集團[質量中心的距離也是](../Page/質量中心.md "wikilink")950
千秒差距，這意味著AqrDIG在空間上可能是被隔離著的。相較於[SagDIG他顯然暗淡了許多](../Page/人馬座矮不規則星系.md "wikilink")<ref name="Bergh2000">{{
citation

`| last1 = van den Bergh`
`| first1 = Sidney`
`| authorlink1 = Sidney van den Bergh`
`| title = Updated Information on the Local Group`
`| year = 2000`
`| date = April 2000`
`| journal = The Publications of the Astronomical Society of the Pacific`
`| volume = 112`
`| issue = 770`
`| page = 529-536`
`| url = `<http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=2000PASP>`..112..529V`

}}</ref>。

## 參考資料

[Category:矮不規則星系](../Category/矮不規則星系.md "wikilink")
[Category:不規則星系](../Category/不規則星系.md "wikilink")
[Category:寶瓶座](../Category/寶瓶座.md "wikilink")
[65367](../Category/PGC天體.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.

10.