**環球音樂集團**（英語：Universal Music
Group，簡稱UMG）是全世界第一大[唱片公司](../Page/唱片公司.md "wikilink")，隶属[維旺迪集團](../Page/維旺迪.md "wikilink")，佔有世界唱片市場的25.6%。環球音樂公司原本只是美國電影大廠[環球影業旗下的一個部門](../Page/環球影業.md "wikilink")，後來環球影業在1998年由母公司[Seagram買入專門出版](../Page/Seagram.md "wikilink")[古典音樂及](../Page/古典音樂.md "wikilink")[流行音樂唱片的](../Page/流行音樂.md "wikilink")[寶麗金唱片](../Page/寶麗金唱片.md "wikilink")，成為今日的環球音樂集團。

2012年，[歐盟委員會同意環球音樂集團以](../Page/歐盟委員會.md "wikilink")19億美金從[花旗集團收購](../Page/花旗集團.md "wikilink")[EMI唱片集團](../Page/EMI唱片集團.md "wikilink")（包括[維京唱片](../Page/維京唱片.md "wikilink")）的交易案，環球音樂集團更加穩固世界第一大音樂集團地位。\[1\]

2018年，美國創作型歌手[泰勒絲](../Page/泰勒·斯威夫特.md "wikilink")（Taylor
Swift）重新與環球音樂簽約。新合約中，她可以擁有她的音樂版權；她也要求環球音樂出售[Spotify股份時得到的利益讓環球音樂旗下的歌手抽成](../Page/Spotify.md "wikilink")，而且當中不得扣除任何費用，並且環球音樂也同意以上的要求，此舉保障了音樂人在串流媒體的權益，引起多數媒體的好評，並稱泰勒絲此舉為開創先河。

## 唱片品牌

環球音樂保留了原來寶麗金旗下的各個子公司品牌，其他子公司詳列如下：

  - [寶麗金](../Page/寶麗金.md "wikilink")（PolyGram）
  - [寶麗多唱片](../Page/寶麗多唱片.md "wikilink") (Polydor)
  - [Barclay Records](../Page/Barclay_Records.md "wikilink")
  - [迪卡唱片公司](../Page/迪卡唱片公司.md "wikilink") (Decca)
  - [德意志留聲機公司](../Page/德意志留聲機公司.md "wikilink") (DG)
  - [Dreamworks Records](../Page/Dreamworks_Records.md "wikilink")
  - [聯眾唱片](../Page/Republic_Records.md "wikilink")（Republic Records）
  - [大機器唱片](../Page/大機器唱片.md "wikilink")（Big Machine Records）
  - [格芬唱片](../Page/Geffen_Records.md "wikilink")（Geffen Records）
  - [新視境唱片](../Page/Interscope_Records.md "wikilink")（Interscope
    Records）
  - [小島唱片](../Page/Island_Records.md "wikilink")（Island Reocrds）
  - [Def Jam](../Page/Def_Jam.md "wikilink")
  - [Jazzland Records](../Page/Jazzland_Records.md "wikilink")
  - [Lost Highway Records](../Page/Lost_Highway_Records.md "wikilink")
  - [MCA Nashville Records](../Page/MCA_Nashville_Records.md "wikilink")
  - [Mercury Nashville
    Records](../Page/Mercury_Nashville_Records.md "wikilink")
  - [水星唱片](../Page/水星唱片.md "wikilink") (Mercury Records)
  - [Motor Music Records](../Page/Motor_Music_Records.md "wikilink")
  - [飛利浦唱片公司](../Page/飛利浦唱片公司.md "wikilink") (Philips)
  - [Stockholm Records](../Page/Stockholm_Records.md "wikilink")
  - [Universal Classics
    Group](../Page/Universal_Classics_Group.md "wikilink")
  - [Universal Records](../Page/Universal_Records.md "wikilink")
  - [環球南方唱片](../Page/環球南方唱片.md "wikilink") (Universal South Records)
  - [Blackground Records](../Page/Blackground_Records.md "wikilink")
  - [Motown Records](../Page/Motown_Records.md "wikilink")
  - [Cash Money Records](../Page/Cash_Money_Records.md "wikilink")
  - [Bad Boy Records](../Page/Bad_Boy_Records.md "wikilink")
  - [Casablanca Records](../Page/Casablanca_Records.md "wikilink")
  - [Street Records
    Corporation](../Page/Street_Records_Corporation.md "wikilink")
  - [Urban Records](../Page/Urban_Records.md "wikilink")
  - [Verve Records](../Page/Verve_Records.md "wikilink")

環球音樂公司除了以上的品牌以外，亦在以下71個國家或地区，以本身的名義發行唱片：
[澳大利亞](../Page/澳大利亞.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[捷克](../Page/捷克.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")、[德國](../Page/德國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[日本](../Page/日本.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡等](../Page/新加坡.md "wikilink")。

## 中国大陸环球音樂

[上腾娱乐和](../Page/上腾娱乐.md "wikilink")[天韵文化是环球音樂公司和](../Page/環球唱片上海天韻文化發展有限公司.md "wikilink")[上海文广新闻传媒集团共同创建的两家公司](../Page/上海文广新闻传媒集团.md "wikilink")，前者负责音乐类选秀节目《[我型我秀](../Page/我型我秀.md "wikilink")》的相关事物和艺人开发。

## 台灣環球音樂

環球音樂是現時台灣其中一間主要唱片公司，旗下有[上華唱片及](../Page/上華唱片.md "wikilink")[百代唱片](../Page/百代唱片.md "wikilink")。

  - 1989年，[寶麗金唱片集團](../Page/寶麗金唱片.md "wikilink")（環球音樂前身）在台灣正式成立分公司。
  - 1992年，[寶麗金唱片集團](../Page/寶麗金唱片.md "wikilink")（環球音樂前身）曾收購台灣[福茂唱片](../Page/福茂唱片.md "wikilink")60%股權，使褔茂曾經為台灣環球的附屬公司，而褔茂也同時代理環球旗下[Decca的品牌](../Page/Decca.md "wikilink")。
  - 1995年，環球唱片母公司[美希亞音樂](../Page/美希亞音樂.md "wikilink")（Music Corporation
    of
    America，簡稱[MCA](../Page/MCA.md "wikilink")）在[臺北市成立](../Page/臺北市.md "wikilink")[子公司](../Page/子公司.md "wikilink")「[美希亞國際音樂娛樂](../Page/美希亞國際音樂娛樂.md "wikilink")」（MCA
    International），臺灣區總裁為[王偉忠](../Page/王偉忠.md "wikilink")。
  - 1996年，MCA被[加拿大](../Page/加拿大.md "wikilink")[Seagram集團收購](../Page/Seagram.md "wikilink")。
    1997年，美希亞國際音樂娛樂改名為「環球國際音樂股份有限公司」，以[環球音樂名義發行唱片](../Page/環球音樂.md "wikilink")，歌手有[吳大衛](../Page/吳大衛.md "wikilink")、[金智娟及](../Page/金智娟.md "wikilink")[黃鶯鶯等](../Page/黃鶯鶯.md "wikilink")。同年，[台灣寶麗金曾收購](../Page/寶麗金_\(臺灣\).md "wikilink")[上華唱片](../Page/上華唱片.md "wikilink")60%的股份。
  - 1998年，Seagram集團收購[寶麗金唱片](../Page/寶麗金唱片.md "wikilink")，與舊的環球音樂合併，成立以原寶麗金為主體的新[環球音樂集團](../Page/環球唱片.md "wikilink")。
  - 1999年，[台灣寶麗金與舊的環球國際音樂合併](../Page/寶麗金_\(臺灣\).md "wikilink")，并改名為「環球國際唱片股份有限公司」（簡稱「環球唱片」），原屬於[台灣寶麗金子公司的](../Page/寶麗金_\(臺灣\).md "wikilink")[上華唱片因此成為](../Page/上華唱片.md "wikilink")[環球音樂旗下子公司](../Page/環球唱片.md "wikilink")。
  - 2000年12月6日，上華唱片正式與台灣[環球音樂合併](../Page/環球唱片_\(台灣\).md "wikilink")\[2\]。
  - 2002年，褔茂的創辦人[張人鳳及](../Page/張人鳳.md "wikilink")[張耕宇向環球唱片集團回購該批股份](../Page/張耕宇.md "wikilink")，但Decca品牌的代理權仍由褔茂持有，而福茂旗下藝人於香港也交由[環球唱片負責](../Page/環球唱片_\(香港\).md "wikilink")。
  - 2003年，[環球音樂總公司決定正式整合大中華區的業務](../Page/環球唱片.md "wikilink")，把香港的[正東唱片](../Page/正東唱片.md "wikilink")、[新藝寶唱片及台灣的](../Page/新藝寶唱片.md "wikilink")[上華唱片運作合併](../Page/上華唱片.md "wikilink")，但仍然保持其獨立品牌。
  - 2013年，環球音樂正式收購[百代唱片](../Page/百代唱片.md "wikilink")，成為旗下子公司，並於2014年6月以旗下音樂子品牌之名，重新將EMI品牌引進華語樂壇，[張惠妹為首位加盟歌手](../Page/張惠妹.md "wikilink")，并由[張惠妹擔任大中華區品牌總監](../Page/張惠妹.md "wikilink")。

## 香港環球音樂

环球音樂是現時香港其中一間主要唱片公司，前身寶麗金唱片公司成立於1970年，並於1985年邀請了[陳少寶創立旗下的](../Page/陳少寶.md "wikilink")[新藝寶唱片](../Page/新藝寶唱片.md "wikilink")，並於1995年時邀請了經理人[黃柏高創立旗下的](../Page/黃柏高.md "wikilink")[正東唱片](../Page/正東唱片.md "wikilink")。[上華唱片亦曾被環球音樂併購至今](../Page/上華唱片.md "wikilink")。

环球音樂
(香港)在[宝丽金时代曾经涌现出几位金牌巨星级歌手](../Page/宝丽金_\(香港\).md "wikilink")，80年代的[谭咏麟是整个香港唱片销量的保证](../Page/谭咏麟.md "wikilink")，而1990年代开始后[张学友成为了当时宝丽金全球总公司的十佳销量巨星](../Page/张学友.md "wikilink")，[张学友也成为了全亚洲乃至全世界瞩目的歌手](../Page/张学友.md "wikilink")，但是[张学友在环球前亚太区总裁郑东汉离开后于](../Page/张学友.md "wikilink")2001年于环球期满后加盟了[上华唱片](../Page/上华唱片.md "wikilink")。但上華唱片本身是環球音樂在台灣的子公司，2003年其四間公司合併後，顺理成章又成为了環球音樂的旗下歌手，張學友亦持有環球音樂股份，是環球音樂的老闆之一。\[3\]另外，[張國榮去世後之](../Page/張國榮.md "wikilink")[華星唱片授權亦於](../Page/華星唱片.md "wikilink")2007年售予環球唱片集團
(香港)。

2013年重開[寶麗金力邀實力歌手陳慧嫻加盟](../Page/寶麗金.md "wikilink")，作為重開後第一個回歸寶麗金歌手。另外，隨着環球音樂正式收購[百代](../Page/百代.md "wikilink")(EMI)關係，[百代唱片
(香港)的運作](../Page/百代唱片_\(香港\).md "wikilink")、音像產品及版權現交由[環球音樂
(香港)管理](../Page/環球唱片_\(香港\).md "wikilink")。

### 負面評價

環球高層黃劍濤與商台助理總經理[甘菁菁是夫妻關係](../Page/甘菁菁.md "wikilink")，後者更曾任[陳奕迅經理人](../Page/陳奕迅.md "wikilink")，因此該兩所機構常被質疑其獨立性及公正性。其爭議性更多見於商台每年之[叱咤樂壇流行榜頒獎典禮](../Page/叱咤樂壇流行榜.md "wikilink")，傳媒與外界部分人士皆質疑環球音樂多次於叱咤樂壇流行榜頒獎典禮奪得多個大獎，冠絕全港所有唱片公司，背後是基於某些親密關係。\[4\]

## 日本環球音樂

環球音樂在日本的公司是Universal Music LLC，旗下音乐厂牌有UNIVERSAL J、ユニバーサル
シグマ、EMI日本等。有著眾多知名歌手、組合、作曲家，包括[DREAMS COME
TRUE](../Page/DREAMS_COME_TRUE.md "wikilink")、[Perfume](../Page/Perfume.md "wikilink")、[福山雅治](../Page/福山雅治.md "wikilink")、[Rainbow](../Page/Rainbow.md "wikilink")、[德永英明](../Page/德永英明.md "wikilink")、[桑田佳祐](../Page/桑田佳祐.md "wikilink")、[柴崎幸](../Page/柴崎幸.md "wikilink")、[恰拉](../Page/恰拉.md "wikilink")、[mihimaru
GT](../Page/mihimaru_GT.md "wikilink")、[WaT](../Page/WaT.md "wikilink")、[西城秀樹](../Page/西城秀樹.md "wikilink")、[織田裕二](../Page/織田裕二.md "wikilink")、[中森明菜](../Page/中森明菜.md "wikilink")、[久石讓](../Page/久石讓.md "wikilink")、[KARA](../Page/KARA.md "wikilink")、[少女时代](../Page/少女时代.md "wikilink")、[高橋南](../Page/高橋南.md "wikilink")、[SHINee](../Page/SHINee.md "wikilink")、[防彈少年團等](../Page/防彈少年團.md "wikilink")。

## 注釋

## 參考文獻

## 外部連結

  - [環球唱片（大陸）](http://www.umusic.com.cn/)
  - [環球唱片（香港）](http://www.umg.com.hk/)
  - [環球唱片（台灣）](http://www.umusic.com.tw/)
  - [環球唱片公司的歷史（英語）](https://web.archive.org/web/20051023142709/http://www.universalmusic.com/history.aspx)

[Category:1934年成立的公司](../Category/1934年成立的公司.md "wikilink")
[Category:環球唱片](../Category/環球唱片.md "wikilink")
[Category:威望迪子公司](../Category/威望迪子公司.md "wikilink")
[Category:美國唱片公司](../Category/美國唱片公司.md "wikilink")
[Category:國際唱片業協會成員](../Category/國際唱片業協會成員.md "wikilink")
[Category:加利福尼亚州娱乐公司](../Category/加利福尼亚州娱乐公司.md "wikilink")

1.  [Universal-EMI Merger: A Timeline Of
    Events](http://www.billboard.com/biz/articles/news/1083872/universal-emi-merger-a-timeline-of-events)
2.  [新浪網 台灣經濟不景氣 環球音樂與上華音樂合併求發展](http://ent.sina.com.cn/p/i/25711.html)
3.  [张学友与经纪人闹掰
    个唱取消损失千万](http://ent.sina.com.cn/2015-01-14/doc-icesifvy3660273.shtml)
4.  [商台甘菁菁遭投訴偏幫環球
    黃劍濤夫妻關係掀叱咤餘波](http://ol.mingpao.com/cfm/Archive1.cfm?File=20080103/saa01/mba1h.txt)