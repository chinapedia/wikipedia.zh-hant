**宮城縣**（）是[日本東北部的一個縣](../Page/日本.md "wikilink")，東鄰[太平洋](../Page/太平洋.md "wikilink")，西面與[奧羽山脈為鄰](../Page/奧羽山脈.md "wikilink")。宮城縣人口佔了日本東北地方約4分之1，縣都位於東北的最大都市[仙台](../Page/仙台.md "wikilink")，約有45%人口居住。與[福島縣](../Page/福島縣.md "wikilink")、[山形縣](../Page/山形縣.md "wikilink")、[秋田縣及](../Page/秋田縣.md "wikilink")[岩手縣相互連接](../Page/岩手縣.md "wikilink")。

## 歷史

宮城縣在古時屬[陸奧國的一部分](../Page/陸奧國.md "wikilink")。1600年[伊達政宗於今日的](../Page/伊達政宗.md "wikilink")[仙台建造仙台城](../Page/仙台.md "wikilink")，構築仙台藩的基礎。

[明治時代](../Page/明治時代.md "wikilink")[廢藩置縣](../Page/廢藩置縣.md "wikilink")，1871年設立了仙台縣，後改稱宮城縣。

## 經濟

宮城縣以農業為主。由於對著[太平洋](../Page/太平洋.md "wikilink")，海產豐富，有[鰹魚](../Page/鰹魚.md "wikilink")、[秋刀魚](../Page/秋刀魚.md "wikilink")、[金槍魚和](../Page/金槍魚.md "wikilink")[鱈魚等等](../Page/鱈魚.md "wikilink")。

## 主要大学

  - 文部科學省管轄
      - 国立大学
          - [東北大學](../Page/東北大學_\(日本\).md "wikilink")
          - [宮城教育大学](../Page/宮城教育大学.md "wikilink")
      - 公立大学
          - 宮城大学
      - 私立大学
          - [東北福祉大学](../Page/東北福祉大学.md "wikilink")
          - [東北學院大學](../Page/東北學院大學.md "wikilink")
          - 石卷專修大学
          - 東北藥科大学
          - 中小企業大学校
          - 宮城学院女子大学
          - 尚絅学院大学
          - 仙台大学
          - 仙台白百合女子大学

<!-- end list -->

  - 獨立行政法人
      - 航空大學校 仙台分校

## 交通

### 鐵路

[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（[JR東日本](../Page/JR東日本.md "wikilink")）：東北新幹線、東北本線是縱貫宮城縣南北的交通骨幹。

  - [東北新幹線](../Page/東北新幹線.md "wikilink")
  - [東北本線](../Page/東北本線.md "wikilink")
  - [常磐線](../Page/常磐線.md "wikilink")
  - [仙山線](../Page/仙山線.md "wikilink")
  - [仙石線](../Page/仙石線.md "wikilink")
  - [石卷線](../Page/石卷線.md "wikilink")
  - [陸羽東線](../Page/陸羽東線.md "wikilink")
  - [氣仙沼線](../Page/氣仙沼線.md "wikilink")
  - [大船渡線](../Page/大船渡線.md "wikilink")

#### 公營交通路線

  - [仙台市交通局](../Page/仙台市交通局.md "wikilink")[地下鐵南北線](../Page/仙台市地下鐵南北線.md "wikilink")
  - [仙台市地下鐵東西線](../Page/仙台市地下鐵東西線.md "wikilink")

#### 第三部門鐵道

  - [阿武隈急行](../Page/阿武隈急行.md "wikilink")[阿武隈急行線](../Page/阿武隈急行線.md "wikilink")
  - [仙台機場鐵道](../Page/仙台機場鐵道.md "wikilink")[仙台機場線](../Page/仙台機場鐵道仙台機場線.md "wikilink")
  - [仙台臨海鐵道](../Page/仙台臨海鐵道.md "wikilink")（[貨物線](../Page/貨物線.md "wikilink")）
      - [臨海本線](../Page/仙台臨海鐵道臨海本線.md "wikilink")
      - [仙台埠頭線](../Page/仙台臨海鐵道仙台埠頭線.md "wikilink")
      - [仙台西港線](../Page/仙台臨海鐵道仙台西港線.md "wikilink")

### 航空

  - [仙台机场](../Page/仙台机场.md "wikilink")，為二級機場，有國際線及國內線航班在此起降。2011年3月11日，因芮氏9.0強震引起之海嘯而被沖毀，經整修後已恢復營運。

### 港口

  -   - [仙台港](../Page/仙台港.md "wikilink")
      - [石巻港](../Page/石巻港.md "wikilink")

## 特產

| 水果・蔬菜                              |
| ---------------------------------- |
| [米](../Page/米.md "wikilink")       |
| 魚介類・海產物                            |
| [魚翅](../Page/魚翅.md "wikilink")     |
| [牡蠣](../Page/牡蠣.md "wikilink")     |
| [海鞘](../Page/海鞘.md "wikilink")     |
| 鄉土料理                               |
| [白石温麵](../Page/白石温麵.md "wikilink") |
| はらこ飯                               |
| 笹[蒲鉾](../Page/蒲鉾.md "wikilink")    |
| [牛舌](../Page/牛舌.md "wikilink")     |
| ずんだ餅                               |
| 工藝品・民藝品                            |
| [鳴子漆器](../Page/鳴子漆器.md "wikilink") |
| [雄勝硯](../Page/雄勝硯.md "wikilink")   |
| 宮城傳統こけし                            |
| 參考資料：\[1\]                         |

## 出身名人

## 相關事件

  - 2011年3月11日日本當地時間下午2點46分﹝臺灣時間下午1點46分﹞左右發生強震，至3月13日[日本氣象廳發布為](../Page/日本氣象廳.md "wikilink")[芮氏規模](../Page/芮氏規模.md "wikilink")9.0\[2\]，而根據[美國地質調查局](../Page/美國地質調查局.md "wikilink")([USGS](../Page/USGS.md "wikilink"))所發布則為9.1\[3\]，震央在宮城縣外海的[三陸沖](../Page/三陸沖.md "wikilink")。如以日本氣象廳所發布的資料為準，則是自1900年後規模世界第四的強震。而后由于宫城县外福岛核电站爆炸导致大量核辐射入侵宫城，导致此灾难成为日本有史以来最严重的连环性灾难（地震+海啸+核爆炸）。

## 註解

## 外部連結

  - [宮城縣官方網站](http://www.pref.miyagi.jp/)
  - [宫城县观光联盟](http://japan.miyagi-kankou.or.jp.c.afh.hp.transer.com/)

[G宮](../Category/日本都道府縣.md "wikilink")
[\*](../Category/宮城縣.md "wikilink")

1.
2.  [「平成23年（2011年）東北地方太平洋沖地震」について（第16報）](http://www.jma.go.jp/jma/press/1103/13c/201103131830.html)
3.  [Magnitude 9.1 Near the East Coast of
    Japan](http://www.usgs.gov/blogs/features/2011/03/11/preliminary-magnitude-8-9-near-the-east-coast-of-japan/)