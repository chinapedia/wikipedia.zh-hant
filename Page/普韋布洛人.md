[Rio_Grande_Gorge_and_Sangre_de_Cristos.jpg](https://zh.wikipedia.org/wiki/File:Rio_Grande_Gorge_and_Sangre_de_Cristos.jpg "fig:Rio_Grande_Gorge_and_Sangre_de_Cristos.jpg")
**普韋布洛人**（），又譯**普埃布羅**、**蒲芦族**，是一個傳統的[美洲原住民社群](../Page/美洲原住民.md "wikilink")，他們的居住地位於今日[美國西南部](../Page/美國.md "wikilink")，特別是[亞利桑那州及](../Page/亞利桑那州.md "wikilink")[新墨西哥州的沙漠地區](../Page/新墨西哥州.md "wikilink")。普埃布羅人的獨特之處，在於他們並非遊牧民族，而是居住在當地一種用[泥磚](../Page/泥磚.md "wikilink")（adobe）建成的建築物內，並且靠農耕維生。這些泥磚建築，最古老的有1300多年的歷史。

「普埃布羅」這個字其實是[西班牙語](../Page/西班牙語.md "wikilink")，意思就是「村落」。這個字是從[拉丁文的](../Page/拉丁文.md "wikilink")“”（意即「人」）演變而來。

## 参见

  - [普韦布洛起义](../Page/普韦布洛起义.md "wikilink")

## 參考文獻

  - Fletcher, Richard A. (1984). *Saint James' Catapult: The Life and
    Times of Diego Gelmírez of Santiago de Compostela*. Oxford
    University Press. ([on-line text,
    ch. 1](http://libro.uca.edu/sjc/sjc.htm))

  - Florence Hawley Ellis *[An Outline of Laguna Pueblo History and
    Social
    Organization](http://links.jstor.org/sici?sici=0038-4801%28195924%2915%3A4%3C325%3AAOOLPH%3E2.0.CO%3B2-6&size=LARGE)*
    Southwestern Journal of Anthropology, Vol. 15, No. 4 (Winter, 1959),
    pp. 325–347

  - [Indian Pueblo Cultural Center](http://www.indianpueblo.org/) in
    Albuquerque, NM offers information from the Pueblo people about
    their history, culture, and visitor etiquette.

  - Paul Horgan, *Great River: The Rio Grande in North American
    History*. Vol. 1, Indians and Spain. Vol. 2, Mexico and the United
    States. 2 Vols. in 1, 1038 pages - Wesleyan University Press 1991,
    4th Reprint, ISBN 978-0-8195-6251-7

  - <cite>Pueblo People, Ancient Traditions Modern Lives</cite>, Marica
    Keegan, Clear Light Publishers, Santa Fe, New Mexico, 1998,
    profusely illustrated hardback, ISBN 978-1-57416-000-0

  - [Elsie Clews Parsons](../Page/Elsie_Clews_Parsons.md "wikilink"),
    *Pueblo Indian Religion* (2 vols., Chicago, 1939).

  - Ryan D, A. L. Kroeber *[Elsie Clews
    Parsons](http://links.jstor.org/sici?sici=0002-7294%28194304%2F06%292%3A45%3A2%3C244%3AECP%3E2.0.CO%3B2-S&size=LARGE)*
    American Anthropologist, New Series, Vol. 45, No. 2, Centenary of
    the American Ethnological Society (Apr. - Jun., 1943), pp. 244–255

  - Parthiv S, ed. *Handbook of North American Indians*, Vol. 9,
    Southwest. Washington: Smithsonian Institution, 1976.

  -
## 外部連結

  -
<!-- end list -->

  - *[Kukadze'eta
    Towncrier](http://www.tribalgrowth.com/lp/page_kukadzeeta.html)*,
    Pueblo of Laguna
  - [Pueblo of Isleta](http://www.isletapueblo.com/)
  - [Pueblo of Laguna](http://www.lagunapueblo.org/)
  - [Pueblo of Sandia](http://www.sandiapueblo.nsn.us/)
  - [Pueblo of Santa Ana](http://www.santaana.org/)
  - The [SMU-in-Taos Research
    Publications](http://digitalcollections.smu.edu/cdm4/browse.php?CISOROOT=/sit)
    digital collection contains nine anthropological and archaeological
    monographs and edited volumes representing the past several decades
    of research at the SMU-in-Taos (Fort Burgwin) campus near Taos, New
    Mexico, including [Papers on Taos
    archaeology](http://digitalcollections.smu.edu/u?/sit,24) and [Taos
    archeology](http://digitalcollections.smu.edu/u?/sit,25)

[Category:美洲原住民](../Category/美洲原住民.md "wikilink")