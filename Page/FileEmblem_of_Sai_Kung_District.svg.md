## 摘要

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>香港<a href="../Page/西貢區議會.md" title="wikilink">西貢區議會徽號</a><br />
非官方規格（<a href="../Page/User:Wrightbus.md" title="wikilink">User:Wrightbus版本</a>）</p>
<ul>
<li>區徽顏色：#00A89D（RGB）</li>
<li>每條線的寬度為10單位；</li>
<li>頂部兩條斜線呈水平向下傾斜30度，長度為50單位；</li>
<li>底部長方部份，外圍長度為70單位，高度40單位；內圍長度50單位，高度20單位，中間缺口部份長度40單位；</li>
<li>徽號頂點距離底部80單位；</li>
<li>中間兩條直線距離10單位，由頂方斜線一直伸延至距離底部長方內圍5單位。</li>
</ul></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>按照<a href="http://www.districtcouncils.gov.hk/archive/sk/chinese/welcome.htm">舊版網頁圖片</a>重新製作<br />
（<a href="http://www.districtcouncils.gov.hk/sk/tc/welcome.html">新版網頁圖片</a>採用了本頁重製版本）</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2008年4月20日 (SVG)</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>西貢區議會</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>合理使用</p></td>
</tr>
</tbody>
</table>

## 许可协议

[Category:香港區議會徽號](../Category/香港區議會徽號.md "wikilink")