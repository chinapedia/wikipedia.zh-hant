**張婯嬅**（），是位[臺灣的](../Page/臺灣.md "wikilink")[新聞主播](../Page/新聞主播.md "wikilink")，[高雄市人](../Page/高雄市.md "wikilink")\[1\]，[世新大學畢業](../Page/世新大學.md "wikilink")。現任[壹電視新聞台主播](../Page/壹電視新聞台.md "wikilink")、《[壹WALKER](../Page/壹WALKER.md "wikilink")》主持人兼製作人。

## 學歷

  - [世新大學新聞系](../Page/世新大學.md "wikilink")

## 經歷

  - [真相新聞網主播](../Page/真相新聞網.md "wikilink")
  - [民視新聞台主播](../Page/民視新聞台.md "wikilink")
  - [TVBS新聞台主播](../Page/TVBS新聞台.md "wikilink")
  - [壹電視新聞台主播](../Page/壹電視新聞台.md "wikilink")、主持人、製作人

## 曾主持節目

|                        |                                                    |                                      |                  |
| ---------------------- | -------------------------------------------------- | ------------------------------------ | ---------------- |
| **時間**                 | **頻道**                                             | **節目**                               | **備註**           |
| 時間待查                   | [TVBS](../Page/TVBS_\(頻道\).md "wikilink")          | 《[無線午報](../Page/無線午報.md "wikilink")》 | 主播               |
| 時間待查─2004年             | 《[無線午報—台語新聞](../Page/無線午報—台語新聞.md "wikilink")》     |                                      |                  |
| 時間待查─2004年             | 《[台語晚報](../Page/台語晚報.md "wikilink")》               |                                      |                  |
| 時間待查                   | [TVBS新聞台](../Page/TVBS新聞台.md "wikilink")           | 《整點新聞》                               | 主播（多主持除晚上外之整點新聞） |
| 時間待查                   | 《[一步一腳印 發現新台灣](../Page/一步一腳印_發現新台灣.md "wikilink")》 | 代班主持人                                |                  |
| 2010年12月28日—2013年9月16日 | [壹電視新聞台](../Page/壹電視新聞台.md "wikilink")             | 《台語新聞》                               | 主播               |
| 2011年9月1日—至今           | 《整點新聞》                                             |                                      |                  |
| 時間待查                   | 《台語第壹鮮》                                            | 主持人                                  |                  |
| 2014年1月4日—2014年4月5日    | 《[看見心台灣](../Page/看見心台灣.md "wikilink")》             |                                      |                  |
| 2016年4月2日—至今           | 《[壹WALKER](../Page/壹WALKER.md "wikilink")》         | 主持人兼製作人                              |                  |
| 2016年10月17日—至今         | 《壹電視晚間新聞》                                          | 週末主播                                 |                  |

## 注释

## 參考文獻

## 外部連結

  - [張婯嬅 TVBS官方網站](http://www.tvbs.com.tw/anchor/anchor_8.asp)

  -
  - [主播介紹](http://www.nexttv.com.tw/anchornews)

[Category:世界新聞專科學校校友](../Category/世界新聞專科學校校友.md "wikilink")
[Category:台灣記者](../Category/台灣記者.md "wikilink")
[Category:台灣電視主播](../Category/台灣電視主播.md "wikilink")
[Category:台灣新聞節目主持人](../Category/台灣新聞節目主持人.md "wikilink")
[Chang張](../Category/TVBS主播.md "wikilink")
[Chang張](../Category/壹電視主播.md "wikilink")
[Category:高雄市人](../Category/高雄市人.md "wikilink")
[L婯](../Category/張姓.md "wikilink")

1.