**國會開幕大典**（）是[英國通常在每年](../Page/英國.md "wikilink")5月或6月，於[上議院議事廳舉行的儀典](../Page/英國上議院.md "wikilink")。在儀典中，兩院議員會聚首一堂，恭聽君主致辭，標誌著新一屆[國會會期的正式開始](../Page/英國國會.md "wikilink")；至於遇著有國會[大選的年份](../Page/英國大選.md "wikilink")，有關儀典則會緊接在大選後的首次國會會議召開前進行。

現任英國國君[伊利沙伯二世自](../Page/伊利沙伯二世.md "wikilink")1952年登基以來，除了在1959年及1963年因分別懷有[安德魯王子及](../Page/安德魯王子_\(約克公爵\).md "wikilink")[愛德華王子而未克出席外](../Page/爱德华王子_\(威塞克斯伯爵\).md "wikilink")，歷年每屆的國會會期都是由她親自主持開幕。在那兩次伊利沙伯二世沒有出席的國會開幕大典中，一個以[坎特伯雷大主教為主席的](../Page/坎特伯雷大主教.md "wikilink")[上議院專員曾受託負責主持國會會期揭幕](../Page/上議院專員.md "wikilink")，而[女皇致辭則由](../Page/女皇致辭.md "wikilink")[大法官代行](../Page/大法官_\(英國\).md "wikilink")。

## 儀典程序

### 準備工作

國會開幕大典乃英國一項歷史悠久的傳統，至今仍留存不少繁複的儀式。在大典開始前，[皇室警衛會先巡察](../Page/皇室警衛.md "wikilink")[西敏宮國會大樓的地窖](../Page/西敏宮.md "wikilink")，以防止有人策動陰謀。這項傳統最早可上溯至1605年的[火藥陰謀](../Page/火藥陰謀.md "wikilink")，當年一群亡命的[天主教極端分子試圖在西敏宮地窖放置](../Page/天主教.md "wikilink")[炸藥](../Page/炸藥.md "wikilink")，密謀殺害信奉[新教的英皇](../Page/新教.md "wikilink")[詹姆士一世和他的大部份新教貴族](../Page/詹姆士一世.md "wikilink")。但陰謀最終被及時搗破而沒有實現，此後，巡察地窖就成為了每屆大典開始前指定的象徵式工作。

另一方面，君主在離開皇宮前往國會前，均須先要有一名下議院議員留在[白金漢宮](../Page/白金漢宮.md "wikilink")，以作為「禮節性人質」。這樣的做法，是因為傳統上，國會對國君往往懷有敵意，因此以一名下院議員作為「人質」，可用以保證君主的人身安全。在昔日，政府的管治班子主要由上院議員組成，「人質」由下院議員擔當還可以算是合理；但自從政府的管治班子由下院議員主導後，作為「女皇陛下政府」的一個部份，「人質」的角色便顯得混淆。不過，無論如何，時至今日，這種做法僅具有象徵性意義，而負責留守於皇宮的下院議員通常會是執政黨的其中一位[黨鞭](../Page/黨鞭.md "wikilink")，而當君主安全返回皇宮後，有關「人質」方可獲釋。

至於在君主抵達國會前，其[帝國皇冠](../Page/帝國皇冠.md "wikilink")（Imperial State
Crown）會由專用[馬車由](../Page/馬車.md "wikilink")[倫敦塔小心運送到西敏宮的維多利亞塔](../Page/倫敦塔.md "wikilink")，然後由[駁船長](../Page/駁船長.md "wikilink")（bargemaster）負責將之轉送予[宮務大臣的審計官](../Page/宮務大臣.md "wikilink")，以便把它與[堅忍之冕](../Page/堅忍之冕.md "wikilink")（Cap
of Maintenance）及國家寶劍一同存放於皇家畫廊內備用。

### 抵達國會

在每一次的國會會期開幕儀式中，[女皇都會乘坐馬車抵達國會](../Page/伊利沙伯二世.md "wikilink")[西敏宮](../Page/西敏宮.md "wikilink")（近年的馬車已加裝[空調](../Page/空調.md "wikilink")）\[1\]，並由[維多利亞塔的](../Page/維多利亞塔.md "wikilink")[皇家入口進入西敏宮內](../Page/皇家入口.md "wikilink")。由於西敏宮乃皇家宮殿，因此女皇甫抵達西敏宮，宮殿的屋頂會掛起[皇家旗](../Page/皇家旗_\(英國\).md "wikilink")（而非[英國國旗](../Page/英國國旗.md "wikilink")），直到她離開為止。步入皇家入口後，女皇與其隨從會行經皇家大梯、諾曼門廊（Norman
Porch），然後在禮服廳內穿上國家禮袍及帝國皇冠。女皇準備好後，她通常會在皇夫[愛丁堡公爵的陪同下](../Page/菲臘親王_\(愛丁堡公爵\).md "wikilink")，穿過皇家畫廊而抵達上議院議事廳。

國會開幕大典傳統上在上院，而非下院的議事廳內舉行。這最早可上溯到1642年，時任英皇[查理一世闖入下院議事廳](../Page/查理一世_\(英格蘭\).md "wikilink")，企圖逮捕五名下院議員，但下院議長卻公然抗命，並拒絕供出五人的藏身之所；經此事以後，迄今再沒有在位君主進入過下院議事廳。

女皇進入上院議事廳時，全院所有人士都要即時站立，待女皇登上御座後，再由她著令請「諸卿就坐如次」（My Lords, pray be
seated）。接著，女皇會命令[掌禮大臣去傳召下院議員](../Page/掌禮大臣.md "wikilink")。

### 傳召下院

[Palace_of_Westminster,_London_-_Feb_2007.jpg](https://zh.wikipedia.org/wiki/File:Palace_of_Westminster,_London_-_Feb_2007.jpg "fig:Palace_of_Westminster,_London_-_Feb_2007.jpg")乃[英國國會所在地](../Page/英國國會.md "wikilink")。\]\]
掌禮大臣接到指令後，隨即會舉起他的權杖，示意在上院議事廳大門前候命的[黑杖禮儀官可以傳召下院議員](../Page/黑杖禮儀官.md "wikilink")。在上院門衛及一名警察[督察的陪同下](../Page/督察.md "wikilink")，黑杖禮儀官會抵達下院議事廳的大門口。此時的下院議事廳大門會被加以緊閉，以象徵下院辯論之權利不容皇室代表干預。因此，要進入廳內，黑杖禮儀官須以其黑杖大力敲打大門三次，才獲准內進。進入下院議事廳後，黑杖禮儀官會趨前至廳內的欄柵，並先向下院議長鞠躬，再行前到廳內議事桌上的[公文箱旁](../Page/公文箱.md "wikilink")，向下院議員宣詔如下：

接到指示後，在手持[禮節性權杖的](../Page/禮節性權杖.md "wikilink")[警衛官陪同下](../Page/警衛官.md "wikilink")，下院議長會立即率領一眾下院議員啟程前往上院議事廳\[2\]。在議長後帶頭的，一般會是[首相與及](../Page/英國首相.md "wikilink")[反對黨領袖兩人](../Page/反對黨領袖_\(英國\).md "wikilink")，而他們後面的下院議員，亦會每兩人一組，一排一排的列隊前進。按照慣例，下院議員前往上院議事廳途中，無需保持嚴肅，而是以漫步的形式輕鬆前進，途中還可以隨便談笑。不過，甫抵達上院議事廳後，所有下院議員都須要保持絕對嚴肅，而且只可以在院內欄柵外的地方站立（與下院一樣，凡上院議事廳內正舉行會議，所有非上院成員均不可逾越廳內欄柵後的地方），站好後更要向女皇鞠躬。此後，在整個儀式中，所有下院議員都要站在欄柵外的地方。

### 女皇致辭

[House_of_Lords_Chamber.png](https://zh.wikipedia.org/wiki/File:House_of_Lords_Chamber.png "fig:House_of_Lords_Chamber.png")在[上議院議事廳宣讀](../Page/英國上議院.md "wikilink")。相中盡頭之處乃御座所在。\]\]
當所有人士齊集上院議事廳後，女皇就會發表一篇預先準備好的演講，稱為[御座致辭](../Page/:en:Speech_from_the_throne.md "wikilink")，又名「女皇致辭」。這篇演辭並非由女皇親撰，而是由[內閣所撰](../Page/英國內閣.md "wikilink")，內文勾劃出政府來年的施政大綱，亦講述來年在下院打算進行的立法工作。傳統上，演辭會寫在[羊皮紙上](../Page/羊皮紙.md "wikilink")，並由[大法官負責把演辭親手遞給坐在御座上的女皇](../Page/大法官_\(英國\).md "wikilink")；而女皇發表完畢後，大法官會再一次走到御座前，從女皇手上領回演辭。在昔日，為了表示尊敬，無論是遞交抑或是領回演辭的時候，大法官都要面向女皇，以倒後步行的方式從御座前的台階退下；不過，大法官現時已不再遵守這種傳統。

在發表演辭的時候，女皇都會盡量以一樣的聲線宣讀整篇演辭，以示其中立性，並寓意她沒有支持，亦沒有反對這篇演辭的內容。由於內閣乃[女皇陛下政府的一部份](../Page/女皇陛下政府.md "wikilink")，因此她在宣讀演辭時會一概使用「我的政府」等字眼，而在交代完來年所有主要的立法工作後，女皇會按慣例自行加入「相應措施亦將一應於諸位前呈上」（other
measures will be laid before
you）一句，以示讓政府有更多空間在未來引入其他演辭中沒有交代的草案。至於演辭完結時，女皇一般會作結如下：

女皇致辭完畢後，一眾下院議員就會向女皇鞠躬，然後退回下院議事廳。按照傳統，上、下兩院的議員在女皇致辭時，都必須要肅靜恭聽，並不容許任何交談或發聲。唯一例外是在1998年的時候，正當女皇在演辭中宣告政府正計劃在來年廢除[世襲貴族自動成為上院議員之權利時](../Page/世襲貴族.md "wikilink")，突然有極少數的[工黨議員高呼](../Page/英國工黨.md "wikilink")「對」和「聽、聽」等口號，隨即有數名在場貴族喊叫「不是」和「可恥」以作回應。在事件中，女皇沒有因此而被打斷發言，亦沒有作出任何停頓，而事後有關起事者則被朝野及輿論大肆抨擊，指他們有失體統，而且對女皇十分無禮。

值得一提的是，兩院議員並非強制出席國會開幕大典，他們可選擇不出席典禮。在近代國會歷史上，個別國會議員（例如具有[新芬黨黨席的議員和認為皇室制度應被廢除的議員](../Page/新芬黨.md "wikilink")）就選擇杯葛開幕大典中的御座致辭部份。

### 致謝辯論

女皇離開後，上、下兩院議員會各自返回自己所屬的議事廳，就女皇的致辭進行辯論，並就「回應女皇陛下的美辭演說」（Address in Reply
to Her Majesty's Gracious
Speech）。但在這以前，兩院均須先通過一條形式性的草案，以象徵他們辯論時沒有受到君主-{干}-預或影響。在上院，這個草案叫《[教區委員會草案](../Page/教區委員會草案.md "wikilink")》，在下院則叫《[被剝奪公民權人士草案](../Page/被剝奪公民權人士草案.md "wikilink")》。這些草案沒有實質作用，僅僅用以象徵新一屆會期的辯論開始，所以這些草案亦不會有[二讀等等的程序](../Page/二讀.md "wikilink")。草案引入後，兩院便會就御座致辭的內容作出辯論，有關辯論一般會持續數日，每日更會就致辭入面的不同部份，比如外交、財政等事務加以辯論。由於致辭由內閣所撰，因此從辯論中，可以大抵得出兩院對政府來年施政方針所持的看法。完成辯論後，兩院可就御坐致辭進行致謝投票，有關投票等同對政府的[信任投票](../Page/不信任動議.md "wikilink")；假如投票不通過，政府就要垮台。

### 選出議長

每次[大選以後的國會開幕大典結束](../Page/英國大選.md "wikilink")，緊接內閣成員完成宣誓後，[下院就會選出或重選議長](../Page/英國下議院.md "wikilink")。議長產生後，議長會按照17世紀初留下的習俗，「不情願地」被拉上（dragged
unwillingly）議長席位。這是因為在舊日，議長隨時會觸怒國君，有性命不保之虞，被選出的議長往往不願意登位，當時的議長是被拉上議長席的，結果就留下了這個習俗。當然，時至今日，這些做法已僅餘下象徵意義，而下院議長一位更已成為了極受尊崇的身份。

## 其他地方

在其他比如[加拿大和](../Page/加拿大.md "wikilink")[澳洲等實行兩院制的](../Page/澳洲.md "wikilink")[英聯邦王國國會](../Page/英聯邦王國.md "wikilink")，也會有類似的儀式，而女皇甚至曾親身到當地國會發表[御座致辭](../Page/御座致辭.md "wikilink")。不過，一般而言，女皇很少有機會親身前往當地，所以致辭往往都是由當地總督代行。

至於在[印度](../Page/印度.md "wikilink")，該國[總統亦會在國會發表類似的致辭](../Page/印度總統.md "wikilink")，而其他[英聯邦成員國](../Page/英聯邦.md "wikilink")，例如[馬爾他](../Page/馬爾他.md "wikilink")、[毛里裘斯](../Page/毛里裘斯.md "wikilink")、[马来西亚和](../Page/马来西亚.md "wikilink")[新加坡等也有類似的做法](../Page/新加坡.md "wikilink")。

在非英聯邦國家中，其實不少的[國家元首都有相似的致辭](../Page/國家元首.md "wikilink")。例如在[美國](../Page/美國.md "wikilink")，總統每年都要到[眾議院發表](../Page/美國眾議院.md "wikilink")[國情諮文](../Page/國情諮文.md "wikilink")，而其前殖民地[菲律賓至今也有類似的習慣](../Page/菲律賓.md "wikilink")。在[荷蘭](../Page/荷蘭.md "wikilink")，當地每年9月的第三個星期二均會在國會舉行類似的大典，當日更被定名為[王儲日](../Page/王儲日.md "wikilink")。

## 相關條目

  - [女皇致辭](../Page/女皇致辭.md "wikilink")
  - [英國國會](../Page/英國國會.md "wikilink")
  - [英國上議院](../Page/英國上議院.md "wikilink")
  - [英國下議院](../Page/英國下議院.md "wikilink")
  - [掌禮大臣](../Page/掌禮大臣.md "wikilink")

## 參考資料

<references />

<div class="references-small">

  - Assinder, Nick, "[Queen's speech rebel owns
    up](http://news.bbc.co.uk/1/hi/uk_politics/221845.stm)", *BBC News -
    UK Politics*, November 25, 1998.
  - Heard, Andrew, *[Constitutional Conventions and
    Parliament](http://www.parl.gc.ca/Infoparl/28/2/28n2_05e_Heard.pdf)*,
    CANADIAN PARLIAMENTARY REVIEW, Summer 2005.
  - Wilson, Robert, *The House of Parliament*, Great Britain: Jarrold
    Publishing, 1994, lastest reprint 2004.
  - "[Factsheet M7 - Members
    Series](https://web.archive.org/web/20090325012443/http://www.parliament.uk/documents/upload/M07.pdf)",
    *Parliamentary Elections*, London: House of Commons Information
    Office, Revised August 2007.
  - "[PARLIAMENT OPENS
    TODAY](http://www.aph.gov.au/house/news/news_stories/news_firstsit08.htm)",
    *House News*, House Liaison and Projects Officeof the Parliament of
    Australia, 12 February 2008.
  - "[State Opening of
    Parliament](https://web.archive.org/web/20071004121312/http://www.parliament.uk/about/how/occasions/stateopening.cfm)",
    *How Parliament Works*, The UK Parliament, Updated 12/12/2007.
  - *[THE HOUSE OF LORDS BRIEFING - STATE OPENING OF
    PARLIAMENT](https://web.archive.org/web/20080409233017/http://www.parliament.uk/documents/upload/HofLstateopening.pdf)*,
    UK: House of Lords, 2007.

</div>

## 外部連結

  - [1988年起歷年國會開幕大典](http://www.c-span.org/search/?sdate=&edate=&searchtype=Videos&sort=Most+Recent+Airing&text=0&all%5b%5d=state&all%5b%5d=opening&all%5b%5d=of&all%5b%5d=parliament&locationid%5b%5d=9232)在[C-SPAN](../Page/C-SPAN.md "wikilink")
  - [國會開幕大典](http://www.parliament.uk/about/how/occasions/stateopening/)，英國國會
      - [2006年度國會開幕大典支出](http://www.publications.parliament.uk/pa/cm200607/cmhansrd/cm061128/text/61128w0003.htm#061128100000456)
      - [2010年5月25日國會開幕大典的報導](http://www.parliament.uk/business/news/2010/05/state-opening-25-may-2010/)
      - [2010年5月25日的國會開幕大典](http://www.parliamentlive.tv/Main/Player.aspx?meetingId=6321)
  - [1958年國會開幕大典片段](http://www.youtube.com/watch?v=drWIHqnFG3I)
  - [1998年御坐致辭片段](http://news.bbc.co.uk/player/nol/newsid_6710000/newsid_6710100/6710121.stm?bw=nb&mp=wm&news=1&bbcws=1)，BBC
  - [2007年御坐致辭片段](http://news.bbc.co.uk/player/nol/newsid_7080000/newsid_7080900/7080921.stm?bw=nb&mp=wm&news=1&nol_storyid=7080921&bbcws=1)，BBC
  - [2016年國會開幕大典片段](https://www.youtube.com/watch?v=E0UcjSHd7mo)

[Category:英國國會](../Category/英國國會.md "wikilink")
[Category:11月節日](../Category/11月節日.md "wikilink")
[Category:国家元首演讲](../Category/国家元首演讲.md "wikilink")
[Category:开幕式](../Category/开幕式.md "wikilink")

1.  在2017年的國會開幕大典時，女王並沒有乘坐馬車，而是改成坐禮車。詳可見BBC的全程實況https://www.youtube.com/watch?v=Xk_pi_nq838
2.  近兩年來有些議員會在此時發表一些口號，如2016年工黨下議員Dennis Skinner就很喊了Hands off the
    BBC，並引發一陣騷動，詳可見英國國會收錄的影像https://www.youtube.com/watch?v=E0UcjSHd7mo