**彼得·查爾斯·阿齊博爾德·愛華特·詹寧斯**（**Peter Charles Archibald Ewart
Jennings**，），[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）著名新聞主播，與[CBS的](../Page/CBS.md "wikilink")[丹·拉瑟](../Page/丹·拉瑟.md "wikilink")（Dan
Rather）及
[NBC的](../Page/NBC.md "wikilink")[湯姆·布洛考](../Page/湯姆·布洛考.md "wikilink")（Tom
Brokaw）同列為美國三大新播主播。詹寧斯於2005年4月7日宣布患上肺癌，決心與癌症搏鬥，可惜治療令聲綫變得沙啞，無法在幕前，只可為節目提供意見，最後於2005年8月7日在[美國](../Page/美國.md "wikilink")[紐約家中去世](../Page/紐約.md "wikilink")。

詹寧斯生於加拿大[多倫多](../Page/多倫多.md "wikilink")，父親查爾斯是加拿大著名電視新聞從業員。他九歲曾主持[渥太華](../Page/渥太華.md "wikilink")[CBC電台兒童節目](../Page/加拿大廣播公司.md "wikilink")「"Peter's
People"」，但在其學術生涯卻終身未曾獲得大學畢業資格。詹寧斯1964年赴美採訪民主黨全國代表大會，獲賞識進入[美國廣播公司工作](../Page/美國廣播公司.md "wikilink")，入行早期曾任駐中東記者七年，1972年詹寧斯直擊報道以色列選手在[慕尼黑奧運遭恐怖分子挾持的事件](../Page/慕尼黑奧運.md "wikilink")，並於1978年在巴黎訪問伊朗革命領袖[霍梅尼](../Page/霍梅尼.md "wikilink")，都使他的名氣大增。1983年9月5日詹寧斯接掌[美國廣播公司黃金時段的](../Page/美國廣播公司.md "wikilink")《[晚間世界新聞](../Page/ABC世界新聞.md "wikilink")》節目，一直到2005年4月1日，這期間他主播的新聞經常是美國收視率最高的晚間新聞。

詹寧斯採訪過許多重大新聞，包括1960年代[柏林圍牆建立及](../Page/柏林圍牆.md "wikilink")[越戰](../Page/越戰.md "wikilink")，到其後柏林圍牆倒塌、[冷戰結束等](../Page/冷戰.md "wikilink")，他都親身見證報道。在2001年9月11日的「[九一一](../Page/九一一.md "wikilink")」事件發生時，他更不休息地轉播六十個小時。他在自己的事業生涯中共拿下16個[艾美獎以及多個新聞獎項](../Page/艾美獎.md "wikilink")。

## 外部链接

  -
  -
  - [ABC News: Peter Jennings](http://abcnews.go.com/WNT/PeterJennings/)

[Category:美国记者](../Category/美国记者.md "wikilink")
[Category:美國電視主播](../Category/美國電視主播.md "wikilink")
[Category:罹患肺癌逝世者](../Category/罹患肺癌逝世者.md "wikilink")
[Category:移民美國的加拿大人](../Category/移民美國的加拿大人.md "wikilink")