}} | coor_pinpoint = | coordinates_footnotes = | subdivision_type =
[Country](../Page/List_of_sovereign_states.md "wikilink") |
subdivision_name =  | parts_type = [Largest
cities](../Page/List_of_cities_in_Ukraine_by_population.md "wikilink") |
parts_style = para | p1 = [Cherkasy](../Page/Cherkasy.md "wikilink"),
[Uman](../Page/Uman.md "wikilink"),
[Smila](../Page/Smila,_Ukraine.md "wikilink"),
[Zolotonosha](../Page/Zolotonosha.md "wikilink") | established_title =
{{\#if:January 7, 1954|Established}} | established_date = January 7,
1954 | seat_type = [Administrative
center](../Page/Administrative_centre.md "wikilink") | seat =
[Cherkasy](../Page/Cherkasy.md "wikilink") | leader_party = [Petro
Poroshenko Bloc
"Solidarity"](../Page/Petro_Poroshenko_Bloc_"Solidarity".md "wikilink")\[1\]
| leader_title =
[Governor](../Page/Governor_of_Cherkasy_Oblast.md "wikilink") |
leader_name = [Oleksandr
Velbivets](../Page/Oleksandr_Velbivets.md "wikilink")\[2\] |
leader_title1 = [Oblast
council](../Page/Cherkasy_Oblast_Council.md "wikilink") | leader_name1
= {{\#if:76|76 seats}} | leader_title2 = {{\#if:Valentyna
Kovalenko|Chairperson}} | leader_name2 = {{\#if:Valentyna
Kovalenko|Valentyna Kovalenko {{\#if:|()}}}} | unit_pref = Metric |
area_footnotes = | area_total_km2 = 20900 | area_land_km2 = |
area_water_km2 = | area_water_percent = {{\#if:|% }} | area_rank =
{{\#if:18th|[Ranked
18th](../Page/List_of_Ukrainian_oblasts_and_territories_by_area.md "wikilink")}}
| elevation_footnotes = | elevation_m = | population_footnotes = |
population_total = 1231207 | population_rank = {{\#if:15th|[Ranked
15th](../Page/List_of_Ukrainian_oblasts_and_territories_by_population.md "wikilink")}}
| population_as_of = 2017 | population_blank1_title = Annual growth
| population_blank1 = {{\#if:|% }} | population_density_km2 = auto |
demographics_type1 =
{{\#if:[Ukrainian](../Page/Ukrainian_language.md "wikilink")817|Demographics}}
| demographics1_footnotes = | demographics1_title1 =  |
demographics1_info1 =
[Ukrainian](../Page/Ukrainian_language.md "wikilink") |
demographics1_title2 = [Average
salary](../Page/List_of_Ukrainian_oblasts_and_territories_by_salary.md "wikilink")
| demographics1_info2 =
{{\#if:817|[UAH](../Page/Ukrainian_hryvnia.md "wikilink") 817
{{\#if:2006|<small>(2006)</small>}} }} | demographics1_title3 = Salary
growth | demographics1_info3 = {{\#if:|%}} | blank_name_sec1 =
[Raions](../Page/Raions_of_Ukraine.md "wikilink") | blank_info_sec1 =
20 | blank1_name_sec1 =
[Cities](../Page/List_of_cities_in_Ukraine_by_subdivision#Cherkasy_Oblast.md "wikilink")
<small>(total)</small> | blank1_info_sec1 = 25 | blank2_name_sec1 =
{{\#if:25|• }}[Regional
cities](../Page/City_of_regional_significance_\(Ukraine\).md "wikilink")
| blank2_info_sec1 = 6 | blank3_name_sec1 =
\[\[List_of_urban-type_settlements_in_Ukraine_by_subdivision\#Cherkasy_Oblast|
**切爾卡瑟州**（）是[烏克蘭中部的一個州](../Page/烏克蘭.md "wikilink")。[第聶伯河流經](../Page/第聶伯河.md "wikilink")，把全州分為東（平原）西（丘陵）不均等的兩半。面積20,900平方公里，人口1,367,685（2005年）。首府[切爾卡瑟](../Page/切爾卡瑟.md "wikilink")。

## 參考文獻

[Category:乌克兰州份](../Category/乌克兰州份.md "wikilink")
[切爾卡瑟州](../Category/切爾卡瑟州.md "wikilink")

1.
2.  [Poroshenko appoints new governor in Cherkasy
    region](https://www.unian.info/m/politics/10344795-poroshenko-appoints-new-governor-in-cherkasy-region.html),
    [UNIAN](../Page/UNIAN.md "wikilink") (20 November 2018)