**谢晓亮**
（，），为[美国知名](../Page/美国.md "wikilink")[化学家](../Page/化学家.md "wikilink")、[生物物理学家](../Page/生物物理学.md "wikilink")。现任[北京大学李兆基讲席](../Page/北京大学.md "wikilink")[教授](../Page/教授.md "wikilink")。

## 简历

出生于化学世家，其父[谢有畅教授为中国知名](../Page/谢有畅.md "wikilink")[物理化学家和](../Page/物理化学.md "wikilink")[化学工程专家](../Page/化学工程.md "wikilink")，与其母[杨骏英教授均长期任教于](../Page/杨骏英.md "wikilink")[北京大学化学系](../Page/北京大学.md "wikilink")（现[化学与分子工程学院](../Page/北京大学化学与分子工程学院.md "wikilink")）。1962年生于[北京](../Page/北京.md "wikilink")。1984年毕业于[北京大学](../Page/北京大学.md "wikilink")，获得化学[理学](../Page/理学.md "wikilink")[学士](../Page/学士.md "wikilink")[学位](../Page/学位.md "wikilink")。赴美国留学，就读于[加州大学圣地牙哥分校](../Page/加州大学圣地牙哥分校.md "wikilink")，师从物理化学家约翰·西蒙教授（Prof.
John D. Simon），并于1990年获得[博士学位](../Page/博士.md "wikilink")。

1990年-1992年，在[芝加哥大学做博士后研究](../Page/芝加哥大学.md "wikilink")，与著名激光光谱学家格雷厄姆·佛莱明教授（）
共事。

1992年-1998年，就职于[美国能源部所属](../Page/美国能源部.md "wikilink")[西北太平洋国家实验室](../Page/西北太平洋国家实验室.md "wikilink")（）之环境分子科学实验室（）从研究员晋升为Chief
Scientist。

1999年，成为哈佛大学化学及化学生物学系教授。

谢晓亮教授是单分子酶学（Single Molecule Enzymology）的奠基人之一，大幅发展改良了单分子荧光显微镜（Single
Molecule Microscopy）技术。谢晓亮教授对 （CARS Microscopy
相干反斯托克斯拉曼散射显微镜）以及stimulated Raman
scattering microscopy（SRS Microscopy
受激拉曼散射显微镜），做出了创造性的巨大贡献，并观察到许多重要结果。其实验室侧重单分子酶学，生物大分子变构机制，单分子动力学方面的研究。

## 荣誉

  - 2015年，美国化学学会的物理化学\[1\]
  - 2014年，美国光学学会会士
  - 2013年，[國立衛生研究院 (美國)先锋奖](../Page/國立衛生研究院_\(美國\).md "wikilink")
  - 2012年，[美国微生物学会会士](../Page/美国微生物学会.md "wikilink")
  - 2011年，[美国科学院](../Page/美国科学院.md "wikilink")[院士](../Page/院士.md "wikilink")
  - 2010年，中国化学会荣誉会士
  - 2009年，[美国能源部劳伦斯奖](../Page/美国能源部.md "wikilink")（化学类）
  - 2008年， for Applied Laser Technology
    （[德国雷宾赫应用激光技术奖](../Page/德国.md "wikilink")）\[2\]
  - 2008年，[美国文理科学院院士](../Page/美国文理科学院.md "wikilink")
  - 2007年，Willis E. Lamb Award for Laser Science and Quantum Optics
    （威利斯·兰姆奖，激光科学和量子光学，纪念诺奖得主激光科技先驱兰姆）
  - 2006年，[美国科学促进会会士](../Page/美国科学促进会.md "wikilink")
  - 2006年，生物物理学会（国际，）会士
  - 2004年，[國立衛生研究院 (美國)先锋奖](../Page/國立衛生研究院_\(美國\).md "wikilink")\[3\]

## 参考资料

## 参考

  - [谢晓亮教授主页](http://bernstein.harvard.edu/pages/AboutProfXie.html)
  - [威利斯·兰姆奖 得主介绍](http://www.lambmedal.org/2007/2007-xie.html)
  - [谢晓亮教授荣获雷宾赫应用激光技术奖](http://blogs.physicstoday.org/wht/2008/05/sunney_xie_to_receive_leibinge.html)

[Xie](../Category/美国生物化学家.md "wikilink")
[Xie](../Category/哈佛大学教授.md "wikilink")
[Xie](../Category/美国文理科学院院士.md "wikilink")
[Xie](../Category/长江学者讲座教授.md "wikilink")
[Xie](../Category/美国科学促进会会员.md "wikilink")
[Xie](../Category/美国国家科学院院士.md "wikilink")
[Xie](../Category/北京大学校友.md "wikilink")
[Xie](../Category/北京大学教授.md "wikilink")
[Xie](../Category/芝加哥大学校友.md "wikilink")
[Xie](../Category/聖地牙哥加州大學校友.md "wikilink")
[Xie](../Category/归化美国公民的中华人民共和国人.md "wikilink")
[Xie](../Category/北京人.md "wikilink")
[X晓](../Category/谢姓.md "wikilink")
[X](../Category/光谱学家.md "wikilink")
[Xie](../Category/美国国家医学院院士.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:美籍华裔科学家](../Category/美籍华裔科学家.md "wikilink")

1.  [Peter Debye
    Award](https://www.acs.org/content/acs/en/funding-and-awards/awards/national/bytopic/peter-debye-award-in-physical-chemistry.html)
2.  [Berthold Leibinger
    Zukunftspreis](http://www.leibinger-stiftung.de/de/foerderaktivitaeten/laser-forschungs-innovationspreis/zukunftspreis.html)
3.  [NIH Director's Pioneer
    Award 2004](https://commonfund.nih.gov/pioneer/Recipients04)