**失蹤的以色列十支派**是指古代[以色列人](../Page/以色列人.md "wikilink")[十二支派](../Page/以色列十二支派.md "wikilink")（或稱[氏族](../Page/氏族.md "wikilink")）中失去蹤跡的10支。這10個支派屬於[北国以色列](../Page/以色列王国_\(后期\).md "wikilink")，在[北国以色列被](../Page/以色列王国_\(后期\).md "wikilink")[亞述摧毀](../Page/亞述.md "wikilink")（約公元前722年）以後，便消失於[聖經的記載](../Page/聖經.md "wikilink")。这十个支派是：[流便支派](../Page/流便.md "wikilink")、[西缅支派](../Page/西缅.md "wikilink")、[但支派](../Page/但.md "wikilink")、[拿弗他利支派](../Page/拿弗他利.md "wikilink")、[迦得支派](../Page/迦得.md "wikilink")、[亚设支派](../Page/亚设.md "wikilink")、[以萨迦支派](../Page/以萨迦.md "wikilink")、[西布伦支派](../Page/西布伦.md "wikilink")、[以法莲支派](../Page/以法莲.md "wikilink")、[玛拿西支派](../Page/玛拿西.md "wikilink")。剩下[猶大支派](../Page/猶大_\(創世記\).md "wikilink")、[便雅憫支派](../Page/便雅悯.md "wikilink")，及部份[利未支派](../Page/利未.md "wikilink")。

## 十二支派

十個失蹤的支派的由來，可以參考列王記下17章6節：「何細亞第九年亞述王攻取了撒瑪利亞，將以色列人擄到亞述，把他們安置在哈臘與歌散的哈博河邊，並瑪代人的城邑。」

根據希伯來聖經，[雅各](../Page/雅各_\(舊約聖經\).md "wikilink")（就是後來被神命名為以色列；創35:10）與二個妻子(利亞、拉結)及二個妾侍(悉帕、辟拉)生下十二個兒子。後來十二個兒子發展為[以色列十二支派](../Page/以色列十二支派.md "wikilink")。

  - 在約書亞的時代，十二支派分攤以色列的土地，其中因為利未支派被神揀選作祭司職分，所以沒有給他們分地。(書13:33,
    14:3)不過，利未支派卻在各支派中分得城邑，其中六個作為以色列的逃城，由利未人管理。逃城分別位於約旦河兩側。還有42個城(包城外四圍的郊野)，總數48個城，全部歸利未支派所有。
  - 雅各提升以法蓮和瑪拿西的子孫(約瑟的兒子)加入成為十二支派，平分他們父親約瑟的產業。兩個「半支派」都各自分地為業，在曠野四十年都有自己的營地。

根據聖經，以色列國（或稱北國）本是以色列王國的一部分。由於北面的以色列支派拒絕所羅門的兒子羅波安作他們的王，在公元前931年後分裂成為北國。

### 新約

有些證據表明，在幾個世紀以後，仍有屬於失蹤十支派的以色列人存在。例如，在新約聖經[路加福音](../Page/路加福音.md "wikilink")2章36節提到屬於[亞設的以色列人](../Page/亚设.md "wikilink")。「有一個女先知安娜，是亞設支派法努埃的女兒。她已經上了年紀，是出嫁以後與丈夫生活了七年的，」

### 次經/偽經

## 宗教觀點

先知說：*「人子啊，你要取一根木杖，在其上寫『為猶大和他的同伴以色列人』；又取一根木杖，在其上寫『為約瑟，就是為以法蓮，又為他的同伴以色列全家』。你要使這兩根木杖接連為一，在你手中成為一根。*[以西結書](../Page/以西結書.md "wikilink")37:16-17

### 猶太教

在猶太法典[塔木德議論著十個失蹤的支派是否最終將與](../Page/塔木德.md "wikilink")[猶大支派](../Page/犹大支派.md "wikilink")，也就是猶太民族重聚。

### 基督宗教

## 民族學與人類學

## 宣稱是失蹤的以色列十支派後裔的族群

### 以色列之子（Bene Israel）

### 瑪拿西之子（Bnei Menashe）

### [贝塔以色列](../Page/贝塔以色列.md "wikilink")

### [伊博猶太](../Page/伊博族.md "wikilink")（Igbo Jews）

### 阿富汗的普什图人和[巴基斯坦宗教](../Page/巴基斯坦.md "wikilink")

[普什圖人相信自己是其中一支](../Page/普什圖人.md "wikilink")。

## 對其他種族的猜測

### [斯基泰人](../Page/斯基泰人.md "wikilink")/[辛梅里安人理論](../Page/辛梅里安人.md "wikilink")

### [美洲原住民](../Page/美洲原住民.md "wikilink")

### [日本人](../Page/日猶同祖論.md "wikilink")

### 倫巴人（Lemba）

### [不列颠猶太主義](../Page/英国.md "wikilink")

### Brit-Am組織

### 中国[羌族](../Page/羌族.md "wikilink")

### 其它組織

## 請求

## 參見

  - [撒馬利亞人](../Page/撒馬利亞人.md "wikilink")

## 参考文献

### Documentary

## 外部連結

  - [Biblical
    History](http://www.dinur.org/resources/resourceCategoryDisplay.aspx?categoryID=411&rsid=478)
    The Jewish History Resource Center—Project of the Dinur Center for
    Research in Jewish History, The Hebrew University of Jerusalem
  - [Brit Am Israel](http://www.britam.org)
  - [What happened to the 10 lost tribes? video feature direct from
    Jerusalem](http://www.aish.com/literacy/jewishhistory/Video_The_10_Lost_Tribes.asp)
  - [Covering Several Claims of Lost Tribes of
    Israel](http://www.pbs.org/wgbh/nova/israel/losttribes.html) - PBS
    website, re Nova episode
  - [United Israel](http://www.unitedisrael.org/) Lost Tribes Research

{{-}}

[T](../Category/以色列十二支派.md "wikilink")
[L](../Category/猶太歷史.md "wikilink")
[T](../Category/傳說.md "wikilink")
[T](../Category/歷史懸案.md "wikilink")