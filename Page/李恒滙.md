**李恆滙**（**Li Hang
Wui**，1985年2月15日－），生於[香港](../Page/香港.md "wikilink")，已退役[香港足球運動員](../Page/香港.md "wikilink")，司職[後衛](../Page/後衛_\(足球\).md "wikilink")，持有[亞洲足協B級教練牌照](../Page/亞洲足球協會.md "wikilink")，於2012年6月2日與女朋友結婚，現時擔任[香港超級聯賽球會](../Page/香港超級聯賽.md "wikilink")[和富大埔助教](../Page/大埔足球會.md "wikilink")。

## 球員生涯

李恆滙生於[香港](../Page/香港.md "wikilink")，出身於[流浪青年軍](../Page/香港流浪足球會.md "wikilink")，於2002年加盟[香港08首度在](../Page/香港08.md "wikilink")[甲組亮相](../Page/香港甲組足球聯賽.md "wikilink")，在2004年轉投[公民直到](../Page/公民足球隊.md "wikilink")2006年再轉會加盟[傑志](../Page/傑志體育會.md "wikilink")，惟未幾便被外借至[港會一季後被](../Page/香港足球會.md "wikilink")[傑志收回](../Page/傑志體育會.md "wikilink")，於2010年再度被外借予[大中](../Page/大中足球會.md "wikilink")，於2011年轉會加盟[晨曦](../Page/晨曦體育會.md "wikilink")。李恒滙曾經擔任[香港奧運隊隊長](../Page/香港奧運足球代表隊.md "wikilink")，於2010年首次入選[香港足球代表隊出戰](../Page/香港足球代表隊.md "wikilink")[省港盃](../Page/省港盃.md "wikilink")。

## 國際賽生涯

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>Date</p></th>
<th><p>Venue</p></th>
<th><p>Opponent</p></th>
<th><p>Result</p></th>
<th><p>Goals</p></th>
<th><p>Competition</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>2010年10月4日</p></td>
<td><p><a href="../Page/印度.md" title="wikilink">印度</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>0</p></td>
<td><p>國際友誼賽</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>2011年2月9日</p></td>
<td><p><a href="../Page/馬來西亞.md" title="wikilink">馬來西亞</a></p></td>
<td></td>
<td><p>0–2</p></td>
<td><p>0</p></td>
<td><p>國際友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2011年9月30日</p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/國家體育場.md" title="wikilink">國家體育場</a></p></td>
<td></td>
<td><p>3–3</p></td>
<td><p>0</p></td>
<td><p><a href="../Page/2011年龍騰盃國際足球邀請賽.md" title="wikilink">2011年龍騰盃</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2011年10月2日</p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/國家體育場.md" title="wikilink">國家體育場</a></p></td>
<td></td>
<td><p>5–1</p></td>
<td><p>0</p></td>
<td><p><a href="../Page/2011年龍騰盃國際足球邀請賽.md" title="wikilink">2011年龍騰盃</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2011年10月4日</p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/國家體育場.md" title="wikilink">國家體育場</a></p></td>
<td></td>
<td><p>6–0</p></td>
<td><p>0</p></td>
<td><p><a href="../Page/2011年龍騰盃國際足球邀請賽.md" title="wikilink">2011年龍騰盃</a></p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [香港足球總會網頁球員註冊資料](https://www.hkfa.com/ch/club/4/detail?player=164)

[Category:李姓](../Category/李姓.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:港超球員](../Category/港超球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:香港08球員](../Category/香港08球員.md "wikilink")
[Category:公民球員](../Category/公民球員.md "wikilink")
[Category:傑志球員](../Category/傑志球員.md "wikilink")
[Category:港會球員](../Category/港會球員.md "wikilink")
[Category:大中球員](../Category/大中球員.md "wikilink")
[Category:晨曦球員](../Category/晨曦球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")