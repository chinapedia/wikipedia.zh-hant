**西恩·比格斯塔夫**（[英语](../Page/英语.md "wikilink")：****，），[苏格兰电影](../Page/苏格兰.md "wikilink")、电视剧、舞台剧和广播剧演员，曾参演《[哈利波特：神秘的魔法石](../Page/哈利波特：神秘的魔法石_\(電影\).md "wikilink")》、《[哈利波特：消失的密室](../Page/哈利波特：消失的密室_\(電影\).md "wikilink")》、《[超市夜未眠](../Page/超市夜未眠.md "wikilink")》等电影，《[夜惊情](../Page/夜惊情.md "wikilink")》等电视剧。

## 电影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>电影名称</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/冬天的访客.md" title="wikilink">冬天的访客</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/哈利波特：神秘的魔法石_(電影).md" title="wikilink">哈利波特：神秘的魔法石</a>》</p></td>
<td><p><a href="../Page/哈利波特角色列表#學生.md" title="wikilink">奥利佛·木透</a></p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/哈利波特：消失的密室_(電影).md" title="wikilink">哈利波特：消失的密室</a>》</p></td>
<td><p><a href="../Page/哈利波特角色列表#學生.md" title="wikilink">奥利佛·木透</a></p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/超市夜未眠.md" title="wikilink">超市夜未眠</a>》（短篇）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/CHEM087.md" title="wikilink">CHEM087</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/超市夜未眠.md" title="wikilink">超市夜未眠</a>》（长篇）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/摇摆嬉皮士.md" title="wikilink">摇摆嬉皮士</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/哈利波特：死神的聖物Ⅱ.md" title="wikilink">哈利波特：死神的聖物Ⅱ</a>》</p></td>
<td><p><a href="../Page/哈利波特角色列表#學生.md" title="wikilink">奥利佛·木透</a></p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>《》</p></td>
<td><p>伯斯維爾伯爵四世詹姆士·赫伯恩</p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>《》</p></td>
<td><p>Sergeant Odd</p></td>
</tr>
</tbody>
</table>

## 外部链接

  - [西恩·比格斯塔夫官方网站](https://web.archive.org/web/20031214070054/http://www.seanbiggerstaff.com/)

  -
[Category:英国电影演员](../Category/英国电影演员.md "wikilink")
[Category:英国舞台演员](../Category/英国舞台演员.md "wikilink")
[Category:英国电视演员](../Category/英国电视演员.md "wikilink")