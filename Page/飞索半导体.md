**飞索半导体**是美国一家以NOR[闪存为主要产品的美国](../Page/闪存.md "wikilink")[半导体技术公司](../Page/半导体.md "wikilink")。总部位于[美国](../Page/美国.md "wikilink")[加州](../Page/加州.md "wikilink")[森尼韦尔](../Page/森尼韦尔.md "wikilink")。

## 历史

1993年，[AMD和](../Page/AMD.md "wikilink")[富士通将其各自的闪存部门合并成为飞索半导体的前身FASL](../Page/富士通.md "wikilink")。[AMD和](../Page/AMD.md "wikilink")[富士通各自持股](../Page/富士通.md "wikilink")50％。

2003年，[AMD股份增加到](../Page/AMD.md "wikilink")60%。

2005年，在NASDAQ上市成功。

2007年，收購以色列 Saifun 半導體。

2013年，收購日本[富士通微控制器與類比事業部門](../Page/富士通.md "wikilink")。

2014年12月1日賽普拉斯半導體（Cypress
Semiconductor）和飛索半導體已經達成了最終協議，將通過一項價值約為40億美元的全股票免稅交易進行合併。新公司名稱仍為賽普拉斯半導體，總部將設於加州聖荷西。

2016年7月8日[賽普拉斯半導體Cypress](../Page/賽普拉斯半導體.md "wikilink")
Semiconductor以5.5億美元收購[博通旗下物聯網業務](../Page/博通.md "wikilink") Wi-Fi、藍牙和
Zigbee 無線技術

## 相關條目

### 母公司

  - [AMD](../Page/AMD.md "wikilink")
  - [富士通](../Page/富士通.md "wikilink")

### 競爭公司

  - [旺宏電子](../Page/旺宏電子.md "wikilink")

## 外部連結

  - [飛索半導體官方網站](http://www.spansion.com/)
  - [飛索半導體官方部落格](https://archive.is/20130630083528/http://blog.spansion.com/)

[Category:美國電子公司](../Category/美國電子公司.md "wikilink")
[Category:紐約證券交易所上市公司](../Category/紐約證券交易所上市公司.md "wikilink")
[S](../Category/森尼韋爾公司.md "wikilink")