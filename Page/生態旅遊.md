[Fernando_noronha.jpg](https://zh.wikipedia.org/wiki/File:Fernando_noronha.jpg "fig:Fernando_noronha.jpg")[費爾南多](../Page/費爾南多.md "wikilink")[迪諾羅尼亞群島](../Page/費爾南多·迪諾羅尼亞群島.md "wikilink")\]\]
[Rhinos_leofleck.jpg](https://zh.wikipedia.org/wiki/File:Rhinos_leofleck.jpg "fig:Rhinos_leofleck.jpg")的[奇特旺国家公园](../Page/奇特旺国家公园.md "wikilink")\]\]
[GnusAndZebrasInMaraMasai.jpg](https://zh.wikipedia.org/wiki/File:GnusAndZebrasInMaraMasai.jpg "fig:GnusAndZebrasInMaraMasai.jpg")馬賽馬拉公園保護區內的[斑馬群](../Page/斑馬.md "wikilink")\]\]

**生態旅遊**或稱**生態觀光**（ecotourism）一詞最早出現可追溯至1965年，學者[赫茲特建議對](../Page/赫茲特.md "wikilink")[文化](../Page/文化.md "wikilink")、[教育以及](../Page/教育.md "wikilink")[旅遊再省思](../Page/旅遊.md "wikilink")，並倡導所謂的生態旅遊，發展至今生態旅遊已成國際保育和永續發展之基礎概念。1983年，學者[赫克特提出](../Page/赫克特.md "wikilink")「生態旅遊」一詞，成為目前最普遍使用的語彙。其將生態旅遊歸結出三大特點：

1.  生態旅遊是一種仰賴當地資源的旅遊；
2.  生態旅遊是一種強調當地資源保育的旅遊；
3.  生態旅遊是一種維護當地社區概念的旅遊。

最後，學者赫克特將生態旅遊定義為「到相對未受干擾或未受污染的自然區域旅行，有特定的研究主題，且體驗或欣賞其中的野生動植物景象，並且關心區內的文化特色」。
[生態旅遊學會](../Page/生態旅遊學會.md "wikilink") (The Ecotourism Society)
在1991年為生態旅遊下了一個廣為各界接受的註解：「生態旅遊是一種具有環境責任感的旅遊方式，保育自然環境與延續當地住民福祉為發展生態旅遊的最終目標」

## 無污染旅行者

1.  使用最少污染的[交通工具](../Page/交通工具.md "wikilink")
2.  盡量少用過份[食品加工及包裝的](../Page/食品加工.md "wikilink")[食品](../Page/食品.md "wikilink")[飲品](../Page/飲品.md "wikilink")
3.  盡量不用用完即棄的物品
4.  帶走一切帶去的物品，絕不留下[垃圾](../Page/垃圾.md "wikilink")
5.  盡量不生火
6.  不干擾[野生生物](../Page/野生生物.md "wikilink")，不[捕獵或採摘](../Page/捕獵.md "wikilink")[植物](../Page/植物.md "wikilink")
7.  不干擾當地居民的生活
8.  保持寧靜，盡量不使用[唱機或](../Page/唱機.md "wikilink")[揚聲器](../Page/揚聲器.md "wikilink")
9.  盡量少帶[高科技產品](../Page/高科技.md "wikilink")
10. 遵守一切[郊野公園管理當局的指示](../Page/郊野公園.md "wikilink")

## 參見

  - [生物多樣性](../Page/生物多樣性.md "wikilink")
  - [保育生物學](../Page/保育生物學.md "wikilink")
  - [保育倫理](../Page/保育倫理.md "wikilink")
  - [保育運動](../Page/保育運動.md "wikilink")
  - [地球科學](../Page/地球科學.md "wikilink")
  - [生態系統](../Page/生態系統.md "wikilink")
  - [環境保護](../Page/環境保護.md "wikilink")
  - [自然环境](../Page/自然环境.md "wikilink")
  - [自然资源](../Page/自然资源.md "wikilink")
  - [自然](../Page/自然.md "wikilink")
  - [可持續發展](../Page/可持續發展.md "wikilink")
  - [可持續性](../Page/可持續性.md "wikilink")
  - [生态社区主义](../Page/生态社区主义.md "wikilink")
  - [生态村](../Page/生态村.md "wikilink")

[Category:生態學](../Category/生態學.md "wikilink")
[Category:环境保护](../Category/环境保护.md "wikilink")
[Category:旅遊類型](../Category/旅遊類型.md "wikilink")
[Category:生態旅遊](../Category/生態旅遊.md "wikilink")