**露得清**（[英語](../Page/英語.md "wikilink")：**Neutrogena**）是一個[美國的保養品](../Page/美國.md "wikilink")[品牌](../Page/品牌.md "wikilink")，產品包含[臉部保養](../Page/臉部.md "wikilink")、護髮、護膚等產品。露得清原為獨立公司，1994年被[强生集團併購](../Page/强生.md "wikilink")，成為該集團的子公司。

露得清的[產品在許多](../Page/產品.md "wikilink")[國家皆有販售](../Page/國家.md "wikilink")，同時也因為在各地邀請許多名人代言，使得該[品牌在](../Page/品牌.md "wikilink")[國際之間頗有知名度](../Page/國際.md "wikilink")。

## 歷史

1954年，推出第一块纯净、温和的透明洁面皂。清洁后肌肤即刻回复正常PH值
1967年，开始在皮肤科医学界受到关注和推崇
1997年，研制出对亚洲肌肤的护肤品，首次引进维他命A酸
2002年，从植物中提取有效成分，应用于祛痘护肤系列
2003年，从植物中提取天然有效成分，应用于祛痘护肤系列
2004年，露得清正式进入中国
2008年，将“Helioplex”应用于防晒产品，效果倍加持久稳定
2009年，创造天然亲肤结构，帮助肌肤持久水润
2010年，全球首创电动洁仪魔力振现引领3G洁面新潮流
高端美白一活采皙白系列，全新上市，亲眼见证护肤奇迹
2011年，直击30层肌肤老化问题，全新凝时赋活系列突破上市
2013年，Neutrogena® Fine Fairness™系列細白晶透光能面膜機

## 產品

  - 抗[粉刺產品](../Page/粉刺.md "wikilink")
  - [身體清潔產品](../Page/身體.md "wikilink")
  - 身體潤膚產品
  - [臉部清潔產品](../Page/臉.md "wikilink")
  - [臉部潤膚產品](../Page/臉部.md "wikilink")
  - [護髮產品](../Page/護髮.md "wikilink")
  - [男性](../Page/男性.md "wikilink")[肌膚保養產品](../Page/皮膚.md "wikilink")
  - [防曬產品](../Page/防曬.md "wikilink")

## 参考文献

## 外部連結

  - [露得清台灣官方網站](http://www.neutrogena.com.tw/)

  - [Clean & Clear (露得清關係品牌)網站](http://www.cleanandclear.com)

  - [Johnson & Johnson
    企業年表](https://web.archive.org/web/20061031034832/http://www.jnj.com/our_company/timeline/1990.htm)

  -
{{-}}

[Category:清洁用品品牌](../Category/清洁用品品牌.md "wikilink")
[Category:個人護理用品](../Category/個人護理用品.md "wikilink")
[Category:强生品牌](../Category/强生品牌.md "wikilink")
[Category:1930年成立的公司](../Category/1930年成立的公司.md "wikilink")