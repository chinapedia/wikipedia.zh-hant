[运动](../Page/運動_\(物理學\).md "wikilink") |list2 =

  - [热力学](../Page/热力学.md "wikilink")
  - [力学](../Page/力学.md "wikilink")
      - [经典力学](../Page/经典力学.md "wikilink")
          - [弹道](../Page/弹道学.md "wikilink")
          - [拉格朗日](../Page/拉格朗日力学.md "wikilink")
          - [哈密顿](../Page/哈密顿力学.md "wikilink")
      - [连续介质](../Page/连续介质力学.md "wikilink")
      - [天体](../Page/天体力学.md "wikilink")
      - [统计](../Page/统计力学.md "wikilink")
      - [流体](../Page/流体力学.md "wikilink")
      - [量子](../Page/量子力学.md "wikilink")

|group3 =
[波](../Page/波.md "wikilink"){{,}}[场](../Page/场_\(物理\).md "wikilink")
|list3 =

  - [引力](../Page/引力.md "wikilink")
  - [电磁学](../Page/电磁学.md "wikilink")
      - [经典](../Page/经典电磁学.md "wikilink")
  - [量子场论](../Page/量子场论.md "wikilink")
  - [相对论](../Page/相对论.md "wikilink")
      - [狭义相对论](../Page/狭义相对论.md "wikilink")
      - [广义相对论](../Page/广义相对论.md "wikilink")

|group4 = 按专业 |list4 =

  - [數碼物理學](../Page/數碼物理學.md "wikilink")
  - [计算物理学](../Page/计算物理学.md "wikilink")
  - [粒子物理學](../Page/粒子物理學.md "wikilink")
  - [原子核物理学](../Page/原子核物理学.md "wikilink")
      - [等离子体](../Page/等离子体物理学.md "wikilink")
      - [高分子](../Page/高分子物理学.md "wikilink")
      - [统计](../Page/统计物理学.md "wikilink")
  - [原子分子與光物理學](../Page/原子分子與光物理學.md "wikilink")
      - [分子](../Page/分子物理学.md "wikilink")
      - [原子](../Page/原子物理学.md "wikilink")
  - [声学](../Page/声学.md "wikilink")
  - [凝聚体物理学](../Page/凝聚体物理学.md "wikilink")
      - [固体](../Page/固体物理学.md "wikilink")
      - [介观](../Page/介观物理学.md "wikilink")
      - [软](../Page/软物质.md "wikilink")
  - [光学](../Page/光学.md "wikilink")
      - [几何](../Page/几何光学.md "wikilink")
      - [物理](../Page/物理光学.md "wikilink")
      - [非线性](../Page/非线性光学.md "wikilink")
      - [量子](../Page/量子光学.md "wikilink")
  - [天体物理学](../Page/天体物理学.md "wikilink")
      - [核](../Page/核天体物理学.md "wikilink")
      - [恒星](../Page/恒星物理学.md "wikilink")
      - [天体粒子](../Page/天体粒子物理学.md "wikilink")
      - [太阳](../Page/太阳物理学.md "wikilink")
      - [空间](../Page/空间物理学.md "wikilink")

|group5 = 生命科学中的物理学 |list5 =

  - [生物物理学](../Page/生物物理学.md "wikilink")
      - [生物力学](../Page/生物力学.md "wikilink")
  - [醫學物理](../Page/醫學物理.md "wikilink")
      -
      - [核医学](../Page/核医学.md "wikilink")

      - [医学影像](../Page/医学影像.md "wikilink")

      - [心理物理学](../Page/心理物理学.md "wikilink")

|group6 = [交叉学科](../Page/交叉学科.md "wikilink") |list6 =

  - [数学物理](../Page/数学物理.md "wikilink")

  - [非线性物理学](../Page/非线性物理学.md "wikilink")

  -
  - [材料科学](../Page/材料科学.md "wikilink")

  - [化学](../Page/化学物理学.md "wikilink")

  - [物理宇宙学](../Page/物理宇宙学.md "wikilink")

  - [大气](../Page/大气物理学.md "wikilink")

      - [云](../Page/云物理学.md "wikilink")

  - [地球物理学](../Page/地球物理学.md "wikilink")

      -
  -
|belowclass = hlist |below =

  - **[分类](../Category/物理学.md "wikilink")**

  - **[主題](../Page/Portal:物理学.md "wikilink")**

  - **[共享资源](../Page/commons:Category:Physics.md "wikilink")**

  - **[专题](../Page/Wikipedia:物理学专题.md "wikilink")**

}}<noinclude>

</noinclude>

[\*](../Category/物理学分支.md "wikilink")
[Main](../Category/物理学分支导航模板.md "wikilink")