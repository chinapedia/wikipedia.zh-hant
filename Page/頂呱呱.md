**頂呱呱**（[英語](../Page/英語.md "wikilink")：TKK International
Inc.）是一間[台灣的](../Page/台灣.md "wikilink")[速食](../Page/速食.md "wikilink")[連鎖餐廳](../Page/連鎖店.md "wikilink")，於1974年7月20日於[西門町成立第一家門市](../Page/西門町.md "wikilink")，以[炸雞類餐點為主](../Page/炸雞.md "wikilink")。截至2019年1月，頂呱呱在台灣共有70間餐廳、於中國大陸上海有1間分店、美國紐約有1間分店。

頂呱呱是台湾速食店始祖，創辦人[史桂丁原為公務員](../Page/史桂丁.md "wikilink")，後與人合夥養[雞](../Page/雞.md "wikilink")。在[日本看到肯德基炸雞](../Page/日本.md "wikilink")，便決定仿效其模式，經營炸雞速食店生意，並採用小門面的外帶式櫃台。其事業曾經盛極一時，直到正統[美國速食業龍頭](../Page/美國.md "wikilink")[麥當勞](../Page/麥當勞.md "wikilink")、[肯德基等正式進入台灣為止](../Page/肯德基.md "wikilink")。史桂丁有三男一女，除二子[史洪法外皆陸續移居美國](../Page/史洪法.md "wikilink")，其頂呱呱事業後由史洪法接手。史洪法為整頓經營現況，近年來開展多品牌展店策略，透過自創、同業結盟、[投資等方式陸續涉足](../Page/投資.md "wikilink")[拉麵](../Page/拉麵.md "wikilink")、[日式料理](../Page/日式料理.md "wikilink")、[韓式料理等其他餐飲事業](../Page/韓式料理.md "wikilink")。2017年展開頂呱呱海外門市的拓展。

## 歷史

1974年，頂呱呱由史桂丁創辦。史桂丁曾為公務員，之後轉行成為養雞業者。為了讓所飼養的雞隻能有穩定銷量，因而決定開設販售[炸雞餐點的速食餐廳](../Page/炸雞.md "wikilink")。第一間頂呱呱餐廳於1974年7月20日在[台北市](../Page/台北市.md "wikilink")[西門町開幕](../Page/西門町.md "wikilink")。

1983年，頂呱呱在[美國](../Page/美國.md "wikilink")[舊金山設立第一間海外門市](../Page/舊金山.md "wikilink")，包括[洛杉磯等城市在內陸續共開設四間門市](../Page/洛杉磯.md "wikilink")。

1986年，頂呱呱在台灣已有10間餐廳。隨後頂呱呱持續擴張，至1993年時已有30間分店\[1\]。

1999年，史桂丁逝世，由二子史洪法接掌經營頂呱呱。

2007年，開設台南店，位於台南市東寧路，為[南台灣首店](../Page/南台灣.md "wikilink")。

2014年，開設MR.TKK炸雞鬆餅咖啡廳，門市定期舉辦小小店長及公益活動，以感恩的心回饋在地台灣人。

2015年3月，與[台灣虎航合作創造頂呱呱飛機餐AIR](../Page/台灣虎航.md "wikilink")
TKK，讓台灣及外國朋友可以在飛航途中享受來自台灣的炸雞。

2015年11月，開設韓式居食屋UNCLES(魷魚大叔)[辣炒年糕專賣店](../Page/辣炒年糕.md "wikilink")。UNCLES在韓國擁有約20家門市，頂呱呱為台灣總代理。

2016年2月，開設潮流[韓式拌飯盒BOBBYBOX](../Page/韓式拌飯.md "wikilink")。BOBBYBOX在韓國擁有約30家門市，菲律賓也有兩家門市，頂呱呱為台灣總代理。

2016年5月，於[台北東區開設與魷魚大叔老闆共同創作的TKK](../Page/台北東區.md "wikilink")
CAMP露營風[韓式烤肉店](../Page/韓式烤肉.md "wikilink")（現已改名為BURIYA露營瘋烤肉），把韓國最流行的露營風烤肉介紹給台灣的年輕消費者。醬料及菜單研發由韓國最大醬料商大象公司指導研發。

2016年7月，與日本SAPPORO製麵簽訂旗下品牌[東京油組總本店代理權](../Page/東京油組總本店.md "wikilink")，頂呱呱為台灣總代理。

2017年2月，於台北東區開設東京油組總本店海外第一家分店 台北敦南組。

2017年9月，於台北華山文創園區附近開設東京油組總本店台灣第二間分店 台北華山組。

2017年12月，於中國大陸上海開設頂呱呱第一家海外門市頂呱呱上海一號店。

2017年12月，美國功夫茶為頂呱呱集團在美國地區的策略聯盟夥伴，頂呱呱與功夫茶將針對美國市場一同進行品牌、全球行銷與加盟業務等全面性的合作。美國功夫茶創立於2009年紐約，目前全美國擁有130間分店遍及28州並拓展國際市場至澳洲和越南等地。相信在雙方品牌的合作下，頂呱呱能成功拓展美國市場並深根茁壯。

2018年5月，於台北ATT 4 FUN開設SHELL OUT，為馬來西亞吉隆坡的人氣手抓海鮮品牌。

2018年底，於美國紐約開設第一家美國門市。

## 分店據點

至2019年1月時，頂呱呱在國內外共開設了70間分店，分布於[基隆市](../Page/基隆市.md "wikilink")、[台北市](../Page/台北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")、[台中市](../Page/台中市.md "wikilink")、[台南市](../Page/台南市.md "wikilink")、[彰化縣](../Page/彰化縣.md "wikilink")、[高雄市和](../Page/高雄市.md "wikilink")[屏東市](../Page/屏東市.md "wikilink")。除了傳統的速食餐廳型態外，也開設了名為「BURIYA」、「MR.TKK」、「TKK
Buffet 頂呱呱自助吧」、「UNCLES TAIWAN」、「BOBBYBOX TAIWAN」、「東京油組總本店」、「SHELL
OUT」的副品牌據點\[2\]\[3\]。

## 參見

  - [台灣速食店列表](../Page/台灣速食店列表.md "wikilink")

## 參考資料

  - 葉卉軒.[台北燈節開幕後
    柯P直奔頂呱呱用悠遊卡買「呱呱包」](https://udn.com/news/story/7238/3650351).聯合新聞網.2019-02-18
  - 張芳瑜.[搶速食界之先
    頂呱呱第70家門市落腳微風南山](https://udn.com/news/story/7270/3584339).聯合新聞網.2019-01-09
  - 葉卉軒.[本土炸雞名店頂呱呱出招
    搶佔滴雞精市場紅海](https://udn.com/news/story/7241/3467985).聯合新聞網.2018-11-08
  - 王筱君.[頂呱呱年收將破10億　明年全台插旗](https://www.mirrormedia.mg/story/20180802bus002/).鏡傳媒.2018-08-02
  - silvia.[馬來西亞手抓海鮮Shell
    Out台灣一號店開幕，生猛海鮮辛辣調味涮嘴到停不下來](https://www.vogue.com.tw/feature/foods/content-41128.html).VOGUE.2018-06-20
  - 沈俐萱.[東京拉麵又登台！「沒有湯的拉麵」2/15落腳台北東區](https://news.tvbs.com.tw/travel/705626).TVBS.2017-01-08
  - [不用去野外也能烤肉
    ？頂呱呱炸雞引進韓國露營風餐廳](http://travel.ettoday.net/article/699708.htm).ETtoday旅遊雲.2016-05-17
  - 李芳瑜.[不可思議！竟能邊吹冷氣邊露營？！](http://news.ltn.com.tw/news/life/breakingnews/1724693).自由時報電子報.2016-06-09
  - 陳乙杉.[速食飽和？頂呱呱引2韓品牌　轉型年輕化](http://news.tvbs.com.tw/life/news-644172/).TVBS.2016-03-13
  - [全台都瘋Buffet！頂呱呱改平價自助吧炸雞無限供應](http://travel.ettoday.net/article/629559.htm).ETtoday旅遊雲.2016-01-12

## 外部連結

  - [頂呱呱](http://www.tkkinc.com.tw/)

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
[Category:速食餐廳](../Category/速食餐廳.md "wikilink")
[Category:1974年建立](../Category/1974年建立.md "wikilink")
[Category:總部在台灣的跨國公司](../Category/總部在台灣的跨國公司.md "wikilink")
[Category:總部位於新北市五股區的工商業機構](../Category/總部位於新北市五股區的工商業機構.md "wikilink")

1.  [關於頂呱呱](https://www.tkkinc.com.tw/about.html)
2.
3.