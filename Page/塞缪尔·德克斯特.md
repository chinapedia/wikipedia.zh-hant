**塞缪尔·德克斯特**（**Samuel
Dexter**，），[美国政治家](../Page/美国.md "wikilink")，[联邦党成员](../Page/联邦党.md "wikilink")，曾任[美国众议员](../Page/美国众议员.md "wikilink")（1793年-1795年）、[美国参议员](../Page/美国参议员.md "wikilink")（1799年-1800年）、[美国战争部长](../Page/美国战争部长.md "wikilink")（1800年-1801年）和[美国财政部长](../Page/美国财政部长.md "wikilink")（1801年）。

## 外部链接

  - [Samuel
    Dexter](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=6304729)

[D](../Category/美国财政部长.md "wikilink")
[D](../Category/美国战争部长.md "wikilink")
[D](../Category/哈佛大學校友.md "wikilink")
[Category:大波士顿人士](../Category/大波士顿人士.md "wikilink")
[Category:麻薩諸塞州聯邦參議員](../Category/麻薩諸塞州聯邦參議員.md "wikilink")
[Category:麻薩諸塞州聯邦眾議員](../Category/麻薩諸塞州聯邦眾議員.md "wikilink")
[Category:麻薩諸塞州參議員](../Category/麻薩諸塞州參議員.md "wikilink")
[Category:美國聯邦黨聯邦參議員](../Category/美國聯邦黨聯邦參議員.md "wikilink")
[Category:美国禁酒活动家](../Category/美国禁酒活动家.md "wikilink")