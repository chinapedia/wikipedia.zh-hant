**米洛·久卡諾維奇**
（，），曾任[蒙特內哥羅共和國](../Page/蒙特內哥羅.md "wikilink")[總理](../Page/蒙特內哥羅總理.md "wikilink")，屬於[蒙特內哥羅社會主義者民主黨](../Page/蒙特內哥羅社會主義者民主黨.md "wikilink")（Democratic
Party of Socialists of Montenegro）。

## 政治生涯

他曾經在1991年－1998年、2003年－2006年和2008年到2010年擔任蒙特內哥羅總理。原先與[塞爾維亞總統](../Page/塞爾維亞總統.md "wikilink")[米洛塞維奇關係密切](../Page/米洛塞維奇.md "wikilink")。1996年，久卡諾維奇政府宣佈和[米洛塞維奇掌權的](../Page/米洛塞維奇.md "wikilink")[塞爾維亞斷絕關係](../Page/塞爾維亞.md "wikilink")。之後久卡諾維奇即支持蒙特內哥羅[獨立於](../Page/獨立.md "wikilink")[南斯拉夫聯邦共和國](../Page/南斯拉夫聯邦共和國.md "wikilink")。

2003年1月8日久卡諾維奇再度成為蒙特內哥羅總理。

2006年5月21日蒙特內哥羅舉行[公民投票](../Page/2006年黑山獨立公民投票.md "wikilink")，獨派以55.5%的微弱優勢險勝，6月3日，蒙特內哥羅國會正式宣佈獨立，蒙特內哥羅因而成為歐洲最新的獨立國家，並且恢復其在[第一次世界大戰之前的獨立地位](../Page/第一次世界大戰.md "wikilink")。

久卡諾維奇過去25年，利用憲法漏洞，多次出任總統及總理職位，大權在握，他的家族及政府高層官員多次被指控貪污，令民眾不滿情緒升溫。\[1\]

## 參考來源

<references/>

[D](../Category/黑山总统.md "wikilink")
[D](../Category/蒙特內哥羅總理.md "wikilink")
[Category:黑山总理](../Category/黑山总理.md "wikilink")
[Category:現任國家領導人](../Category/現任國家領導人.md "wikilink")

1.  [黑山反政府示威演變成警民衝突](http://news.now.com/home/international/player?newsId=93197)2014年02月16日