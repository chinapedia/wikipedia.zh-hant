**漢摩拉比**（或譯**-{zh-hans:汉謨拉比;zh-hant:漢摩拉比;zh-cn:汉摩拉比;zh-hk:漢謨拉比;zh-sg:汉摩拉比;zh-tw:漢謨拉比;zh-mo:漢謨拉比;}-**、**漢穆拉比**、**哈慕拉比**等；[阿卡德語](../Page/阿卡德語.md "wikilink")，演變自[亞摩利語](../Page/亞摩利語.md "wikilink")
** <small>（*ˤAmmu*「父系的親人」與
*Rāpi*「治療者」）</small>；[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****），是[阿摩利人的](../Page/阿摩利人.md "wikilink")[巴比伦王国的第六任](../Page/巴比倫.md "wikilink")[國王](../Page/國王.md "wikilink")（約西元前1792年－前1750年<small>（中年表）</small>或前1728年－前1686年<small>（短年表）</small>在位）。在一連串戰爭中，他擊敗鄰國，將巴比倫的統治區域擴展至整个[两河流域](../Page/两河流域.md "wikilink")（[美索不達米亞](../Page/美索不達米亞.md "wikilink")），从而成為[巴比倫尼亞帝國的第一任國王](../Page/巴比倫尼亞.md "wikilink")\[1\]。儘管他的帝國掌控了整個兩河流域，他的繼承者卻無力保持他的偉業，帝國最終走向衰亡。

漢摩拉比以制定了《[漢摩拉比法典](../Page/漢摩拉比法典.md "wikilink")》而聞名，在現代被譽為古代立法者。漢摩拉比法典是[-{zh-hans:信史;
zh-hant:歷史時代;}-的第一部成文](../Page/信史.md "wikilink")[民法典之一](../Page/民法典.md "wikilink")，刻寫於超過2.4公尺（8英尺）高、於1901年出土的石碑上，核心論點是[報復主義](../Page/報復.md "wikilink")。

## 历史

### 背景

[Hammurabi's_Babylonia_1.svg](https://zh.wikipedia.org/wiki/File:Hammurabi's_Babylonia_1.svg "fig:Hammurabi's_Babylonia_1.svg")领土对比\]\]

汉谟拉比是[巴比伦](../Page/巴比伦.md "wikilink")[城邦](../Page/城邦.md "wikilink")[第一王朝](../Page/巴比伦第一王朝.md "wikilink")（又称[古巴比伦王国](../Page/古巴比伦.md "wikilink")）的第六位[国王](../Page/国王.md "wikilink")，于大约西元前1792年继承其父王[辛-穆巴利特的权力](../Page/辛-穆巴利特.md "wikilink")\[2\]。巴比伦是在[美索不达米亚平原星罗棋布的众多古代城邦的一员](../Page/美索不达米亚平原.md "wikilink")，这些城邦为了争夺富饶的[农业用地互相之间经常开战](../Page/农业.md "wikilink")\[3\]。尽管古代两河流域有很多文明同时存在，但在[中东](../Page/中东.md "wikilink")[具有读写能力的人心目中巴比伦文明具有突出地位](../Page/识字率.md "wikilink")\[4\]。汉谟拉比之前的巴比伦诸王已经开始着手统一两河流域中部，征服了[博尔西帕](../Page/博尔西帕.md "wikilink")、[基什和](../Page/基什.md "wikilink")[西帕尔等城邦](../Page/西帕尔.md "wikilink")，在该地区树立起巴比伦的[霸权](../Page/霸权.md "wikilink")\[5\]。但汉谟拉比之前的巴比伦仍然是一个微不足道的小城邦，因此汉谟拉比即位时，巴比伦所处的[地缘政治情况十分复杂](../Page/地缘政治.md "wikilink")：强大的[埃什努纳王国控制着](../Page/埃什努纳.md "wikilink")[底格里斯河上游](../Page/底格里斯河.md "wikilink")，[拉尔萨控制着两河流域三角洲](../Page/拉尔萨.md "wikilink")，在东方有[埃兰王国](../Page/埃兰.md "wikilink")，在北方[亚述国王](../Page/亚述.md "wikilink")[沙姆希-阿达德一世虎视眈眈](../Page/沙姆希-阿达德一世.md "wikilink")\[6\]，只是由于他不合时宜的死亡才使得他新建的[闪族帝国土崩瓦解](../Page/闪族.md "wikilink")，亚述对巴比伦的威胁得以消除\[7\]。

### 统治

汉谟拉比在位前二十年该地区相对和平。他大兴土木，例如为了防御外敌而筑高城墙，还扩建庙宇\[8\]。大约前1766年时，强大的[埃兰王国为了控制穿越](../Page/埃兰.md "wikilink")[扎格罗斯山脉的重要](../Page/扎格罗斯山脉.md "wikilink")[商路而入侵美索不达米亚平原](../Page/贸易.md "wikilink")\[9\]。埃兰与平原上的其他城邦联盟，袭击并摧毁了[埃什努纳和其他很多城邦](../Page/埃什努纳.md "wikilink")，从而第一次将势力范围扩展到该地区\[10\]。为了巩固自己在该地区的统治，埃兰试图挑起汉谟拉比统治下的巴比伦王国和[拉尔萨王国之间的战争以坐收渔利](../Page/拉尔萨.md "wikilink")\[11\]。汉谟拉比和拉尔萨国王获悉埃兰的离间图谋后结成联盟，粉碎了埃兰人的进攻，但拉尔萨在战斗中并没有出多大力\[12\]。汉谟拉比愤怒于拉尔萨没有帮助自己，转而于前1763年南下征服拉尔萨，从而统一了两河流域下游\[13\]。

当汉谟拉比在他的北方盟友的帮助下在南方作战时，缺少士兵的北方陷入动荡\[14\]。汉谟拉比乘机挥师北上，平息了北方的动乱，并占领了埃什努纳\[15\]。接着巴比伦军队征服了北方的其他城邦，包括它的老盟友[马里也被兵不血刃地攻下](../Page/马里_\(叙利亚\).md "wikilink")\[16\]\[17\]\[18\]。经过几年的征伐，汉谟拉比统一了两河流域的大部（前1758年\[19\]）\[20\]。该地区的重要城邦中，只有西部[叙利亚的](../Page/叙利亚.md "wikilink")[阿勒颇和](../Page/阿勒颇.md "wikilink")[卡特纳依然保持独立](../Page/卡特纳.md "wikilink")\[21\]。从发现的石碑来看，汉谟拉比的统治疆域甚至北达[迪亚巴克尔](../Page/迪亚巴克尔.md "wikilink")（今属[土耳其共和国](../Page/土耳其共和国.md "wikilink")），在那里他自称“[阿摩利人之王](../Page/阿摩利人.md "wikilink")”
\[22\]。

记录汉谟拉比及其继任者统治年代的大量[粘土板和汉谟拉比自己的](../Page/粘土板.md "wikilink")[书信已经被发现](../Page/书信.md "wikilink")\[23\]，汉谟拉比统治一个帝国所要处理的应对[水灾](../Page/水灾.md "wikilink")、完善[历法](../Page/历法.md "wikilink")、照看大群的[家畜等日常政务从这些信件中可见一斑](../Page/家畜.md "wikilink")\[24\]。汉谟拉比大约于前1686年逝世，王位由其子[萨穆苏伊鲁纳继承](../Page/萨穆苏伊鲁纳.md "wikilink")\[25\]。

## 法典

汉谟拉比最让世人熟知的事迹是他颁布了一部新的[巴比伦法律](../Page/巴比伦法律.md "wikilink")《[汉谟拉比法典](../Page/汉谟拉比法典.md "wikilink")》。法典刻在一块巨大的[玄武岩](../Page/玄武岩.md "wikilink")[石碑上](../Page/石碑.md "wikilink")，并放置于公共场所以便所有人能看见，尽管那时可能很少有人识字。石碑后来被[埃兰人所夺](../Page/埃兰.md "wikilink")，并被运回埃兰首都[苏萨](../Page/苏萨.md "wikilink")。1901年石碑再次被考古人员发现，现藏于[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[卢浮宫博物馆](../Page/卢浮宫博物馆.md "wikilink")。《汉谟拉比法典》包括282条[法律](../Page/法律.md "wikilink")，由[书吏用](../Page/书吏.md "wikilink")[阿卡德语的](../Page/阿卡德语.md "wikilink")[楔形文字刻写在石碑](../Page/楔形文字.md "wikilink")，因为阿卡德语是当时巴比伦的日常用语，阿卡德语的条文可以让城中所有识字的人看懂\[26\]。

<File:Code> of Hammurabi.jpg|[卢浮宫内的汉谟拉比法典](../Page/卢浮宫.md "wikilink")
<File:Codice> di hammurabi
01.JPG|[卢浮宫内的汉谟拉比法典](../Page/卢浮宫.md "wikilink")
<File:Milkau> Oberer Teil der Stele mit dem Text von Hammurapis
Gesetzescode 369-2.jpg|《[汉谟拉比法典](../Page/汉谟拉比法典.md "wikilink")》石碑正面
[File:CodeOfHammurabi.jpg|法典石碑文字](File:CodeOfHammurabi.jpg%7C法典石碑文字)

在刻有法典的石碑顶部描绘了[巴比伦](../Page/古巴比伦神话.md "wikilink")[太阳神](../Page/太阳神.md "wikilink")[沙玛什](../Page/沙玛什.md "wikilink")（同时也是[正义之](../Page/正义.md "wikilink")[神](../Page/神.md "wikilink")）将法律授予汉谟拉比的情景，寓意着汉谟拉比是神选择出来统治他的臣民，并给他们带来法制的。与《汉谟拉比法典》类似的神启法律还有[希伯来人的](../Page/希伯来人.md "wikilink")[先知](../Page/先知.md "wikilink")[摩西領受](../Page/摩西.md "wikilink")[上帝](../Page/上帝.md "wikilink")[耶和华所寫的](../Page/耶和华.md "wikilink")《[十誡](../Page/十誡.md "wikilink")》兩塊石版。两河流域周边文明制定类似的法典还有新[苏美尔的](../Page/苏美尔.md "wikilink")《[乌尔纳姆法典](../Page/乌尔纳姆法典.md "wikilink")》和早期[赫梯的](../Page/赫梯.md "wikilink")《[赫梯法典](../Page/赫梯法典.md "wikilink")》\[27\]。

《汉谟拉比法典》受到了古代[苏美尔法律及两河流域其他各国法律的影响](../Page/苏美尔.md "wikilink")，但也保留了一些古代阿摩利人[习惯法的痕迹](../Page/习惯法.md "wikilink")\[28\]，例如它采用“同态复仇法”即“[以眼还眼，以牙还牙](../Page/以眼还眼，以牙还牙.md "wikilink")”（第196和200条）的方式来解决上层[自由民之间的纠纷](../Page/自由民.md "wikilink")\[29\]，造成许多[伤残甚至](../Page/残疾.md "wikilink")[死亡](../Page/死亡.md "wikilink")，以今天的标准看来过于残酷\[30\]。尽管如此，该法典在历史上仍然有着广泛而深远的影响，之后的历代帝王仍不时提到它，以它作为榜样\[31\]。

[Hammurabi_bas-relief_in_the_U.S._House_of_Representatives_chamber.jpg](https://zh.wikipedia.org/wiki/File:Hammurabi_bas-relief_in_the_U.S._House_of_Representatives_chamber.jpg "fig:Hammurabi_bas-relief_in_the_U.S._House_of_Representatives_chamber.jpg")

## 遗产与影响

汉谟拉比逝世后，其继任者们统治下的巴比伦帝国在[赫梯人的军事打击下逐渐衰落](../Page/赫梯人.md "wikilink")，赫梯人最终于前1595年占领巴比伦城，巴比伦第一王朝灭亡\[32\]。但赫梯人在巴比伦并未久留，赫梯人退走后，[伊新建立](../Page/伊新.md "wikilink")[巴比伦第二王朝](../Page/巴比伦第二王朝.md "wikilink")，但[加喜特人最终征服了巴比伦](../Page/加喜特人.md "wikilink")，并统治两河流域达400多年。加喜特人受巴比伦文化影响，包括《[汉谟拉比法典](../Page/汉谟拉比法典.md "wikilink")》。

因为汉谟拉比被誉为一位立法者，一些[美国政府大楼内可以发现他的形象](../Page/美国政府.md "wikilink")。汉谟拉比是[美国国会大厦](../Page/美国国会大厦.md "wikilink")[众议院会客厅](../Page/美国众议院.md "wikilink")[大理石](../Page/大理石.md "wikilink")[浅浮雕上雕刻的](../Page/浮雕.md "wikilink")23位立法者之一\[33\]。在[美国最高法院大厦南墙的](../Page/美国最高法院大厦.md "wikilink")[横饰带上刻有汉谟拉比从巴比伦](../Page/横饰带.md "wikilink")[太阳神](../Page/太阳神.md "wikilink")[沙玛什手中接受](../Page/沙玛什.md "wikilink")《汉谟拉比法典》的形象\[34\]。

## 注释

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  - .

  - .

  - .

  -
## 外部連結

  - [近观汉谟拉比法典（卢浮宫）](http://www.louvre.fr/llv/dossiers/detail_oal.jsp?CONTENT%3C%3Ecnt_id=10134198673229909&CURRENT_LLV_OAL%3C%3Ecnt_id=10134198673229909&FOLDER%3C%3Efolder_id=0&bmLocale=en)
  - [“汉谟拉比”一词的词源](http://hammu-rabi.blogspot.com/)

## 參見

  - 《[漢摩拉比法典](../Page/漢摩拉比法典.md "wikilink")》

{{-}}

[Category:巴比倫人](../Category/巴比倫人.md "wikilink")
[Category:伊拉克人](../Category/伊拉克人.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.

9.

10.

11.

12.
13.

14.
15.

16.

17.

18.

19.
20.
21.
22.

23.

24.

25.

26.

27.

28. 黄民兴《中东国家通史·伊拉克卷》第23－28页。

29. 令狐若明《世界上古史》第155－166页。

30. 《西方文明史I》第38－40页。

31.
32.

33.

34.