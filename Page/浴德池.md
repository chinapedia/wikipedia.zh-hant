[HK_LCK_Prince_Edward_Road_West_Yuk_Tak_Chee_Sauna.JPG](https://zh.wikipedia.org/wiki/File:HK_LCK_Prince_Edward_Road_West_Yuk_Tak_Chee_Sauna.JPG "fig:HK_LCK_Prince_Edward_Road_West_Yuk_Tak_Chee_Sauna.JPG")\]\]
[HK_LCK_Yuk_Tak_Chee_Sauna_Reclamation_Street.JPG](https://zh.wikipedia.org/wiki/File:HK_LCK_Yuk_Tak_Chee_Sauna_Reclamation_Street.JPG "fig:HK_LCK_Yuk_Tak_Chee_Sauna_Reclamation_Street.JPG")
**浴德池**（1949年12月—2006年10月3日）是[香港第一間上海](../Page/香港.md "wikilink")[澡堂](../Page/澡堂.md "wikilink")，全名為**上海同記浴德池浴室**，位於[旺角](../Page/旺角.md "wikilink")[太子道西](../Page/太子道.md "wikilink")123號地下及2樓，與[九巴創辦人之一](../Page/九巴.md "wikilink")[雷亮興建的](../Page/雷亮.md "wikilink")[雷生春為鄰](../Page/雷生春.md "wikilink")，於1949年12月由李振威創辦。設施包括冷熱水浴池、蒸氣房、擦背房、按摩房及休息廳。

1970年代是浴德池的黃金時代，到了1980年代經濟起飛，商家顧客也就多起來。但隨著近年芬蘭式浴池在本港興起，使浴客的品味轉變了，而[沙士事件令生意進一步下降](../Page/嚴重急性呼吸道症候群.md "wikilink")；另外因為浴德池所在的樓宇已經相當殘破，加上發展商[裕泰興已宣佈收購該處一帶的樓宇作重建](../Page/裕泰興.md "wikilink")，浴德池的業主終將現址以1.4億元售予[裕泰興](../Page/裕泰興.md "wikilink")，作為香港僅餘的一個上海澡堂浴德池已經於2006年10月3日凌晨結業。

曾光顧的知名人士包括有[粵劇名伶](../Page/粵劇.md "wikilink")[梁醒波](../Page/梁醒波.md "wikilink")、[導演](../Page/導演.md "wikilink")[李翰祥](../Page/李翰祥.md "wikilink")、商人[林百欣](../Page/林百欣.md "wikilink")、藝人[張國榮](../Page/張國榮.md "wikilink")、[黃霑](../Page/黃霑.md "wikilink")、[曾志偉及其父親](../Page/曾志偉.md "wikilink")[曾啟榮等](../Page/曾啟榮.md "wikilink")。

浴德池從不招待女性，多年來不少女士誤闖，亦有發生妻子要求入內尋找丈夫的事，但一律被拒。其充滿二十世紀中期特色的設計亦成為拍攝取景之地，例如[謝霆鋒主演的電影](../Page/謝霆鋒.md "wikilink")《半支煙》，[蕭正楠主唱的](../Page/蕭正楠.md "wikilink")[西門子](../Page/西門子公司.md "wikilink")[手機](../Page/手機.md "wikilink")[廣告歌](../Page/廣告.md "wikilink")《最短的情信》MV，以及由[何韻詩主唱的](../Page/何韻詩.md "wikilink")《禁色》MV也在浴德池拍攝，何韻詩更因此成為唯一在該浴池浸浴的女性。

## 參考資料

  - [星島日報（2006年10月2日）「浴德池」結業澡堂文化沒落](http://news.sina.com.hk/cgi-bin/news/show_news.cgi?ct=headlines&type=headlines&date=2006-10-02&id=2229598)
  - [蘋果日報（2006年10月2日）56年泡出無數風流人物](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060930&sec_id=4104&subsec_id=11867&art_id=6361592)
  - [蘋果日報（2006年10月2日）拒曾志偉母入內尋夫](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060930&sec_id=4104&subsec_id=11867&art_id=6361645)

[Category:旺角](../Category/旺角.md "wikilink")
[Category:香港已不存在的建築物](../Category/香港已不存在的建築物.md "wikilink")
[Category:中式澡堂](../Category/中式澡堂.md "wikilink")