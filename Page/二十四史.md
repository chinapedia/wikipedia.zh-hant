**二十四史**，[中國古代](../Page/中國.md "wikilink")[各朝撰写的二十四部](../Page/中国朝代.md "wikilink")[史书的总称](../Page/史書_\(東亞\).md "wikilink")，是獲歷朝代納為正統的史書，故又稱「**[正史](../Page/正史.md "wikilink")**」，记载逾四千年的[中国历史](../Page/中国历史.md "wikilink")，上起传说的[黄帝](../Page/黄帝.md "wikilink")，止于[明朝](../Page/明朝.md "wikilink")[崇祯十七年](../Page/崇祯.md "wikilink")（1644年），計3213卷，约4000万字，且统一使用分[本紀](../Page/本紀.md "wikilink")、[列傳的](../Page/列傳.md "wikilink")[纪传体编写](../Page/纪传体.md "wikilink")。“正史”这个名称见于《隋书·[经籍志](../Page/藝文志.md "wikilink")》：“世有著述，皆拟[班](../Page/班固.md "wikilink")、[马](../Page/司馬遷.md "wikilink")，以为正史。”当[乾隆帝钦定](../Page/乾隆帝.md "wikilink")“二十四史”以后，“正史”一称就由“二十四史”所专有，取得了正统史书的地位。

1921年，[中华民国大总统](../Page/中华民国大总统.md "wikilink")[徐世昌下令将](../Page/徐世昌.md "wikilink")《新元史》列入正史，与“二十四史”合称为“二十五史”，而多数地方不将《新元史》列入，而改将《清史稿》列为“二十五史”之一，如果将两书都列入正史，则成“二十六史”。

## 歷史名稱

### 三史

[三国时社会上已有](../Page/三国.md "wikilink")“三史”之称。“三史”通常是指《[史记](../Page/史记.md "wikilink")》、《[汉书](../Page/汉书.md "wikilink")》和[东汉](../Page/东汉.md "wikilink")[刘珍等写的](../Page/劉珍_\(東漢\).md "wikilink")《[东观汉记](../Page/東觀漢記.md "wikilink")》。

錢大昕《[十駕齋養新錄](../Page/十駕齋養新錄.md "wikilink")》說：「《續漢書·郡國志》：『今錄中興以來郡縣改異，及《春秋》、三史會同、征伐地名。』三史，謂《史記》、《漢書》及《東觀記》也。《吳志·呂蒙傳》注引《江表傳》：『權謂蒙曰：孤統軍以來，省三史、諸家兵法，大有益。』……自唐以來，《東觀記》失傳，乃以范蔚宗書當『三史』之一。（卷六〈三史〉，頁119）

[范曄](../Page/范晔_\(刘宋\).md "wikilink")《[后汉书](../Page/后汉书.md "wikilink")》問世，唐代以後遂漸取代《東觀漢记》，成为“三史”之一。

### 四史

“三史”加上《三國志》，称为“**前四史**”。前四史的行文精简扼要，而且基本偏重使用“[春秋筆法](../Page/春秋筆法.md "wikilink")”，所以在史学上有很高的地位。

近代学者[嚴復曾经评论说](../Page/嚴復.md "wikilink")：“古人好读前四史，亦以其文字耳。”

### 十史與十三史

唐代時，記載[三国至](../Page/三国.md "wikilink")[隋朝十個王朝的](../Page/隋朝.md "wikilink")[史書](../Page/史書_\(東亞\).md "wikilink")：《三國志》、《晉書》、《宋書》、《南齊書》、《梁書》、《陳書》、《魏書》、《北齊書》、《周書》、《隋書》十部正史，合稱「十史」。

清代[錢大昕](../Page/錢大昕.md "wikilink")《十駕齋養新錄·十三史十史》說：「十史者，自三國至隋十代之史，馬、班、范三家不在其數。」

據《舊唐書·經籍志上》，十史與《史記》、《漢書》、《後漢書》三史合稱「十三代史」。

### 十七史

到了[宋代](../Page/宋朝.md "wikilink")，在“十三史”的基础上，加入《南史》、《北史》、《新唐书》、《新五代史》，形成了“十七史”。

[元朝](../Page/元朝.md "wikilink")[丞相博羅刁難宋朝宰相](../Page/丞相.md "wikilink")[文天祥](../Page/文天祥.md "wikilink")：「汝謂有興有廢，且問[盤古帝王至今日幾帝幾王](../Page/盤古.md "wikilink")。一一為我言之。」天祥反駁：「一部十七史，從何說起！我今日非應[博學宏辭科](../Page/博學宏辭科.md "wikilink")、[神童科](../Page/神童.md "wikilink")，何暇泛論！」

順治十三年（1656年）[毛晉重刻](../Page/毛晋.md "wikilink")《[十三經](../Page/十三经.md "wikilink")》與《十七史》，並撰寫〈重鐫十三經十七史緣起〉，順治十四年（1657年）[钱谦益又撰寫](../Page/钱谦益.md "wikilink")〈汲古閣毛氏新刻十七史序〉。後世對毛晉之評價可以說是「功於史學偉矣！」

[王鳴盛](../Page/王鳴盛.md "wikilink")（1722年－1797年）著有《[十七史商榷](../Page/十七史商榷.md "wikilink")》一書，共100卷，但事實上此書也包含《[旧唐书](../Page/旧唐书.md "wikilink")》與《[旧五代史](../Page/旧五代史.md "wikilink")》。

[葉德輝](../Page/葉德輝.md "wikilink")《書林清話》說：「明季藏書家以常熟毛晉汲古閣為最著者。當時曾遍刻《十三經》、《十七史》、《津逮秘書》、唐宋元人別集。以至道藏、詞曲，無不搜刻傳之。」

### 二十一史

[明朝](../Page/明朝.md "wikilink")[萬曆年間](../Page/萬曆.md "wikilink")，[國子監刊行的正史](../Page/國子監.md "wikilink")，在宋代的「十七史」之上，增加了《宋史》、《遼史》、《金史》、《元史》四部史書，合称「二十一史」。

清代[顧炎武](../Page/顧炎武.md "wikilink")《日知錄·監本二十一史》說：「宋時止有十七史，今則併宋、遼、金、元四史為二十一史。」[张廷玉](../Page/张廷玉.md "wikilink")
《上明史表》時也說：「興衰有自，七十二君之跡何稱；法戒攸關，二十一史之編具在。參見『十七史』。」

### 二十四史

[清朝](../Page/清朝.md "wikilink")[乾隆初年](../Page/乾隆.md "wikilink")，刊行《明史》，加先前各史，总名“二十二史”。后来又增加了《旧唐书》，成为“二十三史”。从《[永乐大典](../Page/永乐大典.md "wikilink")》中辑录出来的《旧五代史》也被列入。[乾隆四年](../Page/乾隆.md "wikilink")（1739年），经[乾隆帝钦定](../Page/乾隆帝.md "wikilink")，合称“二十四史”。并刊“[武英殿本](../Page/武英殿.md "wikilink")”。

### 二十五史、二十六史

  - 《新元史》

因《[元史](../Page/元史.md "wikilink")》撰寫之內容實為缺漏，且用字離疏，故[柯劭忞於](../Page/柯劭忞.md "wikilink")[民國六年](../Page/民國紀年.md "wikilink")（1917年）召編《[新元史](../Page/新元史.md "wikilink")》，民国九年（1920年）脫稿，民国十年（1921年）大总统[徐世昌下令将](../Page/徐世昌.md "wikilink")《[新元史](../Page/新元史.md "wikilink")》列入“正史”，与“二十四史”合称“二十五史”。

  - 《清史稿》

但也有人將[趙爾巽等編的](../Page/趙爾巽.md "wikilink")《[清史稿](../Page/清史稿.md "wikilink")》列為二十五史之一。

  - 《清史稿》、《新元史》並列二十六史

或者將兩書都列入正史的總數計算，合稱「二十六史」。

## 主要版本

  - [南宋紹興十四年四川轉運使井憲孟刊行](../Page/南宋.md "wikilink")「眉山七史」（宋、南齊、梁、陳、魏、北齊、周書，現收入百衲本二十四史）
  - [明朝](../Page/明朝.md "wikilink")[南京国子监刻](../Page/南京国子监.md "wikilink")“二十一史”（“[南监本](../Page/南京国子监.md "wikilink")”）
  - 明[万历](../Page/万历.md "wikilink")[北京国子监刻](../Page/北京国子监.md "wikilink")“二十一史”（“[北监本](../Page/北京国子监.md "wikilink")”）
  - 明[崇祯毛氏汲古阁刻](../Page/崇祯.md "wikilink")“十七史”
  - [清朝](../Page/清朝.md "wikilink")[乾隆武英殿刻](../Page/乾隆.md "wikilink")“二十四史”
  - 清[同治](../Page/同治.md "wikilink")[光绪间五省官书局合刻](../Page/光绪.md "wikilink")“二十四史”
  - [中華民國](../Page/中華民國.md "wikilink")[商務印書館印](../Page/商务印书馆.md "wikilink")[張元濟輯](../Page/張元濟.md "wikilink")《[百衲本二十四史](../Page/百衲本.md "wikilink")》
  - 民國45-46年[二十五史編刊館印行](../Page/二十五史編刊館.md "wikilink")《[仁壽本二十五史](../Page/仁壽本.md "wikilink")》（1961年國防研究院出版修訂本《清史》，1971年成文出版社有限公司補入出版，改稱「仁壽本二十六史」）
  - 民國五十六年[臺灣商務印書館](../Page/臺灣商務印書館.md "wikilink")[王雲五重印](../Page/王雲五.md "wikilink")《[百衲本二十四史](../Page/百衲本.md "wikilink")》（16开
    精装）
  - 民國六十五年[臺灣商務印書館](../Page/臺灣商務印書館.md "wikilink")[王雲五補校](../Page/王雲五.md "wikilink")《[百衲本二十四史](../Page/百衲本.md "wikilink")》（大32开
    平装）
  - 民國九十九年[臺灣商務印書館增大开本](../Page/臺灣商務印書館.md "wikilink")、放大字体重印[王雲五補校](../Page/王雲五.md "wikilink")《[百衲本二十四史](../Page/百衲本.md "wikilink")》（精装。出版说明为18开，但对于32开而言，开本的增大主要在宽度上而不是在长度上）
  - [中华书局排印点校本](../Page/中华书局.md "wikilink")“二十四史”

## 人物列表

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/史記人物列表.md" title="wikilink">史記人物列表</a></li>
<li><a href="../Page/汉书人物列表.md" title="wikilink">汉书人物列表</a></li>
<li><a href="../Page/后汉书人物列表.md" title="wikilink">后汉书人物列表</a></li>
<li><a href="../Page/三国志人物列表.md" title="wikilink">三国志人物列表</a></li>
<li><a href="../Page/晋书人物列表.md" title="wikilink">晋书人物列表</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/宋书人物列表.md" title="wikilink">宋书人物列表</a></li>
<li><a href="../Page/南齐书人物列表.md" title="wikilink">南齐书人物列表</a></li>
<li><a href="../Page/梁书人物列表.md" title="wikilink">梁书人物列表</a></li>
<li><a href="../Page/陈书人物列表.md" title="wikilink">陈书人物列表</a></li>
<li><a href="../Page/魏书人物列表.md" title="wikilink">魏书人物列表</a></li>
<li><a href="../Page/北齐书人物列表.md" title="wikilink">北齐书人物列表</a></li>
<li><a href="../Page/周书人物列表.md" title="wikilink">周书人物列表</a></li>
<li><a href="../Page/北史人物列表.md" title="wikilink">北史人物列表</a></li>
<li><a href="../Page/南史人物列表.md" title="wikilink">南史人物列表</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/唐书人物列表.md" title="wikilink">唐书人物列表</a></li>
<li><a href="../Page/五代史人物列表.md" title="wikilink">五代史人物列表</a></li>
<li><a href="../Page/宋史人物列表.md" title="wikilink">宋史人物列表</a></li>
<li><a href="../Page/辽史人物列表.md" title="wikilink">辽史人物列表</a></li>
<li><a href="../Page/金史人物列表.md" title="wikilink">金史人物列表</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/元史人物列表.md" title="wikilink">元史人物列表</a></li>
<li><a href="../Page/明史人物列表.md" title="wikilink">明史人物列表</a></li>
<li><a href="../Page/清史稿人物列表.md" title="wikilink">清史稿人物列表</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/春秋人物列表.md" title="wikilink">春秋人物列表</a></li>
<li><a href="../Page/战国人物列表.md" title="wikilink">战国人物列表</a></li>
<li><a href="../Page/资治通鉴人物列表.md" title="wikilink">资治通鉴人物列表</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [中华文化网《歷史傳記》](https://web.archive.org/web/20061018211621/http://www.chinapage.com/big5/02.html)

## 参见

  - [中国历史](../Page/中国历史.md "wikilink")
  - [正史](../Page/正史.md "wikilink")、[野史](../Page/野史.md "wikilink")

[Category:纪传体](../Category/纪传体.md "wikilink")
[二十四史](../Category/二十四史.md "wikilink")
[ESSS](../Category/中國書籍並稱.md "wikilink")