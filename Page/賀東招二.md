**賀東招二**（，），[日本](../Page/日本.md "wikilink")[小-{zh:說;zh-hans:说;zh-hant:説;}-家](../Page/小說家_\(職業\).md "wikilink")、[編劇](../Page/編劇.md "wikilink")。主要著作是[輕小說](../Page/輕小說.md "wikilink")。出身於[東京都](../Page/東京都.md "wikilink")。東京都立神代高等學校畢業，[中央大學經濟學部肄業](../Page/中央大學_\(日本\).md "wikilink")。大學時期參與[SF研究會](../Page/科幻小說.md "wikilink")。

驚爆危機決定在[超級機器人大戰J加入參戰的時候](../Page/超級機器人大戰J.md "wikilink")，曾有在個人日記公開了劇本的一小部份，高興到忘我的事。另外，在[『超級機械人大戰』系列中](../Page/超級機械人大戰系列.md "wikilink")，有用「及把[夏亞駕駛的](../Page/夏亞·阿茲納布爾.md "wikilink")[沙薩比擊墜](../Page/MSN-04型機動戰士.md "wikilink")」的玩法的樣子。

在[驚爆危機作品中](../Page/驚爆危機.md "wikilink")，[Dr
Pepper以女主角喜愛的飲料登場](../Page/Dr_Pepper.md "wikilink")，因此作者被誤解為喜歡該飲料，據作者所述，與其說是不習慣喝這東西，不如說是根本喝不下。

另外，在[京都動畫接電視動畫](../Page/京都動畫.md "wikilink")[驚爆危機？校園篇的製作後建立深厚關係](../Page/驚爆危機#驚爆危機_校園篇（ふもっふ）.md "wikilink")，2006年的[涼宮春日的憂鬱放送序第](../Page/涼宮春日的憂鬱.md "wikilink")11話的「射手座之日」中開始參與除了驚爆危機以外的故事劇本，爾後，2007年的[幸運☆星中負責的第](../Page/幸運☆星.md "wikilink")5話「神射手」、第12話「一起去祭典吧」和第22話「在這裡的彼方」的劇本，2012年的[冰果擔任劇本統籌](../Page/古籍研究社系列.md "wikilink")，2014年的[甘城輝煌樂園救世主擔任系列監修和部分集數的腳本](../Page/甘城輝煌樂園救世主.md "wikilink")。

與以[成人漫畫為主舞台活躍的](../Page/成人漫畫.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")是很親密的朋友。

和[漫畫家](../Page/漫畫家.md "wikilink")[赤松健是大學時期電影研究會的前輩與後輩關係](../Page/赤松健.md "wikilink")。

## 作品列表

### 執筆作品

  - 系列（[富士見書房](../Page/富士見書房.md "wikilink")、[富士見Fantasia文庫](../Page/富士見Fantasia文庫.md "wikilink")，1991年－1997年）
    ※共著

  - [驚爆危機！](../Page/驚爆危機.md "wikilink")，又译全金属狂潮（富士見書房、富士見Fantasia文庫，1998年－2011年）

  - （[小学館](../Page/小学館.md "wikilink")、[GAGAGA文庫](../Page/GAGAGA文庫.md "wikilink")，2009年－續刊中）

      - （[竹書房](../Page/竹書房.md "wikilink")、，2006年－2007年）（擔當原案） ※未完，COP
        CRAFT的原版本，但連載期間因為被取消的緣故而停刊，後來作者將其加寫後轉到GAGAGA文庫發行。

  - [驚爆危機！Another](../Page/驚爆危機！Another.md "wikilink")（富士見書房、富士見Fantasia文庫，2011年8月－2016年2月）※原案、監修

  - [甘城輝煌樂園救世主](../Page/甘城輝煌樂園救世主.md "wikilink")（富士見書房、富士見Fantasia文庫，2013年2月－）

### 系列構成、劇本

  - [驚爆危機！](../Page/驚爆危機.md "wikilink")（2002年）劇本統籌
  - [驚爆危機？校園篇](../Page/驚爆危機#驚爆危機?校園篇（ふもっふ）.md "wikilink")（2003年）劇本統籌・劇本
  - [驚爆危機\!The Second
    Raid](../Page/驚爆危機#驚爆危機!The_Second_Raid.md "wikilink")（2005年）劇本統籌・劇本
  - [驚爆危機\!The Second Raid
    特別版](../Page/驚爆危機#驚爆危機!The_Second_Raid_特別版.md "wikilink")（2006年）劇本
  - 驚爆危機！ The Second Raid 特別版OVA初回限定版特典DramaCD（2006年）劇本
  - [涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")（2006年）劇本
  - [幸運☆星](../Page/幸運☆星.md "wikilink")（2007年）劇本
  - [迷宮塔 ～烏魯克之盾～](../Page/迷宮塔_\(動畫\).md "wikilink")（2008年）劇本統籌・劇本
  - [迷宮塔 ～烏魯克之劍～](../Page/迷宮塔_\(動畫\).md "wikilink")（2009年）劇本統籌・劇本
  - [冰果](../Page/古籍研究社系列.md "wikilink")（2012年）劇本統籌・劇本
  - [甘城輝煌樂園救世主](../Page/甘城輝煌樂園救世主.md "wikilink")（2014年）劇本監修・劇本
  - [驚爆危機\!IV](../Page/驚爆危機#驚爆危機!IV.md "wikilink")（2017年）劇本統籌・劇本

## 外部連結

  - [賀東招二官方網頁](http://www.gatoh.com/)

  -
[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:日本輕小說作家](../Category/日本輕小說作家.md "wikilink")
[Category:滋賀縣出身人物](../Category/滋賀縣出身人物.md "wikilink")
[Category:日本中央大學校友](../Category/日本中央大學校友.md "wikilink")