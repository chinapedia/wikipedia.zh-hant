**益華電腦股份有限公司**（Cadence Design Systems,
Inc）成立於1988年，是EDA(電子設計自動化)軟體與工程服務的重要廠商，主要提供設計積體電路(IC)、系統單晶片(SoC)、以及印刷電路板(PCB)所需的軟體工具與矽智財(IP)，涵蓋類比/數位/混合電路設計、驗證、封裝/PCB設計等各領域。

## 历史

Cadence是由SDA
Systems和ECAD兩家公司於1988年合併而成立的，從1990年代起，透過內部開發以及多項併購行動，逐步建構包含佈線、合成、以及硬體模擬等完整的設計解決方案，例如1998年收購Ambit
Design System、1999年收購Quickturn、2002年收購IBM的硬體模擬業務、以及2003年收購Get2Chip等。

此外，Cadence自2010年起強化IP業務，當年度收購片上記憶體IP領先廠商Denali以及2013年收購可配置處理器IP廠商Tensilica最為重要，後陸續收購包括Cosmic
Circuits、Evatronix的IP部門、以及Transwitch的HIS部門，建立完整IP產品組合。

因應智慧聯網系統的快速發展，Cadence近年積極朝系統實現(System Design
Enablement)方向移轉，於2014年收購高階合成(HLS)業者Forte
Design System以及形式驗證業者Jasper Design Automation。

現任執行長陳立武(Lip-Bu Tan)於2009年上任迄今，該公司已於2015年起連續三年，被財星雜誌列為「百大最佳職場」。

## 产品

Cadence的電子設計自動化產品涵蓋了電子設計的整個流程，包括：

• 客製IC技術 ─
Virtuoso平台，這是設計全自訂積體電路所需的工具，包括原理圖輸入、行為建模(Verilog-AMS)、電路模擬、自訂佈局、實體驗證、萃取等。主要用於類比、混合訊號、RF標準單元的設計。

• 數位與簽核技術 ─ RTL到GDS II建置，包括
Genus合成技術、Joules功率分析、Innovus佈局與繞線、Tempus時脈簽核、Voltus功率完整性簽核、Modus自動測試產生器。

• 系統與驗證技術 ─ Cadence 驗證套件，包括JasperGold形式驗證、Xcelium模擬、Palladium
Z1硬體模擬、Protium S1 FPGA原型製作、Perspec軟體驅動測試、Indago除錯、以及驗證IP產品組合。

• 矽智財 ─
設計IP解決方案鎖定的領域包括記憶體/儲存/高效能介面協定、以及適用於音訊、視覺、無線數據機、以及卷積式神經網路的Tensilica
DSP處理器。

• PCB與封裝技術
─積體電路、封裝與PCB的協同設計工具Allegro平台、PCB設計工具OrCAD/PSpice、以及系統級簽核與介面遵循性的訊號與功率驗證工具Sigrity。

• 特定應用解決方案 ─
鎖定汽車產業，提供從IP、工具、設計流程到諮詢服務的完整解決方案，同時符合ISO26262標準的設計流程均已建立，並可提供Cadence汽車安全設計套件。

同時，Cadence公司還提供設計方法論服務，幫助客戶最佳化其設計流程，協助客戶進入新的市場領域。

## 参考文献

## 外部链接

Cadence site
\*''<https://www.cadence.com/content/cadence-www/global/zh_TW/home.html>

Cadence Taiwan Facebook:
<https://www.facebook.com/cadence.taiwan/?ref=bookmarks>

[Category:美國電子公司](../Category/美國電子公司.md "wikilink")
[C](../Category/聖荷西公司.md "wikilink")
[Category:电子设计自动化公司](../Category/电子设计自动化公司.md "wikilink")