**聶安達**（，），生於[美國](../Page/美國.md "wikilink")，有[瑞典血統](../Page/瑞典.md "wikilink")。[香港著名的音樂人](../Page/香港.md "wikilink")、演員及電影配樂師。從1960年代開始就已在香港的樂壇活躍，並在1990年代積極重振香港的樂隊風潮，是著名的音樂幕後人員。後來因為在2000年[亞洲電視的電視劇中](../Page/亞洲電視.md "wikilink")《[世紀之戰](../Page/世紀之戰.md "wikilink")》飾演股神賽斯，所以又得「股神」這個外號。現為[無綫電視特約演員](../Page/無綫電視.md "wikilink")。

## 簡歷

父母皆是瑞典裔的傳教人員，1950年4歲的聶安達隨父母從中國內地來港
，在香港沙田[道風山基督教堂長大](../Page/道風山.md "wikilink")，說得一口流利廣東話。\[1\]他曾就讀[英皇佐治五世學校](../Page/英皇佐治五世學校.md "wikilink")。最初在[麗的呼聲工作](../Page/麗的呼聲.md "wikilink")，參演電視劇《浴血太平山》和《再會太平山》，後加入[無綫電視](../Page/無綫電視.md "wikilink")；首部無綫作品為[香城浪子](../Page/香城浪子.md "wikilink")。聶熱愛音樂，1962年組Band《The
Kontinentals》第一次出騷，其後亦組織樂隊 The Inspiration 和 The
Ming「盟」樂隊，推出作品包括為《[啼笑姻緣](../Page/啼笑姻緣.md "wikilink")》譜上英文歌詞的
This Time Tomorrow
。聶安達亦創辦《聶安達製作公司》為電影、電視[作曲及](../Page/作曲.md "wikilink")[配樂](../Page/配樂.md "wikilink")，作品有無綫電視電視劇《[荳芽夢](../Page/荳芽夢.md "wikilink")》主題曲等；曾憑《[愛人女神](../Page/愛人女神.md "wikilink")》提名[1983年香港電影金像獎最佳電影歌曲及](../Page/1983年香港電影金像獎.md "wikilink")《[殭屍先生](../Page/殭屍先生.md "wikilink")》奪得[1986年香港電影金像獎最佳電影配樂](../Page/1986年香港電影金像獎.md "wikilink")；亦經常在電影電視劇中客串\[2\]，例如[翡翠台高清數碼廣播廣告](../Page/翡翠台.md "wikilink")（與[楊思琦合作](../Page/楊思琦.md "wikilink")），他表示他最深記憶的是在[猛龍過江的小角色](../Page/猛龍過江.md "wikilink")。

1990年代，香港再度興起[樂隊熱潮](../Page/樂隊.md "wikilink")，聶安達亦是當中的一位積極分子，經常都有參與每年一度的[嘉士伯流行音樂節](../Page/嘉士伯流行音樂節.md "wikilink")，與香港的著名地下樂隊如[ANODIZE等夾Band](../Page/ANODIZE.md "wikilink")。後來擔當EMI唱片公司音樂總監，曾簽下[鄭伊健及](../Page/鄭伊健.md "wikilink")[瑪俐亞等歌手](../Page/瑪俐亞.md "wikilink")。

2008年，推出多首著名中文歌曲重新填上英文歌詞的單曲，包括「[月亮代表我的心](../Page/月亮代表我的心.md "wikilink")」、「[南泥灣](../Page/南泥灣_\(歌曲\).md "wikilink")」、「為你鍾情」等。

## 電影配樂作品

1982

  - [賓妹](../Page/賓妹_\(電影\).md "wikilink")
  - [愛人女神](../Page/愛人女神.md "wikilink")

1985

  - [偷情](../Page/偷情_\(1985年電影\).md "wikilink")
  - [殭屍先生](../Page/殭屍先生.md "wikilink")
  - [花街時代](../Page/花街時代.md "wikilink")
  - [夏日福星](../Page/夏日福星.md "wikilink")

1986

  - [茅山學堂](../Page/茅山學堂.md "wikilink")
  - [殭屍家族](../Page/殭屍家族.md "wikilink")
  - [開心鬼精靈](../Page/開心鬼精靈.md "wikilink")
  - [富貴列車](../Page/富貴列車_\(電影\).md "wikilink")

1987

  - [小生夢驚魂](../Page/小生夢驚魂.md "wikilink")
  - [呷醋大丈夫](../Page/呷醋大丈夫.md "wikilink")
  - [猛鬼差館](../Page/猛鬼差館.md "wikilink")
  - [七年之癢](../Page/七年之癢.md "wikilink")
  - [魔高一丈](../Page/魔高一丈.md "wikilink")
  - [不歸路](../Page/不歸路.md "wikilink")
  - [表哥到](../Page/表哥到.md "wikilink")
  - [靈幻先生](../Page/靈幻先生.md "wikilink")
  - [命帶桃花](../Page/命帶桃花.md "wikilink")

1988

  - [愛情謎語](../Page/愛情謎語.md "wikilink")
  - [奸人本色](../Page/奸人本色.md "wikilink")
  - [靈幻小姐](../Page/靈幻小姐.md "wikilink")

1989

  - [火燭鬼](../Page/火燭鬼.md "wikilink")
  - [我愛唐人街](../Page/我愛唐人街.md "wikilink")
  - [一眉道人](../Page/一眉道人.md "wikilink")
  - [捉鬼大師](../Page/捉鬼大師.md "wikilink")

1992

  - [新殭屍先生](../Page/新殭屍先生.md "wikilink")

## 電視劇演出

### [麗的電視](../Page/麗的電視.md "wikilink")/[亞洲電視](../Page/亞洲電視.md "wikilink")

  - 1981年：[浴血太平山](../Page/浴血太平山.md "wikilink") 飾 威廉臣
  - 1981年：[再會太平山](../Page/再會太平山.md "wikilink") 飾 威廉臣
  - 1983年：[誓不低頭續集](../Page/誓不低頭_\(亞洲電視劇集\).md "wikilink")
  - 1983年：[少女慈禧](../Page/少女慈禧.md "wikilink") 飾 巴夏禮
  - 1988年：[賽金花](../Page/賽金花_\(亞視電視劇\).md "wikilink") 飾 克林德
  - 1988年：[張保仔](../Page/張保仔_\(1988年電視劇\).md "wikilink") 飾 特文
  - 2000年：[世紀之戰](../Page/世紀之戰.md "wikilink") 飾 賽斯

### [無綫電視](../Page/無綫電視.md "wikilink")

  - 1982年：[香城浪子](../Page/香城浪子.md "wikilink") 飾 Robinson/校　長
  - 2002年：[皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink") 飾 大衛
  - 2006年：[飛短留長父子兵](../Page/飛短留長父子兵.md "wikilink") 飾 Thomas
  - 2007年：[歲月風雲](../Page/歲月風雲.md "wikilink")
  - 2012年：[怒火街頭2](../Page/怒火街頭2.md "wikilink") 飾 退休律師
  - 2012年：[名媛望族](../Page/名媛望族.md "wikilink") 飾 洋人工務官
  - 2012年：[雷霆掃毒](../Page/雷霆掃毒.md "wikilink") 飾 神父
  - 2015年：[以和為貴](../Page/以和為貴.md "wikilink")
  - 2016年：[殭](../Page/殭.md "wikilink") 飾 未來人
  - 2017年：[蘭花刼](../Page/蘭花刼.md "wikilink") 飾 皮亞斯
  - 2018年：[逆緣](../Page/逆緣.md "wikilink") 飾 Mr. Nelson
  - 2018年：[是咁的，法官閣下](../Page/是咁的，法官閣下.md "wikilink") 飾 神父Garcia

## 電影演出

1972

  - [猛龍過江](../Page/猛龍過江.md "wikilink")

1983

  - [我愛夜來香](../Page/我愛夜來香.md "wikilink")

1986

  - [惡男](../Page/惡男.md "wikilink")
  - [原振俠與衛斯理](../Page/原振俠與衛斯理.md "wikilink")
  - [最佳福星](../Page/最佳福星.md "wikilink")

1988

  - [公子多情](../Page/公子多情.md "wikilink")

1989

  - [奇蹟](../Page/奇蹟.md "wikilink")

1990

  - [靚足100分](../Page/靚足100分.md "wikilink")

1992

  - [黃飛鴻笑傳](../Page/黃飛鴻笑傳.md "wikilink") 飾 鴉片商人

1999

  - [千禧巨龍](../Page/千禧巨龍.md "wikilink")

2016

  - [iGirl夢情 - 智能博士](../Page/iGirl夢情_-_智能博士.md "wikilink")

## 注釋

## 外部連結

  - [官方網站](http://www.andersnelsson.com/)

  -
  - [2008年新浪網訪問](http://blog.sina.com.cn/s/blog_462e9d710100as56.html)

  - [Anders Nelson唱片封面](http://60spunk.m78.com/image/andersnelson.jpg)

[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前麗的電視藝員](../Category/前麗的電視藝員.md "wikilink")
[Category:香港作曲家](../Category/香港作曲家.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:美國裔香港人](../Category/美國裔香港人.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美國創作歌手](../Category/美國創作歌手.md "wikilink")

1.  [世紀．移民音樂﹕專訪聶安達：不願離開香港的瑞典人](http://premium.mingpao.com/pda/palm/colDocDetail.cfm?PublishDate=20110822&File=vx001a.txt&token=b218bc260b89c0)《明報網站》
    2011年08月22日
2.  <http://hkmdb.com/db/people/portraits.mhtml?id=6379&display_set=big5>