**歐錦棠**（**Stephen Au Kam
Tong**，），香港演員和電台主持，1990年與其妻子[萬斯敏出道於](../Page/萬斯敏.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")。他演藝創作興趣多方面，包括攝影、寫作、中國武術等。

妻子為前[亞洲電視藝員](../Page/亞洲電視.md "wikilink")[萬斯敏](../Page/萬斯敏.md "wikilink")，兩人1997年結婚，無子女。\[1\]另外他任職國際[空手道](../Page/空手道.md "wikilink")「正道塾」的武術顧問。\[2\]

## 生平

歐錦棠中學就讀[高主教書院](../Page/高主教書院.md "wikilink")，在那個時代他已經喜愛新聞及寫作。他曾經在著名武術雜誌《[新武俠雜誌](../Page/新武俠雜誌.md "wikilink")》（New
Martial Hero Magazine）當採訪及攝影記者，並且投稿多份報章，當自由撰稿人。

歐錦棠喜愛攝影。在1981年，參加政府舉辦的「清潔香港海報攝影比賽」，獲公開組冠軍。1990年，歐畢業於的第1屆[亞洲電視訓練學院](../Page/亞洲電視訓練學院.md "wikilink")，戲劇導師有[張錚及](../Page/張錚.md "wikilink")[何偉龍](../Page/何偉龍.md "wikilink")。其後於亞視工作多年，直至1999年8月。其後他到過中國大陸及新加坡拍劇，也曾主持[有線電視怪談節目](../Page/有線電視.md "wikilink")。1996年，他於[新城電台](../Page/新城電台.md "wikilink")（Metro
Broadcast）主持《香蕉俱樂部》（Banana
Club）節目及深夜節目《驚叫一點鐘》九個月。直至2001年才加入[無綫電視](../Page/電視廣播有限公司.md "wikilink")。代表作為《[同事三分親](../Page/同事三分親.md "wikilink")》的武紀勇、《[畢打自己人](../Page/畢打自己人.md "wikilink")》的閆汝大及《[誰家灶頭無煙火](../Page/誰家灶頭無煙火.md "wikilink")》的鍾國棟。

他也是[中國功夫電影巨星](../Page/中國功夫.md "wikilink")[李小龍的愛好者之一](../Page/李小龍.md "wikilink")。
在2000年5月，開設全港首家李小龍展覽館「小龍舘」（Dragon
Pavilion），免費開放供市民及遊客參觀，持續了1年。歐錦棠又在2003年7月，導演《李小龍之道》[紀錄片](../Page/紀錄片.md "wikilink")（Bruce
Lee On The Road），在[RoadShow播出](../Page/路訊通.md "wikilink")。

2010年，歐錦棠與妻子[萬斯敏創立舞台劇劇團](../Page/萬斯敏.md "wikilink")[劇道場](../Page/劇道場.md "wikilink")，致力培訓戲劇人才及創作劇目。

2017年9月，歐錦棠演出音樂劇《馬丁路德》時發生意外，跳現代舞時左手突然甩骱。他被送往急症室，醫生要他留院一晚駁骨，但他包紮後自行簽紙出院，繼續演出尾場。及後去睇骨科醫生，幸好不用動手術，但要打石膏\[3\]。

## 舞台及藝術作品

1.  2019年 1月 決戰巖流島 (Musashi Vs Kojiro) 飾 佐佐木小次郎
2.  2018年 10月 我愛喵星人(重演) (Meow the Musical Re-run) 飾 Summer
3.  2018年 6月 阿席 (Good Old Days) 飾 林正港
4.  2018年 1月 藝海唐生 (The Poetry Journey of Tong) 飾
    [唐滌生](../Page/唐滌生.md "wikilink")
5.  2017年 10月 男女PK大作戰(第三度公演) (Love Battles 3rd-run) 飾 阿修
6.  2017年 9月 馬丁路德
7.  2017年 6月 擊動情心 (gLOVE) 飾 蘇敏政
8.  2017年 1月 [科學怪人](../Page/科學怪人.md "wikilink") (Frankenstein) 飾 受造物
    (Creature)
9.  2016年 9月 [貞德](../Page/貞德.md "wikilink") (Jeanne d'Arc) 飾 查理七世
10. 2016年 5月 春雨歸來 (Don't Forget to Remember) 飾 潘道英
11. 2016年 1月 我愛喵星人 (Meow the Musical) 飾 靜夫(成年)
12. 2015年10月 再見別離時(第四度公演) (A Time for Farewells 4th Run) 飾 Alex
13. 2015年 8月 男女PK大作戰(重演) (Love Battles Re-run) 飾 阿修
14. 2015年 6月 男女PK大作戰 (Love Battles) 飾 阿修
15. 2014年 6月 聖女。貞德 (Jeanne S'Are) 飾 焦爾德
16. 2014年 1月 棋廿三 (Checkmate) 飾 松板石根
17. 2013年 8月 [宮本武藏](../Page/宮本武藏.md "wikilink") (Miyamoto Musashi) 飾
    宮本武藏
18. 2013年 6月 奪命追魂Call (Dead Man's Cell Phone)
19. 2012年 4月 想變成人的貓 (The Cat & The Solitary Man) 飾 麥基
20. 2011年 12月 20000赫茲的說話 (Beyond the Words)
21. 2011年 11月 中佬40戇居居之中國隊長 (Captain China)
22. 2008年至2011年 死佬日記(Caveman)
23. 2011年 女上男下(Move over Mrs. Markham)
24. 2011年 1月 再見別離時 (A Time for Farewells) 飾 Alex
25. 2010年 一樹梨花壓海棠 (Lolita is Mine)
26. 2010年 情場如戰場 (The Battle of Love)
27. 2010年至2011年 好姐賣粉果(I Love Steam Dumpling)
28. 2010年 灰闌(The Chalk Circle in China)
29. 2009年 讓我愛一次(Let Me Love Once)
30. 2009年 鬥角勾心(American Buffalo)
31. 2009年 霧夜謀殺案(The Unexpected Guest)
32. 2008年 布萊希特的情人(La Maîtresse de Brecht)
33. 2008年 奇異訪客(The Visitor)
34. 2007年 麥迪遜之橋(The Bridges of Madison County)
35. 2006年 哲拳太極(Taiji)
36. 2006年 1月 [誘心人](../Page/誘心人.md "wikilink")（Closer）
37. 2005年 [劍雪浮生A](../Page/劍雪浮生.md "wikilink") Sentimental Journey）-
    [唐滌生](../Page/唐滌生.md "wikilink")
38. 2005年 7月至10月，個人攝影展《畢加索的旋轉蒙馬特》（Revolving Montmartre of Picasso）
39. 2005年 畢加索遇上愛恩斯坦(Picasso at the Lapin Agile )
40. 2005年 承受清風(Inherit the Wind)
41. 2003年 (界刂)食男女(Dinner With Friends)
42. 2003年7月 音樂劇《細鳳》（Sai Fung）
43. 2003年/2006年 十二怒漢(Twelve Angry Men)
44. 2002年 看不到的故事(Molly Sweeney)
45. 2001年5月 創辦武術雜誌《新格鬥》（Xin-Combat Magazine）
46. 2001年12月 大刀王五(The Swordman Wang Ng)
47. 2001年 義海雄風(A Few Good Men)
48. 2000年 夢斷維港(West Side Story)
49. 1999年 今晚我o地棟篤笑(Tonite We Play Stand-up Comedy)
50. 1999年 向李小龍致敬的自主錄像[影片](../Page/影片.md "wikilink")《1959某日某》（What Are
    You Gonna Do, Sai Fung?）
51. 1998年
    自主錄像《理想》及《無常》（是為1999年[香港藝術中心示範作](../Page/香港藝術中心.md "wikilink")）
52. 1998年 編導[荃青劇社的](../Page/荃青劇社.md "wikilink")《情咬新郎哥》（Runaway Bridal）
53. 1996年
    參與資深舞台劇家[麥秋的](../Page/麥秋.md "wikilink")《[虎度門](../Page/虎度門.md "wikilink")》（Hu
    Du Men）

## 演出作品

### 電視劇集（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 1990年：[曲終人未散](../Page/曲終人未散.md "wikilink")
  - 1990年：[夏天的童話](../Page/夏天的童話.md "wikilink")（又名：[Q表姐](../Page/Q表姐.md "wikilink")）
  - 1990年：[鴻運迫人來](../Page/鴻運迫人來.md "wikilink")
  - 1990年：[鬼做你老婆](../Page/鬼做你老婆.md "wikilink")
  - 1991年：[觸電情緣](../Page/觸電情緣.md "wikilink")
  - 1991年：[勝者為王](../Page/勝者為王.md "wikilink")
  - 1991年：[豪門](../Page/豪門.md "wikilink")
  - 1991年：[香港奇案](../Page/香港奇案.md "wikilink")
  - 1991年：[爭雄歲月](../Page/爭雄歲月.md "wikilink")
  - 1991年：[劍神不敗](../Page/劍神不敗.md "wikilink")
  - 1992年：[八婆會館](../Page/八婆會館.md "wikilink")
  - 1992年：[點解阿Sir係隻鬼](../Page/點解阿Sir係隻鬼.md "wikilink") 飾 衛家國
  - 1992年：[摩登七十二家房客](../Page/摩登七十二家房客.md "wikilink")
  - 1992年：[烈火雄風](../Page/烈火雄風.md "wikilink")
  - 1992年：[仙鶴神針](../Page/仙鶴神針.md "wikilink")
  - 1992年：[龍在江湖](../Page/龍在江湖_\(電視劇\).md "wikilink")
  - 1992年：[新香港奇案之電梯情殺案](../Page/新香港奇案之電梯情殺案.md "wikilink")
  - 1993年：[中國教父](../Page/中國教父.md "wikilink")
  - 1993年：[勝者為王III王者之戰](../Page/勝者為王III王者之戰.md "wikilink")
  - 1995年：[精武門](../Page/精武門_\(電視劇\).md "wikilink")
  - 1996年：[誰是兇手](../Page/誰是兇手.md "wikilink")
  - 1997年：[我來自潮州](../Page/我來自潮州.md "wikilink") 飾 李乃強
  - 1998年：[流氓·律師](../Page/流氓·律師.md "wikilink") 飾 黎志鵬
  - 1998年：[我來自廣州](../Page/我來自廣州.md "wikilink") 飾 陳炳
  - 1999年：[縱橫四海](../Page/縱橫四海.md "wikilink") 飾 崔小龍
  - 1999年：[英雄之廣東十虎](../Page/英雄之廣東十虎.md "wikilink") 飾
    [黃澄可](../Page/黃澄可.md "wikilink")
  - 2000年：[影城大亨](../Page/影城大亨.md "wikilink") 飾 雷龍

### 電視劇集（[無綫電視](../Page/電視廣播有限公司.md "wikilink")）

  - 2002年：[牛郎織女](../Page/牛郎織女_\(2002年電視劇\).md "wikilink") 飾 比干
  - 2002年：[法網伊人](../Page/法網伊人.md "wikilink") 飾 葉向輝 （第三男主角）
  - 2002年：[騎呢大狀](../Page/騎呢大狀.md "wikilink") 飾 武龍
  - 2002年：[烈火雄心II](../Page/烈火雄心II.md "wikilink") 飾 蔡南豐（紀瑤之舅父）
  - 2003年：[洗冤錄II](../Page/洗冤錄II.md "wikilink") 飾 鄒子龍
  - 2003年：[十萬噸情緣](../Page/十萬噸情緣.md "wikilink") 飾 劉華
  - 2004年：[翡翠戀曲](../Page/翡翠戀曲.md "wikilink") 飾 梁柏傑
  - 2004年：[無名天使3D](../Page/無名天使3D.md "wikilink") 飾 李天華
  - 2004年：[心理心裏有個謎](../Page/心理心裏有個謎.md "wikilink") 飾 龍錦威（於2006年播映）
  - 2005年：[佛山贊師父](../Page/佛山贊師父.md "wikilink") 飾 隆科貝克（客串）
  - 2006年：[人生馬戲團](../Page/人生馬戲團.md "wikilink") 飾 甘霖
  - 2007-08年：[同事三分親](../Page/同事三分親.md "wikilink") 飾 武紀勇
  - 2008年：[搜神傳](../Page/搜神傳.md "wikilink") 飾 干將
  - 2008-10年：[畢打自己人](../Page/畢打自己人.md "wikilink") 飾 閆汝大（第二男主角）
  - 2011年：[誰家灶頭無煙火](../Page/誰家灶頭無煙火.md "wikilink") 飾 鍾國棟（第三男主角）
  - 2012年：[衝呀！瘦薪兵團](../Page/衝呀！瘦薪兵團.md "wikilink") 飾 高偉霆（第三男主角）
  - 2012年：[驚艷一槍](../Page/驚艷一槍_\(電視劇\).md "wikilink") 飾 元十三限

### 電視劇集（[香港電台](../Page/香港電台.md "wikilink")）

  - 2000年：非常平等任務
  - 2000年：法門2000（第一集：禍從口出）飾 趙忠強
  - 2000年：識食新人類
  - 2004年：鐵窗邊緣
  - 2005年：樓上樓下
  - 2005年：外判劇集:十封信
  - 2010年：功夫傳奇
  - 2015年：[獅子山下2015](../Page/獅子山下.md "wikilink")（第一集：一場飯局） 飾 黃家寶
  - 2018年：[火速救兵IV](../Page/火速救兵IV.md "wikilink") 飾 王智軍

### 電視電影

  - 2004年：[懸案追兇](../Page/懸案追兇.md "wikilink") 飾 周仁

### 電影

| 年份                                         | 片名                                            | 角色                                            |
| ------------------------------------------ | --------------------------------------------- | --------------------------------------------- |
| 1996                                       | [大內密探零零發](../Page/大內密探零零發.md "wikilink")      | 解剖「天外飛仙」那一場戲中一直念「重覆，重覆，又重覆」的司儀                |
| [正牌香蕉俱樂部](../Page/正牌香蕉俱樂部.md "wikilink")   | BTV「今天睇多啲」記者                                  |                                               |
| [七月十三之龍婆](../Page/七月十三之龍婆.md "wikilink")   | 龍婆死去的兒子                                       |                                               |
| [血腥Friday](../Page/血腥Friday.md "wikilink") | 劉偉健                                           |                                               |
| [食神](../Page/食神_\(電影\).md "wikilink")      | 食神大賽司儀                                        |                                               |
| 1997                                       | [香港大夜總會](../Page/香港大夜總會.md "wikilink")        | Michael                                       |
| 1998                                       | [無常](../Page/無常.md "wikilink")                | 自導                                            |
| [理想](../Page/理想.md "wikilink")             | 自導自演                                          |                                               |
| [想見你](../Page/想見你.md "wikilink")           | Simon（客串）                                     |                                               |
| 1999                                       | [原始武器](../Page/原始武器.md "wikilink")            | 李明生                                           |
| [1959某日某](../Page/1959某日某.md "wikilink")   | 李小龍/細鳳                                        |                                               |
| 2000                                       | [行規](../Page/行規.md "wikilink")                | 韋Sir                                          |
| [無法無天](../Page/無法無天.md "wikilink")         | 狄高                                            |                                               |
| [生死拳速](../Page/生死拳速.md "wikilink")         | 打手                                            |                                               |
| [我在監獄的日子速](../Page/我在監獄的日子速.md "wikilink") | 阿峰                                            |                                               |
| 2009                                       | [竊聽風雲](../Page/竊聽風雲.md "wikilink")            | 阿華「是馬志華的貼身保鑣,戲份不多但卻是關鍵人物」                     |
| 2010                                       | [火龍](../Page/火龍_\(電影\).md "wikilink")         | 紀少群（角色配音，由[任賢齊飾演](../Page/任賢齊.md "wikilink")） |
| [線人](../Page/線人_\(電影\).md "wikilink")      | 巴閉（角色配音，由[陸毅飾演](../Page/陸毅.md "wikilink")）    |                                               |
| [飛砂風中轉](../Page/飛砂風中轉.md "wikilink")       | 王經理                                           |                                               |
| 2011                                       | [報應](../Page/報應_\(電影\).md "wikilink")         | 姚啟初（角色配音，由[任賢齊飾演](../Page/任賢齊.md "wikilink")） |
| [反斗車王2](../Page/反斗車王2.md "wikilink")       | 聲演 Guido,Sarge,Mack,Francesco Bernoulli       |                                               |
| [奪命金](../Page/奪命金.md "wikilink")           | 張正方（角色配音，由[任賢齊飾演](../Page/任賢齊.md "wikilink")） |                                               |
| [關雲長](../Page/關雲長.md "wikilink")           | 韓福（角色配音，由[聶遠飾演](../Page/聶遠.md "wikilink")）    |                                               |
| 2013                                       | [飛虎出征](../Page/飛虎出征.md "wikilink")            | 談判專家                                          |
| [激戰](../Page/激戰.md "wikilink")             | 坤叔（程輝的師父）                                     |                                               |
| 2014                                       | [魔警](../Page/魔警.md "wikilink")                | 「鬼王黨」成員 堂倌                                    |
| [Z風暴](../Page/Z風暴.md "wikilink")           | 廉政公署調查員 安達                                    |                                               |
| 2015                                       | [吉星高照2015](../Page/吉星高照2015.md "wikilink")    |                                               |
| [五個小孩的校長](../Page/五個小孩的校長.md "wikilink")   | 呂慧紅兄                                          |                                               |
| [破風](../Page/破風_\(電影\).md "wikilink")      | 韓國社團大佬                                        |                                               |
| 2016                                       | [樹大招風](../Page/樹大招風.md "wikilink")            | 胡警官                                           |
| [三人行](../Page/三人行_\(電影\).md "wikilink")    | 溫敬棠                                           |                                               |
| 2017                                       | [救殭清道夫](../Page/救殭清道夫.md "wikilink")          | 張天龍（男配角）                                      |
| [毒·誡](../Page/毒·誡.md "wikilink")           |                                               |                                               |
| [明月幾時有](../Page/明月幾時有.md "wikilink")       |                                               |                                               |
| [空手道](../Page/空手道_\(電影\).md "wikilink")    | 啞狗（第三男主角）                                     |                                               |
| 2018                                       | [黑警](../Page/黑警.md "wikilink")（籌備中）           | 許尚偉（男配角）                                      |

### 節目主持

  - 1990年至1991年：[下午茶](../Page/下午茶_\(亞視婦女節目\).md "wikilink")（[亞洲電視](../Page/亞洲電視.md "wikilink")）
  - 1994年：[今日睇真D](../Page/今日睇真D.md "wikilink")（亞洲電視）
  - [美味夫妻檔](../Page/美味夫妻檔.md "wikilink")（年份不詳）
  - 2005年：[香港直播](../Page/香港直播.md "wikilink")（[無綫電視](../Page/無綫電視.md "wikilink")）
  - 2012年：功夫傳奇II 再戰江湖 沖繩空手道番外篇（[香港電台](../Page/香港電台.md "wikilink")）
  - 2012年：體通體透（[無綫電視](../Page/無綫電視.md "wikilink")）
  - 2016年：正義同盟（[有線電視](../Page/有線電視.md "wikilink")）
  - 2017年：無「恐」不入（奇妙電視）
  - 2018年：[晚吹](../Page/晚吹.md "wikilink") -
    罪光燈（[ViuTV](../Page/ViuTV.md "wikilink")）

## 参考文献

## 外部連結

  - [歐錦棠官方網頁](http://www.stephenau.com)

  -
  -
  - [劇道場 Theatre Dojo官方網站](http://www.theatredojo.com)

  -
  -
  -
  -
  -
  -
  - [歐錦棠。死佬重生日記](https://web.archive.org/web/20090906202100/http://www.theark.cc/myblog.php?blogname=StephenAu)
    歐錦棠網誌

  - [名人戰商界——歐錦棠倒貼辦劇社 夫妻檔搞舞台劇
    頭條日報](http://news.hkheadline.com/dailynews/headline_news_detail_columnist.asp?id=163233&section_name=wtt&kw=107)

[tam](../Category/歐姓.md "wikilink")
[Category:香港順德人](../Category/香港順德人.md "wikilink")
[Category:20世紀男演員](../Category/20世紀男演員.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港電視主持人](../Category/香港電視主持人.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港舞臺劇演員](../Category/香港舞臺劇演員.md "wikilink")
[Category:香港戲劇导演](../Category/香港戲劇导演.md "wikilink")
[Category:電影演員兼導演](../Category/電影演員兼導演.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:高主教書院校友](../Category/高主教書院校友.md "wikilink")

1.  [紀念結婚15週年 歐錦棠
    萬斯敏哭著「再婚」](http://paper.wenweipo.com/2012/04/29/EN1204290007.htm)，[文匯報](../Page/文匯報.md "wikilink")
2.  [StephenAu.com 棠聲](http://stephenau.com/words/words_main.html)
3.  [歐錦棠演音樂劇跌斷手](http://bka.mpweekly.com/20170919-81966)