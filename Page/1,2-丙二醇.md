**1,2-丙二醇**，也称作**[丙二醇](../Page/丙二醇.md "wikilink")**，是一种[有机化合物](../Page/有机化合物.md "wikilink")（[二](../Page/羟基.md "wikilink")[醇](../Page/醇.md "wikilink")），其[化學式為C](../Page/化學式.md "wikilink")<sub>3</sub>H<sub>8</sub>O<sub>2</sub>。丙二醇通常是略有甜味、无臭、无色透明的稠状液体。化學上，丙二醇屬於[二元醇的一種](../Page/二元醇.md "wikilink")，可與[水](../Page/水.md "wikilink")、[丙酮及](../Page/丙酮.md "wikilink")[氯仿等多種溶劑](../Page/氯仿.md "wikilink")[混溶](../Page/混溶.md "wikilink")。

丙二醇可通过[环氧丙烷的](../Page/环氧丙烷.md "wikilink")[水合作用製造](../Page/水合作用.md "wikilink")，主要用來生成[聚合物](../Page/聚合物.md "wikilink")，但也被用作食品添加劑，其[E編碼為E](../Page/E編碼.md "wikilink")1520。

丙二醇的[同分異構物为一個稳定](../Page/同分異構物.md "wikilink")和一个不稳定的[偕二醇](../Page/偕二醇.md "wikilink")[2,2-丙二醇](../Page/2,2-丙二醇.md "wikilink")。

## 應用

45%的丙二醇被用來作為生成不飽和聚酯樹脂的化學原料。丙二醇會和不飽和的[順丁烯二酸酐及](../Page/順丁烯二酸酐.md "wikilink")[間苯二甲酸反應生成](../Page/間苯二甲酸.md "wikilink")[共聚物](../Page/共聚物.md "wikilink")，再進一步[交聯形成](../Page/交叉鏈接.md "wikilink")[熱固性塑膠](../Page/熱固性聚合物.md "wikilink")。另外，丙二醇亦能與[環氧丙烷反應生成用來製造](../Page/環氧丙烷.md "wikilink")[聚氨酯的](../Page/聚氨酯.md "wikilink")[低聚物及聚合物](../Page/低聚物.md "wikilink")\[1\]
。

丙二醇亦被用作食品及煙草製品中的[保濕劑](../Page/保濕劑.md "wikilink")(E1520)、溶劑與防腐劑，亦與[植物甘油同為](../Page/甘油.md "wikilink")[電子煙內](../Page/電子煙.md "wikilink")「電子液體」(e-liquid)的主要成份之一。用來傳遞藥物或個人護理產品的[蒸發器中](../Page/蒸發器.md "wikilink")，所蒸發的成份通常會包含丙二醇\[2\]。丙二醇亦被作為多[藥物的溶劑](../Page/藥物治療.md "wikilink")，包括[口服](../Page/口服.md "wikilink")、[注射及](../Page/注射.md "wikilink")[外用等用途](../Page/外用.md "wikilink")，如[不溶於水的](../Page/溶解性.md "wikilink")[地西泮及](../Page/地西泮.md "wikilink")[蘿拉西泮](../Page/蘿拉西泮.md "wikilink")\[3\]。

和[乙二醇一樣](../Page/乙二醇.md "wikilink")，丙二醇能降低水的冰點，所以被用作飛機的[除冰劑](../Page/除冰.md "wikilink")\[4\]\[5\]。水和丙二醇的混合作會被染成粉紅色，以表示其相對無毒，並以[露營車或海洋防凍劑的名稱銷售](../Page/露營車.md "wikilink")。丙二醇經常用作乙二醇的替代品，作為低毒性、對環境友善的汽車[防凍劑](../Page/防凍劑.md "wikilink")。丙二醇亦被用於作為空屋中管道系統冬天防凍的工具\[6\]。水和丙二醇的比例為40:60時的[共晶溫度為](../Page/共晶系統.md "wikilink")-60°C\[7\]\[8\]，但一般防凍劑的水會比較多，為60:40，其共晶溫度為-45°C\[9\]。丙二醇是化油劑（曾被用於清理[2010年墨西哥灣漏油事故中的漏油](../Page/2010年墨西哥灣漏油事故.md "wikilink")）中的少量成份\[10\]\[11\]。

丙二醇被用作治療[反芻動物](../Page/反芻.md "wikilink")[高血酮症的口服動物用藥](../Page/高血酮症.md "wikilink")。非反芻動物的高血酮症通常會使用葡萄糖來治療，但因為葡萄糖會被反芻動物[瘤胃中的微生物所吸收](../Page/瘤胃.md "wikilink")，所以不是很有效。丙二醇會在瘤胃中被部分[代謝成](../Page/代謝.md "wikilink")，作為能量來源。其餘則被吸收至血液中，在肝臟中用來進行[葡萄糖新生](../Page/葡萄糖新生.md "wikilink")\[12\]。

## 参见

  - [甲醇](../Page/甲醇.md "wikilink")
  - [乙醇](../Page/乙醇.md "wikilink")
  - [乙二醇](../Page/乙二醇.md "wikilink")
  - [丙醇](../Page/丙醇.md "wikilink")
  - [丁醇](../Page/丁醇.md "wikilink")
  - [聚丙二醇](../Page/聚丙二醇.md "wikilink")

## 参考文献

[Category:家用化学品](../Category/家用化学品.md "wikilink")
[Category:醇类溶剂](../Category/醇类溶剂.md "wikilink")
[Category:化妆品化学品](../Category/化妆品化学品.md "wikilink")
[Category:食品添加剂](../Category/食品添加剂.md "wikilink")
[Category:赋形剂](../Category/赋形剂.md "wikilink")
[Category:二醇](../Category/二醇.md "wikilink")
[Category:三碳有机物](../Category/三碳有机物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.

2.
3.

4.
5.

6.

7.

8.

9.

10.

11.

12.