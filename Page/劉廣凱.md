**劉廣凱**（），[遼寧](../Page/遼寧.md "wikilink")[海城縣人](../Page/海城縣.md "wikilink")，曾任[中華民國海軍總司令](../Page/中華民國海軍.md "wikilink")，但由於1965年8月6日爆發的[八六海戰慘敗](../Page/八六海戰.md "wikilink")，直接導致中華民國為[反攻大陸進行的](../Page/反攻大陸.md "wikilink")[國光計畫徹底失敗而引咎辭職](../Page/國光計畫.md "wikilink")，只短暫任職七個月，屬於中華民國海軍青島系。歷任艦隊司令、海軍總司令、[聯勤總司令](../Page/聯勤.md "wikilink")。歷經[抗日](../Page/八年抗戰.md "wikilink")、[國共內戰多次戰役](../Page/國共內戰.md "wikilink")，獲得過中華民國軍職的[寶鼎](../Page/寶鼎勳章.md "wikilink")、[雲麾等勳章](../Page/雲麾勳章.md "wikilink")\[1\]。

## 生平

### 臺灣時期

1954年，從[國防大學聯戰班第二期結業](../Page/國防大學.md "wikilink")。

劉廣凱資歷完整，中英文在海軍軍官中亦屬中規中矩，在政治上接近[桂永清](../Page/桂永清.md "wikilink")，故桂永清對劉廣凱頗為倚重；但重視程度次於[馬紀壯](../Page/馬紀壯.md "wikilink")，故發展速度一直在同學馬紀壯之後。

劉廣凱一向不喜[福建人](../Page/福建.md "wikilink")，當海軍總部把剛從[海軍軍官學校畢業的](../Page/海軍軍官學校.md "wikilink")[葉昌桐分發至劉的侍從官時](../Page/葉昌桐.md "wikilink")，曾遭一段時日的冷凍。但不久，劉感到葉昌桐確有本事，日後劉廣凱至[大陳島擔任特種任務艦隊司令後](../Page/大陳島.md "wikilink")，就帶著葉兼任作戰官；葉把握機會立功，所以劉廣凱一向對葉極為賞識。

劉廣凱的長處是能接受部屬意見，擔任總司令時當部屬曉得[解放軍有飛彈後](../Page/解放軍.md "wikilink")，將影響兩岸海軍作戰情勢，部屬乃找機會向劉建議，海軍應該加速飛彈化。劉聽後馬上接受，想盡辦法加速中華民國海軍的飛彈化。

劉廣凱的作風，甚至他的學長楊元忠都認為是非常「軍閥」。其部屬的看法則是，劉廣凱不願意與他不喜歡的人打交道，因此得罪不少人。如劉廣凱和[黎玉璽頗為水火不容](../Page/黎玉璽.md "wikilink")，當劉擔任黎玉璽總司令的副總司令時，黎想打擊[俞柏生和](../Page/俞柏生.md "wikilink")[宋長志兩人](../Page/宋長志.md "wikilink")，則把前途看好的兩人調到國防部當助理次長，劉廣凱曉得以後，和黎攤牌，說：「*你幹總司令不是幹電雷的總司令，是大家的總司令。如果今天你要他們兩人調到國防部當助理次長的話，我告訴你，我給老先生（按：[蔣中正](../Page/蔣中正.md "wikilink")）打報告辭職，並把辭職的理由告訴老先生*。」這樣就阻止了黎玉璽的意圖。

宋長志與劉廣凱接近的原因，因宋長志可說全是劉一手提拔的。因為宋不是從艦隊上來的，他擔任六年官校校長後，沒地方調，劉於是向[彭孟緝溝通](../Page/彭孟緝.md "wikilink")，希望提拔宋長志，由於兩人私交甚篤，乃接受劉的建議。宋長志在一軍區等劉廣凱擔任總司令後，就獲得提拔當參謀長。宋也就是在參謀長任內接觸了[蔣經國](../Page/蔣經國.md "wikilink")，所以才有後來的發展。

## 参考文献

## 外部參考

  - 戴寶村：《[臺灣全志](../Page/臺灣全志.md "wikilink")》職官志武職表，南投：[國史館臺灣文獻館](../Page/國史館.md "wikilink")，[民國](../Page/民國.md "wikilink")93年12月初版。
  - [劉廣凱](http://www.cna.edu.tw/~na/chinese/historydata/History_services5_8.pdf)
    -
    [海軍軍官學校](../Page/海軍軍官學校.md "wikilink")（[PDF](../Page/PDF.md "wikilink")）
  - [海軍人物訪問紀錄
    第二輯](http://www.mh.sinica.edu.tw/PGPublication_Detail.aspx?tmid=3&mid=44&pubid=387&majorTypeCode=1&minorTypeCode=3&major=1&minor=3)-
    收錄[陳在和](../Page/陳在和.md "wikilink")、[徐學海](../Page/徐學海.md "wikilink")、[馬順義三位先生訪問稿](../Page/馬順義.md "wikilink")，[中央研究院](../Page/中央研究院.md "wikilink")，出版年：2002，開本：25開，頁數：266，[pdf](http://www.mh.sinica.edu.tw/MHDocument/Publication/Publication_387.pdf)

{{-}}   |- |colspan="3"
style="text-align:center;"|**[中華民國](../Page/中華民國.md "wikilink")[國防部](../Page/中華民國國防部.md "wikilink")**
      [category:中華民國總統府戰略顧問](../Page/category:中華民國總統府戰略顧問.md "wikilink")

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中華民國聯勤總司令](../Category/中華民國聯勤總司令.md "wikilink")
[Category:中華民國海軍總司令](../Category/中華民國海軍總司令.md "wikilink")
[Category:中華民國海軍二級上將](../Category/中華民國海軍二級上將.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中華民國海軍軍官學校校友](../Category/中華民國海軍軍官學校校友.md "wikilink")
[Category:台灣戰後東北移民](../Category/台灣戰後東北移民.md "wikilink")
[Category:海城人](../Category/海城人.md "wikilink")
[G廣凱](../Category/劉姓.md "wikilink")

1.  [後備指揮部國軍示範公墓--海軍二級上將劉廣凱勳略](http://afrc.mnd.gov.tw/Cemetery/meritorious/49.html)