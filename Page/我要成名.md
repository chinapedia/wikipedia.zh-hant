是2006年由[劉國昌執導的一部](../Page/劉國昌.md "wikilink")[香港電影](../Page/香港電影.md "wikilink")，[劉青雲和](../Page/劉青雲.md "wikilink")[霍思燕主演](../Page/霍思燕.md "wikilink")。亦獲得不少影壇當紅演員以及工作人員的鼎力支持，包括[梁家輝](../Page/梁家輝.md "wikilink")、[鄭伊健](../Page/鄭伊健.md "wikilink")、[周麗淇](../Page/周麗淇.md "wikilink")、[薛凱琪](../Page/薛凱琪.md "wikilink")、[黎耀祥](../Page/黎耀祥.md "wikilink")、[余安安](../Page/余安安.md "wikilink")、[劉丹](../Page/劉丹.md "wikilink")、[谷祖琳](../Page/谷祖琳.md "wikilink")。幕後人員包括[董瑋](../Page/董瑋.md "wikilink")、[陳嘉上](../Page/陳嘉上.md "wikilink")、[許鞍華](../Page/許鞍華.md "wikilink")、[趙良駿等均以真實身份出演](../Page/趙良駿.md "wikilink")，使得觀衆在欣賞整部電影時更加貼近電影界最真實的一面。刘青云凭借本片获得第一座[香港电影金像奖最佳男主角](../Page/香港电影金像奖最佳男主角.md "wikilink")。

## 情节

劇情是介紹香港[娛樂圈的現實生活](../Page/娛樂圈.md "wikilink")，由實力派演員潘家輝及其內地徒弟新晉女演員吳曉菲演繹出娛樂圈中一夜成名的辛酸歷程以及星海浮沉的故事。

## 演員

  - [劉青雲](../Page/劉青雲.md "wikilink") 飾 潘家輝
  - [霍思燕](../Page/霍思燕.md "wikilink") 飾 吳曉菲
  - [余安安](../Page/余安安.md "wikilink") 飾 琪琪
  - [黎耀祥](../Page/黎耀祥.md "wikilink") 飾 阿偉
  - [江漢](../Page/江漢.md "wikilink") 飾 輝父
  - [梁珊](../Page/梁珊.md "wikilink") 飾 輝母
  - [曾國祥](../Page/曾國祥.md "wikilink") 飾 樂仔

### 友情演出

  - 演員
      - [梁家輝](../Page/梁家輝.md "wikilink")
      - [鄭伊健](../Page/鄭伊健.md "wikilink")
      - [薛凱琪](../Page/薛凱琪.md "wikilink")
      - [周麗淇](../Page/周麗淇.md "wikilink")
      - [蔡一傑](../Page/蔡一傑.md "wikilink")
      - [蔡一智](../Page/蔡一智.md "wikilink")
      - [蘇志威](../Page/蘇志威.md "wikilink")
      - [劉丹](../Page/劉丹.md "wikilink")
      - [谷祖琳](../Page/谷祖琳.md "wikilink")
      - [江美儀](../Page/江美儀.md "wikilink")
      - [谷峰](../Page/谷峰.md "wikilink")
      - [方平](../Page/方平.md "wikilink")
  - 導演
      - [許鞍華](../Page/許鞍華.md "wikilink")
      - [陳嘉上](../Page/陳嘉上.md "wikilink")
      - [趙良駿](../Page/趙良駿.md "wikilink")
      - [董瑋](../Page/董瑋.md "wikilink")
      - [陳果](../Page/陳果.md "wikilink")
      - [崔允信](../Page/崔允信.md "wikilink")
      - [陸劍明](../Page/陸劍明.md "wikilink")
      - [鄧漢強](../Page/鄧漢強.md "wikilink")

## 獎項

<table>
<caption><a href="../Page/2007年香港電影金像獎.md" title="wikilink">香港電影金像獎（第26屆）</a></caption>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>名單</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>最佳男主角</p></td>
<td><p><a href="../Page/劉青雲.md" title="wikilink">劉青雲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女配角</p></td>
<td><p><a href="../Page/余安安.md" title="wikilink">余安安</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳新演員</p></td>
<td><p><a href="../Page/霍思燕.md" title="wikilink">霍思燕</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - {{@movies|fnhk40843287}}

  -
  -
  -
  -
  -
  -
  -
[6](../Category/2000年代香港電影作品.md "wikilink")
[Category:香港剧情片](../Category/香港剧情片.md "wikilink")
[Category:刘国昌电影](../Category/刘国昌电影.md "wikilink")
[Category:香港電影金像獎最佳男主角獲獎電影](../Category/香港電影金像獎最佳男主角獲獎電影.md "wikilink")
[Category:阮世生编剧作品](../Category/阮世生编剧作品.md "wikilink")
[Category:黎允文配乐电影](../Category/黎允文配乐电影.md "wikilink")
[Category:博纳影业电影](../Category/博纳影业电影.md "wikilink")