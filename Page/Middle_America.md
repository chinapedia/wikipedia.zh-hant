**Middle
America**，常译作“（美国的）**心脏地带**”“（美国的）**小镇价值**”“**半个美国**”“**美国中产阶级**”“**中间美国**”“**中美**”“**中部美国**”，是[美式英语中的一句](../Page/美式英语.md "wikilink")[俗语](../Page/俗语.md "wikilink")
，用于描述地理上的美国大部分乡村与郊区的文化状态和区域特征。此术语通常在与[城市进行比较时提及](../Page/城市群.md "wikilink")：这两种状态反应的不同价值观刻画出美国文化上的分歧。

## 作为文化和地理的描述词

[在地理上](../Page/美国地理.md "wikilink")，“Middle
America”指的是居于[美国东岸](../Page/美国东岸.md "wikilink")（特别是[东北部](../Page/美国东北部.md "wikilink")）与[西岸之间的区域](../Page/美国西岸.md "wikilink")。这个词语在某些场合会被作为沿海州份内陆地区的指代，特别是当这些地区为[乡村时](../Page/乡村.md "wikilink")。大部分的[加利福尼亚州中部谷地和](../Page/中部谷地_\(加利福尼亚州\).md "wikilink")[宾夕法尼亚州内陆部分通常被作为](../Page/宾夕法尼亚州.md "wikilink")“Middle
American”。在这种情况下，“Middle
American”这个术语被用于描述[美国中部](../Page/美国中部.md "wikilink")
。

“Middle America”更常见的用法是作为文化上的描述词，而非地理上的描述词。在文化上，“Middle
America”可以指那些主要由[中产阶级](../Page/中产阶级.md "wikilink")、[新教徒与](../Page/新教.md "wikilink")[白人居民聚居的](../Page/白人.md "wikilink")[小镇或](../Page/城市.md "wikilink")[郊区](../Page/郊区.md "wikilink")。因此，这个词语常被画成漫画来讽刺1950年代的美国。“Middle
America”一般不包括[芝加哥](../Page/芝加哥.md "wikilink")（[美国第三大城市](../Page/美国.md "wikilink")，以及[全球十大城市之一](../Page/全球城市.md "wikilink")）或极度富有的[亞思朋这类地区](../Page/阿斯彭_\(科罗拉多州\).md "wikilink")。不过，[美国南部的沿海地区通常也包括在](../Page/美国南部.md "wikilink")“Middle
America”之中。另外，“Middle
America”这一以美国为中心定义的概念易同[中部美洲或](../Page/中部美洲.md "wikilink")[中美洲产生混淆](../Page/中美洲.md "wikilink")。

## 经济

“Middle
America”的经济通常是传统的农业，尽管现今绝大多数的“中部美国人”都居住在郊区。与美国沿岸相比，这些地区的房地产价格偏低（因为土地充裕），但经济上的差距并不十分明显。“Middle
America”地区的房地产价格要比沿海地区稳定，升值情况也明显慢于其他地区。

[Category:美国地区](../Category/美国地区.md "wikilink")