**執政官**一詞是[漢語對其他一些](../Page/漢語.md "wikilink")[語言中許多性質非常不同的官職的對應翻譯](../Page/語言.md "wikilink")。另外，[宋朝诸如](../Page/宋朝.md "wikilink")[元丰改制之前的](../Page/元丰改制.md "wikilink")[参知政事](../Page/参知政事.md "wikilink")，元丰改制以后的[尚书丞](../Page/尚书丞.md "wikilink")、[中书侍郎](../Page/中书侍郎.md "wikilink")、[门下侍郎](../Page/门下侍郎.md "wikilink")，以及[枢密院长贰官等副宰相之任也统称执政官](../Page/枢密院.md "wikilink")。

## 宋朝

### [东府](../Page/中书门下.md "wikilink")、[三省执政](../Page/三省.md "wikilink")

#### [元丰改制前](../Page/元丰改制.md "wikilink")

  - [参知政事](../Page/参知政事.md "wikilink")（员数不定，但[相参合计不超过五人](../Page/同平章事.md "wikilink")）

#### [元丰改制后](../Page/元丰改制.md "wikilink")

  - [尚书左丞](../Page/尚书左丞.md "wikilink")（一人）
  - [尚书右丞](../Page/尚书右丞.md "wikilink")（一人）
  - 不兼[尚书仆射之](../Page/尚书仆射.md "wikilink")[门下侍郎](../Page/门下侍郎.md "wikilink")（一人）
  - 不兼[尚书仆射之](../Page/尚书仆射.md "wikilink")[中书侍郎](../Page/中书侍郎.md "wikilink")（一人）

### [西府执政](../Page/枢密院.md "wikilink")

  - [枢密使](../Page/枢密使.md "wikilink")（枢密院长官，长官资历老者授此职，故与知枢密院事不并置，一人）
  - [知枢密院事](../Page/知枢密院事.md "wikilink")（枢密院长官，长官资历浅者授此职，故与枢密使不并置，一人，位低于枢密使）
  - [同知枢密院事](../Page/同知枢密院事.md "wikilink")（枢密院次官，员数不定，长官为知枢院时次官授此职，故与枢密副使不并置，位与知枢院同而高于枢副）
  - [枢密副使](../Page/枢密副使.md "wikilink")（枢密院次官，员数不定，长官为枢密使时次官授此职，故与同知枢院不并置，位低于同知枢院）
  - [签署枢密院事](../Page/签署枢密院事.md "wikilink")/签书枢密院事（枢密院副贰，员数不定，位次于同知枢院与枢副）
  - [同签署枢密院事](../Page/同签署枢密院事.md "wikilink")/同签书枢密院事（枢密院副贰，员数不定，位列西府执政之末）

## 外国官位

主要翻译作执政官。

<div align="center">

<table>
<thead>
<tr class="header">
<th><p>條目</p></th>
<th><p>在原語言中的原始形式</p></th>
<th><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/古希臘.md" title="wikilink">古希臘</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/執政官_(雅典).md" title="wikilink">執政官 (雅典)</a></p></td>
<td><p>；複數形式：</p></td>
<td><p>共有九人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/古羅馬.md" title="wikilink">古羅馬</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/執政官_(羅馬).md" title="wikilink">執政官 (羅馬)</a></p></td>
<td><p>；複數形式：；縮寫：</p></td>
<td><p>每任2人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法蘭西第一共和國.md" title="wikilink">法蘭西第一共和國</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第一執政.md" title="wikilink">執政官 (法國)</a></p></td>
<td><p>；複數形式：</p></td>
<td><p>每任3人，僅存在于1799年至1804年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聖馬力諾.md" title="wikilink">聖馬力諾</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聖馬力諾執政官列表.md" title="wikilink">執政官 (聖馬力諾)</a></p></td>
<td></td>
<td><p>每任2人</p></td>
</tr>
</tbody>
</table>

</div>

## 其他例子

[荷蘭共和國的](../Page/荷蘭共和國.md "wikilink")[省督也時常被譯為執政官](../Page/荷蘭省督.md "wikilink")；[基督教](../Page/基督教.md "wikilink")[异端派別](../Page/异端.md "wikilink")[保羅派所建立的泰夫里卡共和國的領袖也稱爲執政官](../Page/保羅派.md "wikilink")。最有名的保羅派執政官是[赫利梭黑](../Page/赫利梭黑.md "wikilink")，意即“金手執政官”。

## 其他

在現代，“執政官”也可能指這些事物：

  - [執政官 (遊戲)](../Page/執政官_\(遊戲\).md "wikilink")，EA出品的一款電子遊戲
  - [執政官
    (星際爭霸)](../Page/執政官_\(星際爭霸\).md "wikilink")，電子遊戲[星際爭霸中的一個單位](../Page/星際爭霸.md "wikilink")
  - [執政官
    (Travian)](../Page/執政官_\(Travian\).md "wikilink")，電腦策略網上大型遊戲[Travian中的一個兵種](../Page/Travian.md "wikilink")