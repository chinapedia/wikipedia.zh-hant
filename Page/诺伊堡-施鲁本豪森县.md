**诺伊堡-施鲁本豪森县**（Landkreis
Neuburg-Schrobenhausen）是[德国](../Page/德国.md "wikilink")[巴伐利亚州的一个县](../Page/巴伐利亚州.md "wikilink")，隶属于[上巴伐利亚行政区](../Page/上巴伐利亚行政区.md "wikilink")，首府[多瑙河畔诺伊堡](../Page/多瑙河畔诺伊堡.md "wikilink")。

## 華語譯名

由於兩岸對於德國行政區「**Landkreis**」翻譯的不同，台灣稱為「**-{zh-tw:郡;
zh-cn:郡}-**」；中國大陸稱為「**-{zh-tw:縣; zh-cn:县}-**」。

## 地理

诺伊堡-施鲁本豪森县北面与[艾希施泰特县](../Page/艾希施泰特县.md "wikilink")，东北面与[因戈尔施塔特市](../Page/因戈尔施塔特.md "wikilink")，东面与[伊尔姆河畔普法芬霍芬县](../Page/伊尔姆河畔普法芬霍芬县.md "wikilink")，南面与[艾夏赫-费里德贝格县](../Page/艾夏赫-费里德贝格县.md "wikilink")，西面与[多瑙-里斯县相邻](../Page/多瑙-里斯县.md "wikilink")。

[N](../Category/巴伐利亚州行政区划.md "wikilink")