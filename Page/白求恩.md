**亨利·诺尔曼·白求恩**\[1\]（；）是一位[加拿大著名](../Page/加拿大.md "wikilink")[胸外科](../Page/胸外科.md "wikilink")[医师](../Page/医师.md "wikilink")、医疗创新者及[人道主义者](../Page/人道主义.md "wikilink")、[加拿大共产党党员](../Page/加拿大共产党.md "wikilink")。

## 生平

白求恩出生在加拿大的[安大略省](../Page/安大略.md "wikilink")[格雷文赫斯特](../Page/格雷文赫斯特.md "wikilink")。他的祖先是[苏格兰人](../Page/苏格兰.md "wikilink")。他的祖父是医生，父亲是[基督教](../Page/基督教.md "wikilink")[长老会牧师](../Page/长老会.md "wikilink")，母亲是传教士。白求恩1916年从加拿大[多伦多大学医学院毕业](../Page/多伦多大学.md "wikilink")，获得医学博士学位。1916年加入[加拿大皇家海军成为一名中尉军医](../Page/加拿大皇家海军.md "wikilink")，参加第一次世界大战，往返于[欧洲](../Page/欧洲.md "wikilink")、加拿大之间。一战后，白求恩在[底特律行医](../Page/底特律.md "wikilink")。1923年到[苏格兰](../Page/苏格兰.md "wikilink")[爱丁堡参加](../Page/爱丁堡.md "wikilink")[英国外科医学会会员考试时结识了当地的一个富裕家庭出身时年](../Page/英国外科医学会.md "wikilink")22岁的弗兰西丝·坎贝尔·彭尼，二人相恋，于1924年结婚，婚后定居底特律。1926年白求恩患[肺结核](../Page/肺结核.md "wikilink")，认为不能把病传染给年仅25岁的妻子，独自前往纽约州的特鲁多疗养院治疗，妻子回到了爱丁堡，一年后法院判决离婚。由于白求恩自己就是胸外科医生，在自身上做了当时试验性的“人工气胸疗法”，并由此发明了一系列的胸外科手术器械，如“白求恩肋骨剪”。在他康复之后给弗兰西丝去信，二人于1928年在[蒙特利尔复婚](../Page/蒙特利尔.md "wikilink")。白求恩在[蒙特利尔](../Page/蒙特利尔.md "wikilink")[麦吉尔大学皇家维多利亚医院任胸外科医生](../Page/麦吉尔大学.md "wikilink")。很可能由于他见到了富人得了病可以治，穷人只有等死的情况，而信仰[共产主义](../Page/共产主义.md "wikilink")。1931年1月，白求恩在《加拿大医生》杂志发表一篇文章《从医疗事业中清除私利》：“穷人有穷人的肺结核，富人有富人的肺结核。穷人得了它就会死去，而富人却能活下来。”“让我们把盈利、私人经济利益从医疗事业中清除出去，使我们的职业因清除了贪得无厌的个人主义而变得纯洁起来。让我们把建筑在同胞们苦难之上的致富之道，看作是一种耻辱。”1933年白求恩成为[卡特威尔市圣心医院](../Page/卡特威尔.md "wikilink")[胸外科主任并当选美国胸外科学会理事](../Page/胸外科.md "wikilink")。然而由于白求恩过于专注工作，两人于1933年再次离婚。但二人仍然保持着通信联系。

白求恩1935年访问并出席在[苏联莫斯科召开的国际生理学大会](../Page/苏联.md "wikilink")，看到了那里医疗健康福利制度的优点，回国后大力推动全民健保，并曾身体力行为穷人免费医疗。1935年10月加入了[加拿大共产党](../Page/加拿大共产党.md "wikilink")。1936年10月29日至1937年5月率领一支加拿大医疗队到[西班牙作为支持国际反](../Page/西班牙.md "wikilink")[法西斯志愿者投身](../Page/法西斯.md "wikilink")[西班牙内战](../Page/西班牙内战.md "wikilink")。在此期间他创办了一个移动的伤员急救系统，成了日后被广泛采用的移动军事外科医院的雏形。为了输血以抢救失血过多的伤员，他发明了世界上第一种运输血液的方法。

[Norman_Bethune_China_1938.jpg](https://zh.wikipedia.org/wiki/File:Norman_Bethune_China_1938.jpg "fig:Norman_Bethune_China_1938.jpg")司令\]\]
[1967-06_1939年10月的白求恩在做手术.jpg](https://zh.wikipedia.org/wiki/File:1967-06_1939年10月的白求恩在做手术.jpg "fig:1967-06_1939年10月的白求恩在做手术.jpg")
1937年7月30日，白求恩参加美国洛杉矶医疗局举行的欢迎“西班牙人民之友”宴会，遇到了[中华全国各界救国联合会领导人之一的](../Page/中华全国各界救国联合会.md "wikilink")[陶行知](../Page/陶行知.md "wikilink")。陶行知向白求恩介绍了七七事变后中国的[抗日战争形势](../Page/抗日战争.md "wikilink")，白求恩向陶行知表示：“如果需要，我愿意到中国去！”
白求恩就此事向[加拿大共产党作了汇报](../Page/加拿大共产党.md "wikilink")，称“西班牙和中国都是同一场战争中的一部分”“中国比西班牙更加迫切需要医生”，“（白求恩）在西班牙的经验可以运用于中国的反法西斯战场”。1937年12月，受美共与加共的派遣，白求恩赴[纽约接受](../Page/纽约.md "wikilink")[国际援华委员会](../Page/国际援华委员会.md "wikilink")（由共产国际授意美共与加共推动组建，配合[宋庆龄主持的](../Page/宋庆龄.md "wikilink")[保卫中国大同盟为援华抗日做募捐选传](../Page/保卫中国大同盟.md "wikilink")）资助组建了加美医疗队准备赴华从事战地医疗工作。该医疗队的另外两人是：[北温尼伯的圣约瑟夫医院外科手术医疗部的精通中文的加拿大籍女护士简](../Page/北温尼伯.md "wikilink")·伊文（Jean
Ewen，或译作琼·尤恩，中文名于青莲）；临行前参加的美国外科医生帕森斯。
[白求恩使用的听诊器等.jpg](https://zh.wikipedia.org/wiki/File:白求恩使用的听诊器等.jpg "fig:白求恩使用的听诊器等.jpg")

1938年1月8日，加美医疗队携带一批药品和手术器材，从[温哥华乘船赴中国](../Page/温哥华.md "wikilink")。登船前白求恩给前妻弗朗西丝写了告别信。1938年1月20日抵达香港，收到了[美国共产党中国局通知的](../Page/美国共产党中国局.md "wikilink")[八路军驻香港办事处负责人](../Page/八路军驻香港办事处.md "wikilink")[廖承志安排加美医疗队乘飞机至武汉](../Page/廖承志.md "wikilink")。在武汉等待北上期间，白求恩与伊文到汉阳长老会医院（今武汉市第五医院）帮助工作；医生帕森斯决定留在武汉国统区，白求恩明确拒绝了在[蒋中正所领导的](../Page/蒋中正.md "wikilink")[国民政府管辖区工作](../Page/国民政府.md "wikilink")，而转而帮助[中国共产党](../Page/中国共产党.md "wikilink")。经过在武汉的[军委会政治部副主任](../Page/国民政府军事委员会.md "wikilink")[周恩来安排](../Page/周恩来.md "wikilink")，1938年2月22日，白求恩与伊文一行运送医疗物资的数十辆大车离开武汉，经战乱中的河南、陕西[潼关渡黄河到山西](../Page/潼关.md "wikilink")[风陵渡](../Page/风陵渡.md "wikilink")、运城、至[临汾附近的八路军总部](../Page/临汾.md "wikilink")。这时遇到日军沿[同蒲铁路南下进攻](../Page/同蒲铁路.md "wikilink")，白求恩一行又经潼关转道于3月20日抵达西安，在[八路军驻西安办事处负责人](../Page/八路军驻西安办事处.md "wikilink")[林伯渠的安排下](../Page/林伯渠.md "wikilink")，最终于1938年3月31日抵达延安。4月1日与毛泽东会面时，白求恩说利用他从北美带来的医疗器械足以组建一支战地医疗队，到前线就近抢救重伤员；毛泽东对白求恩说的伤员立即手术将有75%的复原率很高兴，表示会大力支持他的工作。1938年5月2日白求恩率一批医护人员离开延安前往[晋察冀边区](../Page/晋察冀边区.md "wikilink")，成了一名战地医生。简·伊文留在了陕甘宁晋绥边区，后于1939年初赴皖南[新四军总部工作半年](../Page/新四军.md "wikilink")，1981年在加拿大出版回忆录详尽回忆了与白求恩赴华的全过程。\[2\]毛泽东致电[晋察冀军区司令员兼政委](../Page/晋察冀军区.md "wikilink")[聂荣臻](../Page/聂荣臻.md "wikilink")：“同意任白求恩为军区卫生顾问，对其意见和能力完全信任”。白求恩同晋察冀的木匠铁匠一起制造手术器械，他也帮助创立医生护士和医院勤务兵的训练，一再致信聂荣臻创办[军区卫生学校大量培养医护人员](../Page/八路军晋察冀军区卫生学校.md "wikilink")。他也设计把包装箱用作手术台以及马背药箱。他曾创下了冀中[齐会战斗在](../Page/齐会战斗.md "wikilink")69个小时内为115名伤员动手术的纪录。他在中国时期曾和[毛泽东通信](../Page/毛泽东.md "wikilink")。白求恩在中国的一年九个月另十七天，写了很多信与文章寄给美国加拿大的机构与友人，向国际社会宣传中国的抗战形势，呼吁国际社会为中国抗战提供经费、物资和人员援助。

白求恩原定于1939年10月20日启程短暂回北美为组建八路军后方医院与晋察冀军区卫生学校筹款。这时遇上了日军发动对晋察冀冬季大扫荡。白求恩选择留下来参与反扫荡作战的医疗救治。1939年10月29日[涞源县摩天岭战斗中为一名腿部受重伤的伤员做手术](../Page/涞源县.md "wikilink")，在日军迫近手术所时为加快手术进程，白求恩把左手伸进伤口掏取碎骨，一片碎骨刺破了白求恩的中指。3天后的11月1日白求恩为一名颈部患[丹毒合并](../Page/丹毒.md "wikilink")[蜂窝组织炎的伤员做手術](../Page/蜂窝组织炎.md "wikilink")，手指处的开放创口遭到感染。11月12日因[败血症在](../Page/败血症.md "wikilink")[河北省](../Page/河北省.md "wikilink")[唐县黄石口村去世](../Page/唐县.md "wikilink")。去世前一天的遗书中“请求国际援华委员会给我的离婚妻子拨一笔生活的款子，分期给也可以”“向她说明，我是十分抱歉的，也告诉她，我曾经是很愉快的”。

1939年12月1日，延安举行了白求恩追悼大会。

## 动机

[Norman_Bethune_transfusion_unit_1936.jpg](https://zh.wikipedia.org/wiki/File:Norman_Bethune_transfusion_unit_1936.jpg "fig:Norman_Bethune_transfusion_unit_1936.jpg")
加拿大共产党认为，1935年白求恩加入该党是为了投入[国际共产主义运动](../Page/国际共产主义运动.md "wikilink")。他一加入[加拿大共产党就去了西班牙帮助反抗](../Page/加拿大共产党.md "wikilink")[法西斯主义的民主斗争](../Page/法西斯主义.md "wikilink")，接着又去中国帮助共产主义者对抗日本帝国主义。最新关于他的传记，所著的**，里面也记述了他拒绝在[蒋中正领导的](../Page/蒋中正.md "wikilink")[国民政府工作而去帮助](../Page/国民政府.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")。

## 纪念

### 中国

[A_statue_of_Norman_Bethune.jpg](https://zh.wikipedia.org/wiki/File:A_statue_of_Norman_Bethune.jpg "fig:A_statue_of_Norman_Bethune.jpg")内[白求恩墓前的白求恩像](../Page/白求恩墓.md "wikilink")。\]\]

1939年12月21日，[毛泽东为八路军政治部](../Page/毛泽东.md "wikilink")、卫生部将于1940年出版的《诺尔曼白求恩纪念册》专门写了一篇文章《[纪念白求恩](../Page/纪念白求恩.md "wikilink")》。文中说：“从前线回来的人说到白求恩，没有一个不佩服，没有一个不为他的精神所感动。晋察冀边区的军民，凡亲身受过白求恩医生的治疗和亲眼看过白求恩医生的工作的，无不为之感动。”
“我们大家要学习他毫无自私自利之心的精神。从这点出发，就可以变为大有利于人民的人。一个人的能力有大小，但只要有这点精神，就是一个高尚的人。”这篇文章后来和毛泽东的另外两篇同一时期的文章《[为人民服务](../Page/为人民服务.md "wikilink")》和《[愚公移山](../Page/愚公移山.md "wikilink")》一起在[文革期间成了所有](../Page/文革.md "wikilink")[中国大陆人都要背诵的](../Page/中国大陆.md "wikilink")“[老三篇](../Page/老三篇.md "wikilink")”。白求恩的名字也因此而家喻户晓。

他的遗体葬在唐县，1949年迁葬到[石家庄的](../Page/石家庄.md "wikilink")[華北军区烈士陵園](../Page/華北军区烈士陵園.md "wikilink")。陵园中仅有的两座雕像是为他和[柯棣华大夫立的](../Page/柯棣华.md "wikilink")。他是被中国政府树立雕像的第一位外国人。并以他命名成立了[白求恩国际和平医院和](../Page/白求恩国际和平医院.md "wikilink")[白求恩医科大学](../Page/白求恩医科大学.md "wikilink")(前身为白求恩主持创建的[八路军晋察冀军区卫生学校](../Page/八路军晋察冀军区卫生学校.md "wikilink")，亲自制定教学方针和教学计划，亲自修订教案并授课、带实习。该校沿革2000年并入[吉林大学](../Page/吉林大学.md "wikilink")，改名白求恩医学部)。
[The_tomb_of_Norman_Bethune.JPG](https://zh.wikipedia.org/wiki/File:The_tomb_of_Norman_Bethune.JPG "fig:The_tomb_of_Norman_Bethune.JPG")

1964年中国[上海天马电影制片厂和](../Page/上海天马电影制片厂.md "wikilink")[八一电影制片厂以他的事迹合拍了一部电影](../Page/八一电影制片厂.md "wikilink")《白求恩大夫》。编剧[赵拓](../Page/赵拓.md "wikilink")，总导演[张骏祥](../Page/张骏祥.md "wikilink")（[耶鲁大学毕业](../Page/耶鲁大学.md "wikilink")），总摄影[吴印咸](../Page/吴印咸.md "wikilink")。吴印咸曾经在白求恩生前拍摄过白求恩的纪录片和多幅照片。主演美国人[谭宁邦](../Page/谭宁邦.md "wikilink")（1916年-2001年，美国共产党员，1946年到中国，《[人民中国](../Page/人民中国.md "wikilink")》画报主编），演员有[田华](../Page/田华.md "wikilink")、[梁波罗](../Page/梁波罗.md "wikilink")、[村里](../Page/村里.md "wikilink")、[英若诚](../Page/英若诚.md "wikilink")、[扬在葆等](../Page/扬在葆.md "wikilink")。

2009年中国网民在互联网上评选“十大国际友人”，白求恩得票最多\[3\]。

1991年设立的**[白求恩奖章](../Page/白求恩奖章.md "wikilink")**，由[国家人事部](../Page/中华人民共和国人事部.md "wikilink")、[卫生部共同颁发](../Page/中华人民共和国卫生部.md "wikilink")，是对全国卫生系统模范个人的**最高行政奖励**。凡获得“白求恩奖章”者，同时接受荣誉证书和通报表彰，并享受省、部级[劳动模范待遇](../Page/劳动模范.md "wikilink")。每年颁发一次，每次获奖者1至10人不等。

### 加拿大

他在加拿大远不如在中国有名，就象白求恩故居纪念馆展厅里对他的评价和介绍写的一样：加拿大只是对他的医学贡献和创新天分绝对肯定，而在中国，他更多的是因为国际主义精神与共产主义精神，\[4\]被作为一位英雄和圣人来爱戴。

在他的出生地格雷文赫斯特，1970年以他名字命名了小镇的主要街道白求恩大道。加拿大[约克大学以他命名了一个](../Page/约克大学.md "wikilink")“诺尔曼·白求恩学院”，安大略省他家乡多倫多市士嘉堡区的一所中学\[5\]也以他命名。

[BethuneMemorialhouse.jpg](https://zh.wikipedia.org/wiki/File:BethuneMemorialhouse.jpg "fig:BethuneMemorialhouse.jpg")的国立**白求恩纪念馆**\]\]
[Norman_Bethune's_study_in_Gravenhurst.JPG](https://zh.wikipedia.org/wiki/File:Norman_Bethune's_study_in_Gravenhurst.JPG "fig:Norman_Bethune's_study_in_Gravenhurst.JPG")
1970年中加建交，[加拿大政府购下了白求恩的父亲在](../Page/加拿大政府.md "wikilink")[格雷文赫斯特的住宅](../Page/格雷文赫斯特.md "wikilink")，将其恢复到1890年白求恩诞生时的样子，又把邻居的白色房子布置成了“白求恩纪念馆”。故居的客厅，餐厅，书房、卧室等都还原了19世纪末白求恩在此居住时布置。2000年8月19日，中国血统的加拿大总督[伍冰枝到此为白求恩的铜像揭幕](../Page/伍冰枝.md "wikilink")\[6\]\[7\]。1996年白求恩故居被加拿大政府列入加拿大国家文化遗产。同年，白求恩被加拿大医学名人堂列为医学人物。

1977年加拿大拍了一部电影《白求恩》（*Bethune*）。

1998年他被正式接纳入“加拿大医学名人纪念堂”，2004年[加拿大广播公司评选](../Page/加拿大广播公司.md "wikilink")“[最伟大的加拿大人](../Page/最伟大的加拿大人.md "wikilink")”，他名列第26位，排在[席琳·狄翁](../Page/席琳·狄翁.md "wikilink")（27名）之前。

### 共同

1990年3月，为庆祝白求恩大夫诞辰100周年，中国和加拿大联合发行了纪念邮票\[8\]。

1990年，中、加、[法合拍了一部电影](../Page/法国.md "wikilink")《白求恩：一个英雄的成长》（**）。

2005年，中、加合作拍摄了一部电视剧《诺尔曼·白求恩》。

## 轶事

白求恩一生都热爱艺术，曾创作了很多艺术作品，包括肺结核的感悟，自画像，火炉，夜间手术台等等。多伦多大学医学院毕业之后，因为曾服役一战和加拿大海军的关系，他开始了在英国伦敦病童医院的学习实习生活，在那里他结交了很多艺术家朋友，艺术的热忱和奔放，给予了他的人生更广阔和激情的方向。

没有艺术上的专业培训，白求恩凭借频繁参观艺术馆，艺术圈朋友们的熏陶，阅读和自己的那点小聪明，很快学会了鉴别一件艺术品的优劣。在西伦敦医院工作时期，他了解到在西班牙和葡萄牙买到的廉价作品，很快能在伦敦卖到一个好价钱。他以此借钱经商，作为副业来支持自己在伦敦的社交生活和挥金如土的习惯
。\[9\]

## 参考文献

## 外部链接

  - [白求恩年表（英文）](http://www.biographi.ca/EN/ShowBio.asp?BioId=40096)
  - [白求恩年表（英文）](https://web.archive.org/web/20050609084512/http://collections.ic.gc.ca/heirloom_series/volume4/58-63.htm)
  - [白求恩故居（加拿大）](http://pc.gc.ca/baiqiuen)
  - [纪念白求恩（英文）](http://www.iisg.nl/~landsberger/nb.html)
  - [《纪念白求恩》](https://web.archive.org/web/20050517001903/http://xcb.ysu.edu.cn/jdzz/mzd/jnbqe.htm)

## 参见

  - [印度援华医疗队](../Page/印度援华医疗队.md "wikilink")：[柯棣华](../Page/柯棣华.md "wikilink")
  - 《[纪念白求恩](../Page/纪念白求恩.md "wikilink")》
  - [白求恩奖章](../Page/白求恩奖章.md "wikilink")
  - [白求恩军医学院](../Page/白求恩军医学院.md "wikilink")、[白求恩国际和平医院](../Page/白求恩国际和平医院.md "wikilink")
  - [加拿大共产党](../Page/加拿大共产党.md "wikilink")
  - [中国人民的老朋友](../Page/中国人民的老朋友.md "wikilink")
  - [白求恩模范病室旧址](../Page/白求恩模范病室旧址.md "wikilink")

{{-}}

[category:多倫多大學校友](../Page/category:多倫多大學校友.md "wikilink")

[Category:抗战外籍援华牺牲者](../Category/抗战外籍援华牺牲者.md "wikilink")
[Category:加拿大医学家](../Category/加拿大医学家.md "wikilink")
[Category:加拿大军医](../Category/加拿大军医.md "wikilink")
[Category:八路军军医](../Category/八路军军医.md "wikilink")
[Category:加拿大共产党党员](../Category/加拿大共产党党员.md "wikilink")
[Category:在中国的加拿大人](../Category/在中国的加拿大人.md "wikilink")
[Category:安大略省人](../Category/安大略省人.md "wikilink")
[Category:蘇格蘭裔加拿大人](../Category/蘇格蘭裔加拿大人.md "wikilink")
[Category:中国人民的老朋友](../Category/中国人民的老朋友.md "wikilink")
[Category:西班牙内战人物](../Category/西班牙内战人物.md "wikilink")

1.  “白求恩”专指“［［Norman
    Bethune］］”，其他“Bethune”翻译为“[贝修恩](../Page/贝修恩.md "wikilink")”，不译为“白求恩”，[新华社](../Page/新华社.md "wikilink")《[英语姓名译名手册](../Page/英语姓名译名手册.md "wikilink")》
2.  Jean Ewen: 《Canadian Nurse in China》 McClelland and Steward Limited.
    1981.
    中译本为：琼·尤恩著《在中国当护士的年月》黄诚,何兰译.北京:[时事出版社](../Page/时事出版社.md "wikilink"),1984。
3.  [“中国缘·十大国际友人”网络评选结果揭晓](http://news.xinhuanet.com/politics/2009-10/16/content_12248066.htm)[新华网](../Page/新华网.md "wikilink")，2009年10月16日，2009年10月16日造访
4.  毛泽东在《纪念白求恩》中评价：“这是什么精神？这是国际主义的精神，这是共产主义的精神”
5.  [Dr Norman Bethune Collegiate
    Institute](http://en.wikipedia.org/wiki/Dr_Norman_Bethune_Collegiate_Institute)
6.
7.
8.
9.  Roderick Stewart and Sharon Stewart， Phoenix: The Life of Norman
    Bethune， McGill-Queen's Universtiy Press，
    Montreal\&Kingston.London.Ithaca， 2011