[缩略图](https://zh.wikipedia.org/wiki/File:Ichinomiya_nukisaki_jinja_Soumon.JPG "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Godo_hydroelectric_power_station_1975.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Omama_Post_Office.jpg "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Ayado_Dam.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Green_Bell_21_003.JPG "fig:缩略图")\]\]

**群馬縣**（）是位於[關東地方中部的一個內陸縣](../Page/關東地方.md "wikilink")。人口超過190萬。群馬縣的行政中心是[前橋市](../Page/前橋市.md "wikilink")。境内的[高崎市是](../Page/高崎市.md "wikilink")[上越新幹線](../Page/上越新幹線.md "wikilink")、[長野新幹線](../Page/長野新幹線.md "wikilink")、[高崎線](../Page/高崎線.md "wikilink")、[信越本線](../Page/信越本線.md "wikilink")、[八高線](../Page/八高線.md "wikilink")、[兩毛線的交匯點](../Page/兩毛線.md "wikilink")，地理位置相當重要。群馬縣也是近代出日本首相比較多的一個縣。[福田赳夫](../Page/福田赳夫.md "wikilink")、[福田康夫父子](../Page/福田康夫.md "wikilink")，以及[中曾根康弘](../Page/中曾根康弘.md "wikilink")、[小淵惠三都是群馬縣出身](../Page/小淵惠三.md "wikilink")。

## 地理

### 位置

  - 位處[關東地方的西北部](../Page/關東.md "wikilink")，北為[福島縣和](../Page/福島縣.md "wikilink")[新潟縣](../Page/新潟縣.md "wikilink")，西為[長野縣](../Page/長野縣.md "wikilink")，南為[埼玉縣](../Page/埼玉縣.md "wikilink")，東為[栃木縣](../Page/栃木縣.md "wikilink")。

<!-- end list -->

  - 縣域除東南部的[關東平原外多為山地](../Page/關東平原.md "wikilink")，由著名的[活火山](../Page/活火山.md "wikilink")[淺間山開始](../Page/淺間山.md "wikilink")，有[榛名山](../Page/榛名山.md "wikilink")、[赤城山](../Page/赤城山.md "wikilink")、[妙義山等名山](../Page/妙義山.md "wikilink")。縣内諸川集合成[利根川往東流進](../Page/利根川.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")。

### 自然公園

  - [國立公園](../Page/國立公園.md "wikilink")

<!-- end list -->

  -
    [日光國立公園](../Page/日光國立公園.md "wikilink")、[上信越高原國立公園](../Page/上信越高原國立公園.md "wikilink")

<!-- end list -->

  - [國定公園](../Page/國定公園.md "wikilink")

<!-- end list -->

  -
    [妙義荒船佐久高原國定公園](../Page/妙義荒船佐久高原國定公園.md "wikilink")

### 地勢

北部有[赤城山](../Page/赤城山.md "wikilink")、[谷川岳等山](../Page/谷川岳.md "wikilink")，冬季降雪量多。
南部是[關東平原的北端](../Page/關東平原.md "wikilink")，[前橋市和](../Page/前橋市.md "wikilink")[高崎市是縣的中心](../Page/高崎市.md "wikilink")。[太田市設有](../Page/太田市.md "wikilink")[富士重工業的工場](../Page/富士重工業.md "wikilink")，與[高崎市同是有名的](../Page/高崎市.md "wikilink")[工業城市](../Page/工業城市.md "wikilink")。

縣域由於大多為山地，縣人口中的7成集中於南部。

  - [日本](../Page/日本.md "wikilink")、[關東地方](../Page/關東地方.md "wikilink")
  - **鄰接都道府縣:** [福島縣](../Page/福島縣.md "wikilink") -
    [新潟縣](../Page/新潟縣.md "wikilink") -
    [長野縣](../Page/長野縣.md "wikilink") -
    [埼玉縣](../Page/埼玉縣.md "wikilink") -
    [栃木縣](../Page/栃木縣.md "wikilink")
  - **平地**
      - [關東平野](../Page/關東平野.md "wikilink")
      - [沼田盆地](../Page/沼田盆地.md "wikilink")
  - **山地**
      - [越後山地](../Page/越後山地.md "wikilink")、[谷川岳](../Page/谷川岳.md "wikilink")、[武尊山](../Page/武尊山.md "wikilink")
      - [足尾山地](../Page/足尾山地.md "wikilink")、[皇海山](../Page/皇海山.md "wikilink")、[赤城山](../Page/赤城山.md "wikilink")
      - [淺間山](../Page/淺間山.md "wikilink")、[榛名山](../Page/榛名山.md "wikilink")
      - 妙義荒船國定公園（[妙義山](../Page/妙義山.md "wikilink")、[荒船山](../Page/荒船山.md "wikilink")）
      - 碓冰峠
  - **河川**
      - [利根川](../Page/利根川.md "wikilink")
          - [薄根川](../Page/薄根川.md "wikilink")、[發知川](../Page/發知川.md "wikilink")
          - [片品川](../Page/片品川.md "wikilink")、[塗川](../Page/塗川.md "wikilink")、[泙川](../Page/泙川.md "wikilink")、[根利川](../Page/根利川.md "wikilink")、[白澤川](../Page/白澤川.md "wikilink")
          - [沼尾川](../Page/沼尾川_\(利根川水系\).md "wikilink")
          - [吾妻川](../Page/吾妻川.md "wikilink")、[万座川](../Page/万座川.md "wikilink")、[白砂川](../Page/白砂川.md "wikilink")、[温川](../Page/温川.md "wikilink")、[四万川](../Page/四万川.md "wikilink")、[沼尾川](../Page/沼尾川_\(吾妻川水系\).md "wikilink")
          - [烏川](../Page/烏川.md "wikilink")、[碓氷川](../Page/碓氷川.md "wikilink")、[鏑川](../Page/鏑川.md "wikilink")、[鮎川](../Page/鮎川_\(利根川水系\).md "wikilink")、[神流川](../Page/神流川.md "wikilink")
          - [廣瀬川](../Page/廣瀬川_\(利根川水系\).md "wikilink")、[粕川](../Page/粕川_\(利根川水系\).md "wikilink")
      - [渡良瀬川](../Page/渡良瀬川.md "wikilink")
          - [桐生川](../Page/桐生川.md "wikilink")
  - **湖沼**
      - [尾瀬沼](../Page/尾瀬沼.md "wikilink")、[矢木澤水庫](../Page/矢木澤水庫.md "wikilink")、[渡良瀬遊水地](../Page/渡良瀬遊水地.md "wikilink")、[野反湖](../Page/野反湖.md "wikilink")、[群馬用水](../Page/群馬用水.md "wikilink")

## 主要交通網

### 縣内的高速公路

  - [東北自動車道](../Page/東北自動車道.md "wikilink")（限[館林IC](../Page/館林IC.md "wikilink")）
  - [關越自動車道](../Page/關越自動車道.md "wikilink")（[藤岡JCT](../Page/藤岡JCT.md "wikilink")-[谷川岳PA](../Page/谷川岳PA.md "wikilink")）
  - [上信越自動車道](../Page/上信越自動車道.md "wikilink")（藤岡JCT-[碓冰輕井澤IC](../Page/碓冰輕井澤IC.md "wikilink")）
  - [北關東自動車道](../Page/北關東自動車道.md "wikilink")（[高崎JCT](../Page/高崎JCT.md "wikilink")-[伊勢崎IC](../Page/伊勢崎IC.md "wikilink")）

### 縣内的國道

  - [國道17號](../Page/國道17號.md "wikilink")（[高崎市](../Page/高崎市.md "wikilink")-[利根郡](../Page/利根郡.md "wikilink")[水上町](../Page/水上町.md "wikilink")）　（中山道、三國街道・高澀繞道）
  - [國道18號](../Page/國道18號.md "wikilink")（高崎市-[安中市](../Page/安中市.md "wikilink")）　（中山道・豐岡繞道）
  - [國道50號](../Page/國道50號.md "wikilink")（[前橋市](../Page/前橋市.md "wikilink")-[太田市](../Page/太田市.md "wikilink")）（前橋水戶線）
  - [國道120號](../Page/國道120號.md "wikilink")（[沼田市](../Page/沼田市.md "wikilink")-利根郡[片品村](../Page/片品村.md "wikilink")）（日光沼田線）
  - [國道122號](../Page/國道122號.md "wikilink")（[綠市](../Page/綠市.md "wikilink")-[邑樂郡](../Page/邑樂郡.md "wikilink")[明和町](../Page/明和町_\(群馬縣\).md "wikilink")）　（日光東京線）（銅山街道）
  - [國道144號](../Page/國道144號.md "wikilink")（[吾妻郡](../Page/吾妻郡.md "wikilink")[長野原町](../Page/長野原町.md "wikilink")-吾妻郡[嬬戀村](../Page/嬬戀村.md "wikilink")）（長野原上田線）
  - [國道145號](../Page/國道145號.md "wikilink")（吾妻郡長野原町-[沼田市](../Page/沼田市.md "wikilink")）（長野原沼田線）
  - [國道146號](../Page/國道146號.md "wikilink")（吾妻郡長野原町）（長野原輕井澤線）
  - [國道254號](../Page/國道254號.md "wikilink")（[藤岡市](../Page/藤岡市.md "wikilink")-[甘樂郡](../Page/甘樂郡.md "wikilink")[下仁田町](../Page/下仁田町.md "wikilink")）（東京小諸線）（信州街道）
  - [國道291號](../Page/國道291號.md "wikilink")（利根郡水上町）
  - [國道292號](../Page/國道292號.md "wikilink")（吾妻郡長野原町）
  - [國道353號](../Page/國道353號.md "wikilink")（[桐生市](../Page/桐生市.md "wikilink")-吾妻郡[中之条町](../Page/中之条町.md "wikilink")）
  - [國道354號](../Page/國道354號.md "wikilink")（高崎市-[邑樂郡](../Page/邑樂郡.md "wikilink")[板倉町](../Page/板倉町_\(群馬縣\).md "wikilink")）（日光例幣使街道、古河街道・駒形線）

<!-- end list -->

  - [國道405號](../Page/國道405號.md "wikilink")（吾妻郡長野原町-吾妻郡[六合村](../Page/六合村.md "wikilink")）
  - [國道406號](../Page/國道406號.md "wikilink")（高崎市）
  - [國道407號](../Page/國道407號.md "wikilink")（太田市）（日光街道）
  - [國道462號](../Page/國道462號.md "wikilink")（[伊勢崎市](../Page/伊勢崎市.md "wikilink")-[多野郡](../Page/多野郡.md "wikilink")[上野村](../Page/上野村_\(群馬縣\).md "wikilink")）

### 縣内的縣道

  - [群馬縣縣道一覽](../Page/群馬縣縣道一覽.md "wikilink")

### 縣内的鐵道路線

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")
      - [上越新幹線](../Page/上越新幹線.md "wikilink")（…高崎站～上毛高原站…）
      - [北陸新幹線](../Page/北陸新幹線.md "wikilink")（…高崎站～安中榛名站…）
      - [高崎線](../Page/高崎線.md "wikilink")（高崎站～新町站…）
      - [信越本線](../Page/信越本線.md "wikilink")（高崎站～橫川站）
      - [上越線](../Page/上越線.md "wikilink")（高崎站～土合站…）
      - [吾妻線](../Page/吾妻線.md "wikilink")（澀川站～大前站）
      - [兩毛線](../Page/兩毛線.md "wikilink")（新前橋站～桐生站…）
      - [八高線](../Page/八高線.md "wikilink")（倉賀野站～群馬藤岡站…）

<!-- end list -->

  - [上信電鐵](../Page/上信電鐵.md "wikilink")
      - [上信線](../Page/上信電鐵上信線.md "wikilink")（高崎站～下仁田站）

<!-- end list -->

  - [上毛電氣鐵道](../Page/上毛電氣鐵道.md "wikilink")
      - [上毛線](../Page/上毛電氣鐵道上毛線.md "wikilink")（中央前橋站～西桐生站）

<!-- end list -->

  - [東武鐵道](../Page/東武鐵道.md "wikilink")
      - [伊勢崎線](../Page/東武伊勢崎線.md "wikilink")（…川俣站～多多良站…韮川站～伊勢崎站）
      - [日光線](../Page/東武日光線.md "wikilink")（限板倉東洋大前站）
      - [桐生線](../Page/東武桐生線.md "wikilink")（太田站～赤城站）
      - [佐野線](../Page/東武佐野線.md "wikilink")（館林站～渡瀬站…）
      - [小泉線](../Page/東武小泉線.md "wikilink")（太田站～東小泉站、館林站～西小泉站）

<!-- end list -->

  - [渡良瀬溪谷鐵道](../Page/渡良瀬溪谷鐵道.md "wikilink")
      - [渡良瀬溪谷線](../Page/渡良瀬溪谷鐵道渡良瀬溪谷線.md "wikilink")（桐生站～澤入站…）

### 縣内的機場

  - [群馬機場](../Page/群馬機場.md "wikilink")（前橋市）
  - [高崎機場](../Page/日本高崎机场.md "wikilink")（高崎市）
  - [大西飛行場](../Page/大西飛行場.md "wikilink")（館林市・邑樂町）〔民間・廢止〕

## 特產

| 水果・蔬菜                              |
| ---------------------------------- |
| [梅](../Page/梅.md "wikilink")       |
| [蒟蒻](../Page/蒟蒻.md "wikilink")     |
| 下仁田葱                               |
| [蘋果](../Page/蘋果.md "wikilink")     |
| 鄉土料理                               |
| 水澤[烏龍麵](../Page/烏龍麵.md "wikilink") |
| 桐生烏龍麵                              |
| 工藝品・民藝品                            |
| [伊勢崎絣](../Page/伊勢崎絣.md "wikilink") |
| [桐生織](../Page/桐生織.md "wikilink")   |
| 參考資料：\[1\]                         |

## 出身名人

## 註解

## 外部资源

  - [群馬縣观光物产协会](https://www.visitgunma.jp/cn/)
  - [群馬旅遊信息網站](https://web.archive.org/web/20100312081239/http://www.wind.ne.jp/g-kanko/chinese/)
    群馬觀光官方網站

[Q群](../Category/日本都道府縣.md "wikilink")
[\*](../Category/群馬縣.md "wikilink")

1.