[thumb](../Page/file:Yanosteus_longidorsalis_MHNT.jpg.md "wikilink")
*Yanosteus longidorsalis*\]\]

**鲟形目**（[学名](../Page/学名.md "wikilink")：）是分布于[北半球北部的一种古老](../Page/北半球.md "wikilink")[鱼种](../Page/物种.md "wikilink")，多为[洄游鱼类](../Page/洄游.md "wikilink")，其经济价值很高，正面临灭绝的危险。\[1\]

## 形态

鲟形目的鱼一般体形较大，尾鳍和背鳍很短，尾鳍上叶上有菱形硬鳞，口下位，吻延长出来。鲟科身上有五列大的菱形骨板，匙吻鲟科身上无骨板裸露无鳞。

## 分类

鲟形目目前只有两科：

  - [鲟科](../Page/鲟科.md "wikilink") Acipenseridae
      - [鲟亚科](../Page/鲟亚科.md "wikilink") Acipenserinae
      - [铲鲟亚科](../Page/铲鲟亚科.md "wikilink") Scaphirhynchinae
  - [匙吻鲟科](../Page/匙吻鲟科.md "wikilink") Polyodontidae

### 中国水域的鲟形目鱼类

  - 黑龙江、乌苏里江、松花江水系的[施氏鲟和](../Page/施氏鲟.md "wikilink")[鳇](../Page/鳇.md "wikilink")
  - 长江水系的[中华鲟](../Page/中华鲟.md "wikilink")、[达氏鲟](../Page/达氏鲟.md "wikilink")、[白鲟](../Page/白鲟.md "wikilink")
  - 新疆地区的[裸腹鲟](../Page/裸腹鲟.md "wikilink")、[小体鲟和](../Page/小体鲟.md "wikilink")[西伯利亚鲟](../Page/西伯利亚鲟.md "wikilink")

## 参考资料

[\*](../Category/鲟形目.md "wikilink")
[Category:瀕危物種](../Category/瀕危物種.md "wikilink")

1.