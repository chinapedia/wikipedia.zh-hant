[Cygnus_melancoryphus_MHNT.ZOO.2010.11.11.1.jpg](https://zh.wikipedia.org/wiki/File:Cygnus_melancoryphus_MHNT.ZOO.2010.11.11.1.jpg "fig:Cygnus_melancoryphus_MHNT.ZOO.2010.11.11.1.jpg")
**黑颈天鹅**（[学名](../Page/学名.md "wikilink")：）是小型[鴨科](../Page/鴨科.md "wikilink")[天鹅屬鳥類](../Page/天鹅屬.md "wikilink")，主要分布於[南美洲](../Page/南美洲.md "wikilink")[南回歸線以南的地區](../Page/南回歸線.md "wikilink")，是南美洲體型最大的原生雁鴨類。

體長1至1.4公尺，體重3.5-6.7公斤；雄性身型較雌性大。生活於淡水和鹹淡水沼澤。

## 參考來源

[Category:天鹅属](../Category/天鹅属.md "wikilink")
[Category:西半球候鳥](../Category/西半球候鳥.md "wikilink")