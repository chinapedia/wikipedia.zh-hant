[Pentagon.svg](https://zh.wikipedia.org/wiki/File:Pentagon.svg "fig:Pentagon.svg")
**正多边形**是所有角都相等、并且所有边都相等的[简单多边形](../Page/简单多边形.md "wikilink")，简单多边形是指在任何位置都不与自身相交的多边形。

所有具有同样边数的正多边形都是[相似多边形](../Page/相似.md "wikilink")。

## 示例

  - [等边三角形](../Page/等边三角形.md "wikilink") (正三角形)
  - [正方形](../Page/正方形.md "wikilink") (正四邊形)
  - 正[五边形](../Page/五边形.md "wikilink")
  - 正[六边形](../Page/六边形.md "wikilink")
  - 正[八边形](../Page/八边形.md "wikilink")
  - 正[十边形](../Page/十边形.md "wikilink")
  - 正[十二边形](../Page/十二边形.md "wikilink")

## 特性

正 *n* 边形每个[内角为](../Page/内角.md "wikilink")
\(\left (1-\frac{2}{n} \right) \times 180^\circ\) 或者表示为
\(\frac{(n-2)\times 180^\circ}{n}\)
[角度](../Page/角度.md "wikilink")。也可以用[弧度表示为](../Page/弧度.md "wikilink")\(\frac{(n-2)\pi}{n}\)或者\(\frac{n-2}{2n}\)。

正多边形的所有[顶点都在同一个](../Page/顶点.md "wikilink")[外接圆上](../Page/外接圆.md "wikilink")，每个正多边形都有一个外接圆。

正多边形可尺规做图[当且仅当正多边形的边数](../Page/当且仅当.md "wikilink") *n*
的[奇](../Page/奇数.md "wikilink")[质数因子是](../Page/质数.md "wikilink")[费马数](../Page/费马数.md "wikilink")。参见[可尺规作图的多边形](../Page/可尺规作图的多边形.md "wikilink")。

*n* \> 2 的正多边形的[对角线数目是](../Page/对角线.md "wikilink")
\(\frac{n (n-3)}{2}\)，如 0、2、5、9、... 等，这些对角线将多边形分成 1、4、 11、24、... 块。

## 面积

[Apothem_of_hexagon.svg](https://zh.wikipedia.org/wiki/File:Apothem_of_hexagon.svg "fig:Apothem_of_hexagon.svg")的垂直[边心距](../Page/边心距.md "wikilink")\]\]
正 *n* 边形的面积为

\[Deg : A=\frac{nt^2\sin(\frac{360}{n})}{4[1-\cos(\frac{360}{n})]}\]

\[Rad : A=\frac{nt^2\sin(\frac{2\pi}{n})}{4[1-\cos(\frac{2\pi}{n})]}\]
其中 *t* 是边长。正多边形的面积还等于多边形的周长与边心距离乘积的一半。边心距离是多边形中心到边的垂直距离。

如果 *t*=1 则正多边形的面积为,

\[Deg : A=\frac{n\sin(\frac{360}{n})}{4[1-\cos(\frac{360}{n})]}\]

\[Rad : A=\frac{n\sin(\frac{2\pi}{n})}{4[1-\cos(\frac{2\pi}{n})]}\]
从而可以得到

|                                     |                                       |             |
| ----------------------------------- | ------------------------------------- | ----------- |
| [3](../Page/三角形.md "wikilink")      | \(\frac{\sqrt{3}}{4}\)                | 0.433       |
| [4](../Page/正方形.md "wikilink")      | 1                                     | 1.000       |
| [5](../Page/五边形.md "wikilink")      | \(\frac {1}{4} \sqrt{25+10\sqrt{5}}\) | 1.720       |
| [6](../Page/六边形.md "wikilink")      | \(\frac{3 \sqrt{3}}{2}\)              | 2.598       |
| [7](../Page/七边形.md "wikilink")      |                                       | 3.634       |
| [8](../Page/八边形.md "wikilink")      | \(2 + 2 \sqrt{2}\)                    | 4.828       |
| [9](../Page/九边形.md "wikilink")      |                                       | 6.182       |
| [10](../Page/十边形.md "wikilink")     | \(\frac{5}{2} \sqrt{5+2\sqrt{5}}\)    | 7.694       |
| [11](../Page/十一边形.md "wikilink")    |                                       | 9.366       |
| [12](../Page/十二边形.md "wikilink")    | \(6+3\sqrt{3}\)                       | 11.196      |
| [13](../Page/十三边形.md "wikilink")    |                                       | 13.186      |
| 14                                  |                                       | 15.335      |
| [15](../Page/十五边形.md "wikilink")    |                                       | 17.642      |
| [16](../Page/十六边形.md "wikilink")    |                                       | 20.109      |
| [17](../Page/十七边形.md "wikilink")    |                                       | 22.735      |
| 18                                  |                                       | 25.521      |
| 19                                  |                                       | 28.465      |
| [20](../Page/二十边形.md "wikilink")    |                                       | 31.569      |
| [100](../Page/一百边形.md "wikilink")   |                                       | 795.513     |
| [1000](../Page/一千边形.md "wikilink")  |                                       | 79577.210   |
| [10000](../Page/一万边形.md "wikilink") |                                       | 7957746.893 |

n\<8
的正多边形的面积比同[周长的](../Page/周长.md "wikilink")[圆的面积小大约](../Page/圆.md "wikilink")
0.26，随着 *n* 的增加，这个差值趋近于 π/12。

## 对称性

*n*边多边形的[对称群](../Page/对称群.md "wikilink") 为 2*n* 阶的 [dihedral
group](../Page/dihedral_group.md "wikilink")
*D<sub>n</sub>*：*D*<sub>2</sub>,
[*D*<sub>3</sub>](../Page/Dihedral_group_of_order_6.md "wikilink"),
[*D*<sub>4</sub>](../Page/Examples_of_groups#A_symmetry_group.md "wikilink"),...
它包括 *C<sub>n</sub>* 中的 *n* 阶[旋转对称以及经过中心的](../Page/旋转对称.md "wikilink")
*n* 条轴线的[镜像对称](../Page/镜像对称.md "wikilink")。如果 *n*
是[偶数](../Page/偶数.md "wikilink")，则这些轴线中有一半经过相对的顶点，另外一半经过相对边的中点。如果
*n* 是[奇数](../Page/奇数.md "wikilink")，则所有的轴线都是经过一个顶点以及其相对边的中心。

## 非凸正多边形

正多边形的广义分类包括，例如[五角星与](../Page/五角星.md "wikilink")[五边形的顶点相同](../Page/五边形.md "wikilink")，但是顶点要交替相连。

示例：

  - [五角星](../Page/五角星.md "wikilink") - {5/2}
  - [七角星](../Page/七角星.md "wikilink") - {7/2}, {7/3}
  - [八角星](../Page/八角星.md "wikilink") - {8/3}
  - [九角星](../Page/九角星.md "wikilink") - {9/2}, {9/4}
  - [十角星](../Page/十角星.md "wikilink") - {10/3}

## 多面体

[正多面体是以正多边形作为面的](../Page/正多面体.md "wikilink")[多面体](../Page/多面体.md "wikilink")，因此对于每两个顶点来说都有一个[等距的映射将其中一点映射到另一点](../Page/等距.md "wikilink")。

## 参见

  - [正多邊形](../Page/正多邊形.md "wikilink")
  - [正多面體](../Page/正多面體.md "wikilink")
  - [正圖形列表](../Page/正圖形列表.md "wikilink")
  - [正多邊形鑲嵌](../Page/正多邊形鑲嵌.md "wikilink")
  - [外接圓](../Page/外接圓.md "wikilink")
  - [內切圓](../Page/內切圓.md "wikilink")
  - [外接球](../Page/外接球.md "wikilink")
  - [內切球](../Page/內切球.md "wikilink")

## 外部链接

  - [Mathworld: Regular
    Polygon](http://mathworld.wolfram.com/RegularPolygon.html)
  - [Regular Polygon
    description](http://www.mathopenref.com/polygonregular.html) With
    interactive animation
  - [Incircle of a Regular
    Polygon](http://www.mathopenref.com/polygonincircle.html) With
    interactive animation
  - [Area of a Regular
    Polygon](http://www.mathopenref.com/polygonregulararea.html) Three
    different formulae, with interactive animation

[de:Polygon\#Regelmäßige
Polygone](../Page/de:Polygon#Regelmäßige_Polygone.md "wikilink")

[Category:多邊形](../Category/多邊形.md "wikilink")
[Category:平面幾何](../Category/平面幾何.md "wikilink")