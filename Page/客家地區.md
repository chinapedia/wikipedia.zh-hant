[Idioma_hakka.png](https://zh.wikipedia.org/wiki/File:Idioma_hakka.png "fig:Idioma_hakka.png")
[Haaka_Gan_Min_Yue.png](https://zh.wikipedia.org/wiki/File:Haaka_Gan_Min_Yue.png "fig:Haaka_Gan_Min_Yue.png")
[hakkapopulationguangdong.png](https://zh.wikipedia.org/wiki/File:hakkapopulationguangdong.png "fig:hakkapopulationguangdong.png")
[hakkapopulationjiangxi.png](https://zh.wikipedia.org/wiki/File:hakkapopulationjiangxi.png "fig:hakkapopulationjiangxi.png")
[hakkapopulationfujian.png](https://zh.wikipedia.org/wiki/File:hakkapopulationfujian.png "fig:hakkapopulationfujian.png")
[hakkapopulationguangxi.png](https://zh.wikipedia.org/wiki/File:hakkapopulationguangxi.png "fig:hakkapopulationguangxi.png")

**客家地區**是指有較多[客家人聚集的地區](../Page/客家人.md "wikilink")。一般來說客家人比較集中在中國的閩粵贛交界處，然而其他地方廣東西部、廣西東部等等也有不少客家人居住。客家人是分佈範圍較廣的漢民系之一，因此客家地區也相當廣闊。

## 中國大陸客家分布地区

### 粵東

  - 今[廣東省](../Page/廣東省.md "wikilink")[梅州市](../Page/梅州市.md "wikilink")、[惠州市](../Page/惠州市.md "wikilink")、[河源市](../Page/河源市.md "wikilink")、[韶關市](../Page/韶關市.md "wikilink")、[深圳市](../Page/深圳市.md "wikilink")。其中[河源市是唯一純客家地級市](../Page/河源市.md "wikilink")。
  - 此區為[客家話主流音](../Page/客家話.md "wikilink")([粵東系](../Page/粵東系.md "wikilink"))分佈區，臺灣客家人亦多以此區為原鄉，粵臺片即得名于此。除粵臺片外，此區還以方言島的形式分佈著[水源音](../Page/水源音.md "wikilink")。水源音以河源市源城區為代表，惠州[老惠城話](../Page/老惠城話.md "wikilink")、紅海灣尖米話、博羅衍語，都是其分支。\*[明清時期](../Page/明清.md "wikilink")，此區屬[惠州府](../Page/惠州府.md "wikilink")、[韶州府](../Page/韶州府.md "wikilink")、[潮州府](../Page/潮州府.md "wikilink")、[廣州府](../Page/廣州府.md "wikilink")、[嘉應直隸州](../Page/嘉應.md "wikilink")。

### 贛南

  - 主要指今[江西省](../Page/江西省.md "wikilink")[贛州市](../Page/贛州市.md "wikilink")。
  - 贛州市轄區范圍內主要流行于桂片、寧龍片的客家話，不屬于客家話主流音([粵東系](../Page/粵東系.md "wikilink"))，但龍南、全南、定南、尋鄔等四縣基本屬于粵東音范疇。
  - 明清時期，此區屬[贛州府](../Page/贛州府.md "wikilink")、[寧都州](../Page/寧都州.md "wikilink")、[南安府](../Page/南安府.md "wikilink")。

### 閩西

  - 主要指今[福建省](../Page/福建省.md "wikilink")[龍岩市](../Page/龍岩市.md "wikilink")。
  - 龍巖市轄區范圍內主要流行粵臺片客家話，包括永定、上杭、武平和長汀縣南部都屬于粵東音；長汀縣北部屬清寧話，與贛州寧石方言較接近；連城縣客家話帶有少量閩語特征。
  - 明清時期，此區屬汀州府、龍巖州。另外[三明市寧化](../Page/三明市.md "wikilink")、清流兩縣也是純客住縣，其中[寧化](../Page/寧化縣.md "wikilink")[石壁被客家人視為祖地](../Page/石壁村.md "wikilink")。三明市[明溪縣](../Page/明溪縣.md "wikilink")(原汀州府歸化縣)屬于贛語(邵將方言)與客家話混合區。

### 地区称呼

客家四州：[梅州](../Page/梅州.md "wikilink")、[惠州](../Page/惠州.md "wikilink")、[赣州](../Page/赣州.md "wikilink")、[汀州](../Page/汀州.md "wikilink")(现[龙岩市及](../Page/龙岩市.md "wikilink")[三明市一带](../Page/三明市.md "wikilink"))
客家祖地：[宁化](../Page/宁化县.md "wikilink")[石壁](../Page/石壁村.md "wikilink")

客家通衢：[石城](../Page/石城.md "wikilink")

客家首府：[汀州](../Page/汀州.md "wikilink")

客家摇篮：[赣州](../Page/赣州.md "wikilink")

世界客都：[梅州](../Page/梅州.md "wikilink")

客家古邑：[河源](../Page/河源.md "wikilink")

客家吾州：[韶關](../Page/韶關.md "wikilink")（客家第五州）

### 其它

以粵閩贛交界帶之外，其余地區都沒有純客住縣。以下非客家大本營地區之中，客家人所占比例較大的縣域：

  - 粤西：茂名信宜、化州、湛江廉江
  - 廣西：博白、陸川、合浦、賀州、防城港、鐘山、昭平、貴港
  - 赣北：[铜鼓](../Page/铜鼓县.md "wikilink")
  - 湖南：[株洲](../Page/株洲市.md "wikilink")[炎陵](../Page/炎陵.md "wikilink")、[茶陵](../Page/茶陵.md "wikilink")、[攸縣](../Page/攸縣.md "wikilink")；湘南地區[汝城](../Page/汝城.md "wikilink")、[宜章](../Page/宜章.md "wikilink")、[桂東](../Page/桂東.md "wikilink")、[江華](../Page/江華.md "wikilink")；湘北地區[平江](../Page/平江.md "wikilink")、[瀏陽](../Page/瀏陽.md "wikilink")。

## 臺灣客家分布地區

[Taiwan_Hakka.svg](https://zh.wikipedia.org/wiki/File:Taiwan_Hakka.svg "fig:Taiwan_Hakka.svg")\]\]

[臺灣客家人口比例較高地區為](../Page/臺灣.md "wikilink")[桃竹苗](../Page/桃竹苗.md "wikilink")、[高屏](../Page/高屏.md "wikilink")[六堆](../Page/六堆.md "wikilink")，雲林的詔安縣及[花東縱谷](../Page/花東縱谷.md "wikilink")\[1\]\[2\]。

## 海外客家分布地区

在東南亞、南亞、非洲、歐洲、美洲、大洋洲，都有客家人分佈。海外華裔客家人的居住地，廣義上也可稱為客家地區：

  - 南洋：東南亞地區以印尼、緬甸、泰國、柬埔寨、马来西亚較多華裔客家籍居住。其中马来西亚每4个[华人就有一个是客家人](../Page/马来西亚华人.md "wikilink")，分散在马来西亚各大城镇，最高比例的城镇为[怡保市](../Page/怡保市.md "wikilink")\[3\]。
  - 南亞：以印度[布拉馬普特拉河三角洲的](../Page/布拉馬普特拉河.md "wikilink")[西孟加拉邦](../Page/西孟加拉邦.md "wikilink")、[阿薩姆邦較多華裔客家人居住](../Page/阿薩姆邦.md "wikilink")，尤以加爾各答市為最。
  - 非洲:有大量華裔客家籍，其原鄉以[梅州市](../Page/梅州市.md "wikilink")[梅縣區或舊](../Page/梅縣區.md "wikilink")[嘉應州為多](../Page/嘉應州.md "wikilink")。主要分布在毛里求斯、留尼汪、塞舌爾群島、南非等國。
  - [歐洲](../Page/歐洲.md "wikilink")：歐洲華裔客家籍[華裔以](../Page/華裔.md "wikilink")[英國](../Page/英國.md "wikilink")、[法國為多](../Page/法國.md "wikilink")。
  - [美洲](../Page/美洲.md "wikilink")：以[牙買加](../Page/牙買加.md "wikilink")、[蘇里南](../Page/蘇里南.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[烏拉圭](../Page/烏拉圭.md "wikilink")、[秘魯](../Page/秘魯.md "wikilink")、[巴西等國為多](../Page/巴西.md "wikilink")，拉美客家華人原籍多在[寶安](../Page/寶安.md "wikilink")、[東莞](../Page/東莞.md "wikilink")、[增城](../Page/增城.md "wikilink")、[歸善](../Page/歸善.md "wikilink")，其多為舊[廣州府客家的后裔](../Page/廣州府.md "wikilink")，其語言與中國大陸的惠陽話（即属于客语粤台片新惠小片）最為相近。
  - 大洋洲：大洋洲客家籍華人主要分布在[澳大利亞](../Page/澳大利亞.md "wikilink")、[大溪地](../Page/大溪地.md "wikilink")（[法屬波利尼西亞](../Page/法屬波利尼西亞.md "wikilink")），其原鄉與拉美[客家華裔一樣](../Page/客家.md "wikilink")，多來自舊[廣東的](../Page/廣東.md "wikilink")[廣州府](../Page/廣州府.md "wikilink")。

## 參考資料

  - 張正田，〈從1926年臺灣漢人籍貫調查資料看「臺灣客家傳統地域」〉，《客家研究》(桃園中壢：國立中央大學客家學院)，3:
    2，2009.12，頁165-210

## 外部連結

  - [日治時期台灣閩、粵、原住民分佈概圖](http://img.freebase.com/api/trans/image_thumb/wikipedia/images/commons_id/1956555?maxheight=510&mode=fit&maxwidth=510)

[Category:客家地區](../Category/客家地區.md "wikilink")
[Category:广东聚居地](../Category/广东聚居地.md "wikilink")
[Category:福建聚居地](../Category/福建聚居地.md "wikilink")
[Category:江西聚居地](../Category/江西聚居地.md "wikilink")
[Category:台灣聚居地](../Category/台灣聚居地.md "wikilink")
[Category:马来西亚聚居地](../Category/马来西亚聚居地.md "wikilink")

1.
2.
3.