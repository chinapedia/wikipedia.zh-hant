**[九龍巴士N](../Page/九龍巴士.md "wikilink")43線**是[香港的一條巴士路線](../Page/香港.md "wikilink")，由[荃灣鐵路站往](../Page/荃灣站.md "wikilink")[長宏](../Page/長宏.md "wikilink")。\[1\]

## 歷史

  - 1994年2月：投入服務，原稱**43S**，由[荃灣地鐵站往來](../Page/荃灣站.md "wikilink")[青衣美景花園](../Page/青衣島.md "wikilink")，以循環線運作
  - 1995年1月31日：改為[荃灣單](../Page/荃灣.md "wikilink")-{向}-往青衣美景花園
  - 1997年2月6日：改稱**N43**
  - 2006年1月29日：路線延長至[長宏](../Page/長宏.md "wikilink")
  - 2008年2月6日：改為全空調服務

## 服務時間

  - 0030-0200：15分鐘一班
      - 只在[農曆新年期間行走](../Page/農曆新年.md "wikilink")

## 收費

  - 全程收費：$10.5

## 途經街道

  - [西樓角路](../Page/西樓角路.md "wikilink")，[大河道](../Page/大河道.md "wikilink")，[沙咀道](../Page/沙咀道.md "wikilink")，[德士古道](../Page/德士古道.md "wikilink")，[荃青交匯處](../Page/荃青交匯處.md "wikilink")，[青荃路](../Page/青荃路.md "wikilink")，[擔杆山交匯處](../Page/擔杆山交匯處.md "wikilink")，[青敬路](../Page/青敬路.md "wikilink")，[長安巴士總站](../Page/長安巴士總站.md "wikilink")，[擔杆山路](../Page/擔杆山路.md "wikilink")，擔杆山交匯處，[楓樹窩路](../Page/楓樹窩路.md "wikilink")，楓樹窩路迴旋處，[青衣鄉事會路](../Page/青衣鄉事會路.md "wikilink")，[青衣路](../Page/青衣路.md "wikilink")，[青康路](../Page/青康路.md "wikilink")，[青衣西路及](../Page/青衣西路.md "wikilink")[寮肚路](../Page/寮肚路.md "wikilink")。

### 車站一覽

| [長宏方向](../Page/長宏邨.md "wikilink") |
| --------------------------------- |
| **車站**                            |
| 1                                 |
| 2                                 |
| 3                                 |
| 4                                 |
| 5                                 |
| 6                                 |
| 7                                 |
| 8                                 |
| 9                                 |
| 10                                |
| 11                                |
| 12                                |
| 13                                |
| 14                                |
| 15                                |
| 16                                |
| 17                                |
| 18                                |
| 19                                |

## 參考資料

  - 《巴士路線發展綱要(2)──荃灣．屯門．元朗》，BSI(香港)，ISBN 9628414720003，110頁

<references />

[N043](../Category/九龍巴士路線.md "wikilink")
[N043](../Category/九龍巴士特別巴士路線.md "wikilink")
[N043](../Category/荃灣區巴士路線.md "wikilink")
[N043](../Category/葵青區巴士路線.md "wikilink")

1.  <http://www.681busterminal.com/n43.html>