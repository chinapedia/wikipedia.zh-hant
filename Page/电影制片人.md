**电影制作人**（通称：**電影監製**）是[管理整個](../Page/管理.md "wikilink")[電影](../Page/電影.md "wikilink")、[電視電影或](../Page/電視電影.md "wikilink")[電視連續劇拍片的](../Page/電視連續劇.md "wikilink")[專案管理工作](../Page/專案管理.md "wikilink")\[1\]。有认为\[2\]中文中使用的监-{}-制一词起于香港电影业。

工作性质好比[商人一樣](../Page/商人.md "wikilink")，包括如資金籌措，尋找或購買電影劇本，[演員](../Page/演員.md "wikilink")、合作廠商與工作人員的尋找、聘顧、任用與調度，[預算的控制](../Page/預算.md "wikilink")，拍攝時間進度控管等，讓電影能在合理的時間與預算下完成。在拍片過程中，-{zh-hk:監製;zh-tw:製作人;zh-cn:制片人}-的一舉一動都容易影響到整部[影片的品質](../Page/影片.md "wikilink")，是不可缺失的職位。

在商業電影發達的地區，許多經驗老到的成功監製可以要求拍攝何種走向及風格的電影，甚至乎凌駕[導演成為電影內容的主導者](../Page/電影導演.md "wikilink")，成為電影製作過程中權力最大的人，佼佼者包括[向華勝与](../Page/向華勝.md "wikilink")[向華強](../Page/向華強.md "wikilink")、[傑瑞·布洛克海默](../Page/傑瑞·布洛克海默.md "wikilink")、[喬·西佛和](../Page/喬·西佛.md "wikilink")[喬治·盧卡斯等](../Page/喬治·盧卡斯.md "wikilink")。

不過，現在華人[演員或](../Page/演員.md "wikilink")[導演有很多都身兼](../Page/導演.md "wikilink")[監製](../Page/監製.md "wikilink")，開始廣泛獨立起來，佼佼者導演包括[徐克与](../Page/徐克.md "wikilink")[陳可辛](../Page/陳可辛.md "wikilink")、[杜琪峰](../Page/杜琪峰.md "wikilink")、[王家衛和](../Page/王家衛.md "wikilink")[王晶等](../Page/王晶.md "wikilink")。演員包括[周星馳与](../Page/周星馳.md "wikilink")[劉德華](../Page/劉德華.md "wikilink")、[洪金寶](../Page/洪金寶.md "wikilink")、[李修賢等](../Page/李修賢.md "wikilink")。

## 注释

## 參見

  - [執行製作人](../Page/執行製作人.md "wikilink")

[eo:Produktoro](../Page/eo:Produktoro.md "wikilink")
[he:מפיק](../Page/he:מפיק.md "wikilink")

[电影监制](../Category/电影监制.md "wikilink")

1.
2.