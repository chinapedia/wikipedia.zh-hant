**林肯縣**（）是[美國](../Page/美國.md "wikilink")[新墨西哥州中南部的一個縣](../Page/新墨西哥州.md "wikilink")。面積12,513平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口20,497人。縣治[喀里索索](../Page/喀里索索.md "wikilink")。

## 歷史

[Billykid.jpg](https://zh.wikipedia.org/wiki/File:Billykid.jpg "fig:Billykid.jpg")\]\]

本縣成立於1869年，縣名紀念第十六任[總統](../Page/美國總統.md "wikilink")[亞伯拉罕·林肯](../Page/亞伯拉罕·林肯.md "wikilink")。於1870年代後期，在[牧場主與全縣最大綜合商店的擁有者之間](../Page/牧場.md "wikilink")，爆發了一場戰爭，史稱[林肯縣戰爭](../Page/林肯縣戰爭.md "wikilink")。\[1\]當時，[比利小子因其朋友及上司均被殺](../Page/比利小子.md "wikilink")，而加入了牧場主的陣營。最終，比利小子殺掉了包括殺害他友人的人與縣警長在內總共4至21人。\[2\]於1878年，新的領地總督赦免了多個參戰者，使林肯縣戰爭正式完結。\[3\]

## 地理

[Trigo-fire-in-canyon-USFS.jpg](https://zh.wikipedia.org/wiki/File:Trigo-fire-in-canyon-USFS.jpg "fig:Trigo-fire-in-canyon-USFS.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，林肯縣的總面積為，其中有，即99.99%為陸地；，即0.01%為水域。\[4\]本縣既是新墨西哥州面積第八大的縣份，亦是[美國面積第69大的縣份](../Page/美國面積最大縣份列表.md "wikilink")。

### 毗鄰縣

因為林肯縣位於新墨西哥州中部，因此所有毗鄰縣皆為新墨西哥州的縣份。

  - [托蘭斯縣](../Page/托蘭斯縣.md "wikilink")：北方
  - [瓜達盧佩縣](../Page/瓜達盧佩縣_\(新墨西哥州\).md "wikilink")：北方
  - [迪巴卡縣](../Page/迪巴卡縣.md "wikilink")：東北方
  - [沙維什縣](../Page/沙維什縣.md "wikilink")：東方
  - [奧特羅縣](../Page/奧特羅縣_\(新墨西哥州\).md "wikilink")：南方
  - [謝拉縣](../Page/謝拉縣_\(新墨西哥州\).md "wikilink")：西南方
  - [索科羅縣](../Page/索科羅縣.md "wikilink")：西方

### 國家保護區

  - [西勃拉國家森林公園](../Page/西勃拉國家森林公園.md "wikilink")（部分）
  - [斯坦頓堡 - 雪河洞國家保護區](../Page/斯坦頓堡_-_雪河洞國家保護區.md "wikilink")
  - [林肯國家森林公園](../Page/林肯國家森林公園.md "wikilink")（部分）

## 人口

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，林肯縣擁有20,497居民。而人口是由85.1%[白人](../Page/歐裔美國人.md "wikilink")、0.5%[黑人](../Page/非裔美國人.md "wikilink")、2.4%[土著](../Page/美國土著.md "wikilink")、0.4%[亞洲人](../Page/亞裔美國人.md "wikilink")、9.1%其他[種族和](../Page/種族.md "wikilink")2.5%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")29.8%。\[5\]

### 2000年

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，林肯縣擁有19,411居民、8,202住戶和5,634家庭。\[6\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")4居民（每平方公里2居民）。\[7\]本縣擁有15,298間房屋单位，其密度為每平方英里3間（每平方公里1間）。\[8\]而人口是由83.6%[白人](../Page/歐裔美國人.md "wikilink")、0.35%[黑人](../Page/非裔美國人.md "wikilink")、1.95%[土著](../Page/美國土著.md "wikilink")、0.27%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.06%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、11.28%其他[種族和](../Page/種族.md "wikilink")4.6%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")25.63%。\[9\]

在8,202
住户中，有26.2%擁有一個或以上的兒童（18歲以下）、55.6%為夫妻、9.3%為單親家庭、31.3%為非家庭、26.7%為獨居、10%住戶有同居長者。平均每戶有2.34人，而平均每個家庭則有2.80人。在19,411居民中，有22.7%為18歲以下、6%為18至24歲、23.2%為25至44歲、30.2%為45至64歲以及17.9%為65歲以上。人口的年齡中位數為44歲，女子對男子的性別比為100：95.9。成年人的性別比則為100：93.3。\[10\]

本县的住戶收入中位數為$33,886，而家庭收入中位數則為$40,035。男性的收入中位數為$27,323，而女性的收入中位數則為$19,923，[人均收入為](../Page/人均收入.md "wikilink")$19,338。約10.8%家庭和14.9%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括24.7%兒童（18歲以下）及8.7%長者（65歲以上）。\[11\]

## 參考文獻

[L](../Category/新墨西哥州行政区划.md "wikilink")

1.

2.  Wallis, Michael (2007). *Billy the Kid: The Endless Trail*. New
    York: W.W. Norton & Company. ISBN 978-0-393-06068-3, p. 244

3.

4.

5.  ["2010 Census P.L. 94-171 Summary File
    Data"](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/).
    United States Census Bureau.

6.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

7.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

8.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

9.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

10. [American FactFinder](http://factfinder.census.gov)

11.