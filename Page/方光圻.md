**方光圻**（），字「千里」，[江蘇](../Page/江蘇.md "wikilink")[江都](../Page/江都.md "wikilink")（今[江蘇省揚州市](../Page/江蘇省.md "wikilink")[江都区](../Page/江都区.md "wikilink")）人，清光緒二十四年（1898年）
生，[物理学家](../Page/物理学.md "wikilink")，[光學家](../Page/光學.md "wikilink")。

## 求學

  - 毕业于[南京高等师范学校](../Page/南京高等师范学校.md "wikilink")<small>（1921年改建[国立东南大学](../Page/国立东南大学.md "wikilink")，1928年更名[国立中央大学](../Page/国立中央大学_\(南京\).md "wikilink")）</small>理化科。
  - 留学美国[芝加哥大学物理研究所](../Page/芝加哥大学.md "wikilink")，爲[美國物理學會會員](../Page/美國物理學會.md "wikilink")。\[1\]

## 任職

  - 回国后,任國立清華大學物理學教授,任[中央大学](../Page/國立中央大學.md "wikilink")<small>（1949年在大陸改名[南京大學](../Page/南京大學.md "wikilink")，1962年在台復校）</small>物理系教授、系主任,國民政府軍政部兵工署理化研究所研究員,軍政部兵工署技術司技正。
  - 1939年時曾任[漢陽兵工專門學校校長](../Page/漢陽兵工專門學校.md "wikilink")，1949年后到[台湾](../Page/台湾.md "wikilink")，曾任改制為[陆军理工学院](../Page/陆军理工学院.md "wikilink")<small>（今[中正理工学院](../Page/中正理工学院.md "wikilink")）</small>院长等职。\[2\]\[3\]1965年兼任中國航空太空學會第7屆理事長。\[4\]\[5\]

## 翻譯及著作

  - ，《原子能之軍事用途：美國政府發展原子彈之官方報告》，方光圻（譯），重慶：軍政部兵工學校，1946年5月版

  - 柏爾（Bell,H.C.F.），《威爾遜傳》，方光圻（譯），中華文化出版事業委員會，民國49年<small>（1960年）</small>

:\*《Woodrow Wilson and the
People》([1945](http://www.unz.org/Pub/BellHCF-1945)) By H.C.F.；

  - 張儀尊.方光圻，《科學奧妙的運用》，教育部中等教育司，1961年版

## 知交

  - [李國鼎](../Page/李國鼎.md "wikilink")（方光圻夫人為李氏小學教師，李氏與方氏學生亦多所往來）

## 學生

  - [吴健雄](../Page/吴健雄.md "wikilink")<small>（女）</small>
  - [谢立惠](../Page/谢立惠.md "wikilink")

## 注釋

  - 腳注

<!-- end list -->

  - 引用

<!-- end list -->

  - 文獻

<!-- end list -->

  - 信札附封[](http://tanimg.kongfz.com.cn/20120604/1915/34247e4NfE0_n.jpg)、[](http://tanimg.kongfz.com.cn/20120604/1915/34247Gtmjm1_n.jpg)

[分類:國立中央大學教授](../Page/分類:國立中央大學教授.md "wikilink")
[分類:南京大學教授](../Page/分類:南京大學教授.md "wikilink")

[Category:江都人](../Category/江都人.md "wikilink")
[Category:中華民國物理學家](../Category/中華民國物理學家.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[理](../Category/中央大学校友.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:台灣外省人](../Category/台灣外省人.md "wikilink")
[Guang光](../Category/方姓.md "wikilink")
[Category:國防大學理工學院教師](../Category/國防大學理工學院教師.md "wikilink")

1.
2.  《方光圻與簡立--兩位對軍火工業教育貢獻最大任期最久的大學校長》，[01](http://readopac2.ncl.edu.tw/nclJournal/search/detail.jsp?sysId=0005413897&dtdId=000040&search_type=detail&la=ch)[02](http://catalog.digitalarchives.tw/item/00/4b/50/7e.html)，陳國怡，《傳記文學》，64:6=385，民83.06（1994-06），頁57-62，系統識別號：A94010849

3.
4.

5.