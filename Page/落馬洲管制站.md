[Lok_Ma_Chau_Control_Point.jpg](https://zh.wikipedia.org/wiki/File:Lok_Ma_Chau_Control_Point.jpg "fig:Lok_Ma_Chau_Control_Point.jpg")
[Luomazhouguanzhizhan.jpg](https://zh.wikipedia.org/wiki/File:Luomazhouguanzhizhan.jpg "fig:Luomazhouguanzhizhan.jpg")

**落馬洲管制站**位於[落馬洲](../Page/落馬洲.md "wikilink")[新深路](../Page/新深路.md "wikilink")，設有道路通往[深圳](../Page/深圳.md "wikilink")[皇崗口岸](../Page/皇崗口岸.md "wikilink")，是香港往[中國內地車輛交通的重要口岸](../Page/中國內地.md "wikilink")。2003年1月27日起，落馬洲管制站及皇崗口岸全日24小時通關。

## 歷史

香港與[中國內地的公路車輛](../Page/中國內地.md "wikilink")，原本只可經[文錦渡往返](../Page/文錦渡口岸.md "wikilink")。為舒緩中港道路交通需求，中港兩地政府在1980年代協議，由[香港政府興建落馬洲管制站](../Page/香港政府.md "wikilink")，連接至深圳[皇崗口岸](../Page/皇崗口岸.md "wikilink")，而該管制站亦能緩減九龍車站（今[紅磡站](../Page/紅磡站.md "wikilink")）和[羅湖站在](../Page/羅湖站_\(香港\).md "wikilink")1981到88年間的擠壓情況。1989年12月29日貨檢通道正式通車。1991年8月8日，客運部分正式開通。管制站開放初期通關時間由每日早上9時至下午5時。由於接近[新田公路和](../Page/新田公路.md "wikilink")[粉嶺公路](../Page/粉嶺公路.md "wikilink")，出入交通較方便，落馬洲管制站漸漸成為香港與中國內地最主要的陸路通道，通關時間亦多次延長，2001年12月1日延長至早上6時30分至凌晨零時。為應付凌晨時分市民往返中港的需求，2003年1月27日正式實施24小時通關。

## 貨運

來往中港兩地的[貨櫃車一般都經過落馬洲管制站往返](../Page/貨櫃車.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")，隨著中港貨運貿易發展，現時管制站每日皆大排長龍。遇有特別事故，例如邊防[電腦系統發生故障](../Page/電腦.md "wikilink")、風球除下及嚴重交通意外等，車龍更可能沿公路伸延至[上水和](../Page/上水.md "wikilink")[錦繡花園等地](../Page/錦繡花園.md "wikilink")。

## 客運

### 直通巴士

前往内地的旅客，可在香港各區乘搭[跨境巴士往返中國内地](../Page/跨境巴士.md "wikilink")。跨境巴士需在檢查大樓停站，旅客需落車接受邊防檢查。

### 穿梭巴士

旅客亦可乘搭香港各種交通工具至落馬洲，再轉乘[皇巴士](../Page/皇巴士.md "wikilink")（或稱「黃巴士」）進入中國內地。落馬洲至皇崗口岸的穿梭巴士服務於1997年3月20日開始。

## 來往落馬洲/皇崗跨境巴士服務

在運輸署的規範下，業界共開設六條來往皇崗口岸及香港市區的跨境巴士服務，包括：

  - [24小時跨境快線](../Page/24小時跨境快線.md "wikilink")
      - [觀塘24小時跨境快線](../Page/觀塘24小時跨境快線.md "wikilink")
          - [皇崗口岸](../Page/皇崗口岸.md "wikilink") ↔
            [藍田站](../Page/藍田站.md "wikilink")（經[創紀之城第](../Page/創紀之城.md "wikilink")5期[apm](../Page/apm.md "wikilink")、[九龍灣站](../Page/九龍灣站.md "wikilink")）（回程加停[鑽石山](../Page/鑽石山.md "wikilink")[荷里活廣場](../Page/荷里活廣場.md "wikilink")）
      - [油尖24小時跨境快線](../Page/油尖24小時跨境快線.md "wikilink")
          - [皇崗口岸](../Page/皇崗口岸.md "wikilink") ↔
            [尖沙咀](../Page/尖沙咀.md "wikilink")[柯士甸道](../Page/柯士甸道.md "wikilink")[香港童軍中心](../Page/香港童軍中心.md "wikilink")（經[佐敦站](../Page/佐敦站.md "wikilink")）
  - [跨境全日通](../Page/跨境全日通.md "wikilink")
      - [灣仔跨境全日通](../Page/皇崗過境巴士灣仔線.md "wikilink")
          - [灣仔](../Page/灣仔.md "wikilink")←→皇崗口岸
      - [旺角跨境全日通](../Page/皇崗過境巴士旺角線.md "wikilink")
          - [旺角](../Page/旺角.md "wikilink")←→皇崗口岸
      - [錦上路跨境全日通](../Page/皇崗過境巴士錦上路線.md "wikilink")
          - [錦上路站](../Page/錦上路站.md "wikilink")←→皇崗口岸
  - [中港直通快線](../Page/皇崗過境巴士荃灣線.md "wikilink")
      - [荃灣](../Page/荃灣.md "wikilink")[愉景新城](../Page/愉景新城.md "wikilink")(早上)/[荃灣站](../Page/荃灣站.md "wikilink")(深夜)←→皇崗口岸（經停[港鐵](../Page/港鐵.md "wikilink")[荃灣車廠停車場入口對出](../Page/荃灣車廠.md "wikilink")）

此外，配合香港迪士尼樂園啟用，運輸署特別准許[樂園快線投入服務](../Page/樂園快線.md "wikilink")。

## 來往落馬洲管制站之公共交通工具接駁

(部份路線經停**落馬洲管制站**，另外路線為[落馬洲轉車站或](../Page/落馬洲轉車站.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")[落馬洲站](../Page/落馬洲站.md "wikilink"))


## 皇崗拆樓封關事件

2005年5月22日下午1時，深圳市政府以[爆破形式拆卸皇崗口岸附近的漁農村](../Page/爆破拆卸.md "wikilink")18幢樓宇，落馬洲至皇崗的過境交通暫停約半小時，是落馬洲口岸在正常情況下首次暫時封關，拆卸工作順利完成。

## 參看

  - [港鐵](../Page/港鐵.md "wikilink")：[東鐵綫](../Page/東鐵綫.md "wikilink")[落馬洲支線](../Page/落馬洲支線.md "wikilink")
    - [落馬洲站](../Page/落馬洲站.md "wikilink")

## 參見

  - [新田公路](../Page/新田公路.md "wikilink")
  - [粉嶺公路](../Page/粉嶺公路.md "wikilink")
  - [新深路](../Page/新深路.md "wikilink")
  - [落馬洲大橋](../Page/落馬洲大橋.md "wikilink")
  - [皇崗口岸](../Page/皇崗口岸.md "wikilink")

## 外部連結

  - [元朗區議會](https://web.archive.org/web/20060524091925/http://www.districtcouncils.gov.hk/yl/chinese/welcome.htm)
  - [元朗區分界圖](http://www.eac.gov.hk/pdf/distco/maps/dc2003m.pdf)
  - [元朗網站 Yuen Long Special](http://www.yl.com.hk)
  - [元朗遊記 The Incredible Journey of Yuen
    Long](https://web.archive.org/web/20170630235514/http://www.go2yl.com/)

[落馬洲](../Category/落馬洲.md "wikilink")
[Category:香港邊境禁區](../Category/香港邊境禁區.md "wikilink")
[Category:米埔](../Category/米埔.md "wikilink")
[Category:香港出入境管制站](../Category/香港出入境管制站.md "wikilink")
[Category:港深口岸](../Category/港深口岸.md "wikilink")