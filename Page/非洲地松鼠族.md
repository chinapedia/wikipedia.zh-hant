**非洲地松鼠族**（**Xerini**）是一類生活於非洲的[松鼠科動物](../Page/松鼠科.md "wikilink")，其下含有[非洲地松鼠屬](../Page/非洲地松鼠屬.md "wikilink")（*Xerus*），這類動物完全棲息於地面或地下。

## 參考文獻

  -
[Category:非洲地松鼠族](../Category/非洲地松鼠族.md "wikilink")