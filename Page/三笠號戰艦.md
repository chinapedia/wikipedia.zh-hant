[JBMikasa.jpg](https://zh.wikipedia.org/wiki/File:JBMikasa.jpg "fig:JBMikasa.jpg")
[MIKASAPAINTING.jpg](https://zh.wikipedia.org/wiki/File:MIKASAPAINTING.jpg "fig:MIKASAPAINTING.jpg")
[Battleship_Mikasa_from_JFS1906_Cropped.png](https://zh.wikipedia.org/wiki/File:Battleship_Mikasa_from_JFS1906_Cropped.png "fig:Battleship_Mikasa_from_JFS1906_Cropped.png")要目\]\]
[Mikasa04.jpg](https://zh.wikipedia.org/wiki/File:Mikasa04.jpg "fig:Mikasa04.jpg")保存的戰艦**三笠**\]\]
[Imperial_Seal_of_Japan.jpg](https://zh.wikipedia.org/wiki/File:Imperial_Seal_of_Japan.jpg "fig:Imperial_Seal_of_Japan.jpg")
[MIKASA02.jpg](https://zh.wikipedia.org/wiki/File:MIKASA02.jpg "fig:MIKASA02.jpg")
[MIKASA03.jpg](https://zh.wikipedia.org/wiki/File:MIKASA03.jpg "fig:MIKASA03.jpg")
**三笠**（）是[大日本帝國海軍的](../Page/大日本帝國海軍.md "wikilink")[戰艦](../Page/戰艦.md "wikilink")，為[敷島級戰艦四號艦](../Page/敷島級戰艦.md "wikilink")，以[奈良縣的](../Page/奈良縣.md "wikilink")[三笠山命名](../Page/三笠山.md "wikilink")。同級艦有[敷島](../Page/敷島號戰艦.md "wikilink")、[初瀬](../Page/初瀬號戰艦.md "wikilink")、[朝日](../Page/朝日號戰艦.md "wikilink")。

在1904年（[明治](../Page/明治.md "wikilink")37年）[日俄戰争中](../Page/日俄戰争.md "wikilink")，擔任[聯合艦隊](../Page/聯合艦隊.md "wikilink")[旗艦](../Page/旗艦.md "wikilink")、[聯合艦隊司令長官](../Page/聯合艦隊司令長官.md "wikilink")[東鄉平八郎](../Page/東鄉平八郎.md "wikilink")[大將座艦](../Page/大將.md "wikilink")。現在於[横須賀市的](../Page/横須賀市.md "wikilink")[三笠公園以紀念艦形式保存](../Page/三笠公園.md "wikilink")。

## 背景

1894年至1895年期間的[中日甲午戰爭](../Page/甲午戰爭.md "wikilink")，日本在[俄羅斯帝國的壓力下被迫歸還](../Page/俄羅斯帝國.md "wikilink")[遼東半島給中國](../Page/遼東半島.md "wikilink")。

為對抗俄羅斯帝國，日本開始進行一系列加強[軍事力量的計畫](../Page/軍事力量.md "wikilink")。敷島級戰艦在其中的『六六艦隊計劃』（配備戰艦6艘、裝甲巡洋艦6艘的計劃）中開始興建。

1899年1月24日，三笠號在英國動工，1900年11月8日完工下水，耗時三年、耗資八百八十[萬](../Page/萬.md "wikilink")[日元](../Page/日元.md "wikilink")。

「[六・六艦隊](../Page/六・六艦隊.md "wikilink")」計劃因為需要龐大金額，日本海軍的債務雖然在[甲午戰爭以前就已著手重組](../Page/甲午戰爭.md "wikilink")、改善，但該計劃所需的金額仍就超出海軍預算，要完成計劃就只有非法挪用預算這個違反[憲法的方法](../Page/憲法.md "wikilink")。

在「六・六艦隊」計畫的提議人[山本權兵衛跟協助者](../Page/山本權兵衛.md "wikilink")[西鄉從道的商量时](../Page/西鄉從道.md "wikilink")，西郷說：「山本先生，無論如何一定要買（軍艦）。要挪用預算，那當然是違反憲法的。如果議會追究違憲責任，就在二重橋切腹吧。如果死兩個人就能買到軍艦，也算死得其所了。」于是三笠号在非法挪用預算下完成。

## 影響

日本艦隊的表現被西方大國觀察和分析，成為對下一代戰艦（[無畏號戰列艦等](../Page/無畏號戰列艦.md "wikilink")）的定義的重要角色，從衝突中證實了-{重型}-火炮更高的效率和遠程火力的重要性。

而且俄國艦隊的失敗和[樸次茅斯和約的簽處](../Page/樸次茅斯和約.md "wikilink")（9月5日），俄國的不安增強。1905年有海軍在[塞凡堡](../Page/塞凡堡.md "wikilink")（Sevastopol）、[海參崴](../Page/海參崴.md "wikilink")（Vladivostok）和[喀琅施塔得](../Page/喀琅施塔得.md "wikilink")（Kronstadt）反叛，叛亂在6月銳化，叛軍登上[波坦金號戰艦](../Page/波坦金號戰艦.md "wikilink")（Potemkin）。1917年俄國革命在10月達到了高潮，當沙皇[尼古拉二世被迫放棄他專治主義的獨裁力量](../Page/尼古拉二世.md "wikilink")，並且簽署[十月宣言](../Page/十月宣言.md "wikilink")。

終於，日本的勝利證實了日本正式成為在亞洲的超群力量和可以立足國際場面的力量。保存至今的三笠是前無畏式戰艦時代最後的戰艦。

## 特點

在三笠交付之時，是當時科技先進的戰艦，[火力和](../Page/火力.md "wikilink")[防護力量史無前例地組合](../Page/防護力.md "wikilink")，改良了[英國皇家海軍最新的](../Page/英國皇家海軍.md "wikilink")-{[莊嚴級戰艦](../Page/莊嚴級戰艦.md "wikilink")}-設計，增加了[噸位](../Page/噸位.md "wikilink")（由14,900噸增至15,140噸），改善[速度](../Page/速度.md "wikilink")（由17[節改善至](../Page/節.md "wikilink")18節），輕微地加強[軍備](../Page/軍備.md "wikilink")（增加兩門十五厘米[炮](../Page/炮.md "wikilink")），和更強的[裝甲](../Page/裝甲.md "wikilink")：保留了同樣的裝甲厚度但使用了高性能[克虜伯鋼](../Page/克虜伯鋼.md "wikilink")（Krupp
armour）裝甲，防禦力大約較-{莊嚴級}-所採用的[哈威鋼](../Page/哈威鋼.md "wikilink")（Harvey
armour）加強五成。

三笠的[主炮被編組在中央的](../Page/主炮.md "wikilink")[艦橋裝甲](../Page/艦橋.md "wikilink")，考慮到船能均勻地受重克虜伯防護鋼板保護。由於這設計，三笠能承受很大數量的炮彈直接命中：於1904年8月10日的[黄海海戰被擊中了](../Page/黃海海戰_\(1904年\).md "wikilink")20發炮彈和在[對馬海峽海戰被擊中了](../Page/對馬海峽海戰.md "wikilink")30發炮彈，但只受到有限的損傷。高火力和高射程的三笠大炮被高度訓練的日本[炮兵充分地利用了](../Page/炮兵.md "wikilink")。

## 歷史

### 交接

三笠是六・六艦隊的最後一隻艦，1899年1月24日在[威格士造船廠動工](../Page/威格士造船廠.md "wikilink")。1900年11月8日進水。1902年1月15日至20日公開測試，3月1日在[修咸頓舉行交接儀式交予日本海軍](../Page/南安普敦.md "wikilink")。船身建造費為88萬[英磅](../Page/英磅.md "wikilink")，兵器費32[萬英磅](../Page/萬.md "wikilink")。3月13日在英國[普利茅斯啟航](../Page/普利茅斯.md "wikilink")，經[蘇伊士運河在](../Page/蘇伊士運河.md "wikilink")5月18日到達[横須賀](../Page/横須賀.md "wikilink")。初代艦長是[早崎源吾](../Page/早崎源吾.md "wikilink")[大佐](../Page/大佐.md "wikilink")。在[横須賀整備後](../Page/横須賀.md "wikilink")6月23日出航，7月17日到達本籍港[舞鶴](../Page/舞鶴.md "wikilink")。

### 聯合艦隊旗艦

1903年12月28日，**三笠**成為[聯合艦隊](../Page/聯合艦隊.md "wikilink")[旗艦](../Page/旗艦.md "wikilink")。1904年2月6日開始參加[日俄戰爭](../Page/日俄戰爭.md "wikilink")。8月10日參加[黄海海戰](../Page/黃海海戰_\(1904年\).md "wikilink")。12月28日駛入[吳港修理](../Page/吳市.md "wikilink")。1905年2月14日、駛出吳港，經[江田島](../Page/江田島.md "wikilink")、[佐世保](../Page/佐世保.md "wikilink")，21日進駐[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[鎮海灣](../Page/鎮海灣.md "wikilink")。之後以該地為據點，在[對馬海峡進行訓練](../Page/對馬海峡.md "wikilink")。5月27日、28日在[對馬海峡海戰與](../Page/對馬海峡海戰.md "wikilink")[俄羅斯的](../Page/俄羅斯.md "wikilink")[波羅的海艦隊交戰](../Page/波羅的海艦隊.md "wikilink")。

三笠在[對馬海峡海戰帶領日本聯合艦隊在歷史上進入一場最果斷的](../Page/對馬海峡海戰.md "wikilink")[海戰](../Page/海戰.md "wikilink")，俄國艦隊幾乎完全地被殲滅了：俄國喪失38艘船，21艘沉沒，7艘遭日本海軍擄獲，6艘解除武裝，對馬海峽海戰共有4,545名俄國軍人死亡，6,106人遭俘。

另一方面，日本海軍只喪失了116名士兵與3艘[魚雷艇](../Page/魚雷艇.md "wikilink")。日本海軍組建時是根據英國皇家海軍的高度專業標準所組織訓練；反觀俄國波罗的海舰队不僅組織力較低且經過万里跋涉，到達東北亞時已师老兵疲。

### 事故沉沒

日俄戰爭終結之後緊接的1905年9月11日，三笠在[佐世保港內因為後部彈藥庫的爆炸事故而沉沒了](../Page/佐世保港.md "wikilink")，這個事故中死者共339名。

當時在[水兵間流行在彈藥庫面前玩](../Page/水兵.md "wikilink")「點燃信號用酒精并吹熄後，就着臭味飲用」\[1\]的[惡作劇](../Page/惡作劇.md "wikilink")，在惡作劇的進行中，有一說是有水兵不留神翻倒了有火的[洗臉盆](../Page/洗臉盆.md "wikilink")，另一說是[下瀬火藥的變質](../Page/下瀬火藥.md "wikilink")。這個爆炸沉沒事故被認為是[秋山真之](../Page/秋山真之.md "wikilink")[少佐埋頭於宗教研究的原因之一](../Page/少佐.md "wikilink")。10月23日的海軍凱旋式[敷島號戰艦替換三笠成為了旗艦](../Page/敷島號戰艦.md "wikilink")。

事故當時，[聯合艦隊司令長官](../Page/聯合艦隊司令長官.md "wikilink")[東鄉平八郎](../Page/東鄉平八郎.md "wikilink")[大將上陸平安](../Page/大將.md "wikilink")。同時，艦隊附屬[軍樂隊到任的](../Page/軍樂隊.md "wikilink")[瀬戶口藤吉](../Page/瀬戶口藤吉.md "wikilink")[海軍軍樂長又在事故當時是上陸中逃過了大難](../Page/海軍軍樂長.md "wikilink")。不過，很多[軍樂兵由於該事故而殉職了](../Page/軍樂兵.md "wikilink")。

三笠成為預備艦，1906年8月8日打捞起到佐世保工廠修理，1908年4月24日第1艦隊旗艦再次回復現役。

### 第一次世界大戰西伯利亞出兵

1914年8月23日，[日本參加](../Page/日本.md "wikilink")[第一次世界大戰初期三笠在日本海等從事警備活動](../Page/第一次世界大戰.md "wikilink")。

此後從1918年開始至1921年之間，為從[東面牽制大戰中誕生的](../Page/東.md "wikilink")[社會主義國](../Page/社會主義.md "wikilink")[蘇聯](../Page/蘇聯.md "wikilink")，日本出兵支援[西伯利亞](../Page/西伯利亞.md "wikilink")（參加前實施防寒工事，飛機實行臨時搭載）。

1921年9月1日三笠成為一等[海防艦](../Page/海防艦.md "wikilink")，不過，9月16日在[海參崴港](../Page/海參崴.md "wikilink")（Vladivostok）外[海峽中航行時在濃霧中觸礁](../Page/海峽.md "wikilink")。離礁後在海參崴港（Владивосток）入[船塢修理](../Page/船塢.md "wikilink")，11月3日歸投[舞鶴](../Page/舞鶴.md "wikilink")。

### 紀念艦

根據[华盛顿海军条约使](../Page/华盛顿海军条约.md "wikilink")**三笠**決定廢艦。1923年9月1日因為[關東大地震使三笠撞到](../Page/關東大地震.md "wikilink")[碼頭](../Page/碼頭.md "wikilink")、浸水，9月20日於帝國海軍[除籍](../Page/除籍.md "wikilink")。

根據华盛顿海军条约廢艦後，由於日本國民對三笠的保存運動非常勃興，日本將解体的地方作為三笠公園。1925年1月[內閣](../Page/內閣.md "wikilink")[會議決定把不能復歸現役狀態做為](../Page/會議.md "wikilink")[條件的特別認可去保存三笠](../Page/條件.md "wikilink")，作為[紀念艦在](../Page/紀念艦.md "wikilink")[橫須賀保存](../Page/橫須賀市.md "wikilink")，同年6月18日開始保存的工程，把船頭固定轉向[皇宮](../Page/皇宮.md "wikilink")。11月10日工程完成，11月12日進行保存式。

### 复原

三笠在[第二次世界大戰當中被](../Page/第二次世界大戰.md "wikilink")[美國陸軍航空軍](../Page/美國陸軍航空軍.md "wikilink")（USAAF）轟炸，[太平洋戰爭戰敗後](../Page/太平洋戰爭.md "wikilink")，[同盟國軍佔領日本期間](../Page/同盟國.md "wikilink")，[蘇聯要求将三笠号进行拆毁](../Page/蘇聯.md "wikilink")，但是由于思想比较亲日的尼米兹上将和陆军威洛比少将的运动而得幸免。舰上為了[美軍而在军官室設置了](../Page/美軍.md "wikilink")“东条夜总会”等[娛樂設施](../Page/娛樂設施.md "wikilink")，上部[兵裝和上層建築物全被撤去](../Page/兵裝.md "wikilink")，能取下了的[金屬類](../Page/金屬.md "wikilink")（[銅和](../Page/銅.md "wikilink")[黃銅等](../Page/黃銅.md "wikilink")）被拿走，大部分[煤氣切斷](../Page/煤氣.md "wikilink")，[柚木製](../Page/柚木.md "wikilink")[甲板也因需要](../Page/甲板.md "wikilink")[柴火和](../Page/柴火.md "wikilink")[建材而被剝下](../Page/建材.md "wikilink")，三笠就此荒廢。

[英國人約翰](../Page/英國.md "wikilink")·S·鲁賓（John S.
Rubin）觀察了這個慘狀氣憤地向《[日本時報](../Page/日本時報.md "wikilink")》（*Japan
Times*）投稿，引起極大迴響。而且[美國海軍](../Page/美國海軍.md "wikilink")[五星上將](../Page/五星上將.md "wikilink")[尼米兹憂慮三笠的狀況而著書](../Page/切斯特·威廉·尼米兹.md "wikilink")，三笠保存捐獻等事慢慢顯示了復元運動的熱烈。
復元運動在1958年恢復進行，因[美國通過財政的參與支持和尼米兹的直接介入](../Page/美國.md "wikilink")。復元在1961年5月27日完成，共花10億8000萬日元。大量的失蹤零件和配件由當時在日本解體的[智利](../Page/智利.md "wikilink")[戰艦](../Page/戰艦.md "wikilink")[拉托雷海军上將號](../Page/拉托雷海军上將號戰艦.md "wikilink")（Almirante
Latorre）作为智利政府的礼物提供\[2\]。。它现在是防卫省名下的国有财产。

### 略年表

  - 1898年（[明治](../Page/明治.md "wikilink")31年）- 日本政府向英國巴洛造船廠訂貨。
  - 1899年（明治32年）1月24日－動工。
  - 1902年（明治35年）3月1日－竣工。
  - 1905年（明治38年）5月27日－5月28日－在[對馬海峡海戰成為](../Page/對馬海峡海戰.md "wikilink")[聯合艦隊](../Page/聯合艦隊.md "wikilink")[旗艦](../Page/旗艦.md "wikilink")、[聯合艦隊司令長官](../Page/聯合艦隊司令長官.md "wikilink")[東鄉平八郎座艦](../Page/東鄉平八郎.md "wikilink")。
  - 1905年（明治38年）9月11日－佐世保港内爆發事故沈没。
  - 1906年（明治39年）- [打撈後開始重新修理](../Page/打撈.md "wikilink")。
  - 1923年（[大正](../Page/大正.md "wikilink")12年）-
    因[华盛顿海军条约成為廢艦](../Page/华盛顿海军条约.md "wikilink")。
  - 1923年（大正12年）9月20日－除籍。

<!-- end list -->

  - 1925年（大正14年）-
    通過會議決定以紀念艦形式由横須賀保存。財團法人[三笠保存會成立](../Page/三笠保存會.md "wikilink")。
  - 1926年（大正15年）11月12日－**三笠**保存紀念式舉行。其後改稱「紀念艦**三笠**」。

<!-- end list -->

  - 1945年（[昭和](../Page/昭和.md "wikilink")20年）- 被聯合國軍接收。**三笠**保存會解散。其荒廢。
  - 1958年（昭和33年）- **三笠**保存會再次成立，開始籌集復原資金。
  - 1959年（昭和34年）－1961年（昭和36年）- 進行復原整備工程。
  - 1961年（昭和36年）5月27日－「紀念艦**三笠**」復原紀念式舉行。

## 裝備資料

[Mikasa_zenbu_shuho2.jpg](https://zh.wikipedia.org/wiki/File:Mikasa_zenbu_shuho2.jpg "fig:Mikasa_zenbu_shuho2.jpg")

  - [速力](../Page/速度.md "wikilink")：18節（時速33公里）
  - 動力

:\*
2台往復式[蒸汽機](../Page/蒸汽機.md "wikilink")，2軸推動，15,000[馬力](../Page/馬力.md "wikilink")

  - 裝備

:\*
[主砲](../Page/主砲.md "wikilink")：305mmxL40[倍徑砲](../Page/倍徑.md "wikilink")4門，雙聯裝兩座。

:\* 副砲：152mmxL40倍徑砲14門，單裝14座。

:\* 補助砲：76.2mmxL40倍徑[速射砲](../Page/速射砲.md "wikilink")20門

:\* [魚雷發射管](../Page/魚雷發射管.md "wikilink")：單裝450mm魚雷發射管4門（裝備於水線下）

:\* [衝角](../Page/衝角.md "wikilink")

  - 防御力

<!-- end list -->

  -
    擁有壓倒性的火力同時，使用了當時世界防御力水準最高的[克虜伯鋼裝甲板](../Page/克虜伯鋼.md "wikilink")。日本的軍艦在三笠以後，因船幅過大而不能通過[蘇伊士運河](../Page/蘇伊士運河.md "wikilink")，而敵對的[俄羅斯](../Page/俄羅斯.md "wikilink")，其戰艦亦是不能通過蘇伊士。因此[波羅的海艦隊要行經](../Page/波羅的海艦隊.md "wikilink")[好望角周圍經過困難的大航海而來](../Page/好望角.md "wikilink")。

<!-- end list -->

  - 通信能力

<!-- end list -->

  -
    當時裝備有最新鋭的[無線電機](../Page/無線電機.md "wikilink")（三六式無線機），其通信能力在[對馬海峡海戰之中非常有效](../Page/對馬海峡海戰.md "wikilink")。

## 同級艦

| 船名                                | 圖片                                                                |
| --------------------------------- | ----------------------------------------------------------------- |
| [敷島](../Page/敷島號戰艦.md "wikilink") | [120px](../Page/檔案:Battleship_Shikishima.jpg.md "wikilink")       |
| [朝日](../Page/朝日號戰艦.md "wikilink") | [120px](../Page/檔案:Japanese_battleship_Asahi_2.jpg.md "wikilink") |
| [初瀬](../Page/初瀬號戰艦.md "wikilink") | [120px](../Page/檔案:Japanese_battleship_Hatsuse.jpg.md "wikilink") |
|                                   |                                                                   |

## 參考資料

<references/>

## 關連項目

  - [大日本帝國海軍艦艇列表](../Page/大日本帝國海軍艦艇列表.md "wikilink")

<!-- end list -->

  - [戰艦列表](../Page/戰艦列表.md "wikilink")

## 外部連結

  - [記念艦三笠公式網頁（財團法人三笠保存會）](http://www.kinenkan-mikasa.or.jp/)（世界三大紀念艦於横須賀市的三笠公園保存）

[Category:日本海军战列舰](../Category/日本海军战列舰.md "wikilink")
[Category:日俄戰爭](../Category/日俄戰爭.md "wikilink")
[Category:1902年竣工的船只](../Category/1902年竣工的船只.md "wikilink")
[Category:1900年下水](../Category/1900年下水.md "wikilink")
[Category:日俄战争舰艇](../Category/日俄战争舰艇.md "wikilink")
[Category:敷島級戰艦](../Category/敷島級戰艦.md "wikilink")

1.  日語中的惡作劇：
2.  中山定義『一海軍士官の回想』(毎日新聞社、1981)p.98
    该舰现在的桅杆，烟囱炮塔是复制品，而舰体内受华盛顿条约限制将房间用水泥填埋。因此只开放了甲板以上的舱室供参观。