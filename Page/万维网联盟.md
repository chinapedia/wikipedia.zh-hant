**万维网联盟**（），又称**W3C理事会**，是[全球資訊網的主要國際](../Page/全球資訊網.md "wikilink")[標準組織](../Page/標準組織.md "wikilink")\[1\]。為半自治非政府組織（quasi-autonomous
non-governmental organisation）。

## 歷史

萬維網聯盟（W3C）由[提姆·柏內茲-李於](../Page/提姆·柏內茲-李.md "wikilink")1994年10月離開[歐洲核子研究組織](../Page/歐洲核子研究組織.md "wikilink")（CERN）後成立，在[歐盟執委會和](../Page/歐盟執委會.md "wikilink")[國防高等研究計劃署](../Page/國防高等研究計劃署.md "wikilink")（DARPA）的支持下成立於[麻省理工學院](../Page/麻省理工學院.md "wikilink")[MIT計算機科學與人工智慧實驗室](../Page/MIT計算機科學與人工智慧實驗室.md "wikilink")（MIT／LCS）\[2\]，DARPA推出了[ARPANET](../Page/ARPANET.md "wikilink")，是[互聯網前身之一](../Page/互聯網.md "wikilink")。

該組織試圖透過W3C制定的新標準來促進業界成員間的兼容性和協議。不相容的HTML版本由不同的供應商提供，導致網頁顯示方式不一致。聯盟試圖讓所有的供應商實施一套由聯盟選擇的核心原則和元件。

CERN最初打算做為W3C的歐洲分支機構，然而CERN希望把重點放在[粒子物理而不是資訊技術上](../Page/粒子物理.md "wikilink")。1995年4月，[法國國家信息與自動化研究所](../Page/法國國家信息與自動化研究所.md "wikilink")（INRIA）成為W3C的歐洲機構。1996年9月，[慶應義塾大學SFC研究所成為W](../Page/慶應義塾大學.md "wikilink")3C亞洲機構\[3\]。從1997年開始，W3C在世界各地建立了區域辦事處。截至2009年9月，已有十八個區域辦事處，涵蓋澳大利亞、[比荷盧聯盟](../Page/比荷盧聯盟.md "wikilink")、巴西、中國、芬蘭、德國、奧地利、希臘、香港、匈牙利、印度、以色列、意大利、南韓、摩洛哥、南非、西班牙、瑞典\[4\]。2013年1月，[北京航空航天大學成為W](../Page/北京航空航天大學.md "wikilink")3C中國機構。2016年，W3C在英國和愛爾蘭建立了區域辦事處。

## 標準

为解决網路应用中不同平台、技术和开发者带来的不兼容问题，保障網路信息的顺利和完整流通，万维网联盟制定了一系列标准并督促網路应用开发者和内容提供者遵循这些标准。标准的内容包括使用语言的规范，开发中使用的导则和解释引擎的行为等等。W3C也制定了包括[XML和](../Page/XML.md "wikilink")[CSS等的众多影响深远的标准规范](../Page/CSS.md "wikilink")。

但是，W3C制定的網路标准似乎并非强制，而只是推荐标准。因此部分网站仍然不能完全实现这些标准，特别是使用早期[所见即所得网页编辑软件设计的网页往往会包含大量非标准代码](../Page/所见即所得.md "wikilink")。

  - [W3C推荐标准](../Page/W3C推荐标准.md "wikilink")

  - [CSS](../Page/CSS.md "wikilink")：階層式樣式表

  - [DOM](../Page/DOM.md "wikilink")：文件物件模型

  - [HTML](../Page/HTML.md "wikilink")：超文件標示語言

  - [RDF](../Page/资源描述框架.md "wikilink")：資源描述框架

  - [SMIL](../Page/SMIL.md "wikilink")：同步多媒體集成語言

  - [SVG](../Page/SVG.md "wikilink")：可縮放向量圖形

  -
  -
  - [XHTML](../Page/XHTML.md "wikilink")：可延伸超文件標示語言

  - [XML](../Page/XML.md "wikilink")：可延伸標記式語言

  - [PICS](../Page/PICS.md "wikilink")：網路內容篩選平台

## 参考文献

## 外部連結

  -
  - [關於全球資訊網協會](https://www.w3.org/Consortium/)

  - [W3C技術報告和出版物](https://www.w3.org/TR/)

  - [W3C流程文件](http://www.w3.org/Consortium/Process/)

  - [W3C歷史](http://www.w3.org/History/)

  - [如何閱讀W3C規範](http://www.alistapart.com/articles/readspec)

{{-}}

[W](../Category/非政府組織.md "wikilink")
[W](../Category/標準制訂機構.md "wikilink")
[万维网联盟](../Category/万维网联盟.md "wikilink")
[W](../Category/1994年建立.md "wikilink")
[W](../Category/網站開發.md "wikilink")
[W](../Category/全球資訊網.md "wikilink")

1.

2.
3.

4.