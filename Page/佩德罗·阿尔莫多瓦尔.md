**佩德羅·阿莫多瓦·卡瓦耶羅**（，；\[1\]\[2\]），[西班牙](../Page/西班牙.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")、[編劇和](../Page/編劇.md "wikilink")[製作人](../Page/電影監製.md "wikilink")。於2001年成為[美國文理科學院榮譽成員](../Page/美國文理科學院.md "wikilink")\[3\]，因為藝術成就於2009年獲得[哈佛大學授于榮譽](../Page/哈佛大學.md "wikilink")[博士學位](../Page/博士.md "wikilink")\[4\]。佩德羅·阿莫多瓦·卡瓦耶羅被称为[路易斯·布紐爾之后最有影响的西班牙导演](../Page/路易斯·布努埃爾.md "wikilink")，其作品具有争议性，着重表现欲望、暴力、[宗教等议题](../Page/宗教.md "wikilink")，并通过鲜艳的色彩，展示出一种后现代的审美眼光以及对权威的极大藐视。

## 生涯

### 早年

佩德罗·阿莫多瓦在1949年9月25日出生于西班牙[卡尔萨达德卡拉特拉瓦](../Page/卡尔萨达德卡拉特拉瓦.md "wikilink")[雷阿爾城](../Page/雷阿爾城.md "wikilink")，他出生在一個龐大而貧困農民家庭，並有三個手足。他的父親安東尼奧·阿莫多瓦幾乎無法讀書或寫字，使用[騾子來運送酒桶](../Page/騾子.md "wikilink")。阿爾莫多瓦的母親，弗朗西斯卡·瓦列羅讓佩德罗·阿莫多瓦學習識字。由于父亲的贩酒事业经营不善，阿莫多瓦的孩童時代过得十分贫苦。佩德罗·阿莫多瓦八岁时，他前往艾克斯杜雷马杜拉省[卡塞雷斯](../Page/卡塞雷斯_\(西班牙\).md "wikilink")，在那里进入[教会学校学习](../Page/教会学校.md "wikilink")，當時父母希望有一天他會成為一名[牧師](../Page/牧師.md "wikilink")。他的家人後來也來到卡塞雷斯，他的父親開了一家[加油站](../Page/加油站.md "wikilink")，母親則經營[葡萄酒店](../Page/葡萄酒.md "wikilink")\[5\]。糟糕且专制的教育制度，使得少年时代的阿莫多瓦就对[宗教产生疑惑](../Page/宗教.md "wikilink")。他正是在这个时候开始迷恋电影，同时电影也给了他最大的安慰。他後來在接受記者採訪時說：「我從電影中學到真正的教育，遠遠超過任何一位牧師」。他有時住在學校，有時則睡在[電影院裡](../Page/電影院.md "wikilink")\[6\]。

雖然受到父母反對，佩德罗·阿莫多瓦於1967年搬到[馬德里](../Page/馬德里.md "wikilink")。他的目標是成為一名電影導演，但是缺乏經濟支持，當時[弗朗西斯科·佛朗哥關閉所有的電影學校](../Page/弗朗西斯科·佛朗哥.md "wikilink")，所以他只好自學電影技術。為了賺錢，阿莫多瓦打了一些零工，包括在馬德里著名的[跳蚤市場](../Page/跳蚤市場.md "wikilink")[埃爾拉斯特洛賣東西](../Page/埃爾拉斯特洛.md "wikilink")。他最終成為[西班牙電信員工](../Page/西班牙電信.md "wikilink")，他在那裡擔任12年的行政助理。他的工作到下午三點，剩下的時間則自學電影技術。

### 文化運動

在1970年代初，阿莫多瓦對於[實驗電影和](../Page/實驗電影.md "wikilink")[戲劇持續感到興趣](../Page/戲劇.md "wikilink")。他與洛杉磯Goliardos合作，這是佩德罗·阿莫多瓦首次職業演出，並認識了[卡門·莫拉](../Page/卡門·莫拉.md "wikilink")。他還創作[漫畫和](../Page/漫畫.md "wikilink")[反主流文化](../Page/反主流文化.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，例如《Star》、《Víbora》與《Vibraciones》。

佛朗哥去世後，阿莫多瓦的參加馬德里文化復興運動，成為[運動的重要人物](../Page/運動.md "wikilink")。他使用化名在各大報刊雜誌刊登[文章](../Page/文章.md "wikilink")，如《[國家報](../Page/國家報.md "wikilink")》、《[16日報](../Page/16日報.md "wikilink")》、《月光報》。他不停地創作故事，最終發表在《El
sueño de la razón》。

### 開始導演影片

阿莫多瓦從西班牙電信獲得薪水後，買了他的第一台[超8毫米膠片](../Page/超8毫米膠片.md "wikilink")[攝影機](../Page/攝影機.md "wikilink")。他在22歲時，開始製作[短片](../Page/短片.md "wikilink")。1974年左右，他完成第一部電影短片。1970年代末，這些短片在馬德里和[巴塞羅那公開放映](../Page/巴塞羅那.md "wikilink")。這些短片敘述性愛情節，而且沒有配樂。

他在1978年完成首部超8毫米膠片長片《Folle, folle, fólleme,
Tim》，這是他第一次接觸專業的電影世界。這部電影的演員卡門·莫拉和Felix
Rotaeta鼓勵他使用16毫米膠片拍攝，並且幫助他解決財務問題。

### 成名

[PedroAlmodovar06TIFF.jpg](https://zh.wikipedia.org/wiki/File:PedroAlmodovar06TIFF.jpg "fig:PedroAlmodovar06TIFF.jpg")\]\]

佩德羅·阿莫多瓦·卡瓦耶羅在1980年執導處女作《[佩比、露西、朋](../Page/佩比、露西、朋\(1980年電影\).md "wikilink")》，電影[劇本由他親自完成](../Page/劇本.md "wikilink")。《[佩比、露西、朋](../Page/佩比、露西、朋\(1980年電影\).md "wikilink")》劇情充滿文化與性自由等[元素](../Page/元素.md "wikilink")，獲得許多[邪典電影影迷](../Page/邪典電影.md "wikilink")。

1988年，佩德羅·阿莫多瓦·卡瓦耶羅以《[崩溃边缘的女人](../Page/崩溃边缘的女人.md "wikilink")》赢得[威尼斯影展最佳剧本奖以及](../Page/威尼斯影展.md "wikilink")[欧洲影展最佳青年电影奖](../Page/欧洲影展.md "wikilink")，使他成为西班牙最受欢迎的新锐导演。佩德羅·阿莫多瓦·卡瓦耶羅因為《[崩溃边缘的女人](../Page/崩溃边缘的女人.md "wikilink")》展現出女性主義風格\[7\]，逐漸被認為是女性電影導演，類似[寧那·華納·法斯賓德與](../Page/寧那·華納·法斯賓德.md "wikilink")[乔治·丘克](../Page/乔治·丘克.md "wikilink")
。

1999年，阿莫多瓦以《[我的母親](../Page/我的母親.md "wikilink")》獲得[奧斯卡最佳外語片](../Page/奧斯卡最佳外語片獎.md "wikilink")、[金球獎最佳外語片獎](../Page/金球獎最佳外語片獎.md "wikilink")、[戛納電影節最佳導演獎](../Page/最佳導演獎_\(坎城影展\).md "wikilink")\[8\]、法國[凱撒獎最佳外語片獎](../Page/凱撒獎.md "wikilink")，[哥雅獎最佳影片獎](../Page/哥雅獎.md "wikilink")。《[我的母親](../Page/我的母親.md "wikilink")》劇情以一名中年的單親媽媽為中心，描述她在兒子死後，前去尋找前夫的[故事](../Page/故事.md "wikilink")，角色包含[變性](../Page/變性.md "wikilink")[妓女](../Page/妓女.md "wikilink")、懷孕的[修女和](../Page/修女.md "wikilink")[同性戀的女演員](../Page/同性戀.md "wikilink")。

三年後，阿莫多瓦以2002年的《[悄悄告訴她](../Page/悄悄告訴她.md "wikilink")》再奪下[金球獎最佳外語片獎](../Page/金球獎.md "wikilink")、英國影藝學院獎、[奧斯卡最佳原創劇本獎](../Page/奧斯卡最佳原創劇本獎.md "wikilink")\[9\]。《[悄悄告訴她](../Page/悄悄告訴她.md "wikilink")》劇情描述年輕的男[護士班尼諾](../Page/護士.md "wikilink")、[作家馬可](../Page/作家.md "wikilink")、[芭蕾舞者阿麗西亞](../Page/芭蕾舞.md "wikilink")、女[鬥牛士莉蒂亞之間的故事](../Page/鬥牛士.md "wikilink")。2004年電影《[壞教慾](../Page/壞教慾.md "wikilink")》則有[天主教神父猥褻](../Page/天主教.md "wikilink")[兒童](../Page/兒童.md "wikilink")、性虐待、勒索、謀殺等情節，阿莫多瓦使用[黑色電影的元素來創作](../Page/黑色電影.md "wikilink")。這部電影的主角胡安（蓋爾·加西亞·伯納爾），主要是仿照[派翠西亞·海史密斯所創造的人物](../Page/派翠西亞·海史密斯.md "wikilink")[湯姆·雷普利](../Page/湯姆·雷普利.md "wikilink")。阿莫多瓦花費十多年才完成《[壞教慾](../Page/壞教慾.md "wikilink")》電影劇本\[10\]，並在2004年於[第57屆戛納電影節首映](../Page/第57屆戛納電影節.md "wikilink")，成為第一部在戛納電影節擔任開幕片的[西班牙電影](../Page/西班牙電影.md "wikilink")\[11\]。

佩德羅·阿莫多瓦·卡瓦耶羅與[佩内洛普·克鲁兹合作完成](../Page/佩内洛普·克鲁兹.md "wikilink")《[玩美女人](../Page/玩美女人.md "wikilink")》，它是[喜劇](../Page/喜劇.md "wikilink")、[家庭劇和鬼故事的混合體](../Page/家庭劇.md "wikilink")，並獲得[第59屆坎城電影節最佳劇本](../Page/第59屆坎城電影節.md "wikilink")、[第59屆坎城電影節最佳女演員](../Page/第59屆坎城電影節.md "wikilink")、[哥雅獎最佳影片](../Page/哥雅獎.md "wikilink")、最佳導演、最佳女主角、最佳女配角、最佳原創音樂、[國家評論協會最佳外語片](../Page/國家評論協會.md "wikilink")。2013年，佩德羅·阿莫多瓦·卡瓦耶羅與[安东尼奥·班德拉斯](../Page/安东尼奥·班德拉斯.md "wikilink")、[佩内洛普·克鲁兹再度於](../Page/佩内洛普·克鲁兹.md "wikilink")《[HIGH爆雲霄](../Page/HIGH爆雲霄.md "wikilink")》中合作\[12\]。阿莫多瓦早期有不少作品都由[安东尼奥·班德拉斯主演](../Page/安东尼奥·班德拉斯.md "wikilink")，班德拉斯亦因此得到[荷里活的注意](../Page/荷里活.md "wikilink")；艾慕杜華可說是造就了班德拉斯的成功。

他與弟弟[奧斯丁·阿莫多瓦近期共同成立El](../Page/奧斯丁·阿莫多瓦.md "wikilink")
deseo慾望電影公司。2013年他获得[欧洲电影学院颁发的](../Page/欧洲电影学院.md "wikilink")[欧洲电影世界成就奖](../Page/欧洲电影奖.md "wikilink")。\[13\]

## 風格

[Rossy_de_Palma_-_Pedro_Almodovar_and_Penélope_Cruz_-_Cannes_2009.jpg](https://zh.wikipedia.org/wiki/File:Rossy_de_Palma_-_Pedro_Almodovar_and_Penélope_Cruz_-_Cannes_2009.jpg "fig:Rossy_de_Palma_-_Pedro_Almodovar_and_Penélope_Cruz_-_Cannes_2009.jpg")、[蘿西·德·帕瑪出席](../Page/蘿西·德·帕瑪.md "wikilink")2009年[坎城電影節](../Page/坎城電影節.md "wikilink")\]\]

阿莫多瓦的創作受到一些導演影響，像是[路易斯·布紐爾](../Page/路易斯·布努埃爾.md "wikilink")、[寧那·華納·法斯賓德](../Page/寧那·華納·法斯賓德.md "wikilink")、[亞佛烈德·希區考克](../Page/亞佛烈德·希區考克.md "wikilink")、[約翰·華特](../Page/約翰·華特.md "wikilink")（）、[英格瑪·柏格曼](../Page/英格瑪·柏格曼.md "wikilink")、[艾德加·納維爾](../Page/艾德加·納維爾.md "wikilink")（）、[費德里柯·費里尼](../Page/費德里柯·費里尼.md "wikilink")、[喬治·丘克](../Page/喬治·丘克.md "wikilink")、[路易·加西亞·貝蘭卡](../Page/路易·加西亞·貝蘭卡.md "wikilink")（）和[新寫實主義者](../Page/新寫實主義.md "wikilink")[馬可·菲萊利](../Page/馬可·菲萊利.md "wikilink")（）\[14\]。

他的電影敘事錯綜，情節誇張、戲劇化，風格呈現濃烈的艷麗色調，並融入流行文化、歌曲、性嘲諷等元素。他的作品長期關注女性、社會邊緣人和中下階層人民，探索慾望、愛、死亡、親情與性別認同等領域。

當記者尋問他電影的成功之道時，他回答道因為這些電影非常有趣：重要的關鍵是電影為[娛樂媒體](../Page/娛樂.md "wikilink")。他受到早期好萊塢電影的嚴重影響，劇情發生在女性周圍。

## 私生活

阿莫多瓦已經公開同性戀身分\[15\]，並將同性戀文化的元素融入主流文化中，重新定義了西班牙電影和西班牙印象。不過阿莫多瓦承認他的電影相當個人化，「也非常有西班牙的風格\[16\]，但是不能藉由我的電影來評估西班牙，因為它們是反复無常的個人化電影」\[17\]。

## 導演作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1980</p></td>
<td><p><a href="../Page/佩比、露西、朋(1980年電影).md" title="wikilink">佩比、露西、朋</a><br />
'' Pepi, Luci, Bom y otras chicas del montón''</p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="even">
<td><p>1982</p></td>
<td><p><a href="../Page/激情迷宮.md" title="wikilink">激情迷宮</a><br />
<em>Laberinto de pasiones</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="odd">
<td><p>1983</p></td>
<td><p><a href="../Page/修女夜難熬.md" title="wikilink">修女夜難熬</a><br />
<em>Entre tinieblas</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984</p></td>
<td><p><a href="../Page/我造了什麼孽？.md" title="wikilink">我造了什麼孽？</a><br />
<em>Qué he hecho yo para merecer esto?</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td><p><a href="../Page/鬥牛士.md" title="wikilink">鬥牛士</a><br />
<em>Matador</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="odd">
<td><p>1987</p></td>
<td><p><a href="../Page/慾望法則.md" title="wikilink">慾望法則</a><br />
<em>La ley del deseo</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/柏林影展.md" title="wikilink">柏林影展</a>: 首座<a href="../Page/泰迪熊獎.md" title="wikilink">泰迪熊獎</a></p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/瀕臨崩潰邊緣的女人.md" title="wikilink">瀕臨崩潰邊緣的女人</a><br />
<em>Mujeres al norde de un ataque de nervios</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/奧斯卡最佳外語片獎.md" title="wikilink">奧斯卡最佳外語片獎提名</a><br />
<a href="../Page/哥雅獎.md" title="wikilink">哥雅獎</a>：最佳影片</p></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/綑著妳，困著我.md" title="wikilink">綑著妳，困著我</a><br />
<em>¡Átame!</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/柏林影展.md" title="wikilink">柏林影展</a>：<a href="../Page/金熊獎.md" title="wikilink">金熊獎</a></p></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/高跟鞋_(電影).md" title="wikilink">高跟鞋</a><br />
<em>Tacones lejanos</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/凱撒電影獎.md" title="wikilink">凱撒電影獎</a>: 最佳外語片</p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/愛慾情狂.md" title="wikilink">愛慾情狂</a><br />
<em>Kika</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td><p><a href="../Page/窗邊上的玫瑰.md" title="wikilink">窗邊上的玫瑰</a><br />
<em>La flor de mi secreto</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/顫抖的慾望.md" title="wikilink">顫抖的慾望</a><br />
<em>Carne trémula</em></p></td>
<td><p><a href="../Page/雷·羅里加.md" title="wikilink">雷·羅里加</a>（Ray Loriga）和 Jorge Guerricaechevarría 共同編劇，源自<a href="../Page/藍黛兒.md" title="wikilink">藍黛兒</a>（Ruth Rendell）的小說</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/我的母親.md" title="wikilink">我的母親</a><br />
<em>Todo sobre mi madre</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/奧斯卡最佳外語片獎.md" title="wikilink">奧斯卡最佳外語片獎</a><br />
<a href="../Page/坎城影展.md" title="wikilink">坎城影展最佳導演</a><br />
<a href="../Page/坎城影展.md" title="wikilink">坎城影展人道精神特別獎</a><br />
<a href="../Page/英國電影和電視藝術學院.md" title="wikilink">英國影視學院獎</a>: 最佳外語片、最佳原創劇本<br />
<a href="../Page/凱撒電影獎.md" title="wikilink">凱撒電影獎</a>: 最佳外語片<br />
<a href="../Page/哥雅獎.md" title="wikilink">哥雅獎</a>: 最佳導演、最佳影片、最佳女演員、最佳剪輯、最佳配樂、最佳音效<br />
<a href="../Page/芝加哥影評人協會.md" title="wikilink">芝加哥影評人協會最佳外語片</a></p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/悄悄告訴她.md" title="wikilink">悄悄告訴她</a><br />
<em>Hable con ella</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/第75屆奧斯卡金像獎.md" title="wikilink">奧斯卡</a><a href="../Page/奧斯卡最佳原創劇本獎.md" title="wikilink">最佳原創劇本</a><br />
<a href="../Page/英國電影和電視藝術學院.md" title="wikilink">英國影視學院獎</a>: 最佳外語片、最佳原創劇本<br />
<a href="../Page/金球獎.md" title="wikilink">金球獎</a>: 最佳外語片<br />
<a href="../Page/哥雅獎.md" title="wikilink">哥雅獎</a>: 最佳原創音樂（Alberto Iglesias）<br />
<a href="../Page/洛杉磯影評人協會.md" title="wikilink">洛杉磯影評人協會最佳導演</a></p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/壞教慾.md" title="wikilink">壞教慾</a><br />
<em>La mala educación</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/紐約影評人協會.md" title="wikilink">紐約影評人協會最佳外語片</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/玩美女人.md" title="wikilink">玩美女人</a><br />
<em>Volver</em></p></td>
<td><p>原創劇本<br />
<a href="../Page/第59屆坎城電影節.md" title="wikilink">坎城影展最佳劇本獎</a><br />
<a href="../Page/第59屆坎城電影節.md" title="wikilink">坎城影展最佳女演員</a>（所有女性角色）<br />
<a href="../Page/哥雅獎.md" title="wikilink">哥雅獎</a>: 最佳影片、最佳導演、最佳女主角、最佳女配角、最佳原創音樂<br />
<a href="../Page/國家評論協會.md" title="wikilink">國家評論協會最佳外語片</a></p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/破碎的擁抱.md" title="wikilink">破碎的擁抱</a><br />
<em>Los abrazos rotos</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/切膚慾謀.md" title="wikilink">切膚慾謀</a><br />
<em>La piel que habito</em></p></td>
<td><p>劇本改編自法國作家<a href="../Page/提爾希·容凱.md" title="wikilink">提爾希·容凱</a>（Thierry Jonquet）的作品Mygale<br />
第65屆英國電影學院獎：最佳外語片</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/HIGH爆雲霄.md" title="wikilink">HIGH爆雲霄</a><br />
<em>Los amantes pasajeros</em></p></td>
<td><p>原創劇本</p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/沉默茱麗葉.md" title="wikilink">沉默茱麗葉</a><br />
<em>Julieta</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p><a href="../Page/痛苦和荣耀.md" title="wikilink">痛苦和荣耀</a><br />
<em>Dolor y gloria</em></p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

## 相關文獻

  - Allinson, Mark. *A Spanish Labyrinth: The Films of Pedro Almodóvar*,
    I.B Tauris Publishers, 2001, ISBN 1-86064-507-0
  - Almodóvar, Pedro. *Some Notes About the Skin I Live In*.
    [Taschen](../Page/Taschen.md "wikilink") Magazine, Winter 2011/12.
  - Bergan, Ronald. *Film*, D.K Publishing, 2006, ISBN 0-7566-2203-4
  - Cobos, Juan and Marias Miguel. *Almodóvar Secreto*, Nickel Odeon,
    1995
  - D’ Lugo, Marvin. *Pedro Almodóvar*, University of Illinois Press,
    2006, ISBN 0-252-07361-4-4
  - Edwards, Gwyne. *Almodóvar: labyrinths of Passion.* London: Peter
    Owen. 2001, ISBN 0-7206-1121-0
  - Strauss, Frederick. '' Almodóvar on Almodóvar'', Faber and Faber,
    2006, ISBN 0-571-23192-6

## 外部連結

  - [Viva Pedro](http://www.vivapedro.com) Official website of the Viva
    Pedro series, featuring the theatrical re-release of 8 of the
    celebrated auteur's films.

  -
[Category:西班牙电影导演](../Category/西班牙电影导演.md "wikilink")
[Category:男性编剧](../Category/男性编剧.md "wikilink")
[Category:西班牙编剧](../Category/西班牙编剧.md "wikilink")
[Category:西班牙电影制片人](../Category/西班牙电影制片人.md "wikilink")
[Category:奧斯卡最佳原創劇本獎獲獎者](../Category/奧斯卡最佳原創劇本獎獲獎者.md "wikilink")
[Category:英国电影学院奖最佳导演得主](../Category/英国电影学院奖最佳导演得主.md "wikilink")
[Category:欧洲电影奖最佳导演获得者](../Category/欧洲电影奖最佳导演获得者.md "wikilink")
[Category:凱撒電影獎獲得者](../Category/凱撒電影獎獲得者.md "wikilink")
[Category:LGBT導演](../Category/LGBT導演.md "wikilink")
[Category:西班牙LGBT人物](../Category/西班牙LGBT人物.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:哈佛大学荣誉博士](../Category/哈佛大学荣誉博士.md "wikilink")
[Category:奥斯卡最佳外语片获奖导演](../Category/奥斯卡最佳外语片获奖导演.md "wikilink")
[Category:英国电影学院奖最佳外语片获奖导演](../Category/英国电影学院奖最佳外语片获奖导演.md "wikilink")
[Category:戛纳电影节评审团主席](../Category/戛纳电影节评审团主席.md "wikilink")
[Category:英国电影学院奖最佳原创剧本获得者](../Category/英国电影学院奖最佳原创剧本获得者.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.  ["Pedro Almodovar." Encyclopedia of World Biography. Thomson
    Gale. 2004. Encyclopedia.com. 2
    Jan. 2010](http://www.encyclopedia.com)

2.

3.
4.  [Ten honorary degrees awarded at
    Commencement](http://news.harvard.edu/gazette/story/2009/06/ten-honorary-degrees-awarded-at-commencement)

5.  D’Lugo, '' Pedro Almodóvar'', p. 13

6.  Allison, *A Spanish Labyrinth*, p. 7

7.  Almodóvar Secreto: Cobos and Marias, p.100

8.

9.

10.

11.

12. [Almodovar laughs with The Brief
    Lovers](http://www.screendaily.com/news/production/almodovar-laughs-with-the-brief-lovers/5038013.article?referrer=RSS)

13.

14. D’Lugo, '' Pedro Almodóvar'', p. 14

15.

16. Film: Bergan, p.252

17.