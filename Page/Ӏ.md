__NOTOC__

**Ӏ**, **ӏ**（，*paločka* 或
*palochka*，意思是“棍子”）是一个[西里尔字母](../Page/西里尔字母.md "wikilink")\[1\]，用来书写[高加索语言](../Page/高加索语系.md "wikilink")，如
[阿巴札语](../Page/阿巴札语.md "wikilink")、[阿迪格语](../Page/阿迪格语.md "wikilink")、[阿瓦尔语](../Page/阿瓦尔语.md "wikilink")、[车臣语](../Page/车臣语.md "wikilink")、、[印古什语](../Page/印古什语.md "wikilink")、[阿瓦爾語](../Page/阿瓦爾語.md "wikilink")、[卡巴尔达语](../Page/卡巴尔达语.md "wikilink")、[拉克语](../Page/拉克语.md "wikilink")（）、[莱兹金语](../Page/莱兹金语.md "wikilink")（（））和。

在 Unicode 5.0 版之前，只收录了大写 páločka；小写 páločka 在 2006 年的 Unicode 5.0
版才被收录，故此大多数字体都不能显示小写 páločka 。

## 音值

Páločka
很多时用来指出它前面的辅音是一個[挤喉音](../Page/挤喉音.md "wikilink")(緊喉輔音/[緊喉音](../Page/緊喉音.md "wikilink"))。

在阿迪格語、卡巴尔达语、印古什语等语言，这字母同时用作
（[喉塞音](../Page/喉塞音.md "wikilink")）。在车臣语，这字母用作
（[咽音](../Page/咽音.md "wikilink")）。

## 转写

当这字母[转写为拉丁字母时](../Page/转写.md "wikilink")，以“‡”表示。

## 字符

## 参看

  - [ISO 9](../Page/ISO_9.md "wikilink")（西里尔字母转写）

## 参考资料

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")

1.  ["Cyrillic:
    Range: 0400–04FF"](http://unicode.org/charts/PDF/U0400.pdf). The
    Unicode Standard, Version 6.0. 2010. pp. 42, 43. Retrieved
    2011-05-23.