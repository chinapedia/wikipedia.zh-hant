**縣道169號
豐山－里佳**，北起[嘉義縣](../Page/嘉義縣.md "wikilink")[梅山鄉太和村社興橋](../Page/梅山鄉_\(台灣\).md "wikilink")，縱貫[阿里山地區](../Page/阿里山山脈.md "wikilink")，南至嘉義縣[阿里山鄉里佳](../Page/阿里山鄉.md "wikilink")，全長共計50.246公里（公路總局資料）。本縣道原起點為阿里山鄉[豐山村](../Page/豐山村.md "wikilink")，但豐山至太和村社興橋路段不知何故未納入里程，現為一般產業道路。原終點阿里山鄉[達邦村](../Page/達邦村.md "wikilink")，後來納入至里佳部落的產業道路，終點遂位於阿里山鄉[里佳村](../Page/里佳村.md "wikilink")。

## 行經行政區域

[Stone_table_at_Shihjhuo.JPG](https://zh.wikipedia.org/wiki/File:Stone_table_at_Shihjhuo.JPG "fig:Stone_table_at_Shihjhuo.JPG")路口。\]\]
    梅山鄉 |tspan = 6 |location = 社後坪 |km = *（未納里程）* |notes = }}

## 沿線風景

[Fenchihu_Station07.jpg](https://zh.wikipedia.org/wiki/File:Fenchihu_Station07.jpg "fig:Fenchihu_Station07.jpg")

  - [豐山風景區](../Page/豐山風景區.md "wikilink")
  - [太和風景區](../Page/太和風景區.md "wikilink")
  - [奮起湖](../Page/奮起湖.md "wikilink")
      - 奮起湖老街
      - 奮起湖文史陳列室
  - [大凍山](../Page/大凍山_\(嘉義縣\).md "wikilink")

## 參見

## 相關連結

[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:嘉義縣道路](../Category/嘉義縣道路.md "wikilink")