[Turkey_ancient_region_map_caria.JPG](https://zh.wikipedia.org/wiki/File:Turkey_ancient_region_map_caria.JPG "fig:Turkey_ancient_region_map_caria.JPG")
**卡里亚**（[古希腊语](../Page/古希腊语.md "wikilink")：****）[安纳托利亚历史上的一个地区](../Page/安纳托利亚.md "wikilink")，在今[土耳其境内](../Page/土耳其.md "wikilink")。

卡里亚位于[伊奥尼亚以南](../Page/伊奥尼亚.md "wikilink")，[弗里吉亚和](../Page/弗里吉亚.md "wikilink")[吕基亚以西](../Page/吕基亚.md "wikilink")。[希腊人把当地的原住民称为](../Page/希腊.md "wikilink")“[卡里亚人](../Page/卡里亚人.md "wikilink")”。远在希腊人到达之前，与“卡里亚”这个名字相当的地名可能就已经出现在许多中东古国的记载中了，如[赫梯人所说的](../Page/赫梯.md "wikilink")“卡尔基亚”，[巴比伦人所说的](../Page/巴比伦人.md "wikilink")“卡尔萨”和[波斯人所说的](../Page/波斯人.md "wikilink")“库尔卡”。另一些文献则称，该地区原来被叫作“腓尼基亚”，因为当地在很早的时候曾有一个[腓尼基人的殖民地](../Page/腓尼基人.md "wikilink")。至于“卡里亚”这个名字的由来，据说与神话传说中的腓尼基国王卡耳有关。[荷马在](../Page/荷马.md "wikilink")《[伊利亚特](../Page/伊利亚特.md "wikilink")》中写到，在[特洛伊战争时](../Page/特洛伊战争.md "wikilink")[米利都城属于卡里亚人](../Page/米利都.md "wikilink")，并且是[特洛伊一方的盟友](../Page/特洛伊.md "wikilink")。

在赫梯帝国崩溃后，卡里亚是安纳托利亚地区众多的独立小国之一。[阿契美尼德王朝的崛起使卡里亚被并入](../Page/阿契美尼德王朝.md "wikilink")[波斯帝国](../Page/波斯帝国.md "wikilink")，并成为帝国的一个[省](../Page/省_\(波斯帝国\).md "wikilink")（约在前545年）。该地区最主要的[城市是](../Page/城市.md "wikilink")[哈利卡尔纳苏斯](../Page/哈利卡尔纳苏斯.md "wikilink")，为[总督的驻节地](../Page/总督.md "wikilink")。其他的大城市包括[赫拉克利亚](../Page/赫拉克利亚.md "wikilink")，[迈安德河畔安条克](../Page/迈安德河畔安条克.md "wikilink")，[密都斯](../Page/密都斯.md "wikilink")，[劳迪基亚](../Page/代尼兹利.md "wikilink")，[阿林达和](../Page/阿林达.md "wikilink")[阿拉班达](../Page/阿拉班达.md "wikilink")。

卡里亚省的首府哈利卡尔纳苏斯是[世界七大奇迹之一的](../Page/世界七大奇迹.md "wikilink")[摩索拉斯王陵墓的所在地](../Page/摩索拉斯王陵墓.md "wikilink")。[摩索拉斯是波斯帝国驻卡里亚的总督](../Page/摩索拉斯.md "wikilink")，该陵墓係由其妻子[阿耳忒弥西亚所建](../Page/阿耳忒弥西亚二世.md "wikilink")。

前334年，[亚历山大大帝征服了卡里亚](../Page/亚历山大大帝.md "wikilink")。亚历山大死后，该地区先后属于[利西马古](../Page/利西马古.md "wikilink")、[塞琉古帝国和](../Page/塞琉古帝国.md "wikilink")[罗马](../Page/罗马共和国.md "wikilink")。[罗马帝国后期](../Page/罗马帝国.md "wikilink")，卡里亚成为一个独立的[行省](../Page/罗马帝国行省.md "wikilink")。

## 外部链接

  - [卡里亚的历史与文化](http://www.livius.org/cao-caz/caria/caria.html)
  - [卡里亚城市照片集](http://www.losttrails.com/pages/Hproject/Caria/Caria.html)

[Category:土耳其地理](../Category/土耳其地理.md "wikilink")
[Category:土耳其历史](../Category/土耳其历史.md "wikilink")
[Category:罗马帝国行省](../Category/罗马帝国行省.md "wikilink")