[Niigata_Airport_Aerial_photograph.2009.jpg](https://zh.wikipedia.org/wiki/File:Niigata_Airport_Aerial_photograph.2009.jpg "fig:Niigata_Airport_Aerial_photograph.2009.jpg")
[Niigata_Airport_from_Apron.JPG](https://zh.wikipedia.org/wiki/File:Niigata_Airport_from_Apron.JPG "fig:Niigata_Airport_from_Apron.JPG")

**新潟機場**（，）是位於[日本](../Page/日本.md "wikilink")[新潟市的民用機場](../Page/新潟市.md "wikilink")，位處新潟市區的東北方，北鄰[日本海](../Page/日本海.md "wikilink")，東靠的入海口。根據日本《空港法》分類為「國管理機場」。其以開行[國內航線為主](../Page/國內航線.md "wikilink")，亦有數條[國際航線](../Page/國際航線.md "wikilink")。

## 航空公司與航點

## 參考來源

  -

<div class="references-small">

<references />

</div>

## 外部連結

  - [新潟機場](http://www.niigata-airport.gr.jp/)

  -
  -
{{-}}

[Category:新潟縣交通](../Category/新潟縣交通.md "wikilink")
[Category:中部地方機場](../Category/中部地方機場.md "wikilink")
[Category:東區 (新潟市)](../Category/東區_\(新潟市\).md "wikilink")