[Nakajima_Ki-43_Hayabusa_"Peregrine_Falcon".jpg](https://zh.wikipedia.org/wiki/File:Nakajima_Ki-43_Hayabusa_"Peregrine_Falcon".jpg "fig:Nakajima_Ki-43_Hayabusa_\"Peregrine_Falcon\".jpg")
**加藤隼戰鬥隊**（**飛行第64戰隊**）是[太平洋戰爭初期由](../Page/太平洋戰爭.md "wikilink")[日本陸軍](../Page/日本陸軍.md "wikilink")[加藤建夫](../Page/加藤建夫.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[中佐](../Page/中佐.md "wikilink")（戰死後為[少將](../Page/少將.md "wikilink")）率領的飛行戰隊（[通稱號](../Page/通稱號.md "wikilink")「高二一九四部隊」）主要是在南方戰線活躍，其後該隊的事跡被制作成同名[電影](../Page/電影.md "wikilink")，而採用的[軍歌](../Page/軍歌.md "wikilink")（戰隊歌）亦相當有名。
\[1\]

## 歷史

1938年（昭和13年）8月1日，日本陸軍在中國[河南省安陽縣的彰德機場集結飛行第](../Page/河南省.md "wikilink")2大隊（在立川成立的飛行第5連隊改編）的第1、第2中隊、獨立飛行第9中隊（在[平壤成軍的飛行第](../Page/平壤.md "wikilink")6連隊所屬）三個飛行中隊共27架戰機合編，正式編制稱**飛行第64戰隊**，指揮官寺西多美彌[少校](../Page/少校.md "wikilink")。

在64戰隊成軍前，飛行第2大隊隸屬在第一飛行團下，已經在[華北轉戰多度](../Page/華北.md "wikilink")，並在[徐州會戰期間與](../Page/徐州會戰.md "wikilink")[中華民國空軍多度交戰](../Page/中華民國空軍.md "wikilink")。在1938年9月開始加入[武漢會戰](../Page/武漢會戰.md "wikilink")，10月9日輪調至廣東。1939年移防到[海拉爾](../Page/海拉爾區.md "wikilink")，參加了[諾門罕戰役](../Page/諾門罕戰役.md "wikilink")。在日本陸軍舉兵南侵東南亞前，64戰隊在幾次大型空戰中都納入戰列，麾下飛官亦有充足的空戰經驗。

這支部隊後來被帝國陸軍頌揚的緣由是1941年4月第4任隊長加藤建夫上任以後，64戰隊在1941年8月成為首先領取一式戰鬥機·隼的唯二單位，也是帝國陸軍南侵的空優主力支柱，而後「加藤隼戰鬥隊」這個暱稱的來源也是源於64戰隊在對英美開戰時的優異表現。

1941年12月3日，64戰隊移駐[法屬印度支那的](../Page/法屬印度支那.md "wikilink")[富國島](../Page/富國島.md "wikilink")，掩護從海上運輸到[泰國的日本陸軍船團](../Page/泰國.md "wikilink")；在日本陸軍借道泰國攻入[英屬緬甸時](../Page/英屬緬甸.md "wikilink")，64戰隊開始在東南亞與[同盟國戰機作戰](../Page/同盟國.md "wikilink")。先後在[馬來半島](../Page/馬來半島.md "wikilink")、[爪哇](../Page/爪哇島.md "wikilink")、[緬甸方面與](../Page/緬甸.md "wikilink")[英國皇家空軍](../Page/英國皇家空軍.md "wikilink")、[中國空軍及支援中國空軍的](../Page/中華民國空軍.md "wikilink")[飛虎隊進行作戰](../Page/飛虎隊.md "wikilink")。\[2\]1942年（昭和17年）5月22日，加藤與僚機共同追撃攻擊基地的[布倫亨式轟炸機時](../Page/布倫亨式轟炸機.md "wikilink")，被布倫海姆式機尾部的機槍擊中[戰死](../Page/戰死.md "wikilink")。加藤死後獲2階級特進成為少將，並被讚揚為[軍神](../Page/軍神.md "wikilink")。

## 組織人事

### 戰隊長

  - [寺西多美彌](../Page/寺西多美彌.md "wikilink") 少佐：8月1日 - 3月9日
  - [横山八男](../Page/横山八男.md "wikilink") 少佐：昭和14年3月9日 - 昭和14年10月26日
  - [佐藤猛夫](../Page/佐藤猛夫.md "wikilink") 少佐：昭和14年10月26日 - 昭和16年4月10日
  - [加藤建夫](../Page/加藤建夫.md "wikilink") 中佐：昭和16年4月10日 -
    昭和17年5月22日戰死（戰死後特進升少將）
  - [八木正巳](../Page/八木正巳.md "wikilink") 少佐：昭和17年5月29日 - 昭和18年2月12日戰死
  - [明樂武世](../Page/明樂武世.md "wikilink") 少佐：昭和18年2月23日 - 昭和18年2月25日戰死
  - [廣瀬吉雄](../Page/廣瀬吉雄.md "wikilink") 少佐：昭和18年3月5日 - 昭和19年6月10日
  - [江藤豊喜](../Page/江藤豊喜.md "wikilink") 少佐：昭和19年6月10日 - 昭和20年4月28日
  - [宮邊英夫](../Page/宮邊英夫.md "wikilink") 少佐：4月28日 - 終戰

### 飛行隊長

  - [奥山清藏](../Page/奥山清藏.md "wikilink") 大尉：6月 - 3月
  - [藤本秋助](../Page/藤本秋助.md "wikilink") 大尉：昭和17年5月 - 昭和17年6月18日戰死
  - [明樂武世](../Page/明樂武世.md "wikilink") 少佐：昭和17年10月 - 昭和18年2月23日（升戰隊長）
  - [黒江保彦](../Page/黒江保彦.md "wikilink") 大尉：昭和18年3月 - 昭和18年12月
  - [宮邊英夫](../Page/宮邊英夫.md "wikilink") 少佐：昭和19年4月 - 昭和20年4月28日
  - [角田一郎](../Page/角田一郎.md "wikilink") 大尉：4月 - 終戰

### 第1中隊長

  - [澤田貢](../Page/澤田貢.md "wikilink") 大尉：昭和13年8月1日 - 3月
  - [丸田文雄](../Page/丸田文雄.md "wikilink") 大尉：昭和14年3月 - 昭和16年5月
  - [高橋三郎](../Page/高橋三郎.md "wikilink") 中尉：昭和16年5月 - 昭和16年12月7日戰死
  - [丸尾晴康](../Page/丸尾晴康.md "wikilink") 大尉：昭和17年1月 - 昭和17年11月10日戰死
  - [高橋俊二](../Page/高橋俊二.md "wikilink") 中尉：昭和17年11月 - 昭和18年9月6日戰死
  - [相原彪夫](../Page/相原彪夫.md "wikilink") 大尉：昭和18年10月 - 昭和18年12月26日戰死
  - [中村三郎](../Page/中村三郎.md "wikilink") 大尉：昭和18年12月 - 昭和19年10月3日戰死
  - [岡崎正三](../Page/岡崎正三.md "wikilink") 大尉：12月 - 1月15日戰死

### 第2中隊長

  - [中尾次六](../Page/中尾次六.md "wikilink") 大尉：8月1日 - 7月
  - [安西秀一](../Page/安西秀一.md "wikilink") 大尉：昭和14年7月 - 昭和14年9月1日
  - [坂井菴](../Page/坂井菴.md "wikilink") 大尉：昭和14年9月1日 - 昭和16年7月
  - [高山忠雄](../Page/高山忠雄.md "wikilink") 中尉：昭和16年7月 - 昭和16年12月22日戰死
  - [大谷益造](../Page/大谷益造.md "wikilink") 大尉：昭和17年1月 - 昭和17年12月5日戰死
  - [關二郎](../Page/關二郎.md "wikilink") 大尉：昭和18年1月 - 昭和18年2月13日戰死
  - [宮邊英夫](../Page/宮邊英夫.md "wikilink") 少佐：昭和18年3月15日 - 昭和19年4月
  - [松井弘至](../Page/松井弘至.md "wikilink") 中尉：6月 -9月

### 第3中隊長

  - [鈴木五郎](../Page/鈴木五郎.md "wikilink") 大尉：8月1日 - 9月
  - [安間克巳](../Page/安間克巳.md "wikilink") 大尉：昭和15年9月 - 昭和17年4月8日
  - [黒江保彦](../Page/黒江保彦.md "wikilink") 大尉：昭和17年4月 - 昭和18年3月
  - [遠藤健](../Page/遠藤健.md "wikilink") 中尉：昭和18年3月 - 昭和18年5月15日戰死
  - [檜與平](../Page/檜與平.md "wikilink") 中尉：昭和18年5月 - 昭和18年12月
  - [黒澤直](../Page/黒澤直.md "wikilink") 大尉：和19年1月 - 昭和19年5月15日戰死
  - [北鄉丈夫](../Page/北鄉丈夫.md "wikilink") 大尉：6月 - 9月10日戰死

### 戰隊附

  - [若松幸禧](../Page/若松幸禧.md "wikilink") 中尉：9月 - 5月
  - [相澤忠雄](../Page/相澤忠雄.md "wikilink") 中尉：

## 電影《加藤隼戰鬥隊》

昭和19年（1944年），[東寶將](../Page/東寶株式會社.md "wikilink")『加藤隼戰鬥隊』電影化。由[陸軍省作為後援](../Page/陸軍省.md "wikilink")，並由情報局選定成為國民電影公開。

  - 監督：[山本嘉次郎](../Page/山本嘉次郎.md "wikilink")
  - 特攝監督：[圓谷英二](../Page/圓谷英二.md "wikilink")
  - 出演：[藤田進](../Page/藤田進.md "wikilink")（加藤隊長役）、[大河内傳次郎](../Page/大河内傳次郎.md "wikilink")、[志村喬](../Page/志村喬.md "wikilink")、[灰田勝彦](../Page/灰田勝彦.md "wikilink")、[高田稔](../Page/高田稔.md "wikilink")、[黒川彌太郎](../Page/黒川彌太郎.md "wikilink")、[沼崎勳](../Page/沼崎勳.md "wikilink")、[中村彰](../Page/中村彰.md "wikilink")、[河野秋武他](../Page/河野秋武.md "wikilink")

## 軍歌

在昭和15年（1940年）3月於[南寧創作完成](../Page/南寧.md "wikilink")。正式曲名是「飛行第六十四戰隊歌」。

  - 作詞：飛行第64戰隊所屬　田中林平准尉
  - 作曲：南支派遣軍樂隊所屬　原田喜一軍曹、岡野正幸軍曹（只為第4段的旋律）
  - [JASRAC管理著作物](../Page/日本音樂著作權協會.md "wikilink")。

## 關連項目

  - [大日本帝國陸軍飛行戰隊列表](../Page/大日本帝國陸軍飛行戰隊列表.md "wikilink")
  - [許崙墩](../Page/許崙墩.md "wikilink")

## 注釋

## 参考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  -
[category:大日本帝國陸軍飛行戰隊](../Page/category:大日本帝國陸軍飛行戰隊.md "wikilink")
[category:第二次世界大戰電影](../Page/category:第二次世界大戰電影.md "wikilink")
[category:航空電影](../Page/category:航空電影.md "wikilink")
[category:1944年电影](../Page/category:1944年电影.md "wikilink")

[Katouhayafusasentoutai](../Category/日本電影作品.md "wikilink")
[Katouhayafusasentoutai](../Category/1944年電影.md "wikilink")
[Katouhayabusasentoutai](../Category/日本战争片.md "wikilink")
[Category:日本軍歌](../Category/日本軍歌.md "wikilink")
[Category:太平洋戰爭東南亞前線題材作品](../Category/太平洋戰爭東南亞前線題材作品.md "wikilink")

1.  「隼戰鬥隊」的「隼」是該部隊使用的[一式戰鬥機「隼」的簡稱](../Page/一式戰鬥機.md "wikilink")，而實際上，戰隊歌完成的時期仍在使用[九七式戰鬥機](../Page/九七式戰鬥機.md "wikilink")，不過將戰鬥機稱為[隼](../Page/遊隼.md "wikilink")（）是很普遍的事情。飛行第64戰隊在配備[一式戰鬥機之前是使用](../Page/一式戰鬥機.md "wikilink")[九七式戰鬥機與](../Page/九七式戰鬥機.md "wikilink")[九五式戰鬥機](../Page/九五式戰鬥機.md "wikilink")
2.  [「模型阿嬤」傳奇老伴許崙墩　台籍日本兵唯一空戰英雄](http://www.ettoday.net/news/20140112/315308.htm),
    [ETtoday](../Page/ETtoday.md "wikilink"), 2014年01月12日