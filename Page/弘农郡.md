**弘农郡**，是中国[汉朝至](../Page/汉朝.md "wikilink")[唐朝的一个](../Page/唐朝.md "wikilink")[郡置](../Page/郡.md "wikilink")，其范围历代有一定变化，以[西汉为最大](../Page/西汉.md "wikilink")，包括今天[河南省西部的](../Page/河南省.md "wikilink")[三门峡市](../Page/三门峡市.md "wikilink")、[南阳市西部](../Page/南阳市.md "wikilink")，以及[陕西省东南部的](../Page/陕西省.md "wikilink")[商洛市](../Page/商洛市.md "wikilink")。由于其地处[长安](../Page/长安.md "wikilink")、[洛阳之间的](../Page/洛阳.md "wikilink")[黄河南岸](../Page/黄河.md "wikilink")，一直是历代[军事](../Page/军事.md "wikilink")[政治要地](../Page/政治.md "wikilink")。

## 漢代弘農郡

[西汉](../Page/西汉.md "wikilink")[元鼎四年](../Page/元鼎.md "wikilink")（前113年），[汉武帝设立弘农郡](../Page/汉武帝.md "wikilink")，设郡治在[秦国名关](../Page/秦国.md "wikilink")[函谷关边](../Page/函谷关.md "wikilink")，县名也是[弘农](../Page/弘农县.md "wikilink")，故址在今天[河南省](../Page/河南省.md "wikilink")[三门峡市](../Page/三门峡市.md "wikilink")[灵宝市东北](../Page/灵宝市.md "wikilink")，辖11县，118911户，475954人。[东汉](../Page/东汉.md "wikilink")、[三国沿置](../Page/三国.md "wikilink")，但今[商洛市范围划规](../Page/商洛市.md "wikilink")[京兆尹](../Page/京兆尹.md "wikilink")，只领有河南省西部范围。[西晋时](../Page/西晋.md "wikilink")，郡南部析置[上洛郡](../Page/上洛郡.md "wikilink")，其区域进一步缩小到黄河流域今[三门峡市范围](../Page/三门峡市.md "wikilink")。[南北朝时](../Page/南北朝.md "wikilink")，为避讳[北魏獻文帝](../Page/北魏.md "wikilink")[拓跋弘之名曾改为](../Page/拓跋弘.md "wikilink")**恒农郡**，[隋朝時廢郡](../Page/隋朝.md "wikilink")。

## 隋代弘農郡

隋炀帝[大業年間](../Page/大業.md "wikilink")，改虢州為弘農郡，郡治弘农向西南迁到了今[灵宝市中心](../Page/灵宝市.md "wikilink")，且失去了黄河沿岸的辖地。

[唐朝时](../Page/唐朝.md "wikilink")，弘农郡分为**陕州**、**虢州**，从此失去郡名。虢州仍治[弘农县](../Page/弘农县.md "wikilink")（今灵宝市），而陕州境内\[天宝
(唐朝)|天宝\]\]元年因为在函谷关遗迹发现宝符而更名为**灵宝**的桃林县，却不是今天的灵宝市所在地，而是汉晋时的[弘农县所在地](../Page/弘农县.md "wikilink")。到[北宋时](../Page/北宋.md "wikilink")，弘农县先改为**常农**，后以州名改为**虢略**。从此弘农不再作为地名使用。

## 著名家族

  - [弘农杨氏](../Page/弘农杨氏.md "wikilink")

## 参考文献

  - 《[中国历史地图集](../Page/中国历史地图集.md "wikilink")》
  - 《[汉书](../Page/汉书.md "wikilink")》
  - 《[后汉书](../Page/后汉书.md "wikilink")》
  - 《[晋书](../Page/晋书.md "wikilink")》
  - 《[隋书](../Page/隋书.md "wikilink")》
  - 《[唐书](../Page/唐书.md "wikilink")》
  - 《[宋史](../Page/宋史.md "wikilink")》

{{-}}

[Category:汉朝的郡](../Category/汉朝的郡.md "wikilink")
[Category:曹魏的郡](../Category/曹魏的郡.md "wikilink")
[Category:西晋的郡](../Category/西晋的郡.md "wikilink")
[Category:五胡十六国的郡](../Category/五胡十六国的郡.md "wikilink")
[Category:北魏的郡](../Category/北魏的郡.md "wikilink")
[Category:隋朝的郡](../Category/隋朝的郡.md "wikilink")
[Category:唐朝的郡](../Category/唐朝的郡.md "wikilink")
[Category:河南的郡](../Category/河南的郡.md "wikilink")
[Category:陕西的郡](../Category/陕西的郡.md "wikilink")