**NBA年度最佳新秀**（）是每年由[美國](../Page/美國.md "wikilink")[NBA聯盟頒發給](../Page/NBA.md "wikilink")[例行賽表現最優秀的該年新秀之獎項](../Page/NBA例行賽.md "wikilink")，首次頒獎是在1953年，獎盃的全名為「[艾迪·戈特里布獎](../Page/艾迪·戈特里布.md "wikilink")」。

## 歷屆最佳新秀

[Wilt_Chamberlain3.jpg](https://zh.wikipedia.org/wiki/File:Wilt_Chamberlain3.jpg "fig:Wilt_Chamberlain3.jpg")奪得的最佳新秀獎，他的新秀球季便有每場平均37.6分的成績。\]\]
[Kareem_Abdul_Jabbar_crop.jpg](https://zh.wikipedia.org/wiki/File:Kareem_Abdul_Jabbar_crop.jpg "fig:Kareem_Abdul_Jabbar_crop.jpg")在贏得最佳新秀獎\]\]
[Larrybird.jpg](https://zh.wikipedia.org/wiki/File:Larrybird.jpg "fig:Larrybird.jpg")在贏得最佳新秀獎\]\]
[Michael_Jordan.jpg](https://zh.wikipedia.org/wiki/File:Michael_Jordan.jpg "fig:Michael_Jordan.jpg")在贏得最佳新秀獎\]\]
[Allen_Iverson,_Denver_Nuggets.jpg](https://zh.wikipedia.org/wiki/File:Allen_Iverson,_Denver_Nuggets.jpg "fig:Allen_Iverson,_Denver_Nuggets.jpg")在贏得最佳新秀獎\]\]
[LebronFT2.jpg](https://zh.wikipedia.org/wiki/File:LebronFT2.jpg "fig:LebronFT2.jpg")在贏得最佳新秀獎\]\]

|    |                                                     |
| -- | --------------------------------------------------- |
| ^  | 以此顏色標示者表示為現役球員                                      |
| \* | 以此顏色標示者表示為入選[籃球名人堂之球員](../Page/籃球名人堂.md "wikilink") |
| T  | [地緣選秀](../Page/地緣選秀.md "wikilink")                  |

<table>
<thead>
<tr class="header">
<th><p>球季</p></th>
<th><p>球員</p></th>
<th><p>場上位置</p></th>
<th><p>國籍</p></th>
<th><p>所屬球隊</p></th>
<th><p>選秀順位</p></th>
<th><p>選秀年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/唐·曼尼克.md" title="wikilink">唐·曼尼克</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">韋恩堡活塞</a></p></td>
<td></td>
<td><p><a href="../Page/1952年NBA選秀.md" title="wikilink">1952</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/雷·菲利克斯.md" title="wikilink">雷·菲利克斯</a></p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/巴爾的摩子彈_(1944年–1954年).md" title="wikilink">巴爾的摩子彈隊</a></p></td>
<td></td>
<td><p><a href="../Page/1953年NBA選秀.md" title="wikilink">1953</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/鲍勃·佩蒂特.md" title="wikilink">鲍勃·佩蒂特</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">密爾瓦基老鷹</a></p></td>
<td></td>
<td><p><a href="../Page/1954年NBA選秀.md" title="wikilink">1954</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/莫里斯·斯托克斯.md" title="wikilink">莫里斯·斯托克斯</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">羅徹斯特皇家</a></p></td>
<td></td>
<td><p><a href="../Page/1955年NBA選秀.md" title="wikilink">1955</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/湯姆·海因索恩.md" title="wikilink">湯姆·海因索恩</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p>T</p></td>
<td><p><a href="../Page/1956年NBA選秀.md" title="wikilink">1956</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/伍迪·索德贝里.md" title="wikilink">伍迪·索德贝里</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">費城勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1957年NBA選秀.md" title="wikilink">1957</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/埃爾金·貝勒.md" title="wikilink">埃爾金·貝勒</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/明尼亞波利斯湖人.md" title="wikilink">明尼亞波利斯湖人</a></p></td>
<td></td>
<td><p><a href="../Page/1958年NBA選秀.md" title="wikilink">1958</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/威爾特·張伯倫.md" title="wikilink">威爾特·張伯倫</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">費城勇士</a></p></td>
<td><p>T</p></td>
<td><p><a href="../Page/1959年NBA選秀.md" title="wikilink">1959</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/奧斯卡·羅伯特森.md" title="wikilink">奧斯卡·羅伯特森</a>*</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">辛辛那提皇家</a></p></td>
<td></td>
<td><p><a href="../Page/1960年NBA選秀.md" title="wikilink">1960</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/沃尔特·貝拉米.md" title="wikilink">沃尔特·貝拉米</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">芝加哥包裝工人</a></p></td>
<td></td>
<td><p><a href="../Page/1961年NBA選秀.md" title="wikilink">1961</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/特里·狄辛格.md" title="wikilink">特里·狄辛格</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">芝加哥西風</a></p></td>
<td></td>
<td><p><a href="../Page/1962年NBA選秀.md" title="wikilink">1962</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/傑瑞·盧卡斯.md" title="wikilink">傑瑞·盧卡斯</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">辛辛那提皇家</a></p></td>
<td><p>T</p></td>
<td><p><a href="../Page/1962年NBA選秀.md" title="wikilink">1962</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/威利斯·瑞德.md" title="wikilink">威利斯·瑞德</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td></td>
<td><p><a href="../Page/1964年NBA選秀.md" title="wikilink">1964</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/里克·巴里.md" title="wikilink">里克·巴里</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">舊金山勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1965年NBA選秀.md" title="wikilink">1965</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大衛·賓恩.md" title="wikilink">大衛·賓恩</a>*</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td></td>
<td><p><a href="../Page/1966年NBA選秀.md" title="wikilink">1966</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/厄尔·门罗.md" title="wikilink">厄尔·门罗</a>*</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">巴爾的摩子彈</a></p></td>
<td></td>
<td><p><a href="../Page/1967年NBA選秀.md" title="wikilink">1967</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/韦斯·昂塞尔德.md" title="wikilink">韦斯·昂塞尔德</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">巴爾的摩子彈</a></p></td>
<td></td>
<td><p><a href="../Page/1968年NBA選秀.md" title="wikilink">1968</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/卡里姆·阿卜杜勒·賈巴尔.md" title="wikilink">卡里姆·阿卜杜勒·賈巴尔</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td></td>
<td><p><a href="../Page/1969年NBA選秀.md" title="wikilink">1969</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/戴夫·考文斯.md" title="wikilink">戴夫·考文斯</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td></td>
<td><p><a href="../Page/1970年NBA選秀.md" title="wikilink">1970</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吉奥夫·皮特里.md" title="wikilink">吉奥夫·皮特里</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西德尼·威克斯.md" title="wikilink">西德尼·威克斯</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td></td>
<td><p><a href="../Page/1971年NBA選秀.md" title="wikilink">1971</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/鮑伯·麥卡杜.md" title="wikilink">鮑伯·麥卡杜</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/水牛城勇士.md" title="wikilink">水牛城勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1972年NBA選秀.md" title="wikilink">1972</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/厄尔尼·格雷格里奥.md" title="wikilink">厄尔尼·格雷格里奥</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">水牛城勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1973年NBA選秀.md" title="wikilink">1973</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/贾马尔·威尔克斯.md" title="wikilink">贾马尔·威尔克斯</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1974年NBA選秀.md" title="wikilink">1974</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿尔万·亚当斯.md" title="wikilink">阿尔万·亚当斯</a></p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td></td>
<td><p><a href="../Page/1975年NBA選秀.md" title="wikilink">1975</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阿德里安·丹特利.md" title="wikilink">阿德里安·丹特利</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">水牛城勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1976年NBA選秀.md" title="wikilink">1976</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/沃尔特·戴维斯.md" title="wikilink">沃尔特·戴维斯</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td></td>
<td><p><a href="../Page/1977年NBA選秀.md" title="wikilink">1977</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/菲爾·福特.md" title="wikilink">菲爾·福特</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">堪薩斯國王</a></p></td>
<td></td>
<td><p><a href="../Page/1978年NBA選秀.md" title="wikilink">1978</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a>*</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td></td>
<td><p><a href="../Page/1978年NBA選秀.md" title="wikilink">1978</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/达内尔·格里菲斯.md" title="wikilink">达内尔·格里菲斯</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td></td>
<td><p><a href="../Page/1980年NBA選秀.md" title="wikilink">1980</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/巴克·威廉姆斯.md" title="wikilink">巴克·威廉姆斯</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a></p></td>
<td></td>
<td><p><a href="../Page/1981年NBA選秀.md" title="wikilink">1981</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/特里·卡明斯.md" title="wikilink">特里·卡明斯</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">聖地牙哥快艇</a></p></td>
<td></td>
<td><p><a href="../Page/1982年NBA選秀.md" title="wikilink">1982</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/雷夫·辛普森.md" title="wikilink">雷夫·辛普森</a></p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td></td>
<td><p><a href="../Page/1983年NBA選秀.md" title="wikilink">1983</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/麥可·喬丹.md" title="wikilink">麥可·喬丹</a>*</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td></td>
<td><p><a href="../Page/1984年NBA選秀.md" title="wikilink">1984</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/帕特里克·尤因.md" title="wikilink">帕特里克·尤因</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td></td>
<td><p><a href="../Page/1985年NBA選秀.md" title="wikilink">1985</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/扎克·珀森.md" title="wikilink">扎克·珀森</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/印第安納溜馬.md" title="wikilink">印第安納溜馬</a></p></td>
<td></td>
<td><p><a href="../Page/1986年NBA選秀.md" title="wikilink">1986</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/馬克·傑克森.md" title="wikilink">馬克·傑克森</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td></td>
<td><p><a href="../Page/1987年NBA選秀.md" title="wikilink">1987</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/米奇·里奇蒙德.md" title="wikilink">米奇·里奇蒙德</a>*</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1988年NBA選秀.md" title="wikilink">1988</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大衛·羅賓森.md" title="wikilink">大衛·羅賓森</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td></td>
<td><p><a href="../Page/1987年NBA選秀.md" title="wikilink">1987</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/德瑞克·柯曼.md" title="wikilink">德瑞克·柯曼</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a></p></td>
<td></td>
<td><p><a href="../Page/1990年NBA選秀.md" title="wikilink">1990</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/拉里·约翰逊.md" title="wikilink">拉里·约翰逊</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/紐奧良黃蜂.md" title="wikilink">夏洛特黃蜂</a></p></td>
<td></td>
<td><p><a href="../Page/1991年NBA選秀.md" title="wikilink">1991</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a>*</p></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></p></td>
<td></td>
<td><p><a href="../Page/1992年NBA選秀.md" title="wikilink">1992</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/克里斯·韋伯.md" title="wikilink">克里斯·韋伯</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td></td>
<td><p><a href="../Page/1993年NBA選秀.md" title="wikilink">1993</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/格蘭特·希爾_(籃球員).md" title="wikilink">格蘭特·希爾</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td></td>
<td><p><a href="../Page/1994年NBA選秀.md" title="wikilink">1994</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傑森·基德.md" title="wikilink">傑森·基德</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/戴蒙·史陶德邁爾.md" title="wikilink">戴蒙·史陶德邁爾</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/多倫多猛龍.md" title="wikilink">多倫多猛龍</a></p></td>
<td></td>
<td><p><a href="../Page/1995年NBA選秀.md" title="wikilink">1995</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿倫·艾佛森.md" title="wikilink">阿倫·艾佛森</a>*</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td></td>
<td><p><a href="../Page/1996年NBA選秀.md" title="wikilink">1996</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/蒂姆·鄧肯.md" title="wikilink">蒂姆·鄧肯</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td></td>
<td><p><a href="../Page/1997年NBA選秀.md" title="wikilink">1997</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/文斯·卡特.md" title="wikilink">文斯·卡特</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/多倫多猛龍.md" title="wikilink">多倫多猛龍</a></p></td>
<td></td>
<td><p><a href="../Page/1998年NBA選秀.md" title="wikilink">1998</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/埃爾頓·布蘭德.md" title="wikilink">埃爾頓·布蘭德</a>^</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td></td>
<td><p><a href="../Page/1999年NBA選秀.md" title="wikilink">1999</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史蒂夫·弗朗西斯.md" title="wikilink">史蒂夫·弗朗西斯</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/迈克·米勒.md" title="wikilink">迈克·米勒</a>^</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></p></td>
<td></td>
<td><p><a href="../Page/2000年NBA選秀.md" title="wikilink">2000</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/保羅·加索尔.md" title="wikilink">保羅·加索尔</a>^</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/曼非斯灰熊.md" title="wikilink">曼非斯灰熊</a></p></td>
<td></td>
<td><p><a href="../Page/2001年NBA選秀.md" title="wikilink">2001</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阿瑪雷·史陶德迈尔.md" title="wikilink">阿瑪雷·史陶德迈尔</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td></td>
<td><p><a href="../Page/2002年NBA選秀.md" title="wikilink">2002</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a>^</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td></td>
<td><p><a href="../Page/2003年NBA選秀.md" title="wikilink">2003</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/埃梅卡·奥卡福.md" title="wikilink">埃梅卡·奥卡福</a>^</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td></td>
<td><p><a href="../Page/夏洛特山貓.md" title="wikilink">夏洛特山貓</a></p></td>
<td></td>
<td><p><a href="../Page/2004年NBA選秀.md" title="wikilink">2004</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/克里斯·保羅.md" title="wikilink">克里斯·保羅</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/紐奧良黃蜂.md" title="wikilink">紐奧良/奧克拉荷馬黃蜂隊</a></p></td>
<td></td>
<td><p><a href="../Page/2005年NBA選秀.md" title="wikilink">2005</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/布蘭頓·羅伊.md" title="wikilink">布蘭頓·羅伊</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td></td>
<td><p><a href="../Page/2006年NBA選秀.md" title="wikilink">2006</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/凱文·杜蘭特.md" title="wikilink">凱文·杜蘭特</a>^</p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a>/<a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/俄克拉何马城雷霆.md" title="wikilink">俄克拉何马城雷霆</a></p></td>
<td></td>
<td><p><a href="../Page/2007年NBA選秀.md" title="wikilink">2007</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/德瑞克·羅斯.md" title="wikilink">德瑞克·羅斯</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td></td>
<td><p><a href="../Page/2008年NBA選秀.md" title="wikilink">2008</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/泰瑞克·埃文斯.md" title="wikilink">泰瑞克·埃文斯</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td></td>
<td><p><a href="../Page/2009年NBA選秀.md" title="wikilink">2009</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/布雷克·葛里芬.md" title="wikilink">布雷克·葛里芬</a>^</p></td>
<td><p><a href="../Page/大前鋒.md" title="wikilink">大前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
<td></td>
<td><p><a href="../Page/2009年NBA選秀.md" title="wikilink">2009</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/凱里·厄文.md" title="wikilink">凱里·厄文</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td></td>
<td><p><a href="../Page/2011年NBA選秀.md" title="wikilink">2011</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/達米恩·利拉德.md" title="wikilink">達米安·里拉德</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td></td>
<td><p><a href="../Page/2012年NBA選秀.md" title="wikilink">2012</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/迈克尔·卡特-威廉姆斯.md" title="wikilink">麥可·卡特-威廉斯</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td></td>
<td><p><a href="../Page/2013年NBA選秀.md" title="wikilink">2013</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/安德鲁·威金斯.md" title="wikilink">安德鲁·威金斯</a>^</p></td>
<td><p><a href="../Page/小前锋.md" title="wikilink">小前锋</a></p></td>
<td></td>
<td><p><a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a></p></td>
<td></td>
<td><p><a href="../Page/2014年NBA選秀.md" title="wikilink">2014</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡爾-安東尼·唐斯.md" title="wikilink">卡爾-安東尼·唐斯</a>^</p></td>
<td><p><a href="../Page/中锋.md" title="wikilink">中锋</a></p></td>
<td><p>{{#tag:ref|卡爾-安東尼·唐斯出生與長大於美國，但他的母親為多明尼加人（父親則是美國人），因此安東尼有資格代表多明尼加或美國出賽，而他選擇了前者。[1]|group="注"}}</p></td>
<td><p><a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a></p></td>
<td></td>
<td><p><a href="../Page/2015年NBA選秀.md" title="wikilink">2015</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/麥爾坎·波格登.md" title="wikilink">麥爾坎·波格登</a>^</p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td></td>
<td><p><a href="../Page/2016年NBA選秀.md" title="wikilink">2016</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/賓·施蒙斯.md" title="wikilink">賓·施蒙斯</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td></td>
<td><p><a href="../Page/2016年NBA選秀.md" title="wikilink">2016</a></p></td>
</tr>
</tbody>
</table>

## 註解

## 資料來源

[Category:籃球紀錄及統計數據](../Category/籃球紀錄及統計數據.md "wikilink")
[Category:职业列表](../Category/职业列表.md "wikilink")
[Category:NBA獎項](../Category/NBA獎項.md "wikilink")

1.  \[<http://m.startribune.com/sports/wolves/309936841.html?interstitial=true>|
    Karl-Anthony Towns taken No.1 by Wolves; Tyus Jones acquired in
    trade with Cavaliers\], *Star Tribune*, June 26, 2015