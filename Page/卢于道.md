[盧于道.jpg](https://zh.wikipedia.org/wiki/File:盧于道.jpg "fig:盧于道.jpg")
**卢于道**（），[中国解剖学家](../Page/中国.md "wikilink")，中国解剖学的先驱之一，尤开拓了中国近现代[神经解剖学领域](../Page/神经.md "wikilink")。[浙江](../Page/浙江.md "wikilink")[鄞县](../Page/鄞县.md "wikilink")（今[宁波](../Page/宁波.md "wikilink")）人。

## 生平

1926年，毕业于[国立东南大学](../Page/国立东南大学.md "wikilink")[心理学系](../Page/心理学.md "wikilink")，在校曾修读生物學。

毕业后，赴[美国留学](../Page/美国.md "wikilink")，于[芝加哥大学攻读神经解剖学](../Page/芝加哥大学.md "wikilink")，后获解剖学[哲学博士](../Page/哲学博士.md "wikilink")[学位](../Page/学位.md "wikilink")，因成绩优异，得金钥匙奖，并入选美国人类学会终身会员。

1930年，学成归国，于上海医学院任[副教授](../Page/副教授.md "wikilink")。

1931年开始，担任[中央研究院心理学研究所研究员](../Page/中央研究院.md "wikilink")。

1941年，担任[湘雅医学院](../Page/湘雅医学院.md "wikilink")（当时在[贵阳](../Page/贵阳.md "wikilink")）[教授](../Page/教授.md "wikilink")。

1941年-1942年，担任中国科学社生物研究所教授和中国科学社代理总干事。

1942年开始，出任[复旦大学生物系教授](../Page/复旦大学.md "wikilink")。

1942年—1946年，出任复旦大学生物系主任。

1949年—1951年，再度出任复旦大学生物系主任。

1943年，入中国科学工作者协会。

1944年，入民主科学座谈会（该社后来改名作民主科学社）。

1946年，参与并发起[九三学社](../Page/九三学社.md "wikilink")，当选九三学社监事。

1948年10月，赴解放区，在[石家庄得到](../Page/石家庄.md "wikilink")[毛泽东接见](../Page/毛泽东.md "wikilink")。

1949年3月，以中国代表团团员的身份参加世界和平大会（[捷克](../Page/捷克.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")）。

1949年9月，出席了[中国人民政治协商会议](../Page/中国人民政治协商会议.md "wikilink")，参与制定了《中国人民政治协商会议共同纲领》。

1949年10月1日，登[天安门城楼](../Page/天安门.md "wikilink")，参与了[中华人民共和国开国大典](../Page/中华人民共和国.md "wikilink")。

1949年—1952年，任复旦大学理学院院长。

1953年，任复旦大学研究生部主任。

1953年，参加中国第4次赴[朝鲜慰问团](../Page/朝鲜.md "wikilink")（[贺龙为慰问团团长](../Page/贺龙.md "wikilink")）。

参加中国访问[苏联的代表团](../Page/苏联.md "wikilink")。

1985年，因病逝世，享年79岁。

## 任职

### 中国

  - 中国生理学会会员
  - 中国心理学会会员
  - 中国动物学会会员

曾先后担任[九三学社中央常委](../Page/九三学社.md "wikilink")，九三学社中央副主席，九三学社[上海分社主任委员](../Page/上海.md "wikilink")。

是第二、三、五、六届[全国人大代表](../Page/全国人大.md "wikilink")。

是上海市人民委员会委员，曾担任第四届上海市政协副主席，上海市科协主任，上海市科协理事等职务。

### 美国

  - [美国希格鹤赛](../Page/美国.md "wikilink")（Sigma xi）科学荣誉学会会员
  - 美国体质人类学会终身会员

## 著作

  - 《神经解剖学》（是中国第一部人体[神经系统解剖学专著](../Page/神经系统.md "wikilink")）
  - 《自然辩证法》
  - 《西洋哲学史》
  - 《活的身体》
  - 《科学概念》
  - 《脑的进化》

[L卢](../Category/中国医学家.md "wikilink")
[医](../Category/中央大学校友.md "wikilink")
[Category:南京大學校友](../Category/南京大學校友.md "wikilink")
[L卢](../Category/芝加哥大学校友.md "wikilink")
[L卢](../Category/复旦大学学者.md "wikilink")
[L卢](../Category/鄞县人.md "wikilink")
[L卢](../Category/中国人民政治协商会议第一届全体会议代表.md "wikilink")
[L卢](../Category/国务院参事.md "wikilink")
[L卢](../Category/九三学社社员.md "wikilink")
[Category:上海市政协副主席](../Category/上海市政协副主席.md "wikilink")
[YU](../Category/卢姓.md "wikilink")
[Category:解剖學家](../Category/解剖學家.md "wikilink")