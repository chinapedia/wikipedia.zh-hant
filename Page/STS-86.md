****是历史上第86次航天飞机任务，也是[亚特兰蒂斯号航天飞机的第](../Page/亞特蘭提斯號太空梭.md "wikilink")20次太空飞行。

## 任务成员

  - **[吉姆·威瑟比](../Page/吉姆·威瑟比.md "wikilink")**（，曾执行、、、、以及任务），指令长
  - **[迈克尔·布卢姆菲尔德](../Page/迈克尔·布卢姆菲尔德.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[弗拉基米尔·蒂托夫](../Page/弗拉基米尔·蒂托夫.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行、、、[和平号空间站](../Page/和平號太空站.md "wikilink")、、以及任务），任务专家
  - **[斯科特·帕拉金斯基](../Page/斯科特·帕拉金斯基.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[让-卢·克雷蒂安](../Page/让-卢·克雷蒂安.md "wikilink")**（，曾执行[联盟T-6号](../Page/联盟T-6号.md "wikilink")、[联盟TM-7号](../Page/联盟TM-7号.md "wikilink")、[联盟TM-6号以及](../Page/联盟TM-6号.md "wikilink")任务），任务专家
  - **[温迪·劳伦斯](../Page/温迪·劳伦斯.md "wikilink")**（，曾执行、、以及任务），有效载荷专家

### 发射后停留在和平号空间站

  - **[大卫·沃尔夫](../Page/大卫·沃尔夫.md "wikilink")**（，曾执行、以及任务），任务专家

### 从和平号空间站返回

  - **[迈克尔·福奥勒](../Page/迈克尔·福奥勒.md "wikilink")**（，曾执行、、、、以及[远征8号任务](../Page/远征8号.md "wikilink")）

[Category:载人航天](../Category/载人航天.md "wikilink")
[Category:航天飞机任务](../Category/航天飞机任务.md "wikilink")
[Category:1997年](../Category/1997年.md "wikilink")