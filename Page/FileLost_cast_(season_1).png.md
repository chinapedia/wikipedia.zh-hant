## Summary

The starring cast of the [Emmy](../Page/Emmy.md "wikilink")-winning [24
episode](../Page/List_of_Lost_episodes.md "wikilink")
[2004](../Page/2004_in_television.md "wikilink")-[2005](../Page/2005_in_television.md "wikilink")
[first season](../Page/Lost_\(season_1\).md "wikilink") of
[ABC](../Page/American_Broadcasting_Company.md "wikilink")'s
*[Lost](../Page/Lost_\(TV_series\).md "wikilink")*. From left to right:
[Josh Holloway](../Page/Josh_Holloway.md "wikilink") as [James "Sawyer"
Ford](../Page/James_"Sawyer"_Ford.md "wikilink") in 22 episodes, [Terry
O'Quinn](../Page/Terry_O'Quinn.md "wikilink") as [John
Locke](../Page/John_Locke_\(Lost\).md "wikilink") in 24 episodes, [L.
Scott Caldwell](../Page/L._Scott_Caldwell.md "wikilink") as [Rose
Henderson](../Page/Rose_Henderson.md "wikilink") in 4 episodes, [Daniel
Dae Kim](../Page/Daniel_Dae_Kim.md "wikilink") as [Jin
Kwon](../Page/Jin-Soo_Kwon.md "wikilink") in 19 episodes, [Yunjin
Kim](../Page/Yunjin_Kim.md "wikilink") as [Sun
Kwon](../Page/Sun-Hwa_Kwon.md "wikilink") in 22 episodes, [Naveen
Andrews](../Page/Naveen_Andrews.md "wikilink") as [Sayid
Jarrah](../Page/Sayid_Jarrah.md "wikilink") in 24 episodes, [Dominic
Monaghan](../Page/Dominic_Monaghan.md "wikilink") as [Charlie
Pace](../Page/Charlie_Pace.md "wikilink") in 23 episodes, [Malcolm David
Kelley](../Page/Malcolm_David_Kelley.md "wikilink") as [Walt
Lloyd](../Page/Walt_Lloyd.md "wikilink") in 21 episodes, [Harold
Perrineau](../Page/Harold_Perrineau.md "wikilink") as [Michael
Dawson](../Page/Michael_Dawson_\(Lost\).md "wikilink") in 24 episodes,
[Matthew Fox](../Page/Matthew_Fox_\(actor\).md "wikilink") as [Jack
Shephard](../Page/Jack_Shephard.md "wikilink") in 24 episodes,
[Evangeline Lilly](../Page/Evangeline_Lilly.md "wikilink") as [Kate
Austen](../Page/Kate_Austen.md "wikilink") in 24 episodes, [Jorge
Garcia](../Page/Jorge_Garcia.md "wikilink") as [Hurley
Reyes](../Page/Hugo_"Hurley"_Reyes.md "wikilink") in 24 episodes,
[Maggie Grace](../Page/Maggie_Grace.md "wikilink") as [Shannon
Rutherford](../Page/Shannon_Rutherford.md "wikilink") in 23 episodes,
[Emilie de Ravin](../Page/Emilie_de_Ravin.md "wikilink") as [Claire
Littleton](../Page/Claire_Littleton.md "wikilink") in 19 episodes and
[Ian Somerhalder](../Page/Ian_Somerhalder.md "wikilink") as [Boone
Carlyle](../Page/Boone_Carlyle.md "wikilink") in 21 episodes.

## 來源

Photographed in the [summer](../Page/summer.md "wikilink") of
[2004](../Page/2004.md "wikilink") while shooting the
"[Pilot](../Page/Pilot_\(Lost\).md "wikilink")" episode and subsequently
uploaded to [ABC Medianet](../Page/ABC_Studios.md "wikilink"), however
it has since been taken down.

## Fair use rationale

This image is claimed under [U.S.](../Page/U.S..md "wikilink") [fair
use](../Page/fair_use.md "wikilink") laws on the English
[Wikipedia](../Page/Wikipedia.md "wikilink") page of [Lost (season
1)](../Page/Lost_\(season_1\).md "wikilink") because:

  - The image clearly identifies the main cast
  - It is of lesser resolution than the original copy
  - It was released to promote the show
  - It provides critical commentary
  - It is a picture depicting a video
  - The characters are copyrighted
      - Thus no free image can be made

## 許可協議

[Category:迷失圖片](../Category/迷失圖片.md "wikilink")