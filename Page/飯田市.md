**飯田市**（）是位於[日本](../Page/日本.md "wikilink")[中部地方](../Page/中部地方.md "wikilink")[長野縣南部的一個都市](../Page/長野縣.md "wikilink")，在[江戶時代時是](../Page/江戶時代.md "wikilink")[飯田藩所發展出來的一個城下町](../Page/藩.md "wikilink")。

另外，飯田市與[下伊那郡統稱為](../Page/下伊那郡.md "wikilink")「」，飯田市是其主要的中心都市，商圈相當廣泛，包含一部份的[上伊那郡](../Page/上伊那郡.md "wikilink")。

## 地理環境

**飯田市**位於[長野縣最南端的](../Page/長野縣.md "wikilink")[伊那盆地中央](../Page/伊那盆地.md "wikilink")，西北方向是[木曾山脈臨界的](../Page/木曾山脈.md "wikilink")[木曾郡為境](../Page/木曾郡.md "wikilink")，東北部與[上伊那郡飯島町以及](../Page/上伊那郡.md "wikilink")[下伊那郡松川町等三個村莊銜接](../Page/下伊那郡.md "wikilink")。西南方向與縣內五個村莊和[靜岡縣的](../Page/靜岡縣.md "wikilink")[靜岡市](../Page/靜岡市.md "wikilink")、[滨松市鄰接](../Page/滨松市.md "wikilink")。地理環境有一部份屬於盆地地形與一部份的高原地形。市區中央有條由北向南流向的[天龍川](../Page/天龍川.md "wikilink")。

這附近地區自古就是以工商業為中心的繁華地區，[天龍川河畔主要為水田](../Page/天龍川.md "wikilink")，有小部份丘陵地帶也有許多田地和果園，山林間也有部份超過2,000m以上的高山。

山：[聖岳](../Page/聖岳.md "wikilink")、[光岳](../Page/光岳.md "wikilink")、[金森山](../Page/金森山.md "wikilink")、[雞冠山](../Page/雞冠山.md "wikilink")、[熊伏山](../Page/熊伏山.md "wikilink")、神之峰（）、[卯月山](../Page/卯月山.md "wikilink")、[摺古木山](../Page/摺古木山.md "wikilink")、[安平路山](../Page/安平路山.md "wikilink")

河川：[天龍川](../Page/天龍川.md "wikilink")、[松川](../Page/松川.md "wikilink")、[阿知川](../Page/阿知川.md "wikilink")、[遠山川](../Page/遠山川.md "wikilink")、[上村川](../Page/上村川.md "wikilink")

湖沼：[澤城湖](../Page/澤城湖.md "wikilink")

### 鄰近自治體

  - [長野縣](../Page/長野縣.md "wikilink")
      - [上伊那郡](../Page/上伊那郡.md "wikilink")：[飯島町](../Page/飯島町.md "wikilink")
      - [下伊那郡](../Page/下伊那郡.md "wikilink")：[松川町](../Page/松川町.md "wikilink")、[高森町](../Page/高森町_\(長野縣\).md "wikilink")、[阿智村](../Page/阿智村.md "wikilink")、[下條村](../Page/下條村.md "wikilink")、[泰阜村](../Page/泰阜村.md "wikilink")、[天龍村](../Page/天龍村.md "wikilink")、[喬木村](../Page/喬木村.md "wikilink")、[豐丘村](../Page/豐丘村_\(日本\).md "wikilink")、[大鹿村](../Page/大鹿村.md "wikilink")
      - [木曾郡](../Page/木曾郡.md "wikilink")：[南木曾町](../Page/南木曾町.md "wikilink")、[大桑村](../Page/大桑村.md "wikilink")
  - [靜岡縣](../Page/靜岡縣.md "wikilink")
      - [靜岡市](../Page/靜岡市.md "wikilink")：[葵區](../Page/葵區.md "wikilink")
      - [滨松市](../Page/滨松市.md "wikilink")
      - [榛原郡](../Page/榛原郡.md "wikilink")：[川根本町](../Page/川根本町.md "wikilink")

## 友好城市

  - [岡山縣](../Page/岡山縣.md "wikilink")[津山市](../Page/津山市.md "wikilink")
    （1969年）

  - [阿登省](../Page/阿登省.md "wikilink")[沙勒维尔-梅济耶尔](../Page/沙勒维尔-梅济耶尔.md "wikilink")
    （1988年）

## 參考來源

  - [飯田市役所](https://web.archive.org/web/20090831091330/http://www.city.iida.nagano.jp/)

  - [](http://www.iidacci.or.jp/)