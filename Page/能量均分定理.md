[Thermally_Agitated_Molecule.gif](https://zh.wikipedia.org/wiki/File:Thermally_Agitated_Molecule.gif "fig:Thermally_Agitated_Molecule.gif")[肽分子的](../Page/肽.md "wikilink")**熱運動**。這種不停的運動是既隨機又複雜的，而且任一原子的能量起伏都可以很大。然而，使用能量均分定理可以計算出每個原子的平均動能，以及許多振動態的平均勢能。灰色、紅色及藍色的球分別代表[碳](../Page/碳.md "wikilink")、[氧及](../Page/氧.md "wikilink")[氮](../Page/氮.md "wikilink")[原子](../Page/原子.md "wikilink")，而小白球則代表[氫原子](../Page/氫.md "wikilink")。\]\]

在[经典](../Page/經典物理學.md "wikilink")[統計力學中](../Page/統計力學.md "wikilink")，**能量均分定理**（Equipartition
Theorem）是一種聯繫系統[溫度及其平均](../Page/溫度.md "wikilink")[能量的基本公式](../Page/能量.md "wikilink")。能量均分定理又被稱作**能量均分定律**、**能量均分原理**、**能量均分**，或僅稱**均分**。能量均分的初始概念是[熱平衡時能量被等量分到各種形式的运动中](../Page/熱平衡.md "wikilink")；例如，一个分子在[平移運動时的平均](../Page/平移運動.md "wikilink")[動能應等於其做](../Page/動能.md "wikilink")[旋轉運動时的平均動能](../Page/旋轉運動.md "wikilink")。

能量均分定理能够作出定量預測。类似于[均功定理](../Page/均功定理.md "wikilink")，对于一个给定温度的系统，利用均分定理，可以計算出系統的總平均動能及勢能，從而得出系统的[熱容](../Page/熱容.md "wikilink")。均分定理還能分別給出能量各個组分的平均值，如某特定粒子的動能又或是一个[彈簧的勢能](../Page/彈簧.md "wikilink")。例如，它預測出在熱平衡時[理想氣體中的每個粒子平均動能皆為](../Page/理想氣體.md "wikilink")*(3/2)k<sub>B</sub>T*，其中*k<sub>B</sub>*為[玻爾兹曼常數而](../Page/玻爾兹曼常數.md "wikilink")*T*為溫度。更普遍地，無論多複雜也好，它都能被應用於任何处于[熱平衡的](../Page/熱平衡.md "wikilink")[经典系統中](../Page/經典物理學.md "wikilink")。能量均分定理可用於推導[经典](../Page/經典物理學.md "wikilink")[理想氣體定律](../Page/理想氣體定律.md "wikilink")，以及固體[比熱的](../Page/比熱.md "wikilink")[杜隆-珀蒂定律](../Page/杜隆-珀蒂定律.md "wikilink")。它亦能夠應用於預測[恒星的性質](../Page/恒星.md "wikilink")，因为即使考虑[相對論效應的影響](../Page/狭义相对论.md "wikilink")，该定理依然成立。

儘管均分定理在一定条件下能够对物理现象提供非常準確的預測，但是當[量子效應變得显著時](../Page/量子力學.md "wikilink")（如在足够低的温度条件下），基于这一定理的预测就变得不准确。具体来说，当熱能*k<sub>B</sub>T*比特定[自由度下的量子能級間隔要小的時候](../Page/自由度.md "wikilink")，該自由度下的平均能量及熱容比均分定理預測的值要小。当熱能比能級間隔小得多时，这样的一個自由度就說成是被“凍結”了。比方說，在低溫時很多種類的運動都被凍結，因此固體在低溫時的熱容會下降，而不像均分定理原測的一般保持恒定。對十九世紀的物理學家而言，這种熱容下降现象是表明經典物理学不再正確，而需要新的物理学的第一個徵兆。均分定理在預測[電磁波的失敗](../Page/電磁波.md "wikilink")（被稱为“[紫外災變](../Page/紫外災變.md "wikilink")”）[普朗克提出了光本身被量子化而成為](../Page/普朗克.md "wikilink")[光子](../Page/光子.md "wikilink")，而這一革命性的理論對刺激[量子力學及](../Page/量子力學.md "wikilink")[量子場論的發展起到了重要作用](../Page/量子場論.md "wikilink")。

## 基本概念及簡易例子

[MaxwellBoltzmann_zh.gif](https://zh.wikipedia.org/wiki/File:MaxwellBoltzmann_zh.gif "fig:MaxwellBoltzmann_zh.gif")（即25[°C](../Page/攝氏溫標.md "wikilink")）時，四種[惰性氣體分子速度的](../Page/稀有气体.md "wikilink")[機率密度函數](../Page/機率密度函數.md "wikilink")。圖中的四種氣體為[氦](../Page/氦.md "wikilink")（<sup>4</sup>He）、[氖](../Page/氖.md "wikilink")（<sup>20</sup>Ne）、[氬](../Page/氬.md "wikilink")（<sup>40</sup>Ar）及[氙](../Page/氙.md "wikilink")（<sup>132</sup>Xe）；左上角的數字代表它們的[質量數](../Page/質量數.md "wikilink")。這些機率密度函數的[量綱為概率除以速度](../Page/量綱.md "wikilink")；由於概率無量綱，總量綱能以秒／米表示。\]\]

应用波尔兹曼统计方法可以得到：气体处于平衡态时，分子任何一个自由度的平均能量都相等，均为kT/2,这就是能量按自由度均分定理，简称能量均分定理。名字裏面的“均分”是指“攤分或類似於攤分”。能量均分定理的原始概念是，當系統達到熱平衡時，系統的總[動能由各獨立分量所等分](../Page/動能.md "wikilink")。均分定理也為這些能量做出量化的預測。例如它預測[惰性氣體的每一個原子](../Page/稀有气体.md "wikilink")，當於溫度*T*達至熱平衡時，會有平移平均動能*(3/2)K<sub>B</sub>T*，其中K<sub>B</sub>為[波茲曼常數](../Page/波茲曼常數.md "wikilink")。隨此引出的是，在等溫時[氙的重原子速度會比](../Page/氙.md "wikilink")[氦的較輕原子要低](../Page/氦.md "wikilink")。圖二顯示的是四種惰性氣體原子速度的[麦克斯韦-玻尔兹曼分布](../Page/麦克斯韦-玻尔兹曼分布.md "wikilink")。

在這例子中，關鍵點是動能是速度的二次齐函数。均分定理顯示出於熱平衡時，任何在能量中只以二次出現的[自由度](../Page/自由度.md "wikilink")（例如是一粒子的位置或速度的一個分量）有着等於*½K<sub>B</sub>T*的平均能量，並因此向系統的[熱容提供了](../Page/熱容.md "wikilink")½K<sub>B</sub>。這個結果有着許多的應用。

### 平移能量與理想氣體

一粒子質量為*m*，速度為*v*，其（牛頓力學）動能為：

\[H^{\mathrm{kin}} = \tfrac12 m |\mathbf{v}|^2 = \tfrac{1}{2} m\left( v_{x}^{2} + v_{y}^{2} + v_{z}^{2} \right),\]
其中*v<sub>x</sub>*、*v<sub>y</sub>*及*v<sub>z</sub>*是速度*v*的直角坐標的分量。這裏，*H*是[哈密頓量](../Page/哈密頓力學.md "wikilink")，由於哈密頓表述是均分定理一般形式的中心，故下文將以其作為能量的符號。

由於能量是速度各分量的二次方，均分這三分量得每分量在熱平衡時向平均動能提供½*k<sub>B</sub>T*。因此粒子的平均動能為(3/2)*k<sub>B</sub>*T，跟上面惰性氣體的例子一樣。

更普遍地，[理想氣體中的](../Page/理想氣體.md "wikilink")，總能量幾乎全為（平移）動能：假定粒子無內自由度且運動不受其他粒子影響。均分因此預測有*N*個粒子的理想氣體有平均總能量(3/2)
*N k<sub>B</sub>T*。

而氣體的熱容則為(3/2) *N
k<sub>B</sub>*，因此這樣一摩爾氣體的熱容為(3/2)*N<sub>A</sub>k<sub>B</sub>*=(3/2)*R*，其中*N<sub>A</sub>*是[阿伏伽德罗常數](../Page/阿伏伽德罗常數.md "wikilink")，而*R*則是[氣體常數](../Page/氣體常數.md "wikilink")。由於*R*
≈
2 [Cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))，均分預測理想氣體的摩爾比熱容約為3 [Cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))。這個預測已被實驗證實\[1\]。

從平均動能可以求出氣體粒子的[均方根速度](../Page/均方根速度.md "wikilink")*v<sub>rms</sub>*：

\[v_{\mathrm{rms}} = \sqrt{\langle v^{2} \rangle} = \sqrt{\frac{3 k_{B} T}{m}} = \sqrt{\frac{3 R T}{M}},\]
其中*M =
N<sub>A</sub>m*是一摩爾氣體粒子的質量。這個結果對很多應用方面都有用處，例如[逸散用的](../Page/逸散.md "wikilink")[格銳目定律為](../Page/格銳目定律.md "wikilink")[鈾](../Page/鈾.md "wikilink")[濃縮提供了一個方法](../Page/濃縮鈾.md "wikilink")\[2\]。

### 旋轉能量與溶液中的分子滾翻

在另一個相近的例子中，有一粒子其主[轉動慣量](../Page/轉動慣量.md "wikilink")*I<sub>1</sub>*、*I<sub>2</sub>*及*I<sub>3</sub>*。它的旋轉能量是：

\[H^{\mathrm{rot}} = \tfrac{1}{2} ( I_{1} \omega_{1}^{2} + I_{2} \omega_{2}^{2} + I_{3} \omega_{3}^{2} ),\]
其中*ω<sub>1</sub>*、*ω<sub>2</sub>*及*ω<sub>3</sub>*是[角速度的主分量](../Page/角速度.md "wikilink")。使用跟平移同一套的論證，均分意味着每個粒子的平均旋轉能量為*(3/2)K<sub>B</sub>T*。同樣地，均分使計算出分子均方根角速度成為可能\[3\]。

剛性粒子的滾翻——即是分子於溶液中的隨機旋轉——在[核磁共振中觀測到](../Page/核磁共振.md "wikilink")[弛緩中有着重要的角色](../Page/弛緩_\(核磁共振\).md "wikilink")，尤其是在[蛋白質核磁共振及](../Page/蛋白質核磁共振.md "wikilink")[剩餘雙極耦合中](../Page/剩餘雙極耦合.md "wikilink")\[4\]。旋轉滲透可被其他生物物理探測法所觀測到，例如是[螢光異向性](../Page/螢光異向性.md "wikilink")、[流動雙折射及](../Page/流動雙折射.md "wikilink")[介電質光譜學](../Page/介電質光譜學.md "wikilink")\[5\]。

### 勢能與諧波振蕩器

均分定理除可應用於動能外，還能被應用於[勢能計算](../Page/勢能.md "wikilink")：重要例子包括像[彈簧這樣的](../Page/彈簧.md "wikilink")[諧波振蕩器](../Page/諧波振蕩器.md "wikilink")，其二次勢能為

\[H^{\mathrm{pot}} = \tfrac 12 a q^{2},\,\]
其中常數*a*描述彈簧的韌性，而*q*則是由平衡導出的。假若這樣一個系統的質量為*m*，那麼它的動能*H*<sup>kin</sup>為½*mv<sup>2</sup>*=*p*<sup>2</sup>/2*m*，其中*v*及*p*=*mv*代表振蕩器的速度和動量。聯合這些項可得總能量\[6\]：

\[H = H^{\mathrm{kin}} + H^{\mathrm{pot}} = \frac{p^{2}}{2m} + \frac{1}{2} a q^{2}.\]
因此均分定理預測在熱平衡時，振蕩器有平均能量

\[\langle H \rangle =
\langle H^{\mathrm{kin}} \rangle + \langle H^{\mathrm{pot}} \rangle =
\tfrac{1}{2} k_{B} T + \tfrac{1}{2} k_{B} T = k_{B} T,\]
其中角括號\(\left\langle \ldots \right\rangle\)代表括號內的平均量\[7\] 。

這個結果對任何種類的諧波振蕩器都是有效的，例如[鐘擺](../Page/擺.md "wikilink")，一個振動中的粒子或是被動的電子[振蕩器](../Page/振蕩器.md "wikilink")。這樣的振蕩器在很多情況下都會出現；由均分可得，每個這樣的振蕩器都得到一個平均總能量*k<sub>B</sub>T*並因此向系統[熱容提供](../Page/熱容.md "wikilink")*k<sub>B</sub>*。這個可以被用於導出[熱雜音的公式](../Page/熱雜音.md "wikilink")\[8\]
及[固體](../Page/固體.md "wikilink")[摩爾](../Page/摩爾.md "wikilink")[比熱容的](../Page/比熱容.md "wikilink")[杜隆-珀蒂定律公式](../Page/杜隆-珀蒂定律.md "wikilink")。後者在均分定理的歷史中尤其重要。

[Vätskefas.png](https://zh.wikipedia.org/wiki/File:Vätskefas.png "fig:Vätskefas.png")下於平衡位置附近振動。這樣的振動佔了晶體[介電質](../Page/介電質.md "wikilink")[熱容的一大部分](../Page/熱容.md "wikilink")；對金屬而言，電子也對熱容有作用。\]\]

### 固體的比熱容

均分定理的一個重要應用是在於晶狀固體的比熱容。如此固體的每一個原子都能夠在三個獨立的方向下振蕩，因此該固體可以被視為一個擁有各自獨立的*3N*個[簡諧振子的系統](../Page/簡諧振子.md "wikilink")，其中*N*為晶格中的原子數。由於每一個諧振子都有平均能量*k<sub>B</sub>T*，所以固體的平均總能量為*3Nk<sub>B</sub>T*，而比熱容則為*3Nk<sub>B</sub>*。如取*N*為[阿伏伽德罗常數](../Page/阿伏伽德罗常數.md "wikilink")*N<sub>A</sub>*，並使用*R
=
N<sub>A</sub>k<sub>B</sub>*這個聯繫[氣體常數](../Page/氣體常數.md "wikilink")*R*及波茲曼常數*k<sub>B</sub>*的關係式，可得固體[摩爾比熱容的](../Page/摩爾比熱容.md "wikilink")[杜隆-珀蒂定律的一個解釋](../Page/杜隆-珀蒂定律.md "wikilink")，定律指出晶格中每摩爾的原子熱容為
*3R* ≈
6 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))。\[9\]

然而，由於量子效應的關係，這條定律在低溫時並不準確；這也不符合實驗導出的[熱力學第三定律](../Page/熱力學第三定律.md "wikilink")，第三定律指出[摩爾比熱容於絕對零度時必為零](../Page/摩爾比熱容.md "wikilink")\[10\]。[艾爾伯特·愛因斯坦](../Page/艾爾伯特·愛因斯坦.md "wikilink")（1907年）及[彼得·德拜](../Page/彼得·德拜.md "wikilink")（1911年）在基礎上加入了量子效應，發展出一套更準確的理論\[11\]。

固体中每个原子的振动不是独立的，可以用一組組的[耦合振子作為模型](../Page/耦合振子.md "wikilink")。如此振蕩子的模型可以被分解成[简振模](../Page/简振模.md "wikilink")，這跟[鋼琴弦的振動模態及](../Page/鋼琴弦.md "wikilink")[管風琴的共振模態是相近的](../Page/管風琴.md "wikilink")。\[12\]另一方面，均分定理被應用於這種系統時一般都會失敗，因為正常模態間是沒有能量交換的。在一個非常的情況下，模態獨立且它們的能量獨立地守恒。這個顯示出有某種的能量混合，正式叫做*遍歷性*，對於均分定理的成立是十分重要的。\[13\]

### 粒子的澱積

勢能並不一定跟位置成二次關係的。不過，均分定理指出若能量对自由度*x*是s次齐次的（對一固定實數*s*而言），則該部份於熱平衡時的平均能量為*k<sub>B</sub>T/s*。

在[重力下澱積的這個延伸有一個簡單的應用](../Page/重力.md "wikilink")。例如在[啤酒裏有時見到的薄霧能由一團團會](../Page/啤酒.md "wikilink")[散射光的](../Page/散射.md "wikilink")[蛋白質所組成](../Page/蛋白質.md "wikilink")\[14\]。一段時間以後，這些蛋白質團因受重力影響而向下沉澱，使得近底下的部份比頂端的薄霧更多。不過，一個向相合方向作用的過程中，粒子也會向上[滲透回到酒瓶的頂部](../Page/滲透.md "wikilink")。一達到平衡狀態時，就可以使用均分定理來斷定某一[浮力質量](../Page/浮力.md "wikilink")*m<sub>b</sub>*的蛋白質團的平均位置。對一支瓶高無限的啤酒而言，重力[勢能可由下式求出](../Page/勢能.md "wikilink")

\[H^{\mathrm{grav}} = m_{b} g z\,,\]
其中*z*為蛋白質團的高度，而[*g*則為重力](../Page/地球重力.md "wikilink")[加速度](../Page/加速度.md "wikilink")。由於*s=1*的關係，蛋白質團的平均勢能等於*k<sub>B</sub>T*。因此，一個浮力質量為10[MDa](../Page/原子質量單位.md "wikilink")（大體上為病毒的大小）的蛋白質團會於平衡狀態做出一股2cm高的薄霧。這樣一種往平衡的澱積由所描述\[15\]。

## 歷史

  -

      -
        *本文使用非[國際單位制單位的](../Page/國際單位制.md "wikilink")[Cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))作為[摩爾](../Page/摩爾.md "wikilink")[比熱容的單位](../Page/比熱容.md "wikilink")，因為它能用單位數字提供更佳的準確度。
        如欲將該單位轉換成國際單位制的對應單位[J](../Page/焦耳.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))，將原數值乘上4.2[J](../Page/焦耳.md "wikilink")/[Cal可得約值](../Page/卡路里.md "wikilink")。*

動能均分這個概念最早是在1843年，或更準確地說應是1845年，由[約翰·詹姆斯·瓦塔斯頓提出的](../Page/約翰·詹姆斯·瓦塔斯頓.md "wikilink")\[16\]。於1859年，[詹姆斯·克拉克·麥克斯韋主張氣體的動熱能由線性及旋轉能量所等量攤分](../Page/詹姆斯·克拉克·麥克斯韋.md "wikilink")\[17\]
於1876年，[路德維希·波茲曼因表明了平均能量是被一系統中各獨立分量所等分](../Page/路德維希·波茲曼.md "wikilink")，而將原理進一步擴展\[18\]\[19\]。波茲曼亦應用了均分定理去為固體[比熱容的](../Page/比熱容.md "wikilink")[杜隆-珀蒂定律提出了一個理論解釋](../Page/杜隆-珀蒂定律.md "wikilink")。

[DiatomicSpecHeat1.png](https://zh.wikipedia.org/wiki/File:DiatomicSpecHeat1.png "fig:DiatomicSpecHeat1.png")[比熱容對溫度的理想化曲線圖像](../Page/比熱容.md "wikilink")。高溫時比熱容跟均分定理預測的(7/2)*R*一致（其中*R*為[理想氣體常數](../Page/理想氣體常數.md "wikilink")），但當低溫時會降至(5/2)*R*及後來的(3/2)*R*，這是由於振動及旋轉態被“凍結”了的緣故。均分定理的這次失敗引出一個只能被[量子力學解釋的](../Page/量子力學.md "wikilink")[悖論](../Page/悖論.md "wikilink")。對大部分分子而言，平移溫度T<sub>rot</sub>比室溫要低得多，而T<sub>vib</sub>則要比這要大十倍以上。[一氧化碳](../Page/一氧化碳.md "wikilink")，CO，是一個典型的例子，其T<sub>rot</sub>
≈ 2.8 [K而T](../Page/熱力學溫標.md "wikilink")<sub>vib</sub> ≈
3103 [K](../Page/熱力學溫標.md "wikilink")。對非常大的分子或不太受束縛的原子T<sub>vib</sub>能接近室溫（約300 [K](../Page/熱力學溫標.md "wikilink")）；例如，[碘氣I](../Page/碘.md "wikilink")<sub>2</sub>的T<sub>vib</sub>
≈ 308 [K](../Page/熱力學溫標.md "wikilink")\[20\]。\]\]

能量均分定理的歷史與[摩爾](../Page/摩爾.md "wikilink")[比熱容的歷史是密不可分的](../Page/比熱容.md "wikilink")，兩者都是在十九世紀時被研究的。於1819年，法國物理學家[皮埃爾·路易斯·杜隆與](../Page/皮埃爾·路易斯·杜隆.md "wikilink")[阿勒克西斯·泰雷塞·珀蒂發現了所有室溫下的固體比熱容幾乎都是相等的](../Page/阿勒克西斯·泰雷塞·珀蒂.md "wikilink")，約為6 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))\[21\]。他們的定律曾在多年間被用作量度[原子質量的一種技巧](../Page/原子質量.md "wikilink")\[22\]，然而，後來[詹姆斯·杜瓦及](../Page/詹姆斯·杜瓦.md "wikilink")的研究表明[杜隆-珀蒂定律只於高溫時成立](../Page/杜隆-珀蒂定律.md "wikilink")\[23\]；在低溫時或像[金剛石這種異常地硬的固體](../Page/金剛石.md "wikilink")，比熱還要再低一點\[24\]。

氣體比熱的實驗觀測也引起了對均分定理是否有效的質疑。定理預測簡單單原子氣體的摩爾比熱容應約為3 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))，而雙原子氣體則約為7 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))。實驗驗證了預測的前者\[25\]，但卻發現雙原子氣體的典型摩爾比熱容約為5 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))\[26\]，並於低溫時下跌到約3 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))\[27\]。[詹姆斯·克拉克·麥克斯韋於](../Page/詹姆斯·克拉克·麥克斯韋.md "wikilink")1875年指出實驗與均分定理的不合比這些數字暗示的要壞得多\[28\]；由於原子有內部部份，熱能應該走向這些內部部分的運動，使得單原子及雙原子的比熱預測值比3 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))7 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))要高得多。

第三個有關的不符之處是金屬的比熱\[29\]。根據古典[德魯德模型](../Page/德魯德模型.md "wikilink")，金屬電子的舉止跟幾乎理想的氣體一樣，因此它們應該向(3/2)*N<sub>e</sub>k<sub>B</sub>*的熱容，其中*N<sub>e</sub>*為電子的數量。不過實驗指出電子對熱容的供給並不多：很多的金屬的摩爾比熱容與絕緣體幾乎一樣\[30\]。

數個說明均分失敗原因的解釋被提出了。[波茲曼辯護他的均分定理推導是正確的](../Page/路德維希·波茲曼.md "wikilink")，但就提出氣體可能因為與[以太相互作用而不處於熱平衡狀態](../Page/以太.md "wikilink")\[31\]。由於與實驗不符，[開爾文勳爵提出均分定理的推導一定是不定確的](../Page/威廉·湯姆森，第一代開爾文男爵.md "wikilink")，但卻說不出甚麼不正確\[32\]。反而[瑞利勳爵提出一個更徹底的看法](../Page/瑞利.md "wikilink")，說均分定理及實驗時系統處於熱平衡的假設這兩者*都*正確；為使兩者相符，他指出需要一個能為均分定理提供“從破壞性的簡單中逃走的去路”的新原理\[33\]。[艾爾伯特·愛因斯坦就提供了這條去路](../Page/艾爾伯特·愛因斯坦.md "wikilink")，於1907年他表明了這些比熱的異數都是由量子效應引起的，尤其是固體彈性模態能量的量子化\[34\]。愛因斯坦用了均分定理的失敗作為需要一個新物質量子理論的論據\[35\]。[瓦爾特·能斯特於](../Page/瓦爾特·能斯特.md "wikilink")1910年在低溫的比熱量度支持了\[36\]愛因斯坦的理論，並引起了物理學家們對[量子理論的廣泛承認](../Page/量子理論.md "wikilink")\[37\]。

## 能量均分定理的通用公式化

均分定理最通用的形式\[38\]\[39\]\[40\]
明確表示在適當的假設下（下文會討論），對一個有[哈密頓能量函數](../Page/哈密頓力學.md "wikilink")*H*及自由度*x<sub>n</sub>*的物理系統而言，以下均分公式於熱平衡時對任何值的指數*m*及*n*都有效：

<div style="border:2px solid black; padding:3px; margin-left: 0em; width: 250px;
text-align:left">

\[\!
\Bigl\langle x_{m} \frac{\partial H}{\partial x_{n}} \Bigr\rangle = \delta_{mn} k_{B} T.\]

</div>

這裏*δ<sub>mn</sub>*為[克羅內克爾δ](../Page/克羅內克爾δ.md "wikilink")，當*m=n*時為一而其他情況為零。平均用的括號\(\left\langle \ldots \right\rangle\)可以代表一個單系統長時間的平均，或，更多地是，相空間的[系綜平均](../Page/系綜平均.md "wikilink")。定理內含的遍歷性假設意味着這兩個平均相符，而且都被用於複雜物理系統的內能估算。

通用均分定理在系統總能量恒定時於[微正則系綜有效](../Page/微正則系綜.md "wikilink")\[41\]，在跟能交換能量的[熱庫耦合時於](../Page/熱庫.md "wikilink")[正則系綜亦都有效](../Page/正則系綜.md "wikilink")\[42\]\[43\]。通式的推導在[下文](../Page/#推導.md "wikilink")。

通式與以下兩式等價

1.  對所有*n*，\(\Bigl\langle x_{n} \frac{\partial H}{\partial x_{n}} \Bigr\rangle = k_{B} T\)
2.  對所有*m*≠*n*，\(\Bigl\langle x_{m} \frac{\partial H}{\partial x_{n}} \Bigr\rangle = 0\)

若一自由度*x<sub>n</sub>*只作為一個二次項*a<sub>n</sub>x<sub>n</sub><sup>2</sup>*於哈密頓函數*H*中出現，則可從第一式引出

\[k_{B} T = \Bigl\langle x_{n} \frac{\partial H}{\partial x_{n}}\Bigr\rangle = 2\langle a_n x_n^2 \rangle,\]
即這自由度向平均能量\(\langle H\rangle\)所提供的兩倍。因此二次能量系統的均分定理結果很容易就出來了。用一個相近的論點，將2換成*s*，以*a<sub>n</sub>x<sub>n</sub><sup>s</sup>*的形式應用於能量中。

自由度*x<sub>n</sub>*是系統[相空間的坐標](../Page/相空間.md "wikilink")，因此一般被細分成[廣義位置坐標](../Page/正則坐標.md "wikilink")*q<sub>k</sub>*及[廣義動量坐標](../Page/正則坐標.md "wikilink")*p<sub>k</sub>*，其中*p<sub>k</sub>*為*q<sub>k</sub>*的[共軛動量](../Page/正則坐標.md "wikilink")。此時，1式對所有*k*值而言

\[\Bigl\langle p_{k} \frac{\partial H}{\partial p_{k}} \Bigr\rangle =  \Bigl\langle q_{k} \frac{\partial H}{\partial q_{k}} \Bigr\rangle = k_{B} T.\]
使用[哈密頓力學的方程](../Page/哈密頓力學.md "wikilink")\[44\]，這些式子可被寫成

\[\Bigl\langle p_{k} \frac{dq_{k}}{dt} \Bigr\rangle = -\Bigl\langle q_{k} \frac{dp_{k}}{dt} \Bigr\rangle = k_{B} T.\]
另外式2明確指出平均為

\[\Bigl\langle q_{j} \frac{\partial H}{\partial q_{k}} \Bigr\rangle, \quad
\Bigl\langle q_{j} \frac{\partial H}{\partial p_{k}} \Bigr\rangle, \quad
\Bigl\langle p_{j} \frac{\partial H}{\partial p_{k}} \Bigr\rangle, \quad
\Bigl\langle p_{j} \frac{\partial H}{\partial q_{k}} \Bigr\rangle, \quad
\Bigl\langle q_{k} \frac{\partial H}{\partial p_{k}} \Bigr\rangle,\]   及   \(\Bigl\langle p_{k} \frac{\partial H}{\partial q_{k}} \Bigr\rangle\)
若*j≠k*則皆為零。

### 與均功定理的關係

通用均分定理是[均功定理的一個延伸](../Page/均功定理.md "wikilink")。均功定理於1870年被提出\[45\]，它明確指出

\[\Bigl\langle \sum_{k} q_{k} \frac{\partial H}{\partial q_{k}} \Bigr\rangle =
\Bigl\langle \sum_{k} p_{k} \frac{\partial H}{\partial p_{k}} \Bigr\rangle =
\Bigl\langle \sum_{k} p_{k} \frac{dq_{k}}{dt} \Bigr\rangle = -\Bigl\langle \sum_{k} q_{k} \frac{dp_{k}}{dt} \Bigr\rangle,\]
其中*t*代表時間。兩個關鍵性的分別在於均功定理聯繫的是平均的*總和*而不是*個別*的平均，而且跟[溫度](../Page/溫度.md "wikilink")*T*沒有關係。另一個分別是均功定理的傳統推導使用時間的平均，而均分定理則用[相空間的平均](../Page/相空間.md "wikilink")。

## 應用

### 理想氣體定律

[理想氣體給能量均分定理提供一個重要應用](../Page/理想氣體.md "wikilink")。也為每粒子的平均動能提供了公式：

\[\begin{align}
\langle H^{\mathrm{kin}} \rangle &= \frac{1}{2m} \langle p_{x}^{2} + p_{y}^{2} + p_{z}^{2} \rangle\\
&=  \frac{1}{2} \biggl(
\Bigl\langle p_{x} \frac{\partial H^{\mathrm{kin}}}{\partial p_{x}} \Bigr\rangle +
\Bigl\langle p_{y} \frac{\partial H^{\mathrm{kin}}}{\partial p_{y}} \Bigr\rangle +
\Bigl\langle p_{z} \frac{\partial H^{\mathrm{kin}}}{\partial p_{z}} \Bigr\rangle \biggr) =
\frac{3}{2} k_{B} T
\end{align}\]
能量均分定理可被用於從古典力學中導出[理想氣體定律](../Page/理想氣體定律.md "wikilink")\[46\]。若**q**=(*q<sub>x</sub>*,*q<sub>y</sub>*,*q<sub>z</sub>*)且**p**=(*p<sub>x</sub>*,*p<sub>y</sub>*,*p<sub>z</sub>*)代表氣體中一個粒子的位置向量及動量向量，而**F**為作用在該粒子上的淨力，則

\[\begin{align}
\langle \mathbf{q} \cdot \mathbf{F} \rangle &= \Bigl\langle q_{x} \frac{dp_{x}}{dt} \Bigr\rangle +
\Bigl\langle q_{y} \frac{dp_{y}}{dt} \Bigr\rangle +
\Bigl\langle q_{z} \frac{dp_{z}}{dt} \Bigr\rangle\\
&=-\Bigl\langle q_{x} \frac{\partial H}{\partial q_x} \Bigr\rangle -
\Bigl\langle q_{y} \frac{\partial H}{\partial q_y} \Bigr\rangle -
\Bigl\langle q_{z} \frac{\partial H}{\partial q_z} \Bigr\rangle = -3k_{B} T,
\end{align}\]
其中第一條等式為[牛頓第二定律](../Page/牛頓第二定律.md "wikilink")，第二行用了[哈密頓力學及均分公式](../Page/哈密頓力學.md "wikilink")。將系統中的*N*個粒子的所有加起來得

\[3Nk_{B} T = - \biggl\langle \sum_{k=1}^{N} \mathbf{q}_{k} \cdot \mathbf{F}_{k} \biggr\rangle\]
[Translational_motion.gif](https://zh.wikipedia.org/wiki/File:Translational_motion.gif "fig:Translational_motion.gif")，一條聯繫氣體[壓力](../Page/壓力.md "wikilink")、[體積及](../Page/體積.md "wikilink")[溫度的方程式](../Page/溫度.md "wikilink")。（圖中五個分子被填成紅色以便追踪他們的行徑，而其他顏色則無關重要。）\]\]

由[牛頓第三定律及理想氣體假設得](../Page/牛頓第三定律.md "wikilink")，系統的淨力是由它們的容器上的牆所施行的，而這個力可由氣體的氣壓*P*所求得。因此

\[-\biggl\langle\sum_{k=1}^{N} \mathbf{q}_{k} \cdot \mathbf{F}_{k}\biggr\rangle = P \oint_{\mathrm{surface}} \mathbf{q} \cdot \mathbf{dS},\]
其中*'dS*為沿着容器牆的無限小面積元件。由於位置向量*q*[散度為](../Page/散度.md "wikilink")

\[\boldsymbol\nabla \cdot \mathbf{q} =
\frac{\partial q_{x}}{\partial q_{x}} +
\frac{\partial q_{y}}{\partial q_{y}} +
\frac{\partial q_{z}}{\partial q_{z}} = 3,\]

而[散度定理表明](../Page/高斯散度定理.md "wikilink")

\[P \oint_{\mathrm{surface}} \mathbf{q} \cdot \mathbf{dS} = P \int_{\mathrm{volume}} \left( \boldsymbol\nabla \cdot \mathbf{q} \right) dV = 3PV,\]
其中*dV*為容器內無限小的體積，而*V*則為容器的總容量。

將這些等式結合得

\[3Nk_{B} T = -\biggl\langle \sum_{k=1}^{N} \mathbf{q}_{k} \cdot \mathbf{F}_{k} \biggr\rangle = 3PV,\]
即馬上導出*N*個粒子[理想氣體定律](../Page/理想氣體定律.md "wikilink")

\[PV = Nk_{B} T = nRT,\,\]
其中*n=N/N<sub>A</sub>*為氣體的[摩爾數](../Page/摩爾.md "wikilink")，而*R=N<sub>A</sub>k<sub>B</sub>*則為[氣體常數](../Page/氣體常數.md "wikilink")。

### 雙原子氣體

一雙原子氣體可用被一[韌度為](../Page/胡克定律.md "wikilink")*a*彈簧連接的兩質量*m<sub>1</sub>*及*m<sub>2</sub>*作為模型，稱為“剛性轉子-諧波振蕩器近似法”\[47\]。此系統的古典能量為

\[H =
\frac{\left| \mathbf{p}_{1} \right|^{2}}{2m_{1}} +
\frac{\left| \mathbf{p}_{2} \right|^{2}}{2m_{2}} +
\frac{1}{2} a q^{2},\]
其中**p**<sub>1</sub>與**p**<sub>2</sub>為兩原子的動量，而*q*則為從平衡值得出的原子間距離的偏差。由於能量的每一自由度都為二次，並故此應向總平均能量提供½*k<sub>B</sub>T*，向熱容提供½*k<sub>B</sub>*。因此一有*N*雙原子分子的氣體的熱容被預測為*7N*·½*k<sub>B</sub>*：**p***<sub>1</sub>*及**p***<sub>2</sub>*各提供三個自由度，*q*的延伸提供第七個。由此得一摩爾無其他自由度的雙原子氣體，其比熱容應為(7/2)*N<sub>A</sub>k<sub>B</sub>*=(7/2)*R*，而因此摩爾比熱容應大概為7 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))。但是雙原子氣體摩熱比熱容的典型實驗結果約為5 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))\[48\]，並於非常低溫時跌至3 [cal](../Page/卡路里.md "wikilink")/([mol](../Page/摩爾.md "wikilink")·[K](../Page/開爾文.md "wikilink"))。\[49\]
這個均分預測與實驗摩爾熱容值的不符，不能被更複雜的分子模型所解釋，因為增加自由度只會*增加*預測比熱值，而不會減少\[50\]。這個不符是需要物質[量子理論的一個關鍵證據](../Page/量子理論.md "wikilink")。

[Chandra-crab.jpg](https://zh.wikipedia.org/wiki/File:Chandra-crab.jpg "fig:Chandra-crab.jpg")的X射線可見光結合圖像。在星雲的中心有一顆高速旋轉的[中子星](../Page/中子星.md "wikilink")，其質量為[太陽的一倍半但直徑只有](../Page/太陽.md "wikilink")25公里（大概跟[馬德里一樣大](../Page/馬德里.md "wikilink")）。均分定理對預測如此中子星的性質很有作用。\]\]

### 極度相對性理想氣體

均分定理被用於從[牛頓力學中導出古典](../Page/牛頓力學.md "wikilink")[理想氣體定律](../Page/理想氣體定律.md "wikilink")。但是[相對性效應在某些系統中變得顯著](../Page/狹義相對論.md "wikilink")，例如是[白矮星及](../Page/白矮星.md "wikilink")[中子星](../Page/中子星.md "wikilink")\[51\]，而此時理想氣體定律一定要被稍作修改。均分定理為導出極度相對性[理想氣體提供了方便的途徑](../Page/理想氣體.md "wikilink")\[52\]。在這樣的個案中，一個單粒子的動能可由以下公式求得：

\[H^{\mathrm{kin}} \approx cp = c \sqrt{p_{x}^{2} + p_{y}^{2} + p_{z}^{2}}.\]
以動量分量*p<sub>x</sub>*取*H*的導數得公式：

\[p_{x} \frac{\partial H^{\mathrm{kin}}}{\partial p_{x}}  = c \frac{p_{x}^{2}}{\sqrt{p_{x}^{2} + p_{y}^{2} + p_{z}^{2}}}\]
之後以同樣的方式處理分量*p<sub>y</sub>*及*p<sub>z</sub>*。將三個分量全加起來得：

\[\begin{align}
\langle H^{\mathrm{kin}} \rangle
&= \biggl\langle c \frac{p_{x}^{2} + p_{y}^{2} + p_{z}^{2}}{\sqrt{p_{x}^{2} + p_{y}^{2} + p_{z}^{2}}}  \biggr\rangle\\
&= \Bigl\langle p_{x} \frac{\partial H^{\mathrm{kin}}}{\partial p_{x}} \Bigr\rangle +
\Bigl\langle p_{y} \frac{\partial H^{\mathrm{kin}}}{\partial p_{y}} \Bigr\rangle +
\Bigl\langle p_{z} \frac{\partial H^{\mathrm{kin}}}{\partial p_{z}} \Bigr\rangle\\
&= 3 k_{B} T
\end{align}\]
其中最後一條等式則是均分定理公式的結果。因此，一極度相對性理想氣體的平均總能量要比非相對性的大兩倍：當有*N*個粒子時，總平均能量為*3
N k<sub>B</sub>T*。

### 非理想氣體

在一理想氣體中，假設粒子只通過碰撞來相互作用。均分定理可以用於推導“非理想氣體”的能量和壓力，這氣體中的粒子也通過守恒力與其他粒子相互作用，守恒力的勢*U*(*r*)只受粒子間距離*r*影響\[53\]。要描述這個狀況，首先將注意力集中於一個粒子，之後把其餘的氣體當[球狀對稱分佈看待](../Page/球狀對稱.md "wikilink")。然後按慣例引入[徑向分佈函數](../Page/徑向分佈函數.md "wikilink")*g(r)*，使得從某固定粒子於距離*r*找到另一粒子的[概率密度等於](../Page/機率密度函數.md "wikilink")4π*r<sup>2</sup>ρ
g(r)*，其中*ρ=N/V*為氣體的平均[密度](../Page/密度.md "wikilink")\[54\]。可得某固定粒子跟其餘氣體的互相作用平均勢能為

\[\langle h^{\mathrm{pot}} \rangle = \int_{0}^{\infty} 4\pi r^{2} \rho U(r) g(r)\, dr\]。

氣體的總平均勢能因此為\(\langle H^{pot} \rangle = \tfrac12 N \langle h^{\mathrm{pot}} \rangle\)，其中*N*是氣體中的粒子數；在加起所有粒子數目的過程中會把每個粒子算兩次，所以需要用到½這個因子。

把動能及勢能加起來，然後使用均分定理，可得*能量方程*

\[H =
\langle H^{\mathrm{kin}} \rangle + \langle H^{\mathrm{pot}} \rangle =
\frac{3}{2} Nk_{B}T + 2\pi N \rho \int_{0}^{\infty} r^{2} U(r) g(r) \, dr\]。

用一套相近的論證下\[55\]，可用於導出*壓力方程*

\[3Nk_{B}T = 3PV + 2\pi N \rho \int_{0}^{\infty} r^{3} U'(r) g(r)\, dr\]。

### 非諧振器

一非諧振器（跟簡單諧波振蕩器相對）的勢能並不與延伸長度*q*（量度平衡點偏差的[廣義位置](../Page/正則坐標.md "wikilink")）成二次關係。這樣的振蕩器為均分定理提供了一個可作補充的觀點\[56\]\[57\]。簡單例子中用的是勢能函數，其形式為

\[H^{\mathrm{pot}} = C q^{s}\] ，

其中*C*和*S*為任意[實數](../Page/實數.md "wikilink")。這些個案中，均分定律預測

\[k_{B} T = \Bigl\langle q \frac{\partial H^{\mathrm{pot}}}{\partial q} \Bigr\rangle =
\langle q \cdot s C q^{s-1} \rangle = \langle s C q^{s} \rangle = s \langle H^{\mathrm{pot}} \rangle\]
。

因此，平均勢能等於*k<sub>B</sub>T/s*，而不像二次諧波振蕩器那樣等於*k<sub>B</sub>T/2*（此時*s*=2）。

更廣義地，一次元系統典型能量函數的有以延伸長度*q*表示的[泰勒級數](../Page/泰勒級數.md "wikilink")：

\[H^{\mathrm{pot}} = \sum_{n=2}^{\infty} C_{n} q^{n}\]

其中*n*須為非負[整數式子方能成立](../Page/整數.md "wikilink")。沒有*n*=1的項是因為在平衡點上並無淨力作用，所以能量第一導數為零。毋需包括*n*=0的項是因為在約定俗成下平衡位置的能量可訂為零。此時，均分定律預測\[58\]：

\[k_{B} T = \Bigl\langle q \frac{\partial H^{\mathrm{pot}}}{\partial q} \Bigr\rangle =
\sum_{n=2}^{\infty} \langle q \cdot n C_{n} q^{n-1} \rangle =
\sum_{n=2}^{\infty} n C_{n} \langle q^{n} \rangle\] 。

相比以上引述的例子，均分定理公式

\[\langle H^{\mathrm{pot}} \rangle = \frac{1}{2} k_{B} T -
\sum_{n=3}^{\infty} \left( \frac{n - 2}{2} \right) C_{n} \langle q^{n} \rangle\]
並*不*能把平均勢能以已知常數來表示。

### 布朗運動

[Wiener_process_3d.png](https://zh.wikipedia.org/wiki/File:Wiener_process_3d.png "fig:Wiener_process_3d.png")

均分定理可用於從[朗之萬方程中導出一粒子的](../Page/朗之萬方程.md "wikilink")[布朗運動](../Page/布朗運動.md "wikilink")\[59\]。根據該方程，一質量為*m*，速度為*v*的粒子的運動是由[牛頓第二定律支配的](../Page/牛頓第二定律.md "wikilink")

\[\frac{d\mathbf{v}}{dt} = \frac{1}{m} \mathbf{F} = -\frac{\mathbf{v}}{\tau} + \frac{1}{m} \mathbf{F}^{\mathrm{rnd}},\]
其中**F**<sup>rnd</sup>為一代表着粒子及其周圍分子的隨機碰撞的隨機力，而時間常數τ反映阻礙粒子在溶液中運動的[阻力](../Page/阻力.md "wikilink")。阻力很多時候被寫成**F**<sup>drag</sup>
=-γ**v**，因此時間常數τ等於*m*/γ。

取方程與位置向量*r*的點積，平均後，得布朗運動用的方程

\[\Bigl\langle \mathbf{r} \cdot \frac{d\mathbf{v}}{dt} \Bigr\rangle +
\frac{1}{\tau} \langle \mathbf{r} \cdot \mathbf{v} \rangle = 0\]
（由於**F**<sup>rnd</sup>跟位置向量*r*不相關）。使用數學恒等式

\[\frac{d}{dt} \left( \mathbf{r} \cdot \mathbf{r} \right) =
\frac{d}{dt} \left( r^{2} \right) = 2 \left( \mathbf{r} \cdot \mathbf{v} \right)\]
及

\[\frac{d}{dt} \left( \mathbf{r} \cdot \mathbf{v} \right) = v^{2} + \mathbf{r} \cdot \frac{d\mathbf{v}}{dt},\]
布朗運動的基本方程可被轉變成

\[\frac{d^{2}}{dt^{2}} \langle r^{2} \rangle + \frac{1}{\tau} \frac{d}{dt} \langle r^{2} \rangle =
2 \langle v^{2} \rangle = \frac{6}{m} k_{B} T,\] 其中從最後的等式可由均分定理得平移動能：

\[\langle H^{\mathrm{kin}} \rangle = \Bigl\langle \frac{p^{2}}{2m} \Bigr\rangle = \langle \tfrac{1}{2} m v^{2} \rangle = \tfrac{3}{2} k_{B} T\]。
以上\(\langle r^2\rangle\)的[微分方程](../Page/微分方程.md "wikilink")（有適當的初始條件）可被整解得

\[\langle r^{2} \rangle = \frac{6k_{B} T \tau^{2}}{m} \left( e^{-t/\tau} - 1 + \frac{t}{\tau} \right)\]。
在短時標上，*t*
\<\<*τ*，粒子的行動就像自由粒子一樣：由[指數函數的](../Page/指數函數.md "wikilink")[泰勒級數得](../Page/泰勒級數.md "wikilink")，距離的平方大約以*二次*增長：

\[\langle r^{2} \rangle \approx \frac{3k_{B} T}{m} t^{2} = \langle v^{2} \rangle t^{2}\]。
但是在長時標上，*t* \>\>*τ*，對數及常數項可被忽略，距離平方只以*一次*增長：

\[\langle r^{2} \rangle \approx \frac{6k_{B} T\tau}{m} t = 6\gamma k_{B} T t\]。
這描述了粒子隨着時間的[滲透](../Page/滲透.md "wikilink")。一條剛性子旋轉滲透用的類似方程可用跟這個近似的方法導出。

### 恒星物理學

均分定理及相關的[均功定理在很早以前就已被用於](../Page/均功定理.md "wikilink")[天體物理學](../Page/天體物理學.md "wikilink")\[60\]。例如，均功定理可被用於由[白矮星質量去估計恒星溫度或其](../Page/白矮星.md "wikilink")[錢德拉塞卡極限](../Page/錢德拉塞卡極限.md "wikilink")\[61\]\[62\]。

一顆恒星的平均溫度可由均分定理估計\[63\]。由於大部份恒星都是球狀對稱，總[重力勢能可由積分法估算](../Page/萬有引力.md "wikilink")

\[H^{\mathrm{grav}}_{\mathrm{tot}} = -\int_{0}^{R} \frac{4\pi r^{2} G}{r} M(r)\, \rho(r)\, dr,\]
其中*M(r)*為半徑*r*以內的質量而*ρ(r)*則是於半徑*r*時的恒星密度；*G*代表[萬有引力常數](../Page/萬有引力常數.md "wikilink")，*R*為恒星總半徑。假定整個恒星內的密度一致，該積分可得公式

\[H^{\mathrm{grav}}_{\mathrm{tot}} = - \frac{3G M^{2}}{5R},\]
其中*M*為恒星總質量。因此一個單粒子平均勢能為

\[H^{\mathrm{grav}}_{\mathrm{tot}} = - \frac{3G M^{2}}{5R},\]
其中*N*為恒星內的粒子數目。由於大部分[恒星都是由](../Page/恒星.md "wikilink")[離子化的](../Page/離子.md "wikilink")[氫所組成的](../Page/氫.md "wikilink")，*N*大概等於*(M/m<sub>p</sub>)*，其中*m<sub>p</sub>*為一個質子的質量。應用均分定理可得一個恒星溫度的約值

\[\Bigl\langle r \frac{\partial H^{\mathrm{grav}}}{\partial r} \Bigr\rangle = \langle -H^{\mathrm{grav}} \rangle =
k_{B} T = \frac{3G M^{2}}{5RN}.\]
將太陽的質量及半徑代入得太陽溫度的大約值*T*等於一千四百萬[開爾文](../Page/開爾文.md "wikilink")，跟其核心溫度一千五百萬開爾文非常接近。但是，太陽被這個模型假設的要複雜得多——它的溫度及質量都隨半徑而變動甚大——而且如此極佳的切合度（≈7%[相對誤差](../Page/逼近誤差.md "wikilink")）在一定情況下是偶然的\[64\]。

### 恒星的形成

同一組方程可被用於斷定巨型[分子雲中的](../Page/分子雲.md "wikilink")[恒星形成狀態](../Page/恒星形成.md "wikilink")\[65\]。這樣一個雲的本地密度起伏可以引致一個很壞的情況，當中分子雲會在自己的重力下向內倒塌。這樣一個倒塌在當均分定理——或同樣地均功定理——不再有效的時候發生，亦即當重力勢能超越動能兩倍的時候

\[\frac{3G M^{2}}{5R} > 3 N k_{B} T\]

設雲的密度固定為ρ

\[M = \frac{4}{3} \pi R^{3} \rho\]

得恒星收縮的最小質量，金斯質量*M<sub>J</sub>*

\[M_{J}^{2} = \left( \frac{5k_{B}T}{G m_{p}} \right)^{3} \left( \frac{3}{4\pi \rho} \right)\]

代入這樣的雲的典型觀測值（*T*=150 [K](../Page/開爾文.md "wikilink")，ρ=2×10<sup>−16</sup>g/cm<sup>3</sup>）可得一為17個太陽質量的最小質量估值，跟恒星形成的觀測一致。這個效應亦被稱為[金斯不穩定性](../Page/金斯不穩定性.md "wikilink")，以在1902年發表這理論的英國物理學家[詹姆斯·霍普伍德·金斯命名](../Page/詹姆斯·霍普伍德·金斯.md "wikilink")\[66\]。

## 推導

### 動能與麥克斯韋-波茲曼分布

均分定理的原公式化表述明確指出，在任何處熱平衡狀態的物理系統中，每一個粒子有着完全一樣的平均[動能](../Page/動能.md "wikilink")*(3/2)k<sub>B</sub>T*\[67\]。這可以使用[麦克斯韦-玻尔兹曼分布](../Page/麦克斯韦-玻尔兹曼分布.md "wikilink")（見圖二）表明，該分佈為對系統中一質量為*m*的粒子的概率分佈

\[f (v) = 4 \pi
\left( \frac{m}{2 \pi k_B T}\right)^{3/2}\!\!v^2
\exp \Bigl(
\frac{-mv^2}{2k_B T}
\Bigr)\] ，

其中速率*v*為[速度](../Page/速度.md "wikilink")[向量](../Page/向量.md "wikilink")\(\mathbf{v} = (v_x,v_y,v_z)\)的大小\(\sqrt{v_x^2 + v_y^2 + v_z^2}\)
。

[麦克斯韦-玻尔兹曼分布可被用於任何物理系統](../Page/麦克斯韦-玻尔兹曼分布.md "wikilink")，而只須假定該系統是一個[正則系綜](../Page/正則系綜.md "wikilink")，亦即是明確地假定於溫度*T*時動能按[波茲曼因子分佈](../Page/波茲曼因子.md "wikilink")\[68\]。一質量為*m*的粒子，可由積分式求出其平均動能

\[\langle H^{\mathrm{kin}} \rangle =
\langle \tfrac{1}{2} m v^{2} \rangle =
\int _{0}^{\infty} \tfrac{1}{2} m v^{2}\  f(v)\  dv = \tfrac{3}{2} k_{B} T\]
，

跟均分定理指出的一致。

### 二次能量與配分函數

更一般地說，能量均分定理明確指出任何只在總能量 \(H\) 中只以一簡單二次項 \(Ax^2\) （此處 \(A\)
為一常數）出現的[自由度](../Page/自由度.md "wikilink") \(x\)
，於熱平衡時有平均能量 \(\frac{1}{2}k_BT\)
。在這個案中均分定理可從[配分函數](../Page/配分函數.md "wikilink")
\(Z(\beta)\) 中導出，其中 \(\beta =\frac{1}{k_BT}\) ，為正則溫度倒數\[69\]。從 \(Z\)
的式子中，經變量 \(x\) 取積分得因子

\[Z_{x} = \int_{-\infty}^{\infty} dx \ e^{-\beta A x^{2}} = \sqrt{\frac{\pi}{\beta A}}\]
，

而跟這個因子有關的平均能量為

\[\langle H_{x} \rangle = - \frac{\partial \log Z_{x}}{\partial \beta} = \frac{1}{2\beta} = \frac{1}{2} k_{B} T\]

跟均分定理指出的一致。

### 一般證明

均分定理的一般證明可於很多[統計力學的教科書中找到](../Page/統計力學.md "wikilink")，包括[微正則系綜用](../Page/微正則系綜.md "wikilink")\[70\]\[71\]
及[正則系綜用](../Page/正則系綜.md "wikilink")\[72\]\[73\]
的。它們都需要取系統[相空間中的平均](../Page/相空間.md "wikilink")，而它是一個[辛流形](../Page/辛流形.md "wikilink")。

要解釋這些推導，需要引入以下的記法。首先，相空間是由[廣義位置坐標](../Page/正則坐標.md "wikilink")*q*<sub>*j*</sub>跟它們的[共軛動量](../Page/共軛動量.md "wikilink")*p*<sub>*j*</sub>一起描述的。量*q*<sub>*j*</sub>完整描述系統[位形](../Page/位形空間.md "wikilink")，而量(*q*<sub>*j*</sub>,*p*<sub>*j*</sub>)一起則描述完整描述系統的[狀態](../Page/狀態.md "wikilink")。

第二，相空間的無限小體積

\[d\Gamma = \prod_{i} dq_{i} dp_{i}\] 需被引入以用於定義

\[\Gamma (E, \Delta E) = \int_{H \in \left[E, E+\Delta E \right]} d\Gamma .\]

在這式子中，*ΔE*被假定為非常小的，*ΔE\<\<E*。同樣地，Σ(*E*)被定義為相空間中能量被*E*低的總體積：

\[\Sigma (E) = \int_{H < E} d\Gamma .\]

由於*ΔE*非常小，以下這兩條積分式是等價的

\[\int_{H \in \left[ E, E+\Delta E \right]} \ldots d\Gamma  = \Delta E \frac{\partial}{\partial E} \int_{H < E} \ldots d\Gamma,\]

其中省略号代表被積函數。由此可得Γ與ΔE成正比

\[\Gamma = \Delta E \ \frac{\partial \Sigma}{\partial E} = \Delta E \ \rho(E),\]

此處*ρ(E)*為[狀態密度](../Page/狀態密度.md "wikilink")。由統計力學常用的定義得，[熵](../Page/熵.md "wikilink")*S*等於*k<sub>B</sub>*log*Σ(E)*，而溫度被定義為

\[\frac{1}{T} = \frac{\partial S}{\partial E} = k_{b} \frac{\partial \log \Sigma}{\partial E} = k_{b} \frac{1}{\Sigma}\,\frac{\partial \Sigma}{\partial E}\]
。

#### 正則系綜

在[正則系綜中](../Page/正則系綜.md "wikilink")，系統正與一[溫度為](../Page/溫度.md "wikilink")*T*（以開爾文計算）的無限熱庫處與熱平衡\[74\]\[75\]。在[相空間中每一態的概率由](../Page/相空間.md "wikilink")[波茲曼因子乘上](../Page/波茲曼因子.md "wikilink")[重整化因子](../Page/重整化因子.md "wikilink")\(\mathcal{N}\)得出，而重整化因子的值是為使總概率成一而設的。

\[\mathcal{N} \int e^{-\beta H(p, q)} d\Gamma = 1,\]

其中*β=1/k<sub>B</sub>T*。為一相空間變量*x<sub>k</sub>*（可為*q<sub>k</sub>*或*p<sub>k</sub>*）使用[分部積分法得方程](../Page/分部積分法.md "wikilink")

\[\mathcal{N} \int  \left[ e^{-\beta H(p, q)} x_{k} \right]_{x_{k}=a}^{x_{k}=b} d\Gamma_{k}+
\mathcal{N} \int  e^{-\beta H(p, q)} x_{k} \beta \frac{\partial H}{\partial x_{k}} d\Gamma = 1,\]

其中*dΓ<sub>k</sub>=dΓ/dx<sub>k</sub>*
，即第一個積分並沒有取*x<sub>k</sub>*積分的。第一項通常為零，這是因為在極限下*x<sub>k</sub>*為零，或因為能量在那極限下趨向無限。此時，可馬上得[正則系綜的均分定理](../Page/正則系綜.md "wikilink")

\[\mathcal{N} \int e^{-\beta H(p, q)} x_{k} \frac{\partial H}{\partial x_{k}} \,d\Gamma =
\Bigl\langle x_{k} \frac{\partial H}{\partial x_{k}} \Bigr\rangle = \frac{1}{\beta} = k_{B} T.\]

這裏被\(\langle \ldots \rangle\)符號化的平均是[正則系綜中的](../Page/正則系綜.md "wikilink")[正則平均](../Page/正則平均.md "wikilink")。

#### 微正則系綜

在[微正則系綜中](../Page/微正則系綜.md "wikilink")，系統與外界隔絕，或只是跟外界很弱的耦合起來\[76\]。因此，其總能量實際上是恒定的；要明確的話，則需指出總能量*H*局限於*E*及*E+ΔE*之間。對某固定能量*E*及其範圍*ΔE*而言，有一[相空間區域Γ](../Page/相空間.md "wikilink")，在內系統有該能量且每一態在該[相空間區域的機率均等](../Page/相空間.md "wikilink")。已知該等定義，相空間變量*x<sub>m</sub>*（可為*q<sub>k</sub>*或*p<sub>k</sub>*）的均分平均為

\[\begin{align}
\Bigl\langle x_{m} \frac{\partial H}{\partial x_{n}} \Bigr \rangle &=
\frac{1}{\Gamma}   \, \int_{H \in \left[ E, E+\Delta E \right]}  x_{m} \frac{\partial H}{\partial x_{n}} \,d\Gamma\\
&=\frac{\Delta E}{\Gamma}\, \frac{\partial}{\partial E} \int_{H < E}  x_{m} \frac{\partial H}{\partial x_{n}} \,d\Gamma\\
&= \frac{1}{\rho} \,\frac{\partial}{\partial E} \int_{H < E}  x_{m} \frac{\partial \left( H - E \right)}{\partial x_{n}} \,d\Gamma,
\end{align}\]

此處能得出最後一等式是因為*E*乃一常數，並不受*x*<sub>*n*</sub>影響。使用[分部積分法得方程](../Page/分部積分法.md "wikilink")

\[\begin{align}
\int_{H < E}  x_{m} \frac{\partial ( H - E )}{\partial x_{n}} \,d\Gamma &=
\int_{H < E}  \frac{\partial}{\partial x_{n}} \bigl( x_{m} ( H - E ) \bigr) \,d\Gamma -
\int_{H < E}  \delta_{mn} ( H - E ) d\Gamma\\
&=  \delta_{mn} \int_{H < E} ( E - H ) \,d\Gamma,
\end{align}\]
這是由於第一行右方第一項為零的緣故（該項能被寫成在*H*=*E*的[超曲面上](../Page/超曲面.md "wikilink")*H*-*E*的一個積分）。

將該結果代入前面的方程可得

\[\Bigl\langle x_{m} \frac{\partial H}{\partial x_{n}} \Bigr\rangle =
\delta_{mn} \frac{1}{\rho} \, \frac{\partial}{\partial E} \int_{H < E}\left( E - H \right)\,d\Gamma  =
\delta_{mn}  \frac{1}{\rho}  \, \int_{H < E} \,d\Gamma =
\delta_{mn}  \frac{\Sigma}{\rho}\] 。

由於\(\rho = \frac{\partial \Sigma}{\partial E}\)，可得均分定理

\[\Bigl\langle x_{m} \frac{\partial H}{\partial x_{n}} \Bigr\rangle =
\delta_{mn} \Bigl(\frac{1}{\Sigma} \frac{\partial \Sigma}{\partial E}\Bigr)^{-1}  =
\delta_{mn} \Bigl(\frac{\partial \log \Sigma} {\partial E}\Bigr)^{-1} = \delta_{mn} k_{B} T\]
。

如是者就導出了[均分定理的通用公式](../Page/#能量均分定理的通用公式化.md "wikilink")

<div style="border:2px solid black; padding:3px; margin-left: 0em; width: 250px;
text-align:left">

\[\!
\Bigl\langle x_{m} \frac{\partial H}{\partial x_{n}} \Bigr\rangle = \delta_{mn} k_{B} T,\]

</div>

也就是在之前應用中如此有用的那條公式。

## 限制

[1D_normal_modes_(280_kB).gif](https://zh.wikipedia.org/wiki/File:1D_normal_modes_\(280_kB\).gif "fig:1D_normal_modes_(280_kB).gif")的隔離系統中，能量並*不*由各[正常模態所攤分](../Page/正常模態.md "wikilink")；每個模態的能量都是固定且跟其他模態沒有關係。因此，均分定理在這樣的一個在[微正則系綜](../Page/微正則系綜.md "wikilink")（受隔離時）的系統，均分定理*不*成立，儘管它在[正則系綜中](../Page/正則系綜.md "wikilink")（跟一熱庫耦合時）的確成立。然而，如果把各模態夠強地非線性耦合起來，能量將會被攤分且均分定理會在兩個系綜中都成立。\]\]

### 遍歷性需求

均分定律只對處於熱平衡的[遍歷系統有效](../Page/遍歷假說.md "wikilink")，這意味着同一能量的態被遷移的可能性必然一樣\[77\]。故此，系統一定要可以讓它所有各形態的能量能夠互相交換，或在[正則系綜中跟一](../Page/正則系綜.md "wikilink")[熱庫一起](../Page/熱庫.md "wikilink")。已被證明為遍歷的系統數量不多；[雅科夫·西奈的](../Page/雅科夫·西奈.md "wikilink")[硬球系統是一個有名的例子](../Page/硬球系統.md "wikilink")\[78\]
。讓隔離系統保證其[遍歷性](../Page/遍歷理論.md "wikilink")——因而，均分定理——的需求已被研究過，同時研究還推動了[動力系統](../Page/動力系統.md "wikilink")[混沌理論的發展](../Page/混沌理論.md "wikilink")。一混沌[哈密頓系統不一定是遍歷系統](../Page/哈密頓系統.md "wikilink")，儘管假定它是通常也足夠準確\[79\]。

有時候能量並*不*由它的各種形式所攤分，且此時均分定理在微正則系綜*不*成立，耦合諧波振蕩器系統就是在這狀況下常被引用的一個例子\[80\]。如果系統跟外界隔絕，那每一個[正常模態的能量是恒定的](../Page/正常模態.md "wikilink")；能量並不由一個模態傳遞到另一模態的。因此在這樣一個系統中均分定理無效；每一個模態能量的量都被它的起始值所固定。如果[能量函數中有着足夠強的非線性量的時候](../Page/能量.md "wikilink")，能量可能可以在正常模態中傳遞，使系統走向遍歷並使均分定律有效。然而，[柯爾莫哥洛夫
- 阿諾德 -
莫澤定理明確指出除非擾動夠強](../Page/柯爾莫哥洛夫_-_阿諾德_-_莫澤定理.md "wikilink")，否則能量不會交換；如擾動小的話，最低限度能量會繼續受困於一些模態中。

### 量子效應引起的失敗

當熱能*k<sub>B</sub>T*比能階間的差要小得多的時候，均分法則就會失效。均分此時不再成立，是因為能階組成平滑[連續能譜的這個假設跟實際情況不近似](../Page/連續能譜.md "wikilink")，而這假設[在上面均分定理推導中有用到](../Page/#推導.md "wikilink")\[81\]\[82\]。歷史上，古典均分定理在解釋[比熱及](../Page/比熱.md "wikilink")[黑體輻射時的失敗](../Page/黑體輻射.md "wikilink")，對表明需要一套物質及輻射的新理論（即[量子力學及](../Page/量子力學.md "wikilink")[量子場論](../Page/量子場論.md "wikilink")）起了關鍵性的作用\[83\]。

[Et_fig2_zh.png](https://zh.wikipedia.org/wiki/File:Et_fig2_zh.png "fig:Et_fig2_zh.png")

要說明均分的失效，可考慮一單（量子）諧波振蕩子的平均能量，古典個案在上文已討論過。它的量子能階為*E<sub>n</sub>=nhν*，其中*h*為[普朗克常數](../Page/普朗克常數.md "wikilink")，*ν*為振蕩子的[基本頻率](../Page/基本頻率.md "wikilink")，而*n*則為一整數。某指定能階正被置於[正則系綜的概率可由其](../Page/正則系綜.md "wikilink")[波茲曼因子得出](../Page/波茲曼因子.md "wikilink")

\[P(E_{n}) = \frac{e^{-n\beta h\nu}}{Z}\]，
其中*β*=1/*k*<sub>*B*</sub>*T*，而分母中的*Z*為[配分函數](../Page/配分函數.md "wikilink")，此處為一等比級數

\[Z = \sum_{n=0}^{\infty} e^{-n\beta h\nu} = \frac{1}{1 - e^{-\beta h\nu}}\]。

則其平均能量為

\[\langle H \rangle = \sum_{n=0}^{\infty} E_{n} P(E_{n}) =
\frac{1}{Z} \sum_{n=0}^{\infty} nh\nu \ e^{-n\beta h\nu} =
-\frac{1}{Z} \frac{\partial Z}{\partial \beta} =
-\frac{\partial \log Z}{\partial \beta}\]。

將*Z*的式子代入得最後結果\[84\]：

\[\langle H \rangle = h\nu \frac{e^{-\beta h\nu}}{1 - e^{-\beta h\nu}}\]。

於高溫時，當熱能*k<sub>B</sub>T*比能階差*hν*大得多的時候，指數變量*βhν*比一要小得多，所以平均能量成了*k<sub>B</sub>T*，跟均分定理一致（見圖十）。然而於低溫時，當*hν*\>\>*k<sub>B</sub>T*的時候，平均能量走向零——高頻能階被“凍結”了（見圖十）。作為另一例子，氫原子內部的受激電子態在室溫下並不提供任何比熱，這是由於熱能*k<sub>B</sub>T*（大概是0.025 [eV](../Page/電子伏.md "wikilink")）比最低及下一高能階之間的差（大概是10 [eV](../Page/電子伏.md "wikilink")）要小得多的緣故。

相近的考量可用於任何能階差比熱能大得多的狀況下。例如，[艾爾伯特·愛因斯坦就是用這套論證解決](../Page/艾爾伯特·愛因斯坦.md "wikilink")[黑體輻射的](../Page/黑體輻射.md "wikilink")[紫外災變](../Page/紫外災變.md "wikilink")\[85\]。由於在一封閉容器下的[電磁場有無限個獨立模態](../Page/電磁場.md "wikilink")，每一個都能被當作諧波振蕩器看待，因而就形成了悖論。如果每一個電磁模態皆有平均能量*k<sub>B</sub>T*，容器內的能量將為無限大\[86\]\[87\]。然而，根據以上的論證，高ω模態的平均值當ω趨向無限時趨向零；而且描術模態實驗中能量分佈的[普朗克黑體輻射定律](../Page/普朗克黑體輻射定律.md "wikilink")，也是根據同一組論證所中得出的\[88\]。

此外，更微妙的量子效應可引起均分定理的修正，例如[全同粒子及](../Page/全同粒子.md "wikilink")[連續對稱](../Page/對稱.md "wikilink")。全同粒子效應可在非常高密度且低溫時有着顯著的效果。比方說金屬的[價電子可以有幾個](../Page/價電子.md "wikilink")[電子伏的平均能量](../Page/電子伏.md "wikilink")，正常情況一般對應數萬開爾文的溫度。如此的狀態，密度高得讓[泡利不相容原理使得古典門徑無效化](../Page/泡利不相容原理.md "wikilink")，被稱為[簡并態費米子氣體](../Page/簡并態物質.md "wikilink")。這種氣體對[白矮星及](../Page/白矮星.md "wikilink")[中子星的結構很重要](../Page/中子星.md "wikilink")。在低溫時，[玻色-愛因斯坦凝聚](../Page/玻色-愛因斯坦凝聚.md "wikilink")（此凝聚中大量全同粒子佔據了低能量態）的[費米子類比能夠形成](../Page/費米凝聚.md "wikilink")；這種[超流體電子是引起](../Page/超流體.md "wikilink")[超導現象的成因](../Page/超導現象.md "wikilink")。

## 另見

  - [均功定理](../Page/均功定理.md "wikilink")
  - [分子運動論](../Page/分子運動論.md "wikilink")
  - [統計力學](../Page/統計力學.md "wikilink")
  - [量子統計力學](../Page/量子統計力學.md "wikilink")

## 註釋及參考資料

## 延伸閱讀

<div style="font-size:90%">

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  -
<!-- end list -->

  - ASIN B00085D6OO

<!-- end list -->

  -

</div>

## 外部連結

  - [一單原子及雙原子混合氣體的均分定理即時Applet展示](http://webphysics.davidson.edu/physlet_resources/thermo_paper/thermo/examples/ex20_4.html)
  - [恒星物理中的能量均分定理](http://www.sciencebits.com/StellarEquipartition)，由[耶路撒冷希伯來大學Racah物理研究所副教授Nir](../Page/耶路撒冷希伯來大學.md "wikilink")
    J. Shaviv所撰寫。

[N](../Category/基本物理概念.md "wikilink")
[N](../Category/統計力學.md "wikilink")
[N](../Category/熱力學.md "wikilink")
[N](../Category/物理定理.md "wikilink")

1.
2.  [Fact Sheet on Uranium
    Enrichment](http://www.nrc.gov/reading-rm/doc-collections/fact-sheets/enrichment.html)
    U.S. Nuclear Regulatory Commission. Accessed 30th April 2007

3.
4.

5.

6.
7.
8.

9.

10.
11.
12.

13.

14.

15.

16.

    (abstract only). Not published in full until  Reprinted
        (reprinted in his *Papers*, **3**, 167, 183.)
    。
    瓦塔斯頓的關鍵論文是於1845年向[英國皇家學會提交的](../Page/英國皇家學會.md "wikilink")。在他拒絕發表他的研究後，學會也拒絕退回稿件並予以存檔。該原稿於1845年被[瑞利勳爵發現](../Page/瑞利.md "wikilink")，他對當時未能夠認可瓦塔斯頓研究重要性的審查員作出了批評。瓦塔斯頓成功將他的見解於1851年發表，因此比麥克斯韋宣佈第一版的能量均分定理要早

17. 。1859年9月21日，在英國科學促進協會在阿伯丁的一次會議中，由麥克斯韋教授讀出。

18.   在這一份初步研究中，波茲曼表明了當系統被外在和諧力作用時，其平均總動能等於平均總勢能。

19.

20.
21.

22.

23.



24.
            1841年1月11日於法國自然科學院被讀出。


25.

26.

27.

28.

29.

30.
31.

32.  Re-issued in 1987 by MIT Press as *Kelvin's Baltimore Lectures and
    Modern Theoretical Physics: Historical and Philosophical
    Perspectives* (Robert Kargon and Peter Achinstein, editors). ISBN
    978-0-262-11117-1

33.

34.





35.
36.

37.

38.
39.
40.

41.

42.

43.

44.

45.

46.
47.

48.
49.
50.
51.
52.
53.
54.

55.
56.

57.

58.
59.
60.

61.

62.

63.

64.

65.

66.

67.

68.
69.

70.
71.
72.
73.
74.
75.
76.
77.
78.

79.
80.

81.
82.
83.
84.
85.  .
    英語[維基文庫上有本論文的](../Page/維基文庫.md "wikilink")[英語翻譯](../Page/s:A_Heuristic_Model_of_the_Creation_and_Transformation_of_Light.md "wikilink")。

86.
87.

88.