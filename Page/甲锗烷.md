**甲锗烷**是[锗烷](../Page/锗烷.md "wikilink")（Ge<sub>n</sub>H<sub>2n+2</sub>）中最简单的一种，分子式为GeH<sub>4</sub>。和同族的[甲烷](../Page/甲烷.md "wikilink")、[甲硅烷一样](../Page/甲硅烷.md "wikilink")，甲锗烷也是[正四面体结构](../Page/正四面体.md "wikilink")。甲锗烷在空气中燃烧生成[二氧化锗和](../Page/二氧化锗.md "wikilink")[水](../Page/水分子.md "wikilink")。

## 合成

甲锗烷可通过以下方法合成：

  - 化学还原法\[1\]：[硼氢化钠](../Page/硼氢化钠.md "wikilink")、[硼氢化钾](../Page/硼氢化钾.md "wikilink")、[硼氢化锂](../Page/硼氢化锂.md "wikilink")、[氢化铝锂](../Page/氢化铝锂.md "wikilink")、[氢化铝钠](../Page/氢化铝钠.md "wikilink")、[氢化锂](../Page/氢化锂.md "wikilink")、[氢化钠或](../Page/氢化钠.md "wikilink")[氢化镁还原](../Page/氢化镁.md "wikilink")[锗](../Page/锗.md "wikilink")、[四氯化锗或](../Page/四氯化锗.md "wikilink")[二氧化锗](../Page/二氧化锗.md "wikilink")。这类反应既可在水溶液中进行，也可在有机溶剂中进行。实验室中可通过含氢负离子的化合物与Ge（IV）化合物反应得到，如\[2\]：

<!-- end list -->

  -

      -
        Na<sub>2</sub>GeO<sub>3</sub> + NaBH<sub>4</sub> + H<sub>2</sub>
        → GeH<sub>4</sub> + NaBO<sub>2</sub> + 2NaOH

<!-- end list -->

  - 电化学还原法：锗作[阴极](../Page/阴极.md "wikilink")，[钼或](../Page/钼.md "wikilink")[镉作](../Page/镉.md "wikilink")[正极](../Page/正极.md "wikilink")。反应时阴极产生甲锗烷和[氢气](../Page/氢气.md "wikilink")，阳极生成钼或镉的[氧化物](../Page/氧化物.md "wikilink")。
  - 等离子合成法：用[原子氢轰击锗](../Page/原子氢.md "wikilink")，得到甲锗烷和[乙锗烷](../Page/乙锗烷.md "wikilink")。

## 自然含量

[木星的大气中检测到了甲锗烷的存在](../Page/木星.md "wikilink")\[3\]。

## 用途

甲锗烷在600K时分解为锗和氢气。这种对热不稳定性被用于[半导体工业中](../Page/半导体.md "wikilink")，即[有机金属化学气相沉积法](../Page/有机金属化学气相沉积法.md "wikilink")（MOCVD）\[4\]。由于甲锗烷毒性较大，某些[有机锗化合物](../Page/有机锗化合物.md "wikilink")（如[异丁基锗](../Page/异丁基锗.md "wikilink")）可以替代甲锗烷，应用于MOCVD中\[5\]。

## 安全

甲锗烷[可燃](../Page/可燃.md "wikilink")，并且有[自燃的可能性](../Page/自燃.md "wikilink")。甲锗烷有毒。

## 参考资料

## 外部链接

  - \[<http://www.praxair.com/>.../05048df209e48e19852569940080e600/$FILE/GermaneGeH4-5.0.pdf
    PRAXAIR\]
  - [Airliquide](https://web.archive.org/web/20070329234436/http://www.us.airliquide.com/en/business/products/gases/gasdata/index.asp?GasID=31)
  - [pdf MSDS at
    MIT](https://web.archive.org/web/20080309071802/http://sauvignon.mit.edu/safety/GeH4.pdf)

[ru:Германы](../Page/ru:Германы.md "wikilink")

[Category:锗化合物](../Category/锗化合物.md "wikilink")
[Category:金属氢化物](../Category/金属氢化物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.  US Patent 7,087,102 (2006)
2.  Girolami, G. S.; Rauchfuss, T. B. and Angelici, R. J., Synthesis and
    Technique in Inorganic Chemistry, University Science Books: Mill
    Valley, CA, 1999.
3.
4.
5.