[Ritalin-SR-20mg-full.jpg](https://zh.wikipedia.org/wiki/File:Ritalin-SR-20mg-full.jpg "fig:Ritalin-SR-20mg-full.jpg")錠\]\]
[Cocaine_lines_2.jpg](https://zh.wikipedia.org/wiki/File:Cocaine_lines_2.jpg "fig:Cocaine_lines_2.jpg")常拿來做為娛樂性藥物\]\]

**興奮劑**又稱為**中樞神經興奮劑**、**中樞神經刺激劑**（英文名稱：**stimulant**、**psycho-stimulant**）是一系列[精神藥物的統稱](../Page/精神藥物.md "wikilink")，其中包括可以增加活動力的藥物\[1\]、會令人感到愉快和振奮的藥物，以及有交感兴奋作用的藥物\[2\]。兴奋剂可以提升警覺心、[注意力和活力](../Page/注意力.md "wikilink")，同時也增加[血壓](../Page/血壓.md "wikilink")、[心跳和](../Page/心跳.md "wikilink")[呼吸](../Page/呼吸.md "wikilink")\[3\]，常用作[處方藥](../Page/處方藥.md "wikilink")（例如[ADHD的兒童或成人](../Page/ADHD.md "wikilink")\[4\]、[嗜睡症](../Page/嗜睡症.md "wikilink")），但也有用於藥物治療以外的使用（可能是或是[非法使用](../Page/毒品.md "wikilink")），可能做為或是[娛樂性藥物](../Page/娛樂性藥物.md "wikilink")。

一些藥物能影響自我管理能力。例如：歸類為中樞神經興奮劑的藥物：[哌甲酯](../Page/哌甲酯.md "wikilink")（methylphenidate）和[安非他命](../Page/安非他命.md "wikilink")（amphetamine）。<u>適度適量</u>使用，能提升一個人整體的[衝動控制能力](../Page/衝動控制能力.md "wikilink")（inhibitory
control），且被用來治療[注意力不足過動症](../Page/注意力不足過動症.md "wikilink")（ADHD）患者。\[5\]\[6\]
同理，（depressants）（例如：[酒精](../Page/酒精.md "wikilink")）由於會讓腦中[神經傳導物質濃度降低](../Page/神經傳導物質.md "wikilink")、減少許多大腦區域的活性等，所以可能會造成專注力、神智清醒度等自我管理能力的下降。\[7\]

在美國，2013年最常用的處方藥兴奋剂有[lisdexamfetamine](../Page/lisdexamfetamine.md "wikilink")、[哌甲酯及](../Page/哌甲酯.md "wikilink")[安非他命](../Page/安非他命.md "wikilink")\[8\]。

## 醫療用途

中樞神經刺激劑在醫學上用來治療[肥胖](../Page/肥胖.md "wikilink")、[睡眠疾患](../Page/睡眠疾患.md "wikilink")、[嗜睡症](../Page/narcolepsy.md "wikilink")（narcolepsy）、[情感性疾患](../Page/情感性疾患.md "wikilink")、[注意力不足過動症](../Page/注意力不足過動症.md "wikilink")、、、[氣喘](../Page/氣喘.md "wikilink")、和[鼻塞等](../Page/鼻塞.md "wikilink")。
\[9\] \[10\]

### 注意力不足過動症

用來治療ADHD的興奮劑包含：

  - [哌甲酯](../Page/哌甲酯.md "wikilink")

  - [苯丙胺](../Page/苯丙胺.md "wikilink")

  - [Dextroamphetamine](../Page/Dextroamphetamine.md "wikilink")

  -
\[11\]

## 安全性

只要按照指示使用，中樞神經刺激劑如同其他藥品一樣安全，不會導致藥物濫用或成癮。研究顯示，以中樞神經興奮劑治療注意力不足過動症的患者，其往後將比<u>沒有</u>以中樞神經興奮劑治療的注意力不足過動症患者享有明顯更低的藥物濫用風險。\[12\]

## 副作用

如同其他藥物，中樞神經刺激劑也有副作用產生的可能，然而不是每個服用興奮劑的人都會有副作用。中樞神經刺激劑的副作用大都輕微（minor）並且在劑量降低或與其他藥物併用、調整服藥時間等調適後消失。

較常見的副作用包含：

  - 難以入睡或難以維持睡眠。
  - 胃口降低。
  - 胃部不適。
  - 頭疼。

較少見的副作用包含：

  - 肢體抽動或聲音抽動。（突然的、重複的動作或聲音）

\[13\]

## 備註

## 參考資料

## 相關條目

  - [抗精神病药](../Page/抗精神病药.md "wikilink")

  -
  - [致幻剂](../Page/致幻剂.md "wikilink")

  - [益智药](../Page/益智药.md "wikilink")

[category:药物](../Page/category:药物.md "wikilink")
[category:超人类主义](../Page/category:超人类主义.md "wikilink")

[Category:精神藥理學](../Category/精神藥理學.md "wikilink")
[Category:兴奋剂](../Category/兴奋剂.md "wikilink")
[Category:注意力不足過動症的治療](../Category/注意力不足過動症的治療.md "wikilink")

1.   Oxford
    Dictionaries|url=[https://en.oxforddictionaries.com/definition/stimulant|website=Oxford](https://en.oxforddictionaries.com/definition/stimulant%7Cwebsite=Oxford)
    Dictionaries {{\!}} English}}

2.

3.
4.

5.

6.

7.  {{ cite web | url =
    <http://www.drugfreeworld.org/drugfacts/prescription/depressants.html>
    | title = Long-term & Short-term effects, depressants, brand names:
    Foundation for a drug free work }}

8.

9.
10.

11.
12.
13.