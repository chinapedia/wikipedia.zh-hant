**歐洲中央銀行**（；；），簡稱**歐洲央行**，總部位於[德國](../Page/德國.md "wikilink")[法蘭克福](../Page/法蘭克福.md "wikilink")，成立於1998年6月1日，負責[歐盟](../Page/歐盟.md "wikilink")[欧元区的金融及貨幣政策](../Page/欧元区.md "wikilink")，為歐元區的[中央銀行](../Page/中央銀行.md "wikilink")。

首任歐洲中央銀行行長為[维姆·德伊森贝赫](../Page/维姆·德伊森贝赫.md "wikilink")，曾任[荷蘭央行行長](../Page/荷蘭中央銀行.md "wikilink")，曾在[阿姆斯特丹大學教授宏觀經濟學](../Page/阿姆斯特丹大學.md "wikilink")。其繼任人為[让-克洛德·特里谢](../Page/让-克洛德·特里谢.md "wikilink")，法國人，曾任法國央行[法蘭西銀行行長](../Page/法蘭西銀行.md "wikilink")，於2003年11月接任。现任歐洲中央銀行行長為[马里奥·德拉基](../Page/马里奥·德拉基.md "wikilink")，[意大利人](../Page/意大利.md "wikilink")，曾任[意大利银行行長](../Page/意大利银行.md "wikilink")，於2011年11月接任。

## 組織和结构

[Frankfurt_European_Central_Bank.JPG](https://zh.wikipedia.org/wiki/File:Frankfurt_European_Central_Bank.JPG "fig:Frankfurt_European_Central_Bank.JPG")\]\]
歐洲中央銀行由其[董事會負責管理](../Page/董事會.md "wikilink")，設有董事會主席，並設有[理事會](../Page/理事會.md "wikilink")，包括了董事會的成員及各歐洲國家各自的中央銀行代表。

歐洲中央銀行的組織主要是以[德國中央銀行及](../Page/德國中央銀行.md "wikilink")[西德意志地方票據交換銀行為主要模仿對象](../Page/西德意志地方票據交換銀行.md "wikilink")。

## 执行董事会

执行董事会包括六名成员，为欧洲央行的政策制定策略。其中的四个位置为欧盟四个最大的中央银行，法国、德国、意大利和西班牙保持。

## 歐洲中央銀行體系

[歐洲中央銀行體系](../Page/歐洲中央銀行體系.md "wikilink")（ESCB）包含了歐洲中央銀行及歐盟27個成員國各自的中央銀行，只有各成員國自身負責歐盟事務的央行董事可以參與其中及進行決策。

## 批評

歐洲中央銀行主要面對兩種批評，分別為自主性不足及目標不均衡。

### 自主性

[La2-euro.jpg](https://zh.wikipedia.org/wiki/File:La2-euro.jpg "fig:La2-euro.jpg")
部份批評者認為歐洲中央銀行的自主性不足。歐洲中央銀行是各成員國的中央銀行聯合籌備成立的，以避免受到政治的影響。雖然其成立的目的及權力均來自政治，但其決策權力卻由歐洲中央銀行自身掌握，不受各國的影響。而那些不在歐元區內的歐盟國家的中央銀行（如[丹麥中央銀行及](../Page/丹麥中央銀行.md "wikilink")[英格蘭銀行](../Page/英格蘭銀行.md "wikilink")），亦受這些條文約束。部份批評者認為歐洲中央銀行[民主程度不高](../Page/民主.md "wikilink")，其決策過程黑箱作業，缺乏應有的監管，所以其最終政策有侵犯[人權及破壞](../Page/人權.md "wikilink")[自然環境的可能性](../Page/自然環境.md "wikilink")。歐洲中央銀行不會發佈其決定，亦不會接受意見。當執行決策後，其主頁並不會收集市民的意見。因為要保障理事會成員的權益，所以其內部會議的紀錄不會披露。歐盟公民可以通由選舉[歐洲議會議員而間接影響歐洲中央銀行的決策](../Page/歐洲議會.md "wikilink")，然而若然經濟環境不許可，政客亦可能沒法改變歐洲中央銀行的決策。不過歐洲中央銀行需要向歐洲議會負責，主要官員的委任亦需要獲得其通過。而法律亦規定歐洲中央銀行行長需要每年向歐洲議會提交年度報告，除此之外，歐洲中央銀行董事會成員亦需每年四次與歐洲議會進行會面，以交待中央銀行自身情況，若有需要，可以舉行更頻密的舉行。很多經濟學家均認為歐洲中央銀行的自主性是極其重要的，因為這可以防止市場因政治目的而被操控。

### 通膨目標

部份批評者認為歐洲中央銀行所定下的目標並不合理，歐洲中央銀行以利率機制來控制[通脹率](../Page/通脹率.md "wikilink")，但卻不為[失業率及](../Page/失業率.md "wikilink")[貨幣匯率設定調控政策](../Page/貨幣匯率.md "wikilink")，這使得部份人感到這樣的經濟政策並不平衡，有可能導致各項經濟及民生的問題。很多英國的經濟學家均指出歐洲中央銀行需要實行平衡的目標組合，就如英格蘭中央銀行現在所採用的。而歐洲中央銀行所設定的不平常的低[利率環境亦受到很多批評者批評](../Page/利率.md "wikilink")，認為並不符合歐盟成員國自身的情況。這個低利率環境正是[愛爾蘭經濟泡沫的成因](../Page/愛爾蘭經濟泡沫.md "wikilink")。雖然這可以防止歐盟主要成員國如[法國](../Page/法國.md "wikilink")、[德國及](../Page/德國.md "wikilink")[義大利出現經濟不景氣](../Page/義大利.md "wikilink")，然而卻沒有顧及其他成員國的利益。需要注意的是[日本及](../Page/日本.md "wikilink")[美國近年亦採用低利率政策](../Page/美國.md "wikilink")。

## 參看條目

  - [歐洲聯盟](../Page/歐洲聯盟.md "wikilink")
  - [歐元](../Page/歐元.md "wikilink")
  - [英倫銀行](../Page/英倫銀行.md "wikilink")
  - [日本銀行](../Page/日本銀行.md "wikilink")
  - [美國聯邦儲備局](../Page/美國聯邦儲備局.md "wikilink")
  - [中华民国中央银行](../Page/中华民国中央银行.md "wikilink")
  - [中国人民银行](../Page/中国人民银行.md "wikilink")

## 参考资料

## 參考书籍

  - Francesco Papadia, La Banque centrale européenne, Paris, Revue
    "Banque", 1999.
  - Michel Dévoluy, La banque centrale européenne, Paris, Presses
    universitaires de France, 2000.
  - Patrick Artus, L'euro et la Banque centrale européenne, Paris,
    Economica, 2001.
  - Conseil d'analyse économique, La Banque centrale européenne, Paris,
    la Documentation française, 2002.
  - Jean-Paul Fitoussi, Jérôme Creel, How to reform the European Central
    Bank, Londres, Centre for European reform, 2002.
  - David Howarth, Peter Loedel, The European Central Bank \[Texte
    imprimé\] : the new European leviathan ?, Basingstoke, Palgrave
    Macmillan, 2003.
  - Emmanuel Appel, Central banking systems compared : the ECB, the
    pre-euro Bundesbank, and the Federal Reserve System, London ; New
    York : Routledge, 2003.
  - Hanspeter K. Scheller, The European Central Bank, History, role and
    functions, Francfort, European Central Bank,
    2004[1](http://www.ecb.int/pub/pdf/other/ecbhistoryrolefunctions2004en.pdf).

  - Banque Centrale Européenne, Les statistiques de la BCE, Aperçu, août
    2005, Francfort, Banque Centrale Européenne,
    2005[2](http://www.ecb.int/pub/pdf/other/statbriefoverview2005fr.pdf).


## 外部連結

  - [歐洲中央銀行主頁](http://www.ecb.int)
  - [獲勝的歐洲中央銀行未來總部的設計](http://www.ecb.int/ecb/premises/html/image29.en.html)
  - [歷史上的歐洲貨幣圖表](http://forex-history.net)

[E](../Category/1998年建立的組織.md "wikilink")
[E](../Category/1998年德國建立.md "wikilink")
[E](../Category/中央銀行.md "wikilink")
[E](../Category/歐盟組織.md "wikilink")
[E](../Category/歐洲中央銀行體系.md "wikilink")
[E](../Category/歐洲國家政府間組織.md "wikilink")
[E](../Category/國際經濟組織.md "wikilink")
[E](../Category/總部在法蘭克福的國際組織.md "wikilink")