[Shing_Mun_River_View_201304.jpg](https://zh.wikipedia.org/wiki/File:Shing_Mun_River_View_201304.jpg "fig:Shing_Mun_River_View_201304.jpg")
[Shing_Mun_River_Dusk_201207.jpg](https://zh.wikipedia.org/wiki/File:Shing_Mun_River_Dusk_201207.jpg "fig:Shing_Mun_River_Dusk_201207.jpg")
[HK_ShingMunRiver_Upstream.JPG](https://zh.wikipedia.org/wiki/File:HK_ShingMunRiver_Upstream.JPG "fig:HK_ShingMunRiver_Upstream.JPG")
[ShaTin-ShingMunRiver-EarlyStageOfDevelopment.jpg](https://zh.wikipedia.org/wiki/File:ShaTin-ShingMunRiver-EarlyStageOfDevelopment.jpg "fig:ShaTin-ShingMunRiver-EarlyStageOfDevelopment.jpg")
[Shing_Mun_River_Dusk_view_201511.jpg](https://zh.wikipedia.org/wiki/File:Shing_Mun_River_Dusk_view_201511.jpg "fig:Shing_Mun_River_Dusk_view_201511.jpg")
[Shing_Mun_River_Channel_at_night_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Shing_Mun_River_Channel_at_night_\(Hong_Kong\).jpg "fig:Shing_Mun_River_Channel_at_night_(Hong_Kong).jpg")

**城門河**（）又稱**城門河道**（），是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[沙田區的一條](../Page/沙田區.md "wikilink")[河流](../Page/河流.md "wikilink")。城門河兩旁建有不少多層住宅大廈，亦有一些商業及工業大廈，又有多條[橋樑橫跨城門河](../Page/橋.md "wikilink")，方便居民往來兩岸。

今日城門河大部份為人工河道，其建築原意是為把沙田區的[污水及可能出現的](../Page/污水.md "wikilink")[洪水排出大海](../Page/洪水.md "wikilink")，排水範圍有37[平方公里](../Page/平方千米.md "wikilink")。亦為區內居民提供一個進行康樂活動，包括騎[自行車](../Page/自行車.md "wikilink")、[緩步跑](../Page/緩步跑.md "wikilink")、[划艇及](../Page/划艇.md "wikilink")[龍舟競渡的地方](../Page/龍舟.md "wikilink")。

## 歷史

城門河上游為[城門水塘](../Page/城門水塘.md "wikilink")，有多條引水道將[大帽山](../Page/大帽山.md "wikilink")、[針山及](../Page/針山.md "wikilink")[鉛礦坳等地方的山水引到水塘](../Page/鉛礦坳.md "wikilink")，當水塘滿溢時，山水會經城門峽流至[下城門水塘](../Page/下城門水塘.md "wikilink")，城門峽附近的[走私坳及](../Page/走私坳.md "wikilink")[草山水源亦會流進下城門水塘](../Page/草山_\(香港\).md "wikilink")，每當下城門水塘滿溢，水塘內的儲水會排進城門河，出口昔日是流往[沙田海](../Page/沙田海.md "wikilink")，約在今日[大圍附近](../Page/大圍.md "wikilink")。昔日的沙田海又稱潮水灣，是一個形似[喇叭的狹長形](../Page/喇叭.md "wikilink")[海灣](../Page/海灣.md "wikilink")，連接[吐露港](../Page/吐露港.md "wikilink")，當潮漲時，海水便會湧入沙田海。

早期的城門河盡頭是在現今[獅子橋附近](../Page/獅子橋.md "wikilink")，河口爲兩面環山的沙田海。當時，[九廣鐵路英段路軌以東已經是海岸](../Page/九廣鐵路英段.md "wikilink")，而[火炭](../Page/火炭.md "wikilink")、[小瀝源爲沿海的小村落](../Page/小瀝源.md "wikilink")，[圓洲角更是在沙田海中間的小島](../Page/圓洲角.md "wikilink")。及後，經過無數次的填海工程，沙田海變成了筆直的城門河，一直伸展至[大水坑](../Page/大水坑.md "wikilink")。

1970年代政府發展沙田新市鎮，在沙田海兩岸進行[填海工程](../Page/填海.md "wikilink")，並把城門河延長成為一條7公里長，200米闊的人工河道，由大圍開始，流經[沙田市中心及](../Page/沙田市中心.md "wikilink")[沙田馬場](../Page/沙田馬場.md "wikilink")，流向沙田海及吐露港。其他昔日流向沙田海的小河亦改為流入城門河的支流或明渠，這些支流共有3條，包括[大圍渠](../Page/大圍渠.md "wikilink")、[火炭渠及](../Page/火炭渠.md "wikilink")[小瀝源渠](../Page/小瀝源渠.md "wikilink")，當中大圍渠更大約在1970、80年代封閉成一條暗渠。不過，在城門河出口處有一條流經大水坑及[富安花園一帶的明渠](../Page/富安花園.md "wikilink")——[女婆東坑](../Page/女婆東坑.md "wikilink")，由於它是位於城門河出口處匯入城門河及流出沙田海，所以女婆東坑一般都不會認為是城門河的支流。

而沙田海大部分[面積成為了今日城門河兩岸的土地](../Page/面积.md "wikilink")，當時位於沙田海中央的小島圓洲則成為東岸陸地的一部份，並改稱圓洲角。現在城門河的出口，即[馬料水與](../Page/馬料水.md "wikilink")[馬鞍山之間的海灣](../Page/馬鞍山_\(香港市鎮\).md "wikilink")，仍稱為沙田海<small></small>。

2003年，[民建聯建議](../Page/民建聯.md "wikilink")<small></small>由[香港賽馬會及地產商合作興建多項設施以提高城門河的吸引力](../Page/香港賽馬會.md "wikilink")，包括興建接駁賽馬會及[馬鞍山鐵路](../Page/馬鞍山鐵路.md "wikilink")[石門站的購物橋](../Page/石門站_\(香港\).md "wikilink")，及在[沙田公園附近的城門河上興建](../Page/沙田公園.md "wikilink")[人工島](../Page/人工島.md "wikilink")，作為購物及飲食地區等，惟建議尚未落實。

在[2008年夏季奧林匹克運動會香港區火炬接力中](../Page/2008年夏季奧林匹克運動會香港區火炬接力.md "wikilink")，第64位火炬手[施幸余乘坐](../Page/施幸余.md "wikilink")[龍舟在城門河上傳遞](../Page/龍舟.md "wikilink")[奧運火炬長達](../Page/奧運.md "wikilink")60分鐘。此種傳遞[奧運火炬的方法是](../Page/奧運.md "wikilink")[奧運史上首次](../Page/奧運.md "wikilink")，亦是香港區火炬接力歷時最長的一棒。

2013年2月1日，[沙田區議會就](../Page/沙田區議會.md "wikilink")[香港政府撥款的](../Page/香港政府.md "wikilink")[社區重點項目計劃舉行特別會議](../Page/社區重點項目計劃.md "wikilink")，初步決定參考[中國大陸](../Page/中國大陸.md "wikilink")[清溪河及](../Page/清溪河.md "wikilink")[韓國](../Page/韓國.md "wikilink")[清溪川等的發展模式](../Page/清溪川.md "wikilink")，將撥款作為美化城門河兩岸，發展景點及河上觀光等項目，以及鋪平城門河大圍段一帶不平路段，為以後興建地區設施作好準備\[1\]。但最後只在大圍明渠一小部分建興建5人足球場及看台，落實在[沙田運動場對出一段行人路興建伸延平台](../Page/沙田運動場.md "wikilink")，在[瀝源橋](../Page/瀝源橋.md "wikilink")、[沙燕橋及](../Page/沙燕橋.md "wikilink")[翠榕橋上安裝LED燈飾](../Page/翠榕橋.md "wikilink")，此舉比較以往在不同節日安裝燈泡後又拆走更加符合[環境保護要求](../Page/環境保護.md "wikilink")；另外在鄰近[香港文化博物館及](../Page/香港文化博物館.md "wikilink")[沙田公園的兩條行人橋的欄杆亦會被更換](../Page/沙田公園.md "wikilink")，與照明系統融為一體\[2\]。不過地區人士認為五人足球場旁的大圍遊樂場屬「住宅（甲）」類用途，質疑在球場建成後將會改建為「插針樓」。而且天橋安裝燈飾不切實際。

<File:Lower> Shing Mun 2017.jpg|人工河道起點 <File:HK>
ShingMunRiver_UpperStream_HeungFanLiu.jpg|上游近[香粉寮](../Page/香粉寮.md "wikilink")
<File:HK> ShingMunRiver
UpperStream.jpg|上游近[美林邨](../Page/美林邨.md "wikilink")
<File:Shing> Mun River Tai Wai Section View
201103.jpg|中游近[文禮閣開始擴闊](../Page/文禮閣.md "wikilink")
<File:Shing> Mun River Mid Stream Heritage Museum
2013.jpg|中游近[文化博物館](../Page/香港文化博物館.md "wikilink")
<File:HK> Shing Mun River view 201305.jpg|從沙燕橋前望 <File:Shing> Mun River
View1 20120911.jpg|沙田海出口 <File:Tai> Wai Drainage 201206.jpg|大圍渠
<File:Fo> Tan Nullah 200808.jpg|火炭渠 <File:Shek> Mun 2016.jpg|小瀝源渠

## 污染問題

1980年代，城門河曾經受到鄰近工業區及住宅等排出的污水所污染，[河水完全不適合任何生物生存](../Page/河水.md "wikilink")。[香港政府於](../Page/香港殖民地時期#香港政府.md "wikilink")1988年開始實施多項措施，以改善城門河的水質，包括禁止任何廢水排放到城門河，禁止沙田區居民飼養[禽](../Page/禽.md "wikilink")[畜](../Page/畜.md "wikilink")，亦把區內所有鄉村的污水，排放到[沙田污水處理廠進行處理](../Page/沙田污水處理廠.md "wikilink")。政府亦把[沙田濾水廠的](../Page/沙田濾水廠.md "wikilink")[明礬淤泥收集](../Page/鉀鋁礬.md "wikilink")，加以處理，及把經污水處理廠處理的污水排放到[維多利亞港](../Page/維多利亞港.md "wikilink")。近年，政府亦在[文禮閣附近興建了一條約](../Page/文禮閣.md "wikilink")250米長的人工河堤，及挖掘由[香港體育學院至](../Page/香港體育學院.md "wikilink")[文禮閣的一段城門河河床](../Page/文禮閣.md "wikilink")，以減輕該處的污染及異味情況。城門河發出臭味，是因為河道在過去被有機物（禽畜廢物、住宅污水）污染，污染物沉積在河床上並分解，造成水中氧氣減少。雖然城門河近年的污染問題已大為改善，但原生河床底泥中的微生物，在缺氧情況下分解有機物質，會釋放出惡臭的硫化物，這就是臭味的來源。

這些措施成效顯著，城門河的水質逐漸得到改善。自1993年開始，水質指標由普通轉為良好，河裡亦開始發現[魚類及](../Page/鱼.md "wikilink")[無脊椎動物](../Page/无脊椎动物.md "wikilink")。1987年城門河的有機污染物生化需氧量約每天9,000公斤，1997年底，城門河的污水排放量減少接近90%，生化需氧量已降至大約每天1,050公斤。

2015年3月，曾發現一神秘生物。有目擊者認為是[鱷魚](../Page/鱷魚.md "wikilink")、也有認為是俗稱為五爪金龍的[圆鼻巨蜥](../Page/圆鼻巨蜥.md "wikilink")。\[3\]

<File:Shing> Mun River Tai Wai Section
201305.jpg|近[文禮閣的人工河堤](../Page/文禮閣.md "wikilink")
<File:Shing> Mun River Tai Wai Rain Drainage 2017.jpg|雨水渠流入城門河
[File:HK_ShingMunRiver_RainDrainage2.JPG|近](File:HK_ShingMunRiver_RainDrainage2.JPG%7C近)[沙角邨的雨水渠仍可見生活污水排入城門河](../Page/沙角邨.md "wikilink")

## 人工河道

[Shing_Mun_River_View1_201207.jpg](https://zh.wikipedia.org/wiki/File:Shing_Mun_River_View1_201207.jpg "fig:Shing_Mun_River_View1_201207.jpg")\]\]

人工河道包括挖掘[河床及](../Page/河床.md "wikilink")[河堤](../Page/河堤.md "wikilink")，改變河道的闊度、形狀、結構及位置，在河堤和河床表面蓋上[混凝土](../Page/混凝土.md "wikilink")，目的是[防洪](../Page/防洪.md "wikilink")、加快河水排放及方便維修河道等。人工河道會破壞河流為生物提供的棲息地，同時減低河床及河堤的[生物多樣性](../Page/生物多樣性.md "wikilink")。石礫或泥沼等天然河床被蓋上[混凝土](../Page/混凝土.md "wikilink")，加上河道被拉直後，令原來河流中應有的急流淺槽和水流慢的深槽消失，水流變得統一，最後使河流變得不再適合生物居住。

擴闊河道可能要清理河道兩旁的[樹木](../Page/樹木.md "wikilink")，使河堤樹蔭消失，破壞[鳥類和](../Page/鳥類.md "wikilink")[哺乳類動物的棲息地](../Page/哺乳類.md "wikilink")，也使[魚類及河流生物失去天然遮蓋](../Page/魚類.md "wikilink")。混凝土河道散熱和吸熱都較天然河床快，河流的[溫度因此變得不穩定和出現較大的](../Page/溫度.md "wikilink")[溫差](../Page/溫差.md "wikilink")，使河流生物難以適應。

河道維修工程一般包括清理河岸和河床的工作，河床的沈澱物和植物會被挖走，生物的棲息地經常受到破壞，令生態系統難以復甦。河道整治工程施工期間，產生大量污染物影響水質，增加河流的[懸浮物和](../Page/懸浮物.md "wikilink")[沉積物](../Page/沉積物.md "wikilink")。這些沉積物夾雜在礫石之中，[魚卵及很多](../Page/魚卵.md "wikilink")[無脊椎動物便會因缺](../Page/無脊椎動物.md "wikilink")[氧而死亡](../Page/氧.md "wikilink")，對生態系統造成深遠的影響。

## 康樂活動

[Dragon_Boat_Festival_in_Shing_Mun_River_2017.jpg](https://zh.wikipedia.org/wiki/File:Dragon_Boat_Festival_in_Shing_Mun_River_2017.jpg "fig:Dragon_Boat_Festival_in_Shing_Mun_River_2017.jpg")龍舟競賽在翠榕橋至沙燕橋之間一段城門河道舉行\]\]
在水質達到可接受水平後，有關單位於1996年6月在城門河首次舉辦[國際龍舟邀請賽](../Page/國際龍舟邀請賽.md "wikilink")，亦進行過各種划艇及[獨木舟等水上活動](../Page/獨木舟.md "wikilink")。而每年[端午節在沙田區的龍舟競賽亦在翠榕橋至沙燕橋之間一段城門河道舉行](../Page/端午節.md "wikilink")。

在城門河兩岸建有緩跑徑和[單車徑](../Page/單車徑.md "wikilink")，地勢較隔鄰的馬路略低，並種有樹木等植物與馬路分隔。緩跑徑的起點在沙田[香港文化博物館](../Page/香港文化博物館.md "wikilink")，沿西岸經沙田馬場及[吐露港公路連接至](../Page/吐露港公路.md "wikilink")[大埔](../Page/大埔_\(香港\).md "wikilink")，而東岸則連接到馬鞍山一帶。

<File:HK> Shing Mun River Rowing.jpg|城門河上划艇 <File:ShingMunRiver>
joggingtrail 20070828.jpg|城門河畔緩跑徑 <File:HK>
ShatinRowingCentre.JPG|沙田賽艇中心

## 活化工程

2017年，渠務署計劃將城門河上游長達1,900米一段由混凝土改為加入石頭、泥土及有助魚類生存的「魚梯」及進行大量綠化，河道旁能否供市民行走則要再研究。\[4\]

## 沿岸地標

  - [沙田大會堂](../Page/沙田大會堂.md "wikilink")
  - [瀝源橋](../Page/瀝源橋.md "wikilink")
  - [新城市廣場](../Page/新城市廣場.md "wikilink")
  - [沙田公園](../Page/沙田公園.md "wikilink")
  - [沙田馬場](../Page/沙田馬場.md "wikilink")
  - [香港體育學院](../Page/香港體育學院.md "wikilink")
  - [水中天](../Page/水中天.md "wikilink")（前身為明星海鮮舫）
  - [沙田運動場](../Page/沙田運動場.md "wikilink")

<File:MingSingSeafoodRestaurant> 20070828.jpg|小瀝源渠口的明星海鮮舫

## 相關事件

  - [GoBee.Bike推出後不久](../Page/GoBee.Bike.md "wikilink")，已有幾架單車被扔落此處，警方列[刑事毀壞案處理](../Page/刑事毀壞.md "wikilink")。

## 另見

  - [屯門河](../Page/屯門河.md "wikilink")
  - [林村河](../Page/林村河.md "wikilink")
  - [山貝河](../Page/山貝河.md "wikilink")

## 參考資料

  - [中原地圖](../Page/中原地圖.md "wikilink")，網上版

  - 《沙田城門河倡建人工島》，[蘋果日報](../Page/蘋果日報.md "wikilink")，2003年9月8日

  - 《沙田區議會四項工作》，[大公報](../Page/大公報.md "wikilink")，2003年10月30日

## 外部連結

  - [城門河改善工程-香港環境保護署](http://www.epd.gov.hk/epd/tc_chi/news_events/current_issue/shingmunriver.html)
  - [城門河主道及支流環境改善工程](http://www.legco.gov.hk/yr98-99/chinese/panels/ea/papers/p523ca.pdf)
  - [城門河可否成為水上樂園？](http://www.epd.gov.hk/epd/misc/tc_chi/annualrpts/textonly/annual_rpt1997/rpt1997_ch7_3.html)
  - [城門河緩跑徑](http://shingmunriver.com/)

[Category:沙田區](../Category/沙田區.md "wikilink")
[Category:城門谷](../Category/城門谷.md "wikilink")
[Category:香港河流](../Category/香港河流.md "wikilink")

1.  [億元美化城門河初敲定](http://orientaldaily.on.cc/cnt/news/20130202/00176_032.html)
    《東方日報》 2013年2月2日
2.  [大圍明渠變箱型渠
    建足球場](http://orientaldaily.on.cc/cnt/news/20150330/00176_043.html)
    《東方日報》 2015年3月30日
3.  [城門河五爪金龍現身30秒，《太陽報》，2015年3月6日](https://hk.news.yahoo.com/%E5%9F%8E%E9%96%80%E6%B2%B3%E4%BA%94%E7%88%AA%E9%87%91%E9%BE%8D%E7%8F%BE%E8%BA%AB30%E7%A7%92-215020027.html)

4.