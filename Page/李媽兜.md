**李媽兜**（），是一位出身臺南[大內的](../Page/大內庄.md "wikilink")[社會運動參與者及](../Page/社會運動.md "wikilink")[中國共產黨黨員](../Page/共產黨.md "wikilink")，也是[臺灣白色恐怖時期的受難者之一](../Page/臺灣白色恐怖時期.md "wikilink")。

## 生平

1900年，李媽兜出生於[大日本帝國臺灣臺南州](../Page/大日本帝國臺灣.md "wikilink")[曾文郡大內庄](../Page/曾文郡.md "wikilink")[二層行溪畔某](../Page/二層行溪.md "wikilink")[農村](../Page/農村.md "wikilink")。後來，李媽兜的父母在他十三歲時將他送入[公學校就讀](../Page/公學校.md "wikilink")。

李媽兜於十八歲時[畢業於公學校](../Page/畢業.md "wikilink")，並隨後於[善化公學校擔任](../Page/善化公學校.md "wikilink")[教員](../Page/教員.md "wikilink")；因為無法忍受[日籍人士欺侮](../Page/日本人.md "wikilink")[臺籍教員的行徑](../Page/臺灣人.md "wikilink")，李媽兜與日本籍[校長常人旋行發生嚴重](../Page/校長.md "wikilink")[肢體衝突](../Page/暴力.md "wikilink")，並隨即辭去教職。但是，李媽兜隨後又返回善化公學校任職，並再度與教務主任發生[言語及肢體衝突](../Page/言語.md "wikilink")，且雙雙遭[開除](../Page/解僱.md "wikilink")。後來，李媽兜曾任新化糖廠糖業科試驗員、[新化街役場巡視員等職位](../Page/新化街役場.md "wikilink")，但都因無法忍受日臺人員的差別待遇而與他人發生肢體衝突並被迫[辭職](../Page/辭職.md "wikilink")。不久，李媽兜的[妻子黃再美訴請當局准予](../Page/妻子.md "wikilink")[離婚](../Page/離婚.md "wikilink")。

1928年，李媽兜自[澎湖](../Page/澎湖.md "wikilink")[偷渡至](../Page/偷渡.md "wikilink")[廈門白水營圩開設](../Page/廈門.md "wikilink")[西藥房](../Page/藥房.md "wikilink")，並隨後向[國民政府申請變更](../Page/國民政府.md "wikilink")[國籍](../Page/國籍.md "wikilink")。

[中國抗日戰爭爆發後](../Page/中國抗日戰爭.md "wikilink")，[日軍沒多久就進駐廈門](../Page/日軍.md "wikilink")，並查獲李媽兜的真實身分為化籍臺灣人；李媽兜因此逃離當地轉入[臺灣義勇隊](../Page/臺灣義勇隊.md "wikilink")，並負責[敵後情報工作](../Page/敵後.md "wikilink")。

1946年，李媽兜回到臺灣定居於臺南縣大內鄉曲溪村；此後，他曾嘗試開設[肉醬及](../Page/肉醬.md "wikilink")[煉乳](../Page/煉乳.md "wikilink")[工廠](../Page/工廠.md "wikilink")，但都以失敗收場。因此，李媽兜接受友人[陳福星安排至高雄湖內](../Page/陳福星.md "wikilink")[大湖甘仔蜜會社擔任職員](../Page/大湖站_\(高雄捷運\).md "wikilink")。當時，[臺灣長官公署及](../Page/臺灣省行政長官公署.md "wikilink")[國民黨政權的治理導致臺灣](../Page/蔣中正政府.md "wikilink")[人民面臨](../Page/人民.md "wikilink")[經濟及](../Page/經濟.md "wikilink")[生活不安定的情況](../Page/生活.md "wikilink")，他因而察覺到由[階級造成的](../Page/階級.md "wikilink")[弱勢者](../Page/弱势群体.md "wikilink")[壓迫](../Page/壓迫.md "wikilink")。

1946年6月，在陳福星的引介下，李媽兜經[崔志信介紹結識](../Page/崔志信.md "wikilink")[張志忠](../Page/張志忠.md "wikilink")，隨後加入[中國共產黨](../Page/中國共產黨.md "wikilink")。同年10月，李媽兜與陳文山、陳福星等成立[臺南市工委會](../Page/中國共產黨臺南市工作委員會.md "wikilink")，並受[蔡孝乾領導](../Page/蔡孝乾.md "wikilink")；李媽兜自任委員兼[書記](../Page/書記.md "wikilink")，雙陳則負責[宣傳](../Page/宣傳.md "wikilink")、[組織](../Page/組織_\(社會學\).md "wikilink")。\[1\]

後來，雙陳退出，[鄭海樹](../Page/鄭海樹.md "wikilink")、[何川](../Page/何川.md "wikilink")、[何秀吉加入](../Page/何秀吉.md "wikilink")；其人最終皆受牽連遭[當局判處](../Page/蔣中正政府.md "wikilink")[死刑](../Page/死刑.md "wikilink")。

[二二八事件爆發後](../Page/二二八事件.md "wikilink")，李媽兜曾與[張志忠](../Page/張志忠.md "wikilink")、[陳篡地等籌組](../Page/陳篡地.md "wikilink")[台灣自治聯軍](../Page/台灣自治聯軍.md "wikilink")，在[嘉南地區抗擊](../Page/嘉南地區.md "wikilink")[中國國民黨](../Page/中國國民黨.md "wikilink")。1948年5月，李媽兜赴[香港參加](../Page/英屬香港.md "wikilink")「臺灣省工作研究會」，並在回到臺灣後建立26個[中國共產黨台灣省工作委員會支部](../Page/中國共產黨台灣省工作委員會.md "wikilink")。

1950年1月，蔡孝乾被逮捕並「自新」，李媽兜與組織聯繫中斷，隨後與[女友](../Page/女友.md "wikilink")[陳淑端展開逃亡生活](../Page/陳淑端.md "wikilink")。

1952年2月16日，李媽兜在臺南市[安平港擬偷渡赴香港時遭逮](../Page/安平港.md "wikilink")，隨後於1953年7月18日在[臺北川端橋](../Page/臺北.md "wikilink")（後為[中正橋](../Page/中正橋.md "wikilink")）南端刑場被[槍決](../Page/槍決.md "wikilink")。\[2\]\[3\]

## 外部鏈結

  - [李媽兜的革命人生| PNN
    公視新聞議題中心](https://web.archive.org/web/20171017150144/https://pnn.pts.org.tw/main/?s=%E6%9D%8E%E5%AA%BD%E5%85%9C%E7%9A%84%E9%9D%A9%E5%91%BD%E4%BA%BA%E7%94%9F&x=0&y=0)

## 參考資料

  - 2008年，台灣國史館出版《李媽兜案史料彙編》收於「戰後臺灣政治案件」系列，編撰者歐素瑛；ISBN：9789860130478。書中收錄李媽兜及陳淑端案之司法文件原件影印多種，包括:當時警備總部下令追查、緝捕之機密文件；兩人被捕後之偵訊記錄、口供、自白書；法院判決文及行刑命令等。

[Category:中国共产党党员
(1946年入党)](../Category/中国共产党党员_\(1946年入党\).md "wikilink")
[Category:中國共產黨台灣省委員會領導人](../Category/中國共產黨台灣省委員會領導人.md "wikilink")
[L李](../Category/臺灣日治時期社會運動者.md "wikilink")
[M](../Category/李姓.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")
[L李](../Category/被槍決的台灣人.md "wikilink")
[L李](../Category/被中華民國處決者.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Category:臺灣農民運動者](../Category/臺灣農民運動者.md "wikilink")

1.
2.
3.