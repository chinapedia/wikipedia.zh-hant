## 大事记

  - **[中國](../Page/中國.md "wikilink")**
      - [姜維第9次北伐](../Page/姜維北伐.md "wikilink")，與蜀[鎮西將軍](../Page/鎮西將軍.md "wikilink")[胡濟約定在](../Page/胡濟.md "wikilink")[上邽](../Page/上邽.md "wikilink")（今[甘肅](../Page/甘肅.md "wikilink")[天水](../Page/天水.md "wikilink")）會合，率兵出[祁山](../Page/祁山.md "wikilink")，但[胡濟失期未至](../Page/胡濟.md "wikilink")，被曹魏將領[鄧艾於](../Page/鄧艾.md "wikilink")[段谷之戰大敗](../Page/段谷之戰.md "wikilink")。[姜維上表自貶為](../Page/姜維.md "wikilink")[後將軍](../Page/後將軍.md "wikilink")，一樣行使[大將軍權力](../Page/大將軍.md "wikilink")，[曹魏封](../Page/曹魏.md "wikilink")[鄧艾為鎮西將軍](../Page/鄧艾.md "wikilink")、都督隴右諸軍事。
      - [東吳丞相大將軍](../Page/東吳.md "wikilink")[孫峻受](../Page/孫峻.md "wikilink")[文欽慫恿伐魏](../Page/文欽.md "wikilink")，派文欽和呂據、[車騎將軍](../Page/車騎將軍.md "wikilink")[劉纂](../Page/劉纂.md "wikilink")、[鎮南將軍](../Page/鎮南將軍.md "wikilink")[朱異](../Page/朱異.md "wikilink")、[前將軍](../Page/前將軍.md "wikilink")[唐諮率軍從](../Page/唐諮.md "wikilink")[江都進入淮](../Page/江都.md "wikilink")、泗一帶，以便佔領[青州和](../Page/青州_\(古代\).md "wikilink")[徐州](../Page/徐州_\(古代\).md "wikilink")，但不久[孫峻去世](../Page/孫峻.md "wikilink")，吳軍撤退，堂弟[孫綝以侍中](../Page/孫綝.md "wikilink")、武衛將軍、都督中外諸軍事接替[孫峻掌權](../Page/孫峻.md "wikilink")。
      - 東吳將領[呂據與大臣](../Page/呂據.md "wikilink")[滕胤合謀廢孫綝](../Page/滕胤.md "wikilink")，但失敗，皆被誅殺。

<!-- end list -->

  - **[罗马帝国](../Page/罗马帝国.md "wikilink")**
      - [哥特人入侵](../Page/哥特人.md "wikilink")[小亚细亚](../Page/小亚细亚.md "wikilink")。
      - 皇帝[瓦勒良迫害](../Page/瓦勒良.md "wikilink")[基督教](../Page/基督教.md "wikilink")。
      - [法兰克人渡过](../Page/法兰克人.md "wikilink")[莱茵河](../Page/莱茵河.md "wikilink")，[阿勒曼尼人到达](../Page/阿勒曼尼人.md "wikilink")[米兰](../Page/米兰.md "wikilink")。
      - 哥特人出现在[塞萨洛尼卡的城墙边](../Page/塞萨洛尼卡.md "wikilink")。
      - 在[阿非利加行省](../Page/阿非利加_\(羅馬行省\).md "wikilink")，[柏柏尔人屠戮了罗马殖民地](../Page/柏柏尔人.md "wikilink")。
      - 后来的皇帝[奥勒良检阅并组织了莱茵河沿岸的防御](../Page/奥勒良.md "wikilink")。
      - [萨珊波斯的](../Page/萨珊波斯.md "wikilink")[沙普尔一世入侵了](../Page/沙普尔一世.md "wikilink")[美索不达米亚及](../Page/美索不达米亚.md "wikilink")[叙利亚](../Page/叙利亚.md "wikilink")，他攻下并洗劫了[安条克](../Page/安条克.md "wikilink")。
      - [杜拉歐羅普斯为萨珊波斯所摧毁](../Page/杜拉歐羅普斯.md "wikilink")。
      - [安纳托利亚城市](../Page/安纳托利亚.md "wikilink")、[幼发拉底河畔的](../Page/幼发拉底河.md "wikilink")（今土耳其[加济安泰普省境内](../Page/加济安泰普省.md "wikilink")）为沙普尔一世洗劫，不久后该城便连遭大火和地震，最终遭废弃。

<!-- end list -->

  - 其它
      - 一场大[瘟疫从](../Page/瘟疫.md "wikilink")[黑海南岸的](../Page/黑海.md "wikilink")[本都爆发](../Page/本都.md "wikilink")，导致[亚历山大港大量人口丧生](../Page/亚历山大港.md "wikilink")，数千人皈依基督教。

      - [教宗斯德望一世威胁开除](../Page/教宗斯德望一世.md "wikilink")[迦太基主教](../Page/迦太基.md "wikilink")[居普良及其他阿非利加及小亚细亚的](../Page/居普良.md "wikilink")[主教的教籍](../Page/主教.md "wikilink")，除非他们停止为[异端再施洗](../Page/异端.md "wikilink")。居普良发文攻击[教宗](../Page/教宗.md "wikilink")，并在[迦太基会议上得到支持](../Page/迦太基会议.md "wikilink")。他派代表前往[罗马](../Page/罗马.md "wikilink")，导致罗马及迦太基[教会之间几近分裂](../Page/教会.md "wikilink")。

      - 召开。

## 出生

  - [賈南風](../Page/賈南風.md "wikilink")，[西晉](../Page/西晉.md "wikilink")[皇后](../Page/皇后.md "wikilink")。（[300年去世](../Page/300年.md "wikilink")，44岁）
  - [王衍](../Page/王衍_\(西晉\).md "wikilink")，西晉重要官員。（[311年去世](../Page/311年.md "wikilink")，55岁）
  - [司馬覲](../Page/司馬覲.md "wikilink")，西晉宗室，[晉元帝父親](../Page/晉元帝.md "wikilink")。（[290年去世](../Page/290年.md "wikilink")，34岁）
  - [阿利烏](../Page/阿利烏.md "wikilink")，[阿里烏教派创始人](../Page/阿里烏教派.md "wikilink")。（**256年**出生）

## 逝世

  - [孫峻](../Page/孫峻.md "wikilink")，東吳宗室。
  - [呂據](../Page/呂據.md "wikilink")，東吳將領。
  - [滕胤](../Page/滕胤.md "wikilink")，東吳官員。
  - [呂岱](../Page/呂岱.md "wikilink")，東吳重要將領。（[161年出生](../Page/161年.md "wikilink")，95岁）
  - [管輅](../Page/管輅.md "wikilink")，中國[方士](../Page/方士.md "wikilink")，擅長卜筮之術。（[209年出生](../Page/209年.md "wikilink")，47岁）

[\*](../Category/256年.md "wikilink")
[6年](../Category/250年代.md "wikilink")
[5](../Category/3世纪各年.md "wikilink")