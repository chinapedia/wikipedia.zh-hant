[mcitylogo.jpg](https://zh.wikipedia.org/wiki/File:mcitylogo.jpg "fig:mcitylogo.jpg")
[Millennium_city.svg](https://zh.wikipedia.org/wiki/File:Millennium_city.svg "fig:Millennium_city.svg")
**創紀之城**（****）位於[香港](../Page/香港.md "wikilink")[九龍東](../Page/九龍東.md "wikilink")[觀塘區](../Page/觀塘區.md "wikilink")，是[新鴻基地產旗下一系列甲級商業大廈及商場](../Page/新鴻基地產.md "wikilink")。建築群沿[觀塘道興建](../Page/觀塘道.md "wikilink")，橫跨[港鐵](../Page/港鐵.md "wikilink")[觀塘綫](../Page/觀塘綫.md "wikilink")[牛頭角站至](../Page/牛頭角站.md "wikilink")[觀塘站](../Page/觀塘站.md "wikilink")，提供330萬平方呎寫字樓及60萬平方呎商場，是[觀塘這個東九龍工業區中最大型的商業項目](../Page/觀塘.md "wikilink")，現時共分六期（一至三期，五至六期，絲寶國際大廈（第七期））。

## 歷史

創紀之城是[新鴻基地產自](../Page/新鴻基地產.md "wikilink")1989年起在[觀塘區籌建的綜合商業發展項目](../Page/觀塘區.md "wikilink")。在1980年代[觀塘車廠已不敷應用](../Page/九巴觀塘車廠.md "wikilink")，九巴考慮到[九龍灣車廠投入運作後](../Page/九巴九龍灣車廠.md "wikilink")，觀塘車廠之重要性將會大大降低，同時觀塘A廠面積較大及市場內少數可發展地皮，於是九巴開始醞釀車廠重建計劃。至1989年九巴宣布[觀塘車廠北翼重新發展計劃](../Page/九巴觀塘車廠.md "wikilink")，九巴大股東新鴻基地產亦有意參與發展。

[九龍灣車廠在](../Page/九巴九龍灣車廠.md "wikilink")1990年11月啟用，觀塘B廠成為九龍灣廠轄下衛星廠，而A廠於1991年第一季連同行車天橋率先拆卸。隨後新鴻基地產併購觀塘A廠附近舊式工廠大廈，重建為兩座30層甲級辦公大樓，於1998年12月落成，名為「創紀之城」。在落成前一年，時任[渣打銀行首席](../Page/渣打銀行.md "wikilink")[行政總裁杜華](../Page/行政總裁.md "wikilink")（Rana
Talwar）於1997年11月耗資13億港元向新鴻基地產購入第二座15層，及額外租用12層共55萬平方呎樓面連命名權，故第二座名為「渣打中心」（Standard
Chartered
Tower），1999年4月27日正式啟用，2000年7月易名為「[創紀之城一期](../Page/創紀之城一期.md "wikilink")」。

自此新地在區內建立商業王國，版圖日趨壯大。「[創紀之城二期](../Page/創紀之城二期.md "wikilink")」2000年落成時，一度稱為「怡和科技中心」，及後改回原有名稱。森那美汽車服務中心重建而成的「**創紀之城三期**」，位於觀塘道370號，與二期為鄰，2003年中入伙。新地再於2000年4月完成收購一期與毅力工業中心之間的協光工業大廈，納入「**創紀之城六期**」，樓高32層，寫字樓佔其中23層，其餘地下、6及7樓為商場部份，2008年5月陸續入伙。

毗鄰[觀塘站的東九龍廣場](../Page/觀塘站.md "wikilink")，2001年重新發展為「[創紀之城五期](../Page/創紀之城五期.md "wikilink")」，也是眾項目中規模最大。[東亞銀行在](../Page/東亞銀行.md "wikilink")2002年耗資13.3億港元，向發展商購入辦公大樓15層共40.67萬平方呎樓面作為後勤總部連命名權，因此辦公大樓名為「東亞銀行中心」，於2004年11月落成。至於[apm商場](../Page/apm.md "wikilink")2005年3月試業，7月17日正式開幕，並與樓齡相差十年，位於[巧明街](../Page/巧明街.md "wikilink")117號的[港貿中心連接起來](../Page/港貿中心.md "wikilink")，後者電梯大堂亦屬apm管轄範圍。

位處[偉業街](../Page/偉業街.md "wikilink")108號的絲寶國際大廈原為「創紀之城七期」，樓高26層，共提供20層寫字樓及六層停車場。新地於2009年11月發出新聞稿，指這座大廈名為「一號東港」（One
Harbour East）；絲寶集團翌年以10.68億元購入大廈高層半幢（18至28樓）作長線投資，大廈從此剔出創紀之城。

觀塘A廠於1991年清拆後，B廠一直正常運作，至2008年12月1日全面停用。[載通子公司KTRE與新地子公司TRL在](../Page/載通.md "wikilink")2016年8月4日接納地政總署批出之43.05億元補地價，B廠用地正式由工業轉為非工業用途，並於補地價程序完成後在2017年8月拆卸，作為創紀之城項目的延續。

## 現有物業

[Millennium_City_Phase_1.jpg](https://zh.wikipedia.org/wiki/File:Millennium_City_Phase_1.jpg "fig:Millennium_City_Phase_1.jpg")
[Millennium_City_2-3.jpg](https://zh.wikipedia.org/wiki/File:Millennium_City_2-3.jpg "fig:Millennium_City_2-3.jpg")
[Millennium_City_5_2014.jpg](https://zh.wikipedia.org/wiki/File:Millennium_City_5_2014.jpg "fig:Millennium_City_5_2014.jpg")
[Millennium_City_6_(blue_sky).jpg](https://zh.wikipedia.org/wiki/File:Millennium_City_6_\(blue_sky\).jpg "fig:Millennium_City_6_(blue_sky).jpg")

### 創紀之城一期

一期在整個創紀之城發展項目中間，由2座樓高30層的甲級商業大廈組成；其中第2座是[渣打銀行（香港）的後勤總部](../Page/渣打銀行（香港）.md "wikilink")，商廈名為[渣打中心](../Page/渣打中心.md "wikilink")。1998年渣打銀行集團首席[行政總裁](../Page/行政總裁.md "wikilink")[杜華](../Page/杜華.md "wikilink")（Rana
Talwar）表示，該行以13億港元購入渣打中心其中15層，並額外租用12層；而該中心知名租戶是[Levi's](../Page/Levi's.md "wikilink")、[亮視點](../Page/亮視點.md "wikilink")（已遷往第一座7樓708-710室）和山田社集團（Yoki
Magokoro，前稱Yamada
Miyura，已遷往[創紀之城五期](../Page/創紀之城五期.md "wikilink")），以及[工業貿易署的中小企業支援與諮詢中心](../Page/工業貿易署.md "wikilink")（23樓15-18室，已遷往[啟德](../Page/啟德.md "wikilink")[工業貿易大樓](../Page/工業貿易大樓.md "wikilink")）。

另外一座設有寫字樓、餐飲及零售商店，總樓面[建築面積約為](../Page/建築面積.md "wikilink")123萬平方呎。其中特色建築是高40呎的玻璃天幕建於地面大堂中央。

### 創紀之城二期

二期鄰近[牛頭角港鐵站](../Page/牛頭角站.md "wikilink")，由1座樓高28層的商業大廈組成，包括24層寫字樓及4層停車場，總樓面建築面積超過26.6萬平方呎。[怡和科技曾為本期最大的租戶](../Page/怡和科技.md "wikilink")。其他租戶大多以從事電腦、電子科技以及其他與互聯網業務為主，包括[數碼通](../Page/數碼通.md "wikilink")（31樓），亦包括[社會福利署長者生活津貼處理組](../Page/社會福利署.md "wikilink")（12樓）。該大廈前稱**怡和科技中心**，2005年底怡和科技不續租大部分樓面後，大廈名稱回復為創紀之城二期。

### 創紀之城三期

三期鄰近[牛頭角港鐵站](../Page/牛頭角站.md "wikilink")，於2003年6月落成，創紀之城三期是最接近[牛頭角站的商廈](../Page/牛頭角站.md "wikilink")，由1座樓高22層的商業大廈組成，包括19層寫字樓及3層停車場，每層約7,500平方呎，總樓面建築面積超過15.4萬平方呎。設有兩層停車場共60個車位。大廈前身為森那美汽車服務中心。

大堂內以綠色雲石大堂為主色，樓底高度達38呎，而整座大廈則利用玻璃幕牆天然採光。此外，創紀之城3期設有地面行車通道，直達地面大堂，並貫通觀塘道及創業街及設有通道連接二期。

2011年4月6日AEW斥資5億元，購入觀塘創紀之城3期商廈10層，涉及樓面分散高中低層，每層面積約8,140平方呎，合共涉資約5.1億元，呎價約6265元。

2012年4月17日AEW拆售12樓全層，約8140方呎，獲買家斥資6100萬元買入，呎價7494元，而新買家為售賣蘋果產品的專門店[NewVision](../Page/NewVision.md "wikilink")。

2012年8月24日美資基金AEW持有的觀塘創紀之城3期26樓全層，8140方呎，剛以7326萬元易手，呎價達9000元，創區內商廈分層呎價新高。
AEW亦售出22樓全層，成交價7082萬元，呎價8700元。

2012年8月中觀塘創紀之城3期21樓全層，面積約8140方呎，以7000.4萬元易手，呎價8600元，新買家為施永青基金有限公司，原業主為美資基金AEW。

2012年9月施永青再度斥資7081.8萬元，增持觀塘創紀之城3期22樓1號及2號單位，面積約8140方呎，呎價約8700元，原業主為美資基金AEW。

#### 業主

  - AEW持有創紀之城三期9層商廈。
  - [浸會大學持有創紀之城三期](../Page/浸會大學.md "wikilink")7樓及8樓。
  - [循道衞理聯合敎會觀塘堂持有創紀之城三期](../Page/香港基督教循道衛理聯合教會.md "wikilink")6樓。
  - 已知[崇基學院神學院持有創紀之城三期其中一層](../Page/香港中文大學崇基學院神學院.md "wikilink")，作兼讀制證書課程教學中心。

### 創紀之城五期

五期鄰近[觀塘站](../Page/觀塘站.md "wikilink")，是一座購物中心與辦公室混合的建築物，由多個建築部分組成，分別為[apm商場以及](../Page/apm_\(香港\).md "wikilink")[東亞銀行中心](../Page/東亞銀行中心.md "wikilink")。寫字樓於2004年11月落成，而商場則於2005年3月落成試業，並於2005年7月17日正式開幕，而東亞銀行中心亦為邦民日本財務後勤總部。

### 創紀之城六期

六期位於創紀之城1期旁，由一25層寫字樓及2層商場組成。設有地面行車通道，直達地面大堂，並貫通[觀塘道及](../Page/觀塘道.md "wikilink")[巧明街](../Page/巧明街.md "wikilink")。創紀之城6期約有2萬方呎價樓面作商舖用途，主要吸納的租客為聘珍樓，大快活和Pacific
Coffee等。

創紀之城六期的主要租戶包括星展銀行（11-13樓）、德國時尚品牌[Hugo
Boss](../Page/Hugo_Boss.md "wikilink")（佔大樓2層樓面），其他知名租戶包括American Express
Global Business
Travel（16-17樓）、[環球唱片](../Page/環球唱片_\(香港\).md "wikilink")（註：包括與華星唱片聯營的張國榮慈善基金會）（28樓全層）\[1\]、信安信託（亞洲）有限公司
（30樓）、利潔時，以及由傳統商業核心區包括[中環](../Page/中環.md "wikilink")、[灣仔](../Page/灣仔.md "wikilink")、[銅鑼灣及](../Page/銅鑼灣.md "wikilink")[尖沙咀遷移至東九龍的跨國或大型企業](../Page/尖沙咀.md "wikilink")\[2\]，[選舉事務處選舉部部分辦公室](../Page/選舉事務處.md "wikilink")（8樓全層、15樓02室及23樓01-03室）\[3\]、[民政事務總署的觀塘](../Page/民政事務總署.md "wikilink")[民政事務處](../Page/民政事務處.md "wikilink")（21樓）及[觀塘區議會秘書處的辦事處](../Page/觀塘區議會.md "wikilink")（20樓05-07室）、[民眾安全服務隊](../Page/民眾安全服務隊.md "wikilink")（19樓03室）、[醫療輔助隊](../Page/醫療輔助隊.md "wikilink")（19樓05室）、[勞工處](../Page/勞工處.md "wikilink")-職業安全及健康部-行動科的辦事處（19樓06-07室），以及[食物及衞生局轄下的自願醫保計劃辦事處](../Page/食物及衞生局.md "wikilink")（29樓2902室）。

### 絲寶國際大廈

[缩略图](https://zh.wikipedia.org/wiki/File:One_Harbour_East.jpg "fig:缩略图")
**絲寶國際大廈**（**C-Bons International
Center**），前稱一號東港、創紀之城七期，為一座25層高的商業大廈，位於偉業街108號，原址為[1979年落成的](../Page/1979年.md "wikilink")[香港麵粉廠大廈](../Page/香港麵粉廠大廈.md "wikilink")，該廈現已拆卸。項目原計劃興建酒店，但後來更改用途。該物業1樓至5樓為停車場；6樓至28樓為辦公室樓面，每層樓面約16,150至16,830方呎，實用率約八成。大廈在2010年8月開始有商戶進駐。\[4\]同時獲中港企業[絲寶集團購入作長綫投資](../Page/絲寶集團.md "wikilink")，因而獲得命名權。

大廈主要租戶包括王氏國際、手袋名牌Coach及多間本地集團。\[5\]

<File:Millennium> City 7.JPG|創紀之城七期地盤（2007年3月） <File:Millennium> City
Phase 7 200812.jpg|創紀之城七期地盤（2008年12月）

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/牛頭角站.md" title="wikilink">牛頭角站A出口</a>（1-3期、6期）、B6出口（絲寶大廈）、<a href="../Page/觀塘站.md" title="wikilink">觀塘站A</a>2出口（5期）</li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小型巴士.md" title="wikilink">專線小巴</a></dt>

</dl>
<dl>
<dt><a href="../Page/跨境巴士.md" title="wikilink">跨境巴士</a></dt>

</dl>
<ul>
<li><a href="../Page/觀塘24小時跨境快線.md" title="wikilink">觀塘24小時跨境快線</a>
<ul>
<li><a href="../Page/藍田站.md" title="wikilink">藍田站</a>↔︎<a href="../Page/皇崗口岸.md" title="wikilink">皇崗口岸</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [創紀之城官方網站(一)](http://www.millenniumcity.hk/)
  - [創紀之城官方網站(二)](http://www.shkp.com/html/mcity/chinese/index.htm)
  - [創紀之城會](https://web.archive.org/web/20090915192144/http://www.millen.com.hk/)
  - [絲寶國際大廈官方網站](https://web.archive.org/web/20160422074947/http://www.cbic108.com/)

[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:觀塘區寫字樓](../Category/觀塘區寫字樓.md "wikilink")
[Category:觀塘](../Category/觀塘.md "wikilink")
[Category:觀塘道](../Category/觀塘道.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")

1.  <http://www.umg.com.hk/main#>
2.  <http://www.shkp.com.hk/zh-hk/scripts/news/news_press_detail.php?press_id=3674>
3.  <http://tel.directory.gov.hk/0241009521_CHI.html>
4.  <http://www.hkdailynews.com.hk/property.php?id=56855> 新地「一號東港」短期售
    意向呎價近5000元
5.