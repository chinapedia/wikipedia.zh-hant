[缩略图](https://zh.wikipedia.org/wiki/File:Lý_Thái_Tổ_monument_Oct_2010.jpg "fig:缩略图")紀念館\]\]
**北寧省**（）是[越南的一個省](../Page/越南.md "wikilink")，位於[河內東方](../Page/河內.md "wikilink")，區屬[紅河三角洲](../Page/紅河三角洲.md "wikilink")。

## 歷史

北寧省的首府[北寧市](../Page/北寧市_\(越南\).md "wikilink")，在古代曾称为**龙编**，在越南的[第二次北屬時期](../Page/越南第二次北屬時期.md "wikilink")，曾是[交州的州治](../Page/交州.md "wikilink")。

1962年，置[河北省](../Page/河北省_\(越南\).md "wikilink")。1996年，因人口增長問題，舊河北省分割為[北江](../Page/北江省.md "wikilink")、北寧兩省。

## 行政區劃

下轄1市1市社6縣，省莅位於[北寧市](../Page/北寧市_\(越南\).md "wikilink")。

  - [北寧市](../Page/北寧市_\(越南\).md "wikilink")（Thành phố Bắc Ninh）
  - [慈山市社](../Page/慈山市社.md "wikilink")（Thị xã Từ Sơn）
  - [嘉平縣](../Page/嘉平縣.md "wikilink")（Huyện Gia Bình）
  - [良才縣](../Page/良才縣.md "wikilink")（Huyện Lương Tài）
  - [桂武縣](../Page/桂武縣.md "wikilink")（Huyện Quế Võ）
  - [順成縣](../Page/順成縣.md "wikilink")（Huyện Thuận Thành）
  - [仙遊縣](../Page/仙遊縣_\(越南\).md "wikilink")（Huyện Tiên Du）
  - [安丰縣](../Page/安丰縣.md "wikilink")（Huyện Yên Phong）

## 人民

北寧省是越南最小的行政區塊，也是人口密度最高的省份。

## 外部連結

  - [北寧省政府 (越南語)](http://www.bacninh.gov.vn/)

[Category:北寧省](../Category/北寧省.md "wikilink")
[Category:越南省份](../Category/越南省份.md "wikilink")