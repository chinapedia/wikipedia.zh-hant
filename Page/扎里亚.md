[Ahmadu_bello_university_senate.jpg](https://zh.wikipedia.org/wiki/File:Ahmadu_bello_university_senate.jpg "fig:Ahmadu_bello_university_senate.jpg")的非洲第二大学，位于扎里亚的艾哈迈德·贝洛大学\]\]
[Nigeria-karte-politisch-kaduna.png](https://zh.wikipedia.org/wiki/File:Nigeria-karte-politisch-kaduna.png "fig:Nigeria-karte-politisch-kaduna.png")
**扎里亚**（）是[尼日利亚北方](../Page/尼日利亚.md "wikilink")[卡杜纳州的一座城市](../Page/卡杜纳州.md "wikilink")，人口1,018,827人（2007年）\[1\]
。

扎里亚原名扎造，是古代[豪萨人扎造王国的都城](../Page/豪萨人.md "wikilink").\[2\]，大约在1490年代，[伊斯兰教开始传入扎里亚](../Page/伊斯兰教.md "wikilink")，[商业也开始繁荣起来](../Page/商业.md "wikilink")，到处都是[骆驼拉的大车](../Page/骆驼.md "wikilink")，用[盐交换](../Page/盐.md "wikilink")[奴隶和](../Page/奴隶.md "wikilink")[粮食是主要贸易](../Page/粮食.md "wikilink")\[3\]，在15世纪和16世纪期间，扎造王国成为[桑海帝国的附属](../Page/桑海帝国.md "wikilink")，1805年这里被[富拉尼人占领](../Page/富拉尼人.md "wikilink")，1901年，成为[英国的殖民地](../Page/英国.md "wikilink")，直到尼日利亚独立。

扎里亚的[经济主要以](../Page/经济.md "wikilink")[农业为主](../Page/农业.md "wikilink")，种植[高粱和](../Page/高粱.md "wikilink")[小米](../Page/小米.md "wikilink")，此外的作物有[棉花](../Page/棉花.md "wikilink")、[花生和](../Page/花生.md "wikilink")[烟草](../Page/烟草.md "wikilink")\[4\]。

## 注释

## 外部链接

  - [地图](http://www.fallingrain.com/world/NI/0/Zaria.html)

[Z](../Category/尼日利亚城市.md "wikilink")

1.

2.
3.
4.