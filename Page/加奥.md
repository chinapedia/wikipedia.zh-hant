**加奥**（[法语](../Page/法语.md "wikilink")：**Gao**）是位于[马里](../Page/马里.md "wikilink")[尼日尔河畔的一座城市](../Page/尼日尔河.md "wikilink")，[加奧區的首府](../Page/加奧區.md "wikilink")，2009年人口约87,000。

2004年，加奥的[阿斯基亚陵被列入](../Page/阿斯基亚陵.md "wikilink")[联合国教科文组织的](../Page/联合国教科文组织.md "wikilink")[世界遗产名录](../Page/世界遗产名录.md "wikilink")。

2012年3月31日，[阿扎瓦德民族解放运动等叛軍從政府軍手上攻佔加奧](../Page/阿扎瓦德民族解放运动.md "wikilink")。伊斯蘭武裝分子在6月的加奧之戰擊敗阿扎瓦德民族解放運動，奪得加奧的控制權。2013年1月，法國出動戰機轟炸加奧的伊斯蘭武裝分子營地和倉庫。\[1\]

## 參考文獻

[Category:马里城市](../Category/马里城市.md "wikilink")
[Category:尼日河城市](../Category/尼日河城市.md "wikilink")

1.