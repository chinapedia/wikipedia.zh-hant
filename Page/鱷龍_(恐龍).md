**鱷龍**（學名：*Suchosaurus*）意為「[鱷魚](../Page/鱷魚.md "wikilink")[蜥蜴](../Page/蜥蜴.md "wikilink")」，最初被認為是[鱷魚](../Page/鱷魚.md "wikilink")，後來被改歸類於[獸腳亞目](../Page/獸腳亞目.md "wikilink")[棘龍科](../Page/棘龍科.md "wikilink")。化石發現於[英格蘭的](../Page/英格蘭.md "wikilink")[白堊紀地層](../Page/白堊紀.md "wikilink")。[模式標本包含兩個牙齒](../Page/模式標本.md "wikilink")。目前有兩個已命名種：**刀齒鱷龍**（*S.
cultridens*）\[1\]、*S. girardi*\[2\]。

其中的*S. girardi*已在2007年改歸類於[沃克氏重爪龍](../Page/重爪龍.md "wikilink")（*Baryonyx
walkeri*）\[3\]。而刀齒鱷龍的牙齒極度類似同時代的重爪龍，但還是有些微差異。鱷龍可能是[重爪龍的](../Page/重爪龍.md "wikilink")[次同物異名](../Page/次同物異名.md "wikilink")\[4\]。鱷龍的目前狀態是[疑名](../Page/疑名.md "wikilink")\[5\]。

## 參考資料

## 外部連結

  - [*Suchosaurus* in The Dinosaur
    Encyclopaedia](http://www.dinoruss.com/de_4/5cab3a6.htm) at Dino
    Russ's Lair
  - [First post of a long discussion of *Suchosaurus* as a dinosaur and
    its implications](http://dml.cmnh.org/2007Oct/msg00010.html), in the
    Dinosaur Mailing List Archives
  - [*Suchosaurus* as a
    crocodilian](https://www.webcitation.org/query?id=1256562340597290&url=www.geocities.com/dinowight/croc.html),
    at DinoWight

[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:棘龍科](../Category/棘龍科.md "wikilink")

1.  Owen, R. (1840–1845). *Odontography*. London: Hippolyte Bailli
    \`ere, 655p. 1–32.

2.  Sauvage, H. E. (1897–1898). *Vert ́ebr ́es fossiles du Portugal.
    Contribution \`a l’ ́etude des poissons et des reptiles du
    Jurassique et du Cr ́etacique.* Lisbonne: Direction des Travaux g
    ́eologiques du Portugal, 46p.

3.  Buffetaut, E. (2007). "The spinosaurid dinosaur *Baryonyx*
    (Saurischia, Theropoda) in the Early Cretaceous of Portugal."
    *Geological Magazine*, **144**(6): 1021-1025.
    <doi:10.1017/S0016756807003883>

4.
5.  Mateus, O., Araújo, R., Natário, C. & Castanhinha, R. 2011.
    *Zootaxa* **2827**: 54–68.