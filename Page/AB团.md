[Lianjiang_Academy_-_Former_site_of_Pingchuan_High_School.jpg](https://zh.wikipedia.org/wiki/File:Lianjiang_Academy_-_Former_site_of_Pingchuan_High_School.jpg "fig:Lianjiang_Academy_-_Former_site_of_Pingchuan_High_School.jpg")[潋江书院墙壁上的标语](../Page/潋江书院.md "wikilink")“打倒勾结军阀进攻革命的AB团……”\]\]
**AB团**是[中国国民党中反共者于](../Page/中国国民党.md "wikilink")1926年12月在[江西成立的一个团体](../Page/江西.md "wikilink")，AB团的主要工作是在与[共产党争夺江西省国民党的党内权力](../Page/中国共产党.md "wikilink")。

[第一次国共合作时期](../Page/第一次国共合作.md "wikilink")，共产党帮助国民党在江西建立党组织。因此在国民党江西省党部中[共产党员和](../Page/中国共产党员.md "wikilink")[国民党左派占有优势](../Page/国民党左派.md "wikilink")。1926年11月8日，[北伐军攻克](../Page/北伐军.md "wikilink")[南昌](../Page/南昌.md "wikilink")。[蒋介石发现江西的国民党省](../Page/蒋介石.md "wikilink")、市党部，完全由共产党员“把持党务”。于是蒋介石指示国民党[中央特派员](../Page/中央特派员.md "wikilink")[段锡朋组织AB团](../Page/段锡朋.md "wikilink")。与共产党争夺江西省国民党的领导权\[1\]。有说“AB”是英文Anti-Bolševik的缩写，意思是反[布尔什维克](../Page/布尔什维克.md "wikilink")\[2\]。1927年4月2日，发生了针对江西省国民党党部的[四·二暴动](../Page/四·二暴动.md "wikilink")，AB团随后垮台。但其影响并未结束。此后数年，共产党在其党群机关中开展一系列的[反AB团运动](../Page/反AB团.md "wikilink")。

随同国民党退居台湾的[鄭學稼在](../Page/鄭學稼.md "wikilink")《中共富田事變真相》一書中评論道：「我們知道：國民黨人組織的AB團，不是後來所說的Anti-Bolševik，它只存在三個月的時間。[毛澤東利用這個名稱](../Page/毛澤東.md "wikilink")，發動[富田事變](../Page/富田事變.md "wikilink")，[張國燾也利用它](../Page/張國燾.md "wikilink")，發動『[許繼慎派慘案](../Page/白雀园肃反.md "wikilink")』。」

## 主要成员

段锡朋、[周利生](../Page/周利生.md "wikilink")、[程天放](../Page/程天放.md "wikilink")、[洪轨](../Page/洪轨.md "wikilink")、[巫启圣](../Page/巫启圣.md "wikilink")、[曾华英](../Page/曾华英.md "wikilink")、[熊育钖](../Page/熊育钖.md "wikilink")、[王礼锡](../Page/王礼锡.md "wikilink")、[王冠英](../Page/王冠英.md "wikilink")、[罗时实](../Page/罗时实.md "wikilink")、[贺扬灵等](../Page/贺扬灵.md "wikilink")。

## 影响

AB团仅存在三个月，但后来在江西的中国共产党认为有AB团混入了他们内部，进而导致[肃反运动的开展](../Page/肃反运动.md "wikilink")，大量黨員被打成AB團并被处决。1930年5月，在赣西南苏区的党群机关中普遍开展中反AB团的斗争，9月进入高潮。11月反AB团的斗争由地方发展到军队中，12月初地方和军队同时并进。12月12日[红二十军一部分爆发反抗滥捕](../Page/红二十军.md "wikilink")、滥杀AB团的富田事变。中共中央认定“富田事变是AB团领导的反革命暴动”，进一步掀起捕杀AB团的高潮\[3\]。

目前，中共承认绝大多数被处决者是无辜的受害者，所谓的AB团并不存在。根据《红太阳是怎么升起的》一书及党内各种文献，毛和周应对此运动负主要责任。周恩来作为中共中央负责人，主要目的是为了贯彻共产国际“反右倾”的指示，而毛泽东则是为了巩固自己在赣南根据地的权威。但中共至今未对富田事变彻底平反\[4\]。

## 参考文献

## 延伸阅读

  - 王健民，《中國共產黨史稿》
  - 鄭學稼，《中共富田事變真相》
  - 張國燾，《我的回憶》

## 外部連結

  - [許行：江西時期的紅色恐怖](https://web.archive.org/web/20091006062951/http://www.open.com.hk/2006_12p32.htm)，《開放》雜誌2006年第12期

{{-}}

[Category:1926年中国政治](../Category/1926年中国政治.md "wikilink")
[Category:中國國民黨組織](../Category/中國國民黨組織.md "wikilink")
[Category:江西民国时期政治组织](../Category/江西民国时期政治组织.md "wikilink")
[Category:中國國民黨歷史](../Category/中國國民黨歷史.md "wikilink")
[Category:苏区肃反](../Category/苏区肃反.md "wikilink")
[Category:共产主义恐怖主义](../Category/共产主义恐怖主义.md "wikilink")

1.

2.  AB團團員[王禮錫在](../Page/王禮錫.md "wikilink")《讀書雜誌》上發表[佐藤貞一的一封信中附帶提到AB團時說](../Page/佐藤貞一.md "wikilink")：「說到AB團，不過是前五年的一種政治組織，也不過是幾個人的一個小團體。所謂AB團者，原不過是兩重組織，並不是什麼的省寫，後來不久就解散了。」

3.
4.