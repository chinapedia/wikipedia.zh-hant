**YG
Entertainment**是韓國的经纪与娛樂公司（上市日期：2011年11月23日），由[梁鉉錫於](../Page/梁鉉錫.md "wikilink")1996年所創立，當年與[SM娛樂](../Page/SM娛樂.md "wikilink")、[JYP娛樂被稱為](../Page/JYP娛樂.md "wikilink")**韓國三大娛樂公司**。2016年YG娛樂營業額90億新臺幣。已上市分公司YG
PLUS（經營模特，服飾，彩妝，餐飲等副業）2016年銷售額50億新臺幣。

YG是韓國[R\&B和](../Page/R&B.md "wikilink")[Hip
hop音樂最具代表性的娛樂公司](../Page/Hip_hop.md "wikilink")。CEO梁鉉錫曾是1992年出道的[徐太志與孩子們的成员](../Page/徐太志和孩子們.md "wikilink")，该组合被稱為韓國K-pop与Hip-Hop的開山鼻祖。YG旗下藝人擁有龐大人氣，[Youtube共有](../Page/Youtube.md "wikilink")3000萬人追蹤、200億點擊率，[Instagram追蹤者](../Page/Instagram.md "wikilink")7000萬人、[Facebook](../Page/Facebook.md "wikilink")8000萬人。

YG整套造星團隊包括：音樂錄製、發行、藝人包裝等；同時還具有頂尖[製作人](../Page/製作人.md "wikilink")、實力派創作歌手和極具創造力的策劃團隊。YG現旗下藝人有唱片歌手[BIGBANG](../Page/BIGBANG.md "wikilink")、[CL](../Page/李彩麟.md "wikilink")、[Dara](../Page/Dara.md "wikilink")、[李遐怡](../Page/李遐怡.md "wikilink")、[樂童音樂家](../Page/樂童音樂家.md "wikilink")、[WINNER](../Page/WINNER.md "wikilink")、[iKON](../Page/iKON.md "wikilink")、[BLACKPINK](../Page/BLACKPINK.md "wikilink")、[水晶男孩](../Page/水晶男孩.md "wikilink")、[ONE](../Page/鄭帝元.md "wikilink")、[TREASURE
13](../Page/TREASURE_13.md "wikilink")，獨立廠牌的[Zion.T](../Page/Zion.T.md "wikilink")、[hyukoh](../Page/hyukoh.md "wikilink")、等，演員[車勝元](../Page/車勝元.md "wikilink")、[姜棟元](../Page/姜棟元.md "wikilink")、[金喜愛](../Page/金喜愛.md "wikilink")、[崔智友](../Page/崔智友.md "wikilink")、[劉寅娜](../Page/劉寅娜.md "wikilink")、[南柱赫](../Page/南柱赫.md "wikilink")、[李聖經等](../Page/李聖經.md "wikilink")。

## 歷史

### 1996年－2005年：公司成立

1996年，90年代當紅組合[徐太志和孩子們解散後](../Page/徐太志和孩子們.md "wikilink")，其中的一名團員[梁鉉錫](../Page/梁鉉錫.md "wikilink")，在同年的3月創立了「鉉企劃」（、）並在5月推出首個組合Keep
Six。

1997年，變更商號為「MF企劃」（、）並推出組合[Jinusean](../Page/Jinusean.md "wikilink")，出道曲為[梁鉉錫親自作詞作曲的](../Page/梁鉉錫.md "wikilink")《Gasoline》。出道後，以由李鉉道（音譯）作詞作曲，[嚴正化參與主唱的](../Page/嚴正化.md "wikilink")《TELL
ME》，給大眾留下了印象，YG娛樂公司也進入了大眾的視線。

1998年2月，改稱「梁君企劃」（、）推出組合[1TYM出道](../Page/1TYM.md "wikilink")。1999年，發行第一張家族專輯《Famillenium》，同年12月發行了《Y.G.
Best Of Album》。

2001年，商號再更改為[韓國](../Page/韓國.md "wikilink")「YG娛樂有限公司」（、）。2002年，[YG娛樂推出首個女子組合Swi](../Page/YG娛樂.md "wikilink").T，並且開始與R\&B專業的[M.Boat娛樂合作](../Page/M.Boat娛樂.md "wikilink")，推出Solo歌手[輝晟](../Page/輝晟.md "wikilink")、WANTED、BIGMAMA等歌手在YG出道，同年發行第3張家族專輯《2nd
Album : Why Be
Normal？》\[1\]。[M.Boat娛樂所屬歌手有](../Page/M.Boat娛樂.md "wikilink")[Gummy](../Page/Gummy_\(韓國歌手\).md "wikilink")、WANTED、BIG
MAMA等。然而其合作關係於2008年結束。

2003年，Big
Mama，[Gummy](../Page/Gummy_\(歌手\).md "wikilink")，[SE7EN和Lexy出道](../Page/SE7EN.md "wikilink")。同年發行第四張家族專輯《Live
Album : Color Of The Soul Train》。2004年，組合XO出道。2005年， 45RPM，Stony
Skunk，Soulstar出道。

### 2006年－2015年

2006年，YG透過《BIGBANG出道實錄》進行選拔及淘汰賽，最後[BIGBANG以五名成員組成並在](../Page/BIGBANG.md "wikilink")8月19日出道。9月，發行家族專輯《YG
10th》。RED-ROC和首個男女混合組合Moo Ga
Dang出道。2009年，3月，四人新人女團[2NE1與同門師兄](../Page/2NE1.md "wikilink")[BIGBANG合唱LG手機廣告曲](../Page/BIGBANG.md "wikilink")，在5月17日出道並發行數位單曲《FIRE》。2010年，[PSY和YG娛樂簽約](../Page/PSY.md "wikilink")。2011年，YG娛樂和日本最大的唱片公司[AVEX共同創立新公司](../Page/愛貝克思娛樂.md "wikilink")，取名為YGEX。2012年，[Epik
High和YG娛樂簽約](../Page/Epik_High.md "wikilink")。SBS選秀節目[K-pop Star
1的亞軍](../Page/K-pop_Star_1.md "wikilink")[Lee
Hi正式和YG娛樂簽約](../Page/李遐怡.md "wikilink")，在同年的11月以個人歌手身份出道。2012年，[Psy發行了單曲](../Page/Psy.md "wikilink")[江南Style](../Page/江南Style.md "wikilink")，此單曲在網絡上爆紅也成了全球人人皆知的歌曲。《[江南Style](../Page/江南Style.md "wikilink")》也成為了觀看次數最多的[YouTube影片和韓國流行音樂錄影帶觀看次數第一位](../Page/YouTube.md "wikilink")\[2\]。2013年,K-pop
Star第二季的冠軍才氣兄妹組合[樂童音樂家選擇與YG簽約並在](../Page/樂童音樂家.md "wikilink")2014年出道\[3\]。YG娛樂繼《BIGBANG出道實錄》，再次以生存節目《[WIN：WHO
IS
NEXT](../Page/WIN.md "wikilink")》進行選拔淘汰賽\[4\]。這也是繼[BIGBANG之後再次推出的男子組合](../Page/BIGBANG.md "wikilink")。獲勝的隊伍將會以團名[WINNER出道](../Page/WINNER.md "wikilink")。2014年8月17日，[WINNER正式出道](../Page/WINNER.md "wikilink")。同年，生存節目《[MIX
&
MATCH](../Page/MIX_&_MATCH.md "wikilink")》的最後勝利者將會以[iKON身份出道](../Page/iKON.md "wikilink")\[5\]。2015年9月15日，[iKON組合出道](../Page/iKON.md "wikilink")\[6\]。

### 2016年

2016年4月5日，YG娛樂證實[2NE1的](../Page/2NE1.md "wikilink")[Minzy退出](../Page/孔旻智.md "wikilink")[2NE1](../Page/2NE1.md "wikilink")，組合將會以三人式進行活動\[7\]。5月11日，YG與已解散的90年代當紅男子組合[水晶男孩簽約](../Page/水晶男孩.md "wikilink")，再次重啟活動\[8\]。8月8日，繼[2NE1時隔](../Page/2NE1.md "wikilink")7年後而推出的新人女團[BLACKPINK以數碼單曲雙主打出道](../Page/BLACKPINK.md "wikilink")\[9\]。11月25日，YG娛樂宣布[2NE1正式解散](../Page/2NE1.md "wikilink")，因合約到期朴春不續約，CL和Dara則續約。在同日也宣布所屬組合[WINNER成員南太鉉正式退出](../Page/WINNER.md "wikilink")[WINNER](../Page/WINNER.md "wikilink")\[10\]。2017年2月9日[BIGBANG成員](../Page/BIGBANG.md "wikilink")[T.O.P.入伍](../Page/T.O.P..md "wikilink")，於年末「[Last
Dance](../Page/Last_Dance巡迴演唱會.md "wikilink")」巡迴演唱會終場演出後，[G-Dragon於](../Page/G-Dragon.md "wikilink")2月27日\[11\]、太陽於3月12日、大聲於3月13日相繼入伍\[12\]，BIGBANG在2018年正式暫停所有演藝活動。2017年10月29日推出自製選秀綜藝節目《[MIXNINE](../Page/MIXNINE.md "wikilink")》，最終勝出的少年隊TOP9成員，因節目收視率不如預期，YG娛樂5月3日證實《[MIXNINE](../Page/MIXNINE.md "wikilink")》的少年隊取消出道計劃\[13\]。2018年1月25日，[iKON發行了第二張正規專輯](../Page/iKON.md "wikilink")《Return》，其主打歌《LOVE
SCENARIO》橫掃韓國Melon等7大音樂排行榜，連續位居實時榜單一位達41日\[14\]，突破近十年來韓國樂壇史的最長紀錄\[15\]。

### 2017年

2017年，[Naver公司投資](../Page/Naver_\(公司\).md "wikilink")1000億韓元（約為美金8852萬元）入股YG娛樂，成為第二大股東。\[16\]

### 組織

  - 梁珉錫（总裁）
  - [梁鉉錫](../Page/梁鉉錫.md "wikilink")（社長）
  - Jinu（理事）
  - Sean（理事）
  - 金鉉浩（理事，Lucy娱乐集团创办人）
  - [Teddy](../Page/Teddy_Park.md "wikilink")（音樂部長，THE BLACK LABEL创办人）

## 旗下藝人

### 唱片歌手

#### 組合

  - [Jinusean](../Page/Jinusean.md "wikilink")
  - [1TYM](../Page/1TYM.md "wikilink")<small>（暫停活動）</small>
  - [水晶男孩](../Page/水晶男孩.md "wikilink")
  - [BIGBANG](../Page/BIGBANG.md "wikilink")
  - [樂童音樂家](../Page/樂童音樂家.md "wikilink")
  - [WINNER](../Page/WINNER.md "wikilink")
  - [iKON](../Page/iKON.md "wikilink")
  - [BLACKPINK](../Page/BLACKPINK.md "wikilink")
  - [TREASURE 13](../Page/TREASURE_13.md "wikilink")
      - [TREASURE](../Page/Treasure_\(組合\).md "wikilink")
      - [MAGNUM](../Page/Magnum.md "wikilink")\[17\]

#### 企劃組合

  - [GD\&TOP](../Page/GD&TOP.md "wikilink")
  - [GD X TAEYANG](../Page/GD_X_TAEYANG.md "wikilink")
  - [HI秀賢](../Page/HI秀賢.md "wikilink")
  - [MOBB](../Page/MOBB.md "wikilink")

#### 個人歌手

  - [李遐怡](../Page/李遐怡.md "wikilink")
  - [BIGBANG](../Page/BIGBANG.md "wikilink")
      - [太陽](../Page/太陽_\(歌手\).md "wikilink")
      - [G-Dragon](../Page/G-Dragon.md "wikilink")
      - [T.O.P](../Page/T.O.P.md "wikilink")
      - [大聲](../Page/大聲.md "wikilink")
  - [CL](../Page/李彩麟.md "wikilink")
  - [金鉉浩](../Page/金鉉浩.md "wikilink")
  - [Dara](../Page/Dara.md "wikilink")
  - [WINNER](../Page/WINNER.md "wikilink")
      - [YOON](../Page/姜昇潤.md "wikilink")
      - [MINO](../Page/MINO.md "wikilink")
  - [iKON](../Page/iKON.md "wikilink")
      - [BOBBY](../Page/金知元_\(饒舌者\).md "wikilink")
  - [樂童音樂家](../Page/樂童音樂家.md "wikilink")
      - [李秀贤](../Page/樂童音樂家.md "wikilink")
  - [ONE](../Page/鄭帝元.md "wikilink")
  - [BLACKPINK](../Page/BLACKPINK.md "wikilink")
      - [Jennie](../Page/Jennie_\(韓國歌手\).md "wikilink")

#### 製作人

  - \[18\]

  - P.K of FUTURE BOUNCE\[19\]

  - Dee.P of FUTURE BOUNCE\[20\]

  - Uk Jin Kang (姜旭真)

  - 함승천

  - AiRPLAY\[21\]

  - ROVIN\[22\]

  - 趙成確（조성확）\[23\]

  - BIGTONE\[24\]

  - 민연재\[25\]

  - 韓東哲(影視製作人)\[26\]

  - 金鉉浩(김현호)

  - Millennium(崔來星)

### 独立音乐人

#### HIGHGRND

  -
  -
  - [hyukoh](../Page/hyukoh.md "wikilink")

  - [吳赫](../Page/吳赫.md "wikilink") (hyukoh主唱)

  - Punchnello

  - Code Kunst（製作人）

  - millic（製作人）

  - offonoff（製作人）

  - 8（製作人）

#### THE BLACK LABEL

  - [泰迪·朴](../Page/泰迪·朴.md "wikilink")（製作人）\[27\]

  - [Zion.T](../Page/Zion.T.md "wikilink")

  -
  - Bryan Chase

  - [全昭彌](../Page/全昭彌.md "wikilink")

  - Peejay（製作人）

  - Kush（製作人）

  - Seo Wonjin（製作人）

  - DRESS（製作人）

  - Joe Rhee（製作人）

  - R.Tee（製作人）

  - 24（製作人）

  - Cawlr（製作人）

  - Danny Chung（製作人）

#### YGX Entertainment

  - ANDA

### 演員

  - [金喜愛](../Page/金喜愛.md "wikilink")
  - [車勝元](../Page/車勝元.md "wikilink")
  - [崔智友](../Page/崔智友.md "wikilink")
  - [姜棟元](../Page/姜棟元.md "wikilink")
  - [崔勝鉉](../Page/崔勝鉉.md "wikilink")
  - [劉寅娜](../Page/劉寅娜.md "wikilink")
  - [金鉉浩](../Page/金鉉浩.md "wikilink")
  - [李聖經](../Page/李聖經.md "wikilink")
  - [南柱赫](../Page/南柱赫.md "wikilink")
  - [鄭粲右](../Page/鄭粲右.md "wikilink")
  - [金賽綸](../Page/金賽綸.md "wikilink")
  - [葛素媛](../Page/葛素媛.md "wikilink")
  - [林藝真](../Page/林藝真.md "wikilink")
  - [張鉉誠](../Page/張鉉誠.md "wikilink")
  - [鄭慧英](../Page/鄭慧英.md "wikilink")
  - [徐正妍](../Page/徐正妍.md "wikilink")
  - [孫浩俊](../Page/孫浩俊.md "wikilink")
  - [李龍宇](../Page/李龍宇.md "wikilink")
  - [朴山多拉](../Page/Dara.md "wikilink")
  - [姜昇润](../Page/姜昇润.md "wikilink")
  - [金希庭](../Page/金希庭.md "wikilink")
  - [金秦禹](../Page/金秦禹.md "wikilink")
  - [黃勝妍](../Page/黃勝妍.md "wikilink")
  - [景收真](../Page/景收真.md "wikilink")
  - [鄭帝元](../Page/鄭帝元.md "wikilink")
  - [李洙赫](../Page/李洙赫.md "wikilink")
  - [張基龙](../Page/張基龙.md "wikilink")（YGKPlus）
  - [李夏恩](../Page/李夏恩.md "wikilink")（YGKPlus）
  - 裴正南（YGKPlus）
  - 李皓京（YGKPlus）
  - 李賢旭（YGKPlus）
  - 池依秀（YGKPlus）
  - 朴在根（YGKPlus）
  - 姜承贤（YGKPlus）
  - 黃素熙（YGKPlus）
  - 大和孔太（YGEX）
  - 勸休寺保都（YGEX）
  - 飞叶大树（YGEX）
  - 戸奈步美（YGEX）
  - 高田真史帆（YGEX）

### 藝人

  - [朴山多拉](../Page/朴山多拉.md "wikilink")（前[2NE1成員](../Page/2NE1.md "wikilink")）
  - [柳炳宰](../Page/柳炳宰.md "wikilink")
  - [安英美](../Page/安英美.md "wikilink")
  - [權玄彬](../Page/權玄彬.md "wikilink")（[YG K
    Plus](../Page/YG_K_Plus.md "wikilink")）

### 運動員（YG Sports）

  - [孫世恩](../Page/孫世恩.md "wikilink")：職業高爾夫球選手
  - 金孝周：職業高爾夫球選手
  - 李東敏：職業高爾夫球選手
  - Byun Jin-Jae：職業高爾夫球選手
  - Yoo Go-un：職業高爾夫球選手

### 練習生

  - 男練習生

<!-- end list -->

  - [金宗燮](../Page/金宗燮.md "wikilink")\[28\]（[K-pop Star
    6](../Page/K-pop_Star_6.md "wikilink")、[YG寶石盒](../Page/YG寶石盒.md "wikilink")）
  - [王君豪](../Page/王君豪.md "wikilink")（[YG寶石盒](../Page/YG寶石盒.md "wikilink")、[PRODUCE
    X 101](../Page/PRODUCE_X_101.md "wikilink")）
  - Mahiro（[YG寶石盒](../Page/YG寶石盒.md "wikilink")、[PRODUCE X
    101](../Page/PRODUCE_X_101.md "wikilink")）
  - 李仁宏（[YG寶石盒](../Page/YG寶石盒.md "wikilink")）
  - 鄭俊赫（[YG寶石盒](../Page/YG寶石盒.md "wikilink")）
  - 尹施允（[YG寶石盒](../Page/YG寶石盒.md "wikilink")）
  - 金延圭（[YG寶石盒](../Page/YG寶石盒.md "wikilink")）

<!-- end list -->

  - 女練習生

<!-- end list -->

  - 李彩英
  - 林秀雅
  - 韓星\[29\]

## 已離開藝人

  - 個人歌手

<!-- end list -->

  - Keep Six（1996年）

  - XO（2003－2004年）

  - [輝晟](../Page/輝晟.md "wikilink")（2002年－2006年，與[M.Boat娛樂合作](../Page/M.Boat娛樂.md "wikilink")）

  - （2003－2007年）

  - 45RPM（2005－2008年，與Booda Sound合作）

  - Kim Ji-eun（2007－2008年）

  - （2003－2010年）

  - Digital Masta（2003－2011年）

  - [GUMMY](../Page/Gummy_\(韓國歌手\).md "wikilink")（2003-2008，與[M.Boat娛樂合作](../Page/M.Boat娛樂.md "wikilink")，2008-2013年，YG）

  - [SE7EN](../Page/崔東昱.md "wikilink")（2003－2015年）

  - [JC지은](../Page/JC지은.md "wikilink")（2007年5月－2009年9月,YG）

  - [Masta Wu](../Page/Masta_Wu.md "wikilink")（2000－2016年）\[30\]

  - [南太鉉](../Page/南太鉉.md "wikilink")（2014－2016年）\[31\]

  - [PSY](../Page/PSY.md "wikilink")（2010－2018年）\[32\]

  - [姜成勳](../Page/姜成勳.md "wikilink")（2016－2018年）\[33\]

  - [勝利](../Page/勝利_\(歌手\).md "wikilink")（2006－2019年）

<!-- end list -->

  - 演員

<!-- end list -->

  - [姜惠貞](../Page/姜惠貞.md "wikilink")（1997－2013年）
  - [朴韓星](../Page/朴韓星.md "wikilink")
  - [具惠善](../Page/具惠善.md "wikilink")（2003－2017年）\[34\]
  - [鄭釉珍](../Page/鄭釉珍.md "wikilink")（2008－2018年）
  - [史蒂芬妮·李](../Page/史蒂芬妮·李.md "wikilink")（2014－2017年）
  - [李鍾碩](../Page/李鍾碩.md "wikilink")（2016－2018年）\[35\]
  - [高準熹](../Page/高準熹.md "wikilink")（2017－2019年）\[36\]
  - [吳尚津](../Page/吳尚津.md "wikilink")（2017－2019年）\[37\]

<!-- end list -->

  - 組合

<!-- end list -->

  - Swi.T（2002－2005年）

  - （2003－2007年，與[M.Boat娛樂合作](../Page/M.Boat娛樂.md "wikilink")）

  - Wanted（2004－2007年，與[M.Boat娛樂合作](../Page/M.Boat娛樂.md "wikilink")）

  - SoulStaR（2005－2007年，與EunGun合作）

  - [2NE1](../Page/2NE1.md "wikilink")（2009－2017年）

      - [孔旻智](../Page/孔旻智.md "wikilink")（2009－2016年）\[38\]\[39\]
      - [朴春](../Page/朴春.md "wikilink")（2009－2016年）\[40\]

  - [Epik High](../Page/Epik_High.md "wikilink")（2011－2018年）\[41\]

## 昔日練習生

  - 組合

<!-- end list -->

  - [鄭有津](../Page/S#afla.md "wikilink")（[S\#afla](../Page/S#afla.md "wikilink")）
  - [李珠京](../Page/MyB.md "wikilink")（[MyB](../Page/MyB.md "wikilink")）
  - [瑞淵](../Page/李瑞淵.md "wikilink")（[fromis_9](../Page/fromis_9.md "wikilink")）
  - [美延](../Page/曹美延.md "wikilink")（[(G)I-DLE](../Page/\(G\)I-DLE.md "wikilink")）
  - [金宥娜](../Page/金宥娜.md "wikilink")\[42\]（[KHAN](../Page/KHAN.md "wikilink")）
  - [Jun.
    K](../Page/金閔俊.md "wikilink")（[2PM](../Page/2PM.md "wikilink")）
  - [洪碩](../Page/梁洪硕.md "wikilink")（[PENTAGON](../Page/Pentagon_\(男子組合\).md "wikilink")）
  - [Ravn](../Page/金英助.md "wikilink")（[ONEUS](../Page/ONEUS.md "wikilink")）
  - [Lee
    Do](../Page/金建學.md "wikilink")（[ONEUS](../Page/ONEUS.md "wikilink")）
  - [Wow](../Page/金世潤.md "wikilink")（[A.C.E](../Page/A.C.E.md "wikilink")）
  - [全雄](../Page/全雄.md "wikilink")（[AB6IX](../Page/AB6IX.md "wikilink")）
  - [李炳坤](../Page/李炳坤.md "wikilink")（[C9BOYZ](../Page/C9BOYZ.md "wikilink")）
  - [金昇勳](../Page/金昇勳.md "wikilink")（[C9BOYZ](../Page/C9BOYZ.md "wikilink")）

<!-- end list -->

  - SOLO

<!-- end list -->

  - [張賢勝](../Page/張賢勝.md "wikilink")
  - [鄭鎮馨](../Page/鄭鎮馨.md "wikilink")
  - [凱蒂·金](../Page/凱蒂·金.md "wikilink")\[43\]
  - [柳珠伊](../Page/Cosmic_Girl.md "wikilink")\[44\]
  - [金保亨](../Page/金保亨.md "wikilink")
  - [金常彬](../Page/金常彬.md "wikilink")（藝名：BIL）

<!-- end list -->

  - 演員

<!-- end list -->

  - [李荷妮](../Page/李荷妮.md "wikilink")
  - Linzy

<!-- end list -->

  - 選秀節目

<!-- end list -->

  - 《[少年24](../Page/BOYS24.md "wikilink")》
      - 李滄珉
      - [金成賢](../Page/金成賢.md "wikilink")（[IN2IT](../Page/IN2IT.md "wikilink")）
      - 吳振碩
  - 《[PRODUCE 101 S2](../Page/PRODUCE_101_\(第二季\).md "wikilink")》
      - 金常彬
  - 《[PRODUCE 48](../Page/PRODUCE_48.md "wikilink")》
      - 朴珍熙 Jinny Park
      - 朴書英 Park Seo Young
  - 《[YG寶石盒](../Page/YG寶石盒.md "wikilink")》
      - [李炳坤](../Page/李炳坤.md "wikilink")（《[MIXNINE](../Page/MIXNINE.md "wikilink")》、[C9BOYZ](../Page/C9BOYZ.md "wikilink")）
      - [金昇勳](../Page/金昇勳.md "wikilink")（《[Stray
        Kids](../Page/Stray_Kids_\(電視節目\).md "wikilink")》、[C9BOYZ](../Page/C9BOYZ.md "wikilink")）
      - 李彌談（《[Stray
        Kids](../Page/Stray_Kids_\(電視節目\).md "wikilink")》)、（[PRODUCE
        X 101](../Page/PRODUCE_X_101.md "wikilink")）
      - 姜碩和（《[PRODUCE X 101](../Page/PRODUCE_X_101.md "wikilink")》）
      - 金成淵（[MIXNINE](../Page/MIXNINE.md "wikilink")、《[PRODUCE X
        101](../Page/PRODUCE_X_101.md "wikilink")》）
      - 張尹瑞
      - 吉道煥
      - Kotaro
      - Keita

<!-- end list -->

  - 練習生

<!-- end list -->

  - [朴賢鎮](../Page/朴賢鎮.md "wikilink")
  - Denise Kim
  - [文秀雅](../Page/文秀雅.md "wikilink")
  - 張漢娜
  - Noa
  - [金恩菲](../Page/金恩菲.md "wikilink")\[45\]

## 旗下品牌

  - Moonshot (化妝品)
  - YG REPUBLIQUE (餐飲)

## 影視製作

### 選秀節目

  - 2013年
      - [WIN: Who is next?](../Page/WIN.md "wikilink")
  - 2014年
      - [MIX & MATCH](../Page/MIX_&_MATCH.md "wikilink")
  - 2017年
      - [MIX NINE](../Page/MIX_NINE.md "wikilink")
  - 2018年
      - [YG寶石盒](../Page/YG寶石盒.md "wikilink")

### 綜藝節目

  - 2016年
      - [花樣旅行](../Page/花樣旅行.md "wikilink")
  - 2017年
      - 水晶男孩 無本質青春旅行
      - [違反校規的修學旅行](../Page/違反校規的修學旅行.md "wikilink")
  - 2018年
      - [善良地活吧](../Page/善良地活吧.md "wikilink")\[46\]
      - [YG戰略資料室](../Page/YG戰略資料室.md "wikilink")\[47\]

### 電視劇

  - 2016年
      - [月之戀人－步步驚心：麗](../Page/月之戀人－步步驚心：麗.md "wikilink")
  - 2018年
      - [心動警報](../Page/心動警報.md "wikilink")

## YG Family家族專輯

<table>
<thead>
<tr class="header">
<th><p>專輯</p></th>
<th><p>專輯資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1st</strong></p></td>
<td><p>《<strong>Famillenium</strong>》</p>
<ul>
<li>發行日期：1999年7月29日</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2nd</strong></p></td>
<td><p>《<strong>Y.G. Best Of Album</strong>》</p>
<ul>
<li>發行日期：1999年12月4日</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>3rd</strong></p></td>
<td><p>《<strong>2nd Album : Why Be Normal？</strong>》</p>
<ul>
<li>發行日期：2002年10月24日</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>4th</strong></p></td>
<td><p>《<strong>Live Album : Color Of The Soul Train</strong>》</p>
<ul>
<li>發行日期：2003年12月17日</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>5th</strong></p></td>
<td><p>《<strong>YG 10th</strong>》</p>
<ul>
<li>發行日期：2006年9月1日</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>6th</strong></p></td>
<td><p>《<strong>2011 YG FAMILY CONCERT LIVE CD</strong>》</p>
<ul>
<li>發行日期：2012年4月18日</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>7th</strong></p></td>
<td><p>《<strong>2014 YG FAMILY CONCERT in SEOUL LIVE CD</strong>》</p>
<ul>
<li>發行日期：2014年11月28日</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

## 華人翻唱

<table>
<tbody>
<tr class="odd">
<td><p><strong>翻唱</strong></p></td>
<td><p><strong>翻唱曲名</strong></p></td>
<td><p><strong>原唱</strong></p></td>
<td><p><strong>原曲歌名</strong></p></td>
<td><p><strong>作曲</strong></p></td>
<td><p><strong>编曲</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅志祥.md" title="wikilink">羅志祥</a></p></td>
<td><p>Show Time</p></td>
<td><p><a href="../Page/1TYM.md" title="wikilink">1TYM</a></p></td>
<td><p>Nasty (2001)</p></td>
<td><p><a href="../Page/Teddy_Park.md" title="wikilink">Teddy</a> (朴洪俊)</p></td>
<td><p><a href="../Page/Teddy_Park.md" title="wikilink">Teddy</a></p></td>
<td><p>罗志祥出道曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/潘瑋柏.md" title="wikilink">潘瑋柏</a></p></td>
<td><p>壁虎漫步</p></td>
<td><p><a href="../Page/1TYM.md" title="wikilink">1TYM</a></p></td>
<td><p>Mother (2001)</p></td>
<td><p>Teddy</p></td>
<td><p>Teddy</p></td>
<td><p>潘瑋柏出道曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘瑋柏.md" title="wikilink">潘瑋柏</a>, <a href="../Page/許慧欣.md" title="wikilink">許慧欣</a></p></td>
<td><p>Tell Me</p></td>
<td><p>Jinusean ft. <a href="../Page/嚴正化.md" title="wikilink">嚴正化</a></p></td>
<td><p>Tell Me (1997)</p></td>
<td><p>Jinusean, Lee Hyundo</p></td>
<td><p>Lee Hyundo</p></td>
<td><p>YG演唱會ft.<a href="../Page/Dara.md" title="wikilink">Dara</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/潘瑋柏.md" title="wikilink">潘瑋柏</a></p></td>
<td><p>Good Love</p></td>
<td><p><a href="../Page/1TYM.md" title="wikilink">1TYM</a></p></td>
<td><p>Good Love (1998)</p></td>
<td><p>Perry, Baekkyoung</p></td>
<td><p>Perry</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘瑋柏.md" title="wikilink">潘瑋柏</a></p></td>
<td><p>I Like You Like That</p></td>
<td><p><a href="../Page/SE7EN.md" title="wikilink">SE7EN</a></p></td>
<td><p>I Like You Like That (2003)</p></td>
<td><p>Perry</p></td>
<td><p>Perry</p></td>
<td><p>SE7EN 2013年满约</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陈冠希.md" title="wikilink">陳冠希</a></p></td>
<td><p>Make It Last</p></td>
<td><p><a href="../Page/1TYM.md" title="wikilink">1TYM</a></p></td>
<td><p>Make It Last (2001)</p></td>
<td><p>Teddy</p></td>
<td><p>Teddy</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陈冠希.md" title="wikilink">陳冠希</a></p></td>
<td><p>夏夜神話</p></td>
<td><p><a href="../Page/輝星.md" title="wikilink">輝星</a> ft. Masta Wu</p></td>
<td><p>噩梦 (2002)</p></td>
<td><p>Teddy, Jeon Seungwoo</p></td>
<td><p>Yoon Seunghwan</p></td>
<td><p>輝星2006年滿約</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Energy.md" title="wikilink">Energy</a></p></td>
<td><p>One Time</p></td>
<td><p><a href="../Page/1TYM.md" title="wikilink">1TYM</a></p></td>
<td><p>1TYM (1998)</p></td>
<td><p>Perry</p></td>
<td><p>Perry</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Energy.md" title="wikilink">Energy</a></p></td>
<td><p>某年某月某一天</p></td>
<td><p><a href="../Page/SE7EN.md" title="wikilink">SE7EN</a></p></td>
<td><p>Come Back To Me (2003)</p></td>
<td><p>Yoon Seunghwan</p></td>
<td><p>Yoon Seunghwan</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾漢良.md" title="wikilink">鍾漢良</a></p></td>
<td><p>獨角獸</p></td>
<td><p>Swi.T</p></td>
<td><p>I'll Be There (2002)</p></td>
<td><p>Perry, <a href="../Page/梁鉉錫.md" title="wikilink">梁鉉錫</a></p></td>
<td><p>Perry</p></td>
<td><p>Swi.T成员李恩珠和梁鉉錫结婚</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王心凌.md" title="wikilink">王心凌</a></p></td>
<td><p>心心相印</p></td>
<td><p>Swi.T</p></td>
<td><p>너와난 하난거야 (2002)</p></td>
<td><p>Baekkyoung (宋栢京)</p></td>
<td><p>Baekkyoung</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溫嵐.md" title="wikilink">溫嵐</a></p></td>
<td><p>D.I.S.C.O</p></td>
<td><p><a href="../Page/嚴正化.md" title="wikilink">嚴正化</a> ft. <a href="../Page/T.O.P..md" title="wikilink">T.O.P.</a></p></td>
<td><p>D.I.S.C.O (2008)</p></td>
<td><p>Teddy, Kush</p></td>
<td><p>Teddy</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/A-Lin.md" title="wikilink">A-Lin</a></p></td>
<td><p>愛請問怎麽走</p></td>
<td><p>Gummy</p></td>
<td><p>Forget Me Now (2004)</p></td>
<td><p>Lee Yonghyun</p></td>
<td><p>Kim Jaesuk</p></td>
<td><p>Gummy 2013年满约</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/A-Lin.md" title="wikilink">A-Lin</a></p></td>
<td><p>我还是不懂</p></td>
<td><p>Gummy</p></td>
<td><p>Memory Loss (2004)</p></td>
<td><p>Kim Dohoon</p></td>
<td><p>Kim Dohoon</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>黒棒</p></td>
<td><p>散</p></td>
<td><p><a href="../Page/1TYM.md" title="wikilink">1TYM</a></p></td>
<td><p>One Love (2000)</p></td>
<td><p>Teddy</p></td>
<td><p>Teddy, Kim Jongsoo</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>黑棒</p></td>
<td><p>哎呦</p></td>
<td><p>Jinusean</p></td>
<td><p>A-Yo (2001)</p></td>
<td><p>Teddy</p></td>
<td><p>Teddy</p></td>
<td><p><a href="../Page/太陽_(歌手).md" title="wikilink">太陽</a>12歲出演MV<br />
<a href="../Page/G-Dragon.md" title="wikilink">G-Dragon</a>12歲演唱Remix</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何炅.md" title="wikilink">何炅</a></p></td>
<td><p>无形的手</p></td>
<td><p><a href="../Page/SE7EN.md" title="wikilink">SE7EN</a></p></td>
<td><p>Even I Can't Miss You (2006)</p></td>
<td></td>
<td></td>
<td><p>Smile Again原聲帶</p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  -

  -

  -

  -

  -

  - [YGEX日本官方網站|YGEX日本](http://ygex.jp/)

  -

  - [优酷网上的](../Page/优酷网.md "wikilink")[YG娛樂|YG娛樂](http://tvs.youku.com/ygfamily)
    頻道

[Category:YG娛樂](../Category/YG娛樂.md "wikilink")
[Category:韓國證券交易所上市公司](../Category/韓國證券交易所上市公司.md "wikilink")
[Category:韓國藝人經紀公司](../Category/韓國藝人經紀公司.md "wikilink")
[Category:騰訊](../Category/騰訊.md "wikilink")
[Category:1996年韓國建立](../Category/1996年韓國建立.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.