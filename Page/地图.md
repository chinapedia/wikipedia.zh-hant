[World_Map_1689.JPG](https://zh.wikipedia.org/wiki/File:World_Map_1689.JPG "fig:World_Map_1689.JPG")繪製的一幅世界地圖\]\]
**地图**，是根据一定的[数学法则](../Page/数学.md "wikilink")，例如[墨卡托投影法](../Page/麥卡托投影法.md "wikilink")，将[地球或其他](../Page/地球.md "wikilink")[星球的自然现象和社会现象通过概括和符号缩绘在](../Page/星球.md "wikilink")[平面上的图形](../Page/平面.md "wikilink")。按照統一的設計和要求編制的多幅地圖的彙集被稱作“地圖集”或者“地圖冊”。

## 历史

[Hereford_Mappa_Mundi_1300.jpg](https://zh.wikipedia.org/wiki/File:Hereford_Mappa_Mundi_1300.jpg "fig:Hereford_Mappa_Mundi_1300.jpg")
自从[石器时代](../Page/石器时代.md "wikilink")，人类开始制作地图。其中最早的地图在公元前6200年于[土耳其地区出现](../Page/土耳其.md "wikilink")\[1\]。古[埃及人使用](../Page/埃及.md "wikilink")[蘆葦繪製地圖](../Page/蘆葦.md "wikilink")，但[蘆葦不易保存](../Page/蘆葦.md "wikilink")，故存世的古[埃及地圖很少](../Page/埃及.md "wikilink")。[中國的地圖最早見於](../Page/中國.md "wikilink")2400年前的[战国](../Page/战国.md "wikilink")。\[2\]根据实物考古发现中国的最早的地图，是[湖南](../Page/湖南省.md "wikilink")[长沙](../Page/长沙市.md "wikilink")[马王堆三号汉墓中](../Page/马王堆汉墓.md "wikilink")，三幅绘在[帛上的地图](../Page/帛.md "wikilink")，绘制年代大约在[西汉时期公元前](../Page/西汉.md "wikilink")168年。在[中國的出土最早的](../Page/中國.md "wikilink")[紙上繪製的就是一幅戰場地圖](../Page/紙.md "wikilink")。1895年在[俄罗斯](../Page/俄罗斯.md "wikilink")[迈科普古墓出土的银壶上的线刻地图](../Page/迈科普古墓.md "wikilink")，于公元前3000年制作。两条[河流相汇成](../Page/河流.md "wikilink")[湖](../Page/湖.md "wikilink")，[山地](../Page/山地.md "wikilink")、[树木](../Page/树木.md "wikilink")、鸟兽皆为侧面图。颇为写实的把[高加索山脉的轮廓描绘出来](../Page/高加索.md "wikilink")。古[希臘在地圖方面作出了許多貢獻](../Page/希臘.md "wikilink")。[埃拉托斯特尼首先推算出了](../Page/埃拉托斯特尼.md "wikilink")[地球的大小以及](../Page/地球.md "wikilink")[子午圈的長度](../Page/經綫圈.md "wikilink")，繪製出默認[地球為](../Page/地球.md "wikilink")[球體的地圖](../Page/球體.md "wikilink")。[喜帕恰斯創立](../Page/喜帕恰斯.md "wikilink")[投影法](../Page/地图投影.md "wikilink")，提出將地球圓周劃分360°。[托勒密於公元](../Page/托勒密.md "wikilink")2世紀編纂的《地理學指南》詳細叙述了地圖繪製的方法，創立了更多新的[投影法](../Page/地图投影.md "wikilink")。該書附地圖27張（1張世界地圖，26張分區地圖），認為是世界最早的地圖集之雛形。\[3\]中國[西晋時期的](../Page/西晋.md "wikilink")[地圖學家](../Page/地圖學.md "wikilink")[裴秀發明了](../Page/裴秀.md "wikilink")“[製圖六體](../Page/製圖六體.md "wikilink")”，開創了東方精確製圖的先河。
[Vinland_Map_HiRes.jpg](https://zh.wikipedia.org/wiki/File:Vinland_Map_HiRes.jpg "fig:Vinland_Map_HiRes.jpg")
在现今世代，我们倾向觉得地图是理性及[科学的产物](../Page/科学.md "wikilink")，其实地图本身也有神秘的一面。古代地图，尤其是在未知的领域中，常常跟非科学的[宇宙观结合来表达](../Page/宇宙观.md "wikilink")[人于](../Page/人.md "wikilink")[宇宙的关系](../Page/宇宙.md "wikilink")。例如：[中世纪的](../Page/中世纪.md "wikilink")「[TO地图](../Page/TO地图.md "wikilink")」把[耶路撒冷描绘成](../Page/耶路撒冷.md "wikilink")[世界的中心](../Page/世界.md "wikilink")，把[地球当作](../Page/地球.md "wikilink")[耶稣的躯体](../Page/耶稣.md "wikilink")。此時的地圖沒有經緯網和比例尺，科學實用價值喪失。相反地，[地中海人的航海地图却是非常准确](../Page/地中海.md "wikilink")。1568年，[荷蘭製圖學者](../Page/荷蘭.md "wikilink")[墨卡托創立了](../Page/墨卡托.md "wikilink")[正軸等角圓柱投影](../Page/正軸等角圓柱投影.md "wikilink")。這個投影將[等角航綫繪製為直綫](../Page/等角航綫.md "wikilink")，因此至今還在[海圖製圖時使用](../Page/海圖.md "wikilink")。

隨著[地理大發現的開始](../Page/地理大發現.md "wikilink")，各種[行業都對精確的地圖産生需求](../Page/行業.md "wikilink")，當時使用[三角測量繪製精准地圖很風行](../Page/三角測量.md "wikilink")。到了18世紀，很多國家開始繪製詳細的軍用地圖。19世紀末，各國出于經濟利益的需要，開始編繪國際統一規格的詳細地圖。此時，[自然科學發展起來](../Page/自然科學.md "wikilink")，出現了描述[氣候](../Page/氣候.md "wikilink")、[土壤](../Page/土壤.md "wikilink")、[水文等自然專題的](../Page/水文.md "wikilink")[專題地圖](../Page/專題地圖.md "wikilink")。20世紀時，[飛機發明後](../Page/飛機.md "wikilink")，[航空測繪地圖興起](../Page/航空.md "wikilink")。

## 构成要素

[Vatican_City_map_EN.png](https://zh.wikipedia.org/wiki/File:Vatican_City_map_EN.png "fig:Vatican_City_map_EN.png")城的[開放街圖](../Page/開放街圖.md "wikilink")\]\]
地圖大多以可視的圖形形式出現。地圖的載體多樣，[紙和](../Page/紙.md "wikilink")[螢幕最常見](../Page/螢幕.md "wikilink")。而所有地圖及其介質的共同構成要素是**圖形要素**、**數學要素**、**輔助要素**以及**補充說明**。\[4\]

  - **圖形要素**
    圖形是地圖的主體。把[自然](../Page/自然.md "wikilink")、[社會](../Page/社會.md "wikilink")[經濟等需要表示的現象通過地圖符號的表示](../Page/經濟.md "wikilink")，從而形成圖形要素。
  - **數學要素**
    [數學是保證地圖可量和可比的基礎](../Page/數學.md "wikilink")。數學要素裏包括[地圖投影](../Page/地圖投影.md "wikilink")、[地圖坐標系統](../Page/地圖坐標系統.md "wikilink")、[比例尺和](../Page/比例尺.md "wikilink")。
  - **輔助要素**
    輔助要素包括地圖名稱、[圖例](../Page/圖例.md "wikilink")、地圖編號、作者、繪製時間、地圖參數等。這是保證地圖完整的重要部分。
  - **補充說明**
    使用統計圖表、[剖面圖](../Page/剖面圖.md "wikilink")、[照片](../Page/照片.md "wikilink")、[文字等形式補充地圖內容的說明](../Page/文字.md "wikilink")。

## 分类

[Ocean_gravity_map.gif](https://zh.wikipedia.org/wiki/File:Ocean_gravity_map.gif "fig:Ocean_gravity_map.gif"))\]\]
隨著地圖的不斷發展，其數量和品種日益增多，為更加有效的管理與使用，地圖分類的重要性就凸顯出來。地圖的分類方法有很多。包括按分類標志劃分、[普通地圖分類](../Page/普通地圖.md "wikilink")、[專題地圖分類](../Page/專題地圖.md "wikilink")、系列地圖分類和地圖集分類。\[5\]

  - 按分類標志劃分

<!-- end list -->

1.  按**內容**劃分為[普通地圖和](../Page/普通地圖.md "wikilink")[專題地圖](../Page/專題地圖.md "wikilink")
2.  按**[比例尺](../Page/比例尺.md "wikilink")**劃分為大比例尺地圖、中比例尺地圖和小比例尺地圖。其中，大、中、小是相對而言的。
3.  按**製圖區域**劃分為自然區劃、政治行政區劃和經濟區劃。
4.  按**用途**劃分為通用地圖和專用地圖。
5.  按**使用方式**
6.  按**其他方式**

<!-- end list -->

  - 普通地圖分類：以相对平衡的详细程度综合地反映制图区域内各种自然和社会经济现象一般特征的地图。包括地形、水系、居民地、交通线、境界、土质与植被等内容，如街道图，城市交通图，城市旅游图，行政区划图，国道交通图，地形图等
  - 專題地圖分類
  - 地圖集分類

## 相關學科

  - [地圖學](../Page/地圖學.md "wikilink")
  - [地理學](../Page/地理學.md "wikilink")
  - [測量學](../Page/測量學.md "wikilink")
  - [測繪學](../Page/測繪學.md "wikilink")
  - [色彩學](../Page/色彩學.md "wikilink")
  - [美學](../Page/美學.md "wikilink")
  - [數學](../Page/數學.md "wikilink")
  - [遙感技術](../Page/遙感技術.md "wikilink")
  - [計算機技術](../Page/計算機技術.md "wikilink")
  - [地理信息學](../Page/地理信息學.md "wikilink")

## 相關條目

  - [电子地图服务](../Page/电子地图服务.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

[地图](../Category/地图.md "wikilink")
[Category:地图学](../Category/地图学.md "wikilink")
[Category:测绘学](../Category/测绘学.md "wikilink")

1.  Miles Harvey，*The Island of Lost Maps: A True Story of Cartographic
    Crime*。纽约：Random House，2000 ISBN 0-7679-0826-0，cited above；also ISBN
    0-375-50151-7
2.  Hsu, Mei-ling. "The Qin Maps: A Clue to Later Chinese Cartographic
    Development," Imago Mundi (Volume 45, 1993): 90–100.
3.  祝国瑞等，《地图设计与编绘》。武汉：武汉大学出版社，2001年：14。ISBN 7-307-03285-6
4.  蔡孟裔等，《新編地圖學教程》。北京：高等教育出版社，2000年：6-7。ISBN 7-04-007263-7
5.  祝国瑞等，《地图设计与编绘》。武汉：武汉大学出版社，2001年：3-4。ISBN 7-307-03285-6