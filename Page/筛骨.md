**筛骨**(ethmoid
bone)组成[颅骨的](../Page/颅骨.md "wikilink")29块[骨骼之一](../Page/骨骼.md "wikilink")。共一块。位于两眶之间，额骨的下方，被其他骨骼包围着，无法直接看到。其后方为[蝶骨](../Page/蝶骨.md "wikilink")。外观为为**巾**字形，分为筛板、垂直板和筛骨迷路三部。

## 圖片

`Image:Gray149.png|從上方觀察的篩骨。 `
`Image:Gray150.png|篩骨`[`垂直版`](../Page/垂直版.md "wikilink")`。`
`Image:Gray151.png|篩骨的後視圖。`
`Image:Gray152.png|篩骨的右側視圖。`
`Image:Gray164.png|在`[`眼窩內部中央的篩骨顯而易見`](../Page/眼窩.md "wikilink")`。`
`Image:Gray188.png|`[`頭顱的側視圖`](../Page/頭顱.md "wikilink")`。`
`Image:Gray190.png|`[`頭顱的前視圖`](../Page/頭顱.md "wikilink")`。`
`Image:Gray192.png|左側`[`眼窩的內側壁`](../Page/眼窩.md "wikilink")`。`
`Image:Gray193.png|`[`頭顱的底部`](../Page/頭顱.md "wikilink")`。上面。 `
`Image:Gray195.png|左側`[`鼻窩`](../Page/鼻窩.md "wikilink")`(鼻前庭)的內側壁。`
`Image:Gray196.png|左側`[`鼻腔的側邊`](../Page/鼻腔.md "wikilink")`、頂部和底部。`
`Image:Gray153.png |`[`鼻腔的側邊顯示著篩骨的位置`](../Page/鼻腔.md "wikilink")`。 `
`Image:Siebbein1.jpg|篩骨。`
<File:Nasal>` cavity - anterior view.jpg|`[`垂直版`](../Page/垂直版.md "wikilink")`。`

## 参见

  - [人体骨骼列表](../Page/人体骨骼列表.md "wikilink")

## 外部連結

  - [解剖學(Anatomy)- ethmoid
    bone(篩骨)](http://smallcollation.blogspot.com/2013/01/anatomy-ethmoid-bone.html)

[Category:頭頸骨](../Category/頭頸骨.md "wikilink")