[角樓.JPG](https://zh.wikipedia.org/wiki/File:角樓.JPG "fig:角樓.JPG")角楼\]\]
[中卫鼓楼3.JPG](https://zh.wikipedia.org/wiki/File:中卫鼓楼3.JPG "fig:中卫鼓楼3.JPG")

**十字脊顶**是[中国傳統建筑的一种](../Page/中国.md "wikilink")[屋顶形式](../Page/屋顶.md "wikilink")，是十字組屋的特殊形式，可以是[硬山或](../Page/硬山顶.md "wikilink")[悬山式](../Page/悬山式.md "wikilink")，也可以是[歇山式](../Page/歇山式.md "wikilink")。

《[清明上河图](../Page/清明上河图.md "wikilink")》中就有很多民居采用十字脊顶，其[山面多采用](../Page/山面.md "wikilink")[悬山顶](../Page/悬山顶.md "wikilink")。后来[宋代的](../Page/宋代.md "wikilink")《[金明池夺标图](../Page/金明池夺标图.md "wikilink")》就出现了歇山式的十字脊顶，即四个山面都是歇山式，故又称“四面歇山顶”。硬山、懸山式的十字脊頂建築現已少見，不過2013年重建的[聊城](../Page/聊城市.md "wikilink")[東昌府古城中存在大量的硬山十字脊頂磚屋民居](../Page/東昌府區.md "wikilink")，及不少歇山十字脊頂建築。

十字脊頂多出現于北方建築，不過南方一些新建仿古建築也開始應用歇山式十字脊頂，如[贛縣文昌閣](../Page/贛縣文昌閣.md "wikilink")、[溫嶺東輝閣](../Page/溫嶺東輝閣.md "wikilink")、[杭州望宸閣](../Page/杭州望宸閣.md "wikilink")、[休寧](../Page/休寧.md "wikilink")-{[迴源亭](../Page/迴源亭.md "wikilink")}-，以及大量南京仿古建築等。

[僧庠中常見多重簷十字脊顶](../Page/奘房.md "wikilink")。

<File:东羊后土庙.JPG>|[東羊后土廟的元代歇山式十字脊頂戲臺](../Page/東羊后土廟.md "wikilink")
<File:Datong> Huayan Si 2013.08.29
08-18-29.jpg|[大同華嚴寺仿古鼓樓](../Page/大同華嚴寺.md "wikilink")

## 起源

**十字組屋**（）是[中国及](../Page/中国.md "wikilink")[朝鮮傳統建筑](../Page/朝鮮半島.md "wikilink")，是由两个屋顶垂直相交而成，可以由[硬山顶](../Page/硬山顶.md "wikilink")、[悬山顶](../Page/悬山顶.md "wikilink")、[歇山顶或](../Page/歇山顶.md "wikilink")[廡殿頂組成](../Page/廡殿頂.md "wikilink")。

朝鮮雖然沒有眞正意義上的歇山式十字脊頂，卻有一些**十字（脊）歇山頂**組屋，如和，以及宋代的[岳陽樓](../Page/岳陽樓.md "wikilink")。注意朝鮮不少被認作十字歇山頂組屋的建築實爲帶兩個抱廈的歇山頂，不同于上述十字歇山頂組屋。

[中衛鼓樓是罕見的](../Page/中衛鼓樓.md "wikilink")**十字脊廡殿頂**組屋。注意十字脊廡殿頂一般來説衹能縮合成四角攢尖頂，所以并沒有出現廡殿式十字脊頂（理論上可通過提高廡殿頂左右兩坡和前後兩坡的斜率比來得到）的實例。

[高棉寺中常見多層簷十字懸山頂](../Page/暹寺.md "wikilink")。

<File:Zhongwei> IGP4689.jpg|[中衛鼓樓十字脊廡殿頂組屋](../Page/中衛鼓樓.md "wikilink")
<File:Changdeokgung>
Buyongjeong.jpg|[昌德宮芙蓉亭十字歇山頂組屋](../Page/昌德宮芙蓉亭.md "wikilink")

## 参考文献

## 参见

  - [中国古代建筑的屋顶](../Page/中国古代建筑的屋顶.md "wikilink")

{{-}}

[Category:中国传统屋顶](../Category/中国传统屋顶.md "wikilink")