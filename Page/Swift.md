**Swift**在英文中指[雨燕](../Page/雨燕.md "wikilink")，另外也可能指下列事物：

## 人物

**Swift**作为人名姓氏时通常译为**斯威夫特**，可以指：

|                                                          |                         |                                                                                                                                |
| -------------------------------------------------------- | ----------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| [查爾斯·斯威夫特](../Page/查爾斯·斯威夫特.md "wikilink")               | Charles Swift           | [美國海軍](../Page/美國海軍.md "wikilink")、[法律](../Page/法律.md "wikilink")[教授](../Page/教授.md "wikilink")、[律師](../Page/律師.md "wikilink") |
| [克萊夫·斯威夫特](../Page/克萊夫·斯威夫特.md "wikilink")               | Clive Swift             | 英格蘭[演員](../Page/演員.md "wikilink")                                                                                              |
| [愛德華·斯威夫特](../Page/愛德華·斯威夫特.md "wikilink")               | Edward D. Swift         | 美國[太空人](../Page/太空人.md "wikilink")。天文學家[路易斯·斯威夫特的兒子](../Page/路易斯·斯威夫特.md "wikilink")                                           |
| [喬治·斯威夫特](../Page/喬治·斯威夫特.md "wikilink")                 | George Swift            | 美國[參議員](../Page/參議員.md "wikilink")                                                                                             |
| [格雷厄姆·斯威夫特](../Page/格拉汉姆·史威夫特.md "wikilink")             | Graham Swift            | 英格蘭[作家](../Page/作家.md "wikilink")                                                                                              |
| [古斯塔夫斯·富蘭克林·斯威夫特](../Page/古斯塔夫斯·富蘭克林·斯威夫特.md "wikilink") | Gustavus Franklin Swift | 美國肉類包裝[商人](../Page/商人.md "wikilink")、Swift & Company主席                                                                         |
| [亨利·阿多奈拉姆·斯威夫特](../Page/亨利·阿多奈拉姆·斯威夫特.md "wikilink")     | Henry Adoniram Swift    | 美國[明尼蘇達州第](../Page/明尼蘇達州.md "wikilink")3任[州長](../Page/明尼蘇達州州長列表.md "wikilink")                                                 |
| [簡·斯威夫特](../Page/簡·斯威夫特.md "wikilink")                   | Jane Swift              | 美國[麻省署任](../Page/麻省.md "wikilink")[州長](../Page/馬薩諸塞州州長列表.md "wikilink")                                                        |
| [喬納森·斯威夫特](../Page/喬納森·斯威夫特.md "wikilink")               | Johnathan Swift         | 英國－愛爾蘭諷刺[作家](../Page/作家.md "wikilink")，著作《[格理弗遊記](../Page/格理弗遊記.md "wikilink")》                                                |
| [凱·斯威夫特](../Page/凱·斯威夫特.md "wikilink")                   | Kay Swift               | 美國女[作曲人](../Page/作曲家.md "wikilink")                                                                                            |
| [羅伯特·斯威夫特](../Page/羅伯特·斯威夫特.md "wikilink")               | Robert Swift            | 美國[NBA](../Page/NBA.md "wikilink")[西雅圖超音速](../Page/西雅圖超音速.md "wikilink")[籃球員](../Page/籃球.md "wikilink")                        |
| [斯威夫特](../Page/斯威夫特_\(歌手\).md "wikilink")                | Swift                   | 美國樂隊[D12成員](../Page/D12.md "wikilink")                                                                                         |
| [斯蒂法妮·斯威夫特](../Page/斯蒂法妮·斯威夫特.md "wikilink")             | Stephanie Swift         | 美國[色情女演員](../Page/色情演員.md "wikilink")                                                                                          |
| [斯特羅邁爾·斯威夫特](../Page/斯特羅邁爾·斯威夫特.md "wikilink")           | Stromile Swift          | 美國[NBA](../Page/NBA.md "wikilink")[新澤西網隊](../Page/新澤西網隊.md "wikilink")[籃球員](../Page/籃球.md "wikilink")                          |
| [泰勒·斯威夫特](../Page/泰勒·斯威夫特.md "wikilink")                 | Taylor Swift            | 美國女[歌手](../Page/歌手.md "wikilink")                                                                                              |

## 地名

  - [斯威夫特縣 (明尼蘇達州)](../Page/斯威夫特縣_\(明尼蘇達州\).md "wikilink")（Swift
    County）：美國[明尼蘇達州轄下的縣份](../Page/明尼蘇達州.md "wikilink")
  - [斯威夫特
    (火衛二隕石坑)](../Page/斯威夫特_\(火衛二隕石坑\).md "wikilink")（Swift）：[火星的天然衛星](../Page/火星.md "wikilink")[火衛二上之撞擊坑](../Page/火衛二.md "wikilink")

## 電腦

  - [OpenStack的分布式存储组件](../Page/OpenStack.md "wikilink")
  - [Apple A6和](../Page/Apple_A6.md "wikilink")[Apple
    A6X的CPU代號](../Page/Apple_A6X.md "wikilink")
  - [Swift語言](../Page/Swift語言.md "wikilink")，[苹果公司推出的高階](../Page/苹果公司.md "wikilink")[程式語言](../Page/程式語言.md "wikilink")

## 經濟金融

  - [环球银行金融电信协会](../Page/环球银行金融电信协会.md "wikilink")（SWIFT）

## 汽車

  - [鈴木Swift](../Page/鈴木Swift.md "wikilink")：日本[鈴木公司開發製造的](../Page/鈴木公司.md "wikilink")[次緊湊型車](../Page/次緊湊型車.md "wikilink")。