**载具**，**载运工具**，是指使用於人或貨物運輸的設備。本身不产生位移的[传送带或非人造的某些水面漂浮物都不能称为](../Page/传送带.md "wikilink")“载具”。为上述目的以人的意志为驱使的[动物和](../Page/动物.md "wikilink")[人本身也可以称为载具](../Page/人.md "wikilink")。包含陸上、水上、空中、水下、太空等，均有載具的使用。

**车辆**是[陆地轮式](../Page/陆地.md "wikilink")、链式和[轨道式](../Page/鐵路軌道.md "wikilink")[运输工具的总称](../Page/运输工具.md "wikilink")，是載具的一種。例如[火车](../Page/火车.md "wikilink")、[汽车](../Page/汽车.md "wikilink")、[人力车和](../Page/人力车.md "wikilink")[畜力车等几大类](../Page/畜力车.md "wikilink")。大部分的車輛有[輪](../Page/輪.md "wikilink")。中文中，车最早的记录出于五千年以前中国历史上的黄帝时期。

## 歷史

## 介绍

### 無動力車

車本無任何動力來源，而需靠[人力或](../Page/人力.md "wikilink")[獸力才能行進](../Page/獸力.md "wikilink")（人和這些動物（如[牛](../Page/牛.md "wikilink")、[馬](../Page/馬.md "wikilink")、[狗](../Page/狗.md "wikilink")）屬於運輸的動力來源，並不在車的範圍內）。车最早的文史记录出自于五千年前的[中国](../Page/中国.md "wikilink")，為中國历史上的[黄帝时期](../Page/黄帝.md "wikilink")。黄帝是“轩辕”的发明者，所以黄帝又被称为“轩辕氏”（轩辕在中文里的意思就是“车”）。

### 轉折

數千年以來，人類都使用無動力車，但人們不斷的提出新的想法、做了許多實驗，讓科技一步一步的改革，並持續的發展。西元1698-1769年出現了初代的**[蒸汽機](../Page/蒸汽機.md "wikilink")**，西元1800年初開始使用，如果將蒸汽機用於[汽車上只能跑幾十公里](../Page/汽車.md "wikilink")。蒸汽機使用了80幾年時（西元1885年），世界上第一輛真正的汽車出現了。而西元1794-1867年的這幾項發明，也為汽車舗了長遠的後路。

### 有動力車

後來車有了動力[引擎](../Page/引擎.md "wikilink")（內燃機），只需人類的控制即可行進。或是編寫一些[程式](../Page/程式.md "wikilink")，使車可自行行進，此即[自動駕駛汽車](../Page/自動駕駛汽車.md "wikilink")。汽車引擎的動力可取代以前無動力車的人力和獸力，引擎動力比人力和獸力更有效率。速度加快，運輸的時間也跟著減少，現今成為人們主要的載運工具。而現今的無動力車多數用於休閒之活動使用（如[牛車](../Page/牛車.md "wikilink")、[馬車](../Page/馬車.md "wikilink")），但如[腳踏車這類的人力無動力車](../Page/腳踏車.md "wikilink")，也有不少人當作主要的載運工具（像是近年來，因臺灣環保意織抬頭，也愈來愈多人使用腳踏車通勤，而部分西歐國家也致力推行腳踏車的好處）。1885年，真正的現代汽車誕生的時刻，德國工程師[卡爾·賓士在曼海姆製造成一輛裝有](../Page/卡爾·賓士.md "wikilink")0.85馬力汽油機的[三輪車](../Page/三輪車.md "wikilink")，這一輛裝有[內燃機的汽車被認為是世界上第一輛真正的汽車](../Page/內燃機.md "wikilink")（因為它以汽油為動力源，而不是蒸汽機）。

## 用途

### 人們

### 貨物

### 休閒

### 娛樂

### 軍武

## 種類

  - [飛機](../Page/飛機.md "wikilink")
  - [直升機](../Page/直升機.md "wikilink")
  - [滑翔機](../Page/滑翔機.md "wikilink")
  - [無人飛機](../Page/無人飛機.md "wikilink")
  - [船](../Page/船.md "wikilink")
  - [軍艦](../Page/軍艦.md "wikilink")
  - [潛艇](../Page/潛艇.md "wikilink")
  - [腳踏車](../Page/腳踏車.md "wikilink")
  - [機車](../Page/摩托車.md "wikilink")
  - [汽車](../Page/汽車.md "wikilink")
  - [貨車](../Page/貨車.md "wikilink")
  - [公車](../Page/公共汽車.md "wikilink")
  - [無軌電車](../Page/無軌電車.md "wikilink")
  - [有軌電車](../Page/有軌電車.md "wikilink")
  - [火車](../Page/火車.md "wikilink")
  - [地鐵](../Page/地鐵.md "wikilink")
  - [高鐵](../Page/高鐵.md "wikilink")
  - [輕軌](../Page/輕軌運輸系統.md "wikilink")
  - [空中纜車](../Page/空中纜車.md "wikilink")
  - [遙控車](../Page/遙控車.md "wikilink")
  - [飛船](../Page/飛艇_\(輕航空器\).md "wikilink")
  - [太空船](../Page/太空船.md "wikilink")
  - [載人飛船](../Page/載人飛船.md "wikilink")
  - [飛行艇](../Page/飛行艇.md "wikilink")
  - [火箭](../Page/火箭.md "wikilink")
  - [人造衛星](../Page/人造衛星.md "wikilink")
  - [太空梭](../Page/太空梭.md "wikilink")
  - [太空飛機](../Page/太空飛機.md "wikilink")
  - [空天飛機](../Page/空天飛機.md "wikilink")

## 圖集

<File:Qin_dynasty_bronze_chariot_and_horses.jpg>|[秦朝](../Page/秦朝.md "wikilink")\]銅馬車（陪葬品）
<File:Powerful> landlord in chariot. Eastern Han 25-220 CE. Anping,
Hebei.jpg|[東漢](../Page/東漢.md "wikilink")[馬戰車](../Page/馬戰車.md "wikilink")
<File:3wheelscar.jpg>|[人力車](../Page/人力車.md "wikilink")[單車](../Page/單車.md "wikilink")
<File:BNSF> 7663.jpg|[火車](../Page/火車.md "wikilink") <File:HK> Wan Chai
Grand Hyatt Hong Kong black 1.JPG|[跑車](../Page/跑車.md "wikilink")
<File:HK> Parkview Shuttle Bus in Edinburgh
Place.jpg|[遊覽車](../Page/遊覽車.md "wikilink")
[File:MSMajestyOfTheSeasEdit1.JPG|郵輪](File:MSMajestyOfTheSeasEdit1.JPG%7C郵輪)
<File:Stryker> FSV front q.jpg|[史崔克裝甲車](../Page/史崔克裝甲車.md "wikilink")
<File:Ilyushin>
Il-96.jpg|[伊留申-96](../Page/伊留申-96.md "wikilink")[廣體飛機](../Page/廣體飛機.md "wikilink")

## 参考文献

## 参见

  - [运载火箭](../Page/运载火箭.md "wikilink")

{{-}}

[交通工具](../Category/交通工具.md "wikilink")