**冠军侯**，是[西汉曾经出现的](../Page/西汉.md "wikilink")[列侯爵号](../Page/列侯.md "wikilink")，取“功冠诸军”或“勇冠三军”之意，于[元朔六年](../Page/元朔.md "wikilink")（前123年）分封名将[霍去病](../Page/霍去病.md "wikilink")。[冠军侯国是汉武帝专门设立的](../Page/冠军侯国.md "wikilink")，其地来自于[南阳郡下属的](../Page/南阳郡.md "wikilink")[穰县卢阳乡及](../Page/穰县.md "wikilink")[宛县临菑郡](../Page/宛县.md "wikilink")。\[1\]

霍去病[元狩六年](../Page/元狩.md "wikilink")（前117年）即病逝，年仅24岁，子[霍嬗嗣国](../Page/霍嬗.md "wikilink")，但是也于[元封元年](../Page/元封.md "wikilink")（前110年）去世，侯国无嗣而除。

[地节二年](../Page/地节.md "wikilink")（前68年），霍去病的异母弟权臣[霍光临死前分封邑](../Page/霍光.md "wikilink")3000户给侄孙[霍山为乐平侯继祀霍去病](../Page/霍山_\(西汉\).md "wikilink")，但是一年后[汉宣帝即诛灭霍氏](../Page/汉宣帝.md "wikilink")。\[2\]

## 封户情况

| 冠军侯                              | 《[史记](../Page/史记.md "wikilink")》记载封户 | 《[汉书](../Page/汉书.md "wikilink")》记载封户 | 备注                                                                                        |
| -------------------------------- | ------------------------------------ | ------------------------------------ | ----------------------------------------------------------------------------------------- |
| [霍去病](../Page/霍去病.md "wikilink") | 1600户                                | 2500户                                | 初封                                                                                        |
|                                  | 3600户                                | 4700户                                | [元狩二年春](../Page/元狩.md "wikilink")，《史记》记载加封2000户，《[汉书](../Page/汉书.md "wikilink")》记载加封2200户 |
|                                  | 8800户                                | 10100户                               | [元狩二年夏](../Page/元狩.md "wikilink")，《史记》记载加封5000户，《[汉书](../Page/汉书.md "wikilink")》记载加封5400户 |
|                                  | 10500户                               | 11800户                               | [元狩二年秋](../Page/元狩.md "wikilink")，加封1700户                                                 |
|                                  | 16300户                               | 17600户                               | [元狩四年春](../Page/元狩.md "wikilink")，加封5800户                                                 |
| [霍嬗](../Page/霍嬗.md "wikilink")   |                                      |                                      | [元狩六年](../Page/元狩.md "wikilink")，霍去病薨，谥**景桓侯**，子嬗袭冠军侯                                     |
| 侯国绝                              |                                      |                                      | [元封元年](../Page/元封.md "wikilink")，霍嬗卒，谥**哀侯**，嗣绝国除                                         |

## 历史上的冠军侯

  - [王甫](../Page/王甫_\(东汉\).md "wikilink")\[3\]，[汉灵帝时期宦官](../Page/汉灵帝.md "wikilink")，以诬勃海王[刘悝之功](../Page/刘悝.md "wikilink")，封冠军侯。

## 参看

  - [冠军县](../Page/冠军县.md "wikilink")
  - [窦宪](../Page/窦宪.md "wikilink")
  - [贾复](../Page/贾复.md "wikilink")

## 注释

[Category:汉朝列侯爵号](../Category/汉朝列侯爵号.md "wikilink")
[Category:中國特殊官爵](../Category/中國特殊官爵.md "wikilink")

1.  《汉书·地理志上》
2.  《汉书·霍光金日磾传》：“光秉政前后二十年，地节二年春病笃，车驾自临问光病，上为之涕泣。光上书谢恩曰：‘愿分国邑三千户，以封兄孙奉车都尉山为列侯，奉兄票骑将军去病祀。’事下丞相、御史，即日拜光子禹为右将军。……既葬，封山为乐平侯，以奉车都尉领尚书事。”
3.  后汉书卷七十八 宦者列传第六十八 「节遂与王甫等诬奏桓帝弟勃海王悝谋反，诛之。以功封者十二人。甫封冠军侯。」