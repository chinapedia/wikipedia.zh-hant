[缩略图](https://zh.wikipedia.org/wiki/File:Air_Nostrum_ATR_72-600.jpg "fig:缩略图")ATR
72-600\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:ATR-72-500_Precision_Air_Services_\(PRF\)_F-WWEW_-_MSN_923_-_Named_Kilimandjaro_-_Will_be_5H-PWG_\(5000147631\).jpg "fig:缩略图")
[UNI_AIR_ATR_72-600_IN_RCSS.jpg](https://zh.wikipedia.org/wiki/File:UNI_AIR_ATR_72-600_IN_RCSS.jpg "fig:UNI_AIR_ATR_72-600_IN_RCSS.jpg")ATR
72-600\]\] **ATR
72**為[法國與](../Page/法國.md "wikilink")[義大利合資的](../Page/義大利.md "wikilink")[飛機製造商](../Page/飛機.md "wikilink")[ATR製造的雙螺旋槳](../Page/ATR.md "wikilink")[民航機](../Page/民航機.md "wikilink")，結構上與42型一樣，但機身加長4.5米，載客數提升至72人，油箱容量更大，航程更遠。目前ATR
72型已經出售超過678架。
[缩略图](https://zh.wikipedia.org/wiki/File:ATRv1.0.png "fig:缩略图")

## 發展

ATR 72是[ATR
42的改良型號](../Page/ATR_42.md "wikilink")，機身加長4.5米，改用更大馬力的引擎，增加約10%的燃料。ATR
72於1988年進行首航\[1\]，1989年投入商業服務，[芬蘭航空是首個接收此型號飛機的航空公司](../Page/芬蘭航空.md "wikilink")。ATR系列的行李儲存架設置在駕駛室與客艙之間，所以前方機門通常是用作裝卸行李，而乘客大多數從尾部機門進入機艙。相比其它飛機，此型號並沒有提供空氣及電力功能的[輔助動力系統](../Page/輔助動力系統.md "wikilink")（APU），而是在右邊的二號發動機上設置螺旋槳煞車裝置，讓2號發動機可以在啟動的狀態下，不讓螺旋槳旋轉，僅擔任電力和空氣的供應。

ATR公司曾計劃發展ATR 82型，載客量提升為78人，速度提高至610公里／時，但於1996年終止此計劃。

## 型號

### ATR 72-101/102型

最初設計的兩種次機種皆被稱為100系列(-100)。均採用PW124B引擎。

#### ATR 72-101型

101型的設計中，前後兩處機門皆可供乘客上下機。於1989年9月完成認證。

#### ATR 72-102型

102型的設計中，前機門被設計為裝卸行李用，乘客由後機門上下機。於1989年12月完成認證。

### ATR 72-201/202型

200系列(-200)是該系列最先生產的版本，採用PW124B引擎。

#### ATR 72-201型

101型的升級版本，提高了101型的最大起飛重量。採用PW124B引擎的版本於1989年9月完成認證。

#### ATR 72-202型

102型的升級版本，提高了102型的最大飛重量。採用PW124B引擎的版本於1989年12月完成認證。

### ATR 72-211/212型

200系列的改進型，採用高空和高溫性能較好的[普惠PW](../Page/普惠.md "wikilink")127螺旋槳引擎。每座PW127引擎可輸出2,750匹馬力（2,050千瓦），在高空與高溫環境下的表現更為優異。此兩種機型皆於1992年12月完成認證。同樣地，200系列兩種機型的分別在於其機門配置。

### ATR 72-212A型

212型的改進型，改採用由全複合材料製造的六槳葉螺旋槳，並選用[普惠PW](../Page/普惠.md "wikilink")127F（ATR
72-500型）和PW127M（ATR 72-600型）螺旋槳引擎。除此之外也提升了最大起飛重量，和更加自動化的能源管理，讓駕駛工作更容易。

#### ATR 72-500型

原始名稱即為ATR 72-212A型，於1997年1月完成認證。

#### ATR 72-600型

2007年10月2日，ATR公司宣佈會研發新版的ATR 72-212A，並計畫在2010年下半年開始交付。2009年7月24日，由ATR
72-500改裝而成的ATR 72-600原型機完成首航。

作為ATR 72-500型的改良型號\[2\]，ATR
72-600型的提升之處如下：採用PW127M引擎，新增「加速功能」可提升額外5%起飛動力；改良駕駛艙儀器，把傳統的電子儀器改為[液晶顯示器](../Page/液晶顯示器.md "wikilink")\[3\]；多功能電腦系統可提升飛航安全和操縱性能；新採用Thales公司的[航空電子提供了](../Page/航空电子.md "wikilink")[所需導航性能](../Page/所需导航性能.md "wikilink")；輕質座椅；更大的客艙置物空間；也增加了起飛重量。2015年12月，[歐洲航空安全局核准了新的高密度座位配置](../Page/欧洲航空安全局.md "wikilink")，載客量可由74人提高至78人。

### 其他版本

#### 貨機版本

包含散貨機（Bulk Freighter）和特大機門型貨機（ULD
Freighter）。[ATR在](../Page/ATR.md "wikilink")2002年的[法茵堡航空展展出了ATR](../Page/法茵堡航空展.md "wikilink")
72各機型的特大機門改裝版。[聯邦快遞](../Page/聯邦快遞.md "wikilink")、[DHL和](../Page/DHL.md "wikilink")[優比速等快遞公司皆有使用ATR](../Page/联合包裹服务.md "wikilink")
72貨機。

#### ATR 72 ASW 

ATR 72的海軍反潛艦機種，整合了軍用版[ATR
42的海上巡邏系統並增加了](../Page/ATR_42.md "wikilink")[反潛作戰能力](../Page/反潛作戰.md "wikilink")。[義大利海軍訂製了ATR](../Page/意大利海军.md "wikilink")
72-500的ASW版本，以執行[反潛作戰和水面作戰任務](../Page/反潛作戰.md "wikilink")。義大利海軍訂製的四架ATR
72
ASW於2012年開始交付。為了執行反潛與水面作戰任務，此機種將配備一挺吊艙式機關槍、輕型空投式魚雷、水面攻擊導彈和深水炸彈。而Thales公司的機載海上定位與控制系統（Airborne
Maritime Situation and Control System, AMASCOS）亦賦予此機種執行搜救任務的能力。

#### 豪華版本

高級內裝版本的ATR 72-500，用以運輸商務人士或政府官員。

#### ATR 82

1980年代中期，ATR公司從ATR
72的衍生出了一個78人座的變異版。此版本預計採用[艾利森AE](../Page/艾利逊发动机公司.md "wikilink")
2100[渦輪螺旋槳發動機](../Page/渦輪螺旋槳發動機.md "wikilink")，預計巡航速度可達330[節](../Page/節_\(單位\).md "wikilink")，此計畫於1996年初中止。

#### ATR Quick Change

為了方便航空公司以貨機進行改裝、載客，ATR設計了可在45分鐘內完成改裝的客貨兩用機型（ATR 42的Quick
Change版本可在30分鐘內完成）。

## 主要用戶

### 主要民航用戶（擁有8架以上）

  - [藍色巴西航空](../Page/藍色巴西航空.md "wikilink")：49架

  - [Wings Air](../Page/Wings_Air.md "wikilink")：49架

  - [聯邦快遞](../Page/聯邦快遞.md "wikilink")：21架

  - ：22架

  - [飛螢航空](../Page/飛螢航空.md "wikilink")：19架

  - ：19

  - ：18架

  - ：16架

  - [烏塔航空](../Page/烏塔航空.md "wikilink")：15架

  - [阿爾及利亞航空](../Page/阿尔及利亚航空.md "wikilink")：15架

  - [北歐航空](../Page/北歐航空.md "wikilink")：14架

  - [馬來西亞之翼航空](../Page/馬來西亞之翼航空.md "wikilink")：14架

  - ：14架

  - ：15架

  - [曼谷航空](../Page/曼谷航空.md "wikilink")：13架

  - [維珍澳洲航空](../Page/維珍澳洲航空.md "wikilink")：13架

  - [立榮航空](../Page/立榮航空.md "wikilink")：15架

  - [捷特航空](../Page/捷特航空.md "wikilink")：13架

  - ：12架

  - [HOP\!](../Page/HOP!.md "wikilink")：12架

  - [越南國家航空](../Page/越南國家航空.md "wikilink")：12架

  - [加魯達印尼航空](../Page/加鲁达印尼航空.md "wikilink")：11架

  - [馬印航空](../Page/马印航空.md "wikilink")：11架

  - [遠東航空](../Page/遠東航空.md "wikilink")：9架（租用）

  - [華信航空](../Page/華信航空.md "wikilink") 3架租用 6架自有

2016年2月，[伊朗航空向ATR訂購了](../Page/伊朗航空.md "wikilink")20至40架ATR 72-600

### 軍用客戶

  - 土耳其海軍
  - [巴基斯坦海軍](../Page/巴基斯坦海军.md "wikilink")

## 意外

  - [美國鷹航空4184號班機](../Page/美國鷹航空4184號班機.md "wikilink")
      - 1994年10月31日，美鷹航空國內線在芝加哥墜毀，機上68人全部罹難。
  - [復興航空510A號班機](../Page/復興航空510A號班機.md "wikilink")
      - 1995年1月30日，[復興航空編號B](../Page/復興航空.md "wikilink")-22717的**ATR
        72**型客機，執飛GE510A航班，由於當時是農曆除夕，載客從台北飛抵馬公後，空機飛返台北松山機場，結果不幸途中偏離航道，在桃園[龜山區兔子坑撞山墜毀](../Page/龜山區.md "wikilink")。這架飛機空機無乘客，正副駕駛及空中服務人員等4名機組員不幸罹難。
  - [復興航空791號班機](../Page/復興航空791號班機.md "wikilink")
      - 2002年12月21日，復興航空编号B-22708的貨機，执飞GE791航班，在臺北時間01:52時，於飛航途中機翼遭嚴重積冰後，在[馬公西南方約](../Page/馬公市.md "wikilink")17公里處墜海失事，機上駕駛員2人皆失蹤。
  - [突尼西亞國際航空1153號班機](../Page/突尼西亞國際航空1153號班機.md "wikilink")
      - 2005年8月6日，一架飛往意大利的突尼西亞國際航空ATR 72，因耗盡燃油在海面緊急降落時發生意外，39人中有16人死亡。
  - [曼谷航空](../Page/曼谷航空.md "wikilink")
      - 2009年8月4日，泰国[曼谷航空公司一架ATR](../Page/曼谷航空.md "wikilink")
        72-500型客机在苏梅岛机场着陆时滑出跑道并起火，造成至少1人死亡、数十人受伤。
  - [古巴加勒比航空公司](../Page/加勒比海航空.md "wikilink")
      - 2010年11月4日，[古巴加勒比航空公司的一架ATR](../Page/加勒比海航空.md "wikilink")
        72-212民航客机在[古巴中部城市](../Page/古巴.md "wikilink")[圣斯皮里图斯附近坠毁](../Page/圣斯皮里图斯.md "wikilink")，机上68人全部遇难。
  - [UTair航空公司](../Page/UTair航空.md "wikilink")
      - 2012年4月2日，俄[UTair航空的一架ATR](../Page/UTair航空.md "wikilink")
        72-212民航客机在西伯利亚[秋明州坠毁](../Page/秋明州.md "wikilink")。
  - [寮國航空301號班機空難](../Page/寮國航空301號班機空難.md "wikilink")
      - 2013年10月16日，寮國航空公司的一架ATR 72客机在南部占巴塞省失事，机上人员全部遇难。
  - [復興航空222號班機事故](../Page/復興航空222號班機事故.md "wikilink")
      - 2014年7月23日，復興航空的一架機號為B-22810的ATR
        72-500在馬公機場大雨及疑似受[麥德姆颱風影響](../Page/麥德姆.md "wikilink")，班機向塔台要求重飛後於[澎湖縣湖西鄉西溪村重飛失敗](../Page/澎湖縣.md "wikilink")，事故造成49死9傷，地面人員5傷。\[4\]\[5\]\[6\]
  - [復興航空235號班機事故](../Page/復興航空235號班機事故.md "wikilink")
      - 2015年2月4日，[復興航空的一架機號為B](../Page/復興航空.md "wikilink")-22816的ATR
        72-600於10時45分從[台北松山機場起飛](../Page/台北松山機場.md "wikilink")，目的地為[金門尚義機場](../Page/金門機場.md "wikilink")。因發動機失效，又誤關正常的發動機，於上午10時55分飛機急轉墜下擦撞[環東大道高架橋導致墜入距南陽大橋](../Page/環東大道.md "wikilink")800公尺的內溝溪抽水站附近基隆河流域，機上共58人，事故造成43人死亡、17人受傷（含地面2人）。左側機翼撞擊高架橋之前，還擦到一台正常行駛的[計程車](../Page/計程車.md "wikilink")，計程車車頭損毀。兩台在[環東大道行駛的車輛裡面行車記錄儀記錄了飛機失事墜落的過程](../Page/環東大道.md "wikilink")。\[7\]
  - [伊朗阿塞曼航空3704號班機空难](../Page/伊朗阿塞曼航空3704號班機空难.md "wikilink")
      - 2018年2月18日，一架往[伊朗](../Page/伊朗.md "wikilink")[亞蘇季的伊朗阿塞曼航空ATR](../Page/亞蘇季.md "wikilink")
        72-212客機在伊斯法罕省的塞米羅姆附近墜毀，機上全部人員遇難。\[8\]

## 規格（ATR 72-600型）

[ATRv1.0.png](https://zh.wikipedia.org/wiki/File:ATRv1.0.png "fig:ATRv1.0.png")

  - 長度：27.17米
  - 翼展：27.05米
  - 高度：7.65米
  - 載客量：68-74人
  - 空機重量：12,950公斤
  - 最大起飛重量：22,500公斤
  - 航程：2,414公里
  - 巡航速度：511公里／時
  - 起飛所需最短跑道:1,450公尺
  - 巡航高度：7,000 m (21,000 ft)\[9\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [ATR公司](http://www.atraircraft.com/)

[Category:支线客机](../Category/支线客机.md "wikilink")

1.  [Airliners.net](http://www.airliners.net/info/stats.main?id=42)
2.  [ATR公司新聞](http://www.atraircraft.com/public/atr/html/press/releases-details.php?aid=796)

3.  [Degal.com](http://www.deagel.com/Turboprop-Airliners/ATR-72-600_a000090006.aspx)
4.  [復興失事機盤旋
    呼叫兩次重飛](http://www.cna.com.tw/news/firstnews/201407245005-1.aspx)，中央社，2014年7月24日
5.  [澎湖空難
    民航局：尋獲48具遺體](http://www.cna.com.tw/news/firstnews/201407245008-1.aspx)，中央社，2014年7月24日
6.  [復興航空澎湖墜機 49死9傷](http://udn.com/NEWS/NATIONAL/NATS3/8824245.shtml)，聯合報，2014年7月24日
7.  [復航墜河 12人救起送醫](https://tw.news.yahoo.com/%E5%BE%A9%E8%88%AA%E5%A2%9C%E6%B2%B3-16%E4%BA%BA%E6%95%91%E8%B5%B7%E9%80%81%E9%86%AB-042416129.html)，教育廣播電台，2015年2月4日
8.
9.  [ATR公司](http://www.atraircraft.com/public/atr/html/products/products.php?aid=506)