**台北國際商業銀行**（簡稱**台北商銀**、**北商銀**），是過去曾經存在於[台灣的](../Page/台灣.md "wikilink")[商業銀行之一](../Page/商業銀行.md "wikilink")，已於2006年11月併入[永豐金融控股公司旗下的](../Page/永豐金融控股公司.md "wikilink")[永豐商業銀行](../Page/永豐商業銀行.md "wikilink")。

## 歷史

[Card_case_of_IBTPE.jpg](https://zh.wikipedia.org/wiki/File:Card_case_of_IBTPE.jpg "fig:Card_case_of_IBTPE.jpg")
1948年5月4日，「**台北區合會儲蓄公司**」成立。1975年，《銀行法》修正條文將[合會儲蓄公司納入](../Page/合會儲蓄公司.md "wikilink")[銀行體制](../Page/銀行.md "wikilink")、並提供民眾與[中小企業授信業務](../Page/中小企業.md "wikilink")\[1\]；因此台北區合會儲蓄公司於1978年1月1日改制為「**台北區中小企業銀行**」\[2\]。1983年4月台北區中小企業銀行[股票上市](../Page/股票上市.md "wikilink")，1998年5月14日再改制為「**台北國際商業銀行**」。\[3\]

由於台北國際商業銀行名稱與[台北銀行近似](../Page/台北銀行.md "wikilink")，台北銀行向[經濟部要求撤銷台北國際商業銀行名稱](../Page/中華民國經濟部.md "wikilink")\[4\]。台北銀行向經濟部提起[訴願及](../Page/訴願.md "wikilink")[再訴願](../Page/再訴願.md "wikilink")，要求撤銷台北國際商業銀行名稱，均被駁回；台北銀行遂向[行政法院](../Page/最高行政法院_\(中華民國\).md "wikilink")（今[最高行政法院](../Page/最高行政法院_\(中華民國\).md "wikilink")）提起[行政訴訟](../Page/行政訴訟.md "wikilink")，2000年6月12日遭行政法院判決駁回\[5\]。

2005年12月26日，台北國際商業銀行以換股方式成為建華金融控股公司（2006年更名為[永豐金融控股公司](../Page/永豐金融控股公司.md "wikilink")）百分之百持股的[子公司](../Page/子公司.md "wikilink")\[6\]。2006年11月13日，台北國際商業銀行與同屬永豐金控的[建華商業銀行合併](../Page/建華商業銀行.md "wikilink")，合併後台北國際商業銀行為消滅銀行，建華銀行為存續銀行並更名為「**永豐商業銀行**」。

## 參考資料

## 參見

  - [永豐商業銀行](../Page/永豐商業銀行.md "wikilink")
  - [永豐金融控股公司](../Page/永豐金融控股公司.md "wikilink")

[T](../Category/台灣已結業銀行.md "wikilink")
[Category:永豐金控](../Category/永豐金控.md "wikilink")
[Category:1948年成立的公司](../Category/1948年成立的公司.md "wikilink")
[Category:1948年台灣建立](../Category/1948年台灣建立.md "wikilink")
[Category:2006年結業公司](../Category/2006年結業公司.md "wikilink")
[Category:2006年台灣廢除](../Category/2006年台灣廢除.md "wikilink")

1.
2.
3.
4.
5.
6.