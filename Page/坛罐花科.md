**坛罐花科**(Siparunaceae)共有2[属约](../Page/属.md "wikilink")75[种](../Page/种.md "wikilink")，分布在中[南美洲](../Page/南美洲.md "wikilink")(Siparuna属)和[西非](../Page/西非.md "wikilink")(Glossocalyx属)。

本科[植物都是](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，单[叶对生](../Page/叶.md "wikilink")，常绿，叶片有腺点，无托叶，芳香，可以提炼香精油。

1981年的[克朗奎斯特分类法将其列入](../Page/克朗奎斯特分类法.md "wikilink")[玉盘桂科](../Page/玉盘桂科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，仍然放在[樟目中](../Page/樟目.md "wikilink")。

## 参考文献

## 外部链接

:\* 在[L. Watson和M.J. Dallwitz
(1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[坛罐花科](http://delta-intkey.com/angio/www/siparuna.htm)

:\* [坛罐花科](http://www.arbolesornamentales.com/Monimiaceae.htm)

:\* [坛罐花科网站](http://www.umsl.edu/~biosrenn/siparuna/description.html)

:\*
[NCBI坛罐花科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=104773&lvl=3&lin=f&keep=1&srchmode=1&unlock)

:\*
[CSDL坛罐花科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Siparunaceae)

[\*](../Category/坛罐花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")