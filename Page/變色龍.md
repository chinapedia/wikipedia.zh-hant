**避役科**（[學名](../Page/學名.md "wikilink")：，）俗稱**變色龍**，是屬於[爬行綱的一種](../Page/爬行綱.md "wikilink")[動物](../Page/動物.md "wikilink")，與[蜥蜴同屬於](../Page/蜥蜴.md "wikilink")[蜥蜴亞目](../Page/蜥蜴亞目.md "wikilink")。主要分佈於非洲東部與[馬達加斯加](../Page/馬達加斯加.md "wikilink")。

## 特徵

[皮肤被顆粒状鱗覆盖](../Page/皮肤.md "wikilink")，真皮内有多种色素细胞，能随时伸缩，变化体色；头上有钝三角形突起；雙眼各自獨立，可以自由轉向不同視角。背部有嵴棱；[四肢較長](../Page/四肢.md "wikilink")，前後肢均具5指、趾，分為相對2組，前肢內側3指癒合，外側2指癒合在一起，可相互握持;後肢相反，內側2趾和外側3趾癒合，並相對持，這樣，牠就更善於在樹上攀爬。

一些種類的變色龍是根據周圍[環境來變換體色](../Page/環境.md "wikilink")，而另一些則是用體色來表達[情緒](../Page/情緒.md "wikilink")：當受到威脅或贏得配偶時體色最為鮮艷。也有表現身體狀況的顏色：發黑是身體狀況差時，發白則是體溫過高時。[舌頭較長](../Page/舌頭.md "wikilink")，幾乎與軀幹等長，捕食時通過舌尖產生的巨大吸力吸住獵物（一說舌尖上面有黏液，捕食時，舌頭能迅速「射」出，黏住昆蟲）。尾較長，善於纏繞在樹幹上

## 照片集錦

Caméléon Madagascar 02.jpg| Chamaeleo chamaeleon Frightened thus
black.JPG| Chamäleon2.jpg| Chameleon India.jpg|

[Category:變色龍](../Category/變色龍.md "wikilink")