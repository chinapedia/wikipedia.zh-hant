[劇院風琴](../Page/劇院風琴.md "wikilink"){{·w}}[電子風琴](../Page/電子風琴.md "wikilink"){{·w}}[哈蒙德風琴](../Page/哈蒙德風琴.md "wikilink"){{·w}}[艾倫風琴](../Page/艾倫風琴.md "wikilink"){{·w}}[簧風琴](../Page/簧風琴.md "wikilink")
}}

**風琴**（）是一種[鍵盤樂器](../Page/鍵盤樂器.md "wikilink")，外形類似[直立式鋼琴](../Page/直立式鋼琴.md "wikilink")，通過腳下的踏板送風，吹響音管，並配合雙手鍵盤彈奏的一種樂器。踏板鼓風進入風道，如果不彈奏，則整個風道是密閉的；當手指按下某琴鍵後，該鍵對應的音管的管塞就會打開，氣流通過而吹響音管，則發出一個樂音。

[管風琴在西方宗教](../Page/管風琴.md "wikilink")，尤其是[基督宗教的](../Page/基督宗教.md "wikilink")[彌撒儀式上](../Page/彌撒.md "wikilink")，扮演著莫大的信仰角色。

風琴在日治時代被引入臺灣的中小學音樂教室，至20世紀末才漸漸汰除。至今，音樂課的風琴仍存在於多數台灣人的記憶中。\[1\]

## 參考資料

<references />

## 外部連結

  - [管风琴音乐（MP3录音）](http://www.organ-music.net)

  - [Organ Music of the Day](http://jonuvargonai.weebly.com)

[Category:鍵盤樂器](../Category/鍵盤樂器.md "wikilink")

1.  高有智《打開音樂課本》，出自《台灣久久：台灣百年生活印記——人文一百年》王美玉編，天下文化出版，2011年。http://www.bookzone.com.tw/event/gb310/page041.asp
    2014-11-09