是1978年邵氏電影公司出品，[劉家良所執導之武打電影](../Page/劉家良.md "wikilink")。

## 電影內容

故事敘述[康熙年間](../Page/康熙.md "wikilink")，[平西王](../Page/平西王.md "wikilink")[吳三桂密謀造反](../Page/吳三桂.md "wikilink")，於是[皇帝命御書房韋學士之子韋風](../Page/皇帝.md "wikilink")（姜大衛飾）擔任密探，潛入江北圻平縣田家村五陽莊找出叛變之實據。

韋假作不識武功，混入田家做教書先生，教導田家小姐田芝芝（黃杏秀飾），日久生情，兩人逐結為夫婦，但芝祖父田老太爺（劉家榮飾）疑韋乃[清廷所派](../Page/清廷.md "wikilink")，命韋終生不得離開五陽莊。韋盜得造反實據後，欲與芝離莊，田老遂萌殺意，惡鬥之下，芝和芝母為掩護韋離去而雙雙死於田老拳下。

韋風死裡逃生，為替芝芝報仇創出[螳螂拳法](../Page/螳螂.md "wikilink")，再次闖入五陽莊，大破田老形影相隨拳法，更從田老處取得[平西王造反密件](../Page/平西王.md "wikilink")。回京後康熙大加讚許，豈知韋父（井淼飾）有愧子忠於[清朝](../Page/清朝.md "wikilink")，荼害反清志士，於是在酒中下毒，以父子兩人之命慰反清志士之靈。

## 人物角色

| 角色名稱                           | 演員                               | 備註   |
| ------------------------------ | -------------------------------- | ---- |
| 韋風                             | [姜大衛](../Page/姜大衛.md "wikilink") |      |
| 田芝芝                            | [黃杏秀](../Page/黃杏秀.md "wikilink") |      |
| 田老太爺                           | [劉家榮](../Page/劉家榮.md "wikilink") |      |
| 汪氏                             | [李麗麗](../Page/李麗麗.md "wikilink") | 芝芝母  |
| 大伯母                            | [夏萍](../Page/夏萍.md "wikilink")   |      |
| 三叔                             | [唐偉成](../Page/唐偉成.md "wikilink") |      |
| 四叔                             | [徐少強](../Page/徐少強.md "wikilink") |      |
| 田忠                             | [張午郎](../Page/張午郎.md "wikilink") | 田家管家 |
| 小胖                             | [倫家駿](../Page/倫家駿.md "wikilink") | 田家家僕 |
| [康熙](../Page/康熙.md "wikilink") | [韋弘](../Page/韋弘.md "wikilink")   |      |
|                                | [劉家輝](../Page/劉家輝.md "wikilink") | 武僧   |
|                                | [李海生](../Page/李海生.md "wikilink") | 蒙古力士 |
| 韋正元                            | [井淼](../Page/井淼.md "wikilink")   | 韋風父  |
|                                | [呂紅](../Page/呂紅.md "wikilink")   | 韋風母  |
| 王老師                            | [王清河](../Page/王清河.md "wikilink") |      |
|                                | [金軍](../Page/金軍.md "wikilink")   | 大臣   |
|                                | [張作舟](../Page/張作舟.md "wikilink") | 村民   |
|                                | [杜永亮](../Page/杜永亮.md "wikilink") | 村民   |
|                                | [西瓜刨](../Page/西瓜刨.md "wikilink") | 村民   |
|                                | [陳龍](../Page/陳龍.md "wikilink")   |      |
|                                | [余袁穩](../Page/余袁穩.md "wikilink") |      |
|                                | [小侯](../Page/小侯.md "wikilink")   |      |
|                                | [李擎柱](../Page/李擎柱.md "wikilink") |      |
|                                | [林克明](../Page/林克明.md "wikilink") |      |
|                                |                                  |      |

## 相關條目

  - [螳螂拳](../Page/螳螂拳.md "wikilink")

## 参考文献

## 外部連結

  - {{@movies|fshk20078360|螳螂}}

  -
  -
  -
  -
  -
  -
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[Category:粤语電影](../Category/粤语電影.md "wikilink")
[Category:香港动作片](../Category/香港动作片.md "wikilink")
[Category:功夫片](../Category/功夫片.md "wikilink")
[8](../Category/1970年代香港電影作品.md "wikilink")
[Category:劉家良電影](../Category/劉家良電影.md "wikilink")