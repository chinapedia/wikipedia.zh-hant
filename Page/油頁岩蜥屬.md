**油頁岩蜥屬**（[學名](../Page/學名.md "wikilink")：*Petrolacosaurus*）是種史前[爬行動物](../Page/爬行動物.md "wikilink")，身長約40公分長，是已知最早的[雙孔亞綱爬行動物](../Page/雙孔亞綱.md "wikilink")，生存在晚[石炭紀的](../Page/石炭紀.md "wikilink")[美國](../Page/美國.md "wikilink")[堪薩斯州](../Page/堪薩斯州.md "wikilink")，約3億200萬年前\[1\]。

油頁岩蜥可能主要以小型[昆蟲為食](../Page/昆蟲.md "wikilink")。油頁岩蜥有特化、類似犬齒的牙齒，這特徵也在[獸孔目與較晚的](../Page/獸孔目.md "wikilink")[哺乳類身上發現](../Page/哺乳類.md "wikilink")\[2\]。

## 大眾文化

油頁岩蜥曾出現在[BBC的節目](../Page/BBC.md "wikilink")《[與巨獸共舞](../Page/與巨獸共舞.md "wikilink")》（*Walking
with
Monsters*）。該節目說牠是[合弓類](../Page/合弓類.md "wikilink")、[蜥形綱動物的共同祖先](../Page/蜥形綱.md "wikilink")。牠們與幾種巨大[節肢動物生活在同一時代](../Page/節肢動物.md "wikilink")，像是巨型[中突蛛](../Page/中突蛛.md "wikilink")、[巨尾蜻蜓](../Page/巨尾蜻蜓.md "wikilink")、以及兩棲類[石炭蜥目的](../Page/石炭蜥目.md "wikilink")[原水蝎螈](../Page/原水蝎螈.md "wikilink")（*Proterogyrinus*）。

但是，因為油頁岩蜥是種原始的[雙孔動物](../Page/雙孔動物.md "wikilink")，牠們並非[基龍與](../Page/基龍.md "wikilink")[異齒龍的共同祖先](../Page/異齒龍.md "wikilink")。[爬行動物與](../Page/爬行動物.md "wikilink")[合弓動物有其他的共同祖先](../Page/合弓動物.md "wikilink")，早在油頁岩蜥出現之前，兩個[演化支已經分開演化](../Page/演化支.md "wikilink")。[異齒龍的祖先可能是](../Page/異齒龍.md "wikilink")[哈普托獸](../Page/哈普托獸.md "wikilink")（*Haptodus*），一種原始的合弓動物，生存於3億年前到2億8000萬年前。油頁岩蜥是種原始的雙孔動物，頭顱的兩側各有兩個[顳顬孔](../Page/顳顬孔.md "wikilink")，可使下頜肌肉附著在上面。哈普托獸只有一個位在眼窩後的[顳顬孔](../Page/顳顬孔.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

  -

</div>

[分类:石炭紀爬行動物](../Page/分类:石炭紀爬行動物.md "wikilink")
[分类:纖肢龍目](../Page/分类:纖肢龍目.md "wikilink")

[Category:石炭紀動物](../Category/石炭紀動物.md "wikilink")

1.  Falcon-Lang, H.J., Benton, M.J. & Stimson, M. (2007): Ecology of
    early reptiles inferred from Lower Pennsylvanian trackways. *Journal
    of the Geological Society*, London, 164; no. 6; pp 1113-1118.
    [article](http://findarticles.com/p/articles/mi_qa3721/is_200711/ai_n21137484/?tag=content;col1)
2.