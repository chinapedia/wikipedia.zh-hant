**李嘉誠家族**是[香港的商人家族之一](../Page/香港.md "wikilink")。

## 興起方法

李嘉誠家族中各人之興起歷程：

  - **[李嘉誠](../Page/李嘉誠.md "wikilink")**的興起歷程：先創辦[長江塑膠廠](../Page/長江塑膠廠.md "wikilink")（製造）塑膠花，及後投資地產業致富
  - **[李澤鉅](../Page/李澤鉅.md "wikilink")**的興起歷程：承繼[家族生意](../Page/家族生意.md "wikilink")
  - **[李澤楷](../Page/李澤楷.md "wikilink")**的興起歷程：轉售衛星電視賺下第一桶金再透過併購，鯨吞香港電訊

## 家族資產

  - [長江和記實業](../Page/長江和記實業.md "wikilink")30.07%（11.6090351億股）
  - [長江實業地產](../Page/長江實業地產.md "wikilink")30.1%（11.6090351億股）
  - [赫斯基能源公司](../Page/赫斯基能源公司.md "wikilink")29.31%
  - [facebook](../Page/facebook.md "wikilink")0.75%
  - [中國郵政儲蓄銀行](../Page/中國郵政儲蓄銀行.md "wikilink")11.62%（22.67億H股）總股本2.8%

## 家族特色

李嘉誠家族中之各種特色：

  - 與[莊靜庵家族有兩段](../Page/莊靜庵家族.md "wikilink")[姻親關係](../Page/姻親.md "wikilink")：
      - [李雲經](../Page/李雲經.md "wikilink")==[莊碧琴](../Page/莊碧琴.md "wikilink")
      - [李嘉誠](../Page/李嘉誠.md "wikilink")==[莊月明](../Page/莊月明.md "wikilink")

## 家族統計

|    |                                                                |
| -- | -------------------------------------------------------------- |
| 1位 | 曾任[中國人民政治協商會議全國委員會常務委員](../Page/中國人民政治協商會議全國委員會.md "wikilink") |
|    | 李澤鉅                                                            |
| 1位 | 獲頒最優秀不列顛帝國爵級司令勳章（ＫＢＥ）                                          |
|    | 李嘉誠                                                            |
| 1位 | 獲頒最優秀不列顛帝國司令勳章（ＣＢＥ）                                            |
|    | 李嘉誠                                                            |
| 1位 | 獲頒香港特別行政區大紫荊勳章（ＧＢＭ）                                            |
|    | 李嘉誠                                                            |
|    |                                                                |

## 家族成員

[長江和記實業及](../Page/長江和記實業.md "wikilink")[長江實業地產副董事總經理](../Page/長江實業地產.md "wikilink")[甘慶林為](../Page/甘慶林.md "wikilink")[李嘉誠的襟弟](../Page/李嘉誠.md "wikilink")。

## 参考文献

## 外部連結

  - [長江和記實業有限公司](http://www.ckh.com.hk/)
  - [長江實業地產有限公司](http://www.ckph.com.hk/)
  - [長江基建](http://www.cki.com.hk/)
  - [電能實業](http://www.powerassets.com/)
  - [赫斯基能源](http://www.huskyenergy.ca/)
  - [和記中國醫療科技](http://www.chi-med.com/)
  - [和記電訊（澳洲）（Hutchison Telecommunications (Australia)
    Limited）](http://www.hutchison.com.au/)
  - [香港電燈 及 港燈電力投資](http://www.heh.com/)
  - [和記電訊香港控股](http://www.hthkh.com/)
  - [TOM集團](https://web.archive.org/web/20060126211351/http://www.tomgroup.com/english/)
  - [長江生命科技](http://www.ck-lifesciences.com/)
  - [李嘉誠基金會](http://www.lksf.org/)
  - [電訊盈科](http://www.pccw.com/)
  - [香港電訊 及 香港電訊信託](http://www.hkt.com/)
  - [富衛集團](http://WWW.fwd.com/)
  - [盈科大衍地產](http://www.pcpd.com/)
  - [新城廣播](http://www.metroradio.com.hk/)
  - [港大零售國際控股有限公司](http://www.s-culture.com)

{{-}}

[李嘉誠家族](../Category/李嘉誠家族.md "wikilink")
[Category:香港家族](../Category/香港家族.md "wikilink")