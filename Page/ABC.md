**ABC**可以指：

## 語言

  - [英文](../Page/英文.md "wikilink")

## 科技

  - [阿塔那索夫貝理電腦](../Page/阿塔那索夫貝理電腦.md "wikilink")（**A**tanasoff-**B**erry
    **C**omputer）
  - [ABC (程式語言)](../Page/ABC_\(程式語言\).md "wikilink")
  - [ABC記譜法](../Page/ABC記譜法.md "wikilink")，一種以 A 到 G
    字母組成音階的程式語言，用以產出五線譜及MIDI音樂檔。

## 科学

  - [ABC猜想](../Page/Abc猜想.md "wikilink")（**abc** conjecture）
  - [ABC模型](../Page/ABC模型.md "wikilink")
  - [ABC百科全书](../Page/ABC百科全书.md "wikilink")

## 會計

  - [作業基礎成本制度](../Page/作業基礎成本制度.md "wikilink")（**A**ctivity-**B**ased
    **C**osting），或譯「[作業成本法](../Page/作業成本法.md "wikilink")」ABC

## 化學

  - [磷酸二氫銨](../Page/磷酸二氫銨.md "wikilink")，又名「ABC化學劑」，因為此種化學劑常放在A類，B類及C類[滅火器裏](../Page/滅火器.md "wikilink")。

## 軍事

  - [ABC武器](../Page/大規模殺傷性武器.md "wikilink")，核 (Atomic)，生物 (Biological)
    和化學 (Chemical)武器的舊稱。

## 体育与娱乐

  - [亞洲籃球錦標賽](../Page/亞洲籃球錦標賽.md "wikilink")（**A**sia **B**asketball
    **C**hampionship）
  - [ABC (專輯)](../Page/ABC_\(專輯\).md "wikilink")
  - [ABC (樂隊)](../Page/ABC_\(樂隊\).md "wikilink")
  - [ABC歌](../Page/ABC歌.md "wikilink")，即字母歌
  - [ABC
    (唱片)](../Page/ABC_\(唱片\).md "wikilink")，一隻由[美籍華人](../Page/美籍華人.md "wikilink")[饒舌歌手](../Page/饒舌.md "wikilink")[歐陽靖灌錄的唱片](../Page/歐陽靖_\(饒舌者\).md "wikilink")。
  - [ABC (澳大利亞電視頻道)](../Page/ABC_\(澳大利亞電視頻道\).md "wikilink")

## 廣播

  - [朝日放送](../Page/朝日放送.md "wikilink") (**A**sahi **B**roadcasting
    **C**orporation)
  - [美國廣播公司](../Page/美國廣播公司.md "wikilink") (**A**merican
    **B**roadcasting **C**ompany)
  - [澳大利亚广播公司](../Page/澳大利亚广播公司.md "wikilink")（**A**ustralian
    **B**roadcasting **C**orporation）

## 政治

  - [ABC强国](../Page/ABC强国.md "wikilink")，拉丁美洲最强三国巴西、阿根廷、智利的合称
  - [ABC群島](../Page/ABC群島.md "wikilink")，加勒比海阿魯巴、波内赫和库拉索三個島的合称
  - Anyone but CY or Anyone but
    Carrie，只接受除[梁振英或](../Page/梁振英.md "wikilink")[林鄭月娥以外的人選](../Page/林鄭月娥.md "wikilink")，擔任[香港特別行政區行政長官](../Page/香港特別行政區行政長官.md "wikilink")

## 公司

  - [中國農業銀行](../Page/中國農業銀行.md "wikilink") (Agricultural Bank of China)

## 組織

  - [美北浸禮會](../Page/美北浸禮會.md "wikilink") (American Baptist Churches)
  - 出版銷數公證會 (Audit Bureau of
    Circulations)，一家統計出版刊物銷量的組織。[香港](../Page/香港.md "wikilink")，[澳洲](../Page/澳洲.md "wikilink")，[紐西蘭](../Page/紐西蘭.md "wikilink")，[印度](../Page/印度.md "wikilink")，[英國及](../Page/英國.md "wikilink")[日本的出版銷數公證會均使用ABC這縮寫](../Page/日本.md "wikilink")。

## 人文

  - [安德魯·康寧漢，第一代海德霍普的康寧漢子爵](../Page/安德魯·康寧漢，第一代海德霍普的康寧漢子爵.md "wikilink")
    (Andrew Browne Cunningham, 1st Viscount Cunningham of Hyndhope)
    ，暱稱ABC。
  - [香蕉人](../Page/香蕉人.md "wikilink")，即[美國出生華裔](../Page/美國.md "wikilink")——[美籍華人](../Page/美籍華人.md "wikilink")（American-Born
    Chinese）和[澳洲出生華人](../Page/澳洲.md "wikilink")——[澳籍華人](../Page/澳籍華人.md "wikilink")（Australian-Born
    Chinese）
  - [ABC冰](../Page/ABC冰.md "wikilink")，馬來西亞甜品。
  - 用于形容某一专业的基础知识，也被用于普及读物的题目中，如《共产主义ABC》

## 交通

  - [北京地铁机场线的线路标志](../Page/北京地铁机场线.md "wikilink")“**ABC**”（**A**irport
    **B**eijing **C**ity）

## 機械

  - 比利時中速柴油機製造廠。