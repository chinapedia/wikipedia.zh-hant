**纳希切万自治共和国**（；；；），[阿塞拜疆共和国裡的](../Page/阿塞拜疆.md "wikilink")[自治共和國](../Page/自治共和國.md "wikilink")，位於[外高加索南部](../Page/外高加索.md "wikilink")，是阿塞拜疆的[外飞地](../Page/外飞地.md "wikilink")，北邻[亚美尼亚](../Page/亚美尼亚.md "wikilink")，南部和[伊朗接壤](../Page/伊朗.md "wikilink")，一小部分国土和[土耳其相邻](../Page/土耳其.md "wikilink")。面积5500平方公里，人口约44万（2015年），首都为[納希契凡](../Page/納希契凡.md "wikilink")。自治共和国成立于1924年的[蘇聯](../Page/蘇聯.md "wikilink")，当时全称为[納希切萬蘇維埃社會主義自治共和國](../Page/納希切萬蘇維埃社會主義自治共和國.md "wikilink")，隸屬於蘇聯的加盟共和国之一[外高加索联邦蘇維埃社會主義共和國](../Page/外高加索联邦蘇維埃社會主義共和國.md "wikilink")，後隸屬於[亞塞拜然蘇維埃社會主義共和國](../Page/亞塞拜然蘇維埃社會主義共和國.md "wikilink")。

除南部平原以外，大部分屬於山区，屬大陸性乾燥氣候，降雨量為500毫米以下。工業以食品和採礦为主，農業是纳希切万自治共和国的主導經濟部门。

## 歷史

納希契凡歷史可以追溯到2500年前，公元前1世紀，依靠亞美尼亞成為當時西亞最強大的國家之一。其後經歷[羅馬帝國](../Page/羅馬帝國.md "wikilink")、[帕提亞帝國](../Page/帕提亞帝國.md "wikilink")、[蒙古](../Page/蒙古帝國.md "wikilink")、[鄂圖曼帝國等外族入侵](../Page/鄂圖曼帝國.md "wikilink")，其後為俄國所吞併，後來伴隨阿塞拜疆在1991年獨立，成為納希契凡自治共和國。同年[納希契凡亦宣佈脫離阿塞拜疆獨立](../Page/納希契凡.md "wikilink")，但不為國際所承認。

## 外交

納希切萬自治共和國政府於1993年10月5日承認了[北賽普勒斯土耳其共和國的主權国家地位](../Page/北賽普勒斯土耳其共和國.md "wikilink")，但这一承认未获得阿塞拜疆中央政府的认可。

## 参见

  - [阿尔扎赫共和国](../Page/阿尔扎赫共和国.md "wikilink")

## 參考文獻

## 外部連結

  - [纳希切万自治共和国官方网站](http://www.nakhchivan.az/)
  - [纳希切万国立大学](http://www.ndu.edu.az/)

[納希契凡自治共和國](../Category/納希契凡自治共和國.md "wikilink")
[Category:亞塞拜然](../Category/亞塞拜然.md "wikilink")
[Category:外飛地](../Category/外飛地.md "wikilink")
[Category:自治共和国](../Category/自治共和国.md "wikilink")
[Category:爭議地區](../Category/爭議地區.md "wikilink")
[Category:突厥語族政權](../Category/突厥語族政權.md "wikilink")