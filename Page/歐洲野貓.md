**歐洲野貓**（學名：）\[1\]是一種小型[貓科動物](../Page/貓科動物.md "wikilink")，可見於[西歐](../Page/西歐.md "wikilink")、[中歐](../Page/中歐.md "wikilink")、[東歐](../Page/東歐.md "wikilink")、[蘇格蘭和](../Page/蘇格蘭.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")；在[斯堪地那維亞](../Page/斯堪地那維亞.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[英格蘭](../Page/英格蘭.md "wikilink")、[威爾斯以及](../Page/威爾斯.md "wikilink")[愛爾蘭則已滅絕](../Page/愛爾蘭.md "wikilink")。

歐洲野貓的体型上比[非洲野猫和](../Page/非洲野猫.md "wikilink")[家貓大很多](../Page/家貓.md "wikilink")，它的重量与家猫类似，雄性的平均重量为，雌性为，体重季节性波动很大，最高可达2.5千克。\[2\]欧洲野猫毛发厚重，体型和非锥形尾巴是其显-{著}-特点，通常不会被误认为是家猫。研究表明，误认率达39％。\[3\]欧洲野猫主要为[夜行性](../Page/夜行性.md "wikilink")，在白天没有人类干扰时也活动。\[4\]

## 圖片

Felis_silvestris_silvestris_Luc_Viatour.jpg|歐洲野貓 Felis silvestris
Kocka divoká zoo
cropped.jpg|thumb|生活在[捷克共和國](../Page/捷克共和國.md "wikilink")[杰钦一間動物園內的歐洲野貓](../Page/杰钦.md "wikilink")

## 參考文獻

## 外部鏈結

  - [Wildcat Haven](https://www.wildcathaven.com/)
  - [Species portrait European wildcat; IUCN/SSC Cat Specialist
    Group](http://www.catsg.org/index.php?id=101)
  - [Save the Scottish Wildcat](http://www.scottishwildcats.co.uk/),
    general information and education website for Scottish wildcats.
  - [Electric Scotland: Scottish
    Wildcats](http://www.electricscotland.com/lifestyle/scottish_wildcats.htm)
  - [Kreiszeitung: *Wildkatzen rücken nach Norden vor* (Wildcats Reach
    The
    North)](http://www.kreiszeitung.de/nachrichten/lokales/niedersachsen/wildkatzen-ruecken-nach-norden-1131024.html)

[Category:貓屬](../Category/貓屬.md "wikilink")
[Category:歐洲哺乳動物](../Category/歐洲哺乳動物.md "wikilink")
[Category:西南亞哺乳動物](../Category/西南亞哺乳動物.md "wikilink")

1.

2.

3.

4.