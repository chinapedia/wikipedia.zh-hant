**葉偉信**（，），[香港](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")。葉偉信自1985年在新藝城電影公司開始其電影幕後工作生涯，曾參與超過二十部電影的拍攝工作，於1995年首次擔任電影導演，拍攝作品《[夜半一點鐘](../Page/夜半一點鐘.md "wikilink")》。並憑1996年拍攝的《[旺角風雲](../Page/旺角風雲.md "wikilink")》獲得第三屆[香港電影評論學會推薦電影](../Page/香港電影評論學會.md "wikilink")。

## 個人簡介及入行經過

葉偉信於[樂民邨長大](../Page/樂民新村.md "wikilink")\[1\]，曾就讀於[東莞同鄉會小學](../Page/東莞同鄉會小學.md "wikilink")（1976年小六）、[新亞中學](../Page/新亞中學.md "wikilink")（讀至1979年中三），後於1981年畢業於[五育中學](../Page/五育中學_\(香港\).md "wikilink")。自1985年，他在[新藝城電影公司開始其電影幕後工作生涯](../Page/新藝城電影公司.md "wikilink")，最初是當信差，後來再經[鄭則士轉做場記](../Page/鄭則士.md "wikilink")，繼而再做上去當副導演。曾參與超過二十部電影的拍攝工作，一連做了十年的副導演，直到1995年才有機會獨立執導電影。首次擔任電影導演的副導演，作品為《[夜半一點鐘](../Page/夜半一點鐘.md "wikilink")》。並憑1996年拍攝的《[旺角風雲](../Page/旺角風雲.md "wikilink")》獲得第三屆[香港電影評論學會推薦電影](../Page/香港電影評論學會.md "wikilink")，同時獲得最佳編劇獎。長期為許多名導打下手為葉偉信累積了不少拍片經驗，對其日後的發展產生了極大影響。

葉偉信開始做導演的兩部作品《[夜半一點鐘](../Page/夜半一點鐘.md "wikilink")》和《[迷姦犯](../Page/迷姦犯.md "wikilink")》不見有任何出奇之處，充其量只是普通的港產類型片罷了。稍後的《[旺角風雲](../Page/旺角風雲.md "wikilink")》通過一個初入黑幫的小混混的視角展示編導自己的內心的古惑世界，雖然不算有新意，但葉偉信的對生活細節的準確把握已經展露出不俗的導演功底。

葉偉信以拍鬼怪靈異片起家，而且也以擅拍此類型影片聞名。早前的《[生化壽屍](../Page/生化壽屍.md "wikilink")》主幹情節雖取自西方遊戲《[生化危機](../Page/生化危機.md "wikilink")》，卻注入了港人的本土特色。2002年的《[2002](../Page/2002_\(電影\).md "wikilink")》和《[五個嚇鬼的少年](../Page/五個嚇鬼的少年.md "wikilink")》雖然皆為鬼片，但風格路數卻大不相同。

對於大多數的港片迷而言，葉偉信最棒的電影不是他執導的票房成績不錯的《[神偷次世代](../Page/神偷次世代.md "wikilink")》或者愛情喜劇《[乾柴烈火](../Page/乾柴烈火.md "wikilink")》，相反卻是當年叫好不叫座的《[爆裂刑警](../Page/爆裂刑警.md "wikilink")》和《[朱麗葉與梁山伯](../Page/朱麗葉與梁山伯.md "wikilink")》。兩部影片一部火爆激烈,
於類型片中刻劃入情入理的刑警內心戲;
另一部看似死水微瀾，以為又是一個俗套的古惑仔故事，實則張力十足，一對平凡男女的愛情故事被演繹得淡而有味，回味無窮。其他作品如《[大城小事](../Page/大城小事_\(電影\).md "wikilink")》，《[殺破狼](../Page/殺破狼.md "wikilink")》和《[龍虎門](../Page/龍虎門_\(電影\).md "wikilink")》，反應也不錯。

## 事业佳境

2009年初《[葉問](../Page/葉問_\(電影\).md "wikilink")》在香港和內地分别取得2000多万和1億元的好票房，好評如潮最终获得[第28届香港電影金像獎最佳电影](../Page/第28届香港電影金像獎.md "wikilink")，將一貫低調的葉偉信推至台前，知名度大增。2010年上映的《[葉問2](../Page/葉問2.md "wikilink")》在香港收获过4000万票房，为近年来华语片在港非常罕见的佳绩；同时也在台湾和内地分别收获了1亿多台币和2.2亿人民币，成为他目前为止商业上最成功的电影。

## 電影作品

### 導演

  - 1995年 《[夜半一點鐘](../Page/夜半一點鐘.md "wikilink")》
  - 1995年 《[迷姦犯](../Page/迷姦犯.md "wikilink")》
  - 1996年 《[旺角風雲](../Page/旺角風雲.md "wikilink")》
  - 1997年 《[迴轉壽屍](../Page/迴轉壽屍.md "wikilink")》
  - 1997年 《[誤人子弟](../Page/誤人子弟.md "wikilink")》
  - 1998年 《[生化壽屍](../Page/生化壽屍.md "wikilink")》
  - 1999年 《[爆裂刑警](../Page/爆裂刑警.md "wikilink")》
  - 2000年 《[朱麗葉與梁山伯](../Page/朱麗葉與梁山伯.md "wikilink")》
  - 2000年 《[神偷次世代](../Page/神偷次世代.md "wikilink")》
  - 2001年 《[2002](../Page/2002_\(電影\).md "wikilink")》
  - 2002年 《[乾柴烈火](../Page/乾柴烈火.md "wikilink")》
  - 2002年 《[五個嚇鬼的少年](../Page/五個嚇鬼的少年.md "wikilink")》
  - 2004年 《[大城小事](../Page/大城小事_\(電影\).md "wikilink")》
  - 2004年 《[小白龍情海翻波](../Page/小白龍情海翻波.md "wikilink")》
  - 2005年 《[殺破狼](../Page/殺破狼.md "wikilink")》
  - 2006年 《[龍虎門](../Page/龍虎門_\(電影\).md "wikilink")》
  - 2007年 《[導火綫](../Page/導火綫_\(電影\).md "wikilink")》
  - 2008年 《[葉問](../Page/葉問_\(電影\).md "wikilink")》
  - 2009年 《[葉問2](../Page/葉問2.md "wikilink")》
  - 2011年 《[倩女幽魂](../Page/倩女幽魂_\(2011年電影\).md "wikilink")》
  - 2011年 《[開心魔法](../Page/開心魔法.md "wikilink")》
  - 2015年 《[衝上雲霄](../Page/衝上雲霄_\(電影\).md "wikilink")》
  - 2016年 《[葉問3](../Page/葉問3.md "wikilink")》
  - 2017年 《[殺破狼·貪狼](../Page/殺破狼·貪狼.md "wikilink")》
  - 2019年 《[葉問4](../Page/葉問4.md "wikilink")》

### 執行導演

  - 1994年 《[青春火花](../Page/青春火花.md "wikilink")》
  - 1996年 《[鬼劇院之驚青艷女郎](../Page/鬼劇院之驚青艷女郎.md "wikilink")》

### 第二組導演

  - 2001年 《[4X100 水著份子](../Page/4X100_水著份子.md "wikilink")》

### 副導演

  - 1987年 《[心跳一百](../Page/心跳一百.md "wikilink")》
  - 1988年 《[肥貓流浪記](../Page/肥貓流浪記.md "wikilink")》
  - 1989年 《[瀟酒先生](../Page/瀟酒先生.md "wikilink")》
  - 1990年 《[瘦虎肥龍](../Page/瘦虎肥龍.md "wikilink")》
  - 1991年 《[正紅旗下](../Page/正紅旗下.md "wikilink")》
  - 1992年 《[花貨](../Page/花貨.md "wikilink")》
  - 1993年 《[一九四九之劫後英雄傳](../Page/一九四九之劫後英雄傳.md "wikilink")》
  - 1994年 《[新天龍八部之天山童姥](../Page/新天龍八部之天山童姥.md "wikilink")》

### 演員

  - 1996年 《[大內密探零零發](../Page/大內密探零零發.md "wikilink")》飾演 嫖客
  - 2001年 《[願望樹](../Page/願望樹.md "wikilink")》飾演
    [警察](../Page/警察.md "wikilink")
  - 2001年 《[恐怖熱線之大頭怪嬰](../Page/恐怖熱線之大頭怪嬰.md "wikilink")》飾演 律師
  - 2002年 《[當男人變成女人](../Page/當男人變成女人.md "wikilink")》
  - 2002年 《[絕世好B](../Page/絕世好B.md "wikilink")》飾演 CID葉Sir
  - 2002年 《[走火槍](../Page/走火槍.md "wikilink")》
  - 2002年 《[新紮師妹](../Page/新紮師妹.md "wikilink")》飾演
    [救護人員](../Page/救護人員.md "wikilink")
  - 2002年 《[風流家族](../Page/風流家族.md "wikilink")》
  - 2004年 《[太陽無知](../Page/太陽無知.md "wikilink")》
  - 2007年 《[戲王之王](../Page/戲王之王.md "wikilink")》飾演
    [警察](../Page/警察.md "wikilink")
  - 2010年 《[最強囍事](../Page/最強囍事.md "wikilink")》飾演 神父

## 獎項

### 獲獎

  - 1996年
    第三屆[香港電影評論學會大獎推荐电影獎](../Page/香港電影評論學會大獎.md "wikilink")《[旺角風雲](../Page/旺角風雲.md "wikilink")》
  - 1999年
    第六屆[香港電影評論學會大獎最佳編劇獎](../Page/香港電影評論學會大獎.md "wikilink")《[爆裂刑警](../Page/爆裂刑警.md "wikilink")》
  - 2009年
    第四屆[香港電影導演會年度大獎年度杰出导演](../Page/香港電影導演會年度大獎.md "wikilink")《[葉問](../Page/叶问_\(電影\).md "wikilink")》
  - 2009年
    第十六屆[北京大學生電影節頒獎典禮最受歡迎導演獎](../Page/北京大學生電影節.md "wikilink")《[葉問](../Page/叶问_\(電影\).md "wikilink")》

### 提名

  - 2001年
    [第20屆香港電影金像獎](../Page/第20屆香港電影金像獎.md "wikilink")[最佳導演提名](../Page/香港電影金像獎最佳導演.md "wikilink")《[朱麗葉與梁山伯](../Page/朱麗葉與梁山伯.md "wikilink")》
  - 2009年
    [第28屆香港電影金像獎最佳導演提名](../Page/第28屆香港電影金像獎.md "wikilink")《[葉問](../Page/叶问_\(電影\).md "wikilink")》
  - 2011年
    [第30屆香港電影金像獎最佳導演提名](../Page/第30屆香港電影金像獎.md "wikilink")《[叶问2](../Page/叶问2.md "wikilink")》
  - 2016年
    [第35屆香港電影金像獎最佳導演提名](../Page/第35屆香港電影金像獎.md "wikilink")《[叶问3](../Page/叶问3.md "wikilink")》
  - 2018年
    [第37屆香港電影金像獎最佳導演提名](../Page/第37屆香港電影金像獎.md "wikilink")《[殺破狼·貪狼](../Page/殺破狼·貪狼.md "wikilink")》

## 相關人物

  - [甄子丹](../Page/甄子丹.md "wikilink")
  - [鄭則士](../Page/鄭則士.md "wikilink")
  - [馬偉豪](../Page/馬偉豪.md "wikilink")
  - [鄭保瑞](../Page/鄭保瑞.md "wikilink")
  - [鄒凱光](../Page/鄒凱光.md "wikilink")
  - [司徒錦源](../Page/司徒錦源.md "wikilink")

## 注釋

## 參考資料

## 外部連結

  - [中國影視資料館 －
    葉偉信](https://web.archive.org/web/20090602111532/http://hk.cnmdb.com/name/43765)

  -
  -
  - \[<http://lcsd.gov.hk/CE/>.../HKFA/chinese/newsletter02/c-38-more-happenings.pdf
    座談會 － 葉偉信\]

  -
[Category:香港电影导演](../Category/香港电影导演.md "wikilink")
[Category:香港编剧](../Category/香港编剧.md "wikilink")
[Category:新亞中學校友](../Category/新亞中學校友.md "wikilink")
[Category:五育中學校友](../Category/五育中學校友.md "wikilink")
[WEI](../Category/葉姓.md "wikilink")

1.