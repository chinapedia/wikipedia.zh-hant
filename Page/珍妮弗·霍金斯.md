**珍妮弗·霍金斯** （ **Jennifer
Hawkins**，），生于[澳大利亞](../Page/澳大利亞.md "wikilink")[新南威爾士州](../Page/新南威爾士州.md "wikilink")[紐卡斯爾市](../Page/纽卡斯爾_\(新南威爾士\).md "wikilink")，是2004年的[世界環球小姐](../Page/世界環球小姐.md "wikilink")
冠軍得主。現在是[澳洲九號電視台](../Page/澳洲九號電視台.md "wikilink")──《大地縱橫》（《*[The Great
Out-Doors](../Page/:en:The_Great_Out-Doors.md "wikilink")*》）節目的主持人。她在2006年獲得由[香港](../Page/香港.md "wikilink")[恆基兆業與](../Page/恆基兆業.md "wikilink")[中華煤氣有限公司合作發展的](../Page/中華煤氣有限公司.md "wikilink")[翔龍灣形象大使一職](../Page/翔龍灣.md "wikilink")。\[1\]\[2\]

## 外部連結

  - [澳洲環球小姐選美公式網址](http://www.missuniverse.com.au/)
  - [澳洲九號電視臺公式網址](http://www.ninemsn.com.au/)
  - [珍妮弗粉絲的(fans)俱樂部](http://www.jenniferhawkinsonline.com)
  - [澳洲墨爾本《世紀報》(The
    Age)報導](http://www.theage.com.au/photogallery/2004/09/03/1093939108249.html)
  - [《娛樂澳洲》(EntertainOz)專文](http://www.entertainoz.com.au/speakers.cfm?oid=15171)
  - [《首爾時報》(Seoul
    Times)載文](http://theseoultimes.com/ST/?url=/ST/db/read.php?idx=601)\<\!--
  - [Exquisite
    Soul](http://www.geocities.com/exquisite_souls/H/jenniferhawkins.html)--\>
  - [《愛斯赫文》(Askmen)扎記珍妮弗·霍金斯小姐](https://web.archive.org/web/20080515225124/http://www.askmen.com/women/models_250/251_jennifer_hawkins.html)

## 參考

<references />

[Category:环球小姐冠军](../Category/环球小姐冠军.md "wikilink")
[Category:澳大利亞主持人](../Category/澳大利亞主持人.md "wikilink")
[Category:新南威爾斯人](../Category/新南威爾斯人.md "wikilink")

1.  [恆基兆業地產有限公司：「翔龍灣」形象大使、環姐Jennifer
    Hawkins主持花車宣傳活動；示範單位首天開放予公眾人士，錄得約8,000人踴躍參觀](http://www.hld.com/chi/news/getnews.asp?id=54)
2.  [恆基兆業地產有限公司：「翔龍灣」Universe Crown寰宇冠邸 ，環姐Jennifer
    Hawkins競艷，明日正式開放示範單位](http://www.hld.com/chi/news/getnews.asp?id=52)