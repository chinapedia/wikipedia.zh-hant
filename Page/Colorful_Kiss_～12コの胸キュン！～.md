《****》是[戲畫在](../Page/戲畫_\(遊戲品牌\).md "wikilink")2003年3月14日發售的[戀愛冒險類型](../Page/戀愛冒險.md "wikilink")[成人遊戲](../Page/日本成人遊戲.md "wikilink")，2007年12月14日發售對應[Windows
Vista的記念版](../Page/Windows_Vista.md "wikilink")（）。和Colorful系列第二作《》在內容上並無關聯。

## 登場人物

  -











## 主題曲

[KOTOKO主唱](../Page/KOTOKO.md "wikilink")、作詞、[C.G
mix作曲](../Page/C.G_mix.md "wikilink")、[I've製作的主題曲](../Page/I've.md "wikilink")「」是網上「惹笑的[電波歌曲](../Page/電波歌曲.md "wikilink")」的代表之一。

## 參見

  - [戲畫](../Page/戲畫_\(遊戲品牌\).md "wikilink")

  - （Colorful系列第二作）

  - （Colorful系列第三作）

## 外部連結

  - [戲畫](http://www.web-giga.com/top/top.html)

  -
  - [Colorful Kiss/Colorful Heart Memorial
    Edition](http://www.web-giga.com/colorful_me/colorfulme.htm)

[Category:戲畫](../Category/戲畫.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:戀愛冒險遊戲](../Category/戀愛冒險遊戲.md "wikilink")
[Category:2003年日本成人遊戲](../Category/2003年日本成人遊戲.md "wikilink")
[Category:高中題材電子遊戲](../Category/高中題材電子遊戲.md "wikilink")