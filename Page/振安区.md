**振安区**是[辽宁省](../Page/辽宁省.md "wikilink")[丹东市下辖的一个市辖区](../Page/丹东市.md "wikilink")。面积659.73平方千米，人口17.38万。邮政编码118001。区人民政府驻珍珠街250号。

## 行政区划

下辖4个[街道办事处](../Page/街道办事处.md "wikilink")、5个[镇](../Page/镇_\(中华人民共和国\).md "wikilink")\[1\]：

  - 街道办事处：鸭绿江街道、珍珠街道、太平湾街道、金矿街道。
  - 镇：五龙背镇、楼房镇、汤山城镇、同兴镇、九连城镇。

## 参考文献

[振安区](../Category/振安区.md "wikilink")
[区](../Category/丹东区县市.md "wikilink")
[丹东](../Category/辽宁市辖区.md "wikilink")

1.   2009年丹东市行政区划统计表