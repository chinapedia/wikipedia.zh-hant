在[英國作家](../Page/英國.md "wikilink")[托爾金的](../Page/托爾金.md "wikilink")[中土大陸裡包含了許多地區](../Page/中土大陸.md "wikilink")，以下列出一些位於[阿爾達](../Page/阿爾達.md "wikilink")「太陽系」的地區，以漢語拼音順序排列。

## A

<div id="奧瑪倫島">

;[奧瑪倫島](../Page/奧瑪倫島.md "wikilink"):大湖裡的一個島嶼，是[維拉在亞爾達中央的家](../Page/維拉.md "wikilink")，兩盞巨燈的光在這裡混合。奧瑪倫島受到[米爾寇的攻擊而毀壞](../Page/魔苟斯.md "wikilink")，維拉遂遷往[維林諾](../Page/維林諾.md "wikilink")。根據一些傳說，[伊瑞西亞島和奧瑪倫島是同一個島嶼](../Page/伊瑞西亞島.md "wikilink")。

</div>

<div id="阿拉曼n">

;阿拉曼:[阿門洲北部狹窄的沿岸地區](../Page/阿門洲.md "wikilink")，於[佩羅瑞山脈以外](../Page/佩羅瑞.md "wikilink")。這地區為丘陵地帶，荒涼而寒冷。阿拉曼以北是[西爾卡瑞西海峽](../Page/西爾卡瑞西海峽.md "wikilink")，西爾卡瑞西海峽與貝爾蘭相連。米爾寇及諾多族在被流放後都遁此路前往中土大陸。

</div>

<div id="艾佛隆尼">

;艾佛隆尼:伊瑞西亞島東部城市，精靈的海港。這是帖勒瑞族精靈在這裡曾停留過一段時間時建立的，又或者是在[憤怒之戰後](../Page/憤怒之戰.md "wikilink")，精靈返回中土大陸途中所建立的。無論如何，這裡成為了一些不願意居住在維林諾的[諾多族及](../Page/諾多族.md "wikilink")[辛達族精靈的主要居所](../Page/辛達族.md "wikilink")。

</div>

  -
    這城市在人類眼中是一塊樂土，在晴朗的時候，從[努曼諾爾的](../Page/努曼諾爾.md "wikilink")[米涅爾塔瑪可以遠晀到艾佛隆尼的塔樓](../Page/米涅爾塔瑪.md "wikilink")。努曼諾爾陸沉後，這裡成為那些取[筆直航道而來的船隻的港口](../Page/筆直航道.md "wikilink")。有說[愛洛斯提理安真知晶球的主晶球放置在艾佛隆尼](../Page/愛洛斯提理安.md "wikilink")。

<!-- end list -->

  -
    托爾金似乎是從[亞瑟王傳說裡的](../Page/亞瑟王.md "wikilink")[阿瓦隆](../Page/阿瓦隆.md "wikilink")（Avalon）得到艾佛隆尼的靈感。
    </div>

<div id="阿維塔">

  - 阿維塔:位於[艾爾達瑪灣以南](../Page/艾爾達瑪灣.md "wikilink")、[佩羅瑞山脈及](../Page/佩羅瑞.md "wikilink")[貝烈蓋爾海之間的狹長地帶](../Page/貝烈蓋爾海.md "wikilink")，在[昂哥立安與](../Page/昂哥立安.md "wikilink")[魔苟斯前往中土大陸之前](../Page/魔苟斯.md "wikilink")，這裡是昂哥立安的居所。

<!-- end list -->

  -
    阿維塔的地理比阿拉曼更加狹窄，阿維塔是一片黑暗之地，那裡「有著全世界最深最厚的黑影」\[1\]，這正是取名為阿維塔的原因，阿維塔在古老的[昆雅語裡解作](../Page/昆雅語.md "wikilink")「黑暗」\[2\]。
    </div>

## B

<div id="艾爾達瑪灣">

  - [艾爾達瑪灣](../Page/艾爾達瑪灣.md "wikilink"):[阿門洲最大的海灣](../Page/阿門洲.md "wikilink")，以阿門洲東部的[艾爾達瑪命名](../Page/艾爾達瑪灣.md "wikilink")，在[昆雅語裡](../Page/昆雅語.md "wikilink")，艾爾達瑪解作「精靈的家園」。[諾多族及](../Page/諾多族.md "wikilink")[凡雅族來到這裡後](../Page/凡雅族.md "wikilink")，他們就住在這個海灣。後來[帖勒瑞族精靈也來了](../Page/帖勒瑞族.md "wikilink")，[歐希懇求](../Page/歐希.md "wikilink")[烏歐牟將](../Page/烏歐牟.md "wikilink")[伊瑞西亞島停泊在艾爾達瑪灣](../Page/伊瑞西亞島.md "wikilink")，烏歐牟答應了。諾多族精靈很想拜訪他們的同類，曼威便使歐希教曉帖勒瑞族精靈造船的技術。

## C

<div id="庫維因恩">

  - [庫維因恩](../Page/庫維因恩.md "wikilink"):[精靈甦醒之地](../Page/精靈.md "wikilink")。庫維因恩解作「甦醒的海域」，這是因為庫維因恩位於中土大陸遠東[赫爾卡內海在海灣處](../Page/赫爾卡內海.md "wikilink")。[精靈甦醒發生的遠古時代](../Page/精靈甦醒.md "wikilink")，庫維因恩在[雙樹紀遭到破壞而消失](../Page/雙樹紀.md "wikilink")（可能是由自然力量造成）。

<!-- end list -->

  -
    精靈在離開庫維因恩的時候便發生首次[精靈分裂](../Page/精靈分裂.md "wikilink")，[亞維瑞精靈決定留在中土](../Page/亞維瑞.md "wikilink")，其他精靈則起程前往[維林諾](../Page/維林諾.md "wikilink")。不知道亞維瑞精靈逗留在庫維因恩多久，但是可以肯定的是，赫爾卡內海在[憤怒之戰後經已消失](../Page/憤怒之戰.md "wikilink")。

## D

<div id="黑暗大陸">

;[黑暗大陸](../Page/黑暗大陸.md "wikilink"):中土大陸以南一片神秘的大陸，沒有[精靈和](../Page/精靈.md "wikilink")[矮人居住](../Page/矮人.md "wikilink")，一些原始人類可能曾經在這裡生活。[努曼諾爾人喜歡到黑暗大陸探險](../Page/努曼諾爾.md "wikilink")，不知道他們有沒有在黑暗大陸建立聚居地。

</div>

<div id="黑夜之門">

;黑夜之門:世界極西處的門戶，[魔苟斯在](../Page/魔苟斯.md "wikilink")[憤怒之戰失敗後被拋出這道門](../Page/憤怒之戰.md "wikilink")。這道門的來歷不明，有傳這是[維拉所造的](../Page/維拉.md "wikilink")，是太陽的通道，太陽從極東的[清晨之門升起](../Page/清晨之門.md "wikilink")。另一些說法則指這是特意為驅逐魔苟斯而設的。黑夜之門由[埃蘭迪爾駕著掛上](../Page/埃蘭迪爾.md "wikilink")[精靈寶鑽的飛船](../Page/精靈寶鑽.md "wikilink")[威基洛特把守](../Page/威基洛特.md "wikilink")。

</div>

## F

<div id="佛密諾斯">

  - 佛密諾斯:位於[維林諾以北](../Page/維林諾.md "wikilink")，是[費諾及其子的要塞](../Page/費諾.md "wikilink")，建於費諾在[提理安被驅逐後](../Page/提理安.md "wikilink")。大量[諾多族精靈跟著費諾流亡](../Page/諾多族.md "wikilink")，包括他們的君王[芬威](../Page/芬威.md "wikilink")。

<!-- end list -->

  -
    費諾的寶庫亦位於佛密諾斯，他將[精靈寶鑽藏在寶庫內](../Page/精靈寶鑽.md "wikilink")。這裡亦是魔苟斯殺死芬威的地方。佛密諾斯是一座巨大的城堡和兵工廠。這是少數在維林諾藏有軍備的地方。
    </div>

## G

<div id="清晨之門">

;[清晨之門](../Page/清晨之門.md "wikilink"):根據一些托爾金作品的早期著作，清晨之門是位於[阿爾達極東境的門戶](../Page/阿爾達.md "wikilink")。太陽行經[太虛](../Page/太虛.md "wikilink")，在每個早上從清晨之門升起。這概念在後來大概被放棄了，但仍可見於《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》一書裡。

</div>

## H

<div id="西爾卡瑞西海峽">

  - [西爾卡瑞西海峽](../Page/西爾卡瑞西海峽.md "wikilink"):又稱為堅冰海峽，是位於[阿門洲及](../Page/阿門洲.md "wikilink")[中土大陸之間永久結冰的荒地](../Page/中土大陸.md "wikilink")。冰地似乎已被打破，變成覆蓋[貝烈蓋爾海北部的](../Page/貝烈蓋爾海.md "wikilink")[流冰](../Page/流冰.md "wikilink")。

<!-- end list -->

  -
    自雙聖樹被破壞後，[魔苟斯和](../Page/魔苟斯.md "wikilink")[昂哥立安是首批能夠成功渡過西爾卡瑞西海峽的生物](../Page/昂哥立安.md "wikilink")，後來的[芬國昐及其子民也渡過了](../Page/芬國昐.md "wikilink")。[特剛的妻子](../Page/特剛.md "wikilink")[埃蘭薇死於此地](../Page/埃蘭薇.md "wikilink")。因憤怒之戰所造成的破壞，貝爾蘭陸沉，西爾卡瑞西海峽遂不復存在。
    </div>

<div id="希爾多瑞恩">

  - 希爾多瑞恩:人類甦醒之地，希爾多瑞恩意指「追隨者之地」，希爾多（追隨者）是精靈給予人類的名稱。有言希爾多瑞恩位於[中土大陸的遠東](../Page/中土大陸.md "wikilink")。

<!-- end list -->

  -
    根據一些伊甸人的傳說，[魔苟斯曾到訪希爾多瑞恩](../Page/魔苟斯.md "wikilink")，將人類從[伊露維塔一方策反](../Page/伊露維塔.md "wikilink")，人類遂受到神明的懲罰，成為須面對死亡的生物。除了是一項懲罰，這也是伊露維塔給予人類的一個特徵。為了逃離魔苟斯的魔掌，[人類之父是首批逃離希爾多瑞恩的人類](../Page/人類之父.md "wikilink")，終抵達[貝爾蘭](../Page/貝爾蘭.md "wikilink")。
    </div>

## I

<div id="伊爾瑪林">

;伊爾瑪林（Ilmarin）:[佩羅瑞山脈](../Page/佩羅瑞.md "wikilink")[泰尼魁提爾山頂峰的殿堂](../Page/泰尼魁提爾山.md "wikilink")，[曼威及](../Page/曼威.md "wikilink")[瓦爾妲在這裡觀察著阿爾達](../Page/瓦爾妲.md "wikilink")。

</div>

## L

<div id="魯曼諾爾">

  - 魯曼諾爾:中土大陸以東的虛無大陸，太陽的晨光首先在這裡出現。除了一道與[佩羅瑞山脈相連](../Page/佩羅瑞.md "wikilink")、被稱為「太陽之牆」的山脈外，有關這片大陸的地理不詳，太陽之牆的最高峰是奇羅米，奇羅米與西方的[泰尼魁提爾山相對應](../Page/泰尼魁提爾山.md "wikilink")。太陽進入阿爾達的清晨之門也位於魯曼諾爾。

<!-- end list -->

  -
    據《中土的形成》一書所述，魯曼諾爾又稱為「太陽的黑暗大陸」。[努曼諾爾人可能曾經到訪](../Page/努曼諾爾.md "wikilink")。在一些地圖裡，魯曼諾爾的地形很像[美洲](../Page/美洲.md "wikilink")，[洛磯山脈及](../Page/洛磯山脈.md "wikilink")[安地斯山脈則成為了太陽之牆](../Page/安地斯山脈.md "wikilink")，托爾金對此並沒有表示。
    </div>

## M

<div id="判決圈">

;[判決圈](../Page/判決圈.md "wikilink"):[維拉舉行會議的地點](../Page/維拉.md "wikilink")，位於[維利瑪西門外](../Page/維利瑪.md "wikilink")。

</div>

## P

<div id="佩羅瑞">

;[佩羅瑞山脈](../Page/佩羅瑞.md "wikilink"):[阿門洲的山脈](../Page/阿門洲.md "wikilink")，將[艾爾達瑪](../Page/艾爾達瑪灣.md "wikilink")、[阿拉曼及](../Page/阿拉曼.md "wikilink")[阿維塔從維林諾隔開](../Page/阿維塔.md "wikilink")。最高峰是[泰尼魁提爾山](../Page/泰尼魁提爾山.md "wikilink")，曼威的居所亦位於泰尼魁提爾山，而[曼督斯的居所似乎位於佩羅瑞山脈的北面山麓](../Page/曼督斯.md "wikilink")。

</div>

## S

<div id="赫爾卡內海">

  - 赫爾卡內海:[第一紀元時期的巨大內陸湖](../Page/第一紀元.md "wikilink")。在阿爾達形成的初期，維拉創造了[兩盞巨燈](../Page/兩盞巨燈.md "wikilink")。魔苟斯破壞了兩盞巨燈，北面的一盞形成了赫爾卡內海。

<!-- end list -->

  -
    赫爾卡內海的一個海灣[庫維因恩是精靈甦醒的位置](../Page/庫維因恩.md "wikilink")。在[大遠行期間](../Page/大遠行.md "wikilink")，精靈穿越赫爾卡內海的北面，通過[大荒原前往](../Page/大荒原.md "wikilink")[貝爾蘭](../Page/貝爾蘭.md "wikilink")。[第一紀元末憤怒之戰以後](../Page/第一紀元.md "wikilink")，地形發生重大的改變，赫爾卡內海的海水經[大海灣流出](../Page/大海灣.md "wikilink")，赫爾卡內海遂乾涸消失。

<!-- end list -->

  -
    克里斯托夫·托爾金及一些人懷疑[盧恩內海可能](../Page/盧恩內海.md "wikilink")「就是赫爾卡內海」（《珠寶之戰》，第174頁）。在《[中土世界地圖集](../Page/中土世界地圖集.md "wikilink")》，[凱倫·韋恩·方斯坦推測](../Page/凱倫·韋恩·方斯坦.md "wikilink")[魔多](../Page/魔多.md "wikilink")、[侃德及](../Page/侃德.md "wikilink")[盧恩是赫爾卡內海的遺址](../Page/盧恩.md "wikilink")，而盧恩內海及[諾南內海是赫爾卡內海殘存的部分](../Page/諾南內海.md "wikilink")。《[中土世界的人民](../Page/中土世界的人民.md "wikilink")》有資料顯示，盧恩內海在第一紀元時已經存在，但沒有迹象顯示盧恩內海是或不是赫爾卡內海。
    </div>

<div id="筆直航道">

;[筆直航道](../Page/筆直航道.md "wikilink"):突破地球彎曲的通道，穿越宇宙抵達[阿門洲](../Page/阿門洲.md "wikilink")。當地球還是平面的時候，筆直航道橫越[貝烈蓋爾海](../Page/貝烈蓋爾海.md "wikilink")。筆直航道只開放給[精靈使用](../Page/精靈.md "wikilink")，在維拉的恩典下，精靈可乘船經筆直航道抵達阿門洲。

</div>

## T

<div id="泰尼魁提爾山 ">

;[泰尼魁提爾山](../Page/泰尼魁提爾山.md "wikilink"):[佩羅瑞山脈的最高山峰](../Page/佩羅瑞.md "wikilink")，[伊爾瑪林宮殿的所在地](../Page/伊爾瑪林.md "wikilink")。泰尼魁提爾山的意思是「高大、白色的山峰」。居在的這裡的[凡雅族精靈稱之為奧伊奧羅西](../Page/凡雅族.md "wikilink")（Oiolossë）。

</div>

## U

<div id="烏塔莫">

  - [烏塔莫](../Page/烏塔莫.md "wikilink"):也稱為**烏頓**，是魔苟斯在中土大陸極北的要塞。魔多東北部的一個峽谷也名為烏頓。魔苟斯在重返阿爾達後便著手興建烏塔莫。烏塔莫位於[英格林山脈](../Page/英格林山脈.md "wikilink")，兩盞巨燈的光芒在這裡很黯淡。

## W

<div id="黑夜之牆">

  - [黑夜之牆](../Page/黑夜之牆.md "wikilink"):古時包圍阿爾達的牆壁，在東面和西面，外環海較廣闊，黑夜之牆遠離陸地，而南北兩面較狹窄。魔苟斯便越過了北面的黑夜之牆，來到阿爾達的北部。

## 參考

<div class="references-small">

<references />

</div>

[cs:Araman](../Page/cs:Araman.md "wikilink")
[es:Araman](../Page/es:Araman.md "wikilink")
[fr:Avathar](../Page/fr:Avathar.md "wikilink")
[nl:Avallónë](../Page/nl:Avallónë.md "wikilink")
[no:Avallónë](../Page/no:Avallónë.md "wikilink")
[pl:Máhanaxar](../Page/pl:Máhanaxar.md "wikilink")

[MPA](../Category/中土大陸的地理.md "wikilink")
[MA](../Category/中土大陸的列表.md "wikilink")

1.  [托爾金著](../Page/托爾金.md "wikilink")、[克里斯托夫·托爾金編](../Page/克里斯托夫·托爾金.md "wikilink")
    《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》 1977年 波士頓：霍頓·米福林（Boston:
    Houghton Mifflin） 第八章 「維林諾變為黑暗」 第七十三頁 ISBN 0-395-25730-1
2.  [托爾金著](../Page/托爾金.md "wikilink")、[克里斯托夫·托爾金編](../Page/克里斯托夫·托爾金.md "wikilink")
    《[魔苟斯的戒指](../Page/魔苟斯的戒指.md "wikilink")》 1993年 波士頓：霍頓·米福林 第二百八十四頁
    ISBN 0-395-68092-1