**林韋辰**（，），原名**林健新**，[香港男演員及主持](../Page/香港.md "wikilink")。出身無綫電視，曾是[亞洲電視藝員](../Page/亞洲電視.md "wikilink")，現為[無綫電視基本藝人合約兼](../Page/無綫電視藝員列表.md "wikilink")[邵氏兄弟藝員](../Page/邵氏兄弟.md "wikilink")。

## 背景

### 早年生活

林韋辰於[九龍](../Page/九龍.md "wikilink")[土瓜灣天光道前已婚警察宿舍長大](../Page/土瓜灣.md "wikilink")，父親是名警察。他與家中兩位胞兄和一位胞弟皆畢業於[天光道官立警察小學上午校](../Page/天光道官立警察小學.md "wikilink")。而由於童年時比較活躍，所以其父親建議他向國術師傅林煥光學習龍形拳數年，後來又轉到[香港中華基督教青年會總部跟外國人學習](../Page/香港中華基督教青年會.md "wikilink")[空手道](../Page/空手道.md "wikilink")（現為黑帶初段）。中學畢業後，當時已退休、當上了駕車師傅的父親親自教導駕駛技術，使他應徵了人生第一份工作
——
駕駛[可口可樂宣傳車去接載搖搖選手至不同校園宣傳](../Page/可口可樂.md "wikilink")。那時接載的該名[哥倫比亞籍選手教授了八式基本功](../Page/哥倫比亞.md "wikilink")，讓他領會到玩搖搖的技巧就進入了羅素國際搖搖表演隊，前往北歐、[法國](../Page/法國.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[瑞典北部等地表演](../Page/瑞典.md "wikilink")。這份工作由該名選手介紹，而他還因此以未滿20歲之齡成為那時中國區兩位搖搖大王之一。

### 入行經過

林韋辰當了3年搖搖大王後覺得這份工作不安穩，再者公司亞洲區職位的晉升機會不高，故決定返回香港打工，在一間絲花廠擔任老闆的辦公室助理。幾年後，[馮美基有意招募些習武青年入藝員訓練班培訓](../Page/馮美基.md "wikilink")，結果他經兄長[東方報業集團前副總採訪主任林健明](../Page/東方報業集團.md "wikilink")（現為[先機網董事](../Page/先機網.md "wikilink")）推薦，成功透過1986年[無綫電視第二期藝員進修班加入](../Page/無綫電視藝員訓練班.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")。而1980年代正值無綫電視招募武打演員，自幼練習[空手道的他由武術班轉到藝員班](../Page/空手道.md "wikilink")，也對演戲產生了濃厚興趣。當年同屆訓練班同學包括[鄭伊健](../Page/鄭伊健.md "wikilink")、[郭政鴻](../Page/郭政鴻.md "wikilink")、[王維德](../Page/王維德.md "wikilink")、[歐瑞偉](../Page/歐瑞偉.md "wikilink")、[李耀敬等人](../Page/李耀敬.md "wikilink")。

### 演藝歷程

林韋辰從無綫藝訓班畢業後，只能在劇中擔綱跑龍套的角色。雖然曾在《[義不容情](../Page/義不容情.md "wikilink")》、《[大時代](../Page/大時代.md "wikilink")》等劇中演出過，但仍然星運平平，於是投身[馬來西亞發展](../Page/馬來西亞.md "wikilink")，結果接拍了《[灰色都市](../Page/灰色都市.md "wikilink")》、《[壯志驕陽](../Page/壯志驕陽.md "wikilink")》、《[咫尺情仇](../Page/咫尺情仇.md "wikilink")》等多齣劇集，成為當地紅星之一。

1994年，林韋辰在[亞洲電視高薪邀約下回港發展](../Page/亞洲電視.md "wikilink")。當時他首份工作是接拍電影劇《[美夢成真](../Page/美夢成真_\(電視劇\).md "wikilink")》，自此隨即獲[余詠珊安排入](../Page/余詠珊.md "wikilink")《[今日睇真D](../Page/今日睇真D.md "wikilink")》擔任主持。1996年，他於電視劇《[再見豔陽天](../Page/再見豔陽天.md "wikilink")》中飾演文質彬彬的「方賀文」，頗受觀眾喜愛，不僅令他登上「亞視視帝」寶座，還獲選為亞視最受歡迎男演員。及後，他亦曾演出不少代表作，包括在《[國際刑警](../Page/國際刑警.md "wikilink")》內扮演硬朗、魯莽的刑警「楊火點」；在《[屋企有個肥大佬](../Page/屋企有個肥大佬.md "wikilink")》中與[鄭則士合演兄弟](../Page/鄭則士.md "wikilink")；在《[非常女警](../Page/非常女警.md "wikilink")》裏飾演「唐偉雄」；在為慶祝[香港主權移交而專門拍攝的長編劇集](../Page/香港主權移交.md "wikilink")《[97變色龍](../Page/97變色龍.md "wikilink")》中擔綱反派；在《[穆桂英大破天門陣](../Page/穆桂英大破天門陣.md "wikilink")》裡飾演內心善惡互相掙扎的「耶律皓南」。1999年，由其主演的電視劇《[南海十三郎](../Page/南海十三郎_\(電視劇\).md "wikilink")》更是《[南海十三郎](../Page/南海十三郎.md "wikilink")》相關影視作品中的經典\[1\]。

2001年，林韋辰因合約條款問題而放棄與亞洲電視續約，選擇重返無綫電視，惟未能晉身一線\[2\]，遂於2004年重回亞視，並擔任劇集《爸爸兩邊走》的男主角。由於亞視後期節目製作減少，劇集拍攝更加停頓，他主力擔任亞洲小姐競選等大型綜藝節目司儀；至2013年合約屆滿後，不再續約亞視，改到中國大陸發展\[3\]，同時管理他於2011年11月11日成立，以單車遊名義替傷健人士籌款的「千里單騎」基金。2002年，他受前藝人[何寶生啟發](../Page/何寶生.md "wikilink")，開始與[呂頌賢](../Page/呂頌賢.md "wikilink")、[吳廷燁等人一起踏單車](../Page/吳廷燁.md "wikilink")，至2012年又成立了「千里單騎單車隊」到台灣行善籌款。

2016年7月，林韋辰於活動中表示已經簽約3年，第三度重歸無綫電視\[4\]。

## 事件

### 醉酒駕駛案

林韋辰於2016年7月在[青沙公路因涉嫌酒後駕駛而被拘捕](../Page/青沙公路.md "wikilink")\[5\]。

## 家庭

林韋辰為了照顧患病的母親，於2015年遷入[上水](../Page/上水.md "wikilink")[古洞至其母親病逝](../Page/古洞.md "wikilink")。他在2017年9月接受訪問時表示，新居樓高三層，面積近二千呎，空間十足\[6\]。

## 感情生活

林韋辰曾與圈外台灣人Ivy
Lin於1989年結婚，並育有一女林詩茵（Peony），可婚姻只維持至2006年。2008年，他和[2004年亞洲小姐冠軍](../Page/亞洲小姐競選.md "wikilink")[呂晶晶拍拖](../Page/呂晶晶.md "wikilink")，惟四年後分手\[7\]\[8\]。2011年，他再跟前香港女子網球代表隊成員、[無綫電視體育節目主持](../Page/無綫電視.md "wikilink")[李思雅拍拖](../Page/李思雅.md "wikilink")\[9\]\[10\]\[11\]。

## 軼聞

  - 林韋辰是位[搖搖高手](../Page/搖搖.md "wikilink")，曾成為[羅素搖搖公司大使](../Page/羅素搖搖公司.md "wikilink")，赴[日本](../Page/日本.md "wikilink")、[法國](../Page/法國.md "wikilink")、[西班牙和](../Page/西班牙.md "wikilink")[韓國等地宣傳](../Page/韓國.md "wikilink")\[12\]\[13\]，以及到各大專上院校巡迴演出。
  - 他跟[陳啟泰及](../Page/陳啟泰.md "wikilink")[袁文傑都是](../Page/袁文傑.md "wikilink")[無綫電視出身](../Page/電視廣播有限公司.md "wikilink")，曾是[亞洲電視的當紅小生](../Page/亞洲電視.md "wikilink")，合稱「**亞視三雄**」。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1986年</p>
<center></td>
<td><p><a href="../Page/倚天屠龍記_(1986年電視劇).md" title="wikilink">倚天屠龍記</a></p></td>
<td><p>漁　民</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大班密令.md" title="wikilink">大班密令</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1987年</p>
<center></td>
<td><p><a href="../Page/書劍恩仇錄_(1987年電視劇).md" title="wikilink">書劍恩仇錄</a></p></td>
<td><p>馬公子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奇門鬼谷.md" title="wikilink">奇門鬼谷</a></p></td>
<td><p>孫　卓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新紮師兄1988.md" title="wikilink">新紮師兄1988</a></p></td>
<td><p>陳炎坤手下</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1988年</p>
<center></td>
<td><p><a href="../Page/誓不低頭_(無綫電視劇).md" title="wikilink">誓不低頭</a></p></td>
<td><p>陸國榮之司機</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/無名火.md" title="wikilink">無名火</a></p></td>
<td><p>江　龍（張毅手下）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/贏單傳奇.md" title="wikilink">贏單傳奇</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鬥氣一族.md" title="wikilink">鬥氣一族</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/太平天國_(無綫電視劇).md" title="wikilink">太平天國</a></p></td>
<td><p>團　練</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1989年</p>
<center></td>
<td><p><a href="../Page/還我本色.md" title="wikilink">還我本色</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/他來自江湖.md" title="wikilink">他來自江湖</a></p></td>
<td><p>Jimmy（第15 - 16、27集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/義不容情.md" title="wikilink">義不容情</a></p></td>
<td><p>楊柏祖</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/相愛又如何.md" title="wikilink">相愛又如何</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公私三文治.md" title="wikilink">公私三文治</a></p></td>
<td><p>Bobo 父</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飛虎群英.md" title="wikilink">飛虎群英</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天涯歌女_(無綫電視劇).md" title="wikilink">天涯歌女</a></p></td>
<td><p>醫　生</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1990年</p>
<center></td>
<td><p><a href="../Page/成功路上.md" title="wikilink">成功路上</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/打工貴族.md" title="wikilink">打工貴族</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我本善良_(無綫電視劇).md" title="wikilink">我本善良</a></p></td>
<td><p>林權手下</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孖仔孖心肝.md" title="wikilink">孖仔孖心肝</a></p></td>
<td><p>沙塵標手下</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/零點出擊.md" title="wikilink">零點出擊</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/螳螂小子.md" title="wikilink">螳螂小子</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天上凡間.md" title="wikilink">天上凡間</a></p></td>
<td><p>二郎神（第20集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1991年</p>
<center></td>
<td><p><a href="../Page/人海驕陽.md" title="wikilink">人海驕陽</a></p></td>
<td><p>被帶返警署的色魔（第20集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/幹探群英.md" title="wikilink">幹探群英</a></p></td>
<td><p>標（單元：《紅星五四》）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/今生無悔_(無綫電視劇).md" title="wikilink">今生無悔</a></p></td>
<td><p>阿　彪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三八佳人.md" title="wikilink">三八佳人</a></p></td>
<td><p>貴利財手下甲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/投奔自由.md" title="wikilink">投奔自由</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/血璽金刀.md" title="wikilink">血璽金刀</a></p></td>
<td><p>元武官</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/灰網.md" title="wikilink">灰網</a></p></td>
<td><p>律　師</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皇家鐵馬.md" title="wikilink">皇家鐵馬</a></p></td>
<td><p>師爺九（單元：《馬路狂徒》）</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1992年</p>
<center></td>
<td><p><a href="../Page/大時代.md" title="wikilink">大時代</a></p></td>
<td><p>Michael（第15 - 17、40集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妙探出更.md" title="wikilink">妙探出更</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我愛牙擦蘇.md" title="wikilink">我愛牙擦蘇</a></p></td>
<td><p>爛賭二</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九反威龍.md" title="wikilink">九反威龍</a></p></td>
<td><p>律　師</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2002年</p>
<center></td>
<td><p><a href="../Page/洛神.md" title="wikilink">洛神</a></p></td>
<td><p><a href="../Page/楊修.md" title="wikilink">楊　修</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/烈火雄心II.md" title="wikilink">烈火雄心II</a></p></td>
<td><p>唐　偉（Kelvin）</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2003年</p>
<center></td>
<td><p><a href="../Page/九五至尊.md" title="wikilink">九五至尊</a></p></td>
<td><p>岑日禮</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜</a></p></td>
<td><p>Franco Mao</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2004年</p>
<center></td>
<td><p><a href="../Page/無名天使3D.md" title="wikilink">無名天使3D</a></p></td>
<td><p>楊少峰</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楚漢驕雄.md" title="wikilink">楚漢驕雄</a></p></td>
<td><p><a href="../Page/陈平_(汉朝).md" title="wikilink">陳　平</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/棟篤神探.md" title="wikilink">棟篤神探</a></p></td>
<td><p>梁仲文</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2005年</p>
<center></td>
<td><p><a href="../Page/鄭板橋_(電視劇).md" title="wikilink">鄭板橋</a></p></td>
<td><p>蔣南沙</p></td>
</tr>
<tr class="even">
<td><center>
<p>2006年</p>
<center></td>
<td><p><a href="../Page/謎情家族.md" title="wikilink">謎情家族</a></p></td>
<td><p>張漢明</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/心理心裏有個謎.md" title="wikilink">心理心裏有個謎</a></p></td>
<td><p>萬家興</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2017年</p>
<center></td>
<td><p><a href="../Page/踩過界.md" title="wikilink">踩過界</a></p></td>
<td><p>韋國涵（Gotham）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雜警奇兵.md" title="wikilink">雜警奇兵</a></p></td>
<td><p>梁　一</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2019年</p>
<center></td>
<td><p><a href="../Page/福爾摩師奶.md" title="wikilink">福爾摩師奶</a></p></td>
<td><p>喬百川</p></td>
</tr>
<tr class="odd">
<td><center>
<p>待播映</p>
<center></td>
<td><p><a href="../Page/過街英雄.md" title="wikilink">過街英雄</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃金有罪.md" title="wikilink">黃金有罪</a></p></td>
<td><p>沈岳泰</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1995年</p>
<center></td>
<td><p><a href="../Page/美夢成真_(電視劇).md" title="wikilink">美夢成真</a></p></td>
<td><p>侯山河</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/悲情城市_(電視劇).md" title="wikilink">悲情城市</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/有房出租_(亞洲電視劇集).md" title="wikilink">有房出租之逃妻</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/城中姊妹花II子夜情人.md" title="wikilink">城中姊妹花II子夜情人</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/包青天_(亞洲電視劇集).md" title="wikilink">包青天</a></p></td>
<td><p>張少游（單元：《再世情仇》）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1996年</p>
<center></td>
<td><p><a href="../Page/真相_(亞洲電視劇集).md" title="wikilink">真相</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/再見豔陽天.md" title="wikilink">再見豔陽天</a></p></td>
<td><p><strong>方賀文</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飄零燕_(電視劇).md" title="wikilink">飄零燕</a></p></td>
<td><p>葉國良</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1997年</p>
<center></td>
<td><p><a href="../Page/國際刑警1997.md" title="wikilink">國際刑警1997</a></p></td>
<td><p><strong>楊火點</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/97變色龍.md" title="wikilink">97變色龍</a></p></td>
<td><p><strong>胡　斌</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/屋企有個肥大佬.md" title="wikilink">屋企有個肥大佬</a></p></td>
<td><p><strong>朱文佳</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/著數一族.md" title="wikilink">著數一族</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1998年</p>
<center></td>
<td><p><a href="../Page/穆桂英.md" title="wikilink">穆桂英之大破天門陣</a></p></td>
<td><p><strong>劉皓南 ／ 耶律皓南</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穆桂英.md" title="wikilink">穆桂英十二寡婦征西</a></p></td>
<td><p><strong>劉皓南 ／ 耶律皓南 ／ 張　牛</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>1999年</p>
<center></td>
<td><p><a href="../Page/非常女警.md" title="wikilink">非常女警</a></p></td>
<td><p><strong>唐偉雄</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南海十三郎_(電視劇).md" title="wikilink">南海十三郎</a></p></td>
<td><p><strong><a href="../Page/江譽鏐.md" title="wikilink">江譽鏐</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2004年</p>
<center></td>
<td><p><a href="../Page/爸爸兩邊走.md" title="wikilink">爸爸兩邊走</a></p></td>
<td><p><strong>曾耀乾</strong></p></td>
</tr>
<tr class="odd">
<td><center>
<p>2005年</p>
<center></td>
<td><p><a href="../Page/情陷夜中環.md" title="wikilink">情陷夜中環</a></p></td>
<td><p><strong>關卓雄</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爸爸向前走.md" title="wikilink">爸爸向前走</a></p></td>
<td><p><strong>曾耀乾</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2006年</p>
<center></td>
<td><p><a href="../Page/香港奇案實錄.md" title="wikilink">香港奇案實錄</a></p></td>
<td><p>張 Sir（單元：《五屍離奇命案》）</p></td>
</tr>
<tr class="even">
<td><center>
<p>2008年</p>
<center></td>
<td><p><a href="../Page/今日法庭.md" title="wikilink">今日法庭</a></p></td>
<td><p>（第41、42集）</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2009年</p>
<center></td>
<td><p><a href="../Page/東邊西邊.md" title="wikilink">東邊西邊</a></p></td>
<td><p><strong>張寶國</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇（其他）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>馬來西亞</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>？年</p>
<center></td>
<td><p><a href="../Page/圍裙DADDY.md" title="wikilink">圍裙DADDY</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>？年</p>
<center></td>
<td><p><a href="../Page/大慈善家.md" title="wikilink">大慈善家</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>？年</p>
<center></td>
<td><p><a href="../Page/灰色都市.md" title="wikilink">灰色都市</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>？年</p>
<center></td>
<td><p><a href="../Page/壯志驕陽_(電視劇).md" title="wikilink">壯志驕陽</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>？年</p>
<center></td>
<td><p><a href="../Page/咫尺情仇.md" title="wikilink">咫尺情仇</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/隱世者們.md" title="wikilink">隱世者們</a></p></td>
<td><p>楊有為</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/香港電台.md" title="wikilink">香港電台</a></strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2003年</p>
<center></td>
<td><p><a href="../Page/賭海迷徒.md" title="wikilink">賭海迷徒</a></p></td>
<td><p>（單元：《一個人在賭途上》）</p></td>
</tr>
<tr class="even">
<td><center>
<p>2015年</p>
<center></td>
<td><p><a href="../Page/證義搜查線系列.md" title="wikilink">證義搜查線3</a></p></td>
<td><p><strong>李逢源</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>內地</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>|<a href="../Page/銀河寶貝.md" title="wikilink">銀河寶貝</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/蘭陵王妃.md" title="wikilink">蘭陵王妃</a></p></td>
<td><p><a href="../Page/宇文護.md" title="wikilink">宇文護</a></p></td>
</tr>
<tr class="even">
<td><center>
<p>未播映</p>
<center></td>
<td><p><a href="../Page/108異人錄.md" title="wikilink">108異人錄</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 主持節目（無綫電視）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>節目名</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>2001年</p>
<center></td>
<td><p><a href="../Page/中華狀元紅.md" title="wikilink">中華狀元紅</a></p></td>
<td><p>與<a href="../Page/黃德如.md" title="wikilink">黃德如合作</a></p></td>
</tr>
<tr class="odd">
<td><center>
<p>2002年</p>
<center></td>
<td><p><a href="../Page/慈善星輝仁濟夜.md" title="wikilink">慈善星輝仁濟夜</a></p></td>
<td><p>與<a href="../Page/沈殿霞.md" title="wikilink">沈殿霞</a>、<a href="../Page/黃德如.md" title="wikilink">黃德如</a>、<a href="../Page/劉綽琪.md" title="wikilink">劉綽琪</a>、<a href="../Page/王賢誌.md" title="wikilink">王賢誌合作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星晨旅遊台灣真程趣.md" title="wikilink">星晨旅遊台灣真程趣</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2017年</p>
<center></td>
<td><p><a href="../Page/玩轉香港日與夜.md" title="wikilink">玩轉香港日與夜</a></p></td>
<td><p>第13集；與<a href="../Page/袁文傑.md" title="wikilink">袁文傑及</a><a href="../Page/丁子朗.md" title="wikilink">丁子朗合作</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>與<a href="../Page/譚永浩.md" title="wikilink">譚永浩</a>、<a href="../Page/黃庭鋒.md" title="wikilink">黃庭鋒及</a><a href="../Page/林伊麗.md" title="wikilink">林伊麗合作</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>與譚永浩、黃庭鋒及林伊麗合作</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td></td>
<td><p>與<a href="../Page/賴慰玲.md" title="wikilink">賴慰玲</a>、<a href="../Page/林希靈.md" title="wikilink">林希靈及</a><a href="../Page/鄒兆霆.md" title="wikilink">鄒兆霆合作</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 主持節目（亞洲電視）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>節目名</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1994年</p>
<center></td>
<td><p><a href="../Page/今日睇真D.md" title="wikilink">今日睇真D</a></p></td>
<td><p>主持之一</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2000年</p>
<center></td>
<td><p><a href="../Page/深圳故鄉的故事.md" title="wikilink">深圳故鄉的故事</a></p></td>
<td><p>第1、2集</p></td>
</tr>
<tr class="even">
<td><center>
<p>2004年</p>
<center></td>
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與<a href="../Page/陳啟泰.md" title="wikilink">陳啟泰及</a><a href="../Page/梁思浩.md" title="wikilink">梁思浩合作</a></p></td>
</tr>
<tr class="odd">
<td><center>
<p>2005年</p>
<center></td>
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與陳啟泰及袁文傑合作</p></td>
</tr>
<tr class="even">
<td><center>
<p>2006年</p>
<center></td>
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>陳啟泰、袁文傑及<a href="../Page/張嘉倫.md" title="wikilink">張嘉倫合作</a></p></td>
</tr>
<tr class="odd">
<td><center>
<p>2007年</p>
<center></td>
<td><p><a href="../Page/第一手真相.md" title="wikilink">第一手真相</a></p></td>
<td><p>與陳啟泰、袁文傑、<a href="../Page/陳展鵬.md" title="wikilink">陳展鵬</a>、<a href="../Page/秦啟維.md" title="wikilink">秦啟維</a>、<a href="../Page/李可瑩.md" title="wikilink">李可瑩</a>、<br />
<a href="../Page/黃慧敏.md" title="wikilink">黃慧敏</a>、<a href="../Page/翟鋒.md" title="wikilink">翟鋒</a>、<a href="../Page/甄思羽.md" title="wikilink">甄思羽</a>、<a href="../Page/曾敏.md" title="wikilink">曾敏及</a><a href="../Page/伍偉樂.md" title="wikilink">伍偉樂合作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧運精華遊.md" title="wikilink">奧運精華遊</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/跨國風雲.md" title="wikilink">跨國風雲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/省靚招牌.md" title="wikilink">省靚招牌</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2008年</p>
<center></td>
<td><p><a href="../Page/會嚇室.md" title="wikilink">會嚇室</a></p></td>
<td><p>與<a href="../Page/吳亭欣.md" title="wikilink">吳亭欣及</a><a href="../Page/施明.md" title="wikilink">施明合作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與陳啟泰、<a href="../Page/谷德昭.md" title="wikilink">谷德昭</a>、<a href="../Page/林曉峰.md" title="wikilink">林曉峰及</a><a href="../Page/王賢誌.md" title="wikilink">王賢誌合作</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2009年</p>
<center></td>
<td><p><a href="../Page/開心大發現2009.md" title="wikilink">開心大發現2009</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/aTV_2010亞洲電視節目巡禮.md" title="wikilink">aTV 2010亞洲電視節目巡禮</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與<a href="../Page/鄭丹瑞.md" title="wikilink">鄭丹瑞</a>、林曉峰、王賢誌及袁文傑合作</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/財神到_(遊戲節目).md" title="wikilink">財神到</a></p></td>
<td><p>2009 - 2010年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2010年</p>
<center></td>
<td><p><a href="../Page/開心大發現2010.md" title="wikilink">開心大發現2010</a></p></td>
<td><p>與鮑起靜、<a href="../Page/梁慧恩.md" title="wikilink">梁慧恩</a>、何卓瑩、<a href="../Page/蔡梓銘.md" title="wikilink">蔡梓銘及</a><a href="../Page/于天龍.md" title="wikilink">于天龍合作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中國神秘檔案.md" title="wikilink">中國神秘檔案</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011_aTV節目巡禮.md" title="wikilink">2011 aTV節目巡禮</a></p></td>
<td><p>與袁文傑合作</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與谷德昭、<a href="../Page/王健_(亞洲電視男藝人).md" title="wikilink">王健</a>、<a href="../Page/林泉.md" title="wikilink">林泉及</a><a href="../Page/管婷婷.md" title="wikilink">管婷婷合作</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2011年</p>
<center></td>
<td><p><a href="../Page/人氣春晚.md" title="wikilink">人氣春晚‧唱紅亞洲</a></p></td>
<td><p>與陳啟泰、<a href="../Page/朱慧珊.md" title="wikilink">朱慧珊</a>、<a href="../Page/鮑起靜.md" title="wikilink">鮑起靜</a>、<a href="../Page/黎燕珊.md" title="wikilink">黎燕珊</a>、<a href="../Page/袁文傑.md" title="wikilink">袁文傑及</a><br />
<a href="../Page/姚佳雯.md" title="wikilink">姚佳雯合作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/兔氣揚眉過肥年.md" title="wikilink">兔氣揚眉過肥年</a></p></td>
<td><p>與<a href="../Page/劉錫賢.md" title="wikilink">劉錫賢</a>、秦啟維、<a href="../Page/吳嘉星.md" title="wikilink">吳嘉星</a>、<a href="../Page/陳若嵐.md" title="wikilink">陳若嵐</a>、<a href="../Page/何卓瑩.md" title="wikilink">何卓瑩及</a><br />
<a href="../Page/李志豪_(藝人).md" title="wikilink">李志豪合作</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/智醒智勁四方城.md" title="wikilink">智醒智勁四方城</a></p></td>
<td><p>第二輯、與<a href="../Page/譚杏藍.md" title="wikilink">譚杏藍及</a><a href="../Page/陳蕾.md" title="wikilink">陳蕾合作</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ATV台慶勁唱會.md" title="wikilink">ATV台慶勁唱會</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香港有飯開.md" title="wikilink">香港有飯開</a></p></td>
<td><p>與<a href="../Page/何守信.md" title="wikilink">何守信</a>、<a href="../Page/鄭啟泰.md" title="wikilink">鄭啟泰</a>、<a href="../Page/姜皓文.md" title="wikilink">姜皓文</a>、劉錫賢、秦啟維、<br />
<a href="../Page/蔡國威.md" title="wikilink">蔡國威</a>、<a href="../Page/劉子葱.md" title="wikilink">劉子葱</a>、<a href="../Page/戚黛黛.md" title="wikilink">戚黛黛</a>、梁慧恩、蔡梓銘、<a href="../Page/車車.md" title="wikilink">車車及</a><a href="../Page/李麗珊_(藝人).md" title="wikilink">李麗珊合作</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/感動香港十大人物評選.md" title="wikilink">2011年度第二屆感動香港十大人物頒獎禮</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞洲先生競選.md" title="wikilink">亞洲先生競選</a></p></td>
<td><p>與戚黛黛合作</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與陳啟泰及<a href="../Page/金鈴_(亞洲電視女藝人).md" title="wikilink">金鈴合作</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2012年</p>
<center></td>
<td><p><a href="../Page/人氣春晚.md" title="wikilink">ATV2012人氣春晚‧唱紅亞洲</a></p></td>
<td><p>與陳啟泰、朱慧珊、鮑起靜、黎燕珊、袁文傑及<br />
姚佳雯合作</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/倫敦奧運開幕大派對.md" title="wikilink">倫敦奧運開幕大派對</a></p></td>
<td><p><a href="../Page/亞視.md" title="wikilink">亞視與</a><a href="../Page/無綫電視.md" title="wikilink">無綫電視聯合製作</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>亞洲先生競選</p></td>
<td><p>與<a href="../Page/張家瑩.md" title="wikilink">張家瑩</a>、<a href="../Page/顏子菲.md" title="wikilink">顏子菲及蔡國威合作</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞洲小姐競選.md" title="wikilink">亞洲小姐競選</a></p></td>
<td><p>與陳啟泰、姜皓文及蔡梓銘合作</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2013年</p>
<center></td>
<td><p><a href="../Page/人氣春晚.md" title="wikilink">ATV 2013人氣春晚 唱紅亞洲</a></p></td>
<td><p>與鮑起靜、黎燕珊、陳啟泰、顏子菲及<a href="../Page/張啟樂.md" title="wikilink">張啟樂合作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廣東黃金海岸遊.md" title="wikilink">廣東黃金海岸遊</a></p></td>
<td><p>與陳啟泰、張家瑩、<a href="../Page/姚嘉雯.md" title="wikilink">姚嘉雯</a>、<a href="../Page/蒲進.md" title="wikilink">蒲進</a>、顏子菲、<br />
劉子葱及譚杏藍合作</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 綜藝節目

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>節目名</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>無綫電視</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2001年</p>
<center></td>
<td><p><a href="../Page/2001年度萬千星輝賀台慶.md" title="wikilink">2001年度萬千星輝賀台慶</a></p></td>
<td><p>列席者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歡樂滿東華.md" title="wikilink">歡樂滿東華</a></p></td>
<td><p>演出嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2002年</p>
<center></td>
<td><p><a href="../Page/2002年度萬千星輝賀台慶.md" title="wikilink">2002年度萬千星輝賀台慶</a></p></td>
<td><p>列席者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/一觸即發.md" title="wikilink">一觸即發</a></p></td>
<td><p>嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2004年</p>
<center></td>
<td><p><a href="../Page/2004年度萬千星輝賀台慶.md" title="wikilink">2004年度萬千星輝賀台慶</a></p></td>
<td><p>列席者</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2016年</p>
<center></td>
<td><p><a href="../Page/東張西望.md" title="wikilink">東張西望</a></p></td>
<td><p>4月1日嘉賓（惜別亞視）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/myTV_SUPER.md" title="wikilink">myTV SUPER呈獻：萬千星輝睇多D</a></p></td>
<td><p>嘉賓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/myTV_SUPER.md" title="wikilink">myTV SUPER呈獻：萬千星輝放暑假</a></p></td>
<td><p>嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/今日VIP.md" title="wikilink">今日VIP</a></p></td>
<td><p>7月22日嘉賓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬千星輝賀台慶.md" title="wikilink">萬千星輝賀台慶</a></p></td>
<td><p>嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2017年</p>
<center></td>
<td><p><a href="../Page/築·動·愛.md" title="wikilink">築·動·愛</a></p></td>
<td><p>飾演 Man Sir；第2集</p></td>
</tr>
<tr class="odd">
<td><p>今日VIP</p></td>
<td><p>7月28日嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>健康儲蓄銀行</p></td>
<td><p>演出</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>今日VIP</p></td>
<td><p>11月10日嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/深夜美食團.md" title="wikilink">深夜美食團</a></p></td>
<td><p>第6集嘉賓</p></td>
</tr>
<tr class="odd">
<td><p>今日VIP</p></td>
<td><p>6月13日嘉賓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>亞洲電視</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2009年</p>
<center></td>
<td><p><a href="../Page/康熙來了節目列表_(2009年).md" title="wikilink">康熙來了</a></p></td>
<td><p>第1227集嘉賓</p></td>
</tr>
<tr class="even">
<td><center>
<p>2013年</p>
<center></td>
<td><p><a href="../Page/亞姐百人.md" title="wikilink">亞姐百人</a></p></td>
<td><p>第一輯 第28、29集嘉賓</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/有線娛樂台.md" title="wikilink">有線娛樂台</a></strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2015年</p>
<center></td>
<td><p><a href="../Page/我愛煮食男.md" title="wikilink">我愛煮食男</a></p></td>
<td><p>7月4日嘉賓[14]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>首映</strong></p></td>
<td><p><strong>電影名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1989年</p>
<center></td>
<td><p><a href="../Page/英雄重英雄.md" title="wikilink">英雄重英雄</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>1990年</p>
<center></td>
<td><p><a href="../Page/龍鳳茶樓_(電影).md" title="wikilink">龍鳳茶樓</a></p></td>
<td><p>Johnny</p></td>
</tr>
<tr class="even">
<td><center>
<p>1992年</p>
<center></td>
<td><p><a href="../Page/辣手神探.md" title="wikilink">辣手神探</a></p></td>
<td><p>Johnny Wong 手下</p></td>
</tr>
<tr class="odd">
<td><center>
<p>1993年</p>
<center></td>
<td><p><a href="../Page/正牌韋小寶之奉旨溝女.md" title="wikilink">正牌韋小寶之奉旨溝女</a></p></td>
<td><p>霞女前男友</p></td>
</tr>
<tr class="even">
<td><center>
<p>1996年</p>
<center></td>
<td><p><a href="../Page/暴劫傾情.md" title="wikilink">暴劫傾情</a></p></td>
<td><p>Tom Lee</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2001年</p>
<center></td>
<td><p><a href="../Page/忠奸道.md" title="wikilink">忠奸道</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/基因迷途.md" title="wikilink">基因迷途</a></p></td>
<td><p>顧先生</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 舞台劇

  - 2016年1月8 - 10日：《才子亂點俏佳人》 飾 **潘六如**

### 廣播劇

  - 2001年：[香港電台](../Page/香港電台.md "wikilink")《[雍正皇帝](../Page/雍正皇帝.md "wikilink")》
    飾 **劉墨林**\[15\]

### 電台節目

  - 2015年3月4日：[香港電台第二台](../Page/香港電台第二台.md "wikilink")《[守下留情](../Page/守下留情.md "wikilink")》嘉賓（近代豪俠系列十九）

## 廣告

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>產品</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1993年</p>
<center></td>
<td><p><a href="../Page/惠而浦.md" title="wikilink">惠而浦</a>「超級多心思洗衣機」</p></td>
</tr>
<tr class="odd">
<td><center>
<p>2012年</p>
<center></td>
<td><p><a href="../Page/英利按揭財務公司.md" title="wikilink">英利按揭財務公司</a>「財務聖手編」</p></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/恒昌隆.md" title="wikilink">恒昌隆</a>「追風活絡丸」</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 曾獲獎項與提名

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><center>
<p>1997年</p>
<center></td>
<td><p>1997年亞洲電視「一個台慶頒獎典禮」 —— 「最受歡迎電視劇男主角」（方賀文 - 《再見艷陽天》）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2001年</p>
<center></td>
<td><p><a href="../Page/2001年度萬千星輝賀台慶.md" title="wikilink">2001年度萬千星輝賀台慶</a>「本年度我最喜愛的拍檔（非戲劇組）」（與<a href="../Page/黃德如.md" title="wikilink">黃德如及</a><a href="../Page/簡慕華.md" title="wikilink">簡慕華</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2002年</p>
<center></td>
<td><p><a href="../Page/2002年度萬千星輝賀台慶.md" title="wikilink">2002年度萬千星輝賀台慶</a>「本年度我最喜愛的飛躍進步男藝員」</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>2004年</p>
<center></td>
<td><p><a href="../Page/2004年度萬千星輝賀台慶.md" title="wikilink">2004年度萬千星輝賀台慶</a>「本年度我最喜愛的飛躍進步男藝員（飛躍進步男藝員）」</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>2018年</p>
<center></td>
<td><p><a href="../Page/香港電視大獎.md" title="wikilink">2018香港電視大獎</a>「節目主持人獎」<a href="https://www.facebook.com/yautaitung/photos/a.2614008305292243/2627669760592764/?type=1&amp;theater">1</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

## 參考資料

## 外部連結

  -
  -
  -
  -
[Wai](../Page/category:林姓.md "wikilink")

[Category:香港中山人](../Category/香港中山人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港電視男演員](../Category/香港電視男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:香港電視主持人](../Category/香港電視主持人.md "wikilink")
[Category:20世紀男演員](../Category/20世紀男演員.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:無綫電視藝員訓練班](../Category/無綫電視藝員訓練班.md "wikilink")

1.
2.
3.  [亞視逃亡潮 林韋辰不續約
    陳啟泰拒白收人工](http://www.mingpaocanada.com/Tor/htm/News/20130901/HK-mck1.htm?m=0)，[明報](../Page/明報.md "wikilink")，2013年9月1日
4.
5.
6.
7.
8.
9.
10.
11.
12. [搖搖武林盟主
    林韋辰絕世好招：三角搖籃打橫飛](http://hk.apple.nextmedia.com/entertainment/art/20020224/2450735)，蘋果日報，2002年2月24日
13. [搖搖王子林韋辰出山　兩代互
    YO](http://hk.apple.nextmedia.com/supplement/culture/art/20070807/7413735)，蘋果日報，2007年8月7日
14. [密友互爆勁料大宇滴汗 -
    我愛煮食男](http://ent.i-cable.com/ci/videopage/program/48156/我愛煮食男/密友互爆勁料大宇滴汗/)
15. [香港電台廣播劇頻道 --
    雍正皇帝](http://rthk.hk/radiodrama/1classics/f_kingyc.htm)