**PCM電腦廣場**，前稱**星島電腦廣場**或**PC
Market**，是一本[香港](../Page/香港.md "wikilink")[電腦](../Page/電腦.md "wikilink")[科技](../Page/科技.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")。

《PCM
電腦廣場》曾是香港[星島新聞集團旗下刊物](../Page/星島新聞集團有限公司.md "wikilink")，為香港歷史較悠久的電腦科技雜誌，銷量在前列位置。早期逢星期二隨《[星島日報](../Page/星島日報.md "wikilink")》附送，後改為獨立發售；並改成星期一出版。

2016年1月起《PCM 電腦廣場》脫離星島新聞集團，由雜誌員工成立 Plug Media Services Ltd. 接手出版。

周刊提供各類數碼科技及[影音產品的最新消息](../Page/影音產品.md "wikilink")、電腦產品的測試報告及介紹、潮流資訊、科技界企業新聞、產品價格等。《PCM
電腦廣場》目前的主要內容也由從前以[電腦軟硬件為主](../Page/電腦軟硬件.md "wikilink")，漸漸轉向以[數碼產品為主導](../Page/數碼產品.md "wikilink")。

《PCM 電腦廣場》逢星期二出版（多數已在星期一有售），售價為15港元，一書兩冊，包括：

  - 《PCM Advance》- 主要刊載最新電腦軟硬件情報、測試報告及價格；又附錄《Biz.IT》，介紹IT在企業界使用的最新情形。
  - 《PCM Gears》- 主要報道潮流界IT新品、家庭電器及高清影音產品的介紹及測試。

根據 Synovate Media Atlas 2008 的讀者人數調查報告顯示，《PCM 電腦廣場》於 2008 年全年的讀者人數為
181,000 人；至於其主要競爭對手《e-zone》的讀者人數為 190,000人。另一傳媒調查研究 Nielsen Media Index
的數據顯示，《PCM 電腦廣場》於 2008 年全年的讀者人數為 141,000 人，《e-zone》的讀者人數則約為 200,000 人。

## 外部链接

  - [PCM 電腦廣場](http://www.pcmarket.com.hk/)

[Category:香港電腦雜誌](../Category/香港電腦雜誌.md "wikilink")
[Category:星島新聞集團](../Category/星島新聞集團.md "wikilink")