**Game Boy
Color**（简称**GBC**）是[日本](../Page/日本.md "wikilink")[任天堂公司开发的一种电池驱动彩色屏幕](../Page/任天堂.md "wikilink")[掌上游戏机](../Page/掌上游戏机.md "wikilink")，是原[Game
Boy的加强版](../Page/Game_Boy.md "wikilink")。[日本於](../Page/日本.md "wikilink")1998年10月21日發行。其最大特点在于相对原版Game
Boy使用了彩色屏幕。

Game Boy Color在日本的主要竞争对手是16位灰色掌机[Neo Geo
Pocket和](../Page/Neo_Geo_Pocket.md "wikilink")[WonderSwan](../Page/WonderSwan.md "wikilink")。不过在销售方面，Game
Boy Color仍然远远超过竞争者。SNK和万代紧接着继续推出[Neo Geo Pocket
Color和](../Page/Neo_Geo_Pocket_Color.md "wikilink")[Wonderswan
Color](../Page/Wonderswan_Color.md "wikilink")，但是这个对于任天堂的主导地位几乎没有变化。当世嘉于1997年结束支持Game
Gear，Game Boy Color在北美市场的主要竞争者就是Game Boy。直到短暂生命周期的Neo Geo Pocket
Color于1999年8月出现。

## 規格

\[1\]

  - **[處理器](../Page/處理器.md "wikilink"):**
    [夏普公司](../Page/夏普公司.md "wikilink") LR35902 (基於
    [Zilog
    Z80](../Page/Zilog_Z80.md "wikilink"))，時脈為4.194304Mhz或8.388Mhz(雙處理器模式)
  - **解析度:** 160 x 144 像素 (與 [Game
    Boy相同](../Page/Game_Boy.md "wikilink"))
  - **可顯示色彩數:** 32,768 (15位元)
  - **最大同螢幕顯示色彩數:** 56色
  - **音效:** 單聲道喇叭、3.5mm立體聲插座，音效處理晶片具備四個頻道：兩個方型波，一個任意波，一個雜波
  - **[ROM](../Page/ROM.md "wikilink"):** 8 MB
  - **[RAM](../Page/隨機存取記憶體.md "wikilink"):** 32 KB
  - **[顯示記憶體](../Page/顯示記憶體.md "wikilink"):** 16 KB
  - **電源:**2個三號[乾電池](../Page/乾電池.md "wikilink")，約能使用30小時，或3[伏特變壓器](../Page/伏特.md "wikilink")。
  - **尺寸:**75 mm x 27 mm x 133 mm

## 游戏

由于对Game Boy游戏的向下兼容，在发行之初，Game Boy
Color具有很大的可供游玩的游戏库。在超过四年的生命周期里，游戏系统聚集了576款Game
Boy Color游戏。其中大多数游戏是Game Boy Color独占，大约30%的游戏能够向下兼容Game Boy。

## 销售

Game Boy和Game Boy Color都获得了商业成功。整個Game
Boy衍生系列在日本售出3247万台，在北美售出4406万台，在其他地区售出4216万台。

## 參考資料

[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink") [Category:Game
Boy遊戲機](../Category/Game_Boy遊戲機.md "wikilink")
[Category:第五世代遊戲機](../Category/第五世代遊戲機.md "wikilink")
[Category:1998年面世的產品](../Category/1998年面世的產品.md "wikilink")

1.