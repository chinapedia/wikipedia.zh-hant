**普羅富莫事件**（[英文](../Page/英文.md "wikilink")：**Profumo
Affair**）是一件發生於1963年的[英國](../Page/英國.md "wikilink")[政治醜聞](../Page/政治醜聞.md "wikilink")，該醜聞以事件主角，時任[陸軍大臣的](../Page/陸軍大臣.md "wikilink")[約翰·普羅富莫命名](../Page/約翰·普羅富莫.md "wikilink")。

普羅富莫原是一名受重視的[保守黨](../Page/英國保守黨.md "wikilink")[內閣閣員](../Page/英國內閣.md "wikilink")，他的妻子是女演員[瓦萊莉婭·霍布森](../Page/瓦萊莉婭·霍布森.md "wikilink")（Valerie
Hobson）。而醜聞之焦點在於普羅富莫和[歌舞演員](../Page/歌舞演員.md "wikilink")[克莉絲汀·基勒](../Page/克莉絲汀·基勒.md "wikilink")（Christine
Keeler）發生的一段婚外情。在1961年，當時[倫敦有名的整骨醫生](../Page/倫敦.md "wikilink")[斯蒂芬·沃德](../Page/斯蒂芬·沃德.md "wikilink")（Stephen
Ward）在[克萊芙頓](../Page/克萊芙頓.md "wikilink")（Cliveden）舉行派對，普羅富莫和基勒就是在那裡認識的，但他們倆的關係只維持了數星期便告吹。然而，有關他們緋聞的謠言卻在1962年開始在公眾間流傳，而令人驚訝的是，原來基勒更曾同時與一位倫敦高級[海軍武官](../Page/海軍武官.md "wikilink")上校有染（伊凡諾夫是蘇聯[間諜](../Page/間諜.md "wikilink")）。

普羅富莫犯下的最大失誤，就是向[下議院撒了謊](../Page/英國下議院.md "wikilink")。在1963年3月，他在下院聲稱自己與基勒「沒有任何不適當」的關係。但到了同年6月，他卻承認了自己誤導下院。結果在6月5日，他宣佈辭去內閣的官職、下議院議員和[樞密院顧問官](../Page/英國樞密院.md "wikilink")。

他辭職後，由[丹寧勳爵撰寫的政府官方報告隨後在同年](../Page/湯姆·丹寧，丹寧男爵.md "wikilink")9月25日發表。一個月後，當時的保守黨首相[麥美倫因為病情受醜聞影響](../Page/麥美倫.md "wikilink")，宣佈辭職，並由[道格拉斯-休姆爵士接任](../Page/亞歷克·道格拉斯-休姆.md "wikilink")。

沃德後來被控賣淫，並在1963年8月[自殺身亡](../Page/自殺.md "wikilink")。基勒則被控[偽證罪成](../Page/偽證.md "wikilink")，入獄8個月。事件主角普羅富莫則在2006年3月9日逝世。

## 相關作品

有關事件在1989年翻拍成電影，名為《[醜聞](../Page/丑闻_\(1989年电影\).md "wikilink")》。

2017年，在美英合作的電視影集《王冠》第2季第10集中，大幅描寫了普羅富莫事件的影響。

## 請參見

  - [克莉絲汀·基勒](../Page/克莉絲汀·基勒.md "wikilink")
  - [約翰·普羅富莫](../Page/約翰·普羅富莫.md "wikilink")
  - [斯蒂芬·沃德](../Page/斯蒂芬·沃德.md "wikilink")

## 參考書目

  -
  -
  -
  -
  -
## 外部連結

  - [《醜聞》（1989年）](http://www.imdb.com/title/tt0098260/)

[Category:1963年英国](../Category/1963年英国.md "wikilink")
[Category:1963年政治事件](../Category/1963年政治事件.md "wikilink")
[Category:1960年代英国政治事件](../Category/1960年代英国政治事件.md "wikilink")
[Category:英国政治丑闻](../Category/英国政治丑闻.md "wikilink")