**林惠官**（）[中國國民黨籍](../Page/中國國民黨籍.md "wikilink")[連江縣政治人物](../Page/連江縣.md "wikilink")，曾任[立法委員](../Page/立法委員.md "wikilink")，鐵道工人出身，1988年涉足[工運](../Page/工運.md "wikilink")，曾任[中華民國全國總工會理事長](../Page/中華民國全國總工會.md "wikilink")。

[中華民國立法院中少數勞工出身的](../Page/中華民國立法院.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，立法委員任內仍然是[台灣鐵路管理局](../Page/台灣鐵路管理局.md "wikilink")[台北機廠技佐助理員](../Page/台北機廠.md "wikilink")。

[2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")，林惠官轉戰區域立委，代表[親民黨參與](../Page/親民黨.md "wikilink")[連江縣選舉區立委選舉](../Page/連江縣選舉區.md "wikilink")，他是唯一一位代表親民黨參選立委選舉的候選人，其餘地方選區親民黨候選人均統一以[中國國民黨提名](../Page/中國國民黨.md "wikilink")。

但在該屆中，因[中國國民黨提名](../Page/中國國民黨.md "wikilink")[曹爾忠競選連任](../Page/曹爾忠.md "wikilink")，再加上天候不佳導致其故鄉莒光鄉有大量滯外選民不及回鄉投票，故雖有同黨縣長[陳雪生加持](../Page/陳雪生.md "wikilink")，林惠官仍以118票、不足3%的些微差距三連霸失利，並退出[親民黨](../Page/親民黨.md "wikilink")，立委卸任後，林轉任馬祖酒廠董事長，2009年重回[中國國民黨](../Page/中國國民黨.md "wikilink")。

2009年5月間，在[陳雪生](../Page/陳雪生.md "wikilink")、[連江縣議長陳振清](../Page/連江縣議會.md "wikilink")、[連江縣副議長陳貴忠](../Page/連江縣議會.md "wikilink")、[連江縣議員曹以標](../Page/連江縣議員.md "wikilink")、林貽祥、陳建光、張永江等三分之二民意代表支持下，決定爭取代表[中國國民黨參選](../Page/中國國民黨.md "wikilink")[連江縣長](../Page/連江縣長.md "wikilink")，積極布局年底[2009年連江縣長選舉](../Page/2009年中華民國地方公職人員選舉.md "wikilink")。

2009年7月中，林惠官因脊椎鈣化，在[台北市立聯合醫院仁愛院區進行頸椎手術](../Page/台北市立聯合醫院.md "wikilink")，但手術後併發細菌感染與敗血症，被轉送台大醫院加護病房照護。2009年8月26日凌晨6時55分，林惠官病逝於[台大醫院](../Page/台大醫院.md "wikilink")，享年51歲。\[1\]

## [2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")

| 2008年[連江縣選舉區](../Page/連江縣選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| --------------------------------------------------------------------------------- |
| 號次                                                                                |
| 1                                                                                 |
| 2                                                                                 |
| 3                                                                                 |
| **選舉人數**                                                                          |
| **投票數**                                                                           |
| **有效票**                                                                           |
| **無效票**                                                                           |
| **投票率**                                                                           |

## 注释

## 參考文獻

## 外部連結

  - [立法院全球資訊網－立法委員－林惠官委員簡介](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00061&stage=5)
  - [立法院全球資訊網－立法委員－林惠官委員簡介](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00073&stage=6)

[category:第6屆中華民國立法委員](../Page/category:第6屆中華民國立法委員.md "wikilink")
[category:第5屆中華民國立法委員](../Page/category:第5屆中華民國立法委員.md "wikilink")

[Category:臺灣勞工運動者](../Category/臺灣勞工運動者.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:前親民黨黨員](../Category/前親民黨黨員.md "wikilink")
[Category:臺灣省立臺北工業專科學校校友](../Category/臺灣省立臺北工業專科學校校友.md "wikilink")
[Category:馬祖人](../Category/馬祖人.md "wikilink")
[H](../Category/林姓.md "wikilink")

1.  [〈前立委林惠官病逝〉](http://www.cna.com.tw/ReadNews/FirstNews_Read.aspx?magNo=4&magNum=8795&pageNo=1)