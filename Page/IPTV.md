[IPTV-Countries2015.svg](https://zh.wikipedia.org/wiki/File:IPTV-Countries2015.svg "fig:IPTV-Countries2015.svg")

**網路協定電視**（[英文](../Page/英文.md "wikilink")：**Internet Protocol
Television**，[縮寫](../Page/縮寫.md "wikilink")：**IPTV**）是[寬頻電視的一種](../Page/寬頻電視.md "wikilink")。IPTV是用[寬頻網絡作為介質傳送電視信息的一種系統](../Page/寬頻網絡.md "wikilink")，將廣播節目透過寬頻上的[網際協議](../Page/網際協議.md "wikilink")（）向訂戶傳遞[數碼電視服務](../Page/數碼電視.md "wikilink")。由於需要使用[網路](../Page/網際網路.md "wikilink")，IPTV服務供應商經常會一併提供連接[互聯網及](../Page/互聯網.md "wikilink")[IP電話等相關服務](../Page/IP電話.md "wikilink")，也可稱為「三重服務」或「三合一服務」（）。IPTV是數位電視的一種，因此普通電視機需要配合相應的[机顶盒接收頻道](../Page/机顶盒.md "wikilink")，也因此供應商通常會向客戶同時提供[隨選視訊服務](../Page/隨選視訊.md "wikilink")。雖透過寬頻網路及網際協議，但IPTV不一定透過[網際網路](../Page/網際網路.md "wikilink")，為傳輸品質會通过[局域网傳輸](../Page/局域网.md "wikilink")。

## 应用及功能

传统意义上的網路協定電視主要功能有[直播](../Page/直播.md "wikilink")、[点播](../Page/点播.md "wikilink")、[回看](../Page/回看.md "wikilink")、[时移电视](../Page/时移.md "wikilink")、[个人本地节目录制](../Page/个人本地节目录制.md "wikilink")、[个人网络节目录制](../Page/个人网络节目录制.md "wikilink")、[Web
On TV](../Page/Web_On_TV.md "wikilink")、[Flash On
TV](../Page/Flash_On_TV.md "wikilink")、在线游戏。

而跟[IMS网络相结合后的](../Page/IMS.md "wikilink")[IMS](../Page/IMS.md "wikilink")
based IPTV又可以实现来电显示、IM聊天、短讯的接收和发送、视频聊天、视频监控、电视购物、电子地图、电视互动等等功能。

## 歷史

1999年，[英國](../Page/英國.md "wikilink")[Video
Networks推出全球第一個IPTV業務](../Page/Video_Networks.md "wikilink")。

2003年，[香港](../Page/香港.md "wikilink")[電訊盈科推出IPTV業務](../Page/電訊盈科.md "wikilink")，定名[now寬-{}-頻電視](../Page/now寬頻電視.md "wikilink")。

2003年11月底，[台灣](../Page/台灣.md "wikilink")[中華電信經](../Page/中華電信.md "wikilink")[行政院新聞局核](../Page/行政院新聞局.md "wikilink")-{准}-推出IPTV業務，定名[中華電信MOD](../Page/中華電信MOD.md "wikilink")。

2005年，[中国电信与](../Page/中国电信.md "wikilink")[上海文广合作在](../Page/上海文广新闻传媒集团.md "wikilink")[上海推出IPTV业务](../Page/上海.md "wikilink")，获得中国大陆第一张IPTV服务牌照，以[BesTV百视通为品牌](../Page/百视通.md "wikilink")。随后扩展到中国主要沿海省市。

2006年10月，台灣[華人衛星電視傳播機構](../Page/華人衛星電視傳播機構.md "wikilink")（CSTV）網-{}-路事業群成員「[台灣互動電視公司](../Page/台灣互動電視公司.md "wikilink")」（TITV）在中華電信MOD推出各類頻道組合。

2006年12月，台灣[聖約翰科技大學學生成立IPTV](../Page/聖約翰科技大學.md "wikilink")「[台灣美食網-{}-路電視台](../Page/台灣美食網路電視台.md "wikilink")」。

2010年12月，台灣[壹傳媒旗下](../Page/壹傳媒.md "wikilink")[壹多媒體娛樂服務股份有限公司成立IPTV](../Page/壹多媒體娛樂服務股份有限公司.md "wikilink")「[壹網樂](../Page/壹網樂.md "wikilink")」，2012年10月31日終止服務。

2011年2月，台灣原「台灣美食網-{}-路電視台」團隊[臺灣繽紛數-{}-位多媒體有限公司成立IP電視](../Page/臺灣繽紛數位多媒體有限公司.md "wikilink")。

2013年，[中国中央电视台下属](../Page/中国中央电视台.md "wikilink")[央视网与](../Page/央视网.md "wikilink")[百视通合资成立](../Page/百视通.md "wikilink")[爱上电视传媒公司](../Page/爱上电视传媒.md "wikilink")，作为唯一中国大陆IPTV总平台。

## 相關條目

  - [類比電視](../Page/類比電視.md "wikilink")
  - [數碼電視](../Page/數碼電視.md "wikilink")
  - [寬頻電視](../Page/寬頻電視.md "wikilink")
  - [衛星電視](../Page/衛星電視.md "wikilink")
  - [地面電視](../Page/地面電視.md "wikilink")
  - [有線電視](../Page/有線電視.md "wikilink")
  - [互動電視](../Page/互動電視.md "wikilink")
  - [網絡電視](../Page/網絡電視.md "wikilink")
  - [輕量級交互多媒體環境](../Page/輕量級交互多媒體環境.md "wikilink")

## 註釋

## 参考文献

  - [International IPTV Guide](http://www.3w-tv.com)
  - [IPTV MPEG Quality of
    Experience](https://web.archive.org/web/20080309044041/http://www.shenick.com/pdfs/Testing_MPEG_IPTV_VOD_QOE.pdf)

## 外部連結

  - [ip電視](http://www.iptv.com.tw/)(台灣)
  - [台灣美食網路電視台](http://www.twsfood.com/)(台灣)
  - [中華電信MOD](https://web.archive.org/web/20110120045205/http://mod.cht.com.tw/MOD/Web/content/)(台灣)
  - [壹網樂](https://web.archive.org/web/20131128051446/http://www.nextvod.com.tw/)(台灣)
  - [儒園HQP-IPTV](https://web.archive.org/web/20170524035358/http://www.hqptv.net/)(台灣)
  - [ITU IPTV Global Standard
    Initiative](https://web.archive.org/web/20081126060537/http://www.itu.int/ITU-T/IPTV/)
  - [An Introduction to
    IPTV](http://arstechnica.com/guides/other/iptv.ars)
  - [IPTV over
    IMS](http://isoc.nl/activ/2008-SIPSIG-standards-OskarVanDeventer-IMS-basedIPTV.ppt)
  - [Assuring Quality of Experience for
    IPTV](https://web.archive.org/web/20090106170453/http://www.alcatel.com/bnd/news/ip/heavy_reading/HR_wp_Assuring_QOE_4_IPTV.pdf)
    key challenges and solution approaches for service control and
    assurance
  - [HbbTV website](http://www.hbbtv.org)
  - [DETV
    至爱中文电视-Malaysia](https://web.archive.org/web/20101002134018/http://detv.my/)

[IPTV](../Category/IPTV.md "wikilink")
[Category:隨選視訊服務](../Category/隨選視訊服務.md "wikilink")