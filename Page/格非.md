**格非**（1964年8月－），原名**刘勇**，[江蘇](../Page/江苏省.md "wikilink")[丹徒人](../Page/丹徒区.md "wikilink")，中国作家，[清华大学](../Page/清华大学.md "wikilink")[中国语言文学系](../Page/清华大学中国语言文学系.md "wikilink")[比较文学教授](../Page/比较文学.md "wikilink")\[1\]。與[余華](../Page/余華.md "wikilink")、[蘇童等並列為](../Page/蘇童.md "wikilink")20世纪80年代末90年代初的中國先鋒派作家之一。

## 生平

1985年7月畢業於[華東師範大學中文系](../Page/華東師範大學.md "wikilink")。1986年发表《追忆乌攸先生》，由此开始发表作品。1987年因发表中篇小说《迷舟》成名。1988年发表的中篇小说《褐色鸟群》富有玄奥特色，被认为是先锋文学的一部重要作品。2000年7月，毕业于华东师范大学中文系中国现当代文学专业，获博士学位。同年调任清华大学中文系。

2005年憑作品「江南三部曲」第一卷《人面桃花》奪第四屆[華語文學傳媒大獎年度傑出作家獎](../Page/華語文學傳媒大獎.md "wikilink")、第二屆[鼎鈞雙年文學獎](../Page/鼎鈞雙年文學獎.md "wikilink")。2012年「江南三部曲」第三卷《春盡江南》獲第四屆[紅樓夢獎決審團獎](../Page/紅樓夢獎.md "wikilink")。2015年「江南三部曲」全卷獲第九屆[茅盾文學獎](../Page/茅盾文學獎.md "wikilink")。2018年《望春風》獲第七屆紅樓夢獎決審團獎。

## 參考資料

[category:中華人民共和國小說家](../Page/category:中華人民共和國小說家.md "wikilink")
[category:华东师范大学校友](../Page/category:华东师范大学校友.md "wikilink")
[category:清华大学教授](../Page/category:清华大学教授.md "wikilink")
[category:镇江人](../Page/category:镇江人.md "wikilink")
[category:1964年出生](../Page/category:1964年出生.md "wikilink")

[Category:劉姓](../Category/劉姓.md "wikilink")
[Category:紅樓夢獎得主](../Category/紅樓夢獎得主.md "wikilink")
[Category:茅盾文学奖得主](../Category/茅盾文学奖得主.md "wikilink")
[Category:国际写作计划校友](../Category/国际写作计划校友.md "wikilink")
[Category:华语文学传媒大奖年度杰出作家奖得主](../Category/华语文学传媒大奖年度杰出作家奖得主.md "wikilink")

1.