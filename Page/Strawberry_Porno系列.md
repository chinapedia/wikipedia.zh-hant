**Strawberry
Porno系列**是[光榮於](../Page/光榮.md "wikilink")1982年至1985年開發並發售的[8位元電腦](../Page/8位元.md "wikilink")[十八禁遊戲系列](../Page/十八禁遊戲.md "wikilink")，系列包括《[社區主婦誘惑](../Page/#社區主婦誘惑.md "wikilink")》和《[充氣娃娃夢到了電鰻魚嗎？](../Page/#充氣娃娃夢到了電鰻魚嗎？.md "wikilink")\[1\]》兩套遊戲。此系列作品和之前同公司發售的《[Night
Life](../Page/#Night_Life.md "wikilink")》和光榮的歷史三部作（[信長之野望系列](../Page/信長之野望系列.md "wikilink")、[三國志系列](../Page/三國志_\(遊戲\).md "wikilink")、[成吉思汗系列](../Page/成吉思汗系列.md "wikilink")）被人相對地合稱為光榮的「成人三部作」。其後，光榮旗下的COMIX亦有發售[ENIX的十八禁遊戲](../Page/ENIX.md "wikilink")《[Lolita
Syndrome](../Page/Lolita_Syndrome.md "wikilink")》的續篇《[My
Lolita](../Page/My_Lolita.md "wikilink")》。

## Night Life

《Night
Life》（）是光榮於1982年發售的日本史上第一套的十八禁遊戲。和1990年代開始定型的十八禁遊戲完全不同，而且嚴格說不是一個「用來玩的遊戲」，而是幫助情侶性生活的軟件，如計算[安全日和建議](../Page/安全日.md "wikilink")[體位](../Page/性交体位.md "wikilink")。

## 社區主婦誘惑

《社區主婦誘惑》（）是Strawberry
Porno系列的第一作，於1983年發售的[PC-8801](../Page/PC-8801.md "wikilink")、[FM-7](../Page/FM-7.md "wikilink")、[FM-8電腦遊戲](../Page/FM-8.md "wikilink")。遊戲主角是一個工作於[川崎市新丸子的第日本家庭計劃](../Page/川崎市.md "wikilink")[北千住營業所的推銷員](../Page/北千住.md "wikilink")，要在一天內（9時至17時）把所有[避孕用品售出](../Page/避孕.md "wikilink")。他每次經過新[越谷小區H號館](../Page/越谷.md "wikilink")（7層）向居住的太太和女子大學生推銷貨品時都會產生下流的念頭。主角在遊戲中有「體力」、「智力」、「精力」等的設定參數，光榮稱此作的類型為「成人色情[角色扮演遊戲](../Page/角色扮演遊戲.md "wikilink")」。

## 充氣娃娃夢到了電鰻魚嗎？

《充氣娃娃夢到了電鰻魚嗎？》（）是Strawberry
Porno系列的第二作，於1985年發售。標題來自[菲利普·狄克的長編](../Page/菲利普·狄克.md "wikilink")[科幻小說](../Page/科幻小說.md "wikilink")《[银翼杀手](../Page/银翼杀手.md "wikilink")》，但和遊戲內容無關。主角會在遊戲中向女子搭訕並作進一步行動，有時會被警察追捕。

## 參見

  - [光榮](../Page/光榮.md "wikilink")
  - [十八禁遊戲](../Page/十八禁遊戲.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  - 多根清史、八霧敬次（2006年12月4日）．《》（第一章  P.9～11）．太田出版．ISBN 4-7783-1052-7

</div>

## 外部連結

  - [某有名遊戲廠商的黑歷史](http://www5f.biglobe.ne.jp/~yek-chimera/kurorekishi.htm)

[Category:1982年日本成人遊戲](../Category/1982年日本成人遊戲.md "wikilink")
[Category:PC-8001遊戲](../Category/PC-8001遊戲.md "wikilink")
[Category:PC-8800遊戲](../Category/PC-8800遊戲.md "wikilink")
[Category:FM-7系列遊戲](../Category/FM-7系列遊戲.md "wikilink")
[Category:光荣游戏](../Category/光荣游戏.md "wikilink")
[Category:日本成人遊戲系列](../Category/日本成人遊戲系列.md "wikilink")

1.  [著名游戏厂商的不可见人游戏开发史](http://games.sina.com.cn/t/n/2005-04-26/99452.shtml)（譯名參考）