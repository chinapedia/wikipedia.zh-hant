**周防大島町**（）是位于[日本](../Page/日本.md "wikilink")[山口縣東南部](../Page/山口縣.md "wikilink")[周防大島上的一个](../Page/周防大島.md "wikilink")[町](../Page/町.md "wikilink")。於2004年10月1日，由[大島郡](../Page/大島郡_\(山口縣\).md "wikilink")[大島町](../Page/大島町_\(山口縣\).md "wikilink")、[久賀町](../Page/久賀町.md "wikilink")、[橘町](../Page/橘町_\(山口縣\).md "wikilink")、[東和町](../Page/東和町_\(山口縣\).md "wikilink")4町[合并而成](../Page/市町村合併.md "wikilink")。

轄區包括周防大島以及周邊的5個[有人島及](../Page/有人島.md "wikilink")25個[無人島](../Page/無人島.md "wikilink")，並以[大島大橋與位於本土的](../Page/大島大橋.md "wikilink")[柳井市相連](../Page/柳井市.md "wikilink")。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[大島郡](../Page/大島郡_\(山口縣\).md "wikilink")[油田村](../Page/油田村.md "wikilink")、[和田村](../Page/和田村_\(山口線大島郡\).md "wikilink")、[森野村](../Page/森野村.md "wikilink")、家室西方村、安下庄村、[日良居村](../Page/日良居村.md "wikilink")、久賀村、[蒲野村](../Page/蒲野村.md "wikilink")、[屋代村](../Page/屋代村.md "wikilink")、小松志佐村、[沖浦村](../Page/沖浦村.md "wikilink")。
  - 1904年1月1日：久賀村改制為[久賀町](../Page/久賀町.md "wikilink")。
  - 1915年11月1日：安下庄村改制為[安下庄町](../Page/安下庄町.md "wikilink")。
  - 1916年6月1日：小松志佐村改制並改名為[小松町](../Page/小松町_\(山口縣\).md "wikilink")。
  - 1941年11月3日：家室西方村改名為[白木村](../Page/白木村.md "wikilink")。
  - 1952年9月15日：小松町與屋代村[合併為](../Page/市町村合併.md "wikilink")[大島町](../Page/大島町_\(山口縣\).md "wikilink")。
  - 1955年1月15日：大島町、蒲野村、沖浦村合併為新設制的大島町。
  - 1955年4月1日：油田村、和田村、森野村、白木村合併為[東和町](../Page/東和町_\(山口縣\).md "wikilink")。
  - 1955年4月10日：安下庄町和日良居村合併為[橘町](../Page/橘町_\(山口縣\).md "wikilink")。
  - 1956年4月5日：大島町大字椋野被併入久賀町。
  - 1956年4月10日：大島町大字秋的一部分被併入橘町。
  - 2004年10月1日：久賀町、大島町、東和町、橘町合併為**周防大島町**。\[1\]
  - 2018年10月22日：一艘馬爾他籍的貨輪Erna
    Oldendorff，在山口縣沿岸航行時，沒有注意到船上起重機超高，因而撞到連結離島周防大島町與柳井市的大島大橋，造成橋上附掛的自來水管及光纜等斷裂。使得周防大島町9000戶居民無水可用，需仰賴送水車送水，直到11月2日臨時自來水管才緊急完工恢復供水。肇事的印尼籍船長已被移送法辦\[2\]。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>油田村</p></td>
<td><p>1955年4月1日<br />
合併為東和町</p></td>
<td><p>2004年10月1日<br />
合併為周防大島町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>和田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>森野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>家室西方村</p></td>
<td><p>1941年11月3日<br />
改名為白木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>安下庄村</p></td>
<td><p>1915年11月1日<br />
改制為安下庄町</p></td>
<td><p>1955年4月10日<br />
合併為橘町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>日良居村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>久賀村</p></td>
<td><p>1904年1月1日<br />
改制為久賀町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>蒲野村</p></td>
<td><p>1955年1月15日<br />
合併為大島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>屋代村</p></td>
<td><p>1952年9月15日<br />
合併為大島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>小松志佐村</p></td>
<td><p>1916年6月1日<br />
改制並改名為小松町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>沖浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 海上航路

  - 町營渡船
      - 笠佐航線（[小松港](../Page/小松港.md "wikilink") -
        [笠佐島港](../Page/笠佐島港.md "wikilink")）
      - 浮島航線（[日前港](../Page/日前港.md "wikilink") -
        [江浦港](../Page/江浦港.md "wikilink")、[樽見港](../Page/樽見港.md "wikilink")）
      - 前島航線（[久賀港](../Page/久賀港.md "wikilink") -
        [前島港](../Page/前島港.md "wikilink")）
      - 情島航線（[伊保田港](../Page/伊保田港.md "wikilink") - 情島港）
  - 周防大島 松山渡輪
      - [柳井港](../Page/柳井港.md "wikilink") - 伊保田港 -
        [松山（三津濱）港](../Page/松山港.md "wikilink")

## 觀光景點

[日本ハワイ移民資料館.JPG](https://zh.wikipedia.org/wiki/File:日本ハワイ移民資料館.JPG "fig:日本ハワイ移民資料館.JPG")[移民資料館](../Page/移民.md "wikilink")\]\]
[片添ヶ浜海水浴場.jpg](https://zh.wikipedia.org/wiki/File:片添ヶ浜海水浴場.jpg "fig:片添ヶ浜海水浴場.jpg")\]\]

  - 片添濱海濱公園
  - 片添濱溫泉
  - 龍崎溫泉
  - 陸奥記念館
  - 逗子濱海水浴場
  - 星野哲郎記念館
  - 日本[夏威夷移民資料館](../Page/夏威夷.md "wikilink")

## 教育

[大島商船高等専門学校(立ち入り口）.jpg](https://zh.wikipedia.org/wiki/File:大島商船高等専門学校\(立ち入り口）.jpg "fig:大島商船高等専門学校(立ち入り口）.jpg")

### 高等專門學校

  - [大島商船高等專門學校](../Page/大島商船高等專門學校.md "wikilink")

### 高等學校

  - [山口縣立周防大島高等學校](../Page/山口縣立周防大島高等學校.md "wikilink")

## 姊妹、友好都市

### 海外

  - [可愛島](../Page/可愛島.md "wikilink")（[美國](../Page/美國.md "wikilink")[夏威夷州](../Page/夏威夷州.md "wikilink")）
      -
        於1963年3月20日締結為姊妹都市。\[3\]

## 本地出身之名人

  - [岩政大樹](../Page/岩政大樹.md "wikilink")：[職業足球選手](../Page/職業足球.md "wikilink")
  - [受田新吉](../Page/受田新吉.md "wikilink")：前[日本眾議院議員](../Page/日本眾議院.md "wikilink")
  - [嶋原清子](../Page/嶋原清子.md "wikilink")：[馬拉松選手](../Page/馬拉松.md "wikilink")
  - [星野哲郎](../Page/星野哲郎.md "wikilink")：[作詞家](../Page/作詞家.md "wikilink")
  - [宮本常一](../Page/宮本常一.md "wikilink")：民俗學者
  - [森福都](../Page/森福都.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [安廣欣記](../Page/安廣欣記.md "wikilink")：政治評論員、前[讀賣新聞](../Page/讀賣新聞.md "wikilink")[政治部次長](../Page/政治部.md "wikilink")

## 參考資料

## 外部連結

  - [周防大島觀光協會](http://www.suo-oshima-kanko.net/)

<!-- end list -->

1.
2.  [貨物船の船長らを書類送検](https://www3.nhk.or.jp/lnews/yamaguchi/20181102/4060001435.html)
3.