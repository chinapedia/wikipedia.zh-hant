**蓮花樂隊**（），香港六十年代著名英語流行樂隊，成立於1966年，前身是**Bar
Six**。成員包括[張浚英](../Page/張浚英.md "wikilink")（David
Cheung，[鼓手](../Page/鼓手.md "wikilink")）、[蘇雄](../Page/蘇雄.md "wikilink")（Danny
So，[低音結他](../Page/低音結他.md "wikilink")）、[李松江](../Page/李松江.md "wikilink")（Albert
Li，[旋律結他](../Page/旋律結他.md "wikilink")）、[周華年](../Page/周華年.md "wikilink")（Wallace
Chow，[主音結他](../Page/主音結他.md "wikilink")），[許冠傑](../Page/許冠傑.md "wikilink")（[主音歌手](../Page/主音歌手.md "wikilink")）。\[1\]

蓮花樂隊與[羅文四步合唱團也是以唱英美英語歌曲著名](../Page/羅文四步合唱團.md "wikilink")。\[2\]

## 唱片

  - [Just A Little c/w Spoonful Of
    Sugar](../Page/Just_A_Little_c/w_Spoonful_Of_Sugar.md "wikilink")（細碟
    1967）\[3\]
  - [I'll Be Waiting c/w Cute Little
    June](../Page/I'll_Be_Waiting_c/w_Cute_Little_June.md "wikilink")（細碟
    1967）\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")

1.

2.  Shoesmith, Brian. Rossiter, Ned. \[2004\] (2004). Refashioning Pop
    Music in Asia: Cosmopolitan flows, political tempos and aesthetic
    Industries. Routeledge Publishing. ISBN 0700714014

3.

4.