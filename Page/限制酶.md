[EcoRI_restriction_enzyme_recognition_site.svg](https://zh.wikipedia.org/wiki/File:EcoRI_restriction_enzyme_recognition_site.svg "fig:EcoRI_restriction_enzyme_recognition_site.svg")
[SmaI_restriction_enzyme_recognition_site.svg](https://zh.wikipedia.org/wiki/File:SmaI_restriction_enzyme_recognition_site.svg "fig:SmaI_restriction_enzyme_recognition_site.svg")
**限制酶**（）又稱**限制內切酶**或**限制性內切酶**，全稱**限制性核酸內切酶**\[1\]，是一種能將雙股[DNA切開的](../Page/DNA.md "wikilink")[酶](../Page/酶.md "wikilink")。切割方法是將醣類分子與磷酸之間的鍵結切斷，進而於兩條DNA鏈上各產生一個切口，且不破壞[核苷酸與](../Page/核苷酸.md "wikilink")[鹼基](../Page/鹼基.md "wikilink")。切割形式有兩種，分別是可產生具有突出單股DNA的黏狀末端，以及末端平整無凸起的平滑末端。\[2\]由於斷開的DNA片段可由另一種稱為[DNA連接酶的酵素黏合](../Page/DNA連接酶.md "wikilink")，因此[染色體或DNA上不同的](../Page/染色體.md "wikilink")**限-{制}-片段**，得以經由[剪接作用而結合在一起](../Page/剪接_\(遺傳學\).md "wikilink")。

限制酶在[分子生物學與](../Page/分子生物學.md "wikilink")[遺傳工程領域有廣泛的應用](../Page/遺傳工程.md "wikilink")，此類酵素最早發現於某些品系的[大腸桿菌體內](../Page/大腸桿菌.md "wikilink")，這些品系能夠「限制」[噬菌體對其感染](../Page/噬菌體.md "wikilink")，因此得名。科學家認為限制酶是細菌所演化出來對抗病毒感染，並幫助將已殖入的病毒序列移除的機制。是[限制修飾系統的一部分](../Page/限制修飾系統.md "wikilink")。[約翰霍普金斯大學的](../Page/約翰霍普金斯大學.md "wikilink")[丹尼爾·那森斯](../Page/丹尼爾·那森斯.md "wikilink")、[漢彌爾頓·史密斯與](../Page/漢彌爾頓·史密斯.md "wikilink")[伯克利加州大學的](../Page/伯克利加州大學.md "wikilink")[沃納·亞伯因為限制酶的發現及研究](../Page/沃納·亞伯.md "wikilink")，而共同獲得1978年的[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")。此酵素最早的應用之一，是用來將[胰島素基因轉殖到大腸桿菌](../Page/胰島素.md "wikilink")，使其具備生產人類胰島素的能力。

## 命名

限制酶的命名是根據細菌種類而定，以EcoRI為例：

|      |               |             |
| ---- | ------------- | ----------- |
| *E*  | *Escherichia* | （屬）         |
| *co* | *coli*        | （種）         |
| R    | RY13          | （品系）        |
| I    | 首先發現          | 在此類細菌中發現的順序 |

## 類型

根據限制酶的結構，輔因子的需求切位與作用方式，可將限制酶分為三種類型，分別是第一型（Type I）、第二型（Type II）及第三型（Type
III）。

### 第一型限制酶

同時具有修飾及限制性切割的作用；另有识别DNA上特定鹼基序列的能力，通常其切割位距離识别位点可達數千個鹼基之遠，并不能准确定位切割位点，所以并不常用。例如：*Eco*B、*Eco*K。

### 第二型限制酶

只具有限制性切割的作用，修飾作用由其他酶進行。所识别的位置多為短的[迴文序列](../Page/迴文序列.md "wikilink")；所剪切的鹼基序列通常即為所识别的序列。是遺傳工程上，實用性較高的限制酶種類。例如：*Eco*RI、*Hind*III。

### 第三型限制酶

與第一型限制酶類似，同時具有修飾及识别切割的作用。可识别短的不對稱序列，切割位與识别序列約距24-26個鹼基對，并不能准确定位切割位点，所以并不常用。例如：*Eco*PI、*Hinf*III。

## 例子

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>來源</p></th>
<th><p>识别序列</p></th>
<th><p>切割位点</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/EcoRI.md" title="wikilink"><em>Eco</em>RI</a></p></td>
<td><p><em><a href="../Page/Escherichia_coli.md" title="wikilink">Escherichia coli</a></em></p></td>
<td><p><code>5'GAATTC</code><br />
<code>3'CTTAAG</code></p></td>
<td><p><code>5'---G     AATTC---3'</code><br />
<code>3'---CTTAA     G---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BamHI.md" title="wikilink"><em>Bam</em>HI</a></p></td>
<td><p><em><a href="../Page/Bacillus_amyloliquefaciens.md" title="wikilink">Bacillus amyloliquefaciens</a></em></p></td>
<td><p><code>5'GGATCC</code><br />
<code>3'CCTAGG</code></p></td>
<td><p><code>5'---G     GATCC---3'</code><br />
<code>3'---CCTAG     G---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HindIII.md" title="wikilink"><em>Hin</em>dIII</a></p></td>
<td><p><em><a href="../Page/流感嗜血桿菌.md" title="wikilink">Haemophilus influenzae</a></em></p></td>
<td><p><code>5'AAGCTT</code><br />
<code>3'TTCGAA</code></p></td>
<td><p><code>5'---A     AGCTT---3'</code><br />
<code>3'---TTCGA     A---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/TaqI.md" title="wikilink"><em>Taq</em>I</a></p></td>
<td><p><em><a href="../Page/水生棲熱菌.md" title="wikilink">Thermus aquaticus</a></em></p></td>
<td><p><code>5'TCGA</code><br />
<code>3'AGCT</code></p></td>
<td><p><code>5'---T   CGA---3'</code><br />
<code>3'---AGC   T---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NotI.md" title="wikilink"><em>Not</em>I</a></p></td>
<td><p><em><a href="../Page/Nocardia_otitidis.md" title="wikilink">Nocardia otitidis</a></em></p></td>
<td><p><code>5'GCGGCCGC</code><br />
<code>3'CGCCGGCG</code></p></td>
<td><p><code>5'---GC   GGCCGC---3'</code><br />
<code>3'---CGCCGG   CG---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HinfI.md" title="wikilink"><em>Hin</em>fI</a></p></td>
<td><p><em><a href="../Page/流感嗜血桿菌.md" title="wikilink">Haemophilus influenzae</a></em></p></td>
<td><p><code>5'GANTC</code><br />
<code>3'CTNAG</code></p></td>
<td><p><code>5'---G   ANTC---3'</code><br />
<code>3'---CTNA   G---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Sau3A.md" title="wikilink"><em>Sau</em>3A</a></p></td>
<td><p><em><a href="../Page/金黃色葡萄球菌.md" title="wikilink">Staphylococcus aureus</a></em></p></td>
<td><p><code>5'GATC</code><br />
<code>3'CTAG</code></p></td>
<td><p><code>5'---     GATC---3'</code><br />
<code>3'---CTAG     ---3'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PovII.md" title="wikilink"><em>Pov</em>II*</a></p></td>
<td><p><em><a href="../Page/Proteus_vulgaris.md" title="wikilink">Proteus vulgaris</a></em></p></td>
<td><p><code>5'CAGCTG</code><br />
<code>3'GTCGAC</code></p></td>
<td><p><code>5'---CAG  CTG---3'</code><br />
<code>3'---GTC  GAC---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SmaI.md" title="wikilink"><em>Sma</em>I*</a></p></td>
<td><p><em><a href="../Page/粘質沙雷菌.md" title="wikilink">Serratia marcescens</a></em></p></td>
<td><p><code>5'CCCGGG</code><br />
<code>3'GGGCCC</code></p></td>
<td><p><code>5'---CCC  GGG---3'</code><br />
<code>3'---GGG  CCC---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HaeIII.md" title="wikilink"><em>Hae</em>III*</a></p></td>
<td><p><em><a href="../Page/Haemophilus_egytius.md" title="wikilink">Haemophilus egytius</a></em></p></td>
<td><p><code>5'GGCC</code><br />
<code>3'CCGG</code></p></td>
<td><p><code>5'---GG  CC---3'</code><br />
<code>3'---CC  GG---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/AluI.md" title="wikilink"><em>Alu</em>I*</a></p></td>
<td><p><em><a href="../Page/Arthrobacter_luteus.md" title="wikilink">Arthrobacter luteus</a></em></p></td>
<td><p><code>5'AGCT</code><br />
<code>3'TCGA</code></p></td>
<td><p><code>5'---AG  CT---3'</code><br />
<code>3'---TC  GA---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/EcoRV.md" title="wikilink"><em>Eco</em>RV*</a></p></td>
<td><p><em><a href="../Page/Escherichia_coli.md" title="wikilink">Escherichia coli</a></em></p></td>
<td><p><code>5'GATATC</code><br />
<code>3'CTATAG</code></p></td>
<td><p><code>5'---GAT  ATC---3'</code><br />
<code>3'---CTA  TAG---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/KpnI.md" title="wikilink"><em>Kpn</em>I</a>[3]</p></td>
<td><p><em><a href="../Page/Klebsiella_pneumonia.md" title="wikilink">Klebsiella pneumonia</a></em></p></td>
<td><p><code>5'GGTACC</code><br />
<code>3'CCATGG</code></p></td>
<td><p><code>5'---GGTAC  C---3'</code><br />
<code>3'---C  CATGG---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PstI.md" title="wikilink"><em>Pst</em>I</a>[4]</p></td>
<td><p><em><a href="../Page/Providencia_stuartii.md" title="wikilink">Providencia stuartii</a></em></p></td>
<td><p><code>5'CTGCAG</code><br />
<code>3'GACGTC</code></p></td>
<td><p><code>5'---CTGCA  G---3'</code><br />
<code>3'---G  ACGTC---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SacI.md" title="wikilink"><em>Sac</em>I</a>[5]</p></td>
<td><p><em><a href="../Page/Streptomyces_achromogenes.md" title="wikilink">Streptomyces achromogenes</a></em></p></td>
<td><p><code>5'GAGCTC</code><br />
<code>3'CTCGAG</code></p></td>
<td><p><code>5'---GAGCT  C---3'</code><br />
<code>3'---C  TCGAG---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SalI.md" title="wikilink"><em>Sal</em>I</a>[6]</p></td>
<td><p><em><a href="../Page/Streptomyces_albue.md" title="wikilink">Streptomyces albue</a></em></p></td>
<td><p><code>5'GTCGAC</code><br />
<code>3'CAGCTG</code></p></td>
<td><p><code>5'---G  TCGAC---3'</code><br />
<code>3'---CAGCT  G---5'</code></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SphI.md" title="wikilink"><em>Sph</em>I</a>[7]</p></td>
<td><p><em><a href="../Page/Streptomyces_phaeochromogenes.md" title="wikilink">Streptomyces phaeochromogenes</a></em></p></td>
<td><p><code>5'GCATGC</code><br />
<code>3'CGTACG</code></p></td>
<td><p><code>5'---G  CATGC---3'</code><br />
<code>3'---CGTAC  G---5'</code></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/XbaI.md" title="wikilink"><em>Xba</em>I</a>[8]</p></td>
<td><p><em><a href="../Page/Xanthomonas_badrii.md" title="wikilink">Xanthomonas badrii</a></em></p></td>
<td><p><code>5'TCTAGA</code><br />
<code>3'AGATCT</code></p></td>
<td><p><code>5'---T  CTAGA---3'</code><br />
<code>3'---AGATC  T---5'</code></p></td>
</tr>
<tr class="odd">
<td><p>* = 平滑末端（blunt ends）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  - [Restriction Enzyme Digestion of DNA
    Protocol](http://www.biologicalworld.com/digestions.htm)
  - [Restriction enzymes: protein data bank molecule of the
    month](http://www.rcsb.org/pdb/static.do?p=education_discussion/molecule_of_the_month/pdb8_1.html)
  - [Restriction Homepage](http://insilico.ehu.es/restriction) - Six
    different tools

{{-}}

[Category:EC 3.1](../Category/EC_3.1.md "wikilink")
[限制酶](../Category/限制酶.md "wikilink")
[Category:分子生物學](../Category/分子生物學.md "wikilink")
[Category:生物技術](../Category/生物技術.md "wikilink")

1.  [高中](../Page/高中.md "wikilink")《[生物](../Page/生物.md "wikilink")》選修 3 -
    现代生物科技专题，[人民教育出版社](../Page/人民教育出版社.md "wikilink")

2.  <http://nar.oxfordjournals.org/cgi/content/full/31/7/1805>

3.  Molecular cell biology. Lodish, Harvey F. 5. ed. : - New York : W.
    H. Freeman and Co., 2003, 973 s. b ill. ISBN 0-7167-4366-3

4.
5.
6.
7.
8.