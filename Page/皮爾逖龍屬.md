**皮爾逖龍屬**（[學名](../Page/學名.md "wikilink")：*Piveteausaurus*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[侏羅紀中期](../Page/侏羅紀.md "wikilink")（[卡洛夫階](../Page/卡洛夫階.md "wikilink")）的[歐洲](../Page/歐洲.md "wikilink")[法國](../Page/法國.md "wikilink")。牠的[化石主要是一個](../Page/化石.md "wikilink")[頭顱骨](../Page/頭顱骨.md "wikilink")，與[異特龍的相似](../Page/異特龍.md "wikilink")，但眼睛上方的角冠較不顯著。皮爾逖龍被估計長約11公尺及重2噸。

## 研究歷史

[P1050190_-_Piveteausaurus.JPG](https://zh.wikipedia.org/wiki/File:P1050190_-_Piveteausaurus.JPG "fig:P1050190_-_Piveteausaurus.JPG")
皮爾逖龍的[模式標本](../Page/模式標本.md "wikilink")（編號MNHN
1920-7）是一個部分腦殼，是在1920年由業餘地質學家發現於法國北部[諾曼第](../Page/諾曼第.md "wikilink")[卡爾瓦多斯省的Marnes](../Page/卡爾瓦多斯省.md "wikilink")
de
Dives地層，地質年代為侏儸紀中期的[牛津階](../Page/牛津階.md "wikilink")。之後化石交給[巴黎自然史博物館的教授](../Page/巴黎.md "wikilink")[馬·蒲勒](../Page/馬·蒲勒.md "wikilink")（Marcellin
Boule）\[1\]。近年的研究顯示，該地層的地質年代為上[卡洛維階](../Page/卡洛維階.md "wikilink")，約1億6100萬年前\[2\]。

在1923年，法國古生物學家[尚·皮爾逖](../Page/尚·皮爾逖.md "wikilink")（Jean
Piveteau）研究這個標本、繪製素描圖。這個腦殼的大小，相當於大型異特龍的腦殼大小\[3\]，外形類似[阿根廷的](../Page/阿根廷.md "wikilink")[皮亞尼茲基龍](../Page/皮亞尼茲基龍.md "wikilink")\[4\]。在這之前，當地曾經發現其他零碎標本，在1808年經由法國科學家[喬治·居維葉](../Page/喬治·居維葉.md "wikilink")（Georges
Cuvier）研究\[5\]。後在1861年，被[英國古生物學家](../Page/英國.md "wikilink")[理查·歐文](../Page/理查·歐文.md "wikilink")（Richard
Owen）建立為[扭椎龍的新種](../Page/扭椎龍.md "wikilink")，居氏扭椎龍（*Streptospondylus
cuvieri*）\[6\]。尚·皮爾逖將這個新發現的腦殼，歸類於居氏美\\扭椎龍\[7\]。

在1964年，[艾力克·沃克](../Page/艾力克·沃克.md "wikilink")（Alick
Walker）在研究[肉食龍下目的演化與](../Page/肉食龍下目.md "wikilink")[鳥鱷時](../Page/鳥鱷.md "wikilink")，他認為這個腦殼是屬於美扭椎龍的新種，於是將這個標本獨立為新種，蒂弗美扭椎龍（*E.
divesensis*）\[8\]。為了圖方便，沃克將被歸類於居氏美扭椎龍的零碎標本，同時改歸類於蒂弗美扭椎龍，但普遍不被接受。\[9\]後來於1977年，才由[菲利普·塔丘特](../Page/菲利普·塔丘特.md "wikilink")（Philippe
Taquet）及Samuel Paul
Welles成立了獨立的屬，**皮爾逖龍**（*Piveteausaurus*），屬名是以最初的研究者尚·皮爾逖為名；[模式種為](../Page/模式種.md "wikilink")**蒂弗皮爾逖龍**（*P.
divesensis*）\[10\]。在1988年，[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
S.
Paul）將其歸類於[原角鼻龍](../Page/原角鼻龍.md "wikilink")，成為蒂弗原角鼻龍（*Proceratosaurus
divesensis*）\[11\]，但沒有其他科學家採用這個歸類法\[12\]\[13\]。

由於目前只有發現一個腦殼，造成皮爾逖龍在分類上的困難。皮爾逖龍曾先後被被認為是[角鼻龍](../Page/角鼻龍.md "wikilink")\[14\]、[美扭椎龍](../Page/美扭椎龍.md "wikilink")\[15\]\[16\]、[原角鼻龍的近親](../Page/原角鼻龍.md "wikilink")\[17\]，也曾被歸類於美扭椎龍、原角鼻龍的一個種。

## 分類

皮爾逖龍最初被歸類於[斑龍科](../Page/斑龍科.md "wikilink")；斑龍科在當時是個「未分類物種集中地」，許多無法分類的物種被歸類於此科。在2004年，[湯瑪斯·荷茲](../Page/湯瑪斯·荷茲.md "wikilink")（Thomas
Holtz）等人把皮爾逖龍歸類於[堅尾龍類的分類未定位屬](../Page/堅尾龍類.md "wikilink")，是種原始堅尾龍類，但當時沒有提出[種系發生學分析](../Page/種系發生學.md "wikilink")\[18\]。在2010年的一份種系發生學研究，提到皮爾逖龍的分類難以確定，但有可能屬於斑龍科\[19\]。如同其他原始堅尾龍類恐龍，皮爾逖龍是種大型、二足[肉食性恐龍](../Page/肉食性.md "wikilink")\[20\]。

## 參考資料

<div class="references-small">

<references>

</references>

  -

</div>

## 外部連結

  - <https://web.archive.org/web/20070927222057/http://www.users.qwest.net/~jstweet1/tetanurae.htm>
  - [*Piveteausaurus*](https://archive.is/20121219224900/home.comcast.net/~eoraptor/Carnosauria.htm%23Piveteausaurusdivesensis)
    in the Theropod Database
  - [*Piveteausaurus*](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=38593)
    in the Paleobiology Database

[Category:堅尾龍類](../Category/堅尾龍類.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")

1.

2.

3.

4.  Rauhut, 2004. Braincase structure of the Middle Jurassic theropod
    dinosaur *Piatnitzkysaurus*. Canadian Journal of Earth Science. 41,
    1109-1122.

5.

6.
7.
8.
9.
10.

11.

12.
13.

14.

15.
16.
17.
18.
19.

20.