****是历史上第九十三次航天飞机任务，也是[发现号航天飞机的第二十六次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[肯特·罗明格](../Page/肯特·罗明格.md "wikilink")**（，曾执行、、、以及任务），指令长
  - **[里克·赫斯本德](../Page/里克·赫斯本德.md "wikilink")**（，曾执行以及任务），飞行员
  - **[艾伦·奥查](../Page/艾伦·奥查.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[塔玛拉·杰尼根](../Page/塔玛拉·杰尼根.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[丹尼尔·巴利](../Page/丹尼尔·巴利.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[朱莉·佩耶特](../Page/朱莉·佩耶特.md "wikilink")**（，[加拿大宇航员](../Page/加拿大.md "wikilink")，曾执行任务），任务专家
  - **[瓦列里·托卡列夫](../Page/瓦列里·托卡列夫.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行、[联盟TMA-7以及](../Page/联盟TMA-7.md "wikilink")[远征12号任务](../Page/远征12号.md "wikilink")），任务专家

[Category:1999年美国](../Category/1999年美国.md "wikilink")
[Category:1999年科學](../Category/1999年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1999年5月](../Category/1999年5月.md "wikilink")
[Category:1999年6月](../Category/1999年6月.md "wikilink")