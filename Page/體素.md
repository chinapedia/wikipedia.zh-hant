**體素**（，或**立體像素**），是**體積像素**（Volume
Pixel）的簡稱。概念上類似[二維空間的最小單位](../Page/二維空間.md "wikilink")——[像素](../Page/像素.md "wikilink")，像素用在[二維電腦圖像的影像資料上](../Page/二維電腦圖像.md "wikilink")。體積像素一如其名，是[數位資料於](../Page/數位.md "wikilink")[三維空間分割上的最小單位](../Page/三維空間.md "wikilink")，應用于[三維成像](../Page/三維成像.md "wikilink")、[科學資料與](../Page/科學.md "wikilink")[醫學影像等領域](../Page/醫學影像.md "wikilink")。有些真正的[三維顯示器運用體素來描述它們的](../Page/三維顯示器.md "wikilink")[解析度](../Page/解析度.md "wikilink")，舉例來說：可以顯示512×512×512體素的顯示器。

如同[像素](../Page/像素.md "wikilink")，體素本身並不含有空間中位置的資料(即它們的[座標](../Page/座標系统.md "wikilink"))，然而卻可以從它們相對於其他體素的位置來推敲，意即它們在構成單一張體積影像的資料結構中的位置。

## 體素資料

体素用恒定的[标量或者](../Page/标量.md "wikilink")[向量表示一个立体的区域](../Page/向量.md "wikilink")，体素的边界在于相邻晶格的中间位置。这样，“体素”这个术语仅仅用来表示最邻近的插值，而不用来表示如三次线性、立方等等高次插值，这些情况可以用单元体积分支来表示。

体素的数值可以表示不同的特性。在[CT扫描中](../Page/CT.md "wikilink")，这些值是[Hounsfield
单位](../Page/Hounsfield_单位.md "wikilink")，表示身体对于 X
光的不透光性。在[MRI或者](../Page/MRI.md "wikilink")[超声诊断学中会得到不同类型的数值](../Page/超声诊断学.md "wikilink")。

体素可以包含本质上是[向量的多个](../Page/向量.md "wikilink")[标量数值](../Page/标量.md "wikilink")。在
B
模式[超声扫描以及](../Page/超声.md "wikilink")[多普勒数据中](../Page/超声诊断学.md "wikilink")，在同一个体素位置的[密度与](../Page/密度.md "wikilink")[流速经过独立通道获取](../Page/流速.md "wikilink")。

如[曲面法线与](../Page/曲面法线.md "wikilink")[颜色这样的一些其它数值可能对直接三维](../Page/颜色.md "wikilink")[渲染非常有用](../Page/渲染.md "wikilink")。

## 用途

### 視覺化

包含体素的立体可以通过[立体渲染或者提取给定阈值轮廓的](../Page/立体渲染.md "wikilink")[多边形等值面表现出来](../Page/多边形.md "wikilink")。[marching
cubes](../Page/marching_cubes.md "wikilink") 算法经常用于等值面提取，当然也有其它一些方法。

### 電腦遊戲

  - 许多 [NovaLogic](../Page/NovaLogic.md "wikilink")
    计算机游戏已经使用开始使用基于体素的渲染技术，其中包括[三角洲部队
    (游戏)系列游戏](../Page/三角洲部队_\(游戏\).md "wikilink")。
  - [Westwood Studios](../Page/Westwood_Studios.md "wikilink") *[Command
    & Conquer: Tiberian
    series](../Page/Command_&_Conquer:_Tiberian_series.md "wikilink")*
    游戏使用体素渲染车辆。
  - 现在已经破产的[比利时](../Page/比利时.md "wikilink")[视频游戏开发商](../Page/视频游戏开发商.md "wikilink")
    Appeal 开发的 [Outcast](../Page/Outcast_\(game\).md "wikilink")
    的户外风景就是体素引擎渲染生成的。
  - [Sega Saturn](../Page/Sega_Saturn.md "wikilink") 的视频游戏 A+M+O+K
    在场景中也使用了体素。

## 枝微末節

In the minimalist webcomic
*[Pixel](../Page/Pixel_\(webcomic\).md "wikilink")*, in which pixels
inside a computer are the main characters, one 'race' of supporting
characters are the voxels, who have the "supernatural" power of moving
in three dimensions.

## 相關條目

  - [立体渲染](../Page/立体渲染.md "wikilink")
  - [CT](../Page/CT.md "wikilink")
  - [磁振造影](../Page/磁振造影.md "wikilink")

## 外部連結

  - [Volex](https://web.archive.org/web/20060218004341/http://www.ucsi.edu.my/research/projects.html),
    基于 LED 的[立体显示器](../Page/立体显示器.md "wikilink")
  - [Voxel3D,
    基于体素的建模软件](https://web.archive.org/web/20060710175307/http://everygraph.com/frame.php?contents=product)
  - [Voxlap, Ken Silverman
    所写的开放源代码的体素引擎](http://advsys.net/ken/voxlap.htm)
  - [HVox,
    另外一个基于体素的地形引擎](https://web.archive.org/web/20060914053107/http://www.p0werup.de/)
  - [Iehovah,
    一个用于实时显示的、基于立体的曲面生成库](http://www.home.zonnet.nl/petervenis)
  - [Geek,
    使用花边噪声生成自然形状几何图形的体素地形引擎](https://web.archive.org/web/20060704021424/http://www.flipcode.org/cgi-bin/fcarticles.cgi?show=62853)
  - [Cavernosa,
    基于二进制等级体素网格的地形与洞穴刻画工具](https://web.archive.org/web/20080725064128/http://www.btinternet.com/~ahcox/Cavernosa/index.html)
  - [一个解释如何象 Commanche/Outcast 那样用 C++
    语言绘制体素地形的教程](http://www.massal.net/article/voxel/)

[Category:三维成像](../Category/三维成像.md "wikilink")
[Category:三维计算机图形学](../Category/三维计算机图形学.md "wikilink")