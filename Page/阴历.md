**陰曆**（又稱**太陰曆**，[英語](../Page/英語.md "wikilink")：****）在[天文学中与](../Page/天文学.md "wikilink")[阳曆對應](../Page/阳曆.md "wikilink")，指主要按[月亮的](../Page/月亮.md "wikilink")[月相周期来安排的](../Page/月相.md "wikilink")[曆法](../Page/曆法.md "wikilink")；不據地球圍繞太陽公轉軌道位置。它的一年有12个[朔望月](../Page/朔望月.md "wikilink")，约354或355日。主要根據月亮繞[地球運行一周時間為一個月](../Page/地球.md "wikilink")，稱為朔望月，大約29.530588日，分為大月30日、小月29日。

纯粹的陰曆有[伊斯蘭曆](../Page/伊斯蘭曆.md "wikilink")，而大部分通常说的陰曆实际上根据现代学者都是[阴阳曆](../Page/阴阳曆.md "wikilink")，例如全世界所有华人及[朝鲜](../Page/朝鲜.md "wikilink")、[韩国](../Page/韩国.md "wikilink")、中国和[越南及](../Page/越南.md "wikilink")[明治維新前的](../Page/明治維新.md "wikilink")[日本均有使用的](../Page/日本.md "wikilink")[農曆](../Page/農曆.md "wikilink")。

在[農業气象学中](../Page/農業气象学.md "wikilink")，陰曆略微不同於农历、殷曆、古曆、舊曆，是指[中国傳統上使用的農曆](../Page/中国.md "wikilink")。而在[天文学中认为農曆實際上是一種](../Page/天文学.md "wikilink")[陰陽曆](../Page/陰陽曆.md "wikilink")。

在古代[農業](../Page/農業.md "wikilink")[經濟中](../Page/經濟.md "wikilink")，春天播種、秋天收耕，本來陽曆應更能反映農業週期，但不少古代曆法都是由月亮算起，一個推測是黑夜中的月亮特別容易觀察，月亮盈虧一目了然，直至天文技術成熟後，他們才能觀察到太陽在曆法中的作用[1](http://www.math.nus.edu.sg/aslaksen/calendar/cal.pdf)。

## 参見

  - [阴阳历](../Page/阴阳历.md "wikilink")
  - [陽曆](../Page/陽曆.md "wikilink")
  - [陰曆新年 (消歧義)](../Page/陰曆新年_\(消歧義\).md "wikilink")

## 外部链接

  - [陰曆超過1300年世界各地數千年來（英文）](http://www.rodurago.net/en/index.php?site=details&link=calendar)
  - [中国日历](http://staticlake-hrd.appspot.com/cnCalendar/index.html)
  - [中華民國行政院新聞局國曆/農曆對照表](https://web.archive.org/web/20080220045622/http://www.gio.gov.tw/info/festival_c/calendar/calendar.htm)
  - [农历月历
    (附黄历)](https://web.archive.org/web/20080509165622/http://chinesecalendar.orados.com/)

[L](../Category/曆法.md "wikilink") [L](../Category/天文学.md "wikilink")