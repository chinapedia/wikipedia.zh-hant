2005年，[美国电影学会从](../Page/美国电影学会.md "wikilink")100份候选名单中选出25部最优秀电影配乐作为美国电影学院的“百年百大”系列的一个重要部分——“**AFI百年百大电影配乐**”（**AFI's
100 Years of Film Scores**）。

[约翰·威廉姆斯的作品在](../Page/约翰·威廉姆斯.md "wikilink")25部电影里就独占了3部，还包括第一名的《[星际大战四部曲：曙光乍现](../Page/星际大战四部曲：曙光乍现.md "wikilink")》。

## 百年百大电影配乐（按照时间排序）

1.  [金刚](../Page/金刚_\(1933年电影\).md "wikilink") (1933) —
    [马克斯·史坦纳](../Page/马克斯·史坦纳.md "wikilink")
2.  [侠盗罗宾汉](../Page/侠盗罗宾汉_\(1938年电影\).md "wikilink") (1938) —
    [康果爾德](../Page/康果爾德.md "wikilink")
3.  [乱世佳人](../Page/乱世佳人.md "wikilink") (1939) — 马克斯·史坦纳
4.  [罗娜秘记](../Page/罗娜秘记.md "wikilink") (1944) — 大卫·拉克辛
5.  [日落大道](../Page/日落大道.md "wikilink") (1950) — 弗朗茨·沃克斯曼
6.  [欲望号街车](../Page/欲望号街车.md "wikilink") (1951) — 亚历克士·诺斯
7.  [-{zh-hans:正午;zh-hk:日正当中;zh-tw:日正当中;}-](../Page/日正当中.md "wikilink")
    (1952) — 迪米特里·迪奥姆金
8.  [码头风云](../Page/码头风云.md "wikilink") (1954) —
    [雷納德·伯恩斯坦](../Page/雷納德·伯恩斯坦.md "wikilink")
9.  [迷魂记](../Page/迷魂记.md "wikilink") (1958) — 伯纳德·赫尔曼
10. [-{zh-hans:宾虚;zh-hk:賓漢;zh-tw:賓漢;}-](../Page/賓漢.md "wikilink") (1959)
    — 罗饶·米克罗斯
11. [惊魂记](../Page/惊魂记.md "wikilink") (1960) — 伯纳德·赫尔曼
12. [豪勇七蛟龙](../Page/豪勇七蛟龙.md "wikilink") (1960) — 艾玛伯恩斯坦
13. [阿拉伯的劳伦斯](../Page/阿拉伯的劳伦斯_\(电影\).md "wikilink") (1962) — 莫里斯·贾尔
14. [杀死一只知更鸟](../Page/怪屋疑雲.md "wikilink") (1962) — 艾玛伯恩斯坦
15. [西部开拓史](../Page/西部开拓史.md "wikilink") (1962) — 艾佛瑞·纽曼
16. [粉红豹](../Page/粉红豹.md "wikilink") (1964) —
    [亨利·曼西尼](../Page/亨利·曼西尼.md "wikilink")
17. [人猿星球](../Page/人猿星球.md "wikilink") (1968) — 杰里·戈德史密斯
18. [教父](../Page/教父_\(电影\).md "wikilink") (1972) — 尼诺·罗塔
19. [唐人街](../Page/唐人街_\(电影\).md "wikilink") (1974) — 杰里·戈德史密斯
20. [大白鲨](../Page/大白鲨_\(电影\).md "wikilink") (1975) —
    [約翰·威廉斯](../Page/約翰·威廉斯.md "wikilink")
21. [-{zh-hans:星际大战四部曲：新希望;zh-hk:星际大战四部曲：曙光乍现;zh-tw:星际大战四部曲：曙光乍现;}-](../Page/星际大战四部曲：曙光乍现.md "wikilink")
    (1977) — [約翰·威廉斯](../Page/約翰·威廉斯.md "wikilink")
22. [金色池塘](../Page/金色池塘.md "wikilink") (1981) — 戴夫·格鲁辛
23. [E.T. 外星人](../Page/E.T._外星人.md "wikilink") (1982) —
    [約翰·威廉斯](../Page/約翰·威廉斯.md "wikilink")
24. [走出非洲](../Page/走出非洲.md "wikilink") (1985) —
    [約翰·巴瑞](../Page/約翰·巴瑞.md "wikilink")
25. [战火浮生](../Page/战火浮生.md "wikilink") (1986) — 恩尼奥·莫里科内

## （按评分排序）

1.  [-{zh-hans:星际大战四部曲：新希望;zh-hk:星际大战四部曲：曙光乍现;zh-tw:星际大战四部曲：曙光乍现;}-](../Page/星际大战四部曲：曙光乍现.md "wikilink")
    (1977) — [约翰·威廉姆斯](../Page/约翰·威廉斯_\(作曲家\).md "wikilink")
2.  [乱世佳人](../Page/乱世佳人.md "wikilink") (1939) —
    [马克斯·史坦纳](../Page/马克斯·史坦纳.md "wikilink")
3.  [阿拉伯的劳伦斯](../Page/阿拉伯的劳伦斯_\(电影\).md "wikilink") (1962) —
    [莫里斯·贾尔](../Page/莫里斯·贾尔.md "wikilink")
4.  [惊魂记](../Page/惊魂记.md "wikilink") (1960) —
    [伯纳德·赫尔曼](../Page/伯纳德·赫尔曼.md "wikilink")
5.  [教父](../Page/教父_\(电影\).md "wikilink") (1972) —
    [尼诺·罗塔](../Page/尼诺·罗塔.md "wikilink")
6.  [大白鲨](../Page/大白鲨_\(电影\).md "wikilink") (1975) —
    [约翰·威廉姆斯](../Page/约翰·威廉姆斯.md "wikilink")
7.  [罗娜秘记](../Page/罗娜秘记.md "wikilink") (1944) —
    [大卫·拉克辛](../Page/大卫·拉克辛.md "wikilink")
8.  [豪勇七蛟龙](../Page/豪勇七蛟龙.md "wikilink") (1960) —
    [艾瑪·伯恩斯坦](../Page/艾瑪·伯恩斯坦.md "wikilink")
9.  [唐人街](../Page/唐人街_\(电影\).md "wikilink") (1974) —
    [杰里·戈德史密斯](../Page/杰里·戈德史密斯.md "wikilink")
10. [-{zh-hans:正午;zh-hk:日正当中;zh-tw:日正当中;}-](../Page/日正当中.md "wikilink")
    (1952) — [迪米特里·迪奥姆金](../Page/迪米特里·迪奥姆金.md "wikilink")
11. [侠盗罗宾汉](../Page/侠盗罗宾汉_\(1938年电影\).md "wikilink") (1938) —
    [埃里希·科恩戈尔德](../Page/埃里希·科恩戈尔德.md "wikilink")
12. [迷魂记](../Page/迷魂记.md "wikilink") (1958) —
    [伯纳德·赫尔曼](../Page/伯纳德·赫尔曼.md "wikilink")
13. [金刚](../Page/金刚_\(1933年电影\).md "wikilink") (1933) —
    [马克斯·史坦纳](../Page/马克斯·史坦纳.md "wikilink")
14. [E.T. 外星人](../Page/E.T._外星人.md "wikilink") (1982) —
    [约翰·威廉姆斯](../Page/约翰·威廉斯_\(作曲家\).md "wikilink")
15. [走出非洲](../Page/走出非洲.md "wikilink") (1985) —
    [约翰·巴瑞](../Page/约翰·巴瑞.md "wikilink")
16. [日落大道](../Page/日落大道.md "wikilink") (1950) —
    [弗朗茨·沃克斯曼](../Page/弗朗茨·沃克斯曼.md "wikilink")
17. [杀死一只知更鸟](../Page/怪屋疑雲.md "wikilink") (1962) —
    [艾瑪·伯恩斯坦](../Page/艾瑪·伯恩斯坦.md "wikilink")
18. [人猿星球](../Page/人猿星球.md "wikilink") (1968) —
    [杰里·戈德史密斯](../Page/杰里·戈德史密斯.md "wikilink")
19. [欲望号街车](../Page/欲望号街车.md "wikilink") (1951) —
    [亚历克士·诺斯](../Page/亚历克士·诺斯.md "wikilink")
20. [粉红豹](../Page/粉红豹.md "wikilink") (1964) —
    [亨利·曼西尼](../Page/亨利·曼西尼.md "wikilink")
21. [-{zh-hans:宾虚;zh-hk:賓漢;zh-tw:賓漢;}-](../Page/賓漢.md "wikilink") (1959)
    — [米克罗斯·罗兹萨](../Page/米克罗斯·罗兹萨.md "wikilink")
22. [码头风云](../Page/码头风云.md "wikilink") (1954) —
    [伦纳德·伯恩斯坦](../Page/伦纳德·伯恩斯坦.md "wikilink")
23. [战火浮生](../Page/战火浮生.md "wikilink") (1986) —
    [恩尼奥·莫里科内](../Page/恩尼奥·莫里科内.md "wikilink")
24. [金色池塘](../Page/金色池塘.md "wikilink") (1981) —
    [戴夫·格鲁辛](../Page/戴夫·格鲁辛.md "wikilink")
25. [西部开拓史](../Page/西部开拓史.md "wikilink") (1962) —
    [艾佛瑞·纽曼](../Page/艾佛瑞·纽曼.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [AFI's list](http://www.afi.com/tvevents/100years/scores.aspx)

[Category:美国电影](../Category/美国电影.md "wikilink")
[Category:美国电影学院百年百大系列](../Category/美国电影学院百年百大系列.md "wikilink")
[Category:美国电影学会](../Category/美国电影学会.md "wikilink")