《**SNOW\! SNOW\!
SNOW\!**》，[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")22張[單曲](../Page/單曲.md "wikilink")，於2005年12月21日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 简介

本單曲和了前一年2004年的單曲《[Anniversary](../Page/Anniversary.md "wikilink")》一樣，是一張於年末發行的單曲。唱片封面凝造了KinKi
Kids被冰封的效果。

與單曲的標題一樣，本單曲的所有收錄歌曲皆以[冬天為形象](../Page/冬天.md "wikilink")，不論是歌名，就連印於[CD碟片上的圖案也以雪花為主](../Page/CD.md "wikilink")。雖然首批銷量比上張單曲為少，但總銷量則與上張單曲差不多。收錄歌曲方式方面，這次的方式與之前一向的方式有少許分別。因為這次初回版和通常版都會有一首只會收錄在該版本的獨有歌曲，而初回版更會收錄A面曲的純音樂版本。所以樂迷一定要同時購入兩個版本才能收聽所有歌曲。

A面曲『SNOW\! SNOW\!
SNOW\!』由曾提供過[SPEED多數暢銷歌曲的](../Page/SPEED_\(日本音樂團體\).md "wikilink")[伊秩弘將負責作曲](../Page/伊秩弘將.md "wikilink")，[秋元康負責作詞](../Page/秋元康.md "wikilink")，可謂創作暢銷歌曲的豪華組合。[歌詞就像歌名一樣](../Page/歌詞.md "wikilink")，以[雪及](../Page/雪.md "wikilink")[冬天等的字眼為中心](../Page/冬天.md "wikilink")，描寫男女之間的戀愛姿態。而且在[副歌部份](../Page/副歌.md "wikilink")，更用上了自「[夏之王者](../Page/夏之王者/除了你誰都不愛.md "wikilink")」以來的高音階。另外在發售日12月21日至12月31日期間的10日，更可以在BMB的[卡拉OK機器](../Page/卡拉OK.md "wikilink")[UGA中看到原裝的](../Page/UGA_\(卡拉OK\).md "wikilink")[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")。就是說，點選這首歌唱的話，就能看到該首歌曲的原裝音樂錄影帶片段。其實[第一興商的](../Page/第一興商.md "wikilink")[DAM系統亦一向根據與不同](../Page/DAM.md "wikilink")[唱片公司的協定](../Page/唱片公司.md "wikilink")，而使用歌手的原裝音樂錄影帶片段於卡拉OK中的（即所謂的本人映像），但以[傑尼斯相關歌曲來說](../Page/傑尼斯事務所.md "wikilink")，以這樣的方式播放還是較為罕有。也正因如此，本曲目在發售前實質上成為了UGA方面的廣告歌。

## 名稱

  - 日語原名：**SNOW\! SNOW\! SNOW\!**
  - 中文意思：**下雪啊！下雪啊！下雪啊！**
  - [香港](../Page/香港.md "wikilink")[正東譯名](../Page/正東唱片.md "wikilink")：**SNOW\!
    SNOW\! SNOW\!**
  - [台灣](../Page/台灣.md "wikilink")[艾迴譯名](../Page/艾迴唱片.md "wikilink")：**SNOW\!
    SNOW\! SNOW\!**

## 收錄歌曲

### 初回版收錄歌曲

1.  **SNOW\! SNOW\! SNOW\!**
      - 作曲：[伊秩弘將](../Page/伊秩弘將.md "wikilink")
      - 作詞：[秋元康](../Page/秋元康.md "wikilink")
      - 編曲：[家原正樹](../Page/家原正樹.md "wikilink")
2.  **雨雪**（****）
      - 作曲、作詞：[井手功二](../Page/井手功二.md "wikilink")
      - 編曲：Tadasuke
3.  **雪的繪版**（****）
      - 作曲、編曲：[中崎英也](../Page/中崎英也.md "wikilink")
      - 作詞：[篠原隆一](../Page/篠原隆一.md "wikilink")
4.  **SNOW\! SNOW\! SNOW\! (Backing Track)**
      - 作曲：伊秩弘將
      - 編曲：家原正樹

### 通常版收錄歌曲

1.  **SNOW\! SNOW\! SNOW\!**
2.  **雨雪**
3.  **雪白之月**（****）
      - 作曲：[松本良喜](../Page/松本良喜.md "wikilink")
      - 作詞：[Satomi](../Page/Satomi.md "wikilink")
      - 編曲：[十川知司](../Page/十川知司.md "wikilink")

[Category:近畿小子歌曲](../Category/近畿小子歌曲.md "wikilink")
[Category:2005年單曲](../Category/2005年單曲.md "wikilink")
[Category:2006年Oricon單曲月榜冠軍作品](../Category/2006年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:2006年Oricon單曲週榜冠軍作品](../Category/2006年Oricon單曲週榜冠軍作品.md "wikilink")
[Category:廣告歌曲](../Category/廣告歌曲.md "wikilink")
[Category:秋元康創作的歌曲](../Category/秋元康創作的歌曲.md "wikilink")
[Category:雪題材樂曲](../Category/雪題材樂曲.md "wikilink")