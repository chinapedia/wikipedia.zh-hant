**宏池会**是日本[自由民主黨的其中一個派閥](../Page/自由民主党_\(日本\).md "wikilink")。

## 概要

由[池田勇人所創](../Page/池田勇人.md "wikilink")，「宏池会」之名稱，是出自[東漢的學者](../Page/東漢.md "wikilink")[馬融所言](../Page/馬融.md "wikilink")：「棲遲乎昭明之觀，休息乎高光之榭，以臨乎**宏池**。」其實這是個[雙關](../Page/雙關.md "wikilink")，宏即廣也，指池田勇人的故鄉[廣島](../Page/廣島.md "wikilink")；池即池田。

除池田以外，共有[大平正芳](../Page/大平正芳.md "wikilink")、[鈴木善幸](../Page/鈴木善幸.md "wikilink")、[宮澤喜一共四位](../Page/宮澤喜一.md "wikilink")[首相](../Page/首相.md "wikilink")。是舊自由黨派系（[保守本流](../Page/保守本流.md "wikilink")）。由於多議員出身官僚，所以雖然政策能力強，卻不善應對政局，被日本社會稱為「[公家集團](../Page/公家.md "wikilink")」。

政策方面，歷史上沒有重大改變，在自民黨屬中間派，特別重視[安保及日美關係](../Page/安保.md "wikilink")，多為穩健鴿派。因[小泉純一郎](../Page/小泉純一郎.md "wikilink")[拜相以來傾向](../Page/拜相.md "wikilink")[新自由主義](../Page/新自由主義.md "wikilink")，使得本派系再度團結。

## 沿革

派系演變：**池田派**→**前尾派**→**大平派**→**鈴木派**→**宮澤派**→**加藤派**→（二派閥分裂中）→**古賀派**→**岸田派**。派系於2000年11月[加藤之亂時因是否支持當時宏池會會長加藤紘一對森喜朗倒閣運動而分裂](../Page/加藤之亂.md "wikilink")。

  - **加藤派**→**小里派**→**谷垣派**
  - **堀内派**→**丹羽・古賀派**→**古賀派**

兩派系於2008年5月13日合二為一。

### 結成

1957年結成以[池田勇人為中心的派系](../Page/池田勇人.md "wikilink")。
當時，[吉田茂派跟](../Page/吉田茂.md "wikilink")[佐藤榮作派形成對立的派系](../Page/佐藤榮作.md "wikilink")。池田之下有後來於1973-76年擔任眾議院議長的[前尾繁三郎](../Page/前尾繁三郎.md "wikilink")、[大平正芳](../Page/大平正芳.md "wikilink")、[黒金泰美](../Page/黒金泰美.md "wikilink")、[鈴木善幸](../Page/鈴木善幸.md "wikilink")、[宮澤喜一及](../Page/宮澤喜一.md "wikilink")[小坂善太郎等官僚系統為中心的人材](../Page/小坂善太郎.md "wikilink")。由當時經濟學者[下村治為派系訂立政策](../Page/下村治.md "wikilink")。跟其他早期派系不同，宏池會在池田以後沒有出現分裂。

### 大平派時代

宏池會在60年代由前尾繁三郎領導，及後和平交棒至[大平正芳](../Page/大平正芳.md "wikilink")。大平派三個重要人物[伊東正義](../Page/伊東正義.md "wikilink")、[齊藤邦吉及](../Page/齊藤邦吉.md "wikilink")[佐佐木義武被當時稱為](../Page/佐佐木義武.md "wikilink")「大平派三羽烏」。大平跟打破來[椎名裁定以來總裁及幹事長屬不同派系的慣例](../Page/椎名裁定.md "wikilink")，委任[齊藤邦吉為新任幹事長](../Page/齊藤邦吉.md "wikilink")。結果大平―斎藤在1979年眾議院選舉中，大平派議員增加了50人。

### 鈴木派・宮澤派時代

大平在在任期間去死，鈴木善幸成為宏池会代表（後成会長）及首相之位。派系內部出現了宮澤喜一及田中六助之間的鬥爭 (「一六戰爭」)。

鈴木首相退任後，[中曾根康弘總裁向田中派傾倒](../Page/中曾根康弘.md "wikilink")，宏池會跟主流派有更大的分歧。在田中派有以擁立派系領袖[二階堂進時](../Page/二階堂進.md "wikilink")，宏池會並沒有表態。這時候，田中六助因糖尿病惡化而離開政壇。派系亦由[宮澤喜一禪任](../Page/宮澤喜一.md "wikilink")。
在1987年時，宮澤參選[自民黨總裁](../Page/自民黨總裁.md "wikilink")，但「中曾根裁定」竹下派會長[竹下登勝出](../Page/竹下登.md "wikilink")。

宮澤喜一在輸掉眾議院選舉後辭職，但之前[加藤紘一及](../Page/加藤紘一.md "wikilink")[河野洋平之間已就派系主導權出現權力爭鬥](../Page/河野洋平.md "wikilink")（「[KK戰爭](../Page/KK戰爭.md "wikilink")」）。最後，河野脫離宏池會，由加藤繼承宏池會。派內反加藤議員成立了新派系[大勇会（現時為為公會，即麻生派）](../Page/為公会.md "wikilink")。

### 加藤之亂與派閥的分裂

當2000年11月在野黨提出對[森喜朗內閣不信任案時](../Page/森喜朗.md "wikilink")，加藤決定支持。結果，派系分裂成支持加藤及反對加藤
(以[古賀誠為中心](../Page/古賀誠.md "wikilink"))。結果，兩個派系都用宏池會為派系名，出現一名兩個派系的怪例。（加藤在2年後因秘書被逮捕而辭任會長，由[小里貞利繼任](../Page/小里貞利.md "wikilink")。在小里從政界引退後，在2005年9月26日由[谷垣禎一出現會長](../Page/谷垣禎一.md "wikilink")）。

宏池会分裂時（2000年11月 - 2008年5月），可參照下列條目。

  - [宏池会 (古賀派)](../Page/宏池会_\(古賀派\).md "wikilink")
  - [宏池会 (谷垣派)](../Page/宏池会_\(谷垣派\).md "wikilink")

### 宏池会結集構想

2006年宏池會各分支開始出現「大宏池会構想」，9月谷垣跟麻生參選總裁選後，河野派、丹羽・古賀派及谷垣派之間幹部開始討論合併事宜。但在丹羽・古賀派內部，不少人支持安倍晉三。而其中柳澤伯夫更出任安倍陣營的選舉團隊隊長
(後來獲委任為厚生勞動相)。除了[丹羽雄哉留任總務會長以外](../Page/丹羽雄哉.md "wikilink")，更有4名成員入閣。而河野派的[麻生太郎亦獲留任外相](../Page/麻生太郎.md "wikilink")，以谷垣派則完全沒有重要職位。這令討論暫時停止。

2007年9月安倍辭職後，古賀派跟谷垣派很快就支持[福田康夫](../Page/福田康夫.md "wikilink")，一下子令原先大熱的[麻生太郎失去其他所有派系的支持](../Page/麻生太郎.md "wikilink")
(「麻生包圍網」)。結果在近一年的福田政府內，古賀派及谷垣派各自有三人出任重要職位，而麻生則拒絕入閣，令其先為反主流派。這令古賀及麻生變得疏遠，總裁選舉令大宏池會合併變得十分困難。

### 古賀派・谷垣派再合併

在支持福田就任，加強了古賀両派的關係。結果在2007年末達成除麻生派外的「中宏池会」。在2008年5月，兩派完成合併，結束7年「宏池会」名稱2派共用的場面。

結果在[通常国会](../Page/第169回国会.md "wikilink")，2008年5月13日決定由古賀出任派閥會長、谷垣出任發言人、堀内光雄出任名譽會長、[逢澤一郎出任事務總長](../Page/逢澤一郎.md "wikilink")。在2008年10月，合併後的宏池會成為黨內第3大派系，有61人。

在[2009年日本眾議院議員選舉](../Page/第45屆日本眾議院議員總選舉.md "wikilink")，宏池會所屬的眾議員少了一半，只得25人。但因第1派閥清和政策研究會和第2派閥平成研究會都只贏得之前1/3議席下，宏池會成為眾議院第1派閥，直至[2012年日本眾議院議員選舉自民黨重新執政為止](../Page/第46屆日本眾議院議員總選舉.md "wikilink")。

## 現在組成

### 主要幹部

<table>
<thead>
<tr class="header">
<th><p>會長</p></th>
<th><p>名譽會長</p></th>
<th><p>名譽顧問</p></th>
<th><p>發言人代表</p></th>
<th><p>顧問</p></th>
<th><p>會長代行</p></th>
<th><p>副會長<br />
（衆議院）</p></th>
<th><p>副會長<br />
（参議院）</p></th>
<th><p>事務總長</p></th>
<th><p>事務總長代理、廣報局長兼社會經濟調査委員會會長</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/岸田文雄.md" title="wikilink">岸田文雄</a></p></td>
<td><p><a href="../Page/古賀誠.md" title="wikilink">古賀誠</a></p></td>
<td><p><br />
<a href="../Page/丹羽雄哉.md" title="wikilink">丹羽雄哉</a></p></td>
<td><p><a href="../Page/谷垣禎一.md" title="wikilink">谷垣禎一</a></p></td>
<td><p><a href="../Page/左藤惠.md" title="wikilink">左藤惠</a><br />
<a href="../Page/森田一.md" title="wikilink">森田一</a><br />
<a href="../Page/真鍋賢二.md" title="wikilink">真鍋賢二</a></p></td>
<td><p><a href="../Page/太田誠一.md" title="wikilink">太田誠一</a></p></td>
<td><p><a href="../Page/川崎二郎.md" title="wikilink">川崎二郎</a><br />
<a href="../Page/柳澤伯夫.md" title="wikilink">柳澤伯夫</a><br />
<a href="../Page/金子一義.md" title="wikilink">金子一義</a><br />
<a href="../Page/園田博之.md" title="wikilink">園田博之</a><br />
<a href="../Page/二田孝治.md" title="wikilink">二田孝治</a><br />
<a href="../Page/鈴木俊一_(衆議院議員).md" title="wikilink">鈴木俊一</a></p></td>
<td><p><a href="../Page/溝手顯正.md" title="wikilink">溝手顯正</a><br />
<a href="../Page/市川一朗.md" title="wikilink">市川一朗</a><br />
<a href="../Page/加治屋義人.md" title="wikilink">加治屋義人</a></p></td>
<td><p><a href="../Page/逢澤一郎.md" title="wikilink">逢澤一郎</a></p></td>
<td><p><a href="../Page/鹽崎恭久.md" title="wikilink">鹽崎恭久</a></p></td>
</tr>
</tbody>
</table>

宏池会合流後主要人物詳細介紹

  - 會長 - [岸田文雄](../Page/岸田文雄.md "wikilink")
  - 名譽會長 - [堀內光雄](../Page/堀內光雄.md "wikilink")
  - 發言人代表 - [谷垣禎一](../Page/谷垣禎一.md "wikilink")
  - 發言人代表補佐-
    [實川幸夫](../Page/實川幸夫.md "wikilink")、[望月義夫](../Page/望月義夫.md "wikilink")、[山本幸三](../Page/山本幸三.md "wikilink")
  - 名譽顧問 - [丹羽雄哉](../Page/丹羽雄哉.md "wikilink")
  - 顧問 -
    [左藤惠](../Page/左藤惠.md "wikilink")（元[眾議院議員](../Page/眾議院議員.md "wikilink")、元[郵政大臣](../Page/郵政大臣.md "wikilink")、[法務大臣](../Page/法務大臣.md "wikilink")、[国土廳長官](../Page/国土廳長官.md "wikilink")）、[森田一](../Page/森田一.md "wikilink")（[大平正芳女婿](../Page/大平正芳.md "wikilink")、元衆議院議員、元[運輸大臣兼](../Page/運輸大臣.md "wikilink")[北海道開發廳長官](../Page/北海道開發廳長官.md "wikilink")）、[真鍋賢二](../Page/真鍋賢二.md "wikilink")（前[参議院議員](../Page/参議院議員.md "wikilink")、元[環境廳長官](../Page/環境廳長官.md "wikilink")）
  - 會長代行 - [太田誠一](../Page/太田誠一.md "wikilink")
  - 會長代行補佐 -
    [根本匠](../Page/根本匠.md "wikilink")、[宮澤洋一](../Page/宮澤洋一.md "wikilink")、[林芳正](../Page/林芳正.md "wikilink")
  - 副會長 -
    [川崎二郎](../Page/川崎二郎.md "wikilink")、[柳澤伯夫](../Page/柳澤伯夫.md "wikilink")、[金子一義](../Page/金子一義.md "wikilink")、[園田博之](../Page/園田博之.md "wikilink")、[二田孝治](../Page/二田孝治.md "wikilink")、[鈴木俊一](../Page/鈴木俊一_\(眾議院議員\).md "wikilink")、[溝手顯正](../Page/溝手顯正.md "wikilink")、[市川一朗](../Page/市川一朗.md "wikilink")、[加治屋義人](../Page/加治屋義人.md "wikilink")
  - 副會長補佐 -
    [岸田文雄](../Page/岸田文雄.md "wikilink")、[小野寺五典](../Page/小野寺五典.md "wikilink")、[近藤基彦](../Page/近藤基彦.md "wikilink")、[寺田稔](../Page/寺田稔.md "wikilink")、[葉梨康弘](../Page/葉梨康弘.md "wikilink")、[三ツ矢憲生](../Page/三ツ矢憲生.md "wikilink")、[岸宏一](../Page/岸宏一.md "wikilink")、、[西島英利](../Page/西島英利.md "wikilink")、[水落敏榮](../Page/水落敏榮.md "wikilink")
  - 事務總長 - [逢澤一郎](../Page/逢澤一郎.md "wikilink")
  - 事務總長代理、広報局長兼社会経済調査委員会会長 - [鹽崎恭久](../Page/鹽崎恭久.md "wikilink")
  - 事務總長補佐 -
    [今井宏](../Page/今井宏_\(政治家\).md "wikilink")、[岩永峯一](../Page/岩永峯一.md "wikilink")、[遠藤利明](../Page/遠藤利明.md "wikilink")、[佐藤勉](../Page/佐藤勉.md "wikilink")、[竹本直一](../Page/竹本直一.md "wikilink")、[西野陽](../Page/西野陽.md "wikilink")、[宮腰光寛](../Page/宮腰光寛.md "wikilink")
  - 廣報局長代理 - [中谷元](../Page/中谷元.md "wikilink")
  - 廣報局長補佐 -
    [山本公一](../Page/山本公一.md "wikilink")、[北村誠吾](../Page/北村誠吾.md "wikilink")、[田中直紀](../Page/田中直紀.md "wikilink")
  - 社會經濟調査委員会会長代理 - [村田吉隆](../Page/村田吉隆.md "wikilink")
  - 社會經濟調査委員会会長補佐 -
    [上川陽子](../Page/上川陽子.md "wikilink")、[平井卓也](../Page/平井卓也.md "wikilink")、[加納時男](../Page/加納時男.md "wikilink")
  - 派内選舉対策總局長 - [福井照](../Page/福井照.md "wikilink")
  - 派内選舉対策總局長代理 - [小里泰弘](../Page/小里泰弘.md "wikilink")
  - 派内選舉対策總局長補佐 -
    [井澤京子](../Page/井澤京子.md "wikilink")、[木原誠二](../Page/木原誠二.md "wikilink")、[清水鴻一郎](../Page/清水鴻一郎.md "wikilink")、[土井真樹](../Page/土井真樹.md "wikilink")、[萩原誠司](../Page/萩原誠司.md "wikilink")、[林潤](../Page/林潤.md "wikilink")、[藤井勇治](../Page/藤井勇治.md "wikilink")、[盛山正仁](../Page/盛山正仁.md "wikilink")
  - 派閥離脱中役員特別補佐 -
    [原田令嗣](../Page/原田令嗣.md "wikilink")、[松山政司](../Page/松山政司.md "wikilink")

### 眾議院議員

<table>
<tbody>
<tr class="odd">
<td><p><a href="../Page/古賀誠.md" title="wikilink">古賀誠</a><br />
（10回、<a href="../Page/福岡縣第7區.md" title="wikilink">福岡7區</a>）</p></td>
<td><p><a href="../Page/谷垣禎一.md" title="wikilink">谷垣禎一</a><br />
（10回、<a href="../Page/京都府第5區.md" title="wikilink">京都5區</a>）</p></td>
<td><p><a href="../Page/川崎二郎.md" title="wikilink">川崎二郎</a><br />
（8回、比例東海）</p></td>
<td><p><a href="../Page/園田博之.md" title="wikilink">園田博之</a><br />
（8回、<a href="../Page/熊本縣第4區.md" title="wikilink">熊本4區</a>）</p></td>
<td><p><a href="../Page/金子一義.md" title="wikilink">金子一義</a><br />
（8回、<a href="../Page/岐阜縣第4區.md" title="wikilink">岐阜4區</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逢澤一郎.md" title="wikilink">逢澤一郎</a><br />
（8回、<a href="../Page/岡山縣第1區.md" title="wikilink">岡山1區</a>）</p></td>
<td><p><a href="../Page/村田吉隆.md" title="wikilink">村田吉隆</a><br />
（7回、比例中國）</p></td>
<td><p><a href="../Page/中谷元.md" title="wikilink">中谷元</a><br />
（7回、<a href="../Page/高知縣第2區.md" title="wikilink">高知2區</a>）</p></td>
<td><p><a href="../Page/山本公一.md" title="wikilink">山本公一</a><br />
（6回、<a href="../Page/愛媛縣第4區.md" title="wikilink">愛媛4區</a>）</p></td>
<td><p><strong><a href="../Page/岸田文雄.md" title="wikilink">岸田文雄</a></strong><br />
（6回、<a href="../Page/廣島縣第1區.md" title="wikilink">廣島1區</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西野陽.md" title="wikilink">西野陽</a><br />
（6回、<a href="../Page/大阪府第13區.md" title="wikilink">大阪13區</a>）</p></td>
<td><p><a href="../Page/竹本直一.md" title="wikilink">竹本直一</a><br />
（5回、比例近畿）</p></td>
<td><p><a href="../Page/山本幸三.md" title="wikilink">山本幸三</a><br />
（5回、比例九州）</p></td>
<td><p><a href="../Page/遠藤利明.md" title="wikilink">遠藤利明</a><br />
（5回、比例東北）</p></td>
<td><p><a href="../Page/宮腰光寛.md" title="wikilink">宮腰光寛</a><br />
（5回、<a href="../Page/富山縣第2區.md" title="wikilink">富山2區</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鹽崎恭久.md" title="wikilink">鹽崎恭久</a><br />
（5回・參1回、<a href="../Page/愛媛縣第1區.md" title="wikilink">愛媛1區</a>）</p></td>
<td><p><a href="../Page/佐藤勉.md" title="wikilink">佐藤勉</a><br />
（5回、比例北關東）</p></td>
<td><p><a href="../Page/北村誠吾.md" title="wikilink">北村誠吾</a><br />
（4回、比例九州）</p></td>
<td><p><a href="../Page/福井照.md" title="wikilink">福井照</a><br />
（4回、<a href="../Page/高知縣第1區.md" title="wikilink">高知1區</a>）</p></td>
<td><p><a href="../Page/平井卓也.md" title="wikilink">平井卓也</a><br />
（4回、比例四國）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小野寺五典.md" title="wikilink">小野寺五典</a><br />
（4回、<a href="../Page/宮城縣第6區.md" title="wikilink">宮城6區</a>）</p></td>
<td><p><a href="../Page/三ツ矢憲生.md" title="wikilink">三ツ矢憲生</a><br />
（3回、<a href="../Page/三重縣第5區.md" title="wikilink">三重5區</a>）</p></td>
<td><p><a href="../Page/小里泰弘.md" title="wikilink">小里泰弘</a><br />
（2回、<a href="../Page/鹿兒島縣第4區.md" title="wikilink">鹿兒島4區</a>）</p></td>
<td><p><a href="../Page/徳田毅.md" title="wikilink">徳田毅</a><br />
（2回、<a href="../Page/鹿兒島縣第2區.md" title="wikilink">鹿兒島2區</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

（計24名）

### 參議院議員

<table>
<tbody>
<tr class="odd">
<td><p><a href="../Page/溝手顯正.md" title="wikilink">溝手顯正</a><br />
（4回、<a href="../Page/広島縣選舉區.md" title="wikilink">広島縣</a>）</p></td>
<td><p><a href="../Page/市川一朗.md" title="wikilink">市川一朗</a><br />
（3回、<a href="../Page/宮城縣選舉區.md" title="wikilink">宮城縣</a>）</p></td>
<td><p><a href="../Page/林芳正.md" title="wikilink">林芳正</a><br />
（3回、<a href="../Page/山口縣選舉區.md" title="wikilink">山口縣</a>）</p></td>
<td><p><a href="../Page/加治屋義人.md" title="wikilink">加治屋義人</a><br />
（2回、<a href="../Page/鹿兒島縣選舉區.md" title="wikilink">鹿兒島縣</a>）</p></td>
<td><p><a href="../Page/加納時男.md" title="wikilink">加納時男</a><br />
（2回、比例區）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岸宏一.md" title="wikilink">岸宏一</a><br />
（2回、<a href="../Page/山形縣選舉區.md" title="wikilink">山形縣</a>）</p></td>
<td><p><a href="../Page/松山政司.md" title="wikilink">松山政司</a><br />
（2回、<a href="../Page/福岡縣選舉區.md" title="wikilink">福岡縣</a>）</p></td>
<td><p><a href="../Page/西島英利.md" title="wikilink">西島英利</a><br />
（1回、比例區）</p></td>
<td><p><a href="../Page/水落敏榮.md" title="wikilink">水落敏榮</a><br />
（1回、比例區）</p></td>
<td></td>
</tr>
</tbody>
</table>

（計9名）

## 備考

  - [田中直紀](../Page/田中直紀.md "wikilink")[參議院議員](../Page/參議院議員.md "wikilink")（參2回・眾3回、[新潟縣](../Page/新潟縣選舉區.md "wikilink")）中宏池會結時參與[宏池会
    (古賀派)](../Page/宏池会_\(古賀派\).md "wikilink")，跟其妻[田中真紀子](../Page/田中真紀子.md "wikilink")（無所屬・民主派黨團參加）不同。但在9月時以地緣問題提出離黨的要求並在10月獲得接納。他在2009年8月跟妻子一起加入民主黨。

## 歷代首相

  - [池田勇人](../Page/池田勇人.md "wikilink") (1960年至1964年)
  - [大平正芳](../Page/大平正芳.md "wikilink") (1978年至1980年)
  - [鈴木善幸](../Page/鈴木善幸.md "wikilink") (1980年至1982年)
  - [宮澤喜一](../Page/宮澤喜一.md "wikilink") (1991年至1993年)

## 歷代自民黨總裁

  - [池田勇人](../Page/池田勇人.md "wikilink") (1960年至1964年)
  - [大平正芳](../Page/大平正芳.md "wikilink") (1978年至1980年)
  - [鈴木善幸](../Page/鈴木善幸.md "wikilink") (1980年至1982年)
  - [宮澤喜一](../Page/宮澤喜一.md "wikilink") (1991年至1993年)
  - [谷垣禎一](../Page/谷垣禎一.md "wikilink") (2009年至2012年)

## 外部連結

<div>

<references />

</div>

## 關連項目

  - [自由民主黨](../Page/自由民主黨_\(日本\).md "wikilink")
  - [自由民主黨派閥](../Page/自由民主黨派閥.md "wikilink")
  - [保守本流](../Page/保守本流.md "wikilink")
  - [保守左派](../Page/保守左派.md "wikilink")
  - [宏池会 (古賀派)](../Page/宏池会_\(古賀派\).md "wikilink")
  - [宏池会 (谷垣派)](../Page/宏池会_\(谷垣派\).md "wikilink")
  - [大勇会](../Page/大勇会.md "wikilink") （現[為公会](../Page/為公会.md "wikilink")）

[Category:自由民主黨派閥](../Category/自由民主黨派閥.md "wikilink")