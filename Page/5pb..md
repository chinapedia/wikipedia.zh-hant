**株式会社5pb.**是一家[日本](../Page/日本.md "wikilink")[音樂製作和](../Page/音樂製作.md "wikilink")[遊戲開發商](../Page/遊戲開發商.md "wikilink")，公司名稱為The
**Five** **p**owered & **b**asics.的缩写。

2011年4月，5pb.的[母公司](../Page/母公司.md "wikilink")[Dwango宣布](../Page/Dwango.md "wikilink")，5pb.将于2011年6月1日合併入同属Dwango旗下的游戏公司AG-ONE，合并后的新公司名为。

## 旗下藝人

  - [彩音](../Page/彩音.md "wikilink")
  - [村田步](../Page/村田步.md "wikilink")
  - [KAORI](../Page/KAORI.md "wikilink")
  - [榊原ゆい](../Page/榊原ゆい.md "wikilink")
  - [宫崎羽衣](../Page/宫崎羽衣.md "wikilink")
  - [野川樱](../Page/野川樱.md "wikilink")
  - [今井麻美](../Page/今井麻美.md "wikilink")
  - [伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

<!-- end list -->

  - [志倉千代丸](../Page/志倉千代丸.md "wikilink")
  - [市川和弘](../Page/市川和弘.md "wikilink")
  - [柴田太郎](../Page/柴田太郎.md "wikilink")
  - [盛政樹](../Page/盛政樹.md "wikilink")
  - [阿保剛](../Page/阿保剛.md "wikilink")
  - [亞咲花](../Page/亞咲花.md "wikilink")

## 遊戲作品

  - [秋之回憶5：中斷的影片encore](../Page/秋之回憶5：中斷的影片.md "wikilink")（PC、[PS2](../Page/PS2.md "wikilink")、[PSP](../Page/PSP.md "wikilink")）（製作、販售：[KID](../Page/KID.md "wikilink")、[CyberFront](../Page/CyberFront.md "wikilink")）
  - [秋之回忆6～T-wave～](../Page/秋之回忆6～T-wave～.md "wikilink")（PC、PS2、PSP、[iOS](../Page/iOS.md "wikilink")）
  - [秋之回忆6～Next
    Relation～](../Page/秋之回忆6～Next_Relation～.md "wikilink")（PS2、Xbox360、PSP）
  - [秋之回憶6 完全版](../Page/秋之回憶6_完全版.md "wikilink")（PS3、PS VITA）
  - [秋之回憶：打勾勾的記憶](../Page/秋之回憶：打勾勾的記憶.md "wikilink")（PC、Xbox360、PSP、PS3、PS
    VITA）
  - [妳的・秋之回憶〜Girl's Style〜](../Page/秋之回憶系列.md "wikilink")
  - [暗夜魔法使 The VIDEO GAME ～Denial of the
    World～](../Page/暗夜魔法使.md "wikilink")
  - QUE～エンシェントリーフの妖精～（製作、販售：[Princess
    Soft](../Page/Princess_Soft.md "wikilink")）
  - [海商](../Page/GO！純情水泳社！.md "wikilink")
  - L的季節|L的季節2 invisible memories
  - PRISM ARK -AWAKE-
  - ケツイ〜絆地獄たち〜（Xbox 360、移植）
  - 怒首領蜂 大往生（[Xbox 360](../Page/Xbox_360.md "wikilink")、移植）
  - [11eyes CrossOver](../Page/11eyes.md "wikilink")（Xbox 360、PSP）
  - 彈魂（Xbox360）
  - [車輪之國、向日葵的少女](../Page/車輪之國、向日葵的少女.md "wikilink")（Xbox360）
  - [大正野球娘](../Page/大正野球娘.md "wikilink")（PSP）
  - W.L.O. 世界戀愛機構（Xbox360）
  - [屍體派對](../Page/屍體派對.md "wikilink")（PSP）
  - 屍體派對：影之書（PSP）
  - [CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")（PC、PSP、XBOX360、iOS)
  - [CHAOS;HEAD NOAH](../Page/CHAOS;HEAD_NOAH.md "wikilink")（PSP）
  - [CHAOS;HEAD Love
    Chu☆Chu！](../Page/CHAOS;HEAD.md "wikilink")（PSP、Xbox360）
  - [Steins;Gate](../Page/Steins;Gate.md "wikilink")（PC、PSP、PS3、Xbox
    360、iOS、Android）
  - [Steins;Gate：比翼戀理的愛人](../Page/Steins;Gate.md "wikilink")（PSP、PS3、Xbox360）
  - [ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")（PS3、XBOX360）
  - 屍體派對2U：幸子的戀愛遊戲（PSP）
  - 屍體派對：Blood Drive（PSV）
  - [CHAOS;CHILD](../Page/CHAOS;CHILD.md "wikilink")(XBOX
    ONE、PSV、PS3、PS4)
  - [Steins;Gate 0](../Page/Steins;Gate.md "wikilink")（PC、PS4、PS3 、PS
    Vita）
  - [Occultic;Nine](../Page/Occultic;Nine.md "wikilink") (PS4、PSV、XBOX
    ONE)

## 外部連結

  - [官方網站](http://5pb.jp/)
  - [5pb.Games官方網站](http://5pb.jp/games/)

[Category:2005年開業電子遊戲公司](../Category/2005年開業電子遊戲公司.md "wikilink")
[\*](../Category/5bp..md "wikilink")
[Category:多玩國](../Category/多玩國.md "wikilink")
[Category:电子游戏开发公司](../Category/电子游戏开发公司.md "wikilink")
[Category:电子游戏发行商](../Category/电子游戏发行商.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:日本媒體公司](../Category/日本媒體公司.md "wikilink")
[Category:澀谷區公司](../Category/澀谷區公司.md "wikilink")