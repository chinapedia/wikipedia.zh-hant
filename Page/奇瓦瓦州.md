<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>契瓦瓦州<br />
Estado Libre y Soberano de Chihuahua</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Chihuahua.svg" title="fig:Coat_of_arms_of_Chihuahua.svg">Coat_of_arms_of_Chihuahua.svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>位置</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Chihuahua_in_Mexico_(location_map_scheme).svg" title="fig:Chihuahua_in_Mexico_(location_map_scheme).svg">Chihuahua_in_Mexico_(location_map_scheme).svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>基本資料</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>首府</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>面積</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>人口<br />
<small>(2013年人口普查)</small></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>州長<br />
<small>(2004-2010)</small></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/墨西哥眾議院.md" title="wikilink">眾議院議席數</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/墨西哥參議院.md" title="wikilink">參議院議席數</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/ISO_3166-2.md" title="wikilink">ISO 3166-2</a><br />
<small>郵政簡寫</small></p></td>
</tr>
</tbody>
</table>

**契瓦瓦州**（）位於[墨西哥西北內陸](../Page/墨西哥.md "wikilink")，北靠[美國](../Page/美國.md "wikilink")[新墨西哥州和](../Page/新墨西哥州.md "wikilink")[德克薩斯州](../Page/德克薩斯州.md "wikilink")，是該國面積最大的[州](../Page/墨西哥行政區劃.md "wikilink")，佔全國面的八分之一。

## 地理

奇瓦瓦除了有廣大的[沙漠外](../Page/沙漠.md "wikilink")，其[森林面積比墨西哥其它州都要多](../Page/森林.md "wikilink")。在山坡地上有短草草原，是墨西哥主要的[農業區](../Page/農業.md "wikilink")。

## 歷史

州名源於州的首府。「契瓦瓦」這個名字的來源不詳，但在[西班牙人來到前已經存在](../Page/西班牙.md "wikilink")，可能出自[納瓦特爾語的](../Page/納瓦特爾語.md "wikilink")*Xicuahua*，意思是「乾燥、多沙的」。

该州在[墨西哥革命時期發揮了重要作用](../Page/墨西哥革命.md "wikilink")。

## 經濟

主要產品有[蘋果](../Page/蘋果.md "wikilink")、[堅果](../Page/堅果.md "wikilink")、[木材](../Page/木材.md "wikilink")、[牛隻及](../Page/牛.md "wikilink")[奶製品](../Page/奶製品.md "wikilink")、[綿羊](../Page/綿羊.md "wikilink")，[金](../Page/金.md "wikilink")、[銀](../Page/銀.md "wikilink")、[鉛](../Page/鉛.md "wikilink")、[鋅等各種金屬](../Page/鋅.md "wikilink")，[水泥及](../Page/水泥.md "wikilink")[陶瓷材料](../Page/陶瓷材料.md "wikilink")。工業方面以出口美國為主。[Mata
Ortiz](http://en.wikipedia.org/wiki/Mata_Ortiz)的新式陶瓷飲譽國際。[第三產業發達](../Page/第三產業.md "wikilink")，以[旅遊業](../Page/旅遊業.md "wikilink")、[銀行業](../Page/銀行.md "wikilink")、高科技產業為主。然而由於鄰近美國，亦成為了[毒販活躍的地方](../Page/毒販.md "wikilink")，大大影響了當地的治安及旅遊業。

## 民族

[麥士蒂索人為主](../Page/麥士蒂索人.md "wikilink")，[白人的比例全墨西哥第一](../Page/白人.md "wikilink")（35%）。其它族群有居於山區的[印地安人](../Page/印地安人.md "wikilink")，[門諾派](../Page/門諾派.md "wikilink")（有[德國](../Page/德國.md "wikilink")-[荷蘭血統](../Page/荷蘭.md "wikilink")）、[摩門教](../Page/摩門教.md "wikilink")（有[英美血統](../Page/英國.md "wikilink")）徒等。

## 另見

  - [吉娃娃](../Page/吉娃娃.md "wikilink")

[\*](../Category/奇瓦瓦州.md "wikilink")
[Category:1824年建立的一級行政區](../Category/1824年建立的一級行政區.md "wikilink")