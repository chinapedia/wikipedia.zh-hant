**亚基马**
（）是[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[雅基馬縣縣治](../Page/雅基馬縣.md "wikilink")，位於該州中南部。2000年人口為71,845人，都會區人口為229,094人。\[1\]這個數字於2010年上升至243,231。\[2\]\[3\]其所在的雅基馬谷地以生產[蘋果和](../Page/蘋果.md "wikilink")[啤酒花馳名](../Page/啤酒花.md "wikilink")，而美國75%啤酒花皆於亚基马一帶生產。\[4\]亚基马都會區名為「大亚基马」（Greater
Yakima）。\[5\]

城名來自[雅基馬族](../Page/雅基馬族.md "wikilink")。城東南、南部為[雅基馬印第安人保留地](../Page/雅基馬印第安人保留地.md "wikilink")。\[6\]

## 地理

根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，本城面積為，其中為陸地，為水域。\[7\]

## 氣候

雅基馬擁有[半乾旱氣候](../Page/半乾旱氣候.md "wikilink")，其夏季酷熱，冬季寒冷。\[8\]雅基馬的最低紀錄溫度為1950年2月的，而其最高紀錄溫度則為1971年8月的。\[9\]

## 姐妹城市

  - [基隆市](../Page/基隆市.md "wikilink")

  - [板柳町](../Page/板柳町.md "wikilink")

  - [傑爾賓特](../Page/傑爾賓特.md "wikilink")

  - [莫雷利亞](../Page/莫雷利亞.md "wikilink")

  - [愛達荷州](../Page/愛達荷州.md "wikilink")[伯利](../Page/伯利_\(愛達荷州\).md "wikilink")

## 参考文献

[Y](../Category/華盛頓州城市.md "wikilink")

1.

2.  [2010 census factfinder2](http://factfinder2.census.gov/)

3.

4.

5.

6.

7.

8.
9.