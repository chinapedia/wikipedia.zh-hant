**雙斑阿南魚**，又稱**黃胸鹦鯛**、**星阿南魚**，俗名白肚龍，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[隆頭魚科的其中一](../Page/隆頭魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[留尼旺](../Page/留尼旺.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[聖誕島](../Page/聖誕島.md "wikilink")、[中國](../Page/中國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[越南](../Page/越南.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[關島](../Page/關島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[東加](../Page/東加.md "wikilink")、[法屬波里尼西亞等海域](../Page/法屬波里尼西亞.md "wikilink")。

## 深度

水深1至30公尺。

## 特徵

本魚體長形，側扁。口小；唇稍厚，內側具縐褶；上下頜前方各有一對向前伸出門狀齒，扁平而有切截緣。成魚呈深褐色，其明顯特徵乃喉部及胸部、腹部為鮮黃色。背鰭和臀鰭上許多藍點，其末端並各有一枚具藍緣的眼狀斑，但其會隨成長而逐漸消退。體被大或中大鱗，頭部眼前無鱗，胸部鱗片不較體側為大；側線連續。尾鰭為淡褐色上一有許多排呈弧型的藍點。幼魚體色和成魚相似但較偏橘紅色。背鰭硬棘9枚、背鰭軟條12枚、臀鰭硬棘3枚、臀鰭軟條11至13。體長可達18公分。

## 生態

本魚體色不隨成長或性別不同而變，多棲息在混有沙石及珊瑚的礁湖或向海礁坡上，會潛沙而眠。以底生動物為食。

## 經濟利用

可為觀賞魚，大魚亦可食用。[清蒸](../Page/清蒸.md "wikilink")、[紅燒均可](../Page/紅燒.md "wikilink")。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[twistii](../Category/阿南魚屬.md "wikilink")