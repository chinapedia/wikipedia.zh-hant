**帝力**（[德頓語](../Page/德頓語.md "wikilink")：；）是[东帝汶的](../Page/东帝汶.md "wikilink")[首都](../Page/首都.md "wikilink")，位于[帝汶岛的北岸](../Page/帝汶岛.md "wikilink")。帝力是东帝汶的主要港口和商业中心，其机场同时为民航及军方所用。

## 历史

帝力于1520年由[葡萄牙人占领并成为其殖民地](../Page/葡萄牙.md "wikilink")，于1596年成为[葡属帝汶的首府](../Page/葡属帝汶.md "wikilink")。[第二次世界大战期间为](../Page/第二次世界大战.md "wikilink")[日本所占](../Page/日本.md "wikilink")。二战后重归葡萄牙占领。东帝汶于1975年从葡萄牙独立，帝力成为首都。1976年7月东帝汶被[印尼吞并](../Page/印度尼西亚.md "wikilink")、帝力仍為东帝汶省府所在。

在东帝汶寻求独立期间，独立武装组织与印尼军队不断爆发游击战，有数千市民被杀。直到1999年，东帝汶成功争取自治，受到[联合国监管](../Page/联合国.md "wikilink")。到2002年5月20日，东帝汶重新独立，帝力再次恢復成為东帝汶首都。

## 地理

帝力位于[帝汶岛的北岸](../Page/帝汶岛.md "wikilink")，是[小巽他群岛中最繁盛的城市](../Page/小巽他群岛.md "wikilink")。

## 经济

帝力是东帝汶的主要港口和商业中心，其机场同时为民航及军方所用。

## 交通

[尼古勞·洛巴托總統國際機場](../Page/尼古勞·洛巴托總統國際機場.md "wikilink")

## 旅游

  - [古斯芒旧居](../Page/古斯芒旧居.md "wikilink")

## 教育

拥有[东帝汶国立大学](../Page/东帝汶国立大学.md "wikilink")（Universidade Nacional de
Timor-Leste）一所大学及数所高中，除此外还有为外国人开办的学校。

## 友好城市

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>地区</p></th>
<th><p>国家</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/科英布拉.md" title="wikilink">科英布拉</a></p></td>
<td><p><a href="../Page/中部大区.md" title="wikilink">中部大区</a></p></td>
<td><p>葡萄牙</p></td>
<td><p>2002</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/达尔文_(澳洲).md" title="wikilink">达尔文</a></p></td>
<td><p><a href="../Page/北领地.md" title="wikilink">北领地</a></p></td>
<td><p>澳大利亚</p></td>
<td><p>2003年9月</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/澳门.md" title="wikilink">澳门</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Macau.svg" title="fig:Flag_of_Macau.svg">Flag_of_Macau.svg</a> <a href="../Page/澳门特别行政区.md" title="wikilink">澳门特别行政区</a></p></td>
<td><p>中华人民共和国</p></td>
<td><p>2002年6月</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冲绳市.md" title="wikilink">冲绳市</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Emblem_of_Okinawa_Prefecture.svg" title="fig:Emblem_of_Okinawa_Prefecture.svg">Emblem_of_Okinawa_Prefecture.svg</a> <a href="../Page/冲绳县.md" title="wikilink">冲绳县</a></p></td>
<td><p>日本</p></td>
<td><p>2005年11月</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴塞罗那.md" title="wikilink">巴塞罗那</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Catalonia.svg" title="fig:Flag_of_Catalonia.svg">Flag_of_Catalonia.svg</a> <a href="../Page/加泰罗尼亚.md" title="wikilink">加泰罗尼亚</a></p></td>
<td><p>西班牙</p></td>
<td><p>2008年6月</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/圣保罗.md" title="wikilink">圣保罗</a></p></td>
<td><p>圣保罗州</p></td>
<td><p>巴西</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/悉尼.md" title="wikilink">悉尼</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_New_South_Wales.svg" title="fig:Flag_of_New_South_Wales.svg">Flag_of_New_South_Wales.svg</a> <a href="../Page/新南威尔士州.md" title="wikilink">新南威尔士州</a></p></td>
<td><p>澳大利亚</p></td>
<td><p>2010年9月</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马尼拉.md" title="wikilink">马尼拉</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Manila.svg" title="fig:Flag_of_Manila.svg">Flag_of_Manila.svg</a> <a href="../Page/马尼拉大都会.md" title="wikilink">马尼拉大都会</a></p></td>
<td><p>菲律宾</p></td>
<td><p>2011年月</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/堪培拉.md" title="wikilink">堪培拉</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Australian_Capital_Territory.svg" title="fig:Flag_of_the_Australian_Capital_Territory.svg">Flag_of_the_Australian_Capital_Territory.svg</a> <a href="../Page/澳洲首都領地.md" title="wikilink">澳洲首都領地</a></p></td>
<td><p>澳大利亚</p></td>
<td><p>2004年6月</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安汶.md" title="wikilink">安汶</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Maluku.png" title="fig:Flag_of_Maluku.png">Flag_of_Maluku.png</a> <a href="../Page/马鲁古省.md" title="wikilink">马鲁古省</a></p></td>
<td><p>印尼</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

## 著名人物

  - [古斯芒](../Page/沙納納·古斯芒.md "wikilink")

## 外部链接

[Category:東帝汶城市](../Category/東帝汶城市.md "wikilink")
[帝力](../Category/帝力.md "wikilink")
[Category:1520年建立的城市](../Category/1520年建立的城市.md "wikilink")
[Category:大洋洲首都](../Category/大洋洲首都.md "wikilink")