**卡蜜拉·貝兒·露絲**（，），是一位[美國](../Page/美國.md "wikilink")[女演員](../Page/女演員.md "wikilink")，曾經演出2000年的迪士尼頻道《[衝浪美眉](../Page/衝浪美眉.md "wikilink")》（*Rip
Girls*）、2005年的《[寂靜殺機](../Page/寂靜殺機.md "wikilink")》，以及2006年的《[奪命電話](../Page/奪命電話.md "wikilink")》（*When
a Stranger Calls*）等電影。

卡蜜拉曾經和[喬·強納斯交往一陣時間](../Page/喬·強納斯.md "wikilink")，之後成為他的女朋友；但卻有不少聲浪指向是卡蜜拉從美國創作型歌手[泰勒絲搶走強納斯](../Page/泰勒·斯威夫特.md "wikilink")。此事過後，[泰勒絲寫了一首歌](../Page/泰勒·斯威夫特.md "wikilink")《Better
Than
Revenge》來反擊卡蜜拉，並且收錄在她的專輯《[愛的告白](../Page/爱的告白.md "wikilink")》裡面，此專輯不但是美國2011年最暢銷的專輯，也讓[泰勒絲得到](../Page/泰勒·斯威夫特.md "wikilink")[全美音樂獎](../Page/全美音樂獎.md "wikilink")。卡蜜拉經此事後事業逐漸走向下峰。

## 演出電影

| 年份                                     | 作品譯名                                               | 作品名稱                            | 備註                     |
| -------------------------------------- | -------------------------------------------------- | ------------------------------- | ---------------------- |
| 1995年                                  | 小公主                                                | **                              |                        |
| 1996年                                  | [慾海潮2:莉莉](../Page/慾海潮2:莉莉.md "wikilink")           | **                              |                        |
| 1997年                                  | [侏儸紀公園2：失落的世界](../Page/侏儸紀公園2：失落的世界.md "wikilink") | *The Lost World: Jurassic Park* | 片頭逗小恐龍被攻擊小女孩           |
| 1998年                                  | [火線戰將](../Page/火線戰將.md "wikilink")                 | **                              |                        |
| [超異能快感](../Page/超異能快感.md "wikilink")   | **                                                 |                                 |                        |
| 1999年                                  | 南美尋寶記                                              | **                              |                        |
| 2000年                                  | 衝浪美眉                                               | **                              |                        |
| 2001年                                  | 心狂線索                                               | **                              |                        |
| [重返秘密花園](../Page/重返秘密花園.md "wikilink") | **                                                 |                                 |                        |
| 2005年                                  | 傑克與羅絲的民謠                                           | **                              | Limited release        |
| 我行我素                                   | **                                                 | Limited release                 |                        |
| [寂靜殺機](../Page/寂靜殺機.md "wikilink")     | **                                                 | Limited release                 |                        |
| 2006年                                  | 奪命電話                                               | **                              |                        |
| 2007年                                  | 七里伏                                                | **                              |                        |
| 2008年                                  | [史前一萬年](../Page/史前一萬年.md "wikilink")               | **                              | 伊芙樂（女主角）               |
| 2009年                                  | [移動城市](../Page/移動城市.md "wikilink")                 | *Push*                          | 琪拉，「推手」：具有能夠改變對方思想的超異能 |
| 2011年                                  | [小姐好窮](../Page/小姐好窮.md "wikilink")                 | **                              | 諾拉（女主角）                |
| 2016年                                  | 日落之前                                               | Sundown                         | Gaby(女主角)              |

## 外部連結

  -
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:巴西裔美國人](../Category/巴西裔美國人.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")