**《泣血四月》**（）為2005年[HBO自製的電影](../Page/HBO.md "wikilink")，也是第一部在[盧安達實地拍攝的電影](../Page/盧安達.md "wikilink")；[導演為Raoul](../Page/導演.md "wikilink")
Peck。劇情描述Augustin兄弟及其家人與朋友在盧安達種族屠殺期間所遭遇的種種。

## 演員表

  - Idris Elba([伊德瑞斯·艾爾巴](../Page/伊德瑞斯·艾爾巴.md "wikilink")) 飾 Augustin
  - Carole Karemera 飾 Jeanne
  - Pamela Nomvete 飾 Martine
  - Oris Erhuero 飾 Honoré
  - Fraser James 飾 Xavier
  - Abby Mukiibi Nkaaga 飾 Colonel Bagosora
  - Cleophas Kabasita 飾 Valentine (as Cléophas Kabasiita)
  - Noah Emmerich 飾 Lionel Quaid
  - Debra Winger 飾 Prudence Bushnell
  - Peninah Abatoni 飾 Woman Refugee \#1
  - Ashani Alles 飾 Prosecutor
  - Hope Azeda 飾 Brigitte
  - Théogène Barasa 飾 RAF Soldier \#1
  - Dan Barlow 飾 Belgian UN Soldier
  - Johannes Bausch 飾 UN Officer \#1

[S](../Category/美國電影作品.md "wikilink")
[S](../Category/卢旺达大屠杀.md "wikilink")