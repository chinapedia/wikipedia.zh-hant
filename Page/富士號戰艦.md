**富士**是[日本海軍的](../Page/大日本帝國海軍.md "wikilink")[戰艦](../Page/戰艦.md "wikilink")。[富士級戰艦一號艦](../Page/富士級戰艦.md "wikilink")。與其餘五艘日本戰艦（[敷島](../Page/敷島號戰艦.md "wikilink")、[八島](../Page/八島號戰艦.md "wikilink")、[初瀬](../Page/初瀬號戰艦.md "wikilink")、[朝日](../Page/朝日號戰艦.md "wikilink")、[三笠](../Page/三笠號戰艦.md "wikilink")）在1904年至1905年的[日俄戰爭中組成日本主要戰艦群](../Page/日俄戰爭.md "wikilink")。本艦是繼[富士山艦後另一艦以日本著名高峰](../Page/富士山艦.md "wikilink")[富士山命名的日本海軍艦船](../Page/富士山.md "wikilink")。

## 概要

富士和姊妹艦[八島是最早二艘外國為](../Page/八島號戰艦.md "wikilink")[日本製造的](../Page/日本.md "wikilink")[戰艦](../Page/戰艦.md "wikilink")。因為日本在當時未有能力自行製造現代鋼製軍艦，本艦在1894年從[英國](../Page/英國.md "wikilink")[泰晤士鐵工所訂購](../Page/泰晤士鐵工所.md "wikilink")。

在1897年富士從泰晤士鐵工所交付，幫助日本艦隊在[日俄戰爭形成戰艦群核心](../Page/日俄戰爭.md "wikilink")。其後兩次在1904年2月9日的[旅順](../Page/旅順.md "wikilink")（[亞瑟港](../Page/亞瑟港.md "wikilink")）砲擊期間被擊中，其後在3月22日再次砲擊旅順。1904年8月10日，參與了[黃海海戰](../Page/黄海海战_\(1904年\).md "wikilink")。

在1905年5月27日的[對馬海峽海戰中](../Page/對馬海峽海戰.md "wikilink")，富士被11發砲彈命中，而在返航時致命地擊中[俄羅斯的](../Page/俄羅斯.md "wikilink")[博羅第諾號戰艦](../Page/博羅第諾號戰艦.md "wikilink")(Borodino)，導致該艦爆炸並沉沒，當中的830名乘員僅有1人生還。

日俄戰爭結束，富士被安排整修，其中包括換上新鍋爐。1910年，艦上由英國製造的艦砲均由日本製造的所替化，並且艦種被更加為一等海防艦並負起訓練砲手及水兵的責任。其後在[第一次世界大戰時因本艦太過落伍](../Page/第一次世界大戰.md "wikilink")，所以沒有進行戰鬥任務，整個一戰時期都在[吳港中作為訓練船](../Page/吳港.md "wikilink")。

1922年，富士被解除了武裝，但被保留了作為適應船。其推進器、主艦橋、所有砲門均被移除，但增加了巨大的木製甲板屋結構，並且用弄平了的平臺蓋成主甲板。保留為浮動營房和培訓中心在橫須賀超過二十年。於1944年，老舊的富士被用作為開發中心及觀察站，並使用1米長的日本航空母艦模-{型}-去測試各種各樣有效率的[偽裝計劃](../Page/偽裝.md "wikilink")。

在[二戰末期富士遭受了](../Page/第二次世界大戰.md "wikilink")[美國的空襲](../Page/美國.md "wikilink")，卻順利被保留下來，但最後在1948年在[橫須賀被解體](../Page/橫須賀.md "wikilink")。

## 艦歷

  - 1894年8月1日 於[英國泰晤士鐵工所動工](../Page/英國.md "wikilink")。
  - 1896年3月31日 進水。
  - 1897年6月14日
    參加[維多利亞女王即位](../Page/維多利亞女王.md "wikilink")60周年記念的[觀艦式](../Page/觀艦式.md "wikilink")。
  - 1897年8月17日 竣工。
  - 1897年10月31日 到達[橫須賀](../Page/橫須賀.md "wikilink")。
  - 1898年3月21日 一等戰艦。
  - 1898年11月19日 成為常備艦隊親閲御召艦。
  - 1904年
    以[第1艦隊第](../Page/第1艦隊_\(日本海軍\).md "wikilink")1戰隊3號艦參加[日俄戰爭](../Page/日俄戰爭.md "wikilink")。
      - 2月6日開始 參加[旅順港閉塞作戰](../Page/旅順港閉塞作戰.md "wikilink")。
      - 8月10日 參加[黄海海戰](../Page/黄海海战_\(1904年\).md "wikilink")。
  - 1905年5月27日、28日 參加[對馬海峽海戰](../Page/對馬海峽海戰.md "wikilink")。
  - 1911年8月28日 一等[海防艦](../Page/海防艦.md "wikilink")。
  - 1912年 [練習艦](../Page/練習艦.md "wikilink")。
  - 1922年9月1日 [運送特務艦](../Page/運送特務艦.md "wikilink")。
  - 1922年12月1日 [練習特務艦](../Page/練習特務艦.md "wikilink")。
  - 1945年11月30日 除籍。
  - 1948年 於横須賀解體。

## 同級艦

| 船名                                | 圖片                                                                |
| --------------------------------- | ----------------------------------------------------------------- |
| [八島](../Page/八島號戰艦.md "wikilink") | [120px](../Page/檔案:Japanese_battleship_Yashima.jpg.md "wikilink") |
|                                   |                                                                   |

## 參考文獻

  - 、[光人社](../Page/光人社.md "wikilink")、2003年、ISBN 4769811519

<!-- end list -->

  -

    陳寶蓮編譯、《聯合艦隊軍艦大全》、麥田、1997年、 ISBN 9577085334

<!-- end list -->

  - Gibbons, Tony: The Complete Encyclopedia of Battleships and
    Battlecruisers

  - Burt, R.A.: Japanese Battleships, 1897-1945

## 關連項目

  - [大日本帝國海軍艦艇一覽](../Page/大日本帝國海軍艦艇一覽.md "wikilink")

## 外部連結

  -
[Category:日俄战争舰艇](../Category/日俄战争舰艇.md "wikilink")
[Fushi](../Category/日本海军战列舰.md "wikilink")
[Fushi](../Category/日俄戰爭.md "wikilink")
[Category:1897年竣工的船只](../Category/1897年竣工的船只.md "wikilink")