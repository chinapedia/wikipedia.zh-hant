[缩略图执行第一次无线](../Page/文件:Bruce_McCandless_II_during_EVA_in_1984.jpg.md "wikilink")[舱外活动](../Page/舱外活动.md "wikilink")。图片来源自[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")。\]\]
**宇航员**是指接受航天训练后，指挥、操纵或搭乘[航天器的人员](../Page/航天器.md "wikilink")。

## 定义

在美国，以旅行高度超过海拔80[公里](../Page/公里.md "wikilink")（50[英里](../Page/英里.md "wikilink")）的人被称为“”。[国际航空联合会](../Page/国际航空联合会.md "wikilink")（FAI）定义的宇宙航行则需超过100公里。

到2004年4月18日为止，按照美国的定义共计440人，在太空里度过了一共27,082个航行日（crew-day），在太空中散步共用了98个航行日。

按[国际航空联合会的定义](../Page/国际航空联合会.md "wikilink")，只有434人符合资格。[进入太空的宇航员来自至少](../Page/各国宇航员上天时间表.md "wikilink")32个国家。

## 职业称呼

[缩略图](../Page/文件:Astronauts_by_nation.svg.md "wikilink")
[缩略图](https://zh.wikipedia.org/wiki/File:1967_CPA_3476_cancelled.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:McDonnell-Douglas_Space_Station_Concept_-_GPN-2003-00110.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:EVA_Exercise_Device_-_GPN-2000-001819.jpg "fig:缩略图")
截至2013年，全世界仅有前苏联/俄罗斯、美国、中国三个国家拥有发射载人航天任务的能力。其他国家的宇航员都需要与以上三国合作来完成载人航天任务。自1961年人类首次飞天以来，共有来自38个国家的宇航员先后飞天。

[俄语环境下](../Page/俄语.md "wikilink")，按照宇航员升空所承载的发射器来辨别称呼。乘坐前苏联/俄罗斯发射器升空的宇航员称作“”。该词由[希腊语的](../Page/希腊语.md "wikilink")“”（宇宙）和“”（水手）组合而成，意思是“在宇宙中航行的人”。乘坐美国发射器升空的宇航员使用英语借词“”。美俄合作后隶属的[美国国家航空航天局的宇航员一律称作](../Page/美国国家航空航天局.md "wikilink")“”，即便是乘坐俄罗斯的[联盟号飞行](../Page/联盟号宇宙飞船.md "wikilink")，而隶属[欧洲空间局的宇航员一律称作](../Page/欧洲空间局.md "wikilink")“”。乘坐中国发射器升空的宇航员有时称作“”。

[英语环境下](../Page/英语.md "wikilink")，按照宇航员隶属航天机构的国家来辨别称呼。宇航员隶属航天机构的国家除俄罗斯与中国之外，泛称为“”。该词由希腊语的“”（星星）和“”（水手）组合而成，意思是“在星际航行的人”。而隶属前苏联/俄罗斯航天机构的航天员，则使用俄语借词“”。隶属中国航天机构的航天员，有时称作“”，该词由拼音化的“tàikōng”（太空）和希腊语“”（水手）组合而成，

中國將所有国家的宇航员泛称作“-{宇航员}-”。此外，[中国载人航天工程的宇航员又称作](../Page/中国载人航天工程.md "wikilink")“-{航天员}-”。台港澳以及部分海外华侨时常使用“-{太空飛行員}-”作為正式称谓，但也較常使用“太空人”作為这个称谓的簡稱。

[日文环境下](../Page/日文.md "wikilink")，泛称宇航员为“”。

[法语环境下](../Page/法语.md "wikilink")，[法国](../Page/法国.md "wikilink")、[比利时](../Page/比利时.md "wikilink")、[瑞士三国有时称其欧洲空间局的宇航员为](../Page/瑞士.md "wikilink")“”，但一般还是按照发射器隶属国家分稱作“”或“”。“”一词是由[拉丁语](../Page/拉丁语.md "wikilink")“”（空间）和希腊语“”（水手）组合而成，意思是“在空间航行的人”。该词同时被借到英语，拼写为“”。[马来语环境下](../Page/马来语.md "wikilink")，[马来西亚称其宇航员为](../Page/马来西亚.md "wikilink")“”，由“”（空间）与后缀“”（人）组合而成。[印度载人航天计划出炉后](../Page/印度载人航天计划.md "wikilink")，印度各界开始议论未来印度自主发射的宇航员的正式称呼。[印地语环境下](../Page/印地语.md "wikilink")，宇航员在印度正式称呼为“”（源自[梵语](../Page/梵语.md "wikilink")“[地外空间游人](../Page/地球.md "wikilink")”），简称作“”（源自梵语“天空游人”）或“”（源自梵语“空间游人”）。除印地语称呼，还有多个英语翻译的提议，均使用希腊语后缀“”（水手）。包括：“”，源自梵语“”（天堂）；“”，源自梵语“”（宇宙）；“”，源自梵语“”（此生此世）；“”，源自梵语“”（天空或空间）。[印度空间研究组织最终选定](../Page/印度空间研究组织.md "wikilink")“”。

## 太空里程碑

世界上第一名宇航员是[尤里·加加林](../Page/尤里·加加林.md "wikilink")，他在1961年4月12日乘坐[东方一号](../Page/东方一号.md "wikilink")（）进入太空。

第一位女性宇航员是[瓦蓮京娜·捷列什科娃](../Page/瓦蓮京娜·捷列什科娃.md "wikilink")，她在1963年6月乘坐东方6号（）进入太空。

在1961年5月上太空的[艾伦·谢泼德则成为美国首位航天员](../Page/艾伦·谢泼德.md "wikilink")。

[王赣骏是第一位登上太空的華人](../Page/王赣骏.md "wikilink")，他於1985年4月29日至5月6日乘坐挑戰者號太空飛機（使命代號：STS-51-B）進行了為期7天的太空飛行。

2003年10月15日[杨利伟乘坐](../Page/杨利伟.md "wikilink")[神舟五号升空成为中国首名航天员](../Page/神舟五号.md "wikilink")。

其他曾经进入过太空的[美籍华裔包括](../Page/美籍华裔.md "wikilink")[卢杰](../Page/盧傑_\(太空人\).md "wikilink")、[焦立中](../Page/焦立中.md "wikilink")、[张福林](../Page/张福林.md "wikilink")。

最年轻的宇航员是[戈尔曼·季托夫](../Page/戈尔曼·季托夫.md "wikilink")，他乘坐东方2号升空时只有26岁。

最老的是[约翰·格伦](../Page/约翰·格伦.md "wikilink")，他乘坐STS-95升空时已经77岁了。

在太空中逗留时间最长的是[瓦列里·波利亚科夫](../Page/瓦列里·波利亚科夫.md "wikilink")（共437.7天）。

到2003年，个人上太空次数最多的为7次，该项纪录由[杰里·L·罗斯和](../Page/杰里·L·罗斯.md "wikilink")[張福林两人保持](../Page/張福林.md "wikilink")。

宇航员离地球最远的距离是401,056公里，发生在[阿波罗13号紧急事件中](../Page/阿波罗13号.md "wikilink")。

首个自制太空船升空的航天员是迈克·梅尔维尔，乘坐的是[宇宙飞船一号](../Page/宇宙飞船一号.md "wikilink")（SpaceShipOne
Flight
15P）。这与各式各样的百万富翁太空游客形成对比，那些太空游客只是作为公开提供资金的飞行乘客或少数人员（通常由俄罗斯提供飞到[国际空间站的服务](../Page/国际空间站.md "wikilink")）。

在美国，被选为候选航天员将获得银质奖章。他们进入太空后，将收到金质宇航员奖章。美国空军也授予飞越海拔80公里的飞行员宇航员奖章。

## 国际宇航员

直到20世纪70年代末，只有美国和苏联有现役的宇航员。

1976年苏联开始实行[国际宇航员计划](../Page/国际宇航员计划.md "wikilink")，从其他社会主义国家选出6名宇航员组成第一组。第二组于1978年开始训练。

1978年，[欧洲空间局选出](../Page/欧洲空间局.md "wikilink")4名宇航员进行训练，以执行在航天飞机上的首个[太空实验室任务](../Page/太空实验室.md "wikilink")。[法国在](../Page/法国.md "wikilink")1980年开始训练自己的宇航员，接着是[德国于](../Page/德国.md "wikilink")1982年，[加拿大于](../Page/加拿大.md "wikilink")1983年，[日本和](../Page/日本.md "wikilink")[意大利分别于](../Page/意大利.md "wikilink")1985年和1988年。更多的国际运载专家被选为航天飞机航天员，之后俄罗斯的联盟任务也有国际专家的参与。

1998年欧洲空间局组成单一的宇航员组织，以取代此前法国、德国和意大利的国家宇航员组织。

## 女性宇航员

据不完全统计，截至2013年7月，全世界已经有57名女航天员上天，其中美国46名，俄罗斯3名，中国2名

人类历史上第一位进入太空的女性宇航员是[瓦莲京娜·捷列什科娃](../Page/瓦莲京娜·捷列什科娃.md "wikilink")，瓦莲京娜·捷列什科娃于1963年6月16日单独乘坐[东方六号宇宙飞船进入太空](../Page/东方六号.md "wikilink")。\[1\]

## 宇航员的组成

苏联、美国和中国的首位宇航员均来自喷射战斗机的飞行员，通常是有军衔的[试飞员](../Page/试飞员.md "wikilink")。军事宇航员将被授予特别资格奖章，完成宇航员训练和参与太空飞行的军事宇航员则授予[宇航员奖章](../Page/宇航员奖章.md "wikilink")。

## 太空人遇难

到目前为止共有18名宇航员在航天任务中丧生，在地面训练中意外丧生的至少也有10人。

## 参阅

  - [宇航员列表](../Page/宇航员列表.md "wikilink")
  - [各国宇航员首次上天时间表](../Page/各国宇航员首次上天时间表.md "wikilink")
  - [阿波罗宇航员列表](../Page/阿波罗宇航员列表.md "wikilink")

## 参考文献

<div class='references-small'>

<references/>

</div>

## 外部链接

  - [Go
    Taikonaut网站](https://web.archive.org/web/20050405145901/http://www.geocities.com/CapeCanaveral/Launchpad/1921/taikonaut.htm)

{{-}}

[Category:职业](../Category/职业.md "wikilink")
[宇航员](../Category/宇航员.md "wikilink")

1.