[Reloadednfo.png](https://zh.wikipedia.org/wiki/File:Reloadednfo.png "fig:Reloadednfo.png")
RELOADED [.nfo](../Page/.nfo.md "wikilink") by nerv of [Superior Art
Creations](../Page/Superior_Art_Creations.md "wikilink")\]\]

**Reloaded**（又称**RELOADED**或**RLD**）是一个在2004年6月据称由[DEViANCE的前成员创立的](../Page/DEViANCE.md "wikilink")[破解组](../Page/破解组.md "wikilink")。\[1\]RELOADED在PC游戏破解界是个著名的组织，占据了游戏破解界的半壁江山。他们破解了一些受[StarForce和他光盘防拷保护的游戏](../Page/StarForce.md "wikilink")。它们通常在游戏上市的当天就能破解并且发布或者在零售版发布日之前将其破解并发布。他们在[孢子发布前](../Page/孢子_\(游戏\).md "wikilink")4天将其发布并破解\[2\]，但并非所有游戏都可以在正式发布之前被**RELOADED**破解并发布。

## 历史

在2004年6月，RELOADED发布了第一个作品[分裂细胞3：混沌理论](../Page/縱橫諜海：混沌理論.md "wikilink")，这个破解版本是在正式版可用424天后发布的。
\[3\]
在2010年2月27日，**RELOADED**发布了[战地：叛逆连队2的破解版本](../Page/战地：叛逆连队2.md "wikilink")（提前于正式版3天），但有玩家报告游戏控制中存在问题。\[4\]
RELOADED曾经在2次重大的扫荡中生存下来，第一次行动是FBI以代号为“Operation Site
down”进行的扫荡。扫荡中有2个游戏破解组织被迫解散，分别是[HOODLUM和](../Page/HOODLUM.md "wikilink")[VENGEANCE](../Page/VENGEANCE.md "wikilink")。在这以后，RELOADED几乎没有了竞争对手。第二次是GVU策划的。有传言说RELOADED的老大被捕了，但是消息没有被确认，GVU也没发布任何声明。在此期间RELOADED换了一个名字继续发布游戏破解。

## 成就

2008年3月29号。RELOADED发布了“刺客信条”破解版，当时零售版距上市日期还有一个月。

## 参考资料

## 外部链接

  - [RELOADED发布的所有作品](http://www.nforce.nl/index.php?switchto=nfos&menu=quicknav&item=search&search=true&group=RELOADED)
  - [Slyck.com关于GVU's行动的文章](http://www.slyck.com/news.php?story=1065)

[Category:Warez](../Category/Warez.md "wikilink")

1.
2.
3.
4.