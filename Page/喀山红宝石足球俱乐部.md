**喀山红宝石足球俱乐部**（**FC Rubin
Kazan**，，）是一家[俄罗斯职业足球俱乐部](../Page/俄罗斯.md "wikilink")，位于[鞑靼斯坦共和国的](../Page/鞑靼斯坦共和国.md "wikilink")[喀山](../Page/喀山.md "wikilink")，球队在此前的队名叫作
"Iskra" （火花队）(1958-1964), "鲁宾-TAN队" (1992-1993)。

喀山红宝石从未在苏联足球的顶级联赛中露过脸。2003年，球队升入[俄罗斯足球超级联赛](../Page/俄罗斯足球超级联赛.md "wikilink")，在联赛的首个赛季中球队以第三名的佳绩，并且取得了[欧洲联盟杯的参赛资格](../Page/欧洲联盟杯.md "wikilink")。球队在随后的2004赛季中未能再有出彩表现，仅取得第10名。

在[2008球季中](../Page/2008年俄羅斯足球超級聯賽.md "wikilink")，喀山红宝石在開季連勝7場，打破了[莫斯科戴拿模之前所保持的](../Page/莫斯科迪納摩足球俱樂部.md "wikilink")6連勝紀錄，表現一鳴驚人。最終，喀山红宝石更歷史性勇奪聯賽冠軍，並奪得[2009/10賽季歐洲聯賽冠軍盃分組賽一席位](../Page/2009/10賽季歐洲聯賽冠軍盃.md "wikilink")。

2009赛季喀山红宝石保持了强势，第二度夺得俄超冠军也是首度蝉联该项殊荣。

## 现时阵容

## 著名球員

  - 蘇聯/俄羅斯

<!-- end list -->

  - **[高洛杜夫](../Page/高洛杜夫.md "wikilink")**

  -  [捷連舒奧夫](../Page/捷連舒奧夫.md "wikilink")

  - **[阿達莫夫](../Page/阿達莫夫.md "wikilink")**

  - **[保耶連斯夫](../Page/保耶連斯夫.md "wikilink")**

  - **[保卡洛夫](../Page/保卡洛夫.md "wikilink")**

  - [拜斯度夫](../Page/拜斯度夫.md "wikilink")

  - **[施治奧夫](../Page/施治奧夫.md "wikilink")**

  - **[列高](../Page/列高.md "wikilink")**

  - **[施馬克](../Page/施馬克.md "wikilink")**

  - **[辛歷高夫](../Page/辛歷高夫.md "wikilink")**

  - **[沙朗洛夫](../Page/沙朗洛夫.md "wikilink")**

  - **[舒洛哥夫](../Page/舒洛哥夫.md "wikilink")**

  - [施梅頓高夫](../Page/施梅頓高夫.md "wikilink")

  - [卡利辛](../Page/卡利辛.md "wikilink")

  - [列耶辛斯夫](../Page/列耶辛斯夫.md "wikilink")

  - [連捷哥夫](../Page/連捷哥夫.md "wikilink")

  - [華斯耶夫](../Page/華斯耶夫.md "wikilink")

<!-- end list -->

  - 前蘇聯國家

<!-- end list -->

  - [賀夫漢尼斯恩](../Page/賀夫漢尼斯恩.md "wikilink")

  - **[卡查泰恩](../Page/卡查泰恩.md "wikilink")**

  - [柏斯恩](../Page/柏斯恩.md "wikilink")

  - [高維蘭高](../Page/高維蘭高.md "wikilink")

  - **[列迪斯尤克](../Page/列迪斯尤克.md "wikilink")**

  - **[艾殊維迪亞](../Page/艾殊維迪亞.md "wikilink")**

  - **[沙拉迪斯](../Page/沙拉迪斯.md "wikilink")**

<!-- end list -->

  - **[堅克拉錫](../Page/堅克拉錫.md "wikilink")**

  - **[基域維利亞](../Page/基域維利亞.md "wikilink")**

  - **[雷維舒維利](../Page/雷維舒維利.md "wikilink")**

  - **[沙魯卡華迪斯](../Page/沙魯卡華迪斯.md "wikilink")**

  - [施拉加迪斯](../Page/施拉加迪斯.md "wikilink")

  - **[艾斯達捷夫斯](../Page/艾斯達捷夫斯.md "wikilink")**

  - [保拉高夫斯](../Page/保拉高夫斯.md "wikilink")

  - **[哥連高](../Page/哥連高.md "wikilink")**

  - **[薛斯利維斯](../Page/薛斯利維斯.md "wikilink")**

  - [貝特高斯](../Page/貝特高斯.md "wikilink")

  - [丹辛卡](../Page/丹辛卡.md "wikilink")

  - [卡朗拿斯](../Page/卡朗拿斯.md "wikilink")

  - **[米卡拉古拿斯](../Page/米卡拉古拿斯.md "wikilink")**

  - **[加卡恩](../Page/加卡恩.md "wikilink")**

  - [施賓奴](../Page/施賓奴.md "wikilink")

  - **[拿沙拜拉莫禾](../Page/拿沙拜拉莫禾.md "wikilink")**

  - **[維拉迪米拜拉莫禾](../Page/維拉迪米拜拉莫禾.md "wikilink")**

  - **[卡亞斯克](../Page/卡亞斯克.md "wikilink")**

  - [夏蘭奴夫斯基](../Page/夏蘭奴夫斯基.md "wikilink")

  - [列布夫](../Page/列布夫.md "wikilink")

  - [史維斯倫諾夫](../Page/史維斯倫諾夫.md "wikilink")

  - [達維利托夫](../Page/達維利托夫.md "wikilink")

  - **[費奧多羅夫](../Page/費奧多羅夫.md "wikilink")**

  - **[加利連](../Page/加利連.md "wikilink")**

  - [夏辛諾夫](../Page/夏辛諾夫.md "wikilink")

  - [拿斯莫夫](../Page/拿斯莫夫.md "wikilink")

<!-- end list -->

  - 歐洲

<!-- end list -->

  - [羅素利](../Page/羅素利.md "wikilink")

  - [史積賓湯馬斯](../Page/史積賓湯馬斯.md "wikilink")

  - [杜斯達歷](../Page/杜斯達歷.md "wikilink")

  - [積利路禾尼](../Page/積利路禾尼.md "wikilink")

  - [柏特魯斯](../Page/柏特魯斯.md "wikilink")

  - [拿赫奧](../Page/拿赫奧.md "wikilink")

  - **[梅羅斯基](../Page/梅羅斯基.md "wikilink")**

  - **[米路斯域](../Page/米路斯域.md "wikilink")**

  - [包奴域](../Page/包奴域.md "wikilink")

  - [施薩拿華斯](../Page/施薩拿華斯.md "wikilink")

  - [簡錫](../Page/簡錫.md "wikilink")

  - [特基](../Page/特基.md "wikilink")

  - **[卡拿丹尼斯](../Page/卡拿丹尼斯.md "wikilink")**

  - [杜基奧](../Page/杜基奧.md "wikilink")

<!-- end list -->

  - 美洲

<!-- end list -->

  - [杜明基斯](../Page/杜明基斯.md "wikilink")

  - **[安沙迪](../Page/安沙迪.md "wikilink")**

  - [艾萊斯奧](../Page/艾萊斯奧.md "wikilink")

  - [隆尼](../Page/洛尼捷臣·度斯·山度士.md "wikilink")

  - **[卡路士莫拿](../Page/卡路士莫拿.md "wikilink")**

  - **[盧保亞](../Page/盧保亞.md "wikilink")**

  - **[拉夫](../Page/拉夫.md "wikilink")**

  - **[史葛迪](../Page/史葛迪.md "wikilink")**

<!-- end list -->

  - 非洲

<!-- end list -->

  - **[斯拉](../Page/斯拉.md "wikilink")**

  - **[艾比度基斯](../Page/艾比度基斯.md "wikilink")**

  - **[施巴亞](../Page/西巴耶.md "wikilink")**

  - **[艾捷奧亞](../Page/艾捷奧亞.md "wikilink")**

<!-- end list -->

  - 亞洲

<!-- end list -->

  - **[金東炫](../Page/金東炫.md "wikilink")**

## 獎項

  - [俄羅斯足球超級聯賽](../Page/俄羅斯足球超級聯賽.md "wikilink")：**2**
      - 冠軍

<!-- end list -->

  -

      -
        [2008](../Page/2008年俄羅斯足球超級聯賽.md "wikilink")、2009

<!-- end list -->

  - [俄羅斯超級盃](../Page/俄羅斯超級盃.md "wikilink")：**1**
      - 亞軍

<!-- end list -->

  -

      -
        2009

## 外部連結

  - [Club's web site](http://www.rubin-kazan.ru/)

[Category:喀山體育](../Category/喀山體育.md "wikilink") [Rubin
Kazan](../Category/俄羅斯足球俱樂部.md "wikilink")
[Category:1958年建立的足球俱樂部](../Category/1958年建立的足球俱樂部.md "wikilink")