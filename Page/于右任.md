__NOTITLECONVERT__

<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>（<a href="../Page/清朝.md" title="wikilink">清</a>）辛丑<a href="../Page/舉人.md" title="wikilink">舉人</a></li>
<li><a href="../Page/震旦學院.md" title="wikilink">震旦學院</a></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>（<a href="../Page/中華民國臨時政府_(南京).md" title="wikilink">臨時政府</a>）<a href="../Page/中華民國交通部.md" title="wikilink">交通部次長</a></li>
<li><a href="../Page/陝西省.md" title="wikilink">陝西省總司令</a></li>
<li><a href="../Page/國民黨.md" title="wikilink">國民黨中央執行委員委員</a></li>
<li>國民黨北京政治分會委員</li>
<li><a href="../Page/國民政府.md" title="wikilink">國民政府委員</a><br />
<span style="color: blue;">(1925年7月1日－1947年)</span></li>
<li>（國民政府）審計院院長<br />
<span style="color: blue;">(1928年2月28日－1931年2月21日)</span></li>
<li>（國民政府）<a href="../Page/監察院.md" title="wikilink">監察院院長</a><br />
<span style="color: blue;">(1930年11月18日－1947年)</span></li>
<li>（國民政府）總理陵園管理委員會委員<br />
<span style="color: blue;">(1929年6月28日－1946年7月9日)</span></li>
<li>（國民政府）國父陵園管理委員會委員<br />
<span style="color: blue;">(1946年7月9日－1948年)</span></li>
<li>（國民政府）稽勳委員會委員<br />
<span style="color: blue;">(1941年)</span></li>
<li>（國民政府）財政委員會委員<br />
<span style="color: blue;">(1929年1月31日－1932年)</span></li>
<li>（國民政府）首都建設委員會委員<br />
<span style="color: blue;">(1929年)</span></li>
<li>（國民政府）法官懲戒委員會委員<br />
<span style="color: blue;">(1928年)</span></li>
<li>（國民政府）預算委員會委員<br />
<span style="color: blue;">(1928年8月29日－1929年3月4日)</span></li>
<li>（國民政府）軍事委員會委員<br />
<span style="color: blue;">(1927年10月19日－1928年)</span></li>
<li><a href="../Page/西北农林科技大学.md" title="wikilink">國立西北農林專科學校</a>（第一任）校長<br />
<span style="color: blue;">(1934年-1935年)</span></li>
<li><a href="../Page/監察院.md" title="wikilink">監察院</a>（第一屆，第一任）院長<br />
<span style="color: blue;">(1948年6月9日－1964年11月10日)</span></li>
<li>最高國防委員會委員</li>
<li><a href="../Page/監察院.md" title="wikilink">監察院</a><a href="../Page/監察委員.md" title="wikilink">監察委員</a><br />
<span style="color: blue;">(1948年6月9日－1964年11月10日)</span></li>
<li><a href="../Page/中國國民黨.md" title="wikilink">中國國民黨中央評議委員會委員</a></li>
<li><a href="../Page/制憲國民大會.md" title="wikilink">制憲國民大會代表</a></li>
<li><a href="../Page/國民大會.md" title="wikilink">國民大會</a>（第一屆）代表</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 著作</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><span style="color: green;">《右任詩存初二集》</li>
<li><span style="color: green;">《右任近十年詩存》</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**-{于}-右任**（），[陕西](../Page/陕西.md "wikilink")[三原人](../Page/三原.md "wikilink")，祖籍[涇陽](../Page/涇陽.md "wikilink")。原名**伯循**，字**誘人**，爾後以“**誘人**”諧音“右任”為名；别署“**髾心**”、“**髯翁**”，晚年自號“**太平老人**”。[中華民國開國元勛之一](../Page/中華民國.md "wikilink")。-{于}-右任早年係[中國同盟會成員](../Page/中國同盟會.md "wikilink")，[民國成立之後長年在](../Page/中華民國.md "wikilink")[政府擔任高級官員](../Page/中華民國政府.md "wikilink")，尤其擔任[監察院](../Page/監察院.md "wikilink")[院長長達](../Page/監察院院長.md "wikilink")34年，是歷史上在任最久的[五院院長](../Page/五院.md "wikilink")。同時也是[中國近代知名的](../Page/中國.md "wikilink")[書法家](../Page/書法家.md "wikilink")。-{于}-右任長髯飄飄，是其一大特徵。

## 早年經歷

\-{于}-右任是清朝[光绪年间](../Page/光绪.md "wikilink")[举人](../Page/举人.md "wikilink")，1904年因刊印《半哭半笑樓詩草》譏諷時政被[三原](../Page/三原县.md "wikilink")[县令德锐和](../Page/县令.md "wikilink")[陕甘总督](../Page/陕甘总督.md "wikilink")[升允举发](../Page/升允.md "wikilink")，遭[清廷](../Page/清廷.md "wikilink")[通緝](../Page/通緝.md "wikilink")，[流亡](../Page/流亡.md "wikilink")[上海](../Page/上海.md "wikilink")，遂進入[震旦公學](../Page/震旦公學.md "wikilink")。[震旦学院肄业](../Page/震旦学院.md "wikilink")。同年4月到达[日本](../Page/日本.md "wikilink")，加入[光复会和](../Page/光复会.md "wikilink")[同盟会](../Page/同盟会.md "wikilink")。

## 报人生涯

\-{于}-右任的報刊活動主要在1907年到1912年[民國成立前](../Page/中華民國.md "wikilink")5年當中。其中與1909年開始連續創辦三份報紙：《民呼日報》、《民籲日報》、《[民立報](../Page/民立報.md "wikilink")》被稱為‘[豎三民](../Page/豎三民.md "wikilink")’。

1907年，於上海四馬路群學社書店創辦大型日報《[神州日報](../Page/神州日報.md "wikilink")》，雖未聲明是革命派言論機關，但實際上革命傾向非常明顯。創刊後僅80天，因鄰居失火，殃及報社，編輯、印刷、營業部均被燒毀，于右任無力恢復，自行辭退。離開後經幾個月的籌備，1909年5月15日創辦《民呼日報》，由於負面報導很快遭到清廷控告，被逐出租界，被迫停刊。停刊兩月後，-{於}-右任在[法租界創辦](../Page/上海法租界.md "wikilink")《民籲日報》。很快由於報導時任[日本](../Page/日本.md "wikilink")[首相](../Page/日本首相.md "wikilink")[伊藤博文遭](../Page/伊藤博文.md "wikilink")[朝鮮人](../Page/朝鮮半島.md "wikilink")[安重根刺殺事件傾向性明顯遭到日方抗議](../Page/安重根.md "wikilink")，租界當局於1909年11月查封了《民籲日報》。後在[沈縵雲資助下與](../Page/沈縵雲.md "wikilink")1910年10月創辦《民立報》，積極宣傳民主革命。

1913年[二次革命失败后](../Page/二次革命.md "wikilink")，-{于}-右任逃亡日本，《民立报》停刊。

## 仕途

[Yu_Youren's_voting_20090420.jpg](https://zh.wikipedia.org/wiki/File:Yu_Youren's_voting_20090420.jpg "fig:Yu_Youren's_voting_20090420.jpg")[國民政府](../Page/國民政府.md "wikilink")[监察院](../Page/监察院.md "wikilink")[院长](../Page/監察院.md "wikilink")-{于}-右任投票\]\]
[中華民國成立後](../Page/中華民國.md "wikilink")，任[临时政府](../Page/中華民國臨時政府_\(南京\).md "wikilink")[交通部次长](../Page/中華民國交通部.md "wikilink")。1917年响应[孙中山](../Page/孙中山.md "wikilink")[护法运动](../Page/护法运动.md "wikilink")，成立陕西靖国军，任驻陕[总司令](../Page/总司令.md "wikilink")。1922年创办[上海大学](../Page/上海大学.md "wikilink")。1924年出席[国民党一大](../Page/国民党一大.md "wikilink")。年底陪同[孙中山北上赴京](../Page/孙中山.md "wikilink")。

1926年，代表[中國國民黨赴](../Page/中國國民黨.md "wikilink")[蘇聯促](../Page/蘇聯.md "wikilink")[馮玉祥速回國參加](../Page/馮玉祥.md "wikilink")[國民革命軍北伐](../Page/國民革命軍北伐.md "wikilink")。馮玉祥由蘇俄回國，督率所部橫越朔漠，經過[寧夏](../Page/寧夏.md "wikilink")，進入[甘肅](../Page/甘肅.md "wikilink")，以[孫良誠為援陝總指揮](../Page/孫良誠.md "wikilink")\[1\]。-{于}-右任先由五原起行，兼程回陝西收集國民二軍及國民三軍餘部，組成聯軍，率領7個師大軍，東進援救陝西\[2\]。

[國民政府](../Page/國民政府.md "wikilink")[北伐統一全國后](../Page/北伐.md "wikilink")，-{于}-右任歷任审计院、[监察院院长](../Page/监察院.md "wikilink")，是国民政府重要决策人物，前後共任監察院院長34年。

1932年秋，籌備建設國立西北農林專科學校（今[西北農林科技大學](../Page/西北農林科技大學.md "wikilink"))，並就任首任校長。

## 晚年經歷

1948年4月19日，在當年3月的[總統選舉過後](../Page/1948年中華民國總統選舉.md "wikilink")，曾参选副总统選舉，然而根據當時的國民大會秘書處所公布副總統候選人簽署代表人數：于右任獲得512票的簽署支持，僅次於[孫科](../Page/孫科.md "wikilink")。在4月20日下午的首輪投票中，于右任只獲得了493票，卻在同時競選的六人中排名第四，而無法進入第二輪投票。

1948年6月，-{于}-右任當選為監察院院長，[劉哲當選為副院長](../Page/劉哲.md "wikilink")\[3\]。

1949年，随[中華民國政府遷居](../Page/中華民國政府.md "wikilink")[台灣](../Page/台灣.md "wikilink")[台北市青田街官邸](../Page/台北市.md "wikilink")。-{于}-右任寓居臺灣十五年，台灣書法風氣首次出現碑學壓倒館閣帖的趨勢。

## 逝世

1962年1月12日，-{于}-右任日記寫道：「我百年後，願葬於[玉山或](../Page/玉山.md "wikilink")[阿里山樹木多的高處](../Page/阿里山山脈.md "wikilink")，可以時時望大陸（旁邊自注：山要高者，樹要大者）。遠遠是何鄉，是我之故鄉。我之故鄉是中國大陸\[4\]。」

1月24日，-{于}-右任於日記中寫下《思鄉歌》：“葬我於高山之上兮，望我大陆；[大陆不可见兮](../Page/中国大陆.md "wikilink")，只有痛哭。葬我於高山之上兮，望我故乡；故乡不可见兮，永不能忘。天苍苍，海茫茫，山之上，有国殇。不得大陆，不能回乡，大陆乎，何日光复\[5\]？”

1964年8月12日，于右任因病於臺北榮民總醫院住院治療，後於11月10日20:08病逝，墓位於[台灣](../Page/台灣.md "wikilink")[陽明山國家公園](../Page/陽明山國家公園.md "wikilink")[巴拉卡公路上](../Page/巴拉卡公路.md "wikilink")。

## 身後

[三峽行修宮中的于右任書法.jpg](https://zh.wikipedia.org/wiki/File:三峽行修宮中的于右任書法.jpg "fig:三峽行修宮中的于右任書法.jpg")內于右任書「忠心昭日月，義氣貫乾坤」，[1964年](../Page/1964年.md "wikilink")[1月](../Page/1月.md "wikilink")\]\]
[Standing_statue_of_Yu_Youren_at_Sun_Yat-sen_Memorial_Hall_20060825.jpg](https://zh.wikipedia.org/wiki/File:Standing_statue_of_Yu_Youren_at_Sun_Yat-sen_Memorial_Hall_20060825.jpg "fig:Standing_statue_of_Yu_Youren_at_Sun_Yat-sen_Memorial_Hall_20060825.jpg")
-{于}-右任去世後，[中華民國政府在](../Page/中華民國政府.md "wikilink")[玉山主峰樹立其雕像](../Page/玉山.md "wikilink")，計3公尺高，本意在補足4,000公尺高度（玉山當時測量數據為海拔3,997米），並有尊重-{于}-右任遙望大陸遺願「葬我於高山兮，望我大陸」之意。唯銅像1995年、1996年[二次遭到破壞](../Page/玉山于右任銅像破壞事件.md "wikilink")；其中參與1995年11月1日第一次「砍頭」的「元兇」[葉博文](../Page/葉博文.md "wikilink")（前[建國廣場成員](../Page/建國廣場.md "wikilink")、前[台北二二八紀念館館長](../Page/台北二二八紀念館.md "wikilink")、[核四公投促進會執行長](../Page/核四公投促進會.md "wikilink")）在追訴期過後的2007年2月12日首次公開承認破壞行為並引以為榮，而另二位破壞者為[林長安](../Page/林長安.md "wikilink")（前[綠色和平電台主持人](../Page/綠色和平電台.md "wikilink")）與[陳朝順](../Page/陳朝順.md "wikilink")（[綠黨](../Page/綠黨_\(臺灣\).md "wikilink")2002年[台北市第六選舉區](../Page/台北市.md "wikilink")[市議員候選人](../Page/台北市議員.md "wikilink")），三人破壞的理由為「還我玉山原貌」；在首次被「斷頭」經修復後不久，在1996年5月初被整個拆除、丟入山谷，不知所終，這次破壞迄今無人出面承認說明\[6\]。

台北市[仁愛路與](../Page/仁愛路_\(台北市\).md "wikilink")[敦化南路交叉處的仁愛圓環設有](../Page/敦化南路.md "wikilink")-{于}-右任銅像，後來在[陳水扁市長任內藉](../Page/陳水扁.md "wikilink")[元宵節燈會佈置之便破壞銅像基座](../Page/臺北燈節.md "wikilink")，並以此為由遷移至[國父紀念館](../Page/國父紀念館.md "wikilink")\[7\]。

位在[基隆市的](../Page/基隆市.md "wikilink")[崇右技術學院為](../Page/崇右技術學院.md "wikilink")-{于}-右任所提議創設，並於1964年開始招生，學校名稱有崇敬-{于}-右任之意涵。

2012年，位於陽明山的[于右任墓被定為新北市三級古蹟](../Page/于右任墓.md "wikilink")。

2014年3月，臺灣爆發[佔領國會事件](../Page/佔領國會事件.md "wikilink")，-{于}-右任題寫的“立法院”匾額被[示威群眾拆下](../Page/示威.md "wikilink")，台北市警局中正一分局20日表示，匾額已取回，先暫存分局內\[8\]。

2014年，于右任逝世50周年紀念大會12月17日在臺北舉行，兩岸逾百位人士出席。[中華民國副總統](../Page/中華民國副總統.md "wikilink")[吳敦義出席](../Page/吳敦義.md "wikilink")，並認為：于右任先生參與革命有成，在任[監察院長期間端肅政風](../Page/監察院.md "wikilink")，更在書法上取得很大成就。

為紀念于右任逝世50周年，其祖籍陝西涇陽和出生地三原近日都舉行了紀念儀式和家祭。于右任的侄孫女于媛與中國大陸鄉親60余人近日還前往位於臺北陽明山的于右任墓園，以古禮獻上了來自家鄉的泥土、水和五穀等祭品。

## 名言

《亡国三恶因》：

1.  民穷财尽，社会破产，国家破产。国有金，吝不与人，为他人藏。此其一。
2.  善不能举，恶不能退，利不能兴，害不能除。化善而作贪，使学而为盗。此其二。
3.  宫中、府中、梦中，此哭中、彼笑中，外人窥伺中、霄小拨弄中，国际侦探金钱运动中，一举一动，一黜一陟，堕其术中。此其三。

## 著作和成就

\-{于}-右任精[书法](../Page/书法.md "wikilink")，尤擅[草书](../Page/草书.md "wikilink")，首創“[標準草書](../Page/標準草書.md "wikilink")”，被誉为“当代草圣”、“近代書聖”。-{于}-右任被列為「民國四大[书法家](../Page/书法家.md "wikilink")」\[9\]，其为[楷书](../Page/楷书.md "wikilink")[谭延闿](../Page/谭延闿.md "wikilink")、[草书](../Page/草书.md "wikilink")-{于}-右任、[隶书](../Page/隶书.md "wikilink")[胡汉民](../Page/胡汉民.md "wikilink")、[篆书](../Page/篆书.md "wikilink")[吴稚晖](../Page/吴稚晖.md "wikilink")。

\-{于}-右任1932年在上海創辦標準草書社，以易識、易寫、準確、美麗為原則，整理、研究與推廣草書，整理成系统的草书代表符号，集字編成《標準草書千字文》（1936年由[上海文正楷印书局初版](../Page/上海.md "wikilink")），影響深遠，至今仍在重印。著作《右任詩存》、《右任文存》、《右任墨存》、《標準草書》、《牧羊兒的自述》（1953）等。

## 軼事

\-{于}-右任擔任監察院長時看到員工隨處小便，遂提筆寫下「不可隨處小便」公告張貼，因墨寶珍貴被人偷去收藏；又因-{于}-的標準草書字字可分離，後人便重新組裝成「小處不可隨便」\[10\]。

## 子女

長子，[-{于}-望德](../Page/望德.md "wikilink")（1910－？），早年赴[英國留學](../Page/英國.md "wikilink")，先入[愛丁堡大學](../Page/愛丁堡大學.md "wikilink")，獲法學士學位。後入[倫敦大學](../Page/倫敦大學.md "wikilink")，獲[政治學](../Page/政治學.md "wikilink")[博士學位](../Page/博士.md "wikilink")。期間曾任國民黨英國愛丁堡支部書記。返國後歷任[重慶大學](../Page/重慶大學.md "wikilink")、[中央大學](../Page/中央大學.md "wikilink")[教授](../Page/教授.md "wikilink")。國民政府[行政院憲政計劃委員會法制組專門委員](../Page/行政院憲政計劃委員會.md "wikilink")、經濟會議政務組副主任、國民政府[行政院](../Page/行政院.md "wikilink")[參事](../Page/參事.md "wikilink")。後在國民黨中央訓練團黨政訓練班十三期和[國防研究院受訓](../Page/國防研究院.md "wikilink")。1945年當選爲國民黨第六屆中央候補執行委員。1947年5月，任駐[哥倫比亞全權公使](../Page/哥倫比亞.md "wikilink")；6月兼駐[委內瑞拉全權公使](../Page/委內瑞拉.md "wikilink")；7月兼駐[厄瓜多爾全權公使](../Page/厄瓜多爾.md "wikilink")。1948年當選爲行憲國民大會代表。1954年11月，任駐[巴拿馬大使](../Page/巴拿馬.md "wikilink")。1955年兼任駐[洪都拉斯公使](../Page/洪都拉斯.md "wikilink")。1956年卸任，從巴拿馬返回台灣，任[外交部顧問](../Page/外交部.md "wikilink")、行政院顧問、[外交領使人員講習所顧問](../Page/外交領使人員講習所.md "wikilink")、[中國文化學院政治研究所所長](../Page/中國文化學院.md "wikilink")、國防研究院首席講座等。1984年-{于}-望德捐贈「南宋影青印花水草紋蓋盒」給[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")。

次子，[-{于}-彭](../Page/于彭.md "wikilink")（1916年9月9日－），字仲芩，[金陵大學肄業後](../Page/金陵大學.md "wikilink")，於英國愛丁堡大學獲碩士學位，後在[哥倫比亞大學任國際關係研究員](../Page/哥倫比亞大學.md "wikilink")。曾任中華民國外交部美洲司專門委員。1956年3月，任中華民國駐秘魯大使館參事。1962年10月，任駐牙買加大使館代辦。1967年2月起，先後任外交部情報司副司長、簡任秘書兼[國外經濟文化事務委員會執行秘書](../Page/國外經濟文化事務委員會.md "wikilink")、[研究設計委員會委員兼執行秘書](../Page/研究設計委員會.md "wikilink")、行政院參議等職。1971年7月，調任中華民國駐[菲律賓大使館公使](../Page/菲律賓.md "wikilink")。1976年1月，任外交部領事事務處處長。1980年1月起，擔任中華民國駐宏都拉斯大使。2016年-{于}-彭捐贈所藏18件其父書法珍品給[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")\[11\]，2017年6月國立故宮博物院展出3個月的「自然生姿態－-{于}-右任書法特展」\[12\]，當中主打選件就為-{于}-彭捐贈之精品。

四女于念慈，其夫[張澄基](../Page/張澄基.md "wikilink")。

## 書法作品集錦

[File:行天宮3.jpg|-{于}-右任題字的](File:行天宮3.jpg%7C-%7B于%7D-右任題字的)[台北市](../Page/台北市.md "wikilink")[行天宮](../Page/行天宮.md "wikilink")
<File:The> memory bud of Taiping steamer in
Keelung.jpg|-{于}-右任題字的[太平輪紀念碑文](../Page/太平輪.md "wikilink")
<File:TW> The Grand Hotel 4
words.JPG|-{于}-右任題字的[圓山飯店之](../Page/圓山飯店.md "wikilink")「劍潭勝跡」碑石
[File:東海大學標準字.png|缩略图](File:東海大學標準字.png%7C缩略图)|[東海大學匾額](../Page/東海大學.md "wikilink")

## 参考资料

  - 《[标准草书](../Page/标准草书.md "wikilink")》，-{于}-右任编著，上海书店出版社，2002年，ISBN
    978-7-80569-228-9

### 腳註

## 外部連結

  - [梅庭 臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/206)

|- |colspan="3"
style="text-align:center;"|**[中華民國](../Page/中華民國.md "wikilink")[監察院](../Page/監察院.md "wikilink")**
|-    |- |colspan="3"
style="text-align:center;"|**[中華民國](../Page/中華民國.md "wikilink")[國民政府](../Page/國民政府.md "wikilink")**
|-

[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:制憲國民大會代表](../Category/制憲國民大會代表.md "wikilink")
[Category:第一屆監察委員](../Category/第一屆監察委員.md "wikilink")
[Category:監察院院長](../Category/監察院院長.md "wikilink")
[Category:陕西靖国军将领](../Category/陕西靖国军将领.md "wikilink")
[Category:北洋将军府冠字将军](../Category/北洋将军府冠字将军.md "wikilink")
[Category:中華民國副總統參選人](../Category/中華民國副總統參選人.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中華民國書法家](../Category/中華民國書法家.md "wikilink")
[Category:中國書法家](../Category/中國書法家.md "wikilink")
[Category:台灣戰後陜西移民](../Category/台灣戰後陜西移民.md "wikilink")
[Category:三原人](../Category/三原人.md "wikilink")
[Y右](../Category/于姓.md "wikilink")
[Category:中華民國審計部首長](../Category/中華民國審計部首長.md "wikilink")
[Category:安葬於新北市者](../Category/安葬於新北市者.md "wikilink")
[Category:光緒二十八年壬寅科舉人](../Category/光緒二十八年壬寅科舉人.md "wikilink")
[Category:中国国民党第一届中央执行委员会委员](../Category/中国国民党第一届中央执行委员会委员.md "wikilink")
[Category:中国国民党第二届中央执行委员会委员](../Category/中国国民党第二届中央执行委员会委员.md "wikilink")
[Category:中国国民党第三届中央执行委员会委员](../Category/中国国民党第三届中央执行委员会委员.md "wikilink")
[Category:中国国民党第四届中央执行委员会委员](../Category/中国国民党第四届中央执行委员会委员.md "wikilink")

1.
2.

3.

4.

5.

6.

7.

8.

9.  [自然生姿態－于右任書法特展](https://www.npm.gov.tw/Article.aspx?sNo=04009391)，[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")

10.

11. [于右任18件傳世寶
    于彭全都捐給故宮](http://www.cna.com.tw/news/firstnews/201712140149-1.aspx)

12. [自然生姿態—于右任書法特展_展覽概述](http://theme.npm.edu.tw/exh106/YuYujensCalligraphy/ch/index.html)