**Eurorunner**是由[德國](../Page/德國.md "wikilink")[西門子公司製造的一系列柴油機車](../Page/西門子公司.md "wikilink")，是歐洲鐵路常用的柴油機車（尤其是[立陶宛鐵路及](../Page/立陶宛鐵路.md "wikilink")[奧地利聯邦鐵路](../Page/奧地利聯邦鐵路.md "wikilink")，是本型機車最大使用者），並可在客、貨運上使用，亦是[港鐵](../Page/港鐵.md "wikilink")[東鐵綫的](../Page/東鐵綫.md "wikilink")[柴油機車車款之一](../Page/柴油機車.md "wikilink")，曾經是貨運用機車，鐵路貨運停辦後用作牽引維修列車、新列車與前往[港鐵八鄉車廠進行擴編的列車](../Page/港鐵八鄉車廠.md "wikilink")，以及在何東樓車廠過夜的一列[25T型客車](../Page/25T型客車.md "wikilink")。港鐵所用機車由[九廣鐵路公司擁有](../Page/九廣鐵路公司.md "wikilink")，[兩鐵合併後租予](../Page/兩鐵合併.md "wikilink")[港鐵公司使用](../Page/香港鐵路有限公司.md "wikilink")，目前主要作為工程車\[1\]。

## 概述

這款機車由[德國](../Page/德國.md "wikilink")[西門子交通公司設計](../Page/西門子交通.md "wikilink")，是一款在歐洲地區被廣泛使用的[柴電機車](../Page/柴電機車.md "wikilink")，本車型的廠方型號是**ER20**，代表其[電動機輸出](../Page/電動機.md "wikilink")[功率為](../Page/功率.md "wikilink")2,000 kW\[2\]。此外，本型機車亦採用德國柴電潛艇的隔音技術，以降低機車發生的噪音。

### 香港

供香港使用的ER20 8000系是以[奧地利聯邦鐵路的Rh](../Page/奧地利聯邦鐵路.md "wikilink")
2016型機車為基礎設計。該機車使用[柴油機推動](../Page/柴油機.md "wikilink")[發電機](../Page/發電機.md "wikilink")，發動機輸出的電力會供應予4台500
kW的交流牽引[電動機](../Page/電動機.md "wikilink")，從而產生推動機車前進的動力。ER20與1954年起使用的[G12型柴油機車相比](../Page/G12型柴油機車.md "wikilink")，ER20型具有低污染及低噪音等環保特點。

## 應用

### 立陶宛

[Lietuvos_Geležinkeliai_ER20-001,_Vilnius.jpg](https://zh.wikipedia.org/wiki/File:Lietuvos_Geležinkeliai_ER20-001,_Vilnius.jpg "fig:Lietuvos_Geležinkeliai_ER20-001,_Vilnius.jpg")
2007年，西門子在ER20原型機車基礎之上，再研發了增強型的ER20
CF貨運機車，首個客戶為[立陶宛鐵路](../Page/立陶宛鐵路.md "wikilink")。該批機車與原型車採用同款引擎，但油箱容量增至7000升，而軌距亦改為俄羅斯軌距（1520毫米），以適應當地固有鐵路。此外，車頭設計亦有改變。

### 奧地利

[OEBB2016.jpg](https://zh.wikipedia.org/wiki/File:OEBB2016.jpg "fig:OEBB2016.jpg")
[奧地利聯邦鐵路亦有使用本型機車](../Page/奧地利聯邦鐵路.md "wikilink")，並命名為Rh
2016型「大力神」貨運機車，分別由四間當地租賃公司擁有，合共100輛，統一使用聯邦鐵路漆裝及「OBB」品牌。該等機車外觀與原型車相同，但馬力略小。

本型機車是[中歐班列貨運列車在奧地利段的本務機](../Page/中歐班列.md "wikilink")，以雙機重聯方式運作。

### 德國

[ER20_Vogtlandbahn.jpg](https://zh.wikipedia.org/wiki/File:ER20_Vogtlandbahn.jpg "fig:ER20_Vogtlandbahn.jpg")插座，位於車頭下方兩側\]\]
[德國鐵路亦使用Eurorunner統型機車](../Page/德國鐵路.md "wikilink")（即ER27
CF機車，按照[國際鐵路聯盟標準設計](../Page/國際鐵路聯盟.md "wikilink")），分別由四間營運公司擁有。

### 香港

[ER20_near_MKK.JPG](https://zh.wikipedia.org/wiki/File:ER20_near_MKK.JPG "fig:ER20_near_MKK.JPG")
[九廣鐵路公司於](../Page/九廣鐵路公司.md "wikilink")2001年向西門子訂購5輛ER20型非統型機車，合約總值1.3億港元，全數5輛機車編號8001-8005。合約於2001年11月9日簽訂。

2003年7月24日，該5輛機車首度曝光，並帶有九廣鐵路公司標誌及編號，於[德國](../Page/德国.md "wikilink")[慕尼黑郊區的](../Page/慕尼黑.md "wikilink")[德國鐵路路軌上試運輸](../Page/德國鐵路.md "wikilink")\[3\]。

5輛機車及後經船運於2003年9月13日運抵香港，並在紅磡貨場上岸，至2004年1月投入服務，並於1月29日舉行新機車移交典禮，由8005號機車擔當主禮機車。本型機車最初主要用於牽引貨運列車。九鐵訂購這些機車的目的，是為了取代1950年代製造的[G12型柴油機車](../Page/G12型柴油機車.md "wikilink")，並為大圍車廠提供一輛用作牽引列車進出維修區的機車（但統一配屬[羅湖編組站機車行車室](../Page/羅湖編組站.md "wikilink")（2012年後），[港鐵大圍車廠用車每次不定](../Page/港鐵大圍車廠.md "wikilink")，直至配屬該廠的[CKD0A型內燃機車於](../Page/CKD0A型內燃機車.md "wikilink")2015年投入服務後，本型機車不再在[馬鞍山綫上使用為止](../Page/馬鞍山綫.md "wikilink")），同時預留日後用於[港口鐵路線的可能性](../Page/港口鐵路線.md "wikilink")（但此計劃已廢止）。

而在[九龍南綫及](../Page/九龍南綫.md "wikilink")[沙田至中環綫工程中](../Page/沙田至中環綫.md "wikilink")，本型機車主力用作牽引[SP1900列車往](../Page/SP1900.md "wikilink")[港鐵八鄉車廠進行改裝及擴編](../Page/港鐵八鄉車廠.md "wikilink")，以及牽引新抵港的[港鐵屯馬綫中國製列車及](../Page/港鐵屯馬綫中國製列車.md "wikilink")[港鐵東鐵綫現代列車](../Page/港鐵東鐵綫現代列車.md "wikilink")（由第八編組起）由進入車廠。而自2017年起，CKD0A型內燃機車亦同時負責此牽引工作。

另外，本型機車亦預期負責將退役後的[港鐵中期翻新列車送往八鄉車廠解體](../Page/港鐵中期翻新列車.md "wikilink")。

現時，本型機車是牽引[25T型客車往返何東樓車廠及紅磡站的主要使用機車](../Page/25T型客車.md "wikilink")，而其他舊式機車則主要用作工程車及東鐵線列車更換，較少執行此任務。

在緊急情況，本款機車亦可用作牽引港穗直通車進出香港（如2015年直通車故障，部分班次列車由ER20機車牽引下，由羅湖站到達[紅磡站](../Page/紅磡站.md "wikilink")）。

由於隔音技術來自德國柴電潛艇，本型機車在歐盟自[六四事件後生效的對](../Page/六四事件.md "wikilink")[中華人民共和國政府](../Page/中華人民共和國政府.md "wikilink")「軍事技術禁運」條款下，不能越過[羅湖橋進入](../Page/羅湖橋.md "wikilink")[中華人民共和國鐵路系統](../Page/中華人民共和國鐵路.md "wikilink")。

### 各租賃公司

部分統型機車則售予各間大型鐵路租賃公司，當中包括日本[三井物產轄下的](../Page/三井物產.md "wikilink")[Dispolok](../Page/Dispolok.md "wikilink")、Angel
Trains等，主要在歐洲使用。

## 改進型

在ER20 CF推出市場後，西門子公司亦打算研製改進版的同系機車，並命名為ER30型（分為俄羅斯軌距的ER30 CF型，以及標準軌距的ER30
CU型），但最後因ER20
CF型機車銷路不佳（僅立陶宛鐵路購置）而告吹。在2012年，當最後一批ER20型機車交付捷克及斯洛文尼亞後，Eurorunner柴油機車已悉數停產，並改為生產下一代的電力或柴油機車車系。

## 參見

  - [G12型柴油機車](../Page/G12型柴油機車.md "wikilink")
  - [G16型柴油機車](../Page/G16型柴油機車.md "wikilink")
  - [G26型柴油機車](../Page/G26型柴油機車.md "wikilink")
  - [CKD0A型內燃機車](../Page/CKD0A型內燃機車.md "wikilink")
  - [EuroSprinter](../Page/EuroSprinter.md "wikilink")，同一公司同時研發的電力機車車系
  - [神華號HXD1八軸/十二軸機車](../Page/和諧1型電力機車#改進.md "wikilink")，同一公司授權[中車株機生產之電力機車](../Page/中車株洲電力機車.md "wikilink")，採用與本型柴油機車相似頭型設計

## 參考資料

## 外部連結

  - [九鐵全新環保柴油機車運抵本港（九鐵新聞稿 13/9/2003）](http://www.kcrc.com/tc/investor/pr/2003/030913.html)
  - [產品介紹](https://web.archive.org/web/20160306023256/http://www.mobility.siemens.com/mobility/en/pub/references/details.cfm?z=1&do=app.detail&referenceID=1592&lID=1)

[Category:香港柴油機車](../Category/香港柴油機車.md "wikilink")
[Category:西门子制铁路机车](../Category/西门子制铁路机车.md "wikilink")
[Category:東鐵綫](../Category/東鐵綫.md "wikilink")
[Category:九廣鐵路](../Category/九廣鐵路.md "wikilink")
[Category:Bo-Bo軸式機車](../Category/Bo-Bo軸式機車.md "wikilink")
[Category:Co-Co軸式機車](../Category/Co-Co軸式機車.md "wikilink")
[Category:奧地利柴油機車](../Category/奧地利柴油機車.md "wikilink")
[Category:德國鐵路柴油機車](../Category/德國鐵路柴油機車.md "wikilink")

1.  [Siemens sells final Eurorunner
    locomotive](http://www.railjournal.com/index.php/locomotives/siemens-sells-final-eurorunner-locomotive.html)，Simmons-Boardman
    Publishing，2012-07-26
2.  [MRCE
    ER 20](http://www.mainlinediesels.net/index.php?nav=1000128&lang=en)，
    Mainline Diesels
3.  [Railfaneurope](http://www.railfaneurope.net/pix/ne/China/diesel/ER20/pix.html)，同項參考