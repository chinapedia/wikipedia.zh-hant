**粵曲演唱家列表**列出演唱[粵曲的藝術家](../Page/粵曲.md "wikilink")。他們只會在觀眾面前演唱，而沒有穿上戲服。

  - 四大平喉／平喉四傑：[小明星](../Page/小明星.md "wikilink")、[徐柳仙](../Page/徐柳仙.md "wikilink")、[張惠芳](../Page/張惠芳.md "wikilink")、[張月兒](../Page/張月兒.md "wikilink")

__NOTOC__ [A](../Page/#A.md "wikilink")
[B](../Page/#B.md "wikilink") [C](../Page/#C.md "wikilink")
[D](../Page/#D.md "wikilink") [E](../Page/#E.md "wikilink")
[F](../Page/#F.md "wikilink") [G](../Page/#G.md "wikilink")
[H](../Page/#H.md "wikilink") [I](../Page/#I.md "wikilink")
[J](../Page/#J.md "wikilink") [K](../Page/#K.md "wikilink")
[L](../Page/#L.md "wikilink") [M](../Page/#M.md "wikilink")
[N](../Page/#N.md "wikilink") [O](../Page/#O.md "wikilink")
[P](../Page/#P.md "wikilink") [Q](../Page/#Q.md "wikilink")
[R](../Page/#R.md "wikilink") [S](../Page/#S.md "wikilink")
[T](../Page/#T.md "wikilink") [U](../Page/#U.md "wikilink")
[V](../Page/#V.md "wikilink") [W](../Page/#W.md "wikilink")
[X](../Page/#X.md "wikilink") [Y](../Page/#Y.md "wikilink")
[Z](../Page/#Z.md "wikilink")

## A

  - [歐偉泉](../Page/歐偉泉.md "wikilink"), [區均祥](../Page/區均祥.md "wikilink")

## B

  - [白燕仔](../Page/白燕仔.md "wikilink")、[白燕飛](../Page/白燕飛.md "wikilink")（白燕仔外孫）、[碧雲](../Page/碧雲.md "wikilink")

## C

  - [崔妙芝](../Page/崔妙芝.md "wikilink")、[翠姬](../Page/翠姬.md "wikilink")
  - [陳鳳仙](../Page/陳鳳仙.md "wikilink"), [陳彩蘭](../Page/陳彩蘭.md "wikilink"),
    [陳錦紅](../Page/陳錦紅.md "wikilink")（[小明星嫡傳女弟子](../Page/小明星.md "wikilink")）、[陳笑風](../Page/陳笑風.md "wikilink")、[陳玲玉](../Page/陳玲玉.md "wikilink")、[陳鑑](../Page/陳鑑.md "wikilink")、[陳曼虹](../Page/陳曼虹.md "wikilink")、
  - [張惠芳](../Page/張惠芳.md "wikilink")、[張月兒](../Page/張月兒.md "wikilink")、[張琼仙](../Page/張琼仙.md "wikilink")（玉京仙子）、[張琴思](../Page/張琴思.md "wikilink")、[張小顰](../Page/張小顰.md "wikilink")
  - [鄭君綿](../Page/鄭君綿.md "wikilink")、[鄭幗寶](../Page/鄭幗寶.md "wikilink")（濠江之鶯）、[程德芬](../Page/程德芬.md "wikilink")
  - [鍾麗蓉](../Page/鍾麗蓉.md "wikilink")、[鍾雲山](../Page/鍾雲山.md "wikilink")、[鍾德](../Page/鍾德.md "wikilink"),
    [鍾志雄](../Page/鍾志雄.md "wikilink")

## D

  - [杜煥](../Page/杜煥.md "wikilink")、[董潔貞](../Page/董潔貞.md "wikilink")

## F

  - [馮玉玲](../Page/馮玉玲.md "wikilink")

## H

  - [熊飛影](../Page/熊飛影.md "wikilink")、[何麗芳](../Page/何麗芳.md "wikilink")、[何萍](../Page/何萍.md "wikilink")(星腔第三代傳人)、[何臣](../Page/何臣.md "wikilink")(南音)、[韓道鳴](../Page/韓道鳴.md "wikilink")

## J

  - [招大銀](../Page/招大銀.md "wikilink")

## K

  - [甘明超](../Page/甘明超.md "wikilink")、[金山女](../Page/金山女.md "wikilink")、[郭少文](../Page/郭少文.md "wikilink")、[瓊仙](../Page/瓊仙.md "wikilink"),
    [江平](../Page/江平.md "wikilink"), [江峰](../Page/江峰.md "wikilink")

## L

  - [李少芳](../Page/李少芳.md "wikilink")([小明星弟子](../Page/小明星.md "wikilink"))、[李月友](../Page/李月友.md "wikilink")(星腔第三代傳人)、[李寶瑩](../Page/李寶瑩.md "wikilink")、[李寶倫](../Page/李寶倫.md "wikilink"),
    [李若呆](../Page/李若呆.md "wikilink"),
    [李銳祖](../Page/李銳祖.md "wikilink")、[李慧](../Page/李慧.md "wikilink"),
    [李學優](../Page/李學優.md "wikilink")
  - [梁素琴](../Page/梁素琴.md "wikilink")、[梁之潔](../Page/梁之潔.md "wikilink")、[梁瑛](../Page/梁瑛.md "wikilink"),
    [梁漢威](../Page/梁漢威.md "wikilink")
  - [黎文所](../Page/黎文所.md "wikilink")
  - [賴天涯](../Page/賴天涯.md "wikilink")
  - [劉就](../Page/劉就.md "wikilink")

## M

  - [文千歲](../Page/文千歲.md "wikilink"), [梅欣](../Page/梅欣.md "wikilink"),
    [馬師鉅](../Page/馬師鉅.md "wikilink")

## N

  - [吳少庭](../Page/吳少庭.md "wikilink"), [伍木蘭](../Page/伍木蘭.md "wikilink")

## P

  - [白楊](../Page/白楊.md "wikilink"), [白艷紅](../Page/白艷紅.md "wikilink"),
    [白醒芬](../Page/白醒芬.md "wikilink"),
    [潘小桃](../Page/潘小桃.md "wikilink"),
    [潘珮璇](../Page/潘珮璇.md "wikilink")、[潘賢達](../Page/潘賢達.md "wikilink")、[佩珊](../Page/佩珊.md "wikilink")

## S

  - [小明星](../Page/小明星.md "wikilink")、[崔妙芝](../Page/崔妙芝.md "wikilink")、[冼劍麗](../Page/冼劍麗.md "wikilink")、[小甘羅](../Page/小甘羅.md "wikilink"),
    [小燕飛](../Page/小燕飛.md "wikilink")([小明星弟子](../Page/小明星.md "wikilink"))

## T

  - [徐柳仙](../Page/徐柳仙.md "wikilink")、[天涯](../Page/天涯.md "wikilink"),
    [譚佩儀](../Page/譚佩儀.md "wikilink")、[唐建垣](../Page/唐建垣.md "wikilink")

## Y

  - [源妙生](../Page/源妙生.md "wikilink")、[葉幼琪](../Page/葉幼琪.md "wikilink")、[邱鶴儔](../Page/邱鶴儔.md "wikilink")、[嚴淑芳](../Page/嚴淑芳.md "wikilink")

## W

  - [黃少梅](../Page/黃少梅.md "wikilink")(星腔第三代傳人)、[胡美儀](../Page/胡美儀.md "wikilink")

[\*](../Category/粵劇.md "wikilink")