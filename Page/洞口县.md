**洞口县**位于[中国](../Page/中华人民共和国.md "wikilink")[湖南省中西部](../Page/湖南省.md "wikilink")、是[邵阳市西部下辖的一个](../Page/邵阳市.md "wikilink")[县](../Page/县.md "wikilink")。为[湖南省第八大、中国第53大肉类生产大县市](../Page/中国肉类生产百强县列表.md "wikilink")。

## 地理

洞口县地处[雪峰山脉东麓](../Page/雪峰山脉.md "wikilink")，[资水上游](../Page/资江.md "wikilink")；地理上西北部为雪峰山脉，平均海拔500米以上，千米以上山峰百余座，普子垴海拔1821米，为辖境至高点。境内大小溪流130多条，分属资水和[沅水二大水系](../Page/沅水.md "wikilink")，主要河流有资水、蓼水河、平溪江、黄泥江和公溪河。

相邻[县级行政区有](../Page/县级行政区.md "wikilink")，北接[溆浦县](../Page/溆浦.md "wikilink")，东北与[隆回县为邻](../Page/隆回.md "wikilink")，东南和[武冈市接壤](../Page/武冈.md "wikilink")，西南与[绥宁县接邻](../Page/绥宁.md "wikilink")，西部和[会同县交接](../Page/会同.md "wikilink")，西北和[洪江市相连](../Page/洪江.md "wikilink")。

## 行政区划

1982年12月31日，洞口县下辖8区和1区级镇、31乡和7乡级镇，1996年经过[撤區併鄉区划调整](../Page/撤区并乡.md "wikilink")，撤并为22乡镇，近年经过多次调整后\[1\]\[2\]，现辖3街道、11镇、6乡、3民族乡及1类似乡级单位。

  - 3个[街道办事处](../Page/街道办事处.md "wikilink")：[文昌街道](../Page/文昌街道_\(洞口县\).md "wikilink")、雪峰街道和花古街道；
  - 11个[镇](../Page/镇.md "wikilink")：[山门镇](../Page/山门镇_\(洞口县\).md "wikilink")、[石江镇](../Page/石江镇.md "wikilink")、[江口镇](../Page/江口镇_\(洞口县\).md "wikilink")、[竹市镇](../Page/竹市镇_\(洞口县\).md "wikilink")、[高沙镇](../Page/高沙镇.md "wikilink")、[黄桥镇](../Page/黄桥镇_\(洞口县\).md "wikilink")、[毓兰镇](../Page/毓兰镇.md "wikilink")、[花园镇](../Page/花园镇_\(洞口县\).md "wikilink")、[醪田镇](../Page/醪田镇.md "wikilink")、[水东镇和](../Page/水东镇_\(洞口县\).md "wikilink")[岩山镇](../Page/岩山镇.md "wikilink")；
  - 6个[乡](../Page/乡.md "wikilink")：[月溪乡](../Page/月溪乡.md "wikilink")、[石柱乡](../Page/石柱乡_\(洞口县\).md "wikilink")、[杨林乡](../Page/杨林乡_\(洞口县\).md "wikilink")、[桐山乡](../Page/桐山乡.md "wikilink")、[渣坪乡和](../Page/渣坪乡.md "wikilink")[古楼乡](../Page/古楼乡_\(洞口县\).md "wikilink")；
  - 3个[民族乡](../Page/民族乡.md "wikilink")：[大屋瑶族乡](../Page/大屋瑶族乡.md "wikilink")、[长塘瑶族乡和](../Page/长塘瑶族乡.md "wikilink")[𦰡溪瑶族乡](../Page/𦰡溪瑶族乡.md "wikilink")；
  - 1个[类似乡级单位](../Page/类似乡级单位.md "wikilink")：茶铺茶场管理区。

## 历史

洞口县始建于1952年2月16日，以[武冈县](../Page/武冈.md "wikilink")（今武冈市）的第八、九、十、十一区全部和第三区部分地区为行政区域。因县城西南有洞口潭（洞口塘）得县名。

## 事件

2011年7月，洞口縣就縣內治安問題進行問卷調查，被發現問卷內選項全是正面的形容詞，事件被傳媒報導，被在網上熱爆\[3\]\[4\]。

## 名胜古迹

  - 迴龙洲，位于平溪江江心，形似巨龙回头。洲内古木参天，葱绿苍翠，洲青水绿，鸟语花香。
  - 洞口塘：出县城沿平溪江西行1公里，江水从西横断雪峰山向东流去，两岸险峰对峙，形成峡谷，江水穿洞而去，至此形成深潭。
  - 龙眼洞：位于雪峰山东麓，距县城3公里，属于喀斯特溶洞，约形成于6000万年前，恰似地下龙宫。
  - [罗溪](../Page/𦰡溪國家森林公園.md "wikilink")：位于洞口县西部的罗溪瑶族乡境内，平均海拨1200米，为国家级森林公园，素有“小西藏”之称，并誉为“神奇的绿洲”。
  - 文昌塔：位于迴龙洲侧平溪江北岸，始建于清朝咸丰十年（1860年），为湖南省重点文物保护单位。
  - 水东桥，清光绪三年建，跨于黄泥江；
  - 义学书院、义学碑，清道光二十四年（1844年）建；
  - 雪峰山：抗日阵亡将士墓，江口烈士塔等。

## 参考文献

[洞口县](../Category/洞口县.md "wikilink")
[邵阳](../Category/湖南省县份.md "wikilink")
[县](../Category/邵阳区县市.md "wikilink")

1.  [2014年中华人民共和国县以下行政区划变更情况（湘民行发【2014】1号）](http://www.mca.gov.cn/article/sj/tjbz/a/2014/201706020952.html)
2.  [2015年中华人民共和国县以下行政区划变更情况（湘民行发【2015】10号）](http://www.mca.gov.cn/article/sj/tjbz/a/2015/below/201602/20160200880232.htm)
3.
4.  <http://images.plurk.com/85f26e50f1221674915628508313d954.jpg>