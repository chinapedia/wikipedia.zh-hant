**雪松龍屬**（[屬名](../Page/學名.md "wikilink")：）是一[屬鼻子隆起的](../Page/屬.md "wikilink")[腕龍科](../Page/腕龍科.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生活於[下白堊紀](../Page/下白堊紀.md "wikilink")[巴列姆階的](../Page/巴列姆階.md "wikilink")[美國](../Page/美國.md "wikilink")[猶他州](../Page/猶他州.md "wikilink")。牠首先由Tidwell、Carpenter及Brooks於1999年所描述。牠的學名是取自發現其[化石的](../Page/化石.md "wikilink")。\[1\]

牠與在[英格蘭南部](../Page/英格蘭.md "wikilink")的[優腔龍及](../Page/優腔龍.md "wikilink")[莫里遜組的](../Page/莫里遜組.md "wikilink")[腕龍有一些相似的地方](../Page/腕龍.md "wikilink")。

## 參考資料

## 外部連結

  - [The
    Brachiosauridae](https://web.archive.org/web/20070816003158/http://www.users.qwest.net/~jstweet1/brachiosauridae.htm)
  - [雪松龍的骨頭化石](https://scientists.dmns.org/sites/kencarpenter/Holotypes/Holotypes.aspx?PageView=Shared)

[Category:腕龍科](../Category/腕龍科.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")

1.