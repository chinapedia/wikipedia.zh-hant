[Hong_Kong_Tsuen_Wan_MTR_Depot.JPG](https://zh.wikipedia.org/wiki/File:Hong_Kong_Tsuen_Wan_MTR_Depot.JPG "fig:Hong_Kong_Tsuen_Wan_MTR_Depot.JPG")
[Tsuen_Wan_Depot_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_Depot_\(Hong_Kong\).jpg "fig:Tsuen_Wan_Depot_(Hong_Kong).jpg")
[港鐵](../Page/港鐵.md "wikilink")**荃灣車廠**（）是[荃灣綫的車廠](../Page/荃灣綫.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣區](../Page/荃灣區.md "wikilink")[荃灣北部一帶](../Page/荃灣.md "wikilink")，於1982年5月10日啟用。車廠旁邊是[荃灣綫北面的終點站](../Page/荃灣綫.md "wikilink")──[荃灣站](../Page/荃灣站.md "wikilink")，車廠上蓋[物業為由](../Page/物業.md "wikilink")[港鐵公司發展的](../Page/香港鐵路有限公司.md "wikilink")[綠楊新邨](../Page/綠楊新邨.md "wikilink")[住宅項目](../Page/住宅.md "wikilink")。本車廠及鄰近的[荃灣站由香港著名建築師](../Page/荃灣站.md "wikilink")[何弢設計](../Page/何弢.md "wikilink")。

## 設施

荃灣車廠可以容納40多輛[港鐵](../Page/港鐵.md "wikilink")[列車](../Page/列車.md "wikilink")，為列車提供停車、測試、清洗、預防及糾正性維修設施，包括更換重型設備時用的起重及頂升設備。

本廠主要分為幾個部分：

### 停車坑

本廠北側設有18條停車軌道，停泊各荃灣線列車，而該停車坑與試車線及西側出入廠線連接。

### 維修車坑

本廠南面部分共設7條維修車坑，為上述列車提供日常維修，與西側出入廠線連接。

### 洗車機、磨輪機及調度控制塔

在南側出入線附近設機務段，設有洗車及磨輪機各一台，所有進廠列車必須駛過，而列車輪拋光工作亦在此進行。而調度控制塔，則位於機務段專用線與出入線之間。

### 機務段

機務段位於出入線側，用於停泊及檢修調車機車及路軌碾磨車，有專用線接入；部分檢修工場採有蓋設計。

### 車廠出入線與荃灣站越位軌道

越位軌道位於[荃灣站西面](../Page/荃灣站.md "wikilink")，與車廠出入線連接，是荃灣綫列車進出車廠的唯一路徑。同時供正常服務、抵達[荃灣站的列車進行調頭](../Page/荃灣站.md "wikilink")。另外，所有車廠內調度工作在出入廠線進行。

此越位軌道原先作為荃灣綫延伸至元朗的預留工程，因此其長度較一般越位軌道長（性質上類似[大圍車廠接駁](../Page/港鐵大圍車廠.md "wikilink")[顯徑站路軌](../Page/顯徑站.md "wikilink")），但最後因鐵路發展政策改變而保持原狀。

### 港鐵員工會所

荃灣車廠附設機務段側設有[港鐵員工康體中心](../Page/港鐵.md "wikilink")，供予[港鐵](../Page/港鐵.md "wikilink")[職員消閒及聯誼的用途](../Page/職員.md "wikilink")。

## 歷史

[荃灣站雖然是](../Page/荃灣站.md "wikilink")[荃灣綫的北面終點站](../Page/荃灣綫.md "wikilink")，卻使用了[側式月台設計](../Page/側式月台.md "wikilink")。這是由於根據原來設計，[荃灣站只是中途站](../Page/荃灣站.md "wikilink")，並且打算在今[荃灣站的西面](../Page/荃灣站.md "wikilink")──[荃景圍](../Page/荃景圍.md "wikilink")（近[愉景新城及](../Page/愉景新城.md "wikilink")[荃景花園一帶](../Page/荃景花園.md "wikilink")），興建一個[荃灣西站作為終點站](../Page/荃灣西站.md "wikilink")，以和新界西部鐵路（即現今的[西鐵綫](../Page/西鐵綫.md "wikilink")）接駁，但是該處因為收地（該處存在[古墓](../Page/古墓.md "wikilink")）及人口不足問題而一直無興建。

在建築荃灣車廠及[荃灣站期間](../Page/荃灣站.md "wikilink")，[荃錦公路需要改道](../Page/荃錦公路.md "wikilink")。而[三棟屋村亦需要搬遷](../Page/三棟屋村.md "wikilink")。在原來地址在工程完成後，三棟屋村則被更改興建為[三棟屋博物館](../Page/三棟屋博物館.md "wikilink")。

1998年至2001年，荃灣車廠曾經為所有[都城嘉慕電動列車進行全面性的翻新工作](../Page/港鐵都城嘉慕電動列車（直流電）.md "wikilink")。

2016-2018年，荃灣車廠曾經為部份都城嘉慕電動列車（直流電）進行新訊號系統安裝工程。

## 參見

  - [港鐵車廠](../Page/港鐵車廠.md "wikilink")

[Category:港鐵](../Category/港鐵.md "wikilink")
[Category:荃灣綫](../Category/荃灣綫.md "wikilink")
[Category:香港鐵路車廠](../Category/香港鐵路車廠.md "wikilink")
[Category:荃灣](../Category/荃灣.md "wikilink")