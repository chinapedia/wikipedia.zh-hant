**黃克孫**（英文名：，），生於[中國](../Page/中國.md "wikilink")[广西](../Page/广西.md "wikilink")[南宁](../Page/南宁.md "wikilink")，在[菲律賓](../Page/菲律賓.md "wikilink")[馬尼拉長大](../Page/馬尼拉.md "wikilink")。美籍华裔物理学家、翻译家，曾任[麻省理工學院物理學教授](../Page/麻省理工學院.md "wikilink")，出版[統計力學的著作](../Page/統計力學.md "wikilink")。他是美国物理学会会员和美国艺术与科学院院士。\[1\]

對華文讀者來說，黃克孫最有名的是把[波斯詩人](../Page/波斯.md "wikilink")[歐瑪爾·海亞姆的](../Page/歐瑪爾·海亞姆.md "wikilink")《[魯拜集](../Page/魯拜集.md "wikilink")》翻譯成中文。黃克孫在成為物理系研究生時，根據的英文譯本將《魯拜集》譯成七言詩。《魯拜集》於多年前絕版，後於1989年在[台灣再版](../Page/台灣.md "wikilink")。

他亦與妻子一起將《[易經](../Page/易經.md "wikilink")》翻譯成英文，該書於1984年出版。

## 所著書籍

  - 2010\. *Introduction to Statistical Physics*, 2nd ed. Taylor &
    Francis. ISBN 1420079026
  - 2007\. *Fundamental Forces of Nature: The Story of Gauge Fields*.
    World Scientific. Aimed at educated lay readers.
  - 2005\. *Lectures on Statistical Physics and Protein Folding*. World
    Scientific. ISBN 981-256-143-9
  - 2001\. *Introduction to Statistical Physics*. Taylor & Francis. ISBN
    0-7484-0941-6
  - 1998\. *Quantum Field Theory: From Operators to Path Integrals*.
    John Wiley & Sons. ISBN 0-471-14120-8
  - 1992\. *Quarks, Leptons and Gauge Fields*, 2nd ed. World Scientific.
    ISBN 981-02-0659-3
  - 1987\. *Statistical Mechanics*, 2nd ed. John Wiley & Sons. ISBN
    0-471-81518-7
  - 1984\. *I Ching, the Oracle*. World Scientific. ISBN 9971-966-25-5
  - 1963\. *Statistical Mechanics*, John Wiley & Sons.

## 參考資料

[Category:中國物理學家](../Category/中國物理學家.md "wikilink")
[Category:中國翻譯家](../Category/中國翻譯家.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:麻省理工学院教授](../Category/麻省理工学院教授.md "wikilink")
[Category:南宁人](../Category/南宁人.md "wikilink")
[K](../Category/黃姓.md "wikilink")

1.