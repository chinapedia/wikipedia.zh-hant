[Model_of_Communication_Depart,_Taiwan_Governor's_Office_20131118.jpg](https://zh.wikipedia.org/wiki/File:Model_of_Communication_Depart,_Taiwan_Governor's_Office_20131118.jpg "fig:Model_of_Communication_Depart,_Taiwan_Governor's_Office_20131118.jpg")
**台灣總督府交通局遞信部**是[台灣](../Page/台灣.md "wikilink")[日治時代時](../Page/台灣日治時期.md "wikilink")，[台灣總督府轄下負責全台](../Page/台灣總督府.md "wikilink")[郵政及](../Page/郵政.md "wikilink")[電信的部門](../Page/電信.md "wikilink")。

臺灣總督府交通局遞信部建築位於[台北市](../Page/台北市.md "wikilink")[中正區長沙街一段](../Page/中正區_\(臺北市\).md "wikilink")2號，興建於1925年，由[森山松之助設計](../Page/森山松之助.md "wikilink")，為[中華民國國家古蹟](../Page/台灣古蹟列表.md "wikilink")，在1956年至2006年11月之間為[中華民國交通部本部所在](../Page/中華民國交通部.md "wikilink")。2006年11月10日，交通部遷出，移交[國史館使用](../Page/國史館.md "wikilink")。2010年4月29日，國史館本部（即國史館臺北館區）自[臺北縣](../Page/新北市.md "wikilink")[新店市遷入](../Page/新店區.md "wikilink")。

1935年，臺灣總督府交通局遞信部高等官舍於台北市[幸町](../Page/幸町_\(台北市\).md "wikilink")149番地建立，即今[李國鼎故居](../Page/李國鼎故居.md "wikilink")。

[Category:台北市古蹟](../Category/台北市古蹟.md "wikilink")
[Category:國立臺灣博物館](../Category/國立臺灣博物館.md "wikilink")
[Category:1925年完工建築物](../Category/1925年完工建築物.md "wikilink")
[Category:1925年台灣建立](../Category/1925年台灣建立.md "wikilink")
[Category:臺灣總督府](../Category/臺灣總督府.md "wikilink")
[Category:台灣日治時期官署建築](../Category/台灣日治時期官署建築.md "wikilink")