**普魯士藍**（；；[化學名稱](../Page/化學.md "wikilink")：**亞鐵氰化鐵**；[分子式](../Page/分子式.md "wikilink")：Fe<sub>7</sub>(CN)<sub>18</sub>⋅14H<sub>2</sub>O，或書寫成
·
*x*[簡稱](../Page/簡稱.md "wikilink")：PB）是一種[深藍色的顏料](../Page/深藍色.md "wikilink")，在畫圖和青花[瓷器中應用](../Page/瓷器.md "wikilink")。普魯士藍是[狄斯巴赫](../Page/狄斯巴赫.md "wikilink")（Johann
Jacob
Diesbach）在意外中被發現，他原本是打算製造[紅色顏料的](../Page/紅色.md "wikilink")。**滕士蓝**（）与普鲁士蓝是同一种物质，只是由不同[试剂制取的](../Page/化学试剂.md "wikilink")\[1\]\[2\]\[3\]\[4\]。

[德国的前身](../Page/德国.md "wikilink")[普鲁士军队的](../Page/普鲁士.md "wikilink")[制服颜色就是使用该种颜色](../Page/制服.md "wikilink")，以至1871年[德意志第二帝国成立后相当长一段时间仍然沿用普鲁士蓝军服](../Page/德意志第二帝国.md "wikilink")，直至[第一次世界大战前夕方更换成](../Page/第一次世界大战.md "wikilink")。

普鲁士蓝，或柏林蓝、中國藍，或滕士蓝的扩展含义并非一种颜色，而是指氰化亚铁这种深蓝色染料。氰化亚铁染料本身在历史上已有多次出现，甚至可追溯至古埃及，直至据现有记载1706年由Johann
Jacob Diesbach于柏林人工合成，后经现代手段分析，并开发出工业合成手段，由BASF前身IG
Farben工业大批量生产。作为首次出现的工业合成染料，因氰化亚铁的稳定性，且不溶于水，其着色效果远强于以往的有机天然染料靛蓝，虽存在一定毒性，但被德意志第二帝国作为军服染料长时间使用，后北洋政府、国民政府也以其为正规军队的标准染料。再后在中国，因军阀纷争和日本入侵，中国军队的军服染料鱼龙混杂，从土黄到深蓝五花八门，而各路武装中，以国民政府“中央军”为优先供给单位，包括供应使用普鲁士蓝染料的军服统一着装，后又成为电视剧中的一个特色。

## 歷史

狄斯巴赫把[草木灰和牛](../Page/草木灰.md "wikilink")[血混在一起](../Page/血.md "wikilink")，製成[亞鐵氰化鉀](../Page/亞鐵氰化鉀.md "wikilink")，其三水合物俗称黄血盐，後與[三氯化鐵或其他能够提供](../Page/三氯化鐵.md "wikilink")[三价铁离子的物质反應後產生](../Page/三价铁离子.md "wikilink")。

3K<sub>4</sub>Fe(CN)<sub>6</sub> + 4FeCl<sub>3</sub> →
Fe<sub>4</sub>\[Fe(CN)<sub>6</sub>\]<sub>3</sub> + 12KCl

## 製造

普魯士藍是由[亞鐵氰化鉀和](../Page/亞鐵氰化鉀.md "wikilink")[三氯化鐵反應後產生](../Page/三氯化鐵.md "wikilink")。在反應過程中，能見度和顏色會立刻轉變。

## 物质信息

普鲁士蓝是经典的配合物。其配体为六个氰基，中心离子为二价铁离子。氰基与二价铁离子共同通过配位键组成六氰合铁（II）酸根（整体显-4价）作为普鲁士蓝的内配位层（内界）。而外层的[三价铁离子与钾离子作为普鲁士蓝的外配位层](../Page/三价铁离子.md "wikilink")（外界）通过离子键与六氰合铁（II）酸根以离子键的形式相连接。结构方面，普鲁士蓝为六面立（正）方结构。氰基作为立（正）方的各条棱连结处于顶点的铁离子，其中相同价态的铁离子在各面上均互为对角，而每间隔一个立（正）方，钾离子会被包裹在其中。

## 化学作用

在医疗上[铊可置换普鲁士蓝上的](../Page/铊.md "wikilink")[钾后形成不溶性物质随](../Page/钾.md "wikilink")[粪便排出](../Page/粪便.md "wikilink")，对治疗经口急慢性[铊中毒有一定疗效](../Page/铊.md "wikilink")。用量一般为每日250mg/kg，分4次，溶于50ml
15%甘露醇中口服。（适量补充[氯化钾](../Page/氯化钾.md "wikilink")，高钾能增加[肾对](../Page/肾.md "wikilink")[铊的清除能力](../Page/铊.md "wikilink")，可能与钾竞争性阻断肾小管对[铊的吸收有关](../Page/铊.md "wikilink")，同时[钾可动员细胞内的](../Page/钾.md "wikilink")[铊到](../Page/铊.md "wikilink")[细胞外](../Page/细胞.md "wikilink")，使血[铊含量增加](../Page/铊.md "wikilink")，可使临床病情加重，因此要慎用）
[琉璃色、紺色、普魯士藍.png](https://zh.wikipedia.org/wiki/File:琉璃色、紺色、普魯士藍.png "fig:琉璃色、紺色、普魯士藍.png")、[琉璃色的比較](../Page/琉璃色.md "wikilink")。\]\]

## 用途

  - 治療[鉈中毒](../Page/鉈中毒.md "wikilink")
  - [顏料](../Page/顏料.md "wikilink")
  - [工程師藍](../Page/工程師藍.md "wikilink")

## 参考文献

[Category:铁氰酸盐](../Category/铁氰酸盐.md "wikilink")
[B](../Category/颜料.md "wikilink") [P](../Category/色彩系.md "wikilink")
[P](../Category/靛青色系.md "wikilink") [P](../Category/藍色系.md "wikilink")
[B](../Category/颜色.md "wikilink")
[Category:铁(II,III)化合物](../Category/铁\(II,III\)化合物.md "wikilink")
[B](../Category/解毒劑.md "wikilink")

1.  Carson, Freida L. (1997). *Histotechnology: A Self-Instructional
    Text* (2nd ed.), pp. 209-211. Chicago: American Society of Clinical
    Pathologists. ISBN 0-89189-411-X.
2.  Tafesse, F. (2003). [Comparative studies on Prussian blue or
    diaquatetraamine-cobalt(III) promoted hydrolysis
    of 4-nitrophenylphosphate in
    microemulsions](http://www.mdpi.org/ijms/papers/i4060362.pdf).
    *International Journal of Molecular Sciences*, *4*(6): 362-370.
3.  Verdaguer, M., Galvez, N., Garde, R., & Desplanches, C. (2002).
    Electrons at work in Prussian blue analogues. *Electrochemical
    Society Interface*, *11*(3): 28-32.
4.