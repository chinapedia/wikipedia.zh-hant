在[数学中](../Page/数学.md "wikilink")，**布尔环***R*是对于所有*R*中的*x*有\(x^2 = x\)的环，就是说*R*由[幂等元素组成](../Page/幂等律.md "wikilink")。这些环引发自（和引发）[布尔代数](../Page/布尔代数.md "wikilink")。

## 例子

一个例子是任何集合*X*的[幂集](../Page/幂集.md "wikilink")，在这个环中：0是[空集](../Page/空集.md "wikilink")，1是[全集](../Page/全集.md "wikilink")，加法是[对称差](../Page/对称差.md "wikilink")，乘法是[交集](../Page/交集.md "wikilink")。另一个例子我们考虑*X*的所有[有限子集的集合](../Page/有限集合.md "wikilink")，运算还是对称差和交集。更一般的说通过这些运算任何[集合域都是布尔环](../Page/集合域.md "wikilink")。通过[Stone布尔代数表示定理所有布尔环都同构于一个集合域](../Page/Stone布尔代数表示定理.md "wikilink")（作为带有这些运算的环处理）。

## 与布尔代数的关系

如果定义

\[x \land y = xy\]

\[x \lor y = x + y + xy\]

\[\lnot x = 1 + x\]

则它们满足在布尔代数中交、并和补的所有公理。所以每个布尔环都成为了布尔代数。类似的，通过如下定义布尔代数成为了布尔环：

\[xy = x \land y\]

\[x + y =(x \lor y)\land \lnot(x \land y) =(x \land \lnot y) \lor(\lnot x \land y)\]

在两个布尔环之间的映射是[环同态](../Page/环同态.md "wikilink")，当且仅当它是相应的布尔代数的同态。进一步的，布尔环的子集是[环理想](../Page/环理想.md "wikilink")（素环理想，极大环理想），当且仅当它是相应的布尔代数的[理想](../Page/理想_\(数学\).md "wikilink")（素理想，极大理想）。布尔环模以环理想的[商环对应于相应的布尔代数模以相应的理想的商代数](../Page/商环.md "wikilink")。

## 性质

所有布尔环*R*满足对于所有*R*中的*x*有*x* + *x* = 0；因此 -*x* =
x，所有元素都是自身的[加法逆元](../Page/加法逆元.md "wikilink")，在布尔环中使用减号没有意义。因为我们知道

  -
    *x* + *x* =(*x* + *x*)<sup>2</sup> = *x*<sup>2</sup> +
    2*x*<sup>2</sup> + *x*<sup>2</sup> = *x* + 2*x* + *x* = *x* + *x* +
    *x* + *x*

并且因为\<*R*,+\>是[阿贝尔群](../Page/阿贝尔群.md "wikilink")，我们可以从这个等式的两端减去*x* +
*x*，这给出了*x* + *x* = 0。类似的证明证实了布尔环是[可交换的](../Page/交换律.md "wikilink")：

  -
    *x* + *y* =(*x* + *y*)<sup>2</sup> = *x*<sup>2</sup> + *xy* + *yx* +
    *y*<sup>2</sup> = *x* + *xy* + *yx* + *y*

而这产生了*xy* + *yx* = 0，它意味着*xy* = −*yx* = *yx*（使用上面第一个性质）。

*x* + *x* =
0的性质证实了布尔环是在带有两个元素的[域](../Page/域_\(數學\).md "wikilink")**F**<sub>2</sub>上的[结合代数](../Page/结合代数.md "wikilink")，但只在这个方向上。特别是，任何有限布尔环都有[二的幂的](../Page/二的幂.md "wikilink")[势](../Page/势.md "wikilink")。不是所有的在**F**<sub>2</sub>上的[单作结合代数都是布尔环](../Page/单作.md "wikilink")：比如[多项式环](../Page/多项式环.md "wikilink")**F**<sub>2</sub>\[*X*\]。

任何布尔环*R*模以任何环理想*I*的商环*R*/*I*也是布尔环。类似的，布尔环的任何[子环是布尔环](../Page/子环.md "wikilink")。

在布尔环*R*中所有素环理想*P*是极大环理想：
*R*/*P*的[商环是](../Page/商环.md "wikilink")[整环并其同时是布尔环](../Page/整环.md "wikilink")，所以它必定同构于域**F**<sub>2</sub>，这证实了*P*的极大性。因为极大环理想总是素环理想，我们得出素环理想和极大环理想在布尔环中是一致的。

## 引用

  -
[B](../Category/環論.md "wikilink") [B](../Category/布尔代数.md "wikilink")