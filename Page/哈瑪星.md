**哈瑪星**是位於[台灣](../Page/台灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[鼓山區的一個地名](../Page/鼓山區.md "wikilink")，泛指現今[五福四路與](../Page/五福四路.md "wikilink")[鐵路平交道的交會處以南](../Page/鐵路平交道.md "wikilink")，至鼓山漁港、漁市場，東至[高雄港車站一帶的](../Page/高雄港車站.md "wikilink")[臨港線鐵路](../Page/高雄臨港線.md "wikilink")（已停用），西至[哨船頭東側](../Page/哨船頭.md "wikilink")。曾是高雄的政經中心，臺南廳打狗支廳廳舍、高雄郡役所、高雄街役場、高雄市役所都曾設於此。

## 歷史

[Minato-machi,_Takao.jpg](https://zh.wikipedia.org/wiki/File:Minato-machi,_Takao.jpg "fig:Minato-machi,_Takao.jpg")

哈瑪星原本是海域，[日治時期時](../Page/台灣日治時期.md "wikilink")，[日本當局在高雄建立港口](../Page/日本.md "wikilink")，為了疏濬航道，於是利用淤泥[填海造陸而形成](../Page/填海.md "wikilink")。「哈瑪星」此名稱的由來，是因為當地有兩條濱海[鐵路通往商港](../Page/鐵路.md "wikilink")、漁港和漁市場，[日語稱為](../Page/日語.md "wikilink")「濱線」（，Hamasen），當地居民以[臺灣話稱之為](../Page/臺灣話.md "wikilink")「哈瑪星」（Há-má-seng）。

哈瑪星一帶在日治時期分屬[壽町](../Page/壽町_\(高雄市\).md "wikilink")、[新濱町](../Page/新濱町.md "wikilink")、[湊町等行政區](../Page/湊町.md "wikilink")，都是新生地。而從新濱町港邊至渡船頭邊的漁市場有一條專為轉運鮮魚的濱海鐵路，且因該地區的各種行業幾乎皆與港區及濱線具有密不可分的關係，因此後來哈瑪星即泛指今南鼓山地區。

哈瑪星設立的「打狗停車場」（今[高雄港車站](../Page/高雄港車站.md "wikilink")）為縱貫鐵道的終點。附近不但是高雄市政經中心，更是[高雄港現代化發展的起源地](../Page/高雄港.md "wikilink")，曾是當地最繁榮的地區。1917年完工的[臺南廳](../Page/臺南廳.md "wikilink")[打狗支廳廳舍即位於哈瑪星](../Page/打狗支廳.md "wikilink")；1920年廢廳改州，打狗支廳廳舍續用為高雄州[高雄郡郡役所](../Page/高雄郡.md "wikilink")，1924年廢高雄郡，郡役所改為[高雄警察署](../Page/高雄警察署.md "wikilink")。此外1920年高雄郡下轄[高雄街的街役場亦位於哈瑪星](../Page/高雄街.md "wikilink")，1924年高雄設市，街役場續用為市役所，直到1939年[新高雄市役所完工才遷移](../Page/高雄市立歷史博物館.md "wikilink")，足見哈瑪星曾是高雄的政經中心。但因[市中心東移而慢慢沒落](../Page/市中心.md "wikilink")；近年來則朝著觀光等方向發展，並因捷運的興建而形成新的商圈。

## 景點

[TRA_KaoHsiung_Port_Station_Gate.JPG](https://zh.wikipedia.org/wiki/File:TRA_KaoHsiung_Port_Station_Gate.JPG "fig:TRA_KaoHsiung_Port_Station_Gate.JPG")
[鼓山輪渡站.JPG](https://zh.wikipedia.org/wiki/File:鼓山輪渡站.JPG "fig:鼓山輪渡站.JPG")。\]\]

  - [打狗英國領事館](../Page/打狗英國領事館.md "wikilink")
  - [開台福德宮](../Page/開台福德宮.md "wikilink")
  - [雄鎮北門](../Page/雄鎮北門.md "wikilink")
  - [打狗驛](../Page/打狗驛.md "wikilink")（[高雄港車站](../Page/高雄港車站.md "wikilink")）
  - 捷運[西子灣站](../Page/西子灣站.md "wikilink")
  - [哈瑪星龍鳳宮](../Page/哈瑪星龍鳳宮.md "wikilink")（紅姑娘廟）
  - [文龍宮](../Page/文龍宮.md "wikilink")
  - [西子灣](../Page/西子灣.md "wikilink")
  - [哈瑪星站](../Page/哈瑪星站.md "wikilink")
  - [舊打狗驛故事館](../Page/舊打狗驛故事館.md "wikilink")
  - [高雄市武德殿](../Page/高雄市武德殿.md "wikilink")
  - [高雄港漁人碼頭](../Page/高雄港漁人碼頭.md "wikilink")
  - [原愛國婦人會館](../Page/原愛國婦人會館.md "wikilink")（今紅十字育幼中心）
  - [鼓山輪渡站](../Page/鼓山輪渡站.md "wikilink")
  - [新濱碼頭](../Page/新濱碼頭.md "wikilink")
  - [打狗郵便局](../Page/打狗郵便局.md "wikilink")（今鼓山郵局）
  - [高雄代天宮](../Page/高雄代天宮.md "wikilink")（舊[高雄市役所](../Page/高雄市役所.md "wikilink")）
  - [山形屋書店](../Page/山形屋書店.md "wikilink")（今壹貳樓古蹟餐廳）
  - [高雄警察署](../Page/高雄警察署.md "wikilink")（今永光行）
  - [湊町市場](../Page/湊町市場.md "wikilink")（今鼓山第一公有市場）
  - [高雄市忠烈祠](../Page/高雄市忠烈祠.md "wikilink")
  - [國立中山大學](../Page/國立中山大學.md "wikilink")
  - [打狗尋常高等小學校](../Page/打狗尋常高等小學校.md "wikilink")（今鼓山國小）
  - [駁二藝術特區](../Page/駁二藝術特區.md "wikilink")
  - [靈興殿](../Page/靈興殿.md "wikilink")
  - [繽紛海濱鬥牛場](../Page/繽紛海濱鬥牛場.md "wikilink")

## 地方組織

  - [打狗文史再興會社](../Page/打狗文史再興會社.md "wikilink")
  - [哈瑪星願景聯盟](../Page/哈瑪星願景聯盟.md "wikilink")
  - [哈瑪星文化協會](../Page/哈瑪星文化協會.md "wikilink")

## 事件年表

  - 1912年，歷時四年、共分六期、面積達60,570坪的湊町埋立工程宣告完成\[1\]，今天的哈瑪星也正式誕生。
  - 1923年4月21日，下午一點二十八分，時任皇太子的[昭和天皇裕仁](../Page/昭和天皇.md "wikilink")，從臺南抵達高雄驛，並前往[高雄州廳](../Page/高雄州廳.md "wikilink")、[高雄第一尋常高等小學校](../Page/高雄第一尋常高等小學校.md "wikilink")。
  - 2012年4月8日，哈瑪星殘存的日本時代建築群，廣三用地，因為地權所有為高雄市政府，公告三月將進行土地查估，打算拆除老屋，改建成停車廠或公園，使得在地文化團體以及居住在老屋的居民，走上街頭表達強烈抗議\[2\]。
  - 2015年4月29日，哈瑪星居民與中山大學師生，在西子灣隧道口抗議大量遊覽車爆量湧入，影響居住品質，呼籲市府立即總量管制大型巴士進入\[3\]。
  - 2015年8月23日，因臺灣鐵路打算將有著百年歷史的[高雄港站綠地招標出售](../Page/高雄港站.md "wikilink")，由[哈瑪星願景聯盟](../Page/哈瑪星願景聯盟.md "wikilink")，號召市民到鐵道園區放風箏，拒絕車站分區招商租售\[4\]

## 相關著作

  - 打狗文史再興會社《[濱線追憶](../Page/濱線追憶.md "wikilink")》\[5\]
  - [王聰威](../Page/王聰威.md "wikilink")《[濱線女兒](../Page/濱線女兒.md "wikilink")》
  - 米果《[朝顏時光](../Page/朝顏時光.md "wikilink")》
  - 《[藍鯨](../Page/藍鯨_\(雜誌\).md "wikilink")》創刊號特輯「哈瑪星」\[6\]
  - 楊玉姿、張守真《哈瑪星的文化故事》
  - 高雄市中正文化中心管理處《哈瑪星海洋的鏡像導覽手冊》
  - 劉文放《高雄市旗鼓地區之文學地景書寫研究》
  - 行政法人高雄市立歷史博物館《哈瑪星物語》

## 當地出身的名人

  - [林易增](../Page/林易增.md "wikilink")：前[中華職業棒球聯盟](../Page/中華職業棒球聯盟.md "wikilink")[兄弟象隊球員及總教練](../Page/兄弟象隊.md "wikilink")，綽號為盜帥。

## 參考資料

## 外部連結

  - [哈瑪星社區網站](http://hamasen.tacocity.com.tw/index.html)
  - [哈瑪星南方不墜的星星](https://web.archive.org/web/20080216194911/http://www.kusps.kh.edu.tw/fr-hamasen/index.htm)
  - [哈瑪星文化協會部落格](http://hamasen.pixnet.net/blog)
  - [不滅的哈瑪星](https://web.archive.org/web/20070224031439/http://www.easytravel.com.tw/action/hamaks/)

[哈瑪星](../Category/哈瑪星.md "wikilink")
[H](../Category/高雄市商圈.md "wikilink")
[Category:高雄市地名](../Category/高雄市地名.md "wikilink")
[Category:鼓山區](../Category/鼓山區.md "wikilink")
[Category:源自日本統治的台灣地名](../Category/源自日本統治的台灣地名.md "wikilink")

1.
2.
3.
4.
5.
6.