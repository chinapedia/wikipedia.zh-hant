**OTS
44**是一顆在[蝘蜓座內](../Page/蝘蜓座.md "wikilink")，距離大約550光年遠的[棕矮星](../Page/棕矮星.md "wikilink")。在[蝘蜓座110913-773444被發現之前](../Page/蝘蜓座110913-773444.md "wikilink")，它是已知的棕矮星中最小的。

OTS 44
的質量大約是[木星質量的](../Page/木星.md "wikilink")15倍，或是[太陽質量的](../Page/太陽.md "wikilink")1.5%。它的半徑大約是太陽的20%。

有證據顯示OTS 44被由冰和石頭組成的環圍繞著，而這個環可能發展成[行星系統](../Page/行星系統.md "wikilink")。

## 相關條目

  - [SCR 1845-6357](../Page/SCR_1845-6357.md "wikilink")

## 外部連結

  - [**SST**: Astronomers Discover Beginnings of 'Mini' Solar
    System](https://web.archive.org/web/20081005083741/http://www.spitzer.caltech.edu/Media/releases/ssc2005-06/release.shtml)



[Category:棕矮星](../Category/棕矮星.md "wikilink")
[Category:蝘蜓座](../Category/蝘蜓座.md "wikilink")