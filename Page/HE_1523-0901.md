[He1523a.jpg](https://zh.wikipedia.org/wiki/File:He1523a.jpg "fig:He1523a.jpg")
 **HE
1523-0901**是一颗位于[天秤座的](../Page/天秤座.md "wikilink")[红巨星](../Page/红巨星.md "wikilink")，它在[银河系内](../Page/银河系.md "wikilink")，距离地球大约7500[光年](../Page/光年.md "wikilink")。它被认为是一颗[第二星族星](../Page/金属量#第二星族星.md "wikilink")（或贫金属星）为目前发现年龄最老的[恒星](../Page/恒星.md "wikilink")（\[Fe/H\]
=
−2.95）。它的質量為太陽的0.8倍。通过[欧洲南方天文台的](../Page/欧洲南方天文台.md "wikilink")[甚大望远镜测量出为](../Page/甚大望远镜.md "wikilink")132亿年，而[宇宙年龄大约为](../Page/宇宙.md "wikilink")137亿年。

這顆恆星也是人們首次透過量度元素[鈾](../Page/鈾.md "wikilink")、[釷等衰變](../Page/釷.md "wikilink")，而推算其年齡。

## 參考資料

  -
  -
## 外部链接

  - [银河系发现132亿岁最古老恒星
    几乎与宇宙同龄](http://tech.sina.com.cn/d/2007-05-14/07091504405.shtml)

[Category:紅巨星](../Category/紅巨星.md "wikilink")
[Category:第二星族星](../Category/第二星族星.md "wikilink")
[Category:天秤座](../Category/天秤座.md "wikilink")
[Category:未分類恆星](../Category/未分類恆星.md "wikilink")