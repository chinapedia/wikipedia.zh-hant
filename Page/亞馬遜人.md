[right](../Page/圖像:Amazon_preparing_for_the_battle_\(Queen_Antiope_or_Armed_Venus\)_-_Pierre-Eugene-Emile_Hebert_1860_-_NG_of_Arts_Wash_DC.jpg.md "wikilink")[華盛頓特區](../Page/華盛頓特區.md "wikilink")[國家藝廊展出的一尊正在準備打仗的亞馬遜人雕像](../Page/國家藝廊.md "wikilink")\]\]
**亞馬遜人**（，）\[1\]（中國大陸譯作**阿玛-{}-宗人**；臺灣和香港譯作-{**亞馬遜人**}-，但中國民間流行**亚马-{}-逊人**这一译名；另外也有中文資料翻譯成**亞馬遜女傑族**），是[古希臘神話中一個由全部皆為女戰士構成的民族](../Page/古希臘神話.md "wikilink")，亞馬遜人占据着[小亞細亞](../Page/小亞細亞.md "wikilink")、[佛里及亞](../Page/弗里吉亚.md "wikilink")、[色雷斯和](../Page/色雷斯.md "wikilink")[敘利亞的許多地方](../Page/敘利亞.md "wikilink")。[希臘](../Page/希臘.md "wikilink")[歷史學家](../Page/歷史學家.md "wikilink")[希羅多德認為這一民族來自位於](../Page/希羅多德.md "wikilink")[薩爾馬提亞](../Page/薩爾馬提亞.md "wikilink")[斯基泰一帶的地區](../Page/斯基泰.md "wikilink")。根據一些考古遺跡觀察所得，古薩爾馬提亞女性有可能曾經參與過戰爭，所以有一小部分的學者認為亞馬遜女人是真的在歷史上存在。

## 古希腊记载中的阿玛宗人

[0_Amazzone_ferita_-_Musei_Capitolini_(1).JPG](https://zh.wikipedia.org/wiki/File:0_Amazzone_ferita_-_Musei_Capitolini_\(1\).JPG "fig:0_Amazzone_ferita_-_Musei_Capitolini_(1).JPG")

根据古希腊传说，阿玛宗人是[阿瑞斯和](../Page/阿瑞斯.md "wikilink")[哈耳摩尼亚的后代](../Page/哈耳摩尼亚.md "wikilink")\[2\]，居住在[本都](../Page/本都.md "wikilink")[欧克辛斯海](../Page/欧克辛斯海.md "wikilink")（Euxine
Sea，即[黑海](../Page/黑海.md "wikilink")）南岸的一部分。在那里，他们成立了一个由[希波吕忒](../Page/希波吕忒.md "wikilink")（一译“伊波利特”\[3\]）女王统领下的独立王国。阿玛宗人建立了许多城镇，其中有[士麦那](../Page/士麦那.md "wikilink")、[以弗所](../Page/以弗所.md "wikilink")，[锡诺普和](../Page/锡诺普.md "wikilink")[帕福斯](../Page/帕福斯.md "wikilink")。据剧作家[埃斯库罗斯讲](../Page/埃斯库罗斯.md "wikilink")，在遥远的过去，她们一直居住在[西徐亚](../Page/斯基提亚.md "wikilink")（今[克里米亚](../Page/克里米亚.md "wikilink")，即“[亚速海](../Page/亚速海.md "wikilink")”）\[4\]；而[普鲁塔克说](../Page/普鲁塔克.md "wikilink")，阿玛宗人住在塔奈斯河（今[顿河](../Page/顿河.md "wikilink")）附近，但这是斯基泰人所称的“阿玛宗人”。阿玛宗人后来迁到了忒耳摩冬河（今土耳其北部的）边的（今[泰尔梅](../Page/泰尔梅.md "wikilink")）。[希罗多德依据西徐亚人对她们](../Page/希罗多德.md "wikilink")“欧约尔帕塔”（Oiorpata）称呼的含意，称她们为“Androktones”（男人杀手）。

在《[奧德賽](../Page/奧德賽.md "wikilink")》中，阿玛宗人不允许男人住在她们的领地中或与女性接触；但为避免绝种，她们会定期同邻族[加加尔人男人结合以繁衍后代](../Page/加加尔人.md "wikilink")。生了男孩则送归男方（一说杀死或弄成残废），生了女孩就留下抚养，训练她们打仗。在另外的版本中，当阿玛宗人打仗时，不会杀死所有的男人。她们会留下一些作为性奴隶，一年里与他们有一到两次的结合。

阿玛宗人同其他部落通婚也被用来解释其人口的不绝。例如，阿玛宗人与斯基泰人组家的故事。\[5\]

阿玛宗人以弓箭、战斧、轻盾为武器。她们自己制造战盔、战袍\[6\]。她们崇拜战神[阿瑞斯和狩猎女神](../Page/阿瑞斯.md "wikilink")[阿尔忒弥斯](../Page/阿尔忒弥斯.md "wikilink")，在战斗中求生活。

传说“阿玛宗”一词的意义为“烙去少女的右乳”，以利于使用弓箭。但是这种词源学上的考证是靠不住的。在古代的造型艺术中，阿玛宗人的形象是肌肉丰满的美妇，并没有伤残的反映。

## 主要故事

关于阿玛宗人的神话主要有这样几组：

1.  [赫剌克勒斯夺取阿玛宗人女王](../Page/海克力士.md "wikilink")[希波吕忒的魔带的故事](../Page/希波吕忒.md "wikilink")\[7\]。
2.  [忒修斯抢夺阿玛宗人女王](../Page/忒修斯.md "wikilink")的故事\[8\]。
3.  阿玛宗人女王[彭忒西勒亚率兵援助](../Page/彭忒西勒亚.md "wikilink")[特洛亚](../Page/特洛亚.md "wikilink")，最后死于[阿喀琉斯之手的故事](../Page/阿喀琉斯.md "wikilink")\[9\]。
4.  柏勒罗丰征讨阿玛宗人的故事\[10\]。

## 后世影响

传说阿玛宗人修建了[以弗所城](../Page/以弗所.md "wikilink")\[11\]\[12\]并在那里修建了著名的[阿尔忒弥斯神庙](../Page/阿尔忒弥斯.md "wikilink")\[13\]。

关于阿玛宗人的传说形成于[希腊的](../Page/希腊.md "wikilink")[史前时期](../Page/史前时期.md "wikilink")，是[母系社会的反映](../Page/母系社会.md "wikilink")。这一传说遍布世界各地，一直到[中世纪还很流行](../Page/中世纪.md "wikilink")。尤其是在地理大发现时期，人们在美洲寻找阿玛宗人的踪迹。阿玛宗河（即[亚马逊河](../Page/亚马逊河.md "wikilink")）的名字就源自阿玛宗人。

围绕着阿玛宗人的神话故事产生了许多造型艺术作品。[帕特农神庙西间壁上的阿玛宗人同希腊英雄战斗的](../Page/帕特农神庙.md "wikilink")[浮雕产生于公元七世纪](../Page/浮雕.md "wikilink")；在[德尔斐的](../Page/德尔斐.md "wikilink")间壁上的阿玛宗人的浮雕创作于公元五世纪初。[巴赛的阿波罗·伊壁鸠鲁神庙](../Page/巴赛的阿波罗·伊壁鸠鲁神庙.md "wikilink")创作于公元前五世纪；的[阿尔忒弥斯神庙的浮雕产生于公元前五至公元前四世纪](../Page/阿尔忒弥斯.md "wikilink")。[埃皮達魯斯的](../Page/埃皮達魯斯.md "wikilink")[阿斯克勒皮俄斯圣殿西山墙三角楣饰上的浮雕和](../Page/阿斯克勒皮俄斯.md "wikilink")[哈利卡尔纳索斯陵墓上西缘饰上的浮雕都创作于公元前世纪](../Page/哈利卡尔纳索斯.md "wikilink")。在古棺和瓶绘上也都画有阿玛宗人的故事。在古代的造型艺术中，阿玛宗人像占有特殊的地位。

据[老普林尼的记述](../Page/老普林尼.md "wikilink")，为了创作[以弗所的](../Page/以弗所.md "wikilink")[阿耳忒弥斯神庙上的阿玛宗人的塑像](../Page/阿耳忒弥斯.md "wikilink")，特组成了一个雕塑小组，包括[波留克列特斯](../Page/波留克列特斯.md "wikilink")、[菲狄阿斯](../Page/菲狄阿斯.md "wikilink")、[克勒西拉斯和](../Page/克勒西拉斯.md "wikilink")\[14\]。流传下来的阿玛宗人塑像的[罗马复制品](../Page/罗马.md "wikilink")，其希腊原品的作者都是上面谈到的前三人。波留克列特斯的《》非常有名。

在[巴罗克派的绘画艺术中](../Page/巴罗克.md "wikilink")，[鲁本斯以阿玛宗人同希腊人作战为题材的绘画极负盛名](../Page/鲁本斯.md "wikilink")。阿玛宗人神话也吸引着16-17世纪的欧洲诗人和戏剧家。法国剧作家[罗贝尔·加尼耶写了](../Page/罗贝尔·加尼耶.md "wikilink")《》；西班牙剧作家写了《阿玛宗人》，另一位西班牙剧作家写了《[阿玛宗人在印度](../Page/:s:es:Amazonas_en_las_Indias.md "wikilink")》。19世纪的[浪漫主义者也借用这一神话题材](../Page/浪漫主义.md "wikilink")，德国戏剧作家[克莱斯特写了](../Page/海因里希·冯·克莱斯特.md "wikilink")《》；奥地利剧作家[格里帕泽写了](../Page/弗朗茨·格里帕泽.md "wikilink")《金羊毛》，其中[美狄亞設定為阿瑪宗人](../Page/美狄亞.md "wikilink")；意大利诗人[塔索的](../Page/托爾夸托·塔索.md "wikilink")《》虽然写的不是阿玛宗人的故事，但也从中吸收了个别的情节；意大利画家[丁托列托](../Page/丁托列托.md "wikilink")、[老帕尔马](../Page/老帕尔马.md "wikilink")、[雷尼等人的某些绘画也都取材于阿玛宗人的故事](../Page/圭多·雷尼.md "wikilink")。

## 相關族群

  - [加加爾人](../Page/加加爾人.md "wikilink")

## 参考资料

## 外部链接

  - [Amazon](http://www.infoplease.com/encyclopedia/entertainment/amazon-greek-mythology.html)

[分類:希腊神话](../Page/分類:希腊神话.md "wikilink")
[\*](../Page/分類:阿玛宗人.md "wikilink")
[分類:傳說中的種族](../Page/分類:傳說中的種族.md "wikilink")
[分類:單性別世界](../Page/分類:單性別世界.md "wikilink")

1.
2.  阿波洛尼斯(罗得岛的)Ⅱ
3.  意为“放纵、脱缰的母马”
4.  埃斯库罗斯：《[普罗米修斯](../Page/普罗米修斯.md "wikilink")》
5.  希罗多德史4.110.1-117.1
6.  斯特剌玻Ⅵ
7.  欧里庇得斯：《赫剌克勒斯的子女》
8.  普卢塔克：《忒修斯》
9.  狄俄多罗斯 Ⅱ
10. 波洛多罗斯 Ⅱ，《[伊利亚特](../Page/伊利亚特.md "wikilink")》Ⅵ
11. \[<http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Strabo/11E>\*.html
    STRABO, GEOGRAPHY, Book XI, Chapter 5\]
12. [THE AMAZONS IN GREEK
    LEGEND](http://www.sacred-texts.com/wmn/rca/rca02.htm)
13. *[Etymologicum Magnum](../Page/Etymologicum_Magnum.md "wikilink")*
    402. 8, under *Ephesos*
14. 老普林尼：《[自然史](../Page/博物志_\(老普林尼\).md "wikilink")》