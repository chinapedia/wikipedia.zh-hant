**阿尼尔·加西亚·奥蒂斯**（Anier García
Ortiz，）是[古巴](../Page/古巴.md "wikilink")[田径运动员](../Page/田径.md "wikilink")，[2000年夏季奥林匹克运动会男子](../Page/2000年夏季奥林匹克运动会.md "wikilink")[110米栏金牌得主](../Page/110米栏.md "wikilink")。

加西亚出生于[圣地亚哥](../Page/圣地亚哥_\(古巴\).md "wikilink")，1995年在泛美青年田径运动会上夺得冠军而成名，但在次年的[奥运会上未能进入决赛](../Page/1996年夏季奥林匹克运动会.md "wikilink")。1999年，他在[世界田径锦标赛上被](../Page/世界田径锦标赛.md "wikilink")[科林·杰克逊击败](../Page/科林·杰克逊.md "wikilink")，屈居亚军。

2000年悉尼奥运会上，加西亚击败了所有对手，取得冠军。[2004年夏季奥林匹克运动会上](../Page/2004年夏季奥林匹克运动会.md "wikilink")，加西亚获得第三名。他的个人最好成绩为13.00秒。

## 參考資料

## 外部連結

  -
[Category:古巴跨栏运动员](../Category/古巴跨栏运动员.md "wikilink")
[Category:古巴奧林匹克運動會金牌得主](../Category/古巴奧林匹克運動會金牌得主.md "wikilink")
[Category:古巴奧林匹克運動會銅牌得主](../Category/古巴奧林匹克運動會銅牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會田徑運動員](../Category/1996年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會田徑運動員](../Category/2000年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會田徑運動員](../Category/2004年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會獎牌得主](../Category/2004年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:奥林匹克运动会田径铜牌得主](../Category/奥林匹克运动会田径铜牌得主.md "wikilink")
[Category:世界田徑錦標賽獎牌得主](../Category/世界田徑錦標賽獎牌得主.md "wikilink")