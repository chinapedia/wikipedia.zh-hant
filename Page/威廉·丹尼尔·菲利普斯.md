**威廉·丹尼尔·菲利普斯**（William Daniel
Phillips，）。生于[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")[威尔克斯-巴里](../Page/威尔克斯-巴里_\(宾夕法尼亚州\).md "wikilink")），[美国物理学家](../Page/美国.md "wikilink")，1997年获[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

## 生平

菲利普斯是威廉·科尼利厄斯·菲利普斯和玛丽·凯瑟琳·萨维诺的儿子。1959年他的父母移居到坎普山（靠近宾夕法尼亚州哈里斯堡），在那儿他上了高中，毕业时曾做过班里的学生代表。1970年他以最优等生在[朱尼亚塔学院毕业](../Page/朱尼亚塔学院.md "wikilink")。之后，他获得了美国[麻省理工学院物理学博士学位](../Page/麻省理工学院.md "wikilink")。

1996年，他获得了富兰克林研究所A.迈克尔逊奖章。\[1\]

菲利普斯的博士论文是关于水中质子的磁矩。这对他后来的研究颇为重要。后来他在玻色 -
爱因斯坦凝聚态方面做了一些工作。1997年，他因对激光冷却的研究，尤其是发明[塞曼减速器而赢得了](../Page/塞曼减速器.md "wikilink")[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")（与克洛德·科昂-唐努德日和[朱棣文一起](../Page/朱棣文.md "wikilink")）。该技术能够减缓气体原子的运动以便更好的研究它们，是在[國家標準技術研究所研究出来的](../Page/國家標準技術研究所.md "wikilink")。

菲利普斯还是[馬里蘭大學學院市分校物理学教授](../Page/馬里蘭大學學院市分校.md "wikilink")。

他还曾与另外34位诺贝尔奖得主签署一封信，敦促奥巴马总统提供一个150亿美元的稳定预算，支持[清洁能源的研究](../Page/清洁能源.md "wikilink")、技术和示范。\[2\]他还是参与了科学与宗教对话的信仰卫理宗的科学家之一。

## 参考资料

  - [诺贝尔官方网站威廉·丹尼尔·菲利普斯自传](http://nobelprize.org/nobel_prizes/physics/laureates/1997/phillips-autobio.html)

[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:美國循道宗教徒](../Category/美國循道宗教徒.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:美國科學家](../Category/美國科學家.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:馬里蘭大學教師](../Category/馬里蘭大學教師.md "wikilink")
[Category:宾夕法尼亚州哈里斯堡人物](../Category/宾夕法尼亚州哈里斯堡人物.md "wikilink")
[Category:光学学会会士](../Category/光学学会会士.md "wikilink")

1.
2.  [Open Letter to President
    Obama](http://www.fas.org/press/_docs/Nobelist%20Letter%20to%20White%20House%20-%2007162009.pdf)