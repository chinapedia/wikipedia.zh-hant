**乌代德空军基地**（[英语](../Page/英语.md "wikilink")：**Al Udeid Air
Base**），位于[卡塔尔](../Page/卡塔尔.md "wikilink")[首都](../Page/首都.md "wikilink")[多哈西南](../Page/多哈.md "wikilink")35[公里](../Page/公里.md "wikilink")，是[美国驻海外最大军事基地之一](../Page/美国.md "wikilink")。

2003年，美军联合空战中心由[沙特阿拉伯的](../Page/沙特阿拉伯.md "wikilink")[苏丹王子空军基地迁至此处](../Page/苏丹王子空军基地.md "wikilink")。

近年来，乌代德空军基地不断扩建，有消息称，随着未来从[伊拉克和](../Page/伊拉克.md "wikilink")[阿富汗的撤兵](../Page/阿富汗.md "wikilink")，美军有意将这里建成[中东地区永久军事基地](../Page/中东地区.md "wikilink")。

## 参考资料

  - [外军观察：美国着手扩建海湾永久性空军基地](http://jczs.sina.com.cn/2006-05-15/0844370302.html)

## 外部链接

  - [Al Udeid Air Base,
    Qatar](http://www.globalsecurity.org/military/facility/udeid.htm)

[Category:卡塔尔军事](../Category/卡塔尔军事.md "wikilink")
[卡](../Category/美國空軍海外基地.md "wikilink")
[Category:卡塔爾機場](../Category/卡塔爾機場.md "wikilink")
[Category:卡塔爾外交](../Category/卡塔爾外交.md "wikilink")
[Category:美国建筑之最](../Category/美国建筑之最.md "wikilink")