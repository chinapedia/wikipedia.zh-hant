[缩略图](https://zh.wikipedia.org/wiki/File:Council_of_Constantinople_381-stavropoleos_church.jpg "fig:缩略图")

**第一次君士坦丁堡公會議**（，），是於公元381年五月至六月在[君士坦丁堡由罗马帝国皇帝](../Page/君士坦丁堡.md "wikilink")[狄奥多西一世舉行的第二次](../Page/狄奧多西一世.md "wikilink")[基督教](../Page/基督教.md "wikilink")[大公會議](../Page/大公會議.md "wikilink")。\[1\]\[2\]召开场所为君士坦丁堡[神圣和平教堂](../Page/神圣和平教堂.md "wikilink")。作为第二次基督教大公会议，可以将其看作是通过大会方式取得除[西方教会以外教内共识的一次尝试](../Page/拉丁禮教會.md "wikilink")。\[3\]此會議繼續討論[聖父](../Page/聖父.md "wikilink")（即天父）及[聖子](../Page/聖子.md "wikilink")（即[耶穌](../Page/耶穌.md "wikilink")）本質的教義。結果，此大公會議修訂了《[尼西亞信經](../Page/尼西亞信經.md "wikilink")》的內容，並獲得一致通過。進一步奠下[三位一體的](../Page/三位一體.md "wikilink")[神学教義](../Page/神学.md "wikilink")。会议的结果则由451年的[迦克墩公会议重申并确认](../Page/迦克墩公會議.md "wikilink")。

## 历史

由於在[羅馬](../Page/羅馬.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")[君士坦丁大帝死後](../Page/君士坦丁大帝.md "wikilink")，繼位的[君士坦提烏斯二世支持](../Page/君士坦提烏斯二世.md "wikilink")[亞流派](../Page/亞流派.md "wikilink")。故此，聖父（即天父）及聖子（即耶穌）本質的教義并未平息。在君士坦提烏斯二世死後，繼位的[狄奧多西一世於公元](../Page/狄奧多西一世.md "wikilink")380年入教。狄奧多西一世反對亞流教派，更頒布一系列法令支持基督教，確立其國教的地位。在此政治背景下，狄奧多西一世於公元381年召開第一次君士坦丁堡公會議，欲藉此解決基督教的長期紛爭。是次大公會議由[额我略·纳齐安主持](../Page/额我略·纳齐安.md "wikilink")，共186位[主教出席](../Page/主教.md "wikilink")。

由於《尼西亚信经》没有提及[圣神的神性](../Page/圣神.md "wikilink")，故此會議除重新肯定《尼西亚信经》外，更宣告了「圣灵具有神性」的信仰。再定[亞流教派為](../Page/阿里烏教派.md "wikilink")[異端](../Page/異端.md "wikilink")。此後，三位一體的神學教義才正式成為基督教傳統教義。會議還限制了主教在其教区内的司法审判权和宣布：「君士坦丁堡主教在羅馬主教之後，亦享有首席的尊榮，因為君士坦丁堡是新羅馬。」\[4\]

## 教会法

会议共颁布了七条[教会法](../Page/教会法.md "wikilink")，其中四条是教义性质的，三条是教规性质的。[正教会和](../Page/正教會.md "wikilink")[东方正统教会接受并承认全部七条教会法](../Page/東方正統教會.md "wikilink")。而[天主教会仅承认前四条](../Page/天主教會.md "wikilink")。\[5\]其原因是由于天主教会认为前四条教会法被收录于最早的记录中，并宣称后三条是附加的。\[6\]

  - 第一条教会法是一项重要教义。全方面谴责了[阿利乌派](../Page/阿里烏教派.md "wikilink")、[马西顿派以及](../Page/马西顿派.md "wikilink")[亚波里拿留派](../Page/亞波里拿留派.md "wikilink")。\[7\]
  - 第二条教会法限制了[主教的权利](../Page/主教.md "wikilink")。主教的权利缩小至其自身教区内部事务，禁止主教插手其他教区事务。（[安提阿教会除外](../Page/安条克希腊正教会.md "wikilink")）该教法颁布时，[额我略·那齐安正等候安提阿牧首梅勒提乌斯将他祝圣为君士坦丁堡主教](../Page/聖額我略·納齊安.md "wikilink")。而亚历山大主教彼得则同时试图推举马克西姆斯为正统的君士坦丁堡主教，该条教法旨在防止彼得的谋划得到实施。\[8\]
  - 第三条宣布[君士坦丁堡主教作为新罗马主教](../Page/君士坦丁堡普世牧首.md "wikilink")，其权柄仅次于[罗马主教](../Page/教宗.md "wikilink")，位居第二。\[9\]
  - 第四条宣布免去君士坦丁堡主教[马克西姆斯一世的职位](../Page/马克西姆斯一世.md "wikilink")，任何由他按立的神职人员全部取消神职资格。\[10\]
  - 第五条（可能于翌年，382年颁布）不承认西部各主教对安提阿教会颁布的一些教令。并支持安提阿教会的立场。至于这些教令是指具体哪些教令，有人认为是[塞尔迪卡公会议期间颁布的](../Page/塞尔迪卡公会议.md "wikilink")，但其后被学者驳斥为不合史实。其实际情况应为380年罗马教宗[達瑪穌一世对安提阿牧首](../Page/達瑪穌一世.md "wikilink")[梅勒提乌斯颁布革除教籍的教令](../Page/梅勒提乌斯.md "wikilink")。或369年的另一份西方教会致东方教会的教令。\[11\]
  - 第六条（可能于翌年，382年颁布）禁止随意控告、投诉主教和教会管理者。投诉必须经过检验。所有被革除教籍的人员无权投诉任何神职人员。合理的投诉需经过地方审理后才能向上呈报。\[12\]
  - 第七条（可能于翌年，382年颁布）除[欧诺米派](../Page/欧诺米派.md "wikilink")、[孟他努派](../Page/孟他努派.md "wikilink")、萨贝利派以外的异端（如阿利乌派、马西顿派、[诺洼天派等](../Page/诺洼天派.md "wikilink")）如要重新回归正统教义，只需傅油。欧诺米派等则需外加[驱魔仪式](../Page/驅魔.md "wikilink")，并重新接受洗礼。\[13\]

## 具深遠影響的結果

  - 修訂了《尼西亞信經》，宣告了「圣神（聖靈）具有神性」的信仰。
  - 肯定了君士坦丁堡主教的优越地位。

## 参考

[Category:大公會議](../Category/大公會議.md "wikilink")
[Category:君士坦丁堡](../Category/君士坦丁堡.md "wikilink")
[Category:381年](../Category/381年.md "wikilink")
[Category:基督教神学争议](../Category/基督教神学争议.md "wikilink")
[Category:4世纪罗马帝国](../Category/4世纪罗马帝国.md "wikilink")

1.  Socrates Scholasticus, *Church History*, book 5, chapters 8 & 11,
    puts the council in the same year as the revolt of Magnus Maximus
    and death of Gratian.

2.

3.  Richard Kieckhefer (1989). "Papacy". *[Dictionary of the Middle
    Ages](../Page/Dictionary_of_the_Middle_Ages.md "wikilink")*. .

4.  [各地方教會的組織和彼此間的關係及羅馬主教的首席地位](http://www.vaticanradio.org/cinesebig5/churchistory/storiaconcis/1storia26.html)
     [梵蒂岡電臺](../Page/梵蒂岡電臺.md "wikilink")

5.

6.

7.
8.

9.

10.

11.

12.

13.