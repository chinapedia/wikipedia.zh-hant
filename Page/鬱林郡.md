\-{T|zh:鬱林郡;zh-hans:鬱林郡;zh-hant:鬱林郡;}-

**-{A|zh:鬱林;zh-hans:鬱林;zh-hant:鬱林;}-郡**，為中國古代[行政區劃之一](../Page/行政區劃.md "wikilink")，[秦朝時置](../Page/秦朝.md "wikilink")「**桂林郡**」，漢武帝以後改稱「**鬱林郡**」。秦、漢時郡治設於[布山縣](../Page/布山縣.md "wikilink")，即今廣西[桂平市西](../Page/桂平市.md "wikilink")，轄境集中在今天的[广西壮族自治区境内](../Page/广西壮族自治区.md "wikilink")。

## 秦代

秦代的桂林郡初設於西元前214年。該郡為[秦始皇於嶺南地區設置的三郡之一](../Page/秦始皇.md "wikilink")，位置約在今日[廣西一帶](../Page/廣西.md "wikilink")，面積達20萬平方公里以上。郡治布山縣（今[貴港市城區南岸南江村](../Page/貴港市.md "wikilink")），該行政中心則設於今日[貴港市城區南岸](../Page/貴港市.md "wikilink")。而之所以取名桂林，應與境中多桂樹有關。桂林郡於秦末為[南越朝所轄](../Page/南越國.md "wikilink")。

## 漢代

[西漢](../Page/西漢.md "wikilink")[元鼎五年](../Page/元鼎.md "wikilink")（前112年）[漢武帝因](../Page/漢武帝.md "wikilink")[南越國內亂派](../Page/南越國.md "wikilink")[路博德率軍南下](../Page/路博德.md "wikilink")，翌年南越滅亡，改置**鬱林郡**，郡治仍布山縣。統轄布山、安广、河林、广都、中留、桂林、谭中、临尘、定周、领方、增食、雍鸡等县。即現今[南宁](../Page/南宁.md "wikilink")、[百色及](../Page/百色.md "wikilink")[柳州大部分地区](../Page/柳州.md "wikilink")，[玉林北部地区](../Page/玉林.md "wikilink")，[河池东部和南部地区](../Page/河池.md "wikilink")。

## 魏晉南北朝

## 参考资料

[Category:秦朝的郡](../Category/秦朝的郡.md "wikilink")
[Category:汉朝的郡](../Category/汉朝的郡.md "wikilink")
[Category:东吴的郡](../Category/东吴的郡.md "wikilink")
[Category:晋朝的郡](../Category/晋朝的郡.md "wikilink")
[Category:南朝的郡](../Category/南朝的郡.md "wikilink")
[Category:隋朝的郡](../Category/隋朝的郡.md "wikilink")
[Category:唐朝的郡](../Category/唐朝的郡.md "wikilink")
[Category:广西的郡](../Category/广西的郡.md "wikilink")