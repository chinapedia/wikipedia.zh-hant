\-{zh-hant:**杯葛**（），又稱作**抵制**;zh-hans:**抵制**，又称作**杯葛**（）}-，在商業領域上則稱為**不買運動**，是指联合抵制某个个人或公司，甚至是國家的集體行為。通常普遍由于被杯葛者被认为做了不道德的行為，而具体行动则包括拒绝参加与被抵制者相关的活动等。更严厉的抵制则包括拒绝各种形式的[贸易往来](../Page/贸易.md "wikilink")，这种杯葛运动则通常被支持者称为“[禁运](../Page/禁运.md "wikilink")”。

这种不道德可以用任何语言来说明，也不一定是普遍的。一次杯葛根据时间长短和范围不同，可能更倾向于使冒犯者在精神上感到羞耻，而不是在[经济上惩罚他们](../Page/经济.md "wikilink")。如果是长期和广泛的，杯葛就成为[道德的消费战术之一](../Page/有良知的消费观.md "wikilink")。

## 词源

台湾及港澳地区常用的“-{杯葛}-”一词譯自英語“”，源自英军退役上尉[查爾斯·杯-{}-葛](../Page/查爾斯·杯葛.md "wikilink")（），他退役后在[爱尔兰担当](../Page/爱尔兰.md "wikilink")[梅欧郡地主欧恩伯爵的土地经纪人](../Page/梅欧郡.md "wikilink")。1880年，因为反对当时的[土地改革](../Page/土地改革.md "wikilink")，拒绝降低地租，并驱逐[佃户](../Page/佃農.md "wikilink")，受到了由[爱尔兰土地同盟组织的](../Page/爱尔兰土地同盟.md "wikilink")-{抵制}-。当时查爾斯无法雇佣任何人收割[农作物](../Page/农作物.md "wikilink")，直到[爱尔兰联合主义者和](../Page/爱尔兰联合主义者.md "wikilink")[英军志愿為他服務](../Page/英军.md "wikilink")，但有时甚至需要7000个護衛来保护他，而最后也被迫离开了爱尔兰。

## 早期实践

虽然直到1880年才发明杯葛一词，但其实践却可以至少追溯到1830年，当时美国[全国黑人大会鼓励抵制奴隶生产的商品](../Page/全国黑人大会.md "wikilink")。其他的实践有[非裔美国人在](../Page/非裔美国人.md "wikilink")[美国公民权运动中进行的抵制](../Page/美国公民权运动.md "wikilink")；[美国农场工人联合会组织的对葡萄和生菜的抵制](../Page/美国农场工人联合会.md "wikilink")；在[美国革命期间美国人对英国商品的抵制](../Page/美国革命.md "wikilink")。在印度由[莫罕达斯·甘地组织的对英国商品的抵制](../Page/莫罕达斯·甘地.md "wikilink")；以及[阿拉伯联盟对](../Page/阿拉伯联盟.md "wikilink")[以色列和与](../Page/以色列.md "wikilink")[以色列进行贸易的公司的抵制](../Page/以色列.md "wikilink")。1973年，[阿拉伯国家规定的对西方国家的](../Page/阿拉伯.md "wikilink")[原油禁运](../Page/原油.md "wikilink")，参见[1973年石油危机](../Page/1973年石油危机.md "wikilink")。其他的例子还包括[美国](../Page/美国.md "wikilink")（在[卡特](../Page/卡特.md "wikilink")[总统领导下](../Page/总统.md "wikilink")）为了抗议[苏联侵略](../Page/苏联.md "wikilink")[阿富汗](../Page/阿富汗.md "wikilink")，抵制在[莫斯科举办的](../Page/莫斯科.md "wikilink")[1980年夏季奥林匹克运动会](../Page/1980年夏季奥林匹克运动会.md "wikilink")，以及大多数[社会主义阵营国家报复性地抵制在](../Page/社会主义阵营.md "wikilink")[洛杉矶举办的](../Page/洛杉矶.md "wikilink")[1984年夏季奥林匹克运动会](../Page/1984年夏季奥林匹克运动会.md "wikilink")，以及在1980年代倡议从[南非](../Page/南非.md "wikilink")[撤资](../Page/撤资.md "wikilink")，以抗议该国实行的[种族隔离制度的运动](../Page/种族隔离制度.md "wikilink")。1989年中国发生[六四事件](../Page/六四事件.md "wikilink")，世界各国纷纷进行制裁，其中美国和欧盟至今对中国武器禁运。2008年西藏发生[三一四事件](../Page/三一四事件.md "wikilink")，西方国家开始抵制[北京奥运](../Page/北京奥运.md "wikilink")，英国和法国甚至出现抢夺圣火事件。

## 运用和效果

杯葛通常是一次性的行动，用于纠正单个显著的错误。如果延续为长时期活动，或者成为唤醒公众意识或改革法律和制度的整体计划一部分时，杯葛成为[有良知的消费观的一部分](../Page/有良知的消费观.md "wikilink")，这些经济和政治的术语更好一些。

今天大多数有组织的贸易杯葛需要长期改变参与者的消费习惯，因此作为越大型的政治战略的一部分，在政策上就需要越长期的保证，比如整个[商品市场的结构改革](../Page/商品市场.md "wikilink")，或者政府出面倡导[有良知的消费](../Page/有良知的消费.md "wikilink")，例如上面提到的为抗议种族隔离制度而对南非公司进行的为时甚久的抵制。这些都扩展了杯葛的意义。

贸易杯葛的另一种形式是用相当产品替代，如[麦加可乐或](../Page/麦加可乐.md "wikilink")[天房可乐](../Page/天房可乐.md "wikilink")。

今天杯葛的一个主要目标是[消费主义本身](../Page/消费主义.md "wikilink")，例如[感恩节后的星期五在全球范围进行的](../Page/感恩节.md "wikilink")“[国际无消费日](../Page/国际无消费日.md "wikilink")”。另一个现代杯葛的例子是[狄克西女子合唱團因成员](../Page/狄克西女子合唱團.md "wikilink")[娜塔丽·麦恩斯贬损](../Page/娜塔丽·麦恩斯.md "wikilink")[小布什总统而被列入黑名单](../Page/乔治·沃克·布什.md "wikilink")，结果是大多数乡村音乐电台都拒绝播放她们的音乐。

## 参见

  - [莫斯科奧運](../Page/莫斯科奧運.md "wikilink")
  - [北京奧運](../Page/北京奧運.md "wikilink")
  - [禁运](../Page/禁运.md "wikilink")
  - [抵制选举](../Page/抵制选举.md "wikilink")
  - [杯葛列表](../Page/杯葛列表.md "wikilink")
  - [有良知的消费观](../Page/有良知的消费观.md "wikilink")
  - [獵奇行動](../Page/獵奇行動.md "wikilink")
  - [1973年石油危机](../Page/1973年石油危机.md "wikilink")
  - [抵制雀巢产品](../Page/抵制雀巢产品.md "wikilink")
  - [非暴力抵抗](../Page/非暴力抵抗.md "wikilink")
  - [一级抵制](../Page/一级抵制.md "wikilink")
  - [二级抵制](../Page/二级抵制.md "wikilink")
  - [阻止埃索行动](../Page/阻止埃索行动.md "wikilink")
  - [抵制日货](../Page/抵制日货.md "wikilink")
  - [抵制、撤资、制裁（以色列）](../Page/抵制、撤资、制裁.md "wikilink")

[杯葛](../Category/杯葛.md "wikilink")
[Category:经济](../Category/经济.md "wikilink")