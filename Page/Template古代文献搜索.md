<div style="float: right; padding: .5em; margin-left: .5em; margin-top:.5em; background-color: #f0f0ff; border: 1px solid #c6c9ff; white-space: nowrap; width:16em">

![Search_icon.png](Search_icon.png "Search_icon.png") **古代文献搜索：**
*关键词：*

<div style="float: right">

<span class="editlink">**\[ 编辑\]**</span>

</div>

  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/jinbu/
    国学网—经部\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/shibu/
    国学网—史部\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/zibu/
    国学网—子部\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/jibu/
    国学网—集部\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/qts/
    国学网—全唐诗\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/tangyanjiu/tdsl/
    国学网—唐代笔记史料\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/wenxian/
    国学网—隋唐五代文献\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/qsc/
    国学网—全宋词\]
  - \[[http://www.google.com/search?q=+site:www.guoxue.com+](http://www.google.com/search?q=+site:www.guoxue.com+.md)+inurl:www.guoxue.com/minqingstory/
    国学网—明清小说\]

</div>