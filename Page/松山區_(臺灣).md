[Songshan_District_Administration_Center,_Taipei_City_20170201.jpg](https://zh.wikipedia.org/wiki/File:Songshan_District_Administration_Center,_Taipei_City_20170201.jpg "fig:Songshan_District_Administration_Center,_Taipei_City_20170201.jpg")
**松山區**是[中華民國](../Page/中華民國.md "wikilink")[臺北市轄下的](../Page/臺北市.md "wikilink")[行政區](../Page/市轄區.md "wikilink")，位於[臺灣北部](../Page/臺灣.md "wikilink")，臺北市中心偏東地帶。區內有許多大型[百貨公司及](../Page/百貨公司.md "wikilink")[購物中心](../Page/購物中心.md "wikilink")、夜店，[臺北小巨蛋及](../Page/台北小巨蛋.md "wikilink")[松山機場也位於此區](../Page/臺北松山機場.md "wikilink")。區內的[饒河街夜市則為臺灣著名的](../Page/饒河街夜市.md "wikilink")[觀光](../Page/觀光.md "wikilink")[夜市之一](../Page/夜市.md "wikilink")。而區內的[敦化北路及](../Page/敦化北路.md "wikilink")[南京東路一帶是臺北重要的金融商圈](../Page/南京東路_\(臺北市\).md "wikilink")，不少國內外知名的企業及[金融機構均在此雲集](../Page/金融機構.md "wikilink")，而有「臺北[華爾街](../Page/華爾街.md "wikilink")」之稱。此區也有臺北市最早的[都市計劃開發區](../Page/都市計劃.md "wikilink")[民生社區](../Page/民生社區_\(臺北市\).md "wikilink")，由於該區域內[建蔽率低](../Page/建蔽率.md "wikilink")，[容積率嚴格限制](../Page/容積率.md "wikilink")，而且處處[公園](../Page/公園.md "wikilink")，文教設施完善，而成為臺北市區傳統的優質[住宅區之一](../Page/住宅區.md "wikilink")，2010年臺北市舉辦[臺北國際花卉博覽會而制定區花](../Page/臺北國際花卉博覽會.md "wikilink")，松山區為[朱槿](../Page/朱槿.md "wikilink")，[臺北市政府民政局也特別製作區花版門牌](../Page/臺北市政府民政局.md "wikilink")。

## 歷史

  - 昔日為[凱達格蘭族](../Page/凱達格蘭族.md "wikilink")-{猫里}-[錫口社](../Page/錫口.md "wikilink")（[羅馬拼音](../Page/羅馬拼音.md "wikilink")：Malotsigauan）之地，亦音譯作**麻里折口**、**麻里即吼**、**毛里即錫口**。
  - 1815年（清嘉慶20年）時刪去-{猫里}-，改稱錫口。
  - 1920年（大正9年），全臺地名重整，錫口改為日式地名「松山」，設「-{[松山庄](../Page/松山庄.md "wikilink")}-」，隸屬[臺北州](../Page/臺北州.md "wikilink")[七星郡](../Page/七星郡.md "wikilink")。
  - 1938年（昭和13年），-{[松山庄](../Page/松山庄.md "wikilink")}-併入臺北市。
  - 1945年（民國34年；昭和20年）10月25日，中華民國接管臺灣，松山-{庄}-改置松山區。
  - 1990年（民國79年）3月23日，臺北市行政區重劃，劃出南半部設立[信義區](../Page/信義區_\(臺北市\).md "wikilink")。

<!-- end list -->

  - 由來和英譯

「松山」的由來一說是因為當地風景和[四國的](../Page/四國.md "wikilink")[松山相似](../Page/松山市.md "wikilink")，另一說是因為附近山丘多松樹。松山原為日式地名，其發音為[日語之](../Page/日語.md "wikilink")「Matsuyama」，在[日治時期對外音譯均採用之](../Page/台灣日治時期.md "wikilink")，如松山飛行場稱作「Matsuyama
Airdrome」。戰後雖沿用該名，但對外音譯則改為[華語發音](../Page/華語.md "wikilink")，而成為「Sungshan」（[威妥瑪拼音](../Page/威妥瑪拼音.md "wikilink")），2003年後改為
「Songshan」（[漢語拼音](../Page/漢語拼音.md "wikilink")）。但一般來說，松科植物在北緯40-60度間，或高海拔的地方較為常見。松山位於臺北盆地，既不是山，也不可能有松樹。但臺灣真的有一座"松山"，位於阿里山山脈的中間，海拔2500公尺。因此也有可能只是當時負責改名的人來自日本某個縣的松山也不一定。（大灣大員福爾摩沙
翁佳音 曹銘宗）

## 地理位置

**東界**：以三張犁截水圳、[玉成橋](../Page/玉成橋.md "wikilink")、[松山橋鄰接](../Page/松山橋.md "wikilink")[南港區](../Page/南港區.md "wikilink")。
**西界**：以復興北路、南路及[松山機場西側圍牆鄰接](../Page/松山機場.md "wikilink")[中山區](../Page/中山區_\(臺北市\).md "wikilink")。
**南界**：市民大道四段（復興南路至光復南路），鄰接[大安區](../Page/大安區_\(臺北市\).md "wikilink")。市民大道五段至[松山車站](../Page/松山車站.md "wikilink")，鄰接[信義區](../Page/信義區_\(臺北市\).md "wikilink")。
**北界**：以基隆河三張犁截水圳出口至港墘路口堤外河道，鄰接[內湖區](../Page/內湖區.md "wikilink")。港墘路口堤外基隆河道、七號水門至機場北邊圍牆鄰接[中山區](../Page/中山區_\(臺北市\).md "wikilink")。

[缩略图](https://zh.wikipedia.org/wiki/File:District_Office_Layout,_Songshan_District_Administration_Center,_Taipei_City_20150912.jpg "fig:缩略图")

## 行政區域

[TaipeiSongShanSubDistrictMap.png](https://zh.wikipedia.org/wiki/File:TaipeiSongShanSubDistrictMap.png "fig:TaipeiSongShanSubDistrictMap.png")
全區共分為33里，各里名稱為：

  - **東社次分區**：[中華里](../Page/中華里_\(臺北市\).md "wikilink")、民福里、東昌里、松基里、龍田-{里}-、民有里、[東光里](../Page/東光里_\(臺北市\).md "wikilink")、東勢里、[精忠里共](../Page/精忠里.md "wikilink")9里。
  - **三民次分區**：三民里、東榮里、[新東里](../Page/新東里_\(臺北市\).md "wikilink")、富泰里、[介壽里](../Page/介壽里_\(臺北市\).md "wikilink")、[莊敬里](../Page/莊敬里_\(臺北市\).md "wikilink")、新益里、富錦里共8里。
  - **[中崙次分區](../Page/中崙_\(臺北市\).md "wikilink")**：[中正里](../Page/中正里_\(臺北市\).md "wikilink")、吉仁里、敦化里、復源里、福成里、中崙里、美仁里、復建里、復勢里共9里。
  - **本鎮次分區**：安平里、[自強里](../Page/自強里_\(臺北市\).md "wikilink")、新聚里、鵬程里、吉祥里、復盛里、慈祐里共7里。

## 人口

## 教育與醫療機構

### 醫療機構

  - [臺安醫院](../Page/臺安醫院.md "wikilink")-位於八德路
  - [博仁綜合醫院](../Page/博仁綜合醫院.md "wikilink")-位於光復北路
  - [三軍總醫院松山分院](../Page/三軍總醫院松山分院.md "wikilink")（前國軍松山總醫院、俗稱807醫院、空軍總醫院、空軍醫院、空總）-位於健康路
  - [長庚醫院臺北院區](../Page/財團法人長庚紀念醫院.md "wikilink")-位於敦化北路
  - 臺北市立聯合醫院松山門診部：位於八德路4段
  - 民生醫事檢驗所：位於民生東路5段

### 高級中等學校

  - [臺北市立中崙高級中學](../Page/臺北市立中崙高級中學.md "wikilink")
  - [臺北市立西松高級中學](../Page/臺北市立西松高級中學.md "wikilink")
  - [臺北市私立育達高級商業家事職業學校](../Page/臺北市私立育達高級商業家事職業學校.md "wikilink")

### 國民中學

[台北市立中山國民中學.jpg](https://zh.wikipedia.org/wiki/File:台北市立中山國民中學.jpg "fig:台北市立中山國民中學.jpg")。\]\]

  - [臺北市立民生國民中學](../Page/臺北市立民生國民中學.md "wikilink")
  - [臺北市立中山國民中學](../Page/臺北市立中山國民中學.md "wikilink")
  - [臺北市立敦化國民中學](../Page/臺北市立敦化國民中學.md "wikilink")
  - [臺北市立介壽國民中學](../Page/臺北市立介壽國民中學.md "wikilink")
  - [臺北市立中崙高級中學附屬國民中學](../Page/臺北市立中崙高級中學.md "wikilink")
  - [臺北市立西松高級中學附屬國民中學](../Page/臺北市立西松高級中學.md "wikilink")

### 國民小學

  - [臺北市松山區松山國民小學](../Page/臺北市松山區松山國民小學.md "wikilink")
  - [臺北市松山區民生國民小學](http://www.msps.tp.edu.tw/)
  - [臺北市松山區民權國民小學](http://www.mcps.tp.edu.tw/)
  - [臺北市松山區敦化國民小學](http://www.thps.tp.edu.tw/)
  - [臺北市松山區三民國民小學](http://www.smps.tp.edu.tw/)
  - [臺北市松山區民族國民小學](http://www.mces.tp.edu.tw/)
  - [臺北市松山區西松國民小學](../Page/臺北市松山區西松國民小學.md "wikilink")
  - [臺北市松山區健康國民小學](../Page/臺北市松山區健康國民小學.md "wikilink")

### 社會教育

  - [臺北市松山社區大學](../Page/臺北市松山社區大學.md "wikilink")

## 臺北市立圖書館（松山區各分館）

  - 台北小巨蛋FastBook全自動借書站
  - 松山分館
  - 民生分館
  - 三民分館
  - 中崙分館
  - 啟明分館
  - 松山機場智慧圖書館

## 私立圖書館

  - [行天宮附設玄空圖書館](../Page/行天宮.md "wikilink")-敦化總館：位於敦化北路

## 藝文

  - 臺北市藝文推廣處-位於八德路三段，前身為臺北市立社會教育館。

## 體育場館

  - [松山運動中心](../Page/松山運動中心.md "wikilink")
  - [臺北體育館](../Page/台北體育館.md "wikilink")
  - 民生社區活動中心
  - [臺北小巨蛋](../Page/台北小巨蛋.md "wikilink")

## 經濟

  - [日商華大成營造總部](http://www.taisei.co.jp/)
  - [台北文華東方酒店](../Page/台北文華東方酒店.md "wikilink")：前身即「中泰賓館」。
  - 日商百樂文具(PILOT)股份有限公司台灣分公司：位於臺北市松山區民權東路三段
  - 大臺北區瓦斯股份有限公司：成立於1964年，供應臺北市之民生用「天然氣」，營業區域為台北市的松山、信義、大安、大同、萬華、中正、中山等七個行政區及士林區之福華、明勝二里，總部位於臺北市松山區光復北路。
  - [台灣角川股份有限公司](../Page/台灣角川.md "wikilink")：日本角川集團在臺灣設立，知名小說和漫畫的出版公司，總部位於臺北市松山區光復北路。
  - 日商NIKON相機總代理商【國祥貿易】(台北總公司)：位於臺北市松山區南京東路三段
  - 圓神出版社有限公司：新聞曾報導老闆一週只讓員工上四天班的知名公司，位於臺北市松山區南京東路四段。
  - 香港商亞洲費列羅有限公司台灣分公司：以銷售「金莎」、「健達」巧克力知名企業，台灣分公司位於臺北市松山區南京東路五段。
  - 台灣[歐姆龍公司](../Page/歐姆龍.md "wikilink")：知名電子血壓計銷售企業，總公司位於臺北市松山區復興北路。
  - [宏碁股份有限公司](../Page/宏碁.md "wikilink")(Acer
    Incorporated)：知名全球五大品牌之一電腦企業，臺北市總部位於松山區復興北路。
  - [台塑關係企業](../Page/台塑關係企業.md "wikilink")：總部位於臺北市松山區敦化北路
  - [汎航通運](../Page/汎航通運.md "wikilink")：總部位於臺北市松山區敦化北路
  - [富邦集團](../Page/富邦集團.md "wikilink")：總部位於臺北市松山區敦化南路一段
  - [好來化工](../Page/好來化工.md "wikilink")：以生產與銷售「黑人牙膏」、「高露潔牙膏」著名，總部位於臺北市松山區敦化南路一段

## 各國駐臺使館與代表機構

  - [日本台灣交流協會臺北事務所](../Page/日本台灣交流協會.md "wikilink")：位於慶城街

  - [法國在臺協會](../Page/法國在臺協會.md "wikilink")：位於敦化北路

  - [南非聯絡辦事處](../Page/南非聯絡辦事處.md "wikilink")：位於敦化北路

  - [丹麥商務辦事處](../Page/丹麥商務辦事處.md "wikilink")：位於敦化北路

  - [馬來西亞友誼及貿易中心](../Page/馬來西亞友誼及貿易中心.md "wikilink")：位於敦化北路

  - [比利時台北辦事處](../Page/比利時台北辦事處.md "wikilink")：位於民生東路三段

## 購物商場

  - [家樂福三民店](../Page/家樂福.md "wikilink")：位於臺北市松山區三民路

## 團體

  - 財團法人[行天宮文教發展促進基金會](../Page/行天宮.md "wikilink")：位於南京東路三段
  - 財團法人圓覺宗智敏慧華金剛上師教育基金會：位於南京東路五段
  - [新黨總部](../Page/新黨.md "wikilink")：位於松山區光復南路

## 交通

### 捷運

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - ：[南京復興站](../Page/南京復興站.md "wikilink") -
    [中山國中站](../Page/中山國中站.md "wikilink") -
    [松山機場站](../Page/松山機場站.md "wikilink")

  - ：[南京復興站](../Page/南京復興站.md "wikilink") -
    [台北小巨蛋站](../Page/台北小巨蛋站.md "wikilink") -
    [南京三民站](../Page/南京三民站.md "wikilink") -
    [松山站](../Page/松山車站_\(台灣\).md "wikilink")

註：臺鐵[松山車站實際上位於](../Page/松山車站_\(台灣\).md "wikilink")[信義區](../Page/信義區_\(台北市\).md "wikilink")。

### 高速公路

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號（汐止五股高架橋）](../Page/汐止五股高架橋.md "wikilink")：<span style="border: 2px solid black; background: orange; color: black; font-family:Arial Unicode MS,黑體,黑体, Arial;">20下塔悠</span>下塔悠出口

### 航空

  - [臺北松山機場](../Page/臺北松山機場.md "wikilink")

### 未來

#### 捷運

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - <font color={{台北捷運色彩|黃}}>█</font> </small>[環狀線](../Page/環狀線_\(台北捷運\).md "wikilink")（規劃中）:[松山站](../Page/松山車站_\(台北市\).md "wikilink")
  - <font color={{台北捷運色彩|民}}>█</font> </small>[民生汐止線](../Page/民生汐止線.md "wikilink")（規劃中）：[<font color="#888888">東社站</font>](../Page/東社站.md "wikilink")
    - [<font color="#888888">民生社區站</font>](../Page/民生社區站.md "wikilink")

## 旅遊

### 公園

[台北市松山區敦中公園.jpg](https://zh.wikipedia.org/wiki/File:台北市松山區敦中公園.jpg "fig:台北市松山區敦中公園.jpg")

  - [敦中公園](../Page/敦中公園.md "wikilink")
  - [民權公園](../Page/民權公園.md "wikilink")
  - [撫遠公園](../Page/撫遠公園.md "wikilink")
  - [富民生態公園](../Page/富民生態公園.md "wikilink")
  - [民生公園](../Page/民生公園.md "wikilink")
  - [觀山河濱公園](../Page/觀山河濱公園.md "wikilink")
  - [迎風河濱公園](../Page/迎風河濱公園.md "wikilink")

### 廟宇

  - [東社慈福宮](../Page/東社慈福宮.md "wikilink")
  - [松山慈祐宮](../Page/松山慈祐宮.md "wikilink")
  - [台北府城隍廟](../Page/台北府城隍廟.md "wikilink")
  - [松山霞海城隍廟](../Page/松山霞海城隍廟.md "wikilink")
  - [應媽廟](../Page/應媽廟.md "wikilink")
  - [北巡武德宮](../Page/北巡武德宮.md "wikilink")：位於松山區塔悠路，新聞媒體曾報導知名藝人張小燕、蕭敬騰、黃韻玲等人必點此宮的財神光明燈。

### 商場、商圈、夜市

  - [饒河街夜市](../Page/饒河街夜市.md "wikilink")
  - [京華城](../Page/京華城.md "wikilink")
  - [微風廣場](../Page/微風廣場.md "wikilink")
  - [微風南京](../Page/微風南京.md "wikilink")
  - [CITYLINK松山店](../Page/CITYLINK_\(購物中心\).md "wikilink")
  - [富錦街](../Page/富錦街.md "wikilink")：有「台北小歐洲」之稱的街道。

## 媒體

### 電視臺

  - [臺灣電視公司](../Page/臺灣電視公司.md "wikilink")：一般大眾習慣簡稱「臺視」。總部位於臺北市松山區敦化里3鄰八德路三段
  - [非凡電視臺](../Page/非凡電視台.md "wikilink")：知名股票投資新聞電視台。
  - 美商超躍有限公司臺灣分公司（代理[Animax](../Page/Animax.md "wikilink")、[AXN](../Page/AXN.md "wikilink")，位於民生東路三段）

### 音樂唱片公司

  - [台灣索尼音樂娛樂](../Page/台灣索尼音樂娛樂.md "wikilink")(SONY Music Entertainment
    (Taiwan) Ltd)：舊稱新力音樂唱片公司，總部位於光復北路，世界三大唱片公司之一。
  - [華納音樂 (台灣)](../Page/華納音樂_\(台灣\).md "wikilink")（Warner Music Taiwan
    Ltd）：總部位於民生東路四段，世界三大唱片公司之一。
  - [環球音樂 (台灣)](../Page/環球音樂_\(台灣\).md "wikilink")(Universal Music
    Taiwan Ltd)：總部位於敦化北路，世界三大唱片公司之一。
  - 世界三大唱片公司的台灣總部，全都設立於臺北市松山區。
  - [愛貝克思 (台灣)](../Page/愛貝克思_\(台灣\).md "wikilink")(Avex Taiwan
    Inc)：是日本音樂商Avex Trax的台灣子公司，總部位於復興北路。
  - [杰威爾音樂有限公司](../Page/杰威爾音樂.md "wikilink")(JVR Music International
    Ltd.)：由音樂人[周杰倫](../Page/周杰倫.md "wikilink")(Jay)、填詞人[方文山](../Page/方文山.md "wikilink")(Vincent)以及前阿爾發音樂總經理楊峻榮(JR)合辦，杰威爾音樂旗下歌手唱片均由[台灣索尼音樂娛樂代理發行](../Page/台灣索尼音樂娛樂.md "wikilink")，總部位於臺北市松山區長春路。
  - [相信音樂國際股份有限公司](../Page/相信音樂國際股份有限公司.md "wikilink")(B'in Music Co.
    Ltd)：由音樂人[五月天](../Page/五月天.md "wikilink")(Mayday)、前[滾石唱片策略長陳勇志](../Page/滾石唱片.md "wikilink")（[陳沒](../Page/陳沒.md "wikilink")）(chen
    mo)以及前[滾石唱片](../Page/滾石唱片.md "wikilink")[謝芝芬合辦](../Page/謝芝芬.md "wikilink")，公司名稱為[五月天](../Page/五月天.md "wikilink")[阿信所命名](../Page/陳信宏.md "wikilink")，相信音樂旗下歌手唱片均由[台灣索尼音樂娛樂代理發行](../Page/台灣索尼音樂娛樂.md "wikilink")，總部位於臺北市松山區光復南路33巷12號2樓、6樓、7樓。

## 知名企業

  - [台灣玻璃工業股份有限公司](../Page/台灣玻璃工業股份有限公司.md "wikilink")(Taiwan Glass Ind.
    Corp.)：簡稱台玻，[上市公司](../Page/上市公司.md "wikilink")，一家玻璃及其製品製造公司，總部位於台北市松山區[南京東路](../Page/南京東路_\(台北市\).md "wikilink")。
  - [中華工程股份有限公司](../Page/中華工程股份有限公司.md "wikilink")(BES Engineering
    Corporation)：簡稱中華工程，[上市公司](../Page/上市公司.md "wikilink")，一家營造工程及其不動產開發、規劃設計及銷售公司，總部位於台北市松山區東興路。
  - [台灣角川股份有限公司](../Page/台灣角川股份有限公司.md "wikilink")(KADOKAWA TAIWAN
    CORPORATION)：簡稱台灣角川，一家出版公司，是日本[角川集團在台灣設立的出版公司](../Page/角川集團.md "wikilink")，亦是其首間海外子公司，總部位於台北市松山區光復北路。

## 名人

  - [孫越 (演員)](../Page/孫越_\(演員\).md "wikilink")
  - [傅達仁](../Page/傅達仁.md "wikilink")：臺灣電視公司體育主播之一，也是知名節目主持人。
  - [廖亦崟](../Page/廖亦崟.md "wikilink")：藝名威廉，以前曾是[模范棒棒堂成員](../Page/模范棒棒堂.md "wikilink")，現為[Lollipop@F成員](../Page/Lollipop@F.md "wikilink")。
  - [賴琳恩](../Page/賴琳恩.md "wikilink")：女藝人，以前曾是[我愛黑澀會成員](../Page/我愛黑澀會.md "wikilink")，2008年參加香港亞洲小姐競賽獲得季軍，進而知名度大開。
  - [王力宏](../Page/王力宏.md "wikilink")：出生於美國紐約，是著名華語流行音樂創作男歌手。
  - [賀軍翔](../Page/賀軍翔.md "wikilink")：臺灣男藝人。
  - [張孝全](../Page/張孝全.md "wikilink")：臺灣男演員。
  - [阮經天](../Page/阮經天.md "wikilink")：臺灣男演員。
  - [許瑋甯](../Page/許瑋甯.md "wikilink")：臺灣女演員。
  - [彭于晏](../Page/彭于晏.md "wikilink")：臺灣男演員。
  - [藍正龍](../Page/藍正龍.md "wikilink")：臺灣男藝人。
  - [卜學亮](../Page/卜學亮.md "wikilink")：臺灣綜藝界人物。
  - [李蒨蓉](../Page/李蒨蓉.md "wikilink")：臺灣女模特兒、主持人、演員。
  - [許慧欣](../Page/許慧欣.md "wikilink")：臺灣歌手、演員、芭蕾舞者。
  - [言承旭](../Page/言承旭.md "wikilink")：臺灣男模特兒，2001年演出偶像劇《流星花園》的男主角道明寺，合組表演團體F4成員之一。
  - [周渝民](../Page/周渝民.md "wikilink")：臺灣男藝人，2001年演出偶像劇《流星花園》的男主角花澤類，合組表演團體F4成員之一。
  - [喻虹淵](../Page/喻虹淵.md "wikilink")：臺灣女演員。
  - [邵昕](../Page/邵昕.md "wikilink")：臺灣男藝人。
  - [陳昇](../Page/陳昇.md "wikilink")：臺灣創作歌手。
  - [吳玟萱](../Page/吳玟萱.md "wikilink")：臺灣女演員。
  - [趙詠華](../Page/趙詠華.md "wikilink")：臺灣80年代末期出道的女歌手，〈最浪漫的事〉是其90年代最著名歌曲。
  - [簡愷樂](../Page/簡愷樂.md "wikilink")：藝名愷樂，原名簡筑翎，舊藝名蝴蝶姐姐，臺灣女藝人。
  - [桂綸鎂](../Page/桂綸鎂.md "wikilink")：臺灣知名女演員。
  - [金完宣](../Page/金完宣.md "wikilink")：即「金元萱」，韓國女藝人，以前曾住在民生社區。
  - [徐巧芯](../Page/徐巧芯.md "wikilink")：第8屆國民黨青年團總團長、第14任總統參選人朱立倫辦公室發言人、網路節目主持人，現為前總統馬英九辦公室發言人，有「松山鄧麗君」的稱號。

## 更多松山的相關資訊

  - [波麗士大人](../Page/波麗士大人.md "wikilink")：是2008年的一部臺灣電視劇，由三立電視製作、臺視與三立都會台共同播出。本劇是臺灣一部以警察為主題的電視劇，此電視劇的拍攝地點，就位於松山警分局三民派出所。
  - [民生社區](../Page/民生社區_\(臺北市\).md "wikilink")
  - 臺北市松山區民生社區發展協會
  - 富錦街：有「台北小歐洲」之稱，膾炙人口的電影《藍色大門》、《第36個故事》以及電視偶像劇，都曾在此地取景拍攝。

## 軼事

值得一提的是，許多以該區為名的機構皆不在該區境內，如[松山家商](../Page/松山家商.md "wikilink")、[松山工農](../Page/松山工農.md "wikilink")、[松山高中](../Page/松山高中.md "wikilink")、[松山火車站](../Page/松山火車站.md "wikilink")、[松山文創園區](../Page/松山文創園區.md "wikilink")、[松山順天宮](../Page/松山順天宮.md "wikilink")、[松山慈惠堂](../Page/松山慈惠堂.md "wikilink")、[TOYOTA松山服務廠等等](../Page/TOYOTA.md "wikilink")...
松山區亦無松山國中，僅有中山國中。這是因為原本信義區 是屬於松山區一部分，1990年才劃出。

## 相關條目

  - [錫口](../Page/錫口.md "wikilink")
  - [民生社區](../Page/民生社區_\(臺北市\).md "wikilink")

## 参考资料

## 外部連結

  - [松山區公所](http://ssdo.gov.taipei/)
  - [松山區戶政事務所](http://sshr.gov.taipei/)

[松山區_(臺灣)](../Category/松山區_\(臺灣\).md "wikilink")
[Category:臺北市行政區劃](../Category/臺北市行政區劃.md "wikilink")
[Songshan](../Category/源自日本統治的台灣地名.md "wikilink")
[Songshan](../Category/1946年建立的行政區劃.md "wikilink")