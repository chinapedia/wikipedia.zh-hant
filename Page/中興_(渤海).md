**中興**（794年）是[渤海国成王](../Page/渤海国.md "wikilink")[大華璵的](../Page/大華璵.md "wikilink")[年号](../Page/年号.md "wikilink")，共计1年。

## 纪年

| 中興                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 794年                           |
| [干支](../Page/干支纪年.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") |

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用[中兴年号的政权](../Page/中兴_\(年号\).md "wikilink")
  - 同期存在的其他政权年号
      - [貞元](../Page/貞元_\(唐德宗\).md "wikilink")（785年正月－805年八月）：唐德宗的年號
      - [延曆](../Page/延曆.md "wikilink")（782年八月十九日－806年五月十八日）：奈良時代、[平安時代](../Page/平安時代.md "wikilink")[桓武天皇之年號](../Page/桓武天皇.md "wikilink")
      - [上元](../Page/上元_\(南诏\).md "wikilink")（784年起）：[南詔領袖](../Page/南詔.md "wikilink")[異牟尋之年號](../Page/異牟尋.md "wikilink")

[Category:渤海国年号](../Category/渤海国年号.md "wikilink")
[Category:8世纪中国年号](../Category/8世纪中国年号.md "wikilink")
[Category:790年代中国政治](../Category/790年代中国政治.md "wikilink")
[Category:794年](../Category/794年.md "wikilink")