**葫芦科**（[学名](../Page/学名.md "wikilink")：）是[真双子叶植物](../Page/真双子叶植物.md "wikilink")[葫芦目的一](../Page/葫芦目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，其中包括[黄瓜](../Page/黄瓜.md "wikilink")、[南瓜](../Page/南瓜.md "wikilink")、[丝瓜](../Page/丝瓜.md "wikilink")、[西瓜等常见的蔬菜和瓜果](../Page/西瓜.md "wikilink")。葫芦科是世界上最重要的食用植物科之一，其重要性仅次于[禾本科](../Page/禾本科.md "wikilink")、[豆科和](../Page/豆科.md "wikilink")[茄科](../Page/茄科.md "wikilink")。

大多数葫芦科的植物是[一年生的](../Page/一年生植物.md "wikilink")[爬藤植物](../Page/攀缘植物.md "wikilink")，常有螺旋状卷须，叶互生，形大，它们的[花比较大](../Page/花.md "wikilink")，比较鲜艳。同一植物上有雄性和雌性的花，雌性的花为子房下位花。[果实实际上是](../Page/果实.md "wikilink")[浆果的一种](../Page/浆果.md "wikilink")，稱為[瓠果或瓜果](../Page/瓠果.md "wikilink")。

大多数葫芦科植物会受到[南瓜藤透翅蛾](../Page/南瓜藤透翅蛾.md "wikilink")（）的[幼虫危害](../Page/幼虫.md "wikilink")。

## 分布

葫芦科广泛分布在全球[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，但被引种到世界各地栽培。[中国原产](../Page/中国.md "wikilink")20[属约](../Page/属.md "wikilink")130[种](../Page/种.md "wikilink")，引种栽培的有7属约30种。

## 分类

葫芦科共有约118个属、825个种，以下分类參照1990年[查尔斯·杰弗瑞](../Page/查尔斯·杰弗瑞.md "wikilink")（）的著作：

  - 翅子瓜亚科
      - 翅子瓜族
          - 亚族：属

          - 翅子瓜亚族：属、[翅子瓜属](../Page/翅子瓜属.md "wikilink")、属、属、属、[碧雷鼓属](../Page/碧雷鼓属.md "wikilink")、[棒锤瓜属](../Page/棒锤瓜属.md "wikilink")

          - 锥形果亚族：[雪胆属](../Page/雪胆属.md "wikilink")、[锥形果属](../Page/锥形果属.md "wikilink")、[绞股蓝属](../Page/绞股蓝属.md "wikilink")

          - 合子草亚族：[合子草属](../Page/合子草属.md "wikilink")、[假贝母属](../Page/假贝母属.md "wikilink")

          - 亚族：属、属、属、属、属
  - 葫芦亚科
      - [马㼎儿族](../Page/馬㼎兒族.md "wikilink")

          - 亚族：属、属、属、属、属、属、属、属、属、属、属、属、属、属、属、属

          - 亚族：属、属、属

          - 葫芦亚族：属、属、属、[马交儿属](../Page/马交儿属.md "wikilink")、、[老鼠拉冬瓜属](../Page/老鼠拉冬瓜属.md "wikilink")、属、[甜瓜属](../Page/甜瓜属.md "wikilink")、属、属、[红钮子属](../Page/红钮子属.md "wikilink")、属

          - 亚族：[茅瓜属](../Page/茅瓜属.md "wikilink")、属、属、属

      - 裂瓜族：[裂瓜属](../Page/裂瓜属.md "wikilink")

      - 族

          - 赤瓟亚族：[藏瓜属](../Page/藏瓜属.md "wikilink")、[罗汉果属](../Page/罗汉果属.md "wikilink")、[赤瓟属](../Page/赤瓟属.md "wikilink")、[苦瓜属](../Page/苦瓜属.md "wikilink")

          - 亚族：属

      - 族

          - 油渣果亚族：[油渣果属](../Page/油渣果属.md "wikilink")

          - 亚族：属、属

          - 栝楼亚族：[金瓜属](../Page/金瓜属.md "wikilink")、[栝楼属](../Page/栝楼属.md "wikilink")、属

          - 波棱瓜亚族：[波棱瓜属](../Page/波棱瓜属.md "wikilink")、、[三裂瓜属](../Page/三裂瓜属.md "wikilink")、[三棱瓜属](../Page/三棱瓜属.md "wikilink")

      - 冬瓜族

          - 冬瓜亚族：[冬瓜属](../Page/冬瓜属.md "wikilink")、属、属、[葫芦属](../Page/葫芦属.md "wikilink")、、[西瓜属](../Page/西瓜属.md "wikilink")、属、属、属、属、[红瓜属](../Page/红瓜属.md "wikilink")、[毒瓜属](../Page/毒瓜属.md "wikilink")、属、属、属、[喷瓜属](../Page/喷瓜属.md "wikilink")、[泻根属](../Page/泻根属.md "wikilink")
          - 丝瓜亚族：[丝瓜属](../Page/丝瓜属.md "wikilink")

      - 南瓜族：[南瓜属](../Page/南瓜属.md "wikilink")、属、属、属、属、属、属、属、属、属、属、属、属

      - 族

          - 辣子瓜亚族：属、属、属、属、属、属、属、属、属、属、[辣子瓜属](../Page/辣子瓜属.md "wikilink")、属
          - 棘瓜亚族：[棘瓜属](../Page/棘瓜属.md "wikilink")、属、属、属、[佛手瓜属](../Page/佛手瓜属.md "wikilink")、属、属

      - 不确定：属

参考：[Watson and
Dallwitz](http://delta-intkey.com/angio/www/cucurbit.htm)（2002年9月3日）

## 外部链接

  - [南京大学植物资源网葫芦科](http://www.nju.edu.cn/njuc/plantsweb/ZJ_families/huluke.htm)
  - [植物学网络课程](https://web.archive.org/web/20070115093022/http://www.nuist.edu.cn/courses/zwx/content/Botany2/jibenleiqun/gaodengzhiwu/beizi/szy-huluke.htm)
  - [中国作物种质信息网](https://web.archive.org/web/20070927192639/http://icgr.caas.net.cn/kp/%E8%91%AB%E8%8A%A6%E7%A7%91.htm)
  - \[<http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=family_latin='Cucurbitaceae>'
    葫蘆科 Cucurbitaceae\] 藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/葫芦科.md "wikilink")
[Category:葫芦目](../Category/葫芦目.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")