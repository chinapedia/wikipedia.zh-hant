**雞公嶺**（）是[香港](../Page/香港.md "wikilink")[新界西北的一座](../Page/新界.md "wikilink")[山峰](../Page/山峰.md "wikilink")，位於[元朗新市鎮和](../Page/元朗新市鎮.md "wikilink")[粉嶺／上水新市鎮之間](../Page/粉嶺／上水新市鎮.md "wikilink")，海拔585米。雞公嶺原名**圭角山**或**掛角山**。根據《[大清一統志](../Page/大清一統志.md "wikilink")》：「掛角山，在縣南三十里，兩峰突起如角，曰**大掛**，**小掛**，一名[牛潭山](../Page/牛潭山.md "wikilink")」。而在《[新安縣志](../Page/新安縣志.md "wikilink")》中，「掛角山一名桂角在縣東南四十里。山多老桂，兩峰對峙，其形如角，故名」。雞公嶺在[十八區區域行政上大部份屬於](../Page/香港十八區.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")，惟東北麓則屬於[北區](../Page/北區_\(香港\).md "wikilink")。山嶺大部份屬於[林村郊野公園](../Page/林村郊野公園.md "wikilink")，西北麓除外。

雞公嶺由兩個山峰組成，包括海拔585米的主峰（又稱為**大羅天**或**大掛**），以及其西海拔374米的副峰（又稱為**雞公山**、**金雞嶺**或**小掛**）。兩山山頂均設有[三角測量站](../Page/三角測量站.md "wikilink")（不過山峰的三角測量站是建於西面的572米山頭，而不是585米最高點），而副峰更設有[香港數碼地面電視廣播之輔助發射站](../Page/香港數碼地面電視廣播.md "wikilink")，服務[元朗東部](../Page/元朗.md "wikilink")、[錦田](../Page/錦田.md "wikilink")、[水邊村及](../Page/水邊村.md "wikilink")[輞井圍等地](../Page/輞井圍.md "wikilink")。在旅行界，雞公嶺主峰上的三個山頭由西至東細分為**大羅天**（572米）、**羅天頂**（585米）和**龍潭山**（550米），而雞公嶺向北伸展，有[牛潭山](../Page/牛潭山.md "wikilink")（337米）和[麒麟山](../Page/麒麟山.md "wikilink")（222米）。

該山山體廣闊，而其東、北和西方都沒有比它高的山，所以從元朗和上水等地都可清楚望見。站在山上，可鳥瞰[元朗平原和](../Page/元朗平原.md "wikilink")[米埔一帶的鄉郊景色與及](../Page/米埔.md "wikilink")[深圳市區與](../Page/深圳.md "wikilink")[蛇口一帶景色](../Page/蛇口.md "wikilink")。雞公嶺的山脊東西走向，東西兩端分別為[蕉徑和](../Page/蕉徑.md "wikilink")[逢吉鄉](../Page/逢吉鄉.md "wikilink")，亦是主要的登山入口。雖然山嶺屬於郊野公園範圍，但沒有任何郊遊設施。路徑除了沒有維修之外，更因為非法[越野電單車活動而變得打滑](../Page/越野電單車.md "wikilink")，所以只宜有經驗的遠足者前往。

雞公嶺的東南面有一走向和大小相約的[大刀屻和](../Page/大刀屻.md "wikilink")[北大刀屻](../Page/北大刀屻.md "wikilink")，兩組山峰之間的山谷則建有[粉錦公路](../Page/粉錦公路.md "wikilink")，沿途主要有[八鄉和](../Page/八鄉.md "wikilink")[錦田](../Page/錦田.md "wikilink")。

## 參考資料

  - 饒玖才，《香港的地名與地方歷史（下）》（天地圖書有限公司，ISBN 978-988-219-461-8），157頁。

  -
  - [元朗374號山轉播站加入網絡](http://www.tvb.com/affairs/faq/press/20040726.html)，電視廣播有限公司

  -
  -
  -
[Category:元朗區](../Category/元朗區.md "wikilink") [Category:北區
(香港)](../Category/北區_\(香港\).md "wikilink")