《**火影忍者疾風傳 激鬥忍者大戰！EX2**》 （日文：NARUTO -ナルト- 疾風伝
激闘忍者大戦\!EX2）為2007年於Wii遊戲主機平臺上所發行的一款格鬥遊戲，為《[火影忍者疾風傳
激鬥忍者大戰！EX](../Page/火影忍者疾風傳_激鬥忍者大戰！EX.md "wikilink")》的續作。

## 簡介

### 原著

原著[漫畫](../Page/漫畫.md "wikilink")《[火影忍者](../Page/火影忍者.md "wikilink")》（日文：ナルトNARUTO）於[集英社發行](../Page/集英社.md "wikilink")，[漫畫家](../Page/漫畫家.md "wikilink")[岸本齊史所著](../Page/岸本齊史.md "wikilink")。

### 遊戲模式

主要為單人劇情模式、對戰模式、練習模式。

單人劇情模式由[動畫](../Page/動畫.md "wikilink")[火影忍者疾風傳的劇情開始](../Page/火影忍者疾風傳.md "wikilink")，中途須經歷28個關卡，最後一戰為[漩渦鳴人與](../Page/漩渦鳴人.md "wikilink")[宇智波佐助的對決](../Page/宇智波佐助.md "wikilink")。

對戰模式可選擇一至四人的對戰，單人對戰模式之下，還多出了績分戰、時間戰、生存戰等三種模式。
雙人對戰模式之下，一樣有績分戰、時間戰、生存戰等三種模式

練習模式可選擇一對一練習，一對多練習（組手模式）與結印練習。

## 外部連結

  - [日本官方網站](https://web.archive.org/web/20080314063114/http://www.takaratomy.co.jp/products/gamesoft/naruto_ex2/)

[en:Naruto: Clash of Ninja
(series)](../Page/en:Naruto:_Clash_of_Ninja_\(series\).md "wikilink")
[fr:Naruto: Clash of
Ninja](../Page/fr:Naruto:_Clash_of_Ninja.md "wikilink") [it:Naruto:
Clash of Ninja](../Page/it:Naruto:_Clash_of_Ninja.md "wikilink")
[pt:Naruto: Clash of
Ninja](../Page/pt:Naruto:_Clash_of_Ninja.md "wikilink") [ja:NARUTO -ナルト-
疾風伝 激闘忍者大戦\!EX](../Page/ja:NARUTO_-ナルト-_疾風伝_激闘忍者大戦!EX.md "wikilink")

[Shippuuden Gekitou Ninja Taisen
EX2](../Category/火影忍者改编游戏.md "wikilink")
[N](../Category/2007年电子游戏.md "wikilink")
[N](../Category/Wii独占游戏.md "wikilink")