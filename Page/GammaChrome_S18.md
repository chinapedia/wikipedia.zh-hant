**GammaChrome S18**是[S3
Graphics第一款PCI](../Page/S3_Graphics.md "wikilink")-E顯示卡，採用[0.13微米製程](../Page/0.13微米製程.md "wikilink")。它有強勁2D和影片处理能力，而3D处理能力已接近當時的中階顯示卡。电視輸出方面，支持[Pure
HDTV技術](../Page/Pure_HDTV.md "wikilink")，成為S3顯示卡的一个特色。

## 規格

  - 核心频率：400MHz
  - 显存频率：800MHz
  - 显存頻宽：128bit
  - 有4个顶点处理单元。支援版本是[Vertex](../Page/Vertex.md "wikilink") 2.0。
  - 有4个像素着色处理单元。支援版本是[Pixel Shader](../Page/Pixel_Shader.md "wikilink")
    2.0。
  - 顏色精度是FP24（与[ATi](../Page/ATi.md "wikilink") [Radeon X
    Series相同](../Page/Radeon_X_Series.md "wikilink")）。
  - 支援PCT（Percentage Closer
    Filtering），用來繪出半影（介乎于黑色阴影和亮光之间的昏暗阴影）贴图，令阴影过渡的更柔和。
  - DST（Depth Stencil Texture），用來优化阴影渲染。

## Pure HDTV技術

直接輸出[YUV格式](../Page/YUV.md "wikilink")，不用转譯为[RGB格式](../Page/RGB.md "wikilink")。这样排除了转化过程导致的信号衰减问题，提高电視輸出畫質。

## Chromotion引擎

Chromotion引擎是根据微软[DirectX视频加速标准制定](../Page/DirectX.md "wikilink")，支持IDCT和动态补偿来帮助解码压缩字节流，用以加速DVD播放。

## ChroMAT

當显存不足时，能將系統記憶體，作為顯示記憶體，与[TurboCache和](../Page/TurboCache.md "wikilink")[HyperMemory相似](../Page/HyperMemory.md "wikilink")

## 關連項目

  - [威盛電子](../Page/威盛電子.md "wikilink")

## 另見

  - [威盛處理器列表](../Page/威盛處理器列表.md "wikilink")

## 參見

  - [威盛電子](../Page/威盛電子.md "wikilink")
  - [S3 Graphics](../Page/S3_Graphics.md "wikilink")
  - [VIA Envy](../Page/VIA_Envy.md "wikilink")
  - [ViRGE](../Page/S3_ViRGE.md "wikilink") 系列
  - [Savage](../Page/S3_Savage.md "wikilink") 系列
  - [DeltaChrome S8](../Page/DeltaChrome_S8.md "wikilink") 系列
  - [Chrome S20](../Page/ChromeS20_Series.md "wikilink") 系列
  - [Chrome 400](../Page/Chrome_400系列显卡.md "wikilink") 系列
  - [Chrome 500](../Page/Chrome_500系列显卡.md "wikilink") 系列

[de:S3
Chrome\#GammaChrome](../Page/de:S3_Chrome#GammaChrome.md "wikilink")

[Category:顯示卡](../Category/顯示卡.md "wikilink")
[Category:威盛電子](../Category/威盛電子.md "wikilink")