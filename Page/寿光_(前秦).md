**寿光**（355年六月-357年五月）是[十六国时期](../Page/十六国.md "wikilink")[前秦政权前秦厲王](../Page/前秦.md "wikilink")[苻生的](../Page/苻生.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

寿光三年六月[苻堅即位](../Page/苻堅.md "wikilink")，改元[永兴元年](../Page/永兴.md "wikilink")。

## 纪年

| 寿光                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 355年                           | 356年                           | 357年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [永和](../Page/永和_\(东晋\).md "wikilink")（345年-356年）：[东晋皇帝](../Page/东晋.md "wikilink")[晉穆帝司馬聃的年号](../Page/晉穆帝.md "wikilink")
      - [升平](../Page/升平.md "wikilink")（357年-361年）：[东晋皇帝](../Page/东晋.md "wikilink")[晉穆帝司馬聃的年号](../Page/晉穆帝.md "wikilink")
      - [建兴](../Page/建兴_\(前凉\).md "wikilink")：[前凉政权年号](../Page/前凉.md "wikilink")
      - [和平](../Page/和平_\(前凉\).md "wikilink")（354年-355年九月）：[前凉政权](../Page/前凉.md "wikilink")[张祚年号](../Page/张祚.md "wikilink")
      - [元玺](../Page/元玺.md "wikilink")（352年十一月-357年正月）：[前燕政权](../Page/前燕.md "wikilink")[慕容儁年号](../Page/慕容儁.md "wikilink")
      - [光寿](../Page/光寿.md "wikilink")（357年二月-359年）：[前燕政权](../Page/前燕.md "wikilink")[慕容儁年号](../Page/慕容儁.md "wikilink")
      - [建国](../Page/建国_\(代\).md "wikilink")（338年十一月-376年）：[代政权](../Page/代国.md "wikilink")[拓跋什翼犍年号](../Page/拓跋什翼犍.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:前秦年号](../Category/前秦年号.md "wikilink")
[Category:350年代中国政治](../Category/350年代中国政治.md "wikilink")