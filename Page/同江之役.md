**同江之役**是1929年[中东路事件中](../Page/中东路事件.md "wikilink")[苏联和](../Page/苏联.md "wikilink")[中国一次大规模武装冲突](../Page/中国.md "wikilink")。

1929年10月12日5时，苏联以800余骑兵、3000余步兵，配合以40余辆[坦克](../Page/坦克.md "wikilink")，向中国境内[同江守军发起进攻](../Page/同江.md "wikilink")。同时，苏联[阿穆尔河舰队出动](../Page/阿穆尔河.md "wikilink")8艘舰艇，由指挥官[斯加斯克率领](../Page/斯加斯克.md "wikilink")，进攻[尹祚干率领的中国](../Page/尹祚干.md "wikilink")[东北海军江防舰队](../Page/东北海军.md "wikilink")，由于开展地区位于黑龙江、[松花江与](../Page/松花江.md "wikilink")[乌苏里江的汇合处](../Page/乌苏里江.md "wikilink")，故又名**三江口中苏海战**。

苏联海军舰种齐全，火力强大，很快便压制住了中国方面的火力。但守军将领[沈鸿烈预先以一艘拖船](../Page/沈鸿烈.md "wikilink")“东乙号”搭载2门大炮埋伏于交战地区附近的芦苇丛中，偷袭苏军旗舰“雪尔诺夫”号得手，因而双方形成僵持。9时日出，苏军出动15架战机掌握了战场的制空权，战局急转直下。中方6艘江防舰艇5艘被击沉，1艘重伤退出战场。

苏方陆军随即渡过黑龙江，向同江进攻，中国守军有东北[海军陆战队第一大队和陆军一个营](../Page/中華民國海軍陸戰隊#東北海軍.md "wikilink")，坚守5个小时后被全歼。下午2时，苏联占领同江，次日退出，但保留了对[黑瞎子岛的占领](../Page/黑瞎子岛.md "wikilink")。

根据中方公布的战报，中方损失士兵900人左右，江防舰队失去战力，苏方损失士兵700人左右、舰队旗舰“雪尔诺夫”号等三艘被击沉、两艘重伤、飞机被击落两架。苏方战报未知。

## 外部链接

  - [香港商报·三江口中苏海战爆发](https://web.archive.org/web/20070219145942/http://www.cnwnc.com/20041012/ca1202961.htm)
  - [1929年10月12日苏军向张学良军队发动猛烈进攻](https://web.archive.org/web/20061025091710/http://www.wst.net.cn/history/10.12/4.htm)
  - [沈阳晚报·收回中东路权激怒苏联
    三江口之战张学良损失重](https://web.archive.org/web/20070311093455/http://www.syd.com.cn/sywb/2006-09/27/content_23794051.htm)

[Category:中东路事件战役](../Category/中东路事件战役.md "wikilink")
[Category:黑龙江民国时期战役](../Category/黑龙江民国时期战役.md "wikilink")
[Category:佳木斯军事史](../Category/佳木斯军事史.md "wikilink")
[Category:同江市](../Category/同江市.md "wikilink")
[Category:1929年10月](../Category/1929年10月.md "wikilink")