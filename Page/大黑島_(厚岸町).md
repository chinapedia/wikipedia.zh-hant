**大黑島**（）是[日本](../Page/日本.md "wikilink")[北海道東部](../Page/北海道.md "wikilink")[厚岸町床潭海岸外約](../Page/厚岸町.md "wikilink")4[公里的](../Page/公里.md "wikilink")[無人島](../Page/無人島.md "wikilink")，行政上隸屬[厚岸郡厚岸町管轄](../Page/厚岸郡.md "wikilink")。面積1.08[平方公里](../Page/平方公里.md "wikilink")，周長6.1公里，島上最高點海拔105公尺。目前全島皆為自然保護區。

島上為海鳥和[海豹的棲息地](../Page/海豹.md "wikilink")，並且是[白腰叉尾海燕最南方的棲息地](../Page/白腰叉尾海燕.md "wikilink")\[1\]，也在日本的唯一棲息地，；在1951年，已被日本指定為自然保護區。\[2\]

過去被原住民[阿伊努族稱為mosir](../Page/阿伊努族.md "wikilink")-ka或poro-mosir，在[阿伊努語的意思是大島](../Page/阿伊努語.md "wikilink")。[江戶時代後被來自](../Page/江戶時代.md "wikilink")[本州的漁民稱為大黑島](../Page/本州.md "wikilink")，附近為[鱈魚的漁產](../Page/鱈魚.md "wikilink")，並產有[昆布](../Page/昆布.md "wikilink")，因此在19世紀初，島上便設有小屋供漁民短期居住。

第二次世界大戰時，島上設置了[日軍基地](../Page/日軍.md "wikilink")，並將原本居住於島上的居民撤離，因此目前島上仍留有[高射炮陣地和](../Page/高射炮.md "wikilink")[日本帝國海軍的自殺式攻擊艇的停泊處等遺跡](../Page/日本帝國海軍.md "wikilink")。

目前島上無人定居，但仍有供漁民短期居住用的小屋。

## 参考資料

## 外部連結

  - [自然百景 - 大黑島](http://www.nhk.or.jp/sawayaka/daikoku.html)（NHK網頁）

  - [厚岸町官方網頁 -
    大黑島](https://web.archive.org/web/20070929143110/http://info.town.akkeshi.hokkaido.jp/pubsys/public/mu1/bin/view.rbz?cd=152)

  - [北海道的島 -
    大黑島](https://web.archive.org/web/20071004231508/http://www.h7.dion.ne.jp/~imagic/sima/hokkaido/daikokutou.html)

  - [大黑島的野生海豹觀察之旅](https://web.archive.org/web/20070807170550/http://marimo.xrea.jp/seal/daikoku/daioku0609.html)

[Category:北海道島嶼](../Category/北海道島嶼.md "wikilink")
[Category:厚岸町](../Category/厚岸町.md "wikilink")
[Category:太平洋島嶼](../Category/太平洋島嶼.md "wikilink")
[Category:日本無人島](../Category/日本無人島.md "wikilink")

1.  北海道的島 - 大黑島
    <http://www.h7.dion.ne.jp/~imagic/sima/hokkaido/daikokutou.html>
2.  厚岸町官方網頁 - 大黑島
    <http://info.town.akkeshi.hokkaido.jp/pubsys/public/mu1/bin/view.rbz?cd=152>