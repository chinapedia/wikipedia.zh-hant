**乙巳**为[干支之一](../Page/干支.md "wikilink")，顺序为第42个。前一位是[甲辰](../Page/甲辰.md "wikilink")，后一位是[丙午](../Page/丙午.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之乙屬陰之木](../Page/天干.md "wikilink")，[地支之巳屬陰之火](../Page/地支.md "wikilink")，是木生火相生。

## 乙巳年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")42年称“**乙巳年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘45，或年份數減3，除以10的餘數是2，除以12的餘數是6，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“乙巳年”：

<table>
<caption><strong>乙巳年</strong></caption>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/45年.md" title="wikilink">45年</a></li>
<li><a href="../Page/105年.md" title="wikilink">105年</a></li>
<li><a href="../Page/165年.md" title="wikilink">165年</a></li>
<li><a href="../Page/225年.md" title="wikilink">225年</a></li>
<li><a href="../Page/285年.md" title="wikilink">285年</a></li>
<li><a href="../Page/345年.md" title="wikilink">345年</a></li>
<li><a href="../Page/405年.md" title="wikilink">405年</a></li>
<li><a href="../Page/465年.md" title="wikilink">465年</a></li>
<li><a href="../Page/525年.md" title="wikilink">525年</a></li>
<li><a href="../Page/585年.md" title="wikilink">585年</a></li>
<li><a href="../Page/645年.md" title="wikilink">645年</a></li>
<li><a href="../Page/705年.md" title="wikilink">705年</a></li>
<li><a href="../Page/765年.md" title="wikilink">765年</a></li>
<li><a href="../Page/825年.md" title="wikilink">825年</a></li>
<li><a href="../Page/885年.md" title="wikilink">885年</a></li>
<li><a href="../Page/945年.md" title="wikilink">945年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1005年.md" title="wikilink">1005年</a></li>
<li><a href="../Page/1065年.md" title="wikilink">1065年</a></li>
<li><a href="../Page/1125年.md" title="wikilink">1125年</a></li>
<li><a href="../Page/1185年.md" title="wikilink">1185年</a></li>
<li><a href="../Page/1245年.md" title="wikilink">1245年</a></li>
<li><a href="../Page/1305年.md" title="wikilink">1305年</a></li>
<li><a href="../Page/1365年.md" title="wikilink">1365年</a></li>
<li><a href="../Page/1425年.md" title="wikilink">1425年</a></li>
<li><a href="../Page/1485年.md" title="wikilink">1485年</a></li>
<li><a href="../Page/1545年.md" title="wikilink">1545年</a></li>
<li><a href="../Page/1605年.md" title="wikilink">1605年</a></li>
<li><a href="../Page/1665年.md" title="wikilink">1665年</a></li>
<li><a href="../Page/1725年.md" title="wikilink">1725年</a></li>
<li><a href="../Page/1785年.md" title="wikilink">1785年</a></li>
<li><a href="../Page/1845年.md" title="wikilink">1845年</a></li>
<li><a href="../Page/1905年.md" title="wikilink">1905年</a></li>
<li><a href="../Page/1965年.md" title="wikilink">1965年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2025年.md" title="wikilink">2025年</a></li>
<li><a href="../Page/2085年.md" title="wikilink">2085年</a></li>
<li><a href="../Page/2145年.md" title="wikilink">2145年</a></li>
<li><a href="../Page/2205年.md" title="wikilink">2205年</a></li>
<li><a href="../Page/2265年.md" title="wikilink">2265年</a></li>
<li><a href="../Page/2325年.md" title="wikilink">2325年</a></li>
<li><a href="../Page/2385年.md" title="wikilink">2385年</a></li>
<li><a href="../Page/2445年.md" title="wikilink">2445年</a></li>
<li><a href="../Page/2505年.md" title="wikilink">2505年</a></li>
<li><a href="../Page/2565年.md" title="wikilink">2565年</a></li>
<li><a href="../Page/2625年.md" title="wikilink">2625年</a></li>
<li><a href="../Page/2685年.md" title="wikilink">2685年</a></li>
<li><a href="../Page/2745年.md" title="wikilink">2745年</a></li>
<li><a href="../Page/2805年.md" title="wikilink">2805年</a></li>
<li><a href="../Page/2865年.md" title="wikilink">2865年</a></li>
<li><a href="../Page/2925年.md" title="wikilink">2925年</a></li>
<li><a href="../Page/2985年.md" title="wikilink">2985年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 645年－日本[乙巳之變](../Page/乙巳之變.md "wikilink")
  - 1905年－大韓帝國[乙巳五賊](../Page/乙巳五賊.md "wikilink")

## 乙巳月

天干丁年和壬年，[立夏到](../Page/立夏.md "wikilink")[芒種的時間段](../Page/芒種.md "wikilink")，就是**乙巳月**：

  - ……
  - [1977年](../Page/1977年.md "wikilink")5月立夏到6月芒種
  - [1982年](../Page/1982年.md "wikilink")5月立夏到6月芒種
  - [1987年](../Page/1987年.md "wikilink")5月立夏到6月芒種
  - [1992年](../Page/1992年.md "wikilink")5月立夏到6月芒種
  - [1997年](../Page/1997年.md "wikilink")5月立夏到6月芒種
  - [2002年](../Page/2002年.md "wikilink")5月立夏到6月芒種
  - [2007年](../Page/2007年.md "wikilink")5月立夏到6月芒種
  - ……

## 乙巳日

## 乙巳時

天干丁日和壬日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）9時到11時，就是**乙巳時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/乙巳年.md "wikilink")