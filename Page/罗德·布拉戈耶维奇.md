**罗德·布拉戈耶维奇**（**Rod
Blagojevich**，）出生於[芝加哥](../Page/芝加哥.md "wikilink")，[美国政治家](../Page/美国.md "wikilink")，[美国民主党成员](../Page/美国民主党.md "wikilink")，曾任[美国众议员](../Page/美国众议员.md "wikilink")（1997年-2003年），第四十任[伊利诺伊州州长](../Page/伊利诺伊州州长.md "wikilink")（2003年至2009年1月29日）。布拉戈耶维奇是继[俄亥俄州州长](../Page/俄亥俄州州长.md "wikilink")[乔治·沃伊诺维奇之后第二位当选州长的](../Page/乔治·沃伊诺维奇.md "wikilink")[塞尔维亚裔美国人](../Page/塞尔维亚.md "wikilink")。

## 生平

2008年12月9日早上6:15分，布拉戈耶维奇因为涉嫌倒卖[巴拉克·奥巴马当选第](../Page/巴拉克·奥巴马.md "wikilink")44任[美国总统之后](../Page/美国总统.md "wikilink")，留下的伊利诺伊州[联邦参议员的空缺职位等腐败行为](../Page/美国参议院.md "wikilink")，被[联邦调查局特工人员在家中逮捕](../Page/联邦调查局.md "wikilink")。

2009年1月29日，[州參議院以](../Page/伊利諾伊州參議院.md "wikilink")59票同意、0票反對，通過彈劾案，使他成為美國逾20年來首位被彈劾下台的州長。州長一職由副州長[帕特·奎因繼任](../Page/帕特·奎因.md "wikilink")。\[1\]

## 内容扩充

<div class="references-small">

<references />

</div>

1.2010年参加《学徒名人版》第三季为自己的涉嫌腐败辩解

## 外部链接

  - [Illinois Governor Rod R.
    Blagojevich](https://web.archive.org/web/20100829055441/http://www.illinois.gov//gov//)
  - [Follow the Money — Rod
    Blagojevich](http://www.followthemoney.org/database/StateGlance/candidate.phtml?si=200614&c=417180)
  - [New York Times — Times Topics: Rod
    Blagojevich](http://topics.nytimes.com/top/reference/timestopics/people/b/rod_r_blagojevich/index.html)
  - [On the Issues — Rod
    Blagojevich](http://www.ontheissues.org/Rod_Blagojevich.htm)
  - [Project Vote Smart — Governor Rod R.
    Blagojevich](https://web.archive.org/web/20071104023516/http://www.vote-smart.org/bio.php?can_id=BC031097)
  - [Rod Blagojevich for Illinois](http://www.rodforillinois.com/)

[Category:伊利诺伊州州长](../Category/伊利诺伊州州长.md "wikilink")
[Category:美国民主党联邦众议员](../Category/美国民主党联邦众议员.md "wikilink")
[Category:塞爾維亞裔美國人](../Category/塞爾維亞裔美國人.md "wikilink")
[Category:美國政治醜聞](../Category/美國政治醜聞.md "wikilink")
[Category:美国民主党州长](../Category/美国民主党州长.md "wikilink")
[Category:美國籍囚犯及被拘留者](../Category/美國籍囚犯及被拘留者.md "wikilink")

1.