**石鑒**（），[字](../Page/表字.md "wikilink")**大郎**，一作**大朗**，[十六國時期](../Page/十六國.md "wikilink")[後趙國君主](../Page/後趙.md "wikilink")，為[石虎第三子](../Page/石虎.md "wikilink")，[石遵](../Page/石遵.md "wikilink")、[石世之兄](../Page/石世.md "wikilink")。後趙[建平三年](../Page/建平_\(石勒\).md "wikilink")（333年），後趙帝[石勒去世](../Page/石勒.md "wikilink")，石虎掌控大權，石鑒當時被封為代王。[建武三年](../Page/建武_\(石虎\).md "wikilink")（337年），石虎改稱[天王後](../Page/天王.md "wikilink")，被降封為義陽公。[太寧元年](../Page/太寧_\(石虎\).md "wikilink")（349年），石虎稱帝後，再被進封為義陽王。

石鑒在鎮守關中的時候，賦役繁重，文武官員只要頭髮長得比較長，就會被拔下來做帽帶，有剩下的會給宮女，曾因為這種荒唐的行徑，被石虎召回都城鄴城（今[河北](../Page/河北.md "wikilink")[臨漳縣](../Page/臨漳縣.md "wikilink")）。

太寧元年（349年），石遵廢皇帝石世，自登帝位，石鑒被命為[侍中](../Page/侍中.md "wikilink")、[太傅](../Page/太傅.md "wikilink")。石遵因[石閔有叛變之意](../Page/冉閔.md "wikilink")，召两位兄弟石鑒、乐平王[石苞與太后](../Page/石苞_\(后赵\).md "wikilink")[鄭櫻桃等人商議](../Page/鄭櫻桃.md "wikilink")，不料會後石鑒出賣其他人，將此事告知石閔。不久，石閔即率軍入宮，殺石遵，石鑒因此被擁立為帝。石遵被杀时说：“我尚且如此，石鉴能长久吗？”

然而石鑒登位後，處處受制於[大將軍石閔](../Page/大将军_\(东亚\).md "wikilink")，於是派石苞和将军李松、张才暗殺之，然而卻事敗，他装作自己不知情，杀死石苞三人；后又鼓励将军孙伏都攻打石闵，不果，又对石闵说孙伏都谋反，命石闵讨灭。石閔知道石鑒有殺己之意，遂頒殺胡令，被殺的人共有20餘萬；并软禁石鉴于御龙观，派尚书王简、少府王郁率数千人看守，用绳子把食物吊给他。

次年（350年），完全控制國政的石閔將後趙國號改為[魏](../Page/魏.md "wikilink")\[1\]（[衛](../Page/衛.md "wikilink")\[2\]），石閔也将包括自己在内的后赵皇族改姓为李，並改年號為[青龍](../Page/青龍.md "wikilink")。不久，石鑒為求擺脫控制，遂趁李閔外出作戰，秘密派[宦官告知在外的將軍抚军将军](../Page/宦官.md "wikilink")[张沈等](../Page/张沈.md "wikilink")，命他们趁虛攻都城鄴城，但宦官告知李閔此事，李閔因而回軍，石鑒遂被誅殺，在位僅103日。

## 注釋

<div class="references-small">

<references />

</div>

|- |width="30%" align="center" |前任：
**弟[石遵](../Page/石遵.md "wikilink")** |width="40%"
align="center"|[後趙皇帝](../Page/东晋十六国君主列表#後趙.md "wikilink")
<small>349-350 |width="30%" align="center" |繼任：
**弟[石祗](../Page/石祗.md "wikilink")** |- |width="30%" align="center"
|前任：
**--** |width="40%"
align="center"|[衛（魏）國皇帝](../Page/东晋十六国君主列表##衛.md "wikilink")
<small>350 |width="30%" align="center" |繼任：
**--**

[category:后赵皇帝](../Page/category:后赵皇帝.md "wikilink")

[Category:石姓](../Category/石姓.md "wikilink")
[Category:五胡十六國被殺害人物](../Category/五胡十六國被殺害人物.md "wikilink")
[Category:中國被弒帝王](../Category/中國被弒帝王.md "wikilink")
[Category:十六國郡公](../Category/十六國郡公.md "wikilink")

1.  《[十六國春秋](../Page/十六國春秋.md "wikilink")·卷二·后赵录》
2.  《[資治通鑑](../Page/資治通鑑.md "wikilink")·卷九八·晋紀二十》