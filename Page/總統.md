**總統**（），[日本及](../Page/日本.md "wikilink")[朝鲜半岛称为](../Page/朝鲜半岛.md "wikilink")“大總統”（；），是[共和制](../Page/共和制.md "wikilink")[國家的](../Page/國家.md "wikilink")[元首稱呼之一](../Page/國家元首.md "wikilink")。

## 名稱起源與演變

總統的[英文譯稱](../Page/英文.md "wikilink")「」，在英文中是指[領導者或](../Page/領導者.md "wikilink")[領導人](../Page/領導人.md "wikilink")（由[拉丁文](../Page/拉丁文.md "wikilink")
pre-“之前”+sedere“坐”而來），在[羅馬帝國時期為副執政官或行省總督的職位](../Page/羅馬帝國.md "wikilink")，並非[執政官](../Page/執政官.md "wikilink")。

但「President」不一定是政治領袖，某些機構或團體的[主席](../Page/主席.md "wikilink")、負責人，[公司的](../Page/公司.md "wikilink")[總裁和](../Page/總裁.md "wikilink")[大學的](../Page/大學.md "wikilink")[校長也使用這稱謂](../Page/校長.md "wikilink")。

## 中文舊稱

[清朝末年曾音译為](../Page/清朝.md "wikilink")“伯理玺天德”、「柏理璽天德」、「普利玺天德」\[1\]。

舊稱統兵的首領為總統。民國初年及北洋政府時期，皆冠以「大」字以區別或彰顯之；如臨時大總統、非常大總統。

1931年制訂的[中華民國訓政時期約法中](../Page/中華民國訓政時期約法.md "wikilink")，僅設國民政府主席。

1936年制訂的[五五憲草中](../Page/五五憲草.md "wikilink")，始稱「總統」，不再冠以「大」字，沿用至今。

## 各國制度

  - 在[總統制國家](../Page/總統制.md "wikilink")，總統兼任[政府首脑](../Page/政府首脑.md "wikilink")，直接領導行政機關，不設[總理](../Page/總理.md "wikilink")，由人民直接或間接選舉，如、、、、、（1966年以后）、（1978年以后）、（2018年以后）以及[拉丁美洲和](../Page/拉丁美洲.md "wikilink")[非洲的不少](../Page/非洲.md "wikilink")[國家](../Page/國家.md "wikilink")，皆實行[總統制](../Page/總統制.md "wikilink")。部分国家设总理，但實際上是總統制，如、。
  - 在[议会制國家](../Page/议会制.md "wikilink")，总统作为国家元首通常只是[虛位元首](../Page/虛位元首.md "wikilink")，真正行使行政職權的是[總理](../Page/總理.md "wikilink")（部分共和國或稱[部長會議主席](../Page/部長會議主席.md "wikilink")），總統由議會組成的選舉團選出，如、、、、、、、、。
  - 在[议会制國家](../Page/议会制.md "wikilink")，总统作为国家元首通常只是象徵性職位，但由[直選產生](../Page/直選.md "wikilink")，因此擁有部分外交权力，如、、、、、。
  - 在总統领導的[议会制國家](../Page/议会制.md "wikilink")，总统兼任[政府首脑](../Page/政府首脑.md "wikilink")，直接領導行政及立法機關，不設[總理](../Page/總理.md "wikilink")，總統由民選的[議會選出](../Page/議會.md "wikilink")，如、、。
  - 在[半总统制](../Page/半总统制.md "wikilink")（又称[雙首長制](../Page/雙首長制.md "wikilink")）國家，总统与[总理职权的划分](../Page/总理.md "wikilink")，依其國情的不同，实行「合法性二元化」措施，总理代表[行政机关向议会负责](../Page/行政机关.md "wikilink")，但总统的权力普遍较总理大，總統擁有国防及外交权力，且擁有解散国会的权力。最典型的例子是，其他如、、、、、、等也可视为半总统制。
  - 在[委员制国家](../Page/委员制.md "wikilink")，“[瑞士聯邦主席](../Page/瑞士聯邦主席.md "wikilink")”（也有翻译成總統）在法律上与[总统制类似](../Page/总统制.md "wikilink")，是国家元首和政府首脑；但其实际权力及地位都不比其他委員高，如同[议会制下的國家元首](../Page/议会制.md "wikilink")。现今实行这一制度的只有和两国。
  - 在[社会主义国家](../Page/社会主义国家.md "wikilink")，国家元首一般不称总统，因为[一党执政的原因](../Page/一党执政.md "wikilink")，其地位和权力比不上[执政党的](../Page/执政党.md "wikilink")[最高领导人](../Page/政党领袖.md "wikilink")（[总书记](../Page/总书记.md "wikilink")、[第一书记](../Page/第一书记.md "wikilink")，除非兼任），部分國家元首甚至是[虚位元首](../Page/虚位元首.md "wikilink")。、、称其国家元首为[国家主席](../Page/国家主席.md "wikilink")，、、等称[国务委员会主席](../Page/国务委员会主席.md "wikilink")。曾经使用“总统”为国家元首名称的社会主义国家有（1959-1976）、（1990-1991）、（1948-1989）、（1949-1960）、（1974-1989）、（1945-1990）和（1947-1952、1989-1990）。

<!-- end list -->

  - 在不同國家設定上，總統職位可任職一定年期，可連任或不可連任，或任職一定年期後不能再出任。

### 历史上社会主义国家的国家元首为总统的列表

<table>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>知名领袖</p></th>
<th><p>职务名称</p></th>
<th><p>职务开始</p></th>
<th><p>职务终结</p></th>
<th><p>所属国家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:RIAN_archive_850809_General_Secretary_of_the_CPSU_CC_M._Gorbachev_(crop).jpg" title="fig:RIAN_archive_850809_General_Secretary_of_the_CPSU_CC_M._Gorbachev_(crop).jpg">RIAN_archive_850809_General_Secretary_of_the_CPSU_CC_M._Gorbachev_(crop).jpg</a></p></td>
<td><p><a href="../Page/米哈伊尔·戈尔巴乔夫.md" title="wikilink">米哈伊尔·戈尔巴乔夫</a></p></td>
<td><p><a href="../Page/苏联总统.md" title="wikilink">苏联总统</a></p></td>
<td><p>1990年3月15日</p></td>
<td><p>1991年12月25日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nicolae_Ceaușescu.jpg" title="fig:Nicolae_Ceaușescu.jpg">Nicolae_Ceaușescu.jpg</a></p></td>
<td><p><a href="../Page/尼古拉·齐奥塞斯库.md" title="wikilink">尼古拉·齐奥塞斯库</a></p></td>
<td><p><a href="../Page/罗马尼亚总统.md" title="wikilink">罗马尼亚总统</a></p></td>
<td><p>1974年3月28日</p></td>
<td><p>1989年12月22日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Siabar_003.jpg" title="fig:Siabar_003.jpg">Siabar_003.jpg</a></p></td>
<td><p><a href="../Page/穆罕默德·西亚德·巴雷.md" title="wikilink">穆罕默德·西亚德·巴雷</a></p></td>
<td><p><a href="../Page/索马里总统.md" title="wikilink">索马里总统</a></p></td>
<td><p>1969年10月21日</p></td>
<td><p>1991年1月27日</p></td>
<td><p><a href="../Page/索马里民主共和国.md" title="wikilink">索马里民主共和国</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Mathieu_Kérékou_2006Feb10.JPG" title="fig:Mathieu_Kérékou_2006Feb10.JPG">Mathieu_Kérékou_2006Feb10.JPG</a></p></td>
<td><p><a href="../Page/马蒂厄·克雷库.md" title="wikilink">马蒂厄·克雷库</a></p></td>
<td><p><a href="../Page/贝宁总统.md" title="wikilink">贝宁总统</a></p></td>
<td><p>1975年11月30日</p></td>
<td><p>1980年9月29日（1990年3月1日恢复）</p></td>
<td><p><a href="../Page/贝宁人民共和国.md" title="wikilink">贝宁人民共和国</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Mieczysław_Rakowski.jpg" title="fig:Mieczysław_Rakowski.jpg">Mieczysław_Rakowski.jpg</a></p></td>
<td><p><a href="../Page/米奇斯瓦夫·拉科夫斯基.md" title="wikilink">米奇斯瓦夫·拉科夫斯基</a></p></td>
<td><p><a href="../Page/波兰人民共和国总统.md" title="wikilink">波兰人民共和国总统</a></p></td>
<td><p>1989年7月19日</p></td>
<td><p>1989年12月31日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gustáv_Husák_-_oříznuto.JPG" title="fig:Gustáv_Husák_-_oříznuto.JPG">Gustáv_Husák_-_oříznuto.JPG</a></p></td>
<td><p><a href="../Page/古斯塔夫·胡萨克.md" title="wikilink">古斯塔夫·胡萨克</a></p></td>
<td><p><a href="../Page/捷克斯洛伐克总统.md" title="wikilink">捷克斯洛伐克总统</a></p></td>
<td><p>1948年6月14日</p></td>
<td><p>1989年12月29日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Fotothek_df_roe-neg_0002793_004_Portrait_Wilhelm_Piecks_im_Publikum_der_Bachfeier.jpg" title="fig:Fotothek_df_roe-neg_0002793_004_Portrait_Wilhelm_Piecks_im_Publikum_der_Bachfeier.jpg">Fotothek_df_roe-neg_0002793_004_Portrait_Wilhelm_Piecks_im_Publikum_der_Bachfeier.jpg</a></p></td>
<td><p><a href="../Page/威廉·皮克.md" title="wikilink">威廉·皮克</a></p></td>
<td><p><a href="../Page/东德领导人列表.md" title="wikilink">东德总统</a></p></td>
<td><p>1949年10月7日</p></td>
<td><p>1960年9月12日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_President_of_Cuba.svg" title="fig:Flag_of_the_President_of_Cuba.svg">Flag_of_the_President_of_Cuba.svg</a></p></td>
<td><p><a href="../Page/奥斯瓦尔多·多尔蒂科斯·托拉多.md" title="wikilink">奥斯瓦尔多·多尔蒂科斯·托拉多</a></p></td>
<td><p><a href="../Page/古巴总统.md" title="wikilink">古巴总统</a></p></td>
<td><p>1959年7月18日</p></td>
<td><p>1976年12月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Josip_Broz_Tito_uniform_portrait.jpg" title="fig:Josip_Broz_Tito_uniform_portrait.jpg">Josip_Broz_Tito_uniform_portrait.jpg</a></p></td>
<td><p><a href="../Page/约瑟普·布罗兹·铁托.md" title="wikilink">约瑟普·布罗兹·铁托</a></p></td>
<td><p><a href="../Page/南斯拉夫总统.md" title="wikilink">南斯拉夫总统</a></p></td>
<td><p>1945年11月29日</p></td>
<td><p>1980年5月4日</p></td>
<td><p><a href="../Page/南斯拉夫社会主义联邦共和国.md" title="wikilink">南斯拉夫社会主义联邦共和国</a></p></td>
</tr>
</tbody>
</table>

## 脚注

## 相關條目

  - [國家元首](../Page/國家元首.md "wikilink")
  - [政府首腦](../Page/政府首腦.md "wikilink")
  - [總統制](../Page/總統制.md "wikilink")
  - [半總統制](../Page/半總統制.md "wikilink")
  - [國家主席](../Page/國家主席.md "wikilink")
  - [國務委員會主席](../Page/國務委員會主席.md "wikilink")
  - [總理](../Page/總理.md "wikilink")

[总统](../Category/总统.md "wikilink")
[Category:政治制度](../Category/政治制度.md "wikilink")

1.  清·薛福成，《出使四国日记·光绪十六年十二月二十九日》：「美洲各国及欧洲之瑞士与法国皆民主之国也，其政权全在议院，而伯理璽天德无权焉。」；严復《＜法意＞按语》：「虽伯理由于公推，议院有其聚散，而精神之贯彻始终则一而已。」