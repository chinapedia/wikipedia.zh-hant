**屎星**位於[天鴿座的一顆恆星](../Page/天鴿座.md "wikilink")，在[拜耳命名法中是](../Page/拜耳命名法.md "wikilink")**天鴿座μ星**（**Mu
Columbae**，μ
Col），是能以肉眼看見的少數O型星之一。已知這恆星距離[太陽系約](../Page/太陽系.md "wikilink")1,300[光年](../Page/光年.md "wikilink")，但可能有數百年的誤差。在中國的[二十八宿中屬](../Page/二十八宿.md "wikilink")[參宿](../Page/參宿.md "wikilink")，代表[星官](../Page/星官.md "wikilink")[廁裡的](../Page/廁一.md "wikilink")[糞便](../Page/糞便.md "wikilink")。

這是相對而言自轉較快的一顆恆星，大約每1.5天自轉一周。相較於[太陽](../Page/太陽.md "wikilink")，直徑是屎星的22%，自轉一周卻要25.4天。但這種自轉速度是此類恆中較典型的。

基於[自行和](../Page/自行.md "wikilink")[徑向速度的測量](../Page/徑向速度.md "wikilink")，天文學家知道，這顆星和[御夫座AE正以超過](../Page/御夫座AE.md "wikilink")200Km/S的速度在相互遠離。它們共同的起源點交會在[獵戶四邊形星團中的](../Page/獵戶四邊形星團.md "wikilink")[伐三](../Page/伐三.md "wikilink")。最可能的情況是在250萬年前，有兩對[聯星的交互作用](../Page/聯星.md "wikilink")（碰撞），造成了這些速逃星從徑向上不同的點拋射而逃逸\[1\]。

## 語源

在[中國天文學](../Page/中國天文學.md "wikilink")，天鴿座μ稱為屎，[漢語拼音](../Page/漢語拼音.md "wikilink")：Shǐ，意思是"排泄物"或"糞便"，因為這顆星是單獨一顆成為一個星官，位置在[廁的下方](../Page/廁.md "wikilink")，屬於[參宿](../Page/參宿.md "wikilink")\[2\]\[3\]。

這顆星與[大犬座ζ](../Page/大犬座ζ.md "wikilink")、[大犬座λ](../Page/大犬座λ.md "wikilink")、[天鴿座γ](../Page/天鴿座γ.md "wikilink")、[天鴿座δ](../Page/天鴿座δ.md "wikilink")、[天鴿座θ](../Page/天鴿座θ.md "wikilink")、[天鴿座κ](../Page/天鴿座κ.md "wikilink")、[天鴿座λ](../Page/天鴿座λ.md "wikilink")
、[天鴿座ξ是猿](../Page/天鴿座ξ.md "wikilink")（**Al Ḳurūd** 、**ألقرد** -
*al-qird*）\[4\]。

## 參考資料

## 進階讀物

  -
  -
[Category:天鴿座](../Category/天鴿座.md "wikilink")
[Category:O-型主序星](../Category/O-型主序星.md "wikilink")
[Category:速逃星](../Category/速逃星.md "wikilink")
[Category:疑似變星](../Category/疑似變星.md "wikilink")
[天鴿座μ](../Category/拜耳天體.md "wikilink")
[1996](../Category/HR天體.md "wikilink")
[038666](../Category/HD天體.md "wikilink")
[Category:BD、CD和CP天体](../Category/BD、CD和CP天体.md "wikilink")
[027204](../Category/伊巴谷天體.md "wikilink")

1.
2.   [AEEA (Activities of Exhibition and Education in Astronomy)
    天文教育資訊網 2006 年 7 月 15
    日](http://aeea.nmns.edu.tw/2006/0607/ap060715.html)
3.  \[<http://penelope.uchicago.edu/Thayer/E/Gazetteer/Topics/astronomy/_Texts/secondary/ALLSTA/Columba_Noae>\*.html
    Richard Hinckley Allen: Star Names — Their Lore and Meaning:
    Columbae\]
4.