[HK_ChikKwaiStudyHall_2011.JPG](https://zh.wikipedia.org/wiki/File:HK_ChikKwaiStudyHall_2011.JPG "fig:HK_ChikKwaiStudyHall_2011.JPG")
**植桂書室**是[香港一座建於](../Page/香港.md "wikilink")[清朝的歷史建築](../Page/清朝.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[元朗](../Page/元朗.md "wikilink")[八鄉](../Page/八鄉.md "wikilink")，屬香港典型的中國傳統書室，同時作祭祀祖先和教育村中子弟用途。於2007年被[古物古蹟辦事處列為](../Page/古物古蹟辦事處.md "wikilink")[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")。

## 歷史

植桂書室早於1899年[英國租借](../Page/英國.md "wikilink")[新界前便已存在](../Page/新界.md "wikilink")，估計建於[清朝中葉](../Page/清朝.md "wikilink")。書室由[八鄉](../Page/八鄉.md "wikilink")[上村的](../Page/上村_\(香港\).md "wikilink")[黎氏族人黎金泰所建](../Page/黎姓.md "wikilink")，作為教育鄉村子弟的用途。1930年代，該建築改作[祭祖之用](../Page/祭祖.md "wikilink")，並兼作村民舉行會議、春秋二祭及婚嫁儀式等用途。[香港重光後](../Page/香港重光.md "wikilink")，該建築開辦「永慶學校」提供現代[小學教育](../Page/小學.md "wikilink")，後來改為[幼稚園](../Page/幼稚園.md "wikilink")，最後約於1960年代停辦後一直空置。

2007年5月4日，植桂書室被列為[香港三級歷史建築](../Page/香港三級歷史建築.md "wikilink")。由於日久失修，古物古蹟辦事處會先進行修復工程，之後才開放給市民參觀。2007年6月29日，植桂書室獲升級為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")\[1\]。[康樂及文化事務署擬斥資](../Page/康樂及文化事務署.md "wikilink")450萬元，重建書室屋頂及書室兩側的廂房\[2\]。

## 建築

植桂書室屬於「兩進一天井」設計，為清朝傳統的建築設計。建築物的正面採用了[花崗石塊和](../Page/花崗石.md "wikilink")[青磚築砌而成](../Page/青磚.md "wikilink")。而書室內保存了大量原有的建築構件，包括位於[屋脊和石](../Page/屋脊.md "wikilink")[牆上的](../Page/牆.md "wikilink")[灰塑](../Page/灰塑.md "wikilink")，以及[木刻和](../Page/木刻.md "wikilink")[壁畫等](../Page/壁畫.md "wikilink")。由於書室在建成後一直未有翻新或改建，因此具有高度的歷史價值。

## 修復

被列為法定古蹟的元朗八鄉上村植桂書室，由於日久失修變得破舊，古物古蹟辦事處已完成其保育研究及地圖測量，展開修復工程，於回復舊觀後可開放予市民參觀\[3\]，植桂書室的全面復修工程於2010年底竣工。

## 交通

  - [九龍巴士64K線](../Page/九龍巴士64K線.md "wikilink")：來往[元朗（西）及](../Page/元朗（西）巴士總站.md "wikilink")[大埔墟鐵路站](../Page/大埔墟鐵路站.md "wikilink")（經[錦上路鐵路站及](../Page/錦上路鐵路站.md "wikilink")[太和鐵路站](../Page/太和鐵路站.md "wikilink")）
  - 乘坐任何途經[大欖隧道巴士在大欖隧道站下車轉乘](../Page/大欖隧道.md "wikilink")[九龍巴士251A線](../Page/九龍巴士251A線.md "wikilink")：來往[八鄉路及](../Page/八鄉路.md "wikilink")[上村](../Page/上村.md "wikilink")（循環線）
  - 兩線途經[黎屋村](../Page/黎屋村.md "wikilink")

## 參考

<div class="references-column">

<references/>

</div>

## 外部連結

  - [康樂及文化事務署 -
    植桂書室](https://web.archive.org/web/20070830040848/http://www.lcsd.gov.hk/CE/Museum/Monument/b5/monuments_82.php)
  - [香港歷史文物：植桂書室](http://www.heritage.gov.hk/tc/buildings/monuments_82.htm)

[Category:香港法定古蹟](../Category/香港法定古蹟.md "wikilink")
[Category:八鄉](../Category/八鄉.md "wikilink")
[Category:香港書塾](../Category/香港書塾.md "wikilink")

1.
2.
3.  [植桂書室快展開修復](http://orientaldaily.on.cc/new/new_a29cnt.html?pubdate=20080123)