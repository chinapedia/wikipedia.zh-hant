**泉**（）是[法国画家](../Page/法国.md "wikilink")[让·奥古斯特·多米尼克·安格尔于](../Page/让·奥古斯特·多米尼克·安格尔.md "wikilink")1856年创作的一幅[油画](../Page/油画.md "wikilink")。现藏于法国[巴黎的](../Page/巴黎.md "wikilink")[奧塞博物館](../Page/奧塞博物館.md "wikilink")。画家1820年在[佛罗伦萨时就开始绘制这幅作品了](../Page/佛罗伦萨.md "wikilink")，直到1856年76岁时才完成\[1\]\[2\]\[3\]。

画中少女全裸伫立，手捧水罐。朝下的罐口流淌着清水。少女的身体丰满细润，面容悠闲端庄，处处散发着青春的气息和生命的活力。

## 参考文献

## 参见

  - [泉 (杜尚)](../Page/噴泉_\(杜象\).md "wikilink")

[Category:讓·奧古斯特·多米尼克·安格爾畫作](../Category/讓·奧古斯特·多米尼克·安格爾畫作.md "wikilink")
[Category:奧塞美術館藏品](../Category/奧塞美術館藏品.md "wikilink")
[Category:1856年畫作](../Category/1856年畫作.md "wikilink")

1.
2.
3.