[WiFi_Logo.svg](https://zh.wikipedia.org/wiki/File:WiFi_Logo.svg "fig:WiFi_Logo.svg")

**Wi-Fi**（\[1\]\[2\]\[3\]），在[中文裡又稱作](../Page/中文.md "wikilink")「無線熱點」，是[Wi-Fi聯盟製造商的](../Page/Wi-Fi聯盟.md "wikilink")[商標做為產品的品牌認證](../Page/商標.md "wikilink")，是一個建立於[IEEE
802.11標準的](../Page/IEEE_802.11.md "wikilink")[無線局域網技術](../Page/無線局域網.md "wikilink")。基於兩套系統的密切相關，也常有人把Wi-Fi當做IEEE
802.11標準的同义術語。「Wi-Fi」常被寫成「**WiFi**」或「**Wifi**」，但是它們並沒有被Wi-Fi聯盟認可。

並不是每樣符合IEEE 802.11的產品都申請Wi-Fi聯盟的認證，相對地缺少Wi-Fi認證的產品並不一定意味著不兼容Wi-Fi設備。

IEEE
802.11的設備已安裝在市面上的許多產品，如：[個人電腦](../Page/個人電腦.md "wikilink")、[遊戲機](../Page/遊戲機.md "wikilink")、[MP3播放器](../Page/MP3播放器.md "wikilink")、[智慧型手機](../Page/智慧型手機.md "wikilink")、[平板電腦](../Page/平板電腦.md "wikilink")、[印表機](../Page/印表機.md "wikilink")、[筆記型電腦以及其他可以無線上網的週邊設備](../Page/筆記型電腦.md "wikilink")。

Wi-Fi聯盟成立于1999年，當時的名稱叫做Wireless Ethernet Compatibility
Alliance（WECA）。在2002年10月，正式改名為Wi-Fi Alliance。

## 由來

Wi-Fi這個術語被人們普遍誤以為是指-{zh-cn:无线保真;zh-tw:無線傳真;}-（Wireless
Fidelity）\[4\]，類似歷史悠久的-{zh-cn:音频;zh-tw:音響;}-設備分類：-{zh-cn:长期高保真;zh-tw:高傳真;}-（1930年開始採用）或Hi-Fi（1950年開始採用）。即便是Wi-Fi聯盟本身也經常在新聞稿和文件中使用「Wireless
Fidelity」這個詞，Wi-Fi還出現在[ITAA的一個論文中](../Page/ITAA.md "wikilink")。事實上，Wi-Fi一詞是沒有任何意義，也沒有全寫的。\[5\]

[IEEE
802.11第一个版本发表于](../Page/IEEE_802.11.md "wikilink")1997年，其中定义了[-{zh-cn:介质访问接入控制层;zh-tw:媒體存取控制;}-和](../Page/介质访问控制.md "wikilink")[物理层](../Page/物理层.md "wikilink")。物理层定义了在2.4GHz的[ISM频段上的两种无线调频方式和一种红外線传输的方式](../Page/ISM频段.md "wikilink")，总数据传输速率设计为2Mbit/s。两个设备之间的通信可以自由直接（[ad
hoc](../Page/Ad_hoc網路.md "wikilink")）的方式进行，也可以在基站（Base
Station，BS）或者访问点（Access
Point，AP）的协调下进行。1999年加上了两个补充版本：802.11a定义了一个在5GHz
ISM频段上的数据传输速率可达54Mbit/s的物理层，802.11b定义了一个在2.4GHz的ISM频段上但数据传输速率高达11Mbit/s的物理层。

2.4GHz的[ISM频段为世界上绝大多数国家通用](../Page/ISM频段.md "wikilink")，因此802.11b得到了最为广泛的应用。[苹果公司把自己开发的](../Page/苹果公司.md "wikilink")802.11标准起名叫[AirPort](../Page/AirPort.md "wikilink")。1999年工业界成立了[Wi-Fi联盟](../Page/Wi-Fi联盟.md "wikilink")，致力解决符合802.11标准的产品的生产和设备兼容性问题。Wi-Fi為制定802.11無線網路的組織，並非代表[無線網路](../Page/無線網路.md "wikilink")。

### 802.11标准和补充

  - 802.11，1997年，原始标准（2Mbit/s，2.4GHz頻道）。
  - 802.11a，1999年，物理层补充（54Mbit/s，5GHz頻道）。
  - 802.11b，1999年，物理层补充（11Mbit/s，2.4GHz頻道）。\*

<!-- end list -->

  - 下列常見的標準于2007年收录在802.11

<!-- end list -->

  - 802.11c，符合802.1D的媒体-{zh-cn:接入控制;zh-tw:存取控制;}-层（MAC）桥接（MAC Layer
    Bridging）。
  - 802.11d，根据各国无线电规定做的调整。
  - 802.11e，对服务等级（Quality of Service, QoS）的支持。
  - 802.11f，基站的互连性（Interoperability）。
  - 802.11g，物理层补充（54Mbit/s，2.4GHz頻道）。
  - 802.11h，DFS /TPC，信道（5GHz频段）。
  - 802.11i，安全和鉴权（Authentication）方面的补充。

<!-- end list -->

  - 2008年以後的標準

<!-- end list -->

  - 802.11k
  - 802.11n，2009年，導入多重輸入輸出（MIMO）和40Mbit信道宽度（HT40）技術，基本上是802.11a/g的延伸版。
  - 802.11r
  - 802.11t
  - 802.11u
  - 802.11v
  - 802.11w
  - 802.11z

<!-- end list -->

  - 2011年以後的標準

<!-- end list -->

  - 802.11ac，物理层补充（8×8 MIMO，256QAM，80MHz信道宽度）。

除了上面的IEEE标准，另外有一个称为IEEE 802.11b+的技术，通过PBCC技术（Packet Binary Convolutional
Code）在IEEE802.11b（2.4GHz频段）基础上提供22Mbit/s的数据传输速率。但这事实上并不是IEEE的公开标准，而是一项产权私有的技术（产权属於[美国](../Page/美国.md "wikilink")[德州仪器](../Page/德州仪器.md "wikilink")，Texas
Instruments）。也有一些稱為802.11g+的技術，在[IEEE
802.11g的基礎上提供](../Page/IEEE_802.11g-2003.md "wikilink")108Mbit/s的傳輸速率，跟802.11b+一樣，同樣是非標準技術，由無線網絡晶片生產商[Atheros所提倡的則為SuperG](../Page/Atheros.md "wikilink")。

## 年代

Wi-Fi可分為六代。\[6\]由於[ISM頻段中的](../Page/ISM頻段.md "wikilink")2.4GHz頻段被廣泛使用，例如[微波爐](../Page/微波爐.md "wikilink")、[藍牙](../Page/藍牙.md "wikilink")，它們會干擾WiFi，令速度減慢，5GHz干擾則較小。雙頻路由器可同時使用2.4GHz和5GHz，裝置則只能使用某一個頻段。

  - 第一代
    [802.11](../Page/IEEE_802.11.md "wikilink")：1997年制定，只使用2.4GHz，最快2Mbit/s
  - 第二代
    [802.11b](../Page/IEEE_802.11b-1999.md "wikilink")，只使用2.4GHz，最快11Mbit/s，正逐漸淘汰
  - 第三代
    [802.11g](../Page/IEEE_802.11g-2003.md "wikilink")/[a](../Page/IEEE_802.11a-1999.md "wikilink")，分別使用2.4GHz和5GHz，最快54Mbit/s
  - 第四代 [802.11n](../Page/IEEE_802.11n.md "wikilink")（Wi-Fi
    4），可使用2.4GHz或5GHz，20和40MHz[頻寬下最快](../Page/頻寬.md "wikilink")72和150Mbit/s
  - 第五代 [802.11ac](../Page/IEEE_802.11ac.md "wikilink")（Wi-Fi
    5），可使用2.4GHz，5GHz
  - 第六代 [802.11ax](../Page/IEEE_802.11ax.md "wikilink")（Wi-Fi
    6），可使用2.4GHz，5GHz (未來可能納入6GHz)

\[7\] \[8\]

## 用途

### 網路連接

  - 具Wi-Fi功能的設備：如個人電腦，遊戲機，智慧型手機或數位音訊播放器可以從範圍內的無線網絡連接到網路。一個或多個（[互聯](../Page/互聯.md "wikilink")）存取點–稱之為[熱點](../Page/熱點.md "wikilink")
    -
    可以組成一個面積由幾間房間到數平方英里範圍的上網空間，覆蓋的面積大小可能取決於存取點的重疊範圍。Wi-Fi技術已被用於無線網狀網路，例如，在倫敦、英國，除了家裡和辦公室使用外，Wi-Fi無線網絡還可以提供免費使用的公開熱點和各種商業服務。
  - 組織和企業：例如機場、飯店、餐廳等經常提供来访者免費熱點，以吸引或協助客戶。商家會依愛好者希望提供服務，有時也為在某些領域推銷企業而提供免費的Wi-Fi站點。目前在中国大陆和台灣，许多大型飯店和商场的内部，都会提供免费Wi-Fi热点供来访者使用[互联网](../Page/互联网.md "wikilink")，一些城市或区域如北京市海淀区还提供了区域性的政府免费无线网“海淀区免费无线网络”[1](http://www.bjhd.gov.cn/zt/wxsw/)。截至2008年為止，Wi-Fi的（Muni-Fi）的項目已超過300個城市參與。2010年捷克共和國已有1150家Wi-Fi網路服務供應商。
  - [路由器](../Page/路由器.md "wikilink")，結合了數據機和Wi-Fi存取點，通常設置在家裡房間、飯店客房或其他场所，可以提供互聯網連接以及和互聯網絡的所有設備連接（無線或有線）。但因为家用无线路由器的功率较小，所以其信号覆盖范围、信号强度也較小。隨著MiFi和WiBro（攜帶式Wi-Fi路由器）的出現，可以很容易地建立自己的Wi-Fi熱點，透過電信網路連接到網路。現在，許多移动电话（智能手机）也可充当小型无线路由器，供周围的设备連接互联网。
  - 也可以使用ad-hoc模式，不經路由器而是客戶端直接連接到另一個客戶端的Wi-Fi設備。Wi-Fi無線覆蓋範圍，也包含了浴室、廚房和花園等地，使網路無所不在。

### 城市Wi-Fi覆蓋

[Metro_Wireless_Node.jpg](https://zh.wikipedia.org/wiki/File:Metro_Wireless_Node.jpg "fig:Metro_Wireless_Node.jpg")
[Toronto_WiFi.jpg](https://zh.wikipedia.org/wiki/File:Toronto_WiFi.jpg "fig:Toronto_WiFi.jpg")

21世紀初期，世界各地的許多城市都宣布計劃构建全市Wi-Fi網絡。但這比最初發起人設想的更為困難，結果這些項目大多被取消或無限期擱置。但是有幾個是成功的，例如在2005年，美國加州[森尼维尔成為在美國的第一個提供全市免費Wi](../Page/森尼维尔_\(加利福尼亚州\).md "wikilink")-Fi的城市。2010年5月，[倫敦市長](../Page/倫敦.md "wikilink")[鮑里斯·約翰遜承諾到](../Page/鮑里斯·約翰遜.md "wikilink")2012年伦敦Wi-Fi普及，几个自治市镇包括[威斯敏斯特和](../Page/威斯敏斯特.md "wikilink")[伊斯灵顿已经有了广泛的Wi](../Page/伊斯灵顿.md "wikilink")-Fi覆盖。全球已建和建造中的Wi-Fi城市已經超過500個，其中覆蓋率最高者為[台北市](../Page/台北市.md "wikilink")，其已達到全市已有4000個無線存取點（AP,
Access
Point），未來將至10,000個，覆蓋率達到90%，全球主要的[大都市的重要公共場所多已有Wi](../Page/大都市.md "wikilink")-Fi技術，如[上海](../Page/上海.md "wikilink")、[台北](../Page/台北.md "wikilink")、[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[漢堡](../Page/漢堡.md "wikilink")、[巴黎](../Page/巴黎.md "wikilink")、[華盛頓](../Page/華盛頓.md "wikilink")、[伦敦](../Page/伦敦.md "wikilink")、[纽约等](../Page/纽约.md "wikilink")。

### 校園的Wi-Fi覆蓋

[卡內基美隆大學於](../Page/卡內基美隆大學.md "wikilink")1994年在其[匹茲堡校區建立了世界上第一個無線網絡](../Page/匹茲堡.md "wikilink")，比起源於1999年的Wi-Fi品牌還要早\[9\]。2000年，[費城](../Page/費城.md "wikilink")[德雷克塞爾大學創造了歷史](../Page/卓克索大學.md "wikilink")，成為[美國第一個提供全校園無線網路覆蓋的主要大學](../Page/美國.md "wikilink")。現在大多數校園已設置無線網路。

在[臺灣的许多大学图书馆内](../Page/臺灣.md "wikilink")，也设有免费Wi-Fi热点，提供学生使用。

在[中国大陆](../Page/中国大陆.md "wikilink")，各大高等院校以及许多中学校园内覆盖有免费或收费的教育网校园Wi-Fi，以及电信营运商架设的收费校园热点（[中国电信](../Page/中国电信.md "wikilink")：ChinaNet，[中国移动](../Page/中国移动.md "wikilink")：CMCC，[中国联通](../Page/中国联通.md "wikilink")：ChinaUnicom）。

在[香港](../Page/香港.md "wikilink")，[各大專院校各自提供該校專門的Wi](../Page/香港專上教育.md "wikilink")-Fi供學生使用，多數需要以學生個人編號和密碼來登入。

另外，全世界许多科研和教育机构都提供教育网漫游服务[eduroam](../Page/eduroam.md "wikilink")，加入该联盟的机构成员可使用本机构的账号，在各联盟单位内实现无线网络访问的无障碍漫游。

### 電腦對電腦直接通訊

Wi-Fi無線通信也可以不需通過存取點，直接從一台電腦傳出到另一台。這就是所謂[Ad-hoc模式的Wi](../Page/Ad_hoc网络.md "wikilink")-Fi傳輸。這種無線ad-hoc網絡模式受到掌上遊戲機（如[任天堂的](../Page/任天堂.md "wikilink")[3DS游戏机](../Page/任天堂3DS.md "wikilink")）、數位相機和其它消費性電子設備的歡迎。Wi-Fi聯盟推動一個新的安全方法規範，稱為Wi-Fi
Direct，直接進行文件傳輸和媒體共享。

## 優勢和挑戰

### 商業優勢

[WiFi-detector.jpg](https://zh.wikipedia.org/wiki/File:WiFi-detector.jpg "fig:WiFi-detector.jpg")

Wi-Fi部署區網（LAN）可讓客戶端設備無需使用電線，降低網絡部署和擴充的成本。許多空間不能架設電纜，如戶外區和歷史建築，可運用無線區網來改善。

現在大多數筆記型電腦製造商已經內建無線網路裝置。Wi-Fi的價位持續下跌，使之漸漸普及，已成為企業普遍的基礎設施。

根據Wi-Fi聯盟指定，“Wi-Fi認證”是向後相容的。它指定一套全球統一標準：不同於行動電話，任何Wi –
Fi標準設備將在世界上任何地方正確執行。

Wi –
Fi已在22萬個以上公開熱點和幾千萬戶家庭、公司及世界各地的大學校園中使用。2010年，Wi-Fi保護訪問加密(WPA2)被認定安全，能讓用戶使用強大的密碼。新協議的Wi-Fi
Multimedia（WMM）說明Wi-Fi更適合於延遲敏感型應用（如語音和視頻），此外，WMM的省電機制能提高電池效率。

### 限制

Wi-Fi在全球各地的頻率分配和操作限制並不相同。台灣、美國所用的標準在2.4
GHz頻帶有11個頻道，而在歐洲大部份地區有另外的2個頻道，即13個頻道（1-11
v.s 1-13），日本還要追加一個（1-14）。這造成混亂現象：一個Wi-Fi信號在2.4
GHz頻段實際上佔用五個頻道，兩個頻道編號之差大於5的頻道，如2和7，不會發生頻道重疊，因此在美國只有3個非重疊頻道：1、6、11。在歐洲有三個或四個非重疊頻道：1、6、13或1、5、9、13。Effective
Isotropic Radiated Power（EIRP）在歐盟被限制為20 dBm的（100mW）。

### 傳遞的距離

[Wimax.svg](https://zh.wikipedia.org/wiki/File:Wimax.svg "fig:Wimax.svg")、[UMTS](../Page/UMTS.md "wikilink")、[GSM](../Page/GSM.md "wikilink")\]\]

Wi-Fi網絡範圍有限。一個使用802.11b或802.11g的典型無線路由器和天線，802.11n可到達超過這個範圍兩倍的距離，範圍隨頻率的波段調整。Wi-Fi在2.4
GHz的頻率區段範圍比5 GHz的頻率區段稍微好些。

通過使用定向天線，室外覆蓋範圍可提高數公里或以上。在一般情況下一個Wi-Fi設備的最高功率傳輸是受限於地方法規，如美國FCC第15條。

為達到無線區網的應用要求，和其他裝置相比Wi-Fi顯得相當耗電。其他技術如藍牙（可支持無線[PAN應用](../Page/PAN.md "wikilink")）提供了一個較小的傳播範圍（小於十公尺），因此耗電量較低。其他低耗電技術，如[ZigBee的有相當長的範圍](../Page/ZigBee.md "wikilink")，但傳輸速率卻很低。Wi-Fi的高耗電特性使得電池壽命的問題漸受重視。

研究人員已經開發出“不須新線”的技術，試圖彌補Wi-Fi室內範圍不足的問題。安裝新的電線（如cat-5）不具成本效益，ITU-T
標準的高速區域網絡使用現有的家庭線路（[同軸電纜](../Page/同軸電纜.md "wikilink")，電話線和電源線）來達成。雖然G.hn不具備一些Wi-Fi優勢（如可移動性或戶外使用），它的設計應用（如[IPTV分配](../Page/IPTV.md "wikilink")）仍在室內範圍發揮功能。

典型Wi-Fi的頻率由於電波傳播的複雜性，特別是樹和建築物影響信號的反射，只能大約測出Wi-Fi有關地區的發射器的信號強度。但這不包括遠距離Wi-Fi，因為遠距離Wi-Fi是使用塔台或高建築頂上的天線所架設的。

基本上，Wi-Fi的實際應用範圍非常有限，例如；倉庫中或零售空間的盤點機、結帳條碼閱讀器、收發台。市面上的無線路由器最低覆蓋率可達80平方公尺，[蘋果公司的](../Page/蘋果公司.md "wikilink")[AirPort技術更可達](../Page/AirPort.md "wikilink")100-140平方公尺。

## 技术简述

[DIR-895L_A1_Images_L(Side_Left).png](https://zh.wikipedia.org/wiki/File:DIR-895L_A1_Images_L\(Side_Left\).png "fig:DIR-895L_A1_Images_L(Side_Left).png")
[缩略图](https://zh.wikipedia.org/wiki/File:3Com_OfficeConnect_ADSL_Wireless_11g_Firewall_Router_2012-10-28-0862.jpg "fig:缩略图")

### 网络成员和结构

  - 站点（Station），网络最基本的组成部分。
  - 基本服务单元（Basic Service
    Set，BSS）。网络最基本的服务单元。最简单的服务单元可以只由两个站点组成。站点可动态連结（associate）到基本服务单元中。
  - 分配系统（Distribution
    System，DS）。分配系统用于连結不同的基本服务单元。邏輯上，分配系统使用的媒介（Medium）和基本服务单元使用的媒介完全不同，尽管物理上它们可能会是同一个媒介，例如同一个无线频段。
  - 接入点（Access Point，AP）。接入点即有普通站点的身份，又有連接到分配系统的功能。
  - 扩展服务单元（Extended Service
    Set，ESS）。由分配系统和基本服务单元组合而成。这种组合是逻辑上，并非物理上。不同的基本服务单元物有可能在地理位置相去甚远。分配系统也可以使用各种各样的技术。
  - 关口（Portal）。也是一个逻辑成分，用于将无线局域网和有线[局域网或其它网络联系起来](../Page/局域网.md "wikilink")。

这裡有3种媒介，站点使用的无线媒介，分配系统使用的媒介，以及和无线局域网集成一起的其它[局域网使用的媒介](../Page/局域网.md "wikilink")。物理上它们可能互相重叠。[IEEE
802.11只负责在站点使用的无线的媒介上的寻址](../Page/IEEE_802.11.md "wikilink")（Addressing）。分配系统和其它局域网的寻址不属无线局域网的范围。

IEEE802.11没有具体定义分配系统，只是定义了分配系统应该提供的服务（Service）。整个无线局域网定义了9种服务：

  - 5种服务属於分配系统的任务，分别为，連接（Association）、结束連接（Diassociation）、分配（Distribution）、集成（Integration）、再連接（Reassociation）。

<!-- end list -->

  - 4种服务属於站点的任务，分别为，鉴权（Authentication）、结束鉴权（Deauthentication）、隐私（Privacy）、MAC数据传输（MSDU
    delivery）。

## 運作原理

Wi-Fi的設置至少需要一個存取點（Access
Point，AP）和一個或一個以上的客戶端使用者（client）。無線AP每100ms將[SSID](../Page/SSID.md "wikilink")（Service
Set Identifier）經由beacons（信號台）封包廣播一次，beacons封包的傳輸速率是1
Mbit/s，並且長度相當的短，所以這個廣播動作对網路效能的影響不大。因為Wi-Fi規定的最低傳輸速率是1
Mbit/s，所以確保所有的Wi-Fi
client端都能收到這個[SSID廣播封包](../Page/SSID.md "wikilink")，client可以藉此決定是否要和這一個[SSID的AP連線](../Page/SSID.md "wikilink")。使用者可以設定要連線到哪一個[SSID](../Page/SSID.md "wikilink")。Wi-Fi系統開放對客戶端的連接並支援漫遊，這就是Wi-Fi的好處。但亦意味着，一个无线适配器有可能在性能上优于其他的适配器。*由于Wi-Fi通过空气传送信号*，所以和非交换以太网路有相同的特点。

近两年，出现一种WIFI over cable的新方案。此方案属于EOC（ethernet over cable）中的一种技术。通过将2.4G
wifi射频降频后在cable中传输。此种方案已经在中国小范围内測试商用。

## Wi-Fi認證

Wi-Fi技術建立在標準上。

但IEEE開發和出版這些標準，卻不測試符合他們的設備。非營利性的Wi-Fi聯盟成立於1999年，以填補這一段空白——建立並執行標準，並推動無線區域網路技術。

至2009年截止Wi-Fi聯盟擁有超過300多家來自世界各地公司和廠家會員，他們的產品通過認證過程後，有權標明Wi-Fi標誌。

認證過程具體來說檢查是否符合IEEE 802.11無線標準的規定、WPA和WPA2安全標準，以及EAP的認證標準。

認證時可以選擇測試包括IEEE 802.11的標準802.11n (CERTIFIED
n)、與行動網路互動設備的結合、有關安全和功能設置、多媒體、以及省電能力。

## 公共-{zh-hans:移动热点;zh-hant:行動熱點}-(Wi-Fi)

### [臺灣](../Page/臺灣.md "wikilink")

常見的公共-{zh-hans:移动热点;zh-hant:行動熱點}-如：

iTaiwan\[10\]
：是由[國家發展委員會建立的公共網路](../Page/國家發展委員會.md "wikilink")，自2011年10月7日上午9時起開始提供台灣民眾免費無線上網基本資訊服務，主要建置的地點以各地旅遊景點、交通運輸節點、文教館所、申辦洽公場所、醫療院所等室內公共空間，或是建置於民間超商旁的公共電話亭為主。申辦方式是以民眾在台灣申辦的手機號碼建立帳號，並且在取得簡訊驗證碼後，始能使用其公共網路。

## 数据安全

[超文本传输协议](../Page/超文本传输协议.md "wikilink")（HTTP）网站容易在公众WI-FI上的档案输送过程中披露个人资料，[超文本传输安全协议](../Page/超文本传输安全协议.md "wikilink")（HTTPS）利用[SSL](../Page/SSL.md "wikilink")/[TLS加密HTTP](../Page/TLS.md "wikilink")，保護交換資料的完整性和隱私。

## 读音争议

2012年11月6日，《[山东商报](../Page/山东商报.md "wikilink")》引用[山东外事翻译职业学院商务英语教研室主任温颜的说法称](../Page/山东外事翻译职业学院.md "wikilink")：“Wi-Fi的发音从语法角度来说，应该为‘微费’，因为“这个词是由‘wireless’（无线电）和‘fidelity’（保真度）这两个英语单词组成，但是发音又不能按照这个词的发音来，因为‘Wi-Fi’是一个合成词，应该按照一个单词的语法来发音。这个词中有两个元音字母‘i’，所以应该发短音，故从语法角度讲，发音应是‘微费’”。这种说法一时在[中国大陆网络上引起了很大争议](../Page/中国大陆.md "wikilink")，有网友引用牛津字典中的音标和美剧《[生活大爆炸](../Page/生活大爆炸.md "wikilink")》的相关片段来质疑。但也有网友称法国小罗伯特词典（Le
Petit Robert）中Wi-Fi一词法语发音的音标是\[11\]，发音接近汉语的“微费”\[12\]。

## 参考文献

## 参见

  - [WiGig](../Page/WiGig.md "wikilink")：WIFI联盟表示将与WiGig联盟合作展开无线传输技术合作。此举动意味着WiGig成员现在可以开发WIFI
    60GHz频谱，并提供高达7Gbps的高速数据传输速度。
  - [Wi-Fi Direct](../Page/Wi-Fi_Direct.md "wikilink")
  - [WiMAX](../Page/WiMAX.md "wikilink")

{{-}}

[Wi-Fi](../Category/Wi-Fi.md "wikilink")

1.

2.

3.

4.  [WiFi isn't short for "Wireless
    Fidelity"](http://boingboing.net/2005/11/08/wifi-isnt-short-for.html)

5.
6.

7.  <https://itw01.com/CSQ9ERR.html>

8.  <https://www.eettaiwan.com/news/article/20180725NT01-Wi-Fi-Preps-for-Leap-to-6GHz>

9.

10.

11. （并非小罗伯特词典）

12.