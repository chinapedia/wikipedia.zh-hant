[Hanami_Dango.jpg](https://zh.wikipedia.org/wiki/File:Hanami_Dango.jpg "fig:Hanami_Dango.jpg")糰子\]\]
[Amazake_and_mitarashi_dango_by_verigi_in_Takao,_Tokyo.jpg](https://zh.wikipedia.org/wiki/File:Amazake_and_mitarashi_dango_by_verigi_in_Takao,_Tokyo.jpg "fig:Amazake_and_mitarashi_dango_by_verigi_in_Takao,_Tokyo.jpg")
**糰子**是[日式點心](../Page/日式點心.md "wikilink")（[和菓子](../Page/和菓子.md "wikilink")）的一種。製作方法介於[元宵和](../Page/元宵.md "wikilink")[湯圓之間](../Page/湯圓.md "wikilink")，是藉由搓半濕的[糯米](../Page/糯米.md "wikilink")-{zh-cn:米粉;zh-hk:米粉;zh-tw:粉;}-製成。沒有[餡料](../Page/餡.md "wikilink")，用開水煮熟撈出，食用時會裹上[豆粉](../Page/黄豆粉.md "wikilink")、黑[芝麻](../Page/芝麻.md "wikilink")、[白糖或](../Page/白糖.md "wikilink")[花生粉等塒粉](../Page/花生.md "wikilink")。到了日本經過各地特色綜合下，加入日本民族特色之後，便形成了今日的糰子。

## 糰子的作法

將米磨成粉末（一般使用糯米）後，加上開水揉捏成小團狀蒸熟即成，類似年糕。通常是三到五個串在長竹籤上。可以根據地方產物或特性的不同使用[麵粉或](../Page/麵粉.md "wikilink")[黍子等的穀物粉作材料](../Page/黍子.md "wikilink")。

## 糰子的質地

糰子剛做好時是軟的，但時間一久就會變硬。要預防糰子變硬，可以加入[砂糖混合](../Page/砂糖.md "wikilink")，使糰子保持彈性。另外也有人加入[山芋](../Page/山芋.md "wikilink")，口感更好。

## 糰子的主要變化與吃法

  - 包餡：在糰子裡包紅[豆沙餡](../Page/豆沙.md "wikilink")。
  - 淋醬：以[醬油](../Page/醬油.md "wikilink")，[糖](../Page/糖.md "wikilink")，[澱粉和](../Page/澱粉.md "wikilink")[水](../Page/水.md "wikilink")，煮成鹹甜的佐料，淋在糰子上。
  - 裹粉：裹上豆粉與砂糖。
  - 用[海藻包上了](../Page/海藻.md "wikilink")[磯部](../Page/磯部.md "wikilink")，加上點了醬油的糰子吃。
  - 加入磨碎的[毛豆和糖](../Page/毛豆.md "wikilink")，成為甜甜的綠色糰子。
  - 可以放入[年糕小豆湯和](../Page/年糕小豆湯.md "wikilink")[什錦甜涼粉一起吃](../Page/什錦甜涼粉.md "wikilink")。

## 其他種類的糰子

[吉備糰子.png](https://zh.wikipedia.org/wiki/File:吉備糰子.png "fig:吉備糰子.png")

  - [花見糰子](../Page/花見糰子.md "wikilink")：以[櫻花花瓣與](../Page/櫻花.md "wikilink")[艾草等植物改變糰子顏色](../Page/艾草.md "wikilink")，通常為紅綠白三色組合。於[花見](../Page/花見.md "wikilink")（觀賞[櫻花](../Page/櫻花.md "wikilink")）的時候吃。
  - [月見糰子](../Page/月見糰子.md "wikilink")：[農曆八月十五](../Page/農曆.md "wikilink")[中秋節](../Page/中秋節.md "wikilink")（[十五夜](../Page/十五夜_\(日本\).md "wikilink")）賞月的時候吃。
  - 蓬糰子：磨碎的的糰子，加上黃豆麵和糖一起吃。
  - 白玉糰子：糯米粉製作的糰子。
  - 吉備糰子（黍子麵糰子）：以黍子為材料，傳說是[桃太郎的祖母製作的](../Page/桃太郎.md "wikilink")。
  - 羽二重糰子：用製作者羽二重命名的糰子。
  - 馬鈴薯團子：使用[馬鈴薯與](../Page/馬鈴薯.md "wikilink")[太白粉製成的一種點心](../Page/太白粉.md "wikilink")，通常會用火爐來烤，在[北海道一帶較盛行](../Page/北海道.md "wikilink")。

## 參見

  - [搓湯圓](../Page/搓湯圓.md "wikilink")
  - [湯圓](../Page/湯圓.md "wikilink")
  - [糖不甩](../Page/糖不甩.md "wikilink")

[category:糰子類食品](../Page/category:糰子類食品.md "wikilink")
[category:和菓子](../Page/category:和菓子.md "wikilink")