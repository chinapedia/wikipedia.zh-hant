**HTC
Universal**（研發代號），是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，全世界首部搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
5的雙模3頻PDA手機，迷你筆電外形，擁有3.6吋VGA翻轉式TFT螢幕，62鍵QWERTY鍵盤，曾堪稱世界最強的PDA手機，全球最迷你的3G中文筆記型電腦。2005年9月於歐洲首度發表。已知客製版本Qtek
9000，Dopod 900，SFR v1640，Vodafone v1640，Vodafone VPA IV，T-Mobile MDA
Pro，i-mate JASJAR，O2 Xda Exec，Orange SPV M5000，Grundig GR980，E-Plus
PDA IV。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：Intel XScale PXA270 520MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：127.7 毫米 X 81 毫米 X 25 毫米
  - 重量：285g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：VGA 解析度、3.6 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA/GSM
  - [藍牙](../Page/藍牙.md "wikilink")：Bluetooth 1.2
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b
  - [相機](../Page/相機.md "wikilink")：主鏡頭130萬畫素相機，「不」支援自動對焦功能。第二鏡頭30萬畫素
  - 擴充：支援MMC/SDIO介面記憶卡
  - [電池](../Page/電池.md "wikilink")：1620mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")

## 外部連結

  - [HTC](http://www.htc.com/)
  - [Qtek](https://web.archive.org/web/20080707182953/http://www.myqtek.com/)
  - [Dopod](http://www.dopod.com/)

[H](../Category/智能手機.md "wikilink")
[Universal](../Category/宏達電手機.md "wikilink")