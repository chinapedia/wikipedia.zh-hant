**Socket A** 又稱 **Socket
462**，是[AMD](../Page/AMD.md "wikilink")[中央處理器插座的一種](../Page/中央處理器.md "wikilink")，為了對抗[Intel的](../Page/Intel.md "wikilink")[Socket
370而推出](../Page/Socket_370.md "wikilink")。它擁有462個插孔，插入處理器時無需用力鎖緊。它適用於AMD的[Athlon](../Page/Athlon.md "wikilink")
[K7至](../Page/K7.md "wikilink")[Athlon
XP](../Page/Athlon_XP.md "wikilink")
3200+及低階的[Duron及](../Page/Duron.md "wikilink")[Sempron](../Page/Sempron.md "wikilink")。有時FSB頻率和電壓規格的轉變，會造成舊的[晶片組無法支援新的處理器](../Page/晶片組.md "wikilink")。

隨著更多新型號處理器的開發，這款插座被新款插座所取代，有供[Athlon
64和](../Page/Athlon_64.md "wikilink")[Sempron使用的](../Page/Sempron.md "wikilink")[Socket
754](../Page/Socket_754.md "wikilink")，和供Athlon 64及[Athlon 64
FX使用的](../Page/Athlon_64_FX.md "wikilink")[Socket
939](../Page/Socket_939.md "wikilink")。而AMD也宣佈在2005年第二季起停止推出Socket
A的處理器。

## 使用Socket A的處理器

  - *Thunderbird* Athlon 650MHz-1.4GHz (FSB 200MHz/266MHz, L2 Cache
    256KB)
  - *Palomino* Athlon XP 1500+ \~ 2100+ (FSB 266MHz, L2 Cache 256KB)
  - *Thoroughbred* Athlon XP 1700+ \~ 2800+ (FSB 266MHz/333MHz, L2 Cache
    128KB)
  - *Barton* Athlon XP 2500+ \~ 3200+ (FSB 333MHz/400MHz, L2 Cache
    512KB)
  - *Thorton* Athlon XP 2000+/2200+/2400+ (FSB 266MHz, L2 Cache 256KB)
  - *Spitfire* Duron 600MHz-950MHz (FSB 200MHz, L2 Cache 64KB)
  - *Morgan* Duron 1000MHz-1300MHz (FSB 200MHz, L2 Cache 64KB)
  - *Applebred* Duron 1400MHz/1600MHz/1800MHz (FSB 266MHz, L2 Cache
    64KB)
  - *Thoroughbred* Sempron 2200+(1.5GHz) \~ 2800+(2GHz) (FSB 333MHz, L2
    Cache 256KB)
  - *Barton* Sempron 3000+(2GHz)/3300+(2.2GHz) (FSB 333MHz/400MHz, L2
    Cache 512KB)

### 技術資料

  - 支援時脈介乎600MHz (Duron) 至2200MHz (Athlon XP 3200+) 之間的處理器。

[Category:CPU插座](../Category/CPU插座.md "wikilink")