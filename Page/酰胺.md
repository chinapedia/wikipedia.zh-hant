[AmideTypes.png](https://zh.wikipedia.org/wiki/File:AmideTypes.png "fig:AmideTypes.png")
**酰胺**是指含有和氮和与氧相连的[酰基的一类化合物](../Page/酰基.md "wikilink")，官能团为R<sub>n</sub>E(O)<sub>x</sub>NR'<sub>2</sub>，其中R和R'指氢原子或有机基团，E常见的有碳、硫、磷等。有机化学中命名中未指明E原子时E为碳，可以看作[羧酸与](../Page/羧酸.md "wikilink")[氨或](../Page/氨.md "wikilink")[胺](../Page/胺.md "wikilink")[缩合形成的化合物](../Page/缩合反应.md "wikilink")，是[羧酸衍生物的一类](../Page/羧酸衍生物.md "wikilink")，如[甲酰胺](../Page/甲酰胺.md "wikilink")，[乙酰胺](../Page/乙酰胺.md "wikilink")。当E为硫原子，x=2时为[磺酰胺](../Page/磺酰胺.md "wikilink")。本文以下所指为羰基-氮酰胺。

## 结构

[有机酰胺中的C](../Page/有机.md "wikilink")-N键比起胺中的C-N键要短很多。这一方面是因为酰胺中C-N键的[碳是用](../Page/碳.md "wikilink")*sp*<sup>2</sup>[混成軌域与氮成键](../Page/混成軌域.md "wikilink")，而胺中C-N键的碳是用*sp*<sup>3</sup>混成軌域与氮成键，*s*成分较少；另一方面是因为羰基与氨基的氮[共轭](../Page/共轭.md "wikilink")，从而使C-N键具有某些[双键性质造成的](../Page/双键.md "wikilink")。

  -
    [AmideResonance.png](https://zh.wikipedia.org/wiki/File:AmideResonance.png "fig:AmideResonance.png")

## 合成

羧酸衍生物如酰氯，酸酐和的酯氨（胺）解是制备酰胺的常用方法。\[1\]
酰胺可以由[肟通过](../Page/肟.md "wikilink")[Beckmann重排而获得](../Page/Beckmann重排.md "wikilink")。也可以从[腈水解而来](../Page/腈.md "wikilink")。

## 性质

除甲酰胺外，大部分具有RCONH<sub>2</sub>结构的酰胺为无色固体。在羧酸衍生物中，酰胺具有最强的稳定性，其水解最难发生，一般需要在强酸性或碱性条件下回流。相较于一般胺类，酰胺弱碱很弱，在水溶液中几乎不体现碱性。能与酸反应成盐，其质子化发生在氧原子上，酰胺的共轭酸的pKa在-0.5左右。酰亚胺（RCONHCOR'）的氮原子上的氢具有弱酸性。

[蛋白质和](../Page/蛋白质.md "wikilink")[肽是含有酰胺键的重要生物分子](../Page/肽.md "wikilink")。一些生物碱中也含有酰胺键。

  -

      -
        [酰胺2.png](https://zh.wikipedia.org/wiki/File:酰胺2.png "fig:酰胺2.png")

## 参考资料

## 参见

  - [肽](../Page/肽.md "wikilink")
  - [*N*-溴代丁二酰亚胺](../Page/N-溴代丁二酰亚胺.md "wikilink") -
    [*N,N*-二甲基甲酰胺](../Page/N,N-二甲基甲酰胺.md "wikilink")
  - [聚酰胺](../Page/聚酰胺.md "wikilink")

[\*](../Category/酰胺.md "wikilink") [X](../Category/官能团.md "wikilink")

1.  邢其毅等．《基础有机化学》第三版 下册．北京：高等教育出版社，2005年．ISBN 978-7-04-017755-8