**李元度**，字**次青**，又字**笏庭**，清代[湖南省](../Page/湖南省.md "wikilink")[平江县沙塅村人](../Page/平江县.md "wikilink")，[曾國藩好友](../Page/曾國藩.md "wikilink")、親家、歷史學家，出身貧寒，累官至[貴州](../Page/貴州.md "wikilink")[布政使](../Page/布政使.md "wikilink")，有官譽好評。

## 生平

  - 父親販豆腐；4歲成孤兒。
  - 18歲中秀才，23歲中湖南[舉人](../Page/舉人.md "wikilink")，任[黔陽县教儒](../Page/黔陽县.md "wikilink")
  - 1853年，李任湖南[郴州州学训导](../Page/郴州.md "wikilink")，憤[太平軍非儒](../Page/太平軍.md "wikilink")，寫長信給[衡陽招湘勇的曾國藩自荐](../Page/衡陽.md "wikilink")，並自平江招勇500人，棄文從武投效衡陽曾國藩。
  - 曾練兵兩年，在[長沙](../Page/長沙.md "wikilink")[靖港慘敗](../Page/靖港.md "wikilink")；曾寫好遺書給李後投水自盡，被撈救起，**李元度**皆予勸慰，曾感動之餘，把侄女許配給李。

### [徽州失守](../Page/徽州.md "wikilink")

  - 咸豐十年1860年，已是[兩江總督曾國藩](../Page/兩江總督.md "wikilink")，令李元度領軍守[徽州](../Page/徽州.md "wikilink")，為[湘軍司令部](../Page/湘軍.md "wikilink")[祁門門戶](../Page/祁門.md "wikilink")。
  - 太平軍10萬攻至，李幾乎戰歿逃出，徽州失守；曾彈劾**李元度**上准。

### [同治復出貴州](../Page/同治.md "wikilink")

  - 同治初年，[貴州民變起事](../Page/貴州.md "wikilink")；
  - 同治五年（1866年三月），李在平江募勇2000，前往貴州平亂；兩年內攻佔[號軍村寨](../Page/號軍.md "wikilink")900座，累戰功升任雲南[按察使](../Page/按察使.md "wikilink")，在籍未赴任（因另有缺）。

### [光緒佈署海防](../Page/光緒.md "wikilink")

  - 光緒10年[中法戰爭時應](../Page/中法戰爭.md "wikilink")[彭玉麟之邀出任](../Page/彭玉麟.md "wikilink")[廣東防務](../Page/廣東.md "wikilink")，建議堵塞[虎門阻法軍海軍](../Page/虎門.md "wikilink")。
  - 11年六月補貴州[按察使](../Page/按察使.md "wikilink")，上疏[北京有關東南海防建言](../Page/北京.md "wikilink")：

<!-- end list -->

1.  沿岸仿外國廣修炮台；
2.  為籌備海防餉，建議改江南[漕運為](../Page/漕運.md "wikilink")[折色](../Page/折色.md "wikilink")。
3.  為防法國、日本犯邊，**福建[巡撫需改置於](../Page/巡撫.md "wikilink")[台灣](../Page/台灣.md "wikilink")**；
4.  裁汰冗官，廢督撫同省城之湖北、雲南、廣東三省巡撫，收權於[總督](../Page/總督.md "wikilink")；
5.  於國外華僑居地，廣置公使或是領事。

立意雖好，唯開罪既得利益者眾，朝廷礙難採納實施。

  - 光緒13年升任貴州布政使 (負責省內民政財務)，同年九月廿七日（1887年11月12日）病逝任內。

## 著作

  - 《[国朝先正事略](../Page/国朝先正事略.md "wikilink")》
  - 《湖南通志》

[L李](../Page/category:平江人.md "wikilink")
[U](../Page/category:李姓.md "wikilink")

[L李](../Category/清朝貴州按察使.md "wikilink")
[L李](../Category/湘軍人物.md "wikilink")
[L李](../Category/清朝詩人.md "wikilink")
[L李](../Category/清朝詞人.md "wikilink")
[L李](../Category/清朝駢文家.md "wikilink")
[L李](../Category/清朝歷史學家.md "wikilink")
[L李](../Category/清朝地理學家.md "wikilink")
[Category:幼年失親者](../Category/幼年失親者.md "wikilink")