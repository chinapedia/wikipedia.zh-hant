**老牛皮國際股份有限公司**（[英語](../Page/英語.md "wikilink")：La New
Corporation，縮寫：**La
New**），是一家[臺灣](../Page/臺灣.md "wikilink")[皮鞋](../Page/皮鞋.md "wikilink")[品牌](../Page/品牌.md "wikilink")，負責人是[劉保佑](../Page/劉保佑.md "wikilink")。

## 發展歷史

[La_New皮鞋新竹東門店.jpg](https://zh.wikipedia.org/wiki/File:La_New皮鞋新竹東門店.jpg "fig:La_New皮鞋新竹東門店.jpg")東門店\]\]
[劉保佑因為朋友的關係到](../Page/劉保佑.md "wikilink")[越南投資鞋廠](../Page/越南.md "wikilink")，並於1996年在[臺灣成立皮鞋品牌La](../Page/臺灣.md "wikilink")
New，並投入每年1000萬成立「足部研究所」，專門替特殊腳型開發鞋款。

最初以傳統寄賣的方式為主，但是由於一般商家拖欠貨款的情形普遍存在，後來便撤回在各皮鞋行及[百貨公司的](../Page/百貨公司.md "wikilink")[專櫃](../Page/專櫃.md "wikilink")，而在[台北市](../Page/台北市.md "wikilink")[民生東路展開第一家的La](../Page/民生東路.md "wikilink")
New專門店，並開始逐漸擴大展店，最高峰的時候，曾經達到一個月同時開出五家門市。

近年由於經濟不景氣，加上La New許多業外投資的回收並不如預期，原本預計2007年上市上櫃的計畫已經撤回。

2006年藉由[La
New熊隊奪得](../Page/La_New熊隊.md "wikilink")[亞洲職棒大賽亞軍](../Page/亞洲職棒大賽.md "wikilink")，助長La
New在[日本開店的宣傳](../Page/日本.md "wikilink")，目前在日本已經有多家店面。

2009年起成立那米哥集團，經營包括行社、休閒會館、宴會廣場業務\[1\]。

## 參見

  - [劉保佑](../Page/劉保佑.md "wikilink")
  - [達達集團](../Page/達達集團.md "wikilink")
  - [Lamigo桃猿](../Page/Lamigo桃猿.md "wikilink")

## 参考資料

  - [皮鞋大戰 阿瘦降價拉客 La
    new文風不動](https://web.archive.org/web/20120114125422/http://www.libertytimes.com.tw/2005/new/oct/11/today-stock1.htm)

## 外部連結

  - [La New](http://www.lanew.com.tw/)

  -
  -
[Category:鞋類品牌](../Category/鞋類品牌.md "wikilink")
[Category:台灣品牌](../Category/台灣品牌.md "wikilink")
[Category:總部位於新北市的工商業機構](../Category/總部位於新北市的工商業機構.md "wikilink")
[Category:1996年台灣建立](../Category/1996年台灣建立.md "wikilink")

1.