**RTBF**（[法語全名](../Page/法語.md "wikilink")：），意即**比利時法語區電視台及電台**，是[比利時南部法語區](../Page/比利時.md "wikilink")[瓦隆省](../Page/瓦隆.md "wikilink")（Wallonia）的公營廣播機構（比利時北部[荷蘭語區](../Page/荷蘭語.md "wikilink")[佛蘭德省](../Page/佛蘭德.md "wikilink")（Flanders）的公營廣播機構名為[VRT](../Page/弗拉芒广播电视公司.md "wikilink")），總部位於[布魯塞爾](../Page/布魯塞爾.md "wikilink")。
[Tour-RTBF_Luc_Viatour.JPG](https://zh.wikipedia.org/wiki/File:Tour-RTBF_Luc_Viatour.JPG "fig:Tour-RTBF_Luc_Viatour.JPG")
[RTBF_vrachtwagen.jpg](https://zh.wikipedia.org/wiki/File:RTBF_vrachtwagen.jpg "fig:RTBF_vrachtwagen.jpg")

## 頻道

RTBF有三個[電視頻道](../Page/電視.md "wikilink")：

  - *La Une*
  - *La Deux*
  - *La Trois*
  - *RTBF Sat*（RTBF[衛星電視](../Page/衛星電視.md "wikilink")）

並有多個[電台頻道](../Page/電台.md "wikilink")：

  - **
  - *RTBF International*（RTBF國際）
  - **
  - *Musiq3*
  - *Classic21*
  - *PureFM*

## 歷史

RTBF原名INR（*Institut national de
radiodiffusion*），成立於1930年6月18日。INR於1940年6月14日因[二戰](../Page/二戰.md "wikilink")[德國入侵而停播](../Page/德國.md "wikilink")，並被改名*Radio
Bruxelles*（布魯塞爾電台）。比利時[流亡政府於](../Page/流亡政府.md "wikilink")[倫敦成立](../Page/倫敦.md "wikilink")*Office
de Radiodiffusion Nationale
Belge*（RNB）廣播。二戰結束後，1945年9月14日，INR與RNB重新合併。1953年INR開始電視廣播（每天2小時），1960年INR與RTB（**）合併。1971年RTB首次作彩色電視廣播。1973年RTB併入RTBF並成立第二條電視頻道RTbis。1984年RTBF與多個[歐洲法語電視台一起成立向全歐廣播的泛歐法語頻道](../Page/歐洲.md "wikilink")[TV5](../Page/TV5.md "wikilink")。

## 爭議事件

  - [2006年佛蘭德省獨立虛構新聞事件](../Page/2006年佛蘭德省獨立虛構新聞事件.md "wikilink")

## 外部連結

  - [RTBF官方網站](http://www.rtbf.be)

[Category:比利時媒體](../Category/比利時媒體.md "wikilink")