[Trygve_Bratteli.jpg](https://zh.wikipedia.org/wiki/File:Trygve_Bratteli.jpg "fig:Trygve_Bratteli.jpg")
**特里格弗·马丁·布拉特利**（[挪威语](../Page/挪威语.md "wikilink")：****
，），[挪威](../Page/挪威.md "wikilink")[政治家](../Page/政治家.md "wikilink")。

布拉特利生于[讷特岛](../Page/讷特岛.md "wikilink")，在那里他上完小学。他有一段时间是失业的，曾当过信使、[捕鲸船员和](../Page/捕鲸.md "wikilink")[建筑工人](../Page/建筑.md "wikilink")。1928年他加入[工党青年组织](../Page/挪威工党.md "wikilink")，30年代任《劳动青年报》编辑。在[纳粹入侵挪威期间他任命为工党的危机委员会秘书](../Page/纳粹.md "wikilink")，1941年[希特勒颁布了臭名昭著的](../Page/希特勒.md "wikilink")“[夜与雾法令](../Page/夜与雾法令.md "wikilink")”（[德文](../Page/德文.md "wikilink")：），这是针对被占领国的抵抗运动，用于提供镇压方式的法令。1942年他被[德国人拘捕](../Page/德国.md "wikilink")，从1943年到1945年他被关押在德国[集中营和](../Page/集中营.md "wikilink")[监狱中](../Page/监狱.md "wikilink")。

在返回到挪威以后，1945年他成为了工党的副主席，担任新组成的防御委员会委员，1965年他被选为工党主席。从1950年到1981年他在[国民议会](../Page/挪威国民议会.md "wikilink")（）任职，并曾任财政和运输大臣。从1971年到1972年和从1973年到1976年他是[挪威首相](../Page/挪威首相.md "wikilink")。1970年6月，挪威第三次提出加入[欧共体的申请并与欧共体进行了谈判](../Page/欧洲共同体.md "wikilink")，1972年1月谈判结束，布拉特利政府与欧共体签订了加入欧洲共同体的协议，但协议被同年9月挪威举行的全国[公民投票否决](../Page/公民投票.md "wikilink")（反对者为53.9%，支持者为46.1%），他辞去了他的[内阁首相职务](../Page/内阁.md "wikilink")。

特里格弗·布拉特利被认为一位熟练的政客和正直的人。他写了一定数量的[自传体和](../Page/自传.md "wikilink")[政治书籍](../Page/政治.md "wikilink")。关于他在德国集中营时的经历——“囚犯在夜和雾里”（）在挪威成为了一本畅销书。

[Category:挪威政治人物](../Category/挪威政治人物.md "wikilink")
[Category:挪威首相](../Category/挪威首相.md "wikilink")
[Category:挪威工党党员](../Category/挪威工党党员.md "wikilink")