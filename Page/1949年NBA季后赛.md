**1949年NBA季后赛**是**BAA**改名[NBA前最後一屆季後賽](../Page/NBA.md "wikilink")，因此正名為**1949年BAA季后赛**，現會當作第3屆NBA季後賽。於[1948-49
BAA賽季後舉行](../Page/1948-49_BAA賽季.md "wikilink")。最終西岸冠軍[明尼阿波利斯湖人於](../Page/洛杉矶湖人队.md "wikilink")[NBA總決賽以](../Page/NBA总决赛.md "wikilink")4-2擊敗東岸冠軍[華盛頓國會](../Page/華盛頓國會隊.md "wikilink")。

## 晉級表

## 参考链接

  - [Basketball-Reference.com's 1949 BAA Playoffs
    page](http://www.basketball-reference.com/playoffs/BAA_1949.html)

[fi:BAA-kausi
1948–1949\#Pudotuspelit](../Page/fi:BAA-kausi_1948–1949#Pudotuspelit.md "wikilink")
[it:Basketball Association of America
1948-1949\#Play-off](../Page/it:Basketball_Association_of_America_1948-1949#Play-off.md "wikilink")

[N](../Category/1949年体育.md "wikilink")
[4](../Category/NBA季后赛.md "wikilink")