[Paris_-_Salon_de_la_moto_2011_-_Suzuki_-_Hayabusa_-_001.jpg](https://zh.wikipedia.org/wiki/File:Paris_-_Salon_de_la_moto_2011_-_Suzuki_-_Hayabusa_-_001.jpg "fig:Paris_-_Salon_de_la_moto_2011_-_Suzuki_-_Hayabusa_-_001.jpg")
**鈴木GSX1300R隼**（）乃是[日本](../Page/日本.md "wikilink")[鈴木公司自](../Page/鈴木公司.md "wikilink")1999年起開始製造的一款高性能重型[機車](../Page/機車.md "wikilink")。除了原本的車型代號之外，其產品別名「**隼**」（）也是廣為知悉的稱呼方式。

最早於1999年出產，其特殊車頭造型的概念是從一種能急速俯衝的鳥類 -
[游隼](../Page/游隼.md "wikilink")，所仿製而來的，獨特的進氣孔與空氣力學使得本車的風阻係數極低。其高達175ps[馬力](../Page/馬力.md "wikilink")、14kg[扭力與](../Page/扭力.md "wikilink")11:1的[壓縮比](../Page/壓縮比.md "wikilink")，提供了GSX
1300R極快的加速度。

GSX
1300R的傲人紀錄包括：在2000年時創下與[保時捷捉對廝殺的加速度勝出紀錄](../Page/保時捷.md "wikilink")，最高速度超出時速345KM/H，以及一檔就可以時速破百的驚人表現，當時知名汽車雜誌為它定了一個封號
– 地表最速車。這個紀錄一直到[MTT渦輪超級機車出現之後才被超越](../Page/MTT渦輪超級機車.md "wikilink")。

1999-2007為第一代，2008-至今為第二代。

## 第一代（1999-2007）

[Hayabusa_Dash.jpg](https://zh.wikipedia.org/wiki/File:Hayabusa_Dash.jpg "fig:Hayabusa_Dash.jpg")

## 第二代（2008）

  - 引擎型式：水冷四行程，四汽缸16汽門，水冷式DOHC
  - 總排氣量：1340 cc
  - 最大馬力 198ps/ 9,500
  - 最大扭力 15.8kgm/7200rpm
  - 內徑x行程：81.0 mm x 65.0 mm
  - 風阻係數：\< 0.270 CdA
  - 壓縮比:12.5:1
  - 燃油系統：電子燃油噴射系統
  - 變速器：6段變速constant mesh
  - 傳動系統：鏈傳動，112環
  - 車長/寬/高(mm)：2,190mm x 740mm x 1,165mm
  - 輪距：1,485mm
  - 最低離地距：120mm
  - 座高：805mm
  - 淨重：217kg
  - 前懸吊系統：伸縮式倒立線圈彈簧，預載全能調校，回彈及受壓調校
  - 後懸吊系統：後搖臂式，氣體/線圈彈簧，氣體/油壓damped，彈簧預載5向全能調校，回彈及受壓調校
  - 前剎車：320mm雙碟煞
  - 後剎車：260mm單碟煞
  - 前輪：120/70ZR17(58W)
  - 後輪：190/50ZR17(73W)
  - 點火形式：電子點火
  - 油箱容量：21.0L

## 內部連結

  - [鈴木隼方程式賽車](../Page/鈴木隼方程式賽車.md "wikilink")

## 參考資料

  - [GLOBAL SUZUKI](http://www.globalsuzuki.com/motorcycle/index.html)　

## 外部連結

  - [日本官方網站](http://www1.suzuki.co.jp/motor/product/gsx1300ral5/top)

  - [北美洲官方網站](http://www.suzukicycles.com/Product%20Lines/Cycles/Products/Hayabusa/2016/GSX1300R.aspx)

[it:Suzuki GSX-R\#GSX-R 1300
Hayabusa](../Page/it:Suzuki_GSX-R#GSX-R_1300_Hayabusa.md "wikilink")

[Category:鈴木機車車輛](../Category/鈴木機車車輛.md "wikilink")
[Category:運動摩托車](../Category/運動摩托車.md "wikilink")