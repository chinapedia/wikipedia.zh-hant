**白尾梢虹雉**（[学名](../Page/学名.md "wikilink")：）俗稱**雪鹅**，一种大型高山雉类，属于[虹雉属](../Page/虹雉属.md "wikilink")，主要分布于[中国南部](../Page/中国.md "wikilink")、[缅甸北部和](../Page/缅甸.md "wikilink")[印度东北部海拔](../Page/印度.md "wikilink")2500-4200米[喜马拉雅山东麓林区](../Page/喜马拉雅山.md "wikilink")。

白尾梢虹雉的体长可达68厘米。**雄鸟**通体颜色艳丽，上体羽毛为金属紫绿色，颈部为铜绿色，喉部为紫黑色，背部白色，喙部为金黄色。尾巴为白色，夹杂栗色带纹。**雌鸟**的色彩暗弱，通体深褐色，喉部为白色，喙部浅黄色。

白尾梢虹雉主要以种子和花为食物，每次产卵3-5枚。

中国特有鸟类种，由于栖息地的不断减少以及偷猎的威胁，数量十分稀少，[IUCN列为易危物种](../Page/IUCN.md "wikilink")，被列入[CITES附录I](../Page/CITES.md "wikilink")。

## 分类

### 亚种

  - 指名亚种（*L.s.sclateri*）
  - *L.s.orientalis*

## 参考

  - Database entry includes a brief justification of why this species is
    vulnerable and the criteria used

[Category:虹雉属](../Category/虹雉属.md "wikilink")
[Category:中国国家一级保护动物](../Category/中国国家一级保护动物.md "wikilink")
[Category:中國動物](../Category/中國動物.md "wikilink")
[Category:印度鸟类](../Category/印度鸟类.md "wikilink")
[Category:緬甸動物](../Category/緬甸動物.md "wikilink")
[Category:云南动物](../Category/云南动物.md "wikilink")