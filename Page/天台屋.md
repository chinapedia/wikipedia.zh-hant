**天台屋**建於[大廈的](../Page/大廈.md "wikilink")[天台之上的](../Page/天台.md "wikilink")[建築物](../Page/建築物.md "wikilink")，見證了昔日住屋供應緊張的狀況。在香港，大多數的天台屋都是非法興建，而且都在政府清拆的行列裡。不過，到現在仍然有不少[草根階層的市民仍在天台屋居住](../Page/草根階層.md "wikilink")。這些住客有部份是[業主](../Page/業主.md "wikilink")，以較市價為低的價錢購入單位，亦有部份人士只是租客。天台屋不論是售價或租金都較其他樓層為低，一方面有可能因為它們是非法，但另一方面，[夏日的炎熱和](../Page/夏日.md "wikilink")[冬天的寒冷亦使其他可以負擔較佳質素樓宇的人作其他選擇](../Page/冬天.md "wikilink")。

安全問題亦是天台屋的一大問題。在2006年平安夜晚上八時，在[土瓜灣就一所天台屋由於天氣乾燥及](../Page/土瓜灣.md "wikilink")[滅火設備不足而使](../Page/滅火.md "wikilink")[火災迅速惡化](../Page/火災.md "wikilink")。

此類搭建物的業主很多時都會封掉大廈樓梯通往天台的入口，將其變成僭建天台屋的門口，如遇火災的話居民可能會逃生無門\[1\]。部分天台屋多已建成數十年，樓宇[折舊](../Page/折舊.md "wikilink")，[物業管理不良](../Page/物業管理.md "wikilink")，常有漏水、[蟲](../Page/蟲.md "wikilink")[鼠甚至有住戶在走火通道](../Page/鼠.md "wikilink")（即大廈樓梯）焚燒香燭[冥鏹](../Page/冥鏹.md "wikilink")（俗稱燒衣紙）等問題，走火通道放滿三不管的雜物。

2011年中，[新界](../Page/新界.md "wikilink")[丁屋以致眾多](../Page/丁屋.md "wikilink")[香港立法局](../Page/香港立法局.md "wikilink")[議員及](../Page/議員.md "wikilink")[香港特區高官的僭建天台玻璃屋問題備受關注](../Page/香港特區.md "wikilink")，不過，假如真要拿那些「僭建物」與存在已久的、甚為擁擠、衛生環境惡劣的「鐵皮天台屋」比較的話，玻璃屋的居住環境和安全程度應該算是高出很多了。

由於香港經濟貧富懸殊，直到現在，仍然有不少貧困人士居住在天台屋裡，而這班人與其他居住於[籠屋或街上的貧民合共](../Page/籠屋.md "wikilink")100,000人（[香港社區組織協會數字](../Page/香港社區組織協會.md "wikilink")）\[2\]。

## 參考資料

## 參看

  - [套房](../Page/套房.md "wikilink")
  - [板套房](../Page/板套房.md "wikilink")
  - [車房屋](../Page/車房屋.md "wikilink")（）
  - [貧民區](../Page/貧民區.md "wikilink")/[徙置區](../Page/徙置區.md "wikilink")/[寮屋區](../Page/寮屋區.md "wikilink")
  - [2011年香港僭建風波](../Page/2011年香港僭建風波.md "wikilink")
  - [屋塔房王世子](../Page/屋塔房王世子.md "wikilink")：[朝鲜语](../Page/朝鲜语.md "wikilink")「屋塔房」即天台屋

[Category:建築](../Category/建築.md "wikilink")
[Category:房屋](../Category/房屋.md "wikilink")

1.  [「三無大廈」亂封逃生口](http://hk.news.yahoo.com/三無大廈-亂封逃生口-213153889.html)
2.