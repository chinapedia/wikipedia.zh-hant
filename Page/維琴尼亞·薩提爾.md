[VirginiaSatir.jpg](https://zh.wikipedia.org/wiki/File:VirginiaSatir.jpg "fig:VirginiaSatir.jpg")
**維琴尼亞·薩提爾（）**（）是一名來自美國的家族治療先驅，她原先是一名教師、[社會工作者](../Page/社會工作師.md "wikilink")，後開創並發展了屬於自己的[薩提爾模式（The
Satir
Model）](../Page/薩提爾模式.md "wikilink")，並運用於家族治療中。她相信，不論外在條件如何，在這個世界上，沒有人是無法做出改變的。她也相信，人類可以實現其所想要實現的，可以更正向、更有效率地運用自己。而這樣的信念也促使她去往世界各地開展工作坊，以帶去希望、新的觀點，以及家族治療的新取向。\[1\]

## 生平

維琴尼亞·薩提爾出生在[美國](../Page/美國.md "wikilink")[威斯康星州的农场里](../Page/威斯康星州.md "wikilink")，在家中五個孩子中排行老大。

## 重要著作

  - 《家庭聯合治療》（Conjoint Family Therapy，1964）
  - 《家庭如何塑造人》（Peoplemaking，1972）
  - 《尊重自己》（Self Esteem，1975）
  - 《與人接觸》（Making Contact，1976）
  - 《心的面貌》（Your Many Faces，1978）
  - 與[John Grinder](../Page/John_Grinder.md "wikilink")、[Richard
    Bandler合著](../Page/Richard_Bandler.md "wikilink")《與家庭一同改變》（Changing
    with familes，1979）

## 外部連結

  - [薩提爾在Avanta.net的簡歷](http://www.avanta.net/writings/biography/biography.html)
  - [Brief
    biography](https://web.archive.org/web/20050322181719/http://www.webster.edu/~woolflm/satir2.html)
    at Webster University
  - [Satir
    Ottawa](https://web.archive.org/web/20060803212458/http://satirottawa.org/)
  - [Satir Institute of the Rockies](http://satirtraining.org/) Satir
    Model Training
  - [Satir Systems](http://www.satirsystems.com/)
  - [Virginia Satir group to promote Virginia Satir's work in
    Europe](http://health.groups.yahoo.com/group/satir-europe/) started
    by [Phil Stubbington](../Page/Phil_Stubbington.md "wikilink")
  - [Satir Institute of the Southeast](http://satirinstitute.org/) The
    Key To Congruence: Learning Opportunities Through the Satir
    Institute of the Southeast, started by Jean McLendon
  - [Satirworks: Friends of
    Satir](http://health.groups.yahoo.com/group/satirworks/) Satirworks
    is a site dedicated to conversations and work related to the late
    Virgina Satir.
  - [台灣薩提爾人文發展中心](http://www.shiuhli.org.tw)

## 註解

<references/>

[Category:心理学家](../Category/心理学家.md "wikilink")

1.