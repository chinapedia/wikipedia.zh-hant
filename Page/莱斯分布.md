在[概率论與](../Page/概率论.md "wikilink")[数理統計领域](../Page/数理統計.md "wikilink")，**萊斯分布**（Rice
distribution或Rician
distribution）是一種[连续概率分布](../Page/连续概率分布.md "wikilink")，以美国科学家[斯蒂芬·莱斯](../Page/斯蒂芬·莱斯.md "wikilink")（[:en:Stephen
O.
Rice](../Page/:en:Stephen_O._Rice.md "wikilink")）的名字命名，其[概率密度函数为](../Page/概率密度函数.md "wikilink")：

\[f(x|v,\sigma)=\,\]

\[\frac{x}{\sigma^2}\exp\left(\frac{-(x^2+v^2)}
{2\sigma^2}\right)I_0\left(\frac{xv}{\sigma^2}\right)\]
其中\(I_0(z)\)是修正的第一类零阶[貝索函數](../Page/貝索函數.md "wikilink")（[Bessel
function](../Page/:en:Bessel_function.md "wikilink")）。当\(v=0\)时，莱斯分布退化为[瑞利分布](../Page/瑞利分布.md "wikilink")。

## 矩

## 极限情况

For large values of the argument, the Laguerre polynomial becomes (See
Abramowitz and Stegun
[§13.5.1](http://www.math.sfu.ca/~cbm/aands/page_508.htm))

\[\lim_{x\rightarrow -\infty}L_\nu(x)=\frac{|x|^\nu}{\Gamma(1+\nu)}\]

It is seen that as \(v\) becomes large or \(\sigma\) becomes small the
mean becomes \(v\) and the variance becomes \(\sigma^2\)

## 相關條目

  - [Stephen O. Rice](../Page/Stephen_O._Rice.md "wikilink") (1907-1986)
  - [瑞利分布](../Page/瑞利分布.md "wikilink")
  - [莱斯衰落](../Page/莱斯衰落.md "wikilink")

## 外部連結

  - Yongjun Xie and Yuguang Fang, "A General Statistical Channel Model
    for Mobile Satellite Systems" IEEE Transactions on Vehicular
    Technology, VOL. 49, NO. 3, MAY 2000.
    <http://www.fang.ece.ufl.edu/mypaper/tvt00_xie.pdf>

[Category:连续分布](../Category/连续分布.md "wikilink")