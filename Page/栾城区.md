**栾城区**是[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[石家庄市所辖的一个](../Page/石家庄市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。原为栾城县，2014年9月撤县设石家庄市栾城区。\[1\]

## 名人

  - 春秋：晋国正卿、中军元帅[栾书](../Page/栾书.md "wikilink")
  - 汉代：棘蒲侯[柴武](../Page/柴武.md "wikilink")
  - 唐代：初唐“文章四友”之一的宰相、文学家[苏味道](../Page/苏味道.md "wikilink")
  - 元代：著名数学家[李冶](../Page/李冶.md "wikilink")、红巾军起义领袖[韩山童](../Page/韩山童.md "wikilink")、[韩林儿](../Page/韩林儿.md "wikilink")
  - 现代：水电部副部长[王英先](../Page/王英先.md "wikilink")、河北省副省长[王力](../Page/王力.md "wikilink")、山西省副省长[吴俊洲](../Page/吴俊洲.md "wikilink")、解放军总后勤部副部长[左建昌](../Page/左建昌.md "wikilink")、历史学家[乔明顺](../Page/乔明顺.md "wikilink")、全国政协委员、地质学家[李廷栋等](../Page/李廷栋.md "wikilink")。

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 外部链接

  - [栾城县政府网站](http://www.luancheng.gov.cn/)

## 参考文献

[栾城区](../Category/栾城区.md "wikilink")
[区](../Category/石家庄县级行政区.md "wikilink")
[石家庄](../Category/河北市辖区.md "wikilink")

1.