[淡水河](淡水河.md "wikilink"){{.w}}[磺溪](磺溪_\(新北市\).md "wikilink"){{.w}}[鳳山溪](鳳山溪.md "wikilink"){{.w}}[頭前溪](頭前溪.md "wikilink")
 |group3=[縣市管河川](縣市管河川.md "wikilink") |list3=

` `[`南澳溪`](南澳溪.md "wikilink")`{{.w}}`[`新城溪`](新城溪.md "wikilink")`{{.w}}`[`得子口溪`](得子口溪.md "wikilink")`{{.w}}`[`東澳溪`](東澳溪.md "wikilink")`{{.w}}`[`大溪川`](大溪川.md "wikilink")
` |group2 = `[`新北市`](新北市.md "wikilink")
` |list2 = `[`雙溪`](雙溪_\(新北市\).md "wikilink")`{{.w}}`[`尖山腳溪`](尖山腳溪.md "wikilink")`{{.w}}`[`瑪鋉溪`](瑪鋉溪.md "wikilink")`{{.w}}`[`員潭溪`](員潭溪.md "wikilink")`{{.w}}`[`小坑溪`](小坑溪_\(石門區\).md "wikilink")`{{.w}}`[`乾華溪`](乾華溪.md "wikilink")`{{.w}}`[`石門溪`](石門溪.md "wikilink")`{{.w}}`[`老梅溪`](老梅溪.md "wikilink")`{{.w}}`[`楓林溪`](楓林溪.md "wikilink")`{{.w}}`[`八甲溪`](八甲溪.md "wikilink")`{{.w}}`[`埔坪溪`](埔坪溪.md "wikilink")`{{.w}}`[`八蓮溪`](八蓮溪.md "wikilink")`{{.w}}`[`大屯溪`](大屯溪.md "wikilink")`{{.w}}`[`後洲溪`](後洲溪.md "wikilink")`{{.w}}`[`興仁溪`](興仁溪.md "wikilink")`{{.w}}`[`水仙溪`](水仙溪.md "wikilink")`{{.w}}`[`寶斗溪`](寶斗溪.md "wikilink")`{{.w}}`[`林口溪`](林口溪.md "wikilink")`{{.w}}`[`林子溪`](林子溪.md "wikilink")
` |group3 = `[`桃園市`](桃園市.md "wikilink")
` |list3 = `[`南崁溪`](南崁溪.md "wikilink")`{{.w}}`[`老街溪`](老街溪.md "wikilink")`{{.w}}`[`社子溪`](社子溪.md "wikilink")`{{.w}}`[`富林溪`](富林溪.md "wikilink")`{{.w}}`[`大堀溪`](大堀溪.md "wikilink")`{{.w}}`[`觀音溪`](觀音溪.md "wikilink")`{{.w}}`[`新屋溪`](新屋溪.md "wikilink")
` |group4 = `[`新竹縣`](新竹縣.md "wikilink")
` |list4 = `[`新豐溪`](新豐溪.md "wikilink")
` }}`

|list5=**[中臺灣](中臺灣.md "wikilink")** |group6=[中央管河川](中央管河川.md "wikilink")
|list6=[中港溪](中港溪.md "wikilink"){{.w}}[後龍溪](後龍溪.md "wikilink"){{.w}}[大安溪](大安溪.md "wikilink"){{.w}}[大甲溪](大甲溪.md "wikilink"){{.w}}[烏溪](烏溪.md "wikilink"){{.w}}[濁水溪](濁水溪.md "wikilink"){{.w}}[北港溪](北港溪.md "wikilink")
|group7=[縣市管河川](縣市管河川.md "wikilink") |list7=

` `[`通霄溪`](通霄溪.md "wikilink")`{{.w}}`[`苑裡溪`](苑裡溪.md "wikilink")`{{.w}}`[`房裡溪`](房裡溪.md "wikilink")
` |group2 = `[`臺中市`](臺中市.md "wikilink")
` |list2 = `[`溫寮溪`](溫寮溪.md "wikilink")
` |group3 = `[`雲林縣`](雲林縣.md "wikilink")
` |list3 = `[`新虎尾溪`](新虎尾溪.md "wikilink")`  `
` }}`

|list9=**[南臺灣](南臺灣.md "wikilink")**
|group10=[中央管河川](中央管河川.md "wikilink")
|list10=[朴子溪](朴子溪.md "wikilink"){{.w}}[八掌溪](八掌溪.md "wikilink"){{.w}}[急水溪](急水溪.md "wikilink"){{.w}}[曾文溪](曾文溪.md "wikilink"){{.w}}[鹽水溪](鹽水溪.md "wikilink"){{.w}}[二仁溪](二仁溪.md "wikilink"){{.w}}[阿公店溪](阿公店溪.md "wikilink"){{.w}}[高屏溪](高屏溪.md "wikilink"){{.w}}[東港溪](東港溪.md "wikilink"){{.w}}[四重溪](四重溪.md "wikilink")
|group11=[縣市管河川](縣市管河川.md "wikilink") |list11=

` `[`率芒溪`](率芒溪.md "wikilink")`{{.w}}`[`枋山溪`](枋山溪.md "wikilink")`{{.w}}`[`楓港溪`](楓港溪.md "wikilink")`{{.w}}`[`保力溪`](保力溪.md "wikilink")`{{.w}}`[`港口溪`](港口溪.md "wikilink")`{{.w}}`[`七里溪`](七里溪.md "wikilink")`{{.w}}`[`石盤溪`](石盤溪.md "wikilink")`{{.w}}`[`九棚溪`](九棚溪.md "wikilink")`{{.w}}`[`港子溪`](港子溪.md "wikilink")`{{.w}}`[`牡丹溪`](牡丹溪.md "wikilink")`{{.w}}`[`里仁溪`](里仁溪.md "wikilink")
` }}`

|list13=**[東臺灣](東臺灣.md "wikilink")**
|group14=[中央管河川](中央管河川.md "wikilink")
|list14=[和平溪](和平溪.md "wikilink"){{.w}}[花蓮溪](花蓮溪.md "wikilink"){{.w}}[秀姑巒溪](秀姑巒溪.md "wikilink"){{.w}}[卑南溪](卑南溪.md "wikilink")
 |group15=[縣市管河川](縣市管河川.md "wikilink") |list15=

` `[`美崙溪`](美崙溪.md "wikilink")`{{.w}}`[`立霧溪`](立霧溪.md "wikilink")`{{.w}}`[`三富溪`](三富溪.md "wikilink")`{{.w}}`[`豐濱溪`](豐濱溪.md "wikilink")`{{.w}}`[`加蘭溪`](加蘭溪.md "wikilink")`{{.w}}`[`薯寮溪`](薯寮溪.md "wikilink")`{{.w}}`[`三棧溪`](三棧溪.md "wikilink")`{{.w}}`[`水連溪`](水連溪.md "wikilink")`{{.w}}`[`石公溪`](石公溪.md "wikilink")`{{.w}}`[`大富溪`](大富溪.md "wikilink")`{{.w}}`[`大清水溪`](大清水溪.md "wikilink")
` `
` |group2 =`[`台東縣`](臺東縣.md "wikilink")
` |list2 = `[`知本溪`](知本溪.md "wikilink")`{{.w}}`[`利嘉溪`](利嘉溪.md "wikilink")`{{.w}}`[`太平溪`](太平溪.md "wikilink")`{{.w}}`[`塔瓦溪`](塔瓦溪.md "wikilink")`{{.w}}`[`達仁溪`](達仁溪.md "wikilink")`{{.w}}`[`安朔溪`](安朔溪.md "wikilink")`{{.w}}`[`朝庸溪`](朝庸溪.md "wikilink")`{{.w}}`[`大武溪`](大武溪.md "wikilink")`{{.w}}`[`烏萬溪`](烏萬溪.md "wikilink")`{{.w}}`[`津林溪`](津林溪.md "wikilink")`{{.w}}`[`大竹溪`](大竹溪.md "wikilink")`{{.w}}`[`金崙溪`](金崙溪.md "wikilink")`{{.w}}`[`太麻里溪`](太麻里溪.md "wikilink")`{{.w}}`[`文里溪`](文里溪.md "wikilink")`{{.w}}`[`都蘭溪`](都蘭溪.md "wikilink")`{{.w}}`[`八里溪`](八里溪.md "wikilink")`{{.w}}`[`馬武窟溪`](馬武窟溪.md "wikilink")`{{.w}}`[`成功溪`](成功溪.md "wikilink")`{{.w}}`[`富家溪`](富家溪.md "wikilink")`{{.w}}`[`都威溪`](都威溪.md "wikilink")`{{.w}}`[`沙灣溪`](沙灣溪.md "wikilink")`{{.w}}`[`寧埔溪`](寧埔溪.md "wikilink")`{{.w}}`[`竹湖溪`](竹湖溪.md "wikilink")`{{.w}}`[`大德溪`](大德溪.md "wikilink")`{{.w}}`[`長濱溪`](長濱溪.md "wikilink")`{{.w}}`[`城埔溪`](城埔溪.md "wikilink")`{{.w}}`[`馬海溪`](馬海溪.md "wikilink")`{{.w}}`[`山間溪`](山間溪.md "wikilink")`{{.w}}`[`水母溪`](水母溪.md "wikilink")
` }}`

}} <noinclude></noinclude>