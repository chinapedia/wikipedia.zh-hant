《**卒業 ～Next Graduation～**》是[IRI Commerce and
Technology於](../Page/IRI_Commerce_and_Technology.md "wikilink")2005年6月在[Windows平台上發售的教師](../Page/Windows.md "wikilink")[模擬遊戲](../Page/模擬遊戲.md "wikilink")，為《[卒業](../Page/卒業_\(遊戲\).md "wikilink")》系列繼《卒業III
～Wedding Bell～》以來暌違7年的新作品。

在台灣由[英特衛多媒體代理發行中文電腦版](../Page/英特衛多媒體.md "wikilink")。2006年3月9日在[PlayStation
2上發售了題名為](../Page/PlayStation_2.md "wikilink")《卒業 2nd Generation》的移植版。

在中國大陆由[北京娱乐通公司代理PC版](../Page/北京娱乐通.md "wikilink").
2006年10月30日发布了豪华版（与[见习天使捆绑](../Page/见习天使.md "wikilink")），稍后发行了普通版，名为「**毕业生
Next Graduation——华青女子中学恋爱物语**」。

## 概要

承襲系列作品的特徵，本作品遊玩家扮演5個問題學生的導師，教導她們順利畢業。

## 故事前情

過去就讀過清華學園高等部的傳說5人組——新井聖美、加藤美夏、-{志}-村麻美（）、高城麗子、中本静，在學校裡製造了各式大大小小的問題；經由某位新任教師嚴厲但溫馨的指導後，順利地從學校畢業。然而，清華學園又即將再次掀起軒然大波：當時的教師重回清華學園教書，擔任清華學園中等部3年B班的導師，而轉校過來的竟然是當時學生的女兒。

## 人物

  - 新井家：
    ; 新井聖美 （聲優：[鶴弘美](../Page/鶴弘美.md "wikilink")）
    ; 新井勝美 （聲優：[吉原ナツキ](../Page/吉原ナツキ.md "wikilink")）
    高城家：
    ; 高城麗子（聲優：[冬馬由美](../Page/冬馬由美.md "wikilink")）
    ; （聲優：[植田佳奈](../Page/植田佳奈.md "wikilink")）
    加藤家：
    ; 加藤美夏（聲優：[嶋方淳子](../Page/嶋方淳子.md "wikilink")）
    ; 加藤結夏（聲優：[鹿野優以](../Page/鹿野優以.md "wikilink")）
    志村家：
    ; 志村麻美（）（聲優：[芳野日向子](../Page/芳野日向子.md "wikilink")）
    ; （聲優：）
    中本家：
    ; 中本静（聲優：[久川綾](../Page/久川綾.md "wikilink")）
    ; 中本梓（聲優：[鎌田梢](../Page/鎌田梢.md "wikilink")）
    副導師：
    ; 諏訪優美子（聲優：[満仲由紀子](../Page/満仲由紀子.md "wikilink")）

## 外部連結

  - [卒業 Next
    Generation](https://web.archive.org/web/20050317024001/http://graduation.jp/)（線上備份）


[Category:2005年电子游戏](../Category/2005年电子游戏.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:師生戀題材作品](../Category/師生戀題材作品.md "wikilink")
[Category:日本開發電子遊戲](../Category/日本開發電子遊戲.md "wikilink")
[Category:模擬遊戲](../Category/模擬遊戲.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:教師主角作品](../Category/教師主角作品.md "wikilink")
[Category:電擊文庫](../Category/電擊文庫.md "wikilink")