**戈登縣**（**Gordon County,
Georgia**）是[美國](../Page/美國.md "wikilink")[喬治亞州西北部的一個縣](../Page/喬治亞州.md "wikilink")。面積927平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口44,104人。縣治[卡爾霍恩](../Page/卡爾霍恩_\(喬治亞州\).md "wikilink")
(Calhoun)。

成立於1850年2月13日。縣名紀念曾任軍官、[喬治亞中央鐵路第一任主席的](../Page/喬治亞中央鐵路.md "wikilink")[威廉·華盛頓·戈登](../Page/威廉·華盛頓·戈登.md "wikilink")
（William Washington Gordon）\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/佐治亚州行政区划.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.