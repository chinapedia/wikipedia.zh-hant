[HK_Yuen_Long_元朗_Tsoi_Uk_Tsuen_蔡屋村_Warning_notic_Rules.jpg](https://zh.wikipedia.org/wiki/File:HK_Yuen_Long_元朗_Tsoi_Uk_Tsuen_蔡屋村_Warning_notic_Rules.jpg "fig:HK_Yuen_Long_元朗_Tsoi_Uk_Tsuen_蔡屋村_Warning_notic_Rules.jpg")

**新界小型屋宇政策**（），俗稱**丁屋**政策，是[香港](../Page/香港.md "wikilink")[新界原居民的](../Page/新界原居民.md "wikilink")[男性後人](../Page/男性.md "wikilink")（即「男丁」）獲准在私人土地興建的[房屋](../Page/住宅.md "wikilink")，為[香港殖民地時期沿用至今的一項政策](../Page/英屬香港.md "wikilink")。

## 歷史

傳統以來，新界居民均於村落內或鄰近的私人農地或荒地之上興建房屋居住。而自[英國租借新界後](../Page/香港割讓#租借新界.md "wikilink")，有關的傳統一直延續，而以政府向居民批出「建屋牌」或以換地重批的方式進行。及至1970年代政府計劃發展[新界](../Page/新界.md "wikilink")，為了得到新界原居民的支持，當時的[香港政府於](../Page/英屬香港#政治.md "wikilink")1972年12月實施的「小型屋宇政策」，規定年滿18歲，父系源自1890年代新界認可鄉村\[1\]居民的男性香港原居民，每人一生可申請一次於認可範圍內建造一座最高3層（上限27[呎](../Page/呎.md "wikilink")／8.22米高），每層面積不超過700平方呎的丁屋\[2\]，無需向政府補地價。

1972年11月29日，新界民政署長的[黎敦義在向立法局宣布丁屋政策時](../Page/黎敦義.md "wikilink")，已強調丁屋只是一項中短期措施（）。目的是希望藉興建丁屋，讓原居民獲得環境較佳的居所，因新界當年只有7%房屋是屬於正常的永久性房屋，其餘93%統統是老舊村屋、臨時性房屋甚至是非法搭建物，故准許原居民興建丁屋是迫在眉睫。有評論認為這反映丁屋只是臨時的恩恤政策，並非承認原居民擁有特權\[3\]。新界民政署（現合併於[民政事務總署](../Page/民政事務總署.md "wikilink")）內部解密檔案指出港英政府當時因為遺漏「無屋住才可建丁屋」的審批條款，變成男性原居民人人可建丁屋\[4\]。1977年，新界民政署作出內部檢討，報告指丁屋濫用問題嚴重，西貢民政專員指西貢有1000個丁屋申請，惟只有很小比例涉及真正住屋需要，風景較佳的[西貢區和](../Page/西貢區.md "wikilink")[離島區的丁屋](../Page/離島區.md "wikilink")，被市區人士認為是理想[度假屋](../Page/度假屋.md "wikilink")\[5\]。而丁屋5年禁止轉讓條款變得無效\[6\]。

政府同時訂立「限制買賣轉讓條款」，規定擁有丁屋的原居民如果想把丁屋出售及轉讓予非原居民，需向政府申請作[補地價](../Page/補地價.md "wikilink")，並取得地政專員書面同意，才可進行。興建丁屋的土地，只可是位於新界（包括[離島](../Page/香港離島.md "wikilink")）的認可村落村界內的「鄉村式發展用地」或農地。根據[新界鄉議局的估計](../Page/新界鄉議局.md "wikilink")，擁有申建丁屋權利（俗稱「**丁權**」）的男性原居民有24萬。

1973年制訂的《差餉條例》則規定在鄉村範圍內的屋宇，包括丁屋，可獲豁免繳交[差餉](../Page/差餉.md "wikilink")。1987年，政府把條例作出修訂，丁屋及村屋需取得由地政處發出的豁免紙後，才可興建。

在1972年至2011年期間，地政總署共批出36,912宗興建丁屋的申請。\[7\]

## 改革及爭議

### 統合丁權

1995年8月，香港政府曾經檢討新界小型屋宇政策。雖然根據《[香港基本法](../Page/香港基本法.md "wikilink")》第40條規定，新界原居民的原有合法傳統權益，在[香港主權移交後仍然受到保護](../Page/香港主權移交.md "wikilink")，但在1997年7月1日前，仍有大量新界原居民向[香港地政總署提出興建丁屋申請](../Page/香港地政總署.md "wikilink")，令丁屋申請一直積壓。地政總署擬於2004年4月1日開始實施「新審理丁屋申請程序」，但其後因原居民反對而暫時擱置。

[合和實業主席](../Page/合和實業.md "wikilink")[胡應湘曾建議政府實施](../Page/胡應湘.md "wikilink")「丁權[證券化](../Page/證券.md "wikilink")」，讓擁有興建丁屋權的原居民，可以將發展權在市場上自由買賣，以增加新界土地供應。

由於[香港可供發展的土地越來越少](../Page/香港土地利用.md "wikilink")，為了善用土地資源，政府在2006年2月建議放寬[地積比率](../Page/地積比率.md "wikilink")，嘗試准許原居民興建超過3層的丁屋。政府計劃先在[沙田](../Page/沙田.md "wikilink")[排頭村](../Page/排頭.md "wikilink")、上[禾輋村](../Page/禾輋.md "wikilink")，及[元朗](../Page/元朗.md "wikilink")[蝦尾新村進行試驗](../Page/蝦尾新村.md "wikilink")，興建高達20層的多層丁屋，預料可提供2000個丁屋單位，希望可解決積壓的申請個案，不過建議後來不了了之\[8\]。長遠而言，政府亦正研究「一次性」解决新界男丁興建丁屋的安排。

### 反對丁權

據一份撰寫於1980年、現已解密的新界民政署（現合併於[民政事務總署](../Page/民政事務總署.md "wikilink")）內部報告，顯示香港政府當年因漏寫若無足夠居住空間（），才可獲批建丁屋的審批條款，結果因這錯誤，令新界原居民男丁人人可建丁屋。該報告又指出，丁屋政策實施5年後即1977年，已出現嚴重濫用問題\[9\]。

一些非原居民的香港市民，質疑丁屋制度令新界原居民享有特權。[聯合國消除對婦女歧視委員會](../Page/聯合國.md "wikilink")，曾表示只有男性可享有丁權的丁屋政策，對女性造成歧視。

2012年，時任[發展局長](../Page/發展局長.md "wikilink")[林鄭月娥指出](../Page/林鄭月娥.md "wikilink")[新界原居民之丁權](../Page/新界原居民.md "wikilink")，不能無限期維持下去，建議[2047年後停止](../Page/2047大限.md "wikilink")[丁屋政策](../Page/丁屋.md "wikilink")，以2029年為劃界線，之後出生之新界男丁不再享有丁權\[10\]。

2013年9月12日，《[大公報](../Page/大公報.md "wikilink")》認為大量[內地來港新移民蜂擁而入不是造成香港住房短缺的主要原因](../Page/香港新移民.md "wikilink")，而是香港現有住房問題很大程度上是由於[新界原居民大量非法佔用土地](../Page/新界原居民.md "wikilink")，以及受到「新界小型屋宇」政策的影響\[11\]。

2015年，「覆核王」[郭卓堅入稟](../Page/郭卓堅.md "wikilink")[香港高等法院提出](../Page/香港高等法院.md "wikilink")[司法覆核](../Page/司法覆核.md "wikilink")，就丁屋政策入稟。法官在2019年4月13日頒下判詞，裁定涉及政府用地的私人協約方式，或以換地方式批出的丁權違憲，但暫緩執行判決6個月。[鄉議局對此表示非常遺憾及失望](../Page/鄉議局.md "wikilink")，表示考慮上訴。\[12\]

## 罪案與違規

### 違法購買丁權

2015年12月4日，丁屋發展商透過[沙田鄉事委員會主席](../Page/沙田鄉事委員會.md "wikilink")[莫錦貴等多名中間人](../Page/莫錦貴.md "wikilink")，向多名原居民違法購買其丁權，並在申請興建丁屋時詐騙[地政總署](../Page/地政總署.md "wikilink")。區域法院經審訊後，裁定發展商與11名原居民串謀詐騙罪成，成為首宗「套丁」被定罪的案件，引發「套丁」是否涉及刑事罪行的爭議。\[13\]

### 違規僭建

[Ads_of_No_Unauthorized_Building_Works_in_Village_Houses_(Hong_Kong).JPG](https://zh.wikipedia.org/wiki/File:Ads_of_No_Unauthorized_Building_Works_in_Village_Houses_\(Hong_Kong\).JPG "fig:Ads_of_No_Unauthorized_Building_Works_in_Village_Houses_(Hong_Kong).JPG")車身貼上屋宇署的「無僭建村屋
安居又幸福」宣傳廣告\]\]

據發展局副秘書長鄭偉源估計，新界村屋僭建個案數以萬計\[14\]。發展局長[林鄭月娥曾於](../Page/林鄭月娥.md "wikilink")2011年年底表明政府鐵定於2012年4月起，嚴厲執法取締僭建村屋，首輪目標是三層以上的村屋，屋宇署並會逐一巡查新界六百條村\[15\]。

2012年4月1日，為遏止新僭建物的出現及保障新界村屋的樓宇結構安全，屋宇署開始推行「新界村屋申報計畫」，並將新界分為九區派員入村巡查，首輪取締目標是四層或以上或的僭建村屋。[鄉議局則表明](../Page/鄉議局.md "wikilink")，凡收到清拆令的，會向政府提出訴訟\[16\]。

在「新界村屋僭建物申報計劃」下，新界村屋的業主可為符合資格參加「申報計劃」的僭建物向屋宇署申報。已申報的僭建物，除非有迫切危險，否則不會在首輪取締目標執法階段被強制即時清拆\[17\]。但是，有鄉事委員會委員批評政府選擇性實施登記制度，登記制度只適用於新界，對新界居民不公平。鄉事委員會並號召原居民不要配合申報計劃\[18\]。惟時任發展局長林鄭月娥一再重申在新界村屋僭建的問題上會依法辦事，不會有特赦。

## 評價

《[01周報](../Page/香港01.md "wikilink")》有評論指出，「丁權」制度強調男性子孫的[財產繼承權](../Page/財產繼承權.md "wikilink")，是傳統中國法律的遺存，是「[中華法系](../Page/中華法系.md "wikilink")」在現代社會裏所留下的一定的痕跡\[19\]。

## 影視文化

  - 《[竊聽風雲3](../Page/竊聽風雲3.md "wikilink")》，一部以描寫香港的丁屋計劃所衍生的社會題材電影。

## 資料來源

## 外部連結

  - 《[胡應湘倡丁權證券化 自由售賣發展權
    增新界土地供應](http://hk.news.yahoo.com/060120/12/1kl90.html)》，[明報](../Page/明報.md "wikilink")，2006年1月21日
  - 《[政府擬批建20層丁屋 沙田元朗試行
    提供2000單位](http://hk.news.yahoo.com/060228/12/1lmjc.html)》，[明報](../Page/明報.md "wikilink")，2006年3月1日
  - [如何申請批准建造小型屋宇](http://www.landsd.gov.hk/tc/legco/house.htm) -
    [地政總署](../Page/地政總署.md "wikilink")
  - [新界小型屋宇政策](https://web.archive.org/web/20070930191058/http://heungyeekuk.org/009a.htm)
    - [新界鄉議局](../Page/新界鄉議局.md "wikilink")
  - [「你繞道、我杯葛」-丁屋審批制度變革風波](https://web.archive.org/web/20070327044859/http://hkila.org.hk/laq/summer2004/shpolicy.htm)
  - [村屋與丁屋法律資料-美聯物業](https://web.archive.org/web/20060208030549/http://www.midland.com.hk/agency/chi/campus/bcourse/lawtalk/note3a.shtml)
  - [西貢村屋](https://web.archive.org/web/20080923001959/http://www.saikung-hk.com/)

[Category:香港建築](../Category/香港建築.md "wikilink")
[Category:香港房屋](../Category/香港房屋.md "wikilink")
[Category:香港新界原居民](../Category/香港新界原居民.md "wikilink")

1.

2.

3.

4.
5.
6.

7.

8.

9.
10.

11.

12.

13.

14.

15.

16.

17.

18.

19.