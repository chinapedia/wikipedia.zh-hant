**罂粟科**（[學名](../Page/學名.md "wikilink")：）植物广泛分布在全世界[温带和](../Page/温带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，大部分[种类是](../Page/种.md "wikilink")[草本](../Page/草本.md "wikilink")，也有少数是[灌木或小](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")，整个植株都有导管系统，分泌[白色](../Page/白色.md "wikilink")、[黄色或](../Page/黄色.md "wikilink")[红色的汁液](../Page/红色.md "wikilink")。单[叶互生或对生](../Page/叶.md "wikilink")，无托叶，常分裂。[花两性](../Page/花.md "wikilink")，虫媒，有少数种是风媒花，单生，有萼片和[花冠分离](../Page/花冠.md "wikilink")，只有其中的[博落回属没有花瓣](../Page/博落回属.md "wikilink")，花多大而鲜艳，无香味。

[Argemone_mexicana_02.jpg](https://zh.wikipedia.org/wiki/File:Argemone_mexicana_02.jpg "fig:Argemone_mexicana_02.jpg")
(*Argemone mexicana*)\]\]

花瓣4-6或8-12，雄蕊分离有16-60个，分为两轮；雌蕊符合，心皮有2-100个合成一室；子房上位，蒴果，成熟后裂开放出种子，种子很小，胚乳油质。
[California_Poppy_Eschscholzia_californica_02.jpg](https://zh.wikipedia.org/wiki/File:California_Poppy_Eschscholzia_californica_02.jpg "fig:California_Poppy_Eschscholzia_californica_02.jpg")
(*Eschscholzia californica*)\]\]

本科植物几乎都含有[生物碱](../Page/生物碱.md "wikilink")，许多种类有毒，如误食[金杯罂粟可导致水肿或](../Page/金杯罂粟.md "wikilink")[青光眼](../Page/青光眼.md "wikilink")，[动物例如](../Page/动物.md "wikilink")[山羊吃了它](../Page/山羊.md "wikilink")，乳汁也会含有毒素。

## 分类

1981年的[克朗奎斯特分类法单独设立](../Page/克朗奎斯特分类法.md "wikilink")[罂粟目](../Page/罂粟目.md "wikilink")，2003年的[APG
II
分类法将罂粟科分入](../Page/APG_II_分类法.md "wikilink")[毛茛目](../Page/毛茛目.md "wikilink")，并建议可以和[荷包牡丹科以及](../Page/荷包牡丹科.md "wikilink")[蕨叶草科合并](../Page/蕨叶草科.md "wikilink")，如果不包括上述两[科](../Page/科.md "wikilink")，罂粟科有26[属大约](../Page/属.md "wikilink")250[种](../Page/种.md "wikilink")。[中国有](../Page/中国.md "wikilink")11[属](../Page/属.md "wikilink")55[种](../Page/种.md "wikilink")。

### 属

[Starr_010714-0013_Bocconia_frutescens.jpg](https://zh.wikipedia.org/wiki/File:Starr_010714-0013_Bocconia_frutescens.jpg "fig:Starr_010714-0013_Bocconia_frutescens.jpg")
(*Bocconia frutescens*)\]\]
[Argemone.mexicana3web.jpg](https://zh.wikipedia.org/wiki/File:Argemone.mexicana3web.jpg "fig:Argemone.mexicana3web.jpg")

  - [Arctomecon](../Page/Arctomecon.md "wikilink")''
  - [蓟罂粟属](../Page/蓟罂粟属.md "wikilink") *Argemone*
  - [肖博落回属](../Page/肖博落回属.md "wikilink") *Bocconia*
  - [Canbya](../Page/Canbya.md "wikilink")''
  - [白屈莱属](../Page/白屈莱属.md "wikilink") *Chelidonium*
  - [木罂粟属](../Page/木罂粟属.md "wikilink") *Dendromecon*
  - [秃疮花属](../Page/秃疮花属.md "wikilink") *Dicranostigma*
  - [血水草属](../Page/血水草属.md "wikilink") *Eomecon*
  - [花菱草属](../Page/花菱草属.md "wikilink") *Eschscholzia*
  - [海罂粟属](../Page/海罂粟属.md "wikilink") *Glaucium*
  - [金杯罂粟属](../Page/金杯罂粟属.md "wikilink") *Hunnemannia*
  - [荷青花属](../Page/荷青花属.md "wikilink") *Hylomecon*
  - [博落回属](../Page/博落回属.md "wikilink") *Macleaya*
  - [Meconella](../Page/Meconella.md "wikilink")''
  - [绿绒篙属](../Page/绿绒篙属.md "wikilink") *Meconopsis*
  - [罂粟属](../Page/罂粟属.md "wikilink") *Papaver*
  - [宽丝罂粟属](../Page/宽丝罂粟属.md "wikilink") *Platystemon*
  - *[Platystigma](../Page/Platystigma.md "wikilink")*
  - [疆罂粟属](../Page/疆罂粟属.md "wikilink") *Roemeria*
  - [大罂粟属](../Page/大罂粟属.md "wikilink")*Romneya*
  - [血根草属](../Page/血根草属.md "wikilink") *Sanguinaria*
  - [风罂粟属](../Page/风罂粟属.md "wikilink")*Stylomecon*
  - [金罂粟属](../Page/金罂粟属.md "wikilink") *Stylophorum*

## 应用

本科[植物由于花色鲜艳](../Page/植物.md "wikilink")，被广泛种植作为观赏植物，稱為[罌粟花](../Page/罌粟花.md "wikilink")，如[花菱草](../Page/花菱草.md "wikilink")(*Eschscholtzia
californica*)、[虞美人](../Page/虞美人_\(花卉\).md "wikilink")(*Papaver
rhoeas*)等，其中也有许多种类的种子可以作为去烹饪和烘烤面包的添加剂；但[罂粟](../Page/罂粟.md "wikilink")(*Papaver
somniferum*)是制取[鸦片毒品的主要来源](../Page/鸦片.md "wikilink")，成为扫毒摧毁的主要目标。

## 外部链接

  - L. Watson和M.J. Dallwitz
    (1992年)《[显花植物分类](http://delta-intkey.com/angio/)》中的[罂粟科](https://web.archive.org/web/20060320121514/http://delta-intkey.com/angio/www/papavera.htm)
  - [《北美植物》中的罂粟科](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=10650)
  - [J.L.Reveal](https://web.archive.org/web/20070311000831/http://www.life.umd.edu/emeritus/Reveal/PBIO/pb450/magn6.html)
  - [NCBI分类的罂粟科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=3465&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)
  - [links at
    CSDL分类](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Papaveraceae)
  - \[<http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=family_latin='Papaveraceae>'
    罌粟科 Papaveraceae\] 藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/罂粟科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")