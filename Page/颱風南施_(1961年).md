**颱風南施**（，國際編號：**6118**，中國大陸編號：**6123**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**18W**）是[1961年太平洋台风季的一個](../Page/1961年太平洋台风季.md "wikilink")[热带气旋](../Page/热带气旋.md "wikilink")，风暴在9月7日形成，在9月17日消散，维持了10天。南施在[日本](../Page/日本.md "wikilink")[本州登陆之前](../Page/本州.md "wikilink")，影响了[關島](../Page/關島.md "wikilink")、[日本和](../Page/日本.md "wikilink")[琉球群岛](../Page/琉球群岛.md "wikilink")，並為該區造成巨大損失。南施的一分鐘最高平均風速為約345[公里每小时](../Page/公里每小时.md "wikilink")（215[英里每小时](../Page/英里每小时.md "wikilink")）或185[節](../Page/節_\(單位\).md "wikilink")，可能成為全球有紀錄以來風速最高的熱帶氣旋（但一般認為當時的氣象機構傾向高估熱帶氣旋的風速，因此南施的風力數據仍有爭議）。由於南施為日本帶來嚴重破壞，故[日本氣象廳為它命名為](../Page/氣象廳_\(日本\).md "wikilink")**第二室戶颱風**（），是為氣象廳8個命名颱風其中之一。

## 氣象歷史

一個[熱帶低氣壓於](../Page/熱帶低氣壓.md "wikilink")1961年9月7日在[瓜加林環礁附近](../Page/瓜加林環礁.md "wikilink")（[北緯](../Page/北緯.md "wikilink")8.8度，[東經](../Page/東經.md "wikilink")161.6度）形成，在不遠處先後迅速增強為[熱帶風暴及](../Page/熱帶風暴.md "wikilink")[颱風](../Page/颱風.md "wikilink")，命名為南施。受惠著西太平洋的水溫高、幅散及幅合強勁和[風切變低的環境下](../Page/風切變.md "wikilink")，南施漸漸向西移動及爆發性增強，並於9月9日增強為五級颱風。\[1\]在隨後幾天仍能維持其強度，於9月12日達到[聯合颱風警報中心所定一分鐘平均風速](../Page/聯合颱風警報中心.md "wikilink")185[節](../Page/節_\(單位\).md "wikilink")（345[公里每小時](../Page/公里每小時.md "wikilink")）的顛峰強度，其中心最低[氣壓低至](../Page/氣壓.md "wikilink")882[百帕斯卡](../Page/百帕斯卡.md "wikilink")。

稍後，南施在[琉球群岛登陸](../Page/琉球群岛.md "wikilink")，並轉向偏北方向移動，掠過[沖繩縣及](../Page/沖繩縣.md "wikilink")[奄美大島](../Page/奄美大島.md "wikilink")。之後轉向東北方向移動，趨向[日本](../Page/日本.md "wikilink")[本州一帶](../Page/本州.md "wikilink")。9月16日上午9時在[室戶岬以西登陸](../Page/室戶岬.md "wikilink")，4小時後在[兵庫縣](../Page/兵庫縣.md "wikilink")[尼崎市與](../Page/尼崎市.md "wikilink")[西宮市之間再次登陸](../Page/西宮市.md "wikilink")。登陸後的南施迅速橫過本州，且速度不斷加快，達到顛峰移動速度100[公里每小時](../Page/公里每小時.md "wikilink")（65[英里每小時](../Page/英里每小時.md "wikilink")）或55[節](../Page/節_\(單位\).md "wikilink")。\[2\]後南施掠過[北海道](../Page/北海道.md "wikilink")，隨後進入[鄂霍次克海並減弱為熱帶風暴](../Page/鄂霍次克海.md "wikilink")，於9月17日受[西風槽影響而轉變成](../Page/西風槽.md "wikilink")[溫帶氣旋](../Page/溫帶氣旋.md "wikilink")，最後經過[堪察加半島進入](../Page/堪察加半島.md "wikilink")[白令海](../Page/白令海.md "wikilink")。\[3\]

## 影響

雖然事後仍未能計算所有在貨幣價值上的損失，但南施在[關島和](../Page/關島.md "wikilink")[日本所帶來的破壞卻是](../Page/日本.md "wikilink")「驚人的」。\[4\]是次風災共釀成173人死亡，19人失蹤。然而「南施」這個熱帶氣旋名字並無退役，並多次用於往後在西北[太平洋生成的颱風](../Page/太平洋.md "wikilink")，直至[1989年後正式退役](../Page/1989年太平洋颱風季.md "wikilink")。\[5\]

###

在關島地區，全島有過半[農作物在強風和大雨下受到破壞](../Page/農作物.md "wikilink")。島上[道路亦受到破壞](../Page/道路.md "wikilink")，損失近40,000[美元](../Page/美元.md "wikilink")（以1961年計）。在南部的災情較為嚴重，幸島上無人死亡。\[6\]

### 琉球群島

在[沖繩縣內](../Page/沖繩縣.md "wikilink")，低窪地區受到嚴重水浸，[農田和](../Page/農田.md "wikilink")[建築物受到破壞](../Page/建築物.md "wikilink")，但無人死亡。\[7\]而在[奄美大島則有](../Page/奄美大島.md "wikilink")1人失蹤及1人受重傷，1艘船隻沉沒。當地的水浸為農作物和房屋造成破壞，152人無家可歸。\[8\]

###

在日本，有172人在風災中死亡、18人失蹤、另有3,184人受傷，是為日本史上第六多人死亡的風災。及時的熱帶氣旋警告以及防風準備令死亡人數減低。南施所帶來的破壞，比起其他颱風在日本人口密集地區所帶來的破壞相對地「較小」。\[9\]

南施為日本帶來破壞，有11,539間房子被吹毀，32,604間住家被破壞，另有280,078間被淹浸。[美國國防部旗下的官方報章](../Page/美國國防部.md "wikilink")《[星條旗報](../Page/星條旗報.md "wikilink")》於9月尾報道，當地有1,056艘船隻和漁船沉沒或在岸邊擱淺，當中大多受到損壞。\[10\]南施所帶來的洪水沖毀566座[橋樑](../Page/橋樑.md "wikilink")，並造成1,146起[山泥傾瀉](../Page/山泥傾瀉.md "wikilink")，2,053處道路被毀。\[11\]
在[大阪共造成](../Page/大阪.md "wikilink")5億美元（以1961年計）的損失。\[12\]

由於南施為日本帶來嚴重破壞，故[日本氣象廳為它命名為](../Page/氣象廳_\(日本\).md "wikilink")**第二室戶颱風**，是為氣象廳8個命名颱風其中之一。

## 紀錄

[聯合颱風警報中心在](../Page/聯合颱風警報中心.md "wikilink")9月12日派遣一架[偵察機到南施的中心](../Page/偵察機.md "wikilink")，並錄得其一分鐘持續風速為約345[公里每小时](../Page/公里每小时.md "wikilink")（215[英里每小时](../Page/英里每小时.md "wikilink")）或185[節](../Page/節_\(單位\).md "wikilink")；而[中央氣象局則測出中心最大風速](../Page/中央氣象局.md "wikilink")100公尺/秒\[13\]，氣壓846hPa（後來修正為882hPa）。如果這些數據屬實的話，那麼南施會成為全球有紀錄以來風速最高的[熱帶氣旋](../Page/熱帶氣旋.md "wikilink")。\[14\]可是，那個時代尚未有衛星，風速都是人工性預報推估數據，後來[美国国家海洋和大气管理局發現在](../Page/美国国家海洋和大气管理局.md "wikilink")1940年代至1960年代期間對熱帶氣旋風速的計算被高估。換言之，南施的實際風速可能低於官方公佈的數值。

雖然當時[萨菲尔-辛普森飓风等级尚未設立](../Page/萨菲尔-辛普森飓风等级.md "wikilink")，但後來經過統計，南施能夠維持五級颱風長達五天半（或132小時），成為[北半球有紀錄以來維持五級颱風時期最長的熱帶氣旋](../Page/北半球.md "wikilink")。\[15\]

## 參看

  - [1961年太平洋颱風季](../Page/1961年太平洋颱風季.md "wikilink")
  - [室戶颱風](../Page/室戶颱風.md "wikilink")

## 參考

## 外部連結

  - [颱風南施報告](https://web.archive.org/web/20110607040559/http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1961atcr/pdf/wnp/52.pdf)，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")

  - [颱風南施資料](http://www.data.jma.go.jp/obd/stats/data/bosai/report/1961/19610915/19610915.html)，[日本氣象廳](../Page/氣象廳_\(日本\).md "wikilink")

  - [颱風南施資料](http://rdc28.cwb.gov.tw/data.php?num=1961180908&year=1961&c_name=%ABn%ACI&e_name=NANCY)，[中央氣象局](../Page/中央氣象局.md "wikilink")

  - [颱風南施路徑圖](https://web.archive.org/web/20060312104623/http://www.weather.unisys.com/hurricane/w_pacific/1961/18/track.gif)

[N](../Category/五级热带气旋.md "wikilink")
[Category:影響臺灣的熱帶氣旋](../Category/影響臺灣的熱帶氣旋.md "wikilink")
[Category:影響日本的熱帶氣旋](../Category/影響日本的熱帶氣旋.md "wikilink")
[Category:影响关岛的热带气旋](../Category/影响关岛的热带气旋.md "wikilink")
[Category:1961年日本](../Category/1961年日本.md "wikilink")
[Category:1961年台灣](../Category/1961年台灣.md "wikilink")
[Category:台风](../Category/台风.md "wikilink")

1.

2.

3.

4.
5.
6.
7.
8.
9.
10.
11.
12. David Longshore *Encyclopedia of Hurricanes, Typhoons, and Cyclones*
    pg. 233

13. 當時中央氣象局的颱風風速為1分鐘測得風速

14.

15.