**大甲西社抗清事件**，又稱**[大甲西社事件](../Page/大甲西社事件.md "wikilink")**，為台灣原住民[中部平埔族群歷史上最大的民眾起義事件](../Page/中部平埔族群.md "wikilink")，以[大甲西社的](../Page/大甲西社.md "wikilink")[林武力](../Page/林武力.md "wikilink")（姓名漢化的[道卡斯族人](../Page/道卡斯族.md "wikilink")）為首，發生於[台灣清領時期](../Page/台灣清領時期.md "wikilink")[雍正年間](../Page/雍正.md "wikilink")，持續長達一年。事件過後不僅[漢人與](../Page/漢人.md "wikilink")[平埔各族群間傷亡慘重](../Page/平埔.md "wikilink")，也讓[清廷於平埔各族境內加強駐兵](../Page/清廷.md "wikilink")，加強控制平埔族群。此「番亂」名稱，來自[福建](../Page/福建.md "wikilink")[提督](../Page/提督.md "wikilink")[許良彬奏摺所稱](../Page/許良彬.md "wikilink")。

此外，事件爆發後，[鳳山總兵奉令率軍前往大甲協助平定](../Page/鳳山.md "wikilink")，導致鳳山守備鬆散，曾參與[朱一貴事件的舊部便趁機起事](../Page/朱一貴事件.md "wikilink")，造成[鳳山的](../Page/鳳山.md "wikilink")[吳福生事件](../Page/吳福生事件.md "wikilink")\[1\]。

## 事件經過

1731年（清[雍正九年](../Page/雍正.md "wikilink")）年末，原住民族[道卡斯族位於](../Page/道卡斯族.md "wikilink")[大甲一帶的](../Page/大甲區.md "wikilink")[大甲西社](../Page/大甲西社.md "wikilink")，因清廷官吏指派勞役過多而群起反抗，發動武裝抗官行動；燒毀[臺灣府淡水撫民同知衙門](../Page/臺灣府淡水撫民同知.md "wikilink")，殺傷衙役兵丁，同知[張弘章逃逸](../Page/張弘章.md "wikilink")。事發之初，時任[臺灣鎮總兵](../Page/臺灣鎮總兵.md "wikilink")[呂瑞麟正好北巡到北台灣](../Page/呂瑞麟.md "wikilink")，聞變趕回貓盂（在今[苗栗縣](../Page/苗栗縣.md "wikilink")[苑里鎮](../Page/苑里鎮.md "wikilink")），被原住民起事者圍困。隨後，呂某突圍入彰化縣治，並向[臺灣府徵兵合攻](../Page/臺灣府.md "wikilink")，仍未平復。

1732年八月，負責征討大甲西社亂事的[福建分巡臺灣道](../Page/福建分巡臺灣道.md "wikilink")[倪象愷的劉姓表親為求立功](../Page/倪象愷.md "wikilink")，竟將[大肚社](../Page/大肚社.md "wikilink")（在今臺中市[大肚區](../Page/大肚區.md "wikilink")）五名前來幫助官府運糧的「良番」（歸化的[原住民](../Page/原住民.md "wikilink")）斬首，謊稱這五人是大甲西社的「作亂生番」。此事引起已歸化原住民的不滿，群湧[彰化縣城理論抗議](../Page/彰化縣_\(清朝\).md "wikilink")；知縣隨意敷衍，引起大肚社原住民不滿，於是聯合原[大肚王國成員之部落](../Page/大肚王國.md "wikilink")，即[南大肚社](../Page/南大肚社.md "wikilink")、[沙轆社](../Page/沙轆社.md "wikilink")（在今臺中市[沙鹿區](../Page/沙鹿區.md "wikilink")）、[牛罵頭社](../Page/牛罵頭社.md "wikilink")（在今臺中市[清水區](../Page/清水區_\(臺中市\).md "wikilink")）、[樸子籬社](../Page/樸子籬社.md "wikilink")（今臺中市[東勢區](../Page/東勢區.md "wikilink")），[吞霄社](../Page/吞霄社.md "wikilink")（今苗栗縣[通霄鎮](../Page/通霄鎮.md "wikilink")）、[阿里史社](../Page/阿里史社.md "wikilink")（今臺中市[潭子區](../Page/潭子區.md "wikilink")）等十餘社的[平埔族群原住民約兩千餘人圍攻彰化縣城](../Page/平埔族.md "wikilink")，焚燒附近數十里民房，漢人百姓逃散。

事件擴大後，其他各社如[蓬山社](../Page/蓬山社.md "wikilink")（今大甲）、[貓羅社](../Page/貓羅社.md "wikilink")（今彰化縣[芬園鄉](../Page/芬園鄉.md "wikilink")）、[岸裡社](../Page/岸裡社.md "wikilink")（今臺中市[神岡區](../Page/神岡區.md "wikilink")）、水裡（今臺中[龍井區](../Page/龍井區.md "wikilink")）、[阿束社](../Page/阿束社.md "wikilink")（今彰化市茄冬腳、彰化縣和美鎮）等原住民番社也起而響應。至此[大安溪到](../Page/大安溪.md "wikilink")[大甲溪之間](../Page/大甲溪.md "wikilink")，原屬於[大肚王國領域內的山谷平原都陷入動盪之中](../Page/大肚王國.md "wikilink")。這是清領時期平埔族群武力反抗事件中規模最大的一次，當時彰化縣（大約為今臺中市、彰化縣之平原地區）境內平埔族各社幾乎全部參加了此行動。

## 派兵鎮壓

當時署理[福建總督](../Page/閩浙總督.md "wikilink")[郝玉麟派](../Page/郝玉麟.md "wikilink")[臺灣總兵](../Page/臺灣總兵.md "wikilink")[王郡親往督征](../Page/王郡.md "wikilink")，並先後從[中國大陸徵調官兵六千多名](../Page/中國大陸.md "wikilink")，以及[銀錢](../Page/銀錢.md "wikilink")、[米糧](../Page/白米.md "wikilink")、[軍火](../Page/軍火.md "wikilink")、[器械](../Page/冷兵器.md "wikilink")，船載一百多艘來臺征討。九月初，清軍採「以番制番」之策，使各敵對之部落互相攻擊，並得[岸裡社部分原住民的協助](../Page/岸裡社.md "wikilink")，襲破阿束社。十月中旬清軍兵分七路，攻破水裡社、牛罵頭社及沙轆社。十一月初清兵攻進小坪山反抗軍據點，獲[牛千餘頭](../Page/牛.md "wikilink")、[馬八匹](../Page/馬.md "wikilink")、[車數百輛](../Page/車.md "wikilink")，焚毀[糧食四百堆](../Page/糧食.md "wikilink")。最後[吞霄社](../Page/吞霄社.md "wikilink")、[大甲西社](../Page/大甲西社.md "wikilink")、[貓盂社](../Page/貓盂社.md "wikilink")、[雙嶚社](../Page/雙嶚社.md "wikilink")、[苑裡社](../Page/苑裡社.md "wikilink")、[房裡社等抵抗失敗](../Page/房裡社.md "wikilink")，各[部落先後率眾投降](../Page/部落.md "wikilink")，事件才告結束。事件歷經七個月，清軍「計擒獲男、婦一千餘名，陣斬首級四十一名，傷死二十一名，軍前梟首一十八名」\[2\]，林武力等十三名首領依[清律判決斬決梟示](../Page/清律.md "wikilink")，沙轆土官被判成立唆謀罪名而杖斃\[3\]。

事件後，清廷改大甲西社為「德化社」、牛罵頭社為「感恩社」、沙轆社為「遷善社」、貓盂社為「興隆社」，並且建了一個「鎮番亭」於彰化東郊的瞭望山（即[八卦山](../Page/八卦山.md "wikilink")），並改山名為「定軍山」。

## 結果

亂平之後，參與的番社勢力大幅衰退。直到乾隆年間，生計仍然困苦，官方屢次救濟。[福建分巡臺灣道](../Page/福建分巡臺灣道.md "wikilink")[劉良璧作](../Page/劉良璧.md "wikilink")〈沙轆行〉形容：「皇恩許遷善，生者還其鄉；番婦半寡居，番童少雁行。嗟乎沙轆番，盛衰物之常。祇今防廳廨，荒烟蔓道旁。」

其後清朝放寬海禁，岸裡社接收大肚王國舊地、鹿場等，領域遍及台中盆地。

## 相關條目

  - [大肚王國](../Page/大肚王國.md "wikilink")

## 參考文獻

### 註釋

### 一般參考

  - 《臺灣史》 頁115-116 林呈蓉、李筱峰
  - 《大肚社古文書》 劉澤民 2000年

[category:台灣清治時期民變事件](../Page/category:台灣清治時期民變事件.md "wikilink")
[category:台灣原住民歷史](../Page/category:台灣原住民歷史.md "wikilink")

[Category:清朝民變](../Category/清朝民變.md "wikilink")
[D](../Category/大甲區.md "wikilink")
[Category:台中市歷史](../Category/台中市歷史.md "wikilink")
[Category:大肚王國](../Category/大肚王國.md "wikilink")
[Category:1731年衝突](../Category/1731年衝突.md "wikilink")
[Category:1732年衝突](../Category/1732年衝突.md "wikilink")
[Category:1731年台灣](../Category/1731年台灣.md "wikilink")
[Category:1732年台灣](../Category/1732年台灣.md "wikilink")

1.  [吳福生事件](../Page/吳福生事件.md "wikilink")
2.  [周璽](../Page/周璽.md "wikilink")《彰化縣志》卷六·田賦志·番丁
3.  [國立故宮博物院](../Page/國立故宮博物院.md "wikilink")《宮中檔雍正朝奏摺》二十輯