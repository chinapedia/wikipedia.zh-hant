**云浮**（官方音译：，传统外文：）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[广东省下辖的](../Page/广东省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于广东省西部。市境北靠[肇庆市](../Page/肇庆市.md "wikilink")，东邻[佛山市](../Page/佛山市.md "wikilink")，南接[江门市](../Page/江门市.md "wikilink")、[阳江市](../Page/阳江市.md "wikilink")、[茂名市](../Page/茂名市.md "wikilink")，西界[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[梧州市](../Page/梧州市.md "wikilink")。地處[北回歸線南面](../Page/北回歸線.md "wikilink")，[珠江三角洲以西](../Page/珠江三角洲.md "wikilink")，[西江南岸山地丘陵区](../Page/西江.md "wikilink")，[天露山](../Page/天露山.md "wikilink")、[云雾山与](../Page/云雾山.md "wikilink")[云开大山的北部分支](../Page/云开大山.md "wikilink")，主要地貌为低山及狭长的河谷盆地。西江支流[罗定江](../Page/罗定江.md "wikilink")、[新兴江流经境内](../Page/新兴江.md "wikilink")。全市总面积7,785平方公里，总人口246.05万。雲浮過去盛产[大理石](../Page/大理石.md "wikilink")（云石），現以加工大理石聞名。

## 历史

[唐天宝元年](../Page/唐.md "wikilink")（742年）设**[云浮郡](../Page/云浮郡.md "wikilink")**，[明万历五年十一月戊寅](../Page/明.md "wikilink")（1578年1月3日），建置**东安县**属[羅定直隸州辖地](../Page/羅定直隸州.md "wikilink")。[民国三年](../Page/民国.md "wikilink")（1914年），改名为**[云浮县](../Page/云浮县.md "wikilink")**，隶属[粤海道](../Page/粤海道.md "wikilink")。民国17年隶属广东省西区[绥靖公署](../Page/绥靖公署.md "wikilink")，民国25年隶属广东省第三[行政督察专员公署](../Page/行政督察专员公署.md "wikilink")。

1949年10月1日中华人民共和国成立后，隶属[西江专区](../Page/西江专区.md "wikilink")。1952年属[粤中行政区](../Page/粤中行政区.md "wikilink")。1958年与[新兴县合并称](../Page/新兴县.md "wikilink")**新云县**，属[高要专区](../Page/高要专区.md "wikilink")。1959年4月新云县改称**新兴县**，属[江门专区](../Page/江门专区.md "wikilink")。1961年4月，恢复云浮县建制，属[肇庆专区](../Page/肇庆专区.md "wikilink")。1988年1月，属肇庆市。

1992年9月3日，撤销云浮县建制，设立县级云浮市，由省直辖，肇庆市代管，以原云浮县的行政区域为云浮市的行政区域。1994年4月5日，国务院批准将云浮市升格为地级市，辖地：云城区、新兴县、郁南县。

1994年4月28日，广东省人民政府委托云浮市代管罗定市，下辖：云城区、云安县、郁南县、新兴县、代管：罗定市。

2014年，經國務院批准，云安撤县設区。下辖：云城区、云安区、郁南县、新兴县、代管：罗定市。

## 地理及氣候

雲浮市地處[兩廣丘陵的核心處](../Page/兩廣丘陵.md "wikilink")，全市地勢偏高。氣候屬[副熱帶季風氣候](../Page/副熱帶季風氣候.md "wikilink")，由於雲浮市地勢偏高，典型山区。且全市在經濟發展上比較落後，大部份地方都沒有大型的[城市化發展](../Page/城市化.md "wikilink")，故全年大部份的氣溫一般比附近的城市為低。

## 政治

### 现任领导

<table>
<caption>云浮市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党云浮市委员会.md" title="wikilink">中国共产党<br />
云浮市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/云浮市人民代表大会.md" title="wikilink">云浮市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/云浮市人民政府.md" title="wikilink">云浮市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议云浮市委员会.md" title="wikilink">中国人民政治协商会议<br />
云浮市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/黄汉标_(1963年).md" title="wikilink">黄汉标</a>[1]</p></td>
<td><p><a href="../Page/王胜_(1968年).md" title="wikilink">王胜</a>[2]</p></td>
<td><p><a href="../Page/黄达辉.md" title="wikilink">黄达辉</a>[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/广东省.md" title="wikilink">广东省</a><a href="../Page/陆丰市.md" title="wikilink">陆丰市</a></p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/阳谷县.md" title="wikilink">阳谷县</a></p></td>
<td><p>广东省<a href="../Page/新兴县.md" title="wikilink">新兴县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年6月</p></td>
<td></td>
<td><p>2016年6月</p></td>
<td><p>2012年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市辖2个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[云城区](../Page/云城区.md "wikilink")、[云安区](../Page/云安区.md "wikilink")
  - 县级市：[罗定市](../Page/罗定市.md "wikilink")
  - 县：[新兴县](../Page/新兴县.md "wikilink")、[鬱南县](../Page/鬱南县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>云浮市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[4]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>445300</p></td>
</tr>
<tr class="odd">
<td><p>445302</p></td>
</tr>
<tr class="even">
<td><p>445303</p></td>
</tr>
<tr class="odd">
<td><p>445321</p></td>
</tr>
<tr class="even">
<td><p>445322</p></td>
</tr>
<tr class="odd">
<td><p>445381</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>云浮市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[5]（2010年11月）</p></th>
<th><p>户籍人口[6]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>云浮市</p></td>
<td><p>2367154</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>云城区</p></td>
<td><p>318145</p></td>
<td><p>13.44</p></td>
</tr>
<tr class="even">
<td><p>新兴县</p></td>
<td><p>431635</p></td>
<td><p>18.23</p></td>
</tr>
<tr class="odd">
<td><p>郁南县</p></td>
<td><p>388732</p></td>
<td><p>16.42</p></td>
</tr>
<tr class="even">
<td><p>云安县</p></td>
<td><p>269636</p></td>
<td><p>11.39</p></td>
</tr>
<tr class="odd">
<td><p>罗定市</p></td>
<td><p>959006</p></td>
<td><p>40.51</p></td>
</tr>
</tbody>
</table>

根据2010年的[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")2360128人\[7\]，同[第五次全国人口普查的](../Page/第五次全国人口普查.md "wikilink")2152915人相比，十年共增加207213人，增长9.62%。年平均增长率为0.92%。其中，男性人口为1206871人，占51.14%
；女性人口为1153257人，占48.86%。总人口性别比（以女性为100）为104.65。0－14岁人口为527627人，占22.36%；15－64岁人口为1601383人，占67.85%；65岁及以上人口为231118人，占9.79%。

### 语言

云浮市境内通行[粤语](../Page/粤语.md "wikilink")。

## 物产

  - [大理石加工](../Page/大理石.md "wikilink")（本地产大理石基本无，从全国各地和国外进口大理石及[花岗岩进行加工](../Page/花岗岩.md "wikilink")）
  - [硫铁矿](../Page/硫铁矿.md "wikilink")

## 文化

全境居民的语言为粵語。推廣普通話後大多數居民會說普通話。

## 交通

  - 公路：[324国道](../Page/324国道.md "wikilink")
  - 高速公路：[广梧高速公路](../Page/广梧高速公路.md "wikilink")、[云岑高速公路](../Page/云岑高速公路.md "wikilink")、[汕湛高速公路](../Page/汕湛高速公路.md "wikilink")（[清雲段](../Page/汕湛高速公路清云段.md "wikilink")）、[江羅高速公路](../Page/江羅高速公路.md "wikilink")
  - 铁路：[三茂铁路云浮支线](../Page/三茂铁路.md "wikilink")（位於雲城區的[雲浮站現時只有貨運沒有客運](../Page/雲浮站.md "wikilink")）、[南廣鐵路](../Page/南廣鐵路.md "wikilink")（[雲浮東站](../Page/雲浮東站.md "wikilink")）。過去有[窄軌鐵路連接](../Page/窄軌.md "wikilink")[西江邊](../Page/西江.md "wikilink")（現屬雲安區六都鎮），作用是將當地出產的硫铁矿运到江邊，但現已廢弃。
  - 航空：[羅定機場](../Page/羅定機場.md "wikilink")

## 市区交通

### 公交车

市区共设有5条公交线路，分别是：

  - **1**城北–云浮国际石材城
  - **2**[云浮站](../Page/云浮站.md "wikilink")–上洞
  - **3**云浮站–大降坪
  - **5**牧羊–鹏石
  - **10**市府广场–工业园

### 出租车

云浮市城区出租车起步价2公里内7元，超出2公里后每公里2.4元，行驶单程超过25公里以上须加收50%空驶返程费，候车费每分钟0.5元，即每小时30元；22：00–6：00加收附加费每公里0.30元。

## 名人

  - [惠能](../Page/惠能.md "wikilink")：佛教南宗始祖
  - [黃俊英](../Page/黃俊英.md "wikilink")：相聲演員
  - [何英強](../Page/何英強.md "wikilink")：中國舉重運動員
  - [歐紹燕](../Page/歐紹燕.md "wikilink")：中國女子賽艇運動員

## 参考文献

## 外部链接

  - [云浮市人民政府](https://web.archive.org/web/20080723122617/http://www.yunfu.gov.cn/)

[粤](../Category/中國中等城市.md "wikilink")
[Category:廣東地级市](../Category/廣東地级市.md "wikilink")
[Category:云浮](../Category/云浮.md "wikilink")

1.
2.
3.
4.
5.
6.
7.