[Mao_Renfeng.jpg](https://zh.wikipedia.org/wiki/File:Mao_Renfeng.jpg "fig:Mao_Renfeng.jpg")

**毛人凤**（），字齐五，[浙江省](../Page/浙江省.md "wikilink")[江山縣](../Page/江山縣.md "wikilink")（今[浙江省](../Page/浙江省.md "wikilink")[江山市](../Page/江山市.md "wikilink")）人，[中華民國](../Page/中華民國.md "wikilink")[陸軍](../Page/中華民國陸軍.md "wikilink")[上將](../Page/上將.md "wikilink")，[軍統局長](../Page/軍統局.md "wikilink")。

## 情治工作

### 軍統局時代

早年入读[复旦大学](../Page/复旦大学.md "wikilink")，后考入[黃埔軍校第四期](../Page/黃埔軍校.md "wikilink")，不久因病休学。曾是[戴笠的助手](../Page/戴笠.md "wikilink")，在戴笠去世后，于1946年接任[軍事委員會調查統計局](../Page/軍事委員會調查統計局.md "wikilink")（1947年改制為[國防部保密局](../Page/國防部保密局.md "wikilink")）局长，主要負責打擊[中共的間諜組織](../Page/中共.md "wikilink")。毛人鳳在1949年，中華民國政府撤守到臺灣前，與[杨进兴](../Page/杨进兴.md "wikilink")、[周养浩等人集中處決了一批](../Page/周养浩.md "wikilink")[中國共產黨黨員](../Page/中國共產黨.md "wikilink")，其中包含[杨虎城家眷及其未成年子女](../Page/杨虎城.md "wikilink")。

### 保密局時代

1949年中，[中華民國政府移至](../Page/中華民國.md "wikilink")[臺灣省臺北市](../Page/臺灣省.md "wikilink")（今[臺北市](../Page/臺北市.md "wikilink")，1967年升格為[直轄市](../Page/直轄市.md "wikilink")），中華民國總統[蔣中正續聘毛人鳳為保密局局長](../Page/蔣中正.md "wikilink")，毛是國府以“肅清匪諜”為目標而展開了“[白色恐怖](../Page/白色恐怖.md "wikilink")”行動，因此視為該政治活動的實際執行者之一。1952年接連發生「[毛邦初事件](../Page/毛邦初#毛蒋斗法.md "wikilink")」與「[杜長城案](../Page/杜長城.md "wikilink")」，[蔣經國對毛人鳳恨之入骨](../Page/蔣經國.md "wikilink")，[杜长城與](../Page/杜长城.md "wikilink")[胡凌影等人被槍決](../Page/胡凌影.md "wikilink")\[1\]\[2\]。蔣經國開始進行情報改組，建立现代谍报体制。

### 情報局時代

1955年保密局改組為不再[肅諜與](../Page/肅諜.md "wikilink")[保安的](../Page/保安.md "wikilink")[情報局](../Page/國防部情報局.md "wikilink")，隸屬[國防部](../Page/國防部.md "wikilink")。毛人鳳仍為首任[中華民國國防部情報局首任局長](../Page/中華民國國防部.md "wikilink")，官拜[上將](../Page/上將.md "wikilink")，基本上已無實權。

## 任內病逝

1956年12月11日情報局長任內因心臟病病逝。毛人鳳葬於[新北市](../Page/新北市.md "wikilink")[汐止區](../Page/汐止區.md "wikilink")[昊天嶺](../Page/昊天嶺.md "wikilink")，其墓園約百坪，稱為[昊天](../Page/昊天.md "wikilink")。毛人鳳墓旁有“陸軍上將毛君人鳳墓表”，墓誌銘則為“忠勤永念”\[3\]。該墓表與墓誌銘皆為時任[監察院院長](../Page/監察院.md "wikilink")，知名[書法家](../Page/書法家.md "wikilink")[-{于右任}-作品](../Page/于右任.md "wikilink")。現在墓已遷離原址。

## 後人

兒子毛渝南對父親情治工作並不十分瞭解，曾獲美國[康奈爾大學材料冶金碩士](../Page/康奈爾大學.md "wikilink")、及[麻省理工學院MBA學位](../Page/麻省理工學院.md "wikilink")。曾擔任美国[ITT公司及](../Page/ITT公司.md "wikilink")[阿爾卡特-朗訊台灣區負責人](../Page/阿爾卡特-朗訊.md "wikilink")。1997年9月起轉赴大陸工作，擔任北電網絡中國公司總裁兼首席執行長官長駐北京。2006年8月9日任3COM大中國區企業發展業務執行副總裁，負責3COM在中國的合資公司——華為3Com的業務。后于2013年經惠普公司任命为新设立的惠普中国区董事长，统一负责惠普在中国的战略发展，直屬惠普公司总裁兼首席执行官[梅格·惠特曼](../Page/梅格·惠特曼.md "wikilink")。

毛人鳳的族侄是[毛森](../Page/毛森.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[中將](../Page/中將.md "wikilink")，跟隨毛人鳳，所以也是[軍統特務出身](../Page/軍統.md "wikilink")，然而[毛森因為得罪](../Page/毛森.md "wikilink")[蔣經國而亡命海外](../Page/蔣經國.md "wikilink")。[毛森將軍的兒子](../Page/毛森.md "wikilink")，是[中央研究院](../Page/中央研究院.md "wikilink")[歷史語言研究所前](../Page/歷史語言研究所.md "wikilink")[研究員](../Page/研究員.md "wikilink")**毛漢光**，另一子為[中央研究院](../Page/中央研究院.md "wikilink")[院士](../Page/院士.md "wikilink")**[毛河光](../Page/毛河光.md "wikilink")**。

## 注釋

[分類:安葬於新北市者](../Page/分類:安葬於新北市者.md "wikilink")

[Category:中華民國陸軍二級上將](../Category/中華民國陸軍二級上將.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中華民國政治人物](../Category/中華民國政治人物.md "wikilink")
[Category:中華民國軍事人物](../Category/中華民國軍事人物.md "wikilink")
[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國大陸時期軍事人物](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:黄埔军校第四期](../Category/黄埔军校第四期.md "wikilink")
[Category:台灣戰後浙江移民](../Category/台灣戰後浙江移民.md "wikilink")
[M毛](../Category/江山人.md "wikilink") [R人](../Category/毛姓.md "wikilink")
[Category:复旦大学校友](../Category/复旦大学校友.md "wikilink")
[Category:軍統局人物](../Category/軍統局人物.md "wikilink")

1.  《军统最后的暗杀名单》蓬山此去无多路(9)
2.  [蔣經國對毛人鳳的最後一擊](http://big5.chinanews.com.cn:89/tw/lscq/news/2007/09-04/1018588.shtml)

3.  [八羅漢山(南)，麗山橋口步道，麗山峰；昊天嶺2003/11/19](http://www.yougoipay.com/kenny/w237/index.htm)