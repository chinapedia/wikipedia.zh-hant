**摩根县**（**Morgan County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州東部](../Page/肯塔基州.md "wikilink")[坎伯蘭高原的一個縣](../Page/坎伯蘭高原.md "wikilink")。面積994平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,948人。縣治[西利伯蒂](../Page/西利伯蒂_\(肯塔基州\).md "wikilink")
(West Liberty)。

成立於1822年，縣政府成立於1823年3月10日。縣名紀念[美國獨立戰爭軍官](../Page/美國獨立戰爭.md "wikilink")、代表[維吉尼亞州的](../Page/維吉尼亞州.md "wikilink")[眾議員](../Page/美國眾議院.md "wikilink")[丹尼爾·摩根](../Page/丹尼爾·摩根.md "wikilink")。本縣為一[禁酒的縣](../Page/禁酒.md "wikilink")。

[M](../Category/肯塔基州行政区划.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")