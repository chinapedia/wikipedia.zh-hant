[LensCrafters_in_CF_Richmond_Centre_2018.jpg](https://zh.wikipedia.org/wiki/File:LensCrafters_in_CF_Richmond_Centre_2018.jpg "fig:LensCrafters_in_CF_Richmond_Centre_2018.jpg")[溫哥華](../Page/溫哥華.md "wikilink")[列治文CF](../Page/列治文.md "wikilink")
Richmond Centre的「亮視點」分店\]\]
[Lenscrafters_hongkong_mongkok.jpg](https://zh.wikipedia.org/wiki/File:Lenscrafters_hongkong_mongkok.jpg "fig:Lenscrafters_hongkong_mongkok.jpg")[旺角](../Page/旺角.md "wikilink")[惠豐中心的](../Page/惠豐中心.md "wikilink")「LensCrafters高登眼鏡」分店\]\]
**亮視點**（前稱**高登眼鏡**，而英文名為**LensCrafters**）為[北美最大](../Page/北美.md "wikilink")[眼鏡零售連鎖店](../Page/眼鏡.md "wikilink")。該公司在1983年創立，在[加拿大](../Page/加拿大.md "wikilink")、[美國等已超過](../Page/美國.md "wikilink")850家連鎖店；零售店通常是獨立的及熟悉的快速倉庫內的周轉，准予顧客走進去，取得[單片眼鏡](../Page/單片眼鏡.md "wikilink")，選擇眼鏡架，及在一個個別的拜訪期間製造其眼鏡，並用了大約一小時等候建造。他們維持公司總部接近[美國](../Page/美國.md "wikilink")[俄亥俄州的](../Page/俄亥俄州.md "wikilink")[辛辛那提市](../Page/辛辛那提市.md "wikilink")，由[Sunglass
Hut
International獨得](../Page/Sunglass_Hut_International.md "wikilink")；但事實上，兩間公司均由以[義大利人為首的](../Page/義大利人.md "wikilink")[羅薩奧蒂卡集團全資擁有](../Page/羅薩奧蒂卡.md "wikilink")。該集團於1995年收購LensCrafters連鎖店。

在[香港](../Page/香港.md "wikilink")，羅薩奧蒂卡分別於2003年及2002年收購本地兩大眼鏡連鎖店——[高登眼鏡及](../Page/高登眼鏡.md "wikilink")[藝視眼鏡](../Page/藝視眼鏡.md "wikilink")。高登眼鏡是香港其中一間歷史悠久的眼鏡零售連鎖店，於1971年創立。該公司亦是全港第一間為顧客提供免費電腦驗眼、眼鏡調校服務、一年免費保養及專業視光師諮詢服務的眼鏡連鎖店。而藝視眼鏡則由藝人[苗僑偉和](../Page/苗僑偉.md "wikilink")[戚美珍在](../Page/戚美珍.md "wikilink")1987年在香港成立的。2006年，Luxottica將高登眼鏡及藝視眼鏡合併，並重新命名為「LensCrafters
高登眼鏡」，正式將LensCrafters的名字帶入香港市場。

在[中國大陸](../Page/中國大陸.md "wikilink")，Luxottica集團於2005年至2006年間收購內地眼鏡連鎖店，分別為**北京雪亮**、**廣州明廊**和**上海現代光學**，並陸續將這些連鎖店的品牌改為「LensCrafters
亮視點」。2008年1月，香港的高登眼鏡連鎖店亦開始陸續更名為「亮視點」。

## 外部連結

  - [lenscrafters.com](http://www.lenscrafters.com/)
  - [lenscrafters.com.hk](http://www.lenscrafters.com.hk/)
  - [LensCrafters Online Contact Lens
    Catalog](https://web.archive.org/web/20061016220709/https://www.lenscrafterscontacts.com/default.asp)

[Category:眼鏡品牌](../Category/眼鏡品牌.md "wikilink")
[Category:零售商](../Category/零售商.md "wikilink")
[Category:1983年成立的公司](../Category/1983年成立的公司.md "wikilink")
[Category:俄亥俄州為首的公司](../Category/俄亥俄州為首的公司.md "wikilink")
[Category:羅薩奧蒂卡品牌](../Category/羅薩奧蒂卡品牌.md "wikilink")