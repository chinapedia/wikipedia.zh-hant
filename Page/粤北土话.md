**粤北土话**，舊稱**韶州土话**，当地人称之为**虱乸话**（粤语耶魯拼音：Sat Na
Wa），分布在[广东北部的](../Page/广东.md "wikilink")[乐昌](../Page/乐昌.md "wikilink")、[仁化](../Page/仁化.md "wikilink")、[乳源](../Page/乳源.md "wikilink")、[曲江](../Page/曲江.md "wikilink")、[南雄](../Page/南雄.md "wikilink")、[浈江](../Page/浈江.md "wikilink")、[武江](../Page/武江.md "wikilink")、[连州](../Page/连州.md "wikilink")、[连南等县区](../Page/连南.md "wikilink")，对于粤北土话的归属问题至今还没有一个定论，但与同为归属未定的[湘南土话较为近似](../Page/湘南土话.md "wikilink")。有学者认为粤北土话是[广西平话的扩展地区](../Page/广西平话.md "wikilink")；也有学者认为：粤北土话是在[宋代](../Page/宋代.md "wikilink")[赣语的基础上](../Page/赣语.md "wikilink")，混合了[客家话](../Page/客家话.md "wikilink")、[粤语](../Page/粤语.md "wikilink")、[官话等的混合性方言](../Page/官话.md "wikilink")。

目前使用人數約50-100萬，而語言歸屬也不明。

## 粤北土话现状

目前[粤北地区主要的语言已经变为粤语和客家话等广东省内强势语言](../Page/粤北.md "wikilink")，粤北土话受到严重侵蚀，并已退居至一些村镇之中，形成一个个[方言岛](../Page/方言岛.md "wikilink")，是一种濒危[漢語變體](../Page/漢語變體.md "wikilink")，很多操粤北土话的人对内用土话，但对外一般都用客家话或粤语与人交流，目前[韶关市区的主流语言是粤语和](../Page/韶关市.md "wikilink")[普通话](../Page/普通话.md "wikilink")（[現代標準漢語](../Page/現代標準漢語.md "wikilink")），乡村地区则通行客家话，除少数老人家能说过去[韶关的](../Page/韶关.md "wikilink")“土话”以外，絕大部分的年青人都不会说了。

## 语音特点

粤北土话的语音特点大多和[湘南土话大致相同](../Page/湘南土话.md "wikilink")，它们应属同一种方言。

## 参考

## 外部連結

  - [Classification of Tuhua
    Dialects](http://www.glossika.com/en/dict/classification/tuhua/index.php)

## 参看

  - [平话](../Page/平话.md "wikilink")
  - [湘南土话](../Page/湘南土话.md "wikilink")

[Category:广东汉语变体](../Category/广东汉语变体.md "wikilink")
[Category:中国混合语言](../Category/中国混合语言.md "wikilink")