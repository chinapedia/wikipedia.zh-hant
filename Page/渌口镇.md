**渌口**是[湖南省](../Page/湖南省.md "wikilink")[株洲市](../Page/株洲市.md "wikilink")[渌口区的驻地](../Page/渌口区.md "wikilink")。

## 历史

  - 古时[三国称](../Page/三国.md "wikilink")[津口](../Page/津口.md "wikilink")，因有[津水绕城而过](../Page/津水.md "wikilink")，且入[湘江](../Page/湘江.md "wikilink")，而为津水入湘之口得名。
  - 自古为军事屯兵要塞。
      - [东汉时期](../Page/东汉.md "wikilink")，[伏波将军](../Page/伏波将军.md "wikilink")[马援受](../Page/马援.md "wikilink")[汉](../Page/汉.md "wikilink")[光武帝派遣](../Page/光武帝.md "wikilink")，南征[交趾](../Page/交趾.md "wikilink")，而往返屯兵于此。至今有古迹[伏波庙](../Page/伏波庙.md "wikilink")。
  - 三国时期，原属[荆州](../Page/荆州.md "wikilink")，自[蜀国获得荆州之后](../Page/蜀国.md "wikilink")，由[关羽](../Page/关羽.md "wikilink")，[张飞屯兵驻守](../Page/张飞.md "wikilink")。時为荆州長江以南重要的軍事要塞。
  - 而关张的足迹，在此地尚存多处。
      - 有街道旧称[关口](../Page/关口.md "wikilink")，因关羽把守入河口而命名。至今仍有[关帝庙](../Page/关帝庙.md "wikilink")，香火旺盛。
  - 民国初年，爱国[华侨](../Page/华侨.md "wikilink")[陈嘉庚曾临此地](../Page/陈嘉庚.md "wikilink")。
  - 原为[醴陵辖区](../Page/醴陵.md "wikilink")。1959年因建[株洲而从醴陵析置](../Page/株洲.md "wikilink")。1968年建[株洲县为县城所在](../Page/株洲县.md "wikilink")。
  - 2018年，[株洲县撤销设立](../Page/株洲县.md "wikilink")[株洲市](../Page/株洲市.md "wikilink")[渌口区](../Page/渌口区.md "wikilink")，区政府驻渌口镇学堂路1号。

## 地理

  - 今渌口之名，因河[渌江又称渌水](../Page/渌江.md "wikilink")，而依津口而命名。绕城之渌江除春季洪水，水质终年清澈。
  - 城中地名，旧街道有[接龙桥](../Page/接龙桥.md "wikilink")，[沙河里](../Page/沙河里.md "wikilink")，关口，[半边街](../Page/半边街.md "wikilink")，[庙弄子](../Page/庙弄子.md "wikilink")，[学堂岭](../Page/学堂岭.md "wikilink")，[燕窝里](../Page/燕窝里.md "wikilink")。
      - 今有[渌滨路](../Page/渌滨路.md "wikilink")，[南江路](../Page/南江路.md "wikilink")，[学堂路](../Page/学堂路.md "wikilink")，[迎宾路](../Page/迎宾路.md "wikilink")，[向阳路](../Page/向阳路.md "wikilink")，[伏波大道](../Page/伏波大道.md "wikilink")，[漉浦路](../Page/漉浦路.md "wikilink")，[黎韶路](../Page/黎韶路.md "wikilink")。
  - 新建有[渌口经济工业园](../Page/渌口经济工业园.md "wikilink")。

## 交通

  - 公路：境内有[S211](../Page/S211.md "wikilink")[省道](../Page/省道.md "wikilink")，贯城而过。2009年城区街道已经全部实现交通指挥电子化。
  - 铁路：[京广线](../Page/京广线.md "wikilink")。城内有[渌口站](../Page/渌口站.md "wikilink")，目前仅[管内列车靠站](../Page/管内列车.md "wikilink")。
  - 航运：渌江原可通行[机驳船](../Page/机驳船.md "wikilink")，上可沿渌江而上，到达醴陵。湘江可上抵[衡阳](../Page/衡阳.md "wikilink")，下至[岳阳](../Page/岳阳.md "wikilink")。

## 教育

  - [中学](../Page/中学.md "wikilink")：[株洲县第五中学](../Page/株洲县第五中学.md "wikilink")，[潇湘双语学校](../Page/潇湘双语学校.md "wikilink")。
  - [职教](../Page/职教.md "wikilink")：[株洲县渌口职业中学](../Page/株洲县渌口职业中学.md "wikilink")。
  - [小学](../Page/小学.md "wikilink")：[育红小学](../Page/育红小学.md "wikilink")，[南塘小学](../Page/南塘小学.md "wikilink")，[湖塘小学](../Page/湖塘小学.md "wikilink")，[双月小学](../Page/双月小学.md "wikilink")。

[Category:渌口区行政区划](../Category/渌口区行政区划.md "wikilink")
[Category:株洲乡镇](../Category/株洲乡镇.md "wikilink")