**厦门教区**是[天主教在](../Page/天主教.md "wikilink")[中国](../Page/中国.md "wikilink")[福建省设立的一个](../Page/福建省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1883年12月3日，从福建代牧区分设**厦门代牧区**，掌管[泉州](../Page/泉州府.md "wikilink")、[漳州](../Page/漳州府.md "wikilink")、[台灣三府和](../Page/台灣府.md "wikilink")[龙岩](../Page/龙岩州.md "wikilink")、[永春两州之教務](../Page/永春州.md "wikilink")\[1\]。1913年7月13日，[台灣从厦门代牧区](../Page/台灣.md "wikilink")-{zh-hans:划;zh-hant:劃;}-出，单独成立[台灣監牧區](../Page/天主教高雄教區.md "wikilink")，仍由西班牙道明会负责，而原屬[福州代牧区的](../Page/福州教区.md "wikilink")[兴化府](../Page/兴化府.md "wikilink")（莆田和仙游两县）划归厦门管轄。1946年4月11日，厦门代牧区升格为**厦门教区**。

## 教堂

  - [鼓浪屿耶稣君王主教座堂](../Page/鼓浪屿耶稣君王主教座堂.md "wikilink")
  - 厦门玫瑰圣母堂
  - 高浦天主堂
  - 海沧圣方济各沙勿略堂
  - 同安天主堂

## 教徒

1950年止有教徒16,588人\[2\]。

## 历任主教

  - [杨真崇](../Page/杨真崇.md "wikilink")（Andrés Chinchón,
    [O.P.](../Page/道明會.md "wikilink")）1883 - 1892
  - \-{[于班略](../Page/于班略.md "wikilink")}-（Ignacio Ibáñez, O.P. 1893
  - [桑傑斯](../Page/桑傑斯.md "wikilink")（Esteban Sánchez de las Heras,
    O.P.）1895 - 1896
  - [黎克勉](../Page/黎克勉.md "wikilink")（Isidoro Clemente Gutiérrez,
    O.P.）1899 - 1915
  - [马守仁](../Page/马守仁.md "wikilink")（Manuel Prat Pujoldevall, O.P.）1916
    - 1947
  - [茅中砥](../Page/茅中砥.md "wikilink")（Juan Bautista Velasco Díaz,
    O.P.）1948 - 1983
      - [黄子玉](../Page/黄子玉.md "wikilink") 1911年生，1986年自选自圣，1991年去世
  - [蔡炳瑞](../Page/蔡炳瑞.md "wikilink") 2010 -
    ，2010年5月8日由[天主教沂州教区主教房兴耀祝圣](../Page/天主教沂州教区.md "wikilink")，获得教宗认可。

## 注釋

<div class="references-small">

<references />

</div>

[Category:中國天主教教區](../Category/中國天主教教區.md "wikilink")
[Category:福建天主教](../Category/福建天主教.md "wikilink")
[Category:厦门基督教](../Category/厦门基督教.md "wikilink")

1.  [1](http://www.xmtz.cn/InfoShow.asp?Nid=1825&Sid=239&SbjId=618)
2.  [Dioecesis
    Sciiamenensis](http://www.catholic-hierarchy.org/diocese/dhsia.html)