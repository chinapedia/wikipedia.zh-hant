**KAITO**是[山葉以](../Page/山葉_\(公司\).md "wikilink")[VOCALOID](../Page/VOCALOID.md "wikilink")[語音合成引擎為基礎開發](../Page/語音合成.md "wikilink")[CRYPTON
FUTURE
MEDIA販售的虛擬歌手軟件第二作](../Page/CRYPTON_FUTURE_MEDIA.md "wikilink")，或此軟件的印象角色（這只是軟件的象徵，不會在實際使用時出現）。於2006年2月發售，開放價格，官方估計系列軟件實際價格約19,950日圓。由[風雅直人提供原聲](../Page/風雅直人.md "wikilink")\[1\]。日語第二套VOCALOID的產品，擅長[歌謠曲](../Page/歌謠曲.md "wikilink")、[童謠](../Page/童謠.md "wikilink")。因為同類型軟件的受眾多是男性\[2\]，發售當初只賣出500套，相比同系列[MEIKO的](../Page/MEIKO.md "wikilink")3000套顯得遜色\[3\]，但是至2009年10月27日售出數已經與MEIKO同為約5000套\[4\]。VOCALOID
3引擎聲庫KAITO V3是由[CRYPTON FUTURE
MEDIA開發并販售](../Page/CRYPTON_FUTURE_MEDIA.md "wikilink")，于2013年2月15日发售。

## 發展

  - 2007年下旬，CRYPTON發售下一代的同類型軟件[初音未來](../Page/初音未來.md "wikilink")，並爆發熱潮，間接令KAITO重新引起注意。其後成為[四格漫畫](../Page/四格漫畫.md "wikilink")《[小初音未來的日常](../Page/小初音未來的日常.md "wikilink")》的主角之一。
  - 同年11月28日由kobapie（又稱）上載至[NICONICO動畫的原創曲](../Page/NICONICO動畫.md "wikilink")《[](http://www.nicovideo.jp/watch/sm1639267)》引起大反響，並於2008年4月4日播放的電視綜合節目《[The☆Net
    Star\!](../Page/The☆Net_Star!.md "wikilink")》中被介紹\[5\]。
  - 2008年7月25日發表於[C74發售](../Page/Comic_Market.md "wikilink")，收錄[初音未來](../Page/初音未來.md "wikilink")、鏡音鈴、連、[MEIKO](../Page/MEIKO.md "wikilink")、KAITO的五首原創曲，是初次包含KAITO的非同人CD\[6\]。
  - 另在[C74發售初次的官方精品](../Page/Comic_Market.md "wikilink")\[7\]。2008年6月5日[GOOD
    SMILE
    COMPANY所做的調查中](../Page/GOOD_SMILE_COMPANY.md "wikilink")，希望KAITO製成[人物模型的要求是第一位](../Page/人物模型.md "wikilink")，其後於10月21日正式發表預定於2009年2月發售該人物模型\[8\]。
  - 2008年12月28日KAITO原創曲《[](http://www.nicovideo.jp/watch/sm1639267)》CD專輯推出於一般流通市場發售，由KAITO的原聲本人翻唱。另外同專輯內KAITO原創曲《[虚空戦士マジスパイダー](http://www.nicovideo.jp/watch/sm3610516)》成為日本[湯咖哩店](../Page/湯咖哩.md "wikilink")[MAGIC
    SPICE](http://www.magicspice.net/)的公式收市曲。
  - 2011年6月3日,官方Crypton Furture Media於Youtube發佈KAITO更新為VOCALOID3（KAITO
    V3）的DEMO曲《[からくり時計と恋の話](http://www.youtube.com/watch?v=etdsxkcd__U)》
  - 2012年2月15日，**KAITO V3**发布。

## 知名作品

  - 資料參考來源： [週刊VOCALOIDランキング](http://www.geocities.jp/vocaran_link/)

  - ：2008年2月21日上載的原創曲，至2010年10月26日觀看次數超過133萬。

## 參見

  -
  - [MEIKO](../Page/MEIKO.md "wikilink")

  - [角色主唱系列](../Page/角色主唱系列.md "wikilink")

  - [VOCALOID](../Page/VOCALOID.md "wikilink")

  - [CRYPTON FUTURE MEDIA](../Page/CRYPTON_FUTURE_MEDIA.md "wikilink")

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - 官方

<!-- end list -->

  - [KAITO](http://www.crypton.co.jp/mp/do/prod?id=27720)

  - [MEDIA
    PHAGE](https://web.archive.org/web/20071226012725/http://blog.crypton.co.jp/)（製作人員blog）

  - [KAITOV3](https://web.archive.org/web/20130104061446/http://www.crypton.co.jp/mp/pages/prod/vocaloid3/kaitov3.jsp)

<!-- end list -->

  - 其他

<!-- end list -->

  - [資料](https://web.archive.org/web/20050904125301/http://www.white.cx/~lam/naoto.html)

  - [Vocaloid Music Search](http://bokasachi.natsu.gs/index.html)

  - [「○○P」列表Wiki](http://wikiwiki.jp/oop/)

[Category:Vocaloid
(第一代)引擎产品](../Category/Vocaloid_\(第一代\)引擎产品.md "wikilink")
[Category:Vocaloid3引擎产品](../Category/Vocaloid3引擎产品.md "wikilink")
[Category:Vocaloid角色](../Category/Vocaloid角色.md "wikilink")
[Category:2006年音樂](../Category/2006年音樂.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:音樂軟件](../Category/音樂軟件.md "wikilink")

1.  KAITO製品包裝
2.
3.
4.
5.  《The☆Net Star\!》 2008年4月4日日本時間24:00～24:40播放
6.
7.
8.