《**女神候補生**》（****）是[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")[杉崎由綺琉在](../Page/杉崎由綺琉.md "wikilink")1997年於《[Comic
Gum](../Page/Comic_Gum.md "wikilink")》月刊所連載的漫畫。《女神候補生》於2000年時改編成[電視動畫](../Page/電視動畫.md "wikilink")，雖然一開始時預定製作26話，不過因為許多緣故，最後只製作12話。

## 故事背景

星曆4088年，由於人類犯錯而引起星系危機，人類喪失了大多數的行星；人類只能居住在[宇宙](../Page/宇宙.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")，唯一殘留下來的行星「ZION」是人類最後的希望，但是ZION遭受神秘生物「威克提姆」的侵略。為了對抗威克提姆的侵略，人類製造了巨大人型武器「英格利德」來保護ZION。五架英格利德被尊稱為「女神」，只有具備「特殊能力」（簡稱「EX」）者才能成為女神的駕駛員。為了培養女神駕駛員，人類在宇宙中建造了養成機構「G.O.A.」。

星曆5030年，傑洛·苑名（Zero
Enna）從偏遠的殖民地前往G.O.A.，成為候補生。當天正是五架女神及其駕駛員齊聚G.O.A.的日子。在G.O.A.中迷路的傑洛來到機庫，看見了女神伊娃·莉娜（Eeva
Leena）。在正常情形下，女神們的配備與維修都只能配合各自的專屬駕駛員，不是專屬駕駛員的人無法搭乘女神；但不知何故，伊娃·莉娜竟然認同初次見面的傑洛為其駕駛員，將傑洛吸進駕駛艙內開始「連接」。在「連接」過程中痛苦不堪的傑洛，在朦朧的意識中聽見了伊娃·莉娜的聲音……

## 主要人物

[配音員順序為動畫版](../Page/配音員.md "wikilink")/CD版。

### 女神候補生

  - **傑洛·苑名**/**苑名 零**（**Zero
    Enna**、配音員：[小尾元政](../Page/小尾元政.md "wikilink")/[福山潤](../Page/福山潤.md "wikilink")）

<!-- end list -->

  -
    男主角，15歲，是一位「女神」候補生，編號88號，個性開朗直率，夢想成為「女神」的駕駛者。
    有「無重力恐懼症」，在拍擋的幫助下慢慢克服。
    在身體檢查中，被發現完全沒有用原子修補的部分，是「完全體」。

<!-- end list -->

  - **克雷·克里夫·佛特蘭**（**Clay Cliff
    Fortran**、配音員：[吉野裕行](../Page/吉野裕行.md "wikilink")/[白石稔](../Page/白石稔.md "wikilink")）

<!-- end list -->

  -
    15歲，是一位「女神」候補生，編號89號，帶著眼鏡個性冷靜，雖然體力不如其他的候補生，不過頭腦相當發達。

<!-- end list -->

  - **希多·古那**（**Hiead
    Gner**、配音員：[千葉進步](../Page/千葉進步.md "wikilink")/[小野坂昌也](../Page/小野坂昌也.md "wikilink")）

<!-- end list -->

  -
    15歲，是一位「女神」候補生，編號87號，個性陰沉冷酷，對其他人的態度相當冷漠，企圖成為「女神」的駕駛者。

<!-- end list -->

  - **山義·櫛田**（**Yamagi
    Kushida**、配音員：[陶山章央](../Page/陶山章央.md "wikilink")/[田中一成](../Page/田中一成.md "wikilink")）

<!-- end list -->

  -
    15歲，是一位「女神」候補生，編號86號，由於很在意自己的身高，因此不喜歡被叫矮子。

<!-- end list -->

  - **路茲·澤村**（**Roose
    Sawamura**、配音員：[大本よしみち](../Page/大本命.md "wikilink")/[保志總一朗](../Page/保志總一朗.md "wikilink")）

<!-- end list -->

  -
    15歲，是一位「女神」候補生，編號85號
    開始時被判別為「超重」十五公斤，接受了拍擋的特別減肥課程，回復標準體重，同時發現自己的特殊能力。

<!-- end list -->

  - **亞次·威爾尼·寇克多**（**Erts Virny
    Cocteau**、配音員：[野島健兒](../Page/野島健兒.md "wikilink")）

<!-- end list -->

  -
    14歲，是一位上級生，編號05號，也是候補生中成績最好的一位，爾尼斯特·科雷的弟弟。

### 候補維修師

  - **基娜·涂利克**（**Kizna Towryk**、配音員：[長澤美樹](../Page/長澤美樹.md "wikilink")）

<!-- end list -->

  -
    15歲，為女神的候補維修師，獸耳是其特徵，搭配的駕駛員是傑洛·苑名。

<!-- end list -->

  - **早紀·御森**（**Saki Mimori**、配音員：[千葉千恵巳](../Page/千葉千恵巳.md "wikilink")）

<!-- end list -->

  -
    為女神的候補維修師，紅髮雙馬尾，搭配的駕駛員是克雷·克里夫·佛特蘭。

<!-- end list -->

  - **伊克妮·亞歷克多**（**Ikhny
    Allecto**、配音員：[釘宮理惠](../Page/釘宮理惠.md "wikilink")）

<!-- end list -->

  -
    為女神的候補維修師，帶著眼鏡的內向少女，搭配的駕駛員是希多·古那。

<!-- end list -->

  - **塚沙·庫夏**（**Tukasa Kuscha**、配音員：[天野由梨](../Page/天野由梨.md "wikilink")）

<!-- end list -->

  -
    為女神的候補維修師，黑色長髮，身高較其他人還高，搭配的駕駛員是山義·櫛田。

<!-- end list -->

  - **蕾卡·多辛格**（**Wrecka
    Toesing**、配音員：[涌澤利香](../Page/涌澤利香.md "wikilink")）

<!-- end list -->

  -
    為女神的候補維修師，藍色短髮，搭配的駕駛員是路茲·澤村。

### 女神駕駛員

  - **蒂拉·札因·愛爾梅斯**（**Teela Zain
    Elmes**、配音員：[天野由梨](../Page/天野由梨.md "wikilink")（OVA版改為[井上喜久子](../Page/井上喜久子.md "wikilink")）/[冬馬由美](../Page/冬馬由美.md "wikilink")）

<!-- end list -->

  -
    15歲，為女神的駕駛員，也是唯一一位女性駕駛員，在5位駕駛員中地位最高。

<!-- end list -->

  - **加爾伊斯·艾利多**（**Gareas
    Elidd**、配音員：[高木涉](../Page/高木涉.md "wikilink")/[岸尾大輔](../Page/岸尾大輔.md "wikilink")）

<!-- end list -->

  -
    17歲，為女神的駕駛員，自信心相當強烈。

<!-- end list -->

  - **優·緋座**/**緋座 優**（**Yu
    Hikura**、配音員：[石田彰](../Page/石田彰.md "wikilink")）

<!-- end list -->

  -
    16歲，為女神的駕駛員，相當沉默寡言。

<!-- end list -->

  - **里歐利特·威爾吉那**（**Rioroute
    Vilgyna**、配音員：[遠近孝一](../Page/遠近孝一.md "wikilink")）

<!-- end list -->

  -
    16歲，為女神的駕駛員，行動能力相當強。

<!-- end list -->

  - **爾尼斯特·科雷**（**Ernest
    Cuore**、配音員：[柏倉つとむ](../Page/柏倉つとむ.md "wikilink")/[置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink")）

<!-- end list -->

  -
    17歲，為女神的駕駛員，具有強烈的精神感應能力。

### 維修師

  - **莉娜·藤村**（**Leena Fujimura**、配音員：[嘉數由美](../Page/嘉數由美.md "wikilink")）

<!-- end list -->

  -
    18歲，為女神的維修師，搭配的駕駛員是加爾伊斯·艾利多。

<!-- end list -->

  - **妃·緋座**/**緋座 一妃**（**Kazuhi
    Hikura**、配音員：[川澄綾子](../Page/川澄綾子.md "wikilink")）

<!-- end list -->

  -
    15歲，為女神的維修師，搭配的駕駛員是優·緋座，也是他的妹妹。

<!-- end list -->

  - **芙爾弗雷拉·蒂德**（**Philphleora
    Deed**、配音員：[半場友惠](../Page/半場友惠.md "wikilink")）

<!-- end list -->

  -
    17歲，為女神的維修師，搭配的駕駛員是里歐利特·威爾吉那。

<!-- end list -->

  - **丘恩·優格**（**Tune Youg**、配音員：[白鳥由里](../Page/白鳥由里.md "wikilink")）

<!-- end list -->

  -
    14歲，為女神的維修師，搭配的駕駛員是爾尼斯特·科雷。

### G.O.A.人士

  - **東·肘方**（**Azuma Hijikata**、配音員：[藤原啓治](../Page/藤原啓治.md "wikilink")）

<!-- end list -->

  -
    G.O.A.的第1級教官。

<!-- end list -->

  - **莉魯醫生**（Rill、配音員：[小山茉美](../Page/小山茉美.md "wikilink")）

<!-- end list -->

  -
    G.O.A.的醫生，負責管理候補生的身體狀況。

<!-- end list -->

  - **校長**（配音員：[田中正彥](../Page/田中正彥.md "wikilink")）

### 其他

  - **[旁白](../Page/旁白.md "wikilink")**（配音員：[小山茉美](../Page/小山茉美.md "wikilink")）

## 女神

  - 耶恩·拉提艾絲（Errn Laties）：為女神的原型機，駕駛員為蒂拉·札因·愛爾梅斯。
  - 伊娃·莉娜（Eeva Leena）：駕駛員為加爾伊斯·艾利多。
  - 泰利雅·卡利斯托（Tellia Kallisto）：駕駛員為優·緋座。
  - 露瑪·克雷因（Luhma Klein）：駕駛員為爾尼斯特·科雷。
  - 亞姬·基邁雅（Agui Keameia）：駕駛員為里歐利特·威爾吉那。

## 名詞

  - **GOA**

<!-- end list -->

  -
    Goddess Operator Academy，訓練女神駕駛員的地方，收容了三千多人。

在GOA內使用EX及吵架動粗的，與在旁觀看的人同樣有罪，會被扣五分，當扣至五十分的時候有機會受到退學的處分。
候補生剛進入GOA時，需接受各種身體的檢查。首先是運動能力的極限檢查，以嚴苛運動中的身體異常，來調查整個人的綜合運動能力。檢查內容分為九個階段，包括﹕瞬間爆發力、持久力、跳躍力、反射神經、握力、腕力、背肌、仰臥起坐、韻律感。

  - **EX中和系統**

<!-- end list -->

  -
    在GOA中，用作壓制各候補生的EX。

<!-- end list -->

  - **括巴爾**

<!-- end list -->

  -
    實戰測驗用的模擬裝置，是一種體感模擬的作戰模式。

## 書籍

  - 《女神候補生設定資料集》（女神候補生ビジュアルブック）
    【日文版：】Wani Books編輯部著，Wani Books 2000年4月發行，ISBN 4847025660
    【全球中文版：】Wani
    Books編輯部著，-{游}-若琪譯，[尖端出版](../Page/尖端出版.md "wikilink")2002年11月1版1刷，EAN
    4719863040462

## 動畫版

### 製作人員

  - 原作：[杉崎由綺琉](../Page/杉崎由綺琉.md "wikilink")
  - [系列構成](../Page/系列構成.md "wikilink")：[桶谷顯](../Page/桶谷顯.md "wikilink")
  - 科幻設定：堺三保
  - [人物設計](../Page/人物設計.md "wikilink")：山岡信一
  - 主要[機械設計](../Page/機械設計_\(動畫\).md "wikilink")：武半慎吾、[石垣純哉](../Page/石垣純哉.md "wikilink")
  - 總作畫監督：山岡信一
  - 特技監修：前田明寿
  - 美術監督：小山俊久
  - 色彩設定：金丸ゆう子
  - 音響監督：[三間雅文](../Page/三間雅文.md "wikilink")
  - 音樂：[朝川朋之](../Page/朝川朋之.md "wikilink")
  - 助監督：星合貴彦
  - [監督](../Page/監督.md "wikilink")：[本郷みつる](../Page/本郷みつる.md "wikilink")
  - 製作群：石田智巳、佐藤徹、高梨實
  - 3D制作：IKIF+、[Production I.G](../Page/Production_I.G.md "wikilink")
  - 動畫制作: [XEBEC](../Page/XEBEC.md "wikilink")
  - [製作](../Page/製作.md "wikilink")：[Bandai
    Visual](../Page/Bandai_Visual.md "wikilink")、[Wani
    Books](../Page/Wani_Books.md "wikilink")、ING、[XEBEC](../Page/XEBEC.md "wikilink")

### 主題曲

  - 片頭曲『希望の星をめざせ！』
    作曲・編曲：[朝川朋之](../Page/朝川朋之.md "wikilink")
  - 片尾曲『チャンス』
    作詞：[富田京子](../Page/富田京子.md "wikilink")／作曲：小泉恒平／編曲：小野澤篤／歌：[小泉恒平](../Page/小泉恒平.md "wikilink")
  - 第12話片尾曲『かがやき』
    作詞：桶谷顕／作曲・編曲：朝川朋之／歌：小泉恒平

### 各話列表

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>日文標題</p></th>
<th><p>中文標題</p></th>
<th><p>腳本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
<th><p>播放日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>CURRICULUM 00</p></td>
<td></td>
<td><p>接續</p></td>
<td><p><a href="../Page/桶谷顯.md" title="wikilink">桶谷顯</a></p></td>
<td><p><a href="../Page/本鄉滿.md" title="wikilink">本鄉滿</a></p></td>
<td><p>山岡信一</p></td>
<td><p><strong>2000年</strong><br />
1月10日 |-你</p></td>
<td><p>CURRICULUM 01</p></td>
</tr>
<tr class="even">
<td><p>CURRICULUM 02</p></td>
<td><p>EX</p></td>
<td><p>EX</p></td>
<td><p>星合貴彥</p></td>
<td><p><a href="../Page/本橋秀之.md" title="wikilink">本橋秀之</a></p></td>
<td><p>1月24日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CURRICULUM 03</p></td>
<td></td>
<td><p>自覺</p></td>
<td><p><a href="../Page/木村真一郎.md" title="wikilink">木村真一郎</a></p></td>
<td><p>植田實</p></td>
<td><p>1月31日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>CURRICULUM 04</p></td>
<td></td>
<td><p>搭檔</p></td>
<td><p>原博</p></td>
<td><p>佐土原武之</p></td>
<td><p>日向正樹</p></td>
<td><p>2月7日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CURRICULUM 05</p></td>
<td></td>
<td><p>默契</p></td>
<td><p>西村博之</p></td>
<td><p><a href="../Page/真野玲.md" title="wikilink">真野玲</a></p></td>
<td><p><a href="../Page/玉川達文.md" title="wikilink">玉川達文</a></p></td>
<td><p>2月14日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>CURRICULUM 06</p></td>
<td><p>PRO-ING</p></td>
<td><p>PRO-ING</p></td>
<td><p>星合貴彥</p></td>
<td><p>本橋秀之</p></td>
<td><p>2月21日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CURRICULUM 07</p></td>
<td></td>
<td><p>陸戰</p></td>
<td><p>西村博之</p></td>
<td><p><a href="../Page/成田歲法.md" title="wikilink">成田歲法</a></p></td>
<td><p>伊東克修</p></td>
<td><p>2月28日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>CURRICULUM 08</p></td>
<td></td>
<td><p>死</p></td>
<td><p>須藤隆</p></td>
<td><p>河野利幸</p></td>
<td><p>3月6日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CURRICULUM 09</p></td>
<td></td>
<td><p>夢</p></td>
<td><p><a href="../Page/堺三保.md" title="wikilink">堺三保</a></p></td>
<td><p>星合貴彥</p></td>
<td><p><a href="../Page/日向正樹.md" title="wikilink">日向正樹</a></p></td>
<td><p>3月13日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>CURRICULUM 10</p></td>
<td></td>
<td><p>嫉妒</p></td>
<td><p>桶谷顯</p></td>
<td><p><a href="../Page/志水淳兒.md" title="wikilink">志水淳兒</a></p></td>
<td><p><a href="../Page/玉川達文.md" title="wikilink">玉川達文</a></p></td>
<td><p>3月20日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>CURRICULUM 11</p></td>
<td></td>
<td><p>漂流</p></td>
<td><p>西村博之</p></td>
<td><p>成田歲法</p></td>
<td><p>伊東克修</p></td>
<td><p>3月27日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>CURRICULUM 12</p></td>
<td></td>
<td><p>夥伴</p></td>
<td><p>（OVA）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相關商品

### 雜誌書

アニメビジュアルブックス。やむなく中断した製作者の無念が伝わるインタビューを收錄。

  - 「女神候補生ビジュアルブック」 ワニブックス ISBN 4-8470-2566-0 2000年4月發售

### CD

ビクターエンタテインメントより發售。

  - 「チャンス」 小泉恒平 2000年2月2日發售
  - 「女神候補生 サントラ」 2000年3月23日發售
  - 「発進前夜 〜Get a dream\!〜」 2000年1月21日發售
      - 出演声優によるイメージソング。
  - 「女神候補生 ドラマアルバム」 2000年7月26日發售
  - 「decade 〜Yukiru Sugisaki 10th Anniversary〜」2005年
      - 杉崎ゆきるの10周年記念に発売された。ほかにもDNANGELやりぜるまいん、ラグーンエンジンのキャラが登場。ゼロ役が福山潤になったり、他の登場声優が兼ね役をやるなど
        一部キャストが変わっている。

### DVD

バンダイビジュアルより發售。「スペシャルカリキュラム」は、最終13話をOVAとしてリリースしたもの。

  - 「女神候補生 Vol.1」 2000年4月25日發售
  - 「女神候補生 Vol.2」 2000年5月25日發售
  - 「女神候補生 Vol.3」 2000年6月25日發售
  - 「女神候補生 Vol.4」 2000年7月25日發售
  - 「女神候補生 スペシャルカリキュラム」 2002年5月25日發售

## 外部連結

  -
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:Comic
Gum](../Category/Comic_Gum.md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:2000年日本電視動畫](../Category/2000年日本電視動畫.md "wikilink")
[Category:衛星動畫劇場](../Category/衛星動畫劇場.md "wikilink")
[Category:2002年日本OVA動畫](../Category/2002年日本OVA動畫.md "wikilink")
[Category:華視外購動畫](../Category/華視外購動畫.md "wikilink")
[Category:超視電視節目](../Category/超視電視節目.md "wikilink")
[Category:机器人动画](../Category/机器人动画.md "wikilink")
[Category:未來題材作品](../Category/未來題材作品.md "wikilink")
[Category:架空專校背景作品](../Category/架空專校背景作品.md "wikilink")