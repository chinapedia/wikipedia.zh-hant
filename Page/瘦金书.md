[Songhuizong.jpg](https://zh.wikipedia.org/wiki/File:Songhuizong.jpg "fig:Songhuizong.jpg")
[Songhuizong6.jpg](https://zh.wikipedia.org/wiki/File:Songhuizong6.jpg "fig:Songhuizong6.jpg")
**瘦金书**亦称**瘦金体**、**鹤体**，[楷书的一种](../Page/楷书.md "wikilink")，由[宋徽宗赵佶](../Page/宋徽宗.md "wikilink")（1082年—1135年）所创。宋徽宗赵佶的楷书字體吸收了[褚遂良](../Page/褚遂良.md "wikilink")、[薛曜](../Page/薛曜.md "wikilink")、[薛稷](../Page/薛稷.md "wikilink")、柳公權、[黃庭堅等人的風格](../Page/黃庭堅.md "wikilink")，并創出新意，运笔挺劲犀利。\[1\]笔道瘦细峭硬而有腴润洒脱的风神，橫似鶴骨，勾若鷺喙，捺彷蘭葉，撇如金針，瘦逸而遒勁，自成一家，自号“瘦金书”。

瘦金体的运笔飘忽快捷，笔迹瘦劲，至瘦而不失其肉，转折处可明显见到藏锋，露鋒等运转提顿的痕迹，是一风格相当独特，灑脫明快，氣韻脫俗的字体，今日的「[仿宋体](../Page/仿宋体.md "wikilink")」，亦是从此中脱出。

此书体以形象论，本应为“瘦筋体”，以“金”易“筋”，是对御书的尊重。\[2\]

## 古人評論

  - [蔡絛](../Page/蔡絛.md "wikilink")《[鐵圍山叢談](../Page/鐵圍山叢談.md "wikilink")》

<!-- end list -->

  - [陶宗儀](../Page/陶宗儀.md "wikilink")《[書史會要](../Page/書史會要.md "wikilink")》

## 現代應用

進入現代，為更好適應[計算機帶給傳統](../Page/計算機.md "wikilink")[書法藝術的衝擊](../Page/書法.md "wikilink")，瘦金書作為一種字體被計算機字庫所收錄。目前有[漢儀科印開發的](../Page/漢儀科印.md "wikilink")**瘦金書**、華康科技（現名[威鋒數位](../Page/威鋒數位.md "wikilink")）開發的**徽宗宮體**以及[方正集團開發的](../Page/方正集團.md "wikilink")**瘦金書**等，且分別含有[繁體](../Page/繁體.md "wikilink")、[簡體兩個版本](../Page/簡體.md "wikilink")。

在中國大陸，一般[建築製圖所用之字體](../Page/建築製圖.md "wikilink")，即為**類瘦金體**（[仿宋體](../Page/仿宋體.md "wikilink")），通稱｢**[工程字](../Page/工程字.md "wikilink")**｣。為[建築](../Page/建築.md "wikilink")[土木科組之新生於](../Page/土木.md "wikilink")[製圖課程時須練習的第一道習題](../Page/製圖.md "wikilink")，且必須練到隨時都能寫出之，不論以後是否繼續從事建築土木業，都必須於簽寫正式文件時使用。

民國時代，廣自書局印行之駢體文鈔、古文觀止、十三家詩鈔等，即為手雕仿宋體，造功精美，惜字版毀於日軍侵華期間。

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [金錯刀](../Page/金錯刀_\(筆法\).md "wikilink")

[Category:楷書](../Category/楷書.md "wikilink")
[Category:漢字書體](../Category/漢字書體.md "wikilink")

1.
2.