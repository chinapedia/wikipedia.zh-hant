[2016_Minolta_Dynax_404si.JPG](https://zh.wikipedia.org/wiki/File:2016_Minolta_Dynax_404si.JPG "fig:2016_Minolta_Dynax_404si.JPG")
广义上，**照相机**是任何可以捕捉和记录影像的设备。最常见的照相机拍摄[可见光的影像](../Page/可见光.md "wikilink")，但并不是所有照相机都需要可见光（如[红外线热像仪](../Page/红外线热像仪.md "wikilink")），有的甚至不需要一个传统意义上的[光源](../Page/光源.md "wikilink")（如[扫描隧道显微镜](../Page/扫描隧道显微镜.md "wikilink")）。很多设备都具备照相机的特征，如[雷达](../Page/雷达.md "wikilink")、[医学成像设备](../Page/醫學影像.md "wikilink")、[天文观测设备等等](../Page/天文观测.md "wikilink")。

## 歷史

  - 1550年意大利的[卡尔达诺将双](../Page/卡尔达诺.md "wikilink")[凸透镜置于原来的针孔位置上](../Page/凸透镜.md "wikilink")，映像的效果比暗箱更为明亮清晰。
  - 1558年意大利的[巴尔巴罗又在卡尔达诺的装置上加上光圈](../Page/巴尔巴罗.md "wikilink")，使成像清晰度大为提高。
  - 1665年德国神父[约翰章设计制作了一种小型的可携带的单镜头反光映像暗箱](../Page/约翰章.md "wikilink")，因为当时没有感光材料，只能用于绘画。
  - 1827年法国的[約瑟夫·尼塞福爾·涅普斯在感光材料上制出了世界上第一张照片](../Page/約瑟夫·尼塞福爾·涅普斯.md "wikilink")，但成像不太清晰，而且需要八个小时的曝光。
  - 1839年8月19日[法国](../Page/法国.md "wikilink")[画家](../Page/画家.md "wikilink")[路易·达盖尔发明了世界上第一台真正的照相机](../Page/路易·达盖尔.md "wikilink")，可携式木箱照相机。\[1\]
  - 1841年[光学家](../Page/光学家.md "wikilink")[沃哥兰德发明了第一台全金属机身的照相机](../Page/沃哥兰德.md "wikilink")。
  - 1845年[德国人](../Page/德国人.md "wikilink")[冯·马腾斯发明了世界上第一台可摇摄](../Page/冯·马腾斯.md "wikilink")150°的转机。
  - 1849年[大卫·布儒斯特发明了立体照相机和双镜头的立体观片镜](../Page/大卫·布儒斯特.md "wikilink")。
  - 1861年[物理学家](../Page/物理学家.md "wikilink")[詹姆斯·麦克斯韦发明了世界上第一张彩色照片](../Page/詹姆斯·麦克斯韦.md "wikilink")。\[2\]
  - 1866年[德国](../Page/德国.md "wikilink")[化学家](../Page/化学家.md "wikilink")[奥托·肖特与光学家](../Page/奥托·肖特.md "wikilink")[卡尔·蔡司在](../Page/卡尔·蔡司.md "wikilink")[蔡司公司发明了正光摄影](../Page/蔡司公司.md "wikilink")[镜头](../Page/镜头.md "wikilink")。
  - 1888年[美国](../Page/美国.md "wikilink")[柯达公司生产出了新型感光材料](../Page/柯达公司.md "wikilink")“[胶卷](../Page/胶卷.md "wikilink")”。同年，柯达公司生产出了世界上第一台安装胶卷的可携式照相机。
  - 1906年美国人[乔治·希拉斯首次使用了](../Page/乔治·希拉斯.md "wikilink")[闪光灯](../Page/闪光灯.md "wikilink")。
  - 1948年11月26日美国[宝丽来公司](../Page/宝丽来公司.md "wikilink")（[Polaroid](../Page/Polaroid.md "wikilink")）在市场推出世界上第一个即时成像相机Polaroid
    95。
  - 1975年美国[柯达公司](../Page/柯达公司.md "wikilink")（[Kodak](../Page/Kodak.md "wikilink")）发明第一台[数码相机](../Page/数码相机.md "wikilink")。

## 结构和元件

通常，照相机主要元件包括：成像元件、暗室、成像介质与成像控制结构。

感光元件可以进行成像。通常是由[光学玻璃制成的](../Page/光学玻璃.md "wikilink")[透镜组](../Page/透镜组.md "wikilink")，称之为[镜头](../Page/镜头.md "wikilink")。对于一些特定的设备，小孔、电磁閥等亦可起到“镜头”的作用。

成像介质则负责捕捉和记录影像。包括[底片](../Page/底片.md "wikilink")、[CCD](../Page/CCD.md "wikilink")、[CMOS等](../Page/CMOS.md "wikilink")。

暗室负责连接[镜头与成像介质](../Page/镜头.md "wikilink")，用以保护成像介质并确保在成像的过程中成像介质不会受到外界的光的干扰。

控制结构可以改变成像或记录影像的方式以及影像最终的成像效果。并对[光圈](../Page/光圈.md "wikilink")、[快门](../Page/快门.md "wikilink")、[對焦等进行控制](../Page/對焦.md "wikilink")。

## 术语

  - [成像平面](../Page/成像平面.md "wikilink")（焦平面）：一般是指成像材料所在的平面或者感應器所在的平面。光经过[镜头聚集在成像平面上](../Page/镜头.md "wikilink")，从而形成清晰的照片。
  - [焦距](../Page/焦距.md "wikilink")：是指镜头距底片的距离。如果焦距合适，景物反射的光通过镜头能够聚集在成像平面上，成为一个点，如果焦距不合适，则成为一个圆，从而导致照片失焦模糊。
  - [曝光](../Page/曝光.md "wikilink")：快门打开时，光线透过镜头，经过光圈，进入暗室，最后照在成像材料上，这个过程称为曝光。
  - [曝光量](../Page/曝光量.md "wikilink")：曝光量是指一次曝光中光线的多少。如果曝光量过低会使得照片颜色发暗，如果曝光量过高会使照片颜色发白，过低或过高都会使照片中的细节丢失。曝光量通常是由光圈值和快门速度共同决定的。
  - [光圈值](../Page/光圈值.md "wikilink")：是指暗室窗口的大小，光圈值越低，窗口越大，则透进的光越多，使得曝光量增加，反之亦然。
  - [快门速度](../Page/快门速度.md "wikilink")：是指快门打开的时间，如果快门速度越慢，打开的时间越长，光透进的越多，使得曝光量增加，反之亦然。如果被摄物是移动的物体，则需要较快的快门速度才能將物體凍結，使得成像清楚。
  - [景深](../Page/景深.md "wikilink")：指照片中景物都能清晰显示的前后距离，在风景照片中要求景深大，较小的光圈能获得较大的景深。
  - [变焦](../Page/变焦.md "wikilink")：數位相機之變焦分為光學與數位兩種。
      - [光學變焦](../Page/光學變焦.md "wikilink")：通過鏡片移動來放大與縮小需要拍攝的景物
      - [数码變焦](../Page/数码變焦.md "wikilink")：簡單地將CCD所截取之影像加以裁剪。
  - [视角](../Page/视角.md "wikilink")：鏡頭成相範圍內的對角線角度
  - [测光](../Page/测光.md "wikilink")：對光線進行測量，從而決定曝光量
  - [测距](../Page/测距.md "wikilink")：測量被攝物與成像平面之間的距離，從而對焦
  - [手动曝光](../Page/手动曝光.md "wikilink")：由使用者自行決定光圈以及快門值，來改變曝光量
  - [自动曝光](../Page/自动曝光.md "wikilink")：由照相機決定光圈以及快門值，來改變曝光量
      - [光圈优先](../Page/光圈优先.md "wikilink")：指拍摄人手动指定一个光圈值，照相机根据测光结果自动计算对应快门速度的曝光模式，适合需要控制景深的场景
      - [快门优先](../Page/快门优先.md "wikilink")：指拍摄人手动指定一个快门速度，照相机根据测光结果自动计算对应光圈值的曝光模式，适合拍摄處於運動中的物体的场景
      - [曝光量补偿](../Page/曝光量补偿.md "wikilink")：由使用者在拍攝的時候，改變快門時間或者改變光圈大小來增加或減少曝光量
  - [手动对焦](../Page/手动对焦.md "wikilink")：由使用者自行調整焦距
  - [自动对焦](../Page/自动对焦.md "wikilink")：由照相機調整焦距

## 分类

  - 根據其成像介質的不同：
      - [底片相機](../Page/底片相機.md "wikilink")
      - [數位照相機](../Page/數位照相機.md "wikilink")

<!-- end list -->

  - 根据取景方式可以分为
      - 双镜头反光相机（[双反相机](../Page/双反相机.md "wikilink")）
      - 單鏡頭反光相機（[單反相機](../Page/單反相機.md "wikilink")）
      - 平視取景相機(Viewfinder)
      - 平視測距器相機(Rangfinder)

<!-- end list -->

  - 根據[鏡頭的安裝方式可分為](../Page/鏡頭.md "wikilink")：
      - [可換鏡頭相機](../Page/可換鏡頭相機.md "wikilink")
      - [固定鏡頭相機](../Page/固定鏡頭相機.md "wikilink")

<!-- end list -->

  - 根据呈现介质的规格：
      - [大画幅相机](../Page/大画幅相机.md "wikilink")

      - [中画幅相机](../Page/中畫幅.md "wikilink")

      -
      - [120相机](../Page/120底片.md "wikilink")

      - [135相机](../Page/135底片.md "wikilink")

      - [APS相机](../Page/APS相机.md "wikilink")

      - [微型相机](../Page/微型相机.md "wikilink")

<!-- end list -->

  - 根据用途可以分为专业相机和消费类相机（[傻瓜相机](../Page/傻瓜相机.md "wikilink")）

## 照相機品牌

  - 日本
      - [佳能(Canon)](../Page/佳能.md "wikilink")

      - [尼康(Nikon)](../Page/尼康.md "wikilink")

      - [索尼(SONY)](../Page/索尼.md "wikilink")

          - [柯尼卡美能達(Konica
            Minolta)](../Page/柯尼卡美能達.md "wikilink")（目前已經被[Sony收購](../Page/Sony.md "wikilink")）
              - [美能達Minolta](../Page/美能達.md "wikilink")（2003年，美能達正式與柯尼卡合併，成為[柯尼卡美能達公司](../Page/柯尼卡美能達.md "wikilink")。）

      - [奧林巴斯(Olympus)](../Page/奧林巴斯.md "wikilink")

      - [松下(Panasonic)](../Page/松下電器.md "wikilink")

      - [卡西歐(Casio)](../Page/卡西歐.md "wikilink")

      - [適馬(Sigma)](../Page/適馬.md "wikilink")

      - [騰龍（Tamron）](../Page/騰龍.md "wikilink")

      - [騎士(Horseman)](../Page/騎士相機.md "wikilink")

      - [百佳(Practika)](../Page/百佳.md "wikilink")

      - [理光(Ricoh)](../Page/理光.md "wikilink")

          - [賓得(Pentax)](../Page/賓得.md "wikilink")

      - [富士膠片(Fujifilm)](../Page/富士膠片.md "wikilink")

      -
      -
      - [瑪米亞(Mamiya)](../Page/瑪米亞.md "wikilink")

      - [騎士(Horseman)](../Page/騎士\(Horseman\).md "wikilink")

      - [雅佳(Arca Swiss)](../Page/雅佳.md "wikilink")
  - 韓國
      - [三星(Samsung)](../Page/三星電子.md "wikilink")
  - 美國
      - [柯達(Kodak)](../Page/柯達.md "wikilink")
      - [寶麗來(Polaroid)](../Page/寶麗來.md "wikilink")
  - 德國
      - [卡爾·蔡司(Carl Zeiss)](../Page/蔡司公司.md "wikilink")

      - [徠卡(Leica)](../Page/徠卡相機.md "wikilink")

      - [密諾斯(Minox)](../Page/密諾斯.md "wikilink")

      - [祿萊(Rollei)](../Page/祿萊.md "wikilink")

      -
      -
      -
      - [Voigtländer](../Page/福倫達.md "wikilink")

      - [Linhof](../Page/林可夫.md "wikilink")

      - [康太時(Contax)](../Page/康泰時.md "wikilink")===\>目前已經退出相機市場
  - 俄羅斯
      - [LOMO](../Page/LOMO公司.md "wikilink")
  - 中華人民共和國
      - [海鷗(Seagull)](../Page/上海海鷗照相機有限公司.md "wikilink")
      - [鳳凰(Phoenix)](../Page/鳳凰光學集團有限公司.md "wikilink")
      - [東風](../Page/上海照相機廠.md "wikilink")
      - [紅旗](../Page/上海照相機二廠_\(上海海鷗照相機有限公司\).md "wikilink")
  - 中華民國（臺灣）
      - [明基(BenQ)](../Page/明基.md "wikilink")
      - [宏碁(Acer)](../Page/宏碁.md "wikilink")
      - [華晶(Altek)](../Page/華晶\(Altek\).md "wikilink")
  - 其它
      - [哈蘇(Hasselblad)](../Page/哈蘇.md "wikilink")
      - [基輔](../Page/基辅牌照相机.md "wikilink")
  - 瑞士
      - [仙娜(Sinar)](../Page/仙娜.md "wikilink")
      - [阿爾帕(Alpa)](../Page/阿爾帕.md "wikilink")

## 參考資料

<references/>

## 参看

  - [暗箱](../Page/暗箱.md "wikilink")
  - [摄影](../Page/摄影.md "wikilink")
  - [攝影機](../Page/攝影機.md "wikilink")
  - [数码相机](../Page/数码相机.md "wikilink")
  - [相片](../Page/相片.md "wikilink")
  - [先進攝影系統](../Page/先進攝影系統.md "wikilink")
  - [針孔相機](../Page/針孔相機.md "wikilink")
  - [數位單眼相機](../Page/數位單眼相機.md "wikilink")
  - [智能手机](../Page/智能手机.md "wikilink")

[照相机](../Category/照相机.md "wikilink")
[Category:光学仪器](../Category/光学仪器.md "wikilink")

1.

2.