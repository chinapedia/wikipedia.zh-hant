[CDSJ6.JPG](https://zh.wikipedia.org/wiki/File:CDSJ6.JPG "fig:CDSJ6.JPG")
[CDSJ_PlayGround.jpg](https://zh.wikipedia.org/wiki/File:CDSJ_PlayGround.jpg "fig:CDSJ_PlayGround.jpg")

**聖若瑟教區中學第六校**（政府立案中是為**聖若瑟教區中學第六校**，）非牟利[私立學校](../Page/私立學校.md "wikilink")，於1931年創立，由[天主教澳門教區持牌](../Page/天主教澳門教區.md "wikilink")。自2007學年起校區開始重建計劃，於2017/9/4正式使用。

## 簡史

### 建立校園

1980年，[天主教澳門教區把青洲河邊馬路撥歸](../Page/天主教澳門教區.md "wikilink")[聖若瑟教區中學並新建校舍](../Page/聖若瑟教區中學.md "wikilink")，在[教育司註冊立案為](../Page/教育暨青年局.md "wikilink")**第六校**；翌年2月16日校本部遷入第六校辦公。

1981年3月21日，校舍舉行揭幕祝聖儀式由澳門護督江樹培[上校剪綵](../Page/上校.md "wikilink")、[高秉常主教揭幕揚及祝聖](../Page/高秉常.md "wikilink")，出席觀禮的有澳門華人領袖[何賢](../Page/何賢.md "wikilink")、著名大律師[宋玉生等](../Page/宋玉生.md "wikilink")200多人，極一時之盛。隨著校本部的落成及遷入，聖若瑟教區中學進行第四次改編，在校本部設師範、高中理組、中四、中五、中六文商組。

1982學年起，初三遷入上課。1983學年在校本部復辦英文部。1986學年，高秉常主教諭令[聖若瑟教區中學第五校行政獨立](../Page/聖若瑟教區中學第五校.md "wikilink")，委任何發全神父為該校校長，亦表示該校和[聖若瑟教區中學正式分家](../Page/聖若瑟教區中學.md "wikilink")。1987年遷夜間部至第二校上課。

1989年5月1日起行政部門大改組，合併為「聖若瑟教區中學行政中心」。翌年的夜間部增設[葡萄牙文](../Page/葡萄牙文.md "wikilink")、[電腦](../Page/電腦.md "wikilink")、[普通話](../Page/普通話.md "wikilink")、[英文等課程](../Page/英文.md "wikilink")；修訂多個規章，包括《校務規章》、《教務規章》、《體育規章》等。1990年度夜間部增設中四、中五文商科課程；成立「聖若瑟教區中學教師聯誼會」。

1991年3月19日，即聖若瑟教區中學建校60週年，[教宗](../Page/教宗.md "wikilink")[若望保祿二世向聖若瑟教區中學發出賀詞](../Page/若望保祿二世.md "wikilink")；同年9月將英文部遷入歸[第五校管理](../Page/聖若瑟教區中學第五校.md "wikilink")。1992年把聖若瑟教區中學教師聯誼會易名為「聖若瑟教區中學教職員聯誼會」；而校園內的鑽禧臺及立德樓也相繼落成，在5月24日的[聖母進教之佑瞻禮當日舉行啟用感恩彌撒](../Page/聖母進教之佑瞻禮.md "wikilink")。1993年2月，聖中管樂團成立。1993學年把第二校初中部遷入校本部上課；[天神巷](../Page/天神巷.md "wikilink")43號新校舍命名為第二、三校，設小學。1996年成立聖中史地學會、聖中救傷隊。

1999年校本部新教學大樓落成啟用；11月4日舉行第25屆校運會，由候任[澳門行政長官](../Page/澳門行政長官.md "wikilink")[何厚鏵主禮](../Page/何厚鏵.md "wikilink")；12月20日，[澳門回歸中國](../Page/澳門回歸中國.md "wikilink")，聖若瑟教區中學全體中學師生參加在[澳門運動場舉行的](../Page/澳門運動場.md "wikilink")「澳門同胞慶祝澳門回歸祖國大型文藝表演」；2005年10月29日，聖若瑟教區中學初三至高三師生參與[第四屆澳門東亞運動會開幕典禮的表演](../Page/2005年東亞運動會.md "wikilink")。2006年3月19日，即聖若瑟教區中學建校75週年，[教宗](../Page/教宗.md "wikilink")[本篤十六世](../Page/本篤十六世.md "wikilink")、[澳門行政長官](../Page/澳門行政長官.md "wikilink")[何厚鏵等向聖若瑟教區中學發出賀詞](../Page/何厚鏵.md "wikilink")。

### 校本部重建

據澳門葡文報章《[句號報](../Page/句號報.md "wikilink")》在2005年年中報道，由[葡萄牙天主教大學和澳門教區合辧的](../Page/葡萄牙天主教大學.md "wikilink")[澳門高等校際學院將遷往青洲](../Page/澳門高等校際學院.md "wikilink")。新校園的佔地面積約二萬九千平方米，建築費用達三億五千萬元。大學有意重新打造澳門作為東方宗教培訓基地的角色，以及成為學術培訓及文化活動的中心。另外，也將會與聖若瑟教區中學校本部共用此新校區，該中學的學生將來可直接升讀該大學。

羅玉成校長在2006學年的[開學禮上宣布](../Page/開學禮.md "wikilink")，天主教澳門教區[黎鴻昇主教已將青洲校本部擴建之圖則呈交](../Page/黎鴻昇.md "wikilink")[土地運輸工務局](../Page/土地運輸工務局.md "wikilink")，意味著聖中之發展已開始。2007學年開始，校園清拆，在2007年9月10日起，對於行政總部以及在校本部上課的學生遷往[第二、三校上課](../Page/聖若瑟教區中學第二、三校.md "wikilink")，高一則遷往[第四校上課](../Page/聖若瑟教區中學第四校.md "wikilink")。9月15日下午3時至6時，校本部的師生、校友一同舉行對校部的告別儀式，翌日正式開展了重建計劃。

隨著2007年9月15日的告別儀式完結後，校園清拆工作隨即開始，預料需要2個學年的時間進行清拆工作，同年，[澳門高等校際學院](../Page/澳門高等校際學院.md "wikilink")
(今聖若瑟大學)正式遷入青洲與聖中共用新校舍，並成為全[港](../Page/香港.md "wikilink")[澳](../Page/澳門.md "wikilink")（以至全[亞洲](../Page/亞洲.md "wikilink")）唯一一所具備[幼稚園到](../Page/幼稚園.md "wikilink")[大學各階段完備的一條龍學習階級的學校](../Page/大學.md "wikilink")。聖若瑟學校校方曾指2012年將建成新校舍，但直至2012年初，原校址仍是一片荒蕪，直至2017年才正式使用，比預期遲了五年。

## 政府立案

聖若瑟教區中學在1980年時向[教育暨青年司註冊立案為](../Page/教育暨青年局.md "wikilink")**聖若瑟教區中學第六校**；但由於在學校本身以該校園為行政總部的所在處，也是學校的根本校部，因此校內行政通告、聖中學生、校友等均稱聖若瑟教區中學第六校為「校本部」至今，而這個也會令校外人士摸不著頭是何解。

## 校歌

<div style="text-align: center;">

**<u>聖若瑟教區中學校歌**</u>

</div>

## 歷任校長

<table>
<thead>
<tr class="header">
<th></th>
<th><p>中文名稱</p></th>
<th><p>外文名稱</p></th>
<th><p>開始擔任日期</p></th>
<th><p>最後擔任日期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>9</p></td>
<td><p>羅玉成神父</p></td>
<td></td>
<td><p>1981年</p></td>
<td><p>1988年</p></td>
<td><p>以<a href="../Page/聖若瑟教區中學.md" title="wikilink">聖若瑟教區中學為任期序</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>高天予神父</p></td>
<td><p>Fr. José KAO</p></td>
<td><p>1989年</p></td>
<td><p>2000年</p></td>
<td><p>因年紀的問題而向<a href="../Page/天主教澳門教區.md" title="wikilink">天主教澳門教區請辭</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>袁偉明神父</p></td>
<td></td>
<td><p>2000年</p></td>
<td><p>2004年6月28日</p></td>
<td><p>校史以來最年輕的校長</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>羅玉成神父</p></td>
<td></td>
<td><p>2004年6月29日</p></td>
<td><p>2017年</p></td>
<td><p>第二次擔任校長<br />
</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>喬樹勇博士</p></td>
<td></td>
<td><p>2018年</p></td>
<td></td>
<td><p>為現任校長</p></td>
</tr>
</tbody>
</table>

## 著名校友

### 教育界

  - [謝國平](../Page/謝國平.md "wikilink")，著名英語教學博士
  - [蔡根祥](../Page/蔡根祥.md "wikilink")，臺灣師範大學文學博士，專研經學，尤其是「尚書」。高雄師範大學經學研究所教授兼所長。
  - [歐犬輝](../Page/歐犬輝.md "wikilink")，著名數學教師

### 政界

  - [崔樂其](../Page/崔樂其.md "wikilink")，知名實業家、慈善家，[誠興銀行創辦人](../Page/誠興銀行.md "wikilink")
  - [李偉農](../Page/李偉農.md "wikilink")；[市政署市政管理委員會副主席](../Page/市政署.md "wikilink")
  - [梁勵](../Page/梁勵.md "wikilink")；前澳門教育暨青年局局長

### 專業界

  - [陳百祥](../Page/陳百祥\(會計師\).md "wikilink")，著名[核數師](../Page/核數師.md "wikilink")

### 演藝界

  - [孫鳳明](../Page/孫鳳明.md "wikilink")，澳門歌手
  - [黃偉燐](../Page/黃偉燐.md "wikilink")，曾為[澳門電視台及](../Page/澳門電視台.md "wikilink")[澳門電台主持](../Page/澳門電台.md "wikilink")，兼任[模特兒](../Page/模特兒.md "wikilink")
  - [陳毅鋒](../Page/陳毅鋒.md "wikilink")，即老陳，[澳門日報](../Page/澳門日報.md "wikilink")、[澳門電訊CyberCTM](../Page/澳門電訊.md "wikilink")、[澳門銀河飲食專欄作家](../Page/澳門銀河.md "wikilink")，著作有《[老陳漫味
    日本四國](../Page/老陳漫味_日本四國.md "wikilink")》。曾主持澳廣視飲食節目《[老陳食通街](../Page/老陳食通街.md "wikilink")》並參與資料搜集，曾於[品報](../Page/品報.md "wikilink")、[SODA雜誌](../Page/SODA雜誌.md "wikilink")、[港澳台自由行及深圳](../Page/港澳台自由行.md "wikilink")[游遍天下等刊物客串撰寫飲食及旅遊稿](../Page/游遍天下.md "wikilink")，不時參與[U
    Magazine](../Page/U_Magazine.md "wikilink")、[新假期](../Page/新假期.md "wikilink")、[明報周刊](../Page/明報周刊.md "wikilink")、[蘋果日報](../Page/蘋果日報.md "wikilink")、[力報關於澳門地道食店的介紹](../Page/力報.md "wikilink")。
  - [何嘉穎](../Page/何嘉穎.md "wikilink")，香港及澳門歌手

### 體育界

  - [杜偉崎](../Page/杜偉崎.md "wikilink")，澳門排球代表隊球員、1998年班社聯會主席\[1\]
  - [梁永滿](../Page/梁永滿.md "wikilink")，澳門手球代表隊球員
  - [李偉權](../Page/李偉權.md "wikilink")，澳門田徑總會理事、兼任聖中體育老師、田徑隊教練
  - [張家彥](../Page/張家彥.md "wikilink")，[尼泊爾奧林匹克委員會名譽成員](../Page/尼泊爾奧林匹克委員會.md "wikilink")

### 科研界

  - [徐裕輝](../Page/徐裕輝.md "wikilink")，澳門動物檢疫監管處處長
  - [李文建](../Page/李文建.md "wikilink")，以人工智能遺傳演算法設計一套旅遊代理程序參加國際貿易經紀比賽中獲得第六名的成績
  - [陳宇聰](../Page/陳宇聰.md "wikilink")，在國際貿易經紀比賽的供應鏈管理比賽上獲得第三名
  - [梁福氣](../Page/梁福氣.md "wikilink")，蓝牙遥控器設計者

### 工商界

  - [關樹權](../Page/關樹權.md "wikilink")，[聯營集團主席](../Page/聯營集團.md "wikilink")
  - [周紹湘](../Page/周紹湘.md "wikilink")，[華輝印刷有限公司董事](../Page/華輝印刷有限公司.md "wikilink")
  - [蕭華洲](../Page/蕭華洲.md "wikilink")，[華輝印刷有限公司董事](../Page/華輝印刷有限公司.md "wikilink")
  - [梁耀偉](../Page/梁耀偉.md "wikilink")，[珠海](../Page/珠海.md "wikilink")[斗門](../Page/斗門.md "wikilink")[德光電子製品廠廠長](../Page/德光電子製品廠.md "wikilink")
  - [馮信堅](../Page/馮信堅.md "wikilink")，[富順製衣廠廠長](../Page/富順製衣廠.md "wikilink")

### 創業名人

  - [張衛峰](../Page/張衛峰.md "wikilink")，澳門首家台式飲品食店[小泉居創辦人](../Page/小泉居.md "wikilink")

## 外部連結

  - [澳門聖若瑟教區中學](http://cdsj6.edu.mo/)

[category:天主教澳門教區學校](../Page/category:天主教澳門教區學校.md "wikilink")

[Category:聖若瑟教區中學學校網絡學校](../Category/聖若瑟教區中學學校網絡學校.md "wikilink")
[Category:澳門天主教學校](../Category/澳門天主教學校.md "wikilink")
[Category:1981年創建的教育機構](../Category/1981年創建的教育機構.md "wikilink")

1.  [/ 精華片段](http://www.youtube.com/watch?v=eKtglmbS7Bw&mode=related)