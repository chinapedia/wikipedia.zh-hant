**季铭义**，出生于[辽宁省](../Page/辽宁省.md "wikilink")[大连市](../Page/大连市.md "wikilink")[金州区](../Page/金州区.md "wikilink")，中国退役足球运动员。他曾效力[大连实德及](../Page/大连实德.md "wikilink")[成都谢菲联](../Page/成都谢菲联.md "wikilink")，司职[后卫](../Page/后卫_\(足球\).md "wikilink")
。他是国内足坛公认的顶尖中后卫之一，也是前[中国国家足球队的成员](../Page/中国国家足球队.md "wikilink")。

2005年，季铭义是唯一一名在俱乐部球队、国家队出满勤的球员，打满了全部90分钟的比赛，在[2005年东亚足球锦标赛作为队长帮助](../Page/2005年东亚足球锦标赛.md "wikilink")[国家队夺标](../Page/中国国家足球队.md "wikilink")，并成为[最有价值球员](../Page/最有价值球员.md "wikilink")。

2008年1月23日，他离开效力了近十年的[大连实德以](../Page/大连实德.md "wikilink")400万元转会[成都谢菲联成为](../Page/成都谢菲联.md "wikilink")“标王”，在赛季结束后，成都队将其挂牌，但国内没有俱乐部表示想引进季铭义，因此很多人都在猜测季铭义可能就此退役。

2009年2月，季铭义接受[韩国](../Page/韩国.md "wikilink")[K联赛球队](../Page/K联赛.md "wikilink")[济州联的试训](../Page/济州联.md "wikilink")。但由于和成都队还有合同，而且语言不通，其球风稳健有余却勇猛不足，外加一旦转会必然涉及转会费。考虑到种种因素，[济州联最终决定放弃引进季铭义](../Page/济州联.md "wikilink")。

2014年1月，季铭义加盟[大连超越](../Page/大连超越.md "wikilink")，之后随[大连超越抵达海埂基地进行试训](../Page/大连超越.md "wikilink")。

## 联赛出场纪录

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p>俱乐部</p></th>
<th><p>国家</p></th>
<th><p>出场</p></th>
<th><p>入球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/大连万达.md" title="wikilink">大连万达</a></p></td>
<td></td>
<td><p>21</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>26</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>24</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>11</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>23</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>18</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>26</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>24</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/大连实德.md" title="wikilink">大连实德</a></p></td>
<td></td>
<td><p>19</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/成都谢菲联.md" title="wikilink">成都谢菲联</a></p></td>
<td></td>
<td><p>24</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/成都谢菲联.md" title="wikilink">成都谢菲联</a></p></td>
<td></td>
<td><p>21</p></td>
<td><p>0</p></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部链接

  -
[J](../Category/大连籍足球运动员.md "wikilink")
[J](../Category/中国国家足球队运动员.md "wikilink")
[J](../Category/大连实德球员.md "wikilink") [M](../Category/季姓.md "wikilink")
[Category:成都谢菲联球员](../Category/成都谢菲联球员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")