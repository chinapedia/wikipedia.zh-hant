**瓊楠屬**（[學名](../Page/學名.md "wikilink")：*Beilschmiedia*）是屬於[樟目](../Page/樟目.md "wikilink")[樟科植物的一個](../Page/樟科.md "wikilink")[屬](../Page/屬_\(生物\).md "wikilink")，有多種植物。主要分佈於[東南亞](../Page/東南亞.md "wikilink")、[熱帶非洲](../Page/熱帶非洲.md "wikilink")、[大洋洲與](../Page/大洋洲.md "wikilink")[美洲](../Page/美洲.md "wikilink")。

## 種

此列表是屬於瓊楠屬的各種植物，此列表可能不完整。
[BeilschmiediaTawaKing.jpg](https://zh.wikipedia.org/wiki/File:BeilschmiediaTawaKing.jpg "fig:BeilschmiediaTawaKing.jpg")
King, Martha 1803-1897. Wakefield, Edward Jerningham. "Aventure en New
Zeland". Pl. 13, nº 2\]\]

  - *[Beilschmiedia
    albiramea](../Page/Beilschmiedia_albiramea.md "wikilink")*
  - *[Beilschmiedia
    alloiophylla](../Page/Beilschmiedia_alloiophylla.md "wikilink")*
    (Rusby) Kosterm.
  - Beilschmiedia ambigua, Frans Hubert Edouard Arthur Walter Robyns &
    R. Wilczek
  - *[Beilschmiedia anay](../Page/Beilschmiedia_anay.md "wikilink")*,
    (S.F. Blake) Kosterm.
  - [山潺](../Page/山潺.md "wikilink")（Beilschmiedia appendiculata）, S. Lee
    et Y. T. Wei
  - *[B. assamica](../Page/B._assamica.md "wikilink")* (Syn.: B.
    praecox)
  - [保亭琼楠](../Page/保亭琼楠.md "wikilink") Beilschmiedia baotingensis, S.
    Lee et Y. T. Wei
  - *[Beilschmiedia
    brachythyrsa](../Page/Beilschmiedia_brachythyrsa.md "wikilink")*
    H.W. Li
  - Beilschmiedia berteroana, (Claude Gay) André Joseph Guillaume Henri
    Kostermans, (Southern belloto)
  - Beilschmiedia bracteata,
    [Robyns](../Page/Frans_Hubert_Edouard_Arthur_Walter_Robyns.md "wikilink")
    & R. Wilczek
  - Beilschmiedia brenesii
  - [短序瓊楠](../Page/短序瓊楠.md "wikilink")（Beilschmiedia brevipaniculata）
    [Allen](../Page/Allen.md "wikilink")
  - Beilschmiedia brevipes, Ridley
  - *[Beilschmiedia
    bullata](../Page/Beilschmiedia_bullata.md "wikilink")*
  - *[Beilschmiedia
    castrisinensis](../Page/Beilschmiedia_castrisinensis.md "wikilink")*
  - *[Beilschmiedia
    costaricensis](../Page/Beilschmiedia_costaricensis.md "wikilink")*
    Mez & Pittier
  - *[Beilschmiedia
    curviramea](../Page/Beilschmiedia_curviramea.md "wikilink")*
    (Meisn.) Kosterm.
  - [柱果瓊楠](../Page/柱果瓊楠.md "wikilink")（Beilschmiedia cylindrica）
  - [美脈瓊楠](../Page/美脈瓊楠.md "wikilink")（Beilschmiedia delicata S．Lee ＆
    Y．T．Wei
  - *[Beilschmiedia
    dictyoneura](../Page/Beilschmiedia_dictyoneura.md "wikilink")*
  - *[Beilschmiedia
    elliptica](../Page/Beilschmiedia_elliptica.md "wikilink")*
  - [瓊楠或](../Page/瓊楠.md "wikilink")[九瓊舅](../Page/九瓊舅.md "wikilink")（Beilschmiedia
    erythrophloia）, [Hayata](../Page/Hayata.md "wikilink")
  - [白柴果](../Page/白柴果.md "wikilink")（Beilschmiedia fasciata）, H. W. Li
  - *[Beilschmiedia
    fluminensis](../Page/Beilschmiedia_fluminensis.md "wikilink")*
    Kosterm. ([Brazil](../Page/Brazil.md "wikilink"))
  - [廣東瓊楠](../Page/廣東瓊楠.md "wikilink")（Beilschmiedia fordii）, Dunn
  - [糠秕瓊楠](../Page/糠秕瓊楠.md "wikilink")（Beilschmiedia furfuracea）, Chun
    et H. T. Chang
  - *[Beilschmiedia
    gammieana](../Page/Beilschmiedia_gammieana.md "wikilink")*
  - *[Beilschmiedia
    gemmiflora](../Page/Beilschmiedia_gemmiflora.md "wikilink")*
    Kosterm.
  - Beilschmiedia giorgii, Frans Hubert Edouard Arthur Walter Robyns &
    R. Wilczek
  - [腺葉瓊楠](../Page/腺葉瓊楠.md "wikilink")（Beilschmiedia glandulosa）, N. H.
    Xia， F. N. Wei & Y. F. Deng
  - [粉背瓊楠](../Page/粉背瓊楠.md "wikilink")（Beilschmiedia glauca）
  - *[Beilschmiedia
    hondurensis](../Page/Beilschmiedia_hondurensis.md "wikilink")*,
    Kosterm.
  - ''[Beilschmiedia
    immersinervis](../Page/Beilschmiedia_immersinervis.md "wikilink"),
    Sa. Nishida
  - Beilschmiedia insignis, James Sykes Gamble
  - [瓊楠或](../Page/瓊楠.md "wikilink")[荔枝公](../Page/荔枝公.md "wikilink")（Beilschmiedia
    intermedia）
  - *[Beilschmiedia
    javanica](../Page/Beilschmiedia_javanica.md "wikilink")*, Miq
  - *[Beilschmiedia
    kunstleri](../Page/Beilschmiedia_kunstleri.md "wikilink")*, Gamble
  - Beilschmiedia kweo, (Mildbr.) Robyns & Wilczek
  - [紅枝瓊楠或](../Page/紅枝瓊楠.md "wikilink")[大葉槁](../Page/大葉槁.md "wikilink")（Beilschmiedia
    laevis）
  - *[Beilschmiedia
    letouzeyi](../Page/Beilschmiedia_letouzeyi.md "wikilink")*
  - [李欖瓊楠](../Page/李欖瓊楠.md "wikilink")（Beilschmiedia linocieroides）
  - [長柄瓊楠](../Page/長柄瓊楠.md "wikilink")（Beilschmiedia longipetiolata）
  - *[Beilschmiedia
    lucidula](../Page/Beilschmiedia_lucidula.md "wikilink")*, (Miq.)
    Kosterm.
  - Beilschmiedia lumutensis, James Sykes Gamble
  - [肉柄瓊楠](../Page/肉柄瓊楠.md "wikilink")（Beilschmiedia macropoda）
  - [馬達加斯加瓊楠](../Page/馬達加斯加瓊楠.md "wikilink") Beilschmiedia
    madagascariensis
  - *[Beilschmiedia
    madang](../Page/Beilschmiedia_madang.md "wikilink")*, Blume ,
    ([Indonesia](../Page/Indonesia.md "wikilink"))
  - *[Beilschmiedia
    maingayi](../Page/Beilschmiedia_maingayi.md "wikilink")*
  - *[Beilschmiedia
    malaccensis](../Page/Beilschmiedia_malaccensis.md "wikilink")*
  - Beilschmiedia mayumbensis, Frans Hubert Edouard Arthur Walter Robyns
    & R. Wilczek
  - Beilschmiedia membranacea, James Sykes Gamble
  - [墨西哥瓊楠](../Page/墨西哥瓊楠.md "wikilink") Beilschmiedia mexicana
  - Beilschmiedia miersii (Northern belloto), (Claude Gay) André Joseph
    Guillaume Henri Kostermans
  - [瘤果瓊楠](../Page/瘤果瓊楠.md "wikilink")（Beilschmiedia muricata）, H. T.
    Chang
  - [銹葉瓊楠](../Page/銹葉瓊楠.md "wikilink")（Beilschmiedia obconica）
  - [隱脈瓊楠](../Page/隱脈瓊楠.md "wikilink")（Beilschmiedia obscurinervia）, H.
    T. Chang
  - *[Beilschmiedia
    obtusifolia](../Page/Beilschmiedia_obtusifolia.md "wikilink")*
    (Meisn.) F.Muell., blush-walnut, hard bolly-gum
  - *[Beilschmiedia
    oligandra](../Page/Beilschmiedia_oligandra.md "wikilink")*
  - *[Beilschmiedia
    ovalis](../Page/Beilschmiedia_ovalis.md "wikilink")*, (S.F. Blake)
    C.K. Allen
  - Beilschmiedia pahangensis, James Sykes Gamble
  - [少花瓊楠](../Page/少花瓊楠.md "wikilink")（Beilschmiedia pauciflora）
  - Beilschmiedia penangiana, James Sykes Gamble
  - *[Beilschmiedia
    pendula](../Page/Beilschmiedia_pendula.md "wikilink")*, (Sw.) Hemsl.
  - *[Beilschmiedia
    peninsularis](../Page/Beilschmiedia_peninsularis.md "wikilink")*
  - [厚葉瓊楠](../Page/厚葉瓊楠.md "wikilink")（Beilschmiedia percoriacea）, Allen
  - [紙葉瓊楠或](../Page/紙葉瓊楠.md "wikilink")[黑葉瓊楠](../Page/黑葉瓊楠.md "wikilink")（Beilschmiedia
    pergamentacea）
  - Beilschmiedia preussii, Engler
  - *[Beilschmiedia
    punctilimba](../Page/Beilschmiedia_punctilimba.md "wikilink")* H.W.
    Li
  - *[Beilschmiedia rigida](../Page/Beilschmiedia_rigida.md "wikilink")*
    (Mez) Kosterm.
  - [粗壯瓊楠](../Page/粗壯瓊楠.md "wikilink")（Beilschmiedia robusta）
  - [稠瓊楠或](../Page/稠瓊楠.md "wikilink")[洛氏瓊楠](../Page/洛氏瓊楠.md "wikilink")（Beilschmiedia
    roxburghiana）, Christian Gottfried Daniel Nees von Esenbeck
  - *[Beilschmiedia
    rufohirtella](../Page/Beilschmiedia_rufohirtella.md "wikilink")*
    H.W. Li
  - [西疇瓊楠](../Page/西疇瓊楠.md "wikilink") （Beilschmiedia sichourensis）
  - Beilschmiedia sikkimensis
  - *[Beilschmiedia
    sulcata](../Page/Beilschmiedia_sulcata.md "wikilink")* (Ruiz & Pav.)
    Kosterm.
  - Beilschmiedia tarairi (Taraire), A. Cunn.
  - [塔瓦琼楠](../Page/塔瓦琼楠.md "wikilink") *Beilschmiedia tawa* (Tawa),
    Allan Cunningham Thomas Kirk
    （此樹僅在[紐西蘭生長](../Page/紐西蘭.md "wikilink")，[昆士半琼楠應為](../Page/昆士半琼楠.md "wikilink")*Endiandra
    palmerstonii*（Queensland walnut））
  - *[Beilschmiedia
    tilaranensis](../Page/Beilschmiedia_tilaranensis.md "wikilink")*,
    Sa. Nishida
  - *[Beilschmiedia tooram](../Page/Beilschmiedia_tooram.md "wikilink")*
  - *[Beilschmiedia
    tovarensis](../Page/Beilschmiedia_tovarensis.md "wikilink")*
    (Meisn.) Sa. Nishida
  - [華河瓊楠或](../Page/華河瓊楠.md "wikilink")[網脈瓊楠或](../Page/網脈瓊楠.md "wikilink")[懷德瓊楠或](../Page/懷德瓊楠.md "wikilink")[廣東瓊楠或](../Page/廣東瓊楠.md "wikilink")[毛瓊楠](../Page/毛瓊楠.md "wikilink")（Beilschmiedia
    tsangii）, [Merr.](../Page/Merr..md "wikilink")
  - [東方瓊楠或](../Page/東方瓊楠.md "wikilink")[大果抽蚊](../Page/大果抽蚊.md "wikilink")（Beilschmiedia
    tungfangensis）
  - Beilschmiedia ugandensis, Alfred Barton Rendle
  - *[Beilschmiedia
    undulata](../Page/Beilschmiedia_undulata.md "wikilink")*
  - *[Beilschmiedia
    velutina](../Page/Beilschmiedia_velutina.md "wikilink")*
  - Beilschmiedia vermoesenii, Frans Hubert Edouard Arthur Walter Robyns
    & R. Wilczek
  - [黃志瓊楠或](../Page/黃志瓊楠.md "wikilink")[滇楠](../Page/滇楠.md "wikilink")（Beilschmiedia
    wangii）, Allen
  - *[Beilschmiedia
    wightii](../Page/Beilschmiedia_wightii.md "wikilink")*
  - [雅安瓊楠](../Page/雅安瓊楠.md "wikilink")（Beilschmiedia yaanica）
  - [滇瓊楠或](../Page/滇瓊楠.md "wikilink")[滇拜土密木](../Page/滇拜土密木.md "wikilink")（Beilschmiedia
    yunnanensis）, Hu
  - *[Beilschmiedia
    zapoteoides](../Page/Beilschmiedia_zapoteoides.md "wikilink")*
  - Beilschmiedia zeylanica, Henry Trimen

## 外部链接

  - \[<http://www.hebesoc.org/nz_plants/nz_plants_b/beilschmiedia_tawa/beilschmiedia_tawa.htm>*Beilschmiedia*:
    information from Hebe Society New Zealand\]

<!-- end list -->

  - \[<http://www.florachilena.cl/Niv_tax/Angiospermas/Ordenes/Laurales/Lauraceae/Beilschmiedia/Beilschmiedia.htm>*Beilschmiedia*:
    information from Encyclopedia of Chilean Flora\]

[Category:樟科](../Category/樟科.md "wikilink")
[瓊楠屬](../Category/瓊楠屬.md "wikilink")