**秦州**，[中国古代的](../Page/中国.md "wikilink")[州](../Page/州.md "wikilink")。

[西晋](../Page/西晋.md "wikilink")[泰始五年](../Page/泰始_\(西晋\).md "wikilink")（269年）分[雍](../Page/雍州.md "wikilink")、[凉](../Page/凉州.md "wikilink")、[梁三州置](../Page/梁州.md "wikilink")，治[冀县](../Page/冀县.md "wikilink")（今[甘肃省](../Page/甘肃省.md "wikilink")[甘谷县东](../Page/甘谷县.md "wikilink")）。[太康七年](../Page/太康_\(西晋\).md "wikilink")（286年）移治[上邽县](../Page/上邽县.md "wikilink")（今[天水市](../Page/天水市.md "wikilink")）。辖境相当今[甘肃省](../Page/甘肃省.md "wikilink")[定西市](../Page/定西市.md "wikilink")、[静宁县以南](../Page/静宁县.md "wikilink")，[清水县以西](../Page/清水县.md "wikilink")，[陕西省](../Page/陕西省.md "wikilink")[凤县](../Page/凤县.md "wikilink")、[略阳县](../Page/略阳县.md "wikilink")、[四川省](../Page/四川省.md "wikilink")[平武县以北](../Page/平武县.md "wikilink")，[青海省](../Page/青海省.md "wikilink")[黄河以南](../Page/黄河.md "wikilink")[贵德县及甘肃省](../Page/贵德县.md "wikilink")[临潭](../Page/临潭.md "wikilink")、[迭部等县以东的](../Page/迭部.md "wikilink")[渭河](../Page/渭河.md "wikilink")、[西汉水](../Page/西汉水.md "wikilink")、[白龙江上游和](../Page/白龙江.md "wikilink")[洮河流域等广大地区](../Page/洮河.md "wikilink")。其后辖境缩小。[北魏渭河上游以西分属](../Page/北魏.md "wikilink")[河州](../Page/河州.md "wikilink")、[凉州](../Page/凉州.md "wikilink")；西汉水上游西部属[宕昌](../Page/宕昌.md "wikilink")、[邓至](../Page/邓至.md "wikilink")，东部属[梁州](../Page/梁州.md "wikilink")。北魏[正光五年](../Page/正光.md "wikilink")（524年）州境各族人民推羌人[莫折大提为帅起义](../Page/莫折大提.md "wikilink")。

[唐朝时](../Page/唐朝.md "wikilink")，属[陇右道](../Page/陇右道.md "wikilink")。辖境相当今甘肃省[武山县以东的渭水上游地](../Page/武山县.md "wikilink")。[开元二十二年](../Page/开元.md "wikilink")（734年）因地震移治敬亲川（今[秦安县西北](../Page/秦安县.md "wikilink")），[天宝元年](../Page/天宝_\(唐朝\).md "wikilink")（742年）迁还旧治。[宝应元年](../Page/宝应_\(年号\).md "wikilink")（762年）地入[吐蕃](../Page/吐蕃.md "wikilink")。[大中收复](../Page/大中_\(唐朝\).md "wikilink")，三年（849年）移治[成纪县](../Page/成纪县.md "wikilink")（今秦安县西北，[北宋移今天水市](../Page/北宋.md "wikilink")）。置[天雄军节度使治此](../Page/天雄军节度使.md "wikilink")。

[宋为](../Page/宋朝.md "wikilink")[秦凤路治](../Page/秦凤路.md "wikilink")。[金属](../Page/金朝.md "wikilink")[凤翔路](../Page/凤翔路.md "wikilink")。[元朝属](../Page/元朝.md "wikilink")[巩昌路](../Page/巩昌路.md "wikilink")。[明朝属](../Page/明朝.md "wikilink")[巩昌府](../Page/巩昌府.md "wikilink")，[洪武初年设茶马司于此地](../Page/洪武.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[雍正七年](../Page/雍正.md "wikilink")（1729年），升为[秦州直隸州](../Page/秦州直隸州.md "wikilink")，[直隶甘肃省](../Page/直隶.md "wikilink")。辖境相当今甘肃省天水、秦安、清水、[两当](../Page/两当县.md "wikilink")、[西和](../Page/西和县.md "wikilink")、[礼县](../Page/礼县.md "wikilink")、[徽县](../Page/徽县.md "wikilink")、[成县等市县地](../Page/成县.md "wikilink")。1913年废，改为天水县。

## 参考文献

{{-}}

[Category:晋朝的州](../Category/晋朝的州.md "wikilink")
[Category:北朝的州](../Category/北朝的州.md "wikilink")
[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:北宋的州](../Category/北宋的州.md "wikilink")
[Category:金朝的州](../Category/金朝的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:明朝的州](../Category/明朝的州.md "wikilink")
[Category:清朝的州](../Category/清朝的州.md "wikilink")
[Category:甘肃的州](../Category/甘肃的州.md "wikilink")
[Category:天水行政区划史](../Category/天水行政区划史.md "wikilink")
[Category:陇南行政区划史](../Category/陇南行政区划史.md "wikilink")
[Category:260年代建立的行政区划](../Category/260年代建立的行政区划.md "wikilink")
[Category:1729年废除的行政区划](../Category/1729年废除的行政区划.md "wikilink")