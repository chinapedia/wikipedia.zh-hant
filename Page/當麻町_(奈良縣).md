**當麻町**（）本屬[奈良縣轄下的一個](../Page/奈良縣.md "wikilink")[町](../Page/町.md "wikilink")，在奈良縣西北部，於2004年10月1日與與鄰近同屬[北葛城郡的](../Page/北葛城郡.md "wikilink")[新-{庄}-町合併至](../Page/新庄町_\(奈良縣\).md "wikilink")[葛城市](../Page/葛城市.md "wikilink")。

## 地理

位於[奈良縣西北部](../Page/奈良縣.md "wikilink")[二上山山麓位置](../Page/二上山_\(奈良縣\).md "wikilink")，與[大阪府接壤](../Page/大阪府.md "wikilink")。

## 行政

  - 町長:[安川義雄](../Page/安川義雄.md "wikilink")

## 經濟

  - 栽培[菊花](../Page/菊.md "wikilink")
  - 編織、襪製造業

## 交通

### 鐵路

  - [近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")
      - [南大阪線](../Page/近鐵南大阪線.md "wikilink")：[尺土站](../Page/尺土站.md "wikilink")
        - [磐城站](../Page/磐城站.md "wikilink") -
        [當麻寺站](../Page/當麻寺站.md "wikilink") -
        [二上神社口站](../Page/二上神社口站.md "wikilink")

## 古蹟、觀光及節慶

### 寺院

  - [當麻寺](../Page/當麻寺.md "wikilink")
  - [石光寺](../Page/石光寺.md "wikilink")

## 外部連結

  - [新庄町及當麻町合併議會](https://web.archive.org/web/20070629222339/http://www.town.shinjo.nara.jp/gappei/)
  - [國立國會圖書館](http://warp.ndl.go.jp/)——以當麻町搜尋可搜尋合併相關書籍

[Category:北葛城郡](../Category/北葛城郡.md "wikilink")
[Category:葛城市](../Category/葛城市.md "wikilink")