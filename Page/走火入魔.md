**走火入魔**（[英文](../Page/英文.md "wikilink")：**kundalini
syndrome**、**kundalini psychosis**、**qigong
psychosis**）指在进行[气功练习时](../Page/气功.md "wikilink")，因意外情况造成的轻度[精神病症状](../Page/精神病.md "wikilink")。严重者可发展至器质性神经系统疾病。

  - 在氣功修鍊中，會因個人體質跟心理狀態的不同有許多奇特的現象產生，因為有了偏差（泛指心理跟生理上的）而造成氣血運行不順，引發血管或心臟疾病，神經叢收縮，進而產生幻象或幻聽的現象。
  - 在練習氣功期間，必須要呼吸自然，不要在專注肌肉緊繃或「[氣感](../Page/氣感.md "wikilink")<ref>「[氣感](../Page/氣感.md "wikilink")」通常指下述的[生理](../Page/生理.md "wikilink")[反應](../Page/反應.md "wikilink")：

<!-- end list -->

1.  當深度放鬆時，[微血管的微循環旺盛起來時的各種](../Page/微血管.md "wikilink")[感覺](../Page/感覺.md "wikilink")，多數是暖，如果還有麻癢的話，[中醫還說這是有](../Page/中醫.md "wikilink")「風」。初學者常說這是「氣到指尖」。
2.  [呼吸深長](../Page/呼吸.md "wikilink")，血氣旺盛，肢體[大腦得到充足供血供](../Page/大腦.md "wikilink")[氧時的精神爽利](../Page/氧.md "wikilink")[感覺](../Page/感覺.md "wikilink")。常被描述為「感到一股氣湧上來」，甚至是大小周天運行，內力增進等。
3.  進入類似默劇或自我[催眠的狀態](../Page/催眠.md "wikilink")，嘗試用[意識去影響不容易控制的部位時的](../Page/意識.md "wikilink")[感覺](../Page/感覺.md "wikilink")（[內臟之類的不隨意肌](../Page/內臟.md "wikilink")）。有時肢體還會處於一種平時很少會做的姿勢下（不同的樁功），[重心](../Page/重心.md "wikilink")、負重位置都跟慣常的有所不同，再配合設想和[呼吸](../Page/呼吸.md "wikilink")，因而[刺激到一些平時很少](../Page/刺激.md "wikilink")[運動的位置以至](../Page/運動.md "wikilink")[內臟](../Page/內臟.md "wikilink")[器官](../Page/器官.md "wikilink")，使之都得到[運動](../Page/運動.md "wikilink")，為習練者帶來一種不常見，但又很愜意的個人境界[經歷](../Page/經歷.md "wikilink")。這往往會被稱為以意導氣，以氣導體，以至打通手少陰心經、手太陰肺經……等等。
4.  [神經的生物電反應](../Page/神經.md "wikilink")，例如肉跳、眼眉跳、以至[抽筋等](../Page/抽筋.md "wikilink")。如果這些反應是氣功修習著希望它發生的，或者在控制和預期下發生的，可能會說這是「神功大成」了；如果是不想它發生的，或者失控地發生的，就叫「出偏差」，以至「**走火入魔**」</ref>」在那裡期間，不知不覺下閉了呼吸，人便會休克，傷了腦部。

<!-- end list -->

  - 最重要的是，此時氣功已經在自己的意志下呼吸，而不是出於自然了（已經自我催眠），如果當時由於一些外來引導，胡思亂想，或自我潛意識作用，有可能引起呼吸困難，或做出不能自己控制的潛藏心裡的動作，等傷害自己的後果。

## 註解

<div class="references-small">

<references />

</div>

## 參見

  - [氣功](../Page/氣功.md "wikilink")
  - [南拳](../Page/南拳.md "wikilink")
  - [中國武術](../Page/中國武術.md "wikilink")

[Category:气功](../Category/气功.md "wikilink")
[Category:症狀](../Category/症狀.md "wikilink")
[Category:文化結合症候群](../Category/文化結合症候群.md "wikilink")