**伊兹密特**（），又名**科贾埃利**、**科喀艾里**（**Kocaeli**），是位于[土耳其西北部的城市](../Page/土耳其.md "wikilink")，也是[科贾埃利省的首府](../Page/科贾埃利省.md "wikilink")，共有人口300,611人（2011年人口普查）。伊兹密特是一个重要的工业中心，拥有大量石化、造纸和水泥工厂设施。同时，它还是交通枢纽和主要港口。

## 大事記

  - 337年5月22日，[君士坦丁大帝在伊兹密特附近的皇家别墅中去世](../Page/君士坦丁大帝.md "wikilink")
  - 1999年8月17日，伊兹密特发生了**[里氏7.4级的地震](../Page/1999年伊茲密特地震.md "wikilink")**，造成约19,000人遇难
  - 2012年11月16日，科喀艾里的防震减灾项目“Prepare Before It's Too Late: Learn To Live
    With
    Earthquake（未雨綢繆：學會在地震下生存）\[1\]”获得[广州国际城市创新奖](../Page/广州国际城市创新奖.md "wikilink")\[2\]\[3\]

## 參考資料

[Category:土耳其城市](../Category/土耳其城市.md "wikilink")
[Category:科賈埃利省](../Category/科賈埃利省.md "wikilink")

1.  [Prepare Before It's Too Late: Learn To Live With
    Earthquake](http://www.uclg.org/sites/default/files/Kocaeli.pdf)
2.  [Kocaeli | UCLG](http://www.uclg.org/en/node/4110)
3.