**王尚義**（），[臺灣](../Page/臺灣.md "wikilink")[作家](../Page/作家.md "wikilink")，[河南](../Page/河南.md "wikilink")[汜水人](../Page/汜水.md "wikilink")。

## 生平

1948年，王尚義隨家人自[河南輾轉逃難到](../Page/河南.md "wikilink")[香港](../Page/香港.md "wikilink")[調景嶺](../Page/調景嶺.md "wikilink")，又遷往[高雄](../Page/高雄.md "wikilink")、[澎湖](../Page/澎湖.md "wikilink")，就讀[員林實驗中學](../Page/員林實驗中學.md "wikilink")。王尚義之後考入[國立台灣大學醫學院牙醫系](../Page/國立台灣大學醫學院.md "wikilink")。王尚義是牙科學生，但真正的興趣是[哲學與](../Page/哲學.md "wikilink")[文學](../Page/文學.md "wikilink")，會拉[小提琴](../Page/小提琴.md "wikilink")、會畫[油畫](../Page/油畫.md "wikilink")、會[刻印](../Page/刻印.md "wikilink")，多才多藝，還經常在校刊中寫稿。跟當時台大才子[李敖走得很近](../Page/李敖.md "wikilink")，其妹[王尚勤曾為李敖女友](../Page/王尚勤.md "wikilink")，李敖的非婚生女儿[李文即王尚勤所出](../Page/李文（作家）.md "wikilink")。王尚義在大二時想轉哲學系，但被父親拒絕。\[1\]又和表妹胡建華談起戀愛，卻受到兼任[國大代表的姑媽強烈反對](../Page/國大代表.md "wikilink")，轉而[學佛](../Page/學佛.md "wikilink")。曾在[獅頭山專心著作](../Page/獅頭山.md "wikilink")。課業輒常借同學筆記應付充數，因此留級兩年。\[2\]醫學系畢業不久，便因[肝癌而英年早逝](../Page/肝癌.md "wikilink")。\[3\]墓碑上刻有他自己生前寫的一首小詩：“像那山忘記那雲，像那樹忘記那風，像那橋忘記那水底幽情。明天的路上，有水、有雲、有風。讓生命在今天沈默吧！”

身後，家人、朋友整理出版他的遺稿，出版了六本著作；最著名的著作是《從異鄉人到失落的一代》及《野鴿子的黃昏》，[文星書店出版](../Page/文星書店.md "wikilink")《從異鄉人到失落的一代》。1966年[水牛出版社出版](../Page/水牛出版社.md "wikilink")《野鴿子的黃昏》，熱賣數十萬本。有人評其作品太過陰鬱，有女學生因為讀其遺作《野鴿子的黃昏》而自殺身亡。\[4\]

## 作品

  - 《狂流》
  - 《深谷足音》
  - 《落霞與孤鶩》
  - 《荒野流泉》
  - 《從異鄉人到失落的一代》
  - 《野鴿子的黃昏》
  - 《野百合花》

## 注釋

## 外部連結

  - [博客來書籍館\>出版社專區\>水牛\>王尚義作品集](https://web.archive.org/web/20070524015434/http://www.books.com.tw/exep/prod/books/editorial/publisher_booklist.php?pubid=buffalo&qseries=buffalo51DA)
  - [王尚義和他所處的時代](http://mjlsh.usc.cuhk.edu.hk/Book.aspx?cid=8&tid=48)

[Category:台灣作家](../Category/台灣作家.md "wikilink")
[Category:罹患肝癌逝世者](../Category/罹患肝癌逝世者.md "wikilink")
[Category:國立臺灣大學校友](../Category/國立臺灣大學校友.md "wikilink")

1.  王尚義曾向其弟王長安訴苦：“弟弟，你不知道，我讀醫學讀得好痛苦哦！”王長安還回憶，王尚義的室友曾對他說：“你哥哥在那兒拉小提琴，馬上就回來。你到裡面等一下。”接著他又回頭說了一句話：“你哥哥實在不該讀醫學院！”。（王長安：〈我的哥哥王尚義〉）
2.  吳興鏞：《黃金檔案》
3.  吳興鏞在《黃金檔案》裡回憶王尚義，稱王尚義是得了膽管腺癌，並非肝癌。
4.  王長安：〈我的哥哥王尚義〉