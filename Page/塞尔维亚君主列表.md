**塞爾維亞君主列表**列出[塞爾維亞從早期歷史記載](../Page/塞爾維亞.md "wikilink")，直至1945年帝制被廢止間，所有的君主名稱。

## 中世紀時期

[Serb_lands02.jpg](https://zh.wikipedia.org/wiki/File:Serb_lands02.jpg "fig:Serb_lands02.jpg")
中世紀時期，[塞爾維亞分為五個部落或](../Page/塞爾維亞.md "wikilink")[公國](../Page/公國.md "wikilink")，由北至南列舉如下：

  - [拉什卡](../Page/拉什卡.md "wikilink")（）與[波斯尼亞](../Page/波斯尼亞.md "wikilink")（）
  - [帕干尼亞](../Page/帕干尼亞.md "wikilink")（）
  - [扎胡姆列](../Page/扎胡姆列.md "wikilink")（）
  - [特拉翁亞](../Page/特拉翁亞.md "wikilink")（）
  - [杜克里亞](../Page/杜克里亞.md "wikilink")（）

### 最早的統治者

  - [捷尔万](../Page/捷尔万.md "wikilink")（，拉丁化：），統治[白塞爾維亞](../Page/白塞爾維亞.md "wikilink")（，又稱**波耶卡**，位於中歐東北方，[塞爾維亞族發源地](../Page/塞爾維亞人.md "wikilink")），（?－626年）。

### 早期統治者

  - **[不知名的王子](../Page/不知名的王子.md "wikilink")**（），於7世紀早期，當[席哈克略統治](../Page/席哈克略.md "wikilink")[拜占廷帝國時](../Page/拜占廷帝國.md "wikilink")，帶領塞爾維亞人，從白塞爾維亞移居至[巴爾幹半島](../Page/巴爾幹半島.md "wikilink")。
  - [斯威夫拉德](../Page/斯威夫拉德.md "wikilink")（，拉丁化：），（?－660年），統治拉什卡的[波德戈里察](../Page/波德戈里察.md "wikilink")。
  - [谢利米爾](../Page/谢利米爾.md "wikilink")（，拉丁化：），（?－679或680年）。
  - [弗拉金](../Page/弗拉金.md "wikilink")（，拉丁化：），**不知名的王子**後裔，（?－700年）。
  - [拉季米爾](../Page/拉季米爾.md "wikilink")（，拉丁化：），**不知名的王子**後裔，（?－730年）。

### 全面統治塞爾維亞各部

  - [烏伊謝斯拉夫王子](../Page/烏伊謝斯拉夫.md "wikilink")（，拉丁化：），**不知名的王子**曾孫，（?－780年）。
  - [拉多斯拉夫王子](../Page/拉多斯拉夫.md "wikilink")（，拉丁化：），**烏伊謝斯拉夫**的兒子，（?－約800年）。
  - [普路斯哥耶王子](../Page/普路斯哥耶.md "wikilink")（，拉丁化：），**拉多斯拉夫**的兒子，（?－約836年）。

### [弗拉斯提米洛維奇王朝](../Page/弗拉斯提米洛維奇王朝.md "wikilink")

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/弗拉斯提米爾.md" title="wikilink">弗拉斯提米爾王子</a><br />
<small></small></p></td>
<td><p>830年</p></td>
<td><p>851年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/穆提米爾.md" title="wikilink">穆提米爾王子</a><br />
<small></small></p></td>
<td><p>852年</p></td>
<td><p>891年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/皮利畢斯拉夫.md" title="wikilink">皮利畢斯拉夫王子</a><br />
<small></small></p></td>
<td><p>891年</p></td>
<td><p>892年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/彼打爾·哥耶尼可維奇.md" title="wikilink">彼打爾·哥耶尼可維奇王子</a><br />
<small></small></p></td>
<td><p>892年</p></td>
<td><p>918年<br />
（被<a href="../Page/保加利亞人.md" title="wikilink">保加利亞人擄走</a>）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/帕夫列·伯拉努維奇.md" title="wikilink">帕夫列·伯拉努維奇王子</a><br />
<small></small></p></td>
<td><p>917年<br />
（由<a href="../Page/保加利亞.md" title="wikilink">保加利亞扶植</a>）</p></td>
<td><p>921年<br />
（被拜占廷罷黜）</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/扎哈里耶·皮利畢斯拉夫留維奇.md" title="wikilink">扎哈里耶·皮利畢斯拉夫留維奇王子</a><br />
<small></small></p></td>
<td><p>921年<br />
（由<a href="../Page/拜占廷.md" title="wikilink">拜占廷扶植</a>）</p></td>
<td><p>924年<br />
（被保加利亞罷黜）</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Simeon_the_Great_anonymous_seal.jpg" title="fig:Simeon_the_Great_anonymous_seal.jpg">Simeon_the_Great_anonymous_seal.jpg</a></p></td>
<td><p>被保加利亞沙皇<a href="../Page/西美昂一世.md" title="wikilink">西美昂一世所統治</a></p></td>
<td><p>924年</p></td>
<td><p>931年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/察斯拉夫·卡羅尼米洛維奇.md" title="wikilink">察斯拉夫·卡羅尼米洛維奇王子</a><br />
<small></small></p></td>
<td><p>927年<br />
（於塞爾維亞中部脫離保加利亞管治）</p></td>
<td><p>約950年</p></td>
</tr>
</tbody>
</table>

### 拜占廷佔領時期

#### [杜克里亞附庸國](../Page/杜克里亞.md "wikilink")

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:JovanVladimirSlika.jpg" title="fig:JovanVladimirSlika.jpg">JovanVladimirSlika.jpg</a></p></td>
<td><p><a href="../Page/伊万·弗拉基米尔.md" title="wikilink">伊万·弗拉基米尔王子</a><br />
（附庸於<a href="../Page/保加利亞王國.md" title="wikilink">保加利亞王國</a>）<br />
<small></small></p></td>
<td><p>約990年</p></td>
<td><p>1016年</p></td>
</tr>
</tbody>
</table>

### [孚歐意斯拉夫雷維奇王朝](../Page/孚歐意斯拉夫雷維奇王朝.md "wikilink")

**孚歐意斯拉夫雷維奇王朝**（，拉丁化：）為由**斯特凡·孚歐意斯拉夫**王子（，拉丁化：）以降，統治杜克里亞地區的王朝。

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/斯特凡·孚歐意斯拉夫.md" title="wikilink">斯特凡·孚歐意斯拉夫王子</a><br />
<small></small></p></td>
<td><p>1036年<br />
（宣告杜克里亞自拜占廷獨立）</p></td>
<td><p>1050年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/米海羅·孚歐意斯拉夫雷維奇.md" title="wikilink">米海羅·孚歐意斯拉夫雷維奇大王公</a><br />
<small></small></p></td>
<td><p>1050年</p></td>
<td><p>1077年</p></td>
</tr>
<tr class="odd">
<td><p>國王<a href="../Page/米海羅·孚歐意斯拉夫雷維奇.md" title="wikilink">米海羅·孚歐意斯拉夫雷維奇</a><br />
<small></small></p></td>
<td><p>1077年<br />
（受教宗<a href="../Page/額我略七世.md" title="wikilink">額我略七世冊封</a>）</p></td>
<td><p>1081年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Konstantin_Bodin.jpg" title="fig:Konstantin_Bodin.jpg">Konstantin_Bodin.jpg</a></p></td>
<td><p>國王<a href="../Page/康斯坦丁·博丁.md" title="wikilink">康斯坦丁·博丁</a><br />
（曾為保加利亞沙皇）<br />
<small></small></p></td>
<td><p>1081年</p></td>
<td><p>1101年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>國王<a href="../Page/多巴羅斯拉夫.md" title="wikilink">多巴羅斯拉夫</a><br />
<small></small></p></td>
<td><p>1101年</p></td>
<td><p>1102年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/科佐帕爾.md" title="wikilink">科佐帕爾</a><br />
<small></small></p></td>
<td><p>1102年</p></td>
<td><p>1103年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>國王<a href="../Page/弗拉狄米爾.md" title="wikilink">弗拉狄米爾</a><br />
<small></small></p></td>
<td><p>1103年</p></td>
<td><p>1113年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/佐爾杰·波狄挪維奇.md" title="wikilink">佐爾杰·波狄挪維奇</a><br />
<small></small></p></td>
<td><p>1113年</p></td>
<td><p>1118年<br />
（遭格魯別紗王子篡位）</p></td>
</tr>
<tr class="odd">
<td><p>1125年<br />
（戰勝格魯別紗王子，奪回王位）</p></td>
<td><p>1131年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/格魯別紗.md" title="wikilink">格魯別紗王子</a><br />
<small></small></p></td>
<td><p>1118年<br />
（取得拜占廷支持）</p></td>
<td><p>1125年<br />
（戰敗）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>國王<a href="../Page/格拉狄那.md" title="wikilink">格拉狄那</a><br />
<small></small></p></td>
<td><p>1131年</p></td>
<td><p>1146年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/拉多斯拉夫.md" title="wikilink">拉多斯拉夫王子</a><br />
<small></small></p></td>
<td><p>1146年</p></td>
<td><p>1148年<br />
（被拉什卡的爹紗王子所取代）</p></td>
</tr>
<tr class="odd">
<td><p>1162年<br />
（恢復王位）</p></td>
<td><p>1162年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/米海羅·孚歐意斯拉維奇.md" title="wikilink">米海羅·孚歐意斯拉維奇王子</a><br />
<small></small></p></td>
<td><p>1162年</p></td>
<td><p>1186年<br />
（被<a href="../Page/斯特凡·涅曼雅.md" title="wikilink">斯特凡·涅曼雅大公合併</a>）</p></td>
</tr>
</tbody>
</table>

### [孚勘挪維奇王朝](../Page/孚勘挪維奇王朝.md "wikilink")

**孚勘挪維奇王朝**（，拉丁化：）為由**孚勘**大王公（，拉丁化：，**王公**或譯作**茹潘**）以降，統治拉什卡地區的王朝。

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/孚勘.md" title="wikilink">孚勘大王公</a><br />
<small></small></p></td>
<td><p>約1083年<br />
（受<a href="../Page/康斯坦丁·博丁.md" title="wikilink">康斯坦丁·博丁任命管治拉什卡</a>）</p></td>
<td><p>約1116年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/烏洛許一世·孚勘挪維奇.md" title="wikilink">烏洛許一世·孚勘挪維奇大王公</a><br />
<small></small></p></td>
<td><p>1112年</p></td>
<td><p>約1145年</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Géza_II.jpg" title="fig:Géza_II.jpg">Géza_II.jpg</a></p></td>
<td><p><a href="../Page/烏洛許二世·普利密斯拉夫.md" title="wikilink">烏洛許二世·普利密斯拉夫大王公</a><br />
<small></small></p></td>
<td><p>約1145年</p></td>
<td><p>1162年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/別勞許.md" title="wikilink">別勞許</a><br />
<small></small></p></td>
<td><p>1163年</p></td>
<td><p>1163年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/爹紗.md" title="wikilink">爹紗王公</a><br />
<small></small></p></td>
<td><p>1162年</p></td>
<td><p>1166年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/提賀米爾.md" title="wikilink">提賀米爾王公</a><br />
<small></small></p></td>
<td><p>1163年<br />
（由拜占廷選定的拉什卡統治者）</p></td>
<td><p>1166年</p></td>
</tr>
</tbody>
</table>

### [尼曼雅王朝](../Page/尼曼雅王朝.md "wikilink")

[Dusanova_Srbija200.jpg](https://zh.wikipedia.org/wiki/File:Dusanova_Srbija200.jpg "fig:Dusanova_Srbija200.jpg")納入了統治範圍\]\]
[Nemanjić_dynasty_coat_of_arms,_Palavestra.jpg](https://zh.wikipedia.org/wiki/File:Nemanjić_dynasty_coat_of_arms,_Palavestra.jpg "fig:Nemanjić_dynasty_coat_of_arms,_Palavestra.jpg")
**尼曼雅王朝**（，拉丁化：）為由塞尔维亚大[茹潘](../Page/茹潘.md "wikilink")[斯特凡·尼曼雅二世以降](../Page/斯特凡·尼曼雅二世.md "wikilink")，統治拉什卡、杜克利亞、特拉翁亞、達爾馬提亞與扎胡姆列等塞爾維亞人居住地區的王朝。

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Svsimeon.jpg" title="fig:Svsimeon.jpg">Svsimeon.jpg</a></p></td>
<td><p><a href="../Page/斯特凡·尼曼雅一世.md" title="wikilink">斯特凡·尼曼雅一世大茹潘</a><br />
<small></small></p></td>
<td><p>1166年<br />
（宣示成為全拉什卡的大茹潘，並冠上<strong>斯特凡</strong>名號）</p></td>
<td><p>1196年</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stefan_the_First-Crowned,_fresco_from_Mileševa.jpg" title="fig:Stefan_the_First-Crowned,_fresco_from_Mileševa.jpg">Stefan_the_First-Crowned,_fresco_from_Mileševa.jpg</a></p></td>
<td><p><a href="../Page/斯特凡·尼曼雅二世.md" title="wikilink">斯特凡·尼曼雅二世大茹潘</a><br />
<small></small></p></td>
<td><p>1196年</p></td>
<td><p>1217年</p></td>
</tr>
<tr class="odd">
<td><p>國王<a href="../Page/斯特凡·尼曼雅二世.md" title="wikilink">斯特凡·尼曼雅二世</a>（“第一个加冕的”）<br />
<small></small></p></td>
<td><p>1217年<br />
（教宗<a href="../Page/何諾三世.md" title="wikilink">何諾三世賜封為國王</a>）</p></td>
<td><p>1219年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1219年<br />
（<a href="../Page/君士坦丁堡牧首.md" title="wikilink">君士坦丁堡牧首</a><a href="../Page/馬奴艾一世.md" title="wikilink">馬奴艾一世賜封為</a><strong>塞爾維亞東正國王</strong>）</p></td>
<td><p>1227年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stefanradoslav.jpg" title="fig:Stefanradoslav.jpg">Stefanradoslav.jpg</a></p></td>
<td><p>國王<a href="../Page/斯特凡·拉多斯拉夫.md" title="wikilink">斯特凡·拉多斯拉夫</a><br />
<small></small></p></td>
<td><p>1227年</p></td>
<td><p>1234年</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Fresco_of_Stefan_Vladislav,_Mileševa,_edited.jpg" title="fig:Fresco_of_Stefan_Vladislav,_Mileševa,_edited.jpg">Fresco_of_Stefan_Vladislav,_Mileševa,_edited.jpg</a></p></td>
<td><p>國王<a href="../Page/斯特凡·弗拉迪斯拉夫一世.md" title="wikilink">斯特凡·弗拉迪斯拉夫一世</a><br />
<small></small></p></td>
<td><p>1234年</p></td>
<td><p>1243年</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:King_Stefan_Uroš_I_with_his_son_Stefan_Dragutin.jpg" title="fig:King_Stefan_Uroš_I_with_his_son_Stefan_Dragutin.jpg">King_Stefan_Uroš_I_with_his_son_Stefan_Dragutin.jpg</a></p></td>
<td><p>國王<a href="../Page/斯特凡·乌罗什一世.md" title="wikilink">斯特凡·乌罗什一世</a><br />
<small></small></p></td>
<td><p>1243年</p></td>
<td><p>1276年</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:StefanDragutin.jpg" title="fig:StefanDragutin.jpg">StefanDragutin.jpg</a></p></td>
<td><p>國王<a href="../Page/斯特凡·德拉古廷.md" title="wikilink">斯特凡·德拉古廷</a><br />
<small></small></p></td>
<td><p>1276年</p></td>
<td><p>1282年</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Le_roi_Milutin_de_serbie.jpg" title="fig:Le_roi_Milutin_de_serbie.jpg">Le_roi_Milutin_de_serbie.jpg</a></p></td>
<td><p>國王<a href="../Page/斯特凡·乌罗什二世.md" title="wikilink">斯特凡·乌罗什二世</a><br />
<small></small></p></td>
<td><p>1282年</p></td>
<td><p>1321年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·弗拉迪斯拉夫二世.md" title="wikilink">斯特凡·弗拉迪斯拉夫二世</a><br />
<small></small></p></td>
<td><p>1321年<br />
（在保加利亞沙皇<a href="../Page/米哈伊爾·希什曼.md" title="wikilink">米哈伊爾三世支持下稱王</a>）</p></td>
<td><p>1325年<br />
（被<strong>烏羅什三世</strong>驅逐）</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>國王<a href="../Page/斯特凡·乌罗什三世.md" title="wikilink">斯特凡·乌罗什三世</a>（“德查尼的”）<br />
<small></small></p></td>
<td><p>1321年</p></td>
<td><p>1331年</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:CarDusan.jpg" title="fig:CarDusan.jpg">CarDusan.jpg</a></p></td>
<td><p>國王<a href="../Page/斯特凡·乌罗什四世.md" title="wikilink">斯特凡·乌罗什四世</a>（即斯特凡·杜尚）<br />
<small></small></p></td>
<td><p>1331年</p></td>
<td><p>1346年</p></td>
</tr>
<tr class="odd">
<td><p>沙皇<a href="../Page/斯特凡·乌罗什四世.md" title="wikilink">斯特凡·乌罗什四世</a><br />
<small></small></p></td>
<td><p>1346年<br />
（宣示成為塞爾維亞與希臘的沙皇）</p></td>
<td><p>1355年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:UrosV.jpg" title="fig:UrosV.jpg">UrosV.jpg</a></p></td>
<td><p>沙皇<a href="../Page/斯特凡·乌罗什五世.md" title="wikilink">斯特凡·乌罗什五世</a><br />
<small></small></p></td>
<td><p>1355年</p></td>
<td><p>1371年<br />
（烏羅什五世沒有子嗣）</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>沙皇<a href="../Page/西美昂·乌罗什.md" title="wikilink">西美昂·乌罗什</a><br />
<small></small></p></td>
<td><p>1359年<br />
（宣示成為塞爾維亞與希臘的沙皇）</p></td>
<td><p>1370年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>沙皇<a href="../Page/伊万·乌罗什.md" title="wikilink">伊万·乌罗什</a><br />
<small></small></p></td>
<td><p>1370年</p></td>
<td><p>1373年<br />
（退位出家為修道士）</p></td>
</tr>
</tbody>
</table>

### [科特羅曼尼奇王朝](../Page/科特羅曼尼奇王朝.md "wikilink")

[Tvrtko.png](https://zh.wikipedia.org/wiki/File:Tvrtko.png "fig:Tvrtko.png")
**科特羅曼尼奇王朝**（又譯作**科特羅曼王朝**，，[波斯尼亞語](../Page/波斯尼亞語.md "wikilink")、拉丁化：）為由**彼利捷斯達一世**領主（，波斯尼亞語、拉丁化：，**領主**或譯作**都督**）以降，統治[波斯尼亞地區](../Page/波斯尼亞.md "wikilink")，後來擴及塞爾維亞的王朝。

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/彼利捷斯達一世.md" title="wikilink">彼利捷斯達一世領主</a><br />
<small></small></p></td>
<td><p>1250年<br />
（由<a href="../Page/匈牙利.md" title="wikilink">匈牙利國王</a><a href="../Page/貝拉四世.md" title="wikilink">貝拉四世冊封</a>）</p></td>
<td><p>1287年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/彼利捷斯達二世.md" title="wikilink">彼利捷斯達二世領主</a><br />
<small></small></p></td>
<td><p>1287年<br />
（與<strong>斯特凡一世·科特羅曼尼奇</strong>共同統治）</p></td>
<td><p>1290年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/斯特凡一世·科特羅曼尼奇.md" title="wikilink">斯特凡一世·科特羅曼尼奇領主</a><br />
<small></small></p></td>
<td><p>1287年<br />
（與<strong>彼利捷斯達二世</strong>共同統治）</p></td>
<td><p>1290年</p></td>
</tr>
<tr class="even">
<td><p>1290年</p></td>
<td><p>1314年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/斯特凡二世·科特羅曼尼奇.md" title="wikilink">斯特凡二世·科特羅曼尼奇領主</a><br />
<small></small></p></td>
<td><p>1322年<br />
（獲<a href="../Page/克羅地亞.md" title="wikilink">克羅地亞領主</a><a href="../Page/姆拉登二世.md" title="wikilink">姆拉登二世認可</a>）</p></td>
<td><p>1353年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/斯特凡·特夫爾特科一世·科特羅曼尼奇.md" title="wikilink">斯特凡·特夫爾特科一世·科特羅曼尼奇領主</a><br />
<small></small></p></td>
<td><p>1353年</p></td>
<td><p>1366年<br />
（被波斯尼亞貴族廢除）</p></td>
</tr>
<tr class="odd">
<td><p>1367年<br />
（在<a href="../Page/匈牙利.md" title="wikilink">匈牙利與</a><a href="../Page/杜布羅夫尼克.md" title="wikilink">杜布羅夫尼克協助下恢復王位</a>）</p></td>
<td><p>1377年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>國王<a href="../Page/斯特凡·特夫爾特科一世.md" title="wikilink">斯特凡·特夫爾特科一世</a><br />
<small></small></p></td>
<td><p>1377年<br />
（宣示成為塞爾維亞與波士尼亞等地的國王）</p></td>
<td><p>1391年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/斯特凡·沃克.md" title="wikilink">斯特凡·沃克領主</a><br />
<small></small></p></td>
<td><p>1366年<br />
（獲波斯尼亞貴族擁立）</p></td>
<td><p>1367年<br />
（失去政權）</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·達比沙.md" title="wikilink">斯特凡·達比沙</a><br />
<small></small></p></td>
<td><p>1391年</p></td>
<td><p>1395年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>女國王<a href="../Page/耶蓮娜·葛魯巴.md" title="wikilink">耶蓮娜·葛魯巴</a><br />
<small></small></p></td>
<td><p>1395年</p></td>
<td><p>1398年<br />
（被斯特凡·歐斯托亞所廢除）</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·歐斯托亞.md" title="wikilink">斯特凡·歐斯托亞</a><br />
<small></small></p></td>
<td><p>1398年<br />
（受<a href="../Page/克羅地亞.md" title="wikilink">克羅地亞</a><a href="../Page/赫雷沃耶·孚克赤奇.md" title="wikilink">赫雷沃耶·孚克赤奇公爵支持稱王</a>）</p></td>
<td><p>1404年<br />
（被赫雷沃耶·孚克赤奇所廢除）</p></td>
</tr>
<tr class="odd">
<td><p>1409年<br />
（受<a href="../Page/匈牙利.md" title="wikilink">匈牙利國王</a><a href="../Page/西吉斯蒙德.md" title="wikilink">西吉斯蒙德支持稱王</a>）</p></td>
<td><p>1418年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·特夫爾特科二世·科特羅曼尼奇.md" title="wikilink">斯特凡·特夫爾特科二世·科特羅曼尼奇</a><br />
<small></small></p></td>
<td><p>1404年<br />
（受赫雷沃耶·孚克赤奇所擁立）</p></td>
<td><p>1409年<br />
（被斯特凡·歐斯托亞所廢除）</p></td>
</tr>
<tr class="odd">
<td><p>1421年<br />
（重新繼承王位）</p></td>
<td><p>1443年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·歐斯托奕奇.md" title="wikilink">斯特凡·歐斯托奕奇</a><br />
<small></small></p></td>
<td><p>1418年</p></td>
<td><p>1421年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·托馬斯.md" title="wikilink">斯特凡·托馬斯</a><br />
<small></small></p></td>
<td><p>1443年</p></td>
<td><p>1461年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>國王<a href="../Page/斯特凡·托馬舍維奇.md" title="wikilink">斯特凡·托馬舍維奇</a><br />
<small></small></p></td>
<td><p>1461年</p></td>
<td><p>1463年<br />
（國家被<a href="../Page/奧斯曼帝國.md" title="wikilink">奧斯曼帝國佔領</a>，托馬舍維奇被斬首）</p></td>
</tr>
</tbody>
</table>

### [巴爾士奇王朝](../Page/巴爾士奇王朝.md "wikilink")

[CoatOfArmsOfTheBalsics.png](https://zh.wikipedia.org/wiki/File:CoatOfArmsOfTheBalsics.png "fig:CoatOfArmsOfTheBalsics.png")
**巴爾士奇王朝**（，[波斯尼亞語](../Page/波斯尼亞語.md "wikilink")、拉丁化：）為由**巴爾沙一世**（
）以降，統治**宅它**（）地區，後擴至[阿爾巴尼亞的王朝](../Page/阿爾巴尼亞.md "wikilink")。

<table style="width:81%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 31%" />
<col style="width: 18%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p>肖像</p></th>
<th><p>名稱</p></th>
<th><p>登位</p></th>
<th><p>退位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/巴爾沙一世.md" title="wikilink">巴爾沙一世領主</a><br />
<small></small></p></td>
<td><p>1356年<br />
（由<a href="../Page/尼曼雅王朝.md" title="wikilink">尼曼雅王朝的沙皇</a><a href="../Page/斯特凡·烏羅什四世.md" title="wikilink">斯特凡·烏羅什四世授權統治</a>）</p></td>
<td><p>1362年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/祖拉治一世.md" title="wikilink">祖拉治一世領主</a><br />
<small></small></p></td>
<td><p>1362年</p></td>
<td><p>1378年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/巴爾沙二世.md" title="wikilink">巴爾沙二世領主</a><br />
<small></small></p></td>
<td><p>1378年</p></td>
<td><p>1385年</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p><a href="../Page/祖拉治二世·斯特拉西米羅維奇.md" title="wikilink">祖拉治二世·斯特拉西米羅維奇领主</a><br />
<small></small></p></td>
<td><p>1385年</p></td>
<td><p>1403年</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p><a href="../Page/巴爾沙三世.md" title="wikilink">巴爾沙三世领主</a><br />
<small></small></p></td>
<td><p>1403年</p></td>
<td><p>1421年<br />
（被其舅舅<a href="../Page/斯特凡·拉扎列維奇.md" title="wikilink">斯特凡·拉扎列維奇篡位併吞</a>）</p></td>
</tr>
</tbody>
</table>

### [姆雷耶車維奇王朝](../Page/姆雷耶車維奇王朝.md "wikilink")

## 塞尔维亚专制公

### [拉扎列维奇王朝](../Page/拉扎列维奇王朝.md "wikilink")

  - [拉扎尔·赫雷别利亚诺维奇](../Page/拉扎尔·赫雷别利亚诺维奇.md "wikilink") 1371年 - 1389年
  - [斯特凡·拉扎列维奇](../Page/斯特凡·拉扎列维奇.md "wikilink") 1389年 - 1427年

### [布兰科维奇王朝](../Page/布兰科维奇王朝.md "wikilink")

  - [杜拉德·布兰科维奇](../Page/杜拉德·布兰科维奇.md "wikilink") 1427年 - 1456年
  - [拉扎尔·布兰科维奇](../Page/拉扎尔·布兰科维奇.md "wikilink") 1456年 - 1458年
  - [斯特凡·布兰科维奇](../Page/斯特凡·布兰科维奇.md "wikilink") 1458年 - 1459年

## 奥斯曼帝国统治时期

（1459年 - 1878年）

## 塞尔维亚大公

  - [卡拉乔治·彼得罗维奇](../Page/卡拉喬爾傑·彼得羅維奇.md "wikilink") 1804年 - 1813年
  - [米洛什·奥布雷诺维奇一世](../Page/米洛什·奥布雷诺维奇一世.md "wikilink") 1815年 - 1839年
  - [米兰·奥布雷诺维奇二世](../Page/米兰·奥布雷诺维奇二世.md "wikilink") 1839年
  - [米哈伊洛·奥布雷诺维奇三世](../Page/米哈伊洛·奥布雷诺维奇三世.md "wikilink") 1839年 - 1842年
  - [亚历山大·卡拉格奥尔基维奇](../Page/亚历山大_\(塞尔维亚大公\).md "wikilink") 1842年 - 1858年
  - [米洛什·奥布雷诺维奇一世](../Page/米洛什·奥布雷诺维奇一世.md "wikilink") 1858年 - 1860年
  - [米哈伊洛·奥布雷诺维奇三世](../Page/米哈伊洛·奥布雷诺维奇三世.md "wikilink") 1860年 - 1868年
  - [米兰·奥布雷诺维奇四世](../Page/米兰一世_\(塞尔维亚\).md "wikilink") 1868年 - 1882年

## 塞尔维亚国王

  - [米兰一世·奥布雷诺维奇](../Page/米兰一世_\(塞尔维亚\).md "wikilink") 1882年 - 1889年
  - [亚历山大·奥布雷诺维奇](../Page/亚历山大·奥布雷诺维奇.md "wikilink") 1889年 - 1903年
  - [彼得一世·卡拉格奥尔基维奇](../Page/彼得一世_\(塞尔维亚\).md "wikilink") 1903年 - 1918年

## 南斯拉夫国王

  - [亚历山大一世](../Page/亚历山大一世_\(南斯拉夫\).md "wikilink") 1918年 - 1934年
  - [彼得二世](../Page/彼得二世_\(南斯拉夫\).md "wikilink") 1934年 - 1945年

[category:塞尔维亚君主](../Page/category:塞尔维亚君主.md "wikilink")
[category:欧洲君主列表](../Page/category:欧洲君主列表.md "wikilink")