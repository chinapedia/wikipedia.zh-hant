**罗伯特·李·“胡特”·吉布森**（，），前[美國海軍](../Page/美國海軍.md "wikilink")[上校及](../Page/上校.md "wikilink")[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[STS-41-B](../Page/STS-41-B.md "wikilink")、[STS-61-C](../Page/STS-61-C.md "wikilink")、[STS-27](../Page/STS-27.md "wikilink")、[STS-47以及](../Page/STS-47.md "wikilink")[STS-71任务](../Page/STS-71.md "wikilink")。

## 外部链接

  - [美国国家航空航天局网站的吉布森介绍](http://www.jsc.nasa.gov/Bios/htmlbios/gibson.html)

[G](../Category/美國海軍上校.md "wikilink")
[G](../Category/第八组宇航员.md "wikilink")
[G](../Category/加州理工州立大學校友.md "wikilink")