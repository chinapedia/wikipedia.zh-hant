**生物科技工業園**(，簡稱**BIP**)是一種專注於[生物科技的](../Page/生物科技.md "wikilink")[工業園](../Page/工業園.md "wikilink")。通常為求合理的運用資源，生物科技工業園內的企業集合起來，可以發揮出[生物分餾的效益](../Page/生物分餾.md "wikilink")。生物分餾的作用，就好像[原油透過](../Page/原油.md "wikilink")[分層蒸餾的作用而產生出不同的產品出來](../Page/分層蒸餾.md "wikilink")。對於生物分餾技術來說，原材料（例如：[廢油或受污染的](../Page/廢油.md "wikilink")[泥土](../Page/泥土.md "wikilink")）在透過分餾的作用，重新成為多種可用於不同用途的產品。

[缩略图](https://zh.wikipedia.org/wiki/File:Biopolis-Singapore-20080712.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Biopolis_2,_Aug_06.JPG "fig:缩略图")

## [台灣方面](../Page/台灣.md "wikilink")

[台灣在生物科技的領域上](../Page/台灣.md "wikilink")，比[歐美](../Page/歐美.md "wikilink")、[日本起步晚了許多](../Page/日本.md "wikilink")，尚無自行研發製藥的優勢，目前在台灣亦只有協助發展的協會，如：[財團法人生物技術開發中心](../Page/財團法人生物技術開發中心.md "wikilink")(DCB)\[1\]
、[生物科技產業研究中心](../Page/生物科技產業研究中心.md "wikilink")\[2\]。

但於2007年[中研院向](../Page/中研院.md "wikilink")[行政院提出計畫案之後](../Page/行政院.md "wikilink")，於2010年歷經三次總算通過環境評估許可，中研院院長表示預計在在2014年完成基礎建設。因此將在[南港設立](../Page/南港區.md "wikilink")『[國家生技發展園區](../Page/國家生技發展園區.md "wikilink")』，此為發揮產業聚落效應，開發經費也由270億元降為160億元。[翁啟惠院長並解釋](../Page/翁啟惠.md "wikilink")，設立此機構的目的，是為銜接基礎研究與產業發展，與任何新藥的開發，由基礎研究轉為市場產品。這項園區設立之工程預計在2017年完工。\[3\]

## [新加坡方面](../Page/新加坡.md "wikilink")

為[新加坡科技研究局旗下的](../Page/新加坡科技研究局.md "wikilink")**生物醫藥科技園區**名為**啟奧城**([Biopolis](../Page/:en:Biopolis.md "wikilink"))\[4\]，於2003年10月29日正式開幕，而又在開幕沒幾年便網羅世界各地的菁英到這裡為新加坡效力，如[英國](../Page/英國.md "wikilink")[癌症基因專家大衛](../Page/癌症基因.md "wikilink")．連（Pro.
David Lane），則出任了『分子與細胞生物研究院』院長。

成功複製世界第一隻哺乳動物[多利羊的](../Page/多利.md "wikilink")[英國著名](../Page/英國.md "wikilink")[科學家科爾曼](../Page/科學家.md "wikilink")（Alan
Colman）也被網羅至此。目前他為[新加坡經濟發展局所投資的醫藥科技公司](../Page/新加坡經濟發展局.md "wikilink")**ES
Cell
International**工作，從事研發根治[糖尿病和](../Page/糖尿病.md "wikilink")[心臟病的藥物](../Page/心臟病.md "wikilink")。

除此之外，更吸引了許多國際知名藥廠來這設廠，如[英國的GlaxoSmithKline](../Page/英國.md "wikilink")
、[瑞士藥廠](../Page/瑞士.md "wikilink")
Novartis等。GSK藥廠則投資6千2百萬[新加坡元在新加坡設立一個](../Page/新加坡元.md "wikilink")**R\&D中心**。Novartis則設立『熱帶疾病研究院』，從事研發治療[骨痛熱病和](../Page/骨痛熱病.md "wikilink")[肺癆病的新藥](../Page/肺癆病.md "wikilink")。\[5\]\[6\]

## 參考

<div style="font-size: 85%">

<references />

</div>

[Category:生物工程](../Category/生物工程.md "wikilink")
[Category:工业](../Category/工业.md "wikilink")

1.
2.
3.
4.
5.
6.