**远朋班**，全稱**远朋国建班**，后來更名为**国家建设班**，為[中華民國于](../Page/中華民國.md "wikilink")1971年设立的軍事訓練機構。成立原由為[冷战期間](../Page/冷战.md "wikilink")[中華民國政府为了](../Page/中華民國政府.md "wikilink")[反共救國](../Page/反共.md "wikilink")，特别对[亞洲](../Page/亞洲.md "wikilink")、[非洲和](../Page/非洲.md "wikilink")[拉丁美洲国家培养的初级军事人才](../Page/拉丁美洲.md "wikilink")。

## 沿革

早年遠朋班祕密設置在[臺北市](../Page/臺北市.md "wikilink")[北投區](../Page/北投區.md "wikilink")[復興崗政工幹部學校](../Page/復興崗.md "wikilink")（今[國防大學](../Page/國防大學_\(中華民國\).md "wikilink")[政治作戰學院](../Page/政治作戰學院.md "wikilink")）內，班址與學生活動區區隔開，數十年來已經有數千人接受過訓練，結訓後大都在[拉丁美洲位居要職](../Page/拉丁美洲.md "wikilink")，或是擔任軍事指揮官。後來遠朋班轉型，學員擴及[中東](../Page/中東.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[加勒比海等地](../Page/加勒比海.md "wikilink")，由原先[反共](../Page/反共.md "wikilink")、反滲透，轉型為政經課程。

## 知名学员

  - [曼紐·諾瑞嘉](../Page/曼纽尔·诺列加.md "wikilink")

## 參考資料

<div class="references-small">

<references />

  -
  -
  -

</div>

## 参见

  - [西半球安全合作学院](../Page/西半球安全合作学院.md "wikilink")

## 外部链接

  - [單位簡介 - 遠朋班](http://ndc.ndu.edu.tw/files/11-1017-83.php?Lang=zh-tw)

[Category:中華民國外交](../Category/中華民國外交.md "wikilink")
[Category:政治作戰學院](../Category/政治作戰學院.md "wikilink")
[Category:冷战](../Category/冷战.md "wikilink")
[Category:中國反共主義](../Category/中國反共主義.md "wikilink")
[Category:中華民國軍事訓練](../Category/中華民國軍事訓練.md "wikilink")
[Category:1964年建立](../Category/1964年建立.md "wikilink")