**栃木縣**\[1\]（）是位於[日本](../Page/日本.md "wikilink")[關東地方北部的一個內陸](../Page/關東地方.md "wikilink")[縣](../Page/縣.md "wikilink")。東面連接[茨城縣](../Page/茨城縣.md "wikilink")、西面為[群馬縣](../Page/群馬縣.md "wikilink")、南面為[埼玉縣](../Page/埼玉縣.md "wikilink")、北面則接鄰[福島縣](../Page/福島縣.md "wikilink")。縣政府所在地為[宇都宮市](../Page/宇都宮市.md "wikilink")。「栃」在日語的意思即是[日本七葉樹](../Page/日本七叶树.md "wikilink")。

栃木縣與[群馬縣的地域在古代為](../Page/群馬縣.md "wikilink")「[毛野國](../Page/毛野國.md "wikilink")（）」的一部份。[仁德天皇時](../Page/仁德天皇.md "wikilink")，以地理上的上下分為「（）」和「（）」。此兩國於[奈良時代改稱](../Page/奈良時代.md "wikilink")「[上野國](../Page/上野國.md "wikilink")（，即現時群馬縣）」、「[下野國](../Page/下野國.md "wikilink")（）」。

## 地理

栃木縣位於關東地方北部，面積約6,408.28[平方公里](../Page/平方公里.md "wikilink")，在日本全國排名第20位，是關東最大的一個縣，2012年總人口
1,994,199人。栃木縣東西距離約84公里、南北距離約98公里。縣都[宇都宮市距離](../Page/宇都宮市.md "wikilink")[東京約](../Page/東京.md "wikilink")100公里、從[東京站乘搭](../Page/東京站.md "wikilink")[東北新幹線至宇都宮市約需](../Page/東北新幹線.md "wikilink")50分鐘、也是北關東最高人口地區。縣內的[日光市為日本著名的觀光勝地](../Page/日光市.md "wikilink")。

## 行政区划

  - [宇都宫市](../Page/宇都宫市.md "wikilink")（县厅所在地、[中核市](../Page/中核市.md "wikilink")）
  - [足利市](../Page/足利市.md "wikilink")
  - [栃木市](../Page/栃木市.md "wikilink")
  - [佐野市](../Page/佐野市.md "wikilink")
  - [鹿沼市](../Page/鹿沼市.md "wikilink")
  - [日光市](../Page/日光市.md "wikilink")
  - [小山市](../Page/小山市.md "wikilink")
  - [真冈市](../Page/真岡市.md "wikilink")
  - [大田原市](../Page/大田原市.md "wikilink")
  - [矢板市](../Page/矢板市.md "wikilink")
  - [那须盐原市](../Page/那須鹽原市.md "wikilink")
  - [樱市](../Page/櫻市.md "wikilink")
  - [那须乌山市](../Page/那須烏山市.md "wikilink")

<!-- end list -->

  - [下野市](../Page/下野市.md "wikilink")

<!-- end list -->

  - [河内郡](../Page/河內郡_\(栃木縣\).md "wikilink")
      - [上三川町](../Page/上三川町.md "wikilink")

<!-- end list -->

  - [芳贺郡](../Page/芳贺郡.md "wikilink")
      - [益子町](../Page/益子町.md "wikilink")
      - [茂木町](../Page/茂木町.md "wikilink")
      - [市贝町](../Page/市貝町.md "wikilink")
      - [芳贺町](../Page/芳賀町.md "wikilink")

<!-- end list -->

  - [下都贺郡](../Page/下都賀郡.md "wikilink")
      - [壬生町](../Page/壬生町.md "wikilink")
      - [野木町](../Page/野木町.md "wikilink")

<!-- end list -->

  - [盐谷郡](../Page/鹽谷郡.md "wikilink")
      - [盐谷町](../Page/鹽谷町.md "wikilink")
      - [高根泽町](../Page/高根澤町.md "wikilink")
  - [那须郡](../Page/那須郡.md "wikilink")
      - [那须町](../Page/那須町.md "wikilink")
      - [那珂川町](../Page/那珂川町.md "wikilink")

## 著名地點

  - [日光的神社與寺院](../Page/日光的神社與寺院.md "wikilink")（[聯合國世界遺產](../Page/世界遺產.md "wikilink")）
      - [日光東照宮](../Page/日光東照宮.md "wikilink")
      - [輪王寺](../Page/輪王寺.md "wikilink")
      - [日光二荒山神社](../Page/日光二荒山神社.md "wikilink")
  - [帝釋山地](../Page/帝釋山地.md "wikilink")
  - [足尾山地](../Page/足尾山地.md "wikilink")
  - [那珂川](../Page/那珂川.md "wikilink")
  - [鬼怒川](../Page/鬼怒川.md "wikilink")
  - [渡良瀬川](../Page/渡良瀬川.md "wikilink")
  - [日光國立公園](../Page/日光國立公園.md "wikilink")

<File:Tochigi> prefectural Library.jpg|栃木縣立圖書館 <File:Utsunomiya>
museum.jpg|栃木日本藝術博物館 <File:Tochigi> Kuranomachi Art Holl.JPG|藏街美術館
<File:Stone> Plaza.jpg|石頭美術館 <File:Shark> meat in supermarket.jpg|栃木煮魚
[File:Tobu-kuzuu-platform-at-night.jpg|葛生車站](File:Tobu-kuzuu-platform-at-night.jpg%7C葛生車站)
EXPO84_TOCHIGI.jpg|1984栃木博 <File:Utsunomiya> Air Field Aerial
Photograph.jpg|宇都宮機場 <File:Flag> of JSDF(20070408).jpg|宇都宮陸軍基地
[File:Mimi-udon.jpg|栃木麵耳](File:Mimi-udon.jpg%7C栃木麵耳)

## 特產

<table>
<thead>
<tr class="header">
<th><p>水果、蔬菜</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/草莓.md" title="wikilink">草莓</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乾瓢.md" title="wikilink">乾瓢</a></p></td>
</tr>
<tr class="odd">
<td><p>魚類、海產</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香魚.md" title="wikilink">香魚</a></p></td>
</tr>
<tr class="odd">
<td><p>肉類、乳製品</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/和牛.md" title="wikilink">和牛</a></p></td>
</tr>
<tr class="odd">
<td><p>鄉土料理</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐野拉麵.md" title="wikilink">佐野拉麵</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/餃子.md" title="wikilink">餃子</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蕎麥麵.md" title="wikilink">蕎麥麵</a></p></td>
</tr>
<tr class="even">
<td><p>工藝品、民藝品</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/益子燒.md" title="wikilink">益子燒</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/結城紬.md" title="wikilink">結城紬</a></p></td>
</tr>
<tr class="odd">
<td><p>參考資料：[2]</p></td>
</tr>
</tbody>
</table>

## 出身名人

## 註釋

## 相關條目

  - [兩毛](../Page/兩毛.md "wikilink")
  - [首都機能移轉](../Page/首都機能移轉.md "wikilink")

## 外部連結

  - [栃木縣官方網站](http://www.pref.tochigi.lg.jp/)
  - [日文「栃木（tochigi）」，中文怎唸？](http://blog.sina.com.tw/rayi/article.php?pbgid=1894&entryid=1579)
  - [栃木縣观光物产协会](https://travel.tochigiji.or.jp/zh_CN/)
  - [栃木观光](https://web.archive.org/web/20120817225449/http://abc0120.com/wiki/category-view-120.html)

[L栃](../Category/日本都道府縣.md "wikilink")
[\*](../Category/栃木縣.md "wikilink")

1.  普遍認為「」是[和製漢字](../Page/和製漢字.md "wikilink")，故沒有對應中文字。有研究指中文「櫔」與日文「」指同一類樹木，若將「-{萬}-」簡化為「-{万}-」，則兩字形體便幾乎相同，雖然「」右上方是一撇，「櫔」右上方是一橫，但「櫔」在字形和字義上還是較接近「」。不過仍沒有證據證明「」源自「櫔」字。不少人追溯「」的字源時張冠李戴，曲解「」的字形字義，誤以為「」的對應中文字是「槴」、「梔」、「朸」、「枋」、「櫪」，這些都不是正確用法。
2.