**尚義县**在[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河北省西北部](../Page/河北省.md "wikilink")、[洋河上游](../Page/洋河.md "wikilink")，[外长城以北](../Page/长城.md "wikilink")，是[张家口市下辖的一个县](../Page/张家口市.md "wikilink")。位于河北省张家口市西部，邻接[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")。

1934年由[商都县析置尚义设治局](../Page/商都县.md "wikilink")，1936年改为尚义县。农产品以[莜麦](../Page/莜麦.md "wikilink")、[小麦](../Page/小麦.md "wikilink")、[马铃薯](../Page/马铃薯.md "wikilink")、[胡麻为主](../Page/胡麻.md "wikilink")。[草原广阔](../Page/草原.md "wikilink")，畜牧业发达。[工业有煤炭](../Page/工业.md "wikilink")、机械、[造纸](../Page/造纸.md "wikilink")、皮毛加工、建材等。[古迹有明长城](../Page/古迹.md "wikilink")。

## 气候

年平均气温只有3.5℃，属寒温带大陆性季风气候。

## 行政区划

下辖7个[镇](../Page/行政建制镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [河北省尚义县政府网](http://www.shangyi.gov.cn/)
  - [中国·张家口尚义](http://www.zjksy.gov.cn/)

[尚义县](../Page/category:尚义县.md "wikilink")
[县](../Page/category:张家口区县.md "wikilink")
[张家口](../Page/category:河北省县份.md "wikilink")
[冀](../Page/category:国家级贫困县.md "wikilink")