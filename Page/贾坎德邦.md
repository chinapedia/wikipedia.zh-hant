**贾坎德邦**（，，[拉丁字母转写](../Page/拉丁字母.md "wikilink")：）是[印度第](../Page/印度.md "wikilink")28个邦，2000年11月15日从[比哈尔邦分离出来建立](../Page/比哈尔邦.md "wikilink")，意为“林地”。邦官方语言为印地语，[首府是](../Page/首府.md "wikilink")[兰契](../Page/兰契.md "wikilink")，为该邦的一个工业城市。贾坎德邦境内的其他主要城市还包括：[博卡罗](../Page/博卡罗.md "wikilink")、[但巴德以及](../Page/但巴德.md "wikilink")[詹谢普尔](../Page/詹谢普尔.md "wikilink")，其中，但巴德为印度的煤都，人口亦是该邦中最多。另外，[迪奥加尔为该邦的宗教中心](../Page/迪奥加尔县.md "wikilink")。

## 历史

该邦是2000年11月15日建立的三个新邦之一，另外两个邦是[恰蒂斯加尔邦及](../Page/恰蒂斯加尔邦.md "wikilink")[北阿坎德邦](../Page/北阿坎德邦.md "wikilink")。改邦自[比哈尔邦分离出来而建立](../Page/比哈尔邦.md "wikilink")。

## 行政区划

本邦共分为24個**县**（****），分別为：

| 县名                                           | 天城体                   | 拉丁字母转译               |
| -------------------------------------------- | --------------------- | -------------------- |
| [蘭契縣](../Page/蘭契縣.md "wikilink")             | राँची जिला            | Ranchi               |
| [根拉縣](../Page/根拉縣.md "wikilink")             | गुमला जिला            | Gumla                |
| [洛哈达伽县](../Page/洛哈达伽县.md "wikilink")         | लोहरदग्गा जिला        | Lohardaga            |
| [辛德伽縣](../Page/辛德伽縣.md "wikilink")           | सिमडेगा जिला          | Simdega              |
| [帕拉木縣](../Page/帕拉木縣.md "wikilink")           | पलामू जिला            | Palamu               |
| [拉特哈尔县](../Page/拉特哈尔县.md "wikilink")         | लातेहार जिला          | Latehar              |
| [加瓦縣](../Page/加瓦縣.md "wikilink")             | गढवा जिला             | Garhwa               |
| [西新奔縣](../Page/西新奔縣.md "wikilink")           | पश्चिमी सिंहभूम जिला  | West Singhbhum       |
| [薩萊克拉卡爾薩旺縣](../Page/薩萊克拉卡爾薩旺縣.md "wikilink") | सराइकेला खरसावाँ जिला | Seraikella Kharsawan |
| [东新奔县](../Page/东新奔县.md "wikilink")           | पूर्वी सिंहभूम जिला   | East Singhbhum       |
| [敦卡縣](../Page/敦卡縣.md "wikilink")             | दुमका जिला            | Dumka                |
| [詹塔拉縣](../Page/詹塔拉縣.md "wikilink")           | जामताड़ा जिला         | Jamtara              |
| [赛义布甘杰县](../Page/赛义布甘杰县.md "wikilink")       | साहिबगंज जिला         | Sahibganj            |
| [帕考縣](../Page/帕考縣.md "wikilink")             | पाकुड़ जिला           | Pakur                |
| [高达县](../Page/高达县.md "wikilink")             | गोड्डा जिला           | Godda                |
| [哈扎里巴縣](../Page/哈扎里巴縣.md "wikilink")         | हजारीबाग जिला         | Hazaribagh           |
| [查特拉縣](../Page/查特拉縣.md "wikilink")           | चतरा जिला             | Chatra               |
| [科达马县](../Page/科达马县.md "wikilink")           | कोडरमा जिला           | Koderma              |
| [基里迪縣](../Page/基里迪縣.md "wikilink")           | गिरीडीह जिला          | Giridih              |
| [但巴德縣](../Page/但巴德縣.md "wikilink")           | धनबाद जिला            | Dhanbad              |
| [博卡罗县](../Page/博卡罗县.md "wikilink")           | बोकारो जिला           | Bokaro               |
| [迪奥加尔县](../Page/迪奥加尔县.md "wikilink")         | देवघर जिला            | Deoghar              |
| [昆蒂縣](../Page/昆蒂縣.md "wikilink")             |                       | Khunti               |
| [拉姆格爾縣](../Page/拉姆格爾縣.md "wikilink")         |                       | Ramgarh              |

## 人口

根據2001年人口普查，[印度教徒佔全邦人口的](../Page/印度教徒.md "wikilink")68.57%，[穆斯林佔](../Page/穆斯林.md "wikilink")14.5%，基督教徒佔4.5%。\[1\]\[2\]

## 语言

该邦官方语言为[印地语及](../Page/印地语.md "wikilink")[乌尔都语](../Page/乌尔都语.md "wikilink")\[3\]。其他主要语言包括：[孟加拉语](../Page/孟加拉语.md "wikilink")、[桑塔利语](../Page/桑塔利语.md "wikilink")、[奥里亚语](../Page/奥里亚语.md "wikilink")、[迈蒂利语](../Page/迈蒂利语.md "wikilink")、[库鲁克语及](../Page/库鲁克语.md "wikilink")[霍语等](../Page/霍语.md "wikilink")\[4\]。

## 參考文獻

## 外部連結

  - [贾坎德邦官方网页](https://web.archive.org/web/20090513031444/http://jharkhand.nic.in/)

[Category:贾坎德邦](../Category/贾坎德邦.md "wikilink")
[邦](../Category/印度的邦和中央直辖区.md "wikilink")
[Category:2000年印度建立](../Category/2000年印度建立.md "wikilink")

1.  [Indian Census 2001 –
    Religion](http://demotemp257.nic.in/httpdoc/Census_Data_2001/Census_data_finder/C_Series/Population_by_religious_communities.htm)

2.  <http://timesofindia.indiatimes.com/india/Kerala-not-Goa-has-maximum-no-of-Christians/articleshow/2649158.cms>
3.  [Report of the Commissioner for linguistic minorities: 50th report
    (July 2012 to
    June 2013)](http://nclm.nic.in/shared/linkimages/NCLM50thReport.pdf)
     Commissioner for Linguistic Minorities, Ministry of Minority
    Affairs, Government of India
4.  [1](http://www.bihardays.com/jharkhands-11-second-languages-will-create-new-jobs-enrich-national-culture/)