{{ infobox nobility | name = 安內·德·蒙莫朗西 | image =
Clouet-montmorencyanne.jpg | caption = 蒙莫朗西公爵安內 | noble family =
[蒙莫朗西家族](../Page/蒙莫朗西家族.md "wikilink") | father = 紀堯姆·德·蒙莫朗西
| mother = 安妮·波特 | spouse = 薩伏依的瑪德蓮 | birth_date =  | birth_place =
[夏特雷](../Page/夏特雷.md "wikilink") | death_date =  | death_place =
[巴黎](../Page/巴黎.md "wikilink") }}
**安內·德·蒙莫朗西，第一代蒙莫朗西公爵**（；1493年3月15日\~1567年11月12日）[法国军人和政治家](../Page/法国.md "wikilink")，[弗朗索瓦一世](../Page/弗朗索瓦一世.md "wikilink")、[亨利二世和](../Page/亨利二世_\(法兰西\).md "wikilink")[查理九世三朝身經百戰的重臣](../Page/查理九世_\(法兰西\).md "wikilink")。1551年起，他的封号是[蒙莫朗西公爵](../Page/蒙莫朗西公爵.md "wikilink")。

蒙莫朗西出生後，由當權的王室公主─[法蘭西的安妮擔任](../Page/法蘭西的安妮.md "wikilink")[教母](../Page/教母.md "wikilink")，安妮把自己的名字──Anne，給了這個初生男嬰。蒙莫朗西與[弗朗索瓦一世一起長大](../Page/弗朗索瓦一世.md "wikilink")，弗朗索瓦一世1515年即王位後，开始受到重用。他参加了1515年发生于義大利[马里尼亚诺附近的战役和](../Page/马里尼亚诺.md "wikilink")[梅济耶尔防卫战](../Page/梅济耶尔.md "wikilink")，表现突出；1522年，他晉升為[法国元帅](../Page/法国元帅.md "wikilink")。1524年保衛普羅旺斯，後又回義大利作戰。1525年，他与弗朗索瓦一世一起在[帕维亚战役中被](../Page/帕维亚战役.md "wikilink")[西班牙军队俘虏](../Page/西班牙.md "wikilink")，獲釋後參加[馬德里條约的签订](../Page/馬德里.md "wikilink")。1526年任[郎格罗克总督和王家管家](../Page/郎格罗克.md "wikilink")（Grand
Master of
France），帶來巨額收入；他又奉命从事外交工作，后成为国王的首席大臣，主管国防、王室、公共工程和外交事务。1527年他使英格兰加入[科尼亚克联盟](../Page/科尼亚克联盟.md "wikilink")，反对[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[卡尔五世](../Page/卡尔五世.md "wikilink")，但1529年又参与议定法国与卡尔五世之间的[康布雷和约](../Page/康布雷和约.md "wikilink")。1536年再次与卡尔五世作战，先是在[普罗旺斯](../Page/普罗旺斯.md "wikilink")，后在意大利北部。1538年议和，被任命为法国王室統帥（Grand
Constable of
France），幾乎主導了法國的內外政策，也標誌着国王对他的宠信达到了顶峰（身兼最高二職─王室統帥與王室管家）。但不久弗朗索瓦一世怀疑蒙莫朗西与王太子结党，从1541年起禁止他进入宫廷。

1547年[亨利二世继承法国王位后](../Page/亨利二世_\(法兰西\).md "wikilink")，蒙莫朗西得以官复原职，并作为国王最信任的顾问之一领导政府。1548年，他率领军队镇压[阿基坦](../Page/阿基坦.md "wikilink")（尤其是[波尔多](../Page/波尔多.md "wikilink")）居民反抗[盐税的起义](../Page/法國鹽稅.md "wikilink")。1551年被封为**蒙莫朗西公爵**。在1553年的佛蘭德爾戰役中，他在軍事上表現出十足的無能；1557年，蒙莫朗西在[圣康坦战役中指挥法国军队](../Page/圣康坦战役.md "wikilink")，结果被西班牙军队打得大败，本人再次被俘，1559年法西之间签订《[卡托-康布雷齊和約](../Page/卡托-康布雷齊和約.md "wikilink")》后获释。

1559年中法王亨利二世過世後，蒙莫朗西的權勢被[吉斯公爵重重削弱](../Page/吉斯公爵_\(第二\).md "wikilink")，當時吉斯迅速掌握了宮廷，強逼蒙莫朗西放棄王室管家的顯職與其巨額收入（改由吉斯擔任王室管家），只換來長子弗朗索瓦獲得[法國元帥的職位交易](../Page/法國元帥.md "wikilink")，而蒙莫朗西則保住了王室統帥的職位。

在[法国宗教战争爆发后](../Page/法国宗教战争.md "wikilink")，蒙莫朗西作为[天主教势力的主要代表和将领](../Page/天主教.md "wikilink")，对[胡格诺派表现出不妥协的态度](../Page/胡格诺派.md "wikilink")。在[查理九世统治时期](../Page/查理九世_\(法兰西\).md "wikilink")，他重新掌权，与[吉斯公爵弗朗索瓦和](../Page/吉斯公爵_\(第二\).md "wikilink")[圣安德烈元帅组织三巨头执政](../Page/雅克·德伯恩·德·圣安德烈.md "wikilink")，实际上将国王变成傀儡。1562年他在[德勒战役中指挥王室军队打败胡格诺派](../Page/德勒战役.md "wikilink")，但旋即第三次被敌人俘虏，1563年获释，同年蒙莫朗西指挥军队将[英国军队逐出](../Page/英国.md "wikilink")[勒阿弗尔](../Page/勒阿弗尔.md "wikilink")，重新夺回勒阿弗尔。

1567年与胡格诺派的战争重新爆发，蒙莫朗西在巴黎近郊[圣但尼战役中指挥军队与第一代](../Page/圣但尼战役.md "wikilink")[孔代亲王](../Page/孔代亲王.md "wikilink")[路易一世·德·波旁率领的胡格诺派军队激烈交战](../Page/孔代亲王_\(第一\).md "wikilink")。他在此役中受致命伤两天后去世。

## 子女

1.  長子[弗朗索瓦·德·蒙莫朗西](../Page/蒙莫朗西公爵_\(第二\).md "wikilink")
2.  次子[亨利一世·德·蒙莫朗西](../Page/蒙莫朗西公爵_\(第三\).md "wikilink")
3.  長女伊莉諾（1557年卒），嫁給「La Tour
    d'Auvergne家族」的弗朗索瓦，生下[布永公爵](../Page/布永公爵.md "wikilink")[亨利·德拉圖爾·多韋涅](../Page/亨利·德拉圖爾·多韋涅.md "wikilink")（[胡格諾派](../Page/胡格諾派.md "wikilink")），亨利的次子為法國在17世紀的第一名將、天才[元帥](../Page/法國元帥.md "wikilink")[蒂雷納子爵](../Page/蒂雷納子爵.md "wikilink")。

## 注釋

<references />

[Category:蒙莫朗西公爵](../Category/蒙莫朗西公爵.md "wikilink")
[Category:法国元帅](../Category/法国元帅.md "wikilink")
[M](../Category/1493年出生.md "wikilink")
[M](../Category/1567年逝世.md "wikilink")