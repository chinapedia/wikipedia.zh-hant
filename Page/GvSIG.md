**gvSIG**
是一个处理地理信息的应用软件。该系统不仅能够访问本地的矢量数据或者栅格数据，而且也能够通过支持[开放空间协会](../Page/开放空间协会.md "wikilink")（OGC）规范的远程服务器访问该类数据。支持网络地图服务（WMS）、网络要素服务（WFS）、网络覆盖服务（WCS）、目录服务以及地名辞典服务等一系列地理信息网络服务规范是gvSIG与其它[地理信息系统的特大区别](../Page/地理信息系统.md "wikilink")。

## 标准

gvSIG 能够支持以下数据格式：

  - 矢量格式：[Shapefile](../Page/Shapefile.md "wikilink")，[DXF](../Page/DXF.md "wikilink")，[DGN](../Page/DGN.md "wikilink")，[DWG](../Page/AutoCAD_DWG.md "wikilink")
  - 栅格格式：[ECW](../Page/ECW_\(文件格式\).md "wikilink")，[MrSID](../Page/MrSID.md "wikilink")，[JPEG](../Page/JPEG.md "wikilink")，[jp2](../Page/JPEG_2000.md "wikilink")，[TIFF](../Page/TIFF.md "wikilink")，[geoTIFF](../Page/geoTIFF.md "wikilink")，[PNG](../Page/PNG.md "wikilink")，[GIF](../Page/GIF.md "wikilink")
  - 基于XML的格式：[KML](../Page/KML.md "wikilink")，[GML](../Page/GML.md "wikilink")

gvSIG能够访问支持[Open Geospatial
Consortium](../Page/Open_Geospatial_Consortium.md "wikilink")（开放空间协会）标准的远程数据：

  - [Web Map Service](../Page/Web_Map_Service.md "wikilink")（网络地图服务）
  - [Web Feature
    Service](../Page/Web_Feature_Service.md "wikilink")（网络要素服务）
  - [Web Coverage
    Service](../Page/Web_Coverage_Service.md "wikilink")（网络覆盖服务）

gvSIG也能访问[ESRI公司的](../Page/ESRI.md "wikilink")[ArcIMS服务提供的远程数据](../Page/ArcIMS.md "wikilink")。

[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")
[Category:GIS软件](../Category/GIS软件.md "wikilink")