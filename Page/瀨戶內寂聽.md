**瀨戶內寂聽**（，）為[日本的](../Page/日本.md "wikilink")[小說家](../Page/小說家.md "wikilink")，也是一[佛教](../Page/佛教.md "wikilink")[天台宗的](../Page/天台宗.md "wikilink")[僧侶](../Page/僧侶.md "wikilink")\[1\]，[俗名](../Page/俗名.md "wikilink")**瀨戶內晴美**（）。她現為[大僧正](../Page/大僧正.md "wikilink")（有如[基督教的](../Page/基督教.md "wikilink")[大主教或](../Page/大主教.md "wikilink")[樞機主教](../Page/樞機主教.md "wikilink")）。她亦是日本的文化功勞者。[德島縣立高等女學校](../Page/德島縣立高等女學校.md "wikilink")、[東京女子大學國語專攻部畢業](../Page/東京女子大學.md "wikilink")。她的最終[學位為](../Page/學位.md "wikilink")[文學士](../Page/文學士.md "wikilink")（東京女子大學）。[德島縣德島市名譽市民](../Page/德島縣.md "wikilink")。前[天台寺](../Page/天台寺.md "wikilink")[寺主](../Page/寺主.md "wikilink")。前[敦賀短期大學](../Page/敦賀短期大學.md "wikilink")[校長](../Page/校長.md "wikilink")。

其代表作有《夏之殘戀》（）、《場所》（）、《白話文譯　[源氏物語](../Page/源氏物語.md "wikilink")》（）等。近年增加了與《[源氏物語](../Page/源氏物語.md "wikilink")》相關的著作。迄今為止，其著作贏得了許多的文學獎。

## 生平

生於[日本](../Page/日本.md "wikilink")[德島縣](../Page/德島縣.md "wikilink")[德島市的經營](../Page/德島市.md "wikilink")[佛壇店的瀨戶內家](../Page/佛壇.md "wikilink")。

在就讀[東京女子大學時結婚](../Page/東京女子大學.md "wikilink")，但與丈夫的學生相戀，留下丈夫和長女離家。正式[離婚後](../Page/離婚.md "wikilink")，到[東京認真地朝向](../Page/東京.md "wikilink")[小說家之路邁進](../Page/小說家.md "wikilink")。過了幾年後她長女和解了。

1956年（[昭和](../Page/昭和.md "wikilink")31年）發表了處女作《女大學生・曲愛玲》（）並獲得新潮同人雜誌獎成功進入小說行業。但是，後來發表的《花芯》被人批評為[官能小說](../Page/官能小說.md "wikilink")，被稱作「子宮作家」。1963年（昭和38年），她因發表了小說《夏日終焉》（）獲得了[女流文學獎](../Page/女流文學獎.md "wikilink")，確立了她做為作家的地位。又，於1992年（平成4年）《問花》（）獲得了文壇裡評價十分高的[谷崎潤一郎獎](../Page/谷崎潤一郎獎.md "wikilink")。《[源氏物語](../Page/源氏物語.md "wikilink")》的現代日語譯也讓她聲名遠播。於2005年（平成17年）於日本播出了以她為主角的特別[電視劇](../Page/電視劇.md "wikilink")─[女人一代記](../Page/女人一代記.md "wikilink")。

她起初想成為[天主教會的](../Page/天主教.md "wikilink")[修女](../Page/修女.md "wikilink")，但由於她過去的行為舉止被天主教教會拒絕。後來由[今東光](../Page/今東光.md "wikilink")[和尚的引導](../Page/和尚.md "wikilink")，受[天台宗的幫助](../Page/天台宗.md "wikilink")，於1973年[出家](../Page/出家.md "wikilink")。她做為一名[尼姑也十分熱忱的活躍著](../Page/尼姑.md "wikilink")，在每週末說法話**青空說法**（天台寺說法）。

如同在[女人一代記裡描述的一樣](../Page/女人一代記.md "wikilink")，現實中的她到了東京後曾和好幾位男性發生關係，並且瀨戶內本人也說過去的自己太過沉溺於男色。她在[女人一代記播放前](../Page/女人一代記.md "wikilink")，於母校公開[演講](../Page/演講.md "wikilink")，說明了在劇中闡述的僅為事實的一小部分而已，其實她和更多的男性發生關係。當了尼姑後，她斷了所有男性關係，並認真的過著信仰生活。她曾大力幫助吸麻上癮而被逮捕的[萩原健一復健](../Page/萩原健一.md "wikilink")。和[永山則夫是摯友](../Page/永山則夫.md "wikilink")，於2016年10月參加日本律師聯合會所舉辦之死刑廢止研討會中，不但透過影片批評死刑制度，且作出「請與這些只想著殺戮的笨蛋們戰鬥吧\!」的發言，此舉被認為是對於受害人家屬的侮辱，因而遭到許多被害者協會成員、受害人家屬及社會猛烈批判，自此被視為[死刑廢止論者](../Page/死刑廢止.md "wikilink")。她反對[九一一恐怖攻擊事件的報復攻擊](../Page/九一一恐怖攻擊事件.md "wikilink")，並[絕食](../Page/絕食.md "wikilink")[抗議](../Page/抗議.md "wikilink")。

### 年譜

  - 1953年　《女大學生・曲愛玲》（）　[新潮社同人雜誌獎](../Page/新潮社同人雜誌獎.md "wikilink")
  - 1961年　《田村俊子》　田村俊子獎
  - 1963年　《夏之殘戀》（）　[女流文學獎](../Page/女流文學獎.md "wikilink")
  - 1973年　於[岩手縣](../Page/岩手縣.md "wikilink")[平泉町的](../Page/平泉町.md "wikilink")[中尊寺出家](../Page/中尊寺.md "wikilink")，成為尼姑，[法名為](../Page/法名.md "wikilink")**瀨戶內寂聽**
  - 1987年　就任[天台寺寺主](../Page/天台寺.md "wikilink")
  - 1988年　就任敦賀女子短期大學[校長](../Page/校長.md "wikilink")
  - 1992年　《問花》（）獲[谷崎潤一郎獎](../Page/谷崎潤一郎獎.md "wikilink")，辭去敦賀女子短期大學校長一職
  - 1995年　藝術選獎[文部大臣獎](../Page/文部大臣獎.md "wikilink")（文學部門）※因小說《白道》的成果
  - 1997年　被選為[文化功勞者](../Page/文化功勞者.md "wikilink")
  - 1998年　獲[NHK放送文化獎](../Page/NHK放送文化獎.md "wikilink")。《白話文譯　源氏物語》（）全20本完結
  - 2000年　德島縣德島市名譽市民
  - 2001年　因《場所》一書獲[野間文藝獎](../Page/野間文藝獎.md "wikilink")
  - 2002年　德島書道文學館竣工，館內設置了瀨戶內寂聽紀念室。同時因母校[德島縣立高等女學校](../Page/德島縣立城東高等學校.md "wikilink")（現為縣立城東高等學校）迎100週年，她表明將全面支持協助《致君夢100萬圓》（）的獎學金制度
  - 2005年6月　自天台寺的寺主退隱
  - 2006年
      - 2月　撰寫歌劇《愛怨》的劇本
      - 3月　為第73回高等學校部的課題曲《在一個深夜裡》（）作詞（合唱曲。作曲者為[千原英喜](../Page/千原英喜.md "wikilink")。）
      - 11月 獲頒[文化勳章](../Page/文化勳章.md "wikilink")
      - 12月31日 NHK [紅白歌合戰特別評審委員](../Page/紅白歌合戰.md "wikilink")
  - 2007年3月
    就任[延曆寺直轄寺院](../Page/延曆寺.md "wikilink")「[禪光坊](../Page/禪光坊.md "wikilink")」之住持
  - 2008年
    獲[新潟市頒贈](../Page/新潟市.md "wikilink")[安吾獎](../Page/安吾獎.md "wikilink")
  - 2011年 以作品《風景》獲頒[泉鏡花文學獎](../Page/泉鏡花文學獎.md "wikilink")
  - 2018年
    獲頒[朝日獎](../Page/朝日獎.md "wikilink")，[俳句集](../Page/俳句.md "wikilink")《》獲頒第6屆[星野立子獎](../Page/星野立子獎.md "wikilink")

## 主要著作

  - 《問花》（），1992年，谷崎潤一郎獎獲獎作品
  - 《場所》（），2001年，[野間文藝獎獲獎作品](../Page/野間文藝獎.md "wikilink")
  - 《白話文譯　[源氏物語](../Page/源氏物語.md "wikilink")》（）
  - 《釋迦》（）

## 曾飾演過瀨戶內寂聽的演員

  - [宮澤理惠](../Page/宮澤理惠.md "wikilink")（[女人一代記系列](../Page/女人一代記.md "wikilink")）
  - [满岛光](../Page/满岛光.md "wikilink")（[夏之殘戀](../Page/夏之殘戀.md "wikilink")）

## 參考資料

## 外部連結

  - [寂庵](http://www.jakuan.com/index.html)（日語）
  - [瀨戶內佛壇店](http://www.butsugu.co.jp/index.html)（日語，她的娘家）

[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")
[Category:谷崎潤一郎獎獲得者](../Category/谷崎潤一郎獎獲得者.md "wikilink")
[Category:日本女性作家](../Category/日本女性作家.md "wikilink")
[Category:日本女性小说家](../Category/日本女性小说家.md "wikilink")
[Category:德島縣出身人物](../Category/德島縣出身人物.md "wikilink")
[Category:天台宗僧人](../Category/天台宗僧人.md "wikilink")
[Category:佛教出家女眾](../Category/佛教出家女眾.md "wikilink")
[Category:東京女子大學校友](../Category/東京女子大學校友.md "wikilink")

1.