**中华人民共和国财政部**，是[中华人民共和国国务院負責](../Page/中华人民共和国国务院.md "wikilink")[财政事务的](../Page/财政.md "wikilink")[组成部门](../Page/国务院组成部门.md "wikilink")。

## 沿革

中华人民共和国财政部前身为1949年10月1日成立的[中央人民政府财政部](../Page/中央人民政府财政部.md "wikilink")。

1954年9月，[第一届全国人民代表大会第一次会议在北京召开](../Page/第一届全国人民代表大会第一次会议.md "wikilink")，会议通过了《[中华人民共和国宪法](../Page/中华人民共和国宪法.md "wikilink")》和《[中华人民共和国国务院组织法](../Page/中华人民共和国国务院组织法.md "wikilink")》，成立[中华人民共和国国务院根据国务院](../Page/中华人民共和国国务院.md "wikilink")《关于设立、调整中央和地方国家机关及有关事项的通知》，中央人民政府财政部即告结束。国务院按照《国务院组织法》的规定，将原中央人民政府财政部改为中华人民共和国财政部，接替相关工作，成为[国务院组成部门](../Page/国务院组成部门.md "wikilink")。

2018年3月17日[第十三届全国人民代表大会第一次会议通过](../Page/第十三届全国人民代表大会第一次会议.md "wikilink")《第十三届全国人民代表大会第一次会议
关于国务院机构改革方案的决定》，批准《国务院机构改革方案》。方案规定：“调整全国社会保障基金理事会隶属关系。将全国社会保障基金理事会由国务院管理调整为由财政部管理，作为基金投资运营机构，不再明确行政级别。”\[1\]\[2\]\[3\]

## 职责

根据《》，财政部承担下列职能\[4\]，

## 机构设置

根据《》，财政部设置下列机构\[5\]：

### 内设机构

[国家农业综合开发办公室设在财政部](../Page/国家农业综合开发办公室.md "wikilink")。[财政部巡视工作领导小组办公室列入财政部党组办事机构序列](../Page/财政部巡视工作领导小组办公室.md "wikilink")。

### 财政部管理的事业单位

  - [全国社会保障基金理事会](../Page/全国社会保障基金理事会.md "wikilink")（不明确行政级别）

### 直属事业单位

### 主管社会团体

### 派出机构

## 历任部长

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>肖像</p></th>
<th><p><a href="../Page/籍贯.md" title="wikilink">籍贯</a></p></th>
<th><p>在任时间</p></th>
<th><p>时任总理</p></th>
<th><p>最高职务</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>1954年9月29日－1970年6月</p></td>
<td><p><a href="../Page/周恩来.md" title="wikilink">周恩来</a></p></td>
<td><p>中国共产党中央委员会副主席<br />
中華人民共和國主席<br />
中国人民政治协商会议全国委员会主席</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/殷承禎.md" title="wikilink">殷承禎</a></p></td>
<td></td>
<td><p><a href="../Page/湖北.md" title="wikilink">湖北</a><a href="../Page/崇陽.md" title="wikilink">崇陽</a></p></td>
<td><p>1967年7月－1970年6月（财政部<a href="../Page/军管会.md" title="wikilink">军管会主任</a>）<br />
1970年6月－1975年1月17日（财政部<a href="../Page/革命委员会.md" title="wikilink">革命委员会主任</a>）</p></td>
<td><p><a href="../Page/周恩来.md" title="wikilink">周恩来</a></p></td>
<td><p>財政部革命委員會主任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張勁夫.md" title="wikilink">張勁夫</a></p></td>
<td></td>
<td><p><a href="../Page/安徽.md" title="wikilink">安徽</a><a href="../Page/肥東.md" title="wikilink">肥東</a></p></td>
<td><p>1975年1月17日－1979年9月13日</p></td>
<td><p><a href="../Page/周恩来.md" title="wikilink">周恩来</a><a href="../Page/华国锋.md" title="wikilink">华国锋</a></p></td>
<td><p><a href="../Page/中国共产党中央顾问委员会.md" title="wikilink">中国共产党中央顾问委员会常务委员会委员</a><br />
<a href="../Page/國務委員.md" title="wikilink">國務委員</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳波_(政治家).md" title="wikilink">吳波</a></p></td>
<td></td>
<td><p><a href="../Page/安徽.md" title="wikilink">安徽</a><a href="../Page/涇縣.md" title="wikilink">涇縣</a></p></td>
<td><p>1978年9月13日－1980年8月26日</p></td>
<td><p><a href="../Page/华国锋.md" title="wikilink">华国锋</a></p></td>
<td><p><a href="../Page/全国人民代表大会财政经济委员会.md" title="wikilink">全国人民代表大会财政经济委员会副主任委员</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王丙乾.md" title="wikilink">王丙-{乾}-</a></p></td>
<td></td>
<td><p><a href="../Page/河北.md" title="wikilink">河北</a><a href="../Page/蠡縣.md" title="wikilink">蠡縣</a></p></td>
<td><p>1980年8月26日－1992年9月4日</p></td>
<td><p><a href="../Page/赵紫阳.md" title="wikilink">赵紫阳</a><a href="../Page/李鹏.md" title="wikilink">李鹏</a></p></td>
<td><p><a href="../Page/全国人民代表大会常务委员会副委员长.md" title="wikilink">全国人民代表大会常务委员会副委员长</a><br />
国务委员</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘仲藜.md" title="wikilink">刘仲藜</a></p></td>
<td></td>
<td><p><a href="../Page/浙江.md" title="wikilink">浙江</a><a href="../Page/寧波.md" title="wikilink">寧波</a></p></td>
<td><p>1992年9月4日－1998年3月18日</p></td>
<td><p><a href="../Page/李鹏.md" title="wikilink">李鹏</a></p></td>
<td><p><a href="../Page/全国社会保障基金理事会.md" title="wikilink">全国社会保障基金理事会理事长</a><br />
<a href="../Page/中国人民政治协商会议全国委员会经济委员会.md" title="wikilink">中国人民政治协商会议全国委员会经济委员会主任</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/项怀诚.md" title="wikilink">项怀诚</a></p></td>
<td></td>
<td><p><a href="../Page/江蘇.md" title="wikilink">江蘇</a><a href="../Page/吳江.md" title="wikilink">吳江</a></p></td>
<td><p>1998年3月18日－2003年3月17日</p></td>
<td><p><a href="../Page/朱镕基.md" title="wikilink">朱镕基</a></p></td>
<td><p><a href="../Page/全国社会保障基金理事会.md" title="wikilink">全国社会保障基金理事会理事长</a><br />
中国人民政治协商会议全国委员会经济委员会主任</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金人慶.md" title="wikilink">金人慶</a></p></td>
<td></td>
<td><p><a href="../Page/江蘇.md" title="wikilink">江蘇</a><a href="../Page/蘇州.md" title="wikilink">蘇州</a></p></td>
<td><p>2003年3月17日－2007年8月30日</p></td>
<td><p><a href="../Page/温家宝.md" title="wikilink">温家宝</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/谢旭人.md" title="wikilink">谢旭人</a></p></td>
<td></td>
<td><p><a href="../Page/浙江.md" title="wikilink">浙江</a><a href="../Page/寧波.md" title="wikilink">寧波</a></p></td>
<td><p>2007年8月30日－2013年3月16日</p></td>
<td><p><a href="../Page/温家宝.md" title="wikilink">温家宝</a></p></td>
<td><p><a href="../Page/全国社会保障基金理事会.md" title="wikilink">全国社会保障基金理事会理事长</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楼继伟.md" title="wikilink">楼继伟</a></p></td>
<td></td>
<td><p><a href="../Page/浙江.md" title="wikilink">浙江</a><a href="../Page/义乌.md" title="wikilink">义乌</a></p></td>
<td><p>2013年3月16日－2016年11月7日</p></td>
<td><p><a href="../Page/李克强.md" title="wikilink">李克强</a></p></td>
<td><p><a href="../Page/全国社会保障基金理事会.md" title="wikilink">全国社会保障基金理事会理事长</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/肖捷.md" title="wikilink">肖捷</a></p></td>
<td></td>
<td><p><a href="../Page/辽宁.md" title="wikilink">辽宁</a><a href="../Page/开原.md" title="wikilink">开原</a></p></td>
<td><p>2016年11月7日－2018年3月19日</p></td>
<td><p><a href="../Page/李克强.md" title="wikilink">李克强</a></p></td>
<td><p><a href="../Page/国务委员.md" title="wikilink">国务委员兼</a><a href="../Page/中华人民共和国国务院秘书长.md" title="wikilink">国务院秘书长</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘昆.md" title="wikilink">刘昆</a></p></td>
<td></td>
<td><p><a href="../Page/广东.md" title="wikilink">广东</a><a href="../Page/饶平.md" title="wikilink">饶平</a></p></td>
<td><p>2018年3月19日至今</p></td>
<td><p><a href="../Page/李克强.md" title="wikilink">李克强</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 派驻财政监察专员办事处

财政部於全国各省级行政区、[计划单列市派駐财政监察专员办事处](../Page/计划单列市.md "wikilink")，作为财政部派出机构，其主要职责為：

1.  监督检查有关部门和单位执行国家财税政策、法规的情况；反映中央财政收支管理中的重大问题；提出加强中央财政管理的相关建议。
2.  实施中央财政收入、支出的监管。
3.  监督检查中央驻该地区二级以下预算单位及有关企业的会计信息质量。
4.  监督检查有关会计师事务所及其注册会计师的执业质量。
5.  监督检查驻该地区的中央级金融单位国有资产管理及会计核算等情况。
6.  承办财政部交办的其他事项。

## 参考文献

## 参见

  - [中央财经委员会](../Page/中央财经委员会.md "wikilink")、[中央财经委员会办公室](../Page/中央财经委员会办公室.md "wikilink")
  - [全国人民代表大会财政经济委员会](../Page/全国人民代表大会财政经济委员会.md "wikilink")
  - [中华人民共和国国家发展和改革委员会](../Page/中华人民共和国国家发展和改革委员会.md "wikilink")
  - [中国人民银行](../Page/中国人民银行.md "wikilink")、[国家外汇管理局](../Page/国家外汇管理局.md "wikilink")
  - [中华人民共和国海关总署](../Page/中华人民共和国海关总署.md "wikilink")、[国家税务总局](../Page/国家税务总局.md "wikilink")
  - [中华人民共和国商务部](../Page/中华人民共和国商务部.md "wikilink")、[中华人民共和国国家工商行政管理总局](../Page/中华人民共和国国家工商行政管理总局.md "wikilink")
  - [中华人民共和国审计署](../Page/中华人民共和国审计署.md "wikilink")、[中华人民共和国国家统计局](../Page/中华人民共和国国家统计局.md "wikilink")

{{-}}                        |-   |-       |-

[Category:中国財政部門](../Category/中国財政部門.md "wikilink")
[中华人民共和国财政部](../Category/中华人民共和国财政部.md "wikilink")
[Category:中华人民共和国中央政府经济行政部门](../Category/中华人民共和国中央政府经济行政部门.md "wikilink")
[Finance](../Category/1954年建立政府機構.md "wikilink")
[Category:第1届国务院组成部门](../Category/第1届国务院组成部门.md "wikilink")
[Category:第2届国务院组成部门](../Category/第2届国务院组成部门.md "wikilink")
[Category:第3届国务院组成部门](../Category/第3届国务院组成部门.md "wikilink")
[Category:第4届国务院组成部门](../Category/第4届国务院组成部门.md "wikilink")
[Category:第5届国务院组成部门](../Category/第5届国务院组成部门.md "wikilink")
[Category:第6届国务院组成部门](../Category/第6届国务院组成部门.md "wikilink")
[Category:第7届国务院组成部门](../Category/第7届国务院组成部门.md "wikilink")
[Category:第8届国务院组成部门](../Category/第8届国务院组成部门.md "wikilink")
[Category:第9届国务院组成部门](../Category/第9届国务院组成部门.md "wikilink")
[Category:第10届国务院组成部门](../Category/第10届国务院组成部门.md "wikilink")
[Category:第11届国务院组成部门](../Category/第11届国务院组成部门.md "wikilink")
[Category:第12届国务院组成部门](../Category/第12届国务院组成部门.md "wikilink")
[Category:第13届国务院组成部门](../Category/第13届国务院组成部门.md "wikilink")

1.
2.
3.
4.
5.