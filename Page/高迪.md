**高迪**（）出生于[山东](../Page/山东.md "wikilink")[曲阜](../Page/曲阜.md "wikilink")，是一名中国足球运动员，司职前锋，现效力于[中国足球超级联赛俱乐部](../Page/中国足球超级联赛.md "wikilink")[上海绿地申花](../Page/上海绿地足球俱乐部.md "wikilink")。

## 职业生涯

### 青年队

  - 1999年进入[山东鲁能泰山的鲁能足校](../Page/山东鲁能泰山.md "wikilink")。

### 俱乐部

  - 2008年进入[山东鲁能泰山一队](../Page/山东鲁能泰山.md "wikilink")，在联赛中多次进入18人名单，但未获得出场机会。
  - 2009年在[亚足联冠军联赛客场对阵](../Page/亚足联冠军联赛.md "wikilink")[斯利维加亚的比赛中第一次代表球队在正式比赛中出场](../Page/斯里维加亚足球俱乐部.md "wikilink")，并打入一粒进球\[1\]，但球队却被连扳四球失利\[2\]。
  - 2013赛季租借到[杭州绿城](../Page/杭州绿城.md "wikilink")，在联赛第一轮为杭州绿城攻入制胜一球。
  - 2014年2月24日转会加盟[上海绿地申花](../Page/上海绿地申花足球俱乐部.md "wikilink")。\[3\]
  - 2017年1月6日，高迪27岁生日当天，[上海绿地申花宣布与其续约](../Page/上海绿地申花足球俱乐部.md "wikilink")，新合同的期限不详。\[4\]
  - 2017年2月27日，[江苏苏宁宣布高迪租借加盟球队](../Page/江苏苏宁足球俱乐部.md "wikilink")，租期为一年。\[5\]

### 国家队

  - 2006年代表中国国少队参加新加坡亚少赛，打入3球。
  - 2007年代表中国国青队参加亚青赛的预选赛，由于伤病未参加第二年的亚青赛决赛阶段比赛。
  - 2014年6月18日，在[中国和](../Page/中国国家足球队.md "wikilink")[马其顿的友谊赛第](../Page/马其顿国家足球队.md "wikilink")68分钟替补[陈子介出场](../Page/陈子介.md "wikilink")，上演成年国家队首秀，并在88分钟射进一球，帮助中国2比0击败对手。

## 荣誉

### 山东鲁能泰山

  - [中国足球超级联赛冠军](../Page/中国足球超级联赛.md "wikilink")：[2008](../Page/2008年中国足球超级联赛.md "wikilink")、[2010](../Page/2010年中国足球超级联赛.md "wikilink")

## 参考

[Category:曲阜籍足球运动员](../Category/曲阜籍足球运动员.md "wikilink")
[Category:山东鲁能泰山球员](../Category/山东鲁能泰山球员.md "wikilink")
[Category:杭州绿城球员](../Category/杭州绿城球员.md "wikilink")
[Category:上海申花球员](../Category/上海申花球员.md "wikilink")
[Category:江苏苏宁球员](../Category/江苏苏宁球员.md "wikilink")
[D迪](../Category/高姓.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:中超球员](../Category/中超球员.md "wikilink")
[Category:中国国家足球队运动员](../Category/中国国家足球队运动员.md "wikilink")

1.
2.
3.
4.
5.