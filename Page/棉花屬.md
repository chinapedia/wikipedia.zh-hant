**棉花属**（學名：**），或作**棉属**，是[被子植物](../Page/被子植物.md "wikilink")[锦葵科](../Page/锦葵科.md "wikilink")之下的一個[属](../Page/属.md "wikilink")，日常生活所用的[棉花皆為本属植物](../Page/棉花.md "wikilink")[種筴内的纖維](../Page/種筴.md "wikilink")。本屬物種是[旧大陆和](../Page/旧大陆.md "wikilink")[新大陸上熱帶及亞熱帶地區的原生物種](../Page/新大陸.md "wikilink")。現時整個屬包括有約50個物種，使之成為棉族之下物種多元化最高的屬。估計未來仍有可能發現新的物種。

我們日常生活所用的棉花主要由四个栽培棉种组成，即[亚洲棉](../Page/亚洲棉.md "wikilink")、[非洲棉](../Page/非洲棉.md "wikilink")、[大陆棉](../Page/大陆棉.md "wikilink")（又叫[细绒棉](../Page/细绒棉.md "wikilink")）、[海岛棉](../Page/海岛棉.md "wikilink")（又叫[长绒棉](../Page/长绒棉.md "wikilink")）。

## 中国的棉业

中国植棉大约有2000年的[历史](../Page/历史.md "wikilink")。但到1950年代末，陆地棉成为主要品种，其次是长绒棉。长绒棉纤维较长，從前蘇聯引進，所以在[新疆一些地区有一定产量](../Page/新疆.md "wikilink")。中国是世界第一大产棉國，其次是[美国](../Page/美国.md "wikilink")，第三是[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")。

## 參考文獻

## 參看

  - [棉花](../Page/棉花.md "wikilink")

## 外部連結

  -
  -

[棉屬](../Category/棉屬.md "wikilink") [棉](../Category/棉.md "wikilink")
[Category:纤维植物](../Category/纤维植物.md "wikilink")
[Category:能源作物](../Category/能源作物.md "wikilink")
[Category:生物燃料](../Category/生物燃料.md "wikilink")
[Category:饲料](../Category/饲料.md "wikilink")
[Category:非食用農作物](../Category/非食用農作物.md "wikilink")