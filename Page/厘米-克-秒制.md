**厘米-克-秒單位制**或**厘米-克-秒系統**（英文：centimetre-gram-second
system，故常簡稱**CGS**制）是一種物理單位的系統制度，分別以[厘米](../Page/厘米.md "wikilink")、[克及](../Page/克.md "wikilink")[秒為](../Page/秒.md "wikilink")[長度](../Page/長度.md "wikilink")、[質量及](../Page/質量.md "wikilink")[時間的基本單位](../Page/時間.md "wikilink")。

在力學單位方面厘米-克-秒單位制是一致的，但在[電學單位方面則有幾種變體](../Page/電學.md "wikilink")。此單位系統後來被MKS-{制}-取代，也就是米-千克-秒系統（meter-kilogram-second
system），而其又被[國際單位制](../Page/國際單位制.md "wikilink")（SI
system）所取代；國際單位制具有MKS制的三個基本單位，再加上[凱氏溫標](../Page/凱氏溫標.md "wikilink")、[安培](../Page/安培.md "wikilink")、[燭光及](../Page/燭光.md "wikilink")[莫耳](../Page/摩尔_\(单位\).md "wikilink")，有許多工程及科學領域只使用國際單位制，不過仍有一些領域常使用厘米-克-秒單位制。

在量測純[力學系統時](../Page/力學系統.md "wikilink")（即只和[長度](../Page/長度.md "wikilink")、[質量](../Page/質量.md "wikilink")、[力](../Page/力.md "wikilink")、[壓力](../Page/壓力.md "wikilink")、[能量等物理量有關的系統](../Page/能量.md "wikilink")），厘米-克-秒制和國際單位制之間的轉換相當單純及明確。單位間的轉換係數均為10的次幂，均可由以下關係推導而成；100 cm
= 1 m及1000 g =
1 kg。例如厘米-克-秒制下，力的單位為[達因](../Page/達因.md "wikilink")，等於1 g·cm/s<sup>2</sup>，國際單位制力的單位為[牛頓](../Page/牛頓_\(單位\).md "wikilink")，等於1 kg·m/s<sup>2</sup>，因此可以依上述關係推得1 達因=10<sup>−5</sup> 牛頓。

厘米-克-秒單位制下[熱能的單位為](../Page/熱能.md "wikilink")[卡路里](../Page/卡路里.md "wikilink")，其定義為將1克的水由溫度15.5 °C變成16.5 °C所需的熱量。

但在量測有關電磁的系統時（例如和[電荷](../Page/電荷.md "wikilink")、[電場](../Page/電場.md "wikilink")、[磁場](../Page/磁場.md "wikilink")、[電壓等物理量有關的系統](../Page/電壓.md "wikilink")），厘米-克-秒制和國際單位制之間的轉換就相當的複雜。甚至[電磁學定律](../Page/電磁學.md "wikilink")（例如[馬克士威方程組](../Page/馬克士威方程組.md "wikilink")）的公式需要依所使用的單位加以調整。國際單位制的電磁學單位和厘米-克-秒制的對應單位之間沒有[一一对应的關係](../Page/一一对应.md "wikilink")。在厘米-克-秒制中，對應同一物理量（例如電流）有幾種不同的電磁學單位，因此產生了幾種厘米-克-秒制的變體，包括[高斯單位制](../Page/高斯單位制.md "wikilink")、[靜電單位制](../Page/靜電單位制.md "wikilink")、電磁單位制及[勞侖茲-黑維塞單位制等](../Page/勞侖茲-黑維塞單位制.md "wikilink")，後來最常用的是高斯單位制，有時仍會出現在技術文獻中，特別是在美國的[電動力學及](../Page/電動力學.md "wikilink")[天文學領域](../Page/天文學.md "wikilink")，因此常常用厘米-克-秒制代表高斯單位制。

## 歷史

此單位系統最先是由德國數學家[卡爾·高斯於](../Page/卡爾·高斯.md "wikilink")1832年所提案\[1\]，並在1874年由於英國物理學家[詹姆斯·馬克士威及](../Page/詹姆斯·馬克士威.md "wikilink")[威廉·湯姆森加入了電磁學單位而延伸](../Page/威廉·湯姆森.md "wikilink")。厘米-克-秒單位制的尺度在實際應用上顯得過小而不方便，例如一般人的體重若用厘米-克-秒單位制表示時，需要用到5位數才能表示，因此很少用在[電動力學以外的領域](../Page/電動力學.md "wikilink")，並且自1880年代開始國際漸不採用，但直到20世紀中葉才由更實用的[MKS制取代](../Page/MKS制.md "wikilink")，隨後MKS制又轉化成現代通行的[國際單位制](../Page/國際單位制.md "wikilink")。

由於厘米-克-秒單位制逐漸的被MKS制及國際單位-{制}-取代，在技術領域使用厘米-克-秒單位制的情形正逐漸減少。許多科學期刊或國際標準單位已不使用厘米-克-秒單位制，不過在[天文學的期刊中仍會使用](../Page/天文學.md "wikilink")。美國的[材料科學](../Page/材料科學.md "wikilink")、電動力學及天文學中偶爾會使用厘米-克-秒單位制。由於MKS制（及國際單位制）的磁通量密度單位[特斯拉太大](../Page/特斯拉.md "wikilink")，在日常使用上不便，一般會使用厘米-克-秒單位制的對應單位[高斯](../Page/高斯_\(单位\).md "wikilink")，因此在磁學及其相關領域中仍會使用厘米-克-秒單位制。

厘米-克-秒單位制的基礎單位[公克及](../Page/公克.md "wikilink")[厘米雖不是國際單位制的基礎單位](../Page/厘米.md "wikilink")，仍被使用在一些簡單的，可在實驗桌上操作的物理及化學實驗中。不過在使用衍生單位時，只會使用國際單位，例如物理實驗室可能會用公克及厘米為質量及長度的單位，但力的單位（衍生單位）會使用國際單位制的單位牛頓，而不會使用厘米-克-秒單位制的單位達因。

## 厘米-克-秒制力學單位的定義

厘米-克-秒制及國際單位制用相同的方式定義力學的單位，二者的差異是使用不同的長度及質量基礎單位，厘米-克-秒制使用厘米和克為長度及質量基礎單位，國際單位制使用米和千克為基礎單位，厘米-克-秒制及國際單位制的時間基礎單位相同，都是秒。

厘米-克-秒制及國際單位制的力學單位之間有一對一的對應關係，力學定律的型式不會依使用的單位而改變。衍生單位是利用力學定律來定義，是三個基礎單位的組合，因此二種單位系統的衍生單位有明確的對應關係：

\[v = \frac{dx}{dt}\] （[速度的定義](../Page/速度.md "wikilink")）

\[F=m\frac{d^2x}{dt^2}\]  （[牛頓第二運動定律](../Page/牛頓第二運動定律.md "wikilink")）

\[E = \int \vec{F}\cdot \vec{dx}\] 
（[能量定義為](../Page/能量.md "wikilink")[機械功的形式](../Page/機械功.md "wikilink")）

\[p = \frac{F}{L^2}\]  （[壓強定義為單位面積的受力](../Page/壓強.md "wikilink")）

\[\eta = \tau/\frac{dv}{dx}\] 
（[黏度定義為單位速度](../Page/黏度.md "wikilink")[梯度下的](../Page/梯度.md "wikilink")[剪應力](../Page/剪應力.md "wikilink")）

例如厘米-克-秒制的壓強單位[巴](../Page/巴.md "wikilink")（Ba）和其基礎單位之的間關係，和國際單位制的壓強單位[帕斯卡](../Page/帕斯卡.md "wikilink")（Pa）和其基礎單位之間的關係完全相同：

  -
    1 單位壓強 = 1 單位力/(1 單位長度)<sup>2</sup> =
    1 單位質量/(1 單位長度·(1 單位時間)<sup>2</sup>)
    1 Ba = 1 g/(cm·s<sup>2</sup>)
    1 Pa = 1 kg/(m·s<sup>2</sup>).

若要將厘米-克-秒制的衍生單位以國際單位制的衍生單位表示，需要考慮二個單位制中基礎單位之間的係數，反之亦然。

  -
    1 Ba = 1 g/(cm·s<sup>2</sup>) = 10<sup>-3</sup>
    kg/(10<sup>-2</sup>m·s<sup>2</sup>) = 10<sup>-1</sup>
    kg/(m·s<sup>2</sup>) = 10<sup>-1</sup> Pa.

### 厘米-克-秒制力學單位的定義以及轉換係數

| 物理量                              | 符號       | 單位                                            | 定義                                                         | SI單位制                                                       |
| -------------------------------- | -------- | --------------------------------------------- | ---------------------------------------------------------- | ----------------------------------------------------------- |
| [長度](../Page/長度.md "wikilink")   | *L*, *x* | [厘米](../Page/厘米.md "wikilink")(cm)            | 1 cm                                                       | \= 10<sup>−2</sup> 米(m)                                     |
| [質量](../Page/質量.md "wikilink")   | *m*      | [克](../Page/克.md "wikilink")(g)               | 1 g                                                        | \= 10<sup>−3</sup> 千克(kg)                                   |
| [時間](../Page/時間.md "wikilink")   | *t*      | [秒](../Page/秒.md "wikilink")(s)               | 1 s                                                        |                                                             |
| [速度](../Page/速度.md "wikilink")   | *v*      | 厘米/秒(cm/s)                                    | 1 cm/s                                                     | 10<sup>−2</sup> 米/秒                                         |
| [加速度](../Page/加速度.md "wikilink") | *a*      | [伽](../Page/伽.md "wikilink")(gal)             | 1 cm/s²                                                    | 10<sup>−2</sup> 米/秒²                                        |
| [力](../Page/力.md "wikilink")     | *F*      | [達因](../Page/達因.md "wikilink")(dyn)           | 1 dyne = 1 g·cm/s²                                         | \= 10<sup>−5</sup> [牛頓(N)](../Page/牛頓_\(單位\).md "wikilink") |
| [能量](../Page/能量.md "wikilink")   | *E*      | [爾格](../Page/爾格.md "wikilink")(erg)           | 1 erg = 1 g·cm²/s²                                         | \= 10<sup>−7</sup> [焦耳(J)](../Page/焦耳.md "wikilink")        |
| [功率](../Page/功率.md "wikilink")   | *P*      | 爾格/秒(erg/s)                                   | 1 erg/s = 1 g·cm²/s³                                       | \= 10<sup>−7</sup> [瓦特(W)](../Page/瓦特.md "wikilink")        |
| [壓力](../Page/壓力.md "wikilink")   | *p*      | [巴](../Page/巴.md "wikilink")(Bar)             | 1 Bar = 10<sup>6</sup> dyne/cm² = 10<sup>6</sup> g/(cm·s²) | \= 10<sup>5</sup> [帕(Pa)](../Page/帕.md "wikilink")          |
| [黏度](../Page/黏度.md "wikilink")   | *μ*      | [泊](../Page/泊_\(單位\).md "wikilink")(P)        | 1 P = 1 g/(cm·s)                                           | \= 10<sup>−1</sup> 帕-秒(Pa·s)                                |
| 動黏度                              | *ν*      | [斯托克](../Page/斯托克_\(單位\).md "wikilink")(St)   | 1 St = 1 cm²/s                                             | \= 10<sup>-4</sup> 米/秒                                      |
| [波數](../Page/波數.md "wikilink")   | *k*      | [凱塞](../Page/凱塞_\(單位\).md "wikilink")(kayser) | 1 kayser = cm<sup>−1</sup>                                 | \= 100 米<sup>−1</sup>                                       |

力學厘米-克-秒單位制

## 厘米-克-秒制對於電磁學單位的作法

厘米-克-秒制及國際單位制在[電磁學的單位有很大的差異](../Page/電磁學.md "wikilink")，厘米-克-秒制因為電磁學單位的不同，有不同的變體，甚至電磁學定律的形式也會隨使用單位制不同而不同，以下描述二者的基本差異：

  - 國際單位制中將[電流的單位](../Page/電流.md "wikilink")[安培定義為基本單位](../Page/安培.md "wikilink")，其定義為二條電流為1安培，距離為1米的平行無限長導線，其產生的作用力為2×10<sup>–7</sup>
    [N](../Page/牛頓_\(單位\).md "wikilink")/[m](../Page/米_\(單位\).md "wikilink")（此定義方式類似厘米-克-秒制中的電磁單位制，因此國際單位制和電磁單位制比較接近，許多單位的轉換係數都只是10的乘幂）。安培和米、千克及秒一樣都是基本單位，因此安培無法由米、千克及秒等基本單位組合而成。因此國際單位制的電磁學定律需要額外的常數（例如[真空电容率](../Page/真空电容率.md "wikilink")）來將電磁學的單位轉換為力學單位，常數的大小和安培的定義方式有關。所有其他的電磁學單位都是由安培、米、千克及秒所組成的衍生單位，例如[電荷](../Page/電荷.md "wikilink")*q*定義為電流*I*和時間*t*的乘積：

\[q=I\cdot t\],

  -
    因此電荷的單位[庫侖](../Page/庫侖.md "wikilink")（C）定義為1 C = 1 A·s。

<!-- end list -->

  - 厘米-克-秒制中的[靜電單位制及](../Page/靜電單位制.md "wikilink")[高斯單位制不為電學新增基本單位](../Page/高斯單位制.md "wikilink")，因此所有的電磁學單位都是由厘米、克及秒組成的衍生單位，由電磁學和力學有關的定律推導而來。

### 厘米-克-秒制電磁學單位的推導方式

有許多方式可以推導電磁學的物理量及長度、時間及質量等單位之間的關係。其中有二種方式是以電荷的受力為主。有二個互相獨立的定律，分別描述電荷及其微分量（[電流](../Page/電流.md "wikilink")）和力之間的關係。二個定律可以寫成以下可通用於各單位制的形式\[2\]：

  - 第一個是[库仑定律](../Page/库仑定律.md "wikilink")，\(F = k_C \frac{q \cdot q^\prime}{d^2}\)，描述二個距離為\(d\)的電荷*q*及*q*'之間的靜電力\(F\)。此處的\(k_C\)為常數，和電荷單位的定義方式有關。
  - 第二個是[安培力定律](../Page/安培力定律.md "wikilink")，\(\frac{dF}{dL} = 2 k_A\frac{I \, I^\prime}{d}\)，描述二個距離\(d\)的無限長平行導線，導線直徑遠小於距離，其電流分別是*I*及*I*'，單位長度導線\(L\)所受到的電磁力\(F\)。由於\(I=q/t\,\)、\(I^\prime=q^\prime/t\)，常數\(k_A\)的數值也和電荷單位的定義方式有關。

[馬克士威電磁方程連結上述二個定律](../Page/馬克士威方程組.md "wikilink")，根據麦克斯韦電磁方程，以上二個常數
\(k_C\)及
\(k_A\)需符合\(k_C / k_A = c^2\)的關係，其中*c*為[真空中的](../Page/真空.md "wikilink")[光速](../Page/光速.md "wikilink")。因此上述二個常數無法個別獨立調整。若根據库仑定律定義電荷的單位，令\(k_C=1\)，則安培定律就會出現\(2/c^2\)的係數。相對的，若利用安培力定律定義電流單位，令\(k_A = 1\)或\(k_A = 1/2\)，同時也固定了库仑定律中的的係數。

在厘米-克-秒制的發展過程中分別有人使用上述二種不同的電荷單位衍生方式，因此產生了二種厘米-克-秒制的變體。不過還有其他方式可由長度、時間及質量推導電磁學的單位。例如利用以下二個[磁場和其他力學物理量的公式](../Page/磁場.md "wikilink")，也可推導電磁學的單位：

  - 第一個定律描述磁場**B**對一個以速度**v**運動的電荷**q**產生的磁力：

<!-- end list -->

  -

      -
        \(\mathbf{F} =  \alpha_L q\;\mathbf{v} \times \mathbf{B}\;.\)

<!-- end list -->

  - 第二個定律為[毕奥－萨伐尔定律](../Page/毕奥－萨伐尔定律.md "wikilink")，描述一個有限長度d**l**，上面有電流*I*的導線對於一個位置以向量**r**表示的一點產生的靜磁場**B**：

<!-- end list -->

  -

      -
        \(d\mathbf{B} =  \alpha_B\frac{I d\mathbf{l} \times \mathbf{\hat r}}{r^2}\;,\)其中*r*及
        \(\mathbf{\hat r}\)為向量**r**的長度及單位向量。

上述二定律可以推導安培力定律，而三個定律中的常數有以下的關係：\(k_A  = \alpha_L \cdot \alpha_B\;\)。若利用安培力定律定義電荷，使得\(k_A  = 1\)，很自然的可以令\(\alpha_L = \alpha_B=1\;\)，利用上述二個定律定義磁場。否則，需要在上述二個定律中選擇一個較合適的定律來定義磁場的單位。

若需要描述在非真空介質下的[電位移](../Page/電位移.md "wikilink")
**D**及磁場**H**，需要定義二個常數，分別是[真空電容率ε](../Page/真空電容率.md "wikilink")<sub>0</sub>及[真空磁導率μ](../Page/真空磁導率.md "wikilink")<sub>0</sub>。因此可得到以下的通式\[3\]\(\mathbf{D} = \epsilon_0 \mathbf{E} + \lambda \mathbf{P}\)及
\(\mathbf{H} = \mathbf{B} / \mu_0 - \lambda^\prime \mathbf{M}\)，其中**P**及**M**分別是[電極化強度及](../Page/電極化強度.md "wikilink")[磁化強度向量](../Page/磁化強度.md "wikilink")。而因子λ及λ′稱為有理化常數，是一個無因次量，一般會選為\(4 \pi k_C \epsilon_0\)。若λ
= λ′ =
1，此單位制稱為「有理化單位制」\[4\]：關於球面的電磁方程式會含有4π，關於圓柱面的則含有2π，處理直導線或平行板的則完全不含π。不過原始的厘米-克-秒制是使用λ
= λ′ = 4π，亦即\(k_C \epsilon_0=1\)。因此以下要介紹的高斯單位制、靜電單位制或靜磁單位制都不是有理化單位制。

### 厘米-克-秒制電磁學單位的變體

下表列出常用的厘米-克-秒制變體中，對應上述常數的值。

<table>
<thead>
<tr class="header">
<th><p>單位制</p></th>
<th><p><span class="math inline"><em>k</em><sub><em>C</em></sub></span></p></th>
<th><p><span class="math inline"><em>α</em><sub><em>B</em></sub></span></p></th>
<th><p><span class="math inline"><em>ϵ</em><sub>0</sub></span></p></th>
<th><p><span class="math inline"><em>μ</em><sub>0</sub></span></p></th>
<th><p><span class="math inline">$k_A=\frac{k_C}{c^2}$</span></p></th>
<th><p><span class="math inline">$\alpha_L=\frac{k_C}{\alpha_Bc^2}$</span></p></th>
<th><p><span class="math inline"><em>λ</em> = 4<em>π</em><em>k</em><sub><em>C</em></sub> ⋅ <em>ϵ</em><sub>0</sub></span></p></th>
<th><p><span class="math inline"><em>λ</em>′</span></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>CGS靜電單位制[5]<br />
(ESU, esu, 或 stat-)</p></td>
<td><p>1</p></td>
<td><p><em>c</em><sup>−2</sup></p></td>
<td><p>1</p></td>
<td><p><em>c</em><sup>−2</sup></p></td>
<td><p><em>c</em><sup>−2</sup></p></td>
<td><p>1</p></td>
<td><p>4π</p></td>
<td><p>4π</p></td>
</tr>
<tr class="even">
<td><p>CGS電磁單位制[6]<br />
(EMU, emu, 或 ab-)</p></td>
<td><p><em>c</em><sup>2</sup></p></td>
<td><p>1</p></td>
<td><p><em>c</em><sup>−2</sup></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>4π</p></td>
<td><p>4π</p></td>
</tr>
<tr class="odd">
<td><p>CGS<a href="../Page/高斯單位制.md" title="wikilink">高斯單位制</a>[7]</p></td>
<td><p>1</p></td>
<td><p><em>c</em><sup>−1</sup></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p><em>c</em><sup>−2</sup></p></td>
<td><p><em>c</em><sup>−1</sup></p></td>
<td><p>4π</p></td>
<td><p>4π</p></td>
</tr>
<tr class="even">
<td><p>CGS<a href="../Page/勞侖茲-黑維塞單位制.md" title="wikilink">勞侖茲-黑維塞單位制</a>[8]</p></td>
<td><p><span class="math inline">$\frac{1}{4\pi}$</span></p></td>
<td><p><span class="math inline">$\frac{1}{4\pi c}$</span></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p><span class="math inline">$\frac{1}{4\pi c^2}$</span></p></td>
<td><p><em>c</em><sup>−1</sup></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/國際單位制.md" title="wikilink">國際單位制</a></p></td>
<td><p><span class="math inline">$\frac{c^2}{b}$</span></p></td>
<td><p><span class="math inline">$\frac{1}{b}$</span></p></td>
<td><p><span class="math inline">$\frac{b}{4\pi c^2}$</span></p></td>
<td><p><span class="math inline">$\frac{4\pi}{b}$</span></p></td>
<td><p><span class="math inline">$\frac{1}{b}$</span></p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
</tr>
</tbody>
</table>

國際單位制中的常數*b*是一個單位轉換有關的常數，定義為：\(b=10^7\,\mathrm{A}^2/\mathrm{N} = 10^7\,\mathrm{m/H}=4\pi/\mu_0=4\pi\epsilon_0 c^2\;\)

有些書籍會使用以下名稱的常數\[9\]\[10\]

\[k_C=k_1=k_E\]

\[\alpha_B=\alpha\cdot k_2=k_B\]

\[k_A=k_2=k_E/c^2\]

\[\alpha_L=k_3=k_F\]

[麦克斯韦方程组可以寫成以下可通用於各單位制的形式](../Page/麦克斯韦方程组.md "wikilink")\[11\]\[12\]：

\(\begin{array}{ccl}
\vec \nabla \cdot \vec E & = & 4 \pi k_C \rho \\
\vec \nabla \cdot \vec B & = & 0 \\
\vec \nabla \times \vec E & = & \displaystyle{- \alpha_L \frac{\partial \vec B}{\partial t}} \\
\vec \nabla \times \vec B & = & \displaystyle{4 \pi \alpha_B \vec J + \frac{\alpha_B}{k_C}\frac{\partial \vec E}{\partial t}}
\end{array}\)

在以上幾種單位制中，只有高斯單位制及勞侖茲-黑維塞單位制的\(\alpha_L\)等於\(c^{-1}\)而不是1。
因此真空中[電磁波產生的](../Page/電磁波.md "wikilink")\(\vec E\)及\(\vec B\)向量場，以上述二種單位表示時有相同的單位。

### 靜電單位制（ESU）

靜電單位制（electrostatic
units）簡稱ESU，是厘米-克-秒制的一種變體。靜電單位制的電荷是以電荷對其他電荷的施力來定義，而電流定義成電荷對時間的微分。靜電單位制的庫侖常數\(k_C\)定義為1，因此靜電單位制下的[庫侖定律中沒有出現](../Page/庫侖定律.md "wikilink")[比例量](../Page/比例.md "wikilink")。

靜電單位制的電荷單位**franklin**
(**Fr**)，也稱為[靜電庫侖](../Page/靜電庫侖.md "wikilink")（statcoulomb）、靜庫侖或esu電荷（esu
charge），其定義如下：\[13\]
因此在靜電單位制中，一靜電庫侖等於[厘米和達因平方根的乘積](../Page/厘米.md "wikilink")：

  -
    \(\mathrm{1\,Fr = 1\,statcoulomb = 1\,esu\; charge = 1\,cm\sqrt{dyne}=1\,g^{1/2} \cdot cm^{3/2} \cdot s^{-1}}\).

電流的單位則定義如下：

  -
    \(\mathrm{1\,Fr/s = 1\,statampere = 1\,esu\; current = 1\,(cm/s)\sqrt{dyne}=1\,g^{1/2} \cdot cm^{3/2} \cdot s^{-2}}\).

在靜電單位制中，電荷*q*的因次為m<sup>1/2</sup>L<sup>3/2</sup>t<sup>−1</sup>。電荷或電流都不是有獨立因次的物理量，都可以由其他物理量的函數所表示。這種單位的簡化是應用[白金漢π定理的結果](../Page/白金漢π定理.md "wikilink")。

#### 靜電單位-{制}-表示法

在厘米-克-秒制的電磁單位制中，若電磁相關單位沒有特殊名稱，會使用其對應國際單位制的單位名稱，但前面加上字首stat或是以一縮寫esu表示\[14\]，單位的中文名稱則會在對應國際單位制的單位名稱前加上「靜」或「靜電」。

### 電磁單位制（EMU）

電磁單位制（electromagnetic
units）簡稱EMU，也是厘米-克-秒制的一種變體。電磁單位制的電流是以二無限長的平行載流導線之間的施力來定義，而電荷定義成電流和時間的乘積。（國際單位制也是用類似的方式來定義[安培](../Page/安培.md "wikilink")）。電磁單位制的安培力常數\(k_A\)定義為1，因此電磁單位制下的[安培力定律中只有出現](../Page/安培力定律.md "wikilink")[比例係數](../Page/比例.md "wikilink")2。（比例係數2是將一般形式的安培力定律對無窮長導線積分的結果）。

電磁單位制的電流單位[毕奥](../Page/毕奥.md "wikilink")（Bi）也稱為[絕對安培](../Page/絕對安培.md "wikilink")（abampere），其定義如下：\[15\]
因此在電磁單位制中，一毕奥等於一達因的平方根：

  -
    \(\mathrm{1\,Bi = 1\,abampere = 1\,emu\; current= 1\,\sqrt{dyne}=1\,g^{1/2} \cdot cm^{1/2} \cdot s^{-1}}\).

而電荷的單位為：

  -
    \(\mathrm{1\,Bi\cdot s = 1\,abcoulomb = 1\,emu\, charge= 1\,s\cdot\sqrt{dyne}=1\,g^{1/2} \cdot cm^{1/2}}\).

在電磁單位制中，電荷*q*的因次為m<sup>1/2</sup>L<sup>1/2</sup>。電荷或電流都不是有獨立因次的物理量，都可以由其他物理量的函數所表示。

#### 電磁單位-{制}-表示法

在厘米-克-秒制的靜電單位制中，若電磁相關單位沒有特殊名稱，會使用其對應國際單位制的單位名稱，但前面加上字首ab或是以一縮寫emu表示\[16\]，單位的中文名稱則會在對應國際單位制的單位名稱前加上「絕對」。

### 靜電單位制和電磁單位制之間的關係

靜電單位制及電磁單位制的關係是以\(k_C / k_A = c^2\)的關係式為基礎，其中*c* = 29,979,245,800 ≈
3·10<sup>10</sub>是以厘米每秒為單位下的[光速](../Page/光速.md "wikilink")。因此像二單位制下的電流、電荷、電壓……等電磁物理量之間的比例會是*c<sup>-1</sup>*或是*c*：\[17\]

\[\mathrm{\frac{1\,statcoulomb}{1\,abcoulomb}}=
\mathrm{\frac{1\,statampere}{1\,abampere}}=c^{-1}\] 及

\[\mathrm{\frac{1\,statvolt}{1\,abvolt}}=
\mathrm{\frac{1\,stattesla}{1\,gauss}}=c\].
二單位制下的其他衍生單位，之間的比例可能會是*c*的高次方，例如

\[\mathrm{\frac{1\,statohm}{1\,abohm}}=
\mathrm{\frac{1\,statvolt}{1\,abvolt}}\times\mathrm{\frac{1\,abampere}{1\,statampere}}=c^2\].

### 其他變體

在歷史上曾使用過幾種不同的電磁單位，大部份都是由厘米-克-秒制衍生而來\[18\]，其中也包括了[高斯單位制及](../Page/高斯單位制.md "wikilink")[勞侖茲-黑維塞單位制](../Page/勞侖茲-黑維塞單位制.md "wikilink")。

有些美國的科學家及工程師會使用混合的單位制，例如用[伏特每](../Page/伏特.md "wikilink")[厘米表示電場](../Page/厘米.md "wikilink")。其實上述作法類似國際單位制，只是所所所有的長度都要以厘米來表示。

## 不同厘米-克-秒單位-{制}-下的電磁學單位

<table>
<caption>國際單位制的電磁學單位及靜電單位制、電磁單位制及高斯單位制相關單位的轉換[19]<br />
<em>c</em> = 29,979,245,800 ≈ 3·10<sup>10</sub></caption>
<thead>
<tr class="header">
<th><p>物理量</p></th>
<th><p>符號</p></th>
<th><p>國際單位制</p></th>
<th><p>靜電單位制</p></th>
<th><p>電磁單位制</p></th>
<th><p>高斯單位制</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/電荷.md" title="wikilink">電荷</a></p></td>
<td><p><em>q</em></p></td>
<td><p>1 <a href="../Page/庫侖.md" title="wikilink">C</a></p></td>
<td><p>= (10<sup>−1</sup> <em>c</em>) <a href="../Page/靜庫侖.md" title="wikilink">statC</a></p></td>
<td><p>= (10<sup>−1</sup>) <a href="../Page/絕對庫侖.md" title="wikilink">abC</a></p></td>
<td><p>= (10<sup>−1</sup> <em>c</em>) <a href="../Page/靜庫侖.md" title="wikilink">Fr</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/電流.md" title="wikilink">電流</a></p></td>
<td><p><em>I</em></p></td>
<td><p>1 <a href="../Page/安培.md" title="wikilink">A</a></p></td>
<td><p>= (10<sup>−1</sup> <em>c</em>) statA</p></td>
<td><p>= (10<sup>−1</sup>) <a href="../Page/絕對安培.md" title="wikilink">abA</a></p></td>
<td><p>= (10<sup>−1</sup> <em>c</em>) Fr·s<sup>−1</sup></p></td>
</tr>
<tr class="odd">
<td><p>電動勢<br />
<a href="../Page/電壓.md" title="wikilink">電壓</a></p></td>
<td><p><em>φ</em><br />
<em>V</em></p></td>
<td><p>1 <a href="../Page/伏特.md" title="wikilink">V</a></p></td>
<td><p>= (10<sup>8</sup> <em>c</em><sup>−1</sup>) <a href="../Page/靜伏特.md" title="wikilink">statV</a></p></td>
<td><p>= (10<sup>8</sup>) <a href="../Page/絕對伏特.md" title="wikilink">abV</a></p></td>
<td><p>= (10<sup>8</sup> <em>c</em><sup>−1</sup>) <a href="../Page/靜伏特.md" title="wikilink">statV</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/電場.md" title="wikilink">電場</a></p></td>
<td><p><strong>E</strong></p></td>
<td><p>1 <a href="../Page/伏特.md" title="wikilink">V</a>/<a href="../Page/公尺.md" title="wikilink">m</a></p></td>
<td><p>= (10<sup>6</sup> <em>c</em><sup>−1</sup>) <a href="../Page/靜伏特.md" title="wikilink">statV</a>/<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
<td><p>= (10<sup>6</sup>) <a href="../Page/絕對伏特.md" title="wikilink">abV</a>/<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
<td><p>= (10<sup>6</sup> <em>c</em><sup>−1</sup>) <a href="../Page/靜伏特.md" title="wikilink">statV</a>/<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/磁場.md" title="wikilink">B場</a></p></td>
<td><p><strong>B</strong></p></td>
<td><p>1 <a href="../Page/特斯拉.md" title="wikilink">T</a></p></td>
<td><p>= (10<sup>4</sup> <em>c</em><sup>−1</sup>) statT</p></td>
<td><p>= (10<sup>4</sup>) <a href="../Page/高斯_(單位).md" title="wikilink">G</a></p></td>
<td><p>= (10<sup>4</sup>) <a href="../Page/高斯_(單位).md" title="wikilink">G</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/磁場.md" title="wikilink">H場</a></p></td>
<td><p><strong>H</strong></p></td>
<td><p>1 <a href="../Page/安培.md" title="wikilink">A</a>/<a href="../Page/公尺.md" title="wikilink">m</a></p></td>
<td><p>= (4π 10<sup>−3</sup> <em>c</em>) statA/<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
<td><p>= (4π 10<sup>−3</sup>) <a href="../Page/奥斯特_(电磁单位).md" title="wikilink">Oe</a></p></td>
<td><p>= (4π 10<sup>−3</sup>) <a href="../Page/奥斯特_(电磁单位).md" title="wikilink">Oe</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/磁矩.md" title="wikilink">磁矩</a></p></td>
<td><p><strong>μ</strong></p></td>
<td><p>1 <a href="../Page/安培.md" title="wikilink">A</a>·<a href="../Page/平方公尺.md" title="wikilink">m²</a></p></td>
<td><p>= (10<sup>3</sup> <em>c</em>) statA·cm²</p></td>
<td><p>= (10<sup>3</sup>) <a href="../Page/絕對安培.md" title="wikilink">abA</a>·cm²</p></td>
<td><p>= (10<sup>3</sup>) <a href="../Page/erg.md" title="wikilink">erg</a>/<a href="../Page/高斯_(單位).md" title="wikilink">G</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/磁通量.md" title="wikilink">磁通量</a></p></td>
<td><p><em>Φ<sub>m</sub></em></p></td>
<td><p>1 <a href="../Page/韦伯_(单位).md" title="wikilink">Wb</a></p></td>
<td><p>= (10<sup>8</sup> <em>c</em><sup>−1</sup>) statT·cm²</p></td>
<td><p>= (10<sup>8</sup>) <a href="../Page/馬克士威（單位）.md" title="wikilink">Mw</a></p></td>
<td><p>= (10<sup>8</sup>) <a href="../Page/高斯_(單位).md" title="wikilink">G</a>·cm²</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/電阻.md" title="wikilink">電阻</a></p></td>
<td><p><em>R</em></p></td>
<td><p>1 <a href="../Page/歐姆.md" title="wikilink">Ω</a></p></td>
<td><p>= (10<sup>9</sup> <em>c</em><sup>−2</sup>) <a href="../Page/秒.md" title="wikilink">s</a>/<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
<td><p>= (10<sup>9</sup>) <a href="../Page/绝对欧姆.md" title="wikilink">abΩ</a></p></td>
<td><p>= (10<sup>9</sup> <em>c</em><sup>−2</sup>) <a href="../Page/秒.md" title="wikilink">s</a>/<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/電阻率.md" title="wikilink">電阻率</a></p></td>
<td><p><em>ρ</em></p></td>
<td><p>1 <a href="../Page/歐姆.md" title="wikilink">Ω</a>·<a href="../Page/公尺.md" title="wikilink">m</a></p></td>
<td><p>= (10<sup>11</sup> <em>c</em><sup>−2</sup>) <a href="../Page/秒.md" title="wikilink">s</a></p></td>
<td><p>= (10<sup>11</sup>) <a href="../Page/绝对欧姆.md" title="wikilink">abΩ</a>·<a href="../Page/公分.md" title="wikilink">cm</a></p></td>
<td><p>= (10<sup>11</sup> <em>c</em><sup>−2</sup>) <a href="../Page/秒.md" title="wikilink">s</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/電容.md" title="wikilink">電容</a></p></td>
<td><p><em>C</em></p></td>
<td><p>1 <a href="../Page/法拉.md" title="wikilink">F</a></p></td>
<td><p>= (10<sup>−9</sup> <em>c</em><sup>2</sup>) <a href="../Page/公分.md" title="wikilink">cm</a></p></td>
<td><p>= (10<sup>−9</sup>) <a href="../Page/絕對法拉.md" title="wikilink">abF</a></p></td>
<td><p>= (10<sup>−9</sup> <em>c</em><sup>2</sup>) <a href="../Page/公分.md" title="wikilink">cm</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/電感.md" title="wikilink">電感</a></p></td>
<td><p><em>L</em></p></td>
<td><p>1 <a href="../Page/亨利_(单位).md" title="wikilink">H</a></p></td>
<td><p>= (10<sup>9</sup> <em>c</em><sup>−2</sup>) <a href="../Page/公分.md" title="wikilink">cm</a><sup>−1</sup>·<a href="../Page/秒.md" title="wikilink">s</a><sup>2</sup></p></td>
<td><p>= (10<sup>9</sup>) <a href="../Page/絕對亨利.md" title="wikilink">abH</a></p></td>
<td><p>= (10<sup>9</sup> <em>c</em><sup>−2</sup>) <a href="../Page/公分.md" title="wikilink">cm</a><sup>−1</sup>·<a href="../Page/秒.md" title="wikilink">s</a><sup>2</sup></p></td>
</tr>
</tbody>
</table>

此表中的*c* = 29,979,245,800 ≈
3·10<sup>10</sub>為厘米-克-秒制中的[光速](../Page/光速.md "wikilink")。
國際標準制下的庫侖常數*k<sub>C</sub>*可以表示為下式：

\[k_C=\frac{1}{4\pi\epsilon_0}=\frac{\mu_0 (c/100)^2}{4\pi}=10^{-7}\cdot 10^{-4}\cdot c^2 = 10^{-11}\cdot c^2 .\]
而靜電單位制下的*k<sub>C</sub>*=1，因此在靜電單位制下可以簡化一些物理量的單位。例如1 靜法拉 = 1 厘米，1 靜歐姆 = 1
秒/厘米。1 靜法拉的電容是半徑一厘米的球殼在真空介質下相對無窮遠處形成的電容。靜電單位制下，半徑分別為 *R* 及 *r*
的二個同心空心球殼所形成的電容為：

  -
    \(\frac{1}{\frac{1}{r}-\frac{1}{R}}\).

當*R*趨近無限大時上式簡化為*r*。

## 厘米-克-秒單位制下的物理常數

以下是一些用厘米-克-秒單位-{制}-表示的物理常數：

| 常數                                                                       | 符號                                                                                         | 數值                                                                                                                                            |
| ------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------- |
| [原子质量单位](../Page/原子质量单位.md "wikilink")                                   | u                                                                                          | 1.660 538 782 × 10<sup>−24</sup> [g](../Page/克.md "wikilink")                                                                                 |
| [波耳磁元](../Page/波耳磁元.md "wikilink")                                       | *μ*<sub>B</sub>                                                                            | 9.274 009 15 × 10<sup>−21</sup> [erg](../Page/爾格.md "wikilink")/[G](../Page/高斯_\(单位\).md "wikilink")〈EMU，高斯單位制〉                               |
| 2.780 278 00 × 10<sup>−10</sup> statA·cm<sup>2</sup> 〈ESU〉               |                                                                                            |                                                                                                                                               |
| [波耳半徑](../Page/波耳半徑.md "wikilink")                                       | *a*<sub>0</sub>                                                                            | 5.291 772 0859 × 10<sup>−9</sup> [cm](../Page/公分.md "wikilink")                                                                               |
| [波茲曼常數](../Page/波茲曼常數.md "wikilink")                                     | *k*                                                                                        | 1.380 6504 × 10<sup>−16</sup> [erg](../Page/爾格.md "wikilink")/[K](../Page/绝对温度.md "wikilink")                                                 |
| [電子質量](../Page/電子.md "wikilink")                                         | *m*<sub>e</sub>                                                                            | 9.109 382 15 × 10<sup>−28</sup> [g](../Page/克.md "wikilink")                                                                                  |
| [基本电荷](../Page/基本电荷.md "wikilink")                                       | *e*                                                                                        | 4.803 204 27 × 10<sup>−10</sup> [Fr](../Page/靜庫侖.md "wikilink") 〈ESU，高斯單位制〉                                                                   |
| 1.602 176 487 × 10<sup>−20</sup> [abC](../Page/絕對庫侖.md "wikilink") 〈EMU〉 |                                                                                            |                                                                                                                                               |
| [精细结构常数](../Page/精细结构常数.md "wikilink")                                   | *α* ≈ 1/137                                                                                | 7.297 352 570 × 10<sup>−3</sup>                                                                                                               |
| [万有引力常数](../Page/万有引力常数.md "wikilink")                                   | *G*                                                                                        | 6.674 28 × 10<sup>−8</sup> [cm](../Page/公分.md "wikilink")<sup>3</sup>/([g](../Page/克.md "wikilink")·[s](../Page/秒.md "wikilink")<sup>2</sup>) |
| [普朗克常数](../Page/普朗克常数.md "wikilink")                                     | *h*                                                                                        | 6.626 068 85 × 10<sup>−27</sup> [erg](../Page/爾格.md "wikilink")·[s](../Page/秒.md "wikilink")                                                  |
| *\(\hbar\)*                                                              | 1.054 5716 × 10<sup>−27</sup> [erg](../Page/爾格.md "wikilink")·[s](../Page/秒.md "wikilink") |                                                                                                                                               |
| [真空中的光速](../Page/光速.md "wikilink")                                       | *c*                                                                                        | ≡ 2.997 924 58 × 10<sup>10</sup> [cm](../Page/公分.md "wikilink")/[s](../Page/秒.md "wikilink")                                                  |

以厘米-克-秒單位制表示的常見物理常數\[20\]

## 優點及缺點

厘米-克-秒制的優點是有些電磁學定理在特定單位制變體下可以簡化其係數，有助於計算，但其中一些單位很難用實驗加以定義，是厘米-克-秒制的缺點。厘米-克-秒制有時會用emu表示電磁單位制下的物理量單位，esu表示靜電制下的物理量單位，因此15emu可能表示15[絕對伏特](../Page/絕對伏特.md "wikilink")、15emu單位的[電偶極矩或](../Page/電偶極矩.md "wikilink")[磁化率](../Page/磁化率.md "wikilink")，需根據前後文判斷其物理量，容易造成誤解。

相對的，國際單位制使用[安培為電流的基本單位](../Page/安培.md "wikilink")，較容易用實驗加以定義，但電磁學定理的係數會比較複雜。國際單位制的單位都有獨一無二的名稱，因此不容易出現誤解。

[高斯單位制的特點是電場及磁場的單位相同](../Page/高斯單位制.md "wikilink")，\(4\pi\epsilon_0\)的常數變成\(1\)，方程式中唯一有量綱的常數為[光速](../Page/光速.md "wikilink")。[勞侖茲-黑維塞單位制也有類似的特質](../Page/勞侖茲-黑維塞單位制.md "wikilink")（\(\epsilon_0\)為\(1\)），但此單位制和國際單位制一樣都是「有理化單位制」，[馬克士威方程組中沒有](../Page/馬克士威方程組.md "wikilink")\(4 \pi\)的因子，而[庫侖定律中有](../Page/庫侖定律.md "wikilink")\(4 \pi\)的因子，使用勞侖茲-黑維塞單位制時，可以使馬克士威方程組有最簡單的形式。

國際單位制（及其他「有理化單位制」）的選擇是使關於球面的電磁方程式會含有4π，關於線圈的則含有2π，處理直導線的則完全不含π，這樣的作法對電機工程應用來說是最便利的。高斯單位制會使得關於球面的電磁方程式中不含4π或π，在一些領域中關於球面的式子佔主要比例（例如：[天文學](../Page/天文學.md "wikilink")），有些論點認為高斯單位制在符號標記上其實還更方便些。

## 參考資料

## 參閱

  - [高斯單位制](../Page/高斯單位制.md "wikilink")
  - [计量单位](../Page/计量单位.md "wikilink")
  - [国际单位制](../Page/国际单位制.md "wikilink")

{{-}}

[Category:米制](../Category/米制.md "wikilink")
[厘米-克-秒制](../Category/厘米-克-秒制.md "wikilink")
[Category:计量单位制](../Category/计量单位制.md "wikilink")

1.  {{ cite book | first1 = William | last1 = Hallock | first2 = Herbert
    Treadwell | last2 = Wade | page = 200 | year = 1906 | place = New
    York | title = Outlines of the evolution of weights and measures and
    the metric system | publisher = The Macmillan Co | url =
    <http://books.google.com/?id=NVZKAAAAMAAJ> }}

2.

3.
4.  {{ cite book | author = Cardarelli, F. | year = 2004 | title =
    Encyclopaedia of Scientific Units, Weights and Measures: Their SI
    Equivalences and Origins | publisher = Springer | edition = 2nd |
    page = 20 | isbn= 1-8523-3682-X | url=
    <http://books.google.com/?id=6KCx8Ww75VkC> }}

5.
6.
7.
8.
9.
10.
11.
12. {{ cite journal | author = Leung, P. T. | title = A note on the
    'system-free' expressions of Maxwell's equations | year = 2004 |
    journal = European Journal of Physics | volume = 25 | issue = 2 |
    pages = N1–N4 | doi = 10.1088/0143-0807/25/2/N01}}

13. {{ cite book | author = Cardarelli, F. | year = 2004 | title =
    Encyclopaedia of Scientific Units, Weights and Measures: Their SI
    Equivalences and Origins | publisher = Springer | edition = 2nd |
    pages = 20–25 | isbn= 1-8523-3682-X | url=
    <http://books.google.com/?id=6KCx8Ww75VkC> }}

14.
15.
16.
17.
18. {{ cite journal | author = Bennett, L. H.; Page, C. H.; and
    Swartzendruber, L. J. | title = Comments on units in magnetism |
    year = 1978 | journal = Journal of Research of the National Bureau
    of Standards | volume = 83 | issue = 1 | pages = 9–12 | doi = }}

19.
20.