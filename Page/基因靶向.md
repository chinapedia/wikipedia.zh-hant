**基因標的**（，又称为**基因標靶**）是一种利用[同源重组方法改变生物体某一](../Page/同源重组.md "wikilink")[内源](../Page/内源.md "wikilink")[基因的](../Page/基因.md "wikilink")[遗传学技术](../Page/遗传学.md "wikilink")。这一技术可以用于删除某一基因、去除[外显子或导入](../Page/外显子.md "wikilink")[点突变](../Page/点突变.md "wikilink")，从而可以对此基因的功能进行研究。基因標的的效果可以是持续的，也可以是条件化的。例如，条件可以是生物体[发育或整个生命过程中的一个特定时刻](../Page/发育生物学.md "wikilink")，或将效应限制在某一特定[组织中](../Page/组织_\(生物学\).md "wikilink")。基因標的的优点在于可以应用于任何基因，无需考虑其大小和[转录活性](../Page/转录.md "wikilink")。

## 历史

基因標的技术是从20世纪80年代发展起来的，是建立在对同源重组不断了解的基础之上。首先是以[酵母这种较为简单的](../Page/酵母.md "wikilink")[真核](../Page/真核.md "wikilink")[模式生物为基础](../Page/模式生物.md "wikilink")，来对相关技术进行研究。\[1\]随着外源DNA导入酵母[细胞方法的建立](../Page/细胞.md "wikilink")、标记基因有效选择的利用、同源重组[转化子筛选系统的建立](../Page/转化子.md "wikilink")、[载体同源序列DNA](../Page/载体.md "wikilink")（靶标）双链断裂提高同源重组效率的利用，使得基因打靶技术逐渐成熟起来。\[2\]而对于[哺乳动物细胞](../Page/哺乳动物.md "wikilink")，由于其基因组比酵母细胞要大且复杂，基因打靶的成功率很低。因此要将基因打靶技术应用于[哺乳动物](../Page/哺乳动物.md "wikilink")，还需要新的策略。1989年，利用[胚胎](../Page/胚胎.md "wikilink")[干细胞](../Page/干细胞.md "wikilink")，美国科学家[马里奥·卡佩奇和](../Page/马里奥·卡佩奇.md "wikilink")[奥利弗·史密斯与英国科学家](../Page/奥利弗·史密斯.md "wikilink")[马丁·埃文斯](../Page/马丁·埃文斯.md "wikilink")，将基因打靶技术成功地应用于[小鼠](../Page/小鼠.md "wikilink")；他们也因此获得了2007年[诺贝尔生理学与医学奖](../Page/诺贝尔生理学与医学奖.md "wikilink")。\[3\]

## 原理和方法

将[外源](../Page/外源.md "wikilink")[DNA导入標靶细胞后](../Page/DNA.md "wikilink")，利用基因重组，将外源DNA与標靶细胞内染色体上[同源DNA间进行重组](../Page/同源.md "wikilink")，从而将外源DNA定点整合入標靶细胞基因组上某一确定的位点。\[4\]

对于不同的生物体，所使用的基因打靶的方法也不用。对于[小鼠来说](../Page/小鼠.md "wikilink")，大致的流程如下：\[5\]

1.  从小鼠胚胎上提取干细胞；
2.  同时，构建標靶载体，標靶载体中含有与靶基因同源的DNA片段；
3.  将標靶载体[转染到干细胞中](../Page/转染.md "wikilink")；
4.  将筛选后获得的含有靶载体的干细胞进行扩增；
5.  将含有靶载体的干细胞注入小鼠胚胎；
6.  胚胎发育为[嵌合體小鼠](../Page/嵌合體.md "wikilink")（部分器官为该改造后的干细胞发育而成）后，通过选择性培育（将嵌合體小鼠与正常小鼠交配，在下一代中进行筛选），获得[基因剔除小鼠](../Page/基因剔除小鼠.md "wikilink")（全部器官为该改造后的干细胞发育而成）。

修改后的方法还可用于其他哺乳动物，如[牛](../Page/牛.md "wikilink")、[羊](../Page/羊.md "wikilink")、[猪等](../Page/猪.md "wikilink")。

基因標的技术还被用于一些[植物](../Page/植物.md "wikilink")，特别是[小立碗藓的研究](../Page/小立碗藓.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 参见

  - [遗传学](../Page/遗传学.md "wikilink")
  - [同源重组](../Page/同源重组.md "wikilink")
  - [基因剔除](../Page/基因剔除.md "wikilink")、[基因剔除小鼠](../Page/基因剔除小鼠.md "wikilink")
  - [小立碗藓](../Page/小立碗藓.md "wikilink")
  - [Toll样受体](../Page/Toll样受体.md "wikilink")

## 外部链接

  - [guide to gene
    targeting](http://cancer.ucsd.edu/Research/Shared/tgm/genetargeting.asp)

  - [outline of gene
    targeting](http://www.med.umich.edu/tamc/esoutline.html)

  - [gene targeting diagram &
    summary](https://web.archive.org/web/20131002010529/http://141.217.91.198/knockout.htm)

  - [research highlights on reporter
    genes](http://reportergene.blogspot.com) used in gene targeting

[Category:分子生物学](../Category/分子生物学.md "wikilink")
[Category:基因工程](../Category/基因工程.md "wikilink")
[Category:生物工程](../Category/生物工程.md "wikilink")
[Category:遗传修饰生物体](../Category/遗传修饰生物体.md "wikilink")

1.

2.

3.

4.

5.