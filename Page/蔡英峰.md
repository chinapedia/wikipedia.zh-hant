**蔡英峰**（），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾效力於[中華職棒](../Page/中華職棒.md "wikilink")[La
New熊](../Page/La_New熊.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")。

涉入[2009年中華職棒假球事件遭檢方調查](../Page/2009年中華職棒假球事件.md "wikilink")，坦承擔任雨刷集團白手套收買球員，遭La
New熊開除。

## 經歷

  - [台南市](../Page/台南市.md "wikilink")[立人國小少棒隊](../Page/立人國小.md "wikilink")
  - [台南市](../Page/台南市.md "wikilink")[大成國中青少棒隊](../Page/大成國中.md "wikilink")
  - [台南市](../Page/台南市.md "wikilink")[南英商工青棒隊](../Page/南英商工.md "wikilink")
  - [台灣體院棒球隊](../Page/台灣體院.md "wikilink")（富邦公牛）
  - [La New熊隊代訓球員](../Page/La_New熊.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[La
    New熊隊](../Page/La_New熊.md "wikilink")

## 職棒生涯成績

|       |                                          |     |    |    |    |    |    |    |    |     |     |       |      |
| ----- | ---------------------------------------- | --- | -- | -- | -- | -- | -- | -- | -- | --- | --- | ----- | ---- |
| 年度    | 球隊                                       | 出賽  | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死 | 三振  | 責失  | 投球局數  | 防禦率  |
| 2006年 | [La New熊](../Page/La_New熊.md "wikilink") | 26  | 11 | 5  | 1  | 2  | 0  | 0  | 26 | 47  | 41  | 109.2 | 3.37 |
| 2007年 | [La New熊](../Page/La_New熊.md "wikilink") | 35  | 4  | 3  | 2  | 7  | 0  | 0  | 26 | 38  | 33  | 81.2  | 3.64 |
| 2008年 | [La New熊](../Page/La_New熊.md "wikilink") | 21  | 1  | 4  | 1  | 1  | 0  | 0  | 16 | 24  | 18  | 38.0  | 4.26 |
| 2009年 | [La New熊](../Page/La_New熊.md "wikilink") | 18  | 3  | 3  | 0  | 0  | 0  | 0  | 11 | 34  | 24  | 53.0  | 4.08 |
| 合計    | 4年                                       | 100 | 19 | 15 | 4  | 10 | 0  | 0  | 79 | 143 | 116 | 282.1 | 3.70 |

## 特殊紀錄

## 外部資源

[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:Lamigo桃猿隊球員](../Category/Lamigo桃猿隊球員.md "wikilink")
[Category:臺南市立大成國民中學校友](../Category/臺南市立大成國民中學校友.md "wikilink")
[Category:南英高級商工職業學校校友](../Category/南英高級商工職業學校校友.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")
[Category:蔡姓](../Category/蔡姓.md "wikilink")