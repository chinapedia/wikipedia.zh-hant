**西法蘭克王國**（[法語](../Page/法語.md "wikilink")：****）為[西歐的一個](../Page/西歐.md "wikilink")[君主制國家](../Page/君主制.md "wikilink")，存在時間為843年至987年。

## 建立

843年，[法蘭克國王](../Page/法蘭克國王列表.md "wikilink")[虔誠者路易的三個兒子](../Page/路易一世_\(虔誠者\).md "wikilink")，[洛泰爾](../Page/洛泰爾.md "wikilink")、[日耳曼人路易及](../Page/日耳曼人路易.md "wikilink")[禿頭查理簽署](../Page/禿頭查理.md "wikilink")《[凡爾登條約](../Page/凡爾登條約.md "wikilink")》，把[查理曼遺留下來的](../Page/查理曼.md "wikilink")[法蘭克王國一分為三](../Page/法蘭克王國.md "wikilink")。禿頭查理得到王國西部的領土，並建立君主制國家，稱西法蘭克王國。而法蘭克王國[東部和](../Page/東法蘭克王國.md "wikilink")[中部的領土則分別由日耳曼人路易及洛泰爾繼承](../Page/中法蘭克王國.md "wikilink")。西法蘭克王國的領土包括了[阿基坦](../Page/阿基坦.md "wikilink")、[布列塔尼](../Page/布列塔尼.md "wikilink")、[勃艮第](../Page/勃艮第.md "wikilink")、[加泰羅尼亞](../Page/加泰羅尼亞.md "wikilink")、[法蘭德斯](../Page/法蘭德斯.md "wikilink")、[加斯科涅](../Page/加斯科涅.md "wikilink")（）、[普羅旺斯](../Page/普羅旺斯.md "wikilink")、[圖盧茲及](../Page/圖盧茲.md "wikilink")[法蘭西島](../Page/法蘭西島.md "wikilink")（）等。

## 發展

870年，禿頭查理及日耳曼人路易再次勘定邊界，簽署《[墨爾森條約](../Page/墨爾森條約.md "wikilink")》，瓜分洛泰爾子嗣遺留下來的中法蘭克王國。在這份條約下，西法蘭克王國得以瓜分中法蘭克王國西部領土，這逐漸形成[法蘭西王國版圖的雛形](../Page/法蘭西王國.md "wikilink")。

雖然原來法蘭克王國的[加洛林王朝得以繼續延續](../Page/加洛林王朝.md "wikilink")，但境內的大家族卻逐漸崛起，使得王權旁落。9世紀中葉，[北歐的](../Page/北歐.md "wikilink")[諾曼人侵擾](../Page/諾曼人.md "wikilink")[西歐](../Page/西歐.md "wikilink")、[不列顛一帶](../Page/不列顛.md "wikilink")，西法蘭克王國於是派出[強者羅貝爾](../Page/強者羅貝爾.md "wikilink")（）抗擊諾曼人，並成功阻止他們的入侵。爾後，羅貝爾的兒子巴黎伯爵[厄德同樣抗擊諾曼人有功](../Page/厄德.md "wikilink")，令他領導的[卡佩家族聲望日隆](../Page/卡佩家族.md "wikilink")，而厄德也曾經被推舉為王。這使得加洛林王室和卡佩家族進行長期的鬥爭，以爭奪西法蘭克的王位。

## 終結

987年，卡洛林王朝的[路易五世去世](../Page/路易五世_\(西法蘭克\).md "wikilink")，由於他膝下無嗣，使得西法蘭克王國的卡洛林王朝絕嗣。其後，神職人員及貴族們推舉卡佩家族的繼承人[雨果·卡佩為西法蘭克國王](../Page/雨果·卡佩.md "wikilink")。雨果·卡佩建立起[卡佩王朝](../Page/卡佩王朝.md "wikilink")，西法蘭克王國從此由[法蘭西王國所取代](../Page/法蘭西王國.md "wikilink")。

## 相關條目

  - [法蘭克王國](../Page/法蘭克王國.md "wikilink")
  - [凡爾登條約](../Page/凡爾登條約.md "wikilink")
  - [卡洛林王朝](../Page/卡洛林王朝.md "wikilink")
  - [卡佩王朝](../Page/卡佩王朝.md "wikilink")

## 參考資料

<div class="references-small">

  -

</div>

[\*](../Category/西法兰克王国.md "wikilink")
[FW](../Category/已不存在的歐洲君主國.md "wikilink")
[FW](../Category/王國.md "wikilink")