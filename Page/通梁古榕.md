[Taiwan-Penghu-Tongliang-Great-Banyan.JPG](https://zh.wikipedia.org/wiki/File:Taiwan-Penghu-Tongliang-Great-Banyan.JPG "fig:Taiwan-Penghu-Tongliang-Great-Banyan.JPG")
[保安宮400多年_澎湖縣_Venation_1.JPG](https://zh.wikipedia.org/wiki/File:保安宮400多年_澎湖縣_Venation_1.JPG "fig:保安宮400多年_澎湖縣_Venation_1.JPG")

**通梁古榕**為一株植于[明朝](../Page/明朝.md "wikilink")[永曆](../Page/永曆.md "wikilink")27年（公元1673年）、樹齡達{{\#expr:
-1673}}岁、[氣根近百條的古](../Page/氣根.md "wikilink")[白榕](../Page/白榕.md "wikilink")（[学名](../Page/学名.md "wikilink")：）。位於[台灣](../Page/台灣.md "wikilink")[澎湖縣](../Page/澎湖縣.md "wikilink")[白沙鄉通梁村保安宮前廣場](../Page/白沙鄉_\(台灣\).md "wikilink")。地近聞名的[澎湖跨海大橋附近](../Page/澎湖跨海大橋.md "wikilink")，與[西嶼鄉橫礁村隔橋相鄰](../Page/西嶼鄉.md "wikilink")。

## 緣起與沿革

考其古榕緣起，回溯到[明朝](../Page/明朝.md "wikilink")[永曆](../Page/永曆.md "wikilink")27年(1673年)由村民林瑤琴氏所植。當地人並於樹旁捐款蓋一[廟謂之保安宮](../Page/廟.md "wikilink")，宮內奉祀康府王爺。並於宮前古榕主幹處立碑述其緣起。樹下設有石椅供遊客及當地居民聊天休憩，其旁為一排販賣特產及飲食之商家。

## 古榕特色

其主要特色在於近百條的氣根以橫向往四方伸展，形成一蓬架型的覆蓋結構。覆蓋面積達2000多平方[公尺之大](../Page/公尺.md "wikilink")，亦緣於樹種屬氣根成長茂盛型的白榕。加之澎湖土地貧瘠，甚至氣候一到冬天吹來威力強大的[東北季風](../Page/東北季風.md "wikilink")。本屬草木不易生長之澎湖能有此奇樹，令人蔚為奇觀。　

## 參考文獻

1.  陳永森；林孟龍/著，台灣的國家風景區，2004年2月ISBN 9867630211

## 相關項目

  - [澎湖跨海大橋](../Page/澎湖跨海大橋.md "wikilink")
  - [榕亞屬](../Page/榕亞屬.md "wikilink")

## 外部連結

  - [澎湖縣白沙鄉公所](http://www.baisha.gov.tw/)
  - [通樑古榕旅遊景點介紹](http://travel.network.com.tw/tourguide/point/showpage/173.html)
  - [澎湖本島北環之旅-通梁古榕](http://ezph.myweb.hinet.net/chinese/play/bd-oldtree.htm)
  - [澎湖之旅14](http://www.ttvs.cy.edu.tw/kcc/90penfu/penfu14.htm)
  - [澎湖國家風景區網站](http://www.penghu-nsa.gov.tw/)

[category:澎湖縣旅遊景點](../Page/category:澎湖縣旅遊景點.md "wikilink")

[白沙鄉](../Category/白沙鄉_\(臺灣\).md "wikilink")
[Category:澎湖縣建築物](../Category/澎湖縣建築物.md "wikilink")
[Category:澎湖縣廟宇](../Category/澎湖縣廟宇.md "wikilink")