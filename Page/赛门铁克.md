**赛门铁克公司**（，）是一家總部設於[美国](../Page/美国.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[山景城的互联网安全技术厂商](../Page/山景城_\(加利福尼亚州\).md "wikilink")，創立於1982年3月1日在全球有40個國家設有分公司，在《[财富](../Page/財星_\(雜誌\).md "wikilink")》500强企业名单中名列第391位，全球拥有20,500多名员工。
2015年8月11日賽門鐵克以80億美元出售旗下的資訊管理業務Veritas給由The Carlyle
Group、GIC與其他投資人共組的投資集團，於2016年1月1日完成交易。

## 主要產品

  - **電腦安全軟體**
      - [诺顿网络安全大師](../Page/Norton_Internet_Security.md "wikilink") -
        網路安全套裝
      - [诺顿防毒软件](../Page/Norton_AntiVirus.md "wikilink") - 防毒軟體
      - [诺顿360](../Page/Norton_360.md "wikilink") - 包括系統防護身分安全和備份還原及電腦整理
      - [Symantec Endpoint
        Protection](../Page/Symantec_Endpoint_Protection.md "wikilink")
        - 企業專用產品
  - **備份及還原軟體**
      - [諾頓魅影系統](../Page/Norton_Ghost.md "wikilink")
      - [諾頓線上備份大師](../Page/Norton_Online_Backup.md "wikilink")
      - [諾頓360](../Page/Norton_360.md "wikilink") - 包括系統防護身分安全和備份還原及電腦整理
  - **電腦整理軟體**
      - [諾頓電腦醫生](../Page/Norton_Utilities.md "wikilink")
      - [諾頓360](../Page/Norton_360.md "wikilink") - 包括系統防護身分安全和備份還原及電腦整理
  - **磁碟分區軟體**
      - [Norton
        PartitionMagic](../Page/Norton_PartitionMagic.md "wikilink")
  - **网站信任检测**
      - [Norton Verisign](../Page/Verisign.md "wikilink")
  - **密碼管理解決方案商**
      - [PasswordBank](../Page/PasswordBank.md "wikilink")
  - **备份和还原解决方案商**
      - [Symantec NetBackup](../Page/Symantec_NetBackup.md "wikilink")
      - [Symantec Backup
        exec](../Page/Symantec_Backup_exec.md "wikilink")
  - **维护管理工具**
      - [Symantec Operations Readiness
        Tools](../Page/Symantec_Operations_Readiness_Tools.md "wikilink")
        - 帮助系统管理员识别数据中心风险，提高运维效率，从而使之能够管理和控制由数据中心构架和规模引起的复杂性。

## 历史

  - 2015年8月12日，賽門鐵克以80億美元將旗下的資訊管理業務Veritas华睿泰出售給由The Carlyle
    Group、GIC與其他投資人共組的投資集團

<!-- end list -->

  - 2016年6月13日，賽門鐵克以46.5億美元收購網絡安全公司Blue Coat System Inc。

<!-- end list -->

  - 2016年11月20日，賽門鐵克以23億美元收購防盜軟件商LifeLock

<!-- end list -->

  - 2017年3月，谷歌认为由于赛门铁克
    CA（证书签发机构）签发了3万多个有问题的证书，所以Chrome浏览器将逐步减少对赛门铁克证书的信任。\[1\]

<!-- end list -->

  - 2017年8月2日，[DigiCert以](../Page/DigiCert.md "wikilink")9.5亿美元现金和DigiCert业务的30%股权收购[赛门铁克安全认证业务](../Page/赛门铁克.md "wikilink")

## 参见

  -
  -
  -
## 参考文献

## 外部連結

  -
{{-}}

[赛门铁克](../Category/赛门铁克.md "wikilink")
[Category:计算机安全公司](../Category/计算机安全公司.md "wikilink")
[Category:计算机安全软件公司](../Category/计算机安全软件公司.md "wikilink")
[Category:內容管制軟體](../Category/內容管制軟體.md "wikilink")
[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:前证书颁发机构](../Category/前证书颁发机构.md "wikilink")
[Category:山景城公司](../Category/山景城公司.md "wikilink")
[Category:納斯達克上市公司](../Category/納斯達克上市公司.md "wikilink")

1.