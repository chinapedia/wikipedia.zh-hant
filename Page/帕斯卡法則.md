**帕斯卡法則**是[組合數學上的一個關於](../Page/組合數學.md "wikilink")[二項式係數的](../Page/二項式係數.md "wikilink")[恆等式](../Page/恆等式.md "wikilink")。它說明對於正[整數](../Page/整數.md "wikilink")\(n\),\(k\)（\(k \le n\)），

  -
    \({n-1\choose k} + {n-1\choose k-1} = {n\choose k}\)。

## 組合數學上的意義和證明

\({n\choose k}\)表示在有\(n\)個元素的集內，有\(k\)個元素的子集的數目。其實這些子集之中，可分為包含第一個元素的和不含第一個元素的。包含第一個元素的子集有\({n-1\choose k-1}\)個，不含的有\({n-1\choose k}\)個。

## 代數證明

\[{ n-1 \choose k } + { n-1 \choose k-1 } = { n \choose k }\]

重寫左邊為

  -
    <math>

\\begin{align} & {} \\frac{(n-1)\!}{k\!(n-k-1)\!} +
\\frac{(n-1)\!}{(k-1)\!(n-k))\!} \\\\ & =
\\frac{(n-k)(n-1)\!}{(n-k-1)\!k\!(n-k)}+\\frac{k(n-1)\!}{k(k-1)\!(n-k)\!}
\\\\ & = \\frac{(n-k)(n-1)\!+k(n-1)\!}{k\!(n-k)\!} \\\\ & =
\\frac{(n-1)\!n}{k\!(n-k)\!} \\\\ & = \\frac{n\!}{k\!(n-k)\!} \\\\ & = {
n \\choose k } \\end{align} </math>

## 推广

设\(n, k_1, k_2, k_3,\dots ,k_p, p \in \mathbb{N}^* \,\!\)及\(n=k_1+k_2+k_3+ \cdots +k_p \,\!\)。那么：

  -
    <math>

\\begin{align} & {} \\quad {n-1\\choose k_1-1,k_2,k_3, \\dots,
k_p}+{n-1\\choose k_1,k_2-1,k_3,\\dots, k_p}+\\cdots+{n-1\\choose
k_1,k_2,k_3,\\dots,k_p-1} \\\\ & =
\\frac{(n-1)\!}{(k_1-1)\!k_2\!k_3\! \\cdots k_p\!} +
\\frac{(n-1)\!}{k_1\!(k_2-1)\!k_3\!\\cdots k_p\!} + \\cdots +
\\frac{(n-1)\!}{k_1\!k_2\!k_3\! \\cdots (k_p-1)\!} \\\\ & =
\\frac{k_1(n-1)\!}{k_1\!k_2\!k_3\! \\cdots k_p\!} +
\\frac{k_2(n-1)\!}{k_1\!k_2\!k_3\! \\cdots k_p\!} + \\cdots +
\\frac{k_p(n-1)\!}{k_1\!k_2\!k_3\! \\cdots k_p\!} \\\\ & =
\\frac{(k_1+k_2+\\cdots+k_p) (n-1)\!}{k_1\!k_2\!k_3\!\\cdots
k_p\!} \\\\ & = \\frac{n(n-1)\!}{k_1\!k_2\!k_3\! \\cdots k_p\!}
\\\\ & = \\frac{n\!}{k_1\!k_2\!k_3\! \\cdots k_p\!} \\\\ & =
{n\\choose k_1, k_2, k_3, \\dots , k_p} \\end{align} </math>

## 参见

  - [杨辉三角形](../Page/杨辉三角形.md "wikilink")

[ru:Закон Паскаля](../Page/ru:Закон_Паскаля.md "wikilink")

[Category:组合数学](../Category/组合数学.md "wikilink")
[Category:数学恒等式](../Category/数学恒等式.md "wikilink")