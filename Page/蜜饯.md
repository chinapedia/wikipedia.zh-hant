[Candied_Fruit_-_La_Boquería.jpg](https://zh.wikipedia.org/wiki/File:Candied_Fruit_-_La_Boquería.jpg "fig:Candied_Fruit_-_La_Boquería.jpg")
**蜜饯**（），也称**果脯**，[閩南語稱之為](../Page/閩南語.md "wikilink")**鹹酸甜**（）\[1\]，是以桃、杏、梨、枣、冬瓜、生薑或果仁等为原料，用[糖或](../Page/糖.md "wikilink")[蜂蜜醃漬后而加工製成的食品](../Page/蜂蜜.md "wikilink")。除了作為[小吃或](../Page/小吃.md "wikilink")[零食直接食用外](../Page/零食.md "wikilink")，蜜餞也可以用來放於[蛋糕](../Page/蛋糕.md "wikilink")、[饼干等点心上作为点缀](../Page/饼干.md "wikilink")。传说果脯是明朝时期的御膳房发明的\[2\]。

## 品种

  - 糖渍蜜饯类：原料经糖渍蜜制后，成品浸渍在一定浓度的糖液中，略有透明感，如：蜜[金桔](../Page/金桔.md "wikilink")、糖[桂花](../Page/桂花.md "wikilink")、化皮[榄等](../Page/榄.md "wikilink")。
  - 返砂类：原料经糖渍糖煮后，成品表面乾燥，附有白色糖霜，如：糖[冬瓜](../Page/冬瓜.md "wikilink")、[金丝蜜枣](../Page/金丝蜜枣.md "wikilink")、[金桔饼等](../Page/金桔饼.md "wikilink")。
  - 果脯类：原料以糖渍糖制后，经过乾燥，成品表面不粘不燥，有透明感，无糖霜析出，如：[杏脯](../Page/杏脯.md "wikilink")、糖[菠萝](../Page/菠萝.md "wikilink")(片、块、芯)、[糖薑](../Page/糖薑.md "wikilink")、[糖木瓜](../Page/糖木瓜.md "wikilink")（条、粒）等。
  - [涼果類](../Page/涼果.md "wikilink")：原料在糖漬或糖煮過程中，添加甜味劑、香料等，成品表面呈乾態，具有濃郁香味，如：[丁香李雪花應子](../Page/嘉應子.md "wikilink")、[八珍梅](../Page/八珍梅.md "wikilink")、[梅味金桔等](../Page/梅味金桔.md "wikilink")。
  - [甘草製品](../Page/甘草.md "wikilink")：原料採用果脯，配以糖、甘草和其他食品添加劑浸漬處理後，進行乾燥，成品有甜、酸、鹹等風味，如[話梅](../Page/話梅.md "wikilink")、[甘草欖](../Page/甘草欖.md "wikilink")、九制[陳皮](../Page/陳皮.md "wikilink")、[話李](../Page/話李.md "wikilink")、[甘草杏等](../Page/甘草杏.md "wikilink")。
  - [果糕](../Page/果糕.md "wikilink")：原料加工成酱状，经浓缩干燥，成品呈片、条、块等形状，如：[山楂糕](../Page/山楂糕.md "wikilink")、[开胃金桔](../Page/开胃金桔.md "wikilink")、[果丹皮等](../Page/果丹皮.md "wikilink")。

## 参考资料

[蜜餞](../Category/蜜餞.md "wikilink")

1.  [台灣閩南語常用詞辭典](http://twblg.dict.edu.tw/holodict_new/index.html)
2.