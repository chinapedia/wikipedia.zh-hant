**北凉**（397年或401年－439年）是[十六国之一](../Page/十六国.md "wikilink")。由[匈奴支系](../Page/匈奴.md "wikilink")[盧水胡族的首領](../Page/盧水胡.md "wikilink")[沮渠蒙逊所建立](../Page/沮渠蒙逊.md "wikilink")；另有一種看法認為建立者為[段業](../Page/段業.md "wikilink")，此說是以蒙遜堂兄[沮渠男成擁立段業稱涼州牧](../Page/沮渠男成.md "wikilink")，並改元[神璽為立國之始](../Page/神璽.md "wikilink")（397年）。

## 歷史

401年蒙遜誣男成謀反，段業斬男成，蒙遜以此為藉口攻滅段業，仍稱涼州牧，改元[永安](../Page/永安_\(沮渠蒙逊\).md "wikilink")，因此亦有人以此為北涼立國之時。

北涼[首都为](../Page/首都.md "wikilink")[张掖](../Page/张掖.md "wikilink")，蒙遜自称张掖公。412年迁都姑臧（今甘肃[武威](../Page/武威.md "wikilink")），称河西王。最强盛的时候控制今[甘肃西部](../Page/甘肃.md "wikilink")、[宁夏](../Page/宁夏.md "wikilink")、[新疆](../Page/新疆.md "wikilink")、[青海的一部分](../Page/青海.md "wikilink")，是河西一帶最強大的勢力。420年灭[西凉](../Page/西凉.md "wikilink")。433年蒙逊去世，其子[沮渠牧犍继位](../Page/沮渠牧犍.md "wikilink")。439年[北魏大军围攻](../Page/北魏.md "wikilink")[姑臧](../Page/姑臧.md "wikilink")，[沮渠牧犍出降](../Page/沮渠牧犍.md "wikilink")，北涼亡，北魏統一華北。

後牧犍弟[沮渠無諱西行至](../Page/沮渠無諱.md "wikilink")[高昌](../Page/高昌.md "wikilink")，建立高昌北涼，一般認為已脫離五胡十六國時代之範圍，460年高昌北涼為[柔然所攻滅](../Page/柔然.md "wikilink")，無諱弟[沮渠安周被殺](../Page/沮渠安周.md "wikilink")，高昌北涼亦亡。

**北涼**

  - 君主头衔：

<!-- end list -->

  - 凉[州牧](../Page/州牧.md "wikilink")、建康公397年5月－399年2月
  - [凉王](../Page/凉王.md "wikilink")399年2月－401年6月
  - [张掖公](../Page/张掖郡.md "wikilink")401年6月－412年11月
  - 河西王412年11月－431年9月
  - 凉王431年9月－433年4月
  - 河西王433年4月－439年9月

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p><a href="../Page/廟號.md" title="wikilink">廟號</a></p></th>
<th><p><a href="../Page/諡號.md" title="wikilink">諡號</a></p></th>
<th><p>統治時間</p></th>
<th><p><a href="../Page/年號.md" title="wikilink">年號</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/段業.md" title="wikilink">段業</a></p></td>
<td><p>無</p></td>
<td><p>無</p></td>
<td><p>397年-401年</p></td>
<td><p><a href="../Page/神璽.md" title="wikilink">神璽</a> 397年-399年<br />
<a href="../Page/天璽_(北涼).md" title="wikilink">天璽</a> 399年-401年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沮渠蒙遜.md" title="wikilink">沮渠蒙遜</a></p></td>
<td><p>太祖</p></td>
<td><p>武宣王</p></td>
<td><p>401年-433年</p></td>
<td><p><a href="../Page/永安_(北涼).md" title="wikilink">永安</a> 401年-412年<br />
<a href="../Page/玄始.md" title="wikilink">玄始</a> 412年-428年<br />
<a href="../Page/承玄.md" title="wikilink">承玄</a> 428年-431年<br />
<a href="../Page/義和_(北涼).md" title="wikilink">義和</a> 431年-433年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沮渠牧犍.md" title="wikilink">沮渠牧犍</a></p></td>
<td><p>無</p></td>
<td><p>哀王</p></td>
<td><p>433年-439年</p></td>
<td><p><a href="../Page/承和_(沮渠牧犍).md" title="wikilink">承和</a> 431年-439年</p></td>
</tr>
<tr class="even">
<td><center>
<p><strong>高昌北涼</strong></p>
</center></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沮渠無諱.md" title="wikilink">沮渠無諱</a></p></td>
<td><p>無</p></td>
<td><p>無</p></td>
<td><p>443年-444年</p></td>
<td><p><a href="../Page/承平_(北涼).md" title="wikilink">承平</a> 443年-444年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沮渠安周.md" title="wikilink">沮渠安周</a></p></td>
<td><p>無</p></td>
<td><p>無</p></td>
<td><p>444年-460年</p></td>
<td><p><a href="../Page/承平_(北涼).md" title="wikilink">承平</a> 444年-460年</p></td>
</tr>
</tbody>
</table>

## 世系图

<center>

</center>

[Category:北涼](../Category/北涼.md "wikilink")
[Category:五胡十六国](../Category/五胡十六国.md "wikilink")
[Category:中國古代民族與國家](../Category/中國古代民族與國家.md "wikilink")
[Category:397年建立的國家或政權](../Category/397年建立的國家或政權.md "wikilink")
[Category:401年建立的國家或政權](../Category/401年建立的國家或政權.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")