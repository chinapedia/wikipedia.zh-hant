**TVBS頻道**（），是台灣[TVBS](../Page/TVBS.md "wikilink")（聯利媒體）經營的有線電視頻道，也是該公司第一個開播的頻道，簡稱**S台**（原簡稱為**T台**）。

## 頻道歷史

台灣在1990年之前，電視僅有[台視](../Page/台視.md "wikilink")、[中視](../Page/中視.md "wikilink")、[華視](../Page/華視.md "wikilink")，號稱「[老三台](../Page/老三台.md "wikilink")」。1993年，[立法院三讀通過](../Page/立法院.md "wikilink")《[有線廣播電視法](../Page/有線廣播電視法.md "wikilink")》，開放[有線電視](../Page/有線電視.md "wikilink")。同年9月28日，[TVBS頻道率先開播](../Page/TVBS頻道.md "wikilink")，為全國第一個有線衛星頻道，宣傳口號「第四個電視台，第二個選擇」，標榜與無線電視做出市場區隔。

1993年至2004年，定頻在38頻道，播出節目內容為綜合台類型。開播時畫面右上角頻道標誌為交錯使用**無線電視台**與**[TVBS.svg](https://zh.wikipedia.org/wiki/File:TVBS.svg "fig:TVBS.svg")**，各節目開播前播出的頻道形象動畫則統一使用**[TVBS.svg](https://zh.wikipedia.org/wiki/File:TVBS.svg "fig:TVBS.svg")**標誌。

2005年，因應[中華民國](../Page/中華民國.md "wikilink")[行政院新聞局](../Page/行政院新聞局.md "wikilink")「同類節目區塊定頻政策」，以「新聞綜合台」的執照定頻於「新聞台區塊（50\~58頻道）」中的56頻道\[1\]。

自2006年起，原先綜合台的內容，如[港劇和](../Page/港劇.md "wikilink")[綜藝節目等搬移到](../Page/綜藝.md "wikilink")[TVBS-G頻道繼續播送](../Page/TVBS歡樂台.md "wikilink")，而TVBS頻道則成為「全方位的新聞評論性頻道」\[2\]。

2014年2月27日，該頻道的HD高畫質版本開始播出（試播）。2014年3月3日，該頻道的HD高畫質版本正式開播。2016年12月21日，更換全新台標。\[3\]

## TVBS頻道主播群

### 新聞主播

| 節目名稱                                            | 主播                                | 備註 |
| ----------------------------------------------- | --------------------------------- | -- |
| [Focus全球新聞](../Page/Focus全球新聞.md "wikilink")    | |[方念華](../Page/方念華.md "wikilink") |    |
| [TVBS 56台 午間新聞](../Page/TVBS午間新聞.md "wikilink") | [黃斐瑜](../Page/黃斐瑜.md "wikilink")  | 平日 |
| 主播排班                                            | 假日                                |    |
| 排班主播                                            | [鄭凱云](../Page/鄭凱云.md "wikilink")  | 專任 |
| 吳安琪                                             | 專任                                |    |
| 古彩彥                                             | 專任                                |    |
| [秦綾謙](../Page/秦綾謙.md "wikilink")                | 專任                                |    |
| 游皓婷                                             | 專任                                |    |
| 張靖玲                                             | 專任                                |    |
| 蔡宜靜                                             | 專任                                |    |
| 葉佳蓉                                             | 專任                                |    |
| 蔡孟樺                                             | 專任                                |    |
| 黃星樺                                             | 專任                                |    |
| 楊茜雯                                             | 兼播                                |    |
|                                                 |                                   |    |

### 節目主持人

| 節目名稱                                             | 主持人                                                                  | 備註                                           |
| ------------------------------------------------ | -------------------------------------------------------------------- | -------------------------------------------- |
| [Today正經話](../Page/Today正經話.md "wikilink")       | [夏嘉璐](../Page/夏嘉璐.md "wikilink")                                     | 已停播                                          |
| [少康戰情室](../Page/少康戰情室.md "wikilink")             | [趙少康](../Page/趙少康.md "wikilink")                                     |                                              |
| [少康會客室](../Page/少康會客室.md "wikilink")             | 已停播                                                                  |                                              |
| [TVBS看板人物](../Page/TVBS看板人物.md "wikilink")       | [方念華](../Page/方念華.md "wikilink")                                     |                                              |
| [全球進行式](../Page/全球進行式.md "wikilink")             | [廖芳潔](../Page/廖芳潔.md "wikilink")                                     |                                              |
| [麗文正經話](../Page/麗文正經話.md "wikilink")             | [鄭麗文](../Page/鄭麗文.md "wikilink")                                     | 已停播                                          |
| [健康2.0](../Page/健康2.0.md "wikilink")             | [鄭凱云](../Page/鄭凱云.md "wikilink")                                     |                                              |
| [中國進行式](../Page/中國進行式.md "wikilink")             | [莊開文](../Page/莊開文.md "wikilink")                                     |                                              |
| [T觀點](../Page/T觀點.md "wikilink")                 |                                                                      |                                              |
| [一步一腳印 發現新台灣](../Page/一步一腳印_發現新台灣.md "wikilink") | [詹怡宜](../Page/詹怡宜.md "wikilink")                                     |                                              |
| [Vision 全球視野](../Page/Vision_全球視野.md "wikilink") | [莊開文](../Page/莊開文.md "wikilink")、[楊永明](../Page/楊永明.md "wikilink")    | 已停播                                          |
| [讚聲大國民](../Page/讚聲大國民.md "wikilink")             | [蘇宗怡](../Page/蘇宗怡.md "wikilink")                                     |                                              |
| [地球黃金線](../Page/地球黃金線.md "wikilink")             | 前主持[廖盈婷與](../Page/廖盈婷.md "wikilink")[劉姿霖](../Page/劉姿霖.md "wikilink") |                                              |
| [宅男的世界](../Page/宅男的世界.md "wikilink")             | [蔡宜靜](../Page/蔡宜靜.md "wikilink")、[凱開](../Page/賴世凱.md "wikilink")     | 已停播                                          |
| [背包客來寮](../Page/背包客來寮.md "wikilink")             | [劉道玄](../Page/劉道玄.md "wikilink")                                     |                                              |
| [TVBS國民大會](../Page/TVBS國民大會.md "wikilink")       | [于美人](../Page/于美人.md "wikilink")                                     |                                              |
| [星鮮話](../Page/星鮮話.md "wikilink")                 | [郭靜](../Page/郭靜.md "wikilink")                                       | 改由[TVBS歡樂台播出](../Page/TVBS歡樂台.md "wikilink") |
|                                                  |                                                                      |                                              |

## 停播節目

### 新聞資訊節目

  - 《[錢線新聞](../Page/錢線新聞.md "wikilink")》：[秦綾謙播報](../Page/秦綾謙.md "wikilink")。
  - 《[善耕台灣](../Page/善耕台灣.md "wikilink")》：[李濤主持](../Page/李濤_\(臺灣\).md "wikilink")。
  - 《[TVBS晚間新聞](../Page/TVBS晚間新聞.md "wikilink")》：[張雅琴](../Page/張雅琴.md "wikilink")、[詹怡宜](../Page/詹怡宜.md "wikilink")、[蘇逸洪](../Page/蘇逸洪.md "wikilink")、[詹慶齡](../Page/詹慶齡.md "wikilink")、[李四端](../Page/李四端.md "wikilink")、[岑永康](../Page/岑永康.md "wikilink")、[謝向榮](../Page/謝向榮.md "wikilink")、[蕭子新](../Page/蕭子新.md "wikilink")、[莊開文播報](../Page/莊開文.md "wikilink")。
  - 《[TVBS哈新聞](../Page/TVBS哈新聞.md "wikilink")》：[翁滋蔓](../Page/翁滋蔓.md "wikilink")、[張嘉雲主持](../Page/張嘉雲.md "wikilink")。
  - 《[TVBS創富報導](../Page/TVBS創富報導.md "wikilink")》：[黃斐瑜](../Page/黃斐瑜.md "wikilink")、[黃洛婷](../Page/黃洛婷.md "wikilink")、[李岱娟](../Page/李岱娟.md "wikilink")、[王慧文](../Page/王慧文.md "wikilink")、[林婉婷播報](../Page/林婉婷.md "wikilink")。（轉型為創富新聞）
  - 《TVBS錢線新聞》
  - 《[TVBS午間新聞](../Page/TVBS午間新聞.md "wikilink")》：[詹怡宜](../Page/詹怡宜.md "wikilink")、[蘇逸洪](../Page/蘇逸洪.md "wikilink")、[蘇宗怡](../Page/蘇宗怡.md "wikilink")、[陳雅琳](../Page/陳雅琳.md "wikilink")、[張婯嬅](../Page/張婯嬅.md "wikilink")、[吳安琪](../Page/吳安琪.md "wikilink")、[莊開文播報](../Page/莊開文.md "wikilink")。
  - 《[TVBS夜間新聞](../Page/TVBS夜間新聞.md "wikilink")》：[馬度芸](../Page/馬度芸.md "wikilink")、[廖筱君](../Page/廖筱君.md "wikilink")、[李晶玉](../Page/李晶玉.md "wikilink")、[詹慶齡](../Page/詹慶齡.md "wikilink")、[丁靜怡](../Page/丁靜怡.md "wikilink")、[岑永康播報](../Page/岑永康.md "wikilink")。
  - 《[晚間台語新聞](../Page/晚間台語新聞.md "wikilink")》：[蘇逸洪](../Page/蘇逸洪.md "wikilink")、[詹慶齡](../Page/詹慶齡.md "wikilink")、[蘇宗怡](../Page/蘇宗怡.md "wikilink")、[張婯嬅](../Page/張婯嬅.md "wikilink")、[陳雅琳播報](../Page/陳雅琳.md "wikilink")。
  - 《[午間台語新聞](../Page/午間台語新聞.md "wikilink")》：[蘇逸洪](../Page/蘇逸洪.md "wikilink")、[詹慶齡](../Page/詹慶齡.md "wikilink")、[蘇宗怡](../Page/蘇宗怡.md "wikilink")、[張婯嬅](../Page/張婯嬅.md "wikilink")、[陳雅琳播報](../Page/陳雅琳.md "wikilink")。
  - 《[TVBS創富新聞](../Page/TVBS創富新聞.md "wikilink")》：[黃斐瑜](../Page/黃斐瑜.md "wikilink")、[蔡宜靜播報](../Page/蔡宜靜.md "wikilink")。
  - 《[台灣是我家](../Page/台灣是我家.md "wikilink")》：2014年4月5日開播，由[吳安琪](../Page/吳安琪.md "wikilink")、[李文儀主持](../Page/李文儀.md "wikilink")。
  - 《[背包客來尞](../Page/背包客來尞.md "wikilink")》：2015年10月24日開播，由[劉道玄主持](../Page/劉道玄.md "wikilink")。
  - 《[宅男的世界](../Page/宅男的世界.md "wikilink")》：2016年3月14日開播，由[蘇宗怡主持](../Page/蘇宗怡.md "wikilink")。

### 談話節目

  - 《[真情指數](../Page/真情指數.md "wikilink")》：[蔡康永主持](../Page/蔡康永.md "wikilink")。
  - 《[新聞夜總會](../Page/新聞夜總會.md "wikilink")》：[李艷秋主持](../Page/李艷秋.md "wikilink")。
  - 《[2100全民開講](../Page/2100全民開講.md "wikilink")》：[李濤主持](../Page/李濤.md "wikilink")。
  - 《[2100黑白相對論](../Page/2100黑白相對論.md "wikilink")》
  - 《[TVBS創富人生](../Page/TVBS創富人生.md "wikilink")》：[林婉婷](../Page/林婉婷.md "wikilink")、[王慧文主持](../Page/王慧文.md "wikilink")。（轉型為創富新聞）
  - 《[麗文正經話](../Page/麗文正經話.md "wikilink")》：2014年3月17日開播（前身為《Today正經話》），由[鄭麗文固定主持](../Page/鄭麗文.md "wikilink")。
  - 《[少康會客室](../Page/少康會客室.md "wikilink")》：2015年9月28日開播，由[趙少康主持](../Page/趙少康.md "wikilink")。
  - 《[讚聲大國民](../Page/讚聲大國民.md "wikilink")》：2015年7月13日開播，由[鄭凱云固定主持](../Page/鄭凱云.md "wikilink")，2016年2月1日起改成[TVBS主播](../Page/TVBS_\(頻道\).md "wikilink")[蘇宗怡固定主持](../Page/蘇宗怡.md "wikilink")。
  - 《[TVBS特別企劃](../Page/TVBS特別企劃.md "wikilink")》由[趙少康](../Page/趙少康.md "wikilink")、[鄭麗文主持](../Page/鄭麗文.md "wikilink")

## 美國版本播放

**TVBS**美國版本的節目編排時間以[西岸為準](../Page/美國西岸.md "wikilink")，從啟播至2012年11月18日在[美國曾透過](../Page/美國.md "wikilink")[DIRECTV於](../Page/w:en:DirecTV.md "wikilink")「TVB翡翠組合」及「國語白金組合」分別使用第452頻道及第2057頻道收看，在[美國透過](../Page/美國.md "wikilink")[Dish
Network使用第](../Page/w:en:Dish_Network.md "wikilink")9985頻道收看\[4\]，該版本除了大部分時間轉播在台灣收看的TVBS直播中的節目之外，其餘時間插播TVBS各頻道製播的節目，還有在美國當地製作而播放的《[美國新聞](../Page/美國新聞.md "wikilink")》。

## 相關條目

  - [TVBS新聞台](../Page/TVBS新聞台.md "wikilink")
  - [電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")
  - [TVBS節目列表](../Page/TVBS節目列表.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 出版雜誌

  - 1997年11月8日至2015年2月12日《TVBS周刊》（雙週刊），出版社：英特發（已停刊）

## 外部連結

  - [TVBS官方網站](http://www.tvbs.com.tw/)
  - [TVB
    USA官方網站](http://www.tvbusa.com/index.php?option=com_channel&Itemid=118&channel=JS)

[en:TVBS](../Page/en:TVBS.md "wikilink")

[Category:1993年成立的电视台或电视频道](../Category/1993年成立的电视台或电视频道.md "wikilink")
[Category:聯利媒體電視頻道](../Category/聯利媒體電視頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")

1.  [TVBS新聞《12月20日TVBS聲明》](http://www.tvbs.com.tw/news/news_list.asp?no=jean20051220190834).\[2007-10-02\]

2.  [TVBS 56台8點港劇
    今起改42台播](https://news.tvbs.com.tw/entry/413390).TVBS新聞網.2006-01-02
    \[2018-03-03\]
3.  [TVBS新風貌！　全新logo亮相「新舊媒體融合」](http://news.tvbs.com.tw/news/detail/inter-news/694839).TVBS新聞網.2016-12-21
4.  [2012年11月18日起，TVB節目在DISH衛星平台繼續播放。](http://www.tvbusa.com/index.php?option=com_channel&Itemid=140&id=3686)
    電視廣播(美國)有限公司網站