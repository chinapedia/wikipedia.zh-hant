[JohnstownDiogenite.jpg](https://zh.wikipedia.org/wiki/File:JohnstownDiogenite.jpg "fig:JohnstownDiogenite.jpg")
**HED 隕石**是三種[無球粒隕石的總稱](../Page/無球粒隕石.md "wikilink")，它們分別是：

  - [古銅鈣無粒隕石](../Page/古銅鈣無粒隕石.md "wikilink")（Howardites）
  - [鈣長輝長無粒隕石](../Page/鈣長輝長無粒隕石.md "wikilink")（Eucrites）
  - [古銅無球隕石](../Page/古銅無球隕石.md "wikilink")（Diogenites）

它們都被認為是來自小行星[灶神星的地殼](../Page/灶神星.md "wikilink")，期間的差異只在於來自不同地質歷史的母岩石。由[放射線同位素測定這些隕石結晶體的年齡都在](../Page/放射線同位素.md "wikilink")44.3和45.5億年之間。

在比較上，它們是非常普通的類型，佔所有發現隕石的5%，並佔[無粒隕石的](../Page/無粒隕石.md "wikilink")60%。

她們可能是經由如下的途徑由[灶神星來到](../Page/灶神星.md "wikilink")[地球](../Page/地球.md "wikilink")
：

1.  灶神星被撞擊拋射出碎片，產生直徑10公里或更小的[V-型小行星碎片](../Page/V-型小行星.md "wikilink")。無論拋出的是大的小行星，或成為小的碎片，其中一些小的小行星組成了[灶神星族](../Page/灶神星族.md "wikilink")，其他的則進一步的散射至各處。
    這些事件被認為發生至今不超過10億年，其中有一次巨大的撞擊，在南半球的巨大隕石坑是這個撞擊點位置的最佳證據。而且需要多次的撞擊，拋射出的岩石才能達到我們已知的V-型小行星數量。
2.  有些小行星的碎片經歷漫長的旅程進入3：1的[柯克伍德空隙](../Page/柯克伍德空隙.md "wikilink")。由於[木星強大的攝動](../Page/木星.md "wikilink")，這是一個不穩定的區間，在這兒的小行星大約經過100萬年的時間就會被拋出，遠離到不同的軌道上。有些受到攝動後進入到接近地球的軌道，形成小的V-型[近地小行星](../Page/近地小行星.md "wikilink")，例如（3551）
    [Verenia](../Page/Verenia.md "wikilink")、（3908）
    [Nyx](../Page/Nyx.md "wikilink")、和（4055）[麥哲倫](../Page/麥哲倫.md "wikilink")。
3.  然後發生在這些接近地球天體上的較小型撞擊，將她們擊碎成隕石體大小的石塊，其中一些在稍後抵達地球。根據[宇宙射線暴露的測量](../Page/宇宙射線.md "wikilink")，認為多數的HED隕石來自幾次不同型態的撞擊事件，並且在太空中經歷600～7,300萬年的時間才撞擊到地球。

## 參考

1.  Lindstrom, Marilyn M.; Score, Roberta. [Populations, Pairing and
    Rare Meteorites in the U.S. Antarctic Meteorite
    Collection](https://web.archive.org/web/20041204104304/http://www-curator.jsc.nasa.gov/antmet/ppr/ppr.htm)。NASA
    Johnson Space Center.

2.  Drake, Michael J.
    \[<http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=2001M%26PS>...36..501D\&db_key=AST\&data_type=HTML\&format=\&high=4374b9c9ce01839
    The eucrite/Vesta story\]. *Meteoritics and Planetary Science*, Vol.
    36, p. 501 (2001).

3.  Binzel, R. P.; Xu, S. Chips off of asteroid 4 Vesta: Evidence for
    the parent body of basaltic achondrite meteorites.
    *[Science](../Page/Science_\(journal\).md "wikilink")*, Vol. 260,
    186 (1993).

4.  Binzel, R.P.; et al. Geologic Mapping of Vesta from 1994 Hubble
    Space Telescope Images. *Icarus*, Vol. 128, p. 95 (1997).

5.  Eugster, O.; Michel, Th.
    \[<http://adsabs.harvard.edu/cgi-bin/nph-bib_query?bibcode=1995GeCoA>..59..177E\&db_key=AST\&data_type=HTML\&format=\&high=4374b9c9ce08038
    Common asteroid break-up events of eucrites, diogenites, and
    howardites, and cosmic-ray production rates for noble gases in
    achondrites\] *Geochemica et Cosmochimica Acta*, Vol. 59, p. 177
    (1995).

[Category:灶神星](../Category/灶神星.md "wikilink")
[Category:隕石](../Category/隕石.md "wikilink")
[Category:隕石類型](../Category/隕石類型.md "wikilink")
[Category:行星科學](../Category/行星科學.md "wikilink")