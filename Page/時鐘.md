[Clock_lock.jpg](https://zh.wikipedia.org/wiki/File:Clock_lock.jpg "fig:Clock_lock.jpg").\]\]
[MontreGousset001.jpg](https://zh.wikipedia.org/wiki/File:MontreGousset001.jpg "fig:MontreGousset001.jpg")\]\]

[Automatic_skeleton_watch.jpg](https://zh.wikipedia.org/wiki/File:Automatic_skeleton_watch.jpg "fig:Automatic_skeleton_watch.jpg")\]\]
**時鐘**簡稱為**鐘**，所有計時裝置都可以称为**计时仪器**。**钟表**在现代汉语中一般有两种意思，一是各类**鐘**和[錶的总称](../Page/手表.md "wikilink")，另一个是专指[体积较大的表](../Page/体积.md "wikilink")，尤指机械结构的有[钟摆的鐘](../Page/單擺.md "wikilink")。

時鐘是人類最早[發明的物品之一](../Page/發明.md "wikilink")，原因是需要持續量測[時間間隔](../Page/時間間隔.md "wikilink")，有些自然的時間間隔（如[日](../Page/日.md "wikilink")、[閏月及](../Page/閏月.md "wikilink")[年](../Page/年.md "wikilink")）可以用觀測而得，較短的時間間隔就需要利用時鐘。數千年計時設備的原理也有大幅變化，[日晷是利用在物體在一平面上影子的變化來計時](../Page/日晷.md "wikilink")，計算時間間隔的儀器也有許多種，包括最廣為人知的[沙漏](../Page/沙漏.md "wikilink")。配合日晷的水鐘可能是最早的計時儀器。歐洲在1300年發明了[擒縱器](../Page/擒縱器.md "wikilink")，後來也創作了第一個機械鐘，可以利用像之類的振盪計時設備\[1\]\[2\]\[3\]\[4\]。發條驅動的時鐘約在15世紀出現，鐘錶業約在15世紀至16世紀開始發展，1656年發明了[摆钟](../Page/摆钟.md "wikilink")，因此在計時的準確性又進一步提昇，當時因為航海導航對時間的精確性要求，也帶動時鐘可靠性及精確性的提昇。[電子時鐘在](../Page/電子時鐘.md "wikilink")1840年申請專利，二十世紀[電子學的發展產生了可以完全不用機械](../Page/電子學.md "wikilink")的時鐘。

現在時鐘內的計時元件是[諧振子](../Page/諧振子.md "wikilink")，一個會以固定精準[頻率](../Page/頻率.md "wikilink")[振盪的物體](../Page/振盪.md "wikilink")\[5\]，諧振子可能是[單擺](../Page/單擺.md "wikilink")、[音叉](../Page/音叉.md "wikilink")、，或是[原子在發射](../Page/原子.md "wikilink")[微波時](../Page/微波.md "wikilink")[電子的振盪](../Page/電子.md "wikilink")。類比型的時鐘會用指針及角度表示時間，數位時鐘則是用數字的方式表示，有兩種時間表示法：[十二小時制及](../Page/十二小時制.md "wikilink")[二十四小時制](../Page/二十四小時制.md "wikilink")。大部份數位時鐘都是用電子設備及[液晶](../Page/液晶.md "wikilink")、[LED及](../Page/LED.md "wikilink")[真空熒光顯示器來顯示時間](../Page/真空熒光顯示器.md "wikilink")。時鐘功能也是現在[電腦](../Page/電腦.md "wikilink")、[手機的標準功能之一](../Page/手機.md "wikilink")。

為了方便性、距離、電話或是[失明人士的需求](../Page/失明人士.md "wikilink")，有用聲音報時的听觉时钟。為了盲人需求，也有用觸摸方式可以感知其時間的盲人时钟，其中有些類似傳統時間，但調整其設計，可以直接觸摸錶面得知時間，但又不會影響計時功能。計時技術也在持續演進之中。

有關鐘表的研究稱為。

## 字源

[英語中的](../Page/英語.md "wikilink")「Clock」源自[拉丁語](../Page/拉丁語.md "wikilink")「clocca」，這個字於13世紀在歐洲出現。

在漢語上，「鍾」與「鐘」是兩種不同的事物，「鍾」原本是指一種酒器，「鐘」是一種樂器。中國大陸、新加坡及马来西亚的簡化字表中，「鍾」與「鐘」合併成「钟」，另外「锺」字在有歧義時方能使用，以作區別。「鐘」古作樂器，至少唐代具時計作用，古分夜五更，每更敲鐘，故鐘生時計之意，屬于衍生字義。日本使用時計作鐘的漢字載體。于漢字文化圈中時計皆具有鐘的意義。

## 歷史

原始人憑天空顏色的變化、太陽的光度來判斷時間。古埃及發現影子長度會隨時間改變，發明[日晷在早上計時](../Page/日晷.md "wikilink")，他們亦發現水的流動需要的時間是固定的，因此發明了[水鐘](../Page/水鐘.md "wikilink")。古代中國人亦有以水來計時的工具——[銅壺滴漏](../Page/水鐘.md "wikilink")。

中國除了用水流來計時外，中國古代民間亦有利用燃點線香來計量時間。龍舟報時更香就是利用燒香來計時的儀器，它更設有定時響鬧的作用。龍舟上掛了數條兩端繫著金屬球的幼線，線下放了燃著的香。每隔一段時間，香便會燒斷一條線子，當金屬球跌進下面的盛器時，便會發出報時響鬧。這種燒香時計最早見於宋代的文獻中。用更香來計算時間的精度不高，但由於它簡單易行，極之適合民間使用，所以曾經十分流行。據文獻記載有些更香可燃燒一晝夜，有些甚至可以燃燒至一個月。

公元1088年，宋朝的科學家[蘇頌和](../Page/蘇頌.md "wikilink")[韓工廉等人製造了史上首座以水力作自動化機械操作的](../Page/韓工廉.md "wikilink")[水運儀象台](../Page/水運儀象台.md "wikilink")\[6\]，它是把渾儀、渾象和機械計時器組合起來的裝置。它以水力作為動力來源，具有科學的[擒縱機構](../Page/擒縱.md "wikilink")，高約12米，7米見方，分三層：上層放渾儀，進行天文觀測；中層放渾象，可以模擬天體作同步演示；下層是該儀器的心臟，計時、報時、動力源的形成與輸出都在這一層中。

公元1276年，中國元代的[郭守敬制成](../Page/郭守敬.md "wikilink")[大明燈漏](../Page/大明燈漏.md "wikilink")。它是利用水力驅動，通過齒輪系及相當複雜的凸輪結構，帶動木偶進行「一刻鳴鐘、二刻鼓、三鉦、四鐃」的自動報時。自宋起，十二時辰分初正即廿四小時系統，一刻即今天的十五分鐘，其準確度較德國之桌鐘早三百多年。

公元1283年在[英格蘭的修道院出現史上首座以砝碼帶動的機械鐘](../Page/英格蘭.md "wikilink")。

13世紀[意大利北部的僧侶開始建立鐘塔](../Page/意大利.md "wikilink")（[鐘樓](../Page/鐘樓.md "wikilink")），其目的是提醒人禱告的時間。

公元1360年[詹希元創制](../Page/詹希元.md "wikilink")「五輪沙漏」，以齒輪、時刻盤合成。

16世紀中在德國開始有桌上的鐘。那些鐘只有一支針，鐘面分成四部分，使時間準確至最近的十五分鐘。

公元1657年，[惠更斯發現](../Page/惠更斯.md "wikilink")[擺的頻率可以計算時間](../Page/擺.md "wikilink")，造出了第一個擺鐘。1670年英國人William
Clement發明錨形[擒縱器](../Page/擒縱器.md "wikilink")。

公元1797年，美國人獲得一個鐘的[專利權](../Page/專利權.md "wikilink")。他被視為美國鐘錶業的始祖。

公元1840年，英国的钟表匠发明了电钟。

公元1946年，美国的物理学家[伊西多·拉比博士弄清楚了](../Page/伊西多·拉比.md "wikilink")[原子钟的原理](../Page/原子钟.md "wikilink")。于两年后，创造出了世界上第一座原子钟，原子钟至今也是最先进的钟。它的运转是借助[铯](../Page/铯.md "wikilink")、[氫原子的天然振动而完成的](../Page/氫.md "wikilink")，它可以在300年内都能准确运转，误差十分小。

18到19世纪，钟表制造业逐步实行了工业化生产。

20世纪，开始进入石英化时期。

21世纪，根据原子钟原理而研制的能自动对时的电波钟表技术逐渐成熟。

## 類型

[KimberleyBavarianClock.jpg](https://zh.wikipedia.org/wiki/File:KimberleyBavarianClock.jpg "fig:KimberleyBavarianClock.jpg")
[Digitalframe.jpg](https://zh.wikipedia.org/wiki/File:Digitalframe.jpg "fig:Digitalframe.jpg")
[手錶亦可以算是鐘的一種](../Page/手錶.md "wikilink")，但一般的鐘都是指較大型，不是常常可以隨身攜帶的。

按計時原理：

  - [水鐘](../Page/水鐘.md "wikilink")：利用水的流動計時。
  - [沙漏](../Page/沙漏.md "wikilink")：利用沙的流動計時。
  - [日晷](../Page/日晷.md "wikilink")：利用一物體影子的變化計時。
  - [擺鐘](../Page/擺鐘.md "wikilink")：利用[單擺的](../Page/單擺.md "wikilink")[簡諧運動計時](../Page/簡諧運動.md "wikilink")。
  - [日晷沙漏](../Page/日晷沙漏.md "wikilink")
  - [漏壺](../Page/漏壺.md "wikilink")：利用水漏出後，水面的高低來計時。
  - [火鐘](../Page/火鐘.md "wikilink")：靠燃燒某物，觀看剩餘量得知時間。
      - [燈鐘](../Page/燈鐘.md "wikilink")：用[油燈計時](../Page/油燈.md "wikilink")，燃燒後觀看剩餘的[油量指示出時間](../Page/油.md "wikilink")。
      - [香鐘](../Page/香鐘.md "wikilink")：比油燈方便。價格便宜，方便實用，古代頗為流行。用[模子製成](../Page/模子.md "wikilink")[盤香](../Page/盤香.md "wikilink")，粗細均勻，燃燒速度平均，同樣觀看盤香上刻度知道[時辰](../Page/時辰.md "wikilink")。亦有[鬧鐘作用](../Page/鬧鐘.md "wikilink")，盤香燒到某一點時，掛在盤香上的重物落下，擊中硬物，發出聲響。
      - [蠟燭鐘](../Page/蠟燭鐘.md "wikilink")：燃燒速度基本相同，在[蠟燭上刻上刻度便可簡單計算時間間隔](../Page/蠟燭.md "wikilink")。
  - [原子鐘](../Page/原子鐘.md "wikilink")：它以原子[共振頻率標準來計算及保持時間的準確](../Page/共振.md "wikilink")
  - [石英鐘](../Page/石英鐘.md "wikilink")：利用[石英晶體電壓特性的精確時鐘](../Page/石英.md "wikilink")。
  - [電波鐘](../Page/電波鐘.md "wikilink")：是指可以通過接收授時[無線電波進行即時時間校準的時鐘](../Page/無線電波.md "wikilink")。

按外觀：

  - ：以二進位方式顯示的時鐘。

  - ：長型，有鐘擺的的時鐘。

  - [布穀鳥鐘](../Page/布穀鳥鐘.md "wikilink")：在特定時刻會出現布穀鳥，發出悅耳的「咕咕」叫聲，19世紀後半葉起，成為世界聞名的紀念品和外國人眼中德國的一種標誌。

  - ：其鐘面是用投影方式投射到其他表面的時鐘。

按功用：

  - [鬧鐘](../Page/鬧鐘.md "wikilink")：可設定在指定時間響鬧的時鐘。
  - [天文鐘](../Page/天文鐘.md "wikilink")：能同時显示天文信息的时钟。
  - [世界鐘](../Page/世界鐘.md "wikilink")：能顯示全世界各大城市時間的時鐘。
  - [棋鐘](../Page/棋鐘.md "wikilink")：兩組對應的時鐘，比別計算如[西洋棋比賽二位對手使用的時間](../Page/西洋棋.md "wikilink")。
  - [足球賽鐘](../Page/足球賽鐘.md "wikilink")（45分鐘）：計算足球賽進行的時間。

## 發展

### 有摆钟表

是由[阿拉伯工匠最早](../Page/阿拉伯.md "wikilink")[设计出来的](../Page/设计.md "wikilink")。其工作原理是[等速运动原理](../Page/等速运动原理.md "wikilink")。

### 机械钟表

机械钟表的[动力系统是](../Page/动力系统.md "wikilink")[发条](../Page/发条.md "wikilink")，计时单位是小时、[分钟](../Page/分_\(时间\).md "wikilink")、秒，分别采用了12[进位制和](../Page/进位制.md "wikilink")60[进位制](../Page/进位制.md "wikilink")。

### 手提電話報時

## 功用

在家庭、辦公室及其他場所都有時鐘，像[錶之類小型的設備可以戴在手腕上或是身上](../Page/錶.md "wikilink")，較大型的會在公眾場所，例如[火車站或是](../Page/火車站.md "wikilink")[教堂內](../Page/教堂.md "wikilink")，在[電腦顯示器](../Page/電腦.md "wikilink")、[手機](../Page/手機.md "wikilink")、[可攜式媒體播放器的角落也會有時間顯示](../Page/可攜式媒體播放器.md "wikilink")。

大部份的[數位電腦會依靠一定固定頻率的信號來同步處理](../Page/數位電腦.md "wikilink")，這稱為[時脈訊號](../Page/定時器訊號.md "wikilink")
（有些研究計劃正在發展用[非同步電路處理的CPU](../Page/非同步電路.md "wikilink")）。像電腦等設備，也會顯示像日期，年份等資訊，這和時脈訊號不同，不過也會透過時脈訊號計時而得，也有些設備會有專門的[實時時鐘](../Page/實時時鐘.md "wikilink")（RTC）晶片。

時鐘的主要目的是顯示時間，但也有以下的一些功用。

  - [時間管理](../Page/時間管理.md "wikilink")：時鐘也可以在指定的時間發出警示聲響提醒使用者，幫助人們進行時間管理，這種時鐘稱為鬧鐘，其音量可能一開始較小聲，漸漸的變大聲，或是聲響先中斷一段時間，隔幾分鐘後再重新發出警示聲響。
  - 定時控制：時鐘可以控制一些需定時動作的設備，例如、[錄影機等](../Page/錄影機.md "wikilink")，這種應用下的時鐘常稱為[計時器](../Page/計時器.md "wikilink")。另外像及[望远镜架都需要根據地球自轉而進行位置或角度的精細調整](../Page/望远镜架.md "wikilink")，也需要配合時鐘的資訊。
  - [導航](../Page/導航.md "wikilink")：自十二世紀起，[航海上的導航需要精確的](../Page/航海.md "wikilink")[經度及](../Page/經度.md "wikilink")[緯度](../Page/緯度.md "wikilink")，纬度可以依[天文導航得知](../Page/天文導航.md "wikilink")，而經度的量測就需要準確的時間計算，[約翰·哈里森在十八世紀中發明了](../Page/約翰·哈里森.md "wikilink")，[開普敦的](../Page/開普敦.md "wikilink")[午砲會固定在中午發射](../Page/午砲.md "wikilink")，方便船員校正時間。
  - [地震學](../Page/地震學.md "wikilink")：在判斷[地震震央位置時](../Page/地震.md "wikilink")，會用一個公共的時鐘，至少有四個觀測員，觀測並記錄不同位置不同[地震波的](../Page/地震波.md "wikilink")，以檢測震央位置。

### 時間標準

有些科學研究需要非常準確的時間，在校正時鐘時也需要一個時間的標準。像原子中在[能階之間的](../Page/能階.md "wikilink")[电子跃迁之類的過程](../Page/电子跃迁.md "wikilink")，其發生週期非常固定，因此若計算這類過程的週期，即可得到準確的時間，這就是[原子鐘](../Page/原子鐘.md "wikilink")。這類的時鐘體積龐大，價格昂貴，且需要在受控環境下運作，不過精確度會遠高於一般的需求，一般會在计量学的標準實驗室中才會有這類設備。

## 文化

### 華人

在華人社會，不可將時鐘贈予他人，因在漢語中「[送鐘](../Page/送鐘.md "wikilink")」是「送終」的[諧音](../Page/諧音.md "wikilink")\[7\]。有些習俗也將手錶視為鐘，因此也不能送人\[8\]。

## 相關條目

  - [計時工具的歷史](../Page/計時工具的歷史.md "wikilink")

  -
  -
  -
  - [昼夜节律](../Page/昼夜节律.md "wikilink")

  - [鐘樓](../Page/鐘樓.md "wikilink")（[中式和](../Page/钟楼_\(中式\).md "wikilink")[西式](../Page/钟楼_\(西式\).md "wikilink")）

  -
  -
  -
  - [定時器訊號](../Page/定時器訊號.md "wikilink")

  - [钟表匠](../Page/钟表匠.md "wikilink")

  -
  - [圣体钟](../Page/圣体钟.md "wikilink")

  -
  -
  - [各地日期和时间表示法](../Page/各地日期和时间表示法.md "wikilink")

  -
  - [末日之鐘](../Page/末日之鐘.md "wikilink")

  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [計量學](../Page/計量學.md "wikilink")

  -
  -
  -
  -
  -
  - [系統時間](../Page/系統時間.md "wikilink")

  -
  -
  - [計時器](../Page/計時器.md "wikilink")

  - [製錶者](../Page/製錶者.md "wikilink")

  - [魔表](../Page/魔表.md "wikilink")

## 參考文獻

## 外部連結

  - [話說計時器的演變](http://www.sciam.com.tw/read/readshow.asp?FDocNo=136&DocNo=214)

[時鐘](../Category/時鐘.md "wikilink") [S](../Category/时间.md "wikilink")

1.  , p.103-104

2.

3.  , p.31

4.

5.
6.  [苏州微缩仿制古代水运仪象台](http://news.xinhuanet.com/photo/2006-08/21/content_4989820.htm)，新華網2006年8月21日

7.

8.