**分子生態學**是利用[进化生物学理论](../Page/进化生物学.md "wikilink")，采用[分子生物学和](../Page/分子生物学.md "wikilink")[基因组学的实验方法和技术手段](../Page/基因组学.md "wikilink")、[统计学和](../Page/统计学.md "wikilink")[数学的分析方法](../Page/数学.md "wikilink")，通过[计算机科学的技术和算法](../Page/计算机科学.md "wikilink")，旁及[地学](../Page/地学.md "wikilink")、[古气候学等](../Page/古气候学.md "wikilink"),
研究[物种和](../Page/物种.md "wikilink")[种群的](../Page/种群.md "wikilink")[遗传变异及](../Page/遗传变异.md "wikilink")[表观遗传变异](../Page/表观遗传变异.md "wikilink")、[遗传谱系](../Page/遗传谱系.md "wikilink"),
探讨[生物多样性演化](../Page/生物多样性演化.md "wikilink")、[生物地理演化](../Page/生物地理演化.md "wikilink")、[物种分化](../Page/物种分化.md "wikilink")、[生态适应等的](../Page/生态适应.md "wikilink")[生态学和进化生物学机制](../Page/生态学.md "wikilink")\[1\]。

研究范围包括种群演化历史、种群分化和物种形成机制、[寄主选择](../Page/寄主.md "wikilink")、[配偶选择](../Page/配偶.md "wikilink")、繁殖行为、生殖方式、[精子竞争](../Page/精子竞争.md "wikilink")、扩散和建群、遗传谱系地理演化、遗传分化和局部适应、生态适应的遗传基础、气候变化的生态学和进化生物学后果、生物对环境变化的响应机制、遗传多样性评估、食性鉴定、觅食行为、濒危生物贸易的监测、个体和种群鉴定、外来入侵种溯
源及入侵机制、遗传修饰生物的生态风险评估、有害生物防控等\[2\]。

分支学科包括[分子群体遗传学](../Page/分子群体遗传学.md "wikilink")、[谱系生物地理学](../Page/谱系生物地理学.md "wikilink")、[生态基因组学](../Page/生态基因组学.md "wikilink")、[景观遗传学](../Page/景观遗传学.md "wikilink")、[系统发生群落生态学等](../Page/系统发生群落生态学.md "wikilink")\[3\]。

由於很多[微生物不能很容易地在實驗室中培養](../Page/微生物.md "wikilink")（海水中的0.001\~0.1%，土壤中的0.3%左右，活性污泥中1\~15%可被分離培養），因此不能用傳統的鑒別和描述[菌株的辦法研究它們](../Page/菌株.md "wikilink")。另外，隨著[聚合酶鏈式反應](../Page/聚合酶鏈式反應.md "wikilink")(PCR)技術的發展，人們可以快速擴增遺傳物質[DNA](../Page/DNA.md "wikilink")。

環境樣品中DNA的擴增通常需要一組用於特定微生物的[引物](../Page/引物.md "wikilink")，而得到遺傳物質的混合物，將其分離，隨後進行[測序和鑒別](../Page/測序.md "wikilink")。經典的分離辦法是通過克隆，將擴增的DNA片段插入到細菌[質粒上實現的](../Page/質粒.md "wikilink")。較新的方法包括[變性梯度凝膠電泳](../Page/變性梯度凝膠電泳.md "wikilink")(DGGE)，可以更快地得到結果。

分子生態學的發展也和[DNA芯片的使用緊密相關](../Page/DNA芯片.md "wikilink")，該技術可以高通量檢測環境中的特定生物或基因。

分子生態學中可以使用很多[基因進行研究](../Page/基因.md "wikilink")，在[分類學角度](../Page/分類學.md "wikilink")，最常應用的基因是[核糖體小亞基](../Page/核糖體.md "wikilink")[RNA](../Page/RNA.md "wikilink")(SSU
rRNA)。而功能性基因的研究有助於判斷微生物在該環境中的活動。

[微生物生態學中和分子技術相關的一個重要問題就是](../Page/微生物生態學.md "wikilink")，這些生物以主動（進行正常[代謝和](../Page/代謝.md "wikilink")[繁殖](../Page/繁殖.md "wikilink")）還是被動（靜息休眠）的方式存在。這可以用幾種方式來解決：

  - 利用[逆轉錄酶擴增活躍的基因](../Page/逆轉錄酶.md "wikilink")
  - 用[熒光原位雜交](../Page/熒光原位雜交.md "wikilink")(FISH)對環境中包含特定基因的細胞進行檢測和計數。

## 发展历史

分子生態學开始于1950年代。是随着[DNA双螺旋结构的发现](../Page/DNA双螺旋结构.md "wikilink")，分子水平的[群体遗传学研究得以开展](../Page/群体遗传学.md "wikilink")，相继出现了[同工酶电泳](../Page/同工酶电泳.md "wikilink")、[限制性内切酶酶切技术](../Page/限制性内切酶酶切技术.md "wikilink")、[分子克隆和](../Page/分子克隆.md "wikilink")[DNA测序技术等](../Page/DNA测序.md "wikilink")，使遗传变异研究得以实现,
并可以在群体水平进行检测。分子进化的中性理论的提出，引发了[适应性进化和](../Page/适应性进化.md "wikilink")[中性进化的争论](../Page/中性进化.md "wikilink")，推动了群体遗传学和分子进化研究，[分子钟假说的提出和](../Page/分子钟假说.md "wikilink")[谱系进化关系重建理论](../Page/谱系进化.md "wikilink")、方法、算法和软件的出现,
进一步促进了分子进化和分子群体遗传学研究的发展\[4\]。

到了1980年代，[DNA聚合酶链反应的发明和](../Page/DNA聚合酶链反应.md "wikilink")[热稳定DNA聚合酶的发现](../Page/热稳定DNA聚合酶.md "wikilink")，进一步推动了分子生态学和分子群体遗传学研究。1992年《Molecular
Ecology》的创刊，标志着分子生態學的建立。[溯祖理论](../Page/溯祖理论.md "wikilink")、[贝叶斯方法和](../Page/贝叶斯方法.md "wikilink")[马尔可夫过程等的引入](../Page/马尔可夫过程.md "wikilink")，提升了分子生态学研究的可靠性\[5\]。

## 参考文献

[Category:生态学分科](../Category/生态学分科.md "wikilink")
[Category:分子生物学](../Category/分子生物学.md "wikilink")
[Category:分子生態學](../Category/分子生態學.md "wikilink")

1.

2.
3.
4.
5.