**仰光省** （）是[緬甸的一個省](../Page/緬甸.md "wikilink")，位於該國中南部平原區的東南角
（另外在[印度洋上的](../Page/印度洋.md "wikilink")[科科群岛亦屬於本省](../Page/科科群岛.md "wikilink")）。面積10,170平方公里，1999年人口5,420,000人。首府[仰光是舊都](../Page/仰光.md "wikilink")，但維持著最大的城市和商業中心的地位。其中[仰光下分](../Page/仰光.md "wikilink")4個分区、35個區。在仰光境外还有[端迪](../Page/端迪.md "wikilink")，[高穆等城镇](../Page/高穆.md "wikilink")。

## 行政區劃

  - [西仰光區](../Page/西仰光區.md "wikilink")（ရန်ကုန်အနောက်ပိုင်းခရိုင်）
  - [東仰光區](../Page/東仰光區.md "wikilink")（ရန်ကုန်အရှေ့ပိုင်းခရိုင်）
  - [北仰光區](../Page/北仰光區.md "wikilink")（ရန်ကုန်မြောက်ပိုင်းခရိုင်）
  - [南仰光區](../Page/南仰光區.md "wikilink")（ရန်ကုန်တောင်ပိုင်းခရိုင်）

[Y](../Category/緬甸省份.md "wikilink") [仰光省](../Category/仰光省.md "wikilink")