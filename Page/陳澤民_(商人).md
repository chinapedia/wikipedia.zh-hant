**陳澤民**（），上海裔香港人，人稱**九哥**，[香港](../Page/香港.md "wikilink")[商人](../Page/商人.md "wikilink")，藝人[陳冠希之父](../Page/陳冠希.md "wikilink")\[1\]。

陳澤民活躍於香港娛樂界，是[英皇娛樂老闆](../Page/英皇娛樂.md "wikilink")[楊受成及影星](../Page/楊受成.md "wikilink")[成龍的好友](../Page/成龍.md "wikilink")。陳澤民旗下有多間演藝及唱片公司，他亦是[上市公司](../Page/上市公司.md "wikilink")[駿雷國際有限公司的主席](../Page/駿雷國際有限公司.md "wikilink")。2003年5月23日，陳澤民所控制的369控股曾經被證券行入稟追討647萬元未付孖展款項。\[2\]陳澤民與前妻Carol育有兩女一子，長女陳麗華、次女[陳見飛及孻仔](../Page/陳見飛.md "wikilink")[陳冠希](../Page/陳冠希.md "wikilink")。除陳冠希外，陳見飛亦曾在娛樂圈發展，並推出唱片，但近來已淡出幕前。陳澤民的父親[陳邦平](../Page/陳邦平.md "wikilink")，\[3\]曾在[香港政府](../Page/香港政府.md "wikilink")[教育署工作](../Page/教育署.md "wikilink")，2005年陳邦平家族出現爭產風波，陳一度被申請破產。

## 参考文献

[Z](../Page/category:陈姓.md "wikilink")
[category:香港雙性戀者](../Page/category:香港雙性戀者.md "wikilink")

[Category:香港商人](../Category/香港商人.md "wikilink")
[C](../Category/上海人.md "wikilink")

1.
2.
3.