《**追日星少年**》（）是[澳大利亞一套以](../Page/澳大利亞.md "wikilink")[少年為主要角色的](../Page/少年.md "wikilink")[科幻故事](../Page/科幻小说.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，由[澳洲廣播公司](../Page/澳洲廣播公司.md "wikilink")（Australian
Broadcasting
Corporation，簡稱ABC）製作。本劇曾經在[香港](../Page/香港.md "wikilink")[亞洲電視國際台播放](../Page/亞洲電視國際台.md "wikilink")。

## 演員列表

## 外部連結

  - [香港亞洲電視：追日星少年](https://web.archive.org/web/20070927000456/http://app.hkatv.com/world/search.php?id=7241)

  - [The ABC's *Silversun* home
    page](https://web.archive.org/web/20051027190845/http://www.abc.gov.au/silversun/)

  - [ABC:
    *Silversun*](https://web.archive.org/web/20070407131803/http://abc.net.au/silversun/)

  - [*Silversun* entry at
    TVTome.com](https://web.archive.org/web/20050720201556/http://www.tvtome.com/tvtome/servlet/ShowMainServlet/showid-26934)


[Category:澳洲电视节目](../Category/澳洲电视节目.md "wikilink")
[Category:科幻電視劇](../Category/科幻電視劇.md "wikilink")