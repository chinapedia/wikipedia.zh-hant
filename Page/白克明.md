**白克明**（），[陕西省](../Page/陕西省.md "wikilink")[靖边县人](../Page/靖边县.md "wikilink")；[哈尔滨军事工程学院导弹工程系毕业](../Page/中国人民解放军国防科技大学.md "wikilink")。1975年加入[中国共产党](../Page/中国共产党.md "wikilink")；是[中共第十六届中央委员](../Page/中国共产党第十六届中央委员会委员列表.md "wikilink")。曾任[人民日報社長](../Page/人民日報.md "wikilink")、[中共海南省委書記](../Page/中国共产党海南省委员会.md "wikilink")、[河北省委書記](../Page/中国共产党河北省委员会.md "wikilink")、[第十一届全国人大常委会委员](../Page/第十一届全国人民代表大会常务委员会.md "wikilink")、[全國人大教育科學文化衛生委員會主任委員等職](../Page/全国人民代表大会教育科学文化卫生委员会.md "wikilink")。

## 从政经历

### 起步

白克明是原[一機部副部長](../Page/中华人民共和国第一机械工业部.md "wikilink")[白堅的兒子](../Page/白坚.md "wikilink")；1968年至1970年[文化大革命期間被下放到](../Page/文化大革命.md "wikilink")[黑龙江省军区](../Page/中国人民解放军黑龙江省军区.md "wikilink")“[五七干校](../Page/五七干校.md "wikilink")”劳动锻炼，后被分配到[黑龙江省冶金地质局科技处工作](../Page/黑龙江省.md "wikilink")；1970年调入[哈尔滨船舶工程学院二系](../Page/哈尔滨工程大学.md "wikilink")，擔任教师。1973年返回[陕西](../Page/陕西.md "wikilink")；并先后在[陕西省国防工办科研部](../Page/陕西省.md "wikilink")、二机局工作。其间，在1977年2月到1978年5月被外借调往[二机部工作](../Page/中华人民共和国核工业部.md "wikilink")。

### 攀升

1978年，白克明正式留在[北京工作](../Page/北京.md "wikilink")，并进入[国家教育委员会办公厅](../Page/中华人民共和国教育部.md "wikilink")，历任副处长、处长、办公厅副主任等职；其间在1983年12月到1985年5月獲借调到中央整党工作指导委员会办公室政策研究组工作；1989年1月出任[国务院研究室教科文卫局局长](../Page/国务院研究室.md "wikilink")。1993年1月转任[中共中央宣传部秘书长](../Page/中共中央宣传部.md "wikilink")；三个月后，升任中宣部副部长。2000年3月，白克明又出任[中共中央办公厅副主任](../Page/中共中央办公厅.md "wikilink")；同年6月起，出任《[人民日报](../Page/人民日报.md "wikilink")》社社长。

### 外放

2001年8月，[中共中央任命白克明出任中共](../Page/中国共产党中央委员会.md "wikilink")[海南](../Page/海南省.md "wikilink")[省委书记](../Page/省委书记.md "wikilink")；同年9月，在海南省第二届人民代表大会第五次会议上，他又当选为海南省[人大常委会主任](../Page/人民代表大会制度.md "wikilink")。一年后，即2002年11月，白克明又转任中共[河北](../Page/河北省.md "wikilink")[省委书记](../Page/省委书记.md "wikilink")；并在2003年1月召开的河北省第十届人民代表大会第一次会议上，当选为河北省人大常委会主任。

### 退休

因年龄到限，2007年8月，白克明被宣布免去中共河北省委書記的职务；同时，被任命為[全國人大教科文衛委員會副主任委員](../Page/全国人民代表大会教育科学文化卫生委员会.md "wikilink")。2008年3月，在[第十一屆全国人大第一次会议上](../Page/第十一届全国人民代表大会.md "wikilink")，白克明当选为[全国人大常委會委員兼](../Page/全国人民代表大会常务委员会.md "wikilink")[全國人大教科文衛委員會主任委員](../Page/全国人民代表大会教育科学文化卫生委员会.md "wikilink")。

## 参考文献

  - [中国网：白克明](http://www.china.org.cn/chinese/zhuanti/283321.htm)
  - [人民网：白克明简历](http://www.people.com.cn/GB/shizheng/252/9667/9684/20021126/874992.html)
  - [白克明連任河北省委書記](https://web.archive.org/web/20070930204426/http://www.mpinews.com/htm/INews/20061114/ca21723c.htm)

{{-}}

[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:人民日报社社长](../Category/人民日报社社长.md "wikilink")
[Category:河北省人大常委会主任](../Category/河北省人大常委会主任.md "wikilink")
[Category:海南省人大常委会主任](../Category/海南省人大常委会主任.md "wikilink")
[Category:中共河北省委书记](../Category/中共河北省委书记.md "wikilink")
[Category:中共海南省委书记](../Category/中共海南省委书记.md "wikilink")
[Category:中国人民解放军军事工程学院校友](../Category/中国人民解放军军事工程学院校友.md "wikilink")
[Category:靖邊人](../Category/靖邊人.md "wikilink")
[K克](../Category/白姓.md "wikilink")
[Category:中共中央办公厅副主任](../Category/中共中央办公厅副主任.md "wikilink")