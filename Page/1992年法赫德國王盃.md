**1992年法赫德國王盃**（），即第1届[联合会杯足球赛](../Page/联合会杯足球赛.md "wikilink")，于1992年10月15日至20日在[沙特阿拉伯举行](../Page/沙特阿拉伯.md "wikilink")。决赛中[阿根廷以](../Page/阿根廷国家足球队.md "wikilink")
3–1
击败了[沙特阿拉伯](../Page/沙特阿拉伯国家足球队.md "wikilink")，夺得了首个冠军。该项赛事也是[国际足联联合会杯的前身](../Page/国际足联联合会杯.md "wikilink")。本届杯赛也是唯一一次直接举行[淘汰赛的联合会杯比赛](../Page/淘汰赛.md "wikilink")，共有4队参加。

## 参赛球队

[1992_confed_cup.png](https://zh.wikipedia.org/wiki/File:1992_confed_cup.png "fig:1992_confed_cup.png")

  - \- 东道主、[1988年亚洲杯冠军](../Page/1988年亚洲杯足球赛.md "wikilink")

  - \- [1991年南美國家盃冠军](../Page/1991年南美國家盃.md "wikilink")

  - \- [1991年美洲金盃冠军](../Page/1991年美洲金盃.md "wikilink")

  - \- [1992年非洲國家盃冠军](../Page/1992年非洲國家盃.md "wikilink")

## 比賽場地

本届比赛所有场次都在沙特首都[利雅得](../Page/利雅得.md "wikilink")[法赫德國王國際體育場举行](../Page/法赫德國王國際體育場.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/利雅德.md" title="wikilink">利雅德</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/法赫德國王國際體育場.md" title="wikilink">法赫德國王國際體育場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>可容納觀眾: <strong>67,000</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 晉級圖

## 半决赛

-----

## 三四名决赛

## 决赛

## 射手榜

  - 2球

<!-- end list -->

  - [巴迪斯圖達](../Page/巴迪斯圖達.md "wikilink")

  - [布魯士·梅利](../Page/布魯士·梅利.md "wikilink")

## 參見

  - [國際足聯聯合會盃](../Page/國際足聯聯合會盃.md "wikilink")

## 外部链接

[Category:国际足联联合会杯](../Category/国际足联联合会杯.md "wikilink")
[Category:沙特阿拉伯足球](../Category/沙特阿拉伯足球.md "wikilink")
[Category:沙特阿拉伯体育史](../Category/沙特阿拉伯体育史.md "wikilink")