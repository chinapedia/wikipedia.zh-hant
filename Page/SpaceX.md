}}
**太空探索技术公司**（，商业名称：）是美国一家[私人航天制造商和太空运输公司](../Page/私人公司.md "wikilink")，总部位于美国[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[霍桑](../Page/霍桑_\(加利福尼亚州\).md "wikilink")。SpaceX由企业家[伊隆·马斯克](../Page/伊隆·马斯克.md "wikilink")（）于2002年创办，目标是降低太空运输的成本，并进行[火星殖民](../Page/火星殖民.md "wikilink")。
\[1\]\[2\]\[3\]SpaceX现开发出[猎鹰系列运载火箭及](../Page/猎鹰系列运载火箭.md "wikilink")[龙系列飞船](../Page/龙飞船.md "wikilink")，用于运送荷载至[地心轨道](../Page/地心轨道.md "wikilink")。

截止2019年2月，SpaceX已经与[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")（）进行了16次[国际空间站](../Page/国际空间站.md "wikilink")（）补给任务的合作。\[4\]2011年，NASA向SpaceX授予了，研发并测试拥有的龙飞船，用以运送航天员往返国际空间站。\[5\]

2011年，SpaceX开始进行。2015年12月，SpaceX将猎鹰9号火箭飞回发射场附近的着陆场，并成功进行了，是轨道级火箭首次达成此成就。\[6\]在2016年4月的发射任务中，SpaceX成功让火箭第一级垂直降落在海上的，并将一艘龙飞船送入[低地球轨道](../Page/低地球轨道.md "wikilink")。\[7\]2017年3月，SpaceX将已回收的火箭第一级再次发射，并再次成功着陆。\[8\]

2016年9月，首席执行官[伊隆·马斯克公布了](../Page/伊隆·马斯克.md "wikilink")[行星际运输系统](../Page/行星际运输系统.md "wikilink")（）的任务架构。这一私人投资项目的目标是研发太空飞行技术，用于载人[行星际航行](../Page/行星际航行.md "wikilink")。\[9\]\[10\]2017年，马斯克宣布行星际运输系统改名为[大猎鹰火箭](../Page/大猎鹰火箭.md "wikilink")（），它可以完全重复使用，将成为世界上最大的火箭，计划于21世纪20年代初投入使用。\[11\]\[12\]

## 历史

[Dragon_capsule_and_SpaceX_employees_(16491695667).jpg](https://zh.wikipedia.org/wiki/File:Dragon_capsule_and_SpaceX_employees_\(16491695667\).jpg "fig:Dragon_capsule_and_SpaceX_employees_(16491695667).jpg")

2001年，伊隆·马斯克诞生了“火星绿洲”（）这一概念，计划向火星发射一个小型实验温室并种植植物。“这个温室将成为生命曾到达的最远处”。\[13\]计划的目的是吸引公众对于太空探索的兴趣，增加。\[14\]\[15\]\[16\]马斯克尝试从俄罗斯购买火箭，但因价格高昂而空手而归。\[17\]\[18\]在从俄罗斯归来的飞机上，马斯克意识到他可以创立一家公司制造他所需要便宜火箭。\[19\][特斯拉及SpaceX早期投资人](../Page/特斯拉_\(公司\).md "wikilink")表示，马斯克计算了生产火箭的原材料价格，发现仅是当时火箭售价的3%。\[20\]通过，\[21\]自行生产85%的发射设备及软件模块，\[22\]\[23\]SpaceX能将火箭发射价格降至十分之一且仍拥有70%[毛利率](../Page/毛利率.md "wikilink")。\[24\]

[Launch_of_Falcon_9_carrying_ORBCOMM_OG2-M1_(16601442698).jpg](https://zh.wikipedia.org/wiki/File:Launch_of_Falcon_9_carrying_ORBCOMM_OG2-M1_\(16601442698\).jpg "fig:Launch_of_Falcon_9_carrying_ORBCOMM_OG2-M1_(16601442698).jpg")發射。\]\]

2002年初，马斯克开始为他的新航天公司招募员工，公司被命名为SpaceX。马斯克找到了火箭工程师[汤姆·穆勒](../Page/汤姆·穆勒.md "wikilink")（现任SpaceX推进部门首席技术官），穆勒同意了他的邀请，于是SpaceX诞生了。\[25\]最初，SpaceX的总部位于加利福尼亚州[埃尔塞贡多的一间仓库](../Page/埃尔塞贡多_\(加利福尼亚州\).md "wikilink")。自2002年创立以来，SpaceX增长迅速。2005年11月，SpaceX拥有160名员工，至2017年11月已增加至将近7000人。\[26\]2016年，马斯克在[国际宇航大会中发表演讲表示](../Page/国际宇航大会.md "wikilink")，美国政府将火箭技术认定为“先进武器科技”，因此SpaceX难以雇佣非美国人为员工。\[27\]

[Falcon_9_Flight_20_OG2_first_stage_post-landing_(23273082823)_cropped.jpg](https://zh.wikipedia.org/wiki/File:Falcon_9_Flight_20_OG2_first_stage_post-landing_\(23273082823\)_cropped.jpg "fig:Falcon_9_Flight_20_OG2_first_stage_post-landing_(23273082823)_cropped.jpg")

截止2018年3月，SpaceX已经进行了超过100次发射任务，合同收入约120亿美元。\[28\]其中既有商业合同也有政府合同（来自美国国家航空航天局及[美国国防部](../Page/美国国防部.md "wikilink")）。\[29\]2013年末，太空行业媒体引用马斯克的评论表示，SpaceX“增加了火箭发射行业的竞争”，它在[通信卫星发射领域的主要竞争对手有](../Page/通信卫星.md "wikilink")、[联合发射联盟和](../Page/联合发射联盟.md "wikilink")。\[30\]马斯克还说道，竞争加剧是“对太空的未来有益的事”。\[31\]目前，从订单数量来看，SpaceX是全球领先的商业发射服务提供商。\[32\]

马斯克於2006年3月獨自投資了1億美元。\[33\]2008年8月4日，SpaceX接受了来自的2000萬美元投资。\[34\]三分之二的股权由创始人持有，约700000股，价值8.75亿美元。2012年2月，SpaceX市值13亿美元。预计到2013年底，公司完成代号为COTS
2+的发射任务后，市值将会翻倍到24亿。

Elon
Musk認為太空發射服務之所以价格高昂，一部分是因为是不必要的官僚架構。他的目標是从十個因素着手降低成本、提高太空服務的可靠性。\[35\]
为了尽可能的降低成本，SpaceX在[瓜加林環礁仅有](../Page/瓜加林環礁.md "wikilink")25個員工及6個任務控制員。
CEO Elon Musk 说: “我认为$1,100/kg或更低的发射价格是可以达到的”\[36\]

2005年1月，SpaceX購買了[薩里衛星技術公司](../Page/薩里衛星技術公司.md "wikilink")10％的股份。\[37\]

2006年8月18日，SpaceX宣布它獲得[美國太空總署](../Page/美國太空總署.md "wikilink")[商業軌道運輸服務的合約](../Page/商業軌道運輸服務.md "wikilink")，證明了自己是NASA運送貨物到國際空間站的選項之一。\[38\]

2008年12月23日，SpaceX獲得價值16億美元的商業補給服務的合約，從而保證[太空梭在](../Page/太空梭.md "wikilink")2010年退役後[國際空間站補給任務能够正常进行](../Page/國際空間站.md "wikilink")。\[39\]

2012年8月，公司與NASA簽署了一項大型開發合約，旨在設計下一代載人航空器，以在2017年能夠重新啟動美國載人航天計劃，波音和Sierra
Nevada也參與了類似的合約。該計劃由NASA的CCiCap（商業船員綜合能力）制定相关標準。這將導致政府和商业公司都有能力提供商业载人航天飞行服务。作为該協議的一部分，‬SpaceX公司獲得了一份价值高達4.4亿美元的合約，於2012年至2014年5月间交付。

2015年12月21日，SpaceX成功回收猎鹰九号的第一节火箭，火箭成功在佛罗里达州卡纳维拉尔角发射基地LZ-1垂直降落。这是SpaceX首次成功实现第一节火箭回收，也是陆地回收首次成功。

2016年4月8日，SpaceX成功回收獵鷹九號的第一節火箭，發射後約10分鐘降落在海面上名為“Of Course I Still Love
You”的船上。这是SpaceX第二次成功实现第一级火箭回收，也是首次成功的海上平台回收。

2016年5月6日，SpaceX又成功在大西洋上回收了一枚火箭，距4月8日SpaceX实现首次海上火箭回收不到一个月。
这是SpaceX第三次完成火箭回收，第二次在海上实现火箭回收，也是全世界首次在地球同步转移轨道（GTO）发射任务中实现火箭回收。

2018年2月6日3点40分（美東时间）SpaceX在美国卡纳维拉尔角将载有特斯拉跑车的獵鷹重型運載火箭[发射升空](../Page/獵鷹重型運載火箭測試任務.md "wikilink")\[40\]，重型猎鹰含有3枚“[猎鹰九号](../Page/猎鹰九号.md "wikilink")”火箭芯组成的助推器和27个引擎，发射能够产生2270吨火箭推力，是目前运载能力最强的火箭，它能将54吨有效载荷送入轨道。有两枚成功降落在位于陆地上的回收平台。\[41\]。

[CRS-8_(26239020092).jpg](https://zh.wikipedia.org/wiki/File:CRS-8_\(26239020092\).jpg "fig:CRS-8_(26239020092).jpg")

### 目标

马斯克曾表示，他的目标是降低前往太空的成本同时提高可靠性。\[42\]他还说道：“我相信低于每磅500美元（每千克1100美元）的价格是十分可能达成的。”\[43\]

[Falcon_Heavy_cropped.jpg](https://zh.wikipedia.org/wiki/File:Falcon_Heavy_cropped.jpg "fig:Falcon_Heavy_cropped.jpg")矗立于[肯尼迪航天中心39号发射台](../Page/肯尼迪航天中心39号发射台.md "wikilink")。\]\]

SpaceX的主要目标是开发一种，计划包括两个方面：[蚱蜢火箭用于低空低速的](../Page/蚱蜢火箭.md "wikilink")测试\[44\]\[45\]\[46\]，[猎鹰9号运载火箭用于高空高速的着陆测试](../Page/猎鹰9号运载火箭.md "wikilink")。2015年12月21日，SpaceX首次成功将轨道级火箭降落在着陆场。

### 成就

SpaceX的标志性成就包括：

  - 2008年，设计、制造并发射了世界首个由私人投资的轨道级液体燃料火箭[猎鹰1号](../Page/猎鹰1号.md "wikilink")<ref name=sfn20080928>

</ref>

  - 2009年，作为一家私营公司首次成功使用液体燃料火箭将商业卫星运送至轨道
  - 2010年，作为一家私营公司首次成功将飞船送入低地球轨道并回收
  - 2012年，作为一家私营公司首次成功将飞船送至国际空间站\[47\]
  - 2013年，将发射进入[GTO](../Page/地球同步转移轨道.md "wikilink")（SpaceX首次GTO发射）
  - 2015年，将发射至地球轨道之外（SpaceX首次BEO发射）
  - 研发了[猎鹰1号运载火箭和](../Page/猎鹰1号运载火箭.md "wikilink")[猎鹰9号运载火箭](../Page/猎鹰9号运载火箭.md "wikilink")。[猎鹰1号运载火箭](../Page/猎鹰1号运载火箭.md "wikilink")、[猎鹰9号运载火箭与为](../Page/猎鹰9号运载火箭.md "wikilink")[国际空间站补给货物的](../Page/国际空间站.md "wikilink")[龙飞船的设计都秉持可重复使用的理念](../Page/龙飞船.md "wikilink")。载人版本的[龙飞船正在开发当中](../Page/龙飞船.md "wikilink")。
  - 發射首個私人投資的運載物至地球軌道之外（[獵鷹重型運載火箭測試任務](../Page/獵鷹重型運載火箭測試任務.md "wikilink")）

### 资本

截至2012年5月，SpaceX公司在其第一個十年計劃中，已經掌控约10亿美元的資金。其中，私募股权投资2亿，Elon
Musk注资1亿，其他资本占1亿，其余来自长期开发合約。NASA投入4-5亿用于发射合約。2012年5月，公司獲得40個發射合約，每个合約都已在簽訂時支付了訂金，很多还会隨著設備製造的進展支付額外的費用。

## 公司机构组成

### 总部

[Entrance_to_SpaceX_headquarters.jpg](https://zh.wikipedia.org/wiki/File:Entrance_to_SpaceX_headquarters.jpg "fig:Entrance_to_SpaceX_headquarters.jpg")
SpaceX公司的总部位于[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[霍桑市火箭路](../Page/霍桑_\(加利福尼亞州\).md "wikilink")1号。这个过去用于制造波音747机身的巨大设施现在成为了SpaceX的办公室，地面指挥中心和载具制造工厂。\[48\]

2009年6月16日，SpaceX宣布启用宇航员安全与任务保障部门。NASA前宇航员肯·鲍尔索克斯受雇成为公司副总裁之一以监管这一部门。\[49\]
然而，据报道这位前宇航员已于2011年底离开SpaceX公司。对于肯·鲍尔索克斯辞职的原因，各方没有给予解释。而SpaceX亦未公布执掌这一部门的替代人选。\[50\]

### 测试，和降落后备部

SpaceX有两个火箭测试设施：其一是位于德州McGregor市的SpaceX火箭发展与测试部，其二是租用新墨西哥州南部[美国太空港的垂直发射垂直降落](../Page/美国太空港.md "wikilink")（VTVL，vertical
takeoff, vertical
landing）测试设施。所有SpaceX的火箭发动机均经过[火箭发动机试车台测试](../Page/火箭发动机试车台.md "wikilink")，猎鹰9号[蚱蜢火箭](../Page/蚱蜢火箭.md "wikilink")1.0版测试飞行器在McGregor完成了低空垂直发射垂直降落（VTVL）测试。蚱蜢火箭1.1版计划于[美国太空港进行高空高速飞行测试](../Page/美国太空港.md "wikilink")。此外，飞行后的火箭拆解和燃料抽取工作也在McGregor进行。

### 飞行控制中心，发射站

#### 發射基地

  - 上的

  - [肯尼迪航天中心](../Page/肯尼迪航天中心.md "wikilink")[39号发射复合体A](../Page/肯尼迪航天中心39号发射复合体.md "wikilink")（KSC
    LC-39A）

  - [卡納維拉爾角空軍基地](../Page/卡納維拉爾角空軍基地.md "wikilink")[40號航天發射複合體](../Page/卡納維拉爾角空軍基地40號航天發射複合體.md "wikilink")（CCAFS
    LC-40）

  - [范登堡空軍基地](../Page/范登堡空軍基地.md "wikilink")，E（VAFB SLC-4E）

  - [南德克萨斯发射场](../Page/太空探索科技公司南德克萨斯发射场.md "wikilink")（正在建设中）

所有獵鷹1號均發射於奥莫莱克岛。

## 商业订单与合約

### COTS/CRS计划

2005年5月2日，SpaceX宣布它已被授予不确定交付时间/不确定交付数量（IDIQ）合約給為[美國空軍提供發射服務的Responsive](../Page/美國空軍.md "wikilink")
Small
Spacelift（RSS），這可能使美國空軍購買最多1亿美元的發射服務。\[51\]2006年，[NASA向SpaceX授予了](../Page/NASA.md "wikilink")（COTS）合約，在该合約下SpaceX将设计并演示一个可以执行国际空间站补给任务的发射系统。2008年4月22日，[美國太空總署宣布已与SpaceX签订一個有关獵鷹](../Page/美國太空總署.md "wikilink")1號或獵鷹9號發射的IDIQ合約。該合約價值20,000美元至10億美元，價格取決於有多少任務獎勵。該合約涵蓋从2010年6月30日到2012年的發射服務。\[52\]

伊隆·马斯克於4月22日宣佈，SpaceX已售出14份各種獵鷹運載火箭的合約。\[53\]

2009年6月26日，SpaceX宣布新增宇航員安全和任務保證部，并聘請前美國太空總署宇航員[肯內斯·鮑威索克斯作為該部門的主管及公司的副總裁](../Page/肯內斯·鮑威索克斯.md "wikilink")。\[54\]

### CCDev/CCiCap计划

### “红龙”火星任务设想

除了SpaceX公司诸多由民间资本支持的终极目标指向火星的计划，截至2011年7月美国太空总署下属的阿姆斯研究中心（Ames Research
Center）已经开发出了一个低成本的火星任务设想，在这一设想中将使用“重型猎鹰”火箭作为发射和火星中途—入轨载具，运送“天龙”太空舱进入火星大气层。这个代号“红龙”的设想将会在2012/2013年度作为一项美国太空总署的探索任务提出以便筹集资金，计划在2018年进行发射，并于数月后抵达火星。这项任务的科学目标是寻找生命存在的证据，包括搜索“可以证明生命存在的分子，例如DNA和高氯酸盐还原酶……通过生物分子证明生命的存在……‘红龙’将钻探至地下一米（3.3英尺）左右，以获取已知潜藏于红色土壤之下的水冰储藏的样本”。不包括发射成本在内，这次任务的费用预计将低于4.25亿美元。\[55\]\[56\]

## 技术

### 火箭发动机，發射器

<table style="width:80%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>發射器</p></th>
<th><p><a href="../Page/獵鷹1號運載火箭.md" title="wikilink">獵鷹1號</a></p></th>
<th><p><a href="../Page/獵鷹9號運載火箭.md" title="wikilink">獵鷹9號</a></p></th>
<th><p><a href="../Page/獵鷹重型運載火箭.md" title="wikilink">獵鷹重型</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>第一節</strong></p></td>
<td><p>1 × <a href="../Page/默林火箭发动机#默林1A.md" title="wikilink">默林1A发动机</a>（2006–2007） 1 x <a href="../Page/默林火箭发动机#默林1C.md" title="wikilink">默林1C发动机</a>（2008–2009）<ref>{{cite web</p></td>
<td><p>title = Updates Archive</p></td>
<td><p>publisher = SpaceX</p></td>
</tr>
<tr class="even">
<td><p><strong>第二節</strong></p></td>
<td><p>1 × <a href="../Page/Kestrel_(火箭發動機).md" title="wikilink">Kestrel</a></p></td>
<td><p>1 × <a href="../Page/默林火箭发动机#默林真空1D.md" title="wikilink">默林真空1D</a></p></td>
<td><p>1 × <a href="../Page/默林火箭发动机#默林真空1D.md" title="wikilink">默林真空1D</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>高度</strong><br />
(<a href="../Page/米.md" title="wikilink">m</a>)</p></td>
<td><p>21.3</p></td>
<td><p><strong>v1.0</strong>:54.9<br />
<strong>v1.1</strong>:68.4<br />
<strong>FT</strong>:70</p></td>
<td><p>70</p></td>
</tr>
<tr class="even">
<td><p><strong>直徑</strong><br />
(<a href="../Page/米.md" title="wikilink">m</a>)</p></td>
<td><p>1.7</p></td>
<td><p>3.7</p></td>
<td><p>3.7</p></td>
</tr>
<tr class="odd">
<td><p><strong>海平面推力</strong><br />
(<a href="../Page/牛頓_(單位).md" title="wikilink">kN</a>)</p></td>
<td><p>318</p></td>
<td><p><strong>Block 5(2017年中)</strong>:8,451<br />
<strong>FT(2016年末)</strong>:7,607<br />
<strong>FT</strong>:6,806<br />
<strong>v1.1</strong>:5,885<br />
<strong>v1.0</strong>:4,940</p></td>
<td><p>22,819</p></td>
</tr>
<tr class="even">
<td><p><strong>質量</strong><br />
(<a href="../Page/kg.md" title="wikilink">kg</a>)</p></td>
<td><p>27,200</p></td>
<td><p><strong>FT</strong>:549,054<br />
<strong>v1.1</strong>:505,846<br />
<strong>v1.0</strong>:333,400<br />
</p></td>
<td><p>1420,788</p></td>
</tr>
<tr class="odd">
<td><p><strong>有效載荷</strong><br />
(<a href="../Page/近地軌道.md" title="wikilink">LEO</a>; <a href="../Page/公斤.md" title="wikilink">kg</a>)</p></td>
<td><p>570</p></td>
<td><p>22,800</p></td>
<td><p>54,400</p></td>
</tr>
<tr class="even">
<td><p><strong>有效載荷</strong><br />
(<a href="../Page/GTO.md" title="wikilink">GTO</a>; <a href="../Page/公斤.md" title="wikilink">kg</a>)</p></td>
<td><p> —</p></td>
<td><p>8,300</p></td>
<td><p>22,200</p></td>
</tr>
<tr class="odd">
<td><p><strong>價格</strong><br />
(百萬 <a href="../Page/美元.md" title="wikilink">USD</a>)</p></td>
<td><p><strong>$6.7</strong></p></td>
<td><p><strong>$62</strong></p></td>
<td><p><strong>$90</strong>( &lt;8000kg <a href="../Page/GTO.md" title="wikilink">GTO</a>)</p></td>
</tr>
<tr class="even">
<td><p><strong>成功率</strong><br />
(成功次數/發射次數)</p></td>
<td><p>2/5</p></td>
<td><p>29/30</p></td>
<td><p>1/1</p></td>
</tr>
</tbody>
</table>

\[57\] \[58\] \[59\] \[60\] \[61\]

### 航天器

SpaceX製造了兩個主要的太空運載火箭：[獵鷹1號於](../Page/獵鷹1號運載火箭.md "wikilink")2008年8月28日成功首次飛行，較大的級[獵鷹9號於](../Page/獵鷹9號運載火箭.md "wikilink")2010年首次發射。[獵鷹5號因](../Page/獵鷹5號運載火箭.md "wikilink")[獵鷹9號而暫停開發](../Page/獵鷹9號運載火箭.md "wikilink")。SpaceX同時開發[龙飞船](../Page/龙飞船.md "wikilink")，一個由獵鷹9號發射到軌道的載人航天器。

#### 獵鷹1號

[SpaceX_Falcon_vertical_on_the_launch_pad.jpg](https://zh.wikipedia.org/wiki/File:SpaceX_Falcon_vertical_on_the_launch_pad.jpg "fig:SpaceX_Falcon_vertical_on_the_launch_pad.jpg")於[范登堡空軍基地](../Page/范登堡空軍基地.md "wikilink")3號航天發射複合體。由於延誤獵鷹1號被解除，最終於[瓜加林環礁發射](../Page/瓜加林環礁.md "wikilink")\]\]

#### 獵鷹9號

#### 獵鷹重型

#### 天龍號太空船

[SpaceX_Dragon_-_2007_X-Prize_Cup.JPG](https://zh.wikipedia.org/wiki/File:SpaceX_Dragon_-_2007_X-Prize_Cup.JPG "fig:SpaceX_Dragon_-_2007_X-Prize_Cup.JPG")在[新墨西哥州](../Page/新墨西哥州.md "wikilink")[霍洛曼空軍基地展出](../Page/霍洛曼空軍基地.md "wikilink")\]\]

在2013年10月7日的测试中，SpaceX公司研发的“蚱蜢”火箭完成了“第八跳”，在发射测试中成功升空744米，随后准确降落到发射台上。“蚱蜢”火箭使用了全新的垂直起飞垂直降落（VTVL）概念，SpaceX航天公司希望凭借该技术打造出可重复使用的火箭系统，这样在运载火箭发射后就可以自动降落在预定场地上，不需要从海洋中人工打捞火箭助推器或者其他部件。这项技术有助于降低运载火箭的发射成本，甚至使火箭可以完成可重复使用。SpaceX航天公司希望将垂直起飞垂直降落技术安装到下一代的猎鹰火箭上。
\[62\]

#### 被取消的發射器設計

## 參考文獻

### 引用

### 来源

  - [SpaceX對波音公司和洛克希德馬丁公司的全部傳票和訴狀](http://www.spaceref.com/news/viewsr.html?pid=18460)
    (2005年10月23日)

  - [SpaceX發射前會議的筆記](https://web.archive.org/web/20090110015444/http://michaelbelfiore.com/blog/2005/11/spacex-prelaunch-conference.html)
    (米高·貝爾菲奧雷，2005年11月18日)

  - [*瓜加林環礁與火箭*](http://kwajrockets.blogspot.com/) (Candid and highly
    unofficial blog by Elon Musk's brother Kimbal Musk, with on-site
    pictures and reporting)

  - [Space
    Fellowship關於的SpaceX新聞](http://www.spacefellowship.com/News/?cat=38)
    ([Space Fellowship](../Page/Space_Fellowship.md "wikilink"))

  - (2009年5月11日)

  -
  - Parts [2](http://thespacereview.com/article/568/1),
    [3](http://thespacereview.com/article/575/1),
    [4](http://www.thespacereview.com/article/578/1)

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [SpaceX](http://www.spacex.com)

  -
  -
  -
  -
## 参见

  - [軌道科學公司](../Page/軌道科學公司.md "wikilink")
  - [太空探索](../Page/太空探索.md "wikilink")
  - [藍色起源](../Page/藍色起源.md "wikilink")
  - [維珍銀河](../Page/維珍銀河.md "wikilink")

{{-}}

[太空探索科技公司](../Category/太空探索科技公司.md "wikilink")
[Category:美國航空航太公司](../Category/美國航空航太公司.md "wikilink")
[Category:2002年成立的公司](../Category/2002年成立的公司.md "wikilink")
[Category:私人航天公司](../Category/私人航天公司.md "wikilink")
[Category:2002年加利福尼亞州建立](../Category/2002年加利福尼亞州建立.md "wikilink")
[SpaceX](../Category/SpaceX.md "wikilink")
[Category:商業發射服務提供商](../Category/商業發射服務提供商.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.
    2016|last=|first=|date=2018|website=RELAYTO/|archive-url=|archive-date=|dead-url=|access-date=}}

12.

13.

14.

15.

16.

17.

18.

19.
20.

21.
22.

23.

24.

25.

26.
27.

28.

29.  SpaceX |publisher=SpaceX |accessdate=March 1, 2017 }}

30.

31.
32.

33.

34.

35.

36. CEO Elon Musk said: "I believe $500 per pound ($1,100/kg) or less is
    very achievable."

37.

38.

39.

40. [SpaceX成功发射"猎鹰重型"
    完全碾压世界其他火箭](http://news.163.com/18/0207/10/DA1M3HAV0001875O.html)

41. [SpaceX成功发射重型火箭把特斯拉跑车送入太空](https://www.voachinese.com/a/news-space-x-car-launch-20180206/4241409.html).美国之音.\[2018-02-07\].

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62. [美企研发可重复使用火箭：升空744米后落回发射台](http://news.ifeng.com/world/detail_2013_10/19/30475508_0.shtml)，凤凰网转载自北方网，2013-10-19