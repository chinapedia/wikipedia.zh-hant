**二色石豆蘭**（[学名](../Page/学名.md "wikilink")：**），又名**二色卷瓣蘭**，[蘭科](../Page/蘭科.md "wikilink")[石豆蘭屬](../Page/石豆蘭屬.md "wikilink")，[香港特有的](../Page/香港.md "wikilink")[極危](../Page/極危.md "wikilink")[附生植物](../Page/附生植物.md "wikilink")，僅生長於[大帽山的](../Page/大帽山.md "wikilink")[溪流旁的](../Page/溪流.md "wikilink")[岩石上](../Page/岩石.md "wikilink")。但亦有指二色石豆蘭亦發現於[泰國](../Page/泰國.md "wikilink")。

在[香港島首次發現的時間大約在](../Page/香港島.md "wikilink")1880年。大部分分佈於[郊野公園內](../Page/郊野公園.md "wikilink")，現在受到《[林務規例](../Page/林務規例.md "wikilink")》及《[動植物(瀕危物種保護)條例](../Page/動植物\(瀕危物種保護\)條例.md "wikilink")》所保護，亦為中國國家重點保護野生植物以及[瀕臨絕種野生動植物國際貿易公約附錄II物種](../Page/瀕臨絕種野生動植物國際貿易公約.md "wikilink")。

外型特徵為根狀莖，[葉生於卵球形](../Page/葉.md "wikilink")[假鱗莖上](../Page/假鱗莖.md "wikilink")，[橢圓形](../Page/橢圓形.md "wikilink")。[淡黃色](../Page/淡黃色.md "wikilink")[花朵](../Page/花朵.md "wikilink")，花葶從假鱗莖的基部發出，傘形[花序](../Page/花序.md "wikilink")，邊緣生有紅色緣毛。[唇瓣初為欖綠色](../Page/唇瓣.md "wikilink")，後為橘紅色，卵形，與蕊柱足末端以關節方式連接，[蕊柱伸展呈](../Page/蕊柱.md "wikilink")[三角形](../Page/三角形.md "wikilink")。花期為每年5月。

## 參考文獻

  - [香港植物標本室 -
    二色石豆蘭](http://www.hkherbarium.net/Herbarium/html%20text/83Bulbophyllum%20bicolor.htm)
  - [中國植物誌](http://www.cvh.org.cn/zhiwuzhi/page/19/227.pdf)

## 外部链接

[bicolor](../Category/石豆蘭屬.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")