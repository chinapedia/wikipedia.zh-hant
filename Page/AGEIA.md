**AGEIA
Technologies**曾是一家美國的[無廠半導體公司](../Page/無廠半導體公司.md "wikilink")（Fabless），於2002年由五名技術成員成立。在2005年3月9日，它在『遊戲開發者論壇』中發表了世界上第一顆採用硬體加速的[物理處理器](../Page/物理處理器.md "wikilink")－[PhysX](../Page/PhysX.md "wikilink")，令這間寂寂無名的公司一夜成名。公司名字
"AGEIA"
是取自五名創辦人來自的不同地區，分別為[美國](../Page/美國.md "wikilink")（**A**merica）、[德國](../Page/德國.md "wikilink")（**G**ermany）、[埃及](../Page/埃及.md "wikilink")（**E**gypt）、[印度](../Page/印度.md "wikilink")（**I**ndia）和[美洲](../Page/美洲.md "wikilink")（**A**merica）。2008年2月4日，[NVIDIA宣佈收購AGEIA](../Page/NVIDIA.md "wikilink")\[1\]。使其[G80及之後的產品支援PhysX加速](../Page/GeForce_8.md "wikilink")。

[物理處理器是專用來執行物理運算](../Page/物理處理器.md "wikilink")，而速度則比傳統处理器快上多倍。而PhysX物理處理器則采用NovodeX作為其[中间件](../Page/中间件.md "wikilink")，用於遊戲軟體中。現時已有多款電腦遊戲及平台，使用和支援PhysX技术，包括[Sony](../Page/Sony.md "wikilink")
[PlayStation
3](../Page/PlayStation_3.md "wikilink")、[Microsoft](../Page/Microsoft.md "wikilink")
[XBOX
360](../Page/XBOX_360.md "wikilink")、[Nintendo](../Page/Nintendo.md "wikilink")
[Wii和](../Page/Wii.md "wikilink")[個人電腦](../Page/個人電腦.md "wikilink")。遊戲引擎方面，魔域幻境引擎3已使用PhysX技术作為其物理引擎。由於PhysX物理引擎开发包（前稱NovodeX
SDK）是免費的（源代碼要5萬[美元一份](../Page/美元.md "wikilink")），所以吸引了不少的用戶使用\[2\]。而公司會透過售賣物理加速卡來賺錢。

## 参考連結

## 外部連結

  - [AGEIA产品支持网站](http://devsupport.ageia.com/)

[Category:英伟达](../Category/英伟达.md "wikilink")
[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[A](../Category/聖塔克拉拉公司.md "wikilink")
[Category:2002年加利福尼亞州建立](../Category/2002年加利福尼亞州建立.md "wikilink")

1.  [NVIDIA to Acquire AGEIA
    Technologies](http://www.nvidia.com/object/io_1202161567170.html)
2.  [Ageia更新PhysX授權模式：免費+收費](http://news.sina.com.tw/tech/sinacn/cn/2007-09-29/163638196649.shtml)