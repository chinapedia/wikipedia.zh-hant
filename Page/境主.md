**境主**，又稱**統境主**、**統境王**、**境主公**，是[地方](../Page/地區.md "wikilink")[行政神祇](../Page/行政.md "wikilink")，類似[佛教](../Page/佛教.md "wikilink")[伽藍神與](../Page/伽藍神.md "wikilink")[儒教的](../Page/儒教_\(宗教\).md "wikilink")[城隍神](../Page/城隍神.md "wikilink")；與[土地神有分別](../Page/土地神.md "wikilink")。一般說法，有[城池的](../Page/城池.md "wikilink")[行政區之神](../Page/行政區.md "wikilink")，謂之[城隍](../Page/城隍.md "wikilink")。[佛寺的守護神](../Page/佛寺.md "wikilink")，謂之[伽藍](../Page/伽藍神.md "wikilink")。而[道教](../Page/道教.md "wikilink")[廟宇的轄區或是無城池的](../Page/廟宇.md "wikilink")[街道](../Page/街道.md "wikilink")、[部落之守護神](../Page/部落.md "wikilink")，稱為**境主**。

有時信眾會把「信仰中心的主神」稱為「境主」，如[臺灣](../Page/臺灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")[淡水人稱](../Page/淡水區.md "wikilink")[淡水清水巖](../Page/淡水清水巖.md "wikilink")[清水祖師為](../Page/清水祖師.md "wikilink")「境主」；[高雄](../Page/高雄.md "wikilink")[鳳山](../Page/鳳山區.md "wikilink")[五甲龍成宮稱](../Page/五甲龍成宮.md "wikilink")[媽祖](../Page/媽祖.md "wikilink")、[清水祖師](../Page/清水祖師.md "wikilink")、戴府元帥為「三境主」，此「境主」並非地方行政神，而為「信仰中心的主神」的意思。

## 簡介

「境主公」的職司，一般有幾種說法，有說是廟宇建築所在土地範圍的守護神，不過此一職守似乎應屬廟宇落成「安龍謝土」時所安的[土地龍神](../Page/土地龍神.md "wikilink")。較普遍的說法則是廟宇「祭祀圈」範圍內的守護神，其「轄區」隨著主廟的祭祀圈大小而定，往往跨越數個[土地祠所在地](../Page/土地祠.md "wikilink")。

細考其「境主」名稱由來，其實可溯至[福建的行政區](../Page/福建.md "wikilink")，自[元代以來即有](../Page/元代.md "wikilink")[境](../Page/境.md "wikilink")、[舖](../Page/舖.md "wikilink")、[都](../Page/都.md "wikilink")、[社等劃分](../Page/社.md "wikilink")，一般街市大約都屬「境」的層級，也因此各區域廟宇所祀地方管轄神也就稱為「境主」，在泉州一舖也有舖主公\[1\]通常一舖分為若干境，如清代台南有十個聯境組織如東段的八協境，主廟為[東門大人廟](../Page/東門大人廟.md "wikilink")；南段的八吉境主廟為馬兵營保和宮；西段的六興境主廟為[開山宮](../Page/開山宮.md "wikilink")，主廟底下各境又有屬廟，而府城只有一個[臺灣府城隍廟](../Page/臺灣府城隍廟.md "wikilink")。比如[泉州府東隅分為四舖](../Page/泉州府.md "wikilink")，其中中華舖分三境，壺中境境廟為壺中廟，中和境境廟為粜籴廟。境主宮廟常為鄉約中心，也是清代[丁隨地派](../Page/丁隨地派.md "wikilink")，[公家取消](../Page/公家.md "wikilink")[傜役後](../Page/傜役.md "wikilink")，地方[仕紳臨時加派民眾服役的組織](../Page/仕紳.md "wikilink")，也類似明代巡警舖（亦是差役的一種）民防、防盜、[消防](../Page/消防.md "wikilink")、守望相助的基層鄉里組織，尤其在[林爽文事件後](../Page/林爽文事件.md "wikilink")，[府城廣設聯境組織](../Page/府城.md "wikilink")。

也有人認為，有城（城牆）和隍（護城河）的地區，其護境神為城隍爺；沒有城池範圍的村莊市鎮，其護境神才稱為「境主公」或「統境尊王」，有些地方甚至還建有以「境主公」為主神的廟宇，如称“大王宫”。

在廟宇中奉祀的「境主」，還有司法管轄權，被認為有資格管轄廟宇轄境內的鬼魂魑魅，故多比照城隍之例，配祀文武[判官](../Page/判官_\(神祇\).md "wikilink")、[陰陽司](../Page/陰陽司.md "wikilink")、[范謝將軍](../Page/范謝將軍.md "wikilink")、[韓盧將軍](../Page/韓盧將軍.md "wikilink")、[甘柳將軍](../Page/甘柳將軍.md "wikilink")、[枷鎖將軍](../Page/枷鎖將軍.md "wikilink")、[牛馬將軍](../Page/牛馬將軍.md "wikilink")、[日夜遊神等神者](../Page/日夜遊神.md "wikilink")。

## 相關條目

  - [巡警](../Page/巡警.md "wikilink")
  - [鄉約](../Page/鄉約.md "wikilink")
  - [鄉飲酒禮](../Page/鄉飲酒禮.md "wikilink")
  - [林爽文事件](../Page/林爽文事件.md "wikilink")
  - [山神](../Page/山神.md "wikilink")
  - [城隍](../Page/城隍.md "wikilink")
  - [青山王](../Page/青山王.md "wikilink")
  - [伽藍神](../Page/伽藍神.md "wikilink")
  - [土地神](../Page/土地神.md "wikilink")
  - [地基主](../Page/地基主.md "wikilink")
  - [民主公王](../Page/民主公王.md "wikilink")
  - [五福王爺](../Page/五福王爺.md "wikilink")
  - [台灣民間信仰](../Page/台灣民間信仰.md "wikilink")

## 參考資料

### 外部文獻

  - [新莊地藏庵--認識眾神](http://xzdza.org.tw/introduce_02.html)

### 註釋

[Category:道教神祇](../Category/道教神祇.md "wikilink")
[Category:東亞民間信仰](../Category/東亞民間信仰.md "wikilink")
[Category:地域守護神](../Category/地域守護神.md "wikilink")

1.  《云泥叹》: 而宫中[相公爷](../Page/田都元帥.md "wikilink")、铺主公香烛皆不及他，其故何软。