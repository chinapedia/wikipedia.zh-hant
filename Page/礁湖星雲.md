**礁湖星云**（**M8 - The Lagoon Nebula**、**[NGC](../Page/NGC天体.md "wikilink")
6523**）是一个位于[人马座的](../Page/人马座.md "wikilink")[发射星云](../Page/发射星云.md "wikilink")，明亮的气体云被一条遮掩物质形成的暗带切开。利用[雙筒望遠鏡觀測](../Page/雙筒望遠鏡.md "wikilink")，它是一個有明顯核心的橢圓型雲狀物。这个星云中包含了一个稀疏的[疏散星团](../Page/疏散星团.md "wikilink")[NGC
6530](../Page/NGC_6530.md "wikilink")。

礁湖星云距離[地球約五千二百](../Page/地球.md "wikilink")[光年](../Page/光年.md "wikilink")，其目視大小有90×40[角分](../Page/角分.md "wikilink")，即實際大小大約為140×60光年。

人马座的[梅西耶天体除礁湖星云外](../Page/梅西耶天体.md "wikilink")，还包括[奥米加星云](../Page/奥米加星云.md "wikilink")、[三裂星云](../Page/三裂星云.md "wikilink")、[小人马恒星云](../Page/小人马恒星云.md "wikilink")、[M18](../Page/M18.md "wikilink")、[M21](../Page/M21.md "wikilink")、[M22](../Page/M22_\(星團\).md "wikilink")、[M23等](../Page/M23.md "wikilink")。

## 图片

Image:Messier 8.jpg|Image taken by the Wide Field Channel of the
Advanced Camera for Surveys on Hubble. Image:VISTA's infrared view of
the Lagoon Nebula (Messier 8).jpg|红外线中的礁湖星云 <File:Lagoon> nebula
SALT.jpg|礁湖星云的中心区域，可以看到右侧为礁湖星云中的沙漏星雲。 <File:Lagoon> Nebula
(ESO).jpg|[ESO拍摄的礁湖星云](../Page/ESO.md "wikilink") <File:GigaGalaxy> Zoom
composite.jpg|<small>ESO GigaGalaxy
Zoom计划的三张夜空全景图，从不同层次展示了夜空的全景：第一张是天文爱好者望远镜看到的夜空全景，最後一张是专业天文望远镜中所看到的礁湖星云。</small>
[Messier_008_2MASS.jpg](https://zh.wikipedia.org/wiki/File:Messier_008_2MASS.jpg "fig:Messier_008_2MASS.jpg")
Image:Messier 8 Centre.jpg|Waves breaking in the stellar lagoon
Credit: [NASA](../Page/NASA.md "wikilink"),
[ESA](../Page/ESA.md "wikilink"). <File:Diving> into the Lagoon
Nebula.OGG|深入礁湖星云.

## 参见

  - [深空天体](../Page/深空天体.md "wikilink")
  - [梅西耶天体](../Page/梅西耶天体.md "wikilink")
  - [梅西耶天体列表](../Page/梅西耶天体列表.md "wikilink")
  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 参考文献

[Category:人马座NGC天体](../Category/人马座NGC天体.md "wikilink")
[008](../Category/梅西耶天體.md "wikilink")
[Category:人马座](../Category/人马座.md "wikilink")
[Category:發射星雲](../Category/發射星雲.md "wikilink")
[025](../Category/沙普利斯天体.md "wikilink")