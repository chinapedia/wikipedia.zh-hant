[AtomicEffects-p7b.jpg](https://zh.wikipedia.org/wiki/File:AtomicEffects-p7b.jpg "fig:AtomicEffects-p7b.jpg")
**原爆點**（，直譯為「地面零點」）為一[軍事](../Page/軍事.md "wikilink")[術語](../Page/術語.md "wikilink")，狹義指[原子彈](../Page/原子彈.md "wikilink")[爆炸時投影至地面的中心點](../Page/爆炸.md "wikilink")，廣義則可指任何大規模爆炸的中心點。後來在英文世界中，「」也泛指受到嚴重損毀或破壞的地方。

一般提及「原爆點」時，常是特指1945年發生的[廣島市原子彈爆炸](../Page/廣島市原子彈爆炸.md "wikilink")、或[長崎市原子彈爆炸的爆炸中心點](../Page/長崎市原子彈爆炸.md "wikilink")。而2001年發生[九一一事件後](../Page/九一一袭击事件.md "wikilink")，也使用「」來指稱在[恐怖攻擊中遭到摧毀的](../Page/恐怖攻擊.md "wikilink")[世界貿易中心雙子星大樓原址](../Page/世界貿易中心.md "wikilink")。此外，[美國國防部所在地](../Page/美國國防部.md "wikilink")[五角大廈的中庭廣場](../Page/五角大廈.md "wikilink")，也暱稱為「」，這是因[冷戰時期曾假設](../Page/冷戰.md "wikilink")[蘇聯如對美國進行](../Page/蘇聯.md "wikilink")[核子武器攻擊](../Page/核子武器.md "wikilink")，五角大廈會成為攻擊目標之一\[1\]。

## 參考資料

[Category:灾难](../Category/灾难.md "wikilink")
[Category:军事术语](../Category/军事术语.md "wikilink")

1.