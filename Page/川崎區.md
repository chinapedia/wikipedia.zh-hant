{{ 日本區

`| 自治體名=川崎區`

|圖像=[Kawasaki_Daishi_Main_Hall.jpg](https://zh.wikipedia.org/wiki/File:Kawasaki_Daishi_Main_Hall.jpg "fig:Kawasaki_Daishi_Main_Hall.jpg")
|圖像說明=[京](../Page/東京.md "wikilink")[濱四大](../Page/橫濱市.md "wikilink")[本山之一](../Page/本山_\(佛教\).md "wikilink")[平間寺](../Page/平間寺.md "wikilink")（關東八十八箇所特別[靈場](../Page/靈場.md "wikilink")）的大[本堂](../Page/本堂.md "wikilink")

`| 區章=`
`| 日文原名=川崎区`
`| 平假名=かわさきく`
`| 羅馬字拼音=Kawasaki-ku`
`| 都道府縣=神奈川縣`
`| 支廳=`
`| 市=川崎市`
`| 編號=14131-3`
`| 面積=40.15`
`| 邊界未定=`
`| 人口=212,777`
`| 統計時間=2008年5月1日`
`| 自治體=`[`川崎市`](../Page/川崎市.md "wikilink")`（`[`幸區`](../Page/幸區.md "wikilink")`）、`[`橫濱市`](../Page/橫濱市.md "wikilink")`（`[`鶴見區`](../Page/鶴見區_\(橫濱市\).md "wikilink")`）`
[`東京都`](../Page/東京都.md "wikilink")`：`[`大田區`](../Page/大田區.md "wikilink")
[`千葉縣`](../Page/千葉縣.md "wikilink")`：`[`木更津市`](../Page/木更津市.md "wikilink")
`| 樹=`
`| 花=`
`| 其他象徵物=`
`| 其他象徵=`
`| 郵遞區號=210-8570`
`| 所在地=川崎區東田町8番地`
`| 電話號碼=44-201-3113`
`| 外部連結=`<http://www.city.kawasaki.jp/61/61kawasakiku/index.htm>
`| 經度=`
`| 緯度=`
`| 地圖=`

}}

**川崎區**（）是[川崎市的](../Page/川崎市.md "wikilink")7區之一。為[多摩川到河口的南側地區](../Page/多摩川.md "wikilink")。全域為平地，沿海地區有填海工程。另有東扇島和扇島，扇島全域為企業私有地，一般人難以進入。

## 交通

### 鐵路

[Kanarin_chidori.JPG](https://zh.wikipedia.org/wiki/File:Kanarin_chidori.JPG "fig:Kanarin_chidori.JPG")

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [JR_JT_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JT_line_symbol.svg "fig:JR_JT_line_symbol.svg")
    [東海道線](../Page/東海道線_\(JR東日本\).md "wikilink")·[上野東京線](../Page/上野東京線.md "wikilink")：[川崎站](../Page/川崎站.md "wikilink")
  - [JR_JK_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JK_line_symbol.svg "fig:JR_JK_line_symbol.svg")
    [京濱東北線](../Page/京濱東北線.md "wikilink")：川崎站
  - [JR_JN_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JN_line_symbol.svg "fig:JR_JN_line_symbol.svg")
    [南武線](../Page/南武線.md "wikilink")：川崎站
      - 南武支線：[八丁畷站](../Page/八丁畷站.md "wikilink") -
        [川崎新町站](../Page/川崎新町站.md "wikilink") -
        [小田榮站](../Page/小田榮站.md "wikilink") -
        [濱川崎站](../Page/濱川崎站.md "wikilink")
  - [JR_JI_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JI_line_symbol.svg "fig:JR_JI_line_symbol.svg")
    [鶴見線](../Page/鶴見線.md "wikilink")：[武藏白石站](../Page/武藏白石站.md "wikilink")
    - 濱川崎站 - [昭和站](../Page/昭和站.md "wikilink") -
    [扇町站](../Page/扇町站_\(神奈川縣\).md "wikilink")
      - 大川支線：[大川站](../Page/大川站_\(日本\).md "wikilink")

<!-- end list -->

  - [京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")

<!-- end list -->

  - [Number_prefix_Keikyū.PNG](https://zh.wikipedia.org/wiki/File:Number_prefix_Keikyū.PNG "fig:Number_prefix_Keikyū.PNG")
    [本線](../Page/京急本線.md "wikilink")：[八丁畷站](../Page/八丁畷站.md "wikilink")
    - [京急川崎站](../Page/京急川崎站.md "wikilink")
  - [Number_prefix_Keikyū.PNG](https://zh.wikipedia.org/wiki/File:Number_prefix_Keikyū.PNG "fig:Number_prefix_Keikyū.PNG")
    [大師線](../Page/大師線_\(京濱急行電鐵\).md "wikilink")：京急川崎站 -
    [港町站](../Page/港町站.md "wikilink") -
    [鈴木町站](../Page/鈴木町站.md "wikilink") -
    [川崎大師站](../Page/川崎大師站.md "wikilink") -
    [東門前站](../Page/東門前站.md "wikilink") -
    [產業道路站](../Page/產業道路站.md "wikilink") -
    [小島新田站](../Page/小島新田站.md "wikilink")（全線）

<!-- end list -->

  - [日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")

<!-- end list -->

  - ：

<!-- end list -->

  -

<!-- end list -->

  -
  -
  -
## 外部連結