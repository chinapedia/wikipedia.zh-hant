[2012年4月25日開幕當日的雷生春.jpg](https://zh.wikipedia.org/wiki/File:2012年4月25日開幕當日的雷生春.jpg "fig:2012年4月25日開幕當日的雷生春.jpg")
[HK_LSC_Lui_Seng_Chun_1.JPG](https://zh.wikipedia.org/wiki/File:HK_LSC_Lui_Seng_Chun_1.JPG "fig:HK_LSC_Lui_Seng_Chun_1.JPG")

**雷生春**位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[旺角](../Page/旺角.md "wikilink")[荔枝角道及](../Page/荔枝角道.md "wikilink")[塘尾道交界](../Page/塘尾道.md "wikilink")，是具古典[意大利](../Page/意大利.md "wikilink")[建築特色](../Page/建築.md "wikilink")\[1\]的[唐樓](../Page/唐樓.md "wikilink")，業權原本由[九龍巴士創辦人之一](../Page/九龍巴士.md "wikilink")[雷亮](../Page/雷亮.md "wikilink")（又名雷鴻維）持有。

[建築物的名字源於一對](../Page/建築物.md "wikilink")[對聯](../Page/對聯.md "wikilink")，「雷雨功深揚灑露，生民仰望藥回春」，寓意雷生春所生產的藥品能夠妙手回春。

2000年，雷亮後人捐贈給浸大中醫藥學院，後被[香港特區政府列為](../Page/香港特區政府.md "wikilink")[一級歷史建築](../Page/香港一級歷史建築列表.md "wikilink")，2012年4月更活化成為[非牟利機構經營的中醫診所](../Page/非牟利機構.md "wikilink")。

## 歷史淵源

生於[廣東省](../Page/廣東省.md "wikilink")[台山縣的雷亮來到香港後以經營運輸及貿易生意致富](../Page/台山縣.md "wikilink")，是九龍巴士的創辦人之一。

1929年，[雷亮家族向](../Page/雷亮家族.md "wikilink")[香港政府購入旺角荔枝角道](../Page/香港殖民地時期#香港政府.md "wikilink")119號，並邀請建築師布爾（W.
H.
Bourne）興建雷生春。雷生春於1931年建成（亦有網頁指為1934年），地舖為雷亮的台山同鄉兄弟[雷瑞德中醫師](../Page/雷瑞德.md "wikilink")（他後來與雷亮和另外三人合作創辦九龍巴士）開設的雷生春醫館及藥店，上面三層為雷亮家庭成員的住所。雷生春藥品在當時廣受附近居民歡迎，並且行銷海外。

雷亮於1942年逝世，雷生春跌打藥店在數年後結業，其後曾用作商住及出租作洋服店等用途。約1980年代開始，雷生春住宅部分長期荒廢，但地面商鋪仍在營業。

[File:LSC1949.jpg|1949年的雷生春](File:LSC1949.jpg%7C1949年的雷生春)
[File:意大利建築特色的唐樓雷生春.JPG|活化前的雷生春](File:意大利建築特色的唐樓雷生春.JPG%7C活化前的雷生春)
（2004年） <File:Lui> Seng Chun.jpg|修葺中的雷生春
（2004年） <File:Lui> Seng Chun, 2005.jpg|修葺完成的雷生春
（2005年） <File:HK> LSC Lui Seng Chun 2.JPG|空置的雷生春
（2007年）
[File:2012年4月25日的雷生春建築正面.jpg|中醫診所開幕當日](File:2012年4月25日的雷生春建築正面.jpg%7C中醫診所開幕當日)
（2012年）

## 建築特色

[Lui_Seng_Chun_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Lui_Seng_Chun_\(Hong_Kong\).jpg "fig:Lui_Seng_Chun_(Hong_Kong).jpg")

### 建築風格

雷生春樓高4層，以鋼筋[混凝土築建](../Page/混凝土.md "wikilink")，樓面面積為762平方米，是典型的「走馬大騎樓」唐樓，結合現代流線（[裝飾藝術](../Page/裝飾藝術.md "wikilink")）風格的橫線設計與[古典元素](../Page/新古典主義.md "wikilink")，這可從其方型框架及其上的欄杆裝飾得見其建築特色。建築物落成之時，只須符合1903年版《公共衞生及建築物條例》，規定每幢建築物須在後方設置小面積的露天地方作天然通風用途；建築物的高度不得超逾對開街道的闊度或75呎，以較少者為準；以及每幢建築物的深度不得超逾40呎。

雷生春正門設於荔枝角道和塘尾道交界，建寬闊用石磨柱支撐的露台，能抵禦風雨及阻隔陽光，有助降低室內氣溫。大樓頂層外牆嵌有家族店號的石匾，這些設計都是本地戰前騎樓的典型建築風格。大樓正面設計甚為精緻，富有古典意大利風格，如樓頂凹凸不平的山牆即為一例，位於塘尾道的側門入口設有後院。至於大樓的弧形主立面，則是為了遷就道路交界的窄角而特別設計。結構方面，雷生春的外牆為紅磚身外加灰泥批盪；樑、柱則以鋼筋混凝土製造，1、2、3樓均建有伸延出行人道之上寬闊的外廊。它可說是香港戰前騎樓式建築的代表。

### 活化過程

雷生春進行活化時，將盡量保留原有的建築設計和特色，包括騎樓式的外觀、欄杆裝飾、遊廊、門窗、地磚，以及頂層外牆嵌有雷生春家族店號的石匾等。其外部需保存整體建築物的混凝土構架、前外廊連扶欄、各層露台的排水口、刻有雷生春的石匾及簷板和楣飾；而內部需保存上海批盪實心磚牆、灰泥簷板與模塑、所有門道的花崗石門檻、木或灰泥牆線、幾何圖案彩色磁磚、天花裝飾灰泥、木製模塑及天花線、木門及門框與舊式五金裝置、窗及楣窗的裝飾性鐵花和後外廊連混凝土板及柱座。由於雷生春位於荔枝角道和塘尾道交界而有嚴重噪音問題，在不影響建築物的原本外觀下安裝玻璃窗以解決問題。

## 發展

[Lui_Seng_Chun,_wood_plaque_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Lui_Seng_Chun,_wood_plaque_\(Hong_Kong\).jpg "fig:Lui_Seng_Chun,_wood_plaque_(Hong_Kong).jpg")

### 歷史評級

2000年，雷亮的後人向[古物古蹟辦事處提出將雷生春捐贈給港府](../Page/古物古蹟辦事處.md "wikilink")，[古物諮詢委員會同年將雷生春列為一級歷史建築物](../Page/古物諮詢委員會.md "wikilink")。2003年10月7日，港府正式接收雷生春\[2\]。2004年，雷生春開始修葺，工程於2005年中完成。之後香港政府研究雷生春未來用途，計劃邀請私人發展商提交方案，將其發展為結合文化與商機的新地標，可惜具體用途遲遲未有落實\[3\]。

### 保育活化

2008年，雷生春成為首批「活化歷史建築伙伴計劃」下的7幢建築物之一\[4\]\[5\]。2009年2月17日港府宣佈[香港浸會大學從](../Page/香港浸會大學.md "wikilink")30份遞交的申請書中，成功獲選參與活化雷生春計劃，獲得政府資助2千8百萬港元，將雷生春發展為集中醫藥保健服務、公共健康教育、歷史文化展覽於一身的「香港浸會大學中醫藥保健中心──雷生春堂」，設有5間診症室、1間售賣中藥及涼茶的店鋪、展覽場地和一間屋頂草藥花園，活化後將提供[內科](../Page/內科.md "wikilink")，骨傷及[跌打科](../Page/跌打.md "wikilink")，[推拿科和](../Page/推拿.md "wikilink")[針灸科的中醫門診服務](../Page/針灸.md "wikilink")，並會定期舉辦義診，為區內弱勢社群服務\[6\]，預留每日兩成的門診名額予綜援人士。此外，中心亦會提供教學、實習機會和培訓課程，並舉辦中醫藥展覽、健康講座，也會連結浸大現有的[孔憲紹博士伉儷中醫藥博物館和](../Page/孔憲紹博士伉儷中醫藥博物館.md "wikilink")[中國銀行中藥標本中心發展成為中醫藥展覽系列](../Page/中國銀行中藥標本中心.md "wikilink")，並為參觀團體提供導賞服務。

### 雷生春堂

2011年1月14日浸大舉行「香港浸會大學中醫藥學院──雷生春堂」項目啟動典禮，活化計劃建設成本為2,800萬港元，預計於2012年4月完成\[7\]。活化工程完成後，雷生春堂地下原有的跌打藥水售賣處將會活化為涼茶鋪及中成藥零售和展覽區，一樓用作接待處、製藥房、藥材儲存及預備室，二至三樓則主要為3間診症室、兩間針灸及推拿治療室，置有8張治療床位；一至三樓的騎樓位置為展覽廳；此外，每層均設有不同主題的展示區。雷生春堂外圍騎樓用了玻璃圍封以擴大空間，增加了2千多平方呎，作為病人候診室，亦可以隔絕從街道傳來的噪音及空氣污染；為了符合《消防條例》，外面新建了一條樓梯及加建一部[升降機](../Page/升降機.md "wikilink")。屋頂部份設立具綠化建築物作用的中藥園圃，種有20多種草藥作教學用途。此外，同時適量引入[再生能源及雨水收集系統](../Page/再生能源.md "wikilink")，以達致[環境保護的效益](../Page/環境保護.md "wikilink")。

浸大物業處處長[林朗秋指出](../Page/林朗秋.md "wikilink")，這座大宅為一級歷史建築物，他們在活化過程中刻意保留了建築物的原有特色。大宅的門、窗和地板均全數有保留，[跌打館內的雷生春及敬福堂](../Page/跌打館.md "wikilink")[牌匾亦被重置原位](../Page/牌匾.md "wikilink")，還原跌打館的風味\[8\]。

雷生春堂於2012年4月25日正式開幕，26日起向社會提供中醫服務，預料每日可以為70-80人服務\[9\]，當中20%名額預留給綜援人士。綜援人士與65歲以上長者可以享有優惠診症收費，雷生春堂亦會不定時提供免費義診服務，預計每日名額1百個。屆時將會成為[香港浸會大學中醫藥學院旗下第](../Page/香港浸會大學中醫藥學院.md "wikilink")14間中醫診所，為保留雷生春提供醫藥跌打的傳統，於診所底層售賣浸大配方的[凉茶](../Page/凉茶.md "wikilink")\[10\]\[11\]。

## 曾居住雷生春之人物

  - [雷添良](../Page/雷添良.md "wikilink")：中華人民共和國香港特別行政區[第十二屆全國人民代表大會代表](../Page/第十二屆全國人民代表大會.md "wikilink")、[羅兵咸永道會計師事務所高級顧問](../Page/羅兵咸永道.md "wikilink")、[香港會計師公會前主席](../Page/香港會計師公會.md "wikilink")
  - [伍立德](../Page/伍立德.md "wikilink")：[香港電台電視部記者](../Page/香港電台.md "wikilink")/編導，[香港記者協會執委](../Page/香港記者協會.md "wikilink")（2015-2016，2016-2017）

## 流行文化

2016年美國超級英雄電影《[奇異博士](../Page/奇異博士_\(電影\).md "wikilink")》中有3大至聖所，分別在[紐約](../Page/纽约.md "wikilink")、[倫敦和香港](../Page/伦敦.md "wikilink")，其中香港的至聖所就以雷生春為設計藍本。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[太子站](../Page/太子站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 參見

  - [雷亮家族](../Page/雷亮家族.md "wikilink")
  - [九龍巴士](../Page/九龍巴士.md "wikilink")

## 参考資料

## 外部連結

  - [古物古蹟辦事處-雷生春](https://web.archive.org/web/20060411192533/http://www.lcsd.gov.hk/ce/Museum/Monument/b5/built_reuse1.php)
  - [香港地方：建設及建築物：歷史建築（三）九龍](http://www.hk-place.com/view.php?id=236)
  - [活化歷史建築伙伴計劃：雷生春資料冊](http://www.heritage.gov.hk/tc/doc/%E9%9B%B7%E7%94%9F%E6%98%A5%E8%B3%87%E6%96%99%E5%86%8A.pdf)
  - [香港浸會大學中醫藥保健中心-雷生春堂](http://www.heritage.gov.hk/doc/rhbtp/Lui%20Seng%20Chun.pdf)
  - [立法會財委會：4QW活化計劃－活化雷生春為香港浸會大學中醫藥保健中心](http://www.legco.gov.hk/yr09-10/chinese/fc/pwsc/papers/p10-09c.pdf)
  - [香港浸會大學中醫藥學院-雷生春堂](http://scm.hkbu.edu.hk/lsc)

{{-}}

[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")
[Category:香港浸會大學](../Category/香港浸會大學.md "wikilink")
[Category:旺角](../Category/旺角.md "wikilink")
[Category:香港唐樓](../Category/香港唐樓.md "wikilink")
[Category:轉角唐樓](../Category/轉角唐樓.md "wikilink")
[Category:活化歷史建築伙伴計劃](../Category/活化歷史建築伙伴計劃.md "wikilink")

1.
2.

3.  [活化雷生春
    港府無良策](https://web.archive.org/web/20070504045330/http://hk.news.yahoo.com/070501/60/26kmm.html)
    雅虎新聞，2007年5月2日

4.  [活化歷史建築伙伴計劃 －
    首批歷史建築](http://www.heritage.gov.hk/tc/rhbtp/buildings.htm)

5.  [第一期活化計劃 -
    香港歷史文物-保育·活化](http://www.heritage.gov.hk/tc/rhbtp/ProgressResult_Lui_Seng_Chun.htm)

6.  [雷生春發展為浸大中醫藥中心](http://paper.wenweipo.com/2011/01/15/HK1101150028.htm)，文匯報

7.

8.  [雷生春首兩月看病六折](https://archive.is/20120710011512/hk.news.yahoo.com/%E9%9B%B7%E7%94%9F%E6%98%A5%E9%A6%96%E5%85%A9%E6%9C%88%E7%9C%8B%E7%97%85%E5%85%AD%E6%8A%98-211500831.html)
    《明報》 2012年4月12日

9.  [雷生春堂活化成中醫診所月底開幕](http://hk.news.yahoo.com/雷生春堂活化成中醫診所月底開幕-031200265.html)

10. [雷生春活化後本月底揭幕　提供中醫服務](http://hk.news.yahoo.com/雷生春活化後本月底揭幕-提供中醫服務-033500571.html)

11. [雷生春重生 4月26日開診](http://orientaldaily.on.cc/cnt/news/20120412/00176_021.html)
    《東方日報》 2012年4月12日