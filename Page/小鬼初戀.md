《**小鬼初戀**》（）是一部1991年出品的劇情片，主角是[丹·艾克洛德](../Page/丹·艾克洛德.md "wikilink")、[潔美·李·寇蒂斯](../Page/潔美·李·寇蒂斯.md "wikilink")、[麥考利·克金](../Page/麥考利·克金.md "wikilink")、[安娜·克倫斯基以及](../Page/安娜·克倫斯基.md "wikilink")[彼得·麥可·古茲](../Page/彼得·麥可·古茲.md "wikilink")。

影片劇情的背景是設定於1972年的[美國](../Page/美國.md "wikilink")[賓州](../Page/賓州.md "wikilink")[麥迪遜](../Page/麥迪遜.md "wikilink")，故事描述一位小女孩（由[安娜·克倫斯基演出](../Page/安娜·克倫斯基.md "wikilink")）的成長，以及週遭人物的變故。影片名稱則是取自1965年的一首歌曲《My
Girl》。

## 外部連結

  -
[Category:1991年電影](../Category/1991年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1990年代喜劇片](../Category/1990年代喜劇片.md "wikilink")
[Category:美國浪漫喜劇片](../Category/美國浪漫喜劇片.md "wikilink")
[Category:美國喜劇劇情片](../Category/美國喜劇劇情片.md "wikilink")
[Category:1970年代背景電影](../Category/1970年代背景電影.md "wikilink")
[Category:賓夕法尼亞州背景電影](../Category/賓夕法尼亞州背景電影.md "wikilink")
[Category:哥倫比亞影業電影](../Category/哥倫比亞影業電影.md "wikilink")
[Category:佛羅里達州取景電影](../Category/佛羅里達州取景電影.md "wikilink")