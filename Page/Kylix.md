**Kylix**是[Borland公司推出的](../Page/Borland.md "wikilink")[GNU/Linux版的开发环境](../Page/GNU/Linux.md "wikilink")，相对于[Windows下的](../Page/Windows.md "wikilink")[Delphi以及](../Page/Delphi.md "wikilink")[C++
Builder](../Page/C++_Builder.md "wikilink")。通过Kylix，程序员可以在[GNU/Linux下使用](../Page/GNU/Linux.md "wikilink")[Object
Pascal](../Page/Object_Pascal.md "wikilink")、[C++或者](../Page/C++.md "wikilink")[C语言](../Page/C语言.md "wikilink")，进行软件开发。

目前这个工具的前景不明朗，没有推出新版（Kylix 4）的迹象。对于Delphi 2005，也没有升级的途径，Delphi
2005可能不包含[CLX](../Page/CLX.md "wikilink")（跨平台元件库）的支持。此外，对其最新的[GNU/Linux官方支持包括](../Page/GNU/Linux.md "wikilink")：RedHat
7.2、SUSE 7.3以及Mandrake
8.2。在更新版的GNU/Linux中，应该也能用，但可能需要搜索一下网络，看看如何对默认配置进行相应的调整，比如保留旧版的[glibc等](../Page/glibc.md "wikilink")。

## 参见

  - [Delphi和](../Page/Delphi.md "wikilink")[Object
    Pascal](../Page/Object_Pascal.md "wikilink")

  - [Lazarus和](../Page/Lazarus.md "wikilink")[Free
    Pascal](../Page/Free_Pascal.md "wikilink")

  - [VCL](../Page/VCL.md "wikilink")

  - [LCL](../Page/Lazarus.md "wikilink")

  -
## 外部链接

  - [Kylix官方网站](https://web.archive.org/web/20050604003105/http://www.borland.com/kylix/)
  - [kylixforum.de.vu德国Kylix论坛的英语板块](https://web.archive.org/web/20051103004722/http://kylixforum.betterproducts.de/viewforum.php?f=18)
  - [CrossKylix](http://crosskylix.untergrund.net/)：一个第三方软件，可以在Delphi下编译Linux本地程序
  - [CrossFPC](http://crossfpc.untergrund.net/)：一个开发中的第三方工具集，将[Free
    Pascal的编译器整合到Delphi中](../Page/Free_Pascal.md "wikilink")。在Free
    Pascal编译器的支持下，可以针对多种操作系统（包括Linux）进行编译。
  - [有关跨平台开发的讨论](http://www.codecomments.com/Kylix/message541921.html)

[Category:Pascal](../Category/Pascal.md "wikilink")
[Category:Borland软件](../Category/Borland软件.md "wikilink")
[Category:集成开发环境](../Category/集成开发环境.md "wikilink")