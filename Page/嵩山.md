**嵩山**古名又称**外方**、**嵩高**、**崇高**，位于[河南省中部](../Page/河南省.md "wikilink")，属[伏牛山系](../Page/伏牛山.md "wikilink")，地处[登封市西北面](../Page/登封市.md "wikilink")，是[五岳的](../Page/五岳.md "wikilink")**中岳**。总面积约为450平方公里，嵩山又分为**[少室山](../Page/少室山.md "wikilink")**和**[太室山](../Page/太室山.md "wikilink")**两部分，共72峰，最高峰**连天峰**高达1512米，峻极峰海拔1491米，古诗曰：“嵩高维岳，峻极于天”，中岳嵩山无论从自然地质还是文化遗存上，都堪称五岳之尊、万山之祖。\[1\]

1982年，嵩山被[中国国务院列为第一批国家级风景名胜区](../Page/中国国务院.md "wikilink")。2004年2月13日被[联合国教科文组织地学部评选为](../Page/联合国教科文组织.md "wikilink")“[世界地质公园](../Page/世界地质公园.md "wikilink")”。2007年5月8日，**嵩山少林景区**又被中国国家旅游局批准为[国家5A级旅游景区](../Page/国家5A级旅游景区.md "wikilink")。嵩山是儒释道共处的圣地之一，不仅是中国[道教圣地](../Page/道教.md "wikilink")，[汉传佛教的发祥地](../Page/汉传佛教.md "wikilink")，也是中国新[儒教的诞生地](../Page/儒教.md "wikilink")。\[2\]

## 地理环境

[Songshan.JPG](https://zh.wikipedia.org/wiki/File:Songshan.JPG "fig:Songshan.JPG")
嵩山地處[中原](../Page/中原.md "wikilink")，东西横卧，古称“外方”，夏商时称“嵩高”、“崇山”，[西周时称岳山](../Page/西周.md "wikilink")。公元前770年平王迁都洛阳后，以“嵩为中央、左岱、右华”，为“天地之中”，稱中嶽嵩山。海拔最低为350米，最高处为少室山的连天峰，海拔1512米，主峰峻极峰位于太室山，高1491.7米。\[3\]嵩山北瞰[黃河](../Page/黃河.md "wikilink")、[洛水](../Page/洛水.md "wikilink")，南臨潁水、箕山，東接五代[京都](../Page/中國首都.md "wikilink")[汴梁](../Page/汴梁.md "wikilink")，西連九朝古都[洛陽](../Page/洛陽.md "wikilink")，素有“汴洛兩京、畿內名山”之稱。於奇異的峻峰，宮觀林立，故為中原地區第一名山。\[4\]

## 地质演变

嵩山因为地质演化纪录上的多样性而被批准为世界地质公园。嵩山地质构造以其岩龄古老、构造复杂、地壳发育完整、出露良好而闻名。在嵩山72峰里，从[太古代到](../Page/太古代.md "wikilink")[新生代的岩层都有发现](../Page/新生代.md "wikilink")，地质史上的[太古宙](../Page/太古宙.md "wikilink")、[元古宙](../Page/元古宙.md "wikilink")、[古生代](../Page/古生代.md "wikilink")、[中生代](../Page/中生代.md "wikilink")、[新生代的地壳和岩石均有出露](../Page/新生代.md "wikilink")，被地质学界称为“五世同堂”。嵩山地区的[岩浆岩](../Page/岩浆岩.md "wikilink")、[沉积岩](../Page/沉积岩.md "wikilink")、[变质岩的出露](../Page/变质岩.md "wikilink")，构成了中国最古老的岩系，登封群的“登封朵岩”。[华北地区几乎所有的岩石和地层类型都集中于此](../Page/华北.md "wikilink")，时间跨度达30亿年。嵩山古老的岩石系形成于23亿年前，保存最为完好的是反映太古代至[古生代三次地质构造运动](../Page/古生代.md "wikilink")——“嵩阳运动”、“中岳运动”、“少林运动”的遗迹。嵩山成为许多地质学研究者们的热点考察区域。\[5\]

## 历史人文

### 名胜古迹

嵩山曾有30多位皇帝、150多位著名文人所親临，更是神仙相聚对话的洞天福地。由於名勝古蹟眾多，勘稱於五嶽。《[诗经](../Page/诗经.md "wikilink")》有“嵩高惟岳，峻极于天”的名句。始建于[秦朝的](../Page/秦朝.md "wikilink")[中嶽廟](../Page/中嶽廟.md "wikilink")，是中國古代建築群之一。太室山南麓的[嵩阳书院](../Page/嵩阳书院.md "wikilink")，是中国古代四大书院之一。太室山南麓的[法王寺](../Page/法王寺_\(登封\).md "wikilink")，是中国最早的寺庙之一。以[少林武術聞名於天下的](../Page/少林武術.md "wikilink")[少林寺](../Page/少林寺.md "wikilink")，就是位於嵩山的少室山中，歷代高僧都長眠於此。

### 文学作品

嵩山的水流量虽然不是很大，但却赋予了嵩山灵动和神韵。山间涌泉，星罗棋布；溪水潺潺，汇积成潭；卢崖瀑布，最为壮观。[唐朝进士](../Page/唐朝.md "wikilink")[郑谷有](../Page/郑谷.md "wikilink")“八景诗”曰：


## 参看

  - [少林寺](../Page/少林寺.md "wikilink")
  - [天地之中](../Page/天地之中.md "wikilink")
  - [嵩岳寺塔](../Page/嵩岳寺塔.md "wikilink")

## 参考资料

[Category:国家5A级旅游景区](../Category/国家5A级旅游景区.md "wikilink")
[Category:伏牛山](../Category/伏牛山.md "wikilink")
[Category:河南山峰](../Category/河南山峰.md "wikilink")
[Category:河南旅游景点](../Category/河南旅游景点.md "wikilink")
[Category:郑州地理](../Category/郑州地理.md "wikilink")
[Category:登封市](../Category/登封市.md "wikilink")
[Category:中国山峰](../Category/中国山峰.md "wikilink")

1.
2.
3.
4.
5.