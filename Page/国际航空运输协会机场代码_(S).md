| 代碼       | 機場                                                             | 城市                                            | 国家和地区                                                                |
| -------- | -------------------------------------------------------------- | --------------------------------------------- | -------------------------------------------------------------------- |
| SAN      | [聖地牙哥國際機場](../Page/聖地牙哥國際機場.md "wikilink")                     | [聖地牙哥](../Page/聖地牙哥_\(加利福尼亞州\).md "wikilink") | [美國](../Page/美國.md "wikilink")                                       |
| SCL      | [阿圖羅·梅里諾·貝尼特斯准將國際機場](../Page/阿圖羅·梅里諾·貝尼特斯准將國際機場.md "wikilink") | [聖地亞哥](../Page/聖地亞哥_\(智利\).md "wikilink")     | [智利](../Page/智利.md "wikilink")                                       |
| SDA\[1\] | [巴格達國際機場](../Page/巴格達國際機場.md "wikilink")                       | [巴格達](../Page/巴格達.md "wikilink")              | [伊拉克](../Page/伊拉克.md "wikilink")                                     |
| SDJ      | [仙台機場](../Page/仙台機場.md "wikilink")                             | [宮城縣](../Page/宮城縣.md "wikilink")              | [日本](../Page/日本.md "wikilink")                                       |
| SDK      | [山打根機場](../Page/山打根機場.md "wikilink")                           | [山打根](../Page/山打根.md "wikilink")              | [馬來西亞](../Page/馬來西亞.md "wikilink")                                   |
| SEA      | [西雅圖-塔科馬國際機場](../Page/西雅圖-塔科馬國際機場.md "wikilink")               | [西雅圖](../Page/西雅圖.md "wikilink")              | [美國](../Page/美國.md "wikilink")                                       |
| SEZ      | [塞席爾國際機場](../Page/塞席爾國際機場.md "wikilink")                       | [馬埃島](../Page/馬埃島.md "wikilink")              | [塞席爾](../Page/塞席爾.md "wikilink")                                     |
| SFO      | [舊金山國際機場](../Page/舊金山國際機場.md "wikilink")                       | [旧金山](../Page/旧金山.md "wikilink")              | [美國](../Page/美國.md "wikilink")                                       |
| SGN\[2\] | [新山一國際機場](../Page/新山一國際機場.md "wikilink")                       | [胡志明市](../Page/胡志明市.md "wikilink")            | [越南](../Page/越南.md "wikilink")                                       |
| SHA      | [上海虹桥国际机场](../Page/上海虹桥国际机场.md "wikilink")                     | [上海市](../Page/上海市.md "wikilink")              | [中国](../Page/中華人民共和國.md "wikilink")                                  |
| SHE      | [沈阳桃仙国际机场](../Page/沈阳桃仙国际机场.md "wikilink")                     | [沈阳市](../Page/沈阳市.md "wikilink")              | [中国](../Page/中華人民共和國.md "wikilink")                                  |
| SHM      | [南紀白濱機場](../Page/南紀白濱機場.md "wikilink")                         | [和歌山縣](../Page/和歌山縣.md "wikilink")            | [日本](../Page/日本.md "wikilink")                                       |
| SIN      | [樟宜机场](../Page/樟宜机场.md "wikilink")                             | [新加坡](../Page/新加坡.md "wikilink")              | [新加坡](../Page/新加坡.md "wikilink")                                     |
| SJC      | [聖荷西國際機場](../Page/聖荷西國際機場.md "wikilink")                       | [聖荷西](../Page/聖荷西_\(美國加州\).md "wikilink")     | [美國](../Page/美國.md "wikilink")                                       |
| SJJ      | [塞拉耶佛國際機場](../Page/塞拉耶佛國際機場.md "wikilink")                     | [塞拉耶佛](../Page/塞拉耶佛.md "wikilink")            | [波士尼亞](../Page/波斯尼亚和黑塞哥维那.md "wikilink")                             |
| SJU      | [聖胡安國際機場](../Page/聖胡安國際機場.md "wikilink")                       | [聖胡安](../Page/聖胡安.md "wikilink")              | [波多黎各](../Page/波多黎各.md "wikilink")                                   |
| SJW      | [石家莊正定機場](../Page/石家莊正定機場.md "wikilink")                       | [石家莊市](../Page/石家莊市.md "wikilink")            | [中國](../Page/中華人民共和國.md "wikilink")                                  |
| SLC      | [鹽湖城國際機場](../Page/鹽湖城國際機場.md "wikilink")                       | [鹽湖城](../Page/鹽湖城.md "wikilink")              | [美國](../Page/美國.md "wikilink")                                       |
| SMF      | [沙加緬度國際機場](../Page/沙加緬度國際機場.md "wikilink")                     | [沙加緬度](../Page/沙加緬度.md "wikilink")            | [美國](../Page/美國.md "wikilink")                                       |
| SOU      | [南安普敦機場](../Page/南安普敦機場.md "wikilink")                         | [南安普敦](../Page/南安普敦.md "wikilink")            | [英國](../Page/英國.md "wikilink")                                       |
| SPN      | [塞班國際機場](../Page/塞班國際機場.md "wikilink")                         | [塞班島](../Page/塞班島.md "wikilink")              | [北馬里亞納群島](../Page/北馬里亞納群島.md "wikilink")                             |
| SSG      | [马拉博圣伊莎贝尔机场](../Page/马拉博圣伊莎贝尔机场.md "wikilink")                 | [马拉博](../Page/马拉博.md "wikilink")              | [赤道几内亚](../Page/赤道几内亚.md "wikilink")                                 |
| STL      | [聖路易蘭伯特國際機場](../Page/聖路易蘭伯特國際機場.md "wikilink")                 | [聖路易](../Page/聖路易.md "wikilink")              | [美國](../Page/美國.md "wikilink")                                       |
| STN      | [倫敦斯坦斯特德機場](../Page/倫敦斯坦斯特德機場.md "wikilink")                   | [倫敦](../Page/倫敦.md "wikilink")                | [英國](../Page/英國.md "wikilink")                                       |
| SUB      | [祝安達國際機場](../Page/祝安達國際機場.md "wikilink")                       | [泗水](../Page/泗水_\(印尼\).md "wikilink")         | [印尼](../Page/印尼.md "wikilink")                                       |
| SUV      | [瑙索里國際機場](../Page/瑙索里國際機場.md "wikilink")                       | [蘇瓦](../Page/蘇瓦.md "wikilink")                | [斐濟](../Page/斐濟.md "wikilink")                                       |
| SVO      | [舍列梅季耶夫國際機場](../Page/舍列梅季耶夫國際機場.md "wikilink")                 | [莫斯科](../Page/莫斯科.md "wikilink")              | [俄羅斯](../Page/俄羅斯.md "wikilink")                                     |
| SXB      | [史特拉斯堡機場](../Page/史特拉斯堡機場.md "wikilink")                       | [史特拉斯堡](../Page/史特拉斯堡.md "wikilink")          | [法國](../Page/法國.md "wikilink")                                       |
| SXF      | [柏林-舍內費爾德機場](../Page/柏林-舍內費爾德機場.md "wikilink")                 | [柏林](../Page/柏林.md "wikilink")                | [德國](../Page/德國.md "wikilink")                                       |
| SXJ      | [鄯善机场](../Page/鄯善机场.md "wikilink")                             | [鄯善县](../Page/鄯善县.md "wikilink")              | [中國](../Page/中華人民共和國.md "wikilink")                                  |
| SXM      | [茱莉安娜公主國際機場](../Page/茱莉安娜公主國際機場.md "wikilink")                 | [荷屬聖馬丁](../Page/荷屬聖馬丁.md "wikilink")          | [荷屬安地列斯](../Page/荷屬安地列斯.md "wikilink")                               |
| SYD      | [京斯福特·史密斯機場](../Page/京斯福特·史密斯機場.md "wikilink")                 | [雪梨](../Page/雪梨市.md "wikilink")               | [澳洲](../Page/澳洲.md "wikilink")                                       |
| SYX      | [三亚凤凰国际机场](../Page/三亚凤凰国际机场.md "wikilink")                     | [三亚市](../Page/三亚市.md "wikilink")              | [中国](../Page/中華人民共和國.md "wikilink")                                  |
| SZB      | [梳邦机场](../Page/梳邦机场.md "wikilink")                             | [梳邦](../Page/梳邦.md "wikilink")                | [马来西亚](../Page/马来西亚.md "wikilink")                                   |
| SZG      | [薩爾斯堡機場](../Page/薩爾斯堡機場.md "wikilink")                         | [薩爾斯堡](../Page/薩爾斯堡.md "wikilink")            | [奧地利](../Page/奧地利.md "wikilink")                                     |
| SZX      | [深圳宝安国际机场](../Page/深圳宝安国际机场.md "wikilink")                     | [深圳市](../Page/深圳市.md "wikilink")              | [中国](../Page/中華人民共和國.md "wikilink")                                  |
| SCN      | Ensheim                                                        | 索爾布呂肯                                         | [德國](../Page/德國.md "wikilink")                                       |
| SAB      |                                                                | Saba Island                                   | [荷蘭](../Page/荷蘭.md "wikilink")                                       |
| SPD      |                                                                | Saidpur                                       | [孟加拉](../Page/孟加拉.md "wikilink")                                     |
| STC      | Saint Cloud                                                    | Saint Cloud                                   | [美國](../Page/美國.md "wikilink")                                       |
| SGU      | St George                                                      | Saint George                                  | [美國](../Page/美國.md "wikilink")                                       |
| SNO      |                                                                | Sakon Nakhon                                  | [泰國](../Page/泰國.md "wikilink")                                       |
| SID      | Amilcar Cabral International                                   | Sal                                           | [Cape Verde](../Page/Cape_Verde.md "wikilink")                       |
| SLN      | Salina Municipal                                               | Salina                                        | [美國](../Page/美國.md "wikilink")                                       |
| SBY      | Salisbury                                                      | Salisbury                                     | [美國](../Page/美國.md "wikilink")                                       |
| SLA      | International                                                  | Salta                                         | [阿根廷](../Page/阿根廷.md "wikilink")                                     |
| SLW      | Saltillo                                                       | Saltillo                                      | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| SSA      | Dois De Julho                                                  | Salvador                                      | [巴西](../Page/巴西.md "wikilink")                                       |
| SKD      |                                                                | Samarkand                                     | [烏茲別克斯坦](../Page/烏茲別克斯坦.md "wikilink")                               |
| SAQ      |                                                                | San Andros/Andros Island                      | [巴哈馬](../Page/巴哈馬.md "wikilink")                                     |
| SJT      |                                                                | San Angelo                                    | [美國](../Page/美國.md "wikilink")                                       |
| SVZ      |                                                                | 聖安東尼                                          | [委內瑞拉](../Page/委內瑞拉.md "wikilink")                                   |
| SAT      | San Antonio International                                      | 聖安東尼奧                                         | [美國](../Page/美國.md "wikilink")                                       |
| SJO      | Juan Santamaria International                                  | 聖荷塞                                           | [哥斯大黎加](../Page/哥斯大黎加.md "wikilink")                                 |
| SJI      | Mcguire Field                                                  | 聖荷塞                                           | [菲律賓](../Page/菲律賓.md "wikilink")                                     |
| SJD      | Los Cabos Intl Airport                                         | San Jose Del Cabo                             | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| SBP      | SanLuisObispoCountyAirport                                     | San Luis Obispo                               | [美國](../Page/美國.md "wikilink")                                       |
| SLP      |                                                                | San Luis Potosi                               | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| SPR      | San Pedro                                                      | San Pedro                                     | [伯利茲](../Page/伯利茲.md "wikilink")                                     |
| SAP      | La Mesa                                                        | San Pedro Sula                                | [洪都拉斯](../Page/洪都拉斯.md "wikilink")                                   |
| SAL      | El Salvadore Intl Airport                                      | San Salvador                                  | [薩爾瓦多](../Page/薩爾瓦多.md "wikilink")                                   |
| SOM      |                                                                | 聖多美                                           | [委內瑞拉](../Page/委內瑞拉.md "wikilink")                                   |
| SAH      | [萨那国际机场](../Page/萨那国际机场.md "wikilink")                         | [萨那](../Page/萨那.md "wikilink")                | [葉門](../Page/葉門.md "wikilink")                                       |
| SDN      |                                                                | Sandane                                       | [挪威](../Page/挪威.md "wikilink")                                       |
| SSJ      | Stokka                                                         | Sandnessjoen                                  | [挪威](../Page/挪威.md "wikilink")                                       |
| SBA      | Santa Barbara Airport                                          | 桑塔巴巴拉                                         | [美國](../Page/美國.md "wikilink")                                       |
| SPC      | [拉帕尔马机场](../Page/拉帕尔马机场.md "wikilink")                         | [拉帕爾馬島](../Page/拉帕爾馬島.md "wikilink")          | [西班牙](../Page/西班牙.md "wikilink")                                     |
| SFN      |                                                                | 聖德菲                                           | [阿根廷](../Page/阿根廷.md "wikilink")                                     |
| SAF      | Santa FeMunicipal Airport                                      | 聖大非                                           | [美國](../Page/美國.md "wikilink")                                       |
| SMX      | Santa MariaPublic Airport                                      | Santa Maria                                   | [美國](../Page/美國.md "wikilink")                                       |
| STS      | Sonoma County Airport                                          | Santa Rosa                                    | [美國](../Page/美國.md "wikilink")                                       |
| SDR      | Santander                                                      | Santander                                     | [西班牙](../Page/西班牙.md "wikilink")                                     |
| STM      |                                                                | Santarem                                      | [巴西](../Page/巴西.md "wikilink")                                       |
| SCU      | Santiago                                                       | Santiago                                      | [古巴](../Page/古巴.md "wikilink")                                       |
| STI      | Municipal                                                      | Santiago                                      | [多明尼加](../Page/多明尼加.md "wikilink")                                   |
| SCQ      | Santiago                                                       | Santiago De Compostela                        | [西班牙](../Page/西班牙.md "wikilink")                                     |
| SDQ      | Las Americas                                                   | [圣多明各](../Page/圣多明各.md "wikilink")            | [多明尼加](../Page/多明尼加.md "wikilink")                                   |
| STD      | Mayor Humberto Vivas Guerrero                                  | Santo Domingo                                 | [委內瑞拉](../Page/委內瑞拉.md "wikilink")                                   |
| SLZ      | Tirirical                                                      | Sao Luiz                                      | [巴西](../Page/巴西.md "wikilink")                                       |
| SLK      | Adirondack                                                     | Saranac Lake                                  | [美國](../Page/美國.md "wikilink")                                       |
| SRQ      | Sarasota                                                       | Sarasota/Bradenton                            | [美國](../Page/美國.md "wikilink")                                       |
| SAV      | TravisField                                                    | [萨凡纳](../Page/萨凡纳.md "wikilink")              | [美國](../Page/美國.md "wikilink")                                       |
| SVL      | [萨翁林纳机场](../Page/萨翁林纳机场.md "wikilink")                         | [萨翁林纳](../Page/萨翁林纳.md "wikilink")            | [芬蘭](../Page/芬蘭.md "wikilink")                                       |
| SVU      | Savvu                                                          | Savusavu                                      | [斐濟](../Page/斐濟.md "wikilink")                                       |
| SDX      |                                                                | 塞多納                                           | [美國](../Page/美國.md "wikilink")                                       |
| SMM      | Semporna                                                       | Semporna                                      | [馬來西亞](../Page/馬來西亞.md "wikilink")                                   |
| SVQ      |                                                                | [塞维利亚](../Page/塞维利亚.md "wikilink")            | [西班牙](../Page/西班牙.md "wikilink")                                     |
| SFA      | Sfax Airport                                                   | Sfax                                          | [突尼斯](../Page/突尼斯.md "wikilink")                                     |
| SNN      | [香农机场](../Page/香农机场.md "wikilink")                             | [香农](../Page/香农.md "wikilink")                | [愛爾蘭](../Page/愛爾蘭.md "wikilink")                                     |
| SHJ      | [沙迦国际机场](../Page/沙迦国际机场.md "wikilink")                         | [沙迦](../Page/沙迦.md "wikilink")                | [阿拉伯聯合酋長國](../Page/阿拉伯聯合酋長國.md "wikilink")                           |
| SSH      | [沙姆沙伊赫国际机场](../Page/沙姆沙伊赫国际机场.md "wikilink")                   | [沙姆沙伊赫](../Page/沙姆沙伊赫.md "wikilink")          | [埃及](../Page/埃及.md "wikilink")                                       |
| SHR      | Sheridan County Airport                                        | Sheridan                                      | [美國](../Page/美國.md "wikilink")                                       |
| SYZ      | Shiraz                                                         | Shiraz                                        | [伊朗](../Page/伊朗.md "wikilink")                                       |
| SYO      | Shonai                                                         | Shonai                                        | [日本](../Page/日本.md "wikilink")                                       |
| SOW      |                                                                | Show Low                                      | [美國](../Page/美國.md "wikilink")                                       |
| SHV      | Regional Airport                                               | Shreveport                                    | [美國](../Page/美國.md "wikilink")                                       |
| SBW      | Sibu                                                           | Sibu                                          | [馬來西亞](../Page/馬來西亞.md "wikilink")                                   |
| SDY      |                                                                | Sidney                                        | [美國](../Page/美國.md "wikilink")                                       |
| SIP      | Simferopol                                                     | Simferopol                                    | [烏克蘭](../Page/烏克蘭.md "wikilink")                                     |
| SIT      | Sitka                                                          | Sitka                                         | [美國](../Page/美國.md "wikilink")                                       |
| SGY      |                                                                | Skagway                                       | [美國](../Page/美國.md "wikilink")                                       |
| SFT      | Skelleftea                                                     | Skelleftea                                    | [瑞典](../Page/瑞典.md "wikilink")                                       |
| SKP      | Skopje                                                         | Skopje                                        | [馬其頓](../Page/馬其頓.md "wikilink")                                     |
| SXL      | Collooney                                                      | Sligo                                         | [愛爾蘭](../Page/愛爾蘭.md "wikilink")                                     |
| SOF      | [索菲亚机场](../Page/索菲亚机场.md "wikilink")                           | [索非亞](../Page/索非亞.md "wikilink")              | [保加利亞](../Page/保加利亞.md "wikilink")                                   |
| SOG      | Haukasen                                                       | Sogndal                                       | [挪威](../Page/挪威.md "wikilink")                                       |
| SHO      |                                                                | Sokcho                                        | [南韓](../Page/南韓.md "wikilink")                                       |
| SGD      | Sonderborg                                                     | Sonderborg                                    | [丹麥](../Page/丹麥.md "wikilink")                                       |
| SBN      | Michiana Regional Airport                                      | South Bend                                    | [美國](../Page/美國.md "wikilink")                                       |
| SOP      | Pinehurst                                                      | Southern Pines                                | [美國](../Page/美國.md "wikilink")                                       |
| SPW      | Spencer Municipal Airport                                      | Spencer                                       | [美國](../Page/美國.md "wikilink")                                       |
| SGF      | Springfield Regional Airport                                   | [春田](../Page/春田.md "wikilink")                | [美國](../Page/美國.md "wikilink")                                       |
| SPI      | Capital Airport                                                | [春田](../Page/春田.md "wikilink")                | [美國](../Page/美國.md "wikilink")                                       |
| SXR      |                                                                | Srinagar                                      | [印度](../Page/印度.md "wikilink")                                       |
| SBH      |                                                                | St Barthelemy                                 | [Guadeloupe](../Page/Guadeloupe.md "wikilink")                       |
| STX      |                                                                | St Croix Island                               | [美國](../Page/美國.md "wikilink")                                       |
| SSB      | Sea Plane Base                                                 | St Croix Island                               | [美國](../Page/美國.md "wikilink")                                       |
| SGO      | St George                                                      | St George                                     | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| SKB      | Golden Rock                                                    | St Kitts                                      | [Saint Kitts And Nevis](../Page/Saint_Kitts_And_Nevis.md "wikilink") |
| SLU      | Vigie Field                                                    | [聖盧西亞](../Page/聖盧西亞.md "wikilink")            | [聖盧西亞](../Page/聖盧西亞.md "wikilink")                                   |
| SMS      |                                                                | [聖瑪麗](../Page/聖瑪麗.md "wikilink")              | [馬達加斯加](../Page/馬達加斯加.md "wikilink")                                 |
| SFG      | Esperance                                                      | [荷属圣马丁](../Page/荷属圣马丁.md "wikilink")          | [荷属安地列斯](../Page/荷属安地列斯.md "wikilink")                               |
| STP      | Downtown Airport                                               | [聖保羅](../Page/聖保羅.md "wikilink")              | [美國](../Page/美國.md "wikilink")                                       |
| STT      | CyrilE King Arpt                                               | St Thomas Island                              | [美國](../Page/美國.md "wikilink")                                       |
| SPB      | SeaplaneBase                                                   | St Thomas Island                              | [美國](../Page/美國.md "wikilink")                                       |
| SVD      |                                                                | [聖文森特](../Page/聖文森特.md "wikilink")            | [聖文森特和格林納丁斯群島](../Page/聖文森特和格林納丁斯群島.md "wikilink")                   |
| SCE      | University Park Arpt                                           | State College                                 | [美國](../Page/美國.md "wikilink")                                       |
| SHD      | Shenandoah Valley Regional                                     | Staunton                                      | [美國](../Page/美國.md "wikilink")                                       |
| SVG      | Sola                                                           | [斯塔万格](../Page/斯塔万格.md "wikilink")            | [挪威](../Page/挪威.md "wikilink")                                       |
| SML      |                                                                | Stella Maris                                  | [巴哈馬](../Page/巴哈馬.md "wikilink")                                     |
| SYY      | Stornoway                                                      | Stornoway                                     | [聯合王國](../Page/聯合王國.md "wikilink")                                   |
| STR      | Echterdingen                                                   | [斯圖加特](../Page/斯圖加特.md "wikilink")            | [德國](../Page/德國.md "wikilink")                                       |
| SRE      | Sucre                                                          | Sucre                                         | [波利維亞](../Page/波利維亞.md "wikilink")                                   |
| SWQ      | Brang Bidji                                                    | Sumbawa Island                                | [印度尼西亞](../Page/印度尼西亞.md "wikilink")                                 |
| SUN      |                                                                | Sun Valley/Hailey                             | [美國](../Page/美國.md "wikilink")                                       |
| SDL      | Sundsvall                                                      | Sundsvall                                     | [瑞典](../Page/瑞典.md "wikilink")                                       |
| SGC      |                                                                | Surgut                                        | [俄羅斯](../Page/俄羅斯.md "wikilink")                                     |
| SWA      | [揭阳潮汕机场](../Page/揭阳潮汕机场.md "wikilink")                         | [揭阳](../Page/揭阳.md "wikilink")                | [中国](../Page/中华人民共和国.md "wikilink")                                  |
| SWP      |                                                                | Swakopmund                                    | [納米比亞](../Page/納米比亞.md "wikilink")                                   |
| SYR      | Hancock International                                          | [雪城](../Page/雪城.md "wikilink")                | [美國](../Page/美國.md "wikilink")                                       |
| SZZ      | Goleniow                                                       | Szczecin                                      | [波蘭](../Page/波蘭.md "wikilink")                                       |

## 注釋

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")

1.  IATA代碼使用機場舊名薩達姆·海珊國際機場（Saddam Hussein Airport）之縮寫
2.  IATA代碼使用胡志明市的舊稱[西貢](../Page/西貢.md "wikilink")（Saigon）之縮寫