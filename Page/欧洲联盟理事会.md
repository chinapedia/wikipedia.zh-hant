[Justus_Lipsius,_Eastern_side.jpg](https://zh.wikipedia.org/wiki/File:Justus_Lipsius,_Eastern_side.jpg "fig:Justus_Lipsius,_Eastern_side.jpg")歐盟理事會的總部Justus
Lipsius大楼\]\]

**欧洲联盟理事会**（），简称**欧盟理事会**，是[欧洲联盟事实上的](../Page/欧洲联盟.md "wikilink")[兩院制](../Page/兩院制.md "wikilink")[立法機關](../Page/立法機關.md "wikilink")[上議院](../Page/上議院.md "wikilink")，由來自28個[歐盟成員國各國政府部長所組成的理事會](../Page/歐盟.md "wikilink")；與[歐洲議會為是歐盟的主要決策機構](../Page/歐洲議會.md "wikilink")。每一個國家在理事會中都有一名理事，但代表不同国家的理事所拥有的投票数不同。在2004年羅馬條約簽屬之後，通常也稱之為“部長理事會”，目的是為了把它和[欧洲理事会的理事即國家元首或政府首腦區分開來](../Page/欧洲理事会.md "wikilink")。

歐盟理事會正式的具體名稱應該是“欧洲共同体理事會”，不過這一名稱使用並不廣泛。歐盟理事會俗稱**歐盟部長理事會**\[1\]，在歐盟官方內部也簡稱“理事會”。

歐盟理事會在沒有特殊情況下，通常在[比利時](../Page/比利時.md "wikilink")[首都](../Page/首都.md "wikilink")[布魯塞爾召開](../Page/布魯塞爾.md "wikilink")。

## 组成

理事会有一名主席和一名秘书长，实行轮换制，由各成员国轮流出任，每六个月轮换一次。欧盟理事会轮值主席国外交部长出任主席。理事会秘书长由欧盟各成员国联合推举任命，他同样是[共同外交与安全政策高级代表](../Page/共同外交与安全政策.md "wikilink")。2009年[里斯本条约生效之後](../Page/里斯本条约.md "wikilink")，理事會主席由輪任主席國的政府首腦每半年一任（兼任），改為經由選舉產生的職位，任期二年半，可連任一次。

## 任务

欧盟部长理事会主要任务是帮助整合欧洲共同体各个国家间事务，制定欧盟法律和法规。在预算方面，它和[欧洲议会共同拥有决策权](../Page/欧洲议会.md "wikilink")。理事会的另一主要权利在货币方面，它负责引导货币交易率方面的政策。

## 运作方式

理事会有辅助性质的日常办事机构包括\[2\]：

  - 常驻代表团委员会，由各成员国驻欧盟的使团长和副使团长组成，每周举行例会。
  - 政治和安全委员会，是处理欧盟[共同外交与安全政策的](../Page/共同外交与安全政策.md "wikilink")“轴心”，由各成员国驻欧盟代表、欧盟委员会代表和理事会秘书长的代表组成。

### 欧盟理事会各司

歐盟理事會是一個單一的法律實體，且根據所涉及的主題在10個部門中召開會議。

1.  主要事务司
2.  外界关系司
3.  经济和贸易事务司
4.  法律和内政事务司
5.  就业、社会政策、卫生和消费司
6.  竞争力司（内部市场，工业，科研）
7.  交通、通讯和能源司
8.  农业和渔业司
9.  环境司
10. 教育、青年和文化司

## 現任主席國和秘書長

現任[歐洲聯盟理事會主席國為](../Page/歐洲聯盟理事會主席國.md "wikilink")[保加利亚共和国](../Page/保加利亚共和国.md "wikilink")，於2018年上任輪值。

前任為[德國](../Page/德國.md "wikilink")[柏林人](../Page/柏林.md "wikilink")。

## 参考文献

## 外部链接

  - [欧盟官方主页](http://www.consilium.europa.eu/showPage.ASP?lang=en)

  - [欧盟理事会
    CVCE](http://www.cvce.eu/obj/the_council_of_the_european_union_and_the_european_council-en-30129edd-45e3-4c2e-8cfe-2e7e301f10e8.html)

[Category:歐盟組織](../Category/歐盟組織.md "wikilink")
[欧洲联盟理事会](../Category/欧洲联盟理事会.md "wikilink")
[Category:跨國立法機構](../Category/跨國立法機構.md "wikilink")
[Category:上议院](../Category/上议院.md "wikilink")

1.  請參閱[駐臺辦事處網頁](https://eeas.europa.eu/delegations/taiwan_zh-hant)
2.  <http://www.chinamission.be/chn/rsom/omlsh/t88300.htm>