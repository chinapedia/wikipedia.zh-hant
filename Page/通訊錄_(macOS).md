**通訊錄**，是指電話簿軟體應用程式（address book software application），目前美國蘋果電腦只有[Mac OS
X有支援這項功能](../Page/Mac_OS_X.md "wikilink")。

## 特色

  - 輸入與輸出完整之規格（Exports and imports cards to and from
    [vCard](../Page/vCard.md "wikilink") format.）
  - 可從LDIF輸入。（Imports cards from LDIF, tab-delimited, and
    comma-separated files）
  - 與其它應用程式溝通介面（[API](../Page/Application_Programming_Interface.md "wikilink")
    to interface with other applications.）
  - [AppleScript](../Page/AppleScript.md "wikilink")
    支援查詢與新增、修改功能成員與群組。（support for
    querying, adding, modifying, and removing people and groups）
  - [iSync](../Page/iSync.md "wikilink") 可與手機，PDAs, iPods，及其它Macs產品同步化。
  - 與[iChat](../Page/iChat.md "wikilink") 的整合。
  - 列印標籤（Prints labels）
  - 列印郵件列表（Print mailing lists）
  - 位址更動的改變（Change of address notification）
  - 連絡群組（Contact groups）
  - 智慧群組（Smart groups）
  - 當輸入時自動合併（Auto-merge when importing vCards）
  - 欄位與類型的客制化（Customize fields and categories）
  - 電話號碼格式的自動化（Automatic formatting of phone numbers）
  - 與微軟的系統同步化（Synchronizes with [Microsoft Exchange
    Server](../Page/Microsoft_Exchange_Server.md "wikilink")）
  - 語音辨認搜尋（[Speech
    recognition](../Page/Speech_recognition.md "wikilink") searching）
  - 整合藍芽（Integrates [Bluetooth](../Page/Bluetooth.md "wikilink")-enabled
    phones (displaying incoming and missed calls, displaying incoming
    [text messages](../Page/Short_message_service.md "wikilink")，lets
    you send text messages using the phone, etc.)）
  - 具查詢資料庫的功能。Capability to query an
    [LDAP](../Page/Lightweight_Directory_Access_Protocol.md "wikilink")
    database containing person information
  - 可整合協力廠商的程式。[Plugin](../Page/Plugin.md "wikilink") interface so that
    [third-party](../Page/third-party.md "wikilink") developers can add
    functionality to the program

[C](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")