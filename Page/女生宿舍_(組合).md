**女生宿舍**，[大國文化在](../Page/大國文化.md "wikilink")2004年9月17日所推出的[女子音樂組合](../Page/女子音樂組合.md "wikilink")，由名經理人[李進在各地發掘出來的新人組合而成的](../Page/李進.md "wikilink")，共有4個成員，包括[陳美詩](../Page/陳美詩.md "wikilink")（Macy）、[廖雋嘉](../Page/廖雋嘉.md "wikilink")（Elise）、[梁靖琪](../Page/梁靖琪.md "wikilink")（Toby）與（Bella）。已于2006年年初解散。其中[陳美詩來自](../Page/陳美詩.md "wikilink")[香港](../Page/香港.md "wikilink")；[廖雋嘉則是一個](../Page/廖雋嘉.md "wikilink")[湖南的女生](../Page/湖南.md "wikilink")，於[廣州星海音樂學院讀書時被發掘](../Page/廣州星海音樂學院.md "wikilink")；[梁靖琪則來自](../Page/梁靖琪.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")；則是來自[大陸內地](../Page/大陸.md "wikilink")，在[廣州被發掘](../Page/廣州.md "wikilink")。由於背景不同，因此可朝向多元化的工作發展，也可兼顧[香港與](../Page/香港.md "wikilink")[大陸兩地市場](../Page/大陸.md "wikilink")。在2004年時，在[now.com.hk所播放的](../Page/now.com.hk.md "wikilink")[網劇](../Page/網劇.md "wikilink")—[金甲蟲探偵學院演出](../Page/金甲蟲探偵學院.md "wikilink")，一炮而紅，並發行第一張專輯[Life
Is
Beautiful](../Page/Life_Is_Beautiful.md "wikilink")，獲得不錯的成績。但由於4位成員各有各的發展，因此之後僅有再合作出寫真集，並無4人再合作的機會。[梁靖琪及](../Page/梁靖琪.md "wikilink")[陳美詩曾為](../Page/陳美詩.md "wikilink")[無綫電視旗下藝人](../Page/無綫電視.md "wikilink")，主要從事電視劇的拍攝；[廖雋嘉在內地繼續音樂上的發展](../Page/廖雋嘉.md "wikilink")；而已退出娛樂圈，并育有一女。

## 唱片

  - [Life Is Beautiful](../Page/Life_Is_Beautiful.md "wikilink")

<!-- end list -->

  -

      -
        發行日期：[2004年11月27日](../Page/2004年11月27日.md "wikilink")

## 電影

  - [千杯不醉](../Page/千杯不醉.md "wikilink")（Drink, Drank, Drunk）

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")、[廖雋嘉](../Page/廖雋嘉.md "wikilink")
        上映日期：2005年4月18日

<!-- end list -->

  - [飄移凶間](../Page/飄移凶間.md "wikilink")（Demoniac Flash）

<!-- end list -->

  -

      -
        演出成員：[廖雋嘉](../Page/廖雋嘉.md "wikilink")、
        上映日期：2005年5月26日

<!-- end list -->

  - [擁抱每一刻花火](../Page/擁抱每一刻花火.md "wikilink")（Moments of Love）

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")
        上映日期：2005年5月26日

<!-- end list -->

  - [得閒飲茶](../Page/得閒飲茶.md "wikilink")（I Will Call You）

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")
        上映日期：2006年3月23日

<!-- end list -->

  - [傷城](../Page/傷城.md "wikilink")（Confession of Pain）

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")
        上映日期：2006年12月23日

<!-- end list -->

  - [校墓處](../Page/校墓處.md "wikilink")(The Haunted School)

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")
        上映日期：2007年3月15日

<!-- end list -->

  - [我不賣身、我賣子宮](../Page/我不賣身、我賣子宮.md "wikilink")（True Women For Sale）

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")
        上映日期：2008年

<!-- end list -->

  - [保持愛你](../Page/保持愛你.md "wikilink")（Love Connected）

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")
        上映日期：2009年2月19日

<!-- end list -->

  - [72家租客](../Page/72家租客.md "wikilink")（72 Tenants of Prosperity）

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")
        上映日期：2010年2月11日

## 戲劇表演

### 2004

  - 廣播劇—[四個女仔搞乜鬼](../Page/四個女仔搞乜鬼.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")、[廖雋嘉](../Page/廖雋嘉.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")、

<!-- end list -->

  - [新城電台廣播劇](../Page/新城電台.md "wikilink")—[愛上．女生宿舍](../Page/愛上．女生宿舍.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")、[廖雋嘉](../Page/廖雋嘉.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")、

<!-- end list -->

  - \[now.com.hk\]網劇—[金甲蟲探偵學院](../Page/金甲蟲探偵學院.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")、[廖雋嘉](../Page/廖雋嘉.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")、

<!-- end list -->

  - 香港[TVB靈幻單元劇](../Page/TVB.md "wikilink")“[奇幻潮](../Page/奇幻潮.md "wikilink")—神奇藥水”

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")、[廖雋嘉](../Page/廖雋嘉.md "wikilink")、[梁靖琪](../Page/梁靖琪.md "wikilink")、

### 2005

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [男人之苦](../Page/男人之苦.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - [PSP網劇](../Page/PSP.md "wikilink")—[心跳季節之](../Page/心跳季節.md "wikilink")「[夏日故事：雙重角色](../Page/夏日故事：雙重角色.md "wikilink")」

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 音樂舞台劇—[開心鬼](../Page/開心鬼.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 赤沙印記@四葉草.2

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink") ,
        [陳美詩](../Page/陳美詩.md "wikilink")

### 2007

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [歲月風雲](../Page/歲月風雲.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [兩妻時代](../Page/兩妻時代.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

### 2008

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [法證先鋒II](../Page/法證先鋒II.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [與敵同行](../Page/與敵同行.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [師奶股神](../Page/師奶股神.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

### 2009

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [Click入黃金屋](../Page/Click入黃金屋.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [ID精英](../Page/ID精英.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [王老虎搶親](../Page/王老虎搶親.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [古靈精探B](../Page/古靈精探B.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [蔡鍔與小鳳仙](../Page/蔡鍔與小鳳仙.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

### 2010

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [情人眼裏高一D](../Page/情人眼裏高一D.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [掌上明珠](../Page/掌上明珠.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [摘星之旅](../Page/摘星之旅.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [讀心神探](../Page/讀心神探.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [囧探查過界](../Page/囧探查過界.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

### 2011

  - 香港[TVB單元電視劇](../Page/TVB.md "wikilink")
    [你們我們他們](../Page/你們我們他們.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink") [Only You
    只有您](../Page/Only_You_只有您.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [洪武三十二](../Page/洪武三十二.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [花花世界花家姐](../Page/花花世界花家姐.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [團圓](../Page/團圓_\(香港電視劇\).md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [荃加福祿壽探案](../Page/荃加福祿壽探案.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [萬凰之王](../Page/萬凰之王.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

### 2012

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [結·分@謊情式](../Page/結·分@謊情式.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [回到三國](../Page/回到三國.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

### 2013

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [戀愛季節](../Page/戀愛季節.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [女警愛作戰](../Page/女警愛作戰.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[陳美詩](../Page/陳美詩.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [衝上雲霄II](../Page/衝上雲霄II.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [My盛Lady](../Page/My盛Lady.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

### 2014

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [廉政行動2014](../Page/廉政行動2014.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [點金勝手](../Page/點金勝手.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [使徒行者](../Page/使徒行者.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

### 2015

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [潮拜武當](../Page/潮拜武當.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

<!-- end list -->

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [實習天使](../Page/實習天使.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

### 2017

  - 香港[TVB電視劇](../Page/TVB.md "wikilink")
    [味想天開](../Page/味想天開.md "wikilink")

<!-- end list -->

  -

      -
        演出成員：[梁靖琪](../Page/梁靖琪.md "wikilink")

## 寫真集

  - Life is Beautiful 女生宿舍寫真集

<!-- end list -->

  -

      -
        發行日期：[2005年7月17日](../Page/2005年7月17日.md "wikilink")

## 廣告

  - [必勝客](../Page/Pizza_Hut.md "wikilink")（2005）

## 獎項

### 2004

  - [Yahoo](../Page/Yahoo.md "wikilink")\!雅虎香港「[Yahoo\!搜尋人氣大獎2004](../Page/Yahoo!搜尋人氣大獎2004.md "wikilink")」演藝新力軍組別
  - [TVB](../Page/TVB.md "wikilink")「[勁歌金曲](../Page/勁歌金曲.md "wikilink")2004優秀選第二回」新星試打
  - 「[PM第三屆人氣歌手奪標頒獎禮](../Page/PM.md "wikilink")」PM新進閃爍女子組合
  - 「[PM第三屆樂壇頒獎禮](../Page/PM.md "wikilink")」PM火熱概念組合新人獎
  - [廣東電台](../Page/廣東電台.md "wikilink")「2004年音樂先鋒榜」廣東最受歡迎女子組合獎—金獎
  - [廣州電台](../Page/廣州電台.md "wikilink")「2004年金曲金榜頒獎晚會」港台金曲獎—「全女打」
  - [廣州電台](../Page/廣州電台.md "wikilink")「2004年金曲金榜頒獎晚會」新登場女組合大獎
  - [新城電台](../Page/新城電台.md "wikilink")「勁爆頒獎禮2004」勁爆新人王（組合）
  - [香港電台](../Page/香港電台.md "wikilink")「第二十七屆十大中文金曲頒獎禮」最有前途新人獎（組合）銅獎

## 外部連結

  - [大國文化](http://www.musicnationgroup.com/)
  - [女生宿舍討論區](https://web.archive.org/web/20060517001856/http://libunion.suddenlaunch3.com/index.cgi)

[Category:香港女子演唱團體](../Category/香港女子演唱團體.md "wikilink")
[Category:已解散的女子演唱團體](../Category/已解散的女子演唱團體.md "wikilink")