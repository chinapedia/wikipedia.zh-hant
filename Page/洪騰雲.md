**洪騰雲**（；），[字](../Page/表字.md "wikilink")**合樂**，一字**会乐**，[號](../Page/號.md "wikilink")**忠慎**、**合益**，[譜名](../Page/譜名.md "wikilink")**球唱**。[臺灣](../Page/臺灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")[艋舺人](../Page/艋舺.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[貢生](../Page/貢生.md "wikilink")，知名商人，賞四品[同知銜](../Page/同知.md "wikilink")。1880年，他捐銀建造考棚，獲得清廷建造[急公好義坊予以表揚](../Page/急公好義坊.md "wikilink")。急公好義坊現存於[臺北市](../Page/臺北市.md "wikilink")[二二八公園](../Page/二二八公園.md "wikilink")。

## 生平

洪騰雲原籍[泉州府](../Page/泉州府.md "wikilink")[晉江縣](../Page/晉江縣.md "wikilink")，六歲隨父定居[臺北](../Page/臺北.md "wikilink")[艋舺](../Page/艋舺.md "wikilink")。成年後，洪騰雲，並從事[泉州與](../Page/泉州.md "wikilink")[艋舺間](../Page/艋舺.md "wikilink")[米](../Page/米.md "wikilink")、[食鹽生意](../Page/食鹽.md "wikilink")，因而致富。不僅是富商，洪騰雲也以好善樂施著名。其中最重大的捐獻，就是位於[臺北城東北角的考棚行署](../Page/臺北城.md "wikilink")。

考棚行署，俗稱考棚。就是舉行[科舉考試的場所](../Page/科舉.md "wikilink")，在此考棚建好之前，全台灣只有[台南府城有此場地](../Page/台南府城.md "wikilink")。[台灣清治時期之後](../Page/台灣清治時期.md "wikilink")，會定期於[台南府考棚舉行科舉考試](../Page/台南府.md "wikilink")。不過，位於[臺北一帶兩千名以上的考生](../Page/臺北.md "wikilink")，卻都要遠赴約三百公里遠的[台南赴考](../Page/台南.md "wikilink")，實在相當不方便。於是洪騰雲特於1880年捐地以及[銀元來興建考試用的考棚](../Page/銀元.md "wikilink")。如此一來，[北台灣的考生就不用至路途遙遠的](../Page/北台灣.md "wikilink")[南台灣考試](../Page/南台灣.md "wikilink")。

1887年[臺灣巡撫](../Page/臺灣巡撫.md "wikilink")[劉銘傳上稟](../Page/劉銘傳.md "wikilink")[朝廷此一義舉](../Page/朝廷.md "wikilink")，並請[聖旨建立旌表類](../Page/聖旨.md "wikilink")[牌坊來表揚洪騰雲](../Page/牌坊.md "wikilink")，獲得了當時[光緒帝](../Page/光緒帝.md "wikilink")[敕令批准](../Page/敕令.md "wikilink")。於是特別在石坊街，即現在的[台北市衡陽路](../Page/台北市.md "wikilink")），蓋[牌坊來褒揚洪之義舉](../Page/牌坊.md "wikilink")，是為[急公好義坊](../Page/急公好義坊.md "wikilink")。

1899年，洪騰雲病逝。

據[日治時代](../Page/臺灣日治時代.md "wikilink")[臺灣總督府的調查](../Page/臺灣總督府.md "wikilink")，臺北地區有三大富豪：排名第三的是[艋舺的洪騰雲有資產](../Page/艋舺.md "wikilink")20萬[圓](../Page/圓.md "wikilink")，排名第二的是[大稻埕的](../Page/大稻埕.md "wikilink")「番勢－[李春生](../Page/李春生.md "wikilink")」有資產120萬圓，排名第一的就是[板橋林家第四代的](../Page/板橋林家.md "wikilink")[林維源](../Page/林維源.md "wikilink")，有資產1億1000萬圓\[1\]。一說調查中的「圓」，非[日圓](../Page/日圓.md "wikilink")，而是[銀圓](../Page/銀圓.md "wikilink")，如此則其身家又加倍，當時一銀圓相當於一[美元](../Page/美元.md "wikilink")，可兌換約2[日圓](../Page/日圓.md "wikilink")。

## 後人

  - 孫：[洪以南](../Page/洪以南.md "wikilink")，擔任[瀛社首任社長](../Page/瀛社.md "wikilink")，為知名詩人，書畫家。

<!-- end list -->

  - 曾孫：[洪長庚](../Page/洪長庚.md "wikilink")，則為臺灣第一位[眼科](../Page/眼科.md "wikilink")[醫學博士](../Page/醫學博士.md "wikilink")，洪以南之子。
  - [洪致文](../Page/洪致文.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[鐵道文化研究者與](../Page/鐵道迷.md "wikilink")[氣象學家](../Page/氣象學.md "wikilink")，洪長庚之孫。

## 參考

<references/>

  - 《臺灣歷史辭典》.許雪姬 ISBN 9570174293

[Category:台灣清治時期人物](../Category/台灣清治時期人物.md "wikilink")
[H](../Category/台灣清治時期企業家.md "wikilink")
[T](../Category/洪姓.md "wikilink") [H](../Category/萬華人.md "wikilink")
[H](../Category/晉江人.md "wikilink")

1.  《臺灣，沒說你不知道：生活在這塊土地的你可以拿來說嘴的七十則冷知識》 ISBN 9789571065519