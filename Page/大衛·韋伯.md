{{ infobox politician | name = 大衛·韋伯 | birth_name = David Michael Webb
| birth_date =  | birth_place = 英國倫敦 | image = 20130519 David Webb.jpg
| imagesize = | caption = 2013年的大衛·韋伯 | alma_mater =
[牛津大學埃克塞特學院](../Page/牛津大學埃克塞特學院.md "wikilink") |
nationality =  | spouse = | children = | religion = | residence =  |
occupation = 活動家 | profession = 投資銀行家 | website =
<https://webb-site.com> }}  **大衛·迈克尔·韋伯**
（，），香港傳媒一般直接使用其英文名字。生於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，是香港證監會收購及合併委員會副主席，[香港交易所前](../Page/香港交易所.md "wikilink")[非執行董事](../Page/非執行董事.md "wikilink")，[香港](../Page/香港.md "wikilink")[股票投資評論員](../Page/股票.md "wikilink")，為[壹傳媒第二大股東](../Page/壹傳媒.md "wikilink")，曾是[新確科技主要](../Page/新確科技.md "wikilink")[股東](../Page/股東.md "wikilink")。

於1986年，大衛·韋伯在英國[牛津大學埃克塞特學院](../Page/牛津大學埃克塞特學院.md "wikilink")[數學系畢業](../Page/數學.md "wikilink")，曾寫過[Spectrum及](../Page/Spectrum.md "wikilink")[Commodore
64](../Page/Commodore_64.md "wikilink")[電腦遊戲](../Page/電腦遊戲.md "wikilink")。其後轉往金融界發展，在[投資銀行任職](../Page/投資銀行.md "wikilink")，1991年開始定居香港，1998年退休。他因經常批評香港部份上市公司管理層的不當行為而成名，被部份投資者視為代表小股東權益的聲音，但被大股東或上市公司視為「麻煩製造者」，故有「股壇[長毛](../Page/長毛.md "wikilink")」之稱。

2001年，韋伯獲選為香港交易所收購及合併委員會委員，及公眾股東權益小組成員。2003年，韋伯參與香港交易所董事選舉，成功當選。有指他獲得外資[基金的支持](../Page/基金.md "wikilink")。2006年4月港交所董事改選，韋伯以2.1億淨票數高票連任董事，他亦協助[陸恭蕙成功當選港交所董事](../Page/陸恭蕙.md "wikilink")。

韋伯是[香港證券專業學會會員](../Page/香港證券專業學會.md "wikilink")，他創辦的非牟利獨立[網站](../Page/網站.md "wikilink")
Webb-site.com，對香港財經界、經濟及政治發表評論。韋伯每年[聖誕節前](../Page/聖誕節.md "wikilink")，會公開推介一隻他在未來一年看好的股票，通常都得到不少公眾投資者追捧。

2017年5月，韋伯在其網站發表「50隻不應買的港股」名單，引起關注。\[1\]\[2\]，不過亦有股評家批評，列表不包括股價連跌十年的[壹傳媒](../Page/壹傳媒.md "wikilink")，韋伯為該公司的主要股東\[3\]。

## 榮譽

  - [商業周刊](../Page/商業周刊.md "wikilink") ( Business Week ) 亞洲之星，2000年
  - [世界經濟論壇](../Page/世界經濟論壇.md "wikilink") 明日世界領袖（Global Leader for
    Tomorrow），2001年
  - CFO雜誌 Global 100，2002年
  - [門薩國際](../Page/門薩國際.md "wikilink")（Mensa International）會員

## 資料來源

  - [David Webb當選港交所董事
    小股民對市場信心增加](http://www.atchinese.com/index.php?option=com_content&task=view&id=15760&Itemid=63)
  - [陸恭蕙Webb勝出 「經紀幫」失勢
    基金騎劫港交所](http://the-sun.orisun.com/channels/fina/20060427/20060427013815_0000.html)
  - [星島日報](../Page/星島日報.md "wikilink")[稱David Webb為
    香港股壇長毛](https://web.archive.org/web/20070609154159/http://hk.news.yahoo.com/070607/60/2926l.html)

## 外部連結

  - [Webb-site.com](http://www.webb-site.com/)

[Category:英國裔香港人](../Category/英國裔香港人.md "wikilink")
[Category:英国人](../Category/英国人.md "wikilink")
[Category:牛津大学埃克塞特学院校友](../Category/牛津大学埃克塞特学院校友.md "wikilink")
[Category:香港股評人](../Category/香港股評人.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")

1.
2.
3.  [蘋果日報捉妖股
    有冇計漏壹傳媒](https://www.am730.com.hk/column/%E8%B2%A1%E7%B6%93/%E8%98%8B%E6%9E%9C%E6%97%A5%E5%A0%B1%E6%8D%89%E5%A6%96%E8%82%A1-%E6%9C%89%E5%86%87%E8%A8%88%E6%BC%8F%E5%A3%B9%E5%82%B3%E5%AA%92-80029)