[Suzuki_XL7_Hill_Climb_Special.jpg](https://zh.wikipedia.org/wiki/File:Suzuki_XL7_Hill_Climb_Special.jpg "fig:Suzuki_XL7_Hill_Climb_Special.jpg")所駕駛的Suzuki
XL7 Hill Climb Special賽車\]\]
[Pike's_Peak_2006_Suzuki_Grand_Vitara.jpg](https://zh.wikipedia.org/wiki/File:Pike's_Peak_2006_Suzuki_Grand_Vitara.jpg "fig:Pike's_Peak_2006_Suzuki_Grand_Vitara.jpg")
**派克峰國際爬山賽**（，常簡稱為PPIHC），或被暱稱為「The Race to the
Clouds」，是一個每年都會在[美國](../Page/美國.md "wikilink")[科羅拉多州](../Page/科羅拉多州.md "wikilink")[科羅拉多泉](../Page/科羅拉多泉.md "wikilink")（Colorado
Spring）郊外的[派克峰](../Page/派克峰.md "wikilink")（Pikes
Peak）山頂所進行的汽機車[爬山賽](../Page/爬山賽.md "wikilink")（Hillclimb），是全世界比賽場地[海拔最高](../Page/海拔.md "wikilink")、車輛性能水準也最高的[越野賽車活動之一](../Page/越野賽車.md "wikilink")。在每年7月間進行的PPIHC首度舉辦於1916年，除了偶爾幾年曾因為[戰爭等因素停辦之外](../Page/戰爭.md "wikilink")，一直持續至今（2007年舉行的是第85屆），因此是美國境內歷史第二悠久的車輛競賽活動，僅次於每年在[印第安納波里斯所舉行的](../Page/印第安納波里斯.md "wikilink")[印第500大賽](../Page/印第500大賽.md "wikilink")（Indy500）。

## 簡介

PPIHC的舉辦場地屬於[派克峰公路](../Page/派克峰公路.md "wikilink")（Pikes Peak
Highway）的一部份，是一條通往派克峰頂，採部分[鋪裝](../Page/鋪裝.md "wikilink")（[柏油路面](../Page/柏油.md "wikilink")）、部分未鋪裝（[礫石路面](../Page/礫石.md "wikilink")）的山區道路。比賽的起點位於公路7英里路標、標高9,402[呎](../Page/呎.md "wikilink")（2866公尺）處，朝山頂開跑，賽道全程共12.42[英里](../Page/英里.md "wikilink")（接近20[公里](../Page/公里.md "wikilink")），其中包含61,626呎（18.8公里）長的上坡路段，與3,934呎（1.2公里）長的下坡路段，最大上坡坡度達10.5%，最大下坡坡度則為10.0%，平均坡度7%，終點海拔高達14,110呎（4301公尺），垂直落差達4,720呎（1,439公尺）。賽道沿途共有156個彎道，其中位於賽道後半段、標高12,750呎處的「無底洞」彎（Bottomless
Pit）是最驚險的一個彎道，假如在此失手有可能會垂直墜落達6,000呎的深淵中，所幸迄今從未有人在此彎道發生意外。由於比賽全程都位於三、四千公尺高的高山地區，稀薄的空氣對於參賽的車手與賽車都是極端嚴苛的考驗，除了空氣量不足可能會使車輛損失高達30%的動力輸出之外，[稀薄燃燒可能造成的引擎過熱問題也是負責調校賽車的工程師一大挑戰](../Page/稀薄燃燒.md "wikilink")，因此PPIHC常常被認為是越野賽車界最嚴苛的一場賽事。

PPIHC的參賽車種非常多，從一般的[汽車](../Page/汽車.md "wikilink")、[機車與](../Page/摩托車.md "wikilink")[全地形車](../Page/全地形車.md "wikilink")，到[拖車頭](../Page/拖車頭.md "wikilink")、[電動車乃至於競技專用的越野賽車](../Page/電動車.md "wikilink")，每一年的參賽組別都持續地在修改中。1916年第一屆比賽舉辦時，原本只有「Open
Wheel」（[車輪外露賽車](../Page/車輪外露賽車.md "wikilink")）一個組別，到2007年時，已經發展成多達19組的競賽分組。在各組之中車輛性能最強、速度也最快的是汽車組別中的「無限組」（Unlimited），使用的都是一些完全針對爬山賽特別打造的專用賽車。PPIHC賽史上的最快紀錄是由爬山賽老將、也是[鈴木世界拉力車隊](../Page/鈴木世界拉力車隊.md "wikilink")[經理的](../Page/車隊經理.md "wikilink")[日本籍車手](../Page/日本.md "wikilink")[田嶋伸博](../Page/田嶋伸博.md "wikilink")（[綽號](../Page/綽號.md "wikilink")「怪獸田嶋」，Monster
Tajima）所創下，他先是在2007年7月21日的正式賽中駕駛鈴木XL7 Hill Climb
Special賽車跑出10分1秒408的賽道記錄\[1\]，打破另一位爬山賽名人、在1994年時駕駛Toyota
Celica
GT賽車所創下的10分4秒06之舊記錄，此舊記錄高懸13年才被打破。2011年6月26日田嶋伸博駕著[怪獸運動SX4爬山賽特製賽車再度挑戰](../Page/鈴木SX4#競速賽事.md "wikilink")，結果創下9分51秒278的記錄\[2\]。作為比較，1916年PPIHC第一次舉辦時，當時的冠軍成績只有20分55秒06。

傳統上，PPIHC原本都是在歷年的美國[國慶日](../Page/國慶日.md "wikilink")（7月4日）當天進行正式決賽，但這傳統在2006年時開始有變化，改為在7月1日比賽，2007年則又更改到7月21日比賽。

## 現行分組

以下列出為2007年賽事中，一些主要的競賽組別與說明：

  - 汽車與卡車
      - Unlimited（無限組）：參賽車除了必須通過安全相關的規定之外，完全沒有任何規格上的限制，因此參賽車種通常都是針對爬山賽特別打造的專用賽車，也是越野賽車領域裡面輸出最大（近年來幾輛參賽車大都有超過1000[匹馬力的動力輸出](../Page/匹.md "wikilink")）、速度最快（田嶋曾在2006年時創下205公里/小時的極速）的車輛。無限組的參賽車在外觀上最明顯的特徵就是大量使用誇張的[空氣動力鈑件](../Page/空氣動力.md "wikilink")，雖然近年來比較流行使用大型[定風翼之類的設計手法](../Page/定風翼.md "wikilink")，但在過去此組曾出現過一些類似[C組賽車](../Page/C組賽車.md "wikilink")（Group
        C）、使用[地面效應來增加高速時車身下壓力的賽車](../Page/地面效應.md "wikilink")。
      - Pikes Peak
        Open（派克峰開放組）：此組別的參賽車與無限組同樣、可以在內部機件與結構上進行任意的改裝，唯一的要求就是必須保持量產車輛大致的外觀模樣，因此是在性能表現上僅次於無限組的組別，先前也曾出現過搭載前後兩具引擎、根本已經與原廠車無關的高度改裝賽車。
      - Super Stock
        Car（超級原廠車組）：此組別的參賽車是以車齡十年內的量產車進行局部程度改裝而成，雖然允許稍事更動安放引擎的幾何位置，但大體上來說規格與原廠車相去不遠，也是競爭最激烈的組別。
      - Showroom
        Stock（原廠車組）：除了增設一些安全相關的配備外，此組別的參賽車被要求必須保持原廠車出廠時的狀態，不能進行任何改裝。
      - Time Attack
        2WD/AWD（二輪/四輪驅動計時組）：此二組別的參賽車輛主要是以量產車為基礎，允許裝置以原廠設計為基礎、市面上購買得到的改裝零件。
      - Big
        Rig（拖車組）：此組別的參賽車大都是一些經過相當程度改裝的量產拖車頭，因此是越野賽車領域中排氣量最大（通常都是搭載超大排氣量[渦輪增壓](../Page/渦輪增壓.md "wikilink")[柴油引擎](../Page/柴油引擎.md "wikilink")）、噸位也最大的賽車。
      - Protruck（專業卡車組）：Protruck是一種使用[空間樑](../Page/空間樑.md "wikilink")（Space
        frame）車體、具有固定規格的越野卡車比賽。參加此組賽事的車輛，都必須符合由Protruck組織所定義的車輛規格。
      - Open
        Wheel（輪胎外露賽車組，OW）：PPIHC最原版的組別。所謂的[輪胎外露賽車是指專門為了參加競技而開發](../Page/輪胎外露賽車.md "wikilink")、使用開放式車體結構（例如管狀樑，Tube
        frame）再加上空氣動力鈑件的車輛，因此包括[一級方程式賽車與](../Page/一級方程式賽車.md "wikilink")[印第賽車之類的車輛](../Page/印第賽車.md "wikilink")，也都是輪胎外露賽車的一種。早年參加PPIHC之OW組的通常都是印第賽車風格的車種，但近年來則轉變為[沙灘車](../Page/沙灘車.md "wikilink")（Dune
        Buggy）風格的車輛居多。根據比賽規定，依照動動力系統設計方式不同，參賽車有不同的排氣量與馬力/重量比上限設定，但對於車身尺碼方面限制很少，因此性能表現與無限制組差異並不大。此組別的歷史紀錄是Robby
        Unser在1994年時所創下的10分05秒85成績，只比當時無限組的歷史紀錄慢了兩秒不到的時間。
      - Mini
        Sprint（迷你輪胎外露賽車組）：基本上是與OW組類似的組別，但限制使用六缸以下、排氣量不超過2.2公升的[自然進氣](../Page/自然進氣.md "wikilink")[四行程引擎與二輪驅動](../Page/四行程.md "wikilink")，因此在速度上不如OW組，主要著重的是車身的輕量化與過彎時的靈巧性。
      - Exhibition（示範組）：此組別的參賽車通常是一些無法被分類到其他標準組別、具有特殊設計理念或機械技術的賽車，在過去的賽事中此組曾出現過[丙烷動力車或](../Page/丙烷.md "wikilink")[電動車這類](../Page/電動車.md "wikilink")、使用[替代能源動力系統的車種](../Page/替代能源.md "wikilink")。
  - 機車與全地形車
      - Quads（全地形車組）：開放給[全地形車](../Page/全地形車.md "wikilink")（ATV，也就是四輪機車）參加的競賽組別，有多個排氣量不同的次分組。
      - Vintage（古董機車組）：此組別僅開放給排氣量650至750[立方釐米的雙缸機車參加](../Page/立方釐米.md "wikilink")，也就是傳統的「TT」賽車（Tourist
        Trophy，[旅遊杯賽](../Page/旅遊杯賽.md "wikilink")）。
      - Sidecar（邊車組）：唯一一個雙人參賽的組別，所使用的是加上了[邊車的機車](../Page/邊車.md "wikilink")，由其中一名選手擔任駕駛，另一位選手擔任乘客，負責利用坐姿的改變調整車輛[重心位置提升過彎速度](../Page/重心.md "wikilink")。
      - 250cc Pro
        Class（250cc專業機車組）：使用排氣量250立方釐米以下、[二行程](../Page/二行程.md "wikilink")/四行程單缸工廠賽車參與的組別。
      - 450cc Pro
        Class（450cc專業機車組）：除了剛出廠的[工廠賽車之外](../Page/工廠賽車.md "wikilink")，也常有一些排氣量適合的老式機車會參加此組別的競賽。
      - 750cc Pro
        Class（750cc專業機車組）：大部分參加此組別競賽的都是一些屬於[超級機車賽](../Page/超級機車賽.md "wikilink")（Superbike）等級的車輛。
      - 1200cc
        Pro（1200cc專業機車組）：提供給超大排氣量越野機車參賽的組別。在這組的比賽中，經常可以見到由[BMW所生產的工廠越野賽車參加](../Page/BMW.md "wikilink")。

## 相關作品

1989年時，[法國](../Page/法國.md "wikilink")[導演尚](../Page/導演.md "wikilink")-路易·穆雷（Jean-Louis
Mourey）拍攝了一部5分鐘長的記錄短片，名為《Climb
Dance》（攀巔之舞），片中記錄了前[世界越野錦標賽冠軍](../Page/世界越野錦標賽.md "wikilink")、[芬蘭車手](../Page/芬蘭.md "wikilink")[阿里·瓦塔寧](../Page/阿里·瓦塔寧.md "wikilink")（Ari
Vatanen）駕駛一輛屬於[B組賽車](../Page/B組賽車.md "wikilink")（Group
B）等級、配備有[四輪轉向](../Page/四輪轉向.md "wikilink")、[四輪驅動與渦輪增壓引擎的](../Page/四輪驅動.md "wikilink")[標緻](../Page/標緻汽車.md "wikilink")（Peugeot）[405
T16賽車參加當年PPIHC賽事的過程](../Page/標緻405_T16.md "wikilink")。這部短片在包括[夏慕尼影展](../Page/夏慕尼影展.md "wikilink")（Festival
du Chamonix）在內的多項電影競賽中獲獎，是賽車運動影片中經常被提到的著名作品。

在1999年上市的[新力](../Page/新力.md "wikilink")[PS2遊戲軟體](../Page/PS2.md "wikilink")《[跑車浪漫旅2](../Page/跑車浪漫旅.md "wikilink")》（Gran
Turismo 2）中，收錄了PPIHC的賽道與當年Suzuki的參賽車、Suzuki Escudo Pikes
Peak，該車款也是GT2遊戲中公認速度最快的一輛車。

## 參考文獻與註釋

  - [PPIHC官方網站](http://www.ppihc.com)

  - [PPIHC介紹（汽車線上資訊網）](http://www.auto-online.com.tw/news.php?CMD=open&UID=1905&CATEGORY=&from=0&keyword=%AC%A3%A7J)

<references/>

## 外部連結

  - 網站
      - [派克峰國際爬山賽官方網站](http://www.usacracing.com/ppihc)
      - [派克峰](http://www.pikespeakcolorado.com/)
      - [Monster-Sport（田嶋伸博所經營之車輛改裝廠）](http://www.monster-sport.com)
      - [MillenWorks（Rod
        Millen所經營之越野賽車隊/車廠）](http://www.millenworks.com/html/)
  - 影片
      - （PPIHC 2012意外事故回顧紀錄片，[Recaro官方頻道](../Page/Recaro.md "wikilink")）

      - （田嶋伸博2010年參賽紀錄影片，[大津輪胎官方頻道](../Page/大津輪胎.md "wikilink")）

[Category:拉力賽](../Category/拉力賽.md "wikilink")
[Category:美國體育競賽](../Category/美國體育競賽.md "wikilink")
[Category:科罗拉多州体育](../Category/科罗拉多州体育.md "wikilink")
[Category:美国的世界之最](../Category/美国的世界之最.md "wikilink")
[Category:7月份的活動](../Category/7月份的活動.md "wikilink")
[Category:1916年美國建立](../Category/1916年美國建立.md "wikilink")
[Category:1916年建立的週期性體育事件](../Category/1916年建立的週期性體育事件.md "wikilink")
[Category:派克斯峰](../Category/派克斯峰.md "wikilink")

1.  [Monster
    Sport官方新聞稿](http://www.monster-sport.com/competition/07/ppihc/0723/index.html)
2.  請看[Monday Motorsports: Tajima Breaks 10-Minute Barrier at Pikes
    Peak](http://wheels.blogs.nytimes.com/2011/06/27/monday-motorsports-tajima-breaks-10-minute-barrier-at-pikes-peak/?_r=0)。