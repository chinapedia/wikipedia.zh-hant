[2613_NRCS_projects_(27388292519).jpg](https://zh.wikipedia.org/wiki/File:2613_NRCS_projects_\(27388292519\).jpg "fig:2613_NRCS_projects_(27388292519).jpg")
[Stalls_on_the_footpath_up_to_Bachkovo_Monastery_01.jpg](https://zh.wikipedia.org/wiki/File:Stalls_on_the_footpath_up_to_Bachkovo_Monastery_01.jpg "fig:Stalls_on_the_footpath_up_to_Bachkovo_Monastery_01.jpg")
**蜂蜜**，-{zh-hans:又称**蜜糖**;zh-hant:在[香港稱為](../Page/香港.md "wikilink")**蜜糖**;zh-hk:即**蜂蜜**}-，是[昆蟲](../Page/昆蟲.md "wikilink")[蜜蜂從](../Page/蜜蜂.md "wikilink")[開花植物的](../Page/開花植物.md "wikilink")[花中採得的](../Page/花.md "wikilink")[花蜜在](../Page/花蜜.md "wikilink")[蜂巢中釀製的蜜](../Page/蜂巢.md "wikilink")，为半透明、带光泽、浓稠的[白色至](../Page/白色.md "wikilink")[淡黄色或橘黄色至黄褐色](../Page/淡黄色.md "wikilink")[液体](../Page/液体.md "wikilink")。自古被當成食物及藥物來使用，也被用於製作蠟燭等各種用品。中醫認為，蜂蜜性味甘、平，对[腹痛](../Page/腹痛.md "wikilink")、[干咳](../Page/干咳.md "wikilink")、[便秘等有疗效](../Page/便秘.md "wikilink")。\[1\]

蜂蜜（因已由蜜蜂的唾液中的酵素分解）為兩種[單糖類的](../Page/單糖.md "wikilink")[葡萄糖和](../Page/葡萄糖.md "wikilink")[果糖所構成](../Page/果糖.md "wikilink")，可以被人體直接吸收，而不需要先分解为单糖，所以比白砂糖（[蔗糖](../Page/蔗糖.md "wikilink")）更容易被人體吸收。成分除了[葡萄糖](../Page/葡萄糖.md "wikilink")、[果糖之外還含有各種](../Page/果糖.md "wikilink")[維生素](../Page/維生素.md "wikilink")、[礦物質和](../Page/礦物質.md "wikilink")[氨基酸](../Page/氨基酸.md "wikilink")。1[kg的蜂蜜含有](../Page/公斤.md "wikilink")2940[kcal的](../Page/千卡.md "wikilink")[熱量](../Page/熱量.md "wikilink")。

市售蜂蜜經過濃縮處理或天然封蓋熟成，[水分含量可低於](../Page/水分.md "wikilink")20%以下，[細菌和](../Page/細菌.md "wikilink")[酵母菌都不能在蜂蜜中存活](../Page/酵母菌.md "wikilink")，因此蜂蜜並不需要放入冰箱保存，某些[厭氧菌](../Page/厭氧菌.md "wikilink")（如[肉毒桿菌](../Page/肉毒桿菌.md "wikilink")）可以以非活性的[孢子形態存在其中](../Page/孢子.md "wikilink")，因為嬰幼兒[腸](../Page/腸.md "wikilink")[胃等](../Page/胃.md "wikilink")[消化器官过于稚嫩](../Page/消化器官.md "wikilink")，胃酸的分泌較差，所以，一歲內的嬰兒不要食用沒有經過[消毒的蜂蜜](../Page/消毒.md "wikilink")\[2\]。蜂蜜中[孢子並不會繁殖產生毒素](../Page/孢子.md "wikilink")，一般情況下，蜂蜜中的[厭氧菌也沒有在人體內繁殖的危險](../Page/厭氧菌.md "wikilink")。尚未封蓋熟成且未經濃縮處理的蜂蜜，因水分含量偏高，室溫下會快速發酵變質，因此仍需放入冰箱低溫保存。

## 蜂蜜的產生

蜜蜂從植物的花中採取含水量約為80%的花蜜或分泌物，存入自己第二個[胃中](../Page/胃.md "wikilink")，在體內轉化酶的作用下經過30分鐘的發酵，回到蜂巢中，一部分吐出給專職加熱的蜜蜂作為燃料\[3\]。蜂巢內溫度經常保持在35℃左右，經過一段時間，水份蒸發，成為水分含量少於20%的蜂蜜，存貯到巢洞中，用[蜂蠟密封](../Page/蜂蠟.md "wikilink")。

## 蜂蜜的結晶

蜂蜜是糖的[過飽和溶液](../Page/過飽和溶液.md "wikilink")，10\~15攝氏度時容易產生[結晶](../Page/結晶.md "wikilink")，生成結晶的是[葡萄糖](../Page/葡萄糖.md "wikilink")，不產生結晶的部分主要是[果糖](../Page/果糖.md "wikilink")。當加熱時結晶又會重新變回液體，並不是品質的問題。人們有時會以在低溫下是否結晶來區分純蜂蜜和加糖的蜂蜜，然而，將蜂蜜中的雜質過濾掉，也可以令蜂蜜不產生結晶，所以這方法並不可靠。
蜂蜜的结晶属于正常现象，而不同的蜂蜜因蜜蜂所采集的花蜜不同，所以结晶情况和条件也不一样，而果糖居多的蜂蜜一般结晶很慢，比如广西的龙眼蜜和枇杷蜜以及红枣蜜；而葡萄糖居多的易结晶，如荔枝蜜和冬蜜；而每一蜂蜜的结晶情况也不同，有的结晶细腻洁白比如冬蜜和东北的椴树蜜；而有的结晶呈颗粒状如荔枝蜜。而目前市场上流通的蜂蜜特别是超市专卖店的蜂蜜一般都为人工浓缩蜂蜜，使得葡萄糖含量少或者破坏了结晶核使得其很难结晶，再者结晶的条件不同使得即使同一批蜂蜜有些结晶快，有些结晶慢这些都属于正常现象。当然不能单靠蜂蜜结晶与否判断真假蜜。

## 物理特性

蜂蜜是一種[混合物](../Page/混合物.md "wikilink")，並且水分的含量不固定，因此其各項[物理特性的值會產生一定的浮動](../Page/物理.md "wikilink")。

  - [密度約為](../Page/密度.md "wikilink")1.4。
  - [結晶化的](../Page/結晶.md "wikilink")[溫度為](../Page/溫度.md "wikilink")10～15攝氏度，因所採取的花蜜種類不同而不同。
  - 作為流體，[粘度可以達到](../Page/粘度.md "wikilink")5000～6000cP（粘度的單位為P，1P=0.1Pa·s，通常用cP表示，水在20[攝氏度時的粘度為](../Page/攝氏度.md "wikilink")1cP。）[番茄醬的粘度為](../Page/番茄醬.md "wikilink")2000～3000cP，[沙拉醬的粘度約為](../Page/沙拉醬.md "wikilink")20萬cP。

## 蜂蜜的使用

  - 蜂蜜加熱並不會產生毒素，所以可以用於烘培、熱飲，但高溫會使大部分營養成分遭受破壞。通常不建議這樣使用
  - 最簡單的是用冰水或常溫水沖成飲料，也經常在麵包或烤餅上直接塗抹。
  - 有時也作為在[咖啡或](../Page/咖啡.md "wikilink")[紅茶等的飲料中代替糖作為](../Page/茶.md "wikilink")[調味品使用](../Page/調味品.md "wikilink")。蜂蜜的主要成分之一[果糖](../Page/果糖.md "wikilink")，在高溫時不容易感覺到甜味，所以在熱的飲品中添加蜂蜜時要注意不要過量。
  - 燒烤時加入蜂蜜，甜味和色澤會更好。
  - 在醫藥上，可以用於治療咳嗽\[4\]。作為外用藥，蜂蜜可以促進傷口癒合，治療[潰瘍](../Page/潰瘍.md "wikilink")。
  - 在[中藥上](../Page/中藥.md "wikilink")，常與中藥材的粉末一起作成蜜丸，稱為蜜煉。蜂蜜炒乾後作為栓劑，可治療便秘；內服有潤燥通便的療效，但也能緩和瀉藥（大黃、芒硝）的瀉下之力。
  - 從蜂巢中搾取蜂蜜後剩下的蜂巢的主要材料是[蜂蠟](../Page/蜂蠟.md "wikilink")，由工蜂所分泌，用來製作蜂巢。蜂蠟可以用來作為[蠟燭](../Page/蠟燭.md "wikilink")，[塗料等的原料](../Page/塗料.md "wikilink")。

## 蜂蜜的種類

蜂蜜因為花源的不同色、香、味和成分也不同，各國所產的蜂蜜也因花源的不同而有不同的顏色和形態。[國際食品法典委員會的蜂蜜國際標準中](../Page/國際食品法典委員會.md "wikilink")，針對部分不同植物蜜源制定有較高的蔗糖含量標準。
\[5\]

  - [荊條蜜](../Page/荊條.md "wikilink")
      - 特淺琥珀色，味芳香，結晶細膩白色，為上等蜜。
  - [椴樹蜜](../Page/椴樹.md "wikilink")
      - 色純，香濃，味甘甜，為上等蜜。
  - [紫雲英蜜](../Page/紫雲英.md "wikilink")
      - 色淡，微香，少異味。
  - [苜蓿花蜜](../Page/苜蓿.md "wikilink")
      - 全世界產量最多，有濃郁的香味和甜味，口感溫和。
  - [槐花蜜](../Page/槐花.md "wikilink")
      - 槐樹別名洋槐、刺槐，槐花蜜水白色，透明，不易結晶，氣味芳香，上等蜜。
  - [荔枝蜜](../Page/荔枝.md "wikilink")
      - 顏色較淡，氣味清香，易結晶，有荔枝香味。
  - [柑橘花蜜](../Page/柑橘.md "wikilink")
      - 色淡，微酸，結晶細膩。
  - [棗花蜜](../Page/棗樹.md "wikilink")
      - 棗花蜜琥珀色，甜度大，不易結晶，為上等蜜。
  - [鸭脚木蜜](../Page/鸭脚木.md "wikilink")
      - 也称深山冬蜜，是由土蜂采集广西珍贵中药树鸭脚木花蜜酿制而成，结晶后呈现乳白如猪油状，或浅黄色，味微苦，有清热解毒功效，特别对便秘有极其好的效果。
  - [百花蜜](../Page/百花蜜.md "wikilink")
      - 由各種植物酿制而成，味道較純花密複雜。味道、顏色因地域、季節差異可相差甚遠，但色澤通常較深。帶一點點酸味，十分普遍但營養價值最高。

### 臺灣主要蜜源

臺灣主要蜜源為[龍眼](../Page/龍眼.md "wikilink")(4-5月)、[荔枝](../Page/荔枝.md "wikilink")(3-4月)、[大花咸豐草](../Page/大花咸豐草.md "wikilink")(6-11月)、油菜(冬春季)\[6\]。依不同季節則因不同花源而產生文旦蜜、柑橘蜜、[烏桕蜜](../Page/烏桕.md "wikilink")、[哈密瓜蜜](../Page/哈密瓜.md "wikilink")、[咸豐草蜜](../Page/咸豐草.md "wikilink")、[蔓澤蘭蜜](../Page/蔓澤蘭.md "wikilink")、[鴨腳木蜜](../Page/鴨腳木.md "wikilink")，偶有時尚有[咖啡蜜](../Page/咖啡.md "wikilink")、[棗蜜](../Page/棗.md "wikilink")(以岡山棗為主)、[番石榴蜜](../Page/番石榴.md "wikilink")(燕巢等地)、[草莓蜜](../Page/草莓.md "wikilink")(苗栗大湖、後龍等，產量最稀少)，甚至還有[阿勃勒蜜](../Page/阿勃勒.md "wikilink")、[鳳凰木蜜等等不同](../Page/鳳凰木.md "wikilink")，主要不同在香味。依開花時節分則通常區分為春蜜、秋蜜及冬蜜。

## 主要產地和產量

[2005honey_(natural).PNG](https://zh.wikipedia.org/wiki/File:2005honey_\(natural\).PNG "fig:2005honey_(natural).PNG")

根据国际粮农组织（FAO）报道，2005年，[欧洲](../Page/欧洲.md "wikilink")、[中国](../Page/中国.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[土耳其和](../Page/土耳其.md "wikilink")[美国是天然蜂蜜的最大产地](../Page/美国.md "wikilink")。

蜂蜜的重要产地包括土耳其（全球第3）和[乌克兰](../Page/乌克兰.md "wikilink")（全球第5）\[7\]。[墨西哥提供了全球蜂蜜供应量的](../Page/墨西哥.md "wikilink")4.36%
\[8\]，其中有1/3来自[尤卡坦半岛](../Page/尤卡坦半岛.md "wikilink")。这里在20世纪引入[西方蜜蜂](../Page/西方蜜蜂.md "wikilink")（*Apis
mellifera*）和意大利蜜蜂（*A. mellifera
ligustica*）后开始养蜂。多数尤卡坦的养蜂户都是家庭式的小养殖户。他们多采用传统的技术，通过移动蜂巢来利用各种热带和亚热带花卉\[9\]。

蜂蜜同时也是法国[科西嘉岛的一种美食](../Page/科西嘉岛.md "wikilink")。科西嘉蜂蜜为原产地认证（[法国原产地命名控制](../Page/法国原产地命名控制.md "wikilink")）产品\[10\]。[塞尔维亚东部的](../Page/塞尔维亚.md "wikilink")[Homolje同样通过了原产地认证](../Page/Homolje.md "wikilink")\[11\]。

### 中国大陸的蜂蜜

蜂蜜在[中國大陸的主要生產地](../Page/中國大陸.md "wikilink")，按種類分：

  - 油菜花蜜：主要產於長江中下游地區如江蘇、江西、安徽、浙江，以及西北地區如甘肅、青海等。
  - 荊條蜜：主要產於華北和東北南部地區。如山西、河北、遼寧。荊條一般生長在海拔1000米以下的山溝、谷地、河流兩岸路旁和荒地。
  - 椴樹蜜：主要產於東北地區，特別是[長白山區](../Page/長白山.md "wikilink")，如[吉林](../Page/吉林.md "wikilink")、[黑龍江](../Page/黑龍江.md "wikilink")。
  - 槐花蜜：主要分佈於[黃河和](../Page/黃河.md "wikilink")[長江中下游地區](../Page/長江.md "wikilink")，如[山東](../Page/山東.md "wikilink")、[河南](../Page/河南.md "wikilink")、[河北](../Page/河北.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[陝西](../Page/陝西.md "wikilink")、[甘肅](../Page/甘肅.md "wikilink")。
  - 荔枝蜜：主要分佈於中國[華南地區](../Page/華南地區.md "wikilink")，如[廣東](../Page/廣東.md "wikilink")、[福建](../Page/福建.md "wikilink")、[廣西](../Page/廣西.md "wikilink")。
  - 棗花蜜：主要產區在[河南](../Page/河南.md "wikilink")、[山西](../Page/山西.md "wikilink")、[山東](../Page/山東.md "wikilink")、[河北](../Page/河北.md "wikilink")、[陝西](../Page/陝西.md "wikilink")、[寧夏](../Page/寧夏.md "wikilink")。
  - 紫雲英蜜：主要分佈於[長江流域](../Page/長江.md "wikilink")，如[安徽](../Page/安徽.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[江蘇省](../Page/江蘇.md "wikilink")。
  - 柑橘蜜：主要分佈於[長江以南地區](../Page/長江.md "wikilink")，如[廣東](../Page/廣東.md "wikilink")、[福建](../Page/福建.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[四川](../Page/四川.md "wikilink")、[浙江省](../Page/浙江.md "wikilink")。
  - 龙眼蜜：主要分布于广西、广东、福建，而广西居多，要数广西的龙眼蜜最正宗

### 臺灣的蜂蜜

[臺灣蜂蜜的產地大部分集中在臺灣中南部](../Page/臺灣.md "wikilink")，其盛名首推[高雄市](../Page/高雄市.md "wikilink")[大崗山](../Page/大崗山.md "wikilink")。

## 歷史

  - 蜂蜜與人類的關係非常古老，在[羅馬神話中](../Page/羅馬神話.md "wikilink")，教人類養蜂的是[阿里斯泰俄斯](../Page/阿里斯泰俄斯.md "wikilink")（Aristaeus）。
  - [西班牙的](../Page/西班牙.md "wikilink")[黎凡特崖發現的距今約](../Page/黎凡特崖.md "wikilink")1萬年前的壁畫上就有描繪著女性從蜂巢中採取蜂蜜的圖畫。
  - [美索不達米亞文明的象形文字裡裡也有與蜂蜜的記載](../Page/美索不達米亞文明.md "wikilink")、在古代[埃及的壁畫中也有關於養蜂的描繪](../Page/埃及.md "wikilink")。
  - [古希臘的](../Page/古希臘.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[亞里士多德在他的著作](../Page/亞里士多德.md "wikilink")《動物誌》中也有著關於養蜂的記述，其中寫道，蜜蜂收集的蜂蜜，不是花的分泌物，而是花賜予的甘露。
  - [舊約聖經中](../Page/舊約聖經.md "wikilink")[以色列人的](../Page/以色列.md "wikilink")[应许之地](../Page/应许之地.md "wikilink")，[迦南](../Page/迦南.md "wikilink")（Canaan）被描寫成一個「流奶与蜜的地方」，蜂蜜被視作豐饒的象徵。
  - [中世紀的歐洲](../Page/中世紀.md "wikilink")，[修道院盛行著養蜂](../Page/修道院.md "wikilink")，因為[蜂蠟被作為照明用的](../Page/蜂蠟.md "wikilink")[蠟燭的原料](../Page/蠟燭.md "wikilink")。

## 現代養蜂

到了19世紀，取得蜂蜜的方法已經簡單到只要從蜂巢中取出巢板。1853年，[美國人L](../Page/美國.md "wikilink").L.Langstroth的著作「巢與蜂蜜」（The
Hive and the Honey
Bee）中闡述了人工飼養蜜蜂及取得蜂蜜的技術，發明了具有活動巢板的巢箱及用於分離蜂蜜的離心機，確立了現代養蜂業。直到現在，養蜂的基本方法還是延續L.L.Langstroth的方法。

活動蜂箱是進行養蜂的工具，在立方體的箱子中，並排放入8～10枚稱為巢框的厚板。像自然形成的蜂巢一樣，蜂框也是以垂直平行方式排列的，蜂框作為形成巢脾的基礎，形狀為[縱橫比](../Page/縱橫比.md "wikilink")1比2的長方形的中空的木框，壁面的一邊以蜂蠟與石蠟製成厚紙狀的平台，上面刻以六角形，作為蜜蜂製作蜂巢的基礎。

在通常情況下，蜂蜜儲藏在巢板的上部，下部作為孵化蜂卵，培育幼蟲的區域，也用來儲藏花粉。蜜蜂在六角柱形巢洞儲存蜂蜜後，會用蜂蠟密封巢洞。

在通常情況下，在[秋季結束到](../Page/秋季.md "wikilink")[春季的這一段時間](../Page/春季.md "wikilink")，幾乎沒有什麼花存在的時期，蜜蜂會消耗所儲藏的蜂蜜。[春初幼蟲孵化的時候](../Page/春.md "wikilink")，是蜂蜜存量最少的時期。從這之後，蜂蜜的存量隨著花的開放開始回覆。[夏季隨著花的減少](../Page/夏季.md "wikilink")，蜂蜜的存量也開始減少。蜜蜂一次帶回的蜂蜜的量約為20mg。

養蜂分移動養蜂和固定養蜂兩種，固定養蜂是在一個地方，按花期一個接一個的採集開放的花的花蜜。移動養蜂則是從春天到夏天按特定的花的花期從南向北移動。

植物花期的頂峰並不會太長，蜜蜂為集中收集一個地方的花蜜，會只採集特定花的花蜜。

固定養蜂的年間日程安排與自然狀態相似，但是會根據養蜂的地方的花期不同而變化。例如（因每地氣候不同而異）：

  - 11月到3月間，蜂箱收回在室內保管，並施展以適合蜜蜂過冬的[溫度與](../Page/溫度.md "wikilink")[光照](../Page/光照.md "wikilink")。
  - 4月到5月間，蜂后開始產卵，3周後，工蜂開始工作。每個蜂箱需要大約2～3萬隻工蜂。
  - 5月到6月間，將蜂箱放於戶外，進行採蜜作業，巢板儲藏滿蜂蜜後，通過煙來控制蜜蜂的活動，取出巢板用[離心力分離機取得蜂蜜](../Page/離心機.md "wikilink")。
  - 6月到11月是休整期，蜜蜂採回的蜂蜜由自己使用。

## 蜜蜂

[Bees_Collecting_Pollen_2004-08-14.jpg](https://zh.wikipedia.org/wiki/File:Bees_Collecting_Pollen_2004-08-14.jpg "fig:Bees_Collecting_Pollen_2004-08-14.jpg")

蜜蜂是一種會飛行的[群居](../Page/群居動物.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")，屬於[蜂族](../Page/蜂族.md "wikilink")，採食[花粉和](../Page/花粉.md "wikilink")[花蜜並釀造](../Page/花蜜.md "wikilink")[蜂蜜](../Page/蜂蜜.md "wikilink")。

蜜蜂源自於亞洲與歐洲，由英國人與西班牙人帶到美洲。群體中有[蜂王](../Page/蜂王.md "wikilink")、[工蜂和](../Page/工蜂.md "wikilink")[雄蜂](../Page/雄蜂.md "wikilink")（Drone）三種類型的**蜜蜂**，群體中有一隻蜂后（有些例外情形有兩隻蜂后），1萬到15萬工蜂，500到1500隻雄蜂。蜜蜂為取得食物不停地工作，白天採蜜、晚上釀蜜，同時替果樹完成授粉任務，為農作物授粉的重要媒介。

[瓦蟎對蜜蜂造成嚴重的危害](../Page/瓦蟎.md "wikilink")。蜜蜂的主要天敌有胡蜂、蟾蜍、蛾、螟虫及其他食蟲動物。

蜂群内部分工明确，个体之间存在着丰富有趣的信息交流，社会行为丰富，是研究[社会行为学](../Page/社会行为学.md "wikilink")、[神经生物学](../Page/神经生物学.md "wikilink")、[行为遗传学](../Page/行为遗传学.md "wikilink")、[免疫学等良好的](../Page/免疫学.md "wikilink")[模式生物](../Page/模式生物.md "wikilink")。

### 蜂巢的構造

[Honey_comb.jpg](https://zh.wikipedia.org/wiki/File:Honey_comb.jpg "fig:Honey_comb.jpg")
蜜蜂的巢，通常情況下為按[水平面](../Page/水平面.md "wikilink")[垂直的方向延伸的](../Page/垂直.md "wikilink")[平面](../Page/平面.md "wikilink")[結構](../Page/結構.md "wikilink")。但是為了利用[空間](../Page/空間.md "wikilink")，有時也會傾斜建造。巢板的數量因為蜂蜜的種類不同而不同。養蜂時用的蜜蜂的巢會由很多枚巢板組成，通常會有10枚以上。

### 蜂蜜的採集

蜜蜂發現蜜源之後，會用跳「8字舞」或「圓圈舞」的方式來告訴同伴方向和距離，這樣工蜂可以被動員集中到蜜源的位置。帶回花蜜後的工作是將蜜交給負責儲藏的儲藏員，儲藏員會優先接待含糖分高的蜜蜂，含糖分低的工蜂則要等待被接待。

## 動物與蜂蜜

蜂蜜的營養價值很高，除了人以外，還有很多[動物也喜歡](../Page/動物.md "wikilink")。襲擊蜂巢，獲取蜂蜜的具有代表性的動物是[熊](../Page/熊.md "wikilink")。

並且，還存在有通過借其它動物的手以取得蜂蜜的[鳥類](../Page/鳥.md "wikilink")。主要是分佈於[撒哈拉以南的](../Page/撒哈拉.md "wikilink")[非洲大陸的](../Page/非洲.md "wikilink")[三趾鶉目的響蜜鴷](../Page/三趾鶉目.md "wikilink")，[響蜜鴷非常善於發現蜂巢](../Page/響蜜鴷.md "wikilink")，當響蜜鴷發現蜂巢時，便會靠近人或[蜜獾](../Page/蜜獾.md "wikilink")，發出叫聲通知他們並帶路。其它的動物破壞蜂巢後，響蜜鴷便可以享用它們吃剩後的蜂蜜和[蜂蠟](../Page/蜂蠟.md "wikilink")。

### 浓度

蜂蜜的浓度有三种表示方法，一是含水量，二是含糖量，三是[波美度](../Page/波美度.md "wikilink")。三者之间可以自由换算，即，知道其中一种，便可换算得出其他两种的数值。国际上通常用波美度来表示蜂蜜的浓度。

蜂蜜标准浓度按旧的标准以气温20摄氏度为准。一级波美度：42度以上，含水量19%；二级波美度：41度以上，含水量21%；三级波美度：40度，含水量23%，四级波美度：39度，含水量25%。按国家新标准，根据蜂蜜理化品质的不同，分为一级品和二级品两个等级。一级品蜂蜜水分含量不高于20%，二级品蜂蜜水分含量要求不高于24%。此两种等级中均要达到：果糖和葡萄糖含量不低于60%，蔗糖含量不高于5%。

蜂蜜在气温严寒的冬天容易结晶。气温在13-15摄氏度时，会形成白色固态的猪油或细粒状。蜂蜜90%都会结晶（槐花蜜、柠檬蜜除外），浓度越高，结晶越多越快（椴树蜜、油菜蜜除外）高浓度蜂蜜会整体结晶，浓缩蜜、调和蜜通常不容易结晶。气温回升后，结晶蜂蜜会逐渐熔解成半透明流体状。结晶蜂蜜可用60摄氏度左右的温水（温度过高会破坏其营养成分）隔水加热半小时即可熔解。

## 禁忌

1歲以內的[嬰兒](../Page/嬰兒.md "wikilink")、[糖尿病患者](../Page/糖尿病.md "wikilink")、[肝硬化患者](../Page/肝硬化.md "wikilink")，此3種人不宜食用蜂蜜。此外，蜂蜜容易遭到[肉毒桿菌所汙染](../Page/肉毒桿菌.md "wikilink")，使用蜂蜜調味過的肉類食品，不宜在未完整殺菌之前，使用真空包裝，以免肉毒桿菌滋生。
蜂蜜中的[葡萄糖容易加速細菌的滋生](../Page/葡萄糖.md "wikilink")，所以開封後的蜂蜜，需要冷藏保存。

## 標準

### [中華民國國家標準](../Page/中華民國國家標準.md "wikilink")

  - 中華民國國家標準 CNS 1305 N5024

<!-- end list -->

1.  蜂蜜（Honey）：蜜蜂採集植物之花蜜（Nectar）或蜜露（Honey），經蜜蜂收集、混合自身特殊物質進行轉化、儲存、脫水到熟成之天然甜味物質。
2.  龍眼蜂蜜 ：蜜蜂採集之花蜜以龍眼花蜜為主要蜜源，稱為龍眼蜂蜜或龍眼花蜜者。
3.  花蜜蜂蜜（Blossom honey or nectar honey）：主要來自植物的花蜜。
4.  蜜露蜂蜜（Honeydew honey）：主要來自吸吮植物汁液的昆蟲所排出的甜味物質。

在成分要求上也各有差別。蜂蜜規定蔗糖含量在5以下、葡萄糖及果糖在60以上；龍眼蜂蜜規定蔗糖含量在2以下、葡萄糖及果糖在60以上；而蜜露蜂蜜容許蔗糖含量在10以下；蜜露蜂蜜、花蜜混合蜜露蜂蜜要求葡萄糖及果糖在45以上。\[12\]台灣並不生產花蜜蜂蜜與蜜露蜂蜜，所以這種誤會較常發生在進口商品，如盛產蜜露蜂蜜的德國及其他歐洲、南北美及大洋洲區。此標準非強制性標準，相關市面蜂蜜的違規通常並非違反該標準規範而受處罰，多是因為宣稱為純蜂蜜，卻添加其他物質，比如蔗糖等。

### [中华人民共和国国家标准](../Page/中华人民共和国国家标准.md "wikilink")

  - 《食品安全国家标准：蜂蜜》（GB 14963—2011)

食品安全標準中僅定義蜂蜜為蜜蜂采集植物的花蜜、分泌物或蜜露，与自身分泌物混合后，经充分酿造而成的天然甜物质。另有要求不得來自有毒蜜源植物。成份上規定葡萄糖及果糖在60以上；部分指定蜜源蜂蜜容許蔗糖含量在10以下，其他容許蔗糖含量在5以下。\[13\]
此標準為強制性標準。

### [中华人民共和国行业标准](../Page/中华人民共和国行业标准.md "wikilink")

  - 中华人民共和国供销合作行业标准 GH/T 18796—2012

針對各蜂蜜分類有較細部規範的(強制性)GB 18796-2005已於2012年廢止\[14\]。(非強制性)行業標準GH/T
18796—2012內文說明演變自GB 18796-2005\[15\]。

  - 蜂蜜：蜜蜂采集植物的花蜜、分泌物或蜜露，与自身分泌物结合后，经充分酿造而成的天然甜物质。
  - 单一花种蜂蜜：蜜蜂要采集一种蜜源植物的花蜜或分泌物酿造的蜂蜜。
  - 多花种(混合)蜂蜜：蜜蜂采集两种或两种以上蜜源植物的花蜜或分泌物酿造的蜂蜜，以及两种或两种以上单一植物蜂蜜的混合物。
  - 蜜露蜂蜜：蜜蜂采集蜜露，与自身分泌物结合后，经充分酿造而成的天然甜物质。

### [中华人民共和国药典](../Page/中华人民共和国药典.md "wikilink")

  - 《中华人民共和国药典》2010年版

本品为蜜蜂科昆虫中华蜜蜂Apis cerana Fabricius或意大利蜂Apis mellifera
Linnaeus所酿的蜜。春至秋季采收，滤过。 【性状】
本品为半透明、带光泽、浓稠的液体，白色至淡黄色或橘黄色至黄褐色，放久或遇冷渐有白色颗粒状结晶析出。气芳香，味极甜。
本品含[还原糖不得少于](../Page/还原糖.md "wikilink")64.0%\[16\]

## 其他

  -
  -
## 參見

  - [巢蜜](../Page/巢蜜.md "wikilink")
  - [蜂鳥](../Page/蜂鳥.md "wikilink")
  - [蜂蜜酒](../Page/蜂蜜酒.md "wikilink")
  - [糖漿](../Page/糖漿.md "wikilink")
  - [酸模樹](../Page/酸模樹.md "wikilink")
  - [素食主義](../Page/素食主義.md "wikilink")
  - [果糖](../Page/果糖.md "wikilink")
  - [花蜜](../Page/花蜜.md "wikilink")
  - [蜂巢](../Page/蜂巢.md "wikilink")
  - [梫木毒素](../Page/梫木毒素.md "wikilink")

## 參考文獻

  - [概述和蜜蜂的摘要（新闻，经济，贸易，问题等）](http://www.lamieldeabejas.com).

[Category:甜味劑](../Category/甜味劑.md "wikilink")
[Category:蜜蜂](../Category/蜜蜂.md "wikilink")
[蜂蜜](../Category/蜂蜜.md "wikilink")

1.  2005年版《中国药典》标准蜂蜜（A0014）
2.  [肉毒中毒與蜜糖
    食物安全之風險評估](http://www.cfs.gov.hk/tc_chi/programme/programme_rafs/programme_rafs_fm_02_10.html)食物安全中心
    風險評估組 二零一零年七月
3.   "Out of Sight"
4.  <http://tw.news.yahoo.com/article/url/d/a/071205/2/pdmo.html>
5.  [CODEX STAN 12-1981 Standard for
    Honey](http://www.codexalimentarius.org/download/standards/310/cxs_012e.pdf)Codex
    Alimentarius
6.  [台灣的蜜源植物](http://mdares.coa.gov.tw/files/web_articles_files/mdares/651/115.pdf)
    苗栗區農改場
7.  [Ukrainian honey – world congress
    triumpher](http://www.ukrinform.ua/eng/order/?id=170297)、[Ukrinform](../Page/Ukrinform.md "wikilink")（September
    23, 2009）
8.  [where food comes from | where honey comes from
    (2012-09-11)（2012-10-07](http://wherefoodcomesfrom.com/article/6380/Where-Honey-Comes-From)

9.
10. [Miel de Corse mele di Corsica, la gamme variétale AOC
    AOP](http://www.miel-corse.eu/Pages/gamme.htm). Miel-corse.eu.
    Retrieved on 2011-02-06.
11. [Intellectual Property
    Office](https://web.archive.org/web/20090326210635/http://www.zis.gov.rs/en/oznake_geografskog_porekla/ogp_spisak.html).
    Zis.gov.rs. Retrieved on 2011-02-06.
12. [一般檢索
    預覽](http://www.cnsonline.com.tw/?node=search&locale=zh_TW)國家標準(CNS)網路服務系統
13. [食品安全国家标准GB 14963—2011](http://www.nhfpc.gov.cn/zwgkzt/psp/201106/51948/files/bf8837ddf29c4dd794d824b26260d206.pdf)
    [中华人民共和国国家卫生和计划生育委员会](../Page/中华人民共和国国家卫生和计划生育委员会.md "wikilink")
14. [GB 18796-2005
    已廢止](http://www.sdsn.org.cn/1_4_sdsn_ncpbz/admin/bztx/UploadFiles/GB%2018796-2005.pdf)
15. [中华人民共和国供销合作行业标准GH/T 18796—2012](http://www.chinaapiculture.org/downloadfile.asp?pkid=14)
16. [国家药品标准信息公开平台](http://fp.chp.org.cn:7001/infopub/)
    [国家药典委员会](../Page/国家药典委员会.md "wikilink")