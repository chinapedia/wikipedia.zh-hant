**胸大肌**為將[手臂拉向](../Page/手臂.md "wikilink")[胸部的](../Page/胸部.md "wikilink")[肌肉](../Page/肌肉.md "wikilink")，兩塊胸大肌位於胸的兩側。胸大肌通常稱為**胸肌**或**胸脯**，成[扇型](../Page/扇型.md "wikilink")，且分裂為大小不等兩部分。在其窄端，兩部分都附著於肱骨之上，在寬的一端，較小的部分附於鎖骨上；較大的部分則附於胸骨和肋骨上面的軟肋骨上，胸肌可將手部向前拉和向內拉近身體，還可使[手臂轉動](../Page/手臂.md "wikilink")。

## 负重训练

[Joseph_Gatt_at_AVN_Adult_Entertainment_Expo_2009.jpg](https://zh.wikipedia.org/wiki/File:Joseph_Gatt_at_AVN_Adult_Entertainment_Expo_2009.jpg "fig:Joseph_Gatt_at_AVN_Adult_Entertainment_Expo_2009.jpg")

  - [卧推](../Page/卧推.md "wikilink")
  - [俯卧撑](../Page/俯卧撑.md "wikilink")

## 性别

男性的胸大肌在[乳房内部](../Page/乳房.md "wikilink")，由乳房悬韧带支撑。女性的胸大肌则在乳腺叶后面。

## 資料來源

〈人體學習百科〉 貓頭鷹出版社 ISBN 957-9684-11-1

## 外部連結

  -
  -
  - [UCC](https://web.archive.org/web/20051123053934/http://faculty.ucc.edu/biology-potter/Musculature/sld013.htm)

  - [www.polands-syndrome.com](https://web.archive.org/web/20110208060348/http://polands-syndrome.com/)

  - [MRI Imaging sequence demonstrating a pectoralis major muscle
    tear](http://www.melbourneradiology.com.au/mri-gallery/mri-pectoralis-major-muscle-tear.html)

[Category:肩部展肌](../Category/肩部展肌.md "wikilink")
[Category:肩部收肌](../Category/肩部收肌.md "wikilink")
[Category:肩部伸肌](../Category/肩部伸肌.md "wikilink")
[Category:肩部屈肌](../Category/肩部屈肌.md "wikilink")
[Category:肩部內旋肌](../Category/肩部內旋肌.md "wikilink")
[Category:上肢肌肉](../Category/上肢肌肉.md "wikilink")