[Al_Hilal_Club_Logo.gif](https://zh.wikipedia.org/wiki/File:Al_Hilal_Club_Logo.gif "fig:Al_Hilal_Club_Logo.gif")
**沙特希拉爾足球俱樂部**（[英文](../Page/英文.md "wikilink")：**Al-Hilal Saudi Football
Club**、[阿拉伯文](../Page/阿拉伯文.md "wikilink")：**الهلال
السعودي**）是一間位於[沙烏地阿拉伯首都](../Page/沙烏地阿拉伯.md "wikilink")[利雅德的職業](../Page/利雅德.md "wikilink")[足球會](../Page/足球會.md "wikilink")。球會於1957年成立，球會也是[沙烏地阿拉伯最受歡迎的球會之一](../Page/沙烏地阿拉伯.md "wikilink")。

## 榮譽

  -
  -
  -
  -
  -
  -
## 亞足聯賽事上的表現

  - **[亞洲聯賽冠軍盃](../Page/亞洲聯賽冠軍盃.md "wikilink")**: 10次出席

<!-- end list -->

  -

      -
        [2002/03年](../Page/2002–03年亚足联冠军联赛.md "wikilink"): 分組賽出局
        [2004年](../Page/2004年亚足联冠军联赛.md "wikilink"): 分組賽出局
        [2006年](../Page/2006年亚足联冠军联赛.md "wikilink"): 分組賽出局
        [2007年](../Page/2007年亚足联冠军联赛.md "wikilink"): 四分之一决赛
        [2009年](../Page/2009年亚足联冠军联赛.md "wikilink"): 八分之一决赛
        [2010年](../Page/2010年亚足联冠军联赛.md "wikilink")：半决赛
        [2011年](../Page/2011年亚足联冠军联赛.md "wikilink")：八分之一决赛
        [2012年](../Page/2012年亚足联冠军联赛.md "wikilink")：四分之一决赛
        [2013年](../Page/2013年亞足聯冠軍聯賽.md "wikilink")：八分之一决赛
        [2014年](../Page/2014年亞足聯冠軍聯賽.md "wikilink")：亚军

<!-- end list -->

  - **[亞洲冠軍球會盃](../Page/亞洲聯賽冠軍盃.md "wikilink")**: 6次出席

<!-- end list -->

  -

      -
        1998年: 亞軍
        1991年: **冠軍**
        1998年: 四強
        1999年: 八強
        2000年: **冠軍**
        2001年: 八強

<!-- end list -->

  - **[亞洲盃賽冠軍盃](../Page/亞洲盃賽冠軍盃.md "wikilink")**: 3次出席

<!-- end list -->

  -

      -
        1990年/91年: 四強
        1996年/97年: **冠軍**
        2001年/02年: **冠軍**

<!-- end list -->

  - **[亞洲超級盃](../Page/亞洲超級盃.md "wikilink")**: 2次出席

<!-- end list -->

  -

      -
        1997年: **冠軍**
        2002年: **冠軍**

## 參考來源

## 外部連結

  - [Official website](http://www.alhilal.com/)
  - [Official website](http://www.alzaeem.com/)

[Category:沙烏地阿拉伯足球俱樂部](../Category/沙烏地阿拉伯足球俱樂部.md "wikilink")
[Category:利雅德](../Category/利雅德.md "wikilink")
[Category:沙特阿拉伯足球俱樂部](../Category/沙特阿拉伯足球俱樂部.md "wikilink")