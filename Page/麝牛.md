**麝牛**，學名：*Ovibos
moschatus*，分布在[加拿大](../Page/加拿大.md "wikilink")[北極地區和](../Page/北極.md "wikilink")[格陵蘭島](../Page/格陵蘭島.md "wikilink")
，小種群引進[瑞典](../Page/瑞典.md "wikilink")，[西伯利亞](../Page/西伯利亞.md "wikilink")，[挪威和](../Page/挪威.md "wikilink")[阿拉斯加](../Page/阿拉斯加.md "wikilink")。因為雄性麝牛在交配季節時會散發強烈的氣味而得名。雌雄都有寬大的角，向下斜長的毛皮，可以讓雪順利滑落，是[羚牛的近亲](../Page/羚牛.md "wikilink")，尽管像[牛](../Page/牛.md "wikilink")，但是许多方面都更像[羊](../Page/羊.md "wikilink")，可以说是一种超大型的野羊。主要生活在加拿大和格陵兰岛北部北极地区。阿拉斯加的原生種麝牛已於十九世紀滅絕，目前見到的麝牛為後來引進的結果。

## 體型

體長1.9至2.3公尺，體重200至410公斤。

## 參考

[Category:麝牛屬](../Category/麝牛屬.md "wikilink")
[Category:北極哺乳動物](../Category/北極哺乳動物.md "wikilink")
[Category:加拿大哺乳動物‎](../Category/加拿大哺乳動物‎.md "wikilink")
[Category:格陵蘭動物](../Category/格陵蘭動物.md "wikilink")
[Category:瑞典動物](../Category/瑞典動物.md "wikilink")
[Category:西伯利亞動物](../Category/西伯利亞動物.md "wikilink")
[Category:挪威動物](../Category/挪威動物.md "wikilink")
[Category:阿拉斯加動物](../Category/阿拉斯加動物.md "wikilink")