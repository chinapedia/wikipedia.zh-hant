**FileZilla**是一種快速、可信賴的[FTP用戶端以及](../Page/FTP.md "wikilink")[伺服器端](../Page/伺服器.md "wikilink")[開放原始碼程式](../Page/開放原始碼.md "wikilink")，具有多種特色、直覺的介面。

FileZilla在2003年11月獲選為[SourceForge.net當月最佳推薦專案](../Page/SourceForge.net.md "wikilink")。\[1\]

## FileZilla功能

  - 可以[断点续传进行上传](../Page/断点续传.md "wikilink")、下载（需要服务器支持）
  - 自定义命令
  - 可进行站点管理
  - 防發呆功能（有的FTP服务器会将發呆过久的用户赶出，这样發呆的用户就得重复登录）
  - 超时侦测
  - 支持[防火墙](../Page/防火墙.md "wikilink")
  - 支持[SOCKS](../Page/SOCKS.md "wikilink")4/5、[HTTP](../Page/HTTP.md "wikilink")1.1代理
  - 可进行[SSL加密连接](../Page/SSL.md "wikilink")
  - 支持[SFTP](../Page/SFTP.md "wikilink")（Secure FTP）
  - 可以排队进行上传、下载
  - 支持拖放
  - 多国语言支持，包括[简体](../Page/简体中文.md "wikilink")、[繁体中文](../Page/繁体中文.md "wikilink")（[Linux平台需額外安裝](../Page/Linux.md "wikilink")「filezilla-locales」套件）
  - 可通过[Kerberos进行](../Page/Kerberos.md "wikilink")[GSS验证与加密](../Page/GSS.md "wikilink")

## 参见

  - [FileZilla 伺服器](../Page/FileZilla_伺服器.md "wikilink")
  - [FTP客户端比较](../Page/FTP客户端比较.md "wikilink")

## 參考資料

<div style="font-size: 90%">

<references />

</div>

## 外部連結

  - [官方網站](https://filezilla-project.org/)
  - [專案站台](http://sourceforge.net/projects/filezilla/) (SourceForge.net)
  - [FileZilla Wiki](http://filezilla-project.org/wiki)
  - [可攜式
    FileZilla](http://portableapps.com/apps/internet/ftp/portable_filezilla)
  - [FileZilla使用教學](http://inote.tw/2006/10/filezilla.html)

### 參與中文翻譯

  - [FileZilla開發及翻譯討論區](http://forum.filezilla-project.org/viewforum.php?f=3)
  - [FileZilla翻譯計劃首頁](http://filezilla-project.org/translations.php)

[Category:FTP客户端](../Category/FTP客户端.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:开源软件](../Category/开源软件.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")

1.  [Project of the Month](http://sourceforge.net/potm/potm-2003-11.php)