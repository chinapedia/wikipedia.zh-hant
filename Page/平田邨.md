[Ping_Tin_Estate.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate.jpg "fig:Ping_Tin_Estate.jpg")
[Clipboard01.jpg](https://zh.wikipedia.org/wiki/File:Clipboard01.jpg "fig:Clipboard01.jpg")
[Ping_Tin_Shopping_Centre.JPG](https://zh.wikipedia.org/wiki/File:Ping_Tin_Shopping_Centre.JPG "fig:Ping_Tin_Shopping_Centre.JPG")
[Hong_Nga_Court.jpg](https://zh.wikipedia.org/wiki/File:Hong_Nga_Court.jpg "fig:Hong_Nga_Court.jpg")
[Ping_Tin_Estate_Ancillary_Facilities_Block_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Ancillary_Facilities_Block_\(Hong_Kong\).jpg "fig:Ping_Tin_Estate_Ancillary_Facilities_Block_(Hong_Kong).jpg")
[Ping_Tin_Estate_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Covered_Walkway.jpg "fig:Ping_Tin_Estate_Covered_Walkway.jpg")
[Ping_Tin_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Basketball_Court.jpg "fig:Ping_Tin_Estate_Basketball_Court.jpg")
[Ping_Tin_Estate_Children_Play_Area.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Children_Play_Area.jpg "fig:Ping_Tin_Estate_Children_Play_Area.jpg")
[Ping_Tin_Estate_Fitness_Area_and_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Fitness_Area_and_Pebble_Walking_Trail.jpg "fig:Ping_Tin_Estate_Fitness_Area_and_Pebble_Walking_Trail.jpg")
[Ping_Tin_Estate_Pavilion_and_Chess_Area.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Pavilion_and_Chess_Area.jpg "fig:Ping_Tin_Estate_Pavilion_and_Chess_Area.jpg")
[Ping_Tin_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Ping_Tin_Estate_Car_Park.jpg "fig:Ping_Tin_Estate_Car_Park.jpg")
**平田邨**（）位於[香港](../Page/香港.md "wikilink")[觀塘](../Page/觀塘區.md "wikilink")[藍田安田街](../Page/藍田_\(香港\).md "wikilink")，是[香港房屋委員會轄下的](../Page/香港房屋委員會.md "wikilink")[公共屋邨之一](../Page/公共屋邨.md "wikilink")。

## 簡介

平田邨前身為[藍田邨第](../Page/藍田邨.md "wikilink")16至24座。第16至20座在1972年至1973年落成；第21至24座在1973年至1974年落成。

第17座是1985年被揭發的「[26座問題公屋](../Page/26座問題公屋醜聞.md "wikilink")」之一，混凝土強度只有6.20[兆帕斯卡](../Page/帕斯卡.md "wikilink")，遠遠低於興建樓宇最低要求的20至30兆帕斯卡，而第22座的混凝土強度亦只有標準的34%\[1\]。基於這個緣故，藍田邨第17座在1988年4月便被迫展開清拆工程，而第20至24座和第16、18及19座也於1992年8月和1994年3月展開清拆工程。清拆時樓齡只有16年至22年。

第20至24座重建為平信樓、平仁樓、平旺樓、平誠樓、平田商場在1997年落成；第16、18及19座重建為平真樓、平善樓、平美樓、服務設施大樓、與[藍田循道衛理小學和](../Page/藍田循道衛理小學.md "wikilink")[聖公會李兆強小學在](../Page/聖公會李兆強小學.md "wikilink")1998年落成。在平田邨落成後，安田街改為順時針方向單程行車，平田街改為單程東行行車。

平田邨除平真樓外，屬於平田選區。平真樓在[2003年香港區議會選舉被劃至藍田選區](../Page/2003年香港區議會選舉.md "wikilink")\[2\]，而在[2007年香港區議會選舉本來曾被考慮劃入廣德選區內](../Page/2007年香港區議會選舉.md "wikilink")\[3\]，但最終仍維持在藍田選區\[4\]。直至[2015年香港區議會選舉](../Page/2015年香港區議會選舉.md "wikilink")，整條平田邨及鄰近[安田邨統一劃入平田選區](../Page/安田邨.md "wikilink")。

## 樓宇

| 樓宇名稱（座號） | 房屋類型  | 落成年份  |
| -------- | ----- | ----- |
| 平信樓（第1座） | 和諧一型  | 1997年 |
| 平仁樓（第2座） |       |       |
| 平旺樓（第3座） |       |       |
| 平誠樓（第4座） |       |       |
| 平真樓（第5座） | 1998年 |       |
| 平善樓（第6座） |       |       |
| 平美樓（第7座） |       |       |
| 服務設施大樓   | 長者住屋  |       |

## 教育及福利設施

### 幼稚園

  - [救世軍平田幼稚園](http://www.salvationarmy.org.hk/esd/ptkg/)（1997年創辦）（位於平誠樓地下）
  - [圓玄幼稚園（平田邨）](http://www.yuenyuenkg.edu.hk/pingtian/)（1998年創辦）（位於平善樓地下）

### 綜合青少年服務中心

  - [香港中華基督教青年會藍田會所賽馬會綜合青少年服務中心](http://ltc.ymca.org.hk/zh-hant/)（位於平信樓地下）

### 長者鄰舍中心

  - [循道衛理觀塘社會服務處藍田樂齡鄰舍中心](https://sub.ktmss.org.hk/elderly/ltnec/info)（位於平仁樓地下5號）

### 安老院

  - [耆康會馮堯敬夫人護理安老院](http://www.sage.org.hk/Service/UnitServe/ElderlyHomes/fykcaCenter.aspx)（1997年創辦）（位於平田邨服務設施大樓2樓）

## 平田商場

平田商場在1997年年底開幕，商場設有行人通道通往[康雅苑和](../Page/康雅苑.md "wikilink")[碧雲道](../Page/碧雲道.md "wikilink")。

商場開幕後不久，即發生一宗事故，一名男童在商場1樓倚着玻璃圍欄時，懷疑玻璃安裝不穩或鬆脫，男童和玻璃板一同從商場一樓跌至地下。\[5\][民建聯](../Page/民建聯.md "wikilink")[臨時立法會議員](../Page/臨時立法會.md "wikilink")[陳鑑林在意外後的實地視察發現](../Page/陳鑑林.md "wikilink")，該商場玻璃圍欄安裝方法，與香港《建築物規例》規定的不同。[民協](../Page/民協.md "wikilink")[廖成利議員亦稱倘若地盤視察組沒有進行此一檢查程序](../Page/廖成利.md "wikilink")，則有人須對意外事件負責。

地下

  -   - [港鐵特惠站](../Page/港鐵特惠站.md "wikilink")
      - [7-11便利店](../Page/7-11便利店.md "wikilink")
      - [跌打](../Page/跌打.md "wikilink")
      - [味之戀人](../Page/味之戀人.md "wikilink")
      - [藥房](../Page/藥房.md "wikilink")
      - [麵包店](../Page/麵包店.md "wikilink")
      - [裝修](../Page/裝修.md "wikilink")

一樓

  -   - [補習社](../Page/補習社.md "wikilink")
      - [零食](../Page/零食.md "wikilink")
      - [洗衣店](../Page/洗衣店.md "wikilink")
      - [惠康](../Page/惠康.md "wikilink")

二樓

  -   - [咸田燒腊茶餐廳](https://www.openrice.com/zh/hongkong/r-%E5%92%B8%E7%94%B0%E7%87%92%E8%85%8A%E8%8C%B6%E9%A4%90%E5%BB%B3-%E8%97%8D%E7%94%B0-%E6%B8%AF%E5%BC%8F-%E7%87%92%E8%87%98-r1092)
      - [百份百餐廳](../Page/百份百餐廳.md "wikilink")
      - [日本城](../Page/日本城.md "wikilink")
      - 凌玉駒牙科醫生
      - [韋楚強醫生](http://www.hongkongdoctorlist.com/dr-wai-chor-keung/prod_1189.html)
      - [裝修](../Page/裝修.md "wikilink")
      - [地產](../Page/地產.md "wikilink")
      - [電器店](../Page/電器店.md "wikilink")
      - [美容院](../Page/美容院.md "wikilink")
      - [浴足](../Page/浴足.md "wikilink")
      - [玩具](../Page/玩具.md "wikilink")
      - [報攤](../Page/報攤.md "wikilink")

## 公共交通路線

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/藍田站.md" title="wikilink">藍田站</a> (步行約10分鐘)</li>
</ul>
<dl>
<dt><a href="../Page/平田巴士總站.md" title="wikilink">平田巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>藍田至<a href="../Page/觀塘.md" title="wikilink">觀塘線</a>[6]</li>
<li><a href="../Page/旺角.md" title="wikilink">旺角至藍田線</a> (24小時服務)[7]</li>
<li><a href="../Page/佐敦道.md" title="wikilink">佐敦道</a>/<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣至藍田線</a>[8]</li>
<li><a href="../Page/青山道.md" title="wikilink">青山道至藍田</a>/<a href="../Page/油塘.md" title="wikilink">油塘線</a>[9]</li>
<li><a href="../Page/灣仔.md" title="wikilink">灣仔至藍田</a>/油塘線 (通宵服務)[10]</li>
</ul></td>
</tr>
</tbody>
</table>

## 著名居民

  - [周志文](../Page/周志文.md "wikilink")，香港男藝人，前[無綫電視藝員](../Page/無綫電視.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [房屋委員會平田邨資料](http://www.housingauthority.gov.hk/b5/interactivemap/estate/0,,3-347-13_5005,00.html)
  - [房屋委員會：平田商場](http://www.housingauthority.gov.hk/b5/interactivemap/centre/0,,3-347-13_5349,00.html)
  - [平田商場](http://www.thelinkreit.com/tc/prop/prop_detail2.asp?pid=31)

[en:Public housing estates in Lam Tin\#Ping Tin
Estate](../Page/en:Public_housing_estates_in_Lam_Tin#Ping_Tin_Estate.md "wikilink")

[Category:藍田 (香港)](../Category/藍田_\(香港\).md "wikilink")
[Category:以街道命名的公營房屋](../Category/以街道命名的公營房屋.md "wikilink")

1.  黃華輝，《公屋醜聞──一名記者的追查實錄》，香港：進一步多媒體，1999年7月，ISBN 962-8326-22-8
2.  [2003年觀塘區議會選區分界](http://www.eac.gov.hk/pdf/distco/maps/dc2003j.pdf)
3.  [2007年觀塘區議會暫定選區分界](http://www.eac.gov.hk/pdf/distco/2007dc/ch/kwuntong_c.pdf)

4.  [2007年觀塘區議會選區分界](http://www.eac.gov.hk/pdf/distco/maps/dc2007j.pdf)
5.  [臨時立法會房屋事務委員會](http://www.legco.gov.hk/yr97-98/chinese/panels/hg/minutes/hg241297.htm)，1997年12月24日
6.  [觀塘市中心　—　藍田](http://www.16seats.net/chi/rmb/r_k60.html)
7.  [旺角花園街　—　藍田](http://www.16seats.net/chi/rmb/r_k16.html)
8.  [佐敦道寧波街　—　藍田](http://www.16seats.net/chi/rmb/r_k55.html)
9.  [青山道香港紗廠　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)
10. [灣仔　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_kh63.html)