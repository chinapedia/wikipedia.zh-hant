**鸡冠区**是中国[黑龙江](../Page/黑龙江.md "wikilink")[鸡西市所辖的一个区](../Page/鸡西市.md "wikilink")，位于鸡西东部，它的名字来自于[鸡冠山](../Page/鸡冠山.md "wikilink")。

## 行政区划

下辖7个街道、2个乡。\[1\] 。

## 参考资料

## 外部链接

  - [鸡西市鸡冠区人民政府网](http://www.jgq.gov.cn/)

[\*](../Category/鸡冠区.md "wikilink")
[Category:黑龙江市辖区](../Category/黑龙江市辖区.md "wikilink")

1.