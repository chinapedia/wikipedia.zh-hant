**貝勒縣**（**Baylor County,
Texas**）位[美國](../Page/美國.md "wikilink")[德克薩斯州中北部的一個縣](../Page/德克薩斯州.md "wikilink")。面積2,334平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口4,093人。縣治西摩（Seymour）。

成立於1858年2月1日，縣政府成立於1879年4月12日。縣名紀念[美墨戰爭時期軍人亨利](../Page/美墨戰爭.md "wikilink")·W·貝勒（Henry
Weidner Baylor）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.