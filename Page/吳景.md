**吳景**（），[東吳](../Page/東吳.md "wikilink")[孫堅的妻弟](../Page/孙坚_\(东吴\).md "wikilink")，其姊[吳夫人是](../Page/孙破虏吴夫人.md "wikilink")[孫策](../Page/孫策.md "wikilink")、[孫權的母親](../Page/孫權.md "wikilink")。

## 生平

吳景追隨[孫堅征伐有功](../Page/孫堅.md "wikilink")，受騎都尉。討伐丹楊太守[周昕](../Page/周昕.md "wikilink")，佔領了丹楊後，[袁術上表吳景爲](../Page/袁術.md "wikilink")[丹楊太守](../Page/丹楊.md "wikilink")。

[初平三年](../Page/初平.md "wikilink")(192年)，孫策因父孫堅之死而依附吳景並於曲阿招募了數百兵力，卻遭到涇縣賊帥[祖郎襲擊](../Page/祖郎.md "wikilink")，險些喪命。後孫策依吳景之建言又與[孫河](../Page/孫河.md "wikilink")、[呂範合眾攻擊祖郎](../Page/呂範.md "wikilink")，使其敗走。

之後吳景又被[劉繇逼迫而北投](../Page/劉繇.md "wikilink")[袁術](../Page/袁術.md "wikilink")。袁術用他爲督軍中郎將，與[孫賁於橫江討伐](../Page/孫賁.md "wikilink")[樊能](../Page/樊能.md "wikilink")、[于糜](../Page/于糜.md "wikilink")，又在秣陵擊敗[笮融](../Page/笮融.md "wikilink")、[薛禮](../Page/薛禮.md "wikilink")。當孫策在[牛渚受傷時](../Page/牛渚.md "wikilink")，遇降兵造反，幸被吳景平定及擒下降兵。吳景跟隨孫策討伐劉繇，劉繇出走[豫章](../Page/豫章.md "wikilink")，他奉孫策命到[袁術處報訊](../Page/袁術.md "wikilink")。袁術與[劉備爭奪](../Page/劉備.md "wikilink")[徐州時](../Page/徐州.md "wikilink")，任命吳景為[廣陵太守](../Page/廣陵.md "wikilink")。

袁術僭越稱帝時，孫策曾給袁術書信，勸喻不可，袁術不聽所勸，兩人於是不再往來。孫策接著派人通知吳景，吳景於是東歸，孫策仍任他為丹楊太守。漢廷遣使到來，授銜揚武將軍，仍舊領丹楊。

## 姊姊

  - [吳夫人](../Page/孙破虏吴夫人.md "wikilink")，孫堅元配夫人，生有四子，孫策孫權孫翊孫匡，以及一女，但并不确定此女是孙坚的哪个女儿。

## 姊夫

  - [孫堅](../Page/孙坚_\(东吴\).md "wikilink")，吳夫人夫君，破黃巾，伐董卓，解區星之亂，長沙太守孫堅。

## 外甥

  - [孫策](../Page/孫策.md "wikilink")，長男，東漢討逆將軍，吳國奠基者，拜吳侯，孫權稱帝後追諡其長兄為長沙桓王。
  - [孫權](../Page/孫權.md "wikilink")，次男，吳大帝，東吳開國君主。
  - [孫翊](../Page/孫翊.md "wikilink")，三男，個性驍勇果烈，有孫策之風，偏將軍領單楊太守。
  - [孫匡](../Page/孫匡.md "wikilink")，四男，長兄孫策讓其烏程侯爵位給幼弟孫匡繼承，早死。

## 子嗣

吳景卒於建安八年（203年），有子[吳奮封新亭候](../Page/吳奮.md "wikilink")。吴奋子[吴安袭父爵](../Page/吴安.md "wikilink")，坐鲁王[孙霸党被杀](../Page/孙霸.md "wikilink")。吴景另一个儿子即吴奋的弟弟[吳祺袭爵](../Page/吳祺.md "wikilink")，封都亭侯，死后由儿子[吴纂袭爵](../Page/吴纂.md "wikilink")。吴纂娶[滕胤女](../Page/滕胤.md "wikilink")。

## 參考文獻

  - 《[三國志](../Page/三國志.md "wikilink")‧吳書‧妃嬪傳第五》
  - 《中國名人志 第二卷》/三國-吳

[W](../Category/东吴政治人物.md "wikilink") [J](../Category/吳姓.md "wikilink")