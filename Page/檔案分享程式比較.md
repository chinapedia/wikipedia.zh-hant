以下為各個檔案分享程式之比較。詳情請參閱各程式之條目。

## 一般資料

檔案分享程式的一般資料：創作者/公司、軟體授權/價格等等。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>創作者</p></th>
<th><p>首個公開發表日期</p></th>
<th><p>最新穩定版本</p></th>
<th><p>價格（<a href="../Page/美元.md" title="wikilink">美元</a>）</p></th>
<th><p><a href="../Page/:en:Software_license.md" title="wikilink">軟體授權</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/aMule.md" title="wikilink">aMule</a></p></td>
<td><p><a href="../Page/aMule_Project.md" title="wikilink">aMule Project</a></p></td>
<td><p>2003-08-25</p></td>
<td><p>2.3.1</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Ares_Galaxy.md" title="wikilink">Ares Galaxy</a></p></td>
<td><p><a href="../Page/Ares_Galaxy_Project.md" title="wikilink">Ares Galaxy Project</a></p></td>
<td><p>2002年</p></td>
<td><p>2.0.8</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Azureus.md" title="wikilink">Azureus</a></p></td>
<td><p><a href="../Page/Azureus_Project.md" title="wikilink">Azureus Project</a></p></td>
<td><p>2003年6月</p></td>
<td><p>4.6.0.2</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BearShare.md" title="wikilink">BearShare</a></p></td>
<td><p><a href="../Page/Free_Peers，Inc．.md" title="wikilink">Free Peers，Inc．</a></p></td>
<td><p>-</p></td>
<td><p>5.2.0</p></td>
<td><p>$3.29/月共享，<br />
廣告，免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BitComet.md" title="wikilink">BitComet</a></p></td>
<td><p><a href="../Page/燦爛微笑.md" title="wikilink">燦爛微笑</a></p></td>
<td><p>-</p></td>
<td><p>1.26</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/專屬.md" title="wikilink">專屬</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BitLord.md" title="wikilink">BitLord</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>1.1</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BitTornado.md" title="wikilink">BitTornado</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>0.3.14a</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BitTorrent.md" title="wikilink">BitTorrent</a></p></td>
<td><p><a href="../Page/Brian_Dessent.md" title="wikilink">Brian Dessent</a></p></td>
<td><p>2002年</p></td>
<td><p>4.2.2</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BT_Plus!.md" title="wikilink">BT Plus!</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>1.0.2</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DCPlusPlus.md" title="wikilink">DC++</a></p></td>
<td><p><a href="../Page/DCPlusPlus_Project.md" title="wikilink">DC++ Project</a></p></td>
<td><p>-</p></td>
<td><p>0.686</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/eDonkey_2000.md" title="wikilink">eDonkey 2000</a></p></td>
<td><p><a href="../Page/MetaMachine.md" title="wikilink">MetaMachine</a></p></td>
<td><p>-</p></td>
<td><p>1.4</p></td>
<td><p>$19.95共享，免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/eMule.md" title="wikilink">eMule</a></p></td>
<td><p><a href="../Page/eMule_Project.md" title="wikilink">eMule Project</a></p></td>
<td><p>2002-07-06</p></td>
<td><p>0.50a</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/eMule_Plus.md" title="wikilink">eMule Plus</a></p></td>
<td><p><a href="../Page/eMule_Plus_Project.md" title="wikilink">eMule Plus Project</a></p></td>
<td><p>2002-10-27</p></td>
<td><p>1.2b</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/eXeem.md" title="wikilink">eXeem</a></p></td>
<td><p><a href="../Page/Swarm_Systems_Inc．.md" title="wikilink">Swarm Systems Inc．</a></p></td>
<td><p>-</p></td>
<td><p>0.27</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FrostWire.md" title="wikilink">FrostWire</a></p></td>
<td><p><a href="../Page/The_FrostWire_Team.md" title="wikilink">The FrostWire Team</a></p></td>
<td><p>-</p></td>
<td><p>4.10.5 Beta</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/giFT-FastTrack.md" title="wikilink">giFT-FastTrack</a></p></td>
<td><p><a href="../Page/giFT-FastTrack_Project.md" title="wikilink">giFT-FastTrack Project</a></p></td>
<td><p>-</p></td>
<td><p>0.8.9</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/iMesh.md" title="wikilink">iMesh</a></p></td>
<td><p><a href="../Page/iMesh，Inc．.md" title="wikilink">iMesh，Inc．</a></p></td>
<td><p>-</p></td>
<td><p>6.0</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/KazaA.md" title="wikilink">KazaA</a></p></td>
<td><p><a href="../Page/Sharman_Networks.md" title="wikilink">Sharman Networks</a></p></td>
<td><p>-</p></td>
<td><p>3.0</p></td>
<td><p>廣告</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LimeWire.md" title="wikilink">LimeWire</a></p></td>
<td><p><a href="../Page/Lime_Wire_LLC..md" title="wikilink">Lime Wire LLC.</a></p></td>
<td><p>-</p></td>
<td><p>4.10.0</p></td>
<td><p>$18.88共享，免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/MLDonkey.md" title="wikilink">MLDonkey</a></p></td>
<td><p>Fabrice Le Fessant</p></td>
<td><p>2002年</p></td>
<td><p>3.0.0</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Maze.md" title="wikilink">Maze</a></p></td>
<td><p>北京大学网络实验室</p></td>
<td><p>2005年</p></td>
<td><p>6.0</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Morpheus.md" title="wikilink">Morpheus</a></p></td>
<td><p><a href="../Page/StreamCast_Networks，Inc．.md" title="wikilink">StreamCast Networks，Inc．</a></p></td>
<td><p>-</p></td>
<td><p>5.1.1</p></td>
<td><p>$19.95共享，廣告</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Napster.md" title="wikilink">Napster</a></p></td>
<td><p><a href="../Page/Napster，Inc．.md" title="wikilink">Napster，Inc．</a></p></td>
<td><p>-</p></td>
<td><p>3.5.2.5</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Share.md" title="wikilink">Share</a></p></td>
<td><p>匿名</p></td>
<td><p>-</p></td>
<td><p>EX2</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Shareaza.md" title="wikilink">Shareaza</a></p></td>
<td><p><a href="../Page/Shareaza_Project.md" title="wikilink">Shareaza Project</a></p></td>
<td><p>2002年</p></td>
<td><p>2.5.0.0</p></td>
<td><p>免費</p></td>
<td><p><a href="../Page/GPL.md" title="wikilink">GPL</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/µTorrent.md" title="wikilink">µTorrent</a></p></td>
<td><p><a href="../Page/Ludvig_Strigeus.md" title="wikilink">Ludvig Strigeus</a></p></td>
<td><p>2005-09-18</p></td>
<td><p>1.8</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/WinMX.md" title="wikilink">WinMX</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>3.53</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/WinNY.md" title="wikilink">WinNY</a></p></td>
<td><p><a href="../Page/金子勇.md" title="wikilink">金子勇</a></p></td>
<td><p>-</p></td>
<td><p>2.0β7.26</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/比特精靈.md" title="wikilink">比特精靈</a></p></td>
<td><p><a href="../Page/BitSpirit_Inc..md" title="wikilink">BitSpirit Inc.</a></p></td>
<td><p>-</p></td>
<td><p>3.2.0.080</p></td>
<td><p>免費</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>創作者</p></td>
<td><p>首個公開發表日期</p></td>
<td><p>最新穩定版本</p></td>
<td><p>價格（<a href="../Page/美元.md" title="wikilink">美元</a>）</p></td>
<td><p><a href="../Page/:en:Software_license.md" title="wikilink">軟體授權</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 檔案分享協議支援

各程式支援的協議。

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/Ares.md" title="wikilink">Ares</a></p></th>
<th><p><a href="../Page/BitTorrent.md" title="wikilink">BitTorrent</a></p></th>
<th><p><a href="../Page/Direct_Connect.md" title="wikilink">Direct Connect</a></p></th>
<th><p><a href="../Page/eDonkey.md" title="wikilink">eDonkey</a></p></th>
<th><p><a href="../Page/FastTrack.md" title="wikilink">FastTrack</a></p></th>
<th><p><a href="../Page/Gnutella.md" title="wikilink">Gnutella</a></p></th>
<th><p><a href="../Page/Kademlia.md" title="wikilink">Kademlia (Kad Network)</a></p></th>
<th><p><a href="../Page/Neo_Network.md" title="wikilink">Neo Network</a></p></th>
<th><p><a href="../Page/OpenNap.md" title="wikilink">OpenNap</a></p></th>
<th><p><a href="../Page/Overnet.md" title="wikilink">Overnet</a></p></th>
<th><p><a href="../Page/WinNY.md" title="wikilink">WinNY</a></p></th>
<th><p><a href="../Page/WPNP.md" title="wikilink">WPNP</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/aMule.md" title="wikilink">aMule</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Ares_Galaxy.md" title="wikilink">Ares Galaxy</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Azureus.md" title="wikilink">Azureus</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BearShare.md" title="wikilink">BearShare</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BitComet.md" title="wikilink">BitComet</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p> <sup><a href="../Page/#fn_2.md" title="wikilink">2</a></sup></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BitLord.md" title="wikilink">BitLord</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BitTornado.md" title="wikilink">BitTornado</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BitTorrent.md" title="wikilink">BitTorrent</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BT_Plus!.md" title="wikilink">BT Plus!</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DCPlusPlus.md" title="wikilink">DC++</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/eDonkey_2000.md" title="wikilink">eDonkey 2000</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/eMule.md" title="wikilink">eMule</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/eMule_Plus.md" title="wikilink">eMule Plus</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/eXeem.md" title="wikilink">eXeem</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FrostWire.md" title="wikilink">FrostWire</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/giFT-FastTrack.md" title="wikilink">giFT-FastTrack</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/iMesh.md" title="wikilink">iMesh</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p> <sup><a href="../Page/#fn_1.md" title="wikilink">1</a></sup></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/KazaA.md" title="wikilink">KazaA</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LimeWire.md" title="wikilink">LimeWire</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/MLDonkey.md" title="wikilink">MLDonkey</a></p></td>
<td></td>
<td></td>
<td><p> <sup><a href="../Page/#fn_3.md" title="wikilink">3</a></sup></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Morpheus.md" title="wikilink">Morpheus</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p> <sup><a href="../Page/#fn_2.md" title="wikilink">2</a></sup></p></td>
<td><p> <sup><a href="../Page/#fn_2.md" title="wikilink">2</a></sup></p></td>
<td><p> <sup><a href="../Page/#fn_1.md" title="wikilink">1</a></sup></p></td>
<td></td>
<td></td>
<td></td>
<td><p> <sup><a href="../Page/#fn_2.md" title="wikilink">2</a></sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Napster.md" title="wikilink">Napster</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Shareaza.md" title="wikilink">Shareaza</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p> <sup><a href="../Page/#fn_1.md" title="wikilink">1</a></sup></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/µTorrent.md" title="wikilink">µTorrent</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/WinMX.md" title="wikilink">WinMX</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/WinNY.md" title="wikilink">WinNY</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/比特精靈.md" title="wikilink">比特精靈</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Ares.md" title="wikilink">Ares</a></p></td>
<td><p><a href="../Page/BitTorrent.md" title="wikilink">BitTorrent</a></p></td>
<td><p><a href="../Page/Direct_Connect.md" title="wikilink">Direct Connect</a></p></td>
<td><p><a href="../Page/eDonkey.md" title="wikilink">eDonkey</a></p></td>
<td><p><a href="../Page/FastTrack.md" title="wikilink">FastTrack</a></p></td>
<td><p><a href="../Page/Gnutella.md" title="wikilink">Gnutella</a></p></td>
<td><p><a href="../Page/Kademlia.md" title="wikilink">Kademlia (Kad Network)</a></p></td>
<td><p><a href="../Page/Neo_Network.md" title="wikilink">Neo Network</a></p></td>
<td><p><a href="../Page/OpenNap.md" title="wikilink">OpenNap</a></p></td>
<td><p><a href="../Page/Overnet.md" title="wikilink">Overnet</a></p></td>
<td><p><a href="../Page/WinNY.md" title="wikilink">WinNY</a></p></td>
<td><p><a href="../Page/WPNP.md" title="wikilink">WPNP</a></p></td>
</tr>
</tbody>
</table>

<small><cite id="fn_1">[註(1):](../Page/#fn_1_back.md "wikilink")</cite>
同時支-{zh-hans:持;
zh-hant:援;}-[Gnutella2](../Page/Gnutella2.md "wikilink")。
<cite id="fn_2">[註(2):](../Page/#fn_2_back.md "wikilink")</cite>
以[插件支](../Page/插件.md "wikilink")-{zh-hans:持; zh-hant:援;}-。
<cite id="fn_3">[註(3):](../Page/#fn_3_back.md "wikilink")</cite>
由于许多MLDonkey用户选择向DC服务器报告夸大的共享资料容量，所以mldc受到许多DC服务器的封杀。 </small>

## 作業系統支-{zh-hans:持; zh-hant:援;}-

程式在毋須模擬下所支-{zh-hans:持; zh-hant:援;}-的[操作系统](../Page/操作系统.md "wikilink")。

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/Windows.md" title="wikilink">Windows</a></p></th>
<th><p><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></th>
<th><p><a href="../Page/Mac_OS_9.md" title="wikilink">Mac OS 9</a></p></th>
<th><p><a href="../Page/Linux.md" title="wikilink">Linux</a></p></th>
<th><p><a href="../Page/BSD.md" title="wikilink">BSD</a></p></th>
<th><p><a href="../Page/Solaris.md" title="wikilink">Solaris</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/aMule.md" title="wikilink">aMule</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Ares_Galaxy.md" title="wikilink">Ares Galaxy</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Azureus.md" title="wikilink">Azureus</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BearShare.md" title="wikilink">BearShare</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BitComet.md" title="wikilink">BitComet</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BitLord.md" title="wikilink">BitLord</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BitTornado.md" title="wikilink">BitTornado</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BitTorrent.md" title="wikilink">BitTorrent</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BT_Plus!.md" title="wikilink">BT Plus!</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DCPlusPlus.md" title="wikilink">DC++</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/eDonkey_2000.md" title="wikilink">eDonkey 2000</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/eMule.md" title="wikilink">eMule</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/eMule_Plus.md" title="wikilink">eMule Plus</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/eXeem.md" title="wikilink">eXeem</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/FrostWire.md" title="wikilink">FrostWire</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/giFT-FastTrack.md" title="wikilink">giFT-FastTrack</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/iMesh.md" title="wikilink">iMesh</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/KazaA.md" title="wikilink">KazaA</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LimeWire.md" title="wikilink">LimeWire</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Maze.md" title="wikilink">Maze</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>仅服务器端</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/MLDonkey.md" title="wikilink">MLDonkey</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Morpheus.md" title="wikilink">Morpheus</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Napster.md" title="wikilink">Napster</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Shareaza.md" title="wikilink">Shareaza</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/µTorrent.md" title="wikilink">µTorrent</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/WinMX.md" title="wikilink">WinMX</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/WinNY.md" title="wikilink">WinNY</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/比特精靈.md" title="wikilink">比特精靈</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a></p></td>
<td><p><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></td>
<td><p><a href="../Page/Mac_OS_9.md" title="wikilink">Mac OS 9</a></p></td>
<td><p><a href="../Page/Linux.md" title="wikilink">Linux</a></p></td>
<td><p><a href="../Page/BSD.md" title="wikilink">BSD</a></p></td>
<td><p><a href="../Page/Solaris.md" title="wikilink">Solaris</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參閱

  - [檔案分享](../Page/檔案分享.md "wikilink")
  - [點對點技術](../Page/點對點技術.md "wikilink")
  - [eD2k软件比较](../Page/eD2k软件比较.md "wikilink")

[Category:软件比较](../Category/软件比较.md "wikilink")
[\*](../Category/檔案分享程式.md "wikilink")