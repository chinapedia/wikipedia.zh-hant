[Okawara_Kunio_"The_World_of_Gundam"_at_Opening_Ceremony_of_the_28th_Tokyo_International_Film_Festival_(22431137765).jpg](https://zh.wikipedia.org/wiki/File:Okawara_Kunio_"The_World_of_Gundam"_at_Opening_Ceremony_of_the_28th_Tokyo_International_Film_Festival_\(22431137765\).jpg "fig:Okawara_Kunio_\"The_World_of_Gundam\"_at_Opening_Ceremony_of_the_28th_Tokyo_International_Film_Festival_(22431137765).jpg")（2015年）\]\]

**大河原邦男**（）是[日本](../Page/日本.md "wikilink")[動畫業界最初的專職](../Page/動畫.md "wikilink")[機械設定師](../Page/機械設定.md "wikilink")。以設計[機動戰士鋼彈系列當中的](../Page/機動戰士鋼彈系列.md "wikilink")[機動戰士而聞名](../Page/機動戰士.md "wikilink")。居住於[東京都](../Page/東京都.md "wikilink")[稻城市](../Page/稻城市.md "wikilink")，以自宅為工作室。

## 簡歷

大河原畢業於[東京造型大學材質設計科](../Page/東京造型大學.md "wikilink")，曾經做過服裝設計師。其後加入[龍之子製作公司美術部門負責背景作畫](../Page/龍之子.md "wikilink")，受部長中村光毅的推薦而擔當《[科學小飛俠](../Page/科學小飛俠.md "wikilink")》的敵方機械設定。在《[鋼鐵小英雄](../Page/鋼鐵小英雄.md "wikilink")》（）片中，初次擔任主要機械的設計工作。經過《[幻影時光](../Page/幻影時光.md "wikilink")》（）系列的工作之後，於1978年獨立成為自由造型師，並開始為[Sunrise的](../Page/Sunrise.md "wikilink")[機器人動畫如](../Page/機器人動畫.md "wikilink")《無敵鋼人泰坦3》、「勇者系列」以及[GUNDAM系列作品系列作品等](../Page/GUNDAM系列作品.md "wikilink")。由於GUNDAM系列所帶起的風潮，使得大河原也開始聲名大噪，也使得一般動畫迷廣泛的認知「機械設定」這樣的工作。自GUNDAM以後的Sunrise系機器人動畫，也多有大河原的參與。

## 作品年表

（諸多作品名稱等待翻譯）

  - 1972年 - [科學小飛俠](../Page/科學小飛俠.md "wikilink")
      -
        以大河原邦男本人為藍本的機械開發者**小河原博士**曾經在本作中登場。
  - 1974年 - [旋風俠](../Page/旋風俠.md "wikilink")（破裏拳ポリマー）
  - 1975年 - [宇宙騎士](../Page/宇宙騎士.md "wikilink")
  - 1976年 - [太空小五義](../Page/太空小五義.md "wikilink")（ゴワッパー5
    ゴーダム）、[幻影時光](../Page/幻影時光.md "wikilink")（タイムボカン）、[無敵飛天俠](../Page/無敵飛天俠.md "wikilink")（ブロッカー軍団IV
    マシーンブラスター）
  - 1977年 - [正義雙俠](../Page/正義雙俠.md "wikilink")（ヤッターマン）、
    [合身戰隊美甘達](../Page/合身戰隊美甘達.md "wikilink")（合身戦隊メカンダーロボ）、[超合體魔術機械](../Page/超合體魔術機械.md "wikilink")（超合体魔術ロボ
    ギンガイザー）、[萬能賽車飛龍號](../Page/萬能賽車飛龍號.md "wikilink")（とびだせ\!マシーン飛竜）、激走\!ルーベンカイザー
  - 1978年 -
    [無敵鋼人泰坦3](../Page/無敵鋼人泰坦3.md "wikilink")、[宇宙魔神](../Page/宇宙魔神.md "wikilink")（宇宙魔神ダイケンゴー）、[神勇飛鷹俠2](../Page/神勇飛鷹俠2.md "wikilink")（科学忍者隊ガッチャマンII）
  - 1979年 -
    [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")、[Z超人](../Page/Z超人.md "wikilink")（ゼンダマン）、[宇宙超人](../Page/宇宙超人.md "wikilink")（ザ・ウルトラマン）、[太空飛鷹俠](../Page/太空飛鷹俠.md "wikilink")（科学忍者隊ガッチャマンF）
  - 1980年 - [無敵機器人
    托萊達G7](../Page/無敵機器人_托萊達G7.md "wikilink")（無敵ロボトライダーG7）、タイムパトロール隊オタスケマン、[無比敵](../Page/無比敵.md "wikilink")（とんでも戦士ムテキング）
  - 1981年 -
    [太陽之牙](../Page/太陽之牙.md "wikilink")、ヤットデタマン、[最強大王者](../Page/最強大王者.md "wikilink")（最強ロボ
    ダイオージャ）、[大雄的宇宙開拓史](../Page/大雄的宇宙開拓史.md "wikilink")、[海底大戰爭](../Page/海底大戰爭.md "wikilink")
  - 1982年 - [戰鬥裝甲](../Page/戰鬥裝甲.md "wikilink")（戦闘メカザブングル）、逆転イッパツマン
  - 1983年 -
    [裝甲騎兵](../Page/裝甲騎兵.md "wikilink")、[銀河漂流](../Page/銀河漂流.md "wikilink")、[未來警察](../Page/未來警察.md "wikilink")
  - 1984年 - [機甲界](../Page/機甲界.md "wikilink")、超力ロボ ガラット
  - 1985年 - [蒼藍流星](../Page/蒼藍流星.md "wikilink")（蒼き流星SPTレイズナー）、[機動戰士Z
    GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（高達MK-II、重高達、重高達MK-II等）
  - 1986年 - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")（デザイン協力）
  - 1987年 - [機甲戰記](../Page/機甲戰記.md "wikilink")
  - 1988年 - [機甲獵兵](../Page/機甲獵兵.md "wikilink")
  - 1989年 - [魔動王](../Page/魔動王.md "wikilink")、[机动战士Gundam
    0080:口袋里的战争](../Page/机动战士Gundam_0080:口袋里的战争.md "wikilink")（MS原案）
  - 1990年 -
    [勇者凱撒](../Page/勇者凱撒.md "wikilink")、[機甲武士劍](../Page/機甲武士劍.md "wikilink"))(からくり剣豪伝ムサシロード)
  - 1991年 - [太陽勇者](../Page/太陽勇者.md "wikilink")、[機動戰士GUNDAM
    F91](../Page/機動戰士GUNDAM_F91.md "wikilink")、[機動戦士ガンダム0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")（MS原案）
  - 1992年 - [勇者傳說](../Page/勇者傳說.md "wikilink")
  - 1993年 - [勇者特急隊](../Page/勇者特急隊.md "wikilink")、[機動戰士V
    GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")、[疾風無敵銀堡壘](../Page/疾風無敵銀堡壘.md "wikilink")（疾風\!アイアンリーガー）
  - 1994年 - [勇者警察](../Page/勇者警察.md "wikilink")、[机动武斗传G
    GUNDAM](../Page/机动武斗传G_GUNDAM.md "wikilink")(撲克聯盟成員)
  - 1995年 - [黃金勇者](../Page/黃金勇者.md "wikilink")、[新機動戰記GUNDAM
    W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - 1996年 - [機動戰士GUNDAM
    第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")、[勇者指令](../Page/勇者指令.md "wikilink")、[機動新世紀GUNDAM
    X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - 1997年 - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")、[勇者王](../Page/勇者王.md "wikilink")
  - 1999年 -
    [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")、[進化戰記](../Page/進化戰記.md "wikilink")（ベターマン）
  - 2000年 - [勇者王GAOGAIGAR FINAL](../Page/勇者王.md "wikilink")、怪盗きらめきマン
  - 2002年 - [超重神](../Page/超重神.md "wikilink")、[機動戰士GUNDAM
    SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - 2004年 - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")
  - 2005年 - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink") 星之繼承者
  - 2006年 - [機動戰士GUNDAM SEED C.E.73
    STARGAZER](../Page/機動戰士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")
  - 2007年 - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")([O
    GUNDAM](../Page/O_GUNDAM.md "wikilink")、[GNMA-XCVII
    Alvatore設計](../Page/GNMA-XCVII_Alvatore.md "wikilink"))
  - 2008年 - 機動戰士GUNDAM 00<span style="font-size:smaller;">（第二季）([O
    GUNDAM實戰配備型](../Page/O_GUNDAM實戰配備型.md "wikilink")、[Reborn
    Gundam設計](../Page/Reborn_Gundam.md "wikilink")\<Reborns
    gundam的設計者是海老川兼武)</span>
  - 2014年 - [宇宙浪子](../Page/宇宙浪子.md "wikilink")(スペース☆ダンディ)

## 其他

曾經在1981年NHK兒童節目「」當中受訪時透露「GUNDAM的初期設定是有嘴巴的」

[Category:機甲設計師](../Category/機甲設計師.md "wikilink")