**比氏鼯鼠**（學名：**''Biswamoyopterus biswasi**''、英語名：**Namdapha Flying
Squirrel**）是一種[飛鼠](../Page/飛鼠.md "wikilink")，也是**比氏鼯鼠屬**（***Biswamoyopterus***）中的唯一[物種](../Page/物種.md "wikilink")。此物種原生於[印度](../Page/印度.md "wikilink")，首次標本收集於1981年4月27日，目前的保育狀況處於極危（CR）狀態。

## 參考文獻

  - Database entry includes a brief justification of why this species is
    critically endangered and the criteria used

  - Saha, S. S. 1981 A new Genus and a new species of flying squirrel
    (Mammalia: Rodentia: Sciuridae) from northeastern India. Bull. Zool.
    surv. India 4: (3): 331-336

  - Saha, S. S. 1985 Mammalia Rec. Zool.surv. India (Special Issue on
    Fauna of Namdapha: Arunachal Pradesh: A Proposed Biosphere Reserve)
    82: (1-4): 321-330

[Category:比氏鼯鼠屬](../Category/比氏鼯鼠屬.md "wikilink")