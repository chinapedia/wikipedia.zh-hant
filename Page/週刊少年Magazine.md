《**週刊少年Magazine**》（[日文原名](../Page/日文.md "wikilink")：）是[日本](../Page/日本.md "wikilink")[講談社出版的](../Page/講談社.md "wikilink")[少年漫畫雜誌](../Page/少年漫畫.md "wikilink")，於1959年3月17日創刊，每周三發售（和[小學館](../Page/小學館.md "wikilink")《[週刊少年Sunday](../Page/週刊少年Sunday.md "wikilink")》相同）。雖然目前總銷量落後[集英社](../Page/集英社.md "wikilink")《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》不少，但仍有不少著名[漫畫家在此雜誌連載漫畫](../Page/漫畫家.md "wikilink")。事實上，於1970年代，此雜誌的總銷量比《週刊少年Jump》還高。

[台灣的](../Page/台灣.md "wikilink")《[新少年快報](../Page/新少年快報.md "wikilink")》和[香港的](../Page/香港.md "wikilink")《[新少年週刊](../Page/新少年週刊.md "wikilink")》乃此漫畫雜誌的中文版，兩者均由[東立出版社出版](../Page/東立出版社.md "wikilink")。
[Shōnen_Magazine_first_issue.jpg](https://zh.wikipedia.org/wiki/File:Shōnen_Magazine_first_issue.jpg "fig:Shōnen_Magazine_first_issue.jpg")為第46代[橫綱](../Page/橫綱.md "wikilink")[朝潮太郎](../Page/朝潮太郎_\(3代\).md "wikilink")（右）\]\]

## 連載中作品

  -

<table>
<thead>
<tr class="header">
<th><p>作品名稱（中文）</p></th>
<th><p>作品名稱（日文）</p></th>
<th><p>作者（作畫）</p></th>
<th><p>原作</p></th>
<th><p>起載號</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/第一神拳.md" title="wikilink">第一神拳</a></p></td>
<td></td>
<td><p><a href="../Page/森川讓次.md" title="wikilink">森川讓次</a></p></td>
<td><p>－</p></td>
<td><p>1989年43号</p></td>
<td><p>未在電子版中刊載</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/籃球少年王.md" title="wikilink">籃球少年王</a></p></td>
<td></td>
<td><p><a href="../Page/日向武史.md" title="wikilink">日向武史</a></p></td>
<td><p>－</p></td>
<td><p>2004年2・3合併号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/妄想學生會.md" title="wikilink">妄想學生會</a></p></td>
<td></td>
<td><p><a href="../Page/氏家ト全.md" title="wikilink">氏家ト全</a></p></td>
<td><p>－</p></td>
<td><p>2008年34号</p></td>
<td><p>從《<a href="../Page/Magazine_SPECIAL.md" title="wikilink">Magazine SPECIAL</a>》移籍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/七大罪_(漫畫).md" title="wikilink">七大罪</a></p></td>
<td></td>
<td><p><a href="../Page/鈴木央.md" title="wikilink">鈴木央</a></p></td>
<td><p>－</p></td>
<td><p>2012年45号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/DAYS_(漫畫).md" title="wikilink">DAYS</a></p></td>
<td></td>
<td><p><a href="../Page/安田剛士.md" title="wikilink">安田剛士</a></p></td>
<td><p>－</p></td>
<td><p>2013年21・22合併号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/家有女友.md" title="wikilink">家有女友</a></p></td>
<td></td>
<td><p><a href="../Page/流石景.md" title="wikilink">流石景</a></p></td>
<td><p>－</p></td>
<td><p>2014年21・22合併号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鑽石王牌.md" title="wikilink">鑽石王牌 act2</a></p></td>
<td></td>
<td><p><a href="../Page/寺嶋裕二.md" title="wikilink">寺嶋裕二</a></p></td>
<td><p>－</p></td>
<td><p>2015年38號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/炎炎消防隊.md" title="wikilink">炎炎消防隊</a></p></td>
<td></td>
<td><p><a href="../Page/大久保篤.md" title="wikilink">大久保篤</a></p></td>
<td><p>－</p></td>
<td><p>2015年43號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/川柳少女.md" title="wikilink">川柳少女</a></p></td>
<td></td>
<td><p><a href="../Page/五十嵐正邦.md" title="wikilink">五十嵐正邦</a></p></td>
<td><p>－</p></td>
<td><p>2016年47號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/大今良時.md" title="wikilink">大今良時</a></p></td>
<td><p>－</p></td>
<td><p>2016年50號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東京卍復仇者.md" title="wikilink">東京卍復仇者</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2017年13號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/請在伸展台上微笑.md" title="wikilink">請在伸展台上微笑</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2017年26號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/女朋友，借我一下.md" title="wikilink">女朋友，借我一下</a></p></td>
<td></td>
<td><p><a href="../Page/宮島礼吏.md" title="wikilink">宮島礼吏</a></p></td>
<td><p>－</p></td>
<td><p>2017年32號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五等分的新娘.md" title="wikilink">五等分的新娘</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2017年36・37合併號</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>千田大輔</p></td>
<td><p>－</p></td>
<td><p>2017年41號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寄宿學校的茱麗葉.md" title="wikilink">寄宿學校的茱麗葉</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2017年43號</p></td>
<td><p>從《<a href="../Page/别册少年Magazine.md" title="wikilink">别册少年Magazine</a>》移籍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/化物語.md" title="wikilink">化物語</a></p></td>
<td></td>
<td><p><a href="../Page/大暮維人.md" title="wikilink">大暮維人</a></p></td>
<td><p><a href="../Page/西尾維新.md" title="wikilink">西尾維新</a></p></td>
<td><p>2018年15號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/想包養男子高中生的大姐姐的故事.md" title="wikilink">想包養男子高中生的大姐姐的故事</a></p></td>
<td></td>
<td></td>
<td><p>－</p></td>
<td><p>2018年19號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Orient.md" title="wikilink">Orient</a></p></td>
<td></td>
<td><p><a href="../Page/大高忍.md" title="wikilink">大高忍</a></p></td>
<td><p>－</p></td>
<td><p>2018年26號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/瀨尾公治.md" title="wikilink">瀨尾公治</a></p></td>
<td><p>－</p></td>
<td><p>2018年29號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/EDENS_ZERO.md" title="wikilink">EDENS ZERO</a></p></td>
<td></td>
<td><p><a href="../Page/真島浩.md" title="wikilink">真島浩</a></p></td>
<td><p>－</p></td>
<td><p>2018年30號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BLUE_LOCK.md" title="wikilink">BLUE LOCK</a></p></td>
<td></td>
<td></td>
<td><p>金城宗幸</p></td>
<td><p>2018年35號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賭徒遊行.md" title="wikilink">賭徒遊行</a></p></td>
<td></td>
<td><p><a href="../Page/中山敦支.md" title="wikilink">中山敦支</a></p></td>
<td></td>
<td><p>2018年45號</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/七八五十六.md" title="wikilink">七八五十六</a></p></td>
<td></td>
<td></td>
<td><p>工藤哲孝</p></td>
<td><p>2018年48號</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/山本崇一朗.md" title="wikilink">山本崇一朗</a></p></td>
<td><p>－</p></td>
<td><p>2019年14號</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>高畑弓</p></td>
<td><p>－</p></td>
<td><p>2019年19號</p></td>
<td></td>
</tr>
</tbody>
</table>

## 過去連載過作品

  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")
    （[天樹征丸](../Page/樹林伸.md "wikilink")、[佐藤文也](../Page/佐藤文也.md "wikilink")）
  - [聲之形](../Page/聲之形.md "wikilink")（[大今良時](../Page/大今良時.md "wikilink")）
  - [我妻同學是我的老婆](../Page/我妻同學是我的老婆.md "wikilink")（[蔵石ユウ](../Page/蔵石ユウ.md "wikilink")、[西木田景志](../Page/西木田景志.md "wikilink")）
  - [CØDE:BREAKER
    法外制裁者](../Page/CØDE:BREAKER_法外制裁者.md "wikilink")（[上條明峰](../Page/上條明峰.md "wikilink")）
  - [纯爱涂鸦](../Page/纯爱涂鸦.md "wikilink")
    （[小林俊彦](../Page/小林俊彦.md "wikilink")）
  - [飛輪少年](../Page/飛輪少年.md "wikilink")（[大暮維人](../Page/大暮維人.md "wikilink")）
  - [絕望先生](../Page/絕望先生.md "wikilink")（[久米田康治](../Page/久米田康治.md "wikilink")）
  - [純情房東俏房客](../Page/純情房東俏房客.md "wikilink")（[赤松健](../Page/赤松健.md "wikilink")）
  - [魔法老師](../Page/魔法老師.md "wikilink")（[赤松健](../Page/赤松健.md "wikilink")）
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")（[天樹征丸](../Page/天樹征丸.md "wikilink")、[佐藤文也](../Page/佐藤文也.md "wikilink")）
  - [Tsubasa翼](../Page/Tsubasa翼.md "wikilink")（[CLAMP](../Page/CLAMP.md "wikilink")）
  - [×××HOLiC](../Page/×××HOLiC.md "wikilink")（[CLAMP](../Page/CLAMP.md "wikilink")）
  - [聖石小子](../Page/聖石小子.md "wikilink")（[真島浩](../Page/真島浩.md "wikilink")）
  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")（[小林尽](../Page/小林尽.md "wikilink")）
  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")（[青樹佑夜](../Page/青樹佑夜.md "wikilink")、[綾峰欄人](../Page/綾峰欄人.md "wikilink")）
  - [鬼眼狂刀](../Page/鬼眼狂刀.md "wikilink")（[上條明峰](../Page/上條明峰.md "wikilink")）
  - [涼風](../Page/涼風.md "wikilink")（[瀨尾公治](../Page/瀨尾公治.md "wikilink")）
  - [中華一番](../Page/中華一番.md "wikilink")（[小川悦司](../Page/小川悦司.md "wikilink"))
  - [BLOODY
    MONDAY](../Page/BLOODY_MONDAY.md "wikilink")（[龍門諒](../Page/龍門諒.md "wikilink")、[惠廣史](../Page/惠廣史.md "wikilink"))
  - [鬼太郎](../Page/鬼太郎.md "wikilink")（[水木茂](../Page/水木茂.md "wikilink")）
  - [天才小廚師](../Page/天才小廚師.md "wikilink")（[寺澤大介](../Page/寺澤大介.md "wikilink")）
  - [將太的壽司](../Page/將太的壽司.md "wikilink")（[寺澤大介](../Page/寺澤大介.md "wikilink")）
  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")（[藤澤亨](../Page/藤澤亨.md "wikilink")）
  - [麻辣教師GTO-湘南14日](../Page/麻辣教師GTO-湘南14日.md "wikilink")（[藤澤亨](../Page/藤澤亨.md "wikilink")）
  - [我的女神](../Page/我的女神.md "wikilink")（[藤岛康介](../Page/藤岛康介.md "wikilink")）
  - [功夫旋風兒](../Page/功夫旋風兒.md "wikilink")（[蛭田達也](../Page/蛭田達也.md "wikilink")）
  - [功夫旋風兒柔道篇](../Page/功夫旋風兒柔道篇.md "wikilink")（[蛭田達也](../Page/蛭田達也.md "wikilink")）
  - [新約「巨人の星」花形](../Page/新約「巨人の星」花形.md "wikilink")（[梶原一騎](../Page/梶原一騎.md "wikilink")、[川崎のぼる](../Page/川崎のぼる.md "wikilink")、[村上よしゆき](../Page/村上よしゆき.md "wikilink")）
  - [BUN BUN
    BEE](../Page/BUN_BUN_BEE.md "wikilink")（[榎本智](../Page/榎本智.md "wikilink")）
  - [天生妙手](../Page/天生妙手.md "wikilink")（[山本航暉](../Page/山本航暉.md "wikilink")）
  - [不良仔與眼鏡妹](../Page/不良仔與眼鏡妹.md "wikilink")（[吉河美希](../Page/吉河美希.md "wikilink")）
  - [逃離伊甸園](../Page/逃離伊甸園.md "wikilink")（[山田惠庸](../Page/山田惠庸.md "wikilink")）
  - [GE～Good
    Ending～](../Page/GE～Good_Ending～.md "wikilink")（[流石景](../Page/流石景.md "wikilink")）
  - [宙斯之種](../Page/宙斯之種.md "wikilink")（）（[飯島浩介](../Page/飯島浩介.md "wikilink")）
  - [ばくだん\!〜幕末男子〜](../Page/ばくだん!〜幕末男子〜.md "wikilink")（[加瀬あつし](../Page/加瀬あつし.md "wikilink")）
  - [幽靈神醫](../Page/幽靈神醫.md "wikilink")（）（[大沢祐輔](../Page/大沢祐輔.md "wikilink")、[木下繁](../Page/木下繁.md "wikilink")）
  - [Dragon
    Collection](../Page/Dragon_Collection.md "wikilink")（）（[芝野郷太](../Page/芝野郷太.md "wikilink")、[金城宗幸](../Page/金城宗幸.md "wikilink")、[科樂美數碼娛樂](../Page/科樂美數碼娛樂.md "wikilink")）
  - [GT-R](../Page/GT-R.md "wikilink")（[藤澤亨](../Page/藤澤亨.md "wikilink")）
  - [AKB49～戀愛禁止條例～](../Page/AKB49～戀愛禁止條例～.md "wikilink")
  - [FAIRY
    TAIL魔導少年](../Page/FAIRY_TAIL魔導少年.md "wikilink")（[真島浩](../Page/真島浩.md "wikilink")）

## 關連項目

  - [月刊少年Magazine](../Page/月刊少年Magazine.md "wikilink")
  - [新少年快報](../Page/新少年快報.md "wikilink")

## 外部链接

  - [官方网站](http://www.shonenmagazine.com/)

[\*](../Category/週刊少年Magazine.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:週刊漫畫雜誌](../Category/週刊漫畫雜誌.md "wikilink")
[M](../Category/講談社的漫畫雜誌.md "wikilink")