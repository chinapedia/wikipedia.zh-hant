**美国国际贸易委员会**（United States International Trade
Commission，缩写USITC）是[美国联邦政府下设的一个独立的](../Page/美国联邦政府.md "wikilink")、非[党派性质的](../Page/政党.md "wikilink")、准[司法联邦机构](../Page/司法.md "wikilink")。它负责向[立法机构和](../Page/立法.md "wikilink")[执法机构提供国际贸易方面的专业意见](../Page/执法.md "wikilink")。同时，该机构还负责判断[进口对美国工业的冲击](../Page/进口.md "wikilink")，并且对不公平[贸易](../Page/贸易.md "wikilink")（例如：[倾销及](../Page/倾销.md "wikilink")[专利](../Page/专利.md "wikilink")、[商标及](../Page/商标.md "wikilink")[版权侵犯](../Page/版权.md "wikilink")）采取措施。

## 历史

## 使命

## 听证会

## 參見

  - [國家法律雜誌](../Page/國家法律雜誌.md "wikilink")(The National Law Review)

## 註釋

## 外部链接

  - [官方主页](http://www.usitc.gov)

[Category:美国联邦政府机构](../Category/美国联邦政府机构.md "wikilink")
[Category:美国对外贸易](../Category/美国对外贸易.md "wikilink")
[Category:1916年美國建立](../Category/1916年美國建立.md "wikilink")