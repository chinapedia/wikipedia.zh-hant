**醛縮酶A**（）是一種[酶](../Page/酶.md "wikilink")，可使[果糖-1,6-雙磷酸斷裂轉變成為](../Page/果糖-1,6-雙磷酸.md "wikilink")[甘油醛-3-磷酸和](../Page/甘油醛-3-磷酸.md "wikilink")[二羥丙酮磷酸](../Page/二羥丙酮磷酸.md "wikilink")。是糖解作用裡的一部分。
{{ Complex Enzymatic Reaction
|major_substrate_1=β-<small>D</small>-[果糖1,6-二磷酸](../Page/果糖-1,6-雙磷酸.md "wikilink")
|major_substrate_1_stoichiometric_constant=
|major_substrate_1_image=beta-D-fructose-1,6-bisphosphate_wpmp.png
|major_substrate_2= |major_substrate_2_stoichiometric_constant=
|major_substrate_2_image=
|major_product_1=<small>D</small>-[甘油醛3-磷酸](../Page/甘油醛3-磷酸.md "wikilink")
|major_product_1_stoichiometric_constant=
|major_product_1_image=D-glyceraldehyde-3-phosphate_wpmp.png
|major_product_2=[二羟丙酮磷酸](../Page/二羟丙酮磷酸.md "wikilink")
|major_product_2_stoichiometric_constant=
|major_product_2_image=glycerone-phosphate_wpmp.png
|foward_enzyme=[果糖二磷酸醛缩酶](../Page/果糖二磷酸醛缩酶.md "wikilink")
|reverse_enzyme=[果糖二磷酸醛缩酶](../Page/果糖二磷酸醛缩酶.md "wikilink")
|reaction_direction_(foward/reversible/reverse)=reversible
|minor_foward_substrate(s)= |minor_foward_product(s)=
|minor_reverse_product(s)= |minor_reverse_substrate(s)= }}

## 参考文献

## 外部連結

  - <http://pdbdev.sdsc.edu:48346/pdb/molecules/pdb50_5.html>

  -
  -
  -
## 反應機制

[ALDO_reaction.png](https://zh.wikipedia.org/wiki/File:ALDO_reaction.png "fig:ALDO_reaction.png")


[Category:糖酵解](../Category/糖酵解.md "wikilink")
[Category:酶](../Category/酶.md "wikilink")