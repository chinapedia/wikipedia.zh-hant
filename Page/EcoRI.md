[EcoRI_restriction_enzyme_recognition_site.svg](https://zh.wikipedia.org/wiki/File:EcoRI_restriction_enzyme_recognition_site.svg "fig:EcoRI_restriction_enzyme_recognition_site.svg")[ECOR1_Crystal_Structure.rsh.png](https://zh.wikipedia.org/wiki/File:ECOR1_Crystal_Structure.rsh.png "fig:ECOR1_Crystal_Structure.rsh.png")

***Eco*RI**（「I」是「1」的意思）是一種[核酸酶](../Page/核酸酶.md "wikilink")，來自某些特定品系的[大腸桿菌](../Page/大腸桿菌.md "wikilink")（*E.
coli*，也是其名稱由來），是此種細菌體內參與[限制修飾系統的酵素](../Page/限制修飾系統.md "wikilink")。

在[分子生物學或其他分子層次的生物學研究上](../Page/分子生物學.md "wikilink")，EcoRI是一種常用的[限制酶](../Page/限制酶.md "wikilink")，可將特定序列的[DNA切割成片段](../Page/DNA.md "wikilink")，切割後的小片段端點為5'端突出的[黏狀末端](../Page/黏狀末端.md "wikilink")。

## 參見

  - [EcoRV](../Page/EcoRV.md "wikilink")
  - [FokI](../Page/FokI.md "wikilink")

## 參考文獻

  - Bitinaite, J., D. A. Wah, Aggarwal, A. K., Schildkraut, I. (1998).
    "FokI dimerization is required for DNA cleavage." Proc Natl Acad Sci
    U S A 95(18): 10570-5.

<!-- end list -->

  - Pingoud, A., Jeltsch, A. (2001) Structure and function of type II
    restriction endonucleases, Nucl. Acids Res. 29. 18, 3705-3727.

<!-- end list -->

  - McClarin, J.A., Frederick, C.A., Wang, B.C., Greene, P., Boyer,
    H.W., Grable, J., Rosenberg, J.M. (1986) Structure of the DNA-EcoRI
    endonucleases recognition complex at 3 Å resolution, Science. 234.
    4783, 1526-1542.

<!-- end list -->

  - Kurpiewski, M.R., Engler, L.E., Wozniak, L.A., Kobylanska, A.,
    Koziolkiewicz, M., Stec, W.J, Jen-Jacobson, L. (2004) Mechanisms of
    coupling between DNA recognition and catalysis in EcoRI
    endonucleases, Structure 12. 1775-1788.

## 外部連結

  - [Restriction Enzyme Digestion of Plasmid
    DNA](http://biologicalworld.com/digestions.htm)

  -
[Category:EC 3.1.21](../Category/EC_3.1.21.md "wikilink")
[Category:分子生物學](../Category/分子生物學.md "wikilink")
[Category:细菌酶](../Category/细菌酶.md "wikilink")
[Category:限制酶](../Category/限制酶.md "wikilink")