[Half_Portraits_of_the_Great_Sage_and_Virtuous_Men_of_Old_-_Yan_Yan_Ziyou_(言偃_子游).jpg](https://zh.wikipedia.org/wiki/File:Half_Portraits_of_the_Great_Sage_and_Virtuous_Men_of_Old_-_Yan_Yan_Ziyou_\(言偃_子游\).jpg "fig:Half_Portraits_of_the_Great_Sage_and_Virtuous_Men_of_Old_-_Yan_Yan_Ziyou_(言偃_子游).jpg")藏）。\]\]
**言偃**（），[字](../Page/表字.md "wikilink")**子游**，亦稱言游，又稱叔氏，春秋末[吳國](../Page/吳國.md "wikilink")（籍贯在今[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")[常熟市](../Page/常熟市.md "wikilink")）人，是[孔子七十二弟子中唯一](../Page/孔子七十二弟子.md "wikilink")[南方人](../Page/中国南方.md "wikilink")，後學成南歸，道啟東南，對江南文化的繁榮有很大貢獻，被譽為“南方夫子”，尊称**言子**，[唐](../Page/唐朝.md "wikilink")[開元封](../Page/開元.md "wikilink")「吳侯」，[宋封](../Page/宋朝.md "wikilink")「丹陽公」，後又稱「吳公」。

時子游為武城宰，以禮樂為教，故邑人皆弦歌也。子游特習於禮，在[孔門十哲中以](../Page/孔門十哲.md "wikilink")[文學著名](../Page/文學.md "wikilink")。曾任[魯國武城宰](../Page/魯國.md "wikilink")，用禮樂教育士民，境內到處有弦歌之聲，[孔子對此表示讚賞](../Page/孔子.md "wikilink")——[孔子曾說](../Page/孔子.md "wikilink")：“君子學道則愛人，小人學道則易使。”（[論語](../Page/論語.md "wikilink")·陽貨:17.4）。

子游和[子夏同列](../Page/子夏.md "wikilink")[孔門十哲的文學科](../Page/孔門十哲.md "wikilink")，文學指詩、書、禮、樂文章而言，所以子游之學以習禮自見，更重要的是他能行禮樂之教。

## 論語中之子游

《論語·陽貨》：子之武城，聞弦歌聲。夫子莞爾而笑曰：「割雞焉用牛刀。」子游對曰：「昔者偃也聞諸夫子曰：君子學道則愛人，小人學道則易使也。」子曰：「二三子，偃之言是也。前言戲之耳。」（陽貨）[孔子的說法一方面是惋惜子游大材小用](../Page/孔子.md "wikilink")，一方面是對子游能行禮樂表示欣慰。

《論語·為政》：子游問孝。子曰：「今之孝者，是謂能養，至於犬馬，皆能有養；不敬，何以別乎？」語譯：子游向孔子請問孝道。孔子回答說：「現在一般所謂的孝順父母，認為只要做到養活父母，就算是盡孝了。如此說來，人飼養犬馬等動物時，也一樣供給牠們食物，如果不以恭敬之心侍奉父母，那和養動物又有何區別呢？」

孔子論孝重在一個「敬」字，養而能敬，才算合了孝的內外之道於一體。

## 後人追封

  - [唐](../Page/唐朝.md "wikilink")[開元](../Page/開元.md "wikilink")，封「吳侯」，
  - [宋](../Page/宋朝.md "wikilink")，封「丹陽公」，後又稱「吳公」。

## 參看

  - [孔子](../Page/孔子.md "wikilink")

[category:儒家](../Page/category:儒家.md "wikilink")
[category:春秋戰國儒家人物](../Page/category:春秋戰國儒家人物.md "wikilink")
[category:常熟人](../Page/category:常熟人.md "wikilink")

[Category:春秋戰國人物](../Category/春秋戰國人物.md "wikilink")
[Category:孔子弟子](../Category/孔子弟子.md "wikilink")
[Category:吴国人](../Category/吴国人.md "wikilink")
[Category:言姓](../Category/言姓.md "wikilink")
[Category:葬于常熟](../Category/葬于常熟.md "wikilink")