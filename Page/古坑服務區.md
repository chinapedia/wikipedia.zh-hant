[古坑服務區.JPG](https://zh.wikipedia.org/wiki/File:古坑服務區.JPG "fig:古坑服務區.JPG")
[古坑服務區_(夜).JPG](https://zh.wikipedia.org/wiki/File:古坑服務區_\(夜\).JPG "fig:古坑服務區_(夜).JPG")
**古坑服務區**是位於[台灣](../Page/台灣.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[古坑鄉的](../Page/古坑鄉.md "wikilink")[高速公路服務區](../Page/公路服務區.md "wikilink")，里程為[福爾摩沙高速公路](../Page/福爾摩沙高速公路.md "wikilink")（國道三號）276公里，南北共站，自[基金交流道](../Page/基金交流道.md "wikilink")（起點）以來的第五個服務區，

於2002年7月2日成立，經營商是海景世界企業股份有限公司，2010年2月1日出口處增設一間由[台灣中油股份有限公司經營的](../Page/台灣中油.md "wikilink")24小時便捷[加油站](../Page/加油站.md "wikilink")。

## 規劃

古坑服務區造型為一曲面屋頂以仿古造型之建築，戶外空間規劃為水舞廣場、香花區、誘鳥區及生態區，並且以「咖啡、花香、鳥鳴、水舞、彩虹、夜景」為設計主題。
[Gukeng_Service_Area,_fountain,_Yunlin_County_(Taiwan).jpg](https://zh.wikipedia.org/wiki/File:Gukeng_Service_Area,_fountain,_Yunlin_County_\(Taiwan\).jpg "fig:Gukeng_Service_Area,_fountain,_Yunlin_County_(Taiwan).jpg")
[Gukeng_Service_Area,_Yunlin_County_(Taiwan).jpg](https://zh.wikipedia.org/wiki/File:Gukeng_Service_Area,_Yunlin_County_\(Taiwan\).jpg "fig:Gukeng_Service_Area,_Yunlin_County_(Taiwan).jpg")
服務區南北共站，設有駕駛人休息室，讓駕駛人可短暫睡眠，以避免[疲勞駕駛釀成事故](../Page/疲勞駕駛.md "wikilink")。\[1\]

## 基本資料

  - 位於國道3號276k北上側，佔地面積約29公頃。
  - 經營承商：海景世界企業股份有限公司，經營期限自2013年7月2日至2019年8月31日止。

## 位置

## 参考文献

## 外部連結

  - [南仁湖企業古坑服務區](http://www.nanrenhu.com.tw/hiwayshopping/index.php?controller=gukeng2)
  - [南仁湖fun國道](https://www.facebook.com/crazyway/)
  - [古坑服務區 幸福、莊園
    花香蝶舞](https://www.facebook.com/%E5%8F%A4%E5%9D%91%E6%9C%8D%E5%8B%99%E5%8D%80-%E5%B9%B8%E7%A6%8F%E8%8E%8A%E5%9C%92-%E8%8A%B1%E9%A6%99%E8%9D%B6%E8%88%9E-654453691337984/)

[G](../Category/2001年完工交通基礎設施.md "wikilink")
[G](../Category/2001年台灣建立.md "wikilink")
[G](../Category/台灣高速公路服務區.md "wikilink")
[G](../Category/國道三號_\(中華民國\).md "wikilink")
[G](../Category/古坑鄉.md "wikilink")
[G](../Category/雲林縣旅遊景點.md "wikilink")
[G](../Category/雲林縣旅遊人文景點.md "wikilink")
[國](../Category/雲林縣道路.md "wikilink")

1.  [國道駕駛人休息室(含簡易休憩區)服務資訊](http://www.freeway.gov.tw/Publish.aspx?cnid=2293&p=6656)