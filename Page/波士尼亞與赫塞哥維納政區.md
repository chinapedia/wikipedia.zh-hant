[Municipalities_and_regions_of_Bosnia_and_Herzegovina.svg](https://zh.wikipedia.org/wiki/File:Municipalities_and_regions_of_Bosnia_and_Herzegovina.svg "fig:Municipalities_and_regions_of_Bosnia_and_Herzegovina.svg")
 波斯尼亚與黑塞哥维那政治實體劃分：

1.
2.
3.  [布爾奇科特區](../Page/布爾奇科特區.md "wikilink")

根據1995年11月21日所簽署的[代頓協議](../Page/代頓協議.md "wikilink")，[波士尼亞與赫塞哥維納區分為兩個政治實體](../Page/波士尼亞與赫塞哥維納.md "wikilink")：[波黑聯邦](../Page/波黑聯邦.md "wikilink")（）與[塞族共和國](../Page/塞族共和國.md "wikilink")（），雙方各自統領一半的國土，擁有自己的首都、政府、国旗、国徽、领导人、議会等。

位於[波黑東北部的](../Page/波黑.md "wikilink")[布爾奇科特區](../Page/布爾奇科特區.md "wikilink")[法律上由](../Page/法律上.md "wikilink")[波黑联邦和](../Page/波黑联邦.md "wikilink")[塞族共和国共管](../Page/塞族共和国.md "wikilink")，[事實上是一个高度自治的政治实体](../Page/事實上.md "wikilink")（半自治區）。

## 波赫聯邦區劃

[州](../Page/州.md "wikilink")<small>（[英](../Page/英語.md "wikilink")：；[波](../Page/波士尼亞語.md "wikilink")：；[克](../Page/克羅埃西亞語.md "wikilink")：；[塞](../Page/塞爾維亞語.md "wikilink")：）</small>是波赫聯邦的地方政府層級。

1.  [烏納-薩納州](../Page/烏納-薩納州.md "wikilink")（）
2.  [波薩維納州](../Page/波薩維納州.md "wikilink")（）
3.  [圖茲拉州](../Page/圖茲拉州.md "wikilink")（）
4.  [澤尼察-多博伊州](../Page/澤尼察-多博伊州.md "wikilink")（）
5.  [波斯尼亞-波德里涅州](../Page/波斯尼亞-波德里涅州.md "wikilink")（）
6.  [中波斯尼亞州](../Page/中波斯尼亞州.md "wikilink")（）
7.  [黑塞哥維那-涅雷特瓦州](../Page/黑塞哥維那-涅雷特瓦州.md "wikilink")（）
8.  [西黑塞哥維那州](../Page/西黑塞哥維那州.md "wikilink")（）
9.  [薩拉熱窩州](../Page/薩拉熱窩州.md "wikilink")（）
10. [西波斯尼亞州](../Page/西波斯尼亞州.md "wikilink")（）

## 塞族共和國區劃

塞族共和國所轄領土分為7區：
[BosniaRegionsRS.png](https://zh.wikipedia.org/wiki/File:BosniaRegionsRS.png "fig:BosniaRegionsRS.png")
1.[班雅盧卡區](../Page/班雅盧卡區.md "wikilink")
2\.[多波伊區](../Page/多波伊區.md "wikilink")
3\.[比耶吉納區](../Page/比耶吉納區.md "wikilink")
4\.[弗拉塞尼察區](../Page/弗拉塞尼察區.md "wikilink")
5\.[沙拉耶佛區](../Page/沙拉耶佛區.md "wikilink")
6\.[福察區](../Page/福察區.md "wikilink")
7\.[特雷比涅區](../Page/特雷比涅區.md "wikilink")

## 布爾奇科特區

[DistriktBrckonaselja.PNG](https://zh.wikipedia.org/wiki/File:DistriktBrckonaselja.PNG "fig:DistriktBrckonaselja.PNG")[Brcko_in_BiH.png](https://zh.wikipedia.org/wiki/File:Brcko_in_BiH.png "fig:Brcko_in_BiH.png")
[布爾奇科特區是波斯尼亞的一個自治區](../Page/布爾奇科特區.md "wikilink")，名義上屬[波黑聯邦和](../Page/波黑聯邦.md "wikilink")[塞族共和國共同所有](../Page/塞族共和國.md "wikilink")，設立於2000年3月，面積493平方公里。

## 參考資料

## 外部連結

  - [A precarious
    peace](http://www.economist.com/surveys/displaystory.cfm?story_id=602361),
    [The Economist](../Page/The_Economist.md "wikilink"), 22 January
    1998
  - [The EU´s pseudosuccess in
    Bosnia](https://web.archive.org/web/20110805022633/http://www.osw.waw.pl/en/publikacje/ceweekly/2011-05-18/eu-s-pseudosuccess-bosnia-and-herzegovina),
    [Centre for Eastern
    Studies](../Page/Centre_for_Eastern_Studies.md "wikilink") 2011
  - [Bosnia and Herzegovina: Ongoing erosion of the
    State](http://www.osw.waw.pl/en/publikacje/ceweekly/2011-03-30/bosnia-and-herzegovina-ongoing-erosion-state),
    [Centre for Eastern
    Studies](../Page/Centre_for_Eastern_Studies.md "wikilink") 2011

'''

[波士尼亞與赫塞哥維納行政區劃](../Category/波士尼亞與赫塞哥維納行政區劃.md "wikilink")