[Otto_II._(HRR).jpg](https://zh.wikipedia.org/wiki/File:Otto_II._\(HRR\).jpg "fig:Otto_II._(HRR).jpg")
**奥托二世**（**Otto
II**，955年—983年12月7日），[东法兰克国王](../Page/德国君主列表.md "wikilink")（961年—983年在位），[罗马帝国皇帝](../Page/神圣罗马帝国皇帝.md "wikilink")（967年起与父亲共治）。皇帝[奥托一世与伦巴第的阿德莱德之子](../Page/奥托一世_\(神圣罗马帝国\).md "wikilink")。

奥托二世在961年父皇尚在世时即已加冕为国王。972年，他在父亲安排下与[拜占庭帝国皇帝](../Page/拜占庭帝国.md "wikilink")[罗曼努斯二世之女](../Page/罗曼努斯二世.md "wikilink")[狄奥凡诺结婚](../Page/狄奥凡诺.md "wikilink")（狄奥凡诺的身份一直有争议。也有[历史学家认为她是](../Page/历史学家.md "wikilink")[约翰一世皇帝之女](../Page/约翰一世_\(拜占庭\).md "wikilink")）。

奥托二世基本延续其父的政策，抑制[德意志各](../Page/德意志.md "wikilink")[部落公爵的势力](../Page/部落公爵.md "wikilink")。973年奥托二世迫使[波兰大公](../Page/波兰.md "wikilink")[梅什科一世臣服](../Page/梅什科一世.md "wikilink")。977年，他因为[洛林与](../Page/洛林.md "wikilink")[西法兰克国王](../Page/法国君主列表.md "wikilink")[洛泰尔一世发生战争](../Page/洛泰尔一世_\(西法兰克\).md "wikilink")。980年双方缔结协约，规定奥托二世仍保有洛林。981年奥托二世侵入[意大利](../Page/意大利.md "wikilink")。982年，他率領德意志與薩克遜士兵組成的聯軍，在[卡拉布里亚战役中败于拜占庭](../Page/卡拉布里亚战役.md "wikilink")—阿拉伯联军，此役奧托二世喪失四千名士兵逃回義大利北部的[維羅納](../Page/維羅納.md "wikilink")。\[1\]983年，奥托二世选定[约翰十四世当](../Page/约翰十四世.md "wikilink")[教皇](../Page/教皇.md "wikilink")。

## 奥托二世的家庭

  - 妻子：[狄奥凡诺](../Page/狄奥凡诺.md "wikilink")
  - 子女：

<!-- end list -->

1.  [奥托三世](../Page/奥托三世_\(神圣罗马帝国\).md "wikilink")
2.  玛蒂尔达（萨克森的）

## 參考

[O](../Category/神聖羅馬皇帝.md "wikilink")
[东法兰克国王](../Category/德意志国王.md "wikilink")
[O](../Category/955年出生.md "wikilink")
[O](../Category/983年逝世.md "wikilink")

1.