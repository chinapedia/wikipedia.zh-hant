**教下**，漢傳佛教術語，在宋明時代，意指[天台](../Page/天台宗.md "wikilink")、[法相](../Page/法相宗.md "wikilink")、[賢首三個宗派](../Page/華嚴宗.md "wikilink")，又稱**教下三家**。而新興的禪宗，則自認為是教外別傳，自稱[宗門](../Page/宗門.md "wikilink")。

至近代，[佛教](../Page/佛教.md "wikilink")[净土宗則自稱為教下](../Page/净土宗.md "wikilink")。

## 分类

漢傳十大宗派的說法，始於清末[楊仁山居士整理日僧](../Page/楊仁山.md "wikilink")[凝然](../Page/凝然.md "wikilink")(1240-1321)《八宗綱要鈔》\[1\]。

  - [大乘](../Page/大乘.md "wikilink")

<!-- end list -->

  - [三論宗](../Page/三論宗.md "wikilink")——始於[鳩摩羅什譯出](../Page/鳩摩羅什.md "wikilink")[龍樹](../Page/龍樹.md "wikilink")《中論》、《十二門論》與[提婆的](../Page/提婆.md "wikilink")《百論》，奠基于[南北朝](../Page/南北朝.md "wikilink")[僧肇](../Page/僧肇.md "wikilink")、興皇朗、[僧朗法師](../Page/僧朗法師.md "wikilink")、茅山明，大成於[吉藏大師](../Page/吉藏.md "wikilink")。吉藏大師後，此派被天台宗、禪宗所融攝，沒有進一步的發展。
  - [涅槃宗](../Page/涅槃宗.md "wikilink")——始於[晉](../Page/晉.md "wikilink")[竺法護譯](../Page/竺法護.md "wikilink")《[方等泥洹經](../Page/方等泥洹經.md "wikilink")》二卷，[法顯與佛陀跋陀譯出六卷本](../Page/法顯.md "wikilink")《大般泥洹經》，至曇無讖於北涼譯出《[大般涅槃經](../Page/大般涅槃經.md "wikilink")》時開始盛行，分南北二派，北方以隨曇無讖譯經的道朗、慧嵩為主，南方則是傳自竺道生一系。[隋代極盛](../Page/隋代.md "wikilink")，[唐之後并入三論宗](../Page/唐朝.md "wikilink")。
  - [地論宗](../Page/地論宗.md "wikilink")——始於北魏菩提流支及勒那摩提譯出[世親所著](../Page/世親.md "wikilink")《十地經論》，屬[唯識學派](../Page/瑜伽行唯識學派.md "wikilink")。後併入[華嚴宗](../Page/華嚴宗.md "wikilink")。
  - [攝論宗](../Page/攝論宗.md "wikilink")——被[法相宗取代](../Page/法相宗.md "wikilink")，始於[南北朝梁](../Page/南北朝.md "wikilink")、陳之際，在南方的[真諦譯出](../Page/真諦.md "wikilink")[無著](../Page/無著.md "wikilink")《攝大乘論》，與[世親之](../Page/世親.md "wikilink")《攝大乘釋論》，與地論宗同屬[唯識學派](../Page/瑜伽行唯識學派.md "wikilink")。在攝論宗出現之後，逐漸吸收了北方的地論宗，南方的地論師也慢慢式微。
  - [天台宗](../Page/天台宗.md "wikilink")（法華宗）——[南北朝](../Page/南北朝.md "wikilink")[智者大師奠基](../Page/智者大師.md "wikilink")。
  - [淨土宗](../Page/淨土宗.md "wikilink")——初祖東晉[慧遠大師](../Page/慧遠.md "wikilink")。
  - [律宗](../Page/律宗.md "wikilink")——始於[南北朝](../Page/南北朝.md "wikilink")[法顯](../Page/法顯.md "wikilink")、[慧光大師](../Page/慧光大師.md "wikilink")，殿基於唐終南[道宣律師](../Page/道宣.md "wikilink")。
  - [禪宗](../Page/禪宗.md "wikilink")——咸認[南北朝](../Page/南北朝.md "wikilink")[達摩祖師東渡](../Page/菩提达摩.md "wikilink")[中國起始](../Page/古中國.md "wikilink")，大盛於[唐六祖](../Page/唐.md "wikilink")[惠能大師](../Page/惠能.md "wikilink")。
  - [法相唯識宗](../Page/法相唯識宗.md "wikilink")（慈恩宗）——[唐](../Page/唐.md "wikilink")[玄奘法師起始](../Page/玄奘法師.md "wikilink")。
  - [華嚴宗](../Page/華嚴宗.md "wikilink")（賢首宗）——[唐](../Page/唐.md "wikilink")[杜順法師起始](../Page/杜順.md "wikilink")，但真正立派於[賢首法藏法師](../Page/賢首法藏.md "wikilink")。
  - [密宗](../Page/密宗.md "wikilink")（真言宗）——[唐玄宗時](../Page/唐玄宗.md "wikilink")，由[善無畏](../Page/善無畏.md "wikilink")、[金剛智與](../Page/金剛智.md "wikilink")[不空三藏先後傳入漢地](../Page/不空.md "wikilink")，號稱開元三大士。

<!-- end list -->

  - [小乘](../Page/小乘.md "wikilink")

<!-- end list -->

  - [俱舍宗](../Page/俱舍宗.md "wikilink")
  - [成實宗](../Page/成實宗.md "wikilink")

## 參考文獻

[Category:佛教術語](../Category/佛教術語.md "wikilink")

1.  《八宗綱要鈔》，“成實宗、俱舍宗、律宗、三論宗、天台宗、法相宗、華嚴宗、真言宗”，補入禪宗、淨土二宗，形成十大宗派。高觀如認為應有十一家(律宗、三論宗、天台宗、淨土宗、禪宗、法相宗、華嚴宗、真言宗、涅盤宗、地論宗、攝論宗)，後因吸收融攝，只剩下八宗。