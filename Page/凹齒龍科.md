**凹齒龍科**（學名：Rhabdodontidae）是群生存於[白堊紀的](../Page/白堊紀.md "wikilink")[草食性](../Page/草食性.md "wikilink")[鳥腳下目](../Page/鳥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。凹齒龍科的外表看似大型、結實的[稜齒龍類](../Page/稜齒龍類.md "wikilink")，並擁有深頭顱骨與頜部。
[Rhabdodontidae.png](https://zh.wikipedia.org/wiki/File:Rhabdodontidae.png "fig:Rhabdodontidae.png")
在2002年，[大衛·威顯穆沛](../Page/大衛·威顯穆沛.md "wikilink")（David B.
Weishampel）等人首次提出凹齒龍科，範圍是：[查摩西斯龍與凹齒龍的最近共同祖先](../Page/查摩西斯龍.md "wikilink")，以及其最近共同祖先的所有後代。在2005年，[保羅·賽里諾](../Page/保羅·賽里諾.md "wikilink")（Paul
Sereno）將凹齒龍科定義為：包含凹齒龍在內，但不包含[沃克氏副櫛龍在內的最大](../Page/副櫛龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")\[1\]。凹齒龍科目前包含：凹齒龍（[模式種](../Page/模式種.md "wikilink")）、查摩西斯龍、可能還有[柵齒龍與](../Page/柵齒龍.md "wikilink")[木他龍](../Page/木他龍.md "wikilink")\[2\]。化石已發現於[歐洲與](../Page/歐洲.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")，生存年代為[白堊紀晚期](../Page/白堊紀.md "wikilink")，約1億到6500萬年前。

## 參考資料

  - [TaxonSearch:
    凹齒龍科](http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=272)
  - <https://web.archive.org/web/20070307004240/http://www.users.qwest.net/~jstweet1/iguanodontia.htm>

[\*](../Category/禽龍類.md "wikilink")

1.  Sereno, P.C. (2005). "Stem Archosauria Version 1.0." *TaxonSearch*.
    Available:
    <http://www.taxonsearch.org/Archive/stem-archosauria-1.0.php> via
    the Internet. Accessed 24 November 2010.
2.  McDonald, A.T., Kirkland, J.I., DeBlieux, D.D., Madsen, S.K., Cavin,
    J., Milner, A.R.C. and Panzarin, L. (2010). "New Basal Iguanodonts
    from the Cedar Mountain Formation of Utah and the Evolution of
    Thumb-Spiked Dinosaurs." *PLoS ONE 5*, **11**: e14075.  PMID
    21124919