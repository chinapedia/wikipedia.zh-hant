**多萝西·利·塞耶斯**（**Dorothy Leigh
Sayers**，）是一位[英國](../Page/英國.md "wikilink")[作家與](../Page/作家.md "wikilink")[翻譯家](../Page/翻譯家.md "wikilink")。最有名的作品是以[彼得·溫西勳爵](../Page/彼得·溫西勳爵.md "wikilink")（Lord
Peter
Wimsey）為主角的一系列[偵探小說](../Page/偵探小說.md "wikilink")，首部作品是1923年的《*Whose
Body?*》。

## 生平

## 作品

  - *Whose Body?* (1923)
  - *Clouds of Witness* (1926)
  - *Unnatural Death* (1927)
  - *The Unpleasantness at the Bellona Club* (1928)
  - *Lord Peter Views the Body* (1928) (12 short stories)
  - *Strong Poison* (1930)
  - *The Five Red Herrings* (1931)
  - *Have His Carcase* (1932)
  - *Hangman's Holiday* (1933) (12 short stories, 4 including Lord
    Peter)
  - *Murder Must Advertise* (1933)
  - *The Nine Tailors* (1934)
  - *Gaudy Night* (1935)
  - *The Silent Passenger* (1935) a film based on the novel
  - *Busman's Honeymoon* (1937)
  - *In the Teeth of the Evidence*

## 外部連結

  -
  - [The Dorothy L. Sayers Society](http://www.sayers.org.uk/)

  - [Dorothy L Sayers in
    Galloway](https://web.archive.org/web/20080417224638/http://www.gatehouse-of-fleet.co.uk/sayers.htm)—the
    [scene](../Page/Dumfries_and_Galloway.md "wikilink") of her novel
    *Five Red Herrings* (1931)

[Category:英格蘭小說家](../Category/英格蘭小說家.md "wikilink")
[Category:英格蘭翻譯家](../Category/英格蘭翻譯家.md "wikilink")
[Category:英格蘭女性作家](../Category/英格蘭女性作家.md "wikilink")
[Category:女性小說家](../Category/女性小說家.md "wikilink")
[Category:女性翻譯家](../Category/女性翻譯家.md "wikilink")
[Category:英格蘭聖公宗教徒](../Category/英格蘭聖公宗教徒.md "wikilink")
[Category:牛津人](../Category/牛津人.md "wikilink")
[Category:牛津大学萨默维尔学院校友](../Category/牛津大学萨默维尔学院校友.md "wikilink")