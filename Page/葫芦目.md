**葫芦目**是一类[有花植物](../Page/被子植物门.md "wikilink")，属于[真双子叶植物](../Page/真双子叶植物.md "wikilink")，本[目植物一般生长在](../Page/目.md "wikilink")[热带](../Page/热带.md "wikilink")、[亚热带的暖](../Page/亚热带.md "wikilink")[温带地区](../Page/温带.md "wikilink")，大部分是[藤本和](../Page/藤本.md "wikilink")[草本植物](../Page/草本.md "wikilink")，也有部分[灌木和小](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")。本目[植物的](../Page/植物.md "wikilink")[花为单性](../Page/花.md "wikilink")，[花瓣为](../Page/花瓣.md "wikilink")5，大部分为虫媒花，有少数为风媒花。

本目植物包括7个[科](../Page/科.md "wikilink")，大约2300[种](../Page/种.md "wikilink")，最大的科是[秋海棠科](../Page/秋海棠科.md "wikilink")，有1400多种，此外就是[葫芦科](../Page/葫芦科.md "wikilink")，有825种。

葫芦目的植物有许多种是重要的[经济作物](../Page/经济作物.md "wikilink")，例如[南瓜](../Page/南瓜.md "wikilink")、[西瓜等瓜类](../Page/西瓜.md "wikilink")（葫芦科），秋海棠科大约有130中是培植的[花卉](../Page/花卉.md "wikilink")。

葫芦目是[APG II
分类法确定的](../Page/APG_II_分类法.md "wikilink")7个科，根据以前的[克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink")，前四科是被分到[堇菜目](../Page/堇菜目.md "wikilink")，其他各科分别属于其他的目。随后的基因分子学研究表明，寄生植物[离花科也属于本目](../Page/离花科.md "wikilink")\[1\]。

## 参考文献

## 外部链接

  - [DOI](http://dx.doi.org/10.1111/j.1095-8339.2003.00281.x)
  - [摘要](http://www.blackwell-synergy.com/links/doi/10.1111/j.1095-8339.2003.00281.x/abstract/)
  - [全文
    (HTML)](http://www.blackwell-synergy.com/links/doi/10.1111/j.1095-8339.2003.00281.x/full/)
  - [全文
    (PDF)](http://www.blackwell-synergy.com/links/doi/10.1111/j.1095-8339.2003.00281.x/pdf))

[\*](../Category/葫芦目.md "wikilink")

1.  Filipowicz, N., and S. S. Renner. 2010. "The worldwide holoparasitic
    Apodanthaceae confidently placed in the Cucurbitales by nuclear and
    mitochondrial gene trees." *BMC Evolutionary Biology* **10**:219.