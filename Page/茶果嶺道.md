[Cha_Kwo_Ling_Road_(Laguna_City_section).JPG](https://zh.wikipedia.org/wiki/File:Cha_Kwo_Ling_Road_\(Laguna_City_section\).JPG "fig:Cha_Kwo_Ling_Road_(Laguna_City_section).JPG")南段\]\]
[Cha_Kwo_Ling_Road_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Cha_Kwo_Ling_Road_\(Hong_Kong\).jpg "fig:Cha_Kwo_Ling_Road_(Hong_Kong).jpg")北段，79號近偉發道，左方建築物為容鳳書紀念中心。\]\]
[Domain_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Domain_\(Hong_Kong\).jpg "fig:Domain_(Hong_Kong).jpg")的一段\]\]
**茶果嶺道**（）是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[觀塘區的一條道路](../Page/觀塘區.md "wikilink")，以途經的[茶果嶺村命名](../Page/茶果嶺村.md "wikilink")。道路東起[鯉魚門的](../Page/鯉魚門.md "wikilink")[崇信街和](../Page/崇信街.md "wikilink")[欣榮街](../Page/欣榮街.md "wikilink")，經過[-{晒}-草灣及](../Page/晒草灣.md "wikilink")[茶果嶺一帶](../Page/茶果嶺.md "wikilink")，西面則連接[麗港城的](../Page/麗港城.md "wikilink")[鯉魚門道及](../Page/鯉魚門道.md "wikilink")[偉業街等](../Page/偉業街.md "wikilink")。

## 途經地點

  - [麗港城](../Page/麗港城.md "wikilink")
  - [麗港城商場](../Page/麗港城商場.md "wikilink")
  - [麗港城公共運輸交匯處](../Page/麗港城公共運輸交匯處.md "wikilink")
  - [城中薈](../Page/城中薈.md "wikilink")
  - 麗港中心
  - [-{晒}-草灣](../Page/晒草灣.md "wikilink")
  - [茶果嶺村](../Page/茶果嶺村.md "wikilink")
  - [東區海底隧道](../Page/東區海底隧道.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[油塘站](../Page/油塘站.md "wikilink")
  - [大本型](../Page/大本型.md "wikilink")
  - [油塘中心](../Page/油塘中心.md "wikilink")
  - [三家村遊樂場](../Page/三家村遊樂場.md "wikilink")
  - [茶果嶺公眾貨物裝卸區](../Page/茶果嶺公眾貨物裝卸區.md "wikilink")

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小型巴士.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參見

  - [茶果嶺](../Page/茶果嶺.md "wikilink")
  - [-{晒}-草灣](../Page/晒草灣.md "wikilink")

## 參考資料

  - [茶果嶺道以村落命名](http://www.am730.com.hk/article.php?article=46662)
    2011年3月2日 AM730

[Category:觀塘區街道](../Category/觀塘區街道.md "wikilink")
[Category:晒草灣](../Category/晒草灣.md "wikilink")
[Category:油塘](../Category/油塘.md "wikilink")