**菸草鑲嵌病毒**（**Tobacco mosaic virus**；**TMV**），又譯為**-{zh-hant:菸草花葉病毒;
zh-hans:菸草镶嵌病毒}-**，是一種[RNA病毒](../Page/RNA病毒.md "wikilink")，專門感染[植物](../Page/植物.md "wikilink")，尤其是[菸草及其他](../Page/菸草.md "wikilink")[茄科植物](../Page/茄科.md "wikilink")，能使這些受感染的[葉片看來斑駁污損](../Page/葉片.md "wikilink")，因此得名（mosaic為[馬賽克](../Page/鑲嵌畫.md "wikilink")，也就是拼貼之意）。19世紀末期人們已知有某種威脅菸草作物生存的疾病，但直到1930年才確知此病毒的存在。
[TMV_virus_super_resolution_microscopy_Christoph_Cremer_Christina_Wege.jpg](https://zh.wikipedia.org/wiki/File:TMV_virus_super_resolution_microscopy_Christoph_Cremer_Christina_Wege.jpg "fig:TMV_virus_super_resolution_microscopy_Christoph_Cremer_Christina_Wege.jpg")

## 延伸閱讀

  - Creager, Angela N. *The Life of a Virus: Tobacco Mosaic Virus as an
    Experimental Model, 1930-1965.* Chicago, IL: University of Chicago
    Press, 2002.

## 外部連結

  - [Description of plant viruses -
    TMV](http://www.dpvweb.net/dpv/showdpv.php?dpvno=370) - contains
    information on symptoms, hosts species, purification etc.
  - [Further
    information](https://web.archive.org/web/20120614075040/http://www.extension.umn.edu/distribution/horticulture/dg1168.html)
  - [Electron microscope image of
    TM](http://www.ncbi.nlm.nih.gov/ICTVdb/Images/Milne/tobamo1.htm)

[Category:煙草](../Category/煙草.md "wikilink")
[Category:病毒](../Category/病毒.md "wikilink")
[Category:模式生物](../Category/模式生物.md "wikilink")
[Category:植物病理學](../Category/植物病理學.md "wikilink")