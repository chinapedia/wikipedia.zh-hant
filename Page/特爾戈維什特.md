**特爾戈維什特**（，意為「市場」）是[保加利亞的一個城市](../Page/保加利亞.md "wikilink")，人口四萬六千人，海拔170公尺，也是[特爾戈維什特州的首府](../Page/特爾戈維什特州.md "wikilink")。它位於[索菲亞東北方](../Page/索菲亞.md "wikilink")339公里，[舒門西方](../Page/舒門.md "wikilink")41公里，[大普雷斯拉夫西北方](../Page/大普雷斯拉夫.md "wikilink")25公里，[奧莫爾塔格東北方](../Page/奧莫爾塔格.md "wikilink")24公里，[大特爾諾沃東北方](../Page/大特爾諾沃.md "wikilink")100公里，[拉茲格勒南方](../Page/拉茲格勒.md "wikilink")36公里，[波波沃東南方](../Page/波波沃.md "wikilink")35公里。它是一個古老的[市集](../Page/市集.md "wikilink")[聚落](../Page/聚落.md "wikilink")。

[十八](../Page/十八世紀.md "wikilink")、[十九世紀特爾戈維什特成為一個著名的動物和](../Page/十九世紀.md "wikilink")[手工藝品的](../Page/手工藝.md "wikilink")[交易](../Page/交易.md "wikilink")[市場](../Page/市場.md "wikilink")，當時名為Eski-Djumaia。[二戰以後](../Page/第二次世界大戰.md "wikilink")[工業開始發展](../Page/工業.md "wikilink")，先有[汽車電池及](../Page/鉛電池.md "wikilink")[食品工業用機械的生產](../Page/食品工業.md "wikilink")，接著還發展了[家具工業及](../Page/家具.md "wikilink")[紡織工業](../Page/紡織工業.md "wikilink")。[保加利亞最大的](../Page/保加利亞.md "wikilink")[葡萄酒工廠之一位在此地](../Page/葡萄酒.md "wikilink")。這裡也是一個文化重鎮。2000年時，[古羅馬市鎮Missionis的遺址在特爾戈維什特附近發現](../Page/古羅馬.md "wikilink")。

## 外部連結

  - <https://web.archive.org/web/20060821224921/http://www.targovishte.bg/>
    - 特爾戈維什特市的官方網站
  - <http://www.tshte.net> - 特爾戈維什特的第一家網路服務供應商
  - <https://web.archive.org/web/20160304171213/http://creative.linia.net/>
    - 特爾戈維什特風情畫，由當地的一間攝影暨網站設計公司製作

[T](../Category/特尔戈维什特州城市.md "wikilink")