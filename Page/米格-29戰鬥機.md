**米格-29**（），[蘇聯](../Page/蘇聯.md "wikilink")[米高揚-古列維奇飛機設計局於](../Page/俄羅斯米格航空器集團.md "wikilink")1970年代設計的雙發高性能制空[戰鬥機](../Page/戰鬥機.md "wikilink")，同時期開發之戰機亦有[Su-27](../Page/苏-27战斗机.md "wikilink")，目的為對抗美軍之同期先進戰機[F-15](../Page/F-15.md "wikilink")。[北約組織給予的綽號是](../Page/北約組織.md "wikilink")「支點」（）。

## 發展

和[苏-27一样](../Page/苏-27战斗机.md "wikilink")，米格-29的历史始于1969年，[苏联获知](../Page/苏联.md "wikilink")[美国空军正在进行](../Page/美国空军.md "wikilink")“FX”计划（即最终形成[F-15的计划](../Page/F-15.md "wikilink")）时。苏联领导人意识到，新的美军飞机将会对苏联现有的所有战斗机都形成巨大的技术优势。[米格-21算是當時机动性很高的飞机](../Page/米格-21.md "wikilink")，然而在航程，武裝與升級潛能上有相當多的缺點。以對抗美國[F-4為研發重點的](../Page/F-4.md "wikilink")[米格-23飛行速度較高](../Page/米格-23戰鬥機.md "wikilink")，也有較多的攜帶燃料與裝備的空間，可是欠缺[纏鬥中需要的運動性](../Page/纏鬥.md "wikilink")。蘇聯欠缺的是一款在各方面性能都相當均衡的戰鬥機，具有優異的運動性和高性能的航電系統。對此，俄國參謀本部發出“先進戰術戰鬥機”（Перспективний
Фронтовой
Истребитель，ПФИ）的需求案，其中諸項性能要求相當高，包括高航程，優異的短場起降能力（包括使用簡易機場的能力），高敏捷性，超過兩[馬赫極速和重武裝](../Page/馬赫.md "wikilink")。新飛機的空氣動力設計交由[蘇聯空氣動力研究所](../Page/蘇聯空氣動力研究所.md "wikilink")（TsAGI）負責，成果與[蘇霍伊飛機公司一同分享](../Page/蘇霍伊.md "wikilink")。

[MiG-29_and_F-16.jpg](https://zh.wikipedia.org/wiki/File:MiG-29_and_F-16.jpg "fig:MiG-29_and_F-16.jpg")的米格-29與美國空軍的F-16在天空飛翔\]\]
然而，蘇聯認為先進戰術戰鬥機的價格會太昂貴，生產數量將無法滿足需求，於是在1971年將這個計畫拆成兩種，即重型先進戰術戰鬥機（Тяжелый
Перспективний Фронтовой Истребитель, TПФИ）和輕型先進戰術戰鬥機（Легкый
Перспективний Фронтовой Истребитель,
ЛПФИ）。在同時期，美國空軍也進行類似的規劃，推出輕型戰鬥機（Lightweight
Fighter）的計畫和[F-16戰隼以及](../Page/F-16.md "wikilink")[YF-17](../Page/YF-17.md "wikilink")。重型戰機的計畫依舊由蘇霍伊負責，輕型戰機的計畫則交由米高揚飛機設計局，[苏-27側衛即是前者的成果](../Page/苏-27战斗机.md "wikilink")。後者在1974年提出9號（Product
9）計畫案，也就是米格-29A，並在1977年10月6日首飛。[美國](../Page/美國.md "wikilink")[偵查衛星在同年](../Page/間諜衛星.md "wikilink")11月發現原型機，由於原型機是在[拉明斯基鎮附近進的朱科夫斯基](../Page/拉明斯基鎮.md "wikilink")（Жуковский）試飛場被發現，於是給予白羊座-L（Ram-L）的代號。早期西方推測白羊座-L的外型類似YF-17，並配備具有[後燃器的](../Page/後燃器.md "wikilink")[渦輪噴射發動機](../Page/渦輪噴射發動機.md "wikilink")。

[Iran_Air_Force_Mikoyan-Gurevich_MiG-29UB_Sharifi.jpg](https://zh.wikipedia.org/wiki/File:Iran_Air_Force_Mikoyan-Gurevich_MiG-29UB_Sharifi.jpg "fig:Iran_Air_Force_Mikoyan-Gurevich_MiG-29UB_Sharifi.jpg")

儘管兩架原型機的墜毀造成了計畫延遲，1983年，米格-29B量產機仍然在庫賓卡基地開始了操作評估。該項評估於隔年完成，並於同年開始交機。由於[北約未能知悉其預量產型米格](../Page/北約.md "wikilink")-29A的存在，故該機被北約命名為"支點A"。降級改裝後的米格-29B被以"米格-29B
9-12A"的型號大量出口給[華約國家](../Page/華約.md "wikilink")（出口非華約國家者則編號為"米格-29B
9-12B"）。這些飛機，一共生產了840架，相較於蘇聯版本米格-29B，它們的功能多有降低且不具備核武投射能力。1986年7月，蘇聯以一隊米格-29訪問[芬蘭](../Page/芬蘭.md "wikilink")。直到這時，米格-29才正式公諸於西方大眾。1988年9月，[蘇聯曾以兩架米格](../Page/蘇聯.md "wikilink")-29參加[英國法茵堡航空展](../Page/英國.md "wikilink")。一總說來，西方觀察家對其機動性能印象深刻，但發動機排煙過於明顯則是其重大缺點。

米格-29的海外用戶包括了[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")、[孟加拉](../Page/孟加拉.md "wikilink")、[保加利亞](../Page/保加利亞.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[捷克、斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")、[東德](../Page/東德.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[印度](../Page/印度.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[朝鮮](../Page/朝鮮.md "wikilink")、[祕魯](../Page/祕魯.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[塞爾維亞](../Page/塞爾維亞.md "wikilink")、[斯洛伐克](../Page/斯洛伐克.md "wikilink")、[敘利亞與](../Page/敘利亞.md "wikilink")[葉門](../Page/葉門.md "wikilink")。另外，蘇聯解體時，[白俄羅斯](../Page/白俄羅斯.md "wikilink")、[哈薩克](../Page/哈薩克.md "wikilink")、[塞爾維亞](../Page/摩達維亞.md "wikilink")、[土庫曼](../Page/土庫曼.md "wikilink")、[烏克蘭](../Page/烏克蘭.md "wikilink")、[烏茲別克等前加盟共和國亦自前蘇聯空軍分得了大批數量](../Page/烏茲別克.md "wikilink")。這些飛機狀況不一，其中許多至今仍在其新用戶的手中服役。

蘇聯精確的區分各種版本的米格-29，即使只有電子設備的改良，然而米高楊推出的多種米格-29，例如[艦載機機種米格](../Page/艦載機.md "wikilink")-29K，都不曾進入量產階段。在前蘇聯時期，由於米高楊設計局相對於競爭對手蘇霍伊，明顯的缺乏政治影響力，造成在開發米格-29過程中，遭遇了許多挫折。許多更先進的型號仍在爭取出口許可，而升級蘇聯舊型的米格-29訂單也還在爭取中。新型機種如米格-29SMT和米格-29M1/M2的開發也才起步。艦載機種米格-29K的發展因為[印度海軍航空母艦](../Page/印度海軍.md "wikilink")[維克拉姆帝亞號](../Page/维兰玛迪雅号航空母舰.md "wikilink")（前蘇聯海軍[戈尔什科夫海军上将号航空母舰](../Page/巴库号航空母舰.md "wikilink")）的需求得以恢復。原本米格-29K是設計供給[库兹涅佐夫号航空母舰使用](../Page/库兹涅佐夫号航空母舰.md "wikilink")，但後來卻使用較大型的[苏-33](../Page/苏-33.md "wikilink")。

雖然非正式的名稱十分普及，但蘇聯官方一如其他的军用飞机一样，並沒有給予米格-29一個正式的名稱。[北約称米格为](../Page/北約.md "wikilink")[支點](../Page/支點.md "wikilink")。2012年2月俄國防部與“米格”飛機制造公司簽訂合同，采購24架米格-29K/КУБ型艦載機。這是俄羅斯本身采購米格-29的最新訂單。據估計，國防部采購的這批飛機將用於“庫茲涅佐夫”號航母。新的艦載米格-29К/КУБ將逐步替代老化的蘇-33戰鬥機。2009年9月俄國防部透露，為印度海軍航空母艦生產的首批4架米格-29K艦載機將在10月份運抵印度。印度共訂購了16架俄產米格-29艦載戰鬥機，其中包括12架單座米格-29K和4架雙座米格-29KUB教練機。這是米格-29最為重要的出口訂單。俄羅斯原計劃在2008年完成印度航母的改裝工作，但由於技術問題，收尾工作被推遲到2013年。在此之前這些艦載機將部署在地面。俄羅斯國防部消息人士表示，戰機預計在10月中旬運抵印度，並將在印度組裝和試飛，印度軍方將在隨後接收這批戰機。\[1\]

2016年[敘利亞戰爭中俄羅斯庫茲涅佐夫航母前往出戰](../Page/俄羅斯在敘利亞內戰的軍事介入.md "wikilink")，新型的米格-29K首次投入實戰進行地面轟炸任務，大幅消滅各種地面敵軍，戰爭中一架米格-29K著艦失敗墜毀但飛行員跳傘成功。\[2\]

## 特點

### 設計

[MiG-29ASlovakia.jpg](https://zh.wikipedia.org/wiki/File:MiG-29ASlovakia.jpg "fig:MiG-29ASlovakia.jpg")

由於設計上的基本參數是來自於TsAGI為PFI計畫所進行的運算，米格-29的氣動力外型設計與[苏-27非常相似](../Page/苏-27战斗机.md "wikilink")，其中較為明顯的差異包括：主要結構以[鋁為主](../Page/鋁.md "wikilink")，加上一些[複合材料](../Page/複合材料.md "wikilink")，機翼是後掠中單翼加上融合成一體的[翼前緣延伸面](../Page/翼前緣延伸面.md "wikilink")（leading-edge
root
extensions，LERSx），後掠角為40度。機身後方位於發動機位側的是有後掠角度的水平尾翼與雙垂直安定面。翼前緣自動縫翼在早期型上分長4個部分，後期型改為5個，翼後緣則有襟翼和副翼。

米格-29採用[液壓控制與SAU](../Page/液壓.md "wikilink")-451三軸[自動飛行儀](../Page/自動飛行儀.md "wikilink")，並不像苏-27使用[線傳飛控系統](../Page/線傳飛控.md "wikilink")，儘管如此，米格-29仍是一架非常靈活的飛機，無論是瞬時或者是持續迴轉性能均接近西方戰機的平均水準，在400節以上速度時擁有和F-16相近的盤旋速度，能量損失率僅為F-16的四倍（60%和240%），良好的高[攻角性能而且不容易進入水平螺旋](../Page/攻角.md "wikilink")。機身結構足以承受9G（88E公尺/秒²）的運動。控制桿有一個可以手動取消的限制，以免飛行員超過g或是攻角限制。

### 引擎

[MiG29-OVT-ENGINE.JPG](https://zh.wikipedia.org/wiki/File:MiG29-OVT-ENGINE.JPG "fig:MiG29-OVT-ENGINE.JPG")的米格-29OVT，可以看到2個矢量噴射口\]\]
米格-29有兩具分開設置的[克里莫夫](../Page/克里莫夫.md "wikilink")[RD-33渦輪扇發動機](../Page/RD-33渦輪扇發動機.md "wikilink")，每具提供50.0千牛頓的推力，啟動[後燃器時可達](../Page/後燃器.md "wikilink")81.3千牛頓。兩具引擎中間的空間，能提供額外昇力，並降低[翼負荷](../Page/翼負荷.md "wikilink")，有效增加機動性。引擎的楔形[進氣口位在翼根前緣延伸板的下方](../Page/進氣口.md "wikilink")，並且有可調整的進氣門，以供高[馬赫飛行的進氣需求](../Page/馬赫.md "wikilink")。在野戰跑道起飛、降落或低速飛行時，進氣門可以幾乎完全關閉，防止地面異物吸入引擎。在這種情況下，翼根前緣延伸板上方的活動進氣口會自動開啟，引擎能從此獲得所需空氣。後續的型號使用類似裝置在[苏-27進氣口的網狀柵欄](../Page/苏-27战斗机.md "wikilink")，取代原來機背進氣口的功能。

### 航程及燃油

[MiG-29_refuelling.jpg](https://zh.wikipedia.org/wiki/File:MiG-29_refuelling.jpg "fig:MiG-29_refuelling.jpg")運輸機上接收油料到翼下油箱\]\]
初期型米格-29B的內部油箱總容量只有4,365[公升](../Page/公升.md "wikilink")。總數六個的油箱分別在兩翼各一個，另外四個則在機身。燃料的因素造成米格-29B的航程有限，然而已經符合蘇聯的原始需求：一架點防禦的前線戰鬥機。在進行長途飛行時，機腹中線能攜帶一具1,500公升的[副油箱後續的量產型也能在兩翼下各攜帶一個](../Page/副油箱.md "wikilink")1,150公升的副油箱。此外，少數的米格-29在機身左側加裝[空中加油設備](../Page/空中加油.md "wikilink")，使用探針和錐管的方式進行空中加油來提升滯空時間。

一些米格-29B（MiG-29
9-13）修改機背的設計（Fatback），增加內部的燃油攜帶量。更先進的後續型號如[米格-35](../Page/米格-35.md "wikilink")，能在機背攜帶適形油箱。

### 駕駛艙

本機配備了[彈射椅](../Page/彈射椅.md "wikilink")，曾在諸多緊急狀況中發揮過令人印象深刻的表現。

座艙中配備有[抬頭顯示器以及ZSh](../Page/抬頭顯示器.md "wikilink")-3UM頭盔瞄準具。然而，其並未配備西方戰鬥機廣泛採用的。特別要強調的是；為了方便飛行員進行機種轉換，米格-29的駕駛艙並沒有大量採用[人體工學設計](../Page/人體工學.md "wikilink")，而是使其儘可能使其類似於之前的[米格-23](../Page/米格-23.md "wikilink")。不過，由於採用了泡型艙罩，其駕駛艙視野較過去蘇式軍機多有改善，但仍然不及同時期的西方戰鬥機。後期的改良型裝置有[多功能顯示器的](../Page/液晶顯示器.md "wikilink")[玻璃座艙](../Page/玻璃驾驶舱.md "wikilink")，並且改換了真正的。

### 感應器

米格-29B配備了法左特龍RLPK-29火控系統，包括了一具支援俯視/俯射的N-019脈衝[多普勒雷達](../Page/多普勒雷達.md "wikilink")（北約命名：Slot
Back）與Ts100.02-02火控電腦。早期的N-019A雷達號稱可使米格-29與西方對手並駕齊驅，但對於蘇聯空軍對它的經驗是令人失望的。在視距外接戰戰鬥機大小的目標時，其所提供的追蹤距離只有：對頭70公里/尾追35公里。雖可同時追蹤10個目標，但只能半主動雷達導引飛彈攻擊其中的一個。在俯視/俯射時，隨著距離與地面回波的增加，期訊號衰減亦十分嚴重，對抗電子干擾的能力也不好。這些問題，代表了這架飛機無法以[白楊飛彈可靠的接戰視距外目標](../Page/R-27.md "wikilink")。

之後，[CIA經由](../Page/CIA.md "wikilink")[間諜獲得N](../Page/間諜.md "wikilink")-019雷達的相關資訊。為了改善這些缺陷，法左特龍推出了N-019M*Topaz*雷達並配備於米格-29S上。然而，其性能仍不能使蘇軍滿意。在最後一種改良型中，米格-29換裝了有[被動相位陣列天線的N](../Page/電子掃描陣列雷達.md "wikilink")-010*[甲蟲-M](../Page/甲蟲-M.md "wikilink")*雷達，在探測距離、解析度上獲得大幅改善，並支援以[RVV-AE](../Page/RVV-AE.md "wikilink")（北約編號：AA-12"毒蛇"）飛彈進行多目標接戰。此外，米格-29在風擋前配備了與蘇-27相同的S-31E2光電探測器，可從動於雷達，亦可獨立運作並提供額外的槍砲瞄準協助。

### 武裝

米格-29的固定武裝為一具[GSh-30-1](../Page/GSh-30-1機炮.md "wikilink")
30毫米[機炮](../Page/機炮.md "wikilink")，射口在左側翼根。原始設計攜彈量為150枚，但在後續型號被減到100枚。由於初期量產型的米格-29B在機腹中線攜帶副油箱時，會擋住拋殼口，因此攜帶中線副油箱時無法使用[機砲](../Page/機砲.md "wikilink")。這個問題直到米格-29S之後的型號才被解決。在每個機翼下，依據不同的型號有三個或四個掛點，兩邊一共有六個或八個掛點。內側掛點能攜帶1,150公升（300加崙）的副油箱、或[R-27中程](../Page/R-27飛彈.md "wikilink")[空對空导彈](../Page/空對空导彈.md "wikilink")，也可攜帶傳統炸彈或火箭，而某些型號的米格-29能在內側掛點攜帶核彈。外側掛點通常攜帶[R-73纏鬥导彈](../Page/Vympel_R-73.md "wikilink")，但某些情況下也會配備較老舊的[R-60短程空對空导彈](../Page/R-60飛彈.md "wikilink")。在兩側發動機中間的機腹中線掛點能攜帶一個1,500公升（400加崙）的油箱，但是無法攜帶戰鬥用武器。原始的米格-29B能在機腹攜帶通用炸彈或火箭吊舱，但是無法配備精確制导武器。直到後繼的改良型才提供攜帶激光或光電制导武器的能力，此時的米格-29才有能力使用[空對地导彈](../Page/空對地导彈.md "wikilink")。

米格-29能攜帶导彈、傳統炸彈、火箭和副油箱共4500公升。

<File:MiG-29> cockpit 3.jpg|類比式駕駛艙
[File:Cokpit.MiG-29M.jpg|數位式駕駛艙](File:Cokpit.MiG-29M.jpg%7C數位式駕駛艙)
<File:MiG-29> gun.jpg|MiG-29UB機槍口 <File:German> MIG-29
Nose.jpg|MiG-29機鼻和紅外線追蹤器 <File:Klimov> RD-33 turbofan
engine.JPG|RD-33引擎

## 操作历史

[Mig_29_firing_AA-10.JPG](https://zh.wikipedia.org/wiki/File:Mig_29_firing_AA-10.JPG "fig:Mig_29_firing_AA-10.JPG")
苏联将米格-29出售到多个国家，由于训练等多方面的原因，其作战的历史表现不一。\[3\]

[MiG-29_wreck.jpg](https://zh.wikipedia.org/wiki/File:MiG-29_wreck.jpg "fig:MiG-29_wreck.jpg")
塞爾維亞的米格29已經是15年以上的老機而且因為長期禁運沒有維護零件。一度投入戰爭，但是許多系統故障影響了飛機性能。6架被擊落（拼裝自14架 +
2架教練機），1架戰鬥受損（後來放在地面當欺敵假機時被炸燬），外加3架在地面被炸燬（共10架）。至少有超過一架式意外墜毀，飛行員於墜落中彈射成功。本戰爭中共兩名[塞爾維亞機員死亡](../Page/塞爾維亞.md "wikilink")。\[4\]本機還有用在[南斯拉夫內戰](../Page/南斯拉夫內戰.md "wikilink")，多半是對地攻擊任務。在[敘利亞方面](../Page/敘利亞.md "wikilink")，支點負責全方位巡邏[敘利亞和](../Page/敘利亞.md "wikilink")[黎巴嫩](../Page/黎巴嫩.md "wikilink")。敘利亞飛行員對於它的機動性和武器系統很滿意。所有敘利亞飛行員都飛米格機也是全球空軍中最有經驗的米格專家。他們的訓練是非常嚴格且繁重，並且加強攻擊性訓練。
[IAFMiG.JPG](https://zh.wikipedia.org/wiki/File:IAFMiG.JPG "fig:IAFMiG.JPG")
[缩略图](https://zh.wikipedia.org/wiki/File:DPRK_MiG-29.jpg "fig:缩略图")MiG-29\]\]
印度曾在[喀什米爾](../Page/喀什米爾.md "wikilink")[卡吉爾邊境衝突出動米格](../Page/卡吉爾邊境衝突.md "wikilink")-29。掩護[幻象2000投下](../Page/幻象2000.md "wikilink")[雷射導引炸彈並維持空優](../Page/雷射導引.md "wikilink")。該分隊Indian
Fulkrums是著名的「北印之鷹」。他們做了許多升級和引擎現代化修改。印度空軍改進並成功測試Mig-29發射[R-77飛彈](../Page/AA-12飛彈.md "wikilink")。可以確信所有印度米格-29都做了修改升級發射可R-77。同時也可以相容印度自製[Astra飛彈](../Page/Astra_Missile.md "wikilink")。

總之除了射下無名的商用機，所有MiG-29八次空對空擊落都是擊落別的MiG系列飛機，其中兩架是遭到友軍攻擊。

### 蘇聯和俄羅斯

[MiG-29_at_Farnborough.jpg](https://zh.wikipedia.org/wiki/File:MiG-29_at_Farnborough.jpg "fig:MiG-29_at_Farnborough.jpg")\]\]
[MiG-29SMT_on_the_MAKS-2009_(01).jpg](https://zh.wikipedia.org/wiki/File:MiG-29SMT_on_the_MAKS-2009_\(01\).jpg "fig:MiG-29SMT_on_the_MAKS-2009_(01).jpg")
苏联/俄罗斯作为米格-29的原生产国，也是装备米格-29数量最多的国家，在苏联解体后俄罗斯初期，俄空军只掌握了原有的米格-29部队中的37%，随着经济的一路滑坡，俄空军中的米格-29数量更是下降到300架以下。

到了1993年底，约100架生产中的米格-29因空军无法支付费用存放在仓库里。其中48架后来用于作为改进型原型机。但随着普京上台之后俄罗斯经济复苏，俄罗斯空军开始下达米格-29的新订单及原米格-29改良维护订单，截至2010年俄罗斯空军在役入列的米格-29约230架，库存220余架，2012年2月29日俄罗斯米格飞机制造公司签订合同，采购24架米格-29K/UBК型舰载机。根据协议，俄罗斯国防部将在2012-2015年间接收20架米格-29K和4架米格-29UBK战斗机；

[阿富汗战争期间时常有阿政府飞行员驾机叛逃的事](../Page/苏联－阿富汗战争.md "wikilink")。有报道称1987年8月苏联的米格-29击落了四架试图攻击总统府的苏-22。\[5\]\[6\]2008年4月，格鲁吉亚政府宣布一架俄罗斯米格-29击落了一架格鲁吉亚无人机并出示了无人机拍到的米格-29发射空對空导弹录像。俄罗斯予以否认。[阿布哈兹政府则说是他们的L](../Page/阿布哈兹.md "wikilink")-39做的，因为该机正在侵犯领空。\[7\]

另外，米格-29亦為俄羅斯國家級的[雨燕飛行表演隊的指定操作機型](../Page/雨燕飛行表演隊.md "wikilink")。

### 德國的米格-29

80年代，[東德空軍接收了一批為數不多的米格](../Page/東德.md "wikilink")-29G（東德特有型號）。1990年，[德國統一](../Page/德國統一.md "wikilink")。由於機齡還很新，性能也還很好，所以這批米格-29成了少數為[德國聯邦國防軍留用的前蘇聯裝備](../Page/德國聯邦國防軍.md "wikilink")。這批飛機都被置於JG-73[史坦霍夫聯隊麾下](../Page/約翰諾斯·史坦霍夫.md "wikilink")，主要用於作戰訓練，並不擔負主要作戰任務。這批飛機在[2003年9月自](../Page/2003年9月.md "wikilink")[德國空軍除役](../Page/德國空軍.md "wikilink")，除一架留做展示外，剩餘的22架米格-29全數以1[歐元的象徵性代價售予](../Page/歐元.md "wikilink")[波蘭](../Page/波蘭.md "wikilink")。

[美國科學家聯盟](../Page/美國科學家聯盟.md "wikilink")（FAS）認為在近距离格斗狀況下，米格-29可能擁有比F-15C相当甚至更好的战斗力，传说這個看法在一场對抗演習中被證實，米格-29展現某種领域的優勢。在一場意大利的視距內（within-visual-range，WVR）的空中演習中，[德國空軍的米格](../Page/德國空軍.md "wikilink")-29對抗一批F-16機群，但并未如一些传闻一样对F-16有压倒优势，只是让美国飞行员发现它的低速机动性很出色，很像F-18。如果说MIG-29占有两百节以下的速度，F-16则称雄了两百节以上的速度\[8\]。另外一大优势是米格-29的頭盔瞄準系統「射手（Archer）」，射手系統讓德國空軍的駕駛員能看到哪、鎖定到哪，其前方攻击区是一个45度的圆锥角。相對而言，美國空軍的戰鬥機必須將機頭指向目標，才能鎖定敵方，后来在F-15C
MISP2和F-16C/D上同样引进了头盔瞄准系统和AIM-9X格斗弹后便使得美国空军重新获得压倒性的优势。美国在91年检查了德国的MIG29后发现其雷达采用落后的卡塞格伦天线等于没有下视能力，告警器的性能也欠妥，另外全机的可靠性也较差，在当时德国空军拥有的23架MIG-29中只有6架有随时出动能力。

### 古巴

1996年，一架[古巴的MiG](../Page/古巴.md "wikilink")-29擊落[救援兄弟会所擁有的民用](../Page/救援兄弟会.md "wikilink")[Cessna
337飛機](../Page/Cessna_337.md "wikilink")。

### 伊拉克

[海湾战争中](../Page/海湾战争.md "wikilink")，5架伊拉克米格-29被联军击落。\[9\]一些俄国报道声称该机至少击落了一架[龍捲風戰鬥轟炸機](../Page/龍捲風戰鬥轟炸機.md "wikilink")。\[10\]\[11\]但是英国报道声称该机是自己坠毁的。战争结束后37架米格-29只有12架残余，有4架逃到了伊朗。\[12\]

### 厄立特里亞

1999年，厄立特里亞空軍宣稱用MiG-29擊落四架[衣索比亞](../Page/衣索比亞.md "wikilink")[MiG-21和](../Page/MiG-21.md "wikilink")[MiG-23](../Page/MiG-23.md "wikilink")，之後2000年又宣佈擊落一架MiG-21。但是衣索比亞也宣稱用[苏-27擊落厄立特里亞五架MiG](../Page/苏-27战斗机.md "wikilink")-29。蘇聯和烏克蘭都有一些傭兵在這場戰爭中參戰。其中，MiG-29暴露出發動機性能的嚴重缺陷，在位於苏-27六點方位的絕佳攻擊位置時卻因吸入廢氣而喪失動力，貢獻了該場戰爭中R27唯一一次擊落，而在全局戰況中，MiG-29均因內部燃油過少的問題而陷於被動。

### 美軍的米格-29

美國曾經購入前[摩尔多瓦空軍的](../Page/摩尔多瓦.md "wikilink")21架米格-29，其中14架「支點C」擁有主動雷達干擾設備並具備核彈投擲功能。這些米格-29全數指派給設在俄亥俄州[萊特-派特森空軍基地的國家航空情報中心](../Page/萊特-派特森空軍基地.md "wikilink")。其中一架「支點C」目前存放於[俄亥俄州](../Page/俄亥俄州.md "wikilink")[達頓市](../Page/代頓_\(俄亥俄州\).md "wikilink")[美國空軍博物館的倉庫中](../Page/美國空軍博物館.md "wikilink")，另外20架目前則下落不明，據信已遭拆解。

私人收藏家唐奇爾林曾以10萬美元的代價自[吉爾吉斯購入兩架米格](../Page/吉爾吉斯.md "wikilink")-29，礙於進口法令限制，無法進口搭配的航電裝備，兩機目前停放於美國伊利諾州的昆西，處於嚴重缺件並等待翻修的狀態\[13\]。

[Hungarian_Air_Force_Mikoyan-Gurevich_MiG-29B_(9-12A)_Lofting-1.jpg](https://zh.wikipedia.org/wiki/File:Hungarian_Air_Force_Mikoyan-Gurevich_MiG-29B_\(9-12A\)_Lofting-1.jpg "fig:Hungarian_Air_Force_Mikoyan-Gurevich_MiG-29B_(9-12A)_Lofting-1.jpg")

## 展示的米格-29

目前為止，共有六架前[摩爾達維亞空軍的米格](../Page/摩爾達維亞.md "wikilink")-29被安置在[美國的下列地點](../Page/美國.md "wikilink")：

  - [德州](../Page/德州.md "wikilink")[聖安格魯的](../Page/聖安格魯.md "wikilink")。
  - [內華達州的](../Page/內華達州.md "wikilink")。
  - 俄亥俄州的[萊特-派特森空軍基地](../Page/萊特-派特森空軍基地.md "wikilink")。
  - 兩架在內華達州的奈利斯空軍基地。一架在威脅訓練中心（[Threat Training
    facility](http://wikimapia.org/1444297/)）的外面，一架在旁邊有米格-23的機棚內\[14\]。
  - 一架正在[俄亥俄州代頓附近的美國空軍博物館內整修](../Page/俄亥俄州.md "wikilink")，一旦整修完成，將會在博物館展示。2006年12月時，該機正在進行油漆。

## 各式型號

  - **米格-29 "支點-A"（Product 9.12）**：初始生產版本;
    在1983年進入服務。北約報告名稱為“Fulcrum-A”。
  - **米格-29 "支點-A"（Product 9.12A）/米格-29B（Product
    9.12B)"支點-A"**：9.12A是[華沙公約外銷版本](../Page/華沙公約.md "wikilink")，而9.12B則是非華沙條約國家的外銷版本。它缺乏核武器運載系統，並擁有最初的生產雷達，ECM和IFF（9.12B中沒有ECM和IFF）
  - '''米格-29G "支點-A"（Product 9.12A)/米格-29GT "支點-B"（Product 9.51）
    '''：原為前[東德空軍米格](../Page/東德.md "wikilink")-29,在[兩德統一西德將所有原屬於東德的](../Page/兩德統一.md "wikilink")24架米格-29一起轉入了德國空軍，並組成為第五航空師。統一的德國就米格-29是否能進入德軍裝備展開爭論，這致使德國政府決定對米格-29的使用價值和戰鬥力進行評估,在1991年兩架米格-29在美國進行了兩個月的測試，最後終於贏得了德國的認可。爾後德國便宣布了米格-29正式進入德國空軍的決定。並將原先東德黃綠迷彩被改成了德國空軍標準的灰色空迷彩。德國在接收米格-29後對其進行了電子設備的改動以適應[北約的標準](../Page/北約.md "wikilink")，德國米格-29機身上的天線罩（雷達罩，邊條，尾翼天線）也都更換過成標準的材料。米格-29GT為聯邦德國空軍米格-29UB型。這批米格-29一直服役到颱風戰機開始裝備德國空軍為止，於2003年德國與波蘭政府簽訂了一項合同，以每架1美元的象徵性價格出售第73戰機聯隊第一中隊的所有可在役的米格-29。
  - **米格-29UB "支點-B"（Product
    9.51）**：MiG-29的作戰訓練版本。該型號MiG-29並沒有雷達，而是採用簡單的無線電定位器和紅外搜索和瞄準系統IRST（MiG-29UB最多只能只能發射R-73導彈，相似於早期美國F-16武裝部屬）。

最早原型9-51-1型從29系列中MiG-29
9.12（北約代號：支點A型)(註:支點A型還是早期量產型號)修改而成，最初於1981年4月29日首飛。米格的編號為9-51型，自1985年以後一直由GAPOiSO（今下諾夫哥羅德索科爾工廠）生產，工廠序號為30。在米格-29部屬第一年裡，由於還尚未部屬MiG-29UB型，因此在那時候還是由MiG-23UB型代為訓練。

  - **米格-29 "支點-C"（Product
    9.13）**：大致與9.12相同，但換裝大型內載油箱並加裝*Gardeniya*雷達干擾器。
  - **米格-29S "支點-C"（Product
    9.13S）**：大致上與9.13相同，但可掛載4噸彈藥與大型翼下副油箱。換裝N019ME雷達，可同時追蹤10個目標並同時攻擊其中兩個。
  - **米格-29S "支點-A"（Product
    9.12S）**：類似於前一個，但並沒有9.13型的Gardeniya干擾器，僅適用於MiG-29
    9.12。
  - **米格-29SE**：外銷型號的米格-29S，稍微降級的N-019ME雷達具有多目標跟踪能力和RVV-AE（R-77導彈）兼容性。其武器組合包括R-27T1，R-27ER1和R-27ET1中程導彈。該飛機可以安裝主動ECM系統，武器輔助工具，改進的內置檢查和訓練系統。米格-29SE可以同時使用兩個空中目標。
  - **米格-29SD**：更新米格-29(9.12)版與SE的改進相似，不過還追空中加油用的探頭。
  - **米格-29N**：為俄羅斯米格-29SD;
    1995年交付馬來西亞皇家空軍第17和19中隊的16架米格-29N單機和2架米格-29NUB雙座機。正常起飛重量為15000千克，最大起重量為2000千克，原為9.12。雷達是N-019ZM，與SE的N-019ME相同。
  - **米格-29SM "支點-C"（Product 9.13M）**：大致上與9.13相同，可掛載多種空對地飛彈與雷射導引炸彈。
  - '''米格-29AS / 米格-29UBS（米格-29SD）：

[斯洛伐克空軍為了](../Page/斯洛伐克.md "wikilink")[北約的兼容性對米格](../Page/北約.md "wikilink")-29/米格-29UB進行了升級。從2005年開始，RAC
Mig和西方公司就開始了這項工作。該飛機現在擁有來自羅克韋爾柯林斯公司的導航和通信系統，BAE系統公司的IFF系統，新型玻璃駕駛艙具有多功能液晶顯示器和數字處理器，與未來的西方設備融合。但是，飛機的軍備保持不變。整個米格-29機隊中有21架升級了12架，並於2008年2月底交付。

  - **米格-29BM**：

米格-29BM（可能是[白俄羅斯現代化](../Page/白俄羅斯.md "wikilink")，可能為Bolyshaya
Modernizaciya -
大型現代化）是在[白俄羅斯巴拉諾維奇的ARZ](../Page/白俄羅斯.md "wikilink")-558飛機修理廠進行的米格-29升級，米格-29BM是一個升級武裝的米格-29戰機，白俄羅斯米格-29BM類似俄羅斯米格-29SMT。它改進包刮的武器，雷達，以及添加空中加油管。

  - '''米格-29MU1 '''：

[烏克蘭現代化的米格](../Page/烏克蘭.md "wikilink")-29。空中目標的偵測範圍增加到29％

  - **米格-29 Sniper**：

[以色列公司為](../Page/以色列.md "wikilink")[羅馬尼亞空軍計劃升級](../Page/羅馬尼亞.md "wikilink")。第一次飛行是在2000年5月5日
但隨著2003年羅馬尼亞米格-29的退役，該方案停止。但由於維修費用高昂，導致羅馬尼亞政府決定停止米格-29升級方案，進一步投資[米格-21](../Page/米格-21.md "wikilink")
LanceR計劃。

  - **[米格-29M](../Page/MiG-29M戰鬥機.md "wikilink") / 米格-33 "支點-E"（Product
    9.15）**：戰鬥轟炸機，重新設計製造鋁材結構機身，以線傳飛控置換機械式操縱裝置，換裝後燃推力86KNt的RD-33K引擎。載彈量增為4500[公斤](../Page/公斤.md "wikilink")，增加機內油箱，最大航程延伸至2000[公里](../Page/公里.md "wikilink")。以[甲蟲雷達取代原先之N](../Page/甲蟲雷達.md "wikilink")-019雷達，駕駛艙加裝兩具多功能顯示器，除支援以[RVV-AE飛彈進行多目標接戰外](../Page/RVV-AE.md "wikilink")，亦題供地形追沿模式，武器掛點增加至8個。
  - **米格-29UBM（Product 9.61）**：雙坐型的米格29M
  - **米格-29SMT（Product
    9.17）**：早期米格29的提升型。加大油箱，航程提升至2100公里，座艙加裝兩具大型彩色液晶[多功能顯示器](../Page/多功能顯示器.md "wikilink")，另有兩具小型多功能顯示器。配備提升後之N019MP雷達，提供更長探測距離與額外對地掃描功能。換裝推力98.1KNt的新型RD-43渦輪扇葉引擎，掛載量提升至4.5噸並提供相同於米格-29M之武器選擇。此型號目前服役於俄國、葉門、阿爾及利亞與敘利亞空軍。
  - **[米格-29K](../Page/米格-29K戰鬥機.md "wikilink")（Product
    9.31）**：海軍型，除加裝折翼、尾勾、與強化之新起落架外，大致與米格-29M同。原計畫配備於[庫茲涅索夫海軍上將號](../Page/庫茲涅佐夫號航空母艦.md "wikilink")，一度計畫取消後恢復。

[MiG-29K_at_MAKS-2007_airshow_(3).jpg](https://zh.wikipedia.org/wiki/File:MiG-29K_at_MAKS-2007_airshow_\(3\).jpg "fig:MiG-29K_at_MAKS-2007_airshow_(3).jpg")

  - **米格-29K "支點-D"（Product
    9.41）**：海軍型，印度海軍與俄羅斯海軍預訂服役之機種，以全新[線傳飛控取代早期機械控制](../Page/線傳飛控.md "wikilink")，並提升航電至米格-29SMT之水準，由俄航母起飛參與轟炸敘利亞的2016年實戰。
  - **米格-29UBT（Product 9.51T）**：提升至SMT水準之米格-29UB。
  - **米格-29M2**：雙座多功能戰鬥轟炸機，性能大致與9.15相同，但配備液晶螢幕與數位化飛控裝置。計畫中單座的米格-29M1並未建造，若生產則大致與9.41相同。

[MiG-29OVT_2001.jpg](https://zh.wikipedia.org/wiki/File:MiG-29OVT_2001.jpg "fig:MiG-29OVT_2001.jpg")

  - **米格-29OVT**：目前只有一架，是其中一架預產裝上Klimov的矢量噴嘴的米格-29M的代號。
  - **米格-29UPG**：印度空軍米格-29B和米格-29UB戰鬥機的升級型，該機型裝備最新甲蟲-M2E雷達，新型航電設備、採用新式OLS-UEM-IRST傳感器、激光、熱成像和電視功能的三模吊艙，配備增強型RD-33系列渦噴發動機。基本武器裝備與米格-29SMT與米格-29K/KUB相同。
  - **[米格-35](../Page/米格-35.md "wikilink") "支點-F"（Product
    9.61）**：集成米高揚所有成就的最終生產型。現有改裝自米格-29M2的雙座預產機一架，計劃中還包括單座型號。該戰鬥機各方面性能大幅提升。特別是擁有與西方第四代戰鬥機看齊的完善航電，因此不再依賴戰管協助，成爲系列中唯一可執行獨立作戰的戰鬥機。

## 用戶

[Mig-29_operators.PNG](https://zh.wikipedia.org/wiki/File:Mig-29_operators.PNG "fig:Mig-29_operators.PNG")

### 現役中

  - （2 - 从乌克兰购买3架，1架毁于飓风）

  - （13）

  - （35）

  - （32）

  - （41）

  - [孟加拉](../Page/孟加拉.md "wikilink")（8）

  - （14 - 大多封存，其中一架存于哈瓦那航空博物馆）

  - （4）

  - （108 - 印度空军63架米格29和印度海军45架米格29k）

  - （44）

  - （14）

  - （12）

  - （18）

  - （30）

  - （177）

  - （11 - 2007年起，該國重新起用MiG-29,2017年又由俄羅斯提供6架早期型MiG-29(9.12/9.13/9.51型)）

  - （13）

  - （10）

  - （50）

  - （20）

  - （21）

  - （30）

  - （24）。

### 已退役

  -
  -
  -
  -
  -
  -
  -
  -
  - : [摩爾達維亞蘇維埃社會主義共和國時期](../Page/摩爾達維亞蘇維埃社會主義共和國.md "wikilink")

  -
  -
  -
### 非正式

  - 從前蘇聯國家購入作為假想敵訓練。\[15\]

  - （購自羅馬尼亞，用於實驗）。

  - (租借米格29用于假想敌研究）

## 有關內容

**相關機種**

[米格-35](../Page/米格-35戰鬥機.md "wikilink")

**類似機種**

[F-18大黃鋒](../Page/F/A-18黃蜂式戰鬥攻擊機.md "wikilink")、
[幻影2000](../Page/幻影2000战斗机.md "wikilink")、
[光輝戰鬥機](../Page/光輝戰鬥機.md "wikilink")、
[F-16戰隼](../Page/F-16戰隼戰鬥機.md "wikilink")、
[Su-27](../Page/苏-27战斗机.md "wikilink")、
[歼-10](../Page/歼-10.md "wikilink")、
[歼-11](../Page/歼-11战斗机.md "wikilink")

**設計時序**

[米格-15](../Page/米格-15战斗机.md "wikilink"){{.w}}[米格-17](../Page/米格-17战斗机.md "wikilink"){{.w}}[米格-19](../Page/米格-19战斗机.md "wikilink"){{.w}}[米格-21](../Page/米格-21戰鬥機.md "wikilink"){{.w}}[米格-23](../Page/米格-23战斗机.md "wikilink"){{.w}}[米格-25](../Page/米格-25战斗机.md "wikilink"){{.w}}**米格-29**{{.w}}[米格-31](../Page/米格-31戰鬥機.md "wikilink"){{.w}}[米格-35](../Page/米格-35戰鬥機.md "wikilink"){{.w}}[米格-1.44](../Page/米格1.44战斗机.md "wikilink")

**相關列表**

  - [飛機列表](../Page/飛機列表.md "wikilink")

<!-- end list -->

  - [雨燕飛行表演隊](../Page/雨燕飛行表演隊.md "wikilink")

## 註釋

## 參考文獻

  - Osprey - Combat Aircraft 053 - F-15C Eagle Units in Combat,作者Steve
    Davies

## 外部链接

  - [AA's aviation
    site](https://web.archive.org/web/20060903010115/http://www.freewebs.com/aaaviation/)
  - [Luftwaffe MiG-29s](http://www.fabulousfulcrums.de/)
  - [MiG-29 by Easy
    Tartar](https://web.archive.org/web/20110514051002/http://www.sci.fi/~fta/MiG-29.htm)
  - [飛米格-29](http://www.migflug.com/en/jet-fighter-flights/flying-with-a-jet/mig-29-fulcrum-in-russia.html)
  - [MiG-29 "Fulcrum" page by the Federation of American
    Scientists](http://www.fas.org/nuke/guide/russia/airdef/mig-29.htm)
  - [MiG-29 "Fulcrum" page by
    GlobalSecurity.org](http://www.globalsecurity.org/military/world/russia/mig-29.htm)
  - [Mikoyan-Gurevich MiG-29
    Videos](https://web.archive.org/web/20061024012947/http://www.flightlevel350.com/aviation_videos.php?airplane=Mikoyan-Gurevich+MiG-29)
  - [MiG-29
    Videos](http://www.patricksaviation.com/videos/search/?q=mig-29)
  - [Cuban MiG-29](http://urrib2000.narod.ru/EqMiG29-e.html)

[Category:蘇聯戰鬥機](../Category/蘇聯戰鬥機.md "wikilink")
[Category:俄羅斯戰鬥機](../Category/俄羅斯戰鬥機.md "wikilink")
[Category:雙發噴射機](../Category/雙發噴射機.md "wikilink")
[Category:米高揚飛機](../Category/米高揚飛機.md "wikilink")

1.  [米格-29戰鬥機家族發展歷程介紹](http://airforceworld.com/fighter/Mig-29_Fulcrum_Soviet_Union_1.htm)

2.  [俄罗斯航母在叙利亚](http://share.iclient.ifeng.com/house/shareNews?aid=114796050&autoFresh=1&channelId=)
3.  ["More MiG
    Malfunctions."](http://www.strategypage.com/htmw/htatrit/articles/20090721.aspx)
    *Strategy Page*, 21 July 2009. Retrieved 14 October 2009.
4.
5.  [Chronological Listing of Afghan
    Ejections](http://www.ejection-history.org.uk/Country-By-Country/Afghanistan.htm)

6.  [Mikoyan-Gurevich
    MiG-29](http://www.harpoondatabases.com/encyclopedia/Entry265.aspx)
7.  Long, Helen. ["Russia ‘shot down Georgia
    drone’."](http://news.bbc.co.uk/2/hi/europe/7358761.stm) BBC, 21
    April 2008. Retrieved 10 March 2009.
8.  [网页截图](http://ishare.iask.sina.com.cn/f/23203602.html)
9.  Steve Davies. *F-15C Eagle Units in Combat*, p. 88. Osprey Combat
    Aircraft 53.
10. ["один самолет Панавиа «Торнадо» английских ВВС'' \[Tornados:
    BBC,（in Russian）.](http://army.lv/?s=502&id=132&v=9) *army.lv.*
    Retrieved 23 October 2010.
11. ["Iraqi air-air victories during the Gulf
    War 1991."](http://aces.safarikovi.org/victories/victories-iraq-gulf.war.pdf)
    *safarikovi.org.com,* 2004. Retrieved 7 December 2009.
12. ["Iran Air
    Force."](http://www.globalsecurity.org/military/world/iran/airforce.htm)
    globalsecurity.org. Retrieved 19 July 2009
13. <http://www.wired.com/wired/archive/13.10/kirlin_pr.html>
14. [1](http://www.richard-seaman.com/Aircraft/Museums/ThreatTrainingFacility/Aircraft/index.html)
15. [air-usa.](http://air-usa.com/aircraft)