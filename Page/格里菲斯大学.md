**格里菲斯大學**是[昆士蘭州](../Page/昆士蘭州.md "wikilink")[首府布里斯本市內的第](../Page/首府.md "wikilink")3所[綜合大學](../Page/綜合大學.md "wikilink")，創立於1971年，是澳洲排名前十五名的大學。

校名以昆士蘭州前任州長[山謬·格里菲斯命之](../Page/山謬·格里菲斯.md "wikilink")。它在[布里斯本有](../Page/布里斯本.md "wikilink")4個校區，在[黃金海岸有一個校區](../Page/黃金海岸_\(澳大利亚\).md "wikilink")。最先設址而且最大的是位於校總區－內森校區（Nathan），素以昆士蘭獨有的山林環境而聞名，地處繁榮的南郊卻為茂密的灌木和森林所包圍著。除了環山車道旁的人行道之外，尚有多條林間小徑通向山下的附屬體育中心。2004年以後，黃金海岸校區因奉-{准}-成立醫學院而躍為該校最大的校區。近年積極與亞洲國家締結姊妹校，使許多國際交換學生前往研讀。

2012年，學生總數有37,787人；研究生人數7,108名，國際學生8,847人，教職人員3,563人。以學生數量觀之，該校目前為澳洲第九大的高等教育機構。\[1\]在全澳洲的大學之中，綜合排名第15大\[2\]；[商學院獲](../Page/格里菲斯商學院.md "wikilink")[AACSB認證](../Page/AACSB.md "wikilink")\[3\]。其商學院的旅遊及酒店管理科亦於[QS世界大學排名](../Page/QS世界大學排名.md "wikilink")2017年全球排名第九。\[4\]

## 校務近況

格里菲斯大學有許多創新，例如：是澳洲第一個提供環境科學和亞洲研究學位的大學，商學院是澳洲第一所簽下聯合國責任管理原則教育的學術機構。州內第2所醫學院奉-{准}-設立於格里菲斯大學黃金海岸分校以後，對該校之財務健康、學術評鑑排名\[5\]
及遠景發展計劃如虎添翼。2006年起，學生總數突破30,000人。格里菲斯各校區內所附屬之健身中心、游泳池、學生課外團體、學子諮詢單位、二手書店、文教物品專賣店、咖啡館、餐廳、學生大會堂等育樂設施齊全。海外學生大多來自歐洲、亞洲和南美洲。

2007年政府研究資金（Australia Research Coucil）的分發中，格里菲斯大學獲得澳幣$5,025,230
\[6\]。至於Learning and Teaching Performance Fund則有澳幣$500,000 \[7\] 。

## 校務發展定位

  - 校總區－內森的強項包括有財經科系（銀行金融、行銷、國際貿易、企業管理、風險管理）、區域文化研究（亞洲太平洋、國際關係、語言學系）、環保科系（沿海環保、果蠅研究、生態觀光、海洋生物、野生動物研究）以及資訊科系（資訊管理、資訊系統學系）等。校址距離市中心約15公里。

<!-- end list -->

  - Gravatt山分校的強項有人文科系、教育科系、心理學系、犯罪學系、運動學系以及觀光學系等。校址與校總區僅相隔3公里，雙邊有區間免費巴士供學子往返。

<!-- end list -->

  - 洛根分校針對未來澳洲最需要的管理人才，設立了企業創新管理和媒體傳播科系。校址設於布里斯本
  - [藝術學院](../Page/昆士蘭藝術學院.md "wikilink")（QCA）、[音樂學院（QCGU）皆設於布里斯本](../Page/昆士蘭音樂學院.md "wikilink")[南岸的](../Page/南岸_\(昆士蘭州\).md "wikilink")[中央商業區內](../Page/布里斯本中央商業區.md "wikilink")，與[昆士蘭科技大學](../Page/昆士蘭科技大學.md "wikilink")、昆士蘭[州政府](../Page/州政府.md "wikilink")、[州議會和](../Page/州議會.md "wikilink")[市政府僅一水之隔](../Page/布里斯本市政府.md "wikilink")，在此有針對藝術和音樂所需之行銷、經營、教育、語言及創意設計等輔修課程，藝術和音樂兩大學院為全澳最佳學校。由於華納兄弟澳洲製片廠設於昆士蘭黃金海岸的「電影世界」遊樂園區（Movie
    World），而格-{里}-菲斯大學黃金海岸分校則是黃金海岸僅有的國立大學；由此利基，格-{里}-菲斯大學遂於QCA南岸校內斥資籌備全澳規模最大的攝影學系\[8\]，以利未來的發展。

<!-- end list -->

  - 黃金海岸分校為國際觀光勝地——[黃金海岸](../Page/黃金海岸_\(澳大利亞\).md "wikilink")（[南港](../Page/南港_\(昆士蘭州\).md "wikilink")）之唯一國立大學校區；設有州內最完整的生態研究科系及企業管理、飯店行銷和餐飲管理等學系。格里菲斯大學的MBA有8年教育的歷史，許多亞裔、阿拉伯裔、當地居民均在該校攻讀學位。校內並設本州第二所醫學院，現為格里菲斯大學的最大校區。

## 學院

  - 文學院（Faculty of Arts）
  - 商學院（Griffith Business School）
  - 教育學院（Faculty of Education）
  - 工程暨資訊學院（Faculty of Engineering & Information Technology）
  - 健康醫學院（Griffith Health）
  - 法學院（Griffith Law School）
  - [昆士蘭音樂學院](../Page/昆士蘭音樂學院.md "wikilink")
  - 環境學院（Faculty of Environmental Sciences）
  - 理學院（Faculty of Science）
  - [昆士蘭藝術學院](../Page/昆士蘭藝術學院.md "wikilink")

### 格里菲斯商學院

簡稱GBS，原名「格里菲斯大學管理研究所」（Griffith University Graduate School of
Management），校址位於[昆士蘭州](../Page/昆士蘭州.md "wikilink")[布里斯本和](../Page/布里斯本.md "wikilink")[黃金海岸兩地](../Page/黃金海岸_\(澳大利亞\).md "wikilink")；革新後，為[澳大利亞東岸地區](../Page/澳大利亞.md "wikilink")[MBA辦學評價最高的](../Page/澳大利亞教育.md "wikilink")[商學院之一](../Page/商學院.md "wikilink")。主要教授[企業管理碩士及學士與學士後等學位課程](../Page/企業管理碩士.md "wikilink")（postgraduate
business
programs），其教學品質2008年獲[AACSB肯定](../Page/AACSB.md "wikilink")\[9\]；2002年起獲GMAA評為5星級\[10\]\[11\]\[12\]。

其下轄七大學-{系}-\[13\]，分別為會計、金融暨經濟學系、工作關係學系、國際貿易暨亞洲研究學系、管理學系、市場行銷學系、政治暨公共政策學系和觀光、休閒、飯店暨體育管理學系。

課程分為五大類：學士學位課程、學士後學位課程、研究學位課程、進修教育課程、高階管理發展課程。

## 分校

  - 黃金海岸 Gold Coast 分校
  - 洛根 Logan 分校
  - Gravatt山 Mt Gravatt 分校
  - 內森 Nathan 校總區
  - 昆士蘭音樂學院 Queensland Conservatorium (QCGU)
  - 昆士蘭藝術學院 Queensland College of Art (QCA)

## 姊妹校

### 亞洲地區

  - ：[高雄醫學大學](../Page/高雄醫學大學.md "wikilink")、[國立台灣海洋大學](../Page/國立台灣海洋大學.md "wikilink")、[臺北醫學大學](../Page/臺北醫學大學.md "wikilink")、[佛教慈濟大學](../Page/佛教慈濟大學.md "wikilink")、[長庚學校財團法人長庚科技大學](../Page/長庚學校財團法人長庚科技大學.md "wikilink")

## 知名校友

  - 昆士蘭州貿易和天然資源、礦產與能源廳長

  - [Andrew
    Fraser](../Page/:en:Andrew_Fraser_\(Queensland_politician\).md "wikilink")
    昆士蘭州財政廳長\[14\]

  - 昆士蘭州議員

  - 前任聯邦議員（1996-2007）和霍華德內閣閣員

  - [布雷特·馬森](../Page/布雷特·馬森.md "wikilink") 聯邦上議員

  - 聯邦議員

  - 聯邦議員（音樂學院畢業生）

  - [羅斯·瓦斯塔](../Page/羅斯·瓦斯塔.md "wikilink") 前任聯邦議員（2004-2007）

  - [斯蒂芬·布拉德伯里](../Page/斯蒂芬·布拉德伯里.md "wikilink") 奧運speed skater項目金牌得主
    \[15\]

  - [萨拉·卡里根](../Page/萨拉·卡里根.md "wikilink") 奧運自行車項目金牌得主\[16\]

  - [Shannon Eckstein](../Page/:en:Shannon_Eckstein.md "wikilink"),
    2002年度鐵人比賽冠軍 \[17\]

  - [莉比·特里克特](../Page/莉比·特里克特.md "wikilink") 奧運-{游}-泳項目金牌得主\[18\]

  - [鄭麗媛](../Page/鄭麗媛.md "wikilink") 南韓歌手／演員\[19\]

  - , 工程師企業家

  - 電視記者

  - 生育運動倡導者

  - [David Vernon](../Page/:en:David_Vernon_\(writer\).md "wikilink") 作家

  - [Robert
    Warren](../Page/:en:Robert_Warren_\(musician\).md "wikilink") 音樂家

  - 退休女演員

  - [尼克·武伊契奇](../Page/尼克·武伊契奇.md "wikilink") 勵志演講人和基督教佈道家

## 参考文献

## 外部链接

  - [格里菲斯大學官方網址](http://www.griffith.edu.au/)
  - [Motion－研究專訪頻道](https://web.archive.org/web/20090509191008/http://video.griffith.edu.au/info.php)
  - [GNN－格里菲斯大學《最新新聞》](https://web.archive.org/web/20090416011653/http://app.griffith.edu.au/03/griffith-news-now/)
  - [格里菲斯商學院
    公共管理碩士班](http://www.griffith.edu.au/future-students/our-study-areas/business-commerce/why-study-at-griffith/teaching-in-beijing)
    北京學苑
  - [格里菲斯研究中心](https://web.archive.org/web/20080719233831/http://www.griffith.edu.au/research/research-centres)
    Research Centres

{{-}}

[Griffith](../Category/昆士蘭州大學.md "wikilink")
[Category:1971年創建的教育機構](../Category/1971年創建的教育機構.md "wikilink")

1.  [Griffith Fast
    facts](http://www.griffith.edu.au/about-griffith/fast-facts.html)

2.  [Rankings of Australian
    Universities 2007](http://www.australian-universities.com/rankings/)

3.  [格里菲斯商學院入學簡章](http://www.griffith.edu.au/__data/assets/pdf_file/0005/141809/griffith-business-school-profile-brochure.pdf)


4.  \[QS World University Rankings by Subject 2017\]

5.  [Griffith MBA scoops the pool with their 5-Star MBA rating now six
    years
    running](http://www.griffith.edu.au/business/master-business-administration)


6.  [ARC](http://www.arc.gov.au/funded_grants/DP07_Selection_Rpt.htm)

7.  [LTPF](http://www.dest.gov.au/Ministers/Media/Bishop/2006/12/b002071206.asp)


8.  [Griffith Film
    School](http://www.griffith.edu.au/faculty/qca/griffith_film_school/)


9.  [Collegiate Institutions Earn AACSB International Accounting and
    Business
    Accreditation](http://www.aacsb.edu/media/releases/2008/accreditation-1208.asp)

10. [2002](http://www.griffith.edu.au/business/griffith-business-school/news-events/griffith-joins-the-ranks-of-harvard)


11. [2004](http://www.gmaa.com.au/files/ACF7A71.pdf)

12. [2005](http://www.gmaa.asn.au/files/GMA%20Star%20Rating%20Assessment%20for%20MBA%20Courses%202005.pdf)


13. [GBS官方網站](http://www.griffith.edu.au/business/griffith-business-school)


14. [Griffith University |
    News](http://www3.griffith.edu.au/03/ertiki/tiki-read_article.php?articleId=12301)


15. [Griffith University News
    Service](http://www.griffith.edu.au/er/archive/2002_1/jan1g02.html)

16. [\[ :: Griffith at the Commonwealth Games - Griffith University
    ::](http://griffith.e-newsletter.com.au/pub/pubType/EN/pubID/92986ab6f93d3a1226bc/nc/fdbb9178727d746f9504/interface.html)
    \]

17. [Griffith University News
    Service](http://www.griffith.edu.au/er/archive/2003_2/03oct29b.html)


18.
19.