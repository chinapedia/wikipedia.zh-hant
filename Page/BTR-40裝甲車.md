**BTR-40**是由[蘇聯](../Page/蘇聯.md "wikilink")[GAZ的V](../Page/GAZ.md "wikilink").
A.
Dedkov在1950至1958年制造的[裝甲運兵車](../Page/裝甲運兵車.md "wikilink")。BTR解為*Bronetransporter*([俄文](../Page/俄文.md "wikilink")：БТР，Бронетранспортер，[英文解作Armoured](../Page/英文.md "wikilink")
Transporter)\[1\] 。

BTR-40由[GAZ-63改進而成](../Page/GAZ-63.md "wikilink")，基本型裝有2道側門及1道尾門，可載1個駕駛員，1個指揮官，及8個士兵。

## 衍生型

**[蘇聯](../Page/蘇聯.md "wikilink")**

  - **BTR-141**—原型
      - **BTR-40**—原型生產型
          - **BTR-40A**（1951年）—防空型，裝有ZPTU-2雙聯裝[KPV
            14.5毫米機槍](../Page/KPV重機槍.md "wikilink")
          - **BTR-40B**（1956年）—裝有裝甲頂版，乘員降至1個駕駛員、1個指揮官加6個乘客
          - **BTR-40V**（1957年）—裝有改良型輪胎壓力調節系統
          - **BTR-40ZhD**（1969年）—試驗性車輛，裝有路軌輪

**[中国](../Page/中华人民共和国.md "wikilink")**

  - **55式**—中国仿制版本

**[以色列](../Page/以色列.md "wikilink")**

  - **BTR-40**—俘虜自[埃及及](../Page/埃及.md "wikilink")[敘利亞軍隊](../Page/敘利亞.md "wikilink")

**[印尼](../Page/印尼.md "wikilink")**

  - **BTR-40**—裝有[轻机槍及方形機槍塔](../Page/轻机槍.md "wikilink")，車頂裝有煙霧彈發射器
  - **BTR-40**—裝有40毫米機炮塔，車頂裝有煙霧彈發射器及[探照燈](../Page/探照燈.md "wikilink")

**[東德](../Page/東德.md "wikilink")**

  - **SPW-40**—東德的BTR-40
  - **SPW-40A**—東德的BTR-40A
  - **SPW-40Ch**—東德的BTR-40Kh
  - **BTR-40**—裝有9M14（AT-3）反坦克導彈發射器

**[古巴](../Page/古巴.md "wikilink")**

  - **BTR-40A-AA**—防空型，但將原本後座的所有座位拆除，改為裝上ZPTU-2雙聯裝[KPV重機槍](../Page/KPV重機槍.md "wikilink")
  - **BTR-40A-PB**—裝有反坦克導彈發射器的BTR-40
  - **Jabali（M1975/4）**—裝有3M11（AT-2）反坦克導彈發射器的BTR-40，只制造了小量

[BTR-40-latrun-2.jpg](https://zh.wikipedia.org/wiki/File:BTR-40-latrun-2.jpg "fig:BTR-40-latrun-2.jpg")的BTR-40\]\]

## 使用國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - —55式

  -
  -
  -
  -
  -
  - —55式

  -
  -
  -
  - [Flag_of_Vietnam.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Vietnam.svg "fig:Flag_of_Vietnam.svg")
    [北越](../Page/北越.md "wikilink")—55式

  -
## 参考文献

<div class="references-small">

<references />

  - Foss, Christopher. Jane's Tank Recognition Guide. (New York:
    HarperCollins, 2006), p.288.
  - ["JED The Military Equipment
    Directory"](http://www.jedsite.info/afv/bravo/btr-40_series/btr40-series.html)

</div>

## 外部連結

  - —[globalsecurity.org的BTR-40介紹](http://www.globalsecurity.org/military/world/russia/btr-40.htm)

  - —[armoured.vif2.ru的BTR-40圖片及介紹](https://web.archive.org/web/20051027060422/http://armoured.vif2.ru/btr40.htm)

  - —[legion.wplus.net的BTR-40圖片及介紹](http://legion.wplus.net/guide/army/ta/btr40.shtml)

[Category:裝甲偵察車](../Category/裝甲偵察車.md "wikilink")
[Category:裝甲運兵車](../Category/裝甲運兵車.md "wikilink")
[Category:蘇聯裝甲戰鬥車輛](../Category/蘇聯裝甲戰鬥車輛.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")

1.  [†](http://www.milparade.com/Soderzhaniye.pdf)