**池田町**（）為福井縣中部的一町。面積194.72平方公里，總人口3,405人。該町90%的面積為山地。現任町長為杉本博文。

## 地理

  - 山: 冠山、部子山
  - 河川：[足羽川](../Page/足羽川.md "wikilink")

### 相鄰的自治体

  - [福井縣](../Page/福井縣.md "wikilink")

<!-- end list -->

  - [福井市](../Page/福井市.md "wikilink")　（旧[美山町](../Page/美山町_\(福井縣\).md "wikilink")）
  - [大野市](../Page/大野市.md "wikilink")
  - [南條郡](../Page/南條郡.md "wikilink") :
    [南越前町](../Page/南越前町.md "wikilink")
  - [越前市](../Page/越前市.md "wikilink")
  - [鯖江市](../Page/鯖江市.md "wikilink")

<!-- end list -->

  - [岐阜縣](../Page/岐阜縣.md "wikilink")

<!-- end list -->

  - [揖斐郡](../Page/揖斐郡.md "wikilink") :
    [揖斐川町](../Page/揖斐川町.md "wikilink")

## 經濟

### 產業

  - 主要產業　
  - 產業人口（2005年國勢調査）
      - 第一次產業：　158人
      - 第二次產業：　697人
      - 第三次產業：　774人

## 外部連結

  - [福井縣池田町](http://www.town.ikeda.fukui.jp/index.html)