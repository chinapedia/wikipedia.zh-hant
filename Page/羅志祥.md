**羅志祥**（），外號**小豬**，[臺灣](../Page/臺灣.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")、[主持人](../Page/主持人.md "wikilink")、[男演員](../Page/男演員.md "wikilink")，[阿美族人](../Page/阿美族.md "wikilink")。1996年，以團體[四大天王出道](../Page/四大天王_\(團體\).md "wikilink")，後與[歐漢聲組成](../Page/歐漢聲.md "wikilink")[羅密歐](../Page/羅密歐_\(團體\).md "wikilink")，解散後轉型為[綜藝節目主持人](../Page/綜藝節目.md "wikilink")。2003年，推出個人專輯，唱片總銷量在亞洲逾700萬張，並連續四年獲得台灣銷售總冠軍（2010至2013年）\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]。羅志祥除了能歌擅舞，又跨足了戲劇節目、電影、主持及實境秀等，被評為全方位藝人的殊榮。2010年與[周杰倫](../Page/周杰倫.md "wikilink")、[王力宏及](../Page/王力宏.md "wikilink")[林俊杰被媒體封為](../Page/林俊杰.md "wikilink")「華語樂壇新四大天王」。

## 早年生平

羅志祥有[阿美族一半血統](../Page/阿美族.md "wikilink")，父親為[鐵路局工務段人員](../Page/臺灣鐵路管理局.md "wikilink")，從小生長在康樂隊及那卡西的家庭中，是獨生子。三歲時因跟隨媽媽林香蘭練歌曲的節奏敲打餅乾罐，被發現其打[架子鼓的天分](../Page/爵士鼓.md "wikilink")，其後更在康樂隊中表演打架子鼓，他在五歲時就成了康樂隊的台柱，隨著父母在[台北和](../Page/台北.md "wikilink")[基隆等地表演](../Page/基隆.md "wikilink")，更在六歲時和爸爸羅忠慶一起參加了[中國電視公司](../Page/中國電視公司.md "wikilink")《[六燈獎](../Page/六燈獎.md "wikilink")》節目的親子歌唱大賽，榮獲亞軍。

因小學及初中時很肥胖，加上皮膚也很黑，所以羅志祥當時被旁人取笑稱作「小豬」，並一直沿用這[暱稱](../Page/暱稱.md "wikilink")。然而因不想再被取笑和被同學欺負，他於[國三暑假以游泳及打籃球的方式減肥](../Page/九年级.md "wikilink")，並成功瘦身。羅志祥當時曾經模仿當紅的[林志穎而考進](../Page/林志穎.md "wikilink")[華岡藝校](../Page/華岡藝校.md "wikilink")，但因為家境貧窮，母親林香蘭建議他回[基隆上學](../Page/基隆.md "wikilink")。由於羅能夠模仿林志穎和其他藝人的簽名而成為學校的風雲人物，後畢業於[基隆市私立培德高級工業家事職業學校](../Page/基隆市私立培德高級工業家事職業學校.md "wikilink")。

羅志祥因青蛙肢\[7\]而未服兵役，他有輕微的「心臟二尖瓣膜閉鎖不全症」，因此不能食用含有咖啡因的食品\[8\]。他從15歲開始跳街舞，導致膝軟骨磨損嚴重，20歲就有「軟腳」症兆，醫生診斷出他患有「髕骨軟化症」，建議開刀進行人工軟骨手術，羅志祥怕手術恐讓他不能再跳舞而拒絕開刀\[9\]。

## 演藝生涯

### 1994年〜2002年

羅志祥於1994年出道。1995年12月，由[中視](../Page/中視.md "wikilink")《歡樂傳真》節目主辦、威聚唱片企劃執行的「四大天王就是你」模仿大賽
\[10\]，羅志祥因五官深遂具有[原住民血統](../Page/原住民.md "wikilink")，報名參加模仿[郭富城](../Page/郭富城.md "wikilink")，唯妙唯肖、獲得好評，與其他三位模仿選手：[歐漢聲](../Page/歐漢聲.md "wikilink")（[張學友](../Page/張學友.md "wikilink")）、陳顯政（[劉德華](../Page/劉德華.md "wikilink")）和陳霆叡（[黎明](../Page/黎明.md "wikilink")）組成偶像團體「四大天王」，正式踏入娛樂圈。1998年四大天王因成員陳顯政收到兵役征召及陳霆叡赴美留學，而宣佈退出，因此羅志祥與剩下的另一名團員歐漢聲（羅志祥高中的同學）另組團體[羅密歐](../Page/羅密歐.md "wikilink")，當時「羅密歐」為踏足主持界而在解散後雙雙拜[胡瓜為師](../Page/胡瓜.md "wikilink")。「羅密歐」因歐漢聲的兵役問題而宣佈解散，當時也牽涉合約問題，幸而得到現任經紀人小霜的幫助，義務幫羅志祥打官司，繼而成為他的經紀人。\[11\]\[12\]

從「四大天王」解散後，羅志祥繼續以個人的身份，在綜藝主持、戲劇、及歌唱發展，累績了人氣，主持的三個節目《TV三賤客》、《少年兵團
旗開得勝》和《台灣 Who怕
Who》都是收視冠軍。演藝事業正起飛時卻因合約問題，以及那三個節目陸續停播，和主持《TV三賤客》前的電影《蘋果咬一口》雖然有其他大明星參與演出，但受到票房奇差的原因，令他蒙上「票房毒藥」的惡名，加上擺脫不了三冠王時期搞笑扮醜的形象，使演藝事業陷入危機，直到三個多月後，於2001年得到[楊登魁的提拔](../Page/楊登魁.md "wikilink")，代班《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》\[13\]，演藝生涯得以延續\[14\]。

### 2003年〜2013年

2003年羅志祥推出首張個人專輯《[Show
Time](../Page/Show_Time.md "wikilink")》，遠赴日本向前「TRF」團員，亦是東洋天后[安室奈美惠前夫Sam拜師學藝](../Page/安室奈美惠.md "wikilink")。爾後羅志祥在[艾迴唱片旗下陸續發行其餘三張專輯](../Page/艾迴唱片.md "wikilink")。其中2005年可算是羅志祥的豐收年。除了於10月21日推出第三張個人專輯《[催眠Show](../Page/催眠Show.md "wikilink")》，更雙料入圍台灣電視[金鐘獎](../Page/金鐘獎.md "wikilink")，其中與[陳建州一起主持的](../Page/陳建州.md "wikilink")《冷知識轟趴》入圍了[綜藝節目獎](../Page/綜藝節目獎.md "wikilink")，個人亦以《[鬥魚2](../Page/鬥魚2.md "wikilink")》入圍了[戲劇節目男主角獎](../Page/戲劇節目男主角獎.md "wikilink")。而羅志祥於同年11月18日舉行首場個人演唱會，成為第一位踏入[台北小巨蛋演唱的流行歌手](../Page/台北小巨蛋.md "wikilink")。

2007年5月21日，羅志祥正式簽約加盟為[EMI成為旗下子公司](../Page/EMI.md "wikilink")[Capitol唱片的歌手](../Page/Capitol唱片.md "wikilink")，並計劃由另一[香港子公司](../Page/香港.md "wikilink")[金牌娛樂代理下擴展他的事業版圖](../Page/金牌娛樂.md "wikilink")。在這年羅志祥與[大S合作演出](../Page/大S.md "wikilink")[偶像劇](../Page/偶像劇.md "wikilink")《[轉角\*遇到愛](../Page/轉角*遇到愛.md "wikilink")》，收視亮眼。同年舉辦「Show
on stage一支獨秀」首場個人大型巡迴演唱會，並將羅志祥內心深處的真實情感以傳記影片串接五段演唱會畫面發行並集結成書發表「羅志祥 Show
on
stage進化三部曲」。視、歌、主持三棲的羅志祥在電視、歌唱等多元發展。同年，[屈臣氏採用羅志祥擔任代言人](../Page/屈臣氏.md "wikilink")，因外界對他的諧星印象深刻，市場普遍不看好；然而相關廣告帶動屈臣氏的營收成長八成，讓羅志祥獲得連續三年的代言\[15\]。

2008年7月11日，羅志祥發行個人首張演唱會專輯《[殘酷舞台](../Page/殘酷舞台.md "wikilink")》，並於同年12月26日發行第六張國語大碟《[潮男正傳](../Page/潮男正傳.md "wikilink")》。2009年，羅志祥和[楊丞琳合作拍攝偶像劇](../Page/楊丞琳.md "wikilink")《[海派甜心](../Page/海派甜心.md "wikilink")》，並再次擔任男主角，此劇收視亮眼，曾在平均收視率及最高分段收視拿下5.14％與6.88％的成績。同年，羅志祥憑《[籃球火](../Page/籃球火.md "wikilink")》一劇再次獲提名金鐘獎戲劇節目男主角獎。

2010年1月15日推出第七張個人專輯《[羅生門](../Page/羅生門_\(專輯\).md "wikilink")》。2010年2月，羅志祥在[臺北縣](../Page/新北市.md "wikilink")[林口鄉拍攝](../Page/林口區.md "wikilink")「舞法舞天3D世界巡迴演唱會」主題曲-舞法舞天的MV，這首歌同時拍攝成另一3D版本，成為華人第1支[3DMV](../Page/3D.md "wikilink")，並收錄於舞法舞天3D影音典藏版內。「舞法舞天3D世界巡迴演唱會」於2010年4月30日至5月2日在香港的[紅磡體育館正式展開](../Page/香港體育館.md "wikilink")。這為羅志祥第一次在紅館舉行個人演唱會，票房十分亮眼。巡迴演唱會的第二站於5月15日至5月16日在台北小巨蛋舉行，並創下了24小時內辦3場演唱會的紀錄。羅志祥該年推出的《羅生門》是2010年台灣的年度唱片銷售總冠軍；在戲劇發展上，他亦以《海派甜心》第三度獲提名金鐘獎戲劇節目男主角獎。

2011年2月18日羅志祥發行第八張個人專輯《[獨一無二](../Page/獨一無二_\(羅志祥專輯\).md "wikilink")》，並首次以抒情歌〈拼什麼〉作為第一波主打歌。3月19日，「舞法舞天
一萬零一夜 ENCORE WORLD LIVE
TOUR」正式於[台中開跑](../Page/台中.md "wikilink")，以新歌單及表演形式取代原本「舞法舞天世界巡迴演唱會」。羅志祥於該年成為日本[波麗佳音唱片公司旗下藝人](../Page/波麗佳音.md "wikilink")，並於7月20日推出《獨一無二》的日文版，收錄《獨一無二》第二主打曲〈獨一無二ONLY
YOU〉的日語版本〈ONLY YOU〉。同年年底，羅志祥因多年來致力於保護、照顧流浪動物，獲邀為「亞洲善待動物組織」（PETA
Asia）拍攝公益廣告，並被選成為該組織的「年度傑出人物」。羅志祥該年推出的《獨一無二》順利蟬聯台灣的年度唱片銷售總冠軍。

2012年，羅志祥正式前往[日本發展](../Page/日本.md "wikilink")，於2月15日推出首張日文[單曲](../Page/單曲.md "wikilink")《[Dante](../Page/Dante_\(單曲\).md "wikilink")》。而其第九張個人專輯《有我在》亦於4月6日正式推出。截至5月17日，《有我在》全台銷量已經超過上張專輯《獨一無二》的銷量，達155,215張。\[16\]
同年6月20日，羅志祥再推出第二張日文單曲〈[Magic](../Page/Magic_\(單曲\).md "wikilink")〉。9月19日，羅志祥的第一張全日文專輯《[THE
SHOW](../Page/THE_SHOW.md "wikilink")》正式發行。因9月中期中日關係緊張，羅志祥於9月17日在其微博及其他報章上宣布將全面停止他在日本的宣傳活動，包括原定於9月29日及9月30日一連兩天在日本[品川舉行與專輯同名的](../Page/品川.md "wikilink")「THE
SHOW」售票演唱會。\[17\]
其第三張個人國語精選專輯《[舞極限](../Page/舞極限.md "wikilink")》於11月23日正式推出，同名主打歌舞極限的MV更是華人史上首支以真實坦克入鏡拍攝的MV作品。\[18\]
根據多份唱片公司業務報表交叉比對，羅志祥憑《有我在》連續三年成為台灣的年度唱片銷售總冠軍，而《舞極限》也擠進年度唱片銷售第8名。\[19\]

在日本宣傳活動停止期間，羅志祥為2013年的「舞極限 Over The
Limit世界巡迴演唱會」擔任視覺、音樂及舞蹈總監，並於1月4日起從台北小巨蛋出發作世界巡迴演出\[20\]。在2013年2月，羅志祥客串演出的[周星馳執導電影](../Page/周星馳.md "wikilink")《[西游·降魔篇](../Page/西游·降魔篇.md "wikilink")》正式上映，讓羅志祥繼12年前參演《蘋果咬一口》後，再次出現於電影大銀幕上\[21\]\[22\]
。同年八月，羅志祥與[蕭亞軒正式簽約轉投](../Page/蕭亞軒.md "wikilink")[索尼音樂](../Page/索尼音樂.md "wikilink")，且重新展開於日本地區的宣傳及演藝活動。個人第十張國語專輯《[獅子吼](../Page/獅子吼_\(專輯\).md "wikilink")》則在睽違1年半後，於11月8日正式發行。作為加盟索尼音樂後的第一張音樂作品，這專輯收錄4首由羅志祥一手包辦詞曲的個人創作，包括專輯同名作《獅子吼》\[23\]。其中《惜命命》更是其第一次創作的台語作品\[24\]。12月5日，羅志祥於記者會中宣佈將於高雄出發，舉辦「舞極限之舞魂再現安可世界巡迴演唱會」，且獅子吼亦獲認證售出四白金唱片（即12萬張）的成績\[25\]。《獅子吼》亦使羅志祥連續四年成為台灣的年度唱片銷售總冠軍，也是第一位蟬聯台灣唱片銷售四連霸的華語樂壇歌手\[26\]\[27\]。羅志祥於2013年度總計共接下17支廣告、30場演唱會、10場以上商演，收入達新台幣6億元，拿下台灣藝人收入排行第一名，創下事業高峰\[28\]。

### 2014年〜至今

2014年，羅志祥與經紀人小霜創立新經紀公司——「創造力娛樂有限公司」。首位加盟旗下的藝人，是擺脫兒童電視台姐姐身份的[簡愷樂](../Page/簡愷樂.md "wikilink")\[29\]。另外，藝人[林大晉的迷你專輯收錄了羅志祥作曲的主打歌](../Page/林大晉.md "wikilink")〈MY
GIRL〉，而羅志祥亦為該曲首次擔任歌曲製作人一職。林大晉於5月18日舉辦的個人演唱會亦邀得羅志祥擔任舞台總監。在2014年6月30日，羅志祥與楊丞琳一同加盟重返臺灣市場的[EMI唱片](../Page/EMI.md "wikilink")。\[30\]同年8月8日，羅志祥發行第三張演唱會專輯《[極限拼圖](../Page/極限拼圖LIVE_TOUR_DVD.md "wikilink")》，記錄2013年舞極限世界巡迴演唱會的片段。

2015年5月，羅志祥參與錄製中國大陸[東方衛視製作的遊戲真人秀](../Page/東方衛視.md "wikilink")《[極限挑戰](../Page/極限挑戰.md "wikilink")》，與[孫紅雷](../Page/孫紅雷.md "wikilink")、[黃磊](../Page/黃磊.md "wikilink")、[黃渤](../Page/黃渤.md "wikilink")、[王迅及](../Page/王迅_\(演员\).md "wikilink")[張藝興共同主持](../Page/張藝興.md "wikilink")，共十二集，並決定在10月份開拍極限挑戰電影版。2016年，羅志祥參與另一部周星馳執導的電影《[美人魚](../Page/美人魚_\(2016年電影\).md "wikilink")》，並於2016年2月8日[大年初一上映](../Page/大年初一.md "wikilink")。

## 爭議事件

### 亞洲空幹王事件

羅志祥簽唱會過往依循「三拍五抱」模式，買三張唱片的粉絲可以跟羅志祥拍照、買五張的粉絲可以跟羅志祥擁抱，曾有瘋狂歌迷為了要「抱9次」砸錢買45張唱片。不過，羅志祥的經紀公司「天地合娛樂製作有限公司」（下稱「天地合娛樂」）決定2015年將取消這項活動，在[臉書上公告接下來的宣傳和簽名會活動的遊戲規則](../Page/臉書.md "wikilink")，不再沿用舊型態，有歌迷問是否可以跟羅志祥合照，也都是得到同樣「不再沿用舊型態」的回答，讓許多粉絲難過直喊「可不可以不要取消！」。\[31\]
然而，同年11月6日羅志祥在臉書上發文，透露剛接獲好消息，表示新專輯《[真人秀？](../Page/真人秀？.md "wikilink")》預購成績相當好、大賣5萬張，除了感謝粉絲的支持，還針對即將舉辦簽名會而引起的爭議，大方做出回應：「大家這麼支持我，如果再不重視你們的需求，真的太對不起你們了。」承諾會向公司爭取以往的福利，並喊：「只要你們開心，不管別人怎麼說我罵我，我都願意做！」暖心舉動引來大批粉絲大讚「超感動！」。\[32\]
另外，經紀人[陳鎮川透露行銷方式將改變](../Page/陳鎮川.md "wikilink")，也就是取消「三拍五抱」的行銷方式，強調歌迷還是有機會跟小豬拍照，只是不再以張數當規則。\[33\]
但這項「所謂的福利」卻引起爭議，因為羅志祥的唱片幾乎都會一再改版，《[獨一無二](../Page/獨一無二_\(羅志祥專輯\).md "wikilink")》甚至改版達六次之譜，粉絲為了合照或抱抱，就得再花錢買專輯，對於粉絲來說是不小的負擔，本來經紀公司已經打算取消這項制度。\[34\]，一度取消之前簽唱會的「三拍五抱」傳統，但最後羅志祥以「替粉絲們爭取福利」的理由，重新回到「三拍五抱」傳統。\[35\]
對此，[陳沂在臉書寫下](../Page/陳沂_\(主持人\).md "wikilink")：「夠了，我真的好想聊這個喔～講得好像多委屈，都是不忍心讓歌迷失望，所以要對抗公司爭取恢復『三張合照、五張抱抱』！？如果你真的像自己講的那麼愛歌迷的話，那應該只要有來簽名會，都可以拍照跟抱抱啊！三拍五抱根本就是用肉體來斂財，亞洲空幹王祥祥這種行徑跟賣淫小模有什麼兩樣？」\[36\]
。陳沂的猛烈砲火引發網友和歌迷熱論\[37\]。

對於陳沂的批評，「天地合娛樂」發聲明稿，認為陳沂的言論涉及公然侮辱及惡意誹謗羅志祥的名譽，已委託律師蒐證，準備提告。11月12日陳沂表示感謝羅志祥，認為提告的行為，根本就是在幫她衝知名度而已，對羅志祥一點好處也沒有，陳沂於臉書回應：「那就交給法院去認證好了，如果羅志祥想讓我更紅的話。但這樣不就成全他的粉絲說的『我想靠批評羅志祥爆紅』的願望了？」。\[38\]
羅志祥回應：「我沒有強迫任何人，老實說這樣做，真的很累，但我從第一張專輯到現在都這樣做，這是我跟歌迷間的約定，很多人這樣做，為什麼不說別人，卻用放大鏡來檢視我。」經紀人則強調一定會告到底。\[39\]
同月16日陳沂繼續在臉書貼文：「很擔心祥祥也在密謀邀請我去他的演唱會當特別嘉賓，所以我要趕快來練被[空幹舞](../Page/空幹舞.md "wikilink")，呼應祥祥的空幹」。\[40\]

對於陳沂的批評，「天地合娛樂」發表聲明如下：「就近日陳沂小姐於其臉書公開發文，批評本公司旗下藝人羅志祥先生『三拍五抱根本就是用肉體來歛財，亞洲空幹王祥祥這種行徑跟賣淫小模有什麼兩樣？』該內容已明顯涉及公然侮辱及惡意誹謗羅志祥先生之名譽，本公司已委託律師蒐證，將採取法律行動，以保護藝人合法權益。請各媒體自重，勿轉載該等惡意污衊他人之言論，若有媒體不實報導，本公司亦將採取必要行動，以悍衛權益，並端正視聽」。\[41\]
另外，對她提告求償[新台幣](../Page/新台幣.md "wikilink")200萬元並要求登《[聯合報](../Page/聯合報.md "wikilink")》等四大報道歉。\[42\]

2016年1月19日[臺灣臺北地方法院首度開庭](../Page/臺灣臺北地方法院.md "wikilink")，羅志祥和陳沂均未出席，皆由律師代表出庭\[43\]
。在開庭過程中，陳沂委任律師[張鈞綸當庭詢問羅志祥委任的律師對於](../Page/張鈞綸.md "wikilink")[空幹舞的瞭解程度](../Page/空幹舞.md "wikilink")，對方律師則回答不清楚該舞蹈風格；法官決定再以書狀表達意見，訂同年3月8日再開庭。律師張鈞綸表示，「亞洲空幹王」是在網路上已存在的名詞，不應以控告特定對象的方式解決\[44\]
\[45\]\[46\]。經台北地院審理後，法官認為陳沂批評羅志祥「肉體斂財」、「賣淫小模」，屬人身攻擊，已構成侵權行為，但是「亞洲空幹王」無損羅志祥名譽\[47\]\[48\]，最終判賠台幣30萬元並須在四家報社的報紙頭版登報道歉。全案可上訴。陳沂得知判決結果後表示：「上訴到底，誓死捍衛[言論自由](../Page/言論自由.md "wikilink")。」\[49\]，羅志祥委任律師隨即與陳沂雙方達成和解\[50\]，和解內容為陳沂須連續7天在臉書上公開道歉\[51\]。

### 羅志祥國族事件

2016年1月14日羅志祥在[中國大陸出席電影宣傳活動](../Page/中國大陸.md "wikilink")\[52\]，公開宣稱：「不用分那麼細，我們都是中國人」，引發[台灣輿論沸騰](../Page/台灣.md "wikilink")，是為**羅志祥國族事件**\[53\]\[54\]。
羅志祥國族事件引起許多網友不滿，一如「[周子瑜事件](../Page/周子瑜事件.md "wikilink")」的網路影響，羅志祥在[臉書上的粉絲頁讚數急速流失至少](../Page/臉書.md "wikilink")4萬人\[55\]\[56\]\[57\]，直至1月24日後才開始逐漸回升\[58\]。

## 師承關係

若僅依羅志祥和相關藝人於主持領域拜師之承啟關係彙整，關係如下：

  - **師公：**
  - [張菲](../Page/張菲.md "wikilink")

<!-- end list -->

  - **師父輩：**
  - [胡瓜](../Page/胡瓜.md "wikilink")
  - [鄭進一](../Page/鄭進一.md "wikilink")
  - [康康](../Page/康康.md "wikilink")

<!-- end list -->

  - **師兄弟：**
  - [澎恰恰](../Page/澎恰恰.md "wikilink") (師承鄭進一)
  - [許效舜](../Page/許效舜.md "wikilink") (師承鄭進一)
  - [王識賢](../Page/王識賢.md "wikilink") (師承鄭進一)
  - [歐漢聲](../Page/歐漢聲.md "wikilink") (師承胡瓜)
  - [浩角翔起](../Page/浩角翔起.md "wikilink") (師承胡瓜)
  - [瑪莉亞](../Page/葉欣眉.md "wikilink") (師承胡瓜)

<!-- end list -->

  - **徒弟輩：**
  - [九孔](../Page/九孔_\(藝人\).md "wikilink") (師承許效舜)
  - [NONO](../Page/NONO.md "wikilink") (師承許效舜)
  - [白雲](../Page/白雲_\(台灣藝人\).md "wikilink") (師承許效舜)
  - \-{[洪都拉斯](../Page/洪勝德.md "wikilink")}- (師承許效舜)
  - [黃鐙輝](../Page/黃鐙輝.md "wikilink") (師承許效舜)
  - [馬國畢](../Page/馬國畢.md "wikilink") (師承許效舜)
  - [蜆仔](../Page/劉俊峰.md "wikilink") (師承許效舜)
  - [黃鴻升](../Page/黃鴻升.md "wikilink") (師承羅志祥)

<!-- end list -->

  - **徒孫輩：**
  - [曾子余](../Page/曾子余.md "wikilink") (師承黃鐙輝)

## 音樂作品

  - 2003《[Show Time](../Page/Show_Time.md "wikilink")》
  - 2004《[達人Show](../Page/達人Show.md "wikilink")》
  - 2005《[催眠Show](../Page/催眠Show.md "wikilink")》
  - 2006《[Speshow](../Page/Speshow.md "wikilink")》
  - 2007《[舞所不在](../Page/舞所不在.md "wikilink")》
  - 2008《[潮男正傳](../Page/潮男正傳.md "wikilink")》
  - 2009《[羅生門](../Page/羅生門_\(專輯\).md "wikilink")》
  - 2011《[獨一無二](../Page/獨一無二_\(羅志祥專輯\).md "wikilink")》
  - 2012《[有我在](../Page/有我在.md "wikilink")》
  - 2013《[獅子吼](../Page/獅子吼_\(專輯\).md "wikilink")》
  - 2015《[真人秀？](../Page/真人秀？.md "wikilink")》
  - 2019《[No Idea](../Page/No_Idea.md "wikilink")》

## 演唱會

## 影视作品

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
<td><p>頻道</p></td>
<td><p>劇名</p></td>
<td><p>角色</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p><a href="../Page/當我們窩在一起.md" title="wikilink">當我們窩在一起</a></p></td>
<td><p>林明宗</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a></p></td>
<td><p><a href="../Page/少年梁祝.md" title="wikilink">少年梁祝</a></p></td>
<td><p>梁山伯</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華視.md" title="wikilink">華視</a></p></td>
<td><p><a href="../Page/女生向前走.md" title="wikilink">女生向前走</a></p></td>
<td><p>羅嘉西</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/麻辣鮮師.md" title="wikilink">麻辣鮮師</a></p></td>
<td><p>余志祥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a></p></td>
<td><p><a href="../Page/Hi_上班女郎.md" title="wikilink">Hi 上班女郎</a></p></td>
<td><p>鄭達倫</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td><p><a href="../Page/鬥魚_(2004年電視劇).md" title="wikilink">鬥魚II</a></p></td>
<td><p>袁承烈</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a></p></td>
<td><p><a href="../Page/轉角*遇到愛.md" title="wikilink">轉角*遇到愛</a></p></td>
<td><p>秦朗</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a>、<a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td><p><a href="../Page/籃球火.md" title="wikilink">籃球火</a></p></td>
<td><p>元大鷹</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/華視.md" title="wikilink">華視</a>、<a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td><p><a href="../Page/海派甜心.md" title="wikilink">海派甜心</a></p></td>
<td><p>林達浪<br />
薛海</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/湖南衛視.md" title="wikilink">湖南衛視</a></p></td>
<td><p><a href="../Page/一男三女合租記.md" title="wikilink">一男三女合租記</a></p></td>
<td><p>宋小雷</p></td>
</tr>
</tbody>
</table>

### 電影

|                                              |                                                          |                                       |                                       |
| -------------------------------------------- | -------------------------------------------------------- | ------------------------------------- | ------------------------------------- |
| 年份                                           | 片名                                                       | 角色                                    | 備註                                    |
| 1999                                         | [神探兩個半又二分之一之學校有鬼](../Page/神探兩個半又二分之一之學校有鬼.md "wikilink") | 羅志祥                                   | 導演[吳宗憲](../Page/吳宗憲.md "wikilink")    |
| 2001                                         | [蘋果咬一口](../Page/蘋果咬一口.md "wikilink")                     | 周家祥                                   | 導演[朱延平](../Page/朱延平.md "wikilink")    |
| 2013                                         | [西游·降魔篇](../Page/西游·降魔篇.md "wikilink")                   | 空虛公子                                  | 執導、編劇[周星馳](../Page/周星馳.md "wikilink") |
| 2016                                         | [極限挑戰之皇家寶藏](../Page/极限挑战.md "wikilink")                  | 羅志祥                                   |                                       |
| [美人魚](../Page/美人魚_\(2016年電影\).md "wikilink") | 八哥                                                       | 執導、編劇[周星馳](../Page/周星馳.md "wikilink") |                                       |
| 2017                                         | [機器之血](../Page/機器之血.md "wikilink")                       | 李森                                    | 監製[成龍](../Page/成龍.md "wikilink")      |

### 電影配音

|       |                                      |           |
| ----- | ------------------------------------ | --------- |
| 年份    | 片名                                   | 角色        |
| 2005年 | 《[四眼天雞](../Page/四眼天雞.md "wikilink")》 | 雞丁（配音）    |
| 2007年 | 《[蜂電影](../Page/蜂電影.md "wikilink")》   | 貝瑞·班森（配音） |

### 微電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2012年</p></td>
<td><p>把樂帶回家（百事2012賀歲微電影）</p></td>
<td><p>二兒子（歌手）</p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/再一次心跳.md" title="wikilink">再一次心跳</a>（<a href="../Page/有我在.md" title="wikilink">有我在專輯音樂微電影</a>）</p></td>
<td><p>李衛成</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>把樂帶回家2013（百事2013賀歲微電影）</p></td>
<td><p>醫生</p></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/誰是你的菜.md" title="wikilink">誰是你的菜</a>（樂事2013微電影）</p></td>
<td><p>羅公子</p></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p>把樂帶回家2014親情篇（百事2014微電影）<br />
把樂帶回家2014人情篇（百事2014微電影）</p></td>
<td><p>樂店長<br />
小羅</p></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/誰是你的菜.md" title="wikilink">誰是你的菜</a>（樂事2015微電影）</p></td>
<td><p>阿宅</p></td>
</tr>
</tbody>
</table>

### 音乐录影带(MV)

  - 2006 [何潔](../Page/何潔.md "wikilink")《希望》
  - 2016 [楊丞琳](../Page/楊丞琳.md "wikilink")《觀眾》
  - 2016 [安心亞](../Page/安心亞.md "wikilink") 《靚仔》(feat.)
  - 2017 [愷樂](../Page/愷樂.md "wikilink") 《三人行》(feat.)
  - 2018 [玖壹壹](../Page/玖壹壹.md "wikilink") 《我跟你卡好》(feat.)

## 書籍作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>書名</p></th>
<th><p>出版社</p></th>
<th><p>ISBN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2002年9月30日</p></td>
<td><p>《羅志祥豬式會社》</p></td>
<td><p>平裝本</p></td>
<td><p>ISBN 9578033923</p></td>
</tr>
<tr class="even">
<td><p>2006年3月8日</p></td>
<td><p>《我愛大明星—驚嘆號》（與<a href="../Page/蔡依林.md" title="wikilink">蔡依林</a>、<a href="../Page/楊丞琳.md" title="wikilink">楊丞琳</a>、<a href="../Page/黃立行.md" title="wikilink">黃立行合著</a>）</p></td>
<td><p>如何</p></td>
<td><p>ISBN 9861361219</p></td>
</tr>
<tr class="odd">
<td><p>2007年10月10日</p></td>
<td><p>《羅志祥Show On Stage進化三步曲》</p></td>
<td><p>如何</p></td>
<td><p>ISBN 9789861361475</p></td>
</tr>
<tr class="even">
<td><p>2010年12月24日</p></td>
<td><p>《<a href="../Page/羅輯課：24個媽媽教我的街頭智慧.md" title="wikilink">羅輯課：24個媽媽教我的街頭智慧</a>》</p>
<ul>
<li>一個月內在台灣售出8萬2000本，亞洲地區銷量達30萬[59]</li>
<li>榮獲香港「第廿三屆中學生好書龍虎榜」第五名（得票:5843）</li>
</ul></td>
<td><p>平裝本</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>2011年8月5日</p></td>
<td><p>《<a href="../Page/澳漫生活.md" title="wikilink">澳漫生活</a>》</p></td>
<td><p>金牌大風</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>2014年8月8日</p></td>
<td><p>《夢想拼圖》</p></td>
<td><p>平裝本</p></td>
<td><p>ISBN 9789578039186</p></td>
</tr>
</tbody>
</table>

## 節目主持

### 目前主持

| 年份      | 頻道                                   | 節目                                     |
| ------- | ------------------------------------ | -------------------------------------- |
| 2001年至今 | [八大綜合台](../Page/八大綜合台.md "wikilink") | 《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》 |
| 2015年至今 | [東方衛視](../Page/東方衛視.md "wikilink")   | 《[極限挑戰](../Page/極限挑戰.md "wikilink")》   |

### 經歷主持

| 頻道                                            | 節目                                                  |
| --------------------------------------------- | --------------------------------------------------- |
| [東風衛視](../Page/東風衛視.md "wikilink")            | 《冷知識轟趴》                                             |
| 《超人氣大挑戰》                                      |                                                     |
| 《亞洲進行式》                                       |                                                     |
| 《亞洲娛樂中心》                                      |                                                     |
| [台視](../Page/台視.md "wikilink")                | 《綜藝這個禮拜6最好笑》                                        |
| 《旗開得勝》                                        |                                                     |
| 《旗開得勝|旗開得勝少年兵團》                               |                                                     |
| 《綜藝夜總會》                                       |                                                     |
| 《歡樂艦隊》                                        |                                                     |
| 《電視宅急便》                                       |                                                     |
| 《台灣who怕who》                                   |                                                     |
| 《藝形帝國》                                        |                                                     |
| 《歡樂一路發之生存巴士》                                  |                                                     |
| [華視](../Page/華視.md "wikilink")                | 《TV搜查線》                                             |
| 《TV三賤客》                                       |                                                     |
| 《週六大勝利》                                       |                                                     |
| 《綜藝小妹大》                                       |                                                     |
| 《[快樂星期天](../Page/快樂星期天.md "wikilink")》        |                                                     |
| [民視](../Page/民視.md "wikilink")                | 《台灣綜藝王》                                             |
| [大地](../Page/大地.md "wikilink")                | 《超猛樂園》                                              |
| [中天](../Page/中天.md "wikilink")                | 《[康熙來了之康永當家](../Page/康永當家.md "wikilink")》（代班）       |
| [東森](../Page/東森.md "wikilink")                | 《1800娛樂開講》                                          |
| 《娛樂新聞》                                        |                                                     |
| [中視](../Page/中視.md "wikilink")                | 《綜藝95995》                                           |
| 《電視大國民》                                       |                                                     |
| 《[紅白勝利](../Page/紅白勝利.md "wikilink")》          |                                                     |
| [MUCH TV](../Page/年代MUCH台.md "wikilink")      | 《MUCH排行榜》                                           |
| [三立](../Page/三立.md "wikilink")                | 《[完全娛樂](../Page/完全娛樂.md "wikilink")》                |
| [浙江衛視](../Page/浙江衛視.md "wikilink")            | 《[天生是優我](../Page/天生是優我.md "wikilink")》(羅教頭)         |
| [芒果TV](../Page/芒果TV.md "wikilink")            | 《[2017快樂男聲](../Page/2017快樂男聲.md "wikilink")》(音樂召換師) |
| [優酷](../Page/優酷.md "wikilink")                | 《[小手牽小狗](../Page/小手牽小狗.md "wikilink")》(道格叔叔)        |
| 《[這就是街舞](../Page/這就是街舞.md "wikilink")》(修樓梯隊長) |                                                     |
| [騰訊](../Page/騰訊.md "wikilink")                | 《[創造101](../Page/創造101.md "wikilink")》(舞蹈導師)        |
| [優酷](../Page/優酷.md "wikilink")                | 《這就是歌唱對唱季》                                          |

## 廣告代言

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>廣告代言</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2015</p></td>
<td><p>《百事可樂》(亞洲)<br />
《樂事洋芋片》 (大陆+台灣)<br />
《MORRIS K 手錶》 (台灣)<br />
《dico's德克士快餐連鎖店》(大陆)(至二月終)<br />
《與狼共舞 休閒服裝》(大陆)<br />
《<a href="https://www.facebook.com/HorienEyewear/">HORIEN海儷恩眼鏡系列</a>》墨鏡品牌推薦 (亞洲)<br />
《明星三缺一》 網絡遊戲<br />
《Airwaves 香口珠》 (台灣+香港)<br />
《麥當勞》歡樂送(台灣)<br />
《Skin Check》黑面膜<br />
《雅麗潔蘆薈膠》黑面膜</p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p>《台灣觀光局》日本地区觀光代言人<br />
《百事可樂》(亞洲)<br />
《樂事洋芋片》 (中國大陆+台灣)<br />
《MORRIS K 手錶》 (台灣)<br />
《dico's德克士快餐連鎖店》(中國大陆)<br />
《<a href="https://www.facebook.com/HorienEyewear/">HORIEN海儷恩太陽眼鏡</a>》潮流墨鏡 (亞洲)<br />
《飄柔 Rejoice 洗髮精》 (亞洲)<br />
《「I AM HERE」保育熊貓慈善公益計畫》(中國大陆)<br />
《與狼共舞 休閒服裝》(中國大陆)<br />
《Airwaves 香口珠》 (台灣+香港)<br />
《海儷恩隱行眼鏡系列》<br />
《全家便利商店》台灣濁金米(中國大陆)<br />
《現代婦女基金會》公益大使<br />
《2014公益悠遊卡》(台灣)<br />
《麥當勞》歡樂送(台灣)</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>《台灣觀光局》日本地區觀光代言人<br />
《百事可樂》(亞洲)<br />
《樂事洋芋片》 (中國大陆+台灣)<br />
《MORRIS K 手錶》 (台灣)<br />
《Manhattan Portage 曼哈頓郵差包》(中國内地+台灣+香港)<br />
《dico's德克士快餐連鎖店》(中國大陆)<br />
《飄柔 Rejoice 洗髮精》 (亞洲)<br />
《與狼共舞 休閒服裝》(中國大陆)<br />
《HORIEN海儷恩隱行眼鏡系列》(中國大陆)<br />
《伊利冰品》 (中國大陆)<br />
《中國夢之聲》 招募大使<br />
《騰訊微信》 (台灣、新加坡、香港)<br />
《七色花女性飾品》 (中國大陆)<br />
《自然堂 男性護膚品》(中國大陆)<br />
《麥當勞快餐連鎖店》麥克瘋大麥克 (台灣)<br />
《雅詩蘭黛》2013粉紅絲帶乳癌防治公益活動名人(台灣)<br />
《Airwaves 香口珠》 (台灣+香港)<br />
《台灣之心愛護動物協會》保護動物愛心大使 (台灣)<br />
《蘋果日報》即時新聞APP (台灣)</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p>《百事可樂》<br />
《飄柔 Rejoice 洗髮精》<br />
《dico's德克士快餐連鎖店》(中國大陆)<br />
《自然堂(中國大陆)》<br />
《SoMi 冰淇淋(中國大陆)》<br />
《巧樂滋雪糕(中國大陆)》<br />
《海昌海儷恩隱形眼鏡和護理液》(中國大陆)<br />
《屈臣氏》<br />
《樂事洋芋片》<br />
《美國棉2012年年度代言人》棉花大使<br />
《M.A.C VIVA GLAM愛滋唇膏義賣活動》台灣公益大使<br />
《御茶園 嫩芽茶》《御茶園 特調系茶飲料》<br />
《Manhattan Portage 曼哈頓郵差包》<br />
《MORRIS K》手錶代言<br />
《與狼共舞服飾(中國大陆)》<br />
《澳洲旅遊大使》<br />
《七色花(中國大陆)》<br />
《台灣7-ELEVEN》OPEN小將占卜喀擦筆<br />
《羅慧夫顱顏基金會》2012國際園丁活動代言人（無酬）<br />
《BVLGARI》Save the Children慈善銀戒宣傳照<br />
《花蓮黎明教養院》喜樂園代言人（無酬）<br />
《台灣7-ELEVEN》關東煮麻辣鍋<br />
《騰訊微信(台灣)》</p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>《Manhattan Portage 曼哈頓郵差包》<br />
《全家魔幻迪士尼》<br />
《李時珍長大人》<br />
《dico's德克士快餐連鎖店》<br />
《MORRIS K》手錶代言<br />
《全家超麵包》<br />
《樂事洋芋片》<br />
《BIG TRAIN大列車系列廣告》<br />
《與狼共舞服飾(中國大陆)》<br />
《巧樂滋雪糕(中國大陆)》<br />
《SoMi 冰淇淋(中國大陆)》<br />
《七色花(中國大陆)》<br />
《飄柔 Rejoice 洗髮精》<br />
《百事可樂》<br />
《屈臣氏》<br />
《善待動物組織PETA公益廣告》(領養狗隻，拒絶購買)<br />
《澳洲旅遊大使》</p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>《與狼共舞服飾(中國大陆)》<br />
《全家超麵包》<br />
《巧樂滋雪糕》<br />
《樂事MAX波樂洋芋片》<br />
《BigTrain牛仔褲系列廣告》<br />
《美克運動鞋》<br />
《內地『康師傅』香辣牛肉麵廣告》<br />
《伊利冰品(中國大陆)》<br />
《百事可樂(香港+中國内地)》<br />
《Manhattan Portage 曼哈頓郵差包》<br />
《七色花》<br />
《TVBS關懷台灣文教基金會》<br />
《屈臣氏》</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>《台北Fun house》房地產<br />
《MORRIS K》手錶代言<br />
《熱力排球》網遊<br />
《愛盲活動代言人》<br />
《森馬服飾廣告 青春潮我看》<br />
《百事可樂 夏季群星廣告》<br />
《樂事MAX波樂洋芋片》<br />
《屈臣氏 衛生棉》<br />
《BigTrain牛仔褲》<br />
《麥當勞 麥樂雞廣告》<br />
《麥當勞開年廣告》<br />
《百事可樂開年廣告》<br />
《飄柔 Rejoice 洗髮精》<br />
《中國大陆『康師傅』香辣牛肉麵》</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>《Rejoice水潤》<br />
《飛柔洗髮精》<br />
《麥當勞 搖搖薯條》<br />
《三陽SYM機車》<br />
《屈臣氏 寵i會員卡》<br />
《第四屆 捷運盃舞蹈大賽》<br />
《台灣啤酒》<br />
《BIG TRAIN牛仔褲》<br />
《百事可樂》<br />
《樂事洋芋片》<br />
《桂格養氣人參雞精》<br />
《飄柔REJOICE洗髮水》<br />
《森馬服飾》<br />
《愛盲基金》</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p>《BIG TRAIN牛仔褲》<br />
《BENQ 手機》<br />
《「御茶園」飲品》<br />
《「百事新益代豆奶」飲品》<br />
《黑松沙士》<br />
《寶島眼鏡》<br />
《愛唱久久 ISing99》<br />
《Yes 123 求職網》<br />
《樺達Q涼糖》<br />
《樂事洋芋片》<br />
《香港紅十字會輸血服務中心 公益捐血廣告》<br />
《「清茶館」飲品》<br />
《麥當勞慈善基金》(世界兒童日愛心大使)<br />
《自由求職網廣告》<br />
《美國棉》</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>《屈臣氏衛生棉》<br />
《子宮頸癌預防大使》<br />
《麥當勞「大麥克」》<br />
《「御茶園」飲品》<br />
《BIG TRAIN牛仔褲》<br />
《BENQ 手機》</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p>《格億奴》(中國大陆)<br />
《高爾夫線上運動遊戲「PangYa魔法飛球」》<br />
《麥當勞慈善基金》(世界兒童日愛心大使)</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>《美克運動鞋》(中國大陆)<br />
《兒福中心愛心大使》</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p>《台南市政府2000反毒大使》<br />
《康是美聯合勸募活動》</p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p>《農林廳鮮乳標章》<br />
《台北市政府「天然氣公車」活動代言人》<br />
《佐媚朵爾「酒沒有」解酒液代言人》</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p>《阿奇儂雪糕》平面廣告<br />
《羅慧夫顱顏基金會》(用愛彌補公益代言人)<br />
《淨化海灘環保活動》環保小天使<br />
《法務部更新保護會》(預防青少年犯罪活動法律小戰士)<br />
《CHA CHA 50機車》 / 《CHA CHA 90機車》<br />
《蓋奇巧克力》<br />
《金雙氧隱形眼鏡清潔液》<br />
《乘機車戴安全帽》之交通大使</p></td>
</tr>
</tbody>
</table>

## 荣誉

### 個人紀錄

1.  第一位登上[台北小巨蛋開演唱會的歌手](../Page/台北小巨蛋.md "wikilink")（2005年11月18日）
2.  第一位於[紐約](../Page/紐約.md "wikilink")實地拍攝音樂錄影帶的亞洲歌手（《愛的主場秀》MV）\[60\]\[61\]
3.  第一位於[台北小巨蛋舉辦](../Page/台北小巨蛋.md "wikilink")24小時三場演唱會的歌手（2010年5月15日-2010年5月16日）
4.  第一位開「裸視3D」演唱會的藝人。（「2010曼哈頓舞法舞天3D WORLD LIVE TOUR」）
5.  第一位拍3D MV的華語歌手。（《舞法舞天》MV）
6.  銷售榜榜單（【玫瑰大眾 風雲榜】【五大金榜】
    【佳佳金榜】）[冠軍蟬聯達最多次](../Page/冠軍.md "wikilink")(連續10週冠軍)，羅志祥的《[羅生門](../Page/羅生門_\(專輯\).md "wikilink")》蟬聯記錄平了[蔡依林](../Page/蔡依林.md "wikilink")《[看我72變](../Page/看我72變.md "wikilink")》的冠軍蟬聯數。
7.  臺灣專輯改版最多次紀錄，9次。（《[獨一無二](../Page/獨一無二.md "wikilink")》專輯）
8.  第一位在台灣辦簽唱活動長達15小時的歌手\[62\]（《[有我在](../Page/有我在.md "wikilink")》專輯）
9.  在[G-music風雲榜上創下史上銷售最高的百分比](../Page/玫瑰大眾唱片.md "wikilink")，百分之80。（《[獅子吼](../Page/獅子吼.md "wikilink")》專輯）\[63\]
10. 在[五大唱片上創下史上銷售最高的百分比](../Page/五大唱片.md "wikilink")，百分之81.58。（《[有我在](../Page/有我在.md "wikilink")》專輯）\[64\]
11. [台湾唱片出版事業基金會認證](../Page/台灣唱片出版事業基金會.md "wikilink")
    -《[羅生門](../Page/羅生門_\(專輯\).md "wikilink")》專輯榮登2010年銷售總冠軍(五[白金](../Page/音樂唱片銷售認證.md "wikilink")-154,218張)成為繼劉德華之後,
    近10年來第1個主動認證的歌手\[65\]
12. 「舞法舞天」一萬零一夜新加坡演唱會破場館有史以來人數最多的記錄。（2011年5月14日）
13. 第一位在日本最大數位音樂內容提供商：[RecoChoku獲得鈴聲下載排行榜冠軍](../Page/RecoChoku.md "wikilink")（歌曲:[Dante](../Page/Dante_\(單曲\).md "wikilink")）的台灣地区歌手（2012年1月31日）\[66\]，且第二張日文單曲主打歌Magic同樣在該排名榜獲得兩天冠軍（2012年6月5日-2012年6月6日），為台灣地区第一人。\[67\]\[68\]
14. 第一位攻上日本[Oricon公信榜](../Page/Oricon公信榜.md "wikilink")[單曲週榜前十名的台灣地区男歌手](../Page/單曲.md "wikilink")。（《[Dante](../Page/Dante_\(單曲\).md "wikilink")》單曲，推定売上枚数：7395）\[69\]（2012年2月13日-2012年2月19日）
15. 第一位同時在[Google+與](../Page/Google+.md "wikilink")[YouTube開播個人影音頻道的華人歌手](../Page/YouTube.md "wikilink")，Google並砸百萬元在台灣總部為羅志祥打造專屬攝影棚\[70\]（2012年3月19日）
16. 第一位在[亞洲地區使用](../Page/亞洲.md "wikilink")[google hangout on
    air的藝人](../Page/google_hangout_on_air.md "wikilink")\[71\]（2012年5月12日）
17. 以400新台幣，創下[台北小巨蛋的演唱會最低門票價格紀錄](../Page/台北小巨蛋.md "wikilink")\[72\]（《舞極限
    Over The Limit》世界巡迴演唱會《台北場》）（2013年1月4日至1月6日）
18. 第一位借用真實坦克，並應用於MV拍攝的華人歌手（《舞極限》MV）\[73\]
19. 其YouTube個人影音頻道為2012年台灣及香港地區的十大熱門頻道\[74\]\[75\]
20. 第一位在台北小巨蛋作360度旋轉飛行環繞場館表演的歌手（《舞極限 Over The
    Limit》世界巡迴演唱會《台北場》）（2013年1月4日至1月6日）\[76\]
21. 唯一受邀於[YouTube](../Page/YouTube.md "wikilink") Music
    Awards亞洲首場演唱會表演的華語歌手（2013年11月3日）\[77\]
22. 第一位與漫畫界人士（[李崇萍](../Page/李崇萍.md "wikilink")）合作，打造MV番外篇的華語樂壇男歌手（《未完的承諾》MV）\[78\]
23. 第一位蟬聯台灣唱片銷售4連霸的華語樂壇歌手\[79\]

## 註釋

## 參考文献

## 外部連結

  -
  -
  -
  -
  - [Pony Canyon 波麗佳音 (羅志祥的日本唱片公司官網)](http://show.ponycanyon.co.jp/)

  - [羅志祥日本官方部落格](http://ameblo.jp/showlo-blog/)

  -
  -
  -
  -
  -
[L](../Category/臺灣原住民混血兒.md "wikilink")
[L羅](../Category/台灣前兒童歌手.md "wikilink")
[L羅](../Category/阿美族人.md "wikilink")
[L羅](../Category/台灣綜藝節目主持人.md "wikilink")
[L羅](../Category/台灣電視主持人.md "wikilink")
[L羅](../Category/華語流行音樂歌手.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[L羅](../Category/六燈獎.md "wikilink")
[L](../Category/索尼音樂娛樂旗下藝人.md "wikilink")
[Category:全球華語歌曲排行榜最受歡迎男歌手](../Category/全球華語歌曲排行榜最受歡迎男歌手.md "wikilink")
[Category:模仿藝人](../Category/模仿藝人.md "wikilink")
[Category:台湾佛教徒](../Category/台湾佛教徒.md "wikilink")
[\*](../Category/羅志祥.md "wikilink") [L羅](../Category/花蓮人.md "wikilink")
[L羅](../Category/羅姓.md "wikilink")
[Category:MTV亞洲大獎獲得者](../Category/MTV亞洲大獎獲得者.md "wikilink")
[Category:金鐘獎綜藝節目主持人獲得者](../Category/金鐘獎綜藝節目主持人獲得者.md "wikilink")
[L羅](../Category/台灣原住民歌手.md "wikilink")

1.  [「亞洲舞王」羅志祥第7張個人專輯「羅生門」
    銷售勇奪2010台灣銷售總冠軍！](http://ent.msn.com.tw/news/content.aspx?sn=1012140014)
    金牌大風2010/12/14

2.  [Play Music
    音樂網 2011年度專輯銷售榜單](http://www.playmusic.tw/images/billboard/2011/2010_topsellabum.jpg)


3.

4.  [Play Music
    音樂網 2011年度專輯銷售榜單](http://www.playmusic.tw/webvote/main_vote/2012/2011_best_selling_album.php)


5.  [羅志祥蔡依林2012冠亞軍](http://showbiz.chinatimes.com/showbiz/110511/112013011400025.html)


6.  [2013台灣年度唱片銷量榜
    -台灣音樂風雲榜](http://ww4.sinaimg.cn/large/65168a61jw1ecgs54g6tnj20lo0zkdp6.jpg)，2014年01月12日
    (日) 15:15 (UTC+8)查閱

7.

8.

9.  [末日戰士舞太激烈小豬髕骨軟化症復發](http://tw.music.yahoo.com/hot/news/article.html?aid=4283)

10.

11.

12.

13.

14.

15. [羅志祥：我不是小豬，我要當打不死的小強](https://www.parenting.com.tw/article/5032270-%E7%BE%85%E5%BF%97%E7%A5%A5%EF%BC%9A%E6%88%91%E4%B8%8D%E6%98%AF%E5%B0%8F%E8%B1%AC%EF%BC%8C%E6%88%91%E8%A6%81%E7%95%B6%E6%89%93%E4%B8%8D%E6%AD%BB%E7%9A%84%E5%B0%8F%E5%BC%B7/?page=6/)，來源：[Cheers快樂工作人雜誌](../Page/Cheers快樂工作人雜誌.md "wikilink")，親子天下，2011-03-01

16.

17. [台灣蘋果日報 - 2012-09-17 小豬自斷日本錢途
    宣傳喊卡損失千萬](http://www.appledaily.com.tw/appledaily/article/entertainment/20120917/34513782)

18. [小豬《舞極限》深入軍區拍攝 坦克車震撼入鏡](http://www.wownews.tw/article.php?sn=4276)
    2012年 11月 06日 WOWNEWS

19. [小豬大豹發
    三連霸唱銷王](http://showbiz.chinatimes.com/showbiz/110511/112013011000013.html)
     2013-01-10 中國時報

20.

21.

22.

23. [羅志祥『獅子吼』專輯介紹](http://www.iwant-radio.com/showinformation.php?sisn=32301)，銀河網路電台（2016年12月18日
    22:30查閱）

24. [羅志祥寫「惜命命」代替爸疼惜媽媽](http://video.chinatimes.com/video-cate-cnt.aspx?cid=5&nid=114357)

25. [羅志祥吼破華語樂壇第1人　蟬連台灣唱片銷售4連霸　獲頒4白金唱片
    -PlayMusic新聞中心](http://www.playmusic.tw/column_info.php?id=2168&type=news)，2013年12月5日
    (四) 23:05 (UTC+8)查閱

26.
27. 各榜單詳細排名及銷售百分比，請參見相關條目

28. [羅志祥：我對舞台的渴望，強烈到可以放棄一切](https://www.businesstoday.com.tw/article-content-92748-105439/)，[今週刊](../Page/今週刊.md "wikilink")，2014-01-30

29. [蝴蝶姐姐改名「愷樂」
    羅志祥逼減肥](http://udn.com/NEWS/ENTERTAINMENT/ENT2/8661671.shtml)


30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50. [【黑心律師傻眼】小豬竟跟陳沂這樣和解！](https://tw.appledaily.com/new/realtime/20160902/941004/)

51. [陳沂再PO「澄清文」打臉吳宗憲　羅志祥躺著也中槍](https://www.nownews.com/news/20180521/2757920/)，[NOWnews
    今日新聞](../Page/NOWnews_今日新聞.md "wikilink")，2018-05-21

52.  娛樂星光雲 {{\!}}
    ETtoday東森新聞雲|accessdate=2016-06-03|work=娛樂星光雲|language=zh-TW}}

53.

54.

55.

56.

57.

58.

59. [小豬遭取金2億腿軟-準備空白支票
    豬媽獅子大開口](http://www.libertytimes.com.tw/2011/new/jan/2/today-show2.htm),自由電子報

60.

61.

62. [“亞洲舞王”羅志祥第9張專輯《有我在》北京慶功
    金牌大風助其繼續領跑2012年](http://video.libertytimes.com.tw/article.php?id=10477)


63. [羅志祥
    教學童與動物做朋友](http://udn.com/NEWS/ENTERTAINMENT/ENT2/8298909.shtml?ch=rss_starsfocus)

64. [羅志祥《有我在》15萬張
    勇奪三連霸年度唱銷王](http://tw.photo.chinayes.com/Gallery/201211201701534616752)
     YES娛樂2012-11-20 17:02

65.
66. [「亞洲舞王」羅志祥 2012持續為華人再爭光
    推出首張日文單曲「DANTE」2/15發行首日銷售拿下公信榜日榜第5名](http://video.libertytimes.com.tw/article.php?id=10195)

67.

68.

69. [繼鄧麗君以來睽違25年
    羅志祥成為首度登上日本公信榜TOP10的台灣男歌手](http://video.libertytimes.com.tw/article.php?id=10238)

70.

71.

72. 蘋果.即時新聞
    [小豬小巨蛋票價4百起跳](http://www.appledaily.com.tw/realtimenews/article/entertainment/20121023/148251)
    2012年10月23日01:02

73.
74. [2012年 YouTube
    回顧，十大熱門影片(台灣)](http://www.youtube.com/user/theyearinreviewTW)
    Youtube.com

75. [2012年 YouTube
    回顧，十大熱門影片(香港)](http://www.youtube.com/user/theyearinreviewHK)
    Youtube.com

76. [羅志祥小巨蛋上演「空中飛人」
    短短3分鐘狂燒120萬](http://www.wownews.tw/article.php?sn=5271)
    WoWNews 2013年01月06日

77. [女神舒淇熱貼獻吻　羅志祥卯死頻冒汗](http://www.nownews.com/n/2013/11/05/1010777)
    NOWnews 2013年11月05日

78. [羅志祥攜手羅媽任愛心大使　徒手救狗流滿鮮血](http://www.nownews.com/n/2013/11/16/1021621)
    NOWnews 2013年11月16日

79.