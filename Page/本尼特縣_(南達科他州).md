**本尼特縣**（**Bennett County, South
Dakota**）是[美國](../Page/美國.md "wikilink")[南達科他州南部的一個縣](../Page/南達科他州.md "wikilink")，南鄰[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")。面積3,084平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口3,574人。縣治[馬丁](../Page/馬丁_\(南達科他州\).md "wikilink")（Martin）。

成立於1909年3月9日，縣政府成立於1912年4月27日。縣名紀念[達科他準州最高法院法官](../Page/達科他準州.md "wikilink")、[國會代表格蘭維爾](../Page/國會代表.md "wikilink")·G·本尼特。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/南達科他州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.