《**來自地球的人**》（
又译：**这个男人来自地球**、**地球不死人**）是一部2007年出品的[獨立電影](../Page/獨立電影.md "wikilink")。由[Richard
Schenkman執導](../Page/Richard_Schenkman.md "wikilink")，[Jerome
Bixby編劇](../Page/Jerome_Bixby.md "wikilink")。本片製作人Eric D.
Wilkinson前所未有地公開向那些沒有需明確許可而使用[P2P軟件](../Page/P2P.md "wikilink")（如[BitTorrent](../Page/BitTorrent.md "wikilink")）來分發電影的用戶表示感謝，因為此舉提升了觀者對影片的關注度，而不僅僅在於對本片經濟上的收益。\[1\]

本片在[IMDB上獲得了](../Page/IMDB.md "wikilink")8.0的高分（截至2013年12月5日）。但收看後觀眾評價褒貶不一，有人大失所望——認為其全片場景不多，大量對白；其他人大呼過癮——認為其跳出近幾年[科幻電影](../Page/科幻電影.md "wikilink")「激光加金屬」的圈子，更加關注精神上，而非視覺上的科幻。

## 故事梗概

歷史學教授約翰·歐德曼（John
Oldman）令人意外地從大學辭職後，他的同事們來到他的家中為他舉辦告別聚會，並希望教授解釋他為何離開。教授聲稱他是一個不會衰老的[克洛曼儂人](../Page/克羅馬儂人.md "wikilink")，到目前為止已經活了一萬四千多年。他之所以辭職選擇離開，是因為他若在一個地方停留超過十年，他不衰老這一事實就會引起那裡人們的懷疑。眾人有些惱怒，爭相想拆穿這個蹩腳的藉口，可很快他們就發現無法為約翰所言證偽，就如同約翰無法為他所聲稱的事情證實一般。原本友好的歡送會開始變得氣氛迥異……\[2\]

## 演員表

  - [John Billingsley](../Page/John_Billingsley.md "wikilink") 飾演
    "Harry"
  - [Ellen Crawford](../Page/Ellen_Crawford.md "wikilink") 飾演 "Edith"
  - [William Katt](../Page/William_Katt.md "wikilink") 飾演 "Art"
  - [Annika Peterson](../Page/Annika_Peterson.md "wikilink") 飾演 "Sandy"
  - [Richard Riehle](../Page/Richard_Riehle.md "wikilink") 飾演 "Gruber"
  - [David Lee Smith](../Page/David_Lee_Smith.md "wikilink") 飾演 "John"
  - [Alexis Thorpe](../Page/Alexis_Thorpe.md "wikilink") 飾演 "Linda"
  - [Tony Todd](../Page/Tony_Todd.md "wikilink") 飾演 "Dan"

## 音樂

All music performed by Mark Hinton Stewart

"[第七號交響曲 - 第二章節](../Page/第七號交響曲_\(貝多芬\).md "wikilink")"

  - 作者[路德維希·凡·貝多芬](../Page/路德維希·凡·貝多芬.md "wikilink")

"Forever"

  - 詞：Richard Schenkman
  - 曲：Mark Hinton Stewart
  - Performed by Mark Hinton Steward 和 Chantelle Duncan
  - Copyright - BDI Music LTD.

## 引用

## 外部連結

  - [官方站點](http://www.manfromearth.com/)

  -
  -
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:獨立電影](../Category/獨立電影.md "wikilink")
[Category:科幻片](../Category/科幻片.md "wikilink")
[Category:長生不老題材作品](../Category/長生不老題材作品.md "wikilink")

1.  <http://www.rlslog.net/piracy-isnt-that-bad-and-they-know-it/>
2.