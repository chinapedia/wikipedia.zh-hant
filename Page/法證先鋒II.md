《**法證先鋒II**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司拍攝製作的時裝查案](../Page/電視廣播有限公司.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，[法證先鋒系列第二部](../Page/法證先鋒系列.md "wikilink")，全劇共30集，由[歐陽震華](../Page/歐陽震華.md "wikilink")、[林文龍](../Page/林文龍.md "wikilink")、[鄭嘉穎](../Page/鄭嘉穎.md "wikilink")、[佘詩曼及](../Page/佘詩曼.md "wikilink")[蒙嘉慧領銜主演](../Page/蒙嘉慧.md "wikilink")，由[鍾嘉欣特別演出](../Page/鍾嘉欣.md "wikilink")，並由[曹永廉及](../Page/曹永廉.md "wikilink")[郭少芸聯合主演](../Page/郭少芸.md "wikilink")，監製[梅小青](../Page/梅小青.md "wikilink")。

## 演員表

### 法證部

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong>高彥博</strong></p></td>
<td><p><strong>Tim Sir、阿博、Tim</strong><br />
高級化驗師<br />
楊逸昇、林汀汀、莫淑媛等之上司<br />
林沛沛之舊情人<br />
古澤琛之前姊夫<br />
古泽瑶之前夫<br />
高通之子<br />
梁小柔之丈夫<br />
梁興隆之女婿<br />
梁小剛之姊夫兼上司</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/林文龍.md" title="wikilink">林文龍</a></strong></p></td>
<td><p><strong>古澤琛</strong></p></td>
<td><p><strong>Sam、Dr. Koo</strong><br />
原为小混混，并与杨逸昇合称“慈云山双龙”，后醒悟而不做小混混<br />
法醫<br />
推理小說作家（筆名<strong>古采尼</strong>）<br />
高彦博之前舅仔<br />
古泽瑶之弟<br />
楊逸昇、馬幗英、梁小柔之好友<br />
林汀汀之夫<br />
林沛沛、许立仁之好友<br />
李蕎之師兄<br />
于1993年7月20日因在慈云山配水库与杨逸昇殴打周长发而被误以为杀害周长发之凶手，后因冯添做证人而消除杀害周长发之嫌疑<br />
青年由<a href="../Page/梁証嘉.md" title="wikilink">梁証嘉飾演</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鄭嘉穎.md" title="wikilink">鄭嘉穎</a></strong></p></td>
<td><p><strong>楊逸昇</strong></p></td>
<td><p><strong>Ivan</strong><br />
原为小混混，并与古泽琛合称“慈云山双龙”，后受其姑姑感化而不做小混混<br />
科學鑑證主任S.E.O<br />
前英國爆炸品處理組拆彈專家<br />
高彥博之下屬<br />
古澤琛之好友<br />
楊秀娟之姪<br />
方妙娜之表哥<br />
馬幗英之夫<br />
於第30集被卓嵐放計時炸彈炸至重傷昏迷，及後甦醒<br />
于1993年7月20日因在慈云山配水库与古泽琛殴打周长发而被误以为杀害周长发之凶手，后因冯添做证人而消除杀害周长发之嫌疑<br />
青年由<a href="../Page/蔡曜力.md" title="wikilink">蔡曜力飾演</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/郭少芸.md" title="wikilink">郭少芸</a></strong></p></td>
<td><p><strong>莫淑媛</strong></p></td>
<td><p><strong>Yvonne</strong><br />
法證部科學鑑證主任S.E.O<br />
高彥博之下屬<br />
沈雄之女友</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鍾嘉欣.md" title="wikilink">鍾嘉欣</a></strong></p></td>
<td><p><strong>林汀汀</strong></p></td>
<td><p><strong>汀汀</strong><br />
化驗師<br />
高彥博之下屬<br />
林沛沛之妹<br />
古澤琛之妻<br />
許立仁之小姨<br />
於第4集被炸彈炸至重傷，搶救後最終身亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭俊弘.md" title="wikilink">鄭俊弘</a></p></td>
<td><p>梁小剛</p></td>
<td><p><strong>Fred、剛仔</strong><br />
法證部技術員<br />
梁興隆之子<br />
梁小柔之弟<br />
高彥博之舅仔兼下屬<br />
參見<a href="../Page/#梁家.md" title="wikilink"><strong>梁家</strong></a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳智燊.md" title="wikilink">陳智燊</a></p></td>
<td><p>王永民</p></td>
<td><p><strong>Man</strong><br />
法證部化驗師<br />
高彥博之下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/河國榮.md" title="wikilink">河國榮</a></p></td>
<td><p>歐紹基</p></td>
<td><p><strong>歐Sir</strong><br />
爆炸品處理组負責人<br />
拆彈專家<br />
高彥博之同事</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙靜儀.md" title="wikilink">趙靜儀</a></p></td>
<td><p>陳敏如</p></td>
<td><p><strong>Tina</strong><br />
法證部化驗師（化學組，負責DNA分析）<br />
高彥博之下屬</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張松枝.md" title="wikilink">張松枝</a></p></td>
<td><p>劉國明</p></td>
<td><p><strong>Jeff</strong><br />
法證部化驗師（生物組）<br />
高彥博之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁健平.md" title="wikilink">梁健平</a></p></td>
<td><p>馬建生</p></td>
<td><p><strong>Victor</strong><br />
法證事務部《物理組》化驗師<br />
高彥博下屬</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>唐禮賢</p></td>
<td><p>法證事務部《物理組》化驗師<br />
高彥博下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湯展雄.md" title="wikilink">湯展雄</a></p></td>
<td><p>馬自達</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧永健.md" title="wikilink">鄧永健</a></p></td>
<td><p>張　強</p></td>
<td><p>筆跡鑑證技術員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳幸美.md" title="wikilink">吳幸美</a></p></td>
<td><p>余素心|-{余}-素心</p></td>
<td><p><strong>Kelly</strong><br />
技術員</p></td>
</tr>
</tbody>
</table>

### 西九龙重案組

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>梁小柔</strong></p></td>
<td><p><strong>Nicole、Madam Leung、小柔</strong><br />
高級督察→刑事情報科高級督察<br />
梁興隆之女<br />
梁小剛之姊<br />
沈雄、凌心怡、劉俊碩、程偉勝前上司<br />
高彥博之妻<br />
與馬幗英先敵後友<br />
於第4集被炸彈炸至右耳失聰，右手神經線受損報廢<br />
於第17集出國进修<br />
於第27集完成進修犯罪心理學，回歸香港</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></strong></p></td>
<td><p><strong>馬幗英</strong></p></td>
<td><p><strong>Bell、Madam Ma、英國馬</strong><br />
毒品調查科高級督察→高級督察<br />
馬錦濤、鄭麗玲之女<br />
馬國宏之姊<br />
郭綺芬之繼女兼天敵，後和好<br />
楊逸昇之妻<br />
古澤琛之好友，並暗戀其<br />
與梁小柔先敵後友<br />
沈雄、凌心怡、莫正康、劉俊碩、程偉勝之上司<br />
方妙娜之偶像<br />
李蕎之天敵<br />
於第30集懷孕</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/曹永廉.md" title="wikilink">曹永廉</a></strong></p></td>
<td><p><strong>沈　雄</strong></p></td>
<td><p><strong>阿沈</strong><br />
警長<br />
梁小柔之前下屬<br />
馬幗英之下屬，曾一度不滿其貪大喜功，後和好<br />
凌心怡、莫正康、劉俊碩、程偉勝之上司<br />
莫淑媛之男友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊秀惠.md" title="wikilink">楊秀惠</a></p></td>
<td><p>凌心怡</p></td>
<td><p><strong>Josie</strong><br />
警员<br />
梁小柔之前下屬<br />
馬幗英之下属</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高鈞賢.md" title="wikilink">高鈞賢</a></p></td>
<td><p>莫正康</p></td>
<td><p><strong>Wilson</strong><br />
警员<br />
毒品調查科警員→重案組警員<br />
馬幗英之下屬<br />
方妙娜之男友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁烈唯.md" title="wikilink">梁烈唯</a></p></td>
<td><p>程偉勝</p></td>
<td><p><strong>Edwin</strong><br />
警员<br />
梁小柔之前下屬<br />
馬幗英之下属</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李雨陽.md" title="wikilink">李雨陽</a></p></td>
<td><p>劉俊碩</p></td>
<td><p><strong>Terence、碩仔</strong><br />
警员<br />
梁小柔之前下屬<br />
馬幗英之下属</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐瑞偉.md" title="wikilink">歐瑞偉</a></p></td>
<td><p>黃卓堅</p></td>
<td><p>B Team 高級督察</p></td>
</tr>
</tbody>
</table>

### 高家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/谷峰.md" title="wikilink">谷　峰</a></p></td>
<td><p>高　通</p></td>
<td><p>高彥博之父<br />
梁小柔、古澤瑤之老爺</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong>高彥博</strong></p></td>
<td><p><strong>Tim</strong><br />
高通之子<br />
古澤瑤之夫<br />
梁小柔之男友，於大結局結婚<br />
參見<strong><a href="../Page/#法證部.md" title="wikilink">法證部</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉錦玲.md" title="wikilink">劉錦玲</a></p></td>
<td><p>古澤瑤</p></td>
<td><p>高彥博之亡妻<br />
古澤琛之家姊<br />
高通之媳婦<br />
已去世<br />
参见<strong><a href="../Page/法证先锋.md" title="wikilink">法证先锋</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>梁小柔</strong></p></td>
<td><p><strong>Nicole</strong><br />
高通之媳<br />
高彥博之女友，於大結局結婚<br />
參見<strong><a href="../Page/#重案組.md" title="wikilink">重案組</a></strong></p></td>
</tr>
</tbody>
</table>

### 古家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉錦玲.md" title="wikilink">劉錦玲</a></p></td>
<td><p>古澤瑤</p></td>
<td><p>高彥博之亡妻<br />
古澤琛之家姊<br />
已去世</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/林文龍.md" title="wikilink">林文龍</a></strong></p></td>
<td><p><strong>古澤琛</strong></p></td>
<td><p><strong>Sam、Dr.Koo</strong><br />
法醫<br />
推理小說作家（筆名<strong>古采尼</strong>）<br />
林汀汀之夫<br />
楊逸昇之好友<br />
馬幗英之好友兼暗戀對象<br />
古澤瑤之弟<br />
高彥博之前舅仔<br />
《最後一块骨头》作者<br />
於第30集出現的巢沛麗而令他回憶跟林汀汀的片段，所以對其有特別的感覺<br />
青年由<a href="../Page/梁証嘉.md" title="wikilink">梁証嘉飾演</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鍾嘉欣.md" title="wikilink">鍾嘉欣</a></strong></p></td>
<td><p><strong>林汀汀</strong></p></td>
<td><p>化驗師<br />
高彥博之下屬<br />
林沛沛之妹<br />
古澤琛之妻<br />
許立仁之小姨<br />
於第4集為救梁小柔而被炸彈炸至重傷，搶救後最終身亡</p></td>
</tr>
</tbody>
</table>

### 梁家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/于洋_(藝人).md" title="wikilink">-{于}-　洋</a></p></td>
<td><p>梁興隆</p></td>
<td><p>梁小柔、梁小剛之父<br />
高彥博之岳父</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>梁小柔</strong></p></td>
<td><p><strong>Nicole、Madam Leung</strong><br />
梁興隆之女<br />
梁小剛之姊<br />
參見<strong><a href="../Page/#重案組.md" title="wikilink">重案組</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭俊弘.md" title="wikilink">鄭俊弘</a></p></td>
<td><p>梁小剛</p></td>
<td><p><strong>Fred</strong><br />
梁興隆之子<br />
梁小柔之弟<br />
參見<strong><a href="../Page/#法證部.md" title="wikilink">法證部</a></strong></p></td>
</tr>
</tbody>
</table>

### 楊家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/呂珊.md" title="wikilink">呂　珊</a></strong></p></td>
<td><p><strong>楊秀娟</strong></p></td>
<td><p><strong>Connie</strong><br />
方妙娜之母<br />
楊逸昇之姑姐</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鄭嘉穎.md" title="wikilink">鄭嘉穎</a></strong></p></td>
<td><p><strong>楊逸昇</strong></p></td>
<td><p><strong>Ivan</strong><br />
楊秀娟之侄<br />
方妙娜之表哥<br />
參見<strong><a href="../Page/#法證部.md" title="wikilink">法證部</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></strong></p></td>
<td><p><strong>馬幗英</strong></p></td>
<td><p><strong>Bell、Madam Ma</strong><br />
毒品調查科情报组高級督察→重案組高級督察<br />
馬錦濤、鄭麗玲之女<br />
馬國宏之姊<br />
郭綺芬之繼女兼天敵，後和好<br />
沈雄、凌心怡、莫正康、劉俊碩、程偉勝之上司<br />
方妙娜之偶像<br />
楊逸昇之妻<br />
李蕎之天敵<br />
古澤琛之好友，並暗戀其<br />
於第30集懷孕<br />
与梁小柔先敌后友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳美詩.md" title="wikilink">陳美詩</a></p></td>
<td><p>方妙娜</p></td>
<td><p><strong>Formula</strong><br />
尖沙咀警署女警→西九龍警署軍裝警員（WPC69163）<br />
楊秀娟之女<br />
楊逸昇之表妹<br />
莫正康之女友</p></td>
</tr>
</tbody>
</table>

### 林家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曾偉權.md" title="wikilink">曾偉權</a></p></td>
<td><p>許立仁</p></td>
<td><p><strong>Matt</strong><br />
富豪<br />
林沛沛之夫<br />
林汀汀之姐夫<br />
於第7集被江有全打劫但被其错手推下樓摔死<br />
參見<strong><a href="../Page/#許立仁堕樓凶殺案（第7—8集）.md" title="wikilink">許立仁坠楼凶杀案（第7—8集）</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陳芷菁.md" title="wikilink">陳芷菁</a></strong></p></td>
<td><p><strong>林沛沛</strong></p></td>
<td><p>善終服務醫師<br />
林汀汀之姊<br />
高彥博之舊情人<br />
許立仁之妻</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鍾嘉欣.md" title="wikilink">鍾嘉欣</a></strong></p></td>
<td><p><strong>林汀汀</strong></p></td>
<td><p>化驗師<br />
高彥博之下属<br />
林沛沛之妹<br />
古澤琛之未婚妻<br />
許立仁之小姨<br />
於第4集為救梁小柔而被炸彈炸至重傷，搶救後最終身亡</p></td>
</tr>
</tbody>
</table>

### 馬家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅樂林.md" title="wikilink">羅樂林</a></p></td>
<td><p>馬錦濤</p></td>
<td><p>郭綺芬、鄭麗玲之夫<br />
馬幗英、馬國宏之父</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盧宛茵.md" title="wikilink">盧宛茵</a></p></td>
<td><p>郭綺芬</p></td>
<td><p>馬錦濤之原配，馬國宏之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/惠英紅.md" title="wikilink">惠英紅</a></p></td>
<td><p>鄭麗玲</p></td>
<td><p>馬錦濤之繼室，馬幗英之母</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></strong></p></td>
<td><p><strong>馬幗英</strong></p></td>
<td><p><strong>Bell</strong><br />
馬錦濤、鄭麗玲之女<br />
郭綺芬之繼女兼天敵，後和好<br />
馬國宏同父異母之姊<br />
參見<strong><a href="../Page/#重案組.md" title="wikilink">重案組</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃長興.md" title="wikilink">黃長興</a></p></td>
<td><p>馬國宏</p></td>
<td><p><strong>Ben</strong><br />
馬錦濤、郭綺芬之子<br />
馬幗英同父異母之弟<br />
於第20集被李志偉綁架<br />
於第22集獲救</p></td>
</tr>
</tbody>
</table>

### 與案件有關人物

#### 野战场雙屍案（第1—4集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/司徒瑞祈.md" title="wikilink">司徒瑞祈</a></strong></p></td>
<td><p><strong>李忠信</strong></p></td>
<td><p>野戰場之員工<br />
Paul之下属<br />
因其爱狗被陳子成射死而殺死陳子成<br />
於第2集被捕</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/胡諾言.md" title="wikilink">胡諾言</a></strong></p></td>
<td><p><strong>葉志文</strong></p></td>
<td><p>職業小丑<br />
葉志武之兄<br />
因为其弟报仇而杀害王正鴻<br />
於第4集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳良韋.md" title="wikilink">陳良韋</a></p></td>
<td><p>陳子成</p></td>
<td><p>「猩猩吧」酒保<br />
林仲佳、劉珊珊之好友<br />
吸毒者<br />
於第1集因射死李忠信之爱狗遭李忠信以鉛彈殺害</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魏惠文.md" title="wikilink">魏惠文</a></p></td>
<td><p>Paul</p></td>
<td><p>野戰場場主<br />
李忠信之上司</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王俊伸.md" title="wikilink">王俊伸</a></p></td>
<td><p>陳文軒</p></td>
<td><p>Ken<br />
法醫<br />
古澤琛助手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李穎芝.md" title="wikilink">李穎芝</a></p></td>
<td><p>劉珊珊</p></td>
<td><p>陳子成之好友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莊狄生.md" title="wikilink">莊狄生</a></p></td>
<td><p>林頌佳</p></td>
<td><p>「猩猩吧」侍應<br />
陳子成之好友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/江梓瑋.md" title="wikilink">江梓瑋</a></p></td>
<td><p>葉志武</p></td>
<td><p>葉志文之弟<br />
因服食過量毒品而被車撞死</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李成昌.md" title="wikilink">李成昌</a></p></td>
<td><p>戴　貴</p></td>
<td><p>富豪<br />
毒販<br />
患有鼻敏感<br />
戴立德之叔<br />
石勇之天敌<br />
曾被懷疑殺害王正鴻<br />
於第4集被王彪放在車子底下的炸彈炸死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林敬剛.md" title="wikilink">林敬剛</a></p></td>
<td><p>戴立德</p></td>
<td><p>毒販<br />
戴貴之姪<br />
石勇之天敌<br />
於第4集被王彪放在車子底下的炸彈炸死</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃智賢.md" title="wikilink">黃智賢</a></p></td>
<td><p>王正鴻</p></td>
<td><p>販毒者<br />
鍾婉媚之夫<br />
馬幗英線人<br />
王文浩之父<br />
與石勇不和<br />
因叶志武吸自己的毒品而出事，所以於第1集遭葉志文踢入捕兽籠夾死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/姚瑩瑩.md" title="wikilink">姚瑩瑩</a></p></td>
<td><p>鍾婉媚</p></td>
<td><p>王正鴻之妻<br />
王文浩之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳諾弘.md" title="wikilink">吳諾弘</a></p></td>
<td><p>王文浩</p></td>
<td><p>王正鴻、鍾婉媚之子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彭冠中.md" title="wikilink">彭冠中</a></p></td>
<td><p>艇仔東</p></td>
<td><p>販毒者<br />
曾與王正鴻發生爭執，而被懷疑是殺害王正鴻之凶手<br />
石勇之天敵</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></strong></p></td>
<td><p><strong>馬幗英</strong></p></td>
<td><p>曾被懷疑是殺害王正鴻之凶手<br />
參見<strong><a href="../Page/#西九龍重案組.md" title="wikilink">西九龍重案組</a></strong>、<strong><a href="../Page/#馬家.md" title="wikilink">馬家</a></strong></p></td>
</tr>
</tbody>
</table>

#### 黑道争权案（第3—7集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鍾嘉欣.md" title="wikilink">鍾嘉欣</a></strong></p></td>
<td><p><strong>林汀汀</strong></p></td>
<td><p>古澤琛之妻<br />
於第4集為救梁小柔而被炸彈炸至重傷，搶救後最終身亡<br />
參見<strong><a href="../Page/#法證部.md" title="wikilink">法證部</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>梁小柔</strong></p></td>
<td><p><strong>Madam Leung</strong><br />
於第4集被炸彈炸至右耳失聰，右手神經線受損報廢<br />
參見<strong><a href="../Page/#西九龍重案組.md" title="wikilink">西九龍重案組</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李家鼎.md" title="wikilink">李家鼎</a></strong></p></td>
<td><p><strong>石　勇</strong></p></td>
<td><p><strong>石頭勇</strong><br />
富豪<br />
羅玉嬌之情人<br />
元海東之天敵<br />
與戴貴、戴立德、王正鴻不和<br />
教唆王彪炸死戴貴、戴立德並意外炸死林汀汀，炸傷梁小柔<br />
因被王彪勒索而杀害王彪<br />
於第7集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伍慧珊.md" title="wikilink">伍慧珊</a></p></td>
<td><p>羅玉嬌</p></td>
<td><p>被石勇收買<br />
王彪、石勇之情人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陸駿光.md" title="wikilink">陸駿光</a></p></td>
<td><p>阮大齊</p></td>
<td><p>酒樓侍應<br />
用手榴彈炸死戴貴未遂<br />
於第4集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>王　彪</p></td>
<td><p>羅玉嬌之情人<br />
受石勇唆使而炸死戴貴、戴立德並意外炸死林汀汀，炸傷梁小柔<br />
於第6集因得知石勇和羅玉嬌之奸情欲向石勇勒索，反遭石勇强制按在水里淹死</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/祝文君.md" title="wikilink">祝文君</a></p></td>
<td><p>姬太太</p></td>
<td><p>姬欣之母</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王若嵐.md" title="wikilink">王若嵐</a></p></td>
<td><p>姬　欣</p></td>
<td><p>姬太太之女<br />
炸彈案的證人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳諾弘.md" title="wikilink">吳諾弘</a></p></td>
<td><p>王文浩</p></td>
<td><p>王正鴻、鍾婉媚之子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李成昌.md" title="wikilink">李成昌</a></p></td>
<td><p>戴　貴</p></td>
<td><p>與石勇不和<br />
參見<strong><a href="../Page/#野戰場雙屍案（第1-4集）.md" title="wikilink">野戰場雙屍案（第1-4集）</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林敬剛.md" title="wikilink">林敬剛</a></p></td>
<td><p>戴立德</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃智賢.md" title="wikilink">黃智賢</a></p></td>
<td><p>王正鴻</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 许立仁坠楼案（第7—8集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曾偉權.md" title="wikilink">曾偉權</a></p></td>
<td><p>許立仁</p></td>
<td><p><strong>Matt</strong><br />
富豪<br />
林沛沛之夫<br />
於第7集被江有全错手推下樓摔死<br />
參見<strong><a href="../Page/#林家.md" title="wikilink">林家</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/游莨維.md" title="wikilink">游莨維</a></p></td>
<td><p>江有全</p></td>
<td><p>流氓<br />
因搶劫許立仁，而誤杀許立仁<br />
於第8集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關浩揚.md" title="wikilink">關浩揚</a></p></td>
<td><p>范子新</p></td>
<td><p>被誤以為殺害許立仁的兇手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/招石文.md" title="wikilink">招石文</a></p></td>
<td><p>老　闆</p></td>
<td><p>糖水鋪老闆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寧進.md" title="wikilink">寧　進</a></p></td>
<td><p>伙　記</p></td>
<td><p>糖水鋪伙記</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾健明.md" title="wikilink">曾健明</a></p></td>
<td><p>李醫生</p></td>
<td><p>許立仁的腦科醫生</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 意外车祸案（第9—10集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳狄克.md" title="wikilink">陳狄克</a></p></td>
<td><p>李建安</p></td>
<td><p>李國棟之父<br />
欲帮李國棟頂替罪名不遂<br />
於第9集被捕<br />
於第10集被正式起訴妨碍司法公正</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭世豪.md" title="wikilink">鄭世豪</a></p></td>
<td><p>李國棟</p></td>
<td><p>李建安之子<br />
意外撞死何韻妍<br />
於第9集被捕<br />
於第10集被正式起訴妨碍司法公正</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇麗明.md" title="wikilink">蘇麗明</a></p></td>
<td><p>何韻妍</p></td>
<td><p>David Leung之顧客<br />
於第9集因與David Leung通電話和听MP3而被李國棟意外撞死</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>David Leung</p></td>
<td><p>何韻妍之股票顧問</p></td>
</tr>
</tbody>
</table>

#### 骸骨案（第10—13集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古明華.md" title="wikilink">古明華</a></p></td>
<td><p>馮　添</p></td>
<td><p><strong>Witness</strong><br />
楊鳳之私家偵探<br />
勒索古澤琛<br />
于1993年7月20日趁周长发被殴打晕倒时抢走其东西</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃鳳瓊.md" title="wikilink">黃鳳瓊</a></p></td>
<td><p>楊　鳳</p></td>
<td><p><strong>Winnie</strong><br />
周龍生之媳婦<br />
張嘉敏之表姊<br />
周長發之妻<br />
王德培之外遇女友<br />
何咏诗之好友</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>周龍生</p></td>
<td><p>周長發之父<br />
楊鳳之老爺<br />
傅正基之爷爷<br />
1993年春节因欠債100元而被人打死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭家生.md" title="wikilink">鄭家生</a></p></td>
<td><p>周長發</p></td>
<td><p><strong>喪狗</strong><br />
黑社會成員<br />
周龍生之子<br />
楊鳳之夫<br />
傅正基之親父<br />
何咏诗之前男友<br />
強姦張妙雲、張嘉敏<br />
於1993年7月20日被古澤琛及楊逸昇毆打暈倒時被馮添搶走東西，後被傅正基打死</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王俊棠.md" title="wikilink">王俊棠</a></p></td>
<td><p>王德培</p></td>
<td><p>拳擊教練<br />
楊鳳之外遇男友<br />
曾經因撞車問题而和人打鬥，被捕後判坐牢三個月</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄺佐輝.md" title="wikilink">鄺佐輝</a></p></td>
<td><p>傅志輝</p></td>
<td><p>張妙雲之夫<br />
傅正基之養父，被其错认为亲父<br />
罹患Huntington's Chorea（亨丁頓舞蹈症）<br />
于1993年得知周长发欺负张妙云而写恐吓信给周长发</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘芳芳.md" title="wikilink">潘芳芳</a></p></td>
<td><p>張妙雲</p></td>
<td><p>「妙雲工作坊」陶瓷教師<br />
傅志輝之妻<br />
傅正基之母<br />
1979年被周長發強姦後，懷有傅正基</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>何咏诗</p></td>
<td><p><strong>Ann</strong><br />
楊鳳之好友<br />
周長發之前女友</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>張嘉敏</p></td>
<td><p>楊鳳之表妹<br />
被周長發强奸<br />
後嫁一名華僑，育有一女<br />
已去世</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/黃嘉樂.md" title="wikilink">黃嘉樂</a></strong></p></td>
<td><p><strong>傅正基</strong></p></td>
<td><p>出生于1980年<br />
周长发、張妙雲之子<br />
周龙生之孙<br />
傅志辉之养子，并错认其为自己亲父<br />
于1993年7月20日因不知道周长发是自己的亲父及为母亲出头而打死周長發<br />
於第13集被捕，后以自衛殺人的理由而被判無罪釋放<br />
少年由<a href="../Page/陳景昆.md" title="wikilink">陳景昆飾演</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳勉良.md" title="wikilink">陳勉良</a></p></td>
<td><p>道　友</p></td>
<td><p>為馮添接贓</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁珈詠.md" title="wikilink">梁珈詠</a></p></td>
<td><p>新聞主播</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 连环凶杀案（第14—16集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/蔡康年.md" title="wikilink">蔡康年</a></strong></p></td>
<td><p><strong>張耀忠</strong></p></td>
<td><p>髮型師<br />
變態殺手<br />
因何廣福半年前先救残疾女童間接導致張朱朱失救，因此對殘疾人特别敏感<br />
水奕文之夫<br />
張朱朱之父<br />
張朱朱、水奕文死後患有<a href="../Page/创伤后心理压力紧张综合症.md" title="wikilink">创伤后心理压力紧张综合症</a>（Post Traumatic Stress Disorder, PTSD）<br />
于第14集用皮带打死黃妙嫦及以头撞墙方式杀害李美琪<br />
于第14-15集袭击梁小柔，并勒死及以头撞墙方式杀害梁小柔未遂<br />
于第16集报复割喉杀害何廣福，后被捕</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a></strong></p></td>
<td><p><strong>梁小柔</strong></p></td>
<td><p><strong>Madam Leung</strong><br />
於第14-15集被張耀忠袭擊，險被張耀忠勒死、以頭撞墙的方式殺害<br />
參見<strong><a href="../Page/#西九龍重案組.md" title="wikilink">西九龍重案組</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張雪芹.md" title="wikilink">張雪芹</a></p></td>
<td><p>黃妙嫦</p></td>
<td><p>瘫痪人士<br />
日本漫画、電影翻译者<br />
雲茵貴之女<br />
於第14集被鄧吉踢了一脚導致从輪椅滚下，后被張耀忠以皮带打死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孫慧雪.md" title="wikilink">孫慧雪</a></p></td>
<td><p>李美琪</p></td>
<td><p><strong>Maggie</strong><br />
聾啞人士<br />
電腦程序員<br />
李美晴之妹<br />
邵中龍之女友<br />
於第14集被張耀忠以頭撞墙的方式殺害</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林祐飛.md" title="wikilink">林祐飛</a></p></td>
<td><p>何廣福</p></td>
<td><p>消防隊員<br />
頭皮屑患者<br />
半年前救回張朱朱失敗，導致張耀忠患上PTSD<br />
於第16集被張耀忠以刀割喉身亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾守明.md" title="wikilink">曾守明</a></p></td>
<td><p>鄧　吉</p></td>
<td><p>鄧震興之父<br />
曾被懷疑殺害黃妙嫦<br />
于第14集踢了黃妙嫦一脚導致她从輪椅滚下<br />
踢出張耀忠的心魔，从而間接害死黃妙嫦</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>鄧震興</p></td>
<td><p>鄧吉之子<br />
於第14集因販賣、飲喝咳嗽藥水而被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岑潔儀.md" title="wikilink">岑潔儀</a></p></td>
<td><p>李美晴</p></td>
<td><p><a href="../Page/空姐.md" title="wikilink">空姐</a><br />
李美琪之家姊</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙敏通.md" title="wikilink">趙敏通</a></p></td>
<td><p>邵中龍</p></td>
<td><p><strong>Ricky</strong><br />
髮型師<br />
李美琪、王秀雅、關曉琦之男友<br />
欺騙李美琪、王秀雅、關曉琦<br />
曾被懷疑殺害李美琪<br />
與王秀雅於星期三發生關係而成為不在場證据</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/謝兆韵.md" title="wikilink">謝兆韵</a></p></td>
<td><p>王秀雅</p></td>
<td><p><strong>Yoyo</strong><br />
邵中龍之女友<br />
因與邵中龍於星期三發生關係而成為其時間證人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/菁瑋.md" title="wikilink">菁　瑋</a></p></td>
<td><p>關曉琦</p></td>
<td><p><strong>Queenie</strong><br />
邵中龍之女友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丁主惠.md" title="wikilink">丁主惠</a></p></td>
<td><p>雲茵貴</p></td>
<td><p>黃妙嫦之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何婷恩.md" title="wikilink">何婷恩</a></p></td>
<td><p>水奕文</p></td>
<td><p>張耀忠之妻<br />
張朱朱之母<br />
半年前服用過量藥物自殺</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>張朱朱</p></td>
<td><p>張耀忠、水奕文之女<br />
半年前因畫畫中心爆炸而死亡</p></td>
</tr>
</tbody>
</table>

#### 女歌手被袭击案（第17—20集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/楊思琦.md" title="wikilink">楊思琦</a></strong></p></td>
<td><p><strong>郭曉琳</strong></p></td>
<td><p><strong>Sharon</strong><br />
潘浚華旗下藝人<br />
當紅女歌手（台前專做慈善家，實際為烟幕）<br />
譚家樂之女友<br />
唆使譚家樂為她作曲<br />
於第17集因不肯捐骨髓给李瑩瑩而被關翠蘭用讲座打伤<br />
於第19集由於座駕黃色平治敞篷車安全氣囊失效車祸受傷<br />
於第20集被控給假口供，被判罰款及社會服務令，最后决定捐骨髓給李瑩瑩，并与譚家樂到台湾发展</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊鴻俊.md" title="wikilink">楊鴻俊</a></p></td>
<td><p>鄒喬堅</p></td>
<td><p><strong>Louis</strong><br />
郭曉琳助手<br />
揭發郭曉琳多件壞事</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳宇琛.md" title="wikilink">陳宇琛</a></strong></p></td>
<td><p><strong>譚家樂</strong></p></td>
<td><p>郭曉琳之男友<br />
被郭曉琳唆使帮她作曲<br />
於第20集与郭曉琳到台湾发展</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麥嘉倫.md" title="wikilink">麥嘉倫</a></p></td>
<td><p>火鳳皇</p></td>
<td><p><strong>Dick</strong><br />
譚家樂經理人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魯振順.md" title="wikilink">魯振順</a></p></td>
<td><p>潘浚華</p></td>
<td><p><strong>Jackie</strong><br />
藝人經理人<br />
郭曉琳經理人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葉凱茵.md" title="wikilink">葉凱茵</a></p></td>
<td><p>關翠蘭</p></td>
<td><p>李瑩瑩之母<br />
于第17集因郭晓琳不肯捐骨髓给李莹莹而用讲座打伤郭曉琳<br />
於第19集被捕</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>何小姐</p></td>
<td><p>醫院員工<br />
付責監察李瑩瑩的病情</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>李瑩瑩</p></td>
<td><p>關翠蘭之女<br />
於第20集得到骨髓後手術成功</p></td>
</tr>
</tbody>
</table>

#### 马国宏绑架案（第20—22集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/黃長興.md" title="wikilink">黃長興</a></strong></p></td>
<td><p><strong>馬國宏</strong></p></td>
<td><p><strong>Ben</strong><br />
馬錦濤、郭綺芬之子<br />
馬幗英同父異母之弟<br />
第20集被李志偉綁架<br />
於第22集被張金水剪斷其中一個手指<br />
參見<strong><a href="../Page/#馬家.md" title="wikilink">馬家</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/羅樂林.md" title="wikilink">羅樂林</a></strong></p></td>
<td><p><strong>馬錦濤</strong></p></td>
<td><p>郭綺芬、鄭麗玲之夫<br />
馬幗英、馬國宏之父<br />
於第20集被綁匪所害、導致遇上車祸受傷<br />
參見<strong><a href="../Page/#馬家.md" title="wikilink">馬家</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/盧宛茵.md" title="wikilink">盧宛茵</a></strong></p></td>
<td><p><strong>郭綺芬</strong></p></td>
<td><p>馬錦濤之元配<br />
馬國宏之母<br />
於第22集被李志偉劫持<br />
參見<strong><a href="../Page/#馬家.md" title="wikilink">馬家</a></strong> |-　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃子恆.md" title="wikilink">黃子恆</a></p></td>
<td><p>張金水</p></td>
<td><p>綁匪“抽水”<br />
剪斷馬國宏其中一个手指<br />
為鄭軍報仇而欲殺馬國宏<br />
於第22集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葉暐.md" title="wikilink">葉　暐</a></p></td>
<td><p>何永虎</p></td>
<td><p>綁匪“虎皮鲨”<br />
於第22集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/游飈.md" title="wikilink">游　飈</a></p></td>
<td><p>王廣昌</p></td>
<td><p>馬國宏的百囍酒樓的經理（部長）<br />
三合會會員 绰号“大佬昌”</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張國威.md" title="wikilink">張國威</a></p></td>
<td><p>黃創業</p></td>
<td><p>受張金水指使偷車，於第21集被捕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃德基.md" title="wikilink">黃德基</a></p></td>
<td><p>李俊光</p></td>
<td><p>毆打郭綺芬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李啟傑.md" title="wikilink">李啟傑</a></p></td>
<td><p>鄭　軍</p></td>
<td><p>幫凶 “大鸡”<br />
於第21集誤傷頸部死亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/姚浩政.md" title="wikilink">姚浩政</a></p></td>
<td><p>-</p></td>
<td><p>車房仔</p></td>
</tr>
</tbody>
</table>

#### 保安凶杀案（第23—25集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/郭峰_(演員).md" title="wikilink">郭　峰</a></strong></p></td>
<td><p><strong>趙應球</strong></p></td>
<td><p><strong>球叔</strong><br />
保安員<br />
莫喬豐之下属<br />
於第24集爆竊屋苑管理處，后被捕</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蔣志光.md" title="wikilink">蔣志光</a></strong></p></td>
<td><p><strong>莫喬豐</strong></p></td>
<td><p><strong>莫經理</strong><br />
保安員經理<br />
何大奇、趙應球、富仔、陳原福之上司<br />
爆竊張燕瓊家，被何大奇发现后杀人灭口<br />
於第25集被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龔茜彤.md" title="wikilink">樂　瞳</a></p></td>
<td><p>戴玉瑩</p></td>
<td><p>護士<br />
照顧何大奇</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇晉霆.md" title="wikilink">蘇晉霆</a></p></td>
<td><p>富　仔</p></td>
<td><p>保安員<br />
莫喬豐之下属</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘_標.md" title="wikilink">潘　標</a></p></td>
<td><p>何大奇</p></td>
<td><p>保安員<br />
莫喬豐之下属<br />
於第23集被莫喬豐殺死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/余子明.md" title="wikilink">-{余}-子明</a></p></td>
<td><p>陳原福</p></td>
<td><p>保安員<br />
莫喬豐之下属<br />
於第24集被解僱</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳文靜.md" title="wikilink">陳文靜</a></p></td>
<td><p>張燕瓊</p></td>
<td><p>何忠泰之前妻<br />
何良之母<br />
曾浩南之女友，於第25集分手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭卓樺.md" title="wikilink">郭卓樺</a></p></td>
<td><p>何忠泰</p></td>
<td><p>張燕瓊之前夫<br />
何良之父</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳少邦.md" title="wikilink">陳少邦</a></p></td>
<td><p>曾浩南</p></td>
<td><p>張燕瓊之男友，於第25集分手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何嘉裕.md" title="wikilink">何嘉裕</a></p></td>
<td><p>方玉清</p></td>
<td><p>管理处职员<br />
负责記錄遗失的物件</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳佩思.md" title="wikilink">陳佩思</a></p></td>
<td><p>朱文靜</p></td>
<td><p>管理處职員<br />
負責記錄遗失的物件</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>何　良</p></td>
<td><p>張燕瓊、何忠泰之子</p></td>
</tr>
</tbody>
</table>

#### 女明星凶杀案（第25—30集）

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／职業</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/鄭嘉穎.md" title="wikilink">鄭嘉穎</a></strong></p></td>
<td><p><strong>楊逸昇</strong></p></td>
<td><p><strong>Ivan</strong><br />
馬幗英之未婚夫<br />
於第29集因撞車爆炸炸死容兆海而使卓嵐記恨<br />
於第30集被卓嵐以炸彈炸至重傷，後蘇醒<br />
參見<strong><a href="../Page/#法證部.md" title="wikilink">法證部</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/林文龍.md" title="wikilink">林文龍</a></strong></p></td>
<td><p><strong>古澤琛</strong></p></td>
<td><p><strong>Sam</strong><br />
為救馬幗英而一拳打死卓嵐，險被控謀殺<br />
參見<strong><a href="../Page/#法證部.md" title="wikilink">法證部</a></strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a></strong></p></td>
<td><p><strong>馬幗英</strong></p></td>
<td><p><strong>Bell</strong><br />
於第29集因與容兆海的車子相撞而爆炸，並炸死容兆海而使卓嵐記恨<br />
於第30集因懷孕而被卓嵐妒忌，後被卓嵐迷昏並挾制，險遭殺害<br />
參見<strong><a href="../Page/#西九龍重案组.md" title="wikilink">西九龍重案组</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陳丹丹.md" title="wikilink">陳丹丹</a></strong></p></td>
<td><p><strong>卓　嵐</strong></p></td>
<td><p>替身演員<br />
患有<a href="../Page/假性懷孕.md" title="wikilink">假性懷孕及</a><a href="../Page/身心障碍.md" title="wikilink">身心障碍</a>（Psychosomatic Disorder）<br />
容兆海之女友<br />
潘安妮之替身<br />
2003年第一屆《全港新偶像挑戰賽》選手<br />
2003年期因是第一届《全港新偶像挑戰賽》夺冠大熱門而被潘安妮與梅芊嫉妒灌醉后，慘遭輪姦及懷孕；事後做了一个很差的落胎手術，令到無法再懷孕<br />
多次企图杀害梅芊未遂<br />
袭击梅芊、楊逸昇、馬幗英<br />
于第29集撞車爆炸炸死容兆海而記恨馬幗英、楊逸昇，欲謀殺其<br />
于第26集杀害潘安妮<br />
于第28集因缪倩看错赠送人姓名而送山埃毒水毒死缪倩，后开车撞死梅芊未遂<br />
于第29集刺伤梅芊，并与容兆海逃跑<br />
於第30集告诉馬幗英有了寶寶，實際為心理作用，因患假性懷孕而產生錯覺，後在醫院證實没有懷孕，后因馬幗英懷孕而妒忌，劫持、袭撃馬幗英，最后遭古澤琛一拳打，導致墮樓身亡</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李施嬅.md" title="wikilink">李施嬅</a></strong></p></td>
<td><p><strong>李　蕎</strong></p></td>
<td><p><strong>Cat</strong><br />
曹世川之私人助理<br />
古澤琛之師妹<br />
馬幗英之天敵<br />
李澤群之家姊<br />
曾被懷疑刺殺潘安妮<br />
於第26集被潘安妮以其弟弟与冯教授之妻偷情之视频要挾，從而使潘安妮接替何秀冰成為電影《最後一块骨头》女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甄志強.md" title="wikilink">甄志強</a></p></td>
<td><p>曹世川</p></td>
<td><p><strong>Peter</strong><br />
富豪<br />
何秀冰、繆倩之男友兼上司<br />
李蕎、潘安妮、梅芊之上司<br />
於第28集因吸可卡因被捕</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁嘉琪.md" title="wikilink">梁嘉琪</a></p></td>
<td><p>何秀冰</p></td>
<td><p>Debby<br />
曹世川旗下女明星兼女友<br />
四小花之一<br />
電影《最後一块骨头》女主角（飾 Madam Mui）<br />
於第25集因减肥过度，急性心臟衰竭身亡</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/李亞男.md" title="wikilink">李亞男</a></strong></p></td>
<td><p><strong>潘安妮</strong></p></td>
<td><p><strong>Annie</strong><br />
曹世川旗下女明星<br />
女演員<br />
梅芊之好友<br />
針對繆倩<br />
2003年第一屆《全港新偶像挑戰賽》選手<br />
因嫉妒卓嵐為夺冠大熱門後與梅芊灌醉卓嵐，間接害卓嵐不能再懷孕<br />
以李澤群的情欲視频而要挟李蕎，從而接替何秀冰成為電影《最後一块骨头》女主角<br />
於第26集被卓嵐刺殺死亡</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陸詩韻.md" title="wikilink">陸詩韻</a></p></td>
<td><p>繆　倩</p></td>
<td><p><strong>Cherry</strong><br />
曹世川旗下女明星兼女友<br />
女演員<br />
接替何秀冰成為曹世川女友<br />
潘安妮、梅芊之針對對象<br />
於第28集因把贈送人的姓名錯看為「miu」而喝下卓嵐下毒的<a href="../Page/山埃.md" title="wikilink">山埃毒水身亡</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鄧上文.md" title="wikilink">鄧上文</a></strong></p></td>
<td><p><strong>梅　芊</strong></p></td>
<td><p><strong>Mable</strong><br />
曹世川旗下女明星<br />
女演員<br />
潘安妮之好友<br />
南華文之情人<br />
針對繆倩<br />
2003年第一屆《全港新偶像挑戰賽》選手<br />
因嫉妒卓嵐為夺冠大熱門後與潘安妮灌醉卓嵐，間接害卓嵐不能再懷孕<br />
於第28集險遭卓嵐開車撞到，後被容兆海救起<br />
於第29集被卓嵐刺傷昏迷後清醒指証卓嵐</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江富強.md" title="wikilink">江富強</a></p></td>
<td><p>容兆海</p></td>
<td><p>爆炸技術員<br />
卓嵐之男友兼幫兇<br />
於第29集撞車爆炸死亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱婉儀.md" title="wikilink">朱婉儀</a></p></td>
<td><p>車曼琦</p></td>
<td><p><strong>Rosa</strong><br />
原為何秀冰助手<br />
于第25集第一个发现何秀冰死亡<br />
於第26集成為潘安妮助手<br />
於第27集成為繆倩助手</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳志健.md" title="wikilink">陳志健</a></p></td>
<td><p>南華文</p></td>
<td><p><strong>Tony</strong><br />
藝人<br />
梅芊之情人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/袁偉豪.md" title="wikilink">袁偉豪</a></p></td>
<td><p>吉家浩</p></td>
<td><p><strong>Eric</strong><br />
藝人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冼灝英.md" title="wikilink">冼灝英</a></p></td>
<td><p>華國嵐</p></td>
<td><p>武術指導</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮素波.md" title="wikilink">馮素波</a></p></td>
<td><p>林佳中</p></td>
<td><p>茶水阿姐<br />
於第26集第一個發現潘安妮死亡</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇敏聰.md" title="wikilink">蘇敏聰</a></p></td>
<td><p>李澤群</p></td>
<td><p><strong>Joe</strong><br />
李蕎之弟<br />
童妙素之情夫，後分手<br />
馮紹祥之徒弟<br />
其情欲視频被潘安妮偷拍來威拹李蕎<br />
於第28集烧炭自殺身亡</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>童妙素</p></td>
<td><p><strong>Jenny</strong><br />
馮紹祥之妻<br />
李澤群之情婦，後分手<br />
其情欲視频被潘安妮偷拍</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>馮紹祥</p></td>
<td><p><strong>Dr.Fung</strong><br />
童妙素之夫<br />
李澤群之師傅</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅天池.md" title="wikilink">羅天池</a></p></td>
<td><p>陳志堅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張穎康.md" title="wikilink">張穎康</a></p></td>
<td><p>吳啟波</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧英敏.md" title="wikilink">鄧英敏</a></p></td>
<td><p>田</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高可慧.md" title="wikilink">高可慧</a></p></td>
<td><p>珊</p></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係／職業</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳敏之.md" title="wikilink">陳敏之</a></p></td>
<td><p>巢沛麗</p></td>
<td><p>古澤琛支持者<br />
與林汀汀相似的一名女子<br />
對古澤琛產生興趣</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾志光.md" title="wikilink">鍾志光</a></p></td>
<td><p>周醫生</p></td>
<td><p>外科醫生</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉琛宜.md" title="wikilink">劉琛宜</a></p></td>
<td><p>鞋店店主</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃淑君.md" title="wikilink">黃淑君</a></p></td>
<td><p>Sue</p></td>
<td><p>店員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘冠霖.md" title="wikilink">潘冠霖</a></p></td>
<td><p>Ada</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李鴻杰.md" title="wikilink">李鴻杰</a></p></td>
<td><p>馮Sir</p></td>
<td><p>毒品調查科總督察<br />
馬幗英前上司</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凌禮文.md" title="wikilink">凌禮文</a></p></td>
<td><p>阿　伯</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Lily</p></td>
<td><p>蛋糕店收銀員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卓姿.md" title="wikilink">卓　姿</a></p></td>
<td><p>餅店店員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張智軒.md" title="wikilink">張智軒</a></p></td>
<td><p>玉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李忠希.md" title="wikilink">李忠希</a></p></td>
<td><p>Jason</p></td>
<td><p>馬幗英前未婚夫<br />
在婚禮上被Alex槍殺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高俊文.md" title="wikilink">高俊文</a></p></td>
<td><p>Alex父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉桂芳.md" title="wikilink">劉桂芳</a></p></td>
<td><p>Alex母</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-{<a href="../Page/謝兆韵.md" title="wikilink">謝兆韵</a>}-</p></td>
<td><p>酒樓知客</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蒲茗藍.md" title="wikilink">蒲茗藍</a></p></td>
<td><p>Alex</p></td>
<td><p>暗恋馬幗英<br />
殺死Jason後自殺身亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溫裕紅.md" title="wikilink">溫裕紅</a></p></td>
<td><p>Jason母</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈可欣.md" title="wikilink">沈可欣</a></p></td>
<td><p>Madam Wong</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁家駒.md" title="wikilink">梁家駒</a></p></td>
<td><p>Dr Leung</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙敏通.md" title="wikilink">趙敏通</a></p></td>
<td><p>Ricky</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日伽.md" title="wikilink">日　伽</a></p></td>
<td><p>-{余}-玉珠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔡考藍.md" title="wikilink">蔡考藍</a></p></td>
<td><p>劉美美</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙樂賢.md" title="wikilink">趙樂賢</a></p></td>
<td><p>馮建國</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬貫東.md" title="wikilink">馬貫東</a></p></td>
<td><p>何志遠</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/裘卓能.md" title="wikilink">裘卓能</a></p></td>
<td><p>Calvin</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王維德.md" title="wikilink">王維德</a></p></td>
<td><p>Dr Hu</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾愛媚.md" title="wikilink">曾愛媚</a></p></td>
<td><p>護　士</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱維德.md" title="wikilink">朱維德</a></p></td>
<td><p>Dr Lee</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何嘉裕.md" title="wikilink">何嘉裕</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳佩思.md" title="wikilink">陳佩思</a></p></td>
<td><p>靜</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邵卓堯.md" title="wikilink">邵卓堯</a></p></td>
<td><p>廖先生</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張達倫.md" title="wikilink">張達倫</a></p></td>
<td><p>金</p></td>
<td></td>
</tr>
</tbody>
</table>

## 獎項

### [萬千星輝頒獎典禮2008](../Page/萬千星輝頒獎典禮2008.md "wikilink")

<table style="width:56%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>單位</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>最佳劇集</p></td>
<td><p>法證先鋒II</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳男主角</p></td>
<td><p>林文龍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳女主角</p></td>
<td><p>佘詩曼</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>飛躍進步男藝員</p></td>
<td><p>梁烈唯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>飛躍進步女藝員</p></td>
<td><p>李施嬅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>飛躍進步女藝員</p></td>
<td><p>陳敏之</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>tvb.com人氣大獎</p></td>
<td><p>佘詩曼</p></td>
<td><p>（最後三強）</p></td>
</tr>
<tr class="even">
<td><p>tvb.com人氣大獎</p></td>
<td><p>鄭嘉穎</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>tvb.com人氣大獎</p></td>
<td><p>楊秀惠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>tvb.com人氣大獎</p></td>
<td><p>楊思琦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>時尚魅力大獎</p></td>
<td><p>佘詩曼</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>時尚魅力大獎</p></td>
<td><p>鄭嘉穎</p></td>
<td></td>
</tr>
</tbody>
</table>

### [星和無綫電視大獎2010](../Page/星和無綫電視大獎2010.md "wikilink")

<table style="width:56%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>單位</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>我最愛TVB女藝人</p></td>
<td><p>佘詩曼</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>我最愛TVB電視女角色</p></td>
<td><p>佘詩曼</p></td>
<td></td>
</tr>
</tbody>
</table>

### [Astro華麗台電視劇大獎2009](../Page/Astro華麗台電視劇大獎2009.md "wikilink")

<table style="width:56%;">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>單位</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>我的至愛女主角</p></td>
<td><p>佘詩曼</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>我的至愛情侶檔</p></td>
<td><p>鄭嘉穎、佘詩曼</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>我的至愛角色</p></td>
<td><p>佘詩曼</p></td>
<td></td>
</tr>
</tbody>
</table>

## 收視

### 香港收视

以下為本劇於[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台之收視紀錄](../Page/翡翠台.md "wikilink")：

| 週次 | 集數    | 日期               | 平均收視                             | 最高收視                             |
| -- | ----- | ---------------- | -------------------------------- | -------------------------------- |
| 1  | 01-04 | 2008年5月19日-5月22日 | 33[點](../Page/收視點.md "wikilink") | 39[點](../Page/收視點.md "wikilink") |
| 2  | 05-09 | 2008年5月26日-5月30日 | 31[點](../Page/收視點.md "wikilink") | 33[點](../Page/收視點.md "wikilink") |
| 3  | 10-14 | 2008年6月2日-6月6日   | 31[點](../Page/收視點.md "wikilink") | 34[點](../Page/收視點.md "wikilink") |
| 4  | 15-19 | 2008年6月9日-6月13日  | 31[點](../Page/收視點.md "wikilink") | 34[點](../Page/收視點.md "wikilink") |
| 5  | 20-24 | 2008年6月16日-6月20日 | 33[點](../Page/收視點.md "wikilink") | 36[點](../Page/收視點.md "wikilink") |
| 6  | 25-29 | 2008年6月23日-6月27日 | 34[點](../Page/收視點.md "wikilink") | 37[點](../Page/收視點.md "wikilink") |
| 6  | 30    | 2008年6月28日       | 36[點](../Page/收視點.md "wikilink") | 39[點](../Page/收視點.md "wikilink") |

本劇平均收視為33[點](../Page/收視點.md "wikilink")。

### 广州收视

| 集数    | CSM                                                                         | AGB   |
| ----- | --------------------------------------------------------------------------- | ----- |
| CSM省网 | 单集最高                                                                        | CSM市网 |
| 1-3   | 因[汶川大地震](../Page/汶川大地震.md "wikilink")[全国哀悼日停播](../Page/全国哀悼日.md "wikilink") |       |
| 4     | 6.7                                                                         | 6.7   |
| 5-9   | 6.69                                                                        | 8.38  |
| 10-14 | 7.52                                                                        | 8.2   |
| 15-19 | 8.65                                                                        | 9.79  |
| 20-24 | 9.12                                                                        | 9.86  |
| 25-30 | 8.48                                                                        | 8.88  |

## 軼事

  - 此劇是[歐陽震華與](../Page/歐陽震華.md "wikilink")[佘詩曼繼](../Page/佘詩曼.md "wikilink")《[洗冤錄II](../Page/洗冤錄II.md "wikilink")》後再度合作。
  - 此劇的原裝版大結局為一小時（連廣告），但此劇首播版大結局被加長至一小時三十分鐘（連廣告）。

## 爭議

  - 有傳歐陽震華不滿所飾演的高彥博戲份銳減，因為被新加入的[鄭嘉穎所飾演的楊逸昇分薄戲份](../Page/鄭嘉穎.md "wikilink")。
  - 由於[鍾嘉欣需要拍攝劇集](../Page/鍾嘉欣.md "wikilink")《[珠光寶氣](../Page/珠光寶氣_\(電視劇\).md "wikilink")》及《[金石良緣](../Page/金石良緣.md "wikilink")》，因此，鍾嘉欣只可以於此劇作特別演出，其戲份則由本輯女主角佘詩曼所演繹，雖然佘詩曼的演技都得到不少觀眾稱讚，但網民仍然認為鍾嘉欣飾演的林汀汀與林文龍飾演的古澤琛合演的戲份太少\[1\]

## 參考

## 外部連結

  - [TVB.com
    法證先鋒II](https://web.archive.org/web/20080722032050/http://tvcity.tvb.com/drama/forensicheroes2/)
  - [《法證先鋒II》 GOTV
    第1集重溫](https://web.archive.org/web/20140222160321/http://gotv.tvb.com/programme/103131/156477/)

## 電視節目的變遷

|align="center" colspan="5"|[同事三分親](../Page/同事三分親.md "wikilink")
\-2008年8月7日 |- |align="center"
colspan="1"|[銀樓金粉](../Page/銀樓金粉.md "wikilink")
\-5月23日 |align="center" colspan="2"|[師奶股神](../Page/師奶股神.md "wikilink")
5月26日-6月21日 |align="center"
colspan="3"|[甜言蜜語](../Page/甜言蜜語.md "wikilink")
6月23日- |- |align="center" colspan="1"|**上一套：**
[原來愛上賊](../Page/原來愛上賊.md "wikilink")
\-5月16日 |align="center"
colspan="3"|**翡翠台/高清翡翠台第三綫劇集（[2008](../Page/翡翠台電視劇集列表_\(2008年\)#第三線劇集.md "wikilink")）**
**法證先鋒II**
5月19日-6月28日 |align="center" colspan="1"|**下一套：**
[疑情別戀](../Page/疑情別戀.md "wikilink")
6月30日-

[Category:2008年無綫電視劇集](../Category/2008年無綫電視劇集.md "wikilink")
[2](../Category/法證先鋒系列.md "wikilink")
[Category:2008年AOD電視劇集](../Category/2008年AOD電視劇集.md "wikilink")
[Category:電視劇續集](../Category/電視劇續集.md "wikilink")
[Category:無綫電視2000年代背景劇集](../Category/無綫電視2000年代背景劇集.md "wikilink")

1.  \[星島日報﹕網友論《法證II》唔想汀汀早死\]，2008年6月6日