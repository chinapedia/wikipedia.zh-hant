****（太空实验室2号）是历史上第十九次航天飞机任务，也是[挑战者号航天飞机的第八次太空飞行](../Page/挑戰者號太空梭.md "wikilink")。

## 任务成员

  - **[戈尔登·福勒顿](../Page/戈尔登·福勒顿.md "wikilink")**（，曾执行以及任务），指令长
  - **[罗伊·布里奇斯](../Page/罗伊·布里奇斯.md "wikilink")**（，曾执行任务），飞行员
  - **[斯多里·马斯格雷夫](../Page/斯多里·马斯格雷夫.md "wikilink")**（，曾执行、、、、以及任务），任务专家
  - **[安东尼·英格兰](../Page/安东尼·英格兰.md "wikilink")**（，曾执行任务），任务专家
  - **[卡尔·海因兹](../Page/卡尔·海因兹.md "wikilink")**（，曾执行任务），任务专家
  - **[罗伦·埃克顿](../Page/罗伦·埃克顿.md "wikilink")**（，曾执行任务），有效载荷专家
  - **[约翰-大卫·巴托](../Page/约翰-大卫·巴托.md "wikilink")**（，曾执行任务），有效载荷专家

## 替补有效载荷专家

  - **[乔治·西蒙](../Page/乔治·西蒙.md "wikilink")**（）
  - **[戴安·普林兹](../Page/戴安·普林兹.md "wikilink")**（）

[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:1985年科學](../Category/1985年科學.md "wikilink")
[Category:挑战者号航天飞机任务](../Category/挑战者号航天飞机任务.md "wikilink")
[Category:1985年7月](../Category/1985年7月.md "wikilink")
[Category:1985年8月](../Category/1985年8月.md "wikilink")