**安杰洛斯·查理斯特亚斯**（，'，），[希臘](../Page/希臘.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，擔任[前鋒](../Page/前鋒_\(足球\).md "wikilink")，為[希臘國家足球隊成員](../Page/希臘國家足球隊.md "wikilink")。他最讓人熟悉的是[2004年歐洲國家盃決賽中攻入一球](../Page/2004年歐洲國家盃.md "wikilink")，擊敗主辦國[葡萄牙](../Page/葡萄牙國家足球隊.md "wikilink")，助希臘首次奪得[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")。

2009年冬季[轉會窗期間被](../Page/轉會窗.md "wikilink")[紐倫堡外借到另一間](../Page/纽伦堡足球俱乐部.md "wikilink")[德甲球會](../Page/德甲.md "wikilink")[利華古遜半個球季](../Page/勒沃库森足球俱乐部.md "wikilink")<small>\[1\]</small>。

## 榮譽

### 球會

  - 雲達不來梅;

<!-- end list -->

  - [德國足球冠軍](../Page/德國足球冠軍.md "wikilink")：2004年；
  - [德國盃冠軍](../Page/德國盃.md "wikilink")：2004年；

<!-- end list -->

  - 阿積士

<!-- end list -->

  - [告魯夫盾冠軍](../Page/告魯夫盾.md "wikilink")：2006年；
  - [荷蘭盃冠軍](../Page/荷蘭盃.md "wikilink")：2006年；

<!-- end list -->

  - 利華古遜

<!-- end list -->

  - [德國盃亞軍](../Page/德國盃.md "wikilink")：2009年；

### 國家隊

  - 希臘國家隊

<!-- end list -->

  - [2004年歐洲國家盃冠軍](../Page/2004年歐洲國家盃.md "wikilink")：2004年；

## 參考文獻

## 外部連結

  -
  - [Angelos
    Charisteas](http://www.transfermarkt.de/de/angelos-charisteas/profil/spieler_1519.html)
    at Transfermarkt.de

  - [Player profile on Feyenoord official
    website](https://web.archive.org/web/20110716135333/http://www.feyenoord.nl/pages/datapresenter/s2/playerinfo.aspx?dataid=10010000020756)

  - [Player profile on
    ESPN](http://soccernet-akamai.espn.go.com/players/gamelog?id=15108&cc=5739)

[Category:希臘足球運動員](../Category/希臘足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:塞薩洛尼基阿里斯球員](../Category/塞薩洛尼基阿里斯球員.md "wikilink")
[Category:雲達不萊梅球員](../Category/雲達不萊梅球員.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:飛燕諾球員](../Category/飛燕諾球員.md "wikilink")
[Category:紐倫堡球員](../Category/紐倫堡球員.md "wikilink")
[Category:利華古遜球員](../Category/利華古遜球員.md "wikilink")
[Category:阿萊斯球員](../Category/阿萊斯球員.md "wikilink")
[Category:史浩克零四球員](../Category/史浩克零四球員.md "wikilink")
[Category:希超球員](../Category/希超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:2005年洲際國家盃球員](../Category/2005年洲際國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")

1.