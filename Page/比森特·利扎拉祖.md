**比森特·利扎拉祖**（****，），[法國](../Page/法國.md "wikilink")[巴斯克族人](../Page/巴斯克.md "wikilink")，前[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，司職[左后卫](../Page/左后卫.md "wikilink")，是[1998年世界杯冠軍](../Page/1998年世界杯.md "wikilink")[法國隊成員](../Page/法國國家足球隊.md "wikilink")，也贏得過[2000年歐洲國家盃](../Page/2000年歐洲國家盃.md "wikilink")。

## 生平

利沙拉素屬巴斯克族，由於長年居於巴斯克族聚居的[法國西南部](../Page/法國.md "wikilink")，故職業生涯第一家球會乃法國西南部球會[波爾多足球俱樂部](../Page/波爾多足球俱樂部.md "wikilink")。利扎拉祖曾在[西班牙效力以巴斯克人為骨幹的](../Page/西班牙.md "wikilink")[西甲球會](../Page/西甲.md "wikilink")[毕尔巴鄂竞技](../Page/毕尔巴鄂竞技.md "wikilink")。

1997年加盟[德甲班霸](../Page/德甲.md "wikilink")[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")，是利扎拉祖職業生涯中最光輝時代，當時雄霸德甲的拜仁已贏過多屆德甲聯賽冠軍（1999、2000、2001），2001年更贏得[歐洲聯賽冠軍杯](../Page/歐洲聯賽冠軍杯.md "wikilink")，是利扎拉祖球會獎項中最佳成績。他亦代表過法國國家隊贏得[1998年世界杯冠軍](../Page/1998年世界杯.md "wikilink")，兩年後也贏得2000年歐洲國家杯，為國家隊成為歐洲國家中唯一接連贏得世界杯和歐洲國家杯的國家隊，而他也挾歐國盃之餘風協助拜仁慕尼黑取歐洲聯賽冠軍盃冠軍殊榮和豐田杯冠軍。

雖然職業生涯晚期利扎拉祖水準有所下降，但仍贏得三屆德甲聯賽冠軍（2003、2005、2006）。原本2004年他返回法國加盟[馬賽](../Page/馬賽足球俱樂部.md "wikilink")，但半年後又再重返拜仁，年半後才宣告退役。至於國家隊則在參加[2004年歐洲國家杯後](../Page/2004年歐洲國家杯.md "wikilink")，便宣佈離開。

## 榮譽

  - [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")
      - [德甲聯賽冠軍](../Page/德甲聯賽.md "wikilink")：1998/99, 1999/2000,
        2000/01, 2002/03, 2005/06
      - [德國盃冠軍](../Page/德國盃.md "wikilink")：1998, 2000, 2003, 2005, 2006
      - [德國聯賽盃冠軍](../Page/德國聯賽盃.md "wikilink")：1997, 1998, 1999, 2000
      - [歐洲聯賽冠軍盃冠軍](../Page/歐洲聯賽冠軍盃.md "wikilink")：2001
      - [豐田盃](../Page/洲際盃足球賽.md "wikilink") (洲際盃) 冠軍：2001
  - [法國隊](../Page/法國國家足球隊.md "wikilink")
      - [世界盃冠軍](../Page/世界盃.md "wikilink")：1998
      - [歐洲國家盃冠軍](../Page/歐洲國家盃.md "wikilink")：2000
      - [洲際國家盃](../Page/洲際國家盃.md "wikilink") 冠軍：2001、2003

利扎拉祖是歷史上第二位球員在**球會級**和**國家隊級**賽事同時成為**歐洲**和**世界**冠軍的球員（第一位是其國家隊隊友，同屬[巴斯克族的](../Page/巴斯克.md "wikilink")[-{zh-hans:德尚;zh-hk:迪甘斯;zh-tw:德尚;}-](../Page/迪迪埃·德尚.md "wikilink")。），而且全數均以主力身份出席決賽：

  - 1998年代表**法國隊**，在**世界盃**決賽以3-0擊敗[巴西隊](../Page/巴西國家足球隊.md "wikilink")，贏得世界冠軍（國家隊）。\[1\]
  - 2000年代表**法國隊**，在**歐洲國家盃**決賽以加時2-1擊敗[意大利隊](../Page/意大利國家足球隊.md "wikilink")，贏得歐洲冠軍（國家隊））。\[2\]
  - 2001年代表**拜仁慕尼黑**，在**歐洲聯賽冠軍盃**決賽以加時1-1（互射十二碼5-4）擊敗[巴伦西亚](../Page/巴伦西亚足球俱乐部.md "wikilink")，贏得歐洲冠軍（球會）。\[3\]
  - 2001年代表**拜仁慕尼黑**，在**豐田盃**決賽以1-0擊敗[小保加](../Page/小保加體育會.md "wikilink")，贏得世界冠軍（球會）。\[4\]

## 參考資料

<div class="references-small">

<references />

[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:法國足球運動員](../Category/法國足球運動員.md "wikilink")
[Category:巴斯克足球運動員](../Category/巴斯克足球運動員.md "wikilink")
[Category:波爾多球員](../Category/波爾多球員.md "wikilink")
[Category:畢爾包球員](../Category/畢爾包球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2001年洲際國家盃球員](../Category/2001年洲際國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:洲際國家盃冠軍隊球員](../Category/洲際國家盃冠軍隊球員.md "wikilink")

1.  [World Cup 1998](http://www.rsssf.com/tables/98full.html)
    [**RSSSF**](../Page/RSSSF.md "wikilink") 1998年世界盃
2.  [European
    Championship 2000](http://www.rsssf.com/tables/00e-final.html#fin)
    RSSSF 2000年歐洲國家盃
3.  [UEFA European
    Competitions 2000-01](http://www.rsssf.com/ec/ec200001.html) RSSSF
    2001年歐洲聯賽冠軍盃
4.  [Intercontinental Club
    Cup 2001](http://www.rsssf.com/tablest/toyota01.html) RSSSF 2001年豐田盃