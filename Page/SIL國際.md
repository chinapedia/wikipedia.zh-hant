**美国國際语言暑期学院**（**SIL
International**）是一個國際性、非牟利、宗教性的科學組織，主要在研習、開發及記錄一些比較鮮為人知的語言，藉以擴展語言學知識、推動世界識字率及扶助少數族裔的語文發展。它透過它的旗艦網站「[民族語](../Page/民族語.md "wikilink")」來為網民提供其各項研究數據。

**SIL**是英語**Summer Institute of
Linguistics**的簡寫，意即“暑期語言學院”的意思。它於1934年在[美國](../Page/美國.md "wikilink")[阿肯色州開辦](../Page/阿肯色州.md "wikilink")，原來的用意是為接受[差傳事工的](../Page/基督教傳教活動.md "wikilink")[宣教士提供一個暑期語言訓練](../Page/宣教士.md "wikilink")，使他們掌握基本的語言學、人類學及翻譯學的基本原理，以便其後參與[聖經的翻譯工作](../Page/聖經.md "wikilink")。這些受訓後的翻譯事工其後被稱為“[威克理夫圣经翻译会](../Page/威克里夫圣经翻译协会.md "wikilink")”。SIL的創辦人是[金纶·汤逊](../Page/金纶·汤逊.md "wikilink")（1896年－1982年），是一位被派往[危地馬拉的宣教士](../Page/危地馬拉.md "wikilink")。在他的第一屆學生中，有一位名叫（1912年－2000年），後來成為了SIL歷史上最重要的人物。他在1942年至1979年期間一直擔任SIL的總裁，之後擔任名譽總裁，直到2000年他逝世為止。派克曾於1982年至1996年連續15年獲提名[諾貝爾和平獎](../Page/諾貝爾和平獎.md "wikilink")。SIL的現任總裁是2008年上任的[Dr.
John
Watters](../Page/Dr._John_Watters.md "wikilink")。他從2000年至2007年間擔任執行長。

SIL到現在仍然是[聯合國及](../Page/聯合國.md "wikilink")[UNESCO的正式顧問](../Page/UNESCO.md "wikilink")。SIL亦於世界多個高等院校為世界各國的[非政府組織提供語言學課程的導師及教材](../Page/非政府組織.md "wikilink")。在美國，這些課程主要於以下大學舉辦：

  - [拜歐拉大學](../Page/拜歐拉大學.md "wikilink")

  - [慕迪聖經學院](../Page/慕迪聖經學院.md "wikilink")

  -
  -
  -
  -
  - [阿靈頓德薩斯大學](../Page/德克薩斯大學系統.md "wikilink")

  - [俄勒岡大學](../Page/俄勒岡大學.md "wikilink")

  - [達拉斯神學院](../Page/達拉斯神學院.md "wikilink")

而在美國以外，則主要位於[加拿大的](../Page/加拿大.md "wikilink")及[澳洲的](../Page/澳洲.md "wikilink")[查爾斯達爾文大學](../Page/查爾斯達爾文大學.md "wikilink")。

SIL國際總部的地址如下：

  -
    7500 West Camp Wisdom Road
    Dallas, Texas, 75236 USA

## SIL代號與《民族語》

## 字型及軟體

SIL開發了不少軟體和出版物。當中有付費的和免費的。以下為部份由他們開發的免費字型、字型技術及多語言編輯軟體

  - [字型](../Page/字型.md "wikilink")
      - [Charis SIL](../Page/Charis_SIL.md "wikilink")
      - [Doulos SIL](../Page/Doulos_SIL.md "wikilink")
      - [Gentium](../Page/Gentium.md "wikilink")
  - 字型技術
      - [Graphite](../Page/Graphite.md "wikilink")
  - 軟體
      - [WorldPad](../Page/WorldPad.md "wikilink")

## 參看

  - 《[民族語](../Page/民族語.md "wikilink")》
  - [ISO 639-3](../Page/ISO_639-3.md "wikilink")

## 外部連結

  - [SIL國際](http://www.sil.org/) -
    [软件及字体](http://www.sil.org/resources/software_fonts)
  - [民族語](http://www.ethnologue.com/)

[Category:语言学](../Category/语言学.md "wikilink")
[Category:国际组织](../Category/国际组织.md "wikilink")
[Category:麥格塞塞獎獲得者](../Category/麥格塞塞獎獲得者.md "wikilink")
[Category:美國民間組織](../Category/美國民間組織.md "wikilink")