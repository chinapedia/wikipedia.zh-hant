**纯净水**，简称**净水**或**纯水**，文義上是纯洁、干净，不含有[杂质或](../Page/杂质.md "wikilink")[细菌的水](../Page/细菌.md "wikilink")，是以符合生活[饮用水卫生标准的](../Page/饮用水.md "wikilink")[水为原水](../Page/水.md "wikilink")，通过[电渗析器法](../Page/电渗析器.md "wikilink")、[离子交换器法](../Page/离子交换器.md "wikilink")、[反渗透法](../Page/反渗透法.md "wikilink")、[蒸馏法及其他适当的加工方法制得而成](../Page/蒸馏.md "wikilink")，密封于容器内，且不含任何[添加物](../Page/添加物.md "wikilink")，无色透明，若食品標準下製造則可直接饮用。市场上出售的[太空水](../Page/太空水.md "wikilink")，[蒸馏水均属纯净水](../Page/蒸馏水.md "wikilink")。有时候这个词也和[化学](../Page/化学.md "wikilink")[实验室中提炼的](../Page/实验室.md "wikilink")[蒸馏水或去離子水類似](../Page/蒸馏水.md "wikilink")，但是製程則有所差異，純度的品質也有不同。

## 纯水器

根据报道纯水器对[阴离子合成](../Page/阴离子.md "wikilink")[洗涤剂](../Page/洗涤剂.md "wikilink")、[氯仿](../Page/氯仿.md "wikilink")、[硝酸盐](../Page/硝酸盐.md "wikilink")、[氨氮](../Page/氨氮.md "wikilink")、[亚硝酸盐氮及细菌指标净化效果较好](../Page/亚硝酸盐.md "wikilink")，特别是反渗透型（RO），除盐率高，可除去对人体健康有害或潜在的危害物质。但在纯水过程中对人体必需[微量元素如](../Page/微量元素.md "wikilink")：[Cu](../Page/铜.md "wikilink")、[Fe](../Page/铁.md "wikilink")、[Zn](../Page/锌.md "wikilink")、[F或](../Page/氟.md "wikilink")[宏量元素](../Page/宏量元素.md "wikilink")[S](../Page/硫.md "wikilink")、[P](../Page/磷.md "wikilink")、[Ca](../Page/钙.md "wikilink")、[Mg](../Page/镁.md "wikilink")、[Na](../Page/钠.md "wikilink")、[K也被除去](../Page/钾.md "wikilink")，但其所占日常摄入比例微乎其微。\[1\]
\[2\]

## 淨化方法

### 去离子

**去离子水**（Deionized water、DI water、de-ionized water或deionised
water），是自然界的水去掉了[钠](../Page/钠.md "wikilink")、[钙](../Page/钙.md "wikilink")、[铁](../Page/铁.md "wikilink")、[铜等元素的](../Page/铜.md "wikilink")[阳离子以及](../Page/阳离子.md "wikilink")[氯](../Page/氯.md "wikilink")、[溴等元素的](../Page/溴.md "wikilink")[阴离子后的](../Page/阴离子.md "wikilink")[水](../Page/水.md "wikilink")。这意味着，除了[H<sub>3</sub>O<sup>+</sup>和](../Page/水合氢离子.md "wikilink")[OH<sup>−</sup>外](../Page/氢氧根离子.md "wikilink")，去离子水中不含有其他任何离子成分，但仍可能有一些有机物以非离子形态存在于其中。去离子水可通过离子交换分离等过程生产。\[3\]

## 参见

  - [純水](../Page/純水.md "wikilink")
  - [超純水](../Page/超純水.md "wikilink")
  - [蒸馏水](../Page/蒸馏水.md "wikilink")
  - [去离子水](../Page/去离子水.md "wikilink")

## 参考资料

[category:饮用水](../Page/category:饮用水.md "wikilink")

[Category:液態水](../Category/液態水.md "wikilink")
[Category:蒸馏](../Category/蒸馏.md "wikilink")
[Category:冷冻剂](../Category/冷冻剂.md "wikilink")
[Category:供水](../Category/供水.md "wikilink")
[Category:赋形剂](../Category/赋形剂.md "wikilink")
[Category:过滤](../Category/过滤.md "wikilink")

1.
2.
3.  [去离子水](http://van.hep.uiuc.edu/van/qa/section/States_of_Matter_and_Energy/Properties_of_Water/20050310005737.htm)
     (Physics Van QA Forum)