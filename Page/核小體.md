**核小體**（，也译作**核體**或**核仁小體**等）是組成[真核生物](../Page/真核生物.md "wikilink")[染色質](../Page/染色質.md "wikilink")（除[精子染色質外](../Page/精子.md "wikilink")）的基本單位。

核小体是由[DNA與](../Page/DNA.md "wikilink")4對[組織蛋白](../Page/組織蛋白.md "wikilink")（共8個）组成的複合物，其中有[H2A和](../Page/組織蛋白H2A.md "wikilink")[H2B的](../Page/組織蛋白H2B.md "wikilink")[二聚體兩組以及](../Page/二聚體.md "wikilink")[H3和](../Page/組織蛋白H3.md "wikilink")[H4的二聚體兩組](../Page/組織蛋白H4.md "wikilink")。另外還有一種[H1負責連結兩個核小體之間的DNA](../Page/組織蛋白H1.md "wikilink")。

核小體假說是在1974年，由Don Olins、Ada
Olins\[1\]與[羅傑·科恩伯格](../Page/羅傑·科恩伯格.md "wikilink")\[2\]\[3\]等人首次提出的。

[Chromatin_Structures.png](https://zh.wikipedia.org/wiki/File:Chromatin_Structures.png "fig:Chromatin_Structures.png")
[Nucleosome_structure.png](https://zh.wikipedia.org/wiki/File:Nucleosome_structure.png "fig:Nucleosome_structure.png")

## 参见

  - [前核小体](../Page/前核小体.md "wikilink")

## 參考文獻

## 外部链接

  - [Nucleosomes on the group page of Timothy
    Richmond](https://web.archive.org/web/20070530005928/http://www.mol.biol.ethz.ch/groups/richmond/projects/nucleosome)
  - [蛋白质数据库中有关核小体的资料](https://web.archive.org/web/20090111044350/http://pdb.rcsb.org/pdb/static.do?p=education_discussion%2Fmolecule_of_the_month%2Fpdb7_1.html)
  - [Dynamic Remodeling of Individual Nucleosomes Across a Eukaryotic
    Genome in Response to Transcriptional
    Perturbation](https://web.archive.org/web/20090803083240/http://www.scivee.tv/node/5532)
  - [Catalog of servers with experimental data and prediction tools for
    nucleosome
    positioning](http://generegulation.info/index.php?option=com_content&view=article&id=10&Itemid=16)

[Category:分子生物学](../Category/分子生物学.md "wikilink")
[Category:表觀遺傳學](../Category/表觀遺傳學.md "wikilink")

1.  AL and Olins DE, ["Spheroid Chromatin Units (nu
    Bodies)"](http://www.sciencemag.org/cgi/content/abstract/183/4122/330),
    Science (1974); 183: 330 - 332
2.  McDonald D, "Milestone 9, (1973-1974) The nucleosome hypothesis: An
    alternative string theory", Nature Milestones: Gene Expression.
    (2005) Dec 1;
    <http://www.nature.com/milestones/geneexpression/milestones/articles/milegene09.html>
3.  Kornberg, RD,
    \[<http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=4825889&query_hl=1&itool=pubmed_Abstract>,"Chromatin
    structure: a repeating unit of histones and DNA"\], Science. (1974);
    184: 868–871