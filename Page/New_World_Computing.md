**New World
Computing**（缩写：****）\[1\]是由[乔·万·坎赫姆和](../Page/乔·万·坎赫姆.md "wikilink")[Marc
Caldwell于](../Page/Marc_Caldwell.md "wikilink")1984年创建的一家[电脑游戏开发公司](../Page/电脑游戏.md "wikilink")，尤以[角色扮演游戏系列](../Page/電子角色扮演遊戲.md "wikilink")[魔法门以及它的衍生系列](../Page/魔法门.md "wikilink")[魔法门之英雄无敌而著称](../Page/魔法门之英雄无敌.md "wikilink")。该公司在1996年被[3DO公司收购](../Page/3DO公司.md "wikilink")。2003年，随3DO公司的破产而消失。同年，[育碧买下了魔法门系列品牌的](../Page/育碧软件.md "wikilink")[特许经营权](../Page/特许经营.md "wikilink")。

## 制作的游戏

该公司发行的游戏主要有[魔法门系列](../Page/魔法门.md "wikilink")（1－9代），[魔法门之英雄无敌系列](../Page/魔法门之英雄无敌.md "wikilink")（1－4代及若干资料片）等。

## 注释

[Category:美國電子遊戲公司](../Category/美國電子遊戲公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[Category:软件公司](../Category/软件公司.md "wikilink")
[Category:加利福尼亚州娱乐公司](../Category/加利福尼亚州娱乐公司.md "wikilink")

1.  罕用中文直译：新世界电脑