**財閥解體**（）指的是1945年到1951年7月10日期間的[盟軍統治時期](../Page/盟軍統治時期.md "wikilink")，[同盟國最高司令官總司令部](../Page/同盟國最高司令官總司令部.md "wikilink")（GHQ）的對[日本](../Page/日本.md "wikilink")[占領](../Page/军事占领.md "wikilink")[政策之一](../Page/政策.md "wikilink")。其主要目的是拆解被視為「[侵略戰爭的](../Page/侵略戰爭.md "wikilink")[經濟基礎](../Page/經濟.md "wikilink")」的[財閥](../Page/財閥.md "wikilink")，也是「經濟[民主化](../Page/民主化.md "wikilink")」政策之一。

## 過程

1945年10月16日，GHQ經濟科學局局長[雷蒙·C·科雷姆](../Page/雷蒙·C·科雷姆.md "wikilink")（Raymond
C.
Kramer）發布聲明，期望[日本政府自發實施財閥解體](../Page/日本政府.md "wikilink")。1945年11月4日，日本政府向GHQ提交財閥解體計畫。1945年11月2日，GHQ下令凍結日本「15大[財閥](../Page/財閥.md "wikilink")」〔[三井財閥](../Page/三井財閥.md "wikilink")、[三菱財閥](../Page/三菱財閥.md "wikilink")、[住友財閥](../Page/住友財閥.md "wikilink")、[安田財閥](../Page/安田財閥.md "wikilink")、[鮎川財閥](../Page/鮎川財閥.md "wikilink")（[日產康采恩](../Page/日產康采恩.md "wikilink")）、[淺野財閥](../Page/淺野財閥.md "wikilink")、[古河財閥](../Page/古河財閥.md "wikilink")、[大倉財閥](../Page/大倉財閥.md "wikilink")、[中島財閥](../Page/中島飛機.md "wikilink")、[野村財閥](../Page/野村財閥.md "wikilink")、[澀澤財閥](../Page/澀澤財閥.md "wikilink")、[神戶川崎財閥](../Page/神戶川崎財閥.md "wikilink")、[理研康采恩](../Page/理研康采恩.md "wikilink")、[日窒康采恩](../Page/日窒康采恩.md "wikilink")、[日曹康采恩](../Page/日曹康采恩.md "wikilink")〕的資產。1945年11月6日，GHQ駐日盟軍總司令[道格拉斯·麥克阿瑟修正並認可日本政府提交的財閥解體計畫](../Page/道格拉斯·麥克阿瑟.md "wikilink")，麥克阿瑟保留監督、檢查財閥解體實施情形的[權力](../Page/權力.md "wikilink")。

[Seizure_of_the_Zaibatsu_families_assets.JPG](https://zh.wikipedia.org/wiki/File:Seizure_of_the_Zaibatsu_families_assets.JPG "fig:Seizure_of_the_Zaibatsu_families_assets.JPG")
1946年4月4日，GHQ核准《[控股公司整理委員會令](../Page/控股公司.md "wikilink")》（）草案。1946年4月20日，《控股公司整理委員會令》公布施行，日本政府籌備成立「控股公司整理委員會」（）。1946年6月3日，GHQ下令限制日本10大財閥家族的個人[金融活動](../Page/金融.md "wikilink")。1946年8月8日，控股公司整理委員會開始運作，命令被整肅的控股公司和財閥家族必須將持有的[股票交由控股公司整理委員會處理](../Page/股票.md "wikilink")。

1947年4月14日，日本政府公布《[反托拉斯法](../Page/反托拉斯法.md "wikilink")》（）。1947年7月1日，《反托拉斯法》部份施行，[公平交易委員會開始運作](../Page/公平交易委員會_\(日本\).md "wikilink")。1947年7月20日，《反托拉斯法》全面施行。1947年11月，日本政府公布《財閥同族支配力排除法》（），解除與財閥有關的[董事與](../Page/董事.md "wikilink")[監事的職務](../Page/監事.md "wikilink")。1947年12月18日，日本政府公布《過度經濟力集中排除法》（），命令具有壟斷性的公司分拆。

1948年2月，原本[三井物產要被分拆為](../Page/三井物產.md "wikilink")200多個公司、[三菱商事要被分拆為](../Page/三菱商事.md "wikilink")139個公司，但此時GHQ態度出現轉變：由積極改革日本戰前的政經模式改為促進日本經濟復原。這個轉變普遍被認為是因為[冷戰剛爆發](../Page/冷戰.md "wikilink")，美國認為一個經濟繁榮、增長迅速的日本能遏止或牽制[共產主義的擴張](../Page/共產主義.md "wikilink")，並有利改善和鞏固[美日關係](../Page/美日關係.md "wikilink")，爭取日本成為美國於亞洲的一個可靠的[反共同盟](../Page/反共.md "wikilink")，因而為了加速日本戰後的經濟復原而減低了財閥解體政策的強度。最後，實際被分拆的只有11家公司：[日本製鐵](../Page/日本製鐵.md "wikilink")、[三菱重工業](../Page/三菱重工業.md "wikilink")、[王子製紙](../Page/王子製紙.md "wikilink")、[大日本麥酒](../Page/大日本麥酒.md "wikilink")、[帝國纖維](../Page/帝國纖維.md "wikilink")、[東洋製罐](../Page/東洋製罐.md "wikilink")、[大建產業](../Page/大建產業.md "wikilink")、[三菱礦業](../Page/三菱礦業.md "wikilink")、[三井礦山](../Page/三井礦山.md "wikilink")、[井華礦業](../Page/井華礦業.md "wikilink")、[北海道酪農協同組合](../Page/北海道酪農協同組合.md "wikilink")。1951年7月10日，日本政府宣布財閥解體完成，控股公司整理委員會解散，公平交易委員會承接其業務。1955年7月25日，日本政府廢止《過度經濟力集中排除法》。

## 關聯條目

  - [財閥](../Page/財閥.md "wikilink")
  - [同盟國軍事佔領日本](../Page/同盟國軍事佔領日本.md "wikilink")

[Category:日本戰後處理](../Category/日本戰後處理.md "wikilink")
[Category:昭和時代戰後經濟](../Category/昭和時代戰後經濟.md "wikilink")
[Category:日本財閥](../Category/日本財閥.md "wikilink")