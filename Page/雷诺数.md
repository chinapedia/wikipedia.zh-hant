在[流体力学中](../Page/流体力学.md "wikilink")，**雷诺数**（Reynolds
number）是[流体的](../Page/流体.md "wikilink")[惯性力](../Page/惯性.md "wikilink")\(\frac{\rho v^2}{L}\)与[黏性力](../Page/黏性.md "wikilink")\(\frac{\mu v}{L^2}\)比值的量度，它是一个[無量纲量](../Page/無量纲量.md "wikilink")。

雷諾數較小時，黏滯力對流場的影響大於慣性力，流場中流速的擾動會因黏滯力而衰減，流體流動穩定，為[層流](../Page/層流.md "wikilink")；反之，若雷諾數較大時，慣性力對流場的影響大於黏滯力，流體流動較不穩定，流速的微小變化容易發展、增強，形成紊亂、不規則的[紊流流場](../Page/紊流.md "wikilink")。

## 定义

对于不同的流场，雷诺数可以有很多表达方式。这些表达方式一般都包括流体性质（[密度](../Page/密度.md "wikilink")、[黏度](../Page/黏度.md "wikilink")）再加上流体速度和一个特征长度或者特征尺寸。这个尺寸一般是根据习惯定义的。比如说半径和直径对于球型和圆形并没有本质不同，但是习惯上只用其中一个。对于管内流动和在流场中的球体，通常使用直径作为特征尺寸。对于表面流动，通常使用长度。

### 管内流场

对于在管内的流动，雷诺数定义为：

\[\mathrm{Re} = {{\rho {\mathbf \mathrm V} D} \over {\mu}}= {{{\mathbf \mathrm V} D} \over {\nu}} = {{{\mathbf \mathrm Q} D} \over {\nu}A}\]

式中：

  - \({\mathbf \mathrm V}\)是平均流速（[国际单位](../Page/国际单位.md "wikilink")：m/s）
  - \({D}\)管直径（一般為[特徵長度](../Page/特徵長度.md "wikilink")）(m)
  - \({\mu}\)流体[动力黏度](../Page/黏滯係數.md "wikilink")（Pa·s或N·s/m²）
  - \({\nu}\)
    [运动黏度](../Page/黏度.md "wikilink")（\(\nu = \mu  /\)*ρ*）(m²/s)
  - \({\rho}\)流体[密度](../Page/密度.md "wikilink")（kg/m³）
  - \({Q}\)体积[流量](../Page/流量.md "wikilink")（m³/s）
  - \({A}\)横截面积（m²）

假如雷諾數的體積流速固定，則雷諾數與密度（ρ）、速度的开方（\(\sqrt{u}\)）成正比；與管徑（Ｄ）和黏度（ｕ）成反比

假如雷諾數的質量流速（即是可以穩定流動）固定，則雷諾數與管徑（Ｄ）、黏度（ｕ）成反比；與√速度（\(\sqrt{u}\)）成正比；與密度（ρ）無關

### 平板流

对于在两个宽板(板宽远大于两板之间距离)之间的流动,特征长度为两倍的两板之间距离

### 流体中的物体

对于流体中的物体的雷诺数,经常用**Re<sub>p</sub>**表示。用雷诺数可以研究物体周围的流动情况，是否有[漩涡分离](../Page/漩涡分离.md "wikilink")，还可以研究沉降速度。

#### 流体中的球

对于在流体中的球，特征长度就是这个球的直径，特征速度是这个球相对于远处流体的速度，密度和黏度都是流体的性质。在这种情况下，层流只存在于Re=10或者以下。
在小雷诺数情况下，力和运动速度的关系遵从[斯托克斯定律](../Page/斯托克斯定律.md "wikilink")。

球在流体中的雷诺数可以用下式计算，其中\(v_f\)为流体速度，\(v_s\)为球速度，\(d_s\)为球直径，\(\rho_f\)为流体密度，\(\mu_f\)为流体粘度\[1\]。

\(Re = \frac{|v_f-v_s| d_s \rho_f}{\mu_f}\)

### 搅拌槽

对于一个圆柱形的搅拌槽，中间有一个旋转的桨或者涡轮，特征长度是这个旋转物体的直径。速度是ND,N是转速(周/秒)。雷诺数表达为:

\[\mathrm{Re} = {{\rho N D^2} \over {\mu}}.\]
当Re\>10,000时，这个系统为完全湍流状态。\[2\]

## 过渡流雷诺数

对于流过平板的[边界层](../Page/边界层.md "wikilink")，实验可以确认，当流过一定长度后,层流变得不稳定形成湍流。对于不同的尺度和不同的流体，这种不稳定性都会发生。一般来说，当\(\mathrm{Re}_x \approx 5 \times 10^5\),
这里x是从平板的前边缘开始的距离,流速是边界层以外的自由流场速度。

一般管道流雷诺数＜2100为[层流](../Page/层流.md "wikilink")(又可稱作黏滯流動、線流)状态，大于4000为[湍流](../Page/湍流.md "wikilink")(又可稱作紊流、擾流)状态，2100～4000为过渡流状态。

層流：流體沿著管軸以平行方向流動，因為流體很平穩，所以可看作層層相疊，各層間不互相干擾。流體在管內速度分佈為拋物體的形狀，面向切面的則是拋物線分佈。因為是個別有其方向和速率流動，所以流動摩擦損失較小。

湍流：此則是管內流體流動狀態為各分子互相激烈碰撞，非直線流動而是漩渦狀，流動摩擦損失較大。

## 管道中的摩擦阻力

[Moody_EN.svg](https://zh.wikipedia.org/wiki/File:Moody_EN.svg "fig:Moody_EN.svg")說明達西摩擦因子*f*和雷诺数和相對粗糙度的關係\]\]
在管道中完全成形（fully
developed）流體的壓降可以用[穆迪圖來說明](../Page/穆迪圖.md "wikilink")，穆迪圖繪製出在不同相對粗糙度下，達西摩擦因子*f*和雷诺数\({\mathrm{Re}}\)及相對粗糙度\(\epsilon / D\)的關係，圖中隨著雷诺数的增加，[管流由層流變為过渡流及湍流](../Page/管流.md "wikilink")，管流的特性和流體為层流、过渡流或湍流有明顯關係。

## 流动相似性

两个流动如果相似的话，他们必须有相同的几何形状和相同的雷诺数和[欧拉数](../Page/欧拉数_\(物理学\).md "wikilink")。当在模型和真实的流动之间比较两个流体中相应的一点，如下关系式成立：

\[\mathrm{Re}_m = \mathrm{Re} \;\]

  -
    \(\mathrm{Eu}_m = \mathrm{Eu} \;   \quad\quad     \mbox{i.e.}   \quad  {p_m \over \varrho_m {v_m}^{2}} = {p\over \varrho v^{2}} \; ,\)

带m下标的表示模型里的量，其他的表示实际流动里的量。
这样工程师们就可以用缩小尺寸的[水槽或者](../Page/水槽.md "wikilink")[风洞来进行试验](../Page/风洞.md "wikilink")，与数值模拟的模型比对数据分析，节约试验成本和时间。实际应用中也许会需要其他的[无量纲量与模型一致](../Page/无量纲量.md "wikilink")，比如说[马赫数](../Page/马赫数.md "wikilink")，[福祿數](../Page/福祿數.md "wikilink")。

以下是一些雷诺数的例子\[3\]\[4\]：

  - [纤毛虫](../Page/纤毛虫.md "wikilink")\~ 1×10<sup>−1</sup>
  - 最小的魚 ～1
  - [大脑中的](../Page/大脑.md "wikilink")[血液流](../Page/血液.md "wikilink")
    ～1×10<sup>2</sup>
  - [主动脉中的血流](../Page/主动脉.md "wikilink")\~ 1×10<sup>3</sup>

**湍流临界值**\~
2.3×10<sup>3</sup>-5.0×10<sup>4</sup>(对于管内流)到10<sup>6</sup>(边界层)

  - [棒球](../Page/棒球.md "wikilink")（[美國職業棒球大聯盟投手投球](../Page/美國職業棒球大聯盟.md "wikilink")）\~2×10<sup>5</sup>
  - [游泳](../Page/游泳.md "wikilink")（人）\~4×10<sup>6</sup>
  - 最快的魚 \~1×10<sup>8</sup>
  - [蓝鲸](../Page/蓝鲸.md "wikilink")\~ 3×10<sup>8</sup>
  - 大型邮轮（）\~ 5×10<sup>9</sup>

## 雷诺数的推导

雷诺数可以从[无量纲的非可压](../Page/无量纲.md "wikilink")[納維－斯托克斯方程推导得来](../Page/納維－斯托克斯方程.md "wikilink"):

\[\rho \left(\frac{\partial \mathbf{v}}{\partial t} + \mathbf{v} \cdot \nabla \mathbf{v}\right) = -\nabla p + \mu \nabla^2 \mathbf{v} + \mathbf{f}.\]

上式中每一项的单位都是加速度乘以密度。无量纲化上式，需要把方程变成一个独立于物理单位的方程。我们可以把上式乘以系数:

\[\frac{D}{\rho V^2}\] 这里的字母跟在雷诺数定义中使用的是一样的。我们设:

\[\mathbf{v'} = \frac{\mathbf{v}}{V},\ p' = p\frac{1}{\rho V^2}, \ \mathbf{f'} = \mathbf{f}\frac{D}{\rho V^2}, \ \frac{\partial}{\partial t'} = \frac{D}{V} \frac{\partial}{\partial t}, \ \nabla' = D \nabla\]

无量纲的纳维-斯托克斯方程可以写为:

\[\frac{\partial \mathbf{v'}}{\partial t'} + \mathbf{v'} \cdot \nabla' \mathbf{v'} = -\nabla' p' + \frac{\mu}{\rho D V} \nabla'^2 \mathbf{v'} + \mathbf{f'}\]

这里\[\frac{\mu}{\rho D V} = \frac{1}{\mathit{Re}}.\]

最后,为了阅读方便把撇去掉:

\[\frac{\partial \mathbf{v}}{\partial t} + \mathbf{v} \cdot \nabla \mathbf{v} = -\nabla p + \frac{1}{\mathit{Re}} \nabla^2 \mathbf{v} + \mathbf{f}.\]

这就是为什么在数学上所有的具有相同雷诺数的流场是相似的。

## 参见

  - [磁雷诺数](../Page/磁雷诺数.md "wikilink")

## 參考文獻

<references/>

[Category:流体力学](../Category/流体力学.md "wikilink")
[Category:无量纲](../Category/无量纲.md "wikilink")

1.
2.  R. K. Sinnott *Coulson & Richardson's Chemical Engineering, Volume
    6: Chemical Engineering Design,* 4th ed (Butterworth-Heinemann) ISBN
    0-7506-6538-6 page 473
3.
4.