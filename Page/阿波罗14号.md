**阿波罗14号**（****）是[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[阿波罗计划中的第八次载人任务](../Page/阿波罗计划.md "wikilink")，是人类第三次成功登月的载人登月任务。

## 任务成员

  - **[艾倫·雪帕德](../Page/艾倫·雪帕德.md "wikilink")**（，曾执行[水星-红石3号以及阿波罗](../Page/水星-红石3号.md "wikilink")14号任务），指令长
  - **[斯图尔特·罗萨](../Page/斯图尔特·罗萨.md "wikilink")**（，曾执行阿波罗14号任务），[指令舱驾驶员](../Page/阿波罗指令/服务舱.md "wikilink")
  - **[艾德加·米切尔](../Page/艾德加·米切尔.md "wikilink")**（，曾执行阿波罗14号任务），[登月舱驾驶员](../Page/阿波罗登月舱.md "wikilink")

### 替补成员

<small>替补成员同样接受任务训练，在主力成员因各种原因无法执行任务时接替。</small>

  - **[尤金·塞尔南](../Page/尤金·塞尔南.md "wikilink")**（，曾执行[双子星9A号](../Page/双子星9A号.md "wikilink")、[阿波罗10号以及](../Page/阿波罗10号.md "wikilink")[阿波罗17号任务](../Page/阿波罗17号.md "wikilink")），指令长
  - **[罗纳德·埃万斯](../Page/罗纳德·埃万斯.md "wikilink")**（，曾执行阿波罗17号任务），指令/服务舱驾驶员
  - **[约瑟夫·恩格](../Page/约瑟夫·恩格.md "wikilink")**（，曾执行以及任务），登月舱驾驶员

### 支持团队

<small>支持团队并不接受任务训练，但被要求能够在会议时代替某位宇航员，并参与任务计划的细节敲定。他们也经常在任务被执行时担任地面通讯任务。</small>

  - **[菲利普·查普曼](../Page/菲利普·查普曼.md "wikilink")**（，从未进入太空）
  - **[布鲁斯·麦克坎德雷斯](../Page/布鲁斯·麦克坎德雷斯.md "wikilink")**（，曾执行以及任务）
  - **[威廉·波格](../Page/威廉·波格.md "wikilink")**（，曾执行[天空实验室4号任务](../Page/天空实验室4号.md "wikilink")）
  - **[戈尔登·福勒顿](../Page/戈尔登·福勒顿.md "wikilink")**（，曾执行以及任务）

[缩略图](https://zh.wikipedia.org/wiki/File:Apollo_14_Shepard.jpg "fig:缩略图")
[RobbinsMedallionApollo14ByPhilKonstantin.jpg](https://zh.wikipedia.org/wiki/File:RobbinsMedallionApollo14ByPhilKonstantin.jpg "fig:RobbinsMedallionApollo14ByPhilKonstantin.jpg")

## 參見

  - [太陽系探測器列表](../Page/太陽系探測器列表.md "wikilink")

[A](../Category/1971年佛罗里达州.md "wikilink")
[A14](../Category/阿波罗计划.md "wikilink")
[Category:1971年航天](../Category/1971年航天.md "wikilink")
[Category:1971年2月](../Category/1971年2月.md "wikilink")
[阿波羅14號](../Category/阿波羅14號.md "wikilink")
[Category:載人月球任務](../Category/載人月球任務.md "wikilink")
[Category:樣本取回任務](../Category/樣本取回任務.md "wikilink")