**妫**（[漢語拼音](../Page/漢語拼音.md "wikilink")：，[注音符號](../Page/注音符號.md "wikilink")：）姓，是漢族[上古八大姓之一](../Page/上古八大姓.md "wikilink")。得姓始祖是[舜帝](../Page/舜帝.md "wikilink")。

## 姓氏渊源

單一淵源：源於[有虞氏](../Page/有虞氏.md "wikilink")，出自[上古](../Page/上古.md "wikilink")[高阳氏後裔](../Page/高阳氏.md "wikilink")[舜帝的封地](../Page/舜帝.md "wikilink")，屬於以居邑名稱為姓。

舜出生于姚墟，當舜還是個平民的時候，就有德有望，[部落](../Page/部落.md "wikilink")[首領堯十分欣賞他的才幹](../Page/首領.md "wikilink")，於是把兩個女兒[娥皇](../Page/娥皇.md "wikilink")、[女英嫁給了他](../Page/女英.md "wikilink")，並讓他居住於[媯水之邊](../Page/媯水.md "wikilink")。媯水，發源於今[山西省](../Page/山西省.md "wikilink")[永濟市南部](../Page/永濟市.md "wikilink")[歷山](../Page/歷山.md "wikilink")，向西流入[黃河](../Page/黃河.md "wikilink")。另有一處媯水，發源於今[北京](../Page/北京.md "wikilink")[延慶縣東南](../Page/延慶縣.md "wikilink")[軍都山](../Page/軍都山.md "wikilink")，向西南流至[河北省](../Page/河北省.md "wikilink")[懷來縣](../Page/懷來縣.md "wikilink")，注入[永定河](../Page/永定河.md "wikilink")。舜所居之媯水，為源於山西永濟歷山的媯汭河。

媯姓為[黃帝支系有虞氏的後裔](../Page/黃帝.md "wikilink")，帝舜為黃帝曾孫[顓頊的六世孫](../Page/顓頊.md "wikilink")，他繼帝堯之後，登上了[中原地區黃帝](../Page/中原.md "wikilink")[族系最大的部落首領之位](../Page/族系.md "wikilink")，躋至[五帝的行列](../Page/五帝.md "wikilink")，成為[中華文明的先祖之一](../Page/中華文明.md "wikilink")。

史書記載說：「堯帝厘降二女於媯汭，嬪於虞。」媯，就是媯水(今山西永濟)之稱，汭，就是水邊。有虞氏興起於[燕山一帶](../Page/燕山.md "wikilink")，虞是其[部落](../Page/部落.md "wikilink")[圖騰](../Page/圖騰.md "wikilink")。虞，屬於[虎的一種](../Page/虎.md "wikilink")，有虞氏聚居於燕山周圍的媯水，轄有其地，遂以所居之地為氏，姓媯。

媯姓之顯則肇始於虞舜時期。舜帝出生于姚墟，故以姚为姓。待堯將帝位傳到舜以後，舜帝的後代便以堯帝封邑居住的地名作為姓氏，稱媯姓，成為中華民族最為古老的八大始姓之一。

舜之子為[商均](../Page/商均.md "wikilink")，商均在[大禹執政時被封於虞地](../Page/大禹.md "wikilink")(今河南[虞城](../Page/虞城.md "wikilink")，至今還保留著商均墓村)。商均之後為虞思，虞思封於商(今陝西[商縣](../Page/商縣.md "wikilink"))。舜的另一支後裔虞遂定居[虞鄉](../Page/虞鄉.md "wikilink")(今山西永濟)，後受封於[遂國](../Page/遂國.md "wikilink")。商滅[夏時](../Page/夏.md "wikilink")，又移封於陳地(今河南[宛丘](../Page/宛丘.md "wikilink"))。

[西周初期](../Page/西周.md "wikilink")，[虞思的後裔](../Page/虞思.md "wikilink")[虞阏父](../Page/虞阏父.md "wikilink")，他繼承了先祖制陶的手藝，做陶的本領首屈一指，因此獲得周人垂青，擔任了周族陶正之官。[周文王後來還特意將長女](../Page/周文王.md "wikilink")[太姬許配給遏父之子](../Page/太姬.md "wikilink")[胡公滿](../Page/胡公滿.md "wikilink")。

[周武王滅商之後](../Page/周武王.md "wikilink")，分封黃帝、唐堯、虞舜之後，以備[三恪](../Page/三恪.md "wikilink")。[胡公满是帝舜三十二代孫](../Page/胡公满.md "wikilink")，為舜裔的嫡脈，因此受封於陳地(今河南[淮陽](../Page/淮陽.md "wikilink")，其時轄地在河南[開封以東至](../Page/開封.md "wikilink")[安徽](../Page/安徽.md "wikilink")[亳縣以北](../Page/亳縣.md "wikilink")，都城在宛丘)，奉帝舜之祀，建立起又一個[陳國](../Page/陳國.md "wikilink")，取代了虞遂所建的陳國，轄地規模也比前者有了明顯的擴大，從此奉為[正朔](../Page/正朔.md "wikilink")，延續了虞舜的一脈香火。

妫姓是[陈](../Page/陈姓.md "wikilink")、[胡](../Page/胡姓.md "wikilink")、[袁](../Page/袁姓.md "wikilink")、[田](../Page/田姓.md "wikilink")、[陆](../Page/陆姓.md "wikilink")、[王](../Page/王姓.md "wikilink")、[车等等许多姓氏的起源之一](../Page/车姓.md "wikilink")。

## [郡望](../Page/郡望.md "wikilink")

  - **[吳興郡](../Page/吳興郡.md "wikilink")：**《姓氏考略》中記載，媯姓望出[吳興](../Page/吳興.md "wikilink")。[三國](../Page/三國.md "wikilink")[寶鼎元年置郡](../Page/寶鼎.md "wikilink")，其時轄地在今[浙江省](../Page/浙江省.md "wikilink")[臨安至](../Page/臨安.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")[宜興一帶地區](../Page/宜興.md "wikilink")。治所在[烏程](../Page/烏程.md "wikilink")(今[浙江](../Page/浙江.md "wikilink")[湖州](../Page/湖州.md "wikilink"))。

<!-- end list -->

  - **[南郡](../Page/南郡.md "wikilink")：**[秦朝時期置郡](../Page/秦朝.md "wikilink")，其實轄地在[晉](../Page/晉.md "wikilink")[湖北舊](../Page/湖北.md "wikilink")[荊州](../Page/荊州.md "wikilink")、[安陸](../Page/安陸.md "wikilink")、[漢陽](../Page/漢陽.md "wikilink")、[武昌](../Page/武昌.md "wikilink")、[黃州](../Page/黃州.md "wikilink")、[德安](../Page/德安.md "wikilink")、[施南諸府及](../Page/施南.md "wikilink")[襄陽府之南境](../Page/襄陽府.md "wikilink")，治所在[郢](../Page/郢.md "wikilink")，故楚都(今湖北[江陵](../Page/江陵.md "wikilink"))，即今[湖北省](../Page/湖北省.md "wikilink")[江陸縣東南一帶](../Page/江陸縣.md "wikilink")。[漢朝時期置漢陵縣為](../Page/漢朝.md "wikilink")[郡治即今江陵縣](../Page/郡治.md "wikilink")，三國時期[吳國移郡治至公安](../Page/吳國.md "wikilink")(今[湖北](../Page/湖北.md "wikilink")[公安](../Page/公安.md "wikilink"))。到[西晉時期復移郡治於江陵](../Page/西晉.md "wikilink")(今湖北荊州)。[唐朝時期改為江陵郡](../Page/唐朝.md "wikilink")，後升為[江陵府](../Page/江陵府.md "wikilink")。

## 历史名人

  - [胡公满](../Page/胡公满.md "wikilink")：西周至春秋時代諸侯國陳國的開國君主。
  - [妫昌](../Page/妫昌.md "wikilink")：(生卒年待考)，[王莽建國初](../Page/王莽.md "wikilink")，封媯昌為如睦侯，專奉虞帝后，即繼承新朝另一派祖先[虞舜的後代](../Page/虞舜.md "wikilink")。
  - [媯覽](../Page/媯覽.md "wikilink")：(？～204年)，與戴員原是吳郡太守[盛憲故吏](../Page/盛憲.md "wikilink")，盛憲被[孫權殺害後](../Page/孫權.md "wikilink")，覽和員都入山匿藏，後來[孫翊任丹陽太守後就受翊招募而出山任大都督領兵](../Page/孫翊.md "wikilink")。後來與孫翊不和，隨與戴員唆使[邊鴻將孫翊殺害並造反](../Page/邊鴻.md "wikilink")，後被[孫河指責兩人](../Page/孫河.md "wikilink")，使到兩人計劃將孫河殺害，迎接揚州刺史[劉馥](../Page/劉馥.md "wikilink")，正式造反。後孫翊屬下徐元，孫高和傅嬰將媯覽，戴員給殺死。
  - [妫昆](../Page/妫昆.md "wikilink")：(生卒年待考)，[吳郡餘杭人](../Page/吳郡.md "wikilink")(今[浙江](../Page/浙江.md "wikilink")[餘杭](../Page/餘杭.md "wikilink"))。[西晉大臣](../Page/西晉.md "wikilink")。時為南郡太守，曾被[彈劾入獄](../Page/彈劾.md "wikilink")，他的兒子媯皓當時只有十六歲，冒著生命危險，血流滿面地闖進公堂，要求奏理。
  - [妫皓](../Page/妫皓.md "wikilink")：(生卒年待考)，字元起，媯昆之子；吳郡餘杭人(今浙江餘杭)。西晉大臣。其父被人彈劾入獄後，媯皓當時只有十六歲，便冒著生命危險，懷裡揣上小石頭，到衙門口後用石頭打破自己的臉，然後血流滿面地闖進公堂，要求奏理。審理此案官員被其[孝制感動](../Page/孝制.md "wikilink")，上奏朝廷，使其父免於刑事。後官至[尚書郎](../Page/尚書郎.md "wikilink")，明達國典(文質彬彬，通曉政務，符合國家政典要求)。

[\*](../Category/妫姓.md "wikilink")
[Category:中國上古姓氏](../Category/中國上古姓氏.md "wikilink")