[AntDotMap_Livingston.png](https://zh.wikipedia.org/wiki/File:AntDotMap_Livingston.png "fig:AntDotMap_Livingston.png")
**南设得兰群岛**（）是[南极海的一组群岛](../Page/南极海.md "wikilink")，位于[南极半岛以北约](../Page/南极半岛.md "wikilink")120公里。\[1\]根据1959年签订的《[南极条约](../Page/南极条约.md "wikilink")》，该群岛的主权不被缔约国承认或争议，而开放予各国作非军事用途。

[阿根廷自](../Page/阿根廷.md "wikilink")1943年起宣称对南设得兰群岛拥有主权，[智利和](../Page/智利.md "wikilink")[英国分别在](../Page/英国.md "wikilink")1940年和1908年起对其拥有主权，1962年，南设得兰群岛成为[英属南极领地的一部分](../Page/英属南极领地.md "wikilink")。

多个国家在群岛设有科学考察站，这些考察站大多位于该群岛的最大岛屿[乔治王岛](../Page/乔治王岛.md "wikilink")，使用智利[弗雷总统站的机场](../Page/弗雷总统站.md "wikilink")。

## 历史

[Williams-Point.jpg](https://zh.wikipedia.org/wiki/File:Williams-Point.jpg "fig:Williams-Point.jpg")
根据一些历史学家的说法，荷兰人[迪尔克·赫里茨在](../Page/迪尔克·赫里茨.md "wikilink")1599年或西班牙人[加夫列尔·德卡斯蒂利亚在](../Page/加夫列尔·德卡斯蒂利亚.md "wikilink")1603年首次抵达南极群岛，两人都被认为曾在[德雷克海峡以南南设得兰群岛地区航行](../Page/德雷克海峡.md "wikilink")。1819年，商船威廉斯号船长[威廉·史密斯在前往](../Page/威廉·史密斯.md "wikilink")[智利](../Page/智利.md "wikilink")[瓦尔帕莱索途中偏离航道](../Page/瓦尔帕莱索.md "wikilink")，到达[合恩角以南海域](../Page/合恩角.md "wikilink")，2月19日发现[利文斯顿岛北端](../Page/利文斯顿岛.md "wikilink")[威廉斯角](../Page/威廉斯角.md "wikilink")。同年，史密斯重返设得兰群岛，10月16日在[乔治王岛登陆](../Page/乔治王岛.md "wikilink")，宣布[英国对其拥有主权](../Page/英国.md "wikilink")。

同时，西班牙海军舰只[圣泰尔莫号于](../Page/圣泰尔莫号.md "wikilink")1819年9月在穿越德雷克海峡时沉没，其部分残骸数月后在利文斯顿岛北岸被发现。1819年12月至1820年1月期间，[爱德华·布兰斯菲尔德上尉乘坐](../Page/爱德华·布兰斯菲尔德.md "wikilink")[皇家海军租用的威廉斯号考察南设得兰群岛](../Page/皇家海军.md "wikilink")，并制作地图。

早在1819年11月15日，在瓦尔帕莱索的[美国特工杰里米](../Page/美国.md "wikilink")·鲁滨逊把斯密斯的发现和布兰斯菲尔德后来的任务汇报[国务卿](../Page/美国国务卿.md "wikilink")[约翰·昆西·亚当斯](../Page/约翰·昆西·亚当斯.md "wikilink")，并建议美国派遣一艘政府船只探索南设得兰群岛。

群岛的发展吸引了英美两国的海豹捕猎者的注意，第一艘在该区作业的海豹捕猎船是英国商人在布宜诺斯艾利斯租用的双桅横帆船Espirito
Santo号。

在环绕南极大陆航行后，俄国[法比安·戈特利布·冯·别林斯高晋和](../Page/法比安·戈特利布·冯·别林斯高晋.md "wikilink")[米哈伊尔·彼得罗维奇·拉扎列夫带领的南极探险队在](../Page/米哈伊尔·彼得罗维奇·拉扎列夫.md "wikilink")1821年1月到达南设得兰群岛。他们曾在乔治王岛和[象岛登陆](../Page/象岛_\(南极洲\).md "wikilink")，在群岛进行考察，并为岛屿命名。当他们在[欺骗岛和利文斯顿岛之间航行时](../Page/欺骗岛.md "wikilink")，美国英雄号商船船长[纳撒尼尔·帕尔默会见别林斯高晋](../Page/纳撒尼尔·帕尔默.md "wikilink")，并知会他该区有数十艘英国海豹捕猎船作业。

群岛曾被短暂称为新南不列颠，但不久改为现在通用的南设得兰群岛（根据[苏格兰以北的](../Page/苏格兰.md "wikilink")[设得兰群岛](../Page/设得兰群岛.md "wikilink")）。从1908年起，群岛被英国划归[马尔维纳斯群岛](../Page/马尔维纳斯群岛.md "wikilink")（英国称福克兰群岛）属地管辖。

## 地理

[South_Shetland_Islands_Map.png](https://zh.wikipedia.org/wiki/File:South_Shetland_Islands_Map.png "fig:South_Shetland_Islands_Map.png")
南设得兰群岛位于南纬61° 00'至63° 37'、西经53° 83'至62°
83'的范围之内，位于马尔维纳斯群岛以南约1,200公里，距离南极大陆约150公里。群岛由11个大岛和一些小岛组成，总陆地面积为3,687平方公里，约80至90%的土地终年被冰川覆盖。群岛的最高点是位于[史密斯岛的](../Page/史密斯岛_\(南设得兰群岛\).md "wikilink")[福斯特山](../Page/福斯特山.md "wikilink")，海拔高程为2,105米。群岛从西南偏南的[史密斯岛到东北偏北的](../Page/史密斯岛_\(南设得兰群岛\).md "wikilink")[象岛](../Page/象岛_\(南极洲\).md "wikilink")，延绵约450公里。

## 气候

南设得兰群岛和北大西洋的[法罗群岛与赤道的距离相约](../Page/法罗群岛.md "wikilink")，但由于前者接近南极大陆，气候远较后者寒冷。群岛附近的海洋每年从4月上旬到12月上旬都会被海冰覆盖，每年4月至11月的平均气温低于0摄氏度。

群岛的[冰川近年正在后退](../Page/冰川.md "wikilink")，但全岛80%以上面积在夏季仍被冰雪覆盖。该区全年多云、潮湿，西风强烈。由于群岛四面环海，全年气温差温不大，夏季平均气温约为1.5摄氏度，冬季则为-5摄氏度。\[2\]

## 岛屿

[Deception-Tourists.jpg](https://zh.wikipedia.org/wiki/File:Deception-Tourists.jpg "fig:Deception-Tourists.jpg"),福斯特[港](../Page/港口.md "wikilink"),[欺骗岛](../Page/欺骗岛.md "wikilink")\]\]
从北到南南设得兰群岛有以下岛屿：

  - [象岛](../Page/象岛_\(南极洲\).md "wikilink")
  - [克拉伦斯岛](../Page/克拉伦斯岛_\(南设得兰群岛\).md "wikilink")
  - [乔治王岛](../Page/乔治王岛.md "wikilink")（最大岛屿）
  - [纳尔逊岛](../Page/纳尔逊岛_\(南设得兰群岛\).md "wikilink")
  - [罗伯特岛](../Page/罗伯特岛_\(南设得兰群岛\).md "wikilink")
  - [格林尼治岛](../Page/格林尼治岛_\(南设得兰群岛\).md "wikilink")
  - [利文斯顿岛](../Page/利文斯顿岛_\(南设得兰群岛\).md "wikilink")（第二大岛屿）
  - [斯诺岛](../Page/斯诺岛_\(南设得兰群岛\).md "wikilink")
  - [史密斯岛](../Page/史密斯岛_\(南设得兰群岛\).md "wikilink")
  - [欺骗岛](../Page/欺骗岛.md "wikilink")
  - [洛岛](../Page/洛岛_\(南设得兰群岛\).md "wikilink")

## 科学考察站

以下国家在南设得兰群岛设有科学考察站：

  - ：[胡瓦尼站](../Page/胡瓦尼站.md "wikilink")（自1959年）

  - ：St. Kliment Ohridski站（自1988年）

  - ：Comandante Ferraz站（自1984年）

  - ：[弗雷总统站](../Page/弗雷总统站.md "wikilink")（自1969年）

  - ：[胡利奥·埃斯库德罗教授站](../Page/胡利奥·埃斯库德罗教授站.md "wikilink")（自1984年）

  - ：[阿图罗·普拉特站](../Page/阿图罗·普拉特站.md "wikilink")（自1947年）

  - [智利](../Page/智利.md "wikilink")／[美国](../Page/美国.md "wikilink")：Shirreff站（自1990年）

  - ：[长城站](../Page/长城站.md "wikilink")（自1985年）

  - ：[佩德罗·文森特·马尔多纳多站](../Page/佩德罗·文森特·马尔多纳多站.md "wikilink")（自1990年）

  - ：[胡安·卡洛斯一世站](../Page/胡安·卡洛斯一世站.md "wikilink")（自1988年）

  - ：[加夫列尔·德卡斯蒂利亚站](../Page/加夫列尔·德卡斯蒂利亚站.md "wikilink")（自1989年）

  - ：[世宗科学基地](../Page/世宗科学基地.md "wikilink")（自1988年）

  - ：[马丘比丘站](../Page/马丘比丘站.md "wikilink")（自1989年）

  - ：[亨里克·阿茨托夫斯基站](../Page/亨里克·阿茨托夫斯基站.md "wikilink")（自1977年）

  - ：[别林斯高晋站](../Page/别林斯高晋站.md "wikilink")（自1968年）

  - ：[阿蒂加斯站](../Page/阿蒂加斯站.md "wikilink")（自1984年）

## 参见

  - [乔治王岛](../Page/乔治王岛.md "wikilink")

## 注释

## 参考文献

  - A.G.E. Jones, Captain William Smith and the Discovery of New South
    Shetland, *Geographical Journal*, Vol. 141, No. 3 (Nov., 1975), pp.
    445-461
  - Alan Gurney, *Below the Convergence: Voyages Toward Antarctica,
    1699-1839*, Penguin Books, New York, 1998
  - R.J. Campbell ed., *The Discovery of the South Shetland Islands: The
    Voyage of the Brig Williams, 1819-1820 and the Journal of Midshipman
    C.W. Poynter*, The Hakluyt Society, London, 2000
  - Capt. Hernán Ferrer Fougá, [El hito austral del confín de América.
    El cabo de Hornos. (Siglo XIX, 1800-1855). (Segunda
    parte)](http://www.revistamarina.cl/revistas/2003/6/ferrer.pdf).
    *Revista de Marina, Valparaíso*, 2004, N° 1
  - General Survey of Climatology V12, Landsberg ed,. (1984), Elsevier

## 外部链接

  - [英联邦秘书处网站对英属南极领地的介绍](https://web.archive.org/web/20070815210241/http://www.thecommonwealth.org/Templates/YearbookInternal.asp?NodeID=140419)
  - [英国外交及联邦事务部：英属南极领地概况](http://www.fco.gov.uk/servlet/Front?pagename=OpenMarket/Xcelerate/ShowPage&c=Page&cid=1007029394365&a=KCountryProfile&aid=1018952685006)
  - [阿根廷政府网站：南设得兰群岛地图](https://web.archive.org/web/20051017074145/http://www.info.gov.ar/images/mapas/tierra.jpg)
  - [阿根廷政府网站：南极洲历史](https://web.archive.org/web/20150618181835/http://www.dna.gov.ar/DIVULGAC/HISTORIA.HTM)

### 地图

  - [Interactive King George Island
    Mapviewer](https://web.archive.org/web/20070927192934/http://www.kgis.scar.org/mapviewer/kgis.phtml)
  - L.L. Ivanov. [Antarctica: Livingston Island and Greenwich, Robert,
    Snow and Smith Islands](http://apcbg.org/image023.jpg). Scale
    1:120000 topographic map. Troyan: Manfred Wörner Foundation, 2009.
    ISBN 978-954-92032-6-4

[南设得兰群岛](../Category/南设得兰群岛.md "wikilink")
[Category:群岛](../Category/群岛.md "wikilink")

1.
2.  GHCN Climate data, GISS data publications, period 1978-2007