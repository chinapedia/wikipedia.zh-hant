**斯卡拉足球會**（**Skála
Ítróttarfelag**）是[法罗群岛的足球俱乐部](../Page/法罗群岛.md "wikilink")，位于[斯卡拉](../Page/斯卡拉.md "wikilink")，俱乐部创建于1965年5月15日。

在2005赛季中球队夺得了俱乐部历史上最好名次，联赛第二名，从而取得了参加2006–07赛季[欧洲联盟杯预选赛的资格](../Page/欧洲联盟杯.md "wikilink")。

## 外部链接

  - [Official website](http://www.skalaif.fo)

[Category:法罗群岛足球俱乐部](../Category/法罗群岛足球俱乐部.md "wikilink")