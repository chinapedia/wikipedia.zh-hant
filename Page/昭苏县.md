**昭苏县**是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[伊犁哈萨克自治州所辖的一个](../Page/伊犁哈萨克自治州.md "wikilink")[县](../Page/县.md "wikilink")。

## 名称

  - 汉名“昭苏”源自“喇嘛**昭**”（[圣佑庙](../Page/圣佑庙.md "wikilink")）和“六**苏**门”（或“六苏厄鲁特”，[达什达瓦别名](../Page/达什达瓦.md "wikilink")），并取“苏醒恢复生机”之意。
      -
        [喇嘛昭](../Page/喇嘛昭.md "wikilink")（圣佑庙）蒙语为“博格达夏格松”。
  - [托忒名](../Page/托忒.md "wikilink")“”（[ᠮᡆᡊᡎᡆᠯᡘᡉᠷᡄᡃ.SVG](https://zh.wikipedia.org/wiki/File:ᠮᡆᡊᡎᡆᠯᡘᡉᠷᡄᡃ.SVG "fig:ᠮᡆᡊᡎᡆᠯᡘᡉᠷᡄᡃ.SVG")）、蒙语名“”源自哈萨克名“蒙古库热”（），原为[喇嘛昭的别称](../Page/喇嘛昭.md "wikilink")，意为蒙古庙宇。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、3个[民族乡](../Page/民族乡.md "wikilink")：

。

## 参考文献

{{-}}

[伊犁](../Category/新疆县份.md "wikilink")
[昭苏县](../Category/昭苏县.md "wikilink")
[县](../Category/伊犁州直管县市.md "wikilink")