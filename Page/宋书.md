《**宋書**》，[二十四史之一](../Page/二十四史.md "wikilink")，由[南朝梁](../Page/南朝梁.md "wikilink")[沈约等人所著](../Page/沈约.md "wikilink")。沈约在[齊武帝](../Page/齊武帝.md "wikilink")[永明五年](../Page/永明.md "wikilink")（487）奉詔撰《宋書》，寫成本紀10篇、列傳60篇、志30篇，共一百篇。记事始于[宋武帝](../Page/宋武帝.md "wikilink")[永初元年](../Page/永初_\(劉裕\).md "wikilink")（420），下迄[宋顺帝](../Page/宋顺帝.md "wikilink")[升明三年](../Page/升明.md "wikilink")（479），记载了[南朝](../Page/南朝_\(中国\).md "wikilink")[刘宋政权](../Page/刘宋.md "wikilink")60年的史事。他根据宋的[何承天](../Page/何承天_\(南朝\).md "wikilink")、[山谦之](../Page/山谦之.md "wikilink")、[苏宝生](../Page/苏宝生.md "wikilink")、[徐爰的](../Page/徐爰.md "wikilink")《宋书》，进行增删、订补工作，将宋末十几年的史迹加以补充，撰寫中大量抄錄了[徐爰的宋書](../Page/徐爰.md "wikilink")，因此七十篇的人物傳記以一年的飛快速度完成。《宋书》保存了很多的史料，包括当时的诏令奏议、书札、文章等，参考价值很高。沈约在《宋书》各志中的叙述，经常溯及到魏晋时期，这完全可以弥补《[三国志](../Page/三国志.md "wikilink")》等书的缺陷，同時因為志才是[沈約親自創作的部分](../Page/沈約.md "wikilink")(人物傳記多抄前人徐爰之宋書)，所以志的價值最高。《宋书》的志有八类，包括《律历志》、《礼志》、《乐志》、《天文志》、《符瑞志》、《五行志》、《州郡志》、《百官志》，号称《宋书》八书，份量占全书的一半。《宋书》志的部分上溯先秦，魏、晋尤为详尽，记载了不少诏诰、奏疏和古代乐曲、歌词等珍贵资料。[余嘉錫稱贊](../Page/余嘉錫.md "wikilink")《宋書》志是“史家之良規”。\[1\]

後人據相關史料補作《宋書》的志表，清人[萬斯同](../Page/萬斯同.md "wikilink")《[歷代史表](../Page/歷代史表.md "wikilink")》中有〈宋諸王世表〉、〈宋方鎮年表〉、〈宋將相大臣年表〉各1卷，[羅振玉補宋書](../Page/羅振玉.md "wikilink")《宗室世系表》1卷，[盛大力](../Page/盛大力.md "wikilink")《宋書補表》4卷，[郝懿行補宋書](../Page/郝懿行.md "wikilink")《刑法志》《食貨志》各1卷。近人[聶崇岐有](../Page/聶崇岐.md "wikilink")《補宋書藝文志》1卷。

## 目录

### 本纪

  - 本紀第一 武帝
  - 本紀第二 武帝中
  - 本紀第三 武帝下
  - 本紀第四 少帝
  - 本紀第五 文帝
  - 本紀第六 孝武帝
  - 本紀第七 前廢帝
  - 本紀第八 明帝
  - 本紀第九 後廢帝
  - 本紀第十 順帝

### 志

  - 志第一 志序 律曆上
  - 志第二 律曆中
  - 志第三 律曆下
  - 志第四 禮一
  - 志第五 禮二
  - 志第六 禮三
  - 志第七 禮四
  - 志第八 禮五
  - 志第九 樂一
  - 志第十 樂二
  - 志第十一 樂三
  - 志第十二 樂四
  - 志第十三 天文一
  - 志第十四 天文二
  - 志第十五 天文三
  - 志第十六 天文四
  - 志第十七 符瑞上
  - 志第十八 符瑞中
  - 志第十九 符瑞下
  - 志第二十 五行一
  - 志第二十一 五行二
  - 志第二十二 五行三
  - 志第二十三 五行四
  - 志第二十四 五行五
  - 志第二十五 州郡一
  - 志第二十六 州郡二
  - 志第二十七 州郡三
  - 志第二十八 州郡四
  - 志第二十九 百官上
  - 志第三十 百官下

### 列传

  - 列傳第一 后妃
  - 列傳第二 [劉穆之](../Page/劉穆之.md "wikilink") [王弘](../Page/王弘.md "wikilink")
    子錫
  - 列傳第三 [徐羨之](../Page/徐羨之.md "wikilink") [傅亮](../Page/傅亮.md "wikilink")
    [檀道濟](../Page/檀道濟.md "wikilink")
  - 列傳第四 [謝晦](../Page/謝晦.md "wikilink")
  - 列傳第五 [王鎮惡](../Page/王鎮惡.md "wikilink") [檀韶](../Page/檀韶.md "wikilink")
    [向靖](../Page/向靖.md "wikilink") [刘怀慎](../Page/刘怀慎.md "wikilink")
    [刘粹](../Page/刘粹.md "wikilink")
  - 列傳第六 [赵伦之](../Page/赵伦之.md "wikilink") [王懿](../Page/王懿.md "wikilink")
    [张邵](../Page/张邵.md "wikilink")
  - 列傳第七 [刘怀肃](../Page/刘怀肃.md "wikilink")
    [孟怀玉](../Page/孟怀玉.md "wikilink") 弟龙符
    [刘敬宣](../Page/刘敬宣.md "wikilink")
    [檀祗](../Page/檀祗.md "wikilink")
  - 列傳第八 [朱齡石](../Page/朱齡石.md "wikilink")
    [毛脩之](../Page/毛脩之.md "wikilink")
    [傅弘之](../Page/傅弘之.md "wikilink")
  - 列傳第九 [孫處](../Page/孫處.md "wikilink") [蒯恩](../Page/蒯恩.md "wikilink")
    [劉鍾](../Page/劉鍾.md "wikilink") [虞丘進](../Page/虞丘進.md "wikilink")
  - 列傳第十 [胡藩](../Page/胡藩.md "wikilink") [劉康祖](../Page/劉康祖.md "wikilink")
    [垣護之](../Page/垣護之.md "wikilink") [張興世](../Page/張興世.md "wikilink")
  - 列傳第十一 宗室
  - 列傳第十二 [庾悅](../Page/庾悅.md "wikilink") [王誕](../Page/王誕.md "wikilink")
    [謝景仁](../Page/謝景仁.md "wikilink") [袁湛](../Page/袁湛.md "wikilink")
    褚叔度長兄秀之 秀之弟淡之
  - 列傳第十三 [张茂度](../Page/张茂度.md "wikilink") 子永
    [庾登之](../Page/庾登之.md "wikilink") 弟炳之
    [谢方明](../Page/谢方明.md "wikilink")
    [江夷](../Page/江夷.md "wikilink")
  - 列傳第十四 [孔季恭](../Page/孔季恭.md "wikilink")
    [羊玄保](../Page/羊玄保.md "wikilink")
    [沈昙庆](../Page/沈昙庆.md "wikilink")
  - 列傳第十五 [臧焘](../Page/臧焘.md "wikilink") [徐广](../Page/徐广.md "wikilink")
    [傅隆](../Page/傅隆.md "wikilink")
  - 列傳第十六 [谢瞻](../Page/谢瞻.md "wikilink")
    [孔琳之](../Page/孔琳之.md "wikilink")
  - 列傳第十七 [蔡廓](../Page/蔡廓.md "wikilink") 子兴宗
  - 列傳第十八 [王惠](../Page/王惠.md "wikilink")
    [謝弘微](../Page/謝弘微.md "wikilink")
    [王球](../Page/王球.md "wikilink")
  - 列傳第十九 [殷淳](../Page/殷淳.md "wikilink") 子孚 弟冲 淡
    [张暢](../Page/张暢.md "wikilink")
    [何偃](../Page/何偃.md "wikilink")
    [江智渊](../Page/江智渊.md "wikilink")
  - 列傳第二十 [范泰](../Page/范泰.md "wikilink")
    [王准之](../Page/王准之.md "wikilink")
    [王韶之](../Page/王韶之.md "wikilink")
    [荀伯子](../Page/荀伯子.md "wikilink")
  - 列傳第二十一 武三王 [庐陵孝献王义真](../Page/刘义真.md "wikilink")
    [江夏文献王义恭](../Page/刘义恭.md "wikilink")
    [衡阳文王义季](../Page/刘义季.md "wikilink")
  - 列傳第二十二 [羊欣](../Page/羊欣.md "wikilink") [张敷](../Page/张敷.md "wikilink")
    [王微](../Page/王微.md "wikilink")
  - 列傳第二十三 [王華](../Page/王華.md "wikilink")
    [王昙首](../Page/王昙首.md "wikilink")
    [殷景仁](../Page/殷景仁.md "wikilink")
    [沈演之](../Page/沈演之.md "wikilink")
  - 列傳第二十四 [鄭鮮之](../Page/鄭鮮之.md "wikilink")
    [裴松之](../Page/裴松之.md "wikilink")
    [何承天](../Page/何承天_\(南朝\).md "wikilink")
  - 列傳第二十五 [吉翰](../Page/吉翰.md "wikilink")
    [刘道产](../Page/刘道产.md "wikilink")
    [杜骥](../Page/杜骥.md "wikilink")
    [申恬](../Page/申恬.md "wikilink")
  - 列傳第二十六 [王敬弘](../Page/王敬弘.md "wikilink")
    [何尚之](../Page/何尚之.md "wikilink")
  - 列傳第二十七 [謝靈運](../Page/謝靈運.md "wikilink")
  - 列傳第二十八 武二王 [彭城王义康](../Page/刘义康.md "wikilink")
    [南郡王义宣](../Page/刘义宣.md "wikilink")
  - 列傳第二十九 [劉湛](../Page/刘湛_\(刘宋\).md "wikilink")
    [范曄](../Page/范晔_\(刘宋\).md "wikilink")
  - 列傳第三十 [袁淑](../Page/袁淑.md "wikilink")
  - 列傳第三十一 [徐湛之](../Page/徐湛之.md "wikilink")
    [江湛](../Page/江湛.md "wikilink")
    [王僧绰](../Page/王僧绰.md "wikilink")
  - 列傳第三十二 文九王
  - 列傳第三十三 [顏延之](../Page/顏延之.md "wikilink")
  - 列傳第三十四 [臧质](../Page/臧质.md "wikilink") [鲁爽](../Page/鲁爽.md "wikilink")
    [沈攸之](../Page/沈攸之.md "wikilink")
  - 列傳第三十五 [王僧達](../Page/王僧達.md "wikilink")
    [顏竣](../Page/顏竣.md "wikilink")
  - 列傳第三十六 [朱修之](../Page/朱修之.md "wikilink")
    [宗悫](../Page/宗悫.md "wikilink")
    [王玄谟](../Page/王玄谟.md "wikilink")
  - 列傳第三十七 [柳元景](../Page/柳元景.md "wikilink")
    [颜师伯](../Page/颜师伯.md "wikilink")
    [沈庆之](../Page/沈庆之.md "wikilink")
  - 列傳第三十八 [蕭思話](../Page/蕭思話.md "wikilink")
    [劉延孫](../Page/劉延孫.md "wikilink")
  - 列傳第三十九 文五王 [竟陵王诞](../Page/刘诞_\(刘宋\).md "wikilink")
    [庐江王祎](../Page/刘祎.md "wikilink")
    [武昌王浑](../Page/刘浑.md "wikilink")
    [海陵王休茂](../Page/刘休茂.md "wikilink")
    [桂阳王休范](../Page/刘休范.md "wikilink")
  - 列傳第四十 孝武十四王
  - 列傳第四十一 [劉秀之](../Page/劉秀之.md "wikilink")
    [顧琛](../Page/顧琛.md "wikilink")
    [顧覬之](../Page/顧覬之.md "wikilink")
  - 列傳第四十二 [周朗](../Page/周朗_\(刘宋\).md "wikilink")
    [沈懷文](../Page/沈懷文.md "wikilink")
  - 列傳第四十三 [宗越](../Page/宗越.md "wikilink") [吴喜](../Page/吴喜.md "wikilink")
    [黄回](../Page/黄回.md "wikilink")
  - 列傳第四十四 [鄧琬](../Page/鄧琬.md "wikilink") [袁顗](../Page/袁顗.md "wikilink")
    [孔覬](../Page/孔覬.md "wikilink")
  - 列傳第四十五 [謝莊](../Page/謝莊.md "wikilink")
    [王景文](../Page/王景文.md "wikilink")
  - 列傳第四十六 [殷孝祖](../Page/殷孝祖.md "wikilink")
    [劉勉](../Page/劉勉.md "wikilink")
  - 列傳第四十七 [蕭惠開](../Page/蕭惠開.md "wikilink")
    [殷琰](../Page/殷琰.md "wikilink")
  - 列傳第四十八 [薛安都](../Page/薛安都.md "wikilink")
    [沈文秀](../Page/沈文秀.md "wikilink")
    [崔道固](../Page/崔道固.md "wikilink")
  - 列傳第四十九 [袁粲](../Page/袁粲.md "wikilink")
  - 列傳第五十 明四王 [邵陵殇王友](../Page/劉友_\(南朝宋\).md "wikilink")
    [随阳王翙](../Page/刘翙.md "wikilink")
    [新兴王嵩](../Page/刘嵩.md "wikilink")
    [始建王禧](../Page/刘禧.md "wikilink")
  - 列傳第五十一 孝義 龚颖 刘瑜 贾恩 郭世道 子原平 严世期 吴逵 潘综 张进之 王彭 蒋恭 徐耕 孙法宗 范叔孙 卜天与 许昭先
    余齐民 孙棘 何子平
  - 列傳第五十二 良吏 王镇之 杜慧度 徐豁 陆徽 阮长之 江秉之 王歆之
  - 列傳第五十三 隱逸 戴颙 [宗炳](../Page/宗炳.md "wikilink") 周续之 王弘之 阮万龄 孔淳之 刘凝之 龚祈
    翟法赐 陶潜 宗彧之 沈道虔 郭希林 雷次宗 朱百年 王素 关康之
  - 列傳第五十四 恩倖 戴法兴 阮佃夫 王道隆 杨运长
  - 列傳第五十五 [索虜](../Page/北魏.md "wikilink")
  - 列傳第五十六 [鮮卑吐谷渾](../Page/吐谷渾.md "wikilink")
  - 列傳第五十七 夷蠻
  - 列傳第五十八 氐胡 略陽清水氐楊氏 胡大且渠蒙遜
  - 列傳第五十九 二凶 [元凶劭](../Page/刘劭_\(南北朝\).md "wikilink")
    [始興王濬](../Page/刘濬.md "wikilink")
  - 列傳第六十 自序

## 参考文献

## 外部链接

  - [*宋書* 在 archive.org 免費下載](https://archive.org/details/06079131.cn)

## 参见

  - [宋书人物列表](../Page/宋书人物列表.md "wikilink")

[Category:史部正史類](../Category/史部正史類.md "wikilink")
[S](../Category/纪传体.md "wikilink")
[南](../Category/二十四史.md "wikilink")
[Category:南北朝史书](../Category/南北朝史书.md "wikilink")
[Category:刘宋](../Category/刘宋.md "wikilink")
[Category:南梁](../Category/南梁.md "wikilink")
[Category:南北朝官修典籍](../Category/南北朝官修典籍.md "wikilink")

1.  《四庫提要辨證》卷3