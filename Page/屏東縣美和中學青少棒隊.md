**美和中學青少棒隊**是由[美和中學所成立的一支棒球隊](../Page/美和中學.md "wikilink")。1971年在董事長[徐傍興支持下](../Page/徐傍興.md "wikilink")，美和中學首先成立了青少棒隊，聘請[曾紀恩為教練](../Page/曾紀恩.md "wikilink")，接著再組成青棒隊。數十年來，美和棒球隊總共為[台灣拿下十三次以上在美國舉辦的世界青少棒](../Page/中華民國.md "wikilink")、青棒賽冠軍。

## 歷屆成員

<table>
<thead>
<tr class="header">
<th><p>畢業年次</p></th>
<th><p>青 少 棒 名 單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1972年</p></td>
<td><p><a href="../Page/梁敬林.md" title="wikilink">梁敬林</a>　<a href="../Page/張沐源.md" title="wikilink">張沐源</a></p></td>
</tr>
<tr class="even">
<td><p>1973年</p></td>
<td><p><a href="../Page/林俊民.md" title="wikilink">林俊民</a>、<a href="../Page/張鳳男.md" title="wikilink">張鳳男</a>、<a href="../Page/李恭正.md" title="wikilink">李恭正</a>、<a href="../Page/陳東豐.md" title="wikilink">陳東豐</a>、<a href="../Page/陳昭銘_(1958).md" title="wikilink">陳昭銘</a>、<a href="../Page/陳進財.md" title="wikilink">陳進財</a>、<a href="../Page/江仲豪.md" title="wikilink">江仲豪</a>、<a href="../Page/楊清瓏.md" title="wikilink">楊清瓏</a>、<a href="../Page/黃明怡.md" title="wikilink">黃明怡</a>、<a href="../Page/陳富嶺.md" title="wikilink">陳富嶺</a>、<a href="../Page/馮重政.md" title="wikilink">馮重政</a>、<a href="../Page/蕭良吉.md" title="wikilink">蕭良吉</a>、<a href="../Page/吳瑞雄.md" title="wikilink">吳瑞雄</a>、<a href="../Page/陳山水.md" title="wikilink">陳山水</a>、<a href="../Page/蘇百慶.md" title="wikilink">蘇百慶</a></p></td>
</tr>
<tr class="odd">
<td><p>1974年</p></td>
<td><p><a href="../Page/蕭光宜.md" title="wikilink">蕭光宜</a>　<a href="../Page/陳重雄.md" title="wikilink">陳重雄</a>　<a href="../Page/李天祥.md" title="wikilink">李天祥</a>　<a href="../Page/黃榮亮.md" title="wikilink">黃榮亮</a>　<a href="../Page/蘇順德.md" title="wikilink">蘇順德</a>　<a href="../Page/王恩鵬.md" title="wikilink">王恩鵬</a>　<a href="../Page/何立高.md" title="wikilink">何立高</a>　<a href="../Page/徐生明.md" title="wikilink">徐生明</a>　<a href="../Page/唐昭鈞.md" title="wikilink">唐昭鈞</a>　<a href="../Page/黃榮銘.md" title="wikilink">黃榮銘</a>　<a href="../Page/劉宗富.md" title="wikilink">劉宗富</a>　<a href="../Page/張業泰.md" title="wikilink">張業泰</a>　<a href="../Page/張永昌.md" title="wikilink">張永昌</a>　<a href="../Page/高文川.md" title="wikilink">高文川</a>、<a href="../Page/李居明.md" title="wikilink">李居明</a>　<a href="../Page/魏景林.md" title="wikilink">魏景林</a>　<a href="../Page/江竹山.md" title="wikilink">江竹山</a></p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p><a href="../Page/黃廣琪.md" title="wikilink">黃廣琪</a>　<a href="../Page/沈堯弘.md" title="wikilink">沈堯弘</a>　<a href="../Page/張裕雄.md" title="wikilink">張裕雄</a>　<a href="../Page/趙士強.md" title="wikilink">趙士強</a>　<a href="../Page/謝燈育.md" title="wikilink">謝燈育</a>　<a href="../Page/李有發.md" title="wikilink">李有發</a>　<a href="../Page/劉名賢.md" title="wikilink">劉名賢</a></p></td>
</tr>
<tr class="odd">
<td><p>1976年</p></td>
<td><p><a href="../Page/柯孟岱.md" title="wikilink">柯孟岱</a>　<a href="../Page/林振賢.md" title="wikilink">林振賢</a>　<a href="../Page/陳振順.md" title="wikilink">陳振順</a>　<a href="../Page/張泓.md" title="wikilink">張泓</a>　　<a href="../Page/羅國章.md" title="wikilink">羅國章</a>　<a href="../Page/洪一中.md" title="wikilink">洪一中</a></p></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td><p><a href="../Page/黃耀賢.md" title="wikilink">黃耀賢</a>　<a href="../Page/陳正中.md" title="wikilink">陳正中</a>　<a href="../Page/王銘正.md" title="wikilink">王銘正</a>　<a href="../Page/王文明.md" title="wikilink">王文明</a>　<a href="../Page/許錫華.md" title="wikilink">許錫華</a></p></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td><p><a href="../Page/黃純清.md" title="wikilink">黃純清</a>　<a href="../Page/龔榮堂.md" title="wikilink">龔榮堂</a>　<a href="../Page/蔡豐年.md" title="wikilink">蔡豐年</a>　<a href="../Page/呂文生.md" title="wikilink">呂文生</a>　<a href="../Page/黃新松.md" title="wikilink">黃新松</a>　<a href="../Page/顧英麟.md" title="wikilink">顧英麟</a>　<a href="../Page/吳振郁.md" title="wikilink">吳振郁</a>　<a href="../Page/林琨瑋.md" title="wikilink">林琨瑋</a>　<a href="../Page/蕭智祥.md" title="wikilink">蕭智祥</a>　<a href="../Page/吳思賢.md" title="wikilink">吳思賢</a>　<a href="../Page/呂明勳.md" title="wikilink">呂明勳</a></p></td>
</tr>
<tr class="even">
<td><p>1979年</p></td>
<td><p><a href="../Page/賴崇光.md" title="wikilink">賴崇光</a>　<a href="../Page/李益良.md" title="wikilink">李益良</a>　<a href="../Page/黃俊碩.md" title="wikilink">黃俊碩</a>　<a href="../Page/潘忠魁.md" title="wikilink">潘忠魁</a>　<a href="../Page/黃啟明.md" title="wikilink">黃啟明</a>　<a href="../Page/姬尚宏.md" title="wikilink">姬尚宏</a>　<a href="../Page/孫長川.md" title="wikilink">孫長川</a>　<a href="../Page/李俊適.md" title="wikilink">李俊適</a></p></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td><p><a href="../Page/李安熙.md" title="wikilink">李安熙</a>　<a href="../Page/李仲弘.md" title="wikilink">李仲弘</a>　<a href="../Page/高聲遠.md" title="wikilink">高聲遠</a>　<a href="../Page/王晉煜.md" title="wikilink">王晉煜</a>　<a href="../Page/何文進.md" title="wikilink">何文進</a>　<a href="../Page/張文泉.md" title="wikilink">張文泉</a>　<a href="../Page/張集正.md" title="wikilink">張集正</a>　<a href="../Page/潘文柱.md" title="wikilink">潘文柱</a>　<a href="../Page/郭建霖.md" title="wikilink">郭建霖</a>　<a href="../Page/連宏仁.md" title="wikilink">連宏仁</a>　<a href="../Page/賴滄凱.md" title="wikilink">賴滄凱</a>　<a href="../Page/廖旭雄.md" title="wikilink">廖旭雄</a>　<a href="../Page/徐國銘.md" title="wikilink">徐國銘</a>　<a href="../Page/鄧耀華.md" title="wikilink">鄧耀華</a>、<a href="../Page/鍾豐明.md" title="wikilink">鍾豐明</a>　<a href="../Page/張耀騰.md" title="wikilink">張耀騰</a></p></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p><a href="../Page/徐整當.md" title="wikilink">徐整當</a>　<a href="../Page/溫志堃.md" title="wikilink">溫志堃</a>　<a href="../Page/劉耀金.md" title="wikilink">劉耀金</a>　<a href="../Page/陳耿佑.md" title="wikilink">陳耿佑</a>　<a href="../Page/潘榮祥.md" title="wikilink">潘榮祥</a>　<a href="../Page/李國清.md" title="wikilink">李國清</a>　<a href="../Page/利文正.md" title="wikilink">利文正</a>　<a href="../Page/呂永雄.md" title="wikilink">呂永雄</a>　<a href="../Page/黃文生.md" title="wikilink">黃文生</a>　<a href="../Page/楊斯祺.md" title="wikilink">楊斯祺</a></p></td>
</tr>
<tr class="odd">
<td><p>1982年</p></td>
<td><p>　　無</p></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td><p><a href="../Page/黃忠原.md" title="wikilink">黃忠原</a>　<a href="../Page/吳世賢.md" title="wikilink">吳世賢</a>　<a href="../Page/潘東漢.md" title="wikilink">潘東漢</a>　<a href="../Page/鄭豐萬.md" title="wikilink">鄭豐萬</a>　<a href="../Page/黃振益.md" title="wikilink">黃振益</a>　<a href="../Page/莊志偉.md" title="wikilink">莊志偉</a>　<a href="../Page/羅朝鐘.md" title="wikilink">羅朝鐘</a></p></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p><a href="../Page/楊正文.md" title="wikilink">楊正文</a>　<a href="../Page/古國謙.md" title="wikilink">古國謙</a>　<a href="../Page/李照富.md" title="wikilink">李照富</a>　<a href="../Page/簡佩輝.md" title="wikilink">簡佩輝</a>　<a href="../Page/林茂華.md" title="wikilink">林茂華</a>　<a href="../Page/邱耀祖.md" title="wikilink">邱耀祖</a></p></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p><a href="../Page/鄭俊男.md" title="wikilink">鄭俊男</a>　<a href="../Page/蕭浚濠.md" title="wikilink">蕭浚濠</a>　<a href="../Page/蔡榮昌.md" title="wikilink">蔡榮昌</a>　<a href="../Page/陳宗世.md" title="wikilink">陳宗世</a>　<a href="../Page/鄞明村.md" title="wikilink">鄞明村</a>　<a href="../Page/黃煌智.md" title="wikilink">黃煌智</a>　<a href="../Page/張榮勳.md" title="wikilink">張榮勳</a>　<a href="../Page/吳坤峰.md" title="wikilink">吳坤峰</a>　<a href="../Page/李健男.md" title="wikilink">李健男</a>　<a href="../Page/康榮華.md" title="wikilink">康榮華</a>　<a href="../Page/鄭英賢.md" title="wikilink">鄭英賢</a>　<a href="../Page/黃耀聰.md" title="wikilink">黃耀聰</a>　<a href="../Page/蔡昱詳.md" title="wikilink">蔡泓澤</a>　<a href="../Page/邱朝宗.md" title="wikilink">邱朝宗</a><br />
<a href="../Page/陳金東.md" title="wikilink">陳金東</a></p></td>
</tr>
<tr class="odd">
<td><p>1986年</p></td>
<td><p><a href="../Page/湯士磊.md" title="wikilink">湯士磊</a>　<a href="../Page/林憲龍.md" title="wikilink">林憲龍</a>　<a href="../Page/劉忠岳.md" title="wikilink">劉忠岳</a>　<a href="../Page/韓振興.md" title="wikilink">韓振興</a>　<a href="../Page/黃威揚.md" title="wikilink">黃威揚</a>　<a href="../Page/楊鎮嘉.md" title="wikilink">楊鎮嘉</a>　<a href="../Page/周義全.md" title="wikilink">周義全</a>　<a href="../Page/王憶傑.md" title="wikilink">王憶傑</a>　<a href="../Page/邱麒璋.md" title="wikilink">邱麒璋</a>　<a href="../Page/潘忠華.md" title="wikilink">潘忠華</a>　<a href="../Page/馮文龍.md" title="wikilink">馮文龍</a>　<a href="../Page/王聖徹.md" title="wikilink">王聖徹</a>　<a href="../Page/羅金賢.md" title="wikilink">羅金賢</a></p></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td><p><a href="../Page/蔡明豐.md" title="wikilink">蔡明豐</a>　<a href="../Page/徐宏根.md" title="wikilink">徐宏根</a>　<a href="../Page/吳家綜.md" title="wikilink">吳家綜</a>　<a href="../Page/邵建倫.md" title="wikilink">邵建倫</a>　<a href="../Page/尤俊隆.md" title="wikilink">尤俊隆</a>　<a href="../Page/吳國政.md" title="wikilink">吳國政</a>　<a href="../Page/鄭瑋咨.md" title="wikilink">鄭瑋咨</a>　<a href="../Page/林讚新.md" title="wikilink">林讚新</a>　<a href="../Page/張守元.md" title="wikilink">張守元</a></p></td>
</tr>
<tr class="odd">
<td><p>1988年</p></td>
<td><p><a href="../Page/楊福群.md" title="wikilink">楊福群</a>　<a href="../Page/林聖億.md" title="wikilink">林聖億</a>　<a href="../Page/孫光義.md" title="wikilink">孫光義</a>　<a href="../Page/鄭智文.md" title="wikilink">鄭智文</a>　<a href="../Page/鄭登耀.md" title="wikilink">鄭登耀</a>　<a href="../Page/吳孟財.md" title="wikilink">吳孟財</a>　<a href="../Page/宋其男.md" title="wikilink">宋其男</a></p></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td><p><a href="../Page/葉宗哲.md" title="wikilink">葉宗哲</a>　<a href="../Page/倪國展.md" title="wikilink">倪國展</a>　<a href="../Page/吳育昆.md" title="wikilink">吳育昆</a>　<a href="../Page/李健璋.md" title="wikilink">李健璋</a>　<a href="../Page/陳慶國.md" title="wikilink">陳慶國</a>　<a href="../Page/黃俊傑.md" title="wikilink">黃俊傑</a>　<a href="../Page/曹瑞華.md" title="wikilink">曹瑞華</a>　<a href="../Page/林敏俊.md" title="wikilink">林敏俊</a>　<a href="../Page/林鼎寰.md" title="wikilink">林鼎寰</a></p></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/藍德威.md" title="wikilink">藍德威</a>　<a href="../Page/潘清吉.md" title="wikilink">潘清吉</a>　<a href="../Page/石金剛.md" title="wikilink">石金剛</a>　<a href="../Page/陳信宜.md" title="wikilink">陳信宜</a>　<a href="../Page/張校源.md" title="wikilink">張校源</a>　<a href="../Page/呂嘉明.md" title="wikilink">呂嘉明</a>　<a href="../Page/趙立.md" title="wikilink">趙立</a>　　<a href="../Page/溫勝虎.md" title="wikilink">溫勝虎</a>　<a href="../Page/王文智.md" title="wikilink">王文智</a></p></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/楊朝行.md" title="wikilink">楊朝行</a>　<a href="../Page/張晉忠.md" title="wikilink">張晉忠</a>　<a href="../Page/涂進德.md" title="wikilink">涂進德</a>　<a href="../Page/潘順隆.md" title="wikilink">潘順隆</a>　<a href="../Page/高玉龍.md" title="wikilink">高玉龍</a>　<a href="../Page/陳瑞振.md" title="wikilink">陳瑞振</a>　<a href="../Page/林仲梁.md" title="wikilink">林仲梁</a>　<a href="../Page/柯景文.md" title="wikilink">柯景文</a>　<a href="../Page/蔡豐安.md" title="wikilink">蔡豐安</a>　<a href="../Page/馮勝賢.md" title="wikilink">馮勝賢</a></p></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/張泰山.md" title="wikilink">張泰山</a>　<a href="../Page/蔡士凡.md" title="wikilink">蔡士凡</a>　<a href="../Page/蔡士勤.md" title="wikilink">蔡士勤</a>　<a href="../Page/高健龍.md" title="wikilink">高健龍</a>　<a href="../Page/葉明煌.md" title="wikilink">葉明煌</a>　<a href="../Page/林明憲.md" title="wikilink">林明憲</a>　<a href="../Page/何紀賢.md" title="wikilink">何紀賢</a>　<a href="../Page/張琮貴.md" title="wikilink">張琮貴</a>　<a href="../Page/葉世山.md" title="wikilink">葉世山</a>　<a href="../Page/唐朝.md" title="wikilink">唐朝</a>　　<a href="../Page/邱仁雄.md" title="wikilink">邱仁雄</a>　<a href="../Page/許俊忠.md" title="wikilink">許俊忠</a>　<a href="../Page/詹俊盛.md" title="wikilink">詹俊盛</a>　<a href="../Page/戴龍水.md" title="wikilink">戴龍水</a></p></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/謝光勇.md" title="wikilink">謝光勇</a>　<a href="../Page/胡務熙.md" title="wikilink">胡務熙</a>　<a href="../Page/徐忠宏.md" title="wikilink">徐忠宏</a>　<a href="../Page/陳建洛.md" title="wikilink">陳建洛</a>　<a href="../Page/朱正勝.md" title="wikilink">朱正勝</a>　<a href="../Page/彭政閔.md" title="wikilink">彭政閔</a>　<a href="../Page/李志傑.md" title="wikilink">李志傑</a>　<a href="../Page/周森毅.md" title="wikilink">周森毅</a>　<a href="../Page/李國明.md" title="wikilink">李國明</a>　<a href="../Page/黃振勝.md" title="wikilink">黃振勝</a>　<a href="../Page/田顯明.md" title="wikilink">田顯明</a>　<a href="../Page/陳峰民.md" title="wikilink">陳峰民</a>　<a href="../Page/陳榮造.md" title="wikilink">陳榮造</a>　<a href="../Page/林義翔.md" title="wikilink">林義翔</a></p></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><a href="../Page/董慶員.md" title="wikilink">董慶員</a>　<a href="../Page/康立群.md" title="wikilink">康立群</a>　<a href="../Page/陳榮發.md" title="wikilink">陳榮發</a>　<a href="../Page/劉啟晟.md" title="wikilink">劉啟晟</a>　<a href="../Page/潘文國.md" title="wikilink">潘文國</a>　<a href="../Page/高國慶.md" title="wikilink">高國慶</a>　<a href="../Page/楊武峻.md" title="wikilink">楊武峻</a>　<a href="../Page/紀明廷.md" title="wikilink">紀明廷</a>　<a href="../Page/曾建雄.md" title="wikilink">曾建雄</a>　<a href="../Page/邱國源.md" title="wikilink">邱國源</a>　<a href="../Page/游建華.md" title="wikilink">游建華</a>　<a href="../Page/李宏裕.md" title="wikilink">李宏裕</a>　<a href="../Page/張其裕.md" title="wikilink">張其裕</a>　<a href="../Page/邱大維.md" title="wikilink">邱大維</a><br />
<a href="../Page/張源新.md" title="wikilink">張源新</a>　<a href="../Page/黃德昌.md" title="wikilink">黃德昌</a>　<a href="../Page/郭君亭.md" title="wikilink">郭君亭</a></p></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/彭政欣.md" title="wikilink">彭政欣</a>　<a href="../Page/曾啟葦.md" title="wikilink">曾啟葦</a>　<a href="../Page/陳柏勳.md" title="wikilink">陳柏勳</a>　<a href="../Page/曹濠鵬.md" title="wikilink">曹濠鵬</a>　<a href="../Page/林俊維.md" title="wikilink">林俊維</a>　<a href="../Page/高志誠.md" title="wikilink">高志誠</a>　<a href="../Page/張仁吉.md" title="wikilink">張仁吉</a></p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p><a href="../Page/劉逸民.md" title="wikilink">劉逸民</a>　<a href="../Page/林俊良.md" title="wikilink">林俊良</a>　<a href="../Page/張民傑.md" title="wikilink">張民傑</a>　<a href="../Page/呂榮宗.md" title="wikilink">呂榮宗</a>　<a href="../Page/曾兆豪.md" title="wikilink">曾兆豪</a>　<a href="../Page/王劍青.md" title="wikilink">王劍青</a>　<a href="../Page/沈鈺傑.md" title="wikilink">沈鈺傑</a>　<a href="../Page/莊智強.md" title="wikilink">莊智強</a>　<a href="../Page/戴志遠.md" title="wikilink">戴志遠</a>　<a href="../Page/張其峰.md" title="wikilink">張其峰</a>　<a href="../Page/張其峻.md" title="wikilink">張其峻</a>　<a href="../Page/王俊聰.md" title="wikilink">王俊聰</a>　<a href="../Page/潘武雄.md" title="wikilink">潘武雄</a>　<a href="../Page/張冠琳.md" title="wikilink">張冠琳</a><br />
<a href="../Page/潘英豪.md" title="wikilink">潘英豪</a>　<a href="../Page/莊立強.md" title="wikilink">莊立強</a>　<a href="../Page/蕭志光.md" title="wikilink">蕭志光</a></p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/潘威倫.md" title="wikilink">潘威倫</a>　<a href="../Page/張億得.md" title="wikilink">張億得</a>　<a href="../Page/宋烜言.md" title="wikilink">宋烜言</a>　<a href="../Page/趙治國.md" title="wikilink">趙治國</a>　<a href="../Page/黃昱安.md" title="wikilink">黃昱安</a>　<a href="../Page/潘源瑋.md" title="wikilink">潘源瑋</a>　<a href="../Page/蔡偉明.md" title="wikilink">蔡偉明</a>　<a href="../Page/李易倫.md" title="wikilink">李易倫</a>　<a href="../Page/徐明孝.md" title="wikilink">徐明孝</a></p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/陳意忠.md" title="wikilink">陳意忠</a>　<a href="../Page/王坤銘.md" title="wikilink">王坤銘</a>　<a href="../Page/陳世恩(1983).md" title="wikilink">陳世恩</a>　<a href="../Page/趙仁豪.md" title="wikilink">趙仁豪</a>　<a href="../Page/胡志閔.md" title="wikilink">胡志閔</a>　<a href="../Page/曾紀威.md" title="wikilink">曾紀威</a>　<a href="../Page/鄭欣明.md" title="wikilink">鄭欣明</a>　<a href="../Page/鍾岳霖.md" title="wikilink">鍾岳霖</a>　<a href="../Page/吳庭維.md" title="wikilink">吳庭維</a>　<a href="../Page/蔡佳偉.md" title="wikilink">蔡佳偉</a>　<a href="../Page/邱啟建.md" title="wikilink">邱啟建</a></p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p>？</p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/楊委儒.md" title="wikilink">楊委儒</a>　<a href="../Page/潘世強.md" title="wikilink">潘世強</a>　<a href="../Page/沈彥丞.md" title="wikilink">沈彥丞</a>　<a href="../Page/曾千嘉.md" title="wikilink">曾千嘉</a>　<a href="../Page/李金木.md" title="wikilink">李金木</a>　<a href="../Page/潘敏祥.md" title="wikilink">潘敏祥</a>　<a href="../Page/李宗融.md" title="wikilink">李宗融</a>　<a href="../Page/吳政憲.md" title="wikilink">吳政憲</a>　<a href="../Page/江建曉.md" title="wikilink">江建曉</a>　<a href="../Page/楊哲鑒.md" title="wikilink">楊哲鑒</a>　<a href="../Page/洪傅偉.md" title="wikilink">洪傅偉</a>　<a href="../Page/陳俊明.md" title="wikilink">陳俊明</a>　<a href="../Page/江易縉.md" title="wikilink">江易縉</a>　<a href="../Page/江昶辰.md" title="wikilink">江昶辰</a><br />
<a href="../Page/李易峰.md" title="wikilink">李易峰</a>　<a href="../Page/黃榮華.md" title="wikilink">黃榮華</a>　<a href="../Page/林奇峰.md" title="wikilink">林奇峰</a>　<a href="../Page/廖英傑.md" title="wikilink">廖英傑</a></p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/尤建程.md" title="wikilink">尤建程</a>　<a href="../Page/王嘉榮.md" title="wikilink">王嘉榮</a>　<a href="../Page/吳鴻毅.md" title="wikilink">吳鴻毅</a>　<a href="../Page/蘇文彰.md" title="wikilink">蘇文彰</a>　<a href="../Page/吳弘傑.md" title="wikilink">吳弘傑</a>　<a href="../Page/周貞銘.md" title="wikilink">周貞銘</a>　<a href="../Page/劉俊寬.md" title="wikilink">劉俊寬</a>　<a href="../Page/陳彥誌.md" title="wikilink">陳彥誌</a>　<a href="../Page/賴伯欣.md" title="wikilink">賴伯欣</a>　<a href="../Page/溫靜榮.md" title="wikilink">溫靜榮</a>　<a href="../Page/劉弘儒.md" title="wikilink">劉弘儒</a>　<a href="../Page/周銘賜.md" title="wikilink">周銘賜</a>　<a href="../Page/陳威.md" title="wikilink">陳威</a>　　<a href="../Page/陳傑.md" title="wikilink">陳傑</a></p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/尤明仁.md" title="wikilink">尤明仁</a>　<a href="../Page/王偉仲.md" title="wikilink">王偉仲</a>　<a href="../Page/吳凱文.md" title="wikilink">吳凱文</a>　<a href="../Page/李岳翰.md" title="wikilink">李岳翰</a>　<a href="../Page/林裕君.md" title="wikilink">林裕君</a>　<a href="../Page/傅宗晏.md" title="wikilink">傅宗晏</a>　<a href="../Page/葉竹軒.md" title="wikilink">葉竹軒</a>　<a href="../Page/蔡璟豪.md" title="wikilink">蔡璟豪</a>　<a href="../Page/鄭君偉.md" title="wikilink">鄭君偉</a>　<a href="../Page/藍永祥.md" title="wikilink">藍永祥</a></p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/張佳偉.md" title="wikilink">張佳偉</a>　<a href="../Page/呂俊傑.md" title="wikilink">呂俊傑</a>　<a href="../Page/梁恆翊.md" title="wikilink">梁恆翊</a>　<a href="../Page/尤建翔.md" title="wikilink">尤建翔</a>　<a href="../Page/蘇韋宏.md" title="wikilink">蘇韋宏</a>　<a href="../Page/李居冠.md" title="wikilink">李居冠</a>　<a href="../Page/石劍章.md" title="wikilink">石劍章</a>　<a href="../Page/鄭君荻.md" title="wikilink">鄭君荻</a>　<a href="../Page/陳彥孝.md" title="wikilink">陳彥孝</a>　<a href="../Page/潘志恆.md" title="wikilink">潘志恆</a>　<a href="../Page/陳冠廷.md" title="wikilink">陳冠廷</a>　<a href="../Page/謝慶文.md" title="wikilink">謝慶文</a>　<a href="../Page/莊凱銘.md" title="wikilink">莊凱銘</a></p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/傅至遠.md" title="wikilink">傅至遠</a>　<a href="../Page/朱俊賓.md" title="wikilink">朱俊賓</a>　<a href="../Page/林慶輝.md" title="wikilink">林慶輝</a>　<a href="../Page/林祖正.md" title="wikilink">林祖正</a>　<a href="../Page/王立名.md" title="wikilink">王立名</a>　<a href="../Page/潘國臻.md" title="wikilink">潘國臻</a>　<a href="../Page/陳冠均.md" title="wikilink">陳冠均</a>　<a href="../Page/沈伯琦.md" title="wikilink">沈伯琦</a>　<a href="../Page/陳欣宏.md" title="wikilink">陳欣宏</a></p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/何家駿.md" title="wikilink">何家駿</a>　<a href="../Page/孫偉哲.md" title="wikilink">孫偉哲</a>　<a href="../Page/陸昕猷.md" title="wikilink">陸昕猷</a>　<a href="../Page/蔡友達.md" title="wikilink">蔡友達</a>　<a href="../Page/林蔚諭.md" title="wikilink">林蔚諭</a>　<a href="../Page/李振堯.md" title="wikilink">李振堯</a>　<a href="../Page/玉智賢.md" title="wikilink">玉智賢</a>　<a href="../Page/胡駿慶.md" title="wikilink">胡駿慶</a>　<a href="../Page/呂銘智.md" title="wikilink">呂銘智</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/張凱翔.md" title="wikilink">張凱翔</a>　<a href="../Page/張俊宏.md" title="wikilink">張俊宏</a>　<a href="../Page/施力偉.md" title="wikilink">施力偉</a>　<a href="../Page/吳俊鴻.md" title="wikilink">吳俊鴻</a>　<a href="../Page/陳亮廷.md" title="wikilink">陳亮廷</a>　<a href="../Page/陳冠廷.md" title="wikilink">陳冠廷</a>　<a href="../Page/邱富明.md" title="wikilink">邱富明</a>　<a href="../Page/柯俊傑.md" title="wikilink">柯俊傑</a>　<a href="../Page/柳柏青.md" title="wikilink">柳柏青</a>　<a href="../Page/于志霖.md" title="wikilink">于志霖</a>　<a href="../Page/林鋕豪.md" title="wikilink">林鋕豪</a></p></td>
</tr>
<tr class="even">
<td><p>三年級</p></td>
<td><p><a href="../Page/郭信顯.md" title="wikilink">郭信顯</a>　<a href="../Page/尤泰億.md" title="wikilink">尤泰億</a>　<a href="../Page/尤朝億.md" title="wikilink">尤朝億</a>　<a href="../Page/張嘉顯.md" title="wikilink">張嘉顯</a>　<a href="../Page/陳冠宇.md" title="wikilink">陳冠宇</a>　<a href="../Page/吳奕呈.md" title="wikilink">吳奕呈</a>　<a href="../Page/謝文智.md" title="wikilink">謝文智</a>　<a href="../Page/戴如量.md" title="wikilink">戴如量</a>　<a href="../Page/吳介宇.md" title="wikilink">吳介宇</a>　<a href="../Page/黃振華.md" title="wikilink">黃振華</a>　<a href="../Page/蔡昀霖.md" title="wikilink">蔡昀霖</a>　<a href="../Page/汪智傑.md" title="wikilink">汪智傑</a></p></td>
</tr>
<tr class="odd">
<td><p>二年級</p></td>
<td><p><a href="../Page/張家慶.md" title="wikilink">張家慶</a>　<a href="../Page/田勝旗.md" title="wikilink">田勝旗</a>　<a href="../Page/林承御.md" title="wikilink">林承御</a>　<a href="../Page/葉子申.md" title="wikilink">葉子申</a>　<a href="../Page/曾精鈺.md" title="wikilink">曾精鈺</a></p></td>
</tr>
<tr class="even">
<td><p>一年級</p></td>
<td><p>無</p></td>
</tr>
</tbody>
</table>

## 外部連結

[Category:青少棒球隊](../Category/青少棒球隊.md "wikilink")