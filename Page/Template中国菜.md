<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="ChineseDishLogo.png" title="File:ChineseDishLogo.png" alt="File:ChineseDishLogo.png" /><figcaption><a href="File:ChineseDishLogo.png.md">File:ChineseDishLogo.png</a></figcaption>
</figure></td>
</tr>
<tr class="even">
<td><p>| <a href="中国菜.md" title="wikilink">中国菜</a><a href="菜系.md" title="wikilink">系列</a></p></td>
</tr>
<tr class="odd">
<td><p>四大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="鲁菜.md" title="wikilink">鲁菜</a> - <a href="川菜.md" title="wikilink">川菜</a><br />
<a href="粤菜.md" title="wikilink">粤菜</a> - <a href="苏菜.md" title="wikilink">苏菜</a></p></td>
</tr>
<tr class="odd">
<td><p>八大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="鲁菜.md" title="wikilink">鲁菜</a> - <a href="川菜.md" title="wikilink">川菜</a><br />
<a href="粤菜.md" title="wikilink">粤菜</a> - <a href="苏菜.md" title="wikilink">苏菜</a><br />
<a href="閩菜.md" title="wikilink">閩菜</a> - <a href="浙菜.md" title="wikilink">浙菜</a><br />
<a href="湘菜.md" title="wikilink">湘菜</a> - <a href="徽菜.md" title="wikilink">徽菜</a></p></td>
</tr>
<tr class="odd">
<td><p>十大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="鲁菜.md" title="wikilink">鲁菜</a> - <a href="川菜.md" title="wikilink">川菜</a><br />
<a href="粤菜.md" title="wikilink">粤菜</a> - <a href="苏菜.md" title="wikilink">苏菜</a><br />
<a href="閩菜.md" title="wikilink">閩菜</a> - <a href="浙菜.md" title="wikilink">浙菜</a><br />
<a href="湘菜.md" title="wikilink">湘菜</a> - <a href="徽菜.md" title="wikilink">徽菜</a><br />
<a href="京菜.md" title="wikilink">京菜</a> - <a href="上海菜.md" title="wikilink">上海菜</a></p></td>
</tr>
<tr class="odd">
<td><p>十二大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="鲁菜.md" title="wikilink">鲁菜</a> - <a href="川菜.md" title="wikilink">川菜</a><br />
<a href="粤菜.md" title="wikilink">粤菜</a> - <a href="苏菜.md" title="wikilink">苏菜</a><br />
<a href="閩菜.md" title="wikilink">閩菜</a> - <a href="浙菜.md" title="wikilink">浙菜</a><br />
<a href="湘菜.md" title="wikilink">湘菜</a> - <a href="徽菜.md" title="wikilink">徽菜</a><br />
<a href="京菜.md" title="wikilink">京菜</a> - <a href="上海菜.md" title="wikilink">上海菜</a><br />
<a href="陝西菜.md" title="wikilink">陝西菜</a> - <a href="豫菜.md" title="wikilink">豫菜</a></p></td>
</tr>
<tr class="odd">
<td><p>十四大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="鲁菜.md" title="wikilink">鲁菜</a> - <a href="川菜.md" title="wikilink">川菜</a><br />
<a href="粤菜.md" title="wikilink">粤菜</a> - <a href="苏菜.md" title="wikilink">苏菜</a><br />
<a href="閩菜.md" title="wikilink">閩菜</a> - <a href="浙菜.md" title="wikilink">浙菜</a><br />
<a href="湘菜.md" title="wikilink">湘菜</a> - <a href="徽菜.md" title="wikilink">徽菜</a><br />
<a href="京菜.md" title="wikilink">京菜</a> - <a href="上海菜.md" title="wikilink">上海菜</a><br />
<a href="陝西菜.md" title="wikilink">陝西菜</a> - <a href="豫菜.md" title="wikilink">豫菜</a><br />
<a href="辽菜.md" title="wikilink">辽菜</a> - <a href="楚菜.md" title="wikilink">楚菜</a></p></td>
</tr>
<tr class="odd">
<td><p>十六大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="鲁菜.md" title="wikilink">鲁菜</a> - <a href="川菜.md" title="wikilink">川菜</a><br />
<a href="粤菜.md" title="wikilink">粤菜</a> - <a href="苏菜.md" title="wikilink">苏菜</a><br />
<a href="閩菜.md" title="wikilink">閩菜</a> - <a href="浙菜.md" title="wikilink">浙菜</a><br />
<a href="湘菜.md" title="wikilink">湘菜</a> - <a href="徽菜.md" title="wikilink">徽菜</a><br />
<a href="京菜.md" title="wikilink">京菜</a> - <a href="上海菜.md" title="wikilink">上海菜</a><br />
<a href="陝西菜.md" title="wikilink">陝西菜</a> - <a href="豫菜.md" title="wikilink">豫菜</a><br />
<a href="辽菜.md" title="wikilink">辽菜</a> - <a href="楚菜.md" title="wikilink">楚菜</a><br />
<a href="津菜.md" title="wikilink">津菜</a> - <a href="云南菜.md" title="wikilink">云南菜</a></p></td>
</tr>
<tr class="odd">
<td><p>新八大菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="陇菜.md" title="wikilink">陇菜</a> - <a href="杭帮菜.md" title="wikilink">杭帮菜</a><br />
<a href="吉林菜.md" title="wikilink">吉林菜</a> - <a href="辽菜.md" title="wikilink">辽菜</a><br />
<a href="浙菜.md" title="wikilink">甬菜</a> - <a href="陝西菜.md" title="wikilink">陝西菜</a><br />
<a href="山西菜.md" title="wikilink">晋菜</a> - <a href="上海菜.md" title="wikilink">沪菜</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="粤菜.md" title="wikilink">粤菜系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="广州菜.md" title="wikilink">广州菜</a> - <a href="潮州菜.md" title="wikilink">潮州菜</a> - <a href="香港飲食.md" title="wikilink">香港菜</a> - <a href="澳門飲食.md" title="wikilink">澳門菜</a><br />
<a href="客家菜.md" title="wikilink">客家菜</a> - <a href="順德菜.md" title="wikilink">順德菜</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="苏菜.md" title="wikilink">苏菜系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="京苏菜.md" title="wikilink">京苏菜</a> - <a href="苏锡菜.md" title="wikilink">苏锡菜</a><br />
<a href="徐海菜.md" title="wikilink">徐海菜</a> - <a href="淮扬菜.md" title="wikilink">淮扬菜</a><br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="东北菜.md" title="wikilink">东北菜系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="龙江菜.md" title="wikilink">龙江菜</a> - <a href="吉林菜.md" title="wikilink">吉林菜</a><br />
<a href="辽菜.md" title="wikilink">辽菜</a> - <a href="满洲菜.md" title="wikilink">满洲菜</a></p></td>
</tr>
<tr class="odd">
<td><p>其他菜系</p></td>
</tr>
<tr class="even">
<td><p><a href="福州菜.md" title="wikilink">福州菜</a> - <a href="大连菜.md" title="wikilink">大连菜</a><br />
<a href="宮廷菜.md" title="wikilink">宮廷菜</a> - <a href="官府菜.md" title="wikilink">官府菜</a><br />
<a href="谭家菜.md" title="wikilink">谭家菜</a> - <a href="新疆菜.md" title="wikilink">新疆菜</a><br />
<a href="上海菜.md" title="wikilink">上海菜</a> - <a href="贛菜.md" title="wikilink">贛菜</a><br />
<a href="鄂菜.md" title="wikilink">鄂菜</a> - <a href="内蒙古菜.md" title="wikilink">内蒙古菜</a><br />
<a href="贵州菜.md" title="wikilink">贵州菜</a> - <a href="广西菜.md" title="wikilink">广西菜</a><br />
<a href="台灣菜.md" title="wikilink">台灣菜</a> - <a href="海南菜.md" title="wikilink">海南菜</a><br />
<a href="清真菜.md" title="wikilink">清真菜</a> - <a href="西藏菜.md" title="wikilink">西藏菜</a><br />
<a href="青海菜.md" title="wikilink">青海菜</a> - <a href="河北菜.md" title="wikilink">河北菜</a><br />
</p></td>
</tr>
<tr class="odd">
<td><p>bgcolor= "#D6D6C4"| 海外中國菜</p></td>
</tr>
<tr class="even">
<td><p>- <br />
 -  - <br />
 - <br />
 - <a href="巴基斯坦式中國菜.md" title="wikilink">巴基斯坦式中國菜</a> - <a href="峇峇娘惹料理.md" title="wikilink">峇峇娘惹料理</a><br />
<a href="秘魯中餐.md" title="wikilink">秘魯中餐</a> -  - <a href="美式中國菜.md" title="wikilink">美式中國菜</a><br />
<a href="新加坡菜.md" title="wikilink">新加坡菜</a></p></td>
</tr>
</tbody>
</table>

<noinclude>

</noinclude>

[\*](../Category/菜系.md "wikilink")
[Category:中国文化导航模板](../Category/中国文化导航模板.md "wikilink")
[Category:中国饮食模板‎](../Category/中国饮食模板‎.md "wikilink")