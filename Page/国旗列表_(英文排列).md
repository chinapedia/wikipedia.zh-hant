__NOTOC__

## A

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/阿布哈茲国旗.md "wikilink")||width="31%"
align=left|[阿布哈茲共和國](../Page/阿布哈茲.md "wikilink")
Abkhazia |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/阿富汗国旗.md "wikilink")||width="31%"
align=left|[阿富汗伊斯蘭共和國](../Page/阿富汗.md "wikilink")
Afghanistan |- |bgcolor=\#DCDCDC|
||**5:7**||[国旗](../Page/阿尔巴尼亚国旗.md "wikilink")||align=left|[阿爾巴尼亞共和國](../Page/阿爾巴尼亞.md "wikilink")
Albania |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/阿尔及利亚国旗.md "wikilink")||align=left|[阿爾及利亞民主人民共和國](../Page/阿爾及利亞.md "wikilink")
Algeria |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/安道尔国旗.md "wikilink")||align=left|[安道爾公國](../Page/安道尔.md "wikilink")
Andorra |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/安哥拉国旗.md "wikilink")||align=left|[安哥拉共和國](../Page/安哥拉.md "wikilink")
Angola |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/安提瓜和巴布达国旗.md "wikilink")||align=left|[安提瓜和巴布达](../Page/安提瓜和巴布达.md "wikilink")
Antigua & Barbuda |bgcolor=\#DCDCDC|
||**9:14**||[国旗](../Page/阿根廷国旗.md "wikilink")||align=left|[阿根廷共和國](../Page/阿根廷.md "wikilink")
Argentina |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/亚美尼亚国旗.md "wikilink")||align=left|[亞美尼亞共和國](../Page/亞美尼亞.md "wikilink")
Armenia |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/澳大利亚国旗.md "wikilink")||align=left|[澳大利亞聯邦](../Page/澳大利亚.md "wikilink")
Australia |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/奧地利國旗.md "wikilink")||align=left|[奧地利共和國](../Page/奧地利.md "wikilink")
Austria |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/阿塞拜疆国旗.md "wikilink")||align=left|[阿塞拜疆共和国](../Page/阿塞拜疆.md "wikilink")
Azerbaijan |}

## B

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/巴哈马国旗.md "wikilink")||width="31%"
align=left|[巴哈馬國](../Page/巴哈马.md "wikilink")
The Bahamas |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**3:5**||width="5%"|[国旗](../Page/巴林国旗.md "wikilink")||width="31%"
align=left|[巴林王國](../Page/巴林.md "wikilink")
Bahrain |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/孟加拉国国旗.md "wikilink")||align=left|[孟加拉人民共和國](../Page/孟加拉國.md "wikilink")
Bangladesh |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/巴巴多斯国旗.md "wikilink")||align=left|[巴巴多斯](../Page/巴巴多斯.md "wikilink")
Barbados |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/白俄罗斯国旗.md "wikilink")||align=left|[白俄羅斯共和國](../Page/白俄罗斯.md "wikilink")
Belarus |bgcolor=\#DCDCDC|
||**13:15**||[国旗](../Page/比利時国旗.md "wikilink")||align=left|[比利時王國](../Page/比利時.md "wikilink")
Belgium |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/伯利兹国旗.md "wikilink")||align=left|[伯利兹](../Page/伯利兹.md "wikilink")
Belize |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/贝宁国旗.md "wikilink")||align=left|[贝宁共和国](../Page/贝宁.md "wikilink")
Benin |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/不丹国旗.md "wikilink")||align=left|[不丹王國](../Page/不丹.md "wikilink")
Bhutan |bgcolor=\#DCDCDC|
||**15:22**||[国旗](../Page/玻利维亚国旗.md "wikilink")||align=left|[玻利維亞共和國](../Page/玻利維亞.md "wikilink")
Bolivia |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/波斯尼亚和黑塞哥维那国旗.md "wikilink")||align=left|[波斯尼亚和黑塞哥维那](../Page/波斯尼亚和黑塞哥维那.md "wikilink")
Bosnia and Herzegovina |bgcolor=\#DCDCDC|
||**5:8**||[国旗](../Page/博茨瓦纳国旗.md "wikilink")||align=left|[博茨瓦纳共和国](../Page/博茨瓦纳.md "wikilink")
Botswana |- |bgcolor=\#DCDCDC|
||**7:10**||[国旗](../Page/巴西国旗.md "wikilink")||align=left|[巴西聯邦共和國](../Page/巴西.md "wikilink")
Brazil |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/文莱国旗.md "wikilink")||align=left|[-{
zh-hans:文莱达鲁萨兰国;zh-hk:汶萊和平之國}-](../Page/文莱.md "wikilink")
Brunei |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/保加利亚国旗.md "wikilink")||align=left|[保加利亞共和國](../Page/保加利亚.md "wikilink")
Bulgaria |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/布基纳法索国旗.md "wikilink")||align=left|[布基纳法索](../Page/布基纳法索.md "wikilink")
Burkina |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/布隆迪国旗.md "wikilink")||align=left|[布隆迪共和国](../Page/布隆迪.md "wikilink")
Burundi |}

## C

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/柬埔寨国旗.md "wikilink")||width="31%"
align=left|[柬埔寨王國](../Page/柬埔寨.md "wikilink")
Cambodia |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/喀麦隆国旗.md "wikilink")||width="31%"
align=left|[喀麥隆共和國](../Page/喀麦隆.md "wikilink")
Cameroon |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/加拿大国旗.md "wikilink")||align=left|[加拿大](../Page/加拿大.md "wikilink")
Canada |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/佛得角国旗.md "wikilink")||align=left|[維德角共和國](../Page/佛得角.md "wikilink")
Cape Verde |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/中非共和國國旗.md "wikilink")||align=left|[中非共和國](../Page/中非共和國.md "wikilink")
Central African Republic |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/乍得国旗.md "wikilink")||align=left|[查德共和國](../Page/乍得.md "wikilink")
Chad |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/智利国旗.md "wikilink")||align=left|[智利共和國](../Page/智利.md "wikilink")
Chile |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/中華民國國旗.md "wikilink")||align=left|[中華民國](../Page/中華民國.md "wikilink")
Republic of China |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/中华人民共和国国旗.md "wikilink")||align=left|[中华人民共和国](../Page/中华人民共和国.md "wikilink")
People's Republic of China |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/哥伦比亚国旗.md "wikilink")||align=left|[哥倫比亞共和國](../Page/哥伦比亚.md "wikilink")
Colombia |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/科摩罗国旗.md "wikilink")||align=left|[葛摩聯盟](../Page/葛摩.md "wikilink")
The Comoros |bgcolor=\#DCDCDC|
||**3:4**||[国旗](../Page/刚果民主共和国国旗.md "wikilink")||align=left|[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")（薩伊）
Democratic Republic of the Congo（Zaire） |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/刚果共和国国旗.md "wikilink")||align=left|[刚果共和国](../Page/刚果共和国.md "wikilink")
Republic of the Congo |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/哥斯达黎加国旗.md "wikilink")||align=left|[哥斯大黎加共和國](../Page/哥斯达黎加.md "wikilink")
Costa Rica |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/克罗地亚国旗.md "wikilink")||align=left|[克羅埃西亞共和國](../Page/克罗地亚.md "wikilink")
Croatia |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/古巴国旗.md "wikilink")||align=left|[古巴共和國](../Page/古巴.md "wikilink")
Cuba |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/塞浦路斯共和国国旗.md "wikilink")||align=left|[-{zh-hans:塞浦路斯;zh-hk:塞浦路斯;zh-tw:賽普勒斯;}-共和國](../Page/塞浦路斯.md "wikilink")
Cyprus |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/北塞浦路斯土耳其共和国国旗.md "wikilink")||align=left|[北-{zh-hans:塞浦路斯;zh-hk:塞浦路斯;zh-tw:賽普勒斯;}-土耳其共和國](../Page/北塞浦路斯土耳其共和国.md "wikilink")
<small>Turkish Republic of Northern Cyprus (TRNC)</small> |-
|bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/捷克国旗.md "wikilink")||align=left|[捷克共和國](../Page/捷克.md "wikilink")
Czech Republic |}

## D

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**28:37**||width="5%"|[国旗](../Page/丹麦国旗.md "wikilink")||width="31%"
align=left|[丹麥王國](../Page/丹麦.md "wikilink")
Denmark |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/吉布提国旗.md "wikilink")||width="31%"
align=left|[吉布提共和国](../Page/吉布提.md "wikilink")
Djibouti |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/多米尼克国旗.md "wikilink")||align=left|[-{zh-hans:多米尼加;zh-hk:多明尼加;zh-tw:多米尼克;}-聯邦](../Page/多米尼克.md "wikilink")
Dominica |bgcolor=\#DCDCDC|
||**5:8**||[国旗](../Page/多明尼加共和国国旗.md "wikilink")||align=left|[-{zh-hans:多米尼加;zh-hant:多明尼加}-共和國](../Page/多米尼加共和国.md "wikilink")
Dominican Republic |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/頓涅茨克人民共和國国旗.md "wikilink")||align=left|[頓涅茨克人民共和國](../Page/頓涅茨克人民共和國.md "wikilink")
Donetsk Peoples Republic |}

## E

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/厄瓜多尔国旗.md "wikilink")||width="31%"
align=left|[厄瓜多尔共和国](../Page/厄瓜多尔.md "wikilink")
Ecuador |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/埃及国旗.md "wikilink")||width="31%"
align=left|[埃及阿拉伯共和国](../Page/埃及.md "wikilink")
Egypt |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/萨尔瓦多国旗.md "wikilink")||align=left|[萨尔瓦多共和国](../Page/萨尔瓦多.md "wikilink")
El Salvador |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/赤道几内亚国旗.md "wikilink")||align=left|[赤道几内亚共和国](../Page/赤道几内亚.md "wikilink")
Equatorial Guinea |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/厄立特里亚国旗.md "wikilink")||align=left|[厄立特里亚国](../Page/厄立特里亚.md "wikilink")
Eritrea |bgcolor=\#DCDCDC|
||**7:11**||[国旗](../Page/爱沙尼亚国旗.md "wikilink")||align=left|[爱沙尼亚共和国](../Page/爱沙尼亚.md "wikilink")
Estonia |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/埃塞俄比亚国旗.md "wikilink")||align=left|[埃塞俄比亚联邦民主共和国](../Page/埃塞俄比亚.md "wikilink")
Ethiopia |}

## F

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/斐济国旗.md "wikilink")||width="31%"
align=left|[斐济共和国](../Page/斐济.md "wikilink")
Fiji |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**11:18**||width="5%"|[国旗](../Page/芬兰国旗.md "wikilink")||width="31%"
align=left|[芬兰共和国](../Page/芬兰.md "wikilink")
Finland |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/法国国旗.md "wikilink")||align=left|[法兰西共和国](../Page/法国.md "wikilink")
France |}

## G

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**3:4**||width="5%"|[国旗](../Page/加蓬国旗.md "wikilink")||width="31%"
align=left|[加蓬共和国](../Page/加蓬.md "wikilink")
Gabon |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/冈比亚国旗.md "wikilink")||width="31%"
align=left|[冈比亚共和国](../Page/冈比亚.md "wikilink")
The Gambia |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/格鲁吉亚国旗.md "wikilink")||align=left|[格鲁吉亚](../Page/格鲁吉亚.md "wikilink")
Georgia |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/德国国旗.md "wikilink")||align=left|[德意志联邦共和国](../Page/德国.md "wikilink")
Germany |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/加纳国旗.md "wikilink")||align=left|[加纳共和国](../Page/加纳.md "wikilink")
Ghana |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/希腊国旗.md "wikilink")||align=left|[希腊共和国](../Page/希腊.md "wikilink")
Greece |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/格林纳达国旗.md "wikilink")||align=left|[格林纳达](../Page/格林纳达.md "wikilink")
Grenada |bgcolor=\#DCDCDC|
||**5:8**||[国旗](../Page/危地马拉国旗.md "wikilink")||align=left|[危地马拉共和国](../Page/危地马拉.md "wikilink")
Guatemala |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/几内亚国旗.md "wikilink")||align=left|[几内亚共和国](../Page/几内亚.md "wikilink")
Guinea |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/几内亚比绍国旗.md "wikilink")||align=left|[几内亚比绍共和国](../Page/几内亚比绍.md "wikilink")
Guinea-Bissau |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/圭亚那国旗.md "wikilink")||align=left|[圭亚那合作共和国](../Page/圭亚那.md "wikilink")
Guyana |}

## H

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**3:5**||width="5%"|[国旗](../Page/海地国旗.md "wikilink")||width="31%"
align=left|[海地共和国](../Page/海地.md "wikilink")
Haiti |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/洪都拉斯国旗.md "wikilink")||width="31%"
align=left|[洪都拉斯共和国](../Page/洪都拉斯.md "wikilink")
Honduras |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/匈牙利国旗.md "wikilink")||align=left|[匈牙利共和国](../Page/匈牙利.md "wikilink")
Hungary |}

## I

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**18:25**||width="5%"|[国旗](../Page/冰岛国旗.md "wikilink")||width="31%"
align=left|[冰岛共和国](../Page/冰岛.md "wikilink")
Iceland |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/印度国旗.md "wikilink")||width="31%"
align=left|[印度共和国](../Page/印度.md "wikilink")
India |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/印度尼西亚国旗.md "wikilink")||align=left|[印度尼西亚共和国](../Page/印度尼西亚.md "wikilink")
Indonesia |bgcolor=\#DCDCDC|
||**4:7**||[国旗](../Page/伊朗国旗.md "wikilink")||align=left|[伊朗伊斯兰共和国](../Page/伊朗.md "wikilink")
Iran |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/伊拉克国旗.md "wikilink")||align=left|[伊拉克共和国](../Page/伊拉克.md "wikilink")
Iraq |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/爱尔兰国旗.md "wikilink")||align=left|[爱尔兰共和国](../Page/爱尔兰共和国.md "wikilink")
Ireland |- |bgcolor=\#DCDCDC|
||**8:11**||[国旗](../Page/以色列国旗.md "wikilink")||align=left|[以色列国](../Page/以色列.md "wikilink")
Israel |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/意大利国旗.md "wikilink")||align=left|[意大利共和国](../Page/意大利.md "wikilink")
Italy |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/科特迪瓦国旗.md "wikilink")||align=left|[科特迪瓦共和国](../Page/科特迪瓦.md "wikilink")
Cote d'Ivoire（Ivory Coast） |}

## J

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/牙买加国旗.md "wikilink")||width="31%"
align=left|[牙买加](../Page/牙买加.md "wikilink")
Jamaica |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/日本国旗.md "wikilink")||width="31%"
align=left|[日本国](../Page/日本.md "wikilink")
Japan |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/约旦国旗.md "wikilink")||align=left|[约旦哈希姆王国](../Page/约旦.md "wikilink")
Jordan |}

## K

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/哈萨克斯坦国旗.md "wikilink")||width="31%"
align=left|[哈萨克斯坦共和国](../Page/哈萨克斯坦.md "wikilink")
Kazakhstan |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/肯尼亚国旗.md "wikilink")||width="31%"
align=left|[肯尼亚共和国](../Page/肯尼亚.md "wikilink")
Kenya |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/基里巴斯国旗.md "wikilink")||align=left|[基里巴斯共和国](../Page/基里巴斯.md "wikilink")
Kiribati |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/朝鲜国旗.md "wikilink")||align=left|[-{zh-hans:朝鲜;zh-hant:朝鲜}-民主主义人民共和国](../Page/朝鲜.md "wikilink")（-{zh-hans:朝鲜;zh-hant:北韓}-）
North Korea |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/韩国国旗.md "wikilink")||align=left|[大韩民国](../Page/大韩民国.md "wikilink")（-{zh-hans:韩国;zh-hant:南韓}-）
South Korea |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/科威特国旗.md "wikilink")||align=left|[科威特国](../Page/科威特.md "wikilink")
Kuwait |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/吉尔吉斯斯坦国旗.md "wikilink")||align=left|[吉尔吉斯共和国](../Page/吉尔吉斯斯坦.md "wikilink")
Kyrgyz Republic |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/科索沃國旗.md "wikilink")||align=left|[科索沃共和國](../Page/科索沃.md "wikilink")
Kosovo |}

## L

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/寮國國旗.md "wikilink")||width="31%"
align=left|[-{
zh-hans:老撾人民民主共和國;zh-hk:寮人民民主共和國}-](../Page/寮國.md "wikilink")
Laos |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/拉脫維亞國旗.md "wikilink")||width="31%"
align=left|[拉脫維亞共和國](../Page/拉脫維亞.md "wikilink")
Latvia |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/黎巴嫩国旗.md "wikilink")||align=left|[黎巴嫩共和國](../Page/黎巴嫩.md "wikilink")
Lebanon |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/賴索托國旗.md "wikilink")||align=left|[賴索托王國](../Page/賴索托.md "wikilink")
Lesotho |- |bgcolor=\#DCDCDC|
||**10:19**||[國旗](../Page/利比里亚国旗.md "wikilink")||align=left|[賴比瑞亞共和國](../Page/賴比瑞亞.md "wikilink")
Liberia |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/利比亚国旗.md "wikilink")||align=left|[利比亚国](../Page/利比亞.md "wikilink")
Libya |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/列支敦士登國旗.md "wikilink")||align=left|[列支敦士登公國](../Page/列支敦士登.md "wikilink")
Liechtenstein |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/立陶宛國旗.md "wikilink")||align=left|[立陶宛共和國](../Page/立陶宛.md "wikilink")
Lithuania |- |bgcolor=\#DCDCDC|
||**3:5**||[国旗](../Page/盧森堡國旗.md "wikilink")||align=left|[盧森堡大公國](../Page/盧森堡.md "wikilink")
Luxembourg |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/盧甘斯克人民共和國國旗.md "wikilink")||align=left|[盧甘斯克人民共和國](../Page/盧甘斯克人民共和國.md "wikilink")
Lugansk People's Republic |}

## M

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[國旗](../Page/馬其頓國旗.md "wikilink")||width="31%"
align=left|[馬其頓共和國](../Page/馬其頓共和國.md "wikilink")
Republic of Macedonia |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/馬達加斯加國旗.md "wikilink")||width="31%"
align=left|[馬達加斯加民主共和國](../Page/馬達加斯加.md "wikilink")
Madagascar |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/馬拉维國旗.md "wikilink")||align=left|[馬拉维共和國](../Page/馬拉威.md "wikilink")
Malawi |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/馬來西亞國旗.md "wikilink")||align=left|[馬來西亞聯邦](../Page/馬來西亞.md "wikilink")
Malaysia |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/馬爾地夫國旗.md "wikilink")||align=left|[馬爾地夫共和國](../Page/馬爾地夫.md "wikilink")
Maldives |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/馬利國旗.md "wikilink")||align=left|[馬利共和國](../Page/馬利.md "wikilink")
Mali |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/馬爾他國旗.md "wikilink")||align=left|[馬爾他共和國](../Page/馬爾他.md "wikilink")
Malta |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/馬爾他騎士團旗.md "wikilink")||align=left|[馬爾他騎士團](../Page/馬爾他騎士團.md "wikilink")
Sovereign Military Order of Malta |- |bgcolor=\#DCDCDC|
||**10:19**||[國旗](../Page/馬紹爾群島國旗.md "wikilink")||align=left|[馬紹爾群島共和國](../Page/馬紹爾群島.md "wikilink")
Marshall islands |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/茅利塔尼亞國旗.md "wikilink")||align=left|[茅利塔尼亞伊斯蘭共和國](../Page/茅利塔尼亞.md "wikilink")
Mauritania |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/模里西斯國旗.md "wikilink")||align=left|[模里西斯共和國](../Page/模里西斯.md "wikilink")
Mauritius |bgcolor=\#DCDCDC|
||**4:7**||[國旗](../Page/墨西哥國旗.md "wikilink")||align=left|[墨西哥合眾國](../Page/墨西哥.md "wikilink")
Mexico |- |bgcolor=\#DCDCDC|
||**10:19**||[國旗](../Page/密克羅尼西亞聯邦國旗.md "wikilink")||align=left|[密克羅尼西亞聯邦](../Page/密克羅尼西亞聯邦.md "wikilink")
Micronesia |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/摩爾多瓦國旗.md "wikilink")||align=left|[摩爾多瓦共和國](../Page/摩爾多瓦.md "wikilink")
Moldova |- |bgcolor=\#DCDCDC|
||**4:5**||[國旗](../Page/摩納哥國旗.md "wikilink")||align=left|[摩納哥公國](../Page/摩納哥.md "wikilink")
Monaco |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/蒙古國旗.md "wikilink")||align=left|[蒙古國](../Page/蒙古國.md "wikilink")
Mongolia |- |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/蒙特內哥羅國旗.md "wikilink")||align=left|[-{zh-hans:黑山共和國;zh-hant:蒙特內哥羅共和國（黑山）}-](../Page/蒙特內哥羅.md "wikilink")
Republic of Montenegro
（Republika Crna Gora） |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/摩洛哥國旗.md "wikilink")||align=left|[摩洛哥王國](../Page/摩洛哥.md "wikilink")
Morocco |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/莫三比克國旗.md "wikilink")||align=left|[莫三比克共和國](../Page/莫三比克.md "wikilink")
Mozambique |bgcolor=\#DCDCDC|
||**6:11**||[國旗](../Page/緬甸國旗.md "wikilink")||align=left|[緬甸聯邦](../Page/緬甸.md "wikilink")
Myanmar （Burma） |}

## N

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/納米比亞國旗.md "wikilink")||width="31%"
align=left|[納米比亞共和國](../Page/納米比亞.md "wikilink")
Namibia |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[國旗](../Page/諾魯國旗.md "wikilink")||width="31%"
align=left|[諾魯共和國](../Page/諾魯.md "wikilink")
Nauru |- |bgcolor=\#DCDCDC|
||**4:3**||[國旗](../Page/尼泊爾國旗.md "wikilink")||align=left|[尼泊爾王國](../Page/尼泊爾.md "wikilink")
Nepal |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/荷蘭國旗.md "wikilink")||align=left|[荷蘭王國](../Page/荷蘭.md "wikilink")
Netherlands |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/紐西蘭國旗.md "wikilink")||align=left|[紐西蘭](../Page/紐西蘭.md "wikilink")
New Zealand |bgcolor=\#DCDCDC|
||**3:5**||[國旗](../Page/尼加拉瓜國旗.md "wikilink")||align=left|[尼加拉瓜共和國](../Page/尼加拉瓜.md "wikilink")
Nicaragua |- |bgcolor=\#DCDCDC|
||**6:7**||[國旗](../Page/尼日國旗.md "wikilink")||align=left|[尼日共和國](../Page/尼日.md "wikilink")
Niger |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/奈及利亞國旗.md "wikilink")||align=left|[奈及利亞聯邦共和國](../Page/奈及利亞.md "wikilink")
Nigeria |- |bgcolor=\#DCDCDC|
||**8:11**||[國旗](../Page/挪威國旗.md "wikilink")||align=left|[挪威王國](../Page/挪威.md "wikilink")
Norway |}

## O

width="49%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/阿曼國旗.md "wikilink")||width="31%"
align=left|[阿曼蘇丹國](../Page/阿曼.md "wikilink")
Oman |}

## P

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[國旗](../Page/巴勒斯坦國旗.md "wikilink")||width="31%"
align=left|[巴勒斯坦國](../Page/巴勒斯坦.md "wikilink")
Palestine |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/巴基斯坦国旗.md "wikilink")||width="31%"
align=left|[巴基斯坦伊斯蘭共和國](../Page/巴基斯坦.md "wikilink")
Pakistan |- |bgcolor=\#DCDCDC|
||**5:8**||[國旗](../Page/帛琉國旗.md "wikilink")||align=left|[帛琉共和國](../Page/帛琉.md "wikilink")
Palau |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/巴拿馬國旗.md "wikilink")||align=left|[巴拿馬共和國](../Page/巴拿馬.md "wikilink")
Panama |- |bgcolor=\#DCDCDC|
||**3:4**||[國旗](../Page/巴布亞紐幾內亞國旗.md "wikilink")||align=left|[巴布亞紐幾內亞獨立國](../Page/巴布亞紐幾內亞.md "wikilink")
Papua New Guinea |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/巴拉圭國旗.md "wikilink")||align=left|[巴拉圭共和國](../Page/巴拉圭.md "wikilink")
Paraguay |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/秘魯國旗.md "wikilink")||align=left|[秘魯共和國](../Page/秘魯.md "wikilink")
Peru |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/菲律賓國旗.md "wikilink")||align=left|[菲律賓共和國](../Page/菲律賓.md "wikilink")
Philippines |- |bgcolor=\#DCDCDC|
||**5:8**||[國旗](../Page/波蘭國旗.md "wikilink")||align=left|[波蘭共和國](../Page/波蘭.md "wikilink")
Poland |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/葡萄牙國旗.md "wikilink")||align=left|[葡萄牙共和國](../Page/葡萄牙.md "wikilink")
Portugal |}

## Q

width="49%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**11:28**||width="5%"|[国旗](../Page/卡塔爾國旗.md "wikilink")||width="31%"
align=left|[卡塔爾國](../Page/卡塔爾.md "wikilink")
Qatar |}

## R

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/羅馬尼亞國旗.md "wikilink")||width="31%"
align=left|[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")
Romania |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/俄国国旗.md "wikilink")||width="31%"
align=left|[俄羅斯聯邦](../Page/俄羅斯.md "wikilink")
Russian Federation |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/盧旺達國旗.md "wikilink")||align=left|[盧安達共和國](../Page/盧安達.md "wikilink")
Rwanda |}

## S

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/聖克里斯多福及尼維斯國旗.md "wikilink")||width="31%"
align=left|[聖克里斯多福及尼維斯聯邦](../Page/聖基茨和尼維斯.md "wikilink")
St. Kitts and Nevis |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[國旗](../Page/聖露西亞國旗.md "wikilink")||width="31%"
align=left|[聖露西亞](../Page/聖露西亞.md "wikilink")
St. Lucia |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/聖文森特和格林納丁斯國旗.md "wikilink")||align=left|[圣文森特和格林纳丁斯](../Page/圣文森特和格林纳丁斯.md "wikilink")
Saint Vincent and the Grenadines |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/薩摩亞國旗.md "wikilink")||align=left|[薩摩亞獨立國](../Page/薩摩亞.md "wikilink")
Samoa |- |bgcolor=\#DCDCDC|
||**3:4**||[國旗](../Page/聖馬力諾國旗.md "wikilink")||align=left|[聖馬力諾](../Page/聖馬力諾.md "wikilink")
San Marino |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/聖多美及普林西比國旗.md "wikilink")||align=left|[聖多美及普林西比民主共和國](../Page/聖多美及普林西比.md "wikilink")
Sao Tome and Principe |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/沙特阿拉伯国旗.md "wikilink")||align=left|[沙烏地阿拉伯王國](../Page/沙烏地阿拉伯.md "wikilink")
Saudi Arabia |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/塞內加爾國旗.md "wikilink")||align=left|[塞內加爾共和國](../Page/塞內加爾.md "wikilink")
Senegal |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/塞爾維亞國旗.md "wikilink")||align=left|[塞爾維亞共和國](../Page/塞尔维亚.md "wikilink")
Serbia |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/塞席爾國旗.md "wikilink")||align=left|[塞席爾共和國](../Page/塞席爾.md "wikilink")
Seychelles |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/塞拉利昂國旗.md "wikilink")||align=left|[塞拉利昂共和國](../Page/塞拉利昂.md "wikilink")
Sierra Leone |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/新加坡国旗.md "wikilink")||align=left|[新加坡共和國](../Page/新加坡.md "wikilink")
Singapore |- |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/斯洛伐克國旗.md "wikilink")||align=left|[斯洛伐克共和國](../Page/斯洛伐克.md "wikilink")
Slovakia |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/斯洛文尼亚國旗.md "wikilink")||align=left|[斯洛文尼亚共和國](../Page/斯洛文尼亚.md "wikilink")
Slovenia |- |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/所罗门群岛國旗.md "wikilink")||align=left|[所罗门群岛](../Page/所罗门群岛.md "wikilink")
Solomon Islands |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/索馬利亞國旗.md "wikilink")||align=left|[索馬利亞](../Page/索馬利亞.md "wikilink")
Somalia |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/南非國旗.md "wikilink")||align=left|[南非共和國](../Page/南非.md "wikilink")
South Africa |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/南苏丹国旗.md "wikilink")||align=left|[南苏丹共和国](../Page/南苏丹.md "wikilink")
South Sudan |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/南奥塞梯共和国国旗.md "wikilink")||align=left|[南奥赛梯共和国](../Page/南奥赛梯.md "wikilink")
South Ossetia |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/西班牙國旗.md "wikilink")||align=left|[西班牙王國](../Page/西班牙.md "wikilink")
Spain |- |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/斯里蘭卡國旗.md "wikilink")||align=left|[斯里蘭卡民主社會主義共和國](../Page/斯里蘭卡.md "wikilink")
Sri Lanka |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/蘇丹國旗.md "wikilink")||align=left|[蘇丹共和國](../Page/蘇丹共和國.md "wikilink")
Sudan |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/蘇利南國旗.md "wikilink")||align=left|[蘇利南共和國](../Page/蘇利南.md "wikilink")
Suriname |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/斯威士兰國旗.md "wikilink")||align=left|[斯威士兰王國](../Page/斯威士兰.md "wikilink")
Swaziland |- |bgcolor=\#DCDCDC|
||**5:8**||[國旗](../Page/瑞典国旗.md "wikilink")||align=left|[瑞典王國](../Page/瑞典.md "wikilink")
Sweden |bgcolor=\#DCDCDC|
||**1:1**||[國旗](../Page/瑞士國旗.md "wikilink")||align=left|[瑞士聯邦](../Page/瑞士.md "wikilink")
Switzerland |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/叙利亚国旗.md "wikilink")||align=left|[敘利亞阿拉伯共和國](../Page/敘利亞.md "wikilink")
Syria |}

## T

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[國旗](../Page/塔吉克國旗.md "wikilink")||width="31%"
align=left|[塔吉克共和國](../Page/塔吉克共和國.md "wikilink")
Tajikistan |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[國旗](../Page/坦尚尼亞國旗.md "wikilink")||width="31%"
align=left|[坦尚尼亞聯合共和國](../Page/坦尚尼亞聯合共和國.md "wikilink")
Tanzania |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/泰國國旗.md "wikilink")||align=left|[泰王國](../Page/泰國.md "wikilink")
Thailand |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/东帝汶国旗.md "wikilink")||align=left|[东帝汶民主共和国](../Page/东帝汶.md "wikilink")
Timor-Leste |- |bgcolor=\#DCDCDC|
||**[1:1.618](../Page/黃金比例.md "wikilink")**||[國旗](../Page/多哥國旗.md "wikilink")||align=left|[多哥共和國](../Page/多哥.md "wikilink")
Togo |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/東加國旗.md "wikilink")||align=left|[東加王國](../Page/東加.md "wikilink")
Tonga |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/德涅斯特河沿岸共和國國旗.md "wikilink")||align=left|[德涅斯特河沿岸摩爾達維亞共和國](../Page/德涅斯特河沿岸摩爾達維亞共和國.md "wikilink")
Transnistria |bgcolor=\#DCDCDC|
||**3:5**||[國旗](../Page/千里達和多巴哥國旗.md "wikilink")||align=left|[-{zh-hans:特立尼达和多巴哥共和國;zh-hant:千里達和托巴哥共和國}-](../Page/特立尼达和多巴哥.md "wikilink")
Trinidad and Tobago |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/突尼西亞國旗.md "wikilink")||align=left|[突尼西亞共和國](../Page/突尼西亞.md "wikilink")
Tunisia |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/土耳其國旗.md "wikilink")||align=left|[土耳其共和國](../Page/土耳其.md "wikilink")
Turkey |- |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/土庫曼斯坦國旗.md "wikilink")||align=left|[土庫曼斯坦](../Page/土庫曼斯坦.md "wikilink")
Turkmenistan |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/吐瓦魯國旗.md "wikilink")||align=left|[吐瓦魯](../Page/吐瓦魯.md "wikilink")
Tuvalu |}

## U

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/烏干達國旗.md "wikilink")||width="31%"
align=left|[烏干達共和國](../Page/烏干達.md "wikilink")
Uganda |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/烏克蘭國旗.md "wikilink")||width="31%"
align=left|[烏克蘭](../Page/烏克蘭.md "wikilink")
Ukraine |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/阿拉伯聯合大公國國旗.md "wikilink")||align=left|[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")
United Arab Emirates |bgcolor=\#DCDCDC|
||**1:2**||[國旗](../Page/英国国旗.md "wikilink")||align=left|[大不列顛及北愛爾蘭聯合王國](../Page/英國.md "wikilink")（英國）
United Kingdom（Britain, Great Britain） |- |bgcolor=\#DCDCDC|
||**10:19**||[國旗](../Page/美国国旗.md "wikilink")||align=left|[美利堅合眾國](../Page/美國.md "wikilink")（美國）
United States of America |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/烏拉圭國旗.md "wikilink")||align=left|[-{
zh-hans:乌拉圭东岸共和国;zh-hant:烏拉圭東部共和國}-](../Page/烏拉圭.md "wikilink")
Uruguay |- |bgcolor=\#DCDCDC|
||**1:2**||[国旗](../Page/烏茲別克國旗.md "wikilink")||align=left|[烏茲別克共和國](../Page/烏茲別克.md "wikilink")
Uzbekistan |}

## V

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**3:5**||width="5%"|[国旗](../Page/萬那杜國旗.md "wikilink")||width="31%"
align=left|[瓦努阿图共和國](../Page/瓦努阿图.md "wikilink")
Vanuatu |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:1**||width="5%"|[國旗](../Page/梵蒂冈国旗.md "wikilink")||width="31%"
align=left|[-{
zh-hans:梵蒂冈;zh-hk:梵蒂岡}-城國](../Page/梵蒂岡.md "wikilink")（教廷）
Vatican City （The Holy See） |- |bgcolor=\#DCDCDC|
||**2:3**||[国旗](../Page/委內瑞拉國旗.md "wikilink")||align=left|[委內瑞拉玻利瓦尔共和國](../Page/委內瑞拉.md "wikilink")
Venezuela |bgcolor=\#DCDCDC|
||**2:3**||[國旗](../Page/越南國旗.md "wikilink")||align=left|[越南社會主義共和國](../Page/越南.md "wikilink")
Vietnam |}

## W

width="49%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[國旗](../Page/西撒哈拉國旗.md "wikilink")||width="31%"
align=left|[撒拉威阿拉伯民主共和國](../Page/西撒哈拉.md "wikilink")（西撒哈拉）
Western Sahara |}

## Y

width="49%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/葉門國旗.md "wikilink")||width="31%"
align=left|[葉門共和國](../Page/葉門.md "wikilink")
Yemen |}

## Z

width="98%" style="text-align:center" |- |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**2:3**||width="5%"|[国旗](../Page/尚比亞國旗.md "wikilink")||width="31%"
align=left|[尚比亞共和國](../Page/尚比亞.md "wikilink")
Zambia |width="8%" bgcolor=\#DCDCDC|
||width="5%"|**1:2**||width="5%"|[国旗](../Page/辛巴威國旗.md "wikilink")||width="31%"
align=left|[辛巴威共和國](../Page/辛巴威.md "wikilink")
Zimbabwe |}

[\*](../Category/国旗.md "wikilink")