**新幹線500系電聯車**是[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）所使用的[新幹線車輛](../Page/新幹線.md "wikilink")。500系由[川崎重工業](../Page/川崎重工業.md "wikilink")、[日立製作所](../Page/日立製作所.md "wikilink")、[近畿車輛和](../Page/近畿車輛.md "wikilink")[日本車輛製造四家公司承造](../Page/日本車輛製造.md "wikilink")，一共生產了144輛（9列16輛編組）。首列列車於1997年3月22日投入服務，行駛[東海道](../Page/東海道新幹線.md "wikilink")、[山陽新幹線直通的](../Page/山陽新幹線.md "wikilink")「[希望](../Page/希望號列車.md "wikilink")」號班次。列車車門為[嵌入式車門](../Page/內崁式門.md "wikilink"),但類似一般新幹線的內藏門。1996年獲得[通商產業省](../Page/通商產業省.md "wikilink")[最佳產品設計獎](../Page/好設計獎.md "wikilink")（現已改稱為財團法人日本產業設計振興會優良產品設計賞），1998年獲得[鐵道友之會頒發第](../Page/鐵道友之會.md "wikilink")41屆[藍絲帶獎](../Page/藍絲帶獎_\(鐵路\).md "wikilink")，為該公司於1987年成立以來第一款獲獎的鐵路車輛。

500系曾是新幹線系統時速最高的列車，設計時速達350km/h。可是因早期修築的東海道新幹線彎道路段較多，500系在該路段的極速被限制在270km/h。在較晚修築、路線規格較高的[山陽新幹線路段](../Page/山陽新幹線.md "wikilink")（姬路站以西路段），500系則能以全速300km/h行駛。

## 概述

[Shinkansen_and_Himeji_Station_M9_45.jpg](https://zh.wikipedia.org/wiki/File:Shinkansen_and_Himeji_Station_M9_45.jpg "fig:Shinkansen_and_Himeji_Station_M9_45.jpg")
500系是因應[山陽新幹線的提速計劃而設計的車款](../Page/山陽新幹線.md "wikilink")，以與當時的航空業競爭。在500系出現以前，JR西日本有關方面曾於1992年製造一列6節原型車進行測試。這列列車正式型號為[新幹線500系900番台](../Page/新幹線500系900番台電力動車組.md "wikilink")，暱稱為“WIN350”，是“**W**est
Japan Railway's **In**novation for the operation at
**350**km/h”的簡稱（而WIN則帶有「勝」、「征服」等意味，在此指「勝過速限」、「征服速度」）。除了挑戰時速350公里的最高速度外，也測試車輛在此高速度環境運作的表現。在500系投入服務後，該列列車已報廢，並保存在[博多總合車輛所及米原站附近的](../Page/博多綜合車輛所.md "wikilink")[鐵道總合技術研究所](../Page/鐵道總合技術研究所.md "wikilink")（JR總研）風洞技術中心內。

在500系的車輛設計過程中，知名的[德国](../Page/德国.md "wikilink")[工業設計師](../Page/工業設計師.md "wikilink")（Alexander
Neumeister）也參與了部分的相關工作\[1\]。

500系在東京～博多之間的1069.1公里（實際里程）路段裡運行，[東海道新幹線區間內](../Page/東海道新幹線.md "wikilink")，和其他列車一樣最高時速為每小時270公里，但在較晚修築、路線規格較高的[山陽新幹線區間內](../Page/山陽新幹線.md "wikilink")，便能發揮出500系原來的實力。在隧道多又狹窄的日本路線上，搖晃既少又安靜的500系曾是日本最高營運速度設定的車種。但由於舒適性（車室內空間與座椅間隔較狹小）以及沒拖車（因使用全動車編組造成行駛時[噪音過大](../Page/噪音.md "wikilink")）等問題，500系在[東海旅客鐵道](../Page/東海旅客鐵道.md "wikilink")（JR東海）轄下的東海道新幹線的服務已逐漸被2007年7月1日開始投入服務的[N700系取代](../Page/新幹線N700系電聯車.md "wikilink")，並於2010年3月1日起全面撤出東海道新幹線服務。而剩下的500系車輛被縮編為8輛編組，取代退役的[0系行駛山陽新幹線的](../Page/新幹線0系電聯車.md "wikilink")「[回聲](../Page/木靈號列車.md "wikilink")」號班次。

## 構造

車輛構造以[鋁合金為主](../Page/鋁合金.md "wikilink")，令全車16節列車總重量只有688噸。
電動機輸出功率達285kW（W2-W9編組為275kW），每節均載有4台電動機，令全車總輸出功率達18,240kW（約25,000hp、W2-W9編組為17,600kW）。

## 外觀

[JR_West_500_051.JPG](https://zh.wikipedia.org/wiki/File:JR_West_500_051.JPG "fig:JR_West_500_051.JPG")
首尾兩節呈飛機尖頭，以造成流線形抵抗風阻及突如其來的氣壓。
首尾兩節列車長達27米，比正常車卡長2米，當中車頭則長達15米。車體均採用圓筒形設計，是該系列車獨有的。

### 500 TYPE EVA

JR西日本為慶祝山陽新幹線通車40年，與動畫《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》（EVA）播出20年\[2\]\[3\]，將一列500系彩繪為紫色底與螢光綠的「[初號機](../Page/初號機.md "wikilink")」，由曾參與EVA作品內機械設計的[山下育人操刀車內外設計](../Page/山下育人.md "wikilink")（他同時是500系的愛好者），並由導演[庵野秀明監製](../Page/庵野秀明.md "wikilink")\[4\]。除了車外塗裝，第一節車廂內有實物大小的初號機駕駛艙模型可體驗乘坐，採售票預約。另外還有自由參觀的EVA作品展示空間。車內廣播提示音樂也改為電視版EVA主題曲《[殘酷天使的行動綱領](../Page/殘酷天使的行動綱領.md "wikilink")》（）。

此彩繪列車稱為「500 TYPE
EVA」，於2015年11月7日開始上線行駛於新大阪至博多間、每日兩班次的回聲號。原本僅行駛至2017年3月，但因深受好評，JR西日本延長行駛至2018年5月13日為止\[5\]\[6\]。

[File:Nozomi.JPG|500系车头](File:Nozomi.JPG%7C500系车头)
[File:JRW-TEC500-frontnose.jpg|500系侧面车头](File:JRW-TEC500-frontnose.jpg%7C500系侧面车头)
<File:201801> Kodama 730 at Shin-Kobe
Station.jpg|[初号机涂装的](../Page/初号机.md "wikilink")500系在[新神户站](../Page/新神户站.md "wikilink")

## 集電装置

每列500系設有2套WPS204型[集电弓](../Page/集电弓.md "wikilink")（希望號時代的16輛W編组是設在第5、13号车厢），為了降低行車時的噪音，500系不再使用傳統的[菱形集電弓构造](../Page/菱形集電弓.md "wikilink")，而是改用官方正式命名為「翼型集電弓」、實際上是在橢圓型断面的支柱上設置翼狀的集電面，整體呈T字型狀。為了要將噪音降低，便採用了[一級方程式賽車](../Page/一級方程式賽車.md "wikilink")（F1賽車）所發展的空氣動力技术，與以滑翔的[猫头鹰羽毛毽做为参考而做成的](../Page/猫头鹰.md "wikilink")[擾流翼](../Page/擾流翼.md "wikilink")。風洞部分則在集電弓臂上开了5mm的小孔，使得集電弓在高速移動時產生氣旋所造成的[風切噪音降低](../Page/風切.md "wikilink")。該列車集电弓所使用的[減振器](../Page/避震器.md "wikilink")（damper）是委託長年製造F1賽車用避震器、對於300km/h車速以上的行車環境經驗豐富資料齊全的汽車用避震器名廠[昭和](../Page/昭和公司.md "wikilink")（；Showa）製造。

其他的新干线车辆的受电弓是金属彈簧上升式，不过，翼型受电弓則采用空气上升式。由於这个缘故，如果在长时间的停电等導致车辆的压缩空气减压时，受电弓會自然下降，使得[接地保護開關](../Page/接地保護開關.md "wikilink")（EGS）無法應付架线接地而[短路的情形](../Page/短路.md "wikilink")。为此，在絕緣礙子蓋内设置彈簧上升式的EGS用预备单臂式受电弓。

在东海道、山阳新干线列车的车辆中，静电天线位於[0系至](../Page/新幹線0系電力動車組.md "wikilink")[300系列車驾驶座位头顶上](../Page/新幹線300系電力動車組.md "wikilink")([700系](../Page/新幹線700系電力動車組.md "wikilink")、N700系則在前头车联结方面)，不过，500系的静电天线被设置在集电装置中的絕緣礙子蓋内，而無法目测确认。

而在改造為V編組之後，主集电装置被换成在700系等被采用的单臂型集电弓（WPS208型），静电天线在第2、7号车厢偏向前头车的目测确认位置也被变更。

## 制動裝置

全車均設有回生制動裝置，在16節編組列車中，第1、8、9及16號車更裝設氧化鋁噴灑制動裝置。

## 內裝

由於需要遷就特別的車體設計，500系列車內部比一般的列車為小是最主要的缺點。
為遷就車頭呈尖頭設計，首尾兩輛車只能載客53（1號車廂）／63（16號車廂）人，比300系少載12人，而且只有尾部一扇車門。座位前後間隔為1020mm，比一般列車窄了20mm，加上圓筒形設計，令乘客壓迫感更甚。

|                 |       |                      |        |      |
| --------------- | ----- | -------------------- | ------ | ---- |
|                 | 博多方   | 300系、500系、700系、N700系 | 東京方    |      |
|                 | 1号車   | 2号車                  | 3号車    | 4号車  |
| 300系、700系、N700系 | 65名   | 100名                 | 85名    | 100名 |
| 500系            | 53名   | 100名                 | 90名    | 100名 |
| 差異              | *12名* | 0名                   | **5名** | 0名   |

新幹線16辆編組車輛定員

\**斜字*是500系比其他列车定员少，**粗体字**是500系比其他列车定员多。

<File:Blue> Seat of JR 500.JPG|普通座(浅蓝色) <File:Red> Seat of JR
500.JPG|普通座(浅红色) <File:Green> Car's Seat of JR 500.JPG|一等座(绿色车厢)
<File:500> Series Shinkansen inside.jpg|普通车厢 <File:Shinkansen>
500-series First-class-interior.jpg|一等座(绿色车厢)

## 列車編組表

<table>
<caption>500系列車構成表</caption>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p>←博多</p></td>
<td><p> </p></td>
<td><p>東京→</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>1号車</p></td>
<td><p>2号車</p></td>
<td><p>3号車</p></td>
</tr>
<tr class="odd">
<td><p>W編組</p></td>
<td><p>521型<br />
(Mc)</p></td>
<td><p>526型<br />
(M1)</p></td>
<td><p>527型<br />
(Mp)</p></td>
</tr>
<tr class="even">
<td><p>第1动力组合</p></td>
<td><p>第2动力组合</p></td>
<td><p>第3动力组合</p></td>
<td><p>第4动力组合</p></td>
</tr>
<tr class="odd">
<td><p>V編組</p></td>
<td><p>521型<br />
(Mc)</p></td>
<td><p>526型<br />
(M1)</p></td>
<td><p>527型<br />
(Mp)</p></td>
</tr>
<tr class="even">
<td><p>第1动力组合</p></td>
<td><p>第2动力组合</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 退出「希望号」

[JRwest_500_nozomi_29_kodama_697_shiniwakuni.jpg](https://zh.wikipedia.org/wiki/File:JRwest_500_nozomi_29_kodama_697_shiniwakuni.jpg "fig:JRwest_500_nozomi_29_kodama_697_shiniwakuni.jpg")
）\]\]
2009年3月14日訂正列車時間表，使用500系運行的東海道、山陽新幹線直通「希望」號定期列車班次為每日來回2班次（分別是6號、28號、29號和51號）。

而「回聲」號定期列車班次為746、766、768、770、854及858號；723、737、745、765、783、787、859及865號。

2009年11月10日起，28號和51號「希望」號定期列車班次改由[N700系行駛](../Page/新幹線N700系電聯車.md "wikilink")，「希望」號定期列車班次進一步縮減至每日來回1班次（6號、29號）。

[JR西日本亦在](../Page/西日本旅客鐵道.md "wikilink")2009年10月16日發表的冬季臨時列車時間表上宣佈，500系列車將於2010年3月1日起全面撤出東海道新幹線服務，所有原使用500系的東海道、山陽新幹線定期希望號班次將改由[N700系行駛](../Page/新幹線N700系電聯車.md "wikilink")。

2010年2月28日，500系车辆定期希望号班次列车（6号、29号）从东京站开出最后班次前往博多，翌日改由N700系行驶。

## V编成8节编组改造

[Shinkansen_500_series_8_cars.png](https://zh.wikipedia.org/wiki/File:Shinkansen_500_series_8_cars.png "fig:Shinkansen_500_series_8_cars.png")
[JRW-500_V2_inHimeji.jpg](https://zh.wikipedia.org/wiki/File:JRW-500_V2_inHimeji.jpg "fig:JRW-500_V2_inHimeji.jpg")停靠的500系V2编組\]\]
至於餘下的500系列車，JR西日本在2007年12月公佈計劃將500系列車縮編組8輛的編組，並在2008年12月起取代退役的0系行走山陽新幹線的「[回聲](../Page/木靈號列車.md "wikilink")」號（）班次（除了W1编成）\[7\]\[8\]\[9\]。

2008年3月28日，博多總合車輛所公佈開始對500系列車進行8輛化改造工程\[10\]。在W編組中，只有第1至4、10、11、13和16號車廂獲得保留，其餘全部報廢\[11\]。

改造後的列車改型號為7000番台，編組編號中的英文字亦從W更改為V。首列完成改裝的列車為V3編組，目前共有5組V編組列車在[山陽新幹線行駛](../Page/山陽新幹線.md "wikilink")「回聲」號班次及[博多南線直達班次](../Page/博多南線.md "wikilink")。根據JR西日本公佈的資料，7000番台列車的最高營運時速為285公里\[12\]。

500系在2007年12月22日，曾以16輛W編組代替原本的[300系F編組擔任回聲](../Page/新幹線300系電力動車組.md "wikilink")627號、682號列車的任務，往返於[岡山與](../Page/岡山車站.md "wikilink")[博多之間](../Page/博多車站.md "wikilink")，打破從1997年問世以來，從不被「超車」的記錄（该纪录已被N700系超越）。

隨著更多舊編組列車改造，以及配合JR東海對東海道新幹線使用的列車變更，500系列車將全數改造為8輛編組。

### 以下是短编組化改造日期及载客人数

<table>
<thead>
<tr class="header">
<th><p>原編組</p></th>
<th><p>废弃日期<br />
（編組名廢除）</p></th>
<th><p>入場日期</p></th>
<th><p>出場日期</p></th>
<th><p>編組名登錄日期</p></th>
<th><p>目前編組</p></th>
<th><p>运营日期</p></th>
<th><p>備考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>W1</p></td>
<td><p>2012年1月30日</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p>全列退役</p></td>
</tr>
<tr class="even">
<td><p>W2</p></td>
<td><p>2008年9月17日</p></td>
<td><p>2008年9月18日</p></td>
<td><p>2009年1月14日</p></td>
<td></td>
<td><p>V2</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>W3</p></td>
<td><p>2007年11月1日</p></td>
<td><p>2007年11月2日</p></td>
<td><p>2008年3月28日</p></td>
<td><p>2008年3月28日</p></td>
<td><p>V3</p></td>
<td><p>2009年9月30日</p></td>
<td><p>短編組化第1号</p></td>
</tr>
<tr class="even">
<td><p>W4</p></td>
<td><p>2008年7月18日</p></td>
<td><p>2008年7月18日</p></td>
<td><p>2008年10月27日</p></td>
<td></td>
<td><p>V4</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>W5</p></td>
<td><p>2008年1月10日</p></td>
<td><p>2008年2月6日</p></td>
<td><p>2008年5月20日</p></td>
<td><p>2008年5月17日</p></td>
<td><p>V5</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>W6</p></td>
<td><p>2008年4月20日</p></td>
<td><p>2008年4月23日</p></td>
<td><p>2008年9月2日</p></td>
<td><p>2008年8月30日</p></td>
<td><p>V6</p></td>
<td><p>2009年9月17日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>W7</p></td>
<td><p>2010年1月13日</p></td>
<td><p>2010年1月13日</p></td>
<td><p>2010年5月10日</p></td>
<td><p>2010年5月10日</p></td>
<td><p>V7</p></td>
<td><p>2010年5月10日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>W8</p></td>
<td><p>2010年3月10日</p></td>
<td><p>2010年3月10日</p></td>
<td><p>2010年6月29日</p></td>
<td><p>2010年6月29日</p></td>
<td><p>V8</p></td>
<td><p>2010年6月29日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>W9</p></td>
<td><p>2009年11月11日</p></td>
<td><p>2009年11月11日</p></td>
<td><p>2010年2月24日</p></td>
<td><p>2010年2月24日</p></td>
<td><p>V9</p></td>
<td><p>2010年2月24日</p></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>博多方</p></td>
<td><p>V編組</p></td>
<td><p>新大阪方</p></td>
</tr>
<tr class="even">
<td><p>編組</p></td>
<td><p>1号車<br />
521(M<small>C</small>)<br />
53名</p></td>
<td><p>2号車<br />
526(M<small>1</small>)<br />
100名</p></td>
<td><p>3号車<br />
527(M<small>p</small>)<br />
78名</p></td>
</tr>
<tr class="odd">
<td><p>V2</p></td>
<td><p>7002</p></td>
<td><p>7004</p></td>
<td><p>7003</p></td>
</tr>
<tr class="even">
<td><p>V3</p></td>
<td><p>7003</p></td>
<td><p>7007</p></td>
<td><p>7005</p></td>
</tr>
<tr class="odd">
<td><p>V4</p></td>
<td><p>7004</p></td>
<td><p>7010</p></td>
<td><p>7007</p></td>
</tr>
<tr class="even">
<td><p>V5</p></td>
<td><p>7005</p></td>
<td><p>7013</p></td>
<td><p>7009</p></td>
</tr>
<tr class="odd">
<td><p>V6</p></td>
<td><p>7006</p></td>
<td><p>7016</p></td>
<td><p>7011</p></td>
</tr>
<tr class="even">
<td><p>V7</p></td>
<td><p>7007</p></td>
<td><p>7019</p></td>
<td><p>7013</p></td>
</tr>
<tr class="odd">
<td><p>V8</p></td>
<td><p>7008</p></td>
<td><p>7022</p></td>
<td><p>7015</p></td>
</tr>
<tr class="even">
<td><p>V9</p></td>
<td><p>7009</p></td>
<td><p>7025</p></td>
<td><p>7017</p></td>
</tr>
</tbody>
</table>

### 短编組化的变更

  - 实施内部装饰的行李架更换
  - 由于全座位禁烟，其中2節車廂（第3、7号车厢的博多方向）设置了吸烟室。再者，[博多南线内因为全面禁烟](../Page/博多南线.md "wikilink")，在吸烟室內也不能吸烟。
      - 由于间隔门位置的关系变更了偏向吸烟室的座位為2列到2列+2列。因此定员减少（第3号车厢：90名→78名，第7号车厢：63名→51名）。新的第3、6、8号车厢（原第3、10、16号车厢）是原吸烟车。
  - 原第10号车厢(新第6号车厢)的原綠色車廂普通车化。撤去座位的枕头、脚垫與音响系统。定员没有变更。
  - 撤去原第13号车厢（新第5号车厢）相称的独创的翼型受电弓（组成的车辆原第5号车厢），新的第2、7号车厢新设单臂型受电弓（WPS208型式）。
      - 为了安裝折叠尺寸比原來翼型受电弓大的单臂型受电弓，因此需要切割屋顶构体並移设。
  - 新的第4、5号车厢间移设原第8、9号车厢间的電源插座。
  - 原第11号车厢对（新第7号车厢）的业务用室的空隙空间，设置补助变压器。
  - 新第2、7号车厢增加半自动悬挂系統、静电天线及补助电动空气压缩机。
  - 新第2、4、6号车厢增加緊急[摺梯](../Page/摺梯.md "wikilink")。
  - 增加車門開關時的門鈴聲（声音與700系、N700系相同）。
  - 按照N700系的设计变更座椅背面桌子上的车内向导图，並变更玄關處的照明為白色LED。

<File:JRW-500> V2 521-7002.jpg|521型7000番台（521-7002） <File:JRW-500> V2
522-7002.jpg|522型7000番台（522-7002） <File:JRW-500> V2
525-7004.jpg|525型7000番台（525-7004） <File:JRW-500> V2
526-7004.jpg|526型7000番台（526-7004） <File:JRW-500> V2
526-7202.jpg|526型7200番台（526-7202） <File:JRW-500> V2
527-7003.jpg|527型7000番台（527-7003） <File:JRW-500> V2
527-7702.jpg|527型7700番台（527-7702） <File:JRW-500> V2
528-7002.jpg|528型7000番台（528-7002）

## 參考資料

<div class="references-small">

<references />

</div>

## 相關條目

  - [新幹線E2系電力動車組](../Page/新幹線E2系電力動車組.md "wikilink")
  - [新幹線E3系電力動車組](../Page/新幹線E3系電力動車組.md "wikilink")
  - [新幹線E4系電力動車組](../Page/新幹線E4系電力動車組.md "wikilink")
  - [新幹線E5系電力動車組](../Page/新幹線E5系電力動車組.md "wikilink")
  - [新幹線E6系電力動車組](../Page/新幹線E6系電力動車組.md "wikilink")
  - [新幹線0系電力動車組](../Page/新幹線0系電力動車組.md "wikilink")
  - [新幹線100系電力動車組](../Page/新幹線100系電力動車組.md "wikilink")
  - [新幹線200系電力動車組](../Page/新幹線200系電力動車組.md "wikilink")
  - [新幹線300系電力動車組](../Page/新幹線300系電力動車組.md "wikilink")
  - [WIN350](../Page/新幹線500系900番台電力動車組.md "wikilink")
  - [新幹線700系電力動車組](../Page/新幹線700系電力動車組.md "wikilink")
  - [新幹線N700系電力動車組](../Page/新幹線N700系電力動車組.md "wikilink")
  - [新幹線800系電力動車組](../Page/新幹線800系電力動車組.md "wikilink")

## 外部連結

  - [JR西日本：車輛圖鑑－新幹線500系（「希望」號（））](https://web.archive.org/web/20090125123333/http://www.jr-odekake.net/train/nozomi_500/index.html)

  - [JR西日本：車輛圖鑑－新幹線500系（「回聲」號（））](http://www.jr-odekake.net/train/kodama_500/index.html)

  - [列車介紹（近畿車輛）](https://web.archive.org/web/20071025014236/http://www.kinkisharyo.co.jp/ja/sharyo/sh-seihin/sh-w500.htm)

  - [列車介紹（川崎重工業）](https://web.archive.org/web/20071009184455/http://www.khi.co.jp/sharyo/pro_final/pro_jrn500.html)

  - [列車介紹（日本車輛）](http://www.n-sharyo.co.jp/business/tetsudo/pages/jrw500.htm)

[500](../Category/新幹線車輛.md "wikilink")
[Category:西日本旅客鐵道車輛](../Category/西日本旅客鐵道車輛.md "wikilink")
[Category:日本電力動車組](../Category/日本電力動車組.md "wikilink")
[Category:日本車輛製鐵路車輛](../Category/日本車輛製鐵路車輛.md "wikilink")
[Category:川崎製鐵路車輛](../Category/川崎製鐵路車輛.md "wikilink")
[Category:近畿製鐵路車輛](../Category/近畿製鐵路車輛.md "wikilink")
[Category:日立製鐵路車輛](../Category/日立製鐵路車輛.md "wikilink")
[Category:25千伏60赫茲交流電力動車組](../Category/25千伏60赫茲交流電力動車組.md "wikilink")
[Category:好設計獎](../Category/好設計獎.md "wikilink") [Category:藍絲帶獎
(鐵路車輛)](../Category/藍絲帶獎_\(鐵路車輛\).md "wikilink")

1.
2.  [「新幹線：エヴァンゲリオン プロジェクト」始動\! 今秋より「500 TYPE
    EVA」を運転します](http://www.westjr.co.jp/press/article/2015/07/page_7409.html)
    - 西日本旅客鉄道 2015年7月23日
3.  [500TYPE EVA PROJECT│新幹線：エヴァンゲリオン
    プロジェクト│JR西日本](http://www.500type-eva.jp/)
4.
5.
6.
7.  [](http://sankei.jp.msn.com/life/lifestyle/071020/sty0710200915001-n1.htm)
    －産經新聞，2007年10月20日
8.  [](http://www.nishinippon.co.jp/nnp/local/yamaguchi/20071020/20071020_005.shtml)
    －西日本新聞，2007年10月20日
9.  [](http://www.westjr.co.jp/news/newslist/article/1173458_799.html)
    －，2007年12月20日
10. 交通新聞，2008年4月1日
11. 《》，2008年6月号pp.86-87。
12. [JR西日本車輛圖鑑：新幹線500系（「回聲」號用）](http://www.jr-odekake.net/train/kodama_500/index.html)