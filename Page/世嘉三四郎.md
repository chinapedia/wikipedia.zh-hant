世嘉三四郎是日本[世嘉公司創造的虛構角色](../Page/世嘉.md "wikilink")。由日本動作演員[藤岡弘､所飾演](../Page/藤岡弘､.md "wikilink")。

## 概要

  - 1997年至1998年間，世嘉公司為了推廣遊戲機[SEGA
    Saturn](../Page/SEGA_Saturn.md "wikilink")，創造了一系列以世嘉三四郎為角色的廣告。
  - 世嘉三四郎這個角色是[戲仿](../Page/戲仿.md "wikilink")[黑澤明的電影](../Page/黑澤明.md "wikilink")[姿三四郎](../Page/姿三四郎_\(電影\).md "wikilink")。除了名字相似，兩者同是柔道選手。而扮演世嘉三四郎的演員[藤岡弘](../Page/藤岡弘.md "wikilink")，本身也是一名武道家。
  - 由於角色大受歡迎，世嘉方面曾經推出以世嘉三四郎為主角的遊戲。而相關廣告歌的[CD也賣出超過十萬張](../Page/CD.md "wikilink")。
  - 1998年，在日本的年末盛會[NHK紅白歌唱大賽登場演出](../Page/NHK紅白歌唱大賽.md "wikilink")。
  - 這一系列廣告於世嘉公司推出新遊戲機[Dreamcast後結束](../Page/Dreamcast.md "wikilink")。

## 人物資料

  - 生於日本，180CM高重74KG。
  - 柔道三段，空手道初段，拔刀道四段和小刀護身道四段
  - 目標是引導其他人走向「SEGA SATURN道」

## 相關廣告

  - '''『SONIC R』 '''

<!-- end list -->

  - '''『Shining Force III』 '''

<!-- end list -->

  - '''『ボンバーマン ファイト\!』 '''
      -
        初次有主歌曲

<!-- end list -->

  - **三シローディスクもついてくる**

<!-- end list -->

  - **今年は凄いゾッ\!**
  - 背著巨大Saturn在深山訓練

<!-- end list -->

  - **『ソロ・クライシス』**

<!-- end list -->

  - '''『AZEL -PANZER DRAGOON RPG-』

'''

  - **『Winter Heat』**

<!-- end list -->

  - **『プロ野球チームもつくろう\!』**

<!-- end list -->

  - **『バーニングレンジャー』**

<!-- end list -->

  - **『GREATEST NINE '98』**

<!-- end list -->

  - **『ザ・ハウス・オブ・ザ・デッド』**

<!-- end list -->

  - **『ドラゴンフォースII』**

<!-- end list -->

  - **『サクラ大戦2 ～君、死にたもうことなかれ～』**

<!-- end list -->

  - **『DEEP FEAR』**

<!-- end list -->

  - **『ワールドカップ98 フランス』**

<!-- end list -->

  - **『日本代表チームの監督になろう』**

<!-- end list -->

  - **『バッケンローダー』**

<!-- end list -->

  - **『せがた三四郎 真剣遊戯』**
  - 最後一個三四郎系列廣告，是為了宣告其角色死亡又推廣以他命名的遊戲而拍攝。

## 相關產品

  - せがた三四郎　真劍遊戲（Sega Saturn／遊戲）
  - せがた三四郎　超人傳說（PC／多媒體資料集）
  - セガサターン、シロ！（音樂CD）
  - セガサターン、りみっくすシロ！（音樂CD）

## 外部連結

  - [せがた三四郎 非官方中文站](http://cubeat.game.tw/346)

[Category:世嘉角色](../Category/世嘉角色.md "wikilink")
[Category:电子游戏角色](../Category/电子游戏角色.md "wikilink")
[Category:虚构柔道者](../Category/虚构柔道者.md "wikilink")