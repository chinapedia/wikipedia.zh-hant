**蘇沃洛夫勳章**（[俄文](../Page/俄文.md "wikilink")：****）是一種[蘇聯和](../Page/蘇聯.md "wikilink")[俄羅斯頒發的](../Page/俄羅斯.md "wikilink")[勳章](../Page/勳章.md "wikilink")，共分三級。该勳章是紀念[亞歷山大·蘇沃洛夫](../Page/亞歷山大·蘇沃洛夫.md "wikilink")，於1942年7月29日（[二戰期間](../Page/二戰.md "wikilink")）由[蘇聯](../Page/蘇聯.md "wikilink")[最高蘇維埃主席團決定設立](../Page/蘇聯最高蘇維埃.md "wikilink")。在[胜利勋章诞生之前](../Page/胜利勋章.md "wikilink")，该勋章中的第一级是等级最高的苏联軍功勋章。
\[1\] \[2\]

## 頒授情況

一級蘇沃洛夫勳章頒授了391枚，其中3枚屬部隊或兵團所有；二級蘇沃洛夫勳章頒授了2862枚，其中676枚屬部隊或兵團所有；三級蘇沃洛夫勳章頒授了4012枚，其中849枚頒授予部隊或兵團。

[蘇聯元帥](../Page/蘇聯元帥.md "wikilink")[格奧爾基·康斯坦丁諾維奇·朱可夫為首位一級蘇沃洛夫勳章的獲得者](../Page/格奧爾基·康斯坦丁諾維奇·朱可夫.md "wikilink")

## 頒授條件

此勳章授予「指揮部隊，組織作戰行動並表現果斷、不懈而取得卓越成就，為祖國取得勝利的紅軍指揮員」，也可授予作戰部隊和兵團。

一級蘇沃洛夫勳章授予[方面軍](../Page/方面軍.md "wikilink")、[集團軍司令](../Page/集團軍.md "wikilink")、副司令、參謀長、作戰部長、作戰局長和兵種([炮兵](../Page/炮兵.md "wikilink")、[航空兵](../Page/航空兵.md "wikilink")、[裝甲兵](../Page/裝甲兵.md "wikilink")、[火箭炮兵](../Page/火箭炮.md "wikilink"))主任；

二級蘇沃洛夫勳章授予[軍長](../Page/軍長.md "wikilink")、[師長](../Page/師長.md "wikilink")、[旅長及副職](../Page/旅長.md "wikilink")、參謀長；

三級蘇沃洛夫勳章授予[團長](../Page/團長.md "wikilink")、[營長及團參謀長](../Page/營長.md "wikilink")。

## 勳章樣式

整章呈五角星形，中央圓框內為蘇沃洛夫頭像，上半部由俄文的[亞歷山大·蘇沃洛夫](../Page/亞歷山大·蘇沃洛夫.md "wikilink")（）圍繞著；下半部則為月桂枝和及橡葉圖案，外圈是一道道五角劍芒。

一級蘇沃洛夫勳章對角距離為56毫米，章體分為兩片：金質頭像與圓盤、铂質劍芒。兩片由前後铆接固定，劍芒12點方向還另外铆接著一枚紅色琺瑯五角星。

二級蘇沃洛夫勳章對角距離為49毫米，章體分為兩片：銀質頭像與圓盤、金質劍芒。兩片由前後铆接固定。

三級蘇沃洛夫勳章對角距離為49毫米，章體全為銀質。1944年前仍分兩片，前後铆接；1944年後簡化為一整片。

| **一級**                                                                                                                                       | **二級**                                                                                                                                                          | **三級**                                                                                                                                                          |
| -------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Order_of_Suvorov_1st_class.jpg](https://zh.wikipedia.org/wiki/File:Order_of_Suvorov_1st_class.jpg "fig:Order_of_Suvorov_1st_class.jpg") | [order_of_suvorov_medal_2nd_class.jpg](https://zh.wikipedia.org/wiki/File:order_of_suvorov_medal_2nd_class.jpg "fig:order_of_suvorov_medal_2nd_class.jpg") | [order_of_suvorov_medal_3rd_class.jpg](https://zh.wikipedia.org/wiki/File:order_of_suvorov_medal_3rd_class.jpg "fig:order_of_suvorov_medal_3rd_class.jpg") |
| 章略                                                                                                                                           |                                                                                                                                                                 |                                                                                                                                                                 |
| [<File:Order_suvorov1_rib.png>](https://zh.wikipedia.org/wiki/File:Order_suvorov1_rib.png "fig:File:Order_suvorov1_rib.png")                 | [<File:Order_suvorov2_rib.png>](https://zh.wikipedia.org/wiki/File:Order_suvorov2_rib.png "fig:File:Order_suvorov2_rib.png")                                    | [<File:Order_suvorov3_rib.png>](https://zh.wikipedia.org/wiki/File:Order_suvorov3_rib.png "fig:File:Order_suvorov3_rib.png")                                    |
|                                                                                                                                              |                                                                                                                                                                 |                                                                                                                                                                 |

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [Описание и история
    ордена](https://web.archive.org/web/20070929102543/http://www.rustrana.ru/article.php?nid=263&sq=19%2C27%2C68%2C396&crypt=)

  - [Изображения знаков ордена трёх
    степеней](https://web.archive.org/web/20101109032006/http://award.adm.gov.ru/orden/orden_8.htm)

  - [Полный список кавалеров ордена Суворова I
    степени](https://web.archive.org/web/20071109005330/http://www.rusawards.ru/ussr/ordena/005%20suvorov/kavalery.htm)

  - [二战苏军勋章之苏沃洛夫勋章](http://mil.sohu.com/20150428/n412069628.shtml)

[Category:蘇聯勳章](../Category/蘇聯勳章.md "wikilink")
[蘇](../Category/俄羅斯軍事獎項及獎章.md "wikilink")
[Category:1942年建立的獎項](../Category/1942年建立的獎項.md "wikilink")
[Category:以人名命名的奖项](../Category/以人名命名的奖项.md "wikilink")

1.
2.  [苏沃洛夫勋章](http://www.unitedcn.com/05JSZL/08xunzhang/new_page_2006.htm)