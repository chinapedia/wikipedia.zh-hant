**Galeon**是[GNOME桌面系统下的](../Page/GNOME.md "wikilink")[网页浏览器](../Page/网页浏览器.md "wikilink")，采用[Mozilla的](../Page/Mozilla.md "wikilink")[Gecko排版引擎](../Page/Gecko.md "wikilink")，按照[GPL进行软件发布](../Page/GPL.md "wikilink")。

## 特点

  - 界面清爽，遵循[W3C的网络标准](../Page/W3C.md "wikilink")
  - 纯粹的网页浏览器，用Galeon开发员的口号来说是：“the web, only the web”
  - 配置灵活，功能强大，用户只通过下拉菜单，就可基本控制浏览内容

## Galeon简史

  - 2000年6月，Marco Pesenti Gritti发布Galeon 0.6
  - 2001年11月，Galeon 1.0先于Mozilla 1.0而诞生
  - 2002年3月，Galeon 1.2释出，增加了许多新鲜功能
  - 2002年10月，用[GTK](../Page/GTK.md "wikilink")2编写的Galeon
    1.3（有的称其为Galeon2）发布
  - 2002年11月，Macro离开Galeon开发团队，另起炉灶，开始[Epiphany浏览器的开发](../Page/Epiphany.md "wikilink")

## Galeon与Epiphany之异同

  - 两者都是运行在Gnome环境下
  - Epiphany转向WebKit后，Galeon继续坚持基于Mozilla的开发
  - Galeon强调配置功能丰富，用户对网页的控制权力大
  - Epiphany强调简洁，让用户一目了然

## 参见

  - [网页浏览器列表](../Page/网页浏览器列表.md "wikilink")
  - [网页浏览器比较](../Page/网页浏览器比较.md "wikilink")

## 外部链接

  - [Galeon Home](http://galeon.sourceforge.net/)
  - [Galeon History](http://galeon.sourceforge.net/Main/GaleonHistory)
  - [Free software and good user
    interfaces](https://web.archive.org/web/20050407084859/http://www106.pair.com/rhp/free-software-ui.html)

[Category:2000年軟體](../Category/2000年軟體.md "wikilink")
[Category:自由網頁瀏覽器](../Category/自由網頁瀏覽器.md "wikilink")
[Category:Gecko衍生軟體](../Category/Gecko衍生軟體.md "wikilink")
[Category:GNOME應用程式](../Category/GNOME應用程式.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")