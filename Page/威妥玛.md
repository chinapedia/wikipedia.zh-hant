**威妥瑪**（；[威妥玛拼音](../Page/威妥玛拼音.md "wikilink")：Wei<sup>1</sup>
Tʻo<sup>3</sup>-ma<sup>3</sup>）；），全名**湯瑪斯·法蘭西斯·韋德**，是19世紀的[英國外交家與](../Page/英國.md "wikilink")[汉学家](../Page/汉学.md "wikilink")，曾在[中国生活四十余年](../Page/中国.md "wikilink")。1869年至1882年任英國駐華全權公使。1888年，任[劍橋大學首位漢學教授](../Page/劍橋大學.md "wikilink")。曾荣获[聖米迦勒及聖喬治勳章及](../Page/聖米迦勒及聖喬治勳章.md "wikilink")[巴斯勳章](../Page/巴斯勳章.md "wikilink")。

威妥瑪与[翟理斯創造了一套用於拼寫](../Page/翟理斯.md "wikilink")[中文](../Page/中文.md "wikilink")[普通話的](../Page/普通話.md "wikilink")[羅馬拼音系統](../Page/羅馬拼音.md "wikilink")，即[威妥瑪拼音](../Page/威妥瑪拼音.md "wikilink")。

## 生平

威妥瑪出生在[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，其父親韋德少校（Major
Wade）是[黑衛士兵團成員](../Page/黑衛士兵團.md "wikilink")，其母親Anne
Smythe是來自[愛爾蘭](../Page/愛爾蘭.md "wikilink")[韋斯特米斯郡的](../Page/韋斯特米斯郡.md "wikilink")[新移民](../Page/新移民.md "wikilink")。他在[哈羅公學和](../Page/哈羅公學.md "wikilink")[劍橋三一學院受過教育](../Page/劍橋三一學院.md "wikilink")。他曾在[愛奧尼亞群島的團隊中服役](../Page/愛奧尼亞群島.md "wikilink")，將休閒放在對[意大利語和現代](../Page/意大利語.md "wikilink")[希臘語的學習中](../Page/希臘語.md "wikilink")。

威妥玛自1842年[鎮江之戰跟隨英軍到](../Page/鎮江之戰_\(1842年\).md "wikilink")[中國後](../Page/中國.md "wikilink")，留居中國長達43年之久。

威妥瑪在華期間根據[漢語的特點](../Page/漢語.md "wikilink")，製作一種基於[拉丁字母的漢語](../Page/拉丁字母.md "wikilink")[拼讀方法](../Page/标音.md "wikilink")，日後被稱為[威妥瑪拼音](../Page/威妥瑪拼音.md "wikilink")。

威妥瑪於1845年任[香港最高法院](../Page/香港最高法院.md "wikilink")[廣東話翻譯](../Page/廣東話.md "wikilink")。

1852年年任上海副領事。

1854年[太平天國期間](../Page/太平天國.md "wikilink")，清政府未能有效管理[江海關](../Page/江海關.md "wikilink")（上海海關）。英、美、法聯合接管江海關管理權後，威妥瑪任江海關的稅務司（海關首長）。

1866年2月，時任英國駐華公使館參贊的威妥瑪拜託公使[阿禮國上呈](../Page/阿禮國.md "wikilink")[總理衙門](../Page/總理衙門.md "wikilink")《新議略論》，希望清政府改革弊制，實行新政。否則完全可能受到各強富之國的干預制裁。

1869年至1882年任英國駐華全權公使。

1874年介入調停[牡丹社事件](../Page/牡丹社事件.md "wikilink")。

1875年（[同治十四年](../Page/同治.md "wikilink")）2月21日，由於英國人[马嘉理在](../Page/马嘉理事件.md "wikilink")[雲南被當地人殺死](../Page/雲南.md "wikilink")，演變成[外交事件](../Page/马嘉理事件.md "wikilink")。在[海關總稅務司](../Page/中國海關總稅務司.md "wikilink")[赫德的斡旋下](../Page/赫德.md "wikilink")，英国公使威妥玛與北洋大臣[李鸿章](../Page/李鸿章.md "wikilink")，於1876年8月21日在[山东](../Page/山东.md "wikilink")[芝罘](../Page/芝罘.md "wikilink")（即今[煙臺](../Page/煙臺.md "wikilink")）谈判，同年9月13日，双方簽訂《[中英煙台條約](../Page/中英煙台條約.md "wikilink")》。英國則取得從北京進入、經過四川、雲南、西藏等地到進入印度的特權。

1883年，威妥瑪返回英國，三年後將他的4,304冊中文藏書捐贈予[劍橋大學](../Page/劍橋大學.md "wikilink")。

1888年，任劍橋大學首任[漢學教授](../Page/漢學.md "wikilink")。

## 家庭

威妥瑪的妻子是知名天文學家[約翰·赫歇爾的女兒愛蜜莉亞](../Page/約翰·赫歇爾.md "wikilink")·赫歇爾（Amelia
Herschel, 1841-1926）。

## 参见

  - [威妥玛拼音](../Page/威妥玛拼音.md "wikilink")

## 參考文獻

  - 清史稿/卷154，志一百二十九，邦交二，（英吉利），[趙爾巽](../Page/趙爾巽.md "wikilink")
  - 清史稿/卷159，志一百三十四，邦交七，（日斯巴尼亞），[趙爾巽](../Page/趙爾巽.md "wikilink")

[Category:英国驻清朝公使](../Category/英国驻清朝公使.md "wikilink")
[Category:英國漢學家](../Category/英國漢學家.md "wikilink")
[Category:文字發明者](../Category/文字發明者.md "wikilink")
[Category:劍橋大學三一學院校友](../Category/劍橋大學三一學院校友.md "wikilink")
[Category:哈罗公学校友](../Category/哈罗公学校友.md "wikilink")
[Category:倫敦人](../Category/倫敦人.md "wikilink")