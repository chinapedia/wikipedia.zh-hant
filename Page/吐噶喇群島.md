**吐噶喇群島**是[琉球群島北部的一個](../Page/琉球群島.md "wikilink")[群島](../Page/群島.md "wikilink")，在行政區劃上，全境設立[日本](../Page/日本.md "wikilink")[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[十島村](../Page/十島村.md "wikilink")。

由於「噶」字在日語中甚少使用，直到[JIS X
0208中才被收錄](../Page/JIS_X_0208.md "wikilink")，因此「吐噶喇」常常被用片假名表記作「**トカラ**」，或用「喝」字取代，表記作「**吐喝喇**」。

吐噶喇群島過去曾有**七島**、**川邊七島**、**寶七島**等稱呼。在日本的[天氣預報中](../Page/天氣預報.md "wikilink")，與[奄美群島同歸入奄美地方進行播報](../Page/奄美群島.md "wikilink")。

## 主要島嶼

  - [口之島](../Page/口之島.md "wikilink")
  - [中之島](../Page/中之島_\(鹿兒島縣\).md "wikilink")
  - [臥蛇島](../Page/臥蛇島.md "wikilink")（[無人島](../Page/無人島.md "wikilink")）
  - [小臥蛇島](../Page/小臥蛇島.md "wikilink")（無人島）
  - [平島](../Page/平島.md "wikilink")
  - [諏訪之瀨島](../Page/諏訪之瀨島.md "wikilink")
  - [惡石島](../Page/惡石島.md "wikilink")
  - [小島](../Page/小島_\(鹿兒島縣\).md "wikilink")（無人島）
  - [小寶島](../Page/小寶島.md "wikilink")
  - [寶島](../Page/寶島_\(鹿兒島縣\).md "wikilink")
  - [上之根島](../Page/上之根島.md "wikilink")（無人島）
  - [橫當島](../Page/橫當島.md "wikilink")（無人島）

[Category:吐噶喇群島](../Category/吐噶喇群島.md "wikilink")
[北](../Category/琉球群岛.md "wikilink")
[Category:鹿兒島縣地理](../Category/鹿兒島縣地理.md "wikilink")