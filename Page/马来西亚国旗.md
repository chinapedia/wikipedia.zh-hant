**马来西亚国旗**又称**辉煌条纹**（），是[马来西亚的](../Page/马来西亚.md "wikilink")[国家](../Page/国家.md "wikilink")[主权象征之一](../Page/主权.md "wikilink")。

国旗由十四道红白相间的横条所组成，左上角为蓝底加上黄色的新月及十四芒星图案。长宽比例为2：1。

这道旗帜由[莫哈末韩查设计](../Page/莫哈末韩查.md "wikilink")，自1963年9月16日马来西亚成立时正式启用。

## 象征意义

14道红白相间的等宽横条原代表马来西亚成立时全国的14个州，自[新加坡在](../Page/新加坡.md "wikilink")1965年独立后，又代表13个州属及联邦政府的平等关系。

[红色象征人民面对挑战时勇敢坚毅的精神](../Page/红色.md "wikilink")。[白色象征着人民的纯洁与高尚](../Page/白色.md "wikilink")。[蓝色象征人民的团结](../Page/蓝色.md "wikilink")、融洽、和睦地生活在一起。新月图案象征伊斯兰教是马来西亚的联邦宗教。十四角星又名“联邦之星”，象征着马来西亚十三个州属与联邦政府联合、团结一致。[黄色象征着皇室的颜色](../Page/黄色.md "wikilink")。

## 命名

1997年马来西亚独立四十周年之际，马来西亚第四任首相[马哈迪·莫哈末宣布将国旗命名为](../Page/马哈迪·莫哈末.md "wikilink")“辉煌条纹”。

## 国旗格式

[Construction_sheet_of_Flag_of_Malaysia.svg](https://zh.wikipedia.org/wiki/File:Construction_sheet_of_Flag_of_Malaysia.svg "fig:Construction_sheet_of_Flag_of_Malaysia.svg")
[Petronas_Towers_at_Night_-_from_the_base_upwards.jpg](https://zh.wikipedia.org/wiki/File:Petronas_Towers_at_Night_-_from_the_base_upwards.jpg "fig:Petronas_Towers_at_Night_-_from_the_base_upwards.jpg")所悬挂的马来西亚国旗变体\]\]

## 国旗建议草案

## 其它主权旗帜

## 历代国旗

### 馬來西亞國旗時間軸

<table>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Federated_Malay_States_(1895_-_1946).svg" title="fig:Flag_of_the_Federated_Malay_States_(1895_-_1946).svg">Flag_of_the_Federated_Malay_States_(1895_-_1946).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Federated_Malay_States_(1895_-_1946).svg" title="fig:Flag_of_the_Federated_Malay_States_(1895_-_1946).svg">Flag_of_the_Federated_Malay_States_(1895_-_1946).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Federated_Malay_States_(1895_-_1946).svg" title="fig:Flag_of_the_Federated_Malay_States_(1895_-_1946).svg">Flag_of_the_Federated_Malay_States_(1895_-_1946).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Malaya.svg" title="fig:Flag_of_Malaya.svg">Flag_of_Malaya.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Malaya.svg" title="fig:Flag_of_Malaya.svg">Flag_of_Malaya.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Malaysia.svg" title="fig:Flag_of_Malaysia.svg">Flag_of_Malaysia.svg</a></p></td>
</tr>
<tr class="even">
<td><p>1896–1946<br />
<a href="../Page/馬來聯邦.md" title="wikilink">馬來聯邦</a></p></td>
<td><p>1946–1948<br />
<a href="../Page/馬來亚聯邦.md" title="wikilink">馬來亚聯邦</a></p></td>
<td><p>1948–1950<br />
<a href="../Page/馬來亞聯合邦.md" title="wikilink">馬來亞聯合邦</a></p></td>
<td><p>1950–1957<br />
<a href="../Page/馬來亞聯合邦.md" title="wikilink">馬來亞聯合邦</a></p></td>
<td><p>1957–1963<br />
<a href="../Page/馬來亞聯合邦.md" title="wikilink">馬來亞聯合邦</a></p></td>
<td><p>1963-至今<br />
<a href="../Page/馬來西亞.md" title="wikilink">馬來西亞</a></p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - 马来西亚新闻局人民资料中心：
      - [马来西亚国旗](http://pmr.penerangan.gov.my/index.php?option=com_content&view=article&id=194%3Abendera-malaysia&catid=13%3Amaklumat-kenegaraan&Itemid=20)
      - [我们的旗帜](http://pmr.penerangan.gov.my/index.php?option=com_content&view=article&id=197%3Abendera-kita-&catid=13%3Amaklumat-kenegaraan&Itemid=20)
      - [州旗](http://pmr.penerangan.gov.my/index.php?option=com_content&view=article&id=195%3Abendera-negeri&catid=13%3Amaklumat-kenegaraan&Itemid=20)
  - [世界旗帜网上的](../Page/世界旗帜网.md "wikilink")[马来西亚旗帜](http://flagspot.net/flags/my.html)

## 参见

  - [马来西亚旗帜列表](../Page/马来西亚旗帜列表.md "wikilink")

{{-}}

[M](../Category/国旗.md "wikilink")
[Category:马来西亚国家象征](../Category/马来西亚国家象征.md "wikilink")
[Category:亞洲國旗](../Category/亞洲國旗.md "wikilink")