[Sampleorf.png](https://zh.wikipedia.org/wiki/File:Sampleorf.png "fig:Sampleorf.png")以紫色突出显示，而[终止密码子以红色突出显示](../Page/终止密码子.md "wikilink")。\]\]

**開放閱讀框**（英語：**Open reading
frame**；縮寫：**ORF**；其他譯名：**開放閱讀框架**、**開放讀架**等）是指在給定的[閱讀框架中](../Page/閱讀框架.md "wikilink")，不包含终止密碼子的一串序列。這段序列是生物個體的[基因組中](../Page/基因組.md "wikilink")，可能作為[蛋白質編碼序列的部分](../Page/蛋白質.md "wikilink")。[基因中的ORF包含並位於開始](../Page/基因.md "wikilink")[編碼與終止編碼之間](../Page/密碼子.md "wikilink")。由於一段[DNA或](../Page/DNA.md "wikilink")[RNA序列有多種不同讀取方式](../Page/RNA.md "wikilink")，因此可能同時存在許多不同的開放閱讀框架。有一些[電腦程式可分析出最有可能编码蛋白质的序列](../Page/電腦程式.md "wikilink")。

## 例子

一段5'-UC**UAA**AGGUCCA-3'序列。此序列共有3種讀取法：

1.  UCU AAA GGU CCA
2.  CUA AAG GUC
3.  **UAA** AGG UCC

由於UAA為終止密码子，因此第三種讀取法不具編譯出蛋白質的潛力，故只有前兩者為開放閱讀框架。

## 找ORF的软件

在线寻找ORF的软件：[ORF Finder](http://www.ncbi.nlm.nih.gov/orffinder/)

### 功能

ORF Finder 被用来预测已存在的编码区的小基因序列。也可用于大规模的开放式阅读框寻找。

## 參見

  - [编码区](../Page/编码区.md "wikilink")
  - [閱讀框架](../Page/閱讀框架.md "wikilink")

## 外部連結

  - [Translation and Open Reading
    Frames](http://bioweb.uwlax.edu/GenWeb/Molecular/Seq_Anal/Translation/translation.html)
    - A Site explaining Open Reading Frames
  - [NCBI ORF finder](http://www.ncbi.nlm.nih.gov/projects/gorf/) - A
    web based interactive tool for predicting and analysing ORFs from
    nucleotide sequences.
  - [ORF finder](http://bioinformatics.org/sms/orf_find.html) - A web
    based interactive tool for predicting and analysing ORFs from
    nucleotide sequences - hosted at bioinformatics.org
  - [Sequerome](../Page/Sequerome.md "wikilink") - A [Sequence profiling
    tool](../Page/Sequence_profiling_tool.md "wikilink") that links each
    [BLAST](../Page/BLAST.md "wikilink") record to the NCBI ORF enabling
    complete ORF analysis of a BLAST report
  - [StarORF](http://web.mit.edu/star/orf) - 多平台，
    Java根据，为预言和分析ORFs和得到反向补全序列的GUI工具

[Category:分子遺傳學](../Category/分子遺傳學.md "wikilink")