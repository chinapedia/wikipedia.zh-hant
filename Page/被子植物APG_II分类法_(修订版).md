[FlowerPlantsSystemTreeZH.PNG](https://zh.wikipedia.org/wiki/File:FlowerPlantsSystemTreeZH.PNG "fig:FlowerPlantsSystemTreeZH.PNG")
**《被子植物APG
II分类法（修订版）》**是由[被子植物种系发生学组](../Page/被子植物种系发生学组.md "wikilink")（APG）于2003年發表的，基于1998年所發表的《[被子植物APG分类法](../Page/被子植物APG分类法.md "wikilink")》之修訂版，在吸收了全世界大部分植物分类学家的意见，2009年發表《[被子植物APG
III分类法](../Page/被子植物APG_III分类法.md "wikilink")》。目前已经在现代的参考文献中占很重要的地位。

## 基本分支

主要的大[分類群之建立](../Page/分類群.md "wikilink")，由於未有確定的證據，[目以上分類群多採用一般性的名稱](../Page/目.md "wikilink")
(非[學名](../Page/學名.md "wikilink"))作為暫時性的分類。基本分支如下：

  - **[被子植物](../Page/被子植物.md "wikilink")** **angiosperms :**
      -
        '''[木兰分支](../Page/木兰分支.md "wikilink") ''' **magnoliids**
        **[单子叶植物分支](../Page/单子叶植物分支.md "wikilink")** **monocots**
          -
            **[鸭跖草分支](../Page/鸭跖草分支.md "wikilink")** **commelinids**
        **[真双子叶植物分支](../Page/真双子叶植物分支.md "wikilink")** **eudicots**
          -
            **[核心真双子叶植物分支](../Page/核心真双子叶植物分支.md "wikilink")** **core
            eudicots**
              -
                **[蔷薇分支](../Page/蔷薇分支.md "wikilink")** **rosids**
                  -
                    **[I类真蔷薇分支](../Page/I类真蔷薇分支.md "wikilink")**
                    **eurosids I**
                    **[II类真蔷薇分支](../Page/II类真蔷薇分支.md "wikilink")**
                    **eurosids II**
                ''' [菊分支](../Page/菊分支.md "wikilink") ''' **asterids**
                  -
                    *' [I类真菊分支](../Page/I类真菊分支.md "wikilink")*'
                    **euasterids I**
                    **[II类真菊分支](../Page/II类真菊分支.md "wikilink")**
                    **euasterids II**

-----

## 分支

  - **[被子植物分支](../Page/被子植物分支.md "wikilink")** **angiosperms**
      -

          -
            [无油樟科](../Page/无油樟科.md "wikilink") *Amborellaceae*
            [金粟兰科](../Page/金粟兰科.md "wikilink") *Chloranthaceae*
            [睡莲科](../Page/睡莲科.md "wikilink") *Nymphaeaceae* +
            [莼菜科](../Page/莼菜科.md "wikilink") *Cabombaceae*

        <!-- end list -->

          -
            [木兰藤目](../Page/木兰藤目.md "wikilink") *Austrobaileyales*
              -
                [木兰藤科](../Page/木兰藤科.md "wikilink") *Austrobaileyaceae*
                [五味子科](../Page/五味子科.md "wikilink") *Schisandraceae* +
                [八角茴香科](../Page/八角茴香科.md "wikilink") *Illiciaceae*
                [腺齿木科](../Page/腺齿木科.md "wikilink") *Trimeniaceae*

        ''' [木兰分支](../Page/木兰分支.md "wikilink") ''' **magnoliids**

          -

              -
                [白桂皮目](../Page/白桂皮目.md "wikilink") *Canellales*
                  -
                    [白桂皮科](../Page/白桂皮科.md "wikilink") *Canellaceae*
                    [林仙科](../Page/林仙科.md "wikilink") *Winteraceae*
                [樟目](../Page/樟目.md "wikilink") *Laurales*
                  -
                    [香皮茶科](../Page/香皮茶科.md "wikilink")
                    *Atherospermataceae*
                    [腊梅科](../Page/腊梅科.md "wikilink") *Calycanthaceae*
                    [腺蕊花科](../Page/腺蕊花科.md "wikilink") *Gomortegaceae*
                    [莲叶桐科](../Page/莲叶桐科.md "wikilink") *Hernandiaceae*
                    [樟科](../Page/樟科.md "wikilink") *Lauraceae*
                    [杯轴花科](../Page/杯轴花科.md "wikilink") *Monimiaceae*
                    [坛罐花科](../Page/坛罐花科.md "wikilink") *Siparunaceae*
                [木兰目](../Page/木兰目.md "wikilink") *Magnoliales*
                  -
                    [番荔枝科](../Page/番荔枝科.md "wikilink") *Annonaceae*
                    [单室木兰科](../Page/单室木兰科.md "wikilink") *Degeneriaceae*
                    [帽花木科](../Page/帽花木科.md "wikilink") *Eupomatiaceae*
                    [舌蕊花科](../Page/舌蕊花科.md "wikilink") ''
                    Himantandraceae''
                    [木兰科](../Page/木兰科.md "wikilink") '' Magnoliaceae''
                    [肉豆蔻科](../Page/肉豆蔻科.md "wikilink") ''
                    Myristicaceae''
                [胡椒目](../Page/胡椒目.md "wikilink") *Piperales*
                  -
                    [馬兜鈴科](../Page/馬兜鈴科.md "wikilink")
                    *Aristolochiaceae*
                    [菌花科](../Page/菌花科.md "wikilink") *Hydnoraceae*
                    [短蕊花科](../Page/短蕊花科.md "wikilink") *Lactoridaceae*
                    [胡椒科](../Page/胡椒科.md "wikilink") *Piperaceae*
                    [三白草科](../Page/三白草科.md "wikilink") *Saururaceae*

        ''' [单子叶植物分支](../Page/单子叶植物分支.md "wikilink") ''' **monocots**

          -

              -
                [无叶莲科](../Page/无叶莲科.md "wikilink") *Petrosaviaceae*

            <!-- end list -->

              -
                [菖蒲目](../Page/菖蒲目.md "wikilink") *Acorales*
                  -
                    [菖蒲科](../Page/菖蒲科.md "wikilink") *Acoraceae*
                [泽泻目](../Page/泽泻目.md "wikilink") *Alismatales*
                  -
                    [泽泻科](../Page/泽泻科.md "wikilink") '' Alismataceae''
                    [水蕹科](../Page/水蕹科.md "wikilink") *Aponogetonaceae*
                    [天南星科](../Page/天南星科.md "wikilink") *Araceae*
                    [花蔺科](../Page/花蔺科.md "wikilink") *Butomaceae*
                    [丝粉藻科](../Page/丝粉藻科.md "wikilink") *Cymodoceaceae*
                    [水鳖科](../Page/水鳖科.md "wikilink") *Hydrocharitaceae*
                    [水麦冬科](../Page/水麦冬科.md "wikilink") *Juncaginaceae*
                    [黄花绒叶草科](../Page/黄花绒叶草科.md "wikilink")
                    *Limnocharitaceae*
                    [波喜荡草科](../Page/波喜荡草科.md "wikilink") *Posidoniaceae*
                    [眼子菜科](../Page/眼子菜科.md "wikilink")
                    *Potamogetonaceae*
                    [川蔓藻科](../Page/川蔓藻科.md "wikilink") *Ruppiaceae*
                    [芝菜科](../Page/芝菜科.md "wikilink") *Scheuchzeriaceae*
                    [岩菖蒲科](../Page/岩菖蒲科.md "wikilink") *Tofieldiaceae*
                    [大叶藻科](../Page/大叶藻科.md "wikilink") *Zosteraceae*
                [天門冬目](../Page/天門冬目.md "wikilink") *Asparagales*
                  -
                    [葱科](../Page/葱科.md "wikilink")*Alliaceae*

            \+ [百子莲科](../Page/百子莲科.md "wikilink")*Agapanthaceae* +
            [石蒜科](../Page/石蒜科.md "wikilink") *Amaryllidaceae*

              -

                  -
                    [天门冬科](../Page/天门冬科.md "wikilink") *Asparagaceae*

            \+ [龙舌兰科](../Page/龙舌兰科.md "wikilink")*Agavaceae* +
            [星捧月科](../Page/星捧月科.md "wikilink")*Aphyllanthaceae*
            + [西丽草科](../Page/西丽草科.md "wikilink")*Hesperocallidaceae* +
            [风信子科](../Page/风信子科.md "wikilink")*Hyacinthaceae* +
            [异蕊草科](../Page/异蕊草科.md "wikilink")*Laxmanniaceae* +
            [假叶树科](../Page/假叶树科.md "wikilink")*Ruscaceae* +
            [紫灯花科](../Page/紫灯花科.md "wikilink")*Themidaceae*

              -

                  -
                    [芳香草科](../Page/芳香草科.md "wikilink") *Asteliaceae*
                    [香水花科](../Page/香水花科.md "wikilink") *Blandfordiaceae*
                    [澳韭兰科](../Page/澳韭兰科.md "wikilink") *Boryaceae*
                    [矛花科](../Page/矛花科.md "wikilink") *Doryanthaceae*
                    [仙茅科](../Page/仙茅科.md "wikilink") *Hypoxidaceae*
                    [鸢尾科](../Page/鸢尾科.md "wikilink") *Iridaceae*
                    [鸢尾蒜科](../Page/鸢尾蒜科.md "wikilink") *Ixioliriaceae*
                    [毛石蒜科](../Page/毛石蒜科.md "wikilink") *Lanariaceae*
                    [兰科](../Page/兰科.md "wikilink") *Orchidaceae*
                    [蓝星科](../Page/蓝星科.md "wikilink") *Tecophilaeaceae*
                    [刺叶树科](../Page/刺叶树科.md "wikilink")*Xanthorrhoeaceae*

            \+ [独尾草科](../Page/独尾草科.md "wikilink")*Asphodelaceae* +
            [萱草科](../Page/萱草科.md "wikilink")*Hemerocallidaceae*

              -

                  -
                    [血剑草科](../Page/血剑草科.md "wikilink") *Xeronemataceae*

                [薯蕷目](../Page/薯蕷目.md "wikilink") *Dioscoreales*

                  -
                    [水玉簪科](../Page/水玉簪科.md "wikilink") *Burmanniaceae*
                    [薯蕷科](../Page/薯蕷科.md "wikilink") *Dioscoreaceae*
                    [纳茜菜科](../Page/纳茜菜科.md "wikilink") *Nartheciaceae*

                [百合目](../Page/百合目.md "wikilink") *Liliales*

                  -
                    [六出花科](../Page/六出花科.md "wikilink")
                    *Alstroemeriaceae*
                    [金梅草科](../Page/金梅草科.md "wikilink") *Campynemataceae*
                    [秋水仙科](../Page/秋水仙科.md "wikilink") *Colchicaceae*
                    [白玉簪科](../Page/白玉簪科.md "wikilink") *Corsiaceae*
                    [百合科](../Page/百合科.md "wikilink") *Liliaceae*
                    [菝葜木科](../Page/菝葜木科.md "wikilink") *Luzuriagaceae*
                    [黑药花科](../Page/黑药花科.md "wikilink") *Melanthiaceae*
                    [垂花科](../Page/垂花科.md "wikilink") *Philesiaceae*
                    [菝葜藤科](../Page/菝葜藤科.md "wikilink") *Rhipogonaceae*
                    [菝葜科](../Page/菝葜科.md "wikilink") *Smilacaceae*

                [露兜树目](../Page/露兜树目.md "wikilink") *Pandanales*

                  -
                    [环花草科](../Page/环花草科.md "wikilink") *Cyclanthaceae*
                    [露兜树科](../Page/露兜树科.md "wikilink") *Pandanaceae*
                    [百部科](../Page/百部科.md "wikilink") *Stemonaceae*
                    [霉草科](../Page/霉草科.md "wikilink") *Triuridaceae*
                    [翡若翠科](../Page/翡若翠科.md "wikilink") *Velloziaceae*

            ''' [鸭跖草分支](../Page/鸭跖草分支.md "wikilink") ''' **commelinids**

              -

                  -
                    [多须草科](../Page/多须草科.md "wikilink") *Dasypogonaceae*

                <!-- end list -->

                  -
                    [槟榔目](../Page/槟榔目.md "wikilink") *Arecales*
                      -
                        [槟榔科](../Page/槟榔科.md "wikilink") *Arecaceae*
                    [鸭跖草目](../Page/鸭跖草目.md "wikilink") *Commelinales*
                      -
                        [鸭跖草科](../Page/鸭跖草科.md "wikilink")
                        *Commelinaceae*
                        [血皮草科](../Page/血皮草科.md "wikilink")
                        *Haemodoraceae*
                        [匍茎草科](../Page/匍茎草科.md "wikilink")
                        *Hanguanaceae*
                        [田葱科](../Page/田葱科.md "wikilink") *Philydraceae*
                        [雨久花科](../Page/雨久花科.md "wikilink")
                        *Pontederiaceae*
                    [禾本目](../Page/禾本目.md "wikilink") *Poales*
                      -
                        [苞穗草科](../Page/苞穗草科.md "wikilink")
                        *Anarthriaceae*
                        [凤梨科](../Page/凤梨科.md "wikilink") *Bromeliaceae*
                        [刺鳞草科](../Page/刺鳞草科.md "wikilink")
                        *Centrolepidaceae*
                        [莎草科](../Page/莎草科.md "wikilink") *Cyperaceae*
                        [二柱草科](../Page/二柱草科.md "wikilink")
                        *Ecdeiocoleaceae*
                        [谷精草科](../Page/谷精草科.md "wikilink")
                        *Eriocaulaceae*
                        [须叶藤科](../Page/须叶藤科.md "wikilink")
                        *Flagellariaceae*
                        [独蕊草科](../Page/独蕊草科.md "wikilink")
                        *Hydatellaceae*
                        [拟苇科](../Page/拟苇科.md "wikilink")
                        *Joinvilleaceae*
                        [灯心草科](../Page/灯心草科.md "wikilink") *Juncaceae*
                        [苔草科](../Page/苔草科.md "wikilink") *Mayacaceae*
                        [禾本科](../Page/禾本科.md "wikilink") *Poaceae*
                        [偏穗草科](../Page/偏穗草科.md "wikilink") *Rapateaceae*
                        [帚灯草科](../Page/帚灯草科.md "wikilink")
                        *Restionaceae*
                        [黑三棱科](../Page/黑三棱科.md "wikilink")
                        *Sparganiaceae*
                        [梭子草科](../Page/梭子草科.md "wikilink") *Thurniaceae*
                        [香蒲科](../Page/香蒲科.md "wikilink") *Typhaceae*
                        [黄眼草科](../Page/黄眼草科.md "wikilink") *Xyridaceae*
                    [姜目](../Page/姜目.md "wikilink") *Zingiberales*
                      -
                        [美人蕉科](../Page/美人蕉科.md "wikilink") *Cannaceae*
                        [闭鞘姜科](../Page/闭鞘姜科.md "wikilink") *Costaceae*
                        [蝎尾蕉科](../Page/蝎尾蕉科.md "wikilink")
                        *Heliconiaceae*
                        [兰花蕉科](../Page/兰花蕉科.md "wikilink") *Lowiaceae*
                        [竹芋科](../Page/竹芋科.md "wikilink") *Marantaceae*
                        [芭蕉科](../Page/芭蕉科.md "wikilink") *Musaceae*
                        [鹤望兰科](../Page/鹤望兰科.md "wikilink")
                        *Strelitziaceae*
                        [姜科](../Page/姜科.md "wikilink") *Zingiberaceae*

        [金鱼藻目](../Page/金鱼藻目.md "wikilink") *Ceratophyllales*

          -
            [金鱼藻科](../Page/金鱼藻科.md "wikilink") *Ceratophyllaceae*

        ''' [真双子叶植物分支](../Page/真双子叶植物分支.md "wikilink") ''' **eudicots**

          -

              -
                [黄杨科](../Page/黄杨科.md "wikilink") *Buxaceae* +
                [双颊果科](../Page/双颊果科.md "wikilink")
                *Didymelaceae*
                [清风藤科](../Page/清风藤科.md "wikilink") *Sabiaceae*
                [昆栏树科](../Page/昆栏树科.md "wikilink")*Trochodendraceae*

            \+ [水青树科](../Page/水青树科.md "wikilink")*Tetracentraceae*

              -
                [山龙眼目](../Page/山龙眼目.md "wikilink") *Proteales*
                  -

                      -
                        [莲科](../Page/莲科.md "wikilink") *Nelumbonaceae*
                        [悬铃木科](../Page/悬铃木科.md "wikilink") *Platanaceae*
                        [山龙眼科](../Page/山龙眼科.md "wikilink") *Proteaceae*
                [毛茛目](../Page/毛茛目.md "wikilink") *Ranunculales*
                  -

                      -
                        [小檗科](../Page/小檗科.md "wikilink") *Berberidaceae*
                        [星叶草科](../Page/星叶草科.md "wikilink")
                        *Circaeasteraceae*
                \+ [独叶草科](../Page/独叶草科.md "wikilink") *Kingdoniaceae*
                  -

                      -
                        [领春木科](../Page/领春木科.md "wikilink")
                        *Eupteleaceae*
                        [木通科](../Page/木通科.md "wikilink")
                        *Lardizabalaceae*
                        [防己科](../Page/防己科.md "wikilink")
                        *Menispermaceae*
                        [罂粟科](../Page/罂粟科.md "wikilink") *Papaveraceae*
                \+ [荷包牡丹科](../Page/荷包牡丹科.md "wikilink") *Fumariaceae*+
                [蕨叶草科](../Page/蕨叶草科.md "wikilink") *Pteridophyllaceae*
                  -

                      -
                        [毛茛科](../Page/毛茛科.md "wikilink") *Ranunculaceae*

            ''' [核心真双子叶植物分支](../Page/核心真双子叶植物分支.md "wikilink") '''
            **core eudicots**

              -

                  -
                    [鳞枝树科](../Page/鳞枝树科.md "wikilink") *Aextoxicaceae*
                    [智利藤科](../Page/智利藤科.md "wikilink")
                    *Berberidopsidaceae*
                    [五桠果科](../Page/五桠果科.md "wikilink") *Dilleniaceae*

                <!-- end list -->

                  -
                    [洋二仙草目](../Page/洋二仙草目.md "wikilink") *Gunnerales*
                      -
                        [洋二仙草科](../Page/洋二仙草科.md "wikilink")
                        *Gunneraceae*

                \+ [香灌木科](../Page/香灌木科.md "wikilink") *Myrothamnaceae*

                  -
                    [石竹目](../Page/石竹目.md "wikilink") *Caryophyllales*
                      -

                          -
                            [玛瑙果科](../Page/玛瑙果科.md "wikilink")
                            *Achatocarpaceae*
                            [番杏科](../Page/番杏科.md "wikilink") *Aizoaceae*
                            [苋科](../Page/苋科.md "wikilink")
                            *Amaranthaceae*
                            [钩枝藤科](../Page/钩枝藤科.md "wikilink")
                            *Ancistrocladaceae*
                            [翼萼茶科](../Page/翼萼茶科.md "wikilink")
                            *Asteropeiaceae*
                            [节柄科](../Page/节柄科.md "wikilink")
                            *Barbeuiaceae*
                            [落葵科](../Page/落葵科.md "wikilink")
                            *Basellaceae*
                            [仙人掌科](../Page/仙人掌科.md "wikilink")
                            *Cactaceae*
                            [石竹科](../Page/石竹科.md "wikilink")
                            *Caryophyllaceae*
                            [龙树科](../Page/龙树科.md "wikilink")
                            *Didiereaceae*
                            [双钩叶科](../Page/双钩叶科.md "wikilink")
                            *Dioncophyllaceae*
                            [茅膏菜科](../Page/茅膏菜科.md "wikilink")
                            *Droseraceae*
                            [露叶毛毡苔科](../Page/露叶毛毡苔科.md "wikilink")
                            *Drosophyllaceae*
                            [瓣鳞花科](../Page/瓣鳞花科.md "wikilink")
                            *Frankeniaceae*
                            [吉粟草科](../Page/吉粟草科.md "wikilink")
                            *Gisekiaceae*
                            [浜藜叶科](../Page/浜藜叶科.md "wikilink")
                            *Halophytaceae*
                            [粟米草科](../Page/粟米草科.md "wikilink")
                            *Molluginaceae*
                            [猪笼草科](../Page/猪笼草科.md "wikilink")
                            *Nepenthaceae*
                            [紫茉莉科](../Page/紫茉莉科.md "wikilink")
                            *Nyctaginaceae*
                            [非洲桐科](../Page/非洲桐科.md "wikilink")
                            *Physenaceae*
                            [商陆科](../Page/商陆科.md "wikilink")
                            *Phytolaccaceae*
                            [蓝雪科](../Page/蓝雪科.md "wikilink")
                            *Plumbaginaceae*
                            [蓼科](../Page/蓼科.md "wikilink")
                            *Polygonaceae*
                            [马齿苋科](../Page/马齿苋科.md "wikilink")
                            *Portulacaceae*
                            [棒木科](../Page/棒木科.md "wikilink")
                            *Rhabdodendraceae*
                            [肉叶刺茎藜科](../Page/肉叶刺茎藜科.md "wikilink")
                            *Sarcobataceae*
                            [油蜡树科](../Page/油蜡树科.md "wikilink")
                            *Simmondsiaceae*
                            [闭籽花科](../Page/闭籽花科.md "wikilink")
                            *Stegnospermataceae*
                            [柽柳科](../Page/柽柳科.md "wikilink")
                            *Tamaricaceae*
                    [檀香目](../Page/檀香目.md "wikilink") *Santalales*
                      -

                          -
                            [铁青树科](../Page/铁青树科.md "wikilink")
                            *Olacaceae*
                            [山柚子科](../Page/山柚子科.md "wikilink")
                            *Opiliaceae*
                            [桑寄生科](../Page/桑寄生科.md "wikilink")
                            *Loranthaceae*
                            [羽毛果科](../Page/羽毛果科.md "wikilink")
                            *Misodendraceae*
                            [檀香科](../Page/檀香科.md "wikilink")
                            *Santalaceae*
                    [虎耳草目](../Page/虎耳草目.md "wikilink") *Saxifragales*
                      -

                          -
                            [枫香科](../Page/枫香科.md "wikilink")
                            *Altingiaceae*
                            [胶藤科](../Page/胶藤科.md "wikilink")
                            *Aphanopetalaceae*
                            [连香树科](../Page/连香树科.md "wikilink")
                            *Cercidiphyllaceae*
                            [景天科](../Page/景天科.md "wikilink")
                            *Crassulaceae*
                            [交让木科](../Page/交让木科.md "wikilink")
                            *Daphniphyllaceae*
                            [茶藨子科](../Page/茶藨子科.md "wikilink")
                            *Grossulariaceae*
                            [小二仙草科](../Page/小二仙草科.md "wikilink")
                            *Haloragaceae*
                    \+ [扯根菜科](../Page/扯根菜科.md "wikilink") *Penthoraceae*
                    + [四果木科](../Page/四果木科.md "wikilink")
                    *Tetracarpaeaceae*
                      -

                          -
                            [金缕梅科](../Page/金缕梅科.md "wikilink")
                            *Hamamelidaceae*
                            [鼠刺科](../Page/鼠刺科.md "wikilink") *Iteaceae*
                    \+ [齿蕊科](../Page/齿蕊科.md "wikilink")
                    *Pterostemonaceae*
                      -

                          -
                            [芍药科](../Page/芍药科.md "wikilink")
                            *Paeoniaceae*
                            [虎耳草科](../Page/虎耳草科.md "wikilink")
                            *Saxifragaceae*

                ''' [蔷薇分支](../Page/蔷薇分支.md "wikilink") ''' **rosids**

                  -

                      -
                        [单果树科](../Page/单果树科.md "wikilink") *Aphloiaceae*
                        [四棱果科](../Page/四棱果科.md "wikilink")
                        *Geissolomataceae*
                        [西兰木科](../Page/西兰木科.md "wikilink") *Ixerbaceae*
                        [美洲苦木科](../Page/美洲苦木科.md "wikilink")
                        *Picramniaceae*
                        [栓皮果科](../Page/栓皮果科.md "wikilink")
                        *Strasburgeriaceae*
                        [葡萄科](../Page/葡萄科.md "wikilink") *Vitaceae*

                    <!-- end list -->

                      -
                        [燧体木目](../Page/燧体木目.md "wikilink")
                        *Crossosomatales*
                          -
                            [燧体木科](../Page/燧体木科.md "wikilink")
                            *Crossosomataceae*
                            [旌节花科](../Page/旌节花科.md "wikilink")
                            *Stachyuraceae*
                            [省沽油科](../Page/省沽油科.md "wikilink")
                            *Staphyleaceae*
                        [牻牛儿苗目](../Page/牻牛儿苗目.md "wikilink")
                        *Geraniales*
                          -
                            [牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink")
                            *Geraniaceae*

                    \+ [高柱花科](../Page/高柱花科.md "wikilink")
                    *Hypseocharitaceae*

                      -

                          -
                            [杜香果科](../Page/杜香果科.md "wikilink")
                            *Ledocarpaceae*
                            [蜜花科](../Page/蜜花科.md "wikilink")
                            *Melianthaceae*

                    \+ [花茎草科](../Page/花茎草科.md "wikilink") *Francoaceae*

                      -

                          -
                            [曲胚科](../Page/曲胚科.md "wikilink")
                            *Vivianiaceae*

                        [桃金娘目](../Page/桃金娘目.md "wikilink") *Myrtales*

                          -
                            [双翼果科](../Page/双翼果科.md "wikilink")
                            *Alzateaceae*
                            [使君子科](../Page/使君子科.md "wikilink")
                            *Combretaceae*
                            [隐翼科](../Page/隐翼科.md "wikilink")
                            *Crypteroniaceae*
                            [异裂果科](../Page/异裂果科.md "wikilink")
                            *Heteropyxidaceae*
                            [千屈菜科](../Page/千屈菜科.md "wikilink")
                            *Lythraceae*
                            [野牡丹科](../Page/野牡丹科.md "wikilink")
                            *Melastomataceae*

                    \+ [谷木科](../Page/谷木科.md "wikilink") *Memecylaceae*

                      -

                          -
                            [桃金娘科](../Page/桃金娘科.md "wikilink")
                            *Myrtaceae*
                            [方枝树科](../Page/方枝树科.md "wikilink")
                            *Oliniaceae*
                            [柳叶菜科](../Page/柳叶菜科.md "wikilink")
                            *Onagraceae*
                            [管萼科](../Page/管萼科.md "wikilink")
                            *Penaeaceae*
                            [裸木科](../Page/裸木科.md "wikilink")
                            *Psiloxylaceae*
                            [喙萼花科](../Page/喙萼花科.md "wikilink")
                            *Rhynchocalycaceae*
                            [蜡烛树科](../Page/蜡烛树科.md "wikilink")
                            *Vochysiaceae*

                    **[I类真蔷薇分支](../Page/I类真蔷薇分支.md "wikilink")**
                    **eurosids I**

                      -

                          -
                            [蒺藜科](../Page/蒺藜科.md "wikilink")*Zygophyllaceae*

                    \+[刺球果科](../Page/刺球果科.md "wikilink")*Krameriaceae*

                      -

                          -
                            [蒜树科](../Page/蒜树科.md "wikilink") *Huaceae*

                        <!-- end list -->

                          -
                            [卫矛目](../Page/卫矛目.md "wikilink")
                            *Celastrales*
                              -
                                [卫矛科](../Page/卫矛科.md "wikilink")
                                *Celastraceae*
                                [洋酢浆草科](../Page/洋酢浆草科.md "wikilink")
                                *Lepidobotryaceae*
                                [梅花草科](../Page/梅花草科.md "wikilink")
                                *Parnassiaceae*

                    \+
                    [微形草科](../Page/微形草科.md "wikilink")*Lepuropetalaceae*

                      -

                          -
                            [葫芦目](../Page/葫芦目.md "wikilink")
                            *Cucurbitales*
                              -
                                [四柱木科](../Page/四柱木科.md "wikilink")
                                *Anisophylleaceae*
                                [秋海棠科](../Page/秋海棠科.md "wikilink")
                                *Begoniaceae*
                                [马桑科](../Page/马桑科.md "wikilink")
                                *Coriariaceae*
                                [棒果木科](../Page/棒果木科.md "wikilink")
                                *Corynocarpaceae*
                                [葫芦科](../Page/葫芦科.md "wikilink")
                                *Cucurbitaceae*
                                [野麻科](../Page/野麻科.md "wikilink")
                                *Datiscaceae*
                                [四数木科](../Page/四数木科.md "wikilink")
                                *Tetramelaceae*
                            [豆目](../Page/豆目.md "wikilink") *Fabales*
                              -
                                [豆科](../Page/豆科.md "wikilink")
                                *Fabaceae*
                                [远志科](../Page/远志科.md "wikilink")
                                *Polygalaceae*
                                [皂皮树科](../Page/皂皮树科.md "wikilink")
                                *Quillajaceae*
                                [海人树科](../Page/海人树科.md "wikilink")
                                *Surianaceae*
                            [壳斗目](../Page/壳斗目.md "wikilink") *Fagales*
                              -
                                [桦木科](../Page/桦木科.md "wikilink")
                                *Betulaceae*
                                [木麻黄科](../Page/木麻黄科.md "wikilink")
                                *Casuarinaceae*
                                [山毛榉科](../Page/山毛榉科.md "wikilink")
                                *Fagaceae*
                                [胡桃科](../Page/胡桃科.md "wikilink")
                                *Juglandaceae*
                                [杨梅科](../Page/杨梅科.md "wikilink")
                                *Myricaceae*
                                [南青冈科](../Page/南青冈科.md "wikilink")
                                *Nothofagaceae*
                                [马尾树科](../Page/马尾树科.md "wikilink")
                                *Rhoipteleaceae*
                                [太果木科](../Page/太果木科.md "wikilink")
                                *Ticodendraceae*
                            [金虎尾目](../Page/金虎尾目.md "wikilink")
                            *Malpighiales*
                              -
                                [钟花科](../Page/钟花科.md "wikilink")
                                *Achariaceae*
                                [槲树果科](../Page/槲树果科.md "wikilink")
                                *Balanopaceae*
                                [多子科](../Page/多子科.md "wikilink")
                                *Bonnetiaceae*
                                [油桃木科](../Page/油桃木科.md "wikilink")
                                *Caryocaraceae*
                                [金壳果科](../Page/金壳果科.md "wikilink")
                                *Chrysobalanaceae*

                    \+ [毒鼠子科](../Page/毒鼠子科.md "wikilink")
                    *Dichapetalaceae* + [大戟科](../Page/大戟科.md "wikilink")
                    *Euphorbiaceae* +[三角果科](../Page/三角果科.md "wikilink")
                    *Trigoniaceae*

                      -

                          -
                            [藤黄科](../Page/藤黄科.md "wikilink")
                            *Clusiaceae*
                            [垂籽树科](../Page/垂籽树科.md "wikilink")
                            *Ctenolophonaceae*
                            [沟繁缕科](../Page/沟繁缕科.md "wikilink")
                            *Elatinaceae*
                            [毛药树科](../Page/毛药树科.md "wikilink")
                            *Goupiaceae*
                            [香膏科](../Page/香膏科.md "wikilink")
                            *Humiriaceae*
                            [金丝桃科](../Page/金丝桃科.md "wikilink")
                            *Hypericaceae*
                            [包芽树科](../Page/包芽树科.md "wikilink")
                            *Irvingiaceae*
                            [粘木科](../Page/粘木科.md "wikilink")
                            *Ixonanthaceae*
                            [裂药花科](../Page/裂药花科.md "wikilink")
                            *Lacistemataceae*
                            [亚麻科](../Page/亚麻科.md "wikilink") *Linaceae*
                            [五翼果科](../Page/五翼果科.md "wikilink")
                            *Lophopyxidaceae*
                            [金虎尾科](../Page/金虎尾科.md "wikilink")
                            *Malpighiaceae*
                            [金莲木科](../Page/金莲木科.md "wikilink")
                            *Ochnaceae*

                    \+ [水母柱科](../Page/水母柱科.md "wikilink")
                    *Medusagynaceae* +
                    [羽叶树科](../Page/羽叶树科.md "wikilink")
                    *Quiinaceae*

                      -

                          -
                            [小盘木科](../Page/小盘木科.md "wikilink")
                            *Pandaceae*
                            [西番莲科](../Page/西番莲科.md "wikilink")
                            *Passifloraceae*

                    \+ [王冠草科](../Page/王冠草科.md "wikilink")
                    *Malesherbiaceae* +
                    [时钟花科](../Page/时钟花科.md "wikilink")
                    *Turneraceae*

                      -

                          -
                            [围盘树科](../Page/围盘树科.md "wikilink")
                            *Peridiscaceae*
                            [叶下珠科](../Page/叶下珠科.md "wikilink")
                            *Phyllanthaceae*
                            [苦胡桃科](../Page/苦胡桃科.md "wikilink") ''
                            Picrodendraceae''
                            [川苔草科](../Page/川苔草科.md "wikilink") ''
                            Podostemaceae''
                            [非洲核果木科](../Page/非洲核果木科.md "wikilink")
                            ''Putranjivaceae'
                            [红树科](../Page/红树科.md "wikilink")
                            *Rhizophoraceae*

                        \+ [古柯科](../Page/古柯科.md "wikilink")
                        *Erythroxylaceae*

                          -

                              -
                                [杨柳科](../Page/杨柳科.md "wikilink")
                                *Salicaceae*
                                [堇菜科](../Page/堇菜科.md "wikilink")
                                *Violaceae*

                            [酢浆草目](../Page/酢浆草目.md "wikilink")
                            *Oxalidales*

                              -
                                [瓣裂果科](../Page/瓣裂果科.md "wikilink")
                                *Brunelliaceae*
                                [土瓶草科](../Page/土瓶草科.md "wikilink")
                                *Cephalotaceae*
                                [牛栓藤科](../Page/牛栓藤科.md "wikilink")
                                *Connaraceae*
                                [火把树科](../Page/火把树科.md "wikilink")
                                *Cunoniaceae*
                                [杜英科](../Page/杜英科.md "wikilink")
                                *Elaeocarpaceae*
                                [酢浆草科](../Page/酢浆草科.md "wikilink")
                                *Oxalidaceae*

                            [蔷薇目](../Page/蔷薇目.md "wikilink") *Rosales*

                              -
                                [钩毛树科](../Page/钩毛树科.md "wikilink")
                                *Barbeyaceae*
                                [大麻科](../Page/大麻科.md "wikilink")
                                *Cannabaceae*
                                [八瓣果科](../Page/八瓣果科.md "wikilink")
                                *Dirachmaceae*
                                [胡颓子科](../Page/胡颓子科.md "wikilink")
                                *Elaeagnaceae*
                                [桑科](../Page/桑科.md "wikilink")
                                *Moraceae*
                                [鼠李科](../Page/鼠李科.md "wikilink")
                                *Rhamnaceae*
                                [蔷薇科](../Page/蔷薇科.md "wikilink")
                                *Rosaceae*
                                [榆科](../Page/榆科.md "wikilink")
                                *Ulmaceae*
                                [荨麻科](../Page/荨麻科.md "wikilink")
                                *Urticaceae*

                    **[II类真蔷薇分支](../Page/II类真蔷薇分支.md "wikilink")**
                    **eurosids II**

                      -

                          -
                            [瘿椒树科](../Page/瘿椒树科.md "wikilink")
                            *Tapisciaceae*

                        <!-- end list -->

                          -
                            [十字花目](../Page/十字花目.md "wikilink")
                            *Brassicales*
                              -
                                [叠珠树科](../Page/叠珠树科.md "wikilink")
                                *Akaniaceae*

                        \+ [钟萼木科](../Page/钟萼木科.md "wikilink")
                        *Bretschneideraceae*

                          -

                              -
                                [藜木科](../Page/藜木科.md "wikilink")
                                *Bataceae*
                                [十字花科](../Page/十字花科.md "wikilink")
                                *Brassicaceae*
                                [番木瓜科](../Page/番木瓜科.md "wikilink")
                                *Caricaceae*
                                [澳远志科](../Page/澳远志科.md "wikilink")
                                *Emblingiaceae*
                                [环蕊科](../Page/环蕊科.md "wikilink")
                                *Gyrostemonaceae*
                                [刺枝树科](../Page/刺枝树科.md "wikilink")
                                *Koeberliniaceae*
                                [池花科](../Page/池花科.md "wikilink")
                                *Limnanthaceae*
                                [辣木科](../Page/辣木科.md "wikilink")
                                *Moringaceae*
                                [瘤药树科](../Page/瘤药树科.md "wikilink")
                                *Pentadiplandraceae*
                                [木犀草科](../Page/木犀草科.md "wikilink")
                                *Resedaceae*
                                [刺茉莉科](../Page/刺茉莉科.md "wikilink")
                                *Salvadoraceae*
                                [夷白花菜科](../Page/夷白花菜科.md "wikilink")
                                *Setchellanthaceae*
                                [烈味三叶草科](../Page/烈味三叶草科.md "wikilink")
                                *Tovariaceae*
                                [旱金莲科](../Page/旱金莲科.md "wikilink")
                                *Tropaeolaceae*

                            [锦葵目](../Page/锦葵目.md "wikilink") *Malvales*

                              -
                                [红木科](../Page/红木科.md "wikilink")
                                *Bixaceae*

                    \+[地果莲木科](../Page/地果莲木科.md "wikilink")
                    *Diegodendraceae* +
                    [弯子木科](../Page/弯子木科.md "wikilink")
                    *Cochlospermaceae*

                      -

                          -
                            [半日花科](../Page/半日花科.md "wikilink")
                            *Cistaceae*
                            [龙脑香科](../Page/龙脑香科.md "wikilink")
                            *Dipterocarpaceae*
                            [锦葵科](../Page/锦葵科.md "wikilink") *Malvaceae*
                            [文定果科](../Page/文定果科.md "wikilink")
                            *Muntingiaceae*
                            [沙莓科](../Page/沙莓科.md "wikilink")
                            *Neuradaceae*
                            [旋花树科](../Page/旋花树科.md "wikilink")
                            *Sarcolaenaceae*
                            [球萼树科](../Page/球萼树科.md "wikilink")
                            *Sphaerosepalaceae*
                            [瑞香科](../Page/瑞香科.md "wikilink")
                            *Thymelaeaceae*

                        <!-- end list -->

                          -
                            [无患子目](../Page/无患子目.md "wikilink")
                            *Sapindales*
                              -
                                [漆树科](../Page/漆树科.md "wikilink")
                                *Anacardiaceae*
                                [薰倒牛科](../Page/薰倒牛科.md "wikilink")
                                *Biebersteiniaceae*
                                [橄榄科](../Page/橄榄科.md "wikilink")
                                *Burseraceae*
                                [番苦木科](../Page/番苦木科.md "wikilink")
                                *Kirkiaceae*
                                [楝科](../Page/楝科.md "wikilink")
                                *Meliaceae*
                                [白刺科](../Page/白刺科.md "wikilink")
                                *Nitrariaceae*

                    \+ [骆驼蓬科](../Page/骆驼蓬科.md "wikilink") *Peganaceae* +
                    [旱霸王科](../Page/旱霸王科.md "wikilink")
                    *Tetradiclidaceae*

                      -

                          -
                            [芸香科](../Page/芸香科.md "wikilink") *Rutaceae*
                            [无患子科](../Page/无患子科.md "wikilink")
                            *Sapindaceae*
                            [苦木科](../Page/苦木科.md "wikilink")
                            *Simaroubaceae*

                ''' [菊分支](../Page/菊分支.md "wikilink") ''' **asterids**

                  -

                      -
                        [山茱萸目](../Page/山茱萸目.md "wikilink") *Cornales*
                          -
                            [山茱萸科](../Page/山茱萸科.md "wikilink")
                            *Cornaceae*

                \+ [珙桐科](../Page/珙桐科.md "wikilink") *Nyssaceae*

                  -

                      -
                        [南非茱萸科](../Page/南非茱萸科.md "wikilink")
                        *Curtisiaceae*
                        [假石南科](../Page/假石南科.md "wikilink") *Grubbiaceae*
                        [绣球花科](../Page/绣球花科.md "wikilink")
                        *Hydrangeaceae*
                        [水穗草科](../Page/水穗草科.md "wikilink")
                        *Hydrostachyaceae*
                        [刺莲花科](../Page/刺莲花科.md "wikilink") *Loasaceae*

                    <!-- end list -->

                      -
                        [杜鹃花目](../Page/杜鹃花目.md "wikilink") *Ericales*
                          -
                            [猕猴桃科](../Page/猕猴桃科.md "wikilink")
                            *Actinidiaceae*
                            [凤仙花科](../Page/凤仙花科.md "wikilink")
                            *Balsaminaceae*
                            [山柳科](../Page/山柳科.md "wikilink")
                            *Clethraceae*
                            [翅萼树科](../Page/翅萼树科.md "wikilink")
                            *Cyrillaceae*
                            [岩梅科](../Page/岩梅科.md "wikilink")
                            *Diapensiaceae*
                            [柿树科](../Page/柿树科.md "wikilink") *Ebenaceae*
                            [杜鹃花科](../Page/杜鹃花科.md "wikilink")
                            *Ericaceae*
                            [福桂花科](../Page/福桂花科.md "wikilink")
                            *Fouquieriaceae*
                            [玉蕊科](../Page/玉蕊科.md "wikilink")
                            *Lecythidaceae*
                            [杜茎山科](../Page/杜茎山科.md "wikilink")
                            *Maesaceae*
                            [蜜囊花科](../Page/蜜囊花科.md "wikilink")
                            *Marcgraviaceae*
                            [紫金牛科](../Page/紫金牛科.md "wikilink")
                            *Myrsinaceae*
                            [五列木科](../Page/五列木科.md "wikilink")
                            *Pentaphylacaceae*

                    \+ [厚皮香科](../Page/厚皮香科.md "wikilink")
                    *Ternstroemiaceae* +
                    [肋果茶科](../Page/肋果茶科.md "wikilink")
                    *Sladeniaceae*

                      -

                          -
                            [花荵科](../Page/花荵科.md "wikilink")
                            *Polemoniaceae*
                            [报春花科](../Page/报春花科.md "wikilink")
                            *Primulaceae*
                            [捕蝇幌科](../Page/捕蝇幌科.md "wikilink")
                            *Roridulaceae*
                            [山榄科](../Page/山榄科.md "wikilink")
                            *Sapotaceae*
                            [瓶子草科](../Page/瓶子草科.md "wikilink")
                            *Sarraceniaceae*
                            [野茉莉科](../Page/野茉莉科.md "wikilink")
                            *Styracaceae*
                            [山矾科](../Page/山矾科.md "wikilink")
                            *Symplocaceae*
                            [四籽树科](../Page/四籽树科.md "wikilink")
                            *Tetrameristaceae*

                    \+ [假红树科](../Page/假红树科.md "wikilink")
                    *Pellicieraceae*

                      -

                          -
                            [山茶科](../Page/山茶科.md "wikilink") *Theaceae*
                            [假轮叶科](../Page/假轮叶科.md "wikilink")
                            *Theophrastaceae*

                    *' [I类真菊分支](../Page/I类真菊分支.md "wikilink")*'
                    **euasterids I**

                      -

                          -
                            [紫草科](../Page/紫草科.md "wikilink")
                            *Boraginaceae*
                            [茶茱萸科](../Page/茶茱萸科.md "wikilink")*Icacinaceae*
                            [五蕊茶科](../Page/五蕊茶科.md "wikilink")*Oncothecaceae*
                            [二歧草科](../Page/二歧草科.md "wikilink")
                            *Vahliaceae*

                        <!-- end list -->

                          -
                            [绞木目](../Page/绞木目.md "wikilink") *Garryales*
                              -
                                [杜仲科](../Page/杜仲科.md "wikilink")
                                *Eucommiaceae*
                                [绞木科](../Page/绞木科.md "wikilink")
                                *Garryaceae*

                    \+ [桃叶珊瑚科](../Page/桃叶珊瑚科.md "wikilink") *Aucubaceae*

                      -

                          -
                            [龙胆目](../Page/龙胆目.md "wikilink")
                            *Gentianales*
                              -
                                [夹竹桃科](../Page/夹竹桃科.md "wikilink")
                                *Apocynaceae*
                                [胡蔓藤科](../Page/胡蔓藤科.md "wikilink")
                                *Gelsemiaceae*
                                [龙胆科](../Page/龙胆科.md "wikilink")
                                *Gentianaceae*
                                [马钱科](../Page/马钱科.md "wikilink")
                                *Loganiaceae*
                                [茜草科](../Page/茜草科.md "wikilink")
                                *Rubiaceae*
                            [唇形目](../Page/唇形目.md "wikilink") *Lamiales*
                              -
                                [爵床科](../Page/爵床科.md "wikilink")
                                *Acanthaceae*
                                [紫葳科](../Page/紫葳科.md "wikilink")
                                *Bignoniaceae*
                                [腺毛草科](../Page/腺毛草科.md "wikilink")
                                *Byblidaceae*
                                [蒲包花科](../Page/蒲包花科.md "wikilink")
                                *Calceolariaceae*
                                [香茜科](../Page/香茜科.md "wikilink")
                                *Carlemanniaceae*
                                [苦苣苔科](../Page/苦苣苔科.md "wikilink")
                                *Gesneriaceae*
                                [唇形科](../Page/唇形科.md "wikilink")
                                *Lamiaceae*
                                [狸藻科](../Page/狸藻科.md "wikilink")
                                *Lentibulariaceae*
                                [母草科](../Page/母草科.md "wikilink")
                                *Linderniaceae*
                                [角胡麻科](../Page/角胡麻科.md "wikilink")
                                *Martyniaceae*
                                [木犀科](../Page/木犀科.md "wikilink")
                                *Oleaceae*
                                [列当科](../Page/列当科.md "wikilink")
                                *Orobanchaceae*
                                [泡桐科](../Page/泡桐科.md "wikilink")
                                *Paulowniaceae*
                                [胡麻科](../Page/胡麻科.md "wikilink")
                                *Pedaliaceae*
                                [透骨草科](../Page/透骨草科.md "wikilink")
                                *Phrymaceae*
                                [车前科](../Page/车前科.md "wikilink")
                                *Plantaginaceae*
                                [环生籽科](../Page/环生籽科.md "wikilink")
                                *Plocospermataceae*
                                [夷地黄科](../Page/夷地黄科.md "wikilink")
                                *Schlegeliaceae*
                                [玄参科](../Page/玄参科.md "wikilink")
                                *Scrophulariaceae*
                                [密穗草科](../Page/密穗草科.md "wikilink")
                                *Stilbaceae*
                                [四粉草科](../Page/四粉草科.md "wikilink")
                                *Tetrachondraceae*
                                [马鞭草科](../Page/马鞭草科.md "wikilink")
                                *Verbenaceae*
                            [茄目](../Page/茄目.md "wikilink") *Solanales*
                              -
                                [旋花科](../Page/旋花科.md "wikilink")
                                *Convolvulaceae*
                                [田基麻科](../Page/田基麻科.md "wikilink")
                                *Hydroleaceae*
                                [山醋李科](../Page/山醋李科.md "wikilink")
                                *Montiniaceae*
                                [茄科](../Page/茄科.md "wikilink")
                                *Solanaceae*
                                [楔瓣花科](../Page/楔瓣花科.md "wikilink")
                                *Sphenocleaceae*

                    **[II类真菊分支](../Page/II类真菊分支.md "wikilink")**
                    **euasterids II**

                      -

                          -
                            [鳞叶树科](../Page/鳞叶树科.md "wikilink")
                            *Bruniaceae*
                            [弯药树科](../Page/弯药树科.md "wikilink")*Columelliaceae*

                        \+[离水花科](../Page/离水花科.md "wikilink")*Desfontainiaceae*

                          -

                              -
                                [寄奴花科](../Page/寄奴花科.md "wikilink")*Eremosynaceae*
                                [南鼠刺科](../Page/南鼠刺科.md "wikilink")*Escalloniaceae*
                                [盔瓣花科](../Page/盔瓣花科.md "wikilink")*Paracryphiaceae*
                                [多香木科](../Page/多香木科.md "wikilink")*Polyosmaceae*
                                [楔蕊花科](../Page/楔蕊花科.md "wikilink")*Sphenostemonaceae*
                                [智利木科](../Page/智利木科.md "wikilink")
                                *Tribelaceae*

                            [伞形目](../Page/伞形目.md "wikilink") *Apiales*

                              -
                                [伞形科](../Page/伞形科.md "wikilink")
                                *Apiaceae*
                                [五加科](../Page/五加科.md "wikilink")
                                *Araliaceae*
                                [假茱萸科](../Page/假茱萸科.md "wikilink")
                                *Aralidiaceae*
                                [夷茱萸科](../Page/夷茱萸科.md "wikilink")
                                *Griseliniaceae*
                                [参棕科](../Page/参棕科.md "wikilink")
                                *Mackinlayaceae*
                                [番茱萸科](../Page/番茱萸科.md "wikilink")
                                *Melanophyllaceae*
                                [裂果红科](../Page/裂果红科.md "wikilink")
                                *Myodocarpaceae*
                                [彭南特氏木科](../Page/彭南特氏木科.md "wikilink")
                                *Pennantiaceae*
                                [海桐花科](../Page/海桐花科.md "wikilink")
                                *Pittosporaceae*
                                [鞘柄木科](../Page/鞘柄木科.md "wikilink")
                                *Torricelliaceae*

                            [冬青目](../Page/冬青目.md "wikilink")
                            *Aquifoliales*

                              -
                                [冬青科](../Page/冬青科.md "wikilink")
                                *Aquifoliaceae*
                                [心翼果科](../Page/心翼果科.md "wikilink") ''
                                Cardiopteridaceae''
                                [青荚叶科](../Page/青荚叶科.md "wikilink")
                                *Helwingiaceae*
                                [叶茶藨科](../Page/叶茶藨科.md "wikilink")
                                *Phyllonomaceae*
                                [金檀木科](../Page/金檀木科.md "wikilink")
                                *Stemonuraceae*

                            [菊目](../Page/菊目.md "wikilink") *Asterales*

                              -
                                [假海桐科](../Page/假海桐科.md "wikilink")
                                *Alseuosmiaceae*
                                [雪叶科](../Page/雪叶科.md "wikilink")
                                *Argophyllaceae*
                                [菊科](../Page/菊科.md "wikilink")
                                *Asteraceae*
                                [头花草科](../Page/头花草科.md "wikilink")
                                *Calyceraceae*
                                [桔梗科](../Page/桔梗科.md "wikilink")
                                *Campanulaceae*

                    \+ [半边莲科](../Page/半边莲科.md "wikilink") *Lobeliaceae*

                      -

                          -
                            [草海桐科](../Page/草海桐科.md "wikilink")
                            *Goodeniaceae*
                            [睡菜科](../Page/睡菜科.md "wikilink")
                            *Menyanthaceae*
                            [五膜草科](../Page/五膜草科.md "wikilink")
                            *Pentaphragmataceae*
                            [石冬青科](../Page/石冬青科.md "wikilink")
                            *Phellinaceae*
                            [卢梭木科](../Page/卢梭木科.md "wikilink")
                            *Rousseaceae*
                            [花柱草科](../Page/花柱草科.md "wikilink")
                            *Stylidiaceae*

                    \+ [陀螺果科](../Page/陀螺果科.md "wikilink") *Donatiaceae*

                      -

                          -
                            [川续断目](../Page/川续断目.md "wikilink")
                            *Dipsacales*
                              -
                                [五福花科](../Page/五福花科.md "wikilink")
                                *Adoxaceae*
                                [忍冬科](../Page/忍冬科.md "wikilink")
                                *Caprifoliaceae*

                        \+ [黄锦带科](../Page/黄锦带科.md "wikilink")
                        *Diervillaceae* +
                        [川续断科](../Page/川续断科.md "wikilink")
                        *Dipsacaceae* +
                        [北极花科](../Page/北极花科.md "wikilink")
                        *Linnaeaceae* +
                        [刺续断科](../Page/刺续断科.md "wikilink")
                        *Morinaceae* + [败酱科](../Page/败酱科.md "wikilink")
                        *Valerianaceae*

注: "+ ..." 是一种建议选择，可以合并也可以单独分为一科。

-----

## 尚无定论的属和科

如果可以列为单属科的属，将科名也同时列出：

  - *[Aneulophus](../Page/Aneulophus.md "wikilink")* Benth.
  - [离花科](../Page/离花科.md "wikilink") *Apodanthaceae* van Tieghem ex
    Takhtajan in Takhtajan （1997年）共三属
  - [美洲簇花草属](../Page/美洲簇花草属.md "wikilink") *Bdallophyton* Eichl.
  - [蛇菰科](../Page/蛇菰科.md "wikilink") *Balanophoraceae* Rich. （1822年）
  - *[Centroplacus](../Page/Centroplacus.md "wikilink")* Pierre
  - [锁阳属](../Page/锁阳属.md "wikilink") *Cynomorium* L.
    （[锁阳科](../Page/锁阳科.md "wikilink") *Cynomoriaceae*
    Lindl. 1833年）
  - [簇花草属](../Page/簇花草属.md "wikilink") *Cytinus* L.
    （[簇花草科](../Page/簇花草科.md "wikilink") *Cytinaceae*
    A.Rich. 1824年）
  - [十齿花属](../Page/十齿花属.md "wikilink") *Dipentodon* Dunn
    （[十齿花科](../Page/十齿花科.md "wikilink") *Dipentodontaceae*
    Merr. (1941年)
  - *[Gumillea](../Page/Gumillea.md "wikilink")* Ruiz & Pav.
  - [单柱花属](../Page/单柱花属.md "wikilink") *Hoplestigma* Pierre
    （[单柱花科](../Page/单柱花科.md "wikilink") *Hoplestigmataceae*
    Engl. & Gilg 1924年）
  - *[Leptaulus](../Page/Leptaulus.md "wikilink")* Benth.
  - [毛丝花属](../Page/毛丝花属.md "wikilink") *Medusandra* Brenan
    （[毛丝花科](../Page/毛丝花科.md "wikilink") *Medusandraceae*
    Brenan 1952年）
  - [管花木属](../Page/管花木属.md "wikilink") *Metteniusa* H.Karst.
    （[管花木科](../Page/管花木科.md "wikilink") *Metteniusaceae*
    H.Karst. ex Schnizl. 1860年-1870年）
  - [帽蕊草属](../Page/帽蕊草属.md "wikilink") *Mitrastema* Makino
    （[帽蕊草科](../Page/帽蕊草科.md "wikilink") *Mitrastemonaceae*
    Makino 1911年）
  - [托叶假樟属](../Page/托叶假樟属.md "wikilink") *Pottingeria* Prain
    （[托叶假樟科](../Page/托叶假樟科.md "wikilink") *Pottingeriaceae*
    (Engl.) Takht. 1987年）
  - [大花草科](../Page/大花草科.md "wikilink") *Rafflesiaceae* Dumort.
    （1829年）共三属
  - [非洲盘树属](../Page/非洲盘树属.md "wikilink") *Soyauxia* Oliv.
  - *[Trichostephanus](../Page/Trichostephanus.md "wikilink")* Gilg

## 參考文獻

  - Stevens, P. F. (2001 onwards). [Angiosperm Phylogeny Website.
    Version 9,
    June 2008](http://www.mobot.org/MOBOT/Research/APweb/welcome.html),
    and more or less continuously updated since\].
  - Angiosperm phylogeny group. 2003. [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    II](http://onlinelibrary.wiley.com/doi/10.1046/j.1095-8339.2003.t01-1-00158.x/pdf).
    Botanical Journal of the Linnean Society 141: 399–436.
  - Angiosperm phylogeny group. 2009. [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    III](http://onlinelibrary.wiley.com/doi/10.1111/j.1095-8339.2009.00996.x/pdf).
    Botanical Journal of the Linnean Society 161: 105–121.

[category:植物學](../Page/category:植物學.md "wikilink")
[category:植物分類學](../Page/category:植物分類學.md "wikilink")

[Category:生物分類學](../Category/生物分類學.md "wikilink")