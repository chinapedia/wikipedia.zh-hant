**凡城省**（土耳其语：**Van**；[亚美尼亚语](../Page/亚美尼亚语.md "wikilink")：**Վան**
）位于[凡湖与](../Page/凡湖.md "wikilink")[伊朗中间](../Page/伊朗.md "wikilink")，是[土耳其東部的一个省](../Page/土耳其.md "wikilink")，面积19,069平方公里，人口約102萬人（2009年），首府[凡城](../Page/凡城.md "wikilink")。著名的[土耳其梵貓的老家就在凡城省及周边地区](../Page/土耳其梵貓.md "wikilink")。

公元前9世纪，凡城地区成为了[烏拉爾圖王國的中心](../Page/烏拉爾圖王國.md "wikilink")。在此后的数个世纪-{zh-hans:里;
zh-hant:裡;}-，这里一直都是[亚美尼亚人的主要居住地](../Page/亚美尼亚人.md "wikilink")。[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，亚美尼亚人遭到土耳其奥斯曼帝国政府的种族大屠杀，被迫离开了这里或被“清除”。

2011年10月23日發生大[地震造成多人傷亡](../Page/2011年凡城地震.md "wikilink")

## 图片

<File:Urartian> fort in Çavuştepe.jpg|Haykaberd <File:Hosap-castle>
(17).jpg|Hoşap 古堡 <File:Muradiye> Falls 2.JPG|Muradiye 瀑布 Image:Akdamar
and mountain.jpg|位于Akdamar岛上的Holy Cross亚美尼亚大教堂（10世纪）
Image:Narekavank.jpg|[亚美尼亚使徒教会Narekavank](../Page/亚美尼亚使徒教会.md "wikilink")[修道院](../Page/修道院.md "wikilink")（10世纪）
Image:Varagavank.jpg|Varagavank修道院（11世纪） Image:Saint Bartholomew
Monastery.jpg|Saint Bartholomew修道院（13世纪） Image:Mountains near Van.jpg|山脉

## 外部链接

  - <http://www.van.gov.tr>
  - [首府凡城图片](http://www.pbase.com/dosseman/van)

[\*](../Category/凡城省.md "wikilink") [V](../Category/土耳其省份.md "wikilink")