**雨果博斯**（）是一個[德國的](../Page/德國.md "wikilink")[時尚](../Page/時尚.md "wikilink")[品牌](../Page/品牌.md "wikilink")，主攻高價位男裝[成衣及配件](../Page/成衣.md "wikilink")，[創辦人是Hugo](../Page/創辦人.md "wikilink")
Ferdinand Boss（1885–1948）。

## 歷史

雨果博斯創立於1923年，[第一次世界大戰之後](../Page/第一次世界大戰.md "wikilink")，發源地在[德國](../Page/德國.md "wikilink")[斯圖加特南面的](../Page/斯圖加特.md "wikilink")[麥琴根](../Page/麥琴根.md "wikilink")（Metzingen）\[1\]，該處原是名牌BOSS工廠的所在地，後來發展成為德國品牌工廠區。

雨果博斯創辦初期主要生產男裝，數年後，因[德國](../Page/德國.md "wikilink")[經濟蕭條而接近](../Page/景氣衰退.md "wikilink")[破產](../Page/破產.md "wikilink")。在1930年代至1940年代初，雨果博斯轉營为纳粹党生產[德國突擊隊](../Page/德國突擊隊.md "wikilink")（Storm
Troopers）、[党卫队](../Page/党卫队.md "wikilink")[制服](../Page/制服.md "wikilink")，及[希特勒青年團的裝備](../Page/希特勒青年團.md "wikilink")。

在1985年，雨果博斯在[德國成為](../Page/德國.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")，之後於1991年被[義大利時裝集團Marzotto](../Page/義大利.md "wikilink")[收購](../Page/收購.md "wikilink")，由此Boss家族與雨果博斯品牌已無任何關係。

2018年，天王Michael Jackson逝世十周年，Hugo Boss與National Portrait
Gallery聯手在英國舉辦名為“Michael Jackson: On the
Wall”展覽，探討Michael Jackson對當代藝術的啓發\[2\]。

## 旗下品牌

对于Boss这个品牌，拥有以下5个子品牌：

  - **Boss**: 商务、晚装类
  - **Boss Tailored** 高质量面料、手工艺
  - **Boss Orange**: 休闲类
  - **Boss Green**: 运动类
  - *'Hugo*

## 经营数字

2007财年该公司营业额为16.32亿欧元，盈利1.54亿欧元。西服生产位于他在德国Metzingen，土耳其和美国的工厂。82%的营业额由旗下知名男士品牌Boss
Black所产生。全世界（截至：2006年12月31日）拥有210家自有专卖店。

## 外部連線

  - *[華盛頓郵報](../Page/華盛頓郵報.md "wikilink")*, 1997年8月14日；Page B01
    [1](https://web.archive.org/web/20051026065449/http://www.thethirdrail.com/hugo/news.htm)
    [2](http://americandefenseleague.com/hilknow1.htm)
  - *[彭博資訊](../Page/彭博資訊.md "wikilink")*, 2006年8月25日 Baldessarini sold
    [3](http://www.bloomberg.com/apps/news?pid=20601100&refer=&sid=aYrSnLCRU.Z4)

## 參考文獻

[Category:德國品牌](../Category/德國品牌.md "wikilink")
[H](../Category/巴登-符騰堡州公司.md "wikilink")
[Category:时尚品牌](../Category/时尚品牌.md "wikilink")
[Category:西装制造商](../Category/西装制造商.md "wikilink")

1.  <http://www.leitour.com/city/metzingen.htm>
2.  [當代藝術向Michael Jackson再致敬｜Hugo
    Boss擕經典Thriller白色西裝倫敦開展](https://mings.mpweekly.com/mings-fashion/20180629-85468)