**新地**或**新陆**（，），是[芬兰的一个](../Page/芬兰.md "wikilink")[行政区](../Page/芬兰行政区.md "wikilink")。它和[西南芬兰區](../Page/西南芬兰區.md "wikilink")、[坎塔海梅區](../Page/坎塔海梅區.md "wikilink")、[派亞特海梅區](../Page/派亞特海梅區.md "wikilink")、[屈米河谷区接壤](../Page/屈米河谷区.md "wikilink")。

2011年1月1日[東新地區合併至新地区](../Page/東新地區.md "wikilink")。

## 市镇

新地划分为4个次区，共26个[市镇](../Page/芬兰市镇.md "wikilink")，其中13个使用“城市”称呼（用粗体标出），括弧內左邊為[芬蘭文](../Page/芬蘭文.md "wikilink")、右邊為[瑞典文](../Page/瑞典文.md "wikilink")。15个市镇的官方语言为双语（芬兰语和瑞典语）。
[Uusimaa_kunnat.svg](https://zh.wikipedia.org/wiki/File:Uusimaa_kunnat.svg "fig:Uusimaa_kunnat.svg")
 **赫爾辛基次區：**

  - [Helsinki.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Helsinki.vaakuna.svg "fig:Helsinki.vaakuna.svg")
    **[赫爾辛基](../Page/赫爾辛基.md "wikilink")**（ / Helsingfors）
  - [Espoo.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Espoo.vaakuna.svg "fig:Espoo.vaakuna.svg")
    **[埃斯波](../Page/埃斯波.md "wikilink")**（ / Esbo）
  - [Vantaa.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Vantaa.vaakuna.svg "fig:Vantaa.vaakuna.svg")
    **[萬塔](../Page/萬塔.md "wikilink")**（ / ）
  - [Kauniainen.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Kauniainen.vaakuna.svg "fig:Kauniainen.vaakuna.svg")
    **[考尼艾寧](../Page/考尼艾寧.md "wikilink")**（ / ）
  - [Kerava.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Kerava.vaakuna.svg "fig:Kerava.vaakuna.svg")
    '''[凱拉瓦](../Page/凱拉瓦.md "wikilink") '''（ / ）
  - [Jarvenpaa.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Jarvenpaa.vaakuna.svg "fig:Jarvenpaa.vaakuna.svg")**[耶爾文佩](../Page/耶爾文佩.md "wikilink")**（
    / ）
  - [Hyvinkaa.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Hyvinkaa.vaakuna.svg "fig:Hyvinkaa.vaakuna.svg")
    **[許溫凱](../Page/許溫凱.md "wikilink")**（ / ）
  - [Karkkila.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Karkkila.vaakuna.svg "fig:Karkkila.vaakuna.svg")
    **[卡爾基拉](../Page/卡爾基拉.md "wikilink")**（ / ）
  - [Lohja.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Lohja.vaakuna.svg "fig:Lohja.vaakuna.svg")
    **[洛赫亞](../Page/洛赫亞.md "wikilink")**（ / ）
  - [Kirkkonummi.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Kirkkonummi.vaakuna.svg "fig:Kirkkonummi.vaakuna.svg")
    [基爾科努米](../Page/基爾科努米.md "wikilink")（ / ）
  - [Mäntsälä.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Mäntsälä.vaakuna.svg "fig:Mäntsälä.vaakuna.svg")
    [曼采萊](../Page/曼采萊.md "wikilink")（）
  - [Nurmijarvi.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Nurmijarvi.vaakuna.svg "fig:Nurmijarvi.vaakuna.svg")
    [努爾米耶爾维](../Page/努爾米耶爾维.md "wikilink")（）
  - [Pornainen.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Pornainen.vaakuna.svg "fig:Pornainen.vaakuna.svg")
    [波爾奈寧](../Page/波爾奈寧.md "wikilink")（ / ）
  - [Sipoo.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Sipoo.vaakuna.svg "fig:Sipoo.vaakuna.svg")
    [錫博](../Page/錫博.md "wikilink")（ / ）
  - [Siuntio.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Siuntio.vaakuna.svg "fig:Siuntio.vaakuna.svg")
    [錫温蒂奥](../Page/錫温蒂奥.md "wikilink")（ / ）
  - [Tuusula.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Tuusula.vaakuna.svg "fig:Tuusula.vaakuna.svg")
    [圖蘇拉](../Page/圖蘇拉.md "wikilink")（ / ）
  - [Vihti.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Vihti.vaakuna.svg "fig:Vihti.vaakuna.svg")[維赫蒂](../Page/維赫蒂.md "wikilink")（
    / ）

**拉塞博里次區：**

  - [Hanko.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Hanko.vaakuna.svg "fig:Hanko.vaakuna.svg")
    **[漢科](../Page/漢科.md "wikilink")**（ / ）
  - [Inkoo.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Inkoo.vaakuna.svg "fig:Inkoo.vaakuna.svg")
    [因戈](../Page/因戈.md "wikilink")（ / ）
  - [Raasepori.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Raasepori.vaakuna.svg "fig:Raasepori.vaakuna.svg")
    **[拉塞博里](../Page/拉塞博里.md "wikilink")**（ / ）

**波爾沃次區：**

  - [Askola.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Askola.vaakuna.svg "fig:Askola.vaakuna.svg")
    [阿斯科拉](../Page/阿斯科拉.md "wikilink")（）
  - [Myrskylä.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Myrskylä.vaakuna.svg "fig:Myrskylä.vaakuna.svg")
    [米爾斯屈萊](../Page/米爾斯屈萊.md "wikilink")（ / ）
  - [Pukkila.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Pukkila.vaakuna.svg "fig:Pukkila.vaakuna.svg")
    [普基拉](../Page/普基拉.md "wikilink")（）
  - [Porvoo.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Porvoo.vaakuna.svg "fig:Porvoo.vaakuna.svg")
    **[波爾沃](../Page/波爾沃.md "wikilink")**（ / ）

**洛維薩次區：**

  - [Lapinjärvi.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Lapinjärvi.vaakuna.svg "fig:Lapinjärvi.vaakuna.svg")
    [拉平耶爾維](../Page/拉平耶爾維.md "wikilink")（ / ）
  - [Loviisa.vaakuna.svg](https://zh.wikipedia.org/wiki/File:Loviisa.vaakuna.svg "fig:Loviisa.vaakuna.svg")
    **[洛維薩](../Page/洛維薩.md "wikilink")**（ / ）

## 相關圖片

<File:Catedral> Luterana de Helsinki, Finlandia, 2012-08-14, DD
02.JPG|[赫爾辛基大教堂](../Page/赫爾辛基大教堂.md "wikilink") <File:Porvoo>
OldTownAndBridge 01.jpg|波爾沃舊城區
[File:Nurmijärven_kirkko.jpg|努爾米耶爾维教堂](File:Nurmijärven_kirkko.jpg%7C努爾米耶爾维教堂)
<File:Ekenäs> old town 1.jpg|[埃克奈斯舊城區](../Page/埃克奈斯.md "wikilink")
[File:11-07-29-helsinki-by-RalfR-104.jpg|赫爾辛基中心區域](File:11-07-29-helsinki-by-RalfR-104.jpg%7C赫爾辛基中心區域)
<File:Helsinki-Vantaa> Airport
4.jpg|[赫爾辛基-萬塔機場](../Page/赫爾辛基-萬塔機場.md "wikilink")，為芬蘭最大的機場
<File:Hanko> Church from water tower.jpg|漢科，為芬蘭最南端的城鎮 <File:Aleksis>
Kiven kuolinmökki IMG 2897
C.JPG|[阿萊克西斯·基維逝世的小屋](../Page/阿萊克西斯·基維.md "wikilink")，坐落於[圖蘇拉](../Page/圖蘇拉.md "wikilink")；此亦為該地區19世紀晚期住房的典型
<File:Fagervik.jpg>|[因戈的法格維克莊園](../Page/因戈.md "wikilink")；該地區有很多類似的莊園。
<File:Lohjan> kirkko 0397.JPG|15世紀中葉的洛赫亞教堂是芬蘭最大的教堂之一
<File:Espoon_Tapiola_kesällä.jpg>|[埃斯波](../Page/埃斯波.md "wikilink")
<File:Hvitträskin>
sisäpiha.jpg|Hvitträsk，在[基爾科努米的民族浪漫主義風格住宅](../Page/基爾科努米.md "wikilink")，現為博物館
<File:Fiskars,_kadunvarsitaloja.jpg>|[菲斯卡斯村庄的景象](../Page/菲斯卡斯.md "wikilink")，同名的公司最早在该村设立打铁工房
<File:Lohilammen> museo 3.JPG|洛希兰皮博物馆内展现的19世纪农民住屋的内部
<File:Ainola_yard.jpg>|[爱诺拉](../Page/艾诺拉.md "wikilink")，位于[耶尔文佩的](../Page/耶尔文佩.md "wikilink")[让·西贝柳斯的故居](../Page/让·西贝柳斯.md "wikilink")
<File:Loviisa> Rath 3.JPG|[洛维萨市政厅](../Page/洛维萨.md "wikilink")
[File:Soderskar-overview.jpg|波尔沃群岛中的芬兰湾风景](File:Soderskar-overview.jpg%7C波尔沃群岛中的芬兰湾风景)
<File:Castle> of Raseborg (Raaseporin linna) in Tammisaari
Finland.jpg|部分已成废墟的[拉塞博里城堡](../Page/拉塞博里城堡.md "wikilink")
<File:Vantaa> church.jpg|，首都地区历史最久的建筑物 <File:Hyvinkään> kirkko harmaana
syyspäivänä.jpg|，一个芬兰现代教堂建筑的典范 <File:Hiidenvesi> Taustan tieltä
1.jpg|希登韦西湖边风景 <File:Kerava>
3.jpg|[凯拉瓦市中心的建筑](../Page/凯拉瓦.md "wikilink")
<File:Hirvihaaran>
kartano5.jpg|位于[曼采莱的希尔维哈拉庄园](../Page/曼采莱.md "wikilink")
<File:Karkkila> - Helsingintie C IMG
4276.JPG|新地区北部的小镇[卡尔基拉](../Page/卡尔基拉.md "wikilink")
<File:Suomenlinna> mereltä
2.jpg|[芬兰城堡](../Page/芬兰城堡.md "wikilink")，背景为赫尔辛基市中心
<File:Nuuksio_lake_4.jpg>|[埃斯波](../Page/埃斯波.md "wikilink")[努克西奥国家公园内的风景](../Page/努克西奥国家公园.md "wikilink")
[File:Panorama_Tower_and_Leppävaara_Tower_from_Säteri_1.JPG|勒派瓦拉，埃斯波的多个市中心之一](File:Panorama_Tower_and_Leppävaara_Tower_from_Säteri_1.JPG%7C勒派瓦拉，埃斯波的多个市中心之一)

## 外部链接

  - [新地行政区理事会](https://web.archive.org/web/20060203225713/http://www2.uudenmaanliitto.fi/eng/)
  - [新地地图](https://web.archive.org/web/20060828123534/http://www.inkoo.fi/kartor/nyland.gif)

[U](../Category/芬蘭區份.md "wikilink")