[Asterisk.PNG](https://zh.wikipedia.org/wiki/File:Asterisk.PNG "fig:Asterisk.PNG")

**星號**（[英文](../Page/英文.md "wikilink")：asterisk，[拉丁文](../Page/拉丁文.md "wikilink")：asteriscum，意謂「小星星」，來自[希臘文](../Page/希臘文.md "wikilink")
）是[印刷](../Page/印刷.md "wikilink")[符號或](../Page/符號.md "wikilink")[字形](../Page/字形.md "wikilink")。之所以稱為星號是因為與一般人印象中的[星星相似](../Page/星星.md "wikilink")。[電腦科學家與](../Page/電腦科學家.md "wikilink")[數學家常稱之為](../Page/數學家.md "wikilink")「**star**」或「**星**」（例如「[A\*搜尋演算法](../Page/A*搜尋演算法.md "wikilink")」和「[C\*-代數](../Page/C*-代數.md "wikilink")」）。

星號起源於[歐洲封建時代](../Page/歐洲封建時代.md "wikilink")，[族譜印製者要表示出生日期的](../Page/族譜.md "wikilink")[符號](../Page/符號.md "wikilink")。最初的形狀是六芒，每一芒都像是由中央散開的淚珠。因此，有些電腦界的圈子稱之為「**splat**」（[狀聲詞](../Page/狀聲詞.md "wikilink")，類似中文的「啪」），或許是因為許多早期的[列式印表機印出來的星號看起來像是被壓扁的蟲子](../Page/列式印表機.md "wikilink")。

很多文化有自己獨特的星號。[中国与](../Page/中国.md "wikilink")[日本用的是](../Page/日本.md "wikilink")「※」，看起來像是[漢字的](../Page/漢字.md "wikilink")「[米](../Page/米.md "wikilink")」。[阿拉伯式的星號是](../Page/阿拉伯.md "wikilink")[六芒星](../Page/六芒星.md "wikilink")。於某些[字體](../Page/字體.md "wikilink")，星號是[五芒](../Page/五芒星.md "wikilink")，阿拉伯星號則為八芒。

## 用法

### 書面文字

  - [註腳](../Page/註腳.md "wikilink")

  - 。例如："what \*would\* sound good to you?"

  - 有時可用一個或數個星號取代一個單字的部分字母（尤其是[髒話](../Page/髒話.md "wikilink")），緩和可能冒犯他人的語氣；在网络时代，也有规避[关键词过滤的功能](../Page/关键词过滤.md "wikilink")。譬如：[f\*\*\*](../Page/fuck.md "wikilink")

  - 製作[列表時](../Page/列表.md "wikilink")，星號代表項目。

### 語言學

#### 歷史語言學

在[歷史語言學](../Page/歷史語言學.md "wikilink")，字旁的星號表示這個字是重建而未經證實的歷史形式。

#### 語言學中的衍生學派

  - 在[衍生語法中](../Page/衍生語法.md "wikilink")，特別是句法學的領域，字旁或句旁的星號代表該字或該句不合[語法](../Page/語法.md "wikilink")。

### 電腦科學

  - 在[電腦科學的領域](../Page/電腦科學.md "wikilink")，星號用於[正则表达式來代表零或某個模式](../Page/正则表达式.md "wikilink")（pattern）的重複；这种用法叫做[Kleene星号或](../Page/Kleene星号.md "wikilink")
    Kleene 闭包。得名于[克莱尼](../Page/克莱尼.md "wikilink")。
      - 在[统一建模语言](../Page/统一建模语言.md "wikilink")，星号用来指示零到多个类。

### 電腦介面

  - 某些電腦介面，例如是[Unix
    shell及](../Page/Unix_shell.md "wikilink")[微軟的](../Page/微軟.md "wikilink")[命令提示字元](../Page/命令提示字元.md "wikilink")([Command
    prompt](../Page/Command_prompt.md "wikilink"))，星號是[通配符及任何一種](../Page/通配符.md "wikilink")[字符](../Page/字符.md "wikilink")。俗稱「萬用字元」。一種常見的萬用字元的用途為在電腦上尋找一個檔案。舉例：使用者想尋找一個檔案名為`Document 1`，搜索字詞可以以`Doc*`及`D*ment*`找到該檔案。

### 程式語言

  - 很多[编程语言和](../Page/编程语言.md "wikilink")[计算器使用星号作为](../Page/计算器.md "wikilink")[乘法符号](../Page/乘法.md "wikilink")。在特定语言中它还有一些特定意义，例如：
      - 在[C语言與](../Page/C语言.md "wikilink")[C++中](../Page/C++.md "wikilink")，星号被用来获得[指针的内容](../Page/指针.md "wikilink")。它是得到变量地址的
        *&* 算子的逆运算。它还被用来声明指针变量。
      - 在 [Common Lisp](../Page/Common_Lisp.md "wikilink")
        编程语言，[全局变量的名字按惯例陪衬上星号](../Page/全局变量.md "wikilink")，`*LIKE-THIS*`。

### 數學

星號在[數學上頻繁使用](../Page/數學.md "wikilink")，當中包括：

  - [复数的](../Page/复数.md "wikilink")[共轭复数](../Page/共轭复数.md "wikilink")（更常用的表示法是
    \(\bar{z}\)）。
  - 两个[群的](../Page/群.md "wikilink")[自由积](../Page/自由积.md "wikilink")
  - 任意[二元运算的](../Page/二元运算.md "wikilink")[中缀表示法](../Page/中缀表示法.md "wikilink")。
  - [卷积](../Page/卷积.md "wikilink")，比如 *f  g* 是 *f* 与 *g* 的卷积。
  - 环的乘法群，特别是在这个环是[域的时候](../Page/域_\(数学\).md "wikilink")，比如
    \(\mathbb{C}^* = \mathbb{C}-\{0\}\)
  - 某个集合中任意一点，比如计算[黎曼和或将单连通群收缩为单元素群时](../Page/黎曼和.md "wikilink"){  }。
  - [向量空间](../Page/向量空间.md "wikilink") *V*
    的[对偶空间符号为](../Page/对偶空间.md "wikilink")
    *V\**。
  - 两个[光滑流形之间的](../Page/光滑流形.md "wikilink")[光滑映射](../Page/光滑映射.md "wikilink")
    *f*的[前推记为](../Page/前推.md "wikilink") *f*<sub>\*</sub>。
  - 向量空间 \(*: A^k \rightarrow A^{n-k}\)
    上的[霍奇对偶算子](../Page/霍奇对偶.md "wikilink")。

星号在所有数学分支中也经常用于表示由一个字母表示的两个数学实体之间的对应——一个有星号，一个没有。

### 医学

  - 著名的“[生命之星](../Page/生命之星.md "wikilink")”图案包含一个类似\*号的六角星形。

[Star_of_life2.svg](https://zh.wikipedia.org/wiki/File:Star_of_life2.svg "fig:Star_of_life2.svg")

### 人類遺傳學

  - 人類遺傳學中，[\*是用來表示某一个体属于某一](../Page/*_\(haplogroup\).md "wikilink")[单倍群且不属于其任何子群](../Page/单倍群.md "wikilink")（參看[\*
    (haplogroup)](../Page/*_\(haplogroup\).md "wikilink")）.

### 電話通訊

  - 在[音頻式](../Page/雙音多頻.md "wikilink")[電話](../Page/電話.md "wikilink")，\*（通常稱作**星**鍵，臺灣常俗稱**米字鍵**）是其中一個特別鍵，在0字左邊。
  - [Asterisk](../Page/Asterisk.md "wikilink")
    是免費／開源[軟體式電話](../Page/軟體.md "wikilink")[用戶交換機](../Page/用戶交換機.md "wikilink")
    (PBX).

### 電子郵件、用戶群組、即時通訊

  - 日常中，[純文字電腦文件](../Page/純文字.md "wikilink")（如於[e-mail通訊或](../Page/e-mail.md "wikilink")[新聞組postings](../Page/新聞組.md "wikilink")），星號用於強調（通常由括弧正文和星號
    `*like
    this*`）。通常，這是當其他強調方法，像是[粗體字](../Page/粗體字.md "wikilink")、[斜體字](../Page/斜體字.md "wikilink")，或[底線不能在該系統使用時](../Page/底線.md "wikilink")。星號還會用於這種時尚表現動作（如
    \*jump\* 或
    \*[glomp](../Page/glomp.md "wikilink")\*），尤其文字[角色扮演遊戲](../Page/角色扮演遊戲.md "wikilink")。
  - 在[聊天室和](../Page/聊天室.md "wikilink")[即時通訊](../Page/即時通訊.md "wikilink")，星號有時用於糾正誤字。星號用在句子之前或之後修正。例如：

<!-- end list -->

  -
    <font color="red">Alice:</font> What do yuo think
    <font color="red">Alice:</font> \*you
    <font color="blue">Bob:</font> Wht,
    <font color="blue">Bob:</font> What?\*\*

<!-- end list -->

  - 在寫作測驗，星號可用於列出名單。

### 板球

  - 在電視轉播比賽比賽中，它用作顯示球員的資料。例如47\*是代表比賽中第47名球員。

### 經濟學

### 教育

  - 在[GCSE](../Page/普通中等教育證書.md "wikilink") 考試，A\*（A星）是代表特等級，在A級中比較突出。
  - 在[香港中學會考的中國語文科及英國語文科中](../Page/香港中學會考.md "wikilink")，Level
    5\*是指在Level
    5之中較優的考生；而[香港中學文憑考試全面採用數字評級](../Page/香港中學文憑考試.md "wikilink")，5\*\*（讀作「五星星」）級為最高之評級，5\*（讀作「五星」）緊隨其後，皆為取得5級學生當中成績較突出者。

### 電子遊戲

### 棒球

  - 近年，星號是用來標準一次「良好的防守」。[1](http://baseball-almanac.com/score2b.shtml)

### 賽馬

  - 在賽馬比賽中，一名[騎師獲得特別讓磅時](../Page/騎師.md "wikilink")，會使用星號或其他符號表示馬匹獲得減輕負重的單位。

### 流行文化

  - [Red Hot Chili
    Peppers使用八角星號作為他們的標記](../Page/Red_Hot_Chili_Peppers.md "wikilink")。
  - [男子音樂組合](../Page/男子音樂組合.md "wikilink")[NSYNC經常寫作](../Page/NSYNC.md "wikilink")「N\*SYNC」或「\*NSYNC」。

## 编码

[Unicode中](../Page/Unicode.md "wikilink"):

  - ,

  - , and

  - .\[1\]

类似符号

| Asterisk | Asterisk Operator | Heavy Asterisk | Small Asterisk | Full Width Asterisk | Open Centre Asterisk |
| -------- | ----------------- | -------------- | -------------- | ------------------- | -------------------- |
| \*       | ∗                 | ✱              | ﹡              | ＊                   | ✲                    |

| Low Asterisk | [Arabic star](../Page/Arabic_star.md "wikilink") | East Asian reference mark | Teardrop-Spoked Asterisk | Sixteen Pointed Asterisk |
| ------------ | ------------------------------------------------ | ------------------------- | ------------------------ | ------------------------ |
| ⁎            | ٭                                                | ※                         | ✻                        | ✺                        |

|                                                                                             |                                              |                                      |                                          |                                        |         |
| ------------------------------------------------------------------------------------------- | -------------------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ------- |
| **名称**                                                                                      | **[Unicode](../Page/Unicode.md "wikilink")** | **[十进制](../Page/十进制.md "wikilink")** | **[UTF-8](../Page/UTF-8.md "wikilink")** | **[HTML](../Page/HTML.md "wikilink")** | **显示为** |
| Asterisk                                                                                    | U+002A                                       | \*                                   | 2A                                       | \*                                     | \*      |
| Combining Asterisk Below                                                                    | U+0359                                       | ͙                                    | CD 99                                    |                                        | ͙       |
| [Arabic Five Pointed Star](../Page/Arabic_star.md "wikilink")                               | U+066D                                       | ٭                                    | D9 AD                                    |                                        | ٭       |
| East Asian Reference Mark                                                                   | U+203B                                       | ※                                    | E2 80 BB                                 |                                        | ※       |
| Flower Punctuation Mark                                                                     | U+2055                                       | ⁕                                    | E2 81 95                                 |                                        | ⁕       |
| Asterism                                                                                    | U+2042                                       | ⁂                                    | E2 81 82                                 |                                        | ⁂       |
| Low Asterisk                                                                                | U+204E                                       | ⁎                                    | E2 81 8E                                 |                                        | ⁎       |
| Two Asterisks Aligned Vertically                                                            | U+2051                                       | ⁑                                    | E2 81 91                                 |                                        | ⁑       |
| Combining Asterisk Above                                                                    | U+20F0                                       | ⃰                                    | E2 83 B0                                 |                                        | ⃰       |
| Asterisk Operator                                                                           | U+2217                                       | ∗                                    | E2 88 97                                 | ∗                                      | ∗       |
| Circled Asterisk Operator                                                                   | U+229B                                       | ⊛                                    | E2 8A 9B                                 |                                        | ⊛       |
| Four Teardrop-Spoked Asterisk                                                               | U+2722                                       | ✢                                    | E2 9C A2                                 |                                        | ✢       |
| Four Balloon-Spoked Asterisk                                                                | U+2723                                       | ✣                                    | E2 9C A3                                 |                                        | ✣       |
| Heavy Four Balloon-Spoked Asterisk                                                          | U+2724                                       | ✤                                    | E2 9C A4                                 |                                        | ✤       |
| Four Club-Spoked Asterisk                                                                   | U+2725                                       | ✥                                    | E2 9C A5                                 |                                        | ✥       |
| Heavy Asterisk                                                                              | U+2731                                       | ✱                                    | E2 9C B1                                 |                                        | ✱       |
| Open Centre Asterisk                                                                        | U+2732                                       | ✲                                    | E2 9C B2                                 |                                        | ✲       |
| Eight Spoked Asterisk                                                                       | U+2733                                       | ✳                                    | E2 9C B3                                 |                                        | ✳       |
| Sixteen Pointed Asterisk                                                                    | U+273A                                       | ✺                                    | E2 9C BA                                 |                                        | ✺       |
| Teardrop-Spoked Asterisk                                                                    | U+273B                                       | ✻                                    | E2 9C BB                                 |                                        | ✻       |
| Open Centre Teardrop-Spoked Asterisk                                                        | U+273C                                       | ✼                                    | E2 9C BC                                 |                                        | ✼       |
| Heavy Teardrop-Spoked Asterisk                                                              | U+273D                                       | ✽                                    | E2 9C BD                                 |                                        | ✽       |
| Heavy Teardrop-Spoked Pinwheel Asterisk                                                     | U+2743                                       | ❃                                    | E2 9D 83                                 |                                        | ❃       |
| Balloon-Spoked Asterisk                                                                     | U+2749                                       | ❉                                    | E2 9D 89                                 |                                        | ❉       |
| Six Teardrop-Spoked Propeller Asterisk                                                      | U+274A                                       | ❊                                    | E2 9D 8A                                 |                                        | ❊       |
| Heavy Eight Teardrop-Spoked Propeller Asterisk                                              | U+274B                                       | ❋                                    | E2 9D 8B                                 |                                        | ❋       |
| Squared Asterisk                                                                            | U+29C6                                       | ⧆                                    | E2 A7 86                                 |                                        | ⧆       |
| Equals With Asterisk                                                                        | U+2A6E                                       | ⩮                                    | E2 A9 AE                                 |                                        | ⩮       |
| Slavonic Asterisk                                                                           | U+A673                                       | ꙳                                    | EA 99 B3                                 |                                        | ꙳       |
| Small Asterisk                                                                              | U+FE61                                       | ﹡                                    | EF B9 A1                                 |                                        | ﹡       |
| Full Width Asterisk                                                                         | U+FF0A                                       | ＊                                    | EF BC 8A                                 |                                        | ＊       |
| [Music Symbol Pedal Up Mark](../Page/Sustain_pedal.md "wikilink")                           | U+1D1AF                                      | 𝆯                                    | F0 9D 86 AF                              |                                        | 𝆯       |
| [Tag](../Page/Plane_\(Unicode\)#Supplementary_Special-purpose_Plane.md "wikilink") Asterisk | U+E002A                                      | 󠀪                                    | F3 A0 80 AA                              |                                        |         |
| Light Five Spoked Asterisk                                                                  | U+1F7AF                                      | 🞯                                    | F0 9F 9E AF                              |                                        | 🞯       |
| Medium Five Spoked Asterisk                                                                 | U+1F7B0                                      | 🞰                                    | F0 9F 9E B0                              |                                        | 🞰       |
| Bold Five Spoked Asterisk                                                                   | U+1F7B1                                      | 🞱                                    | F0 9F 9E B1                              |                                        | 🞱       |
| Heavy Five Spoked Asterisk                                                                  | U+1F7B2                                      | 🞲                                    | F0 9F 9E B2                              |                                        | 🞲       |
| Very Heavy Five Spoked Asterisk                                                             | U+1F7B3                                      | 🞳                                    | F0 9F 9E B3                              |                                        | 🞳       |
| Extremely Heavy Five Spoked Asterisk                                                        | U+1F7B4                                      | 🞴                                    | F0 9F 9E B4                              |                                        | 🞴       |
| Light Six Spoked Asterisk                                                                   | U+1F7B5                                      | 🞵                                    | F0 9F 9E B5                              |                                        | 🞵       |
| Medium Six Spoked Asterisk                                                                  | U+1F7B6                                      | 🞶                                    | F0 9F 9E B6                              |                                        | 🞶       |
| Bold Six Spoked Asterisk                                                                    | U+1F7B7                                      | 🞷                                    | F0 9F 9E B7                              |                                        | 🞷       |
| Heavy Six Spoked Asterisk                                                                   | U+1F7B8                                      | 🞸                                    | F0 9F 9E B8                              |                                        | 🞸       |
| Very Heavy Six Spoked Asterisk                                                              | U+1F7B9                                      | 🞹                                    | F0 9F 9E B9                              |                                        | 🞹       |
| Extremely Heavy Six Spoked Asterisk                                                         | U+1F7BA                                      | 🞺                                    | F0 9F 9E BA                              |                                        | 🞺       |
| Light Eight Spoked Asterisk                                                                 | U+1F7BB                                      | 🞻                                    | F0 9F 9E BB                              |                                        | 🞻       |
| Medium Eight Spoked Asterisk                                                                | U+1F7BC                                      | 🞼                                    | F0 9F 9E BC                              |                                        | 🞼       |
| Bold Eight Spoked Asterisk                                                                  | U+1F7BD                                      | 🞽                                    | F0 9F 9E BD                              |                                        | 🞽       |
| Heavy Eight Spoked Asterisk                                                                 | U+1F7BE                                      | 🞾                                    | F0 9F 9E BE                              |                                        | 🞾       |
| Very Heavy Eight Spoked Asterisk                                                            | U+1F7BF                                      | 🞿                                    | F0 9F 9E BF                              |                                        | 🞿       |

## 參見

  - [阿拉伯文星號](../Page/阿拉伯文星號.md "wikilink")
  - [Star (字體)](../Page/Star_\(字體\).md "wikilink")

[Category:標點符號](../Category/標點符號.md "wikilink")

1.