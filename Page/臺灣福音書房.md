**臺灣福音書房**（），簡稱**TWGBR**，是[李常受于](../Page/李常受.md "wikilink")1949年受[倪柝声差遣前往](../Page/倪柝声.md "wikilink")[台湾开展工作后创办的一个有广泛影响的著名的](../Page/台湾.md "wikilink")[基督教](../Page/基督教.md "wikilink")[出版机构](../Page/出版.md "wikilink")，专门出版倪柝声与李常受的著作。地址在[台北市](../Page/台北市.md "wikilink")[中正區](../Page/中正區_\(臺北市\).md "wikilink")[金山南路一段](../Page/金山南路.md "wikilink")72号（[台北市召会第一聚会所](../Page/台北市召会.md "wikilink")），並於台北市[信義區](../Page/信義區_\(臺北市\).md "wikilink")[信義路四段](../Page/信義路.md "wikilink")460號22樓（[信基大樓](../Page/信基大樓.md "wikilink")）設有編輯部。

## 歷史

1949年，李常受成立臺灣福音書房。1978年，台湾福音书房正式改组为「[财团法人台湾福音书房](../Page/财团法人.md "wikilink")」。台湾福音书房[发行人](../Page/发行人.md "wikilink")[劉遂现定居](../Page/劉遂.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")，日常事务由[吴有成](../Page/吴有成.md "wikilink")、[朱摩西](../Page/朱摩西.md "wikilink")、[李光弘及](../Page/李光弘.md "wikilink")[林天德等负责](../Page/林天德.md "wikilink")。

台湾福音书房的前身可以追溯到1924年倪柝声首创的[福州](../Page/福州.md "wikilink")[罗星塔福音书房](../Page/罗星塔福音书房.md "wikilink")。1927年倪柝声定居[上海以后](../Page/上海.md "wikilink")，将罗星塔福音书房一并迁往上海，改名[上海福音书房](../Page/上海福音书房.md "wikilink")。上海福音书房在1928年至1948年一直在[哈同路](../Page/哈同路.md "wikilink")240弄（[文德-{里}-](../Page/文德里_\(上海\).md "wikilink")），1948年迁往南阳路145号新建的大会所。上海福音书房先后出版的百余种书报（绝大部分都是倪柝声的著作），对于[地方教会在各地的兴起和坚固](../Page/地方教会.md "wikilink")，所起的作用不可低估。上海福音书房的编辑主要是[李渊如](../Page/李渊如.md "wikilink")。上海福音书房一直存在到1958年[中国大陆](../Page/中国大陆.md "wikilink")[基督教开始](../Page/基督教.md "wikilink")[联合礼拜的前夕](../Page/联合礼拜.md "wikilink")。

倪柝声、李常受二人的著作不仅数量繁多，而且内容远较一般基督教著作深奥，注重信徒内里[属灵生命的功课](../Page/属灵.md "wikilink")、高峰真理、教会团体事奉与建造等。其中大部头的套装书就有《倪柝声文集》、《新旧约生命读经》、《新约总论》、《真理课程》及《生命課程》等等。

2010年1月11日，臺灣福音書房營業部及庫房遷至第二門市（台北市[松山區](../Page/松山區_\(臺北市\).md "wikilink")[健康路](../Page/健康路_\(臺北市\).md "wikilink")166號）。

2014年6月10日，臺灣福音書房發布由負責人[吳有成署名的公開聲明](../Page/吳有成.md "wikilink")，抨擊[中國反邪教協會等單位把](../Page/中國反邪教協會.md "wikilink")[地方召會及臺灣福音書房與](../Page/地方召會.md "wikilink")[呼喊派連在一起](../Page/呼喊派.md "wikilink")，強調與呼喊派無涉，支持[中華人民共和國政府依法取締](../Page/中華人民共和國政府.md "wikilink")[邪教](../Page/邪教.md "wikilink")。\[1\]

2017年，臺灣福音書房第二門市遷至[信基大樓一樓](../Page/信基大樓.md "wikilink")。

## 門市

  - 第一門市

[臺北市](../Page/臺北市.md "wikilink")[中正區](../Page/中正區_\(臺北市\).md "wikilink")[金山南路一段](../Page/金山南路.md "wikilink")72號

  - 第二門市

[臺北市](../Page/臺北市.md "wikilink")[信義區](../Page/信義區_\(臺北市\).md "wikilink")[信義路四段](../Page/信義路.md "wikilink")460號1樓

  - 提貨庫房

[臺北市](../Page/臺北市.md "wikilink")[松山區](../Page/松山區_\(臺北市\).md "wikilink")[健康路](../Page/健康路_\(臺北市\).md "wikilink")166號

## 參考資料

## 外部連結

  - [臺灣福音書房](http://www.twgbr.org.tw/)

  -
  - [水深之處福音網站](http://www.luke54.org/#axzz1BlyA0f7c/)

  - [網上聖經](http://www.recoveryversion.com.tw/Style0A/026/bible_menu.php/)

  - [LSM Chinese 水流職事站](http://www.lsmchinese.org/)

[T臺](../Category/台灣召會.md "wikilink")
[T臺](../Category/台灣出版社.md "wikilink")
[Category:臺灣基督教媒體](../Category/臺灣基督教媒體.md "wikilink")
[T臺](../Category/被防火长城封锁的网站.md "wikilink")
[T臺](../Category/1949年建立的組織.md "wikilink")
[Category:台北市組織](../Category/台北市組織.md "wikilink")
[Category:總部位於臺北市中正區的工商業機構](../Category/總部位於臺北市中正區的工商業機構.md "wikilink")

1.  [基督教论坛报2014年6月11日报道：“召会严重抗议在大陆被误导为呼喊派”](http://www.ct.org.tw/news/detail/2014-01614)