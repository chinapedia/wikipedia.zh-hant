978 [法屬聖馬丁](../Page/法屬聖馬丁.md "wikilink"){{.w}}986
[瓦利斯和富圖納](../Page/瓦利斯和富圖納.md "wikilink"){{.w}}987
[法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink")

` |group3 = 特殊地位`
` |list3 = 988 `[`新喀里多尼亞`](../Page/新喀里多尼亞.md "wikilink")`（待独立公投决定地位）`

` |group4 = 984 `[`法属南方和`
`南极洲领地`](../Page/法属南部和南极领地.md "wikilink")
` |list4 = `[`法属印度洋诸岛`](../Page/法属印度洋诸岛.md "wikilink")`（`[`印度礁`](../Page/印度礁.md "wikilink")`、`[`欧罗巴岛`](../Page/欧罗巴岛.md "wikilink")`、`[`格洛里厄斯群岛`](../Page/格洛里厄斯群岛.md "wikilink")`、`[`新胡安島`](../Page/新胡安島.md "wikilink")`、`[`特罗姆兰岛`](../Page/特罗姆兰岛.md "wikilink")`）{{.w}}`[`阿姆斯特丹岛`](../Page/阿姆斯特丹岛.md "wikilink")`{{.w}}`[`圣保罗岛`](../Page/圣保罗岛.md "wikilink")`{{.w}}`[`克罗泽群岛`](../Page/克罗泽群岛.md "wikilink")`{{.w}}`[`凯尔盖朗群岛`](../Page/凯尔盖朗群岛.md "wikilink")`{{.w}}`[`阿黛利地`](../Page/阿黛利地.md "wikilink")

` |group5style = background: #ececec;`
` |group5 = 国有`[`荒岛`](../Page/荒岛.md "wikilink")
` |list5 = 989 `[`克利珀頓島`](../Page/克利珀頓島.md "wikilink")`（隶属于`[`法屬玻里尼西亞`](../Page/法屬玻里尼西亞.md "wikilink")`）`

` |below = 参见：`[`法蘭西殖民帝國`](../Page/法蘭西殖民帝國.md "wikilink")
` }}`

}}<noinclude>

</noinclude>

[法国行政区划模板](../Category/法国行政区划模板.md "wikilink")
[\*‎](../Category/法国行政区划.md "wikilink")
[\*‎](../Category/法国省份.md "wikilink")
[France](../Category/各国二级行政区划导航模板.md "wikilink")