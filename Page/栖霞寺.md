**栖霞寺**位于中国[江苏省](../Page/江苏省.md "wikilink")[南京市](../Page/南京市.md "wikilink")[栖霞区](../Page/栖霞区.md "wikilink")[栖霞山西麓](../Page/栖霞山.md "wikilink")，始建于南北朝时期，梁僧朗于此大弘三论教义，被称为江南三论宗初祖，隋文帝于八十三州造舍利塔，其立舍利塔诏以蒋州栖霞寺为首。唐代时称功德寺，规模浩大，与山东长清的灵岩寺、湖北荆山的玉泉寺，浙江天台的国清寺，并称天下四大丛林。栖霞寺于1983年被确定为[汉族地区全国重点寺院](../Page/1983年汉族地区中国重点寺院.md "wikilink")，2002年被列为[江苏省文物保护单位](../Page/江苏省文物保护单位.md "wikilink")。寺内的隋代[舍利塔于](../Page/栖霞寺舍利塔.md "wikilink")1988年被列为[第三批全国重点文物保护单位](../Page/第三批全国重点文物保护单位.md "wikilink")，寺旁的[千佛崖石窟及](../Page/千佛崖石窟_\(南京\).md "wikilink")[明征君碑于](../Page/明征君碑.md "wikilink")2001年被列为[第五批全国重点文物保护单位](../Page/第五批全国重点文物保护单位.md "wikilink")。

## 历史沿革

始建于[南北朝](../Page/南北朝.md "wikilink")[齊永明七年](../Page/南齊.md "wikilink")（489年），原為南齊隱士[明僧紹的私宅](../Page/明僧紹.md "wikilink")。後宅舍由[智度禪師主持](../Page/智度禪師.md "wikilink")，是江南佛教“[三論宗](../Page/三論宗.md "wikilink")”的發源地。[唐代時稱功德寺](../Page/唐.md "wikilink")，規模浩大，與山東長清的[靈岩寺](../Page/靈岩寺.md "wikilink")、湖北荊山的[玉泉寺](../Page/玉泉寺.md "wikilink")，浙江天台的[國清寺](../Page/國清寺.md "wikilink")，並稱**天下四大叢林**。

1983年被确定为[汉族地区全国重点寺院](../Page/1983年汉族地区中国重点寺院.md "wikilink")，同年创建[中国佛学院栖霞山分院](../Page/中国佛学院栖霞山分院.md "wikilink")。

## 建筑

寺內主要建築有山門、彌勒佛殿、毗盧寶殿、法堂、念佛堂、藏經樓、過海大師紀念堂、舍利石塔。寺前有明徽君碑，寺後有千佛岩等眾多名勝。

### [栖霞寺舍利塔](../Page/栖霞寺舍利塔.md "wikilink")

栖霞寺舍利塔建于[隋代隋文帝时期](../Page/隋代.md "wikilink")，为八角五层石塔。1988年被列入第三批[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

### [千佛崖石窟](../Page/千佛崖石窟_\(南京\).md "wikilink")

[Mural_Painting_in_Qixia_Temple.JPG](https://zh.wikipedia.org/wiki/File:Mural_Painting_in_Qixia_Temple.JPG "fig:Mural_Painting_in_Qixia_Temple.JPG")\]\]
千佛崖石窟始凿于[南齐](../Page/南齐.md "wikilink")[永明七年](../Page/永明.md "wikilink")（公元489年），历经各代凿建，现有佛像515尊。栖霞[飞天壁画在](../Page/飞天_\(佛教\).md "wikilink")2000年进行的考古研究中被发现，为目前千佛岩内唯一发现的、保存完好的壁画。

[File:Qixiasi01.jpg|栖霞山下的牌坊](File:Qixiasi01.jpg%7C栖霞山下的牌坊)
[File:Qixiasi00.jpg|寺门](File:Qixiasi00.jpg%7C寺门)
[File:Qixiasi05.jpg|毗卢宝殿](File:Qixiasi05.jpg%7C毗卢宝殿)
<File:Qixiasi09.jpg>|[千佛崖石窟](../Page/千佛崖石窟.md "wikilink") <File:Gate> of
Qixia Temple nanjing.jpg|正门 <File:Statue1> of Qianfoya
Nanjing.jpg|千佛崖造像一座 <File:FangZhang> Qixia Temple.jpg|方丈
<File:WanDeZhuangYan> Qixia
Temple.jpg|“万德庄严”，[赵朴初题](../Page/赵朴初.md "wikilink")

## 相关事件

1937年[南京大屠殺期間约有](../Page/南京大屠殺.md "wikilink")2萬名难民被撤离到栖霞寺。

2005年8月，以南京大屠杀期间栖霞寺求助难民为主题的电影《[栖霞寺1937](../Page/栖霞寺1937.md "wikilink")》发布。

## 外部链接

  - [栖霞寺网站](http://www.njqixiasi.com/)

[Category:南京佛寺](../Category/南京佛寺.md "wikilink")
[宁](../Category/江蘇省文物保護單位.md "wikilink")
[Q栖](../Category/汉族地区佛教全国重点寺院.md "wikilink")
[Category:1919年完工的宗教建築物](../Category/1919年完工的宗教建築物.md "wikilink")
[Category:1919年中國建立](../Category/1919年中國建立.md "wikilink")