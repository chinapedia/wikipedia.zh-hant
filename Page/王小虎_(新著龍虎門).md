[香港漫畫](../Page/香港漫畫.md "wikilink")《[新著龍虎門](../Page/新著龍虎門.md "wikilink")》的故事主角，故事從18歲的**王小虎**從[廣東來到](../Page/廣東.md "wikilink")[香港找尋失蹤的大伯開始](../Page/香港.md "wikilink")，前期內容主要圍繞在王小虎和一班弟兄與香港[黑社會間鬥毆的過程](../Page/黑社會.md "wikilink")，後來捲入日本黑道羅煞教與韓國白蓮教之間的紛爭，接著又到泰國通天教搭救朋友石黑龍，最新的劇情則是著重在日本兩派[忍者伊賀與甲賀之間的決鬥](../Page/忍者.md "wikilink")。漫畫初期設定王小虎的個性為冷靜果敢、重情重義、嫉惡如仇。

### 內功招式

  - [九陽神功](../Page/九陽神功.md "wikilink") ( 第九陽 ) 、
    [九陽神功](../Page/九陽神功.md "wikilink") ( 十陽境界 ) 、
    [九陽神功](../Page/九陽神功.md "wikilink") ( 十二成功力 )
    、[降龍伏虎氣功](../Page/降龍伏虎氣功.md "wikilink") 。

### 武功招式

  - ( 王家 )
    [降龍十八腿](../Page/降龍十八腿.md "wikilink")、裂頭腳、碎石腳、[裂頭碎石腳](../Page/裂頭碎石腳.md "wikilink")、聚龍一擊、[虎鶴雙形拳](../Page/虎鶴雙形拳.md "wikilink")、
    [電光毒龍鑽](../Page/電光毒龍鑽.md "wikilink")
    、[十陽境界](../Page/十陽境界.md "wikilink")、
    龍捲風、飞龙在天、横扫千军、兩極掌、兩極手、滾地龍、蟠龍雙飛、[九陽_裂頭腳](../Page/九陽_裂頭腳.md "wikilink")、[九陽_小毒龍鑽](../Page/九陽_小毒龍鑽.md "wikilink")、[九陽_電光毒龍鑽](../Page/九陽_電光毒龍鑽.md "wikilink")、飛龍壓頂、還魂飽鶴、倒踢龍捲風、倒轉龍捲風、青龍三出洞
    。

### 自創招式

  - [雷電神腿](../Page/雷電神腿.md "wikilink")。

### 移動身法

  - [蛇形電步](../Page/蛇形電步.md "wikilink")。

### 死於王小虎腳下的犧牲者(依漫畫時間順序)

  - [大龍頭](../Page/大龍頭.md "wikilink")：
    身負[西藏](../Page/西藏.md "wikilink")[密宗的佛家神功](../Page/密宗.md "wikilink")「[大力金剛功](../Page/大力金剛功.md "wikilink")」，[油麻地和](../Page/油麻地.md "wikilink")[尖沙嘴的黑幫老大](../Page/尖沙嘴.md "wikilink")，被王小虎眾人圍毆至死。
  - [王海蛟](../Page/王海蛟.md "wikilink")：
    王小虎三叔，[韓國邪拳道場的場主](../Page/韓國.md "wikilink")，為人心狠手辣，打死親哥哥[王降龍](../Page/王降龍.md "wikilink")，被王小虎打死於韓國邪拳道場。
  - [火魍](../Page/火魍.md "wikilink")：
    五昧法師的手下，被王小虎打死於日本[中華武館](../Page/中華武館.md "wikilink")。
  - [五昧法師](../Page/五昧法師.md "wikilink")：[泰國華僑](../Page/泰國.md "wikilink")，精通神打，[泰國首席暗殺高手](../Page/泰國.md "wikilink")，被香港黑幫份子翻江蛟請回[日本刺殺](../Page/日本.md "wikilink")[羅剎教的](../Page/羅剎教.md "wikilink")[老妖](../Page/老妖.md "wikilink")，被王小虎打死於日本中華武館。
  - [玄災](../Page/玄災.md "wikilink")：日本天地寺的奇門三尊，在日本街頭被王小虎和[石黑龍圍毆至死](../Page/石黑龍.md "wikilink")。
  - [暴君龍](../Page/暴君龍.md "wikilink")：[越南暗殺集團](../Page/越南.md "wikilink")[首領](../Page/首領.md "wikilink")，在[澳門聖母堂一戰被王小虎以惡龍纏溺死於水中](../Page/澳門.md "wikilink")。
  - [烏鴉星](../Page/烏鴉星.md "wikilink")：越南暗殺集團手下，在澳門聖母堂一戰被王小虎打死。
  - [玄劫](../Page/玄劫.md "wikilink")：日本天地寺的奇門三尊，在澳門聖母堂一戰被王小虎以霹靂狂龍打死。
  - [天妖](../Page/天妖.md "wikilink")：日本伊賀派的四大上忍之末，被王小虎打死於伊賀城。
  - [伊賀大聖](../Page/伊賀大聖.md "wikilink")：日本伊賀派手下，[神氣道創始人](../Page/神氣道.md "wikilink")，被王小虎打死於[伊賀城](../Page/伊賀城.md "wikilink")。
  - [虎尊者](../Page/虎尊者.md "wikilink")：密宗活佛第三弟子，被王小虎打死於[甲賀城外的加油站](../Page/甲賀城.md "wikilink")。

## 參考資料

  - [王小虎
    新著龍虎門](https://books.google.com.hk/books?id=xXkQBgAAQBAJ&pg=PA67&lpg=PA67&dq=%E7%8E%8B%E5%B0%8F%E8%99%8E+%E6%96%B0%E8%91%97%E9%BE%8D%E8%99%8E%E9%96%80&source=bl&ots=IXQgfmzwHy&sig=lIT0CPFgKV-Sn6RqzCeP1hZMIzM&hl=zh-TW&sa=X&ved=0CGUQ6AEwDWoVChMI9um1g-7xxgIVSiWUCh03JQKj#v=onepage&q=%E7%8E%8B%E5%B0%8F%E8%99%8E%20%E6%96%B0%E8%91%97%E9%BE%8D%E8%99%8E%E9%96%80&f=false)，香港嘅廣東文化，ISBN
    9789620765261，第67頁

[category:香港漫畫角色](../Page/category:香港漫畫角色.md "wikilink")