[Okurayama_August_2007.jpg](https://zh.wikipedia.org/wiki/File:Okurayama_August_2007.jpg "fig:Okurayama_August_2007.jpg")

**大倉山跳台競技場**（，）是一個位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[中央區宮之森的標準](../Page/中央區_\(札幌市\).md "wikilink")[跳台滑雪場地](../Page/跳台滑雪.md "wikilink")，其所處位置是高達307[米的大倉山東斜面](../Page/米_\(單位\).md "wikilink")，是日本國內舉行過最多次跳台滑雪競技大賽的場所。場地內最多可容納約三萬名觀眾。

## 歷史

1931年，在得到日本[昭和天皇的親弟](../Page/昭和天皇.md "wikilink")[秩父宮雍仁親王的美言及資助之下](../Page/秩父宮雍仁親王.md "wikilink")，[大倉喜七郎男爵動用私人財產建設了大倉山跳台競技場](../Page/大倉喜七郎.md "wikilink")，並送贈予札幌市。1932年舉行開場儀式，當時的札幌市長橋本正治命名場地為「大倉Schanze」（Schanze是[德語](../Page/德語.md "wikilink")「跳台」的意思）。負責設計及監修此場地的是一位曾為[1928年冬季奧林匹克運動會](../Page/1928年冬季奧林匹克運動會.md "wikilink")[挪威隊伍作監督](../Page/挪威.md "wikilink")，並具有專業而權威的設計才能的[奧拉夫·希路捷特中尉所擔任](../Page/奧拉夫·希路捷特.md "wikilink")。\[1\]

1970年以來，日本投入大筆經費，改建跳台競技場為可收容五萬觀眾的場所，目的是為了迎接在1972年舉辦的[札幌冬季奧運會](../Page/1972年冬季奧林匹克運動會.md "wikilink")，並在當時正式改名為「大倉山跳台競技場」。結果，在該屆冬季奧運會中，大倉山跳台競技場用作進行90米跳台滑雪比賽的場地。後來該場地仍不斷進行改善工程，務求使跳台場地更合符國際滑雪聯盟的要求，並能於[夏季進行跳台滑草比賽](../Page/夏季.md "wikilink")。1982年在大倉山頂加裝升降吊車，方便遊客及比賽者上落跳台。

## 備註

## 外部連結

  - [大倉山跳台競技場 - 札幌大倉山展望台](http://okura.sapporo-dc.co.jp/jump/)

  - [大倉山跳台競技場 -
    Rera-Sapporo.com](http://rera-sapporo.com/sapporo/ookurayamawalk.html)

[分類:2017年亞洲冬季運動會比賽場館](../Page/分類:2017年亞洲冬季運動會比賽場館.md "wikilink")

[Category:札幌市建築物](../Category/札幌市建築物.md "wikilink")
[Category:奧運跳台滑雪場](../Category/奧運跳台滑雪場.md "wikilink")
[Category:中央區 (札幌市)](../Category/中央區_\(札幌市\).md "wikilink")

1.  [大倉山跳台競技場建設歷史](http://okura.sapporo-dc.co.jp/history/)