**上海中医药大学**（英文名：Shanghai University of Traditional Chinese Medicine，缩写为
SHUTCM），简称**上中医**，是中华人民共和国诞生后国家首批建立的四所中医药高等院校之一。前身是创办于1916年的**上海中医专科学校**，中华人民共和国建國後，1956年成立**上海中医学院**，1993年更现名。學校位於浦東張江高科技園區科研教育區內，佔地500餘畝，學生約9000餘人。该校为[上海市人民政府与](../Page/上海市人民政府.md "wikilink")[中华人民共和国教育部](../Page/中华人民共和国教育部.md "wikilink")、[国家卫生计生委](../Page/国家卫生计生委.md "wikilink")、[国家中医药管理局共建高校](../Page/国家中医药管理局.md "wikilink")。

## 学院

## 附属医院

目前有3所直属附属医院：

  - [龙华医院](http://www.longhua.net/)
  - [曙光医院](http://www.sgyy.cn/cn/index/)
  - [岳阳中西医结合医院](https://www.shyueyanghospital.com/)

4所非直属附属医院：

  - [普陀区中心医院](https://web.archive.org/web/20180101065738/http://www.sptdch.cn/)
  - [上海市中医医院](http://szy.sh.cn/)
  - [上海市第七人民医院](http://shdqrmyy.ecaihr.com/)
  - [上海市中西医结合医院](http://www.stcmih.com/)

## 学科

学校实行研究院与学校合署的管理体制，上海市中医药研究院是全国七大中医药研究中心之一。

学校现有国家重点学科4个：[中医外科学](../Page/中医外科学.md "wikilink")、[中药学](../Page/中药学.md "wikilink")、[中医内科学及](../Page/中医内科学.md "wikilink")[中医骨伤科学](../Page/中医骨伤科学.md "wikilink")，国家重点学科（培育）2个：[中医医史文献学](../Page/中医医史文献学.md "wikilink")、[针灸推拿学](../Page/针灸推拿学.md "wikilink")

## 图书馆

创立于1956年学校建校之初，馆藏文献总量90余万册。

中医古籍特藏是一大特色，收藏金元至民国年间的中医古籍达32000册，尤以特藏类中医古籍善本为特点，计1114部6200册，主要为明清刻本、日本刻本、抄校本，其中金、元、明刻本200余部，1500册，中医药抄本馆藏达千余种。

## 博物馆

## 参考文献

  - [1](http://exam.edu.sina.com.cn/collegedb/collegeindex.php?collegeid=281)

## 外部链接

  - [上海中医药大学](http://www.shutcm.edu.cn/)

{{-}}

[Category:中国中医院校](../Category/中国中医院校.md "wikilink")
[Category:上海医学院校](../Category/上海医学院校.md "wikilink")
[Category:1916年創建的教育機構](../Category/1916年創建的教育機構.md "wikilink")
[Category:上海中医药大学](../Category/上海中医药大学.md "wikilink")
[Category:中醫藥院校](../Category/中醫藥院校.md "wikilink")
[Category:一流学科建设高校](../Category/一流学科建设高校.md "wikilink")
[Category:上海市教育委员会](../Category/上海市教育委员会.md "wikilink")