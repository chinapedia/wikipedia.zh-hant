**-{zh-cn:杰弗里·雅各布·艾布拉姆斯; zh-tw:傑佛瑞·賈克柏·亞伯拉罕;
zh-hk:謝菲·雅各·艾布斯}-**（，），常稱**J·J·艾布拉姆斯**（），[猶太人](../Page/猶太人.md "wikilink")，是一名[美國](../Page/美國.md "wikilink")[導演](../Page/電影導演.md "wikilink")、[監製](../Page/電影監製.md "wikilink")、[電視製作人](../Page/電視製作人.md "wikilink")、[編劇和](../Page/編劇.md "wikilink")[作家](../Page/作家.md "wikilink")。他成功創造出[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）的電視影集《[雙面女間諜](../Page/特務A.md "wikilink")》（*Alias*）和《[迷失](../Page/迷失.md "wikilink")》（*Lost*），並和麥特·瑞維斯（Matt
Reeves）合作創造華納的《[大学生费莉希蒂](../Page/大学生费莉希蒂.md "wikilink")》（*Felicity*）。額外一提的是台灣譯名
J. J. 亞伯拉罕其實是誤譯，Abrams 並非 Abraham ，發音上不存在「罕」的近似音，依照往例應譯作艾布蘭才是正確的。

亞伯拉罕出生於[美國](../Page/美國.md "wikilink")[紐約市並在](../Page/紐約市.md "wikilink")[洛杉磯長大](../Page/洛杉磯.md "wikilink")。他在紐約[布隆克維](../Page/布隆克維.md "wikilink")（Bronxville）就讀[莎拉勞倫斯學院](../Page/莎拉勞倫斯學院.md "wikilink")（Sarah
Lawrence College）。他的父親（Gerald W. Abrams）和母親（Carol Abrams）也是製作人。

2006年7月14日，亞伯拉罕分別和[派拉蒙影業及](../Page/派拉蒙影業.md "wikilink")[華納兄弟各簽下五年和六年合約](../Page/華納兄弟.md "wikilink")，價值超過5,500萬[美元](../Page/美元.md "wikilink")（約-{zh-hans:人民币4.3亿元;zh-hk:港幣4.2億元;zh-tw:新台幣17.7億元;}-）\[1\]。2013年1月，跟[華特迪士尼影業簽約執導](../Page/華特迪士尼影業.md "wikilink")《[星際大戰：原力覺醒](../Page/星際大戰七部曲：原力覺醒.md "wikilink")》\[2\]。

## 作品

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名</p></th>
<th><p>原名</p></th>
<th><p>導演</p></th>
<th><p>監製</p></th>
<th><p>編劇</p></th>
<th><p>演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1982</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>作曲家</p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>角色︰Delivery Boy</p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>執行製作人</p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>角色︰Doug</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>角色︰Video Photographer #2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><a href="../Page/絕世天劫.md" title="wikilink">絕世天劫</a></p></td>
<td><p><em>Armageddon</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td></td>
<td><p><em>The Suburbans</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>角色︰Rock Journalist</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td></td>
<td><p><em>Joy Ride</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/職業特工隊3.md" title="wikilink">職業特工隊3</a></p></td>
<td><p><em>Mission: Impossible III</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>首次執導</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/科洛弗檔案.md" title="wikilink">科洛弗檔案</a></p></td>
<td><p><em>Cloverfield</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/星际迷航_(电影).md" title="wikilink">星際爭霸戰</a></p></td>
<td><p><em>Star Trek</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/麻辣女強人.md" title="wikilink">麻辣女強人</a></p></td>
<td><p><em>Morning Glory</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/超級8.md" title="wikilink">超級8</a></p></td>
<td><p><em>Super 8</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/碟中谍4.md" title="wikilink">碟中谍4</a></p></td>
<td><p><em>Mission: Impossible – Ghost Protocol</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/星际迷航：暗黑无界.md" title="wikilink">星际迷航：暗黑无界</a></p></td>
<td><p><em>Star Trek Into Darkness</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>執行製作人</p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/碟中谍5.md" title="wikilink">碟中谍5</a></p></td>
<td><p><em>Mission: Impossible – Rogue Nation</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星球大戰：原力覺醒.md" title="wikilink">星球大戰：原力覺醒</a></p></td>
<td><p><em>Star Wars: The Force Awakens</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>聲音客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/科洛弗10號地窖.md" title="wikilink">科洛弗10號地窖</a></p></td>
<td><p><em>10 Cloverfield Lane</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星際爭霸戰：浩瀚無垠.md" title="wikilink">星際爭霸戰：浩瀚無垠</a></p></td>
<td><p><em>Star Trek Beyond</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/大災難家.md" title="wikilink">大災難家</a></p></td>
<td><p><em>The Disaster Artist</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>他自己</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/STAR_WARS：最後的絕地武士.md" title="wikilink">STAR WARS：最後的絕地武士</a></p></td>
<td><p><em>Star Wars: The Last Jedi</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>執行製片人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p><a href="../Page/科洛弗悖論.md" title="wikilink">科洛弗悖論</a></p></td>
<td><p><em>The Cloverfield Paradox</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/不可能的任務：全面瓦解.md" title="wikilink">不可能的任務：全面瓦解</a></p></td>
<td><p><em>Mission: Impossible – Fallout</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大君主行動_(電影).md" title="wikilink">大君主行動</a></p></td>
<td><p><em>Overlord</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p><a href="../Page/STAR_WARS：天行者的崛起.md" title="wikilink">STAR WARS：天行者的崛起</a></p></td>
<td><p><em>Star Wars: The Rise of Skywalker</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視

  - 《[大学生费莉希蒂](../Page/大学生费莉希蒂.md "wikilink")》，創造者之一、編劇、執行製作
  - 《[特務A](../Page/特務A.md "wikilink")》，創造者、編劇、執行製作
  - 《[迷失](../Page/迷失.md "wikilink")》，創造者之一、編劇、執行製作、導演
  - 《What About Brian》，執行製作
  - 《[-{zh-hans:6ﾟ接触;zh-hk:六度分離;zh-tw:6ﾟ接觸;}-](../Page/6ﾟ接觸.md "wikilink")》，執行製作
  - 《[-{zh-hans:危机边缘; zh-tw:危機邊緣;
    zh-hk:F檔案}-](../Page/F檔案.md "wikilink")》，執行製作
  - 《[恶魔岛](../Page/恶魔岛_\(电视剧\).md "wikilink")》，编剧
  - 《[疑犯追踪](../Page/疑犯追踪.md "wikilink")》，執行製作
  - 《[末日重生](../Page/末日重生.md "wikilink")》，執行製作
  - 《[機器之心](../Page/機器之心.md "wikilink")》，執行製作
  - 《[西方極樂園](../Page/西方極樂園.md "wikilink")》，執行製作\[3\]

### 書籍

  - 《[-{zh-hans:S.;zh-hk:S.;zh-tw:S.;}-](../Page/S.中文版.md "wikilink")》（2011），製作

## 關聯項目

  - [壞機器人製片公司](../Page/壞機器人製片公司.md "wikilink")

## 參考文獻

## 外部連結

  -
  - [The J.J. Abrams Universe](http://www.j-j-abrams.com/)：非官方的支持者網站

[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美國電視編劇](../Category/美國電視編劇.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美国电视剧导演](../Category/美国电视剧导演.md "wikilink")
[Category:美國電視監製](../Category/美國電視監製.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:TED演讲人](../Category/TED演讲人.md "wikilink")
[Category:莎拉勞倫斯學院校友](../Category/莎拉勞倫斯學院校友.md "wikilink")
[Category:猶太導演](../Category/猶太導演.md "wikilink")

1.
2.
3.