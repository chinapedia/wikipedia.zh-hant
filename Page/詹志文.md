**詹志文**（，英文名**Leo Chim**。），曾使用藝名**詹志民**，已婚\[1\]，現任自資數碼營銷顧問公司[Song Do
Nam負責人](../Page/Song_Do_Nam.md "wikilink")，是[香港](../Page/香港.md "wikilink")[商業電台前](../Page/商業電台.md "wikilink")[節目主持人](../Page/節目主持人.md "wikilink")，曾被同台[DJ](../Page/DJ.md "wikilink")[森美戲稱](../Page/森美.md "wikilink")「方人方面方剛」，在《[森美小儀山寨王](../Page/森美小儀山寨王.md "wikilink")》時代，名為Pet
Pet Leo。後來名為「[喪Do男](../Page/喪Do男.md "wikilink")」，之後再有「Leo波」這稱號。

## 簡歷

詹志文中學畢業於[聖公會林護紀念中學](../Page/聖公會林護紀念中學.md "wikilink")，之後就讀[香港中文大學](../Page/香港中文大學.md "wikilink")，主修[工商管理](../Page/工商管理.md "wikilink")\[2\]。在2006年11月與現任妻子結婚\[3\]。

為配合電台節目《[空中喱民](../Page/空中喱民.md "wikilink")》，所以名字由「文」改為「民」。後來用回「詹志文」這中文名。其他DJ稱他"四方果"，一度被[森美在](../Page/森美.md "wikilink")[森美移動中於](../Page/森美移動.md "wikilink")[Yahoo\!搜尋四方果一詞](../Page/Yahoo!.md "wikilink")，成為一時佳話。

2001年當時詹仍在中文大學讀書，但他不喜歡沉悶的辦公室工作，希望成為DJ，於是自己做了一個電台節目，向[俞琤自薦加入](../Page/俞琤.md "wikilink")[商業電台](../Page/商業電台.md "wikilink")\[4\]，於[林海峰的節目](../Page/林海峰_\(香港\).md "wikilink")《[好世界](../Page/好世界.md "wikilink")》中，負責其中一個15分鐘的遊戲環節《早晨經理》\[5\]。其後在[森美及](../Page/森美.md "wikilink")[阮小儀的節目](../Page/阮小儀.md "wikilink")《[森美小儀山寨王](../Page/森美小儀山寨王.md "wikilink")》
中擔任副手，亦隨[卓韻芝學習劇本創作](../Page/卓韻芝.md "wikilink")。由於他和森美及小儀合得來，遂正式加入《森美小儀山寨王》。再加入森美的週末節目《[森美移動](../Page/森美移動.md "wikilink")》，其後更加有個人節目《[Pet
Pet Leo](../Page/Pet_Pet_Leo.md "wikilink")》
，2005年與[張家豐](../Page/張家豐.md "wikilink")（阿喱）合做節目《[空中喱民](../Page/空中喱民.md "wikilink")》，亦曾有個人音樂節目《[Chi
Man's Choice](../Page/Chi_Man's_Choice.md "wikilink")》，開始於商業電台抬頭。

2006年6月12日起與[蘇耀宗](../Page/蘇耀宗.md "wikilink")（細So）及[阿喱主持遊戲節目](../Page/阿喱.md "wikilink")《[大鐵人123](../Page/大鐵人123.md "wikilink")》，節目現已結束。

2007年2月26日開始與[阿喱和](../Page/阿喱.md "wikilink")[高郁斐](../Page/高郁斐.md "wikilink")([有耳非文](../Page/有耳非文.md "wikilink"))合作主持《[有耳喱民](../Page/有耳喱民.md "wikilink")》。同日開始其《[喪Do男](../Page/喪Do男.md "wikilink")》的生涯，喪Do男亦是其成名之作，令他人氣急升，人人皆知。

2008年7月4日與合作多年的[阿喱及另一位節目主持人有耳非文共同主持最後一集](../Page/阿喱.md "wikilink")《有耳喱民》後正式宣佈「喱民」組合解散，並於同年7月7日獨自主持新節目《[Do人show](../Page/Do人show.md "wikilink")》。

2008年11月7日成立Do人Show唱唱團LCK，由**詹志文**(L)和[陳強](../Page/陳強.md "wikilink")(C)及[傑仔](../Page/傑仔.md "wikilink")(K)各自將流行曲部分歌詞改成為香港人打氣的歌曲，並清唱出來，於《Do人Show》節目時段內播出。

2009年4月9日他親自宣佈《Do人Show》隨著903節目調動在2009年4月17日結束\[6\]。翌日他表示《Do人Show》後自己只會主持週六的《[森美移動](../Page/森美移動.md "wikilink")》，結束自己9年在商業電台的工作。一般相信他由長期員工轉成合約員工，只會在準備節目時才會到商台。

其後曾在宣明會做義工\[7\]，之後在 [Prefix Group 尚方](http://prefixgroup.com/)\[8\]
中任職企劃總監\[9\]，主力負責品牌、藝人或文化活動相關的宣傳，以及節目製作統籌及企劃工作\[10\]。

2009年11月，他離職尚方；在[香港職業訓練局任職](../Page/香港職業訓練局.md "wikilink")[市場推廣主任](../Page/市場推廣.md "wikilink")，主攻[網上營銷](../Page/網上營銷.md "wikilink")(Digital
Marketing)，創立「Dream's幫」，拍了多條網上短片，並贏得[Red
Bull亞洲首屆飛行日冠軍](../Page/Red_Bull.md "wikilink")，並奪2011香港資訊及通訊科技獎。

2011年9月，他全時間打理自己公司[Song Do Nam](../Page/Song_Do_Nam.md "wikilink")
([喪Do男](../Page/喪Do男.md "wikilink"))，擔任數碼營銷顧問 (Digital Marketing
Consultant)。

在2013年5月5日的《[兒童適宜](../Page/兒童適宜.md "wikilink")》節目中，詹志文表示會離開[商業電台](../Page/商業電台.md "wikilink")，該集是他最後一集主持的電台節目。

2016年，他除打理自己公司外，亦在[新傳媒擔任創作總監](../Page/新傳媒.md "wikilink")（Creative
Director）。2017年，則轉到[Fevaworks Solutions (Hong Kong)
Limited擔任創作總監](../Page/Fevaworks_Solutions_\(Hong_Kong\)_Limited.md "wikilink")（Creative
Director）。

## 曾主持/參與節目

### 電台節目

  - 《[森美小儀山寨王](../Page/森美小儀山寨王.md "wikilink")》(又名Pet Pet Leo)
  - 《[起身翻騰兩周半](../Page/起身翻騰兩周半.md "wikilink")》\[11\]
  - 《[森美小儀K饉40](../Page/森美小儀K饉40.md "wikilink")》(於2004年7月14日至9月3日星期一至五[下午](../Page/下午.md "wikilink")4時至6時)
  - 《[903
    發開口夢](../Page/903_發開口夢.md "wikilink")》（於2005年8月9日至9月2日星期一至五[晚上](../Page/晚上.md "wikilink")8時至10時）
  - 《[正在叱咤](../Page/正在叱咤.md "wikilink")》（於2006年12月[I Love you
    boyz劇場代](../Page/I_Love_you_boyz.md "wikilink")[萬世巨星時於下午](../Page/萬世巨星.md "wikilink")4時至6時）
  - 《[Chi Man's
    Choice](../Page/Chi_Man's_Choice.md "wikilink")》（[星期六](../Page/星期六.md "wikilink")[凌晨](../Page/凌晨.md "wikilink")1時至2時）
  - 《[Pet Pet
    Leo](../Page/Pet_Pet_Leo.md "wikilink")》（[星期六](../Page/星期六.md "wikilink")[凌晨](../Page/凌晨.md "wikilink")1時至2時）
  - 《[大鐵人123](../Page/大鐵人123.md "wikilink")》（星期一至五[晚上](../Page/晚上.md "wikilink")12時至1時）
  - 《[903 傍住你放榜](../Page/903_傍住你放榜.md "wikilink")》（放榜日前一晚凌晨2時至6時）
  - 《[喱民精選](../Page/喱民精選.md "wikilink")》（[星期六](../Page/星期六.md "wikilink")[下午](../Page/下午.md "wikilink")5時至6時）
  - 《[空中喱民](../Page/空中喱民.md "wikilink")》
    （星期一至五[晚上](../Page/晚上.md "wikilink")8時至10時）
  - 《[有耳喱民](../Page/有耳喱民.md "wikilink")》（星期一至五[凌晨](../Page/凌晨.md "wikilink")12時至2時）
  - 《[Do人show](../Page/Do人show.md "wikilink")》（逢星期一至五[下午](../Page/下午.md "wikilink")9時至10時）
  - 《[森美移動](../Page/森美移動.md "wikilink")》（逢[星期六](../Page/星期六.md "wikilink")[晚上](../Page/晚上.md "wikilink")11時至[凌晨](../Page/凌晨.md "wikilink")1時）
  - 《[兒童適宜](../Page/兒童適宜.md "wikilink")》（逢星期日[下午](../Page/下午.md "wikilink")2時至[下午](../Page/下午.md "wikilink")4時）

### 電視節目

  - 《[花旗筋肉大格鬥](../Page/花旗筋肉大格鬥.md "wikilink")》（[高清翡翠台](../Page/高清翡翠台.md "wikilink")
    逢星期日[晚上](../Page/晚上.md "wikilink")11時半至星期一凌晨12時半）
      - *曾在[翡翠台](../Page/翡翠台.md "wikilink")[星期六](../Page/星期六.md "wikilink")[中午](../Page/中午.md "wikilink")12時至[下午](../Page/下午.md "wikilink")1時重播*
  - 《[爆片王](../Page/爆片王.md "wikilink")》（[nowTV](../Page/nowTV.md "wikilink")
    102觀星台 與[吳日言共同主持](../Page/吳日言.md "wikilink")，逢星期一晚上9時30分至10時 ）

## 曾參與廣播劇

  - 佔爺@《公子日記》
  - 《Model王傳奇》
  - 《大衛營廣播劇》
  - 四方果@《[爆籃無敵](../Page/爆籃無敵.md "wikilink")》
  - 《森美幻想劇》
  - 師傅仔@《[街車王](../Page/街車王.md "wikilink")》
  - 壞同學@《uma與neko》
  - 副隊長、雲吞雞將軍@《宇宙突擊隊之 Uma 與 Neko — 砂鍋雲吞雞》
  - 豹膽民@《[大佬冰室](../Page/大佬冰室.md "wikilink")》
  - 吳水@《[黃金少年](../Page/黃金少年.md "wikilink")》（Season 1 & 2 & 3 & 4）
  - 喪民@《[喪Do男前傳](../Page/有耳喱民#喪Do男前傳.md "wikilink")》
  - L先生@《[Cafe de
    Nath](../Page/有耳喱民#Cafe_de_Nath.md "wikilink")》（於[有耳喱民時段內大約凌晨](../Page/有耳喱民.md "wikilink")1時50分後播出；星期日[乜嘢地獄](../Page/乜嘢地獄.md "wikilink")）
  - 《[森美幻想劇](../Page/森美幻想劇.md "wikilink")》 (星期六
    11:00pm–1:00am，時間不定，通常在[森美移動](../Page/森美移動.md "wikilink")
    尾段播放)
  - 《[笑談廣東話](../Page/笑談廣東話.md "wikilink")》 (聲演：夫人)
  - 《[森美移動](../Page/森美移動.md "wikilink")[音樂機械特勤](../Page/音樂機械特勤.md "wikilink")》(飾演：Leo)
    (逢[星期六](../Page/星期六.md "wikilink")[森美移動時段內播放](../Page/森美移動.md "wikilink")，約於
    11 點 半 至 12 點播放)

## 其他演出

  - 《[我要做Model](../Page/我要做Model.md "wikilink")》飾 模特兒公司職員
  - 《[森之愛情](../Page/森之愛情.md "wikilink")》第九集《十五年的約會》飾 LEO
  - 《[飛砂風中轉](../Page/飛砂風中轉.md "wikilink")》\[12\]
  - 《[关人7事](../Page/关人7事.md "wikilink")》飾 便利店老板

## 出版書籍

  - 《[喪Do男](../Page/喪Do男.md "wikilink")》
      - 記錄喪Do男的製作花絮

<!-- end list -->

  - 《[敗犬男](http://www.enlightenfish.com.hk/bookstore/prose/敗犬男-detail)》
      - 記錄Leo九年的電台工作經歷
      - 第廿三屆中學生好書龍虎榜十本好書 \[13\]

<!-- end list -->

  - 《[敗犬男：好想說再見](http://www.enlightenfish.com.hk/bookstore/prose/敗犬男：好想說再見-detail)》
      - 記述Leo父親患癌的經歷

Leo暫時電台作品有一本, 文字作品有兩本。

## 參考文獻

## 外部連結

  - [商業電台主持簡介](http://www.881903.com/Page/ZH-TW/DJ903_26.aspx)
  - [Leo (= 喪Do男 ) Blog](http://hk.myblog.yahoo.com/leo_chim)
  - [Leo 在 my903.com
    的網誌](https://web.archive.org/web/20120317044241/http://leochim.blog.my903.com/)
  - [喪Do男 在 YouTube 的頻道](http://www.youtube.com/user/songdonam2011)
  - [喪Do男 在 YouTube 的舊頻道](http://www.youtube.com/user/1977727)
  - [Leo 在 facebook 的帳號](http://www.facebook.com/chimchiman)
  - [喪Do男 在 facebook fanspage 的帳號](https://www.facebook.com/songdonam)
  - [Leo 在 facebook fanspage 的帳號](http://www.facebook.com/leoball727)
  - [《空中喱民》官方網站](http://hihi.881903.com)

[Category:播音員](../Category/播音員.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[詹志民](../Category/香港中文大學校友.md "wikilink")
[C](../Category/詹姓.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:聖公會林護紀念中學校友](../Category/聖公會林護紀念中學校友.md "wikilink")

1.

2.

3.
4.
5.

6.

7.

8.

9.

10.
11.

12.

13.