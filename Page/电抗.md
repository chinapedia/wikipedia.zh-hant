在[交流电路](../Page/交流電.md "wikilink")（如[串联](../Page/串联.md "wikilink")[RLC电路](../Page/RLC电路.md "wikilink")）中，**电抗**（）是类似于[直流电路中](../Page/直流電.md "wikilink")[电阻对](../Page/电阻.md "wikilink")[电流的阻碍作用](../Page/电流.md "wikilink")，用於描述[电容及](../Page/电容.md "wikilink")[电感对电流的阻碍作用](../Page/电感.md "wikilink")，其计量单位也是[欧姆](../Page/欧姆.md "wikilink")。在交流电路分析中，电抗用
*X*
表示，是[复数](../Page/复数.md "wikilink")[阻抗的](../Page/阻抗.md "wikilink")[虚数部分](../Page/虚数.md "wikilink")，用于表示电感及电容对电流的阻碍作用。电抗随着交流电路[频率而变化](../Page/频率.md "wikilink")，并引起电路电流与电压的相位变化。

## 分析

阻抗即电阻与电抗的总合，用数学形式表示为：

\[\mathbf{Z} = \mathbf{R} + j \mathbf{X} ,\]

  -

      -
        *Z* 即阻抗，单位为欧姆
        *R* 为电阻，单位为欧姆
        *X* 为电抗，单位为欧姆
        *j* 是虚数单位

<!-- end list -->

  - 当 *X* \> 0 时，称为感性电抗
  - 当 *X* = 0 时，阻抗为纯电阻
  - 当 *X* \< 0 时，称为容性电抗

一般应用中，只需知道阻抗的强度即可：

\[\left\| Z \right\| = \sqrt[]{R^2 + X^2}\]

对电阻为0的理想纯[感抗或](../Page/感抗.md "wikilink")[容抗元件](../Page/容抗.md "wikilink")，阻抗强度就是电抗的大小。

一般电路的总电抗等于：

\[X = X_L - X_C\]

其中 \(X_L\) 为电路的感抗，\(X_C\) 为电路的容抗。

現實中，大部份負載都是電感性，例如：[變壓器和](../Page/變壓器.md "wikilink")[電動機](../Page/電動機.md "wikilink")。定義感抗為正，容抗為負，可以避免負數出現，方便計算。

## 感抗

因为电路中存在电感电路（如[线圈](../Page/线圈.md "wikilink")），由此产生的变化的[电磁场](../Page/电磁场.md "wikilink")，会产生相应的阻碍电流变化的[感生電動勢](../Page/感生電動勢.md "wikilink")
。这个作用称为**感抗** (\(X_L\))
。电流变化越大，即电路[频率越大](../Page/频率.md "wikilink")，感抗越大；当频率变为0，即成为直流电时，感抗也变为0。感抗会引起电流与电压之间的[相位差](../Page/相位差.md "wikilink")。感抗可由下面公式计算而来：

\[X_L = \omega L = 2 \pi f L\]

複數分析中：

\[X_L = j\omega L = j2 \pi f L\]

其中

\[j\] 是复数单位

\[X_L\] 就是感抗，单位为欧姆

\[\omega\]
是[角速度](../Page/角速度.md "wikilink")，单位为[弧度](../Page/弧度.md "wikilink")/秒

\[f\] 是频率，单位为[赫兹](../Page/赫兹.md "wikilink")

\[L\] 是线圈电感，单位为[亨利](../Page/亨利_\(電感\).md "wikilink")

## 容抗

**容抗**的概念反映了[交流电可以通过](../Page/交流电.md "wikilink")[电容器这一特性](../Page/电容器.md "wikilink")，交流电频率越高，容抗越小，即电容的阻碍作用越小。容抗同样会引起电流与电容两端[电压的相位差](../Page/电压.md "wikilink")。當频率等於零，容抗無限大，即[直流電不能流過电容器](../Page/直流電.md "wikilink")。

容抗可由下面公式计算而来：

\[X_C = (\omega C)^{-1} = \frac{1}{\omega C} = \frac{1}{2\pi f C}\]

在交流电的复数分析中，容抗表示为：

\[X_C = (j \omega C)^{-1} = \frac{1}{j\omega C} = -\frac{j}{\omega C} = -\frac{j}{2\pi f C}\]

其中

  - \(j\) 是复数单位
  - \(X_C\) 是容抗，单位为欧姆
  - \(\omega = 2\pi f\) 是角速度，单位为 弧度/每秒
  - \(f\) 是频率，单位为赫兹
  - \(C\) 是电容，单位为[法拉](../Page/法拉.md "wikilink")

## 参考资料

  - Pohl R. W. *Elektrizitätslehre.* – Berlin-Gottingen-Heidelberg:
    Springer-Verlag, 1960.
  - Popov V. P. *The Principles of Theory of Circuits.* – M.: Higher
    School, 1985, 496 p. (In Russian).
  - Küpfmüller K. *Einführung in die theoretische Elektrotechnik,*
    Springer-Verlag, 1959.

[he:עכבה חשמלית\#היגב](../Page/he:עכבה_חשמלית#היגב.md "wikilink")

[D](../Category/電學.md "wikilink") [D](../Category/物理量.md "wikilink")