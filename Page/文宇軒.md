**文宇軒**（，），[香港新聞從業員](../Page/香港.md "wikilink")。

文宇軒1998年畢業於[香港培正中學](../Page/香港培正中學.md "wikilink")\[1\]，後畢業於[香港樹仁學院](../Page/香港樹仁學院.md "wikilink")（現香港樹仁大學）新聞及傳播學系，期間加入[無線新聞實習](../Page/無線新聞.md "wikilink")，畢業後出任港聞記者，2005年加入體育組當記者，2008年畢業於[香港中文大學](../Page/香港中文大學.md "wikilink")，獲新聞學文學碩士\[2\]。

文宇軒現時亦兼任[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")、[晚間新聞及](../Page/晚間新聞.md "wikilink")[香港早晨體育新聞主播](../Page/香港早晨.md "wikilink")。

文宇軒曾於9月16日[颱風山竹襲港其間於](../Page/颱風山竹.md "wikilink")[晚間新聞臨時轉為港聞組記者](../Page/晚間新聞.md "wikilink"),於杏花村進行新聞直播。

## 趣事

  - 2008年9月20日[晚間新聞期間](../Page/晚間新聞.md "wikilink")，主播[黃大鈞報道天氣時](../Page/黃大鈞.md "wikilink")，懷疑因人為出錯鏡頭突然拉後，拍攝到本來應在鏡頭以外的文宇軒正在整理衣服。

## 參考資料

## 外部連結

  -
[Category:香港記者](../Category/香港記者.md "wikilink")
[Category:無綫電視新聞報導員](../Category/無綫電視新聞報導員.md "wikilink")
[M](../Category/香港培正中學校友.md "wikilink")
[Category:香港樹仁大學校友](../Category/香港樹仁大學校友.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Y](../Category/文姓.md "wikilink")

1.
2.