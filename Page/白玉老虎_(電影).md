是1977年[邵氏出品](../Page/邵氏.md "wikilink")，[楚原導演](../Page/楚原.md "wikilink")，[狄龍](../Page/狄龍.md "wikilink")、[岳華](../Page/岳華.md "wikilink")、[施思](../Page/施思.md "wikilink")、[爾冬陞](../Page/爾冬陞.md "wikilink")、[羅烈主演](../Page/羅烈.md "wikilink")，由根據[古龍](../Page/古龍.md "wikilink")1976年的同名武俠小說拍攝而成的電影。由於這部小說[古龍並沒有寫完](../Page/古龍.md "wikilink")，因此[古龍先生親自參與編劇](../Page/古龍.md "wikilink")，與[楚原合力完成](../Page/楚原.md "wikilink")。

## 劇情簡介

江湖上一大家族大風堂少主趙無忌新婚之日，竟有人出萬兩黃金買其父趙簡的人頭，當夜趙簡就被斬去頭顱。為了報仇，無忌先是與唐家四少爺鬥智鬥力，後又化名混入唐家伺機報仇，他更與唐家女兒唐羽成親，以圖取信任；無忌的新婚妻子不明究理趕來爭夫，無忌不能相認，眼睜睜的看著妻子自盡而死。後來無忌終於搗破唐家的製毒密室，徹底消滅了唐家，但大風堂亦損傷數百條人命，付出了慘重的代價……

## 演員表

<table style="width:32%;">
<colgroup>
<col style="width: 32%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>演員</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/狄龍.md" title="wikilink">狄龍</a></p></td>
<td><p>趙無忌</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕭瑤.md" title="wikilink">蕭瑤</a></p></td>
<td><p>衛鳳娘（無忌妻）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李麗麗.md" title="wikilink">李麗麗</a></p></td>
<td><p>趙千千（無忌妹）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/谷峰.md" title="wikilink">谷峰</a></p></td>
<td><p>上官刃</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/井淼.md" title="wikilink">井淼</a></p></td>
<td><p>趙簡</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅烈.md" title="wikilink">羅烈</a></p></td>
<td><p>唐缺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岳華.md" title="wikilink">岳華</a></p></td>
<td><p>唐敖</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/施思.md" title="wikilink">施思</a></p></td>
<td><p>唐羽</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/爾冬陞.md" title="wikilink">爾冬陞</a></p></td>
<td><p>唐玉</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姜南.md" title="wikilink">姜南</a></p></td>
<td><p>唐蛇</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/顧冠忠.md" title="wikilink">顧冠忠</a></p></td>
<td><p>西施</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾飛.md" title="wikilink">艾飛</a></p></td>
<td><p>丁棄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊志卿.md" title="wikilink">楊志卿</a></p></td>
<td><p>樊雲山</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/徐少強.md" title="wikilink">徐少強</a></p></td>
<td><p>獨孤勝</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/詹森.md" title="wikilink">詹森</a></p></td>
<td><p>柳三更</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王俠.md" title="wikilink">王俠</a></p></td>
<td><p>司空曉風</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/樊梅生.md" title="wikilink">樊梅生</a></p></td>
<td><p>趙忠</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史仲田.md" title="wikilink">史仲田</a></p></td>
<td><p>馬車異人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郝履仁.md" title="wikilink">郝履仁</a></p></td>
<td><p>活死人</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 幕後花絮

  - 這是[爾冬陞自](../Page/爾冬陞.md "wikilink")1975年簽約[邵氏後的第二部電影](../Page/邵氏.md "wikilink")，也是他與[楚原合作的開始](../Page/楚原.md "wikilink")。[爾冬陞進入邵氏後足足一年沒有戲拍](../Page/爾冬陞.md "wikilink")，因為沒有導演敢用新人，第二年才在[孫仲的](../Page/孫仲.md "wikilink")《阿Sir毒后老虎槍》裡初試啼聲。有一天[楚原在攝影棚外](../Page/楚原.md "wikilink")，偶然看到一名倚在車旁的年輕小夥子，很高，身材很好，有一種吸引人的目光，引起了[楚原的注意](../Page/楚原.md "wikilink")。[楚原打聽之下](../Page/楚原.md "wikilink")，得知他是[姜大衛的弟弟](../Page/姜大衛.md "wikilink")，公司的新人。一向很少捧新人的[楚原決定試試他](../Page/楚原.md "wikilink")，先讓他在《白玉老虎》中先演反派唐玉（因為反派通常比較好演）。誰知等[爾冬陞穿好戲服](../Page/爾冬陞.md "wikilink")，神采盡失，站在那裡活像“傻子”。後來[楚原才知爾冬陞當時只有十九歲](../Page/楚原.md "wikilink")，先幫他改了戲服，再教他一點穿古裝的祕訣，最重要的是把眼神凝聚起來。後來爾冬陞的表現令[楚原滿意](../Page/楚原.md "wikilink")，下一部《[三少爺的劍](../Page/三少爺的劍_\(電影\).md "wikilink")》便用他當主角，順利一炮而紅。\[1\]。

## 資料來源

<references/>

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[Category:楚原電影](../Category/楚原電影.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:武俠片](../Category/武俠片.md "wikilink")
[7](../Category/1970年代香港電影作品.md "wikilink")

1.  楚原影壇回憶錄──星光伴我行