**日本古代职官**是指[日本前近代](../Page/日本.md "wikilink")、律令制廢止之前的統治機構。

## 律令制〈大宝律令〉以前

律令制以前整備的官制體系。

[大和王權形成期時](../Page/大和王權.md "wikilink")，「[氏](../Page/氏.md "wikilink")（）」則指有血缘关系的亲族集团，「[姓](../Page/姓.md "wikilink")（）」是指大和王权的最高执权者“大王（后改为天皇）”根据“氏”的政治地位和职务而赐予的称号。（、、）

一方、以[皇族](../Page/皇族.md "wikilink")（[大和王族](../Page/大和王族.md "wikilink")）為中心的支配体制強化，為了更有效錄用血緣及勢力無關人材登用，將[官位](../Page/官位.md "wikilink")（冠位）制度（官職[位階制度](../Page/位階.md "wikilink")）引進。603年（[推古](../Page/推古天皇.md "wikilink")11年）[聖德太子創制](../Page/聖德太子.md "wikilink")「[冠位十二階](../Page/冠位十二階.md "wikilink")」。官位制度從冠位十二階至到律令官位制為止，出現多次的變遷（）。

整備氏姓制度、官位制及職掌體育的整備是[律令制](../Page/律令制.md "wikilink")。668年（[天智](../Page/天智天皇.md "wikilink")8年）最初制定的「令（）」是[近江令](../Page/近江令.md "wikilink")，689年（[持統](../Page/持統天皇.md "wikilink")3年）[飛鳥淨御原令開始進行體系化](../Page/飛鳥淨御原令.md "wikilink")。701年（[大寶元年](../Page/大寶_\(文武天皇\).md "wikilink")）頒定[大寶律令](../Page/大寶律令.md "wikilink")。

## 律令制（大宝律令）以後

關於律令制詳細可參考「[律令制](../Page/律令制.md "wikilink")」的內容、關於官位制詳細可參考「[官位](../Page/官位.md "wikilink")」內容、關於位階詳細可參考「[位階](../Page/位階.md "wikilink")」內容。以下是律令制中的官制，特別說明關於官職的事項。

### 中央官制

律令制下的中央官制以[二官八省為基本体制](../Page/二官八省.md "wikilink")。在[天皇之下](../Page/天皇.md "wikilink")，置有担当朝廷祭祀的**[神祇官](../Page/神祇官.md "wikilink")**與統括国政的**[太政官](../Page/太政官.md "wikilink")**（二官），太政官之下實際的行政置**八省**分担。

中国的律令制中，[皇帝集權](../Page/皇帝.md "wikilink")，並在三省（[中書省](../Page/中書省.md "wikilink")、[門下省](../Page/門下省.md "wikilink")、[尚書省](../Page/尚書省.md "wikilink")）輔助皇帝。相對上，日本的律令制（二官八省）是天皇與各省之間代理天皇處事的合議体，特-{徵}-是設置太政官。

二官八省以外其他的監察行政組織的[弾正台及宮中等負責守衛的](../Page/弾正台.md "wikilink")[衛府](../Page/衛府.md "wikilink")（衛門府、左右衛士府、左右兵衛府）由天皇直轄管理（まとめて、二官八省一台五衛府）。此與「官」是指「役所的事物」，與現在的用法「辦公室職員」有所差異。

#### 二官

  - [神祇官](../Page/神祇官.md "wikilink") - 管理神祇祭祀（）的役所。從太政官獨自的組織，但是受到太政官的控制。
  - [太政官](../Page/太政官.md "wikilink") -
    管理一般国政的役所。[太政大臣](../Page/太政大臣.md "wikilink")、[左大臣](../Page/左大臣.md "wikilink")、[右大臣](../Page/右大臣.md "wikilink")（以上稱為[三公](../Page/三公.md "wikilink")）與[大納言等的合議体的議政官組織](../Page/大納言.md "wikilink")，其下有[少納言局](../Page/少納言局.md "wikilink")、[左右辨官局](../Page/左右辨官局.md "wikilink")。太政大臣並無分掌之職。之後設置[內大臣](../Page/內大臣.md "wikilink")、[中納言](../Page/中納言.md "wikilink")（大宝令初期中廢除納言、705年（慶雲2）再度設置議政官。）及[参議](../Page/参議.md "wikilink")。
      - [少納言局](../Page/少納言局.md "wikilink") -
        太政官之事務、秘書部局。少納言、大外記、少外記、史生、使部。
      - [左右辨官局](../Page/左右辨官局.md "wikilink") -
        左辨官局與右辨官局。在議政官組織指揮之負責執行政務。由大中少辨（辨官）、大少史（史官）、史生、官掌、使部組成。右辨官局的大辨官名為「右大辨」、左辨官局的少史名為「左少史」。左辨官局是管理中務省、式部省、治部省、民部省的4省政务，右辨官局掌管兵部省、刑部省、大藏省、宮內省的4省政務。
      - [巡察使](../Page/巡察使.md "wikilink")
      - [太政官厨家](../Page/太政官厨家.md "wikilink")

#### 八省

##### 左辨官局

  - [中務省](../Page/中務省.md "wikilink") -
    服侍天皇、編寫詔勅・宣旨、伝奏等宮中事務以及位記、戶籍等事務。四等官其他[侍従](../Page/侍従.md "wikilink")、[內舎人](../Page/內舎人.md "wikilink")、[內記](../Page/內記.md "wikilink")、[監物](../Page/監物.md "wikilink")、[主鈴](../Page/主鈴.md "wikilink")、[典鑰等品官屬於本省](../Page/典鑰.md "wikilink")。
      - [中宮職](../Page/中宮職.md "wikilink") -
        管理[後宮相關事務](../Page/後宮.md "wikilink")。[太皇太后](../Page/太皇太后.md "wikilink")、[皇太后](../Page/皇太后.md "wikilink")、[皇后がいるときには](../Page/皇后.md "wikilink")、それぞれ，置有[太皇太后宮職](../Page/太皇太后宮職.md "wikilink")、[皇太后宮職](../Page/皇太后宮職.md "wikilink")、[皇后宮職](../Page/皇后宮職.md "wikilink")。
      - [左右大舍人寮](../Page/左右大舍人寮.md "wikilink") -
        [平城天皇時代左右統合](../Page/平城天皇.md "wikilink")。
      - [圖書寮](../Page/圖書寮.md "wikilink")
          - [紙屋院](../Page/紙屋院.md "wikilink")
      - [內藏寮](../Page/內藏寮.md "wikilink")
      - [縫殿寮](../Page/縫殿寮.md "wikilink")
          - [糸所](../Page/糸所.md "wikilink")
      - [内匠寮](../Page/内匠寮.md "wikilink")（令外）-[聖武天皇時代設立](../Page/聖武天皇.md "wikilink")。
      - [陰陽寮](../Page/陰陽寮.md "wikilink")
      - [画工司](../Page/画工司.md "wikilink") - 808年與内匠寮合併
      - [内藥司](../Page/内藥司.md "wikilink")- 896年與宮内省之典藥寮合併
      - [内礼司](../Page/内礼司.md "wikilink") - 808年與彈正台合併

<!-- end list -->

  - [式部省](../Page/式部省.md "wikilink") -
    管理文官之人事、朝儀、学校等。758年至764年間改名為[文部省](../Page/文部省.md "wikilink")。
      - [大學寮](../Page/大學寮.md "wikilink")
          - [大學別曹](../Page/大學別曹.md "wikilink")
      - [散位寮](../Page/散位寮.md "wikilink") - 896年合併至式部省之內。

<!-- end list -->

  - [治部省](../Page/治部省.md "wikilink")－掌管各姓氏、族姓葬禮以及[佛寺](../Page/佛寺.md "wikilink")、[雅樂](../Page/雅樂.md "wikilink")、[外交事務](../Page/外交.md "wikilink")。
      - [雅樂寮](../Page/雅樂寮.md "wikilink")
      - [玄蕃寮](../Page/玄蕃寮.md "wikilink")
      - [諸陵司](../Page/諸陵寮.md "wikilink") -
        729年升格為[諸陵寮](../Page/諸陵寮.md "wikilink")。
      - [喪儀司](../Page/喪儀司.md "wikilink") -
        808年與[兵部省之](../Page/兵部省.md "wikilink")[鼓吹司合併](../Page/鼓吹司.md "wikilink")。

<!-- end list -->

  - [民部省](../Page/民部省.md "wikilink")－掌管民政特別是租稅和財政。司[戶籍](../Page/戶籍.md "wikilink")、田畑。
      -   - [廩院](../Page/民部省.md "wikilink")

      - [主計寮](../Page/主計寮.md "wikilink")

      - [主稅寮](../Page/主稅寮.md "wikilink")

##### 右辨官局

  - [兵部省](../Page/兵部省.md "wikilink") -
    掌管一切[武官之](../Page/武官.md "wikilink")[人事與](../Page/人事.md "wikilink")[軍事](../Page/軍事.md "wikilink")。
      - [隼人司](../Page/隼人司.md "wikilink") - 808年由衛門府編入兵部省。
      - [兵馬司](../Page/兵馬司_\(日本\).md "wikilink") - 后其職務由左右馬寮分擔。
      - [造兵司](../Page/造兵司.md "wikilink") -
        [宇多天皇時與兵庫寮合併](../Page/宇多天皇.md "wikilink")。
      - [鼓吹司](../Page/鼓吹司.md "wikilink") -
        [宇多天皇時與兵庫寮合併](../Page/宇多天皇.md "wikilink")。
      - [主船司](../Page/主船司.md "wikilink") - 廢止。
      - [主鷹司](../Page/主鷹司.md "wikilink") - 廢止。

<!-- end list -->

  - [刑部省](../Page/刑部省.md "wikilink")－掌管[司法](../Page/司法.md "wikilink")。
      - [囚獄司](../Page/囚獄司.md "wikilink") - 検非違使抬頭變得有名無實。
      - [贓贖司](../Page/贓贖司.md "wikilink") -
        [平城天皇時與](../Page/平城天皇.md "wikilink")[刑部省合併](../Page/刑部省.md "wikilink")。

<!-- end list -->

  - [大藏省](../Page/大藏省.md "wikilink")－掌管[財宝](../Page/財宝.md "wikilink")、[出納](../Page/出納.md "wikilink")、[物價](../Page/物價.md "wikilink")、[度量衡](../Page/度量衡.md "wikilink")。
      -   - [正藏率分所](../Page/正藏率分所.md "wikilink")

      - [織部司](../Page/織部司.md "wikilink")

      - [典鑄司](../Page/典鑄司.md "wikilink") -
        [光仁天皇時與中務省的内匠寮合併](../Page/光仁天皇.md "wikilink")。

      - [漆部司](../Page/漆部司.md "wikilink") -
        [平城天皇時與中務省的内匠寮合併](../Page/平城天皇.md "wikilink")。

      - [縫部司](../Page/縫部司.md "wikilink") - 平城天皇時與中務省的縫殿寮合併。

      - [掃部司](../Page/掃部司.md "wikilink") -
        [嵯峨天皇時與宮内省之内掃部司合併](../Page/嵯峨天皇.md "wikilink")，於宮内省設置掃部寮。

<!-- end list -->

  - [宮内省](../Page/宮内省.md "wikilink")－掌管宮中的衣食住、財物及其他雜事。
      - [大膳職](../Page/大膳職.md "wikilink")
      - [木工寮](../Page/木工寮.md "wikilink")
      - [大炊寮](../Page/大炊寮.md "wikilink")
          - [供御院](../Page/供御院.md "wikilink")
      - [主殿寮](../Page/主殿寮.md "wikilink")
          - [釜殿](../Page/釜殿.md "wikilink")
      - [典藥寮](../Page/典藥寮.md "wikilink")
          - [乳牛院](../Page/乳牛院.md "wikilink")
      - [掃部寮](../Page/掃部寮.md "wikilink")
      - [正親司](../Page/正親司.md "wikilink")
      - [内膳司](../Page/内膳司.md "wikilink")
          - [進物所](../Page/進物所.md "wikilink")
          - [御廚子所](../Page/御廚子所.md "wikilink")
          - [贄殿](../Page/贄殿.md "wikilink")
      - [造酒司](../Page/造酒司.md "wikilink")
          - [酒殿](../Page/酒殿.md "wikilink")
      - [采女司](../Page/采女司.md "wikilink")
      - [主水司](../Page/主水司.md "wikilink")
          - [冰室](../Page/冰室_\(日本\).md "wikilink")
      - [筥陶司](../Page/筥陶司.md "wikilink") - 與大膳職合併。
      - [鍛冶司](../Page/鍛冶司.md "wikilink") - 與木工寮合併。
      - [官奴司](../Page/官奴司.md "wikilink") - 與主殿寮合併。
      - [主油司](../Page/主油司.md "wikilink") - 與主殿寮合併。
      - [內掃部司](../Page/內掃部司.md "wikilink") - 與掃部寮合併。
      - [內染司](../Page/內染司.md "wikilink") - 與掃部寮合併。
      - [園池司](../Page/園池司.md "wikilink") - 與内膳司合併。
      - [土工司](../Page/土工司.md "wikilink") - 與木工寮合併。

#### 彈正台

  - [彈正台](../Page/彈正台.md "wikilink")（）- 行政監察

#### 衛府

  - [五衛府](../Page/五衛府.md "wikilink")
      - [左兵衛府](../Page/兵衛府.md "wikilink")、[右兵衛府](../Page/兵衛府.md "wikilink")（）
      - [左衛士府](../Page/衛士府.md "wikilink")、[右衛士府](../Page/衛士府.md "wikilink")（）－與衛門府合併後改称[左右衛門府](../Page/左右衛門府.md "wikilink")
      - [衛門府](../Page/衛門府.md "wikilink")（）－與衛士府合併
          - [隼人司](../Page/隼人司.md "wikilink")－移交兵部省管轄
  - [左近衛府](../Page/近衛府.md "wikilink")、[右近衛府](../Page/近衛府.md "wikilink")（）－新設
  - [中衛府](../Page/中衛府.md "wikilink")（）－[奈良時代新設](../Page/奈良時代.md "wikilink")，後來被廢止。
  - [授刀衛](../Page/授刀衛.md "wikilink")（）－奈良時代新設，近衛府的[前身](../Page/前身.md "wikilink")
  - [外衛府](../Page/外衛府.md "wikilink")（）－奈良時代新設，近衛府的[前身](../Page/前身.md "wikilink")

#### 東宮

  - [春宮坊](../Page/春宮坊.md "wikilink")（）
      - [主膳監](../Page/主膳監.md "wikilink")
      - [主藏監](../Page/主藏監.md "wikilink")－廢止
      - [舎人監](../Page/舎人監.md "wikilink")－廢止
      - [主殿署](../Page/主殿署.md "wikilink")
      - [主馬署](../Page/主馬署.md "wikilink")
      - [主工署](../Page/主工署.md "wikilink")－廢止
      - [主書署](../Page/主書署.md "wikilink")－與主藏監合併
      - [主兵署](../Page/主兵署.md "wikilink")－與主藏監合併
      - [主漿署](../Page/主漿署.md "wikilink")－與主膳監合併
  - [東宮傅](../Page/東宮傅.md "wikilink")（）
  - [東宮學士](../Page/東宮學士.md "wikilink")

#### 馬寮

  - [左、右馬寮](../Page/馬寮.md "wikilink") -
    桓武天皇時，左右馬寮合併為[主馬寮](../Page/主馬寮.md "wikilink")，後來再分為兩級。
      - [牧](../Page/牧.md "wikilink")
  - [內廄寮](../Page/內廄寮.md "wikilink")（）－奈良時代新設，合併自左、右馬寮
  - [主馬寮](../Page/主馬寮.md "wikilink")（）－內廄寮與左、右馬寮合併，平安時代廢止

#### 兵庫

  - [左・右兵庫](../Page/兵庫_\(律令制\).md "wikilink")（）－寮格，與内兵庫等組織統合後再改組成[兵庫寮](../Page/兵庫寮.md "wikilink")
  - [内兵庫](../Page/内兵庫.md "wikilink")（）－司格，合併自左、右兵庫
  - [兵庫寮](../Page/兵庫寮.md "wikilink")－新設

#### 後宮

  - [後宮](../Page/日本後宮.md "wikilink")
      - [後宮十二司](../Page/後宮十二司.md "wikilink")
          - [內侍司](../Page/內侍司.md "wikilink")
          - [兵司](../Page/兵司.md "wikilink")
          - [藏司](../Page/藏司.md "wikilink")
          - [書司](../Page/書司.md "wikilink")
          - [縫司](../Page/縫司.md "wikilink")
          - [膳司](../Page/膳司.md "wikilink")
          - [酒司](../Page/酒司.md "wikilink")
          - [闡司](../Page/闡司.md "wikilink")
          - [殿司](../Page/殿司.md "wikilink")（主殿司）
          - [掃司](../Page/掃司.md "wikilink")
          - [水司](../Page/水司.md "wikilink")
          - [藥司](../Page/藥司.md "wikilink")

#### 家令

  - [家令](../Page/家令.md "wikilink")

#### 其他主要令外官

  - [修理職](../Page/修理職.md "wikilink")- 淳和天皇時，一時期與宮内省的木工寮統一。
  - [齋院司](../Page/齋院司.md "wikilink")
  - [鑄錢司](../Page/鑄錢司.md "wikilink")
  - [齋宮寮](../Page/齋宮寮.md "wikilink") -
    [齋宮時運作](../Page/齋宮.md "wikilink")，掌館伊勢太神宮、伊勢神領相關事務。斎宮寮之下置齋宮十二司（舍人司、藏部司、膳部司、炊部司、酒部司、水部司、殿部司、掃部司、-{采}-部司、藥部司、門部司、馬部司）。
  - [檢非違使廳](../Page/檢非違使.md "wikilink")
  - [勘解由使廳](../Page/勘解由使.md "wikilink")
  - [穀倉院](../Page/穀倉院.md "wikilink")
  - [藏人所](../Page/藏人_\(官職\).md "wikilink")
      - [御書所](../Page/御書所.md "wikilink")
      - [一本御書所](../Page/一本御書所.md "wikilink")
      - [內御書所](../Page/內御書所.md "wikilink")
      - [画所](../Page/画所.md "wikilink")
      - [作物所](../Page/作物所.md "wikilink")
      - [御匣殿](../Page/御匣殿.md "wikilink")
      - [樂所](../Page/樂所.md "wikilink")
      - [內豎所](../Page/內豎所.md "wikilink")
  - [院廳](../Page/院廳.md "wikilink")
  - [女院廳](../Page/女院廳.md "wikilink")
  - [後院](../Page/後院.md "wikilink")（[淳和院等](../Page/淳和院.md "wikilink")）
  - [內教坊](../Page/內教坊.md "wikilink")
  - [施藥院](../Page/施藥院.md "wikilink")
      - [崇親院](../Page/崇親院.md "wikilink")
  - [諸使](../Page/日本諸使列表.md "wikilink")
  - [諸司](../Page/日本諸司列表.md "wikilink")
  - [諸所](../Page/日本諸所列表.md "wikilink")
  - [造寺司](../Page/造寺司.md "wikilink")
  - [造京司](../Page/造京司.md "wikilink")、[造宮司](../Page/造宮司.md "wikilink")

### 地方官制

全日本被分數十國的[令制国](../Page/令制国.md "wikilink")，每個國從中央派遣一個國司治理。

此外，一些重要腹地被配置特別職務（[大宰府](../Page/大宰府.md "wikilink")、[左右京職](../Page/京職.md "wikilink")、[攝津職等](../Page/攝津職.md "wikilink")）的人。

### 四等官

諸官司一般分為[長官](../Page/長官.md "wikilink")（）、[次官](../Page/次官.md "wikilink")（）、[判官](../Page/判官.md "wikilink")（）、[主典](../Page/主典.md "wikilink")（）[四等官](../Page/四等官.md "wikilink")。之後是各種[品官](../Page/品官.md "wikilink")（，以上官位相当官）及[史生](../Page/史生.md "wikilink")（）、[伴部](../Page/伴部.md "wikilink")（）、[使部](../Page/使部.md "wikilink")（）等的均屬雜任（下等職員）。

<table>
<thead>
<tr class="header">
<th><p>官名</p></th>
<th><p>長官</p></th>
<th><p>次官</p></th>
<th><p>判官</p></th>
<th><p>主典</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>神祇官</p></td>
<td><p>伯</p></td>
<td><p>大　副</p></td>
<td><p>大　佑</p></td>
<td><p>大　史</p></td>
</tr>
<tr class="even">
<td><p>少　副</p></td>
<td><p>少　佑</p></td>
<td><p>少　史</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>太政官</p></td>
<td><p>太政大臣<br />
左大臣<br />
右大臣<br />
大納言</p></td>
<td><p>中納言<br />
內大臣<br />
參議</p></td>
<td><p>少納言<br />
大、中、少辨</p></td>
<td><p>大、少史<br />
大、少外記</p></td>
</tr>
<tr class="even">
<td><p>八省</p></td>
<td><p>卿</p></td>
<td><p>大　輔</p></td>
<td><p>大　丞</p></td>
<td><p>大　録</p></td>
</tr>
<tr class="odd">
<td><p>少　輔</p></td>
<td><p>少　丞</p></td>
<td><p>少　録</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>彈正台</p></td>
<td><p>尹</p></td>
<td><p>大　弼<br />
少　弼</p></td>
<td><p>大　忠<br />
少　忠</p></td>
<td><p>大　疎<br />
少　疎</p></td>
</tr>
<tr class="odd">
<td><p>五衛府</p></td>
<td><p>督</p></td>
<td><p>佐</p></td>
<td><p>大　尉<br />
少　尉</p></td>
<td><p>大　志<br />
少　志</p></td>
</tr>
<tr class="even">
<td><p>職、坊</p></td>
<td><p>大　夫</p></td>
<td><p>亮</p></td>
<td><p>大　進</p></td>
<td><p>大　属</p></td>
</tr>
<tr class="odd">
<td><p>少　進</p></td>
<td><p>少　属</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>寮</p></td>
<td><p>頭</p></td>
<td><p>助</p></td>
<td><p>大　允<br />
少　允</p></td>
<td><p>大　属<br />
少　属</p></td>
</tr>
<tr class="odd">
<td><p>司、監</p></td>
<td><p>正/（奉膳）</p></td>
<td><p>（典膳）</p></td>
<td><p>佑</p></td>
<td><p>令　史</p></td>
</tr>
<tr class="even">
<td><p>署</p></td>
<td><p>首</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>令　史</p></td>
</tr>
<tr class="odd">
<td><p>後宮十二司</p></td>
<td><p>尚－</p></td>
<td><p>典－</p></td>
<td><p>掌－</p></td>
<td><p>－</p></td>
</tr>
<tr class="even">
<td><p>家　令</p></td>
<td><p>家　令</p></td>
<td><p>家　扶</p></td>
<td><p>家　従</p></td>
<td><p>書　吏</p></td>
</tr>
<tr class="odd">
<td><p>大宰府</p></td>
<td><p>帥</p></td>
<td><p>大　弐<br />
少　弐</p></td>
<td><p>大　監<br />
少　監</p></td>
<td><p>大　典<br />
少　典</p></td>
</tr>
<tr class="even">
<td><p>国　司</p></td>
<td><p>守</p></td>
<td><p>介</p></td>
<td><p>掾</p></td>
<td><p>大　目<br />
少　目</p></td>
</tr>
<tr class="odd">
<td><p>郡　司</p></td>
<td><p>大　領</p></td>
<td><p>少　領</p></td>
<td><p>主　政</p></td>
<td><p>主　帳</p></td>
</tr>
<tr class="even">
<td><p>鎮守府</p></td>
<td><p>將　軍</p></td>
<td><p>副將軍</p></td>
<td><p>軍　監</p></td>
<td><p>軍　曹</p></td>
</tr>
<tr class="odd">
<td><p>近衛府</p></td>
<td><p>大　將</p></td>
<td><p>中　將<br />
少　將</p></td>
<td><p>將　監</p></td>
<td><p>將　曹</p></td>
</tr>
<tr class="even">
<td><p>造寺使</p></td>
<td><p>使</p></td>
<td><p>次　官</p></td>
<td><p>判　官</p></td>
<td><p>主　典</p></td>
</tr>
<tr class="odd">
<td><p>勘解由使廳</p></td>
<td><p>長官</p></td>
<td><p>次　官</p></td>
<td><p>判　官</p></td>
<td><p>主　典</p></td>
</tr>
</tbody>
</table>

1.  　奉膳與典膳在[内膳司出現](../Page/内膳司.md "wikilink")。
2.  　これはあくまでも簡略化した表であり､とくに[職](../Page/職_\(律令制\).md "wikilink")・[寮](../Page/寮_\(律令制\).md "wikilink")・[司の場合は次官](../Page/司.md "wikilink")・判官が欠けたり､増員したりした。詳情可參看各項官職。

## 官位相当表

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p><strong>神祇官</strong></p></td>
<td><p><strong>太政官</strong></p></td>
<td><p><strong>大宰府</strong></p></td>
<td><p><strong>八省</strong></p></td>
<td><p><strong>彈正台</strong></p></td>
<td><p><strong>衛門府<br />
左右衛士府</strong></p></td>
<td><p><strong>左右兵衛府</strong></p></td>
<td><p><strong>国司</strong></p></td>
<td><p><strong>藏人所</strong>（<a href="../Page/令外官.md" title="wikilink">令外官</a>）</p></td>
</tr>
<tr class="even">
<td><p>正一位<br />
從一位</p></td>
<td></td>
<td><p>太政大臣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>正二位<br />
從二位</p></td>
<td></td>
<td><p>左大臣<br />
右大臣<br />
内大臣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>別当</p></td>
</tr>
<tr class="even">
<td><p>正三位</p></td>
<td></td>
<td><p>大納言</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>從三位</p></td>
<td></td>
<td><p>中納言</p></td>
<td><p>帥</p></td>
<td></td>
<td><p>尹</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>正四位上</p></td>
<td></td>
<td></td>
<td></td>
<td><p>中務卿</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>正四位下</p></td>
<td></td>
<td></td>
<td></td>
<td><p>七省卿</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>從四位上</p></td>
<td></td>
<td><p>左右大辨</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>從四位下</p></td>
<td><p>伯</p></td>
<td></td>
<td></td>
<td></td>
<td><p>大弼</p></td>
<td></td>
<td></td>
<td></td>
<td><p>頭</p></td>
</tr>
<tr class="even">
<td><p>正五位上</p></td>
<td></td>
<td><p>左右中辨</p></td>
<td><p>大弐</p></td>
<td><p>中務大輔</p></td>
<td></td>
<td><p>督</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>正五位下</p></td>
<td></td>
<td><p>左右少辨</p></td>
<td></td>
<td><p>七省大輔<br />
大判事</p></td>
<td><p>少弼</p></td>
<td></td>
<td></td>
<td></td>
<td><p>五位藏人</p></td>
</tr>
<tr class="even">
<td><p>從五位上</p></td>
<td></td>
<td></td>
<td></td>
<td><p>中務少輔</p></td>
<td></td>
<td></td>
<td><p>督</p></td>
<td><p>大国守</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>従五位下</p></td>
<td><p>大副</p></td>
<td><p>少納言</p></td>
<td><p>少弐</p></td>
<td><p>侍從<br />
大監物<br />
七省少輔</p></td>
<td></td>
<td><p>佐</p></td>
<td></td>
<td><p>上国守</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>正六位上　</p></td>
<td><p>少副</p></td>
<td><p>左右辨大史</p></td>
<td></td>
<td><p>大内記</p></td>
<td><p>大忠</p></td>
<td></td>
<td></td>
<td></td>
<td><p>六位蔵人</p></td>
</tr>
<tr class="odd">
<td><p>正六位下</p></td>
<td></td>
<td></td>
<td><p>大監</p></td>
<td><p>八省大丞<br />
中判事</p></td>
<td><p>少忠</p></td>
<td></td>
<td><p>佐</p></td>
<td><p>大国介<br />
中国守</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>從六位上</p></td>
<td><p>大祐</p></td>
<td></td>
<td><p>少監</p></td>
<td><p>八省少丞<br />
中監物</p></td>
<td></td>
<td></td>
<td></td>
<td><p>上国介</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>從六位下</p></td>
<td><p>少祐</p></td>
<td></td>
<td><p>大判事</p></td>
<td><p>少判事<br />
大藏大主鑰</p></td>
<td></td>
<td><p>大尉</p></td>
<td></td>
<td><p>下国守</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>正七位上</p></td>
<td></td>
<td><p>大外記<br />
左右辨少史</p></td>
<td><p>大工<br />
少判事<br />
大典<br />
防人正</p></td>
<td><p>中内記<br />
八省大録</p></td>
<td><p>大疏</p></td>
<td><p>少尉</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>正七位下</p></td>
<td></td>
<td></td>
<td><p>主神</p></td>
<td><p>少監物<br />
大主鈴<br />
判事大屬</p></td>
<td><p>巡察</p></td>
<td></td>
<td><p>大尉</p></td>
<td><p>大国大掾</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>從七位上</p></td>
<td></td>
<td><p>少外記</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>少尉</p></td>
<td><p>大国少掾<br />
上国掾</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>從七位下</p></td>
<td></td>
<td></td>
<td><p>博士</p></td>
<td><p>大典鑰<br />
大解部<br />
大藏少主鑰</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>正八位上</p></td>
<td></td>
<td></td>
<td><p>少典、陰陽師<br />
医師、少工<br />
算師、防人佑<br />
主船、主廚</p></td>
<td><p>少内記<br />
八省少錄<br />
少主鈴<br />
典履、典革</p></td>
<td><p>少疏</p></td>
<td></td>
<td></td>
<td><p>中国掾</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>正八位下</p></td>
<td><p>大史</p></td>
<td></td>
<td></td>
<td><p>治部大解部<br />
刑部中解部<br />
判事少屬</p></td>
<td></td>
<td><p>大志<br />
医師</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>從八位上</p></td>
<td><p>少史</p></td>
<td></td>
<td></td>
<td><p>少典鑰</p></td>
<td></td>
<td><p>少志</p></td>
<td><p>大志<br />
医師</p></td>
<td><p>大国大目</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>從八位下</p></td>
<td></td>
<td></td>
<td></td>
<td><p>刑部少解部<br />
治部少解部</p></td>
<td></td>
<td></td>
<td><p>少志</p></td>
<td><p>大国少目<br />
上国目</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>大初位上</p></td>
<td></td>
<td></td>
<td><p>判事大令史</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大初位下</p></td>
<td></td>
<td></td>
<td><p>判事少令史<br />
防人令史</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>中国目</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>少初位上</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>下国目</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>少初位下</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 令外官

顧名思義，令外官也就是在律令制度外所設置的官職。例如[征夷大將軍就是屬於令外官](../Page/征夷大將軍.md "wikilink")。

  - 征夷大將軍（桓武天皇設）
  - 藏人所官（藏人所長官為藏人頭，嵯峨天皇設）
  - 檢非違使（檢非違使別當，檢非違使尉，嵯峨天皇設）

## 参考文献

## 参见

  - [日本近代官制](../Page/日本近代官制.md "wikilink")
  - [日本國家機關](../Page/日本國家機關.md "wikilink")
  - [除目](../Page/除目.md "wikilink")
  - [官人 (日本)](../Page/官人_\(日本\).md "wikilink")
  - [唐名 (日本官制)](../Page/唐名_\(日本官制\).md "wikilink")
  - [解官](../Page/解官.md "wikilink")
  - [中國古代職官](../Page/中國古代職官.md "wikilink")
  - [朝鮮古代職官](../Page/朝鮮古代職官.md "wikilink")
  - [越南古代職官](../Page/越南古代職官.md "wikilink")

[Category:东亚古代官制](../Category/东亚古代官制.md "wikilink")
[日本古代官制](../Category/日本古代官制.md "wikilink")