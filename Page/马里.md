**马里共和国**（）
通稱**馬利**，是位於[西非的](../Page/西非.md "wikilink")[内陆国家](../Page/内陆国家.md "wikilink")，向北与[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")、向东与[尼日尔](../Page/尼日尔.md "wikilink")、向南与[布基纳法索和](../Page/布基纳法索.md "wikilink")[科特迪瓦](../Page/科特迪瓦.md "wikilink")、向西南与[几内亚](../Page/几内亚.md "wikilink")、向西与[毛里塔尼亚和](../Page/毛里塔尼亚.md "wikilink")[塞内加尔接壤](../Page/塞内加尔.md "wikilink")，是西非面积第二大的国家。它的北部边界在[撒哈拉沙漠的中心](../Page/撒哈拉沙漠.md "wikilink")，大多数人集中在南部，[尼日尔河和](../Page/尼日尔河.md "wikilink")[塞内加尔河源于这裡](../Page/塞内加尔河.md "wikilink")。马里过去也被称为[法属苏丹](../Page/法属苏丹.md "wikilink")，它的名字来源于[马里帝国](../Page/马里帝国.md "wikilink")。

## 國名

[法語名稱為](../Page/法語.md "wikilink")「**''République du
Mali***」，簡稱「***Mali**''」；英語名稱為「***Republic of
Mali***」，簡稱「**''Mali**''」。華語名稱，[台灣稱為](../Page/台灣.md "wikilink")「**馬-{}-利共和國**」，簡稱「**馬-{}-利**」；[中國大陸稱為](../Page/中國大陸.md "wikilink")「**馬-{}-里共和國**」，簡稱「**馬-{}-里**」，[新](../Page/新加坡.md "wikilink")、[馬](../Page/馬來西亞.md "wikilink")、[港](../Page/香港.md "wikilink")、[澳與中國大陆相同](../Page/澳門.md "wikilink")。

## 历史

历史上在[萨赫勒地带](../Page/萨赫勒.md "wikilink")（包括今天的马里）有过一系列帝国，其中包括[加纳帝国](../Page/加纳帝国.md "wikilink")、[马里帝国和](../Page/马里帝国.md "wikilink")[桑海帝国等](../Page/桑海帝国.md "wikilink")。[廷巴克图是这些帝国在](../Page/廷巴克图.md "wikilink")[跨撒哈拉贸易中的一个关键城市和学术中心](../Page/跨撒哈拉贸易.md "wikilink")。1591年桑海帝国受[摩洛哥入侵不断虚弱下来](../Page/摩洛哥.md "wikilink")。

从1880年开始[法国入侵马里](../Page/法国.md "wikilink")。法国殖民地法属苏丹还包括马里周边的国家。1959年初，马里与[塞内加尔组成](../Page/塞内加尔.md "wikilink")[马里联邦](../Page/马里联邦.md "wikilink")。1960年6月20日马里联邦从法国独立，数月后塞内加尔退出联邦。同年9月22日在[莫迪博·凯塔领导下马里共和国退出](../Page/莫迪博·凯塔.md "wikilink")[法兰西共同体](../Page/法兰西共同体.md "wikilink")。

从其独立到1991年一系列独裁者统治马里。1991年的反政府抗议导致政变、一个过渡政府和一部新宪法。1992年[阿尔法·乌马尔·科纳雷在马里第一次多党派](../Page/阿尔法·乌马尔·科纳雷.md "wikilink")、民主选举中当选为总统，1997年他再次当选。他加强政治和经济的改革和打击贪污。

2002年，[阿马杜·图马尼·杜尔当任马里总统](../Page/阿马杜·图马尼·杜尔.md "wikilink")，杜尔在1991年的民主运动中是一个关键人物。2007年4月29日，举行总统选举。\[1\]杜尔连任。\[2\]

2012年1月，[圖瓦雷克人在北方發動武裝叛亂](../Page/圖瓦雷克人.md "wikilink")，[马里北部冲突開始](../Page/马里北部冲突.md "wikilink")。2012年3月21日，一批士兵在首都巴馬科发动政变，推翻杜尔，宣布中止宪法、解散国家机构。在国际社会压力下，政变军人4月6日与[西非国家经济共同体签署协议](../Page/西非国家经济共同体.md "wikilink")，同意交权，由国民议会议长出任临时总统。4月8日杜尔辞职。\[3\]在南方局勢不明的時候，圖阿雷格分離主義者在伊斯蘭激進分子幫助下趁機控制了馬里北部地區。稍後強硬派伊斯蘭激進分子撃敗圖阿雷格分離主義者，控制了馬里北部。2013年1月，伊斯蘭反政府武裝向南方進軍，法國出兵協助馬里政府作戰，迅速收復北方所有主要城市。

## 政治

《马里共和国宪法》规定马里为一个[多党制](../Page/多党制.md "wikilink")[民主](../Page/民主.md "wikilink")[共和国](../Page/共和国.md "wikilink")，唯独基于[民族](../Page/民族.md "wikilink")、[宗教信仰](../Page/宗教信仰.md "wikilink")、[地区或性别而成立的党派被禁止](../Page/地区.md "wikilink")。国家[立法机关是](../Page/立法机关.md "wikilink")[国民议会](../Page/国民议会_\(马里\).md "wikilink")，目前国会有147名成员。国会议员按各地区人口数目制定。政府执政期为5年。

## 行政区划

包括首都区在内马里分九个地区：

  - [巴马科](../Page/巴马科.md "wikilink")（Bamako，首都区）
  - [贾欧区域](../Page/加奥地区.md "wikilink")（Gao）
  - [卡伊地区](../Page/卡伊地区.md "wikilink")（Kayes）
  - [基达尔地区](../Page/基达尔地区.md "wikilink")（Kidal）
  - [库里克罗地区](../Page/库里克罗地区.md "wikilink")（Koulikoro）
  - [莫普提地区](../Page/莫普提地区.md "wikilink")（Mopti）
  - [塞古地区](../Page/塞古地区.md "wikilink")（Ségou）
  - [锡加索地区](../Page/锡加索地区.md "wikilink")（Sikasso）
  - [通布图地区](../Page/通布图地区.md "wikilink")（Tombouctou，Timbuktu）

## 地理

[Mali_sat.png](https://zh.wikipedia.org/wiki/File:Mali_sat.png "fig:Mali_sat.png")
马里大部分为海拔约300米的[台地](../Page/台地.md "wikilink")，最东部和西部有一些砂岩低山和陡崖，最高点海拔833米；北部属于[撒哈拉沙漠](../Page/撒哈拉沙漠.md "wikilink")，中、南部为热带半沙漠和草原。

马里约一半的面积是[沙漠或半沙漠](../Page/沙漠.md "wikilink")。北部从[阿尔及利亚伸入的](../Page/阿尔及利亚.md "wikilink")[阿哈加尔山脉形成一个高地](../Page/阿哈加尔山脉.md "wikilink")。南部和中部是尼日尔河宽广的低地。

马里的气候从热带温暖的气候一直到撒哈拉沙漠的沙漠气候。南部年降雨量可达1000毫米，北部有些地方终年不雨。南部是潮湿的草原，向北演变成灌木草原、半沙漠和沙漠。

### 重要城镇

[View_over_Bamako_-_20th_February_2005.jpg](https://zh.wikipedia.org/wiki/File:View_over_Bamako_-_20th_February_2005.jpg "fig:View_over_Bamako_-_20th_February_2005.jpg")
[People_in_Timbuktu.jpg](https://zh.wikipedia.org/wiki/File:People_in_Timbuktu.jpg "fig:People_in_Timbuktu.jpg")居民\]\]
[Great_Mosque_of_Djenné_3.jpg](https://zh.wikipedia.org/wiki/File:Great_Mosque_of_Djenné_3.jpg "fig:Great_Mosque_of_Djenné_3.jpg")的大清真寺\]\]

  - [巴馬科](../Page/巴馬科.md "wikilink")（Bamako），國都
  - [卡伊](../Page/卡伊.md "wikilink")（Kayes）
  - [库里克罗](../Page/库里克罗.md "wikilink")（Koulikoro）
  - [锡加索](../Page/錫卡索.md "wikilink")（Sikasso）
  - [塞古](../Page/塞古.md "wikilink")（SEGOU）
  - [莫普提](../Page/莫普提.md "wikilink")（Mopti）
  - [廷巴克图](../Page/廷巴克图.md "wikilink")（Tombouctou）
  - [加奥](../Page/加奥.md "wikilink")（Gao）
  - [基达尔](../Page/基达尔.md "wikilink")（Kidal）
  - [桑](../Page/桑鎮_\(馬里\).md "wikilink")（San）
  - [傑內](../Page/傑內.md "wikilink")（）

## 居民

马里有约20个不同的民族，其中人数最多的是[班巴拉人](../Page/班巴拉人.md "wikilink")。这些不同的民族的语言和文化各不相同，但是和平共处。东北部苏丹地区的居民很早就与北非的穆斯林交往，大多数信仰[伊斯兰教](../Page/伊斯兰教.md "wikilink")。同时国内也有许多非洲传统的信仰。马里非常重视它多种的文化历史。

马里人约只有三分之二的人口有干净的水源。平均期望寿命为58歲。人均[国民收入只有](../Page/国民收入.md "wikilink")192欧元。

官方语言是[法语](../Page/法语.md "wikilink")，但许多马里人将法语看作外语，马里有许多民族语言，大多数马里人懂多种民族语言。在教育能力上，根據2010年時的統計超過15歲的人口中僅有31.1%可以讀寫，受過教育的馬利人還是以男性居多，佔了男性人口比例的43.4%，相比之下女性僅有20.3%。

在马里北部过去经常发生[圖瓦雷克人的独立运动](../Page/圖瓦雷克人.md "wikilink")。2012年4月6日，[阿扎瓦德民族解放运动声称完全控制了其主张的国土](../Page/阿扎瓦德民族解放运动.md "wikilink")，并宣布[阿扎瓦德独立](../Page/阿扎瓦德.md "wikilink")\[4\]。
人口分布要受到自然环境的影响，对于马里来讲，亦是如此。 从地形来看，马里北部为广大沙漠，中部和南部为平原和台地。
从气候来看，马里由北向南分为热带沙漠、热带草原和热带雨林3种气候区域。
也就是说马里人口多分布在南部，从自然环境来讲，这里相对降水丰富一些。

## 宗教

90%的人信仰伊斯兰教，5%的人信仰[拜物教](../Page/拜物教.md "wikilink")，5%的人信仰[基督教](../Page/基督教.md "wikilink")。其他伊斯兰教国家对马里的经济有一定的支持。

## 经济

縱使馬里在近十多年間，每一年都擁有著5%以上的GDP增幅，不過馬里目前仍然屬於[最不發達國家之一](../Page/最不發達國家.md "wikilink")，2018年馬里的人均收入(國際匯率)只有約900美元。該國經濟支柱為農業，較少工業。在農業上，农用地佔了全國面積的2%，同时80%的劳动力在农业工作。在尼日尔河和塞内加尔河流域以及在南方多雨地区农业用地非常密集。种植物有[花生](../Page/花生.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[高粱和](../Page/高粱.md "wikilink")[棉花等](../Page/棉花.md "wikilink")。

北方由于干旱收获不能获得保证，但由于人口增加所造成的压力到年降雨量200毫米的地方也有很多农田，主要作物是[小麦和绿色饲料](../Page/小麦.md "wikilink")。

近年，馬里開始出口水果到西歐製作果汁，諸如：[芒果](../Page/芒果.md "wikilink")、[薑](../Page/薑.md "wikilink")、[洛神花等](../Page/洛神花.md "wikilink")\[5\]。

## 相关条目

  - [曼沙穆萨](../Page/曼沙穆萨.md "wikilink")
  - [阿扎瓦德民族解放运动](../Page/阿扎瓦德民族解放运动.md "wikilink")

## 参考文献

## 外部連按

  - [馬利駐美國大使館](http://www.maliembassy.us/)

  - [中华人民共和国驻马里共和国大使馆经济商务參贊处](http://ml.mofcom.gov.cn/)

  -
  -
  -
[馬利](../Category/馬利.md "wikilink")
[Category:西非](../Category/西非.md "wikilink")
[Category:前法國殖民地](../Category/前法國殖民地.md "wikilink")
[Category:非洲国家](../Category/非洲国家.md "wikilink")
[Category:內陸國家](../Category/內陸國家.md "wikilink")
[Category:共和國](../Category/共和國.md "wikilink")
[Category:法語國家地區](../Category/法語國家地區.md "wikilink")
[Category:1960年建立的國家或政權](../Category/1960年建立的國家或政權.md "wikilink")

1.  张崇防，韦巍，[马里举行总统选举](http://news.xinhuanet.com/world/2007-04/29/content_6045359.htm)，新华网
2.  张崇防，韦巍，[马里现任总统杜尔成功连任](http://news.xinhuanet.com/world/2007-05/04/content_6056181.htm)，新华网
3.  [马里总统宣布辞职](http://news.163.com/12/0410/02/7UMRNM3B00014AED.html)，网易新闻
4.
5.  [《全球飲勝》第一集](http://programme.tvb.com/foodandtravel/globaldrin必k/episode/1/#page-1)，[高清翡翠台](../Page/高清翡翠台.md "wikilink")，2012-9-23
    17:30\~18:30