是一款[黑色幽默主題的](../Page/黑色幽默.md "wikilink")[冒險遊戲](../Page/冒險遊戲.md "wikilink")，由[LucasArts於](../Page/LucasArts.md "wikilink")1998年在[Windows平台上推出](../Page/Microsoft_Windows.md "wikilink")，[提姆·謝弗](../Page/提姆·謝弗.md "wikilink")（Tim
Schafer）為開發計劃主導人。《神通鬼大》是LucasArts第一款結合[3D電腦圖形與預先](../Page/三维计算机图形.md "wikilink")[渲染背景的冒險遊戲](../Page/渲染.md "wikilink")。與其他的[LucasArts冒險遊戲相同](../Page/LucasArts冒險遊戲.md "wikilink")，玩者必須透過與其他角色互動並正確的查看、收集和使用物品來解開謎題，並讓遊戲故事向前推進。

《神通鬼大》的世界觀結合了[阿茲特克文明對死後世界的信仰](../Page/阿茲特克.md "wikilink")，以及來自《[梟巢喋-{}-血戰](../Page/梟巢喋血戰.md "wikilink")》、《[岸上風雲](../Page/岸上風雲.md "wikilink")》和《[北非諜影](../Page/北非諜影.md "wikilink")》等「[黑色電影](../Page/黑色電影.md "wikilink")」的要素，建構出了「死者國度」（Land
of the
Dead）。死去人物的靈魂（在遊戲中以[卡拉維拉人偶作為其形象](../Page/卡拉維拉人偶.md "wikilink")）在抵達終點「第九陰間」（Ninth
Underworld）前，必須旅行穿越死者國度。遊戲主角曼紐·「曼尼」·卡拉維拉（Manuel "Manny"
Calavera）在陰間旅行社擔任職員，他試圖幫助一名剛來到陰間的少女靈魂梅塞德斯·「米契」·克羅瑪（Mercedes
"Meche" Colomar）平安渡過她的漫長旅程。

本作獲得來自評論家普遍的讚賞，在其藝術設計和遊戲方向方面獲得了許多好評。《神通鬼大》在推出時獲得了多座遊戲獎項，並在許多媒體的史上最佳遊戲列表中占有一席之地。然而，本作在銷售方面的表現不如預期，LucasArts因此終止了其他冒險遊戲的開發，也成為冒險遊戲類型衰退的一部分原因。

在2014年[電子娛樂展](../Page/E3.md "wikilink")（E3）的[索尼媒體大會中發表了重新編輯版本的](../Page/索尼.md "wikilink")《神通鬼大》，由謝弗執掌的工作室[Double
Fine
Productions負責開發](../Page/Double_Fine_Productions.md "wikilink")\[1\]。《神通鬼大》重新編輯版本將會在[PlayStation
4](../Page/PlayStation_4.md "wikilink")、[PlayStation
Vita](../Page/PlayStation_Vita.md "wikilink")、[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[OS
X和](../Page/OS_X.md "wikilink")[Linux平台上推出](../Page/Linux.md "wikilink")\[2\]。[Android和](../Page/Android.md "wikilink")[iOS平台的重新編輯版本則在](../Page/iOS.md "wikilink")2015年5月5日發行。

## 遊戲系統

《神通鬼大》是第一個使用GrimE引擎的遊戲，其控制系統與只需滑鼠點擊的傳統冒險遊戲有某種程度的不同。它拋棄了使用滑鼠移動人物、與周圍物件互動的介面，而使用方向鍵甚至是搖桿來操作主角－曼紐·卡拉貝拉（Manuel
Calavera），簡稱曼尼（Manny）。

在不同場景中走動時，曼尼會轉頭盯着可以與之互動的物件，接着玩家可按下輸入鍵命令曼尼與之互動。按下「I」鍵可開啟物品選單，選單中的物品可用方向鍵切換，若按下輸入鍵可將它拿出曼尼的口袋。當手持物品欄中的物品時，曼尼可以用一般的互動方式來使用物品與場景互動。這種新型的互動方式不僅讓神通鬼大在當時的冒險遊戲中獨樹一幟，並提供了獨特、創新的遊戲方式。

## 遊戲劇情

遊戲背景設定在[阿茲特克神話的死後世界中](../Page/阿茲特克神話.md "wikilink")，描述了曼尼在死者國度（Land of
the
Dead）中的四年之旅，目的地為阿茲特克冥界中所有死者靈魂的終點站－[米克特蘭](../Page/米克特蘭.md "wikilink")（Mictlan）。

故事展開為四個片段，這些片段設定在[亡靈節](../Page/亡靈節.md "wikilink")（Day of the
Dead，墨西哥的萬聖節）後的每一年。遊戲中的多數意像是來自這個節日—大多數的遊戲角色是骷髏狀的「[卡拉維拉人偶](../Page/卡拉維拉人偶.md "wikilink")」形象（以墨西哥版畫家[José
Guadalupe
Posada的作品為依據](../Page/José_Guadalupe_Posada.md "wikilink")，而卡拉維拉人偶是墨西哥死者之日中骷髏狀的裝飾物）。各式各樣的花朵也成為了殺人的工具，一種被稱為「開花素」（Sproutella）的物質會與骨骼起反應，藉由快速地在骨骼中生長，達到摧毀骨骼的目的，遊戲中的人物將這種死法稱為「開花」（sprouting）。遊戲中也遍佈了獨特的動物，比如說會吃骨頭的火海狸，以及專門競速用的巨貓。

與眾不同的是，遊戲揉合了神話中的陰間、以及1930年代[装饰艺术运动](../Page/装饰艺术运动.md "wikilink")（Art
Deco）的設計主題、加上引起[黑色電影](../Page/黑色電影.md "wikilink")（film
noir）類別聯想的灰暗情節。曼尼的工作結合了旅行代理人以及[死神](../Page/死神.md "wikilink")（Grim
Reaper）的角色，而後來又扮演了偵探的角色，在他發現應得善報的人被駁回了死後應有的獎賞－跳過其他靈魂必經的四年之旅，直達米克特蘭。

在遊戲的第二個部份中，曼尼經營夜總會的情節是受到了[亨弗莱·鲍嘉電影](../Page/亨弗莱·鲍嘉.md "wikilink")《[梟巢喋-{}-血戰](../Page/梟巢喋血戰.md "wikilink")》、《[蓋世梟雄](../Page/蓋世梟雄.md "wikilink")》（Key
Largo）、以及《[北非諜影](../Page/北非諜影.md "wikilink")》的啟發。遊戲中，賭徒Chowchilla
Charlie與[彼得·羅瑞](../Page/彼得·羅瑞.md "wikilink")（Peter
Lorre）極端地神似，城鎮中貪污的警長是依據[克勞德·雷恩斯所扮演的雷諾隊長](../Page/克勞德·雷恩斯.md "wikilink")（Captain
Renault）。儘管如此，[Tim Schafer](../Page/Tim_Schafer.md "wikilink")
本人提到，遊戲其實是受到了像是電影《[雙重保險](../Page/雙重保險.md "wikilink")》（Double
Indemnity）的啟發，電影中一個懦弱且平庸的人（是個[保險](../Page/保險.md "wikilink")[推銷員](../Page/推銷員.md "wikilink")，而非偵探）牽涉謀殺以及陰謀之中。\[3\]

## 角色

大多數的角色為拉丁美洲裔，英文對話中偶爾綴以西班牙文。大部分的角色會吸菸（主要為了戲劇效果）。

**曼紐（曼尼）·卡拉貝拉（Manuel "Manny" Calavera）**，配音員：Tony Plana

為了[贖他生前的罪](../Page/救赎.md "wikilink")，曼尼在El Marrow中一個城市的死亡部（Department of
Death）做旅行代理人的工作，藉由販賣旅行套件來賺取往陰間的通行證。旅行套件的範圍從奢華的9號列車四分鐘之旅，到一根堅固的柺杖加上為期四年的徒步旅行。曼尼會爭取好客戶，他們純潔無瑕的生命替他們掙來了往陰間的快速通行，進而替自己爭取更快速的旅行方式，同時也提升了部門的名聲。但很不幸地，所有的好客戶都給曼尼的職場競爭對手多明諾（Domino）搶去了，經過一番努力，曼尼發現了使自己陷入這樁黑暗陰謀的原因。

**梅塞德斯（米契）·克羅瑪（Mercedes "Meche" Colomar）**，配音員：Maria Canals

米契（Meche）是個典型的受難少女（damsel in
distress）。曼尼對她一見鍾情。儘管她生前純潔無瑕，死亡部仍神秘地未批准她9號列車的票，曼尼只得任由她獨自徒步旅行至陰間。被死亡部開除之後，曼尼盡全力地尋找她的行蹤。

[Grim_fandango_screenshot.jpg](https://zh.wikipedia.org/wiki/File:Grim_fandango_screenshot.jpg "fig:Grim_fandango_screenshot.jpg")
**葛拉提斯（Glottis）**，配音員：Alan Blumenfeld
遊戲中的喜劇效果兼配角。葛拉提斯是個巨大的橘色惡魔，他在死亡部擔任技工一職，並充當曼尼的司機。當他被死亡部開除後，成為曼尼在尋找米契時的朋友與同伴。

**Hector LeMans**，配音員：Jim Ward
遊戲中的[反派](../Page/反派.md "wikilink")。Hector LeMans是陰間El
Marrow罪惡世界中的首腦，專門販售車票以獲取暴利。在追隨者的幫助之下，他從那些應得善報的死者手上偷取9號列車的票，並將車票賣給有錢人。他在遊戲中作風殘酷，是個不容小覷的對象。他以將數以百計的人「開花
」為著名，他擁有一個草原圍繞的溫室，草原上存放著他將敵人「開花 」過後的遺體。

**Salvador "Sal" Limones**，配音員：[Sal
Lopez](../Page/Sal_Lopez.md "wikilink")
薩爾瓦多是失魂聯盟 （Lost Souls' Alliance，簡稱LSA）的領袖。該機構是個不擇手段以求打倒Hector
LeMans的地下組織。薩爾瓦多為了在死亡部中能有個內應而招募了曼尼。在曼尼尋找米契時，薩爾瓦多還幫助了他逃離El
Marrow。

## 製作

《神通鬼大》只發行CD版本，並配以全程語音。[Tim
Schafer是此遊戲的設計師](../Page/Tim_Schafer.md "wikilink")，以及《[瘋狂時代](../Page/瘋狂時代.md "wikilink")》（Day
of the Tentacle）的協同設計師，並為《[極速天龍](../Page/極速天龍.md "wikilink")》（Full
Throttle）以及最近《[精神世界](../Page/精神世界.md "wikilink")》（Psychonauts）的製作人。

《神通鬼大》是LucasArts為了復甦圖形冒險遊戲類別所做的一次大膽嘗試，這個遊戲類別在1998年時跌到了谷底，加上一些在視覺上更新奇、更嘆為觀止的遊戲逐漸流行，像是[第一人称射击游戏](../Page/第一人称射击游戏.md "wikilink")。此遊戲是LucasArts自從《Labyrinth》以外，第一個不使用[SCUMM引擎的冒險遊戲](../Page/SCUMM.md "wikilink")，它使用了新型的[GrimE引擎](../Page/GrimE.md "wikilink")，而不是《[死星戰將2：絕地武士](../Page/死星戰將2：絕地武士.md "wikilink")》（Jedi
Knight: Dark Forces
II）所率先使用的[Sith引擎](../Page/Sith.md "wikilink")。GrimE引擎的3D人物及控制系統在外觀與感覺方面和SCUMM引擎的遊戲截然不同，即使它在謎題與對話風格上保留了LucasArts的傳統（主角殺不死的特徵，以及不會完全卡關）。

## 各方評價

《神通鬼大》獲得了近乎一面倒的正面評價，[GameSpot對其劇情讚賞有加](../Page/GameSpot.md "wikilink")，提到「神通鬼大很謝天謝地的避免了八股的內容」且「針對遊戲背景與角色發展它獨有的幽默……在沒有自捅簍子的情況下，創造了這個可信的世界。\[4\]」[PCZone則著重在其製作的完整性](../Page/PCZone.md "wikilink")，「專家級的執導、服裝、角色、與氛圍，神通鬼大其實可以拍成一部絕佳的電影。\[5\]」[Game-Revolution的評論中則以曼尼自己的口吻解說](../Page/Game-Revolution.md "wikilink")，「就藝術成就來說，我給我的冒險五根大腿骨。\[6\]」而[IGN將其評論以此做總結](../Page/IGN.md "wikilink")：「就我所能形容，神通鬼大絕對是我們所見過最佳的冒險遊戲。\[7\]」

另一方面，在《女性玩家》（WomenGamers）的評論中，雖然激賞它「創新的故事情節、角色、以及墨西哥風情的場景」，但感到遊戲介面窒礙難行，提到它令評論員感到「比較像是因為這些限制，而使得遊戲操控性不足。」\[8\]，GameSpot與IGN也為此作出小幅度的批評。

評價儘管不低，《神通鬼大》並未如先前冒險遊戲一般地大賣，且許多人形容這款遊戲是將冒險遊戲封入棺材的最後一根釘子。悲觀論者將此歸諸於，儘管他的高品質以及所得獎項－包括Gamespot的1998年度風雲電腦遊戲，擊敗了其他的經典作，像是即時戰略遊戲《[星海爭霸](../Page/星海爭霸.md "wikilink")》、美式RPG復興之作《[柏德之門](../Page/柏德之門.md "wikilink")》，以及第一人稱射擊遊戲《[戰慄時空](../Page/戰慄時空.md "wikilink")》－它賣得並不好。因此破壞了未來數年對冒險遊戲的需求之形象。

### 獎項

[GameSpot](../Page/GameSpot.md "wikilink")

  - [列入史上最佳遊戲](http://www.gamespot.com/gamespot/features/all/greatestgames/p-19.html)（2003年）
  - [電子遊戲博覽會（E3）最佳電腦遊戲](http://www.gamespot.com/features/e398/gri.html)（1998年）
  - [年度最佳電腦冒險遊戲](http://www.gamespot.com/features/awards1998/genre2b.html)（1998年）
  - [年度風雲電腦遊戲](http://www.gamespot.com/features/awards1998/gameofyear2.html)（1998年）
  - [最佳電腦遊戲圖像（藝術設計）](http://www.gamespot.com/features/awards1998/special2.html)（1998年）
  - [最佳電腦遊戲音樂](http://www.gamespot.com/features/awards1998/special.html)（1998年）
  - [十大最佳電腦遊戲原聲帶](http://www.gamespot.com/features/tenspot_music/page5.html)（1999年）
  - [讀者票選十大最佳電腦遊戲結局](http://www.gamespot.com/features/tenspot_readers_endings/1.html)
    第十名（1999年）

[IGN](../Page/IGN.md "wikilink")

  - [年度最佳冒險遊戲](http://pc.ign.com/articles/066/066665p1.html)（1998年）

[GameSpy](../Page/GameSpy.md "wikilink")

  - [史上二十五大被低估的遊戲](http://archive.gamespy.com/articles/september03/25underrated/index20.shtml)第七名（2003年）
  - [名作堂（Hall of
    Fame）](http://www.gamespy.com/articles/505/505156p1.html)（2004年）

<!-- end list -->

  - [冒險遊戲家（AdventureGamers）](http://www.adventuregamers.com/index.php)
  - [史上前二十大冒險遊戲](http://www.adventuregamers.com/display.php?id=186&p=15)第七名（2004年）

## 參見

  - [LucasArts冒險遊戲](../Page/LucasArts冒險遊戲.md "wikilink")
  - [ScummVM](../Page/ScummVM.md "wikilink")

## 參考資料

## 外部連結

  - [Grim Fandango Network](http://www.grimfandango.net)－《神通鬼大》遊戲迷架設的網站

  - [神通鬼大啟動程式](http://quick.mixnmojo.com/software.php#grimlauncher)－一個替代／改進版的《神通鬼大》啟動程式

  - [ResidualVM](http://residualvm.org/)－可在現代作業系統上執行《神通鬼大》的程式

  -
[Category:1998年电子游戏](../Category/1998年电子游戏.md "wikilink")
[G](../Category/冒險遊戲.md "wikilink")
[Category:LucasArts遊戲](../Category/LucasArts遊戲.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:魔幻寫實主義](../Category/魔幻寫實主義.md "wikilink")
[Category:死神题材作品](../Category/死神题材作品.md "wikilink")
[Category:不死生物题材作品](../Category/不死生物题材作品.md "wikilink")
[Category:死後世界背景作品](../Category/死後世界背景作品.md "wikilink")
[Category:僅有單人模式的電子遊戲](../Category/僅有單人模式的電子遊戲.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink") [Category:Double
Fine Productions](../Category/Double_Fine_Productions.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.