[Ejaculation_educational_seq_4.png](https://zh.wikipedia.org/wiki/File:Ejaculation_educational_seq_4.png "fig:Ejaculation_educational_seq_4.png")

**射精**，是[雄性](../Page/雄性.md "wikilink")[动物](../Page/动物.md "wikilink")[交配行为时將](../Page/交配.md "wikilink")[精液射出的反射性動作](../Page/精液.md "wikilink")，
[阴茎等雄性器官受](../Page/阴茎.md "wikilink")[性愛動作或](../Page/人类性行为.md "wikilink")[自慰等](../Page/自慰.md "wikilink")[刺激而興奮至](../Page/性刺激.md "wikilink")[極致時](../Page/性高潮.md "wikilink")，生殖系統管道的強烈收縮，使精液隨着射出體外的過程。

## 過程、現象

[Spontaneous-Ejaculation-in-a-Wild-Indo-Pacific-Bottlenose-Dolphin-(Tursiops-aduncus)-pone.0072879.s001.ogv](https://zh.wikipedia.org/wiki/File:Spontaneous-Ejaculation-in-a-Wild-Indo-Pacific-Bottlenose-Dolphin-\(Tursiops-aduncus\)-pone.0072879.s001.ogv "fig:Spontaneous-Ejaculation-in-a-Wild-Indo-Pacific-Bottlenose-Dolphin-(Tursiops-aduncus)-pone.0072879.s001.ogv")
射精過程雖然只發生在短暫的數秒之内，但其過程却十分複雜，等到射精結束後會心裡會有性滿足，依發生的時間順序可分為三部份，第一部份為洩精（Emission）即精液由副睪丸尾部、[輸精管精囊及](../Page/輸精管.md "wikilink")[攝護腺排至後尿道區](../Page/攝護腺.md "wikilink")。第二部份為後尿道和膀胱相接處的[膀胱頸口通过閉合](../Page/膀胱.md "wikilink")，以使精液不會逆向流入膀胱。第三部份為射精，當精液洩到後尿道區，使得後尿道區膨脹，後尿道區周圍的感覺神經接受體會感覺出這種膨脹，而將訊息傳到薦骨神經，然後由薦骨神經細胞體發出指令，引起後尿道區周圍及骨盆腔底部的肌肉作收縮运动，並將精液射出體外。

通常男性在射精之前龜頭還會有透明的[尿道球腺液分泌](../Page/尿道球腺液.md "wikilink")，有利後續射精。

人類男性因性高潮而達到射精階段時，通常會伴随[抽搐及呼吸急促等現象](../Page/抽搐.md "wikilink")。同時[生殖器也會有極度](../Page/生殖器.md "wikilink")[興奮的生理反應出現](../Page/性刺激#人類性興奮的生理反應.md "wikilink")。當精液由[尿道排出體外後](../Page/尿道.md "wikilink")，血液开始从陰莖之[海綿體消退](../Page/海綿體.md "wikilink")，陰莖随之收縮。且通常收縮後[龜頭的敏感度亦会增加許多](../Page/龜頭.md "wikilink")。

人類男性在射精之後的一段時間之內，會對性刺激不再發生性反應。這段時間稱為[不應期](../Page/不应期_\(性\).md "wikilink")\[1\]。

## 精液

[缩略图](https://zh.wikipedia.org/wiki/File:Male_anatomy_zh.svg "fig:缩略图")
精液在男性体内是以液体形式存在的。射精前，精囊液產生一種物質促進精液凝固，作用是保護精子，防止有害物質損害和破壞精子。凝固為果凍狀也是為了有助射精後精液在陰道內停留，不會太容易從陰道口流出體外，增加受孕的機會。精液射出后会凝结成胶冻状或块状乳白色物质，5分钟左右后精液将再次液化，因此部分精液会从被射入的阴道内排出体外。

第一次射精通常發生在[男性](../Page/男性.md "wikilink")[青春期开始后一年左右](../Page/青春期.md "wikilink")，一般是通過[手淫或](../Page/手淫.md "wikilink")[夢遺而出现](../Page/夢遺.md "wikilink")。第一次射精（以下簡稱「[初精](../Page/初精.md "wikilink")」）量通常很少，在接下來的3個月生產不到1[毫升的精液](../Page/毫升.md "wikilink")，通常性状较为稀薄。性成熟初期的精液呈現果凍狀，不像[成熟男性精液能液化](../Page/成熟.md "wikilink")\[2\]。

## 症候群

  - [性高潮後病情症候群](../Page/性高潮後病情症候群.md "wikilink")：男性對精液產生[过敏](../Page/过敏.md "wikilink")，於性高潮及射精後出現類似感冒的症狀，包括發燒、流鼻水、極度疲倦和眼睛刺痛等，時間維持數分鐘至一周不等。2011年的研究指出，以稀釋的精液向受訪者進行皮膚敏感測試，結果九成人出現過敏反應。

## 參見

## 參考資料

## 外部链接

  - [Swimming Toward
    Conception：精液分析](https://web.archive.org/web/20080112144449/http://www.focusonfertility.org/resource_swimming_conception.htm)

[Category:男科學](../Category/男科學.md "wikilink")
[Category:生育力](../Category/生育力.md "wikilink")
[精](../Category/性高潮.md "wikilink")
[Category:泌尿科](../Category/泌尿科.md "wikilink")
[Category:男性生殖系统](../Category/男性生殖系统.md "wikilink")
[Category:阴茎](../Category/阴茎.md "wikilink")
[Category:精液](../Category/精液.md "wikilink")
[Category:男色](../Category/男色.md "wikilink")

1.  [男子性高潮后有段“不应期”](http://health.sohu.com/20050311/n224617859.shtml)．搜狐健康，2005年3月17日．2010年2月2日查阅
2.