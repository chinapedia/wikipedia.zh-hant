**妙蛙種子**（，，香港舊譯**奇異種子**）是[精靈寶可夢系列登場的](../Page/精靈寶可夢.md "wikilink")種虛構[角色](../Page/角色.md "wikilink")（[精靈寶可夢](../Page/精靈寶可夢.md "wikilink")）中的一種。

## 特徵

妙蛙種子為草屬性及毒屬性的種子[寶可夢](../Page/精靈寶可夢系列.md "wikilink")，有如[青蛙般的身體上背著大顆充滿營養的](../Page/青蛙.md "wikilink")[種子](../Page/種子.md "wikilink")，與種子是共生關係。這顆種子從出生時開始種植，出生後暫時由種子中獲取養分成長。成為[妙蛙草後由中生長出草](../Page/妙蛙草.md "wikilink")，成為[妙蛙花後開花](../Page/妙蛙花.md "wikilink")。\[1\]\[2\]

身體顏色主要由青色及綠色組成。

## [遊戲中的妙蛙種子](../Page/神奇寶貝遊戲列表.md "wikilink")

初次登場於精靈寶可夢紅／綠版，為『精靈寶可夢 紅・綠・藍版』和『精靈寶可夢
火紅・葉綠版』中最先可挑選的[寶可夢](../Page/精靈寶可夢.md "wikilink")（妙蛙種子、[小火龍](../Page/小火龍.md "wikilink")、[傑尼龜](../Page/傑尼龜.md "wikilink")）。等級16級進化成[妙蛙草](../Page/妙蛙草.md "wikilink")。全國圖鑑及關都圖鑑中編號為001，是位於最前面的寶可夢。

寶石版編號為203，代表技能為寄生種子（）及藤鞭（），特性為茂盛（）。

『精靈寶可夢 黃版』中，[皮卡丘在親密狀態時與華藍市](../Page/皮卡丘.md "wikilink")（）民家裡的女性交談可得到。

等級16級時進化為[妙蛙草](../Page/妙蛙草.md "wikilink")。

## 動畫中的妙蛙種子

妙蛙種子為關都地區（）中，給新人訓練家的三隻神奇寶貝之一。[動畫版中](../Page/動畫.md "wikilink")，主角[小智也有一隻](../Page/小智.md "wikilink")，是在十集中從一個民間的護士取得，個性負責、擅於領導、頑固，目前於[大木博士研究所中維持寶可夢間的和平](../Page/大木博士.md "wikilink")。在第51集中，小智的妙蛙種子已經有進化的條件，但牠與[皮卡丘一樣不願意進化](../Page/皮卡丘.md "wikilink")，但這亦讓牠早一點學會日光束（舊稱陽光烈焰）。
而豐緣聯盟篇的女主角小遙亦有一隻，牠額頭上有個心形斑紋，目前也在大木博士研究所中，後來進化成妙蛙花。

## 漫画中的妙蛙種子

在一个基于游戏[红·绿改编的漫画](../Page/口袋妖怪_红·绿.md "wikilink")[神奇寶貝特別篇中](../Page/神奇寶貝特別篇.md "wikilink")，主角小智从大木博士那里拿到一只妙蛙种子，并起昵称叫“妙蛙”。在第15章“VS卡咪龟”中，它在对战猴怪后进化成[妙蛙草](../Page/妙蛙草.md "wikilink")。

## 參考

## 外部链接

[妙蛙种子 -
神奇宝贝百科](http://wiki.52poke.com/wiki/%E5%A6%99%E8%9B%99%E7%A7%8D%E5%AD%90#.E5.8F.AF.E4.B9.A0.E5.BE.97.E6.8A.80.E8.83.BD.E8.A1.A8)

[ca:Línia evolutiva de
Bulbasaur\#Bulbasaur](../Page/ca:Línia_evolutiva_de_Bulbasaur#Bulbasaur.md "wikilink")
[cs:Seznam pokémonů
(1-20)\#Bulbasaur](../Page/cs:Seznam_pokémonů_\(1-20\)#Bulbasaur.md "wikilink")
[da:Pokémon
(1-20)\#Bulbasaur](../Page/da:Pokémon_\(1-20\)#Bulbasaur.md "wikilink")
[es:Anexo:Pokémon de la primera
generación\#Bulbasaur](../Page/es:Anexo:Pokémon_de_la_primera_generación#Bulbasaur.md "wikilink")
[fi:Luettelo Pokémon-lajeista
(1–20)\#Bulbasaur](../Page/fi:Luettelo_Pokémon-lajeista_\(1–20\)#Bulbasaur.md "wikilink")
[fr:Bulbizarre et ses
évolutions\#Bulbizarre](../Page/fr:Bulbizarre_et_ses_évolutions#Bulbizarre.md "wikilink")

[Category:第一世代寶可夢](../Category/第一世代寶可夢.md "wikilink")
[Category:草屬性寶可夢](../Category/草屬性寶可夢.md "wikilink")
[Category:毒屬性寶可夢](../Category/毒屬性寶可夢.md "wikilink")
[Category:1996年推出的電子遊戲角色](../Category/1996年推出的電子遊戲角色.md "wikilink")

1.  **图鉴:** 它出生的时候背上就有一个奇怪的种子。这种子跟随它发芽成长。
2.  **图鉴:**