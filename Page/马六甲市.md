马六甲州| 南北=北|纬度=2|纬分=11|纬秒=| 东西=东|经度=102|经分=23|经秒=| 海拔=| 面积=303|
人口=180,671人| 统计时间=2005年| 市长=| 时区=UTC +8|
地图档名=MapMalaysiaMalaccaTown.png| 大小=300px| 网址=www.mbmb.gov.my|
}}
**马六甲市**（）是[马来西亚](../Page/马来西亚.md "wikilink")[马六甲州的首府与南部大城](../Page/马六甲.md "wikilink")。州行政与发展中心包括首席部长办公室、立法会和荷兰红屋都位于马六甲市。2008年7月8日马六甲市及[槟城州首府](../Page/槟城.md "wikilink")[乔治市共同被列入](../Page/乔治市.md "wikilink")[世界文化遗产名录](../Page/世界文化遗产.md "wikilink")。

## 世界遗产

[Melaka_Malaysia_Majlis-Bandaraya-Melaka-Bersejarah-02.jpg](https://zh.wikipedia.org/wiki/File:Melaka_Malaysia_Majlis-Bandaraya-Melaka-Bersejarah-02.jpg "fig:Melaka_Malaysia_Majlis-Bandaraya-Melaka-Bersejarah-02.jpg")

## 友好城市

  - [葡萄牙](../Page/葡萄牙.md "wikilink")[里斯本](../Page/里斯本.md "wikilink")（1984年1月16日）

  - [马来西亚](../Page/马来西亚.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")（1989年4月15日）

  - [荷兰](../Page/荷兰.md "wikilink")[荷恩](../Page/荷恩.md "wikilink")（1989年11月8日）

  - [智利](../Page/智利.md "wikilink")[瓦尔帕莱索](../Page/瓦尔帕莱索.md "wikilink")（1991年6月23日）

  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[南京](../Page/南京.md "wikilink")（2001年）

## 观光景点

  - [法摩沙堡](../Page/法摩沙堡.md "wikilink")[A_Famosa_Fortress.JPG](https://zh.wikipedia.org/wiki/File:A_Famosa_Fortress.JPG "fig:A_Famosa_Fortress.JPG")
  - [鸡场街文化坊](../Page/鸡场街.md "wikilink")

[Jonker_Street@Malacca.JPG](https://zh.wikipedia.org/wiki/File:Jonker_Street@Malacca.JPG "fig:Jonker_Street@Malacca.JPG")

  - [圣保罗堂](../Page/圣保罗堂.md "wikilink")

[Melaka_Malaysia_St-Paul's-Church-01.jpg](https://zh.wikipedia.org/wiki/File:Melaka_Malaysia_St-Paul's-Church-01.jpg "fig:Melaka_Malaysia_St-Paul's-Church-01.jpg")

  - [淡敏沙里塔](../Page/淡敏沙里塔.md "wikilink")[Taming_Sari_Tower.JPG](https://zh.wikipedia.org/wiki/File:Taming_Sari_Tower.JPG "fig:Taming_Sari_Tower.JPG")
  - [基督堂](../Page/基督堂_\(马六甲\).md "wikilink")[Firstchurchmelaka.JPG](https://zh.wikipedia.org/wiki/File:Firstchurchmelaka.JPG "fig:Firstchurchmelaka.JPG")
  - [三保山](../Page/三保山.md "wikilink")[马六甲三宝庙.JPG](https://zh.wikipedia.org/wiki/File:马六甲三宝庙.JPG "fig:马六甲三宝庙.JPG")
  - [青云亭](../Page/青云亭.md "wikilink")[ChengHoonTeng.jpg](https://zh.wikipedia.org/wiki/File:ChengHoonTeng.jpg "fig:ChengHoonTeng.jpg")
  - [荷兰广场](../Page/广场.md "wikilink")[Melaka-Dutch-Square-2201.jpg](https://zh.wikipedia.org/wiki/File:Melaka-Dutch-Square-2201.jpg "fig:Melaka-Dutch-Square-2201.jpg")
  - [马六甲保安宫](../Page/马六甲保安宫.md "wikilink")

## 外部链接

  - [Malacca Tourist Attraction](http://www.worldheritage.com.my/)
  - [Malacca Guide](http://www.malaccaguide.com)
  - [Photographs of
    Malacca](https://web.archive.org/web/20070312223735/http://gallery.sindh.ws/v/malaysia/malacca/)
  - [Melaka.TV](http://melaka.tv/)

[Category:马来西亚世界遗产](../Category/马来西亚世界遗产.md "wikilink")
[Category:马来西亚港口](../Category/马来西亚港口.md "wikilink")
[Category:马来西亚城市](../Category/马来西亚城市.md "wikilink")
[Category:马六甲](../Category/马六甲.md "wikilink")