**汪禹**（**Wong Yu**或**Wong
Yue**，），原名**汪志權**，祖籍[江蘇](../Page/江蘇.md "wikilink")，香港功夫片[演員](../Page/演員.md "wikilink")，是導演[李翰祥的契仔](../Page/李翰祥.md "wikilink")（義子），導演[劉家良的弟子](../Page/劉家良.md "wikilink")（1975年拜師），藝名汪禹是契爺（義父）李導演給他改的。

## 生平簡介

汪禹於1970年代入電影行，第1部作品是[吳思遠執導的](../Page/吳思遠.md "wikilink")《[蕩寇灘](../Page/蕩寇灘.md "wikilink")》，做武打演員和主演[陳觀泰的替身](../Page/陳觀泰.md "wikilink")。

1972年他簽了[邵氏電影公司約](../Page/邵氏.md "wikilink")，正式踏進電影界，做主演的第1部是[李翰祥執導的](../Page/李翰祥.md "wikilink")《北地胭脂》。當時功夫片正流行，汪禹經常以武打明星的形象出現，他1975年憑著師父[劉家良執導的](../Page/劉家良.md "wikilink")《[神打](../Page/神打.md "wikilink")》一片而走紅。隨後其與[傅聲](../Page/傅聲.md "wikilink")、[劉家輝等師兄領銜主演多部片](../Page/劉家輝.md "wikilink")，《功夫小子》（[劉家榮執導](../Page/劉家榮.md "wikilink")）、《[通天小子紅槍客](../Page/通天小子紅槍客.md "wikilink")》、《[少林三十六房](../Page/少林三十六房.md "wikilink")》、《[請帖](../Page/請帖.md "wikilink")》、《[洪熙官](../Page/洪熙官.md "wikilink")》、《[黃飛鴻與陸阿采](../Page/黃飛鴻與陸阿采.md "wikilink")》、《[鹿鼎记](../Page/鹿鼎记.md "wikilink")》、《[千王鬥千霸](../Page/千王鬥千霸.md "wikilink")》、《[贼王之王](../Page/贼王之王.md "wikilink")》等其餘的作品也相當突出，在1970年代很紅火，其功夫小子形象混合了喜劇元素，在當時是創新和突破，劉家良、汪禹師徒可說是功夫喜劇的先行者。

1980年代在[英屬香港拍電影電視劇以外還到](../Page/英屬香港.md "wikilink")[台灣主演電視劇](../Page/台灣.md "wikilink")，1980年領銜主演[唐偉成執導的](../Page/唐偉成.md "wikilink")《扮嘢小子》時還用**汪小禹**名做動作設計（與唐導演合作），1988年以後仍演出電視劇和做節目嘉賓，電影只做幕後工作，主要是做龍虎武師和動作設計，幕後作品有與[程小東合作的](../Page/程小東.md "wikilink")《東方不敗2》等。

汪禹在1983年曾與當時著名武打演員[傅聲飙車而發生車禍](../Page/傅聲.md "wikilink")。該事裡傅声因傷重不治。此事使汪禹受千夫所指，不久更染上了毒癮。在1988年以後，汪禹幾乎再沒有參演任何電影，沉淪了一段時期。一些媒體常報道稱汪禹涉及很多財務和暴力糾紛，但都被當事者否認。

他於2003年曾以暴力毆打前女友而被判入獄六星期；2005年則因涉嫌襲擊的士司機被捕。此後又曾做過兩次心臟手術。2008年5月5日因急性肝炎入院，經搶救後，終於在5月15日宣告去世，享年53歲。\[1\]
\[2\]

汪禹未婚，育有一位[女兒](../Page/女兒.md "wikilink")。導演[陳德森是汪禹的表弟](../Page/陳德森.md "wikilink")，導演[程小東是汪禹的妹夫](../Page/程小東.md "wikilink")。

## 參與作品

### 電影

  - 1973年 北地胭脂 / Facets of Love 〔飾-[同治皇帝](../Page/同治皇帝.md "wikilink")〕
  - 1974年 香港73 / Hong Kong 73
  - 1974年 金瓶雙艷 / The Golden Lotus
  - 1974年 成記茶樓 / The Tea House 〔飾-黑仔文〕
  - 1975年 大哥成 / Big Brother Cheng 〔飾-黑仔文〕
  - 1975年 血滴子 / The Flying Guillotine 〔飾-謝天復〕
  - 1975年 捉姦趣事 / That's Adultery\!
  - 1975年 神打 / The Spiritual Boxer
  - 1976年 賭王大騙局 / King Gambler
  - 1976年 瀛台泣血 / The Last Tempest 〔飾-張進喜〕
  - 1976年 陸阿采與黃飛鴻 / Challenge of the Masters
  - 1976年 香港奇案 / The Criminals
  - 1976年 乾隆皇奇遇記 / Emperor Chien Lung 〔飾-周日青〕
  - 1976年 蛇王子 / The Snake Prince 〔飾-黃蛇〕
  - 1977年 洪熙官 / Executioners from Shaolin
    〔飾-[陸阿采](../Page/陸阿采.md "wikilink")〕
  - 1977年 乾隆下江南 / The Adventures of Emperor Chien Lung
  - 1977年 功夫小子 / He Has Nothing But Kung Fu
  - 1977年 應召名冊 / The Call Girls
  - 1978年 少林三十六房 / The 36th Chamber of Shaolin
    〔飾-[舂米六](../Page/舂米六.md "wikilink")〕
  - 1978年 [笑傲江湖](../Page/笑傲江湖_\(1978年電影\).md "wikilink") / The Proud
    Youth 〔飾-[令狐沖](../Page/令狐沖.md "wikilink")〕
  - 1978年 鬼馬功夫 / Dirty Kung Fu
  - 1979年 茅山殭屍拳 / The shadow boxing 〔飾-范振元〕
  - 1979年 教頭 / The Kung-Fu Instructor 〔飾-周平〕
  - 1979年 爛頭何 / Dirty Ho 〔飾-何真〕
  - 1980年 扮嘢小子 / The Young Avenger 〔飾-傅友〕
  - 1980年 請帖 / Rendezvous with Death 〔飾-辛酸〕
  - 1980年 情俠追風劍 / Swift Sword 〔飾-夏侯曉桐〕
  - 1980年 通天小子紅槍客 / The Kid with a Tattoo 〔飾-李寶通〕
  - 1981年 千門八將 / Notorious Eight 〔飾-謠將〕
  - 1981年 辛亥雙十 / The Battle for the Republic of China 〔飾-余七〕
  - 1981年 千王鬥千霸 / Challenge of the Gamesters 〔飾-雷力〕
  - 1981年 南北獅王 / Lion Vs Lion 〔飾-阿寸〕
  - 1982年 廣東靚仔玉 / Kid from Kwangtung 〔飾-家玉〕
  - 1982年 獵魔者 / Mercenaries from Hong Kong 〔飾-咖喱〕
  - 1982年 賊王之王 / Winner Takes All 〔飾-關德興〕
  - 1983年 掌門人 / The Lady Is the Boss
  - 1983年 [鹿鼎記](../Page/鹿鼎記_\(1983年電影\).md "wikilink") / Tales of a
    Eunuch 〔飾-[韋小寶](../Page/韋小寶.md "wikilink")〕
  - 1983年 [五郎八卦棍](../Page/五郎八卦棍.md "wikilink") / The 8 Diagram Pole
    Fighter 〔飾-[楊大郎](../Page/楊延平.md "wikilink")〕
  - 1984年 南斗官三鬥北少爺 / Wits of the Brats 〔飾-時三手〕
  - 1985年 天官賜福 / How to Choose a Royal Bride 〔飾-魏小寶〕
  - 1985年 [鬼馬飛人](../Page/鬼馬飛人.md "wikilink") / The Flying Mr. B
  - 1985年 少年蘇乞兒 / The Young Vagabond
  - 1985年 弟子也瘋狂 / Crazy Shaolin Disciples
    〔飾-[方世玉](../Page/方世玉.md "wikilink")〕
  - 1988年 [飛龍猛將](../Page/飛龍猛將.md "wikilink") / Dragons Forever
  - 1988年 [胭脂扣](../Page/胭脂扣.md "wikilink") / Rouge

### 電視劇

  - 1988年 [滿清十三皇朝](../Page/滿清十三皇朝.md "wikilink") 飾
    [嘉慶帝](../Page/嘉慶帝.md "wikilink")

## 參考

[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:香港武打演員](../Category/香港武打演員.md "wikilink")
[Category:江蘇人](../Category/江蘇人.md "wikilink")
[Y](../Category/汪姓.md "wikilink")

1.  <http://www.atchinese.com/index.php?option=com_content&task=view&id=50287&Itemid=91>
2.  [汪禹病逝 走完53歲人生](http://udn.com/NEWS/ENTERTAINMENT/ENT8/4343858.shtml)