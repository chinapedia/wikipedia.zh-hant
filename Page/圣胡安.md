**圣胡安**（[西班牙语](../Page/西班牙语.md "wikilink")：****）是[圣约翰的西班牙语变体](../Page/圣约翰.md "wikilink")，也可以是[圣胡安包蒂斯塔的简称](../Page/圣胡安包蒂斯塔.md "wikilink")，可以指：

## 城市

### 阿根廷

  - [圣胡安
    (阿根廷)](../Page/圣胡安_\(阿根廷\).md "wikilink")，[阿根廷的一座城市](../Page/阿根廷.md "wikilink")；

### 墨西哥

  - [圣胡安
    (坎佩切州)](../Page/圣胡安_\(坎佩切州\).md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")[坎佩切州的一座城市](../Page/坎佩切州.md "wikilink")；
  - [圣胡安
    (奇瓦瓦州)](../Page/圣胡安_\(奇瓦瓦州\).md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")[奇瓦瓦州的一座城市](../Page/奇瓦瓦州.md "wikilink")；
  - [圣胡安
    (科阿韦拉州)](../Page/圣胡安_\(科阿韦拉州\).md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")[科阿韦拉州的一座城市](../Page/科阿韦拉州.md "wikilink")；
  - [圣胡安
    (圣路易斯波托西州)](../Page/圣胡安_\(圣路易斯波托西州\).md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")[圣路易斯波托西州的一座城市](../Page/圣路易斯波托西州.md "wikilink")；
  - [圣胡安
    (索诺拉州)](../Page/圣胡安_\(索诺拉州\).md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")[索诺拉州的一座城市](../Page/索诺拉州.md "wikilink")；

### 秘鲁

  - [圣胡安
    (秘鲁)](../Page/圣胡安_\(秘鲁\).md "wikilink")，[秘鲁的一座城市](../Page/秘鲁.md "wikilink")；

### 玻利维亚

  - [圣胡安
    (玻利维亚)](../Page/圣胡安_\(玻利维亚\).md "wikilink")，[玻利维亚的一座城市](../Page/玻利维亚.md "wikilink")；

### 古巴

  - [圣胡安
    (古巴)](../Page/圣胡安_\(古巴\).md "wikilink")，[古巴的一座城市](../Page/古巴.md "wikilink")；

### 多米尼加共和国

  - [圣胡安
    (多米尼加共和国)](../Page/圣胡安_\(多米尼加共和国\).md "wikilink")，[多米尼加共和国的一座城市](../Page/多米尼加共和国.md "wikilink")；

### 菲律宾

  - [圣胡安
    (阿布拉省)](../Page/圣胡安_\(阿布拉省\).md "wikilink")，[菲律宾](../Page/菲律宾.md "wikilink")[阿布拉省的一座城市](../Page/阿布拉省.md "wikilink")；
  - [圣胡安
    (八打雁省)](../Page/圣胡安_\(八打雁省\).md "wikilink")，[菲律宾](../Page/菲律宾.md "wikilink")[八打雁省的一座城市](../Page/八打雁省.md "wikilink")；
  - [圣胡安
    (南伊罗戈省)](../Page/圣胡安_\(南伊罗戈省\).md "wikilink")，[菲律宾](../Page/菲律宾.md "wikilink")[南伊罗戈省的一座城市](../Page/南伊罗戈省.md "wikilink")；
  - [圣胡安
    (拉乌尼翁省)](../Page/圣胡安_\(拉乌尼翁省\).md "wikilink")，[菲律宾](../Page/菲律宾.md "wikilink")[拉乌尼翁省的一座城市](../Page/拉乌尼翁省_\(菲律宾\).md "wikilink")；
  - [圣胡安
    (锡基霍尔省)](../Page/圣胡安_\(锡基霍尔省\).md "wikilink")，[菲律宾](../Page/菲律宾.md "wikilink")[锡基霍尔省的一座城市](../Page/锡基霍尔省.md "wikilink")；
  - [圣胡安
    (南莱特省)](../Page/圣胡安_\(南莱特省\).md "wikilink")，[菲律宾](../Page/菲律宾.md "wikilink")[南莱特省的一座城市](../Page/南莱特省.md "wikilink")；

### 美国

  - [圣胡安
    (波多黎各)](../Page/圣胡安_\(波多黎各\).md "wikilink")，[波多黎各首府](../Page/波多黎各.md "wikilink")；
  - [圣胡安
    (新墨西哥州)](../Page/圣胡安_\(新墨西哥州\).md "wikilink")，[美国](../Page/美国.md "wikilink")[新墨西哥州的一座城市](../Page/新墨西哥州.md "wikilink")；
  - [圣胡安
    (德克萨斯州)](../Page/圣胡安_\(德克萨斯州\).md "wikilink")，[美国](../Page/美国.md "wikilink")[德克萨斯州的一座城市](../Page/德克萨斯州.md "wikilink")；

### 特立尼达和多巴哥

  - [圣胡安
    (特立尼达和多巴哥)](../Page/圣胡安_\(特立尼达和多巴哥\).md "wikilink")，[特立尼达和多巴哥的一座城市](../Page/特立尼达和多巴哥.md "wikilink")。

## 行政区划名称

  - [圣胡安区
    (波多黎各)](../Page/圣胡安区_\(波多黎各\).md "wikilink")，[波多黎各的一个](../Page/波多黎各.md "wikilink")[区](../Page/波多黎各行政区划.md "wikilink")；
  - [圣胡安省
    (阿根廷)](../Page/圣胡安省_\(阿根廷\).md "wikilink")，[阿根廷的一个](../Page/阿根廷.md "wikilink")[省](../Page/阿根廷行政区划.md "wikilink")；
  - [圣胡安省
    (多米尼加共和国)](../Page/圣胡安省_\(多米尼加共和国\).md "wikilink")，[多米尼加共和国的一个](../Page/多米尼加共和国.md "wikilink")[省](../Page/多米尼加共和国行政区划.md "wikilink")；
  - [聖胡安縣](../Page/聖胡安縣.md "wikilink")，美国若干以圣胡安为名的县