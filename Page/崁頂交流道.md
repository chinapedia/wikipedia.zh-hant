**崁頂交流道**為[台灣](../Page/台灣.md "wikilink")[國道三號的交流道](../Page/福爾摩沙高速公路.md "wikilink")，位於台灣[屏東縣](../Page/屏東縣.md "wikilink")[崁頂鄉和](../Page/崁頂鄉.md "wikilink")[潮州鎮交界](../Page/潮州鎮.md "wikilink")，指標為421k，僅設南下出口及北上入口。
本交流道可銜接屏70線至崁頂市區。該鄉道亦為國道交流道的橫向聯絡道路中，路寬最窄的例子。\[1\]

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_THWpt70.svg" title="fig:TW_THWpt70.svg">TW_THWpt70.svg</a></p></td>
<td><p><strong>崁頂</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰近交流道

## 參考資料

[Category:屏東縣交流道](../Category/屏東縣交流道.md "wikilink")
[Category:崁頂鄉](../Category/崁頂鄉.md "wikilink")

1.  [台灣大百科 余風](http://nrch.culture.tw/twpedia.aspx?id=23606)(文化部)