《**最終兵器少女**》（），是一部[日本的](../Page/日本.md "wikilink")[漫画和](../Page/漫画.md "wikilink")[动画](../Page/动画.md "wikilink")[悲劇作品](../Page/悲劇.md "wikilink")，原作者為[高橋真](../Page/高橋真.md "wikilink")。由於推出之後大受歡迎，所以於2005年8月至9月推出[OVA版本](../Page/OVA.md "wikilink")《》（另一個愛情故事）\[1\]\[2\]，由[前田亞季主演的真人](../Page/前田亞季.md "wikilink")[电影版於](../Page/电影.md "wikilink")2006年1月28日在日本上映\[3\]。《最終兵器少女
Another love
song》獲選為2005年第九回[日本](../Page/日本.md "wikilink")[文部省](../Page/文部省.md "wikilink")[文化廳媒體藝術祭動畫部門推薦的作品](../Page/文化廳媒體藝術祭.md "wikilink")。

## 劇情簡介

故事的主角是一個[北海道的](../Page/北海道.md "wikilink")[學生修次](../Page/學生.md "wikilink")。他的同班同學千瀨在故事開始的時候就已向修次表白了。但因為千瀨的害羞性格和修次的不善辭令，所以他們就以寫[交換日記來開始他們的情侶關係](../Page/交換日記.md "wikilink")。

一日，當修次和朋友到[札幌購物的時候](../Page/札幌.md "wikilink")，不明來歷的[轰炸机對札幌進行了猛烈的轟炸](../Page/轰炸机.md "wikilink")。修次和他的朋友找地方作掩護時，他見到了一個快速的小型飛行物體向敵軍的轟炸機進行攻擊。修次和他的朋友失散了之後，他在廢墟堆中徘徊。當他在廢墟堆當中見到增生了翅膀、武器、不再心跳且衣著破爛的千瀨之後，千瀨告訴他，她已經成為[日本自卫队的](../Page/日本自卫队.md "wikilink")「最終兵器」，故事於是開始……

## 人物簡介

  -
    男主角，高中生，千瀨的男朋友，希望還給千瀨正常的生活。千瀨暱稱他為「阿修」。

<!-- end list -->

  -
    女主角，高中生，修次的同班同學，修次的女朋友，做事有些莽撞糊塗。
    軍隊在偶然的情況下找到身體最適合成為兵器的她改造成兵器，能力強大，發動千瀨之火時城市會被毀滅。

<!-- end list -->

  -
    [日本自衛隊隊長](../Page/日本自衛隊.md "wikilink")，冬美的[丈夫](../Page/丈夫.md "wikilink")，將千瀨當[人而非無感情的殺人兵器看待](../Page/人.md "wikilink")。

<!-- end list -->

  -
    阿鐵的妻子，曾是修次的任課老師和所屬田徑社的指導老師，常被田徑社的學生暱稱「冬美學姐」。
    由於阿鐵長期在部隊未歸，使冬美的寂寞感有增無減。

<!-- end list -->

  -
    高中生，修次的同班同學，千瀨的好友，很愛護千瀨，但亦暗戀著修次，淳志的女友。

<!-- end list -->

  -
    高中生，修次的同班同學，喜歡明美。後來為了保護明美，與她告白後退學加入[自衛隊](../Page/日本自卫队.md "wikilink")。因陰錯陽差的關係加入部隊後未與同梯次的士兵一樣見到千瀨而始終不知她與最終兵器的關係。在千瀨一次毀滅性攻擊中為撿回明美的相片而捲入爆炸中，生死未卜。

<!-- end list -->

  -
    OVA登場角色，[日本自衛隊中校](../Page/日本自衛隊.md "wikilink")。在一場戰役中失去了所有隊員和手腳各一，為繼續參戰同意做最終兵器開發的試作品。由於是國防大學畢業的正規軍人，戰鬥技巧比千瀨優秀，但因只是試作品所以性能極限不如千瀨。

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p><a href="../Page/小學館.md" title="wikilink">小學館</a></p></th>
<th><p><a href="../Page/尖端出版社.md" title="wikilink">尖端出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2000年5月30日</p></td>
<td><p>ISBN 4-09-185681-0</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2000年7月29日</p></td>
<td><p>ISBN 4-09-185682-9</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2000年11月30日</p></td>
<td><p>ISBN 4-09-185683-7</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2001年3月30日</p></td>
<td><p>ISBN 4-09-185684-5</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2001年6月30日</p></td>
<td><p>ISBN 4-09-185685-3</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2001年11月30日</p></td>
<td><p>ISBN 4-09-185686-1</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2001年12月25日</p></td>
<td><p>ISBN 4-09-185687-X</p></td>
</tr>
<tr class="odd">
<td><p>外傳</p></td>
<td><p>2006年7月19日</p></td>
<td><p>ISBN 4-09-180746-1</p></td>
</tr>
</tbody>
</table>

## 電視動畫

電視動畫版是由[東映錄影](../Page/東映錄影.md "wikilink")（）、[東北新社](../Page/東北新社.md "wikilink")、小學館、[中部日本放送](../Page/中部日本放送.md "wikilink")（CBC）合組的『「」[製作委員会](../Page/制作委员会方式.md "wikilink")』製作，是CBC參與製作的第一部動畫。

### 製作人員

  - 原作：[高橋真](../Page/高橋真.md "wikilink")（小學館「ビッグスピリッツコミックス」刊）
  - 企画製片人：藤原銀次郎（[東映ビデオ](../Page/東映.md "wikilink")）、瀬崎巌（東北新社）、植田文郎（[小學館](../Page/小學館.md "wikilink")）、村濱章司（[GONZO](../Page/GONZO.md "wikilink")）
  - 原案協助：堀靖樹、小室時恵、奥山豊彦（小学館「週刊ビッグコミックスピリッツ」編集部）
  - 系列構成：江良至
  - 人物設計：香川久
  - 機械設計：神戸洋行
  - 総作画監督：佐藤雅将
  - 美術監督：東潤一
  - 色彩設計：鈴木依里
  - 3DCG導演：松浦裕暁
  - 攝影監督：石黒晴嗣
  - 編集：重村建吾、肥田文
  - 音樂：見良津健雄
  - 音響監督：三好慶一郎
  - 音響效果：長谷川卓也（サウンドボックス）
  - 錄音制作：東北新社
  - 音樂製片人：安藤岳
  - 音樂制作／協力：radiosonic records、東芝EMI
  - 製片人：高橋尚子（[東映錄影](../Page/東映.md "wikilink")）、鶴崎りか（NAS）、山根博行（小学館）、加藤直次（[CBC](../Page/中部日本放送.md "wikilink")）、月野正志（GONZO）
  - 監督：加瀬充子
  - 動畫制作生產：[GONZO](../Page/GONZO.md "wikilink")
  - 製作：「」[製作委員会](../Page/制作委员会方式.md "wikilink")（東映錄影、東北新社、小学館、CBC）

### 主題曲

  - 片頭曲「戀愛的心情（）」

<!-- end list -->

  -
    演唱：[谷戶由李亞](../Page/谷戶由李亞.md "wikilink")、作詞・作曲：谷戶由李亞、編曲：[見良津健雄](../Page/見良津健雄.md "wikilink")

<!-- end list -->

  - 片尾曲「再會（）」

<!-- end list -->

  -
    演唱：谷戶由李亞、作詞・作曲：谷戶由李亞、編曲：見良津健雄

<!-- end list -->

  - 劇中歌「為了那夢想（）」

<!-- end list -->

  -
    演唱：[杉内光雅](../Page/杉内光雅.md "wikilink")、作詞：[石川あゆ子](../Page/石川あゆ子.md "wikilink")、作曲・編曲：見良津健雄
    (radiosonic records)

<!-- end list -->

  - 插入歌「體溫（）」

<!-- end list -->

  -
    演唱：谷戶由李亞、作詞・作曲：谷戶由李亞

### 各話列表

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>中文標題</p></th>
<th><p>日文標題</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第1話</p></td>
<td><p>我們將繼續相戀</p></td>
<td></td>
<td><p><a href="../Page/加瀨充子.md" title="wikilink">加瀨充子</a></p></td>
<td><p>山田弘和</p></td>
<td><p><a href="../Page/佐藤雅将.md" title="wikilink">佐藤雅将</a></p></td>
</tr>
<tr class="odd">
<td><p>第2話</p></td>
<td><p>我正在成長</p></td>
<td></td>
<td><p><a href="../Page/もりたけし.md" title="wikilink">もりたけし</a></p></td>
<td><p><a href="../Page/平池芳正.md" title="wikilink">平池芳正</a></p></td>
<td><p>高木信一郎</p></td>
</tr>
<tr class="even">
<td><p>第3話</p></td>
<td><p>兩個人</p></td>
<td></td>
<td><p>東海林真一</p></td>
<td><p><a href="../Page/成田歳法.md" title="wikilink">成田歳法</a></p></td>
<td><p><a href="../Page/吉田隆彦.md" title="wikilink">吉田隆彦</a></p></td>
</tr>
<tr class="odd">
<td><p>第4話</p></td>
<td><p>冬美學姊</p></td>
<td></td>
<td><p><a href="../Page/別所誠人.md" title="wikilink">別所誠人</a></p></td>
<td><p>三宅雄一郎</p></td>
<td><p>江上夏樹</p></td>
</tr>
<tr class="even">
<td><p>第5話</p></td>
<td><p>謊言</p></td>
<td></td>
<td><p>成田歳法</p></td>
<td><p>山田弘和</p></td>
<td><p><a href="../Page/香川久.md" title="wikilink">香川久</a></p></td>
</tr>
<tr class="odd">
<td><p>第6話</p></td>
<td><p>同學</p></td>
<td></td>
<td><p>廣島秀樹</p></td>
<td><p>西山明樹彦</p></td>
<td><p>高木信一郎</p></td>
</tr>
<tr class="even">
<td><p>第7話</p></td>
<td><p>想要保護的東西</p></td>
<td></td>
<td><p>平池芳正</p></td>
<td><p>吉田隆彦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第8話</p></td>
<td><p>大家都在改變</p></td>
<td></td>
<td><p>原口浩</p></td>
<td><p>林千博</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第9話</p></td>
<td><p>明美</p></td>
<td></td>
<td><p>加瀨充子<br />
月野正志</p></td>
<td><p>山田弘和</p></td>
<td><p>佐藤雅将<br />
野崎真一</p></td>
</tr>
<tr class="odd">
<td><p>第10話</p></td>
<td><p>…然後</p></td>
<td></td>
<td><p><a href="../Page/森脇真琴.md" title="wikilink">森脇真琴</a></p></td>
<td><p>三宅雄一郎</p></td>
<td><p>江上夏樹</p></td>
</tr>
<tr class="even">
<td><p>第11話</p></td>
<td><p>兩人的時光</p></td>
<td></td>
<td><p>山田弘和<br />
唐戶光博</p></td>
<td><p><a href="../Page/紅優.md" title="wikilink">紅優</a></p></td>
<td><p>高木信一郎</p></td>
</tr>
<tr class="odd">
<td><p>第12話</p></td>
<td><p>LOVE SONG</p></td>
<td></td>
<td><p><a href="../Page/神戶洋行.md" title="wikilink">神戶洋行</a></p></td>
<td><p>吉田隆彦<br />
米本亨</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第13話</p></td>
<td><p>然後，我們將繼續相戀</p></td>
<td></td>
<td><p>加瀨充子</p></td>
<td><p>山田弘和</p></td>
<td><p>佐藤雅将</p></td>
</tr>
</tbody>
</table>

## [PS2遊戲](../Page/PlayStation_2.md "wikilink")

  - 2003年5月29日發售
  - 發售公司：[科樂美](../Page/科樂美.md "wikilink")
  - 製片人：メタルユーキ(Metal Yuhki)

## OVA版

  - 原作：[高橋真](../Page/高橋真.md "wikilink")（[小學館](../Page/小學館.md "wikilink")「ビッグスピリッツコミックス」刊）
  - 制作：スタジオ・ファンタジア
  - 月野正志製片人
  - 監督：加瀬充子
  - 片尾曲：『真夜中の虹 ～everlasting love～』
      - 歌：麻倉あきら(麻倉晶)，作詞：田形美喜子，作曲：井上大輔，編曲：吉川慶

      -
<!-- end list -->

  - 片尾曲：Rainbow at Midnight（真夜中の虹 英語版）
      - 歌：麻倉あきら/作詞：田形美喜子/訳詞：Gary　Newby/作曲：井上大輔/編曲：吉川慶

      -
## 真人電影版

  - 2006年1月28日、日本全國[東映系公開](../Page/東映.md "wikilink")。
  - 2006年6月3日、日本[DVD](../Page/DVD.md "wikilink")&[VHS開始出組](../Page/VHS.md "wikilink")。
  - 制作：[东映动画](../Page/东映动画.md "wikilink")、東映、東映錄影、SKY Perfect Well
    Think、[小學館](../Page/小學館.md "wikilink")、シリコンスタジオ、Amuse Soft
    Entertainment、[博報堂DY媒體](../Page/博報堂.md "wikilink")
  - 主題曲：『すみか』（演唱：[メレンゲ](../Page/メレンゲ.md "wikilink")）（Warner Music Japan）
  - 插入曲：『きみのうた』（演唱：[MAYUMI](../Page/MAYUMI.md "wikilink")）（作詞原案：高橋真）

### 角色

  - 千瀨（ちせ）：[前田亞季](../Page/前田亞季.md "wikilink")
  - 修次（シュウジ）：[窪塚俊介](../Page/窪塚俊介.md "wikilink")
  - 淳志（アツシ）：[木村了](../Page/木村了.md "wikilink")
  - 明美（アケミ）：[貫地谷栞](../Page/貫地谷栞.md "wikilink")
  - 中村（ナカムラ）：[川久保拓司](../Page/川久保拓司.md "wikilink")
  - 中隊長：[二階堂智](../Page/二階堂智.md "wikilink")
  - 白衣男：[津田寛治](../Page/津田寛治.md "wikilink")
  - 阿鐵（テツ）：[澀川清彦](../Page/澀川清彦.md "wikilink")
  - 冬美（ふゆみ）：[酒井美紀](../Page/酒井美紀.md "wikilink")
  - 村瀬（ムラセ）：[伊武雅刀](../Page/伊武雅刀.md "wikilink")

### 製作群

  - 製作總指導：高橋浩
  - 企画：森下孝三、黒澤満、坂上順
  - 企画協助：遠藤茂行
  - 監督：須賀大観
  - 製片人：北﨑広実、松井俊之、竹本克明、伊藤伴雄
  - 劇本：清水友佳子
  - 音樂：安西実
  - VFX監督：野口光一
  - VFX製片人：氷見武士
  - 攝影：藤澤順一（J.S.C）
  - 美術：中澤克巳
  - 照明：豊見山明長
  - 錄音：湯脇房雄
  - 編集：阿部亙英
  - 監督補：蔵方政俊
  - 製作擔任：丸山昌夫
  - VE：さとうまなぶ
  - B攝影：向後光徳
  - VFX美術指導：木村俊幸
  - 制服設計：小篠ゆま
  - 標題設計：岡野登
  - 裝飾：平井浩一
  - 記錄：増田実子
  - 音響效果：柴崎憲治
  - 角色分配導演：長谷川才帆子
  - 音樂製片人：藤田昭彦
  - 宣傳製片人：杉田薫

## 參考資料

## 外部連結

  - [SINPre.com\! - 原作者高橋真個人網頁](http://www.sinpre.com)
  - [最終兵器彼女動畫版官方網頁](http://www.saikano.net)
  - [真人電影版官方網頁](https://www.webcitation.org/61CwEghHO?url=http://www.saikano-movie.com/)
  - [最終兵器彼女美國版官方網頁](http://www.saikano-usa.com/)

[Category:青年漫畫](../Category/青年漫畫.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:賽博格題材漫畫](../Category/賽博格題材漫畫.md "wikilink")
[Category:賽博格題材動畫](../Category/賽博格題材動畫.md "wikilink")
[Category:2002年日本電視動畫](../Category/2002年日本電視動畫.md "wikilink")
[Category:2005年日本OVA動畫](../Category/2005年日本OVA動畫.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:2006年日本電影](../Category/2006年日本電影.md "wikilink")
[Category:日本漫畫改編真人電影](../Category/日本漫畫改編真人電影.md "wikilink")
[Category:日本爱情片](../Category/日本爱情片.md "wikilink")
[Category:日本科幻片](../Category/日本科幻片.md "wikilink")
[Category:2000年爱情片](../Category/2000年爱情片.md "wikilink")
[Category:2000年科幻片](../Category/2000年科幻片.md "wikilink")
[Category:日本自衛隊電影](../Category/日本自衛隊電影.md "wikilink")
[Category:末日題材電影](../Category/末日題材電影.md "wikilink")
[Category:賽博格題材電影](../Category/賽博格題材電影.md "wikilink")
[Category:小樽市背景作品](../Category/小樽市背景作品.md "wikilink")
[Category:札幌市背景電影](../Category/札幌市背景電影.md "wikilink")
[Category:悲劇電影](../Category/悲劇電影.md "wikilink")

1.
2.
3.