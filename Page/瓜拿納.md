**瓜拿納**，又名**巴西香可可**，英文Guarana（[雙名法](../Page/雙名法.md "wikilink")：*Paullinia
cupana*）原產於[巴西](../Page/巴西.md "wikilink")[亞馬遜盆地](../Page/亞馬遜盆地.md "wikilink")，是[無患子科](../Page/無患子科.md "wikilink")[泡林藤屬](../Page/泡林藤屬.md "wikilink")（[保力藤屬](../Page/保力藤屬.md "wikilink")）的爬藤植物。分布在[巴西](../Page/巴西.md "wikilink")、[祕魯](../Page/祕魯.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[委內瑞拉等](../Page/委內瑞拉.md "wikilink")[亞馬遜盆地區域](../Page/亞馬遜盆地.md "wikilink")，。

## 語源

瓜拿納的名稱（Guaraná）源自於十六世紀[巴西濱海地區的原住民語言](../Page/巴西.md "wikilink")
[tupi](../Page/tupi.md "wikilink") wara'ná。

## 描述

瓜拿納三葉、花小而白。果實含有大量的[咖啡因](../Page/咖啡因.md "wikilink")，因其特殊的刺激成分被使用在製作糖漿、食品，再加工成飲料。果實有紅色的外殼，成熟時會露出白色的果肉和種子，外觀看起來像眼睛。

在[葡萄牙](../Page/葡萄牙.md "wikilink")，1990年代末期開始有人用它來製作飲料。

## 食用效果

瓜拿納是一種興奮刺激物質，可增加[精神和](../Page/精神.md "wikilink")[肌肉強度的](../Page/肌肉.md "wikilink")[抵抗力](../Page/抵抗力.md "wikilink")、忍耐力，並減少[運動和](../Page/運動.md "wikilink")[物理的](../Page/物理.md "wikilink")[疲倦](../Page/疲倦.md "wikilink")。瓜拿納中的[咖啡因和](../Page/咖啡因.md "wikilink")[咖啡鹼所含有的](../Page/咖啡鹼.md "wikilink")[黃嘌呤](../Page/黃嘌呤.md "wikilink")，能夠提供最快速並最穩定的思考\[1\]。但是過多不當的攝取，會產生不良的[副作用效果如](../Page/副作用.md "wikilink")[失眠](../Page/失眠.md "wikilink")、[易怒](../Page/易怒.md "wikilink")、[心跳過快](../Page/心跳過快.md "wikilink")、[心痛](../Page/心痛.md "wikilink")、[身體依賴](../Page/身體依賴.md "wikilink")\[2\]\[3\]。

## 成分

瓜拿納含豐富的[咖啡因](../Page/咖啡因.md "wikilink")、[可可鹼](../Page/可可鹼.md "wikilink")、[茶鹼等功能性物质](../Page/茶鹼.md "wikilink")。它是一種[催情物質](../Page/催情物質.md "wikilink")，有滋補的效用。也是一種[收斂劑](../Page/收斂劑.md "wikilink")，是能沉澱[組織內部](../Page/组织_\(生物学\).md "wikilink")[蛋白質而促使組織皺縮的](../Page/蛋白質.md "wikilink")[藥物](../Page/藥物.md "wikilink")，有[消炎](../Page/消炎.md "wikilink")[退腫的作用](../Page/退腫.md "wikilink")，可[退熱](../Page/退熱.md "wikilink")、[利尿及減緩](../Page/利尿.md "wikilink")[身心疲劳](../Page/身心疲劳.md "wikilink")，增強[免疫力](../Page/免疫力.md "wikilink")、[抗氧化](../Page/抗氧化.md "wikilink")，預防[肥胖](../Page/肥胖.md "wikilink")，改善[血壓](../Page/血壓.md "wikilink")\[4\]

[茶鹼和](../Page/茶鹼.md "wikilink")[可可鹼有保護](../Page/可可鹼.md "wikilink")[支氣管](../Page/支氣管.md "wikilink")、[抗炎和](../Page/抗炎.md "wikilink")[免疫調節作用](../Page/免疫調節作用.md "wikilink")，減緩[老化過程](../Page/老化.md "wikilink")，并抑制[膽固醇在](../Page/膽固醇.md "wikilink")[動脈的](../Page/動脈.md "wikilink")[沉澱](../Page/沉澱.md "wikilink")，使身體有更好的[血液流動](../Page/血液.md "wikilink")。（KUSKOSKI等人，2005）。

瓜拿納的使用增加每日能量消耗和降低[食欲](../Page/食欲.md "wikilink")，維持[血液中穩定的](../Page/血液.md "wikilink")[血糖水平中](../Page/血糖.md "wikilink")，以避免[肥胖](../Page/肥胖.md "wikilink")。

它含有豐富的[兒茶素](../Page/兒茶素.md "wikilink")，它是抗[自由基](../Page/自由基.md "wikilink")，具有[抗氧化作用](../Page/抗氧化.md "wikilink")，是預防[衰老的物质](../Page/衰老.md "wikilink")。

## 形成飲料的過程

## 台灣的瓜拿納飲料

## 用途

瓜拿納常用於作[碳酸飲品或](../Page/碳酸飲品.md "wikilink")[能量飲品裡的](../Page/能量飲品.md "wikilink")[甜味劑](../Page/甜味劑.md "wikilink")，但亦是[草本茶飲品的成份](../Page/草本茶.md "wikilink")，又或以[膠囊的形式出現](../Page/膠囊.md "wikilink")。一般來說，[南美洲地區的](../Page/南美洲.md "wikilink")[咖啡因來源都普遍來自此植物](../Page/咖啡因.md "wikilink")，而非[咖啡豆](../Page/咖啡豆.md "wikilink")\[5\]。

## 照片集

<File:Guarana> - Paullinia cupana.jpg|植株 <File:Guaranà> original do
Brasil.jpg|果 [File:Guarana.jpg|瓜拿納籽粉](File:Guarana.jpg%7C瓜拿納籽粉)
[File:Guaranajesus.jpg|碳酸飲料](File:Guaranajesus.jpg%7C碳酸飲料)

## 參考資料

[Category:泡林藤屬](../Category/泡林藤屬.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")

1.  <http://www.blogers.com.br/efeitos-adversos-do-guarana/>

2.
3.  <http://www.sempretops.com/dicas/os-efeitos-do-guarana-em-po/>

4.  [GUARANÁ: Estudos sobre propriedades medicinais e funcionais do
    Guaraná](http://www.lideragronomia.com.br/2012/08/as-propriedades-do-guarana.html)

5.