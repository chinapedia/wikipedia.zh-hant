**天主教贵阳总教区（Archidioecesis
Coeiiamensis）**是[罗马天主教在中国](../Page/罗马天主教.md "wikilink")[贵州省设立的一个](../Page/贵州省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1696年10月15日，从福建代牧区分设贵州代牧区。1715年并入四川代牧区。1846年3月27日，罗马教廷宣布恢复贵州代牧区。1849年，原在[暹罗传教的法国](../Page/暹罗.md "wikilink")[巴黎外方传教会会士](../Page/巴黎外方传教会.md "wikilink")[白斯德望被任命为首任贵州主教](../Page/白斯德望.md "wikilink")。1853年白斯德望因伤寒病逝，童文献神父任教区长，主持全省（青岩、定番(惠水)、广顺诸地）教务。童文献在贵阳六冲关开办修院，胡傅理担任院长，又在青岩镇北姚家关建成圣伯多禄大修院，由白伯多禄任院长。

1860年，胡傅理被罗马教廷任命为贵州主教。当时的贵州提督田兴恕和巡抚何冠英不愿执行《北京条约》中公开合法传教的条款。1861年端午节，青岩大修院的守门人罗廷荫和修士与“游百病”的人群发生口角，青岩团总赵蔚三（国澍）带领团丁逮捕修士张文澜（张如祥）、陈昌吕及守门人罗廷荫，抓到龙泉寺看管，放火烧毁修院。院长白伯多禄带着部分修生逃走。因而引发[青岩教案](../Page/青岩教案.md "wikilink")。正在交涉期间，田兴恕密令将3人及修院女厨王玛尔大在青岩镇谢家坡斩首，又发生了“开州教案”。最后清政府将田兴恕革职并充军流放新疆，将田兴恕在贵阳城内六洞桥的住宅，赔偿作天主教堂（南天主堂）；赔偿白银120001两。事后另拨土地修建青岩天主教堂。这时贵阳已经拥有5座教堂，即北堂、南堂、圣斯德望堂、六冲关天主堂、和青岩天主堂。

1871年，李万美接任贵州主教，他在1876年重建贵阳北天主堂主教座堂。

20世纪初，天主教在贵州取得较大发展。到1922年，贵州教徒增至3万6千人。1924年12月3日，贵州代牧区以主教驻地更名为贵阳代牧区。此前在1922年分设了[安龙教区](../Page/安龙教区.md "wikilink")，1932年又分设[石阡教区](../Page/天主教石阡监牧区.md "wikilink")。1946年4月11日，升格为贵阳总教区。

1949年后，法国传教士继续留在贵阳，1950年代初被驱逐。

## 教堂

  - [贵阳圣若瑟主教座堂](../Page/贵阳圣若瑟主教座堂.md "wikilink")

## 主教

  - [白斯德望](../Page/白斯德望.md "wikilink")（Bishop Etienne-Raymond Albrand,
    [巴黎外方傳教會](../Page/巴黎外方傳教會.md "wikilink")）1846年 - 1853年
  - [胡缚理](../Page/胡缚理.md "wikilink")（Bishop Louis-Simon Faurie,
    巴黎外方傳教會）1853年 -1871年
  - [李万美](../Page/李万美.md "wikilink")（Bishop François-Eugène Lions,
    巴黎外方傳教會） 1871年 - 1893年
  - [施恩](../Page/施恩.md "wikilink")（Bishop François-Lazare Seguin,
    巴黎外方傳教會） 1913年 - 1942年
  - [蓝士谦](../Page/蓝士谦.md "wikilink")（Archbishop Jean Larrart, 巴黎外方傳教會）
    1942年 - 1966年
  - [胡大国](../Page/胡大国.md "wikilink") 1987年 - 1997年
  - [王充一](../Page/王充一.md "wikilink") 1988年 - 2014年
  - [蕭澤江](../Page/蕭澤江.md "wikilink") 2014年 - 現任

## 信徒

1950年，贵阳教区信徒24,713人\[1\] 。

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:贵州天主教](../Category/贵州天主教.md "wikilink")
[Category:贵阳宗教](../Category/贵阳宗教.md "wikilink")

1.  [Archdiocese of
    Kweyang](http://www.catholic-hierarchy.org/diocese/dkwey.html)