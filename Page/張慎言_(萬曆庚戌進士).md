**張慎言**（），字金銘
，號蘶姑，[山西省](../Page/山西省.md "wikilink")[澤州](../Page/澤州.md "wikilink")[陽城縣屯城人](../Page/陽城縣.md "wikilink")，明朝政治人物。

## 生平

[河南參政](../Page/河南.md "wikilink")[張昇之孫](../Page/張昇.md "wikilink")，早孤，由祖母撫養。自幼穎悟，[萬曆三十八年庚戌](../Page/萬曆.md "wikilink")（1610年）進士\[1\]。授通政司觀政。任山东[寿张](../Page/寿张.md "wikilink")[知县](../Page/知县.md "wikilink")。調[曹縣](../Page/曹縣.md "wikilink")。[萬曆四十八年](../Page/萬曆.md "wikilink")（1620年）为[陕西道](../Page/陕西.md "wikilink")[御史](../Page/御史.md "wikilink")。生性耿直，不事阿奉。[崇祯元年](../Page/崇祯.md "wikilink")（1628年），补[湖广道](../Page/湖广.md "wikilink")[御史](../Page/御史.md "wikilink")，歷官[太仆](../Page/太仆.md "wikilink")[少卿](../Page/少卿.md "wikilink")、[太常卿](../Page/太常卿.md "wikilink")、[刑部侍郎](../Page/刑部侍郎.md "wikilink")。累官南京吏部尚書。

崇祯十七年（1644年），[福王即位南京](../Page/朱由崧.md "wikilink")，張慎言理部事。為勛臣[劉孔昭等所忌](../Page/劉孔昭.md "wikilink")，乞休，加太子太保，蔭一子。國亡後，疽發背卒。\[2\]\[3\]

## 著作

著有《洎水斋文钞》、《洎水斋诗钞》等。

  - 過洹水將入里有懐長安古人

<!-- end list -->

  -
    洹水纔過丹水流，客愁爭似水悠悠。
    汀洲緑映征𫀆淺，桃李香迎語鳥柔。
    耽靜計將書卒嵗，懷人豈但目三秋。
    歸來鄉里都如故，晚計方思馬少游。

## 註釋

{{-}}

[Category:萬曆三十四年丙午科舉人](../Category/萬曆三十四年丙午科舉人.md "wikilink")
[Category:明朝壽張縣知縣](../Category/明朝壽張縣知縣.md "wikilink")
[Category:明朝曹縣知縣](../Category/明朝曹縣知縣.md "wikilink")
[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:明朝太僕寺少卿](../Category/明朝太僕寺少卿.md "wikilink")
[Category:明朝太常寺卿](../Category/明朝太常寺卿.md "wikilink")
[Category:明朝刑部侍郎](../Category/明朝刑部侍郎.md "wikilink")
[Category:陽城人](../Category/陽城人.md "wikilink")
[S](../Category/張姓.md "wikilink")

1.  《萬曆三十八年庚戌科序齒錄》：民籍，治《[易經](../Page/易經.md "wikilink")》，年三十三歲中式萬曆三十八年庚戌科第三甲第一百十八名進士。丁丑正月十六日生，行一，曾祖[張牘](../Page/張牘.md "wikilink")，敕中憲大夫復縣知府；祖[張昇](../Page/張昇.md "wikilink")，□戊進士□□左參政崇祀鄉賢；父[張天和](../Page/張天和.md "wikilink")，廩生初贈文林郎；母王氏（敕贈孺人）；□氏（□□孺人）。[永感下](../Page/永感下.md "wikilink")，妻馬氏（敕贈孺人）繼妻栗氏（敕封孺人）；弟慎修（儒士）；慎德（儒士）；慎思（生員）；慎枙（儒士）；慎機（生員）；慎術，子履旋；兌孚；巽孚。由學生中式丙午鄉試四十八名[舉人](../Page/舉人.md "wikilink")，[會試中式八十二名](../Page/會試.md "wikilink")。
2.  《明史·列传一百六十三》
3.  《遼海叢書·皇清書史附錄》1698：張慎言，字金銘，號藐姑，陽城人。萬曆三十八年進士。官南吏部尚書。國亡後，疽發背卒。放同鄉張叟石船藏有太宰小冊行書自作詩花綾本用筆雅逸以伯生善之不經意書昔人謂有明士大夫無不留意翰墨者信然鬱棲書話