**祖籍**是指一個[家族族群以](../Page/家族.md "wikilink")[父系共同認定的一位](../Page/父系.md "wikilink")[祖先的生長地](../Page/祖先.md "wikilink")\[1\]。一些已經離開了祖先的生長地或已經離開了家鄉的人，他們的後代，仍然追溯遙遠祖先的出生地或家鄉，來作為自己祖籍。

祖籍与[籍貫](../Page/籍貫.md "wikilink")、[戶籍的意義不太相同](../Page/戶籍.md "wikilink")。

祖籍通常是追認極遙遠的[祖先生長地](../Page/祖先.md "wikilink")，籍貫則是[父親與](../Page/父親.md "wikilink")[祖父的長居之地](../Page/祖父.md "wikilink")，戶籍則是其本人現在登記於[政府的居住地](../Page/政府.md "wikilink")[地址](../Page/地址.md "wikilink")。

如逝於[中南海的](../Page/中南海.md "wikilink")[毛澤東](../Page/毛澤東.md "wikilink")，由於遠祖**毛太華**避戰亂，從[江西](../Page/江西.md "wikilink")[吉水遷徙到](../Page/吉水.md "wikilink")[湖南](../Page/湖南.md "wikilink")[韶山](../Page/韶山.md "wikilink")\[2\]，故祖籍乃[江西](../Page/江西.md "wikilink")[吉水](../Page/吉水.md "wikilink")，籍貫是[湖南](../Page/湖南.md "wikilink")[韶山](../Page/韶山.md "wikilink")，戶籍為[北京](../Page/北京.md "wikilink")[西城](../Page/西城.md "wikilink")。

另外[明朝抗](../Page/明朝.md "wikilink")[倭名將](../Page/倭.md "wikilink")[俞大猷](../Page/俞大猷.md "wikilink")，因先祖**俞敏**跟從[明太祖打天下](../Page/明太祖.md "wikilink")，以開國功臣襲[泉州衛百戶官](../Page/泉州衛.md "wikilink")，便從家鄉[南直隸](../Page/南直隶.md "wikilink")[霍丘落戶當地](../Page/霍丘.md "wikilink")，故祖籍是[南直隸](../Page/南直隶.md "wikilink")[霍丘](../Page/霍丘.md "wikilink")，籍貫為[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")。\[3\]由此可知，祖籍、籍貫、戶籍三名詞為不同的概念，不可同語。

## 參見

  - [籍貫](../Page/籍貫.md "wikilink")
  - [戶籍](../Page/戶籍.md "wikilink")
  - [郡望](../Page/郡望.md "wikilink")
  - [堂號](../Page/堂號.md "wikilink")
  - [本貫 (朝鮮半島)](../Page/本貫_\(朝鮮半島\).md "wikilink")

## 參考文獻

[category:家世與傳承](../Page/category:家世與傳承.md "wikilink")

[Category:社會制度](../Category/社會制度.md "wikilink")

1.  [「唐山過台灣」---
    從祖籍與族譜來尋根](http://kaostaiwan.pixnet.net/blog/post/28154761-%e3%80%8c%e5%94%90%e5%b1%b1%e9%81%8e%e5%8f%b0%e7%81%a3%e3%80%8d----%e5%be%9e%e7%a5%96%e7%b1%8d%e8%88%87%e6%97%8f%e8%ad%9c%e4%be%86%e5%b0%8b%e6%a0%b9)
2.  [千年古磚印證毛澤東祖籍在江西](http://news.sohu.com/19/53/news201115319.shtml)，中新社2002年5月27日
3.  《都督俞公生祠记》
    载“原籍直隶凤阳霍丘县人，世泉州卫前所百户，以魁武科授正千户，累迁都督同知，虚江其别号云。岁嘉靖甲子冬十月之吉，赐进士出身、南京户部、山东清吏司主事、同安南洲许廷用撰。”