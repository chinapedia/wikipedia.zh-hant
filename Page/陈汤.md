**陈汤**，生卒年月不详，字**子公**，[西汉](../Page/西汉.md "wikilink")[山阳](../Page/山阳郡.md "wikilink")[瑕丘](../Page/瑕丘縣.md "wikilink")（今[山东省](../Page/山东省.md "wikilink")[兖州市东北](../Page/兖州市.md "wikilink")）人，爵[关内侯](../Page/关内侯.md "wikilink")。

## 生平

陈汤少时家贫好学，有大志，但是品行不端，不为乡里所称。后赴[长安求官](../Page/长安.md "wikilink")，与[张勃相识](../Page/张勃.md "wikilink")。[汉元帝](../Page/汉元帝.md "wikilink")[初元二年](../Page/初元.md "wikilink")（前47年），张勃推荐陈汤为官，但是陈汤在待选期间父亲过世，按照律法，他应該回家奔丧。陈汤隐瞒了此消息，结果事情败露而下狱。

出狱后，他被授予[郎官闲职](../Page/郎官.md "wikilink")，多次请求出使[西域](../Page/西域.md "wikilink")，后于[建昭三年](../Page/建昭.md "wikilink")（前36年）出任[西域都护府副校尉](../Page/西域都护府.md "wikilink")。时[匈奴](../Page/匈奴.md "wikilink")[郅支单于因為杀害汉使](../Page/郅支单于.md "wikilink")[谷吉](../Page/谷吉.md "wikilink")，为了防備[汉朝出兵](../Page/汉朝.md "wikilink")[报复](../Page/报复.md "wikilink")，率領部众向西迁居於[康居](../Page/康居.md "wikilink")，受到康居庇护。康居为借其威名称霸西域，屡借兵与其攻掠汉朝属国[乌孙及](../Page/乌孙.md "wikilink")[大宛等](../Page/大宛.md "wikilink")。

[建昭三年秋](../Page/建昭.md "wikilink")，陈汤[矯诏調发屯田兵士和西域各国军队共](../Page/矯诏.md "wikilink")4万人[远征](../Page/郅支之戰.md "wikilink")[郅支单于](../Page/郅支单于.md "wikilink")，胁迫上司西域都护府校尉[甘延寿服從其事](../Page/甘延寿.md "wikilink")。大軍既發，[遂克郅支城](../Page/郅支之战.md "wikilink")（今[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")[塔拉茲](../Page/塔拉茲.md "wikilink")），全歼郅支单于及其本部部众3千人。大捷后，兩人上疏朝廷“臣闻天下之大义，当混为一。匈奴[呼韩邪单于已称北藩](../Page/呼韩邪单于.md "wikilink")，唯郅支单于叛逆，未伏其辜，[大夏之西](../Page/大夏.md "wikilink")，以为强汉不能臣也。郅支单于惨毒行于民，大恶逼于天。臣延寿、臣汤将义兵，行天诛，赖陛下神灵，阴阳并应，陷阵克敌，斩郅支首及名王以下。宜悬头槁于蛮夷邸间，以示万里，**明犯强汉者，虽远必诛！**”。

陈汤私自贪污了部分[战利品](../Page/战利品.md "wikilink")，当[司隶校尉查办时](../Page/司隶校尉.md "wikilink")，他反而指责司隶校尉“是为郅支报仇也”，加上与之交好的[宗正](../Page/宗正.md "wikilink")[刘向为他上疏](../Page/刘向.md "wikilink")，元帝不顾丞相[石显和](../Page/石显.md "wikilink")[匡衡的反对](../Page/匡衡.md "wikilink")，赐陈汤爵关内侯，命其为[射声校尉](../Page/射声校尉.md "wikilink")。

[汉成帝继位后](../Page/汉成帝.md "wikilink")，陈汤贪污受贿事发，被罷免了官位。其后，他又诬称康居所送[质子非](../Page/質子_\(東亞政治\).md "wikilink")[王子](../Page/王子.md "wikilink")，败露后下狱被判死罪。[太中大夫](../Page/太中大夫.md "wikilink")[谷永为其求情](../Page/谷永.md "wikilink")，使其得免一死，免爵[充军](../Page/充军.md "wikilink")。外戚[王商和](../Page/王商_\(成都侯\).md "wikilink")[王凤当政时](../Page/王凤.md "wikilink")，西域都护[段会宗被乌孙军队包围](../Page/段会宗.md "wikilink")。兩王咨询陈汤意见，陈汤判断乌孙军队必败，果如其言，于是王凤复起陈汤为[从事中郎](../Page/从事中郎.md "wikilink")，掌管幕府参谋事宜。

陈汤复出后党附[王莽](../Page/王莽.md "wikilink")，仍然经常受贿，时[将作大匠](../Page/将作大匠.md "wikilink")[解万年欲求功](../Page/解万年.md "wikilink")，谋划迁移关东良民至[关内](../Page/关内.md "wikilink")，建立初陵邑，陈汤上疏赞同，表示自己将带头迁移。结果多年营造不成，民不聊生，该计划唯有放弃。当拆除造好的房屋时，发现陈汤未带头迁居。陈汤因此再次下狱，发现其過往多次受贿，再次被发配[敦煌](../Page/敦煌.md "wikilink")，后被赦回，过世于[长安](../Page/长安.md "wikilink")。

[王莽掌权后](../Page/王莽.md "wikilink")，追谥汤为**破胡壮侯**。

## 名言

「明犯强汉者，虽远必诛！」

## 家族

  - 子：[陈冯](../Page/陈冯.md "wikilink")，[破胡侯](../Page/破胡侯.md "wikilink")

## 參考資料

《漢書·陳湯傳》

## 相關文學

史杰鵬著，《賭徒陳湯》，北市：馥林文化，2007年。

[Category:西汉郎官](../Category/西汉郎官.md "wikilink")
[Category:西汉校尉](../Category/西汉校尉.md "wikilink")
[Category:西汉关内侯](../Category/西汉关内侯.md "wikilink")
[C陈](../Category/兖州人.md "wikilink") [T](../Category/陈姓.md "wikilink")
[Category:諡壯](../Category/諡壯.md "wikilink")