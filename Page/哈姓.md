**哈姓**為[回族姓氏之一](../Page/回族.md "wikilink")，現代標準漢語讀第三聲hǎ，是指中國中原地區的姓氏標準，即漢字「哈」姓。它不是各少數民族或外國人名字中，音譯時，有「哈」的發音。也不是[伊斯蘭教的](../Page/伊斯蘭教.md "wikilink")[經名](../Page/經名.md "wikilink")，例如：「哈三」、「哈散」、「哈桑」等。

元明時期，很多從西域來華的人，名字中有「哈」發音，統統說成是今天的「哈」姓是錯誤的，事實上他們的後人，絕大多數都不姓「哈」。回回入附中原之前大多是沒有姓氏，只有名字的。[明朝開國之初](../Page/明朝.md "wikilink")，太祖朱元璋是禁止入附的少數民族隨意改易原來姓氏的，即所謂的「以嚴[夷](../Page/夷.md "wikilink")[夏之辨](../Page/夏.md "wikilink")」\[1\]。「[土木之變](../Page/土木之變.md "wikilink")」以前，《[明實錄](../Page/明實錄.md "wikilink")》和其他明代的歷史文獻，尚未查證到改姓名時以「哈」為姓的，中原地區的「哈」姓，源於明中期「土木之變」後。

明代的「哈」姓，分為「[南京哈](../Page/南京.md "wikilink")」，與「[河間哈](../Page/河間.md "wikilink")」。[哈剌卜達是](../Page/哈剌卜達.md "wikilink")「南京哈」的始祖，哥哥[哈剌卜丁是](../Page/哈剌卜丁.md "wikilink")「河間哈」的始祖。現在的[獻縣](../Page/獻縣.md "wikilink")、[肅寧](../Page/肅寧.md "wikilink")、[瀛州](../Page/瀛州.md "wikilink")（河間市），歷史上統歸河間府管轄，稱河間人。

## 明太祖禁用「胡姓」

公元1368年，中原人建立了明王朝，[元順帝從元大都](../Page/元順帝.md "wikilink")（[北京](../Page/北京.md "wikilink")）遷的[漠北後](../Page/漠北.md "wikilink")，遊牧民族的蒙元帝國，結束了在中原近百年的統治。在改朝換代的政治背景下，被[成吉思汗及其子孫西征時](../Page/成吉思汗.md "wikilink")，從[中亞等地區俘虜和征召來華的人](../Page/中亞.md "wikilink")，一部分隨蒙古人到了漠北，大部分滯留中原，成了真正的天涯淪落群體。明王朝有條件的接收了這些人，成為現在[回族先輩的主體](../Page/回族.md "wikilink")。

[明太祖](../Page/明太祖.md "wikilink")[朱元璋](../Page/朱元璋.md "wikilink")，北伐時發布的《諭中原檄》提出了：「驅逐胡虜，恢復中華，立綱陳紀，救濟斯民」，並說他出兵北伐旨在「復漢官之威儀」。宣佈：「歸我者永安於中華，背我者自竄於塞外」，「如[蒙古](../Page/蒙古.md "wikilink")、[色目](../Page/色目人.md "wikilink")，雖非華夏族類，然同生天地之間，有能知禮義、願為臣民者，與中夏之人撫養無異。」\[2\]

洪武元年二月，他下詔復唐式衣冠，即禁用「胡服、胡語、胡姓」。\[3\]

許多蒙古、色目人入仕之後，紛紛改用漢姓漢名。後來，朱元璋覺得此舉有悖「先王致謹氏族之道」，才又下令「禁蒙古、色目人更易姓名」。\[4\]

明朝，洪武六年閏十一月，明太祖朱元璋，頒佈制定了《[大明律](../Page/大明律.md "wikilink")》。朱元璋禁止蒙古、色目人在本民族內部自相嫁娶。《大明律集解附例》規定：「凡蒙古、色目人，聽與中國人(指漢族人)為婚姻，務要兩相情願，不許本類自相嫁娶。違者，杖八十，男女入官為奴。其中國人不願與[回回](../Page/回回.md "wikilink")、[欽察為婚姻者](../Page/欽察.md "wikilink")，聽從本類自相嫁娶，不在禁限。」當時人認為這種規定是因為「恐其種類日滋也。聽其本類為婚者，又憫其種類成色也。立法嚴而用心恕，所以羈靡異類也」。\[5\]

「禁胡服、胡語、胡姓名」，「胡人不能自相嫁娶」等限制和漢化胡人的政策，強迫胡人接受了中原文化（漢文化）。但是，也在這一政策的前提下，接納了回族先輩成為[中華民族的組成部分](../Page/中華民族.md "wikilink")。因此元朝時期由西域和北元入附中原的的胡人（[色目人](../Page/色目人.md "wikilink")），在明王朝建立後、明中期以前，都改了漢姓。

## 「土木之變」後懷柔政策

「[土木之變](../Page/土木之變.md "wikilink")」後，明王朝開始衰敗，被迫逐步改變原有的國策。對[西域和北元的遊牧民族政權](../Page/西域.md "wikilink")，採取了贖買政策，請來入仕（當官），給房屋田產和生活必需品，帶俸，職務可以世襲，不再強調大明律的規定等。

[北京大學歷史系教授](../Page/北京大學.md "wikilink")[張鴻翔](../Page/張鴻翔.md "wikilink")，自1931年起，歷經40多年，搜集明代各民族人士入附中原的歷史資料，整理彙編出入附中原後，入仕（作官）者3267人的相關資料，編輯出版了《明代各民族人士入仕中原考》。書中的緒言中寫到：「蓋明自太祖崛起淮甸，化家為國，成祖繼武，聲威益振，一時殊風異俗，若[女真](../Page/女真.md "wikilink")、[韃靼](../Page/韃靼.md "wikilink")、[兀良哈](../Page/兀良哈.md "wikilink")、[瓦剌](../Page/瓦剌.md "wikilink")、[哈密](../Page/哈密.md "wikilink")、[沙州](../Page/沙州.md "wikilink")、[西番](../Page/西番.md "wikilink")、[扯兒禪](../Page/扯兒禪.md "wikilink")、[車兒禪](../Page/車兒禪.md "wikilink")、[土魯番](../Page/土魯番.md "wikilink")、[赤斤蒙古](../Page/赤斤蒙古.md "wikilink")、[撒里畏吾兒](../Page/撒里畏吾兒.md "wikilink")、[別失八里](../Page/別失八里.md "wikilink")、[撒馬兒罕](../Page/撒馬兒罕.md "wikilink")、[回回欽察](../Page/回回欽察.md "wikilink")、[番國](../Page/番國.md "wikilink")、[高麗](../Page/高麗.md "wikilink")、[交阯](../Page/交阯.md "wikilink")、[暹羅](../Page/暹羅.md "wikilink")、[阿速](../Page/阿速.md "wikilink")、[古里](../Page/古里.md "wikilink")、[天方諸部族](../Page/天方.md "wikilink")，畏威懷德，相率內附。」明為懷柔遠人，固我邊圍，於是授之職位以結其心，賜之田園以固其志，而來歸者逐樂不思蜀，改名易姓，佔籍華土，久而乃為中原之新氏族矣。\[6\]

「土木之變」後，各民族人士入仕中原者，不再被強制改為中原的漢姓，可以改為具有少數民族原生態特徵的姓氏，例如：「哈」姓、「脫」姓、「擺」姓等等，這些姓氏絕大多數源於明中後期。

## 明中期以前西域來華者，名字有哈字發音者，並沒有改為「哈」姓

[元代住平安路同知](../Page/元代.md "wikilink")、中奉大夫、廣東宣慰使都元帥的[哈散](../Page/哈散.md "wikilink")，任宣慰司副使的[哈辛](../Page/哈辛.md "wikilink")，便分別是[北宋神宗年間進入中國的](../Page/北宋.md "wikilink")[布哈拉王後裔](../Page/布哈拉.md "wikilink")[賽典赤贍思丁的次子和孫子](../Page/賽典赤贍思丁.md "wikilink")。在元代的中高級官員中，還有[哈伯](../Page/哈伯.md "wikilink")、[哈八石](../Page/哈八石.md "wikilink")、[哈八失](../Page/哈八失.md "wikilink")、[哈兒沙](../Page/哈兒沙.md "wikilink")、[哈海赤](../Page/哈海赤.md "wikilink")、[哈黑丁](../Page/哈黑丁.md "wikilink")、[哈剌](../Page/哈剌.md "wikilink")、[哈麻](../Page/哈麻.md "wikilink")、[哈迷都丁等三十餘人](../Page/哈迷都丁.md "wikilink")，但後來都改用漢姓。例如：

### 哈剌歹的後裔在明代賜[毛姓](../Page/毛姓.md "wikilink")

  -
    據史載，（元）鄧文原《鞏固武惠公神道碑》。\[7\]
    [元朝統一後](../Page/元朝.md "wikilink")，居留河南南陽的一部分[哈剌魯軍人](../Page/哈剌魯.md "wikilink")，在其首領哈剌歹的率領下，奉命屯駐于慶元（浙江寧波）一帶。\[8\]
    據《明史、毛忠傳》載：「[毛忠](../Page/毛忠.md "wikilink")，字允誠，初名哈喇，西陵人。（中國名人大辭典裡注為「[涼州人](../Page/涼州.md "wikilink")」）曾祖哈剌歹，洪武初歸附，起行伍為千戶，戰歿。祖拜都從征哈密，亦戰歿。父寶以驍勇充總旗，至永昌百戶。正統十年（西元1445年），毛忠以守邊勞，進同知，始賜姓。正統十三年（西元1448年），……始賜名忠。」
    另《殊域周諮錄》的《[天方國](../Page/天方.md "wikilink")》、《[吐魯番](../Page/吐魯番.md "wikilink")》卷中，亦有回回通事（翻譯）毛見、毛進。[毛姓](../Page/毛姓.md "wikilink")[回族主要分佈於](../Page/回族.md "wikilink")[新疆和](../Page/新疆.md "wikilink")[甘肅](../Page/甘肅.md "wikilink")。

### 哈剌得的後裔在明代為[劉姓](../Page/劉姓.md "wikilink")，非賜即改

  -
    哈剌得，《元史》卷，三二有傳。他的功在滅[南宋](../Page/南宋.md "wikilink")。初從軍攻襄樊，這是蒙古軍滅宋的一個重要戰役。過江後攻克宋之江陰、許浦、金山、上海、崇明、金浦，守定海港口。後至[溫州](../Page/溫州.md "wikilink")，鎮守沿海上下。積功至沿海上萬戶府達魯花赤，浙東道宣慰使。大德五年（西元1301年），徵召入見，擢資德大夫、[雲南行省右丞](../Page/雲南.md "wikilink")，偕劉深征八百媳婦國，喪師而還，以罪廢，卒於汝州。皇慶元年（西元1312年），贈榮祿大夫、平章政事、鞏國公，溢武惠。子哈剌不花，襲沿海萬戶府達魯花赤。
    哈剌不花的後裔在明代選擇了[劉姓](../Page/劉姓.md "wikilink")，並溶入[回族](../Page/回族.md "wikilink")。元至正十七年（1357年），河南行省平幸政事劉哈剌不花，《元史》言「其先江西人」。「哈剌不花」屬「探馬赤軍戶」身份。受[回回人泰不華](../Page/回回.md "wikilink")（達不華）賞識推舉為椽史（屬員）哈剌魯人。哈剌不花的後裔劉姓，非賜即改。

### 答失蠻的後裔在明代選擇了[白姓](../Page/白姓.md "wikilink")，並溶入[回族](../Page/回族.md "wikilink")

  -
    現居[廣西](../Page/廣西.md "wikilink")[桂林的白姓回族](../Page/桂林.md "wikilink")，譜系稱始祖為[伯篤魯丁](../Page/伯篤魯丁.md "wikilink")。據《元詩選》記載，伯篤魯丁字至道，稱魯至道，答失蠻人，進士。至元三年（1266年），任嶺南廣西道肅政廉訪副使。因「伯」與「白」對音，其後裔先取「伯」為姓，後改白姓。又據《新元史氏族表》載，西域回回伯德那之子察罕，[元仁宗賜予其](../Page/元仁宗.md "wikilink")[白姓](../Page/白姓.md "wikilink")。

### 哈銘的後裔在明代選擇了[白姓](../Page/白姓.md "wikilink")，並溶入[回族](../Page/回族.md "wikilink")

  -
    哈銘（原名[楊銘](../Page/楊銘.md "wikilink")），其父哈只（哈密回回[楊直](../Page/楊直.md "wikilink")）是明朝官員通事。作為外交官被[瓦剌部扣留](../Page/瓦剌.md "wikilink")，因通曉蒙語被英宗留在身邊。經名：哈只阿力，哈密回回，原為哈密衛都督僉事。英宗北狩有翊戴之功，天順英宗復辟，哈銘舉家內附，任[錦衣衛指揮僉事](../Page/錦衣衛.md "wikilink")。卒後，子阿討剌襲職，賜姓白名瑜。瑜子白鑌，鑌之子白瀾、白贏及五世白鏞、六世白廷圭、七世白三捷。均延用白姓並世襲錦衣衛指揮使。\[9\]

### 哈剌①的後裔在明代選擇了[孫姓](../Page/孫姓.md "wikilink")

  -
    胡人，洪武四年內附，永樂二年升副千戶，七年調盧龍（今[河北](../Page/河北.md "wikilink")[盧龍縣](../Page/盧龍.md "wikilink")）。准更姓名孫盛。\[10\]

### 哈剌⑤的後裔在明代選擇了[張姓](../Page/張姓.md "wikilink")

  -
    胡人，內附中朝，命於燕山左衛帶俸，永樂八年升為千戶，更姓名張友。\[11\]

### 哈剌章的後裔在明代選擇了[於姓](../Page/於姓.md "wikilink")

  -
    胡人，洪武初來歸，累功升金山衛副千戶，更姓名曰於興。\[12\]

### 哈只的後裔在明代選擇了[李姓](../Page/李姓.md "wikilink")

  -
    明初人附，宣宗宣德一二年（1427）賜姓李，名誠。時任通事，授[錦衣衛指揮使](../Page/錦衣衛.md "wikilink")。\[13\]

## 現在的中原“哈”姓

哈喇兄弟，到中原定居已經500多年，子孫22代左右，經歷了[明](../Page/明.md "wikilink")、[清](../Page/清.md "wikilink")、[民國](../Page/民國.md "wikilink")，各種原因的變遷，已經散落到中國各地，且樹大分支，互相並不來往。沒有一部可以遵循的完整族譜，各支族群自行排序。

現在各支族群，都渴望知道整個族群的歷史和現在。現在生活好了，人們都渴望認祖歸宗，心情可以理解，但作法大相徑庭，除少數人想把歷史還原外，多數則希望自己的祖先，是哪代的名人，以耀祖光宗，以至於個別的族群杜撰歷史、杜撰沒有任何史學價值的家譜。有的族群分支，將自己「哈」姓的來源，上溯到[朱棣發動的](../Page/朱棣.md "wikilink")「[靖難之役](../Page/靖難之役.md "wikilink")」和朱棣北征蒙古的「掃北」。那個歷史時期，在《[明實錄](../Page/明實錄.md "wikilink")》等和明代其他歷史文獻上，尚未查證到相關的歷史依據，也不符合歷史背景。

歷史上族群內也存在各種矛盾的利益關係，流動的原因也不相同，戰爭、饑荒等原因，應徵的、逃難的，外出為官的，為非作歹刑事犯罪的，總之族群內也是社會的一部分，什麼情況都有。可以肯定的是，那些外出為官的後人，不在原籍，因為朝代的更迭，政治的演變，做官時的恩怨、因果關係必然要牽連他的後人。

現在中國的「哈」姓，還有[滿人](../Page/滿.md "wikilink")，明代稱「女直」，清代稱「[女真](../Page/女真.md "wikilink")」，但容易區別：明代，他們不需要改姓氏；清代滿人是[八旗成員](../Page/八旗.md "wikilink")，統治者沒有必要改姓氏；這些人大多是清後期到民國初期改的「哈」姓，而且大多在東北。內蒙古人現在有叫「哈〇〇」，那個「哈」不是姓，只是一種發音。

## 歷史名人

### 宋朝

  - [哈迷蚩](../Page/哈迷蚩.md "wikilink")，小说《说岳全传》人物

### 清朝

  - [哈元生](../Page/哈元生.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[雍正](../Page/雍正.md "wikilink")[軍機處大臣](../Page/軍機處.md "wikilink")[學習行走](../Page/學習行走.md "wikilink")。
  - [哈廷樑](../Page/哈廷樑.md "wikilink")
  - [哈攀龍](../Page/哈攀龍.md "wikilink")
  - [哈國興](../Page/哈國興.md "wikilink")

### 近代

  - [哈漢章](../Page/哈漢章.md "wikilink")，[回族](../Page/回族.md "wikilink")，[河間人](../Page/河間.md "wikilink")，[哈元生之後](../Page/哈元生.md "wikilink")。晚清曾留學日本、任新軍將領、禁衛軍高級教官。革命組織興中會成員。袁世凱北洋府陸軍部軍事幕僚。民國政府陸軍中將、將軍府將軍、廉威將軍。
  - [哈雄文](../Page/哈雄文.md "wikilink")，[回族](../Page/回族.md "wikilink")，[哈漢章之子](../Page/哈漢章.md "wikilink")。先後畢業於[清華學校](../Page/清華學校.md "wikilink")、[美國賓夕法尼亞大學建築系](../Page/美國賓夕法尼亞大學.md "wikilink")。曾任[滬江大學教授](../Page/滬江大學.md "wikilink")、建築科主任，國民黨政府內政部營建司司長。中華人民共和國成立後，歷任[復旦大學](../Page/復旦大學.md "wikilink")、[交通大學](../Page/交通大學.md "wikilink")、[同濟大學](../Page/同濟大學.md "wikilink")、[哈爾濱工業大學教授](../Page/哈爾濱工業大學.md "wikilink")，[哈爾濱建築工程學院教授](../Page/哈爾濱建築工程學院.md "wikilink")、建築工程系副主任，[中國建築師學會理事長](../Page/中國建築師學會.md "wikilink")。[上海](../Page/上海.md "wikilink")[九三學社社員](../Page/九三學社.md "wikilink")。長期從事城市規劃、建築設計和建築教育工作。著有《建築理論和建築管理法規》、《美國城市規劃史》，主編《建築十年》。
  - [哈元章](../Page/哈元章.md "wikilink")，台灣[京劇四大鬚生之一](../Page/京劇.md "wikilink")。
  - [哈珮](../Page/哈珮.md "wikilink")，[江蘇](../Page/江蘇.md "wikilink")[蘇州人](../Page/蘇州.md "wikilink")，[回族](../Page/回族.md "wikilink")。著名畫家、書法家。
  - [哈琼文](../Page/哈琼文.md "wikilink")，[北平人](../Page/北平.md "wikilink")，[回族](../Page/回族.md "wikilink")。著名畫家。

## 現代名人

### 中國大陸

  - [哈輝](../Page/哈輝.md "wikilink")，[中國大陸女歌手](../Page/中國大陸.md "wikilink")，被譽為民歌新四小花旦。
  - [哈鑫](../Page/哈鑫.md "wikilink")，著名男模，2006新絲路中國模特大賽冠軍。

### 台灣

  - [哈遠儀](../Page/哈遠儀.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[中視](../Page/中視.md "wikilink")《改變的起點》節目主持人，《中視新聞全球報導》[主播](../Page/主播.md "wikilink")。
  - [哈孝遠](../Page/哈孝遠.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")。

[哈遠儀與](../Page/哈遠儀.md "wikilink")[哈孝遠兩人為親戚關係](../Page/哈孝遠.md "wikilink")

### 香港

  - [哈永安](../Page/哈永安.md "wikilink"),
    香港[亞洲國際博覽館行政總裁](../Page/亞洲國際博覽館.md "wikilink")，他在[回族家族長大](../Page/回族.md "wikilink")，[漢化的回族後代](../Page/漢化.md "wikilink")，[粵語已成為他的母語](../Page/粵語.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 相關網站

  - [哈氏宗族的歷史](http://hi.baidu.com/%B9%FE%CA%CF%BA%F3%C8%CB%F0%A9%D4%C2%D7%A8%C0%B8)
  - 明清時期哈氏宗族的家譜
  - 河間哈氏宗族始祖入仕中原的原因和年代

[\*](../Category/哈姓.md "wikilink") [H哈](../Category/漢字姓氏.md "wikilink")

1.  摘自：《明代入附回回姓氏漢化考》
2.  摘自：《「皇明詔令」卷１》
3.  《摘自：「皇明詔令」卷３０》
4.  摘自：《「皇明詔令」卷５１》
5.  摘自：《「大明律」卷６，戶律，婚姻「蒙古色目人婚姻」》
6.  摘自：《「明代各民族人士入仕中原考」第1頁》
7.  見《巴西文集》卷上。
8.  《古元代東遷西域人屯田論述《西域研究》2001年第四期 馬建春》
9.  （《明孝宗實錄》卷４０；〔明〕《錦衣衛選薄》）
10. [康熙永平府志九](../Page/康熙.md "wikilink")、盧龍衛選簿二四
11. 燕山左衛選簿四八
12. 金山衛選簿二四
13. 《[明宣宗實錄](../Page/明宣宗.md "wikilink")》卷３３