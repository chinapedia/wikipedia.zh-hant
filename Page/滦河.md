**滦河**是华北地区第二大单独入海的河流，发源于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河北省北部](../Page/河北省.md "wikilink")，[承德市境内丰宁满族自治县西北的巴彦古尔图山北麓](../Page/承德.md "wikilink")，向北流入[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")，此段称为[闪电河](../Page/闪电河.md "wikilink")。后向东南急转进入[河北省东北部](../Page/河北省.md "wikilink")，一直向东南流入[渤海](../Page/渤海.md "wikilink")，全长885公里，总流域面积达4.46万平方千米。基本都在河北省境内。**滦河**主要支流有[武烈河](../Page/武烈河.md "wikilink")、[青龙河等](../Page/青龙河.md "wikilink")，它穿越[长城的地方形成长城的一座关口](../Page/长城.md "wikilink")—[喜峰口](../Page/喜峰口.md "wikilink")，流经较大城市只有[承德](../Page/承德.md "wikilink")。

滦河在[内蒙古高原的上游水流缓慢](../Page/内蒙古高原.md "wikilink")，河床宽浅；中游进入[燕山山脉](../Page/燕山.md "wikilink")，坡陡流急，下游进入[冀东平原重又变缓](../Page/冀东平原.md "wikilink")，河道中淤积很多，承德以下可行小船。[清朝时](../Page/清朝.md "wikilink")，皇帝曾从[承德乘大船从其支流武烈河进入干流再转入支流](../Page/承德.md "wikilink")[瀑河向](../Page/瀑河.md "wikilink")[辽宁方向进发](../Page/辽宁.md "wikilink")。但20世纪后半叶以后，由于沿岸植被破坏严重，水流减少，已经不能行大船。

滦河的中上游只经过承德一个城市，承德的工业不发达。流域内基本是非灌溉型的山区农业，因此滦河水量还比较充沛，水质比较清洁。[迁西县城以上有](../Page/迁西.md "wikilink")[大黑汀水库](../Page/大黑汀水库.md "wikilink")，瀑河和干流交汇处有[潘家口水库](../Page/潘家口水库.md "wikilink")。为了缓解海河下游严重缺水的状况，1983年由这两座水库引水，建成[引滦入津工程](../Page/引滦入津工程.md "wikilink")，为[天津和](../Page/天津.md "wikilink")[唐山调水](../Page/唐山.md "wikilink")，现在经[蓟县](../Page/蓟县.md "wikilink")[于桥水库](../Page/于桥水库.md "wikilink")、[北运河向天津调运的滦河水已经成为天津的主要水源](../Page/北运河.md "wikilink")。

滦河沿岸风景秀丽，[郦道元在](../Page/郦道元.md "wikilink")《[水经注](../Page/水经注.md "wikilink")》中就曾经提到过濡水（滦河的古名）支流武烈水畔的“磬锤峰”。中上游原热河省的区域，在[清朝时属于皇家禁区](../Page/清朝.md "wikilink")，两岸山上都是高大的松树，民国以后移民增多，植被破坏严重，再加上军阀和[日本侵略军的盗伐](../Page/日本.md "wikilink")，现在除了[避暑山庄内已经很少能见到大树了](../Page/避暑山庄.md "wikilink")，但次生灌木仍然能组成青山绿水的风景。下游两岸是优质[稻米的主要产区](../Page/稻.md "wikilink")。

随着经济的发展，目前水质污染问题已经成为需要关注的问题，天津市每年都要对滦河中上游地区给予经济补贴，以帮助维护水质清洁。
[LuanheYuan2.jpg](https://zh.wikipedia.org/wiki/File:LuanheYuan2.jpg "fig:LuanheYuan2.jpg")

## 主要支流

  - [闪电河](../Page/闪电河.md "wikilink")
  - [小滦河](../Page/小滦河.md "wikilink")
  - [兴州河](../Page/兴州河.md "wikilink")
  - [伊逊河](../Page/伊逊河.md "wikilink")
  - [武烈河](../Page/武烈河.md "wikilink")
  - [柳河](../Page/柳河.md "wikilink")
  - [长河](../Page/长河.md "wikilink")
  - [青龙河](../Page/青龙河.md "wikilink")

[L](../Category/河北河流.md "wikilink") [L](../Category/内蒙古河流.md "wikilink")