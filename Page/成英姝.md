**成英姝**（），籍貫[江蘇](../Page/江蘇.md "wikilink")[興化](../Page/興化.md "wikilink")，出生於[台北](../Page/台北.md "wikilink")。[台灣](../Page/台灣.md "wikilink")[作家](../Page/作家.md "wikilink")。

## 生平

畢業於[北一女中](../Page/北一女中.md "wikilink")、[國立清華大學](../Page/國立清華大學.md "wikilink")[化學工程學系](../Page/化學工程.md "wikilink")，曾任環境工程師、[電視節目企劃製作](../Page/電視節目.md "wikilink")、電視節目[主持人](../Page/主持人.md "wikilink")、[編劇](../Page/編劇.md "wikilink")、報社總經理等。目前專職寫作。

作品曾獲得[第三屆時報百萬文學獎首獎](../Page/第三屆時報百萬文學獎首獎.md "wikilink")，並獲[文建會選為](../Page/文建會.md "wikilink")2000年十大文學人之一，以及[中國文藝協會第四十八屆](../Page/中國文藝協會.md "wikilink")[中國文藝獎章](../Page/中國文藝獎章.md "wikilink")。

經常被稱為「[美女作家](../Page/美女作家.md "wikilink")」的她，本身並不特別喜好這個頭銜。不過在女性書寫方面亦有相當成就，經常撰寫相關小說或專欄文章。另外也從事[裝置藝術與](../Page/裝置藝術.md "wikilink")[攝影創作](../Page/攝影.md "wikilink")。

成英姝的祖父為武俠小說作家[成鐵吾](../Page/成鐵吾.md "wikilink")。

## 作品

  - [小說](../Page/小說.md "wikilink")
      - 《寂光與烈焰》[長篇小說](../Page/長篇小說.md "wikilink")（[印刻出版社](../Page/印刻出版社.md "wikilink")）
      - 《惡魔的習藝》短篇小說集（印刻出版社）
      - 《人間異色之感官胡亂推理事件簿》[長篇小說](../Page/長篇小說.md "wikilink")（[九歌出版社](../Page/九歌出版社.md "wikilink")）
      - 《哀歌》[長篇小說](../Page/長篇小說.md "wikilink")（[聯合文學](../Page/聯合文學.md "wikilink")）
      - 《男妲》[長篇小說](../Page/長篇小說.md "wikilink")（[聯合文學](../Page/聯合文學.md "wikilink")）
      - 《地獄門》長篇小說（[皇冠文化](../Page/皇冠文化.md "wikilink")）
      - 《似笑那樣遠，如吻這樣近》長篇小說（[印刻出版社](../Page/印刻出版社.md "wikilink")）
      - 《無伴奏安魂曲》長篇小說（[時報文化](../Page/時報文化.md "wikilink")）
      - 《人類不宜飛行》長篇小說（聯合文學）
      - 《究極無賴》[中篇小說集](../Page/中篇小說.md "wikilink")（印刻出版社）
      - 《恐怖偶像劇》[短篇小說](../Page/短篇小說.md "wikilink")（印刻出版社）
      - 《好女孩不做》短篇小說集 （聯合文學）
      - 《公主徹夜未眠》短篇小說集 （聯合文學）
      - 《私人放映室》[極短篇小說集](../Page/極短篇小說.md "wikilink")（聯合文學）

<!-- end list -->

  - [散文](../Page/散文.md "wikilink")
      - 《戀愛無用論》（圓神）
      - 《女流之輩》（聯合文學）

<!-- end list -->

  - [攝影](../Page/攝影.md "wikilink")
      - 《魔術奇花》（印刻）\[1\]

<!-- end list -->

  - [其他](../Page/其他.md "wikilink")
      - 《神之手2：透視你的生命藍圖》（心靈工坊）
      - 《神之手：認識你內在的二十二種神祕人格》（心靈工坊）

## 註釋

## 外部連結

  - [成英姝--中時部落格](http://blog.chinatimes.com/indiacheng/)
  - [博客來OKAPI人物專訪](http://okapi.books.com.tw/index.php/p3/p3_detail/sn/2476)

[Category:台灣女性作家](../Category/台灣女性作家.md "wikilink")
[C](../Category/國立清華大學校友.md "wikilink")
[C](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[C](../Category/江蘇裔台灣人.md "wikilink") [Y](../Category/成姓.md "wikilink")

1.  [\>台灣作家作品資料庫](http://163.22.7.6/literature_tw)