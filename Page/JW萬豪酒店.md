**JW萬豪酒店**（全稱：**JW萬豪奢華酒店及度假村**，）是[萬豪國際旗下的](../Page/萬豪國際.md "wikilink")[旗艦](../Page/旗艦.md "wikilink")[酒店](../Page/酒店.md "wikilink")[品牌](../Page/品牌.md "wikilink")。本品牌酒店總部設於
[法國](../Page/法國.md "wikilink")[巴黎及](../Page/巴黎.md "wikilink")[美國](../Page/美國.md "wikilink")[馬里蘭州](../Page/馬里蘭州.md "wikilink")。品牌的主要競爭對手為[喜達屋的](../Page/喜達屋酒店及度假酒店國際集團.md "wikilink")[威斯汀酒店](../Page/威斯汀酒店.md "wikilink")（同為萬豪國際集團擁有）及[希爾頓酒店集團的](../Page/希爾頓酒店集團.md "wikilink")[港麗酒店](../Page/港麗酒店.md "wikilink")，甚至是向[四季酒店](../Page/四季酒店.md "wikilink")、[麗思卡爾頓酒店](../Page/麗思卡爾頓酒店.md "wikilink")（同為萬豪國際集團擁有）及[柏悅酒店等豪華品牌挑戰](../Page/柏悅酒店.md "wikilink")。

截至2017年8月為止，全球共有85間JW萬豪奢華酒店及度假村營運中。\[1\]酒店酬賓方案為全球萬豪禮賞會員「Marriott
Rewards」。

## 全球營運據點

[Christmas_2016_Decorations_Grosvenor_House_London_United_Kingdom.jpg](https://zh.wikipedia.org/wiki/File:Christmas_2016_Decorations_Grosvenor_House_London_United_Kingdom.jpg "fig:Christmas_2016_Decorations_Grosvenor_House_London_United_Kingdom.jpg")[倫敦](../Page/倫敦.md "wikilink")
Grosvenor House JW萬豪酒店\]\]
[JW_Marriott_Desert_Springs_Foyer.jpg](https://zh.wikipedia.org/wiki/File:JW_Marriott_Desert_Springs_Foyer.jpg "fig:JW_Marriott_Desert_Springs_Foyer.jpg")[加州](../Page/加州.md "wikilink")
Desert Springs JW萬豪酒店\]\]
[JW_Marriott_Essex_House_in_New_York_City_2015.JPG](https://zh.wikipedia.org/wiki/File:JW_Marriott_Essex_House_in_New_York_City_2015.JPG "fig:JW_Marriott_Essex_House_in_New_York_City_2015.JPG")[紐約JW萬豪酒店](../Page/紐約.md "wikilink")\]\]
[JW_Marriott_Singapore_South_Beach,_Singapore_October_2017.jpg](https://zh.wikipedia.org/wiki/File:JW_Marriott_Singapore_South_Beach,_Singapore_October_2017.jpg "fig:JW_Marriott_Singapore_South_Beach,_Singapore_October_2017.jpg")
South Beach JW萬豪酒店\]\]
[JWMDC.JPG](https://zh.wikipedia.org/wiki/File:JWMDC.JPG "fig:JWMDC.JPG")[華盛頓特區JW萬豪酒店](../Page/華盛頓特區.md "wikilink")\]\]

### 歐洲

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>數量</p></th>
<th><p>城市</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/英國.md" title="wikilink">英國</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法國.md" title="wikilink">法國</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/坎城.md" title="wikilink">坎城</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/義大利.md" title="wikilink">義大利</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/威尼斯.md" title="wikilink">威尼斯</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞塞拜然.md" title="wikilink">亞塞拜然</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/巴庫.md" title="wikilink">巴庫</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/安卡拉.md" title="wikilink">安卡拉</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅馬尼亞.md" title="wikilink">羅馬尼亞</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/布加勒斯特.md" title="wikilink">布加勒斯特</a></p></td>
</tr>
</tbody>
</table>

### 美洲

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>數量</p></th>
<th><p>城市</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
<td><p>24</p></td>
<td><p><a href="../Page/紐約.md" title="wikilink">紐約</a>、<a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a>、<a href="../Page/舊金山.md" title="wikilink">舊金山</a>、<a href="../Page/拉斯維加斯.md" title="wikilink">拉斯維加斯</a>、<a href="../Page/休士頓.md" title="wikilink">休士頓</a>、<a href="../Page/亞特蘭大.md" title="wikilink">亞特蘭大</a>、<a href="../Page/芝加哥.md" title="wikilink">芝加哥</a>、<a href="../Page/紐奧良.md" title="wikilink">紐奧良</a>、<a href="../Page/奧蘭多.md" title="wikilink">奧蘭多</a>、<a href="../Page/華盛頓特區.md" title="wikilink">華盛頓特區</a>、<a href="../Page/邁阿密.md" title="wikilink">邁阿密</a> (2)、<a href="../Page/鳳凰城.md" title="wikilink">鳳凰城</a>、<a href="../Page/斯科茨代爾_(亞利桑那州).md" title="wikilink">斯科茨代爾</a>、<a href="../Page/土桑.md" title="wikilink">土桑</a>、<a href="../Page/棕櫚沙漠_(加利福尼亞州).md" title="wikilink">棕櫚沙漠</a>、<a href="../Page/聖塔莫尼卡.md" title="wikilink">聖塔莫尼卡</a>、<a href="../Page/丹佛.md" title="wikilink">丹佛</a>、<a href="../Page/馬可島.md" title="wikilink">馬可島</a>、<a href="../Page/印第安納波利斯.md" title="wikilink">印第安納波利斯</a>、<a href="../Page/大急流城.md" title="wikilink">大急流城</a>、<a href="../Page/明尼亞波利斯.md" title="wikilink">明尼亞波利斯</a>、<a href="../Page/奧斯丁.md" title="wikilink">奧斯丁</a>、<a href="../Page/聖安東尼奧.md" title="wikilink">聖安東尼奧</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加拿大.md" title="wikilink">加拿大</a></p></td>
<td><p>2</p></td>
<td><p><a href="../Page/溫哥華.md" title="wikilink">溫哥華</a>、<a href="../Page/Minett.md" title="wikilink">Minett</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
<td><p>4</p></td>
<td><p><a href="../Page/墨西哥市.md" title="wikilink">墨西哥市</a> (2)、<a href="../Page/坎昆.md" title="wikilink">坎昆</a>、<a href="../Page/San_José_del_Cabo.md" title="wikilink">San José del Cabo</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/里約熱內盧.md" title="wikilink">里約熱內盧</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴拿馬.md" title="wikilink">巴拿馬</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/Rio_Hato.md" title="wikilink">Rio Hato</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/厄瓜多.md" title="wikilink">厄瓜多</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/基多.md" title="wikilink">基多</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哥倫比亞.md" title="wikilink">哥倫比亞</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/波哥大.md" title="wikilink">波哥大</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秘魯.md" title="wikilink">秘魯</a></p></td>
<td><p>2</p></td>
<td><p><a href="../Page/利馬.md" title="wikilink">利馬</a>、<a href="../Page/庫斯科.md" title="wikilink">庫斯科</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/委內瑞拉.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/卡拉卡斯.md" title="wikilink">卡拉卡斯</a></p></td>
</tr>
</tbody>
</table>

### 亞洲

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>數量</p></th>
<th><p>城市</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
<td><p>2</p></td>
<td><p><a href="../Page/首爾.md" title="wikilink">首爾</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
<td><p>13</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a> (2)、<a href="../Page/上海.md" title="wikilink">上海</a> (2)、<a href="../Page/香港.md" title="wikilink">香港</a>、<a href="../Page/澳門.md" title="wikilink">澳門</a> 、<a href="../Page/深圳.md" title="wikilink">深圳</a> (2)、<a href="../Page/杭州.md" title="wikilink">杭州</a>、<a href="../Page/重慶.md" title="wikilink">重慶</a>、<a href="../Page/成都.md" title="wikilink">成都</a>、<a href="../Page/鄭州.md" title="wikilink">鄭州</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泰國.md" title="wikilink">泰國</a></p></td>
<td><p>3</p></td>
<td><p><a href="../Page/曼谷.md" title="wikilink">曼谷</a>、<a href="../Page/普吉島.md" title="wikilink">普吉島</a>、<a href="../Page/攀牙.md" title="wikilink">攀牙</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬來西亞.md" title="wikilink">馬來西亞</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td><p>2</p></td>
<td><p><a href="../Page/河內.md" title="wikilink">河內</a>、<a href="../Page/富國島.md" title="wikilink">富國島</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印度.md" title="wikilink">印度</a></p></td>
<td><p>8</p></td>
<td><p><a href="../Page/新德里.md" title="wikilink">新德里</a>、<a href="../Page/孟買.md" title="wikilink">孟買</a> (2)、<a href="../Page/加爾各答.md" title="wikilink">加爾各答</a>、<a href="../Page/邦加羅爾.md" title="wikilink">邦加羅爾</a>、<a href="../Page/昌迪加爾.md" title="wikilink">昌迪加爾</a>、<a href="../Page/浦那.md" title="wikilink">浦那</a>、<a href="../Page/穆索里.md" title="wikilink">穆索里</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印尼.md" title="wikilink">印尼</a></p></td>
<td><p>3</p></td>
<td><p><a href="../Page/雅加達.md" title="wikilink">雅加達</a>、<a href="../Page/泗水.md" title="wikilink">泗水</a>、<a href="../Page/棉蘭.md" title="wikilink">棉蘭</a></p></td>
</tr>
</tbody>
</table>

### 中東地區與及非洲

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>數量</p></th>
<th><p>城市</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿拉伯聯合大公國.md" title="wikilink">阿拉伯聯合大公國</a></p></td>
<td><p>2</p></td>
<td><p><a href="../Page/杜拜.md" title="wikilink">杜拜</a> (2)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃及.md" title="wikilink">埃及</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/開羅.md" title="wikilink">開羅</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/科威特.md" title="wikilink">科威特</a></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/科威特市.md" title="wikilink">科威特市</a></p></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  - [JW萬豪酒店─奢華酒店及度假村](http://www.marriott.com/jw-marriott/travel.mi)
  - [JW萬豪酒店廣告](https://www.youtube.com/watch?v=vrq7bMlj73w)

[JW万豪酒店](../Category/JW万豪酒店.md "wikilink")
[Category:万豪国际酒店集团品牌](../Category/万豪国际酒店集团品牌.md "wikilink")
[W万](../Category/旅馆品牌.md "wikilink")
[W](../Category/華盛頓哥倫比亞特區公司.md "wikilink")
[Category:馬里蘭州公司](../Category/馬里蘭州公司.md "wikilink")

1.