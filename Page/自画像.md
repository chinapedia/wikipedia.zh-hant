**自画像**是指作者以自己為對象所繪畫的[肖像作品](../Page/肖像.md "wikilink")。

法国艺术家Jean
Fouquet于1450前后创作的自画像或为西方传统中现存的最早的自画像，若不拘泥于平面，则埃及法老Akhenaten之雕塑家Bak早于公元前1365年前后已完成雕刻自己与其妻Taheri的作品。

[西方绘画传统中的自画像](../Page/西方.md "wikilink")，于[文艺复兴时代为人瞩目](../Page/文艺复兴.md "wikilink")。当时，[艺术家被视为优秀乃至卓越的个体](../Page/艺术家.md "wikilink")。独立的自画像的出现，常与给予[艺术家的这种巨大肯定相联系](../Page/艺术家.md "wikilink")。这一时期亦有大量的自画像涉及作画者之外的他人。这其中，有赞助人，或是[親屬](../Page/親屬.md "wikilink")。亦有艺术家在画幅中纳入友人、情人、老师、学生、[模特甚至死神](../Page/模特.md "wikilink")。

在[文艺复兴的](../Page/文艺复兴.md "wikilink")[意大利](../Page/意大利.md "wikilink")，自画像被称为“镜中肖像”。当时的自画像将艺术家本人描绘为[绅士或](../Page/绅士.md "wikilink")[淑女](../Page/淑女.md "wikilink")，而并不表现艺术家[工作的情形](../Page/工作.md "wikilink")。从16世纪中起，艺术家们开始选择表现他们自己与[画架为伍](../Page/画架.md "wikilink")，[手持](../Page/手.md "wikilink")[调色板和画笔](../Page/调色板.md "wikilink")，支着[腕杖](../Page/腕杖.md "wikilink")。

每一幅自画像都可被视为一种[表演](../Page/表演.md "wikilink")，艺术家为其[观众选择一种特别的装束与姿态](../Page/观众.md "wikilink")，呈现一个[角色](../Page/角色.md "wikilink")。几个世纪以来，无数画家耕耘于这一题材，[梵高](../Page/梵高.md "wikilink")、[伦勃朗](../Page/伦勃朗.md "wikilink")、[雷诺兹](../Page/雷诺兹.md "wikilink")（Joshua
Reynolds)、[弗里达·卡罗](../Page/弗里达·卡罗.md "wikilink")（Frida
Kahlo)等尤其留下数量可观的自画像。亦有[收藏专精于这一领域](../Page/收藏.md "wikilink")，最著名的即是[佛罗伦萨的](../Page/佛罗伦萨.md "wikilink")[美第奇家族的藏品](../Page/美第奇家族.md "wikilink")。

## 有名的自畫像

<File:Leonardo_self.jpg>|

<center>

[达·芬奇](../Page/达·芬奇.md "wikilink")

</center>

<File:VanGogh> 1887 Selbstbildnis.jpg|

<center>

[-{zh-hans:文森特·凡高; zh-hant:文生·梵谷;}-](../Page/文森特·梵谷.md "wikilink")

</center>

<File:Van_Gogh_self_portrait_as_an_artist.jpg>|

<center>

[-{zh-hans:文森特·凡高; zh-hant:文生·梵谷;}-](../Page/文森特·梵谷.md "wikilink")

</center>

<File:Rembrandt> Self-portrait (Kenwood).jpg|

<center>

[倫勃朗](../Page/倫勃朗.md "wikilink")

</center>

## 參見

  - [自拍](../Page/自拍.md "wikilink")

[自画像](../Page/category:自画像.md "wikilink")
[category:肖像畫](../Page/category:肖像畫.md "wikilink")