**SopCast**是一套基於[P2P通訊協定的網路電視軟體](../Page/P2P.md "wikilink")。它可以在許多平台上執行，包括微軟[Windows平台](../Page/Windows.md "wikilink")、[Linux平台](../Page/Linux.md "wikilink")、[Android平台](../Page/Android.md "wikilink")、[Mac平台](../Page/Mac.md "wikilink")。SopCast目前为免费软件。SopCast同时提供节目录制功能。
SopCast支持多国语言，目前Windows最新版本为4.2.0。

## 外部連結

  - [官方網站](http://arquivo.pt/wayback/20090718071556/http://www.sopcast.org/)

[Category:P2P](../Category/P2P.md "wikilink")