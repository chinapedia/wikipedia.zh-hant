《**再見理想**》是[香港搖滾樂隊](../Page/香港.md "wikilink")[Beyond第一張自資粵語專輯](../Page/Beyond.md "wikilink")。

## 專輯介紹

《再见理想》以盒帶發行混合广东歌、英文歌的电子音乐特色，其中大部分歌曲反映的，是他们在「地下樂團」不被大众所接受时的感受：失落与无助。《再见理想》为[香港带来一股前卫的气息](../Page/香港.md "wikilink")，Beyond也向职业歌坛踏出了第一步。

## 曲目

1.  永遠等待 5:06
2.  巨人 5:21
3.  Long Way Without Friends 7:22
4.  Last Man Who Knows You 1:53
5.  Myth 6:34
6.  再見理想 7:18
7.  Dead Romance (Part I) 6:53
8.  舊日的足跡 6:22
9.  誰是勇敢 6:18
10. The Other Door 4:24
11. 飛越苦海 4:19
12. 木結他(即興彈奏) 1:34
13. Dead Romance (Part II) 4:03

## 音樂

  - [永遠等待](http://www.youtube.com/watch?v=V3VmEVl_9Zg&feature=related)
  - [巨人](http://www.youtube.com/watch?v=o3vCcP054hA&feature=related)
  - [Long Way Without
    Friends](http://www.youtube.com/watch?v=r7u9sjirnSQ&feature=related)
  - [Last Man Who Knows
    You](http://www.youtube.com/watch?v=1scmnNaoOD8&feature=related)
  - [Myth](http://www.youtube.com/watch?v=5CmTZeCPK0c&NR=1)
  - [再見理想](http://www.youtube.com/watch?v=Nu3YFR37oEg&feature=related)
  - [Dead Romance (Part I)](http://www.youtube.com/watch?v=Ryzk0xYrAwc)
  - [舊日的足跡](http://www.youtube.com/watch?v=GZ4uUwf1Gr4&feature=related)
  - [誰是勇敢](http://www.youtube.com/watch?v=jhEujEb38lc&feature=related)
  - [The Other
    Door](http://www.youtube.com/watch?v=SAdcLHvZXiQ&feature=related)
  - [飛越苦海](http://www.youtube.com/watch?v=Bm0lSwnz1-E&feature=related)
  - [木結他](http://www.youtube.com/watch?v=25bt5Unkg0E&feature=related)
  - [Dead Romance (Part
    II)](http://www.youtube.com/watch?v=105sM7_ZMT4&feature=related)

[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:1986年音樂專輯](../Category/1986年音樂專輯.md "wikilink")
[Category:Beyond音樂專輯](../Category/Beyond音樂專輯.md "wikilink")