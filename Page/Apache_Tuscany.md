**Apache
Tuscany**提供一个[面向服务的核心架构以支持简单快速地开发和运行面向服务的应用程序](../Page/面向服务的架构.md "wikilink")。其轻巧的运行环境为嵌入或加载到不同的平台而设计。Apache
Tuscany实现[服务组件架构](../Page/服务组件架构.md "wikilink")（SCA）标准，后者定义了一个简单的基于服务的模型用于创建、组装和发布独立于编程语言的服务网络，包括现有或新开发的服务。目前Tuscany社区正在开发SCA
1.0版本。Apache
Tuscany也同时实现[服务数据对象](../Page/服务数据对象.md "wikilink")（SDO）标准，后者提供统一的接口处理在服务网络内传递的不同格式的数据包括[XML文档](../Page/XML.md "wikilink")，并可追踪数据变化。目前Tuscany支持SDO
2.1版本。SCA和SDO技术相互独立，也可协同使用以更好支持SOA。Tuscany同时提供[Java和](../Page/Java.md "wikilink")[C++的实现](../Page/C++.md "wikilink")。

## 外部链接

  - [Apache Tuscany项目主页](http://tuscany.apache.org/)
  - [开放面向服务体系结构：SCA及SDO技术官方网站](https://web.archive.org/web/20070902023753/http://www.osoa.org/display/Main/Home)
  - [OASIS OpenCSA主页](http://www.oasis-opencsa.org/sca)

[Category:Apache软件基金会](../Category/Apache软件基金会.md "wikilink")
[Category:企业应用集成](../Category/企业应用集成.md "wikilink")
[Category:面向服务体系相关产品](../Category/面向服务体系相关产品.md "wikilink")