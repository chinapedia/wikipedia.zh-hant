**櫻井智**（本名**八田友江**，），是一名[日本女](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")、歌手及舞台劇演員。[千葉縣](../Page/千葉縣.md "wikilink")[市川市出身](../Page/市川市.md "wikilink")，身高158cm，血型AB型，在1999年6月結婚，育有一女一子。為朝倉薰劇團的看板[女優](../Page/女優.md "wikilink")。2016年9月1日宣布引退。2017年手機遊戲【歌マクロススマホDeカルチャー】的【米蓮.吉納斯】一角由[平野綾替演](../Page/平野綾.md "wikilink")。

從事聲優前，她曾是一名偶像[歌手](../Page/歌手.md "wikilink")（Lemon Angel其中一員）。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

**1987年**

  - レモンエンジェル (アニメ)（**トモ：桜井智**）

**1993年**

  - ドラゴンリーグ（ウィナ姫）

**1994年**

  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")(瑪琳)
  - [超時空要塞 7](../Page/超時空要塞_7.md "wikilink")(**米蓮·吉納斯**)

**1995年**

  - [神秘的世界](../Page/神秘的世界.md "wikilink")(雪拉·雪拉)
  - [怪盜Saint Tail](../Page/怪盜St._Tail.md "wikilink")(**羽丘芽美**)
  - クマのプー太郎（超美人）

**1996年**

  - [奧運高手](../Page/奧運高手.md "wikilink")（**折笠麗子**）
  - [浪客劍心](../Page/浪客劍心.md "wikilink")（**卷町操**）

**1998年**

  - [神秘的世界エルハザード](../Page/神秘的世界.md "wikilink")(雪拉·雪拉)
  - [麗佳公主](../Page/麗佳公主.md "wikilink")(**娃娃騎士小香**)

**2005年**

  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")(坂井千草)
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")(竹蘭)
  - [舞-乙HiME](../Page/舞-乙HiME.md "wikilink")(蕾娜·賽亞斯)

**2006年**

  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")(齋王美壽知)

**2007年**

  - [偶像大師
    XENOGLOSSIA](../Page/偶像大師_XENOGLOSSIA.md "wikilink")（**三浦あずさ**）
  - [大江戶火箭](../Page/大江戶火箭.md "wikilink")(青い女)
  - [灼眼的夏娜II](../Page/灼眼的夏娜.md "wikilink")(坂井千草)
  - [殭屍借貸](../Page/殭屍借貸.md "wikilink")(橘千鶴（思徒之母））
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")(竹蘭)
  - [名侦探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")(小倉朔子、早織)

**2008年**

  - [破天荒遊戲](../Page/破天荒遊戲.md "wikilink")(ジェンフープの妻)

**2009年**

  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")(日輪)
  - [哆啦A夢](../Page/哆啦A夢.md "wikilink")(羅布魯)
  - [Phantom -PHANTOM OF
    INFERNO-](../Page/Phantom_-PHANTOM_OF_INFERNO-.md "wikilink")(美緒之母)
  - [卡片鬥士翔](../Page/卡片鬥士翔.md "wikilink")(布拉格、華れい子)

**2010年**

  - [只有神知道的世界](../Page/只有神知道的世界.md "wikilink")（飛鳥空）
  - 名偵探柯南(名取深汐)

**2011年**

  - [灼眼的夏娜III](../Page/灼眼的夏娜.md "wikilink")（坂井千草）
  - [銀魂'](../Page/銀魂_\(動畫\).md "wikilink") (日輪)

**2012年**

  - [黑魔女學園](../Page/黑魔女學園.md "wikilink")(チョコのママ)
  - [神奇寶貝超級願望](../Page/神奇寶貝超級願望.md "wikilink")(竹蘭)

**2015年**

  - [银魂°](../Page/銀魂_\(動畫\).md "wikilink") (日輪)
  - [潮與虎](../Page/潮與虎.md "wikilink")（）

**2016年**

  - 名侦探柯南（潮路由香里 \#804-805）
  - [甲鐵城的卡巴內里](../Page/甲鐵城的卡巴內里.md "wikilink")（無名的母親）

### OVA

  - [我永遠的聖誕老人](../Page/我永遠的聖誕老人.md "wikilink") - 諾埃爾

### 電玩遊戲

  - [侍魂3](../Page/侍魂3.md "wikilink") - 莉姆露露
  - [碧藍幻想](../Page/碧藍幻想.md "wikilink") - 艾莉西亞

### 動畫專輯

  - [超時空要塞 7](../Page/超時空要塞_7.md "wikilink") - Live Bomber\!\! (as
    米蓮·吉納斯)
  - [超時空要塞 7](../Page/超時空要塞_7.md "wikilink") - Mylene Jenius sings Lynn
    Minmay (Album)
  - [超時空要塞 7](../Page/超時空要塞_7.md "wikilink") - Friends (with
    [飯島真理](../Page/飯島真理.md "wikilink")，Single)
  - [浪客劍心](../Page/浪客劍心.md "wikilink") - Ice Blue Eyes (as 卷町操)

### 個人專輯

  - Baby，Baby (1995，Single)
  - Chase Your Dream\! (1997，Single)
  - T-mode (Album)
  - Cherry (Album)
  - ACTRESS (Album)
  - ACTRESSII (Album)
  - Overlap (1998年，Album)
  - 12の旋律 (Album)
  - Summer Holiday (Album)

## 外部連結

  - [I'm Enterprise介紹頁](http://www.imenterprise.jp/data.php?id=14)
  - [個人Blog「910★BLOG」](https://archive.is/20130430210722/ameblo.jp/tomo8888blog/)

[pt:Anexo:Lista de
seiyū\#S](../Page/pt:Anexo:Lista_de_seiyū#S.md "wikilink")

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")