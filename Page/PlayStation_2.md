**PlayStation
2**（日文：**プレ**イ**ステ**ーション**2**，日文簡稱**プレステ2**。），簡稱**PS2**，是[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")（SCE）在2000年3月4日開始販售的家用[遊戲機](../Page/電視遊戲.md "wikilink")。在[中国大陆](../Page/中国大陆.md "wikilink")，索尼（中國）的行銷口號是：「△○×□，很有[PS风格的感觉](../Page/PlayStation.md "wikilink")」。在[台灣](../Page/台灣.md "wikilink")，索尼電腦娛樂（台湾）則是以：「就是這一部」，強調不仅能玩遊戲更能播放[DVD](../Page/DVD.md "wikilink")。主要競爭對手是世嘉的[Dreamcast](../Page/Dreamcast.md "wikilink")、微軟的[Xbox和任天堂的](../Page/Xbox_\(遊戲機\).md "wikilink")[GameCube](../Page/GameCube.md "wikilink")。PS2平台上已推出了達10,828款遊戲。\[1\]PS2擁有數個[家用遊戲機的紀錄](../Page/家用遊戲機.md "wikilink")：史上最高銷量（1億5768萬部）、销售时间最長（12年）、官方廠商支持時間最長（18年）。

索尼娱乐于2012年12月28日在日本官网上发布消息，確認PS2正式终止销售，至此全世界累计销售1亿5000万部以上。\[2\]日本Sony官方宣布主機維修期限至2018年8月31日止。\[3\]

## 歷史

1998年11月27日日本[世嘉](../Page/世嘉.md "wikilink")（SEGA）公司于1998年11月27日发售[Dreamcast遊戲機](../Page/Dreamcast.md "wikilink")，SONY便開始積極及加快開發第六世代家用遊戲機。PlayStation
2在1999年發表，2000年開始發售，由於生產延誤，當時市場上很難購買到PS2。由於PS2發售同時也同時發售多款遊戲大作，加上PS2採取當時較為先進的[DVD-ROM](../Page/DVD-ROM.md "wikilink")，容量達4.7或8.5GB，而Dreamcast只使用1.2GB的[GD-ROM](../Page/GD-ROM.md "wikilink")，同時SONY為遊戲開發商和為消費者提供更多的PS2支援，所以PS2很快便打敗[Dreamcast](../Page/Dreamcast.md "wikilink")，更間接導致SEGA完全退出家用游戏机市場。

儘管世嘉在2001年3月宣布停止生產Dreamcast，PS2又立即面臨來自新對手的競爭，微軟的[Xbox和任天堂](../Page/Xbox.md "wikilink")[GameCube](../Page/GameCube.md "wikilink")。許多分析家預計三大遊戲機之間的密切的三方對決：Xbox擁有最強大的硬件，而GameCube的是最便宜的遊戲機，加上任天堂改變其政策，以鼓勵第三方開發者，而PlayStation
2則是最弱的規格，但有良好的開端（擊敗[Dreamcast](../Page/Dreamcast.md "wikilink")）、不少的遊戲支援，加上SONY與開發商的承諾。雖然在PlayStation
2的陣容初步遊戲被認為是平庸的，但最後也領先[Xbox](../Page/Xbox.md "wikilink")，[GameCube](../Page/GameCube.md "wikilink")。尤其是[跑車浪漫旅系列](../Page/跑車浪漫旅系列.md "wikilink")、[GUNDAM系列](../Page/GUNDAM.md "wikilink")、[俠盜獵車手系列等等令到PS](../Page/俠盜獵車手系列.md "wikilink")2大賣一時。而微軟推出聯機功能的[Xbox
Live的發布之際](../Page/Xbox_Live.md "wikilink")，SONY在2002年底發布的PlayStation網絡適配器，而且與開發商的推了不少聯機遊戲。

2004年9月，索尼推出[俠盜獵車手：聖安地列斯及PS](../Page/俠盜獵車手：聖安地列斯.md "wikilink")2薄版**PlayStation
2
slimline**（SCPH-70000-90000）。及停止舊型號（SCPH-30000-50000）生產。使PS2再次大賣，更出現短缺現象。

SONY原來設定PS2的壽命為10年（即2010年），但根據SONY表示，它的生命週期將繼續下去，只要開發商繼續開發新的遊戲和有銷售需求，SONY願意繼續生產及支援PS2。

2012年12月28日，日本索尼電腦娛樂（SCEJ）在PlayStation
2（PS2）主機官方產品網頁上公告，日本地區PS2主機的供貨在今天正式結束，替PS2主機在日本長達12年9個月又24天的銷售劃下句點。

太平洋标准时间2016年3月31日早上6点，PS2平台上《最终幻想XI》的线上服务正式停止。\[4\]
4月2日，索尼正式宣布，PS2游戏的所有线上服务已经全部结束。\[5\]

2018年6月22日，日本索尼互動娛樂（SIE）发布通知，宣布从2018年8月31日起停止对PS2及其周边设备的售后服务。\[6\]

## 簡介

PlayStation 2

  - 是[PlayStation的繼承機種](../Page/PlayStation.md "wikilink")，
  - 銷售統計資料：
      - 2000年3月4日開始於[日本販售](../Page/日本.md "wikilink")，販售後僅過了三日，[日本當地就創下約](../Page/日本.md "wikilink")98萬部以上的生產銷售紀錄。
      - 2003年1月,PS2全球銷量突破五千萬部。
      - 2004年1月1日，PlayStation
        2在因故延遲十多日後，终於在2004年[元旦於](../Page/元旦.md "wikilink")[中国大陸開賣](../Page/中国大陸.md "wikilink")。
      - 2005年11月在全球創下一億部的生產銷售部數紀錄。
      - 2009年8月18日，索尼官方宣布PS2全球銷量達到破紀錄的一億四千萬部，是有史以來最暢銷的遊戲主機。
      - 2011年，PS2遊戲機的全球銷量已突破1.5億大關。
      - 2011至2012年的SONY聖誕期間銷售，根據Sony SCE發佈的官方情報，PS2的銷量為超過50萬，竟然比[PS
        Vita的接近](../Page/PS_Vita.md "wikilink")50萬為多。
  - PlayStation
    2本身可向下相容[PlayStation的遊戲軟體](../Page/PlayStation.md "wikilink")，並可播放DVD影音光碟。值得一提的是，PS2的下一代遊戲機[PlayStation
    3的早期部分機型是可以向下支援PS](../Page/PlayStation_3.md "wikilink")2遊戲的（詳見「硬體設計與PlayStation
    3的相容性」）。
  - PlayStation 2的游戏锁区。

[缩略图](https://zh.wikipedia.org/wiki/File:Ps215467890_IGP3832.jpg "fig:缩略图")

  - 直至2012年，PS2仍然銷售強悍，尤其是像東歐，中東，東南亞和南非的銷量。許多人士表示，PS2擁有優惠的價格，眾多遊戲作品加上部份經典遊戲，也能播放DVD，在[未發展國家或](../Page/未發展國家.md "wikilink")[發展中國家的良好家中遊戲機](../Page/發展中國家.md "wikilink")，因此推出超過十二年仍未淘汰。
  - 2012年12月28日SCEJ宣佈日本地區的PlayStation 2停止供貨。

PlayStation 2銷售時期的競爭對手：

  - [世嘉的](../Page/世嘉.md "wikilink")[Dreamcast](../Page/Dreamcast.md "wikilink")（2001年停產）
      - 由於未能打敗[SONY的PlayStation](../Page/索尼.md "wikilink")
        2，[SEGA宣布停產](../Page/世嘉.md "wikilink")[Dreamcast遊戲主機](../Page/Dreamcast.md "wikilink")，並退出家用遊戲主機的市場，但仍繼續開發遊戲軟體。
  - [任天堂的](../Page/任天堂.md "wikilink")[GameCube](../Page/GameCube.md "wikilink")（2007年停產）
      - [任天堂的新遊戲主機](../Page/任天堂.md "wikilink")[Wii發售後](../Page/Wii.md "wikilink")，宣布[GameCube全部停產](../Page/GameCube.md "wikilink")。
  - [微軟的](../Page/微軟.md "wikilink")[XBOX](../Page/XBOX.md "wikilink")（2009年停產）
      - 為了给XBOX的繼任機種[Xbox360上市拓展道路](../Page/Xbox360.md "wikilink")，[微軟的XBOX主机秘密停止市場宣傳](../Page/微軟.md "wikilink")，並在2009年宣布全部停產。

## PlayStation 2 slimline

2004年初，索尼宣布推出PlayStation 2的超薄版「PlayStation 2
slimline」，在2004年11月3日開售。價格與PlayStation
2一樣。但機身尺寸大小足足減少了130%，由30.1 cm X 17.6 cm X
7.8 cm減少至23 cm x 15.2 cm x
2.8 cm。性能與功能和原版沒有分別，依然相容任何PS1的遊戲。隨後PS2原版退出市場。直到目前為止PlayStation
2 slimline還是全世界最輕薄的家用遊戲機的記錄保持者。

## 硬體設計與PlayStation 3的相容性

PS2的核心，中央處理器（CPU）是採用Emotion
Engine（EE），EE為索尼與東芝合作開發的處理器，它是家用機領域首次采用的將GPU記憶體直接植入繪圖芯片的設計，EE整合了CPU核心、2組向量處理單元（VPU）、1組圖形處理單元（IPU），以及記憶體控制器、I/O介面、與圖形合成器（Graphics
Synthesizer, GS）溝通的介面，擁有16KB暫存記憶體（Scratchpad memory），8KB資料快取，16KB指令快取。

EE是採用128位架構，相比起個人電腦流行的16、32或64位架構，EE的設計領先許多，目的是加強CPU的尋址能力，提高同時處理大量數據的能力，以應付同時間的複雜畫面計算。

GS是16管線的GPU，並無特殊之處。但是記憶體介面設計非常前衛。記憶體匯流排被分成3個系統，包括1024bit讀取、1024bit寫入、512bit讀寫雙向，即總頻寬高達2560
bit，嵌入於GPU的DRAM可以提供48GB/s的資料傳輸頻寬。當時2000年的最強的[顯示卡](../Page/顯示卡.md "wikilink")[NVIDIA](../Page/NVIDIA.md "wikilink")
GeForce2 Ultra記憶體頻寬只有7.36GB/s。

PS2的後繼機種 PlayStation 3（PS3）的GPU記憶體頻寬只有22.4
GB/s，讓PS3對PS2軟體的相容性出了很大的問題。早期的解決方法是在PS3主機內加入EE和GS，硬是把PS2的核心零件塞進去，這樣PS3便能完全相容PS2遊戲。考慮到成本和PS2銷售（PS2在PS3推出後一段長時間仍銷售，在2007年更推出全新的PS2
SCPH-90000系列），因此索尼之後推出的PS3取消了EE，改以軟體模擬的方式執行PS2遊戲，但這會造成部份遊戲無法順利於PS3上運作，而且由於使用者無法得知哪一款PS2遊戲能相容以軟體模擬的PS3，最後索尼在這之後推出的PS3便完全不相容任何PS2的遊戲。

## 詳細規格

[Sony_EmotionEngine_CXD9615GB_top.jpg](https://zh.wikipedia.org/wiki/File:Sony_EmotionEngine_CXD9615GB_top.jpg "fig:Sony_EmotionEngine_CXD9615GB_top.jpg")
[CD-ROM_for_PlayStation2.jpg](https://zh.wikipedia.org/wiki/File:CD-ROM_for_PlayStation2.jpg "fig:CD-ROM_for_PlayStation2.jpg")
[DVD-ROM_for_PlayStation2.jpg](https://zh.wikipedia.org/wiki/File:DVD-ROM_for_PlayStation2.jpg "fig:DVD-ROM_for_PlayStation2.jpg")
[PlayStation_2_slim's_motherboard_(top).png](https://zh.wikipedia.org/wiki/File:PlayStation_2_slim's_motherboard_\(top\).png "fig:PlayStation_2_slim's_motherboard_(top).png")
[PlayStation2_in_Magical_of_Taipei_City_Mall_2006-07-15.jpg](https://zh.wikipedia.org/wiki/File:PlayStation2_in_Magical_of_Taipei_City_Mall_2006-07-15.jpg "fig:PlayStation2_in_Magical_of_Taipei_City_Mall_2006-07-15.jpg")

  - **[CPU](../Page/CPU.md "wikilink")**
      - 128 bit Emotion Engine
      - [運作時脈](../Page/運作時脈.md "wikilink")：294.912 MHz
      - 16KB暫存記憶體RAM (SPRAM)
      - 8KB資料快取
      - 16KB指令快取
      - [主記憶體](../Page/主記憶體.md "wikilink")：32MB Direct Rambus DRAM
      - 主記憶體頻寬：3.2GB/s
      - [浮點運算效能](../Page/FLOPS.md "wikilink")：6.2GFLOPS
      - 整數運算單元：64-bit
      - 多媒體擴充指令：128-bit \* 107個
      - 通用暫存器：128-bit \* 32個
      - TLB (Translation-Lookaside Buffer): 48 double entries
      - 直接記憶體存取（DMA）: 10 channels
      - 輔助處理器1: FPU
      - 輔助處理器2: VU0
      - 向量處理器：VU1
      - 製程：0.18μm（厚版）、90nm（薄版）
      - 核心電壓：1.8伏特
      - 耗電量：15W
      - 電晶體數：10.5 M
      - 晶片尺寸：240 mm^2
      - 封裝：540 sockets, PBGA
  - **[GPU](../Page/GPU.md "wikilink")**
      - Graphics Synthesizer
      - GPU運作時脈：147.456 MHz
      - [VRAM](../Page/VRAM.md "wikilink")：混合搭載4MB DRAM
      - [DRAM带宽](../Page/DRAM.md "wikilink")：48GB／秒
      - DRAM带宽：2560 bit
      - 像素结构：64位（24位RGB，8位alpha通道，32位z缓冲）
      - 像素填充率：2.4G像素／秒
  - **音效**
      - SPU2+CPU (PCM)
      - SPU2用[RAM](../Page/隨機存取記憶體.md "wikilink")：2MB
      - [Dolby
        Digital](../Page/Dolby_Digital.md "wikilink")、[DTS](../Page/DTS.md "wikilink")
        96/24
  - **媒體**
      - DVD-ROM光碟機
          - DVD為4倍速，CD為24倍速讀取。
  - **影像輸出能力**
      - [AV端子](../Page/AV端子.md "wikilink")、[S-端子](../Page/S-端子.md "wikilink")、[色差端子](../Page/色差端子.md "wikilink")
  - **原生輸出解析度**
      - [480i](../Page/480i.md "wikilink")、[480p或](../Page/480p.md "wikilink")[1080i](../Page/1080i.md "wikilink")
  - **聲音輸出能力**
      - 紅白線、[S/PDIF](../Page/S/PDIF.md "wikilink")（光纖）

## 累計生產銷售量

  - 2000年3月4日於[日本全球首發](../Page/日本.md "wikilink")
  - 2001年3月23日全球1,000萬部
  - 2001年10月10日全球2,000萬部
  - 2002年5月全球3,000萬部
  - 2002年9月19日全球4,000萬部
  - 2003年1月15日全球5,000萬部
  - 2003年9月6日全球6,000萬部
  - 2004年1月13日全球7,000萬部
  - 2004年12月31日全球8,000萬部
  - 2005年6月2日全球9,000萬部
  - 2005年11月29日全球1億部（PlayStation 2成为当时最快达到一亿部销量的游戏机，现纪录保持者为[Nintendo
    DS](../Page/Nintendo_DS.md "wikilink")）

### 附屬週邊

  - Sony PlayStation 2 NETWORK ADAPTOR 網路卡（SCPH-10350）
  - Sony PlayStation 2 EXPANSION BAYタイプ 40GB（3.5吋 40GB硬碟）（SCPH-20401）
  - Sony PlayStation BB Unit（含PlayStation BB Navigator Version
    0.10（CD-ROM 兩張）和SCPH-10350及SCPH-20401）

## 型號

<table>
<thead>
<tr class="header">
<th><p>迭代</p></th>
<th><p>1代</p></th>
<th><p>2代</p></th>
<th><p>3代</p></th>
<th><p>4代</p></th>
<th><p>5代</p></th>
<th><p>6代</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>型号前缀</p></td>
<td><p>SCPH-1x000</p></td>
<td><p>SCPH-3x00x</p></td>
<td><p>SCPH-5x00x</p></td>
<td><p>SCPH-7000x</p></td>
<td><p>SCPH-7500x<br />
SCPH-7700x<br />
SCPH-7900x</p></td>
<td><p>SCPH-9000x</p></td>
</tr>
<tr class="even">
<td><p>GPU制程</p></td>
<td><p>0.25μm</p></td>
<td><p>0.18μm</p></td>
<td><p>0.18μm</p></td>
<td><p>90nm</p></td>
<td><p>90nm</p></td>
<td><p>90nm</p></td>
</tr>
<tr class="odd">
<td><p>CPU制程</p></td>
<td><p>0.25μm</p></td>
<td><p>0.18μm</p></td>
<td><p>0.18μm</p></td>
<td><p>90nm</p></td>
<td><p>90nm</p></td>
<td><p>90nm</p></td>
</tr>
<tr class="even">
<td><p>硬碟槽</p></td>
<td><p>無（有PCMCIA扩展卡槽）</p></td>
<td><p>有（IDE接口）</p></td>
<td><p>有（IDE接口）</p></td>
<td><p>無</p></td>
<td><p>無</p></td>
<td><p>無</p></td>
</tr>
<tr class="odd">
<td><p>HD Loader</p></td>
<td><p>不支援</p></td>
<td><p>支援</p></td>
<td><p>支援</p></td>
<td><p>支援（无硬盘位，需自行改造）</p></td>
<td><p>不支援</p></td>
<td><p>不支援</p></td>
</tr>
<tr class="even">
<td><p>USB端口</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>製造地</p></td>
<td><p>日本（至少有5家工厂参与组装）</p></td>
<td><p>日本<br />
3900x為日本/中國（中国方面，委托生产商为华硕/MAINTEK或富士康）</p></td>
<td><p>日本（第一批次）/中國</p></td>
<td><p>中國（注：2003年底索尼宣布PS2组装工作完全移转至中国）</p></td>
<td><p>中國</p></td>
<td><p>中國</p></td>
</tr>
<tr class="even">
<td><p>備註</p></td>
<td><p>只限日本发售，故障率高，有FMV（Full Motion Video）问题</p></td>
<td><p>开始出口欧美，可靠性极大提高</p></td>
<td><p>取消1394接口，降低风噪，生产简化</p></td>
<td><p>第一代薄版，使用外置电源</p></td>
<td></td>
<td><p>內建變壓器，发热大幅降低</p></td>
</tr>
</tbody>
</table>

### PlayStation 2

<table>
<thead>
<tr class="header">
<th><p>迭代</p></th>
<th><p>1代</p></th>
<th><p>2代</p></th>
<th><p>3代</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>型號</p></td>
<td><ul>
<li>SCPH-10000</li>
<li>SCPH-15000</li>
<li>SCPH-18000</li>
</ul></td>
<td><ul>
<li>SCPH-30000</li>
<li>SCPH-30001</li>
<li>SCPH-30001 R</li>
<li>SCPH-30002</li>
<li>SCPH-30002 R</li>
<li>SCPH-30003</li>
<li>SCPH-30003 R</li>
<li>SCPH-30004</li>
<li>SCPH-30004 R</li>
<li>SCPH-30005 R</li>
<li>SCPH-30006 R</li>
<li>SCPH-30007 R</li>
</ul>
<p><a href="https://zh.wikipedia.org/wiki/File:SCPH-30000_vertical.jpg" title="fig:SCPH-30000_vertical.jpg">SCPH-30000_vertical.jpg</a></p></td>
<td><ul>
<li>SCPH-35000 GT</li>
<li>SCPH-35001 GT</li>
<li>SCPH-35002 GT</li>
<li>SCPH-35003 GT</li>
<li>SCPH-35004 GT</li>
<li>SCPH-35005 GT</li>
<li>SCPH-35006 GT</li>
<li>SCPH-35007 GT</li>
</ul></td>
</tr>
</tbody>
</table>

### PlayStation 2 Slimline

<table>
<thead>
<tr class="header">
<th><p>迭代</p></th>
<th><p>4代</p></th>
<th><p>5代</p></th>
<th><p>6代</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>型號</p></td>
<td><ul>
<li>SCPH-70000 CB</li>
<li>SCPH-70001 CB</li>
<li>SCPH-70002 CB</li>
<li>SCPH-70003 CB</li>
<li>SCPH-70004 CB</li>
<li>SCPH-70005 CB</li>
<li>SCPH-70006 CB</li>
<li>SCPH-70007 CB</li>
<li>SCPH-70008 CB</li>
</ul>
<p><a href="https://zh.wikipedia.org/wiki/File:Playstation2_slim_front.jpg" title="fig:Playstation2_slim_front.jpg">Playstation2_slim_front.jpg</a></p></td>
<td><ul>
<li>SCPH-75000 CB/FF/SS</li>
<li>SCPH-75001 CB</li>
<li>SCPH-75002 CB</li>
<li>SCPH-75003 CB</li>
<li>SCPH-75004 CB</li>
<li>SCPH-75005 CB</li>
<li>SCPH-75006 CB</li>
<li>SCPH-75007 CB</li>
<li>SCPH-75008 CB</li>
</ul>
<p><a href="https://zh.wikipedia.org/wiki/File:SCPH-75007CB.jpg" title="fig:SCPH-75007CB.jpg">SCPH-75007CB.jpg</a></p></td>
<td><ul>
<li>SCPH-77000 CB/PK (Limited)</li>
<li>SCPH-77001 CB</li>
<li>SCPH-77002 CB</li>
<li>SCPH-77003 CB</li>
<li>SCPH-77004 CB</li>
<li>（無SCPH-77005 CB）</li>
<li>SCPH-77006 CB</li>
<li>SCPH-77007 CB</li>
<li>SCPH-77008 CB</li>
</ul></td>
</tr>
</tbody>
</table>

#### 型號說明

SCPH-9\*為內置電源[變壓器](../Page/變壓器.md "wikilink")，無需外置式電源[變壓器](../Page/變壓器.md "wikilink")

### 區域代碼

| 結尾區域代碼 | 0      | 1        | 2         | 3   | 4   | 5      | 6                          | 7      | 8         | 9      |
| ------ | ------ | -------- | --------- | --- | --- | ------ | -------------------------- | ------ | --------- | ------ |
| 地區     | 日本     | 北美、巴西等地區 | 澳洲、紐西蘭等地區 | 英國  | 歐洲  | 南韓     | 新加坡、馬來西亞、泰國、印度尼西亞、越南、香港等地區 | 台灣     | 俄羅斯、印度等地區 | 中國大陸   |
| 系統規格   | NTSC-J | NTSC-U/C | PAL       | PAL | PAL | NTSC-J | NTSC-J                     | NTSC-J | PAL       | NTSC-C |

### 顏色代碼

| 結尾顏色代碼 | CB | CW | PK  | AQ | SS | CR |
| ------ | -- | -- | --- | -- | -- | -- |
| 顏色     | 黑色 | 白色 | 粉紅色 | 藍色 | 銀色 | 紅色 |

## 游戏

### IGN網站前百大經典遊戲

[Top 100 PlayStation 2 Games](http://www.ign.com/top/ps2-games)

### VGChartz網站票選前十大經典遊戲

  - 汪達與巨像
  - Final Fantasy X
  - ICO古堡迷踪
  - 戰神I
  - 俠盜獵車手聖安地列斯
  - 惡靈古堡4
  - 潛龍諜影3
  - 王國之心IIFinalMix+
  - 跑車浪漫旅GT4
  - 女神異聞錄3

### Fami通台灣讀者票選遊戲

  - Final Fantasy X
  - 鬼武者3
  - 勇者鬥惡龍VIII
  - 真三國無雙4
  - 超級機器人大戰MX
  - Final Fantasy X-2
  - 戰國無雙
  - 潛龍諜影3
  - 勇者鬥惡龍V
  - 零-紅蝶
  - 第三次超級機器人大戰阿法終焉的銀河
  - 真三國無雙3
  - 跑車浪漫旅4
  - 王國之心
  - 火影忍者\~終極英雄2
  - 零\~刺青之聲

### Fami通滿分40分遊戲

  - Final Fantasy XII

### Fami通滿分39分遊戲

  - OKAMI大神
  - 跑車浪漫旅3: A-Spec
  - Final Fantasy X
  - 潛龍諜影3
  - 王國之心II

### Fami通滿分38分遊戲

  - 人中之龍2

## 周邊硬體

[File:DualShock2.jpg|DualShock](File:DualShock2.jpg%7CDualShock)
2（型號為SCPH-10010） <File:Memory> Card for PlayStation
2.jpg|8MB記憶卡（型號為SCPH-10020） <File:PlayStation> BB
Unit.jpg|PlayStation BB Unit（Expansion Bay型） <File:Ps2> linux
contents.jpg|PS2 Linux套件 <File:EyeToy> camera 2.jpg|EyeToy <File:Densya>
de GO\! controller type2
JPN.jpg|《[電車GO\!](../Page/電車GO!.md "wikilink")》專用控制器（第2型）

## 加密与破解

与前代Playstation一样，Playstation
2具有防盗版保护机制。原装压制的盘片1-23道具有逻辑混乱的误码，主机正是通过读取这部分误码来判断游戏是否为正版。刻录复制的盘片，误码由于会被电脑刻录机的自动纠错机制修复或忽略，因此不能通过PS2的检查。同时，PS2播放DVD电影的功能也会检查DVD的区码。\[7\]

唯一的例外是“开发机”(Playstation 2 Developer
Kit，货号DTL-T10000HA）没有设置任何防盗版保护机制，可以运行复制光碟和自制软件。但这种机器发行量非常少，索尼将这种设备视为资产，只以租借的形式提供给授权开发商，在PS2失去商业价值后索尼放松管理，才有少量流入市场。

传统的直读芯片（MODCHIP）法在PS2上市不久就被专业黑客开发出来，这种方法在PS1的破解上便有用到。他的原理是通过逻辑分析，定制出专门功能的单片机，让主机跳过误码判断直接读取盘片有效内容，一些MODCHIP也能帮助主机跳过DVD区码检查。索尼对MODCHIP曾采取过措施，包括硬件布局改动使得相关附加电路在物理上更难加装，后期出版的一些软件也具有在MODCHIP存在的情况下侦测并阻止复制光碟运行的功能。不过MODCHIP也是历经不断的改版升级，后期版本已经达到了接近完美的兼容性。

后来索尼为网络游戏提速而增设的网卡/硬盘扩展装置被黑客加以利用，而开发了可直接利用机载硬盘读取游戏的功能。尤其是在2008年推出的免费软件Free
Mcboot，利用了引导卡的权限漏洞，能以软件方式修改启动菜单使用户运行自制软件，一些自制软件，如HDLoader或OpenPS2Loader，功能强大，可突破原网卡套装40GB硬盘的限制(后期的版本甚至能识别48bit
LBA，能支持市售容量最大的IDE硬盘，容量高达750GB），并且能通过USB（因接口版本为1.1，速度较慢），硬盘，和PC
SMB网络服务器的方式直接运行游戏，完全绕开了原机光驱和防盗版的限制，而且根本不需对原机电路进行任何物理改造。由于此破解方式出现于PS2生命周期末期，索尼并没有下大力封堵，且网卡硬盘套件有限的售出量和2004年后生产的Slim型没有硬盘位的设计也限制了这种改造方式的影响，不过还是在PS2的最后一个版本（9000x后末期批次）在硬件上彻底堵绝了使用HDLoader的可能性，此举的象征意义大于经济意义。

Free
McBoot引导卡还有一种衍生软件叫做ESR，也叫做软直读，不需配套使用网卡或硬盘。原理是在电脑上处理原版镜像，把游戏盘伪装成DVD电影以欺骗主机开始读取。这种方法同样不需改装电路，只需有“引导卡”，但由于电影读取模式会把光驱的读取限定为单倍速，而在特定需要较大带宽的场合不能启用2倍速的高速模式，造成读取不够连贯的情况。

## 另见

  - [PCSX2](../Page/PCSX2.md "wikilink")，Windows与Linux平台上的PlayStation
    2模拟器
  - [PSX](../Page/PSX.md "wikilink")，基于PlayStation 2的多功能媒体设备
  - [第六世代遊戲機歷史](../Page/第六世代遊戲機歷史.md "wikilink")

## 參考

[PlayStation_2](../Category/PlayStation_2.md "wikilink")
[Category:第六世代遊戲機](../Category/第六世代遊戲機.md "wikilink")
[Category:索尼遊戲機](../Category/索尼遊戲機.md "wikilink")
[Category:家用游戏机](../Category/家用游戏机.md "wikilink")
[Category:2000年面世的產品](../Category/2000年面世的產品.md "wikilink")
[Category:好設計獎](../Category/好設計獎.md "wikilink")

1.  <https://www.hollywoodreporter.com/news/sonys-playstation-2-reaches-150-99502>
2.  [寿命长达12年的PS2昨日正式停止销售](http://www.enet.com.cn/article/2012/1229/A20121229218635.shtml)

3.  [PS2傳奇正式落幕
    Sony宣布終止一切售後維修](https://game.udn.com/game/story/10453/3217376)
4.
5.
6.
7.  <http://www.ps2-chips.com/>