**木村KAELA**
是[日本](../Page/日本.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")、[模特兒和](../Page/模特兒.md "wikilink")[藝人](../Page/藝人.md "wikilink")，原名是（Kimura
KaelaRie）。[東京都立向丘高等學校畢業](../Page/東京都立向丘高等學校.md "wikilink")，原所屬唱片公司为[哥倫比亞音樂](../Page/哥倫比亞音樂.md "wikilink")（Columbia
Music Entertainment，契约至2013年终止），现所属唱片公司为VICTOR，而所屬的經紀公司是[Sony Music
Artists](../Page/Sony_Music_Artists.md "wikilink")。常與各式各樣的音樂人合作，音樂及個人特色強烈顯明。在電視現場及演唱會皆與合作已久的音樂夥伴（專屬樂團成員）演出，偶爾自彈[吉他](../Page/吉他.md "wikilink")。

## 來歷

生於[東京都](../Page/東京都.md "wikilink")[足立區](../Page/足立區.md "wikilink")，為[英日混血兒](../Page/英國.md "wikilink")，父親是[英國人](../Page/英國人.md "wikilink")，母親是[日本人](../Page/日本人.md "wikilink")，從母姓「木村」。最初，由於亮麗的外型，在小學六年級時就擔任了雜誌CUTiE的讀者模特兒，到16歲高中那年又被選中而成了[集英社旗下雜誌](../Page/集英社.md "wikilink")[SEVENTEEN的專屬模特兒](../Page/SEVENTEEN_\(雜誌\).md "wikilink")。

因為從小最大的夢想就是音樂、唱歌及當歌手，高中時積極地組樂團，畢業時選擇放棄原本要念的大學而努力往音樂路走去。在18歲那年被Saku
Saku節目邀請成為節目主持人一員，歌手之路也就從此慢慢茁壯。

2004年5月10日（6月在日本正式發行）發行第一張單曲，[Level42即登上公信榜第](../Page/Level42.md "wikilink")14名。同年12月8日發行第一張專輯[KAELA](../Page/KAELA.md "wikilink")，更在首週就登上Oricon公信榜第8名（最高名次第7名）。而2005年3月30日發行的第三張單曲[Real
life Real
heart](../Page/Real_life_Real_heart.md "wikilink")，首週即登上公信榜第3名，自此以後她開始在日本成名。而2006年3月8日發行的第二張專輯[Circle首週即登上Oricon亞軍](../Page/Circle.md "wikilink")。

Kaela終於在2007年2月7日所發行的第三張專輯Scratch在首週以近20萬的銷量登上冠軍，而且蟬聯兩星期的冠軍，是她首次進入日本[Oricon公信榜第一名的專輯](../Page/Oricon公信榜.md "wikilink")\[1\]。

而[2007年4月份開始展開Scratch巡迴演唱會](../Page/2007年4月.md "wikilink")，並且再追加五場大型演唱會，在6月15日首次踏上[日本武道館的舞台舉辦個人大型演唱會](../Page/日本武道館.md "wikilink")。2007年3月17日的「MUSIC
VIDEO AWARDS 07」頒獎典禮中以Magic Music得了最佳POP Video獎「BEST POP
VIDEO」。[2008年7月發行的為](../Page/2008年7月.md "wikilink")[Idoling第三張專輯告白首度自行填詞](../Page/IDOLING.md "wikilink")。2009年12月31日首次在[紅白歌唱大賽亮相](../Page/NHK紅白歌合戰.md "wikilink")。

2010年6月1日，宣佈已懷孕五個月，並與男演員[瑛太結婚](../Page/瑛太.md "wikilink")。

2010年10月28日，順利產下兒子。

2011年第15屆Space Shower Music Video Awards音樂錄影帶頒獎，獲得BEST ARTIST獎和BEST
FEMALE ARTIST獎。

2013年宣布于出道九周年之际移籍至VICTOR开设专属厂牌ELA，并将于10月30日发行翻唱专辑ROCK。并同时宣布再次怀孕。

2013年10月7日，順利產下女兒。

## 名稱由來

本身為英日混血的Kaela從母姓，カエラ（Kaela）之名是身為英國人的父親依照傳統而用祖母的名字字尾「La」來取名，所以Kaela一字在日文本身並無意義，屬於外來字，但在[希伯來文裡卻有](../Page/希伯來文.md "wikilink")「最愛」之意。Kaela曾表示：「爸媽說當初並不曉得『KAELA』在希伯來文中有『最愛』之意，後來知道時也驚訝了一下。」

代理公司原先想替她取「木村愛」為藝名，但因為Kaela堅持希望能用自己的本名，而代理唱片公司為了尊重本人的意見，所以就稱呼為「木村KAELA」。

## 作品

### 單曲

1.  **[Level42](../Page/Level42.md "wikilink")**（2004年5月10日發行）
      - 在神奈川縣限定獨立發行每張日幣390的單曲共390張，開賣3分鐘即搶購一空。同年6月23日正式發行。
2.  **[happiness\!\!\!](../Page/happiness!!!.md "wikilink")**（2004年10月27日發行）
3.  **[Real Life Real
    Heart](../Page/Real_Life_Real_Heart.md "wikilink")**（）（2005年3月30日發行）
4.  **[BEAT](../Page/BEAT.md "wikilink")**（2005年10月5日發行）
5.  **[You](../Page/You_\(木村KAELA\).md "wikilink")**（2006年1月18日發行）
6.  **[Magic Music](../Page/Magic_Music.md "wikilink")**（2006年6月28日發行）
7.  **[TREE
    CLIMBERS](../Page/TREE_CLIMBERS.md "wikilink")**（2006年9月6日發行）
8.  **[Snowdome](../Page/Snowdome.md "wikilink")**（中譯：雪花球）（2007年1月17日發行）
9.  **[Samantha](../Page/Samantha.md "wikilink")**（中譯：珊曼莎）（2007年7月18日發行）
10. **[Yellow](../Page/Yellow_\(木村KAELA單曲\).md "wikilink")**（2007年10月24日發行）
11. **[Jasper](../Page/Jasper.md "wikilink")**（2008年2月6日發行）
12. **[Moustache/memories](../Page/Moustache/memories.md "wikilink")**（中譯：俏鬍子）（2008年9月10日發行）
13. **[在哪裡](../Page/在哪裡.md "wikilink")**（2009年1月28日發行）
14. **[BANZAI](../Page/BANZAI_\(木村KAELA單曲\).md "wikilink")**（2009年5月8日發行）
15. **[Ring a Ding
    Dong](../Page/Ring_a_Ding_Dong.md "wikilink")**（2010年6月9日發行）
16. **[A winter fairy is melting a
    snowman](../Page/A_winter_fairy_is_melting_a_snowman.md "wikilink")**（2010年12月8日發行）
17. **[喜怒哀樂 plus 愛](../Page/喜怒哀樂_plus_愛.md "wikilink")**（2011年8月3日發行）
18. **[Mamireru](../Page/Mamireru.md "wikilink")**(マミレル)（2012年5月16日發行）
19. **[Sun shower](../Page/Sun_shower.md "wikilink")**（2012年10月24日發行）
20. **[OLE\!OH\!](../Page/OLE!OH!.md "wikilink")**（2014年7月9日發行）
21. **[TODAY IS A NEW
    DAY](../Page/TODAY_IS_A_NEW_DAY.md "wikilink")**（2014年10月22日發行）
22. **[EGG](../Page/EGG.md "wikilink")**（2015年9月2日發行）

### 單曲銷量

<table>
<thead>
<tr class="header">
<th><p>Release</p></th>
<th><p>Title</p></th>
<th><p>Notes</p></th>
<th><p>Chart positions</p></th>
<th><p>Oricon<br />
sales<br />
[2]</p></th>
<th><p>Album</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Oricon.md" title="wikilink">Oricon</a> Singles Charts<br />
[3]</p></td>
<td><p><em><a href="../Page/告示牌_(雜誌).md" title="wikilink">Billboard</a></em> <a href="../Page/Japan_Hot_100.md" title="wikilink">Japan Hot 100</a>†<br />
[4]</p></td>
<td><p><a href="../Page/RIAJ_Digital_Track_Chart.md" title="wikilink">RIAJ</a> digital tracks†<br />
[5]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>"<a href="../Page/Level_42_(song).md" title="wikilink">Level 42</a>"</p></td>
<td><p><small>Initially released independently.</small></p></td>
<td><p>14</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>"[[Happiness</p></td>
<td><p>!|Happiness</p></td>
<td><p>!]]"</p></td>
<td></td>
<td><p>11</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p>"<a href="../Page/リルラ_リルハ.md" title="wikilink">リルラ リルハ</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>"<a href="../Page/Beat_(song).md" title="wikilink">Beat</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>470,000</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>"<a href="../Page/You_(Kaela_Kimura_song).md" title="wikilink">You</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>"Circle (Kit Kat Edition)"</p></td>
<td><p><small>Sold in convenience stores with Kit Kats.</small></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/Magic_Music.md" title="wikilink">Magic Music</a>"</p></td>
<td></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>660,000</p></td>
</tr>
<tr class="odd">
<td><p>"<a href="../Page/Tree_Climbers.md" title="wikilink">Tree Climbers</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>500,000</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p>"<a href="../Page/Snowdome_(song).md" title="wikilink">Snowdome</a>"</p></td>
<td></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p>1*</p></td>
</tr>
<tr class="odd">
<td><p>"<a href="../Page/Samantha_(Kaela_Kimura_song).md" title="wikilink">Samantha</a>"</p></td>
<td></td>
<td><p>2</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>400,000</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/Yellow_(Kaela_Kimura_song).md" title="wikilink">Yellow</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>450,000</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p>"<a href="../Page/Jasper_(song).md" title="wikilink">Jasper</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/マスタッシュ/memories.md" title="wikilink">マスタッシュ/memories</a>"</p></td>
<td></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>580,000</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>"<a href="../Page/どこ.md" title="wikilink">どこ</a>"</p></td>
<td></td>
<td><p>2</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/Banzai_(Kaela_Kimura_song).md" title="wikilink">Banzai</a>"</p></td>
<td></td>
<td><p>2</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>480,000</p></td>
</tr>
<tr class="odd">
<td><p>"<a href="../Page/Butterfly_(Kaela_Kimura_song).md" title="wikilink">Butterfly</a>"</p></td>
<td><p><small>Digital download, RIAJ #1 for two weeks.</small></p></td>
<td><p>—</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/ホットペッパーの唄.md" title="wikilink">ホットペッパーの唄</a>"</p></td>
<td><p><small>Digital download.</small></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>"[[You_Bet</p></td>
<td><p>|You Bet</p></td>
<td><p>]]"</p></td>
<td><p><small>Radio single.</small></p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><small>Digital download.</small></p></td>
<td><p>—</p></td>
<td><p>48</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>feat. <a href="../Page/Tamio_Okuda.md" title="wikilink">Tamio Okuda</a></p></td>
<td><p><small>Digital download, Tamio Okuda cover.</small></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/Ring_a_Ding_Dong.md" title="wikilink">Ring a Ding Dong</a>"</p></td>
<td></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>1,010,000</p></td>
</tr>
<tr class="odd">
<td><p>"<a href="../Page/Deep_Beep.md" title="wikilink">Deep Beep</a>"</p></td>
<td><p><small>Digital download.</small></p></td>
<td><p>—</p></td>
<td><p>23</p></td>
<td><p>23</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>"<a href="../Page/A_Winter_Fairy_Is_Melting_a_Snowman.md" title="wikilink">A Winter Fairy Is Melting a Snowman</a>"</p></td>
<td></td>
<td><p>3</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>850,000</p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>"<a href="../Page/喜怒哀楽_plus_愛.md" title="wikilink">喜怒哀楽 plus 愛</a>"</p></td>
<td></td>
<td><p>8</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><small>* charted on monthly Chaku-uta <a href="../Page/RIAJ_Digital_Track_Chart.md" title="wikilink">Reco-kyō Chart</a>.<br />
†Japan Hot 100 established February 2008, RIAJ Digital Track Chart established April 2009.</small></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 特別單曲

1.  **circle**（KIT KAT Edition）（2006年2月20日發行）
      - 與Kit-Kat廠商合作，初回限定發售附贈此單曲的Kit-Kat巧克力。

#### 配信限定單曲

| 日期         | 單曲                                                             |                                                           |
| ---------- | -------------------------------------------------------------- | --------------------------------------------------------- |
| 2009年6月1日  | **[Butterfly](../Page/Butterfly_\(木村KAELA單曲\).md "wikilink")** | 配信限定單曲                                                    |
| 2009年7月1日  | ****                                                           | 手機片段下載期間限定配信發售                                            |
| 2010年2月19日 | **息子（僕音ver）**                                                  | 木村KAELA with [奥田民生](../Page/奥田民生.md "wikilink")「手機全曲下載」配信 |
| 2010年8月11日 | **[deep beep](../Page/deep_beep.md "wikilink")**               | 配信限定單曲                                                    |

### 專輯

1.  **[KAELA](../Page/KAELA.md "wikilink")**（2004年12月8日發行）
2.  **[Circle](../Page/Circle.md "wikilink")**（2006年3月8日發行）
3.  **[Scratch](../Page/Scratch.md "wikilink")**（中譯：憂傷塗鴉）（2007年2月7日發行）
4.  **[+1](../Page/+1.md "wikilink")**（2008年4月2日發行）
5.  **[HOCUS POCUS](../Page/HOCUS_POCUS.md "wikilink")**（2009年6月24日發行）
6.  **[8EIGHT8](../Page/8EIGHT8.md "wikilink")**（2011年10月12日發行）

### 專輯銷量

<table>
<thead>
<tr class="header">
<th><p>Year</p></th>
<th><p>Album Information</p></th>
<th><p>Chart positions<br />
[6]</p></th>
<th><p>Total sales<br />
[7]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004</p></td>
<td><p><strong><em><a href="../Page/Kaela.md" title="wikilink">Kaela</a></em></strong> <small></p>
<ul>
<li>Released: December 8, 2004</li>
<li>Label: <a href="../Page/Columbia_Music_Entertainment.md" title="wikilink">Columbia</a> (COCP-32989 COCP-32988)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>8</p></td>
<td><p>149,187</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><strong><em><a href="../Page/Circle_(Kaela_Kimura_album).md" title="wikilink">Circle</a></em></strong> <small></p>
<ul>
<li>Released: March 8, 2006</li>
<li>Label: Columbia (COCP-33521 COCP-33522)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>2</p></td>
<td><p>197,436</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><strong><em><a href="../Page/Scratch_(album).md" title="wikilink">Scratch</a></em></strong> <small></p>
<ul>
<li>Released: February 7, 2007</li>
<li>Label: Columbia (COZA-243/4 COCP-34093)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>1(連續2週)</p></td>
<td><p>359,590</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><strong><em><a href="../Page/+1_(album).md" title="wikilink">+1</a></em></strong> <small></p>
<ul>
<li>Released: April 2, 2008</li>
<li>Label: Columbia (COZA-301/2 COCP-34795)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>3</p></td>
<td><p>161,000</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><strong><em><a href="../Page/Hocus_Pocus_(Kaela_Kimura_album).md" title="wikilink">Hocus Pocus</a></em></strong> <small></p>
<ul>
<li>Released: June 24, 2009</li>
<li>Label: Columbia (COCP-35635 COZP-373/4)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>3</p></td>
<td><p>220,905</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><strong><em><a href="../Page/8Eight8.md" title="wikilink">8Eight8</a></em></strong> <small></p>
<ul>
<li>Released: October 12, 2011</li>
<li>Label: Columbia (COCP-36925 COZP-595/6)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>1</p></td>
<td><p>75,097</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><strong><em><a href="../Page/Sync.md" title="wikilink">Sync</a></em></strong> <small></p>
<ul>
<li>Released: December 19, 2012</li>
<li>Label: Columbia (COCP-37725 COZP-743/4)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>5</p></td>
<td><p>32,515</p></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><strong><em><a href="../Page/ROCK.md" title="wikilink">ROCK</a></em></strong> <small></p>
<ul>
<li>Released: October 30, 2013</li>
<li>Label: <a href="../Page/Victor_Entertainment.md" title="wikilink">Victor</a> (VICL-64050 VICL-64080 VIZL-601)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>10</p></td>
<td><p>19,127</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 精選專輯

1.  **[5years](../Page/5years.md "wikilink")**（2010年2月3日發行）
2.  **[10years](../Page/10years.md "wikilink")**（2014年6月25日發行）

### 精選專輯銷量

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>專輯名稱</p></th>
<th><p>ORICON週榜<br />
[8]</p></th>
<th><p>最終銷量<br />
[9]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004</p></td>
<td><p><strong><em><a href="../Page/5years.md" title="wikilink">5years</a></em></strong> <small></p>
<ul>
<li>Released: February 3, 2010</li>
<li>Label: <a href="../Page/Columbia_Music_Entertainment.md" title="wikilink">Columbia</a> (COCP-36004 36002/3)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>2</p></td>
<td><p>472,979</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><strong><em><a href="../Page/10years.md" title="wikilink">10years</a></em></strong> <small></p>
<ul>
<li>Released: June 25, 2014</li>
<li>Label: <a href="../Page/Columbia_Music_Entertainment.md" title="wikilink">Columbia</a> (COZP-933/4 COCP-38590)</li>
<li>Formats: <a href="../Page/Compact_disc.md" title="wikilink">CD</a>, <a href="../Page/Paid_download.md" title="wikilink">digital download</a></li>
</ul>
<p></small></p></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他參與

1.  JUDY AND MARY「JUDY AND MARY 15th Anniversary Tribute
    Album」（2009年3月28日發行）
      - 參予JAM出道15週年紀念致敬專輯，選擇改編翻唱自己最喜歡的歌「Happy?」 (第9曲目)。
2.  Hey\!Hey\!Alright（2009年1月14日發行）
      - Kaela與Schadaraparr合作搭檔，並以SCHADARAPARR＋木村KAELA組合名義，由SCHADARAPARR所屬唱片公司tearbridge
        records發行單曲。
3.  [君とどこまでも](https://web.archive.org/web/20080929201256/http://listen.jp/store/album_mstb103.htm)（MO'SOME
    TONEBENDER的Live限定發售單曲）（2008年6月26日發行）
      - Kaela與之合唱「君とどこまでも」
4.  山澤大洋 presents [「music
    tree」](http://columbia.jp/artist-info/taiyo/COCP-34572.html)（作曲人山澤大洋的作品專輯）（2008年1月23日發行）
      - 第2曲目「birdie」，Kaela演唱與DEPAPEPE演奏吉他的合作曲
5.  「[奥田民生](../Page/奥田民生.md "wikilink")・カバーズ／OKUDA TAMIO
    COVERS」（奥田民生出道20周年紀念盤）（2007年10月24日發行）
      - 第1曲目「マシマロ」
6.  Sadistic Mikaela Band「NARKISSOS」擔任主唱（2006年10月25日發行）
7.  LOVE for NANA ～Only 1 Tribute～（2005年3月16日發行）
      - 第3曲目Twinkle，也收入至real life real
        heart單曲和[Circle專輯](../Page/Circle.md "wikilink")
8.  LAST DAYS tribute to Mr.K（2006年4月5日發行）
      - 第4曲目Cloudy，與FOE合作，Kaela擔任作詞和演唱
9.  FOETUNES（2006年4月26日發行）
      - 第3曲目The End Of The Sun（跨刀獻聲）
10. [ATTENTION
    PLEASE](../Page/ATTENTION_PLEASE.md "wikilink")2006日劇原聲帶（2006年5月24日發行）
      - 第1曲目OH PRETTY WOMAN
11. [令人討厭的松子的一生電影原聲帶](../Page/令人討厭的松子的一生.md "wikilink")（2006年5月24日發行）
      - 第1曲目TRILL TRILL
        RECUR，也收入至[Circle專輯](../Page/Circle.md "wikilink")
12. No Reason Why（2007年1月24日）
      - 第2曲目No Reason Why featuring 木村KAELA
13. [「...of　newtypes」](http://www.indiesmusic.com/ItemView.aspx?id=11213)
    10組樂團的合輯（2005年10月12日）
      - Kaela高中時加入的獨立樂團ANIMO，參與了此專輯，收入兩首"Out of luck"和"Is my fate"
14. 「Custom Made 10.30」電影原聲帶（2005年10月26日）
      - 第19曲目Why are you so in a hurry?～父親的Melody

### 作詞提供

  - 「告白」
      - [IDOLING\!\!\!的](../Page/IDOLING.md "wikilink")「告白」單曲（2008年7月16日）1曲目
      - kaela首次提供他人作詞。
  - 「Mother goose」
      - 友坂理恵「トリドリ」專輯（2009年6月24日）第8曲目。

### 書籍

1.  《**Cheeky**》生活寫真集（2007年2月14日）
2.  《**COLLECTION**》Kaela個人第一本明星書（2008年5月14日發行，為一般市面上買的到的通常版）
      - 只限定郵購和特定網頁預定的通販限定版於同年同月29日限定發，特典為kaela本人肖像的布偶。

### DVD

1.  《**Saku Saku Ver.1**》（2005年4月28日）
2.  《**KAELA KIMURA 1st TOUR 2005 "4YOU"**》（2005年7月6日）
3.  《**Custom Made 10.30**》電影見習篇（2005年10月14日）
4.  《**Saku Saku Ver.2**》Vin的復習（2006年3月24日）
5.  《**Custom Made 10.30**》電影（2006年3月24日）
6.  《**LIVE Scratch TOUR@武道館**》（2007年9月19日）
7.  《**BEST VIDEO1**》Kaela歌手出道四年來的MV集大成（2008年7月23日）
8.  《**GO\!5\!KAELAND**》出道五週年紀念演唱會（2009年11月4日）

## 個人出演

### 廣播

  - J-WAVE **『[Oh\! My
    Radio](http://www.j-wave.co.jp/original/ohmyradio/)』**節目（2005年至今）
    1.  擔任主持，現場撥出時間為每星期2晚上11點\~凌晨1點（以台灣時區來算）。
        自2007年4月開始，全世界皆能經由 [Brandnew
        J](https://web.archive.org/web/20080604150336/http://www.j-wavemusic.com/)
        網站線上收聽。
    2.  通稱カエラジ（kaelaji），「カエラのラジオ（Kaela的radio）」的簡稱。

### 電視

  - **『[新堂本兄弟](../Page/新堂本兄弟.md "wikilink")』**，曾於2005年擔任節目班底。
  - **『saku saku』**音樂節目，曾於2003年3月31日到2006年3月31日擔任主持。

### 戲劇

  - 日劇**『[ATTENTION
    PLEASE](../Page/ATTENTION_PLEASE.md "wikilink")』**（2006年），演唱主題曲Oh
    Pretty Woman，也在劇中客串演自己。
  - 日劇**『[櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")**真人版』，客串演出「未來長大的小丸子」。
      -
        註：富士電台為紀念櫻桃小丸子15週年，特別拍攝的真人版劇場。
  - 日劇**『[都市怪談之女](../Page/都市怪談之女.md "wikilink")**』(2012年)，客串第六集，演唱主題曲マミレル，也在劇中客串演自己。

### 電影

  - **『[PACO與魔法繪本](../Page/PACO與魔法繪本.md "wikilink")（パコと魔法の絵本）』**（2008年9月13日），
    以繃帶纏繞全身的姿態客串演出並演唱插曲「A Song For Paco」，且本人的歌曲memorise為其電影主題曲。
  - **『[令人討厭的松子的一生](../Page/令人討厭的松子的一生.md "wikilink")』**（2006年），客串演出電視牆上的大明星，
    自身的歌曲「[TRILL TRILL RECUR](../Page/Circle.md "wikilink")」為其插曲。
  - **『Custom made 10.30』**（2005年），首次擔任女主角（飾演小林Manamo）。
      -
        註：這是一部高中生與音樂的青春物語電影。

### 廣告

  - Loto製藥「Water lip・Sesero」（2004年10月－）
  - 富士重工業汽車
      - Suparu・R1（2004年12月－）（與梶原善共演）
      - Suparu・R2（2005年6月－）
  - Vodafone日本法人（現・SoftBank mobile）「Vodafone 3G（現・SoftBank
    3G）」（2005年2月下旬-6月）
  - [森永乳業](../Page/森永乳業.md "wikilink")「Eskimo Pino」（2005年4月－）
  - [任天堂](../Page/任天堂.md "wikilink")
      - 馬力歐博士（2005年4月－6月）
      - Game Boy micro（2005年9月－）
  - Parco（2005年9月－）
  - 麒麟啤酒「rugger」（2006年2月－）
  - 東京Modo學園（2006年－）
  - [東芝](../Page/東芝.md "wikilink")「gigabeat」（2006年－）
  - KDDI「Hikari one」（與木梨憲武演出，2006年－）
      - 「得意的事不同篇」
      - 「哪個好呢？篇」
      - 「animationにしない？篇」
      - 「Look\!（1）篇」
      - 「Look\!（2）篇」
  - 東日本旅客鐵路|JR東日本「JRSKISKI『FUN～雪上娛樂篇』」（2006年12月－）
  - Pokka corporation「Chelate Lemon」（2008年4月1日－）
  - 日產汽車「NOTE」マイケル&ステファン篇（擔任小男孩角色的配音，2008年10月－）
  - マンダム（Mandom）「LUCIDO-L」（2008年9月－）
  - 株式會社KUREHA「NEW KURELIFE ～『變身成大人』篇」（2009年1月21日－）

## 演唱會上的樂團成員

(專屬樂團成員)

  - **鼓手**：柏昌隆史（暱稱為Cyacy，為[toe及REACH的鼓手](../Page/Toe_\(樂團\).md "wikilink")）
  - **貝斯**：村上洋一郎，通稱4106（暱稱為Yochan，為SCAFULL KING的貝斯手）
  - **吉他**:Masasucks（為FULLSCRATCH的吉他手）、
      -
        渡邊 忍（暱稱為shinoppi，為ASPARAGUS的主唱兼吉他手）、
        會田茂一（暱稱為Aigon，為FOE及EL-MALO的成員之一）
  - **鍵盤**：堀江博久（為NEIL\&IRAIZA的鍵盤手）、
      -
        伊藤寬（為WACK WACK RHYTHM BAND的一員）
        中村圭作（暱稱為Wassyoi/Terry，為kowloon的鍵盤手）

## LIVE

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

1st Tour 2005 “4YOU”

</div>

<div class="NavContent" style="text-align: left;">

  - 2005.04.03　東京・[SHIBUYA-AX](../Page/SHIBUYA-AX.md "wikilink")
  - 2005.04.04　東京・SHIBUYA-AX
  - 2005.04.10　福岡・DRUM LOGOS
  - 2005.04.11　大阪・[BIG CAT](../Page/BIG_CAT.md "wikilink")

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

「BEAT」發片個人演唱會

</div>

<div class="NavContent" style="text-align: left;">

  - 2005.10.21　横濱・[横濱BLITZ](../Page/横濱BLITZ.md "wikilink")
  - 2005.10.28　東京・SHIBUYA-AX

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

Live Tour 2006“Circle”

</div>

<div class="NavContent" style="text-align: left;">

  - 2006.04.02　福岡・[Zepp
    Fukuoka](../Page/Zepp#Zepp_Fukuoka.md "wikilink")
  - 2006.04.03　廣島・[廣島CLUB
    QUATTRO](../Page/CLUB_QUATTRO#廣島CLUB_QUATTRO.md "wikilink")
  - 2006.04.08　大阪・[Zepp Osaka](../Page/Zepp#Zepp_Osaka.md "wikilink")
  - 2006.04.09　名古屋・[Zepp Nagoya](../Page/Zepp#Zepp_Nagoya.md "wikilink")
  - 2006.04.14　札幌・[Zepp
    Sapporo](../Page/Zepp#Zepp_Sapporo.md "wikilink")
  - 2006.04.16　仙台・[Zepp Sendai](../Page/Zepp#Zepp_Sendai.md "wikilink")
  - 2006.04.23　東京・[Zepp Tokyo](../Page/Zepp#Zepp_Tokyo.md "wikilink")
  - 2006.05.05　東京・[日比谷野外大音樂堂](../Page/日比谷野外音樂堂#大音楽堂.md "wikilink")

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

Live Tour 2006 秋～BaRa 木村 de BOB tour～

</div>

<div class="NavContent" style="text-align: left;">

  - 2006.10.04　岩手・盛岡club change WAVE
  - 2006.10.05　秋田・CLUB SWINDLE
  - 2006.10.07　新潟・[LOTS](../Page/NIIGATA_LOTS.md "wikilink")
  - 2006.10.10　静岡・窓枠
  - 2006.10.12　京都・MUSE
  - 2006.10.13　兵庫・神戶WYNTER LAND
  - 2006.10.16　長崎・DRUM Be-7
  - 2006.10.17　熊本・BATTLE STAGE
  - 2006.10.21　石川・金沢EIGHT HALL
  - 2006.10.23　東京・LIQUIDROOM
  - 2006.10.25　神奈川・[CLUB CITTA'](../Page/CLUB_CITTA'.md "wikilink")

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

LIVE TOUR 2007「Scratch」～Agattemasutteba TOUR\~

</div>

<div class="NavContent" style="text-align: left;">

  - 2007.04.10　横濱・横濱BLITZ
  - 2007.04.13　熊本・BATTLE STAGE
  - 2007.04.14　福岡・Zepp Fukuoka
  - 2007.04.16　鹿兒島・CAPARVO HALL
  - 2007.04.20　岐阜・Club-G
  - 2007.04.21　兵庫・神戶WYNTER LAND
  - 2007.04.24　石川・金澤EIGHT HALL
  - 2007.04.26　新潟・LOTS
  - 2007.04.27　新潟・LOTS
  - 2007.04.29　札幌・Zepp Sapporo
  - 2007.05.05　長野・CLUB JUNK BOX
  - 2007.05.07　愛知・豊橋LAHAINA
  - 2007.05.09　廣島・廣島CLUB QUATTRO
  - 2007.05.10　岡山・CRAZYMAMA KINGDOM
  - 2007.05.12　高知・BAY5SQUARE
  - 2007.05.13　香川・高松 Olive Hall
  - 2007.05.17　福島・郡山Hip Shot Japan
  - 2007.05.22　群馬・高崎Club FLEEZ
  - 2007.05.24　青森・青森QUARTER
  - 2007.05.25　仙台・Zepp Sendai
  - 2007.06.12　大阪・[Festival Hall](../Page/Festival_Hall.md "wikilink")
  - 2007.06.13　名古屋・[名古屋市民會館](../Page/名古屋市民會館.md "wikilink")・大Hall
  - 2007.06.15　東京・[日本武道館](../Page/日本武道館.md "wikilink")
  - 2007.06.25　沖繩・NAMURA Hall

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

Live 2007～Gepp Tour～

</div>

<div class="NavContent" style="text-align: left;">

  - 2007.10.08　札幌・Zepp Sapporo
  - 2007.10.14　福岡・Zepp Fukuoka
  - 2007.10.15　名古屋・Zepp Nagoya
  - 2007.10.17　大阪・Zepp Osaka
  - 2007.10.19　仙台・Zepp Sendai
  - 2007.10.23　東京・Zepp Tokyo

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

KAELA企劃活動「女孩祭 男孩祭」

</div>

<div class="NavContent" style="text-align: left;">

  - 2007.12.19　東京・LIQIDROOM ebisu（男孩祭）
  - 2007.12.20　東京・LIQIDROOM ebisu（女孩祭・女性限定）

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

Chelate Lemon presents LIVE TOUR 2008「+1」

</div>

<div class="NavContent" style="text-align: left;">

  - 2008.05.09　千葉・[市川市文化會館](../Page/市川市文化會館.md "wikilink")・大Hall
  - 2008.05.14　新潟・[新潟縣民會館](../Page/新潟縣民會館.md "wikilink")・大廳
  - 2008.05.16　北海道・[北海道厚生年金會館](../Page/厚生年金會館.md "wikilink")
  - 2008.05.18　岩手・[岩手県民会館](../Page/岩手県民会館.md "wikilink")・大Hall
  - 2008.05.20　仙台・[仙台太陽廣場](../Page/仙台太陽廣場.md "wikilink")
  - 2008.05.24　埼玉・[大宮Sonic-City](../Page/大宮Sonic-City.md "wikilink")・大Hall
  - 2008.05.29　岡山・[倉敷市民會館](../Page/倉敷市民會館.md "wikilink")
  - 2008.05.30　廣島・廣島厚生年金會館
  - 2008.06.01　名古屋・[名古屋國際會議場](../Page/名古屋國際會議場.md "wikilink")・世紀廳
  - 2008.06.05　福岡・[福岡太陽宮](../Page/福岡太陽宮.md "wikilink")
  - 2008.06.06　鹿兒島・[鹿兒島市民文化Hall第一](../Page/鹿兒島市民文化Hall#第1Hall.md "wikilink")
  - 2008.06.10　大阪・Festival Hall
  - 2008.06.11　大阪・Festival Hall
  - 2008.06.13　香川・[香川縣縣民大廳](../Page/香川縣縣民大廳.md "wikilink")・Grand Hall
  - 2008.06.15　愛媛・[松山市民會館](../Page/松山市民會館.md "wikilink")・大廳
  - 2008.06.23　神奈川・[神奈川縣民大廳](../Page/神奈川縣民大廳.md "wikilink")・大廳
  - 2008.06.29　栃木・[宇都宮市文化會館](../Page/宇都宮市文化會館.md "wikilink")・大廳
  - 2008.07.02　静岡・[Actcity浜松](../Page/Actcity浜松.md "wikilink")・大廳
  - 2008.07.04　富山・[Aubade・Hall](../Page/Aubade・Hall.md "wikilink")
  - 2008.07.05　石川・石川厚生年金會館
  - 2008.07.09　東京・[NHK大廳](../Page/NHK大廳.md "wikilink")
  - 2008.07.10　東京・NHK大廳

</div>

</div>

<div class="NavFrame" style="clear: both; border:0;">

<div class="NavHead" style="text-align: left;">

LIVE HOUSE TOUR 2008 ～STARs TOURS～

</div>

<div class="NavContent" style="text-align: left;">

  - 2008年8月9日 東京都・赤坂BLITZ
  - 2008年8月10日 東京都・赤坂BLITZ
  - 2008年8月22日 福島縣・郡山Hip Shot Japan
  - 2008年8月28日 神奈川縣・横濱BLITZ
  - 2008年8月29日 神奈川縣・横濱BLITZ
  - 2008年9月4日 滋賀縣・滋賀U★STONE
  - 2008年9月6日 岐阜縣・岐阜club-G

</div>

</div>

## 參考文獻

## 外部連結

  - [哥倫比亞音樂](http://columbia.jp/artist-info/kaela/)
  - [Kaela個人官網](http://kaela-web.com/index.html)
  - [Kaela個人網誌](http://kaela.jugem.jp/)

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本流行音樂歌手](../Category/日本流行音樂歌手.md "wikilink")
[Category:日本搖滾歌手](../Category/日本搖滾歌手.md "wikilink")
[Category:日本女性模特兒](../Category/日本女性模特兒.md "wikilink")
[Category:21世纪歌手](../Category/21世纪歌手.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:英國裔日本人](../Category/英國裔日本人.md "wikilink")
[Category:Seventeen模特兒](../Category/Seventeen模特兒.md "wikilink")
[Category:日本古倫美亞旗下藝人](../Category/日本古倫美亞旗下藝人.md "wikilink")
[Category:勝利娛樂旗下藝人](../Category/勝利娛樂旗下藝人.md "wikilink")

1.  [木村KAELA人氣爆發
    首度封后](http://tw.news.yahoo.com/article/url/d/a/070214/35/ajy4.html)

2.
3.
4.

5.

6.

7.   (subscription only)

8.

9.   (subscription only)