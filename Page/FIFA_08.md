《**FIFA
08**》是[美國](../Page/美國.md "wikilink")[電子藝界旗下](../Page/電子藝界.md "wikilink")[FIFA系列的遊戲作品](../Page/FIFA系列.md "wikilink")，由[EA
Canada小组负责开发](../Page/EA_Canada.md "wikilink")。于2007年9月27日在欧洲和澳大利亚地区发行，于2007年10月在北美地区发行。在[PlayStation
3和](../Page/PlayStation_3.md "wikilink")[Xbox
360的版本裡加強了](../Page/Xbox_360.md "wikilink")[遊戲引擎的功能](../Page/遊戲引擎.md "wikilink")，例如更好的畫面以及不同的比賽評論。[Electronic
Arts稱呼它為](../Page/EA.md "wikilink")"next-generation（下一世代）"。在其他平台（包括
PC）的遊戲是使用舊的遊戲引擎。在[Nintendo
DS的版本裡](../Page/Nintendo_DS.md "wikilink")，由於平台本身的限制，遊戲收錄了較少的隊伍、場地、遊戲模式和球衣。
在下一世代的平台中，兩位球評是來自Sky Sports的[Martin
Tyler和](../Page/Martin_Tyler.md "wikilink")[Andy
Gray](../Page/Andy_Gray_\(footballer_born_1955\).md "wikilink")，但在其他平台中是由ITV
Sports的[Clive
Tyldesley和Gray擔任遊戲的球評](../Page/Clive_Tyldesley.md "wikilink")。

遊戲的標語是"Can You -{FIFA 08}-?"。\[1\]主題曲是[La
Rocca的專輯](../Page/La_Rocca.md "wikilink")*The Truth*裡的"Sketches
(20 Something Life)"。

## 聯盟和球隊

FIFA 08內有621支經過授權的隊伍，30個聯盟（包括27個*FIFA
07*已有的聯盟）,\[2\]以及超過15,000個球員。\[3\]FIFA
08收入所有主要的歐洲聯賽例如英格蘭的[英格蘭超級足球聯賽和](../Page/英格蘭超級足球聯賽.md "wikilink")[英格蘭足球聯賽](../Page/英格蘭足球聯賽.md "wikilink")，西班牙的[西班牙甲組足球聯賽](../Page/西班牙甲組足球聯賽.md "wikilink")，義大利的[義大利甲組足球聯賽和德國的](../Page/義大利甲組足球聯賽.md "wikilink")[德國甲組足球聯賽](../Page/德國甲組足球聯賽.md "wikilink")，以及來自其它地區的聯賽，例如美國的
[美國足球大聯盟](../Page/美國足球大聯盟.md "wikilink")，[A-League](../Page/A-League.md "wikilink")（澳洲聯賽），巴西的[巴西甲組足球聯賽和韓國的](../Page/巴西甲組足球聯賽.md "wikilink")[K聯賽](../Page/K聯賽.md "wikilink")。

新加入FIFA
08的聯賽有[愛爾蘭足球超級聯賽](../Page/愛爾蘭足球超級聯賽.md "wikilink")，\[4\]澳洲[現代](../Page/現代汽車.md "wikilink")[A-League](../Page/A-League.md "wikilink")，\[5\]和捷克的[捷克足球甲級聯賽](../Page/捷克足球甲級聯賽.md "wikilink")。\[6\]遊戲還包括了"Rest
of World" 選項，內有 20
支並不屬於上述所有聯賽的隊伍。這些隊伍是[奧蘭多海盜](../Page/奧蘭多海盜.md "wikilink")，[Mamelodi](../Page/Mamelodi.md "wikilink")，[AC
Lugano](../Page/AC_Lugano.md "wikilink")，[FC La
Chaux-de-Fonds](../Page/FC_La_Chaux-de-Fonds.md "wikilink")，[AEK雅典](../Page/AEK雅典.md "wikilink")，[格丁尼亞阿爾卡](../Page/格丁尼亞阿爾卡足球俱樂部.md "wikilink")、[小保加](../Page/小保加.md "wikilink")、[洛桑](../Page/洛桑足球俱樂部.md "wikilink")，[福塔雷薩](../Page/福塔雷薩體育會.md "wikilink")，[Gornik
Leczna](../Page/Gornik_Leczna.md "wikilink")、[卡薩酋長](../Page/卡薩酋長.md "wikilink")、[奧林比亞高斯](../Page/奧林比亞高斯足球會.md "wikilink")，[什切青波貢](../Page/什切青波貢足球俱樂部.md "wikilink")，[華沙波蘭人](../Page/華沙波蘭人足球俱樂部.md "wikilink")，[邦迪比達](../Page/邦迪比達.md "wikilink")，[河床](../Page/河床足球俱樂部.md "wikilink")，[聖十字](../Page/聖十字足球會.md "wikilink")，[聖卡坦奴](../Page/聖卡坦奴.md "wikilink")，[帕奧克](../Page/帕奧克足球俱樂部.md "wikilink")，[克拉科夫](../Page/克拉科夫.md "wikilink")

在遊戲所收錄的621支隊伍裡， 有29支隊伍未經過授權，其中13支是國家隊。
其它16之為俱樂部隊伍。值得注意的是[荷蘭隊](../Page/荷蘭國家足球隊.md "wikilink")（在遊戲裡是以
*Holland* 為隊名）也沒有經過授權，球員名單內的球員皆使用假名。然而，遊戲裡的 *International Selection*
功能使得玩家可以手動的將真實的國家隊球員名單取代這些虛構的球員。

## 新功能

FIFA 08與[FIFA 07](../Page/FIFA_07.md "wikilink") 相比，它新加入了 "Co-op
mode"。玩家在此模式裡只能操作一名球員。 \[7\]

## 球場

*FIFA 08* 收錄了許多經過授權的比賽場地，包括FIFA 08 所收錄的聯盟的場地和國家隊使用的場地。

### [Benelux.gif](https://zh.wikipedia.org/wiki/File:Benelux.gif "fig:Benelux.gif") 比荷盧

  - [阿姆斯特丹球場](../Page/阿姆斯特丹球場.md "wikilink")（[阿賈克斯](../Page/阿賈克斯.md "wikilink")）

  - （[安德莱赫特](../Page/安德莱赫特足球俱乐部.md "wikilink")）

###  英格蘭

  - [晏菲路球場](../Page/晏菲路球場.md "wikilink")（[利物浦](../Page/利物浦足球俱樂部.md "wikilink")）
  - [酋長球場](../Page/酋長球場.md "wikilink")（[阿仙奴](../Page/阿仙奴.md "wikilink")）
  - [高貝利球場](../Page/高貝利球場.md "wikilink")²（前阿仙奴主場）
  - [聖占士公園球場](../Page/聖占士公園球場.md "wikilink")（[纽卡斯尔联](../Page/纽卡斯尔联足球俱乐部.md "wikilink")）
  - [奧脫福球場](../Page/奧脫福球場.md "wikilink")（[曼聯](../Page/曼聯.md "wikilink")）<sup>4</sup>
  - [史丹福橋球場](../Page/史丹福橋球場.md "wikilink")（[車路士](../Page/車路士.md "wikilink")）
  - [温布利球场](../Page/温布利球场.md "wikilink")（[英格蘭國家足球隊](../Page/英格蘭國家足球隊.md "wikilink")）
  - [白鹿徑球場](../Page/白鹿徑球場.md "wikilink")（[托特納姆熱刺](../Page/托特納姆熱刺.md "wikilink")）<sup>1,5</sup>

###  法國

  - [王子公園體育場](../Page/王子公園體育場.md "wikilink")（[巴黎-{zh-hans:圣日耳曼;zh-hk:聖日耳門;zh-tw:聖日耳曼;}-及](../Page/巴黎聖日爾曼.md "wikilink")[法國國家足球隊](../Page/法國國家足球隊.md "wikilink")）
  - [熱爾蘭球場](../Page/熱爾蘭球場.md "wikilink")（[里昂](../Page/里昂足球俱樂部.md "wikilink")）
  - [韋洛德羅姆球場](../Page/韋洛德羅姆球場.md "wikilink")（[馬賽](../Page/馬賽足球俱樂部.md "wikilink")）
  - [費利克斯-波萊特球場](../Page/費利克斯-波萊特球場.md "wikilink")（[朗斯](../Page/朗斯足球會.md "wikilink")）

###  德國

  - [安聯球場](../Page/安聯球場.md "wikilink")（[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")，[1860慕尼黑](../Page/1860慕尼黑.md "wikilink")）³
  - [-{zh-hans:戈特利布·戴姆勒体育场;zh-hk:葛迪比達馬球場;zh-tw:戈特利布·戴姆勒體育場;}-](../Page/梅赛德斯-奔驰竞技场.md "wikilink")<sup>1</sup>（[斯圖加特](../Page/斯圖加特足球俱樂部.md "wikilink")）
  - [AWD球場](../Page/AWD球場.md "wikilink")（[漢諾威96](../Page/漢諾威96.md "wikilink")）<sup>1</sup>
  - [拜耳竞技场](../Page/拜耳竞技场.md "wikilink")（[勒沃库森](../Page/勒沃库森足球俱乐部.md "wikilink")）
  - [商業銀行球場](../Page/商業銀行球場.md "wikilink")（[法蘭克福](../Page/法蘭克福足球俱樂部.md "wikilink")）<sup>1</sup>
  - [維爾廷斯球場](../Page/維爾廷斯球場.md "wikilink")（[沙爾克04](../Page/沙爾克04.md "wikilink")）
  - [北方银行竞技场](../Page/禾克斯公園球場.md "wikilink") - [AOL
    Arena](../Page/美國線上體育場.md "wikilink")（[漢堡](../Page/漢堡體育俱樂部.md "wikilink")）
  - [柏林奧林匹克體育場](../Page/柏林奧林匹克體育場.md "wikilink")（[柏林赫塔](../Page/柏林赫塔足球俱樂部.md "wikilink")）
  - [西格納伊度納公園](../Page/西格納伊度納公園.md "wikilink")（[多特蒙德](../Page/多特蒙德足球俱樂部.md "wikilink")）

###  意大利

  - [迪爾艾比球場](../Page/迪爾艾比球場.md "wikilink")（[尤文圖斯](../Page/尤文圖斯.md "wikilink")）
  - [羅馬奧林匹克體育場](../Page/羅馬奧林匹克體育場.md "wikilink")（[羅馬](../Page/羅馬足球俱樂部.md "wikilink")，[拉齊奧及](../Page/拉齊奧足球俱樂部.md "wikilink")[義大利國家足球隊](../Page/義大利國家足球隊.md "wikilink")）
  - [聖西路球場](../Page/聖西路球場.md "wikilink")（[AC米蘭](../Page/AC米蘭.md "wikilink")，[國際米蘭](../Page/國際米蘭.md "wikilink")）

###  墨西哥

  - [Estadio
    Jalisco](../Page/Estadio_Jalisco.md "wikilink")（[爪达拉哈拉](../Page/爪达拉哈拉体育俱乐部.md "wikilink")，[阿特拉斯](../Page/阿特拉斯足球俱乐部.md "wikilink")）<sup>1,5</sup>
  - [阿茲特克球場](../Page/阿茲特克球場.md "wikilink")（[亞美利加及](../Page/亞美利加足球俱樂部.md "wikilink")[墨西哥國家足球隊](../Page/墨西哥國家足球隊.md "wikilink")）

###  葡萄牙

  - [盧斯球場](../Page/盧斯球場.md "wikilink")（[-{zh-hans:本菲卡;zh-hk:賓菲加;zh-tw:本菲卡;}-](../Page/本菲卡.md "wikilink")）<sup>5</sup>;

  - （[博維斯塔](../Page/博維斯塔足球會.md "wikilink")）<sup>5</sup>

  - [火龍球場](../Page/火龍球場.md "wikilink")（[波圖及](../Page/波爾圖足球俱樂部.md "wikilink")[葡萄牙國家足球隊](../Page/葡萄牙國家足球隊.md "wikilink")）<sup>5</sup>

  - [阿爾瓦拉德球場](../Page/阿爾瓦拉德球場.md "wikilink")（[-{zh-hans:里斯本竞技;zh-hk:士砵亭;zh-tw:里斯本競技;}-](../Page/里斯本竞技.md "wikilink")）<sup>5</sup>

###  南韓

  - [大邱體育中心](../Page/大邱體育場.md "wikilink")（[大邱FC](../Page/大邱FC.md "wikilink")）
  - [首爾世界盃球場](../Page/首爾世界盃球場.md "wikilink")（[FC首爾及](../Page/FC首爾.md "wikilink")[南韓國家足球隊](../Page/南韓國家足球隊.md "wikilink")）

###  西班牙

  - [馬斯泰拿球場](../Page/馬斯泰拿球場.md "wikilink")（[華倫西亞足球俱樂部及](../Page/華倫西亞足球俱樂部.md "wikilink")[西班牙國家足球隊](../Page/西班牙國家足球隊.md "wikilink")）
  - [卡尔德隆球场](../Page/比森特·卡尔德隆球场.md "wikilink")（[馬德里競技](../Page/馬德里競技.md "wikilink")）
  - [魯營球場](../Page/魯營球場.md "wikilink")（[巴塞隆納](../Page/巴塞隆納足球俱樂部.md "wikilink")）
  - [-{zh-cn:伯纳乌球场;zh-hk:班拿貝球場;zh-tw:貝納烏球場;}-](../Page/伯纳乌球场.md "wikilink")（[皇家馬德里](../Page/皇家馬德里.md "wikilink")）

###  美國

  - [家得寶中心球場](../Page/家得寶中心球場.md "wikilink")<sup>1,5</sup>（[洛杉磯銀河](../Page/洛杉磯銀河.md "wikilink")、[美國芝華士及](../Page/美國芝華士.md "wikilink")[美國國家足球隊](../Page/美國國家足球隊.md "wikilink")）

###  威爾斯

  - [千禧球場](../Page/千禧球場.md "wikilink")（[威爾斯足球代表隊](../Page/威爾斯足球代表隊.md "wikilink")）

### 一般球場

  - Div1 Euro Style

  - Div2 Euro Style

  - Div3 Euro Style

  - Div1 UK Style

  - Div2 UK Style

  - Div3 UK Style

  - Generic Modern

  - Modern Euro

  - Modern South America

  - [Olympic_flag.svg](https://zh.wikipedia.org/wiki/File:Olympic_flag.svg "fig:Olympic_flag.svg")
    Olympic Style

  - Closed Square Style

  - Open Square Style

  - Oval Style

<sup>1</sup> - 首次於FIFA系列登場
² - 在Fan Shop購買可使用，不適用[XBOX 360](../Page/XBOX_360.md "wikilink")
³ - 除了Xbox 360以外，首次登場。
<sup>4</sup> - 奧脫福球場不會有更改，除了Xbox360及PlayStation 3版本。
<sup>5</sup> - 不會在[Xbox
360及](../Page/Xbox_360.md "wikilink")[PlayStation
3版本出現](../Page/PlayStation_3.md "wikilink")，除非你編輯PC的資料庫，球場可以解封。

</div>

## 原聲帶

EA Sports在2007年9月11日公佈了原聲帶。\[8\]

<table>
<thead>
<tr class="header">
<th><p>作家</p></th>
<th><p>歌曲</p></th>
<th><p>專輯</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[[</p></td>
<td><p>!|</p></td>
<td><p>!]]</p></td>
<td><p>All My Heroes Are Weirdos</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Fall Into Place</p></td>
<td><p>The Dreamer Evasive</p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Art_Brut.md" title="wikilink">Art Brut</a></p></td>
<td><p>Direct Hit</p></td>
<td><p>It's A Bit Complicated</p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Paces</p></td>
<td><p>Oye</p></td>
<td><p>哥倫比亞</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>The Core</p></td>
<td><p>Surprising Twists</p></td>
<td><p>法國</p></td>
</tr>
<tr class="even">
<td><p>feat </p></td>
<td><p>What Planet You On?</p></td>
<td><p>What Planet You On?</p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Solta O Frango</p></td>
<td><p>With Lasers</p></td>
<td><p>巴西</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Camp_(band).md" title="wikilink">CAMP</a></p></td>
<td><p>From Extremely Far Away</p></td>
<td><p>Talking Cure</p></td>
<td><p>瑞士</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Human</p></td>
<td><p>All Things To All People</p></td>
<td><p>丹麥</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Malemolência</p></td>
<td><p>CéU</p></td>
<td><p>巴西</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Cheb_i_Sabbah.md" title="wikilink">Cheb i Sabbah</a></p></td>
<td><p>Toura Toura: Nav Deep Remix</p></td>
<td><p>La Ghriba: La Kahena Remix</p></td>
<td><p>法國／阿爾及利亞</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Cansei_de_Ser_Sexy.md" title="wikilink">Cansei de Ser Sexy</a></p></td>
<td><p>Off The Hook</p></td>
<td><p>Cansei De Ser Sexy</p></td>
<td><p>巴西</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Fa-Fa-Fa</p></td>
<td><p>Datarock Datarock</p></td>
<td><p>挪威</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Pogo</p></td>
<td><p>Idealism</p></td>
<td><p>德國</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>We Might Fall Apart</p></td>
<td><p>First Aid Kit</p></td>
<td><p>芬蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Do Ya</p></td>
<td><p>Follow The City Lights</p></td>
<td><p>西班牙</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Into The Light</p></td>
<td><p><a href="../Page/Strange_Constellations.md" title="wikilink">Strange Constellations</a></p></td>
<td><p>挪威</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Que Lloren</p></td>
<td><p>Sentimiento</p></td>
<td><p>波多黎各</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Junkie_XL.md" title="wikilink">Junkie XL</a></p></td>
<td><p>Clash</p></td>
<td><p>Felt EP</p></td>
<td><p>荷蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Unglued</p></td>
<td><p>Jupiter One</p></td>
<td><p>美國</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Kenna.md" title="wikilink">Kenna</a></p></td>
<td><p>Out Of Control (State Of Emotion)</p></td>
<td><p>Make Sure They See My Face</p></td>
<td><p>埃塞羅比亞</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K-Os.md" title="wikilink">K-Os</a></p></td>
<td><p>Born To Run</p></td>
<td><p>Atlantis: Hymns for Disco</p></td>
<td><p>加拿大</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/La_Rocca.md" title="wikilink">La Rocca</a></p></td>
<td><p>Sketches (20 Something Life)</p></td>
<td><p>The Truth</p></td>
<td><p>愛爾蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Love Abuse</p></td>
<td><p>Animated People's Republic</p></td>
<td><p>挪威</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Madness.md" title="wikilink">Madness</a> feat. <a href="../Page/Sway.md" title="wikilink">Sway</a> and <a href="../Page/Baby_Blue.md" title="wikilink">Baby Blue</a></p></td>
<td><p>I'm Sorry</p></td>
<td><p>I'm Sorry</p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Here We Go (To The Next Stage)</p></td>
<td><p>Before They Find Out That We Did It</p></td>
<td><p>日本</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>The Unshockable</p></td>
<td><p><a href="../Page/Our_Earthly_Pleasure.md" title="wikilink">Our Earthly Pleasure</a></p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Melody_Club.md" title="wikilink">Melody Club</a></p></td>
<td><p>Fever Fever</p></td>
<td><p>Scream</p></td>
<td><p>瑞典</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>El Microfono</p></td>
<td><p>Piñata</p></td>
<td><p>墨西哥</p></td>
</tr>
<tr class="even">
<td><p>feat. </p></td>
<td><p>Silikon</p></td>
<td><p><a href="../Page/Hello_Mom!.md" title="wikilink">Hello Mom!</a></p></td>
<td><p>德國</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Noisettes.md" title="wikilink">Noisettes</a></p></td>
<td><p>Don't Give Up</p></td>
<td><p><a href="../Page/What&#39;s_The_Time_Mr._Wolf?.md" title="wikilink">What's The Time Mr. Wolf?</a></p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Don't Let Go</p></td>
<td><p>All Good Things</p></td>
<td><p>美國</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Young_Folks.md" title="wikilink">Young Folks</a></p></td>
<td><p>Writer's Block</p></td>
<td><p>瑞典</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Static</p></td>
<td><p>Static</p></td>
<td><p>意大利</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Robyn.md" title="wikilink">Robyn</a></p></td>
<td><p>Bum Like You</p></td>
<td><p>Robyn</p></td>
<td><p>瑞典</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Wake Up The Town</p></td>
<td><p>Book Of Changes</p></td>
<td><p>加納</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>You Will Find A Way</p></td>
<td><p>Santogold</p></td>
<td><p>美國</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>I Believe</p></td>
<td><p><a href="../Page/Attack_Decay_Sustain_Release.md" title="wikilink">Attack Decay Sustain Release</a></p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Superbus.md" title="wikilink">Superbus</a></p></td>
<td><p>Butterfly</p></td>
<td><p>Wow</p></td>
<td><p>法國</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Switches.md" title="wikilink">Switches</a></p></td>
<td><p>Drama Queen</p></td>
<td><p>Heart Tuned To D.E.A.D</p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/The_Automatic.md" title="wikilink">The Automatic</a></p></td>
<td><p>Monster</p></td>
<td><p>Not Accepted Anywhere</p></td>
<td><p>威爾斯</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Sly</p></td>
<td><p>Two Shoes</p></td>
<td><p>澳洲</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Goodbye Mr. A</p></td>
<td><p>The Trick To Life</p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Ali In The Jungle</p></td>
<td><p><a href="../Page/Narcissus_Road.md" title="wikilink">Narcissus Road</a></p></td>
<td><p>英格蘭</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>More</p></td>
<td><p>The Tellers</p></td>
<td><p>比利時</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Tigarah.md" title="wikilink">Tigarah</a></p></td>
<td><p>Culture, Color, Money, Beauty</p></td>
<td><p>Revolution</p></td>
<td><p>日本</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/特拉维斯.md" title="wikilink">特拉维斯</a></p></td>
<td><p>Closer</p></td>
<td><p>The Boy With No Name</p></td>
<td><p>蘇格蘭</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Afrique</p></td>
<td><p>Tumi And the Volume</p></td>
<td><p>南非</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Vassy.md" title="wikilink">Vassy</a></p></td>
<td><p>Wanna Fly</p></td>
<td><p>My Affection</p></td>
<td><p>澳洲</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Endlich Ein Grund Zur Panik</p></td>
<td><p><a href="../Page/Soundso.md" title="wikilink">Soundso</a></p></td>
<td><p>德國</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Were You Thinking Of Me?</p></td>
<td><p><a href="../Page/Splendid_Isolation.md" title="wikilink">Splendid Isolation</a></p></td>
<td><p>匈牙利</p></td>
</tr>
</tbody>
</table>

## 获奖情况

*FIFA 08*贏取及提名以下獎項：

  - BAFTA Nominations (2007 Video Game Awards)：最佳體育遊戲
  - IGN Awards (2007 Video Game Awards)：最佳體育模擬遊戲
  - IGN Awards (2007 Video Game Awards)：最迫真真實模擬遊戲
  - IGN Awards (2007 Video Game Awards)：年度最佳遊戲
  - IGN Awards (2007 Video Game Awards)：最佳體育遊戲玩法

## 参考来源

## 外部链接

  - [官方网站](https://web.archive.org/web/20080517203310/http://fifa08.ea.com/)

[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[2008](../Category/FIFA系列.md "wikilink") [Category:PlayStation
2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:Xbox 360游戏](../Category/Xbox_360游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:Wii游戏](../Category/Wii游戏.md "wikilink")
[Category:任天堂DS游戏](../Category/任天堂DS游戏.md "wikilink")
[Category:N-Gage服务游戏](../Category/N-Gage服务游戏.md "wikilink")

1.  [EA SPORTS - FIFA 08](http://fifa08.ea.com/ChooseTerritory.aspx)
2.  [*FIFA 08* First
    Look](http://ps3.ign.com/articles/793/793715p1.html) by ign.com
3.  [Master the skills required to play like a pro in EA's *FIFA 08* for
    Xbox 360 and Playstation
    3](http://www.fifa07.ea.com/news/?id=13184&lang=en&delta=1)  by [EA
    Sports](../Page/EA_Sports.md "wikilink"). [June
    5](../Page/June_5.md "wikilink") 2007
4.  [eircom League of Ireland to feature in *FIFA 08* around the
    world](http://www.fai.ie/index.php?option=com_content&task=view&id=2107&Itemid=9),
    by FAI.ie, [11 June](../Page/11_June.md "wikilink") 2007. Retrieved
    on [15 June](../Page/15_June.md "wikilink") 2007
5.  [Hyundai A-League to feature in *FIFA 08* video
    game](http://www.a-league.com.au/default.aspx?s=hal_newsdisplay&id=18867)
     by A-League.com.au, [20 August](../Page/20_August.md "wikilink")
    2007.
6.  [A-League will be in *FIFA
    08*](http://au.fourfourtwo.com/news/59393,aleague-will-be-in-fifa-08.aspx/)
    by au.fourfourtwo.com. [August 20](../Page/August_20.md "wikilink")
    2007
7.  <http://www.fifa08.com>
8.  [1](https://web.archive.org/web/20071118134613/http://fifa2008.wordpress.com/2007/09/11/fifa-08-soundtrack-announced/)
    (September 11, 2007), EA Sports