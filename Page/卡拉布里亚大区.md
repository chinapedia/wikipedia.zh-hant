**卡拉布里亚**（，），從前稱為“Brutium”，是[意大利南部的一個大区](../Page/意大利.md "wikilink")，包含了[那不勒斯以南像](../Page/那不勒斯.md "wikilink")「足尖」的[意大利半島](../Page/意大利半島.md "wikilink")。大区北鄰[巴斯利卡塔大区](../Page/巴斯利卡塔.md "wikilink")，西南鄰[西西里自治区](../Page/西西里.md "wikilink")，西鄰[第勒尼安海](../Page/第勒尼安海.md "wikilink")，及東鄰[伊奧尼亞海](../Page/伊奧尼亞海.md "wikilink")。大区面積15,081[平方公里](../Page/平方公里.md "wikilink")，人口2,007,392人。

## 历史

在地图上看，卡拉布里亚在意大利那个“靴子”上的脚趾部分。

意大利的历史，在地中海地区，不论从文化或社会发展角度都占据着最重要的地位。这一地区有着重要的史前时期人类活动的记录。大希腊、伊特鲁里亚文明以及其后的罗马帝国分别统治了这片土地许多个世纪。意大利的发展亦即是罗马帝国的兴衰。希腊人、罗马人甚至是诺曼底人都在这美丽肥沃的土地停留了很长的一段时间。至今这片土地上还有众多保存完好的古老庙宇、雕像和圆柱式建筑物，古老与传统在这里得到了完美展现，因此，这里成为了大量考古学家的天堂。最有意义的无疑是雷焦卡拉布里亚国家考古博物馆，这是意大利南部最大的博物馆之一，还保存着公元前5世纪的著名青铜文物（Bronzi
di
Riace），它们是1972年在里亚切（Riace）海岸发现的，是两个男性人物雕像，是古典希腊雕塑的重要见证。锡巴里、洛克里的考古区；克罗托内和维博瓦伦蒂亚的考古博物馆也非常有意思。在锡巴里还可以参观有很多剧场、温泉和住宅地基的古罗马城市的遗迹。

## 旅遊景點

卡拉布里亚城区的大部分建筑都是在1908年大地震之后重建的。

  - 大希腊国立博物馆，收藏有许多来自[古勒左卡拉布里亚](../Page/古勒左卡拉布里亚.md "wikilink")（该城旧址就位于此地）和其他古希腊地区的史前古器物。包括一些古希腊的铜像，以及1972年从[里亚契码头附近海里挖掘出来的武士雕像](../Page/里亚契.md "wikilink")。

## 主要足球隊

  - [雷吉纳](../Page/雷吉纳足球俱乐部.md "wikilink")
  - [卡坦扎罗队](../Page/卡坦扎罗队.md "wikilink")
  - [科森查队](../Page/科森查队.md "wikilink")

## 参考文献

## 外部連結

  - [Official Region homepage](http://www.regione.calabria.it/)
  - [Map of
    Calabria](https://web.archive.org/web/20060725102347/http://www.italy-weather-and-maps.com/maps/italy/calabria.gif)
  - [Dictionary](https://web.archive.org/web/20060619213859/http://www.websters-online-dictionary.org/translation/Calabrese/)
    with Calabrese - English Translations from [Webster's Online
    Dictionary](https://web.archive.org/web/20120223164907/http://www.websters-online-dictionary.org/)
    - the Rosetta Edition
  - [ItalianVisits.Com:
    Calabria](https://web.archive.org/web/20060618032523/http://www.italianvisits.com/calabria/)

[\*](../Category/卡拉布里亞大區.md "wikilink")