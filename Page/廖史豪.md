**廖史豪**（），[台灣](../Page/台灣.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[西螺鎮人](../Page/西螺鎮.md "wikilink")，出身西螺望族廖家。[台灣獨立運動參與者](../Page/台灣獨立運動.md "wikilink")，曾任[台灣再解放聯盟台灣地下工作委員會](../Page/台灣再解放聯盟.md "wikilink")。為「[台灣共和國臨時政府](../Page/台灣共和國臨時政府.md "wikilink")」大統領[廖文毅姪子](../Page/廖文毅.md "wikilink")。

## 早年生平

廖史豪於1923年（[大正十二年](../Page/大正.md "wikilink")）2月11日生於[日本](../Page/日本.md "wikilink")[京都](../Page/京都.md "wikilink")，父親[廖溫仁](../Page/廖溫仁.md "wikilink")，是廖文毅大哥。母親[廖蔡秀鸞出身](../Page/廖蔡秀鸞.md "wikilink")[台中](../Page/臺中市.md "wikilink")[清水望族](../Page/清水區_\(臺中市\).md "wikilink")，十八歲與就讀[東北帝國大學醫學院的廖溫仁結婚](../Page/東北帝國大學.md "wikilink")。廖史豪在京都就讀京都第一錦仁小學校。1936年父親病逝後返台，9月轉入[台南州州立](../Page/台南州.md "wikilink")[嘉義中學](../Page/嘉義中學.md "wikilink")。1940年搬至[東京](../Page/東京.md "wikilink")，就讀[關東中學](../Page/關東中學.md "wikilink")。1942年考上[立教大學文學系](../Page/立教大學.md "wikilink")。1944年因[二次世界大戰](../Page/二次世界大戰.md "wikilink")，廖史豪參加徵兵，與[李登輝等人考上甲種高射砲兵](../Page/李登輝.md "wikilink")，同為[千葉高射砲學校十二期兵](../Page/千葉高射砲學校.md "wikilink")，同期訓練，不同中隊。同期亦有劉慶瑞等12名台灣人。大戰結束後曾任職台灣同鄉會，於1946年5月返台。

## 從事台獨運動

廖史豪在戰後回到[台灣](../Page/台灣.md "wikilink")，住在三叔廖文毅台北住家。目睹國民政府的無能。曾參與其廖文毅的《[前鋒雜誌](../Page/前鋒雜誌.md "wikilink")》。1947年2月26日，廖史豪與二叔[廖文奎](../Page/廖文奎.md "wikilink")、三叔[廖文毅到](../Page/廖文毅.md "wikilink")[上海](../Page/上海.md "wikilink")。之後於《[大公報](../Page/大公報.md "wikilink")》中得知[二二八事件](../Page/二二八事件.md "wikilink")。4月[陳儀政府發布](../Page/陳儀.md "wikilink")「[二二八事變首謀叛亂犯在逃主犯名冊](../Page/二二八事變首謀叛亂犯在逃主犯名冊.md "wikilink")」，廖文毅及廖文奎都被列入成為通緝犯，無法返台而留在上海。在上海期間，[共產黨的](../Page/共產黨.md "wikilink")[蔡子民曾欲吸收廖史豪](../Page/蔡子民.md "wikilink")，但遭其拒絕。6月，「[台灣再解放聯盟](../Page/台灣再解放聯盟.md "wikilink")」成立。同年8月，廖史豪返台，除與廖文毅聯絡外，也宣揚台獨理念。

之後廖史豪成為其叔叔們在台灣的代理人，並且吸收人才。期間[邱永漢](../Page/邱永漢.md "wikilink")、[簡文介等人都是在他的介紹下](../Page/簡文介.md "wikilink")，加入台灣再解放聯盟。1948年9月，在[香港與廖文毅印成向](../Page/香港.md "wikilink")[聯合國提出關於台灣希望獨立英文備忘錄](../Page/聯合國.md "wikilink")，送往聯合國及主要會員國。10月，從香港返台，並且於1949年成立「台灣再解放聯盟台灣支部」，廖史豪推[黃紀男擔任支部長](../Page/黃紀男.md "wikilink")，自己屈居為副，並隨即積極吸收成員，先後計有[鍾謙順](../Page/鍾謙順.md "wikilink")、[溫炎煜](../Page/溫炎煜.md "wikilink")、[許劍雄](../Page/許劍雄.md "wikilink")、[許朝卿](../Page/許朝卿.md "wikilink")、[偕約瑟等人加入](../Page/偕約瑟.md "wikilink")，於島內進行組織工作。1950年4月20日與台灣民主獨立黨陳介民之女陳娟娟結婚。

## 被捕

1950年初，國民黨政府開始展開大逮捕。5月25日，廖史豪與黃紀男第一次被補，廖史豪原本逃至省政府民政廳長[楊肇嘉家](../Page/楊肇嘉.md "wikilink")。但因國民黨政府逮捕其母親作為人質，只好自首。判刑七年，在[台北監獄寄押一年多](../Page/台北監獄.md "wikilink")，同期政治犯包括文學家[楊逵](../Page/楊逵.md "wikilink")。1952年4月移至[綠島](../Page/綠島.md "wikilink")。1953年離開綠島，之後因其領導的其他現被破，再度被捕，被以《[懲治叛亂條例](../Page/懲治叛亂條例.md "wikilink")》(俗稱「二條一」)
叛亂罪起訴。1954年6月判無期徒刑，移監新店軍人監獄。1956年[警總曾派](../Page/警總.md "wikilink")[黃朝琴赴日](../Page/黃朝琴.md "wikilink")，要求時任「[台灣共和國臨時政府](../Page/台灣共和國臨時政府.md "wikilink")」外交部長的丈人[陳哲民返台](../Page/陳哲民.md "wikilink")。1956年6月22日陳哲民返台後，由於台灣共和國臨時政府在東京仍有組織，為了對東京行招降之效，調查局申請將廖、黃減刑。12月，廖史豪從無期徒刑減為十二年；黃紀男由十二年減為九年。1957年10月，移往新竹留質所。1958年8月25日與黃出獄。

廖史豪出獄後，取得廖文毅連繫，於1959年1月成立「台灣民主獨立黨台灣地下工作委員會」，參加的人包括廖文毅五弟廖溫進、表弟林奉恩，以及鍾謙順、陳火桐等人。繼續從事宣傳工作。其中獲得[楊肇嘉的贊成](../Page/楊肇嘉.md "wikilink")，也找過[郭雨新](../Page/郭雨新.md "wikilink")。廖史豪認為國民政府視楊肇嘉與[吳三連為台獨精神領袖](../Page/吳三連.md "wikilink")。時任[台灣共和國臨時政府大統領廖文毅給予指示](../Page/台灣共和國臨時政府.md "wikilink")，認為為向國際宣示台灣島內獨立的要求，必須做出革命行動，因此也計畫暗殺[蔣經國](../Page/蔣經國.md "wikilink")，但最後因為軍火、金錢及人員問題而未能成功。導致第二次被捕。

## 再次被捕與獲釋

1962年1月，廖史豪與母親、陳火桐同時被捕。判刑之前曾與施明德同房。1965年1月14日廖史豪與黃紀男同判[死刑](../Page/死刑.md "wikilink")，其母親與五叔廖溫進皆被判刑。國民黨政府也藉此為人質要求廖文毅投降。廖文毅投降後，廖史豪與黃紀男、廖溫進獲得特赦。12月出獄。

1971年，[調查局局長](../Page/調查局.md "wikilink")[沈之岳曾派廖史豪赴日要求](../Page/沈之岳.md "wikilink")[簡文成返台](../Page/簡文成.md "wikilink")，並未成功。

## 參考來源

## 相關書目及外部連結

  - [張炎憲](../Page/張炎憲.md "wikilink")、胡慧玲、曾秋美採訪，台灣獨立運動的先聲：台灣共和國，台北：吳三連台灣史料中心編輯，2000年。
  - [廖史豪從事台獨運動終不悔](http://www.southnews.com.tw/Myword/06/myword_06_006.htm)
  - [廖史豪等叛亂案](https://aa.archives.gov.tw/archivesData.aspx?SystemID=0000011965)，國家檔案資訊網。

[Category:臺灣獨立運動者](../Category/臺灣獨立運動者.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Shi史豪](../Category/廖姓.md "wikilink")
[Category:立教大學校友](../Category/立教大學校友.md "wikilink")