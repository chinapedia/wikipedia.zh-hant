[Victor_Hui.jpg](https://zh.wikipedia.org/wiki/File:Victor_Hui.jpg "fig:Victor_Hui.jpg")
**許晉奎**（**Victor
Hui**，），人稱「大奎」，[廣東](../Page/廣東.md "wikilink")[湛江人](../Page/湛江.md "wikilink")，[香港商人](../Page/香港.md "wikilink")，[許愛周家族成員](../Page/許愛周家族.md "wikilink")，熱心於體育事務。現任[中國香港體育協會暨奧林匹克委員會副會長](../Page/中國香港體育協會暨奧林匹克委員會.md "wikilink")，前[香港賽馬會董事](../Page/香港賽馬會.md "wikilink")，前[香港足球總會主席](../Page/香港足球總會.md "wikilink")。

## 簡歷

許晉奎於1969年加入[南華體育會](../Page/南華體育會.md "wikilink")，1982年首次出任執委會主席，曾先後擔任11任南華會主席。之後擔任多項體育公職，2004年4月1日獲[香港政府委任為](../Page/香港政府.md "wikilink")[精英體育事務委員會主席](../Page/精英體育事務委員會.md "wikilink")。

## 名下馬匹

許晉奎身為[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")，旗下亦有多匹賽駒，全部以「金剛」命名，包括無敵金剛（同一馬名曾使用兩次）、神奇金剛、王者金剛、神勇金剛、風雷金剛、神威金剛、神鑽金剛、神俊金剛、神彩金剛、神龍金剛、神劍金剛、神箭金剛、神速金剛、神力金剛、神雄金剛、神幻金剛、神電金剛、神霸金剛、神寶金剛、神火金剛、神亮金剛，早期大部份由現已退休的[王兆旦](../Page/王兆旦.md "wikilink")、[簡炳墀訓練](../Page/簡炳墀.md "wikilink")，現則交由[葉楚航馬房訓練](../Page/葉楚航.md "wikilink")。其中「神鑽金剛」贏得12場頭馬，該駒自1996-1997年度馬季開始服役，直至2002-2003年度退役，期間共跑了106場，成為香港紀錄，但其後被[胡森馬房的鄺流想名下賽駒](../Page/胡森_\(練馬師\).md "wikilink")「神殿」打破。「神彩金剛」則曾奪得2001年度[皇太后紀念盃](../Page/皇太后紀念盃.md "wikilink")。

## 曾擔任職務

**中國**

  - 廣東省湛江市政協名譽主席
  - 廣東省政協委員（一九八八至二零零三）

**香港**

  - 中國香港體育協會暨奧林匹克委員會副會長 (1998年起)
  - 香港體育委員會 成員 (2005年起)
  - [2009年東亞運動會有限公司董事局成員](../Page/2009年東亞運動會.md "wikilink") (2005年起)
  - 精英體育事務委員會主席 (2004年起)
  - [香港賽馬會董事](../Page/香港賽馬會.md "wikilink") (2004年-2007年)
  - [香港康體發展局主席](../Page/香港康體發展局.md "wikilink") (2003-2004年度)
  - [香港體育代表團團長](../Page/香港體育代表團.md "wikilink")
  - [南華體育會會長](../Page/南華體育會.md "wikilink")
  - [香港足球總會主席](../Page/香港足球總會.md "wikilink")
    (1976年-1979年，1982年-1983年，1985年-1988年，1991年-1993年，1996年-1999年)
  - [香港遊樂場協會主席](../Page/香港遊樂場協會.md "wikilink") (1986年起)
  - [香港排球總會會長](../Page/香港排球總會.md "wikilink") (2000年-2014年)
  - [香港保齡球總會永遠名譽會長](../Page/香港保齡球總會.md "wikilink")
  - [香港單車聯會名譽會長](../Page/香港單車聯會.md "wikilink")
  - [香港乒乓總會名譽會長](../Page/香港乒乓總會.md "wikilink")
  - [香港武術聯會名譽會長](../Page/香港武術聯會.md "wikilink")
  - [香港大專體育協會名譽會長](../Page/香港大專體育協會.md "wikilink")
  - [香港學界體育聯會名譽會長](../Page/香港學界體育聯會.md "wikilink")
  - [香港體育記者協會副會長](../Page/香港體育記者協會.md "wikilink")
  - [香港足球裁判會贊助人](../Page/香港足球裁判會.md "wikilink")

## 榮譽

  - 1989年：[太平紳士](../Page/太平紳士.md "wikilink")
  - 1996年：[MBE勳銜](../Page/MBE.md "wikilink")
  - 2004年：[銀紫荊星章](../Page/銀紫荊星章.md "wikilink")
  - 2010年；[金紫荊星章](../Page/金紫荊星章.md "wikilink")

## 參考

  - [許愛周家族](../Page/許愛周家族.md "wikilink")

## 資料來源

  - [許晉奎　賽馬覓知己](https://web.archive.org/web/20070927061910/http://primecomhk.com/article/0504/0504_100.shtml)
  - [歡迎年輕人加入南華會──訪南華會主席許晉奎](https://web.archive.org/web/20070312062436/http://www.takungpao.com/news/06/06/01/WF-573565.htm)

[Category:湛江人](../Category/湛江人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:香港足球界人士](../Category/香港足球界人士.md "wikilink")
[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[Category:獲頒授香港銀紫荊星章者](../Category/獲頒授香港銀紫荊星章者.md "wikilink")
[Jin晉](../Category/许姓.md "wikilink")
[Jin晉](../Category/許愛周家族.md "wikilink")
[Category:廣東省政協委員](../Category/廣東省政協委員.md "wikilink")