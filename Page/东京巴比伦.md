《**東京巴比倫**》是[日本](../Page/日本.md "wikilink")[漫畫家組合](../Page/漫畫家.md "wikilink")[CLAMP的作品](../Page/CLAMP.md "wikilink")，[漫畫從](../Page/漫畫.md "wikilink")1990年至1993年間在[新書館的](../Page/新書館.md "wikilink")《月刊WINGS》連載，另有2集[OVA及](../Page/OVA.md "wikilink")1部真人電影推出。

故事以[陰陽師皇昴流為主角](../Page/陰陽師.md "wikilink")，反映出很多的[社會問題](../Page/社會問題.md "wikilink")，是一套得到很多成人讀者認同的作品。本作品的線條沒有《[聖傳](../Page/聖傳_\(漫畫\).md "wikilink")》和《[X](../Page/X_\(漫畫\).md "wikilink")》般華麗、複雜，比較簡明。

## 登場人物

  - 皇昴流（、[聲優](../Page/聲優.md "wikilink")：[山口勝平](../Page/山口勝平.md "wikilink")、[演员](../Page/演员.md "wikilink")：[东根作寿英](../Page/东根作寿英.md "wikilink")）
    立居日本[陰陽道頂点的皇一族第](../Page/陰陽道.md "wikilink")13代掌門。
  - 皇北都（、聲優：[伊藤美紀](../Page/伊藤美紀.md "wikilink")）
    昴流的孿生姐姐。
  - 櫻塚星史郎（、聲優：[子安武人](../Page/子安武人.md "wikilink")、演员：[四方堂亘](../Page/四方堂亘.md "wikilink")）
    表面的職業是獸醫，實屬暗殺集團『櫻塚護』。
  - 南雲真司（聲優：[池田秀一](../Page/池田秀一.md "wikilink")）
    利用自己出生起便被靈保護著，所有不幸和事故都會被轉嫁到周圍他人身上的特性，為了往上爬而害死許多人。
  - 麻生風美（聲優：[鶴弘美](../Page/鶴弘美.md "wikilink")）
    為了替被南雲害死的哥哥報仇而試圖對他下詛咒。
  - 山川刑警（聲優：[山寺宏一](../Page/山寺宏一.md "wikilink")）
    追查著南雲身邊一連串事件的刑警。
  - 三村社長（聲優：[西村知道](../Page/西村知道.md "wikilink")）
    南雲的公司社長。
  - 會長（聲優：[沢木郁也](../Page/沢木郁也.md "wikilink")）
    南雲的公司會長。

## 關連商品

### 書籍

  - 單行本
    1.  1991年4月10日發行
    2.  1991年11月10日發行
    3.  1992年1月25日發行
    4.  1992年7月10日發行
    5.  1993年4月5日發行
    6.  1993年8月25日發行
    7.  1994年3月25日發行
  - 畫集
      - TOKYO BABYLON PHOTOGRAPHS 1996年4月25日發行

### CD

  - CD COMIC 東京BABYLON 1990年11月25日發行
  - TOKYO BABYLON IMAGE SOUNDTRACK 1992年7月22日發行
  - 東京BABYLON SINGLE TRIPLE 1993年2月21日發行
  - TOKYO BABYLON 1999 1993年9月22日發行
  - デジャヴ（東京BABYLON2 主題曲）1994年2月21日發行
  - TOKYO BABYLON IMAGE SOUNDTRACK 2 1994年3月9日發行

### 其他

  - 東京BABYLON POST CARDS BOOK（1993年8月26日發行）
  - 東京BABYLON CALENDAR 1992

[Category:CLAMP](../Category/CLAMP.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:少女漫画](../Category/少女漫画.md "wikilink")
[Category:奇幻漫画](../Category/奇幻漫画.md "wikilink")
[Category:日本OVA動畫](../Category/日本OVA動畫.md "wikilink")
[Category:奇幻动画](../Category/奇幻动画.md "wikilink")
[Category:1993年电影](../Category/1993年电影.md "wikilink")
[Category:日本电影作品](../Category/日本电影作品.md "wikilink")
[Category:日本漫畫改編真人電影](../Category/日本漫畫改編真人電影.md "wikilink")
[Category:阴阳道题材作品](../Category/阴阳道题材作品.md "wikilink")
[Category:雙胞胎題材漫畫](../Category/雙胞胎題材漫畫.md "wikilink")
[Category:雙胞胎題材動畫](../Category/雙胞胎題材動畫.md "wikilink")
[Category:東京背景作品](../Category/東京背景作品.md "wikilink")
[Category:亞洲電視外購動畫](../Category/亞洲電視外購動畫.md "wikilink")