**8月25日**是[阳历年的第](../Page/阳历.md "wikilink")237天（[闰年是](../Page/闰年.md "wikilink")238天），离一年的结束还有128天。

## 大事记

### 17世紀

  - [1609年](../Page/1609年.md "wikilink")：[意大利天文学家](../Page/意大利.md "wikilink")[伽利略在](../Page/伽利略·伽利莱.md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink")[议会中展示他制作的首个](../Page/威尼斯议会.md "wikilink")[折射望远镜](../Page/折射望远镜.md "wikilink")。
  - [1667年](../Page/1667年.md "wikilink")：[康熙皇帝在](../Page/康熙皇帝.md "wikilink")[太和殿举行亲政典礼](../Page/太和殿.md "wikilink")，接受百官朝贺，宣诏海内，[大赦天下](../Page/大赦.md "wikilink")。

### 18世紀

  - [1718年](../Page/1718年.md "wikilink")：[法国移民在](../Page/法国.md "wikilink")[路易斯安娜建成](../Page/路易斯安那州.md "wikilink")[新奥尔良市](../Page/紐奧良.md "wikilink")。

### 19世紀

  - [1825年](../Page/1825年.md "wikilink")：[烏拉圭宣布脫離](../Page/烏拉圭.md "wikilink")[巴西帝國獨立](../Page/巴西帝國.md "wikilink")。
  - [1830年](../Page/1830年.md "wikilink")：[比利時獨立革命爆發](../Page/比利時獨立.md "wikilink")。
  - [1894年](../Page/1894年.md "wikilink")：[日本生物學家](../Page/日本.md "wikilink")[北里柴三郎發現](../Page/北里柴三郎.md "wikilink")[鼠疫杆菌](../Page/鼠疫.md "wikilink")。

### 20世紀

  - [1901年](../Page/1901年.md "wikilink")：[臺鐵](../Page/臺灣鐵路管理局.md "wikilink")[淡水線通車](../Page/臺鐵淡水線.md "wikilink")。
  - [1912年](../Page/1912年.md "wikilink")：[中国同盟会等数个党派在](../Page/中国同盟会.md "wikilink")[北京湖广会馆宣告组成](../Page/北京湖广会馆.md "wikilink")[中国国民党](../Page/中国国民党.md "wikilink")，[孙中山任理事长](../Page/孙中山.md "wikilink")。
  - [1917年](../Page/1917年.md "wikilink")：[孙中山先生领导](../Page/孙中山.md "wikilink")[护法运动](../Page/护法运动.md "wikilink")，在廣州召開「[國會非常會議](../Page/中華民國初年國會#廣州國會非常會議.md "wikilink")」。
  - [1920年](../Page/1920年.md "wikilink")：[波苏战争](../Page/波苏战争.md "wikilink")：[波兰军队在](../Page/波兰.md "wikilink")[毕苏斯基的率领下击败](../Page/约瑟夫·毕苏斯基.md "wikilink")[苏联红军](../Page/苏联红军.md "wikilink")，迫使其撤出[华沙](../Page/华沙.md "wikilink")，[华沙战役结束](../Page/华沙战役_\(1920年\).md "wikilink")。
  - [1927年](../Page/1927年.md "wikilink")：[中国国民政府定都](../Page/中国国民政府.md "wikilink")[南京](../Page/南京.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：[中国](../Page/中国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[茂县发生](../Page/茂县.md "wikilink")[里氏](../Page/里氏.md "wikilink")7.5级的[强烈地震](../Page/疊溪大地震.md "wikilink")，共造成约9000人死亡，地震引发的地陷形成[叠溪海子](../Page/叠溪海子.md "wikilink")。
  - [1937年](../Page/1937年.md "wikilink")：國民政府將[中国工农红军改編為](../Page/中国工农红军.md "wikilink")[八路军](../Page/八路军.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[自由法國的军队在](../Page/自由法國.md "wikilink")[盟军的协助下](../Page/盟军.md "wikilink")，[解放了](../Page/解放巴黎.md "wikilink")[德军占领的](../Page/德军.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。同日[羅馬尼亞王國投降盟軍](../Page/羅馬尼亞王國.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[中共中央发表](../Page/中共中央.md "wikilink")《对目前局势的宣言》。
  - [1953年](../Page/1953年.md "wikilink")：[中国过渡时期总路线正式公布](../Page/中国.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：第一袋[方便面](../Page/方便面.md "wikilink")「鸡汁麵」问世。
  - [1960年](../Page/1960年.md "wikilink")：[第17届奥林匹克运动会在](../Page/1960年夏季奧林匹克運動會.md "wikilink")[意大利](../Page/意大利.md "wikilink")[罗马举行](../Page/罗马.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[美国](../Page/美国.md "wikilink")[纳粹党党魁喬治](../Page/美國納粹黨.md "wikilink")·林肯·洛克威爾（）被击毙。
  - [1971年](../Page/1971年.md "wikilink")：。
  - [1976年](../Page/1976年.md "wikilink")：[秀茂坪山泥傾瀉意外](../Page/秀茂坪山泥傾瀉意外.md "wikilink")：[香港](../Page/香港.md "wikilink")[觀塘區](../Page/觀塘區.md "wikilink")[秀茂坪發生自](../Page/秀茂坪.md "wikilink")[六一八雨災以來最嚴重山泥傾瀉](../Page/六一八雨災.md "wikilink")，造成18人死亡24人受傷。
  - [1980年](../Page/1980年.md "wikilink")：[辛巴威加入](../Page/辛巴威.md "wikilink")[聯合國](../Page/聯合國.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：。
  - [1985年](../Page/1985年.md "wikilink")：[巴港航空1808號班機在缅因州坠毁](../Page/巴港航空1808號班機空難.md "wikilink")，造成8人死亡，小型飞机随后被要求安装[飛行紀錄儀](../Page/飛行紀錄儀.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[喀麦隆總統宣布](../Page/喀麦隆.md "wikilink")，連日來已有1200人因[尼奧斯湖](../Page/尼奧斯湖.md "wikilink")[湖底火山噴發冒出的毒烟中毒身亡](../Page/湖底噴發.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[航海家2号接近](../Page/航海家2号.md "wikilink")[海王星并且在飞越时确认了](../Page/海王星.md "wikilink")[海王星环的存在](../Page/海王星环.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[中国人民解放军](../Page/中国人民解放军.md "wikilink")[飞行员王宝玉驾驶](../Page/飞行员.md "wikilink")[歼-6战斗机叛逃到](../Page/歼-6.md "wikilink")[海参崴](../Page/海参崴.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[蘇聯解體](../Page/蘇聯解體.md "wikilink")：[白俄罗斯独立](../Page/白俄罗斯.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：香港[中環](../Page/中環.md "wikilink")[國際大廈發生四級火警](../Page/中保集團大廈.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：越南總理[武文傑與柬埔寨臨時政府聯合主席](../Page/武文傑.md "wikilink")[拉那烈和](../Page/拉那烈.md "wikilink")[洪森在](../Page/洪森.md "wikilink")[河內簽署](../Page/河內.md "wikilink")《越柬聯合公報》。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[肯尼迪航天中心成功地将价值](../Page/肯尼迪航天中心.md "wikilink")7亿[美元的](../Page/美元.md "wikilink")[史匹哲太空望遠鏡送入太空](../Page/史匹哲太空望遠鏡.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[飓风卡特里娜在美国](../Page/2005年颶風卡特里娜.md "wikilink")[佛罗里达州登陆](../Page/佛罗里达州.md "wikilink")。
  - [2017年](../Page/2017年.md "wikilink")：[福爾摩沙衛星五號在美國](../Page/福爾摩沙衛星五號.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[范登堡空軍基地發射升空](../Page/范登堡空軍基地.md "wikilink")。

## 出生

  - [1530年](../Page/1530年.md "wikilink")：[伊凡四世](../Page/伊凡四世.md "wikilink")，[俄羅斯史上第一位](../Page/俄羅斯.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")（[1584年逝世](../Page/1584年.md "wikilink")）
  - [1540年](../Page/1540年.md "wikilink")：[凱瑟琳·格雷](../Page/凱瑟琳·格雷.md "wikilink")，[英格蘭假定女繼承人](../Page/英格蘭.md "wikilink")，英格蘭公主[瑪麗·都鐸的](../Page/瑪麗·都鐸.md "wikilink")[外孫女](../Page/外孫女.md "wikilink")。（[1568年逝世](../Page/1568年.md "wikilink")）
  - [1707年](../Page/1707年.md "wikilink")：[路易斯一世](../Page/路易斯一世_\(西班牙\).md "wikilink")，[西班牙在位最短的國王](../Page/西班牙.md "wikilink")（[1724年逝世](../Page/1724年.md "wikilink")）
  - [1744年](../Page/1744年.md "wikilink")：[赫爾德](../Page/赫爾德.md "wikilink"),
    [德國哲學家](../Page/德國.md "wikilink")、路德派神學家、詩人。其作品《論語言的起源》成為狂飆運動的基礎（[1803年逝世](../Page/1803年.md "wikilink")）
  - [1767年](../Page/1767年.md "wikilink")：[路易·德聖茹斯特](../Page/路易·德聖茹斯特.md "wikilink")，[法國大革命](../Page/法國大革命.md "wikilink")[雅各賓專政時期領袖](../Page/雅各賓專政.md "wikilink")；1791年出版《革命與法國憲法》成為革命青年理論家（[1794年逝世](../Page/1794年.md "wikilink")）
  - [1772年](../Page/1772年.md "wikilink")：[威廉一世](../Page/威廉一世_\(尼德兰\).md "wikilink")，[荷兰国王](../Page/荷兰国王.md "wikilink")（[1843年逝世](../Page/1843年.md "wikilink")）
  - [1786年](../Page/1786年.md "wikilink")：[路德维希一世](../Page/路德维希一世_\(巴伐利亚\).md "wikilink")，[巴伐利亞國王](../Page/巴伐利亞.md "wikilink")（1825年-1848年3月20日）
  - [1841年](../Page/1841年.md "wikilink")：[埃米爾·特奧多爾·科赫爾](../Page/埃米爾·特奧多爾·科赫爾.md "wikilink")，[瑞士科學家](../Page/瑞士.md "wikilink")，1909年獲[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")（[1917年逝世](../Page/1917年.md "wikilink")）
  - [1845年](../Page/1845年.md "wikilink")：[路德維希二世](../Page/路德維希二世.md "wikilink")，[維特爾斯巴赫王朝的巴伐利亞國王](../Page/維特爾斯巴赫王朝.md "wikilink")（[1886年逝世](../Page/1886年.md "wikilink")）
  - [1882年](../Page/1882年.md "wikilink")：[斯恩·奧凱利](../Page/斯恩·奧凱利.md "wikilink")，[愛爾蘭第](../Page/愛爾蘭.md "wikilink")2任總統（[1966年逝世](../Page/1966年.md "wikilink")）
  - [1893年](../Page/1893年.md "wikilink")：[杜聰明](../Page/杜聰明.md "wikilink")，[台灣第一位](../Page/台灣.md "wikilink")[醫學博士](../Page/醫學.md "wikilink")（[1986年逝世](../Page/1986年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：[汉斯·阿道夫·克雷布斯](../Page/汉斯·阿道夫·克雷布斯.md "wikilink")，原籍德國，後移民英國，醫生、生物化學家，獲1953年諾貝爾生理學或醫學獎（[1981年逝世](../Page/1981年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[玛利亚·傅天娜·克瓦尔斯卡](../Page/玛利亚·傅天娜·克瓦尔斯卡.md "wikilink")，[波蘭修女](../Page/波蘭.md "wikilink")，因見證耶穌基督顯靈，2000年被羅馬天主教封聖（[1938年逝世](../Page/1938年.md "wikilink")）
  - [1911年](../Page/1911年.md "wikilink")：[武元甲](../Page/武元甲.md "wikilink")，[越南人民軍大將](../Page/越南.md "wikilink")，號稱「紅色拿破崙」（[2013年逝世](../Page/2013年.md "wikilink")）
  - [1912年](../Page/1912年.md "wikilink")：[劉其偉](../Page/劉其偉.md "wikilink")，台灣[畫家兼](../Page/畫家.md "wikilink")[人類學家](../Page/人類學家.md "wikilink")（[2002年逝世](../Page/2002年.md "wikilink")）
      - [埃里希·昂纳克](../Page/埃里希·昂纳克.md "wikilink")，最後一位正式的[東德領導人](../Page/東德領導人.md "wikilink")（[1994年逝世](../Page/1994年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[坂井三郎](../Page/坂井三郎.md "wikilink")，[二戰時](../Page/二戰.md "wikilink")[日本戰鬥機](../Page/日本.md "wikilink")[王牌飛行員](../Page/王牌飛行員.md "wikilink")（[2000年逝世](../Page/2000年.md "wikilink")）
  - [1918年](../Page/1918年.md "wikilink")：[萊納德·伯恩斯坦](../Page/萊納德·伯恩斯坦.md "wikilink")，[美国指挥家](../Page/美国.md "wikilink")（[1990年逝世](../Page/1990年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[赫伯特·克勒默](../Page/赫伯特·克勒默.md "wikilink")，美籍德裔物理學家，2000年獲得諾貝爾物理學獎
  - [1930年](../Page/1930年.md "wikilink")：[史恩·康納萊](../Page/史恩·康納萊.md "wikilink")，[英国电影演员](../Page/英国.md "wikilink")
  - [1934年](../Page/1934年.md "wikilink")：[阿克巴尔·哈什米·拉夫桑贾尼](../Page/阿克巴尔·哈什米·拉夫桑贾尼.md "wikilink")，[伊朗政治家及作家](../Page/伊朗.md "wikilink")，曾出任第四任[伊朗總統](../Page/伊朗總統.md "wikilink")（[2017年逝世](../Page/2017年.md "wikilink")）
  - [1940年](../Page/1940年.md "wikilink")：[何塞·凡·丹姆](../Page/何塞·凡·丹姆.md "wikilink")，[比利時男中音歌劇唱家](../Page/比利時.md "wikilink")
  - [1949年](../Page/1949年.md "wikilink")：[吉恩·西蒙斯](../Page/吉恩·西蒙斯.md "wikilink")，以色列裔美國搖滾貝斯手、歌手、演員，[Kiss樂團創立者之一](../Page/Kiss_\(乐队\).md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[澤木郁也](../Page/澤木郁也.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[艾維斯·卡斯提洛](../Page/艾維斯·卡斯提洛.md "wikilink")，英國創作歌手
  - [1954年](../Page/1954年.md "wikilink")：[阿基師](../Page/阿基師.md "wikilink")，[台灣的知名廚師](../Page/台灣.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[提姆·波頓](../Page/提姆·波頓.md "wikilink")，[美国电影导演](../Page/美国电影.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[宋丹丹](../Page/宋丹丹.md "wikilink")，[中國女演員](../Page/中國.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：[马克西姆·孔采维奇](../Page/马克西姆·孔采维奇.md "wikilink")，法國俄裔數學物理學家
  - [1967年](../Page/1967年.md "wikilink")：[檜山修之](../Page/檜山修之.md "wikilink")，日本動畫聲優
      - [湯姆·荷蘭德](../Page/湯姆·荷蘭德.md "wikilink")，英國實力派電影及電視演員
      - [陳慧儀](../Page/陳慧儀.md "wikilink")：香港[財經節目主持人](../Page/金融.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[郭少芸](../Page/郭少芸.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[楊宗憲](../Page/楊宗憲.md "wikilink")，台灣男歌手
  - [1970年](../Page/1970年.md "wikilink")：[歌地亞·雪花](../Page/歌地亞·雪花.md "wikilink")，德國[超級名模](../Page/超級名模.md "wikilink")
      - [罗伯特·霍里](../Page/罗伯特·霍里.md "wikilink")，美國職業籃球運動員
  - [1975年](../Page/1975年.md "wikilink")：[黃浩然](../Page/黃浩然.md "wikilink")，香港男演員
      - [彼得利亚·托马斯](../Page/彼得利亚·托马斯.md "wikilink")，[澳大利亞游泳運動員](../Page/澳大利亞.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[戴蒙·琼斯](../Page/戴蒙·琼斯.md "wikilink")，美國職業籃球運動員
      - [亞歷山大·斯卡斯加德](../Page/亞歷山大·斯卡斯加德.md "wikilink")，瑞典演員
      - [淺野真澄](../Page/淺野真澄.md "wikilink")，日本動畫聲優
  - [1977年](../Page/1977年.md "wikilink")：[迭戈·科拉萊斯](../Page/迭戈·科拉萊斯.md "wikilink")，美國[拳擊運動員](../Page/拳擊.md "wikilink")（[2007年逝世](../Page/2007年.md "wikilink")）
  - [1978年](../Page/1978年.md "wikilink")：[九把刀](../Page/九把刀.md "wikilink")，台灣[網路作家](../Page/網路作家.md "wikilink")
      - [陳士駿](../Page/陳士駿.md "wikilink")，[華裔美國人企業家](../Page/華裔美國人.md "wikilink")，[YouTube的創辦人之一](../Page/YouTube.md "wikilink")
      - [馬龍·希亞活](../Page/馬龍·希亞活.md "wikilink")，巴貝多血統英格蘭足球運動員
  - [1981年](../Page/1981年.md "wikilink")：[瑞秋·比爾森](../Page/瑞秋·比爾森.md "wikilink")，美國演員
  - [1982年](../Page/1982年.md "wikilink")：[鄭在成](../Page/鄭在成.md "wikilink")，[韓国男子](../Page/韓国.md "wikilink")[羽毛球运动员](../Page/羽毛球.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[大堀惠](../Page/大堀惠.md "wikilink")，日本的女歌手
      - [周家蔚](../Page/周家蔚.md "wikilink")，香港演員
      - [關伊彤](../Page/關伊彤.md "wikilink")，香港演員
  - [1984年](../Page/1984年.md "wikilink")：[余祥銓](../Page/余祥銓.md "wikilink")，台灣藝人
      - [亨德拉·塞蒂亚万](../Page/亨德拉·塞蒂亚万.md "wikilink")，[印度尼西亞男子羽球運動員](../Page/印度尼西亞.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[夏卡毛](../Page/夏卡毛.md "wikilink")，台灣音乐人
      - [赵芸蕾](../Page/赵芸蕾.md "wikilink")，中国女子[羽毛球运动员](../Page/羽毛球.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[劉亦菲](../Page/劉亦菲.md "wikilink")，美籍華人新生代女演員、歌手
      - [布蕾克·萊芙莉](../Page/布蕾克·萊芙莉.md "wikilink")，美國女演員
      - [艾米·麦克唐纳](../Page/艾米·麦克唐纳.md "wikilink")，[蘇格蘭女歌手](../Page/蘇格蘭.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[黃光熙](../Page/黃光熙.md "wikilink")，韓國组合[ZE:A成員](../Page/ZE:A.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[黃美棋](../Page/黃美棋.md "wikilink")，香港演员
  - [1989年](../Page/1989年.md "wikilink")：[吳海昕](../Page/吳海昕.md "wikilink")，香港女演員
  - [1990年](../Page/1990年.md "wikilink")：[神咲詩織](../Page/神咲詩織.md "wikilink")，日本AV女优
  - [1992年](../Page/1992年.md "wikilink")：[夏燒雅](../Page/夏燒雅.md "wikilink")，日本歌手
      - [里卡多·罗德里格斯](../Page/里卡多·罗德里格斯.md "wikilink")，[瑞士職業足球員](../Page/瑞士.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[邕聖祐](../Page/邕聖祐.md "wikilink")，韓國團體Wanna
    One成員

員

  - [2001年](../Page/2001年.md "wikilink")：[洪薏晴](../Page/洪薏晴.md "wikilink")，中興外文系花（暫定）

## 逝世

  - [383年](../Page/383年.md "wikilink")：[格拉蒂安](../Page/格拉蒂安.md "wikilink")，[罗马帝国皇帝](../Page/罗马帝国.md "wikilink")（[359年出生](../Page/359年.md "wikilink")）
  - [1227年](../Page/1227年.md "wikilink")：[元太祖](../Page/元太祖.md "wikilink")[成吉思汗](../Page/成吉思汗.md "wikilink")，[蒙古帝国大汗](../Page/蒙古帝国.md "wikilink")（[1162年出生](../Page/1162年.md "wikilink")）
  - [1270年](../Page/1270年.md "wikilink")：[路易九世](../Page/路易九世_\(法蘭西\).md "wikilink")，[法國](../Page/法國.md "wikilink")[卡佩王朝第](../Page/卡佩王朝.md "wikilink")9任國王（[1214年出生](../Page/1214年.md "wikilink")）
  - [1776年](../Page/1776年.md "wikilink")：[大衛·休謨](../Page/大衛·休謨.md "wikilink")，[蘇格蘭的](../Page/蘇格蘭.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")、[經濟學家](../Page/經濟學家.md "wikilink")、和[歷史學家](../Page/歷史學家.md "wikilink")（[1711年出生](../Page/1711年.md "wikilink")）
  - [1867年](../Page/1867年.md "wikilink")：[麥可·法拉第](../Page/麥可·法拉第.md "wikilink")，[英国物理学家和化学家](../Page/英国.md "wikilink")（[1791年出生](../Page/1791年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：[弗里德里希·威廉·尼采](../Page/弗里德里希·威廉·尼采.md "wikilink")，[德国思想家](../Page/德国.md "wikilink")（[1844年出生](../Page/1844年.md "wikilink")）
      - [黑田清隆](../Page/黑田清隆.md "wikilink")，[日本第](../Page/日本.md "wikilink")2任[首相](../Page/日本首相.md "wikilink")（[1840年出生](../Page/1840年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[亨利·贝克勒](../Page/亨利·贝克勒.md "wikilink")，法国物理学家，[天然放射性的发现者](../Page/天然放射性.md "wikilink")（[1852年出生](../Page/1852年.md "wikilink")）
  - [1912年](../Page/1912年.md "wikilink")：[冯如](../Page/冯如.md "wikilink")，[中国飞行家](../Page/中国.md "wikilink")（[1884年出生](../Page/1884年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[王尔琢](../Page/王尔琢.md "wikilink")，红军将领（[1903年出生](../Page/1903年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[蔡炳炎](../Page/蔡炳炎.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")([1902年出生](../Page/1902年.md "wikilink"))
  - [1938年](../Page/1938年.md "wikilink")：[亚历山大·伊凡诺维奇·库普林](../Page/亚历山大·伊凡诺维奇·库普林.md "wikilink")，[俄国作家](../Page/俄国.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[陈果夫](../Page/陈果夫.md "wikilink")，[中國國民黨元老](../Page/中國國民黨.md "wikilink")（[1892年出生](../Page/1892年.md "wikilink")）
  - [1956年](../Page/1956年.md "wikilink")：[阿爾弗雷德·金賽](../Page/阿爾弗雷德·金賽.md "wikilink")，知名[生物學家及人類性學科學研究者](../Page/生物學.md "wikilink")（[1894年出生](../Page/1894年.md "wikilink")）
  - [1985年](../Page/1985年.md "wikilink")：[萨曼莎·史密斯](../Page/萨曼莎·史密斯.md "wikilink")，美国和平活动者（[1972年出生](../Page/1972年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[戴厚英](../Page/戴厚英.md "wikilink")，中國女作家（[1938年出生](../Page/1938年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[雷蒙·巴爾](../Page/雷蒙·巴爾.md "wikilink")，法國政治家和經濟學家（[1924年出生](../Page/1924年.md "wikilink")）
      - [雷·瓊斯](../Page/雷·瓊斯.md "wikilink")，[英格蘭職業](../Page/英格蘭.md "wikilink")[足球運動員](../Page/足球.md "wikilink")（[1988年出生](../Page/1988年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[愛德華·甘迺迪](../Page/愛德華·甘迺迪.md "wikilink")，[美國民主黨籍政治家](../Page/美國.md "wikilink")（[1932年出生](../Page/1932年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[尼尔·阿姆斯特朗](../Page/尼尔·阿姆斯特朗.md "wikilink")，美国航天员，第一位登上[月球的人](../Page/月球.md "wikilink")（[1930年出生](../Page/1930年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[劉復之](../Page/劉復之.md "wikilink")，原中共中央顧問委員會委員，最高人民檢察院原檢察長（[1917年出生](../Page/1917年.md "wikilink")）
  - [2018年](../Page/2018年.md "wikilink")：[約翰·麥凱恩](../Page/約翰·麥凱恩.md "wikilink")，美国[共和党政治家](../Page/共和党_\(美国\).md "wikilink")（[1936年出生](../Page/1936年.md "wikilink")）
  - 2018年：[麻生美代子](../Page/麻生美代子.md "wikilink")，[日本](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")（[1926年出生](../Page/1926年.md "wikilink")）

## 节假日和习俗

  - [乌拉圭](../Page/乌拉圭.md "wikilink")：独立日

## 參考資料