**羌语支**是[藏緬語族的一个支系](../Page/藏緬語族.md "wikilink")，有几十万人使用。羌语支民族在史籍中常被称作戎、羌氐。

## 音系沿革

羌语支的很多语言与藏缅语族其他语言相比，元音和辅音系统十分复杂，有些语言有诸如 st-, rm-, gz-
之类的复辅音、三重辅音达将近200个（[尔龚语](../Page/尔龚语.md "wikilink")）。但也有的语言完全没有复辅音（中部[尔苏语](../Page/尔苏语.md "wikilink")、南部[羌语龙溪土话等](../Page/羌语.md "wikilink")），有的语言将复辅音处理为独立的弱化音节（部分[土家语方言](../Page/土家语.md "wikilink")）。但无论元音辅音结构简化程度如何，羌语支语言的声调系统较其他藏缅语不发达。

## 語法沿革

此外羌语支语言在语法上具有高度共通性，均为高度综合的语言，且普遍具有独特的同源的动词方位趋向变位等各种名词动词形容词屈折形式。

## 参考资料

  - 聂鸿音（2003）著《中国少数民族语言》，北京，语文出版社 ISBN 978-7-80184-780-5
  - Bradley, David (1997). Tibeto-Burman languages and classification.
    In D. Bradley (Ed.), *Papers in South East Asian linguistics:
    Tibeto-Burman languages of the Himalayas* (No. 14, pp. 1–71).
    Canberra: Pacific Linguistics.
  - Sun, Hongkai.(1983). The nationality languages in the six valleys
    and their language branches. *Yunnan Minzuxuebao*, *3*, 99-273.
    (Written in Chinese).
  - Sun Hongkai ([Academy of Social Sciences of
    China](../Page/Academy_of_Social_Sciences_\(China\).md "wikilink")
    Institute of Nationality Studies) (1990). "[Languages of the Ethnic
    Corridor in Western
    Sichuan](https://www.webcitation.org/6Z7xfgeaU?url=http://sealang.net/sala/archives/pdf8/sun1990languages.pdf)"
    (" ([Archive](https://www.webcitation.org/6Z7xfgeaU)). *[Linguistics
    of the Tibeto-Burman
    Area](../Page/Linguistics_of_the_Tibeto-Burman_Area.md "wikilink")*,
    13(1), 1-31. English translation by Jackson T.-S. Sun ([University
    of California
    Berkeley](../Page/University_of_California_Berkeley.md "wikilink")
    and [Academia Sinica](../Page/Academia_Sinica.md "wikilink")).

## 外部链接

  - [有关嘉绒语、木雅语和西夏文的网站](https://web.archive.org/web/20061126032803/http://xiang.free.fr/)
  - [The Qiang Language and Culture Web
    Site](https://web.archive.org/web/20130817151216/http://victoria.linguistlist.org/~lapolla/qiang/qiangmain.html)
  - [Site on Qiangic languages (French and
    Chinese)](https://web.archive.org/web/20061126032940/http://xiang.free.fr/indexfr.htm)

[羌语支](../Category/羌语支.md "wikilink")