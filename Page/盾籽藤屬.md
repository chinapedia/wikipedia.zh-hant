**盾籽藤屬**（[拉丁語](../Page/拉丁語.md "wikilink")：*Habropetalum*）為[雙鉤葉科單型的](../Page/雙鉤葉科.md "wikilink")[植物](../Page/植物.md "wikilink")[屬](../Page/屬.md "wikilink")，相當於本屬唯一種類──**道氏盾籽藤**（[學名](../Page/學名.md "wikilink")：*Habropetalum
dawei*）。本種為[藤本植物](../Page/攀緣植物.md "wikilink")，具有毒性且是[獅子山所](../Page/獅子山.md "wikilink")[特有](../Page/特有種.md "wikilink")。\[1\]生長在[雨林的草叢中](../Page/雨林.md "wikilink")。本種如同其他的熱帶藤本植物，需要高溫與高度的濕氣。

## 注釋

## 參考文獻

<div class="references-small">

  - Airy Shaw: "On the Dioncophyllaceae, a remarkable new family of
    flowering plants." Kew Bulletin 327–347, 1951

</div>

## 外部連結

  - [Aluka - Isotype of Dioncophyllum dawei
    Hutch.\&Dalziel](http://www.aluka.org/action/showMetadata?doi=10.5555/AL.AP.SPECIMEN.B%2010%200154971&pgs=)
    道氏盾籽藤的臘葉標本

[Category:双钩叶科](../Category/双钩叶科.md "wikilink")

1.  J. P. M. Brenan: *Some Aspects of the Phytogeography of Tropical
    Africa*, in: Annals of the Missouri Botanical Garden, Vol. 65, No.
    2, p. 460, 1978