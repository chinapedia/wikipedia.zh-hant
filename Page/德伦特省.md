**德伦特省**（）是位于[荷兰东北部的一个省](../Page/荷兰.md "wikilink")，它东邻[德国](../Page/德国.md "wikilink")、南與[上艾瑟爾省接壤](../Page/上艾瑟爾省.md "wikilink")、西接[弗里斯兰省](../Page/弗里斯兰省.md "wikilink")、北靠[格罗宁根省](../Page/格罗宁根省.md "wikilink")，首府[阿森](../Page/阿森.md "wikilink")，2005年人口483,173人。

## 歷史

德倫特，不同於其他省份，一直是人口稀少的農業地帶。荷蘭其他地區未將這裡視為與其他地方一樣的「領土」，而是「荒地」。這片「荒地」從史前時代就有人定居，雖然人數不多。最明顯的證據是在約前3500年左右建造的[支石墓](../Page/支石墓.md "wikilink")。荷蘭54座支石墓中有53座在德倫特省，其中大部分集中在德倫特東北部。第一次提到德倫特的文件是在820年，稱為「Pago
Treanth」（意為“德倫特行政區”）。另外在德倫特檔案館中，1024年至1025年間的文件也曾提到「德倫特郡」。

在經歷[烏得勒支主教長期的管轄之後](../Page/烏得勒支主教.md "wikilink")，德倫特在16世紀成為[查理五世的控制範圍](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")。在[七省聯合共和國成立之後](../Page/荷蘭共和國.md "wikilink")，德倫特成為共和國一部分，但一直到1796年1月1日才獲得省的地位。

[二戰爆發後不久](../Page/二戰.md "wikilink")，荷蘭政府在[韦斯特博克](../Page/韦斯特博克.md "wikilink")（Westerbork）附近建立了一座營區以容納德國（猶太）難民。諷刺的是，德軍在二戰期間將營區作為集中營（命名為[韦斯特博克集中营](../Page/韦斯特博克集中营.md "wikilink")）。許多荷蘭[猶太人](../Page/猶太人.md "wikilink")、[辛提人](../Page/辛提人.md "wikilink")（Sinti）、[羅姆人](../Page/羅姆人.md "wikilink")、反抗的戰士和政治對手都監禁于此，之後再移送至德國和波蘭境內的集中營。[安妮·弗兰克就是坐上韦斯特博克的末班車離開荷蘭](../Page/安妮·弗兰克.md "wikilink")。

## 政治

省議會（Provinciale
Staten）為居民選舉選出，目前有41席，主席則由[女王專員擔任](../Page/女王專員.md "wikilink")。女王專員是女王和[荷蘭內閣所指派](../Page/荷蘭內閣.md "wikilink")。[荷蘭工黨為德倫特省議會第一大黨](../Page/荷蘭工黨.md "wikilink")，擁有13席。省的日常事務由「執行委員會」（Gedeputeerde
Staten），同樣由女王專員擔任主席。

## 市鎮

由於1990年代的重組，德倫特省的市鎮數目減少至12個。因此現在多數市鎮是由數個城鎮和村莊所組成：

<table>
<tbody>
<tr class="odd">
<td><ol>
<li><a href="../Page/阿恩亨泽.md" title="wikilink">阿恩亨泽</a>（Aa en Hunze）</li>
<li><a href="../Page/阿森.md" title="wikilink">阿森</a>（Assen）</li>
<li><a href="../Page/博赫尔－奥多伦.md" title="wikilink">博赫尔－奥多伦</a>（Borger-Odoorn）</li>
<li><a href="../Page/库福尔登.md" title="wikilink">库福尔登</a>（Coevorden）</li>
<li><a href="../Page/埃门.md" title="wikilink">埃门</a>（Emmen）</li>
<li><a href="../Page/霍赫芬.md" title="wikilink">霍赫芬</a>（Hoogeveen）</li>
</ol></td>
<td><ol start="7">
<li><a href="../Page/梅珀尔.md" title="wikilink">梅珀尔</a>（Meppel）</li>
<li><a href="../Page/中德伦特.md" title="wikilink">中德伦特</a>（Midden-Drenthe）</li>
<li><a href="../Page/诺登费尔德.md" title="wikilink">诺登费尔德</a>（Noordenveld）</li>
<li><a href="../Page/蒂纳洛.md" title="wikilink">蒂纳洛</a>（Tynaarlo）</li>
<li><a href="../Page/韦斯特费尔德.md" title="wikilink">韦斯特费尔德</a>（Westerveld）</li>
<li><a href="../Page/德沃登.md" title="wikilink">德沃登</a>（De Wolden）</li>
</ol></td>
<td><p><imagemap> <a href="File:DrentheNumbered.png%7Cright">File:DrentheNumbered.png|right</a></p>
<p>poly 105 76 130 66 141 82 145 84 143 88 131 95 121 104 111 99 <a href="../Page/博赫爾-奧多恩.md" title="wikilink">博赫爾-奧多恩</a> poly 151 100 148 142 123 139 123 131 119 124 121 116 121 106 134 97 143 91 <a href="../Page/埃門.md" title="wikilink">埃門</a> poly 93 135 94 125 89 127 89 123 87 123 96 110 101 105 105 95 114 102 121 104 118 112 119 117 117 121 120 130 122 136 108 139 105 143 103 139 <a href="../Page/庫佛登.md" title="wikilink">庫佛登</a> poly 81 139 79 136 73 133 71 132 70 129 67 129 67 119 64 116 68 116 70 109 77 115 83 118 86 123 95 127 94 134 <a href="../Page/霍赫芬.md" title="wikilink">霍赫芬</a> poly 47 129 39 129 38 124 49 116 52 110 63 113 69 110 65 117 63 127 70 130 71 134 82 136 77 140 80 148 72 145 66 145 62 148 52 134 48 137 <a href="../Page/德沃登.md" title="wikilink">德沃登</a> poly 25 121 31 117 35 122 40 122 38 129 46 131 46 138 41 133 34 135 30 130 28 121 <a href="../Page/梅波爾.md" title="wikilink">梅波爾</a> poly 27 97 37 92 43 82 53 86 58 82 55 88 58 92 65 92 66 98 71 100 69 113 65 111 59 114 55 110 50 110 38 122 33 115 39 112 <a href="../Page/威斯特菲爾德.md" title="wikilink">威斯特菲爾德</a> poly 65 69 74 64 72 72 77 80 80 75 87 82 99 85 99 93 106 95 99 107 96 107 87 126 85 120 83 117 77 117 76 111 71 110 72 101 68 98 69 91 62 90 56 85 60 83 <a href="../Page/中德倫特.md" title="wikilink">中德倫特</a> poly 109 43 117 53 130 65 104 73 104 86 107 94 100 92 100 87 89 82 84 75 97 58 94 54 99 50 95 47 101 49 106 49 <a href="../Page/阿恩亨澤.md" title="wikilink">阿恩亨澤</a> poly 75 57 81 61 82 52 87 52 87 57 92 61 85 74 81 75 77 79 72 70 76 67 <a href="../Page/阿森.md" title="wikilink">阿森</a> poly 74 22 91 41 102 39 102 37 109 45 101 51 97 48 94 55 92 61 89 51 83 51 78 60 75 55 77 47 73 43 74 39 80 37 <a href="../Page/蒂纳洛.md" title="wikilink">蒂纳洛</a> poly 65 68 58 57 58 49 51 46 55 46 57 42 60 34 64 26 68 25 69 21 73 22 79 36 71 38 71 43 76 49 75 64 <a href="../Page/諾德菲爾德.md" title="wikilink">諾德菲爾德</a></p>
<p>desc bottom-left </imagemap></p></td>
</tr>
</tbody>
</table>

## 經濟

農業是德倫特省的重要產業，工業區大多設立在城市周邊。該地的恬靜也吸引了越來越多的遊客。

德倫特以荷蘭的「單車省」而聞名。境內有數百公里的單車道，穿越了森林、荒原，或沿著運河，而沿路的許多城鎮或鄉村則提供茶點。

## 方言

德倫特有著獨有的[荷語](../Page/荷語.md "wikilink")[方言德倫特語](../Page/方言.md "wikilink")，但各個村落都有些許差異。所有方言都屬於下薩克森語系。

## 外部連結

  - [官方網站](http://www.drenthe.nl)
  - [Drentse
    Fietsvierdaagse](http://www.fiets4daagse.nl/)，荷蘭最有名的單車活動「Fietsvierdaagse」。

[\*](../Category/德伦特省.md "wikilink")