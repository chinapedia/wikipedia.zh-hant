**周布站**（）是一個位於[日本](../Page/日本.md "wikilink")[島根縣](../Page/島根縣.md "wikilink")[濱田市治和町的車站](../Page/濱田市.md "wikilink")。唯一通過的路線是[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）的[山陰本線](../Page/山陰本線.md "wikilink")。

## 車站構造

益田方向左側的[側式月台](../Page/側式月台.md "wikilink")1面1線的[地面車站](../Page/地面車站.md "wikilink")，是[無人站](../Page/無人站.md "wikilink")。由於直到2001年3月為止仍是相對式月台，在車站的北側有廢棄的月台遺跡。

## 車站周邊

  - 周布[郵便局](../Page/郵便局.md "wikilink")
  - 周布古墳
  - [石見交通濱田營業所](../Page/石見交通.md "wikilink")
  - [國道9号](../Page/國道9號_\(日本\).md "wikilink")

## 历史

  - 1922年([大正](../Page/大正.md "wikilink")11年)3月10日　開業

## 相鄰車站

※快速「」（下行所有列車、上行只限1班停靠）的相鄰停車站參見列車條目。

  - 西日本旅客鐵道

    山陰本線

      -

        －**周布**－

## 外部連結

  - [JR西日本（周布站）](http://www.jr-odekake.net/eki/top.php?id=0640766)

[Category:島根縣鐵路車站](../Category/島根縣鐵路車站.md "wikilink")
[Fu](../Category/日本鐵路車站_Su.md "wikilink")
[Category:山陰本線車站](../Category/山陰本線車站.md "wikilink")
[Category:濱田市](../Category/濱田市.md "wikilink")
[Category:1922年启用的铁路车站](../Category/1922年启用的铁路车站.md "wikilink")