**汕头市实验学校**是[汕头市教育局直属的一所](../Page/汕头市.md "wikilink")[公办学校](../Page/公办学校.md "wikilink")，于1999年7月作为[九年一贯制的](../Page/九年一贯制.md "wikilink")[国有民办学校开办](../Page/国有民办学校.md "wikilink")，于2010年秋季改制为[公办学校](../Page/公办学校.md "wikilink")。

## 学校介绍

汕头市实验学校曾是[汕头市第一所](../Page/汕头市.md "wikilink")[国有民办学校](../Page/国有民办学校.md "wikilink")\[1\]，于2010年改制为公办学校\[2\]。学校座落于汕头市[龙湖区](../Page/龙湖区.md "wikilink")35街区丰泽庄，临近[星湖公园](../Page/星湖公园.md "wikilink")。学校为[九年一贯制](../Page/九年一贯制.md "wikilink")，其中[小学六年制](../Page/小学.md "wikilink")，[初中三年制](../Page/初中.md "wikilink")。学校占地60亩，建筑面积33000平方米。有教学楼4座，实验楼2座，办公楼、[图书馆](../Page/图书馆.md "wikilink")、文体楼、[礼堂各](../Page/礼堂.md "wikilink")1座，[操场](../Page/操场.md "wikilink")2个。\[3\]\[4\]

学校现任[校长陈克文](../Page/校长.md "wikilink")，副校长杨应春。[校训是](../Page/校训.md "wikilink")“自强不息，厚德载物”\[5\]

## 学校历史

  - 1999年，汕头市实验学校由其前身，外马路270号的汕头市实验[小学迁至现址](../Page/小学.md "wikilink")，并更名为汕头市实验学校。改制为国有民办学校，扩大办学规模，由6年制小学扩大为九年一贯制学校。\[6\]但在大多数情况下，学校被认为是于1999年“创建”。
  - 1999年底，成为汕头市首批“[绿色学校](../Page/绿色学校.md "wikilink")”。\[7\]
  - 2001年，成为省级绿色学校。\[8\]
  - 2001年，成为首批“[广东省现代教育技术实验学校](../Page/广东省.md "wikilink")”。\[9\]
  - 2002年，被列为广东省[课程改革试验区](../Page/课程改革.md "wikilink")，在汕头市区内最先实施“新课程”改革。
  - 2002年5月20日至24日，首界教育教学开放周。\[10\]
  - 2004年，晋升[广东省一级学校](../Page/广东省一级学校.md "wikilink")。\[11\]
  - 2006年，被授予“广东省人民防空先进单位”。\[12\]
  - 2008年，学校通过广东省一级学校复评。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [汕头市实验学校](https://web.archive.org/web/20081219095734/http://stssyxx.stedu.net/)

[Category:汕头中等教育](../Category/汕头中等教育.md "wikilink")
[Category:1999年創建的教育機構](../Category/1999年創建的教育機構.md "wikilink")

1.
2.
3.  [基础建设](http://stssyxx.stedu.net/syxx/portal/index.files/newsShow.asp?res_id=1987)
4.  [汕头市实验学校校园平面图](http://stssyxx.stedu.net/syxx/portal/index.files/pinmian/zhuyue.asp)
5.  [汕头市实验学校校训](http://stssyxx.stedu.net/syxx/portal/index.files/xuexiao/zhuyue.asp)
6.  [汕头市环保局-创绿工作-省级绿色学校-汕头市实验学校详细信息](http://www.stepb.gov.cn/news_detail.asp?newsid=1284)
7.  [汕头市实验学校详细信息](http://www.stepb.gov.cn/news_detail.asp?newsid=1284)，汕头市环保局
8.  [汕头市省级绿色学校名单](http://www.stepb.gov.cn/news_detail.asp?newsid=2333)，汕头市环保局
9.  [汕头市实验学校简介](http://www.stedu.net/read_school.aspx?xxbm=440507098304)
    汕头市教育局
10. [市实验学校开放周给学生搭起施展才华的舞台](http://www.stnews.com.cn/stwb/20020524/GB/stwb%5E1020%5E%5ETqc01007.htm)，汕头特区晚报，2002年5月24日
11. [汕头市实验学校晋升“省一级”](http://www.dahuawang.com/localnews/showlocal.asp?no=44065)
    ，大华网-本地新闻，摘自《汕头都市报》，2004年5月12日
12. [汕头市实验学校被授予“广东省人民防空先进单位”](http://www.stedu.net/read_news.aspx?id=4612)，汕头教育信息网