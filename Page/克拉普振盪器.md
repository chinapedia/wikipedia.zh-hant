**克拉普振盪器**是一種由[電晶體](../Page/電晶體.md "wikilink")（或[真空管](../Page/真空管.md "wikilink")）與一組正[反馈電路組成的](../Page/反馈.md "wikilink")[振盪器](../Page/振盪器.md "wikilink")。

它由在1948年发表。\[1\] 根据Vačkář的文献，\[2\]
此类振荡器是几个发明者自主开发的，其中由[Gouriet开发的那个振荡器从](../Page/Geoffrey_G._Gouriet.md "wikilink")1938年就一直在[BBC工作](../Page/BBC.md "wikilink")。

[Clapp_oscillator.png](https://zh.wikipedia.org/wiki/File:Clapp_oscillator.png "fig:Clapp_oscillator.png")
根據下圖可知此電路使用1個[電感與](../Page/電感.md "wikilink")3個[電容](../Page/電容.md "wikilink")，其中的2個電容（C1及C2）用來分壓以決定施於電晶體輸入端的回授電壓。克拉普振盪器是以[Colpitts振盪器為基礎](../Page/Colpitts振盪器.md "wikilink")，在原本的電感前多串聯1個電容。下圖中使用[場效電晶體的振壓器電路的振盪頻率](../Page/場效電晶體.md "wikilink")（單位為赫茲）是：

\(f_0 = {1 \over 2\pi}
       \sqrt{ {1 \over L}
              \left(   {1 \over C_0}
                     + {1 \over C_1}
                     + {1 \over C_2}
              \right)} \ .\)

在設計使用1個可變電容調整頻率的[變頻振盪器](../Page/變頻振盪器.md "wikilink")（Variable Frequency
Oscillator，VFO）時，一般會選擇克拉普振盪器。若使用Colpitts振器設計的變頻振盪器，分壓部分的電路就得要使用1個可變電容（C1或C2）以改變回授電壓的大小，但改變回授電壓有時也使得Colpitts振盪器無法調敕到預期的振盪頻率範圍。若使用克拉普振盪器，因為分壓電路使用固定的電容，而且改在電感前串連1個可變電容（C0）所以不會出現這樣的問題。

## 参见

  -
## 参考文献

<references/>

  - Ulrich L. Rohde, Ajay K. Poddar, Georg Böck "The Design of Modern
    Microwave Oscillators for Wireless Applications ", John Wiley &
    Sons, New York, NY, May, 2005, ISBN 0-471-72342-8.
  - George Vendelin, Anthony M. Pavio, Ulrich L. Rohde " Microwave
    Circuit Design Using Linear and Nonlinear Techniques ", John Wiley &
    Sons, New York, NY, May, 2005, ISBN 0-471-41479-4.
  - A. Grebennikov, RF and Microwave Transistor Oscillator Design. Wiley
    2007. ISBN 978-0-470-02535-2.

## 外部链接

  - [EE 322/322L Wireless Communication
    Electronics](https://web.archive.org/web/20070222114903/http://whites.sdsmt.edu/classes/ee322/)
    —Lecture \#24: Oscillators. Clapp oscillator. VFO startup

[Category:振荡器](../Category/振荡器.md "wikilink")

1.  J. K. Clapp, "An inductance-capacitance oscillator of unusual
    frequency stability", [Proc. IRE](../Page/Proc._IRE.md "wikilink"),
    vol. 367, pp. 356-358, Mar. 1948.
2.  Jiri Vackar, LC Oscillators and their Frequency Stability, TESLA
    Report 1949, ch. 4,