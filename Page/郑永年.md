**郑永年**（[汉语拼音](../Page/汉语拼音.md "wikilink")：Zhèng, Yǒngnián；Cheng,
Yung-nien，曾用[笔名](../Page/笔名.md "wikilink")：Mong Xiong；
），[浙江](../Page/浙江.md "wikilink")[余姚人](../Page/余姚.md "wikilink")，[中国政治](../Page/中国政治.md "wikilink")、[国际关系与社会问题专家](../Page/国际关系.md "wikilink")。现任[新加坡国立大学东亚研究所所长](../Page/新加坡国立大学.md "wikilink")、[英国](../Page/英国.md "wikilink")[诺丁汉大学](../Page/诺丁汉大学.md "wikilink")[当代中国学学院](../Page/诺丁汉大学当代中国学学院.md "wikilink")[中国政策研究所教授级研究员](../Page/诺丁汉大学中国政策研究所.md "wikilink")（Professorial
Fellows）、[华南理工大学公共政策研究院学术委员会主席](../Page/华南理工大学.md "wikilink")。\[1\]

## 简历

郑永年是[浙江省](../Page/浙江省.md "wikilink")[余姚县](../Page/余姚县.md "wikilink")（今[宁波市辖](../Page/宁波市.md "wikilink")[余姚市](../Page/余姚市.md "wikilink")）人\[2\]。郑永年于[北京大学获得](../Page/北京大学.md "wikilink")[国际关系学学士](../Page/国际关系学.md "wikilink")（1985年）、[政治科学硕士](../Page/政治科学.md "wikilink")（1988年），并留校任教。1989年郑永年曾参与了[六四运动](../Page/六四运动.md "wikilink")，与当时的青年学生一起占领[天安门广场](../Page/天安门广场.md "wikilink")，向中国政府[请愿](../Page/请愿.md "wikilink")。后赴美国留学获[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")[政治科学](../Page/政治科学.md "wikilink")[硕士和](../Page/硕士.md "wikilink")[博士](../Page/博士.md "wikilink")（1995年）。其主要兴趣或研究领域为[民族主义与](../Page/民族主义.md "wikilink")[国际关系](../Page/国际关系.md "wikilink")；[东亚](../Page/东亚.md "wikilink")[国际和](../Page/国际.md "wikilink")[地区](../Page/地区.md "wikilink")[安全](../Page/安全.md "wikilink")；中国的[外交政策](../Page/外交政策.md "wikilink")；[全球化](../Page/全球化.md "wikilink")、国家转型和[社会正义](../Page/社会正义.md "wikilink")；技术变革与[政治转型](../Page/政治转型.md "wikilink")；社会运动与[民主化](../Page/民主化.md "wikilink")；比较中央地方关系；[中国政治](../Page/中国政治.md "wikilink")\[3\]。

## 論點

郑永年在进行学术研究的同时，经常在报刊及其它媒体发表自己的评论。他在1997年到2006年担任过[香港](../Page/香港.md "wikilink")《[信报](../Page/信报.md "wikilink")》的[专栏作家](../Page/专栏作家.md "wikilink")，2004年开始在[新加坡](../Page/新加坡.md "wikilink")《[联合早报](../Page/联合早报.md "wikilink")》撰写专栏。此前还在[中国中央电视台播出的大型](../Page/中国中央电视台.md "wikilink")[纪录片](../Page/纪录片.md "wikilink")《[大国崛起](../Page/大国崛起.md "wikilink")》中作为专家接受过采访，他在该片中的一段评论引起很多人的共鸣，引述如下：

  -
    “所有的以前的国家，崛起中的大国，都是因为它内部的国家制度的健全。所谓的一个国家的外部的崛起，实际上是它内部力量的一个外延。”（第十二集
    大道行思，原音）“
    在一个内部，自己的国家制度还没有健全的情况下，就很难成为一个大国。即使成为一个大国呢，是不是sustainable，不是可持续的。”（第三集
    走向现代，原音）

郑永年是親歷天安門學運後隨著遊歷多國的經驗，轉而支持中國共產黨發展價值觀的新左派代表人物。鄭永年是[亨廷顿](../Page/塞缪尔·P·亨廷顿.md "wikilink")[文明衝突論和](../Page/文明衝突論.md "wikilink")[福山](../Page/弗朗西斯·福山.md "wikilink")[歷史終結論的極力反對者](../Page/歷史終結論.md "wikilink")，認為此兩者的觀點偏頗和短視；他並認為，所謂「[普世價值](../Page/普世價值.md "wikilink")」是帶有侵略性的政治口號，並在世界各地釀成災難後無法收拾\[4\]。其《中國模式經驗與挑戰》一書2010年成為[中共中央黨校指定教材](../Page/中共中央黨校.md "wikilink")。

其核心觀點認為民主有多重不同版本與定義，並非歐美有單一定義權並且認為自己的制度就是最後完美模式，並認為不是民主[法治而是法治民主](../Page/法治.md "wikilink")，法治在前，因為先有民主之後產生法治的國家在世界上連一個範例也沒有，比如許多拉美、非洲国家[二战后就有投票](../Page/二战.md "wikilink")、多党制、宪政、自由[媒体但依然國力衰弱法律混亂](../Page/媒体.md "wikilink")，而日本[明治维新](../Page/明治维新.md "wikilink")，新加坡的李光耀，甚至歐美自己都是先有法治觀念深入社會和經濟適當發展解決溫飽，之後再轉型由君主帝制到民主，反而較上軌道，但他同時也認為這個表像有欺騙性，因為多党制民主已經遇到新時代困境也提不出解決方案，在可見之年有可能逐漸被歷史放棄。\[5\]因為多黨型民主在一定条件下是好的，如果条件變了，這種民主也会是非常糟糕的一个政体。

問題在於現在西方宣傳的民主定义非常窄，就是普选。然而[人均GDP是](../Page/人均GDP.md "wikilink")500美元时可以投票，1万[美元也可以投票](../Page/美元.md "wikilink")，但意义是不同的：在前者大部分公民没有受过教育又貧困，社会水平很低，选票不值钱，有时一块肥皂、一包香烟就收买了，很多民主災難就是由此成長出來，將國家拖入惡循環。而歐美高所得、高教育基礎上的民主也走入困境，連福山都承認：以前的民主是相互争论以达到更理性的目标，现在则变成了一种「vetocracy」互相否定的制度为反对而反对\[6\]，就像觀看球賽的情緒，只求自己支持對象的勝利。

[2016年中華民國總統選舉](../Page/2016年中華民國總統選舉.md "wikilink")，支持[台灣獨立的](../Page/台灣獨立.md "wikilink")[民主進步黨上台](../Page/民主進步黨.md "wikilink")，郑永年表示：[中國國民黨無法處理經濟導致了民進黨獲得機會](../Page/中國國民黨.md "wikilink")，但民進黨更無法處理經濟、同時還要面對大陸發動的經濟戰爭，所以其實這不是哪一黨的錯，是西方式民主本身造成的矛盾怪區；民主的本質其實就是一句話「小政府」，但面對新自由式資本主義發展進入弊病期，小政府沒有權力工具解決大問題，只能不斷地讓人失望和「換人會更好」的死路中迴圈，這是制度性困境，和哪一黨坐在那位置上無關，全世界多國都落入這泥潭而找不到解藥。\[7\]

郑永年認為，中國目前執行的制度其實是傳統[儒家為主](../Page/儒家.md "wikilink")、[法家為輔的儒法制度變形](../Page/法家.md "wikilink")，[皇权是垄断的](../Page/皇权.md "wikilink")，但「官权」是向全社会开放的，只要[科举考试考得好的话都可以進入官場](../Page/科举.md "wikilink")。只是传统的皇权转变成现在的党权，传统的皇权是以个人家族血統傳承驅動，黨則是一個[意識形態組織](../Page/意識形態.md "wikilink")，較大程度壓減了血統效果；[皇帝不再需要姓同一個姓](../Page/皇帝.md "wikilink")，可以從較大人群範圍內遴選；也就是透過「开放的一党制」達成內部多元性的競爭。尤其2012年[習近平就任](../Page/習近平.md "wikilink")[中共中央總書記後成立了多個領域的](../Page/中共中央總書記.md "wikilink")[領導小組吸收基層民意](../Page/領導小組.md "wikilink")，本質是一種內部多黨制競爭辯論，但為了免除無意義無限期的辯論內耗，爭議點最後由最高領導人來下最終裁決，決定後大家就口徑一致對社會推行。但西方的外部多元制，不同意見就各自立黨變成多黨，裁決由四年一次的[普選來總裁判](../Page/普選.md "wikilink")，反而相對粗糙，且容易產生多年的內耗空轉。

鄭永年《未來30年》一書認為，中國總體戰略路徑已經清晰了，[毛泽东的](../Page/毛泽东.md "wikilink")30年主任务是分裂和混亂国家的整合；[邓小平的](../Page/邓小平.md "wikilink")30年基本是在搞经济建设，把物質生產力總量衝上來，把一个穷国变成全世界第二大经济体。那下一個30年就是社会和制度理論成形自己的一條特色道路。这也需要30多年的时间驗證經濟上採共產加市場混合制，政治上採一黨加內部多元混合制，吸收過去百年來世界兩大陣營之所長來融合改進；若是能成功，則中國將形成自己的整套意識形態體系傲立於世界上，達成中華民族的偉大復興。\[8\]

## 研究·出版物（部分）

  - 《論中央-地方關係 :
    中國制度轉型中的一個軸心問題》（与[吴国光合著](../Page/吴国光.md "wikilink")，香港：[牛津大學出版社](../Page/牛津大學出版社.md "wikilink")，1995）ISBN
    0195873564
  - *Discovering Chinese nationalism in China : modernization, identity,
    and international relations* （Cambridge and New York: Cambridge
    University Press, 1999）ISBN 0521641802
  - *Will China become democratic? : elite, class and regime transition*
    (Singapore, London and New York : Eastern Universities Press , 2004)
    ISBN 9812103538
  - *Globalization and state transformation in China* (Cambridge and New
    York: Cambridge University Press, 2004) ISBN 0521830508
  - *De Facto Federalism in China : Reforms and Dynamics of
    Central-Local Relations* (Singapore and New Jersey : World
    Scientific, 2007 ) ISBN 9812700161
  - "Technological Empowerment: The Internet, State, and Society in
    China" (Stanford University Press, 2007) ISBN 0804757372
  - *The Chinese Communist Party as Organizational Emperor: Culture,
    Reproduction, and Transformation* (London: Routledge, December 2009)
    ISBN 9780415559638

## 参考文献

## 外部链接

  - [个人简介@新加坡国立大学东亚研究所](https://research.nus.edu.sg/eai/staff/current-staff/#1510652846225-c593c731-ed31)
  - [郑永年博客](http://www.caogen.com/blog/index.aspx?ID=66)
  - [郑永年：司法独立恰恰是为了执政党的最高利益](http://news.ifeng.com/mainland/detail_2012_01/26/12146405_0.shtml).
    凤凰视频.

## 参见

  - [格雷厄姆·艾利森](../Page/格雷厄姆·艾利森.md "wikilink")
  - [龍安志](../Page/龍安志.md "wikilink")

{{-}}

[Z](../Category/1962年出生.md "wikilink")
[Z](../Category/余姚人.md "wikilink")
[Z](../Category/中国政治学家.md "wikilink")
[Z](../Category/新加坡学者.md "wikilink")
[Z](../Category/北京大学校友.md "wikilink")
[Z](../Category/普林斯顿大学校友.md "wikilink")
[Category:六四人物](../Category/六四人物.md "wikilink")
[YONG](../Category/郑姓.md "wikilink")
[Category:新加坡华人](../Category/新加坡华人.md "wikilink")

1.  [郑永年：TPP与中美关系的前景](http://www.apdnews.com/news/27579.html)
    ，亚太日报，2013年6月5日

2.

3.

4.

5.
6.
7.  [選後專訪郑永年看兩岸](http://star.news.sohu.com/20160116/n434742312.shtml)

8.