**塞吉歐·李昂尼**（，）為知名[意大利](../Page/意大利.md "wikilink")[導演](../Page/導演.md "wikilink")、[編劇](../Page/編劇.md "wikilink")、[製片人](../Page/製片人.md "wikilink")，自1964年起連續三年連拍三部[「鏢客系列」](../Page/鏢客三部曲.md "wikilink")[義大利式西部片而大受世界影迷歡迎](../Page/義大利式西部片.md "wikilink")。

雖然李昂尼只親自導演過七部電影（其餘創作身份大多為[編劇](../Page/編劇.md "wikilink")、[製片人與](../Page/製片人.md "wikilink")[副導演](../Page/副導演.md "wikilink")），但他對世界影壇的影響極為深遠，特別是[動作片與](../Page/動作片.md "wikilink")[犯罪片類型電影及導演](../Page/犯罪片.md "wikilink")，著名[香港導演](../Page/香港.md "wikilink")[吳宇森與](../Page/吳宇森.md "wikilink")[美國導演](../Page/美國.md "wikilink")[馬丁·史柯西斯即是](../Page/馬丁·史柯西斯.md "wikilink")。

李昂尼於1989年完成初步企劃拍攝[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[納粹包圍蘇聯](../Page/納粹.md "wikilink")[列寧格勒](../Page/列寧格勒.md "wikilink")900天的宏偉史詩[戰爭片時過勞逝世](../Page/戰爭片.md "wikilink")。

## 執導作品

### 处女作

  - 1961年：《》（The Colossus of
    Rhodes），影片取材自[罗得岛太阳神铜像毁灭的传说故事](../Page/罗得岛太阳神铜像.md "wikilink")，是一部讲述罗德岛义士在希腊英雄达里奥的带领下击退腓尼基人入侵的史诗巨作。

### [鏢客三部曲](../Page/鏢客三部曲.md "wikilink")

  - 1964年：《[荒野大鏢客](../Page/荒野大鏢客.md "wikilink")》（A Fistful of Dollars）
  - 1965年：《[黃昏雙鏢客](../Page/黃昏雙鏢客.md "wikilink")》（For a Few Dollars More）
  - 1966年：《[黃金三鏢客](../Page/黃金三鏢客.md "wikilink")》（The Good, the Bad and
    the Ugly）

### [美國三部曲](../Page/美國三部曲.md "wikilink")

  - 1968年：《[西部往事](../Page/西部往事.md "wikilink")》（Once Upon a Time in the
    West）
  - 1971年：《》（A Fistful of Dynamite / Once Upon a Time... the Revolution）
  - 1984年：《[美国往事](../Page/美国往事.md "wikilink")》（Once Upon a Time in
    America）

## 外部連結

  -
  -
[Category:美國導演](../Category/美國導演.md "wikilink")
[Category:意大利导演](../Category/意大利导演.md "wikilink")
[Category:意大利编剧](../Category/意大利编剧.md "wikilink")
[Category:罗马人](../Category/罗马人.md "wikilink")
[Category:男性编剧](../Category/男性编剧.md "wikilink")