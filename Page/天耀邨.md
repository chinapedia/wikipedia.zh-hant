[Tin_Yiu_Shopping_Centre_Outside_Access.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Shopping_Centre_Outside_Access.jpg "fig:Tin_Yiu_Shopping_Centre_Outside_Access.jpg")
[Tin_Yiu_Market.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Market.jpg "fig:Tin_Yiu_Market.jpg")
[Tin_Yiu_Estate_Abandoned_Pond_2008.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Estate_Abandoned_Pond_2008.jpg "fig:Tin_Yiu_Estate_Abandoned_Pond_2008.jpg")
[Tin_Yiu_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Estate_Basketball_Court.jpg "fig:Tin_Yiu_Estate_Basketball_Court.jpg")
[Tin_Yiu_Estate_Tennis_Court.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Estate_Tennis_Court.jpg "fig:Tin_Yiu_Estate_Tennis_Court.jpg")
[Tin_Yiu_Estate_Badminton_Court_and_Playground.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Estate_Badminton_Court_and_Playground.jpg "fig:Tin_Yiu_Estate_Badminton_Court_and_Playground.jpg")
[Tin_Yiu_Estate_Gym_Zone.jpg](https://zh.wikipedia.org/wiki/File:Tin_Yiu_Estate_Gym_Zone.jpg "fig:Tin_Yiu_Estate_Gym_Zone.jpg")
**天耀邨**（英文：**Tin Yiu
Estate**）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")[天水圍新市鎮南部](../Page/天水圍新市鎮.md "wikilink")，於1992年4月至1993年初分階段入-{伙}-，為天水圍新市鎮的第一個公共屋邨，現由[雅居物業管理有限公司負責屋邨管理](../Page/雅居物業管理有限公司.md "wikilink")。

**天祐苑**（英文：**Tin Yau
Court**）是位於天耀邨內的一個[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，並有全港首三座建成的和諧式樓宇，於1992年落成，首次推出時售價為294,200
- 594,100港元。

## 屋邨資料

天耀邨是天水圍新市鎮內的第一個公共屋邨，為了建立全市鎮屋邨良好設計榜樣，政府發展時加入很多康樂和便民設施，包括多層[停車場](../Page/停車場.md "wikilink")、[天耀廣場](../Page/天耀廣場.md "wikilink")、露天劇場、四個[籃球場](../Page/籃球場.md "wikilink")、四個[羽毛球場](../Page/羽毛球.md "wikilink")、兩個[排球場](../Page/排球.md "wikilink")、兩個[足球場及兒童遊樂場](../Page/足球.md "wikilink")，兒童遊樂場內更設有小型[迷宮](../Page/迷宮.md "wikilink")。其他設施包括[議員辦事處](../Page/議員.md "wikilink")、元朗區租約事務管理處、屋邨管理處（均位於耀豐樓附近）、社區中心和學生[自修室](../Page/自修室.md "wikilink")。

屋邨內設有有蓋行人通道連接各座樓宇和商場。屋邨花園內曾設有水景設施連接天耀商場至天耀邨一帶，唯有關設備因安全問題已停用，直至水景設施旁邊加設了圍欄，防止居民誤闖後才重用。

### 樓宇

| 屋邨/屋苑  | 樓宇名稱                                | 樓宇類型                              | 落成年份\[1\] |
| ------ | ----------------------------------- | --------------------------------- | --------- |
| 天耀(一)邨 | 耀逸樓                                 | [Y3型](../Page/Y型大廈.md "wikilink") | 1992      |
| 耀民樓    |                                     |                                   |           |
| 耀富樓    |                                     |                                   |           |
| 耀興樓    | Y4型                                 |                                   |           |
| 耀康樓    |                                     |                                   |           |
| 耀盛樓    | [和諧二型](../Page/和諧式大廈.md "wikilink") | 1993                              |           |
| 天耀(二)邨 | 耀澤樓                                 |                                   |           |
| 耀豐樓    |                                     |                                   |           |
| 耀泰樓    |                                     |                                   |           |
| 耀華樓    |                                     |                                   |           |
| 耀昌樓    |                                     |                                   |           |
| 耀隆樓    |                                     |                                   |           |
| 天祐苑    | 祐康閣                                 | 和諧一型                              | 1992      |
| 祐寧閣    |                                     |                                   |           |
| 祐泰閣    |                                     |                                   |           |

### 設施

  - 天耀社區中心
  - [天耀廣場](../Page/天耀廣場.md "wikilink")

#### 天耀街市

街市於1992年開業，共有67個檔攤。

至2016年，領展計劃於年初將街市改建為商場，居民批評領展漠視居民需要，組成「天水圍天耀街市關注組」，發起遊行，要求領展擱置計劃，保留天耀街市方便居民購物。\[2\]有區議員批評領展取消濕貨區等同取消街市，不合乎屋邨街市的基本要求，對此表示強烈反對。又指從[天慈邨步行到](../Page/天慈邨.md "wikilink")[天盛苑需時](../Page/天盛苑.md "wikilink")，輕鐵亦要「兜大圈」才到，設立特價區難以彌補居民損失。他又認為送餸服務是不合理及荒謬，建議[食環署在天水圍增設街市](../Page/食環署.md "wikilink")，增加競爭及市民選擇。領展表示會為區內長者提供免費送餸服務，由天盛街市送到天耀廣場。\[3\]部份檔販留守最後一刻，批評政府漠視民意，縱容領展欺壓基層商戶和街坊，天耀街市關注組等團體，於2月27日到街市舉辦悼念會，最終街市在2月29日正式關閉。\[4\]

### 社區服務

  - [博愛醫院吳鴻茂紀念家庭多元智能中心](https://www.facebook.com/POHTYFMI)
  - [香港青年協會賽馬會天耀青年空間](https://ty.hkfyg.org.hk)
  - [扶康會天耀之家](https://www.fuhong.org/PageInfo.aspx?md=20000&cid=283)
  - [基督教香港信義會天水圍青少年綜合服務中心](../Page/基督教香港信義會天水圍青少年綜合服務中心.md "wikilink")
  - [新家園協會賽馬會天水圍服務中心](../Page/新家園協會賽馬會天水圍服務中心.md "wikilink")
  - [天水圍居民服務協會基金會曾樹和社區服務中心](../Page/天水圍居民服務協會基金會曾樹和社區服務中心.md "wikilink")
  - [元朗大會堂梁學樵夫人老人中心](../Page/元朗大會堂梁學樵夫人老人中心.md "wikilink")
  - [元朗大會堂天耀就業中心](../Page/元朗大會堂天耀就業中心.md "wikilink")
  - [社會福利署南天水圍社會保障辦事處](../Page/社會福利署南天水圍社會保障辦事處.md "wikilink")
  - [社會福利署天水圍綜合家庭服務中心](../Page/社會福利署天水圍綜合家庭服務中心.md "wikilink")

### 教育設施

天耀邨內設有不少教育設施，中學包括[天水圍官立中學及](../Page/天水圍官立中學.md "wikilink")[元朗信義中學](../Page/元朗信義中學.md "wikilink")。小學包括[伊利沙伯中學舊生會小學](../Page/伊利沙伯中學舊生會小學.md "wikilink")、[香港潮陽小學](../Page/香港潮陽小學.md "wikilink")。[中華基督教會方潤華小學已於](../Page/中華基督教會方潤華小學.md "wikilink")2012年9月1日起停辦，校舍曾一度空置，到2014年12月由中華基督教會香港區會申請重開學校。\[5\]幼稚園包括[東華三院黃朱惠芬幼稚園](http://www.twghwcwfkg.edu.hk)、[元朗商會幼稚園](http://www.ylmakg.edu.hk)、[仁愛堂吳黃鳳英幼稚園](http://ppe.yot.org.hk/dn03/index.php)、[宣道會陳李詠貞紀念幼稚園](http://www.caclwtmk.edu.hk/)、[禮賢會元朗幼兒園](http://ylc.ppe.rhenish.org)、[元朗公立中學校友會劉良驤紀念幼稚園](http://www.ylallsk.edu.hk)。

## 事件

  - 2007年10月14日凌晨四時，天耀邨耀豐樓2402室，一名有精神病紀錄的36歲女子，將12歲女兒和9歲兒子，用尼龍繩綑綁擲下樓後，再跳樓自殺，三人均證實當場死亡\[6\]。天耀邨居民其後舉行燭光晚會，悼念三名死者，事件令人再次關注天水圍社會問題。

<!-- end list -->

  - 2014年9月3日早晨10時許，天耀邨耀逸樓24樓單位閉門失火，由火勢猛烈，火舌更波及樓上及樓下單位，消防出動兩條喉及三隊煙帽隊灌救，經2小時灌救後將火救熄，逾300街坊一度疏散暫避。火警中3個單位嚴重焚毀，上下3個樓層走廊及大廈外牆亦嚴重熏黑。而24樓首先起火的單位內更加是滿目瘡痍，全屋物品包括傢具以及房間間隔燒成黑炭，大門亦燒剩半截，惟放於近門位置神樓上的一尊笑佛，則完好無缺，仍然潔白亮麗。\[7\]

<!-- end list -->

  - 2015年7月26日，天耀邨耀民樓32樓一單位揭發雙屍命案，84歲姓招老翁及其61歲姓女婿倒斃屋內，屍體發脹發臭疑死去多天，但估計死亡時間相差一至兩天。消息指出，招伯為肇事的天耀邨耀民樓單位戶主，不良於行及患有多種老人病，平日需坐輪椅及掛尿袋，沒有自理能力。警方調查後相信，從牀上墮地並非致命原因，不排除是病發失救猝死，懷疑招伯欲施以拯救時，亦不慎跌在地上，因無法報警及呼救無援，其後活活餓死。\[8\]

## 圖集

Tin Yiu Estate Theatre (full view and blue sky).jpg|露天劇場 Tin Yiu Estate
Volleyball Court.jpg|排球場 Tin Yiu Estate Table Tennis Zone.jpg|乒乓球檯 HK
Tin Yiu Estate View1.jpg|天耀邨近景 Tin Yiu Estate Garden.jpg|天耀邨花園 Tin Yiu
Estate Covered Walkway.jpg|有蓋行人路

Tin Yiu Estate Playground (2).jpg|兒童遊樂場（2） Tin Yiu Estate Playground
(3).jpg|兒童遊樂場（3） Tin Yiu Estate Playground (4).jpg|兒童遊樂場（4） Tin Yiu
Estate Pebble Walking Trail.jpg|卵石路步行徑 Tin Yiu Estate Night.JPG|天耀邨夜景
Chiu Yang Primary School of Hong Kong (blue sky).jpg|香港潮陽小學

## 交通

<div class="NavFrame collapsed" style="color: gray; background-color: #F3F9FF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #F3F9FF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #F3F9FF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")(C、D出口)
  - [輕鐵](../Page/香港輕鐵.md "wikilink")
      - [天水圍站](../Page/天水圍站_\(輕鐵\).md "wikilink")、[天慈站](../Page/天慈站.md "wikilink")
          - [705、706](../Page/香港輕鐵705、706綫.md "wikilink")、[751、751P](../Page/香港輕鐵751、751P綫.md "wikilink")
      - [天耀站](../Page/天耀站.md "wikilink")
          - 705、706、[761P](../Page/香港輕鐵761P綫.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - **[天耀巴士總站](../Page/天耀巴士總站.md "wikilink")**

<!-- end list -->

  - **天耀邨耀盛樓**巴士站

<!-- end list -->

  - **天耀邨耀民樓**巴士站

<!-- end list -->

  - **[天祐苑](../Page/天祐苑.md "wikilink")**巴士站

<!-- end list -->

  - **天耀邨耀豐樓**巴士站

</div>

</div>

## 參見

  - [天耀廣場](../Page/天耀廣場.md "wikilink")

## 參考

## 外部連結

  - [房委會天耀(一)邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2699)
  - [房委會天耀(二)邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2700)
  - [房委會天祐苑資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3029)

[en:Public housing estates in Tin Shui Wai\#Tin Yiu
Estate](../Page/en:Public_housing_estates_in_Tin_Shui_Wai#Tin_Yiu_Estate.md "wikilink")

[Category:天水圍](../Category/天水圍.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.
2.
3.  [天耀街市撤濕貨 增長者送餸服務
    《香港經濟日報》 2015-10-02](http://topick.hket.com/article/876476/%E5%A4%A9%E8%80%80%E8%A1%97%E5%B8%82%E6%92%A4%E6%BF%95%E8%B2%A8%20%20%E5%A2%9E%E9%95%B7%E8%80%85%E9%80%81%E9%A4%B8%E6%9C%8D%E5%8B%99)
4.  [天耀街市29號結業　商戶留守最後一刻：1分鐘都唔會蝕俾領展
    《香港01》 2016-02-27](http://www.hk01.com/%E6%B8%AF%E8%81%9E/9253/%E5%A4%A9%E8%80%80%E8%A1%97%E5%B8%8229%E8%99%9F%E7%B5%90%E6%A5%AD-%E5%95%86%E6%88%B6%E7%95%99%E5%AE%88%E6%9C%80%E5%BE%8C%E4%B8%80%E5%88%BB-1%E5%88%86%E9%90%98%E9%83%BD%E5%94%94%E6%9C%83%E8%9D%95%E4%BF%BE%E9%A0%98%E5%B1%95)
5.  [天水圍被殺小學「復活」《蘋果日報》 2015-01-17](http://hk.apple.nextmedia.com/news/art/20150117/19005751)
6.  [天水圍有精神病紀錄母親和一對子女墮樓死亡](http://www.hkatvnews.com/v3/share_out/_content/2007/10/14/atvnews_109861.html)
    [亞洲電視](../Page/亞洲電視.md "wikilink")
7.
8.