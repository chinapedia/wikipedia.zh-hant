**昭和通**（）是一條位於[日本](../Page/日本.md "wikilink")[東京都的主要道路](../Page/東京都.md "wikilink")，由位於[港區](../Page/港區_\(東京都\).md "wikilink")[新橋](../Page/新橋_\(東京都港區\).md "wikilink")、[第一京濱與](../Page/第一京濱.md "wikilink")[銀座中央通](../Page/中央通_\(東京都\).md "wikilink")（二者皆屬[國道15號一部份](../Page/國道15號.md "wikilink")）相接的[交叉路口開始](../Page/交叉路口.md "wikilink")，直至位於[台東區](../Page/台東區.md "wikilink")、與[明治通相交會的的大關橫丁交叉路口為止](../Page/明治通_\(東京都\).md "wikilink")，道路的南段屬[東京都道316號日本橋芝浦大森線的一部份](../Page/東京都道316號日本橋芝浦大森線.md "wikilink")，北段則是[國道4號的一部份](../Page/國道4號_\(日本\).md "wikilink")。

## 概要

由[外堀通的](../Page/外堀通.md "wikilink")[東京都](../Page/東京都.md "wikilink")[港區](../Page/港區_\(東京都\).md "wikilink")[新橋交差點開始](../Page/新橋站_\(日本\).md "wikilink")，往[芝浦埠頭](../Page/芝浦埠頭.md "wikilink")、[彩虹橋方向經](../Page/彩虹橋_\(東京\).md "wikilink")[海岸通分岐](../Page/海岸通.md "wikilink")，與[新大橋通連接](../Page/新大橋通.md "wikilink")。[第一京濱方向的昭和通設直通](../Page/第一京濱.md "wikilink")[地下道](../Page/地下道.md "wikilink")，與[江戶橋](../Page/江戶橋.md "wikilink")、日本橋本町連接。這地下道用以緩和[海岸通](../Page/海岸通.md "wikilink")、[第一京濱](../Page/第一京濱.md "wikilink")、[外堀通](../Page/外堀通.md "wikilink")（[霞關](../Page/霞關.md "wikilink")、[六本木等](../Page/六本木.md "wikilink")）各方向來的交通，為連接[國道15號與](../Page/國道15號.md "wikilink")[國道4號的交通作出了重大貢獻](../Page/國道4號.md "wikilink")。另外，地下道與各地下停車場間有連接，各地下停車場使用者可以進入地下道。[江戶橋有與](../Page/江戶橋.md "wikilink")[首都高速道路都心環狀線的](../Page/首都高速道路都心環狀線.md "wikilink")[IC](../Page/IC.md "wikilink")，[江戶橋](../Page/江戶橋.md "wikilink")～[入谷間於](../Page/入谷站_\(東京都\).md "wikilink")[首都高速道路1號上野線的高架下行走](../Page/首都高速道路1號上野線.md "wikilink")。[江戶通交差點之後就屬](../Page/江戶通.md "wikilink")[國道4號](../Page/國道4號.md "wikilink")。直進至與[明治通交差的](../Page/明治通_\(東京都\).md "wikilink")[東京都](../Page/東京都.md "wikilink")[台東區](../Page/台東區.md "wikilink")[三輪交差點](../Page/三輪站.md "wikilink")，之後連接[日光街道](../Page/日光街道.md "wikilink")。

## 通過的自治體

  - [東京都](../Page/東京都.md "wikilink")
      - [港區](../Page/港區_\(東京都\).md "wikilink")
      - [中央區](../Page/中央區_\(東京都\).md "wikilink")
      - [千代田區](../Page/千代田區.md "wikilink")
      - [台東區](../Page/台東區.md "wikilink")

## 主要橋梁

  - [和泉橋](../Page/和泉橋.md "wikilink")（[神田川](../Page/神田川.md "wikilink")）
  - [江戶橋](../Page/江戶橋.md "wikilink")（[日本橋川](../Page/日本橋川.md "wikilink")）

## 與昭和通連接、交差的主要道路

  - [第一京濱](../Page/第一京濱.md "wikilink")、[銀座中央通](../Page/銀座中央通.md "wikilink")（[國道15號](../Page/國道15號.md "wikilink")）
  - [外堀通](../Page/東京都道405號外濠環狀線.md "wikilink")（[東京都道405號外濠環狀線](../Page/東京都道405號外濠環狀線.md "wikilink")、第一京濱開始設地下道）
  - [海岸通](../Page/海岸通.md "wikilink")（[東京都道316號日本橋芝浦大森線](../Page/東京都道316號日本橋芝浦大森線.md "wikilink")、第一京濱開始設地下道）
  - [晴海通](../Page/晴海通.md "wikilink")（[東京都道304號日比谷豊洲埠頭東雲町線](../Page/東京都道304號日比谷豊洲埠頭東雲町線.md "wikilink")、設地下道）
  - [鍛冶橋通](../Page/鍛冶橋通.md "wikilink")（設地下道）
  - [八重洲通](../Page/八重洲通.md "wikilink")（[東京都道408號八重洲寶町線終點](../Page/東京都道408號八重洲寶町線.md "wikilink")、設地下道）
  - [永代通](../Page/永代通.md "wikilink")（[東京都道、千葉県道10號東京浦安線](../Page/東京都道、千葉県道10號東京浦安線.md "wikilink")、設地下道）
  - [江戶通](../Page/江戶通.md "wikilink")（[國道4號](../Page/國道4號.md "wikilink")、[國道6號起點](../Page/國道6號.md "wikilink")、設地下道）
  - [神田金物屋通](../Page/神田金物屋通.md "wikilink")（[神田站方向單線通行](../Page/神田站_\(東京都\).md "wikilink")）
  - [神田警察通](../Page/神田警察通.md "wikilink")（[淺草橋](../Page/淺草橋.md "wikilink")、[兩國橋方向單線通行](../Page/兩國橋.md "wikilink")）
  - [靖國通](../Page/靖國通.md "wikilink")（[東京都道302號新宿兩國線](../Page/東京都道302號新宿兩國線.md "wikilink")）
  - [水天宮通](../Page/水天宮通.md "wikilink")（單方向通行出口）
  - [藏前橋通](../Page/藏前橋通.md "wikilink")（[東京都道315號御徒町小岩線](../Page/東京都道315號御徒町小岩線.md "wikilink")）
  - [春日通](../Page/春日通.md "wikilink")（[東京都道453號本鄉龜戶線](../Page/東京都道453號本鄉龜戶線.md "wikilink")）
  - [中央通](../Page/中央通_\(東京都\).md "wikilink")（[東京都道437號秋葉原雜司谷線](../Page/東京都道437號秋葉原雜司谷線.md "wikilink")）
  - [淺草通](../Page/淺草通.md "wikilink")（[東京都道463號上野月島線](../Page/東京都道463號上野月島線.md "wikilink")）
  - [清洲橋通](../Page/清洲橋通.md "wikilink")
  - [言問通](../Page/言問通.md "wikilink")（[東京都道319號環狀三號線](../Page/東京都道319號環狀三號線.md "wikilink")）
  - [國際通](../Page/東京都道462號藏前三輪線.md "wikilink")（[東京都道462號藏前三輪線](../Page/東京都道462號藏前三輪線.md "wikilink")）
  - [明治通](../Page/明治通_\(東京都\).md "wikilink")（[東京都道306號王子千住南砂町線](../Page/東京都道306號王子千住南砂町線.md "wikilink")）
  - [日光街道](../Page/日光街道.md "wikilink")（[國道4號](../Page/國道4號.md "wikilink")）

## 與昭和通連接的車站、路線

[Hibiyasen_Minowa_eki_1.jpg](https://zh.wikipedia.org/wiki/File:Hibiyasen_Minowa_eki_1.jpg "fig:Hibiyasen_Minowa_eki_1.jpg")\]\]
※港區、中央區内於地下有[都營地下鐵淺草線通過](../Page/都營地下鐵淺草線.md "wikilink")。
※中央區、千代田區、台東區内於地下有[東京地下鐵日比谷線通過](../Page/東京地下鐵日比谷線.md "wikilink")。

  - [新橋站](../Page/新橋站_\(日本\).md "wikilink")（[JR各線](../Page/東日本旅客鐵道.md "wikilink")、[東京地下鐵銀座線](../Page/東京地下鐵銀座線.md "wikilink")、[都營淺草線](../Page/都營地下鐵淺草線.md "wikilink")、[百合鷗](../Page/百合鷗_\(鐵道會社\).md "wikilink")）
  - [東銀座站](../Page/東銀座站.md "wikilink")（[東京地下鐵日比谷線各線](../Page/東京地下鐵日比谷線.md "wikilink")、[都營淺草線](../Page/都營地下鐵淺草線.md "wikilink")）
  - [寶町站](../Page/寶町站_\(東京都\).md "wikilink")（[都營淺草線](../Page/都營地下鐵淺草線.md "wikilink")）
  - [日本橋站](../Page/日本桥站_\(东京都\).md "wikilink")（[都營淺草線](../Page/都營地下鐵淺草線.md "wikilink")）
  - [新日本橋站](../Page/新日本橋站.md "wikilink")（[JR](../Page/東日本旅客鐵道.md "wikilink")、[總武線快速](../Page/總武快速線.md "wikilink")）
  - [秋葉原站](../Page/秋葉原站.md "wikilink")（[JR](../Page/東日本旅客鐵道.md "wikilink")、[東京地下鐵日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")、[首都圈新都市鐵道筑波快線](../Page/首都圈新都市鐵道筑波快線.md "wikilink")）
  - [上野御徒町站](../Page/上野御徒町站.md "wikilink")（[都營大江戶線](../Page/都營地下鐵大江戶線.md "wikilink")）
  - [御徒町站](../Page/御徒町站.md "wikilink")（[JR](../Page/東日本旅客鐵道.md "wikilink")）
  - [仲御徒町站](../Page/仲御徒町站.md "wikilink")（[東京地下鐵日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")）
  - [上野站](../Page/上野站.md "wikilink")（[JR各線](../Page/東日本旅客鐵道.md "wikilink")、[東京地下鐵銀座線](../Page/東京地下鐵銀座線.md "wikilink")、[東京地下鐵日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")）
  - [入谷站](../Page/入谷站_\(東京都\).md "wikilink")（[東京地下鐵日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")）
  - [三之輪站](../Page/三之輪站.md "wikilink")（[東京地下鐵日比谷線](../Page/東京地下鐵日比谷線.md "wikilink")）

## 相關條目

  -
  - [關東地方道路列表](../Page/關東地方道路列表.md "wikilink")

[Category:東京都道路](../Category/東京都道路.md "wikilink")