[缩略图](https://zh.wikipedia.org/wiki/File:US_nuclear_strike_map.svg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Atomic_bombing_of_Japan.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:DF-5B_intercontinental_ballistic_missiles_during_2015_China_Victory_Day_parade.jpg "fig:缩略图")戰略核彈\]\]


**核戰爭**、**核子戰爭**，简称**核战**，是指使用[核子武器作戰的](../Page/核子武器.md "wikilink")[战争](../Page/战争.md "wikilink")。在[核武器历史上](../Page/核武器历史.md "wikilink")，核武器實際在戰爭上使用，只有在[第二次世界大戰中的](../Page/第二次世界大戰.md "wikilink")[廣島與長崎原子彈爆炸](../Page/廣島與長崎原子彈爆炸.md "wikilink")。

## 歷史

核戰爭的歷史可由[第二次世界大戰中期說起](../Page/第二次世界大戰.md "wikilink")，當時的[納粹德國在](../Page/納粹德國.md "wikilink")[歐洲的行動令](../Page/歐洲.md "wikilink")[美國人擔心](../Page/美國.md "wikilink")[納粹德國將攻下](../Page/納粹德國.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，最終入侵[美國本土](../Page/美國.md "wikilink")，於是積極發展[大規模殺傷力武器來控制](../Page/大規模殺傷力武器.md "wikilink")[納粹德國](../Page/納粹德國.md "wikilink")，並在[曼哈頓計劃中發展出](../Page/曼哈頓計劃.md "wikilink")[原子彈](../Page/原子彈.md "wikilink")。最後雖然沒有用於[納粹德國](../Page/納粹德國.md "wikilink")，但在大戰末期為避免美軍在攻佔日本本土時出現重大傷亡而在[日本的](../Page/日本.md "wikilink")[广岛及](../Page/廣島市.md "wikilink")[长崎投下](../Page/长崎市.md "wikilink")[原子彈](../Page/原子彈.md "wikilink")，令[日本投降以結束戰爭](../Page/日本.md "wikilink")。

### 第二次世界大戰

核子武器在二戰末期被首次使用，[美國為了迫使](../Page/美國.md "wikilink")[日本投降](../Page/日本投降.md "wikilink")，儘快結束二戰，於是在[廣島和](../Page/廣島.md "wikilink")[長崎投下原子彈](../Page/長崎.md "wikilink")，這是核武器第一次，也是惟一一次在戰爭中使用。

#### 廣島、長崎原子彈爆炸

[美國已可以掌握](../Page/美國.md "wikilink")[核武器技術](../Page/核武器.md "wikilink")，為令[日本投降](../Page/日本.md "wikilink")，於[广岛及](../Page/廣島市.md "wikilink")[長崎投下](../Page/长崎市.md "wikilink")[原子彈](../Page/原子彈.md "wikilink")，兩市幾乎完全被毀滅。這使世人驚覺[大規模殺傷力武器在](../Page/大規模殺傷力武器.md "wikilink")[戰爭中的作用](../Page/戰爭.md "wikilink")，讓很多國家紛紛發展大規模殺傷力武器，以使自己躋身至「世界強國」之中。

### 冷戰時期

[核武器由兩個敵對的](../Page/核武器.md "wikilink")[超級大國](../Page/超級大國.md "wikilink")——[美國和](../Page/美國.md "wikilink")[蘇聯擁有絕大多數的](../Page/蘇聯.md "wikilink")[核武器](../Page/核武器.md "wikilink")，讓核戰爭有爆發的可能。

#### 古巴導彈危機

[古巴導彈危機是](../Page/古巴導彈危機.md "wikilink")1962年[冷戰時期在](../Page/冷戰.md "wikilink")[美國](../Page/美國.md "wikilink")、[蘇聯與](../Page/蘇聯.md "wikilink")[古巴之間爆發的一場極其嚴重的政治](../Page/古巴.md "wikilink")、軍事危機。事件爆發的原因是[蘇聯在](../Page/蘇聯.md "wikilink")[古巴部署](../Page/古巴.md "wikilink")[飛彈](../Page/飛彈.md "wikilink")。這個事件被看作是[冷戰的頂峰和轉折點](../Page/冷戰.md "wikilink")。歷史上人類從未如此近距離的站在核戰爭爆發的邊緣。

#### 珍宝岛事件

[珍宝岛事件是指](../Page/珍宝岛事件.md "wikilink")[中华人民共和国和](../Page/中华人民共和国.md "wikilink")[苏联因珍宝岛的归属问题于](../Page/苏联.md "wikilink")1969年3月間在岛上发生的武装冲突。发生珍宝岛事件后，苏聯明確发出先发制人和核打擊的威脅，這也是人類史上第二次全面核戰爭威脅。

#### 核不擴散條約

人們在1962年的[古巴导弹危机后開始极为重视](../Page/古巴导弹危机.md "wikilink")[核武器帶來的後果](../Page/核武器.md "wikilink")，並擔心核戰爭一旦爆發整個世界都會被毀滅，於是[核武大國](../Page/核武.md "wikilink")[美國](../Page/美國.md "wikilink")、[蘇聯和当时另外一个擁有](../Page/蘇聯.md "wikilink")[核武的國家](../Page/核武.md "wikilink")——[英國在](../Page/英國.md "wikilink")[古巴导弹危机后便开始积极进行协商制定](../Page/古巴导弹危机.md "wikilink")《核不扩散条约》相关细节的讨论，到1968年美国、苏联和英国便签署核不扩散条约，当时与美国和苏联两个[超级大国同时都处在敌视对立状态的](../Page/超级大国.md "wikilink")[毛泽东领导下的](../Page/毛泽东.md "wikilink")[中国没有签署此条约](../Page/中华人民共和国.md "wikilink")，直到1992年[邓小平和](../Page/邓小平.md "wikilink")[江泽民才同意签署此条约](../Page/江泽民.md "wikilink")。与同在1964年首次核试验成功的中国一样，长期坚持在美国和苏联的对立中保持独立自主的[戴高乐主义的](../Page/戴高乐主义.md "wikilink")[法国也在](../Page/法国.md "wikilink")1992年才签署了核不扩散条约。\[1\]

### 現代

1990年代，全世界大多数国家都簽署了[核不擴散條約](../Page/核不擴散條約.md "wikilink")，[美国](../Page/美国.md "wikilink")、[俄罗斯等大國都放慢](../Page/俄罗斯.md "wikilink")[核武器的發展腳步](../Page/核武器.md "wikilink")，并且宣布暂停本国的核试验，但[印度](../Page/印度.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[北韓等國家卻依然積極發展](../Page/北韓.md "wikilink")[核武器](../Page/核武器.md "wikilink")。北韓在2003年退出了核不扩散条约\[2\]，并且在2006、2009、2013和2016年四次成功进行核试验。\[3\]

## 核戰的後果

[核武的](../Page/核武.md "wikilink")[輻射](../Page/輻射.md "wikilink")、[衝擊波和產生放射性造成殺傷力和破壞作用以及所造成大面積污染](../Page/衝擊波.md "wikilink")，可以使整個人類世界被它毀滅。

核爆后的尘埃带来“[核冬天](../Page/核冬天.md "wikilink")”及[核子末日](../Page/核子末日.md "wikilink")，最终將可能导致人类的灭亡。

## 参见

  - [一次打击](../Page/一次打击.md "wikilink")
  - [二次打击](../Page/二次打击.md "wikilink")
  - [核武器](../Page/核武器.md "wikilink")
  - [大规模杀伤性武器](../Page/大规模杀伤性武器.md "wikilink")
  - [二戰對日本的核攻擊](../Page/廣島與長崎原子彈爆炸.md "wikilink")（[廣島](../Page/广岛市原子弹爆炸.md "wikilink")、[長崎](../Page/長崎市原子彈爆炸.md "wikilink")）
  - [冷戰](../Page/冷戰.md "wikilink")
  - [古巴導彈危機](../Page/古巴導彈危機.md "wikilink")
  - [核不擴散條約](../Page/核不擴散條約.md "wikilink")
  - [279工程](../Page/279工程.md "wikilink")
  - [末日之钟](../Page/末日之鐘.md "wikilink")（一个标示出世界受核武威胁的程度的虚构时钟）

## 參考文獻

[核战争](../Category/核战争.md "wikilink")
[Category:核武器](../Category/核武器.md "wikilink")

1.  [国际原子能机构：不扩散核武器条约](http://www.caea.gov.cn/n16/n83690/83827.html)
2.  [2003年1月10日朝鲜民主主义人民共和国政府就朝鲜退出核不扩散条约所作的声明](http://news.xinhuanet.com/ziliao/2003-11/11/content_1172362.htm)
3.  [凤凰网：朝鲜第三次核试验专题](http://news.ifeng.com/world/special/chaoxiansanheshi/)