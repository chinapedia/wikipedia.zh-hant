**亞歷山大·恩斯特·阿爾弗雷德·赫爾曼·馮·法肯豪森**[男爵](../Page/男爵.md "wikilink")（，）是一位德國陸軍將領，最高軍階為[步兵上將](../Page/步兵上將_\(德國\).md "wikilink")，同時亦是一位積極的[反納粹分子](../Page/德國抵抗運動.md "wikilink")。

法肯豪森於1935年3月接替[漢斯·馮·塞克特](../Page/漢斯·馮·塞克特.md "wikilink")，任[中國](../Page/中華民國.md "wikilink")[蔣中正的](../Page/蔣中正.md "wikilink")[德國軍事顧問團最後一任領導人](../Page/德國軍事顧問團.md "wikilink")，為中國在抗日戰爭初期戰事貢獻頗多，直到1938年德國納粹政府決定聯日棄華而奉召回國，擔任第4軍區司令。1940年在德國佔領比利時與法國後，法肯豪森被任命為比利時佔領區軍政府司令官，期間與[納粹親衛隊和](../Page/納粹親衛隊.md "wikilink")[蓋世太保暗中抗衡](../Page/蓋世太保.md "wikilink")，採取相對溫和的佔領政策，但對比國境內發生的屠殺和猶太人遭受的暴行負有主要責任。法肯豪森於1944年因被捲入「[720密謀案](../Page/720密謀案.md "wikilink")」而被解除職務和逮捕，先後輾轉於多個集中營，最後被美軍俘虜。

戰後法肯豪森因比利時佔領軍領導人而被以戰爭罪犯起訴，被宣判12年有期徒刑，但僅關押3週後即獲釋。1966年7月31日法肯豪森於[萊茵蘭-普法爾茨的](../Page/萊茵蘭-普法爾茨.md "wikilink")[拿騷去世](../Page/拿骚_\(德国\).md "wikilink")。

## 生平

[Falkenhausen_Nagoya_1911.jpg](https://zh.wikipedia.org/wiki/File:Falkenhausen_Nagoya_1911.jpg "fig:Falkenhausen_Nagoya_1911.jpg")第33步兵團時的法肯豪森留影。\]\]
亞歷山大·馮·法肯豪森出生於1878年10月29日[德意志帝國](../Page/德意志帝國.md "wikilink")[西里西亞省](../Page/西里西亞省.md "wikilink")的一個普魯士貴族家庭里，父親為亞歷山大（Alexander，1844—1909），母親為伊麗莎白·舒勒·馮·辛登（Elisabeth
Schuler von Senden，1853—1936），他的叔叔曾於一戰末期的1917年至1918年期間擔任。

法肯豪森於[布雷斯勞接受](../Page/布雷斯勞.md "wikilink")[文理中學的初級教育](../Page/文理中學.md "wikilink")，後於1897年3月13日加入德國陸軍，並以少尉軍階服役於「」。1900年至1901年，法肯豪森被編入第3東亞步兵團（3.
Ostasiatischen
Infanterie-Regiment），跟隨[阿爾弗雷德·馮·瓦德西元帥的東亞遠征軍參加了鎮壓](../Page/阿爾弗雷德·馮·瓦德西.md "wikilink")[義和團的軍事行動](../Page/義和團.md "wikilink")，並以此開啟了他個人對東方事物的興趣。1902年，法肯豪森調回第91步兵團任第2營行政官。1904年10月1日法肯豪森進入進修，於1907年7月20日畢業，後又轉入[柏林大學東方學院研究](../Page/柏林洪堡大學.md "wikilink")，這段期間法肯豪森努力學習[日語](../Page/日語.md "wikilink")，並大量收集日本的各種資訊。自1908年4月1日至1912年3月，法肯豪森調至[總參謀部服務](../Page/德軍總參謀部.md "wikilink")，後於3月22日因通曉語言和亞洲的政治局勢而被任命為駐日本東京武官，在此期間，法肯豪森對於與即將崩潰的中國清政府相比、對國民生活較有紀律的日本較有好感。

1914年8月[第一次世界大戰爆發](../Page/第一次世界大戰.md "wikilink")，法肯豪森奉命回國，並先後擔任和的參謀長。1916年5月9日法肯豪森調往土耳其，成為德國駐土耳其軍事顧問團的一份子，同月29日擔任土軍第2軍團後方調遣督察長參謀長，1917年1月1日法肯豪森晉升為督察。3月26日法肯豪森轉往高加索一帶，擔任土軍參謀長，被授予[藍馬克斯勳章](../Page/藍馬克斯勳章.md "wikilink")。战争结束后继续留在军队服务，1927年开始担任[德累斯頓步兵学校校长](../Page/德累斯頓步兵学校.md "wikilink")。1930年退休后受邀到[中国担任](../Page/中国.md "wikilink")[蔣中正的](../Page/蔣中正.md "wikilink")[军事顾问](../Page/德國軍事顧問團.md "wikilink")。

1937年[納粹德國正式与日本结盟](../Page/納粹德國.md "wikilink")，而日本已向中國不宣而戰。[納粹黨为了向日本表示善意](../Page/納粹黨.md "wikilink")，承认了日本在中国的傀儡[滿洲國](../Page/滿洲國.md "wikilink")，并撤回德国对中国的援助：包括强迫法肯豪森辞去顾问职务。离别前法肯豪森答应蔣中正不会向日本透露任何中国的作战计划。1938年法肯豪森被召回现役，在西部战线出任步兵上将。1940年5月被任命为比利时军事总督。

在比利时总督任上法肯豪森签署了17道反犹太人的命令，是纳粹在1942年从比利时驱逐犹太人的前奏。他负责经济的副手艾格·里德（Eggert
Reeder）在经济方面着手“清除犹太影响”，包括将犹太人清理出传统上由犹太人控制的[安特卫普钻石行业](../Page/安特卫普.md "wikilink")。但法肯豪森也至少接受他的中国朋友[钱卓伦将军的堂妹](../Page/钱卓伦.md "wikilink")、化学家钱秀玲的两次求情，特赦了被判死刑的约100名比利时反抗组织成员，在法肯豪森战后受审时钱秀玲也出面为其辩护作证。

他是两个反[希特勒者](../Page/希特勒.md "wikilink")：[卡尔·弗里德里希·歌德勒和](../Page/卡尔·弗里德里希·歌德勒.md "wikilink")[埃爾溫·馮·維茨萊本的亲密朋友](../Page/埃爾溫·馮·維茨萊本.md "wikilink")，很快地他也开始憎恶希特勒与纳粹政权，并支持維茨萊本的一个军事政变计划。1944年的[7月20日密謀案失败后](../Page/7月20日密謀案.md "wikilink")，馮·法肯豪森在不同的集中营中渡過，直至1945年戰爭結束才被盟国俘获。

1948年法肯豪森被送往比利时受审，钱秀玲和一批比利时犹太人出面为其辩护作证；在獄期間，著名的比利時抗納粹女英雄為他送食物（後來在他第一任妻子去世後，兩人結婚）。1951年3月被控驱逐25,000名[猶太人以及处死比利时俘虏而被判处](../Page/猶太人.md "wikilink")12年劳动监禁，后被送往西德服刑。1955年在（依照比利時法律要求）服刑超过1/3后，鉴于有充分的证据顯示他曾尽力挽救猶太人和比利时人，西德总理将其特赦。

1950年法肯豪森的72歲生日，收到蔣中正的12,000元美金作為賀禮，並稱他為中國的友人。

法肯豪森在晚年曾担任过德中文化协会会长。1966年7月31日在[萊茵蘭-普法爾茨的](../Page/萊茵蘭-普法爾茨.md "wikilink")[拿騷逝世](../Page/拿騷_\(德國\).md "wikilink")。

## 註腳

## 参考文献

  -
  -
  -
  -
  - [Falkenhausen, Alexander Ernst Alfred Hermann von - WW2
    Gravestone](https://ww2gravestone.com/people/falkenhfalkenhausen-alexander-ernst-alfred-hermann-von/)

  - [Go2War2.nl - Falkenhausen, Alexander
    von](http://www.go2war2.nl/artikel/1223/Falkenhausen-Alexander-von.htm)

  - [Alexander von
    Falkenhausen](https://www.belgiumwwii.be/belgique-en-guerre/personnalites/alexander-von-falkenhausen.html)

  - [General Alexander von Falkenhausen: Chinesen auf Spurensuche in
    Nassau - Das Dritte Reich -
    Rhein-Zeitung](https://www.rhein-zeitung.de/das-dritte-reich_artikel,-general-alexander-von-falkenhausen-chinesen-auf-spurensuche-in-nassau-_arid,1196322.html)

## 外部連結

  - [Personal Bio and Military achievement of Von
    Falkenhausen](https://web.archive.org/web/20070317231825/http://www.specialcamp11.fsnet.co.uk/General%20der%20Infanterie%20Alexander%20von%20Falkenhausen.htm)
  - [More tidbit info on Falkenhausen in Axis
    forum](http://forum.axishistory.com/viewtopic.php?t=116829&highlight=falkenhausen)

## 参见

  - [中德合作 (1911年—1941年)](../Page/中德合作_\(1911年—1941年\).md "wikilink")
  - [7月20日密謀案](../Page/7月20日密謀案.md "wikilink")

{{-}}

[F](../Category/德國將軍.md "wikilink")
[F](../Category/外籍援华人士.md "wikilink")
[Category:7月20日密謀案相關人員](../Category/7月20日密謀案相關人員.md "wikilink")
[Category:德國駐外武官](../Category/德國駐外武官.md "wikilink")