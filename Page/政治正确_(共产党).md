在大多數[共產黨](../Page/共產黨.md "wikilink")[一黨執政的國家](../Page/一黨執政.md "wikilink")，**政治正確**是一個重要且具影響力的概念。它的基本涵義是：「做任何事（包括政治層面和非政治層面），都要保證[意識形態本質符合執政黨與政府的規定](../Page/意識形態.md "wikilink")（所謂的「路線、方針、政策」）。」

然而，究竟什麼是「符合規定」，如何做到「符合規定」，一般沒有明確的定義，不同時期的規定也不盡相同，甚至完全相反。

在某些[極權社會時期](../Page/極權社會.md "wikilink")，如[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[文革時期](../Page/文革.md "wikilink")，以至現時由[金正恩家族統治的](../Page/金正恩.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")，「政治正確」幾乎是每位國民都需要時時注意的教條。

在一般時期，「政治正確」對以下人員是特別重要的事情：宣傳機構的從事人員、[媒體的審查者](../Page/媒體.md "wikilink")、某些政府官員（公務員）和共產黨官員等。政治正確甚至影響學術界領域，如要求文章語氣必需在馬克思主義的思維框架中論述，不可公然對顛撲不破的真理逾矩，類似古代要求文章內容須合於先王聖道之言，而非不受規範。

另外，由政府強力推銷特定的意識形態（如[愛國主義](../Page/愛國主義.md "wikilink")、[雅各賓派](../Page/雅各賓俱樂部.md "wikilink")、[納粹黨](../Page/纳粹党.md "wikilink")、[法西斯主義](../Page/法西斯主义.md "wikilink")）也被視為是一種政治正確的行為表現，跟共產黨的政治正確有異曲同工之处。

## 中华人民共和国

[中华人民共和国的](../Page/中华人民共和国.md "wikilink")“正确的政治观”大多数时候指的是党的“路线、方针、政策”，如[四项基本原则](../Page/四项基本原则.md "wikilink")。

在[中蘇交惡時期](../Page/中蘇交惡.md "wikilink")，中國[毛澤東政權曾經將前蘇聯](../Page/毛澤東.md "wikilink")[赫魯晓夫和](../Page/赫魯晓夫.md "wikilink")[勃列日涅夫政權稱做](../Page/勃列日涅夫.md "wikilink")「蘇修（[社會帝國主義](../Page/社會帝國主義.md "wikilink")）」政權。至於其他社會主義政權，如[古巴](../Page/古巴.md "wikilink")、[東德和](../Page/東德.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")，出自於對歐美等西方列強的敵意，官方媒體都會将西方國家冠以「[法西斯主義](../Page/法西斯主義.md "wikilink")（東德當局曾將[柏林圍牆稱做](../Page/柏林圍牆.md "wikilink")「反法西斯保護牆」）」、「美帝」、「[帝國主義集團](../Page/帝國主義.md "wikilink")」或「侵略者」等稱謂。

## 越南

[越南曾在](../Page/越南.md "wikilink")[法越戰爭](../Page/法越戰爭.md "wikilink")、[越南戰爭和](../Page/越南戰爭.md "wikilink")[中越戰爭等時期](../Page/中越戰爭.md "wikilink")，先後同樣將法國、美國和中國等越南觀點之「侵略者」視為「帝國主義集團」；在中越戰爭以後，越南開始與中國為敵並與美國靠攏，而官方媒體大部分的敵意稱謂則多半針對中國。

## 傳媒用法

  - 中国大陆的媒體（如[新華社](../Page/新華社.md "wikilink")、[央視](../Page/央視.md "wikilink")、[人民日報](../Page/人民日報.md "wikilink")）與部分港澳地區的[親中媒體](../Page/親中媒體.md "wikilink")（如[大公報](../Page/大公報.md "wikilink")、[文匯報](../Page/文匯報.md "wikilink")、[香港商報](../Page/香港商報.md "wikilink")、[澳門日報](../Page/澳門日報.md "wikilink")、[澳廣視](../Page/澳廣視.md "wikilink")、[亞視](../Page/亞洲電視.md "wikilink")、[TVB等](../Page/電視廣播有限公司.md "wikilink")），凡是報導兩岸新聞時，都將中华民国各級官署和官職加引号或改变称法，如「總統府」、「立委」、「行政院長」、「內政部長」、「國防部長」分别称为“领导人办公室”、“民代”、“行政机构负责人”、“内政部门负责人”、“防务部门负责人”，以表示不承认中华民国政府的合法性。
  - [朝鮮民主主義人民共和國的官方媒體](../Page/朝鮮民主主義人民共和國.md "wikilink")（如[勞動新聞](../Page/勞動新聞.md "wikilink")、[朝鮮中央電視台](../Page/朝鮮中央電視台.md "wikilink")）凡是提到[大韓民國各級官署和官職時](../Page/大韓民國.md "wikilink")，一律將南韓方面稱之為「叛亂集團」或「南朝鲜[傀儡政權](../Page/傀儡政權.md "wikilink")」。

## 参考文献

## 參見

  - [意識形態](../Page/意識形態.md "wikilink")
  - [政治正確](../Page/政治正確.md "wikilink")
  - [自我審查](../Page/自我審查.md "wikilink")
  - [中國網路審查](../Page/中國網路審查.md "wikilink")
  - [中共中央宣傳部](../Page/中共中央宣傳部.md "wikilink")
  - [中央統戰部](../Page/中央統戰部.md "wikilink")
  - [中央精神文明建設指導委員會](../Page/中央精神文明建設指導委員會.md "wikilink")

## 外部链接

  - [《怎样当新闻记者》：关于自律的若干常规和制度](http://news.xinhuanet.com/newmedia/2003-06/28/content_943048.htm)，[新华网](../Page/新华网.md "wikilink")

[Category:共产党](../Category/共产党.md "wikilink")
[Category:政治正確](../Category/政治正確.md "wikilink")