**麥克·賢治·篠田**\[1\]（**Michael Kenji
Shinoda**，1977年2月11日—）是一位[美國的](../Page/美國.md "wikilink")[音樂家和](../Page/音樂家.md "wikilink")[唱片製作人](../Page/唱片製作人.md "wikilink")，同時也是[聯合公園](../Page/聯合公園.md "wikilink")（Linkin
Park）和[黑暗堡壘](../Page/黑暗堡壘.md "wikilink")（Fort Minor）的成員。

## 生平

### 早期生活

麥克的父母分別是[日裔美國人和](../Page/日裔美國人.md "wikilink")[德裔美國人](../Page/德裔美國人.md "wikilink")，他在[美國](../Page/美國.md "wikilink")[洛杉磯的](../Page/洛杉磯.md "wikilink")[阿古拉山長大](../Page/阿古拉山_\(加利福尼亚州\).md "wikilink")。麥可有一個1979年出生的弟弟傑森（Jason）。

麥克首次接觸音樂是在高中期間，當時他參加了[炭疽熱樂團](../Page/炭疽熱樂團.md "wikilink")（Anthrax）和[人民公敵](../Page/人民公敵_\(樂隊\).md "wikilink")（Public
Enemy）的演唱會。在那之後他開始學習鋼琴，研究古典鋼琴演奏技巧，之後他轉往[爵士樂和](../Page/爵士樂.md "wikilink")[嘻哈領域](../Page/嘻哈音樂.md "wikilink")。

在早期青少年階段，麥克在音樂方面受到好友[布萊德·達爾森](../Page/布萊德·達爾森.md "wikilink")（Brad
Delson）的鼓勵，他和布萊德開始在臥房中的錄音室創作和錄製歌曲。麥可和布萊德是阿古洛高中（Agoura High
School）的同學。在高中即將畢業時，擅長[打擊樂器的](../Page/打擊樂器.md "wikilink")[羅伯·博登](../Page/羅伯·博登.md "wikilink")（Rob
Bourdon）開始參與他們的音樂計劃。隨後他們三人組成了「[Xero](../Page/Xero.md "wikilink")」樂團，並開始更加認真的往音樂業界發展。

高中畢業後，麥克申請到[藝術中心設計學院](../Page/藝術中心設計學院.md "wikilink")（Art Center College
of
Design）就讀[平面設計和](../Page/平面設計.md "wikilink")[插畫](../Page/插畫.md "wikilink")。在學校中他認識了[DJ](../Page/DJ.md "wikilink")[喬瑟夫·漢恩](../Page/喬瑟夫·漢恩.md "wikilink")（Joseph
Hahn）以及達爾森的大學同學[-{費尼克斯}-](../Page/戴維·法雷爾.md "wikilink")（Phoenix），兩人隨後加入
Xero
樂團。麥克最後從學院畢業，取得插畫學位，並在畢業後立即找到一份[平面設計師的工作](../Page/平面設計.md "wikilink")。憑藉著他在平面設計的經驗和天分，麥克和喬負責所有[聯合公園樂團的相關設計](../Page/聯合公園.md "wikilink")。

### 林肯公园

### 黑暗堡垒

### 单独活动

2018年6月，麦克推出了自己的首张个人专辑《[创伤之后](../Page/创伤之后.md "wikilink")》（Post
Traumatic），以此作为向此前去世的乐队主唱[查斯特·班宁顿的致意](../Page/查斯特·班宁顿.md "wikilink")。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [聯合公園官方網站](http://www.linkinpark.com)
  - [黑暗堡壘官方網站](http://www.fortminor.com)
  - [麥克·篠田官方網站](http://www.mikeshinoda.com)

[Category:1977年出生](../Category/1977年出生.md "wikilink")
[Category:聯合公園](../Category/聯合公園.md "wikilink")
[Category:美國男歌手](../Category/美國男歌手.md "wikilink")
[Category:美國饒舌歌手](../Category/美國饒舌歌手.md "wikilink")
[Category:美國音樂家](../Category/美國音樂家.md "wikilink")
[Category:艺术中心设计学院校友](../Category/艺术中心设计学院校友.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")
[Category:日本裔美国人](../Category/日本裔美国人.md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")
[Category:美國結他手](../Category/美國結他手.md "wikilink")
[Category:俄裔混血兒](../Category/俄裔混血兒.md "wikilink")

1.  麥克的本名英文為 Michael Kenji Shinoda，其中 Kenji Shinoda
    應為[日文的拼音](../Page/日文.md "wikilink")，在其[黑暗堡壘](../Page/黑暗堡壘.md "wikilink")（Fort
    Minor）專輯《[The Rising
    Tied](../Page/The_Rising_Tied.md "wikilink")》里他使用了篠田賢治這個漢字寫法。按照日语发音，Kenji（けんじ）可能为健司、健次、健二等，而
    Shinoda（しのだ）也可能是信太或篠田等。在日本，因视其为外国歌手，故其姓名採用音譯的片假名而非本身的汉字（譯為：マイク シノダ）。