**卑爾根郡**（Bergen County, New
Jersey）是[美國](../Page/美國.md "wikilink")[新澤西州東北部的一個縣](../Page/新澤西州.md "wikilink")，東、北鄰[紐約州](../Page/紐約州.md "wikilink")。面積639平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口884,118。縣治[哈肯薩克](../Page/哈肯薩克_\(新澤西州\).md "wikilink")
（Hackensack）。

成立於1675年，[東澤西最早的四個縣之一](../Page/東澤西省與西澤西省.md "wikilink")。縣名來源有二說：一說來自[挪威的](../Page/挪威.md "wikilink")[卑爾根](../Page/卑爾根.md "wikilink")；另一說是來自[荷蘭的](../Page/荷蘭.md "wikilink")[貝亨奧普佐姆](../Page/貝亨奧普佐姆.md "wikilink")
(Bergen op Zoom)。

[B](../Category/新泽西州行政区划.md "wikilink")