[Afrikakonferenz.jpg](https://zh.wikipedia.org/wiki/File:Afrikakonferenz.jpg "fig:Afrikakonferenz.jpg")
**柏林西非会议**（[英语](../Page/英语.md "wikilink")：Berlin
Conference，意指柏林会议；[德语](../Page/德语.md "wikilink")：Kongokonferenz，意指刚果会议）由[欧洲强国在](../Page/欧洲.md "wikilink")1884年至1885年与[德国](../Page/德国.md "wikilink")[柏林举行](../Page/柏林.md "wikilink")，以准备他们在[非洲建立](../Page/非洲.md "wikilink")[殖民地与发展贸易](../Page/殖民地.md "wikilink")。与会国最后达成了**柏林会议总议定书**，正式开始[瓜分非洲](../Page/瓜分非洲.md "wikilink")。

## 缘由

1880年代，欧洲国家对非洲有了更大的兴趣。[亨利·莫頓·史丹利于](../Page/亨利·莫頓·史丹利.md "wikilink")1874至1877年发现了[刚果河盆地](../Page/刚果河.md "wikilink")，终于找到非洲最后的“未知的地域”（[拉丁语](../Page/拉丁语.md "wikilink")：terra
incognita）。[比利时国王](../Page/比利时.md "wikilink")[利奥波德二世于](../Page/利奥波德二世_\(比利时\).md "wikilink")1876年成立了[国际非洲协会](../Page/国际非洲协会.md "wikilink")（[法语](../Page/法语.md "wikilink")：Association
Internationale
Africaine）。得知史丹利的成功后，利奥波德二世邀请他参与协会的工作。该协会的宗旨是探索非洲与为非洲人民带来“文明”。1878年，另一类似组织[国际刚果协会成立](../Page/国际刚果协会.md "wikilink")，虽与非洲协会关系密切，但比较注重经济发展之工作。比利时国王秘密地买通了刚果协会的投资者，把刚果协会变成追求[帝国主义的组织](../Page/帝国主义.md "wikilink")，而表面上则以看似支持[慈善事业的非洲协会作掩饰](../Page/慈善.md "wikilink")。

由1879年到1884年，史丹利再回到刚果，但不是为求探索领地，而是作为利奥波德二世的探子，谋求在刚果建立一个国家。那边厢，[法国的海军军官](../Page/法国.md "wikilink")[皮埃尔·萨沃尼昂·德-布拉柴到达刚果盆地西岸](../Page/皮埃尔·萨沃尼昂·德-布拉柴.md "wikilink")，并于1881年在[布拉柴维尔](../Page/布拉柴维尔.md "wikilink")（今[刚果共和国首都](../Page/刚果共和国.md "wikilink")）建立了法属殖民地。

而且，[葡萄牙由于曾经与刚果帝国签署过条约](../Page/葡萄牙.md "wikilink")，所以在1884年2月26日与[英国签署条约](../Page/英国.md "wikilink")，企图阻止刚果协会扩张到[大西洋的海岸线](../Page/大西洋.md "wikilink")。

除了以上国家，其他欧陆国家也想在非洲建立殖民地。法国在1881年占领了[突尼西亚和今刚果共和国的领土](../Page/突尼西亚.md "wikilink")，又在1884年得到[几内亚](../Page/几内亚.md "wikilink")。1882年，英国占领名义上属于[奥斯曼帝国的](../Page/奥斯曼帝国.md "wikilink")[埃及](../Page/埃及.md "wikilink")，并因此控制了[苏丹与部分的](../Page/苏丹共和国.md "wikilink")[索马利亚](../Page/索马利亚.md "wikilink")。在1870年与1882年，[意大利夺得](../Page/意大利.md "wikilink")[厄立特里亚](../Page/厄立特里亚.md "wikilink")。刚刚崛起的[德国也在](../Page/德国.md "wikilink")1884年把[多哥](../Page/多哥.md "wikilink")、[喀麦隆与](../Page/喀麦隆.md "wikilink")[西南非](../Page/納米比亞.md "wikilink")（今[纳米比亚](../Page/纳米比亚.md "wikilink")）纳入为他的[保护国](../Page/保护国.md "wikilink")。

## 会议

利奥波德二世成功就有关非洲贸易之事，说服法国与德国。在葡萄牙的倡导下，[德国首相](../Page/德国总理.md "wikilink")[奥托·冯·俾斯麦于](../Page/奥托·冯·俾斯麦.md "wikilink")1884年邀请了[奥匈帝国](../Page/奥匈帝国.md "wikilink")、[比利时](../Page/比利时.md "wikilink")、[丹麦](../Page/丹麦.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")、[法国](../Page/法国.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[瑞典与](../Page/瑞典.md "wikilink")[挪威](../Page/挪威.md "wikilink")（挪威在1905年前以[共主邦联之关系属于瑞典](../Page/共主邦联.md "wikilink")）、[西班牙](../Page/西班牙.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[英国](../Page/英国.md "wikilink")、[美国与](../Page/美国.md "wikilink")[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")
十五国代表参加柏林会议，以达成共识。

## 总议定书

议定书定下了以下的条款：

  - 刚果自由邦正式被定为国际刚果协会的私有财产。于是，二百万平方公里的刚果领土（今[刚果民主共和国之全境](../Page/刚果民主共和国.md "wikilink")）被划为利奥波德二世的私人财产。
  - 全体十四个与会国将于刚果盆地全境、[马拉维湖及其以东邻近地区享有自由贸易之权利](../Page/马拉维湖.md "wikilink")；
  - [尼日尔河与](../Page/尼日尔河.md "wikilink")[刚果河将对船只自由开放](../Page/刚果河.md "wikilink")；
  - 与会国签署禁止贩卖[奴隶](../Page/奴隶.md "wikilink")；
  - 根据“有效原则”，与会国只有真正控制某殖民地时，才可以拥有它们。
  - 如若任何国家在将来想拥有非洲海岸的任何部分，都必须通知其他签署国或是订立[保护国](../Page/保护国.md "wikilink")。

值得注意的是，柏林协定书的行文中，第一次提及有关与会国之[势力范围的国际协定](../Page/势力范围.md "wikilink")。

## 结果

[FeizhouZhimindi.png](https://zh.wikipedia.org/wiki/File:FeizhouZhimindi.png "fig:FeizhouZhimindi.png")
会议过后，欧洲列强加速她们瓜分非洲的行动。在几年之内，名义上[撒哈拉沙漠以南的非洲领土全被瓜分](../Page/撒哈拉沙漠.md "wikilink")。1895年时，只有[利比里亚](../Page/利比里亚.md "wikilink")、[奥兰治自由邦与](../Page/奥兰治自由邦.md "wikilink")[德兰士瓦仍为](../Page/德兰士瓦.md "wikilink")[独立国家](../Page/独立.md "wikilink")。[阿比西尼亚](../Page/阿比西尼亚.md "wikilink")（今衣索比亞）于1889年至1896年成功抵抗[意大利自](../Page/意大利.md "wikilink")[厄立特里亚而来的侵略](../Page/厄立特里亚.md "wikilink")，史称[第一次意阿战争](../Page/第一次意阿战争.md "wikilink")，是非洲唯一的独立原居民国家。1902年前，非洲九成以上的领土都被欧陆国家控制。撒哈拉沙漠之土地，大多为法国所有。英国在镇压了[穆罕默德·艾哈邁德·馬赫迪和与法国解决](../Page/穆罕默德·艾哈邁德·馬赫迪.md "wikilink")[法绍达事件后](../Page/法绍达事件.md "wikilink")，与埃及共同统治苏丹。

英国发动了两次[布尔战争](../Page/布尔战争.md "wikilink")，征服了布尔人的国家。1911年，法国与西班牙瓜分了[摩洛哥](../Page/摩洛哥.md "wikilink")。1912年，意大利从[土耳其夺得](../Page/土耳其.md "wikilink")[利比亚](../Page/利比亚.md "wikilink")。1914年，英国正式占领埃及全境，列强瓜分非洲结束。在这个时候，除了利比里亚与埃塞俄比亚之外，其他非洲土地全归欧洲国家所有。

另外列强在会议上罔顾非洲各民族的实际分布，采取直接在[地图上划分势力范围的做法](../Page/地图.md "wikilink")，造成了现今不少非洲国家之间的[国界异常平直](../Page/国界.md "wikilink")，也因此为这些国家遗留了不少影响至今的[部族纷争](../Page/部族.md "wikilink")。

## 参看

  - [非洲历史](../Page/非洲历史.md "wikilink")
  - [新帝国主义](../Page/新帝国主义.md "wikilink")
  - [乔治·塔伯曼·格尔迪](../Page/乔治·塔伯曼·格尔迪.md "wikilink") (George Taubman
    Goldie)

[Category:1880年代欧洲](../Category/1880年代欧洲.md "wikilink")
[Category:1880年代非洲](../Category/1880年代非洲.md "wikilink")
[Category:非洲殖民](../Category/非洲殖民.md "wikilink")
[Category:柏林政治史](../Category/柏林政治史.md "wikilink")
[Category:德国政治会议](../Category/德国政治会议.md "wikilink")
[Category:德意志帝国外交](../Category/德意志帝国外交.md "wikilink")
[Category:奥匈帝国外交](../Category/奥匈帝国外交.md "wikilink")
[Category:比利时外交史](../Category/比利时外交史.md "wikilink")
[Category:丹麦外交史](../Category/丹麦外交史.md "wikilink")
[Category:俄罗斯帝国外交事件](../Category/俄罗斯帝国外交事件.md "wikilink")
[Category:法国外交史](../Category/法国外交史.md "wikilink")
[Category:荷兰外交史](../Category/荷兰外交史.md "wikilink")
[Category:葡萄牙王国外交](../Category/葡萄牙王国外交.md "wikilink")
[Category:瑞典外交史](../Category/瑞典外交史.md "wikilink")
[Category:挪威外交史](../Category/挪威外交史.md "wikilink")
[Category:西班牙外交史](../Category/西班牙外交史.md "wikilink")
[Category:意大利王国外交](../Category/意大利王国外交.md "wikilink")
[Category:英国外交史](../Category/英国外交史.md "wikilink")
[Category:美國外交史](../Category/美國外交史.md "wikilink")
[Category:奥斯曼帝国外交](../Category/奥斯曼帝国外交.md "wikilink")
[Category:政府间会议](../Category/政府间会议.md "wikilink")
[Category:1884年政治](../Category/1884年政治.md "wikilink")