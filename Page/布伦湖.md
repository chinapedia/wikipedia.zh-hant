**布伦湖**（Bullenseen）是由[德國的两个](../Page/德國.md "wikilink")[湖所組成](../Page/湖.md "wikilink")：大布伦湖和小布伦湖。在上一次[冰河时期末期出现](../Page/冰河时期.md "wikilink")，是一块[冰川的遗迹](../Page/冰川.md "wikilink")。坐落于[罗滕堡低地](../Page/罗滕堡_\(维默河\).md "wikilink")，罗腾堡的南端，西临[博特赫](../Page/博特赫.md "wikilink")（地理坐标:
东经9° 24' 到 25' ,北纬 53° 03 '）。

湖的周围是沼泽，高盐的环境使得鱼类不能在湖内生存。湖及其周边地区被列为自然保护区，并且也被视为[不来梅](../Page/不来梅.md "wikilink")、罗滕堡近郊的悠闲地带。

每年5月1日罗滕堡南区的青年都会在此举行聚会。

[B](../Category/德国湖泊.md "wikilink")
[Category:自然保护区](../Category/自然保护区.md "wikilink")