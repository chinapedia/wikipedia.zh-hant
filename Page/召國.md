**召国**，又名**邵**（shao4），為西周的畿內封國，是西周、春秋時代諸侯国之一。召南在周南之西，包括陝西南部和湖北一部分。

## 起源

召公奭為周室三公之一。周武王時，封同族人于東方為諸侯，[召公奭受封於燕地](../Page/召公奭.md "wikilink")，但他並未前往，由長子[克前往就任](../Page/燕侯克.md "wikilink")，自己則留在鎬京（今陝西省長安縣）與周公旦、畢公高一起輔佐天子。其子孫世代繼為三公。因食邑在召，故謂之召公。

## 世系

<table>
<thead>
<tr class="header">
<th><p>諡號</p></th>
<th><p>名</p></th>
<th><p>統治時間</p></th>
<th><p>備注</p></th>
<th><p>出處</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/召公奭.md" title="wikilink">召康公</a></p></td>
<td><p>奭</p></td>
<td><p>周武王元年──周康王24年</p></td>
<td><p>周武王族人，周代三公之一</p></td>
<td><p>《史記》周本紀、燕世家<br />
竹書紀年|《今本竹書紀年》</p></td>
</tr>
<tr class="even">
<td><p>中間世系失考</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/召幽伯.md" title="wikilink">召幽伯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/召穆公.md" title="wikilink">召穆公</a></p></td>
<td><p>虎</p></td>
<td></td>
<td><p>與<a href="../Page/周定公.md" title="wikilink">周定公</a><strong>共和行政</strong></p></td>
<td><p>《史記》周本紀</p></td>
</tr>
<tr class="odd">
<td><p>中間世系失考</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/召伯廖.md" title="wikilink">召伯廖</a></p></td>
<td><p>廖</p></td>
<td></td>
<td></td>
<td><p>《左傳》莊公二十七年</p></td>
</tr>
<tr class="odd">
<td><p>中間一世失考</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/召武公.md" title="wikilink">召武公</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>《左傳》僖公十一年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/召昭公.md" title="wikilink">召昭公</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>《左傳》文公五年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/召戴公.md" title="wikilink">召戴公</a></p></td>
<td></td>
<td><p>？─前594年</p></td>
<td></td>
<td><p>《左傳》宣公十五年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/召桓公.md" title="wikilink">召桓公</a></p></td>
<td><p>襄</p></td>
<td><p>前593年─？</p></td>
<td></td>
<td><p>《左傳》宣公十五年</p></td>
</tr>
<tr class="even">
<td><p>中間一世失考</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/召莊公.md" title="wikilink">召莊公</a></p></td>
<td><p>奐</p></td>
<td></td>
<td><p>周景王死後，周王室內發生了王子朝之亂，有參與其中。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/召簡公.md" title="wikilink">召簡公</a></p></td>
<td><p>盈</p></td>
<td><p>？─前513年</p></td>
<td><p>莊公之子。周景王死後，周王室內發生了王子朝之亂，有參預其中。<br />
前516年，逐<a href="../Page/王子朝.md" title="wikilink">王子朝迎周敬王回國</a>。後被<a href="../Page/周敬王.md" title="wikilink">周敬王誅殺</a></p></td>
<td><p>《左傳》昭公二十四年、二十九年</p></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references />

</div>

左丘明《春秋左傳》

<div class="references-small">

<references />

</div>

司馬遷《史記》

## 参见

  - [召姓](../Page/召姓.md "wikilink")

<!-- end list -->

  - [邵姓](../Page/邵姓.md "wikilink")

<!-- end list -->

  - [春秋时期](../Page/春秋时期.md "wikilink")

<!-- end list -->

  - [周朝](../Page/周朝.md "wikilink")

<!-- end list -->

  - [春秋战国诸侯国列表](../Page/春秋战国诸侯国列表.md "wikilink")

[S召](../Category/春秋時期的國家.md "wikilink")