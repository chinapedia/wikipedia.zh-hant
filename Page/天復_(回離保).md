**天復**（1123年正月-八月）是[辽朝时的奚王](../Page/辽朝.md "wikilink")[回離保的](../Page/回離保.md "wikilink")[年號](../Page/年號.md "wikilink")，共計8个月。

## 纪年

| 天復                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1123年                          |
| [干支](../Page/干支纪年.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权使用的[天復年号](../Page/天復.md "wikilink")
  - 同期存在的其他政权年号
      - [宣和](../Page/宣和_\(宋徽宗\).md "wikilink")（1119年二月至1125年十二月）：宋—[宋徽宗趙佶之年號](../Page/宋徽宗.md "wikilink")
      - [保大](../Page/保大_\(辽天祚帝\).md "wikilink")（1121年-1125年二月）：辽—[辽天祚帝耶律延禧之年号](../Page/辽天祚帝.md "wikilink")
      - [神曆](../Page/神曆_\(耶律雅里\).md "wikilink")（1123年五月至十月）：北遼—梁王[耶律雅里之年號](../Page/耶律雅里.md "wikilink")
      - [天輔](../Page/天輔_\(完顏阿骨打\).md "wikilink")（1117年正月至1123年九月）：金—[金太祖完顏阿骨打之年號](../Page/金太祖.md "wikilink")
      - [天會](../Page/天會_\(金太宗\).md "wikilink")（1123年九月至1137年十二月）：金—[金太宗吳乞買](../Page/金太宗.md "wikilink")、金熙宗完颜亶之年號
      - [元德](../Page/元德.md "wikilink")（1119年正月至1127年三月）：西夏—[夏崇宗李乾順之年號](../Page/夏崇宗.md "wikilink")
      - [天符睿武](../Page/天符睿武.md "wikilink")（1120年至1126年）：李朝—[李乾德之年號](../Page/李乾德.md "wikilink")
      - [保安](../Page/保安_\(鳥羽天皇\).md "wikilink")（1120年四月十日至1124年四月三日）：日本鳥羽天皇與[崇德天皇年号](../Page/崇德天皇.md "wikilink")

[Category:辽朝民变政权年号](../Category/辽朝民变政权年号.md "wikilink")
[Category:12世纪中国年号](../Category/12世纪中国年号.md "wikilink")
[Category:1120年代中国](../Category/1120年代中国.md "wikilink")
[Category:1123年](../Category/1123年.md "wikilink")