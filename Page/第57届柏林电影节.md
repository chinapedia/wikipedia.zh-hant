**第57届柏林国际电影节**于当地时间2007年2月8日到2月18日举行。本届电影节评委会主席由美国导演监编剧[保罗·施瑞德](../Page/保罗·施瑞德.md "wikilink")（[Paul
Schrader](../Page/:en:Paul_Schrader.md "wikilink")）担任。评委会其他成员包括：[法国](../Page/法国.md "wikilink")[巴勒斯坦裔女演员](../Page/巴勒斯坦.md "wikilink")[西亚姆·阿巴斯](../Page/西亚姆·阿巴斯.md "wikilink")（[Hiam
Abbass](../Page/:en:Hiam_Abbass.md "wikilink")）、美国演员[威廉·达福](../Page/威廉·达福.md "wikilink")（[Willem
Dafoe](../Page/:en:Willem_Dafoe.md "wikilink")）、[墨西哥演员](../Page/墨西哥.md "wikilink")[盖尔·加西亚·伯纳尔](../Page/盖尔·加西亚·伯纳尔.md "wikilink")（[Gael
García
Bernal](../Page/:en:Gael_García_Bernal.md "wikilink")）、[德国演员](../Page/德国.md "wikilink")[马里奥·阿道夫](../Page/马里奥·阿道夫.md "wikilink")（[Mario
Adorf](../Page/:en:Mario_Adorf.md "wikilink")）、[丹麦剪辑师莫利](../Page/丹麦.md "wikilink")·玛琳·斯坦斯加德（）以及来自[香港的电影制片人](../Page/香港.md "wikilink")[施南生女士](../Page/施南生.md "wikilink")。

## 開閉幕片

本屆的開閉幕片分別為：[奧利維爾·達昂](../Page/奧利維爾·達昂.md "wikilink")(Olivier
Dahan)的《[玫瑰人生](../Page/玫瑰人生_\(电影\).md "wikilink")》(La Môme)
(法國/英國/捷克)與[法蘭索瓦·奧桑](../Page/法蘭索瓦·奧桑.md "wikilink")(François
Ozon)的《[天使](../Page/天使.md "wikilink")》(Angel)(法國)，《玫瑰人生》同時也是競賽片之一。

## 競賽片

以下電影被選定為競賽片，可角逐[金熊獎和](../Page/金熊獎.md "wikilink")[銀熊獎](../Page/銀熊獎.md "wikilink")：

<table style="width:94%;">
<colgroup>
<col style="width: 27%" />
<col style="width: 22%" />
<col style="width: 18%" />
<col style="width: 27%" />
</colgroup>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原文片名</p></th>
<th><p>導演</p></th>
<th><p>出品國</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/玫瑰人生_(電影).md" title="wikilink">玫瑰人生</a>》(開幕片)</p></td>
<td><p><em>La Vie en rose</em></p></td>
<td><p><a href="../Page/奧利維耶·達安.md" title="wikilink">奧利維耶·達安</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>בופור</em></p></td>
<td><p><a href="../Page/約瑟夫·席達.md" title="wikilink">約瑟夫·席達</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Bordertown</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/伪钞制造者.md" title="wikilink">偽鈔風暴</a>》</p></td>
<td><p><em>Die Fälscher</em></p></td>
<td><p><a href="../Page/斯戴芬·盧佐維茨基.md" title="wikilink">斯戴芬·盧佐維茨基</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>El otro</em></p></td>
<td></td>
<td><p>、、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Goodbye Bafana</em></p></td>
<td><p><a href="../Page/比利·奧古斯特.md" title="wikilink">比利·奧古斯特</a></p></td>
<td><p>、、、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Hallam Foe</em></p></td>
<td><p><a href="../Page/大衛·麥肯齊_(導演).md" title="wikilink">大衛·麥肯齊</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>경계</em></p></td>
<td><p><a href="../Page/張律.md" title="wikilink">張律</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>In memoria di me</em></p></td>
<td><p><a href="../Page/薩維里奧·康斯坦佐.md" title="wikilink">薩維里奧·康斯坦佐</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Irina Palm</em></p></td>
<td></td>
<td><p>、、、、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Les Témoins</em></p></td>
<td><p><a href="../Page/安德烈·泰希內.md" title="wikilink">安德烈·泰希內</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Ne touchez pas la hache</em></p></td>
<td><p><a href="../Page/賈克·希維特.md" title="wikilink">賈克·希維特</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>O Ano em Que Meus Pais Saíram de Férias</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Obsluhoval jsem anglického krále</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/蘋果_(電影).md" title="wikilink">蘋果</a>》</p></td>
<td></td>
<td><p><a href="../Page/李玉_(導演).md" title="wikilink">李玉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>싸이보그지만 괜찮아</em></p></td>
<td><p><a href="../Page/朴贊郁.md" title="wikilink">朴贊郁</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>The Good German</em></p></td>
<td><p><a href="../Page/史蒂芬·索德柏.md" title="wikilink">史蒂芬·索德柏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/特務風雲：中情局誕生秘辛.md" title="wikilink">特務風雲：中情局誕生秘辛</a>》</p></td>
<td><p><em>The Good Shepherd</em></p></td>
<td><p><a href="../Page/勞勃·狄尼洛.md" title="wikilink">勞勃·狄尼洛</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/圖雅的婚事.md" title="wikilink">圖雅的婚事</a>》</p></td>
<td></td>
<td><p><a href="../Page/王全安.md" title="wikilink">王全安</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>When a Man Falls in the Forest</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Yella</em></p></td>
<td><p><a href="../Page/克里斯蒂安·佩措尔德_(导演).md" title="wikilink">克里斯汀·佩佐</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》(閉幕片)</p></td>
<td><p><em>Angel</em></p></td>
<td><p><a href="../Page/法蘭索瓦·歐容.md" title="wikilink">法蘭索瓦·歐容</a></p></td>
<td><p>、、</p></td>
</tr>
</tbody>
</table>

## 觀摩片

## 華語片在57屆柏林影展

根據[官方網站](http://www.berlinale.de/)上的分類，將華語片分為[台灣](../Page/中華民國.md "wikilink")、[香港與](../Page/香港.md "wikilink")[中國大陆三個部份](../Page/中國大陆.md "wikilink")。

### 台灣方面

台灣電影入圍柏林影展的競賽單元只有[台裔導演](../Page/台裔.md "wikilink")[陳駿霖執導的](../Page/陳駿霖.md "wikilink")[短片](../Page/短片.md "wikilink")《[美](../Page/美.md "wikilink")》入圍。觀摩單元則有《[豔光四射歌舞團](../Page/豔光四射歌舞團.md "wikilink")》導演[周美玲執導的新作](../Page/周美玲.md "wikilink")《[刺青](../Page/刺青_\(電影\).md "wikilink")》，獲邀參加[電影大觀](../Page/電影大觀.md "wikilink")（Panorama）單元。

最後《[刺青](../Page/刺青_\(電影\).md "wikilink")》獲得最佳同志電影泰迪熊獎，為台灣第一部獲得此殊榮之電影。而《[美](../Page/美.md "wikilink")》也獲得短片類評審團大獎[銀熊獎](../Page/銀熊獎.md "wikilink")。

由[楊丞琳](../Page/楊丞琳.md "wikilink")、[梁洛施主演的](../Page/梁洛施.md "wikilink")《[刺青](../Page/刺青_\(電影\).md "wikilink")》描述女刺青師梁洛施與楊丞琳飾演的視訊女郎十年之間感情從壓抑到揭露的過程。

《[美](../Page/美_\(消歧義\).md "wikilink")》描述[南機場夜市麵攤老闆父女和一位青年的因緣際會](../Page/南機場夜市.md "wikilink")，由[北藝大學生](../Page/國立臺北藝術大學.md "wikilink")[姚淳耀](../Page/姚淳耀.md "wikilink")、[吳珈瑋合演](../Page/吳珈瑋.md "wikilink")。

### 中國大陆方面

本屆柏林影展競賽單元方面，有兩部中國電影入圍。分別是[王全安執導的](../Page/王全安.md "wikilink")《[圖雅的婚事](../Page/圖雅的婚事.md "wikilink")》和[李玉的新作](../Page/李玉.md "wikilink")《[蘋果](../Page/蘋果.md "wikilink")》。其中《[圖雅的婚事](../Page/圖雅的婚事.md "wikilink")》獲得最佳影片[金熊獎](../Page/金熊獎.md "wikilink")。

[余男主演的](../Page/余男.md "wikilink")《[圖雅的婚事](../Page/圖雅的婚事.md "wikilink")》，以[內蒙為背景](../Page/內蒙古.md "wikilink")，改編自真實故事，描述癱瘓老公逼老婆改嫁的故事。《[蘋果](../Page/蘋果.md "wikilink")》則以現代[北京為背景](../Page/北京.md "wikilink")，描述進城打工的年輕夫妻和鄰居中年夫妻發生桃色風暴，衍生連串的衝突，由[佟大為](../Page/佟大為.md "wikilink")、[范冰冰](../Page/范冰冰.md "wikilink")、[梁家輝](../Page/梁家輝.md "wikilink")、[金燕玲等知名演員演出](../Page/金燕玲.md "wikilink")。

### 香港方面

本屆柏林影展競賽單元並沒有香港電影入圍。

### 收穫

本屆柏林影展華語片參加的數量雖少，但是得獎豐富，是華語電影收穫豐富的一屆。

## 得獎名單

  - 最佳影片[金熊獎](../Page/金熊獎.md "wikilink")：《[圖雅的婚事](../Page/圖雅的婚事.md "wikilink")》
    [中國](../Page/中華人民共和國.md "wikilink")，[王全安導演](../Page/王全安.md "wikilink")
  - 評審團大獎[銀熊獎](../Page/銀熊獎.md "wikilink")：《[另一位](../Page/另一位.md "wikilink")》(El
    otro)
    [阿根廷](../Page/阿根廷.md "wikilink")、[法國與](../Page/法國.md "wikilink")[德國](../Page/德國.md "wikilink")，[艾瑞爾.羅特](../Page/艾瑞爾.羅特.md "wikilink")(Ariel
    Rotter)導演
  - 最佳導演銀熊獎：[約瑟夫.斯達](../Page/約瑟夫.斯達.md "wikilink")(Joseph
    Cedar)於[以色列電影](../Page/以色列.md "wikilink")《[波佛特](../Page/波佛特.md "wikilink")》(Beaufort)
  - 最佳男演員銀熊獎：[胡里歐·查維茲](../Page/胡里歐·查維茲.md "wikilink")（Julio
    Chávez）於[阿根廷電影](../Page/阿根廷.md "wikilink")《[另一位](../Page/另一位.md "wikilink")》(El
    otro)
  - 最佳女演員銀熊獎：[妮娜·霍斯](../Page/妮娜·霍斯.md "wikilink")（Nina
    Hoss）於[德國電影](../Page/德國.md "wikilink")《[耶萊](../Page/耶萊.md "wikilink")》(Yella)
  - 最佳電影音樂銀熊獎：《[偷窺者哈藍](../Page/偷窺者哈藍.md "wikilink")》(Hallam Foe)
    [英國](../Page/英國.md "wikilink")，[大衛.麥坎茨](../Page/大衛.麥坎茨.md "wikilink")(David
    Mackenzie)導演
  - 傑出藝術成就銀熊獎：《[特務風雲：中情局誕生秘辛](../Page/特務風雲：中情局誕生秘辛.md "wikilink")》(The
    Good Shepherd)
    [美國](../Page/美國.md "wikilink")，[罗伯特·德尼罗](../Page/罗伯特·德尼罗.md "wikilink")（Robert
    De Niro）導演
  - 最佳電影首部作獎：《[瓦娜雅](../Page/瓦娜雅.md "wikilink")》(Vanaja)
    [印度](../Page/印度.md "wikilink")，[Rajnesh
    Domalpalli導演](../Page/Rajnesh_Domalpalli.md "wikilink")
  - [阿爾弗萊德獎](../Page/阿爾弗萊德獎.md "wikilink")([敢鬥獎](../Page/敢鬥獎.md "wikilink"))：《[賽柏格之戀](../Page/賽柏格之戀.md "wikilink")》(Saibogujiman
    kwenchana)
    [韓國](../Page/大韓民國.md "wikilink")，[朴贊郁](../Page/朴贊郁.md "wikilink")(Park
    Chan-wook)導演
  - 最佳影片金熊獎(短片)：《[Raak](../Page/Raak.md "wikilink")》
    [荷蘭](../Page/荷蘭.md "wikilink")，[Hanro
    Smitsman導演](../Page/Hanro_Smitsman.md "wikilink")
  - 評審團大獎銀熊獎(短片)：《[Decroche](../Page/Decroche.md "wikilink")》
    [法國](../Page/法國.md "wikilink")，[曼紐.沙比哈](../Page/曼紐.沙比哈.md "wikilink")(Manuel
    Schapira)導演 與 《[美](../Page/美.md "wikilink")》
    [台灣](../Page/中華民國.md "wikilink")，[陳駿霖](../Page/陳駿霖.md "wikilink")（Arvin
    Chen）導演
  - [泰迪熊最佳影片獎](../Page/泰迪熊最佳影片獎.md "wikilink")：《[刺青](../Page/刺青_\(電影\).md "wikilink")》[台灣](../Page/中華民國.md "wikilink")，[周美玲導演](../Page/周美玲.md "wikilink")
  - 最優秀亞洲電影獎：《》，[桃井薰導演](../Page/桃井薰.md "wikilink")

## 參照

  - [柏林電影節](../Page/柏林電影節.md "wikilink")

## 外部連結

  - [柏林国际电影节網頁-英語](http://www.berlinale.de/en/HomePage.html)
  - [第57届柏林电影节之星光乍泄](http://www.dw-world.de/dw/article/0,,2332661,00.html?maca=chi-rss-chi-all-1127-rdf)
  - [2007柏林影展爆冷奇蹟
    ◎膝關節](https://web.archive.org/web/20070219195023/http://app.atmovies.com.tw/eweekly/eweekly.cfm?action=edata&vol=103&eid=1103001)
  - [競賽片奇觀：什麼都有什麼都不怪◎膝關節](https://web.archive.org/web/20070220032440/http://app.atmovies.com.tw/eweekly/eweekly.cfm?action=edata&vol=103&eid=1103002)
  - [柏林當道，好萊塢只有陪榜的份◎膝關節](https://web.archive.org/web/20070212220845/http://app.atmovies.com.tw/eweekly/eweekly.cfm?action=edata&vol=103&eid=1103003)

[57](../Category/柏林电影节.md "wikilink")
[Category:2007年德国](../Category/2007年德国.md "wikilink")
[\*](../Category/2007年電影.md "wikilink")
[Category:2007年2月](../Category/2007年2月.md "wikilink")