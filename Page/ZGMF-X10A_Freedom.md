**ZGMF-X10A Freedom
自由鋼彈**為[日本](../Page/日本.md "wikilink")[電視](../Page/電視.md "wikilink")[動畫](../Page/動畫.md "wikilink")[機動戰士鋼彈
SEED系列中登場](../Page/機動戰士GUNDAM_SEED.md "wikilink")，虛構的有人式人型機器兵器（[MS](../Page/MS.md "wikilink")），主人翁[煌·大和於故事後半搭乘之座機](../Page/煌·大和.md "wikilink")。機體名「Freedom」為[英語的](../Page/英語.md "wikilink")「自由」之意，機體設計為[大河原邦男](../Page/大河原邦男.md "wikilink")。
[Zgmf-x10a.jpg](https://zh.wikipedia.org/wiki/File:Zgmf-x10a.jpg "fig:Zgmf-x10a.jpg")

## 開發經過

在[派屈克·薩拉指示下](../Page/派屈克·薩拉.md "wikilink")，國力、物量均劣於[地球聯合的](../Page/地球聯合.md "wikilink")[P.L.A.N.T.利用](../Page/P.L.A.N.T..md "wikilink")[Z.A.F.T奪取自](../Page/ZAFT.md "wikilink")[大西洋聯邦的四機](../Page/大西洋聯邦.md "wikilink")[G兵器](../Page/G兵器.md "wikilink")，即前期GAT-X系列的[決鬥鋼彈](../Page/GAT-X102_Duel.md "wikilink")、[暴風鋼彈](../Page/GAT-X103_Buster.md "wikilink")、[電擊鋼彈和](../Page/GAT-X207_Blitz.md "wikilink")[神盾鋼彈等機體資料](../Page/GAT-X303_Aegis.md "wikilink")，統合[Z.A.F.T三個設計局開發出酷似](../Page/ZAFT.md "wikilink")[G兵器的試作型](../Page/G兵器.md "wikilink")[MS](../Page/MS.md "wikilink")。C.E.71年4月1日，本機**ZGMF-X10A**和兄弟機**ZGMF-X09A**共同出廠。打著「對[自然人落下正義的鐵槌](../Page/自然人.md "wikilink")，為[調整者贏得真正的自由](../Page/调整者_\(GUNDAM\).md "wikilink")」的旗號，將X09A命名為「**正義**（Justice）」，本機X10A則命名為「**自由**（Freedom）」。型號末尾的X和A為**eXperiment
Atomic**略稱，意味「核動力搭載型試作機」。

## 機體介紹

以多數敵機為對手，單機展示壓倒的戰鬥力，本機作為對抗[地球聯合軍的一張王牌而被開發出來](../Page/地球聯合軍.md "wikilink")。本機搭載可消除N干擾（中子干擾）效果的**反中子干擾器**，以核引擎為動力，獲得了近乎無限的能源。這令本機的兵裝出力大幅提升，且活動時間延長遠超越過去的機體，[PS裝甲亦不再失效](../Page/相轉移裝甲.md "wikilink")，從根本上解決了高功率光束兵器與[PS裝甲雙重運作下令能量消耗迅速的問題](../Page/相轉移裝甲.md "wikilink")（不過由於彈藥、推進劑、氧氣等仍是有限的，故並非完全的獨立運作）。另外為了實現穩定運作與控制，安裝了專用的MOBILE
SUIT NEO OPERATION SYSTEM 「**G.U.N.D.A.M Complex**」，機體操作性更佳。

本機武裝備有平衡的格鬥、實彈、光束兵器，且考慮到以戰術強襲機**M.E.T.E.O.R（流星）**施展飽和攻擊，通過火器的統合管制裝置**多重鎖定系統**可以同時對多數敵機進行精密狙擊，本機以煌·大和作為駕駛員時，同時攻擊10台以上也是可能的\[1\]，駕駛艙內部採用了與機體動作連動的迴轉型全周圍顯示屏幕，控制方面搭載了對應多重鎖定的球型立體顯示面板，在最大限度發揮機體的潛力上花了不少工夫。

本機的最大特徵是，其推力可做到大氣圈內高速、長距離飛行，背部主推進器基座兩側備有左右五對共十只的**高可動空氣動力彈性翼（Active
Aeroelastic
Wing）**。由電腦控制形狀變化，包括在大氣圈內抵抗空氣阻力以及宇宙空間的質量移動。機翼內藏小型推進器，亦兼備散熱板的作用，亦有助於完全活用核能的本機提升信賴性。可簡易變形為最適形態以發揮機動面與攻擊面，高機動空戰形態為「**High
M.A.T. Mode（High Maneuver Aerial Tactical Mode）**」，全武裝發射形態則為「**Full
Burst Mode**」。

頭部兩側裝備**MMI-GAU2
「[啄木鳥式](../Page/啄木鳥.md "wikilink")」76mm近接防禦機關砲**，[蓋茲亦有搭載](../Page/ZGMF-600_GuAIZ.md "wikilink")，命名為啄木鳥是來自其速射性。**MMI-M15
「[旗魚式](../Page/旗魚.md "wikilink")」軌道砲**是兩腰部兩側設置的電磁軌砲兼推進器，發展自[決鬥鋼彈突擊護甲](../Page/GAT-X102_Duel.md "wikilink")（Assault
Shroud）搭載的電磁軌砲「破壞神」。高速射擊可比肩[重砲型攻擊鋼彈的火力](../Page/GAT-X105_攻擊鋼彈#GAT-X105_Strike_+_AQM/E-X03_Launcher.md "wikilink")，攜行彈數多，可同時攻擊多數敵機。此外靠著實體彈，也能夠針對積層裝甲等對光束裝甲或船艦作打擊。側面有加強固定性的輔助把柄，平時是三折的狀態，作為關係到姿勢控制的AMBA（動態質量平衡自動控制）單位，也有推進器機能。另外，亦兼作光束軍刀的刀架，後部還有排除廢熱的導管。與[正義鋼彈共通的](../Page/ZGMF-X09A_Justice.md "wikilink")**MA-M01
「[蜥式](../Page/蜥.md "wikilink")」光束軍刀**，通過核能供給，形成比既有同種類兵器更高出力的長刀身光束刃。將兩刀柄相互連結，被稱為「**Ambidextrous
Halberd（雙頭戟）**」的雙刃長刀狀態也是可以的。比機體更早完成的**MA-M20
「[狼式](../Page/狼.md "wikilink")」光束步槍**，經YFX-600R
火器運用試驗型[蓋茲改](../Page/ZGMF-600_GuAIZ.md "wikilink")（GuAIZ
Type Fire Arms
Experimental）測試後採用，除顏色塗裝之外和[正義鋼彈共通的兵裝](../Page/ZGMF-X09A_Justice.md "wikilink")。跟光束軍刀同樣因為核能供給，比既有同種類的出力更高。最上部兩只機翼內藏的光束加農砲**M100
「[鯨式](../Page/鯨.md "wikilink")」等離子收束光束砲**，單單一門的威力與射程即可匹敵[重砲型攻擊鋼彈的](../Page/GAT-X105_突擊高達#GAT-X105_Strike_+_AQM/E-X03_Launcher.md "wikilink")「炎神」高脈衝砲，擁有本機火器之中最大的破壞力。同「[旗魚式](../Page/旗魚.md "wikilink")」軌道砲一樣，後部存在排除廢熱的導管。在以[蓋茲進行試驗的階段](../Page/ZGMF-600_GuAIZ.md "wikilink")，結果是僅僅兩發便讓電池乾涸，但受惠於核能解決了諸多問題而得以實裝。本機攜行一面**積層反光束盾**，也是和[正義鋼彈僅顏色塗裝不同的共通裝備](../Page/ZGMF-X09A_Justice.md "wikilink")。最大特徵是以積層裝甲製成，相異於其他勢力產的以振動鋼材製的對光束防盾，是兩者差別化的要因。

本機作為[C.E.](../Page/C.E..md "wikilink")
71年[Z.A.F.T最新銳](../Page/ZAFT.md "wikilink")[MS之一](../Page/MS.md "wikilink")，投入了許多最新技術，但控制也變得複雜化，一般調整者掌握不了。據說[P.L.A.N.T原定安排本機由](../Page/P.L.A.N.T.md "wikilink")[伊薩克·焦耳作為預定駕駛員](../Page/伊薩克·焦耳.md "wikilink")\[2\]，但最終由[煌·大和搭乘](../Page/煌·大和.md "wikilink")，使本機性能不留遺憾地發揮出來。

## 機體諸元

  - 型號：ZGMF-X10A
  - 名稱：Freedom
  - 建造：[P.L.A.N.T.](../Page/P.L.A.N.T..md "wikilink")
  - 所屬：[Z.A.F.T](../Page/ZAFT.md "wikilink")→[三艦同盟](../Page/三舰同盟.md "wikilink")（SEED）、Treminal（SEED
    DESTINY）\[3\]
  - 分類：[Z.A.F.T](../Page/ZAFT.md "wikilink")
    [鋼彈目](../Page/鋼彈.md "wikilink")‧殲滅型對MS戰用[MS](../Page/MS.md "wikilink")\[4\]
  - 生產型態：試作機
  - 全高：18.03公尺\[5\]\[6\]
  - 重量：71.50公噸\[7\]\[8\]
  - 裝甲材質：不明\[9\]
  - 動力源：核引擎（MHD發電）\[10\]
  - 出力：8,826千瓦\[11\]
  - 推力：最大527,000kg\[12\]
  - 作業系統：MOBILE SUIT NEO OPERATION SYSTEM「**G**eneration **U**nsubdued
    **N**uclear **D**rive **A**ssault **M**odule Complex」
  - 駕駛：[煌·大和](../Page/煌·大和.md "wikilink")\[13\]

### 特殊裝備或機能

  - 反中子干擾器
  - 多重鎖定系統
  - [MS嵌入式戰術強襲機](../Page/MS.md "wikilink")「M.E.T.E.O.R」（01號機）

## 機體武裝

  - MMI-GAU2 「[啄木鳥式](../Page/啄木鳥.md "wikilink")」76mm近接防禦機關砲 × 2
  - MMI-M15 「[旗魚式](../Page/旗魚.md "wikilink")」軌道砲 × 2
  - MA-M01 「[蜥式](../Page/蜥.md "wikilink")」光束軍刀 × 2
  - MA-M20 「[狼式](../Page/狼.md "wikilink")」光束步槍
  - M100「[鯨式](../Page/鯨.md "wikilink")」等離子收束光束砲 × 2
  - 積層反光束盾

## 戰鬥史

### [C.E.](../Page/C.E..md "wikilink") 71年

  - 5月5日　自由鋼彈強奪事件：[拉克絲·克萊因竊取本機交付予](../Page/拉克絲·克萊因.md "wikilink")[煌·大和](../Page/煌·大和.md "wikilink")，本機自L5宙域[P.L.A.N.T.本國的](../Page/P.L.A.N.T..md "wikilink")[雅金·杜威突圍離去](../Page/雅金·杜威.md "wikilink")。
  - 5月5日　[阿拉斯加防衛戰](../Page/阿拉斯加.md "wikilink")：本機於[地球聯合軍總部](../Page/地球聯合#地球連合軍.md "wikilink")‧[阿拉斯加的](../Page/阿拉斯加.md "wikilink")[約書亞基地外空域首次參戰](../Page/約書亞.md "wikilink")，從天而降及時救援了千鈞一髮的[大天使號](../Page/LCAM-01XA_Archangel.md "wikilink")。緊接著本機壓制了[Z.A.F.T的](../Page/ZAFT.md "wikilink")[MS部隊進攻](../Page/MS.md "wikilink")，並擊退[伊薩克·焦耳的](../Page/伊薩克·焦耳.md "wikilink")[決鬥鋼彈](../Page/GAT-X102_Duel.md "wikilink")，同時勸誘兩軍脫離[大西洋聯邦的陰謀](../Page/大西洋聯邦.md "wikilink")——[獨眼巨人](../Page/獨眼巨人.md "wikilink")。
  - 6月15\~16日　[歐普防衛戰](../Page/O.R.B..md "wikilink")：本機參與了[歐普抵禦](../Page/O.R.B..md "wikilink")[大西洋聯邦侵攻的防衛作戰](../Page/大西洋聯邦.md "wikilink")，先於前線擊破眾多的[攻擊刃](../Page/GAT-01_Strike_Dagger.md "wikilink")；再以一敵三，單獨面對[大西洋聯邦後期GAT](../Page/大西洋聯邦.md "wikilink")-X系列的新型機[瘟神鋼彈](../Page/GAT-X131_Calamity.md "wikilink")、[禁斷鋼彈和](../Page/GAT-X252_Forbidden.md "wikilink")[侵略鋼彈圍攻](../Page/GAT-X370_Raider.md "wikilink")，陷入苦戰。之後得到[阿斯蘭·薩拉的](../Page/阿斯蘭·薩拉.md "wikilink")[正義鋼彈援護](../Page/ZGMF-X09A_Justice.md "wikilink")，地球軍攻勢受阻，暫時撤退。翌日，本機偕[正義鋼彈於輝夜宇宙港阻擋了](../Page/ZGMF-X09A_Justice.md "wikilink")[瘟神鋼彈](../Page/GAT-X131_Calamity.md "wikilink")、[禁斷鋼彈及](../Page/GAT-X252_Forbidden.md "wikilink")[侵略鋼彈的追擊](../Page/GAT-X370_Raider.md "wikilink")，成功保護[大天使號和](../Page/LCAM-01XA_Archangel.md "wikilink")[草薙號升空撤離](../Page/草薙號.md "wikilink")[歐普](../Page/O.R.B..md "wikilink")。
  - 7月1日　本機護送[阿斯兰·萨拉搭乘的穿梭機至](../Page/阿斯兰·萨拉.md "wikilink")[雅金·杜威的警戒範圍後潛伏於附近宙域](../Page/雅金·杜威.md "wikilink")。
  - 7月4日　本機發現並護衛[永恆號脫離](../Page/FFMH-Y101_Eternal.md "wikilink")[P.L.A.N.T.](../Page/P.L.A.N.T..md "wikilink")，癱瘓了來自[雅金·杜威的追兵](../Page/雅金·杜威.md "wikilink")，翌日抵達L4宙域的[孟德爾與](../Page/孟德爾.md "wikilink")[大天使號合流](../Page/LCAM-01XA_Archangel.md "wikilink")。
  - 7月12日　本機與[主天使號的](../Page/LCAM-02XD_Dominion.md "wikilink")[瘟神鋼彈](../Page/GAT-X131_Calamity.md "wikilink")、[禁斷鋼彈及](../Page/GAT-X252_Forbidden.md "wikilink")[侵略鋼彈交戰](../Page/GAT-X370_Raider.md "wikilink")，不久後脫離戰場進入[孟德爾內部](../Page/孟德爾.md "wikilink")，遭遇了[拉烏·鲁·克鲁澤駕駛的](../Page/拉烏·鲁·克鲁澤.md "wikilink")[蓋茲](../Page/ZGMF-600_GuAIZ.md "wikilink")。歸返時執意救援[芙蕾·阿爾斯達搭乘的救生艇](../Page/芙蕾·阿爾斯達.md "wikilink")，背部機翼及頭部遭[侵略鋼彈擊中受損](../Page/GAT-X370_Raider.md "wikilink")。
  - 9月26\~27日　[第二次雅金·杜威攻防戰](../Page/第二次雅金·杜威攻防戰.md "wikilink")：本機同[正義鋼彈聯手以](../Page/ZGMF-X09A_Justice.md "wikilink")[M.E.T.E.O.R擊落地球軍向](../Page/METEOR.md "wikilink")[P.L.A.N.T.本國發射之核彈群](../Page/P.L.A.N.T..md "wikilink")，並癱瘓地球軍與[Z.A.F.T.雙方的眾多](../Page/ZAFT.md "wikilink")[MS](../Page/MS.md "wikilink")，接著與[瘟神鋼彈](../Page/GAT-X131_Calamity.md "wikilink")、[禁斷鋼彈及](../Page/GAT-X252_Forbidden.md "wikilink")[侵略鋼彈激戰](../Page/GAT-X370_Raider.md "wikilink")。後期則與[拉烏·鲁·克鲁澤的](../Page/拉烏·鲁·克鲁澤.md "wikilink")[天帝鋼彈展開死鬥](../Page/ZGMF-X13A_Providence.md "wikilink")，本機的頭部、左胸、右臂與右足嚴重損毀，但最後以特攻擊破[天帝鋼彈的駕駛艙](../Page/ZGMF-X13A_Providence.md "wikilink")，終結戰爭。\[14\]

### [C.E.](../Page/C.E..md "wikilink") 73年10月3日後

  - [拉克絲·克萊因暗殺未遂事件](../Page/拉克絲·克萊因.md "wikilink")：前大戰後，本機被秘密地回收和修復，並藏匿於[歐普境內阿斯哈家別邸的地下防空洞](../Page/O.R.B..md "wikilink")。兩年後的[C.E.](../Page/C.E..md "wikilink")
    73年10月（[機動戰士鋼彈 SEED
    DESTINY](../Page/機動戰士鋼彈_SEED_DESTINY.md "wikilink")），由[調整者組成的特殊部隊疑似試圖暗殺](../Page/调整者_\(GUNDAM\).md "wikilink")[拉克絲·克萊因](../Page/拉克絲·克萊因.md "wikilink")，本機復出並擊破數機[Z.A.F.T.新型兩棲用](../Page/ZAFT.md "wikilink")[MS](../Page/MS.md "wikilink")
    [亞修](../Page/UMF/SSO-3_ASH.md "wikilink")。\[15\]
  - [卡嘉里·由拉·阿斯哈拐走事件](../Page/卡嘉莉·尤拉·阿斯哈.md "wikilink")：本機介入[歐普代表首長](../Page/歐普.md "wikilink")[卡嘉里·由拉·阿斯哈的結婚典禮](../Page/卡嘉莉·尤拉·阿斯哈.md "wikilink")，並強行將其帶離[歐普](../Page/歐普.md "wikilink")，並奪去了第一隊追兵[村雨的戰鬥能力](../Page/MVF-M11C_Murasame.md "wikilink")。\[16\]
  - [達達尼爾海峽之戰](../Page/達達尼爾海峽.md "wikilink")：本機介入[地球聯合軍偕](../Page/地球聯合#地球連合軍.md "wikilink")[歐普派遣軍的聯合艦隊與](../Page/O.R.B..md "wikilink")[智慧女神號之間的戰鬥](../Page/LHM-BB01_Minerva.md "wikilink")，就在[智慧女神號以陽電子砲朝向](../Page/LHM-BB01_Minerva.md "wikilink")[歐普艦隊發射的寸刻之前](../Page/O.R.B..md "wikilink")，本機突然降臨並破壞砲身，接著針對兩軍的[MS展開無差別攻擊](../Page/MS.md "wikilink")。本機癱瘓了眾多量產機，切斷脈衝鋼彈的右手、擊傷深淵鋼彈的推進器，引發的混亂間接造成智慧女神號的海涅·威斯坦弗斯駕駛的古夫烈燄型慘遭蓋亞鋼彈腰斬。
  - 迪歐奇亞劫機事件：[拉克絲·克萊因搭乘穿梭機自](../Page/拉克絲·克萊因.md "wikilink")[黑海南岸的迪歐基亞基地起飛前往](../Page/黑海.md "wikilink")[P.L.A.N.T.](../Page/P.L.A.N.T..md "wikilink")，本機輕鬆擊退[Z.A.F.T的追擊後離去](../Page/ZAFT.md "wikilink")。
  - [克里特外海戰](../Page/克里特.md "wikilink")：本機再次介入[聯合與](../Page/地球聯合#地球連合軍.md "wikilink")[歐普同盟軍對](../Page/O.R.B..md "wikilink")[智慧女神號之間的戰鬥](../Page/LHM-BB01_Minerva.md "wikilink")，一度與[脈衝鋼彈交手未果](../Page/ZGMF-X56S_Impulse.md "wikilink")。後來雖大破[阿斯蘭˙·薩拉的](../Page/阿斯蘭˙·薩拉.md "wikilink")[救星鋼彈](../Page/ZGMF-X23S_Saviour.md "wikilink")，但已來不及阻止[歐普軍艦隊被](../Page/O.R.B..md "wikilink")[脈衝鋼彈殲滅](../Page/ZGMF-X56S_Impulse.md "wikilink")。
  - [柏林之戰](../Page/柏林.md "wikilink")：本機試圖阻止巨大[MS](../Page/MS.md "wikilink")[破滅鋼彈破壞市區但未見效果](../Page/GFAS-X1_Destroy.md "wikilink")，期間擊墜了[尼奧·羅安克搭乘之](../Page/尼奧·羅安克.md "wikilink")[威達](../Page/GAT-04_Windam.md "wikilink")。後來由於[脈衝鋼彈的介入](../Page/ZGMF-X56S_Impulse.md "wikilink")，本機抓住[破滅鋼彈發射光束砲的瞬間](../Page/GFAS-X1_Destroy.md "wikilink")，以光束軍刀破壞其胸部的砲口；雖然成功擊倒破滅鋼彈，但砲口引發的爆炸造成其駕駛員[史黛菈·路歇傷重死亡](../Page/史黛菈·路歇.md "wikilink")。\[17\]
  - Angel
    Down：本機與[Z.A.F.T年輕王牌](../Page/ZAFT.md "wikilink")[真·飛鳥駕駛的](../Page/真·飛鳥.md "wikilink")[脈衝鋼彈展開對決](../Page/ZGMF-X56S_Impulse.md "wikilink")。由於對手實力與以往的敵人不同，且須不時關注大天使號的情況而落居劣勢，本機的左肩和左翼受損。最終在脈衝鋼彈的特攻下，本機腹部的機關部遭貫穿，機身爆炸，殘骸散落海面，不敗的傳說終結。[煌‧大和在最後一刻及时關閉核引擎](../Page/煌‧大和.md "wikilink")，使爆炸威力減弱得以倖存，駕駛艙隨後由[嫣紅攻擊鋼彈回收](../Page/嫣紅攻擊鋼彈.md "wikilink")。

## 參考文獻

## 參見

  - [ZGMF-X09A Justice](../Page/ZGMF-X09A_Justice.md "wikilink")
  - [ZGMF-X13A Providence](../Page/ZGMF-X13A_Providence.md "wikilink")
  - [ZGMF-X19A Infinite
    Justice](../Page/ZGMF-X19A_Infinite_Justice.md "wikilink")
  - [ZGMF-X20A Strike
    Freedom](../Page/ZGMF-X20A_Strike_Freedom.md "wikilink")

[Category:C.E.兵器](../Category/C.E.兵器.md "wikilink")

1.  RG自由鋼彈說明書

2.  自由鋼彈mg1.0說明書(只有暗示，沒有明說)

3.  ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

4.  MG自由鋼彈說明書

5.
6.  ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

7.
8.  ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

9.  ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

10. ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

11. ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

12. 機動戦士ガンダムSEEDコズミック・イラメカニック&ワールド(双葉社2012)

13. ガンダム パーフェクトファイル（デアゴスティーニ･ジャパン）

14. 機動戰士-{鋼彈}-SEED{{〈}}5{{〉}}飛向無止盡的明天 ISBN 986-7299-59-0

15. 機動戰士鋼彈SEED DESTINY{{〈}}2{{〉}}迷惘的眼神 ISBN 986-174-099-6

16.
17. 機動戰士鋼彈SEED DESTINY{{〈}}3{{〉}}錯過的視線 ISBN 986-174-099-6