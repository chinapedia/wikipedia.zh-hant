## 大事记

  - [保时捷公司成立](../Page/保時捷.md "wikilink")。
  - [天王星卫星](../Page/天王星.md "wikilink")[米兰达被发现](../Page/米兰达.md "wikilink")。
  - [卡西米尔效应被发现](../Page/卡西米尔效应.md "wikilink")。

### 1月

  - [1月1日](../Page/1月1日.md "wikilink")——[中国国民党革命委员会成立](../Page/中国国民党革命委员会.md "wikilink")。
  - [1月4日](../Page/1月4日.md "wikilink")——[缅甸独立](../Page/缅甸.md "wikilink")。
  - [1月28日](../Page/1月28日.md "wikilink")——[田中軍吉](../Page/田中軍吉.md "wikilink")、[野田毅](../Page/野田毅.md "wikilink")、[向井敏明](../Page/向井敏明.md "wikilink")
    南京大屠殺犯執行槍決。
  - [1月30日](../Page/1月30日.md "wikilink")——[冬季奥运会在](../Page/冬季奥林匹克运动会.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[圣莫里茨开幕](../Page/圣莫里茨.md "wikilink")。
  - [1月31日](../Page/1月31日.md "wikilink")——[Star
    Tiger](../Page/Star_Tiger.md "wikilink")
    在[百慕達失事](../Page/百慕達.md "wikilink")

### 2月

  - [2月4日](../Page/2月4日.md "wikilink")——[斯里兰卡独立](../Page/斯里蘭卡.md "wikilink")。
  - [2月24日](../Page/2月24日.md "wikilink")——[捷克斯洛伐克共产党上台](../Page/捷克斯洛伐克共產黨.md "wikilink")。

### 3月

  - [3月29日](../Page/3月29日.md "wikilink")，中華民國[行憲國民大會於南京集會](../Page/行憲國民大會.md "wikilink")，選舉總統、副總統。
  - [新加坡第一次选举](../Page/新加坡.md "wikilink")。

### 4月

  - [4月3日](../Page/4月3日.md "wikilink")——[哈利·S·杜鲁门总统签署](../Page/哈利·S·杜鲁门.md "wikilink")[马歇尔计划](../Page/马歇尔计划.md "wikilink")。
  - [4月7日](../Page/4月7日.md "wikilink")——[世界卫生组织成立](../Page/世界卫生组织.md "wikilink")。
  - [4月8日](../Page/4月8日.md "wikilink")——泰国[銮披汶·颂堪元帅与](../Page/銮披汶·颂堪.md "wikilink")“政变小组”上台。
  - [4月9日](../Page/4月9日.md "wikilink")——[代爾亞辛村大屠殺](../Page/代爾亞辛村大屠殺.md "wikilink")。

### 5月

  - [5月10日](../Page/5月10日.md "wikilink")——韩国首次选举。
  - [5月10日](../Page/5月10日.md "wikilink")——[中華民國公布實施](../Page/中華民國.md "wikilink"){{〈}}[動員戡亂時期臨時條款](../Page/動員戡亂時期臨時條款.md "wikilink"){{〉}}，賦予總統緊急處分權。
  - [5月14日](../Page/5月14日.md "wikilink")——[以色列国成立](../Page/以色列国.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——[埃及](../Page/埃及.md "wikilink")、[约旦](../Page/约旦.md "wikilink")、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[叙利亚](../Page/叙利亚.md "wikilink")、[伊拉克和](../Page/伊拉克.md "wikilink")[沙特阿拉伯合攻以色列](../Page/沙特阿拉伯.md "wikilink")。[第一次中东战争爆发](../Page/第一次中东战争.md "wikilink")。
  - [5月16日](../Page/5月16日.md "wikilink")——[哈伊姆·魏茨曼当选以色列总统](../Page/哈伊姆·魏茨曼.md "wikilink")。
  - [5月18日](../Page/5月18日.md "wikilink")——[中華民國第一屆](../Page/中華民國.md "wikilink")[立法院院會在](../Page/立法院.md "wikilink")[南京市召開](../Page/南京市.md "wikilink")。
  - [5月20日](../Page/5月20日.md "wikilink")——[蔣中正在](../Page/蔣中正.md "wikilink")[南京就任第一任](../Page/南京.md "wikilink")[中華民國總統](../Page/中華民國總統.md "wikilink")，[國民政府正式改組為](../Page/國民政府.md "wikilink")[中華民國政府](../Page/中華民國政府.md "wikilink")。
  - [5月23日](../Page/5月23日.md "wikilink")——[长春围困战开始](../Page/长春围困战.md "wikilink")
  - [5月25日](../Page/5月25日.md "wikilink")——[日本](../Page/日本.md "wikilink")[警視廳預備隊創立](../Page/警視廳預備隊.md "wikilink")。

### 6月

  - [6月20日](../Page/6月20日.md "wikilink")——[美](../Page/美國.md "wikilink")、[英](../Page/英国.md "wikilink")、[法占领的](../Page/法国.md "wikilink")[德国地区开始使用](../Page/德国.md "wikilink")[西德马克](../Page/德國馬克.md "wikilink")。
  - [6月23日](../Page/6月23日.md "wikilink")——[苏占德国地区开始使用](../Page/苏联.md "wikilink")[东德马克](../Page/东德马克.md "wikilink")。
  - [6月24日](../Page/6月24日.md "wikilink")——[柏林封锁开始](../Page/柏林封鎖.md "wikilink")。
  - [6月26日](../Page/6月26日.md "wikilink")——第一架[柏林空桥飞机抵达](../Page/柏林空桥.md "wikilink")[柏林](../Page/柏林.md "wikilink")。

### 7月

  - [7月29日](../Page/7月29日.md "wikilink")——[第十四屆夏季奧林匹克運動會在英国](../Page/1948年夏季奥林匹克运动会.md "wikilink")[倫敦开幕](../Page/伦敦.md "wikilink")。

### 8月

  - [8月15日](../Page/8月15日.md "wikilink")——[大韩民国建立](../Page/大韩民国.md "wikilink")。
  - [8月19日](../Page/8月19日.md "wikilink")——[中華民國政府發行](../Page/中華民國政府.md "wikilink")[金圆券](../Page/金圓券.md "wikilink")。

### 9月

  - [9月6日](../Page/9月6日.md "wikilink")——[荷兰](../Page/荷兰.md "wikilink")[朱莉安娜女王登基](../Page/朱麗安娜_\(荷蘭\).md "wikilink")。
  - [9月8日](../Page/9月8日.md "wikilink")——[朝鮮民主主義人民共和國正式使用现在的](../Page/朝鲜民主主义人民共和国.md "wikilink")[朝鲜国旗](../Page/朝鲜国旗.md "wikilink")。
  - [9月12日](../Page/9月12日.md "wikilink")——[辽沈战役開始](../Page/辽沈战役.md "wikilink")。

### 10月

  - [10月15日](../Page/10月15日.md "wikilink")——解放军占领[锦州](../Page/锦州.md "wikilink")
  - [10月19日](../Page/10月19日.md "wikilink")——解放军占领[长春](../Page/长春.md "wikilink")，[长春围困战结束](../Page/长春围困战.md "wikilink")

### 11月

  - [11月2日](../Page/11月2日.md "wikilink")——美国大选，杜鲁门总统获得连任。
  - [11月2日](../Page/11月2日.md "wikilink")——解放军占领[沈阳](../Page/沈阳.md "wikilink")，辽沈战役结束。
  - [11月6日](../Page/11月6日.md "wikilink")——[淮海战役开始](../Page/淮海战役.md "wikilink")。
  - [11月12日](../Page/11月12日.md "wikilink")——[遠東国际軍事法庭判处七名](../Page/遠東国际軍事法庭.md "wikilink")[日本战犯死刑](../Page/日本.md "wikilink")。
  - [11月22日](../Page/11月22日.md "wikilink")——第二次国共内战[淮海战役第一阶段结束](../Page/淮海战役.md "wikilink")，共军获得大胜。
  - [11月27日](../Page/11月27日.md "wikilink")——[中华民国行政院](../Page/中华民国行政院.md "wikilink")《[匪区海上交通经济封锁暨处理截获匪资办法](../Page/匪区海上交通经济封锁暨处理截获匪资办法.md "wikilink")》，意图封锁中国共产党政权控制下的[解放区](../Page/解放区.md "wikilink")。此后形成[关闭政策](../Page/关闭政策.md "wikilink")，自1950年代起封锁[台湾海峡三十年](../Page/台湾海峡.md "wikilink")。

### 12月

  - [12月1日](../Page/12月1日.md "wikilink")——[中国人民银行成立并开始发行人民币](../Page/中国人民银行.md "wikilink")。
  - [12月3日](../Page/12月3日.md "wikilink")——[江亚轮在上海](../Page/江亚轮.md "wikilink")[吴淞口外沉没](../Page/吴淞口.md "wikilink")，3000余人遇难。是为20世纪中国最大的海难。
  - [12月5日](../Page/12月5日.md "wikilink")——[平津战役开始](../Page/平津战役.md "wikilink")。
  - [12月10日](../Page/12月10日.md "wikilink")——[联合国大会通过](../Page/联合国大会.md "wikilink")[世界人权宣言](../Page/世界人权宣言.md "wikilink")。
  - [12月15日](../Page/12月15日.md "wikilink")——解放军攻下[徐州](../Page/徐州.md "wikilink")
  - [12月24日](../Page/12月24日.md "wikilink")——蒋介石亲自下令查封由知名人士储安平创办的上海《[观察](../Page/觀察_\(雜誌\).md "wikilink")》杂志和逮捕《观察》工作人员，这就是中国近代史上著名的“《观察》事件”

## 出生

  - [1月7日](../Page/1月7日.md "wikilink")——[水木一郎](../Page/水木一郎.md "wikilink")，日本動畫歌手、作詞家、作曲家、聲優及演員
  - [1月21日](../Page/1月21日.md "wikilink")——[溫世仁](../Page/溫世仁.md "wikilink")，台灣企業家，[英業達集團副董事長](../Page/英業達集團.md "wikilink")。（[2003年逝世](../Page/2003年.md "wikilink")）
  - [2月11日](../Page/2月11日.md "wikilink")——[桂宮宜仁親王](../Page/桂宮宜仁親王.md "wikilink")，日本[天皇](../Page/天皇.md "wikilink")[明仁堂弟](../Page/明仁.md "wikilink")。（[2014年逝世](../Page/2014年.md "wikilink")）
  - [2月12日](../Page/2月12日.md "wikilink")——[雷洪](../Page/雷洪.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [3月14日](../Page/3月14日.md "wikilink")——[何家駒](../Page/何家駒.md "wikilink")，著名香港及中國演員。
  - [3月17日](../Page/3月17日.md "wikilink")——[威廉·吉布森](../Page/威廉·吉布森.md "wikilink")，作家
  - [3月24日](../Page/3月24日.md "wikilink")——[張聖容](../Page/張聖容.md "wikilink")，數學家
  - [3月31日](../Page/3月31日.md "wikilink")——[艾伯特·戈尔](../Page/阿尔·戈尔.md "wikilink")，美国政治家，副总统
  - [4月1日](../Page/4月1日.md "wikilink")——[柯安龍](../Page/柯安龍.md "wikilink")，英國外交官，前[駐台貿易文化辦事處處長](../Page/英國在台辦事處.md "wikilink")、駐菲律賓大使、駐新加坡高級專員、駐紐約總領事
  - [4月4日](../Page/4月4日.md "wikilink")——[丹·西蒙斯](../Page/丹·西蒙斯.md "wikilink")，美国作家，著有《[海伯利安](../Page/海伯利安.md "wikilink")》等
  - [4月7日](../Page/4月7日.md "wikilink")——[洪秀柱](../Page/洪秀柱.md "wikilink")，[中華民國第](../Page/中華民國.md "wikilink")14任第8屆[立法院副院長](../Page/立法院.md "wikilink")
  - [5月20日](../Page/5月20日.md "wikilink")——[玄田哲章](../Page/玄田哲章.md "wikilink")，日本声优
  - [6月25日](../Page/6月25日.md "wikilink")——[澤田研二](../Page/澤田研二.md "wikilink")，日本歌手
  - [7月7日](../Page/7月7日.md "wikilink")——[西村真悟](../Page/西村真悟.md "wikilink")，日本政治家
  - [7月30日](../Page/7月30日.md "wikilink")——[尚·雷诺](../Page/尚·雷诺.md "wikilink")，法国演员
  - [8月3日](../Page/8月3日.md "wikilink")——[让-皮埃尔·拉法兰](../Page/让-皮埃尔·拉法兰.md "wikilink")，法国政治家，总理
  - [8月3日](../Page/8月3日.md "wikilink")——[翁啟惠](../Page/翁啟惠.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[中央研究院院長](../Page/中央研究院.md "wikilink")、[院士](../Page/中央研究院院士.md "wikilink")
  - [8月11日](../Page/8月11日.md "wikilink")——[張小燕](../Page/張小燕_\(臺灣\).md "wikilink")，[台灣](../Page/台灣.md "wikilink")，台灣藝人
  - [8月24日](../Page/8月24日.md "wikilink")——[让-米歇尔·雅尔](../Page/让-米歇尔·雅尔.md "wikilink")，法国电子音乐艺术家
  - [9月6日](../Page/9月6日.md "wikilink")——[許冠傑](../Page/許冠傑.md "wikilink")，香港創作歌手及演員
  - [9月20日](../Page/9月20日.md "wikilink")——[乔治·R·R·马丁](../Page/乔治·R·R·马丁.md "wikilink")，美国作家及编剧，著有《[冰与火之歌](../Page/冰与火之歌.md "wikilink")》
  - [10月17日](../Page/10月17日.md "wikilink")——[罗伯特·乔丹](../Page/罗伯特·乔丹.md "wikilink")，美国作家，著有《[时间之轮](../Page/时间之轮.md "wikilink")》
  - [11月13日](../Page/11月13日.md "wikilink")——[由紀さおり](../Page/由紀さおり.md "wikilink"),日本歌手，代表作《[夜明けのスキャット](../Page/夜明けのスキャット.md "wikilink")》
  - [11月14日](../Page/11月14日.md "wikilink")——[查尔斯王子](../Page/查爾斯王子_\(威爾斯親王\).md "wikilink")，威尔士亲王，英国王子
  - [11月24日](../Page/11月24日.md "wikilink")——[王晋康](../Page/王晋康.md "wikilink")，中国科幻作家
  - [12月11日](../Page/12月11日.md "wikilink")——[谷村新司](../Page/谷村新司.md "wikilink")，日本音乐家和歌手，代表作《昴》
  - [12月21日](../Page/12月21日.md "wikilink")——[塞缪尔·L·杰克逊](../Page/塞缪尔·L·杰克逊.md "wikilink")，知名美国黑人演员及监制
  - [12月30日](../Page/12月30日.md "wikilink")——[蘭迪·謝克曼](../Page/蘭迪·謝克曼.md "wikilink")，美國加州大學伯克利分校的細胞生物學家

## 逝世

  - [逝世公告](../Page/Portal:逝世公告.md "wikilink")
  - [1月30日](../Page/1月30日.md "wikilink")——[聖雄甘地](../Page/圣雄甘地.md "wikilink")，[印度政治家](../Page/印度.md "wikilink")（[1869年出生](../Page/1869年.md "wikilink")）
  - [1月30日](../Page/1月30日.md "wikilink")——[奥维尔·莱特](../Page/莱特兄弟.md "wikilink")，美国发明家，飞机发明者（[1871年出生](../Page/1871年.md "wikilink")）
  - [2月1日](../Page/2月1日.md "wikilink")——[谢尔盖·爱森斯坦](../Page/谢尔盖·爱森斯坦.md "wikilink")，[苏联导演](../Page/苏联.md "wikilink")（出生[1898年](../Page/1898年.md "wikilink")）
  - [2月28日](../Page/2月28日.md "wikilink")——[周佛海](../Page/周佛海.md "wikilink")，民國政治人物（[1897年出生](../Page/1897年.md "wikilink")）
  - [3月25日](../Page/3月25日.md "wikilink")——[川島芳子](../Page/川島芳子.md "wikilink")，[日本間諜](../Page/日本.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [6月13日](../Page/6月13日.md "wikilink")——[太宰治](../Page/太宰治.md "wikilink")，[日本作家](../Page/日本.md "wikilink")（[1909年出生](../Page/1909年.md "wikilink")）
  - [8月4日](../Page/8月4日.md "wikilink")——[米列娃·馬利奇](../Page/米列娃·馬利奇.md "wikilink")，[南斯拉夫数学家](../Page/南斯拉夫.md "wikilink")，[阿尔伯特·爱因斯坦的第一位夫人](../Page/阿尔伯特·爱因斯坦.md "wikilink")（[1875年出生](../Page/1875年.md "wikilink")）
  - [8月12日](../Page/8月12日.md "wikilink")——[朱自清](../Page/朱自清.md "wikilink")，[中國作家](../Page/中国.md "wikilink")（[1898年出生](../Page/1898年.md "wikilink")）
  - [8月16日](../Page/8月16日.md "wikilink")——[貝比·魯斯](../Page/貝比·魯斯.md "wikilink")，[美國傳奇棒球選手](../Page/美國.md "wikilink")（[1895年出生](../Page/1895年.md "wikilink")）
  - [9月1日](../Page/9月1日.md "wikilink")——[馮玉祥](../Page/馮玉祥.md "wikilink")，[中國](../Page/中国.md "wikilink")[民國至](../Page/民國.md "wikilink")[國共內戰時期著名將軍](../Page/國共內戰.md "wikilink")，抗日將領，後因乘船時遇大火逝世（[1882年出生](../Page/1882年.md "wikilink")）
  - [9月3日](../Page/9月3日.md "wikilink")——[艾德瓦·本尼斯](../Page/艾德瓦·本尼斯.md "wikilink")，[捷克斯洛伐克政治家](../Page/捷克斯洛伐克.md "wikilink")（[1884年出生](../Page/1884年.md "wikilink")）
  - [11月13日](../Page/11月13日.md "wikilink")——-{[陳布雷](../Page/陳布雷.md "wikilink")}-，[蔣介石機要秘書](../Page/蔣中正.md "wikilink")（[1890年出生](../Page/1890年.md "wikilink")）
  - [12月23日](../Page/12月23日.md "wikilink")——[东条英机](../Page/东条英机.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1884年出生](../Page/1884年.md "wikilink")）
  - 12月23日——[板垣征四郎](../Page/板垣征四郎.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1885年出生](../Page/1885年.md "wikilink")）
  - 12月23日——[木村兵太郎](../Page/木村兵太郎.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1888年出生](../Page/1888年.md "wikilink")）
  - 12月23日——[土肥原賢二](../Page/土肥原贤二.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1883年出生](../Page/1883年.md "wikilink")）
  - 12月23日——[松井石根](../Page/松井石根.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1878年出生](../Page/1878年.md "wikilink")）
  - 12月23日——[武藤章](../Page/武藤章.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1892年出生](../Page/1892年.md "wikilink")）
  - 12月23日——[廣田弘毅](../Page/广田弘毅.md "wikilink")，[日本战犯](../Page/日本.md "wikilink")，被处绞刑（[1878年出生](../Page/1878年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[帕特里克·布萊克特](../Page/帕特里克·布萊克特.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[阿爾內·蒂塞利烏斯](../Page/阿爾內·蒂塞利烏斯.md "wikilink")
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")：[保羅·赫爾曼·穆勒](../Page/保羅·赫爾曼·穆勒.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[托马斯·斯特恩斯·艾略特](../Page/托马斯·斯特尔那斯·艾略特.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：未颁发

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第21届，[1949年颁发](../Page/1949年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[王子复仇记](../Page/哈姆雷特.md "wikilink")》（Hamlet）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[約翰·休斯頓](../Page/約翰·休斯頓.md "wikilink")（John
    Huston）《[宝石岭](../Page/宝石岭.md "wikilink")》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[劳伦斯·奥利维尔](../Page/劳伦斯·奥利维尔.md "wikilink")（Laurence
    Olivier）《王子复仇记》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[简·惠曼](../Page/简·惠曼.md "wikilink")（Jane
    Wyman）《[约翰尼·贝林达](../Page/约翰尼·贝林达.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[沃尔特·赫斯顿](../Page/沃尔特·赫斯顿.md "wikilink")（Walter
    Huston）《宝石岭》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[克莱尔·特雷弗](../Page/克莱尔·特雷弗.md "wikilink")（Claire
    Trevor）《[盖世枭雄](../Page/盖世枭雄.md "wikilink")》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖.md "wikilink")）

## 參考文獻

[1948年](../Category/1948年.md "wikilink")
[8年](../Category/1940年代.md "wikilink")
[4](../Category/20世纪各年.md "wikilink")