## 大事记

  - [聯合國國際](../Page/聯合國.md "wikilink")[外太空年](../Page/空間.md "wikilink")
    (ISY) 主題為「和平利用外太空的國際合作」（International co-opration in the peaceful
    uses of outer space）。
  - [日本政府投資十一億日圓](../Page/日本.md "wikilink")，無償援助[敦煌建設石窟文物保護研究陳列中心奠基開工](../Page/敦煌.md "wikilink")。

<!-- end list -->

  - [1月3日](../Page/1月3日.md "wikilink")——[新加坡開始禁止](../Page/新加坡.md "wikilink")[口香糖的生產](../Page/口香糖.md "wikilink")、進口和販賣。
  - 1月15日——[中华民国交通部交法发字第八一](../Page/中華民國交通部.md "wikilink")０一号令废止《[投匪资匪之轮船公司及船只紧急处置办法](../Page/:s:投匪资匪之轮船公司及船只紧急处置办法.md "wikilink")》。此条法令允许[中华民国海军](../Page/中華民國海軍.md "wikilink")、[空军搜捕](../Page/中華民國空軍.md "wikilink")、击毁中华人民共和国籍船只，是[第二次国共内战的产物](../Page/第二次国共内战.md "wikilink")。在1950年8月16日，由[行政院核准](../Page/行政院.md "wikilink")。此时，与之相关的[关闭政策已法理上废止多时](../Page/关闭政策.md "wikilink")，中华民国军方亦早已不再搜捕、击毁中华人民共和国籍船只。但中华人民共和国方面仍反应谨慎，同年11月27日，[中华人民共和国交通部的指示中](../Page/中华人民共和国交通部.md "wikilink")，要求民船避免进入[海峡中线东侧海域](../Page/臺灣海峽中線.md "wikilink")。
  - [1月18日到](../Page/1月18日.md "wikilink")[2月21日](../Page/2月21日.md "wikilink")——「[鄧小平南巡](../Page/邓小平南巡.md "wikilink")」，時年八十八歲。[鄧小平巡視](../Page/鄧小平.md "wikilink")[武漢](../Page/武漢.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[珠海](../Page/珠海.md "wikilink")、[上海等地](../Page/上海.md "wikilink")，沿路發表一系列的有關改革開放的重要談話，呼籲經濟改革。鄧小平指出：「不堅持社會主義、不發展經濟、不改善人民生活，只能是死路一條，基本路線要管一百年，動搖不得。只有堅持這條路線，人民才會相信你、擁護你。誰要改變三中全會以來的路線，老百姓不答應，誰就會被打倒。」
  - [1月24日](../Page/1月24日.md "wikilink")——[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[以色列国建立大使级外交关系](../Page/以色列国.md "wikilink")。
  - [1月30日](../Page/1月30日.md "wikilink")——[中華民國行政院長](../Page/中華民國行政院長.md "wikilink")[郝柏村表示將抑制金融遊戲死灰復燃](../Page/郝柏村.md "wikilink")，台灣股市自5459點高峰一路下滑13個月，跌至[1993年](../Page/1993年.md "wikilink")[1月8日低點](../Page/1月8日.md "wikilink")3098點，下跌43.25％

<!-- end list -->

  - [2月3日](../Page/2月3日.md "wikilink")——[委內瑞拉發生](../Page/委內瑞拉.md "wikilink")[兵變](../Page/兵變.md "wikilink")，6小時內被鎮壓，近千名叛軍被捕。
  - [2月3日](../Page/2月3日.md "wikilink")——香港[石崗船民中心於農曆大年初一發生騷亂](../Page/石崗.md "wikilink")，並[釀成大火](../Page/石崗船民中心縱火騷亂事件.md "wikilink")，導致24死126傷。\[1\]
  - [2月7日](../Page/2月7日.md "wikilink")——[歐洲共同體](../Page/歐洲共同體.md "wikilink")12國外長和財政部長正式簽訂《[貨幣聯盟條約](../Page/貨幣聯盟條約.md "wikilink")》，規定最遲於[1999年](../Page/1999年.md "wikilink")[1月18日在歐共體內發行統一貨幣](../Page/1月18日.md "wikilink")，實行共同的對外與防務政策，並擴大[歐洲議會的權力](../Page/歐洲議會.md "wikilink")。

[Flag_of_Mongolia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Mongolia.svg "fig:Flag_of_Mongolia.svg")\]\]

  - [2月12日](../Page/2月12日.md "wikilink")——[蒙古人民共和国改国名为](../Page/蒙古人民共和国.md "wikilink")[蒙古国](../Page/蒙古国.md "wikilink")，并更改[国旗](../Page/蒙古国旗.md "wikilink")[国徽](../Page/蒙古国徽.md "wikilink")，宣布放弃[社会主义](../Page/社会主义.md "wikilink")，实行[多党制](../Page/多党制.md "wikilink")。
  - [2月16日](../Page/2月16日.md "wikilink")——[潮州体育馆正式竣工](../Page/潮州体育馆.md "wikilink")。
  - [2月20日](../Page/2月20日.md "wikilink")——[英國](../Page/英國.md "wikilink")[英格蘭超級足球聯賽](../Page/英格蘭超級足球聯賽.md "wikilink")（英超）成立。
  - [2月25日](../Page/2月25日.md "wikilink")——中國[全國人大常委會通過](../Page/全國人大常委會.md "wikilink")《領海法》，正式將[釣魚台島列入中國領海範圍](../Page/釣魚台.md "wikilink")，[日本政府提出抗議](../Page/日本政府.md "wikilink")。一艘中國公安船開槍驅逐在釣魚臺海域作業的台灣漁船，撞壞其船後離開。台灣[中華民國海軍向日本求援](../Page/中華民國海軍.md "wikilink")。最後漁船被日方軍用艦艇護送返台。

<!-- end list -->

  - [3月8日](../Page/3月8日.md "wikilink")——[中华人民共和国开放黑龙江省黑河市](../Page/中华人民共和国.md "wikilink")、绥芬河市，吉林省的珲春市，内蒙古自治区的满洲里市、二连浩特市，新疆维吾尔自治区的伊宁市、塔城市、博乐市，云南省的瑞丽市、畹町市、河口市和广西壮族自治区的凭祥市和东兴镇等13个[沿边开放城市](../Page/中华人民共和国沿边开放城市.md "wikilink")、镇。
  - [3月9日](../Page/3月9日.md "wikilink")——中華人民共和國加入《[不擴散核武器條約](../Page/不擴散核武器條約.md "wikilink")》。
  - [3月17日](../Page/3月17日.md "wikilink")——[匯豐銀行收購英國](../Page/匯豐銀行.md "wikilink")[米特蘭銀行](../Page/米特蘭銀行.md "wikilink")。
  - [3月20日到](../Page/3月20日.md "wikilink")[4月3日](../Page/4月3日.md "wikilink")——北京召開七屆[全國人大五次會議](../Page/全國人大.md "wikilink")。是否實施改革成為大會的焦點。在人大會議上，[中共中央書記處書記](../Page/中共中央書記處.md "wikilink")、[中央軍委秘書長兼總政治部主任](../Page/中央軍委.md "wikilink")[楊白冰率先喊出](../Page/楊白冰.md "wikilink")：「為改革開放保駕護航。」同時《[解放軍報](../Page/解放軍報.md "wikilink")》發表題為「為改革開放保駕護航」的社論，公開表示「堅決響應小平同志號召，為改革開放保駕護航」。
  - [3月22日](../Page/3月22日.md "wikilink")——中国于[西昌卫星发射中心用](../Page/西昌卫星发射中心.md "wikilink")[长征2号E运载火箭发射](../Page/长征2号E.md "wikilink")[美国为](../Page/美国.md "wikilink")[澳洲制造的澳赛特B](../Page/澳洲.md "wikilink")1通信卫星，火箭点火后发生了紧急关机，未能顺利起飞，发射中止\[2\]。

<!-- end list -->

  - [3月22日](../Page/3月22日.md "wikilink")——[阿根廷發生爆炸事件](../Page/阿根廷.md "wikilink")。
  - [3月30日](../Page/3月30日.md "wikilink")——[新華社全文播發](../Page/新華社.md "wikilink")「為[改革開放保駕護航](../Page/改革開放.md "wikilink")」。

<!-- end list -->

  - [4月3日](../Page/4月3日.md "wikilink")——中國[全國人民代表大會通過興建](../Page/全國人民代表大會.md "wikilink")[長江三峽工程的決議](../Page/長江三峽工程.md "wikilink")。
  - [4月6日](../Page/4月6日.md "wikilink")——[波斯尼亞戰爭爆發](../Page/波斯尼亞戰爭.md "wikilink")。
  - [4月20日](../Page/4月20日.md "wikilink")——[1992年世界博覽會](../Page/1992年世界博覽會.md "wikilink")（The
    Universal Exposition Seville '92 ）主題為「發現的時代」"The Age of
    Discovery"正式開幕在[西班牙的](../Page/西班牙.md "wikilink")[塞維亞舉行](../Page/塞維亞.md "wikilink")。首次超過100個國家參與展覽。
  - [4月22日](../Page/4月22日.md "wikilink")——[墨西哥發生連橫氣體爆炸](../Page/墨西哥.md "wikilink")，造成206人死亡，500人受傷，15,000人無家可歸，幾十座建築物被夷為平地。
  - [4月27日](../Page/4月27日.md "wikilink")——由[塞爾維亞和](../Page/塞爾維亞.md "wikilink")[黑山組成的](../Page/黑山.md "wikilink")[南斯拉夫聯邦共和國宣告成立](../Page/南斯拉夫聯邦共和國.md "wikilink")。
  - [4月29日](../Page/4月29日.md "wikilink")——陪審團無罪釋放四位毒打黑人摩托車騎士[羅德尼·金的警官](../Page/羅德尼·金.md "wikilink")（全程被第三方使用
    V8
    拍攝下）；該事件隨後造成[1992年洛杉磯暴動](../Page/1992年洛杉磯暴動.md "wikilink")，造成53死、當地10億美元的財產損失。

<!-- end list -->

  - [5月5日](../Page/5月5日.md "wikilink")——[香港](../Page/香港.md "wikilink")[旺角瑞興麻雀館發生香港開埠以來最殘酷的劫殺案](../Page/旺角.md "wikilink")，造成2死19傷。
  - [5月8日](../Page/5月8日.md "wikilink")——[皇家香港天文台](../Page/皇家香港天文台.md "wikilink")（今香港天文台）於上午六時至七時錄得109.9毫米雨量，打破歷年來每小時最高降雨量紀錄，天文台遭到猛烈批評預報失當，同年制訂[暴雨警告系統](../Page/暴雨警告信號_\(香港\).md "wikilink")。這個紀錄被2008年6月7日的145.5毫米打破。
  - [5月11日](../Page/5月11日.md "wikilink")——[拉莫斯當選](../Page/拉莫斯.md "wikilink")[菲律賓總統](../Page/菲律賓.md "wikilink")。
  - [5月12日](../Page/5月12日.md "wikilink")——[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[斯洛文尼亚建交](../Page/斯洛文尼亚.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——经国务院批准，自1992年[8月4日期](../Page/8月4日.md "wikilink")，由[交通部](../Page/中華人民共和國交通部.md "wikilink")、[财政部](../Page/中華人民共和國財政部.md "wikilink")、国家物价局颁布的《内河航道养护费征收和使用办法》开始施行并生效。
  - [5月17日](../Page/5月17日.md "wikilink")——[泰國有幾十萬民眾參加反對軍人干預政治的集會](../Page/泰國.md "wikilink")，軍方動用武力鎮壓，觸發大規模街頭流血衝突，最少有幾十個示威者喪生。
  - [5月18日](../Page/5月18日.md "wikilink")——[香港歌星](../Page/香港.md "wikilink")[陳百強被發現昏迷於家中](../Page/陳百強.md "wikilink")，送入[瑪麗醫院搶救](../Page/瑪麗醫院.md "wikilink")。
  - [5月21日](../Page/5月21日.md "wikilink")——英國與越南達成遣返[越南船民協議後](../Page/越南船民.md "wikilink")，引發滯留香港的[越南船民不滿](../Page/越南船民.md "wikilink")，觸發[大鴉洲船民中心騷亂](../Page/大鴉洲.md "wikilink")，警方施放60枚催淚彈鎮壓。
  - [5月22日](../Page/5月22日.md "wikilink")——中國[上海証券交易所股價全面公開](../Page/上海証券交易所.md "wikilink")。
  - [5月30日](../Page/5月30日.md "wikilink")——[中国人民武装警察部队进驻](../Page/中国人民武装警察部队.md "wikilink")[广东省](../Page/广东省.md "wikilink")[潮州市](../Page/潮州市.md "wikilink")，组建[武警潮州支队](../Page/武警潮州支队.md "wikilink")。

<!-- end list -->

  - [6月9日](../Page/6月9日.md "wikilink")——[日本參議院通過向海外派兵法案](../Page/日本參議院.md "wikilink")，同月15日眾議院亦通過該法案。該法案授權日本向外派遣2,000個[自衛隊隊員](../Page/自衛隊.md "wikilink")，參與[聯合國維持和平任務](../Page/維和部隊.md "wikilink")。這亦是日本自第二次世界大戰以來，[日本國會第一次通過向外派兵法案](../Page/日本國會.md "wikilink")。
  - [6月27日](../Page/6月27日.md "wikilink")——[意大利三大工會組織](../Page/意大利.md "wikilink")10萬人大遊行，抗議[黑手黨暴行](../Page/黑手黨.md "wikilink")。

[Deng_Xiaoping_billboard_in_Lizhi_Park.jpg](https://zh.wikipedia.org/wiki/File:Deng_Xiaoping_billboard_in_Lizhi_Park.jpg "fig:Deng_Xiaoping_billboard_in_Lizhi_Park.jpg")

  - [6月28日](../Page/6月28日.md "wikilink")——[中华人民共和国政府在](../Page/中华人民共和国政府.md "wikilink")[鄧小平曾經作](../Page/鄧小平.md "wikilink")[南巡講話的](../Page/邓小平南巡.md "wikilink")[深圳懸掛一幅](../Page/深圳.md "wikilink")[鄧小平畫像](../Page/鄧小平畫像.md "wikilink")，以宣傳[改革開放](../Page/改革開放.md "wikilink")。
  - [7月9日](../Page/7月9日.md "wikilink")——[香港第二十八任](../Page/香港.md "wikilink")[總督](../Page/香港總督.md "wikilink")[彭定康宣誓就任](../Page/彭定康.md "wikilink")。
  - [7月23日](../Page/7月23日.md "wikilink")——[捷克和](../Page/捷克.md "wikilink")[斯洛伐克分裂](../Page/斯洛伐克.md "wikilink")。
  - [7月25日](../Page/7月25日.md "wikilink")——[第25屆奧運會在](../Page/1992年夏季奧林匹克運動會.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[巴塞隆納開幕](../Page/巴塞隆納.md "wikilink")。
  - [7月31日](../Page/7月31日.md "wikilink")——一架由[南京飛往](../Page/南京.md "wikilink")[廈門的](../Page/廈門.md "wikilink")[中国通用航空2755号于](../Page/中国通用航空2755号班机空难.md "wikilink")[南京大校場機場起飛時滑出](../Page/南京大校場機場.md "wikilink")[跑道](../Page/跑道.md "wikilink")，撞毀防護堤，機上逾100人死亡。
  - [8月2日](../Page/8月2日.md "wikilink")——[南非爆發大規模的](../Page/南非.md "wikilink")、黑人反對白人的全國大[罷工](../Page/罷工.md "wikilink")。

<!-- end list -->

  - [8月12日](../Page/8月12日.md "wikilink")——[美国](../Page/美国.md "wikilink")、[加拿大及](../Page/加拿大.md "wikilink")[墨西哥三国签署](../Page/墨西哥.md "wikilink")[北美自由贸易协议](../Page/北美自由贸易协议.md "wikilink")。
  - [8月13日](../Page/8月13日.md "wikilink")——[台灣調整每月最低基本工資](../Page/台灣.md "wikilink")，此次調幅高達12%，從[新台幣](../Page/新台幣.md "wikilink")11040元調整至12365元，折算日薪為412元。
  - [8月14日](../Page/8月14日.md "wikilink")——北京时间7时，中国[长征2号E捆绑式运载火箭在](../Page/长征2号E.md "wikilink")[西昌卫星发射中心成功发射由美国為澳洲研製的澳赛特B](../Page/西昌卫星发射中心.md "wikilink")1通信卫星，标志着中国的卫星发射开始进入商业化国际市场\[3\]。
  - [8月20日](../Page/8月20日.md "wikilink")——中國[深圳發生股災](../Page/深圳.md "wikilink")，震驚全國，史稱「[八二零事件](../Page/八二零事件.md "wikilink")」。
  - [8月22日](../Page/8月22日.md "wikilink")——[中華民國与](../Page/中華民國.md "wikilink")[大韩民国](../Page/大韩民国.md "wikilink")[歷經四十二年歲月的外交關係宣布終止](../Page/中華民國－大韓民國關係.md "wikilink")。
  - [8月24日](../Page/8月24日.md "wikilink")——上午9时，[中华人民共和国国务委员兼外交部长](../Page/中华人民共和国.md "wikilink")[钱其琛同韩国外务部长官](../Page/钱其琛.md "wikilink")[李相玉](../Page/李相玉.md "wikilink")，在[北京](../Page/北京.md "wikilink")[钓鱼台国宾馆签署](../Page/钓鱼台国宾馆.md "wikilink")《**中华人民共和国和大韩民国关于建立外交关系的联合公报**》，[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[韩国](../Page/韩国.md "wikilink")[建交](../Page/1992年中韩建交.md "wikilink")。
  - [8月28日](../Page/8月28日.md "wikilink")——香港[中華電力](../Page/中華電力.md "wikilink")[青山發電廠氫氣庫發生爆炸](../Page/青山發電廠.md "wikilink")，造成2死10多人傷。

<!-- end list -->

  - [9月8日](../Page/9月8日.md "wikilink")——[日本二战后首次向海外派兵](../Page/日本.md "wikilink")。
  - [9月20日](../Page/9月20日.md "wikilink")——[法国公决以微弱多数批准实现](../Page/法国.md "wikilink")[欧洲联合的](../Page/欧洲.md "wikilink")《[马斯特里赫特条约](../Page/马斯特里赫特条约.md "wikilink")》。
  - [9月21日](../Page/9月21日.md "wikilink")——[中國國家航天局](../Page/中國國家航天局.md "wikilink")[CNSA第一個重大工程](../Page/CNSA.md "wikilink")[載人航天計劃](../Page/載人航天.md "wikilink")[正式立項](../Page/中國載人航天工程.md "wikilink")。
  - [9月28日](../Page/9月28日.md "wikilink")——[韩国总统](../Page/韩国总统.md "wikilink")[卢泰愚访问](../Page/卢泰愚.md "wikilink")[北京](../Page/北京.md "wikilink")，这是[韩国第一位访问](../Page/韩国.md "wikilink")[中华人民共和国的](../Page/中华人民共和国.md "wikilink")[总统](../Page/韩国总统.md "wikilink")。

<!-- end list -->

  - [10月4日](../Page/10月4日.md "wikilink")——[澳門](../Page/澳門.md "wikilink")[新馬路](../Page/新馬路.md "wikilink")[永亨銀行大廈發生大火](../Page/澳門永亨銀行大廈.md "wikilink")，一名保安員從11樓高空摔落死亡。

[Bijlmerramp2_without_link.jpg](https://zh.wikipedia.org/wiki/File:Bijlmerramp2_without_link.jpg "fig:Bijlmerramp2_without_link.jpg")

  - [10月4日](../Page/10月4日.md "wikilink")——[以色列航空](../Page/以色列航空.md "wikilink")[1862號班機在自](../Page/以色列航空1862號班機空難.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[史基浦機場起飛後不久](../Page/史基浦機場.md "wikilink")，撞上兩幢位於市郊的[公寓大樓](../Page/公寓.md "wikilink")，班機墜毀並造成43人死亡。
  - [10月5日](../Page/10月5日.md "wikilink")——[IBM公司](../Page/IBM公司.md "wikilink")[ThinkPad](../Page/ThinkPad.md "wikilink")[筆記型電腦正式上市](../Page/筆記型電腦.md "wikilink")。
  - [10月8日](../Page/10月8日.md "wikilink")——[武漢航空一架旅遊包機在中國](../Page/武漢航空.md "wikilink")[甘肅省墜毀](../Page/甘肅省.md "wikilink")。
  - [10月12日](../Page/10月12日.md "wikilink")——[哥倫布發現](../Page/哥倫布.md "wikilink")[美洲大陸](../Page/美洲大陸.md "wikilink")500周年。1992[世界博覽會](../Page/世界博覽會.md "wikilink")（The
    Universal Exposition Seville '92 ）主題為「發現的時代」"The Age of
    Discovery"正式落幕。
  - [10月12日](../Page/10月12日.md "wikilink")——[中國共產黨第十四次全國代表大會在](../Page/中國共產黨第十四次全國代表大會.md "wikilink")[北京召開](../Page/北京.md "wikilink")。
  - [10月12日](../Page/10月12日.md "wikilink")——香港總督[彭定康首次訪問北京](../Page/彭定康.md "wikilink")。
  - [10月23日](../Page/10月23日.md "wikilink")——[日本天皇](../Page/日本天皇.md "wikilink")[明仁訪問中國](../Page/明仁.md "wikilink")，成為首位訪華的[日本天皇](../Page/日本天皇.md "wikilink")。
  - [10月26日至](../Page/10月26日.md "wikilink")[10月30日](../Page/10月30日.md "wikilink")——[九二香港會談](../Page/九二香港會談.md "wikilink")。
  - [10月28日](../Page/10月28日.md "wikilink")——[俄羅斯](../Page/俄羅斯.md "wikilink")[葉利欽解散俄民族救國陣線組委會](../Page/葉利欽.md "wikilink")。同日[埃及](../Page/埃及.md "wikilink")[賽德港因](../Page/賽德港.md "wikilink")[油輪被撞進入緊急狀態](../Page/油輪.md "wikilink")。
  - [10月31日](../Page/10月31日.md "wikilink")——[伽利略蒙冤](../Page/伽利略.md "wikilink")360年后，终于获得[羅馬教宗的平反](../Page/教宗.md "wikilink")。

<!-- end list -->

  - [11月3日](../Page/11月3日.md "wikilink")——[美國民主黨代表](../Page/美國民主黨.md "wikilink")[克林頓擊敗爭取連任的](../Page/克林頓.md "wikilink")[共和黨代表](../Page/共和黨.md "wikilink")[老布殊當選](../Page/老布殊.md "wikilink")[美國總統](../Page/美國總統.md "wikilink")。
  - [11月7日](../Page/11月7日.md "wikilink")——[中華民國](../Page/中華民國.md "wikilink")[金門](../Page/金門.md "wikilink")、[馬祖地區解除戰地政務](../Page/馬祖.md "wikilink")，實施[地方自治](../Page/地方自治.md "wikilink")。
  - [11月9日](../Page/11月9日.md "wikilink")——[德國發生大規模反種族歧視](../Page/德國.md "wikilink")[遊行](../Page/遊行.md "wikilink")，反對國內對少數族裔（主要是施加於[土耳其的](../Page/土耳其.md "wikilink")[移民](../Page/移民.md "wikilink")）的[歧視](../Page/歧視.md "wikilink")。

<!-- end list -->

  - [11月11日](../Page/11月11日.md "wikilink")——[英國](../Page/英國.md "wikilink")[英格蘭教會准許女性擔任](../Page/英格蘭教會.md "wikilink")[牧師](../Page/牧師.md "wikilink")。
  - [11月13日](../Page/11月13日.md "wikilink")——[秘魯發生未遂政變](../Page/秘魯.md "wikilink")。
  - [11月21日](../Page/11月21日.md "wikilink")——[厄瓜多爾發生](../Page/厄瓜多爾.md "wikilink")[海豚集體自殺事件](../Page/海豚.md "wikilink")。
  - [11月25日](../Page/11月25日.md "wikilink")——[捷克斯洛伐克聯邦議會通過](../Page/捷克斯洛伐克.md "wikilink")《解體法》，議決於翌年1月1日分裂成[捷克和](../Page/捷克.md "wikilink")[斯洛伐克兩個獨立國家](../Page/斯洛伐克.md "wikilink")。
  - [12月2日](../Page/12月2日.md "wikilink")——香港發生兩宗警匪槍戰事件，其中包括[特別任務連](../Page/特別任務連.md "wikilink")（俗稱「[飛虎隊](../Page/飛虎隊.md "wikilink")」）在[荃灣的](../Page/荃灣.md "wikilink")[荃灣中心廣州樓圍剿匪幫時](../Page/荃灣中心.md "wikilink")，爆發導致7名警員受傷，6名匪賊被捕的[槍戰](../Page/槍戰.md "wikilink")，以及同日在[太子道西一間珠寶金行械劫案後](../Page/太子道西.md "wikilink")，有1名賊人死亡，3名匪賊被捕的槍戰。
  - [12月6日](../Page/12月6日.md "wikilink")——[印度爆發](../Page/印度.md "wikilink")[教族騷亂](../Page/教族.md "wikilink")，造成1100多人死亡，4000多人受傷。
  - [12月7日](../Page/12月7日.md "wikilink")——[玻利維亞的](../Page/玻利維亞.md "wikilink")[伊皮山因連場暴雨而發生山體滑坡](../Page/伊皮山.md "wikilink")，造成上千人喪命。
  - [12月19日](../Page/12月19日.md "wikilink")——[中華民國舉辦](../Page/中華民國.md "wikilink")[第二屆立法委員選舉](../Page/1992年中華民國立法委員選舉.md "wikilink")，完成[國會全面改選](../Page/國會.md "wikilink")，結束長期未改選的「[萬年國會](../Page/萬年國會.md "wikilink")」狀況。
  - [12月21日](../Page/12月21日.md "wikilink")——北京時間19時，中國發射[长征2号E捆绑式运载火箭将第二颗澳星送入太空](../Page/长征2号E.md "wikilink")，起飛後約48秒爆炸\[4\]。

## 出生

### 1月

  - [1月1日](../Page/1月1日.md "wikilink")——[王敏奕](../Page/王敏奕.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [1月3日](../Page/1月3日.md "wikilink")——[連晨翔](../Page/連晨翔.md "wikilink")，前台灣男子團體[SpeXial成員](../Page/SpeXial.md "wikilink")。
  - [1月11日](../Page/1月11日.md "wikilink")——[李昇勳](../Page/李昇勳.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[WINNER成員](../Page/WINNER.md "wikilink")。
  - [1月16日](../Page/1月16日.md "wikilink")——[胡心諾](../Page/胡心諾.md "wikilink")，[香港少女模特兒](../Page/香港.md "wikilink")。
  - [1月20日](../Page/1月20日.md "wikilink")——[鄭榮植](../Page/鄭榮植.md "wikilink")，[韓國桌球運動員](../Page/韓國.md "wikilink")
  - [1月23日](../Page/1月23日.md "wikilink")——[特林德爾·玲奈](../Page/特林德爾·玲奈.md "wikilink")（，Reina
    Triendl），德日混血日本[模特兒](../Page/模特兒.md "wikilink")、藝人。

### 2月

  - [2月5日](../Page/2月5日.md "wikilink")——[内马尔·达席尔瓦](../Page/内马尔·达席尔瓦.md "wikilink")，[巴西足球運動員](../Page/巴西.md "wikilink")。
  - [2月5日](../Page/2月5日.md "wikilink")——[李大勛](../Page/李大勛.md "wikilink")，韓國跆拳道運動員
  - [2月7日](../Page/2月7日.md "wikilink")——[矢美島舞](../Page/矢美島舞.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。
  - [2月10日](../Page/2月10日.md "wikilink")——[仲川遙香](../Page/仲川遙香.md "wikilink")，日本女藝人、前[日本女子團體](../Page/日本.md "wikilink")[AKB48](../Page/AKB48.md "wikilink")，[JKT48成員](../Page/JKT48.md "wikilink")
  - [2月12日](../Page/2月12日.md "wikilink")——[姜智賢](../Page/姜智賢.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[SISTAR成員](../Page/SISTAR.md "wikilink")。
  - [2月12日](../Page/2月12日.md "wikilink")——[風田](../Page/風田.md "wikilink"),台灣男子團體[SpeXial成員](../Page/SpeXial.md "wikilink")。
  - [2月14日](../Page/2月14日.md "wikilink")——[費迪·夏摩亞](../Page/費迪·夏摩亞.md "wikilink")，[英國男童星](../Page/英國.md "wikilink")。
  - [2月15日](../Page/2月15日.md "wikilink")——[鄭怡靜](../Page/鄭怡靜.md "wikilink"),台灣女子桌球運動員
  - [2月16日](../Page/2月16日.md "wikilink")——[辜莞允](../Page/辜莞允.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - [2月18日](../Page/2月18日.md "wikilink")——[姚望中国男演员](../Page/姚望_\(演员\).md "wikilink")
  - [2月21日](../Page/2月21日.md "wikilink")——[菲尔·琼斯](../Page/菲尔·琼斯.md "wikilink")，[英格蘭足球運動員](../Page/英格蘭.md "wikilink")。
  - [2月26日](../Page/2月26日.md "wikilink")——[篠崎愛](../Page/篠崎愛.md "wikilink")，日本[寫真女星](../Page/寫真女星.md "wikilink")。

### 3月

  - [3月6日](../Page/3月6日.md "wikilink")——[嗣永桃子](../Page/嗣永桃子.md "wikilink")，日本歌手，前日本女子團體[Berryz工房成員](../Page/Berryz工房.md "wikilink")。
  - [3月7日](../Page/3月7日.md "wikilink")——[任炫植](../Page/任炫植.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[BTOB成員](../Page/BTOB.md "wikilink")。，
  - [3月9日](../Page/3月9日.md "wikilink")——[簡廷芮](../Page/簡廷芮.md "wikilink")，台灣女子團體[Dears成員](../Page/Dears.md "wikilink")。
  - [3月10日](../Page/3月10日.md "wikilink")——[艾蜜莉·奧斯蒙](../Page/艾蜜莉·奧斯蒙.md "wikilink")，美國女演員、歌手和發言人。
  - [3月13日](../Page/3月13日.md "wikilink")——[金明洙](../Page/金明洙.md "wikilink")，韓國男子團體[INFINITE成員](../Page/INFINITE.md "wikilink")。
  - [3月13日](../Page/3月13日.md "wikilink")——[卡雅·斯考達里奧](../Page/卡雅·斯考達里奧.md "wikilink"),
    英格蘭女演員。
  - [3月13日](../Page/3月13日.md "wikilink")——[Ozuna](../Page/Ozuna.md "wikilink")，[波多黎各歌手](../Page/波多黎各.md "wikilink")。
  - [3月16日](../Page/3月16日.md "wikilink")——[蒂姆·哈达威二世](../Page/蒂姆·哈达威二世.md "wikilink")，美国篮球运动员，美国著名篮球明星蒂姆·哈达威之子。
  - [3月20日](../Page/3月20日.md "wikilink")——[燦多](../Page/燦多.md "wikilink")，
    韓國男子團體[B1A4成員](../Page/B1A4.md "wikilink")。
  - [3月23日](../Page/3月23日.md "wikilink")——[凯里·欧文](../Page/凯里·欧文.md "wikilink")，
    美国篮球状元明星，三分球大赛冠军之一。
  - [3月25日](../Page/3月25日.md "wikilink")——[许凯皓](../Page/许凯皓.md "wikilink")，台湾男艺人
  - [3月27日](../Page/3月27日.md "wikilink")——[悠木碧](../Page/悠木碧.md "wikilink")，日本女性[聲優](../Page/聲優.md "wikilink")。

### 4月

  - [4月3日](../Page/4月3日.md "wikilink")——[山田菜菜](../Page/山田菜菜.md "wikilink")，日本女藝人、前[日本女子團體](../Page/日本.md "wikilink")[NMB48成員](../Page/NMB48.md "wikilink")
  - [4月6日](../Page/4月6日.md "wikilink")——[Ken](../Page/Ken.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[VIXX成員](../Page/VIXX.md "wikilink")
  - [4月17日](../Page/4月17日.md "wikilink")——[趙珍虎](../Page/趙珍虎.md "wikilink")，曾為[SM娛樂的練習生](../Page/SM娛樂.md "wikilink")，現為[Cube娛樂旗下的男子團體](../Page/Cube娛樂.md "wikilink")[Pentagon_(男子組合)成員](../Page/Pentagon_\(男子組合\).md "wikilink")。
  - [4月17日](../Page/4月17日.md "wikilink")——[陳冠齊](../Page/陳冠齊.md "wikilink")，臺灣臺中市豐原區人，[太陽花學運](../Page/太陽花學運.md "wikilink")、環境保育、性別平等運動人物。（逝於[2017年](../Page/2017年.md "wikilink")）
  - [4月21日](../Page/4月21日.md "wikilink")——[伊斯高](../Page/伊斯高.md "wikilink")，[西班牙足球員](../Page/西班牙.md "wikilink")
  - [4月28日](../Page/4月28日.md "wikilink")——[黃子菲](../Page/黃子菲.md "wikilink")，[香港模特兒](../Page/香港.md "wikilink")。

### 5月

  - [5月1日](../Page/5月1日.md "wikilink")——[朴炫浩](../Page/朴炫浩.md "wikilink")，[韩国男子团体topp](../Page/韩国.md "wikilink")
    dogg成员
  - [5月1日](../Page/5月1日.md "wikilink")——[安喜延](../Page/安喜延.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[EXID成員](../Page/EXID.md "wikilink")
  - [5月2日](../Page/5月2日.md "wikilink")——[古力娜扎](../Page/古力娜扎.md "wikilink")，中国女艺人
  - [5月6日](../Page/5月6日.md "wikilink")——[宇佐美貴史](../Page/宇佐美貴史.md "wikilink")，日本足球運動員。
  - [5月6日](../Page/5月6日.md "wikilink")——[邊伯賢](../Page/邊伯賢.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[EXO成员](../Page/EXO.md "wikilink")。
  - [5月11日](../Page/5月11日.md "wikilink")——[泰拔·高圖爾斯](../Page/泰拔·高圖爾斯.md "wikilink")，[比利時足球員](../Page/比利時.md "wikilink")
  - [5月22日](../Page/5月22日.md "wikilink")——[徳永千奈美](../Page/徳永千奈美.md "wikilink")，日本歌手。
  - [5月23日](../Page/5月23日.md "wikilink")——[吳若希](../Page/吳若希.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")。
  - [5月30日](../Page/5月30日.md "wikilink")——[歐鎧淳](../Page/歐鎧淳.md "wikilink")，[香港游泳運動員](../Page/香港.md "wikilink")。

### 6月

  - [6月3日](../Page/6月3日.md "wikilink")——[馬里奧·葛斯](../Page/馬里奧·葛斯.md "wikilink")，德國足球運動員
  - [迪麗熱巴](../Page/迪麗熱巴.md "wikilink")，中國女演員
  - [6月6日](../Page/6月6日.md "wikilink")——[村上愛](../Page/村上愛.md "wikilink")，日本歌手
  - [金泫雅](../Page/金泫雅.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[4minute成員](../Page/4minute.md "wikilink")
  - [6月6日](../Page/6月6日.md "wikilink")，[熊梓淇](../Page/熊梓淇.md "wikilink")，[台灣男子團體](../Page/台灣.md "wikilink")[SpeXial内地成員](../Page/SpeXial.md "wikilink")
  - [6月11日](../Page/6月11日.md "wikilink")——[孫子涵](../Page/孫子涵.md "wikilink")，[中國男歌手](../Page/中國.md "wikilink")
  - [6月18日](../Page/6月18日.md "wikilink")——[Shownu](../Page/孫軒宇.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[MONSTA
    X隊長](../Page/MONSTA_X.md "wikilink")

### 7月

  - [7月3日](../Page/7月3日.md "wikilink")——[須藤茉麻](../Page/須藤茉麻.md "wikilink")，日本歌手。
  - [7月8日](../Page/7月8日.md "wikilink")——[孫興慜](../Page/孫興慜.md "wikilink")，韓國足球運動員。
  - [7月15日](../Page/7月15日.md "wikilink")——[久住小春](../Page/久住小春.md "wikilink")，日本歌手。
  - [7月21日](../Page/7月21日.md "wikilink")——[韩东君](../Page/韩东君.md "wikilink")，中国男演员
  - [7月22日](../Page/7月22日.md "wikilink")——[仁藤萌乃](../Page/仁藤萌乃.md "wikilink")，日本舞台劇女演員、前[日本女子團體](../Page/日本.md "wikilink")[AKB48成員](../Page/AKB48.md "wikilink")——[Selena
    Gomez](../Page/賽琳娜·戈梅茲.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")。
  - [7月31日](../Page/7月31日.md "wikilink")——[Lizzy](../Page/Lizzy.md "wikilink")，韓國女子團體[After
    School成員](../Page/After_School.md "wikilink")。

### 8月

  - [8月1日](../Page/8月1日.md "wikilink")——[奥斯汀·里弗斯](../Page/奥斯汀·里弗斯.md "wikilink")，美國篮球运动员，主教练道格·里弗斯之子。
  - [8月1日](../Page/8月1日.md "wikilink")——[金东盛](../Page/金东盛.md "wikilink")，[韩国男子团体topp](../Page/韩国.md "wikilink")
    dogg成员。
  - [8月12日](../Page/8月12日.md "wikilink")——[陳語安](../Page/陳語安.md "wikilink")，台灣女藝人，模特兒。
  - [8月18日](../Page/8月18日.md "wikilink")——[成海璃子](../Page/成海璃子.md "wikilink")，日本女演員。
  - [8月20日](../Page/8月20日.md "wikilink")——[白石麻衣](../Page/白石麻衣.md "wikilink")，[日本女子團體](../Page/日本.md "wikilink")[乃木坂46成員](../Page/乃木坂46.md "wikilink")。
  - [8月20日](../Page/8月20日.md "wikilink")——[黛咪·洛瓦特](../Page/黛咪·洛瓦特.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")。
  - [8月25日](../Page/8月25日.md "wikilink")——[夏燒雅](../Page/夏燒雅.md "wikilink")，日本歌手。
  - [8月27日](../Page/8月27日.md "wikilink")——[松村沙友理](../Page/松村沙友理.md "wikilink")，[日本女子團體](../Page/日本.md "wikilink")[乃木坂46成員](../Page/乃木坂46.md "wikilink")。
  - [8月27日](../Page/8月27日.md "wikilink")——[剛力彩芽](../Page/剛力彩芽.md "wikilink")，日本女演員。
  - [8月28日](../Page/8月28日.md "wikilink")——[俾斯麦·比永博](../Page/俾斯麦·比永博.md "wikilink")，刚果职业篮球运动员。

### 9月

  - [9月1日](../Page/9月1日.md "wikilink")——[王菊](../Page/王菊.md "wikilink")，中国大陆女网红
  - [9月5日](../Page/9月5日.md "wikilink")——[Baro](../Page/Baro.md "wikilink")，
    韓國男子團體[B1A4成員](../Page/B1A4.md "wikilink")
  - [9月14日](../Page/9月14日.md "wikilink")——[Zico](../Page/Zico.md "wikilink")，韓國男子樂團[Block
    B成員](../Page/Block_B.md "wikilink"),歌手、舞者、Rapper、製作人。
  - [9月15日](../Page/9月15日.md "wikilink")——[Jae](../Page/Jae.md "wikilink")，韓國男子樂團[DAY6吉他手](../Page/DAY6.md "wikilink")、副唱、Rapper
  - [9月18日](../Page/9月18日.md "wikilink")——[宋承炫](../Page/宋承炫.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")（[FTIsland團員](../Page/FTIsland.md "wikilink")）。
  - [9月18日](../Page/9月18日.md "wikilink")——[劉逸雲](../Page/劉逸雲.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[f(x)成員](../Page/F\(x\)_\(組合\).md "wikilink")。
  - [9月19日](../Page/9月19日.md "wikilink")——[永井杏](../Page/永井杏.md "wikilink")，日本歌手。
  - [9月21日](../Page/9月21日.md "wikilink")——[金鍾大](../Page/金鍾大.md "wikilink")，[韩国著名歌手](../Page/韩国.md "wikilink")，[EXO成员](../Page/EXO.md "wikilink")。
  - [9月24日](../Page/9月24日.md "wikilink")——[吳心緹](../Page/吳心緹.md "wikilink")，台灣[女藝人](../Page/女藝人.md "wikilink")。

<!-- end list -->

  - [9月30日](../Page/9月30日.md "wikilink")——[伍文岍](../Page/伍文岍.md "wikilink")(網名：伍公子)，香港網絡藝人。

### 10月

  - [10月11日](../Page/10月11日.md "wikilink")——[卡迪B](../Page/卡迪B.md "wikilink")，[美國饒舌女歌手](../Page/美國.md "wikilink")。
  - [10月17日](../Page/10月17日.md "wikilink")——[櫻庭奈奈美](../Page/櫻庭奈奈美.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")。
  - [10月21日](../Page/10月21日.md "wikilink")——[林穎彤](../Page/林穎彤.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")。、[鄧倫](../Page/鄧倫.md "wikilink")，[中國内地男演員](../Page/中國内地.md "wikilink")、[宋芸桦](../Page/宋芸桦.md "wikilink")，[台湾女演员](../Page/台湾.md "wikilink")。
  - [10月26日](../Page/10月26日.md "wikilink")——[歐陽巧瑩](../Page/歐陽巧瑩.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")，[2013年度香港小姐競選友誼小姐](../Page/2013年度香港小姐競選.md "wikilink")。
  - [10月27日](../Page/10月27日.md "wikilink")——[吳家鋒](../Page/吳家鋒.md "wikilink")，[香港田徑運動員](../Page/香港.md "wikilink")。
  - [10月31日](../Page/10月31日.md "wikilink")——[全浩准](../Page/全浩准.md "wikilink")，[韩国男子团体topp](../Page/韩国.md "wikilink")
    dogg成员。

### 11月

  - [11月2日](../Page/11月2日.md "wikilink")——[何雁詩](../Page/何雁詩.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")。
  - [11月2日](../Page/11月2日.md "wikilink")——[馬振桓](../Page/馬振桓.md "wikilink")，[臺灣男子團體](../Page/臺灣.md "wikilink")[SpeXial成員](../Page/SpeXial.md "wikilink")。
  - [11月4日](../Page/11月4日.md "wikilink")——[方旻洙](../Page/方旻洙.md "wikilink")（C。A。P），[韓國組合](../Page/韓國.md "wikilink")[TEEN
    TOP團員](../Page/TEEN_TOP.md "wikilink")。
  - [11月6日一一](../Page/11月6日.md "wikilink")[Yura](../Page/Yura.md "wikilink")，韓國女子團體[Girl's
    Day成員](../Page/Girl's_Day.md "wikilink")。
  - [11月8日](../Page/11月8日.md "wikilink")——[陳嘉桓](../Page/陳嘉桓.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")。
  - [11月9日](../Page/11月9日.md "wikilink")——[沈建宏](../Page/沈建宏.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")、歌手。
  - [11月11日](../Page/11月11日.md "wikilink")——[崔敏煥](../Page/崔敏煥.md "wikilink")，[韓國藝人](../Page/韓國.md "wikilink")（[FTIsland團員](../Page/FTIsland.md "wikilink")）。
  - [11月15日](../Page/11月15日.md "wikilink")——[峯岸南](../Page/峯岸南.md "wikilink")，日本偶像團體[AKB48成員](../Page/AKB48.md "wikilink")。
  - [11月20日](../Page/11月20日.md "wikilink")——[石村舞波](../Page/石村舞波.md "wikilink")，日本歌手。
  - [11月21日](../Page/11月21日.md "wikilink")——[指原莉乃](../Page/指原莉乃.md "wikilink")，日本偶像團體[HKT48成員](../Page/HKT48.md "wikilink")。

<!-- end list -->

  - [11月23日](../Page/11月23日.md "wikilink")——[麥莉·希拉](../Page/麥莉·希拉.md "wikilink")，[美國歌手](../Page/美國.md "wikilink")、演員。
  - [11月23日](../Page/11月23日.md "wikilink")——[高恩妃](../Page/高恩妃.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[Ladies'
    Code成員](../Page/Ladies'_Code.md "wikilink")。(2014年9月3日逝世)
  - [11月24日](../Page/11月24日.md "wikilink")——[李皓晴](../Page/李皓晴.md "wikilink")，[香港乒乓球運動員](../Page/香港.md "wikilink")。
  - [11月27日](../Page/11月27日.md "wikilink")——[朴灿烈](../Page/朴灿烈.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[EXO成员](../Page/EXO.md "wikilink")。

[林正悅](../Page/林正悅.md "wikilink")，[台灣競技啦啦隊運動代表隊成员](../Page/台灣.md "wikilink")。

  - [11月30日](../Page/11月30日.md "wikilink")——[黃景瑜](../Page/黃景瑜.md "wikilink")，中國男演員。

### 12月

  - [12月4日](../Page/12月4日.md "wikilink")——[金碩珍](../Page/金碩珍.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[防彈少年團成員](../Page/防彈少年團.md "wikilink")。
  - [12月8日](../Page/12月8日.md "wikilink")——[橫山由依](../Page/橫山由依.md "wikilink")，日本女子團體[AKB48成員](../Page/AKB48.md "wikilink")
  - [12月11日](../Page/12月11日.md "wikilink")——[林玟如](../Page/林玟如.md "wikilink")，[新店歌手](../Page/新店.md "wikilink")，偶像團體[玫瑰幫成員](../Page/玫瑰幫.md "wikilink")。
  - [12月12日](../Page/12月12日.md "wikilink")——[陳若琳](../Page/陳若琳.md "wikilink")，[中國跳水運動員](../Page/中國.md "wikilink")，
    [2008年及](../Page/2008年.md "wikilink")[2012年](../Page/2012年.md "wikilink")[奧運會金牌得主](../Page/奧運會.md "wikilink")。
  - [12月14日](../Page/12月14日.md "wikilink")——[宮市亮](../Page/宮市亮.md "wikilink")，日本足球運動員。
  - [12月16日](../Page/12月16日.md "wikilink")——[陈骁相](../Page/陈骁相.md "wikilink")，[韩国男子团体Topp](../Page/韩国.md "wikilink")
    Dogg成员
  - [12月20日](../Page/12月20日.md "wikilink")——[李世榮](../Page/李世榮_\(韓國\).md "wikilink")，[韓國](../Page/韓國.md "wikilink")[童星](../Page/童星.md "wikilink")。
  - [12月22日](../Page/12月22日.md "wikilink")——[忽那汐里](../Page/忽那汐里.md "wikilink")，日本女演員
  - [12月22日](../Page/12月22日.md "wikilink")——[文星伊](../Page/文星伊.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[MAMAMOO成員](../Page/MAMAMOO.md "wikilink")
  - [12月26日](../Page/12月26日.md "wikilink")——[夏志明](../Page/夏志明.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")。
  - [12月27日](../Page/12月27日.md "wikilink")——[王家俊](../Page/王家俊.md "wikilink")，[香港無綫電視主播](../Page/香港.md "wikilink")。
  - [12月30日](../Page/12月30日.md "wikilink")——[徐酉奈](../Page/徐酉奈.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[AOA成員](../Page/AOA.md "wikilink")（第二主唱、鍵盤手）

## 逝世

[James_E._Webb,_official_NASA_photo,_1966.jpg](https://zh.wikipedia.org/wiki/File:James_E._Webb,_official_NASA_photo,_1966.jpg "fig:James_E._Webb,_official_NASA_photo,_1966.jpg")
[Li_Xuesan.jpg](https://zh.wikipedia.org/wiki/File:Li_Xuesan.jpg "fig:Li_Xuesan.jpg")

  - [3月16日](../Page/3月16日.md "wikilink")——[王任重](../Page/王任重.md "wikilink")，中國政治家、全國政協副主席
  - [3月27日](../Page/3月27日.md "wikilink")——[詹姆斯·韦伯](../Page/詹姆斯·韦伯.md "wikilink")，[美国国家航空航天局第二任局长](../Page/美国国家航空航天局.md "wikilink")（[1906年出生](../Page/1906年.md "wikilink")）
  - [4月6日](../Page/4月6日.md "wikilink")——[艾萨克·阿西莫夫](../Page/艾萨克·阿西莫夫.md "wikilink")，[美国](../Page/美国.md "wikilink")[科幻小说作家](../Page/科幻小说.md "wikilink")（[1920年出生](../Page/1920年.md "wikilink")）
  - [4月13日](../Page/4月13日.md "wikilink")——[范学淹](../Page/范学淹.md "wikilink")，[天主教保定教区主教](../Page/天主教保定教区.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [5月14日](../Page/5月14日.md "wikilink")——[聂荣臻](../Page/聂荣臻.md "wikilink")，[中国政治家和军事家](../Page/中国.md "wikilink")（[1899年出生](../Page/1899年.md "wikilink")）
  - [5月23日](../Page/5月23日.md "wikilink")——[乔瓦尼·法尔科内](../Page/乔瓦尼·法尔科内.md "wikilink")，[意大利司法部刑法司长](../Page/意大利.md "wikilink")、被视作反[黑手党](../Page/意大利黑手党.md "wikilink")“旗帜”的著名[法官](../Page/法官.md "wikilink")，在[西西里岛被](../Page/西西里岛.md "wikilink")[黑手党](../Page/黑手党.md "wikilink")[暗杀](../Page/暗杀.md "wikilink")
  - [6月21日](../Page/6月21日.md "wikilink")——[李先念](../Page/李先念.md "wikilink")，[中華人民共和國原](../Page/中華人民共和國.md "wikilink")[主席](../Page/中華人民共和國主席.md "wikilink")、[全國政協主席](../Page/全國政協.md "wikilink")（[1909年出生](../Page/1909年.md "wikilink")）
  - [7月11日](../Page/7月11日.md "wikilink")——[鄧穎超](../Page/鄧穎超.md "wikilink")，中國政治家、社会活动家，[周恩來夫人](../Page/周恩來.md "wikilink")（[1904年出生](../Page/1904年.md "wikilink")）
  - [7月27日](../Page/7月27日.md "wikilink")——[藍來訥](../Page/藍來訥.md "wikilink")，[英國駐華臨時代辦](../Page/英國駐華臨時代辦.md "wikilink")（[1900年出生](../Page/1900年.md "wikilink")）
  - [8月3日](../Page/8月3日.md "wikilink")——[王洪文](../Page/王洪文.md "wikilink")，原[中共中央副主席](../Page/中共中央.md "wikilink")，[四人幫成員](../Page/四人幫.md "wikilink")
  - [10月8日](../Page/10月8日.md "wikilink")——[维利·勃兰特](../Page/维利·勃兰特.md "wikilink")，[德國總理](../Page/德國總理.md "wikilink")、政治家（[1913年出生](../Page/1913年.md "wikilink")）
  - [11月7日](../Page/11月7日.md "wikilink")——[杜布切克](../Page/杜布切克.md "wikilink")，[捷克政治家](../Page/捷克.md "wikilink")（[1921年出生](../Page/1921年.md "wikilink")）
  - [11月17日](../Page/11月17日.md "wikilink")——[路遥](../Page/路遥.md "wikilink")，[中國作家](../Page/中國.md "wikilink")（[1949年出生](../Page/1949年.md "wikilink")）
  - [11月30日](../Page/11月30日.md "wikilink")——[殷宏章](../Page/殷宏章.md "wikilink")(Hung-Chang
    Yin)，85歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1908年](../Page/1908年.md "wikilink"))
  - [12月5日](../Page/12月5日.md "wikilink")——[扬·奥尔特](../Page/扬·奥尔特.md "wikilink")，[荷兰天文学家](../Page/荷兰.md "wikilink")
  - [12月1日](../Page/12月1日.md "wikilink")——[鳳凰女](../Page/鳳凰女.md "wikilink")，[香港著名](../Page/香港.md "wikilink")[粵劇演員](../Page/粵劇.md "wikilink")
  - [12月22日](../Page/12月22日.md "wikilink")——[李雪三](../Page/李雪三.md "wikilink")，[中国人民解放军](../Page/中国人民解放军.md "wikilink")[中将](../Page/中国人民解放军中将.md "wikilink")（1910年出生）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[乔治·夏帕克](../Page/乔治·夏帕克.md "wikilink")（[法国](../Page/法国.md "wikilink")）
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[鲁道夫·马库斯](../Page/鲁道夫·马库斯.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[埃德蒙·费希尔](../Page/埃德蒙·费希尔.md "wikilink")（Edmond
    H.
    Fischer，[美国](../Page/美国.md "wikilink")），[埃德温·克雷布斯](../Page/埃德温·克雷布斯.md "wikilink")（Edwin
    G. Krebs，美国）
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[德里克·沃尔科特](../Page/德里克·沃尔科特.md "wikilink")，（圣卢西亚诗人）
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：丽葛贝塔·门楚，（Rigoberta Menchú
    Tum，[危地马拉](../Page/危地马拉.md "wikilink")）
  - [经济](../Page/诺贝尔经济学奖.md "wikilink")：[蓋瑞·貝克](../Page/蓋瑞·貝克.md "wikilink")，（GARY
    S. BECKER，[美国](../Page/美国.md "wikilink")）

## 世界足球先生

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球员</p></th>
<th><p>所在球队</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/范巴斯滕.md" title="wikilink">馬高·雲巴士頓</a></p></td>
<td><p><a href="../Page/AC米兰.md" title="wikilink">AC米兰</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/史岱哲哥夫.md" title="wikilink">史岱哲哥夫</a></p></td>
<td><p><a href="../Page/巴塞罗那足球俱乐部.md" title="wikilink">巴塞罗那</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/托马斯·哈斯勒.md" title="wikilink">湯馬士·希士拿</a></p></td>
<td><p><a href="../Page/罗马足球俱乐部.md" title="wikilink">罗马</a></p></td>
</tr>
</tbody>
</table>

## 參考文獻

[\*](../Category/1992年.md "wikilink")
[2年](../Category/1990年代.md "wikilink")
[9](../Category/20世纪各年.md "wikilink")

1.
2.
3.
4.