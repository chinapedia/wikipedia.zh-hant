**朔州市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山西省下辖的](../Page/山西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于山西省西北部，[大同盆地西南端](../Page/大同盆地.md "wikilink")，[桑干河上游](../Page/桑干河.md "wikilink")。

朔州是中国的一个以煤电为主导的能源城市，也是中国农区最大的奶牛基地和北方重要的陶瓷生产基地。

## 历史

### 史前时期

[峙峪](../Page/峙峪.md "wikilink")、边耀、鹅毛口等古遗址表明，距今约28000年前朔州就有了古人类的活动。

### 先秦至秦汉

[春秋以前](../Page/春秋.md "wikilink")，这里为少数民族[北狄所居](../Page/北狄.md "wikilink")，[战国时归入](../Page/战国.md "wikilink")[赵国版图](../Page/赵国.md "wikilink")。秦始皇三十二年（前215年）边帅[蒙恬在此筑城名马邑](../Page/蒙恬.md "wikilink")，置马邑县，归雁门郡管辖。[西汉时置马邑县](../Page/西汉.md "wikilink")（今朔城区）、中陵县（今平鲁区）、剧阳（今应县）、阴馆（今朔城区东南）、汪陶（今山阴县）、埒县（今朔城区南）、楼烦（今朔城区西南）、善无（今右玉县），归雁门郡管辖。[东汉末年县废](../Page/东汉.md "wikilink")。

### 两晋至隋唐

[西晋时将](../Page/西晋.md "wikilink")[雁门关以北各县民撤往关南](../Page/雁门关.md "wikilink")，地归代王拓拔猗卢。[北魏时属畿内地](../Page/北魏.md "wikilink")，置桑乾郡、繁峙郡、马邑郡。[北齐天保六年](../Page/北齐.md "wikilink")（555年），将朔州治从盛乐（今内蒙古和林格尔县）迁到马邑西南；天保八年改马邑县为招远县，为朔州治。[北周升朔州为总管府](../Page/北周.md "wikilink")。[隋时改为马邑郡](../Page/隋.md "wikilink")，辖鄯阳（又稱开阳，今朔城区南部）、神武（今山阴、应县境）。[唐武德四年](../Page/唐.md "wikilink")（621年）改马邑为朔州；天宝八年（742年）又改朔州为马邑郡。

### 五代到元朝

[五代十国](../Page/五代十国.md "wikilink")、[辽](../Page/辽.md "wikilink")、[金](../Page/金.md "wikilink")、[元皆称鄯阳县](../Page/元.md "wikilink")，[宋为朔宁府治](../Page/宋.md "wikilink")，[金](../Page/金.md "wikilink")、[元皆为朔州治](../Page/元.md "wikilink")。

### 明清时期

[明洪武二年](../Page/明.md "wikilink")，鄯阳县并入朔州，嘉靖中为冀北道驻地。[清仍为朔州](../Page/清.md "wikilink")，属大同府，雍正三年（1725年）改属朔平府。

### 现代

[Wenchangge_Pavillion,_Shuoxian_County.jpg](https://zh.wikipedia.org/wiki/File:Wenchangge_Pavillion,_Shuoxian_County.jpg "fig:Wenchangge_Pavillion,_Shuoxian_County.jpg")
1912年（[民國元年](../Page/民國.md "wikilink")）改朔州为朔县，隶雁门道。后雁门撤道，直隶[山西省](../Page/山西.md "wikilink")。

1946年朔县被[中国共产党攻取](../Page/中国共产党.md "wikilink")，归察哈尔省，1952年察哈尔省撤消，重归山西省。1989年1月5日正式设市。下辖朔城区（原朔县）、平鲁区（原平鲁县）和山阴县。1993年7月原雁北地区撤消，其所辖的应县、右玉县和怀仁县划归朔州市。

## 地理

朔州市地形以山地、丘陵为主，占到总面积的60％以上。境内海拔在1600米以上的山峰就有140多座。最高海拔位于山阴县东南部的翠薇山，主峰馒头山海拔2426米；河流分布较广，主要河流共29条，基本分属海河流域和黄河流域。

[Shuozhou-Nordeuropa.jpg](https://zh.wikipedia.org/wiki/File:Shuozhou-Nordeuropa.jpg "fig:Shuozhou-Nordeuropa.jpg")

### 气候

朔州市属典型的大陆性[温带季风气候](../Page/温带季风气候.md "wikilink")。冬季低温干燥，冬长夏短，多风少雨，日照长，降水集中，年平均气温6.4摄氏度，平均降水量428毫米，无霜期120天左右。

## 政治

### 现任领导

<table>
<caption>朔州市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党朔州市委员会.md" title="wikilink">中国共产党<br />
朔州市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/朔州市人民代表大会.md" title="wikilink">朔州市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/朔州市人民政府.md" title="wikilink">朔州市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议朔州市委员会.md" title="wikilink">中国人民政治协商会议<br />
朔州市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/陈振亮.md" title="wikilink">陈振亮</a>[1]</p></td>
<td><p><a href="../Page/冯云龙.md" title="wikilink">冯云龙</a>[2]</p></td>
<td><p><a href="../Page/高键.md" title="wikilink">高键</a>[3]</p></td>
<td><p><a href="../Page/贾桂梓.md" title="wikilink">贾桂梓</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山西省.md" title="wikilink">山西省</a><a href="../Page/寿阳县.md" title="wikilink">寿阳县</a></p></td>
<td><p>山西省<a href="../Page/宁武县.md" title="wikilink">宁武县</a></p></td>
<td><p>山西省<a href="../Page/稷山县.md" title="wikilink">稷山县</a></p></td>
<td><p>山西省<a href="../Page/朔州市.md" title="wikilink">朔州市</a><a href="../Page/平鲁区.md" title="wikilink">平鲁区</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年1月</p></td>
<td><p>2015年12月</p></td>
<td><p>2018年1月</p></td>
<td><p>2015年12月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[市辖区](../Page/市辖区.md "wikilink")、3个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[朔城区](../Page/朔城区.md "wikilink")、[平鲁区](../Page/平鲁区.md "wikilink")
  - 县级市：[怀仁市](../Page/怀仁市.md "wikilink")
  - 县：[山阴县](../Page/山阴县.md "wikilink")、[应县](../Page/应县.md "wikilink")、[右玉县](../Page/右玉县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>朔州市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>140600</p></td>
</tr>
<tr class="odd">
<td><p>140602</p></td>
</tr>
<tr class="even">
<td><p>140603</p></td>
</tr>
<tr class="odd">
<td><p>140621</p></td>
</tr>
<tr class="even">
<td><p>140622</p></td>
</tr>
<tr class="odd">
<td><p>140623</p></td>
</tr>
<tr class="even">
<td><p>140681</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>朔州市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>朔州市</p></td>
<td><p>1714857</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>朔城区</p></td>
<td><p>505294</p></td>
<td><p>29.47</p></td>
</tr>
<tr class="even">
<td><p>平鲁区</p></td>
<td><p>203793</p></td>
<td><p>11.88</p></td>
</tr>
<tr class="odd">
<td><p>山阴县</p></td>
<td><p>238885</p></td>
<td><p>13.93</p></td>
</tr>
<tr class="even">
<td><p>应　县</p></td>
<td><p>327973</p></td>
<td><p>19.13</p></td>
</tr>
<tr class="odd">
<td><p>右玉县</p></td>
<td><p>112063</p></td>
<td><p>6.53</p></td>
</tr>
<tr class="even">
<td><p>怀仁市</p></td>
<td><p>326849</p></td>
<td><p>19.06</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")1714857人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加262982人，增长18.11%。年平均增长率为1.68%。其中，男性人口为895412人，占52.21%；女性人口为819445人，占47.79%。总人口性别比（以女性为100）为109.27。0－14岁人口为308401人，占17.98%；15－64岁人口为1281166人，占74.71%；65岁及以上人口为125290人，占7.31%。

## 资源

矿产以[煤为主](../Page/煤.md "wikilink")，兼[铁](../Page/铁.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、粘土、铝矾土、高岭土、石墨、云母、明矾、石英、[锰等等](../Page/锰.md "wikilink")。

## 经济

朔州是新兴的工业基地，兼有电力、机械、化工、建材、陶瓷、食品等行业。农业有粮食种植、果树栽培、油料种植、牧业、渔业。

  - [平朔煤业](../Page/平朔煤业.md "wikilink")
  - [神头-{}-发电厂](../Page/神头发电厂.md "wikilink")

## 交通

### 市内公共交通

市内环城公交车有南环和北环，公交公司汽车有1、2、3、5、6、7、8、9、11路。车费为1元。

### 公路铁路

[缩略图](https://zh.wikipedia.org/wiki/File:Shuozhou_Railway_Station_2013.08.30_16-01-19.jpg "fig:缩略图")
北[同蒲铁路电气化复线](../Page/同蒲铁路.md "wikilink")、大运二级公路、神朔铁路、朔黄铁路、平万公路、朔蔚公路和连接[京大高速公路的](../Page/京大高速公路.md "wikilink")[大运高速公路纵贯境内](../Page/大运高速公路.md "wikilink")，铁路专用线和公路干线纵横交错，县乡公路四通八达，是[山西省第一个实现乡乡通油路的地级市](../Page/山西省.md "wikilink")。

### 航空

平朔机场已于2003年撤消。现在改为汽车驾驶培训学校。

## 文化

### 名胜古跡

  - [崇福寺](../Page/崇福寺.md "wikilink")
  - [应县木塔](../Page/应县木塔.md "wikilink")
  - [峙峪遗址](../Page/峙峪遗址.md "wikilink")
  - [马邑汉墓群](../Page/马邑汉墓群.md "wikilink")
  - [广武城](../Page/广武城.md "wikilink")

### 民俗与艺术

  - [旺火](../Page/旺火.md "wikilink")
  - [骡驮轿](../Page/骡驮轿.md "wikilink")
  - [耍孩儿](../Page/耍孩儿.md "wikilink")
  - [右玉道情](../Page/右玉道情.md "wikilink")

### 历史人物

  - [班婕妤](../Page/班婕妤.md "wikilink")
  - [王家屏](../Page/王家屏.md "wikilink")
  - [张辽](../Page/张辽.md "wikilink")
  - [尉迟恭](../Page/尉迟恭.md "wikilink")
  - [李存勖](../Page/李存勖.md "wikilink")
  - [李林](../Page/李林.md "wikilink")
  - [苏体仁](../Page/苏体仁.md "wikilink")

## 教育

### 高等教育

共三所高校（一所本科，两所专科类大专高职院校）

  - [朔州师范高等专科学校](http://www.sxszsfdx.com/)

中北大学朔州校区 ×[朔州职业技术学院](http://www.szvtc.com/)

### 基础教育

代表性学校：

  - [朔城区一中](../Page/朔城区一中.md "wikilink")
  - [怀仁一中](../Page/怀仁一中.md "wikilink")
  - [应县一中](../Page/应县一中.md "wikilink")
  - [右玉一中](../Page/右玉一中.md "wikilink")
  - [山阴中学](../Page/山阴中学.md "wikilink")
  - [朔城区二中](../Page/朔城区二中.md "wikilink")
  - [朔城区三中](../Page/朔城区三中.md "wikilink")
  - [朔城区四中](../Page/朔城区四中.md "wikilink")
  - [朔城区五中](../Page/朔城区五中.md "wikilink")
  - [朔城区六中](../Page/朔城区六中.md "wikilink")
  - [朔城区七中](../Page/朔城区七中.md "wikilink")
  - [朔城区八中](../Page/朔城区八中.md "wikilink")
  - [朔城区九中](../Page/朔城区九中.md "wikilink")
  - [朔州市实验学校](../Page/朔州市实验学校.md "wikilink")
  - [朔州市一中](../Page/朔州市一中.md "wikilink")
  - [朔州市二中](../Page/朔州市二中.md "wikilink")
  - [朔州市四中](../Page/朔州市四中.md "wikilink")
  - [朔州市五中](../Page/朔州市五中.md "wikilink")
  - [占义学校](../Page/占义学校.md "wikilink")
  - [旭日学校](../Page/旭日学校.md "wikilink")
  - [元博中学](../Page/元博中学.md "wikilink")
  - [李林中学](../Page/李林中学.md "wikilink")
  - [开发区实验中学](../Page/开发区实验中学.md "wikilink")
  - [民福中学](../Page/民福中学.md "wikilink")
  - [平朔中学](../Page/平朔中学.md "wikilink")

## 参考文献

## 外部链接

  - [朔州公众信息网](https://web.archive.org/web/20121026014202/http://www.sz.sx.cei.gov.cn/)
  - [中国朔州](http://www.shuozhou.gov.cn)

{{-}}

[朔州](../Category/朔州.md "wikilink")
[Category:山西地级市](../Category/山西地级市.md "wikilink")
[晋](../Category/中国小城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.