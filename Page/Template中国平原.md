[松嫩平原](松嫩平原.md "wikilink")（嫩江平原{{.w}}松花江平原）{{.w}}[辽河平原](辽河平原.md "wikilink"){{.w}}[黑龙江谷地](黑龙江谷地.md "wikilink"){{.w}}[鸭绿江谷地](鸭绿江谷地.md "wikilink")
|group2 = [华北平原](华北平原.md "wikilink") |list2 =
[海河平原](海河平原.md "wikilink"){{.w}}[黄泛平原](黄泛平原.md "wikilink"){{.w}}[黄河三角洲](黄河三角洲.md "wikilink"){{.w}}[胶莱平原](胶莱平原.md "wikilink"){{.w}}[淮河平原](淮河平原.md "wikilink"){{.w}}[黄淮平原](黄淮平原.md "wikilink"){{.w}}[里下河平原](里下河平原.md "wikilink")
|group3 = [长江中下游平原](长江中下游平原.md "wikilink") |list3 =
[两湖平原](两湖平原.md "wikilink")（[江汉平原](江汉平原.md "wikilink"){{.w}}[洞庭湖平原](洞庭湖平原.md "wikilink")）{{.w}}[鄱阳湖平原](鄱阳湖平原.md "wikilink"){{.w}}[皖中平原](皖中平原.md "wikilink"){{.w}}[长江三角洲](长江三角洲.md "wikilink"){{.w}}[太湖平原](太湖平原.md "wikilink"){{.w}}[杭嘉湖平原](杭嘉湖平原.md "wikilink"){{.w}}[萧绍平原](萧绍平原.md "wikilink"){{.w}}[赣抚平原](赣抚平原.md "wikilink"){{.w}}[澧阳平原](澧阳平原.md "wikilink"){{.w}}[巢湖平原](巢湖平原.md "wikilink"){{.w}}[江淮平原](江淮平原.md "wikilink")
|group4 = [东南沿海诸平原](东南沿海诸平原.md "wikilink") |list4 =
[宁绍平原](宁绍平原.md "wikilink"){{.w}}[温黄平原](温黄平原.md "wikilink"){{.w}}[温瑞平原](温瑞平原.md "wikilink"){{.w}}[鳌江平原](鳌江平原.md "wikilink"){{.w}}[福建四大平原](福建四大平原.md "wikilink")([福州平原](福州平原.md "wikilink"){{.w}}[兴化平原](兴化平原.md "wikilink"){{.w}}[泉州平原](泉州平原.md "wikilink"){{.w}}[漳州平原](漳州平原.md "wikilink")){{.w}}[潮汕平原](潮汕平原.md "wikilink"){{.w}}[珠江三角洲](珠江三角洲.md "wikilink"){{.w}}[浔江平原](浔江平原.md "wikilink"){{.w}}[南流江三角洲](南流江三角洲.md "wikilink"){{.w}}[宾阳平原](宾阳平原.md "wikilink"){{.w}}[元朗平原](元朗.md "wikilink")
|group5 = [黄河中上游诸平原](黄河中上游诸平原.md "wikilink") |list5 =
[湟水谷地](湟水谷地.md "wikilink"){{.w}}[银川平原](银川平原.md "wikilink"){{.w}}[河套平原](河套.md "wikilink"){{.w}}[土默川平原](土默川平原.md "wikilink"){{.w}}[汾河谷地](汾河谷地.md "wikilink"){{.w}}[运城盆地](运城盆地.md "wikilink"){{.w}}[沁水盆地](沁水盆地.md "wikilink"){{.w}}[渭河平原](渭河平原.md "wikilink")（关中平原）
|group6 = 其他平原 |list6 =
[成都平原](成都平原.md "wikilink"){{.w}}[汉水谷地](汉水谷地.md "wikilink"){{.w}}[伊犁谷地](伊犁谷地.md "wikilink"){{.w}}[河西走廊](河西走廊.md "wikilink"){{.w}}[遼西走廊](遼西走廊.md "wikilink")
}}<noinclude> </noinclude>

[\*](../Category/中国平原.md "wikilink")
[Category:中国地理模板](../Category/中国地理模板.md "wikilink")