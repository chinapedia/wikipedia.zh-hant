**TYPE-MOON**（日文：，中文：型月（月型）），是[日本遊戲發行商Notes有限公司在發行商品時所使用的品牌名稱](../Page/日本.md "wikilink")。Notes的負責人为画家[武内崇](../Page/武内崇.md "wikilink")，而社名則來自作家[奈須蘑菇的作品](../Page/奈須蘑菇.md "wikilink")《Notes.》。

## 历史

TYPE-MOON原本是一个[同人社團](../Page/同人社團.md "wikilink")，由武内崇及奈須蘑菇兩人主宰。他們第一个比较有知名度的作品是《[空之境界](../Page/空之境界.md "wikilink")》，在1998年10月至1999年5月期间，奈须在他和武内崇的个人网站“[竹箒](../Page/竹箒.md "wikilink")”上的定期连载。而奈须个人的小说作品还有收录于设定本《[青本](../Page/青本.md "wikilink")》的短篇小说《[NOTES.](../Page/NOTES..md "wikilink")》，以及曾日本新文学杂志《浮士德》上连载但未完結的DDD系列。另有未发表作品如《[冰之花](../Page/冰之花.md "wikilink")》等。

TYPE-MOON真正的成名之作是在2000年的冬[Comic
Market上发行的](../Page/Comic_Market.md "wikilink")《[月姬](../Page/月姬.md "wikilink")》，凭着TYPE-MOON独有的世界观以及对于生死的感触而吸引了无数的玩家。該同人遊戲作品，被动画化成《[真月譚月姬](../Page/真月譚月姬.md "wikilink")》後，TYPE-MOON繼續以同人身份出品了《[月姬PLUS](../Page/月姬PLUS.md "wikilink")》、《[歌月十夜](../Page/歌月十夜.md "wikilink")》、《月箱》。而他们还与著名同人[格斗游戏制作社团](../Page/格斗游戏.md "wikilink")“渡边制作所”一起制作了同人格斗游戏《[MELTY
BLOOD](../Page/MELTY_BLOOD.md "wikilink")》，2004年5月发售其资料片《[MELTY BLOOD
Re·act](../Page/MELTY_BLOOD_Re·act.md "wikilink")》。而之后更被[街机化](../Page/街机.md "wikilink")，名为「[Melty
blood Act
Cadenza](../Page/Melty_blood_Act_Cadenza.md "wikilink")」，并于2006年8月发售[PS2版本](../Page/PS2.md "wikilink")。

2003年，武內與奈須設立了Notes有限公司。同年4月，以同人名義發行了收錄過去到現在所發表作品（除了《[Melty
Blood](../Page/Melty_Blood.md "wikilink")》）的《月箱》之後，解散了同人社團TYPE-MOON，並將其定位為Notes有限公司之下的一個商業品牌。

2004年1月30日，有限會社Notes发行了他们在商业公司化后的第一个游戏《[Fate/stay
night](../Page/Fate/stay_night.md "wikilink")》。而《Fate/stay
night》的Fate路線故事也在2006年1月被动画化，其女主角之一的Saber人气極高（在TYPE-MOON两次举办的人气投票上，都是Saber第一、远坂凛第二、Archer第三）。在2005年秋末，《[Fate/stay
night](../Page/Fate/stay_night.md "wikilink")》的FAN DISC《[Fate/hollow
ataraxia](../Page/Fate/hollow_ataraxia.md "wikilink")》发售。

2006年12月29日由TYPE-MOON与Nitro+合作发售了《[Fate/stay
night](../Page/Fate/stay_night.md "wikilink")》的前传小說《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》，並于2010年12月21日公布Fate/Zero动画化的消息，制作公司为ufotable。动画于2011年10月1日首播，动画分成前半部及后半部；前半部共13集，播映时间为2011年10月－2011年12月；后半部共12集，播映时间为2012年4月至2012年6月；共25集。

2012年4月12日，《[魔法使之夜](../Page/魔法使之夜.md "wikilink")》在Windows平台登陆，本作是与《[月姬](../Page/月姬.md "wikilink")》、《[Fate/stay
night](../Page/Fate/stay_night.md "wikilink")》和《[空之境界](../Page/空之境界.md "wikilink")》相关联的奈须世界的原型。片尾曲由[supercell创作](../Page/supercell.md "wikilink")，名为《星が瞬くこんな夜に》。

他们的成功在日本與[東方Project和](../Page/東方Project.md "wikilink")[寒蝉鸣泣之时同被譽為](../Page/寒蝉鸣泣之时.md "wikilink")「同人三大奇蹟」之一\[1\]，具有相当大的影响力，让更多人致力於同人创作。

## 作品列表

### 遊戲

#### 月姬系列

  - 月姬（先行预告版）

<!-- end list -->

  -
    时间：1999年夏COMIKE（即第57次[Comic
    Market](../Page/Comic_Market.md "wikilink")，简称C57，下同）

<!-- end list -->

  - 月姬（体验版）

<!-- end list -->

  -
    时间：1999年冬COMIKE（C58）

<!-- end list -->

  - 月姬（半月版）

<!-- end list -->

  -
    时间：2000年夏COMIKE（C59）

<!-- end list -->

  - 月姬（满月版）

<!-- end list -->

  -
    时间：2000年冬COMIKE（C60）

<!-- end list -->

  - 月姬PLUS-DISK

<!-- end list -->

  -
    时间：2001年1月
    月姬的订购特典，收录了月姬人物茶会闲谈的\[后日谈\]和以远野秋叶的学妹濑尾晶为主角的短篇「幻视同盟」，《空之境界》的前四话以及部分资料与壁纸。类似于资料片。（以下如非特别注明，提到“月姬”都是指满月版）

<!-- end list -->

  - [歌月十夜](../Page/歌月十夜.md "wikilink")

<!-- end list -->

  -
    时间：2001年8月
    月姬的FAN DISC，主要目的就是为了娱乐各位TYPE MOON的FANS。其中有些故事是取自读者的投稿。

<!-- end list -->

  - [Melty Blood](../Page/Melty_Blood.md "wikilink")

<!-- end list -->

  -
    时间：2002年12月30日
    TYPE-MOON与同人团体渡边制作所合作的月姬同人格斗游戏，剧情模式的故事由奈须执笔。
    而這作品在2005年在ps2 發表。

<!-- end list -->

  - [月箱](../Page/月箱.md "wikilink")

<!-- end list -->

  -
    时间：2003年4月
    月姬、PLUS DISK、歌月十夜的集合

<!-- end list -->

  - [MELTY BLOOD Re·Act](../Page/MELTY_BLOOD_Re·Act.md "wikilink")

<!-- end list -->

  -
    时间：2004年5月30日
    MELTY BLOOD的资料片。

#### Fate系列

  - [Fate/stay night](../Page/Fate/stay_night.md "wikilink")（2004年）

<!-- end list -->

  -
    时间：初回限定版2004年1月30日，通常版2004年3月26日，DVD版2006年3月29日
    商业会社化后第一作，当年以146686本的销量位居美少女遊戲榜首。2005年年内的销量仍然有28557本，排在第九位。

<!-- end list -->

  - [Fate/hollow
    ataraxia](../Page/Fate/hollow_ataraxia.md "wikilink")（2005年）

<!-- end list -->

  -
    时间：初回限定版2005年10月28日，通常版2005年12月29日。
    Fate/stay night的FAN DISC，虽然发售日接近年末，但是仍以154015本的销量位居美少女遊戲销量榜首。

<!-- end list -->

  - Fate/stay night \[Réalta Nua\]（2007年）

<!-- end list -->

  - 飛吧！老虎的紙牌道中記（，2007年）

<!-- end list -->

  - 飛吧！超時空老虎的紙牌道中記（，2012年）

<!-- end list -->

  - [Fate/tiger
    colosseum](../Page/Fate/tiger_colosseum.md "wikilink")（2008年）

<!-- end list -->

  - [Fate/EXTRA](../Page/Fate/EXTRA.md "wikilink")（2010年）

<!-- end list -->

  - Fate/EXTRA CCC（2013年）

<!-- end list -->

  - [Fate/kaleid liner
    魔法少女☆伊莉雅](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅.md "wikilink")（2014年）

<!-- end list -->

  - [Fate/Grand Order](../Page/Fate/Grand_Order.md "wikilink")（2015年）

<!-- end list -->

  - [Fate/EXTELLA](../Page/Fate/EXTELLA.md "wikilink")（2016年）

<!-- end list -->

  - [Fate/EXTELLA LINK](../Page/Fate/EXTELLA_LINK.md "wikilink")（2018年）

#### 其他作品

  - [魔法使之夜](../Page/魔法使之夜.md "wikilink")（Witch on the Holy Night）

<!-- end list -->

  -
    奈须未发表、未完成的[奇幻小说](../Page/奇幻小说.md "wikilink")，以苍崎姐妹为主角。
    据武内崇的描述，开场剧情是：“少女苍崎青子在使用魔术时不慎被普通人静希草十郎目击到，于是同为魔术师的久远寺有珠便劝其杀人灭口。”苍崎橙子在这部作品里可能以反派身份出场，其余不详。
    后游戏化为[视觉小说](../Page/视觉小说.md "wikilink")，经历数次跳票后于2012年4月12日发售初回版\[2\]，同年8月10日发售通常版。

### 書籍

#### 小說

  - [Fate/Zero](../Page/Fate/Zero.md "wikilink")（2006年－2007年，作者：[虛淵玄](../Page/虛淵玄.md "wikilink")，插圖：武内崇）
      - 《[Fate/stay night](../Page/Fate/stay_night.md "wikilink")》外傳作品。
  - [Fate/Apocrypha](../Page/Fate/Apocrypha.md "wikilink")（2012年－2014年，作者：[東出祐一郎](../Page/東出祐一郎.md "wikilink")，插圖：[近衛乙嗣](../Page/近衛乙嗣.md "wikilink")）
      - 《Fate/stay night》外傳作品。
  - [Fate/Prototype
    蒼銀的碎片](../Page/Fate/Prototype.md "wikilink")（2013年－2016年，作者：[櫻井光](../Page/櫻井光.md "wikilink")，插圖：中原）

#### 漫畫

  - [琥珀ACE](../Page/琥珀ACE.md "wikilink")
  - Fate/KOHA-ACE 帝都聖杯奇譚

#### 設定資料集

## 員工

### 現任員工

  - 武内崇
    創始成員，公司代表董事，擔任[原畫師](../Page/原畫師.md "wikilink")、[人物設計師和製作人](../Page/人物設計師.md "wikilink")。
  - 奈須蘑菇
    創始成員，劇作及小說家。
  - 清兵衛
    創始成員，負責電腦編程。
  - 芳賀敬太(KATE)
    創始成員，負責聲效創作。
  - OKSG(AYSG)
    於製作月姬時時加入，負責手機及網頁與一般事務。
  - [こやまひろかず](../Page/こやまひろかず.md "wikilink")
    2001年製作『歌月十夜』時加入，負責[CG繪圖及原畫](../Page/電腦圖形.md "wikilink")，於後來成為作畫總監。
  - BLACK
    2002年加入，負責CG繪圖。
  - 徳
    2003年加入，負責營業及發言事項。
  - 蒼月タカオ
    2003年加入，負責CG繪圖。
  - つくりものじ
    2006年加入，負責腳本編寫。
  - [星空流星](../Page/星空流星.md "wikilink")
    2005年加入，負責编剧及小說。
  - のきつ
    2006年加入，負責版權管理及活動營運。
  - 下越
    2006年加入，負責繪圖。
  - 砂鳥音幸
    2008年加入，負責[3D建模](../Page/三维模型.md "wikilink")。
  - アザナシ
    2012年加入，負責電腦編程。
  - 縞うどん
    2013年加入，負責繪圖。

### 前員工

  - MORIYA
    負責繪圖。
  - AZ-UME
    負責電腦編程。

## 参考文献

## 外部連結

**官方网站：**

  - [TYPE-MOON同人團體時期網站](http://www.typemoon.org/)

  - [TYPE-MOON成立公司後之官方網站](http://www.typemoon.com/)

  - [TYPE-MOON第二官方網站](https://web.archive.org/web/20101224122156/http://typemoon.com/index.html)

  - [竹箒](http://www.remus.dti.ne.jp/~takeucto/)（TYPE-MOON創立者[武内崇及](../Page/武内崇.md "wikilink")[奈須蘑菇的日記網站](../Page/奈須蘑菇.md "wikilink")）

  -
**非官方网站：**

  - [TYPE-MOON
    Wiki](http://wiki.cre.jp/typemoon/%E3%83%A1%E3%82%A4%E3%83%B3%E3%83%9A%E3%83%BC%E3%82%B8)
    （介紹TYPE-MOON作品的日文专门百科）

  - [TYPE-MOON
    Wiki](http://typemoon.wikia.com/wiki/TYPE-MOON_Wiki)（建立在[Wikia上的专门百科](../Page/Wikia.md "wikilink")）

  - [Tsuki-kan](http://www.tsukikan.com/)（TYPE-MOON 英文）

[Category:日本成人遊戲公司](../Category/日本成人遊戲公司.md "wikilink")
[\*](../Category/TYPE-MOON.md "wikilink")

1.
2.