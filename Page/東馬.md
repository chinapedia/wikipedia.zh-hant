[East_Malaysia_location_map.svg](https://zh.wikipedia.org/wiki/File:East_Malaysia_location_map.svg "fig:East_Malaysia_location_map.svg")、[砂拉越及](../Page/砂拉越.md "wikilink")[纳闽组成的东马](../Page/纳闽.md "wikilink")\]\]
[East_Malaysia_location_map_with_districts.svg](https://zh.wikipedia.org/wiki/File:East_Malaysia_location_map_with_districts.svg "fig:East_Malaysia_location_map_with_districts.svg")、[砂拉越及](../Page/砂拉越.md "wikilink")[纳闽组成的东马](../Page/纳闽.md "wikilink")\]\]
[Borneo2_map_english_names.svg](https://zh.wikipedia.org/wiki/File:Borneo2_map_english_names.svg "fig:Borneo2_map_english_names.svg")及[北婆三邦的东马来西亚三州](../Page/北婆三邦.md "wikilink")\]\]
**东马**（）又称**沙巴砂拉越**（），简称**沙砂**，别译**东马来西亚**，是[马来西亚位于](../Page/马来西亚.md "wikilink")[婆罗洲的北半部](../Page/婆罗洲.md "wikilink")[领土](../Page/领土.md "wikilink")，由[沙巴](../Page/沙巴.md "wikilink")、[砂拉越及](../Page/砂拉越.md "wikilink")[纳闽组成](../Page/纳闽.md "wikilink")，面积佔全國6成（200,657[平方公里](../Page/平方公里.md "wikilink")）餘，但人口僅佔全國2成。地理上，東馬[西馬的位置被](../Page/馬來西亞半島.md "wikilink")[南海所隔](../Page/南海.md "wikilink")，这间接导致人口密度和发展指数低于位于[马来亚半岛的西馬](../Page/马来亚半岛.md "wikilink")。然而，東馬因此保存了丰富的[天然资源](../Page/天然资源.md "wikilink")。

## 行政區域

  - [沙巴](../Page/沙巴.md "wikilink")（旧称[北婆罗洲](../Page/北婆罗洲.md "wikilink")）及[砂拉越](../Page/砂拉越.md "wikilink")：在1963年9月16日和[马来亚联合邦共同組成马来西亚](../Page/马来亚联合邦.md "wikilink")。作为联邦的一员，[沙巴及](../Page/沙巴.md "wikilink")[砂拉越比半岛的州屬享有更高度的自主权](../Page/砂拉越.md "wikilink")，譬如不同的移民系统。2006年以前，进入东马的半岛人民必须持有[馬來西亞护照](../Page/馬來西亞护照.md "wikilink")，跨越沙砂边境同样设立关卡，须要出示护照，目前则可以采用内附晶片的[大马卡通行](../Page/大马卡.md "wikilink")。
  - [纳闽](../Page/纳闽.md "wikilink")：原是[沙巴的一部分](../Page/沙巴.md "wikilink")；1984年[纳闽被划为联邦直辖区](../Page/纳闽.md "wikilink")，直接由联邦政府管辖。
  - 主要城市：
      - [沙巴](../Page/沙巴.md "wikilink")：[亞庇](../Page/亞庇.md "wikilink")、[山打根](../Page/山打根.md "wikilink")、[斗湖等](../Page/斗湖.md "wikilink")
      - [砂拉越](../Page/砂拉越.md "wikilink")：[古晋](../Page/古晋.md "wikilink")、[美里](../Page/美里.md "wikilink")、[诗巫](../Page/诗巫.md "wikilink")、[民都鲁等](../Page/民都鲁.md "wikilink")

## 天然資源

東馬雖然[經濟上普遍落后于西馬](../Page/經濟.md "wikilink")，但它擁有丰富的天然资源，包括：

  - [石油及](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")：豐富的藏量尚待開採。
  - 姆禄石洞群：位於[砂拉越的](../Page/砂拉越.md "wikilink")[姆禄国家公园](../Page/姆禄国家公园.md "wikilink")，在2001年被列入[世界遗产](../Page/世界遗产.md "wikilink")，最著名的是世界上最大[石灰岩洞穴](../Page/石灰岩.md "wikilink")－[砂拉越洞厅](../Page/砂拉越洞厅.md "wikilink")。
  - 沙巴北端的[京那峇魯山](../Page/京那峇魯山.md "wikilink")，[海拔](../Page/海拔.md "wikilink")4,095.2公尺，马来西亚最高的山峰。
  - [峇贡水电站](../Page/峇贡水电站.md "wikilink")，马来西亚国内最大的[水电站](../Page/水电站.md "wikilink")。
  - [拉让江](../Page/拉让江.md "wikilink")，马来西亚最长的河，其中[诗巫便是位于拉让江河畔](../Page/诗巫.md "wikilink")。

## 参考

[\*](../Category/馬來西亞地理.md "wikilink")
[Category:婆罗洲](../Category/婆罗洲.md "wikilink")
[\*](../Category/东马.md "wikilink")