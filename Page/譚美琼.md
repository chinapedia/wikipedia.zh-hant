**譚美琼**（），籍贯[廣東](../Page/廣東.md "wikilink")[新會](../Page/新會.md "wikilink")[双水](../Page/双水.md "wikilink")，[香港的](../Page/香港.md "wikilink")[女](../Page/女性.md "wikilink")[配音員](../Page/配音員.md "wikilink")，2000年代任職自由身配音員。

## 人物

  - 經常參與話劇演出。\[1\]

## 大碟

  - [經動童心福音故事集](../Page/經動童心.md "wikilink") -
    [真証傳播出版](../Page/真証傳播.md "wikilink")

## 配音作品

### 動畫/電視劇

  - [幽靈公主](../Page/幽靈公主.md "wikilink")([幽靈公主DVD版本](../Page/幽靈公主.md "wikilink"))([宮崎峻系列](../Page/宮崎峻.md "wikilink"))
    飾演 嘉美、姍姍
  - [百變狸貓DVD版本](../Page/百變狸貓.md "wikilink")([宮崎峻系列](../Page/宮崎峻.md "wikilink"))
    飾演 嘉玲
  - [週末糖癡豆](../Page/週末糖癡豆.md "wikilink")[The
    Weekenders](../Page/The_Weekenders.md "wikilink")
    ([迪士尼](../Page/迪士尼.md "wikilink")[動畫](../Page/動畫.md "wikilink"))
    飾演 蝶絲
  - [Fillmore](../Page/Fillmore.md "wikilink")
    ([迪士尼](../Page/迪士尼.md "wikilink")[動畫](../Page/動畫.md "wikilink"))
    飾演 嘉倫
  - [Sabrina](../Page/Sabrina.md "wikilink"), [Sabrinal
    I](../Page/Sabrinal_I.md "wikilink") 飾演 田芬妮
  - [Sabrina Feature](../Page/Sabrina_Feature.md "wikilink") 飾演 寶妮
  - [勇者傳說](../Page/勇者傳說.md "wikilink")/[勇者物語](../Page/勇者物語.md "wikilink")
    ([Asia Video](../Page/Asia_Video.md "wikilink") DVD版本) 飾演 卡斯
  - [獅子王3](../Page/獅子王3.md "wikilink")
  - [Teacher's Pet](../Page/Teacher's_Pet.md "wikilink")
  - [101斑點狗](../Page/101斑點狗.md "wikilink")2
  - [貓之報恩](../Page/貓之報恩.md "wikilink")([宮崎峻系列](../Page/宮崎峻.md "wikilink"))　飾演　小雪
  - [Polly World](../Page/Polly_World.md "wikilink") 飾演 貝絲、香妮
  - [高飛狗故事](../Page/高飛狗.md "wikilink")、[高飛狗故事](../Page/高飛狗.md "wikilink")2
  - [Barbie之十二公主](../Page/Barbie.md "wikilink") 飾演 愛迪蓮公主
  - [Barbie](../Page/Barbie.md "wikilink")[夢境之旅](../Page/夢境之旅.md "wikilink")
  - [小馬樂園](../Page/小馬樂園.md "wikilink")
    ([迪士尼](../Page/迪士尼.md "wikilink")[動畫](../Page/動畫.md "wikilink"))
  - [鬼太郎](../Page/鬼太郎.md "wikilink")([Animax](../Page/Animax.md "wikilink"))
  - [A Kind of
    Magic](../Page/A_Kind_of_Magic.md "wikilink")([迪士尼](../Page/迪士尼.md "wikilink")[動畫](../Page/動畫.md "wikilink"))飾演
    湯美
  - [Tinker
    Bell](../Page/Tinker_Bell.md "wikilink")[奇妙仙子](../Page/奇妙仙子.md "wikilink")1&2
    飾演 露絲娜
  - [Jewelpet](../Page/Jewelpet.md "wikilink")[寶石寵物](../Page/寶石寵物.md "wikilink")([TVB版本](../Page/TVB.md "wikilink"))
    飾演 朝崗南
  - [霹靂MIT](../Page/霹靂MIT.md "wikilink")([TVB](../Page/TVB.md "wikilink")[J2版本](../Page/J2.md "wikilink"))
  - [小馬寶莉：友情就是魔法](../Page/小馬寶莉：友情就是魔法.md "wikilink")(DVD版本) 飾演 **雲寶**

### 電影

  - [哈利波特—消失的密室](../Page/哈利波特—消失的密室.md "wikilink") 飾演
    [愛哭鬼麥朵](../Page/愛哭鬼麥朵.md "wikilink")
  - [哈利波特－阿玆卡班的逃犯](../Page/哈利波特－阿玆卡班的逃犯.md "wikilink")
  - [十面埋伏](../Page/十面埋伏.md "wikilink")
  - [千機變](../Page/千機變.md "wikilink")2
  - [狗狗震](../Page/狗狗震.md "wikilink")[Scooby-Doo](../Page/Scooby-Doo.md "wikilink")
  - [狗狗震多震](../Page/狗狗震多震.md "wikilink")[Scooby-Doo
    2](../Page/Scooby-Doo_2.md "wikilink")
  - [飛天小女警](../Page/飛天小女警.md "wikilink")[The Powerpuff
    Girls](../Page/The_Powerpuff_Girls.md "wikilink")
  - [哈利波特—火盃的考驗](../Page/哈利波特—火盃的考驗.md "wikilink") 飾演
    [愛哭鬼麥朵](../Page/愛哭鬼麥朵.md "wikilink")
  - [小孩不笨2](../Page/小孩不笨2.md "wikilink") (DVD版本) 飾演 晶晶、四眼妹、醫生、銀行職員、士多老闆娘
  - [吾妻16歲](../Page/吾妻16歲.md "wikilink")[My Little
    Bride](../Page/My_Little_Bride.md "wikilink")
  - [幪面超人響鬼](../Page/幪面超人響鬼.md "wikilink")
    \~[響鬼與七人之戰鬼](../Page/響鬼與七人之戰鬼.md "wikilink")
    飾演 一惠、姬
  - [穿越時空的少女](../Page/穿越時空的少女.md "wikilink")
  - [哈利波特—鳳凰會的密令](../Page/哈利波特—鳳凰會的密令.md "wikilink")
  - [-{zh:辛普森一家;zh-hans:辛普森一家;zh-hant:辛普森一家;zh-cn:辛普森一家;zh-tw:辛普森家庭;zh-hk:阿森一族;zh-mo:阿森一族;}-](../Page/辛普森一家.md "wikilink")
  - [森林反恐隊](../Page/森林反恐隊.md "wikilink")
  - [大象阿鈍救細界](../Page/大象阿鈍救細界.md "wikilink")
  - [大耳仔走天涯](../Page/大耳仔走天涯.md "wikilink")
  - [走佬芝娃娃](../Page/走佬芝娃娃.md "wikilink")[Beverly Hills
    Chihuahua](../Page/Beverly_Hills_Chihuahua.md "wikilink")
  - [寶貝狗太空歷險](../Page/寶貝狗太空歷險.md "wikilink")[Space
    Buddies](../Page/Space_Buddies.md "wikilink") 飾演 比利/污卒卒
  - [哈利波特─混血皇子的背叛](../Page/哈利波特─混血皇子的背叛.md "wikilink")
  - [麥兜響噹噹](../Page/麥兜響噹噹.md "wikilink")
  - [愛麗絲夢遊仙境](../Page/愛麗絲夢遊仙境.md "wikilink")

### 廣告/短片旁白

  - [Skinfill Medical](../Page/Skinfill_Medical.md "wikilink")
  - [Wiom奶粉](../Page/Wiom奶粉.md "wikilink")
  - [Blu Spa](../Page/Blu_Spa.md "wikilink")
  - [大排檔即沖活力豆漿](../Page/大排檔即沖活力豆漿.md "wikilink")
  - [慈康農圃](../Page/慈康農圃.md "wikilink")[點點綠](../Page/點點綠.md "wikilink")
  - [綠之聖猴頭菇湯包系列](../Page/綠之聖猴頭菇湯包.md "wikilink")
  - [Easy Channel系列](../Page/Easy_Channel.md "wikilink")
  - [EG Memory好多謝安琪演唱會](../Page/EG_Memory好多謝安琪演唱會.md "wikilink")
  - [TF Gym健體健身中心](../Page/TF_Gym.md "wikilink")
  - [Dr Pro系列](../Page/Dr_Pro.md "wikilink")
  - [Dr Reborn系列](../Page/Dr_Reborn.md "wikilink")
  - [美麗人生美容瘦身工作坊系列](../Page/美麗人生美容瘦身工作坊.md "wikilink")
  - [泰國人海南雞](../Page/泰國人海南雞.md "wikilink")
  - [板之燒日式放題館](../Page/板之燒.md "wikilink")
  - [香港旅遊協會昂平篇](../Page/香港旅遊協會.md "wikilink")
  - [第二十二屆香港國際旅遊展](../Page/第二十二屆香港國際旅遊展.md "wikilink")
  - [E.nopi Math](../Page/E.nopi_Math.md "wikilink")
    [英軒教育中心](../Page/英軒教育中心.md "wikilink")
  - [佐敦381商場招租](../Page/佐敦381商場招租.md "wikilink")
  - [康和農產品系列](../Page/康和農產品.md "wikilink")
  - [創意洋服](../Page/創意洋服.md "wikilink")
  - [野果時代](../Page/野果時代.md "wikilink") [無患子](../Page/無患子.md "wikilink")
  - [BioSlim鄺文珣天橋篇](../Page/BioSlim.md "wikilink")
  - [自然健身氣功會](../Page/自然健身氣功會.md "wikilink")
  - [德勝再生穴位電療中心](../Page/德勝再生穴位電療中心.md "wikilink")
  - [糊塗戲班舞台劇](../Page/糊塗戲班.md "wikilink")[離留記宣傳片](../Page/離留記.md "wikilink")
  - [天使美容中心](../Page/天使美容中心.md "wikilink")
  - [集蘭堂工展會](../Page/集蘭堂.md "wikilink")
  - [潘高壽工展會](../Page/潘高壽.md "wikilink")
  - [歐家全工展會](../Page/歐家全.md "wikilink")
  - [梭坤敬工展會](../Page/梭坤敬.md "wikilink")
  - [御軒工展會](../Page/御軒.md "wikilink")
  - [珠江橋牌工展會](../Page/珠江橋牌.md "wikilink")
  - [保秀麗工展會](../Page/保秀麗.md "wikilink")
  - [古龍牌食品工展會](../Page/古龍牌食品.md "wikilink")
  - [南島牌咖啡工展會](../Page/南島牌咖啡.md "wikilink")
  - [勝景遊越南](../Page/勝景.md "wikilink")
  - [勝景遊不丹](../Page/勝景.md "wikilink")
  - [Q房網](../Page/Q房網.md "wikilink")

## 舞台劇

  - 2001年[六個尋找作者的角色](../Page/六個尋找作者的角色.md "wikilink")（堅道明愛演藝傳意） 飾演 小男孩
  - 2001年[大刀王五](../Page/大刀王五.md "wikilink")（[劇場空間](../Page/劇場空間.md "wikilink")）
    飾演 女僕
  - 2001及2002年[袁崇煥之死](../Page/袁崇煥之死.md "wikilink")（首演/重演及學生專場）（[致群劇社](../Page/致群劇社.md "wikilink")）
    飾演 幼年堂侄/佘家子孫/學生
  - 2003年[五指戰隊](../Page/五指戰隊.md "wikilink")（[優樂場](../Page/優樂場.md "wikilink")）
    飾演 尾指
  - 2005年[做好狗狗](../Page/做好狗狗.md "wikilink")（[優樂場](../Page/優樂場.md "wikilink")）
    飾演 艾雲芬
  - 2010年[斜路黃花](../Page/斜路黃花.md "wikilink")（[致群劇社](../Page/致群劇社.md "wikilink")）
    飾演 小紅母、小販
  - 2010年[防疫禁區](../Page/防疫禁區.md "wikilink")（[影話戲](../Page/影話戲.md "wikilink")）
    飾演 血管人葉維妮
  - 2011年[戰火梨園](../Page/戰火梨園.md "wikilink")（[致群劇社](../Page/致群劇社.md "wikilink")）
    飾演 小娟
  - 2011年[斜路黃花星級重演](../Page/斜路黃花.md "wikilink")
    ([致群劇社](../Page/致群劇社.md "wikilink")) 飾演 小紅母、小販
  - 2012年[風聲](../Page/風聲.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 飾演 龍川芳子、二姨太
  - 2013年[舞步青雲](../Page/舞步青雲.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 負責 錄像設計、音樂總監助理
  - 2014年[棋廿三](../Page/棋廿三.md "wikilink")（[上海](../Page/上海.md "wikilink")，粵語演出）（[現代戲劇谷](../Page/現代戲劇谷.md "wikilink")、[空間劇場](../Page/空間劇場.md "wikilink")），負責錄像製作\[2\]
  - 2015年[布拉格1968](../Page/布拉格1968.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 任職 錄像設計
  - 2016年[細鳳1959](../Page/細鳳1959.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 任職 錄像設計
  - 2016年[莎士对比亞](../Page/莎士对比亞.md "wikilink")
    ([新光戲院](../Page/新光戲院.md "wikilink")) 任職 助理舞台監督
  - 2016年[喜靈洲修女之帶著矛盾去葡京](../Page/喜靈洲修女之帶著矛盾去葡京.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 任職 字幕控制員
  - 2017年[雙城紀失](../Page/雙城紀失.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 任職 助理舞台監督
  - 2018年[主席萬歲](../Page/主席萬歲.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 飾演 蝦哥助手、村民、導遊 及
    任職 助理舞台監督
  - 2018年[主席萬歲萬萬歲](../Page/主席萬歲萬萬歲.md "wikilink")
    ([劇場空間](../Page/劇場空間.md "wikilink")) 飾演 攝影師、臨時演員 及
    任職 助理舞台監督

## 參考

  - <http://www.theatrespace.org/document/201405_checkmatesP.pdf>

[Category:香港配音員](../Category/香港配音員.md "wikilink")
[Category:新會人](../Category/新會人.md "wikilink")

1.  [譚美琼簡介](https://upload.cc/i/U72Lo8.jpg)，[《棋廿三》場刊](http://www.theatrespace.org/document/201405_checkmatesP.pdf)(2015年)，第27頁。
2.  [棋廿三場刊](http://www.theatrespace.org/document/201405_checkmatesP.pdf)