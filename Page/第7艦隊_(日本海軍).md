<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")
<small>本條目是指日本帝國海軍的第七艦隊。關於[美國海軍現役中的相同番號部隊](../Page/美國海軍.md "wikilink")，請參見**[第七艦隊
(美國海軍)](../Page/第七艦隊_\(美國海軍\).md "wikilink")**。</small>

<div style="font-size: small;">

</div>

</div>

**第7艦隊**為[大日本帝國海軍之一艦隊](../Page/大日本帝國海軍.md "wikilink")。[太平洋戰爭末期時](../Page/太平洋戰爭.md "wikilink")，為了反擊[同盟國軍在各海域攻撃船舶輸送而使船隊癱瘓麻痺](../Page/同盟國_\(第二次世界大戰\).md "wikilink")；及確保西内海地域的[關門](../Page/關門海峽.md "wikilink")、[對馬](../Page/對馬海峽.md "wikilink")、[朝鮮等之海峽防衛與船舶安全輸送之故](../Page/朝鮮海峽.md "wikilink")，急遽編制成本艦隊。[艦隊](../Page/艦隊.md "wikilink")[司令部設置在](../Page/司令部.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink")[門司](../Page/門司區.md "wikilink")。

## 歷代司令長官

  - [岸福治中將](../Page/岸福治.md "wikilink")
    1945年([昭和](../Page/昭和.md "wikilink")20年)4月15日 -
    1945年(昭和20年)8月20日
  - [大森仙太郎中將](../Page/大森仙太郎.md "wikilink")
    1945年([昭和](../Page/昭和.md "wikilink")20年)8月20日 -
    1945年(昭和20年)9月15日

## 歷代參謀長

  - [後藤光太郎少將](../Page/後藤光太郎.md "wikilink")
    1945年([昭和](../Page/昭和.md "wikilink")20年)4月15日 -
    1945年(昭和20年)7月10日
  - [藤原喜代間少將](../Page/藤原喜代間.md "wikilink")
    1945年([昭和](../Page/昭和.md "wikilink")20年)7月10日 -
    1945年(昭和20年)9月15日

## 參考文獻

<div class="references-small">

1.  日本陸海軍の制度・組織・人事(日本近代史料研究会編・[東京大学出版会](../Page/東京大学出版会.md "wikilink"))

2.
3.

<references />

</div>

## 關連項目

  - [艦隊](../Page/艦隊.md "wikilink")
  - [第1艦隊](../Page/第1艦隊_\(日本海軍\).md "wikilink")
  - [軍事單位](../Page/軍事單位.md "wikilink")
  - [海軍航空部隊](../Page/海軍航空部隊.md "wikilink")
  - [大日本帝國海軍航空隊列表](../Page/大日本帝國海軍航空隊列表.md "wikilink")

## 外部連結

<div class="references-small">

  -
  -
  - [日本陸海軍事典](https://web.archive.org/web/20100516180653/http://homepage1.nifty.com/kitabatake/rikukaiguntop.html)

<references />

</div>

[category:大日本帝國海軍](../Page/category:大日本帝國海軍.md "wikilink")

[Category:日軍](../Category/日軍.md "wikilink")
[Category:大日本帝國海軍艦隊](../Category/大日本帝國海軍艦隊.md "wikilink")
[Category:太平洋戰爭](../Category/太平洋戰爭.md "wikilink")