在[塔羅牌中](../Page/塔羅牌.md "wikilink")，**錢幣牌組**是[小阿爾克那中的一個花色](../Page/小阿爾克那.md "wikilink")，又稱為**五角星牌組**。在西方四大元素中象徵**土**，對應通用[撲克牌中的](../Page/撲克牌.md "wikilink")**方塊**花色。正如其他塔羅牌組，錢幣牌組同樣包含了十四張牌：一（Ace）、從二到十的數字牌、隨從、騎士、王后以及國王。

這個牌組代表城市中的第三階級，也就是商人、藝術家以及一般居民。

## 占卜學上的含意

作占卜使用時，此牌組與西方[四大基本元素中的](../Page/元素.md "wikilink")**地**互相連結，具有身軀肉體、財產或財富上的意義。在拉丁紙牌中，金幣牌組代表封建制度下的商人及貿易家，因此延伸出諸如代表人生的、實際面上的物質、財運、和女性性格中較實際的一面。雖然跟[權杖一樣可以解釋為工作與勞力](../Page/權杖.md "wikilink")，但[權杖主要是注重生意的經營](../Page/權杖.md "wikilink")，而錢幣強調最後的收穫。相關的特徵以及人格特質包含黑髮、黑眼睛、以及健壯的體格。一般設想人格特徵為比較實際。

在[萊德偉特卡牌及其衍生卡牌裡](../Page/萊德偉特卡牌.md "wikilink")，此牌組被稱作**五角星牌組**，每張紙牌的一至數個金幣上都畫有五角星的圖樣。在[托特之書中](../Page/托特之書.md "wikilink")，此牌組被稱作**圓盤牌組**（the
suit of
discs），這些紙牌也分別與[黃道十二宮中土屬性的](../Page/黃道十二宮.md "wikilink")[金牛座](../Page/金牛座.md "wikilink")、[處女座以及](../Page/處女座.md "wikilink")[山羊座相關](../Page/山羊座.md "wikilink")，都是實際、腳踏實地的星座。

<center>

<file:Pents01.jpg>|[錢幣一](../Page/錢幣一.md "wikilink")
<file:Pents02.jpg>|[錢幣二](../Page/錢幣二.md "wikilink")
<file:Pents03.jpg>|[錢幣三](../Page/錢幣三.md "wikilink")
<file:Pents04.jpg>|[錢幣四](../Page/錢幣四.md "wikilink")
<file:Pents05.jpg>|[錢幣五](../Page/錢幣五.md "wikilink")
<file:Pents06.jpg>|[錢幣六](../Page/錢幣六.md "wikilink")
<file:Pents07.jpg>|[錢幣七](../Page/錢幣七.md "wikilink")
<file:Pents08.jpg>|[錢幣八](../Page/錢幣八.md "wikilink")
<file:Pents09.jpg>|[錢幣九](../Page/錢幣九.md "wikilink")
<file:Pents10.jpg>|[錢幣十](../Page/錢幣十.md "wikilink")
<file:Pents11.jpg>|[錢幣隨從](../Page/錢幣隨從.md "wikilink")
<file:Pents12.jpg>|[錢幣騎士](../Page/錢幣騎士.md "wikilink")
<file:Pents13.jpg>|[錢幣王后](../Page/錢幣王后.md "wikilink")
<file:Pents14.jpg>|[錢幣國王](../Page/錢幣國王.md "wikilink")

</center>

[S](../Page/category:占卜.md "wikilink")
[P](../Page/category:塔羅牌.md "wikilink")