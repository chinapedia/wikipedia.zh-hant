**紋別市**（）為位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[鄂霍次克綜合振興局北部的](../Page/鄂霍次克綜合振興局.md "wikilink")[城市](../Page/城市.md "wikilink")，北側面[鄂霍次克海](../Page/鄂霍次克海.md "wikilink")，冬季時岸邊會有流冰，南部為台地。名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「mo-pet」，意思為安靜的河。

由於過去在[日高支廳的](../Page/日高支廳.md "wikilink")[門別町](../Page/門別町.md "wikilink")（現已[合併為](../Page/市町村合併.md "wikilink")[日高町](../Page/日高町_\(北海道\).md "wikilink")）日文發音同樣為，因此紋別市習慣上以「鄂霍次克紋別」或「北見紋別」的稱呼來區別。

## 歷史

  - 1880年：設置紋別村外9村戶長役場
  - 1897年：湧別村（現在的[湧別町](../Page/湧別町.md "wikilink")）從紋別村外9村戶長役場分離。
  - 1900年：渚滑村從紋別村外9村戶長役場分離。
  - 1906年4月1日：紋別郡湧別村、渚滑村成立為北海道二級村。\[1\]
  - 1909年4月1日：紋別町、藻鼈村合併為紋別村，並成為北海道二級村。
  - 1918年4月1日：瀧上村（現在的[瀧上町](../Page/瀧上町.md "wikilink")）從渚滑村分割獨立設置為北海道二級村。
  - 1919年5月1日：紋別村改制為紋別町。
  - 1921年4月1日：紋別町成為北海道一級町。
  - 1932年6月1日：下渚滑村從渚滑村分割獨立設置為北海道二級村。
  - 1937年11月1日：渚滑村改名為[上渚滑村](../Page/上渚滑村.md "wikilink")。
  - 1940年1月1日：下渚滑村改名為[渚滑村](../Page/渚滑村.md "wikilink")。
  - 1954年7月1日：紋別町、上渚滑村和渚滑村[合併為紋別市](../Page/市町村合併.md "wikilink")。

## 產業

以農業、奶酪畜牧業、林業、漁業和觀光旅遊業為主，過去曾有金和水銀的礦產，但因已無礦脈，現已停止開採。

## 交通

### 機場

  - [紋別機場](../Page/紋別機場.md "wikilink")

### 港口

  - [紋別港](../Page/紋別港.md "wikilink")（重要港灣）

### 鐵路

過去曾有舊日本國有鐵道[渚滑線和](../Page/渚滑線.md "wikilink")[北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")[名寄本線通過](../Page/名寄本線.md "wikilink")，但已先後於1985年4月1日及1989年5月1日停駛。目前距離最近的車站是50[公里外的北海道旅客鐵道](../Page/公里.md "wikilink")[石北本線位於](../Page/石北本線.md "wikilink")[遠輕町的](../Page/遠輕町.md "wikilink")[遠輕車站](../Page/遠輕站.md "wikilink"))。

  - [渚滑線](../Page/渚滑線.md "wikilink")：[渚滑車站](../Page/渚滑車站.md "wikilink")
    - [元西仮臨時站](../Page/元西仮臨時站.md "wikilink") -
    [下渚滑車站](../Page/下渚滑車站.md "wikilink") -
    [十六號線仮臨時站](../Page/十六號線仮臨時站.md "wikilink") -
    [中渚滑車站](../Page/中渚滑車站.md "wikilink") -
    [上東仮臨時站](../Page/上東仮臨時站.md "wikilink") -
    [上渚滑車站](../Page/上渚滑車站.md "wikilink") -
    [奧東仮臨時站](../Page/奧東仮臨時站.md "wikilink") - -
    [瀧下車站](../Page/瀧下車站.md "wikilink") -
    [濁川車站](../Page/濁川車站.md "wikilink") -
    [北見瀧之上車站](../Page/北見瀧之上車站.md "wikilink")
  - [名寄本線](../Page/名寄本線.md "wikilink")：渚滑車站 -
    [潮見町車站](../Page/潮見町車站.md "wikilink") -
    [紋別車站](../Page/紋別車站.md "wikilink") -
    [元紋別車站](../Page/元紋別車站.md "wikilink") -
    [一本松車站](../Page/一本松車站.md "wikilink") -
    [小向車站](../Page/小向車站.md "wikilink") -
    [弘道車站](../Page/弘道車站.md "wikilink") -
    [沼之上車站](../Page/沼之上車站.md "wikilink")

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速國道</dt>

</dl>
<ul>
<li><a href="../Page/旭川紋別自動車道.md" title="wikilink">旭川紋別自動車道</a>（計畫興建）</li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道238號.md" title="wikilink">國道238號</a></li>
<li><a href="../Page/國道239號.md" title="wikilink">國道239號</a></li>
<li><a href="../Page/國道273號.md" title="wikilink">國道273號</a></li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道137號遠輕雄武線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道304號紋別港線</li>
<li>北海道道305號紋別丸瀨布線</li>
<li>北海道道306號丸瀨布上渚滑線</li>
<li>北海道道553號上藻別上渚滑停車場線</li>
<li>北海道道713號中渚滑紋別停車場線</li>
<li>北海道道766號和訓邊渚滑停車場線</li>
<li>北海道道804號和訓邊上渚滑線</li>
<li>北海道道873號小向元紋別線</li>
<li>北海道道932號上渚滑原野上渚滑線</li>
<li>北海道道1055號紋別興部線</li>
<li>北海道道1151號新紋別機場線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="休閒">休閒</h3>
<ul>
<li><a href="http://mombetsu.jp/kanko/point/komuke2.html">Komuke國際露營場</a></li>
<li><a href="http://mombetsu.jp/kanko/point/ooyama.html">市營大山滑雪場</a></li>
<li><a href="http://mombetsu.jp/kanko/point/teien.html">鄂霍次克庭園</a></li>
<li><a href="http://mombetsu.jp/kanko/point/shinrin.html">森林公園</a></li>
<li><a href="http://mombetsu.jp/kanko/point/skytower.html">大山山頂園</a></li>
</ul>
<h3 id="觀光">觀光</h3>
<ul>
<li><a href="../Page/流冰.md" title="wikilink">流冰</a> <a href="http://www.o-tower.co.jp/garinko/">流冰破冰船Garinko號2</a>（<a href="../Page/北海道遺產.md" title="wikilink">北海道遺產</a>）</li>
<li><a href="https://web.archive.org/web/20070426143706/http://www.o-tower.co.jp/towerframe.html">鄂霍次克塔</a></li>
<li><a href="http://giza-ryuhyo.com/">北海道立鄂霍次克流冰科學中心</a></li>
<li><a href="http://mombetsu.jp/kanko/point/tokkari.html">鄂霍次克日本雁中心</a></li>
<li><a href="https://web.archive.org/web/20070815022227/http://mombetsu.jp/syoukai/stair/index.html">鄂霍次克健康游泳池「ステア」</a></li>
<li><a href="http://mombetsu.jp/kanko/point/mombetsu.html">紋別公園</a></li>
<li><a href="https://web.archive.org/web/20070222042311/http://mombetsu.jp/syoukai/hakubutukan/index.html">市立博物館</a></li>
<li><a href="http://mombetsu.jp/kanko/point/omusaro_3.html">Omusaro遺跡公園</a></li>
<li><a href="http://mombetsu.jp/kanko/point/omusaro.html">Omusaro原生花園</a></li>
<li><a href="http://mombetsu.jp/kanko/point/skytower.html">鄂霍次克天空塔</a></li>
<li><a href="http://mombetsu.jp/syoukai/kouikikouen/">鄂霍次克流冰公園</a>（興建中）</li>
</ul></td>
<td><h3 id="文化遺產"><a href="../Page/文化遺產.md" title="wikilink">文化遺產</a></h3>
<ul>
<li><p>（北海道指定史跡）</p></li>
</ul>
<h3 id="慶典">慶典</h3>
<ul>
<li>紋別觀光港節（7月下旬）</li>
<li>紋別美食家節（10月中旬）</li>
<li>紋別流冰節（2月中旬）</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 大學

  - 北海道大學低溫科學研究所 環鄂霍次克觀測研究中心分室

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>道立北海道紋別高等學校</li>
</ul></td>
<td><ul>
<li>道立北海道紋別北高等学校（已停止招生，預計於2009年3月31日廢校）</li>
<li>道立北海道紋別南高等学校（已於2007年3月31日廢校）</li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>紋別市立紋別中學校</li>
<li>紋別市立潮見中學校</li>
</ul></td>
<td><ul>
<li>紋別市立渚滑中學校</li>
<li>紋別市立上渚滑中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>紋別市立紋別小學校</li>
<li>紋別市立潮見小學校</li>
<li>紋別市立南丘小學校</li>
<li>紋別市立元紋別小學校</li>
</ul></td>
<td><ul>
<li>紋別市立小向小學校</li>
<li>紋別市立沼之上小學校</li>
<li>紋別市立渚滑小學校</li>
<li>紋別市立上渚滑小學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 特殊學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>北海道紋別養護學校</li>
</ul></td>
<td><ul>
<li>北海道紋別高等養護學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 海外

  - [紐波特](../Page/紐波特_\(奧勒岡州\).md "wikilink")（[美國](../Page/美國.md "wikilink")
    [奧勒岡州](../Page/奧勒岡州.md "wikilink")）：締結於1966年4月8日。 \[2\]

  - [費爾班克斯](../Page/費爾班克斯.md "wikilink")（美國
    [阿拉斯加州](../Page/阿拉斯加州.md "wikilink")）：締結於1991年2月8日。

  - [科爾薩科夫](../Page/科爾薩科夫.md "wikilink")（[俄羅斯](../Page/俄羅斯.md "wikilink")
    [薩哈林州](../Page/薩哈林州.md "wikilink")）：為過去日本[樺太廳](../Page/樺太廳.md "wikilink")[豐原支廳](../Page/豐原支廳.md "wikilink")[大泊郡](../Page/大泊郡.md "wikilink")[大泊町](../Page/大泊町.md "wikilink")，締結於1991年1月12日。

## 本地出身的名人

  - [勝浦修](../Page/勝浦修.md "wikilink")：[將棋選手](../Page/將棋.md "wikilink")

  - [千葉貴仁](../Page/千葉貴仁.md "wikilink")：[足球選手](../Page/足球.md "wikilink")

  - [長谷川初範](../Page/長谷川初範.md "wikilink")：[演員](../Page/演員.md "wikilink")

  - [本庄陸男](../Page/本庄陸男.md "wikilink")：[作家](../Page/作家.md "wikilink")

  - ：[漫畫家](../Page/漫畫家.md "wikilink")

  - [柿あべさより崎正澄](../Page/柿あべさより崎正澄.md "wikilink")：漫畫家

## 參考資料

## 外部連結

  - [紋別觀光協會](https://web.archive.org/web/20070405145141/http://www1.ocn.ne.jp/%7Emonkan/)

  - [紋別商工會議所](http://www.mon-cci.or.jp/)

  - [鄂霍次克紋別攝影協會](http://omfc.jp/)

<!-- end list -->

1.
2.