**契丹人**，古代[游牧民族](../Page/游牧.md "wikilink")，居住在今[蒙古国及](../Page/蒙古国.md "wikilink")[中国东北地區](../Page/中国东北.md "wikilink")，採取半農半牧生活，語言屬於[阿爾泰語系](../Page/阿爾泰語系.md "wikilink")[蒙古語族](../Page/蒙古語族.md "wikilink")，但受到[通古斯語族的強烈影響](../Page/通古斯語族.md "wikilink")。而目前居住[中國東北的](../Page/中國東北.md "wikilink")[達斡爾族可認定為契丹人直系後裔](../Page/達斡爾族.md "wikilink")。

古契丹有八部：悉萬丹部、何大何部、伏弗鬱部、羽陵部、日連部、匹絜部、黎部、吐六-{於}-部。[涅里是遼的始祖](../Page/耶律涅里.md "wikilink")。契丹人有三年選一次[夷里堇的習慣](../Page/夷里堇.md "wikilink")，早期[夷里堇都在](../Page/夷里堇.md "wikilink")[大贺氏家族中产生](../Page/大贺氏.md "wikilink")，中期在[遥辇氏中产生](../Page/遥辇氏.md "wikilink")，最后由契丹[迭剌部耶律氏族的夷里堇耶律阿保机破坏了传统的贵族选举制度](../Page/迭剌部.md "wikilink")，仿效汉人，建立起世袭的中央集权专制的契丹。\[1\]

据[遼朝大臣](../Page/遼朝.md "wikilink")[耶律儼](../Page/耶律儼.md "wikilink")《[皇朝實錄](../Page/皇朝實錄.md "wikilink")》所稱契丹為[黃帝之後](../Page/黄帝.md "wikilink")。《[遼史](../Page/遼史.md "wikilink")·太祖紀贊》和《世表序》主張契丹為[炎帝之後](../Page/炎帝.md "wikilink")。近年在[雲南發現的契丹遺裔](../Page/雲南.md "wikilink")，保存有一部修於[明代的](../Page/明代.md "wikilink")《[施甸長官司族譜](../Page/施甸長官司族譜.md "wikilink")》，卷首附一首七言詩，詩曰：“遼之先祖始炎帝……”。這些契丹人認可契丹為炎帝苗裔的說法。[回紇人亡國時](../Page/回紇.md "wikilink")，大批回紇人逃入契丹，因此有契丹半回紇的說法，有些回紇人溶入契丹人的[蕭氏](../Page/契丹萧氏.md "wikilink")，[遼太祖](../Page/遼太祖.md "wikilink")[耶律阿保機的皇后](../Page/耶律阿保機.md "wikilink")[述律平就是](../Page/述律平.md "wikilink")[回紇人](../Page/回紇.md "wikilink")，[耶律德光后將](../Page/耶律德光.md "wikilink")[述律氏賜姓蕭氏](../Page/述律氏.md "wikilink")，蕭氏後來融入女真，改為舒穆祿氏，遼朝滅亡后，多融入汉族、蒙古族與滿族。

《辽史·后妃传》记载：“太祖慕汉高皇帝，故耶律氏兼称刘氏；以乙室、拔里比萧相国，遂为萧氏”。《辽史·国语解》记载：“耶律和萧两个姓，以汉字书者曰耶律、萧，以契丹字书者曰移刺、石抹”。《金史·国语解》记载：“移喇曰刘，石抹曰萧”。

## 词源

[Rest_Stop_for_the_Khan_part1.png](https://zh.wikipedia.org/wiki/File:Rest_Stop_for_the_Khan_part1.png "fig:Rest_Stop_for_the_Khan_part1.png")[胡瓌所繪](../Page/胡瓌.md "wikilink")《[卓歇图](../Page/卓歇图.md "wikilink")》（局部），敘述契丹族的可汗、阏氏和他部下於出猎后圍地饮宴的情况。\]\]
[Rest_Stop_for_the_Khan_part2.png](https://zh.wikipedia.org/wiki/File:Rest_Stop_for_the_Khan_part2.png "fig:Rest_Stop_for_the_Khan_part2.png")
[MongolHuntersSong.jpg](https://zh.wikipedia.org/wiki/File:MongolHuntersSong.jpg "fig:MongolHuntersSong.jpg")
“契丹”一词最早见于《[魏书](../Page/魏书.md "wikilink")·契丹传》，但對於契丹這個名稱的來源，學者還未達成一致意見。趙振績在《契丹源流考》中認為，契丹之名起源於[鮮卑](../Page/鮮卑.md "wikilink")，為居於鮮卑之人的意思。

《[金史](../Page/金史.md "wikilink")·太祖本纪》记载“契丹”是镔铁之意，日本人白鸟库吉认为“契丹”与[通古斯语族和](../Page/通古斯语族.md "wikilink")[蒙古语族中的](../Page/蒙古语族.md "wikilink")“小刀”（*K'-ɔtɔ*,''
gɔtɔgɔ''）一词类似，“契丹”可能是刀剑之意\[2\]。这种说法也是史学界最为认可的说法\[3\]。但是有学者认为[契丹语称铁为](../Page/契丹语.md "wikilink")“曷术”，从发音上与“契丹”或者通古斯语族和蒙古语族中的“小刀”一词发音相差很大，而且“契丹”二字各为一词\[4\]。

另一种观点认为“契丹”的“契”可能与契丹始祖[奇首可汗有关](../Page/奇首可汗.md "wikilink")；“丹”相当于[西域语中的](../Page/西域语.md "wikilink")“斯坦”，表示所在地之意。“契丹”也就是“奇首可汗的领地”的意思。但是反对此说者认为西域语和契丹语不同，“丹”和“斯坦”的发音不一样；而“契”与“奇”古音义不同，“奇首”很难简化为“契”\[5\]。

也有认为“契丹”来自于部落酋长的名号，契丹人中常见的一些姓氏如悉独官，逸豆归等去掉词尾读音与[突厥语的](../Page/突厥语.md "wikilink")“契丹”（x'itai）和[蒙古语中](../Page/蒙古语.md "wikilink")“契丹”的读音乞塔、契塔特相近，而语尾的“归”有尊称长者之意。反对者则指出“归”在鲜卑语中是[形容词的构词成分](../Page/形容词.md "wikilink")，表示弱意，读作xui或gui，并无长老之意。悉独官，逸豆归是一个词，而契丹则是两个词。反对者提出，“契丹”是k'eiduan的音译，意为“大中”，鲜卑当年分为中东西三部，晋朝以后，“段”部落统治中部地区。“段”的读音与k'eiduan中的duan吻合，意思为“中”。在“段”前冠以“大”字，而称“契丹”\[6\]。

日本學者[愛宕松男认为](../Page/愛宕松男.md "wikilink")“契”与“奚”相近，“丹”即“东”，契丹在[奚的东面](../Page/奚.md "wikilink")，所以叫做“契丹”，也就是“奚东”之意。《[魏书](../Page/魏书.md "wikilink")·契丹传》中所称“契丹在库莫奚以东”，“库莫奚”即蒙古语“沙子”之意，因此“奚东”就是沙子以东之意\[7\]。也有人推测契丹人所居之地为[辽河地区](../Page/辽河.md "wikilink")，而《契丹国志·族姓原始》中说契丹“以本无姓氏，惟各以所居地名呼之”，因此“契丹”可能是“水草丰美”之意\[8\]。

## 民族起源

学术界考证契丹的起源，有30余种说法。《[晋书](../Page/晋书.md "wikilink")》称契丹为[轲比能之后](../Page/轲比能.md "wikilink")。《[契丹国志](../Page/契丹国志.md "wikilink")》引用白马青牛传说，指契丹为白马青牛仙人之后，发源于木叶山；更早以前的传说，又指契丹始祖为[奇首可汗](../Page/奇首可汗.md "wikilink")，发源于都庵山。\[9\]
后来的元人修订《辽史》时，将两则不同的传说混合一谈。

契丹早期历史以神话传说为主。一种说法契丹源于[東胡後裔](../Page/東胡.md "wikilink")[鮮卑的](../Page/鮮卑.md "wikilink")[柔然部](../Page/柔然.md "wikilink")。柔然部戰敗於鮮卑[拓跋氏的](../Page/拓跋氏.md "wikilink")[北魏](../Page/北魏.md "wikilink")。其中北柔然退到[外興安嶺一帶](../Page/外興安嶺.md "wikilink")，成為[蒙古人的祖先](../Page/蒙古族.md "wikilink")[室韋](../Page/室韋.md "wikilink")。而南柔然避居今[內蒙古的](../Page/內蒙古.md "wikilink")[西喇木倫河以南](../Page/西喇木倫河.md "wikilink")、[老哈河以北地區](../Page/老哈河.md "wikilink")，以聚族分部的組織形式過著遊牧和漁獵的氏族社會生活。另一個說法則認為契丹源自[宇文部鮮卑](../Page/宇文部鮮卑.md "wikilink")。

契丹以原意為[鑌鐵的](../Page/鑌鐵.md "wikilink")“契丹”一詞作為民族稱號，來象徵契丹人頑強的意志和堅不可摧的民族精神。他們是[阿爾泰語系](../Page/阿爾泰語系.md "wikilink")[蒙古語族](../Page/蒙古語族.md "wikilink")，但受到[通古斯語族的強烈影響](../Page/通古斯語族.md "wikilink")。歷史文獻最早記載契丹族開始於405年，553年的[北齊時期](../Page/北齊.md "wikilink")，[高洋曾大破契丹](../Page/高洋.md "wikilink")。\[10\]另一说他们与庫莫奚是[宇文部](../Page/宇文部.md "wikilink")。

## 民族文化

契丹人的衣食住行、風俗習慣與[漢人不同](../Page/漢人.md "wikilink")。契丹人[髡发](../Page/髡发.md "wikilink")，服裝通常為長袍[左衽](../Page/左衽.md "wikilink")，圓領窄袖，褲腳放靴筒內。女人袍內著裙，亦穿長筒皮靴。腰外系蹀躞帶，上掛金玉、[水晶和](../Page/水晶.md "wikilink")[琥珀等飾件](../Page/琥珀.md "wikilink")，還有隨時可用的火石、筷子、餐具刀等。契丹人無論男女皆髡髮。男子在兩鬢各留一綹頭髮，別處的頭髮全剃光。婦女僅剃沿前額邊的頭髮。

契丹人住所為氈帳，皇帝的御帳稱為捺缽。辽朝契丹人只有皇族帐和-{后}-族帐的人才有姓，两帐之外的其他契丹人是没有姓的。契丹贵族只有“[耶律](../Page/耶律姓.md "wikilink")”和“[蕭](../Page/蕭姓.md "wikilink")”兩個姓氏，相傳[耶律阿保機本人羨慕](../Page/耶律阿保機.md "wikilink")[蕭何輔助](../Page/蕭何.md "wikilink")[劉邦的典故](../Page/劉邦.md "wikilink")，將-{拔芮氏}-、乙室氏賜姓蕭氏。[耶律德光將](../Page/耶律德光.md "wikilink")[述律氏賜姓蕭氏](../Page/述律氏.md "wikilink")。此外依附契丹的[奚也姓萧](../Page/奚.md "wikilink")。

“耶律”和“蕭”二姓互為婚姻，婚俗特點是[同姓不婚](../Page/同姓不婚.md "wikilink")，娶親不論輩分，異姓[近親结婚](../Page/近親婚姻.md "wikilink")、[離婚和再嫁均不受限制](../Page/離婚.md "wikilink")。早期尚有[姐亡妹續](../Page/妻姊妹婚.md "wikilink")，[近親结婚的習慣](../Page/近親结婚.md "wikilink")。

契丹[貴族盛行厚葬](../Page/貴族.md "wikilink")，[遼](../Page/辽朝.md "wikilink")[陳國公主與駙馬墓是迄今為止發現的保存最完整](../Page/陳國公主與駙馬墓.md "wikilink")、出土文物最豐富的契丹大貴族墓葬，出土的兩套有金花銀枕、[鎏金銀冠](../Page/鎏金.md "wikilink")、金面具、銀絲網絡、金花銀靴組成的殯葬服飾，完整的體現了貴族的殯葬習俗。[耶律羽之墓室豪華富麗](../Page/耶律羽之.md "wikilink")，天下奇珍瑰寶盡匯其中。

他们早期游牧漁猎为生，阿保机时代开始務农。但遼代皇帝仍有四时[捺钵的傳统](../Page/捺钵.md "wikilink")。

## 後裔

契丹人大多融入其他中國北方的民族，如[漢族](../Page/漢族.md "wikilink")、[蒙古族](../Page/蒙古族.md "wikilink")、[滿族等](../Page/滿族.md "wikilink")。在中亞，哈薩克人的[中玉茲](../Page/中玉茲.md "wikilink")[乃蠻部有契丹遺民](../Page/乃蠻部.md "wikilink")，[吉爾吉斯人也有契丹](../Page/吉爾吉斯人.md "wikilink")（哈剌契丹）部落；在[北高加索的](../Page/北高加索.md "wikilink")[諾蓋人和](../Page/諾蓋人.md "wikilink")[烏拉爾山一帶的](../Page/烏拉爾山.md "wikilink")[巴什基爾人中也融入了契丹人](../Page/巴什基爾人.md "wikilink")，在[欽察部中](../Page/欽察.md "wikilink")**契丹欽察**與**托里欽察**是契丹與當地人的後裔。

據DNA鑑定，目前中國的[達斡爾族很可能是契丹](../Page/達斡爾族.md "wikilink")[大賀氏的後裔](../Page/大賀氏.md "wikilink")。另外在中國雲南被稱為“[本人](../Page/本人.md "wikilink")”的族群，根據家譜記載，是隨[蒙古軍隊出征而留在當地的契丹後裔](../Page/蒙古.md "wikilink")。\[11\]成吉思汗曾經把一批契丹人趕入高麗，朝鮮半島北部曾經叫契丹場。[乌梁海有部分人來自契丹](../Page/乌梁海.md "wikilink")。而現代DNA通過分析古代契丹女屍與現代[達斡爾](../Page/達斡爾.md "wikilink")、鄂溫克、蒙古族和漢族等人群的血樣比較後，得出達斡爾族與契丹有最接近的遺傳關係，為契丹人後裔。

## 契丹作為中國的別稱

由于[金帐汗国自](../Page/金帐汗国.md "wikilink")13世纪至15世纪长期是欧洲的霸主，而蒙古人称中国北方为契丹，后该词泛指中国，欧洲人深感金帐汗国的强大，所以在此期间兴起的[斯拉夫语族和](../Page/斯拉夫语族.md "wikilink")[突厥语族诸民族均以契丹为中原政权的代名词](../Page/突厥语族.md "wikilink")。现在仍有十几个国家将中国称为“契丹”：[斯拉夫语国家](../Page/斯拉夫语.md "wikilink")（[俄罗斯](../Page/俄罗斯.md "wikilink")、[乌克兰](../Page/乌克兰.md "wikilink")、[保加利亚等](../Page/保加利亚.md "wikilink")）称中国为“kitai”；[突厥语国家](../Page/突厥语.md "wikilink")（[中亚各国](../Page/中亚.md "wikilink")）称中国为“Kaitay”、“Kathay”、“Hatay”、“Katay”；

在[俄語](../Page/俄語.md "wikilink")、古[葡萄牙語](../Page/葡萄牙語.md "wikilink")、古[西班牙語](../Page/西班牙語.md "wikilink")，以及[中古英語中](../Page/中古英語.md "wikilink")，整個中國均被稱為“契丹”。在俄語中，中國的發音是“”。[英語也有用](../Page/英語.md "wikilink")“”的雅稱來表示中國的情況，如“”（[國泰航空](../Page/國泰航空.md "wikilink")，“中國太平洋航空”），而葡萄牙語（）和西班牙語（）亦然。突厥人說契丹時，不論漢人還是契丹，一律使用克塔伊一字。

[清代震鈞所著的](../Page/清代.md "wikilink")《[天咫偶聞](../Page/天咫偶聞.md "wikilink")》【卷二•南城】中引述乾隆年間蒙族文人博明的《西斎偶得》：“蒙古呼中國爲‘契塔特’，西洋呼中國爲‘吉代’，皆‘契丹’之轉音也”。\[12\]

## [松漠都督府都督](../Page/松漠都督府.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>汗号</p></th>
<th><p>姓名</p></th>
<th><p>在位時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大贺咄罗.md" title="wikilink">大贺咄罗</a></p></td>
<td><p>619年—623年以后</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/大贺摩会.md" title="wikilink">大贺摩会</a></p></td>
<td><p>628年左右</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/唐朝.md" title="wikilink">唐朝</a> <a href="../Page/松漠都督府.md" title="wikilink">松漠都督府都督</a></strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/大贺窟哥.md" title="wikilink">大贺窟哥</a></p></td>
<td><p>648年－656年左右</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大贺阿卜固.md" title="wikilink">大贺阿卜固</a></p></td>
<td><p>？—660年</p></td>
</tr>
<tr class="even">
<td><p>无上可汗</p></td>
<td><p><a href="../Page/李尽忠.md" title="wikilink">李尽忠</a><br />
（李尽灭）</p></td>
<td><p>660年－696年</p></td>
</tr>
<tr class="odd">
<td><p>松漠郡王</p></td>
<td><p><a href="../Page/孙万荣.md" title="wikilink">孙万荣</a><br />
（孙万斩）</p></td>
<td><p>696年—697年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李失活.md" title="wikilink">李失活</a></p></td>
<td><p>？—718年</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/李娑固.md" title="wikilink">李娑固</a></p></td>
<td><p>718年—720年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李郁干.md" title="wikilink">李郁干</a></p></td>
<td><p>720年—723年</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/李咄干.md" title="wikilink">李咄干</a></p></td>
<td><p>723年—725年</p></td>
</tr>
<tr class="even">
<td><p>广化郡王</p></td>
<td><p><a href="../Page/李邵固.md" title="wikilink">李邵固</a></p></td>
<td><p>725年—730年</p></td>
</tr>
<tr class="odd">
<td><p>洼可汗</p></td>
<td><p><a href="../Page/遥辇屈列.md" title="wikilink">遥辇屈列</a></p></td>
<td><p>730年—734年</p></td>
</tr>
<tr class="even">
<td><p>北平郡王</p></td>
<td><p><a href="../Page/李过折.md" title="wikilink">李过折</a></p></td>
<td><p>734年—735年</p></td>
</tr>
<tr class="odd">
<td><p>阻午可汗</p></td>
<td><p>遥辇俎里<br />
（<a href="../Page/李怀秀.md" title="wikilink">李怀秀</a>）</p></td>
<td><p>735年—745年以后</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡剌可汗.md" title="wikilink">胡剌可汗</a></p></td>
<td><p>遥辇楷落</p></td>
<td><p>746年—755年以后</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/苏可汗.md" title="wikilink">苏可汗</a></p></td>
<td></td>
<td><p>8世纪末</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鲜质可汗.md" title="wikilink">鲜质可汗</a></p></td>
<td><p>遥辇习之</p></td>
<td><p>9世纪初</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/昭古可汗.md" title="wikilink">昭古可汗</a></p></td>
<td></td>
<td><p>830年代左右</p></td>
</tr>
<tr class="even">
<td><p>耶澜可汗</p></td>
<td><p><a href="../Page/遥辇屈戌.md" title="wikilink">遥辇屈戌</a></p></td>
<td><p>840年代左右</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴剌可汗.md" title="wikilink">巴剌可汗</a></p></td>
<td><p><a href="../Page/巴剌可汗.md" title="wikilink">遥辇習爾之</a></p></td>
<td><p>860年代左右</p></td>
</tr>
<tr class="even">
<td><p>痕德廑可汗</p></td>
<td><p><a href="../Page/遥辇钦德.md" title="wikilink">遥辇钦德</a></p></td>
<td><p>？－906年</p></td>
</tr>
</tbody>
</table>

## 辽朝（契丹）君主列表

| 契丹與遼 907年－1125年                  |
| -------------------------------- |
| 廟號                               |
| [遼太祖](../Page/遼太祖.md "wikilink") |
| －                                |
| [遼太宗](../Page/遼太宗.md "wikilink") |
| [辽世宗](../Page/辽世宗.md "wikilink") |
| [遼穆宗](../Page/遼穆宗.md "wikilink") |
| [遼景宗](../Page/遼景宗.md "wikilink") |
| [遼聖宗](../Page/遼聖宗.md "wikilink") |
| [遼興宗](../Page/遼興宗.md "wikilink") |
| [遼道宗](../Page/遼道宗.md "wikilink") |
| －                                |
| 北遼與西北遼 1122年－1123年               |
| 廟號                               |
| [遼宣宗](../Page/遼宣宗.md "wikilink") |
| －                                |
| －                                |
|                                  |
| 西遼 1132年－1218年                   |
| 廟號                               |
| [遼德宗](../Page/遼德宗.md "wikilink") |
| －                                |
| [遼仁宗](../Page/遼仁宗.md "wikilink") |
| －                                |
| －                                |
| －                                |

## 参考文献

## 研究書目

  - 島田正郎 著，何天明 译：《大契丹國：遼代社會史研究》（呼和浩特：內蒙古人民出版社，2007）。
  - 傅樂煥 著：《遼史叢考》（北京：中華書局，1984）。
  - 愛宕松男 著，邢復禮 譯：《契丹古代史研究》（呼和浩特：內蒙古人民出版社，1988）。
  - Thomas Barfield 著，袁劍 譯：《危險的邊疆：游牧帝國與中國》（南京：江蘇人民出版社，2011）。
  - 森安孝夫：〈[从渤海到契丹——征服王朝的成立](http://www.nssd.org/articles/article_read.aspx?id=1002118830)〉。

## 參見

  - [馴鷹](../Page/馴鷹.md "wikilink")

[契丹](../Category/契丹.md "wikilink")
[Category:蒙古亲缘民族](../Category/蒙古亲缘民族.md "wikilink")

1.  《[遼史](../Page/遼史.md "wikilink")》卷三十二

2.

3.

4.

5.
6.
7.

8.

9.

10. 《[資治通鑑](../Page/資治通鑑.md "wikilink")》卷一百六十五
    [國學網_資治通鑒_卷第一百六十五](http://www.guoxue.com/shibu/zztj/content/zztj_165.htm)

11. [契丹人今在何方？](http://www.confucianism.com.cn/html/lishi/8199405.html)

12.