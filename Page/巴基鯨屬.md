**巴基鯨屬**（[学名](../Page/学名.md "wikilink")：），又名**巴基斯坦鯨**，已經絕種的[古鯨](../Page/古鯨.md "wikilink")，生活在陸地，形狀類似狼，屬於[鲸下目](../Page/鲸下目.md "wikilink")[巴基鲸科](../Page/巴基鲸科.md "wikilink")，是現代鯨魚的前身，生存於[始新世](../Page/始新世.md "wikilink")（5580-3390萬年前）的[巴基斯坦](../Page/巴基斯坦.md "wikilink")，故此得名。發現[化石的地層是](../Page/化石.md "wikilink")[特提斯洋的沿岸部份](../Page/特提斯洋.md "wikilink")。

首先發現的巴基鯨化石是一個長[頭顱骨](../Page/頭顱骨.md "wikilink")，最初被認為是屬於[中爪獸目](../Page/中爪獸目.md "wikilink")，但因其[內耳的特徵而確定是早期的鯨魚](../Page/內耳.md "wikilink")。\[1\]故此牠是已滅絕的陸上[哺乳動物及現代鯨魚的過渡物種](../Page/哺乳動物.md "wikilink")。

[Pakicetus_attocki.jpg](https://zh.wikipedia.org/wiki/File:Pakicetus_attocki.jpg "fig:Pakicetus_attocki.jpg")
巴基鯨的完整[骨骼於](../Page/骨骼.md "wikilink")2001年發現，顯示了牠們主要生活在陸地上。巴基鯨約有[狼的大小](../Page/狼.md "wikilink")，外觀很像中爪獸目的动物。\[2\]

## 参考文献

[Category:巴基鯨亞科](../Category/巴基鯨亞科.md "wikilink")
[Category:始新世哺乳類](../Category/始新世哺乳類.md "wikilink")

1.
2.