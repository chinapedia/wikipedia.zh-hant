**蘇芮**（），本名**蘇瑞芬**，[臺灣天后级女歌手](../Page/臺灣.md "wikilink")，生于[臺北市](../Page/臺北市.md "wikilink")，是华人乐坛的「里程碑」，音乐作品影响了几代人，是众多歌坛知名藝人共同崇拜和欣赏的歌手。

## 簡介

蘇芮生長於[臺北](../Page/臺北.md "wikilink")，父親是[山東人](../Page/山東.md "wikilink")，母親為[臺灣本島人](../Page/臺灣本島人.md "wikilink")，故通[閩南語](../Page/閩南語.md "wikilink")，早年為[西餐廳的駐唱歌手](../Page/西餐廳.md "wikilink")，多演唱[英文歌曲](../Page/英文.md "wikilink")，後進入歌壇，唱了許多膾炙人口的[國語](../Page/中華民國國語.md "wikilink")、[臺語歌曲](../Page/臺語.md "wikilink")，一度到香港發行了許多[粵語歌曲](../Page/粵語.md "wikilink")。

雖然蘇芮15歲即開始在民歌西餐廳駐唱,
但真正改變她一生的契機則是1982年,當紅的綜藝女王[崔苔菁重回](../Page/崔苔菁.md "wikilink")[台視主持](../Page/台視.md "wikilink")《夜來香》大型歌舞節目,首集力邀蘇芮於西洋歌曲單元演出，因而獲得電影導演[虞戡平的激賞並加盟](../Page/虞戡平.md "wikilink")[飛碟唱片](../Page/飛碟唱片.md "wikilink")。\[1\]

1983年因替電影《[搭錯車](../Page/搭錯車_\(電影\).md "wikilink")》錄製電影插曲,
一夜爆紅為家喻戶曉的實力派女歌星,\[2\]
從此便以一年兩張唱片的速度迅速地在台灣拓展知名度。

至今，蘇芮发行过30余张唱片专辑和精选集，曾斩获各项华语大奖，主要代表作品有《[奉献](../Page/奉献.md "wikilink")》、《[酒干倘卖无](../Page/酒干倘卖无.md "wikilink")》、《[花若離枝](../Page/花若離枝.md "wikilink")》、《[一样的月光](../Page/一样的月光.md "wikilink")》、《[亲爱的小孩](../Page/亲爱的小孩.md "wikilink")》、《[牵手](../Page/牵手.md "wikilink")》、《[跟着感觉走](../Page/跟着感觉走.md "wikilink")》與《[是否](../Page/是否.md "wikilink")》等，粵語經典作品為《[憑著愛](../Page/憑著愛.md "wikilink")》。

蘇芮是首位把[靈魂樂融入華語樂壇的人物](../Page/靈魂樂.md "wikilink")，她的歌聲具有特有的韌勁，加上其寬廣的音域，造成一種強烈滄桑感，給人有極其深刻的印象。這種憂鬱、悲傷的本質與[藍調音樂一脈相承](../Page/藍調音樂.md "wikilink")，在中文歌曲領域，蘇芮所創建的這種靈歌式演繹風格至今無人能夠超出。後輩歌手[張惠妹](../Page/張惠妹.md "wikilink")、[那英都視蘇芮為自己的偶像](../Page/那英.md "wikilink")。而張惠妹、那英、[张学友](../Page/张学友.md "wikilink")、[王菲](../Page/王菲.md "wikilink")、[孙燕姿](../Page/孙燕姿.md "wikilink")、[信](../Page/蘇見信.md "wikilink")、[迪克牛仔等著名歌手都曾翻唱过苏芮的作品](../Page/迪克牛仔.md "wikilink")。\[3\]

2018年6月23日台灣第29屆金曲獎蘇芮獲頒「特別貢獻獎」得主，並在睽違15年之後趁勢推出新專輯《感動我》。\[4\]
在接受《台灣名人堂》專訪時，蘇芮謙虛的表示：「這個獎不是我個人的，是我們大家一起得了這個獎，那個年代優秀的音樂人、樂手、編曲，我只是代表團隊來領這個獎。」

## 參與演唱會活動

  - 1984年9月29日 于香港红磡體育館 舉行個人演唱會1場。
  - 1984年10月20日 于台北中華體育館 舉行個人演唱會1場。
  - 1985年3月3日至3月10日 于新加坡Rex 舉行個人演唱會16場。
  - 1986年4月12、13日 于香港红磡體育館 舉行個人演唱會2場。
  - 1987年5月10日至24日 于美國、加拿大舉行個人巡迴演唱會共9場。
  - 1993年8月16日至8月21日 于馬來西亞Lidopalace \*舉行個人演唱會12場。
  - 1996年6月13日 於北京首都體育館 舉行個人演唱會2場。
  - 1999年5月22日 于新加坡Harbour Pavilion 舉行個人演唱會1場。
  - 2004年3月27日 于北京首都體育館 舉行牽手個人演唱會1場。
  - 2005年1月7日 于香港大球场参加“爱心无国界演艺界大汇演”，为南亚海啸受灾灾民筹款。
  - 2006年2月19日 于台北巨蛋体育馆参加“情声艺动
    相约东南”晚会，与[罗大佑](../Page/罗大佑.md "wikilink")、[周杰伦](../Page/周杰伦.md "wikilink")、[张惠妹等两岸艺人共同參加](../Page/张惠妹.md "wikilink")。与[潘玮柏合唱](../Page/潘玮柏.md "wikilink")《[我想更懂你](../Page/我想更懂你.md "wikilink")》。
  - 2006年6月24日 参加[成龙和他的朋友们南京慈善演唱会](../Page/成龙.md "wikilink")。
  - 2006年12月24、25日 于美国加州举行「完全动感 苏芮圣诞演唱会」2场。
  - 2007年5月13日，于上海举行母亲节「愿天下的母亲都美丽」个人演唱会1场。
  - 2007年30日，于马来西亚云顶举行「牵手2007」个人演唱会1场。
  - 2008年1月19、20日 于美国南加州华人工商大展举行「一样的月光」演唱会2场。
  - 2008年6月1日，于香港参加“演艺界512关爱行动”大汇演。
  - 2008年9月8日。参加“海上明月共潮生—2008年两岸四地迎中秋大型民族音乐会”。
  - 2008年9月26、27日，于澳门举行「牵手2008」个人演唱会2场。
  - 2009年2月8日，於印尼 Mikie Holiday Resort Hotel举行個人演唱会1场。
  - 2009年6月7日，于河南安阳举行群星演唱会，与周杰伦等歌手共同參加。
  - 2010年7月18日，于银川参加宁夏第二届国际文博会开幕式晚会演出，与周杰伦、[信等歌手共同參加](../Page/蘇見信.md "wikilink")。
  - 2010年9月29日，于辽宁参加[央视](../Page/央视.md "wikilink")《艺苑风景线》“情系辽阳”大型演唱会，与周杰伦、[宋祖英等艺人共同參加](../Page/宋祖英.md "wikilink")。
  - 2010年10月24日，于江苏淮安举办亚洲巨星演唱会，与周杰伦、[蔡依林](../Page/蔡依林.md "wikilink")、[SHE](../Page/SHE.md "wikilink")、[姜育恒等歌手共同參加](../Page/姜育恒.md "wikilink")。
  - 2010年12月18日，于武汉举办的“城市有爱
    爱在武汉——我们的80年代”大型公益演唱会。演唱曲目是《[酒干倘卖无](../Page/酒干倘卖无.md "wikilink")》、《[一样的月光](../Page/一样的月光.md "wikilink")》，并牵手解小东深情演绎《[请跟我来](../Page/请跟我来.md "wikilink")》。苏芮与罗大佑、[谭咏麟](../Page/谭咏麟.md "wikilink")、张惠妹、[韩红](../Page/韩红.md "wikilink")、[解小东等歌手共同參加](../Page/解小东.md "wikilink")。
  - 2011年1月19日，於河北省[沧州市参加](../Page/沧州市.md "wikilink")《放歌沧州》春节晚会录制压轴演出。
  - 2011年2月16日，於[燕郊国家高新区参加](../Page/燕郊国家高新区.md "wikilink")2011年元宵文艺盛典，与姜育恒等艺人共同參加。
  - 2011年3月26日，於[湖南省参加](../Page/湖南省.md "wikilink")“孝敬父母 走进郴州”音乐会。
  - 2011年4月9日，於臺北参加编曲大师[陈志遠追思会](../Page/陈志遠.md "wikilink")，献上一曲《[奉献](../Page/奉献.md "wikilink")》，用歌声聲緬懷好友。與[黃鶯鶯](../Page/黃鶯鶯.md "wikilink")、[張小燕](../Page/張小燕.md "wikilink")、[張清芳](../Page/張清芳.md "wikilink")、姜育恒等艺人共同參加。
  - 2011年5月20日，於[日月潭](../Page/日月潭.md "wikilink")[涵碧楼](../Page/涵碧楼.md "wikilink")“贵宾之夜”舉行個人演唱會。
  - 2013年12月31日，於馬來西亞雲頂雲星劇場，舉行跨年個人演唱會一場。
  - 2014年6月28日，蘇芮為第25屆金曲獎獻唱「一樣的月光」，追思特別貢獻獎得主、已故音樂人[彭國華](../Page/彭國華.md "wikilink")，這是電影《[搭錯車](../Page/搭錯車_\(電影\).md "wikilink")》原聲帶歌曲，也是讓蘇芮一炮而紅的首張專輯。\[5\]
  - 2018年10月20日，睽違已久的蘇芮，於台北三創園區舉辦「感動我My Favorite Your Memory音樂分享會」。\[6\]

## 家庭

  - 第一段婚姻，張天白，1979年-1980年。
  - 第二段婚姻，刘威麟，1985年2月-1999年3月 。

## 歌唱生涯

  - 蘇芮是以演唱英文歌曲起家，高中時候就先後加入Zero《空心人合唱團》及Action等合唱團，與[崔苔菁](../Page/崔苔菁.md "wikilink")、[黃鶯鶯及](../Page/黃鶯鶯.md "wikilink")[羅大佑等人皆受教於有](../Page/羅大佑.md "wikilink")「台灣第一代搖滾教父」稱號的[金祖齡](../Page/金祖齡.md "wikilink")。\[7\]
    蘇芮自述15歲登台時演唱的第一首歌曲是「Jackson 5」靈魂樂名作《Who's Lovin You》，\[8\]
    當時的她，不論是排練或是現場演出，都會隨身帶著錄音機，這個習慣延續至今都沒改變。蘇芮說:「唱歌不是跟別人比，而是跟自己比，時常聆聽自己的表現，才知道自己的缺點在哪裡。」

<!-- end list -->

  - 三十多歲才走紅，蘇芮可說是大器晚成，就在幾乎退出演唱生涯之際，1982年上了人稱台灣「一代妖姬」[崔苔菁在](../Page/崔苔菁.md "wikilink")[台視眾人極大注目的大型新歌舞節目](../Page/台視.md "wikilink")《夜來香》表演時，電影《搭錯車》導演[虞戡平驚覺其歌唱實力](../Page/虞戡平.md "wikilink")，當時苦於找不到適當歌手演唱其電影歌曲的[虞戡平重用了蘇芮](../Page/虞戡平.md "wikilink")，電影《搭錯車》歌曲的爆紅及加入了當時新成立的[飛碟唱片公司](../Page/飛碟唱片.md "wikilink")，真正的改變了蘇芮的一生並開啟了華語歌壇的一個新世代。\[9\]
    蘇芮自述她的第一張專輯就是[張小燕已過世的老公](../Page/張小燕.md "wikilink")[彭國華](../Page/彭國華.md "wikilink")（[豐華唱片的前大家長](../Page/豐華唱片.md "wikilink")）為她量身打造的，搭上電影《搭錯車》，創作黑色炫風時代，所以她始終跟著彭先生做音樂，她說：「我心中知道我是屬於豐華的歌手，所以彭先生過世後，我都沒有再出片，我想很難找到能為我量身打造的企畫，我最信任的人只有他。」。而為了配合「黑色炫風」的冷冷形象，要當個「遙不可及」的歌手，所以很少上電視，當時流行的商演、工地秀蘇芮都沒有唱過，以維持神秘感，她自嘲：「我根本不是大家以為的坦克車，而是停在坦克車上的蝴蝶啊！」。\[10\]

<!-- end list -->

  - 在2018年6月3日[台視播出的](../Page/台視.md "wikilink")《台灣名人堂》節目中，蘇芮提及1988年發行的專輯《跟著感覺走》由於到日本做後製且受當時日本音樂的影響，其長期黑色旋風的剛烈形象因《奉獻》一曲放入了更多的溫暖度，算是其歌唱生涯的一個轉型。

<!-- end list -->

  - 2009年錄製一首單曲《[玉蘭花](../Page/玉蘭花.md "wikilink")》，在線上發行，版稅捐公益，她默默做，完全沒宣傳，「這首歌，是我對歌壇最後的交代了。」她以這首歌獻給高齡母親，也希望自己像[玉蘭花](../Page/玉蘭花.md "wikilink")，雖不豔麗，淡淡留香。

<!-- end list -->

  - 2010年接受[中國時報訪問](../Page/中國時報.md "wikilink"):
    她不追名逐利，生活重心全是兒子，最大樂趣是燒一桌子的菜給兒子吃，當兒子問：「這是哪一家餐廳買的？」就是她最大的讚美。兒子今年考上交大，她就搬到新竹就近照顧兒子。不過，她太黏兒子，怕兒子也太依賴他，最近又搬回台北，「孩子大了，應該要獨立，我現在要學習放手。」多了自己的時間，是否再找個老伴？她說：「我失婚11年，沒有交過一個男朋友。」

<!-- end list -->

  - 2012年蘇芮透露：「因為我要照顧90多歲的老母親，很久不來上電視綜藝節目。」然而蘇芮擋不住歌迷的強烈呼喚，2012年終於再次登上舞台，參加[山東衛視歌聲傳奇節目](../Page/山東衛視.md "wikilink")。

<!-- end list -->

  - 2018年10月20日，睽違已久的蘇芮，慶祝出道35周年並配合新專輯《感動我》，於台北三創園區舉辦「感動我My Favorite
    Your
    Memory音樂分享會」，並接受現場觀眾的點歌演唱。這場音樂分享會與34年前的10月20日舉辦生平首場大型演唱會巧撞同天，蘇芮驚喜說：「我自己都忘了，沒有刻意安排，完全就是一個美麗的巧合。」她除獻唱新歌，還邀新生代歌手[謝嘉全合唱](../Page/謝嘉全.md "wikilink")「Jackson
    5」靈魂樂名作《Who's Lovin You》，以紀念自己這輩子首次登台演唱的第一首歌。\[11\]

## 代表作品

《[一樣的月光](../Page/一樣的月光.md "wikilink")》、《[酒矸倘賣無](../Page/酒矸倘賣無.md "wikilink")》、《[把握](../Page/把握.md "wikilink")》、《[是否](../Page/是否.md "wikilink")》、《[請跟我來](../Page/請跟我來.md "wikilink")》、《[變](../Page/變.md "wikilink")》、、《[是不是這樣](../Page/是不是這樣.md "wikilink")》、《[迷失](../Page/迷失.md "wikilink")》、《[未知](../Page/未知.md "wikilink")》、《[塵緣](../Page/塵緣.md "wikilink")》、《[你屬於你](../Page/你屬於你.md "wikilink")》、《[我曾深愛過](../Page/我曾深愛過.md "wikilink")》、《[錯](../Page/錯.md "wikilink")》、《[也算是奇蹟](../Page/也算是奇蹟.md "wikilink")》、《[Love
Potion
NO.9](../Page/Love_Potion_NO.9.md "wikilink")》、《[不回首](../Page/不回首.md "wikilink")》、《[明天還是要繼續](../Page/明天還是要繼續.md "wikilink")》、《[能輸多少](../Page/能輸多少.md "wikilink")》、《[因為你因為我](../Page/因為你因為我.md "wikilink")》、《[心痛的感覺](../Page/心痛的感覺.md "wikilink")》、《[親愛的小孩](../Page/親愛的小孩.md "wikilink")》、《[牽手](../Page/牽手.md "wikilink")》、《[跟著感覺走](../Page/跟著感覺走.md "wikilink")》、《[奉獻](../Page/奉獻.md "wikilink")》、《[再回首](../Page/再回首.md "wikilink")》、《[容顏](../Page/容顏.md "wikilink")》、《[花若離枝](../Page/花若離枝.md "wikilink")》。

粵語：《[憑著愛](../Page/憑著愛.md "wikilink")》、《[誰可相依](../Page/誰可相依.md "wikilink")》、《[休息工作再工作](../Page/休息工作再工作.md "wikilink")》。

## 主要成就

  - 1983年6月18日，《[搭错车](../Page/搭錯車_\(電影\).md "wikilink")》电影原声大碟（即《苏芮专辑》）发行。专辑打破唱片销量记录。“一样的月光”在台湾国语歌曲排行榜9周排行第一，为新成立的飞碟唱片公司打下半壁江山。
  - 1983年6月18日，《搭错车》电影原声大碟后来成为华语流行音乐史上里程碑式的作品，并荣获“台湾流行音乐百张最佳专辑”第2名，仅次于罗大佑的《之乎者也》。
  - 1983年6月18日，〈酒干倘卖无〉荣获香港电台第7届「十大中文金曲奖」奖 。
  - 1983年6月18日，〈酒干倘卖无〉荣获第3届「香港电影金像奖」最佳电影插曲。
  - 1983年6月18日，《搭错车电影原声带》荣获第3届「香港电影金像奖」最佳电影配乐（陈志远、李寿全）。
  - 1983年11月〈一样的月光〉荣获「[金马奖](../Page/金马奖.md "wikilink")」最佳电影插曲。《搭错车电影原声带》获「金马奖」最佳电影配乐。
  - 1983年12月 荣获「金碟奖」最佳唱片女歌星奖，《苏芮专辑》荣获最佳制作、编曲、歌词、录音奖。
  - 1984年1月，《蓦然回首〉荣获“金鼎奖”优良唱片奖。
  - 1984年1月，〈是不是这样〉荣登新加坡十大流行金曲榜首7周。荣获新加坡“银唱片奖”最佳女歌星。新加坡全年十大歌手之一。
  - 1985年，荣获“金嗓奖”最受欢迎歌星奖。
  - 1985年，《有情天地》中〈沉默的母亲〉荣获「金鼎奖」最佳歌词（陈克华作词）。〈沉默的母亲〉在台湾流行曲唱片畅销排行榜数周排名第一。
  - 1985年，〈谁可相依〉荣获香港电台第8届「十大中文金曲奖」和「最佳中文(流行)歌曲奖」。荣获香港「TVB十大劲歌金曲奖」、「最佳作曲奖」和「最佳编曲奖」。
  - 1985年，〈谁可相依〉荣获第5届「香港电影金像奖」最佳电影插曲。
  - 1985年，〈酒干倘卖无〉荣获香港电台「十年最受欢迎演出奖」。
  - 1986年1月，荣获台湾华视「年度最佳综艺主持人奖」。
  - 1986年1月，〈亲爱的小孩〉荣获「台湾电影金马奖」最佳电影插曲。
  - 1989年，香港电影金像奖：〈凭着爱〉荣获第9届「香港电影金像奖」最佳电影插曲，成为荣获「香港电影金像奖」最佳电影插曲次数最多的歌手（3次），此记录1993年被梅艳芳追平。
  - 1989年，憑<憑著愛>贏得叱咤樂壇流行榜至尊歌曲大獎，成為首位台灣歌手贏得該獎項。
  - 1990年，凭《东方快车谋杀案》荣获“[金带奖](../Page/金带奖.md "wikilink")”最佳表演艺术奖。
  - 1993年3月，《牵手》音乐电视荣获中央电视台MTV一等奖。
  - 1993年10月，入围台湾第5届金曲奖最佳国语女歌手奖，入圍的有[葉蒨文](../Page/葉蒨文.md "wikilink")、[潘越云](../Page/潘越云.md "wikilink")、[张清芳等](../Page/张清芳.md "wikilink")（葉蒨文获奖）。
  - 1993年，〈牵手〉入围台湾第5届金曲奖最佳年度歌曲奖（〈吻别〉获奖） 。
  - 1994年，〈牵手〉荣获《中时晚报》唱片评鉴大赏年度最佳单曲。
  - 1998年，第九屆金曲獎 〈花若離枝〉榮獲最佳方言女演唱人獎
  - 2004年，荣获第4届百事音乐风云榜年度港台及海外华人“终身成就奖”。
  - 2005年6月，于上海荣获当代中国电影音乐庆典「中台湾地区电影音乐家特别奖」。
  - 2009年11月6日，于福州荣获第七届东南劲爆音乐榜“劲爆大奖”。
  - 2010年12月12日，广州举办的“华语金曲奖”30年经典评选“30人30歌30碟”中3项均入围获选。
  - 2018年6月23日，于台灣第二十九屆金曲獎荣获“最佳貢獻獎”

<!-- end list -->

  - 台湾电影金马奖

## 專輯

  - [HOUSE](../Page/HOUSE.md "wikilink")

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' What a difference a day makes</em>'》</p></td>
<td style="text-align: left;"><p>1976年</p></td>
<td style="text-align: left;"><p>英語</p></td>
<td style="text-align: left;"><ol>
<li>Disco Baby</li>
<li>Wedding Bell Blues</li>
<li>What's A Difference A Day Makes</li>
<li>Getting In My Way</li>
<li>Magic In My Life</li>
<li>You And Me Against The World</li>
<li>Day Dreaming</li>
<li>All I Need Is Your Sweet Loving</li>
<li>You've Makde Me So Very Happy</li>
<li>Love Will Keep Us Togther</li>
<li>Not A Little Girl Anymore</li>
<li>Real Good People</li>
</ol></td>
</tr>
</tbody>
</table>

  - [飛碟唱片](../Page/飛碟唱片.md "wikilink")

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' 蘇芮<a href="../Page/一樣的月光.md" title="wikilink">一樣的月光</a></em>'》</p></td>
<td style="text-align: left;"><p>1983年6月18日</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>序曲</li>
<li>一樣的月光</li>
<li>是否</li>
<li>把握</li>
<li>新店溪畔(演奏版亦名：酒矸倘賣無)</li>
<li>請跟我來</li>
<li>變</li>
<li>情路(演奏曲)</li>
<li>一樣的月光(II)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' 驀然回首</em>'》</p></td>
<td style="text-align: left;"><p>1984年1月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>冷的記憶</li>
<li>是不是這樣</li>
<li>心痛的感覺</li>
<li>夜的聲音</li>
<li>也算是奇蹟</li>
<li>迷失</li>
<li>未知</li>
<li>不回首</li>
<li>明天還是要繼續</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《''' 蘇芮3（塵緣） '''》</p></td>
<td style="text-align: left;"><p>1984年9月15日</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>順其自然</li>
<li>內心交戰</li>
<li>愛的完結篇</li>
<li>重逢</li>
<li>塵緣</li>
<li>故事</li>
<li>我曾深愛過</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《''' 有情天地（沉默的母親） '''》</p></td>
<td style="text-align: left;"><p>1985年4月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>沈默的母親</li>
<li>錯</li>
<li>感動我</li>
<li>不再輕易別離</li>
<li>活出自己</li>
<li>再一次</li>
<li>能輸多少</li>
<li>無夢城市</li>
<li>終究是分手</li>
<li>龍</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《''' 蘇芮1986（親愛的小孩） '''》</p></td>
<td style="text-align: left;"><p>1985年12月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>證人</li>
<li>阻擋</li>
<li>親愛的小孩</li>
<li>午夜夢迴</li>
<li>不能說</li>
<li>我沒有錯</li>
<li>慢慢地</li>
<li>為什麼還會想起你</li>
<li>感情是否交流</li>
<li>溫柔的夜</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《''' 龍的心-富貴列車 '''》</p></td>
<td style="text-align: left;"><p>1986年1月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>誰可相依</li>
<li>誰可相依(演奏版)</li>
<li>車站</li>
<li>車站(演奏版)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《''' 第六感（風雨情懷） '''》</p></td>
<td style="text-align: left;"><p>1986年11月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>唱出我和你</li>
<li>風雨情懷</li>
<li>不穩定的愛</li>
<li>我對自己說話</li>
<li>優柔的執著</li>
<li>在轉彎的地方等你</li>
<li>請你告訴我</li>
<li>淡淡的愛</li>
<li>飛躍的羚羊</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' 休息、工作、再工作</em>'》</p></td>
<td style="text-align: left;"><p>1987年9月</p></td>
<td style="text-align: left;"><p>粵語</p></td>
<td style="text-align: left;"><ol>
<li>休息工作再工作</li>
<li>一生一世</li>
<li>後浪</li>
<li>錯愛</li>
<li>午夜鼓聲</li>
<li>機場內</li>
<li>沒有就沒有</li>
<li>只有夜知道</li>
<li>不想追究</li>
<li>舊日的歲月</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' Changes</em>'》</p></td>
<td style="text-align: left;"><p>1987年5月</p></td>
<td style="text-align: left;"><p>英語</p></td>
<td style="text-align: left;"><ol>
<li>One Year Of Love</li>
<li>Changes</li>
<li>Ti Amo</li>
<li>Don't get me wrong</li>
<li>Desperado</li>
<li>A Whiter Shade Of Pale</li>
<li>Living On A Prayer</li>
<li>If You Remember Me</li>
<li>All I Wanted</li>
<li>Love</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' 砂の船</em>'》</p></td>
<td style="text-align: left;"><p>1988年1月</p></td>
<td style="text-align: left;"><p>日語</p></td>
<td style="text-align: left;"><ol>
<li>水の中の淚</li>
<li>砂の船</li>
<li>酒矸倘賣無</li>
<li>一樣的月光</li>
<li>8月のメリークリスマス</li>
<li>少女</li>
<li>Hear Me</li>
<li>やくそく</li>
<li>戀愛一夜</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''台北·東京（跟著感覺走） '''》</p></td>
<td style="text-align: left;"><p>1988年1月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>跟著感覺走</li>
<li>奉獻</li>
<li>陽光照不到的角落</li>
<li>不再流浪</li>
<li>我只要一點暖意</li>
<li>砂之船</li>
<li>不要裝作不在意</li>
<li>冬天的故事</li>
<li>十年前的愛</li>
<li>聖誕禮物</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''一切為明天 '''》</p></td>
<td style="text-align: left;"><p>1988年10月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>一切為明天</li>
<li>不變的心</li>
<li>給愛</li>
<li>我看我自己</li>
<li>鋼鐵的心</li>
<li>北西南東</li>
<li>你還要什麼</li>
<li>北平的夢</li>
<li>也許是你不懂</li>
<li>把今天還給昨天</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''憑著愛 '''》</p></td>
<td style="text-align: left;"><p>1989年5月</p></td>
<td style="text-align: left;"><p>粵語</p></td>
<td style="text-align: left;"><ol>
<li>憑著愛</li>
<li>明朝醒了時</li>
<li>珍惜此際此刻</li>
<li>人生支票</li>
<li>若即若離</li>
<li>一點小風波</li>
<li>欲捨難離</li>
<li>冠軍好人</li>
<li>越愛越深</li>
<li>無悔的愛</li>
<li>我只是個人</li>
<li>多麼的諷刺</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''I've got the music in me '''》</p></td>
<td style="text-align: left;"><p>1989年4月</p></td>
<td style="text-align: left;"><p>英語</p></td>
<td style="text-align: left;"><ol>
<li>I've Got The Music In Me</li>
<li>Missing You</li>
<li>I Still Believe</li>
<li>Whisper In The Dark</li>
<li>Without You</li>
<li>Groovy Kind Of Love</li>
<li>How Am I Supposed To Love Without You</li>
<li>What's Going On</li>
<li>Pa Pa Ya</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''蘇芮經典 '''》</p></td>
<td style="text-align: left;"><p>1989年12月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>一樣的月光</li>
<li>酒矸倘賣嘸</li>
<li>是不是這樣</li>
<li>愛的完結篇</li>
<li>沈默的母親</li>
<li>親愛的小孩</li>
<li>風雨情懷</li>
<li>Changes</li>
<li>跟著感覺走</li>
<li>一切為明天</li>
<li>Without You</li>
<li>再回首</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''蘇芮經典2 '''》</p></td>
<td style="text-align: left;"><p>1991年12月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>唱出我和你</li>
<li>是否</li>
<li>心痛的感覺</li>
<li>優柔的執著</li>
<li>順其自然</li>
<li>鋼鐵的心</li>
<li>請跟我來</li>
<li>奉獻</li>
<li>變</li>
<li>迷失</li>
<li>能輸多少</li>
<li>證人</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《'''牽手 '''》</p></td>
<td style="text-align: left;"><p>1993年3月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>牽手</li>
<li>心也跟著走</li>
<li>不願愛你愛得太疲憊</li>
<li>風阻擋不住我走</li>
<li>不願讓你走</li>
<li>冰冷的記憶</li>
<li>誰的溫柔改變</li>
<li>不停地尋覓</li>
<li>久別重逢</li>
<li>你走了嗎</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>變心</strong>》</p></td>
<td style="text-align: left;"><p>1994年9月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>不要說走就走</li>
<li>生疏的感情</li>
<li>因為你因為我</li>
<li>錯過</li>
<li>變心</li>
<li>除了你(我還有誰)</li>
<li>如果不是為了你</li>
<li>我不該看你的眼神</li>
<li>在雨中分手</li>
<li>愛到天長地久</li>
</ol></td>
</tr>
</tbody>
</table>

  - [福茂唱片](../Page/福茂唱片.md "wikilink")

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>東方快車謀殺案</strong>》</p></td>
<td style="text-align: left;"><p>1989年12月22日</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>你愛的是我還是你自己</li>
<li>蝸牛的家</li>
<li>漲停板</li>
<li>風就是我的朋友</li>
<li>我如何能夠</li>
<li>我不是你偷渡的夜晚</li>
<li>給我現在</li>
<li>淚流在胸口像一場雨</li>
<li>你是唯一的錯</li>
<li>東方快車</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>停在我心裏的溫柔</strong>》</p></td>
<td style="text-align: left;"><p>1990年10月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>要你愛我</li>
<li>停在我心裡的溫柔</li>
<li>讓我們認錯</li>
<li>再被我擁抱你一次</li>
<li>原諒愛情</li>
<li>想你在風中</li>
<li>不要問我能愛你多久</li>
<li>遠走高飛</li>
<li>愛過很多人</li>
<li>你是不是疲倦了</li>
</ol></td>
</tr>
</tbody>
</table>

  - [嘉音唱片](../Page/嘉音唱片.md "wikilink")

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>愛過就是完全</strong>》</p></td>
<td style="text-align: left;"><p>1993年</p></td>
<td style="text-align: left;"><p>粵語</p></td>
<td style="text-align: left;"><ol>
<li>愛過就是完全</li>
<li>共你爸爸再會時</li>
<li>吹熄愛火</li>
<li>漂亮的方向</li>
<li>驟雨中激情</li>
<li>恐怖份子</li>
<li>回頭時是我的愛人</li>
<li>愛你是不是錯</li>
<li>愛的人</li>
<li>愛罪</li>
</ol></td>
</tr>
</tbody>
</table>

  - [豐華唱片](../Page/豐華唱片.md "wikilink")

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>花若離枝</strong>》</p></td>
<td style="text-align: left;"><p>1997年3月28日</p></td>
<td style="text-align: left;"><p>台語</p></td>
<td style="text-align: left;"><ol>
<li>花若離枝</li>
<li>秋風夜雨</li>
<li>夜半路燈</li>
<li>飲者之歌</li>
<li>病相思</li>
<li>抬頭一下看</li>
<li>紅塵夢醒</li>
<li>試了擱再試</li>
<li>為什麼</li>
<li>台北上午零時</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>愛就這麼來</strong>》</p></td>
<td style="text-align: left;"><p>1998年8月13日</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>愛就這麼來</li>
<li>容顏</li>
<li>也算是奇蹟</li>
<li>就愛到這裡</li>
<li>變</li>
<li>河</li>
<li>時差</li>
<li>一樣的月光</li>
<li>你屬於你</li>
<li>是否</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>20年特別選</strong>》</p></td>
<td style="text-align: left;"><p>2003年7月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>奉獻</li>
<li>再回首</li>
<li>請跟我來</li>
<li>酒矸倘賣嘸</li>
<li>一樣的月光</li>
<li>是否</li>
<li>也算是奇蹟</li>
<li>愛就這麼來</li>
<li>容顏</li>
<li>花若離枝</li>
<li>Love Potion No.9</li>
<li>親愛的小孩</li>
<li>跟著感覺走</li>
<li>一樣的月光</li>
<li>Mr. Melody(美樂蒂先生)</li>
<li>How High The Moon(月兒高高)</li>
<li>The Way We Were(往日情懷)</li>
<li>Sophisticated Lady</li>
<li>See Saw(愛的蹺蹺版)</li>
<li>Only You(只有你)</li>
<li>Unchained Melody(奔放的旋律)</li>
<li>For Once In My Life(一生愛一回)</li>
<li>Someday We'll Be Free(我們終將自由)</li>
</ol></td>
</tr>
</tbody>
</table>

  - [華納音樂](../Page/華納音樂.md "wikilink")

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<em>' 回首-時代全經典</em>'》</p></td>
<td style="text-align: left;"><p>2003年9月</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"><ol>
<li>是不是這樣</li>
<li>心痛的感覺</li>
<li>也算是奇蹟</li>
<li>迷失</li>
<li>順其自然</li>
<li>內心交戰</li>
<li>愛的完結篇</li>
<li>塵緣</li>
<li>沈默的母親</li>
<li>錯</li>
<li>能輸多少</li>
<li>再一次</li>
<li>感動我</li>
<li>證人</li>
<li>親愛的小孩</li>
<li>明天還是要繼續</li>
<li>再回首</li>
<li>一切為明天</li>
<li>鋼鐵的心</li>
<li>北西南東</li>
<li>優柔的執著</li>
<li>唱出我和你</li>
<li>在轉彎的地方等你</li>
<li>Changes</li>
<li>風雨情懷</li>
<li>跟著感覺走</li>
<li>奉獻</li>
<li>牽手(牽手唱翻天)</li>
<li>Without You</li>
<li>不回首</li>
<li>休息工作再工作</li>
<li>憑著愛</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>《<strong>感動我 My Favorite Your Memory [12”+7” 典藏黑膠</strong>》</p></td>
<td style="text-align: left;"><p>2018年10月12日</p></td>
<td style="text-align: left;"><p>國語與英語</p></td>
<td style="text-align: left;"><p>Side A</p>
<ol>
<li>感動我</li>
<li>心痛的感覺</li>
<li>未知</li>
<li>不變的心</li>
<li>冷的記憶</li>
</ol>
<p>Side B</p>
<ol>
<li>不回首</li>
<li>風雨情懷</li>
<li>阻擋</li>
<li>故事</li>
<li>不能說</li>
</ol>
<p>7” Bonus黑膠 (45RPM)</p>
<p>Side A</p>
<ol>
<li>坦克上的蝴蝶 [35周年全新新曲]</li>
</ol>
<p>Side B</p>
<ol>
<li>Who’s Lovin’ You [2018 One Take錄音]</li>
</ol></td>
</tr>
</tbody>
</table>

## 曾演唱著名的曲目

  - 酒干倘卖无
  - 一样的月光
  - 请跟我来
  - 变
  - 亲爱的小孩
  - 是否
  - 再回首（凭着爱）
  - 沉默的母亲
  - 是不是这样
  - 一样的感觉
  - 誰可相依
  - 車站
  - 憑著愛
  - 若即若離
  - 奉献
  - 跟着感觉走
  - 牽手
  - 心痛的感覺
  - 鋼鐵的心
  - 花若離枝

## 参考

## 外部链接

  - [苏芮_新浪影音娱乐](http://ent.sina.com.cn/s/h/f/surui/index.html)
  - [苏芮：璀璨三十五年(网易娱乐)](http://ent.163.com/ent_2003/editor/news/star/030907/030907_190396.html)

[Category:台湾女歌手](../Category/台湾女歌手.md "wikilink")
[Category:華語歌手](../Category/華語歌手.md "wikilink")
[Category:閩南語歌手](../Category/閩南語歌手.md "wikilink")
[Category:靈魂樂歌手](../Category/靈魂樂歌手.md "wikilink")
[Category:台灣之最](../Category/台灣之最.md "wikilink")
[Category:五燈獎](../Category/五燈獎.md "wikilink")
[Category:山東裔台灣人](../Category/山東裔台灣人.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[R](../Category/蘇姓.md "wikilink")

1.
2.
3.
4.
5.  [蘇芮追思彭國華
    重現黑色旋風](http://www.cna.com.tw/news/amov/201406280268-1.aspx)，中央社，2014年6月28日
6.
7.
8.
9.
10.
11.