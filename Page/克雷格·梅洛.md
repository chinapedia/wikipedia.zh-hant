**克雷格·卡梅隆·梅洛**（,），[美国](../Page/美国.md "wikilink")[马萨诸塞大学医学院分子医学教授](../Page/马萨诸塞大学.md "wikilink")。2006年因与斯坦福医学院病理学和遗传学教授[安德鲁·法厄发现](../Page/安德鲁·法厄.md "wikilink")[RNA干扰现象而共同获得](../Page/RNA干扰.md "wikilink")2006年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。

1982年获得美国[布朗大学学士学位](../Page/布朗大学.md "wikilink")，1990年在[哈佛大学获得博士学位](../Page/哈佛大学.md "wikilink")。1994年加入马萨诸塞州大学医学院任分子医学教授。

## 外部链接

  - [2006年诺贝尔医学奖](http://nobelprize.org/nobel_prizes/medicine/laureates/2006/)

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:美國醫學家](../Category/美國醫學家.md "wikilink")
[Category:布朗大學校友](../Category/布朗大學校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:葡萄牙裔美国人](../Category/葡萄牙裔美国人.md "wikilink")
[Category:康乃狄克州人](../Category/康乃狄克州人.md "wikilink")
[Category:霍华德·休斯医学研究所研究员](../Category/霍华德·休斯医学研究所研究员.md "wikilink")
[Category:美国地质调查局工作人员](../Category/美国地质调查局工作人员.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")