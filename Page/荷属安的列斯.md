**荷属安的列斯**（；）位于[加勒比海之中](../Page/加勒比海.md "wikilink")，原稱荷屬西印度。曾是[荷蘭王國的](../Page/荷蘭王國_\(聯合王國\).md "wikilink")[自治國](../Page/自治國.md "wikilink")，由相距800多公里的南北两组岛屿组成。面积约800平方公里，人口约21.4万（2002年）。其中80%为黑白混血种人，有少数白人。官方语言为[荷兰语](../Page/荷兰语.md "wikilink")，[帕彼曼都语和](../Page/帕彼曼都语.md "wikilink")[英语](../Page/英语.md "wikilink")，也讲[西班牙语](../Page/西班牙语.md "wikilink")。82%的居民信奉[天主教](../Page/天主教.md "wikilink")，10%的居民信奉[新教](../Page/新教.md "wikilink")。首府为[威廉斯塔德](../Page/威廉斯塔德.md "wikilink")。荷属安的列斯于2010年10月10日起正式解体。

## 解体

1986年[阿魯巴脫離荷属安的列斯](../Page/阿魯巴.md "wikilink")，成為隶属于[荷蘭王國的](../Page/荷蘭王國_\(聯合王國\).md "wikilink")[自治國](../Page/自治國.md "wikilink")。

2007年2月12日，荷蘭政府與群島中除了[古拉索之外的各島達成協議](../Page/古拉索.md "wikilink")：自2010年10月10日起，荷属安的列斯不復存在\[1\]
。五個目前的組成島嶼中，[古拉索和](../Page/古拉索.md "wikilink")[荷属圣马丁的居民希望獲得較高度的自治權](../Page/荷属圣马丁.md "wikilink")，選擇步[阿魯巴之後成為自治國](../Page/阿魯巴.md "wikilink")，與荷蘭本土以平行的地位隸屬於荷蘭王國\[2\]。

至於其他的三個島嶼－[波奈](../Page/波奈.md "wikilink")、[聖佑達修斯與](../Page/聖佑達修斯.md "wikilink")[薩巴](../Page/沙巴_\(荷蘭\).md "wikilink")，合稱BES島嶼，因其居民較傾向於與荷蘭保持緊密的直屬關係，因此成立類似荷蘭本土縣市（[荷語](../Page/荷語.md "wikilink")：gemeenten,
正式: openbare
lichamen）等級的特殊行政區來管轄，居民將享有與荷蘭公民相同的各種權利（包括[立法與](../Page/立法.md "wikilink")[選舉權](../Page/選舉.md "wikilink")）\[3\]。
[Driedaags_bezoek_premier_Den_Uyl_Weeknummer_74-36_-_Open_Beelden_-_58221.ogv](https://zh.wikipedia.org/wiki/File:Driedaags_bezoek_premier_Den_Uyl_Weeknummer_74-36_-_Open_Beelden_-_58221.ogv "fig:Driedaags_bezoek_premier_Den_Uyl_Weeknummer_74-36_-_Open_Beelden_-_58221.ogv")
荷属安的列斯于2010年10月10日起正式解体。
[Koninkrijk_der_Nederlanden.png](https://zh.wikipedia.org/wiki/File:Koninkrijk_der_Nederlanden.png "fig:Koninkrijk_der_Nederlanden.png")）\]\]

## 旗帜

<table>
<caption>荷兰王国成员国家数据对比</caption>
<thead>
<tr class="header">
<th><p>成员国家</p></th>
<th><p>人口<br />
<small>（2009年度）</small></p></th>
<th><p>占王国总人口<br />
的百分比</p></th>
<th><p>面积<br />
<small>（平方千米）</small></p></th>
<th><p>占王国总面积<br />
的百分比</p></th>
<th><p>人口密度<br />
<small>（人／平方千米）</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>16,516,537</p></td>
<td><p>98.30%</p></td>
<td><p>41,848</p></td>
<td><p>98.42%</p></td>
<td><p>394</p></td>
</tr>
<tr class="even">
<td><p>—  <a href="../Page/歐洲區荷蘭.md" title="wikilink">歐洲區荷蘭</a></p></td>
<td><p>16,500,156</p></td>
<td><p>98.20%</p></td>
<td><p>41,526</p></td>
<td><p>97.66%</p></td>
<td><p>394</p></td>
</tr>
<tr class="odd">
<td><p>— </p></td>
<td><p>12,103</p></td>
<td><p>0.07%</p></td>
<td><p>288</p></td>
<td><p>0.68%</p></td>
<td><p>40</p></td>
</tr>
<tr class="even">
<td><p>— </p></td>
<td><p>1,524</p></td>
<td><p>0.01%</p></td>
<td><p>13</p></td>
<td><p>0.03%</p></td>
<td><p>115</p></td>
</tr>
<tr class="odd">
<td><p>— </p></td>
<td><p>2,754</p></td>
<td><p>0.02%</p></td>
<td><p>21</p></td>
<td><p>0.05%</p></td>
<td><p>129</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>106,050</p></td>
<td><p>0.63%</p></td>
<td><p>193</p></td>
<td><p>0.45%</p></td>
<td><p>538</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>140,796</p></td>
<td><p>0.84%</p></td>
<td><p>444</p></td>
<td><p>1.04%</p></td>
<td><p>309</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>40,007</p></td>
<td><p>0.24%</p></td>
<td><p>34</p></td>
<td><p>0.08%</p></td>
<td><p>1,146</p></td>
</tr>
<tr class="odd">
<td><p> <strong><a href="../Page/荷兰王国.md" title="wikilink">荷兰王国</a></strong></p></td>
<td><p><em>16,803,390</em></p></td>
<td><p><em>100.00%</em></p></td>
<td><p><em>42,519</em></p></td>
<td><p><em>100.00%</em></p></td>
<td><p><em>392</em></p></td>
</tr>
</tbody>
</table>

## 参见

  - [荷属加勒比](../Page/荷属加勒比.md "wikilink")
  - [荷兰加勒比区](../Page/荷兰加勒比区.md "wikilink")

## 参考文献

### 引用

### 网页

  -  [保卫西印度群岛所有海岸港口及人口的方法](http://www.wdl.org/zh/item/4393/)

{{-}}

[分類:已不存在的島國](../Page/分類:已不存在的島國.md "wikilink")

[荷属安的列斯](../Category/荷属安的列斯.md "wikilink")
[Category:荷兰历史](../Category/荷兰历史.md "wikilink")
[Category:加勒比地区领地或属地](../Category/加勒比地区领地或属地.md "wikilink")
[Category:阿鲁巴历史](../Category/阿鲁巴历史.md "wikilink")
[Category:圣尤斯特歇斯历史](../Category/圣尤斯特歇斯历史.md "wikilink")
[Category:萨巴历史](../Category/萨巴历史.md "wikilink")

1.   Antillen opgeheven op 10-10-2010 (荷語)
2.  [庫拉索與聖馬丁將擁有國家地位（荷蘭政府官方公告）](http://www.government.nl/actueel/nieuwsarchief/2006/11November/03/0-42-1_42-88793.jsp)
3.  [三個-{zh-tw:安地列斯; zh-hans:安的列斯;
    zh-hant:安的列斯}-島嶼將擁有新地位（荷蘭政府官方公告）](http://www.government.nl/actueel/nieuwsarchief/2006/10October/12/0-42-1_42-84824.jsp)