**鲜卑**（[漢語拼音](../Page/漢語拼音.md "wikilink")：Xiānbēi；[上古汉语擬音](../Page/上古汉语.md "wikilink")：\*S\[a\]r-pe\[1\]），祖先是[古代](../Page/古代史.md "wikilink")[西伯利亞的](../Page/西伯利亚.md "wikilink")[遊牧民族](../Page/遊牧民族.md "wikilink")[东胡](../Page/东胡.md "wikilink")（一說[山戎](../Page/山戎.md "wikilink")）或稱[通古斯部落](../Page/通古斯人種.md "wikilink")。[汉朝时期](../Page/汉朝.md "wikilink")，[匈奴以东的遊牧部落被匈奴的](../Page/匈奴.md "wikilink")[冒顿单于击败后](../Page/冒顿单于.md "wikilink")，退居[乌桓山和](../Page/乌桓山.md "wikilink")[鲜卑山](../Page/鲜卑山.md "wikilink")，成为[乌桓和鲜卑二族](../Page/乌桓.md "wikilink")。鲜卑主要活动于今[赤峰市](../Page/赤峰市.md "wikilink")[内蒙古东部](../Page/内蒙古.md "wikilink")[阿鲁科尔沁旗](../Page/阿鲁科尔沁旗.md "wikilink")[哈古勒河附近](../Page/哈古勒河.md "wikilink")，和[辽宁西北部](../Page/辽宁.md "wikilink")。

2世紀時鮮卑國占据匈奴國領地，稱雄[塞北](../Page/塞北.md "wikilink")。4世紀[西晉滅亡後](../Page/西晉.md "wikilink")，鲜卑陸續在今天的中国北方建立[前燕](../Page/前燕.md "wikilink")、[代國](../Page/代國.md "wikilink")、[後燕](../Page/後燕.md "wikilink")、[西燕](../Page/西燕.md "wikilink")、[西秦](../Page/西秦.md "wikilink")、[南涼](../Page/南涼.md "wikilink")、[南燕及](../Page/南燕.md "wikilink")[北魏等國](../Page/北魏.md "wikilink")，而[漠北則由鮮卑別支](../Page/戈壁.md "wikilink")[柔然稱霸](../Page/柔然.md "wikilink")。439年[北魏統一北方](../Page/北魏.md "wikilink")，之後時常與柔然發生衝突。而後北魏經歷[六鎮之亂後分裂成](../Page/六鎮之亂.md "wikilink")[東魏](../Page/東魏.md "wikilink")、[西魏](../Page/西魏.md "wikilink")，東魏、西魏隨後也分別被[北齊](../Page/北齊.md "wikilink")、[北周所篡](../Page/北周.md "wikilink")。最後北周統一[華北](../Page/華北.md "wikilink")，於581年因[楊堅篡位而亡](../Page/楊堅.md "wikilink")。稱霸塞北的柔然汗國也於552年為[突厥汗國所滅](../Page/突厥.md "wikilink")。於[五胡十六國時期在](../Page/五胡十六國.md "wikilink")[青海建立的鮮卑別支](../Page/青海.md "wikilink")[吐谷渾汗國則維持到](../Page/吐谷渾.md "wikilink")663年為[吐蕃所滅](../Page/吐蕃.md "wikilink")。

## 名稱及起源

鮮卑的名稱來自於這個部落居於鮮卑山所轉化而來\[2\]\[3\]。[大鮮卑山的位置不詳](../Page/大鮮卑山.md "wikilink")，現代學者認為可能是指[大興安嶺北面](../Page/大興安嶺.md "wikilink")。

鮮卑的原意有吉祥與神獸的意義，現在學者認為即是指[馴鹿](../Page/馴鹿.md "wikilink")\[4\]。

“[西伯利亚](../Page/西伯利亚.md "wikilink")”的名称有可能来自“**鲜卑**利亚”\[5\]。

## 歷史

### 传说

鮮卑確實的起源時間不詳。在《[楚辭](../Page/楚辭.md "wikilink")》中，曾稱讚細腰女子，如同鮮卑人\[6\]，因此在[西漢時](../Page/西漢.md "wikilink")，漢人就已知道有鮮卑人的存在。

### [東漢及](../Page/東漢.md "wikilink")[三国时期](../Page/三国.md "wikilink")

東漢初年，鮮卑、烏桓諸部落原仍隸屬於匈奴國。[竇憲聯合北方各游牧部落](../Page/竇憲.md "wikilink")，擊破北匈奴。鮮卑部落佔領[蒙古高原](../Page/蒙古高原.md "wikilink")，開始興起。匈奴各部落皆自號為鮮卑\[7\]。

東漢末年，乌桓被[曹操征伐之后衰落](../Page/白狼山之戰.md "wikilink")，鲜卑崛起。236年，[曹魏](../Page/曹魏.md "wikilink")[幽州](../Page/幽州.md "wikilink")[刺史](../Page/刺史.md "wikilink")[王雄派刺客杀死](../Page/王雄_\(三國\).md "wikilink")[轲比能](../Page/轲比能.md "wikilink")，立其弟为首领。这段时期为鲜卑的古典时期，《[三国志](../Page/三国志.md "wikilink")》“乌丸鲜卑东夷传”中有述。

### [五胡十六国时期](../Page/五胡十六国.md "wikilink")

在[西晋至](../Page/西晋.md "wikilink")[東晉](../Page/東晉.md "wikilink")、五胡[十六國时期](../Page/十六國.md "wikilink")，鲜卑分为三大支部。

#### 东鲜卑

东部有段部、慕容部、宇文部等。

**[段部](../Page/段部.md "wikilink")**曾和[晋阳的](../Page/晋阳.md "wikilink")[刘琨联合](../Page/刘琨.md "wikilink")，后被[羯人的](../Page/羯.md "wikilink")[后赵击溃](../Page/后赵.md "wikilink")，融入中原其他族群之中。

**[慕容部](../Page/慕容部.md "wikilink")**有[慕容氏諸燕的流變](../Page/慕容氏諸燕.md "wikilink")。一開始的[慕容皝於](../Page/慕容皝.md "wikilink")[五胡十六國前期创立的大燕国](../Page/五胡十六國.md "wikilink")，史称[前燕](../Page/前燕.md "wikilink")。前燕於能臣[慕容恪死后衰落](../Page/慕容恪.md "wikilink")，[慕容暐](../Page/慕容暐.md "wikilink")（字景茂）在位时亡于[氐人苻氏的](../Page/氐.md "wikilink")[前秦](../Page/前秦.md "wikilink")，而慕容恪之弟[慕容垂在此之前已降前秦](../Page/慕容垂.md "wikilink")。384年，在前秦[淝水之战败于](../Page/淝水之战.md "wikilink")[东晋之后](../Page/东晋.md "wikilink")，慕容垂复国建立燕国，史称[后燕](../Page/后燕.md "wikilink")。同时慕容暐的弟弟[慕容泓](../Page/慕容泓.md "wikilink")、[慕容冲](../Page/慕容冲.md "wikilink")（小名凤皇，前燕的中山王，[大司马](../Page/大司马.md "wikilink")）也建立了一个燕国，史称[西燕](../Page/西燕.md "wikilink")。394年西燕被后燕所灭。后燕后被鲜卑拓跋氏的[北魏擊敗後北逃](../Page/北魏.md "wikilink")。留在南部的一支后燕[宗室由](../Page/宗室.md "wikilink")[慕容德率領成立](../Page/慕容德.md "wikilink")[南燕](../Page/南燕.md "wikilink")，南燕後被[東晉所灭](../Page/東晉.md "wikilink")。

**[宇文部](../Page/宇文部.md "wikilink")**与[高句丽通好](../Page/高句丽.md "wikilink")，但败给了慕容皝的前燕。之后[南北朝时期宇文氏篡](../Page/南北朝.md "wikilink")[西魏成立](../Page/西魏.md "wikilink")[北周](../Page/北周.md "wikilink")。北周后为[外戚](../Page/外戚.md "wikilink")[杨坚所篡](../Page/杨坚.md "wikilink")，成立[隋朝](../Page/隋朝.md "wikilink")。宇文部落中的**迭刺部**后来是[契丹创始时期的](../Page/契丹.md "wikilink")“契丹八部”之一。

#### 北鲜卑

北部，即中部，有著名的[拓跋氏](../Page/拓跋氏.md "wikilink")，以及与其多次交战的[柔然](../Page/柔然.md "wikilink")，他們同屬**[拓跋部](../Page/拓跋部.md "wikilink")**。

315年，拓跋部領袖[拓跋猗卢被](../Page/拓跋猗卢.md "wikilink")[西晋封为代王](../Page/西晋.md "wikilink")，建立了[代國](../Page/代_\(十六國\).md "wikilink")。376年，代國被[前秦君主](../Page/前秦.md "wikilink")[苻堅攻滅](../Page/苻堅.md "wikilink")。386年，[拓跋珪恢復了代國](../Page/拓跋珪.md "wikilink")，同年改國號為魏，史稱**[北魏](../Page/北魏.md "wikilink")**。北魏后来统一中国北方，在[南北朝初期与南朝对立](../Page/南北朝.md "wikilink")。北魏后分裂为[东魏和](../Page/东魏.md "wikilink")[西魏](../Page/西魏.md "wikilink")，之后分别被[北齐和](../Page/北齐.md "wikilink")[北周所代替](../Page/北周.md "wikilink")。

柔然國与南北朝时期统治中原北方的北魏拓拔氏多次交战。柔然被[突厥系民族击败后](../Page/突厥.md "wikilink")，分为南北两支。柔然的南支逃到[辽河上游](../Page/辽河.md "wikilink")，成为[契丹人的祖先之一](../Page/契丹.md "wikilink")。北支逃到[雅布洛诺夫山脉以东](../Page/雅布洛诺夫山脉.md "wikilink")、[外兴安岭以南的地区](../Page/外兴安岭.md "wikilink")，是[室韦的祖先](../Page/室韦.md "wikilink")。室韦是[蒙古人的祖先之一](../Page/蒙古人.md "wikilink")。

#### 西鲜卑

慕容廆之庶兄[慕容吐谷渾率部从东北迁到](../Page/慕容吐谷渾.md "wikilink")[青海东部](../Page/青海.md "wikilink")，称为**[吐谷浑](../Page/吐谷浑.md "wikilink")**。吐谷浑在鼎盛时期占据现[青海](../Page/青海.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")、[新疆南部](../Page/新疆.md "wikilink")、[四川西部](../Page/四川.md "wikilink")。663年（[唐](../Page/唐朝.md "wikilink")[龙朔三年](../Page/龙朔.md "wikilink")），[吐蕃攻占吐谷浑全境](../Page/吐蕃.md "wikilink")，吐谷浑灭亡。

史書上的看法認為，鲜卑、乌桓与[匈奴混血后代称为](../Page/匈奴.md "wikilink")**[铁弗](../Page/铁弗.md "wikilink")**人。[铁弗人](../Page/铁弗.md "wikilink")[赫连勃勃成立](../Page/赫连勃勃.md "wikilink")[夏国](../Page/夏_\(十六国\).md "wikilink")。

在[阴山以北](../Page/阴山.md "wikilink")，鲜卑与[敕勒融合形成](../Page/敕勒.md "wikilink")**乞伏部**。383年，[前秦的](../Page/前秦.md "wikilink")[乞伏国仁在](../Page/乞伏国仁.md "wikilink")[淝水之战之后造反](../Page/淝水之战.md "wikilink")，后在现[甘肃](../Page/甘肃.md "wikilink")[榆中成立秦国](../Page/榆中.md "wikilink")，史称[西秦](../Page/西秦.md "wikilink")。400年，西秦被[羌人姚氏的](../Page/羌.md "wikilink")[后秦击败](../Page/后秦.md "wikilink")，[乞伏乾归投奔](../Page/乞伏乾归.md "wikilink")[南凉](../Page/南凉.md "wikilink")。409年乞伏乾归重建西秦。431年西秦被赫连氏的夏国所灭。

**[秃发部](../Page/禿髮氏.md "wikilink")**与拓跋部同源\[8\]。397年，臣屬[后凉的](../Page/后凉.md "wikilink")[秃发乌孤叛变独立](../Page/秃发乌孤.md "wikilink")，建立[南凉](../Page/南凉.md "wikilink")。414年南凉灭于乞伏部的[西秦](../Page/西秦.md "wikilink")，秃发部人投奔北魏，被赐姓“[源](../Page/源姓.md "wikilink")”\[9\]。

## 後代

[锡伯族是鲜卑的后裔](../Page/锡伯族.md "wikilink")。有观点认为古代留守祖籍[鲜卑山嘎仙洞的另一支鲜卑部族人](../Page/鲜卑山.md "wikilink")，就是今天的锡伯族。陝西北部、河套一帶和河北地區因長城關塞多次失守，混居大量鮮卑後裔。
因多隱瞞民族或起漢姓，主体併入汉族不得知，其余演化为柔然等草原部落。
現在廣東[鶴山縣宵鄉仍有三千多](../Page/鶴山縣.md "wikilink")[源氏族人](../Page/源姓.md "wikilink")，他們是鮮卑[禿髮氏後人](../Page/禿髮氏.md "wikilink")，1999年他們之一的源可就先生曾到[大興安嶺](../Page/大興安嶺.md "wikilink")([大鮮卑山](../Page/大鮮卑山.md "wikilink"))的[嘎仙洞祭祖](../Page/嘎仙洞.md "wikilink")|需要史料原文。

## 鲜卑君主列表

[西晉時期北方各族分布圖.png](https://zh.wikipedia.org/wiki/File:西晉時期北方各族分布圖.png "fig:西晉時期北方各族分布圖.png")

### 未分裂時

<center>

</center>

  - [軻比能](../Page/軻比能.md "wikilink")

#### 北鲜卑

  - 率觽王 [烏倫](../Page/烏倫.md "wikilink")
  - 率觽侯 [其至鞬](../Page/其至鞬.md "wikilink")

#### 西鲜卑

  - [蒲頭](../Page/蒲頭.md "wikilink")
  - [泄歸泥](../Page/泄歸泥.md "wikilink")

#### 东鲜卑

  - 归义王 [素利](../Page/素利.md "wikilink")
  - [成律歸](../Page/成律歸.md "wikilink")
  - 率義王 [莫護跋](../Page/莫護跋.md "wikilink")

### 拓跋鲜卑（秃发）君主列表

  - [代國](../Page/代國.md "wikilink")→元魏

<!-- end list -->

  - [河西鮮卑](../Page/河西鮮卑.md "wikilink")→[南凉](../Page/南凉.md "wikilink")

| 姓名                                   | 在位時间      |
| ------------------------------------ | --------- |
| [禿髮匹孤](../Page/禿髮匹孤.md "wikilink")   | 210年－231年 |
| [禿髮壽闐](../Page/禿髮壽闐.md "wikilink")   | 231年－252年 |
| [禿髮樹機能](../Page/禿髮樹機能.md "wikilink") | 252年－279年 |
| [禿髮務丸](../Page/禿髮務丸.md "wikilink")   | 279年－？    |
| [禿髮推斤](../Page/禿髮推斤.md "wikilink")   | ？－365年    |
| [禿髮思復鞬](../Page/禿髮思復鞬.md "wikilink") | 365年－？    |

| [庙号](../Page/庙号.md "wikilink")     | [諡號](../Page/諡號.md "wikilink") | 姓名                                   | 在世時间      | [年号](../Page/年号.md "wikilink") | 在位時间      |
| ---------------------------------- | ------------------------------ | ------------------------------------ | --------- | ------------------------------ | --------- |
| [南凉烈祖](../Page/秃发乌孤.md "wikilink") | 武王                             | 秃发乌孤                                 | ？－399年    | 太初                             | 397年－399年 |
|                                    | 康王                             | [秃发利鹿孤](../Page/秃发利鹿孤.md "wikilink") | ？－402年    | 建和                             | 399年－402年 |
|                                    | 景王                             | [秃发傉檀](../Page/秃发傉檀.md "wikilink")   | 365年－415年 | 弘昌、嘉平                          | 402年－414年 |

### 慕容鲜卑君主列表

  - [慕容部](../Page/慕容部.md "wikilink")

| 姓名                                 | 在位時间      |
| ---------------------------------- | --------- |
| [莫護跋](../Page/莫護跋.md "wikilink")   | 220年－245年 |
| [慕容木延](../Page/慕容木延.md "wikilink") | 245年－271年 |
| [慕容涉歸](../Page/慕容涉歸.md "wikilink") | 271年－283年 |
| [慕容耐](../Page/慕容刪.md "wikilink")   | 283年－285年 |
| [慕容廆](../Page/慕容廆.md "wikilink")   | 285年－307年 |

  - [前燕](../Page/前燕.md "wikilink")

| [庙号](../Page/庙号.md "wikilink")    | [諡號](../Page/諡號.md "wikilink") | 姓名                               | 在世時间      | [年号](../Page/年号.md "wikilink") | 在位時间      |
| --------------------------------- | ------------------------------ | -------------------------------- | --------- | ------------------------------ | --------- |
| 前燕高祖                              | 武宣皇帝                           | [慕容廆](../Page/慕容廆.md "wikilink") | 269年－333年 |                                | 307年－333年 |
| 前燕太祖                              | 文明皇帝                           | [慕容皝](../Page/慕容皝.md "wikilink") | 297年－348年 |                                | 333年－348年 |
| [前燕烈祖](../Page/慕容儁.md "wikilink") | 景昭皇帝                           | 慕容儁                              | 319年－360年 | 元玺、光寿                          | 348年－360年 |
|                                   | 幽皇帝                            | [慕容暐](../Page/慕容暐.md "wikilink") | 350年－384年 | 建熙                             | 360年－370年 |

  - [后燕](../Page/后燕.md "wikilink")

| [庙号](../Page/庙号.md "wikilink")          | [諡號](../Page/諡號.md "wikilink") | 姓名                                   | 在世時间      | [年号](../Page/年号.md "wikilink") | 在位時间      |
| --------------------------------------- | ------------------------------ | ------------------------------------ | --------- | ------------------------------ | --------- |
| [后燕世祖](../Page/慕容垂.md "wikilink")       | 成武皇帝                           | 慕容垂                                  | 326年－396年 | 燕元、建兴                          | 384年－396年 |
|                                         |                                | [慕容詳](../Page/慕容詳.md "wikilink")     | ？－397年    | 建始                             | 397年      |
|                                         |                                | [慕容麟](../Page/慕容麟.md "wikilink")     | ？－398年    | 延平                             | 397年      |
|                                         |                                | [蘭汗](../Page/蘭汗.md "wikilink")       | ？－398年    | 青龙                             | 398年      |
| [后燕烈祖](../Page/慕容宝.md "wikilink")(后燕烈宗) | 惠愍皇帝                           | 慕容宝                                  | 355年－398年 | 永康                             | 396年－398年 |
| [后燕中宗](../Page/慕容盛.md "wikilink")       | 昭武皇帝                           | 慕容盛                                  | 373年－401年 | 建平、长乐                          | 398年－401年 |
|                                         | 獻莊皇帝                           | [慕容全](../Page/慕容全.md "wikilink")     | ？－370年    |                                |           |
|                                         | 昭文皇帝                           | [慕容熙](../Page/慕容熙.md "wikilink")     | 385年－407年 | 光始、建始                          | 401年－407年 |
|                                         | 惠懿皇帝                           | [慕容雲](../Page/慕容雲.md "wikilink")(高雲) | ？－409年    | 正始                             | 407年－409年 |

  - [西燕](../Page/西燕.md "wikilink")

| [諡號](../Page/諡號.md "wikilink") | 姓名                               | 在世時间      | [年号](../Page/年号.md "wikilink") | 在位時间      |
| ------------------------------ | -------------------------------- | --------- | ------------------------------ | --------- |
|                                | [慕容泓](../Page/慕容泓.md "wikilink") | ？－384年    | 燕兴                             | 384年      |
| 威帝                             | [慕容冲](../Page/慕容冲.md "wikilink") | 359年－386年 | 更始                             | 384年－386年 |
|                                | [段随](../Page/段随.md "wikilink")   | ？－386年    | 昌平                             | 386年      |
|                                | [慕容顗](../Page/慕容顗.md "wikilink") | ？－386年    | 建明                             | 386年      |
|                                | [慕容瑤](../Page/慕容瑤.md "wikilink") | ？－386年    | 建平                             | 386年      |
|                                | [慕容忠](../Page/慕容忠.md "wikilink") | ？－386年    | 建武                             | 386年      |
|                                | [慕容永](../Page/慕容永.md "wikilink") | ？－394年    | 中兴                             | 386年－394年 |

  - [南燕](../Page/南燕.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/庙号.md" title="wikilink">庙号</a></p></th>
<th><p><a href="../Page/諡號.md" title="wikilink">諡號</a></p></th>
<th><p>姓名</p></th>
<th><p>在世時间</p></th>
<th><p><a href="../Page/年号.md" title="wikilink">年号</a></p></th>
<th><p>在位時间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/慕容德.md" title="wikilink">南燕世祖</a>(南燕世宗)</p></td>
<td><p>献武皇帝</p></td>
<td><p>慕容德</p></td>
<td><p>336年－405年</p></td>
<td><p>燕平、建平</p></td>
<td><p>398年－405年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>穆帝<br />
(慕容超追崇)</p></td>
<td><p><a href="../Page/慕容納.md" title="wikilink">慕容納</a></p></td>
<td><p>？—385年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/慕容超.md" title="wikilink">慕容超</a></p></td>
<td><p>385年－410年</p></td>
<td><p>太上</p></td>
<td><p>405年－410年</p></td>
</tr>
</tbody>
</table>

### [隴西鮮卑君主列表](../Page/隴西鮮卑.md "wikilink")

  - [隴西部](../Page/隴西部.md "wikilink")

| 姓名                                   | 在位時间    |
| ------------------------------------ | ------- |
| [乞伏祐邻](../Page/乞伏祐邻.md "wikilink")   | 265年－?年 |
| [乞伏结权](../Page/乞伏结权.md "wikilink")   |         |
| [乞伏利那](../Page/乞伏利那.md "wikilink")   |         |
| [乞伏祁埿](../Page/乞伏祁埿.md "wikilink")   |         |
| [乞伏述延](../Page/乞伏述延.md "wikilink")   |         |
| [乞伏傉大寒](../Page/乞伏傉大寒.md "wikilink") |         |
| [乞伏司繁](../Page/乞伏司繁.md "wikilink")   | ?年－385年 |

  - [西秦](../Page/西秦.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/庙号.md" title="wikilink">庙号</a></p></th>
<th><p><a href="../Page/諡號.md" title="wikilink">諡號</a></p></th>
<th><p>姓名</p></th>
<th><p>在世時间</p></th>
<th><p><a href="../Page/年号.md" title="wikilink">年号</a></p></th>
<th><p>在位時间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/乞伏国仁.md" title="wikilink">西秦烈祖</a></p></td>
<td><p>宣烈王</p></td>
<td><p>乞伏国仁</p></td>
<td><p>？－388年</p></td>
<td><p>建义</p></td>
<td><p>385年－388年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乞伏乾歸.md" title="wikilink">西秦高祖</a></p></td>
<td><p>武元王</p></td>
<td><p>乞伏乾歸</p></td>
<td><p>？－412年</p></td>
<td><p>太初、更始</p></td>
<td><p>388年－400年<br />
409年－412年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乞伏熾磐.md" title="wikilink">西秦太祖</a></p></td>
<td><p>文昭王</p></td>
<td><p>乞伏炽磐</p></td>
<td><p>？－428年</p></td>
<td><p>永康、建弘</p></td>
<td><p>412年－428年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>厲武王</p></td>
<td><p><a href="../Page/乞伏暮末.md" title="wikilink">乞伏暮末</a></p></td>
<td><p>？－431年</p></td>
<td><p>永弘</p></td>
<td><p>428年－431年</p></td>
</tr>
</tbody>
</table>

### 柔然君主列表

  - 蠕蠕（芮芮、茹茹）部落

| 汗號 | 姓名                               | 在位時间 |
| -- | -------------------------------- | ---- |
|    | [木骨闾](../Page/木骨闾.md "wikilink") |      |
|    | [车鹿会](../Page/车鹿会.md "wikilink") |      |
|    | [吐奴傀](../Page/吐奴傀.md "wikilink") |      |
|    | [跋提](../Page/跋提.md "wikilink")   |      |
|    | [地粟袁](../Page/地粟袁.md "wikilink") |      |
|    | [匹候跋](../Page/匹候跋.md "wikilink") |      |
|    | [缊纥提](../Page/缊纥提.md "wikilink") |      |
|    | [曷多汗](../Page/曷多汗.md "wikilink") |      |

  - 柔然汗国

| 汗號       | 姓名                                      | [年号](../Page/年号.md "wikilink") | 在位時间      |
| -------- | --------------------------------------- | ------------------------------ | --------- |
| 丘豆伐可汗    | [社仑](../Page/社仑.md "wikilink")          |                                | 402年－410年 |
| 蔼苦盖可汗    | [斛律](../Page/斛律.md "wikilink")          |                                | 410年－414年 |
|          | [步鹿真](../Page/步鹿真.md "wikilink")        |                                | 414年      |
| 牟汗纥升盖可汗  | [大檀](../Page/大檀_\(可汗\).md "wikilink")   |                                | 414年－429年 |
| 敕连可汗     | [吴提](../Page/吴提.md "wikilink")          |                                | 429年－444年 |
| 处可汗      | [吐贺真](../Page/吐贺真.md "wikilink")        |                                | 444年－464年 |
| 受罗部真可汗   | [予成](../Page/予成.md "wikilink")          | 永康                             | 464年－485年 |
| 伏名敦可汗    | [豆仑](../Page/豆仑.md "wikilink")          | 太平                             | 485年－492年 |
| 候其伏代库者可汗 | [那盖](../Page/那盖.md "wikilink")          | 太安                             | 492年－506年 |
| 佗汗可汗     | [伏图](../Page/伏图.md "wikilink")          | 始平                             | 506年－508年 |
| 豆罗伏跋豆伐可汗 | [丑奴](../Page/丑奴.md "wikilink")          | 建昌                             | 508年－520年 |
| 敕连头兵豆伐可汗 | [阿那瓌](../Page/阿那瓌.md "wikilink")        |                                | 520年－552年 |
| 弥偶可杜句可汗  | [婆罗门](../Page/婆罗门_\(柔然\).md "wikilink") |                                | 521年－525年 |
|          | [铁伐](../Page/铁伐.md "wikilink")          |                                | 552年－553年 |
|          | [登注俟利](../Page/登注俟利.md "wikilink")      |                                | 552年－553年 |
|          | [库提](../Page/库提.md "wikilink")          |                                | 553年      |
|          | [庵罗辰](../Page/庵罗辰.md "wikilink")        |                                | 553年－554年 |
|          | [邓叔子](../Page/邓叔子.md "wikilink")        |                                | 553年－555年 |

### [段部鮮卑君主列表](../Page/段部鮮卑.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>在位時间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/段日陸眷.md" title="wikilink">段日陸眷</a>（段就陸眷）</p></td>
<td><p>？</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段乞珍.md" title="wikilink">段乞珍</a></p></td>
<td><p>？</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段務目塵.md" title="wikilink">段務目塵</a>（段務勿塵）</p></td>
<td><p>303年－310年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段就六眷.md" title="wikilink">段就六眷</a>（段疾陸眷）</p></td>
<td><p>310年－318年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段涉復辰.md" title="wikilink">段涉復辰</a></p></td>
<td><p>318年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段末波.md" title="wikilink">段末波</a>（段末杯、段末柸）</p></td>
<td><p>318年－325年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段牙.md" title="wikilink">段牙</a></p></td>
<td><p>325年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段遼.md" title="wikilink">段遼</a>（段護遼）</p></td>
<td><p>325年－338年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段蘭.md" title="wikilink">段蘭</a>（段鬱蘭）</p></td>
<td><p>343年－？</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段龕.md" title="wikilink">段龕</a><br />
（後稱齊王）</p></td>
<td><p>？－356年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段勤.md" title="wikilink">段勤</a><br />
（後稱趙帝）</p></td>
<td><p>350年－352年</p></td>
</tr>
</tbody>
</table>

### [宇文部君主列表](../Page/宇文部.md "wikilink")

| 姓名                                            | 在位時间           |
| --------------------------------------------- | -------------- |
| [宇文普回](../Page/宇文普回.md "wikilink")            | ？              |
| [宇文莫那](../Page/宇文莫那.md "wikilink")            | ？              |
| 數代以後                                          |                |
| [宇文莫槐](../Page/宇文莫槐.md "wikilink")            | ？－293年         |
| [宇文普拨](../Page/宇文普拨.md "wikilink")            | 293年－？         |
| [宇文丘不勤](../Page/宇文丘不勤.md "wikilink")          | ？              |
| [宇文莫珪](../Page/宇文莫珪.md "wikilink")（宇文莫廆、宇文莫圭） | ？              |
| [宇文逊昵延](../Page/宇文逊昵延.md "wikilink")（宇文悉獨官）   | ？              |
| [宇文乞得龟](../Page/宇文乞得龟.md "wikilink")          | ？－333年         |
| [宇文逸豆归](../Page/宇文逸豆归.md "wikilink")（宇文侯豆歸）   | 333年－344年或345年 |

### 吐谷浑君主列表

  - [慕容吐谷浑](../Page/慕容吐谷浑.md "wikilink")
  - [吐延](../Page/吐延.md "wikilink")
  - [葉延](../Page/葉延.md "wikilink") 329-351
  - [碎奚](../Page/碎奚.md "wikilink") 352-371
  - [視連](../Page/視連.md "wikilink") 372-390
  - [視羆](../Page/視羆.md "wikilink") 391-400
  - [烏紇提](../Page/烏紇提.md "wikilink") 401-405
  - [樹洛干](../Page/樹洛干.md "wikilink") 406-417
  - [阿豺](../Page/阿豺.md "wikilink") 418-424
  - [慕璝](../Page/慕璝.md "wikilink") 425-436
  - [慕利延](../Page/慕利延.md "wikilink") 437-452
  - [拾寅](../Page/拾寅.md "wikilink") 453-481
  - [度易侯](../Page/度易侯.md "wikilink") 482-490
  - [伏連籌](../Page/伏連籌.md "wikilink") 491-約540
  - （[呵罗真](../Page/呵罗真.md "wikilink")、[佛辅](../Page/佛辅.md "wikilink")、[可沓振](../Page/可沓振.md "wikilink")）
  - [-{夸}-吕](../Page/夸吕.md "wikilink") 約541-591
  - [世伏](../Page/世伏.md "wikilink") 592-597
  - [伏允](../Page/伏允.md "wikilink") 598-634
  - 甘豆可汗 [慕容顺](../Page/慕容顺.md "wikilink") 635
  - 乌地也拔勒豆可汗 [诺曷钵](../Page/诺曷钵.md "wikilink") 636-687

## 語言

鮮卑人使用的語言，今日稱之為[鲜卑语](../Page/鲜卑语.md "wikilink")，早已失傳。現時語言學家只能依靠歷史文獻的紀錄來重構這種語言。根據對於蒙古帝國時代，以突厥文字寫下的鮮卑文，現代語言學家多認為鮮卑語屬於蒙古語族。伯希和認為[室韋即是鮮卑的異譯](../Page/室韋.md "wikilink")，成吉思汗時代，蒙古帝國的核心部落即是[蒙兀室韋](../Page/蒙兀室韋.md "wikilink")。

[北魏孝文帝親政後在文化上開始对鲜卑人进行修整](../Page/北魏孝文帝.md "wikilink")，即後世之所謂[孝文漢化](../Page/孝文漢化.md "wikilink")，遷都[洛陽](../Page/洛陽.md "wikilink")，官民皆著[漢服](../Page/漢服.md "wikilink")。改[漢姓](../Page/漢姓.md "wikilink")，如[皇族](../Page/皇族.md "wikilink")[拓跋氏改](../Page/拓跋氏.md "wikilink")[元姓](../Page/元姓.md "wikilink")、[步六孤改](../Page/步六孤.md "wikilink")[陸姓](../Page/陸姓.md "wikilink")、[賀賴氏改](../Page/賀賴氏.md "wikilink")[賀姓](../Page/賀姓.md "wikilink")、[獨孤改姓](../Page/獨孤.md "wikilink")[劉](../Page/劉.md "wikilink")。斷[胡語](../Page/胡語.md "wikilink")：凡三十歲以下[官員一律使用](../Page/官員.md "wikilink")[漢語](../Page/漢語.md "wikilink")，如果仍用鮮卑語，即降爵黜官。通[婚姻](../Page/婚姻.md "wikilink")：鼓勵與[漢族](../Page/漢族.md "wikilink")[世家通婚](../Page/世家.md "wikilink")，並從己身開始迎娶[漢族](../Page/漢族.md "wikilink")[士族女子](../Page/士族.md "wikilink")。

[六鎮之亂以後](../Page/六鎮之亂.md "wikilink")，胡化較深的六鎮部將再度推廣鮮卑語使用，北中國掀起了鮮卑化熱潮。

直到[隋朝末期](../Page/隋朝.md "wikilink")，鲜卑語失傳，鲜卑人作为一个独立于汉族以外的民族的历史就此结束。

## 流行文化和影視形像

  - [金庸武俠小說](../Page/金庸.md "wikilink")《[天龍八部](../Page/天龍八部.md "wikilink")》當中就有慕容鮮卑後人[慕容復](../Page/慕容復.md "wikilink")

## 参考文献

### 引用

### 来源

  - 劉義棠：《中國西域研究》
  - Thomas Barfield 著，袁劍 譯：《危險的邊疆：游牧帝國與中國》（南京：江蘇人民出版社，2011年）
  - 王明珂：《游牧者的抉擇：面對漢帝國的北亞游牧部族》（桂林：廣西師範大學出版社，2008年）

## 參見

  - [柔然](../Page/柔然.md "wikilink")
  - [北魏](../Page/北魏.md "wikilink")

## 外部連結

  - [中國人、中國心 - 尋找失落了的東胡 -
    源氏](https://www.youtube.com/watch?v=OQ1m3TWIU5M)

{{-}}

[鲜卑](../Category/鲜卑.md "wikilink")

1.  Baxter-Sagart Old Chinese reconstruction
2.  《後漢書》〈烏桓鮮卑列傳〉：「鮮卑者，亦東胡之支也，別依鮮卑山，故因號焉。其言語習俗與烏桓同。」
3.  《魏書》卷1〈帝記第一〉：「昔黃帝有子二十五人，或內列諸華，或外分荒服。昌意少子，受封北土，國有大鮮卑山，因以為號。其后世為君長，統幽都之北，廣漠之野。畜牧遷徙，射獵為業，淳朴為俗，簡易為化，不為文字，刻木紀契而已。」
4.  [北方各民族中建立国家最多的民族--鲜卑](http://www.morinhor.com/special/s_200401/index.asp)
5.  包尔罕、冯家升：《西伯利亚名称的由来》（《锡伯族文学历史论文集》，新疆社会科学院民研所，1981年）
6.  《楚辭》〈大招〉：「滂心綽態，姣麗施只。小腰秀頸，若鮮卑只。」
7.  《後漢書》卷90〈烏桓鮮卑傳〉：「和帝永元中，大將軍竇憲遣右校尉耿夔擊破匈奴，北單于逃走，鲜卑因此轉徙據其地。匈奴餘种留者尚有十餘萬落，皆自號鲜卑，鮮卑由此漸盛。」
8.  《[晉書](../Page/晉書.md "wikilink")·載記第二十六》：禿發烏狐，河西鮮卑人也。其先與後魏同出。
9.  《[魏書](../Page/魏書.md "wikilink")·源賀傳》：源賀，自署河西王禿發傉檀之子也。傉檀為乞伏熾磐所滅，賀自樂都來奔。賀偉容貌，善風儀。世祖素聞其名，及見，器其機辯，賜爵西平侯，加龍驤將軍。謂賀曰：「卿與朕源同，因事分姓，今可為源氏。」