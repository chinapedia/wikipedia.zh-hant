**蒂莫西·迈克尔·“蒂姆”·凯恩**（，）生於[明尼苏达州](../Page/明尼苏达州.md "wikilink")[圣保罗](../Page/圣保罗_\(明尼苏达州\).md "wikilink")，[美国律师](../Page/美国.md "wikilink")、政治人物，現任[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[聯邦參議員](../Page/美國參議院.md "wikilink")。

曾任传教士，從政後曾擔任[里士满市长](../Page/里士满_\(弗吉尼亚州\).md "wikilink")、弗吉尼亚副州长、[弗吉尼亚州州长](../Page/弗吉尼亚州.md "wikilink")。2016年獲[希拉里·克林顿选择作为](../Page/希拉里·克林顿.md "wikilink")[民主党副总统参选人](../Page/民主党_\(美国\).md "wikilink")。

## 早年及教育

凱恩出生於[明尼苏达州](../Page/明尼苏达州.md "wikilink")[圣保罗市一個愛爾蘭裔的](../Page/圣保罗_\(明尼苏达州\).md "wikilink")[天主教家庭](../Page/天主教.md "wikilink")，他的父親是一名焊接工及小鐵器行老闆。凱恩在[堪薩斯州](../Page/堪薩斯州.md "wikilink")[堪薩斯城都會區長大](../Page/堪薩斯城_\(堪薩斯州\).md "wikilink")。

凱恩於[密蘇里大學攻讀經濟](../Page/密蘇里大學.md "wikilink")，1979年獲[文學士學位](../Page/文學士.md "wikilink")，而後進入[哈佛大學法學院就讀](../Page/哈佛大學法學院.md "wikilink")，期間凱恩曾經前往[宏都拉斯從事傳教工作](../Page/宏都拉斯.md "wikilink")，因此他能說流利[西班牙語](../Page/西班牙語.md "wikilink")，1983年獲[法學博士學位](../Page/法學博士.md "wikilink")。他在維吉尼亞州與家庭參加一間非裔美國人為主的[教會](../Page/教會.md "wikilink")。

## 弗吉尼亚州州长

凱恩于2005年當選弗吉尼亚州州长，任期四年。因弗吉尼亚州的州憲法禁止州長連任，他在卸任後，轉為擔任任期兩年的美國民主黨全國委員會主席。

## 联邦参议员

2012年，凱恩當選聯邦參議員。

## 2016年总统大选

2016年7月22日，[希拉里·克林顿宣布选择蒂姆](../Page/希拉里·克林顿.md "wikilink")·凯恩作为副总统参选人角逐[2016年美国总统选举](../Page/2016年美国总统选举.md "wikilink")\[1\]。

## 参考资料

## 延伸閱讀

  - 參議員

<!-- end list -->

  - 州長

<!-- end list -->

  - [2005 campaign
    contributions](http://www.vpap.org/offices/governor/elections/?year_and_type=2005regular)
    at the Virginia Public Access Project
  - [Moving Virginia Forward Archived Web
    Site, 2007](https://wayback.archive-it.org/663/*/http://www.movingvirginiaforward.com/)
    part of [Virginia's Political Landscape, 2007 Web Archive
    Collection](http://www.archive-it.org/public/collection.html?id=663)
    at [Virginia
    Memory](http://www.virginiamemory.com/collections/archival_web_collections)
  - [Moving Virginia Forward Archived Web
    Site, 2009](https://wayback.archive-it.org/1295/*/http://www.movingvirginiaforward.com/)
    part of [Virginia's Political Landscape, 2009 Web Archive
    Collection](http://www.archive-it.org/public/collection.html?id=1295)
    at [Virginia
    Memory](http://www.virginiamemory.com/collections/archival_web_collections)
  - [Tim Kaine for Governor Archived Web
    Site, 2005–2006](https://wayback.archive-it.org/190/*/http://www.kaine2005.org/)
    part of [Virginia's Political Landscape, Fall 2005 Web Archive
    Collection](http://www.archive-it.org/public/collection.html?id=190)
    at [Virginia Memory](http://www.virginiamemory.com/)
  - [Governor Tim Kaine Administration Web Site
    Archive, 2006–2010](http://www.archive-it.org/public/collection.html?id=263)
  - [Kaine Email
    Project](http://www.virginiamemory.com/collections/kaine/) at the
    [Library of Virginia](../Page/Library_of_Virginia.md "wikilink")

## 外部連結

  - [參議員網站](http://www.kaine.senate.gov/)

  - [參議院競選網站](http://kaineforva.com/)

  -
  -
|-    |-    |-     |-    |-    |-    |-   |-     |-

[Category:美国民主党联邦参议员](../Category/美国民主党联邦参议员.md "wikilink")
[Category:美国民主党員](../Category/美国民主党員.md "wikilink")
[Category:弗吉尼亚州州长](../Category/弗吉尼亚州州长.md "wikilink")
[Category:美國民主黨全國委員會主席](../Category/美國民主黨全國委員會主席.md "wikilink")
[Category:弗吉尼亚州民主党人](../Category/弗吉尼亚州民主党人.md "wikilink")
[Category:美國法學家](../Category/美國法學家.md "wikilink")
[Category:美國律師](../Category/美國律師.md "wikilink")
[Category:天主教傳教士](../Category/天主教傳教士.md "wikilink")
[Category:美國傳教士](../Category/美國傳教士.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:密蘇里大學校友](../Category/密蘇里大學校友.md "wikilink")
[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:明尼蘇達州人](../Category/明尼蘇達州人.md "wikilink")
[Category:維吉尼亞州聯邦參議員](../Category/維吉尼亞州聯邦參議員.md "wikilink")
[Category:律師出身的政治人物](../Category/律師出身的政治人物.md "wikilink")
[Category:哈佛法学院校友](../Category/哈佛法学院校友.md "wikilink")
[Category:里士满市长](../Category/里士满市长.md "wikilink")
[Category:2016年美國副總統候選人](../Category/2016年美國副總統候選人.md "wikilink")

1.