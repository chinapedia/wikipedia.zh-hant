**Big
City遠東巨城購物中心**，是一座位於臺灣[新竹市](../Page/新竹市.md "wikilink")[東區的大型](../Page/東區_\(新竹市\).md "wikilink")[購物中心](../Page/購物中心.md "wikilink")，其商場總樓板面積為102,376坪，營業面積71,500坪，總土地面積11,920坪。本購物中心工程由1999年開始興建直至2003年開幕，當時的「風城購物中心」號稱是全東南亞最大的娛樂休閒購物中心。但是因設計及內部規畫不良，在2006年以後便因經營不善而大量負債，內部店家開始撤資，僅存少數幾家店面慘澹經營。到2007年底，因長久積欠電費之故遭到斷電。在剩餘的店家撤出之後，風城購物中心停止營業，於2009年底以前尚未有恢復營業的計畫。在2010年3月，傳出臺灣金聯資產管理公司以及中國長城資產管理公司表達了重新開發與投資本購物中心的意願。
到了5月中旬，經過前2次拍賣流標後， 此棟建築終於在第3次拍賣中，被遠東集團透過競標取得產權與經營權。原預計於2011年12月初，以「Big
City遠東巨城購物中心」的面貌重新開幕，因管線配置延至2012年4月28日開幕，為北台灣最大購物中心\[1\]。2015年起，突破台灣百貨公司業的目標一百億營業額，擠進台灣前8大營業額百貨、購物中心。2017年上半年[Facebook打卡數位居全臺第二](../Page/Facebook.md "wikilink")，250萬次僅次於[桃園國際機場](../Page/桃園國際機場.md "wikilink")\[2\]\[3\]，並宣布將隔壁旅館棟納入購物中心空間，引進更多知名品牌，定名為「創藝大樓」，於2018年6月15日正式納入營運\[4\]。

## 風城購物中心時期（2001年-2007年）

風城購物中心興建計畫之總投資金額達[新臺幣](../Page/新臺幣.md "wikilink")160億，被[行政院經建會列為](../Page/行政院經建會.md "wikilink")「民間建設重大投資案」。其股東團隊及持份比例為：雍聯公司（大雍集團）（67%）、利台紡織（14%）、[中華開發工業銀行](../Page/中華開發工業銀行.md "wikilink")<span style="font-size:smaller;">[（註1）](../Page/#備註.md "wikilink")</span>（5%）、[新竹國際商業銀行](../Page/渣打國際商業銀行.md "wikilink")<span style="font-size:smaller;">[（註2）](../Page/#備註.md "wikilink")</span>（3%）、其他（11%）。\[5\]
[Taiwan_WINDANCE_Shopping_Mall_Map.png](https://zh.wikipedia.org/wiki/File:Taiwan_WINDANCE_Shopping_Mall_Map.png "fig:Taiwan_WINDANCE_Shopping_Mall_Map.png")

1999年，開始興建；2003年，正式開幕。

2006年以後，因經營不善，大量負債，內部店家開始撤資，僅存少數店面慘澹經營。其間，因長久積欠電費，購物中心遭斷電。2007年底，在剩餘店家撤出後，商場停止營業。

2010年3月，臺灣金聯資產管理公司及中國長城資產管理公司表達有意重新開發與投資購物中心。\[6\]\[7\]5月中旬，經前兩次拍賣流標後\[8\]，購物中心終於在第三次拍賣中，被遠東集團以競標方式取得產權與經營權，其成交金額為新臺幣58.89億元。\[9\]\[10\]\[11\]\[12\]\[13\]\[14\]\[15\]\[16\]

## Big City遠東巨城購物中心時期（2012年至今）

2012年4月23日，Big City遠東巨城購物中心試賣；4月28日，正式開始營運。

2015年，商場營業額達[新臺幣](../Page/新臺幣.md "wikilink")104億。\[17\]

2016年，商場營業額達新臺幣105億。\[18\]

2017年，商場營業額達新臺幣110億。\[19\]

2018年，商場營業額達新臺幣116億。\[20\]

### 空間配置

#### 遠東巨城購物中心

Big
City可分為兩大部份，一邊為[遠東巨城購物中心](../Page/遠東巨城購物中心.md "wikilink")，另一邊為[遠東SOGO百貨](../Page/太平洋崇光百貨.md "wikilink")。在巨城Mall方面，地下1樓為較大型的店，例如[city'super](../Page/c!ty'super.md "wikilink")、[UNIQLO](../Page/UNIQLO.md "wikilink")、[GU](../Page/GU.md "wikilink")、、[無印良品](../Page/無印良品.md "wikilink")、[大創百貨](../Page/大創百貨.md "wikilink")、[宜得利家居](../Page/宜得利.md "wikilink")、[MINISO名創優品](../Page/名創優品.md "wikilink")、藍色尾巴寵物用品店、Tomod's、[LOG-ON](../Page/c!ty'super.md "wikilink")、早點名露營用品店、[Family
Mart全家便利商店等店](../Page/全家便利商店.md "wikilink")。3C區則在1樓及地下1樓，地下一樓有除了知名3C品牌[OPPO](../Page/OPPO.md "wikilink")、[SONY
Smartphone](../Page/Xperia.md "wikilink")、[華為](../Page/華為.md "wikilink")、集雅社、[遠傳等](../Page/遠傳.md "wikilink")，一樓則有[德誼Apple](../Page/蘋果公司.md "wikilink")、[BOSE](../Page/博士音響.md "wikilink")、[SONY](../Page/SONY.md "wikilink")。地下3樓為[愛買量販店](../Page/愛買.md "wikilink")。

1樓為國際休閒服飾大店有[GAP](../Page/Gap_\(服飾\).md "wikilink")、H\&M、[PUMA
SELECT](../Page/PUMA.md "wikilink")、[Nike Kicks
Lounge](../Page/Nike.md "wikilink")、[The North
Face黑標等潮流店](../Page/北臉.md "wikilink")，更有知名的[星巴克](../Page/星巴克.md "wikilink")（STARBUCKS）及[PAUL麵包店](../Page/PAUL餐廳.md "wikilink")。2樓為OUTDOOR區，有[ESPRIT](../Page/思捷环球.md "wikilink")、SISLEY（[班尼顿子品牌](../Page/班尼顿.md "wikilink")）及眾多露營品牌等。3樓為流行少男及少女區，有ABC
Mart MEGA
STAGE、及各式服飾品牌等，4樓為美食街及[威秀影城](../Page/威秀影城.md "wikilink")，美食街有台灣復古、日式江戶及法式庭園風格等裝潢設計，共有33家店。5樓則是[誠品書店](../Page/誠品書店.md "wikilink")、生活工場、iloom怡倫家居、WHY
AND 1/2、媽媽餵和Funbox等玩具親子街，6樓是世界健身俱樂部及永續號。

7樓是舊金山大街，包含了一些大型餐廳，有[瓦城泰國料理](../Page/瓦城.md "wikilink")、涓豆腐韓式料理、[彌生軒日式定食](../Page/彌生軒.md "wikilink")、鬥牛士牛排專賣店、開飯川食堂、銀湯匙泰式火鍋、BELLINI
PASTA
PASTA貝里尼義大利麵餐廳、天吉屋、大碼頭海鮮餐廳、京都勝牛、晴木千層豬排、時時香、乾杯燒肉和仿製的[金門大橋](../Page/金門大橋.md "wikilink")(Golden
Gate
Bridge)及漁人碼頭、1:1舊金山叮噹車，以及追風冰上世界和[玩具反斗城](../Page/玩具反斗城.md "wikilink")，最後8樓是運動大街、主題餐廳，包含了大魯閣[棒球打擊練習場](../Page/棒球打擊練習場.md "wikilink")、保齡球場、高爾夫球模擬場、星際戰場、及[湯姆熊歡樂世界和銅盤韓式燒烤](../Page/湯姆熊歡樂世界.md "wikilink")。

整體室內裝修的設計由英國設計公司 concept i及香港負責，將美式購物中心的氛圍帶入每層樓的設計中，例如1樓挑空區上方，鮮豔的Big
City
logo與地面的大同心圓造型相呼應，各個樓層皆以3米以上的挑高及6米的走道重新塑造舒適的購物空間，更賦予不同的空間設計概念，例如3樓針對流行少男及少女打造專屬的品牌迴廊，以及日系辣妹風格的專區，更有潮流鞋區ABC
Mart MEGA
STAGE；4樓美食街的設計更是環繞著台灣復古設計的風格及日系造景和法式庭園風格的美食的區域，仿真的大榕樹、人力車、票亭及仿舊的櫃台，更讓整體氣氛營造的更加生動。

6樓的空間除知名的健身俱樂部外，更有從花博[流行館移設的環生方舟](../Page/流行館.md "wikilink")，為遠東集團投注在綠色環保的心力上增色不少。7樓更以舊金山漁人碼頭的街景來打造，除重新以1:12的比例縮小來打造知名的金門大橋外，Pier
39([39號碼頭](../Page/39號碼頭.md "wikilink"))更是完整被呈現在整個走道上，知名的螃蟹造型地標以及噹噹車（Lum
Lum Bus），都以一比一的比例來呈現，打造出濃厚的美西舊金山碼頭的氣氛。

#### 遠東SOGO百貨

遠東SOGO方面，BigCity店長播本昇表示，遠東SOGO百貨網羅近400個流行品牌進駐，其中40%與新竹既有的[大遠百](../Page/遠東百貨.md "wikilink")、[遠東SOGO百貨新竹店站前館有所區隔](../Page/太平洋崇光百貨.md "wikilink")。同時[CHANEL](../Page/CHANEL.md "wikilink")、[Dior](../Page/Dior.md "wikilink")、ESTEELAUDER、SISLEY、LANCOME、[SKII](../Page/SKII.md "wikilink")、SHISEIDO、[L'OCCITANE等品牌將以旗艦大店登場](../Page/L'OCCITANE.md "wikilink")，ROOTS則打造出新竹首家男女親子複合商品店。1樓為國際精品、化妝品、女鞋及配件包包區，2樓為少淑女裝、淑女裝、珠寶及雜貨配件區，3樓為流行少女裝、內睡衣、配件手錶、包包飾品，4樓為流行服飾、休閒服飾、牛仔服飾、運動服飾等用品區，5樓為紳士服飾用品、高爾夫、童裝及嬰兒用品區，最後6樓則為家庭電器、視聽影音、寢具傢飾、瓷器水晶及鍋具區。

## 歷史

[Taiwan_WINDANCE_Shopping_Mall.JPG](https://zh.wikipedia.org/wiki/File:Taiwan_WINDANCE_Shopping_Mall.JPG "fig:Taiwan_WINDANCE_Shopping_Mall.JPG")[Taiwan_WINDANCE_Shopping_Mall_Night.JPG](https://zh.wikipedia.org/wiki/File:Taiwan_WINDANCE_Shopping_Mall_Night.JPG "fig:Taiwan_WINDANCE_Shopping_Mall_Night.JPG")[巨城的外觀.jpg](https://zh.wikipedia.org/wiki/File:巨城的外觀.jpg "fig:巨城的外觀.jpg")1913至1952年，購物中心之原址曾為[新竹糖廠廠區](../Page/新竹糖廠.md "wikilink")。<span style="font-size:smaller;">[（註4）](../Page/#備註.md "wikilink")</span>\[21\]

1960年，美國氰胺公司<span style="font-size:smaller;">[（註5）](../Page/#備註.md "wikilink")</span>與台灣糖業公司合作投資成立中華民國境內第一家經核准的中外合資製造業公司[臺灣氰胺公司](../Page/臺灣氰胺公司.md "wikilink")，其主要工廠設於今風城購物中心。

1995年，臺灣氰胺公司工廠遷往[新竹縣](../Page/新竹縣.md "wikilink")[新豐鄉之後](../Page/新豐鄉_\(臺灣\).md "wikilink")，該地變更為商業綜合開發區。\[22\]\[23\]

1999年11月購物中心的主建築開始動工\[24\]。

2002年1月16日舉行工地「上樑大典」\[25\]。

2003年7月25日全館正式開幕\[26\]。

2005年8月由風城購物中心開出，支付給廠商的貨款[支票跳票](../Page/支票.md "wikilink")，支票總額為1億2千萬元\[27\]。

2006年4月位於風城購物中心地下1樓的佳士客百貨（JUSCO）結束營業\[28\]。

2006年7月傳出經營不善，入不敷出的消息\[29\]

2006年8月風城購物中心舉辦「轉型大封拍」。「封拍」的意思是指「封館拍賣」，購物中心內的許多商品以不到原售價一半的價格賣出\[30\]。

2006年9月中旬空白多年的[觀光飯店由](../Page/觀光飯店.md "wikilink")[樺福建設以](../Page/樺福建設.md "wikilink")16億元買下，並找來美國[萬豪飯店集團負責經營](../Page/萬豪國際.md "wikilink")，聲明會在2007年4月開幕。這是一座五星級飯店，估計飯店開幕後，將吸引人潮，可望重新帶動風城購物中心商機\[31\]（但至2007年4月初時，本交易案即宣告撤銷）。

2006年9月以後許多商家開始撤資，同時購物中心的網站停止更新\[32\]。

2007年3月26日風城購物中心最大股東雍聯公司向[新竹地方法院聲請重整](../Page/新竹地方法院.md "wikilink")，法院在3月28日裁定緊急保全處分\[33\]。

2007年4月因雍聯集團積欠大量貸款，導致遭廠商對公司財產進行[假扣押並強制執行](../Page/假扣押.md "wikilink")，而假扣押至今未撤銷，故飯店出售案也無法執行\[34\]。

2007年5月風城購物中心總負債已經達新台幣1百億元，其中銀行貸款共計新台幣81.5億元。在19家債權銀行中，新竹國際商銀是最大債權銀行，債權有新台幣13.5億元。[中國信託銀行](../Page/中國信託銀行.md "wikilink")、中華開發銀行、[台北富邦銀行及](../Page/台北富邦銀行.md "wikilink")[國泰世華銀行等](../Page/國泰世華銀行.md "wikilink")，也都是債權銀行。雍聯另外還積欠工程款、土地款、稅金及利息、廠商貨款共18億元\[35\]\[36\]，大部分的商家都已經停止租約。仍然續約的商家為：[亞力山大健康休閒俱樂部](../Page/亞力山大健康休閒俱樂部.md "wikilink")、湯姆熊歡樂世界（接手麻吉室內主題樂園）、威秀影城<span style="font-size:smaller;">[（註3）](../Page/#備註.md "wikilink")</span>、何嘉仁教育中心。

2007年7月2日新竹市議會於市政總質詢時，建議新竹市長[林政則購入風城購物中心](../Page/林政則.md "wikilink")，改建為新竹市政府辦公大樓。

2007年12月18日風城購物中心約60名員工集體抗議長期積欠薪資。

2007年12月28日因風城購物中心一年多以來未繳電費，[臺灣電力公司由該日開始斷電](../Page/臺灣電力公司.md "wikilink")。

2010年3月2日台灣金聯資產管理公司有意整合各銀行債權，重新開發風城購物中心\[37\]。

2010年3月8日中國長城資產管理公司表達了對投資新竹風城購物中心的興趣\[38\]。

2010年4月7日新竹地方法院第2次拍賣，底價新台幣64.42億元流標\[39\]。

2010年5月19日新竹地方法院第3次拍賣，遠東集團以新台幣58.89億元標下風城購物中心\[40\]\[41\]。

2012年4月23日Big City遠東巨城購物中心開始試賣。

2012年4月28日Big City遠東巨城購物中心正式開始營運。

2018年6月15日旁邊旅館棟改造為創藝大樓開幕。

## 周邊

  - [新竹市立演藝廳](../Page/新竹市立演藝廳.md "wikilink")
  - [開臺進士古蹟群](../Page/鄭用錫#遺跡.md "wikilink")
  - [新竹火車站](../Page/新竹車站.md "wikilink")
  - [新竹州廳](../Page/新竹州廳.md "wikilink")
  - [新竹城隍廟](../Page/新竹城隍廟.md "wikilink")
  - [北門街商圈](../Page/北門街商圈.md "wikilink")

<!-- end list -->

  - [新竹之心](../Page/新竹之心.md "wikilink")
  - [黑蝙蝠中隊文物陳列館](../Page/黑蝙蝠中隊文物陳列館.md "wikilink")
  - [新竹市眷村博物館](../Page/新竹市眷村博物館.md "wikilink")
  - [新竹市政府](../Page/新竹市政府.md "wikilink")
  - [新光三越新竹店](../Page/新光三越.md "wikilink")
  - [芙洛麗大飯店](../Page/芙洛麗大飯店.md "wikilink")

<!-- end list -->

  - [國賓大飯店新竹店](../Page/國賓大飯店.md "wikilink")
  - [太平洋SOGO新竹店站前館](../Page/太平洋SOGO.md "wikilink")
  - [晶品城購物廣場](../Page/晶品城購物廣場.md "wikilink")
  - [大遠百新竹店](../Page/大遠百.md "wikilink")
  - [新竹女中](../Page/新竹女中.md "wikilink")
  - [曙光女中](../Page/曙光女中.md "wikilink")

## 樓面

### 遠東巨城購物中心

<table>
<thead>
<tr class="header">
<th><p>使用樓層</p></th>
<th><p>用途</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>12</p></td>
<td><p><a href="../Page/辦公室.md" title="wikilink">辦公室</a></p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p><a href="../Page/辦公室.md" title="wikilink">辦公室</a></p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p><a href="../Page/辦公室.md" title="wikilink">辦公室</a></p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="../Page/辦公室.md" title="wikilink">辦公室</a></p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>室內運動大街</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>舊金山大街</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>健康休閒街</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>文藝親子街</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>美食影城街</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>酷樂潮流街</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>戶外遊趣街</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>國際櫥窗街</p></td>
</tr>
<tr class="odd">
<td><p>B1</p></td>
<td><p>樂活休閒街</p></td>
</tr>
<tr class="even">
<td><p>B2</p></td>
<td><p><a href="../Page/停車場.md" title="wikilink">停車場</a></p></td>
</tr>
<tr class="odd">
<td><p>B3</p></td>
<td><p><a href="../Page/停車場.md" title="wikilink">停車場</a><br />
<a href="../Page/愛買.md" title="wikilink">愛買</a></p></td>
</tr>
<tr class="even">
<td><p>B4</p></td>
<td><p><a href="../Page/停車場.md" title="wikilink">停車場</a></p></td>
</tr>
<tr class="odd">
<td><p>B5</p></td>
<td><p><a href="../Page/停車場.md" title="wikilink">停車場</a></p></td>
</tr>
</tbody>
</table>

### 遠東SOGO

| 使用樓層 | 用途    |
| ---- | ----- |
| 6    | 家用家居館 |
| 5    | 童裝紳士館 |
| 4    | 運動休閒館 |
| 3    | 流行少女館 |
| 2    | 女裝珠寶館 |
| 1    | 精品美妝館 |

## 營業時間

<table>
<thead>
<tr class="header">
<th><p>樓層</p></th>
<th><p>店鋪</p></th>
<th><p>營業時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>8F</p></td>
<td><p><a href="../Page/大魯閣.md" title="wikilink">大魯閣</a></p></td>
<td><p>週日～週四 11：00～21：30<br />
週五～週六 11：00～22：00</p></td>
</tr>
<tr class="even">
<td><p>6F</p></td>
<td><p><a href="../Page/WORLD_GYM.md" title="wikilink">WORLD GYM</a></p></td>
<td><p>06：00～24：00</p></td>
</tr>
<tr class="odd">
<td><p>4F</p></td>
<td><p><a href="../Page/威秀影城.md" title="wikilink">威秀影城</a></p></td>
<td><p>週日～週四 10：00～02：00am<br />
週五～週六 09：00～03：00am</p></td>
</tr>
<tr class="even">
<td><p>B1～8F</p></td>
<td><p>MALL&amp;百貨</p></td>
<td><p>週日～週四 11：00～21：30<br />
週五～週六 11：00～22：00</p></td>
</tr>
<tr class="odd">
<td><p>1F</p></td>
<td><p><a href="../Page/星巴克.md" title="wikilink">星巴克</a></p></td>
<td><p>週日～週四 09：00～21：30<br />
週五～週六 09：00～22：00</p></td>
</tr>
<tr class="even">
<td><p>B3</p></td>
<td><p><a href="../Page/愛買.md" title="wikilink">愛買</a></p></td>
<td><p>10：00～23：00</p></td>
</tr>
</tbody>
</table>

## 備註

  - <span style="font-size:smaller;">註1：「中華開發工業銀行」於2003年12月成為「[中華開發金融控股公司](../Page/中華開發金融控股公司.md "wikilink")」的子公司。</span>\[42\]
  - <span style="font-size:smaller;">註2：「新竹國際商業銀行」已在2006年11月被「[渣打銀行](../Page/渣打銀行.md "wikilink")」併購，並更名為「渣打國際商業銀行股份有限公司」。</span>\[43\]
  - <span style="font-size:smaller;">註3：「華納威秀影城」於2005年2月更名為「[威秀影城公司](../Page/威秀影城.md "wikilink")」。</span>\[44\]
  - <span style="font-size:smaller;">註4：「日本製糖會社」新竹廠是現今「[臺灣糖業公司](../Page/臺灣糖業公司.md "wikilink")」的前身，簡稱「臺糖」--現址後方
    保留部分原貌。</span>\[45\]
  - <span style="font-size:smaller;">註5：「美國家庭用品公司」於1994年併購「」，成為現今的「[美商惠氏藥廠](../Page/惠氏.md "wikilink")」。</span>\[46\]

## 參考資料

## 外部連結

  - [Big City遠東巨城購物中心](https://www.febigcity.com/bigcity)

  -
  - [台灣公司資料](http://company.g0v.ronny.tw/id/53113432)

[Category:東區 (新竹市)](../Category/東區_\(新竹市\).md "wikilink")
[Category:新竹市商場](../Category/新竹市商場.md "wikilink")
[Category:新竹市旅遊景點](../Category/新竹市旅遊景點.md "wikilink")
[Category:2003年完工建築物](../Category/2003年完工建築物.md "wikilink")
[Category:遠東集團](../Category/遠東集團.md "wikilink")

1.  [北台灣最大Mall
    新竹巨城試賣](http://m.xuite.net/blog/li_5516882/twblog/148470035)

2.  [出國瘋打卡　桃園機場上半年打卡300萬次奪冠](http://m.appledaily.com.tw/realtimenews/article/new/20170902/1195732/)

3.  [FB十大打卡景點　新竹這地方榮登全台第2名](http://m.appledaily.com.tw/realtimenews/article/life/20170930/1214265/)

4.  [巨城董事長李靜芳：2018擴大營運面積](https://udn.com/news/story/7241/2533708)

5.  [Taipei
    Times—新竹風城購物中心](http://ecommerce.taipeitimes.com/ecom/eservice/deptstore/windance/profile_b5)

6.  [2010-03-02 經濟日報—台金聯
    要活化風城購物中心](http://udn.com/NEWS/FINANCE/FIN1/5448230.shtml)

7.  [2010-03-08 工商時報—大陸長城資管
    看上新竹風城購物中心](http://news.chinatimes.com/CMoney/News/News-Page-content/0,4993,11050702+122010030800217,00.html)

8.  [2010-04-08 自由電子報—最貴蚊子館
    風城購物中心2度流標](http://www.libertytimes.com.tw/2010/new/apr/8/today-e1.htm)

9.
10.
11. [2010-05-19
    中時電子報—SOGO以58億多億標下新竹風城購物中心](http://news.chinatimes.com/CMoney/News/News-Page-content/0,4993,130507+132010051901684,00.html)

12. [2010-05-19 NOWnews 今日新聞網—風城購物中心三拍
    太平洋崇光百貨58.88億擊敗新光三越](http://www.nownews.com/2010/05/19/11490-2605031.html)

13. [2010-10-01 工商時報—遠東接手
    風城購物中心變裝](http://news.chinatimes.com/CMoney/News/News-Page-content/0,4993,11050701+122010100100458,00.html)


14. [2010-10-27 聯合新聞網—3大新百貨明年第3季開幕
    內容搶先看](http://udn.com/NEWS/FASHION/FAS8/5935627.shtml)

15. [2011-08-15
    中時電子報—新竹巨城購物中心 12月初隆重開幕](http://life.chinatimes.com/2009Cti/Channel/Life/life-article/0,5047,100305+112011081500053,00.html)


16. [2011-11-02 經濟日報—遠東巨城商場
    延明年開幕](http://udn.com/NEWS/STOCK/STO4/6690598.shtml)

17. [BigCity遠東巨城購物中心的Facebook](https://m.facebook.com/FEBigCity)

18. [新竹消費力驚人　巨城今年營收105億](https://tw.appledaily.com/new/realtime/20161230/1024217/)

19. [2017年度營收110億　新竹巨城明年擴大2500坪](https://tw.appledaily.com/new/realtime/20171231/1269499/)

20.

21. [行政院研考會網路村-{里}-—新竹市復中里](http://www.village.gov.tw/webnew/376588100039/)

22.
23. [惠氏藥廠網站—關於我們](http://www.wyeth.com.tw/about/history.html)

24. [風城購物中心網站—關於風城](http://www.windance.com.tw/w_new/wind_guide/wind_intro_about.html)

25.
26.
27. [2005-08-03 跳票1.2億？ 新竹風城：聯貸撥款作業延遲
    營運仍正常](http://nownews.com/2005/08/03/122-1826189.htm)(今日新聞網)

28. [條目：吉之島—臺灣佳士客](../Page/吉之島#台灣佳世客.md "wikilink")

29. 2006-07-28 [台視](../Page/台視.md "wikilink")—[新竹風城購物中心
    債六億封館](http://www.ttv.com.tw/news/view/?i=09507284343505L)

30. [風城購物中心網站—風城新聞](http://www.windance.com.tw/w_new/wind_guide/wind_news_9508.php)

31. [2006-08-16 經濟日報—樺福未來三年推案量
    每年百億](http://tw.myblog.yahoo.com/suite104-hsinchu/article?mid=385&next=380&l=f&fid=14)

32.
33. [2007-04-04 經濟日報](http://bbs.ersite.net/archiver/tid-35208.html)

34.
35.
36. [2007-05-11 聯合新聞網—購物中心競開
    人潮難換錢潮](http://udn.com/NEWS/NATIONAL/NATS5/3840642.shtml)

37.
38.
39.
40.
41.
42. [中華開發工業銀行網站—關於開發工銀](http://www.cdibank.com/aboutcdib.aspx)

43. [2006-11-04 自由電子報—渣打併竹商銀
    不規劃優離優退](http://www.libertytimes.com.tw/2006/new/nov/4/today-e2.htm)

44. [YaHoo奇摩生活—華納威秀改名威秀影城](http://tw.lifestyle.yahoo.com/33b7edcf/061004/180/3k3io.html)

45. [臺灣糖業公司網站—臺糖簡史](http://www.taisugar.com.tw/etaipei2/TaiSugar/index.jsp?categid=318&recordid=79&RecordCount=1)

46.