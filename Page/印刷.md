[Drukarnia-zlamywak.jpg](https://zh.wikipedia.org/wiki/File:Drukarnia-zlamywak.jpg "fig:Drukarnia-zlamywak.jpg")\]\]
**印刷**是指将影像或文字原稿迅速大量复制的一种[技术](../Page/技术.md "wikilink")。一般使用[印刷机将](../Page/印刷机.md "wikilink")[油墨印在](../Page/油墨.md "wikilink")[纸张上](../Page/纸张.md "wikilink")，它是[出版的基本组成部分](../Page/出版.md "wikilink")。[印刷机是能够在承印物上印刷的机械](../Page/印刷机.md "wikilink")。[活字印刷术被称为](../Page/活字印刷术.md "wikilink")[中国古代四大发明之一](../Page/中国古代四大发明.md "wikilink")，可以说[造纸术与印刷术这两项伟大的发明对人类文化知识的传播起到了决定性的作用](../Page/造纸术.md "wikilink")。

今天的[书籍通常采用](../Page/书籍.md "wikilink")[胶印的技术来印刷](../Page/胶印.md "wikilink")，有时也采用[凸版印刷](../Page/凸版印刷.md "wikilink")（主要用于[报纸或](../Page/报纸.md "wikilink")[目录册的印刷](../Page/目录册.md "wikilink")，但比較少有）。

## 印刷的历史

[Jingangjing.png](https://zh.wikipedia.org/wiki/File:Jingangjing.png "fig:Jingangjing.png")\]\]

### 古代印刷术

印刷术最早可是追溯到公元前三千年[兩河流域人民使用](../Page/兩河流域.md "wikilink")[滾筒印章製造印刷品](../Page/滾筒印章.md "wikilink")，主要用作裝飾品和巫術。

[雕版印刷最早是出現在中國](../Page/雕版印刷.md "wikilink")，是[拓石和](../Page/拓石.md "wikilink")[印章兩種方法逐步發展而合成的](../Page/印章.md "wikilink")，是經過很長時間，積累了許多人的經驗而成的，是人類智慧的結晶。現存最早文獻和最早的雕版印刷實物是在600年，即唐朝初期。

  - 7世紀，[唐朝初期出現雕版印刷](../Page/唐朝.md "wikilink")。[沈括](../Page/沈括.md "wikilink")《[梦溪笔谈](../Page/梦溪笔谈.md "wikilink")·技艺》：“板印书籍，唐人尚未盛爲之。”
  - [宋仁宗](../Page/宋仁宗.md "wikilink")[慶曆年間](../Page/慶曆.md "wikilink")（1041-1049），[畢昇發明了膠泥](../Page/畢昇.md "wikilink")[活字印刷术](../Page/活字印刷术.md "wikilink")。
  - 1241年至1250年杨古为[忽必烈的谋士](../Page/忽必烈.md "wikilink")[姚枢用活字版印刷](../Page/姚枢.md "wikilink")[朱熹](../Page/朱熹.md "wikilink")《小学》、《[近思录](../Page/近思录.md "wikilink")》和[吕祖谦的](../Page/吕祖谦.md "wikilink")《经史论集》等书散布四方\[1\]。
  - [元代科學家](../Page/元代.md "wikilink")[王禎](../Page/王禎.md "wikilink")（1260-1330）發明木活字版（亦有人支持宋代就有木要活字本，而且提出了幾種版本加以證明。其中常被人們提到的是被稱為宋本活字本的《毛詩》。由於該書的《唐風·山有樞》篇內的一版中“自”字橫排著，完全可以證明是活字版。
  - 中國[金屬活字的早期記載](../Page/金屬活字.md "wikilink")，於元代科學家[王禎](../Page/王禎.md "wikilink")(1260-1330）在《造活字印書法》(1298)中談到：「近世又鑄錫作字，以鐵條貫之，作行，嵌於盔內，界行印書，但上項字樣，難以使墨，率多印壞，所以不能久行。」
  - [元朝已有雙色紅](../Page/元朝.md "wikilink")、黑套印之書籍。
  - [明朝時期](../Page/明朝.md "wikilink")，出現了雙色、四色套印的印刷品，能印出多層次的彩色印刷品。
  - [德国](../Page/德国.md "wikilink")[约翰内斯·古腾堡](../Page/约翰内斯·古腾堡.md "wikilink")（1397-1468）發明鉛活字版。
  - 19世紀初期，改良鉛活字製作技術並傳播至世界各地。
  - 1804年，英國人[查尔斯·斯坦厄普](../Page/查尔斯·斯坦厄普，第三代斯坦厄普伯爵.md "wikilink")（Charles
    Stanhope）針對活字版弊，發明[泥型鉛版印刷術](../Page/泥型鉛版印刷術.md "wikilink")。
  - 1829年，法國人謝羅發明[紙型鉛版印刷術](../Page/紙型鉛版印刷術.md "wikilink")。
  - 1855年，法國人M.Cillot發明[照相銅鋅版印刷技術](../Page/照相銅鋅版印刷技術.md "wikilink")。進一步發展的[凸版印刷術](../Page/凸版印刷術.md "wikilink")。
  - 1871年，美國人B.B.Blackwell改良[紙型鉛版印刷術](../Page/紙型鉛版印刷術.md "wikilink")，創用薄鉛版，墊以木底印刷。
  - 1882年，德人縻生白克（Meisendach）發明[照相網版印刷術](../Page/照相網版印刷術.md "wikilink")，將照相制版術向前推進了一大步。

### 争议性起源

[SelectedTeachingsofBuddhistSagesandSonMasters1377.jpg](https://zh.wikipedia.org/wiki/File:SelectedTeachingsofBuddhistSagesandSonMasters1377.jpg "fig:SelectedTeachingsofBuddhistSagesandSonMasters1377.jpg")，1377年\]\]
[Yangzhou-Double-Museum-2869.jpg](https://zh.wikipedia.org/wiki/File:Yangzhou-Double-Museum-2869.jpg "fig:Yangzhou-Double-Museum-2869.jpg")
世界上最早的雕版印刷是[中国发明的](../Page/中国.md "wikilink")。十九世纪末曾有日本学者島田主张中国在6世纪的[六朝就有雕版印刷](../Page/六朝.md "wikilink")，根据是[北齐](../Page/北齐.md "wikilink")[顏之推](../Page/顏之推.md "wikilink")《[颜氏家训](../Page/颜氏家训.md "wikilink")》有“书本”一词；又根据[明](../Page/明.md "wikilink")《河汾燕閑錄》中“隋開皇十三年十二月八日敕廢像遺經悉令雕版”一语，认为最晚在隋朝已有雕版印刷。\[2\]

最早的印刷品紀錄

  - [張秀民著](../Page/張秀民.md "wikilink")《中國印刷史》中提出雕版印書始於[唐](../Page/唐.md "wikilink")[貞觀](../Page/貞觀.md "wikilink")，其主要依據是明史學家邵經邦的《弘簡錄》，[唐太宗令梓行](../Page/唐太宗.md "wikilink")[長孫皇后的遺著](../Page/長孫皇后.md "wikilink")《[女則](../Page/女則.md "wikilink")》約在貞觀十年（636年）印刷。書中還引唐[馮贄](../Page/馮贄.md "wikilink")《雲仙散錄》：[唐](../Page/唐.md "wikilink")[玄奘印施普賢菩薩像](../Page/玄奘.md "wikilink")（約645～664年）施送四方為旁證。
  - [唐](../Page/唐.md "wikilink")[開元年間](../Page/開元.md "wikilink")（713～714年）雕本《開元雜報》是世界最早的報紙。

唐代文献根据有

  - （唐）[义净](../Page/义净.md "wikilink")《[南海寄归内法传](../Page/南海寄归内法传.md "wikilink")》：“造泥制底及托模泥像，或印绢纸随处供养。”
  - （唐）[冯贽](../Page/冯贽.md "wikilink")《云仙散录》：“[玄奘以回锋纸印普贤像施于四众](../Page/玄奘.md "wikilink")，每岁五驮无余。”

雕版印刷佛像在唐朝的盛行，和[佛教有莫大关系](../Page/佛教.md "wikilink")，在中国、朝鲜、[日本](../Page/日本.md "wikilink")，佛教技师对早期印刷术的改进及传承做出了巨大贡献。\[3\]后来印刷的范围扩大到其他经典。

  - （宋）[朱益](../Page/朱益.md "wikilink")《猗觉寮杂记》：“雕印文字，唐以前无之，唐末益州始有墨版，后唐方镂《九经》”。

### 印刷品实物

现存最早的印刷品实物有：

  - 1906年新疆吐鲁番出土的690年-699年《[妙法莲华经](../Page/妙法莲华经.md "wikilink")》，现藏日本。
  - 1966年在[韓國](../Page/韓國.md "wikilink")[慶州](../Page/慶州.md "wikilink")[佛國寺发现的](../Page/佛國寺.md "wikilink")《無垢淨光大陀羅尼經》，刻印于7世纪末中国[唐朝](../Page/唐朝.md "wikilink")[武则天时代](../Page/武则天.md "wikilink")。韩国学者认为此经刻于[新罗](../Page/新罗.md "wikilink")。日本学者[长泽规矩对](../Page/长泽规矩.md "wikilink")《無垢淨光大陀羅尼經》是否刻于新罗抱怀疑态度，美国学子钱存训、中国学者张秀民认为此经是唐朝[武周刻本](../Page/武周.md "wikilink")，流入新罗。近期研究表明《無垢淨光大陀羅尼經》在唐武周长安元年（701年）在洛阳佛瘦寺翻译完毕，次年刊于长安，703年分批传入新罗\[4\]。
  - [日本](../Page/日本.md "wikilink")[奈良](../Page/奈良.md "wikilink")[法隆寺](../Page/法隆寺.md "wikilink")《百万塔陀罗尼经》，约770年印刷于日本。
  - [敦煌发现的](../Page/敦煌.md "wikilink")《[金刚经](../Page/金刚经.md "wikilink")》，868年印刷，雕工精细，远胜早先的陀羅尼經。

中国传统的[年画中至今还保留有](../Page/年画.md "wikilink")[木版年画工艺](../Page/木版年画.md "wikilink")，例如著名的[杨柳青年画](../Page/杨柳青年画.md "wikilink")。

由于[雕版印刷制作的印版只能用于特定的印刷品](../Page/雕版印刷.md "wikilink")，[制版的工作量很大](../Page/制版.md "wikilink")、效率低下，使得早期印刷品非常珍贵。为解决制版的问题，后来发明了[活字印刷术](../Page/活字印刷术.md "wikilink")，由于组版中的[活字可以重复使用](../Page/活字.md "wikilink")，使印刷的整体效率大大提高。早期的活字使用胶泥制作，近代则改进为使用铅字。

[活字印刷是由中国北宋](../Page/活字.md "wikilink")1040年的[毕昇发明](../Page/毕昇.md "wikilink")。由当时著名科学家[沈括的](../Page/沈括.md "wikilink")《[梦溪笔谈](../Page/梦溪笔谈.md "wikilink")》记载保存，完整无遗地包括制字、贮字、排版、拆板和刷印等一整套活字印刷术工序，与后世铅字排版的原理完全相同。毕昇发明的[胶泥活字印刷经过精心设计](../Page/胶泥活字印刷.md "wikilink")，反复试验才获得成功。早期活字印刷品的陆续发现和泥活字实证研究成果，证实了毕昇活字印刷术的真实性和科学性，有力地否定了国外学者对毕昇发明活字印刷术的质疑。

[金属活字印刷也是在北宋发明的](../Page/金属活字.md "wikilink")。在12世纪和13世纪，活字印刷术在亚洲广为传播，有许多阿拉伯文和中文的图书馆，含有几万书印刷的书。[金属活字发明于宋代的根据是](../Page/金属活字.md "wikilink")：

1.  元代科学家王祯（1260-1330）在《造活字印书法》（1298）中谈到：“近世又铸锡作字，以铁条贯之，作行，嵌于盔内，界行印书，但上项字样，难以使墨，率多印坏，所以不能久行。”这是中国关于金属活字的早期记载。元初人所说的近世当然是宋代，说明用锡活字印书是在宋代。由非金属活字到金属活字，是印刷材料和造字工艺上的重大改革。
2.  清藏书家[孙从添](../Page/孙从添.md "wikilink")（1769-1840）在《藏书纪要》（1810）中载：“宋刻有铜字刻本、活字本”。明明确确说明宋代有铜活字本。

### 印刷的传播

#### 朝鮮半島

#### 日本

  - 754年唐代高僧[鉴真东渡日本](../Page/鉴真.md "wikilink")，主持印刷三部佛经，将印刷术传入日本。
  - 日本[奈良朝](../Page/奈良朝.md "wikilink")《百万塔陀罗尼经》刻本与韩国[庆州发现的](../Page/庆州.md "wikilink")《无垢净光大陀罗尼经》同属中国唐武周洛阳刻本，可能是日本僧人[道慈两次来华时带回日本的](../Page/道慈.md "wikilink")\[5\]。

<!-- end list -->

  - 1592年日本侵略[朝鲜失败](../Page/朝鲜.md "wikilink")，带回数以万计的铜活字，1593年刊行《古文孝经》。1607年[江兼续在](../Page/江兼续.md "wikilink")[京都刊行铜活字](../Page/京都.md "wikilink")《本六臣注文选》61卷。1615年[林罗山排印铜活字本](../Page/林罗山.md "wikilink")《大藏要览》125部，1616年又印铜活字本群书治要60部。活字多来自朝鲜补以在日本的汉人[林五官铸造的](../Page/林五官.md "wikilink")1.3万字\[6\]。
  - 1637年日本大僧[天海受](../Page/天海.md "wikilink")[幕府将军德川家兴之名主持刻印木活字](../Page/幕府.md "wikilink")《一切经》1453部6323卷\[7\]。

#### 菲律宾

  - 1593年菲律宾又中文印本出现，菲律宾最早的印工是一位中国教徒[约翰维拉](../Page/约翰维拉.md "wikilink")，他在菲律宾创建第一家印刷厂；菲律宾第一部中文基督教义就是他刊印的。原书已失，现存1606年中文木板刻的《[正教便览](../Page/正教便览.md "wikilink")》\[8\]。

#### 西亚

[Egyptian_block_printing_fragment_of_Koran.jpg](https://zh.wikipedia.org/wiki/File:Egyptian_block_printing_fragment_of_Koran.jpg "fig:Egyptian_block_printing_fragment_of_Koran.jpg")
中國的元代，中國和歐洲的交往有了很大的發展。一是蒙古的遠征，將中國的文化帶到西方，一是西方的傳教土也多次來到中國，回國時也帶去了中國的印刷技術。13世紀，意大利人[馬可·波羅在中國旅居多年](../Page/馬可·波羅.md "wikilink")，在他的《遊記》中，敘述了中國印刷紙幣的情況。他的這些介紹，使歐洲人知道了中國的印刷情況。據說，在這一時期，也有人把中國的印刷品及雕版帶到歐洲。中国的印刷术最可能通过穿过[丝绸之路的](../Page/丝绸之路.md "wikilink")[维吾尔人传入](../Page/维吾尔.md "wikilink")[中亚](../Page/中亚.md "wikilink")，经过[阿拉伯世界传播到欧洲](../Page/阿拉伯.md "wikilink")。目前发现的世界上最早的活字母雕版，是在敦煌发现的14世纪古维吾尔文木活字。

1880年考古学家在埃及[法尤姆发现五十余幅自](../Page/法尤姆.md "wikilink")900年至1350年的雕版印刷品残片，其中包括祈祷文和[古兰经片断](../Page/古兰经.md "wikilink")，这些印刷品都是用黑墨印在纸上，拓印如同中国印刷，并非压印。学界一般认为埃及的雕版印刷来自中国而不是土生土长的\[9\]\[10\]。

1294年当时统治[波斯的蒙古人](../Page/波斯.md "wikilink")[凯嘉图汗在](../Page/合赞.md "wikilink")[大不里士印发仿元钞票](../Page/大不里士.md "wikilink")，上印有汉文“钞”字\[11\]。

1310年波斯历史学家[拉希德·丁在](../Page/拉希德·丁.md "wikilink")《世界史》一书中详细记载中国雕版印刷术\[12\]\[13\]。

尽管印刷术很早就传入阿拉伯世界，但受到宗教的阻挠，尤其在1727年，伊斯兰最高领袖宣布，印刷古兰经有违伊斯兰教义，明令禁止，以致印刷术在阿拉伯世界未能发展
\[14\]。1707年，有個名叫[依拉希姆的](../Page/依拉希姆.md "wikilink")[匈牙利人](../Page/匈牙利人.md "wikilink")，曾向土耳其申請在君士坦丁建立印刷所，當時蘇丹亞海默特三世雖予批准，但規定古兰经只可抄写，不准印刷，1729年該所出版了一部埃及史，引起了強烈的反對。一直1825年，阿拉伯各地再沒有進行過印刷的嘗試。不过15世纪也仍有在[意大利印刷的阿拉伯文古兰经](../Page/意大利.md "wikilink")，不过16世纪[叙利亚也仍然存在用阿拉伯文印刷](../Page/叙利亚.md "wikilink")。1797年俄国[凯瑟琳大帝还在俄国用雕版印刷古兰经](../Page/凯瑟琳大帝.md "wikilink")\[15\]。

#### 欧洲

[Ming_Dynasty_playing_card,_c._1400.jpg](https://zh.wikipedia.org/wiki/File:Ming_Dynasty_playing_card,_c._1400.jpg "fig:Ming_Dynasty_playing_card,_c._1400.jpg")
歐洲同中國一樣，最先出現的是雕版印刷，爾後出現的是活字印刷。歐洲人發現中國的活字印刷比雕版印刷效率更高於是很快就把雕版印刷淘汰掉了。關於中國印刷術傳入歐洲的路線，在很多著作中所談的有三條。一條是歐洲的傳教士和旅行家直接把中國的印刷術帶到歐洲；另一條是經由中亞、西亞、北非，最後傳到歐洲；第三條則是由俄國人傳到歐洲其他國家。

发明于中国[宋朝的](../Page/宋朝.md "wikilink")[纸牌在十字军东征前传入西亚](../Page/纸牌.md "wikilink")14世纪初传入欧洲，1377年[西班牙和德国出现纸牌](../Page/西班牙.md "wikilink")，1379年[意大利和](../Page/意大利.md "wikilink")[比利时出现纸牌](../Page/比利时.md "wikilink")，1381年法国出现纸牌\[16\]\[17\]。

德国人認為[美因兹的](../Page/美因兹.md "wikilink")[约翰内斯·古腾堡在](../Page/约翰内斯·古腾堡.md "wikilink")1440年发明了印刷技术，从葡萄酒压榨机改进的机器设计，古腾堡开发了使用凸起的活字，从一开始就使用油性墨。不過，研究學者經過大量的研究與考證得出結論，西方的活字印刷術來源就是中國，漢學家[安田朴曾以](../Page/安田朴.md "wikilink")“歐洲中心論欺騙行為的代表作：所謂古腾堡可能是印刷術的發明人”為題論證\[18\]。

### 工业时代印刷术

[印刷机的发明](../Page/印刷机.md "wikilink")，将印刷技术从手工阶段带入到机械阶段。

## 印刷对知识传播的影响

印刷机的开发使知识的传播发生了革命性的变化：1469年在威尼斯成立第一家印刷社（即出版社），1500年该城就有印刷厂417家。1476年，[威廉·卡克斯顿在英格兰成立第一家印刷社](../Page/威廉·卡克斯顿.md "wikilink")°在1539年，西班牙人Juan
Pablos在墨西哥的墨西哥城。Stephen Day于1628年在美国麻塞诸塞海湾地区建立了北美第一家印刷社，并协助成立了剑桥出版社。

## 信息时代印刷术

在电子技术迅速发展的过程中，印刷进入了电子控制和自动化的时期，电子排版、电子分色、电子雕版广泛应用，在印刷质量和效率上都得到了巨大提高。[计算机的应用继续将印刷带入了](../Page/计算机.md "wikilink")[数字印刷时代](../Page/数字印刷.md "wikilink")，使传统工艺难于完成的小品种、多变化印刷得以顺利实现。

### 電腦印刷

[文件可通过](../Page/文件.md "wikilink")[激光印字机](../Page/激光印字机.md "wikilink")、[喷墨打印机或其它](../Page/喷墨打印机.md "wikilink")[電腦打印机](../Page/電腦打印机.md "wikilink")。在最近几年，计算机打印和工业化印刷工艺已经融合在一起，导致[数位印刷的发展](../Page/数位印刷.md "wikilink")。

### 其他印刷

[版画的绘制过程也常称为印刷](../Page/版画.md "wikilink")，[影印也是印刷的一种](../Page/影印.md "wikilink")。

## 立体印刷（Lenticular printing）

[立体印刷](../Page/立体印刷.md "wikilink")
立体印刷也叫3D透镜印刷是一种印刷效果,光学折射出来栩栩如生的立体效果，物体是由传统印刷(分层设计)和光栅材料。2008年,一公司例如宏大立体印刷提出立体印刷解决方案.

## 三维打印（3D printing）

[三维打印](../Page/三维打印.md "wikilink")
是一种制造技术，物体是由三维文件和[三维打印机生成](../Page/三维打印机.md "wikilink")。物体制造是通过一层层材料的叠加而打印出来.
在2012年,
一些公司例如[Sculpteo或](../Page/Sculpteo.md "wikilink")[Shapeways提出网上的三维打印解决方案](../Page/Shapeways.md "wikilink").

## 参见

<table>
<tbody>
<tr class="odd">
<td><p>方法</p>
<ul>
<li><a href="../Page/活字印刷术.md" title="wikilink">活字印刷术</a></li>
<li><a href="../Page/胶印.md" title="wikilink">胶印</a></li>
<li><a href="../Page/彩色印刷.md" title="wikilink">彩色印刷</a></li>
<li><a href="../Page/凹版印刷.md" title="wikilink">凹版印刷</a></li>
<li><a href="../Page/凸版印刷.md" title="wikilink">凸版印刷</a></li>
<li><a href="../Page/平版印刷.md" title="wikilink">平版印刷</a></li>
<li><a href="../Page/紙版印刷.md" title="wikilink">紙版印刷</a></li>
<li><a href="../Page/無版印刷.md" title="wikilink">無版印刷</a></li>
<li><a href="../Page/雕版印刷.md" title="wikilink">雕版印刷</a></li>
<li><a href="../Page/柔性版印刷.md" title="wikilink">柔性版印刷</a></li>
<li><a href="../Page/照相凹板.md" title="wikilink">照相凹板</a></li>
<li><a href="../Page/防伪印刷.md" title="wikilink">防伪印刷</a></li>
<li><a href="../Page/特种印刷.md" title="wikilink">特种印刷</a></li>
</ul></td>
<td><p>人物</p>
<ul>
<li><a href="../Page/毕昇.md" title="wikilink">毕昇</a></li>
<li><a href="../Page/王祯.md" title="wikilink">王祯</a></li>
<li><a href="../Page/Adam_d&#39;Ambergau.md" title="wikilink">Adam d'Ambergau</a></li>
<li><a href="../Page/David_Bruce.md" title="wikilink">David Bruce</a></li>
<li><a href="../Page/William_Clowes.md" title="wikilink">William Clowes</a></li>
<li><a href="../Page/George_E._Clymer.md" title="wikilink">George E. Clymer</a></li>
<li><a href="../Page/Ivan_Fedorov.md" title="wikilink">Ivan Fedorov</a>，第一位<a href="../Page/俄罗斯.md" title="wikilink">俄罗斯印刷工</a></li>
<li><a href="../Page/约翰内斯·古腾堡.md" title="wikilink">约翰内斯·古腾堡</a></li>
<li><a href="../Page/Francysk_Skaryna.md" title="wikilink">Francysk Skaryna</a>, first <a href="../Page/Belarus.md" title="wikilink">Belarusian</a> printer</li>
<li><a href="../Page/王桢.md" title="wikilink">王桢</a></li>
</ul></td>
<td><p>其它</p>
<ul>
<li><a href="../Page/网纹辊.md" title="wikilink">网纹辊</a></li>
<li></li>
<li></li>
<li><a href="../Page/CUPS.md" title="wikilink">CUPS</a></li>
<li><a href="../Page/书皮.md" title="wikilink">书皮</a></li>
<li><a href="../Page/皮装书.md" title="wikilink">皮装书</a></li>
<li></li>
<li><a href="../Page/古版书.md" title="wikilink">古版书</a></li>
<li><a href="../Page/造纸术.md" title="wikilink">造纸术</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/互联网打印协议.md" title="wikilink">互联网打印协议</a></li>
<li><a href="../Page/排版.md" title="wikilink">排版</a></li>
<li><p>（JDF）</p></li>
</ul>
<ul>
<li><a href="../Page/活字.md" title="wikilink">活字</a></li>
<li></li>
<li><a href="../Page/PostScript.md" title="wikilink">PostScript</a></li>
</ul>
<ul>
<li><a href="../Page/出版.md" title="wikilink">出版</a></li>
<li></li>
<li></li>
<li><a href="../Page/凸版印刷术.md" title="wikilink">凸版印刷术</a></li>
<li><a href="../Page/文字处理.md" title="wikilink">文字处理</a></li>
<li><a href="../Page/隨選列印.md" title="wikilink">隨選列印POD</a>（print on demand）</li>
<li><a href="../Page/字体排印学.md" title="wikilink">字体排印学</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

## 研究書目

  - 錢存訓：《中國古代書籍紙墨及印刷術》（北京：北京圖書館出版社，2002）。
  - 錢存訓：〈[印刷術在中國傳統文化中的功能](http://ccsdb.ncl.edu.tw/ccs/image/01_008_002_01_10.pdf)〉。
  - 潘吉星：《中國、韓國與歐洲早期印刷術的比較》（北京：科學出版社，1997）。
  - 潘吉星：《中國古代四大發明——源流、外傳及世界影響》（合肥：中國科學技術大學出版社，2002）。
  - T.F. Carter著，吳澤炎譯：《中國印刷術的發明和它的西傳》（上海：商務印書館，1957）。

## 外部链接

  - [少量客製印刷](https://eewin-printing.com/printing-gift/)
  - [印刷部落](http://print.ginlee.com.tw)
  - [中华印刷通史](http://www.cgan.net/book/books/print/g-history/gb_12/content.htm)
  - [Online Printing Services](http://www.samedayprinting.com/)
  - [Lenticular Printing](http://3d.lxpack.com/3D-Products-Types.html)
  - [包裝設計](https://www.rtadv.com/)

{{-}}

[印刷](../Category/印刷.md "wikilink")

1.  张秀民 《中国印刷术的发明及其影响》 1958 人民出版社 77页

2.  [向达著](../Page/向达.md "wikilink")《唐代刊书考》收入《唐代长安与西域文明》河北教育出版社2002年ISBN
    978-7-5434-4237-5

3.

4.  潘吉星 《论韩国发现的印本》 《無垢淨光大陀羅尼經》 科学通报 1997 42（10） 1009-1028

5.  潘吉星 《中国金属活字印刷史》 第七章 《活字印刷术在日本和越南的传播》 183页

6.  潘吉星 《中国金属活字印刷技术史》 第七章 《活字印刷在日本和越南的传播》 188页

7.
8.  张秀民 《中国印刷术的发明及其影响》 1958年 人民出版社 168页

9.  Thomas Francis Carter *The Invention of Printing in China and its
    Spread Westward*, The ronald Press, NY 2nd ed 1955, p176-178

10. 钱存训 中国纸和印刷文化史 2004 285页 ISBN 978-7-5633-4472-7/TS

11. 韩琪 《中国科学技术的西传及其影响》 136页

12. [托马斯·弗朗西斯·卡特Thomas](../Page/托马斯·弗朗西斯·卡特.md "wikilink") Francis
    Carter *The Invention of Printing in China and its Spread Westward*,
    The ronald Press, NY 2nd ed 1955, p172-173

13. 张秀民 《中国印刷术的发明及其影响》 1958年 人民出版社 173页

14. Thoms Francis Carter, The Invention of Printing in China and Its
    Spread Westward p178

15. Thoms Francis Carter, The Invention of Printing in China and Its
    Spread Westward p150-151

16. 钱存训 《中国纸和印刷文化史》 287页

17. 韩琪 《中国科学技术的西传及其影响》 136页

18. 《[中國文化西傳歐洲史](../Page/中國文化西傳歐洲史.md "wikilink")》，\[法\]安田朴著，耿昇譯，商務印書館2000年7月。