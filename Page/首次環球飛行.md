**首次環球飛行**是於1924年由一班([美國空軍前身](../Page/美國空軍.md "wikilink"))的[飛行員達成](../Page/飛行員.md "wikilink")，全程用了175天，飛行了44,000[公里](../Page/公里.md "wikilink")(27,000[哩](../Page/哩.md "wikilink"))。

## 歷史

早於1920年代，多個國家都希望能第一個完成環球飛行。1923年春天，對環球飛行感到興趣，希望派一個軍機中隊去完成，並指派了一群軍官去找尋適合的[飛機和計劃任務](../Page/飛機.md "wikilink")。他們首先從勤務隊內現有軍機中尋找，但沒有任何一種能符合要求，於是他們開始向外找尋一種能著陸輪和浮筒互換的[飛機](../Page/飛機.md "wikilink")。作戰部指示航空勤務隊看看*福克(Fokker)F-5*和*戴維斯-道格拉斯Cloudster*是否切合他們的需要，並取得其中一架作測試之用。

當[唐納德·道格拉斯被問及有關Cloudster的資料時](../Page/老唐纳德·威尔士·道格拉斯.md "wikilink")，他反而把一架修改過的魚雷轟炸機—*道格拉斯DT-2*資料交給航空勤務隊。*DT-2*早已被證明是一架堅固的飛機並能夠互換著陸輪和浮筒，而且實機早已面世，[道格拉斯指新飛機可以在取得合約後](../Page/道格拉斯飞行器公司.md "wikilink")45天內交付，並把飛機命名為道格拉斯世界漫遊者(Douglas
World
Cruiser)。航空勤務隊同意計劃並指派埃里克·納爾遜少尉到[加州與](../Page/加州.md "wikilink")[道格拉斯商討詳情](../Page/道格拉斯.md "wikilink")。

[Aircraft_Chicago;Aero27G6.jpg](https://zh.wikipedia.org/wiki/File:Aircraft_Chicago;Aero27G6.jpg "fig:Aircraft_Chicago;Aero27G6.jpg")
道格拉斯在約翰·諾斯拉普的協助下開始改裝*DT-2*去切合航空勤務隊的要求。最主要的修改是把載油量加大，所有炸彈掛架都被移除，並在飛機不同部份加設多個油缸，把載油量由115[加侖](../Page/加侖.md "wikilink")(435[公升](../Page/公升.md "wikilink"))增加到644[加侖](../Page/加侖.md "wikilink")(2,438[公升](../Page/公升.md "wikilink"))。

納爾遜少尉把道格拉斯的方案帶到華盛頓交給航空勤務隊總司令曼森·派翠克少將，並於1923年8月1日獲得批准。作戰部授與道格拉斯一份建造一架測試飛機的合約。測試飛機切合所有規格後，作戰部再授與一份建造四架飛機和備用零件的合約。最後一架飛機在1924年3月11日交付。備用零件包括15具*自由L-12*引擎，14套浮筒和足以建造多兩架飛機的機身部件。備用零件會運送到沿途一些補給站備用。

## 旅程

四架飛機分別被命名為[西雅圖號](../Page/西雅圖.md "wikilink")、[芝加哥號](../Page/芝加哥.md "wikilink")、[波士頓號和](../Page/波士頓.md "wikilink")[新奧爾良號](../Page/新奥尔良.md "wikilink")，於1924年3月17日離開[加州的聖塔摩尼卡飛往](../Page/加州.md "wikilink")[華盛頓州的](../Page/華盛頓州.md "wikilink")[西雅圖](../Page/西雅圖.md "wikilink")。4月6日他們離開[西雅圖正式開始旅程](../Page/西雅圖.md "wikilink")，目的地是[阿拉斯加](../Page/阿拉斯加.md "wikilink")，但西雅圖號因為雖要維修而未有隨大隊出發。維持妥當後，西雅圖號嘗試趕上大隊，但於4月30日在[阿拉斯加半島慕拿港附近的山頭墜毀](../Page/阿拉斯加半島.md "wikilink")。飛行員生還並於5月10日被救起。

餘下的三架飛機繼續他們的旅程，繞過沒有准許他們飛越的[蘇聯](../Page/蘇聯.md "wikilink")，經過[日本](../Page/日本.md "wikilink")、[朝鲜](../Page/日治朝鲜.md "wikilink")、[中國沿岸](../Page/中國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[印支半島](../Page/中南半島.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[印度](../Page/印度.md "wikilink")、[中東之後到達](../Page/中東.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。他們在7月14日[法國國慶日抵達](../Page/法國國慶日.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。再由[巴黎飛到](../Page/巴黎.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，然後飛抵[英國北部準備橫越](../Page/英國.md "wikilink")[大西洋](../Page/大西洋.md "wikilink")。沿途他們著陸輪和浮筒交替使用，以切合起飛著陸時的需要。

8月3日橫越[大西洋途中](../Page/大西洋.md "wikilink")，[波士頓號迫降海面](../Page/波士頓.md "wikilink")，[飛行員被一艘](../Page/飛行員.md "wikilink")[巡洋艦救起](../Page/巡洋艦.md "wikilink")，但波士頓號由巡洋艦拖行期間翻沉。芝加哥號和新奧爾良號繼續橫越大西洋，途經[冰島和](../Page/冰島.md "wikilink")[格陵蘭然後抵達](../Page/格陵蘭.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")。原有的測試飛機被命名為[波士頓](../Page/波士頓.md "wikilink")2號並在[加拿大加入機隊](../Page/加拿大.md "wikilink")，三架飛機飛抵[華盛頓後受到英雄式歡迎](../Page/華盛頓哥倫比亞特區.md "wikilink")。踏入旅程最後階段，他們飛到[美國西岸](../Page/美國.md "wikilink")，短暫停留聖塔摩尼卡後於1924年9月28日到達終點[西雅圖](../Page/西雅圖.md "wikilink")。

整個旅程用了175天。有兩個說法指他們飛行了29,000[哩](../Page/哩.md "wikilink")(46,671[公里](../Page/公里.md "wikilink"))和26,553哩(42,733[公里](../Page/公里.md "wikilink"))。其他國家的努力都失敗了，[美國成功是因為他們付出更多](../Page/美國.md "wikilink")，用了四架飛機和中途大量的支援去完成這個創舉。

## 飛機和組員

  - **西雅圖號**，弗雷德裡克·馬丁少校(飛行員及指揮官)和阿爾華·哈維上士(機械師)；
  - **芝加哥號**，路維爾·史密夫少尉(飛行員)和萊斯利·安納德中尉；
  - **波士頓號**，雷·韋德中尉和亨利·奧登上士；
  - **新奧良爾號**，艾力·納爾遜少尉和傑克·赫丁少尉。

芝加哥號被修復並於1976年在[美國航空航天博物館開始展出](../Page/美國航空航天博物館.md "wikilink")。新奧良爾號由[洛杉磯郡擁有](../Page/洛杉磯郡.md "wikilink")，並借到聖塔摩尼卡的飛行博物館展出。而西雅圖號的殘骸就在[阿拉斯加的航空文物博物館展出](../Page/阿拉斯加.md "wikilink")。

## 外部連結

  - [道格拉斯世界漫遊者
    - 175天環遊世界](https://web.archive.org/web/20090511185902/http://centennialofflight.gov/essay/Aerospace/Douglas_World_Trip/Aero27.htm)
  - [didyouknow.cd:
    環遊世界](http://www.didyouknow.cd/aroundtheworld/flight.htm)
  - [aviation-central.com:
    道格拉斯DT-2世界漫遊者](http://www.aviation-central.com/famous/ab1d0.htm)
  - [聖塔摩尼卡飛行博物館](http://www.museumofflying.com/)

[Category:环球航行](../Category/环球航行.md "wikilink")
[Category:美國空軍歷史](../Category/美國空軍歷史.md "wikilink")
[Category:美国军事的世界之最](../Category/美国军事的世界之最.md "wikilink")
[Category:美国航空史](../Category/美国航空史.md "wikilink")
[Category:1924年美国](../Category/1924年美国.md "wikilink")