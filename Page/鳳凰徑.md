[Lantau_Trail_1.jpg](https://zh.wikipedia.org/wiki/File:Lantau_Trail_1.jpg "fig:Lantau_Trail_1.jpg")
[Lantau_Trail_2.jpg](https://zh.wikipedia.org/wiki/File:Lantau_Trail_2.jpg "fig:Lantau_Trail_2.jpg")

**鳳凰徑**（[英文](../Page/英文.md "wikilink")：**Lantau
Trail**）是[香港著名的](../Page/香港.md "wikilink")[遠足徑](../Page/遠足.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")，於1984年12月4日啟用，由時任[漁農自然護理署署長](../Page/漁農自然護理署.md "wikilink")[李德宏和](../Page/李德宏.md "wikilink")[新界鄉議局副主席](../Page/新界鄉議局.md "wikilink")[曾連主禮](../Page/曾連.md "wikilink")。鳳凰徑是香港第三長的遠足徑，全長70[公里](../Page/公里.md "wikilink")，共分12段，行畢全程大約需時25[小時](../Page/小時.md "wikilink")；全程共有140道[標距柱](../Page/里程碑.md "wikilink")，約每500米一道，貫穿[南大嶼郊野公園及](../Page/南大嶼郊野公園.md "wikilink")[北大嶼郊野公園](../Page/北大嶼郊野公園.md "wikilink")，跨越香港第二高峰[鳳凰山](../Page/鳳凰山_\(香港\).md "wikilink")，繞過[石壁水塘主壩](../Page/石壁水塘.md "wikilink")，以及經過多個沙灘。

## 各段資料

|    路段     |                                   路線                                    |                                                                                           路線                                                                                            | 距離(公里) | 需時(小時) | 難度  |
| :-------: | :---------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :----: | :----: | :-: |
|     1     |  [梅窩至](../Page/梅窩.md "wikilink")[南山](../Page/南山_\(大嶼山\).md "wikilink")  |                                                                        align= | [嶼南道](../Page/嶼南道.md "wikilink")                                                                        |  2.5   |  0.75  |  ＊  |
|     2     | [南山至](../Page/南山_\(大嶼山\).md "wikilink")[伯公坳](../Page/伯公坳.md "wikilink") |                                       align= | [二東山](../Page/二東山.md "wikilink")、[爛頭營](../Page/爛頭營.md "wikilink")、[大東山](../Page/大東山.md "wikilink")                                       |  6.5   |  2.75  | ＊＊＊ |
|     3     |     [伯公坳至](../Page/伯公坳.md "wikilink")[昂坪](../Page/昂坪.md "wikilink")     |                                                                    align= | [鳳凰山](../Page/鳳凰山_\(香港\).md "wikilink")                                                                     |  4.5   |  2.25  | ＊＊＊ |
| 4（舊路線，已封） |     [昂坪至](../Page/昂坪.md "wikilink")[深屈道](../Page/深屈道.md "wikilink")     |                                  align= | [木魚山](../Page/木魚山.md "wikilink")、[獅子頭山](../Page/獅子頭山.md "wikilink")、[大風坳](../Page/大風坳_\(大嶼山\).md "wikilink")                                  |  4.0   |  1.25  | ＊＊  |
|  4（新路線）   |     [昂坪至](../Page/昂坪.md "wikilink")[深屈道](../Page/深屈道.md "wikilink")     |                                    align= | [彌勒山](../Page/彌勒山.md "wikilink")、[昂坪](../Page/昂坪.md "wikilink")、[大風坳](../Page/大風坳_\(大嶼山\).md "wikilink")                                    |  4.0   |  1.25  | ＊＊  |
|     5     |    [深屈道至](../Page/深屈道.md "wikilink")[萬丈布](../Page/萬丈布.md "wikilink")    |                                 align= | [觀音山](../Page/觀音山_\(大嶼山\).md "wikilink")（膝頭哥山）、[羗山](../Page/羗山.md "wikilink")、[靈會山](../Page/靈會山.md "wikilink")                                 |  7.5   |  2.75  | ＊＊  |
|     6     |     [萬丈布至](../Page/萬丈布.md "wikilink")[大澳](../Page/大澳.md "wikilink")     |                                                       align= | [能任亭](../Page/能任亭.md "wikilink")、[凌風亭](../Page/凌風亭.md "wikilink")                                                        |  2.5   |  1.0   | ＊＊  |
|     7     |     [大澳至](../Page/大澳.md "wikilink")[狗嶺涌](../Page/狗嶺涌.md "wikilink")     |    align= | [二澳](../Page/二澳.md "wikilink")、[根頭坳](../Page/根頭坳.md "wikilink")、[煎魚灣](../Page/煎魚灣.md "wikilink")、[鄉鐘岰](../Page/鄉鐘岰.md "wikilink")、[分流](../Page/分流_\(香港\).md "wikilink")     |  10.5  |  3.0   | ＊＊＊ |
|     8     |   [狗嶺涌至](../Page/狗嶺涌.md "wikilink")[石壁水塘](../Page/石壁水塘.md "wikilink")   |                                                                        align= | [宏貝道](../Page/宏貝道.md "wikilink")                                                                        |  5.5   |  1.5   | ＊＊  |
|     9     |    [石壁水塘至](../Page/石壁水塘.md "wikilink")[水口](../Page/水口.md "wikilink")    |                                align= | [石壁警崗](../Page/石壁警崗.md "wikilink")、[香港紅十字會石壁營](../Page/香港紅十字會石壁營.md "wikilink")、[籮箕灣](../Page/籮箕灣.md "wikilink")                                |  6.5   |  2.0   |  ＊  |
|    10     |     [水口至](../Page/水口.md "wikilink")[東涌道](../Page/東涌道.md "wikilink")     |                                                      align= | [塘福](../Page/塘福.md "wikilink")、[長沙](../Page/長沙_\(香港\).md "wikilink")                                                      |  6.5   |  2.0   |  ＊  |
|    11     |     [東涌道至](../Page/東涌道.md "wikilink")[貝澳](../Page/貝澳.md "wikilink")     |                                                                   align= | [䃟石灣](../Page/䃟石灣_\(南大嶼山\).md "wikilink")                                                                    |  4.5   |  1.25  |  ＊  |
|    12     |      [貝澳至](../Page/貝澳.md "wikilink")[梅窩](../Page/梅窩.md "wikilink")      | align= | [芝麻灣道](../Page/芝麻灣道.md "wikilink")、[水牛湖頂](../Page/水牛湖頂.md "wikilink")、[白富田](../Page/白富田.md "wikilink")、[荔枝園村](../Page/荔枝園村.md "wikilink")、[禮智園墳場](../Page/禮智園墳場.md "wikilink") |  9.0   |  3.0   | ＊＊  |

難度:

＊ - 易行之路程 ＊＊ - 難行之路程 ＊＊＊ - 極費力及難行之路程

## 站外鏈結

  - [郊野樂行 -
    鳳凰徑](https://web.archive.org/web/20100202032629/http://www.hkwalkers.net/chi/longtrail/ltrail/ltrail.htm)

[Category:香港遠足徑](../Category/香港遠足徑.md "wikilink")
[Category:大嶼山](../Category/大嶼山.md "wikilink")
[Category:1984年建立](../Category/1984年建立.md "wikilink")