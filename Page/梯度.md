[Gradient2.svg](https://zh.wikipedia.org/wiki/File:Gradient2.svg "fig:Gradient2.svg")
在[向量微积分中](../Page/向量微积分.md "wikilink")，[标量场的](../Page/标量场.md "wikilink")**梯度**是一个[向量场](../Page/向量场.md "wikilink")。标量场中某一点的梯度指向在這點标量场增长最快的方向（當然要比較的話必須固定方向的長度），梯度的絕對值是長度為1的方向中函數最大的增加率，也就是說
\(|\nabla f|=\max_{|v|=1} \{\nabla_v f\}\)，其中 \(\nabla_v\)
代表方向導數。以另一觀點來看，由多變數的泰勒展開式可知，从[欧几里得空间](../Page/欧几里得空间.md "wikilink")**R**<sup>*n*</sup>到**R**的[函数的梯度是在](../Page/函数.md "wikilink")**R**<sup>*n*</sup>某一点最佳的[线性近似](../Page/线性近似.md "wikilink")。在这个意义上，梯度是[雅可比矩阵的一个特殊情况](../Page/雅可比矩阵.md "wikilink")。

  -
<!-- end list -->

  - *梯度*一词有时用于*[斜度](../Page/斜度.md "wikilink")*，也就是一个[曲面沿着给定方向的](../Page/曲面.md "wikilink")*倾斜*程度。可以通过取向量梯度和所研究的方向的[內積来得到斜度](../Page/內積.md "wikilink")。梯度的数值有时也被称为梯度。

## 梯度的解释

假設有一个房间，房间内所有点的温度由一个标量场\(\phi\)给出的，即点\((x,y,z)\)的温度是\(\phi(x,y,z)\)。假设温度不随时间改变。然后，在房间的每一点，该点的梯度将显示变热最快的方向。梯度的大小将表示在该方向上变热的速度。

考虑一座高度在\((x, y)\)点是\(H(x, y)\)的山。\(H\)这一点的梯度是在该点[坡度](../Page/坡度.md "wikilink")（或者说[斜度](../Page/斜度.md "wikilink")）最陡的方向。梯度的大小告诉我们坡度到底有多陡。

梯度也可以告诉我们一个数量在不是最快变化方向的其他方向的变化速度。再次考虑山坡的例子。可以有条直接上山的路其坡度是最大的，则其坡度是梯度的大小。也可以有一条和上坡方向成一个角度的路，例如投影与水平面上的夹角为60°。则，若最陡的坡度是40%，这条路的坡度小一点，是20%，也就是40%乘以60°的余弦。

这个现象可以如下数学的表示。山的高度函数\(H\)的梯度[点积一个单位](../Page/点积.md "wikilink")[向量给出了表面在该向量的方向上的斜率](../Page/向量.md "wikilink")。这称为[方向導數](../Page/方向導數.md "wikilink")。

## 定義 

純量函数
\(\varphi \colon \mathbb{R}^n \mapsto \mathbb{R}\)的梯度表示為：\(\nabla \varphi\)
或\(\operatorname{grad} \varphi\)，其中
\(\nabla\)（[nabla](../Page/nabla符号.md "wikilink")）表示向量[微分算子](../Page/微分算子.md "wikilink")。

函數 \(\varphi\)的梯度，\(\nabla \varphi\)， 為向量場且對任意單位向量  滿足下列方程式:

\[\big(\nabla \varphi(x)\big)\cdot \mathbf{v} = D_{\mathbf v} \varphi(x).\]

### 直角坐標

\(\nabla \varphi\)在三维[直角坐标中表示为](../Page/直角坐标.md "wikilink")

\[\nabla \varphi =\begin{pmatrix}
{\frac{\partial \varphi}{\partial x}},
{\frac{\partial \varphi}{\partial y}},
{\frac{\partial \varphi}{\partial z}}
\end{pmatrix}
=\frac{\partial \varphi}{\partial x} \mathbf{i} + \frac{\partial \varphi}{\partial y}
\mathbf{j} + \frac{\partial \varphi}{\partial z} \mathbf{k}\]

, ,  為標準的單位向量，分別指向 ,  跟  座標的方向。
（*参看[偏导数和](../Page/偏导数.md "wikilink")[向量](../Page/向量.md "wikilink")。*）

虽然使用坐标表达，但结果是在[正交变换下不变](../Page/正交变换.md "wikilink")，从几何的观点来看，这是应该的。

舉例來講，函数\(\varphi=2x+3y^2-\sin (z)\)的梯度为：

\[\nabla \varphi = \begin{pmatrix}
{\frac{\partial \varphi}{\partial x}},
{\frac{\partial \varphi}{\partial y}},
{\frac{\partial \varphi}{\partial z}}
\end{pmatrix} =
\begin{pmatrix}
{2},
 {6y},
 {-\cos (z)}
\end{pmatrix}
=2\mathbf{i}+ 6y\mathbf{j} -\cos(z)\mathbf{k}.\]。

### 圓柱跟球坐標

在圓柱座標中，\(\varphi\) 的梯度為

\[\nabla \varphi(\rho, \theta, z) = \frac{\partial \varphi}{\partial \rho}\mathbf{e}_\rho +
\frac{1}{\rho}\frac{\partial \varphi}{\partial \theta}\mathbf{e}_\theta +
\frac{\partial \varphi}{\partial z}\mathbf{e}_z,\]

是 P 點與 z-軸的垂直距離。  是線 OP 在 xy-面的[投影線與正](../Page/投影.md "wikilink")
x-軸之間的夾角。  與[直角坐標的](../Page/直角坐標.md "wikilink") \(z\) 等值。 ,
跟  為單位向量，指向座標的方向。

在球座標中：

\[\nabla \varphi(r, \theta, \alpha) = \frac{\partial \varphi}{\partial r}\mathbf{e}_r +
\frac{1}{r}\frac{\partial \varphi}{\partial \theta}\mathbf{e}_\theta +
\frac{1}{r \sin\theta}\frac{\partial \varphi}{\partial \alpha}\mathbf{e}_\alpha,\]

##

</math>|time=2014-03-11T02:46:50+00:00}}，定义为

  -

    \\overset{\\underset{\\mathrm{def}}{}}{=} \\left\[ \\frac{\\partial
    }{\\partial x_1}, \\frac{\\partial }{\\partial
    x_2},\\cdots,\\frac{\\partial }{\\partial x_n}
    \\right\]^T=\\frac{\\partial }{\\partial
    \\boldsymbol{x}}</math>|time=2014-03-11T02:53:23+00:00}}

### 对向量的梯度

以n×1实向量**x**为变元的实标量函数f(**x**)相对于**x**的梯度为一n×1列向量**x**，定义为

\[\nabla_{\boldsymbol{x}} f(\boldsymbol{x})\overset{\underset{\mathrm{def}}{}}{=} \left[ \frac{\partial f(\boldsymbol{x})}{\partial x_1}, \frac{\partial f(\boldsymbol{x})}{\partial x_2},\cdots,\frac{\partial f(\boldsymbol{x})}{\partial x_n} \right]^T=\frac{\partial f(\boldsymbol{x})}{\partial \boldsymbol{x}}\]
m维行向量函数\(\boldsymbol{f}(\boldsymbol{x})=[f_1(\boldsymbol{x}),f_2(\boldsymbol{x}),\cdots,f_m(\boldsymbol{x})]\)相对于n维实向量**x**的梯度为一n×m矩阵，定义为

\[\nabla_{\boldsymbol{x}} \boldsymbol{f}(\boldsymbol{x})\overset{\underset{\mathrm{def}}{}}{=}
\begin{bmatrix}
\frac{\partial f_1(\boldsymbol{x})}{\partial x_1} &\frac{\partial f_2(\boldsymbol{x})}{\partial x_1} & \cdots & \frac{\partial f_m(\boldsymbol{x})}{\partial x_1}      \\
\frac{\partial f_1(\boldsymbol{x})}{\partial x_2} &\frac{\partial f_2(\boldsymbol{x})}{\partial x_2} & \cdots & \frac{\partial f_m(\boldsymbol{x})}{\partial x_2}      \\
\vdots &\vdots & \ddots & \vdots \\
\frac{\partial f_1(\boldsymbol{x})}{\partial x_n} &\frac{\partial f_2(\boldsymbol{x})}{\partial x_n} & \cdots &\frac{\partial f_m(\boldsymbol{x})}{\partial x_n}     \\
\end{bmatrix}=\frac{\partial \boldsymbol{f}(\boldsymbol{x})}{\partial \boldsymbol{x}}\]

### 对矩阵的梯度

实标量函数\(\boldsymbol{f}(\boldsymbol{A})\)相对于m×n实矩阵**A**的梯度为一m×n矩阵，简称梯度矩阵，定义为

\[\nabla_{\boldsymbol{A}} \boldsymbol f(\boldsymbol{A})\overset{\underset{\mathrm{def}}{}}{=}
\begin{bmatrix}
\frac{\partial f(\boldsymbol{A})}{\partial a_{11}} &\frac{\partial f(\boldsymbol{A})}{\partial a_{12}} & \cdots & \frac{\partial f(\boldsymbol{A})}{\partial a_{1n}}      \\
\frac{\partial f(\boldsymbol{A})}{\partial a_{21}} &\frac{\partial f(\boldsymbol{A})}{\partial a_{22}} & \cdots & \frac{\partial f(\boldsymbol{A})}{\partial a_{2n}}      \\
\vdots &\vdots & \ddots & \vdots \\
\frac{\partial f(\boldsymbol{A})}{\partial a_{m1}} &\frac{\partial f(\boldsymbol{A})}{\partial a_{m2}} & \cdots &\frac{\partial f(\boldsymbol{A})}{\partial a_{mn}}     \\
\end{bmatrix}=\frac{\partial \boldsymbol{f}(\boldsymbol{A})}{\partial \boldsymbol{A}}\]

### 法则

以下法则适用于实标量函数对向量的梯度以及对矩阵的梯度。

  - 线性法则：若\(f(\boldsymbol{A})\)和\(g(\boldsymbol{A})\)分别是矩阵A的实标量函数，c<sub>1</sub>和c<sub>2</sub>为实常数，则

<center>

\(\frac{\partial [c_1 f(\boldsymbol{A})+c_2 g(\boldsymbol{A})]}{\partial \boldsymbol{A}}=c_1\frac{\partial f(\boldsymbol{A})}{\partial \boldsymbol{A}}+c_2 \frac{\partial g(\boldsymbol{A})}{\partial \boldsymbol{A}}\)

</center>

  - 乘积法则：若\(f(\boldsymbol{A})\)，\(g(\boldsymbol{A})\)和\(h(\boldsymbol{A})\)分别是矩阵A的实标量函数，则

<center>

\(\frac{\partial f(\boldsymbol{A})g(\boldsymbol{A})}{\partial \boldsymbol{A}}=g(\boldsymbol{A})\frac{\partial f(\boldsymbol{A})}{\partial \boldsymbol{A}}+f(\boldsymbol{A}) \frac{\partial g(\boldsymbol{A})}{\partial \boldsymbol{A}}\)

</center>

<center>

\(\frac{\partial f(\boldsymbol{A})g(\boldsymbol{A})h(\boldsymbol{A})}{\partial \boldsymbol{A}}=g(\boldsymbol{A})h(\boldsymbol{A})\frac{\partial f(\boldsymbol{A})}{\partial \boldsymbol{A}}+f(\boldsymbol{A})h(\boldsymbol{A})\frac{\partial g(\boldsymbol{A})}{\partial \boldsymbol{A}}+f(\boldsymbol{A})g(\boldsymbol{A})\frac{\partial h(\boldsymbol{A})}{\partial \boldsymbol{A}}\)

</center>

  - 商法则：若\(g(\boldsymbol{A})\neq 0\)，则

<center>

\(\frac{\partial f(\boldsymbol{A})/ g(\boldsymbol{A})}{\partial \boldsymbol{A}}=\frac{1}{g(\boldsymbol{A})^2} \left[ g(\boldsymbol{A})\frac{\partial f(\boldsymbol{A})}{\partial \boldsymbol{A}}-f(\boldsymbol{A}) \frac{\partial g(\boldsymbol{A})}{\partial \boldsymbol{A}} \right]\)

</center>

  - 链式法则：若**A**为m×n矩阵，且\(y=f(\boldsymbol{A})\)和\(g (y)\)分别是以矩阵**A**和标量y为变元的实标量函数，则

<center>

\(\frac{\partial g(f(\boldsymbol{A}))}{\partial \boldsymbol{A}}=\frac{d g (y)}{dy} \frac{\partial f(\boldsymbol{A})}{\partial \boldsymbol{A}}\)

</center>

## 流形上的梯度

一个[黎曼流形](../Page/黎曼流形.md "wikilink")\(M\)上的对于任意可微函数\(f\)的梯度\(\nabla f\)是一个[向量场](../Page/向量场.md "wikilink")，使得对于每个向量
\(\xi\)，

\[\langle \nabla f, \xi \rangle := \xi f\]
其中\(\langle \cdot, \cdot \rangle\)代表\(M\)上的[内积](../Page/内积.md "wikilink")（度量）而
\(\xi f (p), p\in M\)是\(f\)在點\(p\)，方向為\(\xi (p)\)的[方向導數](../Page/方向導數.md "wikilink")。换句话说，如果\(\varphi :U\subseteq M\mapsto \mathbb{R}^n\)為\(p\)附近的局部座標，在此座標下有\(\xi (x)=\sum_j a_j (x)\frac{\partial}{\partial x_{j} }\),則\(\xi f (p)\)将成为：

\[\xi(f \mid_{p}) := \sum_j a_j(\frac{\partial}{\partial x_{j} }(f \circ \varphi^{-1}) \mid_{\varphi (p)})\]。

函数的梯度和[外微分相关](../Page/外微分.md "wikilink")，因为\(\xi f = df(\xi)\)，实际上內積容许我们可以用一种标准的方式将1-形式\(df\)和向量场\(\nabla f\)建立联系。由\(\nabla f\)的定義，\(df(\xi)=\langle \nabla f, \xi \rangle\)，这样\(f\)的梯度可以"等同"于0-形式的外微分\(df\)，這裡"等同"意味著：兩集合\(\{df \}\)和\(\{\nabla f \}\)之間有1對1的[滿射](../Page/滿射.md "wikilink")。

由定義可算**[流形](../Page/流形.md "wikilink")**上\(\nabla f\)的局部座標表達式為：

\[\nabla f=\sum_{ik} g^{ik}\frac{\partial f}{\partial x^{k}}\frac{\partial}{\partial x^{i}}\]。

請注意這是**[流形](../Page/流形.md "wikilink")**上對黎曼度量
\(ds^2=\sum_{ij}g_{ij}dx^i dx^j\)的公式，跟\(\mathbb{R}^n\)
裡直角座標的公式不同。常常我們寫時會省略求和\(\sum\)符號，不過為了避免混淆，在這裡的公式還是加上去了。

## 柱坐标下的梯度（\(\nabla\)）算符

\(\nabla f(\rho,\theta,z) = \frac{\partial f}{\partial\rho} \mathbf{e}_{\rho}
                            + \frac1{\rho}\frac{\partial f}{\partial\theta} \mathbf{e}_{\theta}
                            + \frac{\partial f}{\partial z} \mathbf{e}_{z}\)

## 球坐标下的梯度（\(\nabla\)）算符

\(\nabla f(r,\theta,\phi) = \frac{\partial f}{\partial r} \mathbf{e}_{r}
                            + \frac1{r}\frac{\partial f}{\partial\theta} \mathbf{e}_{\theta}
                            + \frac1{r\sin \theta}\frac{\partial f}{\partial \phi} \mathbf{e}_{\phi}\)

其中\(\theta\)为极角，\(\phi\)方位角。

## 参考

### 书籍

  -
## 参看

  - [雅可比矩阵](../Page/雅可比矩阵.md "wikilink")
  - [散度](../Page/散度.md "wikilink")
  - [旋度](../Page/旋度.md "wikilink")
  - [偏导数](../Page/偏导数.md "wikilink")
  - [索貝爾算子](../Page/索貝爾算子.md "wikilink")
  - [向量分析](../Page/向量分析.md "wikilink")
  - [离子梯度](../Page/离子梯度.md "wikilink")
  - [梯度下降法](../Page/梯度下降法.md "wikilink")
  - [等位集合](../Page/等位集合.md "wikilink")（Level set）
  - [外微分](../Page/外微分.md "wikilink")

[T](../Category/向量分析.md "wikilink") [T](../Category/場論.md "wikilink")
[Category:导数的推广](../Category/导数的推广.md "wikilink")