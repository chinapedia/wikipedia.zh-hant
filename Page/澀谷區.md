**涩谷区**（）\[1\]，是[日本](../Page/日本.md "wikilink")[東京都的](../Page/東京都.md "wikilink")[特别区之一](../Page/東京23區.md "wikilink")，其名稱源自於位於該行政區中央、以[澀谷車站為中心的](../Page/澀谷車站.md "wikilink")[澀谷地區](../Page/澀谷.md "wikilink")。

與[新宿](../Page/新宿.md "wikilink")、[池袋等因為位處於交通要衝上而發展出的商業街相同](../Page/池袋.md "wikilink")，澀谷也是因為位居[山手線重要轉運站的地位而發展成今日的盛況](../Page/山手線.md "wikilink")。其為[東急集團的大本營](../Page/東急.md "wikilink")，且各種商業活動興旺，這尤其表现在[澀谷車站](../Page/澀谷車站.md "wikilink")[忠犬八公出口處](../Page/忠犬八公.md "wikilink")，著名的[百货店](../Page/百货店.md "wikilink")、[時裝專賣店](../Page/時裝.md "wikilink")、[飲食店](../Page/飲食.md "wikilink")、[咖啡店](../Page/咖啡.md "wikilink")、[休閒設施](../Page/休閒.md "wikilink")、[特種行業等密集如雲](../Page/特種行業.md "wikilink")，是與新宿同樣並列為“24小时不眠之街”的地區。另外，以[西武百貨](../Page/西武百貨.md "wikilink")、[東急百貨](../Page/東急百貨.md "wikilink")、[巴而可](../Page/巴而可.md "wikilink")（）與[109百貨為中心再加上周邊許多服飾店所構成的商店街是最受東京年輕消費族群喜愛的購物勝地](../Page/109百貨.md "wikilink")，向來擁有「年輕人之街」之美稱，也使得澀谷成為日本國内外各種流行風尚的發源地。而澀谷車站八公口外、具有[行人保護時相配置](../Page/行人保護時相.md "wikilink")、屬[井之頭通](../Page/井之頭通.md "wikilink")（）一部份的大型五叉路口，由於超高的行人穿越量經常被譽稱為「全世界最大的交叉路口」，其龐大的行人流通量成為許多觀光客參訪與攝影、拍照的聖地，也經常出現在電視或電影劇情中，作為東京的象徵。

## 概要

[Omotesando_spring.jpg](https://zh.wikipedia.org/wiki/File:Omotesando_spring.jpg "fig:Omotesando_spring.jpg")的[欅樹](../Page/欅.md "wikilink")\]\]
澀谷區位於東京的城西地區，與[千代田區](../Page/千代田區.md "wikilink")、[中央區](../Page/中央區_\(東京都\).md "wikilink")、[港區](../Page/港區_\(東京都\).md "wikilink")、[新宿區為](../Page/新宿區.md "wikilink")「[都心](../Page/都心.md "wikilink")5區」。

以終點站[澀谷站為中心的](../Page/澀谷站.md "wikilink")[澀谷地區是東京的](../Page/澀谷.md "wikilink")[副都心之一](../Page/副都心.md "wikilink")，鄰近[新宿站的](../Page/新宿站.md "wikilink")[代代木與](../Page/代代木.md "wikilink")[千駄谷與](../Page/千駄谷.md "wikilink")[新宿的辦公商區](../Page/新宿.md "wikilink")、繁華街連成一氣。此外，鄰近[青山](../Page/青山_\(東京都港區\).md "wikilink")（港區）的[原宿](../Page/原宿.md "wikilink")、[表參道是時尚的中心](../Page/表參道.md "wikilink")，[代官山周辺與](../Page/代官山町.md "wikilink")[惠比壽等地商業設施與時尚相關產業聚集](../Page/惠比壽_\(東京都\).md "wikilink")。澀谷區有[明治神宮與](../Page/明治神宮.md "wikilink")[代代木公園等都心少見的廣大綠地](../Page/代代木公園.md "wikilink")，周辺的[松濤](../Page/松濤.md "wikilink")、[代代木上原是都內有名的高級住宅地](../Page/代代木上原.md "wikilink")。另外，同樣為高級住宅地的[廣尾則是因鄰近](../Page/廣尾_\(澀谷區\).md "wikilink")[麻布地區而興起](../Page/麻布.md "wikilink")。日本演藝圈的經紀公司也都設在澀谷區。

另一方面，區内北部[甲州街道與](../Page/甲州街道.md "wikilink")[京王線沿線的北側地區與](../Page/京王線.md "wikilink")[中野區](../Page/中野區.md "wikilink")、[新宿區組成連密的住宅商業地區](../Page/新宿區.md "wikilink")。

區內的年間平均氣溫約15℃。冬季最低氣溫與[大手町附近相比較低](../Page/大手町.md "wikilink")，冬日日數也較多。

### 代表地域

[Shibuya_District_at_Night_2015-04_(17806976882).jpg](https://zh.wikipedia.org/wiki/File:Shibuya_District_at_Night_2015-04_\(17806976882\).jpg "fig:Shibuya_District_at_Night_2015-04_(17806976882).jpg")齊名，為東亞經濟發達的象徵\]\]
[Shibuya_Ward_Office.jpg](https://zh.wikipedia.org/wiki/File:Shibuya_Ward_Office.jpg "fig:Shibuya_Ward_Office.jpg")

  - 澀谷站周邊
    [東急集團與](../Page/東急集團.md "wikilink")[西武百貨店為首的舊](../Page/西武百貨店.md "wikilink")[四季集團的據點](../Page/四季集團.md "wikilink")，商業活動興盛。[澀谷站](../Page/澀谷站.md "wikilink")[八公口側](../Page/忠犬八公.md "wikilink")[百貨店](../Page/百貨店.md "wikilink")、時尚專門店、飲食店、[咖啡廳](../Page/咖啡廳.md "wikilink")、遊戯設施與[風俗設施密集](../Page/風俗.md "wikilink")，[澀谷公園通](../Page/澀谷公園通.md "wikilink")、[中心街](../Page/澀谷中心街.md "wikilink")、[道玄坂](../Page/道玄坂.md "wikilink")、[西班牙坂](../Page/西班牙坂.md "wikilink")、[巴而可](../Page/巴而可.md "wikilink")、[SHIBUYA
    109等是知名景點](../Page/109百貨.md "wikilink")。

<!-- end list -->

  - 神宮前、表參道（包含[原宿](../Page/原宿.md "wikilink")）
    [表參沿線與周邊聚集許多精品店](../Page/表參.md "wikilink")、服裝店、相關事務所與美容室。2006年「[表參道之丘](../Page/表參道之丘.md "wikilink")」開幕。[明治神宮的新年參拜客是日本第一](../Page/明治神宮.md "wikilink")。
  - 代官山
    代官山是澀谷區中僅次於表參道的時尚重鎮，[舊山手通沿線有許多知名餐廳](../Page/舊山手通.md "wikilink")。
  - 惠比壽
    惠比壽原為下町地區，往JR山手線沿線通勤便利，近年車站周邊辦公大樓增加，白天上班族眾多。1994年開幕的[惠比壽花園廣場帶動此地發展](../Page/惠比壽花園廣場.md "wikilink")。
  - 神泉町、澀谷區澀谷
    兩者都在澀谷站往坂上的方向。有「Bit
    Valley」之稱，聚集許多[IT相關企業](../Page/IT.md "wikilink")。
  - 澀谷區南部的住宅地
    [南平台町](../Page/南平台町.md "wikilink")、[廣尾](../Page/廣尾_\(澀谷區\).md "wikilink")、[松濤等地價在澀谷區中特別高昂](../Page/松濤.md "wikilink")。

<!-- end list -->

  - [甲州街道沿線](../Page/甲州街道.md "wikilink")（京王新線沿線）
    [笹塚](../Page/笹塚.md "wikilink")、[幡谷](../Page/幡谷.md "wikilink")、[初台](../Page/初台.md "wikilink")（本町）等3區。與澀谷相比，前往新宿較為便利。
  - [代代木地域](../Page/代代木.md "wikilink")（小田急線沿線）
    代代木地域指JR代代木站周邊，廣義包含了[代代木上原與](../Page/代代木上原.md "wikilink")[富谷](../Page/富谷.md "wikilink")。最近的鐵道路線是小田急線。宇田川等河川在此地形成谷地，豐富的地形起伏類似鄰近的世田谷區。

## 人口

### 日夜間人口

2005年夜間人口（居住者）為199,280人，區外通勤者與通學生、居住者在內的區內日間人口為542,803人，是夜間人口的2.724倍\[2\]。

## 歷史

1932年（昭和7年），原屬[東京府](../Page/東京府.md "wikilink")[豐多摩郡的](../Page/豐多摩郡.md "wikilink")[澀谷町](../Page/澀谷町_\(東京府\).md "wikilink")、[千駄谷町](../Page/千駄谷町.md "wikilink")、[代代幡町](../Page/代代幡町.md "wikilink")3町編入[東京市成立](../Page/東京市.md "wikilink")「澀谷區」\[3\]\[4\]。

區名是以合併當時發展最佳的澀谷町命名\[5\]。

### 前史

### 澀谷區成立後

  - 1932年（昭和7年）10月1日 -
    豐多摩郡[澀谷町](../Page/澀谷町_\(東京府\).md "wikilink")、[千駄谷町](../Page/千駄谷町.md "wikilink")、[代代幡町編入東京市同時合併成立](../Page/代代幡町.md "wikilink")**澀谷區**
  - 1943年（昭和18年）7月1日 -
    [東京府](../Page/東京府.md "wikilink")、[東京市都政施行](../Page/東京市.md "wikilink")。
  - 1962年（昭和37年）5月10日 -
    依「[住居表示法律](../Page/住居表示.md "wikilink")」，進行町名改正、街區番號整理

## 姊妹、提攜都市

  - [於斯屈達爾區](../Page/於斯屈達爾.md "wikilink")（[土耳其](../Page/土耳其.md "wikilink")[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")）。2005年（[平成](../Page/平成.md "wikilink")17年）9月5日締結「友好都市協定」\[6\]。

## 地域

區內共有80個町丁，是東京23區中第三少的特別區，僅次於荒川區、文京區\[7\]。

  - 幡谷地域
    [笹塚](../Page/笹塚.md "wikilink")、[幡谷](../Page/幡谷.md "wikilink")、[本町](../Page/本町_\(澀谷區\).md "wikilink")
  - 代代木地域
    [上原](../Page/代代木上原.md "wikilink")、[大山町](../Page/大山町_\(澀谷區\).md "wikilink")、[西原](../Page/西原_\(澀谷區\).md "wikilink")、[初台](../Page/初台.md "wikilink")、[元代代木町](../Page/元代代木町.md "wikilink")、[富谷](../Page/富谷.md "wikilink")、[代代木神園町](../Page/代代木神園町.md "wikilink")、[代代木](../Page/代代木.md "wikilink")
  - 千駄谷地域
    [千駄谷](../Page/千駄谷.md "wikilink")、[神宮前](../Page/神宮前_\(澀谷區\).md "wikilink")
  - 大向、惠比壽地域
    [神山町](../Page/神山町_\(澀谷區\).md "wikilink")、[神南](../Page/神南.md "wikilink")、[宇田川町](../Page/宇田川町.md "wikilink")、[松濤](../Page/松濤.md "wikilink")、[神泉町](../Page/神泉町.md "wikilink")、[圓山町](../Page/圓山町_\(澀谷區\).md "wikilink")、[道玄坂](../Page/道玄坂.md "wikilink")、[南平台町](../Page/南平台町.md "wikilink")、[櫻丘町](../Page/櫻丘町_\(澀谷區\).md "wikilink")、[鉢山町](../Page/鉢山町.md "wikilink")、[鶯谷町](../Page/鶯谷町.md "wikilink")、[猿樂町](../Page/猿樂町_\(澀谷區\).md "wikilink")、[代官山町](../Page/代官山.md "wikilink")、[惠比壽西](../Page/惠比壽西.md "wikilink")、[惠比壽南](../Page/惠比壽南.md "wikilink")
  - 冰川、新橋地域
    [澀谷](../Page/澀谷.md "wikilink")、[東](../Page/東_\(澀谷區\).md "wikilink")、[廣尾](../Page/廣尾_\(澀谷區\).md "wikilink")、[惠比壽](../Page/惠比壽_\(澀谷區\).md "wikilink")

<!-- end list -->

  - 隣接自治體
    東 - [港區](../Page/港區_\(東京都\).md "wikilink")
    北 -
    [新宿區](../Page/新宿區.md "wikilink")、[中野區](../Page/中野區.md "wikilink")
    西 -
    [杉並區](../Page/杉並區.md "wikilink")、[世田谷區](../Page/世田谷區.md "wikilink")
    南 -
    [目黑區](../Page/目黑區.md "wikilink")、[品川區](../Page/品川區.md "wikilink")

## 觀光

[Shibuya_Center-Gai.jpg](https://zh.wikipedia.org/wiki/File:Shibuya_Center-Gai.jpg "fig:Shibuya_Center-Gai.jpg")\]\]
[Yoyogi-National-First-Gymnasium-01.jpg](https://zh.wikipedia.org/wiki/File:Yoyogi-National-First-Gymnasium-01.jpg "fig:Yoyogi-National-First-Gymnasium-01.jpg")\]\]

  - [忠犬八公像](../Page/忠犬八公.md "wikilink")
  - [犘艾像](../Page/犘艾像.md "wikilink")
  - [明治神宮](../Page/明治神宮.md "wikilink")
  - [代代木公園](../Page/代代木公園.md "wikilink")
  - [國立代代木競技場](../Page/國立代代木競技場.md "wikilink")
  - [NHK放送中心](../Page/NHK放送中心.md "wikilink")
  - [NHK STUDIO PARK](../Page/NHK_STUDIO_PARK.md "wikilink")
  - [澀谷公園通](../Page/澀谷公園通.md "wikilink")
  - [澀谷中心街](../Page/澀谷中心街.md "wikilink")
  - [西班牙坂](../Page/西班牙坂.md "wikilink")
  - [道玄坂](../Page/道玄坂.md "wikilink")
  - [宮益坂](../Page/宮益坂.md "wikilink")
  - [表參道](../Page/表參道.md "wikilink")
  - [惠比壽公園廣場](../Page/惠比壽公園廣場.md "wikilink")
  - [原宿](../Page/原宿.md "wikilink")
  - [代官山](../Page/代官山.md "wikilink")
  - [澀谷川](../Page/澀谷川.md "wikilink")
  - [宮下公園](../Page/宮下公園.md "wikilink")
  - [鶯住宅](../Page/鶯住宅.md "wikilink")
  - [新宿御苑](../Page/新宿御苑.md "wikilink")

### 代表地標

  - [NHK會館](../Page/NHK會館.md "wikilink")
  - [澀谷Mark City](../Page/澀谷Mark_City.md "wikilink")
  - [QFRONT](../Page/QFRONT.md "wikilink")
  - [澀谷Hikarie](../Page/澀谷Hikarie.md "wikilink")
  - [SHIBUYA109](../Page/109百貨.md "wikilink")
  - [SHIBUYA STREAM](../Page/SHIBUYA_STREAM.md "wikilink")
  - [表參道之丘](../Page/表參道之丘.md "wikilink")
  - [La Foret原宿](../Page/La_Foret原宿.md "wikilink")
  - [東急百貨店本店](../Page/東急百貨店本店.md "wikilink")
  - [Bunkamura](../Page/Bunkamura.md "wikilink")
  - [Cerulean Tower](../Page/Cerulean_Tower.md "wikilink")
  - [澀谷Cross Tower](../Page/澀谷Cross_Tower.md "wikilink")
  - [新國立劇場](../Page/新國立劇場.md "wikilink")
  - [惠比壽公園廣場](../Page/惠比壽公園廣場.md "wikilink")
  - [澀谷公會堂](../Page/澀谷公會堂.md "wikilink")
  - [SHIBUYA-AX](../Page/SHIBUYA-AX.md "wikilink")
  - [Shibuya O-EAST](../Page/Shibuya_O-EAST.md "wikilink")／[Shibuya
    O-WEST](../Page/Shibuya_O-WEST.md "wikilink")
  - [高島屋時代廣場](../Page/高島屋時代廣場.md "wikilink")（JR[新宿站新南口附近](../Page/新宿站.md "wikilink")。）
  - [新宿Southern
    Terrace](../Page/新宿Southern_Terrace.md "wikilink")（JR新宿站南口。小田急Southern
    Terrace。）
  - [東急廣場表參道原宿](../Page/原宿中央公寓.md "wikilink")

## 區政

  - 區長：桑原敏武
  - 任期：2003年4月27日 - 2015年4月26日（第三任）

### 澀谷區議會

  - 席次：34人
  - 任期：2011年5月1日 - 2015年4月30日

#### 會派構成

（2013年5月20日現在）

| 會派名           | 略稱    | 人數 |
| ------------- | ----- | -- |
| 澀谷區議會自由民主黨議員團 | 自由民主黨 | 8  |
| 澀谷區議會公明黨      | 公明黨   | 6  |
| 日本共產黨澀谷區議會議員團 | 日本共産黨 | 6  |
| 民主黨澀谷區議團      | 民主黨   | 5  |
| 無所屬俱樂部        | 無所属ク  | 3  |
| 新民主澀谷         | 新民主澀谷 | 2  |
| 志士之會          | 志士の會  | 2  |
| （無所屬）         |       | 2  |
|               |       |    |

## 國政、都政

### 眾議院議員

澀谷區與中野區屬[東京都第7區](../Page/東京都第7區.md "wikilink")。

  - 選出議員
      - [長妻昭](../Page/長妻昭.md "wikilink")（[民主黨](../Page/民主黨_\(日本\).md "wikilink")）

### 東京都議會

澀谷區為單一選舉區，選出2名。

  - 澀谷區選出議員
      - 大津浩子（無所屬）
      - 村上英子（自由民主黨）

## 产业

### 位于涩谷区的大型企業

  - [东日本旅客铁道](../Page/东日本旅客铁道.md "wikilink")（JR东日本）
  - [东京急行电铁](../Page/东京急行电铁.md "wikilink")
  - [札幌啤酒](../Page/札幌啤酒.md "wikilink")
  - [可爾必思](../Page/可爾必思.md "wikilink")（CALPIS）
  - [伊藤园](../Page/伊藤园.md "wikilink")
  - [卡西歐](../Page/卡西歐.md "wikilink") （CASIO）
  - [SECOM](../Page/SECOM.md "wikilink")
  - 日本[微軟](../Page/微軟.md "wikilink")
  - [SQUARE ENIX](../Page/SQUARE_ENIX.md "wikilink")
  - [OSCAR PROMOTION](../Page/OSCAR_PROMOTION.md "wikilink")
  - [剧团向日葵](../Page/剧团向日葵.md "wikilink")
  - [日本放送协会](../Page/日本放送协会.md "wikilink")（NHK）
  - 日本[安利](../Page/安利.md "wikilink")
  - 日本[可口可乐](../Page/可口可乐公司.md "wikilink")
  - [麒麟啤酒](../Page/麒麟啤酒.md "wikilink")
  - [星巴克日本](../Page/星巴克.md "wikilink")
  - [爱和谊日生同和损害保险](../Page/爱和谊日生同和损害保险.md "wikilink")
  - [辉瑞日本](../Page/辉瑞.md "wikilink")
  - [pixiv](../Page/pixiv.md "wikilink")

## 交通

### 道路

  - 國道、都道
      - 放射道路
          - 北側起
              - [方南通](../Page/方南通.md "wikilink")
              - [水道道路](../Page/水道道路.md "wikilink")
              - [國道20號](../Page/國道20號.md "wikilink")（甲州街道）
              - [井之頭通](../Page/井之頭通.md "wikilink")
              - [國道246號](../Page/國道246號.md "wikilink")（青山通、玉川通）
              - [駒澤通](../Page/駒澤通.md "wikilink")
      - 環狀道路
          - 都心側起
              - [外苑西通](../Page/外苑西通.md "wikilink")
              - [明治通](../Page/明治通_\(東京都\).md "wikilink")
              - [山手通](../Page/東京都道317號環狀六號線.md "wikilink")
              - [中野通](../Page/中野通.md "wikilink")
  - 高速道路
      - [首都高速3號澀谷線](../Page/首都高速3號澀谷線.md "wikilink")
      - [澀谷出入口](../Page/澀谷出入口.md "wikilink") -
        [大橋JCT](../Page/大橋JCT.md "wikilink")
      - [首都高速4號新宿線](../Page/首都高速4號新宿線.md "wikilink")
      - [外苑出入口（僅入口）](../Page/外苑出入口.md "wikilink") -
        [代代木PA](../Page/代代木PA.md "wikilink") -
        [新宿出入口](../Page/新宿出入口.md "wikilink")（新宿區）-
        [西新宿JCT](../Page/西新宿JCT.md "wikilink") -
        [初台出入口](../Page/初台出入口.md "wikilink") -
        [幡谷出入口](../Page/幡谷出入口.md "wikilink")
      - [首都高速道路中央環狀新宿線](../Page/首都高速中央環狀線.md "wikilink")

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）

<!-- end list -->

  - [JR_JY_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JY_line_symbol.svg "fig:JR_JY_line_symbol.svg")
    [山手線](../Page/山手線.md "wikilink")：[代代木站](../Page/代代木站.md "wikilink")
    - [原宿站](../Page/原宿站.md "wikilink") -
    [澀谷站](../Page/澀谷站.md "wikilink") -
    [惠比壽站](../Page/惠比壽站.md "wikilink")
  - [JR_JB_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JB_line_symbol.svg "fig:JR_JB_line_symbol.svg")
    [中央·總武緩行線](../Page/中央·總武緩行線.md "wikilink")：[代代木站](../Page/代代木站.md "wikilink")
    - [千駄谷站](../Page/千駄谷站.md "wikilink")
  - [JR_JA_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JA_line_symbol.svg "fig:JR_JA_line_symbol.svg")
    [埼京線](../Page/埼京線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink") -
    [惠比壽站](../Page/惠比壽站.md "wikilink")
  - [JR_JS_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JS_line_symbol.svg "fig:JR_JS_line_symbol.svg")
    [湘南新宿線](../Page/湘南新宿線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink")
    - [惠比壽站](../Page/惠比壽站.md "wikilink")

<!-- end list -->

  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")

<!-- end list -->

  - [Subway_TokyoGinza.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoGinza.png "fig:Subway_TokyoGinza.png")
    [銀座線](../Page/銀座線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink")
  - [Subway_TokyoHibiya.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoHibiya.png "fig:Subway_TokyoHibiya.png")
    [日比谷線](../Page/日比谷線.md "wikilink")：[惠比壽站](../Page/惠比壽站.md "wikilink")
  - [Subway_TokyoChiyoda.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoChiyoda.png "fig:Subway_TokyoChiyoda.png")
    [千代田線](../Page/千代田線.md "wikilink")：[明治神宮前站](../Page/明治神宮前站.md "wikilink")
    - [代代木公園站](../Page/代代木公園站.md "wikilink") -
    [代代木上原站](../Page/代代木上原站.md "wikilink")
  - [Subway_TokyoHanzomon.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoHanzomon.png "fig:Subway_TokyoHanzomon.png")
    [半藏門線](../Page/半藏門線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink")
  - [Subway_TokyoFukutoshin.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoFukutoshin.png "fig:Subway_TokyoFukutoshin.png")
    [副都心線](../Page/副都心線.md "wikilink")：[北參道站](../Page/北參道站.md "wikilink")
    - [明治神宮前站](../Page/明治神宮前站.md "wikilink") -
    [澀谷站](../Page/澀谷站.md "wikilink")

<!-- end list -->

  - [東京都交通局](../Page/東京都交通局.md "wikilink")（[都營地下鐵](../Page/都營地下鐵.md "wikilink")）

<!-- end list -->

  - [Subway_TokyoOedo.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoOedo.png "fig:Subway_TokyoOedo.png")
    [大江戶線](../Page/大江戶線.md "wikilink")：[新宿站](../Page/新宿站.md "wikilink")
    - [代代木站](../Page/代代木站.md "wikilink") -
    [國立競技場站](../Page/國立競技場站.md "wikilink")

<!-- end list -->

  - [東京急行電鐵](../Page/東京急行電鐵.md "wikilink")

<!-- end list -->

  - [Tokyu_TY_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_TY_line_symbol.svg "fig:Tokyu_TY_line_symbol.svg")
    [東橫線](../Page/東橫線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink") -
    [代官山站](../Page/代官山站.md "wikilink")
  - [Tokyu_DT_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_DT_line_symbol.svg "fig:Tokyu_DT_line_symbol.svg")
    [田園都市線](../Page/田園都市線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink")

<!-- end list -->

  - [京王電鐵](../Page/京王電鐵.md "wikilink")

<!-- end list -->

  - [Number_prefix_Keio-Inokashira-line.svg](https://zh.wikipedia.org/wiki/File:Number_prefix_Keio-Inokashira-line.svg "fig:Number_prefix_Keio-Inokashira-line.svg")
    [井之頭線](../Page/井之頭線.md "wikilink")：[澀谷站](../Page/澀谷站.md "wikilink")
    - [神泉站](../Page/神泉站.md "wikilink")
  - [Number_prefix_Keio-line.svg](https://zh.wikipedia.org/wiki/File:Number_prefix_Keio-line.svg "fig:Number_prefix_Keio-line.svg")
    [京王線](../Page/京王線.md "wikilink")：[笹塚站](../Page/笹塚站.md "wikilink")
  - [Number_prefix_Keio-line.svg](https://zh.wikipedia.org/wiki/File:Number_prefix_Keio-line.svg "fig:Number_prefix_Keio-line.svg")
    [京王新線](../Page/京王新線.md "wikilink")：[初台站](../Page/初台站.md "wikilink")
    - [幡谷站](../Page/幡谷站.md "wikilink") -
    [笹塚站](../Page/笹塚站.md "wikilink")

<!-- end list -->

  - [小田急電鐵](../Page/小田急電鐵.md "wikilink")

<!-- end list -->

  - [Odakyu_odawara_logo.svg](https://zh.wikipedia.org/wiki/File:Odakyu_odawara_logo.svg "fig:Odakyu_odawara_logo.svg")
    [小田原線](../Page/小田原線.md "wikilink")：[南新宿站](../Page/南新宿站.md "wikilink")
    - [參宮橋站](../Page/參宮橋站.md "wikilink") -
    [代代木八幡站](../Page/代代木八幡站.md "wikilink") -
    [代代木上原站](../Page/代代木上原站.md "wikilink")

### 巴士

  - [都營巴士](../Page/都營巴士.md "wikilink")
      - 都01、都06、學03、學06、出入01、澀66、早81、池86、田87
  - [東急巴士](../Page/東急巴士.md "wikilink")
      - 澀05、澀11、澀12、澀21、澀23、澀24、澀31、澀32、澀33、澀34、澀41、澀51、澀52、澀55、澀71、澀72、澀82、惠32、TRANSSÉS、NHK直行、八公巴士（惠比壽、代官山循環「晚霞線」）、午夜箭號（高速青葉台線、高速新橫濱線、高速新市鎮線、高速二子玉川線）、澀谷空港線、City
        Shuttle
  - [京王巴士東](../Page/京王巴士東.md "wikilink")
      - 澀61、澀64、澀66、澀67、澀68、宿41、宿45、宿51、八公巴士（本町、笹塚循環「春之小川線」、「越過山丘線」）、NHK直行
  - [富士快線](../Page/富士快線.md "wikilink")
      - 八公巴士（神宮前、千駄谷循環「神宮之杜線」

## 機關、設施

### 警察

  - [澀谷警察署](../Page/澀谷警察署.md "wikilink")（澀谷3-8-15）
  - [原宿警察署](../Page/原宿警察署.md "wikilink")（神宮前1-4-17）
  - [代代木警察署](../Page/代代木警察署.md "wikilink")（本町1-11-3）

### 消防

第三消防方面消防救助機動部隊

  -   - 澀谷消防署（神南1-8-3）指揮隊、幫浦隊2（特別消火中隊）、梯隊、救急隊2
          - 惠比壽出張所（惠比壽4-19-24）幫浦隊1、救急隊1
          - 松濤出張所（松濤1-25-14）幫浦隊1、救急隊1
          - 代代木出張所（本町1-6-6）幫浦隊2、救急隊1（消防活動二輪車2）
          - 富谷出張所（富谷1-29-17）幫浦隊2、救急隊1
          - 原宿出張所（代代木1-2-15）幫浦隊1、救急隊1

  - 東京消防廳消防技術安全所（幡谷1-13-20）

  - [東京消防廳消防學校](../Page/東京消防廳消防學校.md "wikilink")（西原2-51-1）

### 醫療

  - 井上醫院（富谷1-53-8）
  - 十字醫院（幡谷2-18-20）
  - [JR東京總合醫院](../Page/JR東京總合醫院.md "wikilink")（代代木2-1-3）
  - [東海大學醫學部附屬東京醫院](../Page/東海大學醫學部附屬東京醫院.md "wikilink")（代代木1-2-5）
  - [東京都立廣尾醫院](../Page/東京都立廣尾醫院.md "wikilink")（惠比壽2-34-10）
  - [日本紅十字社醫療中心](../Page/日本紅十字社醫療中心.md "wikilink")（廣尾4-1-22）
  - [初台復健醫院](../Page/初台復健醫院.md "wikilink")（本町3-53-3）
  - [代代木醫院](../Page/代代木醫院.md "wikilink")（千駄谷1-30-7）

### 文化設施

#### 博物館

  - [刀劍博物館](../Page/刀劍博物館.md "wikilink")（代代木4-25-10）
  - [白根紀念澀谷區鄉土博物館、文學館](../Page/白根紀念澀谷區鄉土博物館、文學館.md "wikilink")（東4-9-1）

#### 美術館

  - [澀谷區立松濤美術館](../Page/澀谷區立松濤美術館.md "wikilink")（松濤2-14-14）

#### 天文館

  - [澀谷區立天文館澀谷](../Page/澀谷區立天文館澀谷.md "wikilink")（櫻丘町23-21
    [澀谷區文化總合中心大和田](../Page/澀谷區文化總合中心大和田.md "wikilink")12樓）

#### 圖書館

  - [澀谷區立中央圖書館](../Page/澀谷區立中央圖書館.md "wikilink")（神宮前1-4-1）
  - 澀谷區立西原圖書館（西原2-28-9）
  - 澀谷區立澀谷圖書館（東1-6-6）
  - 澀谷區立本町圖書館（本町1-33-5）
  - 澀谷區立富谷圖書館（上原1-46-2）
  - [澀谷區立笹塚圖書館](../Page/澀谷區立笹塚圖書館.md "wikilink")（笹塚1-27-1）
  - 澀谷區立臨川大家的圖書館（廣尾1-9-17）
  - 澀谷區立代代木圖書館（代代木3-51-8）
  - 澀谷區立こもれび大和田圖書館（櫻丘町23-21 澀谷區文化總合中心大和田2樓）
  - 澀谷區立笹塚兒童圖書館（笹塚3-3-1）

## 学校

[Aoyama_Gakuin_Majima_Memorial_Hall.JPG](https://zh.wikipedia.org/wiki/File:Aoyama_Gakuin_Majima_Memorial_Hall.JPG "fig:Aoyama_Gakuin_Majima_Memorial_Hall.JPG")\]\]

### 大学

  - [青山学院大学](../Page/青山学院大学.md "wikilink")
  - [国学院大学](../Page/国学院大学.md "wikilink")
  - [东海大学](../Page/东海大学_\(日本\).md "wikilink")
  - [圣心女子大学](../Page/圣心女子大学.md "wikilink")
  - [文化女子大學](../Page/文化女子大學.md "wikilink")
  - [山崎學園大學](../Page/山崎學園大學.md "wikilink")
  - [日本經濟大學澀谷校區](../Page/日本經濟大學.md "wikilink")

### 高等學校

  - 都立
      - [青山高等學校](../Page/東京都立青山高等學校.md "wikilink")
      - [新宿高等學校](../Page/東京都立新宿高等學校.md "wikilink")
      - [廣尾高等學校](../Page/東京都立廣尾高等學校.md "wikilink")
      - [第一商業高等學校](../Page/東京都立第一商業高等學校.md "wikilink")
  - 私立（含中高一貫校）
      - [青山學院中等部、高等部](../Page/青山學院中等部、高等部.md "wikilink")
      - [青森山田高等學校東京校](../Page/青森山田高等學校.md "wikilink")
      - [鹿島學園高等學校原宿連攜校區](../Page/鹿島學園高等學校.md "wikilink")
      - [關東國際高等學校](../Page/關東國際高等學校.md "wikilink")
      - [國學院高等學校](../Page/國學院高等學校.md "wikilink")
      - [澀谷教育學園澀谷中學校、高等學校](../Page/澀谷教育學園澀谷中學校、高等學校.md "wikilink")
      - [實踐女子學園高等學校](../Page/實踐女子學園高等學校.md "wikilink")
      - [東海大學附屬望星高等學校](../Page/東海大學附屬望星高等學校.md "wikilink")
      - [東京女學館中學校、高等學校](../Page/東京女學館中學校、高等學校.md "wikilink")
      - [富士見丘中學校、高等學校](../Page/富士見丘中學校、高等學校.md "wikilink")

### 小中一貫教育校

  - 區立

<!-- end list -->

  - [澀谷區立澀谷本町學園](../Page/澀谷區立澀谷本町學園.md "wikilink")

### 中學校

  - 區立

<!-- end list -->

  - 澀谷區立上原中學校
  - [澀谷區立笹塚中學校](../Page/澀谷區立笹塚中學校.md "wikilink")
  - [澀谷區立松濤中學校](../Page/澀谷區立松濤中學校.md "wikilink")
  - [澀谷區立鉢山中學校](../Page/澀谷區立鉢山中學校.md "wikilink")
  - [澀谷區立原宿外苑中學校](../Page/澀谷區立原宿外苑中學校.md "wikilink")
  - [澀谷區立廣尾中學校](../Page/澀谷區立廣尾中學校.md "wikilink")
  - 澀谷區立代代木中學校

### 小學校

  - 區立

<!-- end list -->

  - [澀谷區立上原小學校](../Page/澀谷區立上原小學校.md "wikilink")
  - 澀谷區立加計塚小學校
  - 澀谷區立笹塚小學校
  - 澀谷區立猿樂小學校
  - [澀谷區立神宮前小學校](../Page/澀谷區立神宮前小學校.md "wikilink")
  - 澀谷區立神南小學校
  - 澀谷區立千駄谷小學校
  - 澀谷區立常磐松小學校
  - 澀谷區立富谷小學校
  - 澀谷區立中幡小學校
  - [澀谷區立西原小學校](../Page/澀谷區立西原小學校.md "wikilink")
  - [澀谷區立長谷戶小學校](../Page/澀谷區立長谷戶小學校.md "wikilink")
  - 澀谷區立幡代小學校
  - 澀谷區立鳩森小學校
  - 澀谷區立廣尾小學校
  - 澀谷區立山谷小學校
  - 澀谷區立代代木小學校
  - 澀谷區立臨川小學校

<!-- end list -->

  - 私立

<!-- end list -->

  - [青山學院初等部](../Page/青山學院初等部.md "wikilink")
  - [東京女學館小學校](../Page/東京女學館小學校.md "wikilink")

### 專門學校

  - [桑澤設計研究所](../Page/桑澤設計研究所.md "wikilink")
  - [國際文化理容美容專門學校澀谷校](../Page/國際文化理容美容專門學校澀谷校.md "wikilink")
  - [帝京醫學技術專門學校](../Page/帝京醫學技術專門學校.md "wikilink")
  - [日本寫真藝術專門學校](../Page/日本寫真藝術專門學校.md "wikilink")
  - [日本設計師學院](../Page/日本設計師學院.md "wikilink")
  - [服部榮養專門學校](../Page/服部榮養專門學校.md "wikilink")
  - [文化服裝學院](../Page/文化服裝學院.md "wikilink")
  - [美好年代美容專門學校](../Page/美好年代美容專門學校.md "wikilink")
  - [MUSE音樂院](../Page/MUSE音樂院.md "wikilink")

### 未認可校

  - [代代木動畫學院東京本部校](../Page/代代木動畫學院.md "wikilink")

### 預備校

  - [代代木Seminar](../Page/代代木Seminar.md "wikilink")

## 放送

  - [NHK放送中心](../Page/NHK放送中心.md "wikilink")
  - [SHIBUYA-FM](../Page/東京通訊放送.md "wikilink") - 澀谷區的地區廣播

<!-- end list -->

  - 攝影棚

<!-- end list -->

  - [大家的廣場接觸會館](../Page/大家的廣場接觸會館.md "wikilink") - NHK
  - [TOKYO FM 西班牙坂工作室](../Page/TOKYO_FM_西班牙坂工作室.md "wikilink") - [TOKYO
    FM](../Page/FM東京.md "wikilink")
  - [STUDIO ViViD](../Page/STUDIO_ViViD.md "wikilink") -
    [FM-FUJI](../Page/FM富士.md "wikilink")
  - 青山學院攝影棚 - 1樓是NHK的衛星攝影棚「NHK＠Campus」。
  - WOWOW澀谷工作站 - [WOWOW](../Page/WOWOW.md "wikilink")

## 大使館

  - [土耳其大使館](../Page/土耳其.md "wikilink")

  - [阿富汗大使館](../Page/阿富汗.md "wikilink")

  - [阿拉伯聯合大公國大使館](../Page/阿拉伯聯合大公國.md "wikilink")

  - [越南大使館](../Page/越南.md "wikilink")

  - [愛沙尼亞大使館](../Page/愛沙尼亞.md "wikilink")

<!-- end list -->

  - [阿曼大使館](../Page/阿曼.md "wikilink")

  - [幾內亞大使館](../Page/幾內亞.md "wikilink")

  - [克羅埃西亞大使館](../Page/克羅埃西亞.md "wikilink")

  - [科特迪瓦大使館](../Page/科特迪瓦.md "wikilink")

  - [捷克大使館](../Page/捷克.md "wikilink")

<!-- end list -->

  - [丹麥大使館](../Page/丹麥.md "wikilink")

  - [紐西蘭大使館](../Page/紐西蘭.md "wikilink")

  - [保加利亞大使館](../Page/保加利亞.md "wikilink")

  - [布吉納法索大使館](../Page/布吉納法索.md "wikilink")

  - [秘魯大使館](../Page/秘魯.md "wikilink")

<!-- end list -->

  - [馬來西亞大使館](../Page/馬來西亞.md "wikilink")

  - [蒙古大使館](../Page/蒙古.md "wikilink")

  - [利比亞人民事務所](../Page/利比亞.md "wikilink")

  - [拉脫維亞大使館](../Page/拉脫維亞.md "wikilink")

## 出身名人

  - [橋本龍太郎](../Page/橋本龍太郎.md "wikilink") -
    [内閣總理大臣](../Page/内閣總理大臣.md "wikilink")
  - [橋下徹](../Page/橋下徹.md "wikilink") -
    [大阪府知事](../Page/大阪府知事.md "wikilink")、[大阪市長](../Page/大阪市長.md "wikilink")
  - [金子修介](../Page/金子修介.md "wikilink") -
    [電影導演](../Page/電影導演.md "wikilink")
  - [原哲夫](../Page/原哲夫.md "wikilink") - [漫畫家](../Page/漫畫家.md "wikilink")
  - [吉永小百合](../Page/吉永小百合.md "wikilink") - 女演員
  - [瀨川瑛子](../Page/瀨川瑛子.md "wikilink") -
    [演歌歌手](../Page/演歌.md "wikilink")
  - [小暮閣下](../Page/小暮閣下.md "wikilink") -
    [音樂家](../Page/音樂家.md "wikilink")、[藝人](../Page/藝人.md "wikilink")
  - [小森真奈美](../Page/小森真奈美.md "wikilink") - 廣播主持人、聲優、隨筆家
  - [杏](../Page/杏_\(模特兒\).md "wikilink") - 女演員
  - [速水茂虎道](../Page/速水茂虎道.md "wikilink") - 演員
  - [岡田將生](../Page/岡田將生.md "wikilink") - 演員
  - [岡政偉](../Page/岡政偉.md "wikilink") - 美國演員
  - [小出惠介](../Page/小出惠介.md "wikilink") - 演員

## 相關人物

### 居住者

  - [麻生太郎](../Page/麻生太郎.md "wikilink") -
    [政治家](../Page/政治家.md "wikilink")、[首相](../Page/首相.md "wikilink")
  - [安倍晋三](../Page/安倍晋三.md "wikilink") - 政治家、首相
  - [安倍晋太郎](../Page/安倍晋太郎.md "wikilink") - 政治家
  - [飯島愛](../Page/飯島愛.md "wikilink") - [藝人](../Page/藝人.md "wikilink")
  - [小田和正](../Page/小田和正.md "wikilink") -
    [音樂家](../Page/音樂家.md "wikilink")、[Off
    Course團長](../Page/Off_Course.md "wikilink")
  - [岸信介](../Page/岸信介.md "wikilink") - 政治家、首相
  - [小池徹平](../Page/小池徹平.md "wikilink") -
    [WaT](../Page/WaT.md "wikilink")

## 澀谷區作品

### 電影

  - 《ファンキーハットの快男児》
  - 《[摩斯拉](../Page/摩斯拉_\(1961年\).md "wikilink")》
  - 《[卡美拉3 邪神覺醒](../Page/卡美拉3_邪神覺醒.md "wikilink")》
  - 《[哥吉拉×美加基拉斯 G消滅作戰](../Page/哥吉拉×美加基拉斯_G消滅作戰.md "wikilink")》
  - 《ラブ&ポップ》
  - 《[日本沉沒](../Page/日本沉沒.md "wikilink")》
  - 《ポルノスター》
  - 《凶気の桜》
  - 《[花水木](../Page/花水木_\(電影\).md "wikilink")》
  - 《[怪物的孩子](../Page/怪物的孩子.md "wikilink")》

### 連續劇

  - 《[超人七號](../Page/超人七號.md "wikilink")》
  - 《傷だらけの天使》
  - 《[月之戀人](../Page/月之戀人.md "wikilink")》
  - 《[零秒出手](../Page/零秒出手.md "wikilink")》
  - 《[率直男人](../Page/率直男人.md "wikilink")》
  - 《[辣妹掌門人](../Page/辣妹掌門人_\(日本電視劇\).md "wikilink")》
  - 《[Sh15uya](../Page/Sh15uya.md "wikilink")》

### 綜藝節目

  - 《[八點！全員集合](../Page/八點！全員集合.md "wikilink")》 - 澀谷公會堂公開現場直播。
  - 《NTV紅白歌のベストテン》 → 《ザ・トップテン》 → 《歌のトップテン》 - 澀谷公會堂公開現場直播。
  - 《\[TOKYO午後3時》 - 澀谷PARCO現場直播。
  - 《今田耕司のシブヤ系うらりんご》 - [澀谷公園通劇場每日現場直播](../Page/澀谷公園通劇場.md "wikilink")。
  - 《Harajukuロンチャーズ》 - 原宿每日現場直播。
  - 《ブラタモリ》 - 2008.12.13[塔摩利拿著舊地圖在原宿附近散步](../Page/塔摩利.md "wikilink")。
  - 《ちい散歩》　-
    [地井武男在區內散步](../Page/地井武男.md "wikilink")、[矢島悠子也在](../Page/矢島悠子.md "wikilink")《流行もの散歩
    矢島が行く》中造訪澀谷（2009.4.9）。

### 遊戲

  - 《街～命運的路口～》
  - 《[這個美妙的世界](../Page/這個美妙的世界.md "wikilink")》
  - 《[CHAOS;HEAD](../Page/CHAOS;HEAD.md "wikilink")》
  - 《[428：被封鎖的澀谷](../Page/428：被封鎖的澀谷.md "wikilink")》
  - 《[Jet Set Radio](../Page/Jet_Set_Radio.md "wikilink")》
  - 《[CHAOS;CHILD](../Page/CHAOS;CHILD.md "wikilink")》

### 漫畫

  - 《[辣妹當家](../Page/辣妹當家.md "wikilink")》
  - 《澀谷區圓山町》
  - 《パギャル\!》
  - 《サムライソルジャー》
  - 《[終結的熾天使](../Page/終結的熾天使.md "wikilink")》

### 音樂

  - 《違う、そうじゃない/渋谷で5時|渋谷で5時》 -
    [鈴木雅之單曲](../Page/鈴木雅之_\(歌手\).md "wikilink")。1994年發行。
  - 《アメリカ橋》 - 狩人（1979年）與山川豐的歌曲（1998年）（同名異曲）。
  - [澀谷系](../Page/澀谷系.md "wikilink") - 90年代以流行西洋音樂為志向的日本音樂總稱。

## 活動

  - [澀谷DEどーも](../Page/澀谷DEどーも.md "wikilink") - 每年5月、NHK放送中心
  - [澀谷、鹿兒島小原祭](../Page/澀谷、鹿兒島小原祭.md "wikilink") - 每年5月、Bunkamura通、道玄坂
  - [神宮外苑花火大會](../Page/神宮外苑花火大會.md "wikilink") - 每年8月、明治神宮外苑
  - [原宿表餐道元氣祭、Super
    YOSAKOI](../Page/原宿表餐道元氣祭、Super_YOSAKOI.md "wikilink")
    - 每年8月
  - [澀谷區區民廣場 家鄉澀谷節](../Page/澀谷區區民廣場_家鄉澀谷節.md "wikilink") -
    每年11月、代代木公園活動廣場等

## 澀谷區特產、紀念品

  - 八公醬

## 車牌號碼

目黑區屬品川號碼（[東京運輸支局本廳舍](../Page/東京運輸支局.md "wikilink")）。

  - 品川號碼區域

<!-- end list -->

  - 中央區、千代田區、港區、品川區、目黑區、大田區、世田谷區、澀谷區與島嶼部[1](http://wwwtb.mlit.go.jp/kanto/s_tokyo/map_riku.html)。

## 註釋

## 參考文獻

  -
  -
  -
  -
## 外部連結

  - [澀谷區網站](http://www.city.shibuya.tokyo.jp/)

  - [澀谷區網站英文網頁](http://www.city.shibuya.tokyo.jp/eng/index.html)

[\*](../Category/澀谷區.md "wikilink")

1.  「」常見的譯名有兩種：「澀谷」與「涉谷」。「」原本所對應的傳統汉字寫法為「**-{澁}-**谷」；按照日本漢字簡化規則，「-{澁}-」的簡化字為「」。「」字下的四個小點即代表上面的「止」字重覆2次（「」亦同此理，為「-{zh-hant:攝;zh-hans:攝(摄)}-」的簡化字）。在中文汉字裡，「-{澁}-」為「-{zh-hant:澀;zh-hans:澀(涩)}-」的[異體字](../Page/異體字.md "wikilink")。「涉谷」此譯名則是因為字形相似而來（但「涉」的對應日本簡化漢字實為「-{渉}-」，而日語平假名拼寫為わたる，羅馬字為Wataru）。而在使用狀況方面，大部分具有官方地位的日本單位與網站在其中文版訊息中，大都使用「澀谷」；而「涉谷」則幾乎都是一般民間用戶以訛傳訛的[錯別字用法](../Page/錯別字.md "wikilink")。
2.  東京都編集『東京都の昼間人口2005』平成20年発行132,133ページ　国勢調査では年齢不詳のものが東京都だけで16万人いる。上のグラフには年齢不詳のものを含め、昼夜間人口に関しては年齢不詳の人物は数字に入っていないの数字の間に誤差は生じる
3.
4.  澀谷町、千駄谷町、代代幡町各別的町域相當於現在的[澀谷警察署](../Page/澀谷警察署.md "wikilink")、[原宿警察署](../Page/原宿警察署.md "wikilink")、[代代木警察署管轄地區](../Page/代代木警察署.md "wikilink")
5.  [澀谷區の変遷と由来等](http://www.e-koto.co.jp/useful/chimei_shibuya/p498.html)

6.  [トルコ共和国イスタンブール市ウスキュダル区との友好交流](http://www.city.shibuya.tokyo.jp/kusei/pi428/uskudar/index.html)
     渋谷区役所、平成23年5月5日閲覧
7.  典拠、東京都総務局統計部人口統計課　編集・発行「住民基本台帳による東京都の世帯と人口」平成22年1月分、平成22年3月発行、P.22より