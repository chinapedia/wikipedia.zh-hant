**布拉加**（[葡萄牙语](../Page/葡萄牙语.md "wikilink")：****）為[葡萄牙](../Page/葡萄牙.md "wikilink")[布拉加区的首府](../Page/布拉加区.md "wikilink")，葡萄牙最重要的城市之一。

## 歷史

布拉加約建於西元前296年。在[羅馬人時代](../Page/羅馬人.md "wikilink")，布拉加曾是[伊比利亞省的大都會之一](../Page/伊比利亞省.md "wikilink")。西元716年被[摩爾人占領](../Page/摩爾人.md "wikilink")。12世紀初併入[葡萄牙王國](../Page/葡萄牙.md "wikilink")。

直至十六世紀時代，布拉加一直都處於低微的地位。直到後來由一個叫Diogo de
Sousa的人開始重建布拉加，包括擴闊街道、興建廣場、廣設醫院和新教堂等等。這局面使布拉加的面貌煥然一新，直到今日依然可窺見其當時風貌。

現在，其商業和工業均有巨大發展，許多紀念性建築物值得駐足。

## 景點

  - 主教堂（十世紀以前）
  - 城堡（十三世紀）
  - 聖母教堂（十四世紀）
  - 大主教府（十六世紀）
  - 比斯卡伊尼奧斯宮（十六世紀）
  - 耶穌聖堂（十八世紀）
  - 和薩梅羅聖堂（十九世紀）
  - [布拉加市政球場](../Page/布拉加足球俱樂部.md "wikilink")

## 友好城市

  - [西班牙](../Page/西班牙.md "wikilink")[阿斯托加](../Page/阿斯托加.md "wikilink")

  - [几内亚比绍](../Page/几内亚比绍.md "wikilink")[比索朗](../Page/比索朗.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[克莱蒙费朗](../Page/克莱蒙费朗.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[皮托](../Page/皮托.md "wikilink")

  - [巴西](../Page/巴西.md "wikilink")[圣安德烈](../Page/圣安德烈_\(巴西\).md "wikilink")

  - [佛得角](../Page/佛得角.md "wikilink")[圣尼古拉岛](../Page/圣尼古拉岛.md "wikilink")

## 外部链接

  - [布拉加市網站](http://www.braga.com.pt/)

[B](../Category/葡萄牙城市.md "wikilink")