**侵华日军**，是[中国抗日战争期间所有参与](../Page/中国抗日战争.md "wikilink")[侵略中国的](../Page/侵略.md "wikilink")[日本军各](../Page/日本军.md "wikilink")[部隊的泛称](../Page/部隊.md "wikilink")，先后包括[中国驻屯军](../Page/中国驻屯军.md "wikilink")、[关东军](../Page/关东军.md "wikilink")、[华北方面军](../Page/华北方面军.md "wikilink")（北支那方面军）、[上海派遣军](../Page/上海派遣军.md "wikilink")、[华中方面军](../Page/华中方面军.md "wikilink")（中支那方面军）、[驻蒙军](../Page/驻蒙军.md "wikilink")、[华南方面军](../Page/华南方面军.md "wikilink")（南支那方面军）、[中国派遣军](../Page/中国派遣军.md "wikilink")（支那派遣军）、参加攻击中国东南沿海和[长江沿岸的](../Page/长江.md "wikilink")[大日本帝国海军部分军队](../Page/大日本帝国海军.md "wikilink")。

## 历史

1937年7月以前（[七七事变前](../Page/七七事变.md "wikilink")），日本侵略军在中国境内只有两支部队：关东军和中国驻屯军。1937年8月31日，日军撤销中国驻屯军[番号](../Page/番号.md "wikilink")，编成华北方面军，辖第1军和第2军。

1937年9月11日，组建上海派遣军，[松井石根任司令官](../Page/松井石根.md "wikilink")。10月7日，[第16师团转隶上海派遣军](../Page/第16师团.md "wikilink")。

1937年10月29日，组建[华中方面军](../Page/华中方面军.md "wikilink")，松井石根担任司令官，将上海派遣军（[第3师团](../Page/第3师团.md "wikilink")、[第9师团](../Page/第9师团.md "wikilink")、[第11师团](../Page/第11师团.md "wikilink")、[第13师团](../Page/第13师团.md "wikilink")、[第101师团](../Page/第101师团.md "wikilink")、[第16师团](../Page/第16师团.md "wikilink")）和新从日本本土调来的10月20日刚刚编成的[第10軍](../Page/第10軍_\(日本陸軍\).md "wikilink")（[第6师团](../Page/第6师团.md "wikilink")、[第18师团](../Page/第18师团.md "wikilink")、[第114师团](../Page/第114师团.md "wikilink")）纳入其[战斗序列](../Page/战斗序列.md "wikilink")，共计9个师团。

1945年8月抗日战争胜利前夕，侵华日军序列有：中国派遣军（总司令官[冈村宁次大将](../Page/冈村宁次.md "wikilink")，总参谋长[小林浅三郎中将](../Page/小林浅三郎.md "wikilink")）、第十方面军（由“[台湾军](../Page/台湾军.md "wikilink")”1944年9月改称）、中国方面舰队、关东军。

## 中国驻屯军

中国驻屯军原称“清国驻屯军”，[清朝滅亡後](../Page/清朝.md "wikilink")，1912年4月（[明治](../Page/明治.md "wikilink")45年）改称「[支那驻屯军](../Page/支那驻屯军.md "wikilink")」，驻[天津](../Page/天津.md "wikilink")、[山海关](../Page/山海关.md "wikilink")、[通县](../Page/通县.md "wikilink")、[丰台](../Page/丰台.md "wikilink")、[北平等地](../Page/北平.md "wikilink")，因军部位于天津[海光寺兵营](../Page/海光寺.md "wikilink")，又被称为天津军。于1937年挑起[七七事变](../Page/七七事变.md "wikilink")。1937年8月31日，日军大本营撤销中国驻屯军番号，编成[华北方面军](../Page/华北方面军.md "wikilink")。\[1\]

## 关东军

关东军的前身是1905年[日俄战争后成立的](../Page/日俄战争.md "wikilink")“满铁守备队”。1919年，日本在[關東州](../Page/關東州.md "wikilink")（包括今[大連](../Page/大連.md "wikilink")、[旅順](../Page/旅順.md "wikilink")、[金州](../Page/金州.md "wikilink")）設立[關東都督府](../Page/關東都督府.md "wikilink")，下設民政部和陸軍部，滿鐵守備隊和駐留師團歸屬關東都督指揮。同年，在關東都督府陸軍部的基礎上，于[旅順口設](../Page/旅順口.md "wikilink")[關東軍司令部](../Page/關東軍司令部.md "wikilink")。1931年[九一八事变当晚](../Page/九一八事变.md "wikilink")，关东军司令部连夜由旅顺迁往[沈阳](../Page/沈阳.md "wikilink")。1939年5月在[諾門罕与](../Page/諾門罕_\(地名\).md "wikilink")[蘇聯紅軍进行](../Page/蘇聯紅軍.md "wikilink")[諾門罕戰役](../Page/諾門罕戰役.md "wikilink")，伤亡18000人，關東軍司令官[植田謙吉及參謀長](../Page/植田謙吉.md "wikilink")[磯谷廉介被撤職](../Page/磯谷廉介.md "wikilink")。1942年10月，日本将关东军司令部升格为关东军总司令部，兵力达到顶峰。从1943年开始，随着太平洋战事发展，关东军主力逐渐被抽调至太平洋战线。1945年8月[苏日战争中](../Page/苏日战争.md "wikilink")，关东军损失约67.7万人，其中8.3万人被击毙，59.4万人投降，而苏军仅伤亡3.2万人。戰後，餘下的關東軍被送到[西伯利亞從事強制勞動](../Page/西伯利亞.md "wikilink")。\[2\]

## 中国派遣军

### 华北方面军

华北方面军前身为[中国驻屯军](../Page/中国驻屯军.md "wikilink")，日本称之为“北支那方面军”。[寺内寿一大将为司令官](../Page/寺内寿一.md "wikilink")，辖[第1军和](../Page/第1军_\(日本陆军\).md "wikilink")[第2军](../Page/第2军_\(日本陆军\).md "wikilink")，直辖[第5师团](../Page/第5师团.md "wikilink")、第109师团、中国驻屯混成第11旅团、临时航空兵团。作战地域包括河北、山西、山东、河南以及绥远、察哈尔、江苏、安徽、湖北等省的一部分\[3\]。1938年7月4日，驻蒙兵团改编为[驻蒙军](../Page/驻蒙军.md "wikilink")，兵力约2万5千人，编入[北支那方面军作战序列](../Page/北支那方面军.md "wikilink")\[4\]。

### 华中方面军

1932年1月，日本因[一二八事變成立上海派遣軍](../Page/一二八事變.md "wikilink")。於1937年8月16日，因[淞滬會戰和](../Page/淞滬會戰.md "wikilink")[南京保衛戰而再建](../Page/南京保衛戰.md "wikilink")。1937年9月11日，日军大本营组建上海派遣军。1937年10月29日，组建中支那方面軍，松井石根担任司令官，将[上海派遣军](../Page/上海派遣军.md "wikilink")（[第3师团](../Page/第3师团.md "wikilink")、[第9师团](../Page/第9师团.md "wikilink")、[第11师团](../Page/第11师团.md "wikilink")、[第13师团](../Page/第13师团.md "wikilink")、[第101师团](../Page/第101师团.md "wikilink")、[第16师团](../Page/第16师团.md "wikilink")）和新从日本本土调来的10月20日刚刚编成的[第10军](../Page/第10軍_\(日本陸軍\).md "wikilink")（[第6师团](../Page/第6师团.md "wikilink")、[第18师团](../Page/第18师团.md "wikilink")、[第114师团](../Page/第114师团.md "wikilink")）纳入其战斗序列，共计9个师团\[5\]。[上海派遣军於](../Page/上海派遣军.md "wikilink")1938年初被解散，其單位被拼入[支那派遣軍](../Page/支那派遣軍.md "wikilink")。\[6\]

### 华南方面军

华南方面军于1940年（[昭和](../Page/昭和.md "wikilink")15年）2月9日編制成軍，編入中国派遣軍[作戰序列](../Page/作戰序列.md "wikilink")。\[7\]

## 大日本帝国海军

[Japanese_cruiser_Izumo_in_Shanghai.jpg](https://zh.wikipedia.org/wiki/File:Japanese_cruiser_Izumo_in_Shanghai.jpg "fig:Japanese_cruiser_Izumo_in_Shanghai.jpg")1937年在[上海参与](../Page/上海.md "wikilink")[淞沪战役](../Page/淞沪战役.md "wikilink")\[8\]。\]\]
1929年，日本帝國海軍成立了海軍陸戰隊（，）。1932年1月23日，日本海军[巡洋艦](../Page/巡洋艦.md "wikilink")「大井號」和第十五驅逐隊（[驅逐艦](../Page/驅逐艦.md "wikilink")4艘）運載第一特海軍陸戰隊457人和大批軍火抵達上海。次日，由旅順出發之日本[航空母艦](../Page/航空母艦.md "wikilink")「能登呂號」駛達上海，参与[一二八事变](../Page/一二八事变.md "wikilink")。\[9\]

## 参见

  - [日军](../Page/日军.md "wikilink")
  - [抗日战争](../Page/抗日战争.md "wikilink")

## 参考文献

## 外部链接

[Category:日军](../Category/日军.md "wikilink")
[军](../Category/抗日战争.md "wikilink")

1.  Sophie Lee, Education in Wartime Beijing 1937–1945 (Ann Arbor,
    Michigan: Michigan University, 1996), 42.
2.  [Surrender of the Kwantung Army. Military
    Memoirs](http://militera.lib.ru/memo/russian/beloborodov/09.html)
3.
4.
5.  [秦郁彦編](../Page/秦郁彦.md "wikilink")『日本陸海軍總合事典』第2版、[東京大學出版會](../Page/東京大學出版會.md "wikilink")、2005年。
6.
7.
8.
9.