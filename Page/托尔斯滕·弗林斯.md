**托尔斯滕·弗林斯**（[德语](../Page/德语.md "wikilink")：****，），已退役[德国足球运动员](../Page/德国.md "wikilink")，司职中场，球員生涯主要在[德甲球會](../Page/德国足球甲级联赛.md "wikilink")[雲達不萊梅渡過](../Page/云达不来梅足球俱乐部.md "wikilink")。

## 生平

芬寧斯出身於德國丙組球會[亞琛](../Page/亞琛足球俱樂部.md "wikilink")，早在1997年已轉投德甲球會[雲達不萊梅](../Page/云达不来梅足球俱乐部.md "wikilink")。其間他協助過這支球會擊敗該國班霸[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")，贏得[德國盃冠軍](../Page/德國盃.md "wikilink")。直至2002年為止，芬寧斯共在雲達不萊梅上陣162場聯賽入15球。

他於[2002年世界杯時入選了德國國家隊](../Page/2002年世界杯.md "wikilink")，成為當屆世界盃亞軍隊成員。其間他轉投了另一支德甲球會[多特蒙德](../Page/多特蒙德足球俱乐部.md "wikilink")。他在這支球會站穩正選，兩季間上陣了47場聯賽入10球，但卻未獲得任何獎項。

2004年芬寧斯獲[德甲班霸](../Page/德甲.md "wikilink")[拜仁慕尼黑招攬過來](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。他出戰過29場聯賽和10場歐冠杯賽事。由於德甲冠軍長期都是拜仁慕尼黑所壟斷，芬寧斯要到加盟拜仁時，才贏得了職業生涯首個德甲聯賽冠軍。可是該季芬寧斯整體表現卻非常失色，常受球會抨擊，結果2005年再重返雲達不萊梅。

德國主辦[2006年世界杯](../Page/2006年世界杯足球赛.md "wikilink")，芬寧斯被主帅[尤尔根·克林斯曼选入國家隊](../Page/尤尔根·克林斯曼.md "wikilink")，担当防守中场的任务。可是在八強战中對[阿根廷勝出後](../Page/阿根廷國家足球隊.md "wikilink")，因牽涉到與對方球員衝突，被國際足協判罰停賽一場。其後他趕在季軍戰復出，協助德國擊敗[葡萄牙](../Page/葡萄牙国家足球队.md "wikilink")，获得季軍。

在不萊梅前任隊長[弗蘭克·鮑曼退役之後](../Page/弗蘭克·鮑曼.md "wikilink")，弗林斯接替成為不萊梅的新隊長。其後他不斷受傷患困擾，直到2013年2月下旬宣佈退役。

## 逸事

芬寧斯喜愛紋身，在他的右臂上紋有「龍」、「蛇」、「羊」、「勇」、「吉」五個漢字。而6月初[歐洲國家盃的一場外圍賽後](../Page/歐洲國家盃.md "wikilink")，記者發現了他新添了一個有趣的紋身，內容是「酸甜鴨子，7.99歐元」。

芬寧斯解釋道這味菜是他最喜愛的一味菜式，而且這菜名與他的孩子的星座有關，可以帶來好運。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [托尔斯滕·弗林斯官方网站](http://www.torsten-frings.com)

[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:德国足球运动员](../Category/德国足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:亞琛球員](../Category/亞琛球員.md "wikilink")
[Category:雲達不萊梅球員](../Category/雲達不萊梅球員.md "wikilink")
[Category:多蒙特球員](../Category/多蒙特球員.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:多倫多FC球員](../Category/多倫多FC球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:美職球員](../Category/美職球員.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:德國旅外足球運動員](../Category/德國旅外足球運動員.md "wikilink")
[Category:银月桂叶获得者](../Category/银月桂叶获得者.md "wikilink")
[Category:德甲主教練](../Category/德甲主教練.md "wikilink")
[Category:达姆施塔特主教练](../Category/达姆施塔特主教练.md "wikilink")
[Category:德乙主教练](../Category/德乙主教练.md "wikilink")

1.  足球週刊 0619 第272期 p.11