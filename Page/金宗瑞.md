**金宗瑞**（，），[字](../Page/表字.md "wikilink")**國卿**()，[號](../Page/號.md "wikilink")**節齋**()，[諡號](../Page/諡號.md "wikilink")**忠翼**()\[1\]，[本貫](../Page/本貫.md "wikilink")[順天金氏](../Page/順天金氏.md "wikilink")，是[朝鮮王朝時代的武臣](../Page/朝鮮王朝.md "wikilink")，歷[世宗](../Page/朝鮮世宗.md "wikilink")、[文宗](../Page/朝鮮文宗.md "wikilink")、[端宗三朝](../Page/朝鮮端宗.md "wikilink")。曾经奉命和[郑麟趾一同编修](../Page/郑麟趾.md "wikilink")《[高丽史](../Page/高丽史.md "wikilink")》。

## 生平

金宗瑞即是一位武臣，也是文人出身，十六歲時([太宗乙酉年](../Page/朝鮮太宗.md "wikilink"))文科及第
。1433年（[世宗](../Page/朝鮮世宗.md "wikilink")15年）赴任[咸吉道](../Page/咸吉道.md "wikilink")[觀察使](../Page/觀察使.md "wikilink")（[國朝人物志作](../Page/國朝人物志.md "wikilink")[都總制使](../Page/都總制使.md "wikilink")
，開拓北部邊疆六鎮，當時“朝議不一”
，金宗瑞“力主其事”，幸得世宗讚同，得以成事。當時朝鮮邊境經常受到[女真族](../Page/女真族.md "wikilink")[兀狄哈的騷擾](../Page/兀狄哈.md "wikilink")。金宗瑞击败女真部落，获得朝鲜半岛东北部女真人的故地。\[2\]在朝鮮初期有金宗瑞防守邊疆六鎮，並有[崔閏德](../Page/崔閏德.md "wikilink")()鎮守四郡，使朝鮮得以守衛着今日在[鴨綠江及](../Page/鴨綠江.md "wikilink")[图们江的邊界](../Page/图们江.md "wikilink")。[文宗朝為](../Page/朝鮮文宗.md "wikilink")[右議政](../Page/右議政.md "wikilink")，壬申年受顧命輔幼主。

[端宗元年十月](../Page/朝鮮端宗.md "wikilink")，[癸酉靖難發生](../Page/癸酉靖難.md "wikilink")，金宗瑞被控以謀反，眾[武士到金宗瑞的家](../Page/武士.md "wikilink")，[林芸即時把金宗瑞椎擊倒地](../Page/林芸.md "wikilink")
。金宗瑞的兒子金承珪在朝為官，當時撲在父親身上，立即被[楊汀拔劍斬殺](../Page/楊汀.md "wikilink")
。至第二日，金宗瑞甦醒，前往[敦義門向門衛求救](../Page/敦義門.md "wikilink")，但不被理會。其匿藏在妻子娘家的另一個兒子金承璧是當年文科及第，獲[直長官職](../Page/直長.md "wikilink")，亦被斬殺。\[3\]

[英祖戊寅年](../Page/朝鮮英祖.md "wikilink")(1758年)時獲賜諡號**忠翼**
；[純祖壬申年](../Page/朝鮮純祖.md "wikilink")(1812年)與子金承珪俱設壇享於端宗的陵寢[莊陵之側](../Page/朝鮮莊陵.md "wikilink")\[4\]

## 诗歌

## 家族

  - 父：[金錘](../Page/金錘.md "wikilink")
  - 母：[星州裵氏](../Page/星州裵氏.md "wikilink")
  - 兄弟：
      - [金宗漢](../Page/金宗漢.md "wikilink")
      - [金宗興](../Page/金宗興.md "wikilink")

<!-- end list -->

  - 妻：[坡平尹氏](../Page/坡平尹氏.md "wikilink")
  - 子：
      - [金承珪](../Page/金承珪.md "wikilink")\[5\]
      - [金承璧](../Page/金承璧.md "wikilink")\[6\]
      - [金承琉](../Page/金承琉.md "wikilink")\[7\]
      - 金目臺
      - 金石臺

## 相關影視作品及飾演者

  - 電視劇
      - 《》，MBC，1984年—1985年，演員：
      - 《》，KBS，1990年，演員：[李順載](../Page/李順載.md "wikilink")
      - 《》，KBS，1994年，演員：
      - 《[王與妃](../Page/王與妃.md "wikilink")》，KBS，1998年—2000年，演員：[趙卿煥](../Page/趙卿煥.md "wikilink")
      - 《》，KBS，2007年，演員：김수일
      - 《[大王世宗](../Page/大王世宗.md "wikilink")》，KBS，2008年，演員：
      - 《[公主的男人](../Page/公主的男人.md "wikilink")》，KBS，2011年，演員：李順載
      - 《[樹大根深](../Page/樹大根深.md "wikilink")》，SBS，2011年，演員：[崔日和](../Page/崔日和.md "wikilink")
      - 《[仁粹大妃](../Page/仁粹大妃_\(電視劇\).md "wikilink")》，JTBC，2011年—2012年，演員：
  - 電影
      - 《[觀相](../Page/觀相.md "wikilink")》，2013年，演員：[白潤植](../Page/白潤植.md "wikilink")

## 參考資料

## 參見

  - [國朝人物志](../Page/國朝人物志.md "wikilink")
  - [朝鮮世宗](../Page/朝鮮世宗.md "wikilink")
  - [尹瓘](../Page/尹瓘.md "wikilink")

[Category:朝鮮王朝军事人物](../Category/朝鮮王朝军事人物.md "wikilink")
[Category:順天金氏](../Category/順天金氏.md "wikilink")
[Category:諡忠翼](../Category/諡忠翼.md "wikilink")
[Category:左議政](../Category/左議政.md "wikilink")
[Category:右议政](../Category/右议政.md "wikilink")
[Category:朝鮮王朝遇刺身亡者](../Category/朝鮮王朝遇刺身亡者.md "wikilink")

1.  [端宗朝:
    金宗瑞](http://yoksa.aks.ac.kr/jsp/aa/VolView.jsp?mode=&page=&fcs=&fcsd=&cf=&cd=&gb=&aa10up=&aa10no=kh2_je_a_vsu_B9C%5E2_001%20&aa15no=001&aa20no=B9C%5E2_001_0270&gnd1=&gnd2=&keywords=%E5%AE%97%E7%91%9E)

2.  [世宗的儒家人本主义](http://www.neworldzj.com/Show.asp?id=22744&data=JP)

3.  《国朝人物志·文宗朝·金宗瑞传》：「字國卿，號節齋，順天人。太宗乙酉文科。世宗置六鎭，以宗瑞爲咸吉道都節制使，時朝議不一，宗瑞力主其事。上曰：“雖有寡人，若無宗瑞，不足以辦此事。”宗瑞旣設鎭，日置酒張樂，大饗將士。嘗有流矢中酒樽，意氣自若。文宗朝爲右議政，壬申受顧命輔幼主，尋陞左議政，賜几杖。宗瑞多智略，時人目爲大虎。癸酉十月，世祖將先誅宗瑞，招諸武士至宗瑞家，使林芸卽椎擊宗瑞，仆地。子承珪奔救，伏其上，楊汀拔劍斬之。翌日宗瑞蘇，使元矩告敦義門守者曰：“夜被傷，幾死門者，不應宗瑞。”匿其子承璧妻家，追捕之。宗瑞曰：“我大臣也，不可以步，將我軺軒來。”遂斬之。英祖戊寅，賜諡忠翼。子承珪，官知部；承璧，癸酉文科，直長；庶子石臺、木臺；承珪子祖同、壽同皆被死。」

4.

5.
6.
7.