**维库**（****）是[中国大陆的一个基於](../Page/中国大陆.md "wikilink")技术的[网站](../Page/网站.md "wikilink")。维库称其目标是利用建立一个自由分享[知识与](../Page/知识.md "wikilink")[思想的](../Page/思想.md "wikilink")[社区](../Page/社区.md "wikilink")\[1\]。

## 关于

维库采用了和[维基百科相同的](../Page/维基百科.md "wikilink")[MediaWiki](../Page/MediaWiki.md "wikilink")[軟體](../Page/軟體.md "wikilink")，使用了[维基百科的部份数据库](../Page/维基百科.md "wikilink")，和一般的镜像站点相比，维库保留了所有维基百科编辑的历史纪录，并在这些内容的基础上不断发展，逐渐形成了特有的风格和用户群\[2\]。

作为一个民间的站点，维库并没有商业赞助等资金的支持，因此，维库上发布了[Google
Adsense的内容相关](../Page/Google_Adsense.md "wikilink")[广告](../Page/广告.md "wikilink")，虽然这并不违反[GFDL](../Page/GFDL.md "wikilink")，但确实导致了一些维基爱好者的反对。维库目前只有[汉语一种版本](../Page/汉语.md "wikilink")，但用户可以使用它提供的[镜像浏览](../Page/网站镜像.md "wikilink")2006年6月19日前的[英文维基](../Page/英文维基.md "wikilink")。

维库必须在登入后，并且用户已经验证了电子邮件，才能编辑。

## 发展历程

  - 2004年9月，WikiLib开始上线运行。
  - 2005年5月，WikiLib正式采用维库做为网站中文名。
  - 2005年10月14日，维库Alexa排名突破十万，访问量稳定上升。
  - 2005年11月28日，维库被-{博客}-研究中心选为中国-{互}-联网2.0百家新锐网站之一。
  - 2006年1月6日，维库Alexa排名为27368（三个月均值）。
  - 2006年7月29日，维库注册用户达到5000人。
  - 2007年2月2日，维库注册用户达到10000人。
  - 2013年，维库关闭\[3\]。

## 外部链接

  - [维库首页](https://web.archive.org/web/20081226003136/http://www.wikilib.com/wiki/%E9%A6%96%E9%A1%B5)

## 参考

[Category:MediaWiki網站](../Category/MediaWiki網站.md "wikilink")
[Category:汉语在线百科全书](../Category/汉语在线百科全书.md "wikilink")

1.
2.
3.  [archive.today](https://archive.is/www.wikilib.com)