**半人馬座A**，也稱為**NGC
5128**，是位於[半人馬座內距離大約](../Page/半人馬座.md "wikilink")1千4百萬光年遠的一個[透鏡星系](../Page/透鏡星系.md "wikilink")。它是最靠近地球的電波源之一，也是被專業天文學家廣泛研究的[活躍星系核](../Page/活躍星系核.md "wikilink")\[1\]。這個星系也是全天第五亮的星系，所以即使這個星系只能在南半球和北半球的低緯度地區看見，它依然也還是業餘天文學家的理想目標\[2\]。
[相對論性噴流的能量被相信是從在星系核心的](../Page/相對論性噴流.md "wikilink")[超大質量黑洞附近的空間喷射出來的](../Page/超大質量黑洞.md "wikilink")，輻射出[X射線和](../Page/X射線.md "wikilink")[無線電波的波長](../Page/無線電波.md "wikilink")。以十年的間隔對噴流的電波進行觀察，天文學家確定噴流內側部分的移動速度達到[光速的一半](../Page/光速.md "wikilink")。X射線則是噴流內的高能微粒在更遠處與周圍的氣體碰撞所產生的。

如同對其他的[星爆星系所做的觀測](../Page/星爆星系.md "wikilink")，碰撞導致恆星的形成和引發強烈的爆炸。使用[史匹哲太空望遠鏡](../Page/史匹哲太空望遠鏡.md "wikilink")，科學家證實了半人馬座
A是經由一個大型橢圓星系與一個小螺旋星系碰撞的結果。

## 型態（組織）

半人馬座A在型態學上可能是[特殊星系](../Page/特殊星系.md "wikilink")，因為從地球上觀察，這個細看起來像是[透鏡星系或](../Page/透鏡星系.md "wikilink")[橢圓星系疊加上一條塵埃帶](../Page/橢圓星系.md "wikilink")\[3\]。這個特殊的星系在1847年首度被[約翰·赫歇爾確認](../Page/約翰·赫歇爾.md "wikilink")，並被收錄在[赫頓·阿普的](../Page/赫頓·阿普.md "wikilink")***《[特殊星系圖集](../Page/特殊星系圖集.md "wikilink")》***（1966年出版），作為"受攝"星系和塵埃吸收的最佳範例之一\[4\]。這個星系的奇怪型態被公認為是兩個較小星系[合併的結果](../Page/星系交互作用.md "wikilink")\[5\]。

這個星系的核球主要由演化中的紅色恆星組成\[6\]；然而，塵埃盤是最近的[恆星形成的場所](../Page/恆星形成.md "wikilink")\[7\]，已經確認的恆星形成區超過100個\[8\]。

## 超新星

在半人馬座A曾經檢測到一顆[超新星](../Page/超新星.md "wikilink")\[9\]。 這顆超新星，[SN
1986G](../Page/SN_1986G.md "wikilink")，是[艾文思於](../Page/羅伯特艾文思_\(天文學家\).md "wikilink")1986年在塵埃帶內發現的\[10\]，
稍後被鑑定是一顆[Ia超新星](../Page/Ia超新星.md "wikilink")\[11\]。Ia超新星通常是有一顆白矮星的[聯星系統](../Page/聯星系統.md "wikilink")，白矮星由伴星攫取氣體，使質量至足以點燃在核心的碳融合反應，引發[熱失控的核融合](../Page/熱失控.md "wikilink")。SN
1986G被用來演示Ia超新星的光譜並不是全都一樣，Ia超新星的光譜可能會隨著時間的推移，在亮度和形式上有不同的改變\[12\]。
2016年2月发现第二颗超新星SN2016adj，稍后经被鉴定为是一颗Ⅱb超新星。

## 距離

自1980年代以來，估計NGC
5128的距離通常在3-5百萬[秒差距](../Page/秒差距.md "wikilink")\[13\]\[14\]\[15\]\[16\]\[17\]\[18\]。發現在NGC
5128的[經典造父變星受到濃厚的塵埃巷道遮蔽](../Page/經典造父變星.md "wikilink")，依據消光法的本質和考慮其他的因素，距離估計在300-350萬秒差距\[19\]\[20\]。在NGC
5128也發現[米拉變星](../Page/米拉變星.md "wikilink")\[21\]和[第二型造父變星](../Page/第二型造父變星.md "wikilink")\[22\]\[23\]，而後者很少在[本星系群之外被發現](../Page/本星系群.md "wikilink")\[24\]。以一些[米拉變星和](../Page/米拉變星.md "wikilink")[行星狀星雲測定的距離](../Page/行星狀星雲.md "wikilink")，認為是更遠的〜380萬秒差距\[25\]\[26\]。

## 鄰近的星系和星系集團的資料

半人馬座 A位於[半人馬座
A/M83星系集團的兩個子集團中的一個的中心位置](../Page/半人馬座_A/M83星系集團.md "wikilink")，是一個鄰近的[星系集團](../Page/星系集團.md "wikilink")\[27\]。[M83](../Page/M83.md "wikilink")
([南風車星系](../Page/南風車星系.md "wikilink"))在另一個集團的中心，有時這兩個集團會被視為一個集團\[28\]\[29\]，有時又會被視為兩個集團\[30\]。然而，環繞著半人馬座
A的星系和環繞著M83實際上是非常接近的，且這兩個集團之間有著相互的運動\[31\]。

## 業餘天文學的資訊

半人馬座 A位於[半人馬座ω](../Page/半人馬座ω.md "wikilink")
(一個肉眼可見的[球狀星團](../Page/球狀星團.md "wikilink"))
北方約4°的地方\[32\]。因為這個星系有著高表面亮度和相對較大的視角，他是業餘天文觀測的理想目標。明亮的核球和黑色的塵埃帶在找星鏡和大的雙筒望遠鏡中就能見到\[33\]，而其他的結構要更大的望遠鏡才能看見\[34\]。

<div style="clear: both">

</div>

## 图片

Image:Centaurus A halo.jpg|半人馬座A的光晕 Image:Radio galaxy Centaurus A by
ALMA.jpg|The radio galaxy Centaurus A, as seen by
[ALMA](../Page/Atacama_Large_Millimeter_Array.md "wikilink") Image:NGC
5128 galaxy.jpg|Image taken by the Wide Field Imager attached to the
[MPG/ESO 2.2-meter telescope](../Page/MPG/ESO_telescope.md "wikilink")
at the [La Silla
Observatory](../Page/La_Silla_Observatory.md "wikilink").
Image:Firestorm of Star Birth in Galaxy Centaurus A.jpg|"Hubble's
panchromatic vision... reveals the vibrant glow of young, blue star
clusters..."\[35\] Image:CentaurusA2.jpg|A [Hubble Space
Telescope](../Page/Hubble_Space_Telescope.md "wikilink") (HST) image of
the dust disk in front of the nucleus of Centaurus A. Credit:
HST/[NASA](../Page/NASA.md "wikilink")/[ESA](../Page/European_Space_Agency.md "wikilink").
Image:CenA-nearIR.jpg|This image of the central parts of Centaurus A
reveals the parallelogram-shaped remains of a smaller galaxy that was
absorbed about 200 to 700 million years ago.
Image:CentaurusA3.jpg|[史匹哲太空望遠鏡的完整盤面影像](../Page/史匹哲太空望遠鏡.md "wikilink")
Image:NGC_5128.jpg|[Chandra
X-ray](../Page/Chandra_X-ray_Observatory.md "wikilink") view of Cen A in
X-rays showing one [relativistic
jet](../Page/relativistic_jet.md "wikilink") from the central [black
hole](../Page/black_hole.md "wikilink") Image:Centaurus A.jpg
<File:Centaurus> A jets.ogv|Video about Centaurus A jets.
<File:Centauros>
a-spc.png|"[False-colour](../Page/False-colour.md "wikilink") image of
the nearby radio galaxy Centaurus A, showing
[radio](../Page/radio_wave.md "wikilink") (red), 24-micrometre
[infrared](../Page/infrared.md "wikilink") (green) and 0.5-5
[keV](../Page/keV.md "wikilink") [X-ray](../Page/X-ray.md "wikilink")
emission (blue). The jet can be seen to emit [synchrotron
emission](../Page/synchrotron_emission.md "wikilink") in all three
wavebands. The lobes only emit in the radio frequency range, and so
appear red. Gas and dust in the galaxy emits [thermal
radiation](../Page/thermal_radiation.md "wikilink") in the infrared.
Thermal X-ray radiation from hot gas and non-thermal emission from
relativistic electrons can be seen in the blue 'shells' around the
lobes, particularly to the south (bottom)."\[36\]

<div style="clear: both">

</div>

## 相關條目

  - [M87](../Page/M87.md "wikilink") - *也是強烈電波源的巨大橢圓星系。*
  - [NGC 1316](../Page/NGC_1316.md "wikilink") - *也是強烈電波源的相似透鏡星系。*

## 外部連結

  - [**SEDS**: Peculiar Galaxy NGC
    5128](https://web.archive.org/web/20080704214447/http://seds.org/Messier/xtra/ngc/n5128.html)

  - [ESA/Hubble images of Centaurus
    A](http://www.spacetelescope.org/images/archive/freesearch/5128/viewall/1)

  - [**NASA's APOD**: The Galaxy Within Centaurus A
    (3/4/06)](http://apod.gsfc.nasa.gov/apod/ap060304.html)

  - [**NASA's APOD**: X-Rays from an Active Galaxy
    (7/5/03)](http://apod.gsfc.nasa.gov/apod/ap030705.html)

  - [High-resolution image of Centaurus
    A](http://www.universetoday.com/wp-content/uploads/2006/07/ngc5128.jpg)
    showing the discrete elements of galactic core

  - [Centaurus A](http://www.universetoday.com/?s=Centaurus) at
    UniverseToday.com

  - [NGC5128 Centaurus
    A](https://web.archive.org/web/20090209000355/http://www.dsi-astronomie.de/Centaurus.html)

  -
## 參考資料

  - STScI. [Hubble Provides Multiple Views of How to Feed a Black
    Hole](http://hubblesite.org/newscenter/newsdesk/archive/releases/1998/14/text/).
    Press release: *Space Telescope Science Institute*. March 14, 1998.
  - Chandra X-Ray Observatory Photo Album [Centaurus A
    Jet](http://chandra.harvard.edu/photo/2003/cenajet/)

<div class="references-small">

<references/>

</div>

[Category:透鏡狀星系](../Category/透鏡狀星系.md "wikilink")
[Category:特殊星系](../Category/特殊星系.md "wikilink")
[Category:電波星系](../Category/電波星系.md "wikilink")
[Category:星暴星系](../Category/星暴星系.md "wikilink")
[Category:半人馬座A/M83星系團](../Category/半人馬座A/M83星系團.md "wikilink")
[5128](../Category/半人馬座NGC天體.md "wikilink")
[46957](../Category/PGC天體.md "wikilink")
[153](../Category/阿普天體.md "wikilink")
[077](../Category/科德韦尔天体.md "wikilink")

1.

2.

3.

4.

5.

6.
7.
8.

9.

10.

11.

12.
13.
14.
15.
16.
17.
18.
19.

20.

21.

22.
23.
24.

25.

26.

27.

28.

29.

30.

31.

32.
33.
34.
35.

36.