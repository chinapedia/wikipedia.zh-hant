**叉龍科**（[學名](../Page/學名.md "wikilink")：Dicraeosauridae）是[蜥腳下目的一科](../Page/蜥腳下目.md "wikilink")，生存於晚[侏儸紀到早](../Page/侏儸紀.md "wikilink")[白堊紀的](../Page/白堊紀.md "wikilink")[非洲與](../Page/非洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。目前僅有三個已承認屬：生存於侏儸紀非洲的[叉龍](../Page/叉龍.md "wikilink")、生存於早[白堊紀南美洲的](../Page/白堊紀.md "wikilink")[短頸潘龍](../Page/短頸潘龍.md "wikilink")、以及生存於早白堊紀南美洲的[阿馬加龍](../Page/阿馬加龍.md "wikilink")；而阿馬加龍擁有獨特的頸部神經棘。依照蜥腳下目的標準，這三個屬的體型較小，擁有相對較短的頸部。

[Dicraeosaurids_BW.png](https://zh.wikipedia.org/wiki/File:Dicraeosaurids_BW.png "fig:Dicraeosaurids_BW.png")、2.[阿馬加龍](../Page/阿馬加龍.md "wikilink")、3.[短頸潘龍](../Page/短頸潘龍.md "wikilink")\]\]
McIntosh在1990年將許多屬恐龍歸類於[梁龍科的叉龍亞科](../Page/梁龍科.md "wikilink")，但當中的許多屬現在被分類於[納摩蓋吐龍科](../Page/納摩蓋吐龍科.md "wikilink")、[雷巴齊斯龍科](../Page/雷巴齊斯龍科.md "wikilink")。

叉龍科被[親緣分支分類法敘述為一個](../Page/親緣分支分類法.md "wikilink")[基群](../Page/基群.md "wikilink")[分類單元](../Page/分類單元.md "wikilink")，並被定義為：包含[漢氏叉龍](../Page/叉龍.md "wikilink")（*Dicraeosaurus
hansemanni*），但不包含[長梁龍](../Page/梁龍.md "wikilink")（*Diplodocus
longus*）在內的最大[演化支](../Page/演化支.md "wikilink")；或者是[梁龍超科當中](../Page/梁龍超科.md "wikilink")，所有親近於叉龍，而離梁龍較遠的所有物種。

## 參考資料

  - Sereno (2005). [edit.php?tax_id=107\&Action=View Taxon Search :
    Dicraeosauridae](http://www.taxonsearch.org/dev/taxon_)
  - Jack McIntosh (1990). "Sauropoda" in *The Dinosauria*, Edited by
    David B. Weishampel, Peter Dodson, and Halszka Osmólska. University
    of California Press, pp. 345-401.
  - Paul Upchurch, Paul M. Barrett and Peter Dodson (2004). Sauropoda.
    In *The Dinosauria*, 2nd edition. Weishampel, Dodson, and Osmólska
    (eds.). University of California Press, Berkeley. Pp. 259-322.
  - Paul Sereneo (1998). A rationale for phylogenetic definitions, with
    application to the higher-level taxonomy of Dinosauria. *Neues
    Jahrbuch fur Geologie und Palaontologie* 210: 41-83.
  - Jeffrey A. Wilson (2002). Sauropod dinosaur phylogeny: critique and
    cladistic analysis, *Zoological Journal of the Linnean Society*
    136(2):215-275
  - \------ (2005) "Overview of Sauropod Phylogeny and Evolution", in
    Kristina Curry Rogers and Wilson, eds, 2005, *The Sauropods:
    Evolution and Paleobiology*, University of California Press,
    Berkeley, ISBN 0-520-24623-3

[\*](../Category/叉龍科.md "wikilink")