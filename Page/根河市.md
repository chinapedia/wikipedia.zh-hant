**根河市**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[内蒙古自治区的一个](../Page/内蒙古自治区.md "wikilink")[县级市](../Page/县级市.md "wikilink")，由[呼伦贝尔市代管](../Page/呼伦贝尔市.md "wikilink")。原名**额尔古纳左旗**，是[中国](../Page/中国.md "wikilink")[纬度最高的](../Page/纬度.md "wikilink")[县级市](../Page/县级市.md "wikilink")，根河市境内最北的城镇为[满归镇](../Page/满归镇.md "wikilink")，位于[内蒙古自治区的东北端](../Page/内蒙古.md "wikilink")、[大兴安岭北段的西侧山坡上](../Page/大兴安岭.md "wikilink")。北接[黑龙江省](../Page/黑龙江省.md "wikilink")。

## 历史

[清代这里是](../Page/清代.md "wikilink")[索伦部的狩猎地](../Page/索伦部.md "wikilink")。1921年设[室韦](../Page/室韦县.md "wikilink")、奇-{乾}-两县。1933年撤两县分设[额尔古纳左旗和](../Page/额尔古纳左旗.md "wikilink")[额尔古纳右旗](../Page/额尔古纳右旗.md "wikilink")。1948年左、右旗合并为[额尔古纳旗](../Page/额尔古纳旗.md "wikilink")。1966年再次分设左、右两旗。1994年两旗同时撤旗设市，8月18日左旗改为根河市，以境内河流[根河和驻地根河镇而得名](../Page/根河.md "wikilink")，右旗则改为[额尔古纳市](../Page/额尔古纳市.md "wikilink")。目前隶属于地级市[呼伦贝尔市](../Page/呼伦贝尔市.md "wikilink")。

## 地理

根河市地处[大兴安岭腹地](../Page/大兴安岭.md "wikilink")，[森林覆盖率达](../Page/森林.md "wikilink")73%，气候属寒温带湿润型森林气候，并具有大陆季风性气候的某些特征，特点是寒冷湿润，冬长夏短，春秋相连，全年无霜期平均为70天左右，结冻期在210天左右，全市年平均气温−5℃，极端低温−58℃，年平均降水量294毫米。\[1\]

## 行政区划

全市辖4个[街道](../Page/街道办事处.md "wikilink")、4个[镇](../Page/镇.md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")。

  - 街道：[河东街道](../Page/河东街道.md "wikilink")、[河西街道](../Page/河西街道.md "wikilink")、[森工路街道](../Page/森工路街道.md "wikilink")、[好里堡街道](../Page/好里堡街道.md "wikilink")
  - 镇：[金河镇](../Page/金河镇.md "wikilink")、[阿龙山镇](../Page/阿龙山镇.md "wikilink")、[满归镇](../Page/满归镇.md "wikilink")、[得耳布尔镇](../Page/得耳布尔镇.md "wikilink")
  - 民族乡：[敖鲁古雅鄂温克族乡](../Page/敖鲁古雅鄂温克族乡.md "wikilink")

## 民族

根河市包括有19个民族，[汉族](../Page/汉族.md "wikilink")、[蒙古族](../Page/蒙古族.md "wikilink")、[回族](../Page/回族.md "wikilink")、[藏族](../Page/藏族.md "wikilink")、[苗族](../Page/苗族.md "wikilink")、[彝族](../Page/彝族.md "wikilink")、[壮族](../Page/壮族.md "wikilink")、[朝鲜族](../Page/朝鲜族.md "wikilink")、[满族](../Page/满族.md "wikilink")、[土族](../Page/土族.md "wikilink")、[锡伯族](../Page/锡伯族.md "wikilink")、[达斡尔族](../Page/达斡尔族.md "wikilink")、[鄂温克族](../Page/鄂温克族.md "wikilink")、[鄂伦春族](../Page/鄂伦春族.md "wikilink")、[俄罗斯族等](../Page/俄罗斯族.md "wikilink")。

根河市是[鄂温克族的聚居地](../Page/鄂温克族.md "wikilink")，下辖[敖鲁古雅乡](../Page/敖鲁古雅乡.md "wikilink")。鄂族居民原本生活在森林中以狩猎和养殖[驯鹿为生](../Page/驯鹿.md "wikilink")，后整体搬迁至根河市附近定居。\[2\]

## 交通

  - 铁路：[牙林铁路](../Page/牙林铁路.md "wikilink")（[牙克石](../Page/牙克石.md "wikilink")－[满归镇](../Page/满归镇.md "wikilink")）[根河站](../Page/根河站.md "wikilink")
  - 公路：

## 经济

经济以[林业为主](../Page/林业.md "wikilink")，有采伐、板业、制浆[造纸](../Page/造纸.md "wikilink")、[印刷](../Page/印刷.md "wikilink")、农机制造等相关产业。近年来为保持生态平衡，实行了天然林保养措施。

## 参考文献

[根河市](../Category/根河市.md "wikilink")
[呼伦贝尔](../Category/内蒙古县级市.md "wikilink")
[市/县级市](../Category/呼伦贝尔县级行政区.md "wikilink")
[Category:中华人民共和国第三批资源枯竭型城市](../Category/中华人民共和国第三批资源枯竭型城市.md "wikilink")

1.  内蒙古根河市人民政府资料
2.  内蒙古根河市人民政府资料