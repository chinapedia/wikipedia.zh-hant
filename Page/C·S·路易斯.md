**克利夫·斯特普尔斯·路易斯**（，），通称**C·S·路易斯**（）或
**魯益師**，其朋友及家人暱稱他為傑克（Jack），出生於[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")[首府](../Page/首府.md "wikilink")[贝尔法斯特](../Page/贝尔法斯特.md "wikilink")，但長年居住於[英格蘭](../Page/英格蘭.md "wikilink")，是[威爾斯裔](../Page/威爾斯.md "wikilink")[英國知名](../Page/英國.md "wikilink")[作家](../Page/作家.md "wikilink")、[詩人及](../Page/詩人.md "wikilink")[護教家](../Page/基督教辨惑学.md "wikilink")。他以[兒童文學作品](../Page/兒童文學.md "wikilink")《[納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")》而聞名於世\[1\]，此外還有神學論文、中世紀文學研究等諸多著作。

路易斯和英國另一知名作家[J·R·R·托爾金互為好友](../Page/J·R·R·托爾金.md "wikilink")，他們都曾同在[牛津大學任教](../Page/牛津大學.md "wikilink")\[2\]\[3\]。

## 簡歷

路易斯於1898年出生於[贝尔法斯特的一個](../Page/贝尔法斯特.md "wikilink")[新教徒家庭中](../Page/新教.md "wikilink")。小時候因為討厭學校，只接受[家庭教師的授課](../Page/家庭教師.md "wikilink")。1916年他取得[獎學金進入](../Page/獎學金.md "wikilink")[牛津大學就讀](../Page/牛津大學.md "wikilink")，期間曾被徵召參與[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")。自1925年起，他在[牛津大學莫德林學院擔任研究員](../Page/牛津大學莫德林學院.md "wikilink")，任教期間，他參加名為「[迹象文学社](../Page/迹象文学社.md "wikilink")（The
Inklings）」的讀書會，並結識Ｎ．柯格希尔（牛津大学英国文学教授），以及著名的[J·R·R·托爾金](../Page/J·R·R·托爾金.md "wikilink")，这场相遇改变了他整个人生。

在这两位朋友的影响下，1929年[复活节](../Page/复活节.md "wikilink")，路易斯“痛改前非，承认上帝是上帝。我跪下，我祷告，那晚，我很可能是全英国最丧气也最不情愿但却回头了的浪子。”

之後在[劍橋大學擔任中世紀與](../Page/劍橋大學.md "wikilink")[文藝復興英語學的教授](../Page/文藝復興.md "wikilink")。並成為二十世紀中葉英語世界中捍衛基督信仰最有力也最受歡迎的人。1954年，获选为剑桥大学中世纪与文艺复兴期英国文学讲座教授，所写文学批评论文已成传世之作。他是一位甚受学生爱戴的老师。而他写的神学和具神学深度的文学作品早已脍炙人口。因而有“向怀疑者传福音的使徒”之称，获到全球教会的称赞。

《納尼亞傳奇》（The Chronicles of
Narnia）更是一部宣揚福音的作品。《[返璞归真](../Page/返璞归真.md "wikilink")》（Mere
Christianity）則是他的著作中影响最大的一部。

## 著作

### 小說

[Statue_of_C.S._Lewis,_Belfast.jpg](https://zh.wikipedia.org/wiki/File:Statue_of_C.S._Lewis,_Belfast.jpg "fig:Statue_of_C.S._Lewis,_Belfast.jpg")的東[貝爾法斯特](../Page/貝爾法斯特.md "wikilink")，C·S·路易斯開啟「[魔衣櫥](../Page/衣櫥_\(納尼亞\).md "wikilink")」銅製雕像。\]\]

  - 《太空三部曲》
    1.  《[來自寂靜的星球](../Page/沉寂的星球.md "wikilink")》（Out of the Silent
        Planet，1938年）
    2.  《[漫遊金星](../Page/皮尔兰德拉星.md "wikilink")》（Perelandra，1943年）
    3.  《[那股邪惡的力量](../Page/那股邪惡的力量.md "wikilink")》（That Hideous
        Strength，1946年）
  - 《地獄來鴻》（又譯作：《大榔頭寫給蠹木的煽情書》，《魔鬼家书》）（The Screwtape Letters，1942年）
  - 《夢幻巴士》（The Great Divorce，1945年）
  - 《[納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")》
    1.  《[獅子·女巫·魔衣櫥](../Page/獅子·女巫·魔衣櫥.md "wikilink")》（The Lion, the
        Witch and the Wardrobe，1950年）
    2.  《[凯斯宾王子](../Page/凯斯宾王子.md "wikilink")》（Prince Caspian: The
        Return to Narnia，1951年）
    3.  《[黎明行者號](../Page/黎明行者號.md "wikilink")》（The Voyage of the Dawn
        Treader，1952年）
    4.  《[銀椅](../Page/銀椅.md "wikilink")》（The Silver Chair，1953年）
    5.  《[奇幻馬和傳說](../Page/奇幻馬和傳說.md "wikilink")》（The Horse and His
        Boy，1954年）
    6.  《[魔法師的外甥](../Page/魔法師的外甥.md "wikilink")》（The Magician's
        Nephew，1955年）
    7.  《[最终一战](../Page/最终一战.md "wikilink")》（The Last Battle，1956年）
  - 《裸顏》（Till We Have Faces，1956年）
  - 《致马尔科姆》（Letters to Malcolm，1963年）

### 非虚构作品

  - 《痛苦的奧秘》（The Problem of Pain，1940年）
  - 《反璞歸真》（又譯作：《如此基督教》）（Mere Christianity，1952年，是1941年至1944年的廣播談話內容）
  - 《詩篇擷思》（Reflections on the Psalms，1958年）
  - 《四種愛》（The Four Loves，1960年）
  - 《卿卿如晤》（A Grief Observed，1960年）
  - 《魯益師：寄小讀者》（又譯作：《给孩子们的信》）（C. S. Lewis: Letters to Children，1999年輯錄）

## 相關影视作品

  - 1993年上映的電影《》（Shadowlands），描寫路易斯晚年的愛情故事。由[李察·艾登保羅](../Page/李察·艾登保羅.md "wikilink")（Richard
    Attenborough）導演，[安東尼·霍普金斯](../Page/安東尼·霍普金斯.md "wikilink")（Anthony
    Hopkins）飾演路易斯，[黛博拉·溫姬](../Page/黛博拉·溫姬.md "wikilink")（Debra
    Winger）飾演他的妻子－美國猶太裔女詩人[海倫·喬依·戴維德曼](../Page/海倫·喬依·戴維德曼.md "wikilink")\[4\]。

## 身後影響

[缩略图](https://zh.wikipedia.org/wiki/File:CSLewismural.png "fig:缩略图")。\]\]
後世的許多文學作品皆受到了C·S·路易斯作品的啟發，其中包括[J·K·羅琳的](../Page/J·K·羅琳.md "wikilink")《[哈利波特](../Page/哈利波特.md "wikilink")》，羅琳曾說路易斯是她最喜歡的文學作家之一\[5\]\[6\]，並且表示自己從小時候就非常喜歡閱讀《納尼亞傳奇》系列\[7\]。

在2008年英國《[泰晤士報](../Page/泰晤士報.md "wikilink")》「1945年以來英國最偉大的50位作家」名單中，路易斯排在第十三名\[8\]。

於1988年發現的[小行星7644被命名為](../Page/小行星7644.md "wikilink")「Cslewis」，以此紀念路易斯\[9\]。

在路易斯百年誕辰的1998年11月，於東貝爾法斯特豎立了一尊路易斯開啟「[魔衣櫥](../Page/魔衣櫥.md "wikilink")」的雕像，是由藝術家按照路易斯青年時的樣子塑造的\[10\]。

## 參見

  - [J·R·R·托爾金](../Page/J·R·R·托爾金.md "wikilink")
  - [迹象文学社](../Page/迹象文学社.md "wikilink")
  - [波琳·拜恩斯](../Page/波琳·拜恩斯.md "wikilink")

## 注释

## 参考文献

### 引用

### 来源

  - 《[返璞归真](../Page/返璞归真.md "wikilink")》一书译序

<!-- end list -->

  - C·S·路易斯相关傳記

<!-- end list -->

  - 《走進魔衣櫥：路易斯與納尼亞的閱讀地圖》，幸佳慧 著，台灣2006年，時報出版。
  - 《魯益師——魔幻王國的創造者》，梁志華 著，台灣2006年，突破出版社。
  - 《聖經、魔戒與奇幻宗師》，[柯林·杜瑞茲](../Page/柯林·杜瑞茲.md "wikilink") 著，台灣2004年，啟示出版。
  - 《納尼亞人：C·S·路易斯的生活與想像》，艾倫·雅各布斯 著，上海2014年，華東師範大學出版社。

## 外部連結

  -
  -
  -
  -
{{-}}

[C·S·路易斯](../Category/C·S·路易斯.md "wikilink")
[Category:兒童文學作家](../Category/兒童文學作家.md "wikilink")
[Category:英国作家](../Category/英国作家.md "wikilink")
[Category:英国基督教神学家](../Category/英国基督教神学家.md "wikilink")
[Category:基督教护教士](../Category/基督教护教士.md "wikilink")
[Category:牛津大学大学学院校友](../Category/牛津大学大学学院校友.md "wikilink")
[Category:愛爾蘭聖公宗教徒](../Category/愛爾蘭聖公宗教徒.md "wikilink")
[Category:威爾斯裔愛爾蘭人](../Category/威爾斯裔愛爾蘭人.md "wikilink")
[Category:贝尔法斯特人](../Category/贝尔法斯特人.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.