**建瓯市**（），又被称作“**芝城**”，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[南平市管辖的](../Page/南平市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，位于福建省的中部偏北，[武夷山脉之东南](../Page/武夷山脉.md "wikilink")，[鹫峰山脉西北侧](../Page/鹫峰山脉.md "wikilink")，[闽江上游](../Page/闽江.md "wikilink")。

建瓯市人口约53万，是[闽北人口最多的县级行政区](../Page/闽北.md "wikilink")，以[汉族为主](../Page/汉族.md "wikilink")，亦有[畲族和](../Page/畲族.md "wikilink")[苗族等](../Page/苗族.md "wikilink")[中國少数民族](../Page/中國少数民族.md "wikilink")，通行[闽北语](../Page/闽北语.md "wikilink")[建瓯话](../Page/建瓯话.md "wikilink")。建瓯市面积達4,233.13平方千米，是福建省面积最大的县级行政区。

建瓯一名来源于古代[**建安**县和](../Page/建安县.md "wikilink")[**瓯宁**县](../Page/瓯宁县.md "wikilink")，是福建最早开发的地区之一。早在[闽越国时期](../Page/闽越国.md "wikilink")，建瓯被当作闽越王[无诸的行宫](../Page/无诸.md "wikilink")。192年，[会稽太守](../Page/会稽.md "wikilink")[孙策析](../Page/孙策.md "wikilink")[侯官县北部地区](../Page/侯官县.md "wikilink")，设置[建安县](../Page/建安县.md "wikilink")，是为建瓯地区建制之始。[三国时](../Page/三国.md "wikilink")[东吴设置的](../Page/东吴.md "wikilink")[建安郡](../Page/建安郡.md "wikilink")、[唐朝设置的](../Page/唐朝.md "wikilink")[建州](../Page/建州_\(唐朝\).md "wikilink")、乃至[南宋始置的](../Page/南宋.md "wikilink")[建宁府](../Page/建宁府.md "wikilink")，皆以建安县为治所。[五代十国时期](../Page/五代十国.md "wikilink")，[王延政建立的](../Page/王延政.md "wikilink")[殷国以建州为国都](../Page/闽国.md "wikilink")。1913年（[民国二年](../Page/民国纪年.md "wikilink")），建宁府被撤销，建安县和[瓯宁县被合并为建瓯县](../Page/瓯宁县.md "wikilink")，该县辖境大致与今日建瓯市相同。

建瓯在福建历史上曾有举足轻重地位，“福建”这一省名，便是从[福州](../Page/福州.md "wikilink")、建州二地地名中各取一字而来的。唐、宋期间，建州经济发达，辖区广大（从浦城县到永定县）；建安县为茶叶、木材、陶瓷、造酒、铸钱、书坊中心，出现过众多著名历史人物，文教兴盛。这一繁荣的景象一直延续到清朝。

1949年后，中国与[苏联合作计划在](../Page/苏联.md "wikilink")[建瓯盆地内建设](../Page/建瓯盆地.md "wikilink")[建溪水库](../Page/建溪水库.md "wikilink")，开始动迁城区，地区行政中心也转移到[建阳县](../Page/建阳县.md "wikilink")（今[建阳区](../Page/建阳区.md "wikilink")）和[南平县](../Page/南平县.md "wikilink")（今[延平区](../Page/延平区.md "wikilink")）。此计划后来因[中苏交恶而中止](../Page/中苏交恶.md "wikilink")，但地区中心位置的转移、交通条件落后等因素，使建瓯失去发展机会，逐渐没落成为一座默默无闻的县。1992年，经[国务院批准](../Page/国务院.md "wikilink")，撤销[建瓯县](../Page/建瓯县.md "wikilink")，设置县级市建瓯市。

## 历史沿革

[西周之前](../Page/西周.md "wikilink")，属于[七闽地](../Page/七闽.md "wikilink")，为蛮荒，史书记载甚少。[战国末年](../Page/战国.md "wikilink")，[越国为](../Page/越国.md "wikilink")[楚国所灭](../Page/楚国.md "wikilink")，越国宗室四处逃散，其中一支迁入[闽地](../Page/福建.md "wikilink")，自称[闽越王](../Page/闽越.md "wikilink")。其王[无诸在今建瓯溪南覆船山下建无诸行宫](../Page/无诸.md "wikilink")。

[秦废无诸闽越王号](../Page/秦.md "wikilink")，置闽中郡，治东冶地，降无诸为郡长。[西汉高祖五年](../Page/西汉.md "wikilink")，因為无诸带兵佐汉击楚，立下功勋，[刘邦复立无诸为闽越王](../Page/刘邦.md "wikilink")。武帝元鼎六年，平东越王叛，次年汉以闽越民风强悍，强行迁徙闽越王族平民于[江淮](../Page/江淮.md "wikilink")，闽越族逐渐没落并同化于[汉](../Page/汉.md "wikilink")。闽地空虚，于东冶旧地设一侯官驻军镇之，远属会稽南部都尉管辖。昭帝时因闽越遗民相聚渐多，立冶县。东汉初属改冶县为东侯官都尉。

东汉汉献帝建安元年（196年）[会稽太守](../Page/会稽.md "wikilink")[孙策攻侯官](../Page/孙策.md "wikilink")，废侯官都尉，并上表奏请于会稽南部分侯官北地设立[建安县](../Page/建安县.md "wikilink")（今建瓯）、[南平](../Page/南平市.md "wikilink")、汉兴（今[浦城](../Page/浦城县.md "wikilink")）三县，寓意建安年间、南方平定、汉室复兴，连同侯官县为福建最早正式设立的四县。

建安八年（203年）孙策令[贺齐进兵建安](../Page/贺齐.md "wikilink")，平定三县暴乱，设**南部都尉府**于建安县，析建安县桐乡设建平县（今[建阳](../Page/建阳.md "wikilink")），连同闽地原建安、南平、汉兴、侯官四县同属南部都尉府。建安县开始成为全闽地经济政治中心。

[三国](../Page/三国.md "wikilink")[吴景帝时](../Page/吴.md "wikilink")，因人口日益繁多，析建安县校乡设[邵武县](../Page/邵武.md "wikilink")，绥安（今泰宁、建宁县），将乐县等等。三国吴景帝永安三年（260年）撤销孙亮侯官封地，并于建安县新设设**建安郡**，改南部都尉为建安郡太守，下辖建安（今建瓯）、建平（今建阳）、吴兴（今浦城）、东平、将乐、邵武、绥安（今泰宁、建宁）、[南平](../Page/南平.md "wikilink")、侯官（今[福州](../Page/福州.md "wikilink")）、东安（今同安、[南安](../Page/南安.md "wikilink")）十县，基本囊括福建全境。并成为孙吴最大造船基地之一。

[晋武帝太康三年](../Page/晋.md "wikilink")（282年）析建安郡侯官、东安二县设晋安郡，治侯官县（今福州），于晋安郡新设八县。[南朝](../Page/南朝.md "wikilink")[陈文帝永定元年](../Page/陈.md "wikilink")（557年）合建安、晋安两郡为闽州，治侯官县（今福州）。天嘉三年（562年）晋安郡陈宝应拥兵自立，攻建安郡城，建安郡太守萧乾拼死抵抗，不为所制。陈文天嘉八年（567年）陈军入闽援建安，剿陈宝应，复建安郡，攻晋安郡，全复闽地。光大二年（568年）改为丰州，建安郡属之。光大年间析晋安郡地设南安郡，治今南安县丰州镇。

[隋文帝开皇三年](../Page/隋.md "wikilink")（583年），隋文帝将地方州、郡、县三级制改为州、县两级制。开皇九年（589年）隋军南征攻陈，于建安郡遭重创，日久城破，屠建安县城，闽地入隋。开皇九年平陈厚，废晋安、建安、南安三郡，改丰州为[泉州](../Page/泉州.md "wikilink")，州治闽县（今福州）。大业二年（606年）改泉州为闽州。大业三年（607年）改闽州为建安郡，治闽县（今福州），原建安郡城（今建瓯）为属县。

[唐高祖武德元年](../Page/唐.md "wikilink")（618年）改建安郡为**[建州](../Page/建州.md "wikilink")**，州治闽县。高祖武德四年（618年），迁建州州治于建安县，辖建安（今建瓯）、闽县（今福州）、唐兴（今浦城）、建阳、将乐、绥城（今建宁，泰宁）六县。武德六年（623年），析闽县地复设泉州（即今福州市），辖闽县、侯官、长乐、连江、长溪5县。武德八年（625年），建州建阳县并入建安县。

太宗贞观三年（629年），建州绥城并入邵武县。后复置沙县、将乐县、建阳县，同属建州。垂拱二年（689年），析泉州（今福州）南部分新设漳州。景云二年（711年），改泉州（今福州）为闽州，而新设[泉州于闽南](../Page/泉州.md "wikilink")。开元十三年（725年），因闽州西北有福山，改称福州都督府。玄宗开元二十一年（733年），各取福州、建州（州衙驻今建瓯）首字，设置**福建经略使**，闽地始有福建之称。

[五代后梁开平三年](../Page/五代.md "wikilink")（909年），封[王审知为闽主](../Page/王审知.md "wikilink")。五代后晋高祖[天福六年](../Page/天福_\(后晋\).md "wikilink")（941年），封王审知之子王延政建州建安镇安军王延政富沙王，此后与闽主互攻不已。五代后晋天福八年（943年），富沙王·王延政在建州称帝，改国号为**大殷**（殷国），次年复改为**大闽**，复称闽王，以建州（今建瓯）为首都，辖福建全境福、建、泉、漳、镛（今将乐）、镡州（今南平）六州。

[南唐保大四年](../Page/南唐.md "wikilink")（945年）唐兵攻闽国，破建州城，劫掠一空，并迁闽王族于[金陵](../Page/金陵.md "wikilink")。改建州为**永安军**，旋改**忠义军**，下辖建安（今建瓯）、闽县（今福州）、浦城、将乐、建阳、松源（新设，今松溪）、建宁、归化八县。

[北宋太祖开宝八年](../Page/北宋.md "wikilink")（975年），平南唐，复称**建州**。太平兴国五年（980年），割邵武、归化、建宁三县新设邵武军。太宗端拱元年（988年），于建安县设**建宁军节度使**，统辖闽北部西部七县。淳化五年（994年），于建州崇安场新设崇安县（今[武夷山市](../Page/武夷山.md "wikilink")）。真宗咸平三年（1000年），割建安县五里、关隶镇新设关隶县（今[政和县](../Page/政和.md "wikilink")）。

英宗治平三年（1066年），于建安县分置**瓯宁县**，治建安县宁远门侧，与建宁节度军、建州、建安县四级政府同城而治。此时建宁军辖建安、建阳、浦城、松溪、崇安（今武夷山）、瓯宁、关隶（今政和）七县。熙宁三年（1070年）将瓯宁县并入建安县。元祐四年（1089年）又分置瓯宁县。

[南宋高宗建炎二年](../Page/南宋.md "wikilink")（1128年），建州叶侬叛乱，建州治所往北逃迁建阳县。旋复。建炎四年建州范汝为起义，建州治所更远迁崇安县。绍兴二年（1132年），[韩世忠率兵攻建州](../Page/韩世忠.md "wikilink")，并欲屠城，[李纲救之](../Page/李纲.md "wikilink")。南宋高宗绍兴三十二年（1162年），[宋孝宗即位](../Page/宋孝宗.md "wikilink")，升原藩国建州为**[建宁府](../Page/建宁府.md "wikilink")**，为闽境第一府。辖境相当今福建北部建瓯以北建溪流域和周宁，寿宁两县。至此福建设有建宁府，福、泉、漳、汀、南剑（今南平）五州，邵武、兴化二州。统称福建为八闽，而建宁府为八闽首府，八闽上郡。

[元世祖至元十七年](../Page/元.md "wikilink")（1280年），建宁府黄华头陀军起义抗元。元世祖至元二十九年（1282年），[马可·波罗过建宁府](../Page/马可·波罗.md "wikilink")。[明景泰六年](../Page/明.md "wikilink")（1455年），封建宁府**朱熹**九世孙世袭翰林五经博士，敕建博士府于朱子祠左。

[清顺治五年](../Page/清.md "wikilink")（1648年戊子），清军攻建宁府城，伐明宗室郧西王。不久城破，屠城，全城人口幸存不过百。城池基本被夷为平地。清光绪二十五年（1899年），建宁府群众爆发反对西方列强的“[建宁教案](../Page/建宁教案.md "wikilink")”。

民国二年（1913年），撤消建宁府，全省分为**建安道**、厦门道、闽海道、漳汀道四道。将原建宁府建安、瓯宁两县合并为**建瓯县**。1926年，于建瓯县成立闽北第一个共产党支部。1927年，成立**[中共闽北临委](../Page/中共闽北临委.md "wikilink")**，为中共福建省委前身。1940年，劃建瓯县第一大镇水吉镇设水吉县，后水吉县并入建阳县。1940－1949年，设**第一行政督察区**驻建瓯，辖闽北十县。

中华人民共和国成立后到文革的混乱行政区划调整后，于闽北设”第一地区（行政公署）“，公署驻建瓯，后搬迁至建阳，与闽中地区·南平的”第三行署“合并，行署改驻南平；代管闽北五县五市，建瓯为代管市之一。1992年，撤县改市
，设立**建瓯市**，定位县级市。

## 地理

建瓯地处[东经](../Page/东经.md "wikilink")117度58分-118度57分，[北纬](../Page/北纬.md "wikilink")26度38分-27度20分之间，北邻[建阳区](../Page/建阳区.md "wikilink")，南接[延平区](../Page/延平区.md "wikilink")、[古田县](../Page/古田县.md "wikilink")，东靠[政和](../Page/政和县.md "wikilink")、[屏南县](../Page/屏南县.md "wikilink")，西与[顺昌县交界](../Page/顺昌县.md "wikilink")。

地势东南高西北低。中部为丘陵谷地，多[峡谷](../Page/峡谷.md "wikilink")[盆地](../Page/盆地.md "wikilink")。东南部为中山[山地](../Page/山地.md "wikilink")，西部为低山山地，北部为中低山地。

主要河流有[建溪及其支流](../Page/建溪.md "wikilink")[崇阳溪](../Page/崇阳溪.md "wikilink")、[南浦溪](../Page/南浦溪.md "wikilink")、[松溪](../Page/松溪.md "wikilink")、[小桥溪](../Page/小桥溪.md "wikilink")。

## 行政区划

建瓯市下辖4个街道、10个镇和4个乡。

街道：[建安街道](../Page/建安街道_\(建瓯市\).md "wikilink")、[通济街道](../Page/通济街道_\(建瓯市\).md "wikilink")、[瓯宁街道](../Page/瓯宁街道.md "wikilink")、[芝山街道](../Page/芝山街道.md "wikilink")

镇：[徐墩镇](../Page/徐墩镇.md "wikilink")、[吉阳镇](../Page/吉阳镇_\(建瓯市\).md "wikilink")、[房道镇](../Page/房道镇.md "wikilink")、[南雅镇](../Page/南雅镇_\(建瓯市\).md "wikilink")、[迪口镇](../Page/迪口镇.md "wikilink")、[小桥镇](../Page/小桥镇_\(建瓯市\).md "wikilink")、[玉山镇](../Page/玉山镇_\(建瓯市\).md "wikilink")、[东游镇](../Page/东游镇.md "wikilink")、[东峰镇](../Page/东峰镇_\(建瓯市\).md "wikilink")、[小松镇](../Page/小松镇_\(建瓯市\).md "wikilink")

乡：[顺阳乡](../Page/顺阳乡.md "wikilink")、[水源乡](../Page/水源乡_\(建瓯市\).md "wikilink")、[川石乡](../Page/川石乡.md "wikilink")、[龙村乡](../Page/龙村乡.md "wikilink")

## 气候

建瓯属[亚热带海洋性季风气候](../Page/亚热带海洋性季风气候.md "wikilink")，年平均气温19.3摄氏度，降雨量1600-1800毫米，日照1612小时，无霜期286天。

## 交通

[Jian'ou_Railway_Station_2012.08.25_10-05-40.jpg](https://zh.wikipedia.org/wiki/File:Jian'ou_Railway_Station_2012.08.25_10-05-40.jpg "fig:Jian'ou_Railway_Station_2012.08.25_10-05-40.jpg")[建瓯站](../Page/建瓯站.md "wikilink")\]\]

  - 铁路：[峰福铁路](../Page/峰福铁路.md "wikilink")，设[建瓯站](../Page/建瓯站.md "wikilink")、
    [京台高速铁路](../Page/京台高速铁路.md "wikilink")，设[建瓯西站](../Page/建瓯西站.md "wikilink")
  - 公路：[国道205线](../Page/国道205线.md "wikilink")、[省道204线](../Page/省道204线.md "wikilink")、[省道303线](../Page/省道303线.md "wikilink")、[京台高速公路](../Page/京台高速公路.md "wikilink")、[长深高速公路](../Page/长深高速公路.md "wikilink")
  - 机场：[建瓯大洲飞机场](../Page/建瓯大洲飞机场.md "wikilink")(现已废弃)
  - 码头：[通济门码头](../Page/通济门码头.md "wikilink")(现已废弃)

## 物产

金属矿主要有[铁](../Page/铁.md "wikilink")
、[铜](../Page/铜.md "wikilink")、[铅](../Page/铅.md "wikilink")、[锌等](../Page/锌.md "wikilink")，非金属矿主要有[煤](../Page/煤.md "wikilink")、[白云石](../Page/白云石.md "wikilink")、[石英石](../Page/石英石.md "wikilink")、[叶蜡石](../Page/叶蜡石.md "wikilink")、[硅灰石](../Page/硅灰石.md "wikilink")、[滑石等](../Page/滑石.md "wikilink")。

建瓯市的主要农产品包括[北苑御茶](../Page/北苑御茶.md "wikilink")、[乌龙茶](../Page/乌龙茶.md "wikilink")、[锥栗](../Page/锥栗.md "wikilink")、[柑橘](../Page/柑橘.md "wikilink")、[白莲](../Page/白莲.md "wikilink")、[毛竹及](../Page/毛竹.md "wikilink")[竹笋等](../Page/竹笋.md "wikilink")。其毛竹林面积124万亩，名列“[中国竹子之乡](../Page/中国竹子之乡.md "wikilink")”榜首。

## 人口

2010年第六次全国人口普查全市户籍人口538910人，常住人口452174人。\[1\]

## 风景名胜

  - 全国重点文物保护单位：[建瓯东岳庙](../Page/建瓯东岳庙.md "wikilink")
  - 福建省文物保护单位：[建瓯文庙](../Page/建瓯文庙.md "wikilink")（建宁府孔庙）、[建瓯通仙门](../Page/建瓯通仙门.md "wikilink")、[北苑凿字岩石刻](../Page/北苑凿字岩石刻.md "wikilink")、[中共闽北临时特委旧址](../Page/中共闽北临时特委旧址.md "wikilink")
  - 建瓯市文物保护单位：城南[光孝寺](../Page/光孝寺_\(建瓯\).md "wikilink")、小松镇李园村报恩寺、小松镇大庙村文昌阁、玉山镇榧村大圣庙、通济门等
  - [归宗岩](../Page/归宗岩.md "wikilink")
  - [万木林自然保护区](../Page/万木林自然保护区.md "wikilink")
  - [辰山](../Page/辰山.md "wikilink")

## 特色小吃

  - [建瓯板鸭](../Page/建瓯板鸭.md "wikilink")（中国四大板鸭之首）
  - [建瓯光饼](../Page/建瓯光饼.md "wikilink")
  - [粿包](../Page/粿包.md "wikilink")
  - [薄饼](../Page/薄饼.md "wikilink") (春卷)
  - [豆腐酿粉](../Page/豆腐酿粉.md "wikilink") (豆浆粉)
  - [扁食](../Page/扁食.md "wikilink") (馄饨)
  - [纳底](../Page/纳底.md "wikilink")
  - [炒底](../Page/炒底.md "wikilink") (又名窝底)
  - [鸡茸](../Page/雞茸_\(建甌\).md "wikilink")
  - [粉丸](../Page/粉丸.md "wikilink")
  - [鼎边粿](../Page/鼎边粿.md "wikilink") (锅边糊)
  - [芋饺](../Page/芋饺.md "wikilink")

## 民间艺术

建瓯市的建瓯[挑幡被列入](../Page/挑幡.md "wikilink")[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

## 民俗技艺

建瓯市的[弓鱼被列入](../Page/弓鱼.md "wikilink")[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

## 教育

  - [建瓯一中](../Page/建瓯一中.md "wikilink")
  - [建瓯二中](../Page/建瓯二中.md "wikilink")
  - [建瓯三中](../Page/建瓯三中.md "wikilink")
  - [建瓯四中](../Page/建瓯四中.md "wikilink")
  - [建瓯芝华中学(五中)](../Page/建瓯芝华中学\(五中\).md "wikilink")
  - [建瓯六中(职业中专)](../Page/建瓯六中\(职业中专\).md "wikilink")
  - [建瓯七中](../Page/建瓯七中.md "wikilink")
  - [建州高中](../Page/建州高中.md "wikilink")

## 参考文献

## 外部链接

  - [建瓯市人民政府网站](https://web.archive.org/web/20080704165642/http://www.jianou.gov.cn/)

[\*](../Category/建瓯市.md "wikilink")
[Category:福建县市级城市](../Category/福建县市级城市.md "wikilink")

1.