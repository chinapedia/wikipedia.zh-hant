**創世電視**（）是由[香港](../Page/香港.md "wikilink")[基督新教影音機構](../Page/基督新教.md "wikilink")「[影音使團](../Page/影音使團.md "wikilink")」在2003年所創立之[電視頻道](../Page/電視頻道.md "wikilink")，是香港首個24小時基督教電視台，運作費用完全依靠來自[基督徒及各個](../Page/基督徒.md "wikilink")[教會的定期](../Page/教會.md "wikilink")[奉獻和](../Page/捐獻.md "wikilink")[賣旗籌款維持](../Page/賣旗籌款.md "wikilink")。節目最初只在[香港有線電視](../Page/香港有線電視.md "wikilink")
15頻道播放，後來擴展到[Now TV](../Page/Now_TV.md "wikilink")
545頻道及[香港寬頻bbTV](../Page/香港寬頻bbTV.md "wikilink")
719頻道播放。除了收費電視外，目前創世電視還可以透過創世電視網頁、創世電視[流動應用程式及](../Page/流動應用程式.md "wikilink")[YouTube直播串流免費收看](../Page/YouTube.md "wikilink")。2017年12月31日起於[myTV
SUPER](../Page/myTV_SUPER.md "wikilink")
602頻道播出，並增設創世電視點播區\[1\]。創世電視會有兩條頻道播出：myTV
SUPER會以[高清頻道播放](../Page/高清電視.md "wikilink")，有線電視及[now寬頻電視會以](../Page/now寬頻電視.md "wikilink")[標清頻道播放](../Page/標清電視.md "wikilink")，而且兩條頻道節目播出時間是不同的。

創世電視之總負責人，是影音使團的總幹事[袁文輝](../Page/袁文輝.md "wikilink")。曾任[亞洲電視執行董事及高級副總裁的](../Page/亞洲電視.md "wikilink")[葉家寶](../Page/葉家寶.md "wikilink")，2017年3月加入創世電視任總監\[2\]。

## 台徽意義

創世電視之台徽，概念來自《[聖經](../Page/聖經.md "wikilink")》中[耶穌受洗後](../Page/耶穌受洗.md "wikilink")[聖靈像鴿子降臨的記載](../Page/聖靈.md "wikilink")。鴿子的翅膀，代表上帝的看顧、保守，更代表上帝喜悅的好訊息，象徵著[傳福音使者的力量](../Page/傳福音.md "wikilink")。台徽影像的顏色是以電視[三原色組合而來](../Page/三原色.md "wikilink")。三原色重疊是白色，正象徵[三位一體的上帝](../Page/三位一體.md "wikilink")，又帶出豐富和精彩的意思。

## 参考文献

## 外部連結

  - [香港創世電視網頁](http://www.creation-tv.com/ctv_hk/)

  - [美國創世電視網頁](http://us.creation-tv.com/)

  -
  -
  - [創世電視Android流動應用程式](https://play.google.com/store/apps/details?id=com.tme.video&hl=zh_HK)

  - [創世電視iOS流動應用程式](https://itunes.apple.com/hk/app/chuang-shi-dian-shi-creationtv/id791877152?mt=8)

{{-}}

[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:香港基督教新教组织](../Category/香港基督教新教组织.md "wikilink")
[Category:基督教電視台](../Category/基督教電視台.md "wikilink")
[Category:网络电视](../Category/网络电视.md "wikilink")

1.  [馬丁路德改教500周年 創世電視登陸myTV SUPER
    祝福全港700萬人](http://www.media.org.hk/tme_main/index.php/action/1031)
2.  [葉家寶任創世電視總監](http://news.mingpao.com/pns/dailynews/web_tc/article/20170421/s00016/1492712178080)