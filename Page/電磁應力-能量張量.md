[物理學中](../Page/物理學.md "wikilink")，**電磁應力-能量張量**是指由[電磁場貢獻於](../Page/電磁場.md "wikilink")[應力-能量張量](../Page/應力-能量張量.md "wikilink")（又稱[能量-動量張量](../Page/能量-動量張量.md "wikilink")）的部份。在自由空間中，以[國際單位制之單位可表示成](../Page/國際單位制.md "wikilink")：

\[T^{\alpha\beta} = \frac{1}{\mu_o}[ -F^{\alpha \gamma}F_{\gamma}{}^{\beta} - \frac{1}{4}g^{\alpha\beta}F_{\gamma\delta}F^{\gamma\delta}]\].
若以明顯的矩陣形式，可寫為：

\[T^{\alpha\beta} =\begin{bmatrix} \frac{1}{2}(\epsilon_o E^2+\frac{1}{\mu_0}B^2) & S_x & S_y & S_z \\
S_x & -\sigma_{xx} & -\sigma_{xy} & -\sigma_{xz} \\
S_y & -\sigma_{yx} & -\sigma_{yy} & -\sigma_{yz} \\
S_z & -\sigma_{zx} & -\sigma_{zy} & -\sigma_{zz} \end{bmatrix}\],

其中

  -
    [坡印廷向量](../Page/坡印廷向量.md "wikilink")
    \(\vec{S}=\frac{1}{\mu_o}\vec{E}\times\vec{B}\),
    [電磁場張量](../Page/電磁場張量.md "wikilink") \(F_{\alpha\beta}\!\),
    [度規張量](../Page/度規張量.md "wikilink") \(g_{\alpha\beta}\!\)，以及
    [馬克士威應力張量](../Page/馬克士威應力張量.md "wikilink") <math>\\sigma_{ij} =
    \\epsilon _o E_i E_j + \\frac{1}

{{\\mu _0 }}B_i B_j - \\frac{1} {2}\\left( {\\epsilon _o E^2 +
\\frac{1} {{\\mu _0 }}B^2 } \\right)\\delta _{ij} </math>.
注意到\(c^2=\frac{1}{\epsilon_o \mu_0}\)，而*c*是[真空中光速](../Page/光速.md "wikilink")。

若以[cgs制單位表示](../Page/cgs制.md "wikilink")，我們可以很簡單地用\(\frac{1}{4\pi}\)取代\(\epsilon_o\,\)，以及用\(4\pi\,\)取代\(\mu_o\,\):

\[T^{\alpha\beta} = \frac{1}{4\pi} [ -F^{\alpha \gamma}F_{\gamma}{}^{\beta} - \frac{1}{4}g^{\alpha\beta}F_{\gamma\delta}F^{\gamma\delta}]\].
若以明顯的矩陣形式，可寫為：

\[T^{\alpha\beta} =\begin{bmatrix} \frac{1}{8\pi}(E^2+B^2) & S_x/c & S_y/c & S_z/c \\ S_x/c & -\sigma_{xx} & -\sigma_{xy} & -\sigma_{xz} \\
S_y/c & -\sigma_{yx} & -\sigma_{yy} & -\sigma_{yz} \\
S_z/c & -\sigma_{zx} & -\sigma_{zy} & -\sigma_{zz} \end{bmatrix}\]

其中，[坡印廷向量變成如下形式](../Page/坡印廷向量.md "wikilink")：

\[\vec{S}=\frac{c}{4\pi}\vec{E}\times\vec{H}\].

[介電材料中的電磁應力](../Page/介電.md "wikilink")-能量張量則較不為人所了解，並且其為未解決的的主題。
(however see Pfeifer et al., Rev. Mod. Phys. 79, 1197 (2007))

能量-動量張量的其中元素（或說分量）\(T^{\alpha\beta}\!\)代表了電磁場的[四維動量](../Page/四維動量.md "wikilink")，其第α個分量——\(P^{\alpha}\!\)通過一[超平面](../Page/超平面.md "wikilink")(hyperplane)「*x*<sup>β</sup>
=
常數」之通量(flux)。其代表了電磁場這個物理客體所帶有的能量、動量及應力，對於重力場（時空曲率）會有怎樣的重力場源貢獻。這些課題出現在[廣義相對論中](../Page/廣義相對論.md "wikilink")。

## 相關條目

  - [應力-能量張量](../Page/應力-能量張量.md "wikilink")

[D](../Category/張量.md "wikilink") [D](../Category/電磁學.md "wikilink")
[D](../Category/廣義相對論所用張量.md "wikilink")