《**COMIC
FANS**》是由[香港的](../Page/香港.md "wikilink")[天下出版社出版的](../Page/天下出版社.md "wikilink")[少女漫畫](../Page/少女漫畫.md "wikilink")[月刊](../Page/漫畫雜誌.md "wikilink")，偏-{向}-日式少女畫風。每月隨漫畫附送印有連載漫畫中的漫畫人物圖案的贈品。於1995年8月創刊，首於當年的漫畫節推出，由於大受歡迎，創刊號共推出了2次再版。根據創刊號的一篇感想（刊登於創刊號第二頁），能夠促成Comic
fans的出版，是由於當時有不少讀者看過[美少女戰士的單行本後](../Page/美少女戰士.md "wikilink")，經常致電查詢有關新一期[美少女戰士的出版時間](../Page/美少女戰士.md "wikilink")，所以天下出版社就推出了這本漫畫月刊。Comic
fans所連載的日本漫畫均來自日本[講談社旗下的少女漫畫月刊](../Page/講談社.md "wikilink")。而近幾年亦多有幾位[香港漫畫家的作品](../Page/香港漫畫家.md "wikilink")。

Comic fans在推出2012年6月號之後，正式宣布停刊。同年7月，天下出版社推出少女漫畫月刊雜誌 COMICフェス（Comic
Festival）\[1\]

## 已完結的連載

### 日本漫畫

  - [美少女戰士 Super S](../Page/美少女战士.md "wikilink")
  - [美少女戰士 Sailor Star](../Page/美少女战士.md "wikilink")
  - [魔法騎士2](../Page/魔法騎士.md "wikilink")
  - [閃閃音符](../Page/閃閃音符.md "wikilink")
  - [偶像小廚師Delicious](../Page/偶像小廚師Delicious.md "wikilink")
  - [怪盜St. Tail](../Page/怪盜St._Tail.md "wikilink")
  - [Card Captor 櫻](../Page/百變小櫻.md "wikilink")（百變小櫻）
  - [千璐璐EXCELLENT\!](../Page/千璐璐EXCELLENT!.md "wikilink")（短篇）
  - [Rain Kiss](../Page/Rain_Kiss.md "wikilink")（短篇）
  - [美少女戰士 Sailor V](../Page/美少女戰士_Sailor_V.md "wikilink")
  - [PQ Angles](../Page/PQ_Angles.md "wikilink")
  - [夢幻傳說](../Page/夢幻傳說.md "wikilink")
  - [CuiteN](../Page/CuiteN.md "wikilink")
  - [B-Wanted](../Page/B-Wanted.md "wikilink")
  - [戀愛的心](../Page/戀愛的心.md "wikilink")（短篇）
  - [夢之蠟筆王國](../Page/夢之蠟筆王國.md "wikilink")
  - [DA\!DA\!DA\!](../Page/外星BB撞地球.md "wikilink")（外星BB撞地球）
  - [新DA\!DA\!DA\!](../Page/新DA!DA!DA!.md "wikilink")
  - [電腦少女★Mink](../Page/電腦少女★Mink.md "wikilink")
  - [東京Mew Mew](../Page/東京喵喵.md "wikilink")
  - [反斗Pet
    Pet](../Page/反斗Pet_Pet.md "wikilink")（後改名為[超大型狗物語](../Page/超大型狗物語.md "wikilink")）
  - [Wild\! 我的野蠻守護男](../Page/Wild!_我的野蠻守護男.md "wikilink")
  - [HONEY☆KIDS](../Page/HONEY☆KIDS.md "wikilink")
  - [美人魚Pichi Pichi Pitch](../Page/唱K小魚仙.md "wikilink")（唱K小魚仙）
  - [戀之魔法波波糖](../Page/戀之魔法波波糖.md "wikilink")
  - [製造王子計劃](../Page/製造王子計劃.md "wikilink")（於2007年2月號完結）
  - [優女夢遊仙境](../Page/優女夢遊仙境.md "wikilink")（於2007年3月號完結）
  - [純愛粉色系](../Page/純愛粉色系.md "wikilink")（於2007年12月號完結）
  - [妖怪之瞳](../Page/妖怪之瞳.md "wikilink")（於2008年6月號完結）
  - [魔界女王候補生](../Page/魔界女王候補生.md "wikilink")（於2008年8月號完結）
  - [小女神花鈴chu](../Page/小女神花鈴.md "wikilink")（短篇）
  - [Yes\!光之美少女5 GoGo\!](../Page/Yes!_光之美少女5.md "wikilink")（於2009年7月號完結）
  - [新地獄少女](../Page/新地獄少女.md "wikilink")（於2009年11月號完結）
  - [平凡戀愛](../Page/平凡戀愛.md "wikilink")(遠山繪麻)（於2010年12月號刊登）
  - [守護甜心小物語](../Page/守護甜心小物語.md "wikilink")（於2011年3月號完結）
  - [朱古力(福島春佳)](../Page/朱古力\(福島春佳\).md "wikilink")（於2011年3月號完結）
  - [妖界領航員★路娜](../Page/妖界領航員★路娜.md "wikilink")（於2011年4月號完結）

### 香港漫畫

  - [精靈少女魔法鼠](../Page/精靈少女魔法鼠.md "wikilink")（作畫：[雪晴](../Page/雪晴.md "wikilink")）
  - [同桌的你](../Page/同桌的你.md "wikilink")（短篇）（作畫：[雪晴](../Page/雪晴.md "wikilink")）
  - [PURE](../Page/PURE.md "wikilink")（短篇）（作畫：[雪晴](../Page/雪晴.md "wikilink")）
  - [小桃的動物園](../Page/小桃的動物園.md "wikilink")（作畫：[唯煒](../Page/唯煒.md "wikilink")）
  - [F.O.X
    I](../Page/F.O.X_I.md "wikilink")（作畫：[OS兔仔貓](../Page/OS兔仔貓.md "wikilink")，故事：[阿寶](../Page/阿寶.md "wikilink")）
  - [我的王子殿下](../Page/我的王子殿下.md "wikilink")（作畫：[瑞雲](../Page/瑞雲.md "wikilink")，故事：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）
  - [阿鱔週記](../Page/阿鱔週記.md "wikilink")（作畫：[瑞雲](../Page/瑞雲.md "wikilink")，原作：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）
  - [Mysteries～華麗偵探團～](../Page/Mysteries～華麗偵探團～.md "wikilink")(作畫：[瑞雲](../Page/瑞雲.md "wikilink")，原作：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）
  - [戀愛見習小魔女](../Page/戀愛見習小魔女.md "wikilink")（作畫：[OS兔仔貓](../Page/OS兔仔貓.md "wikilink")）
  - [天國貴公子DOLLY](../Page/天國貴公子DOLLY.md "wikilink")（作畫：[千草](../Page/千草.md "wikilink")）
  - [4x4王子日記](../Page/4x4王子日記.md "wikilink")（作畫：[風雅頌](../Page/風雅頌.md "wikilink")）
  - [幻境童心](../Page/幻境童心.md "wikilink")（作畫：[尼路燕](../Page/尼路燕.md "wikilink")，原作：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）
  - [STAR★CAGE](../Page/STAR★CAGE.md "wikilink")（作畫：[千草](../Page/千草.md "wikilink")，原作：[莉莉周](../Page/莉莉周.md "wikilink")）
  - [F.O.X
    II](../Page/F.O.X_II.md "wikilink")（作畫：[OS兔仔貓](../Page/OS兔仔貓.md "wikilink")，原作：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）
  - [蘑菇學園](../Page/蘑菇學園.md "wikilink")（作畫：[瑞雲](../Page/瑞雲.md "wikilink")，原作：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）
  - [戀愛Make
    up](../Page/戀愛Make_up.md "wikilink")（作畫：[風雅頌](../Page/風雅頌.md "wikilink")）
  - [華麗魔天使](../Page/華麗魔天使.md "wikilink")（作畫：[千草](../Page/千草.md "wikilink")）
  - [kiss2雙生兒](../Page/kiss2雙生兒.md "wikilink")(作畫:[雪睛](../Page/雪睛.md "wikilink"),原作:[余兒](../Page/余兒.md "wikilink"),[莉莉周](../Page/莉莉周.md "wikilink"))
  - [魔法漫畫學堂Gi\!](../Page/魔法漫畫學堂Gi!.md "wikilink")(作畫:[千草](../Page/千草.md "wikilink"))
  - [仙活草](../Page/仙活草.md "wikilink")(作畫:[千草](../Page/千草.md "wikilink"))(短篇)
  - [鏡之古都](../Page/鏡之古都.md "wikilink")(作畫:[愛芷](../Page/愛芷.md "wikilink"))
  - [彩虹魔法使](../Page/彩虹魔法使.md "wikilink")(作畫:[UMi](../Page/UMi.md "wikilink"))
  - [奇幻TaMaGo](../Page/奇幻TaMaGo.md "wikilink")(作畫:[千草](../Page/千草.md "wikilink"))
  - [吸血鬼達人](../Page/吸血鬼達人.md "wikilink")(作畫：[OS兔仔貓](../Page/OS兔仔貓.md "wikilink")，原作：[莉莉周](../Page/莉莉周.md "wikilink")）
  - [超模糖](../Page/超模糖.md "wikilink")(作畫：[雪晴](../Page/雪晴.md "wikilink")，原作：[莉莉周](../Page/莉莉周.md "wikilink")）
  - [愛麗斯奇夢檔案](../Page/愛麗斯奇夢檔案.md "wikilink")(作畫：[風雅頌](../Page/風雅頌.md "wikilink")）
  - [Mysteries～華麗偵探團～](../Page/Mysteries～華麗偵探團～.md "wikilink")（作畫：[瑞雲](../Page/瑞雲.md "wikilink")，原作：[狄芬妮琳](../Page/狄芬妮琳.md "wikilink")）

## 創刊號連載的內容

  - [美少女戰士 Super S](../Page/美少女戰士.md "wikilink")
  - [魔法騎士2](../Page/魔法騎士.md "wikilink")
  - [閃閃音符](../Page/閃閃音符.md "wikilink")
  - [精靈少女魔法鼠](../Page/精靈少女魔法鼠.md "wikilink")

## 現在主要連載漫畫作品

### 香港漫畫連載

  - [奇幻TaMaGo](../Page/奇幻TaMaGo.md "wikilink")(2009年11月號起連載)
  - [愛麗斯奇夢檔案](../Page/愛麗斯奇夢檔案.md "wikilink")(2010年2月號起連載，不定期刊載)
  - [LOVELY
    BLOOD～血型男子戀愛法～](../Page/LOVELY_BLOOD～血型男子戀愛法～.md "wikilink")(2011年2月號起連載)
  - [G.E.M.COMICS～祕密之城～](../Page/G.E.M.COMICS～祕密之城～.md "wikilink")(2010年10月號起連載)
  - [玻璃鞋騎士](../Page/玻璃鞋騎士.md "wikilink")(2010年8月號起連載)
  - [Secret Prince
    Ken～王子的秘密～](../Page/Secret_Prince_Ken～王子的秘密～.md "wikilink")(2011年4月號起連載)

### 日本漫畫連載

  - [地獄少女R](../Page/地獄少女R.md "wikilink")（2009年12月號起連載）
  - [你的音色～Your
    Melody～](../Page/你的音色～Your_Melody～.md "wikilink")（2011年4月號起連載）

## COMIC FANS連載的漫畫家

### 香港漫畫家

  - [雪晴](../Page/雪晴.md "wikilink")
  - [唯煒](../Page/唯煒.md "wikilink")
  - [OS兔仔貓](../Page/OS兔仔貓.md "wikilink")
  - [瑞雲](../Page/瑞雲.md "wikilink")
  - [千草](../Page/千草.md "wikilink")
  - [風雅頌](../Page/風雅頌_\(漫畫家\).md "wikilink")
  - [尼路燕](../Page/尼路燕.md "wikilink")
  - [愛芷](../Page/愛芷.md "wikilink")
  - [UMi](../Page/UMi.md "wikilink")

### 日本漫畫家

  - [武內直子](../Page/武內直子.md "wikilink")

  - [CLAMP](../Page/CLAMP.md "wikilink")

  -
  - [立川惠](../Page/立川惠.md "wikilink")

  - [阿弓唯](../Page/阿弓唯.md "wikilink")

  - [秋元奈美](../Page/秋元奈美.md "wikilink")

  - [安瑠安崎](../Page/安瑠安崎.md "wikilink")

  - [川村美香](../Page/川村美香.md "wikilink")

  - [片岡未知瑠](../Page/片岡未知瑠.md "wikilink")

  - [真伊藤](../Page/真伊藤.md "wikilink")

  - [花森小桃](../Page/花森小桃.md "wikilink")

  - [桃雪琴梨](../Page/桃雪琴梨.md "wikilink")

  - [安野夢洋子](../Page/安野夢洋子.md "wikilink")

  - [安藤夏美](../Page/安藤夏美.md "wikilink")

  - [菊田珠智代](../Page/菊田珠智代.md "wikilink")

  - [永遠幸](../Page/永遠幸.md "wikilink")

  - [福島春佳](../Page/福島春佳.md "wikilink")

  - [遠山繪麻](../Page/遠山繪麻.md "wikilink")

## COMIC FANS連載的編劇

### 香港編劇者

  - [阿寶](../Page/阿寶_\(編劇\).md "wikilink")
  - [ERICA LUK](../Page/ERICA_LUK.md "wikilink")
  - [狄芬妮琳](../Page/狄芬妮琳.md "wikilink")
  - [KAZE風](../Page/KAZE風.md "wikilink")
  - [莉莉周](../Page/莉莉周.md "wikilink")
  - [飯田唯詩](../Page/飯田唯詩.md "wikilink")
  - [張雪羅](../Page/張雪羅.md "wikilink")
  - [iMU](../Page/iMU.md "wikilink")

### 日本編劇者

  - [吉田玲子](../Page/吉田玲子.md "wikilink")
  - [橫手美智子](../Page/橫手美智子.md "wikilink")
  - [池田美代子](../Page/池田美代子.md "wikilink")
  - [地獄少女計劃](../Page/地獄少女計劃.md "wikilink")

## 外部連結

  - [天下官方網站](https://web.archive.org/web/20070110093538/http://www.comicsworld.com/)
  - [Comic Fans官方Facebook](http://www.facebook.com/comicfans.2012)
  - [Comic Fans官方微博](http://weibo.com/comicfansblog)

## 註釋

[Category:香港漫畫雜誌](../Category/香港漫畫雜誌.md "wikilink")

1.  <https://www.facebook.com/comicsfestival>