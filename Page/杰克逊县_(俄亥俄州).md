**傑克遜縣**（Jackson County,
Ohio）是位於[美國](../Page/美國.md "wikilink")[俄亥俄州南部的一個縣](../Page/俄亥俄州.md "wikilink")。面積1,092平方公里，根據[美國2000年人口普查數字](../Page/美國2000年人口普查.md "wikilink")，共有人口32,641人。縣治[傑克遜](../Page/傑克遜_\(俄亥俄州\).md "wikilink")（Jackson）。

成立於1816年。縣名紀念後來成為[總統的](../Page/美國總統.md "wikilink")[安德魯·傑克遜](../Page/安德魯·傑克遜.md "wikilink")。

## 參考文獻

[J](../Category/俄亥俄州行政區劃.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")
[Category:1816年俄亥俄州建立](../Category/1816年俄亥俄州建立.md "wikilink")