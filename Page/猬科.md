**猬科**（[学名](../Page/学名.md "wikilink")：）是**猬形目**（）的唯一一科，属[脊索动物门](../Page/脊索动物门.md "wikilink")[哺乳纲](../Page/哺乳纲.md "wikilink")，可分成[刺猬亚科和](../Page/刺猬亚科.md "wikilink")[鼠猬亚科两个亚科](../Page/鼠猬亚科.md "wikilink")。[刺猬亚科](../Page/刺猬亚科.md "wikilink")，有刺而尾短，可见于[欧亚大陆和](../Page/欧亚大陆.md "wikilink")[非洲许多地区](../Page/非洲.md "wikilink")。[鼠猬亚科](../Page/鼠猬亚科.md "wikilink")，无刺而尾长，分布于[东南亚](../Page/东南亚.md "wikilink")，在[中国南方也能见到](../Page/中国.md "wikilink")。
過去蝟科多分類在[食蟲目](../Page/食蟲目.md "wikilink")，但食蟲目實際上是一個[多系群而非一個系統發生學上的分類群](../Page/多系群.md "wikilink")，因此目前蝟科獨立成目，属于[真盲缺大目](../Page/真盲缺大目.md "wikilink")（）\[1\]。

## 照片集

Image:Hedgehog Gnesta.jpg|猬科([瑞典](../Page/瑞典.md "wikilink"))
Image:Hedgehog Gnesta 01.jpg Image:Hedgehog Gnesta 02.jpg

## 参考文献

[Category:蝟形目](../Category/蝟形目.md "wikilink")
[猬科](../Category/猬科.md "wikilink")

1.