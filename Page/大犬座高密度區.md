{{ Galaxy | | name =大犬座矮星系 | epoch =
[J2000](../Page/J2000.md "wikilink") | type = 不規則 | ra = \[1\] | dec =
\[2\] | dist_ly = [25](../Page/1_E20_m.md "wikilink")
[kly](../Page/light-year.md "wikilink") | z = | appmag_v = | size_v =
12 [度](../Page/度.md "wikilink") × 12 度 | constellation name =
[大犬座](../Page/大犬座.md "wikilink") | notes = - | names = CMa
矮星系\[3\] }}

**大犬座矮星系**位於[大犬座的方向上](../Page/大犬座.md "wikilink")，估計擁有十億顆的恆星，但[紅巨星的相對百分比偏高](../Page/紅巨星.md "wikilink")。

大犬座矮星系在分類上屬於[不規則星系](../Page/不規則星系.md "wikilink")，被認為是最接近我們的[銀河系的](../Page/銀河系.md "wikilink")[矮星系](../Page/矮星系.md "wikilink")，距離太陽系僅25,000[光年](../Page/光年.md "wikilink")，離銀河系的中心約42,000光年。它的外觀大致接近橢圓形，擁有的恆星數量與早先競逐最接近銀河系頭銜的[人馬座矮橢球星系相當](../Page/人馬座矮橢球星系.md "wikilink")。

## 發現

這個星系在2003年11月才被由法國、義大利、英國和澳大利亞的[天文學家共同組成的國際團隊發現](../Page/天文學家.md "wikilink")。雖然是最接近[地球的星系](../Page/地球.md "wikilink")，但因位於銀河盤面的後方，被濃密的恆星、氣體和[星際塵埃遮蔽](../Page/星際塵埃.md "wikilink")，因此難以偵測到；而體型的矮小，也是未能及早被發現的原因。

發現它的天文學家們是在對[2MASS的數據進行分析與校正時](../Page/2微米全天巡天.md "wikilink")，因為[紅外線不會像可見光一樣被氣體和星際塵埃吸收](../Page/紅外線.md "wikilink")，才得以發現的。由於有了這項技術，科學家才有能力在[大犬座滿佈](../Page/大犬座.md "wikilink")[M型恆星的天空中找到這個星系和其他相關的結構](../Page/恆星分類.md "wikilink")，包括兩個寬廣但微弱的弧。

## 特点

天文學家相信大犬座矮星系是正被銀河系巨大的引力拉扯中，且主體已經被剝離了的星系。[潮汐瓦解造成恆星在軌道的後方形成一條圍繞著銀河旋轉的恆星細流](../Page/潮汐力.md "wikilink")。複雜的環狀結構，像是已經環繞銀河三次的[麒麟座環](../Page/麒麟座環.md "wikilink")，是在21世紀初進行[史隆數位巡天時發現的](../Page/史隆數位巡天.md "wikilink")。而在調查這個環和鄰近空間內的[球狀星團的關係時](../Page/球狀星團.md "wikilink")，才發現這個類似[人馬座矮橢球星系的大犬座矮星系](../Page/人馬座矮橢球星系.md "wikilink")。

與大犬座矮星系有關聯性的[球狀星團包括](../Page/球狀星團.md "wikilink")[NGC
1851](../Page/NGC_1851.md "wikilink")、[NGC
1904](../Page/NGC_1904.md "wikilink")、[NGC
2298和](../Page/NGC_2298.md "wikilink")[NGC
2808](../Page/NGC_2808.md "wikilink")，這些都是被[銀河系吞噬或](../Page/銀河系.md "wikilink")[吸積過程的殘骸](../Page/吸積.md "wikilink")。[NGC
1261是另一個鄰近的星團](../Page/NGC_1261.md "wikilink")，但是因為速度上的不同，難以判定是否也屬於這個集團。大犬座矮星系也可能有些[疏散星團](../Page/疏散星團.md "wikilink")，包括[Dol
25和](../Page/Dol_25.md "wikilink")[H18](../Page/H18.md "wikilink")，[AM2可能也是](../Page/AM2.md "wikilink")。這些疏散星團被認為是矮星系受到重力擾動的刺激，才造成矮星系盤中的物質[形成恆星](../Page/恆星形成.md "wikilink")。

大犬座矮星系和隨後對星協分析上的發現，對現行星系的成長是經由吞噬鄰近的小星系進行的學說給予了證實和支持。馬丁等人相信，優勢的證據指向[銀河系的小](../Page/銀河系.md "wikilink")[衛星星系吸積時大致是在環繞](../Page/衛星星系.md "wikilink")[星系盤的平面上](../Page/星系盤.md "wikilink")

## 争议

[Yazan
Momany利用](../Page/Yazan_Momany.md "wikilink")[2MASS數據進行的新研究](../Page/2微米全天巡天.md "wikilink")，對矮星系的本質起了懷疑。這些資料認為'''大犬座高密度區
'''只是星系盤翹曲的部分。然而，這個論點對大犬座高密度的實際性質依然是一個未知的挑戰。

## 相關條目

  - [梅西爾 79](../Page/M79.md "wikilink")
  - [半人馬座ω星團](../Page/半人馬座ω星團.md "wikilink")
  - [星系的形成和演化](../Page/星系的形成和演化.md "wikilink")

## 參考資料

  - N. F. Martin, R. A. Ibata, M. Bellazzini, M. J. Irwin, G. F. Lewis,
    W. Dehnen, (February 2004). [A dwarf galaxy remnant in Canis Major:
    the fossil of an in-plane accretion onto the Milky
    Way.](http://xxx.lanl.gov/abs/astro-ph/0311010) *Monthly Notices of
    the Royal Astronomical Society* **348** (1) 12.
  - [Official press release announcing discovery (University of
    Strasbourg)](https://web.archive.org/web/20080527020725/http://astro.u-strasbg.fr/images_ri/canm-e.html)
  - [SEDS page on the Canis Major dwarf
    galaxy](https://web.archive.org/web/20070714155533/http://www.seds.org/messier/more/cma_dw.html)

<div class="references-small">

<references/>

</div>

## 外部連結

  - [Deriving The Shape Of The Galactic Stellar
    Disc](https://archive.is/20090114035201/http://www.skynightly.com/reports/Deriving_The_Shape_Of_The_Galactic_Stellar_Disc.html)
    (SkyNightly) Mar 17, 2006
  - [SEDS page on the Canis Major dwarf
    galaxy](http://messier.seds.org/more/cma_dw.html)

[Category:不規則星系](../Category/不規則星系.md "wikilink")
[Category:矮不規則星系](../Category/矮不規則星系.md "wikilink")
[Category:大犬座矮星系](../Category/大犬座矮星系.md "wikilink")
[Category:銀河系次集團](../Category/銀河系次集團.md "wikilink")
[Category:大犬座](../Category/大犬座.md "wikilink")

1.

2.
3.