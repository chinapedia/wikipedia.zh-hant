**金岳霖**（），字**龙荪**，生于[湖南](../Page/湖南.md "wikilink")[长沙](../Page/长沙.md "wikilink")，祖籍[浙江](../Page/浙江.md "wikilink")[诸暨](../Page/诸暨.md "wikilink")，中国[哲学家](../Page/哲学家.md "wikilink")、[逻辑学家](../Page/逻辑学家.md "wikilink")。

## 生平

[金岳霖院士.jpg](https://zh.wikipedia.org/wiki/File:金岳霖院士.jpg "fig:金岳霖院士.jpg")

### 出生

1895年7月14日出生于[大清](../Page/大清.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[长沙府](../Page/长沙府.md "wikilink")（今湖南省[长沙市](../Page/长沙市.md "wikilink")）。父亲[金珍原](../Page/金珍原.md "wikilink")，[浙江](../Page/浙江.md "wikilink")[诸暨人](../Page/诸暨.md "wikilink")，[太平天国时期为避战乱投奔堂叔祖金兆基来到湖南长沙](../Page/太平天国.md "wikilink")。母亲唐淑贤，湖南[衡阳人](../Page/衡阳.md "wikilink")。\[1\]

### 教育经历

1901年，金岳霖進入-{zh-cn|[胡子靖](../Page/胡子靖.md "wikilink")}-創辦的長沙[私立明德學堂讀四書五經](../Page/私立明德學堂.md "wikilink")，接受傳統教育。1907年進入美國教會創辦的[雅禮大學預科](../Page/雅禮中學.md "wikilink")。后考入[清华学堂](../Page/清华学堂.md "wikilink")（1912年改名[清华学校](../Page/清华学校.md "wikilink")）。

1914年，金岳霖毕业于清华学校高等科，同年官费留美，于1920年获美国[哥伦比亚大学的](../Page/哥伦比亚大学.md "wikilink")[政治学博士学位](../Page/政治学.md "wikilink")。后到英国学习，在[伦敦大学经济学院听课](../Page/伦敦大学.md "wikilink")。

### 讲学经历

金岳霖1925年回国，[清华大学聘请金岳霖讲授逻辑学](../Page/清华大学.md "wikilink")。秋，创办清华大学哲学系，任教授兼系主任。1938年，任[西南联大文学院心理学系教授兼清华大学哲学系主任](../Page/西南联大.md "wikilink")。

1948年，金岳霖被选为第一届[中央研究院](../Page/中央研究院.md "wikilink")[院士](../Page/院士.md "wikilink")。1949年后任清华大学文学院院长。1952年，全国高校院系调整，全国6所大学哲学系合并为北京大学哲学系，金岳霖历任[北京大学哲学系教授](../Page/北京大学.md "wikilink")、系主任，中国科学院哲学研究所一级研究员、副所长。1955年被聘为[中国科学院哲学社会科学部](../Page/中国科学院.md "wikilink")[学部委员](../Page/学部委员.md "wikilink")，9月底，任哲学研究所副所长兼逻辑研究组组长。1966年文革開始，金岳霖作為「資產階級學術權威」被批鬥，革命派不准其搭公車去醫院看病，只讓金岳霖踩著三輪木板車去看病，藉此羞辱他，毛澤東對其說過:「你要多接觸社會」。

1977年文革結束，任[中国社会科学院副所长兼研究室主任](../Page/中国社会科学院.md "wikilink")。

1984年，金岳霖在北京寓所逝世，享年89岁。

### 感情经历

终生未娶。1924年[趙元任夫婦赴歐洲旅行](../Page/趙元任.md "wikilink")，遇見留学的金岳霖跟在欧洲时结识的美國人[Lilian
Talor](../Page/Lilian_Talor.md "wikilink")（秦麗琳？秦麗蓮），后者1925年11月随后来到北京，与金同居；据传曾向[梁思成之妻](../Page/梁思成.md "wikilink")[林徽因示爱未果](../Page/林徽因.md "wikilink")\[2\]，与梁家是邻居和挚友；晚年一直都與其子[梁從誡一家住在一起](../Page/梁從誡.md "wikilink")，梁從誡則稱其為「金爸」，情同父子\[3\]。

## 学术成就

金岳霖从事哲学和逻辑学的教学、研究和组织领导工作，是最早把现代逻辑系统地介绍到中国来的主要人物。他把西方哲学与中国哲学相结合，建立了独特的哲学体系，培养了一大批高素养的哲学和逻辑学专门人才。著有：《[逻辑](../Page/逻辑_\(书籍\).md "wikilink")》、《[论道](../Page/论道.md "wikilink")》和《[知识论](../Page/知识论_\(书籍\).md "wikilink")》。出版有《金岳霖学术论文选》、《金岳霖文集》等。

1979年，[中国逻辑学会成立](../Page/中国逻辑学会.md "wikilink")，金岳霖担任首任会长和常务理事；常务理事还有中国数理逻辑教育和研究的开拓者，南京大学数学系教授[莫绍揆](../Page/莫绍揆.md "wikilink")，负责中国逻辑学会[数理逻辑分支学科](../Page/数理逻辑.md "wikilink")。\[4\]

## 受業

  - [殷海光](../Page/殷海光.md "wikilink")

## 参见

  - 有“逐林（林徽因）而居”的美谈？

## 参考文献

{{-}}

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中国科学院哲学社会科学部院士](../Category/中国科学院哲学社会科学部院士.md "wikilink")
[Category:中華人民共和國哲學家](../Category/中華人民共和國哲學家.md "wikilink")
[Category:中華民國哲學家](../Category/中華民國哲學家.md "wikilink")
[Category:中国逻辑学家](../Category/中国逻辑学家.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:国立西南联合大学教授](../Category/国立西南联合大学教授.md "wikilink")
[Category:國立清華大學教授](../Category/國立清華大學教授.md "wikilink")
[Category:哥伦比亚大学校友](../Category/哥伦比亚大学校友.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:國立清華大学校友](../Category/國立清華大学校友.md "wikilink")
[Category:长沙市雅礼中学校友](../Category/长沙市雅礼中学校友.md "wikilink")
[Category:諸暨人](../Category/諸暨人.md "wikilink")
[Category:长沙人](../Category/长沙人.md "wikilink")
[Yue岳](../Category/金姓.md "wikilink")
[Category:文革被迫害学者](../Category/文革被迫害学者.md "wikilink")

1.  [金岳霖的“双重身份”](http://book.sina.com.cn/excerpt/sz/rw/2012-02-13/1023295172.shtml)
    人民网评论部策划 2005年08月26日
2.  [层累的和真实的金岳霖、梁思成与林徽因](http://www.guancha.cn/culture/2013_05_05_142492.shtml)
3.  [最是人间四月天（下）](http://www.81890.gov.cn:88/LaoNian/sanwen/ShowArticle.asp?ArticleID=3470)
4.