**傅宗龍**（），字仲綸、元憲，號括蒼，[雲南](../Page/雲南.md "wikilink")[昆明](../Page/昆明.md "wikilink")[官渡傅家營人](../Page/官渡.md "wikilink")，曾官陝西三邊[總督](../Page/總督.md "wikilink")，討伐[流寇](../Page/流寇.md "wikilink")，被俘，不屈而死。追贈[太子少保](../Page/太子少保.md "wikilink")，[諡](../Page/諡.md "wikilink")**忠壯**。

## 簡介

萬曆三十八年（1610年）進士，任[銅梁](../Page/銅梁.md "wikilink")、[巴縣知縣](../Page/巴縣.md "wikilink")，調戶部主事。

天启四年（1624年）正月任[贵州巡抚兼](../Page/贵州巡抚.md "wikilink")[监军](../Page/监军.md "wikilink")，天启六年（1626年）击败[安邦彦](../Page/安邦彦.md "wikilink")，稳定西南，诏加[太仆少卿](../Page/太仆少卿.md "wikilink")。

崇禎三年（1631年）任為[右僉都御史](../Page/右僉都御史.md "wikilink")，[順天巡撫](../Page/順天巡撫.md "wikilink")。

崇禎十年（1637年），[楊嗣昌力薦](../Page/楊嗣昌.md "wikilink")，宗龍重新起用為[四川巡撫](../Page/四川巡撫.md "wikilink")。

崇禎十二年（1639年），宗龍被召赴京，以[邵捷春代為](../Page/邵捷春.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")。宗龍任[兵部尚書](../Page/兵部尚書.md "wikilink")，但不足三月因與崇禎帝不合被解職，下獄二年。

崇禎十四年（1641年），流寇破河南等境，[杨嗣昌病死](../Page/杨嗣昌.md "wikilink")，北京震動。尚书[陈新甲推荐傅宗龙](../Page/陈新甲.md "wikilink")，皇帝詔赦宗龍出獄，以兵部右侍郎兼[副都御史](../Page/副都御史.md "wikilink")，代替[丁启睿督陝兵討賊](../Page/丁启睿.md "wikilink")，总督陕西三边军务。

八月初旬，李自成有五十五万人攻占[河洛](../Page/河洛.md "wikilink")，陝西、三邊總督傅宗龍在崇禎皇帝催促下，進兵河南，兵力僅兩萬餘人，時关中各地遭旱灾蝗灾，糧餉極乏。九月四日，傅宗龙率川陕兵与保定总督[杨文岳部会合](../Page/杨文岳.md "wikilink")。在孟家莊（今[平輿境內](../Page/平輿.md "wikilink")）為[李自成](../Page/李自成.md "wikilink")、[羅汝才所襲](../Page/羅汝才.md "wikilink")，[賀人龍出逃](../Page/賀人龍.md "wikilink")，[李國奇潰敗](../Page/李國奇.md "wikilink")，[楊文岳亦徑行離去](../Page/楊文岳.md "wikilink")。僅剩宗龍孤軍力戰，被圍八日，糧盡彈絕，十一日杀骡马分食，骡马杀尽，後取尸体“分啖之”，十八日箭矢俱盡，宗龍計劃乘夜突围。十九日，在離[項城八里處](../Page/項城.md "wikilink")，宗龍為賊逮獲。賊軍想利用宗龍向守城官兵喊話。宗龍大罵：“我大臣也，殺則殺耳，豈能為賊賺城以緩死哉！”遂被殺害，死狀甚慘。

[汪喬年得知傅宗龍死訊](../Page/汪喬年.md "wikilink")，大哭說道：“傅公死，討賊無人矣。”後[汪喬年繼為陝西總督](../Page/汪喬年.md "wikilink")，亦力戰而亡。遺作收入[李根源](../Page/李根源.md "wikilink")《明滇南五名臣遺集》。

## 參考書目

  - 《明史》列傳第一百五十

[Category:明朝薊遼總督](../Category/明朝薊遼總督.md "wikilink")
[Category:明朝被處決者](../Category/明朝被處決者.md "wikilink")
[Category:昆明人](../Category/昆明人.md "wikilink")
[Z宗](../Category/傅姓.md "wikilink")
[Category:諡忠壯](../Category/諡忠壯.md "wikilink")