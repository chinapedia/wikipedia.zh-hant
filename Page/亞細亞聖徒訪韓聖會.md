**亞細亞聖徒訪韓聖會**是[韓國](../Page/韓國.md "wikilink")[基督教會主辦](../Page/基督教會.md "wikilink")，給[亞洲](../Page/亞洲.md "wikilink")[基督徒參加的大型聚會](../Page/基督徒.md "wikilink")。每年夏季7至8月間在[韓國舉行](../Page/韓國.md "wikilink")，有來自[台灣](../Page/台灣.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[美國](../Page/美國.md "wikilink")、[日本等地區的華人信徒參加](../Page/日本.md "wikilink")。在訪韓聖會舉行期間，參加者一連三日不停[敬拜](../Page/敬拜.md "wikilink")、[聚會](../Page/聚會.md "wikilink")、[禱告](../Page/禱告.md "wikilink")，及進行七餐[禁食](../Page/禁食.md "wikilink")，沒有房間，與數百人一起在大聖殿中度宿。澳門和台灣則有房間。

## 主辦單位

  - [以斯拉事奉中心](../Page/以斯拉事奉中心.md "wikilink")
  - [台北純福音教會](../Page/台北純福音教會.md "wikilink")
  - [汝矣島純福音教會](../Page/汝矣島純福音教會.md "wikilink")
  - [實業人聯合會東北亞宣教會](../Page/實業人聯合會東北亞宣教會.md "wikilink")

## 舉行地點

  - [韓國](../Page/韓國.md "wikilink")[崔子實紀念祈禱院](../Page/崔子實紀念祈禱院.md "wikilink")
  - [韓國](../Page/韓國.md "wikilink")[首爾](../Page/首爾.md "wikilink")[汝矣島純福音教會](../Page/汝矣島純福音教會.md "wikilink")

## 第十五屆亞細亞聖徒訪韓聖會

  - 2003年7月21日至7月28日

## 第十六屆亞細亞聖徒訪韓聖會

  - 2004年7月26日至8月2日
  - 主題：「醫治與復興」

## 第十七屆亞細亞聖徒訪韓聖會

  - 2005年7月25日至8月1日
  - 主題：「恢復與復興」

## 第十八屆亞細亞聖徒訪韓聖會

  - 2006年7月31日至8月7日
  - 主題：「敬拜與復興」

## 第十九屆亞細亞聖徒訪韓聖會

  - 2007年7月16至23日
  - 主題：「宣教與復興」
  - 大會講員：[趙鏞基牧師](../Page/趙鏞基.md "wikilink")、[洪英基牧師](../Page/洪英基.md "wikilink")、[張茂松牧師](../Page/張茂松.md "wikilink")、[何志滌牧師](../Page/何志滌.md "wikilink")、[包德寧牧師](../Page/包德寧.md "wikilink")、[楊寧亞牧師](../Page/楊寧亞.md "wikilink")、[連加恩宣教士等](../Page/連加恩.md "wikilink")

## 第二十屆亞細亞聖徒訪韓聖會

  - 2008年7月21日至7月24日
  - 主題：「職場與復興」
  - 大會講員：[申成南牧師](../Page/申成南.md "wikilink")、[張茂松牧師](../Page/張茂松.md "wikilink")、[李永熏牧師](../Page/李永熏.md "wikilink")、[洪性健牧師](../Page/洪性健.md "wikilink")、[夏忠堅牧師](../Page/夏忠堅.md "wikilink")、[陳一平牧師](../Page/陳一平.md "wikilink")、[康希牧師](../Page/康希.md "wikilink")、[腓力.曼都法牧師](../Page/腓力.曼都法.md "wikilink")、[楊寧亞牧師](../Page/楊寧亞.md "wikilink")、[劉群茂牧師](../Page/劉群茂.md "wikilink")、[寇紹恩牧師](../Page/寇紹恩.md "wikilink")、[周神助牧師](../Page/周神助.md "wikilink")、[趙鏞基牧師](../Page/趙鏞基.md "wikilink")、[韓世大學](../Page/韓世大學.md "wikilink")[金聖惠總長](../Page/金聖惠.md "wikilink")、[河用達牧師](../Page/河用達.md "wikilink")

## 第二十一屆亞細亞聖徒訪韓聖會

  - 2009年7月
  - 主題：「家庭與復興」

## 第二十二屆亞細亞聖徒訪韓聖會

  - 2010年7月26日至8月2日
  - 主題：「生命與復興」
  - 大會講員：[何志滌牧師](../Page/何志滌.md "wikilink")、[金炳三牧師](../Page/金炳三.md "wikilink")、[金聖惠總長](../Page/金聖惠.md "wikilink")、[楊寧亞牧師](../Page/楊寧亞.md "wikilink")、[張茂松牧師](../Page/張茂松.md "wikilink")、[李永熏牧師](../Page/李永熏.md "wikilink")、[洪性旭牧師](../Page/洪性旭.md "wikilink")、[鄭宰宇牧師](../Page/鄭宰宇.md "wikilink")、[金炯民牧師](../Page/金炯民.md "wikilink")、[鄭福財牧師](../Page/鄭福財.md "wikilink")、[趙鏞基牧師](../Page/趙鏞基.md "wikilink")

## 外部链接

  - [第20屆亞細亞聖徒訪韓聖會CD](http://www.ezra.com.tw/product_data.asp?prd_seq=69)
  - <https://web.archive.org/web/20160612192309/http://tw.ezrami.com/>
  - <https://web.archive.org/web/20060625171901/http://www.kingdomrevival.com.tw/krthome/index.php?id=2088>
  - <http://club.pchome.com.tw/bbs/www/cal_Read.php?club_e_name=jmcoltd&No=3451&usrid=Ym9xdzEyMw>
  - <https://web.archive.org/web/20071008134338/http://www.tpahk.com/site/modules/wfsection/article.php?articleid=166>
  - <https://archive.is/20130503064323/http://www.linqi.org/me/2006_korjap/index.html>
  - <https://web.archive.org/web/20070930022304/http://www.abundantlife.org.hk/slss/040822/council.html>
  - <https://web.archive.org/web/20170918194258/http://ncnc.us/>

[Category:基督教教會與教派](../Category/基督教教會與教派.md "wikilink")
[Category:韓國基督教](../Category/韓國基督教.md "wikilink")