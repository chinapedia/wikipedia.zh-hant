**阮惠**（，）又作**阮文惠**（），後改名**阮光平**（），是[越南歷史上一位著名军事人物](../Page/越南歷史.md "wikilink")，亦是[西山朝第二代皇帝](../Page/西山朝.md "wikilink")，1788年至1792年在位。因[年號](../Page/年號.md "wikilink")[光中](../Page/光中.md "wikilink")，史稱**光中皇帝**（）。他被今日的越南人認為是越南的民族英雄之一。

阮惠生於[歸仁府](../Page/歸仁府.md "wikilink")[西山邑](../Page/西山邑.md "wikilink")（今屬[平定省](../Page/平定省.md "wikilink")[西山縣](../Page/西山縣.md "wikilink")），當時屬於[鄭阮紛爭時期南方](../Page/鄭阮紛爭.md "wikilink")[舊阮的領地之內](../Page/舊阮.md "wikilink")。因不滿舊阮政府裡權臣當道，政局敗壞，阮惠便與兄長[阮岳](../Page/阮岳.md "wikilink")、[阮侶於](../Page/阮侶.md "wikilink")1771年，在[西山發動起義](../Page/西山邑.md "wikilink")。阮惠在戰事中表現驍勇，使西山阮氏兄弟勢如破竹，先後消滅南方舊阮、北方[鄭主及](../Page/鄭主.md "wikilink")[後黎朝](../Page/後黎朝.md "wikilink")，結束了越南二百多年來南北分裂之局，並擊退了來自[暹羅及](../Page/暹羅.md "wikilink")[中國](../Page/中國.md "wikilink")[清朝等](../Page/清朝.md "wikilink")「外國勢力」的軍事干預。但他與兄長阮岳之間的內鬨相攻，致使整個西山朝元氣大傷。他本人則於1792年準備全殲舊阮勢力期間去世。\[1\]

## 家世來歷及早年生活

[西山阮氏三兄弟](../Page/西山.md "wikilink")（[阮文岳](../Page/阮文岳.md "wikilink")、[阮文侶](../Page/阮文侶.md "wikilink")、阮文惠）本姓**胡**，\[2\]\[3\]與越南[胡朝皇帝](../Page/胡朝.md "wikilink")[胡季犛是同宗](../Page/胡季犛.md "wikilink")。其四世祖[胡丕康原居於越南北部](../Page/胡丕康.md "wikilink")[鄭氏勢力範圍下的](../Page/鄭主.md "wikilink")[乂安處](../Page/乂安省.md "wikilink")[英都府](../Page/英都府.md "wikilink")[興元縣](../Page/興元縣.md "wikilink")，在[鄭氏與](../Page/鄭主.md "wikilink")[廣南國阮氏的交戰當中](../Page/阮主.md "wikilink")，胡丕康被[阮福瀕所率的廣南軍隊俘虜](../Page/阮福瀕.md "wikilink")，安置在[歸仁府](../Page/歸仁府.md "wikilink")[西山邑](../Page/西山邑.md "wikilink")墾荒。由於胡丕康出生在書香門第，[阮主](../Page/阮主.md "wikilink")[阮福瀕遂任命他為](../Page/阮福瀕.md "wikilink")「西山寨主」。胡丕康的後代世襲西山寨主之職。到阮惠的父親[胡丕福時](../Page/胡丕福.md "wikilink")，遷居於[堅城邑](../Page/堅城邑.md "wikilink")（今[綏遠縣](../Page/綏遠縣.md "wikilink")[富樂村](../Page/富樂村.md "wikilink")）。\[4\]\[5\]\[6\]

阮岳、阮侶、阮惠都出生在堅城邑。後來阮岳也是因世襲而得到了「西山寨主」之職。而越南黎末學者[裴輝壁的](../Page/裴輝壁.md "wikilink")《乂安志》則記載，阮岳憑藉富裕世家而獲得寨主的官職。因此說西山阮氏三兄弟並非一般的農民家世，而是西山邑裡的一個「下層封建主小康之家」。\[7\]

阮惠原名**胡**（），\[8\]\[9\]其在年少時曾受到思想啟蒙而萌起造反念頭。他的父親胡丕福，曾向儒者[焦獻學習](../Page/焦獻.md "wikilink")。焦獻原姓[張](../Page/張姓.md "wikilink")，是儒者[張文行的兒子](../Page/張文行.md "wikilink")。當時張文行被舊阮的權臣張福巒殺害，焦獻逃亡到安泰邑，在此學習文武之術。焦獻認為其子[阮岳](../Page/阮岳.md "wikilink")、阮惠具備天才，非常器重他們，便激勵他們起兵反抗舊阮。\[10\]

## 參加西山起義

胡丕福死後，[阮岳繼承了西山寨主之職](../Page/阮岳.md "wikilink")，後阮岳擔任[雲屯](../Page/雲屯.md "wikilink")[巡卞吏](../Page/巡卞吏.md "wikilink")（負責徵收稅務的低級官員）。當時廣南阮主治下的領地官員貪污腐敗嚴重，而且克重稅。再加上連年的自然災害，導致農民起義此起彼伏。阮岳將稅收的[銀幣洗劫一空分給窮人](../Page/銀幣.md "wikilink")\[11\]（一說洗劫一空拿去[賭博](../Page/賭博.md "wikilink")\[12\]），因此受到[通緝](../Page/通緝.md "wikilink")。阮岳在不得已之下攜兩位弟弟逃往[西山邑](../Page/西山邑.md "wikilink")，成為盜賊。

1771年（[黎景興三十二年](../Page/景興.md "wikilink")），阮惠與兄長阮岳、[阮侶](../Page/阮侶.md "wikilink")，在西山邑建立屯寨，招納軍士。西山軍聲稱[張福巒腐敗](../Page/張福巒.md "wikilink")，提出要推翻張福巒和[阮福淳的統治](../Page/阮福淳.md "wikilink")，改立有賢能的皇孫[阮福暘為阮主](../Page/阮福暘.md "wikilink")。西山軍劫富濟貧，深得當地百姓的支持。為了更加得到百姓支持，三兄弟將姓氏改為母親的阮姓。西山三兄弟得到土豪[阮樁](../Page/阮樁.md "wikilink")、富商[玄溪的支持](../Page/玄溪.md "wikilink")，吸收了周圍的小股農民起義軍，又得到[集亭](../Page/集亭.md "wikilink")、[李才的起兵響應](../Page/李才.md "wikilink")，勢力越來越壯大。1773年，阮岳自稱第一寨主，以阮樁為第二寨主，玄溪為第三寨主。\[13\]

阮惠當時年僅19歲，但郤健壯而有膽略，在戰鬥中身先[士卒](../Page/士卒.md "wikilink")，因而在軍中甚得眾望。\[14\]而根據成書於阮朝年間的《[大南正編列傳初集](../Page/大南實錄.md "wikilink")·偽西列傳》記載，阮惠「聲如巨鐘，目閃閃若電光，狡黠善鬭」，因此「人皆憚之」。\[15\]

## 消滅南方舊阮

### 聯鄭擊阮

當北方的鄭主[鄭森得悉南方](../Page/鄭森_\(越南\).md "wikilink")[舊阮大亂後](../Page/舊阮.md "wikilink")，在1774年（[黎景興三十五年](../Page/景興.md "wikilink")），命大將[黃五福率水步三萬大軍](../Page/黃五福.md "wikilink")，以征討舊阮權奸[張福巒為名](../Page/張福巒.md "wikilink")，入侵舊阮領地。黃五福的軍隊勢如破竹，攻陷舊阮都城[富春](../Page/富春.md "wikilink")（今[順化](../Page/順化.md "wikilink")），阮主外逃[廣南府](../Page/廣南省.md "wikilink")。與此同時，西山阮軍隊亦正在從[歸仁府出發](../Page/平定省.md "wikilink")，欲攻取廣南府。阮岳、[李才](../Page/李才.md "wikilink")、[集亭率西山軍在](../Page/集亭.md "wikilink")[錦沙村](../Page/錦沙村.md "wikilink")（今[峴港市](../Page/峴港市.md "wikilink")[和榮縣](../Page/和榮縣.md "wikilink")）與鄭軍交戰失利，鄭軍進佔廣南，而南方的舊阮君主[阮福淳則逃到](../Page/阮福淳.md "wikilink")[嘉定](../Page/胡志明市.md "wikilink")（今[胡志明市及附近一帶](../Page/胡志明市.md "wikilink")），以圖重新站穩陣腳，收復失地。阮岳感到自己夾在舊阮和鄭主中間，認為有必要暫緩軍事壓力，便向黃五福講和，並要求成為征舊阮的前驅。鄭森同意了這個請求。\[16\]\[17\]

1775年（[黎景興三十六年](../Page/景興.md "wikilink")），阮岳認為向舊阮施襲的機會已到，便向舊阮軍將領[宋福洽詐稱議和](../Page/宋福洽.md "wikilink")，希望聯阮擊鄭；阮岳又獻女兒壽春於舊阮的東宮[阮福暘](../Page/阮福暘.md "wikilink")。乘宋福洽毫無戒心的時候，命阮惠進行突襲。阮惠不負所望，擊敗舊阮軍隊，佔領了[富安](../Page/富安.md "wikilink")。經此一役，阮惠獲鄭主封為「西山校前鋒將軍」。\[18\]\[19\]

### 攻克嘉定，覆滅舊阮

與此同時，[鄭主軍隊也大舉南下](../Page/鄭主.md "wikilink")。但旋即爆發瘟疫，大量鄭軍士兵死亡。黃五福便率軍放棄[廣南](../Page/廣南省.md "wikilink")，撤回[富春](../Page/富春.md "wikilink")。廣南之地亦被西山阮所佔。這時，[阮岳認為政權基礎已足夠穩固](../Page/阮岳.md "wikilink")，便於1776年（[黎景興三十七年](../Page/景興.md "wikilink")），先派弟弟[阮侶率軍攻擊嘉定地區](../Page/阮侶.md "wikilink")（今[胡志明市及附近一帶](../Page/胡志明市.md "wikilink")）。阮侶雖曾佔領重鎮嘉定城，但旋即被挺舊阮的[杜清仁驅逐](../Page/杜清仁.md "wikilink")，最後亦失利而回。\[20\]

次年（1777年，[黎景興三十八年](../Page/景興.md "wikilink")），阮岳再派[阮侶](../Page/阮侶.md "wikilink")、阮惠攻打嘉定的舊阮軍。嘉定被攻佔，阮福淳外逃[龍川](../Page/龍川道.md "wikilink")（今[金甌省](../Page/金甌省.md "wikilink")），阮福暘逃至\[\[巴𣾼|巴
 |- style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**光中帝阮惠** |-    |-

[Category:西山朝君主](../Category/西山朝君主.md "wikilink")
[Category:西山朝王爵](../Category/西山朝王爵.md "wikilink")
[Category:西山朝將領](../Category/西山朝將領.md "wikilink")
[Category:越南駙馬](../Category/越南駙馬.md "wikilink")
[Category:越南民變首領](../Category/越南民變首領.md "wikilink")
[Category:平定省人](../Category/平定省人.md "wikilink")
[Category:亞洲紙幣上的人物](../Category/亞洲紙幣上的人物.md "wikilink")
[Category:諡武](../Category/諡武.md "wikilink")
[Category:諡忠純](../Category/諡忠純.md "wikilink")
[H](../Category/阮姓.md "wikilink")

1.  《東南亞歷史詞典·「阮文惠」條》，183－184頁。

2.  [阮伯勳](../Page/阮伯勳.md "wikilink")，《西山潛龍錄》

3.  [Tạ Chí Đại Trường (1973), *Lịch Sử Nội Chiến Việt Nam 1771- 1802*,
    Sài Gòn: Nhà xuất bản Văn Sử Học, tr.
    49](http://www.dostbinhdinh.org.vn/DiaChiBD/Lichsu/ChuongIII_Sunghiep79-101.htm)


4.  陳仲金《越南史略》，254頁。

5.
6.  [楚狂](../Page/楚狂.md "wikilink")，《[西山外史](../Page/西山外史.md "wikilink")》

7.  郭振鐸、張笑梅《越南通史》，512-513頁。

8.
9.
10. 郭振鐸、張笑梅《越南通史》，512頁。

11.
12.
13. 《[大南實錄](../Page/大南實錄.md "wikilink")》正編列傳初集·卷三十 偽西列傳·阮文岳傳

14.
15. 《大南正編列傳初集·卷三十·偽西列傳》

16. 陳仲金《越南史略》，255-256頁。

17. 郭振鐸、張笑梅《越南通史》，514頁。

18. 陳仲金《越南史略》，256-257頁。

19.
20. 陳仲金《越南史略》，257-258頁。