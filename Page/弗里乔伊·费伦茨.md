**弗利克賽·費倫茨**（[匈牙利语](../Page/匈牙利语.md "wikilink")：****，），生于[布达佩斯](../Page/布达佩斯.md "wikilink")，卒于巴塞尔，是一位活跃在[匈牙利](../Page/匈牙利.md "wikilink")、[奥地利和](../Page/奥地利.md "wikilink")[德国的匈牙利裔](../Page/德国.md "wikilink")[指挥家](../Page/指挥家.md "wikilink")。

弗里乔伊在布达佩斯音乐学院学习，之后在军队里面当乐长，后来还在[赛格当乐队长](../Page/赛格.md "wikilink")。
后来他以乐队长和指挥的身份前往布达佩斯和[维也纳国家歌剧院](../Page/维也纳国家歌剧院.md "wikilink")。

弗里乔伊成名于1947年，在当年的[萨尔斯堡音乐节上代替](../Page/萨尔斯堡音乐节.md "wikilink")[奥托·克伦佩勒指挥丹东之死](../Page/奥托·克伦佩勒.md "wikilink")
。
1949年弗里乔伊成为柏林市立歌剧院首席指挥。但他很快就离开了，转而到了柏林美占区电台交响乐团（柏林RIAS广播交响乐团）上任，直至1954年。

1956年到1959年他成为[巴伐利亞國立歌劇院音乐总指导](../Page/巴伐利亞國立歌劇院.md "wikilink")，从1961年起同时担任[柏林广播交响乐团首席指挥和柏林德意志歌剧院艺术总监](../Page/柏林广播交响乐团.md "wikilink")。

弗里乔伊是指挥界第一位「媒体明星」，很早便涉足广播和唱片录制。1949年他为[德国唱片公司录制第一张](../Page/德国唱片公司.md "wikilink")[LP](../Page/LP.md "wikilink")，曲目是[柴科夫斯基的交响曲](../Page/柴科夫斯基.md "wikilink")，而且是立体声录音的先驱人物。

虽然他早逝，但他为后世留下了超过200部经典作品，并且从1948年起，将[美国占领区广播电台交响乐团](../Page/美国占领区广播电台.md "wikilink")（柏林RIAS广播交响乐团）提升到可与[柏林爱乐乐团并立的地位](../Page/柏林爱乐乐团.md "wikilink")。

出名的录音有，和[戈扎·安达合作录制的](../Page/戈扎·安达.md "wikilink")[巴托克三首](../Page/巴托克.md "wikilink")[钢琴协奏曲](../Page/钢琴协奏曲.md "wikilink")，[贝多芬的](../Page/贝多芬.md "wikilink")[第九交响曲和](../Page/第9號交響曲_\(貝多芬\).md "wikilink")[三重协奏曲](../Page/三重协奏曲.md "wikilink")。

弗里乔伊是剧院导演[安德拉斯·弗里乔伊的父亲](../Page/安德拉斯·弗里乔伊.md "wikilink")，后者曾导演[彼得·马斐的](../Page/彼得·马斐.md "wikilink")《塔把鲁加》。

## 参考文献

  - Lutz von Pufendorf (Herausgeber): *Ferenc Fricsay. Retrospektive -
    Perspektive.* Bote & Bock, Berlin 1988, ISBN 3-7931-1575-5
  - Friedrich Herzfeld (Herausgeber): *Ferenc Fricsay. Ein Gedenkbuch.*
    Rembrandt Verlag, Berlin 1964

[Category:匈牙利指挥家](../Category/匈牙利指挥家.md "wikilink")