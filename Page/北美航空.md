**北美航空**（North American Aviation
Inc.）是美国的一家航空制造企业。1967年同[洛克维尔-标准公司](../Page/洛克维尔-标准公司.md "wikilink")（Rockwell-Standard
Corporation）合并成为[北美洛克维尔公司](../Page/北美洛克维尔公司.md "wikilink")（North
American Rockwell
Corporation）。这家公司的知名产品包括[T-6德州佬式教練機](../Page/T-6德州佬式教練機.md "wikilink")，[P-51野马型战斗机](../Page/P-51.md "wikilink")，[B-25米歇尔型轰炸机](../Page/B-25.md "wikilink")，[F-86军刀型战斗机](../Page/F-86.md "wikilink")，X-15型火箭飞机，[阿波罗](../Page/阿波罗计划.md "wikilink")[指令/服务舱以及](../Page/阿波罗指令/服务舱.md "wikilink")[農神五號火箭第二级](../Page/土星5号.md "wikilink")。1996年作为洛克维尔国际的一部分被[波音](../Page/波音.md "wikilink")（Boeing）收购。

## 历史

1928年12月6日Clement
Keys创办了北美航空，其本以为创办一家以经营个航空公司和飞行相关公司的股票为主要业务的控股公司。但1934年的空邮法案迫使北美人航空转为制造企业，并雇用前[道格拉斯飞行器公司的James](../Page/道格拉斯飞行器公司.md "wikilink")
H. "Dutch" Kindelberger主席和总经理。

Kindelberger将公司搬到了可以全年飞行的南加州地区，并将公司的主要精力集中在可以避免同其它公司竞争的[教练机方面](../Page/教练机.md "wikilink")。其第一种飞机型号为GA-15观察机和GA-16教练机。后续型号有O-47和BT-9。1937年生产的BC-1是其第一种战斗机。

北美航空在BT-9后生产了T-6德州佬式教练机，这种飞机共制造了17000架，被认为是应用最广泛的教练机。其生产的双引擎[B-25米歇尔型轰炸机被用于](../Page/B-25.md "wikilink")[第二次世界大战中](../Page/第二次世界大战.md "wikilink")[美国对](../Page/美国.md "wikilink")[日本东京的第一次轰炸](../Page/东京.md "wikilink")。[P-51野马式战斗机被认为是在](../Page/P-51.md "wikilink")[第二次世界大战期间美国最好的](../Page/第二次世界大战.md "wikilink")[战斗机](../Page/战斗机.md "wikilink")。

战后，北美航空的雇员从91000人降到了50000人。但其仍然不断推出新型飞机，例如T-28型教练攻击机，A-5型舰载轰炸机，B-45龙卷风型喷气轰炸机，FJ1愤怒战斗机，XB-70三倍音速轰炸机等。

[F-86军刀型战斗机是](../Page/F-86.md "wikilink")[美国在](../Page/美国.md "wikilink")[韩战中大量使用的喷气式战斗机](../Page/韩战.md "wikilink")，制造数量超过了9000架。其后续产品[F-100超級军刀型也被美军大量采购](../Page/F-100.md "wikilink")。1965年北美航空试飞了非常成功的OV-10型飞机，这是第一种专门为反暴乱和前沿空中控制而制造的飞机，至今仍然被[菲律宾](../Page/菲律宾.md "wikilink")，[泰国等国大量使用](../Page/泰国.md "wikilink")。

50年代末[F-107和](../Page/F-107.md "wikilink")[F-108以及Navaho导弹项目的取消给北美航空造成了重大的打击](../Page/XF-108.md "wikilink")。1960年其新[CEO](../Page/CEO.md "wikilink")
Lee
Atwood决定将航天作为公司的主要经营方向。其成功获得了[阿波羅計畫控制舱和服务舱以及](../Page/阿波羅計畫.md "wikilink")[農神五號火箭第二级的制造合同](../Page/土星五号.md "wikilink")。1967年1月[阿波罗1号的火灾事故被部分归咎于北美航空](../Page/阿波罗1号.md "wikilink")。同年3月，北美航空同[洛克维尔合并](../Page/洛克维尔.md "wikilink")。

在同[洛克维尔合并后](../Page/洛克维尔.md "wikilink")，原北美航空部门的知名产品有[B-1型](../Page/B-1槍騎兵戰略轟炸機.md "wikilink")[战略轰炸机](../Page/战略轰炸机.md "wikilink")，[航天飞机以及](../Page/航天飞机.md "wikilink")[全球定位系统的大部分](../Page/全球定位系统.md "wikilink")[卫星](../Page/卫星.md "wikilink")。

1996年12月，[洛克维尔国际名下的防务和太空部门](../Page/罗克韦尔.md "wikilink")（包括北美航空）被[波音收购](../Page/波音.md "wikilink")。

## 产品

[T-2_Buckeye.jpg](https://zh.wikipedia.org/wiki/File:T-2_Buckeye.jpg "fig:T-2_Buckeye.jpg")

  - [P-51戰鬥機](../Page/P-51戰鬥機.md "wikilink")
  - [F-82戰鬥機](../Page/F-82戰鬥機.md "wikilink")
  - [B-25米切爾型轟炸機](../Page/B-25米切爾型轟炸機.md "wikilink")
  - [F-86軍刀戰鬥機](../Page/F-86軍刀戰鬥機.md "wikilink")
  - [F-100 超級軍刀](../Page/F-100_超級軍刀.md "wikilink")
  - [F-107终极佩刀战斗机](../Page/F-107终极佩刀战斗机.md "wikilink")
  - [T-6德州佬式教練機](../Page/T-6德州佬式教練機.md "wikilink")

[North_American_L-17A_USAF.jpg](https://zh.wikipedia.org/wiki/File:North_American_L-17A_USAF.jpg "fig:North_American_L-17A_USAF.jpg")

  - [L-17聯絡機](../Page/L-17聯絡機.md "wikilink")
  - [T-28教練機](../Page/T-28教練機.md "wikilink")
  - [T-2教練機](../Page/T-2教練機.md "wikilink")
  - [XB-21](../Page/North_American_XB-21.md "wikilink")
  - [O-47](../Page/North_American_O-47.md "wikilink")
  - [BT-9](../Page/North_American_BT-9.md "wikilink")
  - [A-36俯衝轟炸機](../Page/A-36俯衝轟炸機.md "wikilink")（改良自P-51A戰鬥機）
  - [XB-28 Dragon](../Page/XB-28_Dragon.md "wikilink")

[North_American_AJ-2_Savage_of_VAH-6_lands_on_USS_Yorktown_(CVA-10)_on_6_December_1955_(NH_97459).jpg](https://zh.wikipedia.org/wiki/File:North_American_AJ-2_Savage_of_VAH-6_lands_on_USS_Yorktown_\(CVA-10\)_on_6_December_1955_\(NH_97459\).jpg "fig:North_American_AJ-2_Savage_of_VAH-6_lands_on_USS_Yorktown_(CVA-10)_on_6_December_1955_(NH_97459).jpg")

  - [AJ Savage](../Page/AJ_Savage.md "wikilink")
  - [P-64](../Page/North_American_P-64.md "wikilink")
  - [T-39教練機](../Page/T-39教練機.md "wikilink")
  - [B-45轟炸機](../Page/B-45轟炸機.md "wikilink")
  - [FJ戰鬥機](../Page/FJ戰鬥機.md "wikilink")
  - [YF-93A](../Page/YF-93戰鬥機.md "wikilink")
  - [North American X-10](../Page/North_American_X-10.md "wikilink")
  - [A-5攻擊機](../Page/A-5攻擊機.md "wikilink")

[North_American_B-45A-5-NA_061020-F-1234S-027.jpg](https://zh.wikipedia.org/wiki/File:North_American_B-45A-5-NA_061020-F-1234S-027.jpg "fig:North_American_B-45A-5-NA_061020-F-1234S-027.jpg")

  - [XF-108戰鬥機](../Page/XF-108.md "wikilink")
  - [OV-10觀測機](../Page/OV-10觀測機.md "wikilink")
  - [X-15](../Page/X-15.md "wikilink")
  - [XB-70戰神侍婢式轟炸機](../Page/XB-70戰神侍婢式轟炸機.md "wikilink")

[Category:已结业飞机制造商](../Category/已结业飞机制造商.md "wikilink")
[Category:美國已結業公司](../Category/美國已結業公司.md "wikilink")
[Category:美国飞机公司](../Category/美国飞机公司.md "wikilink")
[Category:波音](../Category/波音.md "wikilink")
[Category:1928年成立的公司](../Category/1928年成立的公司.md "wikilink")
[Category:1967年結業公司](../Category/1967年結業公司.md "wikilink")
[Category:南加利福尼亚州历史](../Category/南加利福尼亚州历史.md "wikilink")
[Category:1928年加利福尼亞州建立](../Category/1928年加利福尼亞州建立.md "wikilink")