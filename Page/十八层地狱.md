**十八層地狱**是[華人民間信仰的信眾從](../Page/華人民間信仰.md "wikilink")[古印度](../Page/古印度.md "wikilink")[婆羅門教與](../Page/婆羅門教.md "wikilink")[佛教吸收過來的地獄](../Page/佛教.md "wikilink")，繼而演變為[道教與](../Page/道教.md "wikilink")[民間信仰所謂的地府](../Page/民間信仰.md "wikilink")。

佛經《十八泥犁經》便載有十八地獄的描述，但非一層層的十八層，而是地底八火獄和天際十寒獄\[1\]\[2\]。其後的《[梁书](../Page/梁书.md "wikilink")》有一载，[胡人刘萨何暴病而死](../Page/胡人.md "wikilink")，经十日复苏，自称游历了十八地狱。由此觀之，十八地獄的信仰到[南朝](../Page/南朝.md "wikilink")[蕭梁時期早已盛传中國民间](../Page/蕭梁.md "wikilink")。

中國民間信仰吸收佛教的地獄觀念後，產生了常見的「十八層地獄」說法，但十八层地狱詳細所指，卻众说纷纭，各家所說不同。在[明清時期](../Page/明清時期.md "wikilink")，隨白話小說創作的發展，文學作品當中出現的「十八層地獄」與佛經當中的「十八地獄」已有很大不同，越來越表現出本土化的特點，由[十殿閻君管理](../Page/十殿閻君.md "wikilink")\[3\]。

## 不同說法

下列舉出有文獻為據的「十八层地狱」，包括[小說與](../Page/小說.md "wikilink")[善書](../Page/善書.md "wikilink")：

### 《水陆全图》

拔舌地狱、剪刀地狱、铁树地狱、孽镜地狱、蒸笼地狱、铜柱地狱、刀山地狱、冰山地狱、油锅地狱、牛坑地狱、石压地狱、舂臼地狱、血池地狱、枉死地狱、磔刑地狱、火山地狱、石磨地狱、刀锯地狱

### 《酆都[變相](../Page/變相.md "wikilink")》

[酆都城](../Page/酆都城.md "wikilink")「天子殿」两旁分别为东地狱和西地狱：

  - 东地狱：磨推地狱、挖心地狱、火烙地狱、冰山地狱、寒冰地獄、刀山地狱、车裂地狱、蛆虫地狱、剥皮地狱
  - 西地狱：碓舂地狱、锯解地狱、油锅地狱、熱油地獄、拔舌地狱、补经地狱、转轮地狱、畜生地狱、镬汤地狱

### 《西遊記》

吊筋獄、幽枉獄、火坑獄； 酆都獄、拔舌獄、剝皮獄； 磨捱獄、碓搗獄、車崩獄； 寒冰獄、脫殼獄、抽腸獄； 油鍋獄、黑暗獄、刀山獄；
血池獄、[阿鼻獄](../Page/阿鼻地獄.md "wikilink")、秤桿獄。

### 《十殿閻君圖說》

[十殿閻君分別管理](../Page/十殿閻君.md "wikilink")[孤獨地獄](../Page/地獄_\(佛教\).md "wikilink")、[近邊地獄](../Page/地獄_\(佛教\).md "wikilink")、[八寒地獄](../Page/地獄_\(佛教\).md "wikilink")、[八熱地獄](../Page/八熱地獄.md "wikilink")，共計十八地獄。此地獄說法，被[印順法師認可](../Page/印順法師.md "wikilink")。

「孤獨地獄」有兩種，一種於[枉死城的內城](../Page/枉死城.md "wikilink")，通常是管理[自殺者所用](../Page/自殺.md "wikilink")。另一種並非地獄之中，可能在河畔、山頂、曠野，隨著死者業報，臨時出現的地獄。

近邊地獄為地獄門邊附設的小地獄。

| 名號                                   | 祭祀時機 | 簡介                                                                                                                                                                                                                                                                                                                                                                  |
| ------------------------------------ | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 一殿[秦廣王](../Page/秦廣王.md "wikilink")   | 頭七   | 姓蔣，傳說是東漢的[蔣子文](../Page/蔣子文.md "wikilink")，二月初一日誕辰（一說為二月初二日），在[三途川上的](../Page/三途川.md "wikilink")[奈何橋前派](../Page/奈何橋.md "wikilink")[無常鬼引領鬼魂至此殿](../Page/無常鬼.md "wikilink")，專司[業鏡](../Page/業鏡.md "wikilink")，照明善惡，使人在此殿服罪，如果行善遠超過惡業的，就直接送他至十殿，繼續[輪迴](../Page/輪迴.md "wikilink")，或者投生[門閥](../Page/門閥.md "wikilink")，或者[升天享福](../Page/天人.md "wikilink")。如果係一般人，就解送二殿繼續審理。 |
| 二殿[楚江王](../Page/楚江王.md "wikilink")   | 二七   | 姓厲，或作「歷」，三月初一日誕辰，專司「活大地獄」、「具皰地獄」。                                                                                                                                                                                                                                                                                                                                   |
| 三殿[宋帝王](../Page/宋帝王.md "wikilink")   | 三七   | 姓余，二月初八日誕辰，專司「黑繩大地獄」、「皰裂地獄」。                                                                                                                                                                                                                                                                                                                                        |
| 四殿[五官王](../Page/五官王.md "wikilink")   | 四七   | 姓呂， 二月十八日誕辰，專司｢眾合大地獄｣、「緊牙地獄」。                                                                                                                                                                                                                                                                                                                                       |
| 五殿[閻羅天子](../Page/閻羅天子.md "wikilink") | 五七   | 姓包，傳說是[北宋的](../Page/北宋.md "wikilink")[包拯](../Page/包拯.md "wikilink")，正月初八日誕辰，專司「叫喚大地獄」、「呵聲地獄」，同時也是十王之首，負責總管[十八層地獄](../Page/十八層地獄.md "wikilink")，即八大地獄、八寒地獄與附設的「近邊地獄」。五殿殿中有望鄉臺，供亡魂眺望家中情況，一解思鄉之苦。                                                                                                                                                                      |
| 六殿[卞城王](../Page/卞城王.md "wikilink")   | 六七   | 姓畢，三月初八日誕辰，專司「大叫喚大地獄」、「歎聲地獄」。                                                                                                                                                                                                                                                                                                                                       |
| 七殿[泰山王](../Page/泰山王.md "wikilink")   | 七七   | 姓董，三月廿七日誕辰，專司「[焦熱大地獄](../Page/焦熱大地獄.md "wikilink")」、「青蓮地獄」。                                                                                                                                                                                                                                                                                                         |
| 八殿[都市王](../Page/都市王.md "wikilink")   | 百日   | 姓黃，四月初一日誕辰，專司「[大焦熱大地獄](../Page/大焦熱大地獄.md "wikilink")」、「紅蓮地獄」。                                                                                                                                                                                                                                                                                                       |
| 九殿[平等王](../Page/平等王.md "wikilink")   | 一年   | 姓陸，四月初八日誕辰，專司「[阿鼻地獄](../Page/阿鼻地獄.md "wikilink")」、「大紅蓮地獄」、「[枉死城](../Page/枉死城.md "wikilink")」，同時[目連尊者受理冤屈申訴後](../Page/目連.md "wikilink")，發予此王重審。                                                                                                                                                                                                                      |
| 十殿[轉輪王](../Page/轉輪王.md "wikilink")   | 三年   | 姓薛，四月十七日誕辰，專司各殿解到鬼魂，區別善惡，核定等級，由[孟婆灌醉之後](../Page/孟婆.md "wikilink")，發往[轉世](../Page/轉世.md "wikilink")。                                                                                                                                                                                                                                                                 |
|                                      |      |                                                                                                                                                                                                                                                                                                                                                                     |

## 旅遊景點

[Tainan_Madou_Dai_Tian_Temple_Eighteen_Levels_of_Hell.jpg](https://zh.wikipedia.org/wiki/File:Tainan_Madou_Dai_Tian_Temple_Eighteen_Levels_of_Hell.jpg "fig:Tainan_Madou_Dai_Tian_Temple_Eighteen_Levels_of_Hell.jpg")

許多寺廟或景點將「十八層地獄」具體化，或泥塑、或建築、或電動，經由氣氛營造建構出人間對於地獄的想像，並收警醒教化世人之效。

  - [中國](../Page/中國.md "wikilink")
      - [重庆](../Page/重庆.md "wikilink")：[丰都鬼城](../Page/丰都鬼城.md "wikilink")
      - [山東](../Page/山東.md "wikilink")：[東嶽廟](../Page/東嶽廟.md "wikilink")

<!-- end list -->

  - [臺灣](../Page/臺灣.md "wikilink")
      - [彰化縣](../Page/彰化縣.md "wikilink")：[彰化市](../Page/彰化市.md "wikilink")[南天宮](../Page/彰化南天宮.md "wikilink")
      - [臺南市](../Page/臺南市.md "wikilink")：[麻豆代天府](../Page/麻豆代天府.md "wikilink")「十八地獄」

另外，在[香港](../Page/香港.md "wikilink")[大坑原來有](../Page/大坑.md "wikilink")[虎豹別墅](../Page/虎豹別墅_\(香港\).md "wikilink")，內裡亦有十八层地狱的雕塑，以警世人。現時由於有關地段已被地產商開發重建為理由而被清拆。新加坡的[虎豹別墅也有类似景点](../Page/虎豹別墅_\(新加坡\).md "wikilink")，至今仍保留完好。

## 參見

  - [黃泉](../Page/黃泉.md "wikilink")
  - [泰山](../Page/泰山.md "wikilink")
  - [十殿閻羅](../Page/十殿閻羅.md "wikilink")
  - [地獄 (佛教)](../Page/地獄_\(佛教\).md "wikilink")

## 註腳

<references />

## 參考資料

  - [中国民间传说-十殿阎王与十八层地狱](http://tech.163.com/05/0402/09/1GATLJR100091547.html)
  - [華雨集第四冊－三
    鬼與地獄](http://yinshun-edu.org.tw/en/Master_yinshun/y28_03_03)
  - [中國民間崇拜](http://www.chiculture.net/1605/html/index.html)

[十八層地獄](../Category/十八層地獄.md "wikilink")

1.  《十八泥犁經》：「火泥犁有八，寒泥犁有十。入地半以下火泥犁，天地際者寒泥犁……所謂寒犁在天際間，有大山高二千里，主蔽風，名山-{于}-雀盧山，冥無日月，所不及逮。有蔽大山，故冥。外有日月之王甚多，無央數寒犁中」
2.  《[長阿含](../Page/長阿含經.md "wikilink")·[世記經](../Page/世記經.md "wikilink")》：「佛告比丘：此四天下有八千天下圍遶其外，復有大海水周匝圍遶八千天下，復有大金剛山遶大海水。金剛山外復有第二大金剛山，二山中間窈窈冥冥，日月神天有大威力，不能以光照及於彼。彼有八大地獄，其一地獄有十六小地獄。第一大地獄名想，第二名黑繩，第三名堆壓，第四名叫喚，第五名大叫喚，第六名燒炙，第七名大燒炙，第八名無間……又彼二山中間復有十地獄：一名厚雲，二名無雲，三名呵呵，四名奈何，五名羊鳴，六名須乾提，七名優鉢羅，八名拘物頭，九名分陀利，十名鉢頭摩……時，閻羅王以三天使具詰問已，即付獄卒。時，彼獄卒即將罪人詣大地獄，其大地獄縱廣百由旬，下深百由旬。爾時，世尊即說偈言：「四方有四門，巷陌皆相當，以鐵為獄牆，上覆鐵羅網。以鐵為下地，自然火焰出，縱廣百由旬，安住不傾動。黑焰熢㶿起，赫烈難可覩，小獄有十六，火熾由行惡。」
3.