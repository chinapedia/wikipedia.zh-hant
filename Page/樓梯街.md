[Ladderstreet.JPG](https://zh.wikipedia.org/wiki/File:Ladderstreet.JPG "fig:Ladderstreet.JPG")交界的路牌\]\]
[HKSW_LadderStreet_60311.jpg](https://zh.wikipedia.org/wiki/File:HKSW_LadderStreet_60311.jpg "fig:HKSW_LadderStreet_60311.jpg")附近[文武廟之西面](../Page/文武廟.md "wikilink")\]\]
[HKSW_Ladder_Street_60311.jpg](https://zh.wikipedia.org/wiki/File:HKSW_Ladder_Street_60311.jpg "fig:HKSW_Ladder_Street_60311.jpg")附近[文武廟之西面](../Page/文武廟.md "wikilink")\]\]

**樓梯街**（****）位於[香港](../Page/香港.md "wikilink")[上環](../Page/上環.md "wikilink")，顧名思義，是一條以[樓梯為主的街道](../Page/樓梯.md "wikilink")，除了荷李活道以南，[文武廟旁邊一小段可供車輛駛進四方街外](../Page/文武廟.md "wikilink")，其餘各段皆為石級。早於香港開埠初期的1841年至1850年間興建，歷史悠久，現已被[古物古蹟辦事處列為](../Page/古物古蹟辦事處.md "wikilink")[一級歷史建築](../Page/一級歷史建築.md "wikilink")。

樓梯街由[皇后大道中起](../Page/皇后大道中.md "wikilink")，沿山勢而上;經過[摩羅上街](../Page/摩羅街.md "wikilink")、[弓絃巷](../Page/弓絃巷.md "wikilink")、[荷李活道](../Page/荷李活道.md "wikilink")、[四方街](../Page/四方街.md "wikilink")、[必列者士街及](../Page/必列者士街.md "wikilink")[裕林臺](../Page/裕林臺.md "wikilink")，直至[堅道止](../Page/堅道.md "wikilink")，全長350米。不少[電影](../Page/電影.md "wikilink")、[電視劇集曾在此取景](../Page/電視.md "wikilink")。全段共有316級。

樓梯街原本由花崗石鋪成，歷經百多年歷史，現時大部分已鋪上混凝土，亦更換了新型鐵欄，但不少歷史痕跡亦能清晰可看，例如舊式護土牆、樹牆及街道旁的護欄等。樓梯街與荷李活道交界的[文武廟](../Page/文武廟.md "wikilink")，和在必列者士街的[基督教青年會會所](../Page/香港中華基督教青年會中央會所.md "wikilink")，皆被政府列為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")。

## 鄰近

  - [英皇書院同學會小學](../Page/英皇書院同學會小學.md "wikilink")
  - [東華三院之住宅物業](../Page/東華三院.md "wikilink")[東盛台](../Page/東盛台.md "wikilink")
  - [公理堂大廈暨教會](../Page/公理堂大廈暨教會.md "wikilink")
  - [香港醫學博物館](../Page/香港醫學博物館.md "wikilink")
  - [永利街](../Page/永利街.md "wikilink")
  - [香港中華基督教青年會中央會所](../Page/香港中華基督教青年會中央會所.md "wikilink")

[File:Ladderstreet.JPG|2008年](File:Ladderstreet.JPG%7C2008年) <File:HK>
Sheung Wan Caine Road view Ladder Street sign Feb-2011.JPG|2011年

## 站外鏈結

  - [上環樓梯街位於中山史蹟徑上](https://web.archive.org/web/20060118025827/http://xml.lib.hku.hk/syshk/D.jsp)
  - [樓梯街地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=833535&cy=816090&zm=2&mx=833535&my=816169&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[Category:上環街道](../Category/上環街道.md "wikilink")
[Category:香港特色街道](../Category/香港特色街道.md "wikilink")