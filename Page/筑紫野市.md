**筑紫野市**（）是位於[福岡縣的一個](../Page/福岡縣.md "wikilink")[城市](../Page/城市.md "wikilink")，位於[福岡市](../Page/福岡市.md "wikilink")[久留米市中間](../Page/久留米市.md "wikilink")，屬於[福岡市都會區的一部份](../Page/福岡市都會區.md "wikilink")，為福岡市的[通勤城市](../Page/通勤城市.md "wikilink")。

[鹿兒島本線](../Page/鹿兒島本線.md "wikilink")、[天神大牟田線](../Page/天神大牟田線.md "wikilink")、[九州自動車道皆通過市中心地區](../Page/九州自動車道.md "wikilink")，使得本市對外交通非常便利。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，線在的轄區在當時分屬：[御笠郡二日市村](../Page/御笠郡.md "wikilink")、山口村、筑紫村、御笠村、山家村。
  - 1895年8月27日：二日市村改制為[二日市町](../Page/二日市町.md "wikilink")。
  - 1896年4月1日：[那珂郡](../Page/那珂郡_\(福岡縣\).md "wikilink")、[席田郡](../Page/席田郡_\(福岡縣\).md "wikilink")、御笠郡合併為[筑紫郡](../Page/筑紫郡.md "wikilink")。
  - 1955年3月1日：二日市町、山口村、筑紫村、御笠村、山家村[合併為](../Page/市町村合併.md "wikilink")**筑紫野町**；當時曾考慮與水城村、太宰府町（現已合併為[太宰府市](../Page/太宰府市.md "wikilink")）一同合併，但由於太宰府町堅持要保留「太宰府」之名，因此兩地未能合併。
  - 1972年4月1日：改制為**筑紫野市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1896年2月26日</p></th>
<th><p>1896年 - 1911年</p></th>
<th><p>1912年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1959年</p></th>
<th><p>1960年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>御笠郡二日市町</p></td>
<td><p>筑紫郡二日市町</p></td>
<td><p>1955年3月1日<br />
筑紫野町</p></td>
<td><p>1972年4月1日<br />
筑紫野市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>御笠郡山口村</p></td>
<td><p>筑紫郡山口村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>御笠郡筑紫村</p></td>
<td><p>筑紫郡筑紫村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>御笠郡御笠村</p></td>
<td><p>筑紫郡御笠村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>御笠郡山家村</p></td>
<td><p>筑紫郡山家村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

1.  船越平八郎（1967年2月23日～1975年2月22日：共二任）
2.  神代仁臣（1975年2月23日～1979年2月22日）
3.  松田正彦（1979年2月23日～1986年12月24日：共二任）
4.  楠田幹人（1987年2月1日～1995年1月31日：共二任）
5.  田中範隆（1995年2月1日～2003年1月31日：共二任）
6.  平原四郎（2003年2月1日～現：現為第二任）

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [鹿児島本線](../Page/鹿児島本線.md "wikilink")：[二日市車站](../Page/二日市車站.md "wikilink")
        - [天拜山車站](../Page/天拜山車站.md "wikilink") -
        [原田車站](../Page/原田車站_\(福岡縣\).md "wikilink")
      - [筑豐本線](../Page/筑豐本線.md "wikilink")：[筑前山家車站](../Page/筑前山家車站.md "wikilink")
        - 原田車站
  - [西日本鐵道](../Page/西日本鐵道.md "wikilink")
      - [天神大牟田線](../Page/天神大牟田線.md "wikilink")：[西鐵二日市車站](../Page/西鐵二日市車站.md "wikilink")
        - [紫車站](../Page/紫車站.md "wikilink")（興建中） -
        [朝倉街道車站](../Page/朝倉街道車站.md "wikilink") -
        [櫻台車站](../Page/櫻台車站_\(福岡縣\).md "wikilink") -
        [筑紫車站](../Page/筑紫車站.md "wikilink")
      - [太宰府線](../Page/太宰府線.md "wikilink")：西鉄二日市車站

|                                                                                                                                                        |                                                                                                                                                     |
| ------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Nishitetsu_Sakuradai_Station01.jpg](https://zh.wikipedia.org/wiki/File:Nishitetsu_Sakuradai_Station01.jpg "fig:Nishitetsu_Sakuradai_Station01.jpg") | [Nishitetsu_Chikushi_Station02.jpg](https://zh.wikipedia.org/wiki/File:Nishitetsu_Chikushi_Station02.jpg "fig:Nishitetsu_Chikushi_Station02.jpg") |

### 道路

  - 高速道路

<!-- end list -->

  - [九州自動車道](../Page/九州自動車道.md "wikilink")：[筑紫野交流道](../Page/筑紫野交流道.md "wikilink")

## 觀光資源

  - [二日市溫泉](../Page/二日市溫泉.md "wikilink")
  - 天拜山
  - 山神水庫公園

|                                                                                                       |                                                                                                 |
| ----------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [Gozenyu_201508.JPG](https://zh.wikipedia.org/wiki/File:Gozenyu_201508.JPG "fig:Gozenyu_201508.JPG") | [Mount_tenpai.jpg](https://zh.wikipedia.org/wiki/File:Mount_tenpai.jpg "fig:Mount_tenpai.jpg") |

## 教育

### 高等學校

  - [福岡縣立筑紫高等學校](../Page/福岡縣立筑紫高等學校.md "wikilink")
  - [福岡縣立武蔵台高等學校](../Page/福岡縣立武蔵台高等學校.md "wikilink")
  - [九州產業大學附屬九州產業高等學校](../Page/九州產業大學附屬九州產業高等學校.md "wikilink")
  - [福岡常葉高等學校](../Page/福岡常葉高等學校.md "wikilink")

## 本地出身之名人

  - [安西均](../Page/安西均.md "wikilink")：[詩人](../Page/詩人.md "wikilink")
  - [楠田大藏](../Page/楠田大藏.md "wikilink")：[日本眾議院](../Page/日本眾議院.md "wikilink")[議員](../Page/議員.md "wikilink")
  - [立川生志](../Page/立川生志.md "wikilink")：[落語家](../Page/落語.md "wikilink")
  - [田中賢介](../Page/田中賢介.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [でんでん](../Page/でんでん.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [福永武彥](../Page/福永武彥.md "wikilink")：[小説家](../Page/小説家.md "wikilink")、詩人
  - [山內康一](../Page/山內康一.md "wikilink")：日本眾議院議員
  - [陸守繪麻](../Page/陸守繪麻.md "wikilink")：[模特兒](../Page/模特兒.md "wikilink")

## 外部連結

  - [筑紫野市觀光協會](http://www.chikushino.org/)

  - [筑紫野市商工會](http://www.chikushino.ne.jp/)