**梭梭**（[学名](../Page/学名.md "wikilink")：**），亦称“**盐木**”、“**琐琐树**”，是[苋科](../Page/苋科.md "wikilink")[梭梭属植物](../Page/梭梭属.md "wikilink")。

## 形态

落叶[灌木或](../Page/灌木.md "wikilink")[小乔木](../Page/小乔木.md "wikilink")；小枝对生，具有节；[鳞状三角形叶子对生](../Page/鳞.md "wikilink")，[花小腋生](../Page/花.md "wikilink")，两性，[穗状花序](../Page/穗状花序.md "wikilink")。[果实上的萼片形成近圆形之翅](../Page/果实.md "wikilink")。

## 分布

耐干旱，一般生长在[中国](../Page/中国.md "wikilink")[新疆和](../Page/新疆.md "wikilink")[内蒙古西部的](../Page/内蒙古.md "wikilink")[沙漠地区](../Page/沙漠.md "wikilink")。梭梭由於根系發達\[1\]，所以可以固定流沙，是沙漠造林用的重要植物之一。目前在[阿拉善左旗](../Page/阿拉善左旗.md "wikilink")，梭梭林都被圍欄圍起來，以免被動物過度採吃而吃光\[2\]。

同属另有[黑梭梭](../Page/黑梭梭.md "wikilink")（*Haloxylon
aphyllum*）和[白梭梭](../Page/白梭梭.md "wikilink")（*Haloxylon
persicum*）两种。

## 參考文獻

[ammodendron](../Category/梭梭属.md "wikilink")

1.
2.