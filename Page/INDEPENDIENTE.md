《***INDEPENDIENTE***》（中譯：**傲視群雄**）是[日本](../Page/日本.md "wikilink")[樂團](../Page/樂團.md "wikilink")[Dragon
Ash的第十張](../Page/Dragon_Ash.md "wikilink")[專輯](../Page/專輯.md "wikilink")（完整[錄音室專輯為第七張](../Page/錄音室專輯.md "wikilink")）。專輯選在2007年2月21日推出，與十年前（1997年）的第一張專輯《*[The
day dragged
on](../Page/The_day_dragged_on.md "wikilink")*》發行日期相同。專輯名稱「INDEPENDIENTE」在[西班牙語中有](../Page/西班牙語.md "wikilink")「獨立、自由、孤高」的含意。日本初回版與台灣版的專輯內附贈「Ivory」、「few
lights till night」、「夢で逢えたら」三首歌曲的[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")。

專輯內所有歌曲都是由[降谷建志](../Page/降谷建志.md "wikilink")（Kj）作詞、Dragon Ash
作曲，除了第11首歌「Luz del
sol」是由[決明子](../Page/決明子_\(樂團\).md "wikilink")（ケツメイシ）的大藏作詞，並和
Dragon Ash 共同作曲。

## 曲目

1.  Independiente <small>(Intro)</small> - 2:04
2.  Develop the music - 4:00
3.  stir - 4:16
4.  Fly - 4:02
5.  Ivory - 4:16
6.  Libertad de fiesta - 4:06
7.  El Alma <small>feat.SHINJI TAKEDA</small> - 4:00
8.  Rainy - 4:11
9.  Step snow <small>(interlude)</small> - 2:06
10. Samba 'n' bass - 4:11
11. Luz del sol <small>feat. 大蔵 from
    [ケツメイシ](../Page/決明子_\(樂團\).md "wikilink")</small>
    - 4:17
12. few lights till night - 4:35
13. Beautiful - 4:12
14. 夢で逢えたら - 4:43

### CD EXTRA 音樂錄影帶

1.  Ivory
2.  few lights till night
3.  夢で逢えたら

## 外部連結

  - [Dragon Ash 官方網站](http://www.dragonash.co.jp/)

[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")
[Category:勝利娛樂音樂專輯](../Category/勝利娛樂音樂專輯.md "wikilink")
[Category:日本搖滾音樂專輯](../Category/日本搖滾音樂專輯.md "wikilink")