**向前**（），[廣東](../Page/廣東.md "wikilink")[陸豐](../Page/陸豐.md "wikilink")(今屬廣東省[汕尾市](../Page/汕尾市.md "wikilink"))人，曾為[中華民國國軍](../Page/中華民國國軍.md "wikilink")[少將](../Page/少將.md "wikilink")、[僑委會顧問](../Page/僑委會.md "wikilink")、[中華民國國家安全局](../Page/中華民國國家安全局.md "wikilink")[少將銜顧問](../Page/少將.md "wikilink")，曾於[香港從事](../Page/英屬香港.md "wikilink")[抗日活動](../Page/抗日.md "wikilink")，戰後在香港成為[義安工商總會的領導人](../Page/義安工商總會.md "wikilink")，並以[太平山體育會](../Page/太平山體育會.md "wikilink")、[義安公司](../Page/義安公司.md "wikilink")、[新安公司等合法團體作](../Page/新安公司.md "wikilink")[掩護](../Page/掩護.md "wikilink")，被視為[三合會組織](../Page/三合會.md "wikilink")[新義安的](../Page/新義安.md "wikilink")[龍頭大佬創始人](../Page/龍頭.md "wikilink")。

## 生平

清[同治年間](../Page/同治.md "wikilink")，[廣東有潮州人成立的](../Page/廣東.md "wikilink")「萬安社」，本為互助性的民間團體，傳入[香港](../Page/香港.md "wikilink")。1921年萬安社的部分成員脫離社團，成立了義安社，義安社於1921年用[義安工商總會向當時](../Page/義安工商總會.md "wikilink")[香港殖民地時期的](../Page/香港殖民地時期.md "wikilink")[華民政務司署注冊](../Page/華民政務司.md "wikilink")，成為合法團體。向前在幫會中逐漸成為領袖人物，[國共內戰期間](../Page/國共內戰.md "wikilink")，向前自稱「太平山義安堂」堂主，也隨著[廣州的](../Page/廣州.md "wikilink")「[洪門忠義會](../Page/洪門.md "wikilink")」（即「[十四K](../Page/十四K.md "wikilink")」）領袖[葛肇煌](../Page/葛肇煌.md "wikilink")，宣佈效忠[國民政府](../Page/國民政府.md "wikilink")，[蔣介石授向前少將](../Page/蔣介石.md "wikilink")[軍銜](../Page/軍銜.md "wikilink")，向前也成為[國軍](../Page/國軍.md "wikilink")[情報單位的領導人](../Page/情報.md "wikilink")。

1947年[義安工商總會被](../Page/義安工商總會.md "wikilink")[港府取消註冊](../Page/港英政府.md "wikilink")，轉入低調，人稱新義安。1953年，向前被指涉嫌參加[三合會及非法政治組織](../Page/三合會.md "wikilink")，被[驅逐出境](../Page/驅逐出境_\(處分\).md "wikilink")，遷居[台灣](../Page/台灣.md "wikilink")。他出境後，由長子[向華炎開始處理](../Page/向華炎.md "wikilink")[幫會事務](../Page/幫會.md "wikilink")，剛[中學中一畢業的](../Page/中學.md "wikilink")[向華炎便完全接手](../Page/向華炎.md "wikilink")[幫會事務](../Page/幫會.md "wikilink")，直到其於1987年被捕為止。

向前於1975年2月25日在[台灣逝世](../Page/台灣.md "wikilink")，由兒子將骨灰運回香港安放在[沙田](../Page/沙田.md "wikilink")[寶福山](../Page/寶福山.md "wikilink")，佔用12個骨灰靈位，上面列出向前13名兒子中，其中9名的名字：華光、華炎、華榮、華強（第十）、華波（第七）、華坤、華勝（第十三）、華國（十一）、華民。

## 家族列表

  - 太太：[鍾金](../Page/鍾金.md "wikilink")\[1\]\[2\]

### 兒女

  - [向華炎](../Page/向華炎.md "wikilink")「四眼龍」：其前妻[呂杏華的父親](../Page/呂杏華.md "wikilink")[呂六](../Page/呂六.md "wikilink")，是「五億探長」[呂樂之叔父](../Page/呂樂.md "wikilink")。向華炎結婚時，呂樂仍未升任[探長](../Page/探長.md "wikilink")，向、呂兩家故互為世交。
  - [向華玲](../Page/向華玲.md "wikilink")
  - [向華榮](../Page/向華榮.md "wikilink")
  - [向華坤](../Page/向華坤.md "wikilink")
  - [向華國](../Page/向華國.md "wikilink")
  - [向華光](../Page/向華光.md "wikilink")：教師
  - [向華民](../Page/向華民.md "wikilink")
  - [向華波](../Page/向華波.md "wikilink")
  - [向華強](../Page/向華強.md "wikilink")：香港知名的電影出品人，[中國星集團有限公司老闆](../Page/中國星集團有限公司.md "wikilink")。[向華強前妻是台灣](../Page/向華強.md "wikilink")1970年代女星[丁佩](../Page/丁佩.md "wikilink")（前妻）有軍方背景，而現任妻子也是來自台灣的[陳明英](../Page/陳明英.md "wikilink")（又名[陳嵐](../Page/陳嵐.md "wikilink")）人稱向太，妻子幫助丈夫[向華強管理家族生意業務](../Page/向華強.md "wikilink")。
  - [向華勝](../Page/向華勝.md "wikilink")：與兄長[向華強成立](../Page/向華強.md "wikilink")[永盛電影公司](../Page/永盛電影公司.md "wikilink")（[中國星集團有限公司前身](../Page/中國星集團有限公司.md "wikilink")），其中不少著名明星、導演也和[中國星集團有限公司合作](../Page/中國星集團有限公司.md "wikilink")，例如：[劉德華](../Page/劉德華.md "wikilink")、[李连杰](../Page/李连杰.md "wikilink")、[梅艳芳](../Page/梅艳芳.md "wikilink")、[舒淇](../Page/舒淇.md "wikilink")、[張柏芝](../Page/張柏芝.md "wikilink")、[周星驰](../Page/周星驰.md "wikilink")、[甄子丹等等](../Page/甄子丹.md "wikilink")，而著名導演[王晶](../Page/王晶_\(導演\).md "wikilink")、[刘镇伟](../Page/刘镇伟.md "wikilink")、[徐克](../Page/徐克.md "wikilink")、[杜琪峰等等](../Page/杜琪峰.md "wikilink")，不少與[向華強的妻子](../Page/向華強.md "wikilink")，即向太[陳明英](../Page/陳明英.md "wikilink")（又名[陳嵐](../Page/陳嵐.md "wikilink")）關係要好。向氏家族可謂在娛樂、電影界舉足輕重，向前家族成員在商界不少也有立足地位和有頭有面，在任何社會場合[向華強總有太太陪伴](../Page/向華強.md "wikilink")，以及[保鑣跟隨出入](../Page/保鑣.md "wikilink")。

### 孫

  - 孫兒
      - [向展偉](../Page/向展偉.md "wikilink")：[向華炎之子](../Page/向華炎.md "wikilink")，[律師](../Page/律師.md "wikilink")
      - [向展棠](../Page/向展棠.md "wikilink")：[向華波之子](../Page/向華波.md "wikilink")
      - [向展橒](../Page/向展橒.md "wikilink")：[向華波之子](../Page/向華波.md "wikilink")
      - [向展平](../Page/向佐.md "wikilink")：即[向佐](../Page/向佐.md "wikilink")，[向華強之子](../Page/向華強.md "wikilink")
      - [向展凡](../Page/向展凡.md "wikilink")：即[向佑](../Page/向佑.md "wikilink")，[向華強之子](../Page/向華強.md "wikilink")
      - [向展熙](../Page/向展熙.md "wikilink")：[向華坤之子](../Page/向華坤.md "wikilink")
      - [向展飛](../Page/向展飛.md "wikilink")
      - [向展鴻](../Page/向展鴻.md "wikilink")
      - [向展斌](../Page/向展斌.md "wikilink")
      - [向展邦](../Page/向展邦.md "wikilink")：與弟弟[向展威攜手創辦](../Page/向展威.md "wikilink")[M.SPY
        Model
        Agency](../Page/M.SPY_Model_Agency.md "wikilink")，擔任行政總裁\[3\]（[M.SPY模特兒公司](../Page/M.SPY模特兒公司.md "wikilink")）
        - 向展邦
      - [向展威](../Page/向展威.md "wikilink")：與哥哥[向展邦創辦](../Page/向展邦.md "wikilink")[M.SPY
        Model Agency](../Page/M.SPY_Model_Agency.md "wikilink")，擔任董事總經理
      - [向展傑](../Page/向展傑.md "wikilink")：[向華炎之子](../Page/向華炎.md "wikilink")
      - [向展錡](../Page/向展錡.md "wikilink")：已改名為[陳冠錡](../Page/陳冠錡.md "wikilink")
      - [向展輝](../Page/向展輝.md "wikilink")
  - 孫女
      - [向詠怡](../Page/向詠怡.md "wikilink")：[向華炎大女](../Page/向華炎.md "wikilink")，夫為[張人龍已故兒子](../Page/張人龍.md "wikilink")[張亮聲](../Page/張亮聲.md "wikilink")
      - [向詠茵](../Page/向詠茵.md "wikilink")：[向華炎細女](../Page/向華炎.md "wikilink")，[大律師](../Page/大律師.md "wikilink")
      - [向詠恒](../Page/向詠恒.md "wikilink")：即[向鈞羚](../Page/向鈞羚.md "wikilink")，[向華強之女](../Page/向華強.md "wikilink")
      - [向詠彤](../Page/向詠彤.md "wikilink")：[向華國大女](../Page/向華國.md "wikilink")

## 參考資料

[X向](../Category/陆丰人.md "wikilink")
[X向](../Category/中國國民黨黨員.md "wikilink")
[Category:向姓](../Category/向姓.md "wikilink")
[X向](../Category/中華民國陸軍少將.md "wikilink")
[X向](../Category/台灣戰後廣東移民.md "wikilink")
[X向](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:向前家族](../Category/向前家族.md "wikilink")
[Category:香港潮汕人](../Category/香港潮汕人.md "wikilink")
[Category:香港三合會成員](../Category/香港三合會成員.md "wikilink")

1.
2.  [新義安的創辦人遺孀出殯，多名圈中人到場，包括娛樂圈中人。](http://stacylife.pixnet.net/blog/post/5575740)
3.  [兩姪開模特兒公司向華強父子帶隊撐場](http://paper.wenweipo.com/2010/11/24/EN1011240011.htm)