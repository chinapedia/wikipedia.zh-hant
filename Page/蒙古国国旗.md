**蒙古國旗**為[蒙古國的](../Page/蒙古國.md "wikilink")[國旗](../Page/國旗.md "wikilink")。

## 簡介

國旗由兩塊[紅色及一塊](../Page/紅色.md "wikilink")[藍色作為背景](../Page/藍色.md "wikilink")，靠旗桿處的紅色部份有一個[黃色的](../Page/黃色.md "wikilink")「[索永布](../Page/索永布.md "wikilink")」，索永布中的火焰是「吉祥和興旺的種子」，三條火舌象徵了過去、現在與未來，而[太陽和](../Page/太陽.md "wikilink")[月亮是](../Page/月亮.md "wikilink")[蒙古民族傳統的象徵物](../Page/蒙古.md "wikilink")。

火、日、月三者結合，表示國家的昌盛，中央的[太極圖象徵了國家的和諧](../Page/太極圖.md "wikilink")，下方的兩個[三角形等同於](../Page/三角形.md "wikilink")[箭](../Page/箭.md "wikilink")，向下的三角形表示以武力捍衛家園，[太極圖上下方的兩條](../Page/太極圖.md "wikilink")[長方形有堅持正義和忠實之意](../Page/長方形.md "wikilink")，左右兩方的長方形代表城牆，暗示全民的團結，比牆更加堅固。另外，背景的紅色代表了進步與繁榮，「永恆的藍天」就以藍色來取代。

## 歷史國旗

現時蒙古国的國旗是受到之前的國旗所影響，1940年至1992年的[蒙古人民共和國國旗](../Page/蒙古人民共和國.md "wikilink")，比現時的國旗多一顆星。象徵了[社會主義及](../Page/社會主義.md "wikilink")[蒙古人民革命黨](../Page/蒙古人民革命黨.md "wikilink")。中间色彩也为浅蓝色而非深蓝色。



### 其他旗帜

## 色彩

[Mongolia_flag.jpg](https://zh.wikipedia.org/wiki/File:Mongolia_flag.jpg "fig:Mongolia_flag.jpg")

| 标准                                      | 蓝          | 红           | 黄          |
| --------------------------------------- | ---------- | ----------- | ---------- |
| [CMYK](../Page/CMYK.md "wikilink")\[1\] | 100-60-0-0 | 10-100-90-0 | 0-15-100-0 |
|                                         |            |             |            |

## 参考文献

{{-}}

[Category:蒙古国国家象征](../Category/蒙古国国家象征.md "wikilink")
[Category:蒙古国旗帜](../Category/蒙古国旗帜.md "wikilink")
[Category:亞洲國旗](../Category/亞洲國旗.md "wikilink")
[Category:1992年面世的旗幟](../Category/1992年面世的旗幟.md "wikilink")

1.  [Төрийн далбаа стандарттай
    болов](http://www.president.mn/mongolian/node/1958)