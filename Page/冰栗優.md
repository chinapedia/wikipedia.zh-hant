[You_Higuri_20071027_Manga_Expo_1.jpg](https://zh.wikipedia.org/wiki/File:You_Higuri_20071027_Manga_Expo_1.jpg "fig:You_Higuri_20071027_Manga_Expo_1.jpg")
**冰栗優**，日本[漫畫家](../Page/漫畫家.md "wikilink")。[大阪市出身](../Page/大阪市.md "wikilink")。代表作是《禁斷毒天使》。

## 作品

  - 漫畫

<!-- end list -->

  - [禁斷毒天使](../Page/禁斷毒天使.md "wikilink")、全12冊、原名《》
  - [聖魔傳](../Page/聖魔傳.md "wikilink")、全10冊、原名《》
  - [聖獸之森](../Page/聖獸之森.md "wikilink")、全1冊、原名《》
  - [怪盜迷情](../Page/怪盜迷情.md "wikilink")、全4冊、原名《》
  - [薔薇天使戀](../Page/薔薇天使戀.md "wikilink")、全2冊、原名《》
  - [天使之柩](../Page/天使之柩.md "wikilink")、全1冊、原名《》
  - [失樂天使](../Page/失樂天使.md "wikilink")、全1冊、原名《》
  - [FLOWER 花](../Page/FLOWER_花.md "wikilink")、全1冊、原名《》
  - [惡魔聖痕](../Page/惡魔聖痕.md "wikilink")、全2冊、原名《》
  - 《[學園天堂](../Page/學園天堂.md "wikilink")》系列

:\* 「學園天堂 丹羽篇」、原名《》

:\* 「學園天堂 中嶋篇」、原名《》

  - [CROWN -
    特務指令](../Page/CROWN_-_特務指令.md "wikilink")、全6冊、原名《》（[和田慎二原作](../Page/和田慎二.md "wikilink")）
  - 暗夜第六感、全3冊、原名《NIGHT HEAD GENESIS》、原作：飯田譲治

<!-- end list -->

  - 畫冊

<!-- end list -->

  - JeweL
  - POISON

<!-- end list -->

  - 小說插畫

<!-- end list -->

  - 「學園天堂 遠藤篇」（作者：TAMAMI／插畫：冰栗優）

## 外部連結

  - [](https://web.archive.org/web/20060613202339/http://www.diana.dti.ne.jp/~higuri/)（作者公式網站）

:\* 單行本出版紀錄
[日本](https://web.archive.org/web/20080513112251/http://members3.jcom.home.ne.jp/yhigurin/dtig/tankoubon/tanko.htm)
-
[台灣](http://members3.jcom.home.ne.jp/yhigurin/dtig/tankoubon/takokaigai/taiw.htm)
-
[香港](http://members3.jcom.home.ne.jp/yhigurin/dtig/tankoubon/takokaigai/hon.htm)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:BL漫画家](../Category/BL漫画家.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")