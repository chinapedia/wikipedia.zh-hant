|series=未知 |predicted series=未知 |series
comment=可能為[超錒系元素](../Page/超錒系元素.md "wikilink")
|group=n/a |period=8 |block=g |series color= |phase color= |appearance=
|image name= |image size= |image name comment= |image name 2= |image
size 2= |image name 2 comment= |atomic mass=未知 |atomic mass 2= |atomic
mass comment= |electron configuration=
[\[Og](../Page/[Og.md "wikilink")\] 5g<sup>2</sup> 6f<sup>2</sup>
7d<sup>1</sup> 8s<sup>2</sup> 8p<sup>1</sup>
（預測\[1\]） |electrons per shell=2, 8, 18, 32, 34, 20, 9, 3
（預測） |physical properties=未知 |phase=固體 |phase comment=（預測） |density
gplstp= |density gpcm3nrt= |density gpcm3nrt 2= |density gpcm3nrt 3=
|density gpcm3mp= |melting point K= |melting point C= |melting point F=
|melting point pressure= |sublimation point K= |sublimation point C=
|sublimation point F= |sublimation point pressure= |boiling point K=
|boiling point C= |boiling point F= |boiling point pressure= |triple
point K= |triple point kPa= |triple point K 2= |triple point kPa 2=
|critical point K= |critical point MPa= |heat fusion= |heat fusion 2=
|heat fusion pressure= |heat vaporization= |heat vaporization pressure=
|heat capacity= |heat capacity pressure= |vapor pressure 1= |vapor
pressure 10= |vapor pressure 100= |vapor pressure 1 k= |vapor pressure
10 k= |vapor pressure 100 k= |vapor pressure comment= |atomic
properties=未知 |oxidation states= |oxidation states comment=
|electronegativity= |number of ionization energies= |1st ionization
energy= |2nd ionization energy= |3rd ionization energy= |atomic radius=
|atomic radius calculated= |covalent radius= |Van der Waals radius=
|crystal structure= |magnetic ordering= |electrical resistivity=
|electrical resistivity at 0= |electrical resistivity at 20= |thermal
conductivity= |thermal conductivity 2= |thermal diffusivity= |thermal
expansion= |thermal expansion at 25= |speed of sound= |speed of sound
rod at 20= |speed of sound rod at r.t.= |Tensile strength= |Young's
modulus= |Shear modulus= |Bulk modulus= |Poisson ratio= |Mohs hardness=
|Vickers hardness= |Brinell hardness= |CAS number= |isotopes=
|isotopes comment= }}

**126號元素**（**Unbihexium**）是一種尚未被發現的[化學元素](../Page/化學元素.md "wikilink")，它的暫定[化學符號是](../Page/化學符號.md "wikilink")**Ubh**，[原子序數是](../Page/原子序.md "wikilink")126，位於第8周期、g3族，屬於[g區元素之一](../Page/g區元素.md "wikilink")。雖然**126号元素**尚未被合成，但由于其处在假想的[稳定元素岛中而引起兴趣](../Page/稳定岛理论.md "wikilink")。

## 历史

Unbihexium的名字是作为元素周期表中的一个[占位符使用的](../Page/占位符.md "wikilink")，例如用于关于探求126号元素的科学文章中。[鈽以后的](../Page/鈽.md "wikilink")[超铀元素都是人工制造的](../Page/超铀元素.md "wikilink")，并且通常最终以科学家的名字或在原子物理中做出贡献的实验室的所在地的名字命名。

第一次嘗試合成Ubh 126号元素是在1971年，Bimot等人使用了所謂的“熱核聚變反應”\[2\]：

\[\,^{232}_{\ 90}\mathrm{Th} + \,^{84}_{36}\mathrm{Kr} \to \,^{316}_{126}\mathrm{Ubh} ^{*} \to \mathit{no\ atoms}\]
在實驗中，觀察到的具有高能量的α粒子，這可能可以視為成功合成Ubh 126号元素後發生的衰變現象，但最近的研究發現實際上該α粒子並不是來自Ubh
126号元素，因此該次實驗並沒有成功。

1976年，几位科学家宣称他们在一块[独居石中发现了](../Page/独居石.md "wikilink")126号元素以及其他一些元素。\[3\]\[4\]

## 性質

根据使用非相对论[Skyrme能量密度按](../Page/Skyrme.md "wikilink")[Hartree-Fock-Bogoliubov法进行的计算](../Page/Hartree-Fock-Bogoliubov法.md "wikilink")，其很可能是在一个稳定性"井"中或[稳定元素岛中最稳定的元素](../Page/稳定岛理论.md "wikilink")。較早期的文獻認為[<sup>310</sup>Ubh具有](../Page/Ubh的同位素.md "wikilink")[126個](../Page/126.md "wikilink")[質子](../Page/質子.md "wikilink")，與[184個](../Page/184.md "wikilink")[中子](../Page/中子.md "wikilink")，[184和](../Page/184.md "wikilink")[126都是](../Page/126.md "wikilink")[幻數](../Page/幻數.md "wikilink")，且根據[哈特里－福克方程](../Page/哈特里－福克方程.md "wikilink")，[126個](../Page/126.md "wikilink")[質子正好填滿了一個](../Page/質子.md "wikilink")[質子殼層](../Page/核殼層模型.md "wikilink")，故<sup>310</sup>Ubh可能具有很長的[半衰期](../Page/半衰期.md "wikilink")\[5\]\[6\]，並為穩定元素島的中央。然而後來的[核殼層模型認為中子的殼層可能與質子的殼層不同](../Page/核殼層模型.md "wikilink")，而認為穩定元素島的中央可能是<sup>354</sup>Ubh\[7\]。

[日本原子能研究开发机构所使用的](../Page/日本原子能研究开发机构.md "wikilink")[核素圖中預測了小於等於](../Page/核素圖.md "wikilink")*Z*
= 149和*N* = 256 的一系列核素之衰變方式。 而在*Z* = 126
(右上)，β穩定（不β衰變的界線）線穿過不穩定區域向自發裂變（半衰期小於1奈秒）的區域，並延伸到N
=
228填滿[核殼層位置的附近形成一個穩定的](../Page/核殼層模型.md "wikilink")“海岬”，其中可能包含了雙幻數的<sup>354</sup>Ubh，為穩定元素島的中央\[8\]。

### 估计的126号元素的外观和性质

  - 它是一种金属

  - 有放射性（或稳定的）

      - 半衰期（最稳定同位素）：

  - 颜色：

  -
  - 在空气中能被火引燃，像[镁一样发出非常明亮的光](../Page/镁.md "wikilink")

  - 是固体

      - 熔点：
      - 沸点：
      - 硬度：

### 合成方式

<table>
<thead>
<tr class="header">
<th><p>目標</p></th>
<th><p>發射體</p></th>
<th><p>CN</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><sup>182</sup>Hf</p></td>
<td><p><sup>136</sup>Xe</p></td>
<td><p><sup>318</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>232</sup>Th</p></td>
<td><p><sup>84</sup>Kr</p></td>
<td><p><sup>316</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>243</sup>Am</p></td>
<td><p><sup>67</sup>Zn</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>248</sup>Cm</p></td>
<td><p><sup>62</sup>Ni</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>249</sup>Bk</p></td>
<td><p><sup>61</sup>Ni</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>251</sup>Cf</p></td>
<td><p><sup>59</sup>Co</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>254</sup>Es</p></td>
<td><p><sup>56</sup>Fe</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>257</sup>Fm</p></td>
<td><p><sup>53</sup>Mn</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>260</sup>Md</p></td>
<td><p><sup>50</sup>Ti</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>268</sup>Db</p></td>
<td><p><sup>42</sup>Ca</p></td>
<td><p><sup>310</sup>Ubh</p></td>
<td></td>
</tr>
</tbody>
</table>

### 預測化學品

由於**Ubh**会迅速氧化所以可能形成[UbhO](../Page/氧化物.md "wikilink")（氧化Ubh），由於推測的外層電子（2,
8, 18, 32, 38, 18, 8,
2）最外層只有兩個，故易形成[Ubh<sup>2+</sup>的陽離子所以](../Page/陽離子.md "wikilink")[UbhO溶於水將形成](../Page/氧化物.md "wikilink")[Ubh](../Page/陽離子.md "wikilink")[(OH)<sub>2</sub>](../Page/氫氧根離子.md "wikilink")。預測**Ubh**能與[酸性物質反應](../Page/酸性.md "wikilink")，與[鹽酸](../Page/鹽酸.md "wikilink")（）反應也可能形成Ubh鹽即[UbhCl<sub>2</sub>](../Page/氯化物.md "wikilink")；與[硫酸](../Page/硫酸.md "wikilink")（[H<sub>2</sub>](../Page/氫.md "wikilink")[SO<sub>4</sub>](../Page/硫酸根離子.md "wikilink")）反應也可能形成Ubh的硫酸鹽即[Ubh](../Page/陽離子.md "wikilink")[SO<sub>4</sub>](../Page/硫酸根離子.md "wikilink")。

## 在小說中

在[John Byrne所著的](../Page/John_Byrne.md "wikilink")《[Action
Comics](../Page/Action_Comics.md "wikilink")》一书中，将[氪星石元素虚构为](../Page/氪星石.md "wikilink")[元素周期表中的](../Page/元素周期表.md "wikilink")126号元素。\[9\]

## 注釋

## 参考资料

<references />

  - [镎下元素](../Page/镎下元素.md "wikilink")（Eka-neptunium）—[钚下元素](../Page/钚下元素.md "wikilink")（Eka-plutonium）—[镅下元素](../Page/镅下元素.md "wikilink")（Eka-Americium）
  - [125号元素](../Page/Ubp.md "wikilink")–[127号元素](../Page/Ubs.md "wikilink")

[8H](../Category/第8周期元素.md "wikilink")
[8H](../Category/化学元素.md "wikilink")

1.

2.  [クリプトン加速による類似研究](http://prc.aps.org/abstract/PRC/v8/i1/p375_1)Physical
    Review C[アメリカ物理学会](../Page/アメリカ物理学会.md "wikilink")

3.  [Primordial Superheavy
    Element 126](http://prl.aps.org/abstract/PRL/v37/i11/p664_1) Phys.
    Rev. Lett. 37, 664 – Published 13 September 1976

4.  [Evidence for Primordial Superheavy
    Elements](http://prl.aps.org/abstract/PRL/v37/i1/p11_1) Phys. Rev.
    Lett. 37, 11 – Published 5 July 1976

5.

6.

7.
8.

9.  {{ Cite Web | url =
    <https://aggieblueprint.wordpress.com/2012/04/23/science-qa-is-there-such-thing-as-kryptonite/>
    | quote = Kryptonite was described in the first season episode of
    “Lois and Clark: The New Adventures of Superman” as a transuranic
    element (element 126) that eventually decays into solid iron. |
    title = Science Q\&A: Is there such thing as Kryptonite? | date =
    April 23, 2012 | publisher = BluePrint | accessdate = 2018-1-28}}