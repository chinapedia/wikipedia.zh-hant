**Xpdf**
是一個[開放原始碼的](../Page/開放原始碼.md "wikilink")[PDF檔案瀏覽器](../Page/PDF.md "wikilink")，此軟體運行於[X
Window以及](../Page/X_Window.md "wikilink")[Motif上](../Page/Motif.md "wikilink")。
Xpdf 也實際運行於所有[類Unix作業系統上](../Page/類Unix.md "wikilink")。Xpdf
可解碼[LZW壓縮格式並閱讀加密的PDF文件](../Page/LZW.md "wikilink")。官方版本的Xpdf遵循PDF檔案的[智慧財產權政策](../Page/智慧財產權.md "wikilink")，因此可能禁止拷貝、列印或轉換的功能。當然有某些破解補丁可以忽略這些智慧財產管理限制。

Xpdf包含數項不需要X
windows系統的程式，包含了解析PDF的圖檔以及將PDF轉檔成文字檔或[PostScript的程式](../Page/PostScript.md "wikilink")。

Xpdf也被其他PDF瀏覽程式用於前端，例如KPDF（一個運行在[KDE桌面的程式](../Page/KDE.md "wikilink")）。而它的文字引擎則被許多PDF瀏覽程式運用，例如[BeOS上的BePDF](../Page/BeOS.md "wikilink")、RISCOS上的\!PDF以及[Palm
OS上的PalmPDF](../Page/Palm_OS.md "wikilink")。

[Poppler以xpdf](../Page/Poppler.md "wikilink")
3.0的繪圖函式庫為基礎創造出來，以便增加其再用性。許多程式（包括Xpdf自己）可使用poppler為它們的後端繪圖器。

## 外部链接

  - [Xpdf 網站](http://www.foolabs.com/xpdf/)

## 參見

  - [Okular](../Page/Okular.md "wikilink")：支持PDF的多種文件查看器
  - [Evince](../Page/Evince.md "wikilink")
  - [PDF軟體列表](../Page/PDF軟體列表.md "wikilink")

[Category:X Window程式](../Category/X_Window程式.md "wikilink")
[Category:PDF閱讀器](../Category/PDF閱讀器.md "wikilink")
[Category:自由PDF软件](../Category/自由PDF软件.md "wikilink")