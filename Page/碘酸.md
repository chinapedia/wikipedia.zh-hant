**碘酸**（[化学式](../Page/化学式.md "wikilink")：HIO<sub>3</sub>）比[溴酸和](../Page/溴酸.md "wikilink")[氯酸稳定](../Page/氯酸.md "wikilink")，可以呈[晶体状态存在](../Page/晶体.md "wikilink")，110℃分解同时有一部分脱水形成三碘酸（HI<sub>3</sub>O<sub>8</sub>），195℃脱水成[五氧化二碘](../Page/五氧化二碘.md "wikilink")（I<sub>2</sub>O<sub>5</sub>），易溶于水，为强氧化剂及强酸。

[Category:无机酸](../Category/无机酸.md "wikilink")
[\*](../Category/碘酸盐.md "wikilink")
[Category:腐蝕性化學品](../Category/腐蝕性化學品.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")