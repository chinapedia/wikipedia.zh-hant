**SLS拉斯維加斯賭場酒店**（**SLS Las Vegas Hotel &
Casino**）是一個位於美國內華達州拉斯維加斯[賭城大道上的高級](../Page/賭城大道.md "wikilink")[酒店和](../Page/酒店.md "wikilink")[賭場](../Page/賭場.md "wikilink")。由和Stockbridge地產公司持有，目前正在施工改裝中，耗資4.15億美元，預計在2014年秋季開幕後加入SBE集團旗下的SLS酒店連鎖\[1\]。

酒店的前身為**撒哈拉賭場酒店**（**Sahara Hotel and
Casino**），自1952年開業至2011年結業間共營運了59年。酒店以北非[摩洛哥為主題](../Page/摩洛哥.md "wikilink")，曾擁有1,720間酒店客房以及85,000平方公尺的賭場。酒店是[拉斯維加斯單軌鐵路最北的站點](../Page/拉斯維加斯單軌鐵路.md "wikilink")，也是[拉斯維加斯大道東端最北的酒店](../Page/拉斯維加斯大道.md "wikilink")。1950年代著名五人組合「[鼠幫](../Page/鼠幫.md "wikilink")」（Rat
Pack）曾在拉斯維加斯多間賭場酒店長期巡迴演出，而撒哈拉酒店也是唯一現存他們曾表演過的酒店。為了確保主題統一，連酒店大門外上下車的地方也被設計成摩洛哥風格的建築。

## 歷史

撒哈拉酒店於1952年開幕，是拉斯維加斯大道的第六間酒店。在1954年末，酒店邀請了著名的[爵士樂音樂家](../Page/爵士樂.md "wikilink")[路易斯·派馬](../Page/路易斯·派馬.md "wikilink")（Louis
Prima）於酒店的酒廊駐場表演，此舉在當時的拉斯維加斯是屬於一種新的表演概念，在派馬和他的妻子姬莉·史密夫（Keely
Smith）和[薩克管手森姆](../Page/薩克管.md "wikilink")·布迪拉（Sam
Butera）的帶領下，酒店的爵士樂表演成為拉斯維加斯大道最熱門的項目之一。

1961年，酒店被戴爾·韋伯（Del
Webb）收購。其後於1963年，酒店的24層高新酒店大樓落成。1995年酒店再度易手，比爾·賓納特（Bill
Bennett）收購了酒店，並於翌年增建了一幢26層高的酒店大樓以及一個新的酒店入口。另外，於1999年再增建一個過山車設施。

2011年5月16日，酒店因不敵大型賭場度假村競爭，最終結業\[2\]。

## 在此拍攝的電影

  - 《[-{zh-hans:十一羅漢;zh-hk:十一羅漢;zh-tw:瞞天過海;}-](../Page/十一羅漢_\(2001年電影\).md "wikilink")》（*Ocean's
    Eleven*）

## 參考資料

## 外部連結

  - [SLS拉斯維加斯酒店官方網站](http://slshotels.com/lasvegas/)

[Category:拉斯維加斯賭場](../Category/拉斯維加斯賭場.md "wikilink")
[Category:賭城大道](../Category/賭城大道.md "wikilink")
[S](../Category/拉斯維加斯度假村.md "wikilink")
[S](../Category/內華達州地標.md "wikilink")

1.
2.  [不敵新型度假村
    賭城第一代賭場執笠](http://hk.news.yahoo.com/article/110515/4/obob.html)