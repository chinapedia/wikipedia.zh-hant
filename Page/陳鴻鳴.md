[Tan_Hong-beng_(taiwan_nichinichi_shashin).jpg](https://zh.wikipedia.org/wiki/File:Tan_Hong-beng_\(taiwan_nichinichi_shashin\).jpg "fig:Tan_Hong-beng_(taiwan_nichinichi_shashin).jpg")

**陳鴻鳴**（；﹞，字子襟，生於[善化里西堡灣裡庄](../Page/善化里西堡.md "wikilink")（今[台灣](../Page/台灣.md "wikilink")[臺南市](../Page/臺南市.md "wikilink")[善化區](../Page/善化區.md "wikilink")）。[台灣日治時期的糖業富商](../Page/台灣日治時期.md "wikilink")，也是任職於[台灣總督府的少見](../Page/台灣總督府.md "wikilink")[台灣人官員](../Page/台灣人.md "wikilink")。

## 生平

陳鴻鳴家族來自[中國大陸](../Page/中國大陸.md "wikilink")。[明鄭時期](../Page/明鄭時期.md "wikilink")，陳鴻鳴的先祖擔任鄭軍部將，後則因戰功，授官田於灣裡（今善化）。[清治時期](../Page/台灣清治時期.md "wikilink")，族人多棄官從商，成為巨富。

1895年5月，[日](../Page/日.md "wikilink")[台](../Page/台.md "wikilink")[乙未戰爭爆發](../Page/乙未戰爭.md "wikilink")，陳鴻鳴堂兄[陳子鏞任](../Page/陳子鏞.md "wikilink")[台灣民主國臺南籌防總局長](../Page/台灣民主國.md "wikilink")，負責軍事籌款等職。同年10月，日軍攻佔台南，陳子鏞隨軍隊敗退[廈門](../Page/廈門.md "wikilink")，不過未成年的陳鴻鳴則並未跟隨內渡[唐山](../Page/唐山.md "wikilink")。

1896年，[日治時期開始後](../Page/台灣日治時期.md "wikilink")，陳鴻鳴即入灣裡街國語傳習所就讀。學成後因日語流利，隨後任善化里東堡區長、臺南廳灣裡街[辨務署參事等官職](../Page/辨務署.md "wikilink")。20世紀初，他除了移居[臺南市](../Page/臺南市.md "wikilink")，並與[王雪農繼續於台南州州內共創多所糖廠](../Page/王雪農.md "wikilink")，之後歷任任臺南州協議會會員、總督府評議會會員等職，為台灣日治時期少有的台人官員。

戰後，日治時期結束，台灣進入[中華民國時期](../Page/中華民國.md "wikilink")，此期間，他則轉任臺南市第四信用合作社理事主席，並於任內去世。

## 經歷

  - 臺灣總督府評議會員
  - 灣裡辦務署參事、紳章、大目降參事、灣裡公校委員
  - [善化里東堡庄長](../Page/善化里東堡.md "wikilink")\[1\]、赤十字社委員、地方稅調查委員、東區[土地整理組合長](../Page/土地整理組合長.md "wikilink")
  - 臺南製糖創立委員兼監查役
  - 永興製糖社長
  - 南報取締役
  - 灣裡製酒組合長
  - 苗栗製糖取締役
  - 臺灣糖蜜取締役
  - 臺南廳參事
  - 臺灣與業信託與臺灣經鐵兩社取締役
  - 恩賜財團明治救濟會評議員
  - 臺南大舞臺株式會社取締役
  - 臺南公館評議員、大禮紀念章、東洋協會評議員
  - 臺南第二幼稚園幹事
  - 臺南習俗改良副會長、臺南協會長、臺南廰農會評議員、商工校商議員
  - 東京電機品具製造兼南部海產兩會社取締役、臺南信組興信社監事、咸鶯礦株式會社取締役、
  - 臺南女公校委員、臺南公會副會長、三公學務委員、臺灣製鹽取締役、
  - 嘉南大圳會議員、州議員、勸業委員、州議會常置委員、防疫組合常設委員、所得稅州稅調查委員、督府史料編纂委員會顧問

## 參考文獻

  - 《臺南州名士錄》
  - 《台南市志‧卷七人物志》

[Category:台灣日治時期政治人物](../Category/台灣日治時期政治人物.md "wikilink")
[Category:台灣日治時期企業家](../Category/台灣日治時期企業家.md "wikilink")
[Category:善化人](../Category/善化人.md "wikilink")
[Hong鴻](../Category/陳姓.md "wikilink")
[Category:台灣紳章附與人物](../Category/台灣紳章附與人物.md "wikilink")
[Category:臺灣總督府評議會員](../Category/臺灣總督府評議會員.md "wikilink")

1.  <http://nrch.cca.gov.tw/metadataserverDevp/DOFiles/pdf/00/05/73/62/cca100003-od-ta_04413_000253-0001-i.pdf>
    臺南廳善化里東堡東勢寮庄理由書 od-ta_04413_000253，國家文化資料庫