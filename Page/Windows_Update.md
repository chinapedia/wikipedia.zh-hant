**Windows Update**是一個基於[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[操作系统的](../Page/操作系统.md "wikilink")[软件更新服務](../Page/软件更新.md "wikilink")。提供與之前的**Microsoft
Update**相同的服務以外，Windows Update更能為其他較近代Microsoft產品，如[Microsoft
Office](../Page/Microsoft_Office.md "wikilink")，SQL Server
2005等，檢測更新或其他修補。\[1\]

## 介紹

**Windows Update**及**Microsoft
Update**能夠提供一个下载重要[系统组件更新](../Page/系统组件.md "wikilink")、[服务升级包](../Page/服务升级包.md "wikilink")（）、hotfix程序（）、[补丁以及內建系統程序的更新](../Page/补丁.md "wikilink")，會自动查詢使用者的[硬件并提供能使用的](../Page/计算机硬件.md "wikilink")[驱动程序更新](../Page/驱动程序.md "wikilink")，又或者提供一些[微软](../Page/微软.md "wikilink")[程序的](../Page/计算机程序.md "wikilink")[測試](../Page/測試.md "wikilink")[版本](../Page/版本.md "wikilink")。

大多数的更新和补丁会在它们被发布后出现在Windows Update。修补程序和更新通常能够在Microsoft 下載中心上手动下载。

大部分运行Microsoft
Windows的[办公网络和](../Page/办公网络.md "wikilink")[服务器可能会发现使用](../Page/服务器.md "wikilink")[Windows
Server Update
Services更加实用](../Page/Windows_Server_Update_Services.md "wikilink")，因为它使通过Windows
Update接收补丁并分发到各个客户端的过程變得更簡單。

Windows Update
网站需要[ActiveX控制的使用](../Page/ActiveX.md "wikilink")，故需要[Internet
Explorer或者Internet](../Page/Internet_Explorer.md "wikilink")
Explorer外掛代替。同样，它被使用微软的[脚本语言](../Page/脚本语言.md "wikilink")[VBScript和](../Page/VBScript.md "wikilink")[JScript编码](../Page/JScript.md "wikilink")。它和使用IE核心的瀏覽器(例如：Netscape
8.0)相容。

在2005年，微软介绍了一款選用的Microsoft
Update，它可将受支持的[作業系统](../Page/作業系统.md "wikilink")，包含[Microsoft
Office](../Page/Microsoft_Office.md "wikilink")（Office 2007和Office
2010，仅支持所有用户安装模式），[Exchange和](../Page/Microsoft_Exchange_Server.md "wikilink")[SQL
Server的更新递送](../Page/SQL_Server.md "wikilink")。这对许多抱怨很难从微软产品网站中得到所有Microsoft產品重要更新的用户作出了回应。

## 历史

一直以来经过多次修订的Windows Update網站是在[Windows
98版本发布时被首次引入的](../Page/Windows_98.md "wikilink")。

在2004年的晚些时候，微软发布了包含[Service Pack
2和一些针对更新程序主要更改的](../Page/Windows_XP#Service_Pack_2.md "wikilink")
Windows Update 5 for [Windows
XP](../Page/Windows_XP.md "wikilink")。没有宽带访问的用户还可从微软网站预订一张XP
Service Pack 2
[CD](../Page/CD.md "wikilink")；这甚至不需要任何运送费用，并且包装上鼓励与其他Windows
XP用户共享这张CD。在安装SP2之后，用户应该前往微软网站检查自SP2发布以后的新版本更新。

2005年7月，微软提出了面向升级任意版本[Windows
XP](../Page/Windows_XP.md "wikilink")，[Windows Server
2003和](../Page/Windows_Server_2003.md "wikilink")[Windows
2000用户的](../Page/Windows_2000.md "wikilink")[Windows
正版增值计划](../Page/Windows_正版增值计划.md "wikilink")（）。作为一个升级程序对待，该软件会分析用户的计算机，并测定运行的Windows
XP副本是否为正版。如果被检测的软件副本为盗版，它会提供给用户一个机会，揭发购买该盗版副本的地点，并以此可得到一份免费或者减价的正版Windows
XP副本。然而与一些传闻恰恰相反，WGA不会为非法软件下载关键补丁。

### Windows XP中的Windows Update

[WindowsUpdatev6.png](https://zh.wikipedia.org/wiki/File:WindowsUpdatev6.png "fig:WindowsUpdatev6.png")
Windows Update通过[Internet
Explorer网络浏览器自动控制安装补丁](../Page/Internet_Explorer.md "wikilink")。

### Windows Vista中的Windows Update

在[Windows Vista中](../Page/Windows_Vista.md "wikilink")，Windows
Update不是一个网络应用程序。取而代之，所有Windows
Update网站功能已經被整合到一个新的[控制台中](../Page/控制台.md "wikilink")。另外，Windows
Update将负责递送Vista的[Windows
Defender](../Page/Windows_Defender.md "wikilink")[反间谍软件产品的最新定义](../Page/反间谍软件.md "wikilink")，还有[Windows
Mail的垃圾邮件过滤器更新](../Page/Windows_Mail.md "wikilink")。Windows
Update将同样為其他微软产品，比如[Microsoft
Office提供自动更新](../Page/Microsoft_Office.md "wikilink")，用戶可自行選擇是否開啟Microsoft
Update。 基于网络范例的Windows
Update程序的退出，减弱了许多评论家对于运行Windows系统的网络浏览器完成信任的升级操作的能力问题的担忧。

### Windows 7中的Windows Update

[Windows7_windows_update.jpg](https://zh.wikipedia.org/wiki/File:Windows7_windows_update.jpg "fig:Windows7_windows_update.jpg")
在[Windows 7中](../Page/Windows_7.md "wikilink")，Windows
Update的功能也是整合在了控制面板中。功能比Windows Vista中的Windows
Update更加强大，操作也更加人性化。提供几乎所有Microsoft产品的更新支援。

### Windows 8中的Windows Update

在[Windows 8中](../Page/Windows_8.md "wikilink")，Windows
Update除了出現在控制台(Control
Panel)，另外也出現在Windows新推出的設定(Setting)下。

### Windows 8.1中的Windows Update

### Windows 10中的Windows Update

[10-windows-update.png](https://zh.wikipedia.org/wiki/File:10-windows-update.png "fig:10-windows-update.png")
在[Windows
10中](../Page/Windows_10.md "wikilink")，改成只出現在\[設定\](Settings)的\[更新與安全性\](Update
and
Security)下，更新設定不再讓使用者選擇(家用版完全無法延後更新)，改成在使用者使用電腦或行動裝置時背景自動下載與安裝，並要求使用者重新啟動裝置。

## 微软“正版增值”计划

发布于2005年7月28日的Windows正版增值计划是被微软公司用来在用户寻找他们运行的微软软件副本的升级程序时强制执行的许可协议。它会在允许获取任何非紧急的安全更新之前鉴别Windows的合法性。

最初它被认为将不会阻止未经鉴别的用户通过自动更新服务接收紧急的安全更新，因为微软认为，相对于盗版副本带来的收益损失，未打补丁系统的安全和名誉风险更加重要。然而事实并非如此。WGA使用一套独立的程序生成一个密钥，或者一个[ActiveX插件控制](../Page/ActiveX.md "wikilink")，来检查许可密钥是否有效。

[WindizUpdateEnglishOnlyScreen.png](https://zh.wikipedia.org/wiki/File:WindizUpdateEnglishOnlyScreen.png "fig:WindizUpdateEnglishOnlyScreen.png")
WGA系统已经被破解多次。开始时使用一条[JavaScript代码](../Page/JavaScript.md "wikilink")（在2005年7月28日WGA发布24小时内），后来则通过软件。

此外使用不需使用浏览器的[AutoPatcher或者](../Page/AutoPatcher.md "wikilink")[Bigfix](http://www.bigfix.com)。然而多数的Windows
XP用户都使用着开启Windows Update服务的默认设置。这意味着大多数Windows XP用户在运行最新版本的Internet
Explorer，并在他们的操作系统中安装有最新的补丁程序。

## 周二补丁日

周二补丁日（Patch Tuesday）是指微軟每月的第二個星期二定期發佈的系統更新。

## 更新理由

**Windows Update** 協助於電腦更安全及運作順暢而簡單又免費。微軟建議用戶,每週至少檢查一次更新或開啟自動更新。

## 第三方方案

在2005年1月，一个针对Windows
Update的第三方方案WindizUpdate被启用。这个服务支持基于[Mozilla和](../Page/Mozilla.md "wikilink")[Opera的浏览器](../Page/Opera.md "wikilink")，不必通过Internet
Explorer而允许用户接收类似的服务。然而，这项服务的合法性尚存疑问。

## 外部链接

  - [Windows Update](http://www.windowsupdate.com)

## 參考資料

<references />

[Category:Windows组件](../Category/Windows组件.md "wikilink")
[Category:微軟網站](../Category/微軟網站.md "wikilink")

1.  <http://www.microsoft.com/taiwan/athome/security/update/msupdate_keep_current.mspx>