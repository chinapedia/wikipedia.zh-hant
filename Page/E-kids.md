**E-kids**是[香港一隊](../Page/香港.md "wikilink")[男子音樂組合](../Page/男子音樂組合.md "wikilink")，於2002年出道，初期成員包括[阮民安](../Page/阮民安.md "wikilink")（Tommy）、林詠倫（Alan）和[郁禮賢](../Page/郁禮賢.md "wikilink")（Tim/Kasa）。於2004年，[郁禮賢離開E](../Page/郁禮賢.md "wikilink")-kids獨立發展，其後加入了[林中定](../Page/林中定.md "wikilink")（Nick）。2006年，E-kids解散。2013年4月6日，Alan（林詠倫）和Tim（郁禮賢）重組成為E-kids@2，於[九龍灣](../Page/九龍灣.md "wikilink")[國際展貿中心舉辦演唱會](../Page/國際展貿中心.md "wikilink")。2016年8月20日，E-Kids宣佈重組。\[1\]
2017年1月21日舉行E-KIDS 3XL Concert 2017，門票首天開賣便已沽清。\[2\]

## 簡介

本來Tommy、Alan 和 Tim
只是在[Stareast的](../Page/東方魅力.md "wikilink")[餐廳工作](../Page/餐廳.md "wikilink")，後來在樂壇校長[譚詠麟的提攜下成為組合](../Page/譚詠麟.md "wikilink")，曾經擔任其[演唱會嘉賓](../Page/演唱會.md "wikilink")。他們在1999年參加了[Stareast新人類](../Page/Stareast新人類.md "wikilink")。2000年他們以**E-kids**名義參加「全球華人新秀歌唱大賽」，並獲得「最受觀眾歡迎獎」。經過一年的訓練後，他們在2002年正式出道，組成**E-kids**。**E**是代表他們的公司[StarEast](../Page/東方魅力.md "wikilink")，而他們又是公司的小朋友，所以叫**kids**，合併就是「**E-kids**」。**E-kids**的歌曲大部由Tommy作曲，舞蹈則由Tim安排。E-kids的形象都以他們的金髮建立，只有錄製兒歌MV才會染回黑髮。

2004年，有傳聞Tommy及Alan會過檔到[范振鋒所設的新公司](../Page/范振鋒.md "wikilink")。另外，E-kids另一成員Tim改名為Kasa，並離開E-kids作獨立發展，成員Tommy與Alan也會繼續出唱片。到了7月，**E-kids**的去向終於落實了，他們會加入Alan的舊同學[林中定](../Page/林中定.md "wikilink")，而他們在[譚詠麟](../Page/譚詠麟.md "wikilink")，[曾志偉的推薦下](../Page/曾志偉.md "wikilink")，加入了[環球唱片公司](../Page/環球唱片.md "wikilink")，但是推出一首單曲後E-kids就解散了。

其後Tommy獨立發展，推出的歌曲以Hip-Hop([嘻哈](../Page/嘻哈.md "wikilink"))音樂為主。而另一成員Alan則簽約成為[東美廣告娛樂旗下藝人](../Page/東美廣告娛樂.md "wikilink")。

## 成員資料

  - [郁禮賢](../Page/郁禮賢.md "wikilink") （，1981年7月20日）
  - [阮民安](../Page/阮民安.md "wikilink") （，1980年5月13日）
  - 林詠倫（已改名為林泳） （，1980年4月18日）
  - [林中定](../Page/林中定.md "wikilink")（）

## 唱片

  - 《[Smile](../Page/Smile.md "wikilink")》 （唱片公司：東方魅力） (2003年8月)

<!-- end list -->

1.  第一個唱
2.  最佳表情
3.  青春火花
4.  玩玩具
5.  倉鼠俱樂部（TVB卡通片《[倉鼠俱樂部](../Page/倉鼠俱樂部.md "wikilink")》主題曲）
6.  第一個唱 (featuring [和田裕美](../Page/和田裕美.md "wikilink"))

<!-- end list -->

  - 《[戀愛秘笈](../Page/戀愛秘笈.md "wikilink")》（國語）（唱片公司：東方魅力）(2002年12月)

<!-- end list -->

1.  聽到留言請回電
2.  愛情現金卡
3.  戀愛秘笈
4.  班對
5.  私人時段 (粵)
6.  青春變魔術 (粵)
7.  初哥 (粵)
8.  魅力移動 (粵)

<!-- end list -->

  - 《[E-Kids Super
    Diary](../Page/E-Kids_Super_Diary.md "wikilink")》（唱片公司：東方魅力）(2002年12月)

<!-- end list -->

1.  Computer Date File
2.  韓國MV Making of
3.  初哥 MV
4.  開始戀愛 MV
5.  下學期 MV
6.  韓國MV Making of
7.  初哥首播會片段
8.  初哥
9.  開始戀愛
10. 下學期
11. 青春變魔術
12. 私人時段
13. 青春變魔術 Magic Tommy Mix

<!-- end list -->

  - 《[E-Kids
    Diary](../Page/E-Kids_Diary.md "wikilink")》（唱片公司：東方魅力）(2002年12月)

<!-- end list -->

1.  Computer Date File
2.  青春變魔術 MV
3.  私人時段 MV
4.  初哥首播會片段
5.  初哥
6.  開始戀愛
7.  下學期
8.  青春變魔術
9.  私人時段
10. 青春變魔術Magic Tommy Mix

<!-- end list -->

  - 《[E-Kids初哥 IST Dance
    Album](../Page/E-Kids初哥_IST_Dance_Album.md "wikilink")》（唱片公司：東方魅力）(2002年)

<!-- end list -->

1.  遠赴韓國拍攝之初哥MV
2.  E-Kids親身教跳初哥Step By Step
3.  E-Kids真情對話+拍攝花絮
4.  獨家E-Kids Wallpaper+Screen Saver
5.  初哥Step By Step貼紙
6.  E-Kids初回寫真集

<!-- end list -->

  - 《[魅力移動](../Page/魅力移動.md "wikilink")》（與[EO2](../Page/EO2.md "wikilink")，[張詠詩合輯](../Page/張詠詩.md "wikilink")；唱片公司：東方魅力）(2002年)

<!-- end list -->

1.  魅力移動　
2.  普通朋友
3.  上一課
4.  青春變魔術
5.  魂遊
6.  One Day
7.  私人時段
8.  普通朋友 (Rage Mix)
9.  上一課 Get Up Mix
10. 青春變魔術 Magic Tommy Mix

## 未出碟歌曲

  - 再見4.1 - **E-kids**
  - Thriller - **E-kids** (Tim/Alan)
  - 全部都係Kids - **E-kids**

## 電影

**2006年:**

  - 《[半醉人間](../Page/半醉人間.md "wikilink")》
  - 《[打雀英雄傳](../Page/打雀英雄傳.md "wikilink")》
  - 《[龍虎門](../Page/龍虎門_\(2006年電影\).md "wikilink")》
      - [阮民安](../Page/阮民安.md "wikilink") 飾 光頭星
      - 林詠倫 飾 獨眼龍
      - [林中定](../Page/林中定.md "wikilink") 飾 黑仔傑

**2005年:**

  - 《[擁抱每一刻花火](../Page/擁抱每一刻花火.md "wikilink")》
  - 《[三岔口](../Page/三岔口.md "wikilink")》
  - 《[早熟](../Page/早熟.md "wikilink")》
      - [阮民安](../Page/阮民安.md "wikilink") 飾 Roger

**2004年:**

  - 《[旺角黑夜](../Page/旺角黑夜.md "wikilink")》
      - [阮民安](../Page/阮民安.md "wikilink") 飾 靚輝手下

**2003年:**

  - 《[魔宅之鐵鎚凶靈](../Page/魔宅之鐵鎚凶靈.md "wikilink")》
  - 《[絕種鐵金剛](../Page/絕種鐵金剛.md "wikilink")》
  - 《[愛火花](../Page/愛火花.md "wikilink")》

**2002年:**

  - 《[走火槍](../Page/走火槍.md "wikilink")》

**2000年:**

  - 《[唔該借歪\!\!](../Page/唔該借歪!!.md "wikilink")》

## 電視劇

**2003年:**

  - [TVB](../Page/TVB.md "wikilink")《[當四葉草碰上劍尖時](../Page/當四葉草碰上劍尖時.md "wikilink")》
    飾 學生（客串）

**2002年:**

  - [香港電台電視劇](../Page/香港電台.md "wikilink")《平等機會檔案》
  - [Now.com網劇](../Page/Now.com.md "wikilink")《[吻了再說](../Page/吻了再說.md "wikilink")》
    飾 E-kids

## 電視節目

**2002年:**

  - [吾係獎門人](../Page/吾係獎門人.md "wikilink")（第7集嘉賓）

**2017年：**

  - E-Kids 不喜勿插

## 商品代言人

**2003年:**

  - [數碼通](../Page/數碼通.md "wikilink")[澳門](../Page/澳門.md "wikilink") 「點點通」
    增值咭宣傳大使

**2002年:**

  - [Locman](../Page/Locman.md "wikilink")
    [手錶香港區代言人](../Page/手錶.md "wikilink")

## 獎項

**2003年:**

  - 2003年度[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")——十大兒歌金曲《倉鼠俱樂部》
  - 2003年度[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")——最受爹o地媽咪歡迎兒歌大獎《倉鼠俱樂部》
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2003——新城勁爆跳舞歌曲《最佳表情》
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2003——新城勁爆跳舞歌曲《最佳表情》
  - 2003年[勁歌金曲第三季季選](../Page/勁歌金曲第三季季選.md "wikilink")——最佳改編歌曲演繹獎《玩玩具》
  - [全球華人新秀歌唱大賽](../Page/全球華人新秀歌唱大賽.md "wikilink")——最受觀眾歡迎獎
  - [PM新勢力頒獎禮](../Page/PM.md "wikilink")——PM至yeah

**2002年:**

  - [E\*plus 樂壇新人巡禮](../Page/E*plus_樂壇新人巡禮.md "wikilink")2002——新人賞
  - [TVB](../Page/TVB.md "wikilink")
    2002年[勁歌金曲第二季季選](../Page/勁歌金曲第二季季選.md "wikilink")——最受歡迎合唱歌曲獎《魅力移動》
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2002——勁爆新登場組合
  - 2002年度[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")——十大兒歌金曲《青春變魔術》
  - 3周刊至愛人氣藝人大賞——人氣組合
  - 第二屆[PM夏日人氣頒獎禮](../Page/PM.md "wikilink")——PM夏日活力人氣跳舞組合獎
  - [廣州台勁歌王頒獎禮](../Page/廣州台.md "wikilink")——人氣組合獎
  - [TVB](../Page/TVB.md "wikilink") 2002年度十大勁歌金曲頒獎禮——
    \<最受歡迎合唱歌曲（金獎）《魅力移動》
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2002——新城勁爆新登場組合
  - [便利娛樂新人王](../Page/便利娛樂新人王.md "wikilink")2002——最受現場觀眾歡迎大獎
  - [便利娛樂新人王](../Page/便利娛樂新人王.md "wikilink")2002——組合新人王

## 參見

  - [東方魅力](../Page/東方魅力.md "wikilink")

## 參考資料

[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")

1.  [【大師兄返嚟喇】MK-Pop始祖E-kids宣佈重組！](http://sina.com.hk/news/article/20160821/0/3/1/%E5%A4%A7%E5%B8%AB%E5%85%84%E8%BF%94%E5%9A%9F%E5%96%87-MK-Pop%E5%A7%8B%E7%A5%96E-kids%E5%AE%A3%E4%BD%88%E9%87%8D%E7%B5%84-6199913.html)
2.  [E-kids演唱會門票一日賣清光 Tim:
    希望再開多場](http://hk.apple.nextmedia.com/nextplus/%E5%A8%9B%E6%A8%82%E8%A6%81%E8%81%9E/article/20161130/2_456645_0/E-kids%E6%BC%94%E5%94%B1%E6%9C%83%E9%96%80%E7%A5%A8%E4%B8%80%E6%97%A5%E8%B3%A3%E6%B8%85%E5%85%89-Tim-%E5%B8%8C%E6%9C%9B%E5%86%8D%E9%96%8B%E5%A4%9A%E5%A0%B4)