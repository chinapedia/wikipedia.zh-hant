[Resistivity_geometry.png](https://zh.wikipedia.org/wiki/File:Resistivity_geometry.png "fig:Resistivity_geometry.png")
**电导率**（）是表示物质传输[电流能力强弱的一种測量值](../Page/电流.md "wikilink")。當施加[電壓於](../Page/電壓.md "wikilink")[導體的兩端時](../Page/導體.md "wikilink")，其[電荷載子會呈現朝某方向流動的行為](../Page/電荷載子.md "wikilink")，因而產生電流。電導率
\(\sigma\,\!\)
是以[歐姆定律定義為](../Page/歐姆定律.md "wikilink")[電流密度](../Page/電流密度.md "wikilink")
\(\mathbf{J}\,\!\) 和[電場強度](../Page/電場.md "wikilink") \(\mathbf{E}\,\!\)
的比率：

\[\mathbf{J} = \sigma \mathbf{E}\,\!\] 。

有些物質會有[異向性](../Page/異向性.md "wikilink") () 的電導率，必需用 3 X 3
[矩陣來表達](../Page/矩陣.md "wikilink")（使用數學術語，第二階[張量](../Page/張量.md "wikilink")，通常是[對稱的](../Page/對稱矩陣.md "wikilink")）。

電導率是[电阻率](../Page/电阻率.md "wikilink") \(\rho\,\!\)
的[倒數](../Page/倒數.md "wikilink")。在[國際單位制中的單位是](../Page/國際單位制.md "wikilink")[西門子](../Page/西门子_\(单位\).md "wikilink")/[公尺](../Page/公尺.md "wikilink")
(S·m<sup>-1</sup>)：

\[\sigma = {1\over\rho}\,\!\] 。

[電導率儀](../Page/電導率儀.md "wikilink") () 是一種是用來測量溶液電導率的儀器。

## 电传导性

**电传导性** ()
是物质可以传导[电子的性质](../Page/电子.md "wikilink")。按物质是否具有电传导性，可把物质分为[导体](../Page/导体.md "wikilink")，[半导体和](../Page/半导体.md "wikilink")[绝缘体](../Page/绝缘体.md "wikilink")。

  - 導體：[金屬](../Page/金屬.md "wikilink")、[电解质溶液](../Page/电解质.md "wikilink")，一般有很高的電導率，很低的電阻率。
  - 絕緣體：像[玻璃](../Page/玻璃.md "wikilink")、干燥的[木材](../Page/木材.md "wikilink")、[塑料](../Page/塑料.md "wikilink")、[橡胶或](../Page/橡胶.md "wikilink")[真空這類物質的電導率很低](../Page/真空.md "wikilink")，電阻率很高。
  - 半導體：電導率在導體和絕緣體之間。在不同的狀況下，電導率會有很大的變化。例如，暴露於電場或某種頻率的[光波](../Page/光波.md "wikilink")，最重要地，溫度和半導體材料的成份。

固態半導體的摻雜程度會造成電導率很大的變化。增加摻雜程度會造成高電導率。[水](../Page/水.md "wikilink")[溶液的電導率高低跟其內含](../Page/溶液.md "wikilink")[溶質](../Page/溶質.md "wikilink")[鹽的](../Page/鹽.md "wikilink")[濃度有關](../Page/濃度.md "wikilink")，或其它會分解為[電解質的化學雜質](../Page/電解質.md "wikilink")。水樣本的電導率是測量水的含鹽成分、含離子成分、含雜質成分等等的重要指標。水越純淨，電導率越低（電阻率越高）。水的電導率時常以**電導係數**來紀錄；電導係數是水在
25°C 溫度的電導率。

## 一些物質的電導率

|                                                             |                            |         |                                                                                                                                                                               |
| ----------------------------------------------------------- | -------------------------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 物質                                                          | 電導率　 (S·m<sup>-1</sup>)    | 溫度 (°C) | 註記                                                                                                                                                                            |
| [銀](../Page/銀.md "wikilink")                                | 63.01 × 10<sup>6</sup>     | 20      | 所有金屬中電導率（以及[熱導率](../Page/熱導率.md "wikilink")）最高的物質                                                                                                                             |
| [銅](../Page/銅.md "wikilink")                                | 59.6 × 10<sup>6</sup>      | 20      |                                                                                                                                                                               |
| [退火](../Page/退火.md "wikilink") [銅](../Page/銅.md "wikilink") | 58.0 × 10<sup>6</sup>      | 20      | 一般是指 100% IACS 或 100% 國際退火銅標準                                                                                                                                                 |
| [金](../Page/金.md "wikilink")                                | 45.2 × 10<sup>6</sup>      | 20      | 時常用為[電路板或](../Page/電路板.md "wikilink")[電子元件的](../Page/電子元件.md "wikilink")[電連接器](../Page/電連接器.md "wikilink") () 材料                                                              |
| [鋁](../Page/鋁.md "wikilink")                                | 37.8 × 10<sup>6</sup>      | 20      |                                                                                                                                                                               |
| [海水](../Page/海水.md "wikilink")                              | 4.788                      | 20      | 在平均鹽度為 4.8(S·m<sup>-1</sup>) ，溫度為 20(°C) 狀況的平均電導率。詳細資料請參閱[英國國家物理實驗室的](../Page/英國國家物理實驗室.md "wikilink") [國家標準資料](http://www.kayelaby.npl.co.uk/general_physics/2_7/2_7_9。html) |
| [飲用水](../Page/飲用水.md "wikilink")                            | 0.0005 至 0.05              |         | 這是一般高品質飲用水的電導率值域，並不是水品質指標                                                                                                                                                     |
| [去離子水](../Page/去離子水.md "wikilink")                          | 5.5 × 10<sup>-6</sup>\[1\] |         | 假若水內不含氣體，則改變數值為 1.2 × 10<sup>-4</sup>\[2\]                                                                                                                                    |

## 複電導率

假若物質處於隨時間而變的電場，則電導率為複數（對於各向異性物質，電導率為複矩陣），稱為[導納率](../Page/導納率.md "wikilink")
() 。這方法可以應用於[電阻抗斷層成像](../Page/電阻抗斷層成像.md "wikilink") ()
，一種工業或[醫學成像](../Page/醫學成像.md "wikilink")。

另外一種計算[交流的方法使用實值的電導率](../Page/交流.md "wikilink")（跟時間有關）和實值的[電容率](../Page/電容率.md "wikilink")。電導率越大，物質會越快吸收交流訊號，物質越[不透明](../Page/透明.md "wikilink")。

## 電導率與溫度的關係

電導率與溫度紧密相關。金屬的電導率隨著溫度的增高而降低。半導體的電導率隨著溫度的增高而增高。在一段溫度值域內，電導率可以被近似為與溫度成[正比](../Page/正比.md "wikilink")。為了要比較物質在不同溫度狀況的電導率，必須設定一個共同的參考溫度。電導率與溫度的相關性，時常可以表達為，電導率對上溫度線圖的[斜率](../Page/斜率.md "wikilink")，用方程式寫為

\[\sigma(T) = {\sigma_0 \over 1 + \alpha (T - T_0)}\,\!\]；

其中，\(T_0\,\!\) 是參考溫度，\(T\,\!\) 是測量溫度， \(\sigma\,\!\)
是物質的電導率，\(\sigma_0\,\!\)
是物質在參考溫度的固定參考電導率（通常在室溫），\(\alpha\,\!\)
是物質的**溫度補償斜率**。

最常見的水的溫度補償斜率大約為 2 %/°C 。但是，它可以在 (1 to 3) %/°C
值域之間。這數值與[地球化學有關](../Page/地球化學.md "wikilink")，可以很容易地在實驗室裡測量出來。

在非常低溫的狀況（在[絕對零度附近](../Page/絕對零度.md "wikilink")），有些物質的電導率會變得非常高。稱這現象為[超導現象](../Page/超導現象.md "wikilink")。

## 參考文獻

## 參閱

  - [導電性](../Page/導電性.md "wikilink")：講述電導率的物理本質。
  - [電阻](../Page/電阻.md "wikilink")
  - [熱導率](../Page/熱導率.md "wikilink")
  - [經典與量子電導率](../Page/經典與量子電導率.md "wikilink") ()
  - [莫耳電導率](../Page/莫耳電導率.md "wikilink") () 講述在溶液裡因離子產生的電導率。
  - [傳輸現象](../Page/傳輸現象.md "wikilink") ()

[D](../Category/基本物理概念.md "wikilink") [D](../Category/電化學.md "wikilink")
[D](../Category/電現象.md "wikilink") [D](../Category/物理量.md "wikilink")
[D](../Category/電學.md "wikilink")

1.
2.  參閱 [J. Phys. Chem.
    B 2005, 109, 1231-1238](http://dx.doi.org/10.1021%2Fjp045975a) 特別是第
    1235 頁。請注意，這資料的數值單位是 S/cm，不是 S/m，兩者相差 100 倍。