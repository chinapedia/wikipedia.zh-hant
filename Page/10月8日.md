**10月8日**是[阳历一年中的第](../Page/阳历.md "wikilink")281天（[闰年第](../Page/闰年.md "wikilink")282天），离全年的结束还有84天。

## 大事记

### 14世紀

  - [1325年](../Page/1325年.md "wikilink")：[元泰定帝改革全国的行政区划](../Page/元泰定帝.md "wikilink")，将全国划分为18个道。

### 15世紀

  - [1480年](../Page/1480年.md "wikilink")：[金帐汗国](../Page/金帳汗國.md "wikilink")[阿黑麻汗率军与](../Page/阿黑麻汗.md "wikilink")[莫斯科大公](../Page/莫斯科大公.md "wikilink")[伊凡三世对峙于](../Page/伊凡三世.md "wikilink")[乌格拉河](../Page/乌格拉河.md "wikilink")，后不战而退，此役宣告莫斯科完全摆脱金帐汗国控制。

### 16世紀

  - [1600年](../Page/1600年.md "wikilink")：世界上现存的最古老的[共和立宪制国家](../Page/共和立宪制.md "wikilink")[圣马力诺正式通过其成文](../Page/圣马力诺.md "wikilink")[宪法](../Page/宪法.md "wikilink")，并沿用至今。

### 17世紀

  - [1643年](../Page/1643年.md "wikilink")：福临即[清朝皇帝位](../Page/清朝.md "wikilink")，是为[顺治帝](../Page/顺治帝.md "wikilink")。
  - [1683年](../Page/1683年.md "wikilink")：[鄭克塽剃髮降清](../Page/明鄭.md "wikilink")。

### 19世紀

  - [1813年](../Page/1813年.md "wikilink")：[天理教徒](../Page/天理教.md "wikilink")[林清在](../Page/林清.md "wikilink")[北京举事](../Page/北京市.md "wikilink")，率众攻入[皇宫](../Page/紫禁城.md "wikilink")。
  - [1856年](../Page/1856年.md "wikilink")：[第二次鸦片战争](../Page/第二次鸦片战争.md "wikilink")：[亚罗号事件发生](../Page/亞羅號.md "wikilink")，标志着战争的开始。
  - [1879年](../Page/1879年.md "wikilink")：[太平洋战争](../Page/硝石戰爭.md "wikilink")：[智利海军在](../Page/智利.md "wikilink")[安戈莫斯海战中决定性地击败](../Page/安戈莫斯海战.md "wikilink")[秘鲁海军](../Page/秘鲁.md "wikilink")，秘鲁海军司令阵亡。
  - [1895年](../Page/1895年.md "wikilink")：[朝鲜](../Page/朝鲜国.md "wikilink")[高宗的](../Page/朝鮮高宗.md "wikilink")[王妃](../Page/王妃.md "wikilink")[闵妃在](../Page/明成皇后.md "wikilink")[景福宫被](../Page/景福宮_\(朝鮮\).md "wikilink")[日本公使](../Page/日本.md "wikilink")[三浦梧楼领兵刺杀身亡](../Page/三浦梧楼.md "wikilink")。
  - [1897年](../Page/1897年.md "wikilink")：[奥地利](../Page/奥地利.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")[古斯塔夫·马勒被任命成为](../Page/古斯塔夫·马勒.md "wikilink")[维也纳国家歌剧院总监](../Page/维也纳国家歌剧院.md "wikilink")。

### 20世紀

  - [1906年](../Page/1906年.md "wikilink")：[卷发革命从](../Page/卷发革命.md "wikilink")[英国发起](../Page/英国.md "wikilink")。
  - [1912年](../Page/1912年.md "wikilink")：[黑山对](../Page/黑山.md "wikilink")[奥斯曼帝国宣战](../Page/奥斯曼帝国.md "wikilink")，[第一次巴尔干战争开始](../Page/第一次巴尔干战争.md "wikilink")。
  - [1928年](../Page/1928年.md "wikilink")：[蒋介石任](../Page/蒋介石.md "wikilink")[南京國民政府主席](../Page/南京國民政府.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[中华人民共和国发行第一套邮票](../Page/中华人民共和国.md "wikilink")《庆祝[中国人民政治协商会议第一届全体会议](../Page/中国人民政治协商会议.md "wikilink")》发行，为纪念邮票，全套4枚，图案为北京[天安门和](../Page/天安门.md "wikilink")[宫灯](../Page/宫灯.md "wikilink")，由上海[商务印书馆印制](../Page/商务印书馆.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[中国人民志愿军组成](../Page/中国人民志愿军.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[英国](../Page/英国.md "wikilink")[伦敦三列](../Page/伦敦.md "wikilink")[火車相撞](../Page/鐵路列車.md "wikilink")。
  - [1954年](../Page/1954年.md "wikilink")：[法國軍隊在](../Page/法國軍隊.md "wikilink")[越南抗法戰爭中戰敗](../Page/越南.md "wikilink")，被迫撤出[河內](../Page/河內.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[钱学森经](../Page/钱学森.md "wikilink")[香港返回中国](../Page/香港.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：中国第一个[导弹](../Page/导弹.md "wikilink")[火箭研究机构](../Page/火箭.md "wikilink")：[国防部第五研究院成立](../Page/国防部第五研究院.md "wikilink")，首任院長[钱学森](../Page/钱学森.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：中国第一个天然石油基地：[玉门油田基本建成](../Page/玉门油田.md "wikilink")，成为一座拥有地质勘探、钻井、采油、炼油、机械修配、油田建设和石油科学研究等部门的大型石油联合企业。
  - [1959年](../Page/1959年.md "wikilink")：[中央档案馆在](../Page/中央档案馆.md "wikilink")[北京西郊落成](../Page/北京.md "wikilink")，并正式开馆。
  - [1967年](../Page/1967年.md "wikilink")：[拉丁美洲](../Page/拉丁美洲.md "wikilink")[马克思主义革命者](../Page/马克思主义.md "wikilink")[切·格瓦拉在](../Page/切·格瓦拉.md "wikilink")[玻利维亚被政府军俘获](../Page/玻利维亚.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：“[四人帮](../Page/四人帮.md "wikilink")”余党被粉碎。
  - [1985年](../Page/1985年.md "wikilink")：[中华人民共和国第一座太阳能光电站建成](../Page/中华人民共和国.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：由[法国作家](../Page/法国.md "wikilink")[维克多·雨果的](../Page/维克多·雨果.md "wikilink")[同名小说改编的](../Page/悲惨世界.md "wikilink")[音乐剧](../Page/音乐剧.md "wikilink")《[悲惨世界](../Page/悲慘世界_\(音樂劇\).md "wikilink")》在[英国](../Page/英国.md "wikilink")[伦敦首演](../Page/伦敦.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[蒋经国表示](../Page/蒋经国.md "wikilink")[中華民國政府当局将提议解除实施三十七年的](../Page/中華民國政府.md "wikilink")[臺灣省戒嚴令](../Page/臺灣省戒嚴令.md "wikilink")。
  - 1986年：[香港](../Page/香港.md "wikilink")[葵涌一間皮革廠發生大爆炸](../Page/葵涌.md "wikilink")，13人傷重不治。
  - [1990年](../Page/1990年.md "wikilink")：[中国大陸第一家](../Page/中国.md "wikilink")[麦当劳餐厅在](../Page/麦当劳.md "wikilink")[深圳市开业](../Page/深圳市.md "wikilink")。
  - 1990年：千年前[应州大地震遗址被发现](../Page/应州.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[武汉航空一架旅游包机在中國](../Page/武汉航空.md "wikilink")[甘肃省坠毁](../Page/甘肃省.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[哈勃太空望远镜发现迄今最遥远的星系](../Page/哈勃太空望远镜.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：時任[日本首相](../Page/日本首相.md "wikilink")[小泉纯一郎訪問中國](../Page/小泉纯一郎.md "wikilink")。
  - 2001年：[北欧航空](../Page/北欧航空.md "wikilink")[686号班机在](../Page/北欧航空686号班机事故.md "wikilink")[意大利](../Page/意大利.md "wikilink")[米兰的](../Page/米兰.md "wikilink")[连尼治机场与另一架商务客机相撞](../Page/连尼治机场.md "wikilink")，造成共118人死亡。
  - [2005年](../Page/2005年.md "wikilink")：[巴基斯坦控制的](../Page/巴基斯坦.md "wikilink")[克什米尔地区发生](../Page/克什米尔.md "wikilink")[里氏](../Page/里氏.md "wikilink")7.6级的[强烈地震](../Page/2005年克什米尔大地震.md "wikilink")，造成至少7.4万人死亡。
  - [2010年](../Page/2010年.md "wikilink")：中國異議人士、《[零八憲章](../Page/零八憲章.md "wikilink")》起草人[劉曉波获頒](../Page/劉曉波.md "wikilink")[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。劉曉波是中國繼[達賴喇嘛後的第二位諾貝爾和平獎得主](../Page/達賴喇嘛.md "wikilink")，亦是第一位在中華人民共和國出生的[诺贝尔奖得主](../Page/诺贝尔奖.md "wikilink")。

## 出生

  - [1848年](../Page/1848年.md "wikilink")：[皮埃尔·狄盖特](../Page/皮埃尔·狄盖特.md "wikilink")，《[国际歌](../Page/国际歌.md "wikilink")》作曲者（[1932年去世](../Page/1932年.md "wikilink")）
  - [1864年](../Page/1864年.md "wikilink")：[池田菊苗](../Page/池田菊苗.md "wikilink")，[日本化學家](../Page/日本.md "wikilink")（[1936年去世](../Page/1936年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[宮澤喜一](../Page/宮澤喜一.md "wikilink")，日本第78任[首相](../Page/日本首相.md "wikilink")（[2007年去世](../Page/2007年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[關鶴岩](../Page/關鶴岩.md "wikilink")，[中國](../Page/中國.md "wikilink")[音樂家](../Page/音樂家.md "wikilink")，著名[兒歌](../Page/兒歌.md "wikilink")《丟手絹》的作者（[2005年去世](../Page/2005年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[鄒文懷](../Page/鄒文懷.md "wikilink")，[香港電影事業家](../Page/香港.md "wikilink")、製片人，[嘉禾集團創辦人](../Page/橙天嘉禾.md "wikilink")、前主席兼執行董事（[2018年去世](../Page/2018年.md "wikilink")）
  - [1946年](../Page/1946年.md "wikilink")：[阿什拉維](../Page/阿什拉維.md "wikilink")，[巴勒斯坦女政治家](../Page/巴勒斯坦.md "wikilink")
  - [1948年](../Page/1948年.md "wikilink")：[克勞德·傑德](../Page/克勞德·傑德.md "wikilink")，[法国](../Page/法国.md "wikilink")[女演員](../Page/演員.md "wikilink")（[2006年去世](../Page/2006年.md "wikilink")）
  - [1949年](../Page/1949年.md "wikilink")：[-{zh-cn:西格妮·韦弗; zh-hk:薛歌妮·韋花;
    zh-tw:雪歌妮·薇佛}-](../Page/雪歌妮·薇佛.md "wikilink")，[美國女](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[賈韋德·謝赫](../Page/賈韋德·謝赫.md "wikilink")，[巴基斯坦男演員](../Page/巴基斯坦.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[光宗信吉](../Page/光宗信吉.md "wikilink")，[日本](../Page/日本.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[波班](../Page/兹沃尼米尔·博班.md "wikilink")，[克羅地亞退役足球運動員](../Page/克羅地亞.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[野村哲也](../Page/野村哲也.md "wikilink")，日本遊戲設計師、[人物設計師](../Page/人物設計.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink") :
    [马特·戴蒙](../Page/马特·戴蒙.md "wikilink")
    ，[美國](../Page/美國.md "wikilink")[男演員](../Page/演員.md "wikilink")，[編劇](../Page/編劇.md "wikilink")，[製片人](../Page/製片人.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[室伏廣治](../Page/室伏广治.md "wikilink")，[日本鏈球運動員](../Page/日本.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[載寧龍二](../Page/載寧龍二.md "wikilink")，日本男性演員
  - [1985年](../Page/1985年.md "wikilink")：[火星人布魯諾](../Page/火星人布魯諾.md "wikilink")，美國唱作男歌手，唱片監製

## 逝世

  - [1735年](../Page/1735年.md "wikilink")：[清世宗](../Page/清世宗.md "wikilink")[愛新覺羅胤禛](../Page/雍正帝.md "wikilink")，[清朝雍正皇帝](../Page/清朝.md "wikilink")（[1678年出生](../Page/1678年.md "wikilink")）
  - [1754年](../Page/1754年.md "wikilink")：[亨利·菲尔丁](../Page/亨利·菲尔丁.md "wikilink")，[英国](../Page/英国.md "wikilink")[小說家](../Page/小说家.md "wikilink")、[劇作家](../Page/剧作家.md "wikilink")（[1707年出生](../Page/1707年.md "wikilink")）
  - [1793年](../Page/1793年.md "wikilink")：[约翰·汉考克](../Page/约翰·汉考克.md "wikilink")，[美國革命家](../Page/美國.md "wikilink")、[政治家](../Page/政治家.md "wikilink")（[1737年出生](../Page/1737年.md "wikilink")）
  - [1826年](../Page/1826年.md "wikilink")：[佛里德里希·克虏伯](../Page/佛里德里希·克虏伯.md "wikilink")，[德国工业家](../Page/德国.md "wikilink")，[蒂森克虏伯前身克虏伯钢铁厂的奠基人](../Page/蒂森克虏伯股份公司.md "wikilink")（[1787年出生](../Page/1787年.md "wikilink")）
  - [1869年](../Page/1869年.md "wikilink")：[富蘭克林·皮爾斯](../Page/富蘭克林·皮爾斯.md "wikilink")，美國第14任總統。（[1804年出生](../Page/1804年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[艾德禮](../Page/克莱门特·艾德礼.md "wikilink")，前[英国首相](../Page/英国首相.md "wikilink")（[1883年出生](../Page/1883年.md "wikilink")）
  - [1992年](../Page/1992年.md "wikilink")：[维利·勃兰特](../Page/维利·勃兰特.md "wikilink")，[德国前总理](../Page/德国.md "wikilink")（[1913年出生](../Page/1913年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[顾城](../Page/顾城.md "wikilink")，[中國作家](../Page/中國.md "wikilink")、[诗人](../Page/诗人.md "wikilink")，杀妻後自尽（[1956年出生](../Page/1956年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[鈴木沙彩](../Page/鈴木沙彩.md "wikilink")，[日本女童星](../Page/日本.md "wikilink")（[1995年出生](../Page/1995年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[郭金發](../Page/郭金發.md "wikilink")，[臺灣男歌手](../Page/臺灣.md "wikilink")（[1944年出生](../Page/1944年.md "wikilink")）
  - [2018年](../Page/2018年.md "wikilink")：[輪島大士](../Page/輪島大士.md "wikilink")：前橫綱

## 节假日和习俗

  - [中国](../Page/中国.md "wikilink")：全国高血压日
  - [克罗地亚](../Page/克罗地亚.md "wikilink")：[独立日](../Page/独立日.md "wikilink")
  - [秘鲁](../Page/秘鲁.md "wikilink")：海军日