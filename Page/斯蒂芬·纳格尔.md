**斯蒂芬·雷·纳格尔**（，）[美国空军](../Page/美国空军.md "wikilink")[上校](../Page/上校.md "wikilink")[飞行员](../Page/飞行员.md "wikilink")，曾是一位[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")、[航空工程师](../Page/航空航天工程.md "wikilink")，执行过[STS-51-G](../Page/STS-51-G.md "wikilink")、[STS-61-A](../Page/STS-61-A.md "wikilink")、[STS-37以及](../Page/STS-37.md "wikilink")[STS-55任务](../Page/STS-55.md "wikilink")。\[1\]

纳格尔1946年10月27日出生于[伊利诺州坎顿](../Page/伊利诺州.md "wikilink")（Canton），他的爱好包括飞行运动、[业余无线电和](../Page/业余无线电.md "wikilink")[音乐](../Page/音乐.md "wikilink")。妻子Ivan
R.
Nagel，有两个女儿。1964年毕业于坎顿高中，1969年在[伊利诺伊大学获得航空和宇航工程](../Page/伊利诺伊大学香槟分校.md "wikilink")[理学学士学位](../Page/理学学士.md "wikilink")。1978年在加州州立大学弗雷斯诺分校获得机械工程[理学硕士学位](../Page/理学硕士.md "wikilink")。\[2\]

1969年在伊利诺斯大学通过[空军预备役军官训练军团](../Page/空军预备役军官训练军团.md "wikilink")（AFROTC）计划，1970年2月在[德克萨斯州](../Page/德克萨斯州.md "wikilink")[拉雷多空军基地完成了本科飞行员培训](../Page/拉雷多空军基地.md "wikilink")，随后在[亚利桑那州](../Page/亚利桑那州.md "wikilink")[卢克空军基地参加](../Page/卢克空军基地.md "wikilink")[F-100超級軍刀戰鬥機飞行培训](../Page/F-100超級軍刀戰鬥機.md "wikilink")。1970年10月到1971年7月成为[路易斯安那州](../Page/路易斯安那州.md "wikilink")[英格兰空军基地](../Page/英格兰空军基地.md "wikilink")[第68战术战斗机中队F](../Page/第68战术战斗机中队.md "wikilink")-100战斗机飞行员，后作为[T-28特洛伊教练机教练前往](../Page/T-28特洛伊.md "wikilink")[泰国](../Page/泰国.md "wikilink")[乌隆府](../Page/乌隆府.md "wikilink")[乌冬国际机场培训](../Page/乌隆国际机场.md "wikilink")[老挝空军一年](../Page/老挝空军.md "wikilink")，1972年10月回到美国之前是[A-7海盗二式攻击机教练飞行员和飞行考官](../Page/A-7海盗二式攻击机.md "wikilink")。1975年2月到1975年12月进入加州[爱德华兹空军基地的](../Page/爱德华兹空军基地.md "wikilink")[美国空军试飞员学校](../Page/美国空军试飞员学校.md "wikilink")，1976年1月被分配到位于爱德华兹的[第6512测试中队](../Page/第6512测试中队.md "wikilink")。作为一名试飞员，他从事各种试飞项目，包括[F-4鬼怪II战斗机和](../Page/F-4鬼怪II战斗机.md "wikilink")[A-7海盗二式攻击机的飞行](../Page/A-7海盗二式攻击机.md "wikilink")。

他的飞行时间记录为9400小时，喷气式飞机为6650小时。

1979年8月纳格尔成为了美国国家航空航天局宇航员，1985年6月17日作为STS-51G任务专家首次从[佛罗里达](../Page/佛罗里达.md "wikilink")[肯尼迪航天中心乘坐](../Page/肯尼迪航天中心.md "wikilink")[发现号航天飞机进入太空](../Page/发现号航天飞机.md "wikilink")，完成约170小时的太空飞行后，“发现号”在1985年6月24日降落[加州](../Page/加州.md "wikilink")[爱德华兹空军基地](../Page/爱德华兹空军基地.md "wikilink")。同年10月30日再次作为STS-61A任务宇航员，参加了西德[D-1太空实验室的任务](../Page/D-1太空实验室.md "wikilink")。

1991年4月5日作为STS-37任务指挥官，乘坐[亚特兰蒂斯号航天飞机第三次进入太空](../Page/亚特兰蒂斯号航天飞机.md "wikilink")。

1993年4月26日作为STS-55任务指挥官，乘坐[哥伦比亚号航天飞机进行德国D](../Page/哥伦比亚号航天飞机.md "wikilink")-2太空实验室的任务，进行了材料加工、生命科学、机器人技术、天文学、地球的映射等89个项目的学科实验。

纳格尔第四次飞行完成后，在太空中的记录共723小时。

纳格尔1995年2月28日从空军退役，在德克萨斯州休斯顿[约翰逊航天中心担任业务发展](../Page/约翰逊航天中心.md "wikilink")、安全、可靠和质量保证办公室副主任。1996年9月转到飞机业务部门担任研究飞行员。

2011年秋，纳格尔夫妇搬到密苏里州哥伦比亚，在[密苏里大学担任工程学院教师和机械和航空航天工程部教练](../Page/密苏里大学.md "wikilink")。

2014年8月21日纳格尔因[黑色素瘤在哥伦比亚去世](../Page/黑色素瘤.md "wikilink")。\[3\]

## 参考

## 外部链接

  - [美国国家航空航天局网站的纳格尔介绍](http://www.jsc.nasa.gov/Bios/htmlbios/nagel.html)

  - [美国国家航空航天局口述历史项目，Steven R.
    Nagel](http://www.jsc.nasa.gov/history/oral_histories/NagelSR/NagelSR_Bio.pdf)

[N](../Category/美國空軍上校.md "wikilink")
[N](../Category/第八组宇航员.md "wikilink")
[N](../Category/伊利諾大學厄巴納－香檳分校校友.md "wikilink")

1.

2.
3.