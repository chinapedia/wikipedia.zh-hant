**顏福偉**（，），[香港出生](../Page/香港.md "wikilink")，祖籍[福建](../Page/福建.md "wikilink")[海澄](../Page/海澄.md "wikilink")\[1\]，閩南人，[白花油藥廠創辦人](../Page/白花油.md "wikilink")[顏玉瑩之子](../Page/顏玉瑩.md "wikilink")，[白花油國際有限公司執行董事兼](../Page/白花油國際有限公司.md "wikilink")[行政總裁](../Page/首席执行官.md "wikilink")。顏福偉也是一位[歌手](../Page/歌手.md "wikilink")，外號有「[白花油王子](../Page/白花油.md "wikilink")」和「福仔」。是香港望族[顏玉瑩家族的後人](../Page/顏玉瑩家族.md "wikilink")。

## 生平

顏福偉家中排行第五。他曾在玫瑰崗幼稚園、[玫瑰崗學校](../Page/玫瑰崗學校.md "wikilink")（小學部）、[番禺會所華仁小學和聖瑪加利書院讀書](../Page/番禺會所華仁小學.md "wikilink")。1978年他負笈英國，完成預科課程後於1981年至1984年在[英國](../Page/英國.md "wikilink")[羅浮堡大學主修](../Page/羅浮堡大學.md "wikilink")[食物工程學](../Page/食物工程.md "wikilink")。回港後曾任職[大昌行](../Page/大昌行.md "wikilink")，一年半後（1986年）參與管理家族生意，現為[白花油國際有限公司執行董事兼行政總裁](../Page/白花油國際有限公司.md "wikilink")（公司主席顏為善為顏福偉之[侄兒](../Page/侄兒.md "wikilink")）。他曾任[香港中醫藥管理委員會](../Page/香港中醫藥管理委員會.md "wikilink")（中醫組）委員（1999年至2005年）。

顏福偉篤信[佛教](../Page/佛教.md "wikilink")，愛好[音樂](../Page/音樂.md "wikilink")，積極參與表演和作曲，曾多次自資發行[唱片](../Page/唱片.md "wikilink")。他在1986年組成「川鳴樂隊」，主打歌《午夜場》，1987年推出第一張唱片《願妳知》，但未受注視。1989年[北京學運期間](../Page/六四事件.md "wikilink")，顏福偉曾在《[民主歌聲獻中華](../Page/民主歌聲獻中華.md "wikilink")》中獻唱。\[2\]

2004年起，白花油的[電視廣告大多由他親自演出](../Page/電視廣告.md "wikilink")，廣告歌曲也由他主唱，自此被稱為「白花油王子」，歌曲漸受注視。2006年，廣告歌曲《夢想有你》在多個香港的網上討論區流行，但在各頒獎禮沒有得獎。他亦曾參與白花油產品的廣告演出，包括公司旗下的[活絡油及](../Page/活絡油.md "wikilink")[藥油](../Page/藥油.md "wikilink")。

顏福偉曾在[2002年世界盃足球賽決賽親身到](../Page/2002年世界盃足球賽.md "wikilink")[日本](../Page/日本.md "wikilink")[橫濱觀戰](../Page/橫濱.md "wikilink")，並模仿[巴西球星](../Page/巴西.md "wikilink")[朗拿度的](../Page/朗拿度.md "wikilink")[河童頭造型](../Page/河童.md "wikilink")。[2006年世界盃期間](../Page/2006年世界盃.md "wikilink")，他分別在[有線電視及](../Page/有線電視.md "wikilink")[無綫電視的該年世界盃節目中亮相](../Page/無綫電視.md "wikilink")，擔任嘉賓。2007年，憑廣告歌《愛多八十年》首次打入[勁歌金曲的勁歌金榜](../Page/勁歌金曲.md "wikilink")。該廣告片段曾在[無綫電視翡翠台](../Page/無綫電視翡翠台.md "wikilink")、[亞洲電視本港台](../Page/亞洲電視本港台.md "wikilink")、[有線電視等電視台播放](../Page/有線電視.md "wikilink")，而不少論壇都有關於該廣告的討論。

2007年10月9日晚，顏福偉在[葵青劇院舉行一場](../Page/葵青劇院.md "wikilink")「有福同享顏福偉愛多80年講唱會」，當晚全場三百個座位爆滿，觀眾反應熱烈，令他樂不可支，全晚表現風騷。顏更在音樂會上，公開剖白自己的性取向，說：「」（總之男士或女士都沒有所謂，有人疼愛最要紧。）音樂會結束後，在銅鑼灣乘的士返家途中，涉嫌借醉多番撫摸男性的士司機下體，被司機趕下車後留下五百元。肇事司機不甘受辱報警，顏福偉被警方拘捕。同年10月31日，事件被《[頭條日報](../Page/頭條日報.md "wikilink")》及《[星島日報](../Page/星島日報.md "wikilink")》圖文並茂地大肆報道，他曾在9月2日凌晨時分被該報記者以攝影機拍得在銅鑼灣一輛的士上借醉撫摸男司機。

非禮案在2008年4月8日開審，顏福偉於4月9日在法庭上再度公開自己的[同性戀](../Page/同性戀.md "wikilink")[性取向](../Page/性取向.md "wikilink")，並辯稱他用手背觸其[大腿及](../Page/大腿.md "wikilink")[下體作試探](../Page/生殖器.md "wikilink")，司機沒有避開，故認為這示意可作再進一步。同年4月18日，[東區裁判法院判決](../Page/東區裁判法院.md "wikilink")，2003年和2007年兩項[非禮男性](../Page/非禮.md "wikilink")[的士司機罪名成立](../Page/的士司機.md "wikilink")。\[3\]4月20日，顏福偉在銅鑼灣辦事處召開記者會向公眾道歉。\[4\]5月2日，顏福偉被判每項罪名180小時[社會服務令](../Page/社會服務令.md "wikilink")，同期執行。12月22日，高等法院撤銷顏的2003年非禮罪，但維持2007年控罪的原判決。\[5\]

2010年及之後，顏福偉以個人名義支持各慈善機構，包括保護遺棄動物協會及[聯合國難民署](../Page/聯合國難民署.md "wikilink")；2012年建立慈善網頁「Songucare」、[Facebook專頁及](../Page/Facebook.md "wikilink")[YouTube頻道為聯合國難民署及](../Page/YouTube.md "wikilink")[世界宣明會呼籲](../Page/世界宣明會.md "wikilink")。2013年，他創作和平歌曲《活著是什麼？》，同期建立慈善網頁「活著是什麼？」，為戰爭中[難民](../Page/難民.md "wikilink")[籌款](../Page/籌款.md "wikilink")，受惠機構為聯合國難民署；2014年曾被[香港蘋果日報拍攝到跟男伴於](../Page/香港蘋果日報.md "wikilink")[石澳游泳](../Page/石澳.md "wikilink")。

2017年11月，顏福偉接受訪問時表示自己是旅行快閃黨，喜歡短程旅行，別人遊歐洲最少花上十天，他卻經常自製四天歐遊行程，最近他去了羅馬尼亞，並表示因為東歐消費比西歐便宜，而且治安很好，很多人覺得東歐多賊，其實要做賊的都去了西歐搵食\[6\]。

## 唱片

  - 1987年《願妳知》
  - 1989年《昨日的愛》
  - 1994年《遠觀自在》
  - 1999年《明日話今天》

<!-- end list -->

  - 2004年《奔向明天》
  - 2006年《夢想有你》
  - 2007年《愛多八十年》
  - 2010年《音樂再循環》

<!-- end list -->

  - 2011年《音樂再循環2》
  - 2013年《音樂再循環3》

## 電影

  - 2008年：《[愛‧鬥大](../Page/愛‧鬥大.md "wikilink")》 飾演 Tommy父親/顏福偉

## 參見

  - [顏玉瑩家族](../Page/顏玉瑩家族.md "wikilink")
  - [顏為善](../Page/顏為善.md "wikilink")
  - [顏清輝](../Page/顏清輝.md "wikilink")

## 参考文献

## 外部連結

  -
  - [呼籲為聯合國難民署籌款的網頁](https://web.archive.org/web/20170913105128/http://9930.hk/)

  - [顏福偉
    生平簡介](http://www.hkmemory.hk/collections/oral_history/All_Items_OH/oha_17/bio/index_cht.html)

[Y](../Page/category:羅浮堡大學校友.md "wikilink")
[category:玫瑰崗學校校友](../Page/category:玫瑰崗學校校友.md "wikilink")

[Category:福建人](../Category/福建人.md "wikilink")
[顏](../Category/香港商人.md "wikilink")
[顏](../Category/香港男歌手.md "wikilink")
[顏](../Category/香港電影演員.md "wikilink")
[Category:香港雙性戀者](../Category/香港雙性戀者.md "wikilink")
[Y](../Category/龙海人.md "wikilink") [F](../Category/顏姓.md "wikilink")

1.  [顏福偉細佬力撐阿哥唔係基](http://realblog.zkiz.com/greatsoup38/24631)
2.  [YouTube: 【經典回憶】 1989年 - 民主歌聲獻中華
    (顏福偉獻唱)](http://www.youtube.com/watch?v=YqXS4CY-hbQ)
3.  [明報：顏福偉兩項非禮罪成](http://hk.news.yahoo.com/080418/12/2smkk.html)
4.  }}
5.  [《高院撤銷顏福偉一項非禮罪　另一非禮罪維持原判》](http://www.rthk.org.hk/rthk/news/expressnews/20081222/news_20081222_55_547085.htm)，載於www.rthk.org.hk，2008年12月22日。
6.  [【獨家專訪】顏福偉．羅馬尼亞快閃遊](http://bka.mpweekly.com/focus/local/%E5%90%8D%E4%BA%BA%E5%B0%88%E7%B7%9A/secret-address/20171125-91926)