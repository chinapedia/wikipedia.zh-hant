**玉竹**（[学名](../Page/学名.md "wikilink")：**），[假叶树科](../Page/假叶树科.md "wikilink")[黄精属植物](../Page/黄精属.md "wikilink")，又名**葳蕤**、**女萎**、**节地**、**玉术**、**竹节黄**、**竹七根**、**山包米**、**尾参**、**西竹**、**连竹**、**地管子**、**铃铛菜**。

## 形态

多年生[草本](../Page/草本.md "wikilink")，地下具有竹鞭状肉质根状茎；椭圆形叶子互生，带革质；初夏开绿白色钟状花，花腋生，花柄常作两分叉，顶端各生一花，下垂；暗蓝色球形[浆果](../Page/浆果.md "wikilink")，结果一般在8－9月份。

## 分布

野生的玉竹一般生长在山坡草丛或林间的阴湿处，[中国大部分省分都有分布](../Page/中国.md "wikilink")，例如：[江蘇](../Page/江蘇.md "wikilink")、[遼寧](../Page/遼寧.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[浙江](../Page/浙江.md "wikilink")，[日本也有分布](../Page/日本.md "wikilink")。

## 药用

中医上以根茎入药，性平、味甘，是滋补强壮之药，可以养阴润燥，生津止渴，主治热病伤阴、虚热燥咳、消渴等症。

在化學層面，玉竹裡含有鈴蘭苷﹑黏液質等成份，能夠幫助消除胰島素抵抗，修復胰島細胞，增加胰島素的敏感性，進而調節血糖水平。\[1\]

## 外部連結

  -
  - [玉竹
    Yuzhu](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00382)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [玉竹
    Yuzhu](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00122)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [玉竹 Yu
    Zhu](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00597)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

## 参考文献

[Category:黄精属](../Category/黄精属.md "wikilink")
[Category:1758年描述的植物](../Category/1758年描述的植物.md "wikilink")

1.