[Miao_Quansun.jpg](https://zh.wikipedia.org/wiki/File:Miao_Quansun.jpg "fig:Miao_Quansun.jpg")
**缪荃孙**（），[中国近代](../Page/中国.md "wikilink")[教育家](../Page/教育.md "wikilink")、[目录学家](../Page/目录学.md "wikilink")、[史学家](../Page/史学.md "wikilink")、[方志学家](../Page/方志学.md "wikilink")、[金石家](../Page/金石.md "wikilink")，中国近代[图书馆事业的奠基人](../Page/图书馆.md "wikilink")，中国近代教育事业的先驱者，[字](../Page/表字.md "wikilink")**炎之**，一字**筱珊**，晚[号](../Page/号.md "wikilink")**艺风**。[江苏](../Page/江苏.md "wikilink")[江阴申港镇缪家村人](../Page/江阴.md "wikilink")。

## 生平

[Miao_Quansun2.jpg](https://zh.wikipedia.org/wiki/File:Miao_Quansun2.jpg "fig:Miao_Quansun2.jpg")
道光二十四年（1844年）9月20日生，出身於官宦之家，幼承家学，11岁修毕[五经](../Page/五经.md "wikilink")。17岁时[太平军进](../Page/太平军.md "wikilink")[江阴](../Page/江阴.md "wikilink")，侍继母避兵[淮安](../Page/淮安.md "wikilink")，[丽正书院肄业](../Page/丽正书院.md "wikilink")，习[文字学](../Page/文字学.md "wikilink")、[训诂学和](../Page/训诂学.md "wikilink")[音韵学](../Page/音韵学.md "wikilink")。21岁举家迁居[成都](../Page/成都.md "wikilink")，习文史，考订文字。24岁应[四川](../Page/四川.md "wikilink")[乡试中](../Page/乡试.md "wikilink")[举人](../Page/举人.md "wikilink")。光緒二年（1876年）33岁时[殿试中](../Page/殿试.md "wikilink")[进士](../Page/进士.md "wikilink")，选[庶吉士](../Page/庶吉士.md "wikilink")，[散馆授](../Page/散馆.md "wikilink")[翰林院](../Page/翰林院.md "wikilink")[编修](../Page/编修.md "wikilink")。因與翰林院掌院[徐桐不合](../Page/徐桐.md "wikilink")，後來辭職。此后事编撰[校勘十余年](../Page/校勘.md "wikilink")。

1888年任[南菁书院山长](../Page/南菁书院.md "wikilink")。1891年掌[泺源书院](../Page/泺源书院.md "wikilink")。1894年任[南京](../Page/南京.md "wikilink")[钟山书院山长](../Page/钟山书院.md "wikilink")，兼掌[常州](../Page/常州.md "wikilink")[龙城书院](../Page/龙城书院.md "wikilink")。1901年任[江楚编译局总纂](../Page/江楚编译局.md "wikilink")。1902年，钟山书院改为[江南高等学堂](../Page/江南高等学堂.md "wikilink")，任学堂监督。[癸卯学制实施后](../Page/癸卯学制.md "wikilink")，废古江宁府学，[两江总督府拟在江宁](../Page/两江总督.md "wikilink")“先办一大师范学堂，以为学务全局之纲领”，1902年5月出任学堂总稽查，负责筹建[三江师范学堂](../Page/三江师范学堂.md "wikilink")，并与[徐乃昌](../Page/徐乃昌.md "wikilink")、[柳詒-{徵}-等七教席赴东洋考察学务](../Page/柳詒徵.md "wikilink")，学堂遂仿[日本](../Page/日本.md "wikilink")[东京大学](../Page/东京大学.md "wikilink")，在[南京国子监旧址筑校](../Page/南京国子监.md "wikilink")，以后更名两江师范。

1906年得悉藏书家[丁丙去世](../Page/丁氏兄弟.md "wikilink")，他的“八千卷楼”藏书，被卖给日本的“[静嘉堂文库](../Page/静嘉堂文库.md "wikilink")”，缪荃荪以七万多元将书籍買下，运到南京。1907年受聘筹建[江南图书馆](../Page/江南图书馆.md "wikilink")（今[南京图书馆](../Page/南京图书馆.md "wikilink")），出任总办。1909年受聘创办[北京](../Page/北京.md "wikilink")[京师图书馆](../Page/京师图书馆.md "wikilink")（今[中国国家图书馆](../Page/中国国家图书馆.md "wikilink")），任正监督。1914年任[清史总纂](../Page/清史.md "wikilink")。

1919年12月22日在[上海逝世](../Page/上海.md "wikilink")。著有《云自在龛随笔》四卷等。

## 著作

一生历16省，著书200卷。编[校](../Page/校對.md "wikilink")、刻书甚多。

艺风堂系列

  - 《艺风堂文集》
  - 《艺风堂藏书记》
  - 《艺风堂读书记》
  - 《艺风堂续记》

纂辑

  - 《[书目答问](../Page/书目答问.md "wikilink")》（代[张之洞编纂](../Page/张之洞.md "wikilink")，后由[范希曾补](../Page/范希曾.md "wikilink")《书目答问补正》）
  - 《续修四库全书提要》
  - 《清学部图书馆善本书目》
  - 《清学部图书馆方志目》
  - 《续国朝碑传集》
  - 《[碑传集补遗](../Page/碑传集.md "wikilink")》
  - 《艺风堂金石目》
  - 《常州词录》
  - 《南北朝名臣年表》
  - 《近代文学大纲》

方志

  - 《顺天府志》
  - 《湖北通志》
  - 《江苏通志》
  - 《江阴县续志》

编刊

  - 《云自在龛丛书》
  - 《藕香零拾》
  - 《烟画东堂小品》
  - 《对雨楼丛书》

代撰

  - 《聚学轩丛书》（[刘世珩](../Page/刘世珩.md "wikilink")）
  - 《常州往哲遗书》（[盛宣怀](../Page/盛宣怀.md "wikilink")）
  - 《善本书室藏书志》
  - 《适园藏书记》
  - 《积学斋藏书记》
  - 《嘉业堂藏书志》

## 参考文献

[Category:同治六年丁卯科四川鄉試舉人](../Category/同治六年丁卯科四川鄉試舉人.md "wikilink")
[Category:中国教育家](../Category/中国教育家.md "wikilink")
[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝历史学家](../Category/清朝历史学家.md "wikilink")
[Category:图书馆学家](../Category/图书馆学家.md "wikilink")
[Category:中華民國圖書館學家](../Category/中華民國圖書館學家.md "wikilink")
[Q](../Category/江陰申浦繆氏.md "wikilink") [Q](../Category/繆姓.md "wikilink")