[朝阳区](../Page/朝阳区_\(长春市\).md "wikilink"){{.w}}[二道区](../Page/二道区.md "wikilink"){{.w}}[绿园区](../Page/绿园区.md "wikilink"){{.w}}[双阳区](../Page/双阳区.md "wikilink"){{.w}}[九台区](../Page/九台区.md "wikilink"){{.w}}[榆树市](../Page/榆树市.md "wikilink"){{.w}}[德惠市](../Page/德惠市.md "wikilink"){{.w}}[农安县](../Page/农安县.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[龙潭区](../Page/龙潭区_\(吉林市\).md "wikilink"){{.w}}[船营区](../Page/船营区.md "wikilink"){{.w}}[丰满区](../Page/丰满区.md "wikilink"){{.w}}[蛟河市](../Page/蛟河市.md "wikilink"){{.w}}[桦甸市](../Page/桦甸市.md "wikilink"){{.w}}[舒兰市](../Page/舒兰市.md "wikilink"){{.w}}[磐石市](../Page/磐石市.md "wikilink"){{.w}}[永吉县](../Page/永吉县.md "wikilink")

|group4 = [四平市](../Page/四平市.md "wikilink") |list4 =
[铁西区](../Page/铁西区_\(四平市\).md "wikilink"){{.w}}[铁东区](../Page/铁东区_\(四平市\).md "wikilink"){{.w}}[公主岭市](../Page/公主岭市.md "wikilink"){{.w}}[双辽市](../Page/双辽市.md "wikilink"){{.w}}[梨树县](../Page/梨树县.md "wikilink"){{.w}}[伊通满族自治县](../Page/伊通满族自治县.md "wikilink")

|group5 = [辽源市](../Page/辽源市.md "wikilink") |list5 =
[龙山区](../Page/龙山区_\(辽源市\).md "wikilink"){{.w}}[西安区](../Page/西安区_\(辽源市\).md "wikilink"){{.w}}[东丰县](../Page/东丰县.md "wikilink"){{.w}}[东辽县](../Page/东辽县.md "wikilink")

|group6 = [通化市](../Page/通化市.md "wikilink") |list6 =
[东昌区](../Page/东昌区_\(通化市\).md "wikilink"){{.w}}[二道江区](../Page/二道江区.md "wikilink"){{.w}}[梅河口市](../Page/梅河口市.md "wikilink"){{.w}}[集安市](../Page/集安市.md "wikilink"){{.w}}[通化县](../Page/通化县.md "wikilink"){{.w}}[辉南县](../Page/辉南县.md "wikilink"){{.w}}[柳河县](../Page/柳河县.md "wikilink")

|group7 = [白山市](../Page/白山市.md "wikilink") |list7 =
[浑江区](../Page/浑江区.md "wikilink"){{.w}}[江源区](../Page/江源区.md "wikilink"){{.w}}[临江市](../Page/临江市.md "wikilink"){{.w}}[抚松县](../Page/抚松县.md "wikilink"){{.w}}[靖宇县](../Page/靖宇县.md "wikilink"){{.w}}[长白朝鲜族自治县](../Page/长白朝鲜族自治县.md "wikilink")

|group8 = [松原市](../Page/松原市_\(吉林省\).md "wikilink") |list8 =
[宁江区](../Page/宁江区.md "wikilink"){{.w}}[扶余市](../Page/扶余市.md "wikilink"){{.w}}[长岭县](../Page/长岭县.md "wikilink"){{.w}}[乾安县](../Page/乾安县.md "wikilink"){{.w}}[前郭尔罗斯蒙古族自治县](../Page/前郭尔罗斯蒙古族自治县.md "wikilink")

|group9 = [白城市](../Page/白城市.md "wikilink") |list9 =
[洮北区](../Page/洮北区.md "wikilink"){{.w}}[洮南市](../Page/洮南市.md "wikilink"){{.w}}[大安市](../Page/大安市.md "wikilink"){{.w}}[镇赉县](../Page/镇赉县.md "wikilink"){{.w}}[通榆县](../Page/通榆县.md "wikilink")
}}

|group4style =text-align: center; |group4 =
[自治州](../Page/自治州.md "wikilink") |list4 =
[图们市](../Page/图们市.md "wikilink"){{.w}}[敦化市](../Page/敦化市.md "wikilink"){{.w}}[珲春市](../Page/珲春市.md "wikilink"){{.w}}[龙井市](../Page/龙井市.md "wikilink"){{.w}}[和龙市](../Page/和龙市.md "wikilink"){{.w}}[汪清县](../Page/汪清县.md "wikilink"){{.w}}[安图县](../Page/安图县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[吉林省乡级以上行政区列表](../Page/吉林省乡级以上行政区列表.md "wikilink")
}}<noinclude>

[fr:Modèle:Palette Jilin](../Page/fr:Modèle:Palette_Jilin.md "wikilink")
</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/吉林行政区划.md "wikilink")
[吉林行政区划模板](../Category/吉林行政区划模板.md "wikilink")