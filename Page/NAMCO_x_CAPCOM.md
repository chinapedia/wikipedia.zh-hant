**NAMCO x CAPCOM**（Namco Cross
Capcom）是由Capcom特許，Namco（現在的[南夢宮萬代](../Page/南夢宮萬代.md "wikilink")）製作並發售於2005年5月26日的PlayStation
2模擬策略角色扮演遊戲。

## 概要

本遊戲的人物超過200名，是Namco和Capcom的新舊受歡迎人物。Namco的有[命運傳奇](../Page/命運傳奇.md "wikilink")、[Xenosaga](../Page/Xenosaga.md "wikilink")
Episode I: Der Wille zur
Macht、[鐵拳系列等角色](../Page/铁拳系列.md "wikilink")。Capcom的有[街頭霸王系列](../Page/街頭霸王.md "wikilink")、[魔界村系列](../Page/魔界村.md "wikilink")、[恐龍危機系列等角色](../Page/恐龍危機.md "wikilink")。遊戲的廣告及印象曲是由flair主唱。

遊戲與帕佈雷斯特的[超級機器人大戰相似](../Page/超級機器人大戰.md "wikilink")。本作品的導演以前亦有參與超級機器人大戰系列的劇本

  - 戰鬥

單人或2人組成一組攻擊攻擊範圍內的敵人，選擇敵人後進入戰鬥畫面。戰鬥是以輸入指令方式，但不是像格鬥遊戲般複雜。輸入不同的方向及按鈕能使出不同的攻擊，敵人被打飛後能增加「COMBO」。

  - 技能

被分為指令技能和自動技能。指令技能會在戰鬥中消耗mp，就像超級機器人大戰中的精神指令一般。自動技能是要滿足特定的條件才會自動使用的技能

  - MA攻撃

複數攻擊的簡稱。像超級機器人大戰的MAP武器或合體攻擊，一個單位以消耗MP50攻擊複數的敵人（最多3名）。

  - BGM

自方的角色行動時，背景音樂會使用角色原本遊戲的背景音樂

## 登場角色

### 原創角色

  - 森罗（Shinra）

<!-- end list -->

  -
<!-- end list -->

  -
    MA攻擊「銃之型」的動作與燈光，被推測是參考電影「[-{zh-hk:未來殺人網絡; zh-tw:重裝任務;
    zh-cn:撕裂的末日;}-](../Page/撕裂的末日.md "wikilink")」（Equilibrium）的主角使用的武術「[Gun
    Kata](../Page/Gun_Kata.md "wikilink")」並向它致敬。

<!-- end list -->

  -
<!-- end list -->

  -
    是一隻仙狐（中國的仙狐），已有765（なむこ）歳的高齡，狐狸要1000年才能成仙，所以小牟作為仙狐中只是一個年輕人。常說一些在漫畫、遊戲及職業摔交手的口頭禪。以前与零儿的父亲有栖正护拍档。喜欢吃油炸豆腐。

二人與《超級機器人大戰OG》中的南部響介與艾克瑟蓮·白朗寧的設計非常相似。（導演森住在帕佈雷斯特時，負責創作響介和艾克瑟蓮與劇本）。

  - 逢魔（Ouma）

<!-- end list -->

  -
<!-- end list -->

  -
    與小牟不同的妖狐（日本的妖狐），零兒的殺父仇人。最終話中因為想起零兒的事，使九十九的能力削弱。稱零兒為「小孩」。與小牟一樣常引用不知從哪裡來的台詞。

<!-- end list -->

  -
<!-- end list -->

  -
    根據九十九計劃創造出來的巨大戰鬥兵器，被許多[付喪神附體](../Page/付喪神.md "wikilink")。10年前，零兒的父親以性命封在次元的狹縫中，但以沙夜作為祭品，在現世中復活。沙夜被吞噬到腹部，九十九的聲音變成了沙夜的聲音。為了越過次元的牆壁，犧牲了下半身，儘管如此她還有很強大的力量。

<!-- end list -->

  -
<!-- end list -->

  -
    沙夜的分身。與沙夜的容貌相似。不帶感情，在戰場只遵從沙夜的命令，作戰時使用日本刀。

<!-- end list -->

  - 毒[牛頭](../Page/牛頭.md "wikilink")
  - 毒[馬頭](../Page/馬頭.md "wikilink")

<!-- end list -->

  - [鎌鼬](../Page/鎌鼬.md "wikilink")（蒼、橙、紅）
  - [天狗](../Page/天狗.md "wikilink")（惡、業）

### [Namco](../Page/Namco.md "wikilink")

  - [風之少年](../Page/風之少年.md "wikilink")

<!-- end list -->

  - 克洛羅亞 配音員：[渡辺久美子](../Page/渡辺久美子.md "wikilink")

<!-- end list -->

  -
    「風之村」貝斯基路住的長耳少年，具有控制風的能力。

<!-- end list -->

  - 甘兹 配音員：[櫻井孝宏](../Page/櫻井孝宏.md "wikilink")

<!-- end list -->

  -
    稱為「金色之死神」的獨行賞金獵人。本作與古洛羅亞組成一組。

<!-- end list -->

  - Joka (Joker) 配音員：[古川登志夫](../Page/古川登志夫.md "wikilink")
  - Janga 配音員：[檜山修之](../Page/檜山修之.md "wikilink")
  - Moos 配音員：[木川絵里子](../Page/木川絵里子.md "wikilink")
  - Glibz
  - Lolo
  - 大巫女

<!-- end list -->

  - [源平討魔伝](../Page/源平討魔伝.md "wikilink")

<!-- end list -->

  - [平景清](../Page/平景清.md "wikilink")
    配音員：[置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink")

<!-- end list -->

  -
    為了毀滅源氏一族而甦醒的平氏家族武士。本作與Tarosuke組成一組。

<!-- end list -->

  - [源頼朝](../Page/源頼朝.md "wikilink")
    配音員：[大塚明夫](../Page/大塚明夫.md "wikilink")

<!-- end list -->

  -
    以前把靈魂出賣給魔族來交換毀滅平氏家族的源氏的總大將。

<!-- end list -->

  - [源義經](../Page/源義經.md "wikilink")
    配音員：[千葉一伸](../Page/千葉一伸.md "wikilink")
  - [武藏坊辯慶](../Page/武藏坊辯慶.md "wikilink")
    配音員：[鄉里大輔](../Page/鄉里大輔.md "wikilink")
  - [木曾義仲](../Page/木曾義仲.md "wikilink")
    配音員：[野中秀哲](../Page/野中秀哲.md "wikilink")
  - [骸骨](../Page/骸骨.md "wikilink")
  - 琵琶法師 配音員：[渡邊英雄](../Page/渡邊英雄.md "wikilink")
  - 風神 配音員：[望月健一](../Page/望月健一.md "wikilink")
  - 雷神 配音員：望月健一
  - 鬼姫 配音員：[木川繪里子](../Page/木川繪里子.md "wikilink")
  - 安駄婆 配音員：木川繪里子

<!-- end list -->

  - [Xenosaga](../Page/Xenosaga.md "wikilink")

<!-- end list -->

  - [卯月紫苑](../Page/卯月紫苑.md "wikilink")
    配音員：[前田愛](../Page/前田愛.md "wikilink")

<!-- end list -->

  -
    貝克達公司的女職員。本作與M.O.M.O.組成一組。

<!-- end list -->

  - [KOS-MOS](../Page/KOS-MOS.md "wikilink")
    配音員：[鈴木麻里子](../Page/鈴木麻里子.md "wikilink")

<!-- end list -->

  -
    人型格諾西斯掃討兵器。本作以version1的型態，使用version2技能。

<!-- end list -->

  - M.O.M.O. 配音員：[宍戸留美](../Page/宍戸留美.md "wikilink")
  - Allen Ridgeley
  - Gnosis

<!-- end list -->

  - [劍魂系列](../Page/劍魂.md "wikilink")

<!-- end list -->

  - 御劍平四郎 配音員：[森川智之](../Page/森川智之.md "wikilink")

<!-- end list -->

  -
    為了尋求刀魂（Soul Edge）的流浪武士

<!-- end list -->

  - 多喜 配音員：[瀧本富士子](../Page/瀧本富士子.md "wikilink")

<!-- end list -->

  -
    封魔之里的女忍者。為了破壞刀魂而旅行。本作與Waya Hime組成一組。

<!-- end list -->

  - Charade
  - 邪剑Soul Edge

<!-- end list -->

  - [超絶倫人Bravoman](../Page/超絶倫人Bravoman.md "wikilink")

<!-- end list -->

  - Bravoman 配音員：[稲田徹](../Page/稲田徹.md "wikilink")

<!-- end list -->

  -
    由保險公司的上班族變身而成的英雄。本作與超化裝物質變身的Wonder Momo組成一組。

<!-- end list -->

  - Waya Hime 配音員：[鈴木麻里子](../Page/鈴木麻里子.md "wikilink")

<!-- end list -->

  -
    爆田博士製作的女忍者型戰鬥用機器人。遊戲開始事以敵人的身分登場，其後因救了她而成為同伴。

<!-- end list -->

  - Black Bravoman 配音員：[関智一](../Page/関智一.md "wikilink")
  - Benjamin大久保彦左衛門 配音員：[渡辺英雄](../Page/渡辺英雄.md "wikilink")
  - Pistol 大名
  - 爆田博士

<!-- end list -->

  - [Dig Dug](../Page/Dig_Dug.md "wikilink")

<!-- end list -->

  - 堀·泰蔵 配音員：[古川登志夫](../Page/古川登志夫.md "wikilink")

<!-- end list -->

  -
    與原作不同，設定大幅度更變。

<!-- end list -->

  - Pookas

<!-- end list -->

  - [命運傳奇](../Page/命運傳奇.md "wikilink") Tales of Destiny

<!-- end list -->

  - 斯坦爾・艾爾隆 配音員：[関智一](../Page/関智一.md "wikilink")

<!-- end list -->

  -
    守護者・狄穆羅斯的持有者。里歐的死令他非常後悔，所以無論如何都想幫助里歐（裘達斯）。

<!-- end list -->

  - 露蒂・卡特雷特 配音員：[今井由香](../Page/今井由香.md "wikilink")

<!-- end list -->

  -
    守護者・亞托懷特的持有者。本作與斯坦爾一同行動

<!-- end list -->

  - 里歐·馬格那斯

<!-- end list -->

  -
    守護者・夏露狄耶的持有者。露蒂的弟弟，本作開頭即葬身於歐貝隆社

<!-- end list -->

  - 狄穆羅斯

<!-- end list -->

  -
    天地戰爭時遺留下來的守護者，火屬性

<!-- end list -->

  - 亞托懷特

<!-- end list -->

  -
    天地戰爭時遺留下來的守護者，水屬性。狄穆羅斯的戀人

<!-- end list -->

  - イーブルソード
  - バトラー

<!-- end list -->

  - [命運傳奇2](../Page/命運傳奇2.md "wikilink") Tales of Destiny 2

<!-- end list -->

  - 裘達斯 配音員：[綠川光](../Page/綠川光.md "wikilink")

<!-- end list -->

  -
    本作因為Black Valkyrie的力量復活。帶著龍骨的面具隱藏過去，實際身份是里歐·馬格那斯

<!-- end list -->

  - インクイジター
  - コープスリバイバー

<!-- end list -->

  - [鐵拳系列](../Page/铁拳系列.md "wikilink")

<!-- end list -->

  - [風間仁](../Page/風間仁.md "wikilink")
    配音員：[千葉一伸](../Page/千葉一伸.md "wikilink")
  - 三島平八 配音員：[郷里大輔](../Page/郷里大輔.md "wikilink")
  - Devil（三岛一八） 配音員：[篠原正典](../Page/篠原正典.md "wikilink")
  - King

<!-- end list -->

  -
    在本作中是以第2代的身分登場。與Felicia和一雙。

<!-- end list -->

  - Armor King

<!-- end list -->

  -
    King的師傅。本作因為Black Valkyrie的力量甦醒。

<!-- end list -->

  - Ogre
  - Prototype Jack
  - 木头人

只出现在训练模式

  - [迷宮塔](../Page/迷宮塔.md "wikilink")（Ishtar的復活）

<!-- end list -->

  - Gilgamesh 配音員：[石田彰](../Page/石田彰.md "wikilink")

<!-- end list -->

  -
    現已退役，但得知Druaga復活，與Kai 一起作戰。

<!-- end list -->

  - Ki 配音員：[田中理恵](../Page/田中理惠_\(聲優\).md "wikilink")
  - Druaga 配音員：[立木文彦](../Page/立木文彦.md "wikilink")
  - soul of Druaga
  - Ishtar
  - Slimes（綠、黑、青、紅、深綠、深黄色）
  - Ropers（綠、紅、青）
  - Knights
  - Quox
  - Silver Dragon
  - Black Dragon

<!-- end list -->

  - [バーニングフォースBurning](../Page/バーニングフォース.md "wikilink") Force

<!-- end list -->

  - 天現寺裕美 配音員：[横山智佐](../Page/横山智佐.md "wikilink")

<!-- end list -->

  - [バラデュークBaraduke](../Page/バラデューク.md "wikilink")

<!-- end list -->

  - Toby Masuyo 配音員：[水谷優子](../Page/水谷優子.md "wikilink")

<!-- end list -->

  -
    隸屬銀河聯邦宇宙軍的空間騎兵。本作與先後輩關係的天現寺裕美組成一組。

<!-- end list -->

  - Octy（Gilly、Dropping、Chewing、Shell、ブルースナイパー、バガン）
  - Blue Worm

<!-- end list -->

  - [妖怪道中記](../Page/妖怪道中記.md "wikilink")

<!-- end list -->

  - Tarosuke 配音員：[瀧本富士子](../Page/瀧本富士子.md "wikilink")
  - 槍[骸骨](../Page/骸骨.md "wikilink")
  - Gamagaeru
  - Gama Oyabun
  - 乙姫
  - [閻魔大王](../Page/閻魔大王.md "wikilink")

<!-- end list -->

  - [Valkyrie傳説系列](../Page/Valkyrie傳説.md "wikilink")

<!-- end list -->

  - Valkyrie 配音員：[井上喜久子](../Page/井上喜久子.md "wikilink")
  - Klino Xandra（Whirlo） 配音員：[上田祐司](../Page/上田祐司.md "wikilink")
  - Sabine 配音員：[川澄綾子](../Page/川澄綾子.md "wikilink")
  - Zouna 配音員：[田中真弓](../Page/田中真弓.md "wikilink")
  - Kamooz 配音員：[平田広明](../Page/平田広明.md "wikilink")
  - Black Valkyrie 配音員：井上喜久子
  - Zule 配音員：[望月健一](../Page/望月健一.md "wikilink")
  - Black Sandra
  - Quarkman（恶）
  - Kaox
  - Scissors
  - Dadatta
  - Robotian

<!-- end list -->

  - [Wonder Momo](../Page/Wonder_Momo.md "wikilink")

<!-- end list -->

  - Wonder Momo 配音員：[川澄綾子](../Page/川澄綾子.md "wikilink")
  - Amazona 配音員：[渡辺久美子](../Page/渡辺久美子.md "wikilink")
  - Crab Fencer 配音員：[吉野貴宏](../Page/吉野貴宏.md "wikilink")
  - Cannon Potters

### [Capcom](../Page/Capcom.md "wikilink")

  - [魔域幽靈系列](../Page/魔域幽靈.md "wikilink")

<!-- end list -->

  - [Demitri Maximoff](../Page/Demitri_Maximoff.md "wikilink")
    配音員：[檜山修之](../Page/檜山修之.md "wikilink")
  - [Morrigan Aensland](../Page/Morrigan_Aensland.md "wikilink")
    配音員：[神宮司弥生](../Page/神宮司弥生.md "wikilink")
  - [Felicia](../Page/Felicia.md "wikilink")
    配音員：[荒木香恵](../Page/荒木香恵.md "wikilink")
  - [Lei-Lei](../Page/Lei-Lei.md "wikilink")（Hsien-Ko）
    配音員：[根谷美智子](../Page/根谷美智子.md "wikilink")
  - [Lilith](../Page/Lilith.md "wikilink")
    配音員：[今井由香](../Page/今井由香.md "wikilink")
  - [Zabel Zarock](../Page/Zabel_Zarock.md "wikilink")
    配音員：[上田祐司](../Page/上田祐司.md "wikilink")
  - [Phobos](../Page/Phobos.md "wikilink")
  - [Q-Bee](../Page/Q-Bee.md "wikilink")

<!-- end list -->

  - [GUN SURVIVOR4](../Page/GUN_SURVIVOR4.md "wikilink") (Resident Evil
    Dead Aim)

<!-- end list -->

  - Bruce McGivern 配音員：[平田広明](../Page/平田広明.md "wikilink")

<!-- end list -->

  -
    美國統合戰略軍的特務。本作與Regina組成一組。

<!-- end list -->

  - 鳳鈴 配音員：[荒木香恵](../Page/荒木香恵.md "wikilink")

<!-- end list -->

  - [Captain Commando](../Page/Captain_Commando.md "wikilink")

<!-- end list -->

  - Captain Commando 配音員：[置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink")
  - 翔（Ginzu the Ninja） 配音員：[堀秀行](../Page/堀秀行.md "wikilink")
  - Hoover（Baby Head） 配音員：[笹本優子](../Page/笹本優子.md "wikilink")
  - Jennety（Mack the Knife）
  - Shtrom（Shturm） 配音員：[吉野貴宏](../Page/吉野貴宏.md "wikilink")
  - Shtrom Jr. （Shturm Jr. ） 配音員：[望月健一](../Page/望月健一.md "wikilink")
  - Druk 配音員：[野中秀哲](../Page/野中秀哲.md "wikilink")
  - Z 配音員：[渡辺英雄](../Page/渡辺英雄.md "wikilink")
  - Carol 配音員：[木川絵里子](../Page/木川絵里子.md "wikilink")
  - Brenda 配音員：木川絵里子
  - Doppel

<!-- end list -->

  - [Strider飛龍](../Page/Strider飛龍.md "wikilink")

<!-- end list -->

  - 飛竜 配音員：[鳥海浩輔](../Page/鳥海浩輔.md "wikilink")
  - Grandmaster Meio 配音員：[水鳥鉄夫](../Page/水鳥鉄夫.md "wikilink")
  - 飛燕 配音員：鳥海浩輔
  - 東風 配音員：[山田美穂](../Page/山田美穂.md "wikilink")
  - 南風
  - Solo 配音員：[渡辺英雄](../Page/渡辺英雄.md "wikilink")
  - Solo・量産型

<!-- end list -->

  - [街頭霸王系列](../Page/街頭霸王.md "wikilink")

<!-- end list -->

  - [隆](../Page/隆.md "wikilink") 配音員：[森川智之](../Page/森川智之.md "wikilink")
  - [Ken](../Page/Ken.md "wikilink")
    配音員：[岩永哲哉](../Page/岩永哲哉.md "wikilink")
  - [春麗](../Page/春麗.md "wikilink")
    配音員：[田中敦子](../Page/田中敦子.md "wikilink")
  - [嘉米](../Page/嘉米.md "wikilink")
    配音員：[河本明子](../Page/河本明子.md "wikilink")
  - [春日野櫻](../Page/春日野櫻.md "wikilink")
    配音員：[笹本優子](../Page/笹本優子.md "wikilink")
  - [神月花梨](../Page/神月花梨.md "wikilink")
    配音員：[山田美穗](../Page/山田美穗.md "wikilink")
  - [Rose](../Page/Rose_\(ストリートファイター\).md "wikilink") 配音員：根谷美智子
  - [殺意之波動覺醒的隆](../Page/殺意之波動覺醒的隆.md "wikilink") 配音員：森川智之
  - [豪鬼](../Page/豪鬼.md "wikilink")
    配音員：[西村知道](../Page/西村知道.md "wikilink")
  - [Vega](../Page/Vega.md "wikilink") 配音員：西村知道
  - [Juni](../Page/Juni.md "wikilink") 配音員：河本明子
  - [Juli](../Page/Juli.md "wikilink") 配音員：河本明子

<!-- end list -->

  - [恐龍危機](../Page/恐龍危機.md "wikilink")

<!-- end list -->

  - [蕾吉娜](../Page/蕾吉娜.md "wikilink")
    配音員：[田中敦子](../Page/田中敦子_\(配音員\).md "wikilink")
  - 迅猛龙
  - 异特龙

<!-- end list -->

  - [Final Fight系列](../Page/Final_Fight.md "wikilink")

<!-- end list -->

  - [凱](../Page/凱.md "wikilink") 配音員：[岩永哲哉](../Page/岩永哲哉.md "wikilink")
  - [Mike Haggar](../Page/Mike_Haggar.md "wikilink")
    配音員：[玄田哲章](../Page/玄田哲章.md "wikilink")

<!-- end list -->

  - [魔界村系列](../Page/魔界村.md "wikilink")

<!-- end list -->

  - Arthur 配音員：[立木文彦](../Page/立木文彦.md "wikilink")
  - 超魔王Nebiros 配音員：[吉野貴宏](../Page/吉野貴宏.md "wikilink")
  - 大魔王Astaroth 配音員：[野中秀哲](../Page/野中秀哲.md "wikilink")
  - Red Arremer Joker 配音員：[櫻井孝宏](../Page/櫻井孝宏.md "wikilink")
  - [Red Arremer](../Page/Red_Arremer.md "wikilink")
    配音員：[望月健一](../Page/望月健一.md "wikilink")
  - Red Arremer Ace 配音員：望月健一
  - Red Arremer king 配音員：望月健一
  - 死神

<!-- end list -->

  - [私立正義學園](../Page/私立正義學園.md "wikilink")

<!-- end list -->

  - 島津 英雄 配音員：[水鳥鉄夫](../Page/水鳥鉄夫.md "wikilink")
  - 水無月 響子 配音員：[三石琴乃](../Page/三石琴乃.md "wikilink")

<!-- end list -->

  - [Lost Worlds](../Page/Lost_Worlds.md "wikilink")

<!-- end list -->

  - 無名的超戰士1P 配音員：[大塚明夫](../Page/大塚明夫.md "wikilink")
  - 無名的超戰士2P 配音員：[玄田哲章](../Page/玄田哲章.md "wikilink")
  - Sylphie 配音員：[田中理恵](../Page/田中理惠_\(聲優\).md "wikilink")

<!-- end list -->

  - [洛克人DASH系列](../Page/洛克人DASH.md "wikilink")

<!-- end list -->

  - Rock Volnutt 配音員：[田中真弓](../Page/田中真弓.md "wikilink")
  - Roll Casket 配音員：[横泽启子](../Page/横泽启子.md "wikilink")
  - Tron Bonne 配音員：[飯塚雅弓](../Page/飯塚雅弓.md "wikilink")
  - Kobuns(Servbots) 配音員：[横山智佐](../Page/横山智佐.md "wikilink")
  - Rockman Juno 配音員：[石田彰](../Page/石田彰.md "wikilink")
  - Reaverbot

## 製作人員

  - 導演／[森住惣一郎](../Page/森住惣一郎.md "wikilink")（[MONOLITHSOFT](../Page/MONOLITHSOFT.md "wikilink")
    前[帕佈雷斯特SR製作小隊](../Page/帕佈雷斯特.md "wikilink")）
  - 製作人／[石谷浩二](../Page/石谷浩二.md "wikilink")（[MONOLITHSOFT](../Page/MONOLITHSOFT.md "wikilink")
    前[帕佈雷斯特](../Page/帕佈雷斯特.md "wikilink")）
  - 角色插畫／[川野琢嗣](../Page/川野琢嗣.md "wikilink")（[namco](../Page/namco.md "wikilink")）
  - 片頭動畫製作／[Production I.G](../Page/Production_I.G.md "wikilink")
  - 片頭動畫、片尾動畫作曲 [古代祐三](../Page/古代祐三.md "wikilink")

## 外部連結

  - [NAMCO x
    CAPCOM](http://www.bandainamcogames.co.jp/cs/list/namco_x_capcom/)

[Category:2005年电子游戏](../Category/2005年电子游戏.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")
[Category:Monolith Soft游戏](../Category/Monolith_Soft游戏.md "wikilink")
[Category:南梦宫游戏](../Category/南梦宫游戏.md "wikilink") [Category:PlayStation
2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:電子遊戲跨界作品](../Category/電子遊戲跨界作品.md "wikilink")
[Category:平行世界題材作品](../Category/平行世界題材作品.md "wikilink")
[Category:電子遊戲題材作品](../Category/電子遊戲題材作品.md "wikilink")