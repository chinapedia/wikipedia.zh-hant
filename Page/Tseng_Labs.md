**Tseng Laboratories, Inc.**（也稱為**Tseng
Labs**或**TLI**，在台灣過去稱為**曾氏**）是一家研製[顯示晶片與](../Page/顯示晶片.md "wikilink")[IBM相容PC用控制晶片的公司](../Page/IBM相容PC.md "wikilink")，其總部位在[美國](../Page/美國.md "wikilink")[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[Newtown](../Page/Newtown.md "wikilink")，該公司約在1980年代開始活躍，之後在1997年退出顯示、視訊、繪圖方面的業務。

Tseng最為人所熟知的產品是[Tseng Labs
ET3000](../Page/Tseng_Labs_ET3000.md "wikilink")、[Tseng Labs
ET4000以及](../Page/Tseng_Labs_ET4000.md "wikilink")[Tseng Labs
ET6000等](../Page/Tseng_Labs_ET6000.md "wikilink")[VGA相容的顯示晶片](../Page/VGA.md "wikilink")，此在1990年－1995年（[Microsoft
Windows
3.x時代](../Page/Microsoft_Windows_3.x.md "wikilink")）間相當普及與受歡迎。且值得注意的是，該公司的ET4000家族能在使用[ISA匯流排下進行高速傳輸](../Page/ISA.md "wikilink")，而且還是用傳統的DRAM型[視訊記憶體](../Page/視訊記憶體.md "wikilink")（[Framebuffer](../Page/Framebuffer.md "wikilink")）。

Tseng是1990年代中期股票市場暴跌的受害者，Tseng所喪失的市佔則由[S3
Graphics及](../Page/S3_Graphics.md "wikilink")[ATI](../Page/ATI.md "wikilink")
Technologies所瓜分。

此外Tseng過晚將[RAMDAC晶片整合到顯示晶片內](../Page/RAMDAC.md "wikilink")，使一整合一直不順，直到ET6000顯示晶片時才成功，在ET4000顯示晶片告終停產的最後數年，「未能將RAMDAC進行整合」這一點嚴重傷害Tseng的競爭力。

再者，Tseng也為其新顯示晶片、顯示卡所需的記憶體無法獲得順利供貨而苦惱，同時也缺乏資金研發合乎現代（當時）水準的整合型3D立體繪圖引擎，之後董事會決議放棄新世代產品的推出計畫，取而代之的是留存大量的現金，並開始尋求他人能收購Tseng公司。

最後由ATI公司於1997年12月買下Tseng公司，取得該公司的工程師與顯示晶片方面的經驗及實務，而Tseng的原管理階層獲得收購金額後，除了保留既有的現金外，其餘投資到一家新創公司（生化製藥業），然而該新創公司也於1998年由Cell
Pathways製藥公司所收併。

## 附註

1.  \- Cell Pathways公司也於2003年由另一家製藥商：OSI Pharmaceuticals公司所收購。

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:顯示設備硬件公司](../Category/顯示設備硬件公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")