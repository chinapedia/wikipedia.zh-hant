[YOHO_Town_Overview_201508.jpg](https://zh.wikipedia.org/wiki/File:YOHO_Town_Overview_201508.jpg "fig:YOHO_Town_Overview_201508.jpg")
[Kai_Tei_Residences_201707.jpg](https://zh.wikipedia.org/wiki/File:Kai_Tei_Residences_201707.jpg "fig:Kai_Tei_Residences_201707.jpg")單棟式[洋樓](../Page/洋樓.md "wikilink")\]\]
[Fung_Kam_Street_Sports_Centre_Interior_201508.jpg](https://zh.wikipedia.org/wiki/File:Fung_Kam_Street_Sports_Centre_Interior_201508.jpg "fig:Fung_Kam_Street_Sports_Centre_Interior_201508.jpg")
[Fung_Yau_Street_North_2019.jpg](https://zh.wikipedia.org/wiki/File:Fung_Yau_Street_North_2019.jpg "fig:Fung_Yau_Street_North_2019.jpg")
**雞地**（），是[香港](../Page/香港.md "wikilink")[元朗市中心東南部分的一個地方](../Page/元朗市中心.md "wikilink")，位於[青山公路－元朗段以南](../Page/青山公路－元朗段.md "wikilink")、[鳳翔路以東](../Page/鳳翔路.md "wikilink")、[元朗公路以西](../Page/元朗公路.md "wikilink")、[下攸田村以北](../Page/下攸田村.md "wikilink")，現時以[住宅為主](../Page/住宅.md "wikilink")。雞地之名源於這裡曾是[元朗墟買賣](../Page/元朗墟.md "wikilink")[雞](../Page/雞.md "wikilink")[鴨等](../Page/鴨.md "wikilink")[家禽的市場](../Page/家禽.md "wikilink")。

## 歷史

20世紀初，[合益公司建立](../Page/合益公司.md "wikilink")[元朗新墟後](../Page/元朗新墟.md "wikilink")，先在現時[谷亭街一帶設立](../Page/谷亭街.md "wikilink")「大穀亭」供人們買賣糶穀糴米，被稱爲「米地」。「米地」商戶後來被拆遷，然而政府同時亦考慮到元朗需要一個地方供村民販賣雞鴨。至1960年代初期，村民開始在元朗墟以東的空地售賣家禽。每逢墟期，村民會把雞鴨運到空地大棚販賣，並將一籠籠雞鴨放置在地上，久而久之人們便稱呼該地爲「雞地」\[1\]。

到了1980年代起，該處開始發展出一些學校和單棟式[洋樓為主的商住區](../Page/洋樓.md "wikilink")。到了1990年代，墟市的面積逐漸縮小，只剩下現時位於[YOHO
Midtown旁的蔬菜批發市場](../Page/YOHO_Midtown.md "wikilink")，而家禽墟市更已銷聲匿跡。

由於「雞地」之名純屬當地居民約定俗成對該家禽市場的稱呼，而名字本身亦略爲土俗，故政府多年來亦沒有採用「雞地」之名作爲該區正式名稱，然而政府亦有考慮到「雞地」一帶家禽市場的歷史，故此當政府在該地進行新市鎮發展時，便使用了香港人常見對「雞」的雅稱「鳳」字作爲代替\[2\]，如當地道路命名多以「鳳」字起頭，雞地附近的居屋屋苑命名爲「[鳳庭苑](../Page/鳳庭苑.md "wikilink")」，[區議會選區命名爲](../Page/區議會.md "wikilink")「鳳翔」分區，而從政府官方分區來說，該地現在已經被併入[元朗市的一部分](../Page/元朗市.md "wikilink")。

至於「雞地」二字，目前只能見於[港攸路南側](../Page/港攸路.md "wikilink")、[下攸田村外的](../Page/下攸田村.md "wikilink")[雞地停車場](../Page/雞地停車場.md "wikilink")。雖然「雞地」之名不見於任何政府地圖或網絡地圖上，但「雞地」作爲有明確定義的通俗地名一直爲元朗居民所使用，並成為元朗人的集體回憶\[3\]\[4\]。

## 主要建築

  - 私人屋苑
      - [新時代廣場（YOHO Town）](../Page/新時代廣場_\(元朗\).md "wikilink")
      - [YOHO Midtown](../Page/YOHO_Midtown.md "wikilink")
      - [雍翠豪園](../Page/雍翠豪園.md "wikilink")
      - [豪苑](../Page/豪苑.md "wikilink")
      - 好順利大廈
      - 好順意大廈
      - 金龍樓
      - 昌威大廈
      - 偉發大廈
      - 順豐大廈
      - 益發大廈
  - [居屋屋苑](../Page/居屋.md "wikilink")
      - [鳳庭苑](../Page/鳳庭苑.md "wikilink")

<!-- end list -->

  - 康樂設施
      - [鳳琴街體育館](../Page/鳳琴街體育館.md "wikilink")
      - 鳳群街花園

<!-- end list -->

  - 學校

[Kwong_Ming_Ying_Loi_School,_Yuen_Long_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Kwong_Ming_Ying_Loi_School,_Yuen_Long_\(Hong_Kong\).jpg "fig:Kwong_Ming_Ying_Loi_School,_Yuen_Long_(Hong_Kong).jpg")

  -   - [中華基督教會基朗中學](../Page/中華基督教會基朗中學.md "wikilink")
      - [聖公會白約翰會督中學](../Page/聖公會白約翰會督中學.md "wikilink")
      - [中華基督教會基元中學](../Page/中華基督教會基元中學.md "wikilink")
      - [光明英來學校](../Page/光明英來學校.md "wikilink")
      - [佛教榮茵學校](../Page/佛教榮茵學校.md "wikilink")

## 街道

  - [鳳翔路](../Page/鳳翔路.md "wikilink")
  - [鳳琴街](../Page/鳳琴街.md "wikilink")
  - [鳳香街](../Page/鳳香街.md "wikilink")
  - [鳳群街](../Page/鳳群街.md "wikilink")
  - [鳳攸東街](../Page/鳳攸東街.md "wikilink")
  - [鳳攸南街](../Page/鳳攸南街.md "wikilink")
  - [鳳攸北街](../Page/鳳攸北街.md "wikilink")

## 另見

  - \[<https://mmis.hkpl.gov.hk/zh/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_actual_q=(%20(%20allTermsMandatory%3A(true)%20OR+all_dc.title%3A(%E9%9B%9E%E5%9C%B0)%20OR+all_dc.creator%3A(%E9%9B%9E%E5%9C%B0)%20OR+all_dc.contributor%3A(%E9%9B%9E%E5%9C%B0)%20OR+all_dc.subject%3A(%E9%9B%9E%E5%9C%B0)%20OR+fulltext%3A(%E9%9B%9E%E5%9C%B0)%20OR+all_dc.description%3A(%E9%9B%9E%E5%9C%B0)>))\&p_r_p_-1078056564_c=QF757YsWv59JUcUoLpFJoU2upvwjVpYu&_coverpage_WAR_mmisportalportlet_sort_field=score&_coverpage_WAR_mmisportalportlet_hsf=%E9%9B%9E%E5%9C%B0&_coverpage_WAR_mmisportalportlet_sort_order=desc&_coverpage_WAR_mmisportalportlet_o=0&_coverpage_WAR_mmisportalportlet_log=Y\&tabs1=CATALOGUE
    1985年元朗(東)雞地照片 -
    香港公共圖書館館藏\]（現存記錄雞地家禽買賣活動的珍貴歷史照片。照片中可見雞地當時仍然存在的家禽買賣活動，而背景能看見鳳悠北街一帶的住宅。）

## 參考注釋

{{-}}

[Category:元朗市中心](../Category/元朗市中心.md "wikilink")
[Category:元朗](../Category/元朗.md "wikilink")

1.  [Tere Wong：跑遊元朗(8)－雞地](http://blog.terewong.com/archives/12140)
2.  [廣式點心的經典菜式](../Page/廣式點心.md "wikilink")「鳳爪」即爲雞爪。
3.  [雞地停車場
    僅餘的雞地回憶](http://isletforum.com/forum.php?mod=viewthread&tid=12324)
4.