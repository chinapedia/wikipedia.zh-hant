**aMule**是一个[开源](../Page/开源软件.md "wikilink")[免费的](../Page/免费软件.md "wikilink")[P2P](../Page/P2P.md "wikilink")[文件共享软件](../Page/文件共享.md "wikilink")，遵循[GNU通用公共许可证协议发布](../Page/GNU通用公共许可证.md "wikilink")。类似于[eMule](../Page/eMule.md "wikilink")。基于[xMule和](../Page/xMule.md "wikilink")[lMule](../Page/lMule.md "wikilink")。可应用[eDonkey网络](../Page/eDonkey网络.md "wikilink")[协议](../Page/网络传输协议.md "wikilink")，也支持[KAD网络](../Page/Kademlia.md "wikilink")。可在包括各种[类Unix系统](../Page/类Unix.md "wikilink")、[Windows在内的多种操作系统下运行](../Page/Windows.md "wikilink")。

## 元件

完整的 aMule 有以下元件：

  - amule（伺服器／用戶端）- aMule 的桌面程式
  - amuled（伺服器）- 后台程序，可配合 amulecmd 或 amulegui 使用
  - amulecmd（用戶端）- 可以在指令模式下使用
  - amulegui（用戶端）- 在圖形介面下使用
  - amuleweb（伺服器）- 打開後，可以在瀏覽器下使用 aMule
  - ed2k（实用程序） - ed2k 链接处理器，浏览器可以通过该命令向 aMule 添加 ed2k 链接。
  - alc（实用程序） - 计算文件 ed2k 链接的工具
  - alcc（实用程序） - alc 的 [CLI](../Page/CLI.md "wikilink") 版本

## 语言

目前aMule使用[i18n](../Page/i18n.md "wikilink")，有28种语言界面供选择，包括[简体和](../Page/簡體中文.md "wikilink")[繁体中文](../Page/繁体中文.md "wikilink")。

aMule意为。\[1\]

## DLP支援

动态反吸血驴保护（英文全称：Dynamic Leecher Protection, 英文缩写：DLP）是内建于 eMule Xtreme Mod
等一些 eMule 的修改版软体（即 eMule Mod）中的功能元件。在这些 eMule Mod 连线上 eDonkey 网路后，它会根据
DLP 函式库中的列表，侦测出吸血驴并对其做减分或遮蔽处理。

目前aMule尚未内建DLP功能，但仍然可用 **aMule-dlp** 版本加入。

## 参见条目

  - [eDonkey网络](../Page/eDonkey网络.md "wikilink")
  - [eMule](../Page/eMule.md "wikilink")
  - [动态反吸血驴保护](../Page/动态反吸血驴保护.md "wikilink")

## 参考资料

## 外部链接

  - [aMule主页](http://www.amule.org/)

  - [aMule官方Wiki](https://web.archive.org/web/20090927154917/http://wiki.amule.org/index.php/Main_Page)

### 參與中文翻譯

  - [aMule
    繁體中文翻譯說明](https://web.archive.org/web/20100213091616/http://wiki.amule.org/index.php/Translations-tw)
  - [aMule 翻譯討論區](http://forum.amule.org/index.php?board=40.0)
  - [aMule
    翻譯列表](https://web.archive.org/web/20080410181318/http://www.amule.org/translations/)

[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:EDonkey客戶端](../Category/EDonkey客戶端.md "wikilink")
[Category:自由檔案分享軟體](../Category/自由檔案分享軟體.md "wikilink")

1.