**卡尔-海因茨·鲁梅尼格**（****，）是[德國前](../Page/德國.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，欧洲足球巨星。現時是足球行政人員。

## 球員生涯

鲁梅尼格是在1974年加入德國班霸[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")，司職前鋒。當時鲁梅尼格並未太受到注意，蓋因其在場上表現一直都不算太突出，直到1979年教練卡斯尼(Pal
Csernai)執教拜仁後，鲁梅尼格的表現便有所不同。在1979-80年球季，鲁梅尼格共在[德甲聯賽入了](../Page/德甲.md "wikilink")26球。其後他亦能在1981年和1984年分別刷新入29球和26球紀錄，在當時的德甲水平而言可謂相當出色。1980年鲁梅尼格當選為[德國足球先生](../Page/德國足球先生.md "wikilink")，当年及翌年亦當選為[歐洲足球先生](../Page/歐洲足球先生.md "wikilink")。

鲁梅尼格當時已是國家隊重心人物之一，曾在1980年歐洲國家杯贏得冠軍，亦曾參與1982和1986年世界杯（可是德國最終敗在阿根廷和意大利），皆能助球隊打入決賽。他一共為國家隊上陣85場入45球，唯一的遺憾就是在1984年歐洲國家杯首圈出局，衛冕失敗。

1984年鲁梅尼格前往[意大利](../Page/意大利.md "wikilink")，轉投當時喜愛收購德國球員，屬[意甲大球會的](../Page/意甲.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")。可是鲁梅尼格在國際米蘭的數季受傷病困擾，表現不及拜仁時代。隨著他年事已高，1987年改投瑞士一家球會，並在這家球會踢至掛靴。

## 領導生涯

1989年鲁梅尼格宣佈掛靴，起先是在一家電視台擔任足球評述員，1991年拜仁慕尼黑邀請[弗朗茨·贝肯鲍尔和鲁梅尼格加入拜仁管理層](../Page/弗朗茨·贝肯鲍尔.md "wikilink")，結果鲁梅尼格亦一直擔任球會經理長達十年。2002年鲁梅尼格更加晉升為拜仁董事會主席，主要負責球會對外關係。

2004年3月鲁梅尼格被球王贝利選為[125位在世的最偉大的足球運動員名單](../Page/FIFA_100.md "wikilink").

## 生涯統計

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>球隊</p></th>
<th><p>出賽（入球）</p></th>
<th><p>出賽（入球）</p></th>
<th><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1963-74年</p></td>
<td><p>Borussia Lippstadt</p></td>
<td></td>
<td></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p>1974-84年</p></td>
<td><p><a href="../Page/拜仁慕尼黑.md" title="wikilink">拜仁慕尼黑</a></p></td>
<td><p>310（162）</p></td>
<td><p>78（40）</p></td>
<td><p><a href="../Page/丰田杯.md" title="wikilink">洲際盃</a>: 1976年<br />
<a href="../Page/歐聯.md" title="wikilink">歐洲冠軍球會盃</a>: 1975年, 1976年<br />
<a href="../Page/德甲.md" title="wikilink">德國甲組足球聯賽冠軍</a>: 1980年, 1981年<br />
<a href="../Page/德國盃.md" title="wikilink">德國盃</a>: 1982年, 1984年</p></td>
</tr>
<tr class="odd">
<td><p>1984-87年</p></td>
<td><p><a href="../Page/國際米蘭.md" title="wikilink">國際米蘭</a></p></td>
<td><p>64（24）</p></td>
<td><p>17（5）</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p>1987-89年</p></td>
<td><p><a href="../Page/Servette_FC.md" title="wikilink">塞爾維特</a></p></td>
<td><p>50（34）</p></td>
<td></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>1976-86年</p></td>
<td><p><a href="../Page/德國國家足球隊.md" title="wikilink">西德</a></p></td>
<td><p>95（45）</p></td>
<td></td>
<td><p><a href="../Page/欧洲足球锦标赛.md" title="wikilink">-{zh-hans:欧洲足球锦标赛;zh-hk:歐洲國家盃;zh-tw:歐洲足球錦標賽;}-</a>: 1980年</p></td>
</tr>
<tr class="even">
<td><p>同時:</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>個人榮譽</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p><a href="../Page/德甲.md" title="wikilink">德甲神射手</a></p></td>
<td><p>26 球</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1981年</p></td>
<td><p><a href="../Page/德甲.md" title="wikilink">德甲神射手</a></p></td>
<td><p>29 球</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/德甲.md" title="wikilink">德甲神射手</a></p></td>
<td><p>26 球</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p>瑞士聯賽神射手</p></td>
<td><p>24 球</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p><a href="../Page/德國足球先生.md" title="wikilink">德國足球先生</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td><p><a href="../Page/歐洲足球先生.md" title="wikilink">歐洲足球先生</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p><a href="../Page/歐洲足球先生.md" title="wikilink">歐洲足球先生</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 他是德國國家隊中第八個代表次數最多的球員(只包括前西德球員)。
  - 他是德甲史上第十個入球量最多的球員。
  - 他是拜仁慕尼黑歷來入球量最多的球員，僅次於[梅拿](../Page/盖德·穆勒.md "wikilink")。

## 外部連結

  - [Karl-Heinz Rummenigge @
    FCB.de](http://www.fcbayern.t-com.de/en/company/company/00282.php?fcb_sid=d2e0af420517584081b7c30ed42b7697)
  - [Karl-Heinz Rummenigge @
    soccer-europe.com](http://www.soccer-europe.com/Biographies/Rummenigge.html)
  - [Autograph Karl-Heinz
    Rummenigge](https://web.archive.org/web/20060715010351/http://www.autogramm-archiv.de/spieler/rummenigge-karl-heinz.htm)

[Category:1986年世界盃足球賽球員](../Category/1986年世界盃足球賽球員.md "wikilink")
[Category:1982年世界盃足球賽球員](../Category/1982年世界盃足球賽球員.md "wikilink")
[Category:1978年世界盃足球賽球員](../Category/1978年世界盃足球賽球員.md "wikilink")
[Category:1984年歐洲國家盃球員](../Category/1984年歐洲國家盃球員.md "wikilink")
[Category:1980年歐洲國家盃球員](../Category/1980年歐洲國家盃球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:德国足球运动员](../Category/德国足球运动员.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:足球前锋](../Category/足球前锋.md "wikilink")
[Category:塞維特球員](../Category/塞維特球員.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")
[Category:歐洲足球先生](../Category/歐洲足球先生.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:瑞士超聯球員](../Category/瑞士超聯球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:拜仁慕尼黑职员](../Category/拜仁慕尼黑职员.md "wikilink")