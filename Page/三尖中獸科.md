**三尖中獸科**（*Triisodontidae*）是已[滅絕的一科](../Page/滅絕.md "wikilink")[中爪獸目](../Page/中爪獸目.md "wikilink")。大部份三尖中獸科都是生存於[古新世早期的](../Page/古新世.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，而只有[安氏中獸屬是生存於](../Page/安氏中獸屬.md "wikilink")[始新世晚期的](../Page/始新世.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")。\[1\]三尖中獸科是繼[恐龍滅絕後](../Page/恐龍.md "wikilink")，北美洲最先出現的大型掠食[哺乳動物](../Page/哺乳動物.md "wikilink")。\[2\]牠們有較不進化的[牙齒](../Page/牙齒.md "wikilink")，這是與其他中爪獸目不同的地方。\[3\]

由於三尖中獸科的牙齒較為簡單，牠們被認為是中爪獸目的基底。一些學者將三尖中獸科看為[踝節目](../Page/踝節目.md "wikilink")，或甚至是[熊犬科](../Page/熊犬科.md "wikilink")，而非中爪獸目。

## 參考

[Category:中爪獸目](../Category/中爪獸目.md "wikilink")
[Category:三尖中獸科](../Category/三尖中獸科.md "wikilink")

1.
2.
3.