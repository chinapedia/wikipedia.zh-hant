**班·巴恩斯**（，
英國男演員，他曾出現在影集《》跟《》，也在《[星塵傳奇](../Page/星塵傳奇.md "wikilink")》、《[俄國混混在倫敦](../Page/俄國混混在倫敦.md "wikilink")》跟《[納尼亞傳奇：賈思潘王子](../Page/納尼亞傳奇：賈思潘王子.md "wikilink")》幾部電影中演出。

## 學歷

班是（）的畢業生，與他同期的有電影演員（）跟喜劇演員，1997-2003年曾是（）一員，2004年從倫敦[金斯頓大學](../Page/金斯頓大學.md "wikilink")（）畢業，主修[戲劇與](../Page/戲劇.md "wikilink")[英國](../Page/英國.md "wikilink")[文學](../Page/文學.md "wikilink")，曾與（），和[亞當·嘉西亞](../Page/亞當·嘉西亞.md "wikilink")（）為攝影師拍攝照片，曾經短期組過一個叫「」的男孩，他的樂團曾在2004年的[英國星光大道](../Page/英國星光大道.md "wikilink")（）演出他們自己的歌「」[Ben_Barnes_in_Hyrise_for_eurovision_2004.JPG](https://zh.wikipedia.org/wiki/File:Ben_Barnes_in_Hyrise_for_eurovision_2004.JPG "fig:Ben_Barnes_in_Hyrise_for_eurovision_2004.JPG")，媽媽是個[心理治療師擁有一半的](../Page/心理治療師.md "wikilink")[非裔](../Page/非裔.md "wikilink")[加勒比海跟一半的](../Page/加勒比海.md "wikilink")[英格蘭血統](../Page/英格蘭.md "wikilink")，而爸爸是個[心理](../Page/心理.md "wikilink")[醫師擁有絕大多數的](../Page/醫師.md "wikilink")[英格蘭血統加上](../Page/英格蘭.md "wikilink")[威爾斯跟](../Page/威爾斯.md "wikilink")[德國血統](../Page/德國.md "wikilink")，父母教他要以當個認真的演員為目標之前要先去上大學。

## 經歷

在2008年6月/7月所發行的《》[雜誌透露](../Page/雜誌.md "wikilink")，他在是後備歌手，在2006年開始在[電視中嶄露頭角](../Page/電視.md "wikilink")，他的[電影](../Page/電影.md "wikilink")[處女作是](../Page/處女作.md "wikilink")2007年的[星塵傳奇](../Page/星塵傳奇.md "wikilink")，在片中飾演寫實的，之後演出的，此片在2008年8月於[美國上映](../Page/美國.md "wikilink")。

《[納尼亞傳奇：賈思潘王子](../Page/納尼亞傳奇：賈思潘王子.md "wikilink")》的[導演](../Page/導演.md "wikilink")[安得魯·安德森在](../Page/安得魯·安德森.md "wikilink")2007年2月宣布班恩斯將在《[納尼亞傳奇：賈思潘王子](../Page/納尼亞傳奇：賈思潘王子.md "wikilink")》中演出賈思潘一角，製作小組進行試鏡面試超過1年，進行試鏡招慕的國家包括：[阿根廷](../Page/阿根廷.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[法國](../Page/法國.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[英國等](../Page/英國.md "wikilink")。在一個偶然機會下，試鏡導演在[英國的國家劇院中欣賞](../Page/英國.md "wikilink")[舞台劇時](../Page/舞台劇.md "wikilink")，發現光芒四射的巴恩斯，便力薦給身在[澳洲進行選角的導演](../Page/澳洲.md "wikilink")，特別安排雙方在[英國和](../Page/英國.md "wikilink")[澳洲之間的中間點](../Page/澳洲.md "wikilink")――[美國](../Page/美國.md "wikilink")[洛杉磯會面](../Page/洛杉磯.md "wikilink")。在試鏡時，因為角色有大量的[騎馬戲份](../Page/騎馬.md "wikilink")，導演查問巴恩斯是否有過騎馬，班恩斯當時回答有，當他回家詢問母親，才得知自己只在幾歲的時候騎過一次[迷你馬](../Page/迷你馬.md "wikilink")。後來巴恩斯得到角色，卻因此要為他安排整整一個月的騎馬訓練。大學畢業後，成為全職舞台劇演員，參與多個大型演出。被《[納尼亞傳奇：賈思潘王子](../Page/納尼亞傳奇：賈思潘王子.md "wikilink")》試鏡導演發掘之時，他正於被譽為全球最高水準、有話劇聖地之稱的[倫敦西區劇院中演出](../Page/倫敦西區劇院.md "wikilink")《》。

安德森說:「賈思潘是個成年人，在某些程度這是一個喪失天真的故事，賈思潘開始時十分的天真，之後很渴望報仇，但到最後放棄報仇了。」當許多的讀者把賈思潘想像成一個小孩子，小說中的一節曾提到他的年齡與彼得的年紀相近，所以要找一個老一點的演員與[威廉·莫斯利相配](../Page/威廉·莫斯利.md "wikilink")，巴恩斯曾在小時候讀過小說，在與電影製片開會後在三個半星期被選為飾演賈思潘一角，他為了準備拍攝花了兩個月的時間在[紐西蘭訓練騎馬技術和訓練特技](../Page/紐西蘭.md "wikilink")，巴恩斯的[西班牙口音是以](../Page/西班牙.md "wikilink")（）在《》（）中飾演的為藍本，安德森根本沒有預期要找一個[英國演員來飾演賈思潘](../Page/英國.md "wikilink")，還說巴恩斯更適合當亞當斯和四個飾演佩文的演員的家人，因為他使每個見到他的人都歡喜，當在扮演賈思潘時，巴恩斯正準備與[國家大劇院](../Page/國家大劇院.md "wikilink")（）
的《》（）巡迴演出，（）向巴恩斯開玩笑的說：「現在可能不是國家『劇院』最喜歡的演員。」，巴恩斯離開[英國時並沒有告訴劇院](../Page/英國.md "wikilink")。

巴恩斯在六月將到世界各地工作去宣傳賈思潘王子，他也準備在[納尼亞傳奇電影系列第三集](../Page/納尼亞傳奇.md "wikilink")[納尼亞傳奇：黎明行者號再次演出](../Page/納尼亞傳奇：黎明行者號.md "wikilink")(這次將飾演賈思潘國王，賈思潘十世)
，拍攝將在2008年10月展開，預計在2010年5月7日在[美國上映](../Page/美國.md "wikilink")。

他在2009年演出《》飾演約翰·惠特克（），巴恩斯也在[奧斯卡·王爾德的](../Page/奧斯卡·王爾德.md "wikilink")[美少年格雷的畫像來飾演道林](../Page/道林·格雷的畫像.md "wikilink")·格雷（），由[奧利弗·帕克](../Page/奧利弗·帕克.md "wikilink")（）來當導演，在2009年上映。

## 演藝作品

### 電影

<table>
<thead>
<tr class="header">
<th><p><strong>年份</strong></p></th>
<th><div style="text-align: center;">
<p><strong>名稱</strong></p>
</div></th>
<th><div style="text-align: center;">
<p><strong>角色</strong></p>
</div></th>
<th><p><strong>附註</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/星塵傳奇.md" title="wikilink">星塵傳奇</a><br />
<small>（）</small></p></td>
<td><p>Young Dunstan</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td></td>
<td><p>Cobakka</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/納尼亞傳奇：賈思潘王子.md" title="wikilink">納尼亞傳奇：賈思潘王子</a><br />
<small>（）</small></p></td>
<td><p>Prince Caspian X</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><br />
<small>（）</small></p></td>
<td><p>John Whittaker</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/不死魔咒.md" title="wikilink">不死魔咒</a><br />
<small>（）</small></p></td>
<td><p>Dorian Gray</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td></td>
<td><p>Josh</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/納尼亞傳奇：黎明行者號.md" title="wikilink">納尼亞傳奇：黎明行者號</a><br />
<small>（）</small></p></td>
<td><p>King Caspian X</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><br />
<small>（）</small></p></td>
<td><p>Neil McCormick</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><br />
<small>（）</small></p></td>
<td><p>Young Man</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/盛大婚禮.md" title="wikilink">婚禮大聯矇</a><br />
<small>（）</small></p></td>
<td><p>Alejandro Griffin</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><br />
<small>（）</p></td>
<td><p>Nick Tortano</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><br />
<small>（）</p></td>
<td><p>Ryan</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/第七傳人.md" title="wikilink">第七傳人</a><br />
<small>（）</p></td>
<td><p>Tom Ward</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 影集

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p>名稱</p>
</div></td>
<td><div style="text-align: center;">
<p>角色</p>
</div></td>
<td><div style="text-align: center;">
<p>電視台</p>
</div></td>
<td><div style="text-align: center;">
<p>播放日期</p>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Doctors.md" title="wikilink">Doctors</a><br />
Facing Up</p></td>
<td><p>Craig Unwin</p></td>
<td><p><a href="../Page/BBC_One.md" title="wikilink">BBC One</a></p></td>
<td><p>2006年2月20日</p></td>
</tr>
<tr class="odd">
<td><p>Split Decision</p></td>
<td><p>Chris Wilbur</p></td>
<td></td>
<td><p>2006年9月</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西方極樂園.md" title="wikilink">西方極樂園</a><br />
<small>()</p></td>
<td><p>Logan</p></td>
<td><p><a href="../Page/HBO.md" title="wikilink">HBO</a></p></td>
<td><p>2016年10月</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/制裁者_(電視劇).md" title="wikilink">漫威制裁者</a><br />
<small>()</p></td>
<td><p>比利·羅素</p></td>
<td><p><a href="../Page/Netflix.md" title="wikilink">Netflix</a></p></td>
<td><p>2017年11月</p></td>
</tr>
</tbody>
</table>

## 外部連結

  -
  - [Ben Barnes](https://www.facebook.com/BenBarnesOfficial) on
    [Facebook](../Page/Facebook.md "wikilink")

  - [Ben Barnes](https://twitter.com/benbarnes) on
    [Twitter](../Page/Twitter.md "wikilink")

  - [Ben Barnes](https://www.instagram.com/benbarnes/) on
    [Instagram](../Page/Instagram.md "wikilink")

[Category:英格蘭男電影演員](../Category/英格蘭男電影演員.md "wikilink")
[Category:英格蘭男電視演員](../Category/英格蘭男電視演員.md "wikilink")