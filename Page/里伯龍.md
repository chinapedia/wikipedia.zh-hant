**里伯龍**（屬名：*Libonectes*）是[鰭龍超目的一屬](../Page/鰭龍超目.md "wikilink")，屬於[蛇頸龍目](../Page/蛇頸龍目.md "wikilink")。目前的唯一化石發現於[美國](../Page/美國.md "wikilink")[德州的Britton組地層](../Page/德州.md "wikilink")，地質年代相當於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[土侖階](../Page/土侖階.md "wikilink")。

這個化石在1949年被建立為[薄板龍的一種](../Page/薄板龍.md "wikilink")，在1997年由[肯尼思·卡彭特](../Page/肯尼思·卡彭特.md "wikilink")（Kenneth
Carpenter）建立為里伯龍屬，屬名意為「西南的游泳者」\[1\]。

里伯龍身長7到14公尺，外形非常類似牠們的近親[海霸龍](../Page/海霸龍.md "wikilink")，但[頸椎結構不同](../Page/頸椎.md "wikilink")，里伯龍具有較高的[神經棘與較長的骨突](../Page/神經棘.md "wikilink")，鼻孔較接近口鼻部前端。里伯龍的頭顱骨是已知保存狀態最好的[薄板龍科頭顱骨](../Page/薄板龍科.md "wikilink")。目前已在[正模標本腹部發現](../Page/正模標本.md "wikilink")[胃石](../Page/胃石.md "wikilink")\[2\]。

里伯龍擁有結實的身體、長頸部、短尾巴、以及大型鰭狀肢。牠們的[頭顱骨小](../Page/頭顱骨.md "wikilink")，頸部長，擁有長、往前突出的牙齒，適合抓住光滑的[魚類與](../Page/魚類.md "wikilink")[魷魚](../Page/魷魚.md "wikilink")。里伯龍具有四個強壯、充滿肌肉的鰭狀肢，適合在水中游泳，牠們會吞食石頭，以協助在水中的穩定性。里伯龍被認為會高舉頸部，昇出海面。但科學家認為里伯龍的頸部基段過於堅挺，只能做出小幅度的彎曲，無法做出這種動作。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

[Category:蛇頸龍亞目](../Category/蛇頸龍亞目.md "wikilink")
[Category:白堊紀爬行動物](../Category/白堊紀爬行動物.md "wikilink")

1.
2.  Carpenter, K. (1999). "Revision of North American elasmosaurs from
    the Cretaceous of the western interior." *Paludicola*, **2**(2):
    148-173.