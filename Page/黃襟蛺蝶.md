**黃襟蛺蝶**（学名：**）是[蛺蝶科下的一種](../Page/蛺蝶科.md "wikilink")[蝴蝶](../Page/蝴蝶.md "wikilink")，主要分布在南亚和东南亚地区。

## 描述

上半部为赭黄色。前翅有一些圆形的，细长的暗色印记，一条宽且有点弯曲的横向黄色条纹从[肋脉到第一](../Page/鳞翅目术语表.md "wikilink")[翅脉](../Page/鳞翅目术语表.md "wikilink")，不过没有延伸到翅膀末端，条纹的后端变宽，边缘不规则，朝里为黑色，在[间隙](../Page/鳞翅目术语表.md "wikilink")3和间隙4突出，间隙2内凹，间隙1外凸。黄色条纹上有3个呈弧状排列的黑色圆点，分别位于间隙1、2和3，最下方最大。

## 参考资料

  - Bingham, C.T. (1905): The Fauna of British India, Including Ceylon
    and Burma'. Lepidoptera, Volume 1''
  - Hamer, K.C.; Hill, J.K.; Benedick, S.; Mustaffa, N.; Chey, V.K. &
    Maryati, M. (2006): Diversity and ecology of carrion- and
    fruit-feeding butterflies in Bornean rain forest. *Journal of
    Tropical Ecology* **22**: 25–33. <small></small> (HTML abstract)
  - Robinson, Gaden, S.; Ackery, Phillip R.; Kitching, Ian J.;
    Beccaloni, George W. & Hernández, Luis M. (2007): *[HOSTS - a
    Database of the World's Lepidopteran
    Hostplants](http://www.nhm.ac.uk/research-curation/projects/hostplants/)*.
    Accessed July 2007.

[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國蝴蝶](../Category/中國蝴蝶.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:臺灣蝴蝶](../Category/臺灣蝴蝶.md "wikilink")
[Category:襟蛺蝶屬](../Category/襟蛺蝶屬.md "wikilink")