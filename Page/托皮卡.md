**托皮卡**（****）是[美國](../Page/美國.md "wikilink")[堪薩斯州東北部的](../Page/堪薩斯州.md "wikilink")[肖尼縣的縣治](../Page/肖尼縣_\(堪薩斯州\).md "wikilink")，也是堪薩斯州的州政府所在地。人口122,377(2000年統計)。托皮卡創立於1854年12月5日。城市面積為147.6平方公里（57平方英哩），其中陸地面積佔了145.1平方公里（56.0
平方英哩）。美國海軍為紀念這城市，曾經把三艘船命名為「托皮卡」〈USS Topeka〉。托皮卡的主要產業為食品加工。

## 交通

托皮卡陸地交通方便。I-70公路貫穿全市：向西往[丹佛市](../Page/丹佛市.md "wikilink")，在[猶他州內連接I](../Page/猶他州.md "wikilink")-15公路；向東往[堪薩斯城](../Page/堪薩斯城.md "wikilink")、聖路易斯(密蘇里州)、[印第安納波利斯和](../Page/印第安納波利斯.md "wikilink")[匹茲堡等多個城市](../Page/匹茲堡.md "wikilink")。

## 外部連結

  - [托皮卡官方網站](http://www.topeka.org/)

[Category:堪萨斯州城市](../Category/堪萨斯州城市.md "wikilink")
[Category:堪薩斯州縣城](../Category/堪薩斯州縣城.md "wikilink")
[Category:肖尼縣城市 (堪薩斯州)](../Category/肖尼縣城市_\(堪薩斯州\).md "wikilink")
[Category:美國各州首府](../Category/美國各州首府.md "wikilink")
[Category:托皮卡都會區](../Category/托皮卡都會區.md "wikilink")
[Category:1854年建立的聚居地](../Category/1854年建立的聚居地.md "wikilink")
[Category:1854年堪薩斯領地建立](../Category/1854年堪薩斯領地建立.md "wikilink")