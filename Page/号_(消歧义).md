**[号](../Page/号.md "wikilink")**是東亞傳統中人为自己所取的較正式的别名。

**号**还可以指：

## 称谓

“号”用作名称的意思：

  - [称号](../Page/头衔.md "wikilink")，头衔

  - [名号](../Page/名字.md "wikilink")，名字

  - [外号](../Page/外号.md "wikilink")、[诨号](../Page/诨号.md "wikilink")、[绰号](../Page/绰号.md "wikilink")，花名

  - [讹号](../Page/化名.md "wikilink")，假名或化名

  - [商業字號](../Page/商業字號.md "wikilink")，企業基於商業目的所使用的貿易名稱

  - [道号](../Page/道号.md "wikilink")、[法號](../Page/法號.md "wikilink")，出家修行人的名号

  - [佛号](../Page/佛.md "wikilink")，佛的名号

  - [讳号](../Page/避讳.md "wikilink")，避讳使用的称号

  - [帝号](../Page/皇帝.md "wikilink")、[庙号](../Page/庙号.md "wikilink")、[谥号](../Page/谥号.md "wikilink")、[尊号](../Page/尊号.md "wikilink")、[封号](../Page/封号.md "wikilink")、[徽號](../Page/徽號.md "wikilink")，東亞君主、功臣、王侯贵族等的称号

  - 、、[院殿号](../Page/院殿号.md "wikilink")、[寺殿号](../Page/寺殿号.md "wikilink")、[轩号](../Page/轩号.md "wikilink")，日本皇室及王侯贵族的称号

  - [官号](../Page/官职.md "wikilink")，官职

  - [爵号](../Page/爵位.md "wikilink")，爵位

  - [印号](../Page/官印.md "wikilink")，官玺、官印、印章的名号

  - [年號](../Page/年號.md "wikilink")，紀年的名號

  - [堂号](../Page/堂号.md "wikilink")、[郡號](../Page/郡望.md "wikilink")，祠堂的郡望，称“某某堂”

  - [府号](../Page/府号.md "wikilink")，古代官府的名号

  - [斋号](../Page/斋号.md "wikilink")、[轩号](../Page/轩号.md "wikilink")、[亭号](../Page/亭号.md "wikilink")，文人雅士为学堂、书斋、画斋、楼阁、园亭等取的名号，自称为“[雅号](../Page/雅号.md "wikilink")”

  - [商号](../Page/商号.md "wikilink")、[宝号](../Page/宝号.md "wikilink")，旧时商店的代称，自称为“本号”

  - [银号](../Page/银号.md "wikilink")，旧时钱庄的代称，自称为“本号”

  - [国号](../Page/国号.md "wikilink")，国家[政权或](../Page/政权.md "wikilink")[朝代的正式名称](../Page/朝代.md "wikilink")，称“某某国”

  - [屋号](../Page/屋号.md "wikilink")、[家号](../Page/家号.md "wikilink")，日本传统的家族名号，称“某某屋”、“某某家”

  - [山号](../Page/山号.md "wikilink")，佛教寺院称“某山某寺”

## 号码

“号”用来表示数目、数字、数量或号码：

  - [番號](../Page/番號.md "wikilink")、[編號](../Page/编码方案.md "wikilink")，编排依次序称“第几号”

  - [门号](../Page/门牌号码.md "wikilink")，门牌称“某路某号”、“某街某号”、“几楼几号”

  - [地号](../Page/地号.md "wikilink")，番地称“某段某号”

  - [代号](../Page/代号.md "wikilink")

  - [型号](../Page/型号.md "wikilink")

  - [证号](../Page/身份证.md "wikilink")，身份证、通行证或许可证的号码

  - [日期](../Page/日期.md "wikilink")，日期称“某月某号”

  - [鞋号](../Page/鞋号.md "wikilink")

  -
  - [杂志发行刊数](../Page/杂志.md "wikilink")，杂志称“某期某号”

## 交通工具

  - 交通工具的编号或注册号码
      - [车号](../Page/車輛號牌.md "wikilink")、[底盤編號](../Page/底盤編號.md "wikilink")、[車輛識別號碼](../Page/車輛識別號碼.md "wikilink")
      - [舷號](../Page/舷號.md "wikilink")、
      - [飞机编号](../Page/航空器註冊編號.md "wikilink")
  - [列车的名称](../Page/列车.md "wikilink")，列车称“某某号”
  - [船舰的名称](../Page/船舰.md "wikilink")，船舰称“某某号”

## 乐器

在乐器学中，能够吹出响亮声音的乐器称为“[号](../Category/铜管乐器.md "wikilink")”：

  - [大号](../Page/大号.md "wikilink")
  - [小号](../Page/小号.md "wikilink")
  - [长号](../Page/长号.md "wikilink")
  - [柔音號](../Page/柔音號.md "wikilink")
  - [短号](../Page/短号.md "wikilink")
  - [号角](../Page/号角.md "wikilink")
  - [号笛](../Page/号笛.md "wikilink")
  - [军号 (乐器)](../Page/军号_\(乐器\).md "wikilink")
  - [螺号](../Page/螺号.md "wikilink")
  - [牛角号](../Page/牛角号.md "wikilink")
  - 其他铜管或号角状乐器

## 人声

人口中发出响亮的声音称为“号”：

  - [口号](../Page/口号.md "wikilink")，喊标语
  - [号子](../Page/行军歌曲.md "wikilink")，行军的步调
  - [哀号](../Page/痛苦.md "wikilink")、[哭号](../Page/哭.md "wikilink")、[干号](../Page/喊.md "wikilink")，《水浒传》第二十五回：“原来但凡世上妇人哭有三样：有泪有声谓之哭，有泪无声谓之泣，无泪有声谓之号。”

## 讯息或信号

  - [旗号](../Page/讯號旗.md "wikilink")，利用旗帜传递的讯号
  - [军号](../Page/军号.md "wikilink")，利用号角传递的讯号
  - [火号](../Page/火焰信号.md "wikilink")，利用烟火传递的讯号
  - [呼号](../Page/呼号.md "wikilink")，无线电通信的广播呼号
  - [警号](../Page/警号.md "wikilink")，利用警笛、警钟、鼓、哨子等发出的警示讯号
  - [暗号](../Page/暗号.md "wikilink")，秘密讯息或暗码
  - [政号](../Page/政号.md "wikilink")、[号令](../Page/号令.md "wikilink")，口头或文件发出的命令

## 符号或记号

  - [符号](../Page/符号.md "wikilink")
  - [记号](../Page/记号.md "wikilink")

## 代称

“号”用作量词或代称，表示多少单位人数：

  - [病号](../Page/病患.md "wikilink")
  - [伤号](../Page/受伤.md "wikilink")
  - [囚号](../Page/囚犯.md "wikilink")

## 參見

  -
  -