**喉音R**（Guttural
R、[法语R](../Page/法语.md "wikilink")）指在[语音学中](../Page/语音学.md "wikilink")，将[音位](../Page/音位.md "wikilink")[R作为喉辅音发出](../Page/R.md "wikilink")。这些辅音中常见的是**[小舌音R](../Page/小舌音.md "wikilink")**，另外还有**[软腭音R](../Page/软腭音.md "wikilink")**、**[咽音R](../Page/咽音.md "wikilink")**或**[声门音R](../Page/聲門音.md "wikilink")**。有一些语言的使用者将[齿龈颤音](../Page/齿龈颤音.md "wikilink")/r/和[小舌颤音](../Page/小舌颤音.md "wikilink")/ʀ/作为同一个音位的等价音素。然而，值得注意的是，这两种音的发音方法十分不同。

## 喉音R语言

喉音R现在在[法国](../Page/法国.md "wikilink")、[比利时](../Page/比利时.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[德国北部和](../Page/德国.md "wikilink")[丹麦常作为辅音字母](../Page/丹麦.md "wikilink")[R的发音](../Page/R.md "wikilink")。这个辅音字母在世界许多其它地方都有，然而许多地方都不能用它来代替更为常用的[齿龈颤音](../Page/齿龈颤音.md "wikilink")/r/和[齿龈近音](../Page/齿龈近音.md "wikilink")/ɹ/。

### 罗曼语

#### 法语

[法语可谓是使用喉音R的最著名的例子](../Page/法语.md "wikilink")。标准法语是发成[浊小舌擦音](../Page/濁小舌擦音.md "wikilink")//，但是如果R是在濁輔音/p/，/t/，/k/，/s/，/f/，/ʃ/前面或後面，它變成[清小舌擦音](../Page/清小舌擦音.md "wikilink")//。魁北克法语中，傳統是发成[齒齦顫音](../Page/齒齦顫音.md "wikilink")//，但是現代是发成//。

与之毗邻的[布列塔尼地区使用的](../Page/布列塔尼.md "wikilink")[布列塔尼语](../Page/布列塔尼语.md "wikilink")，虽然不是[罗曼语](../Page/罗曼语.md "wikilink")，但曾经受法语很大影响，也在某些地区保持了齿龈颤音R。

#### 葡萄牙语

音素，音节开头写作“r”，元音间作“rr”，原来跟意大利语和西班牙语一样是齿龈颤音，现在已经产生了许多变体。在欧洲和非洲，通常发成[浊小舌擦音](../Page/濁小舌擦音.md "wikilink")以及[齿龈颤音](../Page/齿龈颤音.md "wikilink")。在巴西，通常是[清软腭擦音](../Page/清软腭擦音.md "wikilink")（如西班牙语的
*j*amas），或者是[清声门擦音](../Page/清聲門擦音.md "wikilink")。

在葡萄牙语中，在单词中或音节末尾位置的r，通常发为[齿龈闪音](../Page/齒齦閃音.md "wikilink")，但在巴西葡萄牙语中，根据方言不同有多种变体：

1.  [清軟顎擦音](../Page/清軟顎擦音.md "wikilink") ；
2.  [清聲門過渡音](../Page/清聲門過渡音.md "wikilink") ；
3.  [齿龈闪音](../Page/齿龈闪音.md "wikilink") ；
4.  [齿龈颤音](../Page/齿龈颤音.md "wikilink") ；
5.  [齿龈近音](../Page/齿龈近音.md "wikilink") （即英语的r）。

也可能在重读末音节不发音。头两种发音方法最被广泛认可。

### 西日尔曼地区

传统上，现今[荷兰](../Page/荷兰.md "wikilink")、[德国](../Page/德国.md "wikilink")、[瑞士和](../Page/瑞士.md "wikilink")[奥地利的语言按照](../Page/奥地利.md "wikilink")[莱茵河的下](../Page/莱茵河.md "wikilink")、中、上游划分。低地口音进化成[荷兰语和](../Page/荷兰语.md "wikilink")[低地德语方言](../Page/低地德语.md "wikilink")，发展出小舌R。上游的方言发生了[辅音变迁](../Page/辅音变迁.md "wikilink")，使这里的语言成为[高地德语](../Page/高地德语.md "wikilink")，进一步还可以分为中地和高地方言。在瑞士和奥地利保留了[齿龈颤音](../Page/齿龈颤音.md "wikilink")/r/，德国中部的中地方言也发展出小舌R。小舌R在这些地域的发展还没有完全明白，流行的观点是这些语言由于受[法语的影响借用了其小舌R](../Page/法语.md "wikilink")。但事实上本身法语中小舌R的产生还未完全了解。

与[英语和](../Page/英语.md "wikilink")[苏格兰语相近的](../Page/苏格兰语.md "wikilink")[弗里西语](../Page/弗里西语.md "wikilink")，尽管被说喉音R语言的地区包围着，却仍然保留了齿龈颤音R。不过一些方言现在趋向于喉音R。

#### 荷兰语、佛兰德语和南非荷兰语

现代[荷兰语中使用了相当多不同的R音](../Page/荷兰语.md "wikilink")。[比利时的](../Page/比利时.md "wikilink")[:en:Flemish语中](../Page/:en:Flemish.md "wikilink")，通常R是[齿龈颤音](../Page/齿龈颤音.md "wikilink")，但小舌R也存在，主要在[Limburg省](../Page/:en:Limburg\(Belgium\).md "wikilink")、[Ghent附近区域以及](../Page/Ghent.md "wikilink")[布魯塞爾](../Page/布魯塞爾.md "wikilink")。在[荷兰](../Page/荷兰.md "wikilink")，小舌R在南部省份[Noord-Brabant和](../Page/:en:North_Brabant.md "wikilink")[林堡是主导的R音](../Page/林堡.md "wikilink")。国内的其它部分情况比较复杂。小舌R较为普遍，但不处于主导地位。在西部地区[:en:Randstad](../Page/:en:Randstad.md "wikilink")，包括的城市有[鹿特丹](../Page/鹿特丹.md "wikilink")、[海牙和](../Page/海牙.md "wikilink")[乌德乐支](../Page/乌德乐支.md "wikilink")（[阿姆斯特丹口音却通常是齿龈R](../Page/阿姆斯特丹.md "wikilink")）。小舌R也见于Randstad地区以外的主要城市，如[Zwolle](../Page/Zwolle.md "wikilink")、[Almelo和](../Page/Almelo.md "wikilink")[Leeuwarden](../Page/Leeuwarden.md "wikilink")。在这些小舌R中心区域之外，[齿龈颤音则变得常用了](../Page/齿龈颤音.md "wikilink")。将荷兰语作为外语的学习者也倾向于使用齿龈颤音，因为这样能够与荷兰语中另外的一个音[清软腭擦音](../Page/清软腭擦音.md "wikilink")/x/（“ch”，“g”）形成对比。[南非荷兰语除了在](../Page/南非荷兰语.md "wikilink")[开普敦郊区用小舌R外](../Page/开普敦.md "wikilink")，也使用齿龈颤音R。

#### 标准德语

大多数[德语方言使用小舌R](../Page/德语.md "wikilink")。尽管在[:en:Theodor
Siebs编撰的第一部标准德语发音字典中规定的是](../Page/:en:Theodor_Siebs.md "wikilink")[小舌颤音](../Page/小舌顫音.md "wikilink")，德国南部和[奥地利的标准德语变体则多使用](../Page/奥地利.md "wikilink")[齿龈颤音](../Page/齿龈颤音.md "wikilink")。
许多变体中，两种发音共存，在音节末尾R通常会[元音化](../Page/元音化.md "wikilink")。

在[瑞士德语](../Page/瑞士德语.md "wikilink")，无论R在音节末尾还是音节开头，都发齿龈颤音。

#### 意地绪语

历史上高低地德语的区分也影响到了犹太人的[意地绪语](../Page/意地绪语.md "wikilink")，尤其是原来在莱茵河高地居住的[德系犹太人和](../Page/德系犹太人.md "wikilink")[犹太人所带的口音](../Page/犹太人.md "wikilink")。当这些犹太人移民到其它地区，如[美国和](../Page/美国.md "wikilink")[俄罗斯后](../Page/俄罗斯.md "wikilink")，他们也带去了其发音。

### 北日尔曼语

#### 丹麦语和瑞典语

在[斯堪的纳维亚占主导地位的R音是](../Page/斯堪的纳维亚.md "wikilink")[齿龈颤音](../Page/齿龈颤音.md "wikilink")，另外对于辅音组合/rd/，/rl/，/rn/，/rs/和/rt/在[挪威和](../Page/挪威.md "wikilink")[瑞典大部使用](../Page/瑞典.md "wikilink")[卷舌音](../Page/卷舌音.md "wikilink")。然而在[丹麦完全将R读成](../Page/丹麦.md "wikilink")[咽浊擦音](../Page/咽浊擦音.md "wikilink")，而在瑞典的[:en:Skåne地区完全将R读成](../Page/:en:Skåne.md "wikilink")[小舌颤音](../Page/小舌颤音.md "wikilink")。在Skåne的瑞典语通常作为[瑞典语的一种方言](../Page/瑞典语.md "wikilink")，尽管由于历史原因，它与海峡另一边的[丹麦语可以互通](../Page/丹麦语.md "wikilink")。丹麦和Skåne的喉音R起源未明，因为在丹麦接收Skåne前，R在这两个地方都是读作[齿龈颤音的](../Page/齿龈颤音.md "wikilink")。

#### 挪威语

挪威大部使用[齿龈闪音](../Page/齿龈闪音.md "wikilink")。然而小舌R在南挪威的西南部逐渐占据优势。小舌R的扩散中心在[卑尔根市](../Page/卑尔根.md "wikilink")，在今天起影响仍在扩展。语言学家认为使用[卷舌音的方言对于小舌R具有天然的免疫力](../Page/捲舌音.md "wikilink")，因此不会使用它。这些方言在南挪威东部和北挪威。估计小舌音最终在挪威会占据所有不使用卷舌R的地域。

### 索布语

在[斯拉夫语中特殊](../Page/斯拉夫语.md "wikilink")，但在其所处地区则比较正常，由于受到德语影响，德国东部的两种[索布语使用](../Page/索布语.md "wikilink")[小舌颤音](../Page/小舌颤音.md "wikilink")。

### 闪族语

#### 阿拉伯语

[阿拉伯语的多数方言保留了字母ر](../Page/阿拉伯语.md "wikilink")（*rāʾ*）的古典读音，读作[齿龈颤音](../Page/齿龈颤音.md "wikilink")/r/或者在一些情况下是[齿龈闪音](../Page/齿龈闪音.md "wikilink")，一些方言则转为使用[小舌颤音](../Page/小舌颤音.md "wikilink")。其中包括：

  - [伊拉克的](../Page/伊拉克.md "wikilink")[摩苏尔方言](../Page/摩苏尔.md "wikilink")
  - [巴格达的](../Page/巴格达.md "wikilink")[Christian口音](../Page/Christian.md "wikilink")
  - [阿尔及尔的](../Page/阿尔及尔.md "wikilink")[犹太口音](../Page/犹太.md "wikilink")

#### 希伯来语

[希伯来语](../Page/希伯来语.md "wikilink")，辅音ר（*rêš*）的经典读音是[齿龈闪音](../Page/齿龈闪音.md "wikilink")//，并且在[语法上认为是非](../Page/语法.md "wikilink")[双辅音音素](../Page/双辅音.md "wikilink")。国外的[犹太人说的多数希伯来语方言保持使用闪音或者](../Page/犹太人.md "wikilink")[齿龈颤音](../Page/齿龈颤音.md "wikilink")/r/。然而北欧的德系犹太人使用[小舌R](../Page/小舌.md "wikilink")，可以是[小舌颤音](../Page/小舌颤音.md "wikilink")//或[浊小舌擦音](../Page/濁小舌擦音.md "wikilink")//。这也许是来源于他们的母语[意地绪语](../Page/意地绪语.md "wikilink")，而在礼拜时说的希伯来语也带有口音。

#### 意地绪语的影响

尽管生活在[沙皇](../Page/沙皇.md "wikilink")[俄国](../Page/俄国.md "wikilink")，[錫安主義](../Page/錫安主義.md "wikilink")[艾利澤·本-耶胡達德系犹太人的标准](../Page/艾利澤·本-耶胡達.md "wikilink")[希伯来语原来是基于在](../Page/希伯来语.md "wikilink")[西班牙的西班牙犹太方言](../Page/西班牙.md "wikilink")，使用的是齿龈颤音R。但当犹太人第一波移民到[耶路撒冷时](../Page/耶路撒冷.md "wikilink")，他们借用了[意地绪语的小舌发音](../Page/意地绪语.md "wikilink")，渐渐这变成为标准希伯来语的公認发音。现代[以色列的犹太人祖先来自世界各地](../Page/以色列.md "wikilink")，但几乎全部人都使用小舌R，基於该音在现代的权威性已逐步建立、和历史上的成因。

#### 以色列希伯来语

许多移民到以色列的犹太人在原来的国家讲[阿拉伯语](../Page/阿拉伯语.md "wikilink")，因此将希伯来语的R读作[齿龈颤音](../Page/齿龈颤音.md "wikilink")，如阿拉伯语字母ر（*rāʼ*）。
在大融合的压力下，许多人退为将希伯来R读成阿拉伯语的
غ（*ġayn*），这个字母就本身读作[浊小舌擦音](../Page/濁小舌擦音.md "wikilink")。然而，在现代的[塞法迪犹太人和](../Page/塞法迪犹太人.md "wikilink")[米兹拉希犹太人诗歌和民歌中](../Page/米兹拉希犹太人.md "wikilink")，仍然有使用[齿龈颤音](../Page/齿龈颤音.md "wikilink")。

## 假喉音R

有些语言有一个拼做[R的](../Page/R.md "wikilink")[喉辅音](../Page/喉辅音.md "wikilink")，但许多都是由于方便起见而使用的，历史上不是从[齿龈颤音](../Page/齿龈颤音.md "wikilink")/r/音素变来。由于这样的原因，这些音素不认为是“真”喉音R。

### 格陵兰语

[格陵兰语方言将他们的](../Page/格陵兰语.md "wikilink")[浊小舌擦音翻译到](../Page/濁小舌擦音.md "wikilink")[拉丁字母时写作R](../Page/拉丁字母.md "wikilink")。格陵兰语R是与Q（[清小舌塞音](../Page/清小舌塞音.md "wikilink")）对应的[浊辅音](../Page/浊辅音.md "wikilink")。之所以用字母R来表示只是为了方便起见，以便与欧洲使用的小舌R联系起来。

## 托爾金

[托爾金写作的科幻小说中包含了许多的](../Page/约翰·罗纳德·瑞尔·托尔金.md "wikilink")[语言学细节](../Page/语言学.md "wikilink")，也表达了他对于[优美和丑陋语言的](../Page/优美.md "wikilink")[哲学思考](../Page/哲学.md "wikilink")。托爾金个人不喜欢喉辅音，所以在他构造的精灵语中就完全没有用到[小舌音以及](../Page/小舌音.md "wikilink")[浊软腭擦音](../Page/浊软腭擦音.md "wikilink")，但仍保留了其它[软腭音](../Page/软腭音.md "wikilink")。所以顺理成章，他在其“暗黑”语系，例如[半兽人使用的](../Page/半獸人.md "wikilink")[暗黑语就利用了](../Page/暗黑语.md "wikilink")“丑陋的”[浊软腭擦音和](../Page/浊软腭擦音.md "wikilink")[小舌音R](../Page/小舌音.md "wikilink")。然而他还是最终决定不完全妖魔化这个小舌音，于是将其也用在矮人语中。作为对比，他规定在[精灵语中总是要把R读成](../Page/精灵语.md "wikilink")“华丽的”[齿龈颤音](../Page/齿龈颤音.md "wikilink")。

这些规则在托爾金小说的电影和动画版本中并没有严格遵守。具体举例说，[Rankin-Bass的](../Page/Rankin-Bass.md "wikilink")[The
Hobbit](../Page/The_Hobbit.md "wikilink")[动画版本中的精灵使用的是](../Page/动画.md "wikilink")[浊软腭擦音](../Page/浊软腭擦音.md "wikilink")。另外在[Peter
Jackson的](../Page/Peter_Jackson.md "wikilink")[魔戒三部曲中](../Page/魔戒.md "wikilink")，半兽人和[Uruk-hai说的是](../Page/Uruk-hai.md "wikilink")[伦敦口音的](../Page/伦敦.md "wikilink")[齿龈闪音和](../Page/齿龈闪音.md "wikilink")[卷舌音](../Page/捲舌音.md "wikilink")。当然，在[魔戒中的精灵们还是尝试着发出了齿龈颤音R](../Page/魔戒.md "wikilink")。

## 參考文獻

  - [Unicode reference for
    IPA](http://www.phon.ucl.ac.uk/home/wells/ipa-unicode.htm#alfa)

  -
  -
  -
## 參見

  - [聲門音](../Page/聲門音.md "wikilink")
  - [小舌音](../Page/小舌音.md "wikilink")
  - [R音](../Page/R音_\(非邊音流音\).md "wikilink")(Rhotic consonant)

## 外部連結

  - [Unicode reference for
    IPA](http://www.phon.ucl.ac.uk/home/wells/ipa-unicode.htm#alfa)
  - [Article on the pronunciation of R in
    French](https://web.archive.org/web/20150427111402/http://pronouncefrench.net/consonants/pronounciation-of-french-r/)

[Category:語音學](../Category/語音學.md "wikilink")