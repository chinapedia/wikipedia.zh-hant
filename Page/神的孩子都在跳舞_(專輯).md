《**神的孩子都在跳舞**》（****），是台灣搖滾樂團[五月天的第五張專輯](../Page/五月天.md "wikilink")，專輯發行因為剛好也配合他們團名，專輯發行時還特地在專輯標上「五月天五周年」，表示這是他們五月天成立之後的第五張專輯。

他們也在相隔四年後，重回第二張專輯《[愛情萬歲](../Page/愛情萬歲_\(五月天專輯\).md "wikilink")》時位於[日本](../Page/日本.md "wikilink")[富士山下河口湖的錄音室製作此專輯](../Page/富士山.md "wikilink")，整張專輯五人都出現一段創作爆發期，把對事物的感動換化成[音樂](../Page/音樂.md "wikilink")、[文字](../Page/文字.md "wikilink")，最後從中選出了12首歌曲為此專輯的原創音樂密碼。專輯名稱是以知名[小說家](../Page/小說家.md "wikilink")[村上春樹的同名小說為命名](../Page/村上春樹.md "wikilink")。

為配合2005年的「當我們混在一起」巡迴演唱會，在12月17日推出「神誕快樂跨年特典限量版」，也入圍2005年第16屆[金曲獎最佳樂團獎](../Page/金曲獎.md "wikilink")。

## 曲目

<table>
<tbody>
<tr class="odd">
<td><p><strong>曲序</strong></p></td>
<td><p><strong>曲名</strong></p></td>
<td><p><strong>作詞</strong></p></td>
<td><p><strong>作曲</strong></p></td>
<td><p><strong>時間長度</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p>01</p></td>
<td><p>孫悟空</p></td>
<td><p>阿信</p></td>
<td><p>怪獸</p></td>
<td><p>4:12</p></td>
<td><p>動畫:紅孩兒-決戰火焰山主題曲</p></td>
</tr>
<tr class="odd">
<td><p>02</p></td>
<td><p>倔強</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>4:21</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>03</p></td>
<td><p>垃圾車</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>4:16</p></td>
<td><p>台語歌曲</p></td>
</tr>
<tr class="odd">
<td><p>04</p></td>
<td><p>小護士</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>2:43</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>05</p></td>
<td><p>讓我照顧你</p></td>
<td><p>阿信</p></td>
<td><p>怪獸</p></td>
<td><p>3:59</p></td>
<td><p>國泰世華銀行廣告代言曲</p></td>
</tr>
<tr class="odd">
<td><p>06</p></td>
<td><p>約翰藍儂</p></td>
<td><p>阿信<br />
瑪莎</p></td>
<td><p>怪獸</p></td>
<td><p>3:25</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>07</p></td>
<td><p>回來吧</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>4:30</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>08</p></td>
<td><p>錯錯錯</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>3:45</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>09</p></td>
<td><p>晚安 地球人</p></td>
<td><p>阿信</p></td>
<td><p>石頭</p></td>
<td><p>4:08</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>超人</p></td>
<td><p>阿信</p></td>
<td><p>阿信<br />
冠佑</p></td>
<td><p>4:28</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>神的孩子都在跳舞</p></td>
<td></td>
<td><p>石頭</p></td>
<td><p>0:38</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>聖誕夜驚魂</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>2:43</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>垃圾車（朋友版）</p></td>
<td><p>阿信</p></td>
<td><p>阿信</p></td>
<td><p>4:03</p></td>
<td><p>台語歌曲<br />
全團合唱版本<br />
中華電信emome廣告主打</p></td>
</tr>
</tbody>
</table>

## 唱片版本

| 專輯名稱         | 類型     | 發行日期        | 附註             |
| ------------ | ------ | ----------- | -------------- |
| 神的孩子都在跳舞     | CD+VCD | 2004年11月5日  | 附贈年曆海報(預購版)    |
| 神的孩子都在跳舞     | CD+VCD | 2004年11月5日  |                |
| 神的孩子都在跳舞(新版) | CD+VCD | 2004年12月17日 |                |
| 神的孩子都在跳舞     | LP     | 2015年3月20日  | 180克限量珍藏翠綠彩膠唱片 |

## 音樂錄影帶

<table>
<thead>
<tr class="header">
<th><p>歌名</p></th>
<th><p>導演</p></th>
<th><p>首播日期</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/倔強.md" title="wikilink">倔強</a></p></td>
<td></td>
<td><p>2012年4月17日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 獎項

  - [第16屆金曲獎](../Page/第16屆金曲獎.md "wikilink")

<table style="width:80%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>獎項</p></th>
<th><p>入圍者</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/最佳樂團獎_(金曲獎).md" title="wikilink">最佳樂團獎</a></p></td>
<td><p><a href="../Page/五月天.md" title="wikilink">五月天</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - **2004年度 [中華音樂人交流協會](../Page/中華音樂人交流協會.md "wikilink") 十大單曲暨專輯**

<table style="width:56%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 17%" />
<col style="width: 17%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>獎項</p></th>
<th><p>入圍者</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中華音樂人交流協會 2004年度十大專輯</p></td>
<td><p><strong>《神的孩子都在跳舞》</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中華音樂人交流協會 2004年度十大單曲</p></td>
<td><p>〈倔強〉</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [《神的孩子都在跳舞》日本資訊](http://www.oricon.co.jp/prof/artist/276532/products/music/581997/1/)

[F](../Category/五月天音樂專輯.md "wikilink")
[F](../Category/台灣流行音樂專輯.md "wikilink")
[F](../Category/台灣流行搖滾音樂專輯.md "wikilink")
[F](../Category/2004年音樂專輯.md "wikilink")
[F](../Category/滾石唱片音樂專輯.md "wikilink")
[F](../Category/中華音樂人交流協會十大單曲獲獎作品.md "wikilink")
[F](../Category/中華音樂人交流協會十大專輯獲獎作品.md "wikilink")