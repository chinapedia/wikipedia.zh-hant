[微分几何中](../Page/微分几何.md "wikilink")，**曲率形式**（）描述了[主丛上的](../Page/主丛.md "wikilink")[联络的](../Page/联络形式.md "wikilink")[曲率](../Page/曲率.md "wikilink")。它可以看作是[黎曼几何中的](../Page/黎曼几何.md "wikilink")[曲率张量的替代或是推广](../Page/曲率张量.md "wikilink")。

## 定义

令 *G* 为一个[李群](../Page/李群.md "wikilink")，记 *G*
的[李代数为](../Page/李代数.md "wikilink") \(g\)。设 \(E\to B\)
为一个[主 *G*-丛](../Page/主丛.md "wikilink")。令 \(\omega\) 表示 *E*
上一个[埃雷斯曼联络](../Page/埃雷斯曼联络.md "wikilink")（它是一个*E*上的 *g*-值
[1-形式](../Page/微分形式.md "wikilink")）。

那么**曲率形式**就是 *E* 上的 *g*-值 2-形式，定义为

\[\Omega=d\omega +{1\over 2}[\omega,\omega]=D\omega.\]

这里 \(d\) 表示标准[外导数](../Page/外导数.md "wikilink")，\([*,*]\)
是[李括号](../Page/李括号.md "wikilink")，而 *D*
表示[外共变导数](../Page/外共变导数.md "wikilink")。或者说

\[\Omega(X,Y)=d\omega(X,Y) +[\omega(X),\omega(Y)].\]

### 向量丛上的曲率形式

若 \(E\to B\)
是一个[纤维丛](../Page/纤维丛.md "wikilink")，其[结构群为](../Page/结构群.md "wikilink")
*G*，我们可以在[相伴的主](../Page/相伴丛.md "wikilink") *G*-丛上重复同样的定义。

若 \(E\to B\) 是一个向量丛则我们可以把 \(\omega\) 看作是 1-形式的矩阵，则上面的公式取如下形式：

\[\Omega=d\omega +\omega\wedge \omega,\]

其中 \(\wedge\) 是[楔积](../Page/楔积.md "wikilink")。更准确地讲，若 \(\omega^i_j\) 和
\(\Omega^i_j\) 分别代表 \(\omega\) 和 \(\Omega\) 的分量（所以每个 \(\omega^i_j\)
是一个通常的 1-形式而每个 \(\Omega^i_j\) 是一个普通的2-形式），则

\[\Omega^i_j=d\omega^i_j +\sum_k \omega^i_k\wedge\omega^k_j.\]

例如，[黎曼流形的](../Page/黎曼流形.md "wikilink")[切丛](../Page/切丛.md "wikilink")，我们有
\(O(n)\) 作为结构群而 \(\Omega^{}_{}\) 是在 \(o(n)\) 中取值的
2-形式（给定[标准正交基](../Page/标准正交基.md "wikilink")，可以视为反对称矩阵）。在这种情况，\(\Omega^{}_{}\)
是[曲率张量的一种替换表述](../Page/曲率张量.md "wikilink")，也就是在曲率张量的标准表示中，我们有

\[R(X,Y)Z=\Omega^{}_{}(X\wedge Y)Z.\]

上式使用了黎曼曲率张量标准记号。

## 比安基恒等式

如果 \(\theta\) 是标架丛上的典范向量值 1-形式，联络形式 ω 的[挠率](../Page/挠率形式.md "wikilink")
\(\Theta\) 是由结构方程定义的向量值 2-形式：

\[\Theta=d\theta + \omega\wedge\theta = D\theta,\]

这里 *D* 代表[外共变导数](../Page/外共变导数.md "wikilink")。

第一比安基恒等式（对于[标架丛的有挠率联络](../Page/标架丛.md "wikilink")）取以下形式

\[D\Theta=\Omega\wedge\theta={1\over 2}[\Omega,\theta]\ ,\]

第二比安基恒等式对于一般有联络的丛成立，并有如下形式

\[D\Omega=0\ .\]

## 参看

  - [联络 (主丛)](../Page/联络_\(主丛\).md "wikilink")
  - [陈-西蒙斯形式](../Page/陈-西蒙斯形式.md "wikilink")（Chern-Simons form）
  - [黎曼流形的曲率](../Page/黎曼流形的曲率.md "wikilink")
  - [规范场论](../Page/规范场论.md "wikilink")

## 参考

  - [S.Kobayashi](../Page/小林昭七.md "wikilink") and
    [K.Nomizu](../Page/野水克己.md "wikilink"), "Foundations of
    Differential Geometry", Chapters 2 and 3, Vol.I, Wiley-Interscience.

[Q](../Category/曲率.md "wikilink")