**Audacity**是一款[跨平台的音频编辑软件](../Page/跨平台.md "wikilink")，用於錄音和編輯音訊，是[自由](../Page/自由軟體.md "wikilink")、[開放原始碼的軟體](../Page/開放原始碼.md "wikilink")。可在[Mac
OS X](../Page/Mac_OS_X.md "wikilink")、[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[GNU/Linux和其它](../Page/GNU/Linux.md "wikilink")[作業系統上運作](../Page/作業系統.md "wikilink")。Audacity在2004年7月獲選為[SourceForge.net當月最佳推薦專案](../Page/SourceForge.net.md "wikilink")\[1\]，2007年7月選多媒體類的SourceForge.net
2007社区选择大奖\[2\]。

## 特色

Audacity的特色：

  - [多語](../Page/多語.md "wikilink")[使用者介面](../Page/使用者介面.md "wikilink")（切換時必須重新開啟软件）
  - 匯入與匯出[WAV](../Page/WAV.md "wikilink")、[MP3](../Page/MP3.md "wikilink")、[Ogg
    Vorbis或者其他的聲音檔案格式](../Page/Ogg_Vorbis.md "wikilink")
      - 從1.3Beta開始支援[MP4](../Page/MP4.md "wikilink")、[MOV](../Page/MOV.md "wikilink")、[WMA](../Page/WMA.md "wikilink")、[M4A](../Page/M4A.md "wikilink")、[AC3檔](../Page/AC3.md "wikilink")。
  - 錄音與放音
  - 對聲音做剪下、複製、貼上（可復原無限次數）
  - 多[音軌混音](../Page/音軌.md "wikilink")
  - 數位效果與[外掛程式](../Page/外掛程式.md "wikilink")
  - [波封編輯](../Page/波封.md "wikilink")
  - 雜音消除
  - 支援[多聲道模式](../Page/多聲道.md "wikilink")，[取樣頻率最高可至](../Page/取樣頻率.md "wikilink")96
    kHz，每個取樣點可以以24 bits表示
  - 支援[Nyquist](../Page/Nyquist.md "wikilink")[程式語言](../Page/程式語言.md "wikilink")，讓您自行撰寫效果器
  - 對聲音檔進行切割。（1.3 beta及更高版本）

## 語言支援

Audacity內置了志願者幫助翻譯的超過60種的界面語言，儘管其中一部分翻譯並不完全。

除了英語幫助外，另有供給其他諸多語言的ZIP幫助檔案可供下載。

Audacity網站也提供數個語種的教程。\[3\]

## 接受情況

[CNET給Audacity打](../Page/CNET.md "wikilink")5/5顆星，並稱其「特性豐富而靈活」。\[4\][PC
World的Preston](../Page/PC_World.md "wikilink")
Gralla稱，「如若你對製作音樂、編輯音樂及混音感興趣，那你會想要Audacity。」\[5\][Tech
Republic的Jack](../Page/Tech_Republic.md "wikilink")
Wallen強調了它的特性和易用性。\[6\]的Michael
Muchmore給其打3.5/5顆星，並說：「儘管不如Adobe、Sony、M-Audio旗下同類產品那樣靈巧，但Audacity在免費軟體中可說是特性萬分全面的了。」\[7\]

## 局限

  - 僅支援32位[VST音訊效果插件](../Page/VST.md "wikilink")，而不能支援64位及樂器VST插件。\[8\]
  - 缺少動態均衡器、實時特效和對擦除的支援。\[9\]
  - [MIDI檔只可顯示](../Page/MIDI.md "wikilink")。\[10\]
  - 不能直接支援WMA、AAC及其他的所有權或有限制的檔案格式。要輸入或輸出AC3、AMR
    (NB)、WMA等所有權格式，需要可選的FFmpeg庫。\[11\]

## 參考資料

## 使用教學

  - [Audacity
    Wiki](https://web.archive.org/web/20060324202704/http://audacityteam.org/wiki/index.php?title=Main_Page)
  - [Audacity Manual
    Contents](https://web.archive.org/web/20151029101416/http://manual.audacityteam.org/o/)
  - [Audacity使用手冊](http://netlab.kh.edu.tw/document/Knoppix/audacity-%E9%82%B1%E5%BF%97%E5%BF%A0/Audacity.html)
  - [消除人聲](http://catho7.blogspot.com/2006/03/audacity_16.html)
  - [改變播放速度](http://catho7.blogspot.com/2006/05/audacity.html)
  - [Audacity的使用教學](http://inote.tw/2006/09/audacity.html)

## 外部連結

  - [Audacity: Free Audio Editor and
    Recorder](http://audacity.sourceforge.net)
  - [Audacity简体中文版主页](http://audacity.sourceforge.net/?lang=zh-CN)
  - [Audacity正體中文版主頁](http://audacity.sourceforge.net/?lang=zh-TW)
  - [Audacity
    Portable主頁](http://portableapps.com/apps/music_video/audacity_portable)

[Category:音樂軟體](../Category/音樂軟體.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")
[Category:使用GPL许可证的软件](../Category/使用GPL许可证的软件.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")

1.  [Project of the Month](http://sourceforge.net/potm/potm-2004-07.php)
2.  [2007 Community Choice
    Awards](http://sourceforge.net/community/index.php/landing-pages/cca07/)
3.
4.
5.
6.
7.
8.
9.
10.
11.