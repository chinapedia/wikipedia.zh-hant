**哥伦布**（
）是[美国](../Page/美国.md "wikilink")[俄亥俄州的州府](../Page/俄亥俄州.md "wikilink")，它于1812年建城，位于[塞奥托河与](../Page/塞奥托河.md "wikilink")[奥兰滕吉河的聚会处](../Page/奥兰滕吉河.md "wikilink")。哥伦布从1816年开始成為俄亥俄州州府，現為[美國第15大城市](../Page/按人口排列的美國城市列表.md "wikilink")。

哥伦布几乎位于俄亥俄州的地理中心，它还是[富兰克林县政府所在地](../Page/富兰克林县_\(俄亥俄州\).md "wikilink")，不过它城市的部分地区也延伸入[特拉华县和](../Page/特拉华县_\(俄亥俄州\).md "wikilink")[費爾菲爾德縣](../Page/費爾菲爾德縣_\(俄亥俄州\).md "wikilink")。

## 历史

在塞奥托河与奥兰滕吉河的聚会处附近地区有许多[造墩人的遗迹](../Page/造墩人.md "wikilink")。在哥伦布内市附近就有一座很大的美洲原著人墓墩。欧洲探险家开始进入[伊利湖地区时这些古代文化早已消失了](../Page/伊利湖.md "wikilink")。当时当地居住的土著人有[迈阿密部落](../Page/迈阿密部落.md "wikilink")、[德瓦拉族](../Page/德瓦拉族.md "wikilink")、[威恩达特族](../Page/威恩达特族.md "wikilink")、[肖尼族和](../Page/肖尼族.md "wikilink")等。这些部落抵抗美国的扩张，造成了长年残酷的战斗。在[鹿寨战役中美国终于战胜](../Page/鹿寨战役.md "wikilink")，此后签署的[格林维尔条约为新的殖民打开了大门](../Page/格林维尔条约.md "wikilink")。1797年一个来自[弗吉尼亚州的勘测人在塞奥托河西岸建立了一个居民点](../Page/弗吉尼亚州.md "wikilink")。由于他非常喜欢[本傑明·富蘭克林](../Page/本傑明·富蘭克林.md "wikilink")，他就将这个居民点命名为富兰克林。

### 19世纪

据1803年州政府文献记录由于州内领导人物之间的政治斗争以及一系列的大火俄亥俄州的州府从[奇利科西县移到](../Page/奇利科西县.md "wikilink")[赞斯维尔县](../Page/赞斯维尔县.md "wikilink")，后来又搬回奇利科西。最后州立法机构决定在州的地理中心位置建立一座新的州府作为折衷方案。众多俄亥俄州的小镇竞争成为州府，但是最后一群土地投机商向[俄亥俄州议会提出了一个非常引人的交易](../Page/俄亥俄州议会.md "wikilink")。1812年2月州府设立在富兰克林的对岸，以[克里斯托弗·哥伦布命名](../Page/克里斯托弗·哥伦布.md "wikilink")。

1831年[美国国道从](../Page/美国国道.md "wikilink")[巴爾的摩通到哥伦布](../Page/巴爾的摩.md "wikilink")，加上运河的完成，导致市内人口剧增。从欧洲到达的移民在市郊建立了两个不同民族的市区。[爱尔兰移民主要定居在市北](../Page/爱尔兰.md "wikilink")，[德国移民则获取市南比较便宜的土地](../Page/德国.md "wikilink")。哥伦布的德国移民在市内建立了许多啤酒厂、[信義宗神学院](../Page/信義宗.md "wikilink")、[首府大学和美国的第一座](../Page/首府大学.md "wikilink")[幼儿园](../Page/幼儿园.md "wikilink")。

1850年铁路修到哥伦布。到1875年为止已经有八条铁路通到哥伦布，市内建造了一座新的火车站。

1857年1月7日[俄亥俄州府在建造八年后启用](../Page/俄亥俄州府.md "wikilink")。在[南北战争时期哥伦布是北军的一座大营](../Page/南北战争.md "wikilink")，2.6万士兵驻扎在这里，至九千南军战俘曾被关押在此。至今为止两千多南军士兵埋在这里，是北方南军最大的墓地之一。1870年[俄亥俄农业合作化机械学院成立](../Page/俄亥俄州立大学.md "wikilink")。

19世纪末在哥伦布出现了一些大工厂，其中包括20多家马车厂。此时众多啤酒厂也通过合并成为一座。此外市内还有一座钢厂。哥伦布也是工会的一个重要地点。[美国劳工联盟和](../Page/美国劳工联盟.md "wikilink")[美国矿工联合会分别于](../Page/美国矿工联合会.md "wikilink")1886年和1890年在哥伦布成立。

### 20世纪

20世纪初哥伦布市内的大街上有数十个金属拱，因此它获得了拱城的昵称。本来这些拱被用来照明，后来则使用它们来为[有轨电车提供电力](../Page/有轨电车.md "wikilink")。1913年3月25日富兰克林发大水，39人丧身。为了防止再发生这样的事件，[美国工程兵团建议拓宽塞奥托河](../Page/美国工程兵团.md "wikilink")，建造新的桥梁，并建造大坝。[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，1920年代里哥伦布建筑发展巨大，建成了新的市民中心、[俄亥俄剧院和](../Page/俄亥俄剧院.md "wikilink")[俄亥俄体育场](../Page/俄亥俄体育场.md "wikilink")。

[大萧条对哥伦布的打击不像其它地方那么大](../Page/大萧条.md "wikilink")，哥伦布的经济多样化使得它比起它的“[锈带](../Page/锈带.md "wikilink")”邻居受到的影响小。[第二次世界大战为哥伦布带来了许多新工作](../Page/第二次世界大战.md "wikilink")，它的人口再次剧增。这次的移民主要来自[阿帕拉契农村萧条地区](../Page/阿帕拉契.md "wikilink")，这些移民很快就占据了哥伦布人口三分之一以上。1948年市和县购物中心启用，这个购物中心被看作是第一座美国现代[超大规模购物中心](../Page/超大规模购物中心.md "wikilink")。[州际高速公路的建立也显示着俄亥俄中心地区的市郊化发展](../Page/州际高速公路.md "wikilink")。为了使得城市的税收不受市郊化过程的影响，哥伦布与其市郊地区合并。1990年代初哥伦布在面积和人口上均成为俄亥俄最大的城市。

近十年来试图恢复市中心商业区的企图结果不理想。1970年代里许多老的标志物被拆除来建造高的办公楼和商店，但是市郊的发展阻止了市中心的发展。但是哥伦布依然在努力恢复其市中心。

## 地理

按照[美国人口调查局数据哥伦布面积](../Page/美国人口调查局.md "wikilink")550.5平方公里，其中544.6平方公里是陆地面积，5.9平方公里是水面。与[美国中西部其它城市不同的是哥伦布依然在快速通过与周围县镇合并而扩展](../Page/美国中西部.md "wikilink")。它是美国中西部在面积和人口上增长最迅速的城市。与克里夫蘭和辛辛那堤相比哥伦布周围的市郊城区相对来说比较少。从1950年代开始周围县镇要想从哥伦布获得饮水和废水服务的条件就是与哥伦布合并。这个政策对哥伦布的经济发展有很大的促进作用。

塞奥托河与奥兰滕吉河的聚会处就在哥伦布市中心以西。此外在哥伦布都市地区里还有一些小的支流。哥伦布的地形总的来说比较平坦。在[威斯康星冰川期俄亥俄大部分地区被](../Page/威斯康星冰川期.md "wikilink")[冰川覆盖](../Page/冰川.md "wikilink")，但是在河流附近依然有不少丘陵地形。[落叶树在当地比较多](../Page/落叶树.md "wikilink")，包括[枫树](../Page/枫.md "wikilink")、[栎树](../Page/栎属.md "wikilink")、[橡树](../Page/橡树.md "wikilink")、[核桃](../Page/核桃.md "wikilink")、[胡桃](../Page/胡桃.md "wikilink")、[杨树等](../Page/杨属.md "wikilink")。

## 氣候

哥伦布气候代表美国中西部温带，属[溫帶大陸性濕潤氣候](../Page/溫帶大陸性濕潤氣候.md "wikilink")，四季分明。冬季寒冷，潮湿，日照少，日最高气温低于的平均日数为29天，日最低气温低于的平均日数为19天，低于的有6.7天；夏季炎热潮湿，日最高气温超过的日数年均有44天，以上的有2.6天。\[1\]最冷月（1月）均温，极端最低气温（1994年1月19日）。\[2\]最热月（7月）均温，极端最高气温（1934年7月21日与1936年7月14日）。\[3\]无霜期平均为186天（4月20日至10月22日）；可测量降雪平均期为11月20日至3月31日。\[4\]年均降水量约，年极端最少降水量为（1930年），最多为（2011年）。\[5\]年均降雪量为；1941–42年的降雪量最少，积累降雪量只有，2013–14年的降雪量最多，积累降雪量为。\[6\]

## 市容

哥伦布有一些非常特殊的市区。在其市中心北部有许多艺术馆、饭店、酒店和特殊商店，在这个区的附近又有许多装饰豪华的维多利亚式建筑，被称为维多利亚城。在南边的德国城有许多19世纪的砖建筑，这个区是[國家史跡名錄中最大的私人资助的历史地](../Page/國家史跡名錄.md "wikilink")。市中心西部的富兰克林是哥伦布最老的市区。这个区的许多地方低于塞奥托河与奥兰滕吉河的河面。这些地区必须依靠大坝来防止犯大水。

俄亥俄州立大学校园地区学期里有许多大学生，许多老建筑被改建为大学生公寓。当地有许多专门为大学生需求服务的小吃店、书店等等。

## 人口

按照2000年的人口普查哥伦布有711,470人居民，分301,534户和165,240个家庭。人口密度为1,306.4人／km²。67.93%的居民是白人，24.47%是美国黑人，0.29%是美国原著人，3.44%是亚裔人，0.05%来自太平洋岛屿，1.17%是其它来源，2.65%属于混血儿。2.46%的人是西班牙或拉丁美洲血统。

在301,534户中28.0%有18岁以下的未成年人，36.1%是结婚夫妇，14.5%是带未成年人的没有丈夫的妇女，45.2%不是家庭。34.1%的户是单身汉，7.0%的户中有65岁以上的老年人。平均户含2.30人，平均家庭有3.01人。

市民年龄分布为：24.2%的人小于18岁，14.0%的人在18岁和24岁之间，35.1%的人在25岁和44岁之间，17.9%的人在45和64岁之间，8.9%的人在65岁以上。平均年龄为31岁。女比男的性别比为100:94.6，18岁以上的人的性别比为100:91.9。

市内每户平均收入为37,897美元，家庭的平均收入为47,391美元。男子的平均收入为35,138美元，女子的平均收入为28,705美元。市民的人均收入为20,450美元。约10.8%的家庭、14.8%的市民生活在[贫困线以下](../Page/贫困线.md "wikilink")。18岁以下的未成年人中18.7%生活在贫困线以下，65岁以上的老年人中10.9%生活在贫困线以下。

随着来自拉丁美洲、非洲和亚洲的移民的大量进入，俄亥俄州中部的人口变化很大很快。结合的速度比较低，各个人群建立自己的区。这个现象对社会机构，尤其是公费学校和公共卫生设置施加了巨大的压力。

## 经济

### 公共服务

作为州府哥伦布拥有众多州和城市的政府机关，政府机关是哥伦布最大的雇主。此外俄亥俄州立大学也是重要的雇主。

### 商业

[Columbus-ohio-skyline-panorama.jpg](https://zh.wikipedia.org/wiki/File:Columbus-ohio-skyline-panorama.jpg "fig:Columbus-ohio-skyline-panorama.jpg")
数个美国全国性的和全球性的公司的总部位于哥伦布。[全国相互保险公司的总部位于市中心北部](../Page/全国相互保险公司.md "wikilink")，由多幢楼组成。零售商[有限品牌公司的总部位于城市西部](../Page/有限品牌公司.md "wikilink")。钢铁公司[沃辛顿工业公司的总部位于城市的北部](../Page/沃辛顿工业公司.md "wikilink")。两家快餐连锁店的总部在哥伦布：和[白色城堡](../Page/白色城堡.md "wikilink")。的第一家餐馆现在依然在哥伦布市中心经营，同时它还是一个博物馆。[卡地纳健康的总部在西北的市郊地区](../Page/卡地纳健康.md "wikilink")。[亞伯克朗比及費區的总部位于东北市郊](../Page/亞伯克朗比及費區.md "wikilink")。[史特林商务公司的总部在哥伦布西北的市郊](../Page/史特林商务公司.md "wikilink")。

其它大公司在哥伦布地区也有重要基地。[本田技研工业在这里有两个汽车厂](../Page/本田技研工业.md "wikilink")，其中一个是本田在美国开的第一座厂。[美国第一银行的总部过去在哥伦布](../Page/美国第一银行.md "wikilink")，今天在这里依然有一个大据点。[摩根大通在哥伦布也有一个重要的基地](../Page/摩根大通.md "wikilink")。[百威啤酒在哥伦布市北有一个大啤酒厂](../Page/百威啤酒.md "wikilink")。[麦克劳希儿在市内有一个大办公室](../Page/麦克劳希儿.md "wikilink")。[联合包裹服务公司在市西有一个大的分发中心](../Page/联合包裹服务公司.md "wikilink")。

[化学文摘服务社位于哥伦布](../Page/化学文摘服务社.md "wikilink")，因此哥伦布是世界上最重要的科学信息分布中心之一。[巴特尔研究院也位于哥伦布](../Page/巴特尔研究院.md "wikilink")。

由于哥伦布的人口组成非常杂，收入区分非常大，市内同时有城市、市郊和近似农村居民，因此许多人将哥伦布看作是一个“典型”美国城市。许多零售商和连锁店在这里测试新商品的效果。

#### 飞机制造业

哥伦布国际机场过去是北美飞机厂的基地。在哥伦布制造的飞机包括[F-86佩刀戰鬥機](../Page/F-86佩刀戰鬥機.md "wikilink")、[A-5民團戰略核子攻擊機](../Page/A-5民團戰略核子攻擊機.md "wikilink")、[OV-10北美野马攻击侦察机和](../Page/OV-10北美野马攻击侦察机.md "wikilink")[B-1枪骑兵轰炸机的部件以及许多导弹和导航系统](../Page/B-1枪骑兵轰炸机.md "wikilink")。

## 基础设施

[Columbus-ohio-city-hall.jpg](https://zh.wikipedia.org/wiki/File:Columbus-ohio-city-hall.jpg "fig:Columbus-ohio-city-hall.jpg")

### 政府

市政府由一名市长和一个市议会组成。市议会每两年选举一次，市长指命安全官和公共服务官，财政官、监督人和律师由市民选举产生。目前的市长是[迈克·科曼](../Page/迈克·科曼.md "wikilink")。

### 教育

#### 大學

[俄亥俄州立大学在哥伦布的校园是美国最大的大學校园之一](../Page/俄亥俄州立大学.md "wikilink")。其它位于哥伦布的大學包括[哥伦比亚州立社区学院](../Page/哥伦比亚州立社区学院.md "wikilink")、[富兰克林大学](../Page/富兰克林大学.md "wikilink")、[俄亥俄多米尼加大学](../Page/俄亥俄多米尼加大学.md "wikilink")、[哥伦布艺术与设计学院](../Page/哥伦布艺术与设计学院.md "wikilink")、[俄亥俄卫斯里昂大学](../Page/俄亥俄卫斯里昂大学.md "wikilink")、[首府大学](../Page/首府大学.md "wikilink")、[丹尼森大学](../Page/丹尼森大学.md "wikilink")、[奥特拜因学院和](../Page/奥特拜因学院.md "wikilink")[德锐大学](../Page/德锐大学.md "wikilink")。

#### 初等和中等教育

哥伦布的公共教育主要是幼儿园加上12学年。每个市区的招生范围很广，有时不同学校会在同一地区招生。此外市内还有一些特殊的公共学校，不遵照12学年的秩序。市内还有一些著名的私人学校。

在美国公共教育史上哥伦布有多个首次。哥伦布开办了美国第一个[幼儿园](../Page/幼儿园.md "wikilink")，1909年起办了美国第一个[中学来帮助学生过渡小学到高校之间的变化](../Page/中学.md "wikilink")（当时只有48%的学生在9年级后继续学习）。

1977年联邦法庭判决哥伦布的公共学校中不同人种的分布太不均匀，城市有义务将白人学生分配到黑人为主的学校去。这个判决导致许多白人离开哥伦布，移居市郊，使得市郊的学校受到了巨大的压力，而同时市中心则比过去黑人化和贫困化，市中心的学校往往学生不足，楼房失修，而市郊的学校则不得不增添教师和楼房。这个问题持续至今。

哥伦布的公共学校还吸收了许多移民孩子，其中许多来自拉丁美洲，但也有不少来自非洲和中东的难民。

### 交通

两条重要[州际高速公路在哥伦布交叉](../Page/州际高速公路.md "wikilink")，东西向的[I-70和南北向的](../Page/70号州际公路.md "wikilink")[I-71](../Page/71号州际公路.md "wikilink")。在哥伦布市中心以西有一段约1.5英里长的路段这两条高速公路合并到一起，这段路在尖峰时间里也是最忙的。通过其它高速公路从哥伦布出发可以在两小时内到达俄亥俄州几乎所有地方。
[Columbus-ohio-high-street-night.jpg](https://zh.wikipedia.org/wiki/File:Columbus-ohio-high-street-night.jpg "fig:Columbus-ohio-high-street-night.jpg")

哥伦布的街道安排近似于棋盘状。但是离市中心远处这个棋盘格式并没有很严格地被遵守，尤其在市郊和新区里它们均有自己的规划。

哥伦布没有[地铁或其它轨道系统](../Page/地铁.md "wikilink")。虽然曾有过不同的轻轨计划，但是在近期不会有任何建立这样的系统的计划。不过市内有公共汽车运行。哥伦布市中心的大火车站在1970年代里被拆除了。在2008年12月[鳳凰城开通了轻轨之后](../Page/鳳凰城_\(亞利桑那州\).md "wikilink")，\[7\]哥伦布成为美国没有客运轨道服务的最大[都会区中的城市](../Page/城市群.md "wikilink")。\[8\]哥伦布有四个飞机场，其中最主要的是[哥伦布国际机场](../Page/哥伦布国际机场.md "wikilink")。

## 人物和文化

### 名人

  - [普雷斯科特·布什](../Page/普雷斯科特·布什.md "wikilink")：美国政治家、[乔治·赫伯特·沃克·布什的父亲](../Page/乔治·赫伯特·沃克·布什.md "wikilink")
  - [萨蒙·波特兰·蔡斯](../Page/萨蒙·波特兰·蔡斯.md "wikilink")：美国政治家，曾任俄亥俄州州长、美国财政部长、美国首席大法官

### 地标和博物馆

[Ohio_Statehouse_columbus.jpg](https://zh.wikipedia.org/wiki/File:Ohio_Statehouse_columbus.jpg "fig:Ohio_Statehouse_columbus.jpg")
哥伦布有一些世界一流的建筑，包括仿古希腊式的俄亥俄州府、由[彼得·埃森曼设计的](../Page/彼得·埃森曼.md "wikilink")[维克斯内尔艺术中心和哥伦布会议中心](../Page/维克斯内尔艺术中心.md "wikilink")。

[俄亥俄州府是](../Page/俄亥俄州府.md "wikilink")1839年开始建造的，它的土地是当时哥伦布的地主捐的（约四万平方米）。大厦是由当地的[石灰岩建成的](../Page/石灰岩.md "wikilink")，其地基深五米。据报道石匠工作和地基主要是监狱劳工做的。大厦的大门前有一排简单的[多立克柱式的柱子](../Page/多立克柱式.md "wikilink")。中央有一个由低矮的柱子支撑的圆廊。不像美国许多其它州议会建筑俄亥俄的州府很少模仿[美国国会大楼](../Page/美国国会大楼.md "wikilink")。州府的建筑共用了22年，其中换了七位建筑师。这些建筑师与议会之间的关系不一直很和睦。1857年州府启用，1861年才彻底完工。它位于哥伦布市中心。

[哥伦布艺术博物馆](../Page/哥伦布艺术博物馆.md "wikilink")1931年开幕，其展品集中于欧洲和美国[现代主义早期作用](../Page/现代主义.md "wikilink")。塞奥托河畔的[聖母瑪利亞像是为了纪念克里斯托弗](../Page/聖母瑪利亞.md "wikilink")·哥伦布发现美洲500周年树立的。哥伦布还有一个科学博物馆和一个俄亥俄历史协会的博物馆。

在一定意义上俄亥俄州立大学历史悠久，本身也是一座博物馆。不过它的确拥有一些博物馆和类似博物馆的展览。其中最著名的是展出现代化艺术的维克斯内尔艺术中心

[Columbus-ohio-rhodes-state-office-tower.jpg](https://zh.wikipedia.org/wiki/File:Columbus-ohio-rhodes-state-office-tower.jpg "fig:Columbus-ohio-rhodes-state-office-tower.jpg")
其它地标有：

  - [俄亥俄历史协会的总部及其博物馆俄亥俄历史中心位于市中心以北六千米处](../Page/俄亥俄历史协会.md "wikilink")。
  - [哥伦布都市图书馆是美国一流的图书馆](../Page/哥伦布都市图书馆.md "wikilink")
  - [富兰克林公园温室是一座优美的维多利亚时期的](../Page/富兰克林公园温室.md "wikilink")[温室](../Page/温室.md "wikilink")。
  - [哥伦布动物园举世著名](../Page/哥伦布动物园.md "wikilink")。
  - 哥伦布最高的建筑是41层的罗德州办公大楼，它是以一名前市长和俄亥俄州长命名的。

### 展览和庆祝

年度举行的庆祝包括[俄亥俄州展](../Page/俄亥俄州展.md "wikilink")（美国最大的州展）、哥伦布艺术节、爵士乐节等。哥伦布的[骄傲游行具有相当的规模](../Page/骄傲游行.md "wikilink")，体现出当地同性恋社群的规模。每年五月末或者六月初举办亚洲节，亚洲节非常受欢迎，获得众多公司资助。节上表演传统亚洲音乐和武术，此外还有文化展览。六月玫瑰园召开玫瑰节。此外六月还有拉丁节庆祝拉丁文化、音乐和食品，30多万人参加。7月4日左右哥伦布的烟火是中西地区最大的，每年有50多万观众。每年九月德国城举办[啤酒节](../Page/慕尼黑啤酒节.md "wikilink")，提供真正德德国食品、啤酒、音乐。

在哥伦布的会议中心每年召开众多大会。这个会议中心是彼得·埃森曼设计的，1993年完工，占地56,000平方米。

### 体育

大学橄榄球队[俄亥俄州立大学七叶树基于哥伦布](../Page/俄亥俄州立大学七叶树.md "wikilink")，它参加[全国大学生体育协会的](../Page/全国大学生体育协会.md "wikilink")[十大联盟联赛](../Page/十大联盟.md "wikilink")，其基地是俄亥俄体育场。冬季七叶树的篮球队也非常吸引人。

一些职业运动队和一个参加[美國職棒小聯盟的棒球队的基地在哥伦布](../Page/美國職棒小聯盟.md "wikilink")。[哥倫布藍夾克参加](../Page/哥倫布藍夾克.md "wikilink")[國家冰球聯盟](../Page/國家冰球聯盟.md "wikilink")，[哥伦布摧毁者参加](../Page/哥伦布摧毁者.md "wikilink")[美国室内足球联盟](../Page/美国室内足球联盟.md "wikilink")，[哥倫布機員参加](../Page/哥倫布機員.md "wikilink")[美國職業足球大聯盟](../Page/美國職業足球大聯盟.md "wikilink")，[哥倫布快帆参加美國職棒小聯盟](../Page/哥倫布快帆.md "wikilink")。

从1976年开始[美巡赛的一场比赛在哥伦布市郊的一个高尔夫球场进行](../Page/美巡赛.md "wikilink")。1987年的[莱德杯也是在这里举行的](../Page/莱德杯.md "wikilink")。

从1985年至1988年[国际汽联大赛的比赛在哥伦布举行](../Page/国际汽联大赛.md "wikilink")。[拉哈尔-莱特曼车队的基地在哥伦布以西的市郊](../Page/拉哈尔-莱特曼车队.md "wikilink")，它参加[因迪赛车联盟](../Page/因迪赛车联盟.md "wikilink")。

每年二月底哥伦布举办[阿诺德古典赛健美和举重比赛](../Page/阿诺德古典赛.md "wikilink")。此外市内还年度召开赛马大会。

### 表演艺术

哥伦布有几个大的音乐场合一些中型音乐厅。大多数大的音乐场是近年来完成的。

哥伦布的表演艺术团体包括哥伦布歌剧团、芭蕾舞团、[哥伦布交响乐团](../Page/哥伦布交响乐团.md "wikilink")、当代美国戏剧团、哥伦布爵士乐团等等。夏季剧团往往在德国城的露天剧场里免费表演[威廉·莎士比亚的剧](../Page/威廉·莎士比亚.md "wikilink")。

### 媒体

哥伦布剩下的唯一的一张日报是《[哥伦布电讯](../Page/哥伦布电讯.md "wikilink")》，此外市内还有一些周报，包括市区的小报和一些城市杂志。

市内有众多电台和电视台。
[Columbus-ohio-leveque-tower.jpg](https://zh.wikipedia.org/wiki/File:Columbus-ohio-leveque-tower.jpg "fig:Columbus-ohio-leveque-tower.jpg")

## 姐妹城市

哥伦布有七座[姐妹城市](../Page/姐妹城市.md "wikilink")，最早的一座是[意大利的](../Page/意大利.md "wikilink")[热那亚](../Page/热那亚.md "wikilink")，1955年称为友好城市。当时热那亚向哥伦布赠送了一座巨大的哥伦布雕像，今天这座雕像位于哥伦布市政大厦前。

  - —[德国](../Page/德国.md "wikilink")[德累斯顿](../Page/德累斯顿.md "wikilink")

  - —[意大利](../Page/意大利.md "wikilink")[热那亚](../Page/热那亚.md "wikilink")

  - —[中华人民共和国](../Page/中华人民共和国.md "wikilink")[合肥](../Page/合肥.md "wikilink")

  - —[以色列](../Page/以色列.md "wikilink")[荷兹利亚](../Page/荷兹利亚.md "wikilink")

  - —[丹麦](../Page/丹麦.md "wikilink")[欧登塞](../Page/欧登塞.md "wikilink")

  - —[西班牙](../Page/西班牙.md "wikilink")[塞維利亞](../Page/塞維利亞.md "wikilink")

  - —[中華民國](../Page/中華民國.md "wikilink")[台南](../Page/台南.md "wikilink")

  - —[中华人民共和国](../Page/中华人民共和国.md "wikilink")[武汉](../Page/武汉.md "wikilink")

## 参考链接

<div class="references-small">

<references/>

</div>

## 参见

  - [俄亥俄州立大学](../Page/俄亥俄州立大学.md "wikilink")
  - [哥伦布港国际机场](../Page/哥伦布港国际机场.md "wikilink")

## 外部链接

  - [哥伦布官方网站](https://web.archive.org/web/20060510000041/http://ci.columbus.oh.us/)
  - [哥伦布商会](http://www.columbus.org/)
  - [哥伦布都市图书馆](https://web.archive.org/web/20060512060127/http://www.cml.lib.oh.us/)

[C](../Category/俄亥俄州城市.md "wikilink")
[C](../Category/美国各州首府.md "wikilink")

1.
2.
3.
4.
5.
6.
7.

8.