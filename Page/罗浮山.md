**罗浮山**又名**东樵山**，是[中国十大](../Page/中国.md "wikilink")[道教名山之一](../Page/道教.md "wikilink")，[汉朝](../Page/汉朝.md "wikilink")[史学家](../Page/史学家.md "wikilink")[司马迁曾把罗浮山比作为](../Page/司马迁.md "wikilink")“粤岳”。它位于[广东省](../Page/广东省.md "wikilink")[博罗县的西北部](../Page/博罗县.md "wikilink")，横跨博罗县、[龙门县](../Page/龙门县.md "wikilink")、[增城區三地](../Page/增城區.md "wikilink")，总面积260多平方公里，和位于广东省[佛山市境内的](../Page/佛山市.md "wikilink")[西樵山是姐妹山](../Page/西樵山.md "wikilink")。罗浮山的主峰是**飞云顶**，海拨1296米。其山势雄浑，风光秀丽，四季气候宜人，是中國[国家重点风景名胜区和避暑胜地](../Page/国家重点风景名胜区.md "wikilink")，被誉为“[岭南第一山](../Page/岭南.md "wikilink")”。[北宋](../Page/北宋.md "wikilink")[苏东坡曾在这里作下](../Page/苏东坡.md "wikilink")“罗浮山下四时春，[卢桔杨梅次第新](../Page/卢桔.md "wikilink")。日啖[荔枝三百颗](../Page/荔枝.md "wikilink")，不辞长作[岭南人](../Page/岭南.md "wikilink")”的名句，而使罗浮山闻名于世。

## 地质形成

《[后汉书](../Page/后汉书.md "wikilink")》中的《郡县志》，後人立註这样描写罗浮山:“博罗有罗山，以浮山自会稽来傅之，故名罗浮”。意思是说罗浮山是罗山和浮山撞在一起形成的，但这只是传说。据[地质学家考证](../Page/地质学.md "wikilink")，罗浮山形成于八千万年以前，这一带[地壳发生](../Page/地壳.md "wikilink")[断层](../Page/断层.md "wikilink")，[花岗岩体受挤压而隆起](../Page/花岗岩.md "wikilink")。经过长年的风雨侵蚀，形成现在奇峰林立的罗浮山。

## 自然景色

罗浮山的三大特色是：奇峰怪石、飞瀑名泉和洞天奇景。罗浮山共有432座山峰，较有名的有飞云峰、铁桥峰、玉女峰、骆驼峰和上界峰等。其中飞云峰是主峰，海拨1296米，因为高耸入云而得名。罗浮山共有980多处飞瀑名泉，著名的有白漓瀑布、白水门瀑布、黄龙洞瀑布、白莲湖、芙蓉池、长生井，还有[北宋文人](../Page/北宋.md "wikilink")[苏东坡所推崇的卓锡泉等](../Page/苏东坡.md "wikilink")。罗浮山还有朱明、蓬莱、桃源、蝴蝶、夜乐等18个大洞天，有通天、罗汉、伏虎和滴水等72个小洞天，其中朱明洞是山上最大的洞穴。

## 植物

罗浮山刚好处在[北回归线上](../Page/北回归线.md "wikilink")，属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，雨量充足，植物茂密，呈垂直分布变化明显。山顶是低矮的[灌木林和草甸](../Page/灌木林.md "wikilink")，山腰是灌木林和松木林，山底是[常绿阔叶林](../Page/常绿阔叶林.md "wikilink")。山上生长着1200多种药用植物和各种水果，形成了独特的罗浮山特产，包括有罗浮山百草油、酥醪菜、云雾甜茶、矿泉水等。苏东坡的“罗浮山下四时春，卢桔杨梅次第新。日啖荔枝三百颗，不辞长作岭南人”一诗正是罗浮山盛产各种水果的真实写照。

## 道观佛寺

[东晋](../Page/东晋.md "wikilink")[咸和年间](../Page/咸和.md "wikilink")(326年-334年)，[葛洪在罗浮山创建了四座](../Page/葛洪.md "wikilink")[道观](../Page/道观.md "wikilink")：白鹤观、冲虚观、[黄龙观和酥醪观](../Page/黄龙古观.md "wikilink")。[南北朝时](../Page/南北朝.md "wikilink")[萧誉把](../Page/萧誉.md "wikilink")[佛教引上来](../Page/佛教.md "wikilink")。强盛时期罗浮山上一度为“九观十八寺二十二庵”，。其中最有名的是葛洪所建的冲虚古观，至今已有1600多年的历史，它在[抗日战争时期](../Page/抗日战争.md "wikilink")，曾经是[东江纵队司令部驻地](../Page/东江纵队.md "wikilink")。冲虚观和延祥寺是唯一保存到现在的两座道观佛寺。

## 文化

罗浮山曾经在广东文化史上占有不可忽视的地位。[苏东坡曾在此地住过很长时间](../Page/苏东坡.md "wikilink")，写下《[杂书罗浮事](../Page/杂书罗浮事.md "wikilink")》、《[书卓锡泉](../Page/书卓锡泉.md "wikilink")》等文章。[明朝](../Page/明朝.md "wikilink")[吏部尚书](../Page/吏部尚书.md "wikilink")[湛若水也曾于九十高龄在罗浮山讲学](../Page/湛若水.md "wikilink")。另外还有[司马迁](../Page/司马迁.md "wikilink")、[李白](../Page/李白.md "wikilink")、[杜甫](../Page/杜甫.md "wikilink")、[韩愈](../Page/韩愈.md "wikilink")、[杨万里](../Page/杨万里.md "wikilink")、[刘禹锡](../Page/刘禹锡.md "wikilink")、[朱熹](../Page/朱熹.md "wikilink")、[屈大均](../Page/屈大均.md "wikilink")、[汤显祖等都有题咏罗浮山的名作](../Page/汤显祖.md "wikilink")。

[后土神](../Page/后土.md "wikilink")，俗称地母娘娘，据闻[清代后期由](../Page/清代.md "wikilink")**[广东罗浮山](../Page/广东.md "wikilink")**传入[台湾](../Page/台湾.md "wikilink")，然而[台湾流传的一九八一年辛酉年刊印罗浮山朝元洞藏版本](../Page/台湾.md "wikilink")《地母经》卷首载有**“[光绪九年正月初九陕西汉中府](../Page/光绪.md "wikilink")[城固地母庙飞鸾传经](../Page/城固.md "wikilink")”**之说（飞鸾，亦称[扶鸾](../Page/扶鸾.md "wikilink")、扶乩，依神灵附体托笔，于沙盘书写上谕圣训，光绪九年即1883年）。自上世纪90年代，来来自[广东](../Page/广东.md "wikilink")，[台湾宗教人士和](../Page/台湾.md "wikilink")[道教信众](../Page/道教.md "wikilink")，远赴[陕西](../Page/陕西.md "wikilink")[城固拜竭地母圣神](../Page/城固.md "wikilink")。\[1\]

而位于[陕西省](../Page/陕西省.md "wikilink")[汉中市](../Page/汉中市.md "wikilink")[城固县县城西南](../Page/城固县.md "wikilink")，上元观镇与董家营镇交界处的[南沙河风景区](../Page/南沙河风景区.md "wikilink")，地母娘娘庙兴建于[东汉](../Page/东汉.md "wikilink")[献帝二年](../Page/献帝.md "wikilink")（公元191年）。距今已有1800多年，历朝历代民众虔诚拜谒，香火鼎盛。
参见：[后土神](../Page/后土.md "wikilink")。

在近现代到过罗浮山的名人则有，[孙中山](../Page/孙中山.md "wikilink")、[宋庆龄](../Page/宋庆龄.md "wikilink")、[廖仲恺](../Page/廖仲恺.md "wikilink")、[何香凝](../Page/何香凝.md "wikilink")、[陈济棠](../Page/陈济棠.md "wikilink")、[蒋介石](../Page/蒋介石.md "wikilink")、[周恩来](../Page/周恩来.md "wikilink")、[陈毅](../Page/陈毅.md "wikilink")、[林彪等](../Page/林彪.md "wikilink")。

## 參考文獻

### 引用

### 网页

  - [罗浮山](http://www.southcn.com/travel/guangdong/huizhou/jingdian/200211270330.htm)，[南方网](../Page/南方网.md "wikilink")，2002年11月27日。
  - [惠州·罗浮山](http://travel.163.com/05/0629/13/1NDT5MVJ00061GI2.html)，[网易](../Page/网易.md "wikilink")，2005年6月29日。

## 参见

  - [惠州市](../Page/惠州市.md "wikilink")
      - [博罗县](../Page/博罗县.md "wikilink")
  - 佛山市南海区[西樵山](../Page/西樵山.md "wikilink")

{{-}}

[Category:道教名山](../Category/道教名山.md "wikilink")
[L](../Category/广东山脉.md "wikilink")
[Category:惠州旅游](../Category/惠州旅游.md "wikilink")
[Category:惠州地理](../Category/惠州地理.md "wikilink")
[Category:博罗县](../Category/博罗县.md "wikilink")
[Category:龙门县](../Category/龙门县.md "wikilink")

1.  [台灣地母道教會齊聚漢中城固祭地母](http://big5.huaxia.com/mlhz/hzxw/2014/04/3858025.html)，2014-04-25
    華夏經緯網