下表列出曾獲[大英帝國勳章的部份知名人士](../Page/大英帝國勳章.md "wikilink")。內閣辦公室每年都會公佈一份「新年授勳名單」和「[英女皇壽辰授勳名單](../Page/英女皇壽辰.md "wikilink")」，內裡列出新獲勳人士所得的勳銜。獲勳者並非在名單公佈時獲勳，而是在事後才獲勳的。「新年授勳名單」一般在每年[元旦前後公佈](../Page/元旦.md "wikilink")，而「[英女皇壽辰授勳名單](../Page/英女皇壽辰.md "wikilink")」則在每年6月初公佈。

## 曾獲勳的[華人](../Page/華人.md "wikilink")

### GBE勳銜

  - [簡悅強爵士](../Page/簡悅強.md "wikilink")（1979年獲勳），[香港前](../Page/香港.md "wikilink")[行政](../Page/香港行政會議.md "wikilink")[立法兩局首席非官守](../Page/香港立法會.md "wikilink")[議員](../Page/議員.md "wikilink")，[東亞銀行前主席](../Page/東亞銀行.md "wikilink")。他早於1959年獲OBE，1967年又獲CBE，1972年獲冊封[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。
  - [鍾士元爵士](../Page/鍾士元.md "wikilink")（1989年獲勳），[香港前](../Page/香港.md "wikilink")[行政](../Page/香港行政會議.md "wikilink")[立法兩局首席非官守](../Page/香港立法會.md "wikilink")[議員和前](../Page/議員.md "wikilink")[行政會議召集人](../Page/行政會議.md "wikilink")。他早於1968年獲OBE，1978年又獲CBE\[<https://web.archive.org/web/20070327194536/http://www3.hku.hk/eroonweb/hongrads/person_c.php?mode=detail&id=190%5D%EF%BC%8C1978%E5%B9%B4%E7%8D%B2%E5%86%8A%E5%B0%81%5B%5B%E4%B8%8B%E7%B4%9A%E5%8B%B3%E4%BD%8D%E7%88%B5%E5%A3%AB%5D%5D%EF%BC%8C%E7%94%B1%E6%96%BC%E4%BB%96%E7%8D%B2%E5%8B%B3%E7%9A%84%E6%98%AFGBE%EF%BC%8C%E6%AF%94KBE%E8%A6%81%E9%AB%98%E7%B4%9A%EF%BC%8C%E6%89%80%E4%BB%A5%E8%A2%AB%E7%A8%B1%E7%82%BA「大Sir>」。

### KBE勳銜

  - [陳策將軍](../Page/陳策_\(民國\).md "wikilink")（1942年獲勳），[中華民國將軍](../Page/中華民國.md "wikilink")[1](https://web.archive.org/web/20070708001859/http://www.hamstat.demon.co.uk/HongKong/HK_Chan.htm)。
  - [何東爵士](../Page/何東.md "wikilink")（1955年獲勳），[香港富商](../Page/香港.md "wikilink")。
  - [李孝式爵士](../Page/李孝式.md "wikilink")（1956年獲勳），[馬來亞開國元老](../Page/馬來亞.md "wikilink")。\[1\]
  - [李卓敏博士](../Page/李卓敏.md "wikilink")（1973年獲勳），[香港中文大學創校校長](../Page/香港中文大學.md "wikilink")，早於1967年獲CBE。\[2\]
  - [曾蔭權爵士](../Page/曾蔭權.md "wikilink")（1997年獲勳），前香港[行政長官](../Page/香港特別行政區行政長官.md "wikilink")，早於1993年獲OBE。
  - [李嘉誠爵士](../Page/李嘉誠.md "wikilink")（2000年獲勳），[香港富商](../Page/香港.md "wikilink")，早於1989年獲CBE。
  - [鄧永鏘爵士](../Page/鄧永鏘.md "wikilink")（2008年獲勳），[香港商人](../Page/香港.md "wikilink")，[蘋果日報專欄作家](../Page/蘋果日報.md "wikilink")，[鄧肇堅爵士長孫](../Page/鄧肇堅.md "wikilink")，早年獲OBE。
  - [丹斯里](../Page/丹斯里.md "wikilink")[張曉卿](../Page/張曉卿.md "wikilink")（2009年獲勳），香港[明報企業集團主席](../Page/明報.md "wikilink")。\[3\]
  - [高錕爵士](../Page/高錕.md "wikilink")（2010年獲勳），2009年[諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")、前[香港中文大學校長](../Page/香港中文大學.md "wikilink")、早於1993年獲CBE。

### DBE勳銜

  - [鄧蓮如女男爵](../Page/鄧蓮如.md "wikilink")（1989年獲勳），前行政立法兩局首席非官守議員，早於1979年獲OBE，1983年獲CBE，1990年封[終身貴族](../Page/終身貴族.md "wikilink")。
  - [王䓪鳴女爵士](../Page/王䓪鳴.md "wikilink")（1996年獲勳），前[房委會主席](../Page/房委會.md "wikilink")。

### CBE勳銜

  - [曹善允](../Page/曹善允.md "wikilink")（1935年獲勳），[民生書院創辦人之一](../Page/民生書院.md "wikilink")，早於1928年獲OBE。
  - [周埈年爵士](../Page/周埈年.md "wikilink")（1938年獲勳），香港商人。
  - [孫立人](../Page/孫立人.md "wikilink")（1943年獲勳），[中華民國陸軍總司令](../Page/中華民國.md "wikilink")，因[仁安羌大捷成功解救英軍而獲勳](../Page/仁安羌大捷.md "wikilink")。
  - [周錫年爵士](../Page/周錫年.md "wikilink")（1950年獲勳），香港商人。
  - [鄧肇堅爵士](../Page/鄧肇堅.md "wikilink")（1957年獲勳），香港商人，慈善家，1949年獲OBE。
  - [利銘澤](../Page/利銘澤.md "wikilink")（1963年獲勳），企業家，早於1949年獲OBE[2](http://wylib.jiangmen.gd.cn/jmhq/list.asp?id=6536)。
  - [關祖堯爵士](../Page/關祖堯.md "wikilink")（1965年獲勳），[律師](../Page/律師.md "wikilink")，前[香港房屋協會總幹事](../Page/香港房屋協會.md "wikilink")，早年獲OBE。
  - [馮秉芬爵士](../Page/馮秉芬.md "wikilink")（1965年獲勳），香港商人及前市政局、立法局和行政局非官守議員，早年獲OBE。
  - [胡百全](../Page/胡百全.md "wikilink")（1973年獲勳），前行政、立法兩局非官守議員，前九巴副董事長。
  - [邵逸夫爵士](../Page/邵逸夫.md "wikilink")（1974年獲勳），[邵氏兄弟的創辦人之一](../Page/邵氏兄弟.md "wikilink")，後於1977年獲冊封[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。
  - [張奧偉爵士](../Page/張奧偉.md "wikilink")（1976年獲勳），第一位華人[御用大律師](../Page/御用大律師.md "wikilink")，前香港賽馬會主席，1972年獲OBE。
  - [利國偉爵士](../Page/利國偉.md "wikilink")（1977年獲勳），[恆生銀行前董事長](../Page/恆生銀行.md "wikilink")，1988年為下級勳位爵士。
  - [方心讓爵士](../Page/方心讓.md "wikilink")（1979年獲勳），前行政立法兩局非官守議員及骨科醫生，[聖保祿醫院院長](../Page/聖保祿醫院.md "wikilink")。
  - [李福和](../Page/李福和.md "wikilink")（1979年獲勳），前行政局及立法局議員。
  - [何鴻鑾](../Page/何鴻鑾.md "wikilink")（1981年獲勳），前社會事務司。
  - [羅德丞](../Page/羅德丞.md "wikilink")（1982年獲勳），前行政立法兩局議員，早於1976年獲OBE。
  - [馬臨](../Page/馬臨.md "wikilink")（1983年獲勳），前[香港中文大學校長](../Page/香港中文大學.md "wikilink")。
  - [林思顯](../Page/林思顯.md "wikilink")（1986年獲勳），銀行家。
  - [施偉賢爵士](../Page/施偉賢.md "wikilink")（1987年獲勳），香港御用大律師、前香港立法局主席，早於1980年獲OBE。
  - [李鵬飛](../Page/李鵬飛.md "wikilink")（1988年獲勳），前立法局首席議員，早於1982年獲OBE。
  - [曹廣榮](../Page/曹廣榮.md "wikilink")（1989年獲勳），前政務司。
  - [胡法光](../Page/胡法光.md "wikilink")（1989年獲勳），前立法局議員。
  - [陳方安生](../Page/陳方安生.md "wikilink")（1992年獲勳），首位華人及首位女性[布政司](../Page/布政司.md "wikilink")。
  - [范徐麗泰](../Page/范徐麗泰.md "wikilink")（1992年獲勳），前立法會主席，早於1988年獲OBE。
  - [李國能](../Page/李國能.md "wikilink")（1992年獲勳），前[香港終審法院首席法官](../Page/香港終審法院.md "wikilink")。
  - [楊啟彥](../Page/楊啟彥.md "wikilink")（1993年獲勳），前庫務司、前教育統籌司。
  - [陳祖澤](../Page/陳祖澤.md "wikilink")（1993年獲勳），前教育統籌司，前副布政司，早於1985年獲OBE。
  - [馮國經](../Page/馮國經.md "wikilink")（1993年獲勳），香港商人。
  - [錢果豐](../Page/錢果豐.md "wikilink")（1994年獲勳），香港鐵路有限公司主席，前行政會議成員。
  - [黃錢其濂](../Page/黃錢其濂.md "wikilink")（1994年獲勳），前衛生福利司、前立法局議員。
  - [孫明揚](../Page/孫明揚.md "wikilink")（1994年獲勳），前教育局局長。
  - [周德熙](../Page/周德熙.md "wikilink")（1995年獲勳），前工商局局長。
  - [陳坤耀](../Page/陳坤耀.md "wikilink")（1995年獲勳），前[嶺南大學校長](../Page/嶺南大學.md "wikilink")。
  - [吳家瑋](../Page/吳家瑋.md "wikilink")（1996年獲勳），[香港科技大學創校校長](../Page/香港科技大學.md "wikilink")。
  - [許淇安](../Page/許淇安.md "wikilink")（1997年獲勳），前[香港警務處處長](../Page/香港警務處處長.md "wikilink")。
  - [葉錫安](../Page/葉錫安.md "wikilink")（1997年獲勳），[香港賽馬會主席](../Page/香港賽馬會.md "wikilink")，前立法局議員、前[香港律師會會長](../Page/香港律師會.md "wikilink")。
  - [黎慶寧](../Page/黎慶寧.md "wikilink")（1997年獲勳），前保安司。
  - [梁寶榮](../Page/梁寶榮.md "wikilink")（1997年獲勳），前[規劃環境地政司](../Page/規劃環境地政局.md "wikilink")。
  - [張健利](../Page/張健利.md "wikilink")（1997年獲勳），香港資深大律師、前行政局非官守議員。
  - [麥列菲菲](../Page/麥列菲菲.md "wikilink")（1997年獲勳），香港醫學教授、前立法局非官守議員，早於1991年獲OBE。
  - [吳榮奎](../Page/吳榮奎.md "wikilink")（1997年獲勳），前運輸局局長。
  - [蕭炯柱](../Page/蕭炯柱.md "wikilink")（1997年獲勳），前規劃環境地政局局長。
  - [何鴻章](../Page/何鴻章.md "wikilink")（2001年獲勳），香港商人。
  - [張榮發](../Page/張榮發.md "wikilink")（2006年獲勳），臺灣商人。
  - [潘迪生](../Page/潘迪生.md "wikilink")（2007年獲勳），香港商人。

#### 日期不詳

  - [張有興](../Page/張有興.md "wikilink")，首位華人[市政局主席](../Page/市政局.md "wikilink")，早年獲OBE。
  - [徐家祥](../Page/徐家祥.md "wikilink")，香港首名華人官學生，前任副華民政務司，1968年獲OBE

### OBE勳銜

  - [林文慶](../Page/林文慶.md "wikilink")（1918年獲勳），[廈門大學第二任校長](../Page/廈門大學.md "wikilink")。
  - [徐亨](../Page/徐亨.md "wikilink")（1942年獲勳），陳策副官[3](https://web.archive.org/web/20070708001859/http://www.hamstat.demon.co.uk/HongKong/HK_Chan.htm)。
  - [蔡杨素梅](../Page/蔡杨素梅.md "wikilink")（1946年獲勳），知名[新加坡前立法議會議員](../Page/新加坡.md "wikilink")。
  - [顏成坤](../Page/顏成坤.md "wikilink")（1955年獲勳），[中華巴士創辦人](../Page/中華巴士.md "wikilink")。
  - [林子豐](../Page/林子豐.md "wikilink")（1957年獲勳），[香港培正中學校長](../Page/香港培正中學.md "wikilink")。
  - [劉韻仙](../Page/劉韻仙.md "wikilink")（1958年獲勳），[南洋教育家](../Page/南洋.md "wikilink")。
  - [謝雨川](../Page/謝雨川.md "wikilink")（1958年獲勳），[香港立法局議員](../Page/香港.md "wikilink")、商家及[聖保羅書院同學會會長](../Page/聖保羅書院.md "wikilink")，同時獲[太平紳士](../Page/太平紳士.md "wikilink")。
  - [鄧炳輝](../Page/鄧炳輝.md "wikilink")（1962年獲勳）
  - [何鴻超](../Page/何鴻超.md "wikilink")（1966年獲勳），[醫務衛生署高級專科醫務人員](../Page/醫務衛生署.md "wikilink")。
  - [容啟東](../Page/容啟東.md "wikilink")（1966年獲勳），[香港中文大學首任副校長](../Page/香港中文大學.md "wikilink")。
  - [黃宣平](../Page/黃宣平.md "wikilink")（1967年獲勳）
  - [鄭棟材](../Page/鄭棟材.md "wikilink")（1967年獲勳）
  - [司徒惠](../Page/司徒惠.md "wikilink")（1967年獲勳）
  - [唐炳源](../Page/唐炳源.md "wikilink")（1967年獲勳）
  - [彭定祥](../Page/彭定祥.md "wikilink")（1968年獲勳），法醫。
  - [趙聿修](../Page/趙聿修.md "wikilink")（1969年獲勳），早年獲MBE。
  - [胡鴻烈](../Page/胡鴻烈.md "wikilink")（1976年獲勳），[樹仁大學創校人](../Page/樹仁大學.md "wikilink")。
  - [王霖](../Page/王霖.md "wikilink")（1978年獲勳），前立法局議員。
  - [金庸](../Page/金庸.md "wikilink")（1981年獲勳），[作家](../Page/作家.md "wikilink")。
  - [鄔維庸](../Page/鄔維庸.md "wikilink")（1983年獲勳），[香港基本法起草委員會委員](../Page/香港基本法起草委員會.md "wikilink")。
  - [周厚澄](../Page/周厚澄.md "wikilink")（1984年獲勳），[荃灣區議會主席](../Page/荃灣區議會.md "wikilink")。
  - [周梁淑怡](../Page/周梁淑怡.md "wikilink")（1985年獲勳），前立法會議員。
  - [何鴻燊](../Page/何鴻燊.md "wikilink")（1990年獲勳），港澳商人。
  - [劉皇發](../Page/劉皇發.md "wikilink")（1990年獲勳），立法會議員，[新界鄉議局主席](../Page/新界鄉議局.md "wikilink")，早年獲MBE。
  - [林行止](../Page/林行止.md "wikilink")（1991年獲勳），[作家](../Page/作家.md "wikilink")。
  - [李國寶爵士](../Page/李國寶.md "wikilink")（1991年獲勳），[香港](../Page/香港.md "wikilink")[東亞銀行主席](../Page/東亞銀行.md "wikilink")，2005年6月他獲冊封為下級勳位爵士，同年10月放棄英國籍加入[香港行政會議](../Page/香港行政會議.md "wikilink")，但因為被冊封時有英國公民身份，他仍然可按慣例继续使用爵士稱号。
  - [潘宗光](../Page/潘宗光.md "wikilink")（1991年獲勳），前[香港理工大學校長](../Page/香港理工大學.md "wikilink")。
  - [梁智鴻](../Page/梁智鴻.md "wikilink")（1992年獲勳），前[醫院管理局主席](../Page/醫院管理局.md "wikilink")。
  - [劉健儀](../Page/劉健儀.md "wikilink")（1992年獲勳），前立法會議員。
  - [林貝聿嘉](../Page/林貝聿嘉.md "wikilink")（1993年獲勳），前立法局議員，早於1985年獲MBE。
  - [鄭海泉](../Page/鄭海泉.md "wikilink")（1994年獲勳），[香港上海滙豐銀行有限公司主席](../Page/香港上海滙豐銀行有限公司.md "wikilink")。
  - [李業廣](../Page/李業廣.md "wikilink")（1994年獲勳），前任[香港交易所主席](../Page/香港交易所.md "wikilink")。
  - [陳啟明](../Page/陳啟明.md "wikilink")（1995年獲勳），[香港中文大學醫學院矯形外科及創傷學系講座教授](../Page/香港中文大學.md "wikilink")。
  - [梁錦松](../Page/梁錦松.md "wikilink")（1995年獲勳），前[財政司司長](../Page/財政司司長.md "wikilink")。
  - [李家祥](../Page/李家祥_\(香港\).md "wikilink")（1996年獲勳），執業[會計師](../Page/會計師.md "wikilink")，前立法會議員。
  - [梁智仁](../Page/梁智仁.md "wikilink")（1996年獲勳），[香港公開大學校長](../Page/香港公開大學.md "wikilink")。
  - [潘樂陶](../Page/潘樂陶.md "wikilink")（1996年獲勳）
  - [歐肇基](../Page/歐肇基.md "wikilink")（1997年獲勳）
  - [龐述英](../Page/龐述英.md "wikilink")（1997年獲勳）
  - [陳佳鼐](../Page/陳佳鼐.md "wikilink")（1997年獲勳）
  - [鄭慕智](../Page/鄭慕智.md "wikilink")（1997年獲勳）
  - [張妙清](../Page/張妙清.md "wikilink")（1997年獲勳）
  - [張比德](../Page/張比德.md "wikilink")（1997年獲勳）
  - [蔣震](../Page/蔣震.md "wikilink")（1997年獲勳）
  - [周明權](../Page/周明權.md "wikilink")（1997年獲勳）
  - [戴婉瑩](../Page/戴婉瑩.md "wikilink")（1997年獲勳）
  - [陳馮富珍](../Page/陳馮富珍.md "wikilink")（1997年獲勳），[世界衛生組織總幹事](../Page/世界衛生組織.md "wikilink")。
  - [林瑞麟](../Page/林瑞麟.md "wikilink")（1997年獲勳），前政務司司長。\[4\]
  - [李澤培](../Page/李澤培.md "wikilink")（1997年獲勳）
  - [李承仕](../Page/李承仕.md "wikilink")（1997年獲勳）
  - [曾蔭培](../Page/曾蔭培.md "wikilink")（1997年獲勳），前香港警務處處長。
  - [黃鋼城](../Page/黃鋼城.md "wikilink")（1997年獲勳）
  - [陳廷驊](../Page/陳廷驊.md "wikilink")（1997年獲勳），香港企業家。
  - [金聖華](../Page/金聖華.md "wikilink")（1997年獲勳），[翻譯學者](../Page/翻譯.md "wikilink")。
  - [林兆鑫](../Page/林兆鑫.md "wikilink")（1997年獲勳），[香港大學](../Page/香港大學.md "wikilink")[李嘉誠醫學院前院長](../Page/李嘉誠醫學院.md "wikilink")，於2009年承認身為[公職人員行為失當](../Page/公職人員行為失當.md "wikilink")，被判入獄25個月。
  - [楊永強](../Page/楊永強.md "wikilink")（1997年獲勳），前[衛生福利及食物局局長](../Page/衛生福利及食物局.md "wikilink")。
  - [丘德威](../Page/丘德威.md "wikilink")（2006年獲勳），[倫敦飲食業鉅子](../Page/倫敦.md "wikilink")[4](http://www.bodw.com.hk/trad/speaker_detail.php?speaker=alany)。
  - [陸恭蕙](../Page/陸恭蕙.md "wikilink")（2007年獲勳），環境局副局長，前立法局議員。
  - [葉煥榮](../Page/葉煥榮.md "wikilink")（2010年獲勳），英國葉氏兄弟企業主席, 榮業行創辦人兼董事長,
    第一個進入英國富豪榜的華人
  - [李德銖](../Page/李德銖.md "wikilink")（2010年獲勳），[植物學家](../Page/植物學家.md "wikilink")，[倫敦林奈學會會員](../Page/倫敦林奈學會.md "wikilink")[5](http://www.fco.gov.uk/en/about-us/what-we-do/services-we-deliver/protocol/honours/honorary-awards)。
  - [邓柱廷](../Page/邓柱廷.md "wikilink")（2011年獲勳），[伦敦华埠商会主席](../Page/伦敦华埠商会.md "wikilink")。

#### 日期不詳

  - [張鑑泉](../Page/張鑑泉.md "wikilink")，前立法局議員。
  - [何佐芝](../Page/何佐芝.md "wikilink")，香港商人，[香港商業廣播有限公司創辦人](../Page/香港商業廣播有限公司.md "wikilink")。
  - [何甘棠](../Page/何甘棠.md "wikilink")，[怡和洋行](../Page/怡和洋行.md "wikilink")[買辦](../Page/買辦.md "wikilink")。
  - [何承天](../Page/何承天_\(建築師\).md "wikilink")，香港[建築師](../Page/建築師.md "wikilink")。
  - [梁定邦](../Page/梁定邦_\(醫生\).md "wikilink")，前市政局主席，前[廣安銀行](../Page/廣安銀行.md "wikilink")[董事長](../Page/董事長.md "wikilink")。
  - [羅仲榮](../Page/羅仲榮.md "wikilink")，香港商人，前[行政會議成員](../Page/行政會議.md "wikilink")。
  - [田北俊](../Page/田北俊.md "wikilink")，立法會議員。
  - [黃宏發](../Page/黃宏發.md "wikilink")，前立法局主席。
  - [葉漢](../Page/葉漢.md "wikilink")，澳門[賭業大亨](../Page/賭博.md "wikilink")。
  - 李道生，[馬來西亞](../Page/馬來西亞.md "wikilink")[沙巴](../Page/沙巴.md "wikilink")[亞庇立法議員](../Page/亞庇.md "wikilink")。\[5\]

### MBE勳銜

  - [黃作梅](../Page/黃作梅.md "wikilink")（1947年獲勳），
    [港九大隊國際聯絡組負責人](../Page/港九大隊.md "wikilink")，迄今唯一一位獲勳的[中國共產黨黨員](../Page/中國共產黨.md "wikilink")[6](https://web.archive.org/web/20070929131627/http://app.hkatv.com/nextweek/search2.php?p=148)。
  - [莫幹生](../Page/莫幹生.md "wikilink")（1959年獲勳），[太古洋行買辦](../Page/太古洋行.md "wikilink")。
  - [潘仁昌](../Page/潘仁昌.md "wikilink")（1966年獲勳），《[工商日報](../Page/工商日報.md "wikilink")》總編輯。
  - [陳漢賢](../Page/陳漢賢.md "wikilink")（1967年獲勳）
  - [張作登](../Page/張作登.md "wikilink")（1967年獲勳）
  - [何蔭璇](../Page/何蔭璇.md "wikilink")（1967年獲勳）
  - [傅禮安](../Page/傅禮安.md "wikilink")（1967年獲勳）
  - [梁忠義](../Page/梁忠義.md "wikilink")（1967年獲勳）
  - [林繼振](../Page/林繼振.md "wikilink")（1967年獲勳），前東華三院總理。
  - [陳超常](../Page/陳超常.md "wikilink")（1967年獲勳），前保良局主席。
  - [張瑞榮](../Page/張瑞榮.md "wikilink")（1967年獲勳）
  - [張謝琮賢](../Page/張謝琮賢.md "wikilink")（1967年獲勳）
  - [張人龍](../Page/張人龍.md "wikilink")（1967年獲勳），著名[新界鄉紳](../Page/新界.md "wikilink")，前[新界鄉議局主席](../Page/新界鄉議局.md "wikilink")，前[區域市政局主席](../Page/區域市政局.md "wikilink")，前[立法局議員](../Page/立法局議員.md "wikilink")。
  - [孫秉樞](../Page/孫秉樞.md "wikilink")（1967年獲勳）
  - [黃秉乾](../Page/黃秉乾_\(律師\).md "wikilink")（1967年獲勳）
  - [袁翔雲](../Page/袁翔雲.md "wikilink")（1967年獲勳）
  - [黃湛](../Page/黃湛.md "wikilink")（1967年獲勳）
  - [梁乃康](../Page/梁乃康.md "wikilink")（1967年獲勳）
  - [張紹桂](../Page/張紹桂.md "wikilink")（1968年獲勳），高級教育官。
  - [方潤華](../Page/方潤華.md "wikilink")（1968年獲勳），前東華三院總理。
  - [鄺命光](../Page/鄺命光.md "wikilink")（1968年獲勳）
  - [林植豪](../Page/林植豪.md "wikilink")（1968年獲勳）
  - [岑才生](../Page/岑才生.md "wikilink")（1968年獲勳），《[華僑日報](../Page/華僑日報.md "wikilink")》經理。
  - [徐添福](../Page/徐添福.md "wikilink")（1968年獲勳）
  - [禤偉靈](../Page/禤偉靈.md "wikilink")（1968年獲勳），[香港培道中學前校長](../Page/香港培道中學.md "wikilink")。
  - [許慧嫻](../Page/許慧嫻.md "wikilink")（1968年獲勳），[香港大學化學系講師](../Page/香港大學.md "wikilink")。
  - [張錦添](../Page/張錦添.md "wikilink")（1969年獲勳）
  - [周有](../Page/周有.md "wikilink")（1969年獲勳）
  - [李孟標](../Page/李孟標.md "wikilink")（1969年獲勳）
  - [梁錫洪](../Page/梁錫洪.md "wikilink")（1969年獲勳）
  - [羅徵勤](../Page/羅徵勤.md "wikilink")（1969年獲勳）
  - [汪彼得](../Page/汪彼得.md "wikilink")（1969年獲勳）
  - [張枝繁](../Page/張枝繁.md "wikilink")（1971年獲勳）
  - [曾昭仁](../Page/曾昭仁.md "wikilink")（1971年獲勳）
  - [余祿祐](../Page/余祿祐.md "wikilink")（1971年獲勳）
  - [陳德俊](../Page/陳德俊.md "wikilink")（1971年獲勳）
  - [夏鎮國](../Page/夏鎮國.md "wikilink")（1971年獲勳）
  - [呂壽琨](../Page/呂壽琨.md "wikilink")（1971年獲勳）
  - [葉元章](../Page/葉元章.md "wikilink")（1971年獲勳）
  - [何祥友](../Page/何祥友.md "wikilink")（1972年獲勳），香港職業足球員。
  - [唐余湘畹](../Page/唐余湘畹.md "wikilink")（1977年獲勳），香港[教育家](../Page/教育家.md "wikilink")。
  - [梁醒波](../Page/梁醒波.md "wikilink")（1977年獲勳），著名[粵劇演員](../Page/粵劇.md "wikilink")，有「丑生王」美譽。
  - [新馬師曾](../Page/新馬師曾.md "wikilink")（1978年獲勳），著名[粵劇演員](../Page/粵劇.md "wikilink")，，有「文武生王」、「慈善伶王」美譽。
  - [趙少昂](../Page/趙少昂.md "wikilink")（1980年獲勳），[國畫大師](../Page/國畫.md "wikilink")。
  - [關德興](../Page/關德興.md "wikilink")（1981年獲勳），著名[粵劇演員](../Page/粵劇.md "wikilink")，武打明星。
  - [顧嘉煇](../Page/顧嘉煇.md "wikilink")（1982年獲勳），香港著名[作曲家](../Page/作曲家.md "wikilink")。
  - [劉嘉敏](../Page/劉嘉敏.md "wikilink")（1984年獲勳），前[政府電腦資料處理處首長](../Page/政府電腦資料處理處.md "wikilink")。
  - [廖慶齊](../Page/廖慶齊.md "wikilink")（1984年獲勳），[香港太空館首任館長](../Page/香港太空館.md "wikilink")。
  - [胡永輝](../Page/胡永輝.md "wikilink")（1989年獲勳），香港商人。
  - [成龍](../Page/成龍.md "wikilink")（1989年獲勳），著名電影[演員](../Page/演員.md "wikilink")。
  - [鍾偉明](../Page/鍾偉明.md "wikilink")（1992年獲勳），[香港電台資深傳媒人](../Page/香港電台.md "wikilink")。
  - [周一嶽](../Page/周一嶽.md "wikilink")（1995年獲勳），前[食物及衞生局](../Page/食物及衞生局.md "wikilink")[局長](../Page/局長.md "wikilink")，[平等機會委員會主席](../Page/平等機會委員會.md "wikilink")。
  - [莊陳有](../Page/莊陳有.md "wikilink")（1995年獲勳），香港[樂施會前總幹事](../Page/樂施會.md "wikilink")，[香港大學學生發展總監](../Page/香港大學.md "wikilink")。[7](http://www.eduplus.com.hk/eduguide/ep2_issue.jsp?articleID=397&issueNumber=30)
  - [許晉奎](../Page/許晉奎.md "wikilink")（1996年獲勳），香港商人，前[香港足球總會主席](../Page/香港足球總會.md "wikilink")。
  - [周湛樵](../Page/周湛樵.md "wikilink")（1997年獲勳），前[香港輔助警察隊](../Page/香港輔助警察隊.md "wikilink")[總監](../Page/總監.md "wikilink")。
  - [趙振邦](../Page/趙振邦.md "wikilink")（1997年獲勳），香港商人，保良局前主席（1980年－1981年)，保良局顧問局成員。
  - [李麗珊](../Page/李麗珊.md "wikilink")（1997年獲勳），[奧運會金牌得主](../Page/奧運會.md "wikilink")。
  - [蕭芳芳](../Page/蕭芳芳.md "wikilink")（1997年獲勳），著名電影[演員](../Page/演員.md "wikilink")。
  - [徐尉玲](../Page/徐尉玲.md "wikilink")（1997年獲勳），[香港中樂團理事會主席](../Page/香港中樂團.md "wikilink")。
  - [黃安源](../Page/黃安源.md "wikilink")（1997年獲勳），著名[二胡演奏家](../Page/二胡.md "wikilink")。
  - [杨希雪](../Page/杨希雪.md "wikilink")（2008年獲勳），[英国华裔](../Page/英国.md "wikilink")[画家](../Page/画家.md "wikilink")。
  - [李安鈴](../Page/李安鈴.md "wikilink")（2008年獲勳），台灣人士。

#### 日期不詳

  - [蔡陳葆心](../Page/蔡陳葆心.md "wikilink")，[證券商人](../Page/證券.md "wikilink")。
  - [李鳳英](../Page/李鳳英.md "wikilink")，前立法會議員。
  - [何鍾泰](../Page/何鍾泰.md "wikilink")，前立法會議員。
  - [廖秀冬](../Page/廖秀冬.md "wikilink")，前[環境運輸及工務局局長](../Page/環境運輸及工務局.md "wikilink")。
  - [黃秉槐](../Page/黃秉槐.md "wikilink")，前[立法局議員](../Page/立法局.md "wikilink")。
  - [余錦基](../Page/余錦基.md "wikilink")，香港商人，前香港足球總會主席。
  - [謝俊謙](../Page/謝俊謙.md "wikilink")，[香港大學計算機科學系教授](../Page/香港大學.md "wikilink")。

## 曾獲大英帝國勳章知名人士

### GBE勳銜

  - [伊麗莎白皇后](../Page/伊麗莎白·鮑斯-萊昂.md "wikilink")（1929年獲勳），[喬治六世皇后](../Page/喬治六世.md "wikilink")。
  - [斯利姆子爵](../Page/威廉·斯利姆.md "wikilink")（1946年獲勳），英國軍事指揮官和第十三任[澳大利亚总督](../Page/澳大利亚总督.md "wikilink")，早於1942年獲CBE。
  - [愛丁堡公爵](../Page/菲臘親王_\(愛丁堡公爵\).md "wikilink")（1953年獲勳），[英女皇](../Page/英女皇.md "wikilink")[伊利沙伯二世皇夫](../Page/伊利沙伯二世.md "wikilink")。
  - [夏慤爵士](../Page/夏慤.md "wikilink")（1953年獲勳），[英國皇家海軍少將](../Page/英國皇家海軍.md "wikilink")，早於1940年獲CBE。
  - [根德公爵夫人馬利納郡主](../Page/馬里納郡主_\(根德公爵夫人\).md "wikilink")（1937年獲勳），[英國皇室成員](../Page/英國皇室.md "wikilink")。
  - [威爾遜勳爵](../Page/亨利·梅特蘭·威爾遜.md "wikilink")（1941年獲勳），[英國陸軍元帥](../Page/英國陸軍元帥.md "wikilink")。
  - [麥理浩勳爵](../Page/麥理浩.md "wikilink")（1976年獲勳），前[香港總督](../Page/香港總督.md "wikilink")，早於1946年獲MBE。

### KBE勳銜

  - 爵士（1917年獲勳），於1915年開發[斯托克斯式戰壕迫擊炮迫擊炮](../Page/迫擊炮.md "wikilink")。

  - [施勳爵士](../Page/施勳.md "wikilink")（1923年獲勳），前香港[輔政司](../Page/輔政司.md "wikilink")。

  - [修頓爵士](../Page/修頓.md "wikilink")（1933年獲勳），前香港[輔政司](../Page/輔政司.md "wikilink")。

  - [郝德傑爵士](../Page/郝德傑.md "wikilink")（1935年獲勳），前[香港總督](../Page/香港總督.md "wikilink")，早於1926年獲CBE。

  - [陶法阿豪·圖普四世](../Page/陶法阿豪·圖普四世.md "wikilink")（1958年獲勳），[湯加第](../Page/湯加.md "wikilink")4任國王。

  - [梅紐因勳爵](../Page/耶胡迪·梅紐因.md "wikilink")（1965年獲勳），美国[小提琴家](../Page/小提琴.md "wikilink")，他於1965年以外國人身份獲KBE，不稱為爵士；1985年，他獲頒授英國國籍，因而被尊稱為[爵士](../Page/爵士.md "wikilink")；1993年，他再被冊封為[終身貴族](../Page/終身貴族.md "wikilink")，位列[男爵](../Page/男爵.md "wikilink")，敬稱為[勳爵](../Page/勳爵.md "wikilink")，爵位比爵士高。

  - [郭伯偉爵士](../Page/郭伯偉.md "wikilink")（1968年獲勳），前香港[財政司](../Page/財政司.md "wikilink")，早於1960年獲OBE。

  - [喬治·索爾蒂爵士](../Page/喬治·索爾蒂.md "wikilink")（1971年獲勳），[匈牙利指揮家](../Page/匈牙利.md "wikilink")，1972年成為英國公民，可用「爵士」頭銜。

  - [查理·卓別林爵士](../Page/查理·卓別林.md "wikilink")（1975年獲勳），喜劇泰斗和[導演](../Page/導演.md "wikilink")，英國政府早在1956年[冷戰期間建議授勳於他](../Page/冷戰.md "wikilink")，但他本人憐憫共產思想，此建議被外交部否決[8](http://news.bbc.co.uk/chinese/simp/hi/newsid_2140000/newsid_2141600/2141618.stm)。

  - [尤金·奧曼迪](../Page/尤金·奧曼迪.md "wikilink")（1976年獲勳），[匈牙利](../Page/匈牙利.md "wikilink")[指揮家和](../Page/指揮家.md "wikilink")[小提琴手](../Page/小提琴.md "wikilink")。

  - [姬達爵士](../Page/姬達.md "wikilink")（1979年獲勳），香港前[廉政專員](../Page/香港廉政公署.md "wikilink")。

  - [希治閣爵士](../Page/希治閣.md "wikilink")（1980年獲勳），美國電影導演，英國出生，1980年獲勳，但其本人在獲女皇親自冊封前逝世；他在1956年獲美國公民權時沒有放棄英國臣民身份，因而可尊稱為爵士。

  - [夏鼎基爵士](../Page/夏鼎基.md "wikilink")（1980年獲勳），前香港[財政司](../Page/財政司.md "wikilink")、[布政司](../Page/布政司.md "wikilink")。

  - [陳仲民爵士](../Page/陳仲民.md "wikilink")（1981年獲勳），前[巴布亞紐幾內亞總理](../Page/巴布亞紐幾內亞.md "wikilink")。

  - [彭勵治爵士](../Page/彭勵治.md "wikilink")（1983年獲勳），前香港[財政司](../Page/財政司.md "wikilink")，早於1976年獲OBE。

  - [鍾逸傑爵士](../Page/鍾逸傑.md "wikilink")（1985年獲勳），前[香港](../Page/香港.md "wikilink")[布政司](../Page/布政司.md "wikilink")。

  - [鮑勃·格爾多夫](../Page/鮑勃·格爾多夫.md "wikilink")（1986年獲勳），[愛爾蘭歌手](../Page/愛爾蘭.md "wikilink")。

  - [霍德爵士](../Page/霍德.md "wikilink")（1988年獲勳），前[香港](../Page/香港.md "wikilink")[布政司](../Page/布政司.md "wikilink")。

  - [翟克誠爵士](../Page/翟克誠.md "wikilink")（1989年獲勳），前香港[財政司](../Page/財政司.md "wikilink")，早於1981年獲OBE。

  - [盛田昭夫](../Page/盛田昭夫.md "wikilink")（1992年獲勳），[新力公司](../Page/新力.md "wikilink")(SONY)創辦人之一。

  - [麥高樂爵士](../Page/麥高樂.md "wikilink")（1994年獲勳），前香港[財政司](../Page/財政司.md "wikilink")，早於1992年獲CBE。

  - [比利](../Page/比利.md "wikilink")（1997年獲勳），[巴西足球員](../Page/巴西.md "wikilink")。

  - [鲍勃·霍普](../Page/鲍勃·霍普.md "wikilink")（1998年獲勳），英國出生的[喜剧演员](../Page/喜剧.md "wikilink")，但後來入籍[美國時放棄英籍](../Page/美國.md "wikilink")，早於1976年獲CBE。

  - [史提芬·史匹堡](../Page/史提芬·史匹堡.md "wikilink")（2001年獲勳），[美國电影](../Page/美國.md "wikilink")[导演](../Page/导演.md "wikilink")。

  - [魯道夫·朱利安尼](../Page/魯道夫·朱利安尼.md "wikilink")（2002年獲勳），前[纽约市长](../Page/纽约.md "wikilink")。

  - [艾伦·格林斯潘](../Page/艾伦·格林斯潘.md "wikilink")（2002年獲勳），前[美联储主席](../Page/美联储.md "wikilink")。

  - [羅渣·摩亞爵士](../Page/羅渣·摩亞.md "wikilink")（2003年獲勳），演員，早於1999年獲CBE。

  - [蒂姆·伯納斯-李爵士](../Page/蒂姆·伯納斯-李.md "wikilink")（2004年獲勳），[全球資訊網的發明者](../Page/全球資訊網.md "wikilink")。

  - [西蒙·維森塔爾](../Page/西蒙·維森塔爾.md "wikilink")（2004年獲勳），[猶太人大屠殺的倖存者](../Page/猶太人大屠殺.md "wikilink")，畢生致力追查[納粹黨人和蒐證](../Page/納粹黨.md "wikilink")。

  - [比尔·盖茨](../Page/比尔·盖茨.md "wikilink")（2005年獲勳），[微软总裁](../Page/微软.md "wikilink")。

  - [強納生·保羅·伊夫](../Page/強納生·保羅·伊夫.md "wikilink")（2012年獲勳），[蘋果公司工業設計部門副總裁](../Page/蘋果公司.md "wikilink")，早於2006年獲CBE。

  - [麥克·彭博](../Page/麥克·彭博.md "wikilink")（2014年獲勳），前[紐約市長](../Page/紐約.md "wikilink")。

### DBE勳銜

  - [阿嘉莎·克莉絲蒂女爵士](../Page/阿嘉莎·克莉絲蒂.md "wikilink")（1971年獲勳），[英國](../Page/英國.md "wikilink")[偵探小說](../Page/偵探小說.md "wikilink")[作家](../Page/作家.md "wikilink")。
  - [瓊·蘇瑟蘭女爵士](../Page/瓊·蘇瑟蘭.md "wikilink")（1978年獲勳），[澳大利亞](../Page/澳大利亞.md "wikilink")[歌劇](../Page/歌劇.md "wikilink")[女高音](../Page/女高音.md "wikilink")，早於1961年獲CBE。
  - [凱瑟琳·蒂澤德女爵士](../Page/凱瑟琳·蒂澤德.md "wikilink")（1985年獲勳），前[紐西蘭總督](../Page/紐西蘭.md "wikilink")。
  - [西爾維亞·卡特賴特女爵士](../Page/西爾維亞·卡特賴特.md "wikilink")（1989年獲勳），前[紐西蘭總督](../Page/紐西蘭.md "wikilink")。
  - [瑪姬·史密斯女爵士](../Page/瑪姬·史密斯.md "wikilink")（1990年獲勳），演員。
  - [伊莉莎白·舒瓦茲科普夫女爵士](../Page/伊莉莎白·舒瓦茲科普夫.md "wikilink")（1992年獲勳），德裔[歌劇歌曲演員](../Page/歌劇.md "wikilink")。
  - [伊莉沙伯·泰萊女爵士](../Page/伊莉沙伯·泰萊.md "wikilink")（1999年獲勳），[演員](../Page/演員.md "wikilink")。
  - [約瑟琳·貝爾·伯奈爾女爵士](../Page/約瑟琳·貝爾·伯奈爾.md "wikilink")（1999年獲勳），[天文學家](../Page/天文學家.md "wikilink")。
  - [茱莉·安德丝女爵士](../Page/茱莉·安德丝.md "wikilink")（2000年獲勳），[演員](../Page/演員.md "wikilink")。
  - [珍·古德女爵士](../Page/珍·古德.md "wikilink")（2003年獲勳），[英國](../Page/英國.md "wikilink")[生物學家](../Page/生物學家.md "wikilink")、[動物行為學家和著名動物保育人士](../Page/動物.md "wikilink")，早於1995年獲CBE。
  - [海倫·美蘭女爵士](../Page/海倫·美蘭.md "wikilink")（2003年獲勳），[演員](../Page/演員.md "wikilink")。
  - [薇薇安·魏斯伍德女爵士](../Page/薇薇安·魏斯伍德.md "wikilink")（2006年獲勳），[英國時裝設計師](../Page/英國.md "wikilink")。
  - [安娜·溫特女爵士](../Page/安娜·溫特.md "wikilink")（2017年獲勳），[英國雜誌編輯](../Page/英國.md "wikilink")。

### CBE勳銜

  - [內維爾勳爵](../Page/西里爾·內維爾.md "wikilink")（1919年獲勳），[英國空軍元帥](../Page/英國空軍元帥列表.md "wikilink")。
  - [科林·戴維斯爵士](../Page/科林·戴維斯.md "wikilink")（1965年獲勳），[英國](../Page/英國.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")，1980年成為[爵士](../Page/爵士.md "wikilink")。
  - [哈羅德·品特](../Page/哈羅德·品特.md "wikilink")（1966年獲勳），[英國](../Page/英國.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")。
  - [高登爵士](../Page/高登_\(企業家\).md "wikilink")（1968年獲勳），香港[立法局和](../Page/立法局.md "wikilink")[行政局非官守議員](../Page/行政局.md "wikilink")，早於1965年獲OBE，1972年成為[爵士](../Page/爵士.md "wikilink")。
  - [愛德華多·包洛奇爵士](../Page/愛德華多·包洛奇.md "wikilink")（1968年獲勳），[義大利裔英國](../Page/義大利.md "wikilink")[雕塑家](../Page/雕塑家.md "wikilink")，1989年成為[爵士](../Page/爵士.md "wikilink")。
  - [J·R·R·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")（1972年獲勳），英國[作家](../Page/作家.md "wikilink")。
  - [卜比·查爾頓爵士](../Page/卜比查爾頓.md "wikilink")（1973年獲勳），[英格蘭著名足球員](../Page/英格蘭.md "wikilink")，早於1969年獲OBE。
  - [大衛·阿騰勃羅爵士](../Page/大衛·阿騰勃羅.md "wikilink")（1974年獲勳），[英國廣播公司電視節目主持及製作人](../Page/英國廣播公司.md "wikilink")。
  - [韓達德勳爵](../Page/韓達德.md "wikilink")（1974年獲勳），英國政治家、後來曾任[外相和獲封](../Page/英國外相.md "wikilink")[終身貴族](../Page/終身貴族.md "wikilink")。
  - [約翰·普羅富莫](../Page/約翰·普羅富莫.md "wikilink")（1975年獲勳），[普羅富莫事件主角](../Page/普羅富莫事件.md "wikilink")，早於1944年獲OBE。
  - [李察·邦尼](../Page/李察邦尼.md "wikilink")（1977年獲勳），[澳大利亞著名的指揮家](../Page/澳大利亞.md "wikilink")。
  - [杜葉錫恩](../Page/杜葉錫恩.md "wikilink")（1977年獲勳），前香港立法局議員。
  - [羅保爵士](../Page/羅保.md "wikilink")（1978年獲勳），前香港[市政局](../Page/市政局.md "wikilink")、[行政局及](../Page/行政局.md "wikilink")[立法局議員](../Page/立法局.md "wikilink")，早於1972年獲OBE。
  - [伊恩·麦克莱恩爵士](../Page/伊恩·麦克莱恩.md "wikilink")（1979年獲勳），英國演員。
  - [史提芬·霍金](../Page/史提芬·霍金.md "wikilink")（1982年獲勳），[英國著名](../Page/英國.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")。
  - [賽門·拉圖爵士](../Page/賽門·拉圖.md "wikilink")（1987年獲勳），[英國著名](../Page/英國.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")。
  - [浦偉士爵士](../Page/浦偉士.md "wikilink")（1990年獲勳），[滙豐集團前主席](../Page/滙豐集團.md "wikilink")。
  - [亞歷克斯·弗格森爵士](../Page/亞歷克斯·弗格森.md "wikilink")（1995年獲勳），[足球](../Page/足球.md "wikilink")[教練](../Page/教練.md "wikilink")，早於1983年獲OBE。
  - [艾爾頓·約翰爵士](../Page/艾爾頓·約翰.md "wikilink")（1996年獲勳），[英國](../Page/英國.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [列顯倫](../Page/列顯倫.md "wikilink")（2000年獲勳），香港[法官](../Page/法官.md "wikilink")，早於1989年獲OBE。
  - [約翰·查爾斯](../Page/約翰·查爾斯.md "wikilink")（2001年獲勳），[威爾斯足球員](../Page/威爾斯.md "wikilink")。
  - [博比·罗布森](../Page/博比·罗布森.md "wikilink")（2002年獲勳），足球球员、主教练。
  - [Bee Gees三兄弟](../Page/Bee_Gees.md "wikilink")（2002年獲勳），英国著名家族乐隊。
  - [亞倫·帕克爵士](../Page/亞倫·帕克.md "wikilink")（2002年獲勳），英國[電影](../Page/電影.md "wikilink")[導演](../Page/導演.md "wikilink")。
  - [约翰·加利亚诺](../Page/约翰·加利亚诺.md "wikilink")（2002年獲勳），英国[时装设计师](../Page/时装设计.md "wikilink")。
  - [蜷川幸雄](../Page/蜷川幸雄.md "wikilink")（2002年獲勳），[日本](../Page/日本.md "wikilink")[導演及](../Page/導演.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [亞歷山大·麥昆](../Page/亞歷山大·麥昆.md "wikilink")（2003年獲勳），著名[服裝設計師](../Page/服裝設計.md "wikilink")。
  - [埃里克·克莱普顿](../Page/埃里克·克莱普顿.md "wikilink")（2004年獲勳），[英國](../Page/英國.md "wikilink")[結他手](../Page/結他手.md "wikilink")、[歌手和](../Page/歌手.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。
  - [布赖恩·梅](../Page/布赖恩·梅.md "wikilink")（2005年獲勳），英国[摇滚乐团体](../Page/摇滚乐.md "wikilink")[皇后吉他手](../Page/皇后乐队.md "wikilink")。
  - [艾爾敦](../Page/艾爾敦.md "wikilink")（2005年獲勳），[香港上海滙豐銀行有限公司前主席](../Page/香港上海滙豐銀行有限公司.md "wikilink")。
  - [尼格尔·曼塞尔](../Page/尼格尔·曼塞尔.md "wikilink")（2011年獲勳），英国[一级方程式车手](../Page/一级方程式.md "wikilink")。
  - [安东尼·弗朗西斯·费南德斯](../Page/安东尼·弗朗西斯·费南德斯.md "wikilink")（2011年獲勳），[馬來西亞](../Page/馬來西亞.md "wikilink")[亞洲航空](../Page/亞洲航空.md "wikilink")[首席执行官](../Page/首席执行官.md "wikilink")。\[6\]
  - [柯林·佛斯](../Page/柯林·佛斯.md "wikilink")（2012年獲勳），英国[演員](../Page/演員.md "wikilink")。
  - [海倫娜·寶漢·卡特](../Page/海倫娜·寶漢·卡特.md "wikilink")（2012年獲勳），英国[演員](../Page/演員.md "wikilink")。
  - [路雲·雅堅遜](../Page/路雲·雅堅遜.md "wikilink")（2013年獲勳），英国[喜劇](../Page/喜劇.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [布拉德利·威金斯](../Page/布拉德利·威金斯.md "wikilink")（2013年獲勳），英国[自行車](../Page/自行車.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [班奈狄克·康柏拜區](../Page/班奈狄克·康柏拜區.md "wikilink")（2015年獲勳），英国[演員](../Page/演員.md "wikilink")。
  - [曉·萊利](../Page/曉·萊利.md "wikilink")（2018年獲勳），英国[演員](../Page/演員.md "wikilink")。

### OBE勳銜

  - [修頓爵士夫人](../Page/貝拉·悉尼，修頓爵士夫人.md "wikilink")，前[香港女童軍總會總監](../Page/香港女童軍總會.md "wikilink")。
  - [韋爾遜勳爵](../Page/韋爾遜.md "wikilink")（1945年獲勳），前[英國首相](../Page/英國首相.md "wikilink")。
  - [柏立基爵士](../Page/柏立基.md "wikilink")（1949年獲勳），前[香港總督](../Page/香港總督.md "wikilink")，早於1948年獲MBE。
  - [白嘉時](../Page/白嘉時.md "wikilink")（1954年獲勳），前香港[輔政司](../Page/輔政司.md "wikilink")。
  - [湯·芬尼爵士](../Page/湯·芬尼.md "wikilink")（1961年獲勳），英國足球運動員。
  - [羅樂民爵士](../Page/羅樂民.md "wikilink")（1961年獲勳），前香港[輔政司](../Page/輔政司.md "wikilink")。
  - [喬·戴維斯](../Page/喬·戴維斯.md "wikilink")（1963年獲勳），英國職業[士碌架選手](../Page/士碌架.md "wikilink")。被譽為「現代士碌架之父」。
  - [紐寶璐](../Page/紐寶璐.md "wikilink")（1963年獲勳），香港英華書院校長。
  - [傑克·查爾頓](../Page/傑克·查爾頓.md "wikilink")（1974年獲勳），英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [比爾·香克利](../Page/比爾·香克利.md "wikilink")（1974年獲勳），英國[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [埃姆林·休斯](../Page/埃姆林·休斯.md "wikilink")（1980年獲勳），英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [凱文·基岡](../Page/凱文·基岡.md "wikilink")（1982年獲勳），英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [加里·帕莱斯特](../Page/加里·帕莱斯特.md "wikilink")（1998年獲勳），英國[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [泰瑞·普萊契](../Page/泰瑞·普萊契.md "wikilink")（1998年獲勳），英國作家。
  - [簡·西摩](../Page/簡·西摩.md "wikilink")（1999年獲勳），英國[女演員](../Page/女演員.md "wikilink")。
  - [艾德敦](../Page/艾德敦.md "wikilink")（2000年獲勳），英國知名[指揮](../Page/指揮.md "wikilink")。
  - [J·K·羅琳](../Page/喬安·羅琳.md "wikilink")（2000年獲勳），英國[作家](../Page/作家.md "wikilink")。
  - [史蒂夫·戴維斯](../Page/史蒂夫·戴維斯.md "wikilink")（2001年獲勳），英國職業[士碌架選手](../Page/士碌架.md "wikilink")，早於1998年獲MBE。
  - [阿蘭·希勒](../Page/阿蘭·希勒.md "wikilink")（2002年獲勳），[英格蘭足球員](../Page/英格蘭.md "wikilink")。
  - [阿爾塞納·溫格](../Page/阿爾塞納·溫格.md "wikilink")（2003年獲勳），[法國著名足球教練](../Page/法國.md "wikilink")。
  - [碧咸](../Page/大衛碧咸.md "wikilink")（2003年獲勳），[英格蘭著名足球員](../Page/英格蘭.md "wikilink")。
  - [皮爾斯·布魯斯南](../Page/皮爾斯·布魯斯南.md "wikilink")（2003年獲勳），[愛爾蘭裔美籍電影演員](../Page/愛爾蘭.md "wikilink")。
  - [热拉尔·乌利耶](../Page/热拉尔·乌利耶.md "wikilink")（2003年獲勳），法國足球教練。
  - [强尼·威尔金森](../Page/强尼·威尔金森.md "wikilink")（2004年获勋），前英格兰国家橄榄球队队长，率领英格兰夺得2003年橄榄球世界杯（橄榄球世界杯是仅次于IOC奥运会、FIFA世界杯的世界第三大赛事），决赛中，在剩下26秒，威尔金森射进了定点球，以20-17击败对手，率领英格兰夺得第一个世界杯冠军。
  - [戈登·拉姆齊](../Page/戈登·拉姆齊.md "wikilink")（2006年獲勳），[蘇格蘭](../Page/蘇格蘭.md "wikilink")[廚師](../Page/廚師.md "wikilink")、餐廳老闆。
  - [萊恩·吉格斯](../Page/萊恩·吉格斯.md "wikilink")（2007年獲勳），[威爾士足球員](../Page/威爾士.md "wikilink")。
  - [-{zh-hans:马克·休斯;
    zh-hant:馬克曉士;}-](../Page/马克·休斯.md "wikilink")（2007年獲勳），教練及前[威爾士足球員](../Page/威爾士.md "wikilink")。
  - [凯莉·米洛](../Page/凯莉·米洛.md "wikilink")（2008年獲勳），[澳洲歌手](../Page/澳洲.md "wikilink")、曲作家、演员。
  - [罗斯·布朗](../Page/罗斯·布朗.md "wikilink")（2010年获勋），英国[一级方程式车队技术总监](../Page/一级方程式.md "wikilink")。
  - [亞德里安·紐維](../Page/亞德里安·紐維.md "wikilink")（2011年获勋），英国[一级方程式车队技术总监](../Page/一级方程式.md "wikilink")。
  - [蓋瑞·巴洛](../Page/蓋瑞·巴洛.md "wikilink")（2012年获勋），英国歌手、曲作家。
  - [艾迪·瑞德曼](../Page/艾迪·瑞德曼.md "wikilink")（2015年獲勳），英國演員。

#### 日期不詳

  - [克勞德·奧金萊克爵士](../Page/克勞德·奧金萊克.md "wikilink")，英國陸軍元帥。
  - [夏佳理](../Page/夏佳理.md "wikilink")，前香港立法會議員。
  - [詹伯樂](../Page/詹伯樂.md "wikilink")，[九廣鐵路公司行政總裁](../Page/九廣鐵路公司.md "wikilink")
  - [艾華士](../Page/艾華士.md "wikilink")，[二次大戰英國退伍軍人](../Page/二次大戰.md "wikilink")，[日軍俘虜](../Page/日軍.md "wikilink")。戰後致力搜集日本侵華證據，早年曾獲MBE。
  - [戴蒙·希爾](../Page/戴蒙·希爾.md "wikilink")，英國賽車手。
  - [加利·連尼加](../Page/加利·連尼加.md "wikilink")，英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [苗學禮](../Page/苗學禮.md "wikilink")，港府官員。
  - [鮑勃·佩斯利](../Page/鮑勃·佩斯利.md "wikilink")，英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [布萊恩·羅布森](../Page/布萊恩·羅布森.md "wikilink")，英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [彼德·希爾頓](../Page/彼德·希爾頓.md "wikilink")，英格蘭[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，早年曾獲MBE。
  - [戈登·斯特羅恩](../Page/戈登·斯特羅恩.md "wikilink")，蘇格蘭足球員。
  - [杰奎琳·杜·普蕾](../Page/杰奎琳·杜·普蕾.md "wikilink")，大提琴演奏家。

### MBE勳銜

  - [希思爵士](../Page/愛德華·希思.md "wikilink")（1946年獲勳），前[英國首相](../Page/英國首相.md "wikilink")。

  - [尤德爵士](../Page/尤德.md "wikilink")（1949年獲勳），前[香港總督](../Page/香港總督.md "wikilink")。

  - [保罗·麦卡特尼爵士](../Page/保罗·麦卡特尼.md "wikilink")（1965年獲勳），[披頭四樂隊成員](../Page/披頭四樂隊.md "wikilink")，他在1965年連同樂隊其餘3名成員一同獲勳，但其爵士頭銜是因為他在1997年成為[下級勳位爵士而來](../Page/下級勳位爵士.md "wikilink")。

  - [約翰·連儂](../Page/約翰·連儂.md "wikilink")（1965年獲勳[9](http://www.guardian.co.uk/gallery/image/0,8543,-10804303037,00.html)），[披頭四樂隊成員](../Page/披頭四樂隊.md "wikilink")，但到了1969年，因為反對英國政府對不同戰爭的立場，他把勳章退回頒授者英女皇。

  - [吉奧夫·赫斯特爵士](../Page/吉奧夫·赫斯特.md "wikilink")，（1975年獲勳），英格蘭足球員，後於1998年成為[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。

  - [郭慎墀](../Page/郭慎墀.md "wikilink")（1976年獲勳），[香港](../Page/香港.md "wikilink")[拔萃男書院第七任校長](../Page/拔萃男書院.md "wikilink")。

  - [尼古拉斯·溫頓爵士](../Page/尼古拉斯·溫頓.md "wikilink")（1983年獲勳），[倫敦證券交易所交易員](../Page/倫敦證券交易所.md "wikilink")、[人道主義者](../Page/人道主義.md "wikilink")，後於2002年成為[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。

  - [郭利民](../Page/郭利民.md "wikilink")（1987年獲勳），香港資深[唱片騎師](../Page/唱片騎師.md "wikilink")。

  - [潘靈卓](../Page/潘靈卓.md "wikilink")（1988年獲勳），[宣教士](../Page/宣教士.md "wikilink")，在1966年從英國到香港幫助當時住在九龍城寨的吸毒者、妓女及邊緣青年行回正路。

  - [史蒂芬·亨得利](../Page/史蒂芬·亨得利.md "wikilink")（1994年獲勳），英國[士碌架選手](../Page/士碌架.md "wikilink")。

  - [吉米·懷特](../Page/吉米·懷特.md "wikilink")（1999年獲勳），英國[士碌架選手](../Page/士碌架.md "wikilink")。

  - [戴圖理](../Page/戴圖理.md "wikilink")（2000年獲勳），[騎師](../Page/騎師.md "wikilink")。

  - [彼得·舒梅切爾](../Page/彼得·舒梅切爾.md "wikilink")（2001年獲勳），[丹麥足球員](../Page/丹麥.md "wikilink")。

  - [傑米·奧利弗](../Page/傑米·奧利弗.md "wikilink")（2003年獲勳），[英國著名](../Page/英國.md "wikilink")[廚師](../Page/廚師.md "wikilink")。

  - [马克·威廉姆斯](../Page/马克·威廉姆斯.md "wikilink")（2004年獲勳），英國[士碌架選手](../Page/士碌架.md "wikilink")。

  - [萊斯·費迪南德](../Page/萊斯·費迪南德.md "wikilink")（2005年獲勳），[英國足球員](../Page/英國.md "wikilink")。

  - [史蒂文·杰拉德](../Page/史蒂文·杰拉德.md "wikilink")（2006年獲勳），[利物浦队足球員](../Page/利物浦队.md "wikilink")。

  - [亨里克·拉爾森](../Page/亨里克·拉爾森.md "wikilink")（2006年獲勳），[瑞典足球員](../Page/瑞典.md "wikilink")。

  - [舒寧咸](../Page/舒寧咸.md "wikilink")（2007年獲勳），英國足球員。

  - [約翰·希金斯](../Page/約翰·希金斯.md "wikilink")（2008年獲勳），英國[士碌架選手](../Page/士碌架.md "wikilink")。

  - [刘易斯·汉密尔顿](../Page/刘易斯·汉密尔顿.md "wikilink")（2009年获勋），英国[一级方程式车手](../Page/一级方程式.md "wikilink")。

  - [简森·巴顿](../Page/简森·巴顿.md "wikilink")（2010年获勋），英国[一级方程式车手](../Page/一级方程式.md "wikilink")。

  - [愛黛兒](../Page/愛黛兒.md "wikilink")（2013年獲勳），英國歌手。

  - （2013年獲勳），英國歌手。

  - [約翰·巴洛曼](../Page/約翰·巴洛曼.md "wikilink")（2014年獲勳），美國演員 (英美雙重國籍)。

  - [默希濂](../Page/默希濂.md "wikilink")（2016年獲勳），澳門大學助理教授。

  - [杰克·劳格赫](../Page/杰克·劳格赫.md "wikilink")（2017年獲勳），英國跳水運動員。

  - [克里斯·米尔斯](../Page/克里斯·米尔斯_\(跳水运动员\).md "wikilink")（2017年獲勳），英國跳水運動員。

  - [馬克斯·惠特洛克](../Page/馬克斯·惠特洛克.md "wikilink")（2017年獲勳），英國體操運動員。

  - [賈斯汀·羅斯](../Page/賈斯汀·羅斯.md "wikilink") （2017年獲勳），英國高爾夫球選手。

#### 日期不詳

  - [托尼·亞當斯](../Page/托尼·亞當斯.md "wikilink")，英格蘭足球員。
  - [肯尼·達格利什](../Page/肯尼·達格利什.md "wikilink")，蘇格蘭足球員。
  - [伊恩·拉什](../Page/伊恩·拉什.md "wikilink")，威爾斯足球員。

## 相關條目

  - [大英帝國](../Page/大英帝國.md "wikilink")
  - [大英帝國勳章](../Page/大英帝國勳章.md "wikilink")

## 注腳

## 參考資料

  - [倫敦憲報](http://www.london-gazette.co.uk/)（各年）

[\*](../Category/大英帝國勳章.md "wikilink")
[英國授勳者](../Category/英國授勳者.md "wikilink")

1.  [李孝式](http://www.wutongzi.com/a/351839.html)梧桐子，2015年5月28日
2.
3.  [董事會簡歷](http://www.mediachinesegroup.com/pdf/c_MCI_AR_2013-14_4_Profile_of_Board_of_Directors.pdf)世界華文媒體有限公司
4.  "[Supplement to
    Issue 54794](http://www.london-gazette.co.uk/issues/54794/supplements/25)",
    *London Gazette*, 13 June 1997, p.25.
5.  [沙巴客家人](http://www.hakkaworld.com.tw/files/world/ma001.htm)客家世界網
6.  [掌舵亚航促进马英关系．东尼获英女王颁勋章](http://www.sinchew.com.my/node/1224248)星洲網，2011年4月1日