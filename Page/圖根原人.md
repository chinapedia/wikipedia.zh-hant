**圖根原人**（*Orrorin
tugenensis*），又名**千年人**、**千禧猿**或**土根猿**，是已知最古老與[人類有關的](../Page/人類.md "wikilink")[人族祖先](../Page/人族.md "wikilink")，是原人屬（或稱**千年人屬**）中的唯一種。[種小名是以其](../Page/種小名.md "wikilink")[化石發現地](../Page/化石.md "wikilink")[肯雅的](../Page/肯雅.md "wikilink")[圖根山區命名](../Page/圖根山區.md "wikilink")。利用[放射性測年技術](../Page/放射性測年.md "wikilink")、[凝灰岩與](../Page/凝灰岩.md "wikilink")[動物群的關係及磁性地層學](../Page/動物群.md "wikilink")，估計出發現化石的地層是屬於610-580萬年前的[中新世](../Page/中新世.md "wikilink")。這個發現成為了最古老人科是雙足行走的證據。

發現的化石最少是來自5個個體。化石包括了[大腿骨](../Page/大腿骨.md "wikilink")、右[肱骨及](../Page/肱骨.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")。從大腿骨得知他們是直立行走的，肱骨部份可見是能攀樹（但非[臂行](../Page/臂行.md "wikilink")），而牙齒則顯示他們的食性與現今人類相似。另外在大腿骨後部有閉孔外肌溝，可見他們是雙足行走的。從[丘狀齒](../Page/丘狀齒.md "wikilink")、有微齒的[臼齒及細小的](../Page/臼齒.md "wikilink")[犬齒可見他們主要是吃](../Page/犬齒.md "wikilink")[生果及](../Page/生果.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")，有時會吃肉。千年人屬約有現今[黑猩猩的大小](../Page/黑猩猩.md "wikilink")。

這些化石是由[馬丁·匹克福特](../Page/馬丁·匹克福特.md "wikilink")（Martin
Pickford）及[瑞吉特·森努特](../Page/瑞吉特·森努特.md "wikilink")（Brigitte
Senut）所帶領的研究隊於2000年發現。\[1\]從他們是雙足行走及牙齒解剖結論出他們是屬於人族。故此，他們是生存在人族及[非洲](../Page/非洲.md "wikilink")[類人猿分裂的時期](../Page/類人猿.md "wikilink")，即700萬年前。這個測年法與[分子鐘方法有所不同](../Page/分子鐘.md "wikilink")，但卻普遍被學界所接受。

若圖根原人被證實是人類的直接祖先，如[阿法南方古猿等的](../Page/阿法南方古猿.md "wikilink")[南方古猿類則會成為人科的](../Page/南方古猿類.md "wikilink")[側系群](../Page/側系群.md "wikilink")。圖根原人比阿法南方古猿早了300萬年出現，並且其大腿骨更為接近現今人類。\[2\]不過就此仍有很多的討論。

其他在[陸肯諾岩層發現的化石](../Page/陸肯諾岩層.md "wikilink")（如[樹葉及其他](../Page/樹葉.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")）顯示圖根原人生活在乾旱常綠的[森林環境](../Page/森林.md "wikilink")，而不像很多[人類演化所假設的](../Page/人類演化.md "wikilink")[大草原](../Page/大草原.md "wikilink")。故此，雙足行走是起源於樹棲祖先，而非四足行走的動物。有指人類祖先可能像現今[猩猩般可以在樹上以雙足行走](../Page/猩猩.md "wikilink")，以雙手作為平衡及保持雙腳挺直。現時人類近親的[大猩猩及黑猩猩都發展成可以伸縮的姿勢](../Page/大猩猩.md "wikilink")，更為適合攀樹及在陸地上四足行走。根據小數的學者所指，圖根原人的手腕骨像人類般是融合及加固了的，可見他們仍會用手指行走。\[3\]\[4\]\[5\]

## 參考

## 外部連結

  - [Martin Pickford answers a few questions about this month's fast
    breaking paper in field of
    Geosciences](https://web.archive.org/web/20020602053619/http://esi-topics.com/fbp/comments/december-01-Martin-Pickford.html)
  - [BBC News: First chimpanzee fossils
    found](http://news.bbc.co.uk/2/hi/science/nature/4201666.stm)

[Category:早期人類](../Category/早期人類.md "wikilink")
[O](../Category/人亞族.md "wikilink")
[O](../Category/中新世生物.md "wikilink")

1.
2.
3.
4.
5.