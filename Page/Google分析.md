**Google分析**（Google
Analytics）是一個由[Google所提供的](../Page/Google公司.md "wikilink")[網站流量](../Page/網站.md "wikilink")[統計服務](../Page/統計.md "wikilink")。Google
分析（Analytics）现在是互联网上使用最广泛的网络分析服务.Google
Analytics还提供了一个SDK，允许从iOS和Android应用程序收集使用数据，称为Google
Analytics for Mobile Apps。\[1\]

## 概要

**Google
Analytics**原為**Urchin**所營運的付費網站流量統計服務，在2005年4月時，Google宣佈購併Urchin公司並將原本需要付費的服務開放免費使用。其基本版本是免费使用，高级版本需要额外费用。\[2\]

## 技術

只要在欲觀察的頁面放入**GA**所提供的一小段[JavaScript代碼後](../Page/JavaScript.md "wikilink")，每當執行這個網頁時，即會傳送如：瀏覽者的所在國家、經由什麼關鍵字進入該頁等相關資料至GA伺服器，並整合成易讀的資訊給網站站長。

### 缺点

1.  當使用者使用Firefox，並啟用如NOScript或是Adblock等附加元件時，GA的JS代碼將無法運行，也就無法傳送相關資料。
2.  若使用者啟用如[tor的隱私保護軟體時](../Page/tor.md "wikilink")，所傳送的資料亦可能因被隱蔽而不準確。

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 參考資料

## 外部連結

  - [Google Analytics](http://www.google.com/intl/en/analytics/)

      - [Google分析](http://www.google.cn/intl/zh-CN/analytics/)
      - [Google分析](http://www.google.com/intl/zh-TW/analytics/)

  - [官方開發網誌](http://analytics.blogspot.com/)

  - [Google
    Analytic追蹤程式原始碼](http://harshad.wordpress.com/google-analytics-urchin-module)

[Category:Google](../Category/Google.md "wikilink")

1.
2.  <http://www.google.com/analytics/premium/capabilities.html>