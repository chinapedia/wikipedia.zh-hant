**施存统**（），又名**施复亮**，化名**方国昌**，学者，[中共早期活动家](../Page/中共.md "wikilink")。

## 生平

1899年生于[浙江](../Page/浙江.md "wikilink")[金华县](../Page/金华县.md "wikilink")[源东乡](../Page/源东乡.md "wikilink")。1917年入[浙江省立第一师范学校](../Page/浙江省立第一师范学校.md "wikilink")（今[杭州高级中学](../Page/杭州高级中学.md "wikilink")）。1919年[五四運動前后](../Page/五四運動.md "wikilink")，发起成立研究新文化的「新生学社」，并参与创办《浙江新潮》，在第2期上发表反对傳統家庭制度的《非孝》一文，引发一师风潮。1919年底，离开[杭州赴](../Page/杭州.md "wikilink")[北京](../Page/北京.md "wikilink")。1920年1月加入“[北京工读互助团](../Page/北京工读互助团.md "wikilink")”，3月，该团解散，回到[上海](../Page/上海.md "wikilink")。经[俞秀松介绍](../Page/俞秀松.md "wikilink")，认识《[新青年](../Page/新青年.md "wikilink")》的主编[陈独秀](../Page/陈独秀.md "wikilink")，从此抛弃无政府主义，转向[马克思主义](../Page/马克思主义.md "wikilink")，加入[上海共产主义小组](../Page/上海共产主义小组.md "wikilink")，参与成立马克思主义研究会，成为[中国共产党最早的党员之一](../Page/中国共产党.md "wikilink")。8月参与成立中国社会主义青年团。同年赴[日本留学](../Page/日本.md "wikilink")，创立[东京共产主义小组](../Page/东京共产主义小组.md "wikilink")\[1\]。

1922年被日本驱逐回国，出席[中共二大](../Page/中共二大.md "wikilink")。同年5月在[中国社会主义青年团一大上当选团中央书记](../Page/中国社会主义青年团.md "wikilink")，兼任团中央机关报《先驱》的主编。1924年1月，离开团中央，任中共上海区执行委员会委员长，后到[上海大学](../Page/上海大学.md "wikilink")、广东[中山大学](../Page/中山大学.md "wikilink")、[黄埔军校](../Page/黄埔军校.md "wikilink")、[广州农民运动讲习所任教](../Page/广州农民运动讲习所.md "wikilink")。1927年初任[中央军事政治学校武汉分校教官](../Page/中央军事政治学校.md "wikilink")、政治部主任。同年[国共分裂后脱离中国共产党](../Page/国共分裂.md "wikilink")。

后来他曾任[上海大陆大学教授](../Page/上海大陆大学.md "wikilink")、[广西大学教授](../Page/广西大学.md "wikilink")。1929年后，从事译著马克思主义、革命理论和经济学著作工作，先后翻译三十几本著作，发表论文近百篇。1937年之后抗战期间，为文化界救国会领导人之一。1945年底与[黄炎培](../Page/黄炎培.md "wikilink")、[章乃器等發起](../Page/章乃器.md "wikilink")[中國民主建国会](../Page/中國民主建国会.md "wikilink")（民建）。曾当选为民建中央常委和副主席。

1949年他作为民建代表出席[中国人民政治协商会议第一届全体会议](../Page/中国人民政治协商会议第一届全体会议.md "wikilink")，被选为[第一届全国政协常务委员兼副秘书长](../Page/第一届全国政协.md "wikilink")。后任[劳动部第一副部长](../Page/中華人民共和國勞動部.md "wikilink")。

1970年11月29日他病逝于[北京](../Page/北京.md "wikilink")。

## 著作

他翻译了许多马列主义著作，写作出版了许多经济学专著。著有《现代唯物论》、《中国现代经济史》、译有《资本制度浅说》、《世界史纲》、《社会进化论》等。
笔名光亮、亮、伏量、子充、子由、半解等。

## 家庭

在上海大学任教授时，施复亮与学生钟复光结为伴侣。改名为施复亮。

  - 第一任妻子[王一知](../Page/王一知.md "wikilink")，后離婚改嫁[张太雷](../Page/张太雷.md "wikilink")。
  - 第二任妻子[钟复光](../Page/钟复光.md "wikilink")。
  - 子，[施光南](../Page/施光南.md "wikilink")，作曲家，钟复光所生。

## 参考文献

  - 石川祯浩：〈[青年时期的施存统——“日本小组”与中共建党的过程](http://www.nssd.org/articles/article_read.aspx?id=1003371046)〉。

{{-}}

[C](../Category/施姓.md "wikilink") [S施](../Category/金华人.md "wikilink")
[S施](../Category/中央人民政府劳动部副部长.md "wikilink")
[Category:第二届全国人大常委会委员](../Category/第二届全国人大常委会委员.md "wikilink")
[Category:第三届全国人大常委会委员](../Category/第三届全国人大常委会委员.md "wikilink")
[Category:第二届全国政协常务委员](../Category/第二届全国政协常务委员.md "wikilink")
[Category:第三届全国政协常务委员](../Category/第三届全国政协常务委员.md "wikilink")
[Category:第四届全国政协常务委员](../Category/第四届全国政协常务委员.md "wikilink")
[Category:第一届全国政协常务委员](../Category/第一届全国政协常务委员.md "wikilink")
[Category:中共二大代表](../Category/中共二大代表.md "wikilink")

1.