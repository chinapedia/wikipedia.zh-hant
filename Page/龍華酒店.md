[HK_LungWahHotel.JPG](https://zh.wikipedia.org/wiki/File:HK_LungWahHotel.JPG "fig:HK_LungWahHotel.JPG")
**龍華酒店**（**Lung Wah
Hotel**）是一家位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[沙田下禾輋村的](../Page/沙田_\(香港\).md "wikilink")[酒店及餐廳](../Page/旅館.md "wikilink")，為香港僅有的園林酒店之一。它開幕於1938年，共有十二間客房。當年是沙田區唯一的酒店，過去一直作為酒店經營，已故著名武俠小說家[金庸更曾於酒店長住創作小說](../Page/金庸.md "wikilink")。但自1980年代起，随着沙田新城市發展及沙田新酒店落成．龍華酒店客房逐漸荒廢，又因未能合符香港的《消防條例》，所以開始關閉酒店業務。

1985年開始，龍華酒店開始轉型以純粹[餐館方式營運](../Page/餐館.md "wikilink")，以[紅燒乳鴿為招牌菜](../Page/紅燒乳鴿.md "wikilink")<small>\[1\]</small>，以老饕聞名的末代[港督](../Page/港督.md "wikilink")[彭定康亦是熟客](../Page/彭定康.md "wikilink")<small>\[2\]</small>，而前[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣經國更派專機來港購買乳鴿](../Page/蔣經國.md "wikilink")，故獲「飛天乳鴿」的美名\[3\]。

龍華酒店在1950年代是香港著名的園林酒店，因此當年不少電影公司借用地方拍攝電影，如[紅線女](../Page/紅線女.md "wikilink")、[張活游](../Page/張活游.md "wikilink")、[陳寶珠](../Page/陳寶珠.md "wikilink")、[蕭芳芳](../Page/蕭芳芳.md "wikilink")、[李小龍等](../Page/李小龍.md "wikilink")。至今，由於其裝修仍保留著五六十年代的風格，因此不少電影在龍華酒店取景，如李小龍的《[唐山大兄](../Page/唐山大兄.md "wikilink")》、[彭浩翔的](../Page/彭浩翔.md "wikilink")《[買凶拍人](../Page/買凶拍人.md "wikilink")》等。

香港老牌歌手[李龍基的專輯](../Page/李龍基.md "wikilink")《港澳回憶歌集》中有一首名為「龍華酒店」的情歌<small>\[4\]</small>。

## 歷史

[HK_LungWahHotel_SqaureHouse.JPG](https://zh.wikipedia.org/wiki/File:HK_LungWahHotel_SqaureHouse.JPG "fig:HK_LungWahHotel_SqaureHouse.JPG")
龍華酒店是香港最古老的酒店之一，建於[戰前的](../Page/第二次世界大戰.md "wikilink")1938年，原為東主鍾秀長的家族的渡假別墅，仿照[九龍塘的住宅建築](../Page/九龍塘.md "wikilink")，樓高兩層，外型呈[四方形](../Page/四方形.md "wikilink")，被當地村民稱為「四方樓」。

[日佔時期這幢鍾家別墅被徵用作軍隊總部](../Page/香港日佔時期.md "wikilink")，部分用為馬槽，四周空地成日軍駐紮的軍營，戰後才從日軍手上取回。龍華酒店是戰後於沙田開設的首間酒店，下層是食肆，上層是客房，初期只有8間套房，後來增加到12間房。當時主要的客源是位於[何東樓的](../Page/何東樓.md "wikilink")[華僑工商學院的師生](../Page/華僑工商學院.md "wikilink")，並首創「龍華乳鴿」及「生滾雞粥」，其招牌「雞肥鴿嫩」正是來自華僑工商學院校長[王淑淘女士的手筆](../Page/王淑淘.md "wikilink")。

龍華酒店早年佔地廣闊，現時的沙田警署是當年酒店的[停車場](../Page/停車場.md "wikilink")，而停車場外已是未填海前的[沙田海](../Page/沙田海.md "wikilink")。當[九廣鐵路擴建](../Page/九廣鐵路.md "wikilink")，[政府與鍾氏家族協商收回大片土地拉直鐵路](../Page/港英政府.md "wikilink")。

政府當年應承换地另補土地予鍾氏，至今尚未有结論。

## 龍華乳鴿

美食家[唯靈稱沙田有三寶](../Page/唯靈.md "wikilink")，是「山水[豆腐花](../Page/豆腐花.md "wikilink")」、「明火靚[雞粥](../Page/沙田雞粥.md "wikilink")」和「龍華[乳鴿](../Page/乳鴿.md "wikilink")」。以前龍華最高記錄可以每日賣六千隻乳鴿，現在每天也有上千隻銷量，龍華酒店專門聘請一名[廚師炮製乳鴿](../Page/廚師.md "wikilink")，只選用孵化不超過26天的嫩鴿，在[深圳自家設置的](../Page/深圳.md "wikilink")[農場飼養](../Page/農場.md "wikilink")，分為只有11至12[兩重的](../Page/兩.md "wikilink")「皇鴿」或只有8至10兩重的「頂鴿」，先將鴿用滾水輕烚，再用[滷水汁浸熟](../Page/滷水.md "wikilink")，在鴿皮上[麥芽糖然後再炸](../Page/麥芽糖.md "wikilink")，保持鴿皮色鮮香脆。

## 龍華生活文化村

龍華酒店於2009年成立「龍華生活文化村」，舉辦展覽之餘，又廣邀名嘴、才子舉行座談會，推廣保育文化。首個活動是於10月2日正式舉行的《昔日童玩遊戲展》，地點在由龍華麻雀房變身為展覽廳之懷舊舞台；龍華大花園中的兩座麻雀亭，亦改成文物圖片及龍華博物館。

## 交通

交通方面，可從[港鐵](../Page/港鐵.md "wikilink")[沙田站前往](../Page/沙田站.md "wikilink")，往[新城市中央廣場](../Page/新城市中央廣場.md "wikilink")（[宜家沙田分店](../Page/宜家.md "wikilink")）方向步行前往十分鐘左右。亦可由[沙田](../Page/沙田.md "wikilink")[瀝源邨步行十分鐘前往](../Page/瀝源邨.md "wikilink")，由沙田警署旁的行人天橋跨過港鐵路軌即可到達。也可乘巴士或小巴在[瀝源邨下車](../Page/瀝源邨.md "wikilink")。

## 註腳

<div class="references-small">

<references />

</div>

## 参考文献

  - 韋然 編著，《黃花紅酒醉龍華》，萬里機構，ISBN 978-962-14-3741-9

## 外部連結

  - [龍華酒店](http://www.lungwahhotel.hk/)
  - [龍華酒店](http://www.lungwahhotel.com.hk/)

[Category:沙田區食肆](../Category/沙田區食肆.md "wikilink")
[Category:禾輋](../Category/禾輋.md "wikilink")
[Category:香港旅遊景點](../Category/香港旅遊景點.md "wikilink")

1.  [名店推荐:陈年卤水VS招牌烧汁
    香港乳鸽王](http://gd.sohu.com/20070115/n247615617.shtml)
2.  [肥彭挑戰港式小食](http://appledaily.atnext.com/template/apple_sub/art_main.cfm?iss_id=20051114&sec_id=38163&subsec_id=5300052&art_id=5393641)
3.
4.  [李龍基的港澳回憶歌集](http://www.waiyinworkshop.com.hk/Reception/Reception_14.htm)