**Edubuntu**是[Ubuntu
Linux的教育分支](../Page/Ubuntu.md "wikilink")，並包含了[Linux終端機伺服器計畫](../Page/Linux終端機伺服器計畫.md "wikilink")，以及大量的教育應用程序，例如[GCompris](../Page/GCompris.md "wikilink")、[KDE教育计划和Schooltool](../Page/KDE教育计划.md "wikilink")
Calendar。

Edubuntu在2005年10月13日推出首個版本，與Ubuntu5.10推出時間相同。現時Edubuntu最新的穩定版本為14.04.2，发布于2015年2月。

## 計畫目標

Edubuntu的首要目標是供電腦技術知識有限的教育工作者設立及管理電腦實驗室或在線的學習環境。Edubuntu設計的原則集中在設置、用家和程序的管理。

基本上Edubuntu是建立在Ubuntu的核心上，並且合併了[LTSP連線體制](../Page/LTSP.md "wikilink")。而且與教育有關的[套件均經過挑選](../Page/軟體套件.md "wikilink")，對像為6-18歲的[兒童與](../Page/兒童.md "wikilink")[青少年](../Page/青少年.md "wikilink")。它同時針對那些沒資源購買新電腦作教學而只可用舊電腦作教學的小型學校，令其可充份運用資源。

## 個人參與

作為一個使用者，亦有很多途徑幫忙開發Edubuntu，以下列舉三種主要方法：

  - 幫忙編寫Edubuntu說明文檔。
  - 參與EdubuntuArtwork，幫忙設計Edubuntu的圖案。
  - 幫忙測試Edubuntu並回饋意見予開發者。

## 已使用Edubuntu的组织

  - 在[美國](../Page/美國.md "wikilink")[維吉尼亞州的Yorktown](../Page/維吉尼亞州.md "wikilink")
    High School的Colin Applegate and Jeff Elkner。
  - Shuttleworth
    Foundation的網路實驗室已開始使用Edubuntu，並命名為Skubuntu。其已於[南非五十所學校完成安裝](../Page/南非.md "wikilink")，並計劃在2006年3月將覆蓋範圍擴至200間學校。

## 外部連結

  - [Edubuntu官方網站](http://www.edubuntu.org)

[Category:Ubuntu衍生版](../Category/Ubuntu衍生版.md "wikilink")
[Category:自由教育軟體](../Category/自由教育軟體.md "wikilink")