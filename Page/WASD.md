[W-A-S-D.jpg](https://zh.wikipedia.org/wiki/File:W-A-S-D.jpg "fig:W-A-S-D.jpg")
**WASD**
或**WSAD**\[1\]是傳統[QWERTY鍵盤上的四個按鍵](../Page/QWERTY鍵盤.md "wikilink")，位於[鍵盤的左手面](../Page/鍵盤.md "wikilink")。這四個按鍵在[第一人稱射擊遊戲上通常作為控制玩家角色的移動](../Page/第一人稱射擊遊戲.md "wikilink")\[2\]。W和S鍵用來控制角色向前或向後移動，A和D鍵則用來控制角色左右移動。這組按鍵還有另一個變化W-A-X-D，用以遷就一些習慣使用數字鍵盤的方向按鍵（8-4-6-2）的使用者。

一般認為WASD操作法是[CS所帶起的習慣](../Page/絕對武力.md "wikilink")。現今許多FPS和3D冒險遊戲都支援。

大部份[電腦遊戲使用WASD用來替代方向鍵是有很多理由的](../Page/電腦遊戲.md "wikilink")，其中因為WASD被其他按鍵包圍，因此玩家可以很方便的按下其他按鍵以使用其他遊戲的指令。另外當玩家是使用右手作為主手時，WASD鍵的位置比其他按鍵的位置更為方便和舒適（一隻手留在鍵盤上，另一隻手則留在[滑鼠上時](../Page/滑鼠.md "wikilink")）。不過，以左手作為主手的玩家則會認為WASD鍵十分不便，而傾向使用方向鍵。

## 其他變化

有一些玩家傾向於使用ESDF（如，著名的FPS，部落系列游戏），因為這樣能方便[小指按下更多的按鍵](../Page/小指.md "wikilink")，同時這亦是一個自然的打字手勢。這組按鍵的最大優勢是在鍵盤上的F鍵有一根橫槓，因此玩家可以以食指輕鬆地在鍵盤上定位。這一組按鍵曾經是少數遊戲中預設用來移動的按鍵，例如文明
2。

還有另一組常用的按鍵HJKI（其中HJKI分別代表左下上右）用在[Unix電腦系統](../Page/Unix.md "wikilink")，例如[Vi文字編輯器](../Page/Vi.md "wikilink")。也有遊戲使用這一組按鍵，例如[NetHack](../Page/NetHack.md "wikilink")。

相類似的按鍵組合還有IJKL，用於小數以[DHTML或](../Page/DHTML.md "wikilink")[JavaScript寫成的遊戲](../Page/JavaScript.md "wikilink")。這些[瀏覽器遊戲無法使用方向鍵](../Page/瀏覽器.md "wikilink")，因為方向鍵會導致瀏覽器視窗捲動，這樣將會妨礙遊戲的進行。

ZXC按鍵組合常用於免費的遊戲，同時亦是[模擬器及其他舊](../Page/模擬器.md "wikilink")2D遊戲的預設按鍵。這一組按鍵通常用作控制移動外的其他用途。

## 註解

<references/>

[Category:電子遊戲術語](../Category/電子遊戲術語.md "wikilink")
[Category:鍵盤配置](../Category/鍵盤配置.md "wikilink")

1.  對應了中文的「上下左右」。
2.