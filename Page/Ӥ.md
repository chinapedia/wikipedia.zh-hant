**Ӥ**, **ӥ**
是一个在[乌德穆尔特语使用的](../Page/乌德穆尔特语.md "wikilink")[西里尔字母](../Page/西里尔字母.md "wikilink")，由
[И, и](../Page/И.md "wikilink") 加上[分音符号而成](../Page/分音符号.md "wikilink")。

## 字母的次序

在乌德穆尔特语字母中排第13位。

## 音值

通常为 。

## 字符編碼

<table>
<thead>
<tr class="header">
<th><p>大小寫</p></th>
<th><p>字母</p></th>
<th><p>字符組合</p></th>
<th><p>Unicode碼</p></th>
<th><p>名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>大寫</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td><p><code>U+0418</code><br />
<code>U+0308</code></p></td>
<td><p>西里爾大寫字母<br />
帶分音符的I</p></td>
</tr>
<tr class="even">
<td><p>小寫</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td><p><code>U+0438</code><br />
<code>U+0308</code></p></td>
<td><p>西里爾小寫字母<br />
帶分音符的I</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 參見

  - <span lang="en" style="font-size:120%;">[Ɛ ɛ](../Page/Ɛ.md "wikilink")</span>（[拉丁字母](../Page/拉丁字母.md "wikilink")）
  - [Unicode中的西里爾字母](../Page/Unicode中的西里爾字母.md "wikilink")

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")