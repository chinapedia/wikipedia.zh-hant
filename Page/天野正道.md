**天野正道**（，）出生於[日本](../Page/日本.md "wikilink")[秋田縣](../Page/秋田縣.md "wikilink")[秋田市](../Page/秋田市.md "wikilink")，是一位日本[作曲家](../Page/作曲家.md "wikilink")、[編曲家以及](../Page/編曲家.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")。

## 生平

他在[東京](../Page/東京_\(消歧義\).md "wikilink")[國立音樂大學就讀](../Page/國立音樂大學.md "wikilink")，1982年畢業，獲得[武岡賞](../Page/武岡賞.md "wikilink")。他的交響樂團作品充滿著史詩、雄偉、傳奇，包括《[大逃殺](../Page/大逃殺.md "wikilink")》（）、《[鐵甲人-地球靜止之日](../Page/鐵甲人-地球靜止之日.md "wikilink")》（），以及《[星空防衛隊](../Page/星空防衛隊.md "wikilink")》（）的音樂。他在電影及影片的音樂可分類成動作、[科幻以及詭異](../Page/科幻.md "wikilink")。天野正道所選擇的[交響樂團是](../Page/交響樂團.md "wikilink")[波蘭的](../Page/波蘭.md "wikilink")[華沙愛樂樂團](../Page/華沙愛樂樂團.md "wikilink")（Warsaw
Philharmonic
Orchestra）以及它附屬[合唱團](../Page/合唱團.md "wikilink")。然後他有時被批評其作品有抄襲之意，例如一再重覆使用相同的主題或部分主題。

他也寫了許多給[管樂團的曲目](../Page/管樂團.md "wikilink")，其作品有被[東京佼成管樂團選作錄音](../Page/東京佼成管樂團.md "wikilink")。他的[管樂編曲作品](../Page/管樂.md "wikilink")，亦曾多次於[全日本吹奏樂大賽被參賽隊伍用作自選曲而獲得金奬](../Page/全日本吹奏樂大賽.md "wikilink")。1976年至1982年[全日本吹奏樂大賽](../Page/全日本吹奏樂大賽.md "wikilink")，他的母校[秋田南高校曾六次憑他編曲的作品獲得金奬](../Page/秋田南高校.md "wikilink")。2001年全日本吹奏樂大賽一般組比賽，有四首歌曲是他編曲的，有3隊贏得了金奬。

天野正道的作曲風格與[理查德·瓦格纳](../Page/理查德·瓦格纳.md "wikilink")（）、[古斯塔夫·霍尔斯特](../Page/古斯塔夫·霍尔斯特.md "wikilink")（）、[谢尔盖·瓦西里耶维奇·拉赫玛尼诺夫](../Page/谢尔盖·瓦西里耶维奇·拉赫玛尼诺夫.md "wikilink")（）、[巴托克·贝拉](../Page/巴托克·贝拉.md "wikilink")（）、[莫里斯·拉威爾](../Page/莫里斯·拉威爾.md "wikilink")（）相似。跟他同時代的作曲家包括[佐橋俊彦](../Page/佐橋俊彦.md "wikilink")、[岩崎琢](../Page/岩崎琢.md "wikilink")、[菅野洋子](../Page/菅野洋子.md "wikilink")、[植松伸夫等](../Page/植松伸夫.md "wikilink")。

## 作品列表

### 動畫及電影

  - **
  - *[Battle Royale](../Page/大逃殺_\(電影\).md "wikilink")*
  - *[Battle Royale II: Requiem](../Page/大逃殺2.md "wikilink")*
  - **
  - *[鐵甲人-地球靜止之日](../Page/鐵甲人-地球靜止之日.md "wikilink")*
  - **
  - *[Melty Lancer](../Page/Melty_Lancer.md "wikilink")*
  - *[Miyuki](../Page/美雪、美雪.md "wikilink")*
  - *[Musa](../Page/武士_\(2001年電影\).md "wikilink")* (orchestra conductor
    only)
  - *[Nightmare Campus](../Page/Nightmare_Campus.md "wikilink")*
  - *[新海底軍艦](../Page/新海底軍艦.md "wikilink")*
  - *[Odin - Starlight
    Mutiny](../Page/Odin_-_Starlight_Mutiny.md "wikilink")*
  - *[Phantasy Star Universe](../Page/夢幻之星_新宇宙.md "wikilink")*
  - *[女棒甲子園](../Page/女棒甲子園.md "wikilink")*
  - **
  - *[Shiawase Sou no Okojo-san](../Page/頑皮小白貂.md "wikilink")*
  - **
  - *[Stratos 4](../Page/Stratos_4.md "wikilink")*
  - *[Stratos 4: Return to
    Base](../Page/Stratos_4:_Return_to_Base.md "wikilink")*
  - **
  - *[Super Atragon](../Page/Super_Atragon.md "wikilink")*
  - *[Tenamonya Voyagers](../Page/Tenamonya_Voyagers.md "wikilink")*
  - **
  - *[Urotsukidoji: Legend of the
    Overfiend](../Page/Urotsukidoji:_Legend_of_the_Overfiend.md "wikilink")*
  - **
  - *[MushiKing](../Page/甲蟲王者.md "wikilink")*

## 外部連結

  - [Phoenix Entertainment](http://www.phoenix-ent.co.jp/)
  - [Interview with Amano @
    <http://www.sakigake-adb.co.jp>](https://web.archive.org/web/20060910193809/http://www.sakigake-adb.co.jp/kyo/interview/amano/amano.html)

  - [Anime News
    Network](http://www.animenewsnetwork.com/encyclopedia/people.php?id=151)

  - [Internet Movie Data Base](http://www.imdb.com/name/nm0023916/)

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:動畫音樂作曲家](../Category/動畫音樂作曲家.md "wikilink")
[Category:日本电影音乐作曲家](../Category/日本电影音乐作曲家.md "wikilink")
[Category:國立音樂大學校友](../Category/國立音樂大學校友.md "wikilink")
[Category:秋田縣出身人物](../Category/秋田縣出身人物.md "wikilink")