**XYY三體**是一種[人類](../Page/人類.md "wikilink")[男性的](../Page/男性.md "wikilink")[性染色體](../Page/性染色體.md "wikilink")[疾病](../Page/疾病.md "wikilink")，正常的男性性染色體是XY，而XYY三體者多出一條[Y染色體](../Page/Y染色體.md "wikilink")，所以又稱「超雄綜合症」（super-male
syndrome）。

此病在各種性染色體疾病中比較常見，在男性新生[嬰兒的發病率是](../Page/嬰兒.md "wikilink")1/500到1/1000。患者身材高大，智力一般與正常人沒有過大的差別，據抽查統計，82%的智商比年齡相仿的兄弟姐妹稍低但不明顯（约低10\~15），9%與年齡相仿的兄弟姐妹智商相同。另外9%智商超過年齡相仿的兄弟姐妹，以往人們一直誤解XYY三體者犯罪率比較高，但最新的醫學研究表明XYY三體者犯罪率與常人無異。沒有經過診斷，患者往往不知道自己有XYY三體者。有醫學專家建議XYY三體不能算是一種疾病。\[1\]\[2\]

XYY的病因是父親产生精子形成時，[减数第二次分裂后期两条Y染色体未移向细胞两极](../Page/減數分裂.md "wikilink")，使个别精子細胞含有兩條Y染色體。若該精子和卵子結合，胚胎細胞便會有2條Y染色體与1条X染色体。

## 腳注

<references />

[Category:遗传学](../Category/遗传学.md "wikilink")
[Category:症候群](../Category/症候群.md "wikilink")
[Category:性染色体非整倍性](../Category/性染色体非整倍性.md "wikilink")

1.
2.