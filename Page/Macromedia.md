**Macromedia**，汉译**宏媒體**公司，是在1992年[Authorware公司](../Page/Authorware.md "wikilink")（Authorware的最初開發商）與MacroMind-Paracomp公司（[Macromedia
Director的最初開發商](../Page/Macromedia_Director.md "wikilink")）的合併中產生的，現為一家專門開發圖像處理與Web製作軟件的[軟件公司](../Page/軟件公司.md "wikilink")，總部設在[美國](../Page/美國.md "wikilink")[加州的](../Page/加州.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")。2001年，Macromedia購進[Allaire公司](../Page/Allaire.md "wikilink")，並將其[Web開發軟件加入自己的產品系列](../Page/Web.md "wikilink")。2003年，Macromedia又收購了專門開發製作幫助軟件的[eHelp公司](../Page/eHelp公司.md "wikilink")，其代表產品包括RoboHelp、RoboDemo（現為[Adobe
Captivate](../Page/Adobe_Captivate.md "wikilink")）和RoboInfo。

2005年4月18日，Macromedia被[美國著名圖形軟件開發商](../Page/美國.md "wikilink")[Adobe系統公司以](../Page/Adobe.md "wikilink")34億美元價格收購。這一收購極大豐富了Adobe的產品線，提高了其在[多媒體和網絡出版業的能力](../Page/多媒體.md "wikilink")。

2005年12月3日，隨著Adobe以34億[美元宣佈完成併購](../Page/美元.md "wikilink")，Macromedia從此併入Adobe，Macromedia旗下產品的「Macromedia」品牌名稱亦被「Adobe」取代。

## 主要產品

  - [Macromedia Studio](../Page/Macromedia_Studio.md "wikilink")
      - [Macromedia
        Dreamweaver一個HTML編輯器](../Page/Macromedia_Dreamweaver.md "wikilink")，支援視覺化與程式碼的編輯
      - [Macromedia
        Fireworks圖形編輯器](../Page/Macromedia_Fireworks.md "wikilink")
      - [Macromedia
        Contribute簡易的視覺化HTML編輯器](../Page/Adobe_Contribute.md "wikilink")，提供終端使用者使用
      - [Macromedia
        FlashPaper轉換文件為PDF或FlashPaper的軟體](../Page/Macromedia_FlashPaper.md "wikilink")
      - [Macromedia
        Flash動畫編輯軟體](../Page/Macromedia_Flash.md "wikilink")，增加網頁內容的豐富度
  - [Macromedia Breeze網路會議軟體](../Page/Adobe_Connect.md "wikilink")
  - [Macromedia Flex網頁應用程式開發軟體](../Page/Adobe_Flex.md "wikilink")
  - [Macromedia
    ColdFusion網頁伺服器軟體](../Page/Macromedia_ColdFusion.md "wikilink")，服務".cfm"的檔案內容（類似於ASP、PHP）
  - [Macromedia
    Director多媒體創作軟體](../Page/Macromedia_Director.md "wikilink")
  - [Macromedia
    Authorware](../Page/Authorware#Macromedia_Authorware.md "wikilink")
  - [Macromedia
    FreeHand向量圖編輯器](../Page/Macromedia_FreeHand.md "wikilink")
  - [Macromedia SoundEdit](../Page/Macromedia_SoundEdit.md "wikilink")
  - [Macromedia RoboHelp](../Page/Macromedia_RoboHelp.md "wikilink")
  - [Macromedia
    Captivate原本稱為Robodemo](../Page/Macromedia_Captivate.md "wikilink")，用來錄製監視器上的畫面
  - [Macromedia Shockwave](../Page/Macromedia_Shockwave.md "wikilink")
  - [Macromedia JRun](../Page/Macromedia_JRun.md "wikilink") J2EE的伺服器軟體
  - [Macromedia Flash Media
    Server](../Page/Flash_Media_Server.md "wikilink")
  - [Macromedia
    HomeSite支援程式碼編輯的網頁編輯軟體](../Page/Macromedia_HomeSite.md "wikilink")
  - [Macromedia
    Fontographer](../Page/Macromedia_Fontographer.md "wikilink")
  - [Macromedia
    Central跨平台的Flash應用程式平台](../Page/Macromedia_Central.md "wikilink")
  - [Macromedia FlashCast](../Page/Macromedia_FlashCast.md "wikilink")
  - [Macromedia Web Publishing
    System](../Page/Macromedia_Web_Publishing_System.md "wikilink")
  - [Macromedia Sitespring](../Page/Macromedia_Sitespring.md "wikilink")
  - [Macromedia Action\!](../Page/Macromedia_Action!.md "wikilink")

[Category:Adobe Systems](../Category/Adobe_Systems.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")
[Category:2005年結業公司](../Category/2005年結業公司.md "wikilink")
[Category:美國已結業公司](../Category/美國已結業公司.md "wikilink")
[Category:1992年加利福尼亞州建立](../Category/1992年加利福尼亞州建立.md "wikilink")