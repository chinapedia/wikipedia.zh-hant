**安德烈·安德烈耶维奇·弗拉索夫**（[俄文](../Page/俄文.md "wikilink")：，[拉丁化](../Page/拉丁化.md "wikilink")：Andrej
Andrejewitsch
Wlassow，），[苏联将领](../Page/苏联.md "wikilink")，中将。他作为[苏军将领曾在](../Page/苏联红军.md "wikilink")[苏德战争初期表现优异](../Page/苏德战争.md "wikilink")，却在被俘后选择投降[德军并倒戈](../Page/德军.md "wikilink")。这些使他成为[俄国历史上一位颇有争议的人物](../Page/俄罗斯历史.md "wikilink")。

## 早年

弗拉索夫生于[下诺夫哥罗德一个信奉](../Page/下诺夫哥罗德.md "wikilink")[东正教的](../Page/正教會.md "wikilink")[中农家庭](../Page/中农.md "wikilink")。在他还在新城[神学院读书时](../Page/神学院.md "wikilink")[俄国革命爆发](../Page/1917年俄国革命.md "wikilink")。1919年弗拉索夫决定退学，在简单学习了一些[农学后他加入](../Page/农学.md "wikilink")[苏联红军](../Page/苏联红军.md "wikilink")。当时其所在部队主要在[乌克兰](../Page/乌克兰.md "wikilink")、[高加索和](../Page/高加索.md "wikilink")[克里米亚半岛活动](../Page/克里米亚.md "wikilink")，对手是[邓尼金](../Page/安东·伊万诺维奇·邓尼金.md "wikilink")。弗拉索夫开始当上个狙撃兵第二連隊長的职务，随后军阶开始一步步上升。

1924年7月至1925年，弗拉索夫在[列宁格勒高等骑兵学校训练班深造](../Page/列宁格勒.md "wikilink")，与[朱可夫](../Page/格奥尔基·康斯坦丁诺维奇·朱可夫.md "wikilink")、[罗科索夫斯基](../Page/康斯坦丁·康斯坦丁诺维奇·罗科索夫斯基.md "wikilink")、[巴格拉米扬和](../Page/伊萬·霍夫汉内斯·巴格拉米扬.md "wikilink")[叶廖缅科等人曾是同学](../Page/安德烈·伊万诺维奇·叶廖缅科.md "wikilink")。1930年正式加入[苏共并被选为委员](../Page/苏共.md "wikilink")。

1933年与女医生**安娜·米哈伊洛芙娜**结婚，两人有一子。1936年[大清洗时两人离婚](../Page/大清洗.md "wikilink")。值得一提的是，[大清洗中大批](../Page/大清洗.md "wikilink")[苏军指战员被清洗](../Page/苏联红军.md "wikilink")，弗拉索夫尽管出身中农却未受太大影响。1938年有关他的党内鉴定中“在消灭所部中破坏分子残余的问题上做了很多工作”的记载或许说明了原因。

## 战前

### 在中国

1938年弗拉索夫作为军事顾问被派到[中国](../Page/中国.md "wikilink")，帮助[蒋中正训练](../Page/蒋中正.md "wikilink")[国民革命军](../Page/国民革命军.md "wikilink")。期间蒋中正给予其很高的评价。1939年11月为准备[苏芬战争而回国](../Page/冬季战争.md "wikilink")。临行前蒋中正赠与很多礼物，回国后均被[内务人民委员部没收](../Page/内务人民委员部.md "wikilink")。

### 战前的表现

弗拉索夫回国后先任第99步兵师师长（[基辅军区](../Page/基辅.md "wikilink")）。他采取了有效措施整肃军风，使第99步兵师获得很高评价。1940年9月，[铁木辛哥到基辅军区检阅部队](../Page/谢苗·康斯坦丁诺维奇·铁木辛哥.md "wikilink")，第99师进行了校阅演习，获得红旗奖。同年升为[少将](../Page/少将.md "wikilink")。后又任第4机械化军军长。

## 从苏军名将到被俘

### 基辅战役

[德军依靠其](../Page/德意志國防軍.md "wikilink")[闪击战的威力](../Page/闪击战.md "wikilink")，于1941年8月兵临[基辅](../Page/基辅.md "wikilink")。由于[波塔波夫坦克兵少将的第](../Page/波塔波夫.md "wikilink")5集团军被迫撤至[第聂伯河一线](../Page/第聂伯河.md "wikilink")，[苏联急需新部队补缺](../Page/苏联.md "wikilink")。经推荐和反复研究，苏联西南方面军司令[基尔波诺斯上将和时任前线军事委员会委员](../Page/米哈伊爾·彼得羅維奇·基爾波諾斯.md "wikilink")[赫鲁晓夫决定以弗拉索夫担任一支临时拼凑的第](../Page/尼基塔·謝爾蓋耶維奇·赫魯曉夫.md "wikilink")37集团军司令，此时弗拉索夫刚刚从[普热梅希尔退下来](../Page/普热梅希尔.md "wikilink")。

弗拉索夫上任后收容前线溃败的军士，重整第37集团军，并迅速组织起防御。8月8日德军第6集团军第29军进攻基辅，在弗拉索夫组织的抵抗下，进攻没有收获。8月中旬第37集团军击退了德军在基辅西南的攻势，守住了基辅的正面防线。但是[库兹涅佐夫的第](../Page/尼古拉·格拉西莫維奇·庫茲涅佐夫.md "wikilink")21集团军和[波德拉斯的第](../Page/波德拉斯.md "wikilink")40集团军之间出现了缺口，使德军得以从翼侧包围基辅，此外[叶廖缅科的布良斯克方面军实施反突击失败](../Page/叶廖缅科.md "wikilink")，[古德里安和](../Page/古德里安.md "wikilink")[克莱斯特的装甲集群得以长驱直入](../Page/保罗·路德维希·埃瓦尔德·冯·克莱斯特.md "wikilink")，苏军防线终于崩溃，弗拉索夫徒步逃出包围。

为表彰弗拉索夫的功绩，[斯大林特令用飞机接他到](../Page/斯大林.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，授予其[列宁勋章并任命为防卫莫斯科的第](../Page/列宁勋章.md "wikilink")20集团军司令。

### 莫斯科保卫战

1941年11月[喬爾格-漢斯·萊因哈特的第](../Page/格奧爾格-漢斯·萊因哈特.md "wikilink")3装甲集群和[赫普纳的第](../Page/埃里希·赫普纳.md "wikilink")4装甲集群从北面突破了苏军的防线。弗拉索夫很快率第20集团军以强行军堵住了突破口。在[库兹涅佐夫的第](../Page/库兹涅佐夫.md "wikilink")1突击集团军支援下，弗拉索夫在[德米特罗夫和](../Page/德米特罗夫.md "wikilink")[伊克沙湖之间发动反攻](../Page/伊克沙湖.md "wikilink")，把德军阻挡在[莫斯科运河一线](../Page/莫斯科运河.md "wikilink")。12月6日开始的大反攻中，第20集团军与[别洛夫指挥的近卫骑兵第](../Page/别洛夫.md "wikilink")1军连克德军战略要地，将德军驱至[拉马河](../Page/拉馬河_\(俄羅斯\).md "wikilink")、[鲁扎河一线地区](../Page/魯扎河.md "wikilink")，并收复了[沃洛科拉姆斯克在内的许多居民地](../Page/沃洛科拉姆斯克.md "wikilink")，还缴获了大量的武器装备。

1942年1月13日第20集团军率先从沃洛科拉姆斯克和[沙霍夫斯卡亚方向实施突击](../Page/沙霍夫斯卡亚.md "wikilink")，于1月底抵达[格扎茨克东北](../Page/格扎茨克.md "wikilink")。第20集团军与各路苏军在[维亚济马合围了德国第](../Page/维亚济马.md "wikilink")9集团军和第4装甲集团军一部。但在胜利在望时，苏军最高统帅部却不顾[朱可夫的反对](../Page/朱可夫.md "wikilink")，过早将第1突击集团军和[罗科索夫斯基的第](../Page/罗科索夫斯基.md "wikilink")16集团军从莫斯科以西撤走去增援南北两翼，使第20集团军不得不加宽进攻的正面宽度，逐渐失去了锐势。试图封闭[尤赫诺夫到维亚济马间的缺口的苏军](../Page/尤赫诺夫.md "wikilink")[伞兵和](../Page/伞兵.md "wikilink")[游击队又遭德第](../Page/游击队.md "wikilink")4装甲集团军的猛烈反击。2月3日被围德军打通了与外界的联系，2月5日第20集团军反被[莫德尔的德第](../Page/瓦尔特·莫德尔.md "wikilink")9集团军包围并很快被歼，弗拉索夫又一次逃出。

尽管莫斯科保卫战最后苏军没能完全胜利，弗拉索夫仍以战绩优异获[红旗勋章](../Page/紅旗勳章.md "wikilink")、中将军衔。后弗拉索夫在防守[瓦尔代地区时](../Page/瓦尔代.md "wikilink")，由于[斯大林决策失误发起全线反击](../Page/约瑟夫·维萨里奥诺维奇·斯大林.md "wikilink")，又被德军包围；但他指挥部队成功突围，得到[斯大林赞赏](../Page/约瑟夫·维萨里奥诺维奇·斯大林.md "wikilink")，甚至一度被考虑出任[斯大林格勒战线总指挥](../Page/伏尔加格勒.md "wikilink")，后因[列宁格勒方向吃紧而作罢](../Page/圣彼得堡.md "wikilink")。

### 被俘

1942年3月弗拉索夫出任沃尔霍夫方面军司令[梅列茨科夫的副手](../Page/基里尔·阿法纳西耶维奇·梅列茨科夫.md "wikilink")，支援[列宁格勒](../Page/圣彼得堡.md "wikilink")。4月代重病的[克雷科夫出任第](../Page/克雷科夫.md "wikilink")2突击集团军司令，此时第2突击集团军受德军半包围，战斗力不足，不可能“突击”，5月下旬苏军最高统帅部大本营决定让第2突击集团军突围，但为德军获悉，第2突击集团军被完全包围。

斯大林钦点[华西列夫斯基到前线救援弗拉索夫](../Page/亚历山大·米哈伊洛维奇·华西列夫斯基.md "wikilink")，第2突击集团军一度获德军缺口而转移了大量伤员，但不久就再次被围并被歼，7月12日弗拉索夫被俘，据说当时已经瘦得走不动了。

苏联方面对于第2突击集团军的突围的记载大多称，弗拉索夫在组织突围时毫无作为，开始苏联甚至认为他失踪了。

## 倒戈和最后

### 倒戈

弗拉索夫被俘后先是被囚禁，很快他被德国情报人员“策反”。他发表了《我为什么走上反对[布尔什维克主义的道路](../Page/布尔什维克.md "wikilink")》的公开信，成立了一个“解放俄国人民委员会”，进行[反共宣传](../Page/反共.md "wikilink")，谋求建立一个“自由民主的俄罗斯”，此间他亦开始谋划日后的[俄罗斯解放军](../Page/俄罗斯解放军.md "wikilink")，数次到苏军[战俘中宣传并获得了部分支持](../Page/战俘.md "wikilink")。

尽管弗拉索夫有意合作并得到一部分德国人赞赏，[希特勒和相当一部分德国军政人物却并不买账](../Page/阿道夫·希特勒.md "wikilink")。1943年4月3日希特勒公开表示他永不考虑建立由俄国人组成的部队；[希姆莱也很厌恶他](../Page/希姆莱.md "wikilink")；[威廉·凯特尔甚至下令软禁他](../Page/威廉·凱特爾.md "wikilink")，原因或可归为他们仍在坚持[种族主义立场](../Page/种族主义.md "wikilink")，排斥[斯拉夫人](../Page/斯拉夫人.md "wikilink")。

但随着战局不利，支持弗拉索夫的上层人物亦开始增加，[希特勒也不得不重新考虑](../Page/希特勒.md "wikilink")。

### 俄罗斯解放军

1944年下半年弗拉索夫终于获准成立了**[俄罗斯解放军](../Page/俄罗斯解放军.md "wikilink")**，包括陆空两军，人数有50,000，士兵和指挥官都来自战俘和欧洲的俄国侨民，建军后各原有的[反共](../Page/反共.md "wikilink")[白军都来投奔](../Page/白军.md "wikilink")，人数达到近100,000。

1945年2月9日俄罗斯解放军第一次参与对苏实战，但不久第1师师长[谢尔盖·布尼亚琴科便意识到局势不可逆转](../Page/谢尔盖·布尼亚琴科.md "wikilink")，不愿再为德国效力，从[奥得河前线撤下转往](../Page/奥得河.md "wikilink")[捷克与主力会合](../Page/捷克.md "wikilink")。德军指挥官[斐迪南·舍納爾甚至亲自前往挽留](../Page/斐迪南·舍納爾.md "wikilink")，但在一番舌战之后他意识到无法留住俄罗斯解放军，悻悻而归。

弗拉索夫和下属在意识到苏军已势如破竹之后，为求战争结束后自保，与中立国[瑞典和](../Page/瑞典.md "wikilink")[瑞士曾有接触](../Page/瑞士.md "wikilink")，但没有成果。

### 布拉格之战

正当弗拉索夫和俄罗斯解放军一筹莫展之时，德國的傀儡政權[波希米亞和摩拉维亞保護國首府](../Page/波希米亞和摩拉维亞保護國.md "wikilink")[布拉格爆发](../Page/布拉格.md "wikilink")[反德起義](../Page/布拉格攻勢.md "wikilink")（Prague
uprising）。起义者力量不足，而此时无论[盟军还是苏军距离布拉格都相差甚远](../Page/盟军.md "wikilink")。于是起义者派代表向在捷克境内的俄罗斯解放军求援。弗拉索夫和[谢尔盖·布尼亚琴科等认为如果帮助起义者](../Page/谢尔盖·布尼亚琴科.md "wikilink")，在将来可以获得政治避难权并获[盟国的同情](../Page/盟国.md "wikilink")，同时鉴于同为[斯拉夫人](../Page/斯拉夫人.md "wikilink")，于是决定帮助起义。

5月5日俄罗斯解放军正式出兵，很快便基本控制布拉格全城，但[斯大林获悉后立即派](../Page/约瑟夫·维萨里奥诺维奇·斯大林.md "wikilink")[莫洛托夫向](../Page/维亚切斯拉夫·米哈伊洛维奇·莫洛托夫.md "wikilink")[捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")[流亡政府首脑](../Page/捷克斯洛伐克流亡政府.md "wikilink")[贝奈斯施加压力以迫使其放弃与俄罗斯解放军合作](../Page/爱德华·贝奈斯.md "wikilink")；同时[科涅夫受令率乌克兰第](../Page/伊萬·斯捷潘諾維奇·科涅夫.md "wikilink")1方面军加速前进。[艾森豪威尔受到苏联的压力](../Page/德怀特·艾森豪威尔.md "wikilink")，也命令即将进入布拉格市区的[巴顿放弃布拉格](../Page/乔治·巴顿.md "wikilink")，5月8日弗拉索夫等被迫撤出布拉格。

### 末日

不久弗拉索夫及其部下在东西夹击下被俘，但究竟是哪一方俘获他们（或受降）却仍有争议，不过他终究落回到曾经的“自己人”手里。1946年8月1日他们被[苏联判处](../Page/苏联.md "wikilink")[绞刑](../Page/绞刑.md "wikilink")。据称弗拉索夫是被最残忍的手段折磨至死的：苏联甚至向全体人民详细描述了他死去的过程及他临死前挣扎了多长时间。他部队裡的军官和士兵也都被用同样的方法处死\[1\]。

[苏联解体後原俄罗斯解放军官兵其名誉陸續恢復](../Page/苏联解体.md "wikilink")，但一直未涉及到弗拉索夫\[2\]。

## 个人生活

弗拉索夫的第二次婚姻在1941年。妻子芭芙洛芙娜在其被处决后一直奔走于其名誉恢复。弗拉索夫的第一任妻子米哈伊洛芙娜则受尽牵连，被关到[集中营](../Page/集中营.md "wikilink")。

弗拉索夫另有一私生子。一孙现在[俄罗斯海军](../Page/俄罗斯海军.md "wikilink")。

## 参考文献

  -
  -
## 注释

## 相關書籍與影片

Books:

  - [Wilfried
    Strik-Strikfeldt](../Page/Wilfried_Strik-Strikfeldt.md "wikilink"):
    *Against Stalin and Hitler. Memoir of the Russian Liberation
    Movement 1941-5*. Macmillan, 1970, ISBN 0-333-11528-7

  - Russian version of the above: Вильфрид Штрик-Штрикфельдт: *Против
    Сталина и Гитлера*. Изд. Посев, 1975, 2003. ISBN 5-85824-005-4

  - Бахвалов Анатолий: *Генерал Власов. Предатель или герой?* Изд. СПб
    ВШ МВД России, 1994.

  - Sven Steenberg: *Wlassow. Verräter oder Patriot?* Verlag
    Wissenschaft und Politik, Köln 1968.

  - Russian version of the above: Свен Стеенберг: *Генерал Власов*.
    Изд-во Эксмо, 2005. ISBN 5-699-12827-1

  - Sergej Frölich: *General Wlassow. Russen und Deutsche zwischen
    Hitler und Stalin*.

  - Russian version of the above: Сергей Фрёлих *Генерал Власов. Русские
    и Немцы между Гитлером и Сталиным* (перевод с немецкого Ю.К. Мейера
    при участии Д.А. Левицкого), 1990. Printed by Hermitage.

  - Александров Кирилл М.: *Армия генерала Власова 1944-45*. Изд-во
    Эксмо, 2006. ISBN 5-699-15429-9.

  - Чуев Сергей: *Власовцы - Пасынки Третьего Рейха*. Изд-во Эксмо,
    2006. ISBN 5-699-14989-9.

  - И. Хоффманн: История власовской армии. Перевод с немецкого Е.
    Гессен. 1990 YMCA Press ISBN 2-85065-175-3 ISSN 1140-0854

  - : Die Tragödie der 'Russischen Befreiungsarmee' 1944/45. Wlassow
    gegen Stalin. Herbig Verlag, 2003 ISBN 3-7766-2330-6.

  - Russian version of the above: Гофман Иоахим: Власов против Сталина.
    Трагедия Русской Освободительной Армии. Пер. с нем. В. Ф.
    Дизендорфа. Изд-во АСТ, 2006. ISBN 5-17-027146-8.

  - О. В. Вишлёв(preface): *Генерал Власов в планах гитлеровских
    спецслужб*. Новая и Новейшая История, 4/96, pp. 130–146.
    \[Historical sources with a preface\]

  - В. В. Малиновский: ''Кто он, русский коллаборационнист: Патриот или
    предатель?' Вопросы Истории 11-12/96, pp. 164–166. \[letter to the
    editor\]

  - Martin Berger: *Impossible alternatives*. The Ukrainian Quarterly,
    Summer-Fall 1995, pp. 258–262. \[review of Catherine Andrevyev:
    Vlasov and the Russian liberation movement\]

  - А. Ф. Катусев, В. Г. Оппоков: *Иуды. Власовцы на службе у фашизма*.
    Военно-Исторический Журнал 6/1990, pp. 68–81.

  - П. А. Пальчиков: *История Генерала Власова*. Новая и Новейшая
    История, 2/1993, pp. 123–144.

  - А. В. Тишков: *Предатель перед Советским Судом*. Советское
    Государство и Право, 2/1973, pp. 89–98.

  - Л. Е. Решин, В. С. Степанов: *Судьбы генералские*.
    Военно-Исторический Журнал, 3/1993, pp. 4–15.

  - С. В. Ермаченков, А. Н. Почтарев: *Последний поход власовской
    армии*. Вопросы Истории, 8/98, pp. 94–104.

Documentaries:

  - [General for Two
    Devils 1995](https://web.archive.org/web/20060510082929/http://www.ihffilm.com/aavlasgenfor.html)
  - Europe Central by William T Vollmann

## 外部链接

  - [从英雄到败类——苏联叛将弗拉索夫](https://web.archive.org/web/20080304130912/http://military.china.com/zh_cn/history4/62/20050315/12169972.html)
  - [It's Too Early To Forgive
    Vlasov](http://www.sptimes.ru/index.php?action_id=2&story_id=5830),
    *[The St. Petersburg
    Times](../Page/The_St._Petersburg_Times.md "wikilink")*, November 6,
    2001
  - [Władysław Anders and Antonio Muňoz: Russian Volunteers in the
    German Wehrmacht in WWII (describes role of
    Vlasov)](http://www.mochola.org/russiaabroad/vlasan01.htm)
  - [Congress of Russian Americans article on
    Vlasov](https://web.archive.org/web/20070225164821/http://www.russian-americans.org/CRA_Art_Vlas.htm)

## 參見

  - [俄国内战](../Page/俄国内战.md "wikilink")
  - [苏德战争](../Page/苏德战争.md "wikilink")
  - [基輔戰役 (1941年)](../Page/基輔戰役_\(1941年\).md "wikilink")
  - [當弗坦作戰](../Page/當弗坦作戰.md "wikilink")
  - [莫斯科戰役](../Page/莫斯科戰役.md "wikilink")
  - [熱澤夫戰役](../Page/熱澤夫戰役.md "wikilink")
  - [阿道夫·希特勒](../Page/阿道夫·希特勒.md "wikilink")
  - [约瑟夫·维萨里奥诺维奇·斯大林](../Page/约瑟夫·维萨里奥诺维奇·斯大林.md "wikilink")

[Category:被處決的蘇聯人](../Category/被處決的蘇聯人.md "wikilink")
[Category:被蘇聯處決者](../Category/被蘇聯處決者.md "wikilink")
[Category:蘇聯第二次世界大戰人物](../Category/蘇聯第二次世界大戰人物.md "wikilink") [Wlassow,
Anderi](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:苏联军事人物](../Category/苏联军事人物.md "wikilink")
[Wlassow, Anderi](../Category/纳粹德国人物.md "wikilink") [Vlasov,
Andrey](../Category/蘇聯持不同政見者.md "wikilink")

1.  [匈牙利人尼古拉斯](../Page/匈牙利.md "wikilink")·尼亚拉季当时於莫斯科公务，後做出上述评论。
2.  Valeria Korchagina and Andrei Zolotov Jr.[It's Too Early To Forgive
    Vlasov](http://www.sptimes.ru/index.php?action_id=2&story_id=5830)
    *The St. Petersburg Times*. 6 Nov 2001.