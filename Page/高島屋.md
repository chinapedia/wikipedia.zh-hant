**高島屋**\[1\]（）是一家[日本大型](../Page/日本.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[百貨公司](../Page/百貨公司.md "wikilink")，最初是在1829年創立於[京都的織品](../Page/京都.md "wikilink")[零售商](../Page/零售商.md "wikilink")，之後在1922年踏入百貨公司領域，現今已擴店全日本，並與結盟的的百貨同業組成「」。海外則在[新加坡](../Page/新加坡.md "wikilink")、[上海](../Page/上海市.md "wikilink")、[胡志明市設有分店](../Page/胡志明市.md "wikilink")。總店（大阪店）位於[大阪](../Page/大阪市.md "wikilink")[難波的](../Page/難波.md "wikilink")[南海電鐵](../Page/南海電氣鐵道.md "wikilink")[難波站](../Page/難波站_\(南海\).md "wikilink")內。

## 歷史

  - 1831年（[天保](../Page/天保_\(日本\).md "wikilink")2年） -
    飯田新七在[京都創業](../Page/京都.md "wikilink")，以義父飯田儀兵衛的出身地[近江國](../Page/近江國.md "wikilink")（今[高島市](../Page/高島市.md "wikilink")）命名為「高島屋」。當時主要售賣二手服裝及棉料織品。當時店鋪位置為現今之[京都銀行總行](../Page/京都銀行.md "wikilink")。
  - 1855年（[安政](../Page/安政.md "wikilink")2年） -
    停止二手服裝業務，轉為販售棉料及[吳服](../Page/吳服.md "wikilink")。
  - 1898年（[明治](../Page/明治.md "wikilink")31年） -
    大阪店開店，設於[心齋橋筋](../Page/心齋橋筋.md "wikilink")。
  - 1900年（明治33年） -
    東京店開店，設於西紺屋町（今[中央區](../Page/中央區_\(東京都\).md "wikilink")[銀座西一丁目](../Page/銀座.md "wikilink")）。
  - 1904年（明治37年） - 高島屋的「髙」字[商標註冊](../Page/商標.md "wikilink")。
  - 1909年（明治42年） - 「高島屋飯田合名會社」成立，資本額為100萬日圓。
  - 1916年（[大正](../Page/大正.md "wikilink")5年） -
    東京店新址落成，位於京橋區南傳馬町（今中央區[京橋](../Page/京橋_\(東京都\).md "wikilink")）。貿易部門獨立為「高島屋飯田株式會社」，為今日[丸紅前身之一](../Page/丸紅.md "wikilink")。
  - 1919年（大正8年） - 株式會社高島屋吳服店成立。
  - 1922年（大正11年） - 大阪店新址落成，位於。
  - 1930年（[昭和](../Page/昭和.md "wikilink")5年）12月 - 株式會社高島屋吳服店更名為「株式會社高島屋」。
  - 1932年（昭和7年） -
    南海店開店，位於[南海電鐵](../Page/南海電氣鐵道.md "wikilink")[難波站內](../Page/難波站_\(南海\).md "wikilink")（）。
  - 1939年（昭和14年） - 位於長堀橋筋的大阪店閉店。同時南海店改名大阪店，並沿用至今。
  - 1949年（昭和24年）5月 - 股票於大阪證券交易所及東京證券交易所公開發行。
  - 2008年（[平成](../Page/平成.md "wikilink")20年） -
    大阪國稅局稅務調查，發現疑似漏稅申報約2億9500萬元。
  - 2008年（平成20年） -
    與旗下擁有[阪急](../Page/阪急百貨店.md "wikilink")、兩家連鎖百貨的宣布在[合併的目標下進行業務合作](../Page/合併.md "wikilink")。但合併案在2010年破局。

## 店鋪

### 日本國內

  - 直營店 / 近畿

<!-- end list -->

  - 大阪店（[大阪市](../Page/大阪市.md "wikilink")[中央區](../Page/中央區_\(大阪市\).md "wikilink")[難波](../Page/難波.md "wikilink")。設於[南海電鐵](../Page/南海電氣鐵道.md "wikilink")[難波站內](../Page/難波站_\(南海\).md "wikilink")，也是高島屋的公司登記地址所在。[物業持有權由南海電鐵所有](../Page/物業.md "wikilink")。）
  - 堺店（[堺市](../Page/堺市.md "wikilink")）
  - 泉北店（堺市）
  - 京都店（[京都市](../Page/京都市.md "wikilink")[下京区](../Page/下京区.md "wikilink")；京都是高島屋的始創地，但該店並非創始店鋪原址）
  - 洛西店（京都市[西京區](../Page/西京區.md "wikilink")）

<!-- end list -->

  - 直營店 / 關東

<!-- end list -->

  - 日本橋店（[東京都](../Page/東京都.md "wikilink")[中央區](../Page/中央區_\(東京都\).md "wikilink")[日本橋](../Page/日本橋_\(東京都中央區\).md "wikilink")。原稱東京店。）
  - 新宿店（東京都[涩谷区](../Page/涩谷区.md "wikilink")，位於[高島屋時代廣場內](../Page/高島屋時代廣場.md "wikilink")）
  - 玉川店（東京都[世田谷区](../Page/世田谷区.md "wikilink")）
  - 立川店（東京都[立川市](../Page/立川市.md "wikilink")）
  - 横滨店（[横滨市](../Page/横滨市.md "wikilink")[西区](../Page/西区_\(橫濱市\).md "wikilink")。物業持有權為[相模鐵道所有](../Page/相模鐵道.md "wikilink")。）
  - 港南台店（横滨市[港南区](../Page/港南区.md "wikilink")）
  - Takashimaya FOODMAISON
    新橫濱店（橫濱市[港北區](../Page/港北區_\(日本\).md "wikilink")）
  - Takashimaya FOODMAISON
    海老名店（[神奈川縣](../Page/神奈川縣.md "wikilink")[海老名市](../Page/海老名市.md "wikilink")）
  - 大宮店（[埼玉市](../Page/埼玉市.md "wikilink")[大宮区](../Page/大宮区.md "wikilink")）
  - 柏店（[千葉縣](../Page/千葉縣.md "wikilink")[柏市](../Page/柏市.md "wikilink")）
  - Takashimaya FOODMAISON
    海老名店（[神奈川縣](../Page/神奈川縣.md "wikilink")[海老名市](../Page/海老名市.md "wikilink")）
  - Takashimaya FOODMAISON 蒼鷹之森店（千葉縣[流山市](../Page/流山市.md "wikilink")）

<!-- end list -->

  - 直接子公司所屬店

<!-- end list -->

  - 株式會社高崎髙島屋 /
    高崎店（[群馬縣](../Page/群馬縣.md "wikilink")[高崎市](../Page/高崎市.md "wikilink")）
  - 株式會社岐阜髙島屋 /
    岐阜店（[岐阜縣](../Page/岐阜縣.md "wikilink")[岐阜市](../Page/岐阜市.md "wikilink")）
  - 株式會社岡山髙島屋 /
    岡山店（[岡山市](../Page/岡山市.md "wikilink")[北區](../Page/北區_\(岡山市\).md "wikilink")）
  - Takashimaya FOODMAISON 岡山店（岡山市北區。由岡山髙島屋經營。）

<!-- end list -->

  - 合資公司所屬店

<!-- end list -->

  - 株式會社JR東海髙島屋 /
    （；[名古屋市](../Page/名古屋市.md "wikilink")[中村區](../Page/中村區.md "wikilink")。位於[JR中央大廈內](../Page/JR中央大廈.md "wikilink")。與[JR東海等企業合資](../Page/東海旅客鐵道.md "wikilink")。）
  - 株式會社伊予鐵髙島屋 / 伊予鐵高島屋（；愛媛縣松山市。與[伊予鐵道合資](../Page/伊予鐵道.md "wikilink")。）

### 日本國外

  - [新加坡](../Page/新加坡.md "wikilink") - 新加坡高島屋（Singapore
    Takashimaya；1993年10月8日開店）
  - [台北](../Page/臺北市.md "wikilink") -
    [大葉高島屋](../Page/大葉高島屋.md "wikilink")（1994年7月9日開店；原由[大葉集團與高島屋合資各半](../Page/大葉集團.md "wikilink")，2016年5月17日起日方撤資，改以由大葉集團獨資經營\[2\]）
  - [上海](../Page/上海市.md "wikilink") - 上海高島屋（2012年12月19日開店）
  - [胡志明市](../Page/胡志明市.md "wikilink") - 越南高島屋（；2016年7月30日開店）

## 參考資料

## 外部連結

  - [高島屋](http://www.takashimaya.co.jp/)

  - [新加坡高島屋](https://www.takashimaya.com.sg/)

  - [台北‧大葉高島屋](http://www.dayeh-takashimaya.com.tw/)

  - [上海高島屋](http://www.takashimaya.com.cn/)

  - [越南高島屋](http://www.takashimaya-vn.com/)

  -
[fr:Grands magasins du
Japon\#Takashimaya](../Page/fr:Grands_magasins_du_Japon#Takashimaya.md "wikilink")

[Category:日本百貨公司](../Category/日本百貨公司.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")

1.  實際[商號名稱使用](../Page/商業名稱.md "wikilink")「高」的[異體字](../Page/異體字.md "wikilink")「髙」。
2.