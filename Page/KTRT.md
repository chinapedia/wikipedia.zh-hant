****（簡稱****）是[台北101興建工程的總](../Page/台北101.md "wikilink")[承包商](../Page/承包商.md "wikilink")\[1\]\[2\]，為日商[熊谷組](../Page/熊谷組.md "wikilink")（**K**UMAGAI
GUMI）、華熊營造（**T**AIWAN
KUMAGAI；熊谷組在台子公司）、[榮民工程](../Page/榮民工程公司.md "wikilink")（**R**SEA）、大友為營造（**T**A-YO-WEI；[和信集團成員](../Page/和信集團.md "wikilink")\[3\]）因爭取台北101興建[招標所組成的](../Page/招標.md "wikilink")團隊\[4\]。

## 參考資料

## 外部連結

  - [華熊營造](http://www.taiwankumagai.com.tw/)
  - [榮民工程公司](https://web.archive.org/web/20060828085319/http://www.rsea.gov.tw/)
  - [四方開發建設（大友為營造母公司）](http://www.koos.com.tw/)

[K](../Category/總部位於台北市的工商業機構.md "wikilink")
[Category:台北101](../Category/台北101.md "wikilink")
[Category:策略聯盟](../Category/策略聯盟.md "wikilink")

1.  [台北國際金融中心大樓降低建築高度及對BOT影響之初步探討(一)](http://www.arch.net.tw/modern/month/246/246-2.htm)

2.  [西門子協助台北 101
    成為全世界最高的「綠建築」](http://www.siemens.com.tw/buildingtechnologies/40_Linking_Documents/Home/News/Case_Study_TAIPEI_101_CH.pdf)
    - 工業案例 - 台灣西門子研究樓宇科技事業部
3.  [和信集團有一個最忌諱的話題 -
    《今周刊》257期2001-11-22](http://www.businesstoday.com.tw/article-content-92743-144440)
4.  [「台北國際金融大樓」財務與風險分析](http://www.cem.ncu.edu.tw/johnli/95/BOT/95期中報告/李明璋蔡昇穎台北國際金融大樓財務與風險分析.doc)，李明璋、蔡昇穎，國立中央大學營建管理研究所，民國96年6月28日