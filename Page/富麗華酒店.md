**富麗華酒店**（），是一間[香港已拆卸的](../Page/香港.md "wikilink")[酒店建築](../Page/酒店.md "wikilink")，現址為[友邦金融中心](../Page/友邦金融中心.md "wikilink")。

富麗華酒店位處於香港[中環](../Page/中環.md "wikilink")[干諾道中](../Page/干諾道.md "wikilink")1號，於1973年正式開幕，曾經是一間顯赫一時的酒店。值得一提的是，酒店的頂層設有旋轉餐廳。

[麗新集團於](../Page/麗新集團.md "wikilink")1998年從[傅老榕家族購入富麗華酒店的控股權](../Page/傅老榕.md "wikilink")。2000年，麗新再將65%股權售予[新加坡上市公司](../Page/新加坡.md "wikilink")[百騰置地](../Page/百騰置地.md "wikilink")（後來合併為現時的[凱德集團](../Page/凱德集團.md "wikilink")），並且宣佈決定於2001年11月30日終止富麗華酒店的運作，酒店建築亦將拆卸，重建為一座甲級商業大廈。

富麗華酒店於2001年11月30日正式結業，隨後酒店建築被拆卸，重建成一座甲級商業大廈，即今日的[友邦金融中心](../Page/友邦金融中心.md "wikilink")。

## 軼聞

  - 1986年的第五屆[香港電影金像獎頒獎典禮曾於酒店的宴會廳舉行](../Page/香港電影金像獎.md "wikilink")。
  - 已故的著名作家[巴金曾到過酒店頂樓](../Page/巴金.md "wikilink")[旋轉餐廳吃自助晚餐](../Page/旋轉餐廳.md "wikilink")。

## 參看

  - [希爾頓酒店](../Page/香港希爾頓酒店.md "wikilink")
  - [美國國際集團大廈](../Page/美國國際集團大廈.md "wikilink")
  - [亦舒](../Page/亦舒.md "wikilink")

[Category:香港已拆卸酒店](../Category/香港已拆卸酒店.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:中國摩天大樓酒店](../Category/中國摩天大樓酒店.md "wikilink")
[Category:2001年結業公司](../Category/2001年結業公司.md "wikilink")
[Category:1973年成立的公司](../Category/1973年成立的公司.md "wikilink")
[Category:香港已不存在的建築物](../Category/香港已不存在的建築物.md "wikilink")