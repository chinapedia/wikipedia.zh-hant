\-{zh-hant:**VoiceOver**;zh-hans:**旁白**（）}-最早是一套內建於[蘋果電腦](../Page/蘋果電腦.md "wikilink")[Mac
OS X](../Page/Mac_OS_X.md "wikilink")
v10.4（代號"Tiger"）作業系統上的一套英文語音軟體，允許傷殘者用口音來控制多項系統功能。目前最新版本是OS
X 10.8（代號"Mountain Lion"）中內建，版本號碼6.0的Voice-{}-Over speech synthesiser。

現時，VoiceOver已發展成支援19種語言的[螢幕閱讀軟件](../Page/螢幕閱讀器.md "wikilink")，內至於蘋果公司的大部分電子產品，包括[Mac](../Page/Mac.md "wikilink")、[iPhone](../Page/iPhone.md "wikilink")、[iPad及](../Page/iPad.md "wikilink")[iPod](../Page/iPod.md "wikilink")，歸類為視障的輔助功能。視障使用者啟動此一功能後，配合特定操作指令即可透過語音得知螢幕資訊繼而使用裝置。

[iPod shuffle以及](../Page/iPod_shuffle.md "wikilink")[iPod
nano都可以使用VoiceOver功能](../Page/iPod_nano.md "wikilink")，并且使用者可以用[iTunes](../Page/iTunes.md "wikilink")+VoiceOver的组合将VoiceOver导入到[iPod中](../Page/iPod.md "wikilink")，可使用于[Mac及](../Page/Mac.md "wikilink")[Windows系统中](../Page/Windows.md "wikilink")。

## 外部連結

  - [Apple - 辅助功能 -
    VoiceOver](http://www.apple.com/cn/accessibility/voiceover/)

[Category:MacOS](../Category/MacOS.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:輔助技術](../Category/輔助技術.md "wikilink")