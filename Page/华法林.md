**华法林**（，又名**华法令**、**可邁丁**或**滅鼠靈**）是一種只可口服的[抗凝血素](../Page/抗凝劑.md "wikilink")，學名為**苄丙酮香豆素**，一般都以**苄丙酮香豆素鈉**來儲存及處方，較著名的品牌有**Coumadin**®。由於其化學結構與[維生素K非常相似](../Page/維生素K.md "wikilink")，所以能夠干擾[血液素的運作](../Page/血液素.md "wikilink")，降低患者血液凝結的風險。不過也由於它的強效，一般只會處方與血凝结风险比较高的患者使用，比如心律不整或者有人工心脏瓣膜的患者。

## 药理作用机制

一般來說，維生素K會在[肝臟裡轉化成為氧代維生素K](../Page/肝臟.md "wikilink")（vitamin K
epoxide），之後再由「環氧化物還原酵素」[還原](../Page/還原劑.md "wikilink")。华法林能夠抑制這種在肝臟裡的酵素作用，使[凝血酶原](../Page/凝血酶原.md "wikilink")（prothrombin）失去功能，從而防止血液凝固。

## 功用

### 醫療用途

华法林适用于[预防或](../Page/预防.md "wikilink")[治疗](../Page/治疗.md "wikilink")[静脉](../Page/静脉.md "wikilink")[血栓及其延伸和](../Page/血栓.md "wikilink")[肺栓塞](../Page/肺栓塞.md "wikilink")，预防或治疗[心房颤动或與](../Page/心房颤动.md "wikilink")[心脏瓣膜置换关联的](../Page/心脏瓣膜.md "wikilink")[血栓](../Page/血栓.md "wikilink")[并发症](../Page/并发症.md "wikilink")，降低[死亡](../Page/死亡.md "wikilink")、[再发性心肌梗死和血栓事件](../Page/再发性心肌梗死.md "wikilink")（例如[中风或](../Page/中风.md "wikilink")[心肌梗死后的系统性栓塞](../Page/心肌梗死.md "wikilink")）的风险\[1\]。

### 防治蟲鼠

其毒性必須在第二次食用才易顯現出來，家鼠或幼鼠可能對它產生抗藥性，因此[苯甲香豆醇已被其他藥效更強的](../Page/苯甲香豆醇.md "wikilink")[殺蟲劑取代](../Page/殺蟲劑.md "wikilink")。

## 副作用

可口服的華法林俗稱「**薄血丸**」，但服用後必須注意飲食，有兩種[維生素會影響藥物效能](../Page/維生素.md "wikilink")，如下：

  - [維生素E](../Page/維生素E.md "wikilink")：會加強華法林之效用，易引起[內出血](../Page/內出血.md "wikilink")。
  - [維生素K](../Page/維生素K.md "wikilink")：會減弱華法林之功效。

## 立體化學

華法林含有一個立體中心，由兩種對映體組成。
這是一個[外消旋體](../Page/外消旋體.md "wikilink")，即（“R”）和（“S”）形式的1：1混合物：\[2\]

<table>
<thead>
<tr class="header">
<th><p>華法林對映體</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:(R)-Warfarin_Structural_Formula_V1.svg" title="fig:200 px">200 px</a><br />
<small> CAS-Nummer: 5543-58-8</small></p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  - [Warfarin pesticide
    profile](https://web.archive.org/web/20051028102301/http://ace.orst.edu/cgi-bin/mfs/01/pips/warfarin.htm)
    from US Department of Agriculture
  - [Warfarin
    factsheet](https://web.archive.org/web/20060418204019/http://www.chemsoc.org/exemplarchem/entries/2002/hook/warfarin.htm)
    from the Royal Society of Chemistry

[Category:维生素K拮抗药](../Category/维生素K拮抗药.md "wikilink")
[Category:杀鼠剂](../Category/杀鼠剂.md "wikilink")
[Category:致畸原](../Category/致畸原.md "wikilink")
[Category:香豆素类药物](../Category/香豆素类药物.md "wikilink")
[Category:酮](../Category/酮.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.  [WARFARIN
    Label](http://www.fda.gov/cder/foi/label/2007/009218s105lblv2.pdf)，FDA/Center
    for Drug Evaluation and Research
2.  Rote Liste Service GmbH (Hrsg.): *Rote Liste 2017 –
    Arzneimittelverzeichnis für Deutschland (einschließlich
    EU-Zulassungen und bestimmter Medizinprodukte)*. Rote Liste Service
    GmbH, Frankfurt/Main, 2017, Aufl. 57, ISBN 978-3-946057-10-9, S.
    226.