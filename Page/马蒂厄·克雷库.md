**马蒂厄·克雷库**（，）為[非洲國家](../Page/非洲.md "wikilink")[貝南軍事強人](../Page/貝南.md "wikilink")。他於[塞內加爾接受軍事訓練後](../Page/塞內加爾.md "wikilink")，於1972年10月26日，以少校身分發動武裝政變，並獲得貝南政府控制權，且隨即自立為總統。1975年，他不但將“[達荷美共和国](../Page/達荷美共和国.md "wikilink")”更名為“[贝宁人民共和国](../Page/贝宁人民共和国.md "wikilink")”，还成立了[贝宁人民革命党](../Page/贝宁人民革命党.md "wikilink")，使該國成為[社会主义国家](../Page/社会主义国家.md "wikilink")。另一方面，他成為了該國享有绝对权威的领袖。

1989年后，他宣佈放棄[社会主义制度](../Page/社会主义制度.md "wikilink")，並恢復貝南原先的国旗、国徽。1991年，貝南舉行多黨之總統選舉。該選舉中，他以32%選票落選，結束了1972年－1991年的19年統治。不過，他仍繼續保有軍權。1996年，貝南再度舉行總統選舉，他則再度獲得當選，並连任至2006年。

马蒂厄·克雷库于2015年10月14日逝世，享年82岁。贝宁政府宣布全国哀悼一周\[1\]。

## 参考文献

[K](../Category/贝宁总统.md "wikilink")
[Category:靠政變上台的領導人](../Category/靠政變上台的領導人.md "wikilink")
[Category:軍人出身的總統](../Category/軍人出身的總統.md "wikilink")

1.  Tirthankar Chanda, ["-{Les «Caméléons» meurent aussi: Mathieu
    Kérékou s’en est
    allé}-"](http://www.rfi.fr/afrique/20151014-cameleons-necrologie-mathieu-kerekou-benin-president-dictateur-constitution-democra),
    Radio France Internationale, 14 October 2015 .