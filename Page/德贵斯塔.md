[FLMap-Tequesta-tribe2.PNG](https://zh.wikipedia.org/wiki/File:FLMap-Tequesta-tribe2.PNG "fig:FLMap-Tequesta-tribe2.PNG")

**德貴斯塔**族（）是居住於[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[大西洋海岸](../Page/大西洋.md "wikilink")，即現時[布勞沃德縣及](../Page/布勞沃德縣.md "wikilink")[邁阿密-戴德縣的](../Page/邁阿密-戴德縣_\(佛羅里達州\).md "wikilink")[土著](../Page/美國土著.md "wikilink")。他們也是歐洲人首次所接觸的美國土著。當時16世紀，德貴斯塔人佔據了[佛羅里達礁島群及佛羅里達州南端的](../Page/佛羅里達礁島群.md "wikilink")[沙布爾角](../Page/沙布爾角.md "wikilink")(Cape
Sable)。中央城鎮德貴斯塔可能坐落於邁阿密河河口。到了最近一百年德貴斯塔人才在[比斯坎灣地區居住](../Page/比斯坎灣.md "wikilink")。他們以打漁、狩獵和採集果實為生，但不從事任何形式的農業生產。

當時德貴斯塔人或多或少受到位於佛羅里達州西南海岸[卡盧薩人](../Page/卡盧薩.md "wikilink")(Calusa)所控制。當歐洲人首次登陸當地時，德貴斯塔人大約有800到10000人，而卡盧薩人大約有2000到20000人。[佛羅里達礁島群常常由兩族人輪流佔領](../Page/佛羅里達礁島群.md "wikilink")。

根據荷蘭繪圖家[Hessel
Gerritsz在](../Page/Hessel_Gerritsz.md "wikilink")1630年所出版的《世界歷史》中所繪製的地圖中，佛羅里達半島被稱呼為**德貴斯塔**〈Tegesta〉。在往後的兩百年，仍然被稱呼為**德貴斯塔**。\[1\]\[2\].

1513年[胡安·龐塞·德萊昂登陸當地並稱呼當地為德貴斯塔](../Page/胡安·龐塞·德萊昂.md "wikilink")（Chequesta），即現時的[比斯坎灣](../Page/比斯坎灣.md "wikilink")。1565年，西班牙水手[佩德羅·梅嫩德斯·德阿維萊斯](../Page/佩德羅·梅嫩德斯·德阿維萊斯.md "wikilink")（Pedro
Menéndez de
Avilés）藉口風暴停留比斯坎灣。耶穌會人把德貴斯塔族人首領的姪兒帶到古巴[夏灣拿受教育](../Page/夏灣拿.md "wikilink")，而首領的弟弟則跟隨梅嫩德斯到西班牙轉而信奉[基督教](../Page/基督教.md "wikilink")。1567年，梅嫩德斯帶著Francisco
Villareal及三十名士兵回到比斯坎灣並且建立了一個傳道區(mission)，強迫德貴斯塔人改信[基督教](../Page/基督教.md "wikilink")。這個傳道區到了1570年被迫廢棄。

1763年，當地的西班牙軍隊向英國軍隊投降後，所有人都被迫遷到[古巴](../Page/古巴.md "wikilink")[夏灣拿居住](../Page/夏灣拿.md "wikilink")。

## 外部連結

<references/>

## 參考文獻

  - Austin, Daniel W. (1997). "The Glades Indians and the Plants they
    Used. Ethnobotany of an Extinct Culture." *The Palmetto*, 17(2):7
    -11.[1](http://www.fnps.org/palmetto/v17i2p7austin.pdf) - accessed
    [December 4](../Page/December_4.md "wikilink") 2005
  - [Brickell Point - Home of the Miami Circle (State of Florida
    site)](https://web.archive.org/web/20051218205409/http://dhr.dos.state.fl.us/archaeology/projects/brickellpoint/)
    - accessed [December 4](../Page/December_4.md "wikilink") 2005
  - Bullen, Adelaide K. (1965). "Florida Indians of Past and Present".
    In Ruby L. Carson & [Charlton
    Tebeau](../Page/Charlton_W._Tebeau.md "wikilink") (Eds.), *Florida
    from Indian trail to space age: a history* (Vol. I, pp. 317-350).
    Southern Publishing Company.
  - Escalente Fontaneda, Hernando de. (1944). *Memoir of Do. d'Escalente
    Fontaneda respecting Florida*. Smith, B. (Trans.); True, D. O.
    (Ed.). Miami: University of Miami & Historical Association of
    Southern Florida.
  - Goddard, Ives. (2005). The indigenous languages of the Southeast.
    *Anthropological Linguistics*, *47* (1), 1-60.
  - Hann, John H. (1991). *Missions to the Calusa*. Gainesville:
    University of Florida Press.
  - State of Florida Office of Cultural and Historical Programs.
    "Chapter 12. South and Southeast Florida: The Everglades Region,
    2500 B.P.-Contact". *Historic Contexts*. Version of 9-27-93.
    Downloaded from
    [2](http://dhr.dos.state.fl.us/facts/reports/contexts/comp_plan.doc)
    on [March 27](../Page/March_27.md "wikilink") 2006
  - Sturtevant, William C. (1978). "The Last of the South Florida
    Aborigines". In Jeral Milanich & Samuel Proctor (Eds.). *Tachagale:
    Essays on the Indians of Florida and Southeastern Georgia during the
    Historic Period*. [Gainesville,
    Florida](../Page/Gainesville,_Florida.md "wikilink"): The University
    Presses of Florida. ISBN 0-8130-0535-3
  - [Tebeau, Charlton W.](../Page/Charlton_W._Tebeau.md "wikilink")
    (1968). *Man in the Everglades* (pp. 37-45). [Coral Gables,
    Florida](../Page/Coral_Gables,_Florida.md "wikilink"): University of
    Miami Press. Library of Congress Catalog Card Number: 68-17768
  - [The Tequesta of Biscayne
    Bay](http://fcit.usf.edu/florida/lessons/tequest/tequest1.htm) -
    accessed [December 4](../Page/December_4.md "wikilink") 2005
  - Wenhold, Lucy L. (Ed., Trans.). (1936). *A 17th century letter of
    Gabriel Diaz Vara Calderón, Bishop of Cuba, describing the Indians
    and Indian missions of Florida*. Smithsonian miscellaneous
    collections 95 (16). Washington, D.C.: Smithsonian Institution.

[Category:佛羅里達州土著](../Category/佛羅里達州土著.md "wikilink")
[Category:佛羅里達州歷史](../Category/佛羅里達州歷史.md "wikilink")

1.  [地圖](http://www.georgeglazer.com/maps/florida/delaetflorida.html)
2.  Ehrenberg, Ralph E. ["Marvellous countries and lands" Notable Maps
    of
    Florida, 1507-1846](http://www.broward.org/library/bienes/lii14003.htm)