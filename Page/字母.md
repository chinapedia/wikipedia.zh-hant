[世界范围内各字母系统的分布情况。图例：
](https://zh.wikipedia.org/wiki/File:Alphabet_world_distribution.PNG "fig:世界范围内各字母系统的分布情况。图例：         ")

**字母**是[字母系統中的](../Page/字母系統.md "wikilink")[字位](../Page/字位.md "wikilink")，是書寫時最基本的單位。像[希腊字母及](../Page/希腊字母.md "wikilink")[英文字母等](../Page/英文字母.md "wikilink")。一個字母系統基本會有二十多至三十多個字母，例如[英文字母系統中共有](../Page/英文.md "wikilink")26個字母。

字母組成了[音位](../Page/音位.md "wikilink")，而音位表示了[口语中的](../Page/口语.md "wikilink")[語音](../Page/語音.md "wikilink")。在[輔音音素文字](../Page/輔音音素文字.md "wikilink")（例如[阿拉伯文](../Page/阿拉伯文.md "wikilink")）中也有字母，但其中只有輔音字母，沒有母音字母。

其他不使用字母的文字系統有[音節文字](../Page/音節文字.md "wikilink")（例如[日文](../Page/日文.md "wikilink")），每個符號表示一個[音节](../Page/音节.md "wikilink")，或是[語素文字](../Page/語素文字.md "wikilink")（例如[汉字](../Page/汉字.md "wikilink")），每個符號可以表示一個[字](../Page/字.md "wikilink")，或是由多個符號表示一個詞。

## 定義及用法

字母和[語音有密切關係](../Page/語音.md "wikilink")。在純[音位字母系統中](../Page/音位.md "wikilink")，每一個音位都對應一個字母。不過在實務上，一個字母往往會對應多個音位。若二個字母組合後表示某音位，這二個字母組合稱為[二合字母](../Page/二合字母.md "wikilink")，例如英文中的ch、sh及th。若三個字母組合後表示某音位，這三個字母組合稱為[三合字母](../Page/三合字母.md "wikilink")，例如德文中的sch。

中文「字母」一詞源於中國古代[韻書用字母來](../Page/韻書.md "wikilink")[反切出某字之讀音](../Page/反切.md "wikilink")，例如[三十六字母](../Page/三十六字母.md "wikilink")。

一個字母也可能因為前後字母的不同，或是[语源学上的原因](../Page/语源学.md "wikilink")，對應不止一個音位。例如西班牙文的c，在a、o或u前面發\[k\]的音（例如*cantar*、*corto*、*cuidado*），在e或i前面則發\[θ\]的音（例如*centimo*、*ciudad*）。

字母可能有其對應的名稱，同一個字母可能會隨語言、歷史或方言，而有不同的名稱。例如[Z在大多數英語系的國家會讀zed](../Page/Z.md "wikilink")，但在美國會唸zee\[1\]。

字母組成字母系統．有其規定的順序，一般稱之為字母順序。是指定各種語言中字母順序的科學。像在西班牙文中，[ñ是一個獨立的字母](../Page/ñ.md "wikilink")，排序在n的後面。在英文中，n
和ñ在排序時會視為是一樣的字母。

字母也可以有其對應的數值，像[羅馬數字就是用字母來表示數值](../Page/羅馬數字.md "wikilink")。在英文中，一般會用[阿拉伯數字來表示數字](../Page/阿拉伯數字.md "wikilink")。

字母也可以當成單字使用，像英文中的a（大寫或小寫）及I（固定大寫）是都是常用的英文單字，有時在詩中會用O來代替Oh。在一些不正式的場合中（例如）會用字母來代替一些特定的單字，例如在英文中，u和you是[同音異義語](../Page/同音異義語.md "wikilink")，因此會用u來代替you。

人或事物有時也會用字母來命名，其原因可能有以下幾種：

1.  字母是某個詞語的簡寫，例如俚語中會用G-man來指稱[联邦调查局的探員](../Page/联邦调查局.md "wikilink")，G是政府的簡寫。
2.  用字母順序來計數，例如A計劃、B計劃、[α粒子](../Page/α粒子.md "wikilink")、[β粒子](../Page/β粒子.md "wikilink")、[γ射线](../Page/γ射线.md "wikilink")、、。
3.  字母本身的形狀，例如、[H型引擎](../Page/水平對臥引擎.md "wikilink")、[H型鋼](../Page/H型鋼.md "wikilink")、[O形環](../Page/O形環.md "wikilink")、[U型发动机](../Page/U型发动机.md "wikilink")、[V型引擎等](../Page/V型引擎.md "wikilink")。
4.  其他原因，像[X射線命名使用X](../Page/X射線.md "wikilink")，是因為在數學中用X表示未知數，因此用X命名，表示是一個新發現的射線。

### 經典的定義

[Leys_d'amors.jpg](https://zh.wikipedia.org/wiki/File:Leys_d'amors.jpg "fig:Leys_d'amors.jpg")
是世界上最早的文學院，曾通過舉辦來獎勵最佳的[吟游詩人](../Page/吟游詩人.md "wikilink")，其中的成員曾在其著作（1328–1337）中為字母定義：

|                                                                                                                               |                                              |
| ----------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| *Letra votz no es devisabla.* *E per escriure convenabla.* *Letra per miels esser exposta.* *Es menor part de votz composta.* | | 字母是不可分割的聲音， 適合於書寫， 若要更精準的定義， 字母是複雜聲音的最小部份。 |

## 歷史

第一個[輔音音素文字出現西元前](../Page/輔音音素文字.md "wikilink")2000年，代表埃及的[閃米特人使用的語言](../Page/閃米特人.md "wikilink")（可參考[青銅中期文化時期字母](../Page/青銅中期文化時期字母.md "wikilink")），其字元原則是由埃及的[聖書體衍生而來](../Page/聖書體.md "wikilink")。世界上大多数字母都是由閃米人的字母衍生而來，或是受到其影響\[2\]。

[希臘字母是在前](../Page/希臘字母.md "wikilink")9世纪發明，是第一個同時有[母音及](../Page/母音.md "wikilink")[子音的字母](../Page/子音.md "wikilink")\[3\]。

## 字母的類別

### 各種不同的字母

  - [阿拉伯字母](../Page/阿拉伯字母.md "wikilink")：, , , , , , , , , , , , , , , ,
    , , , , , , , , , , , .
  - [敘利亞字母](../Page/敘利亞字母.md "wikilink")：[ܐ](../Page/Aleph.md "wikilink"),
    [ܒ](../Page/Bet_\(letter\).md "wikilink"),
    [ܓ](../Page/Gimel_\(letter\).md "wikilink"),
    [ܕ](../Page/Dalet_\(letter\).md "wikilink"),
    [ܗ](../Page/He_\(letter\).md "wikilink"),
    [ܘ](../Page/Waw_\(letter\).md "wikilink"),
    [ܙ](../Page/Zayin_\(letter\).md "wikilink"),
    [ܚ](../Page/Heth_\(letter\).md "wikilink"),
    [ܛ](../Page/Tet_\(letter\).md "wikilink"),
    [ܝ](../Page/Yodh_\(letter\).md "wikilink"),
    [ܟܟ](../Page/Kaph_\(letter\).md "wikilink"),
    [ܠ](../Page/Lamedh_\(letter\).md "wikilink"),
    [ܡܡ](../Page/Mem_\(letter\).md "wikilink"),
    [ܢܢ](../Page/Nun_\(letter\).md "wikilink"),
    [ܣ](../Page/Samekh.md "wikilink"),
    [ܥ](../Page/Ayin_\(letter\).md "wikilink"),
    [ܦ](../Page/Pe_\(letter\).md "wikilink"),
    [ܨ](../Page/Tzadi.md "wikilink"), [ܩ](../Page/Qoph.md "wikilink"),
    [ܪ](../Page/Resh_\(letter\).md "wikilink"),
    [ܫ](../Page/Shin_\(letter\).md "wikilink"),
    [ܬ](../Page/Taw.md "wikilink").
  - [西里爾字母](../Page/西里爾字母.md "wikilink")：[А](../Page/А.md "wikilink"),
    [Б](../Page/Б.md "wikilink"), [В](../Page/В.md "wikilink"),
    [Г](../Page/Г.md "wikilink"), [Ґ](../Page/Ґ.md "wikilink"),
    [Д](../Page/Д.md "wikilink"), [Е](../Page/Е.md "wikilink"),
    [Є](../Page/Є.md "wikilink"), [Ж](../Page/Ж.md "wikilink"),
    [З](../Page/З.md "wikilink"), [И](../Page/И.md "wikilink"),
    [І](../Page/І.md "wikilink"), [Ї](../Page/Ї.md "wikilink"),
    [Й](../Page/Й.md "wikilink"), [К](../Page/К.md "wikilink"),
    [Л](../Page/Л.md "wikilink"), [М](../Page/М.md "wikilink"),
    [Н](../Page/Н.md "wikilink"), [О](../Page/О.md "wikilink"),
    [П](../Page/П.md "wikilink"), [Р](../Page/Р.md "wikilink"),
    [С](../Page/С.md "wikilink"), [Т](../Page/Т.md "wikilink"),
    [У](../Page/У.md "wikilink"), [Ф](../Page/Ф.md "wikilink"),
    [Х](../Page/Х.md "wikilink"), [Ц](../Page/Ц.md "wikilink"),
    [Ч](../Page/Ч.md "wikilink"), [Ш](../Page/Ш.md "wikilink"),
    [Щ](../Page/Щ.md "wikilink"), [Ю](../Page/Ю.md "wikilink"),
    [Я](../Page/Я.md "wikilink"), [Ъ](../Page/Ъ.md "wikilink"),
    [Ь](../Page/Ь.md "wikilink"), [Ђ](../Page/Ђ.md "wikilink"),
    [Љ](../Page/Љ.md "wikilink"), [Њ](../Page/Њ.md "wikilink"),
    [Ћ](../Page/Ћ.md "wikilink"), [Џ](../Page/Џ.md "wikilink")
  - [希臘字母](../Page/希臘字母.md "wikilink")：[Α](../Page/Α.md "wikilink"),
    [Β](../Page/Β.md "wikilink"), [Γ](../Page/Γ.md "wikilink"),
    [Δ](../Page/Δ.md "wikilink"), [Ε](../Page/Ε.md "wikilink"),
    [Ζ](../Page/Ζ.md "wikilink"), [Η](../Page/Η.md "wikilink"),
    [Θ](../Page/Θ.md "wikilink"), [Ι](../Page/Ι.md "wikilink"),
    [Κ](../Page/Κ.md "wikilink"), [Λ](../Page/Λ.md "wikilink"),
    [Μ](../Page/Μ.md "wikilink"), [Ν](../Page/Ν.md "wikilink"),
    [Ξ](../Page/Ξ.md "wikilink"), [Ο](../Page/Ο.md "wikilink"),
    [Π](../Page/Π.md "wikilink"), [Ρ](../Page/Ρ.md "wikilink"),
    [Σ](../Page/Σ.md "wikilink"), [Τ](../Page/Τ.md "wikilink"),
    [Υ](../Page/Υ.md "wikilink"), [Φ](../Page/Φ.md "wikilink"),
    [Χ](../Page/Χ.md "wikilink"), [Ψ](../Page/Ψ.md "wikilink"),
    [Ω](../Page/Ω.md "wikilink").
  - [希伯來字母](../Page/希伯來字母.md "wikilink")：[א](../Page/Aleph_\(letter\).md "wikilink"),
    [ב](../Page/Beth_\(letter\).md "wikilink"),
    [ג](../Page/Gimel_\(letter\).md "wikilink"),
    [ד](../Page/Dalet.md "wikilink"),
    [ה](../Page/He_\(letter\).md "wikilink"),
    [ו](../Page/Waw_\(letter\).md "wikilink"),
    [ז](../Page/Zayin.md "wikilink"), [ח](../Page/Heth.md "wikilink"),
    [ט](../Page/Teth.md "wikilink"), [י](../Page/Yodh.md "wikilink"),
    [כ](../Page/Kaph.md "wikilink"), [ל](../Page/Lamedh.md "wikilink"),
    [מ](../Page/Mem.md "wikilink"),
    [נ](../Page/Nun_\(letter\).md "wikilink"),
    [ס](../Page/Samekh.md "wikilink"), [ע](../Page/Ayin.md "wikilink"),
    [פ](../Page/Pe_\(letter\).md "wikilink"),
    [צ](../Page/Tsade.md "wikilink"), [ק](../Page/Qoph.md "wikilink"),
    [ר](../Page/Resh.md "wikilink"),
    [ש](../Page/Shin_\(letter\).md "wikilink"),
    [ת](../Page/Taw_\(letter\).md "wikilink").
  - [諺文字母](../Page/諺文字母列表.md "wikilink")：[ㄱ](../Page/ㄱ.md "wikilink")
    [ㄲ](../Page/ㄲ.md "wikilink") [ㄴ](../Page/ㄴ.md "wikilink")
    [ㄷ](../Page/ㄷ.md "wikilink") [ㄸ](../Page/ㄸ.md "wikilink")
    [ㄹ](../Page/ㄹ.md "wikilink") [ㅁ](../Page/ㅁ.md "wikilink")
    [ㅂ](../Page/ㅂ.md "wikilink") [ㅃ](../Page/ㅃ.md "wikilink")
    [ㅅ](../Page/ㅅ.md "wikilink") [ㅆ](../Page/ㅆ.md "wikilink")
    [ㅇ](../Page/ㅇ.md "wikilink") [ㅈ](../Page/ㅈ.md "wikilink")
    [ㅉ](../Page/ㅉ.md "wikilink") [ㅊ](../Page/ㅊ.md "wikilink")
    [ㅋ](../Page/ㅋ.md "wikilink") [ㅌ](../Page/ㅌ.md "wikilink")
    [ㅍ](../Page/ㅍ.md "wikilink") [ㅎ](../Page/ㅎ.md "wikilink")
    [ㅏ](../Page/ㅏ.md "wikilink") [ㅐ](../Page/ㅐ.md "wikilink")
    [ㅑ](../Page/ㅑ.md "wikilink") [ㅒ](../Page/ㅒ.md "wikilink")
    [ㅓ](../Page/ㅓ.md "wikilink") [ㅔ](../Page/ㅔ.md "wikilink")
    [ㅕ](../Page/ㅕ.md "wikilink") [ㅖ](../Page/ㅖ.md "wikilink")
    [ㅗ](../Page/ㅗ.md "wikilink") [ㅘ](../Page/ㅘ.md "wikilink")
    [ㅙ](../Page/ㅙ.md "wikilink") [ㅚ](../Page/ㅚ.md "wikilink")
    [ㅛ](../Page/ㅛ.md "wikilink") [ㅜ](../Page/ㅜ.md "wikilink")
    [ㅝ](../Page/ㅝ.md "wikilink") [ㅞ](../Page/ㅞ.md "wikilink")
    [ㅟ](../Page/ㅟ.md "wikilink") [ㅠ](../Page/ㅠ.md "wikilink")
    [ㅡ](../Page/ㅡ.md "wikilink") [ㅢ](../Page/ㅢ.md "wikilink")
    [ㅣ](../Page/ㅣ.md "wikilink")
  - [拉丁字母](../Page/拉丁字母.md "wikilink")：[A](../Page/A.md "wikilink"),
    [B](../Page/B.md "wikilink"), [C](../Page/C.md "wikilink"),
    [D](../Page/D.md "wikilink"), [E](../Page/E.md "wikilink"),
    [F](../Page/F.md "wikilink"), [G](../Page/G.md "wikilink"),
    [H](../Page/H.md "wikilink"), [I](../Page/I.md "wikilink"),
    [J](../Page/J.md "wikilink"), [K](../Page/K.md "wikilink"),
    [L](../Page/L.md "wikilink"), [M](../Page/M.md "wikilink"),
    [N](../Page/N.md "wikilink"), [O](../Page/O.md "wikilink"),
    [P](../Page/P.md "wikilink"), [Q](../Page/Q.md "wikilink"),
    [R](../Page/R.md "wikilink"), [S](../Page/S.md "wikilink"),
    [T](../Page/T.md "wikilink"), [U](../Page/U.md "wikilink"),
    [V](../Page/V.md "wikilink"), [W](../Page/W.md "wikilink"),
    [X](../Page/X.md "wikilink"), [Y](../Page/Y.md "wikilink"),
    [Z](../Page/Z.md "wikilink").

其他[書寫系統和他們的字母](../Page/書寫系統.md "wikilink")，請參見[書寫系統列表](../Page/書寫系統列表.md "wikilink")。

[Cyrillic_JA.png](https://zh.wikipedia.org/wiki/File:Cyrillic_JA.png "fig:Cyrillic_JA.png")的[Я](../Page/Я.md "wikilink")，大寫及小寫字母，下方是斜體的大寫及小寫字母\]\]

### 大寫及小寫

有些字母系統中．每個字母有二種不同的形式，分別稱為「大寫」及「小寫」，大寫字母及小寫字母表示的聲音一様，但在書寫時有不同的功能。大寫字母常出現在一句話的第一個字母，[專有名詞的第一個字母等](../Page/專有名詞.md "wikilink")。不過隨著語言的不同，也可能有其他的功能，例如在[德文中](../Page/德文.md "wikilink")，所有名詞的第一個字母都要大寫。

### 字體和字形

[字體是指一套有單一風格的字母或字體形式](../Page/字體.md "wikilink")。依選用字體的不同．字母的形式、字寬、傾斜角度或是一些細節也可能有所不同。[字型除了指定字體外](../Page/字型.md "wikilink")，還指定其字體的大小。

[書法是指用藝術的方式書寫字母或字形](../Page/書法.md "wikilink")．作品中相同的字有可能有不同的方式來表達。

## 字母频率

字母频率是指特定字母出現的頻率，某一語言的字母頻率可以用分析大量的文章，計算各字母的相對頻率而得到。字母频率可以用在[密碼學中](../Page/密碼學.md "wikilink")，也可以用在其他的領域。英文中最常出現的十個字母是*e*,
*t*, *a*, *o*, *i*, *n*, *s*, *h*, *r*及*d*．其中字母*e*出現的頻率約有13%。

## 參見

  - [字母歷史](../Page/字母歷史.md "wikilink")
  - [字形](../Page/字形.md "wikilink")
  - [字體](../Page/字體.md "wikilink")
  - [衍生拉丁字母](../Page/衍生拉丁字母.md "wikilink")
  - [原始西奈文字系譜](../Page/原始西奈文字系譜.md "wikilink")
  - [字母频率](../Page/字母频率.md "wikilink")
  - [元音附標文字](../Page/元音附標文字.md "wikilink")
  - [辅音音素文字](../Page/辅音音素文字.md "wikilink")
  - [字母系統](../Page/字母系統.md "wikilink")

## 參考資料

[it:Lettera (alfabeto)](../Page/it:Lettera_\(alfabeto\).md "wikilink")

[字母](../Category/字母.md "wikilink")

1.  "Z", *Oxford English Dictionary,* 2nd edition (1989);
    *Merriam-Webster's Third New International Dictionary of the English
    Language, Unabridged* (1993); "zee", *op. cit*.
2.  Himelfarb, Elizabeth J. "First Alphabet Found in Egypt",
    *Archaeology* 53, Issue 1 (Jan./Feb. 2000): 21.
3.