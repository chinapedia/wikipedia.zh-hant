**功夫**，又称武功，不仅是搏击术和单纯的拳脚运动，现在，“功夫”有了更深刻的内涵：它讲究的不再仅仅是武艺高强，而是刚柔并济、内外兼修，即习武之人被要求拥有高尚的品德或情操。\[1\]功夫有两种外在表现形式，一种是套路演练形式，另一种是格斗对抗形式，一般上讲的功夫是实用性格斗对抗形式。

「功夫」一詞最早被廣泛認識的是在[二十世紀六十至](../Page/1960年代.md "wikilink")[七十年代](../Page/1970年代.md "wikilink")，由[李小龍開始把這兩個字發揚光大](../Page/李小龍.md "wikilink")；因李氏對武術界的貢獻和影響力，所以功夫的粵語音譯「Kung
Fu」一詞被寫入英文字典內。

## 注释

<references group="註" />

## 参考文献

## 外部链接

  - [馬氏詠春](http://ffoecaf.pixnet.net/)
  - [香港少林](http://www.shaolinhk.com/)

<!-- end list -->

  - [1](http://www.energyfight.com.hk)

[Category:武术](../Category/武术.md "wikilink")
[Category:MixedCombat綜合搏擊](../Category/MixedCombat綜合搏擊.md "wikilink")

1.