[Perseverance_of_Decapitated_Tree.jpg](https://zh.wikipedia.org/wiki/File:Perseverance_of_Decapitated_Tree.jpg "fig:Perseverance_of_Decapitated_Tree.jpg")

**美德**（拉丁语virtus，古希腊语：ἀρετή “arete”）是道德上的卓越。
美德被认为是善在道德上体现出来的特质或者品质，因此美德被视为美好原则和道德的基础。
个人美德被视为是促进和实现集体以及个人伟大的特性。

源自于柏拉图的四个经典的基本美德（枢德）是：节制，审慎，勇气和正义。

**美德**指美好的[品德](../Page/品德.md "wikilink")，優良的品質、情操和[行為](../Page/行為.md "wikilink")，是每一個人都應該培養的優點。一個富有美[德的人](../Page/德.md "wikilink")，是一個有[氣質的人](../Page/氣質.md "wikilink")，被稱為有涵養，有內在美。

## 词义

優美的德性。

[史記](../Page/史記.md "wikilink")·卷二十三·禮書：「洋洋美德乎！宰制萬物，役使群眾，豈人力也哉？」

[魏書](../Page/魏書.md "wikilink")·卷五十·尉元傳：「若不屈從高謨，復何以成其美德也。」

### 相反詞

劣行﹑惡習

## 美德分類

美德包含許多方面，有從正面列舉數算的，也有從負面描述的（不甚麼、不甚麼）。本文盡量嘗試從正面列舉，如[誠實](../Page/誠實.md "wikilink")、[勤奮](../Page/勤奮.md "wikilink")、[恭敬](../Page/恭敬.md "wikilink")、[節儉](../Page/節儉.md "wikilink")、[端莊](../Page/端莊.md "wikilink")、[愛心](../Page/愛心.md "wikilink")、[準時](../Page/準時.md "wikilink")、[寬容](../Page/寬容.md "wikilink")、[饒恕](../Page/饒恕.md "wikilink")、[和藹等](../Page/和藹.md "wikilink")。

### 中國和东亚儒家文化圈中的美德

  - [孔子](../Page/孔子.md "wikilink")、[孟子講](../Page/孟子.md "wikilink")[忠](../Page/忠.md "wikilink")、[孝](../Page/孝.md "wikilink")、[仁](../Page/仁.md "wikilink")、[義](../Page/義.md "wikilink")、[禮](../Page/禮.md "wikilink")、[智等](../Page/智.md "wikilink")。[孟子談論](../Page/孟子.md "wikilink")[四端說](../Page/四端.md "wikilink")：「[惻隱之心](../Page/惻隱.md "wikilink")，仁之端也；[羞惡之心](../Page/羞惡.md "wikilink")，義之端也；[辭讓之心](../Page/辭讓.md "wikilink")，禮之端也；[是非之心](../Page/是非.md "wikilink")，智之端也。」《[中庸](../Page/中庸.md "wikilink")》所講的[三達德為](../Page/三達德.md "wikilink")[智](../Page/智.md "wikilink")、[仁](../Page/仁.md "wikilink")、[勇](../Page/勇.md "wikilink")。
  - [中国画裏的](../Page/中国画.md "wikilink")[梅](../Page/梅.md "wikilink")、[蘭](../Page/蘭科.md "wikilink")、[竹](../Page/竹.md "wikilink")、[菊各被賦予了美德的象徵](../Page/菊属.md "wikilink")：梅代表[堅毅](../Page/堅毅.md "wikilink")、[忍耐](../Page/忍耐.md "wikilink")，蘭代表[高雅](../Page/高雅.md "wikilink")、[文靜](../Page/文靜.md "wikilink")，竹代表[忠貞](../Page/忠貞.md "wikilink")、[正直](../Page/正直.md "wikilink")、[謙虛](../Page/謙虛.md "wikilink")，菊代表[真誠](../Page/真誠.md "wikilink")、[樸素](../Page/樸素.md "wikilink")。
  - [孔融讓梨的故事家喻戶曉](../Page/孔融.md "wikilink")，歌誦[孔融對其哥哥](../Page/孔融.md "wikilink")[謙讓的美德](../Page/謙讓.md "wikilink")。

### 其它地区和文化中的美德

#### 古埃及时期

在古埃及文明中，马特 （Maat）或马阿特（ Ma‘at）（发音被认为是 \[muʔ.ʕat\]）也会被拼写为māt 或
mayet，是古埃及的真理，平衡，秩序，法律，道德和正义的概念。马特也被人格化为一个女神，该女神掌管星星，季节，以及凡人们和神灵们的行为。在创世的时刻，神灵们从混乱中建立了宇宙的秩序。马特（意识形态）的对立面是伊斯菲特（Isfet），伊斯菲特象征着混乱，谎言和不正义。

#### 西方欧美地区

  - 西方欧美世界所公認的四種傳統美德（[枢德](../Page/枢德.md "wikilink")）為[智慧](../Page/智慧.md "wikilink")、[公正](../Page/公正.md "wikilink")、[勇氣和](../Page/勇氣.md "wikilink")[適度](../Page/適度.md "wikilink")。
  - 中世纪欧洲曾将道德等同于宗教信仰，认为离开了基督教就谈不上道德。基督教《[聖經](../Page/聖經.md "wikilink")》中以[信](../Page/信.md "wikilink")、[望](../Page/望.md "wikilink")、[愛為信徒的三大美德](../Page/愛.md "wikilink")（[超德](../Page/超德.md "wikilink")）。[天主教講的](../Page/天主教.md "wikilink")[七美德為](../Page/七美德.md "wikilink")「樞德」與「超德」的集合：[貞潔](../Page/貞潔.md "wikilink")、[適度](../Page/適度.md "wikilink")、[慷慨](../Page/慷慨.md "wikilink")、[熱心](../Page/熱心.md "wikilink")、[溫純](../Page/溫純.md "wikilink")、[善施](../Page/捐贈.md "wikilink")（一說[博愛](../Page/博愛.md "wikilink")）和[謙卑](../Page/謙卑.md "wikilink")。

#### 伊斯兰教社会（主要分布于[西亚](../Page/西亚.md "wikilink")、[北非等地区](../Page/北非.md "wikilink")）

  - [伊斯蘭教所公認的四種傳統美德為](../Page/伊斯蘭教.md "wikilink")[智慧](../Page/智慧.md "wikilink")、善於交流、[信用和](../Page/信用.md "wikilink")[智力](../Page/智力.md "wikilink")。至今在许多伊斯兰教国家，人们认为离开了伊斯兰教就谈不上道德。

#### 印度

  - [印度](../Page/印度.md "wikilink")：[诚实](../Page/诚实.md "wikilink")、[勇氣](../Page/勇氣.md "wikilink")、[服务](../Page/服务.md "wikilink")、[自我控制](../Page/自我控制.md "wikilink")、[非暴力](../Page/非暴力.md "wikilink")

## 参考文献

## 參見

  - [道德](../Page/道德.md "wikilink")
  - [惡行](../Page/惡行.md "wikilink")
  - [亞里士多德](../Page/亞里士多德.md "wikilink")

{{-}}

[Category:人際關係](../Category/人際關係.md "wikilink")
[Category:倫理學](../Category/倫理學.md "wikilink")
[美德](../Category/美德.md "wikilink")
[Category:道德觀念](../Category/道德觀念.md "wikilink")