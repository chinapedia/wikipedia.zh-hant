## 大事记

  - **[中国](../Page/中国.md "wikilink")**
      - [朱粲建立的](../Page/朱粲.md "wikilink")[楚滅亡](../Page/楚_\(朱粲\).md "wikilink")。
      - [李世民在](../Page/李世民.md "wikilink")[虎牢之战中打败](../Page/虎牢之战.md "wikilink")[窦建德](../Page/窦建德.md "wikilink")，迫使[王世充投降](../Page/王世充.md "wikilink")，[竇建德建立的](../Page/竇建德.md "wikilink")[夏與](../Page/夏_\(竇建德\).md "wikilink")[王世充建立的](../Page/王世充.md "wikilink")[鄭滅亡](../Page/鄭_\(王世充\).md "wikilink")。
      - [唐平蕭銑之戰](../Page/唐平蕭銑之戰.md "wikilink")，[李孝恭率唐軍攻滅長江中游](../Page/李孝恭.md "wikilink")、江南割據勢力[蕭銑](../Page/蕭銑.md "wikilink")。
      - [高開道叛離](../Page/高開道.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")，北連[突厥](../Page/突厥.md "wikilink")，南聯[劉黑闥](../Page/劉黑闥.md "wikilink")，復稱燕國。
      - [劉黑闥率](../Page/劉黑闥.md "wikilink")[竇建德餘部起兵](../Page/竇建德.md "wikilink")，半年恢復[竇建德原有勢力](../Page/竇建德.md "wikilink")，並遣使北結[突厥](../Page/突厥.md "wikilink")。
      - 吳王[杜伏威平定](../Page/杜伏威.md "wikilink")[李子通](../Page/李子通.md "wikilink")。
      - [8月2日](../Page/8月2日.md "wikilink")－[開元通寶鑄行](../Page/開元通寶.md "wikilink")。

<!-- end list -->

  - **[中東](../Page/中東.md "wikilink")**
      - [7月27日](../Page/7月27日.md "wikilink")——夜，《古蘭經》記載[穆罕默德在](../Page/穆罕默德.md "wikilink")[麥加被天使帶著神獸迎接](../Page/麥加.md "wikilink")，「夜行」瞬間趕到[耶路撒冷](../Page/耶路撒冷.md "wikilink")。

## 出生

  -
## 逝世

  - [窦建德](../Page/窦建德.md "wikilink")，隋末唐初割據勢力之一。
  - [王世充](../Page/王世充.md "wikilink")，隋末唐初割據勢力之一。
  - [孟海公](../Page/孟海公.md "wikilink")，[隋末民變首領之一](../Page/隋末民變.md "wikilink")，後歸附[竇建德](../Page/竇建德.md "wikilink")。
  - [蕭銑](../Page/蕭銑.md "wikilink")，[西梁宣宗曾孫](../Page/西梁宣宗.md "wikilink")，[隋煬帝](../Page/隋煬帝.md "wikilink")[蕭皇后外戚](../Page/蕭皇后.md "wikilink")，割據長江中游地區，南到[交趾](../Page/交趾.md "wikilink")，北到[漢水](../Page/漢水.md "wikilink")，西達[長江三峽](../Page/長江三峽.md "wikilink")，東及[九江](../Page/九江.md "wikilink")。
  - [沈法興](../Page/沈法興.md "wikilink")，隋末唐初割據勢力之一。
  - [6月5日](../Page/6月5日.md "wikilink")——[單雄信](../Page/單雄信.md "wikilink")，[隋唐十八好漢之一](../Page/隋唐十八好漢.md "wikilink")，被唐軍圍困伏牛山，見勝利無望，單驅馬跳崖未死，被俘押至[洛陽](../Page/洛陽.md "wikilink")，拒不投降，最後斬於洛陽。
  - [6月5日](../Page/6月5日.md "wikilink")——[朱粲](../Page/朱粲.md "wikilink")，[隋末民變領袖之一](../Page/隋末民變.md "wikilink")，號稱[楚皇帝](../Page/楚.md "wikilink")，依附的[王世充兵敗後被](../Page/王世充.md "wikilink")[李世民生擒於](../Page/李世民.md "wikilink")[洛水上斬殺](../Page/洛水.md "wikilink")。

[\*](../Category/621年.md "wikilink")
[1年](../Category/620年代.md "wikilink")
[2](../Category/7世纪各年.md "wikilink")