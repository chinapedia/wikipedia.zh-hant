[諸蕃志（四庫）.jpg](https://zh.wikipedia.org/wiki/File:諸蕃志（四庫）.jpg "fig:諸蕃志（四庫）.jpg")

《**诸蕃志**》是由[南宋](../Page/南宋.md "wikilink")[泉州](../Page/泉州.md "wikilink")[市舶司](../Page/市舶司.md "wikilink")[提举](../Page/提举.md "wikilink")[赵汝-{适}-于](../Page/赵汝适.md "wikilink")[宝庆元年](../Page/宝庆.md "wikilink")（1225年）著成。《诸蕃志》分上下卷，卷上志国，卷下志物。

《诸蕃志》全書涉及158國家和地區，趙汝适本人未親自訪問，只是向到訪中國的商人多方詢問，“列其國名，道其風土，與夫道里之聯屬，山澤之畜產，譯以華言，刪其污渫，存其事實，名曰《诸蕃志》。”

《诸蕃志》的一些条目来自《[岭外代答](../Page/岭外代答.md "wikilink")》（1178年）。

《[文獻通考](../Page/文獻通考.md "wikilink")》、《[宋史](../Page/宋史.md "wikilink")》、《[密齋筆記](../Page/密齋筆記.md "wikilink")》等书都引用《诸蕃志》。

## 卷上志国

记述[交趾国](../Page/交趾.md "wikilink")、[占城国](../Page/占城.md "wikilink")、[真腊国](../Page/真腊.md "wikilink")、[宾瞳龙国](../Page/宾瞳龙.md "wikilink")、[登留眉国](../Page/登留眉.md "wikilink")、[蒲甘国](../Page/蒲甘.md "wikilink")、[三佛齐国](../Page/三佛齐.md "wikilink")、[单马令国](../Page/单马令.md "wikilink")、[凌牙斯加国](../Page/狼牙脩.md "wikilink")、[佛罗安国](../Page/佛罗安.md "wikilink")、[新拖国](../Page/新拖.md "wikilink")、[监篦国](../Page/监篦.md "wikilink")、[兰无里国](../Page/兰无里.md "wikilink")、[细兰](../Page/锡兰.md "wikilink")、[苏吉丹](../Page/苏吉丹.md "wikilink")、[南毗国](../Page/古里.md "wikilink")、[故临国](../Page/故临.md "wikilink")
，[胡茶辣国](../Page/胡茶辣.md "wikilink")、[麻囉华国](../Page/麻囉华.md "wikilink")、[注辇国](../Page/注辇.md "wikilink")、[鹏茄罗国](../Page/鹏茄罗.md "wikilink")、[南尼华囉国](../Page/南尼华囉.md "wikilink")、[大秦国](../Page/大秦.md "wikilink")、[大食国](../Page/大食.md "wikilink")、[麻嘉国](../Page/麦加.md "wikilink")、[弼琶囉国](../Page/弼琶囉.md "wikilink")、
[层拔国](../Page/桑给巴尔.md "wikilink")、
[中理国](../Page/中理.md "wikilink")、[瓮蛮国](../Page/阿曼.md "wikilink")、[白达国](../Page/巴格达.md "wikilink")、[吉兹尼国](../Page/吉兹尼.md "wikilink")、[忽厮离国](../Page/忽厮离.md "wikilink")、[木兰皮国](../Page/木兰皮.md "wikilink")、[遏根陀国](../Page/遏根陀.md "wikilink")、[茶弼沙国](../Page/茶弼沙.md "wikilink")、[斯伽里野国](../Page/西西里岛.md "wikilink")、[默伽猎国](../Page/摩洛哥.md "wikilink")、[渤泥国](../Page/渤泥.md "wikilink")、[麻逸国](../Page/菲律宾.md "wikilink")、[三屿国](../Page/三屿.md "wikilink")、[蒲哩鲁国](../Page/蒲哩鲁.md "wikilink")、[流求国](../Page/琉球.md "wikilink")、[毗舍耶国](../Page/毗舍邪国.md "wikilink")、[新罗国](../Page/新罗.md "wikilink")、[倭国等五十八个国](../Page/倭国.md "wikilink")；其中的「斯伽里野国」篇记述的是[西西里岛的地理和岛上的活火山](../Page/西西里岛.md "wikilink")。这是中文古典典籍中最早记述[意大利](../Page/意大利.md "wikilink")[西西里岛和](../Page/西西里岛.md "wikilink")[埃特纳火山的著作](../Page/埃特纳火山.md "wikilink")。

## 卷下志物

记述脑子、[乳香](../Page/乳香.md "wikilink")、[没药](../Page/没药.md "wikilink")、[血碣](../Page/血碣.md "wikilink")、金颜香、笃褥香、[苏合香油](../Page/苏合香油.md "wikilink")、[安息香](../Page/安息香.md "wikilink")、桤子花、[蔷薇水](../Page/蔷薇.md "wikilink")、[沉香](../Page/沉香.md "wikilink")、速暂香、黄熟香、生香、[檀香](../Page/檀香.md "wikilink")、[丁香](../Page/丁香.md "wikilink")、肉[豆蔻](../Page/豆蔻.md "wikilink")、降真香、[麝香木](../Page/麝香.md "wikilink")、[波罗密](../Page/波罗密.md "wikilink")、[槟榔](../Page/槟榔.md "wikilink")、[椰子](../Page/椰子.md "wikilink")、[没石子](../Page/没石子.md "wikilink")、吉贝、椰心簟、木香、白豆蔻、[胡椒](../Page/胡椒.md "wikilink")、蓽澄茄、阿魏、[芦荟](../Page/芦荟.md "wikilink")、[珊瑚树](../Page/珊瑚.md "wikilink")、[琉璃](../Page/琉璃.md "wikilink")、[猫儿睛](../Page/猫儿睛.md "wikilink")、硨磲、[象牙](../Page/象牙.md "wikilink")、[犀角](../Page/犀角.md "wikilink")、[腽肭脐](../Page/腽肭脐.md "wikilink")、[鹦鹉](../Page/鹦鹉.md "wikilink")、[蝳瑁](../Page/玳瑁.md "wikilink")、[龍涎](../Page/龍涎.md "wikilink")、[黄蜡等五十四篇](../Page/黄蜡.md "wikilink")。

## 版本

  - 《[永乐大典](../Page/永乐大典.md "wikilink")》本
  - 《[函海](../Page/函海.md "wikilink")》本，[乾隆](../Page/乾隆.md "wikilink")、[道光](../Page/道光.md "wikilink")、[光緒刻本](../Page/光緒.md "wikilink")。
  - 《學津討原》
  - 《[四庫全書](../Page/四庫全書.md "wikilink")》本
  - 1937年[馮承鈞註釋本](../Page/馮承鈞.md "wikilink")
  - 楊博文注释：《诸蕃志校释》，[中华书局出版](../Page/中华书局.md "wikilink")，ISBN
    7-101-02059-3

## 译本

  - 《诸蕃志》有1911年[德国](../Page/德国.md "wikilink")[汉学家](../Page/汉学.md "wikilink")[夏德和](../Page/夏德.md "wikilink")[美国](../Page/美国.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")[柔克义合译的](../Page/柔克义.md "wikilink")[英文本](../Page/英文.md "wikilink")。
  - [法国](../Page/法国.md "wikilink")[汉学家](../Page/汉学家.md "wikilink")[费琅在](../Page/费琅.md "wikilink")《苏门答腊古国考》一书中，曾将《诸蕃志·三佛齐国》全文译成[法文](../Page/法文.md "wikilink")。

## 参见

  - 《[岭外代答](../Page/岭外代答.md "wikilink")》

## 註釋

  - F. Hirth and W. W. Rockhill (1991): *Chau Ju-kua, His work on the
    Chinese and Arab trade in the twelfth and thirteenth centuries,
    entitled Chu-fan-chî*, St Petersburg.

  - （法）费琅著《苏门答腊古国考》，[冯承钧译注](../Page/冯承钧.md "wikilink")，中华书局，2002年，ISBN
    710103541-8。

## 外部連結

  - [中国大百科全书中国历史
    《诸蕃志》](http://white-collar.net/02-lib/01-zg/03-guoxue/%C6%E4%CB%FB%C0%FA%CA%B7%CA%E9%BC%AE/%C0%FA%CA%B7%B9%A4%BE%DF%C0%E0/%D6%D0%B9%FA%B4%F3%B0%D9%BF%C6%C8%AB%CA%E9%D6%D0%B9%FA%C0%FA%CA%B7/Resource/Book/Edu/JXCKS/TS011098/0738_ts011098.htm)
  - [大英百科全書線上繁體中文版
    诸蕃志](http://tw.britannica.com/MiniSite/Article/id00014028.html)

[category:中外交通史文献](../Page/category:中外交通史文献.md "wikilink")
[category:地理書籍](../Page/category:地理書籍.md "wikilink")

[Category:史部地理類](../Category/史部地理類.md "wikilink")
[Category:宋朝典籍](../Category/宋朝典籍.md "wikilink")