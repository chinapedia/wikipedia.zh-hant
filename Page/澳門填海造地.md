[Macau_topographic_map-fr_animated.gif](https://zh.wikipedia.org/wiki/File:Macau_topographic_map-fr_animated.gif "fig:Macau_topographic_map-fr_animated.gif")

**[澳門](../Page/澳門.md "wikilink")**主要依靠**[填海造地](../Page/填海造地.md "wikilink")**來擴展可用[土地](../Page/土地.md "wikilink")。由於山多平地少，在[澳門歷史上](../Page/澳門歷史.md "wikilink")，[澳葡政府先後把](../Page/葡屬澳門.md "wikilink")[城市的範圍擴展至](../Page/城市.md "wikilink")[關閘](../Page/關閘.md "wikilink")，佔領[青洲](../Page/青洲_\(澳門\).md "wikilink")、[氹仔](../Page/氹仔.md "wikilink")、[路環](../Page/路環.md "wikilink")，又向[清朝政府索要](../Page/清朝政府.md "wikilink")[橫琴島](../Page/橫琴島.md "wikilink")、[對面山](../Page/灣仔鎮.md "wikilink")，亦在1863年進行第一次[填海工程](../Page/填海.md "wikilink")。截至2011年時，[澳門半島的面積在填海工程下已超過](../Page/澳門半島.md "wikilink")9.3平方公里，比1840年的2.78平方公里增大了3倍以上。縱使澳門沒有法定的水域，澳葡政府時代已在沿岸的[習慣水域內填出不少土地](../Page/習慣水域.md "wikilink")，且無須事先徵得[中國政府的同意](../Page/中國政府.md "wikilink")；[澳門回歸後](../Page/澳門回歸.md "wikilink")，[特區政府仍沿用此習慣](../Page/澳門特區政府.md "wikilink")，直至2002年2月1日，中國開始實施《[海域使用管理法](../Page/海域使用管理法.md "wikilink")》，新的填海工程至始需要取得中國政府的同意。

澳門總面積因沿岸填海而不斷擴大，自有記錄的1912年的11.6平方公里逐步擴展至2011年的29.9平方公里。其中:

  - [澳門半島面積為](../Page/澳門半島.md "wikilink")9.3平方公里，佔總面積的31.1%；
  - [氹仔島面積為](../Page/氹仔島.md "wikilink")7.4平方公里，佔總面積的24.7%；
  - [路環島面積為](../Page/路環島.md "wikilink")7.6平方公里，佔總面積的25.4%；
  - [路氹填海區面積為](../Page/路氹城.md "wikilink")5.6平方公里，佔總面積的18.7%。

從數據顯示現今澳門的土地大部分從填海得來。隨著現今配合[賭場集團的建設](../Page/賭場.md "wikilink")、以及城市建設的需求，路環、氹仔、[路氹城以及](../Page/路氹城.md "wikilink")[新城區的填海正使澳門的總面積繼續擴張](../Page/澳門新城區.md "wikilink")。

## 澳門總面積變化

[Mapa_Peninsula_Macau_1889.gif](https://zh.wikipedia.org/wiki/File:Mapa_Peninsula_Macau_1889.gif "fig:Mapa_Peninsula_Macau_1889.gif")和[青洲](../Page/青洲.md "wikilink")\]\]
[MapaMacau1912.jpg](https://zh.wikipedia.org/wiki/File:MapaMacau1912.jpg "fig:MapaMacau1912.jpg")、[大氹](../Page/大氹.md "wikilink")、[小氹及](../Page/小氹.md "wikilink")[路環島](../Page/路環島.md "wikilink")\]\]
[MapaMacau1986.jpg](https://zh.wikipedia.org/wiki/File:MapaMacau1986.jpg "fig:MapaMacau1986.jpg")全境\]\]

以下數據單位為平方公里\[1\]

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>澳門半島</p></th>
<th><p>氹仔島</p></th>
<th><p>路環島</p></th>
<th><p>路氹填海區</p></th>
<th><p>新城a區</p></th>
<th><p>港珠澳大橋<br />
澳門口岸</p></th>
<th><p>澳門總面積</p></th>
<th><p>澳門大學</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1912</p></td>
<td><p>3.4</p></td>
<td><p>2.3</p></td>
<td><p>5.9</p></td>
<td><p>rowspan="6" </p></td>
<td><p>rowspan="25" </p></td>
<td><p>rowspan="25" </p></td>
<td><p><strong>11.6</strong></p></td>
<td><p>rowspan="20" </p></td>
</tr>
<tr class="even">
<td><p>1936</p></td>
<td><p>5.2</p></td>
<td><p>2.6</p></td>
<td><p>6.0</p></td>
<td><p>'''13.8 '''</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1957</p></td>
<td><p>5.5</p></td>
<td><p>3.3</p></td>
<td><p>6.3</p></td>
<td><p><strong>15.1</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td><p>5.8</p></td>
<td><p>3.7</p></td>
<td><p>7.1</p></td>
<td><p><strong>16.6</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p>6.5</p></td>
<td><p>4.0</p></td>
<td><p>7.6</p></td>
<td><p><strong>18.1</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>7.7</p></td>
<td><p>5.8</p></td>
<td><p>7.6</p></td>
<td><p><strong>21.3</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p>7.8</p></td>
<td><p>6.2</p></td>
<td><p>7.6</p></td>
<td><p>2.2</p></td>
<td><p><strong>23.8</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p>8.5</p></td>
<td><p>6.2</p></td>
<td><p>7.6</p></td>
<td><p>3.1</p></td>
<td><p><strong>25.4</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p>8.5</p></td>
<td><p>6.2</p></td>
<td><p>7.6</p></td>
<td><p>3.5</p></td>
<td><p><strong>25.8</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p>8.5</p></td>
<td><p>6.2</p></td>
<td><p>7.6</p></td>
<td><p>4.5</p></td>
<td><p><strong>26.8</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p>8.7</p></td>
<td><p>6.2</p></td>
<td><p>7.6</p></td>
<td><p>4.7</p></td>
<td><p><strong>27.3</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>8.8</p></td>
<td><p>6.4</p></td>
<td><p>7.6</p></td>
<td><p>4.7</p></td>
<td><p><strong>27.5</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p>8.9</p></td>
<td><p>6.5</p></td>
<td><p>7.6</p></td>
<td><p>5.2</p></td>
<td><p><strong>28.2</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>9.3</p></td>
<td><p>6.5</p></td>
<td><p>7.6</p></td>
<td><p>5.2</p></td>
<td><p><strong>28.6</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p>9.3</p></td>
<td><p>6.7</p></td>
<td><p>7.6</p></td>
<td><p>5.6</p></td>
<td><p><em>' 29.2</em>'</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>9.3</p></td>
<td><p>6.7</p></td>
<td><p>7.6</p></td>
<td><p>5.6</p></td>
<td><p><em>' 29.2</em>'</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>9.3</p></td>
<td><p>6.8</p></td>
<td><p>7.6</p></td>
<td><p>5.8</p></td>
<td><p><strong>29.5</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>9.3</p></td>
<td><p>6.8</p></td>
<td><p>7.6</p></td>
<td><p>6.0</p></td>
<td><p><em>' 29.7</em>'</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011*</p></td>
<td><p>9.3</p></td>
<td><p>7.4</p></td>
<td><p>7.6</p></td>
<td><p>5.6</p></td>
<td><p><strong>29.9</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p>9.3</p></td>
<td><p>7.4</p></td>
<td><p>7.6</p></td>
<td><p>5.6</p></td>
<td><p><strong>29.9</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013#</p></td>
<td><p>9.3</p></td>
<td><p>7.6</p></td>
<td><p>7.6</p></td>
<td><p>5.8</p></td>
<td><p><strong>30.3</strong></p></td>
<td><p>1.0</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p>9.3</p></td>
<td><p>7.6</p></td>
<td><p>7.6</p></td>
<td><p>5.8</p></td>
<td><p><strong>30.3</strong></p></td>
<td><p>1.0</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p>9.3</p></td>
<td><p>7.6</p></td>
<td><p>7.6</p></td>
<td><p>5.9</p></td>
<td><p><strong>30.4</strong></p></td>
<td><p>1.0</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p>9.3</p></td>
<td><p>7.6</p></td>
<td><p>7.6</p></td>
<td><p>6.0</p></td>
<td><p><strong>30.5</strong></p></td>
<td><p>1.0</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p>9.3</p></td>
<td><p>7.9</p></td>
<td><p>7.6</p></td>
<td><p>6.0</p></td>
<td><p><strong>30.8</strong></p></td>
<td><p>1.0</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018/03</p></td>
<td><p>9.3</p></td>
<td><p>7.9</p></td>
<td><p>7.6</p></td>
<td><p>6.0</p></td>
<td><p>1.4</p></td>
<td><p>0.7</p></td>
<td><p><strong>32.9</strong></p></td>
<td><p>1.0</p></td>
</tr>
</tbody>
</table>

**\***
註：2011年協調各區域的劃分，[氹仔](../Page/氹仔.md "wikilink")、[路環與](../Page/路環.md "wikilink")[路氹填海區的面積作了適度調整](../Page/路氹城.md "wikilink")

**\#** 註：[澳門大學橫琴新校區並非通過填海而新增之土地](../Page/澳門大學新校區.md "wikilink")

## 歷史

[ObrasdosPortos.JPG](https://zh.wikipedia.org/wiki/File:ObrasdosPortos.JPG "fig:ObrasdosPortos.JPG")，位於澳門半島東端的填海標誌\]\]
澳門沿海廣泛分佈淺灘，這成為澳門進行填海造地的土地資源。澳門半島最早於1863年，填平了南灣[澳督府對面之](../Page/澳督府.md "wikilink")[海灣](../Page/海灣.md "wikilink")。其後分別在1866-1910年澳門半島西岸的北灣和淺灣、1919-1924年在澳門半島西岸的內港、1923-1938年澳門半島東岸的[新口岸和](../Page/新口岸.md "wikilink")[南灣相繼進行填海工程](../Page/南灣_\(澳門\).md "wikilink")。這些填海造地工程陸續形成了現時的[靑洲](../Page/青洲_\(澳門\).md "wikilink")、[台山](../Page/台山_\(澳門\).md "wikilink")、[黑沙環](../Page/黑沙環.md "wikilink")、[祐漢](../Page/祐漢.md "wikilink")、[筷子基](../Page/筷子基.md "wikilink")、[新口岸和南灣等地區的基本形貌](../Page/新口岸.md "wikilink")，同時因開鑿而改變了[塔石山和](../Page/塔石山.md "wikilink")[望廈山的地貌](../Page/望廈山.md "wikilink")。

離島方面，原[氹仔由大氹](../Page/氹仔.md "wikilink")、小氹、一粒米3個小島組成。1919年大、小氹已填成一個島。而[路環的填海工程進行於](../Page/路環.md "wikilink")[1930年代至](../Page/1930年代.md "wikilink")[1970年代](../Page/1970年代.md "wikilink")，主要在[聯生貨櫃碼頭和](../Page/聯生貨櫃碼頭.md "wikilink")[九澳水泥廠](../Page/九澳水泥廠.md "wikilink")。

1980年代後期起，澳葡政府開展了黑沙環、新口岸和南灣、氹仔地區的填海工程。其中南灣填海工程於1992年7月8日正式動工，其總面積達175公頃，包括兩個人工湖和填海陸地約1.9平方公里。同期的黑沙環新區填海工程、林茂塘填海工程、新口岸填海工程、[澳門國際機場填海工程及氹仔北安填海工程亦陸續完成](../Page/澳門國際機場.md "wikilink")。現氹仔的馬場和澳門國際機場地區，亦是通過在氹仔削山填海而來。至於路環，土地總面積達三十三萬平方公尺的[聯生工業村](../Page/聯生工業村.md "wikilink")，同樣是來自填海造地得來。

## 現況

### 路氹城填海計劃

[Cotai2007.JPG](https://zh.wikipedia.org/wiki/File:Cotai2007.JPG "fig:Cotai2007.JPG")

位於氹仔與路環之間，原本為十字門東側海面。早在1920年代，便有一大型塡海計劃將氹仔與路環之間的大片海域填海成為陸地，形成現稱的「[路氹城](../Page/路氹城.md "wikilink")」。路氹城的出現，把兩個原本獨立的氹仔和路環兩島連成一起。

1960年代[路氹連貫公路落成後](../Page/路氹連貫公路.md "wikilink")，該處已半天然半人工地形成土地。公路西側因水流緩慢，形成大遍淺灘，曾出現[紅樹林](../Page/紅樹林.md "wikilink")，近路環一側後來更成為[垃圾堆填區](../Page/垃圾堆填區.md "wikilink")。公路東側亦形成泥灘，並發展了養蠔業。1990年代澳門政府計劃在該水域填海興建[衛星城市](../Page/衛星城市.md "wikilink")，唯90年代末經濟不景，計劃被擱置，填海工程亦始終未有大規模地進行，只有當設施落實興建時，才進行該部份的填海，故淺灘紅樹林亦得以被保存。至[澳門回歸時](../Page/澳門回歸.md "wikilink")，落成的設施只有[蓮花大橋](../Page/蓮花大橋.md "wikilink")、[路氹城邊檢站](../Page/路氹邊檢大樓.md "wikilink")、[路環小型賽車場及](../Page/路環小型賽車場.md "wikilink")[皇庭海景酒店](../Page/皇庭海景酒店.md "wikilink")。

[澳門博彩業開放後](../Page/澳門博彩業.md "wikilink")，路氹城被重新定位發展旅遊博彩業，多間博彩企業在路氹城投資興建旗艦酒店，另一方面，政府為配合[2005年東亞運動會舉行](../Page/2005年東亞運動會.md "wikilink")，在路氹城興建多個體育場館，包括[澳門東亞運動會體育館](../Page/澳門東亞運動會體育館.md "wikilink")、[澳門國際射擊中心等](../Page/澳門國際射擊中心.md "wikilink")，路氹城兩側的填海(或土地平整)工程正式展開，[路氹濕地亦正式消失](../Page/路氹濕地.md "wikilink")。

2007年，政府在路氹城東面[機場大馬路旁增設](../Page/機場大馬路.md "wikilink")[建築物料堆填區](../Page/建築物料堆填區.md "wikilink")，由於沒有徹底清理海底淤泥，機場與堆填區之間的淤泥，向跑道南聯絡橋滑移，使機場淺灘擴大，引來鳥類覓食，危及飛機安全\[2\]。

### 澳氹新城區填海計劃

[Land_reclamation_of_Macau.JPG](https://zh.wikipedia.org/wiki/File:Land_reclamation_of_Macau.JPG "fig:Land_reclamation_of_Macau.JPG")一帶土地\]\]

澳門政府在2006年[施政報告中提出設立新城區的計劃](../Page/施政報告.md "wikilink")，前[運輸工務司司長](../Page/運輸工務司.md "wikilink")[歐文龍於](../Page/歐文龍.md "wikilink")2006年1月11日，介紹「澳氹新城區規劃」的初步方案，分別在澳門半島南面及東北面，以及氹仔北面共5個區域，發新城區。計劃涉及面積730公頃土地，當中398公頃需要填海，預計可容納11至12萬人口，供澳門未來二十年發展。\[3\]

政府為配合酒店及娛樂設施發展，在未得到中央政府批准下，已率先落實澳門半島南部的填海工程，準備擴闊[孫逸仙大馬路](../Page/孫逸仙大馬路.md "wikilink")，以及作計劃中的[澳氹海底隧道的出入口](../Page/澳氹海底隧道.md "wikilink")，唯該地段至今仍荒置。

2008年4月23日，運輸工務司司長[劉仕堯在立法會表示](../Page/劉仕堯.md "wikilink")，特區政府按照中央政府有關部門的技術意見後，於「澳氹新城區填海規劃」原有基礎上微調，填海面積將較2006年規劃“略大”，用海面積約為500公頃，實際造地面積約有429公頃。

2009年4月21日，劉仕堯在立法會表示，“微調”後的「澳氹新城區填海規劃」的方案，用海面積由原來約500公頃再度調整為約400公頃，用地面積亦由約398公頃調整為350公頃，並在新取得的土地上不發展博彩業及低密度住宅項目(如別墅)\[4\]。但整項計劃經3年多時間，到目前仍在等待中央審批階段。

11月29日，[行政長官辦公室宣佈](../Page/澳門特別行政區行政長官辦公室.md "wikilink")，[國務院日前批准了澳門填海造地](../Page/中華人民共和國國務院.md "wikilink")，填海區共分為五塊，分別位於澳門半島東、南，以及氹仔的北面，總面積約為350公頃，以興建新城區，並要求特區「在填海過程進行跟進管理，加強保護周邊環境，充份發揮新城區的建設作用。」\[5\]

澳門政府隨即就新城填海區的總體規劃開展相關工作，以及相關的公眾諮詢:
於2009年12月至2010年11月進行第一階段規劃概念研究，提出新城發展定位和概念性用地規劃方向。

於2010年12月至2011年12月進行第二階段規劃草案研究，由中國城市規劃學會學等團隊開始對新城功能結構、土地使用、公共服務和重大基建提出多方案比選，更於2011年第四季在澳門科學館進行第二階段公眾諮詢。

於2011年12月至今，則正在進行第三階段規劃方案研究，以草案階段專家、公眾意見為基礎，提出規劃方案，並預計於2012年第四季進行第三階段公眾諮詢\[6\]。

## 爭議

澳門特區政府的土地開發紀錄，自從2005年成為了當地熱門的政治話題\[7\]。

長期的填海使得澳門的天然[海岸線逐步消失](../Page/海岸線.md "wikilink")。

## 参考文献

### 引用

### 来源

  - 期刊文章

<!-- end list -->

  - 薛白、龔唯平：[澳門填海造地的供求理論探析](http://www.umac.mo/cms/cms_journal_MS_2007-c.html)\[J\]，《澳門研究》2007(42)

<!-- end list -->

  - 网页

<!-- end list -->

  - [澳門半島出土最古老的文化：澳門半島的早行者](http://www.cuhk.edu.hk/ics/ccaa/linkfile/oldmacau_txt.pdf)
  - [澳門填海造地成效大](https://web.archive.org/web/20070523073742/http://www.macaudata.com/macauweb/book246/html/01401.htm)
  - [澳门风物志：填海造地历史百年](https://web.archive.org/web/20070523073010/http://www.macaudata.com/macauweb/book141/html/00401.htm)

## 參見

  - [香港填海工程](../Page/香港填海工程.md "wikilink")

[填海造地](../Category/澳門歷史.md "wikilink")
[填海造地](../Category/澳門地理.md "wikilink")
[Category:填海](../Category/填海.md "wikilink")

1.  [1](http://www.dscc.gov.mo/CHT/knowledge/geo_statistic.html)
2.  [人民網，2009年2月2日](http://unn.people.com.cn/BIG5/8735551.html)
3.  澳門日報，2007年1月12日。
4.  澳門日報，2009年4月22日。
5.
6.  新城區總體規劃第二階段公眾諮詢文本。
7.  [新澳門學社](../Page/新澳門學社.md "wikilink")，《[新澳門](../Page/新澳門.md "wikilink")》第32期，2006年7月。