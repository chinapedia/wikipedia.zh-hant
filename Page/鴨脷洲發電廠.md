**鴨脷洲發電廠**是[香港昔日一座](../Page/香港.md "wikilink")[發電廠](../Page/發電廠.md "wikilink")，位於[香港島區的](../Page/香港島.md "wikilink")[鴨脷洲之西](../Page/鴨脷洲.md "wikilink")。發電廠於1968年開始初步投產，1971年發生[五級大火](../Page/鴨脷洲發電廠五級火.md "wikilink")，損失約千萬港元\[1\]。1978年正式取代[北角發電廠](../Page/北角發電廠.md "wikilink")，1981年全部落成並全面投產。發電廠採用燃[油發電機組](../Page/石油.md "wikilink")，總發電量大約為1,061[兆瓦](../Page/兆瓦.md "wikilink")。

由於鴨脷洲發電廠接近已發展區，如鄰近的[鴨脷洲邨](../Page/鴨脷洲邨.md "wikilink")，加上[和記黃埔](../Page/和記黃埔.md "wikilink")（改組後現時[長江和記實業有限公司](../Page/長江和記實業有限公司.md "wikilink")）收購[香港電燈後](../Page/香港電燈.md "wikilink")，有意發展發電廠地皮為大型私人[住宅項目](../Page/住宅.md "wikilink")，故香港電燈開始積極研究將全數搬往南丫島。1989年12月31日，鴨脷洲發電廠正式關閉，香港島、鴨脷洲及[南丫島的電力供應](../Page/南丫島.md "wikilink")，由1990年起全數改由1984年落成的[南丫發電廠負責](../Page/南丫發電廠.md "wikilink")。鴨脷洲發電廠原址則連同附近的[蜆殼鴨脷洲油庫](../Page/蜆殼.md "wikilink")，發展為現在的大型私人屋苑[海怡半島](../Page/海怡半島.md "wikilink")。

## 參考資料

## 外部連結

  - [港燈電力投資有限公司歷史](http://www.hkelectric.com/web/AboutUs/Generation/Index_zh.htm)
  - [鴨脷洲發電廠舊照](http://farm2.static.flickr.com/1225/1397840804_8c16f3f4cd_b.jpg)

[Category:香港已不存在的建築物](../Category/香港已不存在的建築物.md "wikilink")
[Category:香港發電廠](../Category/香港發電廠.md "wikilink")
[Category:鴨脷洲](../Category/鴨脷洲.md "wikilink")
[Category:1968年香港建立](../Category/1968年香港建立.md "wikilink")

1.  <http://i43.tinypic.com/21d3g20.jpg>