《**DNA²**》是[日本漫畫家](../Page/日本漫畫家.md "wikilink")[桂正和創作的](../Page/桂正和.md "wikilink")[科幻](../Page/科幻.md "wikilink")[漫畫以及改編的](../Page/漫畫.md "wikilink")[動畫作品](../Page/動畫.md "wikilink")，1993年至1994年連載於《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》，[單行本全](../Page/單行本.md "wikilink")5卷。

## 概要

在[集英社](../Page/集英社.md "wikilink")《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》上從1993年36・37合併號連載至1994年29號，[台灣中文版由](../Page/台灣.md "wikilink")[東立出版社發行](../Page/東立出版社.md "wikilink")，[香港中文版由](../Page/香港.md "wikilink")[文化傳信發行](../Page/文化傳信.md "wikilink")。

作品本身雖短，但有[電視動畫與](../Page/電視動畫.md "wikilink")[OVA](../Page/OVA.md "wikilink")，也曾經被考慮過拍成[電影](../Page/電影.md "wikilink")。電視動畫版在台灣是由[首華卡通頻道首播](../Page/首華卡通頻道.md "wikilink")，中文片名是《再造基因》，但是是未經授權的[B拷版](../Page/B拷.md "wikilink")，故每集片頭與片尾的「製作協力：[Bandai
Visual](../Page/Bandai_Visual.md "wikilink")、[Movic](../Page/Movic.md "wikilink")」與「製作：[BaoHouse](../Page/MADHOUSE.md "wikilink")」均被覆蓋。

原作和動畫的故事順序與結局並不同。

## 故事大綱

63年後的未來世界，由於[人口過多](../Page/人口過多.md "wikilink")，[法律規定生](../Page/法律.md "wikilink")2個以上的小孩者處[死刑](../Page/死刑.md "wikilink")。在這樣的時代裡，被稱為「超級花花公子」（）的男人擁有100個小孩，而且其[DNA被繼承下來](../Page/DNA.md "wikilink")。未來世界[政府知道超級花花公子的存在時](../Page/政府.md "wikilink")，他已經死了。

未來世界政府的環境廳及時間管理局委託「DNA操作者」**葵華林**（此為台灣中文版譯名，香港中文版譯為**葵加玲**）帶一支手槍來到現代，企圖改變這個男人的DNA，報酬為100億元；然而這個男人**桃生純太**，是個不受女性青睞、不可靠、離超級花花公子十萬八千-{里}-的人。華林的直屬長官**橫森**，以未來世界的[通訊技術指示華林](../Page/通訊技術.md "wikilink")。華林本來應該是依照預定計畫達成任務，卻弄錯DCM（改變DNA的藥，全名「DNA
Control
Medicine」）子彈射向純太，使得純太體內產生[超能力基因](../Page/超能力.md "wikilink")，純太開始出現覺醒為超級花花公子的徵兆。

華林最初認為，她誤拿自己偷偷製造的「和我相配的DCM」子彈，使純太體內產生超能力基因。環境廳的**森**是超能力者，他在未來世界謀害橫森，又來到現代謀害華林。森向華林證實，他把正確的DCM子彈換成賦予超能力的DCM子彈，意圖使純太覺醒為超級花花公子之後的100個小孩都變成「精神戰士」（），讓精神戰士大興戰亂以快速減少人口。純太的女兒**川崎嚕啦啦**跟著森從未來世界來到現代幫助森，一來誘使純太的超能力基因徹底取代純太，二來謀害華林。純太以自己的力量成功永久封印超能力基因，嚕啦啦隨之自然消亡。華林被森逼得走投無路時，橫森跟著未來世界政府的[警視廳來到現代圍捕森](../Page/警視廳.md "wikilink")；森拒捕，以華林的手槍朝自己的[太陽穴開槍](../Page/太陽穴.md "wikilink")，當場死亡（手槍為嚕啦啦完全消失前使用超能力偷來，原本期待森能替她的消失報仇）。華林向橫森表示，無意繼續接受未來世界政府委託擔任DNA操作者。華林消除了純太有關她的記憶，與橫森返回未來世界。

## 登場人物

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>桃生純太：<a href="../Page/難波圭一.md" title="wikilink">難波圭一</a></li>
<li>葵華林（葵加玲）：<a href="../Page/富永美衣奈.md" title="wikilink">富永美衣奈</a></li>
<li>佐伯倫子：<a href="../Page/林原惠.md" title="wikilink">林原惠</a></li>
<li>栗本亞美：<a href="../Page/笠原弘子.md" title="wikilink">笠原弘子</a></li>
<li>高梨琴美：<a href="../Page/椎名碧流.md" title="wikilink">椎名碧流</a></li>
<li>菅下龍二：<a href="../Page/子安武人.md" title="wikilink">子安武人</a></li>
<li>染屋垣麿：<a href="../Page/岩田光央.md" title="wikilink">岩田光央</a></li>
</ul></td>
<td><ul>
<li>市川一期：<a href="../Page/菊池英博.md" title="wikilink">菊池英博</a></li>
<li>栗本三郎：<a href="../Page/峰惠研.md" title="wikilink">峰惠研</a></li>
<li>桃生千代：<a href="../Page/丸山裕子.md" title="wikilink">丸山裕子</a></li>
<li>時光機電腦：<a href="../Page/山田榮子.md" title="wikilink">山田榮子</a></li>
<li>橫森：<a href="../Page/大林隆介.md" title="wikilink">大林隆之介</a></li>
<li>森：<a href="../Page/筈見純.md" title="wikilink">筈見純</a></li>
<li>川崎嚕啦啦：<a href="../Page/玉川紗己子.md" title="wikilink">玉川紗己子</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 單行本

  - 日文版

<!-- end list -->

1.  集英社1993年12月2日刊行 ISBN 4-08-871756-2
2.  集英社1994年4月4日刊行 ISBN 4-08-871757-0
3.  集英社1994年7月4日刊行 ISBN 4-08-871758-9
4.  集英社1994年10月4日刊行 ISBN 4-08-871759-7
5.  集英社1995年2月3日刊行 ISBN 4-08-871760-0

<!-- end list -->

  - 台灣中文版

<!-- end list -->

1.  東立出版社1994年1月30日第1刷發行 ISBN 957-34-1304-3
2.  東立出版社1994年6月20日第1刷發行 ISBN 957-34-1887-8
3.  東立出版社1994年10月20日第1刷發行 ISBN 957-34-1888-6
4.  東立出版社1995年2月20日第1刷發行 ISBN 957-34-1889-4
5.  東立出版社1995年5月20日第1刷發行 ISBN 957-34-1890-8

## 動畫

動畫由[MADHOUSE和](../Page/MADHOUSE.md "wikilink")[STUDIO
DEEN製作](../Page/STUDIO_DEEN.md "wikilink")。1994年10月7日至12月23日由[日本電視台](../Page/日本電視台.md "wikilink")[聯播網播出全](../Page/聯播網.md "wikilink")12集的電視動畫，並與其姐妹台[山形放送](../Page/山形放送.md "wikilink")、[TV新潟放送網及](../Page/TV新潟放送網.md "wikilink")[金澤電視台於毎週五](../Page/金澤電視台.md "wikilink")17:30‐18:00在關東地區播放。

1995年由[萬代影視發行全](../Page/萬代影視.md "wikilink")3集的OVA，內容為動畫版追加的原創故事。VHS・LD全5巻。而桃生純太在動畫版的設定是沒有超能力的。

本作品自從1995年發售OVA以來沒有被製作成DVD等媒介；直到2013年的動畫化20週年紀念活動上才正式宣布製作DVD與BD，2014年1月29日發行\[1\]。

### 工作人員

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>企画：<a href="../Page/武井英彥.md" title="wikilink">武井英彥</a>、<a href="../Page/落合茂一.md" title="wikilink">落合茂一</a></li>
<li>製作群：前田伸一郎、岡田洋、長-{谷}-川洋、丸山正雄</li>
<li>導演：<a href="../Page/坂田純一.md" title="wikilink">坂田純一</a></li>
<li>編劇：<a href="../Page/浦畑達彥.md" title="wikilink">浦畑達彥</a></li>
<li>系列構成：浦畑達彥</li>
<li>色彩設計：秋山久美、三笠修</li>
<li>攝影監督：-{沖}-野雅英、山口仁</li>
<li>美術監督：海野よしみ</li>
</ul></td>
<td><ul>
<li>音樂：高野ふじお</li>
<li>錄音監督：斯波重治</li>
<li>人物設定：<a href="../Page/高橋久美子.md" title="wikilink">高橋久美子</a></li>
<li>機械設定：<a href="../Page/小池健.md" title="wikilink">小池健</a></li>
<li>動畫製作：<a href="../Page/MADHOUSE.md" title="wikilink">MADHOUSE</a>、<a href="../Page/STUDIO_DEEN.md" title="wikilink">STUDIO DEEN</a></li>
<li>企画制作：<a href="../Page/日本電視台.md" title="wikilink">日本電視台</a></li>
<li>製作協力：<a href="../Page/Bandai_Visual.md" title="wikilink">Bandai Visual</a>、<a href="../Page/Movic.md" title="wikilink">Movic</a></li>
<li>製作：<a href="../Page/MADHOUSE.md" title="wikilink">BaoHouse</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 主題歌

  - 片頭曲：〈Blurry Eyes〉
    作詞：[hyde](../Page/hyde.md "wikilink")，作曲：[tetsu](../Page/tetsu.md "wikilink")，編曲、主唱：[L'Arc～en～Ciel](../Page/L'Arc～en～Ciel.md "wikilink")
  - 片尾曲：〈[Single Bed](../Page/Single_Bed.md "wikilink")〉
    作詞：[淳君](../Page/淳君.md "wikilink")，作曲：，編曲：、，主唱：射亂Q

### 副標題

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h4 id="tv版副標題">TV版副標題</h4>
<table>
<thead>
<tr class="header">
<th><p>集数</p></th>
<th><p>標題（中文翻譯）</p></th>
<th><p>標題（日文原文）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>01</p></td>
<td><p>來自未來的少女-卡琳</p></td>
<td><p>未來からの少女　かりん</p></td>
</tr>
<tr class="even">
<td><p>02</p></td>
<td><p>超級花花公子誕生！純太</p></td>
<td><p>メガプレ誕生!　純太</p></td>
</tr>
<tr class="odd">
<td><p>03</p></td>
<td><p>祭典的夜晚-亞美</p></td>
<td><p>祭りの夜に　亜美</p></td>
</tr>
<tr class="even">
<td><p>04</p></td>
<td><p>項鍊送給誰？倫子</p></td>
<td><p>ネックレスは誰に？倫子</p></td>
</tr>
<tr class="odd">
<td><p>05</p></td>
<td><p>跟誰都無法說-琴美</p></td>
<td><p>誰にも言えない!　ことみ</p></td>
</tr>
<tr class="even">
<td><p>06</p></td>
<td><p>純太對琴美做了什麼？</p></td>
<td><p>純太はことみに何をした？</p></td>
</tr>
<tr class="odd">
<td><p>07</p></td>
<td><p>我想給你我的一切</p></td>
<td><p>私のすべてをあげたいの!</p></td>
</tr>
<tr class="even">
<td><p>08</p></td>
<td><p>你一直在我身邊</p></td>
<td><p>ずっとあなたがそばにいた</p></td>
</tr>
<tr class="odd">
<td><p>09</p></td>
<td><p>這個子彈射向龍二胸口</p></td>
<td><p>その弾丸は竜二の胸に…</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>危險龍二的危險能力</p></td>
<td><p>アブナイ竜二の危ない能力</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>不能變成超級花花公子！</p></td>
<td><p>メガプレになっちゃダメ</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>拜拜，超級花花公子</p></td>
<td><p>バイバイメガプレイボーイ</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><h4 id="ova追加故事">OVA追加故事</h4>
<table>
<thead>
<tr class="header">
<th><p>集数</p></th>
<th><p>標題（中文翻譯）</p></th>
<th><p>標題（日文原文）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>01</p></td>
<td><p>另一個時光機器</p></td>
<td><p>もう一つのタイムマシン</p></td>
</tr>
<tr class="even">
<td><p>02</p></td>
<td><p>100年後的遺忘物品</p></td>
<td><p>100年後の忘れもの</p></td>
</tr>
<tr class="odd">
<td><p>03</p></td>
<td><p>我不會忘記妳</p></td>
<td><p>ぼくはあなたを忘れない</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 關連項目

  - [ZETMAN 桂正和短編集](../Page/ZETMAN_桂正和短編集.md "wikilink") - 同時期の短篇を収録。
  - 本作と同じくヒーロを扱った桂正和作品。
      - [銀翼超人](../Page/銀翼超人.md "wikilink")
      - [超機動員ヴァンダー](../Page/超機動員ヴァンダー.md "wikilink")
      - [SHADOW LADY](../Page/SHADOW_LADY.md "wikilink")
      - [ZETMAN](../Page/ZETMAN.md "wikilink")

## 參考來源

[Category:桂正和](../Category/桂正和.md "wikilink")
[Category:週刊少年Jump連載作品](../Category/週刊少年Jump連載作品.md "wikilink")
[Category:科幻漫畫](../Category/科幻漫畫.md "wikilink")
[Category:时间旅行漫画](../Category/时间旅行漫画.md "wikilink")
[Category:1994年日本電視動畫](../Category/1994年日本電視動畫.md "wikilink")
[Category:日本電視台動畫](../Category/日本電視台動畫.md "wikilink")
[Category:1995年日本OVA動畫](../Category/1995年日本OVA動畫.md "wikilink")

1.  [『D・N・A²』キャンペーン「あなたもメガプレ！？20年越しの♡告白♡」結果発表](http://www.presepe.jp/anime_news/events_campaign/events/%E3%80%8Ed%E3%83%BBn%E3%83%BBa%E3%80%8F%E3%82%AD%E3%83%A3%E3%83%B3%E3%83%9A%E3%83%BC%E3%83%B3%E3%80%8C%E3%81%82%E3%81%AA%E3%81%9F%E3%82%82%E3%83%A1%E3%82%AC%E3%83%97%E3%83%AC%EF%BC%81%EF%BC%9F2)
    ，動畫情報商店＆Community -Presepe-（2013年10月15日），2015年8月15日閱覽。