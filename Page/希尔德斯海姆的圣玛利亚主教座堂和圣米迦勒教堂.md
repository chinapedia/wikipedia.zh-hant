[HildesheimDom.jpg](https://zh.wikipedia.org/wiki/File:HildesheimDom.jpg "fig:HildesheimDom.jpg")

**希尔德斯海姆的圣玛利亚主教座堂和圣米迦勒教堂**指位於[希尔德斯海姆的](../Page/希尔德斯海姆.md "wikilink")[聖瑪利亞主教座堂](../Page/希爾德斯海姆主教座堂.md "wikilink")（）與[聖米迦勒教堂](../Page/聖米迦勒教堂_\(希爾德斯海姆\).md "wikilink")（），於1985年被[聯合國教科文組織指定為](../Page/聯合國教科文組織.md "wikilink")[世界遺產](../Page/世界遺產.md "wikilink")。

## 聖瑪利亞主教座堂

西元872年主教阿爾特夫依德（Altfrid）奠基，西元1010年到1022年由主教圣本华（Bernward von
Hildesheim）建築聖弥额尔教堂，並於1046年擴建聖瑪利亞主教座堂。至今由貝爾瓦德所建的主教座堂村（Domhof），仍可看到「本华式」規劃，清晰的村落結構。

### 二次大戰的毀壞

[二次大戰時整個主教座堂全部損毀](../Page/二次大戰.md "wikilink")，直至1950年到1960年再重建，此中去掉[巴洛克式的裝潢](../Page/巴洛克.md "wikilink")，以早期[羅馬式為重建重點](../Page/羅馬式.md "wikilink")。

## 聖米迦勒教堂

聖米迦勒教堂建於1010至20年間，遵循[對稱設計](../Page/對稱.md "wikilink")，有兩個[半圓拱後殿](../Page/半圓拱.md "wikilink")，這是的，它的[內裝](../Page/室內設計.md "wikilink")，特色是其木造[天花板與](../Page/天花板.md "wikilink")[油漆](../Page/油漆.md "wikilink")[泥灰粉刷裝飾](../Page/泥灰粉刷.md "wikilink")，還有其著名的[青銅](../Page/青銅.md "wikilink")[門片與](../Page/門.md "wikilink")青桐[柱](../Page/柱.md "wikilink")，連同聖瑪利亞主教座堂的文物，是[神聖羅馬帝國](../Page/神聖羅馬帝國.md "wikilink")[羅馬式教堂建築典範中的瑰寶](../Page/羅曼式建築.md "wikilink")\[1\]。

### 登録基準

## 參考資料

## 連結

  - [相片集](http://www.raymond-faure.com/Hildesheim/Hildesheim_Dom/hildesheim-dom.html)
  - [希爾斯海姆大教堂介紹](http://www.bistum-hildesheim.de/)
  - [大教堂博物館](http://www.dommuseum-hildesheim.de/)

[Category:德国世界遗产](../Category/德国世界遗产.md "wikilink")

1.