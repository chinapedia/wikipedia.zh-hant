**蒙拉查翁·社尼·巴莫**（；[RTGS](../Page/RTGS.md "wikilink")：Seni
Pramot；），[泰国政治家](../Page/泰国.md "wikilink")，前任[泰國首相](../Page/泰國首相.md "wikilink")。

他的父母是泰王[拉瑪二世的後裔Khamrob亲王](../Page/拉瑪二世.md "wikilink")（Prince
Khamrob）和Daeng王妃（Mom Daeng
<Bunnag>），他早年同弟弟[克立·巴莫一起留学](../Page/克立·巴莫.md "wikilink")[牛津大学伍斯特学院](../Page/牛津大学.md "wikilink")。大學畢業後，於[格雷律師學院工作](../Page/英國律師學院.md "wikilink")，返国以后，他研究泰国法律，在泰國大理院（泰國最高法院）的民事法庭實習6個月，后奉派为。

1941年，[日本入侵泰国](../Page/日本.md "wikilink")，他反对泰国政府与日本人的合作，在外国组成[自由泰人运动](../Page/自由泰人运动.md "wikilink")。1945年9月战争结束，他回到泰国獲任为首相。他的主要成就是避免泰国成为英国的保护国。

1975年和1976年，他两次出任首相，期間任反對黨領袖。1976年10月6日，国防部长[沙鄂·差罗如海军上将认为社尼政府的成员容忍共产主义](../Page/沙鄂·差罗如.md "wikilink")，发动政变，創立泰軍高级军官組成的，推翻社尼組建的短暫文人政府。

## 外部参考

  - [泰国人怎么过春节](http://www.cass.net.cn/file/2007021687843.html)

[Category:大城府人](../Category/大城府人.md "wikilink")
[Category:泰國貴族](../Category/泰國貴族.md "wikilink")
[Category:泰國華人](../Category/泰國華人.md "wikilink")
[Category:伊朗裔泰國人](../Category/伊朗裔泰國人.md "wikilink")
[Category:牛津大學伍斯特學院校友](../Category/牛津大學伍斯特學院校友.md "wikilink")
[Category:泰國律師](../Category/泰國律師.md "wikilink")
[Category:泰國內政部長](../Category/泰國內政部長.md "wikilink")
[Category:泰國駐美國大使](../Category/泰國駐美國大使.md "wikilink")
[Category:泰国总理](../Category/泰国总理.md "wikilink")
[Category:泰國第二次世界大戰人物](../Category/泰國第二次世界大戰人物.md "wikilink")