**马其顿内部革命组织－马其顿民族统一民主党**（Внатрешна македонска револуционерна
организација – Демократска партија за македонско национално
единство），简称ВМРО-ДПМНЕ（VMRO–DPMNE），是[北马其顿共和国主要政党之一](../Page/北马其顿共和国.md "wikilink")，现为执政党，约有党员10万人。

## 历史

该组织自称以1893年成立的[马其顿内部革命组织](../Page/马其顿内部革命组织.md "wikilink")（Internal
Macedonian Revolutionary
Organization）为前身，实际上于1990年6月17日在[斯科普里正式成立](../Page/斯科普里.md "wikilink")，由一批追求马其顿独立的青年[民族主义知识分子为主体组成](../Page/民族主义.md "wikilink")。在当年的马其顿议会选举中，VMRO–DPMNE成为议会第一大党，但是因为拒绝同[阿尔巴尼亚族政党合作而失去组阁权](../Page/阿尔巴尼亚族.md "wikilink")。

1995年后，该党逐渐趋向温和，在1998年的大选中，该党出人意料地同[阿尔巴尼亚族民主党联盟](../Page/阿尔巴尼亚族民主党.md "wikilink")，从而赢得大选，其党首[柳普乔·格奥尔基耶夫斯基出任总理](../Page/柳普乔·格奥尔基耶夫斯基.md "wikilink")。

### 21世纪

2002年，该党在大选中失败，随之分裂，柳普乔·格奥尔耶夫斯基另组[马其顿内部革命组织人民党](../Page/马其顿内部革命组织人民党.md "wikilink")，[尼古拉·格鲁埃夫斯基接任党首](../Page/尼古拉·格鲁埃夫斯基.md "wikilink")。

2006年6月5日国会大选中，该党获得全部120议席中的45席首次名列第一开始执政，而原来的第一大党[馬其頓社會民主聯盟沦为第二](../Page/馬其頓社會民主聯盟.md "wikilink")。

2008年6月1日国会大选中，该党获得全部120议席中的63席名列第一，再次执政。

2011年6月5日，在提前一年举行（理应于2012年选举，因最大在野党[馬其頓社會民主聯盟提议](../Page/馬其頓社會民主聯盟.md "wikilink")）的国会选举中\[1\]\[2\]\[3\]，该党获得总共123席中的56席位居第一，开始第三次连续执政。社會民主聯盟获得42席为第二大党，民主结合联盟(Democratic
Union for Integration)获得15席为第三。

在[2014年马其顿大选中](../Page/2014年马其顿大选.md "wikilink")，统一民主党以赢得42.98%的选票、61席获得胜利。
在[2016年馬其頓議會選舉中](../Page/2016年馬其頓議會選舉.md "wikilink"),再度獲勝.

## 参考资料

## 外部链接

  - [官方网站](http://www.vmro-dpmne.org.mk/)

[Category:北馬其頓政黨](../Category/北馬其頓政黨.md "wikilink")
[Category:基督教民主主義政黨](../Category/基督教民主主義政黨.md "wikilink")
[Category:1990年建立的政黨](../Category/1990年建立的政黨.md "wikilink")
[Category:保守主义政党](../Category/保守主义政党.md "wikilink")

1.  <http://www.b92.net/eng/news/region-article.php?yyyy=2011&mm=03&dd=16&nav_id=73262>
2.  <http://www.reuters.com/article/2011/03/30/macedonia-election-idUSLDE72T1RN20110330>
3.  <http://www.setimes.com/cocoon/setimes/xhtml/en_GB/newsbriefs/setimes/newsbriefs/2011/03/31/nb-08>