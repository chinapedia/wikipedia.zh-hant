**保羅·艾馬臣·卡爾高·恩斯**（，）是一名前職業足球員，司職[防守中場](../Page/中場.md "wikilink")，是前[英格蘭國家隊成員](../Page/英格蘭國家隊.md "wikilink")，曾效力數間著名球會如[韋斯咸](../Page/西汉姆联足球俱乐部.md "wikilink")、[曼聯](../Page/曼聯.md "wikilink")、[利物浦及](../Page/利物浦足球俱樂部.md "wikilink")[國際米蘭等等](../Page/國際米蘭.md "wikilink")。退役後曾擔任[馬爾斯菲](../Page/马科斯菲尔德足球俱乐部.md "wikilink")、[米尔顿凯恩斯](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")、[布力般流浪及](../Page/布莱克本流浪者足球俱乐部.md "wikilink")[黑池的領隊](../Page/黑池足球會.md "wikilink")，是[英超成立後首名](../Page/英超.md "wikilink")[英格蘭籍](../Page/英格蘭.md "wikilink")[黑人領隊](../Page/黑人.md "wikilink")\[1\]。

恩斯的兒子[湯馬士](../Page/湯馬士·恩斯.md "wikilink")（Thomas
Ince）於2008年夏季加入[利物浦青年軍](../Page/利物浦足球俱乐部.md "wikilink")\[2\]，現時效力[德比郡](../Page/德比郡足球俱乐部.md "wikilink")。

## 生平

### 球會

恩斯於19歲之年便首次代表[韋斯咸一隊上陣](../Page/韋斯咸.md "wikilink")，僅僅花了一年時間便成為球隊的重心球員，他出色的體能、準確的攔截、打不死的精神以及強勁的遠射能力令他成為一名被寄予厚望的年青球員。在「鐵鎚」待了4個年頭後，[曼聯以當時的高價](../Page/曼聯.md "wikilink")100萬鎊把他帶到[老特拉福德球場](../Page/老特拉福德球場.md "wikilink")。

來到[曼聯後恩斯很快便成為了中場線上不可或缺的戰力](../Page/曼聯.md "wikilink")，他到步後首年便協助球隊贏得[足總盃](../Page/足總盃.md "wikilink")。其後，隨著曼聯的「神奇隊長」[布萊恩·羅布森開始走下坡](../Page/布萊恩·羅布森.md "wikilink")，恩斯便慢慢成為了曼聯中場的領軍人物。往後的日子，與恩斯並肩作戰的還有一眾名將如[埃里克·坎通納](../Page/埃里克·坎通納.md "wikilink")、[舒米高](../Page/舒米高.md "wikilink")、[艾雲及](../Page/艾雲.md "wikilink")[布魯士等](../Page/布魯士.md "wikilink")，他們先後贏盡了各項錦標。可惜，於1995年恩斯因為與領隊[費格遜傳出不和而被賣到](../Page/費格遜.md "wikilink")[意大利甲組足球聯賽勁旅](../Page/意大利甲組足球聯賽.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")。

恩斯在國際米蘭過得不過不失，在兩個年頭內上陣了54場賽事，射入10球。1997年，他選擇回到[利物浦](../Page/利物浦足球俱樂部.md "wikilink")。

在[利物浦](../Page/利物浦足球俱樂部.md "wikilink")，恩斯表現不錯，他和[麥馬拿文](../Page/麥馬拿文.md "wikilink")、[科拿等年青球星非常合拍](../Page/科拿.md "wikilink")，後來他更成為了隊長。在利物浦待了2年後，他被當時的領隊[侯利亞放棄](../Page/侯利亞.md "wikilink")，被逼轉會至[米杜士堡](../Page/米杜士堡.md "wikilink")。

往後的日子，恩斯無論在[米杜士堡或](../Page/米杜士堡.md "wikilink")[狼隊](../Page/狼隊.md "wikilink")，始終未能踢回他的最佳水準，於2006年，恩斯轉戰到較低級別的聯賽。

### 國家隊

恩斯是[英格蘭國家隊歷史上首位黑人隊長](../Page/英格蘭國家隊.md "wikilink")，他曾以代隊長身份正選出戰對[美國國家隊的友賽](../Page/美國.md "wikilink")。
這名中場硬漢曾參於1996年於[英格蘭舉行的](../Page/英格蘭.md "wikilink")[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")，當時身為東道主的[英格蘭取得不俗成績](../Page/英格蘭國家隊.md "wikilink")，成功殺入4強。
其後恩斯亦有參於1998年[世界盃](../Page/世界盃.md "wikilink")，在這項賽事當中，恩斯與[史高斯及](../Page/史高斯.md "wikilink")[柏提鎮守中場中路](../Page/柏提.md "wikilink")，可惜表現未如理想，球隊最後於16強出局。
2年後恩斯再入參英格蘭大軍出戰2000年[歐洲國家盃](../Page/歐洲國家盃.md "wikilink")，雖然球隊擁有[奧雲](../Page/奧雲.md "wikilink")、恩斯、[舒寧咸](../Page/舒寧咸.md "wikilink")、[舒利亞](../Page/舒利亞.md "wikilink")、[碧咸及](../Page/碧咸.md "wikilink")[麥馬拿文等球員的星級陣容](../Page/麥馬拿文.md "wikilink")，但英格蘭的表現卻再一令人失望，更於小組賽列第三名飲恨出局。一眾球員包括恩斯在內回國後被傳媒大肆抨擊。

### 領隊

2006年8月31日因斯加盟由老友[丹尼斯·韋斯帶領的](../Page/丹尼斯·韋斯.md "wikilink")[史雲頓出任球員兼教練](../Page/斯温登足球俱乐部.md "wikilink")。同年10月23日他轉投[馬爾斯菲擔任球員兼領隊](../Page/马科斯菲尔德足球俱乐部.md "wikilink")，但由於已註冊為史雲頓的球員，需待來年冬季轉會窗重開才可為馬爾斯菲上陣。當因斯接手執教馬爾斯菲時，球隊位於[英乙榜末](../Page/英乙.md "wikilink")，距離最接近對手7分，直到第廿輪賽事才錄得首場勝仗，但因斯扭轉形勢，帶領球隊連續9場不敗，為他贏得2006年12月份「英乙每月最佳領隊」；直到2007年5月7日聯賽煞科日，因斯以後補身份為馬爾斯菲在主場1-1賽和[諾士郡上陣](../Page/諾士郡足球會.md "wikilink")5分鐘\[3\]，是他唯一一場以球員身份替馬爾斯菲上陣，亦是其[職業生涯最後一場賽事](../Page/職業.md "wikilink")，而馬爾斯菲亦以這寶貴1分獲得留級資格。

2007年6月25日因斯加盟另一支[英乙球隊](../Page/英乙.md "wikilink")[米爾頓凱恩斯擔任全職領隊](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")\[4\]，亦正式打嚮名堂，球隊於9月份進佔榜首，因斯先後獲得10月份、12月份及4月份「英乙每月最佳領隊」，更帶領球隊獲得[英格蘭聯賽錦標及](../Page/英格蘭聯賽錦標.md "wikilink")[聯賽](../Page/2007年至2008年英格蘭足球乙級聯賽.md "wikilink")[雙料冠軍](../Page/雙料冠軍.md "wikilink")，來季升班[英甲](../Page/英甲.md "wikilink")。

2008年6月22日因斯接替[馬克·曉士受聘為](../Page/馬克·曉士.md "wikilink")[英超球會](../Page/英超.md "wikilink")[布力般流浪領隊](../Page/布莱克本流浪者足球俱乐部.md "wikilink")\[5\]。12月16日上任後只錄得聯賽17戰3勝的差劣成積而被辭退\[6\]。2009年7月3日重作馮婦，返回[米尔顿凯恩斯任教](../Page/米尔顿凯恩斯足球俱乐部.md "wikilink")\[7\]，在球季結束前因斯以球隊來季縮減預算為由宣佈季後離隊\[8\]。

2010年10月28日因斯與[英甲球會](../Page/英甲.md "wikilink")[諾士郡簽署三年另八個月合約執掌帥印](../Page/諾士郡足球會.md "wikilink")，是球隊近一年來第六任領隊\[9\]。2011年4月3日在球隊五連敗後與諾士郡協議提前解約\[10\]。

## 榮譽

  - 球員

<!-- end list -->

  - **[英格蘭超級聯賽](../Page/英格蘭足球超級聯賽.md "wikilink")**
      - 冠軍（2）：1993年、1994年；
  - **[英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")**
      - 冠軍（2）：1990年、1994年；
      - 亞軍（1）：1995年；
  - **[英格蘭社區盾](../Page/英格蘭社區盾.md "wikilink")**
      - 冠軍（3）：1990年、1993年、1994年；
  - **[歐洲盃賽冠軍盃](../Page/歐洲盃賽冠軍盃.md "wikilink")**
      - 冠軍（1）：1991年；
  - **[歐洲超級盃](../Page/歐洲超級盃.md "wikilink")**
      - 冠軍（1）：1991年；
  - **[英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")**
      - 冠軍（1）：1992年；
  - **[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")**
      - 亞軍（1）：1997年；

<!-- end list -->

  - 領隊

<!-- end list -->

  - **[足總錦標](../Page/英格蘭足總錦標.md "wikilink")**
      - 冠軍（1）：2008年；
  - **[英格蘭乙級聯賽](../Page/英格蘭足球乙級聯賽.md "wikilink")**
      - 冠軍（1）：2008年；

## 註腳

## 外部連結

  -
  -
[Category:倫敦人](../Category/倫敦人.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:韋斯咸球員](../Category/韋斯咸球員.md "wikilink")
[Category:曼聯球員](../Category/曼聯球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:米杜士堡球員](../Category/米杜士堡球員.md "wikilink")
[Category:狼隊球員](../Category/狼隊球員.md "wikilink")
[Category:史雲頓球員](../Category/史雲頓球員.md "wikilink")
[Category:馬爾斯菲球員](../Category/馬爾斯菲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:英格兰足球领队](../Category/英格兰足球领队.md "wikilink")
[Category:馬爾斯菲領隊](../Category/馬爾斯菲領隊.md "wikilink")
[Category:米爾頓凱恩斯領隊](../Category/米爾頓凱恩斯領隊.md "wikilink")
[Category:布力般流浪領隊](../Category/布力般流浪領隊.md "wikilink")
[Category:諾士郡領隊](../Category/諾士郡領隊.md "wikilink")
[Category:黑池領隊](../Category/黑池領隊.md "wikilink")
[Category:英超領隊](../Category/英超領隊.md "wikilink")
[Category:英格蘭足球聯賽領隊](../Category/英格蘭足球聯賽領隊.md "wikilink")

1.
2.  [Paul Ince's son Thomas handed Liverpool
    contract](http://soccernet.espn.go.com/news/story?id=537648&cc=4716)

3.

4.

5.

6.  [Lowly Blackburn sack manager
    Ince](http://news.bbc.co.uk/sport2/hi/football/teams/b/blackburn_rovers/7784967.stm)，2008年12月16日，[BBC](../Page/BBC.md "wikilink")
    Sport，於2008年12月16日查閱

7.

8.

9.

10.