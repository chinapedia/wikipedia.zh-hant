[Taipan_Bread_&_Cakes_in_Shatin_Centre_2015.jpg](https://zh.wikipedia.org/wiki/File:Taipan_Bread_&_Cakes_in_Shatin_Centre_2015.jpg "fig:Taipan_Bread_&_Cakes_in_Shatin_Centre_2015.jpg")分店\]\]
[HK_Sheung_Wan_Shun_Tak_Centre_stall_mooncake_TaiPan_Sept-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_Sheung_Wan_Shun_Tak_Centre_stall_mooncake_TaiPan_Sept-2013.JPG "fig:HK_Sheung_Wan_Shun_Tak_Centre_stall_mooncake_TaiPan_Sept-2013.JPG")
[Taipan_Bread_&_Cakes_at_HKTDC_Food_Expo_2011.jpg](https://zh.wikipedia.org/wiki/File:Taipan_Bread_&_Cakes_at_HKTDC_Food_Expo_2011.jpg "fig:Taipan_Bread_&_Cakes_at_HKTDC_Food_Expo_2011.jpg")的[冰皮月餅展品](../Page/冰皮月餅.md "wikilink")\]\]
**大班麵包西餅**是[香港一家售賣](../Page/香港.md "wikilink")[麵包及](../Page/麵包.md "wikilink")[西餅的連鎖店](../Page/西餅.md "wikilink")，成立於1984年，目前在[香港擁有](../Page/香港.md "wikilink")30間分店，其中部分設於[港鐵站](../Page/港鐵車站列表.md "wikilink")。大班麵包西餅是[冰皮月餅的發明者](../Page/冰皮月餅.md "wikilink")。公司總部及生產基地設於[九龍](../Page/九龍.md "wikilink")[新浦崗](../Page/新浦崗.md "wikilink")。

## 歷史

有「[冰皮月餅之父](../Page/冰皮月餅.md "wikilink")」稱號的大班主席[郭鴻鈞先後在](../Page/郭鴻鈞.md "wikilink")[香港酒店](../Page/香港酒店.md "wikilink")、[嘉頓集團及不同的餐廳工作](../Page/嘉頓.md "wikilink")，從事麵包西餅工作。後來加入[超群餅店](../Page/超群餅店.md "wikilink")，得到老闆[李曾超群的賞識](../Page/李曾超群.md "wikilink")，轉做管理工作，先後引入自助式售賣麵包及預售西餅卡。郭鴻鈞與其他友人在1984年合資在[淘大開設第一間大班餅店](../Page/淘大.md "wikilink")，初期只是一間普通餅店，主要售賣麵包西餅。1989年大班首創[冰皮月餅](../Page/冰皮月餅.md "wikilink")，使傳統[月餅市場出現變化](../Page/月餅.md "wikilink")，其他餅店爭相仿效。1991年大班麵包西餅引入[特許經營計劃](../Page/特許經營.md "wikilink")，希望能加速分店的擴張。

## 產品

大班麵包西餅售賣多種[麵包](../Page/麵包.md "wikilink")，也售賣[蛋撻](../Page/蛋撻.md "wikilink")、[蛋糕和](../Page/蛋糕.md "wikilink")[西餅](../Page/西餅.md "wikilink")。除此之外，大班麵包西餅也推出各種節日產品，如[中式餅食](../Page/中式糕點.md "wikilink")、[粽及](../Page/粽.md "wikilink")[月餅等](../Page/月餅.md "wikilink")。在[葵芳港鐵站大堂開設](../Page/葵芳站.md "wikilink")[大班Express分店](../Page/大班Express.md "wikilink")，售賣[快餐食品如早上的傳統雞球大包](../Page/快餐.md "wikilink")、西式焗飯至各款下午茶套餐。

## 推廣

2003年大班贊助[香港足球隊迎戰](../Page/香港足球代表隊.md "wikilink")[皇家馬德里來港作賽](../Page/皇家馬德里.md "wikilink")，繼而2004年再次贊助紅魔鬼[曼聯到港與香港足球隊對壘](../Page/曼聯.md "wikilink")。2006年8月大班麵包西餅包起全港9000多個[咪表賣廣告](../Page/停車收費碼表.md "wikilink")，成為首個在[咪表賣廣告的商戶](../Page/停車收費碼表.md "wikilink")。

1993年榮獲香港管理專業協會頒發「傑出市場策劃獎」銀獎 2002年至2003年，連續兩年獲選「超級品牌」
2004年獲香港旅遊發展局評為「優質服務」商戶
2006年香港品牌發展局頒頒發「香港名牌」
2006年至2010年，連續五年獲選「中國最喜愛香港名牌月餅第一名」
2010年獲頒「香港名牌永久榮譽金獎」 2007年至2011年，連續五年獲頒發「中國信譽企業認證」榮譽
2000年至2011年，連續11年獲「世界精選」Monde Selection 頒發「金獎優質食品」
2008年至2011年，連續4年獲「國際風味暨品質評鑒所」iTQi頒發「頂級美味獎章

## 參考

  - [專訪冰皮月餅之父　大班郭鴻鈞](http://paper.wenweipo.com/2006/07/15/zt0607150011.htm)
  - [父掌大方向
    子負責對外](http://www.mpfinance.com/htm/Finance/20060922/News/ec_ecp2.htm)
  - [大班發揮 4P
    打月餅戰](http://sme.mpfinance.com/cfm/content2.cfm?PublishDate=20060922&TopicID=ba01&Filename=ecp1.txt)

## 外部連結

  - [大班麵包西餅](http://www.taipan.com.hk/)

[Category:1984年成立的公司](../Category/1984年成立的公司.md "wikilink")
[Category:香港餅店](../Category/香港餅店.md "wikilink")
[Category:香港名牌](../Category/香港名牌.md "wikilink")