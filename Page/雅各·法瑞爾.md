**雅各·高頓·法瑞爾**（**James Gordon
Farrell**，）是個[愛爾蘭](../Page/愛爾蘭.md "wikilink")[歷史](../Page/歷史.md "wikilink")[小説的](../Page/小説.md "wikilink")[著作家](../Page/著作家.md "wikilink")。

## 生平

生於[都柏林的](../Page/都柏林.md "wikilink")[利物浦](../Page/利物浦.md "wikilink")，但父母皆是[英國人](../Page/英國人.md "wikilink")。父親在利物浦擔任會計師的經理，經常到遠東地區和[印度](../Page/印度.md "wikilink")。母親曾在愛爾蘭受教育，並且帶同法瑞爾全家在1945年搬移到愛爾蘭。

1950年代，法瑞爾去[加拿大旅行七個月](../Page/加拿大.md "wikilink")，而實行職業。返回加拿大後，他開始進去[布拉斯諾茲學院和](../Page/布拉斯諾茲學院.md "wikilink")[牛津大學研究](../Page/牛津大學.md "wikilink")[法律學](../Page/法律學.md "wikilink")。他在大學的期間間，並十分熱中於運動。

1956年9月28日，法瑞爾在玩[橄欖球時](../Page/橄欖球.md "wikilink")，似乎受到傷害，但幾天後很明顯的出現重病症狀，被送往醫院，經過診斷之後發現他患[小兒麻痹症](../Page/小兒麻痹症.md "wikilink")，當時因不確定是否能活下來的狀況下，他接受了物理治療，試圖減少損失。但一直無法恢復他體力或兵力，這疾病影響他一輩子。

他的病被醫治後，在1956年秋季時，法瑞爾又回去[布拉斯諾茲學院入學](../Page/布拉斯諾茲學院.md "wikilink")，研讀[法語和](../Page/法語.md "wikilink")[西班牙語](../Page/西班牙語.md "wikilink")。但因他的殘廢，他只能慢慢地學習，研考對他來說變很困難。到了1960年，他終於畢業，不久他就搬移到[法國擔任教師](../Page/法國.md "wikilink")。其受到小兒麻痹症之經驗影響，法瑞爾開始寫痛苦及一連串暗淡的小說。

他最著名的作品是他的《帝國系列》：《[患難](../Page/患難_\(小說\).md "wikilink")》、《[奎許納埠之圍](../Page/奎許納埠之圍.md "wikilink")》（*[The
Siege of
Krishnapur](../Page/:en:The_Siege_of_Krishnapur.md "wikilink")*）與《[新加坡控制](../Page/新加坡控制.md "wikilink")》（*The
Singapore
Grip*），所有這些作品都是攻擊和對付英國殖民統治。在1973年時，《奎許納埠之圍》榮獲了[布克獎](../Page/布克獎.md "wikilink")。1979年，法瑞爾在出海釣魚時因遭逢大浪而溺斃，在這場風暴中也造成十七人死亡。《患難》在1988年被改編成同名電影，由[克里斯多福·摩拉罕執導](../Page/克里斯多福·摩拉罕.md "wikilink")。

## 主要作品

  - *A Man From Elsewhere* (1963年)
  - *The Lung* (1965年)
  - *A Girl in the Head* (1967年)

**帝國系列**：

  - 《[患難](../Page/患難_\(小說\).md "wikilink")》/*Troubles* 1970年)
  - [奎許納埠之圍](../Page/奎許納埠之圍.md "wikilink")/*The Siege of Krishnapur*
    (1973年)
  - [新加坡控制](../Page/新加坡控制.md "wikilink")/*The Singapore Grip* (1978年)
  - *The Hill Station* (1981年)

## 獎項

  - 1973年[费伯纪念奖](../Page/费伯纪念奖.md "wikilink")－《患难》。
  - 1973年[布克奖](../Page/布克奖.md "wikilink")－《奎許納埠之圍》。

## 外部链接

  - [雅各·法瑞尔](http://www.litencyc.com/php/speople.php?rec=true&UID=1486)
  - [法瑞尔介绍](https://web.archive.org/web/20070728103101/http://text.bnc.ox.ac.uk/history/alu/page4.html)

[Category:愛爾蘭作家](../Category/愛爾蘭作家.md "wikilink")
[Category:當代文學作家](../Category/當代文學作家.md "wikilink")