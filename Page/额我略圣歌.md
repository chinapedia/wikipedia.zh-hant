[Graduale_Aboense_2.jpg](https://zh.wikipedia.org/wiki/File:Graduale_Aboense_2.jpg "fig:Graduale_Aboense_2.jpg")的祭文
*Gaudeamus omnes*，以[四线谱记谱法记录](../Page/四线谱.md "wikilink")。\]\]

**額我略聖歌**（）是[西方基督教](../Page/西方基督教.md "wikilink")[单声圣歌的主要传统](../Page/单声圣歌.md "wikilink")，是一种[单声部](../Page/单声部.md "wikilink")、无伴奏的[天主教會](../Page/天主教會.md "wikilink")[宗教音乐](../Page/宗教音乐.md "wikilink")。額我略聖歌主要是在第8世纪和第9世纪，[法兰克人到达西欧和中欧期间发展起来](../Page/法兰克人.md "wikilink")，后来继续有所增加和编写。通常人们认为是[教宗額我略一世发明了额我略圣咏](../Page/教宗額我略一世.md "wikilink")，但学者们认为是在后来的[加洛林王朝时期](../Page/加洛林王朝.md "wikilink")，综合了罗马圣咏和[高卢圣咏而形成](../Page/高卢圣咏.md "wikilink")。

额我略圣咏共有8种[调式](../Page/调式.md "wikilink")。传统上，额我略圣咏由男人或男孩组成的唱诗班在教堂中演唱。

## 历史

### 早期单声圣歌的发展

在[基督教会最初的时期](../Page/早期基督教.md "wikilink")，无伴奏的歌唱就已经是[基督教礼拜仪式的一部分](../Page/基督教礼拜仪式.md "wikilink")。直到1990年代中期，古犹太[圣诗对早期基督教仪式和圣诗的影响得到广泛公认](../Page/圣诗.md "wikilink")。但这一观点已不被学者接受，因为分析显示大部分早期基督教圣诗并没有歌本，而这些圣诗在公元70年[第二圣殿](../Page/第二圣殿.md "wikilink")[被毁后](../Page/耶路撒冷围城_\(70年\).md "wikilink")，已经有数百年未在[犹太会堂中演唱了](../Page/犹太会堂.md "wikilink")\[1\]。不过，早期基督教仪式却是融合了犹太仪式的元素，保存在后来的圣咏传统中。[祈祷时刻来源于犹太的祷告时间](../Page/祈祷时刻.md "wikilink")，“[阿们](../Page/阿们.md "wikilink")”和“[哈利路亚](../Page/哈利路亚.md "wikilink")”都来自于[希伯来语](../Page/希伯来语.md "wikilink")\[2\]。

《[新约圣经](../Page/新约圣经.md "wikilink")》中提到，在[最后晚餐中唱了圣诗](../Page/最后晚餐.md "wikilink")：“他们唱了诗，就出来往[橄榄山去](../Page/橄榄山.md "wikilink")。”\[3\]。

[Gregory_I_-_Antiphonary_of_Hartker_of_Sankt_Gallen.jpg](https://zh.wikipedia.org/wiki/File:Gregory_I_-_Antiphonary_of_Hartker_of_Sankt_Gallen.jpg "fig:Gregory_I_-_Antiphonary_of_Hartker_of_Sankt_Gallen.jpg")化为鸽子降临教宗[額我略一世](../Page/額我略一世.md "wikilink")，口授额我略圣咏。\]\]

## 注释

## 参考文献

### 引用

### 来源

  - *Graduale triplex* (1979). Tournai: Desclée& Socii. ISBN
    978-2-85274-094-5

  - *Liber usualis* (1953). Tournai: Desclée& Socii.

  -
  -
  - Hiley, David (1990). Chant. In *Performance Practice: Music before
    1600*, Howard Mayer Brown and Stanley Sadie, eds., pp. 37-54. New
    York: W.W. Norton & Co. ISBN 978-0-393-02807-2

  -
  -
  -
  -
  - Mahrt, William P. (2000). Chant. In *A Performer's Guide to Medieval
    Music*, Ross Duffin, ed., pp. 1-22. Bloomington, IN: Indiana
    University Press. ISBN 978-0-253-33752-8

  -
  -
  -
  -
  - Wagner, Peter. (1911) *Einführung in die Gregorianischen Melodien.
    Ein Handbuch der Choralwissenschaft*. Leipzig: Breitkopf & Härtel.

  -
## 外部链接

  - Geoffrey Chew and Richard Rastall: "Notation", Grove Music Online,
    ed. L. Macy (Accessed 27 June 2006), [(subscription
    access)](http://www.grovemusic.com)

  - sites of abbeys and schola according to the method of Dom Gajard.
    Gregedit Gregorian
    Software[1](http://gregorian.soft.free.fr/sites.html)

  - H. Bewerung: "Gregorian chant", Catholic Encyclopedia,
    [2](http://www.newadvent.org/cathen/06779a.htm)

  - Joseph Dyer: "Roman Catholic Church Music", Section VI.1, Grove
    Music Online ed. L. Macy (Accessed 28 June 2006), [(subscription
    access)](http://www.grovemusic.com)

  - David Hiley and Janka Szendrei: "Notation", Grove Music Online, ed.
    L. Macy (Accessed 12 June 2006), [(subscription
    access)](http://www.grovemusic.com)

  - Kenneth Levy: "Plainchant", Grove Music Online, ed. L. Macy
    (Accessed 20 January 2006), [(subscription
    access)](http://www.grovemusic.com)

  - William P. Mahrt: "Gregorian Chant as a Paradigm of Sacred Music,"
    Sacred Music, 133.3, pp. 5-14 [online at
    MusicaSacra.com](http://www.musicasacra.com/publications/sacredmusic/133/1/1_1.html)

  - James W. McKinnon: "Christian Church, music of the early", Grove
    Music Online ed. L. Macy (Accessed 11 July 2006), [(subscription
    access)](http://www.grovemusic.com)

  - Canticum Novum, Lessons on Gregorian Chant: Notation,
    characteristics, rhythm, modes, the psalmody and scores at
    <https://web.archive.org/web/20060616141014/http://interletras.com/canticum/Eng/index1_Eng.html>

  - Justine Ward, "The Reform of Church Music," Atlantic Monthly, April
    1906 [online at
    Musicasacra.com](http://www.musicasacra.com/publications/sacredmusic/pdf/ward.pdf)

  - Liber usualis online - MIDI Collection of Traditional Catholic Hymns
    and chants <http://romaaeterna.jp//>

  -
## 参见

  - [教宗額我略一世](../Page/教宗額我略一世.md "wikilink")
  - [圣咏](../Page/圣咏.md "wikilink")、[圣诗](../Page/圣诗.md "wikilink")
  - [基督教音乐](../Page/基督教音乐.md "wikilink")
  - [纽姆记谱法](../Page/纽姆记谱法.md "wikilink")

{{-}}

[Category:基督教音乐体裁](../Category/基督教音乐体裁.md "wikilink")
[Category:天主教音樂](../Category/天主教音樂.md "wikilink")
[Category:声乐](../Category/声乐.md "wikilink")
[Category:弥撒曲](../Category/弥撒曲.md "wikilink")

1.  David Hiley, *Western Plainchant* pp. 484-5.
2.  Willi Apel, *额我略圣咏* p. 34.
3.