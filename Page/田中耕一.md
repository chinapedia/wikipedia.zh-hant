**田中耕一**（，），是一名[日本](../Page/日本.md "wikilink")[化學](../Page/化學.md "wikilink")、[工程學家](../Page/工程學.md "wikilink")，任職於[京都](../Page/京都.md "wikilink")。[日本學士院會員](../Page/日本學士院會員.md "wikilink")。[文化勋章表彰](../Page/文化勋章.md "wikilink")。[文化功勞者](../Page/文化功勞者.md "wikilink")。

2002年，田中因為「開發生物巨分子的[同定與構造解析手法](../Page/基质辅助激光解吸/电离.md "wikilink")\[1\]」，與[库尔特·维特里希及](../Page/库尔特·维特里希.md "wikilink")[约翰·贝内特·芬恩共同獲得](../Page/约翰·贝内特·芬恩.md "wikilink")[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")\[2\]\[3\]\[4\]。他獲獎時僅有[學士](../Page/學士.md "wikilink")[學位](../Page/學位.md "wikilink")，為歷來[諾貝爾化學獎得主中僅見的特例](../Page/諾貝爾化學獎得主列表.md "wikilink")。

## 早年生活與教育

田中耕一於1959年8月3日出生於[日本](../Page/日本.md "wikilink")[富山市](../Page/富山市.md "wikilink")，因為生母26天後不幸過世，後來他由叔父夫婦領養，田中直到大學一年級才獲知此事。就讀[東北大學工學院電氣工程學科二年級時](../Page/东北大学_\(日本\).md "wikilink")，田中因必修的[德文不及格而留級一年](../Page/德文.md "wikilink")。

1983年4月1日進入就職，1992年外派到島津製作所[英國分公司擔任研究員一年](../Page/英國.md "wikilink")，1995年5月與[相親認識的裕子結為連理](../Page/相親.md "wikilink")，1997年再次外派到島津製作所[英國分公司](../Page/英國.md "wikilink")，擔任研究員五年之久。

## 軟雷射脫附法

[Masatoshi_Koshiba_Junichiro_Koizumi_and_Koichi_Tanaka_20021011.jpg](https://zh.wikipedia.org/wiki/File:Masatoshi_Koshiba_Junichiro_Koizumi_and_Koichi_Tanaka_20021011.jpg "fig:Masatoshi_Koshiba_Junichiro_Koizumi_and_Koichi_Tanaka_20021011.jpg")[小泉純一郎](../Page/小泉純一郎.md "wikilink")（中央）在首相官邸接見田中耕一（右）與[小柴昌俊](../Page/小柴昌俊.md "wikilink")（左）\]\]
以往對[大分子諸如](../Page/大分子.md "wikilink")[蛋白質進行質譜分析時](../Page/蛋白質.md "wikilink")，分析物必須通過雷射照射以[電離和](../Page/電離.md "wikilink")[汽化](../Page/汽化.md "wikilink")。但大分子受到強烈雷射脈衝的直接照射後，會導致分析物裂解成微小的碎片，出現結構損失。1985年2月，田中發現過使用[甘油中的超細金屬粉末作為基質的混合物](../Page/甘油.md "wikilink")，就可以將分析物電離而不失去其結構。他的工作在1985年被提交專利申請，專利內容於1987年5月在[京都召開的日本質譜學會年會上公佈](../Page/京都.md "wikilink")，被稱為[軟雷射脫附法](../Page/基質輔助雷射解吸/電離.md "wikilink")（Soft
Laser Desorption，簡稱SLD）\[5\]。

田中因此獲得2002年[諾貝爾化學獎後](../Page/諾貝爾化學獎.md "wikilink")，一些科學家認為[德國的](../Page/德國.md "wikilink")、不應被排除在外\[6\]\[7\]，這是因為他們在1985年首次報導了一種使用小型[有機化合物作為基質的](../Page/有機化合物.md "wikilink")、靈敏度更高的方法，他們稱之為[基質輔助雷射解吸/電離法](../Page/基質輔助雷射解吸/電離.md "wikilink")（MALDI）\[8\]
然而，雖然MALDI開發於SLD之前，但在田中的報告之前，MALDI並未用於電離蛋白質。

## 2002年諾貝爾獎

日本時間2002年10月9日晚間，[瑞典皇家科學院宣佈田中耕一在內的](../Page/瑞典皇家科學院.md "wikilink")3人獲得[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")。接獲[英語電話通知獲獎時](../Page/英語.md "wikilink")，田中正在公司加班。他從「Nobel」「Congratulation」這些單詞推測自己可能獲得「一個海外的小獎」，身旁的同事則認為是整人節目的手法（）。另一方面，日本[文部科學省與大眾](../Page/文部科學省.md "wikilink")[傳媒皆無所適從](../Page/傳媒.md "wikilink")，因為默默無名的田中並非學者，也沒有[博士學位](../Page/博士.md "wikilink")。前兩年的諾貝爾化學獎得主[野依良治與](../Page/野依良治.md "wikilink")[白川英樹互相聯絡後](../Page/白川英樹.md "wikilink")，也都不清楚「田中是誰」\[9\]。

獲獎後，田中因若干「特徵」引起舉世關注，自此職業生涯發生巨大變化。儘管田中拒絕大幅昇職，他仍於同年11月1日獲得島津製作所部長待遇。翌年1月擔任島津製作所「田中耕一記念質量分析研究所」所長（破例的「執行役員待遇」頭銜，享有[董事待遇](../Page/董事.md "wikilink")）。母校東北大學特別修改條例，為他頒發[名譽博士學位](../Page/名譽博士.md "wikilink")。

2011年，田中研究小组开发出能使[血液检查敏感度提高](../Page/血液.md "wikilink")100倍的新技术，更容易發現[癌症等](../Page/癌症.md "wikilink")[疾病](../Page/疾病.md "wikilink")\[10\]。

## 榮譽

  - 1989年 - 日本質量分析学会 獎勵獎
  - 2002年 - [諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")
  - 2002年 -
    [文化勋章](../Page/文化勋章.md "wikilink")·[文化功勞者](../Page/文化功勞者.md "wikilink")
  - 2002年 - [日本東北大學榮譽博士](../Page/日本東北大學.md "wikilink")
  - 2003年 - [富山縣名譽縣民](../Page/富山縣.md "wikilink")
  - 2003年 - 日本質量分析学会 特別獎
  - 2006年 - [日本學士院會員](../Page/日本學士院會員.md "wikilink")（史上最年輕）

## 社會活動

2004年4月，擔任大學的[客座教授](../Page/訪問學人.md "wikilink")，包括[愛媛大學無細胞生命科學工學研究中心](../Page/愛媛大學.md "wikilink")、[筑波大學先端學際領域研究中心](../Page/筑波大學.md "wikilink")、[京都大學國際融合創造中心](../Page/京都大學.md "wikilink")、東北大學[大學院工學研究科](../Page/大學院.md "wikilink")。同年6月應邀訪問[臺灣](../Page/臺灣.md "wikilink")。2008年3月從京都大學客座教授退任。2009年6月擔任[東京大學醫科學研究所客座教授](../Page/東京大學.md "wikilink")（疾患蛋白質組學實驗室顧問）。

[東日本大震災發生後](../Page/東日本大震災.md "wikilink")，田中接受[日本政府的委任](../Page/日本政府.md "wikilink")，於2011年11月擔任的委員一職。

2015年3月1日，諾貝爾獎得主座談（Nobel Week
Dialogue）首次移師海外，在日本[東京舉行](../Page/東京.md "wikilink")。在7名與會的[諾貝爾獎得主當中包括](../Page/諾貝爾獎得主.md "wikilink")2名日本人－－[京都大學的](../Page/京都大學.md "wikilink")[山中伸彌教授](../Page/山中伸彌.md "wikilink")（2012年醫學獎）與[名古屋大學的](../Page/名古屋大學.md "wikilink")[天野浩教授](../Page/天野浩.md "wikilink")（2014年物理學獎）\[11\]。此外，島津製作所的田中耕一（2002年化學獎）出席了最後一輪座談\[12\]。

## 相關書籍

  - 、『』2003年2月号

  - 「」（[『』](http://www.amazon.co.jp/dp/483151313X/)[上山明博](../Page/上山明博.md "wikilink")
    著、）

## 參見

  - [日本人諾貝爾獎得主](../Page/日本人諾貝爾獎得主.md "wikilink")

## 註釋

<div class="references-small">

</div>

## 參考

## 外部連結

  - [田中耕一記念質量分析研究所](http://www.shimadzu.co.jp/aboutus/ms_r/)

  - [島津製作所](http://www.shimadzu.co.jp/jindex.html)

  - [Topics/田中耕一氏の栄誉ある軌跡](http://www.bureau.tohoku.ac.jp/manabi/manabi22/mm22-45.html)

  - [田中耕一関連用語集](http://www.geocities.co.jp/Technopolis/9654/database/tanaka.html)

[category:日本化学家](../Page/category:日本化学家.md "wikilink")
[category:诺贝尔化学奖获得者](../Page/category:诺贝尔化学奖获得者.md "wikilink")
[category:日本諾貝爾獎獲得者](../Page/category:日本諾貝爾獎獲得者.md "wikilink")

[Category:東北大學校友](../Category/東北大學校友.md "wikilink")
[Category:富山縣出身人物](../Category/富山縣出身人物.md "wikilink")
[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")
[Category:文化功勞者](../Category/文化功勞者.md "wikilink")
[Category:日本东北大学名誉博士](../Category/日本东北大学名誉博士.md "wikilink")
[Category:日本学士院会员](../Category/日本学士院会员.md "wikilink")

1.
2.
3.
4.  <http://www.ch.ntu.edu.tw/nobel/2002.html>
5.
6.
7.
8.
9.  黑田龍彥，宋碧華，《上班族的諾貝爾奇蹟：工程師田中耕一的成功傳奇》，遠流，2004。
10. [田中耕一宣布：新技术能从一滴血中查出癌症](http://www.chinadaily.com.cn/hqgj/jryw/2011-11-11/content_4337307.html)科技日报，2011-11-11
11. [“诺贝尔奖获奖者座谈 东京2015” -
    客观日本](http://www.keguanjp.com/kgjp_keji/kgjp_kj_smkx/pt20150217071614.html)
12. [ノーベル受賞者7人、 「生命科学が拓く未来」を予想｜医療維新 -
    m3.comの医療コラム](https://www.m3.com/open/iryoIshin/article/299493/)