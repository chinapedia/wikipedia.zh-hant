**2005年[英国大奖赛](../Page/英国大奖赛.md "wikilink")**是于2005年7月8日至7月10日在[银石赛道举行的一站](../Page/银石赛道.md "wikilink")[一级方程式赛事](../Page/一级方程式.md "wikilink")。在比赛之前，人们为在[2005年7月倫敦爆炸事件中受影响的人而](../Page/2005年7月倫敦爆炸事件.md "wikilink")[默哀一分钟](../Page/默哀.md "wikilink")。

## 赛况

### 排位赛

| 排名 | 车号 | 车手                                                                                                                                                                                  | 车队                                     | 计时圈成绩    | 退后      |
| -- | -- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| 1  | 5  | [Flag_of_Spain.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Spain.svg "fig:Flag_of_Spain.svg") [费尔南多·阿隆索](../Page/费尔南多·阿隆索.md "wikilink")                                      | [雷诺车队](../Page/雷诺车队.md "wikilink")     | 1:19.905 | —       |
| 2  | 9  | [Flag_of_Finland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg "fig:Flag_of_Finland.svg") [奇米·雷克南](../Page/奇米·雷克南.md "wikilink")                                    | [迈凯伦车队](../Page/迈凯伦车队.md "wikilink")   | 1:19.932 | \+0.027 |
| 3  | 3  | [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg") [简森·巴顿](../Page/简森·巴顿.md "wikilink")   | [英美车队](../Page/英美车队.md "wikilink")     | 1:20.207 | \+0.302 |
| 4  | 10 | [Flag_of_Colombia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Colombia.svg "fig:Flag_of_Colombia.svg") [胡安·巴布罗·蒙托亚](../Page/胡安·巴布罗·蒙托亚.md "wikilink")                         | [迈凯伦车队](../Page/迈凯伦车队.md "wikilink")   | 1:20.382 | \+0.477 |
| 5  | 16 | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [雅诺·特鲁利](../Page/雅诺·特鲁利.md "wikilink")                                          | [丰田车队](../Page/丰田车队.md "wikilink")     | 1:20.459 | \+0.554 |
| 6  | 2  | [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg") [鲁本斯·巴里切罗](../Page/鲁本斯·巴里切罗.md "wikilink")                                   | [法拉利车队](../Page/法拉利车队.md "wikilink")   | 1:20.906 | \+1.001 |
| 7  | 6  | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [詹卡洛·费斯切拉](../Page/詹卡洛·费斯切拉.md "wikilink")                                      | [雷诺车队](../Page/雷诺车队.md "wikilink")     | 1:21.010 | \+1.105 |
| 8  | 4  | [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") [佐藤琢磨](../Page/佐藤琢磨.md "wikilink")                                              | [英美车队](../Page/英美车队.md "wikilink")     | 1:21.114 | \+1.209 |
| 9  | 17 | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") [拉尔夫·舒马赫](../Page/拉尔夫·舒马赫.md "wikilink")                                  | [丰田车队](../Page/丰田车队.md "wikilink")     | 1:21.191 | \+1.286 |
| 10 | 1  | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") [迈克尔·舒马赫](../Page/迈克尔·舒马赫.md "wikilink")                                  | [法拉利车队](../Page/法拉利车队.md "wikilink")   | 1:21.275 | \+1.370 |
| 11 | 11 | [Flag_of_Canada.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg "fig:Flag_of_Canada.svg") [雅克·维伦纽夫](../Page/雅克·维伦纽夫.md "wikilink")                                     | [索伯车队](../Page/索伯车队.md "wikilink")     | 1:21.352 | \+1.447 |
| 12 | 7  | [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") [马克·韦伯](../Page/马克·韦伯.md "wikilink")                                | [威廉姆斯车队](../Page/威廉姆斯车队.md "wikilink") | 1:21.997 | \+2.092 |
| 13 | 14 | [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg") [大卫·库塔](../Page/大卫·库塔.md "wikilink")   | [红牛车队](../Page/红牛车队.md "wikilink")     | 1:22.108 | \+2.203 |
| 14 | 8  | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") [尼克·海菲尔德](../Page/尼克·海菲尔德.md "wikilink")                                  | [威廉姆斯车队](../Page/威廉姆斯车队.md "wikilink") | 1:22.117 | \+2.212 |
| 15 | 15 | [Flag_of_Austria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Austria.svg "fig:Flag_of_Austria.svg") [克里斯蒂安·克莱因](../Page/克里斯蒂安·克莱因.md "wikilink")                              | [红牛车队](../Page/红牛车队.md "wikilink")     | 1:22.207 | \+2.302 |
| 16 | 12 | [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg") [菲利佩·马萨](../Page/菲利佩·马萨.md "wikilink")                                       | [索伯车队](../Page/索伯车队.md "wikilink")     | 1:22.495 | \+2.590 |
| 17 | 19 | [Flag_of_India.svg](https://zh.wikipedia.org/wiki/File:Flag_of_India.svg "fig:Flag_of_India.svg") [纳拉因·卡蒂凯扬](../Page/纳拉因·卡蒂凯扬.md "wikilink")                                      | [乔丹车队](../Page/乔丹车队.md "wikilink")     | 1:23.583 | \+3.678 |
| 18 | 21 | [Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg") [克里斯蒂安· 阿尔博斯](../Page/克里斯蒂安·_阿尔博斯.md "wikilink") | [米纳尔迪车队](../Page/米纳尔迪车队.md "wikilink") | 1:24.576 | \+4.671 |
| 19 | 20 | [Flag_of_Austria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Austria.svg "fig:Flag_of_Austria.svg") [帕特里克·弗里萨赫](../Page/帕特里克·弗里萨赫.md "wikilink")                              | [米纳尔迪车队](../Page/米纳尔迪车队.md "wikilink") | 1:25.566 | \+5.661 |
| 20 | 18 | [Flag_of_Portugal.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Portugal.svg "fig:Flag_of_Portugal.svg") [蒂亚戈·蒙泰罗](../Page/蒂亚戈·蒙泰罗.md "wikilink")                               | [乔丹车队](../Page/乔丹车队.md "wikilink")     | 没有成绩     |         |

### 正赛

| 排名 | 车号 | 车手                                                                                                                                                                                    | 车队                                       | 完成圈数 | 时间／退后       | 始发位置 | 本站积分   |
| -- | -- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- | ---- | ----------- | ---- | ------ |
| 1  | 10 | [Flag_of_Colombia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Colombia.svg "fig:Flag_of_Colombia.svg") **[胡安·巴布罗·蒙托亚](../Page/胡安·巴布罗·蒙托亚.md "wikilink")**                       | **[迈凯伦车队](../Page/迈凯伦车队.md "wikilink")** | 60   | 1'24:29.588 | 3    | **10** |
| 2  | 5  | [Flag_of_Spain.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Spain.svg "fig:Flag_of_Spain.svg") **[费尔南多·阿隆索](../Page/费尔南多·阿隆索.md "wikilink")**                                    | **[雷诺车队](../Page/雷诺车队.md "wikilink")**   | 60   | \+2.7       | 1    | **8**  |
| 3  | 9  | [Flag_of_Finland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg "fig:Flag_of_Finland.svg") **[奇米·雷克南](../Page/奇米·雷克南.md "wikilink")**                                  | **[迈凯伦车队](../Page/迈凯伦车队.md "wikilink")** | 60   | \+14.4      | 12 † | **6**  |
| 4  | 6  | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") **[詹卡洛·费斯切拉](../Page/詹卡洛·费斯切拉.md "wikilink")**                                    | **[雷诺车队](../Page/雷诺车队.md "wikilink")**   | 60   | \+17.9      | 6    | **5**  |
| 5  | 3  | [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg") **[简森·巴顿](../Page/简森·巴顿.md "wikilink")** | **[英美车队](../Page/英美车队.md "wikilink")**   | 60   | \+40.3      | 2    | **4**  |
| 6  | 1  | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") **[迈克尔·舒马赫](../Page/迈克尔·舒马赫.md "wikilink")**                                | **[法拉利车队](../Page/法拉利车队.md "wikilink")** | 60   | \+1:15.3    | 9    | **3**  |
| 7  | 2  | [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg") **[鲁本斯·巴里切罗](../Page/鲁本斯·巴里切罗.md "wikilink")**                                 | **[法拉利车队](../Page/法拉利车队.md "wikilink")** | 60   | \+1:16.6    | 5    | **2**  |
| 8  | 17 | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") **[拉尔夫·舒马赫](../Page/拉尔夫·舒马赫.md "wikilink")**                                | **[丰田车队](../Page/丰田车队.md "wikilink")**   | 60   | \+1:19.2    | 8    | **1**  |
| 9  | 16 | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [雅诺·特鲁利](../Page/雅诺·特鲁利.md "wikilink")                                            | [丰田车队](../Page/丰田车队.md "wikilink")       | 60   | \+1:20.9    | 4    |        |
| 10 | 12 | [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg") [菲利佩·马萨](../Page/菲利佩·马萨.md "wikilink")                                         | [索伯车队](../Page/索伯车队.md "wikilink")       | 59   | \+1圈        | 16   |        |
| 11 | 7  | [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") [马克·韦伯](../Page/马克·韦伯.md "wikilink")                                  | [威廉姆斯车队](../Page/威廉姆斯车队.md "wikilink")   | 59   | \+1圈        | 11   |        |
| 12 | 8  | [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg") [尼克·海菲尔德](../Page/尼克·海菲尔德.md "wikilink")                                    | [威廉姆斯车队](../Page/威廉姆斯车队.md "wikilink")   | 59   | \+1圈        | 14   |        |
| 13 | 14 | [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg") [大卫·库塔](../Page/大卫·库塔.md "wikilink")     | [红牛车队](../Page/红牛车队.md "wikilink")       | 59   | \+1圈        | 13   |        |
| 14 | 11 | [Flag_of_Canada.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg "fig:Flag_of_Canada.svg") [雅克·维伦纽夫](../Page/雅克·维伦纽夫.md "wikilink")                                       | [索伯车队](../Page/索伯车队.md "wikilink")       | 59   | \+1圈        | 10   |        |
| 15 | 15 | [Flag_of_Austria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Austria.svg "fig:Flag_of_Austria.svg") [克里斯蒂安·克莱因](../Page/克里斯蒂安·克莱因.md "wikilink")                                | [红牛车队](../Page/红牛车队.md "wikilink")       | 59   | \+1圈        | 15   |        |
| 16 | 4  | [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") [佐藤琢磨](../Page/佐藤琢磨.md "wikilink")                                                | [英美车队](../Page/英美车队.md "wikilink")       | 58   | \+2圈        | 7    |        |
| 17 | 18 | [Flag_of_Portugal.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Portugal.svg "fig:Flag_of_Portugal.svg") [蒂亚戈·蒙泰罗](../Page/蒂亚戈·蒙泰罗.md "wikilink")                                 | [乔丹车队](../Page/乔丹车队.md "wikilink")       | 58   | \+2圈        | 20 ‡ |        |
| 18 | 21 | [Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg") [克里斯蒂安· 阿尔博斯](../Page/克里斯蒂安·_阿尔博斯.md "wikilink")   | [米纳尔迪车队](../Page/米纳尔迪车队.md "wikilink")   | 57   | \+3圈        | 18   |        |
| 19 | 20 | [Flag_of_Austria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Austria.svg "fig:Flag_of_Austria.svg") [帕特里克·弗里萨赫](../Page/帕特里克·弗里萨赫.md "wikilink")                                | [米纳尔迪车队](../Page/米纳尔迪车队.md "wikilink")   | 56   | \+4圈        | 19   |        |
| 退赛 | 19 | [Flag_of_India.svg](https://zh.wikipedia.org/wiki/File:Flag_of_India.svg "fig:Flag_of_India.svg") [纳拉因·卡蒂凯扬](../Page/纳拉因·卡蒂凯扬.md "wikilink")                                        | [乔丹车队](../Page/乔丹车队.md "wikilink")       | 10   | 电子系统故障      | 17   |        |
|    |    |                                                                                                                                                                                       |                                          |      |             |      |        |

## 注释

  - [杆位](../Page/杆位.md "wikilink")：[费尔南多·阿隆索](../Page/费尔南多·阿隆索.md "wikilink")
    1:19.905
  - †
    [奇米·雷克南虽然在排位赛中去的第二名的成绩](../Page/奇米·雷克南.md "wikilink")，但因更换引擎而受到退后十位发车的处罚。
  - ‡
    [蒂亚戈·蒙泰罗没有完成排位赛](../Page/蒂亚戈·蒙泰罗.md "wikilink")，并因更换引擎而受到退后十位发车的处罚。
  - 最快圈速： [奇米·雷克南](../Page/奇米·雷克南.md "wikilink") 1:20.502

[Category:2005年一級方程式賽果](../Category/2005年一級方程式賽果.md "wikilink")
[Category:英国大奖赛](../Category/英国大奖赛.md "wikilink")