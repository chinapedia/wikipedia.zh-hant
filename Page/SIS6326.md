**SIS6326**為[矽統公司推出的首款獨立顯示卡](../Page/SiS.md "wikilink")，於1997年發佈。SIS6326支援[PCI和](../Page/PCI.md "wikilink")[AGP](../Page/AGP.md "wikilink")
1X介面，並搭載4MB\~8MB不等的顯示記憶體。SIS6326顯示核心採用32位元[VLIW架構](../Page/VLIW.md "wikilink")、208針腳[PQFP封裝](../Page/PQFP.md "wikilink")。

## 效能

  - 每秒可生成80萬個多邊形，支援[雙線性](../Page/雙線性.md "wikilink")、[三線性過濾](../Page/三線性過濾.md "wikilink")
  - 170MHz [RAMDAC](../Page/RAMDAC.md "wikilink")
  - 2D/3D繪圖引擎加速架構
  - [NTSC](../Page/NTSC.md "wikilink")/[PAL](../Page/PAL.md "wikilink")
    TV信號輸出
  - AGP/PCI介面控制單元

## 特色

SiS
6326一大特點就是內置了[DVD](../Page/DVD.md "wikilink")[硬體加速功能](../Page/硬體加速.md "wikilink")，內建[Run
length與](../Page/Run_length.md "wikilink")[zigzag解碼器](../Page/zigzag.md "wikilink")、[IDCT](../Page/IDCT.md "wikilink")（Inverse
Discrete Cosine
Transform，[反離散餘弦轉換](../Page/反離散餘弦轉換.md "wikilink")）邏輯電路、Motion
compensation數位邏輯電路，不需要其它DVD硬體加速晶片輔助就支援DVD/MPEG-Ⅱ影像加速，對比軟體模擬可降低80%處理器使用率，以當時的處理器能力，這是很有實用性的，因此在播放DVD時比起其他顯示卡更具優勢。不過SiS
6326中有一個版本沒有硬體加速功能，消費者可能會買到錯誤的版本。

SIS6326縱使沒有出眾的3D性能，但影片播放效果出色。它支援[MPEG-2硬體加速](../Page/MPEG-2.md "wikilink")，不需要其它輔助晶片，使當時低階的[Celeron處理器亦可順利播放](../Page/Celeron.md "wikilink")[DVD](../Page/DVD.md "wikilink")。它也支持NTSC或PAL的電視信號輸出。其低廉的售价使它在低端市场赢得不小的份额，其市场寿命也长达1997－2002年。[中華民國政府過去在推動擴大內需採購時](../Page/中華民國.md "wikilink")，曾大量採用SIS6326顯示卡作為當時政府、學校採購電腦的主要顯示輸出設備。

## 版本差異

SIS6326中，只有SIS6326 Cx與SIS6326-DVD D2版本的晶片支援DVD/MPEG-2的硬體加速，SIS6326-AGP
G0/H0版本則不支援。

|                    |                 |             |
| ------------------ | --------------- | ----------- |
|                    | DVD/MPEG-2 硬體加速 | TV-Out 視訊輸出 |
| SiS6326 Cx版        | Yes             | Yes         |
| SiS6326-DVD D2版    | Yes             | Yes         |
| SiS6326-AGP G0/H0版 | No              | No          |

[Category:顯示卡](../Category/顯示卡.md "wikilink")
[Category:SiS](../Category/SiS.md "wikilink")