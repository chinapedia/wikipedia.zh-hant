**棘背魚科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[棘背魚目的其中一科](../Page/棘背魚目.md "wikilink")。

## 分類

**棘背魚科**下分5個屬，如下：

### 四棘刺魚屬（*Apeltes*）

  - [四棘刺魚](../Page/四棘刺魚.md "wikilink")（*Apeltes auadracus*）

### 溪刺魚屬（*Culaea*）

  - [溪刺魚](../Page/溪刺魚.md "wikilink")（*Culaea inconstans*）

### 刺魚屬（*Gasterosteus*）

  - [三刺魚](../Page/三刺魚.md "wikilink")（*Gasterosteus aculeatus*）
  - [鋸刺魚](../Page/鋸刺魚.md "wikilink")（*Gasterosteus crenobiontus*）
  - （*Gasterosteus gymnurus*）
  - （*Gasterosteus islandicus*）
  - 小頭刺魚（*Gasterosteus microcephalus*）
  - [二棘刺魚](../Page/二棘刺魚.md "wikilink")（*Gasterosteus wheatlandi*）

### 多刺魚屬（*Pungitius*）

  - （*Pungitius bussei*）
  - [沼澤多刺魚](../Page/沼澤多刺魚.md "wikilink")（*Pungitius hellenicus*）
  - [南方多刺魚](../Page/南方多刺魚.md "wikilink")（*Pungitius kaibarae*）
  - [光身多刺魚](../Page/光身多刺魚.md "wikilink")（*Pungitius laevis*）
  - [平腹多刺魚](../Page/平腹多刺魚.md "wikilink")（*Pungitius platygaster*）
  - （*Pungitius polyakovi*）
  - [八棘多刺魚](../Page/八棘多刺魚.md "wikilink")（*Pungitius pungitius*）
  - [中華多刺魚](../Page/中華多刺魚.md "wikilink")（*Pungitius sinensis*）
  - （*Pungitius stenurus*）
  - [圖們江多刺魚](../Page/圖們江多刺魚.md "wikilink")（*Pungitius tymensis*）

### 海刺魚屬（*Spinachia*）

  - [海刺魚](../Page/海刺魚.md "wikilink")（*Spinachia spinachia*）

[\*](../Category/棘背魚科.md "wikilink")