[Soy_Street_View1_20120730.jpg](https://zh.wikipedia.org/wiki/File:Soy_Street_View1_20120730.jpg "fig:Soy_Street_View1_20120730.jpg")
[Soy_Street_2015.jpg](https://zh.wikipedia.org/wiki/File:Soy_Street_2015.jpg "fig:Soy_Street_2015.jpg")

**豉油街**（）是一條位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[油尖旺區](../Page/油尖旺區.md "wikilink")[旺角的街道](../Page/旺角.md "wikilink")。西起德昌街，橫跨多條主要道路，如[上海街](../Page/上海街.md "wikilink")、[彌敦道](../Page/彌敦道.md "wikilink")、[西洋菜南街等](../Page/西洋菜南街.md "wikilink")，東至[窩打老道](../Page/窩打老道.md "wikilink")。介乎[砵蘭街與](../Page/砵蘭街.md "wikilink")[西洋菜南街之間的一段被劃作行人專用區](../Page/西洋菜南街.md "wikilink")，下有[行人隧道橫越彌敦道地下](../Page/行人隧道.md "wikilink")。豉油街最繁忙的一段是介乎彌敦道與[花園街之間](../Page/花園街.md "wikilink")，假日及晚間都擠得水洩不通。

## 歷史

豉油是[醬油的一種](../Page/醬油.md "wikilink")，由[黃豆釀製](../Page/黃豆.md "wikilink")。

2004年5月7日，工人在[通菜街和豉油街交界一個深](../Page/通菜街.md "wikilink")2米的渠務地盤中，發掘出[東漢和](../Page/東漢.md "wikilink")[唐代](../Page/唐代.md "wikilink")[陶器](../Page/陶器.md "wikilink")、製陶工具以至窰爐及窰具殘件，顯示在千多年前，旺角一帶已有人居住。\[1\]

在1980年代啟用的豉油街臨時熟食市場，因晚上常坐滿200名妓女及馬伕，令熟食市場猶如一個[紅燈區](../Page/紅燈區.md "wikilink")，被稱為旺角「人肉市場」，不過警方自2002年起全力掃蕩旺角色情事業後，馬伕、妓女都紛紛離去。熟食市場已於2006年12月1日關閉。\[2\]

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")：

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")、<font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[旺角站E](../Page/旺角站.md "wikilink")1、E2出口

## 鄰近

  - [地王酒店](../Page/地王酒店.md "wikilink")
  - [仕德福酒店](../Page/仕德福酒店.md "wikilink")
  - [聖公會諸聖中學](../Page/聖公會諸聖中學.md "wikilink")
  - [洗衣街花園](../Page/洗衣街花園.md "wikilink")
  - [豉油街12號](../Page/豉油街12號.md "wikilink")
  - [新寶戲院](../Page/新寶戲院.md "wikilink")
  - [荷李活商業中心](../Page/荷李活商業中心_\(旺角\).md "wikilink")
  - [總統商業大廈](../Page/總統商業大廈.md "wikilink")
  - [創興廣場](../Page/創興廣場.md "wikilink")
  - [旺角](../Page/旺角.md "wikilink")[東京銀座](../Page/東京銀座.md "wikilink")
  - [好旺角購物中心](../Page/好旺角購物中心.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[分類:香港特色街道](../Page/分類:香港特色街道.md "wikilink")

[Category:旺角街道](../Category/旺角街道.md "wikilink")

1.  [古物及古蹟辦事處：旺角渠務地盤發現的文物](http://www.amo.gov.hk/b5/news_200405112.php)
2.