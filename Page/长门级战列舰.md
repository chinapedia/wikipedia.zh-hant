**長門級战列舰**是[日本设计建造的一种](../Page/日本.md "wikilink")[战列舰](../Page/战列舰.md "wikilink")，也是在[华盛顿条约生效期间](../Page/华盛顿条约.md "wikilink")，日本威力最大的战列舰。同级舰两艘：[長門及](../Page/長門號戰艦.md "wikilink")[陸奥](../Page/陸奥號戰艦.md "wikilink")。

## 建造

[第一次世界大战势力增强的日本为争夺太平洋地帶的海上霸权](../Page/第一次世界大战.md "wikilink")，于1916年（大正5年）重新制定了[八八舰队海军扩充计划](../Page/八八舰队.md "wikilink")，长门级是该计划中最早开工的战列舰。建造方案A-102，主設計師為[平賀讓](../Page/平賀讓.md "wikilink")。

根据以往海战的经验，日本海军决定建造高航速重火力的战列舰，在与[美国海军造舰竞争中以质补充量的不足](../Page/美国海军.md "wikilink")。安装当时战列舰最大口径的410毫米口径主炮，主炮塔采用背负式艏艉对称布局。主炮仰角达到30度，射程达到3万米，副炮在顶层甲板与上甲板采用炮廓式安装。长门级最具特色的是，为远距离观测以及指挥的需要，采用七根支柱支撑的高大樯式桅楼，顶部设立射击指挥所，并且设计了独特的“勺型”舰艏（为了使用链系水雷，不会勾住连接两枚水雷的链条）。随着战列舰火炮射程增加，根据[日德兰海战的经验](../Page/日德兰海战.md "wikilink")，加强了重点区域防御装甲尤其是水平防御装甲。长门级拥有当时战列舰最快的航行速度，最高航速达到26.7节，日本海军对长门级的航速指标采取了保密措施，当时对外公布的航速是23节。由于之前的日本战列舰是英国的设计或者是基于英国设计的更改，所以完全由日本自行设计的长门级战列舰被视为“第一级纯日本血统的战舰”。长门级是当时世界上最强大的战列舰之一。

## 服役

长门号于1917年8月28日开工，1920年11月竣工。1921年[华盛顿會議中](../Page/华盛顿會議.md "wikilink")，美国、英国要求日本销毁已下水尚未竣工的陆奥号，日本一方面加紧施工，一方面聲稱该舰已经建成坚决反对拆解，最终陆奥号于1921年11月竣工服役，獲得保留；交換條件則是[攝津號戰艦解除武裝成為靶艦](../Page/攝津號戰艦.md "wikilink")、以及美國科羅拉多級戰艦最後一艘可完工。在华盛顿条约有效时期，當時海軍航空兵力尚於雛形，各國判斷戰力指標的方式仍以戰艦數量、裝載主炮口徑為基準；而海軍列強中，只有7艘戰艦有著最大的16英吋艦炮，分別是2艘長門級、2艘[納爾遜級戰艦](../Page/納爾遜級戰艦.md "wikilink")、3艘[科羅拉多級戰艦](../Page/科羅拉多級戰艦.md "wikilink")。这7艘拥有最大口径火炮的战列舰，被各国海军人士称为“[big
seven](../Page/big_seven.md "wikilink")”\[1\]。

长门级服役后为克服桅楼与前烟囱距离过近导致排烟倒灌的影响，最初加装了烟囱帽但效果并不充分，1923年开始改装，将前烟囱被改成大幅度向后弯曲的形状以远离前桅楼。长门号服役后作为[日本联合舰队的](../Page/日本联合舰队.md "wikilink")[旗舰直到](../Page/旗舰.md "wikilink")[大和號戰艦服役](../Page/大和號戰艦.md "wikilink")，快20年的時間經常對外開放與海外訪問，成為日本國民最熟知的战舰，與海軍象徵；當時長門號知名程度已經到如果問日本小學生最喜歡的戰艦是哪一艘，大部分幾乎會立刻回答是長門號，同時如果有寫生軍艦的機會也會以該艦為首選\[2\]。由於日本政府嚴格對大和級建造事項保密，大部分的日本人直到戰爭結束前都不知道大和級事蹟，反而是長門級更為大眾所知曉。

1923年[關東大地震之際](../Page/關東大地震.md "wikilink")，聯合艦隊艦艇多集中在[渤海灣實施聯合演習](../Page/渤海.md "wikilink")，獲知日本本土災情後，長門級隨即載運救災物資全速趕往東京灣。不過在日本[遠州灘海域時](../Page/遠州灘.md "wikilink")，正好遇到英國皇家海軍的，為避免極速被國外軍艦查知，長門號因此放慢航速，比派遣號慢駛進[橫濱港](../Page/橫濱港.md "wikilink")。

1926年（昭和元年）长门号在舰体中后部设立了飞机搭载装置，是第一艘装备水上飞机弹射器的日本战列舰。

## 改装

1934年，2艘长门级进行了现代化改造，包括提高主炮仰角达到43度，增大主炮射程，更新观测瞄准装置，加强舰体与炮塔的防护，舰体舯部增加防鱼雷隔舱。延长舰艉，改建舰桥桅楼等上层建筑，全部更换专烧重油锅炉并将两个烟囱合并成一个，加强了防空火力。改装使标准排水量增加约6千吨，由于排水量增加主机不变最高航速下降为25节。

## 战时经历

在[太平洋战争期间](../Page/太平洋战争.md "wikilink")，长门级因为航速的限制并且作为最后决战的主力而很少出战，*开战以后日本海军的战列舰战队因长期驻泊广岛的柱岛锚地待机而被频繁出击的航空母舰战队的军官讽刺称为“柱岛舰队”。*1943年6月停泊在广岛湾的陆奥号三号主炮塔弹药库发生原因不明的爆炸后沉没。1944年10月长门号参与[莱特湾海战](../Page/莱特湾海战.md "wikilink")。战争末期由于燃油短缺而行动能力不足，作为警备舰被编入预备役。

在日本投降时是唯一尚能作戰之戰列艦，作为战争赔偿移交给美国，1946年在[比基尼岛的](../Page/比基尼岛.md "wikilink")[十字路行动中](../Page/十字路行动.md "wikilink")，被美军用于[核弹爆炸试验](../Page/核弹.md "wikilink")。在經過空中爆炸與水中爆炸的兩次測試後，長門仍然在海面上漂浮，直到第二次試爆後的第五天因大量进水沉沒。

## 性能数据

  - 排水量：33800吨（正常），改装后标准排水量：39130吨
  - 尺寸：全长215.8米/宽28.96米/吃水9.1米，改装后224.9米/宽34.6米/吃水9.45米
  - 动力：主机功率80000马力，改装后82000马力；
  - 航速：26.5节，改装后25节；续航力：5500海里/16节，改装后10600海里/16节；
  - 武备：8门双联装410毫米/45倍口径主炮，20门140毫米口径副炮（改装后18门），4门76毫米高射炮（改装后8门双联装127毫米高射炮，20门双联装25毫米高射炮）
  - 装甲：侧舷装甲带(最大)305毫米（改装后360毫米），炮塔(正面)356毫米（改装后500毫米），装甲甲板(最大)76毫米（改装后185毫米），司令塔336毫米。装甲重量10396吨（改装后13023吨）。
  - 舰员：1333-1368人

## 同級艦

<table>
<thead>
<tr class="header">
<th><p>船名</p></th>
<th><p>製造廠</p></th>
<th><p>建造時間</p></th>
<th><p>船籍</p></th>
<th><p>服役時間</p></th>
<th><p>結局</p></th>
<th><p>圖片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/長門號戰艦.md" title="wikilink">長門號</a></p></td>
<td><p>吳市吳海軍工廠</p></td>
<td><p>1917年8月28日－1919年11月9日</p></td>
<td><p>横须贺镇守府</p></td>
<td><p>1920年11月15日－1945年8月30日（日本海軍）<br />
1945年8月30日－1946年7月29日（美國海軍）</p></td>
<td><p>1946年7月29日在<a href="../Page/十字路行動.md" title="wikilink">十字路行動中沉沒</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Japanese_Battleship_Nagato_1944.jpg" title="fig:Japanese_Battleship_Nagato_1944.jpg">Japanese_Battleship_Nagato_1944.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陸奧號戰艦.md" title="wikilink">陸奧號</a></p></td>
<td><p>橫須賀海軍工廠</p></td>
<td><p>1918年6月1日－1920年5月31日</p></td>
<td><p>佐世保镇守府</p></td>
<td><p>1921年10月24日－1943年6月8日</p></td>
<td><p>1943年6月8日原因不明爆沉</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Japanese_battleship_Mutsu.jpg" title="fig:Japanese_battleship_Mutsu.jpg">Japanese_battleship_Mutsu.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

  - 《战场文集》

## 相關項目

  - [大日本帝國海軍艦艇列表](../Page/大日本帝國海軍艦艇列表.md "wikilink")

[型Nagato](../Category/日本海军战列舰.md "wikilink")
[N](../Category/日本軍艦艦級.md "wikilink")
[日N](../Category/戰艦等級.md "wikilink")

1.  『平易に説いた陸海軍の知識』p.143
2.  [\#増補
    軍艦物語p](../Page/#増補_軍艦物語.md "wikilink").13『「戦闘艦を描いてよう、戦闘艦を」と注文する子供には是が非でも「陸奥」や「長門」のあの仁王様の火見櫓のやうな前檣と芋蟲か何ぞのやうな前部煙突とを描いて見せなければ承知しないのだ。』