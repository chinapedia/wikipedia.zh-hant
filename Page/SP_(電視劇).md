《**SP
警視廳警備部警護課第四係**》是一部從2007年11月3日到2008年1月26日間，在[富士电视](../Page/富士电视台.md "wikilink")[系每](../Page/FNS.md "wikilink")
星期六[晚上](../Page/晚上.md "wikilink")11[時](../Page/小時.md "wikilink")10－55分（[JST](../Page/JST.md "wikilink")）播放，而由[岡田准一主演的](../Page/岡田准一.md "wikilink")[电视连续剧](../Page/电视连续剧.md "wikilink")、全11集。第一集加长15分钟。台灣[緯來日本台以](../Page/緯來日本台.md "wikilink")「**SP特勤型男**」名稱播出，香港則以「**SP特警**」名稱播出，台灣[WakuWaku
Japan於](../Page/WakuWaku_Japan.md "wikilink")2017年7月13日起以「**SP特勤型男**」名稱播出。

## 演員

  - 井上薰：[岡田准一](../Page/岡田准一.md "wikilink")
  - 尾形總一郎：[堤真一](../Page/堤真一.md "wikilink")
  - 笹本繪里：[真木陽子](../Page/真木陽子.md "wikilink")
  - 山本隆文：[松尾諭](../Page/松尾諭.md "wikilink")
  - 石田光男：[神尾佑](../Page/神尾佑.md "wikilink")
  - 原川：[平田敦子](../Page/平田敦子.md "wikilink")
  - 中尾義春：[江上真悟](../Page/江上真悟.md "wikilink")
  - 西島勇司：[飯田基祐](../Page/飯田基祐.md "wikilink")
  - 麻田雄三：[山本圭](../Page/山本圭.md "wikilink")
  - 山西一彌：[平田滿](../Page/平田滿.md "wikilink")

## 製作人員

  - 總監督：[本廣克行](../Page/本廣克行.md "wikilink")
  - 導演：[波多野貴文](../Page/波多野貴文.md "wikilink")、[藤本周](../Page/藤本周.md "wikilink")
  - 原案、劇本：[金城一紀](../Page/金城一紀.md "wikilink")
  - 音樂：[菅野祐悟](../Page/菅野祐悟.md "wikilink")
  - 編成：[松崎容子](../Page/松崎容子.md "wikilink")
  - 監製：[高井一郎](../Page/高井一郎.md "wikilink")

## 播出日、副標題、收視率

<table>
<thead>
<tr class="header">
<th><p>單元</p></th>
<th><p>集數</p></th>
<th><p>播出日</p></th>
<th><p>副標題</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Episode I</p></td>
<td><p>1</p></td>
<td><p>2007年11月3日</p></td>
<td></td>
<td><p>14.5%</p></td>
</tr>
<tr class="even">
<td><p>Episode II</p></td>
<td><p>2</p></td>
<td><p>2007年11月10日</p></td>
<td></td>
<td><p>17.6%</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2007年11月17日</p></td>
<td></td>
<td><p>15.2%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2007年11月24日</p></td>
<td></td>
<td><p>15.7%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Episode III</p></td>
<td><p>5</p></td>
<td><p>2007年12月1日</p></td>
<td></td>
<td><p>14.1%</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2007年12月8日</p></td>
<td></td>
<td><p>14.6%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2007年12月15日</p></td>
<td></td>
<td><p>15.5%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Episode 0</p></td>
<td><p>8</p></td>
<td><p>2007年12月22日</p></td>
<td></td>
<td><p><span style="color:blue">12.6%</p></td>
</tr>
<tr class="odd">
<td><p>Episode IV</p></td>
<td><p>9</p></td>
<td><p>2008年1月12日</p></td>
<td></td>
<td><p>13.8%</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2008年1月19日</p></td>
<td></td>
<td><p>16.6%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最終話</p></td>
<td><p>2008年1月26日</p></td>
<td></td>
<td><p>18.9%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>特別篇</p></td>
<td><p>2008年4月5日</p></td>
<td><p><br />
<br />
</p></td>
<td><p><span style="color:red">21.5%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平均收視率（收視率為日本<a href="../Page/關東地方.md" title="wikilink">關東地區由</a><a href="../Page/Video_Research.md" title="wikilink">Video Research公司調查</a>）</p></td>
<td><p>15.4%</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 劇場版

劇場版『[SP THE MOTION
PICTURE](../Page/SP_THE_MOTION_PICTURE.md "wikilink")』解開電視版未解的謎，2010年10月30日「野望篇」、2011年3月12日「革命篇」順次播映。

## 外部連結

  - [緯來日本台的官網](http://japan.videoland.com.tw/channel/sp)

## 節目的變遷

[Category:富士電視台週六連續劇](../Category/富士電視台週六連續劇.md "wikilink")
[Category:2007年日本電視劇集](../Category/2007年日本電視劇集.md "wikilink")
[Category:2008年日本電視劇集](../Category/2008年日本電視劇集.md "wikilink")
[Category:2008年電視特別劇集](../Category/2008年電視特別劇集.md "wikilink")
[Category:2011年日本電視劇集](../Category/2011年日本電視劇集.md "wikilink")
[Category:2011年電視特別劇集](../Category/2011年電視特別劇集.md "wikilink")
[Category:週六獎賞](../Category/週六獎賞.md "wikilink")
[Category:政治電視劇](../Category/政治電視劇.md "wikilink")
[Category:超能力題材電視劇](../Category/超能力題材電視劇.md "wikilink")
[Category:日本動作劇](../Category/日本動作劇.md "wikilink")
[Category:日本科幻劇](../Category/日本科幻劇.md "wikilink")
[Category:恐怖活動題材作品](../Category/恐怖活動題材作品.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:日劇學院賞最佳作品](../Category/日劇學院賞最佳作品.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:2010年電影](../Category/2010年電影.md "wikilink")
[Category:2011年电影](../Category/2011年电影.md "wikilink")
[Category:電視劇改編電影](../Category/電視劇改編電影.md "wikilink")
[Category:2010年代动作片](../Category/2010年代动作片.md "wikilink")
[Category:2010年代科幻片](../Category/2010年代科幻片.md "wikilink")
[Category:日本动作片](../Category/日本动作片.md "wikilink")
[Category:日本科幻片](../Category/日本科幻片.md "wikilink")
[Category:超能力題材電影](../Category/超能力題材電影.md "wikilink")
[Category:恐怖活動電影](../Category/恐怖活動電影.md "wikilink")
[Category:富士電視台製作的電影](../Category/富士電視台製作的電影.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:預知題材作品](../Category/預知題材作品.md "wikilink")
[Category:金城一紀](../Category/金城一紀.md "wikilink")