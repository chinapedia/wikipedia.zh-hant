**罗南新村站**位于[上海](../Page/上海.md "wikilink")[宝山区](../Page/宝山区_\(上海市\).md "wikilink")[罗店镇](../Page/罗店镇.md "wikilink")[沪太路](../Page/沪太路.md "wikilink")[杨南路西北角](../Page/杨南路.md "wikilink")，为[上海轨道交通7号线二期的车站](../Page/上海轨道交通7号线.md "wikilink")。於2010年12月28日啟用。

## 车站结构

本站为高架车站，设2个侧式站台。

<table>
<tbody>
<tr class="odd">
<td><p><strong>2F</strong></p></td>
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
</tr>
<tr class="even">
<td><p>1站台</p></td>
<td><p>列车往方向 <small>（）</small></p></td>
</tr>
<tr class="odd">
<td><p>2站台</p></td>
<td><p><span style="color:white;">→</span> 列车往方向 <small>（）</small> </p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/侧式站台.md" title="wikilink">侧式站台</a>，右边车门将会开启</small></p>
</center></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1F</strong></p></td>
<td><p>站厅</p></td>
</tr>
</tbody>
</table>

## 公交换乘

552（全程）、963、北华线、北罗专线、罗南线、彭南线、钱泰线、泰罗专线、永罗线、宝山4路

## 参考资料

[罗南新村站建设用地规划许可证](https://web.archive.org/web/20070927192711/http://www.shghj.gov.cn/Ct_3.aspx?ct_id=00060613E09341)

[Category:上海市宝山区地铁车站](../Category/上海市宝山区地铁车站.md "wikilink")
[Category:2010年启用的铁路车站](../Category/2010年启用的铁路车站.md "wikilink")