**中華民國在臺遷都議題**，指的是[兩岸分治後位於](../Page/兩岸分治.md "wikilink")[臺灣地區的](../Page/臺灣地區.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")，將[首都](../Page/中華民國首都.md "wikilink")（[中央政府所在地](../Page/中央政府所在地.md "wikilink")）由[臺北市遷移到其他](../Page/臺北市.md "wikilink")[都市的議題](../Page/都市.md "wikilink")。

## 背景

[臺灣自](../Page/臺灣.md "wikilink")[清治末期開始](../Page/臺灣清治時期.md "wikilink")，政府中樞就一直設置於臺北，包括中華民國政府在1949年遷往臺灣後，也以臺北做為[中央政府所在地](../Page/中央政府所在地.md "wikilink")。而長期以臺北為中心的建設政策，造成「從臺北看臺灣」的「臺北觀點」，各項資源過度集中臺北，北部過於擁擠及北中南發展失衡，又由於產業外移所導致臺灣[中](../Page/中臺灣.md "wikilink")[南部長期經濟不景氣的情況](../Page/南臺灣.md "wikilink")，逐漸形成南北對立的情勢，因此有人提出遷都中南部以緩和南北對立的情勢\[1\]。

## 贊成與反對意見

反對意見主要認為遷都耗費過鉅，且無明顯實益，在臺灣景氣不振的情況下，造成政府財政負擔。

支持者則舉近現代[美國](../Page/美國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[巴西等國之](../Page/巴西.md "wikilink")[遷都前例](../Page/遷都.md "wikilink")，認為在[台灣高速鐵路興建後](../Page/台灣高速鐵路.md "wikilink")，遷都可加強內需、調整臺灣國土規畫的偏頗失當、緩和北部過於擁擠的問題、調和北部與中南部之差距、緩和南北對立的情勢、改善各地房價物價差異、及分散國家安全風險。而地理條件上，仍以台中市為較多推舉的地點，國防上以台中也較為易守且較不易被斷絕（相較於北部若以桃園登陸的話，就直進台北，退無可退,若進攻中部,多為不可登陸海岸,且登陸,南北皆可往中部夾擊），防守撤退線也可從臺中延伸至舊省府霧峰最後再進到最深處南投中興新村，拉長緩衝時間，亦有更多時間可以讓美日等國際外援抵達台灣\[2\]。

## 方案與程序

目前獲得較多討論的遷都地點，主要有[高雄市](../Page/高雄市.md "wikilink")、[臺南市](../Page/臺南市.md "wikilink")、[彰化縣](../Page/彰化縣.md "wikilink")、[臺中市及](../Page/臺中市.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[中興新村等地](../Page/中興新村.md "wikilink")。\[3\]\[4\]\[5\]\[6\]\[7\]
但其中有遷都地點是[2010年縣市合併尚未進行之前](../Page/2010年中華民國縣市改制直轄市.md "wikilink")，故與現今所指遷都地點於行政區劃上則有所差異。有部分政治人物推動立法或依《[公民投票法](../Page/公民投票法.md "wikilink")》由進行遷都\[8\]\[9\]。

### 臺中市

前任[臺中市長](../Page/臺中市長.md "wikilink")[林佳龍即於](../Page/林佳龍.md "wikilink")[立法委員任內提出](../Page/立法委員.md "wikilink")「[國會](../Page/國會.md "wikilink")（[立法院](../Page/立法院.md "wikilink")）遷都臺中」的[副首都計畫](../Page/副都.md "wikilink")，以平衡國土規劃之區域發展，獲得前總統[李登輝與](../Page/李登輝.md "wikilink")[民主進步黨總統候選人](../Page/民主進步黨.md "wikilink")[蔡英文支持](../Page/蔡英文.md "wikilink")。行政院長賴清德亦支持此構想\[10\]\[11\]\[12\]\[13\]\[14\]\[15\][臺中市位於臺灣中部](../Page/臺中市.md "wikilink")、分擔部分首都功能，如韓國的[世宗市](../Page/世宗市.md "wikilink")。林佳龍強調，韓國政府為了解決首都擁擠及城鄉發展不均的問題，投入資源設立世宗市，為鼓勵公務員及居民更願意搬遷至該處，結合教育、交通、環保，不只是要建行政新都，更要打造成宜居城市，值得台中市借鏡。\[16\]

### 南投縣

[中興新村](../Page/中興新村.md "wikilink")，為[臺灣省政府所在地](../Page/臺灣省政府.md "wikilink")，但省府因為[精省已凍結組織](../Page/精省.md "wikilink")，現今無太大功能，然[中興新村原有之行政設施仍存](../Page/中興新村.md "wikilink")，國防有自然屏障，而為其中一支持的聲音。

## 参考文献

## 參考文獻

  - 蕭英利,
    [從民意角度探究遷都中臺灣議題](http://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=X7ObOr/search?q=kwc=%22D-%E7%9F%A9%E9%99%A3%E7%90%86%E8%AB%96%22.&searchmode=basic),
    [國立中興大學國家政策與公共事務研究所碩士論文](../Page/國立中興大學.md "wikilink"), 2013

## 參見

  - [遷都](../Page/遷都.md "wikilink")
  - [中華民國首都](../Page/中華民國首都.md "wikilink")、[中華民國行政區劃](../Page/中華民國行政區劃.md "wikilink")
  - [韓國遷都問題](../Page/韓國遷都問題.md "wikilink")
  - [日本遷都問題](../Page/日本遷都問題.md "wikilink")、[遷都東京](../Page/遷都東京.md "wikilink")

{{-}}

[Category:臺灣政治](../Category/臺灣政治.md "wikilink")
[Category:遷都](../Category/遷都.md "wikilink")

1.  [遷都平衡城鄉
    窮縣市萬靈丹？](http://www.libertytimes.com.tw/2006/new/oct/23/today-life1.htm)
    , 自由電子報, 2006年10月23日
2.  [台灣脫胎換骨
    遷都最佳捷徑](http://magazine.sina.com/bg/newtaiwan/564/2007-01-11/ba27060.html),
    《新台灣新聞週刊》, 2007/1/10,
3.  [遷都高雄 分都先行](http://www.formosamedia.com.tw/weekly/post_2352.html) ,
    玉山周報, 2011-4-19
4.  [遷都台中 平衡發展](http://www.formosamedia.com.tw/weekly/post_2351.html) ,
    玉山周報, 2011-4-19
5.  [推動遷都彰化
    藍綠一起來](http://www.libertytimes.com.tw/2007/new/mar/11/today-center4.htm)
    , 自由電子報, 2007年3月11日
6.  [台灣的新首都想像](http://www.taichung.fcu.edu.tw/ct4.htm), 台中學研究中心
7.  [建都中興新村](http://www.libertytimes.com.tw/2007/new/jan/7/today-o3.htm)
    , 自由電子報, 2007年1月7日
8.  [遷都中南部
    朝野立委連署](http://www.libertytimes.com.tw/2006/new/oct/17/today-fo5.htm)
    , 自由電子報, 2006年10月17日
9.  [遷都中南部公投
    提案程序開跑](http://www.libertytimes.com.tw/2007/new/jan/3/today-p4.htm)
    , 自由電子報, 2007年1月3日
10. [林佳龍談國會遷建-中央社專訪](http://forpeople.pixnet.net/blog/post/14946199) ,
    2012-2-4
11. [林佳龍](../Page/林佳龍.md "wikilink"),
    [國會遷建與國土規劃](http://news.ltn.com.tw/news/opinion/paper/560930),
    [自由時報](../Page/自由時報.md "wikilink"), 2012-02-15
12. [林佳龍：提案「立法院遷建規劃暨監督委員會」送交朝野協商](http://www.ly.gov.tw/03_leg/0301_main/dispatch/dispatchView.action?id=36403&lgno=00028&stage=8&atcid=36403),
    [立法院](../Page/立法院.md "wikilink"), 2012-05-08
13. [「三個台中」升等副首都](http://www.citylove.org.tw/web/parliament/journal01/694-artical01.html)
    , 林佳龍國會通訊, 2013/2
14. [為台中找出路－「大台中發展高峰會」紀要](http://www.taiwanthinktank.org/chinese/page/5/61/2767/0),
    台灣智庫, 2013/12/21
15. [挺立院中遷 小英：高鐵站設會展中心](http://news.ltn.com.tw/news/local/paper/533116),
    [自由時報](../Page/自由時報.md "wikilink"), 2011-10-21
16. [林佳龍：借鏡韓国「行政首都」
    打造台中為宜居城市](http://www.citylove.org.tw/web/parliament/44-press/949-2014-02-14-14-34-38.html)
     2014-02-13