****（Proserpina）是第26颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1853年5月5日发现。的[直径为](../Page/直径.md "wikilink")95.1千米，[质量为](../Page/质量.md "wikilink")9.0×10<sup>17</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1581.184天。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1853年发现的小行星](../Category/1853年发现的小行星.md "wikilink")