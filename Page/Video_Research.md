**Video Research**（，Video Research Ltd.
[縮寫](../Page/縮寫.md "wikilink")**VR**）是[日本一家進行](../Page/日本.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")[收視率與](../Page/收視率.md "wikilink")[廣播節目](../Page/廣播節目.md "wikilink")[收聽率調查的公司](../Page/收聽率.md "wikilink")。

## 历史

1962年由[電通及](../Page/電通.md "wikilink")[民間放送](../Page/商業廣播.md "wikilink")[核心局出資設立](../Page/核心局.md "wikilink")。除了[山梨縣](../Page/山梨縣.md "wikilink")、[福井縣](../Page/福井縣.md "wikilink")、[德島縣](../Page/德島縣.md "wikilink")、[佐賀縣及](../Page/佐賀縣.md "wikilink")[宮崎縣外](../Page/宮崎縣.md "wikilink")，日本42個[都道府縣有電視收視率調查](../Page/都道府縣.md "wikilink")。

## 數據

### 本部

  - 〒102-0075
    [東京都](../Page/東京都.md "wikilink")[千代田区三番町](../Page/千代田区.md "wikilink")6-17

### 支部

  - [關西](../Page/近畿地方.md "wikilink")（[大阪市](../Page/大阪市.md "wikilink")）、
  - [中部](../Page/中部地方.md "wikilink")（[名古屋市](../Page/名古屋市.md "wikilink")）、
  - [九州](../Page/九州_\(日本\).md "wikilink")（[福岡市](../Page/福岡市.md "wikilink")）、
  - [北海道](../Page/北海道.md "wikilink")（[札幌市](../Page/札幌市.md "wikilink")）、
  - [东北](../Page/东北地方.md "wikilink")（[仙台市](../Page/仙台市.md "wikilink")）、
  - [中國](../Page/中国地方.md "wikilink")（[廣島市](../Page/廣島市.md "wikilink")）

### 營業所

  - [静岡](../Page/静岡.md "wikilink")（[静岡市](../Page/静岡市.md "wikilink")）、
  - [岡山](../Page/岡山縣.md "wikilink")（[岡山市](../Page/岡山市.md "wikilink")）、
  - [信越](../Page/信越.md "wikilink")（[新潟市](../Page/新潟市.md "wikilink")）

### 設立年月日

  - 1962年9月20日

### 資金

  - 2億2050萬日圓

### 社長

  - 代表取締役社長 [若杉五馬](../Page/若杉五馬.md "wikilink")

### 社員數

  - 382名（2006年4月）

### 股東

  - [東京放送](../Page/東京放送.md "wikilink")
  - [日本電視台](../Page/日本電視台.md "wikilink")
  - [富士電視台](../Page/富士電視台.md "wikilink")
  - [朝日電視台](../Page/朝日電視台.md "wikilink")
  - [東京電視台](../Page/東京電視台.md "wikilink")
  - [每日放送](../Page/每日放送.md "wikilink")
  - [朝日放送](../Page/朝日放送.md "wikilink")
  - [讀賣電視台](../Page/讀賣電視台.md "wikilink")
  - [關西電視台](../Page/關西電視台.md "wikilink")
  - [中部日本放送](../Page/中部日本放送.md "wikilink")
  - [東海電視台](../Page/東海電視台.md "wikilink")
  - [名古屋電視台](../Page/名古屋電視台.md "wikilink")
  - [中京電視台](../Page/中京電視台.md "wikilink")
  - [RKB每日放送](../Page/RKB每日放送.md "wikilink")
  - [九州朝日放送](../Page/九州朝日放送.md "wikilink")
  - [西日本電視台](../Page/西日本電視台.md "wikilink")
  - [福岡放送](../Page/福岡放送.md "wikilink")
  - [北海道放送](../Page/北海道放送.md "wikilink")
  - [札幌電視台](../Page/札幌電視台.md "wikilink")
  - [北海道電視台](../Page/北海道電視台.md "wikilink")
  - [北海道文化放送](../Page/北海道文化放送.md "wikilink")
  - [東北放送](../Page/東北放送.md "wikilink")
  - [中国放送](../Page/中国放送.md "wikilink")
  - [東芝Solutions](../Page/東芝Solutions.md "wikilink")
  - [電通](../Page/電通.md "wikilink")
  - [博報堂](../Page/博報堂.md "wikilink")
  - [大廣](../Page/大廣.md "wikilink")

## 關聯條目

  - [收視率](../Page/收視率.md "wikilink")
  - [電視藝人形象調查](../Page/電視藝人形象調查.md "wikilink")

## 外部連結

  - [Video Research](http://www.videor.co.jp/)

[Category:日本媒體公司](../Category/日本媒體公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:市场研究公司](../Category/市场研究公司.md "wikilink")
[Category:1962年成立的公司](../Category/1962年成立的公司.md "wikilink")