**ARTLAND**（），是以動畫的企劃、製作為主要營業事項的日本企業。曾分拆成兩個公司，負責動畫企劃、製作的是**株式会社ANIMATION
STUDIO
ARTLAND**，而[Marvelous旗下](../Page/Marvelous.md "wikilink")**株式会社ARTLAND**則退居版權管理、後併入母公司。

## 沿革

  - 1978年9月14日，[石黑昇創設動畫工作室](../Page/石黑昇.md "wikilink")**有限会社ARTLAND**（）。
  - 2006年4月3日， [Marvelous
    Entertainment收購](../Page/Marvelous_Entertainment.md "wikilink")，同年7月正式改組為**株式会社ARTLAND**（）\[1\]。
  - 2010年12月1日，以分割公司及[管理層收購方式讓渡股份](../Page/管理層收購.md "wikilink")，將「動畫工作室」商號化、成立**株式会社ANIMATION
    STUDIO ARTLAND**（），移管企劃、製作部門承繼業務，依舊由石黑昇主掌\[2\]。
  - 2015年4月1日，株式会社ARTLAND併入母公司[Marvelous](../Page/Marvelous.md "wikilink")，創始公司法人主體消滅\[3\]。
  - 2016年4月接受中國動畫公司繪夢的注資，取得了51%的股份。
  - 2017年6月30日，因負債2億9884萬日元而委任律師處理債務事宜\[4\]。
  - 2017年7月12日，繪夢日本發表公告，將持有的Artland 51%股份轉讓給日本動畫監督平澤久義擔任社長的公司株式會社LEVELS。

## 作品列表

### 原請製作

  - [無限地帶23](../Page/無限地帶23.md "wikilink")（OVA，1985年）
  - [星猫フルハウス](../Page/星猫フルハウス.md "wikilink")（OVA，1989年）
  - [勇午](../Page/勇午.md "wikilink")（TV，2003年）
  - [搞笑漫畫日和](../Page/搞笑漫畫日和.md "wikilink")（TV，2005年）
  - [蟲師](../Page/蟲師.md "wikilink")（TV，2005年－2006年）
      - 蟲師 續章（TV，2014年4月及10月）
  - [我們的存在](../Page/我們的存在.md "wikilink")（TV，2006年）
  - [搞笑漫畫日和2](../Page/搞笑漫畫日和.md "wikilink")（TV，2006年）
  - [Happiness\!](../Page/Happiness!.md "wikilink")（TV，2006年）
  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!_\(動畫\).md "wikilink")（TV，2006年）
  - [青空下的约定 ～欢迎来到鸫寮～](../Page/青空下的約定.md "wikilink")（TV，2007年）
  - [GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")（TV，2007年）
  - [GUNSLINGER GIRL -IL
    TEATRINO-](../Page/神槍少女.md "wikilink")（TV，2008年）
  - [伯爵與妖精](../Page/伯爵與妖精.md "wikilink")（TV，2008年）
  - [泰坦尼亞](../Page/泰坦尼亞.md "wikilink")（TV，2008年）
  - [最後大魔王](../Page/最後大魔王.md "wikilink")（TV，2010年）
  - [侦探歌剧 少女福尔摩斯系列](../Page/侦探歌剧_少女福尔摩斯.md "wikilink")
      - 侦探歌剧 少女福尔摩斯 Summer Special （2011年、与J.C.STAFF共同制作）
      - 侦探歌剧 少女福尔摩斯 第2幕 （2012年、与J.C.STAFF共同制作）

<table>
<thead>
<tr class="header">
<th><p>中文名稱</p></th>
<th><p>日文名稱</p></th>
<th><p>播出時間</p></th>
<th><p>導演</p></th>
<th><p>原作類別</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2015年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小森同學拒絕不了！.md" title="wikilink">小森同學拒絕不了！</a></p></td>
<td></td>
<td><p>10月4日－12月20日</p></td>
<td><p><a href="../Page/今泉賢一.md" title="wikilink">今泉賢一</a></p></td>
<td><p>漫畫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/sin_七大罪.md" title="wikilink">sin 七大罪</a></p></td>
<td></td>
<td><p>4月14日－7月29日</p></td>
<td><p><a href="../Page/吉本欣司.md" title="wikilink">吉本欣司</a></p></td>
<td><p>原創</p></td>
<td><p>與<a href="../Page/TNK.md" title="wikilink">TNK共同製作</a></p></td>
</tr>
</tbody>
</table>

### 參與製作

  - [魯邦三世](../Page/魯邦三世.md "wikilink")（第2作、作画協力、1977年－1980年）
  - [原子小金剛](../Page/原子小金剛.md "wikilink")（第2作、1980年－1981年）
  - [戦闘メカ ザブングル](../Page/戦闘メカ_ザブングル.md "wikilink")（作画協力、）
  - [雷鳥2086](../Page/雷鳥2086.md "wikilink")（TV、1982年）
  - [超時空要塞](../Page/超時空要塞.md "wikilink")（TV、1982年－1983年）
  - [超時空世紀](../Page/超時空世紀.md "wikilink")（TV、1983年－1984年）
  - [超時空要塞マクロス
    愛・おぼえていますか](../Page/超時空要塞マクロス_愛・おぼえていますか.md "wikilink")（電影、1984年）
  - [搞怪拍檔](../Page/搞怪拍檔.md "wikilink")（TV、作画協力、1985年）
  - メガゾーン23 PART II　秘密く・だ・さ・い（OVA、1986年）
  - [銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")（OVA、1988年）
  - [機動戰士GUNDAM
    逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（動画協力、1988年）
  - [少年アシベ](../Page/少年アシベ.md "wikilink")（TV、1991年）
  - [橘夢日記](../Page/橘夢日記.md "wikilink")（TV、1992年－1993年）
  - [ジェノサイバー](../Page/ジェノサイバー.md "wikilink")（OVA、1993年）
  - [為食龍少爺](../Page/為食龍少爺.md "wikilink")（TV、1993年－1994年）
  - [飛天少女豬](../Page/飛天少女豬.md "wikilink")（TV、1994年－1995年）
  - ヘヴィ
  - ぶっちぎり
  - [バブルガムクラッシュ\!](../Page/バブルガムクラッシュ!.md "wikilink")（[吹泡糖危機的續篇](../Page/吹泡糖危機.md "wikilink")）
  - 迷走王ボーダー
  - ハード&ルーズ
  - 物部ドクトリン物語
  - [リトルダイナソー](../Page/リトルダイナソー.md "wikilink")
  - 私立探偵土岐正造トラブルハート
  - 青き炎
  - 恋子の毎日
  - Southern Wind
  - キメラ
  - 余命半年
  - [バケツでごはん](../Page/バケツでごはん.md "wikilink")（TV、1996年）
  - [星方天使](../Page/星方天使.md "wikilink")（TV、1999年）
  - [沉默的未知](../Page/沉默的未知.md "wikilink")（TV、2000年）
  - [銀河遊俠](../Page/銀河遊俠.md "wikilink")（TV、2001年）
  - [魯莽天使](../Page/魯莽天使.md "wikilink")（TV、2002年－2003年）
  - [火影忍者](../Page/火影忍者.md "wikilink")（動画協力、2002年－2007年）
  - [Chobits](../Page/Chobits.md "wikilink")（TV、2002年）
  - [京極夏彥 巷說百物語](../Page/巷說百物語.md "wikilink")（TV、2003年）
  - [槍墓](../Page/槍墓.md "wikilink")（TV、2003年）
  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")（TV、2004年）
  - [絕對少年](../Page/絕對少年.md "wikilink")（TV、2005年）
  - [劇場版×××HOLiC
    真夏ノ夜ノ夢](../Page/×××HOLiC.md "wikilink")（劇場作品、動画協力、2005年）
  - [獸王星](../Page/獸王星.md "wikilink")（TV、動画協力、2006年）
  - [櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")（TV、動画協力、2006年）
  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（TV、動画協力、2006年－）
  - [天保異聞 妖奇士](../Page/天保異聞_妖奇士.md "wikilink")（TV、第二原画協力、2006年－2007年）
  - [藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")（TV、仕上協力、2007年）
  - [黑之契約者](../Page/黑之契約者.md "wikilink")（TV、動画協力、2007年）
  - [天翔少女](../Page/天翔少女.md "wikilink")（TV、仕上協力、2007年）
  - [スケッチブック 〜full
    color's〜](../Page/Sketchbook.md "wikilink")（TV、各話第二原画、2007年）
  - [神靈狩/GHOST
    HOUND](../Page/神靈狩/GHOST_HOUND.md "wikilink")（TV、各話動画、2007年）
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")（TV、各話動画、2007年）

## 關連項目

  - [日本動畫工作室列表](../Page/日本動畫工作室列表.md "wikilink")

## 參考資料

## 外部連結

  - [官方網站](http://www.artland.co.jp/)

  - [製作探訪](http://www.style.fm/as/13_special/pro_070109a.shtml)

[Category:绘梦动画](../Category/绘梦动画.md "wikilink")
[\*](../Category/ARTLAND.md "wikilink")
[Category:日本動畫工作室](../Category/日本動畫工作室.md "wikilink")
[Category:東京都公司](../Category/東京都公司.md "wikilink")
[Category:武藏野市](../Category/武藏野市.md "wikilink")
[Category:1978年成立的公司](../Category/1978年成立的公司.md "wikilink")

1.
2.
3.
4.