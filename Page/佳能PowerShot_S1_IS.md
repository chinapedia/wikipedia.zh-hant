**[佳能](../Page/佳能.md "wikilink") PowerShot S1
IS**这部320万像素的[数码相机发布于](../Page/数码相机.md "wikilink")2004年2月9日，它是佳能长焦相机类别中的第二部相机，整合有10倍[光学变焦镜头和](../Page/光学变焦镜头.md "wikilink")[IS影像稳定系统](../Page/IS影像稳定系统.md "wikilink")。它的前一个型号是[Canon
PowerShot
Pro90一部也拥有](../Page/Canon_PowerShot_Pro90.md "wikilink")10倍光学变焦镜头和IS影像稳定系统的价值1500美元的数码单反相机，而且只有260万像素。

The PowerShot S1 IS保留了一些源自于佳能[G
系列相机的设计特点](../Page/Canon_PowerShot_G.md "wikilink")，不过机身主要由两种色调的银色塑料和一些金属以圆角勾勒出来。它是第一部能够提供30帧每秒VGA分辨率画质短片的相机，不过文件大小被限制在1GB（虽然能够生成多个1GB大小的文件）。
由于感光元件的区别，没有一架[数码单反相机能够拍摄短片](../Page/数码单反相机.md "wikilink")。

S1 IS并不是一部小型数码相机，虽然有着关联因素，但是它的宽大的手柄区域和圆的顶部使得它看起来比[Canon PowerShot
G5大好多](../Page/Canon_PowerShot_G5.md "wikilink")

## 特性

  - 320万像素
  - 1/2.7 (5.27 x 3.96 mm) CCD 感光元件
  - 10x 光学变焦（总共大约 32x 变焦）
  - 光学影像稳定系统
  - 超声波马达
  - 超长的VGA分辨率有声短片拍摄功能
  - 带有iSAPS功能的佳能DIGIC处理器
  - 支援[PictBridge协议以及佳能直接打印功能](../Page/PictBridge.md "wikilink") -
    无需个人电脑
  - 13种拍摄模式
  - 光圈范围：F2.8/F3.1 到 F8
  - 微距范围：10 cm
  - 最小快门：1/2000 s，最大快门：15 s
  - 640 x 480 15/30 帧 无限制有声短片
  - 电子取景器
  - 连拍功能：1.7 张每秒 最多24张照片。同时也支持间隔拍摄
  - 1.5 "（翻转式）LCD（11.4万像素）
  - 尺寸：111 x 78 x 66 mm（4.4 x 3.1 x 2.6 英寸）
  - 重量：469 g

## 外部链接

  - [Canon Inc.](http://www.canon.com)
  - [Canon's S1 IS product information
    page](http://web.canon.jp/Imaging/pss1is/index-e.html)

## 图片展览

<File:Canon> Powershot S1 IS Front View 3000px.jpg <File:Canon>
Powershot S1 IS Top View 3000px.jpg <File:Canon> Powershot S1 IS Rear
View 2900px.jpg <File:Canon> Powershot S1 IS Side View 2400px.jpg
<File:Canon> Powershot S1 IS Side Door View 2000px.jpg

[Category:佳能相機](../Category/佳能相機.md "wikilink")