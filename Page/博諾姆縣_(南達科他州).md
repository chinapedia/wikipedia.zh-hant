**博諾姆縣**（）是[美國](../Page/美國.md "wikilink")[南達科他州東南部的一個縣](../Page/南達科他州.md "wikilink")，南隔[密蘇里河與](../Page/密蘇里河.md "wikilink")[內布拉斯加州相望](../Page/內布拉斯加州.md "wikilink")。面積1,506平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口7,260人。縣治[廷德爾](../Page/廷德爾_\(南達科他州\).md "wikilink")（Tyndall）。

成立於1862年4月5日。縣名來自密蘇里河上的一個島
（島名是[法語](../Page/法語.md "wikilink")，直譯為「好人」，即「好人雅克」，與美國人的[山姆大叔同義](../Page/山姆大叔.md "wikilink")）。\[1\]

## 参考文献

[Category:南達科他州行政區劃](../Category/南達科他州行政區劃.md "wikilink")
[博諾姆縣_(南達科他州)](../Category/博諾姆縣_\(南達科他州\).md "wikilink")
[Category:1862年達科他領地建立](../Category/1862年達科他領地建立.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.