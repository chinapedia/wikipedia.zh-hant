**RPD**（[英语全寫](../Page/英语.md "wikilink")：****，[俄语](../Page/俄语.md "wikilink")：****，**РПД**，意思是「[捷格佳廖夫](../Page/瓦西里·捷格佳廖夫.md "wikilink")[轻机枪](../Page/轻机枪.md "wikilink")」）是一种由[瓦西里·捷格佳廖夫](../Page/瓦西里·捷格佳廖夫.md "wikilink")（）设计，[苏联制的](../Page/苏联.md "wikilink")[7.62×39毫米](../Page/7.62×39mm.md "wikilink")[轻机枪](../Page/轻机枪.md "wikilink")。它是用於取代苏联[7.62×54毫米](../Page/7.62×54mmR.md "wikilink")[DP輕機槍](../Page/DP輕機槍.md "wikilink")。长时间里它是[华沙条约组织的制式轻机枪](../Page/华沙条约组织.md "wikilink")，並為蘇聯戰後的第一代班支援武器。

## 歷史

苏联槍械設計師[瓦西里·捷格佳廖夫早在](../Page/瓦西里·捷格佳廖夫.md "wikilink")1943年就已经设计了RPD轻机枪。1944年初在二战中少数RPD轻机枪在对德国的前线进行了测试，并被限量使用。由于当时[第二次世界大战正在进行](../Page/第二次世界大战.md "wikilink")，一直到战后才大规模装备苏联部队。RPD亦是第一把使用7.62×39毫米子弹的[机枪](../Page/机枪.md "wikilink")，與[SKS及](../Page/SKS半自動步槍.md "wikilink")[AK-47所使用的彈藥相同](../Page/AK-47突击步枪.md "wikilink")。

## 设计

### 造型

RPD擁有兩根可以疊起來的[兩腳架](../Page/兩腳架.md "wikilink")。其彈藥從彈鼓中透過一條100發子彈的金屬[彈鏈輸送](../Page/彈鏈.md "wikilink")。[彈鼓裝在](../Page/彈鼓.md "wikilink")[機匣下方](../Page/機匣.md "wikilink")，彈鏈從左邊進入機匣。RPD使用[7.62×39毫米子彈](../Page/7.62×39mm.md "wikilink")，但因使用專門的金屬彈鏈來給彈，並無法直接使用一般步槍的彈匣。[槍托和手柄是](../Page/槍托.md "wikilink")[木製的](../Page/木材.md "wikilink")，其餘部分是[钢製的](../Page/钢.md "wikilink")。在致動機制方面，RPD採用瓦斯氣壓傳動式，在槍機左右兩側各有一突耳，利用這兩個突耳，使槍機與槍機容納部完成閉合，屬於典型的狄格帖諾夫設計。

### 型号

RPD最初是在苏联制造的，这里它一共有过五个不同的型号，但这些型号没有名称：

  - 最早的型号在子弹进入机枪的孔上还没有[防尘蔽](../Page/防尘蔽.md "wikilink")，但是在照門的右面有一个钮来校准侧风
  - 这个型号也没有防尘蔽，瞄准器旁的钮在其左边
  - 这个型号有一个防尘蔽
  - 第四个型号拥有与第三个型号一样的改善，此外它的气管比较长，枪托有垫子。这个型号有时也被称为
  - 第五个型号如第四个型号，此外防尘蔽加强，在枪托上还装配有多节的[清洁杆](../Page/清洁杆.md "wikilink")

### 其它获得允许生产RPD的国家

[Egyptian_marine_DF-ST-86-08092.jpg](https://zh.wikipedia.org/wiki/File:Egyptian_marine_DF-ST-86-08092.jpg "fig:Egyptian_marine_DF-ST-86-08092.jpg")海軍陸戰隊的RPD。\]\]

  - [56式](../Page/56式班用機槍.md "wikilink")：这个型号是[中国](../Page/中国.md "wikilink")[北方工业生产的](../Page/中国北方工业公司.md "wikilink")，相当于第三型
  - [56-1式](../Page/56式班用機槍#衍生型.md "wikilink")：这个型号也是中国北方工业生产的，相当于第五型
  - 62式：这个型号在[朝鲜民主主义人民共和国生产过](../Page/朝鲜民主主义人民共和国.md "wikilink")

### 普及

[Russian_RPDs_DM-SD-04-06986.JPEG](https://zh.wikipedia.org/wiki/File:Russian_RPDs_DM-SD-04-06986.JPEG "fig:Russian_RPDs_DM-SD-04-06986.JPEG")
[Marine_RPD_machine_gun.jpg](https://zh.wikipedia.org/wiki/File:Marine_RPD_machine_gun.jpg "fig:Marine_RPD_machine_gun.jpg")士兵使用RPD\]\]
[PFLP-group-1969.jpg](https://zh.wikipedia.org/wiki/File:PFLP-group-1969.jpg "fig:PFLP-group-1969.jpg")的成員與RPD\]\]

最早使用RPD的是苏联，后来它被出口，比如出口给[越南](../Page/越南.md "wikilink")，在[越南战争中被](../Page/越南战争.md "wikilink")[北越军队使用](../Page/北越.md "wikilink")。在大多数军队中RPD被[RPK](../Page/RPK輕機槍.md "wikilink")、[RPK-74或类似的机枪取代](../Page/RPK-74輕機槍.md "wikilink")，但一些[第三世界国家至今依然使用RPD](../Page/第三世界国家.md "wikilink")。

RPD在[中國](../Page/中國.md "wikilink")、[朝鲜等部分國家都獲授權生產或私自仿製及使用](../Page/朝鲜.md "wikilink")，[埃及也拥有制造和出口RPD的许可](../Page/埃及.md "wikilink")。

## 使用國

[Bangladeshi_soldiers_on_a_BTR-80_APC.jpg](https://zh.wikipedia.org/wiki/File:Bangladeshi_soldiers_on_a_BTR-80_APC.jpg "fig:Bangladeshi_soldiers_on_a_BTR-80_APC.jpg")[裝甲運兵車上手持RPD輕機槍的孟加拉士兵](../Page/裝甲運兵車.md "wikilink")\]\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - —仿製成[56式班用機槍](../Page/56式班用機槍.md "wikilink")。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - —仿製成62式輕機槍。

  -
  -
  -
  -
  -
  - —至今仍有庫存。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 前使用國

  -
  - —於1960年代被[RPK輕機槍所取代](../Page/RPK輕機槍.md "wikilink")。

## 注釋

<div class="references-small">

<references />

</div>

## 参考文献

  -
## 參考

  - [56式班用機槍](../Page/56式班用機槍.md "wikilink")
  - [RPK](../Page/RPK輕機槍.md "wikilink")
  - [RPK-74](../Page/RPK-74輕機槍.md "wikilink")
  - [FN Minimi](../Page/FN_Minimi輕機槍.md "wikilink")／[M249
    SAW](../Page/M249班用自動武器.md "wikilink")
  - [PK／PKM](../Page/PK通用機槍.md "wikilink")
  - [班用機槍](../Page/轻机枪.md "wikilink")
  - [俄羅斯槍械列表](../Page/俄羅斯槍械列表.md "wikilink")

## 外部链接

  - —[Modern
    Firearms—RPD](https://web.archive.org/web/20041207014830/http://world.guns.ru/machine/mg14-e.htm)

[Category:輕機槍](../Category/輕機槍.md "wikilink")
[Category:7.62×39毫米槍械](../Category/7.62×39毫米槍械.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")