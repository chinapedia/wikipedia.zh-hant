此一條目提供用來描述[鱗翅目](../Page/鱗翅目.md "wikilink")[物種的一些專用術語](../Page/物種.md "wikilink")。
[Butterfly_parts-zh-hk.JPG](https://zh.wikipedia.org/wiki/File:Butterfly_parts-zh-hk.JPG "fig:Butterfly_parts-zh-hk.JPG")
如同其他的[昆蟲一般](../Page/昆蟲.md "wikilink")，成蝶的身體可分成三個部份－頭部（head）、胸部（thorax）和腹部（abdomen）。頭部有眼睛（eyes）、口器（mouth
parts）和觸角（antennae）。口器有由下顎（maxillae）中形成的吻管（proboscis）。腳（legs）和翅膀（wings）由胸部中長出。腹部沒有任何附加的器官。位於前翅（fore
wings）和後翅（hind
wings）上的翅脈（venation）和鱗片（scale）的顏色和圖樣通常是辨識時第一個使用的特徵。但在某些特定的物種中，只能依靠生殖器（genitalia）的構造和分子生物的技術來分辨。

## 頭部和身體上的術語

[Lepidoptera_head_gfhampson.jpg](https://zh.wikipedia.org/wiki/File:Lepidoptera_head_gfhampson.jpg "fig:Lepidoptera_head_gfhampson.jpg")

  - **[觸角](../Page/觸角.md "wikilink")（Antennae）**
    位於頭部，且接近眼睛。觸角和頭髮一樣沒有觸覺，但其用途在於感知空氣中的香味和氣流。在[真蝶總科](../Page/真蝶總科.md "wikilink")（Papilionoidea）裡，觸角末端有一個突出的小塊，做叫端感棒（clubs）；但在[弄蝶總科](../Page/弄蝶總科.md "wikilink")（Hesperioidea）裡，牠們的觸角末端會勾起來，而端感棒正好在勾起來部位的前面。在一些如[擬蛾小灰蝶屬](../Page/擬蛾小灰蝶屬.md "wikilink")（Liphyra）的[灰蝶科](../Page/灰蝶科.md "wikilink")（Lycaenidae）裡，其觸角會由頭到尾逐漸變細。

  - **鏈形（Catenulated）**: 有環狀外表的觸角。

  -

    <div id="palpi">

    **[觸鬚](../Page/觸鬚.md "wikilink")（Palpi）**: 有時看起來像是喙的口器。

  - **[氣孔](../Page/氣孔_\(動物\).md "wikilink")（Spiracle）** :
    長在胸部和腹部，允許空氣進入[氣管的呼吸道開口](../Page/氣管.md "wikilink")。

## 翅膀上的術語

[Butterfly_wing_terms-zh-hk.jpg](https://zh.wikipedia.org/wiki/File:Butterfly_wing_terms-zh-hk.jpg "fig:Butterfly_wing_terms-zh-hk.jpg")

  - **翅頂（Apex）**：翅膀頂部的區域。
  - **翅基（Base）**：接近和胸部相接點的區域。
  - **性標（Brands）**：由會發出特別氣味的鱗片，即發香鱗（androconia）所組成的區域，在某些物種的雄蝶身上會發現。
  - **翅室（Cell）**：被翅脈圍住的中央區域，可能被翅脈緊閉著，或有開縫。
  - **纖毛（Cilia）**：翅膀邊緣纖細的毛。
  - **肋脈（Costa）**：翅膀的前端。
  - **翅盤（Disc）**：穿過翅室的中央條紋。
  - **背部（Dorsum）**翅膀的末緣。
  - **翅帶（Fascia）**：指不同顏色斑點所組成的帶狀。
  - **眼點（Eyespot）**或**單眼（Ocelli）**：似哺乳動物眼睛的斑點。此一名詞亦可被用來單純地指眼睛。
  - **間隙（Interspace）**：相鄰鄰翅脈間的區域。
  - **新月片（Lunule）**：通常出現在邊緣的新月斑。
  - **翅痣（Stigma）**：在某些蛾的前翅上很顯著的翅室。其大小、形狀和顏色可以用來辨識某些物種。
  - **線紋（Strigae）**：指細線狀的圖樣。
  - **末端的（Terminal）**和**邊緣的（marginal）**：沿著邊緣的。
  - **末端（Termen）**：翅膀距身體最遠的一端。
  - **翅臀（Tornus）**：翅膀的後端。
  - **翅脈（Vein）**：由翅膀的上緣和下緣長出的細窄架構，提供翅膀堅固性和韌性。

（另見[昆蟲翅膀和](../Page/昆蟲翅膀.md "wikilink")[康尼脈序系統](../Page/康尼脈序系統.md "wikilink")）

## 幼蟲和蛹期間的術語

**1.** 頭部（Head） **2.** 胸部（Thorax） **3.** 腹部（Abdomen） **4.** 氣孔（Spiracle）
**5.** 尾把握器（Anal clasper） **6.** 腹足（Proleg） **7.** 體節（Segment） **8.**
胸足（Thoracic leg） **9.** 觸角（Antenna） \]\]

  - **懸絲（Cremaster）**：大多數蝴蝶的[蛹會有一個由](../Page/蛹.md "wikilink")[毛毛蟲編成的絲墊和樹面或地面等表面接觸](../Page/毛毛蟲.md "wikilink")，並會有一個鉤（懸絲）在蛹腹部的上端。
  - **絲腰（Girdle）** :
    一串用來支撐蛹的細絲，尤其出現在[鳳蝶科的蝴蝶上](../Page/鳳蝶科.md "wikilink")。
  - **[臭角](../Page/臭角.md "wikilink")（Osmeterium）**某些幼蟲的肉質組織，通常會散發出刺鼻的化學物質。
  - **腹足（Proleg）**：由毛毛蟲腹部體節中長出來的肉質的腳，末端有鉤。

## 另見

  - [蝴蝶](../Page/蝴蝶.md "wikilink")
  - [毛毛蟲](../Page/毛毛蟲.md "wikilink")
  - [蛹](../Page/蛹.md "wikilink")

[Category:術語列表](../Category/術語列表.md "wikilink")
[Category:鱗翅目](../Category/鱗翅目.md "wikilink")