**優朵拉**是一個[Windows和](../Page/Windows.md "wikilink")[Mac
OS上的](../Page/Mac_OS.md "wikilink")[電子郵件客戶端軟體](../Page/電子郵件客戶端.md "wikilink")，是美國手機晶片大廠[高通公司](../Page/高通公司.md "wikilink")（Qualcomm）的早期產品，另外有個同名的郵件伺服器程式「
Eudora Internet Mail Server」。

優朵拉最初由Jeff Beckley、JuliaBlumin和Jerry
Pickering所共同發展，後來由高通公司繼續發展為商用軟體並且推出Windows版，優朵拉在90年代曾經流行過一陣子，後來被[Lotus
Notes](../Page/Lotus_Notes.md "wikilink")、[Microsoft
Outlook等軟體所取代](../Page/Microsoft_Outlook.md "wikilink")。優朵拉目前核心碼完全改用[Mozilla
Thunderbird的程式碼](../Page/Mozilla_Thunderbird.md "wikilink")，並成為[自由軟體](../Page/自由軟體.md "wikilink")。

2018年5月22日，[计算机历史博物馆自](../Page/计算机历史博物馆.md "wikilink")[高通處取得](../Page/高通.md "wikilink")
優朵拉 之所有權，此前雙方進行了長達 5 年的談判。\[1\]

## 參考文獻

<references />

## 外部連結

  - [官方網頁](http://www.eudora.com/)

[Category:电子邮件客户端](../Category/电子邮件客户端.md "wikilink")

1.   Computer History
    Museum|accessdate=2018-05-23|work=www.computerhistory.org|language=en-US}}