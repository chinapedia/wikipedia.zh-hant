**草酸**（英文：**Oxalic
acid**，全称**酢浆草酸**），也称**乙二酸**，是一種強[有機酸](../Page/有機酸.md "wikilink")，[化學式為H](../Page/化學式.md "wikilink")<sub>2</sub>C<sub>2</sub>O<sub>4</sub>。常见的草酸通常含有两分子的[结晶水](../Page/结晶水.md "wikilink")（H<sub>2</sub>C<sub>2</sub>O<sub>4</sub>·2H<sub>2</sub>O）。草酸在[菠菜和植物](../Page/菠菜.md "wikilink")[大黃中廣泛存在](../Page/大黃.md "wikilink")。

## 物理性質

乙二酸是一種無水透明晶體或粉末，味酸，易溶於[乙醇](../Page/乙醇.md "wikilink")，溶於[水](../Page/水.md "wikilink")，微溶於[乙醚](../Page/乙醚.md "wikilink")，不溶於[苯](../Page/苯.md "wikilink")。

## 化学性质

### 酸性

乙二酸的[酸性比](../Page/酸性.md "wikilink")[醋酸強](../Page/醋酸.md "wikilink")10,000倍\[1\]，可以使[碳酸钠](../Page/碳酸钠.md "wikilink")（Na<sub>2</sub>CO<sub>3</sub>）分解。乙二酸的一級電離常數5.6×10<sup><small>-2</small></sup>，二級電離常數5.4×10<sup><small>-5</small></sup>。

### 不穩定性

乙二酸受热分解有[CO](../Page/一氧化碳.md "wikilink")、[CO<sub>2</sub>和](../Page/二氧化碳.md "wikilink")[H<sub>2</sub>O生成](../Page/水.md "wikilink")。

  -
    HOOC-COOH → CO + CO<sub>2</sub> + H<sub>2</sub>O

实验室可以用这个反应制取CO。

### 還原性

乙二酸有很強的[還原性](../Page/還原性.md "wikilink")，能被氧化劑氧化成CO<sub>2</sub>。乙二酸可以使[KMnO<sub>4</sub>褪色](../Page/高锰酸钾.md "wikilink")，這個反應可以用來滴定KMnO<sub>4</sub>濃度，可以洗去溅在布条上的墨水迹。

### 酯化反应

乙二酸可以跟[醇反应生成](../Page/醇.md "wikilink")[酯](../Page/酯.md "wikilink")。比如乙二酸跟[乙醇反应生成](../Page/乙醇.md "wikilink")[乙二酸二乙酯](../Page/乙二酸二乙酯.md "wikilink")。

### 草酸的电离

尽管是一种[羧基酸](../Page/羧基酸.md "wikilink")，草酸相对来说為强酸：
:C<sub>2</sub>O<sub>4</sub>H<sub>2</sub>
C<sub>2</sub>O<sub>4</sub>H<sup>−</sup> + H<sup>+</sup>;
p*K*<sub>a</sub> = 1.27

  -
    C<sub>2</sub>O<sub>4</sub>H<sup>−</sup>
    C<sub>2</sub>O<sub>4</sub><sup>2−</sup> + H<sup>+</sup>;
    p*K*<sub>a</sub> = 4.27

### 键长特殊性

在草酸和[草酸盐的已测定的数十种结构中](../Page/草酸盐.md "wikilink")，其中的C—C[键长都要比典型的共价](../Page/键长.md "wikilink")[单键键长值大](../Page/单键.md "wikilink")（约152\~156pm）。草酸分子中，存在单双键交替的体系，分子为平面构型，满足产生[共轭作用的条件](../Page/共轭.md "wikilink")，也已成功經过X射線衍射法测定变形电子密度图证明存在离域[π键](../Page/π键.md "wikilink")。但量子化学计算结果说明两个碳原子间的4个全充满电子的π[分子轨道交替地成为成键轨道和反键轨道](../Page/分子轨道.md "wikilink")，[π轨道对成键的贡献很小](../Page/π轨道.md "wikilink")，π键[键级仅剩余](../Page/键级.md "wikilink")0.015。

## 製備

  -
    4 ROH + 4 CO + O<sub>2</sub> → 2(CO<sub>2</sub>R)<sub>2</sub> + 2
    H<sub>2</sub>O

## 用途

在化学工业上用以制造[季戊四醇](../Page/季戊四醇.md "wikilink")、[草酸钴](../Page/草酸钴.md "wikilink")、[草酸镍](../Page/草酸镍.md "wikilink")、[碱性品绿](../Page/碱性品绿.md "wikilink")，[钢铁](../Page/钢铁.md "wikilink")、土壤分析成套试剂、化学试剂等。快速染料用作显色助染剂。稻草、麦杆制品的[漂白剂](../Page/漂白剂.md "wikilink")（草酸有[还原性](../Page/还原性.md "wikilink")），铁锈污染消除剂（草酸与铁作用，生成可溶性的[草酸铁](../Page/草酸铁.md "wikilink")，容易被水洗去，故可除去织物上所沾染的铁迹）。

## 安全性

草酸是一種存在於許多植物的酸，但也是一種[腎毒性和](../Page/腎毒性.md "wikilink")[腐蝕性的酸性有毒物質](../Page/腐蝕性.md "wikilink")。純草酸的[半致死劑量](../Page/半致死劑量.md "wikilink")（），以對大鼠的影響作計量，大約為每公斤體重375毫克
\[2\]，換算至一個約65公斤的人，大約需要25公克的份量。另外，口服的[最低致死劑量](../Page/最低致死劑量.md "wikilink")（）約為600mg/kg
\[3\]。草酸有著非常刺激性的味道，長久接觸草酸的水溶液，可引致關節痛。在一般人的尿液裡，亦含有微量草酸，份量大約有數毫克\[4\]。草酸對混凝土、木材及玻璃均具腐蝕性\[5\]。

## 參考資料

## 参见

  - [草酸盐](../Page/草酸盐.md "wikilink")
  - [草酸盐中毒](../Page/草酸盐中毒.md "wikilink")

## 外部連接

  - [Oxalic acid MS
    Spectrum](http://gmd.mpimp-golm.mpg.de/Spectrums/ad304d2f-132a-4bba-8e72-d931e77e1c6e.aspx)

  -
  - [NIOSH Guide to Chemical Hazards
    (CDC)](http://www.cdc.gov/niosh/npg/npgd0474.html)

  - [Table: Oxalic acid content of selected vegetables
    (USDA)](https://web.archive.org/web/20051024031722/http://www.nal.usda.gov/fnic/foodcomp/Data/Other/oxalic.html)

  - [Alternative link: Table: Oxalic Acid Content of Selected Vegetables
    (USDA)](http://www.ars.usda.gov/Services/docs.htm?docid=9444)

  - [About rhubarb poisoning (The Rhubarb
    Compendium)](http://www.rhubarbinfo.com/rhubarb-poison.html)

  - [Oxalosis & Hyperoxaluria Foundation (OHF) The Oxalate Content of
    Food 2008
    (PDF)](https://web.archive.org/web/20110902070929/http://www.ohf.org/docs/Oxalate2008.pdf)

  - [Oxalosis & Hyperoxaluria Foundation (OHF) Diet
    Information](https://web.archive.org/web/20110902020938/http://www.ohf.org/diet.html)

  - [Calculator: Water and solute activities in aqueous oxalic
    acid](http://www.aim.env.uea.ac.uk/aim/accent2/inputpage.php)

[\*](../Category/草酸盐.md "wikilink")
[Category:二羧酸](../Category/二羧酸.md "wikilink")
[Category:家用化学品](../Category/家用化学品.md "wikilink")
[Category:二碳有机物](../Category/二碳有机物.md "wikilink")

1.
2.
3.
4.  Wilhelm Riemenschneider, Minoru Tanifuji "Oxalic Acid" in Ullmann's
    Encyclopedia of Industrial Chemistry, 2002, Wiley-VCH, Weinheim.
5.