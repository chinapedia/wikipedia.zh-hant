**數位多功能影音光碟**（；[縮寫](../Page/縮寫.md "wikilink")：**DVD**），是一種[光碟儲存媒體](../Page/光碟.md "wikilink")，通常用來播放[標清](../Page/標清.md "wikilink")（標準解晰度）的[電影](../Page/電影.md "wikilink")，高音質的[音樂與大容量儲存資料用途](../Page/音樂.md "wikilink")。DVD與[CD或Blu](../Page/CD.md "wikilink")-ray
Disc（[藍光光碟](../Page/藍光光碟.md "wikilink")）的外觀極為相似，直徑有80mm、120mm規格等。

## 歷史

在1990年代早期，有两种高容量光盘标准正在研究阶段；一个是多媒体光盘（MMCD），支持者是[飞利浦和](../Page/飞利浦.md "wikilink")[索尼](../Page/索尼.md "wikilink")。另一个是超高密度光盘（Super
High Density
Disc），支持者分别是[东芝](../Page/东芝.md "wikilink")、[时代华纳](../Page/时代华纳.md "wikilink")、[松下电器](../Page/松下电器.md "wikilink")、[日立](../Page/日立.md "wikilink")、[三菱电机](../Page/三菱电机.md "wikilink")、[先锋](../Page/先锋.md "wikilink")、[汤姆逊和](../Page/汤姆逊.md "wikilink")[JVC](../Page/JVC.md "wikilink")。[IBM则出面希望合并两个标准](../Page/IBM.md "wikilink")，以免1980年代VHS和BETAMAX的标准之战又再出现。后来由于电脑界业者（包括[微软](../Page/微软.md "wikilink")、[英特尔等厂商](../Page/英特尔.md "wikilink")）坚持他们仅支持一种统一的规格，两大阵营于是将标准合并成为DVD，并于1995年推出。

DVD原是**D**igital **V**ideo
**D**isc（数字视频光盘）的首字母缩略字，因初推出时大多厂商只针对视频方面的宣传及推出产品，而且当时的电脑产业对高容量的存储媒体没有太大需求。后因定位更改，于1995年规格正式确立时，重新定义为**D**igital
**V**ersatile **D**isc（数字多用途光盘），但旧称的Digital Video
Disc也有人继续沿用。现在一般都只以“DVD”作为其称呼。

## 用途

[光碟片的發展歷史.jpg](https://zh.wikipedia.org/wiki/File:光碟片的發展歷史.jpg "fig:光碟片的發展歷史.jpg")，[DVD](../Page/DVD.md "wikilink")4.7G，[DVD](../Page/DVD.md "wikilink")8.5G，[VCD](../Page/VCD.md "wikilink")，[錄影帶](../Page/錄影帶.md "wikilink")，[LD](../Page/LD.md "wikilink")，[錄音帶](../Page/錄音帶.md "wikilink")\]\]
和CD不同，DVD於一開始已設計為多用途光碟。原始的DVD規格共有五種子規格：

  - [DVD-ROM](../Page/DVD-ROM.md "wikilink")：用作儲存電腦資料
  - [DVD-Video](../Page/DVD-Video.md "wikilink")：用作儲存影像
  - [DVD-Audio](../Page/DVD-Audio.md "wikilink")：用作儲存音樂
  - [DVD-R](../Page/DVD-R.md "wikilink")：只可寫入一次燒錄碟片
  - [DVD-RAM](../Page/DVD-RAM.md "wikilink")：可重覆寫入燒錄碟片

[DVD_two_kinds.jpg](https://zh.wikipedia.org/wiki/File:DVD_two_kinds.jpg "fig:DVD_two_kinds.jpg")的讀取面
右：[DVD-R的讀取面](../Page/DVD-R.md "wikilink")\]\]
這些標準儲存的內容雖然不同，目錄和檔案排列架構也不同，但除了DVD-RAM外，它們的基層結構是一樣的。即是說，DVD-Video或DVD-Audio都只是DVD-ROM的應用特例，將DVD-Video或DVD-Audio放入電腦的DVD驅動器中都可看到裡面的資料以[檔案的方式儲存著](../Page/檔案.md "wikilink")（但未必能播放）。

由于DVD-RAM的一些缺点及[兼容性不佳](../Page/兼容性.md "wikilink")，另一批廠家成立[DVD+RW联盟](../Page/DVD+RW联盟.md "wikilink")（），推出了兼容性較佳的可多次寫入光碟[DVD+RW和相關的單次寫入光碟](../Page/DVD+RW.md "wikilink")[DVD+R標準](../Page/DVD+R.md "wikilink")。制定DVD標準的DVD論壇為了與DVD+RW联盟競爭，又推出可多次寫入的[DVD-RW](../Page/DVD-RW.md "wikilink")，使用與[CD-RW類似的技術](../Page/CD-RW.md "wikilink")，兼容普通DVD機。目前可写入DVD的标准仍然未统一，普遍的DVD刻录机都兼容双制式（DVD±RW/DVD±R）。亦有支持DVD-RW/DVD+RW/DVD-RAM三种规格的刻录机，称为“[Super
Multi](../Page/Super_Multi.md "wikilink")”刻录机。
[Panasonic_DVD-RAM001.JPG](https://zh.wikipedia.org/wiki/File:Panasonic_DVD-RAM001.JPG "fig:Panasonic_DVD-RAM001.JPG")

## DVD驱动器

## DVD容量規格

計算DVD的容量時，1[GB](../Page/Gibibyte.md "wikilink") 代表 1,000,000,000 bytes
(1000<sup>3</sup> bytes)，而不是慣用的 1[GiB](../Page/GiB.md "wikilink")
(1,073,741,824 bytes, 1024<sup>3</sup>
bytes)，詳見[檔案大小轉換](../Page/檔案大小轉換.md "wikilink")。

### DVD-5

DVD-5是DVD論壇定義的規格，為标称容量4.7GB的*單面單層*DVD片，而燒錄實務上，以簡便方便為記，多半以4480MB省略。

### DVD-9

DVD-9的标称容量為8.5GB的*單面雙層*DVD片，實際7.92GiB。

### DVD-10

DVD-10的标称容量為9.4GB的*雙面單層*DVD片，实际8.75[GiB](../Page/GiB.md "wikilink")。

### DVD-14

DVD-14的标称容量為13.2GB的*單面單層+單面雙層*DVD片。 相較於其他四種容量的片子，較不廣為人知及運用。

### DVD-18

DVD-18的标称容量為17GB的*雙面雙層*DVD片，实际15.8[GiB](../Page/GiB.md "wikilink")。
[DVD_uitleeskop.JPG](https://zh.wikipedia.org/wiki/File:DVD_uitleeskop.JPG "fig:DVD_uitleeskop.JPG")
[DVD_AFM_J_REBIS.png](https://zh.wikipedia.org/wiki/File:DVD_AFM_J_REBIS.png "fig:DVD_AFM_J_REBIS.png")
[DVD_querschnitt.svg](https://zh.wikipedia.org/wiki/File:DVD_querschnitt.svg "fig:DVD_querschnitt.svg")

## 光碟種類與規格

## DVD-Video格式

正常情況下，DVD影碟有以下2個目錄（有時只有VIDEO_TS目錄）：

  - AUDIO_TS:預留給DVD-AUDIO的目錄

<!-- end list -->

  - VIDEO_TS:儲存所有DVD-VIDEO（包括音頻，視訊，字幕等）所有數據的目錄

注意：DVD-AUDIO是指只有AUDIO數據的DVD光碟，我們通常說的DVD影碟是指DVD-VIDEO碟。

一個標準的VIDEO-TS目錄中包含有3種類型的文件 VOB、IFO、BUP

  - VOB文件:視訊目標文件(Video OBjects)

<!-- end list -->

  -
    VOB文件有視訊、聲音、字幕串流組成。視訊串流是[MPEG2格式](../Page/MPEG2.md "wikilink")，音頻串流是
    Dolby Digital
    [AC3或者](../Page/AC3.md "wikilink")[LPCM](../Page/LPCM.md "wikilink")、MPEG2、[MP2](../Page/MPEG-1_Audio_Layer_II.md "wikilink")、[DTS等](../Page/DTS.md "wikilink")，[AC3基本上是事實的標準](../Page/AC3.md "wikilink")，MPEG2多聲道只在極少數2區碟上可以看到（比如In
    the line of
    fire，2區）。MP2-{}-只在非出版級DVD上有等。[PCM是高品質無壓縮數位音頻](../Page/PCM.md "wikilink")，需要的空間太多，並不適合用於DVD電影光碟。AC3的位元速率介於192－448kbps之間，192kbps用於2聲道，384－448kbps用於5.1聲道。
    字幕串流由字幕圖片文件（.sub檔案）和字幕索引文件（.idx檔案）組成，為影片提供[字幕功能](../Page/字幕.md "wikilink")。一個.sub文檔可同時包含多個語言的字幕，此外，在DVD中，字幕是以圖片方式呈現在使用者面前的，即俗稱的「硬字幕」。通常情況下，DVD中不會看到這兩個文件，需要通過[播放軟體從VOB文件中進行分離](../Page/播放軟體.md "wikilink")。

<!-- end list -->

  -
    一個VOB文件可以包含一個主要的視訊串流和幾個多語言聲道和字幕。最大視訊位元速率是9.8Mbps，視訊和音頻位元速率的上限是10Mbps。一張DVD最多可以有9種語言聲道和32種字幕。

<!-- end list -->

  - IFO文件:InFOrmation

<!-- end list -->

  -
    IFO文件告訴DVD機播放資訊：比如章節的開始時間，聲道串流在哪裡，字幕在哪裡。也就是說，VOB文件是影片本身，而對應的IFO文件把組成影片的各個片段有連接起來。這樣DVD機才「懂」得如何播放。

<!-- end list -->

  - BUP文件；備份文件(BackUP)

<!-- end list -->

  -
    和IFO文件完全相同，如果IFO文件出錯的時候，DVD播放機也可以通過BUP來得到相應資訊。從這裡你也可以看到IFO文件的重要性，所以還有相應的BUP備份文件。

一個實際的例子：

⒈VIDEO_TS.IFO/VIDEO_TS.BUP是第一個被播放的檔案。就像程序安裝光碟一樣，第一個要找的就是AUTORUN文件，然後就可以自動執行。DVD機會先找到VIDEO_TS文件，然後再執行對應的操作。通常VIDEO_TS是版權訊息、FBI警告和杜比數位的介紹片段，有時包含選單。（在我們的例子中，VIDEO_TS.IFO/VIDEO_TS.BUP就包含標題選單，選單的圖像文件是"VIDEO_TS.VOB"。）

⒉VTS_01_0.IFO
前兩個數碼是標題數字。例如：VTS_01_\*是標題1，VTS_02_\*是標題二。最多可以有99個標題，每個標題最多可以有10個VOB文件。由於通常都有一個含有選單資料的VTS_XX_0.VOB，因此每一個標題至少會有2個VOB文件。

## DVD-Audio

[DVD-Audio是DVD上的高傳真音效格式](../Page/DVD-Audio.md "wikilink")，他允許多種聲道的選項（從單聲道到7.1聲道）和各種不同的取樣頻率（相對於CDDA的16-bits/44.1
kHz，最高可到24-bits/192
kHz）。相較於[CD格式](../Page/CD.md "wikilink")，DVD可容納更長時間的音樂或更高品質的內容（在取樣頻率和分辨率的提升，以及其容許多聲道的內容）
。

## DVD +/-

  - DVD-R/RW

由[DVD論壇](../Page/DVD論壇.md "wikilink")（DVD Forum）制定。

  - DVD+R/RW

由DVD+RW联盟（）制定，其宣稱DVD+R/RW支援較多功能。\[1\]

## 相關條目

[Comparison_CD_DVD_HDDVD_BD.svg](https://zh.wikipedia.org/wiki/File:Comparison_CD_DVD_HDDVD_BD.svg "fig:Comparison_CD_DVD_HDDVD_BD.svg")

  - [DVD區域碼](../Page/DVD區域碼.md "wikilink")
  - [DVD播放機](../Page/DVD播放機.md "wikilink")
  - [藍光光碟](../Page/藍光光碟.md "wikilink")
      - [DVD驅動器](../Page/DVD驅動器.md "wikilink")
  - [DualDisc](../Page/DualDisc.md "wikilink")
  - [DeCSS](../Page/DeCSS.md "wikilink")
  - [影碟出租店](../Page/影碟出租店.md "wikilink")

## 注釋

[DVD](../Category/DVD.md "wikilink")

1.