**杜興氏肌肉營養不良症**（，縮寫**DMD**）是一種相當嚴重的[性聯遺傳](../Page/性聯遺傳.md "wikilink")[肌肉失養症](../Page/肌肉失養症.md "wikilink")\[1\]。男性病患大約在4歲開始就會產生的症狀，此後症狀即會開始快速惡化\[2\]。通常最先從大腿即骨盆肌肉開始萎縮，之後則是上臂肌肉\[3\]。本病會導致站立困難\[4\]，患者大約在12歲之後就無法行走\[5\]。受影響的肌肉會被[脂肪組織佔據](../Page/脂肪.md "wikilink")，因此看起來會較大塊\[6\]。患者亦常見[脊椎側彎或](../Page/脊椎側彎.md "wikilink")[智能障礙](../Page/智能障礙.md "wikilink")\[7\]。女性患者有時會表現些微症狀\[8\]。

本病屬於\[9\]，約三分之二的患者疾病基因來自遺傳，三分之一則來自新突變\[10\]。疾病相關基因位於X染色體上的DMD基因上，該基因負責了的轉譯\[11\]。失養素則與肌肉[細胞膜的維持有關係](../Page/細胞膜.md "wikilink")\[12\]。在孩子出生前可以進行[基因檢測](../Page/基因檢測.md "wikilink")\[13\]。患者血中的[肌酸激酶值也會較高](../Page/肌酸激酶.md "wikilink")\[14\]。

目前該病尚無有效的治療方法\[15\]。[物理治療](../Page/物理治療.md "wikilink")、輔具，以及手術矯正都能幫助舒緩症狀\[16\]。呼吸肌較弱的患者則可搭配\[17\]。藥物則可使用[皮質類固醇來減緩肌肉退化](../Page/皮質類固醇.md "wikilink")；可以用於控制[癲癇發作及肌肉不正常運動](../Page/癲癇發作.md "wikilink")；[免疫抑制劑則能延緩](../Page/免疫抑制劑.md "wikilink")[肌細胞的死亡及傷害](../Page/肌細胞.md "wikilink")\[18\]。

約每5000名男嬰會有一人罹患此病\[19\]，杜興氏肌肉營養不良症為最常見的肌肉失養症\[20\]。患者平均預期壽命約為26歲\[21\]，然而如果接受良好治療，患者亦可能存活至30到40歲\[22\]。
此症由[義大利](../Page/義大利.md "wikilink")[那不勒斯的醫生Giovanni](../Page/那不勒斯.md "wikilink")
Semmola及Gaetano
Conte分別於1834年及1836年提出報告，但症狀名稱是取自1861年在其書中詳細描述此病的法國神經學家Guillaume-Benjamin-Amand
Duchenne。 另一種病情較輕的肌肉萎縮症稱為[贝克型肌肉萎缩症](../Page/贝克型肌肉萎缩症.md "wikilink")（,
BMD）是屬於DMD的亞型。

## 初期症狀

其他身體上的症狀有：

  - 不穩步伐
  - 容易跌倒
  - 不良於行（跑步和跳）
  - 愈來愈嚴重的行動困難
  - 行路能力通常在12歲時消失
  - 容易疲勞
  - 輕度智能障礙（大概30%的患者）
  - 骨骼發育畸形（有些個案為脊椎側彎）
  - 肌肉發育畸形
  - 小腿肌肉異常腫大（Pseudohypertrophy）（這些肌肉的正常組織被硬化組織所取代）

## 徵兆及檢驗方法

  - 一般在5岁以前发病；
  - 临床特点为进行性对称性肌无力，以肢体近端受累多见，起病常于下肢开始；
  - 体查无肌颤，无感觉障碍，多伴有腓肠肌假性肥大；
  - 肌电图呈肌源性损害；
  - 肌活检表现为肌纤维长短不一，出现坏死与降解，纤维透明化，出现结缔组织与脂肪组织代偿增生，免疫组化分析可见dystrophin缺失；
  - 有家族史，呈X连锁隐性遗传；
  - 病情进行性加重。

## 治療

### 支持療法

  - 提升自體體力
  - 提升自體免疫力
  - 心靈治療

### 晚期症狀

杜興氏肌肉營養不良症最終會影響到所有[平滑肌](../Page/平滑肌.md "wikilink")，以及心臟和呼吸肌肉。患者很少能活过35岁。
\[23\] 患者一般由于呼吸衰竭或心功能紊乱而死亡。

## 療法研究

國際上曾經有人試圖用幹細胞移植的方法治療DMD，在老鼠身上獲得一定的進展。但用於人类患者身上尚沒有取得滿意的效果。目前[基因治療仍在研究中](../Page/基因治療.md "wikilink")，\[24\]如何把DMD型改為BMD型，延長患者的壽命或針對“無義突變”發揮作用治療DMD/BMD，已經變成研究的熱點。

## 參考資料

## 外部連結

  - [裘馨氏肌肉失養症(Duchenne
    dystrophy)](http://smallcollation.blogspot.com/2013/04/duchenne-dystrophy.html)
  - [香港肌健協會](http://www.hknmda.org.hk/)
  - [CDC's National Center on Birth Defects and Developmental
    Disabilities](https://web.archive.org/web/20061122074841/http://www.cdc.gov/ncbddd/duchenne/index.htm)
  - [Muscular Dystrophy Campaign
    (UK)](http://www.muscular-dystrophy.org)
  - [DMD Registry](http://www.dmdregistry.org)
  - [Parent Project
    UK](https://web.archive.org/web/20160218052608/http://www.ppuk.org/)
  - [Parent Project USA](http://www.parentprojectmd.org/)
  - [Duchenne/Becker Muscular Dystrophy, NCBDDD,
    CDC](https://web.archive.org/web/20061122074841/http://www.cdc.gov/ncbddd/duchenne/index.htm)
  - [MUSCULAR DYSTROPHY Page on
    NCBI](http://www.ncbi.nlm.nih.gov/entrez/dispomim.cgi?id=158900)
  - [A support group for
    dads](http://health.groups.yahoo.com/group/DadsCafe/)
  - Report on U7 gene transfer studies [PDF
    file](https://web.archive.org/web/20070221124708/http://www.spectraldesign.net/share/2005%20Garcia-Danos%20U7,%20uk.pdf)。
  - [中国神经肌肉疾病协会](http://www.mdachina.org/)
  - [中華民國肌萎縮症病友協會](http://www.mda.org.tw/)
  - [中国DMD关爱协会](http://www.china-dmd.org/)
  - [中国DMD关爱协会论坛](http://bbs.china-dmd.org/forum.php/)
  - [中国DMD肌营养不良注册登记](http://www.dmd-registry.com/)

[Category:肌肉萎缩症](../Category/肌肉萎缩症.md "wikilink")
[Category:X连锁隐性遗传病](../Category/X连锁隐性遗传病.md "wikilink")
[Category:人名疾病](../Category/人名疾病.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23. [About Neuromuscular Diseases](http://www.mda.org/disease/dmd.aspx)

24.