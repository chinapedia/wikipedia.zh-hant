**功績獎狀**（）是[澳門勳章、獎章和獎狀制度中獎狀的其中一種](../Page/澳門勳章、獎章和獎狀制度.md "wikilink")，授予對澳門特別行政區的聲譽、發展或社會進步作出重要貢獻的澳門特別行政區居民。

## 授勳名單

  - 2001年

<!-- end list -->

  - [吳衛鳴](../Page/吳衛鳴.md "wikilink")
  - [馬許願](../Page/馬許願.md "wikilink")
  - [韓靜](../Page/韓靜.md "wikilink")
  - [André Couto](../Page/André_Couto.md "wikilink")

<!-- end list -->

  - 2002年

<!-- end list -->

  - [李峻](../Page/李峻一.md "wikilink")
  - [張佩思](../Page/張佩思.md "wikilink")
  - [黃伯祥](../Page/黃伯祥.md "wikilink")
  - [關雯菲](../Page/關雯菲.md "wikilink")
  - [蘇偉良](../Page/蘇偉良.md "wikilink")
  - [澳門飲食業聯合商會](../Page/澳門飲食業聯合商會.md "wikilink")

<!-- end list -->

  - 2003年

<!-- end list -->

  - [冼坤妹](../Page/冼坤妹.md "wikilink")
  - [梁綺媚](../Page/梁綺媚.md "wikilink")
  - [梁麗斯](../Page/梁麗斯.md "wikilink")
  - [陳倩玲](../Page/陳倩玲.md "wikilink")
  - [麥誠軒](../Page/麥誠軒.md "wikilink")
  - [項秉華](../Page/項秉華.md "wikilink")
  - [廖國敏](../Page/廖國敏.md "wikilink")
  - [廖國瑋](../Page/廖國瑋.md "wikilink")
  - [潘志輝](../Page/潘志輝.md "wikilink")
  - [謝榮瑤](../Page/謝榮瑤.md "wikilink")
  - [澳門特殊奧運會](../Page/澳門特殊奧運會.md "wikilink")

<!-- end list -->

  - 2004年

<!-- end list -->

  - [方約翰](../Page/方約翰.md "wikilink")
  - [艾明達](../Page/艾明達.md "wikilink")
  - [李雅穎](../Page/李雅穎.md "wikilink")
  - [馬偉達](../Page/馬偉達.md "wikilink")
  - [梁嘉詠](../Page/梁嘉詠.md "wikilink")
  - [許健華](../Page/許健華.md "wikilink")
  - [陳輝陽](../Page/陳輝陽.md "wikilink")
  - [黃清怡](../Page/黃清怡.md "wikilink")

<!-- end list -->

  - 2005年

<!-- end list -->

  - [王穎璋](../Page/王穎璋.md "wikilink")
  - [秦志堅](../Page/秦志堅.md "wikilink")
  - [區偉雄](../Page/區偉雄.md "wikilink")
  - [張志遠](../Page/張志遠.md "wikilink")
  - [陳梓俊](../Page/陳梓俊.md "wikilink")
  - [麥沛然](../Page/麥沛然.md "wikilink")
  - [黃響麟](../Page/黃響麟.md "wikilink")
  - [楊厚芹](../Page/楊厚芹.md "wikilink")
  - [賈瑞](../Page/賈瑞_\(武術運動員\).md "wikilink")
  - [賈嘉慧](../Page/賈嘉慧.md "wikilink")
  - [鍾家立](../Page/鍾家立.md "wikilink")
  - [關淑梅](../Page/關淑梅.md "wikilink")
  - 第四屆東亞運動會澳門龍舟女子小龍代表隊
  - 第四屆東亞運動會澳門龍舟男子小龍代表隊
  - 第十一屆亞洲雪屐曲棍球錦標賽澳門雪屐曲棍球代表隊

<!-- end list -->

  - 2006年

<!-- end list -->

  - [朱志偉](../Page/朱志偉_\(武術運動員\).md "wikilink")
  - [何司行](../Page/何司行.md "wikilink")
  - 李楠
  - [李鴻燦](../Page/李鴻燦.md "wikilink")
  - [潘麗芬](../Page/潘麗芬.md "wikilink")
  - [澳門愛心之友協進會](../Page/澳門愛心之友協進會.md "wikilink")

<!-- end list -->

  - 2007年

<!-- end list -->

  - [王衡鏘](../Page/王衡鏘.md "wikilink")
  - [席成清](../Page/席成清.md "wikilink")
  - [楊浩源](../Page/楊浩源.md "wikilink")
  - [鄭志威](../Page/鄭志威.md "wikilink")
  - 第二屆亞洲室內運動會中國澳門南獅代表隊
  - [澳門弱智人士服務協會](../Page/澳門弱智人士服務協會.md "wikilink")
  - [濠江中學步操旗樂隊](../Page/濠江中學步操旗樂隊.md "wikilink")

<!-- end list -->

  - 2008年

<!-- end list -->

  - [利安琪](../Page/利安琪.md "wikilink")
  - [吳偉豪](../Page/吳偉豪.md "wikilink")
  - [區均祥](../Page/區均祥.md "wikilink")
  - [梁嘉俊](../Page/梁嘉俊.md "wikilink")
  - [陳耀棠](../Page/陳耀棠.md "wikilink")
  - [斐保羅](../Page/斐保羅.md "wikilink")
  - [關淑賢](../Page/關淑賢.md "wikilink")
  - [澳門弱智人士家長協進會](../Page/澳門弱智人士家長協進會.md "wikilink")
  - [澳門旅行社協會](../Page/澳門旅行社協會.md "wikilink")
  - [澳門旅遊商會](../Page/澳門旅遊商會.md "wikilink")
  - [澳門旅遊業議會](../Page/澳門旅遊業議會.md "wikilink")

<!-- end list -->

  - 2009年

<!-- end list -->

  - [王俊楠](../Page/王俊楠.md "wikilink")
  - [伍星洪](../Page/伍星洪.md "wikilink")
  - [李詠詩](../Page/李詠詩.md "wikilink")
  - [高敏儀](../Page/高敏儀.md "wikilink")
  - [許朗](../Page/許朗.md "wikilink")
  - [張少玲](../Page/張少玲.md "wikilink")
  - [張丹](../Page/張丹_\(羽毛球運動員\).md "wikilink")
  - [張之博](../Page/張之博.md "wikilink")
  - [張志歡](../Page/張志歡.md "wikilink")
  - [溫家樂](../Page/溫家樂.md "wikilink")
  - [馮燕虹](../Page/馮燕虹.md "wikilink")
  - [劉霞](../Page/劉霞_\(舉重運動員\).md "wikilink")
  - [羅慶江](../Page/羅慶江.md "wikilink")
  - [Soler](../Page/Soler.md "wikilink") - Dino and Julio

<!-- end list -->

  - 2010年

<!-- end list -->

  - [余成斌](../Page/余成斌.md "wikilink")
  - [李廣祥](../Page/李廣祥.md "wikilink")
  - [周文顥](../Page/周文顥.md "wikilink")
  - [徐雪茵](../Page/徐雪茵.md "wikilink")
  - 梁家榮
  - [黃泳文](../Page/黃泳文.md "wikilink")
  - “美國高中數學競賽”澳門區代表隊

<!-- end list -->

  - 2011年

<!-- end list -->

  - [何文輝](../Page/何文輝_\(澳門\).md "wikilink")
  - [吳詠梅](../Page/吳詠梅.md "wikilink")
  - [洪嘉儀](../Page/洪嘉儀.md "wikilink")
  - 胡諾言
  - [敖博文](../Page/敖博文.md "wikilink")
  - [廖俊浩](../Page/廖俊浩.md "wikilink")
  - [趙偉強](../Page/趙偉強.md "wikilink")
  - 澳門基督教新生命團契S.Y部落　(Smart－Youth)

<!-- end list -->

  - 2012年

<!-- end list -->

  - [王嘉暉](../Page/王嘉暉.md "wikilink")
  - [李禕](../Page/李禕_\(武術運動員\).md "wikilink")
  - [劉冠華](../Page/劉冠華.md "wikilink")
  - [謝銀銀](../Page/謝銀銀.md "wikilink")
  - [蘇展鵬](../Page/蘇展鵬.md "wikilink")
  - [中國澳門雪屐曲棍球男子代表隊](../Page/中國澳門雪屐曲棍球男子代表隊.md "wikilink")
  - [澳門青年藝能志願工作會](../Page/澳門青年藝能志願工作會.md "wikilink")

<!-- end list -->

  - 2013年

<!-- end list -->

  - [謝俊昇](../Page/謝俊昇.md "wikilink")
  - [王嘉暉](../Page/王嘉暉.md "wikilink")
  - [譚知微](../Page/譚知微.md "wikilink")
  - [尤俊賢](../Page/尤俊賢.md "wikilink")
  - [庄嘉成](../Page/庄嘉成.md "wikilink")
  - [郭建恆](../Page/郭建恆.md "wikilink")
  - [馮瀟](../Page/馮瀟.md "wikilink")
  - [劉情](../Page/劉情.md "wikilink")
  - [蔡奧龍](../Page/蔡奧龍.md "wikilink")

<!-- end list -->

  - 2014年

<!-- end list -->

  - [徐雪茵](../Page/徐雪茵.md "wikilink")
  - [周文顥](../Page/周文顥.md "wikilink")
  - [澳門跳水隊](../Page/澳門跳水隊.md "wikilink")
  - [李禕](../Page/李禕_\(武術運動員\).md "wikilink")
  - [黃俊華](../Page/黃俊華.md "wikilink")
  - [張佩思](../Page/張佩思.md "wikilink")
  - [賈嘉慧](../Page/賈嘉慧.md "wikilink")
  - [劉情](../Page/劉情.md "wikilink")
  - [王俊楠](../Page/王俊楠.md "wikilink")
  - [陳茵穎](../Page/陳茵穎.md "wikilink")
  - [羅見訢](../Page/羅見訢.md "wikilink")
  - [張偉恆](../Page/張偉恆.md "wikilink")
  - [周鉅宏](../Page/周鉅宏.md "wikilink")

<!-- end list -->

  - 2015年

<!-- end list -->

  - [呂艷蘭](../Page/呂艷蘭.md "wikilink")
  - [周昊天](../Page/周昊天.md "wikilink")
  - [周家熙](../Page/周家熙.md "wikilink")
  - [馬　英](../Page/馬英.md "wikilink")
  - [張清雯](../Page/張清雯.md "wikilink")
  - [張少燕](../Page/張少燕.md "wikilink")
  - [蘇嘉鳳](../Page/蘇嘉鳳.md "wikilink")
  - [蔡嘉淇](../Page/蔡嘉淇.md "wikilink")
  - 「美國高中數學競賽」澳門區代表隊
  - [培正中學水底機械人小組](../Page/澳門培正中學.md "wikilink")
  - [澳門大學游泳隊](../Page/澳門大學.md "wikilink")

<!-- end list -->

  - 2016年

<!-- end list -->

  - [勞工子弟學校](../Page/勞工子弟學校.md "wikilink")「摩擦力分析儀」發明團隊
  - [勞工子弟學校](../Page/勞工子弟學校.md "wikilink")「可攜式環保淨水器」發明團隊
  - [李君濠](../Page/李君濠.md "wikilink")
  - [李偉誠](../Page/李偉誠.md "wikilink")

<!-- end list -->

  - 2017年

<!-- end list -->

  - [張向晨](../Page/張向晨.md "wikilink")
  - [林啟興](../Page/林啟興.md "wikilink")
  - [周昊天](../Page/周昊天.md "wikilink")
  - [培正中學](../Page/培正中學.md "wikilink")「國際科學與工程大獎賽2017」參賽隊伍

<!-- end list -->

  - 2018年

<!-- end list -->

  - [林愛敏](../Page/林愛敏.md "wikilink")
  - [余彩紅](../Page/余彩紅.md "wikilink")
  - [培正中學](../Page/澳門培正中學.md "wikilink") [“國際科學與工程大獎賽2018（Intel
    ISEF）”參賽隊伍](../Page/英特爾國際科技展覽會.md "wikilink")
  - [梁英偉](../Page/梁英偉.md "wikilink")

[Category:澳門頒授勳章、獎章和獎狀制度](../Category/澳門頒授勳章、獎章和獎狀制度.md "wikilink")
[Category:2001年建立的獎項](../Category/2001年建立的獎項.md "wikilink")