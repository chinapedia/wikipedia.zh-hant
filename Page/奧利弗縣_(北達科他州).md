**奧利弗县**（）是位於[美國](../Page/美國.md "wikilink")[北達科他州西南部的一個縣](../Page/北達科他州.md "wikilink")。面積1,894平方公里。根據[美國2000年人口普查估計](../Page/美國2000年人口普查.md "wikilink")，共有人口2,065人。縣治[中心](../Page/中心_\(北達科他州\).md "wikilink")。

奧利弗縣成立於1885年3月12日，縣政府成立於5月18日。縣名是紀念[達科他準州議員](../Page/達科他準州.md "wikilink")、[州議員哈利](../Page/北達科他州州議會.md "wikilink")·S奧利弗\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[O](../Category/北達科他州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.