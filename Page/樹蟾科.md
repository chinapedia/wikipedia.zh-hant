**雨蛙科**（[学名](../Page/学名.md "wikilink")：）即**樹蟾科**，通称为**雨蛙**、**樹蟾**、**树蛙**。其類別分別頗大。棲息在樹上的雨蛙多是綠色的，然而在陸地上和水中的種顏色則較為暗淡。牠們大多數是以捕食[昆蟲為生](../Page/昆蟲.md "wikilink")，但也有部份較大的種是依靠捕食小型[脊椎動物為生](../Page/脊椎動物.md "wikilink")。很多的雨蛙都不是棲息於樹上的，而是生長在陸地上或水中。[圓蛙屬](../Page/圓蛙.md "wikilink")（*Cyclorana*）的蛙是較隱蔽的，有時牠們會隱藏在地下多年。

## 樹蟾的種類

[歐洲樹蟾](../Page/歐洲樹蟾.md "wikilink")，例如[無斑雨蛙](../Page/無斑雨蛙.md "wikilink")（*Hyla
arborea*）是在中南美洲常見的蛙，並分佈在[亞洲和](../Page/亞洲.md "wikilink")[非洲北部一帶](../Page/非洲.md "wikilink")。當將快下雨時，這種蛙會變得非常吵鬧，因此有時他們會被困著來作為一種[晴雨表](../Page/晴雨表.md "wikilink")。

在[北美洲有很多種類的樹蟾](../Page/北美洲.md "wikilink")，包括[灰樹蟾](../Page/灰樹蟾.md "wikilink")（*Hyla
versicolor*），一種灰色的樹蟾，和[美國樹蟾](../Page/美國樹蟾.md "wikilink")（*Hyla
cinerea*），一種綠色的樹蟾。而[春雨蛙](../Page/春雨蛙.md "wikilink")（spring
peeper）則普遍分佈在[美國東部](../Page/美國.md "wikilink")，並常會在[夏天和](../Page/夏天.md "wikilink")[春天晚上聽到牠們的聲音](../Page/春天.md "wikilink")。

**樹蟾**（tree
toad）是某些樹蟾科普遍的名稱。[灰樹蟾是](../Page/灰樹蟾.md "wikilink")[變色樹蟾](../Page/變色樹蟾.md "wikilink")，[Trachycephalus
lichenatus是](../Page/Trachycephalus_lichenatus.md "wikilink")[地衣樹蟾](../Page/地衣樹蟾.md "wikilink")，[夜光蠑螺](../Page/夜光蠑螺.md "wikilink")（*T.
marmoratus*）是[大理石樹蟾](../Page/大理石樹蟾.md "wikilink")。

## 分類

[Hyla_gray_treefrog.jpg](https://zh.wikipedia.org/wiki/File:Hyla_gray_treefrog.jpg "fig:Hyla_gray_treefrog.jpg")
*Hyla versicolor*\]\]
[L_wilcoxi.jpg](https://zh.wikipedia.org/wiki/File:L_wilcoxi.jpg "fig:L_wilcoxi.jpg")
*Litoria wilcoxi*\]\]
[Hylidae_001.jpg](https://zh.wikipedia.org/wiki/File:Hylidae_001.jpg "fig:Hylidae_001.jpg")
*Hyla japonica*\]\]
樹蟾科可以細分為4個[亞科](../Page/亞科.md "wikilink")，和37至39[屬](../Page/屬.md "wikilink")。

  - [澳雨蛙亞科](../Page/澳雨蛙亞科.md "wikilink")（Pelodryadinae） (Austro-Papuan
    tree frogs)
      - [圓蛙屬](../Page/圓蛙屬.md "wikilink")（*Cyclorana*）
      - [雨濱蛙屬](../Page/雨濱蛙屬.md "wikilink")（*Litoria*）
      - （*Nyctimystes*）
  - [葉泡蛙亞科](../Page/葉泡蛙亞科.md "wikilink")（Phyllomedusinae） (Leaf frogs)
      - [红眼蛙属](../Page/红眼蛙属.md "wikilink")（*Agalychnis*）
      - （*Cruziohyla*）
      - （*Hylomantis*）
      - [叶蛙属](../Page/叶蛙属.md "wikilink")（*Pachymedusa*）
      - （*Phasmahyla*）
      - （*Phrynomedusa*）
      - （*Phyllomedusa*）
  - [擴角蛙亞科](../Page/擴角蛙亞科.md "wikilink")（Hemiphractinae）
      - （*Cryptobatrachus*）
      - [碟背蛙属](../Page/碟背蛙属.md "wikilink")（*Flectonotus*）
      - （*Gastrotheca*）
      - [扩角蛙属](../Page/扩角蛙属.md "wikilink")（*Hemiphractus*）
      - （*Stefania*）
  - [雨蛙亞科](../Page/雨蛙亞科.md "wikilink")（Hylinae）
      - [蝗蛙属](../Page/蝗蛙属.md "wikilink")（*Acris*）
      - （*Anotheca*）
      - （*Aparasphenodon*）
      - （*Aplastodiscus*）
      - （*Argenteohyla*）
      - （*Bokermannohyla*）
      - （*Bromeliohyla*）
      - （*Charadrahyla*）
      - （*Corythomantis*）
      - （*Dendropsophus*）
      - （*Duellmanohyla*）
      - （*Ecnomiohyla*）
      - （*Exerodonta*）
      - [雨蛙属](../Page/雨蛙属.md "wikilink")（*Hyla*）
      - （*Hyloscirtus*）
      - （*Hypsiboas*）
      - （*Isthmohyla*）
      - （*Itapotihyla*）
      - （*Lysapsus*）
      - （*Megastomatohyla*）
      - （*Myersiohyla*）
      - （*Nyctimantis*）
      - [首蛙属](../Page/首蛙属.md "wikilink")（*Osteocephalus*）
      - （*Osteopilus*）
      - （*Phyllodytes*）
      - （*Plectrohyla*）
      - [拟蝗蛙属](../Page/拟蝗蛙属.md "wikilink")（*Pseudacris*）
      - [多指节蟾属](../Page/多指节蟾属.md "wikilink")（*Pseudis*）
      - （*Ptychohyla*）
      - （*Scarthyla*）
      - （*Scinax*）
      - （*Smilisca*）
      - （*Sphaenorhynchus*）
      - （*Tepuihyla*）
      - （*Tlalocohyla*）
      - （*Trachycephalus*）
      - （*Triprion*）
      - （*Xenohyla*）

## 參考

<div class="references-small">

  - ''This article incorporates text from the *Collier's New
    Encyclopedia* (1921).''

<references />

</div>

## 外部連結

  - [The complete
    treefrog](https://web.archive.org/web/20061103184315/http://members.core.com/~treefrog/)

  - [關注蛙類](http://www.frogvilla.com/index.htm)
  - [部分蛙類圖片](http://www.cfpet.com/web/ShowClass2.asp?ClassID=11)

[Category:雨蛙科](../Category/雨蛙科.md "wikilink")