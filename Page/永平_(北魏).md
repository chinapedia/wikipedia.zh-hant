**永平**（508年八月—512年四月）是[北魏的君主宣武帝](../Page/北魏.md "wikilink")[元恪的第三个](../Page/元恪.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年餘。

## 大事记

## 出生

## 逝世

## 纪年

| 永平                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 508年                           | 509年                           | 510年                           | 511年                           | 512年                           |
| [干支](../Page/干支纪年.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[永平年號的政權](../Page/永平.md "wikilink")
  - 同期存在的其他政权年号
      - [建平](../Page/建平_\(北魏京兆王\).md "wikilink")（508年八月—九月）：[北魏](../Page/北魏.md "wikilink")[京兆王](../Page/京兆.md "wikilink")[元愉年号](../Page/元愉.md "wikilink")
      - [天監](../Page/天監.md "wikilink")（502年四月—519年十二月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [建昌](../Page/建昌_\(柔然\).md "wikilink")（508年-520年）：[柔然政权豆罗伏跋豆伐可汗](../Page/柔然.md "wikilink")[丑奴年号](../Page/丑奴.md "wikilink")
      - [承平](../Page/承平_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")
      - [義熙](../Page/义熙_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:500年代中国政治](../Category/500年代中国政治.md "wikilink")
[Category:510年代中国政治](../Category/510年代中国政治.md "wikilink")