《**Amazing
Album**》為[Twins](../Page/Twins.md "wikilink")2002年一張[中文專輯](../Page/中文.md "wikilink")，於2002年8月27日在[香港發行](../Page/香港.md "wikilink")，憑《ICHIBAN興奮》、《人比人》、《跅跅步哈姆太郎》、《200%
咒語》及《朋友仔》五曲廣受[香港欢迎](../Page/香港.md "wikilink")。專輯為Twins
2002年9月中的演唱會前的預熱，大碟中首次進行大規模的翻唱及改編，并大多數是動畫片及廣告主題曲。亦是Twins唯一一次的大規模翻唱及改編。

專輯主打歌自然是Twins演唱會的主題曲《ICHIBAN興奮》，動感歡快的節奏把我們引領進Twins快樂的童話音樂世界，你將見到一個精彩嘉年華的《魔法王國》，跟著《哈姆太郎》跳cha
cha步，跟著Twins念超級美妙無比的《200%咒語》，你還可以見到UFO……

而Twins的演唱實力也在不斷提高，在這張專輯中，她們為歌迷獻上了一首國語歌一首英文歌，國語歌《我的UFO》當然不能強求兩個小女孩能唱得如何標準，但我們可以感覺到她們是用了心去唱的。如果你看過她們的最新電影《一碌蔗》，會不會覺得電影中有一首英文歌很浪漫?沒錯，那首歌就是Twins演唱的《Melody
Fair》。最值得稱讚的是她們翻唱許冠傑的《梨渦淺笑》。比起歌神許冠傑的深情，Twins多了一份調皮和可愛，讓聽的人不禁想起她們甜甜的梨渦淺笑……

為了讓專輯更有價值，唱片公司也不惜投下重金，特地讓Twins趕赴日本拍了一部音樂電影《Twins的一千零一夜》。新專輯的圖片均來自音樂電影:Twins浸泡溫泉、穿和服、學柔道、打棒球的鏡頭在專輯中一一閃過。而且每張CD隨碟附送三首VCD，更驚喜的是附送Twins精美剪紙一套，讓你親手裝扮可愛的Twins\!
人氣二人女子組合Twins快將披甲上陣，在香港舉行她們首個《ICHIBAN興奮Twins演唱會》\!

## 曲目列表

## 曲目資料

  - 《ICHIBAN興奮》為[日清](../Page/日清.md "wikilink")[合味道杯麵廣告之廣告歌](../Page/合味道.md "wikilink")、“Twins
    Ichiban興奮演唱會”主題曲。
  - 《跅跅步哈姆太郎》為[動畫](../Page/動畫.md "wikilink")[哈姆太郎主題曲](../Page/哈姆太郎.md "wikilink")、[Twins第一首主唱的](../Page/Twins.md "wikilink")[兒歌](../Page/兒歌.md "wikilink")。
  - 《200%咒語》為動畫[哈姆太郎片尾曲](../Page/哈姆太郎.md "wikilink")。
  - 《你是我的UFO》為[日清UFO廣告之廣告歌](../Page/日清.md "wikilink")。
  - 《我的UFO》為國內[日清UFO廣告之廣告歌](../Page/日清.md "wikilink")、[Twins第一首主唱的](../Page/Twins.md "wikilink")[國語歌曲](../Page/國語.md "wikilink")。
  - 《Melody
    Fair》為電影“一碌蔗”歌曲、[Twins第一首主唱的](../Page/Twins.md "wikilink")[英語歌曲](../Page/英語.md "wikilink")、亦是[Twins第一首主唱的翻唱歌曲](../Page/Twins.md "wikilink")。
  - 《梨渦淺笑》為電影"一碌蔗"歌曲。

## 所獲獎項

  - 2002年[勁歌金曲第三季季選](../Page/勁歌金曲.md "wikilink") ——入選歌曲（"Ichiban興奮"）
  - [IFPI香港唱片銷量大獎](../Page/IFPI.md "wikilink")2002 ——十大銷量廣東唱片
  - 2002年[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink") ——兒歌金曲（"跅跅步哈姆太郎"）
  - 2002年[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink") ——兒歌金曲銀獎（"跅跅步哈姆太郎"）
  - 2002年[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")
    ——全年至尊兒歌金曲（"跅跅步哈姆太郎"）
  - 2003年[兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")
    ——全年至尊兒歌金曲（"200%的咒語"）

## 外部連結

  - ["Amazing Album"
    eegmusic](https://web.archive.org/web/20070927213822/http://www.eegmusic.com/music/details.aspx?CTID=c2d972c1-949a-4e8d-b6d2-cfb0ff685b4f)

[Category:Twins音樂專輯](../Category/Twins音樂專輯.md "wikilink")
[Category:2002年音樂專輯](../Category/2002年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:英皇娛樂音樂專輯](../Category/英皇娛樂音樂專輯.md "wikilink")