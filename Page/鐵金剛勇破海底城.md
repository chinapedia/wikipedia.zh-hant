《**海底城**》（），是第10部[占士·邦系列影片](../Page/占士邦.md "wikilink")。

## 情節概要

世界首富史登堡為了征服世界，滿足野心，建造一座「-{海底城}-」，並攔截英蘇[核子潛艇](../Page/核子潛艇.md "wikilink")，用艦上的核子彈頭企圖摧毀[美國](../Page/美國.md "wikilink")、[蘇聯兩大強國](../Page/蘇聯.md "wikilink")，在[英國](../Page/英國.md "wikilink")[軍情六處的頂尖情報員](../Page/軍情六處.md "wikilink")[詹姆士·龐德](../Page/詹姆士·龐德.md "wikilink")（代號007）的智取勇攻下，展開了一場鬥智鬥狠的生死之戰……

## 得獎紀錄

  - 本片在[第50屆奧斯卡金像獎提名](../Page/第50屆奧斯卡金像獎.md "wikilink")[最佳藝術指導](../Page/奧斯卡最佳藝術指導獎.md "wikilink")、[原創音樂](../Page/奧斯卡最佳原創音樂獎.md "wikilink")、[原創歌曲三項大獎](../Page/奧斯卡最佳原創歌曲獎.md "wikilink")。

## 角色

  - [羅渣·摩亞](../Page/羅渣·摩亞.md "wikilink")
  - [芭芭拉·貝芝](../Page/芭芭拉·貝芝.md "wikilink")
  - [Curt Jurgens](../Page/Curt_Jurgens.md "wikilink")
  - [Richard Kiel](../Page/Richard_Kiel.md "wikilink")
  - [卡羅琳·門若](../Page/卡羅琳·門若.md "wikilink")
  - [勞勃·布朗](../Page/勞勃·布朗.md "wikilink")

## 拍攝地點

  - [奧地利](../Page/奧地利.md "wikilink")
  - [英國](../Page/英國.md "wikilink")
  - [埃及](../Page/埃及.md "wikilink")
  - [義大利](../Page/義大利.md "wikilink")

## 延伸阅读

  -
## 參考

## 外部連結

  -
  -
  -
  -
  -
  - [MGM's official site for the
    film](http://www.mgm.com/title_title.do?title_star=SPYWHOLO)

[Category:1977年電影](../Category/1977年電影.md "wikilink")
[Category:海洋電影](../Category/海洋電影.md "wikilink")
[Category:詹姆士·龐德電影](../Category/詹姆士·龐德電影.md "wikilink")
[Category:奧地利背景電影](../Category/奧地利背景電影.md "wikilink")
[Category:埃及背景電影](../Category/埃及背景電影.md "wikilink")