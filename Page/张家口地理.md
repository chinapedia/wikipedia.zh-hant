[张家口市的面积广阔](../Page/张家口市.md "wikilink")，自然资源十分丰富。

## 土地资源

全市土地总面积5529万亩。

其中[耕地](../Page/耕地.md "wikilink")1662万亩，占总面积的30.06%；园地127万亩，占总面积的2.31%；林地1059万亩，占总面积的19.16%；牧草地792万亩，占总面积的14.34%；居民点及工矿用地156万亩，占总面积的2.83%；交通用地57万亩，占总面积的1.03%；水域118万亩，占总面积的2.14%；未利用土地1555万亩，占总面积的28.13%。

全市人均土地面积12.3亩，人均耕地3.7亩，比河北省人均土地4.2亩、人均耕地l.5亩分别多8.1亩、2.2亩，均高于[中国和](../Page/中国.md "wikilink")[河北省人均水平](../Page/河北省.md "wikilink")。

另外，全市有宜牧草地1600万亩，占全省草地面积的36.4%，仅次于[承德市居第二位](../Page/承德市.md "wikilink")，且集中分布在[坝上](../Page/坝上.md "wikilink")4县（[康保](../Page/康保.md "wikilink")、[尚义](../Page/尚义.md "wikilink")、[张北](../Page/张北.md "wikilink")、[沽源](../Page/沽源.md "wikilink")），是河北省的[畜牧业基地](../Page/畜牧业.md "wikilink")。畜牧业已成为坝上4县的主要产业。

全市宜林面积1643万亩，占全市土地总面积的三分之一，主要分布在丘陵、山区，在河北省仅次于[承德市居第二位](../Page/承德市.md "wikilink")。

[桑干河](../Page/桑干河.md "wikilink")、[洋河](../Page/洋河.md "wikilink")、[潮白河等河流流经境内](../Page/潮白河.md "wikilink")，沿河形成的[河川盆地土质肥沃](../Page/河川盆地.md "wikilink")，灌溉条件好，有利于发展农业和水果业，是全市[粮食和](../Page/粮食.md "wikilink")[水果的主产区](../Page/水果.md "wikilink")。

## 矿产资源

全市已发现各类矿产83种，已探明储量的矿种33种。

其中包括能源矿产3种、[黑色金属矿产](../Page/黑色金属.md "wikilink")2种、[有色金属矿产](../Page/有色金属.md "wikilink")4种、[贵重金属矿产](../Page/贵重金属.md "wikilink")2种、[稀有金属矿产](../Page/稀有金属.md "wikilink")5种、化工原料[非金属矿产](../Page/非金属.md "wikilink")4种以及其它非金属矿产10种、水气矿产2种。

另外，还有已发现尚未探明储量的矿产50种。位居全[河北省第一位的矿产共](../Page/河北省.md "wikilink")12种，前4位的矿产有21种。

一些大宗矿产如[煤](../Page/煤.md "wikilink")、[铁](../Page/铁.md "wikilink")、[金](../Page/金.md "wikilink")、[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[膨润土](../Page/膨润土.md "wikilink")、[沸石](../Page/沸石.md "wikilink")、[石墨](../Page/石墨.md "wikilink")、[磷矿等均在河北省占有重要地位](../Page/磷.md "wikilink")。

## 水利资源

全市水资源总量21.91亿立方米，人均水资源占有量487立方米，其中地表水资源量15.45亿立方米，入境水量近几年平均为1.3-1.4亿立方米。目前，全市已建成的地表供水工程有:[水库](../Page/水库.md "wikilink")127座，总库容7.1亿立方米，其中大型水库2座，库容2.3亿立方米;中型水库6座，库容3.01亿立方米;小型水库119座，库容1.8亿立方米。万亩以上[引水灌区](../Page/引水灌区.md "wikilink")40处，各种[塘](../Page/塘.md "wikilink")[坝](../Page/坝.md "wikilink")359处，[潜流](../Page/潜流.md "wikilink")322处，扬水站点813处。[地下水](../Page/地下水.md "wikilink")[资源量为](../Page/资源.md "wikilink")15.4亿立方米，全市均为浅水资源，其中[坝上总量为](../Page/坝上.md "wikilink")4.5亿立方米，[坝下总量为](../Page/坝下.md "wikilink")10.9亿立方米。

## 森林资源

全市现有林地面积1059万亩，森林覆盖率20.4%。

其中[防护林](../Page/防护林.md "wikilink")626.83万亩，用材林148.98万亩，经济林315万亩，薪炭林10.25万亩，特种用材林26.94万亩。

活立木蓄积量1328万立方米。

育苗总面积3.8万亩，年产苗5.6亿株。

干鲜果品年产量31.08万吨。

[三北防护林工程穿越市境](../Page/三北防护林.md "wikilink")。

## 动植物资源

张家口市现有野生陆生植物120[科](../Page/科.md "wikilink")513[属](../Page/属.md "wikilink")、1343种。

其中资源植物(包括[纤维](../Page/纤维.md "wikilink")、[油脂](../Page/油脂.md "wikilink")、[淀粉](../Page/淀粉.md "wikilink")、[香料等](../Page/香料.md "wikilink"))约300种;药用植物约300种;果树植物约100种;野生水生植物38种。

全市栽培树种中[木本植物共有](../Page/木本植物.md "wikilink")62科、129属、369种，可分为木材、工业原料、制药、饮食原料、蜜源、木本饲料、绿化观赏等七类。其中有代表性的树种有[白桦](../Page/白桦.md "wikilink")、[山杨](../Page/山杨.md "wikilink")、[侧柏](../Page/侧柏.md "wikilink")、[国槐](../Page/国槐.md "wikilink")、[苹果](../Page/苹果.md "wikilink")、[梨](../Page/梨.md "wikilink")、[葡萄](../Page/葡萄.md "wikilink")、[杏等](../Page/杏.md "wikilink")。

动物资源中，家养动物主要有[牛](../Page/牛.md "wikilink")、[马](../Page/马.md "wikilink")([口马](../Page/口马.md "wikilink"))、[驴](../Page/驴.md "wikilink")、[骡](../Page/骡.md "wikilink")、[猪](../Page/猪.md "wikilink")、[羊](../Page/羊.md "wikilink")、[兔](../Page/兔.md "wikilink")、[貉](../Page/貉.md "wikilink")、[蜂等](../Page/蜂.md "wikilink")56种，其中牛、马、羊已形成生产基地，养蜂业在[涿鹿](../Page/涿鹿.md "wikilink")、[怀来已形成规模](../Page/怀来.md "wikilink")。野生动物资源主要分为兽类和鸟类共170余种。

原始林区和草原上有[金钱豹](../Page/金钱豹.md "wikilink")、[狼](../Page/狼.md "wikilink")、[狐狸](../Page/狐狸.md "wikilink")、[貉](../Page/貉.md "wikilink")、[黄鼬](../Page/黄鼬.md "wikilink")、[野猪](../Page/野猪.md "wikilink")、[青山羊](../Page/青山羊.md "wikilink")、[褐马鸡](../Page/褐马鸡.md "wikilink")、[百灵鸟等野生动物](../Page/百灵鸟.md "wikilink")。

淡水鱼主要有[鲢鱼](../Page/鲢鱼.md "wikilink")、[鳃鱼](../Page/鳃鱼.md "wikilink")、[鲤鱼](../Page/鲤鱼.md "wikilink")、[草鱼](../Page/草鱼.md "wikilink")、[坝上高背鲫鱼等](../Page/坝上高背鲫鱼.md "wikilink")。

[张家口地理](../Category/张家口地理.md "wikilink")