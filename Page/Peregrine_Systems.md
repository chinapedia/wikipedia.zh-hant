**Peregrine Systems,
Inc.**（可翻譯成：Peregrine系統公司）是一家專注於研發企業用的[資產管理軟體](../Page/資產管理軟體.md "wikilink")、[變更管理軟體](../Page/變更管理軟體.md "wikilink")、[帮助台軟體的公司](../Page/帮助台軟體.md "wikilink")，該公司成立於1981年，總部位在[美國](../Page/美國.md "wikilink")[加州的](../Page/加州.md "wikilink")[聖地牙哥](../Page/聖地牙哥.md "wikilink")，此外在[美洲](../Page/美洲.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[亞太等地都設有辦事處](../Page/亞太.md "wikilink")。

2001年，一個重大的會計帳務醜聞使該公司名譽掃地，最後是將11名高級（資深）主管移送（以違法蓄意破產之名起訴）法辦而告結。

2003年，[美國證券交易委員會對Peregrine](../Page/美國證券交易委員會.md "wikilink")
Systems公司的銷售浮報與誇大營收等行為，以"重大欺瞞"之名進行罰款。

2005年12月19日，[惠普科技](../Page/惠普科技.md "wikilink")（[Hewlett
Packard](../Page/惠普科技.md "wikilink")，[HP](../Page/HP.md "wikilink")）以4.25億美元收併了Peregrine
Systems公司，該公司的產品現在歸屬到HP
[OpenView這個系列品牌之下](../Page/OpenView.md "wikilink")，同時也成為惠普全球軟體事業單位（HP
Software Global Business Unit）的一部份。

## 產品

  - **AssetCenter** -
    [資產管理軟體](../Page/資產管理.md "wikilink")（[资产管理](../Page/资产管理.md "wikilink")
    software）
  - **ServiceCenter** -
    [服務台](../Page/服務台軟體.md "wikilink")（或稱：[求助桌](../Page/求助桌軟體.md "wikilink")）軟體（
    software）
  - **Connect.It** - 資料整合工具（軟體）（Data integration tool）
  - **Enterprise Discovery** -（資訊資產）探查與庫存工具（軟體）（Discovery and Inventory
    tool）

## 參考引據

  - [HP完全收併Peregrine](https://web.archive.org/web/20061109054138/http://www.managementsoftware.hp.com/news/press/pr/2005/pr_0101.html)

## 外部連結

  - [Peregrine官方網站](http://www.peregrine.com)
  - [非官方的Peregrine
    wiki](https://web.archive.org/web/20060221022751/http://www.sandreas.de/wiki/index.php/Main_Page)

### 公司資料

  - [Peregrine Systems,
    Inc.公司簡介（Yahoo\!）](http://biz.yahoo.com/ic/53/53069.html)

[Category:软件公司](../Category/软件公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[Category:惠普](../Category/惠普.md "wikilink")