「**永劫輪迴**」（**Relentless
Recurrence**）是[閃靈樂團的第三張專輯](../Page/閃靈樂團.md "wikilink")，原專輯於2002年[水晶唱片發行](../Page/水晶唱片.md "wikilink")，2006年發行復刻版，並於2007年6月由Deathlight唱片公司全球發行英文版。

其專輯是以台灣[民間故事](../Page/民間故事.md "wikilink")「[林投姐](../Page/林投姐.md "wikilink")」為背景，描述林昭娘死後為了拯救獨子，不惜化為厲鬼對抗天庭，最後被打入永劫輪迴。專輯的另一特點是整張專輯都是用台語演唱。2003年，閃靈樂團以「永劫輪迴」專輯獲得[第14屆金曲獎](../Page/第14屆金曲獎.md "wikilink")[最佳樂團獎](../Page/最佳樂團獎_\(金曲獎\).md "wikilink")。

「永劫輪迴」的概念在於凸顯出台灣的厲鬼復仇悲劇情節，Freddy
認為，「*當初被中原視為化外之地的台灣，．．中國商人來台欺騙掠奪後隨即返大陸，台灣人承受這樣的人禍卻無法可管，．．於是「林投姐」「周成」這類厲鬼復仇的傳說不但出現，而且成為廣為流傳的民間故事。相較於中原文化中小鬼應受正神懲處、力求陽世大團員結局的價值，這種（台灣）厲鬼傳說不會在中原盛行，也不會成為中原文化的主流。*」\[1\]

## 專輯曲目

### 台灣版

1.  第一章 業
2.  第二章 悲命格
3.  第三章 恨葬林投
4.  第四章 冥河冤賦
5.  第五章 返陽救子
6.  第六章 厲鬼生
7.  第七章 殺途(復讎之一)
8.  第八章 恨弒三界(復讎之二)
9.  第九章 掠魂入閻獄(復讎之三)
10. 第十章 永劫輪迴

### 英文版

1.  Nemesis 02:40
2.  Onset of Tragedy 06:32
3.  Obituary tuning 03:07
4.  Grievance, Acheron Poem 08:35
5.  Revert to Mortal territory 03:42
6.  Funest Demon Born 02:25
7.  Vengeance Arise 04:30
8.  Slaughter in Tri-Territory 08:07
9.  Grab the Soul to Hell 07:02
10. Relentless Recurrence \[Hidden Track\] 08:00

## 參與團員

  - 主唱、二胡手：Freddy
  - 吉他手：Jesse
  - 鍵盤手、女合音：Vivien
  - 貝斯手、女合音：Doris
  - 鼓手：A-Jay

## 注釋

<div style="font-size:85%">

<references/>

</div>

[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:2002年音樂專輯](../Category/2002年音樂專輯.md "wikilink")
[Category:閃靈樂團](../Category/閃靈樂團.md "wikilink")
[Category:台灣清治前期背景作品](../Category/台灣清治前期背景作品.md "wikilink")
[Category:民間傳說與童話題材作品](../Category/民間傳說與童話題材作品.md "wikilink")
[Category:轉生題材作品](../Category/轉生題材作品.md "wikilink")

1.  閃靈王朝：十年搖滾全紀錄‧閃靈樂園著‧邱立崴整理，‧34頁‧2006年