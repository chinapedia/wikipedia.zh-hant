**Maxis**（音譯：**馬西斯**）是一家[電腦遊戲](../Page/電腦遊戲.md "wikilink")[軟件公司](../Page/軟件.md "wikilink")，由[威爾·萊特和Jeff](../Page/威爾·萊特.md "wikilink")
Braun在1987年創辦。1997年被[美商藝電收購](../Page/美商藝電.md "wikilink")，現在是[美商藝電旗下的子公司](../Page/美商藝電.md "wikilink")。以制作“模拟”（Sim）系列游戏而著称。

2015年3月4日，EA宣布将关闭Maxis的原总部所在的Maxis Emeryville工作室。\[1\]

## 歷史

  - 1987年：公司創辦
  - 1989年：出版[模擬城市](../Page/模擬城市.md "wikilink")（Sim City）
  - 1990年：出版[模擬地球](../Page/模擬地球.md "wikilink")（Sim Earth）
  - 1991年：出版[模擬螞蟻](../Page/模擬螞蟻.md "wikilink")（Sim Ant）
  - 1992年：出版[模擬生命](../Page/模擬生命.md "wikilink")（Sim Life）
  - 1992年：出版[模擬城市之古代風情](../Page/模擬城市之古代風情.md "wikilink")（SimCity
    Classic）
  - 1993年：出版[模擬農場](../Page/模擬農場.md "wikilink")（Sim Farm）
  - 1993年：出版[模擬城市2000](../Page/模擬城市2000.md "wikilink")（SimCity 2000）
  - 1993年：出版[模擬城市2000 特別版](../Page/模擬城市2000_特別版.md "wikilink")（SimCity
    2000 Special Edition）
  - 1994年：出版[模擬健康](../Page/模擬健康.md "wikilink")（Sim Health）
  - 1994年：出版[模擬城市2000之城市復興](../Page/模擬城市2000之城市復興.md "wikilink")（SimCity
    2000 Urban Renewal Kit）
  - 1995年：出版[模擬島嶼](../Page/模擬島嶼.md "wikilink")（Sim Isle）
  - 1995年：出版[模擬大樓](../Page/模擬大樓.md "wikilink")（Sim Tower）
  - 1995年：出版[模擬城市2000
    Windows版](../Page/模擬城市2000_Windows版.md "wikilink")（SimCity
    2000）
  - 1995年：出版[模拟系列典藏版
    2](../Page/模拟系列典藏版_2.md "wikilink")（SimClassics:Maxis
    Collections 2）
  - 1996年：出版[模擬城市2000 收集版](../Page/模擬城市2000_收集版.md "wikilink")（SimCity
    2000:CD Collection）
  - 1996年：出版[模擬經典三合一](../Page/模擬經典三合一.md "wikilink")（SimClassics:3 in 1
    Pack）
  - 1996年：出版[模拟小镇](../Page/模拟小镇.md "wikilink")（Sim Town）
  - 1996年：出版[模擬音調](../Page/模擬音調.md "wikilink")（Sim Tunes）
  - 1996年：出版[模擬高爾夫](../Page/模擬高爾夫.md "wikilink")（Sim Golf）
  - 1996年：出版[模擬公園](../Page/模擬公園.md "wikilink")（Sim Park）
  - 1997年：出版[Sim 三合一](../Page/Sim_三合一.md "wikilink")（My 3 Sims\!）
  - 1997年：出版[模拟城市街道](../Page/模拟城市街道.md "wikilink")（Streets of SimCity）
  - 1998年：出版[模拟极限](../Page/模拟极限.md "wikilink")（Ultimate Sim）
  - 1998年：出版[模拟直升机](../Page/模拟直升机.md "wikilink")（Sim Copter）
  - 1998年：出版[模拟旅行团](../Page/模拟旅行团.md "wikilink")（Sim Safari）
  - 1999年：出版[模拟城市3000](../Page/模拟城市3000.md "wikilink")（SimCity 3000）
  - 1999年：出版[模拟城市3000之探索无限](../Page/模拟城市3000之探索无限.md "wikilink")（SimCity
    3000 Unlimited）
  - 1999年：出版[模拟主题公园](../Page/模拟主题公园.md "wikilink")（Sim ThemePark）
  - 2000年：出版[模拟人生](../Page/模拟人生_\(游戏\).md "wikilink")（The Sims）
  - 2003年：出版[模拟城市4](../Page/模拟城市4.md "wikilink")（SimCity
    4）（[台灣地区上市名稱為模擬城市](../Page/台灣地区.md "wikilink")：福爾摩沙（）），同年9月出版模拟城市4：尖峰时刻资料片（）
  - 2004年：出版[模拟人生2](../Page/模拟人生2.md "wikilink")（The Sims 2）和
    [模拟人生：上流社会](../Page/模拟人生：上流社会.md "wikilink")（The
    Urbz，GBA平台）
  - 2005年：出版[模拟人生2第一套资料片](../Page/模拟人生2.md "wikilink")[-{zh-cn:大学城;
    zh-tw:夢幻大學城}-](../Page/模拟人生2：梦幻大学城.md "wikilink")（University）和第二套资料片[-{zh-cn:夜生活;
    zh-tw:美麗夜未眠}-](../Page/模拟人生2：美麗夜未眠.md "wikilink")（NightLife），同年11月出版[模拟人生2欢乐假期包的整合版](../Page/模拟人生2.md "wikilink")（Holiday
    Party Pack）。
  - 2006年：出版[模拟人生2第三套资料片](../Page/模拟人生2.md "wikilink")[-{zh-cn:我要开店;
    zh-tw:開店達人}-](../Page/模擬市民2：開店達人.md "wikilink")（Open for Business）
  - 2006年：出版[模拟人生2第一套物品包](../Page/模拟人生2.md "wikilink")-{zh-cn:家庭娱乐;
    zh-tw:歡樂家庭}-（Family Fun Stuff）
  - 2006年：出版[模拟人生2第二套物品包時尚生活](../Page/模拟人生2.md "wikilink")（Glamour Life
    Stuff）
  - 2006年：出版[模拟人生2第四套资料片](../Page/模拟人生2.md "wikilink")[模拟人生2：宠物](../Page/模拟人生2：宠物.md "wikilink")（Pets）
  - 2007年：出版[模拟人生物语系列第一部](../Page/模拟人生物语.md "wikilink")[-{zh-cn:生活物语;
    zh-tw:人生大冒險}-](../Page/模擬市民趴趴走：人生大冒險.md "wikilink")（Life Stories）
  - 2007年：出版[模拟人生2第五套资料片](../Page/模拟人生2.md "wikilink")[-{zh-cn:缤纷四季;
    zh-tw:度假四季}-](../Page/模擬市民2：度假四季.md "wikilink")（Seasons）
  - 2008年：出版[孢子](../Page/Spore.md "wikilink") (Spore)
  - 2009年：出版[模擬市民3](../Page/模擬市民3.md "wikilink")（2009年6月2日），同年11月推出第一個資料片《模擬市民3：世界歷險記》
  - 2010年：出版[孢子资料片](../Page/Spore.md "wikilink")-{zh-cn:银河大冒险;
    zh-tw:幻想星球}- (Galatic Adventures)
  - 2010年：出版[模擬市民3资料片](../Page/模擬市民3.md "wikilink")《夢想起飛》及《夜店人生》，-{zh-hans:物品包;
    zh-hant:組合}-《頂級奢華》及《慾望街車》
  - 2011年：出版[黑暗孢子](../Page/黑暗孢子.md "wikilink") (Dark Spore)
  - 2011年：出版《模擬市民3》資料片《花樣年華》及《玩美寵物》，-{zh-hans:物品包;
    zh-hant:組合}-《休閒生活》及《摩登生活》
  - 2012年：出版《模擬市民3》資料片《華麗舞台》、《異能新世紀》及《春夏秋冬》，-{zh-hans:物品包;
    zh-hant:組合}-《主臥室》、《凱蒂佩苪甜心包》及《迪赛生活》
  - 2013年：出版《模擬市民3》資料片《大學生活》、《島嶼天堂》及最後一個資料片《前進未來》，最後一個-{zh-hans:物品包;
    zh-hant:組合}-《电影组合》
  - 2013年：出版[模拟城市](../Page/模拟城市_\(2013\).md "wikilink")
    (Simcity)，於3月5日推出。同年11月12日推出第一個資料片《模擬城市：未來之城》(SimCity:
    Cities of Tomorrow)
  - 2014年：出版[模擬市民4](../Page/模擬市民4.md "wikilink") (The Sims 4)，於9月2日推出。
  - 2015年：3月4日，EA宣布将关闭Maxis的原总部所在的Maxis
    Emeryville工作室。\[2\]而模拟系列将交给Maxis的其他工作室制作。

## 参见

  - [模擬城市系列](../Page/模擬城市系列.md "wikilink")
  - [模拟系列列表](../Page/模拟系列列表.md "wikilink")

## 参考文献

## 外部链接

  - [官方网站](https://web.archive.org/web/19961110065156/http://www.maxis.com/)


{{-}}

[Category:1987年開業電子遊戲公司](../Category/1987年開業電子遊戲公司.md "wikilink")
[Category:艺电的部门与子公司](../Category/艺电的部门与子公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:美國電子遊戲公司](../Category/美國電子遊戲公司.md "wikilink")
[Category:加利福尼亚州娱乐公司](../Category/加利福尼亚州娱乐公司.md "wikilink")

1.

2.