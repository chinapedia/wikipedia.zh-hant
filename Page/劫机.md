**劫机**（Aircraft
hijacking），即以[武器](../Page/武器.md "wikilink")、威胁等暴力手段，抑或以其他非經正當程序奪取[民用飞机](../Page/客机.md "wikilink")、[军用航空飞行器之操縱權的行為](../Page/军用航空飞行器.md "wikilink")。

## 劫机的目的

劫機的目的有非常多種，主要有因尋求[政治庇護](../Page/政治庇護.md "wikilink")、訴求釋放[政治犯等政治因素的劫機](../Page/政治犯.md "wikilink")；以宗教名義或者犯罪組織指揮的[恐怖活动](../Page/恐怖活动.md "wikilink")；因各種原因而[報復某人某組織](../Page/報復.md "wikilink")，亦有偽裝成空難事件[詐欺保險金等目的意識明確的劫机行为](../Page/詐欺.md "wikilink")。
也有对交通工具自身有异常的兴趣，精神錯乱等引起的劫机行为。

## Hijack一词的由来

此词据说源于劫车大盗抢劫前假装成搭便车的路人，拦车前先假装友好，跟司机打招呼时说“Hi,
Jack\!”。后引申为劫持飞机，汽车等，日本由此派生出大量如，“Busjack”（劫巴士）、“Seajack”（海劫）、“Carjack”（劫车）等[和製英語单词](../Page/和製英語.md "wikilink")。不过，在欧美也有描写劫持飞机的“Skyjack”（空劫）的小说存在。

此詞來源還有另外一種說法，即highway（[高速公路](../Page/高速公路.md "wikilink")）与jacker（劫持者）的結合。後來被廣泛使用於劫機、劫船以及劫車等各種交通工具上的搶劫事件。

## 主要劫机事件

1930年第一起劫机事件以来，1940年代后期 -
1950年代后期，[共產主义国家逃往](../Page/共產主义国家.md "wikilink")[资本主义](../Page/资本主义.md "wikilink")[国家的政治逃亡性质劫机事件多有发生](../Page/国家.md "wikilink")。1960年代後期
-
1980年前期，[巴勒斯坦人民解放战线](../Page/巴勒斯坦.md "wikilink")（PFLP）、[日本赤軍](../Page/日本赤軍.md "wikilink")、[德國赤軍](../Page/紅軍派.md "wikilink")（RAF）等**极左激进派**组织了多起劫机事件。1990年代，中國大陸客機曾多次被劫持往[台灣](../Page/台灣.md "wikilink")，近年来著名劫机事件有2001年[蓋達組織策划实施的](../Page/蓋達組織.md "wikilink")[911恐怖袭击事件](../Page/九一一袭击事件.md "wikilink")。

### 事例

  - 1948年7月16日：[國泰航空一架租給](../Page/國泰航空.md "wikilink")[澳門航空運輸有限公司的水陸兩用飛機](../Page/澳門航空運輸有限公司.md "wikilink")，於[澳門](../Page/澳門.md "wikilink")[新口岸起飛後隨即被四名中國男子劫持](../Page/新口岸.md "wikilink")，但遭遇乘客激烈反抗。最後劫機者槍殺機長，飛機隨即失控墜毀於[九州外海](../Page/九州.md "wikilink")，造成機上22人死亡，只有一名劫機者生還。這次事件是史上首宗非政治劫機事件。

<!-- end list -->

  - 1969年12月：[大韓航空公司](../Page/大韓航空公司.md "wikilink")[YS-11型飞机于](../Page/YS-11.md "wikilink")[韩国](../Page/韩国.md "wikilink")[釜山机场起飞後遭劫机](../Page/釜山.md "wikilink")、劫机者要求前往[北朝鮮](../Page/北朝鮮.md "wikilink")。抵达北朝鮮后，乘客及乘務員11名连同飞机至今未被返还。

<!-- end list -->

  - 1970年由[日本](../Page/日本.md "wikilink")[赤軍派策划的](../Page/赤軍.md "wikilink")[-{淀}-号劫机事件是日本最早的劫机事件](../Page/淀號劫機事件.md "wikilink")。在这次劫机事件中日本的運輸政務次官被顶替当作人質，劫匪成功流亡北朝鮮，事件有众多未能得到解决。並且由於此事之前沒有關於劫機的处罚法律存在，暨此教训，[日本设立了](../Page/日本.md "wikilink")《[劫机防止法](../Page/防止劫机法.md "wikilink")》。

<!-- end list -->

  - 1970年9月6日：[环球航空公司的](../Page/环球航空公司.md "wikilink")
    [波音707型飞机](../Page/波音707.md "wikilink")、[瑞士航空公司的](../Page/瑞士航空公司.md "wikilink")[DC-8型飞机](../Page/DC-8.md "wikilink")[以色列航空公司的波音](../Page/以色列航空公司.md "wikilink")707型飞机、[泛美航空公司的](../Page/泛美航空公司.md "wikilink")[波音747型飞机](../Page/波音747.md "wikilink")，共计4机[遭同時劫机](../Page/道森機場劫機事件.md "wikilink")。在乘机私服警備人員同劫機者展开枪战后，除以色列航空的飞机和未能着陸的泛美航空飞机以外，其余飞机在[约旦的](../Page/约旦.md "wikilink")[英国空軍基地强行着陸成功](../Page/英国.md "wikilink")，事件发生不久[不列颠航空公司遭劫机](../Page/英国航空.md "wikilink")，于同一空軍基地强行着陸成功。着陸後、全体乘客被解救，3架飞机被同时爆破。这是一起由PFLP策划组织的，意在解救被关押战友的劫机事件。

<!-- end list -->

  - 1971年11月24日：[西北航空305號班機被一名神秘男子劫機](../Page/西北航空305號班機.md "wikilink")，最後該名男子取得所要求的金額及降落傘後跳機逃走，從此下落不明。

<!-- end list -->

  - 1973年7月20日：自称“被佔領地的儿子”的巴勒斯坦游击队与日本赤軍的混成部隊劫持了[阿姆斯特丹飞往](../Page/阿姆斯特丹.md "wikilink")[東京的](../Page/東京.md "wikilink")[日本航空公司](../Page/日本航空公司.md "wikilink")[波音747型飞机](../Page/波音747.md "wikilink")（執行[404號班機](../Page/日本航空404號班機劫機事件.md "wikilink")），强行着陸于[利比亚](../Page/利比亚.md "wikilink")。人質解救後飞机被爆破、劫机者受政府默许逃亡成功。

<!-- end list -->

  - 1976年6月27日：[特拉维夫飞往](../Page/特拉维夫.md "wikilink")[巴黎的](../Page/巴黎.md "wikilink")[法国航空公司](../Page/法国航空公司.md "wikilink")139航班遭[巴勒斯坦人民解放阵线](../Page/巴勒斯坦人民解放阵线.md "wikilink")（PFLP）和[巴德尔—迈因霍夫集团BAF合伙组织的劫机](../Page/巴德尔—迈因霍夫集团.md "wikilink")，途经[利比亚](../Page/利比亚.md "wikilink")，释放了除[犹太人以外的人質後](../Page/犹太人.md "wikilink")，于[乌干达着陸](../Page/乌干达.md "wikilink")。乌干达[独裁者](../Page/独裁者.md "wikilink")[伊迪·阿明](../Page/伊迪·阿明.md "wikilink")[总统支持PFLP](../Page/总统.md "wikilink")，将103名人質关押于机场候机厅。7月3日深夜、以色列[特种部隊实施了称为](../Page/特种部隊.md "wikilink")“[恩德培行动](../Page/恩德培行动.md "wikilink")”的人質救援行动。有2名人质死亡，其餘人员全部救出。

<!-- end list -->

  - 1977年10月13日：[西班牙領](../Page/西班牙.md "wikilink")[马约尔加島飞往](../Page/马约尔加島.md "wikilink")[法兰克福的](../Page/法兰克福.md "wikilink")[德国汉莎航空公司](../Page/德国汉莎航空公司.md "wikilink")[181航班](../Page/漢莎航空181號班機劫機事件.md "wikilink")（[波音737型](../Page/波音737.md "wikilink")）遭自称为[黑色九月的德国红軍](../Page/黑色九月.md "wikilink")（RAF）和PFLP合伙策划的集团劫机，被迫于[索马里着陸](../Page/索马里.md "wikilink")。10月17日，由[慕尼黑惨案而設立的](../Page/慕尼黑惨案.md "wikilink")[西德特种部隊](../Page/西德.md "wikilink")[第九國境守備隊](../Page/第九國境守備隊.md "wikilink")（GSG-9）通过突襲将人質全員解救。

<!-- end list -->

  - 1977年12月4日，[馬來西亞航空653號班機](../Page/馬來西亞航空653號班機.md "wikilink")，為一架[波音737客機](../Page/波音737.md "wikilink")，正前往[吉隆坡梳邦機場](../Page/蘇丹·阿卜都·阿齊茲機場.md "wikilink")。在飛至[煤炭山](../Page/煤炭山.md "wikilink")（Batu
    Arang）時，被[日本赤军脅持](../Page/日本赤军.md "wikilink")，最後飛機不明原因在[柔佛州墜毀](../Page/柔佛州.md "wikilink")，全機100人無人生還，包括多名部长级官员和外国大使。这次事件也是马航的第一次事故。

<!-- end list -->

  - 1983年5月5日：後來被[台灣稱為](../Page/台灣.md "wikilink")『[反共義士](../Page/反共義士.md "wikilink")』的[卓長仁](../Page/卓長仁.md "wikilink")、姜洪軍、高東萍、王豔大、安偉建及吳雲飛等6人，自中國內地劫民航機到[南韓](../Page/南韓.md "wikilink")[江原道春川市](../Page/江原道.md "wikilink")，然後轉赴台灣。該航機原是由[瀋陽飛往](../Page/瀋陽.md "wikilink")[上海的](../Page/上海.md "wikilink")[中國民航](../Page/中國民航.md "wikilink")296號[三叉戟型客機](../Page/霍克薛利三叉戟型.md "wikilink")。機上有9名機組人員，96名乘客，其中3名[日本人](../Page/日本.md "wikilink")。

<!-- end list -->

  - 1985年6月14日：[希腊](../Page/希腊.md "wikilink")[雅典飞往](../Page/雅典.md "wikilink")[意大利](../Page/意大利.md "wikilink")[罗马的环球航空](../Page/罗马.md "wikilink")847航班（[波音727型](../Page/波音727.md "wikilink")），在[地中海上空飛行时遭兩名自称为伊斯兰激进派份子劫持](../Page/地中海.md "wikilink")、一名[美国乘客遭射杀](../Page/美国.md "wikilink")。

<!-- end list -->

  - 1986年5月3日：台灣的[中華航空334號班機](../Page/中華航空334號班機.md "wikilink")，在飛往香港降落前遭機長王錫爵劫持轉降中國[廣州](../Page/廣州.md "wikilink")[白雲機場](../Page/广州白云国际机场.md "wikilink")，是少見的民航機駕駛員自行劫機的案例。

<!-- end list -->

  - 1987年12月7日：[太平洋西南航空1771號班機](../Page/太平洋西南航空1771號班機.md "wikilink")，為一架[BAe
    146-200客機執飛](../Page/BAe_146-200.md "wikilink")，由[洛杉磯國際機場飛往](../Page/洛杉磯國際機場.md "wikilink")[舊金山國際機場的定期航班](../Page/舊金山國際機場.md "wikilink")。在起飛後沒多久，一名遭解雇的員工以麥格農.44左輪手槍於客艙兩槍射殺其前主管，接著至駕駛艙各一槍射殺女空服員、副機長、機長三名機組員，然後將操縱桿前推迫使飛機向下墜落，接著於客艙射殺一名乘客後，飛機墜毀全機罹難。

<!-- end list -->

  - 1989年12月16日，由[北京經](../Page/北京.md "wikilink")[上海](../Page/上海.md "wikilink")、[舊金山飛往](../Page/舊金山.md "wikilink")[紐約的](../Page/紐約.md "wikilink")[中國民航981號班機](../Page/中國民航981號班機劫機事件.md "wikilink")（波音747型）被一名棉機配件廠職工劫持，最後降落在日本[福岡機場](../Page/福岡機場.md "wikilink")，劫機者次年被引渡回國。

<!-- end list -->

  - 1990年10月2日：中國[厦门飞往广州的](../Page/厦门.md "wikilink")[厦門航空8301號班機](../Page/厦門航空8301號班機.md "wikilink")（[波音737型](../Page/波音737.md "wikilink")）遭劫机要求飞往台湾。飞机因油料耗尽在广州白云机场紧急降落时，歹徒与飞行员发生打斗，并导致飞机失控再次加速，撞上停机坪上一架满载乘客等待起飞的[波音757型飞机和另一架](../Page/波音757.md "wikilink")[波音707型飞机](../Page/波音707.md "wikilink")，最终导致128人遇难，三架飞机全部报废。\[1\]

<!-- end list -->

  - 1994年4月7日：[聯邦快遞705號班機被一名隨機非當值員工劫持](../Page/聯邦快遞705號班機.md "wikilink")，機上三名機員全被打至重傷，但最後仍成功制伏劫機者，貨機最後亦安全降落。事件是少數貨機被劫持案例。

<!-- end list -->

  - 1994年12月24日：[法国航空8969号班机在](../Page/法国航空8969号班机劫机事件.md "wikilink")[阿尔及利亚首都](../Page/阿尔及利亚.md "wikilink")[阿尔及尔遭到四名伊斯兰武装组织成员劫机](../Page/阿尔及尔.md "wikilink")，杀死3名乘客之后，于12月25日飞往法国，其余乘客和机组人员在法国[马赛普罗旺斯机场被](../Page/马赛普罗旺斯机场.md "wikilink")[国家宪兵干预队成功解救](../Page/国家宪兵干预队.md "wikilink")。事件共造成9名国家宪兵干预队队员及13位乘客受伤，3名乘客和4名劫机者死亡。

<!-- end list -->

  - 1994年6月6日：中國[福州飞往](../Page/福州.md "wikilink")[廈門的](../Page/廈門.md "wikilink")[中国南方航空班机遭劫机](../Page/中国南方航空.md "wikilink")，劫机者要求飞往台灣。此後飞机被迫降落中正國際機場（今[桃園國際機場](../Page/桃園國際機場.md "wikilink")），着陸后劫机者投降、被台灣政府逮捕。这起劫机事件目的被认为是政治流亡。從1993年4月6日起開始至此，2年間前後共有12架次的中國民航機先後被劫往台灣。

<!-- end list -->

  - 1996年11月23日：从[亚的斯亚贝巴起飞](../Page/亚的斯亚贝巴.md "wikilink")，经[内罗毕](../Page/内罗毕.md "wikilink")、[布拉柴维尔](../Page/布拉柴维尔.md "wikilink")、[拉各斯飞往](../Page/拉各斯.md "wikilink")[阿比让的](../Page/阿比让.md "wikilink")[961号班机遭三名寻求政治庇护的埃塞俄比亚人劫机](../Page/埃塞俄比亚航空961号班机空难.md "wikilink")，于接近科摩罗的印度洋因燃油耗尽坠毁，事件导致125人死亡，有50人生还。

<!-- end list -->

  - 1999年7月23日：[日本](../Page/日本.md "wikilink")[全日空](../Page/全日空.md "wikilink")[61航班遭劫机](../Page/全日空61號班機劫機事件.md "wikilink")，劫机者威胁[空中乘务员后](../Page/空中乘务员.md "wikilink")，闯入操纵室，将机长刺杀后，自行驾驶飞机急速降落。据称劫机犯对飞机以及飞机运行系统有异常的兴趣导致此事件发生。

<!-- end list -->

  - 2001年3月16日，一架俄罗斯弗努科夫航空公司的[图-154客机在从](../Page/圖-154.md "wikilink")[伊斯坦布尔起飞前往](../Page/伊斯坦布尔.md "wikilink")[莫斯科的途中被三名](../Page/莫斯科.md "wikilink")[车臣武装分子劫持](../Page/车臣.md "wikilink")，当时机上有162名乘客。随后飞机降落在[沙特阿拉伯的](../Page/沙特阿拉伯.md "wikilink")[麦地那机场](../Page/穆罕默德·本·阿卜杜勒-阿齐兹亲王国际机场.md "wikilink")。在谈判无果后，沙特特种部队对该机发起进攻，击毙一名劫机者并逮捕其他两人。所有人质除一名俄罗斯机组人员和一名土耳其乘客死亡外其余均获解救。\[2\]

<!-- end list -->

  - 2001年9月11日：[蓋達組織恐怖份子挾持](../Page/蓋達組織.md "wikilink")[美國](../Page/美國.md "wikilink")[聯合航空93號班機](../Page/聯合航空93號班機.md "wikilink")、[175號班機及](../Page/聯合航空175號班機.md "wikilink")[美國航空](../Page/美國航空.md "wikilink")[11號班機](../Page/美国航空11号班机恐怖袭击.md "wikilink")、[77號班機對](../Page/美国航空77号班机恐怖袭击.md "wikilink")[世界貿易中心](../Page/世界貿易中心.md "wikilink")、[五角大廈進行恐怖攻擊](../Page/五角大廈.md "wikilink")，其中11號及175號班機先後撞上世貿大樓，77號班機則撞上五角大廈。93班機則因機上乘客及機組員的奮勇抵抗而在[賓夕法尼亞州郊區墬毀](../Page/賓夕法尼亞州.md "wikilink")，據信機上恐怖份子的目標是美國[國會山莊或](../Page/國會山莊.md "wikilink")[白宮](../Page/白宮.md "wikilink")。（詳見[九一一恐怖襲擊事件](../Page/九一一恐怖襲擊事件.md "wikilink")）

<!-- end list -->

  - 2012年6月29日：[天津航空GS7554号航班在从新疆](../Page/天津航空7554号班机劫机事件.md "wikilink")[和田机场飞往](../Page/和田机场.md "wikilink")[乌鲁木齐地窝堡国际机场中遭到](../Page/乌鲁木齐地窝堡国际机场.md "wikilink")6名维吾尔族男性劫机。劫机者在劫机过程中被制服，航班安全返航至和田机场。最终导致两名劫机者在医院死亡，2名空中安全员、2名乘务员和多名乘客受伤。

<!-- end list -->

  - 2015年3月24日：[德國之翼航空9525號班機](../Page/德國之翼航空9525號班機空難.md "wikilink")，由[空中巴士A320執飛](../Page/空中巴士A320.md "wikilink")，從[西班牙](../Page/西班牙.md "wikilink")[巴塞隆納飛往](../Page/巴塞隆納.md "wikilink")[德國](../Page/德國.md "wikilink")[杜賽爾多夫的航班](../Page/杜賽爾多夫.md "wikilink")。該航班機長途中因故離開駕駛艙，卻遭副機長反鎖於艙外，蓄意操縱該機於[法國境內](../Page/法國.md "wikilink")[上普羅旺斯阿爾卑斯省](../Page/上普羅旺斯阿爾卑斯省.md "wikilink")[阿爾卑斯山脈約海拔](../Page/阿爾卑斯山脈.md "wikilink")2700公尺高速撞山，144名乘客與6名機組員共計150人全數罹難。

### 兩岸劫機事件

  - 劫持[米格系列](../Page/米格.md "wikilink")[战斗机抵](../Page/战斗机.md "wikilink")[臺北](../Page/臺北.md "wikilink")：

1950年代到80年代期间，[中華民國政府以巨额奖励鼓励](../Page/中華民國政府.md "wikilink")[中國人民解放軍劫持战斗机飞往台湾](../Page/中國人民解放軍.md "wikilink")，这些劫机者被为「[反共義士](../Page/反共義士.md "wikilink")」，自1949年到1989年，先後有16名中國人民解放軍空军飞行员劫持13架苏制米格战斗机飞往臺灣。

  - 劫持[民航](../Page/民航.md "wikilink")[客机逃往臺灣](../Page/客机.md "wikilink")：

在1990年代后，特别是在1993年，还出现了中國大陆劫机者将民用客机劫持飞往台灣的“劫机潮”\[3\]，

  - 兩岸劫機事件在[九一一袭击事件之后](../Page/九一一袭击事件.md "wikilink")

在2000年后，由于台灣本身的民主化，而且要遵守世界的秩序特别是“国际反劫机公约\[4\] 与国内法\[5\]”，

## 劫机防治对策

1963年9月14日在日本[东京都订立的](../Page/东京都.md "wikilink")《關於在航空器內犯罪和其他某些有害行為的公約》（Convention
on Offences and Certain Other Acts Committed on Board
Aircraft）是第一个反劫机公约，对公约的范围、管辖权、机长的权利、各国的权利与责任等均作了详尽的规定。1970年12月16日在荷蘭[海牙订立的](../Page/海牙.md "wikilink")《制止非法劫持航空器公約》（Convention
for the Suppression of Unlawful Seizure of
Aircraft），以及1971年9月23日在加拿大[蒙特利爾订立的](../Page/蒙特利爾.md "wikilink")《制止危害民用航空安全非法行為公約》（Convention
for the Suppression of Unlawful Acts Against the Safety of Civil
Aviation），对防止、处理、惩治劫机制订了更明确的规则。1988年2月24日在蒙特利爾订立的《制止在為國際民用航空服務的機場從事非法暴力行為的議定書》（Protocol
on the Suppression of Unlawful Acts of Violence at Airports Serving
International Civil Aviation, supplementary to the Convention for the
Suppression of Unlawful Acts Against the Safety of Civil
Aviation），是《制止危害民用航空安全非法行為公約》的补充议定书。

1970年代开始，激进派组织的劫机頻发；各国为应付这种情况，完善了机场保安系统，并创立了处理劫机事件的特种部隊。1980年代—1990年代，劫机事件相对有所减少。

2001年9月11日，[美国多部飛機遭到劫持並墜毀](../Page/美国.md "wikilink")，[911恐怖袭击事件發生](../Page/九一一袭击事件.md "wikilink")，使得劫機防範再次成为世界性課題，各国机场开始大力加強保安措施，如：实施[行李检查](../Page/行李.md "wikilink")，乘客名单提交[警察当局](../Page/警察.md "wikilink")，严禁随身携带包括[指甲刀在内的任何带刃](../Page/指甲刀.md "wikilink")[金属物品](../Page/金属.md "wikilink")，机内餐具全部[塑料製造](../Page/塑料.md "wikilink")，强化安全检查等等。
 致重大伤亡或事故的劫机行为在中华人民共和国是[绝对死刑的罪名](../Page/劫持航空器罪.md "wikilink")。

## 參考文獻

## 参见

  - [恐怖活动](../Page/恐怖活动.md "wikilink")
  - [恐怖组织](../Page/恐怖组织.md "wikilink")
  - [恐怖主义](../Page/恐怖主义.md "wikilink")
  - [航空公司列表](../Page/航空公司列表.md "wikilink")

[劫機](../Category/劫機.md "wikilink")
[Category:恐怖活动](../Category/恐怖活动.md "wikilink")

1.  [1990年10月02日
    無綫晚間新聞（中國民航廣州劫機事件）](http://www.youtube.com/watch?v=30uW4-GhusA&NR=1)
2.
3.
4.  詳見：
5.  [中華民國刑法第185-1條](http://law.moj.gov.tw/LawClass/LawSingle.aspx?Pcode=C0000001&FLNO=185-1)、[民用航空法第100條](http://law.moj.gov.tw/LawClass/LawSingle.aspx?Pcode=K0090001&FLNO=100)