[AFR_Shay_31_01.jpg](https://zh.wikipedia.org/wiki/File:AFR_Shay_31_01.jpg "fig:AFR_Shay_31_01.jpg")
1907年，[阿里山林業鐵路仍興建時](../Page/阿里山林業鐵路.md "wikilink")，從[美國的](../Page/美國.md "wikilink")[LIMA廠引進一輛](../Page/美国利马机车厂.md "wikilink")13[噸的](../Page/噸.md "wikilink")[蒸氣機車](../Page/蒸氣機車.md "wikilink")（以下簡稱蒸機）。

與一般的蒸機不同的是，它的[汽缸是直立式的](../Page/汽缸.md "wikilink")，且使用傘形[齒輪](../Page/齒輪.md "wikilink")，這兩種特殊的設計是為了山岳鐵路的需要。阿里山林鐵通車後，陸續LIMA廠引進了8輛18噸級的蒸汽機車、12輛28噸級的蒸汽機車，這些全都同樣是直立式汽缸及傘形齒輪的設計，這些型式的蒸汽機車又稱為式蒸汽機車。

除了平地段（嘉義到竹崎）另有1輛日本[川崎造船兵庫廠製及](../Page/川崎重工車輛公司.md "wikilink")2輛英國廠製的蒸汽機車外，其餘都是LIMA廠Shay式的天下。

## 蒸汽機車列表

<table>
<thead>
<tr class="header">
<th><p>製造國與廠商</p></th>
<th><p>型號</p></th>
<th><p>製造年份</p></th>
<th><p>使用年份</p></th>
<th><p>編號</p></th>
<th><p>現況</p></th>
<th><p>存放地點</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>美國LIMA公司</p></td>
<td><p>18噸SHAY型</p></td>
<td><p>1911</p></td>
<td><p>1912</p></td>
<td><p>SL-12</p></td>
<td><p>陳列展示</p></td>
<td><p><a href="../Page/阿里山站.md" title="wikilink">阿里山站</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>SL-13</p></td>
<td><p>陳列展示</p></td>
<td><p>阿里山林業鐵路嘉義車庫園區</p></td>
<td><p>1977年贈予中國電影公司，陳列於<a href="../Page/中影文化城.md" title="wikilink">中影文化城</a><br />
2009年移回北門機廠，隔年整修完成</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-14</p></td>
<td><p>存放</p></td>
<td><p>澳洲普芬比利<br />
（Puffing Billy）森林鐵路</p></td>
<td><p>1972年贈送澳洲</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>SL-15</p></td>
<td><p>報廢</p></td>
<td></td>
<td><p>1993年北門機廠火災燒燬</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-16</p></td>
<td><p>陳列展示</p></td>
<td><p><a href="../Page/池南國家森林遊樂區.md" title="wikilink">池南國家森林遊樂區</a></p></td>
<td><p>1980年調撥花蓮</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1912</p></td>
<td><p>1912</p></td>
<td><p>SL-17</p></td>
<td><p>陳列展示</p></td>
<td><p><a href="../Page/阿里山火車旅館.md" title="wikilink">阿里山火車旅館</a><br />
(<a href="../Page/沼平車站.md" title="wikilink">沼平車站旁</a>)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1912</p></td>
<td><p>1913</p></td>
<td><p>SL-18</p></td>
<td><p>陳列展示</p></td>
<td><p><a href="../Page/奮起湖.md" title="wikilink">奮起湖車庫</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>28噸SHAY型</p></td>
<td><p>1912</p></td>
<td><p>1913</p></td>
<td><p>SL-21</p></td>
<td><p>陳列展示</p></td>
<td><p>嘉義公園</p></td>
<td><p>1975年移撥<a href="../Page/嘉義縣政府.md" title="wikilink">嘉義縣政府</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-22</p></td>
<td><p>陳列展示</p></td>
<td><p><a href="../Page/集集鎮.md" title="wikilink">集集鎮</a></p></td>
<td><p>1978年奉准移撥<a href="../Page/三軍大學.md" title="wikilink">三軍大學</a><br />
1997年8月2日撿整後移至成功嶺軍史公園陳列<br />
2001年1月9日移至集集車站公開陳列</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1912</p></td>
<td><p>1912</p></td>
<td><p>SL-23</p></td>
<td><p>陳列展示</p></td>
<td><p>阿里山林業鐵路嘉義車庫園區</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-24</p></td>
<td><p>用作阿里山火車<br />
旅館的先頭車</p></td>
<td><p>阿里山火車旅館<br />
(沼平站旁)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1913</p></td>
<td><p>1914</p></td>
<td><p>SL-25</p></td>
<td><p>動態運轉</p></td>
<td><p>阿里山林業鐵路嘉義車庫園區</p></td>
<td><p>燒柴油</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-26</p></td>
<td><p>動態運轉</p></td>
<td><p>阿里山林業鐵路嘉義車庫園區</p></td>
<td><p>燒煤炭，<br />
第一輛修復的機車</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>SL-28</p></td>
<td><p>陳列展示</p></td>
<td><p>苗栗鐵道公園</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-29</p></td>
<td><p>陳列展示</p></td>
<td><p><a href="../Page/奮起湖.md" title="wikilink">奮起湖車庫</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1915</p></td>
<td><p>1916</p></td>
<td><p>SL-31</p></td>
<td><p>動態運轉</p></td>
<td><p>阿里山車站</p></td>
<td><p>燒柴油，<br />
為首輛燒柴油的機車</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SL-32</p></td>
<td><p>陳列展示</p></td>
<td><p>竹崎親水公園</p></td>
<td><p>1999年竹崎鄉公所借用</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 攝影集

[File:阿里山森林鐵路16號蒸氣機車.jpg|18噸SHAY](File:阿里山森林鐵路16號蒸氣機車.jpg%7C18噸SHAY) 16號
<File:AFR> Shay 18 in Fenqihu Station.JPG|18噸SHAY 18號 <File:AFR> Shay 21
1.jpg|28噸SHAY 21號 <File:AFR> Shay 22 01.jpg|28噸SHAY 22號 <File:Shay>
23.jpg|28噸SHAY 23號 <File:AFR> Shay 24 01.jpg|28噸SHAY 24號 <File:Shay>
28.JPG|28噸SHAY 28號 <File:Fenchihu> Garage02.jpg|28噸SHAY　29號
<File:森林火車與櫻花在臺灣嘉義阿里山> Tourist Train and Cherry blossom -
Sakura in Alishan, Chiayi, TAIWAN.jpg|28噸SHAY　31號

## 參見

  - [阿里山林業鐵路](../Page/阿里山森林鐵路.md "wikilink")
  - [阿里山林業鐵路柴油機車](../Page/阿里山森林鐵路柴油機車.md "wikilink")

[Category:台灣蒸汽機車](../Category/台灣蒸汽機車.md "wikilink")
[蒸](../Category/阿里山森林鐵路.md "wikilink")