**乙醯苯胺**，又名**N-
Phenylacetamide**、**Acetanilide**，或商標名稱**Antifebrin**，是一種白色有光泽片状结晶或葉片或**鱗片狀**的**固體**化學品。
[Acetanilide_Crystals.jpg](https://zh.wikipedia.org/wiki/File:Acetanilide_Crystals.jpg "fig:Acetanilide_Crystals.jpg")

## 製造方法

乙醯苯胺可以透過[醋酸酐和](../Page/醋酸酐.md "wikilink")[苯胺或](../Page/苯胺.md "wikilink")[苯基苯胺氯化物進行](../Page/苯基苯胺氯化物.md "wikilink")[化學反應產生](../Page/化學反應.md "wikilink")。

## 性質

這種[化合物能溶於熱](../Page/化合物.md "wikilink")[水](../Page/水.md "wikilink")，溶於水呈中性。在達到攝氏545[℃時](../Page/攝氏.md "wikilink")，它会[自燃](../Page/自燃.md "wikilink")，不过在大多數情況下，它是[穩定的](../Page/穩定.md "wikilink")。它的純[晶體是](../Page/晶體.md "wikilink")[白色的](../Page/白色.md "wikilink")。

## 應用

乙醯苯胺可以作為[雙氧水的](../Page/雙氧水.md "wikilink")[抑制劑](../Page/反應抑制劑.md "wikilink")，並可以用以穩定纖維素酯的[varnishes](../Page/varnish.md "wikilink")。它也可以用作橡膠加速器[合成作用的中介物](../Page/合成作用.md "wikilink")；用於染料及染料中間體合成；也可用於[樟腦的合成](../Page/樟腦.md "wikilink")。乙醯苯胺被用作前驅[青黴素的合成作用及其他的](../Page/青黴素.md "wikilink")[醫藥的中間體](../Page/醫藥.md "wikilink")。

乙醯苯胺有[止痛與](../Page/止痛.md "wikilink")[退燒的功能](../Page/退燒.md "wikilink")；它和对乙酰氨基酚（[撲熱息痛](../Page/撲熱息痛.md "wikilink")）（PENADOL）屬同一類藥物。名義上乙醯苯胺這個名字可代表若干[成藥和非處方藥](../Page/成藥.md "wikilink")。在1948年，[朱利斯阿克塞爾羅德及](../Page/朱利斯阿克塞爾羅德.md "wikilink")[伯納德布羅迪發現乙醯苯胺在一些藥理應用上比其他藥物有毒性得多](../Page/伯納德布羅迪_\(藥理\).md "wikilink")，造成[正鐵血紅蛋白血症](../Page/正鐵血紅蛋白血症.md "wikilink")，並最終損害[肝臟及](../Page/肝臟.md "wikilink")[腎臟](../Page/腎臟.md "wikilink")，基本上毒性較低的藥物已取代乙醯苯胺，尤其是[对乙酰氨基酚](../Page/对乙酰氨基酚.md "wikilink")（一種乙醯苯胺的[代謝物](../Page/代謝物.md "wikilink")）

## 外部連結

  - [Acetanilide
    MSDS](http://physchem.ox.ac.uk/MSDS/AC/acetanilide.html)

[Category:酰胺](../Category/酰胺.md "wikilink")
[Category:芳香化合物](../Category/芳香化合物.md "wikilink")
[Category:药物](../Category/药物.md "wikilink")