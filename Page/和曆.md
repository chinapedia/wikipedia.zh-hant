**和曆**（）是[日本傳統历法](../Page/日本.md "wikilink")，歷史上，日本一直使用[華夏曆法](../Page/華夏曆法.md "wikilink")，並隨[中國中原朝廷改換曆書](../Page/中國.md "wikilink")，曾先後使用[劉宋之](../Page/劉宋.md "wikilink")[元嘉曆](../Page/元嘉曆.md "wikilink")、[大唐之](../Page/大唐.md "wikilink")[麟德曆](../Page/麟德曆.md "wikilink")（在日本叫做仪凤历）、[大衍曆](../Page/大衍曆.md "wikilink")、[五紀曆及](../Page/五紀曆.md "wikilink")[宣明曆](../Page/宣明曆.md "wikilink")。但在[大明滅亡之后](../Page/大明.md "wikilink")，日本開始使用日本本土編寫的[貞享曆](../Page/貞享曆.md "wikilink")、[寶曆曆](../Page/寶曆曆.md "wikilink")、[寬政曆及](../Page/寬政曆.md "wikilink")[天保曆](../Page/天保曆.md "wikilink")。[明治维新后](../Page/明治维新.md "wikilink")，[1872年日本政府废除天保历](../Page/1872年.md "wikilink")，改为使用西方[格里历](../Page/格里历.md "wikilink")，並把各傳統節日的官方日期改為西曆日子，但民间占卜、算命和春分上坟仍依照天保历；同時，雖然明治維新後官方不再使用舊曆，但民間習俗上仍保留從中國傳入的[干支紀年](../Page/干支.md "wikilink")，每一年的干支紀年起始以西曆為準。此外，[陰陽合曆中的](../Page/陰陽合曆.md "wikilink")[節氣本來就屬陽曆部分](../Page/節氣.md "wikilink")（按照地球公轉位置計算），因此仍舊保留，[春分](../Page/春分.md "wikilink")、[夏至](../Page/夏至.md "wikilink")、[秋分都是按照傳統規則計算的法定假日](../Page/秋分.md "wikilink")，其西曆日期在不同年份會有數日浮動。

## 年号纪年

[日本自](../Page/日本.md "wikilink")[孝德天皇時期開始受](../Page/孝德天皇.md "wikilink")[唐朝影響](../Page/唐朝.md "wikilink")，於646年開始使用年號，並開始以年號紀年。至今仍然使用。自明治維新後（1868年為明治元年）採[一世一元制](../Page/一世一元制.md "wikilink")，天皇在位期間使用單一年號（如[大正](../Page/大正天皇.md "wikilink")、[昭和等](../Page/裕仁天皇.md "wikilink")）。

## 傳統節日

傳統節日按農曆計算，但明治維新後改為格里历的相應日期，按照[二十四节气所定的節日則不受影響](../Page/二十四节气.md "wikilink")。

## 节句

一些由中國傳入、按照農曆陰曆而定的特定傳統節日在日語中叫做“节句”。

  - [元旦](../Page/元旦.md "wikilink") - 日語。正月初一。
  - [人日](../Page/人日.md "wikilink") - 日語/（七草节）。正月初七。
  - [上巳](../Page/上巳.md "wikilink") - 日語//。三月初三。
  - [端午](../Page/端午.md "wikilink") - 日語（端午节）。五月初五。
  - [七夕](../Page/七夕.md "wikilink") - 日語。七月初七。
  - [重陽](../Page/重陽.md "wikilink") - 日語/（菊花节）。九月初九。

### 杂节

按照[二十四节气所定的日本本土节日叫做](../Page/二十四节气.md "wikilink")“杂节”，由於二十四节气本就是按照地球公轉所定的，因此仍舊按照傳統算法依照節氣定日期。

  - 節分 - [立春前日](../Page/立春.md "wikilink")。2月3日左右。
  - [彼岸](../Page/彼岸.md "wikilink") -
    [春分](../Page/春分.md "wikilink")、[秋分及其前後三日](../Page/秋分.md "wikilink")。
  - 社日 -
    [春分](../Page/春分.md "wikilink")、[秋分最近的](../Page/秋分.md "wikilink")[戊日](../Page/戊.md "wikilink")。
  - 八十八夜 - 从[立春](../Page/立春.md "wikilink")88天。5月2日左右。
  - 入梅 - 太阳通过[黄经](../Page/黄经.md "wikilink")80度的日子。6月11日左右。
  - 半夏生 - 太阳通过黄经100度的日子。7月2日左右。
  - [土用](../Page/土用.md "wikilink") - 一般只指夏土用。
      - 春土用 : 从黄经27度到立夏(黄经45度)
      - 夏土用 : 从黄经117度到立秋(黄经135度)
      - 秋土用 : 从黄经207度到立冬(黄经225度)
      - 冬土用 : 从黄经297度到立春(黄经315度)
  - 二百十日 - 从立春210天。9月1日左右。
  - 二百二十日 - 从立春220天。9月11日左右。

### 其他節日

  - [春之大晦日](../Page/春之大晦日.md "wikilink") -五月最後一天。
  - [盂蘭盆](../Page/盂蘭盆.md "wikilink")（お盆） - 七月十五日。
  - [七五三](../Page/七五三.md "wikilink") - 十一月十五日。
  - [除夕](../Page/除夕.md "wikilink")（大晦日） - 一年最後一天。

## 月份

下列原本是在舊曆（農曆、天保曆）中使用的日本月份之別名，在改制為新曆之後並沒有完全消失，而是轉為引用在新的曆法中。今日在一些有標示[節氣](../Page/節氣.md "wikilink")、比較傳統風格的日本月曆上，往往還可以看到這些月份的別名存在，或以附註的方式補充在月曆的一角。

  - 1月：**[睦月](../Page/農曆一月.md "wikilink")**（）
  - 2月：**[如月](../Page/農曆二月.md "wikilink")**（）
  - 3月：**[彌生](../Page/農曆三月.md "wikilink")**（）
  - 4月：**[卯月](../Page/卯月.md "wikilink")**（）
  - 5月：**[皐月](../Page/農曆五月.md "wikilink")**或**早月**（）
  - 6月：**[水無月](../Page/水無月.md "wikilink")**（）
  - 7月：**[文月](../Page/文月.md "wikilink")**（）
  - 8月：**[葉月](../Page/葉月.md "wikilink")**（）
  - 9月：**[長月](../Page/長月.md "wikilink")**（）
  - 10月：**[神無月](../Page/神無月.md "wikilink")**（），但在[島根縣的](../Page/島根縣.md "wikilink")[出雲地方又稱為](../Page/出雲.md "wikilink")**[神有月](../Page/神有月.md "wikilink")**（）
  - 11月：**[霜月](../Page/霜月.md "wikilink")**（）
  - 12月：**[師走](../Page/師走.md "wikilink")**（）

## 参考

[日本历法](../Category/日本历法.md "wikilink")
[Category:農曆](../Category/農曆.md "wikilink")