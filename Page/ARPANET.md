[Arpanet_logical_map,_march_1977.png](https://zh.wikipedia.org/wiki/File:Arpanet_logical_map,_march_1977.png "fig:Arpanet_logical_map,_march_1977.png")
[Allmystery1988.jpg](https://zh.wikipedia.org/wiki/File:Allmystery1988.jpg "fig:Allmystery1988.jpg")
[Interface_Message_Processor_Front_Panel.jpg](https://zh.wikipedia.org/wiki/File:Interface_Message_Processor_Front_Panel.jpg "fig:Interface_Message_Processor_Front_Panel.jpg")

**高等研究計劃署網路**（，縮寫ARPAnet）是美國[國防高等研究計劃署开发的世界上第一个运营的](../Page/國防高等研究計劃署.md "wikilink")[封包交换网络](../Page/封包交换.md "wikilink")，是全球[互联网的鼻祖](../Page/互联网.md "wikilink")。

## 历史

### 诞生

所谓「阿帕」（ARPA），是美国高级研究计划署（**A**dvanced **R**esearch **P**roject
**A**gency）的简称。他的核心机构之一是信息处理处（IPTO, **I**nformation **P**rocessing
**T**echniques
**O**ffice），一直在关注[电脑图形](../Page/电脑图形.md "wikilink")、[网络通讯](../Page/网络通讯.md "wikilink")、[超级计算机等研究课题](../Page/超级计算机.md "wikilink")。

1962年，J·C·R·利克里德（J.C.R.Licklider）离开MIT，加入ARPA，并在后来成为IPTO的首席执行官。也就是他在任期间将办公室名称从命令控制研究（Command
and Control
Research）改为IPTO。也就是在他任职期间，据估计，整个美国计算机科学领域研究的70%由ARPA赞助，并在许多人看来与一个严格意义上的军事机构相去甚远，并给许多研究者自由领域来实验，结果ARPA不仅成为网络诞生地，同样也是[电脑图形](../Page/电脑图形.md "wikilink")、[平行过程](../Page/平行过程.md "wikilink")、[计算机模拟飞行等重要成果的诞生地](../Page/计算机模拟飞行.md "wikilink")。

1964年伊凡·沙日尔兰德（Ivan
Sutherland）继任担任该处处长，两年后的[鲍勃·泰勒](../Page/鲍勃·泰勒.md "wikilink")（Bob
Taylor）上任，他在任职期间萌发了新型计算机网络的想法，并筹集资金启动试验。在鲍勃·泰勒的一再邀请下，日后成为“阿帕网之父”的[拉里·罗伯茨出任信息处理处处长](../Page/拉里·罗伯茨.md "wikilink")。

1967年，罗伯茨来到高级研究计划署ARPA，着手筹建“分布式网络”。人员调度和工程设计很顺利，不到一年，就提出阿帕网的构想。随着计划的不断改进和完善，罗伯茨在描图纸上陆续绘制了数以百计的网络连接设计图，使之结构日益成熟。

1968年，罗伯茨提交研究报告《资源共享的计算机网络》，其中着力阐发的就是让“阿帕”的电脑达到互相连接，从而使大家分享彼此的研究成果。根据这份报告组建的国防部“高级研究计划网”，就是著名的“阿帕网”，拉里·罗伯茨也就成为“阿帕网之父”。

1969年底，阿帕网正式投入运行。

### 退出历史舞台

ARPA网无法做到和个别计算机网络交流，这引发了研究者的思考。根据诺顿的看法，他的设计需要太多的控制和太多的网络中机器设备的标准化。因此，1973年春，文顿·瑟夫和[鲍勃·康](../Page/鲍勃·康.md "wikilink")（Bob
Kahn）开始思考如何将ARPA网和另外两个已有的网络相连接，尤其是连接卫星网络（SAT
NET）和基于夏威夷的分组无线业务的ALOHA网（ALOHA
NET）瑟夫设想了新的计算机交流协议，最后创造出[传送控制协议](../Page/传送控制协议.md "wikilink")／[互联网协议](../Page/互联网协议.md "wikilink")（TCP/IP）。

1975年，ARPA网被转交到[美国国防部通信处](../Page/美国国防部通信处.md "wikilink")（Defense
Department Communicationg
Agence）。此后ARPA网不再是实验性和独一无二的了。大量新的网络在1970年代开始出现，包括[计算机科学研究网络](../Page/计算机科学研究网络.md "wikilink")（CSNET，Computer
Science Research
Network），[加拿大网络](../Page/加拿大网络.md "wikilink")（CDnet，Canadian
Network），[因时网](../Page/因时网.md "wikilink")（BITNET，Because It's Time
Network）和美国国家自然科学基金网络（NSFnet，National Science Foundation
Network）。最后一个网络最终将在它自身被商业网络取代前代替ARPA网作为互联网的高速链路。

1982年中期ARPA网被停用，原先的交流协议[NCP被禁用](../Page/NCP.md "wikilink")，只允许使用Cern的TCP/IP语言的网站交流。1983年1月1日，NCP成为历史，TCP/IP开始成为通用协议。

1983年ARPA网被分成两部分，用于军事和国防部门的军事网（MILNET）和用于民间的ARPA网版本。

1985年成为TCP/IP协议突破的一年，当时它成为[UNIX操作系统的组成部分](../Page/UNIX.md "wikilink")。最终将它放进了[Sun公司的微系统工作站](../Page/Sun公司.md "wikilink")。

当免费的在线服务和商业的在线服务兴起后，例如Prodigy、FidoNet、Usenet、Gopher等，当NSFNET成为互联网中枢后，ARPA网的重要性被大大减弱了。系统在1989年被关闭，1990年正式退役。

### 结构

最初的“阿帕网”，由西海岸的4个节点构成。第一个节点选在[加州大学洛杉矶分校](../Page/加州大学洛杉矶分校.md "wikilink")（UCLA），因为罗伯茨过去的麻省理工学院同事L.克莱因罗克教授，正在该校主持网络研究。第二个节点选在[斯坦福研究院](../Page/斯坦福研究院.md "wikilink")（SRI），那里有道格拉斯·恩格巴特（D.Engelbart）等一批网络的先驱人物。此外，[加州大学圣巴巴拉分校](../Page/加州大学.md "wikilink")（UCSB）和[犹他大学](../Page/犹他大学.md "wikilink")（UTAH）分别被选为三、四节点。这两所大学都有电脑绘图研究方面的专家，而泰勒之前的信息处理技术处处长伊凡·泽兰教授，此时也任教于犹他大学。

## 评价和影响

以现在的水平论，这个最早的网络显得非常原始，传输速度也慢的让人难以接受。但是，阿帕网的四个节点及其链接，已经具备网络的基本形态和功能。所以阿帕网的诞生通常被认为是网络传播的“创世纪”。

不过，阿帕网问世之际，大部分电脑还互不兼容。于是，如何使硬件和软件都不同的电脑实现真正的互联，就是人们力图解决的难题。这个过程中，[文顿·瑟夫为此做出首屈一指的贡献](../Page/文顿·瑟夫.md "wikilink")，从而被称为“互联网之父”。

## 深入阅读参考

  - Digital Systems Research Center."In
    Memoriam:J.C.R.Licklider,1915-1990."(Reprints"ManComputer
    Symbiosis"&"The Computer as a Communication
    Device.")[1](http://www.memex.org/licklider.pdf)

<!-- end list -->

  - Detouzos,Michael The Unfinished Revolution:HumanCentered Computers
    and What They Can Do for Us.New York:HarperCollins,2001.

<!-- end list -->

  - Haring,Bruce."Who Really Invented the Net?"USA Today,September
    2,1999.

<!-- end list -->

  - Hiltzik,Michael A.Dealers of Lightning:Xerox PARC and the Dawn of
    the Computer Age.New York:HarperCollins,1999.

<!-- end list -->

  - Rheingold,Howard.The Virtual Community:Homesteading on the
    Electronic Frontier.Reading,Mass.:Addison-Wesley,1993.

<!-- end list -->

  - Salus,Peter H.Casting the Net:From ARPANET to Internet and
    Beyand.Reading, Mass.:Addison-Wesley,1995.

[Category:广域网](../Category/广域网.md "wikilink")
[Category:網際網路的歷史](../Category/網際網路的歷史.md "wikilink")
[Category:IEEE里程碑](../Category/IEEE里程碑.md "wikilink")