**通俗拉丁語**（拉丁文：，意为“通俗话”，又叫**民间拉丁語**，或**流行拉丁語**），是[古典拉丁語在](../Page/古典拉丁語.md "wikilink")[法國](../Page/法國.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[羅馬尼亞等地的民间通俗变体](../Page/羅馬尼亞.md "wikilink")。中世紀早期開始分化，至九世紀成為羅曼語支的一支。

通俗拉丁語的[語法比](../Page/語法.md "wikilink")[古典拉丁語简单得多](../Page/古典拉丁語.md "wikilink")，在[語言学上体现了古典拉丁語作为一种](../Page/語言学.md "wikilink")[综合語到今日的](../Page/综合語.md "wikilink")[法語](../Page/法語.md "wikilink")、[義大利語](../Page/義大利語.md "wikilink")、[西班牙語](../Page/西班牙語.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[羅馬尼亞語等](../Page/羅馬尼亞語.md "wikilink")[語言作为一种](../Page/語言.md "wikilink")[分析語的过渡](../Page/分析語.md "wikilink")。

今日[罗曼語族诸語言都是直接从通俗拉丁語演变而来](../Page/罗曼語族.md "wikilink")。

## 历史

[Page_of_Lay_of_the_Cid.jpg](https://zh.wikipedia.org/wiki/File:Page_of_Lay_of_the_Cid.jpg "fig:Page_of_Lay_of_the_Cid.jpg")》是西班牙语最早的书面记录（节选）\]\]

通俗拉丁语（原始罗曼语）在罗马帝国各省发展各异，逐渐产生了不同的[罗曼语](../Page/罗曼语.md "wikilink")。约瑟夫·黑尔曼（József
Herman）称：

公元813年的第三次[图尔会议要求](../Page/图尔会议.md "wikilink")[神父使用地方语言传道](../Page/神父.md "wikilink")，即*rustica
lingua
romanica*（通俗拉丁语）或[日耳曼地方语言](../Page/日耳曼语族.md "wikilink")，因为当时的百姓已经无法理解规范拉丁语。在一代人的时间内，便产生了[查理曼的孙子](../Page/查理曼.md "wikilink")[秃头查理和](../Page/秃头查理.md "wikilink")[日耳曼人路易之间的](../Page/日耳曼人路易.md "wikilink")《[斯特拉斯堡誓言](../Page/斯特拉斯堡誓言.md "wikilink")》（公元842年），它使用了一种已经不同于拉丁语的语言。见下文选段：

[Sacramenta_Argentariae_(pars_brevis).jpg](https://zh.wikipedia.org/wiki/File:Sacramenta_Argentariae_\(pars_brevis\).jpg "fig:Sacramenta_Argentariae_(pars_brevis).jpg")，法语最早的书面记录（节选）\]\]

大约从这时起，本地拉丁语开始被视为独立语言，发展出了地方规范，有些产生了自己的[正字法](../Page/正字法.md "wikilink")，因此既然所有现代罗曼语变体都是通俗拉丁语的后续，则后者不应被视为已灭亡，而是概念和带有区域差异痕迹的术语的替换。

## 參見

  - [古典拉丁語](../Page/古典拉丁語.md "wikilink")
  - [拉丁语语法](../Page/拉丁语语法.md "wikilink")

## 參考文獻

### 引用

### 来源

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
#### 关于羅曼語族其他语种

  - 法语

<!-- end list -->

  -
  -
  -
<!-- end list -->

  - 意大利语

<!-- end list -->

  -
<!-- end list -->

  - 西班牙语

<!-- end list -->

  -
  -
  -
<!-- end list -->

  - 葡萄牙语

<!-- end list -->

  -
<!-- end list -->

  - [奧克语](../Page/奧克语.md "wikilink")

<!-- end list -->

  -
## 外部連結

  -
  -
  -
[Category:義大利語族](../Category/義大利語族.md "wikilink")
[Category:拉丁语](../Category/拉丁语.md "wikilink")