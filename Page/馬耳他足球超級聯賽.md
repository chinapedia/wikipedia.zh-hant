{{ 足球聯賽資料 | name = 馬耳他足球超級聯賽 | another_name = Maltese Premier League |
image = | pixels = | country =  | confed =
[UEFA](../Page/歐洲足球協會聯盟.md "wikilink")（[歐洲](../Page/歐洲.md "wikilink")）
| founded = 1909年 | teams = 14 隊 | relegation =
[馬耳他足球甲級聯賽](../Page/馬耳他足球甲級聯賽.md "wikilink")
| level = 第 1 級 | domestic_cup =
[馬耳他足總盃](../Page/馬耳他足總盃.md "wikilink")、
[馬耳他超級盃](../Page/馬耳他超級盃.md "wikilink") | international_cup =
[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")、
[歐霸盃](../Page/歐足聯歐洲聯賽.md "wikilink") | season = 2017–18 | champions =
[華列達](../Page/瓦萊塔足球俱樂部.md "wikilink") | most_successful_club =
[斯利馬流浪](../Page/斯利馬流浪者足球俱樂部.md "wikilink") | no_of_titles = 26 次
| website =
[mfa.com.mt](http://www.mfa.com.mt/en/leagues/leagues/10/bov-premier-league.htm/)
| current = }}

**馬耳他足球超級聯賽**（**Maltese Premier League**）
是[马耳他最高级别的足球联赛](../Page/马耳他.md "wikilink")，之下還有三個級別聯賽：[馬耳他足球甲級聯賽](../Page/馬耳他足球甲級聯賽.md "wikilink")、[馬耳他足球乙級聯賽和](../Page/馬耳他足球乙級聯賽.md "wikilink")[馬耳他足球丙級聯賽](../Page/馬耳他足球丙級聯賽.md "wikilink")。

## 參賽球隊

2018–19年馬耳他足球超級聯賽參賽隊伍共有 14 支。

| 中文名稱                                       | 英文名稱                  | 所在城市                                     | 上季成績     |
| ------------------------------------------ | --------------------- | ---------------------------------------- | -------- |
| [華列達](../Page/瓦萊塔足球俱樂部.md "wikilink")      | Valletta F.C.         | [華列他](../Page/瓦萊塔.md "wikilink")         | 第 1 位    |
| [巴爾辛](../Page/巴爾辛足球會.md "wikilink")        | Balzan F.C.           | [巴爾贊](../Page/巴爾贊.md "wikilink")         | 第 2 位    |
| [格茲拉聯](../Page/格茲拉聯足球會.md "wikilink")      | Gżira United F.C.     | [納沙爾](../Page/納沙爾.md "wikilink")         | 第 3 位    |
| [拜基卡拉](../Page/比爾基爾卡拉足球俱樂部.md "wikilink")  | Birkirkara F.C.       | [比爾基卡拉](../Page/比爾基卡拉.md "wikilink")     | 第 4 位    |
| [希伯尼恩斯](../Page/希伯尼安斯足球俱樂部.md "wikilink")  | Hibernians F.C.       | [保拉](../Page/保拉_\(馬耳他\).md "wikilink")   | 第 5 位    |
| [科利安拿](../Page/弗羅瑞安娜足球俱樂部.md "wikilink")   | Floriana F.C.         | [科利安拿](../Page/科利安拿.md "wikilink")       | 第 6 位    |
| [施利馬流浪](../Page/斯利馬流浪者足球俱樂部.md "wikilink") | Sliema Wanderers F.C. | [斯利馬](../Page/斯利馬.md "wikilink")         | 第 7 位    |
| [咸侖斯巴坦](../Page/咸侖斯巴坦足球會.md "wikilink")    | Hamrun Spartans F.C.  | [哈姆倫](../Page/哈姆倫_\(馬耳他\).md "wikilink") | 第 8 位    |
| [森格利亞](../Page/森格利亞競技足球會.md "wikilink")    | Senglea Athletic F.C. | [森格萊阿](../Page/森格萊阿.md "wikilink")       | 第 9 位    |
| [莫斯達](../Page/莫斯達足球會.md "wikilink")        | Mosta F.C.            | [莫斯塔](../Page/莫斯塔.md "wikilink")         | 第 10 位   |
| [聖安德魯斯](../Page/聖安德魯斯足球會.md "wikilink")    | St. Andrews F.C.      | [彭布羅克](../Page/彭布羅克.md "wikilink")       | 第 11 位   |
| [塔希恩](../Page/塔希恩彩虹足球會.md "wikilink")      | Tarxien Rainbows F.C. | [塔爾欣](../Page/塔爾欣.md "wikilink")         | 第 12 位   |
| [科爾米](../Page/科爾米足球會.md "wikilink")        | Qormi F.C.            | [戈爾米](../Page/戈爾米_\(馬耳他\).md "wikilink") | 乙組，第 1 位 |
| [皮爾塔熱刺](../Page/皮埃塔熱刺足球俱樂部.md "wikilink")  | Pietà Hotspurs F.C.   | [佩塔](../Page/佩塔.md "wikilink")           | 乙組，第 2 位 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 球會奪冠次數

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/斯利馬流浪者足球俱樂部.md" title="wikilink">施利馬流浪</a></p></td>
<td><p>26</p></td>
<td><p>1920, 1923, 1924, 1926, 1930, 1933, 1934, 1936, 1938, 1939,<br />
1940, 1949, 1954, 1956, 1957, 1964, 1965, 1966, 1971, 1972,<br />
1976, 1989, 1996, 2003, 2004, 2005</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/弗羅瑞安娜足球俱樂部.md" title="wikilink">科利安拿</a></p></td>
<td><p>25</p></td>
<td><p>1910, 1912, 1913, 1921, 1922, 1925, 1927, 1928, 1929, 1931,<br />
1935, 1937, 1950, 1951, 1952, 1953, 1955, 1958, 1962, 1968,<br />
1970, 1973, 1975, 1977, 1993</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瓦萊塔足球俱樂部.md" title="wikilink">華列達</a></p></td>
<td><p>24</p></td>
<td><p>1915, 1932, 1945, 1946, 1948, 1959, 1960, 1963, 1974, 1978,<br />
1980, 1984, 1990, 1992, 1997, 1998, 1999, 2001, 2008, 2011,<br />
2012, 2014, 2016, 2018</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/希伯尼安斯足球俱樂部.md" title="wikilink">希伯尼恩斯</a></p></td>
<td><p>12</p></td>
<td><p>1961, 1967, 1969, 1979, 1981, 1982, 1994, 1995, 2002, 2009,<br />
2015, 2017</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/咸侖斯巴坦足球會.md" title="wikilink">咸侖斯巴坦</a></p></td>
<td><p>7</p></td>
<td><p>1914, 1918, 1947, 1983, 1987, 1988, 1991</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/比爾基爾卡拉足球俱樂部.md" title="wikilink">拜基卡拉</a></p></td>
<td><p>4</p></td>
<td><p>2000, 2006, 2010, 2013</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拉巴特阿積士足球會.md" title="wikilink">拉巴特阿積士</a></p></td>
<td><p>2</p></td>
<td><p>1985, 1986</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬爾薩什洛克足球俱樂部.md" title="wikilink">馬沙斯洛克</a></p></td>
<td><p>1</p></td>
<td><p>2007</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聖喬治足球俱樂部.md" title="wikilink">聖喬治</a></p></td>
<td><p>1</p></td>
<td><p>1917</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬耳他國王團.md" title="wikilink">馬耳他國王團</a></p></td>
<td><p>1</p></td>
<td><p>1919</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [马耳他足球联赛官网](http://www.maltafootball.com/index.shtml)
  - [马耳他足球](http://www.maltafootball.com/)

## 參考

[Malta](../Category/歐洲各國足球聯賽.md "wikilink")
[欧](../Category/國家頂級足球聯賽.md "wikilink")
[Category:馬爾他足球](../Category/馬爾他足球.md "wikilink")

1.