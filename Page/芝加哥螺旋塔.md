**芝加哥螺旋塔**（英文: The Chicago
Spire），是一座正在建造的純[住宅](../Page/住宅.md "wikilink")[摩天大廈](../Page/摩天大廈.md "wikilink")，位於[美國](../Page/美國.md "wikilink")[芝加哥市中心](../Page/芝加哥.md "wikilink")，计划高度为610米。建成後，將會是[北美最高的建築大廈](../Page/北美.md "wikilink")。[建築師是](../Page/建築師.md "wikilink")[聖地牙哥·卡拉特拉瓦](../Page/聖地牙哥·卡拉特拉瓦.md "wikilink")，[開發商則為](../Page/地產發展商.md "wikilink")（Shelbourne
Development）。

2009年3月份因金融危機的衝擊，此建設計劃被迫停工\[1\] 。

由於美國金融危機，芝加哥螺旋塔計畫於2010年被迫取消\[2\]\[3\]。

<File:Fordham> Lot2.JPG|2006年4月
[File:ChicagoSpireAug22,20071.JPG|2007年8月](File:ChicagoSpireAug22,20071.JPG%7C2007年8月)
<File:ChicagoSpire> from LakePointTower 1 12 08 closeup.JPG|2008年1月
<File:ChicagoSpire> 05 25 08.jpg|2008年5月

## 參考

## 外部链接

  - [芝加哥螺旋塔官方網址](http://www.thechicagospire.com/)
  - [芝加哥欲建全美最高摩天樓
    高度將達610米](http://news.eastday.com/w/20070421/u1a2782396.html)

[Category:美國未來建設](../Category/美國未來建設.md "wikilink")
[Category:芝加哥摩天大樓](../Category/芝加哥摩天大樓.md "wikilink")
[Category:美国建筑之最](../Category/美国建筑之最.md "wikilink")
[Category:扭曲建築物](../Category/扭曲建築物.md "wikilink")

1.
2.
3.