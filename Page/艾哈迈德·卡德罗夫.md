**艾哈迈德·卡德罗夫**（，，）是[俄罗斯联邦](../Page/俄罗斯联邦.md "wikilink")[车臣共和国总统](../Page/车臣共和国.md "wikilink")，在首府[格罗兹尼的狄纳莫体育场内被车臣武装份子以](../Page/格罗兹尼.md "wikilink")[炮弹改造的](../Page/炮弹.md "wikilink")[地雷刺杀身亡](../Page/地雷.md "wikilink")。

## 生平

1951年，卡德罗夫生于[苏联](../Page/苏联.md "wikilink")[哈萨克苏维埃社会主义共和国的](../Page/哈萨克苏维埃社会主义共和国.md "wikilink")[卡拉干达](../Page/卡拉干达.md "wikilink")，并于1957年4月随同父母返回当时前苏联俄罗斯联邦的[车臣-印古什自治共和国](../Page/车臣-印古什苏维埃社会主义自治共和国.md "wikilink")。他的家庭是一个[穆斯林家庭](../Page/穆斯林.md "wikilink")，也是当地最大的家族的成员。毕业于原[苏联宗教学校](../Page/苏联宗教学校.md "wikilink")，获得[神学和](../Page/神学.md "wikilink")[世俗哲学两个专业的高等学历](../Page/世俗哲学.md "wikilink")。在1994年至1996年的[第一次车臣战争期间](../Page/第一次车臣战争.md "wikilink")，卡德罗夫与当时车臣武装组织领导人[焦哈尔·杜达耶夫站在同一陣線](../Page/焦哈尔·杜达耶夫.md "wikilink")，任车臣[穆夫提](../Page/穆夫提.md "wikilink")，与俄罗斯联邦军队对抗。在杜达耶夫被殺之后，车臣武装组织的领导由[阿斯兰·马斯哈多夫接任](../Page/阿斯兰·马斯哈多夫.md "wikilink")。由于他与马斯哈多夫在政策上的分歧日益嚴重，他在1999年8月与武装组织決裂，转而支持俄罗斯联邦，并在2003年5月的选举中取得超过八成选民的支持成为总统。2003年5月10日，被俄罗斯授予“[俄罗斯联邦英雄](../Page/俄罗斯联邦英雄.md "wikilink")”称号。

[Category:车臣人](../Category/车臣人.md "wikilink")
[Category:俄罗斯联邦政治人物](../Category/俄罗斯联邦政治人物.md "wikilink")
[Category:俄羅斯聯邦英雄](../Category/俄羅斯聯邦英雄.md "wikilink")
[Category:俄罗斯联邦穆斯林](../Category/俄罗斯联邦穆斯林.md "wikilink")
[Category:俄罗斯联邦遇刺身亡者](../Category/俄罗斯联邦遇刺身亡者.md "wikilink")
[Category:在俄羅斯被謀殺身亡者](../Category/在俄羅斯被謀殺身亡者.md "wikilink")