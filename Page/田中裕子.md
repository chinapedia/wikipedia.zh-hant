**田中裕子**（，），现名**泽田裕子**，[日本女](../Page/日本.md "wikilink")[演员](../Page/演员.md "wikilink")。丈夫是日本著名歌手及演员[泽田研二](../Page/泽田研二.md "wikilink")。

## 生平

田中裕子出生于[大阪府](../Page/大阪府.md "wikilink")[池田市](../Page/池田市.md "wikilink")，在[北海道](../Page/北海道.md "wikilink")[札幌上中学](../Page/札幌.md "wikilink")，从[藤女子短期大学中途退学](../Page/藤女子短期大学.md "wikilink")，之后从[明治大学文学部演剧學系毕业](../Page/明治大学.md "wikilink")。毕业论文题目是《女優論》（论女演员）。

1975年田中裕子在明治大学演剧专业学习期间加入日本三大剧团之一“[文学座](../Page/文学座.md "wikilink")”，开始表演事业。1983年，主演了[NHK电视台的大型](../Page/NHK.md "wikilink")[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")《[阿信](../Page/阿信.md "wikilink")》，得到了世界范围的知名度。1985年退出文学座之后，继续演出多部电影、电视剧。

2010年11月，獲頒[紫綬褒章](../Page/紫綬褒章.md "wikilink")。

## 婚姻

1982年，田中裕子因與[澤田研二拍攝電影](../Page/澤田研二.md "wikilink")《[男人真命苦之寅次郎落英繽紛](../Page/寅次郎的故事30_寅次郎落英缤纷.md "wikilink")》，傳介入澤田與[伊藤惠美的婚姻而被傳媒大肆報道](../Page/伊藤惠美.md "wikilink")，受到強烈譴責。1987年1月澤田與伊藤離婚。1989年11月12日，澤田迎娶田中，多年的感情困局得以開花結果。1999年曾傳分居。\[1\]。

## 作品

### 电影

  - 《ええじゃないか》（1981年，日本[幕末的群众运动的故事](../Page/幕末.md "wikilink")）
  - 《[北斋漫画](../Page/北斋漫画_\(电影\).md "wikilink")》（1981年）
  - 《火红的第五乐章》（1981年）
  - 《[男人真命苦之寅次郎落英繽紛](../Page/寅次郎的故事30_寅次郎落英缤纷.md "wikilink")》（港譯《男人之苦：戀愛專家》，與[澤田研二合演後結婚](../Page/澤田研二.md "wikilink")，1982年）
  - 《[跨越天城山](../Page/跨越天城山.md "wikilink")》（又译《[天城山奇案](../Page/天城山奇案.md "wikilink")》，[松本清张作品](../Page/松本清张.md "wikilink")，1983年）
  - 《[夜叉](../Page/夜叉.md "wikilink")》（1985年，与[高仓健合作](../Page/高仓健.md "wikilink")）
  - 《カポネ大いに泣く》（[松本清张作品](../Page/松本清张.md "wikilink")，與[澤田研二](../Page/澤田研二.md "wikilink")、[萩原健一合演](../Page/萩原健一.md "wikilink")，1983年）
  - 《二十四只眼睛》（1987年）
  - 《[呼啸山庄](../Page/呼啸山庄.md "wikilink")》（1988年，故事移植到[镰仓](../Page/镰仓时代.md "wikilink")[室町时代的日本](../Page/室町时代.md "wikilink")）
  - 《大阪物语》（與[澤田研二合演](../Page/澤田研二.md "wikilink")，1999年）
  - 《萤火虫》（2001年，与高仓健合作）
  - 《何时读书日》（2004年）
  - 《火火》（2005年）
  - 《[被埋葬的樹木](../Page/被埋葬的樹木.md "wikilink")》（2005年）
  - 《[無家可歸的中學生](../Page/無家可歸的中學生.md "wikilink")》（2008年）
  - 《[與春同行](../Page/與春同行.md "wikilink")》（2010年）
  - 《[只為了你](../Page/只為了你.md "wikilink")》（2012年）
  - 《[はじまりのみち](../Page/はじまりのみち.md "wikilink")》（2013年）
  - 《[深夜食堂](../Page/深夜食堂.md "wikilink")》（2015年）

### 電視劇

  - 《》（[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")，1979年）
  - 《[阿信](../Page/阿信_\(電視劇\).md "wikilink")》（飾演青年、中年阿信，1983年）
  - 《[宛如飛翔](../Page/宛如飛翔.md "wikilink")》（飾演[西鄉隆盛的妻子](../Page/西鄉隆盛.md "wikilink")，1990年）
  - 《[東京鐵塔：我的母親父親](../Page/東京鐵塔：我的母親父親.md "wikilink")》（2006年，與[廣末涼子合作](../Page/廣末涼子.md "wikilink")）
  - 《[蒼穹之昴](../Page/苍穹之昴_\(电视剧\).md "wikilink")》（飾[慈禧太后](../Page/慈禧太后.md "wikilink")，2010年）
  - 《[Mother](../Page/Mother_\(電視劇\).md "wikilink")》（飾[望月葉菜](../Page/望月葉菜.md "wikilink")，2010年）
  - 《[Woman](../Page/Woman.md "wikilink")》（飾[植杉紗千](../Page/植杉紗千.md "wikilink")，2013年）
  - 《[小希](../Page/小希.md "wikilink")》（飾桶作文，2015年）
  - 《》（飾松下佳世子，2017年）
  - 《[anone](../Page/anone.md "wikilink")》（飾林田亞乃音，2018年）

### 配音

  - 《[魔法公主](../Page/魔法公主.md "wikilink")》（1997年）（黑帽大人）
  - 《[地海戰記](../Page/地海戰記.md "wikilink")》（2006年）（蜘蛛）

### 话剧

  - 《[暴风雨](../Page/暴风雨.md "wikilink")》（[莎士比亚剧作](../Page/莎士比亚.md "wikilink")，1987年）
  - 《近松心中物語》（1989年）
  - 《泰尔亲王佩力克尔斯》（莎士比亚剧作，2004年）

等等。

## 获奖

  - [第5回日本電影金像獎](../Page/第5回日本電影金像獎.md "wikilink")（1981年）最佳女配角（[北齋漫畫](../Page/北齋漫畫_\(電影\).md "wikilink")、ええじゃないか）
  - 第24回[藍絲帶獎](../Page/藍絲帶獎_\(電影\).md "wikilink")（1981年）最佳女配角（ええじゃないか、北斎漫画）
  - 第6回[报知电影奖](../Page/报知电影奖.md "wikilink")（1981年）最佳女配角（北斎漫画）
  - 第26回藍絲帶獎（1983年）最佳女演员（跨越天城山）
  - 第57回[電影旬报奖](../Page/電影旬报.md "wikilink")（1983年）最佳女演员（跨越天城山）
  - 第38回毎日电影大奖（1983年）最佳女演员（跨越天城山）
  - 第14回高崎电影节（1999年）最佳女配角（大阪物语）
  - 第60回[每日电影奖](../Page/每日电影奖.md "wikilink")（2006年）最佳女演员（何时读书日、火火）
  - 第30回报知电影奖（2006年）最佳女演员（何时读书日、火火）
  - 第26回[橫濱電影節](../Page/橫濱電影節.md "wikilink")（2006年）最佳女主角（何时读书日、火火）
  - [第65回日劇學院賞最佳女配角](../Page/第65回日劇學院賞.md "wikilink")（[Mother](../Page/母親_\(電視劇\).md "wikilink")）
  - 2018年[CONFiDENCE日劇大獎年度最佳女配角獎](../Page/CONFiDENCE日劇大獎.md "wikilink")（[anone](../Page/anone.md "wikilink")）\[2\]

## 參考來源

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")
[Category:日本電影學院獎最佳女配角得主](../Category/日本電影學院獎最佳女配角得主.md "wikilink")
[Category:每日電影獎最佳女主角得主](../Category/每日電影獎最佳女主角得主.md "wikilink")
[Category:藍絲帶獎最佳女主角得主](../Category/藍絲帶獎最佳女主角得主.md "wikilink")
[Category:藍絲帶獎最佳女配角得主](../Category/藍絲帶獎最佳女配角得主.md "wikilink")
[Category:電影旬報十佳獎最佳女主角得主](../Category/電影旬報十佳獎最佳女主角得主.md "wikilink")
[Category:電影旬報十佳獎最佳女配角得主](../Category/電影旬報十佳獎最佳女配角得主.md "wikilink")
[Category:日劇學院賞最佳女配角得主](../Category/日劇學院賞最佳女配角得主.md "wikilink")
[Category:紫綬褒章獲得者](../Category/紫綬褒章獲得者.md "wikilink")
[Category:橫濱電影節最佳女主角得主](../Category/橫濱電影節最佳女主角得主.md "wikilink")
[Category:NHK晨間連續劇主演演員](../Category/NHK晨間連續劇主演演員.md "wikilink")
[Category:日本電影學院獎新人獎得主](../Category/日本電影學院獎新人獎得主.md "wikilink")
[Category:橫濱電影節最佳女配角得主](../Category/橫濱電影節最佳女配角得主.md "wikilink")
[Category:報知電影獎最佳女主角得主](../Category/報知電影獎最佳女主角得主.md "wikilink")
[Category:報知電影獎最佳女配角得主](../Category/報知電影獎最佳女配角得主.md "wikilink")
[Category:明治大學校友](../Category/明治大學校友.md "wikilink")
[Category:日刊體育電影大獎最佳女配角得主](../Category/日刊體育電影大獎最佳女配角得主.md "wikilink")
[Category:田中絹代獎得主](../Category/田中絹代獎得主.md "wikilink")
[Category:CONFiDENCE日劇大獎最佳女配角得主](../Category/CONFiDENCE日劇大獎最佳女配角得主.md "wikilink")

1.  [伊藤エミさん死去 沢田研二と離婚
    慰謝料は18億円超も沢田姓で通す](http://www.sponichi.co.jp/entertainment/news/2012/06/28/kiji/K20120628003561920.html)
2.