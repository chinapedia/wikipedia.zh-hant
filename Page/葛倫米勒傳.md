《**葛倫米勒傳**》（***The Glenn Miller
Story***），1953年[安東尼·曼執導的美國電影](../Page/安東尼·曼.md "wikilink")。由[詹姆斯·史都華](../Page/詹姆斯·史都華.md "wikilink")、[裘·艾莉森等主演](../Page/:en:June_Allyson.md "wikilink")。這是一部結合電影與音樂藝術的經典之作。

《葛倫米勒傳》是[爵士樂搖擺大樂團](../Page/:en:Jazz.md "wikilink")（Big band
swing）作曲家兼指揮家[葛倫·米勒的傳記影片](../Page/:en:Glenn_Miller.md "wikilink")。描述葛倫·米勒原本是一位落魄而默默無聞的伸縮喇叭手，憑著才氣及對理想的堅持，再加上聰明賢慧的妻子與其他工作夥伴的幫助下，其作品終於在藝術和商業上都得到高度的肯定。葛倫·米勒後來在[二次大戰期間](../Page/二次大戰.md "wikilink")，與其樂團成員加入[美國陸軍航空軍](../Page/美國陸軍航空軍.md "wikilink")，在軍中成立樂團，前往歐洲各地對盟軍從事勞軍活動。

現實中的詹姆斯·史都華在二次大戰時，是美國陸軍航空軍（[美國空軍的前身](../Page/美國空軍.md "wikilink")）的飛行員，因此劇中葛倫·米勒所穿的陸航隊軍官制服，也就與他當年所穿的軍服相同型式。演出葛倫·米勒妻子的裘·艾莉森素有「銀幕上的好太太」之稱。

## 片中出現曲目

1.  "[月光小夜曲](../Page/:en:Moonlight_Serenade_\(song\).md "wikilink")"
2.  "[Tuxedo Junction](../Page/:en:Tuxedo_Junction.md "wikilink")"
3.  "[Little Brown Jug](../Page/:en:Little_Brown_Jug.md "wikilink")"
4.  "St. Louis Blues — March"
5.  "Basin Street Blues"
6.  "[In the Mood](../Page/:en:In_the_Mood.md "wikilink")"
7.  "String of Pearls"
8.  "[PEnnsylvania
    6-5000](../Page/:en:PEnnsylvania_6-5000.md "wikilink")"
9.  "American Patrol"
10. "Otchi-Tchor-Ni-Ya"

## 音樂家的客串

多位爵士樂音樂家本人親自在此片客串演出。包括[路易斯·阿姆斯壯](../Page/路易斯·阿姆斯壯.md "wikilink")、[金·克拉巴](../Page/:en:Gene_Krupa.md "wikilink")、[法蘭西絲·藍福](../Page/:en:Frances_Langford.md "wikilink")、[巴尼·畢加](../Page/:en:Barney_Bigard.md "wikilink")、[Cozy
Cole](../Page/:en:Cozy_Cole.md "wikilink")、[Marty
Napoleon](../Page/:en:Marty_Napoleon.md "wikilink")、[Ben
Pollack](../Page/:en:Ben_Pollack.md "wikilink")、[Babe
Russin](../Page/:en:Babe_Russin.md "wikilink")、Arvell Shaw、James Young
(IV)、[現代人樂團等](../Page/:en:The_Modernaires.md "wikilink")。

## 得獎

  - 《葛倫米勒傳》上映時得到巨大的票房成功，電影原聲帶則與電影一般的受歡迎。

<!-- end list -->

  - 《葛倫米勒傳》得到1954年[奧斯卡金像獎的](../Page/奧斯卡金像獎.md "wikilink")[最佳錄音音乐奖](../Page/奧斯卡最佳錄音音乐奖.md "wikilink")，也得到兩項提名：[最佳原创剧本奖及](../Page/奥斯卡最佳原创剧本奖.md "wikilink")[最佳原创音乐奖](../Page/奧斯卡最佳原创音乐奖.md "wikilink")。

<!-- end list -->

  - 詹姆斯·史都華提名[英國電影和電視藝術學院最佳外國男演員獎](../Page/英國電影和電視藝術學院.md "wikilink")。

## 外部連結

  - [*The Glenn Miller Story* at the Internet Movie
    Database](http://www.imdb.com/title/tt0047030/)
  - [Awards won by *The Glenn Miller
    Story*](http://www.imdb.com/title/tt0047030/awards)
  - [Review of *The Glenn Miller Story* at The
    Shelf](http://randomshelf.blogspot.com/2006/03/in-mood)
  - [The Films of Anthony
    Mann](https://web.archive.org/web/20070509083947/http://home.aol.com/MG4273/mann.htm)

[Category:1953年電影](../Category/1953年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1950年代劇情片](../Category/1950年代劇情片.md "wikilink")
[Category:美國傳記片](../Category/美國傳記片.md "wikilink")
[Category:美國浪漫劇情片](../Category/美國浪漫劇情片.md "wikilink")
[Category:第二次世界大戰電影](../Category/第二次世界大戰電影.md "wikilink")
[Category:爵士樂題材電影](../Category/爵士樂題材電影.md "wikilink")
[Category:音樂及音樂家題材電影](../Category/音樂及音樂家題材電影.md "wikilink")
[Category:紐約市背景電影](../Category/紐約市背景電影.md "wikilink")
[Category:1920年代背景電影](../Category/1920年代背景電影.md "wikilink")
[Category:1930年代背景電影](../Category/1930年代背景電影.md "wikilink")
[Category:环球影业电影](../Category/环球影业电影.md "wikilink")
[Category:奥斯卡最佳音响效果获奖电影](../Category/奥斯卡最佳音响效果获奖电影.md "wikilink")