**陳茂男**（），[台灣](../Page/台灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")。曾經代表[民主進步黨任職第五屆](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，第六屆立法委員选举中落选，未能连任。

## 外部連結

  - [立法院
    陳茂男委員簡介](https://web.archive.org/web/20070930200844/http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00105&stage=5)

[C陳](../Category/第5屆中華民國立法委員.md "wikilink")
[C陳](../Category/民主進步黨黨員.md "wikilink")
[M](../Category/陳姓.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")