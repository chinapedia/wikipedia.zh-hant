**OMD**，全名**Orchestral Manoeuvres in the
Dark**（中文通常翻译为**夜半行军乐团**或**夜行者乐团**）是80年代的英國[流行](../Page/流行樂.md "wikilink")[電子音樂團體](../Page/電子音樂.md "wikilink")，在其33年的职业音乐道路上，OMD的作品对当时的[新浪潮与](../Page/新浪潮.md "wikilink")[后朋克音乐运动影响深远](../Page/后朋克.md "wikilink")。OMD樂團起源於[英格蘭西北部的](../Page/英格蘭.md "wikilink")[默西赛德郡](../Page/默西赛德郡.md "wikilink")，由英國[維京唱片公司所屬](../Page/維京唱片.md "wikilink")。著名音樂作品有1982年電子樂搭配[蘇格蘭](../Page/蘇格蘭.md "wikilink")[風笛的](../Page/風笛.md "wikilink")《[聖女貞德](../Page/聖女貞德.md "wikilink")》（）、[新浪漫派樂風代表情歌](../Page/新浪漫主义_\(歌曲\).md "wikilink")《妳若離開》（）、[反战歌曲](../Page/反战歌曲.md "wikilink")《[艾诺拉·盖](../Page/艾诺拉·盖号轰炸机.md "wikilink")》（）等。

樂團最初由与共同成立，之後兩人成為核心人物。1989年樂團分裂後，Andy McCluskey帶領新成員繼續以OMD名義活動。

## 相關聯結

  - [官方網站](http://www.omd.uk.com)
  - [Worldwide Discography
    Website](https://web.archive.org/web/20141218201954/http://omd.eu.com/)

## 参考文献

[Category:電子音樂團體](../Category/電子音樂團體.md "wikilink")
[Category:英國乐团](../Category/英國乐团.md "wikilink")
[Category:2006年復合的音樂團體](../Category/2006年復合的音樂團體.md "wikilink")
[Category:實驗音樂樂團](../Category/實驗音樂樂團.md "wikilink")