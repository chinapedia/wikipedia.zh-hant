**酪氨酸**（Tyrosine, 縮寫為 **Tyr** or **Y**）\[1\] 或 **4 - 羥基苯丙氨酸**,
是[細胞用來合成](../Page/細胞.md "wikilink")[蛋白質的](../Page/蛋白質.md "wikilink")22種[胺基酸之一](../Page/胺基酸.md "wikilink")，在細胞中可用於合成蛋白質，其[密碼子為UAC和UAU](../Page/密碼子.md "wikilink")，屬於含有極性側基，人體可自行合成的非必需胺基酸。單詞“酪氨酸”是來自[希臘語](../Page/希臘語.md "wikilink")
tyros，意思[奶酪](../Page/奶酪.md "wikilink")。19世紀初被德國的化學家[尤斯图斯·冯·李比希首先在起司的](../Page/尤斯图斯·冯·李比希.md "wikilink")[酪蛋白中發現](../Page/酪蛋白.md "wikilink")，\[2\]\[3\]
，當用作於[官能基或側基時則稱做酪氨酰](../Page/官能基.md "wikilink")。

## 功能

除了是一個[蛋白質氨基酸](../Page/蛋白質氨基酸.md "wikilink")，在蛋白質中的訊號傳導過程中，酪氨酸憑藉著[酚官能基具有特殊作用](../Page/酚.md "wikilink")，其功能為被[蛋白激酶](../Page/蛋白激酶.md "wikilink")（所謂的[酪氨酸激酶受體](../Page/酪氨酸激酶受體.md "wikilink")）[信號轉移的磷酸基的受器](../Page/信號轉移.md "wikilink")，而羥基的磷酸化改變的目標蛋白質的活性。

酪氨酸在[光合作用中也扮演重要角色](../Page/光合作用.md "wikilink")，在[葉綠體](../Page/葉綠體.md "wikilink")（[光系統II](../Page/光系統II.md "wikilink")）中，在氧化[葉綠素的](../Page/葉綠素.md "wikilink")[還原反應中被當作電子提供者](../Page/還原反應.md "wikilink")，使其進行酚醛OH-基團的[去質子化](../Page/去質子化.md "wikilink")，最後在光系統II中被四個核心錳簇還原。

## 飲食來源

酪氨酸可在體內由[苯丙氨酸合成](../Page/苯丙氨酸.md "wikilink")，可在許多高[蛋白質食品如如](../Page/蛋白質.md "wikilink")[雞](../Page/雞.md "wikilink")，[火雞](../Page/火雞.md "wikilink")，[魚](../Page/魚.md "wikilink")，[牛奶](../Page/牛奶.md "wikilink")，[酸奶](../Page/酸奶.md "wikilink")，[奶酪](../Page/奶酪.md "wikilink")，[乾酪](../Page/乾酪.md "wikilink")，[花生](../Page/花生.md "wikilink")，[杏仁](../Page/杏仁.md "wikilink")，[南瓜籽](../Page/南瓜籽.md "wikilink")，[芝麻](../Page/芝麻.md "wikilink")，[大豆](../Page/大豆.md "wikilink")，[利馬豆](../Page/利馬豆.md "wikilink")，[鱷梨和](../Page/鱷梨.md "wikilink")[香蕉中被發現](../Page/香蕉.md "wikilink")。\[4\]

## 生物合成

[Tyrosine_biosynthesis.svg](https://zh.wikipedia.org/wiki/File:Tyrosine_biosynthesis.svg "fig:Tyrosine_biosynthesis.svg")

在微生物或植物中，酪氨酸透過[預苯酸](../Page/預苯酸.md "wikilink")（一種[莽草酸反應途徑產生的中間產物](../Page/莽草酸途径.md "wikilink")）產生，預苯酸被保留的[羥基](../Page/羥基.md "wikilink")[氧化脫羧得到對](../Page/氧化脫羧.md "wikilink")-羥基苯基丙酮酸，酪氨酸和[α-酮戊二酸利用](../Page/α-酮戊二酸.md "wikilink")[谷氨酸作為氮源進行](../Page/谷氨酸.md "wikilink")[氨基轉移反應產生對](../Page/氨基轉移反應.md "wikilink")-羥基苯基丙酮酸。

[哺乳動物利用來自食物的必需氨基酸](../Page/哺乳動物.md "wikilink")[苯丙氨酸](../Page/苯丙氨酸.md "wikilink")（PHE）來合成酪氨酸，苯丙胺酸轉換成酪氨酸的反應由[丙氨酸羥化酶](../Page/丙氨酸羥化酶.md "wikilink")（一種單氧化酶）催化而成，這種酶催化反應造成羥基和[苯丙氨酸的](../Page/苯丙氨酸.md "wikilink")6-碳環的芳香環端部的加成反應，使之變成酪氨酸。

## 代謝反應

[Conversion_of_phenylalanine_and_tyrosine_to_its_biologically_important_derivatives.png](https://zh.wikipedia.org/wiki/File:Conversion_of_phenylalanine_and_tyrosine_to_its_biologically_important_derivatives.png "fig:Conversion_of_phenylalanine_and_tyrosine_to_its_biologically_important_derivatives.png")和酪氨酸轉化成它的生物學上重要的[衍生物](../Page/衍生物.md "wikilink")(最終代謝成[兒茶酚胺和胺化合物](../Page/兒茶酚胺.md "wikilink"))。\]\]

### 磷酸化和硫酸鹽化

某些酪氨酸可以用[蛋白激酶的磷酸基標註](../Page/蛋白激酶.md "wikilink")，酪氨酸在[磷酸化形式時被稱作磷酸酪氨酸](../Page/磷酸化.md "wikilink")，酪氨酸磷酸化被認為是信號轉導和酶活性調控中的關鍵步驟之一，磷酸酪氨酸可以被特定[抗體所偵測](../Page/抗體.md "wikilink")，酪氨酸也可以被添加的硫酸基修飾，這個反應過程稱為[酪氨酸硫酸鹽化](../Page/酪氨酸硫酸鹽化.md "wikilink")\[5\]
[酪氨酸硫酸鹽化是由酪氨酰蛋白質中的磺基轉移酶](../Page/酪氨酸硫酸鹽化.md "wikilink")（TPST）所催化而成，如同上面所提到的磷酸酪氨酸抗體，此抗體具有特異性可偵測到磺基酪氨酸。

### 神經傳遞物質和激素的前驅物

在[大腦中的多巴胺細胞中](../Page/大腦.md "wikilink")，酪氨酸被[酶](../Page/酶.md "wikilink")[酪氨酸羥化酶](../Page/酪氨酸羥化酶.md "wikilink")（TH）轉化成[左旋多巴胺](../Page/左旋多巴胺.md "wikilink")，酶酪氨酸羥化酶是一種具有[速率限制酶](../Page/速率限制酶.md "wikilink")，被使用於[神經傳遞物質](../Page/神經傳遞物質.md "wikilink")[多巴胺的合成過程中](../Page/多巴胺.md "wikilink")，多巴胺可以被轉化成[兒茶酚胺](../Page/兒茶酚胺.md "wikilink")[去甲腎上腺素](../Page/去甲腎上腺素.md "wikilink")（去甲腎上腺素）和[腎上腺素](../Page/腎上腺素.md "wikilink")（腎上腺素）。

[甲狀腺激素](../Page/甲狀腺.md "wikilink")[三碘甲狀腺氨酸](../Page/三碘甲狀腺氨酸.md "wikilink")
(T<sub>3</sub>)和在[甲狀腺](../Page/甲狀腺.md "wikilink")[膠體中的](../Page/膠體.md "wikilink")[甲狀腺素](../Page/甲狀腺素.md "wikilink")
(T<sub>4</sub>)也來自於酪氨酸。

就化學而言，[腎上腺素是所謂](../Page/腎上腺素.md "wikilink")[兒茶酚胺的一組](../Page/兒茶酚胺.md "wikilink")[單胺之一](../Page/單胺.md "wikilink")。它從[氨基酸](../Page/氨基酸.md "wikilink")[苯丙氨酸和酪氨酸於中樞神經系統的一些](../Page/苯丙氨酸.md "wikilink")[神經元](../Page/神經元.md "wikilink")，及在[腎上腺髓質的嗜鉻細胞產生](../Page/腎上腺.md "wikilink")。

### 生物鹼的前驅物

[罌粟科催眠物質中的膠乳又稱為罌粟](../Page/罌粟科催眠物質.md "wikilink")，已被證實能將酪氨酸轉換成[生物鹼](../Page/生物鹼.md "wikilink")[嗎啡](../Page/嗎啡.md "wikilink")，在其生物合成反應途徑中，酪氨酸利用碳-14放射性標記的酪氨酸追蹤體內合成路徑。

[三甲氧苯乙胺生產](../Page/三甲氧苯乙胺.md "wikilink")[仙人掌生物合成酪氨酸](../Page/仙人掌.md "wikilink")。
\[6\]

### 天然酚類的前驅物

[酪氨酸解氨酶](../Page/酪氨酸解氨酶.md "wikilink")（TAL）是一種在天然酚生物合成反應途徑中的酶，此酶將左旋酪氨酸轉換成[對香豆酸](../Page/對香豆酸.md "wikilink")。

### 色素的前驅物

酪氨酸也是色素中的[黑色素的前驅物](../Page/黑色素.md "wikilink")。

### 降解作用

[Tyrosinedegradation2.png](https://zh.wikipedia.org/wiki/File:Tyrosinedegradation2.png "fig:Tyrosinedegradation2.png")和[富馬酸](../Page/富馬酸.md "wikilink")。在分解過程中兩個雙加氧酶是必要物質，最終產物都會進入[檸檬酸循環](../Page/檸檬酸循環.md "wikilink")。\]\]

左旋酪氨酸（對-羥基苯丙氨酸）的分解作用，為酪氨酸轉氨酶變成對-羥基苯基[丙酮酸的α](../Page/丙酮酸.md "wikilink")-酮戊二酸的依賴性[轉移反應而成](../Page/轉移反應.md "wikilink")，其結合位置為對位，縮寫為p，表示苯環上的羥基和側鏈是對面方位（見下圖）。

下一步的氧化步驟中，由對-羥基苯基丙酮酸-雙加氧酶和二氧化碳[尿黑酸](../Page/尿黑酸.md "wikilink")（2,5-二羥基苯基-1-乙酸乙酯）裂斷催化而成，為了拆開尿黑酸的芳香環，通過氧分子進一步的結合以得到尿黑酸加氧酶是必須。

富馬酰乙酸經由氧化的羥基所造成的羧基旋轉，而產生maleylacetoacetate
-順式-反式異構酶。含有[穀胱甘肽的順](../Page/穀胱甘肽.md "wikilink")-反異構酶作為[輔酶](../Page/輔酶.md "wikilink")，經由添加水分子，延胡索酰乙酰乙酸最終被[延胡索酰乙酰乙酸水解酶給裂斷](../Page/延胡索酰乙酰乙酸水解酶.md "wikilink")。

[富馬酸鹽](../Page/富馬酸鹽.md "wikilink")（也是檸檬酸循環的代謝產物）和[乙酰乙酸酯](../Page/乙酰乙酸酯.md "wikilink")（3-丁酮酸）為游離狀態，乙酰乙酸酯是一種酮，其被琥珀酰-CoA活化後可以被轉化成[乙酰-CoA](../Page/乙酰-CoA.md "wikilink")，反過來又可被[檸檬酸循環氧化或用於](../Page/檸檬酸循環.md "wikilink")[脂肪酸合成](../Page/脂肪酸合成.md "wikilink")。

[Phloretic酸也是大鼠中泌尿代謝物的酪氨酸](../Page/Phloretic酸.md "wikilink")。\[7\]

## 甜菜鹼

[Betain-Tyrosin.png](https://zh.wikipedia.org/wiki/File:Betain-Tyrosin.png "fig:Betain-Tyrosin.png")

## 鄰位和間位酪氨酸

已知左旋酪氨酸的三種[結構異構物](../Page/結構異構物.md "wikilink")，除了常見的[對位異構物左旋酪氨酸](../Page/對位異構物.md "wikilink")（對-酪氨酸或4-羥基苯丙氨酸）之外，另有兩個區域異構物稱為間位酪氨酸（間-酪氨酸，3-羥基苯丙氨酸或LM-酪氨酸）和鄰位酪氨酸（鄰-酪氨酸或2-羥基苯丙氨酸），兩者皆自然存在，間-酪氨酸和鄰-酪氨酸皆為罕見的，須經由苯丙氨酸的[氧化壓力條件下進行的非酶](../Page/氧化壓力.md "wikilink")[自由基羥化增加數量](../Page/自由基.md "wikilink")。\[8\]\[9\]

間-酪氨酸及其類似物（在自然中罕見但可以人工合成）在[帕金森氏症](../Page/帕金森氏症.md "wikilink")，[阿茲海默症和](../Page/阿茲海默症.md "wikilink")[關節炎皆有所應用](../Page/關節炎.md "wikilink")。\[10\]
[酪氨酸由苯丙氨酸羥化酶酶氧化（頂部）和
通過羥基自由基非酶氧化（中間和底部）。](https://zh.wikipedia.org/wiki/File:Phe_Tyr.png "fig:酪氨酸由苯丙氨酸羥化酶酶氧化（頂部）和 通過羥基自由基非酶氧化（中間和底部）。")

## 藥物用途

酪氨酸為[神經傳遞物質和增加血漿神經傳遞物質程度](../Page/神經傳遞物質.md "wikilink")（尤其是多巴胺和去甲腎上腺素）的前驅物，\[11\]副作用為易對情緒造成影響，\[12\]\[13\]\[14\]使在壓力狀況下對情緒的影響更為明顯。

許多研究發現在壓力，寒冷，疲勞，\[15\]失去至親如死亡或離婚，長時間工作和睡眠剝奪的狀況下酪氨酸可以發揮作用，\[16\]\[17\]可以減少壓力激素的程度，\[18\]在[動物試驗中可以看出經由壓力誘導的體重下降反應來減少這些情況](../Page/動物試驗.md "wikilink")，\[19\]改善認知與物理性功能，\[20\]\[21\]\[22\]在人類試驗中，因為[酪氨酸水解酶為速度限制酶](../Page/酪氨酸水解酶.md "wikilink")，影響較[左旋多巴胺不明顯](../Page/左旋多巴胺.md "wikilink")。

酪氨酸在正常情況下對情緒，認知或物理性功能並無明顯影響，\[23\]\[24\]\[25\]
在臨床試驗中每日成人劑量大約是100 毫克/公斤，相當於約6.8 克，\[26\]通常劑量約為每天500-1500 毫克（大多數製造商建議劑量通常是相當於1-3粒純酪氨酸），不建議超過每天12000 毫克（12克）。

## 参阅

  - [白化症](../Page/白化症.md "wikilink")

  - [黑尿症](../Page/黑尿症.md "wikilink")

  - [Betalain](../Page/Betalain.md "wikilink")

  - [碘化酪氨酸衍生物](../Category/碘化酪氨酸衍生物.md "wikilink")

  - [酪胺](../Page/酪胺.md "wikilink")

  -
  - [Tyrosinemia](../Page/Tyrosinemia.md "wikilink")

  - [Pauly reaction](../Page/Pauly_reaction.md "wikilink")

## 参考资料

## 外部链接

  - [Tyrosine MS
    Spectrum](http://gmd.mpimp-golm.mpg.de/Spectrums/e8b4de66-fbb5-4629-9e12-0cce4503c881.aspx)

  - [Tyrosine
    metabolism](http://www.genome.jp/kegg/pathway/map/map00350.html)

  - [Phenylalanine and tyrosine
    biosynthesis](http://www.chem.qmul.ac.uk/iubmb/enzyme/reaction/AminoAcid/PheTyr.html)

  - [Phenylalanine, Tyrosine, and tryptophan
    biosynthesis](http://www.genome.jp/kegg/pathway/map/map00400.html)

  -
[Category:蛋白氨基酸](../Category/蛋白氨基酸.md "wikilink")
[Category:生糖氨基酸](../Category/生糖氨基酸.md "wikilink")
[Category:生酮氨基酸](../Category/生酮氨基酸.md "wikilink")
[Category:芳香族氨基酸](../Category/芳香族氨基酸.md "wikilink")
[Category:酚](../Category/酚.md "wikilink")

1.

2.

3.

4.

5.

6.

7.  Urinary phenolic acid metabolities of tyrosine. Booth A N, Masri M
    S, Robbins D J, Emerson O H, Jones F T and Deeds F, Journal of
    Biological Chemistry, 1960, Vol. 235, pages 2649-2652
    ([article](http://www.jbc.org/content/235/9/2649.citation))

8.

9.

10. *Optimized Synthesis of L-m-Tyrosine Suitable for Chemical Scale-Up*
    Cara E. Humphrey, Markus Furegati, Kurt Laumen, Luigi La Vecchia,
    Thomas Leutert, J. Constanze D. Müller-Hartwieg, and Markus Vögtle
    Organic Process Research & Development **2007**, 11, 1069–1075

11.

12.

13.

14.

15.

16.

17.

18.

19.
20.
21.

22.

23.

24.

25.

26.