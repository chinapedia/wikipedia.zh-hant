**Stand
Up\!\!**（[日語](../Page/日語.md "wikilink")：****）是[日本](../Page/日本.md "wikilink")[TBS電視台的一部](../Page/TBS電視台.md "wikilink")[電視連續劇](../Page/日本電視劇.md "wikilink")，從2003年7月4日開始到2003年9月12日止播出，共11集。背景是描寫日本[東京都](../Page/東京都.md "wikilink")[品川區](../Page/品川區.md "wikilink")[戶越的](../Page/戶越.md "wikilink")[高中生們的青春喜劇](../Page/高中生.md "wikilink")。

海外播出方面，[台灣分別由](../Page/台灣.md "wikilink")[TVBS-G與](../Page/TVBS歡樂台.md "wikilink")[緯來日本台](../Page/緯來日本台.md "wikilink")，於2004年5月29日晚間7時及2008年2月19日晚間8時播出。2003年12月19日在日本發售『Stand
Up\!\!』[DVD-Video](../Page/DVD-Video.md "wikilink")。

## 主要角色

  - **淺井正平**：[二宮和也飾](../Page/二宮和也.md "wikilink")（[嵐的成員](../Page/嵐.md "wikilink")）

<!-- end list -->

  -
    小正。DB4的成員之一。對望月老師隱約有感情寄托，而且常常在幻想。

<!-- end list -->

  - **岩崎健吾**：[山下智久飾](../Page/山下智久.md "wikilink")（演出時是[Johnny's
    Jr](../Page/傑尼斯事務所.md "wikilink")）

<!-- end list -->

  -
    健健。正平的好友、DB4的成員之一。四人中唯一有女友的人。

<!-- end list -->

  - **大和田千繪**：[鈴木杏飾](../Page/鈴木杏.md "wikilink")

<!-- end list -->

  -
    千繪。和正平們是青梅竹馬。

<!-- end list -->

  - **宇田川隼人**：[成宮寬貴飾](../Page/成宮寬貴.md "wikilink")

<!-- end list -->

  -
    宇田仔。正平的好友、DB4的成員之一。輕音樂社擔任主唱。

<!-- end list -->

  - **江波功司**：[小栗旬飾](../Page/小栗旬.md "wikilink")

<!-- end list -->

  -
    功司。正平的好友、DB4的成員之一。足球社的隊長。

### DB4的家人

  - **淺井京平**：[段田安則飾](../Page/段田安則.md "wikilink")

<!-- end list -->

  -
    正平的父親。經營淺井藥局。

<!-- end list -->

  - **淺井倫子**：[片平渚飾](../Page/片平渚.md "wikilink")

<!-- end list -->

  -
    正平的母親。

<!-- end list -->

  - **淺井百合子**：[西田尚美飾](../Page/西田尚美.md "wikilink")

<!-- end list -->

  -
    正平的姊姊。工作是護士。

<!-- end list -->

  - **岩崎君子**：[杉田薰飾](../Page/杉田薰.md "wikilink")

<!-- end list -->

  -
    健吾的母親。經營旅館。

<!-- end list -->

  - **宇田川信人**：[酒井敏也飾](../Page/酒井敏也.md "wikilink")

<!-- end list -->

  -
    隼人的父親。經營蔬果店。

<!-- end list -->

  - **江波昌司**：[清水章吾飾](../Page/清水章吾.md "wikilink")

<!-- end list -->

  -
    功司的父親。經營仙貝店。里長會的會長。

### 高中的老師

  - **望月五十鈴**：[釋由美子飾](../Page/釋由美子.md "wikilink")

<!-- end list -->

  -
    高中的英語老師。

<!-- end list -->

  - **木村光彥**：[的場浩司飾](../Page/的場浩司.md "wikilink")

<!-- end list -->

  -
    高中的體育老師。正平的班級導師。

<!-- end list -->

  - **佐佐木留美子**：[加藤貴子飾](../Page/加藤貴子.md "wikilink")

<!-- end list -->

  -
    高中的保健室阿姨。喜歡木村老師。

### 高中同學

  - **加賀艾蓮**：[Becky飾](../Page/Becky_\(日本藝人\).md "wikilink")

<!-- end list -->

  -
    父親有一半是美國人。

<!-- end list -->

  - **藤澤園子**：[上野なつひ飾](../Page/上野なつひ.md "wikilink")

<!-- end list -->

  -
    健吾的女友。優等生。

<!-- end list -->

  - **倉田美智子**：[邑野未亜](../Page/邑野未亜.md "wikilink")（現・[邑野みあ](../Page/邑野みあ.md "wikilink")）
  - **齊藤輝美**：[水野はるか](../Page/水野はるか.md "wikilink")（現・[水野友加里](../Page/水野友加里.md "wikilink")）

<!-- end list -->

  -
    在咖啡廳打工。

<!-- end list -->

  - **櫻井真紀子**：[AKINA飾](../Page/AKINA.md "wikilink")（Folder5）

<!-- end list -->

  -
    真紀。女生四人組的領導人。

<!-- end list -->

  - **上原美緒**：[芦名星飾](../Page/芦名星.md "wikilink")

<!-- end list -->

  -
    美緒。

<!-- end list -->

  - **濱野恭子**：[香子飾](../Page/香子.md "wikilink")（ちぇきな）

<!-- end list -->

  -
    恭子。

<!-- end list -->

  - **木田亜紀子**：[真琴飾](../Page/真琴.md "wikilink")（ちぇきな）

<!-- end list -->

  -
    亜子。

### 其他角色

  - **久米直也**：[塚本高史飾](../Page/塚本高史.md "wikilink")

<!-- end list -->

  -
    私立高中的學生會會長。足球社的王牌球員。會給DB4重要寶貴的意見。

<!-- end list -->

  - **富永志保**：[松本莉緒飾](../Page/松本莉緒.md "wikilink")

<!-- end list -->

  -
    商店街糖果屋少女。

<!-- end list -->

  - **橘廣美**：[劇團一人飾](../Page/劇團一人.md "wikilink")

<!-- end list -->

  -
    咖啡廳的負責人。

<!-- end list -->

  - **水澤**：[郭智博飾](../Page/郭智博.md "wikilink")

<!-- end list -->

  -
    與千繪是高中同學。

<!-- end list -->

  - **美由紀**：[水川麻美飾](../Page/水川麻美.md "wikilink")
  - **シン**：[DAIGO飾](../Page/DAIGO.md "wikilink")

<!-- end list -->

  -
    真紀子的男友。

<!-- end list -->

  - [丸岡槳詞](../Page/丸岡槳詞.md "wikilink")
  - [清水恵美](../Page/清水恵美.md "wikilink")
  - [おかやまはじめ](../Page/おかやまはじめ.md "wikilink")
  - [勝又ユキ](../Page/勝又ユキ.md "wikilink")
  - [山本梓](../Page/山本梓.md "wikilink")
  - [石川佳奈](../Page/石川佳奈.md "wikilink")

## 製作團隊

  - 劇本：[金子亞里沙](../Page/金子亞里沙.md "wikilink")
  - 製作人：[石丸彰彥](../Page/石丸彰彥.md "wikilink")
  - 導演：[堤幸彥](../Page/堤幸彥.md "wikilink")、[加藤新](../Page/加藤新.md "wikilink")、[平川雄一朗](../Page/平川雄一朗.md "wikilink")
  - 企画：[植田博樹](../Page/植田博樹.md "wikilink")
  - 制作：[TBS ENTERTAINMENT](../Page/TBS電視.md "wikilink")
  - 製作著作：TBS

## 主題曲

  - [嵐](../Page/嵐.md "wikilink")「」（2003年9月3日由日本[J
    Storm唱片發行](../Page/J_Storm.md "wikilink")）

## 各集日本首播收視率

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>中文翻譯標題</p></th>
<th><p>日文原文標題</p></th>
<th><p>日本播放時間</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第1話</p></td>
<td><p>17歲的性體驗</p></td>
<td></td>
<td><p>2003年7月4日</p></td>
<td><p>14.0%</p></td>
</tr>
<tr class="odd">
<td><p>第2話</p></td>
<td><p>（極秘）外宿計劃</p></td>
<td></td>
<td><p>2003年7月11日</p></td>
<td><p>12.3%</p></td>
</tr>
<tr class="even">
<td><p>第3話</p></td>
<td><p>大人們不在家的晚上！</p></td>
<td></td>
<td><p>2003年7月18日</p></td>
<td><p>11.6%</p></td>
</tr>
<tr class="odd">
<td><p>第4話</p></td>
<td><p>緊張的初體驗！</p></td>
<td></td>
<td><p>2003年7月25日</p></td>
<td><p>　7.5%</p></td>
</tr>
<tr class="even">
<td><p>第5話</p></td>
<td><p>終於出現第一位從處男畢業的人</p></td>
<td></td>
<td><p>2003年8月1日</p></td>
<td><p>　9.7%</p></td>
</tr>
<tr class="odd">
<td><p>第6話</p></td>
<td><p>像是被火紋身般的戀愛！</p></td>
<td></td>
<td><p>2003年8月8日</p></td>
<td><p>10.4%</p></td>
</tr>
<tr class="even">
<td><p>第7話</p></td>
<td><p>純潔VS戀愛教主</p></td>
<td></td>
<td><p>2003年8月15日</p></td>
<td><p>　7.9%</p></td>
</tr>
<tr class="odd">
<td><p>第8話</p></td>
<td><p>高中生的懷孕問題</p></td>
<td></td>
<td><p>2003年8月22日</p></td>
<td><p>　8.4%</p></td>
</tr>
<tr class="even">
<td><p>第9話</p></td>
<td><p>被親友奪去的戀情</p></td>
<td></td>
<td><p>2003年8月29日</p></td>
<td><p>　8.9%</p></td>
</tr>
<tr class="odd">
<td><p>第10話</p></td>
<td><p>傷痕累累的初體驗</p></td>
<td></td>
<td><p>2003年9月5日</p></td>
<td><p>11.3%</p></td>
</tr>
<tr class="even">
<td><p>第11話</p></td>
<td></td>
<td></td>
<td><p>2003年9月12日</p></td>
<td><p>　9.9%</p></td>
</tr>
</tbody>
</table>

  - 平均收視率 10.2%

## 外部連結

  - [緯來日本台《Stand
    Up\!\!》官方網站](http://japan.videoland.com.tw/channel/standup/)

## 作品的變遷

[Category:2003年開播的日本電視劇集](../Category/2003年開播的日本電視劇集.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:性題材電視劇](../Category/性題材電視劇.md "wikilink")
[Category:日本校園劇](../Category/日本校園劇.md "wikilink")
[Category:高中背景電視劇](../Category/高中背景電視劇.md "wikilink")
[Category:東京背景電視劇](../Category/東京背景電視劇.md "wikilink")
[Category:品川區背景作品](../Category/品川區背景作品.md "wikilink")
[Category:二宮和也](../Category/二宮和也.md "wikilink")