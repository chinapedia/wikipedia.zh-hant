二代目**松山會**位於[愛媛縣](../Page/愛媛縣.md "wikilink")[松山市](../Page/松山市.md "wikilink")[道後](../Page/道後.md "wikilink")[湯月本部的設置地](../Page/湯月.md "wikilink")，[日本的](../Page/日本.md "wikilink")[黑社會指定暴力團之一](../Page/黑社會.md "wikilink")、六代目[山口組旗下的](../Page/山口組.md "wikilink")2次團體。正式成員約280人。

松山會的前身為“松山連合会”，初期由[岡本會](../Page/岡本會.md "wikilink")、[兵藤會](../Page/兵藤會.md "wikilink")、[伊藤會](../Page/伊藤會.md "wikilink")、[石鐵會](../Page/石鐵會.md "wikilink")、大西組5團體組成。

2012年11月 正田悟宣告引退、組織解散。

## 略年表

  - 1989年1月[愛媛縣](../Page/愛媛縣.md "wikilink")[松山的岡本會](../Page/松山市.md "wikilink")（會長・[岡本雅博](../Page/岡本雅博.md "wikilink")）、二代目兵藤會（會長・[大谷勝治](../Page/大谷勝治.md "wikilink")）、統風三友會構成3團體（大西組、石鐵會、三代目伊藤會）等計5團體結盟共組「松山連合會」。
  - 1989年7月 加入親睦組織「[西日本二十日會](../Page/西日本二十日會.md "wikilink")」。
  - 1990年11月[高松組加入](../Page/高松組.md "wikilink")。岡本雅博就任會長、組織改編。
  - 1991年1月29日「西日本二十日會」解散。
  - 1991年2月5日 加入五代目山口組（組長・[渡边芳则](../Page/渡边芳则.md "wikilink")），改稱「松山會」。
  - 1996年10月 岡本因病退休。由[正田
    悟](../Page/正田悟.md "wikilink")（二代目岡本會會長）繼承第2代會長、昇格山口組直参。

2012年11月 正田悟宣告引退組織解散；殘餘勢力被[今治市的矢島組吸收](../Page/今治市.md "wikilink")。

## 歷代会长

  - 初代：[岡本雅博](../Page/岡本雅博.md "wikilink")（五代目山口组若中）
  - 第2代：[正田 悟](../Page/正田悟.md "wikilink")（六代目山口组若中）

## 最高幹部

  - 會長・[正田 悟](../Page/正田悟.md "wikilink")
  - 理事長・[藤田正幸](../Page/藤田正幸.md "wikilink")（三代目[伊藤會會長](../Page/伊藤會.md "wikilink")）
  - 理事長代行・[篠原清吾](../Page/篠原清吾.md "wikilink")（二代目[飴矢組組長](../Page/飴矢組.md "wikilink")）
  - 理事長代行・[中山和廣](../Page/中山和廣.md "wikilink")（二代目[河野組組長](../Page/河野組.md "wikilink")）
  - 本部長・[梶田順次](../Page/梶田順次.md "wikilink")（[誠道會會長](../Page/誠道會.md "wikilink")）
  - 特別相談役・[平原義秋](../Page/平原義秋.md "wikilink")（原副会长，[平原組組長](../Page/平原組.md "wikilink")
    ，原[至誠會組織委員長](../Page/至誠會.md "wikilink")）

## 其他組員

  - 〔姓名不明〕（[統風三友會會長](../Page/統風三友會.md "wikilink")）
  - [奧田
    進](../Page/奧田進.md "wikilink")（[奥田組組長](../Page/奥田組.md "wikilink")）
  - [荒川和典](../Page/荒川和典.md "wikilink")（[荒川組組長](../Page/荒川組.md "wikilink")）
  - [村井
    誠](../Page/村井誠.md "wikilink")（[誠興業組長](../Page/誠興業.md "wikilink")）
  - [三宅
    有](../Page/三宅有.md "wikilink")（[三宅組組長](../Page/三宅組.md "wikilink")）
  - [正田賀素巳](../Page/正田賀素巳.md "wikilink")（正田滋知；二代目[正田組組長](../Page/正田組.md "wikilink")。正田
    悟的親弟）

[Category:山口组](../Category/山口组.md "wikilink")
[Category:四國地區暴力團](../Category/四國地區暴力團.md "wikilink")
[Category:松山市](../Category/松山市.md "wikilink")