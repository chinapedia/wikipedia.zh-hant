**甘尼森县** （**Gunnison County,
Colorado**）是[美國](../Page/美國.md "wikilink")[科羅拉多州西部的一個縣](../Page/科羅拉多州.md "wikilink")。面積8,443平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,956人。縣治[甘尼森](../Page/甘尼森_\(科羅拉多州\).md "wikilink")。

成立於1877年3月9日。縣名紀念探險家[约翰·威廉姆斯·甘尼森](../Page/约翰·威廉姆斯·甘尼森.md "wikilink")
（John Williams Gunnison）。\[1\]\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/科罗拉多州行政区划.md "wikilink")

1.
2.  <http://www.colorado.gov/dpa/doit/archives/arcgov.html>