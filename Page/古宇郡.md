**古宇郡**（）為[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[後志綜合振興局的郡](../Page/後志綜合振興局.md "wikilink")，位於後志綜合振興局西北的[積丹半島南部](../Page/積丹半島.md "wikilink")。

## 轄區

轄區包含2村：

  - [泊村](../Page/泊村.md "wikilink")
  - [神惠內村](../Page/神惠內村.md "wikilink")

## 沿革

[Shiribeshi-shicho.png](https://zh.wikipedia.org/wiki/File:Shiribeshi-shicho.png "fig:Shiribeshi-shicho.png")

  - 1869年8月15日：北海道設置11國86郡，[後志國古宇郡成立](../Page/後志國.md "wikilink")。
  - 1897年11月：北海道實施支廳制，此時古宇郡下轄神惠內村、赤石村、珊內村、泊村、盃村、興志內村。\[1\]（6村）
  - 1906年4月1日：神惠內村（神惠內村、赤石村、珊內村合併而成）、泊村（泊村、盃村、興志內村和岩內郡茅沼村、堀株村合併而成）成為北海道二級村。（2村）
  - 1923年4月1日：泊村成為北海道一級村。
  - 1923年9月1日：泊村的堀株地區被[併入岩內郡發足村](../Page/市町村合併.md "wikilink")（現[共和町](../Page/共和町.md "wikilink")）。
  - 1943年6月1日：北海道一級二級町村制廢止，神惠內村改為內務省指定村。
  - 1946年10月5日：指定町村制廢止。

## 參考資料

[Category:後志國](../Category/後志國.md "wikilink")
[Category:北海道的郡](../Category/北海道的郡.md "wikilink")

1.