**毛致用**，[湖南](../Page/湖南.md "wikilink")[岳阳人](../Page/岳阳.md "wikilink")，[中共前](../Page/中共.md "wikilink")[黨和國家領導人](../Page/黨和國家領導人.md "wikilink")。\[1\]

1977年9月至1988年5月擔任[湖南](../Page/湖南.md "wikilink")[省委书记](../Page/省委书记.md "wikilink")，1977年11月-1979年4月擔任第四屆[湖南省政协主席](../Page/湖南省政协.md "wikilink")，1988年至1995年，接替[萬紹芬擔任](../Page/萬紹芬.md "wikilink")[江西](../Page/江西.md "wikilink")[省委书记](../Page/省委书记.md "wikilink")，1995年至1998年出任[江西省人大常委会主任](../Page/江西省人民代表大会.md "wikilink")。1998年至2003年，擔任第九屆[全国政协副主席](../Page/全国政协.md "wikilink")。

于2019年3月4日14时10分在湖南逝世，享年90岁。官方评价其为“中国共产党的优秀党员，忠诚的共产主义战士，我国农业和经济建设战线的杰出领导人”。

## 参考资料

## 外部链接

{{-}}

[ZHI](../Category/毛姓.md "wikilink")
[Category:岳阳人](../Category/岳阳人.md "wikilink")
[Category:中华人民共和国领导人](../Category/中华人民共和国领导人.md "wikilink")
[Category:中共江西省委书记](../Category/中共江西省委书记.md "wikilink")
[Category:江西省人大常委会主任](../Category/江西省人大常委会主任.md "wikilink")
[Category:中共湖南省委书记](../Category/中共湖南省委书记.md "wikilink")
[Category:湖南省政协主席](../Category/湖南省政协主席.md "wikilink")
[Category:第九届全国政协副主席](../Category/第九届全国政协副主席.md "wikilink")

1.