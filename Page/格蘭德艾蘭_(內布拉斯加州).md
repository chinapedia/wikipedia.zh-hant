**格蘭德艾蘭**（Grand Island,
Nebraska）是[美國](../Page/美國.md "wikilink")[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")[霍爾縣的縣治](../Page/霍爾縣_\(內布拉斯加州\).md "wikilink")，位於該州中部[普拉特河畔](../Page/普拉特河.md "wikilink")。面積55.9平方公里，2000年人口42,940人，是該州第四大城市。

建於1872年。1890年在那裡興建全美第一家處理[甜菜的工廠](../Page/甜菜.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/內布拉斯加州城市.md "wikilink")
[Category:19世紀建立的聚居地](../Category/19世紀建立的聚居地.md "wikilink")
[Category:内布拉斯加建州前历史](../Category/内布拉斯加建州前历史.md "wikilink")

1.  [Grand Island
    History](http://www.grand-island.com/Home/History/history_home.htm)