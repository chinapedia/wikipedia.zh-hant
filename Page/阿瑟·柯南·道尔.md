**亞瑟·伊格納修斯·柯南·道爾**爵士（Sir **Arthur Ignatius Conan Doyle**，1859年5月22日 -
1930年7月7日），[英国作家](../Page/英国.md "wikilink")、醫生。因塑造了成功的偵探人物──[福爾摩斯](../Page/夏洛克·福爾摩斯.md "wikilink")，而成爲[偵探小说歷史上最重要的作家之一](../Page/偵探小说.md "wikilink")。除此之外他還曾寫過多部其他類型的小说，如科幻、歷史小说、爱情小说、戲劇、詩歌等。

1876年至1881年间他在[爱丁堡大学学习医学](../Page/爱丁堡大学.md "wikilink")，毕业后作为一名随船医生前往[西非海岸](../Page/西非.md "wikilink")，1882年回国后在[普利茅斯开业行医](../Page/普利茅斯.md "wikilink")。不过他行医并不太顺利，在此期间道尔开始写作。在搬到[朴茨茅斯的](../Page/朴茨茅斯.md "wikilink")[南海城](../Page/南海城.md "wikilink")（Southsea）后，他才开始花更多的时间在写作上。道尔的第一部重要作品是发表於1887年
业余门将并时常客串后卫。

1885年道尔与路易斯·霍金斯（Louisa
Hawkins）结婚，但是霍金斯在1906年因[结核病过世](../Page/结核病.md "wikilink")。1907年道尔与珍·勒奇（Jean
Leckie）小姐结婚。

1890年道尔到[维也纳学习眼科](../Page/维也纳.md "wikilink")，一年之后回到伦敦成为一名眼科医生，这使得他有更多时间写作。1891年11月在一封给母亲的信中道尔写道，“我考虑杀掉福尔摩斯……把他干掉，一了百了。他占据了我太多的时间。”1893年12月在《[最后一案](../Page/最后一案.md "wikilink")》中，道尔让福尔摩斯和他的死敌莫里亚蒂教授一起葬身[莱辛巴赫瀑布](../Page/莱辛巴赫瀑布.md "wikilink")。但是小说的结局令读者们非常不满，这使得道尔最终又让福尔摩斯重新“复活”，在1903年道尔发表了《[空屋](../Page/空屋.md "wikilink")》，使福尔摩斯死里逃生。道尔一生一共写了56篇短篇侦探小说以及4部中篇侦探小说，全部以福尔摩斯为主角。

19世纪末英国在[南非的](../Page/南非.md "wikilink")[布尔战争遭到了全世界的谴责](../Page/布尔战争.md "wikilink")，道尔为此写了一本名为《在南非的战争：起源与行为》（*The
War in South Africa: Its Cause and
Conduct*）的小册子，为英国辩护。这本书被翻译成多种文字发行，有很大影响。道尔相信正是由于这本书使他在1902年被封[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。20世纪初道尔两次参选国会议员，却都没有当选。

道尔本人也曾卷入两桩有趣的案件。一次是在1906年，一名英印混血律师被指控发送恐吓信以及虐待动物。虽然这名律师被逮捕后，依然有动物被虐待，警方却一口咬定这名律师有罪。值得注意的是在这次案件后，1907年英国建立了刑事上诉法庭。因此可以说道尔不但帮助了这名律师，还间接协助建立了一套冤案申诉机制。另一次则是在1908年，一名经营赌场的[德国籍](../Page/德国.md "wikilink")[犹太人被控用棒子袭击一名](../Page/犹太人.md "wikilink")82岁的老妇人。

[Doyle_Arthur_Conan_grave.jpg](https://zh.wikipedia.org/wiki/File:Doyle_Arthur_Conan_grave.jpg "fig:Doyle_Arthur_Conan_grave.jpg")
到晚年时道尔公开相信[唯灵论](../Page/唯灵论.md "wikilink")，甚至还曾以此为主题写过好几部小说，有人认为这和他的儿子在[一战中丧生有关](../Page/一战.md "wikilink")。柯南·道尔在1930年7月7日去世。

## 唯灵论、共济会

[PlanCottingleyFairies.jpg](https://zh.wikipedia.org/wiki/File:PlanCottingleyFairies.jpg "fig:PlanCottingleyFairies.jpg")
柯南·道尔早年生涯已经开始对唯灵论产生兴趣。他於1887年1月26日加入位於汉普郡南海城257號（Phoenix Lodge No.
257）的[共济会](../Page/共济会.md "wikilink")，成为共济会会员。他於1889年申請退會，但於1902年再次回到凤凰廬，並於1911年再次退會。而他的作品包括《[血字的研究](../Page/血字的研究.md "wikilink")》及《[墨氏家族的成人礼](../Page/墨氏家族的成人礼.md "wikilink")》也有暗示共济会。\[1\]

同樣在1887年的南海城，他受到了樸茨茅斯文學與哲學學會（Portsmouth Literary and Philosophical
Society）會員的影響，他開始一系列的超自然調查。其中包括參加約20次的[降神會](../Page/降神會.md "wikilink")、[心靈感應實驗和與靈媒混在一起](../Page/心靈感應.md "wikilink")。他寫給唯靈論雜誌《*Light*》，宣稱自己是一名唯靈論者，並且談到一件令他信服的特殊超自然事件。\[2\]

雖然後來他動搖過，但他仍然對[超自然現象著迷](../Page/超常現象.md "wikilink")。1889年，他成為漢普郡心靈研究學會（Hampshire
Society for Psychical Research）的創始人之一，並於1893年加入了倫敦的。他在1894年加入了Sidney
Scott爵士和，在德文郡進行了一次鬧鬼調查。儘管如此，在此期間，他本質上仍是一名業餘。\[3\]

其後柯南·道爾的妻子路易斯·霍金斯、兒子Kingsley
Doyle、弟弟Innes、兩個姐夫及侄子相繼去世，令道爾寄情於唯靈論之中從而獲得慰藉，他亦試圖想證明可以與死者的靈魂溝通，例如[通靈](../Page/通靈.md "wikilink")。他也曾經加入了一個研究超自然的協會「[鬼魂俱樂部](../Page/鬼魂俱樂部.md "wikilink")」。\[4\]\[5\]

1917年，有兩位小女孩Elsie Wright及Frances
Griffiths在英國[巴拉福特聲稱目睹](../Page/巴拉福特.md "wikilink")[花仙子](../Page/花仙子_\(攝影作品\).md "wikilink")（Cottingley
Fairies），並且拍攝了五張照片。作為唯靈論支持者的柯南·道爾看過照片後認為真實無誤，並在1920年的（*The Strand
Magazine*）上發表文章及在1922年出版著作《*The Coming of the
Fairies*》以詳細說明花仙子的真實性。1980年代初，Elsie及Frances承認照片是造假，但她們堅持自己目睹花仙子真實存在。\[6\]\[7\]

## 著作

[Conandoylestatue.jpg](https://zh.wikipedia.org/wiki/File:Conandoylestatue.jpg "fig:Conandoylestatue.jpg")

### 关于福尔摩斯的著作

柯南·道尔一共写了60个关于福尔摩斯的故事，56个短篇和4个中篇小说。这些故事在40年间陆陆续续在《岸濱月刊》上发表，这是当时的习惯做法（[查尔斯·狄更斯也是用类似的形式发表小说](../Page/查尔斯·狄更斯.md "wikilink")）。故事的主要发生在1878年到1907年间，最晚的一个故事是以1914年为背景。这些故事中两个是以福尔摩斯第一人称口吻写成，还有两个以第三人称写成，其余都是华生的叙述。参见：[福尔摩斯案件](../Page/福尔摩斯案件.md "wikilink")。

### 其他的著作

  - [伟大的布尔战争](../Page/伟大的布尔战争.md "wikilink")（1889年）
  - [失落的世界](../Page/失落的世界.md "wikilink")（1912年）
  - [新启示](../Page/新启示.md "wikilink")（1918年）
  - [重要信息](../Page/重要信息.md "wikilink")（1919年）
  - [唯灵论史](../Page/唯灵论史.md "wikilink")

## 参考资料

## 外部链接

  - [柯南·道尔故居（英文）](http://www.sherlockholmesonline.org/) -
    包括了道尔的生平、故居历史、作品列表等

  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 紀錄片

<!-- end list -->

  - [是誰殺了福爾摩斯 The Man Who Murdered Sherlock
    Holmes](https://www.youtube.com/watch?v=Dx1ACyhX06o)(中文字幕)

<!-- end list -->

  - 電台節目

<!-- end list -->

  - 神秘之夜：[福爾摩斯](https://www.youtube.com/watch?v=FrO2JsumyTI)2009-12-26
    (廣東話)

[Category:1859年出生](../Category/1859年出生.md "wikilink")
[Category:1930年逝世](../Category/1930年逝世.md "wikilink")
[亞瑟·柯南·道爾](../Category/亞瑟·柯南·道爾.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:共济会会员](../Category/共济会会员.md "wikilink")
[Category:英国推理小说作家](../Category/英国推理小说作家.md "wikilink")
[Category:蘇格蘭劇作家](../Category/蘇格蘭劇作家.md "wikilink")
[D](../Category/愛丁堡大學校友.md "wikilink")

1.
2.
3.
4.
5.
6.
7.