**Adobe
Bridge**是[Adobe公司开发的一个组织工具程序](../Page/Adobe.md "wikilink")，并作为[Adobe
Creative Suite](../Page/Adobe_Creative_Suite.md "wikilink")
2（CS2）的一部分于2005年5月发布，其后续版本也作为CS3和CS4的一部分，并且也随多个产品独立发布，如Photoshop
CS4和InDesign CS4。其主要目的是使用类似文件浏览器的格式，管理多媒体文件（如图片，照片和影片），并将Creative
Suite各个程序连-{}-接起来。它可以直接从Creative Suite软件套装其他软件中直接接入。Adobe
Bridge也可以连接線上图片浏览购买网站[Adobe Stock
Photos](../Page/Adobe_Stock_Photos.md "wikilink")。

其前身为[Photoshop中的浏览器](../Page/Photoshop.md "wikilink")，但是被作为一个软體独立出来，Bridge有很多新功能，如“批量重命名”，还有其他组织管理文件的功能。它支持[RAW文件格式的编辑](../Page/RAW.md "wikilink")。管理功能还包括图片的“彩色标签”“分级”，编辑图片内容的[XMP和](../Page/XMP.md "wikilink")[IPTC](../Page/IPTC.md "wikilink")
[元数据](../Page/元数据.md "wikilink")，还可以处理[Adobe Version
Cue](../Page/Adobe_Version_Cue.md "wikilink")（后改名为Adobe
Drive）项目中一个文件的不同版本。

图像文件可以不同尺寸的缩略图、幻灯片和列表形式。每个資料夹可以做标记，并有一个缓存文件以便加速创建读取缩略图。缓存可以统一集中在一个資料夹，或者分别存在各个文件夹中。

Adobe
Bridge可以借助于编写ExtendScript（Adobe开发的内嵌于CS产品中的[JavaScript语言扩展版本](../Page/JavaScript.md "wikilink")）脚本或使用内含的工作流程脚本示例将Adobe
Creative Suite或Adobe Production Studio中各组件的繁重任务实现自动化。

## 参见

  - [Adobe Photoshop
    Lightroom](../Page/Adobe_Photoshop_Lightroom.md "wikilink")
  - [Adobe Photoshop](../Page/Adobe_Photoshop.md "wikilink")
  - [Aperture](../Page/Aperture.md "wikilink")

[Bridge](../Category/Adobe软件.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:Windows软體](../Category/Windows软體.md "wikilink")
[Category:元数据](../Category/元数据.md "wikilink")