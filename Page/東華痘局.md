[Tung_Wah_Smallpox_Hospital_2012_01.jpg](https://zh.wikipedia.org/wiki/File:Tung_Wah_Smallpox_Hospital_2012_01.jpg "fig:Tung_Wah_Smallpox_Hospital_2012_01.jpg")
[Tung_Wah_Smallpox_Hospital_signs_2018.jpg](https://zh.wikipedia.org/wiki/File:Tung_Wah_Smallpox_Hospital_signs_2018.jpg "fig:Tung_Wah_Smallpox_Hospital_signs_2018.jpg")

**東華痘局**（****）是[香港昔日一所](../Page/香港.md "wikilink")[醫院](../Page/醫院.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[堅尼地城](../Page/堅尼地城.md "wikilink")[加惠民道](../Page/加惠民道.md "wikilink")，為當時防治[天花的主要設施之一](../Page/天花.md "wikilink")，現已拆卸，僅留下拱形牌坊及基石。

## 歷史

東華痘局的建築於1901年11月18日奠基，由當時的[香港總督](../Page/香港總督.md "wikilink")[卜力主持安放儀式](../Page/卜力.md "wikilink")。奠基石高70厘米，長78厘米，闊58厘米。當時原本作為政府管理的防疫診所。1907年，政府將其交給[東華醫院作為防治](../Page/東華醫院.md "wikilink")[天花的痘局](../Page/天花.md "wikilink")。東華痘局於1910年正式啟用，並在[卑路乍街與](../Page/卑路乍街.md "wikilink")[域多利道的交界立一拱門](../Page/域多利道.md "wikilink")，其紀念石匾上刻有：「TUNG
WAH SMALLPOX HOSPITAL, A.D.
1910」的[英文字樣](../Page/英文.md "wikilink")。東華痘局當時主要以[中醫藥治理天花病人以及接種](../Page/中醫藥.md "wikilink")[牛痘](../Page/牛痘.md "wikilink")。

隨著天花絕跡，東華痘局於1938年交還政府用作傳染病醫院，至[香港重光後被拆卸](../Page/香港重光.md "wikilink")。拱門及奠基石被移放於附近[西寧街的](../Page/西寧街.md "wikilink")[堅尼地城巴士總站現址](../Page/堅尼地城巴士總站.md "wikilink")，並放置刻有以下文字的基石：

基石上另有一石碑，說明東華痘局的歷史：

拱門和奠基石最近在2008年被翻新，石面潔淨了，文字亦更清晰可辨。

## 現址鄰近

  - [堅尼地城巴士總站](../Page/堅尼地城巴士總站.md "wikilink")
  - [域多利亞公眾殮房](../Page/域多利亞公眾殮房.md "wikilink")
  - 招商局貨倉碼頭

[Category:香港已不存在的建築物](../Category/香港已不存在的建築物.md "wikilink")
[Category:東華三院](../Category/東華三院.md "wikilink")
[Category:堅尼地城](../Category/堅尼地城.md "wikilink")