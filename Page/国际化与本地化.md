在[信息技术领域](../Page/信息技术.md "wikilink")，**国际化与本-{}-地化**（[英文](../Page/英文.md "wikilink")：internationalization
and
localization）是指修改[软件使之能适应目标市场的](../Page/软件.md "wikilink")[语言](../Page/语言.md "wikilink")、[地区差异以及技术需要](../Page/地区.md "wikilink")。

[國際化是指在設計軟體](../Page/國際化.md "wikilink")，將軟體與特定語言及地區[脱鉤的過程](../Page/脱鉤.md "wikilink")。當軟體被[移植到不同的語言及地區時](../Page/遊戲移植.md "wikilink")，軟體本身不用做內部工程上的改變或修正。本地化則是指當移植軟體時，加上與特定[区域设置有關的資訊和翻譯文件的過程](../Page/区域设置.md "wikilink")。

國際化和本地化之間的區别雖然微妙，但卻很重要。國際化意味着產品有適用於任何地方的「潜力」；本地化則是為了更適合於「特定」地方的使用，而另外增添的特色。用一項產品來說，國際化只需做一次，但本地化則要針對不同的區域各做一次。這兩者之間是互補的，並且兩者合起來才能讓一個系統適用於各地。

基於他們的英文單字長度過長，常被分別簡稱成**i18n**（18意味著在“internationalization”這個單字中，i和n之間有18個字母）及**L10n**。使用大寫的L以利區分i18n中的i和易於分辨小寫l與1。

在如[微軟及](../Page/微軟.md "wikilink")[IBM等企業中](../Page/IBM.md "wikilink")，則會使用[全球化](../Page/全球化.md "wikilink")（[英文](../Page/英文.md "wikilink")：globalization）來表示此兩者的合稱。\[1\]\[2\]在英文中，也會使用g11n做為簡稱。也有使用缩写**GILT**(globalization、internationalization、localization和translation)，即“全球化、国际化、本地化和翻译”\[3\]。

## 范围

国际化与本地化工作的焦点包括：

  - 語言
      - 電子文件
          - 字母。目前大部分的系統都採用[Unicode為標準來解決](../Page/Unicode.md "wikilink")[字符编码](../Page/字符编码.md "wikilink")。
          - 不同的數字命名系統。
          - 書寫方向。譬如[德語是從左到右](../Page/德語.md "wikilink")，而[波斯語](../Page/波斯語.md "wikilink")、[希伯來語和](../Page/希伯來語.md "wikilink")[阿拉伯語是由右到左](../Page/阿拉伯語.md "wikilink")。
          - 相同語言在不同地區的拼法差異，如[美國英語](../Page/美國英語.md "wikilink")、[加拿大英語使用](../Page/加拿大英語.md "wikilink")*localization*，而[英國英語和](../Page/英國英語.md "wikilink")[澳洲英語使用](../Page/澳洲英語.md "wikilink")*localisation*。
          - 文件處理上的差異，如某些文字存在大小寫，其它則否。字母順序。
      - 文字的圖像表示（列印物、內含線上圖片）。
      - 读法（-{zh-hans:音频; zh-hant:音訊;}-）
      - \-{zh-hans:视频; zh-hant:視訊;}-的字幕
  - [文化](../Page/文化.md "wikilink")
      - 圖片和顏色：這牽涉到理解和文化適宜的議題。
      - 名字和稱謂
      - 政府給定的編碼（如美國的[社會安全碼](../Page/社會安全號碼.md "wikilink")，英國的[National
        Insurance
        number](../Page/National_Insurance_number.md "wikilink")，愛沙尼亞的[Isikukood及其它各國的](../Page/Isikukood.md "wikilink")[身份證號碼](../Page/身份證.md "wikilink")）和護照
      - 電話號碼、地址和國際[郵遞區號](../Page/郵遞區號.md "wikilink")
      - [貨幣](../Page/貨幣.md "wikilink")（符號、貨幣標誌的位置）
      - [度量衡](../Page/度量衡.md "wikilink")
      - 紙張大小
  - 書寫習慣
      - 日期跟時間的格式，包含各式[日曆](../Page/日曆.md "wikilink")。
      - 時區（在國際場合會使用[世界標準時間](../Page/世界標準時間.md "wikilink")）
      - 數字格式（小數點、分隔點的位置、分隔所用的字符）
  - 產品和服務所要面向的法規

只属于本地化的主题有：

  - [翻译](../Page/翻译.md "wikilink")
  - 針對特定语言（如[东亚语言](../Page/東亞語言.md "wikilink")）的特别-{zh-hans:支持;
    zh-hant:支援;}-
  - 符合当地習慣
  - 符合當地的[道德觀念](../Page/道德.md "wikilink")
  - 針對当地撰寫内容
  - 符号
  - 排序方法
  - [美学](../Page/美学.md "wikilink")
  - 當地的文化价值和社会环境

## 困难

开发软件时，国际化和本地化对开发者是一个有挑战性的任务，特别是当软件当初设计时没有考虑这个问题时。通常作法是将文本和其他环境相关的资源与程序代码相分离。这样在理想的情况下，应对变化的环境时无需修改代码，只要修改资源，从而显著简化了工作。

开发团队需要人了解其他语言和文化；而这样的人才，可能难以寻觅。而且资源的复制也可能成为维护恶梦。例如，如果某个语言中显示给用户的信息变化了，其他的翻译版本都要随之变化。[Gettext之类](../Page/Gettext.md "wikilink")[软件库有助于解决这一问题](../Page/库.md "wikilink")。

由于[自由软件自由地修改和再分发](../Page/自由软件.md "wikilink")，因此它比较容易国际化。当[KDE拥有](../Page/KDE.md "wikilink")70个语言版本时，大多数专有软件只提供商业上有利可图的语言版本。

当然很多[自由软件由于程序结构设计的原因几乎难以国际化](../Page/自由软件.md "wikilink")，比如[HandBrake](../Page/HandBrake.md "wikilink")。

## 区域设置（locale）

计算机中一套定义用户的语言、国家和用于定义用户希望在其用户界面上看到的各种可以改变的选择的参数集合。通常一个 locale
标识符至少包括一个语言标识符和一个区域标识符。

在 UNIX 和 Windows 中，locale 的控制是不同的。在 UNIX 下，通常-{zh-hans:通过;
zh-hant:透過;}-环境变量来控制 locale。这些环境变量包括：`LANG`, `LC_ALL`, `LC_CTYPE`,
`LC_TIME`, 等等。你可以-{zh-hans:通过; zh-hant:透過;}-改变这些环境变量来控制你的程序或者命令所表现出来的
locale，前提是这些程序或者命令必须是已经被国际化的和本地化的。在 Windows 下，你可以-{zh-hans:通过;
zh-hant:透過;}-改变控制面板上的“语言/区域”中的区域的值来设定 Windows 的当前用户的 locale。

## 参见

  - [中日韓統一表意文字](../Page/中日韓統一表意文字.md "wikilink")
  - [全球化](../Page/全球化.md "wikilink")
  - [使用者介面與](../Page/使用者介面.md "wikilink")[翻譯](../Page/翻譯.md "wikilink")
  - [官方译名](../Page/官方译名.md "wikilink")
  - [雙向文稿支援](../Page/雙向文稿.md "wikilink")
  - 多语言文字排版引擎:
      - [Uniscribe](../Page/Uniscribe.md "wikilink") (Windows)
      - [Apple Type Services for Unicode
        Imaging](../Page/Apple_Type_Services_for_Unicode_Imaging.md "wikilink")
        (New Macintosh)
      - [WorldScript](../Page/WorldScript.md "wikilink") (Old Macintosh)
      - [Pango](../Page/Pango.md "wikilink") (Open source)
      - [Graphite](../Page/Graphite_\(SIL\).md "wikilink") (Windows &
        Linux, open source)

## 参考文献

## 外部链接

  - [i18n软件介绍](http://www.debian.org/doc/manuals/intro-i18n/)
  - [Open directory of links to internationalization resources and
    related material](http://www.i18ngurus.com/)
  - [Information for developers about localisation of Free and Open
    Source
    Software](https://web.archive.org/web/20050806081308/http://localisationdev.org/)
  - W3C规范
      - [文本排版](http://www.w3.org/TR/css3-text/#text-layout)
      - [Directional focus
        navigation](http://www.w3.org/TR/css3-ui/#nav-dir)
      - [书写模式](https://web.archive.org/web/20050810021540/http://www.w3.org/TR/xsl/slice7.html#writing-mode)
  - IETF规范
      - [应用程序中的域名国际化 (IDNA)](http://www.ietf.org/rfc/rfc3490.txt)
      - [IETF的国际化的术语](http://www.ietf.org/rfc/rfc3536.txt)

<!-- end list -->

  - [Free/Open Source Software: Localization
    Primer](https://web.archive.org/web/20050805051949/http://www.iosn.net/l10n/foss-localization-primer/foss-localization-primer.pdf)
    (PDF, 1.25 Megabytes)
  - [Translate Tookit guide to localizing Free/Open Source
    Software](http://translate.sourceforge.net/wiki/guide/start)

[國際化與在地化](../Category/國際化與在地化.md "wikilink")
[Category:转写系统](../Category/转写系统.md "wikilink")
[Category:语言学](../Category/语言学.md "wikilink")
[Category:全球化術語](../Category/全球化術語.md "wikilink")

1.  [IBM Globalization web
    site](http://www.ibm.com/software/globalization/)
2.
3.