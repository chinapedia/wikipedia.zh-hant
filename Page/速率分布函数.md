**速率分布函数**是一个描述分子运动速率分布状态的函数。

一个符合[波茲曼分布的粒子体系](../Page/麦克斯韦-波茲曼分布.md "wikilink")，如[理想气体](../Page/理想气体.md "wikilink")，其体系中粒子运动速率的分布可以用如下的速率分布函数来描述：

\(\begin{matrix}f(v)dv&=&4\pi(2\pi mkT)^{-\frac{3}{2}} e^\left(-\frac{mv^2}{2kT}\right) m^3 v^2 dv \\ \ &
=&4\pi (\frac{m}{2\pi kT})^ \frac{3}{2} e^\left(-\frac{mv^2}{2kT}\right) v^2 dv\end{matrix}\)

[MaxwellBoltzmann-en.svg](https://zh.wikipedia.org/wiki/File:MaxwellBoltzmann-en.svg "fig:MaxwellBoltzmann-en.svg")

通常速率分布函数也采用依[动量和依](../Page/动量.md "wikilink")[动能分布的形式](../Page/动能.md "wikilink")，虽然形式上有所不同但因为动量动能和速率的相关关系，这些表达方式本质上和依速率表示的速率分布函数还是一样的。

\(f(p)dp=4\pi(2\pi mkT)^{-\frac{3}{2}} e^\left(-\frac{p^2}{2kT}\right) p^2 dp\)

\(f(\epsilon_t)d\epsilon_t=2\pi (\frac{1}{2\pi kT})^{\frac{3}{2}} e^\left(-\frac{\epsilon_t}{2kT}\right) \epsilon_t^2 d\epsilon_t\)

在处理某些特殊体系的情况下可能会用到二维和一维的速率分布函数，如[固体](../Page/固体.md "wikilink")[表面](../Page/表面.md "wikilink")[吸附的](../Page/吸附.md "wikilink")[理想气体就可以看做是在二维平面上运动的一个二维独立粒子体系](../Page/理想气体.md "wikilink")，当处理这个体系有关分子运动速率的问题的时候就要用到二维速率分布函数

二维速率分布函数：

\(f(v)dv=2\pi(2\pi mkT)^{-\frac{2}{2}} e^\left(-\frac{mv^2}{2kT}\right) v dv\)

一维速率分布函数：

\(f(v_x)dv_x=(2\pi mkT)^{-\frac{1}{2}} e^\left(-\frac{mv_x^2}{2kT}\right) dv_x\)

式中，\(f(v)dv\)表示处于该速率的分子的比例。

## 参阅

  - [麦克斯韦速度分布律](../Page/麦克斯韦速度分布律.md "wikilink")

[S](../Category/统计力学.md "wikilink") [S](../Category/函数.md "wikilink")