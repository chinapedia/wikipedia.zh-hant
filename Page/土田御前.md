**土田御前**（生年不詳－1594年2月26日）是[織田信秀的繼室](../Page/織田信秀.md "wikilink")（最初的正室是[織田達勝的女兒](../Page/織田達勝.md "wikilink")，後來離婚）。實名不詳。別稱**花屋夫人**。法名是**報春院花屋壽永大禪尼**。[織田信長](../Page/織田信長.md "wikilink")、[織田信行](../Page/織田信行.md "wikilink")、[織田秀孝](../Page/織田秀孝.md "wikilink")、[織田信包](../Page/織田信包.md "wikilink")、[織田市](../Page/織田市.md "wikilink")（[淺井長政](../Page/淺井長政.md "wikilink")→[柴田勝家正室](../Page/柴田勝家.md "wikilink")）、[織田犬](../Page/織田犬.md "wikilink")（[佐治信方正室](../Page/佐治信方.md "wikilink")）的生母。父親為[土田政久](../Page/土田政久.md "wikilink")（有異說）。

## 生平

丈夫信秀死後，與次男信行一起居住在[末森城](../Page/末森城.md "wikilink")。討厭被稱為傻瓜（）的信長，喜歡品行方正的信行。在信行與信長爭奪家督之位失敗後，向信長提出赦免信行，於是信行一時間被赦免，但是後來信行仍被信長誅殺。在信行死後與信長和阿市一起生活，照顧年幼的孫子（[信忠](../Page/織田信忠.md "wikilink")、[信雄](../Page/織田信雄.md "wikilink")、[信孝](../Page/織田信孝.md "wikilink")、[茶茶](../Page/淀殿.md "wikilink")、[初](../Page/常高院.md "wikilink")、[江等](../Page/崇源院.md "wikilink")）。後來孫女江嫁到[德川家後形成了與她同樣的家庭環境](../Page/德川家.md "wikilink")（討厭次男[家光](../Page/德川家光.md "wikilink")，寵愛三男[忠長](../Page/德川忠長.md "wikilink")）。

在[本能寺之變中](../Page/本能寺之變.md "wikilink")，信長與信忠自殺後，被孫兒信雄庇護，被尊稱為「**大方殿樣**」，並獲得640貫文化妝費。[文祿元年](../Page/文祿.md "wikilink")（1590年）信雄被改易後，被[伊勢](../Page/伊勢.md "wikilink")[安濃津的信包接走](../Page/安濃津.md "wikilink")，文祿3年（1594年）正月7日在該地死去（亦有指信包不是土田御前的兒子）。

法名是**報春院花屋壽永大禪尼**。墓所在[三重縣](../Page/三重縣.md "wikilink")[津市的](../Page/津市.md "wikilink")[塔世山](../Page/塔世山.md "wikilink")[四天王寺](../Page/四天王寺.md "wikilink")。[織田信秀的](../Page/織田信秀.md "wikilink")[菩提所](../Page/菩提所.md "wikilink")[名古屋市中區](../Page/名古屋市.md "wikilink")[龜岳林](../Page/龜岳林.md "wikilink")[萬松寺有位牌供人祭祀](../Page/萬松寺.md "wikilink")（**花屋壽永大姉**）。

## 出身

一般被認為是[佐佐木](../Page/佐佐木.md "wikilink")[六角氏的後裔](../Page/六角氏.md "wikilink")[土田政久的女兒](../Page/土田政久.md "wikilink")，但是沒有當時的史料能證明，後來才在[土田氏的親戚](../Page/土田氏.md "wikilink")[生駒氏所生的](../Page/生駒氏.md "wikilink")[織田信雄系史料中以土田政久女兒的身份登場](../Page/織田信雄.md "wikilink")。因此當時就有數種不同的史料和說法。

  - 土田御前的日文讀法，如果在是[美濃](../Page/美濃國.md "wikilink")[可兒郡的土田氏應該被讀作](../Page/可兒郡.md "wikilink")「」，而[尾張](../Page/尾張.md "wikilink")[清洲的土田氏則讀作](../Page/清洲.md "wikilink")「」。因此有這支土田氏是[近江六角氏庶流和旗本的說法](../Page/近江.md "wikilink")。

<!-- end list -->

  - 在『[津島大橋記](../Page/津島大橋記.md "wikilink")』『[干城錄](../Page/干城錄.md "wikilink")』中記載，信秀的正室・信長的生母是[小嶋信房的女兒](../Page/小嶋信房.md "wikilink")，這個人物是土田御前以前的信秀的繼室（在這個情況，土田御前是在[織田達勝女兒和小嶋信房女兒之後的繼室](../Page/織田達勝.md "wikilink")），於是土田御前的出身是被竄改的存在，被稱為土田御前的人物是否本來小嶋信房的女兒，實情不明。『津島大橋記』中記述[大橋氏的](../Page/大橋氏.md "wikilink")[大橋重長與信秀女兒](../Page/大橋重長.md "wikilink")[藏之方](../Page/藏之方.md "wikilink")（）結婚之際，藏之方是以小嶋信房的養女身份結婚，由此可以知道小嶋信房和信秀的關係相當深。而且孫兒信孝繼承[神戶氏家督之際](../Page/神戶氏.md "wikilink")，代替[山路氏成為](../Page/山路氏.md "wikilink")[高岡城城主的信孝的異父兄被稱為小嶋兵部少輔](../Page/高岡城.md "wikilink")，因此有是小嶋信房的親戚説法。

<!-- end list -->

  - 『[美濃國諸舊記](../Page/美濃國諸舊記.md "wikilink")』中，信長的生母是[六角高賴的女兒](../Page/六角高賴.md "wikilink")。

## 外部連結

  - [塔世山四天王寺公式サイト・墓碑](http://www.sitennoji.net/jihou/index.html)
  - [亀岳林萬松寺公式サイト](http://www.banshoji.or.jp/index.html)－織田信秀的菩提所

[Category:勝幡織田氏](../Category/勝幡織田氏.md "wikilink")
[Category:生年不详](../Category/生年不详.md "wikilink")
[Category:1594年逝世](../Category/1594年逝世.md "wikilink")
[Category:戰國時代女性 (日本)](../Category/戰國時代女性_\(日本\).md "wikilink")
[Category:安土桃山時代女性](../Category/安土桃山時代女性.md "wikilink")