**卡雷卡**（，），1986年巴西联赛最佳射手。

[1982年世界杯足球赛他因伤缺席](../Page/1982年世界杯足球赛.md "wikilink")。

[1986年世界杯足球赛](../Page/1986年世界杯足球赛.md "wikilink")1/4决赛，他虽攻进一球，但[巴西国家足球队被](../Page/巴西国家足球队.md "wikilink")[普拉蒂尼率领的](../Page/米切尔·普拉蒂尼.md "wikilink")[法国国家足球队](../Page/法国国家足球队.md "wikilink")[点球所击败](../Page/点球.md "wikilink")。

1987年9月27日卡雷卡攻进了他在[那不勒斯足球俱乐部的第一个进球](../Page/那不勒斯足球俱乐部.md "wikilink")。

1990年[马拉多纳率领下](../Page/马拉多纳.md "wikilink")，那不勒斯击败了拥有[荷兰三剑客的](../Page/荷兰三剑客.md "wikilink")[AC米兰](../Page/AC米兰.md "wikilink")，卡雷卡随队成为[意大利甲级联赛的冠军](../Page/意大利甲级联赛.md "wikilink")。

1990年的世界杯足球賽中，他于1/8决赛遇到了队友马拉多纳领军的[阿根廷国家足球队](../Page/阿根廷国家足球队.md "wikilink")，最后巴西因马拉多纳的妙传还是给淘汰了。在卡雷卡所参加的两届世界杯9场比赛中共打入7个入球。

1990年-91年赛季马拉多纳离队，那不勒斯此后再也没赢得任何奖杯，卡雷卡与[佐拉搭档锋线](../Page/佐拉.md "wikilink")，后来他在那不勒斯效力了3年。

## 荣誉

  - [巴西甲組联赛冠军](../Page/巴西甲組联赛.md "wikilink")(1978年、1986年)
  - 巴西联赛最佳射手(1982年、1986年)
  - [意甲冠军](../Page/意甲.md "wikilink")(1990年)
  - [意大利超級杯冠军](../Page/意大利超級杯.md "wikilink")(1991年)
  - [欧洲联盟杯冠军](../Page/欧洲联盟杯.md "wikilink")(1989年)
  - [美洲杯冠军](../Page/美洲杯.md "wikilink")(1989年)

## 注释

[Category:巴西足球运动员](../Category/巴西足球运动员.md "wikilink")
[Category:柏雷素尔球员](../Category/柏雷素尔球员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:古蘭尼球員](../Category/古蘭尼球員.md "wikilink")
[Category:聖保羅球員](../Category/聖保羅球員.md "wikilink")
[Category:拿坡里球員](../Category/拿坡里球員.md "wikilink")
[Category:山度士球員](../Category/山度士球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:1986年世界盃足球賽球員](../Category/1986年世界盃足球賽球員.md "wikilink")
[Category:1990年世界盃足球賽球員](../Category/1990年世界盃足球賽球員.md "wikilink")
[Category:1987年美洲盃球員](../Category/1987年美洲盃球員.md "wikilink")
[Category:1983年美洲盃球員](../Category/1983年美洲盃球員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:日本外籍足球運動員](../Category/日本外籍足球運動員.md "wikilink")
[Category:巴西旅外足球運動員](../Category/巴西旅外足球運動員.md "wikilink")