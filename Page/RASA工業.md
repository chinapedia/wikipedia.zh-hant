**RASA工業**（****、**RASA Industries,
LTD**）為[日本的一個](../Page/日本.md "wikilink")[化學製造廠商](../Page/化學.md "wikilink")。

參予產業包括化學[肥料](../Page/肥料.md "wikilink")、非鐵金屬煉製、[石炭開採](../Page/石炭.md "wikilink")、[硫酸製造](../Page/硫酸.md "wikilink")、採礦機械製造工業的事業。除了擁有[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")[大東群島的](../Page/大東群島.md "wikilink")[沖大東島以外](../Page/沖大東島.md "wikilink")，經營的礦區還包括[岩手縣的](../Page/岩手縣.md "wikilink")[田老礦山](../Page/田老礦山.md "wikilink")、[宮崎縣](../Page/宮崎縣.md "wikilink")[見立礦山](../Page/見立礦山.md "wikilink")、[山形縣](../Page/山形縣.md "wikilink")[田川煤礦](../Page/田川煤礦.md "wikilink")。

最初的肥料製造事業已於1983年分割給關係企業Co-op化學股份有限公司\[1\]，並退出採礦、煉製和硫酸製造等事業，轉型為製造機械和電子材料，目前為全世界最大的[晶圓](../Page/晶圓.md "wikilink")（wafer）再生製造產。

現於[台灣](../Page/台灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[梧棲區投資有子公司](../Page/梧棲區.md "wikilink")[理盛精密科技股份有限公司](../Page/理盛精密科技股份有限公司.md "wikilink")，生產[半導体和](../Page/半導体.md "wikilink")[液晶用高純度](../Page/液晶.md "wikilink")[磷酸](../Page/磷酸.md "wikilink")。\[2\]

## 社名由來

社名「RASA」\[3\]（）即來自於[琉球列島大東群島的](../Page/琉球列島.md "wikilink")[沖大東島](../Page/沖大東島.md "wikilink")，沖大東島的別名為「RASA島」（島，讀音「拉薩」）；1911年為了在沖大東島上開採磷礦生產肥料，而成立了以該島別名所命名的「RASA島磷礦合資公司」，即為Rasa工業之前身，於1937年沖大東島全島區域成為RASA工業私有土地。目前雖然已暫停在該島開採磷礦時，但沖大東島全島目前仍屬於RASA工業的私有土地。\[4\]

## 沿革

  - 1911年：RASA島磷礦合資公司成立。（日文原名：）
  - 1913年：成立RASA島磷礦股份有限公司。（日文原名：）
  - 1934年：改名為RASA工業股份有限公司。（日文原名：）
  - 1937年：沖大東島（）由日本國有轉讓為Rasa工業所有。
  - 1983年：肥料部門分割轉給關係企業Co-op化學股份有限公司。
  - 1984年：進入晶圓（Wafer）再生製造產業。

## 工廠

  - 宮古工廠（[岩手縣](../Page/岩手縣.md "wikilink")[宮古市](../Page/宮古市.md "wikilink")）
  - 三本木工廠（[宮城縣](../Page/宮城縣.md "wikilink")[大崎市](../Page/大崎市.md "wikilink")）
  - 仙台工廠（宮城縣[岩沼市](../Page/岩沼市.md "wikilink")）
  - 伊勢崎工廠（[群馬縣](../Page/群馬縣.md "wikilink")[伊勢崎市](../Page/伊勢崎市.md "wikilink")）
  - 野田工廠（[千葉縣](../Page/千葉縣.md "wikilink")[野田市](../Page/野田市.md "wikilink")）
  - 大阪工廠（[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[大正區](../Page/大正區.md "wikilink")）
  - 羽犬塚工廠（[福岡縣](../Page/福岡縣.md "wikilink")[筑後市](../Page/筑後市.md "wikilink")）

## 外部連結

  - [RASA工業](http://www.rasa.co.jp/)

## 參考資料

<div style="font-size: 85%">

<references />

</div>

[L](../Category/日本化學工業公司.md "wikilink")

1.
2.
3.  中文名稱的使用－
4.