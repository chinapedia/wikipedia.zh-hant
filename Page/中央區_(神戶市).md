**中央区**（）是位於[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市市中心的區級行政區劃](../Page/神戶市.md "wikilink")，神戶的交通中心與主要商業街[三宮即位於本區](../Page/三宮.md "wikilink")，此外亦有不少觀光景點位於此區。

## 歷史

在19世紀末為日本首批對外國開放貿易的地區，曾設有[外國人居留地](../Page/神戶外國人居留地.md "wikilink")，在當時有許多外國[領事館設於此](../Page/領事館.md "wikilink")，但現多已遷移至[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")，僅[巴拿馬及](../Page/巴拿馬.md "wikilink")[大韓民國在神戶市設總領事館](../Page/大韓民國.md "wikilink")。

現在的中央區範圍在1896年神戶市最初設立分區時分屬[葺合區](../Page/葺合區.md "wikilink")、湊東區和神戶區三個區，1945年神戶市重編行政區時，廢除了湊東區和神戶區，改設立[生田區](../Page/生田區.md "wikilink")；1980年葺合區與生田區合併為現在的中央區。

## 交通

### 機場

  - [神戶機場](../Page/神戶機場.md "wikilink")

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：[新神戶站](../Page/新神戶站.md "wikilink")
      - [東海道本線](../Page/東海道本線.md "wikilink")、[山陽本線](../Page/山陽本線.md "wikilink")（[JR神戶線](../Page/JR神戶線.md "wikilink")）：[三之宮車站](../Page/三之宮車站.md "wikilink")
        - [元町車站](../Page/元町車站_\(兵庫縣\).md "wikilink") -
        [神戶車站](../Page/神戶站_\(兵庫縣\).md "wikilink")
  - [阪急電鐵](../Page/阪急電鐵.md "wikilink")
      - [神戶本線](../Page/神戶本線.md "wikilink")： -
        [三宮車站](../Page/三宮車站.md "wikilink")
      - [神戶高速線](../Page/神戶高速線_\(阪急電鐵\).md "wikilink")：三宮車站 -  -
  - [阪神電氣鐵道](../Page/阪神電氣鐵道.md "wikilink")
      - [阪神本線](../Page/阪神本線.md "wikilink")： - 三宮車站 - 元町車站
      - [神戶高速線](../Page/神戶高速線_\(阪神電氣鐵道\).md "wikilink")：元町車站 -  - 高速神戶車站
  - [神戶市營地下鐵](../Page/神戶市營地下鐵.md "wikilink")
      - [西神·山手線](../Page/西神·山手線.md "wikilink")：新神戶車站 - 三宮車站 -  -
      - [海岸線](../Page/海岸線_\(神戶市營地下鐵\).md "wikilink")： –  –  –
  - [北神急行電鐵](../Page/北神急行電鐵.md "wikilink")
      - [北神線](../Page/北神線.md "wikilink")：新神戶車站
  - [神戶新交通](../Page/神戶新交通.md "wikilink")
      - [港灣人工島線](../Page/港灣人工島線.md "wikilink")（港灣快車）：三宮車站 -  –  –  –  –
        –  –  – 、市民廣場車站 –  –  –  - 中公園車站（全線區內）

<File:Jr> kobe station - panoramio.jpg|JR神戶車站 <File:Kobe> motomachi
st01s3200.jpg|JR元町車站 <File:Port-liner> Sannomiya station - panoramio
(1).jpg|三宮車站

### 道路

  - 阪神高速道路

<!-- end list -->

  - [3號神戶線](../Page/阪神高速3號神戶線.md "wikilink")

<!-- end list -->

  - 一般國道

<!-- end list -->

  - [國道2號](../Page/國道2號_\(日本\).md "wikilink")
      - [濱手繞道](../Page/濱手繞道.md "wikilink")
  - [國道28號](../Page/國道28號.md "wikilink")
  - [國道171號](../Page/國道171號.md "wikilink")（區內2號重複）
  - [國道174號](../Page/國道174號.md "wikilink") -
    日本最短的[國道](../Page/國道.md "wikilink")
  - [國道428號](../Page/國道428號.md "wikilink")

## 觀光資源

[Kobe_Nunobiki_Waterfalls03s1920.jpg](https://zh.wikipedia.org/wiki/File:Kobe_Nunobiki_Waterfalls03s1920.jpg "fig:Kobe_Nunobiki_Waterfalls03s1920.jpg")
[Sorakuen14st3200.jpg](https://zh.wikipedia.org/wiki/File:Sorakuen14st3200.jpg "fig:Sorakuen14st3200.jpg")
[Kobe_city_museum02_1920.jpg](https://zh.wikipedia.org/wiki/File:Kobe_city_museum02_1920.jpg "fig:Kobe_city_museum02_1920.jpg")
[Ikuta_Jinja_Romon.jpg](https://zh.wikipedia.org/wiki/File:Ikuta_Jinja_Romon.jpg "fig:Ikuta_Jinja_Romon.jpg")
[Kobe_port_island02s3200.jpg](https://zh.wikipedia.org/wiki/File:Kobe_port_island02s3200.jpg "fig:Kobe_port_island02s3200.jpg")
[Hyogo_prefectural_museum_of_art02s3200.jpg](https://zh.wikipedia.org/wiki/File:Hyogo_prefectural_museum_of_art02s3200.jpg "fig:Hyogo_prefectural_museum_of_art02s3200.jpg")

  -

<!-- end list -->

  - （重要文化財）

  -
  -
  -
  -
  -
  -
  - （徳光院市民公園）

  -
  - 、（國家名勝）

  -
<!-- end list -->

  - [神戶港](../Page/神戶港.md "wikilink")

<!-- end list -->

  -   -
      -
  - [美利堅公園](../Page/美利堅公園.md "wikilink")、

      - [神戶港塔](../Page/神戶港塔.md "wikilink")

      - [神戶海洋博物館](../Page/神戶海洋博物館.md "wikilink")

      -
      -
  -   - [神户税关](../Page/神户税关.md "wikilink")

      - [旧国立生丝检查所](../Page/旧国立生丝检查所.md "wikilink")

      -
      -
<!-- end list -->

  - [北野町山本通](../Page/北野町山本通.md "wikilink")、中山手通

<!-- end list -->

  - [風見雞館](../Page/風見雞館.md "wikilink")（重要文化財）

  - [萌黃之館](../Page/萌黃之館.md "wikilink")（重要文化財）

  - [舒埃凯馆](../Page/舒埃凯馆.md "wikilink")

  -
  - [莱茵馆](../Page/莱茵馆.md "wikilink")

  -
  - [北野物语馆](../Page/北野物语馆.md "wikilink")

  - [神戶清真寺](../Page/神戶清真寺.md "wikilink")

  - [相乐园](../Page/相乐园.md "wikilink")

  - [旧哈萨姆住宅](../Page/旧哈萨姆住宅.md "wikilink") （重要文化財）

  - [旧小寺家厩舍](../Page/旧小寺家厩舍.md "wikilink") （重要文化財）

  - [船屋形](../Page/船屋形.md "wikilink")（重要文化財）

  -
  - [魚鱗之家](../Page/魚鱗之家.md "wikilink")

  - [神戶北野美術館](../Page/神戶北野美術館.md "wikilink")

  - [洋馆长屋](../Page/洋馆长屋.md "wikilink")

  - [山手八番馆](../Page/山手八番馆.md "wikilink")

<!-- end list -->

  - [舊居留地](../Page/旧居留地#神户.md "wikilink")

<!-- end list -->

  - [神戶市立博物館](../Page/神戶市立博物館.md "wikilink")

  - [舊居留地十五番館](../Page/舊居留地十五番館.md "wikilink")（重要文化財）

  -
  -
  - [商船三井大樓](../Page/商船三井大樓.md "wikilink")

<!-- end list -->

  - [三宮](../Page/三宮.md "wikilink")

<!-- end list -->

  -
  - [生田神社](../Page/生田神社.md "wikilink")

  - [旧神户联合教会](../Page/旧神户联合教会.md "wikilink")

<!-- end list -->

  - 、[南京町](../Page/南京町.md "wikilink")

<!-- end list -->

  - [兵库县公馆](../Page/兵库县公馆.md "wikilink")

  - [日本基督教团神户教会](../Page/日本基督教团神户教会.md "wikilink")

  - [日本基督教团神户荣光教会](../Page/日本基督教团神户荣光教会.md "wikilink")

  -
  -
  -
  -
  -
<!-- end list -->

  - 神戶車站周邊

<!-- end list -->

  - [舊三菱銀行神戶支店](../Page/舊三菱銀行神戶支店.md "wikilink")

  - [湊川神社](../Page/湊川神社.md "wikilink")

  -
  -
## 学校

  - [神户中华同文学校](../Page/神户中华同文学校.md "wikilink")

## 相關條目

  - [中央區](../Page/中央區.md "wikilink") - 全國中央區列表