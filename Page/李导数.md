在[微分幾何中](../Page/微分幾何.md "wikilink")，**李导数（Lie
derivative）**是一個以[索甫斯·李命名的](../Page/索甫斯·李.md "wikilink")[算子](../Page/算子.md "wikilink")，作用在[流形上的張量場](../Page/流形.md "wikilink")，向量場或[函数](../Page/光滑函数.md "wikilink")，將該張量沿著某個向量場的[流做](../Page/流.md "wikilink")[方向導數](../Page/方向導數.md "wikilink")。因為該作用在座標變換下保持不變，因此，該李導數在一般的流形上都是[定義良好的](../Page/定義良好.md "wikilink")。

所有李导数组成的[向量空间对应于如下的](../Page/向量空间.md "wikilink")[李括号构成一个无限维](../Page/李括号.md "wikilink")[李代数](../Page/李代数.md "wikilink")。

\[[A,B]:= \mathcal{L}_A B  - \mathcal{L}_B A\]

李导数用[向量场表示](../Page/向量场.md "wikilink")，这些向量场可看作*M*上的流（flow,
也就是时变[微分同胚](../Page/微分同胚.md "wikilink")）的[无穷小生成元](../Page/无穷小生成元.md "wikilink")。从另一角度看，*M*上的微分同胚组成的[群](../Page/群.md "wikilink")，有其对应的李导数的李代数结构，在某种意义上和[李群理论直接相关](../Page/李群.md "wikilink")。

## 定义

李导数有几种等价的定义。在本节，为简便起见，我们用标量场和向量场的李导数的定义开始。李导数也可定义在一般的张量上，如后面的章节所述。

李导数的定义可以从函数的[微分开始](../Page/微分.md "wikilink")。这样，给定一个函数\(f:M\rightarrow \mathbb{R}\)和一个*M*上的[向量场](../Page/向量场.md "wikilink")*X*
, *f*在点\(p\in M\)的李导数定义为

\[\mathcal{L}_Xf(p)=df(p)\, [X(p)]\]

其中\(df\)是*f*的微分。也就是，\(df:M\rightarrow T^*M\)是由下式给出的\[1-形式\]

\[df = \frac{\partial f} {\partial x^a} dx^a\].

这里，\(dx^a\)是[余切丛](../Page/余切丛.md "wikilink")\(T^*M\)的[基向量](../Page/基向量.md "wikilink")。这样，记号\(df(p)\, [X(p)]\)表示取*f*（在*M*中的点*p*）的微分和向量场*X*（在点*p*）的[内积](../Page/内积.md "wikilink")。

或者，可以先表明*M*上的光滑向量场*X*定义了一个*M*上的单参数曲线族。也就是，可以表明存在[曲线](../Page/曲线.md "wikilink")\(\gamma(t)\)在*M*上使得

\[\frac{d\gamma}{dt}(t)=X(\gamma(t))\]

其中\(p=\gamma(0)\)对于所有*M*中的点*p*成立。这个一阶[常微分方程的解的存在性由](../Page/常微分方程.md "wikilink")[皮卡-林德洛夫定理给出](../Page/皮卡-林德洛夫定理.md "wikilink")（更一般的，这种曲线的存在性是[弗罗贝尼乌斯定理给出](../Page/弗罗贝尼乌斯定理.md "wikilink")）。然后可以定义李导数为

\[\mathcal{L}_Xf(p)=\frac{d}{dt}  f(\gamma(t)) \vert_{t=0}\].

第三个可能的定义可以通过先定义一对向量场的[李括号给出](../Page/李括号.md "wikilink")。首先注意到[切空间的基向量可以写为](../Page/切空间.md "wikilink")\(\frac{\partial}{\partial x^a}\)，所以一个向量场，用一组选定的基向量可以表示为

\[X=X^a  \frac{\partial}{\partial x^a}\]

定义**[李括号](../Page/李括号.md "wikilink")**\([X,Y]\)为

\[[X,Y]=
X^a \frac{\partial Y^b}{\partial x^a} \frac{\partial}{\partial x^b} -
Y^a \frac{\partial X^b}{\partial x^a} \frac{\partial}{\partial x^b}\]

然后定义向量场*Y*的李导数等于*X*和*Y*的李导数，也就是，

\[\mathcal{L}_X Y = [X,Y]\].

根据上面任选的一个定义，其他的定义可被证明为其等价形式。 例如，可以证明，对于一个可微函数*f*，

\[\mathcal{L}_X (f) = df(X) = X(f)\]

并且

\[[X,Y]f = X(Y( f )) - Y(X( f ))\].

我们用在[1-形式](../Page/1-形式.md "wikilink")\(\omega = \omega_a dx^a\)上的李导数的定义来结束本节：

\[\mathcal{L}_X \omega =
\left(\frac{\partial \omega_b} {\partial x^a} X^a +
\frac{\partial X^a} {\partial x^b} \omega_a \right) dx^b\].

## 性质

李导数有一些属性。令\(\mathcal{F}(M)\)为[流形](../Page/流形.md "wikilink")*M*上的函数组成的[代数](../Page/代数.md "wikilink")。则

\[\mathcal{L}_X : \mathcal{F}(M) \rightarrow \mathcal{F}(M)\]

是一个在代数\(\mathcal{F}(M)\)上的[导数](../Page/导数.md "wikilink")。也就是，
\(\mathcal{L}_X\)是**R**-线性的，并且

\[\mathcal{L}_X(fg)=(\mathcal{L}_Xf) g + f\mathcal{L}_Xg\].

类似的，它是\(\mathcal{F}(M) \times \mathcal{X}(M)\)上的一个导数，其中\(\mathcal{X}(M)\)是*M*上的向量场的集合：

\[\mathcal{L}_X(fY)=(\mathcal{L}_Xf) Y + f\mathcal{L}_X Y\]

也可写为等价形式

\[\mathcal{L}_X(f\otimes Y)=
(\mathcal{L}_Xf) \otimes Y + f\otimes \mathcal{L}_X Y\]

其中[张量积符号](../Page/张量积.md "wikilink")\(\otimes\)用于强调函数和向量场的积在整个流形上取。

另外的性质和[李括号的一致](../Page/李括号.md "wikilink")。所以，例如，作为向量场的导数，

\[\mathcal{L}_X [Y,Z] = [\mathcal{L}_X Y,Z] + [Y,\mathcal{L}_X Z]\]

容易发现上面就是[雅可比恒等式](../Page/雅可比恒等式.md "wikilink")。这样，就可以得到“装备了李括号的*M*上的向量空间是[李代数](../Page/李代数.md "wikilink")”的重要结果。

## 和外导数的关系、微分形式的李导数

李导数和[外导数密切相关](../Page/外导数.md "wikilink")，因此和[埃里·嘉当的](../Page/埃里·嘉当.md "wikilink")[微分流形理论相关](../Page/微分流形.md "wikilink")。
两个都试图给出导数的思想，其差别几乎只是记号上的。这个区别可以通过引入**反导数**或等效的[内积来消除](../Page/内积.md "wikilink")。
这之后，两者的关系就体现在一组恒等式上。

令*M*为一个流形，*X*为*M*上一个向量场。令\(\omega \in \Lambda^{k+1}(M)\)为一*k*+1-形式。
*X*和ω的**内积**为

\[i_X\omega (X_1,\ldots,X_k) = \omega (X,X_1,\ldots,X_k)\]

注意

\[i_X:\Lambda^{k+1}(M) \rightarrow \Lambda^k(M)\]

以及\(i_X\)是\(\wedge\)-[反导数](../Page/反导数.md "wikilink")。也就是，\(i_X\)是**R**-线性的，并且

\[i_X (\omega \wedge \eta) =
(i_X \omega) \wedge \eta + (-1)^k \omega \wedge (i_X \eta)\]

对于\(\omega \in \Lambda^k(M)\)和另一个微分形式η成立。另外，对于一个函数\(f \in \Lambda^0(M)\)，那是一个实或复值
的*M*上的函数，有

\[i_{fX} \omega = fi_X\omega\]

[外导数和李导数的关系可以总结为以下这些](../Page/外导数.md "wikilink")。对于一般函数*f*，李导数就是外导数和向量场的内积：

\[\mathcal{L}_Xf = i_X df\]

对于一般的微分流形，李导数类似于内积，加上*X*的变化：

\[\mathcal{L}_X\omega = i_Xd\omega + d(i_X \omega)\].

当ω为1-形式，上述恒等式经常写作

\[d\omega(X,Y)=X(\omega(Y))-Y(\omega(X))-\omega([X,Y]).\]

导数的乘积是可分配的

\[\mathcal{L}_{fX}\omega =
f\mathcal{L}_X\omega + df \wedge i_X \omega\]

## 张量场的李导数

在[微分几何中](../Page/微分几何.md "wikilink")，如果我们有一个\((p,q)\)[阶](../Page/张量阶.md "wikilink")[可微](../Page/可微.md "wikilink")[张量场](../Page/张量场.md "wikilink")（我们可以把它当作[余切丛](../Page/余切丛.md "wikilink")\(T^*M\)的[光滑](../Page/光滑.md "wikilink")[截面](../Page/截面_\(纤维丛\).md "wikilink")\(\alpha, \beta, \ldots\)和[切丛](../Page/切丛.md "wikilink")\(TM\)的截面\(X, Y, \ldots\)的线性映射
\(T (\alpha, \beta, \ldots, X, Y, \ldots )\)），使得对于任何函数
\(f_1,\ldots,f_p,f_{p+1},\ldots,f_{p+q}\)有

\[T(f_1\alpha,f_2\beta,\ldots,f_{p+1}X,f_{p+2}Y,\ldots) = f_1 f_2 \cdots f_{p+1} f_{p+2} \cdots f_{p+q} T(\alpha,\beta,\ldots,X,Y,\ldots)\]),

而且如果进一步有一个可微[向量场](../Page/向量场.md "wikilink")（也就是[切丛的一个光滑截面](../Page/切丛.md "wikilink")）\(A\)，则线性映射

\[(\mathcal{L}_{A}T)(\alpha, \beta, \ldots, X, Y, \ldots) \equiv \nabla_A T(\alpha,\beta,\ldots,X,Y,\ldots) - \nabla_{T(\cdot, \beta, \ldots, X, Y, \ldots)} \alpha(A) - \ldots + T(\alpha, \beta, \ldots, \nabla_X A, Y, \ldots) + \ldots\]

独立于[联络](../Page/联络.md "wikilink")∇；只要它是无[挠率的](../Page/挠率.md "wikilink")，事实上，这个映射是一个[张量](../Page/张量.md "wikilink")。这个张量称为\(T\)关于\(A\)的**李导数**。

换句话说，如果你有一个张量场\(T\)和一个由向量场\(U\)给出的微分同胚的无穷小生成元，则\(\mathcal{L}_{U} T\)就是\(T\)在这个无穷小微分同胚下的无穷小变化。

或者，给定向向量场\(U\)，令ψ为\(U\)的积分曲线族，向上面那样。注意ψ是一个局部单参数局部微分同胚[群](../Page/群.md "wikilink")。令\(\psi^*\)为由ψ诱导的[拉回](../Page/拉回.md "wikilink")（pullback）。则张量\(T\)在\(p\)点的李导数如下

\[\mathcal{L}_U T = \frac{d}{dt}\left(\psi^*_t T\right) \vert_{\psi(t)=p}\].

## 参见

  - [基灵场](../Page/基灵向量场.md "wikilink")
  - [李群](../Page/李群.md "wikilink")
  - [测地线](../Page/测地线.md "wikilink")
  - [协变导数](../Page/协变导数.md "wikilink")
  - [联络](../Page/联络.md "wikilink")

## 参考

  - Jurgen Jost, *Riemannian Geometry and Geometric Analysis*, (2002)
    Springer-Verlag, Berlin ISBN 3-540-4267-2 *See section 1.6*.
  - [Ralph Abraham](../Page/Ralph_Abraham.md "wikilink") and Jerrold E.
    Marsden, *Foundations of Mechanics*, (1978) Benjamin-Cummings,
    London ISBN 0-8053-0102-X *See section 2.2*.
  - David Bleecker, *Gauge Theory and Variational Principles*, (1981),
    Addison-Wesley Publishing, ISBN 0-201-10096-7. *See Chapter 0*.

[L](../Category/微分几何.md "wikilink") [L](../Category/黎曼几何.md "wikilink")
[L](../Category/二元運算.md "wikilink")
[Category:导数的推广](../Category/导数的推广.md "wikilink")