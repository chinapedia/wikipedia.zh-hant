**拉夫堡大學**（Loughborough
University），或译**拉夫伯勒大学**、**罗浮堡大学**，位于[英國](../Page/英國.md "wikilink")[英格兰中部](../Page/英格兰.md "wikilink")[东米德兰兹地区](../Page/东米德兰兹.md "wikilink")[莱斯特郡](../Page/莱斯特郡.md "wikilink")[拉夫堡鎮](../Page/拉夫堡.md "wikilink")，乘火车到伦敦需75分钟左右。

## 学校历史

[Hazlerrigg_Front_Lawn.JPG](https://zh.wikipedia.org/wiki/File:Hazlerrigg_Front_Lawn.JPG "fig:Hazlerrigg_Front_Lawn.JPG")
来源于1909年在[拉夫堡](../Page/拉夫堡.md "wikilink")（Loughborough）成立的一个小型技术学院，后来发展为拉夫堡学院（Loughborough
College）。学院由于体育方面的成就很快在1929年加入了大学体育运动联盟（Universities Athletic
Union）。1952年，学院划分为四个分离的机构：工程系成为拉夫堡理工学院（Loughborough College of
Technology）；教师培训部分成为拉夫堡教师培训学院，1963年更名为拉夫堡教育学院；艺术与工艺系成为拉夫堡艺术与设计学院；继续教育系成为拉夫堡技术学院（Loughborough
Technical College）。

1957年，开设高等工程和应用科学课程的拉夫伯勒理工学院被命名为高等理工学院（College of Advanced
Technology）。此后学校发展迅速，于1966年4月19日成为拉夫堡理工大学（Loughborough
University of Technology）。从获准成立大学以来，学生数量从1850人增加到几乎15000人。

## 著名校友

校友包括许多[体育界名人](../Page/体育.md "wikilink")，如打破世界纪录的[塞巴斯蒂安·柯伊爵士](../Page/塞巴斯蒂安·柯伊.md "wikilink")，David
Moorcroft，Paula Radcliffe和Steve
Backley；著名[残疾人奥林匹克运动会](../Page/残疾人奥林匹克运动会.md "wikilink")（Paralympic
Games）运动员Tanni
Grey-Thompson；[橄榄球世界杯](../Page/橄榄球.md "wikilink")[英格兰队教练Sir](../Page/英格兰.md "wikilink")
Clive
Woodward；还有[苏格兰和](../Page/苏格兰.md "wikilink")[阿森纳队](../Page/阿森纳队.md "wikilink")(Arsenal)[足球运动员Bob](../Page/足球.md "wikilink")
Wilson。

## 学校声誉

该大学在英国国内排在中上游，国际上则在前400名之内。

## 学校现状

全校現有師生共17000多名，其中國際學生超過2500名。目前有來自中國大陸的學生約800名，其中超過三分之二的學生在攻讀商學院和經濟系的授課型碩士課程。

## 参考资料

<references/>

## 外部链接

  - [拉夫堡大学](http://www.lboro.ac.uk)
  - [拉夫堡学生会](http://www.lufbra.net)

[Category:英格兰大学](../Category/英格兰大学.md "wikilink")
[Category:1909年創建的教育機構](../Category/1909年創建的教育機構.md "wikilink")