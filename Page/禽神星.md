****（Nemausa）是第51颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1858年1月22日被[法国的](../Page/法国.md "wikilink")[約瑟夫·讓·皮埃爾·洛朗发现](../Page/約瑟夫·讓·皮埃爾·洛朗.md "wikilink")，并用發現地法國[尼姆](../Page/尼姆.md "wikilink")（法文：Nîmes）的拉丁名命名。的[直径为](../Page/直径.md "wikilink")147.9千米，[质量为](../Page/质量.md "wikilink")3.4×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1328.853天。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1858年发现的小行星](../Category/1858年发现的小行星.md "wikilink")