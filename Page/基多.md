**基多**（），是[南美洲](../Page/南美洲.md "wikilink")[国家](../Page/国家.md "wikilink")[厄瓜多尔的](../Page/厄瓜多尔.md "wikilink")[首都](../Page/首都.md "wikilink")，位于该国北部，离[赤道仅](../Page/赤道.md "wikilink")24公里。是[世界上距](../Page/世界.md "wikilink")[赤道最近的](../Page/赤道.md "wikilink")[首都](../Page/首都.md "wikilink")。因地处[高原](../Page/高原.md "wikilink")，[海拔高度](../Page/海拔.md "wikilink")2852米，是仅次于[玻利维亚](../Page/玻利维亚.md "wikilink")[拉巴斯的世界第二高](../Page/拉巴斯.md "wikilink")[首都](../Page/首都.md "wikilink")，一年中月平均[气温都在](../Page/气温.md "wikilink")15℃左右，年平均温差只有1℃，四季如[春](../Page/春.md "wikilink")。

## 历史

[基多原来是](../Page/基多.md "wikilink")[印第安部落](../Page/印第安.md "wikilink")（Quitu）的居住地。8世纪时，基图逐渐扩大并吞并了其它印第安部落，到10世纪的时候，成为一个完整的。今日的地名基多，便是从基图演变而来的。

公元15世纪末时，基多王国成为[印加帝国的政治](../Page/印加帝国.md "wikilink")、经济和文化中心，是僅次於首都[庫斯科的第二大城市](../Page/庫斯科.md "wikilink")，印加帝國最後一任國王[阿塔瓦爾帕的宮殿亦興建於基多](../Page/阿塔瓦爾帕.md "wikilink")，但今日這座宮殿已成為厄瓜多爾的總統府。1526年[西班牙殖民者入侵](../Page/西班牙.md "wikilink")，1534年完全占领。当时驻守基多的印加将军[卢米尼亚维把城市彻底摧毁](../Page/卢米尼亚维.md "wikilink")。今天的基多市是由西班牙在印加帝国的废墟建立起来的。1809年西班牙第一份於[南美洲簽訂的獨立宣言厄瓜多爾獨立宣言就是在基多的聖阿古斯丁修道院進行](../Page/南美洲.md "wikilink")，但直至1830年[厄瓜多尔才正式](../Page/厄瓜多.md "wikilink")[独立](../Page/独立.md "wikilink")，后來定[基多为](../Page/基多.md "wikilink")[首都](../Page/首都.md "wikilink")。1978年被[联合国教育、科学及文化组织列为](../Page/联合国教育、科学及文化组织.md "wikilink")[世界文化遗产](../Page/世界文化遗产.md "wikilink")。

## 概况

基多市分为新城、旧城两部分，西南部是旧城区，许多地方保留了印第安人和西班牙人的建筑风格。新城座落在北部。城区东西两侧的[皮钦查火山海拔](../Page/皮钦查火山.md "wikilink")4790米，峰顶长年积雪，根据2001年的人口统计，基多总人口1,839,853人。

[Ecuador_SanAntoniodePichincha_MitaddelMundo_Monument.JPG](https://zh.wikipedia.org/wiki/File:Ecuador_SanAntoniodePichincha_MitaddelMundo_Monument.JPG "fig:Ecuador_SanAntoniodePichincha_MitaddelMundo_Monument.JPG")
基多市内共有大小教堂、修道院87座，著名的有[圣弗朗西斯科教堂](../Page/聖方濟各堂_\(基多\).md "wikilink")、[耶穌會教堂等](../Page/耶穌會教堂_\(基多\).md "wikilink")。圣弗朗西斯科教堂被视为[巴洛克式建筑风格的杰作](../Page/巴洛克式建筑.md "wikilink")，是西班牙--美洲宗教建筑的典范之一，它由一座大教堂、几座小教堂和众多的回廊组成。教堂内珍藏着印第安人、西班牙人的绘画和雕塑名作。耶穌會教堂建于1722年至1765年间，教堂正面拱形大门上、四周墙壁以及天花板上镶嵌有精美的金叶图案，富有珍贵的历史文化价值。位於舊城中心的大聖堂，裡面安放了南美的解放英雄[西蒙·玻利瓦爾的靈柩](../Page/西蒙·玻利瓦爾.md "wikilink")。

位于该城市的人应该都知道，由于赤道是刚刚好经过该城市，所以该城市也是太阳在一年来两次来往于南北半球所经过的地方（又称“太阳之路”），后来科学家证实了这一说法\[1\]。于是市民就在该城市的郊外地方修建了一座[赤道纪念碑](../Page/基多赤道纪念碑.md "wikilink")。

城市南端的（Panecillo），高約200公尺（海拔3016公尺），山顶上有一座大型基多女神石雕像（Monumento a la Virgen
de Quito），被誉为基多人民争取独立自由的象征。山腰有一座古老的印加神庙（Museo Templo de la
Patria），现已成为博物馆。

基多市长一度决定将住宅墙壁刷成白色，门窗的木结构部分则刷成蓝色，致使该城有点类似于（仅指这一方面而言）地中海城镇。后来，不知是谁想起了18世纪时市中心的一些主要建筑都曾将临街的一面作过彩绘，于是墙面又被刷成了黄色、绿色或蓝色。但另一方面，在城北住宅区的现代公共建筑中，主导色却是混凝土的灰色和玻璃窗的烟色，为的是好抵挡住或许因蓝天伸手可及而特别耀眼的阳光，这与世界其他同纬度地区的情况颇不相同。\[2\]

## 气候

根据[柯本气候分类法](../Page/柯本气候分类法.md "wikilink")，该城市气候是属于[海洋性气候](../Page/海洋性气候.md "wikilink")\[3\]
(Cfb).\[4\]
虽然基多位于赤道地区附近，但该城市不像[新加坡](../Page/新加坡.md "wikilink")、[马累或](../Page/马累.md "wikilink")[雅加达等等其它这些在赤道附近的低海拔城市那么热](../Page/雅加达.md "wikilink")。由於基多是位于高海拔地区，所以每年的气候四季如春。中午平均气温只有，且午夜的平均最低气温僅有.\[5\]；全年平均气温只有。\[6\]该城市只有两季——[旱季和](../Page/旱季.md "wikilink")[雨季](../Page/雨季.md "wikilink")。从6月至9月是旱季，剩下的月份都是雨季，年平均降水量約在左右。

<div>

| 基多气候平均数据                             |
| ------------------------------------ |
| 月份                                   |
| 平均每日白昼时数                             |
| 平均[紫外指数](../Page/紫外指数.md "wikilink") |
| Source: Weather Atlas \[7\]          |

</div>

## 友好城市

[历史悠久的基多市中心](https://zh.wikipedia.org/wiki/File:Plaza_de_San_Francisco_en_Centro_histórico_de_Quito,_Ecuador.JPG "fig:历史悠久的基多市中心")

## 参考文献

  - [基多市政府官方網站](http://www.quito.gob.ec/)
  - [基多觀光局官方網站](http://www.quito.com.ec/)

## 参考来源

[Category:南美洲首都](../Category/南美洲首都.md "wikilink")
[Category:厄瓜多城市](../Category/厄瓜多城市.md "wikilink")
[Category:厄瓜多世界遺產](../Category/厄瓜多世界遺產.md "wikilink")

1.  中国少年儿童百科全书（自然，环境）。浙江教育出版社，第190页）
2.
3.
4.
5.
6.
7.