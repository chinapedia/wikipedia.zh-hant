**Presto**是一個由[Opera
Software開發的](../Page/Opera_Software.md "wikilink")[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")[排版引擎](../Page/排版引擎.md "wikilink")，由[Opera](../Page/Opera.md "wikilink")
7.0至12.18版本所使用。

Presto取代了舊版Opera
4至6版本使用的[Elektra排版引擎](../Page/Elektra.md "wikilink")，包括加入動態功能，例如網頁或其部分可隨著[DOM及Script語法的事件而重新排版](../Page/文档对象模型.md "wikilink")。Presto在推出後不斷有更新版本推出，使不少錯誤得以修正，以及閱讀JavaScript效能得以最佳化。

2013年2月12日，Opera宣佈將結束Presto引擎的開發，未來將以[Webkit引擎為主](../Page/Webkit.md "wikilink")\[1\]。

2016年2月15日，传出Opera被收购的消息，同日Presto内核版Opera更新到[12.18](ftp://ftp.opera.com/pub/opera/win/1218/en/)。

## JavaScript引擎

Opera的Pre-Presto版本使用了Linear A引擎。以Presto的Core fork為基礎，Opera
7.0至9.27的Opera版本使用了Linear B引擎\[2\]。Futhark引擎使用在Presto的Core 2
fork的一些版本，即Opera
9.5至10.10版本。在當時，Futhark是世界上最快的引擎，但在2008年，新一代[JavaScript引擎如](../Page/JavaScript引擎.md "wikilink")[Google](../Page/Google.md "wikilink")（[V8](../Page/V8_\(JavaScript引擎\).md "wikilink")）、[Mozilla](../Page/Mozilla.md "wikilink")（[TraceMonkey](../Page/TraceMonkey.md "wikilink")）及[Apple](../Page/苹果公司.md "wikilink")（[SquirrelFish](../Page/WebKit#元件.md "wikilink")）已領先一步，加入了[機器語言生成](../Page/機器語言.md "wikilink")。這開闢了在客戶端和沉重的計算的可能性，Futhark雖然依舊快速和高效，但仍然無法企及。

2009年2月5日，Opera推出了Carakan引擎，擁有基於暫存器的[位元組碼](../Page/位元組碼.md "wikilink")，機器語言生成，自動對象分類及整體性能改進\[3\]\[4\]。

## 以Presto為基礎的應用程式

### 網路瀏覽器

  - Opera
      - [Opera](../Page/Opera電腦瀏覽器.md "wikilink") 7至12版本
      - [Opera Mobile](../Page/Opera_Mobile.md "wikilink") 9.5至12版本
      - [Opera Mini](../Page/Opera_Mini.md "wikilink")
  - Nintendo
      - [Nintendo DS
        Browser](../Page/Opera_Devices#Nintendo_DS_Browser.md "wikilink")（基於Opera）\[5\]
      - [Nintendo DSi
        Browser](../Page/Opera_Devices.md "wikilink")（基於Opera）\[6\]
      - [Wii](../Page/Wii.md "wikilink") [Internet
        Channel瀏覽器](../Page/Internet_Channel.md "wikilink")（基於Opera）\[7\]
  - [Nokia 770瀏覽器](../Page/諾基亞770網路終端.md "wikilink")（基於Opera）
  - [Sony Mylo](../Page/mylo.md "wikilink") COM-1's瀏覽器（基於Opera）\[8\]

### HTML編輯器

  - [Adobe Dreamweaver](../Page/Adobe_Dreamweaver.md "wikilink")
    MX至CS3版本（CS4及以後版本使用[Webkit](../Page/Webkit.md "wikilink")）
  - [Adobe Creative Suite](../Page/Adobe_Creative_Suite.md "wikilink")
    2\[9\]
  - [Adobe Creative Suite](../Page/Adobe_Creative_Suite.md "wikilink")
    3\[10\]
  - [Virtual Mechanics SiteSpinner
    Pro](../Page/Virtual_Mechanics_SiteSpinner_Pro.md "wikilink")\[11\]

## 原始碼洩漏

原始碼被洩漏到[GitHub](../Page/GitHub.md "wikilink")\[12\]\[13\]\[14\]\[15\]\[16\]\[17\]。2017年1月14日，在[數位千禧年著作權法的要求下被刪除](../Page/數位千禧年著作權法.md "wikilink")\[18\]。2017年1月18日，[Opera軟體公司已經確認了原始碼的真實性](../Page/Opera軟體公司.md "wikilink")\[19\]。

## 参考资料

[Category:2003年軟體](../Category/2003年軟體.md "wikilink")
[Category:網際網路的歷史](../Category/網際網路的歷史.md "wikilink")
[Category:Opera Software](../Category/Opera_Software.md "wikilink")
[Category:排版引擎](../Category/排版引擎.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.