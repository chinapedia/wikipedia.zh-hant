**鹿港辜家**，[臺灣知名望族](../Page/臺灣.md "wikilink")，[五大家族之一](../Page/臺灣五大家族.md "wikilink")，发迹于[彰化](../Page/彰化縣_\(清朝\).md "wikilink")[鹿港而得名](../Page/鹿港_\(大字\).md "wikilink")。該家族的始祖為[辜顯榮](../Page/辜顯榮.md "wikilink")，其於[臺灣日治時期因經商而崛起](../Page/臺灣日治時期.md "wikilink")，家族至今在臺灣政商界仍有舉足輕重的地位；2001年，家族旗下的主要[企業分為](../Page/企業.md "wikilink")[辜岳甫家族](../Page/辜岳甫.md "wikilink")（子[辜濂松](../Page/辜濂松.md "wikilink")）的「**[中信辜家](../Page/中國信託金融控股公司.md "wikilink")**」（[中信集團為主](../Page/中國信託金融控股公司.md "wikilink")，含[中租控股](../Page/中租控股.md "wikilink")，並為[開發金控大股東](../Page/中華開發金融控股公司.md "wikilink")），及[辜振甫家族的](../Page/辜振甫.md "wikilink")「**[和信辜家](../Page/和信集團.md "wikilink")**」（[和信集團為主含](../Page/和信集團.md "wikilink")[臺泥](../Page/臺泥.md "wikilink")、[雲朗觀光集團等](../Page/雲朗觀光集團.md "wikilink")）。

## 家族历史

[Lugang_Koo's_House.JPG](https://zh.wikipedia.org/wiki/File:Lugang_Koo's_House.JPG "fig:Lugang_Koo's_House.JPG")
鹿港辜家祖籍[福建](../Page/福建.md "wikilink")[泉州三邑](../Page/泉州三邑.md "wikilink")[惠安](../Page/惠安.md "wikilink")，曾祖父辜禮歡早期因反抗滿清而從惠安輾轉逃到暹羅及馬來西亞檳城，為檳城富商及僑領。辜禮歡次子辜安平再由檳城遷居[鹿港](../Page/鹿港.md "wikilink")
，至[辜顯榮一代始發跡](../Page/辜顯榮.md "wikilink")。[辜振甫的](../Page/辜振甫.md "wikilink")[姑父是清末著名大](../Page/姑父.md "wikilink")[买办](../Page/买办.md "wikilink")、大实业家[盛宣怀](../Page/盛宣怀.md "wikilink")。日治時期，當時的[臺灣總督府特別](../Page/臺灣總督府.md "wikilink")“惠賜”辜家於鹿港建造一座[洋樓宅邸](../Page/洋樓.md "wikilink")，即今天的[鹿港民俗文物館](../Page/鹿港民俗文物館.md "wikilink")。

時序回到中日[甲午戰爭結束](../Page/甲午戰爭.md "wikilink")，清廷戰敗、[臺灣割讓予日本](../Page/臺灣割讓.md "wikilink")，短暫成立的[臺灣民主國亦在](../Page/臺灣民主國.md "wikilink")「大總統」[唐景崧潛離之後瓦解](../Page/唐景崧.md "wikilink")，當時[臺北城內亂兵四出騷擾劫掠](../Page/臺北城.md "wikilink")，[艋舺](../Page/艋舺.md "wikilink")[仕紳連袂趕赴](../Page/仕紳.md "wikilink")[大稻埕茶葉鉅商](../Page/大稻埕.md "wikilink")[李春生宅邸商議](../Page/李春生.md "wikilink")。李春生眼見社會亂象，痛心疾首，認為臺灣民主國一無可取，且敗兵劫掠，擾亂社會秩序，臺灣應依[馬關條約由](../Page/馬關條約.md "wikilink")[日本統治](../Page/日本.md "wikilink")，故促請該[日軍早日進城鎮撫](../Page/日軍.md "wikilink")。惟請師的呈文雖已擬好，仕紳們既害怕城外的「暴徒」會威脅到他們的生命財產，又怕率先去迎接日軍進城，萬一臺灣民主國捲土重來，恐怕會遭[整肅](../Page/整肅.md "wikilink")，因此無人人敢冒險呈遞文書。此時，艋舺雜貨店「瑞昌成號」店主[辜顯榮自告奮勇](../Page/辜顯榮.md "wikilink")，向眾人表示願意前往[基隆呈遞文書](../Page/基隆.md "wikilink")，促請日軍進入臺北城維持秩序。在辜顯榮的導引之下，日軍於5月15日（陽曆6月7日）凌晨輕易地長驅直入臺北城。

辜顯榮在日治時代第一次被日人委以重任的角色是「臺北[保良局局長](../Page/保良局.md "wikilink")」，當時的他才三十一歲，後來他更創立「[保甲總局](../Page/保甲.md "wikilink")」，在保甲內實施「連坐法」，阻止臺灣本地「抗日軍」之勢力，並組織壯丁團，討伐抗日軍。1923年，以[霧峰林家的](../Page/霧峰林家.md "wikilink")[林獻堂為首所領導之](../Page/林獻堂.md "wikilink")「[臺灣議會設置請願運動](../Page/臺灣議會設置請願運動.md "wikilink")」正蓬勃發展之際，以辜顯榮為首另組成所謂「公益會」，成為與「議會請願運動」對抗的團體。1924年辜更召開「有力者大會」，在[東京各大報紙刊登廣告](../Page/東京.md "wikilink")，指[林獻堂等人領導的議會請願運動並非](../Page/林獻堂.md "wikilink")[臺灣人真正的意願等](../Page/臺灣人.md "wikilink")，藉此對抗如火如荼的臺灣民族運動。自此，許多[臺灣人稱辜顯榮為](../Page/臺灣人.md "wikilink")「[漢奸](../Page/漢奸.md "wikilink")」。

由於辜顯榮對[臺灣總督府方面的各項措施極力配合](../Page/臺灣總督府.md "wikilink")，故順利取得許多特權事業，如[鹽](../Page/鹽.md "wikilink")、[糖](../Page/糖.md "wikilink")、[鴉片](../Page/鴉片.md "wikilink")、[樟腦等經營權](../Page/樟腦.md "wikilink")，並積極從事[土地開墾](../Page/土地.md "wikilink")、[金融事業等各項投資](../Page/金融.md "wikilink")，使得辜顯榮的事業版圖在日治時期達到巔峰，並為鹿港辜家累積了龐大的財富、人脈及社經地位。

## 家族名人

  - [辜顯榮](../Page/辜顯榮.md "wikilink")：辜振甫、辜寬敏的父親，原籍[福建](../Page/福建.md "wikilink")[泉州](../Page/泉州.md "wikilink")[惠安](../Page/惠安.md "wikilink")，一般視為鹿港辜家事業之開創者
  - [辜振甫](../Page/辜振甫.md "wikilink")：辜顯榮之子，長期擔任中華民國[海基會董事長](../Page/海基會.md "wikilink")，曾於1992年與中華人民共和國[海協會會長](../Page/海協會.md "wikilink")[汪道涵於](../Page/汪道涵.md "wikilink")[新加坡進行](../Page/新加坡.md "wikilink")[辜汪會談](../Page/辜汪會談.md "wikilink")
  - [辜嚴倬雲](../Page/辜嚴倬雲.md "wikilink")：辜振甫之妻，北京大學首任校長[嚴復之孫女](../Page/嚴復.md "wikilink")，臺灣[板橋林家林爾康之外孫女](../Page/板橋林家.md "wikilink")，長期擔任中華民國[婦聯會會長](../Page/婦聯會.md "wikilink")
  - [辜寬敏](../Page/辜寬敏.md "wikilink")：辜顯榮之子，辜振甫胞弟（同父異母），[臺灣獨立運動重要的參與者](../Page/臺灣獨立運動.md "wikilink")
  - [辜朝明](../Page/辜朝明.md "wikilink")：辜寬敏之子、辜顯榮之孫，日本[智庫野村總合研究所首席](../Page/智庫.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")

## 先祖及遠房親戚

根據辜振甫傳記《勁寒梅香》內文記載，臺灣鹿港辜家與[馬來西亞](../Page/馬來西亞.md "wikilink")[檳城辜家同源](../Page/檳城.md "wikilink")。

  - [辜禮歡](../Page/辜禮歡.md "wikilink")：辜顯榮之[曾祖父](../Page/曾祖父.md "wikilink")，早年由福建[閩南地區下南洋](../Page/閩南地區.md "wikilink")，為[馬來亞](../Page/馬來亞.md "wikilink")[檳榔嶼](../Page/檳榔嶼.md "wikilink")（今檳城）開埠先賢、商人及僑領（[甲必丹](../Page/甲必丹.md "wikilink")）
  - [辜鴻銘](../Page/辜鴻銘.md "wikilink")：辜顯榮遠房堂兄（兩人為從祖兄弟，皆為辜禮歡曾孫），檳城華僑，清末知名[國學家](../Page/國學.md "wikilink")、[西學家](../Page/西學.md "wikilink")、[翻譯家](../Page/翻譯.md "wikilink")、[作家](../Page/作家.md "wikilink")\[1\]

## 姻親

[臺北](../Page/臺北.md "wikilink")[板橋林家](../Page/板橋林家.md "wikilink")

[福州](../Page/福州.md "wikilink")[嚴復家族](../Page/嚴復.md "wikilink")

[上海](../Page/上海.md "wikilink")[盛宣懷家族](../Page/盛宣懷.md "wikilink")

## 族譜

<center>

</center>

## 家族企業

  - [中信集團](../Page/中國信託金融控股公司.md "wikilink")
  - [和信集團與](../Page/和信集團.md "wikilink")[臺泥企業團](../Page/台灣水泥.md "wikilink")

## 另見

  - [辜汪會談](../Page/辜汪會談.md "wikilink")
  - [橋本九八氏頌德碑](../Page/橋本九八氏頌德碑.md "wikilink")

## 參考資料

  - 陳郁秀編著、陳淳如註解，《台灣民主歌》（台南市：台灣史博館籌備處，2002）
  - 司馬嘯青，《台灣五大家族（下）》（台北市：自立晚報，1987）

[Category:彰化縣歷史](../Category/彰化縣歷史.md "wikilink")
[\*](../Category/鹿港辜家.md "wikilink")
[Category:鹿港人](../Category/鹿港人.md "wikilink")
[Category:鹿港鎮](../Category/鹿港鎮.md "wikilink")
[Category:台灣家族](../Category/台灣家族.md "wikilink")
[Category:辜姓](../Category/辜姓.md "wikilink")
[Category:台灣五大家族](../Category/台灣五大家族.md "wikilink")

1.  <https://books.google.com.tw/books?id=-MBdAAAAIAAJ&q=%E5%8B%81%E5%AF%92%E6%A2%85%E9%A6%99+%E8%BE%9C%E7%A6%AE%E6%AD%A1+%E8%BE%9C%E9%B4%BB%E9%8A%98&dq=%E5%8B%81%E5%AF%92%E6%A2%85%E9%A6%99+%E8%BE%9C%E7%A6%AE%E6%AD%A1+%E8%BE%9C%E9%B4%BB%E9%8A%98&hl=zh-TW&sa=X&ved=0ahUKEwiMqMWDz8_OAhVIW5QKHfkZBi8Q6AEIHDAA>