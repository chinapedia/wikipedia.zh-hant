**施馬田**（**Martin Christopher
SAYER**，），[香港網球員](../Page/香港.md "wikilink")，曾高踞全美青少年網球排名榜第14位。單打世界排名最高為1303位，雙打則為1336位。

## 簡歷

2002年代表香港參加[台維斯盃少年組的賽事](../Page/台維斯盃少年組.md "wikilink")，2005年首次代表香港參加[台維斯盃](../Page/台維斯盃.md "wikilink")，並在賽事中取得11勝3負的戰績。現正在美國[邁亞密一所大學攻讀學位](../Page/邁亞密.md "wikilink")，並有參加[NCAA的比賽](../Page/NCAA.md "wikilink")，在2008/09年的賽季中取得參賽以來的第100場單打勝仗，亦自06年開始連續3年獲得[NCAA
Men's Division I Big South Conference Player of the Year
Award](../Page/NCAA_Men's_Division_I_Big_South_Conference_Player_of_the_Year_Award.md "wikilink")。

根據台維斯盃官方網頁的紀錄，施馬田與[洪立熙的雙打組合是港隊史上成績最好的一隊](../Page/洪立熙.md "wikilink")，共取得4勝1負的成績。此外，2009年3月，施馬田於是項盃賽第一圈首回合的賽事中出戰[菲律賓代表](../Page/菲律賓.md "wikilink")[馬密特](../Page/馬密特.md "wikilink")，雙方總共打了58局，為香港於台盃參賽以來單場最多局數的比賽，該場球賽施馬田最終以4-6,6-4,6-3,6-7,7-9惜敗給對手。

2010年，代表[中國香港參加](../Page/2010年亞洲運動會中國香港代表團.md "wikilink")[2010年亞洲運動會](../Page/2010年亞洲運動會.md "wikilink")，出戰男子單打、混合雙打和男子團體項目，惟兩項賽事都於首場落敗後出局。

## 外部連結

  -
  -
  -
[Category:香港網球運動員](../Category/香港網球運動員.md "wikilink")