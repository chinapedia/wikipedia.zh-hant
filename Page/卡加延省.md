**卡加延省**（**Cagayan**）是[菲律賓](../Page/菲律賓.md "wikilink")[呂宋](../Page/呂宋.md "wikilink")[卡加延河谷](../Page/卡加延河谷.md "wikilink")[政區的一個](../Page/菲律賓政區.md "wikilink")[省份](../Page/菲律賓省份.md "wikilink")。首府為[土格加勞市](../Page/土格加劳.md "wikilink")（Tuguegarao
City），本省位於[呂宋島的最東北端](../Page/呂宋島.md "wikilink")。卡加延省還包括了北邊的[巴布延群島](../Page/巴布延群島.md "wikilink")。卡加延省西鄰[北伊羅戈省](../Page/北伊羅戈省.md "wikilink")、[阿巴堯省](../Page/阿巴堯省.md "wikilink")，南面則是[卡林阿省及](../Page/卡林阿省.md "wikilink")[伊莎貝拉省](../Page/伊莎貝拉省.md "wikilink")。卡加延省跟[民答那峨島上的](../Page/民答那峨島.md "wikilink")[卡加延德奧羅市或](../Page/卡加延德奧羅市.md "wikilink")[南沙群岛的建制卡加延市皆沒有關係](../Page/南沙群岛.md "wikilink")。

## 行政區域

卡加延省省劃分為28個自治市及1個城市。

### 城市

  - [土格加勞市](../Page/土格加劳.md "wikilink")（Tuguegarao）- 首府

### 自治市

  - Abulug
  - [阿爾卡拉](../Page/阿爾卡拉_\(卡加延省\).md "wikilink")（Alcala）
  - Allacapan
  - Amulung
  - [阿帕里](../Page/阿帕里.md "wikilink")（Aparri）
  - [拔高](../Page/巴高_\(卡加延省\).md "wikilink")（Baggao）
  - [巴列斯特羅斯](../Page/巴列斯特罗斯_\(卡加延省\).md "wikilink")（Ballesteros）
  - Buguey
  - [加拉鄢](../Page/加拉鄢_\(卡加延省\).md "wikilink")（Calayan）
  - [加馬拉紐甘](../Page/加馬拉紐甘.md "wikilink")（Camalaniugan）
  - [克拉韋里亞](../Page/克拉韋里亞_\(卡加延省\).md "wikilink")（Claveria）
  - Enrile
  - Gattaran
  - [剛薩加](../Page/剛薩加_\(卡加延省\).md "wikilink")（Gonzaga）
  - Iguig
  - Lal-lo
  - Lasam
  - [潘普洛納](../Page/潘普洛纳_\(卡加延省\).md "wikilink")（Pamplona）
  - [佩尼亞布蘭卡](../Page/佩尼亚布兰卡_\(菲律宾\).md "wikilink")（Peñablanca）
  - [畢亞特](../Page/毕亚特.md "wikilink")（Piat）
  - [黎剎](../Page/黎剎_\(卡加延省\).md "wikilink")（Rizal）
  - Sanchez-Mira
  - [聖安娜](../Page/聖安娜_\(卡加延省\).md "wikilink")（Santa Ana）
  - Santa Praxedes
  - Santa Teresita
  - [聖嬰](../Page/圣婴_\(卡加延省\).md "wikilink")（Santo Niño）
  - Solana
  - Tuao

[C](../Category/菲律賓省份.md "wikilink")