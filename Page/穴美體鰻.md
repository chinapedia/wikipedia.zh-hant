**穴美體鰻**，又稱**白錐體康吉鰻**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目](../Page/鰻鱺目.md "wikilink")[糯鰻亞目](../Page/糯鰻亞目.md "wikilink")[糯鰻科的其中一個](../Page/糯鰻科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[印度東岸](../Page/印度.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國沿海](../Page/中國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[索羅門群島等海域](../Page/索羅門群島.md "wikilink")。

## 特徵

本魚胸鰭、尾鰭存在。眼後緣上下方各有一斑點，前[上頷齒在口閉合時露出](../Page/上頷齒.md "wikilink")，口裂不超過眼中線或在胸鰭1/2處上方。鰓裂大，後鼻孔無瓣膜，鰭條不分節。上下頷骨齒與鋤骨齒呈向後漸細的齒帶；前上頷骨成束；鋤骨齒裂達上頷骨列一半處。背鰭起點在鰓裂上方。脊椎骨數149至159。

體長為體高11.9倍，頭長5.6倍；尾長為頭與軀幹1倍；頭長為吻長4.5倍；吻長為眼徑1.3倍。眼大，吻短。舌寬，游離。尾部側扁。體色為褐色。背鰭、臀鰭黑色。

它体长可达60公分。\[1\]

## 生態

棲息在200公尺深以內海域之砂泥底。

## 經濟利用

量多，具漁業經濟之價值，可食用。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[anago](../Category/美體鰻屬.md "wikilink")

1.