**馬島白尾鼠亞科**（***Mystromyinae***），[哺乳綱](../Page/哺乳綱.md "wikilink")、[囓齒目](../Page/囓齒目.md "wikilink")、[馬島鼠科的一亞科](../Page/馬島鼠科.md "wikilink")，而與馬島白尾鼠亞科同科的動物尚有[短足鼠屬](../Page/短足鼠屬.md "wikilink")（短足鼠）、鼢鼠屬（中華鼢鼠）等之數種[哺乳動物](../Page/哺乳動物.md "wikilink")。

## 參考文獻

  - 中國科學院，《中國科學院動物研究所的世界動物名稱資料庫》，[1](https://web.archive.org/web/20050718232917/http://vzd.brim.ac.cn/division/fauna/index.asp)

[Category:馬島白尾鼠亞科](../Category/馬島白尾鼠亞科.md "wikilink")