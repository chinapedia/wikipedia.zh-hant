**第115師**屬於[國民革命軍第十八集團軍](../Page/國民革命軍第十八集團軍.md "wikilink")，1937年8月25日由[紅一方面軍改編成](../Page/紅一方面軍.md "wikilink")，下轄第343旅、第344旅、獨立團、隨營學校，初期師長為[林彪](../Page/林彪.md "wikilink")，副師長兼政委[聶榮臻](../Page/聶榮臻.md "wikilink")，參謀長[周昆](../Page/周昆.md "wikilink")，政訓處主任[羅榮桓](../Page/羅榮桓.md "wikilink")，副主任[蕭華](../Page/蕭華.md "wikilink")，參謀處長[王秉璋](../Page/王秉璋.md "wikilink")。对日著名战例是在1937年9月配合[國民革命軍友军在](../Page/國民革命軍.md "wikilink")[平型關伏击日军辎重队](../Page/平型關.md "wikilink")，即[平型关战斗](../Page/平型关战斗.md "wikilink")。

## 战斗序列

### 第343旅

第343旅旅长[陈光](../Page/陈光.md "wikilink")、副旅长[周建屏](../Page/周建屏.md "wikilink")、参谋长[陈士榘](../Page/陈士榘.md "wikilink")；

  - 第685团团长[杨得志](../Page/杨得志.md "wikilink")、副团长[陈正湘](../Page/陈正湘.md "wikilink")、政训处主任[邓华](../Page/邓华.md "wikilink")、副主任[吴法宪](../Page/吴法宪.md "wikilink")、参谋长[彭明治](../Page/彭明治.md "wikilink")
      - 一营营长[刘震](../Page/刘震.md "wikilink")
      - 二营营长[曾国华](../Page/曾国华.md "wikilink")
      - 三营营长[梁兴初](../Page/梁兴初.md "wikilink")
          - 连长[杨西彬](../Page/杨西彬.md "wikilink")
  - 第686团团长[李天佑](../Page/李天佑.md "wikilink")、副团长[杨勇](../Page/杨勇_\(上将\).md "wikilink")、政训处主任[符竹庭](../Page/符竹庭.md "wikilink")、参谋长[彭雄](../Page/彭雄.md "wikilink")

### 第344旅

第344旅由原[红十五军团班底组成](../Page/红十五军团.md "wikilink")，旅长[徐海东](../Page/徐海东.md "wikilink")、政委[黄克诚](../Page/黄克诚.md "wikilink")、参谋长[陈漫远](../Page/陈漫远.md "wikilink")；

  - 第687团团长[张绍东](../Page/张绍东.md "wikilink")、副团长[田守尧](../Page/田守尧.md "wikilink")、政训处主任[谭甫仁](../Page/谭甫仁.md "wikilink")
  - 第688团团长[陈锦绣](../Page/陈锦绣.md "wikilink")、副团长[韩先楚](../Page/韩先楚.md "wikilink")、政训处主任[刘震](../Page/刘震.md "wikilink")
  - 一连指导员：黄薇

### 独立团

团长[杨成武](../Page/杨成武.md "wikilink")、政训处主任[罗元发](../Page/罗元发.md "wikilink")、参谋长[熊伯涛](../Page/熊伯涛.md "wikilink")

### 第115师随营学校

校长[孙毅](../Page/孙毅.md "wikilink")

## 发展

鉴于华北日军南下进攻徐州，后方空虚，毛泽东即命115师立即开始创立根据地的战略任务。[聂荣臻和](../Page/聂荣臻.md "wikilink")[罗荣桓各领一部兵力分赴华北和山东](../Page/罗荣桓.md "wikilink")，前者发展为八路军除陕甘宁之外最大的一块根据地[晋察冀根据地](../Page/晋察冀根据地.md "wikilink")，后者发展为[山东军区](../Page/山东军区.md "wikilink")。1938年11月，政委罗荣桓与代师长陈光率师机关和第686团组成的东进支队从晋西出发，于1939年3月进入山东，同八路军山东纵队会合。1943年3月，罗荣桓被中央军委任命为115师政委、代师长，山东军区司令员兼政委。1945年9月向东北挺进。

## 参考文献

## 参见

  - [中国工农红军第一方面军](../Page/中国工农红军第一方面军.md "wikilink")
  - [国民革命军第十八集团军](../Page/国民革命军第十八集团军.md "wikilink")
  - [中国人民解放军第三野战军](../Page/中国人民解放军第三野战军.md "wikilink")

{{-}}

[category:中国人民解放军第三野战军](../Page/category:中国人民解放军第三野战军.md "wikilink")

[Category:八路军的师](../Category/八路军的师.md "wikilink")
[八路军第115师](../Category/八路军第115师.md "wikilink")