|                                                                                                                   |
| :---------------------------------------------------------------------------------------------------------------: |
| [Reynaldo_Bignone-2.jpg](https://zh.wikipedia.org/wiki/File:Reynaldo_Bignone-2.jpg "fig:Reynaldo_Bignone-2.jpg") |
|                                                        任期:                                                        |
|                                                        前任:                                                        |
|                                                        继任:                                                        |
|                                                       副总统:                                                        |
|                                                        生日:                                                        |
|                                                       去世日期:                                                       |
|                                                       出生地:                                                        |
|                                                        职业:                                                        |
|                                                        政党:                                                        |

<big><big>**雷纳尔多·比尼奥内**</big></big>

**雷纳尔多·贝尼托·安东尼奥·比尼奥内·拉马永**（，），[阿根廷总统](../Page/阿根廷.md "wikilink")（1982年－1983年）。

比尼奥内是阿根廷軍政府獨裁時期的最後一任總統，1983年被迫同意舉行總統選舉，結束多年的軍政府獨裁統治。

1999年被指控在[骯髒戰爭中竊取失蹤人犯的兒女](../Page/骯髒戰爭.md "wikilink")，被關七年，一直到2005年才出獄，2007年3月又被通緝，指控他在獨裁時期非法軍事逮捕人犯，在Campo
de Mayo黑牢中侵犯基本人權及謀殺，2010年被判入獄25年。

</center>

[Category:軍人出身的總統](../Category/軍人出身的總統.md "wikilink")
[Category:義大利裔阿根廷人](../Category/義大利裔阿根廷人.md "wikilink")
[Category:阿根廷将军](../Category/阿根廷将军.md "wikilink")
[Category:阿根廷總統](../Category/阿根廷總統.md "wikilink")
[Category:阿根廷罪犯](../Category/阿根廷罪犯.md "wikilink")