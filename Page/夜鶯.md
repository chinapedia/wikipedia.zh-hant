[Luscinia_megarhynchos_MHNT_232_HdB_Bouzareah_Algérie.jpg](https://zh.wikipedia.org/wiki/File:Luscinia_megarhynchos_MHNT_232_HdB_Bouzareah_Algérie.jpg "fig:Luscinia_megarhynchos_MHNT_232_HdB_Bouzareah_Algérie.jpg")
**夜鶯**（[学名](../Page/学名.md "wikilink")：，又名**新疆歌鸲**，俗名**夜歌鸲**），為[雀形目](../Page/雀形目.md "wikilink")[歌鸲属的一種](../Page/歌鸲属.md "wikilink")[鸟类](../Page/鸟类.md "wikilink")。與其他鳥類不同，夜鶯是少有的在夜間鳴唱的鳥類，故得其名。

夜鶯體色灰褐，羽色并不絢麗，但鳴唱非常出眾，音域極廣，是玩賞鳥的種類之一。

## 分布

夜鶯分布于遍布欧洲、东抵阿富汗、南至地中海、小亚细亚、非洲西北部、東至非洲热带地区、台灣屏東縣麟洛鄉以及[中国](../Page/中国.md "wikilink")[新疆等地](../Page/新疆.md "wikilink")，多见于河谷、河漫滩稀疏的落叶林和混交林、灌木丛或园圃间以及常隐匿于矮灌丛或树木的低枝间。该物种的模式产地在德国。\[1\]

## 亚种

  - **新疆歌鸲新疆亚种**（[学名](../Page/学名.md "wikilink")：）。分布于帕米尔、阿富汗等中央亚细亚一带、冬初经伊拉克、阿拉伯而抵非洲东北部以及[中国大陆的](../Page/中国大陆.md "wikilink")[新疆等地](../Page/新疆.md "wikilink")。\[2\]

## 参考文献

[新疆歌鸲](../Category/歌鸲属.md "wikilink")
[Category:鹟科](../Category/鹟科.md "wikilink")

1.
2.