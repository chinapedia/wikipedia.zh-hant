**萧县**古称[萧国](../Page/萧国.md "wikilink")，位于[中国](../Page/中国.md "wikilink")[安徽省北端](../Page/安徽省.md "wikilink")、[苏](../Page/江蘇省.md "wikilink")[鲁](../Page/山東省.md "wikilink")[豫](../Page/河南省.md "wikilink")[皖四](../Page/皖.md "wikilink")[省交界处](../Page/省.md "wikilink")，现为[宿州市下辖的一个](../Page/宿州市.md "wikilink")[县](../Page/县.md "wikilink")。县政府所在地为龙城镇，所以萧县又称为龙城。萧县为“书画之乡”。邮编：235200
代码：341322 区号：0557，总面积1885.3平方千米。 常住人口119.7万，户籍人口1,418,936人。

## 历史

萧县古稱**扶陽**，[春秋時曾分封為](../Page/春秋.md "wikilink")[蕭國](../Page/蕭國.md "wikilink")，[明代隶属](../Page/明代.md "wikilink")[南直隶徐州直隶州](../Page/南直隶.md "wikilink")，[清代隶属](../Page/清代.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[徐州府](../Page/徐州府.md "wikilink")。1950年代由江苏省划归安徽省。自古为“兵家必争之地”，如[楚漢戰爭](../Page/楚漢戰爭.md "wikilink")，[淮海战役](../Page/淮海战役.md "wikilink")。历史名人有[徐樹錚](../Page/徐樹錚.md "wikilink")、[国民党上将](../Page/国民党.md "wikilink")[王仲廉](../Page/王仲廉.md "wikilink")、[方先觉](../Page/方先觉.md "wikilink")，[解放军上将](../Page/解放军.md "wikilink")[王克等](../Page/王克.md "wikilink")。

萧县字画盛行，有“书画之乡”之称，有[刘开渠](../Page/刘开渠.md "wikilink")（现为淮北市杜集区所辖）、[李可染](../Page/李可染.md "wikilink")（现为徐州所辖）、[萧龙士](../Page/萧龙士.md "wikilink")、[朱德群等著名](../Page/朱德群.md "wikilink")[画家](../Page/画家.md "wikilink")。萧县[葡萄酒厂所产葡萄酒较有名](../Page/葡萄酒.md "wikilink")，现已并入安徽古井集团。[皇藏峪国家森林公园位于萧县东南](../Page/皇藏峪国家森林公园.md "wikilink")20余公里的[龙岗山中](../Page/龙岗山.md "wikilink")，为“[汉高祖避难处](../Page/汉高祖.md "wikilink")”。

## 行政区划

全县辖18个镇、5个乡：龙城镇、黄口镇、杨楼镇、阎集镇、新庄镇、刘套镇、马井镇、大屯镇、赵庄镇、杜楼镇、丁里镇、王寨镇、祖楼镇、青龙集镇、张庄寨镇、永堌镇、白土镇、官桥镇、圣泉乡、酒店乡、孙圩孜乡、庄里乡、石林乡，共有708个行政村。县政府驻龙城镇。

此外，衛斯理小說《[木炭](../Page/木炭_\(小說\).md "wikilink")》提及本縣有一處名為「貓爪坳」的地方，實屬虛構。

## 名优特产

全縣年种植[小麦](../Page/小麦.md "wikilink")100万亩，总产值3.7
5亿公斤。[玉米](../Page/玉米.md "wikilink")50万亩，总产值2亿公斤。[棉花](../Page/棉花.md "wikilink")25万亩，总产值1750万公斤。无公害瓜菜30万亩，[花生](../Page/花生.md "wikilink")、[大豆](../Page/大豆.md "wikilink")、[芝麻](../Page/芝麻.md "wikilink")、[山芋等](../Page/山芋.md "wikilink")50万亩。

在[水果方面](../Page/水果.md "wikilink")，全县现有水果面积50万亩，其中[梨](../Page/梨.md "wikilink")20万亩、[苹果](../Page/苹果.md "wikilink")10万亩、[葡萄](../Page/葡萄.md "wikilink")8万亩、[桃](../Page/桃.md "wikilink")7万亩、干杂果5万亩，其中结果面积34万亩，水果总产量达6亿公斤左右。

在[畜牧方面](../Page/畜牧.md "wikilink")，全县常年存栏[牛](../Page/牛.md "wikilink")17万头、生[猪](../Page/猪.md "wikilink")116万头、[羊](../Page/羊.md "wikilink")100万只、[禽](../Page/禽.md "wikilink")1500万只。在[水产方面](../Page/水产.md "wikilink")，全縣水面积11.5万亩，可养水面积3万亩，已养水面积2.4万亩，精养[鱼塘](../Page/鱼.md "wikilink")5000亩，主要引进[鲤](../Page/鲤.md "wikilink")、湘云[鲫](../Page/鲫.md "wikilink")、彭泽鲫、[团头鲂](../Page/团头鲂.md "wikilink")、[美国叉尾{{僻字等品种](../Page/钳鱼.md "wikilink")，年水产品产量达5000吨左右。

在农副产品方面，全縣有板材加工企业300多家，年产值近亿元。年产葡萄汁5000吨。棉花加工、[面粉加工](../Page/面粉.md "wikilink")、[饲料加工](../Page/饲料.md "wikilink")、[奶制品加工已形成规模](../Page/奶制品.md "wikilink")。

在林木方面，全县林木覆被率现为31.8%，林地面积89.4万亩，其中用材林面积36.2万亩，经济林面积35.2万亩，其它林种面积18万亩。全县林木总蓄积量230万立方米，活立木年生长量20万立方米，年可采伐量10万立方米，年产用材林枝丫量10万吨，经济林枝丫量15万吨。

蕭縣[矿产资源丰富](../Page/矿产.md "wikilink")，[水电供应充足](../Page/水电.md "wikilink")。萧县为全国100个重点产[煤县之一](../Page/煤.md "wikilink")，已探明煤炭储量
7.5亿吨，现有17对矿井，年产能力120万吨；探明[石油储量](../Page/石油.md "wikilink")
7亿吨，煤层气储量1000亿立方米，[石灰岩储量](../Page/石灰岩.md "wikilink")30亿吨；[铁矿石](../Page/铁矿.md "wikilink")、瓷石、[高岭土等储量可观](../Page/高岭土.md "wikilink")。萧县南北分属[淮](../Page/淮.md "wikilink")、[黄河水系](../Page/黄河.md "wikilink")，[地下水量为](../Page/地下水.md "wikilink")2.6亿立方米，水质优良，生产、生活用水充足。电力供应属华东电网，设施完善，境内有11万伏以上变电所2个，生产生活供电充足。

## 名胜古迹

  - [萧县文庙](../Page/萧县文庙.md "wikilink")
  - 圣泉寺(萧县)
  - 皇藏峪国家森林公园

## 外部链接

  - [萧县：乡间自有大书家](http://www.people.com.cn/GB/paper40/11502/1037426.html)
  - <http://www.ahxx.gov.cn>
  - [龙城网](http://www.axiaoxian.com)

[宿州](../Page/category:安徽县份.md "wikilink")

[萧县](../Category/萧县.md "wikilink") [县](../Category/宿州区县.md "wikilink")