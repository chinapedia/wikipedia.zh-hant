**鲁尔河畔米尔海姆**（Mülheim an der
Ruhr）是[德国](../Page/德国.md "wikilink")[北莱茵-威斯特法伦州的州直辖市](../Page/北莱茵-威斯特法伦州.md "wikilink")，坐落在[鲁尔河畔](../Page/鲁尔河.md "wikilink")，[鲁尔区城市群](../Page/鲁尔区.md "wikilink")（[杜塞尔多夫](../Page/杜塞尔多夫.md "wikilink")、[埃森和](../Page/埃森.md "wikilink")[杜伊斯堡](../Page/杜伊斯堡.md "wikilink")）的中心，连接着杜塞尔多夫和鲁尔区。1808年米尔海姆建市，1个世纪后人口突破10万，成为一座大型城市。

## 歷史

最早關於米尔海姆的記載是1093年，時為[中世紀](../Page/中世紀.md "wikilink")。後來成為了[貝爾格大公國一部分](../Page/貝爾格大公國.md "wikilink")。1808年米尔海姆建市；在[維也納會議後](../Page/維也納會議.md "wikilink")，此城是[普魯士一部分](../Page/普魯士.md "wikilink")。

此城於1780年左右，已經隨著[魯爾工業區發展而工業化](../Page/魯爾工業區.md "wikilink")。米尔海姆第一家工廠建於1791年，是一家紡紗廠。而第一家於米尔海姆的鋼鐵廠，是建於1849年；與此同時米尔海姆亦興建了大大小小的煤炭廠。

在[二次大戰期間](../Page/二次大戰.md "wikilink")，米尔海姆是[英國皇家空軍的空襲目標](../Page/英國皇家空軍.md "wikilink")，其中最猛烈的攻勢是1943年6月22-23日，一共有逾五百枚炸彈轟向此城。

隨著煤炭需求減少，自1960年代起，米尔海姆遭遇了嚴重的經濟困難，鋼鐵廠、煤炭廠大量倒閉。市政府隨後於1970年代起，展開重建工程，逐漸轉型成科研和物流中心。

## 出身人物

  - [拉爾斯·布斯穆勒](../Page/拉爾斯·布斯穆勒.md "wikilink")：網球員
  - [汉内洛蕾·克拉夫特](../Page/汉内洛蕾·克拉夫特.md "wikilink")：北莱茵-威斯特法伦州州长

## 友好城市

  - Darlington （1953年）

  - [图尔](../Page/图尔.md "wikilink") （1962年）

  - [奥波莱](../Page/奥波莱.md "wikilink") （1989年）

  - [卡法薩巴](../Page/卡法薩巴.md "wikilink") （1993年）

  - Beykoz （2007年）

  - [科沃拉](../Page/科沃拉.md "wikilink") （2009年）

## 圖片

<File:RheinRuhrHafen> MülheimanderRuhr02.jpg|內河港口
[File:MH-Schlossstrasse.jpg|街頭一景](File:MH-Schlossstrasse.jpg%7C街頭一景)
<File:Mülheim> adR - Rheinstraße - Siemens AG Power Generation 03
ies.jpg|西門子在米爾海姆的分公司 <File:Im> Witthausbusch .JPG|市內一處公園

[M](../Category/北莱茵-威斯特法伦州市镇.md "wikilink")