**i.n.g**是[台灣的新晉三人少女歌手組合](../Page/台灣.md "wikilink")，在2006年4月成立，隸屬於[台灣](../Page/台灣.md "wikilink")[艾迴音樂公司與](../Page/艾迴音樂.md "wikilink")[哇哈哈製作有限公司經紀部](../Page/哇哈哈製作有限公司.md "wikilink")。她們初出道就已立即得到[台灣職棒四大球隊之一的](../Page/台灣職棒.md "wikilink")[兄弟象隊採用她們的主打歌](../Page/兄弟象.md "wikilink")「健健美」成為球隊的2006年度的加油主題曲。\[1\]

## 名稱的意思

**i.n.g**這個名稱有兩重意思：

1.  其一，是三位成員Ida、Nara和Gillian的英文名縮寫；
2.  其二，根據艾迴唱片的官方說法，「是因為這三位活力團員總是動個不停、跳個不停，總是保持在現正進行『說』或『跳』或『唱』的狀態中」\[2\]，所以取了英語對進行中的動作後綴「-ing」作為名稱的取材。

## 作品列表

  - 2006年：《Lucky Star幸運星》（6月16日發售）

## 參考

<references />

[Category:臺灣女子演唱團體](../Category/臺灣女子演唱團體.md "wikilink")
[Category:華語流行音樂團體](../Category/華語流行音樂團體.md "wikilink")
[Category:已解散的女子演唱團體](../Category/已解散的女子演唱團體.md "wikilink")
[Category:2006年成立的音樂團體](../Category/2006年成立的音樂團體.md "wikilink")

1.  [i.n.g 健健美
    幫兄弟象加油](http://ndap.dils.tku.edu.tw:8080/ndap/querynews2.jsp?id=218536)
    民生報
2.  [女生團體i.n.g](http://ent.sina.com.tw/music/star/starinfo.php?singerId=2469&type=other)
    ，台灣新浪網