**昌達**（615年十二月－619年閏二月）：[隋朝末期](../Page/隋朝.md "wikilink")[唐朝初期](../Page/唐朝.md "wikilink")[楚政權領袖](../Page/楚.md "wikilink")[朱粲的](../Page/朱粲.md "wikilink")[年号](../Page/年号.md "wikilink")，歷時3年餘。

《[旧唐书](../Page/旧唐书.md "wikilink")》和《[资治通鉴](../Page/资治通鉴.md "wikilink")》则记载618年建元。

## 大事记

## 出生

## 逝世

## 纪年

| 昌達                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 615年                           | 616年                           | 617年                           | 618年                           | 619年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [大业](../Page/大业.md "wikilink")（605年正月－618年三月）：[隋朝政权隋煬帝](../Page/隋朝.md "wikilink")[杨广年号](../Page/杨广.md "wikilink")
      - [义宁](../Page/义宁.md "wikilink")（617年十一月－618年五月）：[隋朝政权隋恭帝](../Page/隋朝.md "wikilink")[杨侑年号](../Page/杨侑.md "wikilink")
      - [皇泰](../Page/皇泰.md "wikilink")（618年五月－619年四月）：[隋朝政权隋煬帝](../Page/隋朝.md "wikilink")[杨侗年号](../Page/杨侗.md "wikilink")
      - [始兴](../Page/始兴_\(操师乞\).md "wikilink")（616年十二月）：[隋朝時期](../Page/隋朝.md "wikilink")[操师乞年号](../Page/操师乞.md "wikilink")
      - [太平](../Page/太平_\(林士弘\).md "wikilink")（616年十二月－622年十月）：[隋朝時期](../Page/隋朝.md "wikilink")[楚政權](../Page/楚.md "wikilink")[林士弘年号](../Page/林士弘.md "wikilink")
      - [丁丑](../Page/丁丑_\(窦建德\).md "wikilink")（617年正月－618年十一月）：[隋朝時期](../Page/隋朝.md "wikilink")[夏政權](../Page/夏.md "wikilink")[窦建德年号](../Page/窦建德.md "wikilink")
      - [五凤](../Page/五凤_\(窦建德\).md "wikilink")（618年十一月－621年五月）：[隋朝時期](../Page/隋朝.md "wikilink")[夏政權](../Page/夏.md "wikilink")[窦建德年号](../Page/窦建德.md "wikilink")
      - [永平](../Page/永平_\(李密\).md "wikilink")（617年二月－618年十二月）：[隋朝時期](../Page/隋朝.md "wikilink")[魏政權](../Page/魏.md "wikilink")[李密年号](../Page/李密.md "wikilink")
      - [天兴](../Page/天兴_\(刘武周\).md "wikilink")（617年三月－620年四月）：[隋朝時期](../Page/隋朝.md "wikilink")[刘武周年号](../Page/刘武周.md "wikilink")
      - [永隆](../Page/永隆_\(梁師都\).md "wikilink")（618年－628年四月）：[隋朝時期](../Page/隋朝.md "wikilink")[梁政權](../Page/梁.md "wikilink")[梁師都年号](../Page/梁師都.md "wikilink")
      - [正平](../Page/正平_\(郭子和\).md "wikilink")（617年三月－618年七月）：[隋朝時期](../Page/隋朝.md "wikilink")[永樂王](../Page/永樂.md "wikilink")[郭子和年号](../Page/郭子和.md "wikilink")
      - [秦兴](../Page/秦兴.md "wikilink")（617年四月－618年十一月）：[隋朝時期](../Page/隋朝.md "wikilink")[秦政權](../Page/秦.md "wikilink")[薛擧年号](../Page/薛擧.md "wikilink")
      - [鸣凤](../Page/鸣凤.md "wikilink")（617年四月－618年十一月）：[隋朝時期](../Page/隋朝.md "wikilink")[梁政權](../Page/梁.md "wikilink")[蕭銑年号](../Page/蕭銑.md "wikilink")
      - [通聖](../Page/通聖.md "wikilink")（617年十二月）：[隋朝時期](../Page/隋朝.md "wikilink")[曹武徹年号](../Page/曹武徹.md "wikilink")
      - [天壽](../Page/天壽.md "wikilink")（618年九月－619年二月）：[隋朝時期](../Page/隋朝.md "wikilink")[許政權](../Page/許.md "wikilink")[宇文化及年号](../Page/宇文化及.md "wikilink")
      - [安乐](../Page/安乐.md "wikilink")（617年七月－619年五月）：[隋朝時期](../Page/隋朝.md "wikilink")[涼政權](../Page/涼.md "wikilink")[李軌年号](../Page/李軌.md "wikilink")
      - [始兴](../Page/始兴_\(高开道\).md "wikilink")（618年十二月－624年二月）：[隋朝時期](../Page/隋朝.md "wikilink")[燕政權](../Page/燕.md "wikilink")[高开道年号](../Page/高开道.md "wikilink")
      - [法輪](../Page/法輪_\(高曇晟\).md "wikilink")（618年十二月）：[隋朝時期](../Page/隋朝.md "wikilink")[齊政權](../Page/齊.md "wikilink")[高曇晟年号](../Page/高曇晟.md "wikilink")
      - [武德](../Page/武德.md "wikilink")（618年五月－626年十二月）：[唐朝政权唐高祖](../Page/唐朝.md "wikilink")[李淵年号](../Page/李淵.md "wikilink")
      - [义和](../Page/义和_\(高昌\).md "wikilink")（614年－619年）：[高昌政权年号](../Page/高昌.md "wikilink")
      - [建福](../Page/建福_\(新羅真平王\).md "wikilink")（584年－634年）：[新羅](../Page/新羅.md "wikilink")[真平王之年號](../Page/新羅真平王.md "wikilink")

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:隋末民变政权年号](../Category/隋末民变政权年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:610年代中国政治](../Category/610年代中国政治.md "wikilink")