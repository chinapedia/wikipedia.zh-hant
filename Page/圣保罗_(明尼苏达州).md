**圣保罗** （**Saint Paul,
Minnesota**）是[美國](../Page/美國.md "wikilink")[明尼蘇達州州府](../Page/明尼蘇達州.md "wikilink")、第二大城市，是明尼苏达州面积最小、人口最密集的[拉姆西縣首府](../Page/拉姆西縣_\(明尼蘇達州\).md "wikilink")。\[1\]根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，人口為287,151
人，2005年人口下降至275,150 人。面積145.5
平方公里。西、南隔[密西西比河與](../Page/密西西比河.md "wikilink")[明尼阿波利斯相望](../Page/明尼阿波利斯.md "wikilink")，組成著名的[雙城](../Page/雙城.md "wikilink")
（Twin
Cities），是美国第16大都会区[明尼阿波利斯-圣保罗都会区的核心](../Page/明尼阿波利斯-圣保罗都会区.md "wikilink")，拥有约352万居民。\[2\]

## 历史

现在印第安丘陵公园的埋葬地区表明，这个地区最初是由两千年前的[霍普韦尔美洲原住民所居住的](../Page/霍普韦尔文化.md "wikilink")。\[3\]\[4\]
从17世纪初到1837年，苏族的Mdewakanton
Dakota部落在逃离祖先的[米拉湖之家后](../Page/米拉湖.md "wikilink")，靠近丘陵，成为了[欧及布威族](../Page/欧及布威族.md "wikilink")。\[5\]\[6\]
他们因暴露的白砂岩悬崖称该地区为I-mni-za ska dan（“小白石”）。\[7\]\[8\]

## 地理

由于[密西西比河和](../Page/密西西比河.md "wikilink")[明尼苏达河在这里交汇](../Page/明尼苏达河.md "wikilink")，圣保罗历史上一直是一座港口城市。

### 气候

圣保罗属典型的美国上中西部地区[大陆气候](../Page/大陆气候.md "wikilink")，按[柯本气候分类法为](../Page/柯本气候分类法.md "wikilink")[湿润性大陆气候](../Page/湿润性大陆气候.md "wikilink")（Dfa）。冬季严寒、大雪，夏季炎热、湿润。可以见到雷暴、龙卷风、冻雨、大雾等各类天气情况。\[9\]

## 人口

| 人口划分                                      | 2010\[10\] | 2000\[11\] | 1990\[12\] | 1970\[13\]  |
| ----------------------------------------- | ---------- | ---------- | ---------- | ----------- |
| [美国白人](../Page/美国白人.md "wikilink")        | 60.1%      | 67.0%      | 82.3%      | 95.4%       |
| —[非西班牙裔白人](../Page/非西班牙裔白人.md "wikilink") | 55.9%      | 64.0%      | 80.4%      | 93.6%\[14\] |
| [非洲裔美国人](../Page/非洲裔美国人.md "wikilink")    | 15.7%      | 11.7%      | 7.4%       | 3.5%        |
| [西班牙裔美国人](../Page/西班牙裔美国人.md "wikilink")  | 9.6%       | 7.9%       | 4.2%       | 2.1%\[15\]  |
| [亚洲裔美国人](../Page/亚洲裔美国人.md "wikilink")    | 15.0%      | 12.4%      | 7.1%       | 0.2%        |

## 教育

[1930s_HU_STUDENTS.jpg](https://zh.wikipedia.org/wiki/File:1930s_HU_STUDENTS.jpg "fig:1930s_HU_STUDENTS.jpg")参加期末考试的学生\]\]

圣保罗的人均高等教育机构数高居全美第二\[16\]，当地有三所公立、八所私立高等教育机构。这包括[圣凯瑟琳大学](../Page/圣凯瑟琳大学.md "wikilink")、[协和大学圣保罗分校](../Page/协和大学圣保罗分校.md "wikilink")、[哈姆莱大学](../Page/哈姆莱大学.md "wikilink")、[玛卡莱斯特学院](../Page/玛卡莱斯特学院.md "wikilink")、[圣托马斯大学](../Page/U圣托马斯大学_\(明尼苏达州\).md "wikilink")、[大都会州立大学](../Page/大都会州立大学.md "wikilink")、[圣保罗学院和](../Page/圣保罗学院.md "wikilink")[米切尔·哈姆莱法学院](../Page/米切尔·哈姆莱法学院.md "wikilink")。\[17\]

[圣保罗公立学校共包含](../Page/圣保罗公立学校.md "wikilink")82所学校，包括52所小学、12所初中、7所高中、10所非传统学校和1所特殊教育学校，有约四万名学生\[18\]\[19\]。另外还有私立学校38座、[特许学校](../Page/特许学校.md "wikilink")21座，美国第一所特许学校就是1992年在这里建立的。\[20\]\[21\]

## 姐妹城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/長崎.md" title="wikilink">長崎</a> （1955）</p></li>
<li><p><a href="../Page/哈代拉.md" title="wikilink">哈代拉</a> （1981）</p></li>
<li><p><a href="../Page/庫利亞坎.md" title="wikilink">庫利亞坎</a> （1983）</p></li>
<li><p><a href="../Page/長沙.md" title="wikilink">長沙</a> （1987）</p></li>
<li><p><a href="../Page/乔治_(西开普省).md" title="wikilink">Lawaaikamp</a> （1988）</p></li>
<li><p><a href="../Page/摩德納.md" title="wikilink">摩德納</a> （1989）</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/新西伯利亞.md" title="wikilink">新西伯利亞</a> （1989）</p></li>
<li><p><a href="../Page/羅梅羅城.md" title="wikilink">羅梅羅城</a> （1991）</p></li>
<li><p><a href="../Page/提比里亞.md" title="wikilink">提比里亞</a> （1996）</p></li>
<li><p><a href="../Page/諾伊斯.md" title="wikilink">諾伊斯</a> （1999）</p></li>
<li><p><a href="../Page/曼薩尼約.md" title="wikilink">曼薩尼約</a> （2002）</p></li>
</ul></td>
</tr>
</tbody>
</table>

[Minnesota_State_Capitol.jpg](https://zh.wikipedia.org/wiki/File:Minnesota_State_Capitol.jpg "fig:Minnesota_State_Capitol.jpg")

## 参考资料

[Category:明尼蘇達州城市](../Category/明尼蘇達州城市.md "wikilink")
[S](../Category/美国各州首府.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.

9.

10.

11.

12.
13.
14. From 15% sample

15.
16.

17.

18.

19.

20.

21.  and