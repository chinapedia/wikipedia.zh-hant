**碳酸钾**（化学式：[K](../Page/钾.md "wikilink")<sub>2</sub>[C](../Page/碳.md "wikilink")[O](../Page/氧.md "wikilink")<sub>3</sub>，
英文名：Potassium
carbonate），呈无色结晶或白色颗粒，能吸湿，可溶于[水](../Page/水.md "wikilink")。其溶液呈[碱性](../Page/碱.md "wikilink")。不溶于[醇和](../Page/醇.md "wikilink")[醚](../Page/醚.md "wikilink")。可由[氢氧化钾与](../Page/氢氧化钾.md "wikilink")[二氧化碳反应得到](../Page/二氧化碳.md "wikilink")。

常用肥料[草木灰中有碳酸钾](../Page/草木灰.md "wikilink")。

[Category:碳酸盐](../Category/碳酸盐.md "wikilink")
[Category:钾化合物](../Category/钾化合物.md "wikilink")
[Category:pH调节剂](../Category/pH调节剂.md "wikilink")
[Category:化學肥料](../Category/化學肥料.md "wikilink")