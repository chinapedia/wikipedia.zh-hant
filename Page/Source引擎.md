**Source引擎**（**次世代引擎**、**起源引擎**）是一个真[三维的](../Page/三维.md "wikilink")[游戏引擎](../Page/游戏引擎.md "wikilink")，由[Valve软件公司为了](../Page/維爾福.md "wikilink")[第一人称射击游戏](../Page/第一人称射击.md "wikilink")《[半条命2](../Page/半条命2.md "wikilink")》而开发，并且对其他的游戏开发者开放授权。这个引擎提供算繪、音效、动画、[抗锯齿](../Page/抗锯齿.md "wikilink")、界面、网络、美工创意和物理模拟方面的支持。

就像以往一样，玩家如果想要享受新的引擎带来的绚丽效果，就要有一块当下比较流行的[显卡进行支持](../Page/显卡.md "wikilink")。Source引擎的互动性和响应性让《半条命2》比前一代更加的具有游戏性，但是Source引擎的真正特性往往不会被人提及。

使用这个引擎制作的第一个游戏是《半条命2》。Valve在《半条命2》发行不久后也用它制作了《[半条命](../Page/半条命_\(游戏\).md "wikilink")》和《[反恐精英](../Page/反恐精英.md "wikilink")》的效果更好的版本。另外一些使用它的游戏，比如《[戰慄時空2：死鬥模式](../Page/戰慄時空2：死鬥模式.md "wikilink")》與《[胜利之日：起源](../Page/胜利之日：起源.md "wikilink")》。

2005年初Troika游戏公司宣布他们的游戏《[吸血鬼之避世–血族](../Page/吸血鬼之避世–血族.md "wikilink")》使用Source引擎，它们也是第一个获得使用许可的公司。Valve之后宣布[Arkane
Studios](../Page/Arkane_Studios.md "wikilink")（制作第一人称[角色扮演游戏](../Page/電子角色扮演遊戲.md "wikilink")《[魔法門之黑暗彌賽亞](../Page/魔法門之黑暗彌賽亞.md "wikilink")》的公司）和Smiling
Gator Productions也获得了使用许可。

[Ritual
Entertainment在](../Page/Ritual_Entertainment.md "wikilink")2005年7月4日宣布他们的新游戏获得了使用Source引擎和[Steam系统的许可](../Page/Steam.md "wikilink")。

## 引擎技术

[Nuclear_Dawn_-_Silo_Environment_01.png](https://zh.wikipedia.org/wiki/File:Nuclear_Dawn_-_Silo_Environment_01.png "fig:Nuclear_Dawn_-_Silo_Environment_01.png")\]\]
[Zeno_Clash_-_Gate.jpg](https://zh.wikipedia.org/wiki/File:Zeno_Clash_-_Gate.jpg "fig:Zeno_Clash_-_Gate.jpg")\]\]
Source引擎针对[半条命系列第一个版本的修改所产生的飞跃是显而易见的](../Page/戰慄時空_\(系列\).md "wikilink")，物理系统真实性和渲染效果均有大幅度提高。现在的引擎渲染是基于阴影的，且允许通过控制图像来产生大量悦目的效果。Source引擎在Windows系統係下使用[DirectX驱动](../Page/DirectX.md "wikilink")、在Linux和Mac
OS X系統下使用OpenGL驅動。

物理系统方面，Source引擎是基于[Havok引擎的](../Page/Havok引擎.md "wikilink")，但是进行大量的几乎重写性质的改写，可以让玩家在单人抑或是联网游戏中体验到额外的交互感觉。人物的死亡可以用称为[布娃娃物理系统的部分控制](../Page/布娃娃物理系统.md "wikilink")，引擎可以模拟物体在真实世界中的交互作用而不会占用大量资源空间。Source引擎中还加入了车辆元素，多人游戏中也有出现。

Source引擎的另外一个特性就是三维的“地图盒子”，可以让地图外的空间展示为类似于3D效果的画面，而不是以前呆板的平面贴图。这样一来，地图的纵深感觉就更好，可以让远处的景物展示在玩家面前，而不用进行渲染。

Source引擎可以让游戏中的人物模拟情感和表达。每个人物的语言系统是独立的，在编码文件的帮助下，和他们的交流就像真实世界中一样。Valve在每个人物的脸部上面添加了42块“数码肌肉”来实现这一功能。嘴唇的翕动也是一大特性，因为根据所说话语的不同，嘴的形状也是不同的。

Source引擎尽力让一切都模拟真实世界的情况：动画贴图可以和其他动画贴图结合产生新的效果，而一个被称为[逆运动学](../Page/逆运动学.md "wikilink")（Inverse
Kinematics）的功能能让人物在不同情况下四肢的运动都是不同的。

## 模块性

Source引擎的中心思想就是模块性。Source引擎针对第一代引擎进行了无数改进，由于[Steam系统的存在](../Page/Steam.md "wikilink")，引擎的每一个小修改玩家都能轻松的得到。当引擎升级到可以支持新的硬件的时候，玩家就能立刻享受到更好的效果。这些核心的特性能保证Source引擎的生命力在几年之内都很旺盛。

## Valve开发者社群

2005年6月28日，Valve启动了[Valve开发者社群](http://developer.valvesoftware.com/)的[维基版本](../Page/维基.md "wikilink")。这个社群现在提供了[Source引擎的SDK文档](http://www.valve-erc.com/srcsdk/index_obsolete.html)。这个完全由维基技术建立起来的社群提供了自由的交流空间。在开放不久后，Valve宣布“社群的文章……数量几乎翻倍了”。文章的范围从絕對武力：次世代的机器人设计到戰慄時空2的人工智能，再到Source引擎的控制，无所不包。

## Source 2

早在2011年5月，Valve其中一项重要计划就是为Source引擎开发新的内容制作工具来取代现存的老旧工具，使得内容创建更有效率。\[1\]\[2\]

Valve在2015年3月举办的[游戏开发者大会上公布了Source](../Page/游戏开发者大会.md "wikilink")
2引擎，同时表示会提供[Vulkan支持](../Page/Vulkan_\(API\).md "wikilink")，并将对开发者免费开放。\[3\]\[4\]此外，Valve还证实他们将会使用自主研发的[物理引擎Rubikon](../Page/物理引擎.md "wikilink")。\[5\]2015年6月17日，[Dota
2发布了名为](../Page/Dota_2.md "wikilink")“Reborn”的Beta更新，成为首个使用Source
2引擎的游戏。\[6\]同年9月，原Source引擎客户端停止使用，Dota 2正式采用Source 2引擎。\[7\]

## 使用Source引擎的游戏

### Valve开发

  - [异形丛生](../Page/异形丛生.md "wikilink") （Alien Swarm）
  - [反恐精英：起源](../Page/反恐精英：起源.md "wikilink") （Counter-Strike: Source）
  - [反恐精英：全球攻势](../Page/反恐精英：全球攻势.md "wikilink") （Counter-Strike: Global
    Offensive）
  - [決勝之日：次世代](../Page/決勝之日：次世代.md "wikilink") （Day of Defeat: Source）
  - [半条命：起源](../Page/半条命：起源.md "wikilink") （Half Life: Source）
  - [戰慄時空死鬥：次世代](../Page/戰慄時空死鬥：次世代.md "wikilink") （Half-Life
    Deathmatch: Source）
  - [半条命2](../Page/半条命2.md "wikilink") （Half-Life 2）
  - [半条命2：消失的海岸线](../Page/半条命2：消失的海岸线.md "wikilink") （Half-Life 2: Lost
    Coast）
  - [戰慄時空2：死鬥](../Page/戰慄時空2：死鬥.md "wikilink") （Half-Life 2: Deathmatch）
  - [戰慄時空2：首部曲](../Page/戰慄時空2首部曲：浩劫重生.md "wikilink") （Half-Life 2:
    Episode One）
  - [戰慄時空2：二部曲](../Page/戰慄時空2：二部曲.md "wikilink") （Half-Life 2: Episode
    Two）
  - \-{zh-hans:[传送门](../Page/传送门.md "wikilink");
    zh-hant:[傳送門](../Page/傳送門.md "wikilink")}- （Portal）
  - \-{zh-hans:[传送门2](../Page/传送门2.md "wikilink");
    zh-hant:[傳送門2](../Page/傳送門2.md "wikilink")}- （Portal 2）
  - [絕地要塞2](../Page/絕地要塞2.md "wikilink") （Team Fortress 2）
  - [惡靈勢力](../Page/惡靈勢力.md "wikilink") （Left 4 Dead）
  - [惡靈勢力2](../Page/惡靈勢力2.md "wikilink") （Left 4 Dead 2）
  - [Dota 2](../Page/Dota_2.md "wikilink")

### 其他开发者游戏

  - [吸血鬼之避世–血族](../Page/吸血鬼之避世–血族.md "wikilink")(2004)
  - [盖瑞模组](../Page/盖瑞模组.md "wikilink")(2004)
  - [原罪前传：浮现](../Page/原罪前传：浮现.md "wikilink")(2006)
  - [魔法门之黑暗弥赛亚](../Page/魔法门之黑暗弥赛亚.md "wikilink")(2006)
  - [致命衝突 Online](../Page/致命衝突_Online.md "wikilink")(2009)
  - [瑪奇英雄傳](../Page/瑪奇英雄傳.md "wikilink")(2010)
  - [噩夢屋2](../Page/噩夢屋2.md "wikilink")(2010)
  - [核子黎明](../Page/核子黎明.md "wikilink")(2011)
  - [黑色高地](../Page/黑色高地.md "wikilink")(2012)
  - [絕對武力2 Online](../Page/絕對武力2_Online.md "wikilink")(2013)
  - [史丹利的寓言](../Page/史丹利的寓言.md "wikilink")(2013)
  - [絕望深淵](../Page/絕望深淵.md "wikilink")(2013)
  - [暴動](../Page/暴動.md "wikilink")(2014)
  - [西部鏢客大戰](../Page/西部鏢客大戰.md "wikilink")(2014)
  - [泰坦隕落](../Page/泰坦隕落.md "wikilink")(2014)
  - [泰坦降臨2](../Page/泰坦降臨2.md "wikilink")(2016)
  - [基建危機](../Page/基建危機.md "wikilink")(2016)
  - [恥辱之日](../Page/恥辱之日.md "wikilink")(2017)
  - [獵殺佛里曼](../Page/獵殺佛里曼.md "wikilink")(2018)
  - [Apex英雄](../Page/Apex英雄.md "wikilink")(2019)

## 参考文献

## 外部链接

  - [Source引擎的许可页](http://source.valvesoftware.com/)
  - [Valve开发者社群](http://developer.valvesoftware.com/)
  - [DevMaster的Source引擎细节](http://www.devmaster.net/engines/engine_details.php?id=34)

[Category:2004年软件](../Category/2004年软件.md "wikilink")
[Category:专有软件](../Category/专有软件.md "wikilink")
[Category:威爾烏](../Category/威爾烏.md "wikilink")
[Category:游戏引擎](../Category/游戏引擎.md "wikilink")
[Source引擎游戏](../Category/Source引擎游戏.md "wikilink")
[Category:Linux游戏引擎](../Category/Linux游戏引擎.md "wikilink")

1.
2.
3.
4.
5.
6.
7.