《**我们的国家**》（，）是[芬兰](../Page/芬兰.md "wikilink")[国歌](../Page/国歌.md "wikilink")，1848年5月13日首次演奏，[弗雷德里克·帕修斯作曲](../Page/弗雷德里克·帕修斯.md "wikilink")（[爱沙尼亚国歌](../Page/爱沙尼亚.md "wikilink")《[我的土地，我的欢愉](../Page/我的土地，我的欢愉.md "wikilink")》也采用了他的这个作品），与爱沙尼亚国歌在乐曲上大致相同（芬兰国歌每段的第3句到第6句要重複一次，而爱沙尼亚国歌則不用），歌词也颇为相似。该歌词节选自芬兰民族诗人[约翰·卢德维格·鲁内贝里于](../Page/约翰·卢德维格·鲁内贝里.md "wikilink")1846年完成的[浪漫民族主义杰作](../Page/浪漫民族主义.md "wikilink")－诗歌《》（），原诗共有11小节，国歌选取了其中的第一段和最后一段。歌词原为[瑞典语](../Page/瑞典语.md "wikilink")，1867年由翻译为[芬兰语](../Page/芬兰语.md "wikilink")。\[1\]\[2\]

《旗手斯托尔传奇》在整个[斯堪的纳维亚地区广为流传](../Page/斯堪的纳维亚.md "wikilink")，直到1917年至1918年[芬兰宣布独立期间](../Page/芬兰独立宣言.md "wikilink")，该曲才被认为对芬兰有特殊意义。帕修斯的曲子和鲁内贝里的歌词也在[丹麦](../Page/丹麦.md "wikilink")、[挪威和](../Page/挪威.md "wikilink")[瑞典广为传唱](../Page/瑞典.md "wikilink")。原来鲁内贝里的歌词里面没有特别指明芬兰（除了第四节和第十节，不过这两节很少唱），只是指一个北方的国家，但是芬兰语的歌词明确提到了[芬兰](../Page/芬兰.md "wikilink")。歌词的节奏和主题，和[瑞典国歌](../Page/瑞典.md "wikilink")《[你古老，你自由](../Page/你古老，你自由.md "wikilink")》（或譯為《古老而自由的北國》）和[挪威国歌](../Page/挪威.md "wikilink")《[對！我們熱愛祖國](../Page/對！我們熱愛祖國.md "wikilink")》很相似。

## 歌詞

[VartLandStatue.JPG](https://zh.wikipedia.org/wiki/File:VartLandStatue.JPG "fig:VartLandStatue.JPG")歌词。\]\]
原詩有11節，官方歌詞為第1節和第11節。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/芬兰语.md" title="wikilink">芬兰语</a></p></th>
<th><p>译文</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt>1</dt>
<dd>Oi maamme, Suomi, synnyinmaa,
</dd>
<dd>soi, sana kultainen!
</dd>
<dd>Ei laaksoa, ei kukkulaa,
</dd>
<dd>ei vettä, rantaa rakkaampaa
</dd>
<dd>kuin kotimaa tää pohjoinen,
</dd>
<dd>maa kallis isien.
</dd>
<dd>Ei laaksoa, ei kukkulaa,
</dd>
<dd>ei vettä, rantaa rakkaampaa
</dd>
<dd>kuin kotimaa tää pohjoinen,
</dd>
<dd>maa kallis isien.
</dd>
<dd>
</dd>
<dt>11</dt>
<dd>Sun kukoistukses kuorestaan
</dd>
<dd>kerrankin puhkeaa;
</dd>
<dd>viel' lempemme saa nousemaan
</dd>
<dd>sun toivos, riemus loistossaan,
</dd>
<dd>ja kerran laulus, synnyinmaa
</dd>
<dd>korkeemman kaiun saa.
</dd>
<dd>viel' lempemme saa nousemaan
</dd>
<dd>sun toivos, riemus loistossaan,
</dd>
<dd>ja kerran laulus, synnyinmaa
</dd>
<dd>korkeemman kaiun saa.
</dd>
</dl></td>
<td><dl>
<dt>　</dt>
<dd>我們的國家，芬蘭，我們的土地，
</dd>
<dd>為無價之名高呼！
</dd>
<dd>沒有峽谷，沒有山丘，
</dd>
<dd>沒有觸及天際的高山
</dd>
<dd>作為北方的故鄉，
</dd>
<dd>如父親般高貴的國家。
</dd>
<dd>沒有峽谷，沒有山丘，
</dd>
<dd>沒有觸及天際的高山
</dd>
<dd>作為北方的故鄉，
</dd>
<dd>如父親般高貴的國家。
</dd>
<dd>
</dd>
<dt>　</dt>
<dd>在寒冷中含苞待放
</dd>
<dd>你將再次崛起；
</dd>
<dd>願我們的愛逐漸升華
</dd>
<dd>你的希望，歡樂與榮耀，
</dd>
<dd>願獻給祖國的歌聲
</dd>
<dd>在高處回響。
</dd>
<dd>願我們的愛逐漸升華
</dd>
<dd>你的希望，歡樂與榮耀，
</dd>
<dd>願獻給祖國的歌聲
</dd>
<dd>在高處回響。
</dd>
</dl></td>
</tr>
</tbody>
</table>

## 参见

  - [国歌列表](../Page/国歌列表.md "wikilink")

## 参考文献

[Category:国歌](../Category/国歌.md "wikilink")
[歌](../Category/芬蘭國家象徵.md "wikilink")
[Category:芬蘭音樂](../Category/芬蘭音樂.md "wikilink")
[Category:芬蘭歌曲](../Category/芬蘭歌曲.md "wikilink")

1.
2.  [J. L.
    Runeberg](http://www.finlit.fi/oppimateriaali/kielijaidentiteetti/main.php?target=jlruneberg)
     - 芬兰文学社网站