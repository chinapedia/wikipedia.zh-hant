**八島**（）是[日本海軍的](../Page/大日本帝國海軍.md "wikilink")[戰艦](../Page/戰艦.md "wikilink")。[富士級戰艦的二號](../Page/富士級戰艦.md "wikilink")[軍艦](../Page/軍艦.md "wikilink")，與其餘五艘[日本戰艦](../Page/日本戰艦.md "wikilink")（[富士](../Page/富士號戰艦.md "wikilink")、[敷島](../Page/敷島號戰艦.md "wikilink")、[初瀬](../Page/初瀬號戰艦.md "wikilink")、[朝日](../Page/朝日號戰艦.md "wikilink")、[三笠](../Page/三笠號戰艦.md "wikilink")）在1904年至1905年的[日俄戰爭中組成](../Page/日俄戰爭.md "wikilink")[日本主要戰艦群](../Page/日本.md "wikilink")，八島的有著非常簡短的戰鬥經歷。

## 概要

八島和姊妹艦富士是最早二艘外國為日本製造的戰艦。因為日本在當時未有能力自行製造現代鋼製軍艦，本艦在1894年從[英國](../Page/英國.md "wikilink")[紐卡素](../Page/泰恩河畔纽卡斯尔.md "wikilink")(Newcastle
upon
Tyne)[埃爾斯維克](../Page/埃爾斯維克.md "wikilink")(Elswick)[阿姆斯壯造船廠](../Page/阿姆斯壯造船廠.md "wikilink")(Sir
W G Armstrong Whitworth & Co Ltd)訂購。

為了幫助阿姆斯壯造船廠賣掉富士與八島，他們之前造出詳細的高精密-{[模型](../Page/模型.md "wikilink")}-。八島的-{模型}-現在仍然在英國[薩福克](../Page/薩福克.md "wikilink")(Suffolk)[哈爾伯克](../Page/哈爾伯克.md "wikilink")(Holbrook)[皇家醫學學院](../Page/皇家醫學學院.md "wikilink")(RHS)(Royal
Hospital School)展出。銘牌上寫著「日本裝甲艦『八島』速度19 3/4 [節](../Page/節.md "wikilink")，
1897年在紐卡素由阿姆斯壯造船廠(Sir W.G. Armstrong Whitworth & Co Ld. Elswick
Shipyard)製造。 由菲利普瓦特、KCB， FRS等先生設計。」\[1\]

1897年，日俄戰爭的開始以後，八島從阿姆斯壯造船廠交付和被分配了到日俄戰爭的第一艦隊，在海軍少將[三須宗太郎](../Page/三須宗太郎.md "wikilink")(Nashiba
Tokioki)(みす そうたろう)帶領下的第一戰隊。

1904年5月14日三須宗太郎[海軍少將與戰艦初瀬](../Page/海軍少將.md "wikilink")（[旗艦](../Page/旗艦.md "wikilink")），
敷島和八島、[防護巡洋艦](../Page/防護巡洋艦.md "wikilink")[笠置和](../Page/笠置號防護巡洋艦.md "wikilink")[輕巡洋艦](../Page/輕巡洋艦.md "wikilink")[龍田投入](../Page/龍田號輕巡洋艦.md "wikilink")[旅順港閉塞作戰去解除](../Page/旅順港閉塞作戰.md "wikilink")[亞瑟港](../Page/亞瑟港.md "wikilink")（[旅順港](../Page/旅順港.md "wikilink")）對日本力量的封鎖。

1904年5月15日，第一艦隊到達[老鐵山後繼續往西北行進直至到達十五](../Page/老鐵山.md "wikilink")[英里外的亞瑟港](../Page/英里.md "wikilink")(旅順港)。三須宗太郎海軍少將繼續由北到東橫跨口岸巡邏，
這條路線帶領了八島等艦進入[阿穆爾河](../Page/阿穆爾河.md "wikilink")（[黑龍江](../Page/黑龍江.md "wikilink")）—[俄國](../Page/俄羅斯.md "wikilink")[布雷艦以前放置的](../Page/布雷艦.md "wikilink")[佈雷區](../Page/佈雷區.md "wikilink")，初瀬和八島觸雷沉沒是日本海軍在日俄戰爭期間受到最大的損失。

## 艦歷

  - 1894年12月28日 英國，阿姆斯壯造船廠製造。
  - 1896年2月28日 下水。
  - 1897年9月9日 竣工。
  - 1897年11月30日 到達[橫須賀](../Page/橫須賀.md "wikilink")。
  - 1898年3月21日 作為一等戰列艦。
  - 1904年 參加第1艦隊第1支隊的4號軍艦日俄戰爭。
      - 2月6日 開始參加旅順港閉塞作戰。
      - 5月15日 老鐵山東南海上兩度觸雷，約六小時半後沉沒。
  - 1905年6月1日 喪失公表。
  - 1905年6月15日 除籍。

## 同級艦

  - [富士](../Page/富士號戰艦.md "wikilink")

## 參考來源

<references/>

## 參考文獻

  - 、[光人社](../Page/光人社.md "wikilink")、2003年、ISBN 4769811519

<!-- end list -->

  -

    陳寶蓮編譯、《聯合艦隊軍艦大全》、麥田、1997年、 ISBN 9577085334

<!-- end list -->

  - Gibbons, Tony: The Complete Encyclopedia of Battleships and
    Battlecruisers

  - Burt, R.A.: Japanese Battleships, 1897-1945

## 關連項目

  - [大日本帝國海軍艦艇列表](../Page/大日本帝國海軍艦艇列表.md "wikilink")

## 外部連結

  -
[Category:日本海军战列舰](../Category/日本海军战列舰.md "wikilink")
[Category:日俄戰爭舰艇](../Category/日俄戰爭舰艇.md "wikilink")
[Category:1896年下水](../Category/1896年下水.md "wikilink")
[Category:1897年竣工的船只](../Category/1897年竣工的船只.md "wikilink")
[Category:阿姆斯特朗-惠特沃斯建造的船舶](../Category/阿姆斯特朗-惠特沃斯建造的船舶.md "wikilink")

1.  英國薩福克哈爾伯克皇家醫學學院展出的「八島」-{模型}-銘牌上寫的原文：'Japanese Armourclad "Yashima"
    19 3/4 knots speed, built by Sir W.G. Armstrong Whitworth & Co Ld.
    Elswick Shipyard 1897 Newcastle on Tyne. Designed by Sir Philip
    Watts, KCB, FRS etc.'