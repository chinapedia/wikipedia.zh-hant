《**聖戰奇兵**》（）是1989年的科幻冒險電影，由[史蒂芬·史匹柏執導](../Page/史蒂芬·史匹柏.md "wikilink")，[喬治·盧卡斯擔任故事編劇](../Page/喬治·盧卡斯.md "wikilink")，為《[印第安納·瓊斯](../Page/印第安納·瓊斯.md "wikilink")》系列的續集電影。[哈里遜·福特三度飾演考古學教授兼探險家](../Page/哈里遜·福特.md "wikilink")[印第安納·瓊斯](../Page/印第安納·瓊斯.md "wikilink")（Indiana
Jones），片中加入了由[史恩·康納萊飾演的印第安納](../Page/史恩·康納萊.md "wikilink")·瓊斯的父親，並敘述了少年時期的印第安納·瓊斯（由[瑞凡·費尼克斯飾演](../Page/瑞凡·費尼克斯.md "wikilink")）。

## 劇情簡介

考古學教授印第安纳·琼斯的父親亨利·琼斯教授，因為研究並尋找傳說中的耶穌[聖杯而失蹤](../Page/聖杯.md "wikilink")。由於德國[納粹黨也要奪取有神奇力量的聖杯](../Page/納粹黨.md "wikilink")，印第安纳·琼斯和父親必須在奧地利及德國納粹黨軍隊追殺下冒險死裡逃生。最终两者空手而归，但只有琼斯父子捡回了性命。

## 角色

<table>
<thead>
<tr class="header">
<th><p>演員</p></th>
<th><p>角色</p></th>
<th><p>简介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/哈里遜·福特.md" title="wikilink">哈里遜·福特</a></strong></p></td>
<td><p><strong><a href="../Page/印第安纳·琼斯.md" title="wikilink">印第安纳·琼斯博士</a><br />
Dr.Indiana Jones</strong></p></td>
<td><p>主角，考古学家，不喜欢别人叫他“二世”。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/里弗·菲尼克斯.md" title="wikilink">里弗·菲尼克斯</a></strong></p></td>
<td><p>十三岁的印第安纳。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/肖恩·康纳利.md" title="wikilink">肖恩·康纳利</a></strong></p></td>
<td><p><strong>亨利·琼斯教授<br />
Professor Henry Jones</strong></p></td>
<td><p>安纳·琼斯的父親，因為研究並尋找傳說中的耶穌<a href="../Page/聖杯.md" title="wikilink">聖杯而失蹤</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p>青年的亨利·琼斯。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong></strong></p></td>
<td><p><strong>kazim</strong></p></td>
<td><p>哈塔伊的軍官，聖杯的守護者，被多诺万殺害。</p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p><strong>聖杯騎士<br />
Grail Knight</strong></p></td>
<td><p>古老的聖杯守護者，於好幾年前死守著聖殿。</p></td>
</tr>
<tr class="odd">
<td><p><strong></strong></p></td>
<td><p><strong>科洛内尔·沃格尔<br />
Colonel Voge</strong></p></td>
<td><p>納粹成員，党卫队旗队领袖。</p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p><strong>沃尔特·多诺万<br />
Walter Donovan</strong></p></td>
<td><p>納粹成員，博物馆收藏家，委托<a href="../Page/印第安纳·琼斯.md" title="wikilink">印第安纳·琼斯去寻找</a><a href="../Page/圣杯.md" title="wikilink">圣杯</a>。为了得到圣杯投靠纳粹，背叛自己的国家，最終選到假聖杯而死。</p></td>
</tr>
<tr class="odd">
<td><p><strong></strong></p></td>
<td><p><strong>艾尔莎·施耐德博士<br />
Dr. Elsa Schneider</strong></p></td>
<td><p>納粹成員，企圖帶走圣杯。最終因为圣杯不能带出圣殿，而艾莎的行为违反了这个规定并导致圣殿的倒塌，因此掉進深淵之中，生死不明。</p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p><strong>马库斯·布罗迪博士<br />
Dr. Marcus Brody</strong></p></td>
<td><p>琼斯在大学的同事，亨利·琼斯的好友。精通多國語言。</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/约翰·里斯-戴维斯.md" title="wikilink">约翰·里斯-戴维斯</a></strong></p></td>
<td><p><strong>萨拉赫<br />
Sallah</strong></p></td>
<td><p>琼斯博士的帮手，来自埃及的考古爱好者，协助琼斯找寻圣杯的下落，性格开朗，擅长挖掘，被称作“埃及最好的挖掘机”。</p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p><strong><a href="../Page/阿道夫·希特勒.md" title="wikilink">阿道夫·希特勒</a><br />
Adolf Hitler</strong></p></td>
<td><p>串客角色，企圖統治全世界，在琼斯博士的日记上签下大名。</p></td>
</tr>
</tbody>
</table>

## 獎項記錄

  - [雨果獎最佳戲劇片](../Page/雨果獎.md "wikilink")。

奥斯卡獎

## 外部連結

  - [IndianaJones.com](http://www.indianajones.com/), 喬治·盧卡斯的印第安纳·琼斯網站

  - [The Indiana Jones
    Wiki](http://indianajones.wikia.com/wiki/Main_Page)印第安纳·琼斯迷的網站

  -
  -
  -
  -
  -
[Category:1989年電影](../Category/1989年電影.md "wikilink")
[Category:1980年代動作片](../Category/1980年代動作片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國續集電影](../Category/美國續集電影.md "wikilink")
[Category:威尼斯背景電影](../Category/威尼斯背景電影.md "wikilink")
[Category:1910年代背景電影](../Category/1910年代背景電影.md "wikilink")
[Category:1930年代背景電影](../Category/1930年代背景電影.md "wikilink")
[Category:第二次世界大戰電影](../Category/第二次世界大戰電影.md "wikilink") [Last
Crusade, The](../Category/虚构纳粹题材作品.md "wikilink")
[Category:有关纳粹神秘主义的电影](../Category/有关纳粹神秘主义的电影.md "wikilink")
[Category:猶他州取景電影](../Category/猶他州取景電影.md "wikilink")
[Category:奥斯卡最佳音效剪辑奖获奖电影](../Category/奥斯卡最佳音效剪辑奖获奖电影.md "wikilink")
[Category:寻宝电影](../Category/寻宝电影.md "wikilink")
[Category:美国特摄电影](../Category/美国特摄电影.md "wikilink")
[Category:盧卡斯影業電影](../Category/盧卡斯影業電影.md "wikilink")
[Category:約翰·威廉斯配樂電影](../Category/約翰·威廉斯配樂電影.md "wikilink")
[Category:祕密結社題材作品](../Category/祕密結社題材作品.md "wikilink")
[Category:印第安納·瓊斯電影](../Category/印第安納·瓊斯電影.md "wikilink")
[Category:長生不老題材作品](../Category/長生不老題材作品.md "wikilink")
[Category:亞瑟王傳說電影](../Category/亞瑟王傳說電影.md "wikilink")