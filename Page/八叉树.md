[Octree2.png](https://zh.wikipedia.org/wiki/File:Octree2.png "fig:Octree2.png")。右：對應的八岔樹\]\]

**八叉树**（）是一种[树形数据结构](../Page/树_\(数据结构\).md "wikilink")，每个内部节点都正好有八个子节点。八叉树常用于分割[三维空间](../Page/三维空间.md "wikilink")，将其递归细分为八个[卦限](../Page/卦限.md "wikilink")。八叉树是[四叉树在三维空间中的对应](../Page/四叉树.md "wikilink")，在[三维图形](../Page/三维计算机图形.md "wikilink")、三维[游戏引擎等领域有很多应用](../Page/游戏引擎.md "wikilink")。

## 表示空间

八叉树的每个节点都可以代表一个[空间](../Page/空间.md "wikilink")，对应的八个子节点则将这个空间细分为八个[卦限](../Page/卦限.md "wikilink")。点域（，简称PR）八叉树的节点中都存储着一个三维[点](../Page/点.md "wikilink")，即该节点对应区域的「中心」，也是八个子节点对应区域中的一个角落。矩阵（，简称MX）八叉树中，节点只记录区域范围，对应的中心点坐标需要从区域范围推算。因此，PR八叉树的根节点可以表示无限大的空间；而MX八叉树的根节点只能表示有限空间，这样才可以得到隐含的中心点。

## 历史

八叉树在[三维计算机图形领域的应用可以追溯到](../Page/三维计算机图形.md "wikilink")1980年[伦斯勒理工学院唐纳德](../Page/伦斯勒理工学院.md "wikilink")·马尔（）的报告《八叉树编码：使用计算机表示、操作、显示任意三维对象的新技术》（）\[1\]。

## 主要用途

  - [三维计算机图形学中的](../Page/三维计算机图形学.md "wikilink")[细节层次渲染](../Page/细节层次.md "wikilink")\[2\]

  - [最邻近搜索](../Page/最邻近搜索.md "wikilink")

  - 三维空间中的高效[碰撞检测](../Page/碰撞检测.md "wikilink")

  -
  - [状态估计](../Page/狀態觀測器.md "wikilink")\[3\]

## 另见

  -
  - [体素](../Page/体素.md "wikilink")

  - [四叉树](../Page/四叉树.md "wikilink")

  - [OGRE](../Page/OGRE.md "wikilink")，有基于八叉树的场景管理器

  - ，支持八叉树场景节点

## 参考资料

## 外部連結

  - [Octree Quantization in Microsoft Systems
    Journal](https://web.archive.org/web/20140605161956/http://www.microsoft.com/msj/archive/S3F1.aspx)
  - [Color Quantization using Octrees in Dr.
    Dobb's](http://www.ddj.com/184409805)
  - [Octree Color Quantization
    Overview](http://web.cs.wpi.edu/~matt/courses/cs563/talks/color_quant/CQoctree.html)
  - [Parallel implementation of octtree generation algorithm, P. Sojan
    Lal, A Unnikrishnan, K Poulose Jacob, ICIP 1997, IEEE Digital
    Library](http://ieeexplore.ieee.org/xpl/freeabs_all.jsp?arnumber=727419)
  - [C++ implementation (GPL
    license)](http://nomis80.org/code/octree.html)
  - [Parallel Octrees for Finite Element
    Applications](http://sc07.supercomputing.org/schedule/pdf/pap117.pdf)
  - [Cube 2: Sauerbraten - a game written in the octree-heavy Cube 2
    engine](http://www.sauerbraten.org/)
  - [Ogre - A 3d Object-oriented Graphics Rendering Engine with a Octree
    Scene Manager Implementation (MIT license)](http://www.ogre3d.org)
  - [Dendro: parallel multigrid for octree meshes (MPI/C++
    implementation)](http://www.cc.gatech.edu/csela/dendro)
  - [**Video**: Use of an octree in state
    estimation](http://www.youtube.com/watch?v=Jw4VAgcWruY)

[B](../Category/数据结构.md "wikilink")

1.
2.
3.  [Henning Eberhardt, Vesa Klumpp, Uwe D. Hanebeck, *Density Trees for
    Efficient Nonlinear State Estimation*, Proceedings of the 13th
    International Conference on Information Fusion, Edinburgh, United
    Kingdom, July,
    2010.](http://isas.uka.de/Publikationen/Fusion10_EberhardtKlumpp.pdf)