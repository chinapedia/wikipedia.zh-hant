**玉山前峰**，海拔3,239公尺，[日治時期名為](../Page/臺灣日治時期.md "wikilink")**前山**、**新高前山**，為[臺灣在](../Page/臺灣.md "wikilink")[玉山山脈中的一座高山](../Page/玉山山脈.md "wikilink")，位於[玉山西峰西側](../Page/玉山西峰.md "wikilink")，隔[塔塔加鞍部而與](../Page/塔塔加鞍部.md "wikilink")[麟趾山對望](../Page/麟趾山.md "wikilink")。全境座落在[玉山國家公園之西北園區境內](../Page/玉山國家公園.md "wikilink")，位置恰為[南投縣](../Page/南投縣.md "wikilink")[信義鄉與](../Page/信義鄉_\(臺灣\).md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[阿里山鄉之交界](../Page/阿里山鄉.md "wikilink")，由玉山國家公園管理處管轄\[1\]。在[百岳排名裡](../Page/台灣百岳.md "wikilink")，玉山前峰位居第69。

1970年代，政府計畫新建三條橫貫公路，分別在北、中、南各闢一條\[2\]。1980年代，其中位在中部的新闢橫貫公路，計畫[水-{里}-](../Page/水里玉山線.md "wikilink")、[嘉義](../Page/嘉義玉山線.md "wikilink")、[玉里等三線交會於沙里仙溪頭](../Page/玉里玉山線.md "wikilink")（指[沙里仙溪的主流源頭](../Page/沙里仙溪.md "wikilink")），也就是在玉山前峰的北側山坡面，後因工程開闢時發現水-{里}-、玉里二線太過接近，形成上下線會影響山坡地質的穩定，考量到日後養護成本而重新探勘，最後決定改線\[3\]。

## 參考文獻

<div class="references-small">

<references />

</div>

[category:台灣百岳](../Page/category:台灣百岳.md "wikilink")

[Category:嘉義縣山峰](../Category/嘉義縣山峰.md "wikilink")
[Category:南投縣山峰](../Category/南投縣山峰.md "wikilink")
[Category:玉山山脈](../Category/玉山山脈.md "wikilink")
[Category:玉山國家公園](../Category/玉山國家公園.md "wikilink")

1.  《南投縣信義鄉行政區域圖》（1：60,000）上河文化、銳俤科技，臺北市，ISBN：986-00-2688-2，內政部，2005年12月。

2.  《玉山仰望：新中橫時代》〈踏勘〉、〈闢建〉李瑞宗，臺北市，ISBN：9789860424089，交通部公路總局，2016年1月。

3.