[Five-Civilized-Tribes-Portraits.png](https://zh.wikipedia.org/wiki/File:Five-Civilized-Tribes-Portraits.png "fig:Five-Civilized-Tribes-Portraits.png")
[Okterritory.png](https://zh.wikipedia.org/wiki/File:Okterritory.png "fig:Okterritory.png")
**文明化五部族**（），指[美國原住民中的五個部族](../Page/美國原住民.md "wikilink")。分別是[奇克索人](../Page/奇克索人.md "wikilink")、[切羅基人](../Page/切羅基人.md "wikilink")、[喬克托人](../Page/喬克托人.md "wikilink")、以及[塞米諾爾人](../Page/塞米諾爾人.md "wikilink")。有時為了避免暗指其他族群較「野蠻」，「文明化五部族」一詞會以「五部族」來取代。

這五個部族吸取了較多的歐洲移民的生活習慣，且與鄰近族群的關係較為良善。早期生活在[美國東南部](../Page/美國.md "wikilink")；後來在聯邦政府的政策下，被向西安置在[密西西比河以西地區一處稱做](../Page/密西西比河.md "wikilink")「印地安領地」（Indian
Territory）的地方，也就是今天的[奧克拉荷馬州東部](../Page/奧克拉荷馬州.md "wikilink")。其中一場遷徙發生在實施之後，又稱1838年的「[血淚之路](../Page/血淚之路.md "wikilink")」（Trail
of Tears）。

在[南北戰爭時期](../Page/南北戰爭.md "wikilink")，五部族除了各自分別支持了不同的陣營，某些部族本身也有分歧。喬克托人與奇克索人主要支持[美利堅聯盟國](../Page/美利堅聯盟國.md "wikilink")；克里克人、塞米諾爾人與切羅基人內部則有不同的支持對象，使其發生民族內的爭鬥。

## 外部連結

  - [Five Civilized Tribes Museum official
    site](http://www.fivetribes.org/)
  - [Five Tribes article at
    nativeamericans.com](http://www.nativeamericans.com/FiveCivilizedTribes.htm)
  - [Descendants of Freedmen of the Five Civilized Tribes
    Association](http://www.freedmen5tribes.com/)

[Category:美國原住民部落](../Category/美國原住民部落.md "wikilink")