**讀谷村**（，）位於[琉球列島](../Page/琉球列島.md "wikilink")[沖繩群島](../Page/沖繩群島.md "wikilink")[沖繩島中部地區西側](../Page/沖繩島.md "wikilink")，為[日本人口最多的](../Page/日本.md "wikilink")「村」級行政區。村中有45%的土地為[駐日美軍基地](../Page/駐日美軍.md "wikilink")，包括[美國海軍楚邊通信所](../Page/美國海軍.md "wikilink")、[美國陸軍](../Page/美國陸軍.md "wikilink")、[美國海軍陸戰隊讀谷輔助飛行場](../Page/美國海軍陸戰隊.md "wikilink")、[美國空軍瀨名波通信設施](../Page/美國空軍.md "wikilink")、嘉手納彈藥庫。

北方為[恩納村](../Page/恩納村.md "wikilink")，東方為[沖繩市](../Page/沖繩市.md "wikilink")，南方為[嘉手納町](../Page/嘉手納町.md "wikilink")，西側靠海。村內31.5%的區域為農業用地，種植菊花、甘蔗和馬鈴薯，35.7%為森林，12.3%為住宅。村里6%的人口從事農業和漁業，25%為製造業，69%為服務業和貿易。

## 地理

### 轄區

<table style="width:99%;">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>伊良皆</li>
<li>上地</li>
<li>宇座</li>
<li>大木</li>
<li>大灣</li>
<li>喜名</li>
<li>儀間</li>
</ul></td>
<td><ul>
<li>座喜味</li>
<li>瀨名波</li>
<li>楚邊</li>
<li>高志保</li>
<li>渡具知</li>
<li>渡慶次</li>
<li>都屋</li>
</ul></td>
<td><ul>
<li>長濱</li>
<li>波平</li>
<li>比謝</li>
<li>比謝矼</li>
<li>古堅</li>
<li>牧原</li>
</ul></td>
</tr>
</tbody>
</table>

## 歷史

  - 在西海岸的沙丘上發現大量貝塚時代的遺跡，渡具知木棉原遺跡的箱式石棺墳被認為是受到了九州的影響。比謝川的出海口附近有的渡具知東原遺跡中亦有發現爪形文土器，可追溯到約7000年前的沖繩史前時代文化。
  - 1896年4月1日：實施[郡區制](../Page/郡區制.md "wikilink")，為[中頭郡轄下](../Page/中頭郡.md "wikilink")**讀谷山間切**。
  - 1908年4月1日：實施[島嶼町村制](../Page/島嶼町村制.md "wikilink")，讀谷間切改設為**讀谷山村**。
  - 1946年12月16日：讀谷山村改名為**讀谷村**。

## 交通

### 道路

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>國道</dt>

</dl>
<ul>
<li>國道58號</li>
</ul></td>
<td><dl>
<dt>一般<a href="../Page/都道府縣道.md" title="wikilink">縣道</a></dt>

</dl>
<ul>
<li>沖繩縣道6號線</li>
<li>沖繩縣道12號線</li>
<li>沖繩縣道16號線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

[Cape_Zampa_Lighthouse_on_Cape_Zampa_5.JPG](https://zh.wikipedia.org/wiki/File:Cape_Zampa_Lighthouse_on_Cape_Zampa_5.JPG "fig:Cape_Zampa_Lighthouse_on_Cape_Zampa_5.JPG")\]\]
[Ryukyu_no_Kaze.jpg](https://zh.wikipedia.org/wiki/File:Ryukyu_no_Kaze.jpg "fig:Ryukyu_no_Kaze.jpg")

  - 座喜味城遺跡（[琉球王國遺產群](../Page/琉球王國.md "wikilink")）

## 教育

### 高等學校

  - [沖繩縣立讀谷高等學校](http://www.yomitan-h.open.ed.jp/)

### 中學校

<table style="width:50%;">
<colgroup>
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20060910013042/http://school.yomitan.jp/yomityu/">讀谷村立讀谷中學校</a></li>
<li>讀谷村立古堅中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20060908094705/http://school.yomitan.jp/tokeshisyo/index.htm">讀谷村立渡慶次小學校</a></li>
<li><a href="https://web.archive.org/web/20070311153531/http://school.yomitan.jp/kinasyo/">讀谷村立喜名小學校</a></li>
<li><a href="https://web.archive.org/web/20070311153343/http://school.yomitan.jp/yomisyo/">讀谷村立讀谷小學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20060907122936/http://school.yomitan.jp/furusyo/index.htm">讀谷村立古堅小學校</a></li>
<li>讀谷村立古堅南小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 本地出身之名人

  - [Kiroro](../Page/Kiroro.md "wikilink")（女子歌唱團體）
  - [糸數慶子](../Page/糸數慶子.md "wikilink")
    （[參議院](../Page/參議院.md "wikilink")[議員](../Page/議員.md "wikilink")）
  - [知花昌一](../Page/知花昌一.md "wikilink") （讀谷村議會議員、和平運動家）
  - [佐久本昌廣](../Page/佐久本昌廣.md "wikilink")（[日本職棒球員](../Page/日本職棒.md "wikilink")）
  - [屋良朝苗](../Page/屋良朝苗.md "wikilink")（第一任沖繩縣[知事](../Page/知事.md "wikilink")）

## 外部連結