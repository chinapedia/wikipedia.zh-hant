[TAIWANESE_Diplomat_&_Hero_-_Martyr_for_Independence_Chen_Chih-hsiung_臺灣外交官與獨立運動先驅烈士_陳智雄.JPG](https://zh.wikipedia.org/wiki/File:TAIWANESE_Diplomat_&_Hero_-_Martyr_for_Independence_Chen_Chih-hsiung_臺灣外交官與獨立運動先驅烈士_陳智雄.JPG "fig:TAIWANESE_Diplomat_&_Hero_-_Martyr_for_Independence_Chen_Chih-hsiung_臺灣外交官與獨立運動先驅烈士_陳智雄.JPG")

**陳智雄**（Tân
Tì-hiông，），[臺灣](../Page/臺灣.md "wikilink")[日治時期生於](../Page/臺灣日治時期.md "wikilink")[阿緱廳](../Page/阿緱廳.md "wikilink")（今[屏東縣中北部](../Page/屏東縣.md "wikilink")）。\[1\]\[2\]曾隨日軍被派至印尼擔任外交官，第二次世界大戰之後居住在印尼，冒死參與[印尼獨立革命](../Page/印度尼西亞獨立革命.md "wikilink")，獨立後首任總統[蘇卡諾授以榮譽國民的最高榮譽](../Page/蘇卡諾.md "wikilink")。因參與[台灣獨立運動成為無國籍者](../Page/台灣獨立運動.md "wikilink")。為了進入日本，取得[瑞士國籍](../Page/瑞士.md "wikilink")。曾經擔任[台灣共和國臨時政府的](../Page/台灣共和國臨時政府.md "wikilink")[東南亞巡迴大使](../Page/東南亞.md "wikilink")\[3\]\[4\]陳五郎撰、吳宗德譯,
[泣 台灣獨立先烈──勇者的畫像 陳智雄](http://www.taiwanus.net/roger/hero_1.htm),
2000/11/23</ref>\[5\]\[6\]，之後遭中華民國政府於[日本誘捕並處決](../Page/日本.md "wikilink")。\[7\]\[8\]\[9\]

## 早年事蹟與印尼經驗

陳智雄於1916年出生於[大日本帝國統治之下的屏東](../Page/大日本帝國.md "wikilink")，家境小康，高中未畢業就赴[日本](../Page/日本.md "wikilink")[青山學院高中部](../Page/青山學院大學.md "wikilink")，之後考上[東京外國語大學](../Page/東京外國語大學.md "wikilink")[荷蘭語科](../Page/荷蘭語.md "wikilink")，畢業後曾任職日本[外務省](../Page/外務省.md "wikilink")。[太平洋戰爭爆發後](../Page/太平洋戰爭.md "wikilink")，日軍佔領南洋的[英](../Page/英國.md "wikilink")、[荷屬](../Page/荷蘭.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")，致使日本政府頓感外文人才的迫切需要。因陳智雄精通[英語](../Page/英語.md "wikilink")、[日語](../Page/日語.md "wikilink")、[荷蘭語](../Page/荷蘭語.md "wikilink")、[馬來語](../Page/馬來語.md "wikilink")、[台語以及](../Page/台語.md "wikilink")[北京官話等六種語言](../Page/北京官話.md "wikilink")，遂被外務省派至[印尼擔任](../Page/印尼.md "wikilink")[翻譯](../Page/翻譯.md "wikilink")。\[10\]\[11\]\[12\]

[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")，陳智雄辭去外務省的工作，滯留印尼從事生意，並與一荷蘭籍女子結婚。當時，長期受[帝國主義統治的殖民地紛紛爭取獨立](../Page/帝國主義.md "wikilink")，受荷蘭統治的印尼也不例外，由[蘇卡諾領導的獨立軍](../Page/蘇卡諾.md "wikilink")，與荷蘭統治當局正做殊死戰鬥。出身日本殖民地——台灣的陳智雄，對殖民地人民爭取民族解放的獨立運動頗為同情，乃藉荷籍夫人為掩護，暗中提供日軍遺留下來的大批武器，援助印尼的獨立革命軍，也因此被荷蘭軍政府逮捕並囚禁達一年之久。印尼獨立後，蘇卡諾擔任首任總統，蘇有感於陳智雄的冒死義援，遂待他如國賓，並授以榮譽國民的最高榮譽。\[13\]\[14\]\[15\]

## 擔任「台灣共和國臨時政府」的駐東南亞巡迴大使

目睹印尼經由人民的奮鬥而終獲獨立的過程，陳智雄不僅萌生了台灣獨立的想法，而且決定獻身於[台灣獨立運動](../Page/台灣獨立運動.md "wikilink")，因此，他接受了設於[日本之](../Page/日本.md "wikilink")「台灣共和國臨時政府」大統領[廖文毅的委任](../Page/廖文毅.md "wikilink")，擔任其駐東南亞巡迴大使一年。\[16\]\[17\]\[18\]透過陳智雄的良好關係及人脈，廖文毅曾於1955年應邀參加了在印尼舉行的「[萬隆會議](../Page/萬隆會議.md "wikilink")」，使他的國際聲望達到了最高點\[19\]。當時出席的各國代表當中，除了台灣共和國臨時政府的廖文毅以外\[20\]，還包括[中國的](../Page/中國.md "wikilink")[周恩來](../Page/周恩來.md "wikilink")、[印度的](../Page/印度.md "wikilink")[尼赫魯](../Page/尼赫魯.md "wikilink")、[埃及的](../Page/埃及.md "wikilink")[納瑟](../Page/納瑟.md "wikilink")、[马来亚联合邦](../Page/马来亚联合邦.md "wikilink")（今天的[马来西亚](../Page/马来西亚.md "wikilink")）的[東姑阿都拉曼](../Page/東姑阿都拉曼.md "wikilink")、以及印尼的[蘇卡諾](../Page/蘇卡諾.md "wikilink")。後來，由於陳智雄的關係，廖文毅也應邀參加了马来亚首相（[马来西亚首相](../Page/马来西亚首相.md "wikilink")）东姑阿都拉曼的就職大典。

後來，由於[中國共產黨的壓力](../Page/中國共產黨.md "wikilink")，親共的蘇卡諾不但阻止陳智雄在[東南亞所進行的外交工作](../Page/東南亞.md "wikilink")，並將其逮捕下獄。後來陳智雄在獄中寫信責罵蘇卡諾忘恩負義，蘇卡諾自知理虧，改以驅逐出境方式釋放陳智雄。\[21\]\[22\]\[23\]

當時，陳智雄決定赴日本與廖文毅會合，但是日本政府卻不讓無國籍的他下機入境。就這樣，陳智雄在[東京和印尼之間的飛機上往來好幾次](../Page/東京.md "wikilink")，直到有一位他在飛機上碰到的[瑞士官員出面](../Page/瑞士.md "wikilink")，安排他到瑞士去居住，並順利取得瑞士公民權以後，他才得以於1958年以瑞士公民身份前往日本，繼續獨立運動的推展。\[24\]\[25\]\[26\]

## 遭到处决

[1963_TAIWANESE_Diplomat_&_Martyr_for_TAIWAN_Independence_Movement_Chen_Chih-hsiung_臺灣外交官與獨立運動先驅_陳智雄.jpg](https://zh.wikipedia.org/wiki/File:1963_TAIWANESE_Diplomat_&_Martyr_for_TAIWAN_Independence_Movement_Chen_Chih-hsiung_臺灣外交官與獨立運動先驅_陳智雄.jpg "fig:1963_TAIWANESE_Diplomat_&_Martyr_for_TAIWAN_Independence_Movement_Chen_Chih-hsiung_臺灣外交官與獨立運動先驅_陳智雄.jpg")
然而，隨著台灣獨立運動在國際間的進展，國民黨也開始透過種種手段想要瓦解其勢力，並將相關運動者加以逮捕。1959年，透過[國民黨駐日單位特工人員的運作](../Page/國民黨.md "wikilink")，持有瑞士聯邦政府頒發證件的陳智雄終被情治人員綁架回台。\[27\]由於日本台獨運動者的強烈抗議，並向媒體新聞界公開陳智雄秘密被捕的實情，國民黨只好將其釋放，條件是要他安份守己，不可在台灣有任何反對政府或是從事台獨運動的言論或行為。

1961年底，陳智雄吸收蕭坤旺、戴村德等人組成「同心社」，擬以該組織推展獨立運動，但因往來信件遭調查局攔截而使同心社的同志被一網打盡。翌年8月，同心社的成員同時接到起訴書，陳智雄遭叛亂罪起訴。1963年5月28日，陳智雄為自己的政治信仰遭到槍決，為台灣獨立運動奉獻出自己的生命。當時與他一起被關在青島東路之軍法處看守所的[施明雄](../Page/施明雄.md "wikilink")，在近三十年後回憶那天的場景：

2013年被中國大陸列名北京西山[無名英雄廣場](../Page/無名英雄廣場.md "wikilink")\[28\]，台灣民間真相與和解促進會執行長[葉虹靈認為無名英雄廣場烈士牆上於考據有失嚴謹](../Page/葉虹靈.md "wikilink")，也與解密的檔案與研究顯有出入，連台獨主張極端對立者也被銘刻其上\[29\]\[30\]。

## 參見

  - [台灣獨立運動](../Page/台灣獨立運動.md "wikilink")
  - [台灣獨立運動相關條目列表](../Page/台灣獨立運動相關條目列表.md "wikilink")
  - [台灣共和國臨時政府](../Page/台灣共和國臨時政府.md "wikilink")
  - [廖文毅](../Page/廖文毅.md "wikilink")
  - [江炳興](../Page/江炳興.md "wikilink")

## 參考資料

## 參考文獻

  - 陳銘城，1992，《海外台獨運動四十年》。台北：自立晚報
  - 陳五郎撰文、吳宗德翻譯，2002，〈台灣獨立先烈，勇者的畫像：陳智雄〉
  - 林樹枝，1992，〈生是台灣人，死是台灣魂：台獨案第一個遭國民黨槍決的陳智雄〉，見林樹枝，《出土政治冤案》第一集，頁11-14。台北：作者發行
  - 施明雄，1998，〈視死如歸的台獨勇士：陳智雄〉，見施明雄，《白色恐怖黑暗時代台灣人受難史》，頁25-29。台北：前衛
  - 湯瑪仕，1998，《聽革命家的歌：革命家陳智雄傳》。台北：前衛
  - [楊碧川](../Page/楊碧川.md "wikilink")，1997，《台灣歷史詞典》。台北：前衛
  - [台灣民族英雄陳智雄烈士的生涯拼圖](http://www.peoplenews.tw/news/a5560e87-c129-4b00-8133-839a78d6eb69),
    [民報 (2014年)](../Page/民報_\(2014年\).md "wikilink"), 2016-01-16
  - [被殖民母國遺棄的空中浪人陳智雄](http://www.peoplenews.tw/news/e2a9297c-de41-48b5-9d92-cb8ccd2b37c4),
    [民報 (2014年)](../Page/民報_\(2014年\).md "wikilink"), 2016-01-22
  - [綁架陳智雄回台的秘密行動](http://www.peoplenews.tw/news/75261578-abc7-4e68-9f06-79207c230e08),
    [民報 (2014年)](../Page/民報_\(2014年\).md "wikilink"), 2016-01-29
  - [施明雄](../Page/施明雄.md "wikilink"),
    [我終生敬仰的陳智雄烈士](http://www.peoplenews.tw/news/07aa3157-115d-4aca-a0e0-0b4fb98cb5fd),
    [民報 (2014年)](../Page/民報_\(2014年\).md "wikilink"), 2016-02-09
  - [張智程 - 51年了，敬你，台灣獨立第一烈士「陳智雄」 - 想想Thinking
    Taiwan](http://www.thinkingtaiwan.com/content/2099)

[Category:臺灣獨立運動者](../Category/臺灣獨立運動者.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Category:台灣革命家](../Category/台灣革命家.md "wikilink")
[Category:被中華民國處決者](../Category/被中華民國處決者.md "wikilink")
[Category:印尼獨立運動人物](../Category/印尼獨立運動人物.md "wikilink")
[Category:被槍決的台灣人](../Category/被槍決的台灣人.md "wikilink")
[Category:被處決的瑞士人](../Category/被處決的瑞士人.md "wikilink")
[Category:歸化瑞士公民](../Category/歸化瑞士公民.md "wikilink")
[Category:東京外國語大學校友](../Category/東京外國語大學校友.md "wikilink")
[Category:屏東人](../Category/屏東人.md "wikilink")
[Category:閩南裔臺灣人](../Category/閩南裔臺灣人.md "wikilink")
[Z智雄](../Category/陈姓.md "wikilink")

1.  [紀念台獨第一烈士陳智雄
    就義50年記者會後記](http://www.228.net.tw/index.php?option=com_content&task=view&id=17611&Itemid=71),
    台灣 228 網站 - 信仰建國228 追思感恩台灣神, 2013-06-30

2.  劉重義,
    [接近完成的陳智雄烈士拼圖](http://blog.roodo.com/aurorahope/archives/25345850.html),
    極光電子報（[台灣教授協會](../Page/台灣教授協會.md "wikilink")）, July 2,2013

3.  [李筱峰](../Page/李筱峰.md "wikilink"),
    [台灣戒嚴時期政治案件的類型](http://www.jimlee.org.tw/article.jsp?b_id=24454&menu_id=4)
    , 2000/11/23

4.
5.  [莊孟學](../Page/莊孟學.md "wikilink"),
    [悼念台灣獨立建國先烈勇者陳智雄](https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnx0YWl3YW5zaG91aHVsaWFubWVuZ3xneDo0MWI1Njg3Y2MyYjhjNzJl),
    台灣守護聯盟周刊, 2012/5/31

6.  [台灣神陳智雄之女 Vonny
    Chen（陳雅芳）聖山巡禮](http://www.taiwantt.org.tw/tw/index.php?option=com_content&task=view&id=7237&Itemid=1),
    台灣大地文教基金會, 2013-06-26

7.  [50年前就義 高喊「台灣獨立萬歲」 第一烈士陳智雄
    獨派追思](http://www.libertytimes.com.tw/2013/new/jun/28/today-p9.htm)
    , [自由時報](../Page/自由時報.md "wikilink"), 2013-6-28

8.  [紀念台獨第一烈士陳智雄就義50年記者會‏](http://newtalk.tw/blog_read.php?oid=18714),
    [新頭殼](../Page/新頭殼.md "wikilink"), 2013-6-27

9.  柯維斯,
    [關於陳智雄被捕的國際法問題](http://blog.roodo.com/aurorahope/archives/25359074.html),
    極光電子報（[台灣教授協會](../Page/台灣教授協會.md "wikilink")）, July 9,2013

10.
11.
12.
13.
14.
15.
16.
17.
18.
19. 「台独」组织与人物，第162頁，杨立宪，九州出版社，2008-01-01

20. [臺灣早期政黨史略（一九○○─
    一九六○）](https://books.google.com.tw/books?id=HAaM_gm4-XsC&pg=PA156)，第156頁，陳正茂，秀威出版，2009-03-01

21.
22.
23.
24.
25.
26.
27.
28.

29.

30.