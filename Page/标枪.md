[Javelin_throw.jpg](https://zh.wikipedia.org/wiki/File:Javelin_throw.jpg "fig:Javelin_throw.jpg")

**标枪**是一種設計成主要用於投擲的輕型槍。大部分的標槍都是用手來投擲，但是用來讓標槍手可以投更長距離的投擲裝置也是存在的。

## 军事应用

标枪是人类历史上有据可靠的最早的远程兵器之一。从[原始社会开始](../Page/原始社会.md "wikilink")，它就被用作重要的[狩猎工具](../Page/狩猎.md "wikilink")。标枪一般由有[镖头和](../Page/镖头.md "wikilink")[枪杆组成](../Page/枪杆.md "wikilink")，有些装有起平衡作用的[尾翼](../Page/尾翼.md "wikilink")。镖头由金属打制而成，一般有[锥形和](../Page/锥形.md "wikilink")[长水滴形等形式](../Page/长水滴形.md "wikilink")，套装在枪杆上。枪杆通常用硬木、竹竿或金属制成的。在战场上，标枪常常与[盾牌配合使用](../Page/盾牌.md "wikilink")，以弥补近身武器的不足。最典型的例子便是在罗马军队中，步兵在冲锋前往往掷出标枪以扰乱敌方阵型，并打击敌方士气。随着[弓](../Page/弓.md "wikilink")[弩的出现](../Page/弩.md "wikilink")，标枪的使用开始减少，但是直到13世纪，标枪仍然是世界许多国家军队的制式装备。

### 标枪與矛

標槍與[矛是很接近的東西](../Page/矛.md "wikilink")，在西方兵器史上，标枪与矛有难以割离的密切关系。在早期的西方战场上，标枪一直被冠以「矛」的称呼。随着战争的需要，作为单纯的[投掷兵器的标枪才与作为](../Page/投掷兵器.md "wikilink")[近战兵器用的矛区分开来](../Page/近战兵器.md "wikilink")。然而直到现在，在西方人的眼中，标枪仍然被看作是与矛相类似的武器，以至于在[英语中](../Page/英语.md "wikilink")，标枪与矛至今共用同一个单词：[Spear](../Page/:en:Spear.md "wikilink")。但在運動上，擲標槍則會專稱為[Javelin
throw](../Page/:en:Javelin_throw.md "wikilink")。

## 体育运动

[古希腊时代](../Page/古希腊.md "wikilink")，在古代[奥林匹克运动中](../Page/奥林匹克运动.md "wikilink")，人们就已经开始将标枪助跑投远和原地投准作为竞技项目。在现代，擲标枪（）已经演变成为了一个[体育运动项目](../Page/体育.md "wikilink")。
[A_mens_and_womens_javelin.png](https://zh.wikipedia.org/wiki/File:A_mens_and_womens_javelin.png "fig:A_mens_and_womens_javelin.png")

## 參見

  - [矛](../Page/矛.md "wikilink")
  - [枪](../Page/枪.md "wikilink")
  - [擲標槍](../Page/擲標槍.md "wikilink")

[Category:槍](../Category/槍.md "wikilink")