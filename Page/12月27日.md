**12月27日**是[公历一年中的第](../Page/公历.md "wikilink")361天（[闰年第](../Page/闰年.md "wikilink")362天），离全年结束还有4天。

## 大事记

### 6世紀

  - [537年](../Page/537年.md "wikilink")：[圣索菲亚大教堂竣工](../Page/圣索菲亚大教堂.md "wikilink")。

### 12世紀

  - [1146年](../Page/1146年.md "wikilink")：[圣伯尔纳铎在](../Page/圣伯尔纳铎.md "wikilink")[施派尔劝说](../Page/施派尔.md "wikilink")[康拉德三世参加](../Page/康拉德三世_\(德意志\).md "wikilink")[第二次十字军东征](../Page/第二次十字军东征.md "wikilink")。

### 16世紀

  - [1521年](../Page/1521年.md "wikilink")：西班牙颁布。
  - [1594年](../Page/1594年.md "wikilink")：企图刺杀[亨利四世失败](../Page/亨利四世_\(法兰西\).md "wikilink")，两天后伏法。

### 17世紀

  - [1655年](../Page/1655年.md "wikilink")：在[第二次北方战争](../Page/第二次北方战争.md "wikilink")（[大洪水时代](../Page/大洪水时代.md "wikilink")）中，[琴斯托霍瓦的](../Page/琴斯托霍瓦.md "wikilink")[光明山修道院的修士成功击退了](../Page/光明山修道院.md "wikilink")。

### 18世紀

  - [1703年](../Page/1703年.md "wikilink")：英国和葡萄牙签订。

### 19世紀

  - [1831年](../Page/1831年.md "wikilink")：[英国博物学家](../Page/英国.md "wikilink")[查尔斯·达尔文搭乘](../Page/查尔斯·达尔文.md "wikilink")[小猎犬号离开](../Page/小猎犬号.md "wikilink")[普里茅斯](../Page/普里茅斯.md "wikilink")，开始五年的环球航行。

### 20世紀

  - [1922年](../Page/1922年.md "wikilink")：[大日本帝國海軍開始為剛完工的](../Page/大日本帝國海軍.md "wikilink")[鳳翔號航空母艦進行測試航行](../Page/鳳翔號航空母艦.md "wikilink")，成為世界上第一艘專門用作搭載艦載機的[航空母艦](../Page/航空母艦.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")：[中國共產黨第一次反圍剿戰爭开始](../Page/中國共產黨第一次反圍剿戰爭.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：29国在[美國签订](../Page/美國.md "wikilink")《[布雷顿森林协定](../Page/布雷顿森林协定.md "wikilink")》，[国际货币基金组织和](../Page/国际货币基金组织.md "wikilink")[世界银行成立](../Page/世界银行.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[希腊政府勒令](../Page/希腊.md "wikilink")[希腊共产党解散](../Page/希腊共产党.md "wikilink")，导致[希腊内战开始](../Page/希腊内战.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[荷兰女王](../Page/荷兰女王.md "wikilink")[朱丽安娜正式签署文件结束对东印度各岛的统治](../Page/朱麗安娜_\(荷蘭\).md "wikilink")，[印度尼西亚独立](../Page/印度尼西亚.md "wikilink")。
  - [1965年](../Page/1965年.md "wikilink")：[美國F](../Page/美國.md "wikilink")5A自由式戰鬥機售賣予[台灣](../Page/台灣.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：[中国宣布在世界上第一次人工合成结晶牛](../Page/中国.md "wikilink")[胰岛素](../Page/胰岛素.md "wikilink")。
  - [1968年](../Page/1968年.md "wikilink")：[阿波罗8号返回地球](../Page/阿波罗8号.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[香港](../Page/香港.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[塘福監獄](../Page/塘福.md "wikilink")500名犯人集體騷動。
  - [1979年](../Page/1979年.md "wikilink")：在[阿富汗战争中](../Page/阿富汗战争_\(1979年\).md "wikilink")，[苏联军队突然攻入](../Page/苏联.md "wikilink")[喀布尔](../Page/喀布尔.md "wikilink")，处决了[阿富汗人民民主党总书记](../Page/阿富汗人民民主党.md "wikilink")[哈菲佐拉·阿明](../Page/哈菲佐拉·阿明.md "wikilink")([333風暴行動](../Page/333風暴行動.md "wikilink"))。
  - [1984年](../Page/1984年.md "wikilink")：中国首支极地探险队在[南极半岛附近的](../Page/南极半岛.md "wikilink")[乔治王岛登陆](../Page/乔治王岛.md "wikilink")，开始建设[长城站](../Page/长城站.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[俄罗斯在](../Page/俄罗斯.md "wikilink")[联合国正式取代](../Page/联合国.md "wikilink")[苏联的席位](../Page/苏联.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：美國正式宣布给予中国永久正常贸易关系地位。
  - [2002年](../Page/2002年.md "wikilink")：中国实施的世界最大的水利工程[南水北调工程动工](../Page/南水北调工程.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")：[巴基斯坦前](../Page/巴基斯坦.md "wikilink")[總理](../Page/巴基斯坦總理.md "wikilink")[贝娜齐尔·布托於](../Page/貝娜齊爾·布托.md "wikilink")[拉瓦爾品第的競選集會上](../Page/拉瓦爾品第.md "wikilink")[遇刺身亡](../Page/貝娜齊爾·布托遇刺事件.md "wikilink")。
  - 2007年：在[姆瓦伊·齊貝吉當選成為](../Page/姆瓦伊·齊貝吉.md "wikilink")[肯亞總統後](../Page/肯亞總統.md "wikilink")，反對派[拉伊拉·奧廷加的支持者開始於](../Page/拉伊拉·奧廷加.md "wikilink")[蒙巴薩等地發起騷亂](../Page/蒙巴薩.md "wikilink")，之後更進一步演變成[2007-2008年肯尼亚危机](../Page/2007-2008年肯尼亚危机.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：[以色列突然以火箭針對位於](../Page/以色列.md "wikilink")[加薩走廊的](../Page/加薩走廊.md "wikilink")[巴勒斯坦武裝集團展開攻擊](../Page/巴勒斯坦.md "wikilink")，為期3周的[加薩戰爭爆發](../Page/2008年加沙戰爭.md "wikilink")。

## 出生

  - [1571年](../Page/1571年.md "wikilink")：[约翰内斯·开普勒](../Page/约翰内斯·开普勒.md "wikilink")，[德国](../Page/德国.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")、數學家，[克卜勒定律發現者](../Page/克卜勒定律.md "wikilink")（逝於[1630年](../Page/1630年.md "wikilink")）
  - [1654年](../Page/1654年.md "wikilink")：[雅各布·伯努利](../Page/雅各布·伯努利.md "wikilink")，[瑞士數學家](../Page/瑞士.md "wikilink")，[伯努利家族成員之一](../Page/伯努利家族.md "wikilink")（逝於[1705年](../Page/1705年.md "wikilink")）
  - [1714年](../Page/1714年.md "wikilink")：[喬治·懷特腓](../Page/喬治·懷特腓.md "wikilink")，[英國國教牧師和傳道人](../Page/英國國教.md "wikilink")，[衛理宗共同創始人](../Page/衛理宗.md "wikilink")（逝於[1770年](../Page/1770年.md "wikilink")）
  - [1715年](../Page/1715年.md "wikilink")：[菲利普·德·诺阿耶](../Page/菲利普·德·诺阿耶.md "wikilink")，[法國元帥](../Page/法國元帥.md "wikilink")（逝於[1794年](../Page/1794年.md "wikilink")）
  - [1717年](../Page/1717年.md "wikilink")：[庇护六世](../Page/庇护六世.md "wikilink")，[羅馬天主教](../Page/羅馬天主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")（逝於[1799年](../Page/1799年.md "wikilink")）
  - [1761年](../Page/1761年.md "wikilink")：[米哈伊尔·博格达诺维奇·巴克莱·德托利](../Page/米哈伊尔·博格达诺维奇·巴克莱·德托利.md "wikilink")，[俄羅斯元帥](../Page/俄羅斯.md "wikilink")（逝於[1818年](../Page/1818年.md "wikilink")）
  - [1797年](../Page/1797年.md "wikilink")：[米尔扎·迦利布](../Page/米尔扎·迦利布.md "wikilink")，[印度詩人](../Page/印度.md "wikilink")（逝於[1869年](../Page/1869年.md "wikilink")）
  - [1814年](../Page/1814年.md "wikilink")：[朱尔·西蒙](../Page/朱尔·西蒙.md "wikilink")，[法国第](../Page/法国.md "wikilink")38任[總理](../Page/法國總理.md "wikilink")（逝於[1896年](../Page/1896年.md "wikilink")）
  - [1822年](../Page/1822年.md "wikilink")：[路易·巴斯德](../Page/路易·巴斯德.md "wikilink")，法国[化学家](../Page/化学家.md "wikilink")、微生物学家（逝於[1895年](../Page/1895年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[薛岳](../Page/薛岳.md "wikilink")，[中華民國抗日将领](../Page/中華民國.md "wikilink")（逝於[1998年](../Page/1998年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[玛莲娜·迪特里茜](../Page/玛莲娜·迪特里茜.md "wikilink")，德裔[美國演員兼歌手](../Page/美國.md "wikilink")，[百年來最偉大的女演員第](../Page/AFI百年百大明星.md "wikilink")9名（逝於[1992年](../Page/1992年.md "wikilink")）
  - [1907年](../Page/1907年.md "wikilink")：[賽巴斯提安·哈夫納](../Page/賽巴斯提安·哈夫納.md "wikilink")，[德國記者及作家](../Page/德國.md "wikilink")（逝於[1999年](../Page/1999年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[威廉·麥斯特](../Page/威廉·麥斯特.md "wikilink")，美國婦科學醫師以及科學家，人類性行為研究科學家（逝於[2001年](../Page/2001年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：[紅線女](../Page/紅線女.md "wikilink")，[中国粵劇演员](../Page/中国.md "wikilink")（逝於[2013年](../Page/2013年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[浦偉士](../Page/浦偉士.md "wikilink")，[香港](../Page/香港.md "wikilink")、[英國銀行家](../Page/英國.md "wikilink")
  - [1934年](../Page/1934年.md "wikilink")：[拉里莎·拉特尼娜](../Page/拉里莎·拉特尼娜.md "wikilink")，前[蘇聯體操運動員](../Page/蘇聯.md "wikilink")，有「芭蕾舞王」之稱
  - [1943年](../Page/1943年.md "wikilink")：[优安·曼努埃尔·色拉特](../Page/优安·曼努埃尔·色拉特.md "wikilink")，[西班牙歌手](../Page/西班牙.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[黃西田](../Page/黃西田.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[埃內斯托·柴迪洛](../Page/埃內斯托·柴迪洛.md "wikilink")，第54任[墨西哥總統](../Page/墨西哥總統.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[林正英](../Page/林正英.md "wikilink")，[香港演员](../Page/香港.md "wikilink")（逝於[1997年](../Page/1997年.md "wikilink")）
  - [1958年](../Page/1958年.md "wikilink")：[沙希德·哈坎·阿巴西](../Page/沙希德·哈坎·阿巴西.md "wikilink")，巴基斯坦总理
  - [1960年](../Page/1960年.md "wikilink")：[車和娟](../Page/車和娟.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[加斯帕·诺](../Page/加斯帕·诺.md "wikilink")，法國電影導演
  - [1965年](../Page/1965年.md "wikilink")：[沙萊曼·罕](../Page/沙萊曼·罕.md "wikilink")，印度歌舞片演員
  - [1971年](../Page/1971年.md "wikilink")：[伍樂城](../Page/伍樂城.md "wikilink")，[香港音樂監製](../Page/香港.md "wikilink")、作曲人
  - 1971年：[DJ
    Tommy](../Page/DJ_Tommy.md "wikilink")，中文名張進偉，[香港音樂人](../Page/香港.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[凯文·奥利](../Page/凯文·奥利.md "wikilink")，前美國職業籃球運動員
  - [1974年](../Page/1974年.md "wikilink")：[折笠富美子](../Page/折笠富美子.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/日本配音員.md "wikilink")
  - 1974年：[岡政偉](../Page/岡政偉.md "wikilink")，[日裔美籍](../Page/亞裔美國人.md "wikilink")[演員和電腦特技師](../Page/演員.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[吳文遠](../Page/吳文遠.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[易小玲](../Page/易小玲.md "wikilink")，[馬尼拉人質事件生還人質](../Page/馬尼拉人質事件.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[大卫·邓恩](../Page/大卫·邓恩.md "wikilink")，[英格蘭足球運動員](../Page/英格蘭.md "wikilink")
  - 1979年：[卡森·帕尔默](../Page/卡森·帕尔默.md "wikilink")，美國美式足球四分衛
  - [1981年](../Page/1981年.md "wikilink")：[鄭少偉](../Page/鄭少偉.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - 1981年：[大卫·阿德斯马](../Page/大卫·阿德斯马.md "wikilink")，[美國職棒大聯盟紐約洋基中繼投手](../Page/美國職棒大聯盟.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[科爾·漢梅爾斯](../Page/科爾·漢梅爾斯.md "wikilink")，美國職棒大聯盟費城費城人投手
  - [1984年](../Page/1984年.md "wikilink")：[丁文琪](../Page/丁文琪.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - 1984年：[吉爾·西蒙](../Page/吉爾·西蒙.md "wikilink")，法國職業網球運動員
  - [1985年](../Page/1985年.md "wikilink")：[热罗姆·丹布罗西奥](../Page/热罗姆·丹布罗西奥.md "wikilink")，[比利時一級方程式錦標賽車手](../Page/比利時.md "wikilink")
  - 1985年：[吳依潔](../Page/吳依潔.md "wikilink")，台灣[新聞主播](../Page/新聞主播.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[玉澤演](../Page/玉澤演.md "wikilink")，韓國男子偶像組合2PM成員之一。
  - 1988年：[扎文·海因斯](../Page/扎文·海因斯.md "wikilink")，[牙買加裔英格蘭足球運動員](../Page/牙買加.md "wikilink")
  - 1988年：[娄艺潇](../Page/娄艺潇.md "wikilink")，[中国](../Page/中国.md "wikilink")[演員](../Page/演員.md "wikilink")
  - 1988年：[海莉·威廉斯](../Page/海莉·威廉斯.md "wikilink")，美國搖滾樂團[帕拉摩爾主唱兼鍵盤手](../Page/帕拉摩爾.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[王淑玲](../Page/王淑玲.md "wikilink")，香港女演員
  - 1989年：[內田真禮](../Page/內田真禮.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[米洛斯·拉奧歷](../Page/米洛斯·拉奧歷.md "wikilink")，[加拿大男子網球選手](../Page/加拿大.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[丹尼·威爾遜](../Page/丹尼·威爾遜.md "wikilink")，[蘇格蘭足球運動員](../Page/蘇格蘭.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[內田真由美](../Page/內田真由美.md "wikilink")，日本女演員，[AKB48前成員](../Page/AKB48.md "wikilink")
  - 1993年：[顏卓靈](../Page/顏卓靈.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - 1995年：[王凡](../Page/顏卓靈.md "wikilink")，中国男学生
  - [2000年](../Page/2000年.md "wikilink")：[吳兆絃](../Page/吳兆絃.md "wikilink")，台灣女演員

## 逝世

  - [232年](../Page/232年.md "wikilink")：[曹植](../Page/曹植.md "wikilink")，[中国文学家](../Page/中国.md "wikilink")（[192年出生](../Page/192年.md "wikilink")）
  - [683年](../Page/683年.md "wikilink")：[唐高宗李治](../Page/唐高宗.md "wikilink")，[唐朝皇帝](../Page/唐朝.md "wikilink")（[628年出生](../Page/628年.md "wikilink")）
  - [1923年](../Page/1923年.md "wikilink")：[居斯塔夫·埃菲爾](../Page/居斯塔夫·埃菲爾.md "wikilink")，[法国建築家](../Page/法国.md "wikilink")（[1832年出生](../Page/1832年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[樂蒂](../Page/樂蒂.md "wikilink")，[香港國語電影演員](../Page/香港.md "wikilink")（[1937年出生](../Page/1937年.md "wikilink")）
  - [1972年](../Page/1972年.md "wikilink")：[萊斯特·皮爾遜](../Page/萊斯特·皮爾遜.md "wikilink")，[加拿大前總理](../Page/加拿大.md "wikilink")（[1897年出生](../Page/1897年.md "wikilink")）
  - [1979年](../Page/1979年.md "wikilink")：[哈菲佐拉·阿明](../Page/哈菲佐拉·阿明.md "wikilink")，[阿富汗人民民主党总书记](../Page/阿富汗人民民主党.md "wikilink")、[阿富汗国家领导人](../Page/阿富汗.md "wikilink")，被入侵的[苏联军队杀死](../Page/苏联.md "wikilink")（[1929年出生](../Page/1929年.md "wikilink")）
  - [1987年](../Page/1987年.md "wikilink")：[路易·艾黎](../Page/路易·艾黎.md "wikilink")，[新西蘭裔中國社會活動家](../Page/新西蘭.md "wikilink")、作家（[1897年出生](../Page/1897年.md "wikilink")）
  - [2002年](../Page/2002年.md "wikilink")：[潘田](../Page/潘田.md "wikilink")，中國著名鐵路工程專家、原鐵道兵司令部副參謀長兼總工程師（[1921年出生](../Page/1921年.md "wikilink")）
  - [2003年](../Page/2003年.md "wikilink")：[英若誠](../Page/英若誠.md "wikilink")，中國著名[話劇](../Page/话剧.md "wikilink")、[電影演員](../Page/电影.md "wikilink")，[中國文化部前副部長](../Page/中华人民共和国文化部.md "wikilink")（[1929年出生](../Page/1929年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[贝娜齐尔·布托](../Page/贝娜齐尔·布托.md "wikilink")，[巴基斯坦前](../Page/巴基斯坦.md "wikilink")[總理](../Page/巴基斯坦总理.md "wikilink")（[1953年出生](../Page/1953年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[嘉莉·費雪](../Page/嘉莉·費雪.md "wikilink")，[美國演員](../Page/美國.md "wikilink")（[1956年出生](../Page/1956年.md "wikilink")）

## 节日、风俗习惯

  - 朝鲜民主主义人民共和国社会主义宪法节

<!-- end list -->

  - 12月27日建築師節