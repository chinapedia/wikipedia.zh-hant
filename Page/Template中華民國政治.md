<includeonly>} |heading1 = 法律制度 |list1title =
[中華民國憲法](中華民國憲法.md "wikilink") |list1name = 憲法
|list1 =

  - [中華民國憲法增修條文](中華民國憲法增修條文.md "wikilink")
  - [國民大會](國民大會.md "wikilink")

|list2title = [中華民國法律](中華民國法律.md "wikilink") |list2name = 法律 |list2 =

  - [民法](民法_\(中華民國\).md "wikilink")
  - [刑法](中華民國刑法.md "wikilink")

|content3 =

|heading4 =

<hr/>

中央政府 |list4title = [總統府](中華民國總統府.md "wikilink") |list4name = 總統 |list4 =

  - [總統](中華民國總統.md "wikilink")：[蔡英文](蔡英文.md "wikilink")
    [副總統](中華民國副總統.md "wikilink")：[陳建仁](陳建仁.md "wikilink")
    <hr/>
      - [中央研究院](中央研究院.md "wikilink")
      - [國史館](國史館.md "wikilink")
      - [國家安全會議](國家安全會議_\(中華民國\).md "wikilink")
          - [國家安全局](中華民國國家安全局.md "wikilink")

|list5title = [行政院](行政院.md "wikilink") |list5name =行政 |list5 =

  - [行政院院長](行政院院長.md "wikilink")：[蘇貞昌](蘇貞昌.md "wikilink")
    [行政院副院長](行政院副院長.md "wikilink")：[陳其邁](陳其邁.md "wikilink")
    <hr/>
      - [秘書長](行政院秘書長.md "wikilink")：[李孟諺](李孟諺.md "wikilink")
      - [政務委員](政務委員_\(中華民國\).md "wikilink")
      - [行政院發言人](行政院新聞傳播處.md "wikilink")
      - [行政院會議](行政院會議.md "wikilink")
  - [中華民國中央行政機關](中華民國中央行政機關.md "wikilink")

|list6title = [立法院](立法院.md "wikilink") |list6name =立法 |list6 =

  - [立法院院長](立法院院長.md "wikilink")：[蘇嘉全](蘇嘉全.md "wikilink")
    [立法院副院長](立法院副院長.md "wikilink")：[蔡其昌](蔡其昌.md "wikilink")
    <hr/>
      - [秘書長](立法院秘書長.md "wikilink")：[林志嘉](林志嘉.md "wikilink")
      - [立法委員](立法委員.md "wikilink")

|list7title = [司法院](司法院.md "wikilink") |list7name =司法 |list7 =

  - [司法院院長](司法院院長.md "wikilink")：[許宗力](許宗力.md "wikilink")
    [司法院副院長](司法院副院長.md "wikilink")：[蔡炯燉](蔡炯燉.md "wikilink")
    <hr/>
      - [司法院大法官](司法院大法官.md "wikilink")
          - [憲法法庭](憲法法庭.md "wikilink")
      - [中華民國最高法院](中華民國最高法院.md "wikilink")
      - [中華民國最高行政法院](中華民國最高行政法院.md "wikilink")
      - [公務員懲戒委員會](公務員懲戒委員會.md "wikilink")

|list8title = [考試院](考試院.md "wikilink") |list8name =考試 |list8 =

  - [考試院院長](考試院院長.md "wikilink")：[伍錦霖](伍錦霖.md "wikilink")
    [考試院副院長](考試院副院長.md "wikilink")：[李逸洋](李逸洋.md "wikilink")
    <hr/>
      - [考選部](考選部.md "wikilink")
      - [銓敘部](銓敘部.md "wikilink")
          - [退撫基金](退撫基金.md "wikilink")
      - [公務人員保障暨培訓委員會](公務人員保障暨培訓委員會.md "wikilink")

|list9title = [監察院](監察院.md "wikilink") |list9name =監察 |list9 =

  - [監察院院長](監察院院長.md "wikilink")：[張博雅](張博雅.md "wikilink")
    [監察院副院長](監察院副院長.md "wikilink")：[孫大川](孫大川.md "wikilink")
    <hr/>
      - [監察委員](監察委員.md "wikilink")
      - [中華民國審計部](中華民國審計部.md "wikilink")

|heading10 = 地方政府 |list10title =
[直轄市](直轄市_\(中華民國\).md "wikilink")、[省](省_\(中華民國\).md "wikilink")
|list10name =省市 |list10 =

|list11title =
[縣](縣_\(中華民國\).md "wikilink")、[市](市_\(中華民國\).md "wikilink")
|list11name = 縣市 |list11 =
[臺灣省轄](臺灣省.md "wikilink")[市](市_\(中華民國\).md "wikilink")：

[臺灣省轄](臺灣省.md "wikilink")[縣](縣_\(中華民國\).md "wikilink")：

[福建省轄](福建省_\(中華民國\).md "wikilink")[縣](縣_\(中華民國\).md "wikilink")：
|content12 =

|heading13 =

<hr/>

政治

|list14title = [政黨](中華民國政黨.md "wikilink") |list14name =政黨 |list14 =

  - [政黨列表](中華民國政黨列表.md "wikilink")

<!-- end list -->

  - [泛藍](泛藍.md "wikilink")：[中國國民黨](中國國民黨.md "wikilink")
      - [橘營](橘營.md "wikilink")：[親民黨](親民黨.md "wikilink")、[無黨團結聯盟](無黨團結聯盟.md "wikilink")
  - [泛綠](泛綠.md "wikilink")：[民主進步黨](民主進步黨.md "wikilink")、[時代力量](時代力量.md "wikilink")

|list15title = [選舉](臺灣選舉.md "wikilink") |list15name =選舉 |list15 =

  - 總統：[2008](2008年中華民國總統選舉.md "wikilink") -
    [2012](2012年中華民國總統選舉.md "wikilink") -
    [2016](2016年中華民國總統選舉.md "wikilink")
  - 立委：[2008](2008年中華民國立法委員選舉.md "wikilink") -
    [2012](2012年中華民國立法委員選舉.md "wikilink") -
    [2016](2016年中華民國立法委員選舉.md "wikilink")
  - 直轄：[2010](2010年中華民國直轄市公職人員選舉.md "wikilink") -
    [2014](2014年中華民國地方公職人員選舉.md "wikilink") -
    [2018](2018年中華民國地方公職人員選舉.md "wikilink")
  - 縣市：[2009](2009年中華民國縣市長、縣市議員暨鄉鎮市長選舉.md "wikilink") -
    [2014](2014年中華民國地方公職人員選舉.md "wikilink") -
    [2018](2018年中華民國地方公職人員選舉.md "wikilink")
  - [公民投票法](公民投票法.md "wikilink")：[全國性](中華民國全國性公民投票.md "wikilink") -
    [地方性](中華民國地方性公民投票.md "wikilink")
  - [臺灣罷免制度](臺灣罷免制度.md "wikilink")

|list16title = [政治議題](中華民國政治.md "wikilink") |list16name =政治 |list16 =

  - [中華民國國民](中華民國國民.md "wikilink")
      - [臺灣人權](臺灣人權.md "wikilink")
  - [中華民國公營事業](中華民國公營事業.md "wikilink")
  - [台灣政治](台灣政治.md "wikilink")

|heading17 = 國安 外交 |list17title = [中華民國外交](中華民國外交.md "wikilink")
|list17name =外交 |list17 =

  - [中華民國駐外機構列表](中華民國駐外機構列表.md "wikilink")
  - [中華民國護照](中華民國護照.md "wikilink")
  - [中華民國簽證政策](中華民國簽證政策.md "wikilink")

|list18title =
[中華民國軍事](中華民國軍事.md "wikilink") <sub>（[模板](Template:臺灣軍事.md "wikilink")）</sub>
|list18name =軍事 |list18 =

  - ![ROC_Ministry_of_National_Defense_Flag.svg](ROC_Ministry_of_National_Defense_Flag.svg
    "ROC_Ministry_of_National_Defense_Flag.svg")[中華民國國軍](中華民國國軍.md "wikilink")
      -
      -   - ![Republic_of_China_Marine_Corps_Flag.svg](Republic_of_China_Marine_Corps_Flag.svg
            "Republic_of_China_Marine_Corps_Flag.svg")
            [中華民國海軍陸戰隊](中華民國海軍陸戰隊.md "wikilink")

      -
      - ![Republic_of_China_Military_Police_Flag.svg](Republic_of_China_Military_Police_Flag.svg
        "Republic_of_China_Military_Police_Flag.svg")
        [中華民國憲兵](中華民國憲兵.md "wikilink")
  - ![Flag_of_the_Republic_of_China_Reserve_Command.svg](Flag_of_the_Republic_of_China_Reserve_Command.svg
    "Flag_of_the_Republic_of_China_Reserve_Command.svg")
    [中華民國國防部後備指揮部](國防部後備指揮部.md "wikilink")

|list19title = [海峽兩岸關係](海峽兩岸關係.md "wikilink") **·**
[臺灣統獨議題](臺灣統獨議題.md "wikilink") |list19name =統獨
|list19 =

  - [臺海現狀](臺海現狀.md "wikilink")
  - [中國统一](中國统一.md "wikilink")
  - [台灣獨立運動](台灣獨立運動.md "wikilink") **·** [台灣正名運動](台灣正名運動.md "wikilink")

|heading20 = 其他板模 |content20 =

|below =
}}</includeonly><noinclude>

</noinclude>

[中華民國政治導航模板](../Category/中華民國政治導航模板.md "wikilink")
[台灣政治導航模板](../Category/台灣政治導航模板.md "wikilink")
[Category:亞洲各國政治模板](../Category/亞洲各國政治模板.md "wikilink")