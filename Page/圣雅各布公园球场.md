**圣雅各布公园球场**（[德语](../Page/德语.md "wikilink")：****、[法语](../Page/法语.md "wikilink")：****、[意大利语](../Page/意大利语.md "wikilink")：****）位于[瑞士](../Page/瑞士.md "wikilink")[巴塞尔](../Page/巴塞尔.md "wikilink")，是该国最大的专业足球场，也是[巴塞尔足球俱乐部的主场](../Page/巴塞尔足球俱乐部.md "wikilink")，容量为42,500个座位。

2008年6月7日，[2008年欧洲足球锦标赛在圣雅各布公园球场开幕](../Page/2008年欧洲足球锦标赛.md "wikilink")。

2016年5月18日，[2016年歐霸盃決賽在圣雅各布公园球场舉行](../Page/2016年歐霸盃決賽.md "wikilink")。

## 外部链接

  - [WorldStadiums.com
    entry](https://web.archive.org/web/20080112224201/http://www.worldstadiums.com/stadium_pictures/europe/switzerland/nordwestschweiz/basel_st_jakob.shtml)
  - [Basel United
    Website](https://web.archive.org/web/20010520193819/http://www.baselunited.ch/)
  - [FC Basel Website](http://www.fcb.ch)

[Category:瑞士足球場](../Category/瑞士足球場.md "wikilink")
[Category:2001年完工體育場館](../Category/2001年完工體育場館.md "wikilink")