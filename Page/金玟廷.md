**金玟廷**\[1\]（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/演員.md "wikilink")，舊譯名為「金敏貞」。

## 演出作品

### 電視劇

  - 1994年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[天國的陌生人](../Page/天國的陌生人.md "wikilink")》
  - 1997年：[KBS](../Page/韓國放送公社.md "wikilink")《》
  - 1999年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[KAIST](../Page/KAIST_\(電視劇\).md "wikilink")》飾演
    金玟熙
  - 1999年：KBS《[歡樂時光](../Page/歡樂時光_\(電視劇\).md "wikilink")》
  - 2000年：MBC《[壞朋友](../Page/壞朋友_\(電視劇\).md "wikilink")》
  - 2001年：KBS《[Mina](../Page/Mina.md "wikilink")》
  - 2002年：SBS《[情敵](../Page/情敵_\(電視劇\).md "wikilink")》飾演 鄭彩姸
  - 2002年：KBS《[胡同裏的人們](../Page/胡同裏的人們.md "wikilink")》
  - 2003年：SBS《[酒國](../Page/酒國_\(電視劇\).md "wikilink")》
  - 2004年：MBC《[藍色愛爾蘭](../Page/愛爾蘭_\(電視劇\).md "wikilink")》飾演 韓希英
  - 2005年：SBS《[流行70](../Page/流行70.md "wikilink")》飾演 高俊熙
  - 2006年：SBS《[比天國陌生](../Page/比天國陌生.md "wikilink")》
  - 2007年：MBC《[New Heart](../Page/赤子之心.md "wikilink")》飾演 南惠昔
  - 2009年：MBC《[2009外人球團](../Page/2009外人球團.md "wikilink")》飾演 崔妍智
  - 2011年：KBS《[荊棘鳥](../Page/荊棘鳥_\(電視劇\).md "wikilink")》飾演 韓柔京
  - 2012年：[tvN](../Page/tvN.md "wikilink")《[第三醫院](../Page/第三醫院.md "wikilink")》飾演
    陳慧仁
  - 2014年：tvN《[岬童夷](../Page/岬童夷.md "wikilink")》飾演 吳瑪莉亞
  - 2015年：KBS《[生意之神 - 客主2015](../Page/生意之神_-_客主2015.md "wikilink")》飾演
    梅月/海住
  - 2017年：[JTBC](../Page/JTBC.md "wikilink")《[Man to
    Man](../Page/Man_to_Man.md "wikilink")》飾演 車度荷
  - 2018年：tvN《[陽光先生](../Page/陽光先生.md "wikilink")》飾演 工藤陽花
  - 2019年：KBS《[各位國民](../Page/各位國民.md "wikilink")》飾演 朴厚子

### 電影

  - 1993年：《[Kid Cop](../Page/Kid_Cop.md "wikilink")》
  - 1998年：《[女高怪談](../Page/女高怪談.md "wikilink")》
  - 1998年：《[Zzang](../Page/Zzang.md "wikilink")》
  - 2001年：《[L'Abri](../Page/L'Abri.md "wikilink")》
  - 2004年：《[Flying Boys](../Page/Flying_Boys.md "wikilink")》
  - 2006年：《[淫亂書生](../Page/淫亂書生.md "wikilink")》
  - 2009年：《》(股神戰場)
  - 2012年：《[家門的榮光5：家門的歸還](../Page/家門的榮光5：家門的歸還.md "wikilink")》
  - 2013年：《[夜之女王](../Page/夜之女王.md "wikilink")》

### MV

  - 2010年：[徐仁國](../Page/徐仁國.md "wikilink")《LOVE U》

## 綜藝節目

  - 2004年11月20日：《[情書](../Page/情書.md "wikilink")》第5集
  - 2011年：《[Running Man](../Page/Running_Man.md "wikilink")》第50集
  - 2013年：《[Running Man](../Page/Running_Man.md "wikilink")》第167集
  - 2016年：O'live TV 《[Tasty Road](../Page/Tasty_Road.md "wikilink")
    S7》主持（與歌手[Yura](../Page/Yura.md "wikilink")，2月20日至10月20日）

## 參考

## 外部連結

  - [EPG](https://web.archive.org/web/20080116140857/http://epg.epg.co.kr/star/profile/index.asp?actor_id=1117)

  - [empas](http://people.empas.com/people/info/ki/mm/kimminjung/)

[K](../Category/韓國前兒童演員.md "wikilink")
[K](../Category/韓國電視演員.md "wikilink")
[K](../Category/韓國電影演員.md "wikilink")
[K](../Category/韓國天主教徒.md "wikilink")
[K](../Category/漢陽大學校友.md "wikilink")
[K](../Category/首爾特別市出身人物.md "wikilink")
[K](../Category/金姓.md "wikilink")

1.  漢字正寫名字：[新唐人電視台](http://www.ntdtv.co.kr/contents_view.asp?news_divide01=1000&news_divide02=2003&news_id=702)