[GilbertIslandsPos.png](https://zh.wikipedia.org/wiki/File:GilbertIslandsPos.png "fig:GilbertIslandsPos.png")的一部分\]\]
[Tarawa_map_w.jpg](https://zh.wikipedia.org/wiki/File:Tarawa_map_w.jpg "fig:Tarawa_map_w.jpg")
[Tarawa_Maiana_STS088-707-6.jpg](https://zh.wikipedia.org/wiki/File:Tarawa_Maiana_STS088-707-6.jpg "fig:Tarawa_Maiana_STS088-707-6.jpg")
**塔拉瓦**，是[基里巴斯的一组](../Page/基里巴斯.md "wikilink")[环礁](../Page/环礁.md "wikilink")，英國殖民時期和[吉爾伯特及埃里斯群島的首都](../Page/吉爾伯特及埃里斯群島.md "wikilink")，當中[南塔拉瓦是基里巴斯的首都](../Page/南塔拉瓦.md "wikilink")。塔拉瓦环礁是以[第二次世界大戰的](../Page/第二次世界大戰.md "wikilink")[塔拉瓦战役而聞名](../Page/塔拉瓦战役.md "wikilink")。

## 地理

塔拉瓦环礁由24个岛屿组成，其中8个有人居住，最大的島嶼南塔拉瓦，由[邦里基沿整個南側延伸至](../Page/邦里基.md "wikilink")[拜里基](../Page/拜里基.md "wikilink")，除了[貝蒂歐的潟湖](../Page/貝蒂歐.md "wikilink")。日本堤道連接拜里基至貝壽。最大城市是[帕埃尼尤和唯一的機場](../Page/帕埃尼尤.md "wikilink")－[邦里基國際機場](../Page/邦里基國際機場.md "wikilink")，均在南塔拉瓦。

### 小島

  - 阿巴尼亚（已於1999年淹沒）
  - [阿巴奧科羅](../Page/阿巴奧科羅.md "wikilink")
  - [阿巴塔奧](../Page/阿巴塔奧.md "wikilink")
  - [拜里基](../Page/拜里基.md "wikilink")
  - [貝壽](../Page/貝壽.md "wikilink")
  - [拜克曼島](../Page/拜克曼島.md "wikilink")（已於1999年淹沒）
  - [比克塔瓦](../Page/比克塔瓦.md "wikilink")
  - [邦里基](../Page/邦里基.md "wikilink")
  - [布阿里基](../Page/布阿里基.md "wikilink")

### 城鎮

  - [阿巴塔奧](../Page/阿巴塔奧.md "wikilink")
  - [拜里基](../Page/南塔拉瓦.md "wikilink")
  - [帕埃尼尤](../Page/帕埃尼尤.md "wikilink")
  - [布阿里基](../Page/布阿里基.md "wikilink")
  - [布奧塔](../Page/布奧塔.md "wikilink")
  - [艾塔](../Page/艾塔.md "wikilink")
  - [馬勒納努卡](../Page/馬勒納努卡.md "wikilink")
  - [塔博里奧](../Page/塔博里奧.md "wikilink")

## 人口

1990年的人口為28,802人，大部分為吉爾伯特人（[密克羅尼西亞人](../Page/密克羅尼西亞群島.md "wikilink")），這可能超過本身[環境承載力](../Page/環境承載力.md "wikilink")，但在[澳洲](../Page/澳洲.md "wikilink")、[日本和](../Page/日本.md "wikilink")[新西蘭的](../Page/新西蘭.md "wikilink")[援助下](../Page/外援.md "wikilink")，使島嶼沒有出現飢荒。

在1995年至2000年間，出現一股移民潮湧入南塔拉瓦，使城市化率增加至5.2%\[1\]。雖然移民潮在接下五年變定穩定，但在某些地區仍出現人口過多問題。人口過多問題引起政府對南塔拉瓦的不可持續發展的關注\[2\]，[湯安諾實行了不同措施](../Page/湯安諾.md "wikilink")，以紓緩該國的主要城市中心的問題，包括在島嶼不同的地方興建發展，以紓緩環礁中心的擠迫問題\[3\]。

## 行政區劃

塔拉瓦环礁可分為三個行政區：

  - 泰奈納諾市政局：南塔拉瓦
  - 貝壽鎮議會：貝壽島
  - 北塔拉瓦：邦里基以東的所有島嶼

基里巴斯的主要行政區是南塔拉瓦的邦里基，國會位於昂波島，部份行政機構在貝壽島、帕埃尼尤和[聖誕島](../Page/聖誕島_\(吉里巴斯\).md "wikilink")。

## 歷史

[Battle_Tarawa.jpg](https://zh.wikipedia.org/wiki/File:Battle_Tarawa.jpg "fig:Battle_Tarawa.jpg")

[東印度公司的夏洛特號船長](../Page/東印度公司.md "wikilink")[托馬斯·吉爾伯特在](../Page/托馬斯·吉爾伯特.md "wikilink")1788年6月20日到達塔拉瓦\[4\]，是首位描述塔拉瓦的歐洲人，他的草圖保存至今天。

[亞瑟·格林布尔爵士在](../Page/亞瑟·格林布尔.md "wikilink")1913年至1919年間為塔拉瓦的見習行政人員\[5\]，並在1926年成為吉爾伯特及埃里斯群島属地代表\[6\]
。

[二战期间](../Page/二战.md "wikilink")，塔拉瓦环礁被[日本占领](../Page/日本.md "wikilink")。1943年11月20日，[美国发动残酷的](../Page/美国.md "wikilink")[塔拉瓦战役](../Page/塔拉瓦战役.md "wikilink")，日軍利用地形優勢使[美國海軍陸戰隊蒙受巨大的傷亡](../Page/美國海軍陸戰隊.md "wikilink")，3日后攻占该岛，雙方共約6,000人陣亡。

## 經濟

貝蒂歐島是[椰子核和](../Page/椰子核.md "wikilink")[珍珠的主要出口港](../Page/珍珠.md "wikilink")。南太平洋船務公司是位於塔拉瓦的船員機構，約950名海員在世界各地替南太平洋船務公司工作。

基里巴斯的貨幣是[澳元](../Page/澳元.md "wikilink")。

## 紀念

[塔拉瓦號是首艘LHA級](../Page/塔拉瓦號.md "wikilink")[兩棲突擊艦](../Page/兩棲突擊艦.md "wikilink")。

## 參考資料

## 外部連結

  - [Tarawa on The Web - A History of the Bloodiest
    Battle](http://tarawaontheweb.org/)
  - [The Marine Assault of
    Tarawa](http://www.ibiblio.org/hyperwar/USMC/USMC-C-Tarawa/)
  - [Tarawa the Aftermath](http://tarawatheaftermath.com/)
  - ["Tarawa" the USCG
    cat](http://www.uscg.mil/history/uscghist/Mascots_2.asp)
  - [Exhibit: The Alfred Agate Collection: The United States Exploring
    Expedition, 1838-1842](https://web.archive.org/web/20120114110615/http://www.history.navy.mil/ac/exploration/wilkes/wilkes.html)
    from the Navy Art Gallery

[Category:基里巴斯环礁](../Category/基里巴斯环礁.md "wikilink")

1.
2.
3.
4.
5.
6.