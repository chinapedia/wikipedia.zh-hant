**庐阳区**为[中國](../Page/中國.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[合肥市下辖的一个区](../Page/合肥市.md "wikilink")，位于合肥市北部。以合肥老城区为主体，向西北延伸与[长丰县](../Page/长丰县.md "wikilink")、[肥西县接壤](../Page/肥西县.md "wikilink")。面积139.32平方千米，常住人口60
9239（2010年11月1日第六次全国人口普查）。作为省会城市的核心区，庐阳区是全省政治、经济、文化、金融中心。庐阳区政府驻地为合肥市濉溪路295号。

## 行政区划

下辖9个[街道办事处](../Page/街道办事处.md "wikilink")、1个[镇](../Page/行政建制镇.md "wikilink")、1个[乡](../Page/乡级行政区.md "wikilink")：

  - 街道：[逍遥津街道](../Page/逍遥津街道.md "wikilink")、[杏花村街道](../Page/杏花村街道_\(合肥市\).md "wikilink")、[杏林街道](../Page/杏林街道_\(合肥市\).md "wikilink")、[双岗街道](../Page/双岗街道.md "wikilink")、[亳州路街道](../Page/亳州路街道.md "wikilink")、[海棠街道](../Page/海棠街道.md "wikilink")、[三孝口街道](../Page/三孝口街道.md "wikilink")、[四里河街道](../Page/四里河街道.md "wikilink")、[林店街道](../Page/林店街道.md "wikilink")
  - 镇：[大杨镇](../Page/大杨镇_\(合肥市\).md "wikilink")
  - 乡：[三十岗乡](../Page/三十岗乡.md "wikilink")

## 旅游

  - [李鸿章故居](../Page/李鸿章故居.md "wikilink")
  - [逍遥津](../Page/逍遥津.md "wikilink")
  - [三国新城遗址公园](../Page/三国新城遗址公园.md "wikilink")

## 外部链接

  - [庐阳区官方网站](https://web.archive.org/web/20081218051425/http://www.ahhfly.gov.cn/)

## 参考文献

[合肥](../Page/category:安徽市辖区.md "wikilink")

[庐阳区](../Category/庐阳区.md "wikilink")
[区/市辖区](../Category/合肥区县市.md "wikilink")