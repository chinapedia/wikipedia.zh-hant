**乌干达总理**的职位在1962年[乌干达从](../Page/乌干达.md "wikilink")[英国独立后设立](../Page/大英帝国.md "wikilink")。1963年，总理[米尔顿·奥博特指定](../Page/米尔顿·奥博特.md "wikilink")[布干达国王](../Page/布干达.md "wikilink")[穆特萨二世为](../Page/穆特萨二世.md "wikilink")[总统](../Page/乌干达总统.md "wikilink")，乌干达成为共和制国家。1966年，奥博特废除宪法，自立为总统，直至1980年总理的职位才恢复。

## 乌干达总理列表

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>乌干达总理</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>姓名</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/贝内迪克托·基瓦努卡.md" title="wikilink">贝内迪克托·基瓦努卡</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/米尔顿·奥博特.md" title="wikilink">米尔顿·奥博特</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/奥泰马·阿里马迪.md" title="wikilink">奥泰马·阿里马迪</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/保罗·穆万加.md" title="wikilink">保罗·穆万加</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/亚伯拉罕·瓦利戈.md" title="wikilink">亚伯拉罕·瓦利戈</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/萨姆森·基塞卡.md" title="wikilink">萨姆森·基塞卡</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/乔治·科斯马斯·阿德耶博.md" title="wikilink">乔治·科斯马斯·阿德耶博</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/金图·穆索凯.md" title="wikilink">金图·穆索凯</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/阿波罗·恩西班比.md" title="wikilink">阿波罗·恩西班比</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/阿马马·姆巴巴齐.md" title="wikilink">阿马马·姆巴巴齐</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 参见

  - [乌干达总统](../Page/乌干达总统.md "wikilink")

[乌干达总理](../Category/乌干达总理.md "wikilink")
[Category:各国总理](../Category/各国总理.md "wikilink")
[Category:总理列表](../Category/总理列表.md "wikilink")