[HK_HongKongFlowerShow_2008.JPG](https://zh.wikipedia.org/wiki/File:HK_HongKongFlowerShow_2008.JPG "fig:HK_HongKongFlowerShow_2008.JPG")
[Fotothek_df_roe-neg_0006713_032_Ausgestellte_Textilien_auf_der_Leipziger_Herbstmesse_1954.jpg](https://zh.wikipedia.org/wiki/File:Fotothek_df_roe-neg_0006713_032_Ausgestellte_Textilien_auf_der_Leipziger_Herbstmesse_1954.jpg "fig:Fotothek_df_roe-neg_0006713_032_Ausgestellte_Textilien_auf_der_Leipziger_Herbstmesse_1954.jpg")
[2008TIBE_Day4_Hall1-2.jpg](https://zh.wikipedia.org/wiki/File:2008TIBE_Day4_Hall1-2.jpg "fig:2008TIBE_Day4_Hall1-2.jpg")\]\]
 **展覽會**（簡稱**展覽**）
是指陳列物品並於特定時間內供人參觀的集會，為一種大型[項目管理及](../Page/項目管理.md "wikilink")[社交活動](../Page/社交.md "wikilink")，其活動展示[商品](../Page/商品.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[公司](../Page/公司.md "wikilink")[組織形象及](../Page/组织_\(社会学\).md "wikilink")[服務](../Page/服務.md "wikilink")。
展覽會可能跟以前的[墟市有類同](../Page/墟市.md "wikilink")，它們都有定期、組織者、参予者、参觀者等，他們之間互動交換[資訊](../Page/資訊.md "wikilink")，達到認知、認同，甚至交易。
由於展覽會有多種多樣，所以可以派分稱為[展銷會](../Page/展銷會.md "wikilink")、[贸易展览会](../Page/贸易展览会.md "wikilink")、[博覽會](../Page/博覽會.md "wikilink")、[路演等](../Page/路演.md "wikilink")。

現代的展覽會中常見有参展商、主辦單位、表演嘉賓、贊助人、[工程](../Page/工程.md "wikilink")[承判商](../Page/承判商.md "wikilink")、[物流供應商](../Page/物流.md "wikilink")、[物業管理](../Page/物業管理.md "wikilink")、[展覽館業主等的互為關係](../Page/展覽館.md "wikilink")。
其中有[合約](../Page/合約.md "wikilink")、場租、攤位、参展商守則、按金及罰則等。

## 歷史

[市集某程度上是人類歷史早期的一種展覽會](../Page/市集.md "wikilink")。隨著19世紀[歐洲的](../Page/歐洲.md "wikilink")[商業主義抬頭](../Page/商業.md "wikilink")，形成了較大規模的現代展覽會。1851年[英國](../Page/英國.md "wikilink")[倫敦的首屆](../Page/倫敦.md "wikilink")[世界博覽會](../Page/世界博覽會.md "wikilink")，可算是首個現代展覽會。
[缩略图](https://zh.wikipedia.org/wiki/File:EXPO84_TOCHIGI.jpg "fig:缩略图")栃木博覽會\]\]

## 各類展覽會

  - [展銷會](../Page/展銷會.md "wikilink")
  - [贸易展览会](../Page/贸易展览会.md "wikilink")（Trade fair）
  - [博覽會](../Page/博覽會.md "wikilink")（Expo）
  - [路演](../Page/路演.md "wikilink")（Roadshow）
  - [博物館](../Page/博物館.md "wikilink")（Museum）
  - [陳列室](../Page/陳列室.md "wikilink")（Showroom）
  - [美術館](../Page/美術館.md "wikilink")（Art museum）
  - [雙年展](../Page/雙年展.md "wikilink")

## 各国展览会

  - [香港展覽列表](../Page/香港展覽列表.md "wikilink")
  - [阿联酋展览会](https://web.archive.org/web/20081027005254/http://www.db1001.cn/zt/zhanlan.html)

## 相關

  - [場地](../Page/場地.md "wikilink")
  - [海報](../Page/海報.md "wikilink")
  - [場刊](../Page/場刊.md "wikilink")
  - [贈品](../Page/贈品.md "wikilink")
  - [商品目錄](../Page/商品目錄.md "wikilink")
  - [報價表](../Page/報價表.md "wikilink")
  - [公司名片](../Page/公司名片.md "wikilink")
  - [抽獎](../Page/抽獎.md "wikilink")
  - 大會[廣播](../Page/廣播.md "wikilink")
  - [講座](../Page/講座.md "wikilink")
  - [空間桁架](../Page/空間桁架.md "wikilink")
  - [折疊式空間桁架](../Page/折疊式空間桁架.md "wikilink")
  - [易拉寶](../Page/易拉寶.md "wikilink")

[Category:市場學](../Category/市場學.md "wikilink")
[Category:公關](../Category/公關.md "wikilink")
[Category:項目管理](../Category/項目管理.md "wikilink")
[展覽](../Category/展覽.md "wikilink")
[Category:博物館學](../Category/博物館學.md "wikilink")