**侯賽因戴伊勝利競技會**（**Nasr Athlétique de Hussein
Dey**；）是一支位于[阿尔及利亚首都](../Page/阿尔及利亚.md "wikilink")[阿尔及尔](../Page/阿尔及尔.md "wikilink")[侯赛因德尔区的足球俱乐部](../Page/侯赛因德尔.md "wikilink")。球队创建于1947年。俱乐部主体育场为席奥伊兄弟体育场（Stade
Frères Zioui）。

## 球队战绩

  - **[阿爾及利亞足球甲級聯賽](../Page/阿爾及利亞足球甲級聯賽.md "wikilink")**: 1

1967

  - **[阿尔及利亚盃](../Page/阿尔及利亚盃.md "wikilink")**: 1

1979

  - **[非洲优胜者盃](../Page/非洲优胜者盃.md "wikilink")**:

亚军：1978

## 外部链接

  - [Official website](http://www.nasria.com/)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")