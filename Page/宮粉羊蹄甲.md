[Bauhinia_variegata_MHNT.BOT.2011.3.22.jpg](https://zh.wikipedia.org/wiki/File:Bauhinia_variegata_MHNT.BOT.2011.3.22.jpg "fig:Bauhinia_variegata_MHNT.BOT.2011.3.22.jpg")

**宮粉羊蹄甲**（[学名](../Page/学名.md "wikilink")：）是一種[豆科](../Page/豆科.md "wikilink")[蘇木亞科](../Page/蘇木亞科.md "wikilink")[羊蹄甲屬的](../Page/羊蹄甲屬.md "wikilink")[有花植物](../Page/有花植物.md "wikilink")，為一種[落葉小](../Page/落葉.md "wikilink")[喬木](../Page/喬木.md "wikilink")，俗稱**蘭花木**、**印度櫻花**、**南洋櫻花**、**馬蹄豆**、**白花羊紫荊**；。原產於[中國南部](../Page/中國.md "wikilink")、[印度及](../Page/印度.md "wikilink")[馬來半島](../Page/馬來半島.md "wikilink")。宮粉羊蹄甲於每年3月至5月落葉後開花，花有5枚粉紅色的花瓣，其中1枚有深紅色條紋。而不少人將此花與同屬屬的[洋紫荊混淆](../Page/洋紫荊.md "wikilink")，主要分別在於後者開花期不同，洋紫荊的開花期為秋天（11月前後）並且在開花時葉茂盛。

花、花芽、嫩葉及幼果可供食用入藥，根皮煎劑可治療消化不良。

宮粉羊蹄甲經常與兩個同屬品種[紅花羊蹄甲](../Page/紅花羊蹄甲.md "wikilink")（）和[洋紫荊](../Page/洋紫荊.md "wikilink")（）混淆，詳見[羊蹄甲屬\#名稱混淆](../Page/羊蹄甲屬#名稱混淆.md "wikilink")。

## 形態特徵

宮粉羊蹄甲是[落葉小](../Page/落葉.md "wikilink")[喬木樹身可達](../Page/喬木.md "wikilink")7米高度，單葉互生，腎形2裂基部心形，[葉脈](../Page/葉脈.md "wikilink")11至13條，由葉基部呈放射形伸展，淺綠色葉面光滑。

總狀[花序頂生或腋生](../Page/花序.md "wikilink")，[花萼管狀](../Page/花萼.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")5片，粉紅或淡紫色，具有香氣，具發育[雄蕊](../Page/雄蕊.md "wikilink")5枚，花期由3月至5月。

## 參考文獻

  - 《樹影花蹤－九龍公園樹木研習徑》，香港園藝學會 著，天地圖書 出版，2005年4月。28-32頁。ISBN 988211147-5。

  -
[Category:羊蹄甲属](../Category/羊蹄甲属.md "wikilink")