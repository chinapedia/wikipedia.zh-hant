**瀧川事件**，又名**京大事件**，是昭和8年（1933年）發生於[京都帝国大学的一起压制思想的事件](../Page/京都帝国大学.md "wikilink")。

## 經緯

事件的起因是，京都帝国大学法学部教授[泷川幸辰](../Page/泷川幸辰.md "wikilink")（后为京大校长）在其著作《[刑法讲义](../Page/刑法.md "wikilink")》、《刑法读本》中，对于“[通奸罪](../Page/通奸罪.md "wikilink")”只适用于妻子一方的[日本法律提出批判](../Page/日本法律.md "wikilink")。泷川的这一主张，遭到了[贵族院议员](../Page/贵族院.md "wikilink")[菊池武夫和](../Page/菊池武夫.md "wikilink")[政友会](../Page/政友会.md "wikilink")[宫泽裕等人的攻击](../Page/宫泽裕.md "wikilink")，被指责为[共产主义的学说](../Page/共产主义.md "wikilink")，因此上述著作於1933年4月被[內務省禁止发售](../Page/內務省.md "wikilink")。[斋藤实](../Page/斋藤实.md "wikilink")[内阁的](../Page/内阁.md "wikilink")[文部大臣](../Page/文部大臣.md "wikilink")[鸠山一郎甚至对京大校长](../Page/鸠山一郎.md "wikilink")[小西重直提出罢免泷川的要求](../Page/小西重直.md "wikilink")，小西校长拒绝；但根据[文官分限令](../Page/文官分限令.md "wikilink")，泷川仍被迫停职。

面对这个事态，京大法学部全体31位教授提出辞职以表抗议，但是未得到京大当局和其他学部的支持。接着，小西校长也被迫辞职，随着强硬派的新任校长[松井元兴](../Page/松井元兴.md "wikilink")（理学博士，后为[立命馆大学校长](../Page/立命馆大学.md "wikilink")）的上任，事件迅速得到了平息。最终导致了泷川教授被免职，[佐佐木惣一](../Page/佐佐木惣一.md "wikilink")（后为立命馆大学校长）、[宫本英雄](../Page/宫本英雄.md "wikilink")、[森口繁治](../Page/森口繁治.md "wikilink")、[末川博](../Page/末川博.md "wikilink")（后为立命馆大学校长、总长）等教授也被免职，其余14名教官辞职的结果。

泷川事件导致京都帝国大学的17名原任教官辭職，並轉往立命館大学擔任教授及助教。而這件事件亦成為了[黑澤明監製的](../Page/黑澤明.md "wikilink")[第二次世界大戰後第](../Page/第二次世界大戰.md "wikilink")1部電影《[青春無悔](../Page/我於青春無悔.md "wikilink")》（）的背景。

## 關連書籍

  - [伊藤孝夫](../Page/伊藤孝夫.md "wikilink") 『瀧川幸辰；汝の道を歩め』（日本評伝選）
    [ミネルヴァ書房](../Page/ミネルヴァ書房.md "wikilink")、2003年 ISBN
    4623039072
  - 世界思想社編集部（編） 『滝川事件 記録と資料』 [世界思想社](../Page/世界思想社.md "wikilink")、2001年
    ISBN 4790708837
  - [松尾尊兊](../Page/松尾尊兌.md "wikilink") 『滝川事件』
    [岩波現代文庫](../Page/岩波現代文庫.md "wikilink")、2005年
    ISBN 4006001363

[Category:戰前昭和時代](../Category/戰前昭和時代.md "wikilink")
[Category:京都大學](../Category/京都大學.md "wikilink")
[Category:言論自由](../Category/言論自由.md "wikilink")
[Category:1933年日本](../Category/1933年日本.md "wikilink")
[Category:日本高等教育史](../Category/日本高等教育史.md "wikilink")