**MADtv**
（[中文翻譯](../Page/中文.md "wikilink")：瘋電視）是[幽默](../Page/幽默.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")[MAD為基礎的](../Page/瘋狂雜誌.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")，於[FOX播放](../Page/FOX.md "wikilink")。MADtv經常以惡搞出現，例如惡搞蘋果公司等。該節目在1995年10月14日首次於電視廣播。2009年5月16日為它的最後一集。

## 外部連結

  - [MADtv星球](http://www.planetmadtv.com/)
  - [MADtv 論壇](http://www.planetmadtv.com/forum/)
  - [*MADtv* -
    官方網站](https://web.archive.org/web/20060305061102/http://www.madtv.com/)

[Category:美國電視節目](../Category/美國電視節目.md "wikilink")
[Category:讽刺类电视节目](../Category/讽刺类电视节目.md "wikilink")
[Category:美國綜藝節目](../Category/美國綜藝節目.md "wikilink")
[Category:戲仿作品](../Category/戲仿作品.md "wikilink")
[Category:真人動畫電視節目](../Category/真人動畫電視節目.md "wikilink")