[Polychaeta_anatomy_zh.svg](https://zh.wikipedia.org/wiki/File:Polychaeta_anatomy_zh.svg "fig:Polychaeta_anatomy_zh.svg")
**多毛纲**（学名：）是[环节动物门下的一个](../Page/环节动物门.md "wikilink")[纲](../Page/纲_\(生物\).md "wikilink")，目前轄下有超過80個科\[1\]。

## 特点

这一纲的动物的身体一般呈圆柱状，或背部略扁，身体分为[口前叶](../Page/口前叶.md "wikilink")、一般体节部分和[尾节](../Page/尾节.md "wikilink")。其中体节上有成对的[刚毛](../Page/刚毛.md "wikilink")，[神经节和](../Page/神经节.md "wikilink")[后肾管](../Page/后肾管.md "wikilink")。[疣足](../Page/疣足.md "wikilink")（Parapodium）负责前进，甚至带有鳃组织。有组多毛纲动物还拥有发达的感官；封闭式的血管系统。发育过程历经螺旋卵裂和[担轮幼体](../Page/担轮幼体.md "wikilink")（Trochophora）阶段。繁殖能力强，通常为无性生殖，如[裂殖生殖](../Page/裂殖生殖.md "wikilink")（Schizogamy）。

这一类动物主要為雌雄异体。虽然也有浮游的，但是大多生活在海底中，即所谓的底栖。一些品种在海底表面生活，可以自由运动。另一些则在沉积物中掘地洞。还有一些多毛纲动物行[寄生生活](../Page/寄生.md "wikilink")。它们以微生物为食。目前有记录的约有10,000种左右，体型从300µm到3m长。是环节动物最大的一个纲。

## 分类

目前生物學界普遍認同現時的多毛綱仍是一個並系群，也就是指本綱還有部分物種目前被分類到其他分類元去，以致本綱物種的演化圖仍未完整。舉例說：[鬚腕動物門及](../Page/须腕动物门.md "wikilink")[螠虫动物门的物種現時發現原來都應該放在多毛綱之下](../Page/螠虫动物门.md "wikilink")。

，多毛綱物種在[WoRMS大致可分成以下四大類](../Page/WoRMS.md "wikilink")：

  - [螠蟲亞綱](../Page/螠蟲亞綱.md "wikilink")
    [Echiura](../Page/Echiura.md "wikilink")
      - [螠蟲目](../Page/螠蟲目.md "wikilink")（[Echiuroidea](../Page/Echiuroidea.md "wikilink")）
      - Echiura *[incertae sedis](../Page/地位未定.md "wikilink")*
  - [足刺亞綱](../Page/足刺亞綱.md "wikilink")
    [Errantia](../Page/Errantia.md "wikilink")
      - [仙蟲目](../Page/仙蟲目.md "wikilink")（[Amphinomida](../Page/Amphinomida.md "wikilink")）
      - [磯沙蠶目](../Page/磯沙蠶目.md "wikilink")（[Eunicida](../Page/Eunicida.md "wikilink")）
      - [葉鬚蟲目](../Page/葉鬚蟲目.md "wikilink")（[Phyllodocida](../Page/Phyllodocida.md "wikilink")）：有29個科，尚可細分成五個亞目\[2\]：
          - [鱗沙蠶亞目](../Page/鱗沙蠶亞目.md "wikilink")（[Aphroditiformia](../Page/Aphroditiformia.md "wikilink")）
          - [吻沙蠶亞目](../Page/吻沙蠶亞目.md "wikilink")（[Glyceriformia](../Page/Glyceriformia.md "wikilink")）及
          - [沙蠶亞目](../Page/沙蠶亞目.md "wikilink")（[Nereidiformia](../Page/Nereidiformia.md "wikilink")）
          - [葉鬚蟲亞目](../Page/葉鬚蟲亞目.md "wikilink")（[Phyllodociformia](../Page/Phyllodociformia.md "wikilink")）
          - Phyllodocida *[incertae sedis](../Page/地位未定.md "wikilink")*
  - Polychaeta *[incertae sedis](../Page/地位未定.md "wikilink")*
  - [隱居亞綱](../Page/隱居亞綱.md "wikilink")
    [Sedentaria](../Page/Sedentaria.md "wikilink")
      - [头结虫下纲](../Page/头结虫下纲.md "wikilink")（[Scolecida](../Page/Scolecida.md "wikilink")）：舊[头结虫亚纲](../Page/头结虫亚纲.md "wikilink")，又名[尖錐蟲目](../Page/尖錐蟲目.md "wikilink")。
          - [小头虫目](../Page/小头虫目.md "wikilink") Capitellida
      - [管觸鬚下綱](../Page/管觸鬚下綱.md "wikilink")（[Canalipalpata](../Page/Canalipalpata.md "wikilink")
        ）
          - [帚毛虫科](../Page/帚毛虫科.md "wikilink")（[Sabellariidae](../Page/Sabellariidae.md "wikilink")
            ）
          - [纓鰓蟲目](../Page/纓鰓蟲目.md "wikilink")（[Sabellida](../Page/Sabellida.md "wikilink")）：有六個科
          - [海稚虫目](../Page/海稚虫目.md "wikilink")（[Spionida](../Page/Spionida.md "wikilink")）：有一個亞目\[3\]
          - [蛰龙介目](../Page/蛰龙介目.md "wikilink")（[Terebellida](../Page/Terebellida.md "wikilink")\[4\]）
      - [Chaetopteridae科](../Page/Chaetopteridae.md "wikilink")

## 參考文獻

## 外部連結

  -
[多毛綱](../Category/多毛綱.md "wikilink")

1.
2.
3.
4.