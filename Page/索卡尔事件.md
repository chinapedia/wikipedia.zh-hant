**索卡尔事件**（），又稱索卡尔惡作劇（Sokal
hoax），發生於1996年，由[物理學家](../Page/物理學.md "wikilink")向[後現代主義學者的著名惡作劇](../Page/後現代主義.md "wikilink")。索卡尔向[文化研究](../Page/文化研究.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《》投了一篇[偽科學的文稿](../Page/偽科學.md "wikilink")，结果成功发表。

## 經過

索卡尔1955年生於紐約，二十一歲獲[哈佛大學學士](../Page/哈佛大學.md "wikilink")，二十六歲（1981年）獲[普林斯頓大學物理學博士](../Page/普林斯頓大學.md "wikilink")。

1996年，时任[紐約大學教授的](../Page/紐約大學.md "wikilink")[索卡尔向](../Page/索卡尔.md "wikilink")[文化研究](../Page/文化研究.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《社會文本》投稿一篇[偽科學的文章](../Page/偽科學.md "wikilink")，文題為《跨越界線：通往量子重力的轉換詮釋學》（*Transgressing
the Boundaries: Toward a Transformative Hermeneutics of Quantum
Gravity*）。

在《社會文本》刊出該文的同日，索卡尔在《》聲明該文是惡作劇，令出版《社會文本》的[杜克大學](../Page/杜克大學.md "wikilink")（Duke
University）[蒙羞](../Page/學術醜聞.md "wikilink")。索卡尔自謂其文是「[左翼暗號的雜燴](../Page/左翼.md "wikilink")、阿諛奉承式的參考、無關痛痒的引用、完完全全的胡扯」，說他用了學術界「在我能找到的範圍中，有關[數學和物理最愚昧的語錄](../Page/數學.md "wikilink")」。

因為該篇論文的成功發表，索卡尔認為《社會文本》欠缺嚴謹的審查，並「能發表一篇有關[量子物理的論文而沒有麻煩任何對這個範疇有認識的人](../Page/量子物理.md "wikilink")，感到很舒服」。

而《社會文本》的編輯群則說他們相信該文因為該文「是專業科學家熱切尋求後現代哲學認同他的範疇的建設」。社會文本甚至把索卡尔的文章刊在一個特別版中。

索卡尔認為重點在於期刊出版論文非基於該文的正確或合理與否，卻基於作者的銜頭：「我的目標不是為了保護科學免受文學評論者的蠻行，而是保護左翼免受它自身其中一個流行支派的影響……有過百個重要政治和經濟議題和科學和技術息息相關。[科學社會學在最好時已去闡明這些主題](../Page/科學社會學.md "wikilink")。但輕率的社會學如同輕率的科學，無用甚至有反效果。」

在[一個訪問](https://web.archive.org/web/20051024172451/http://zpedia.org/Physics_Professor_Parodies_Linguistic_Absurdities)中，索卡尔表示他是在讀過*Higher
Superstition: The Academic Left and Its Quarrels With
Science*後，而有意進行其實驗。

## 影響

1997年，索卡尔和Jean Bricmont合著了《知識的騙局》（*Impostures
Intellectuelles*），在美國出版時名為《》（*Fashionable
Nonsense: Postmodern Intellectuals' Abuse of
Science*）。索卡尔和Bricmont批評了[科學知識社會學中的](../Page/科學知識社會學.md "wikilink")[社會構造主義的](../Page/社會構造主義.md "wikilink")[強勢綱領](../Page/強勢綱領.md "wikilink")。

相對地，後現代主義者則認為「雖然他們對建設性的批評開放，但認為索卡尔欠缺了對他們的範疇的基本理解，因而其大部分反駁語無倫次，毫無用處。」科學社會學者[布鲁诺·拉图尔視整件事為茶杯裏的風波](../Page/布鲁诺·拉图尔.md "wikilink")。

## 参考文献

  - 李國偉. 〈都是索卡惹起的——科學與文化研究界的一次交鋒〉，《一條畫不清的界線》，pp. 92-97.
    台灣：新新聞文化事業股份有限公司出版，1999年.

## 外部链接

  - [Alan Sokal Articles on the "Social Text"
    Affair](http://www.physics.nyu.edu/faculty/sokal/index.html)

[Category:科學哲學](../Category/科學哲學.md "wikilink")
[Category:科学骗局](../Category/科学骗局.md "wikilink")
[Category:後現代主義](../Category/後現代主義.md "wikilink")
[Category:恶作剧](../Category/恶作剧.md "wikilink")
[Category:1996年科学](../Category/1996年科学.md "wikilink")
[Category:偽書](../Category/偽書.md "wikilink")