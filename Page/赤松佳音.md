**赤松佳音**，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")，[东京都出身](../Page/东京都.md "wikilink")。日本[国立音乐大学](../Page/国立音乐大学.md "wikilink")[声乐科毕业](../Page/声乐.md "wikilink")，同校[研究所音樂研究科声乐專修肄业](../Page/研究所.md "wikilink")。丈夫是知名[漫画家](../Page/漫画家.md "wikilink")[赤松健](../Page/赤松健.md "wikilink")。

## 逸事

  - 受赤松健影响，热衷于[Cosplay](../Page/Cosplay.md "wikilink")，因此成为一时话题。
  - 十九歲就讀國立音樂大學，自音大畢業後嫁給34歲的赤松健，兩人認識不到一年就結婚；而且赤松健是佳音的第一個男朋友，《[魔法老師](../Page/魔法老師.md "wikilink")》女主角[神樂坂明日菜是赤松健以佳音為藍本所設定的人物](../Page/神樂坂明日菜.md "wikilink")\[1\]。兩人的[結婚紀念日是](../Page/結婚紀念日.md "wikilink")7月13日，不過結婚登記日（入籍日）是四月一日（[愚人節](../Page/愚人節.md "wikilink")）。
  - [大小姐性格](../Page/大小姐.md "wikilink")，對於[電子產品很不在行](../Page/電子產品.md "wikilink")，只會拍[手機自拍照](../Page/手機.md "wikilink")，曾經把[日本電信電話的](../Page/日本電信電話.md "wikilink")[光纖](../Page/光纖.md "wikilink")[寬頻網路](../Page/寬頻網路.md "wikilink")「光網路」當成是「用『光』做動力的網路」。
  - 害怕[打雷](../Page/打雷.md "wikilink")，曾經有在半夜夢到打雷被嚇哭的經驗。
  - 喜好[Hello
    Kitty](../Page/Hello_Kitty.md "wikilink")，在自己的手機和電腦上面都貼滿了Hello
    Kitty的閃光貼紙。
  - 2008年1月25日，因為「想獲得自己的勳章」而在赤松健支持下成立了女性專屬的攝影工作室「Princess
    Doll」，以提供女性[歐洲宮廷式的Cosplay服和公主式的浪漫場景為號召](../Page/歐洲.md "wikilink")，并且自任[董事长](../Page/董事长.md "wikilink")。Princess
    Doll已於2009年8月31日結束營運。
  - Princess
    Doll開幕當周就被[久米田康治在](../Page/久米田康治.md "wikilink")《[絕望先生](../Page/絕望先生.md "wikilink")》裡面[吐槽了一下](../Page/吐槽.md "wikilink")。對於此事，佳音的回應是：「太感謝久米田老師了，那一格畫得好像我們花錢包下來（做宣傳）的一樣呢。」
  - 2009年10月，為日本網站《Men's
    Cyzo》首次拍攝[比基尼泳衣照片](../Page/比基尼泳衣.md "wikilink")\[2\]。

## 作品

  - [寫真集](../Page/寫真集.md "wikilink")

<!-- end list -->

  - 《》：[LEVEL-X](../Page/LEVEL-X.md "wikilink")（赤松健的[同人團體](../Page/同人團體.md "wikilink")）2007年8月19日發行。

<!-- end list -->

  - 单曲

<!-- end list -->

  - 《》：赤松佳音[作词](../Page/作词.md "wikilink")、Hirotaka
    Juni[作曲](../Page/作曲.md "wikilink")，[Melonbooks](../Page/Melonbooks.md "wikilink")2007年9月27日發行，商品番号「MONO-00001」。

## 参考資料

## 外部链接

  - [赤松佳音的blog](https://archive.is/20130430234949/ameblo.jp/kanon00/)

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本企业家](../Category/日本企业家.md "wikilink")
[Category:东京都出身人物](../Category/东京都出身人物.md "wikilink")
[Category:國立音樂大學校友](../Category/國立音樂大學校友.md "wikilink")

1.  [TBS系綜藝節目](../Page/TBS系.md "wikilink")《》2007年8月8日
2.