**港主制度**（；）是19世紀[馬來亞](../Page/馬來亞.md "wikilink")[柔佛的一種制度](../Page/柔佛.md "wikilink")，柔佛[貴族招攬](../Page/貴族.md "wikilink")[華人到](../Page/華人.md "wikilink")[港腳](../Page/港腳.md "wikilink")（兩河之間）開墾，稱[華人](../Page/華人.md "wikilink")[首領為](../Page/首領.md "wikilink")[港主](../Page/港主.md "wikilink")。

## 簡介

1830年代[英國力量在](../Page/英國.md "wikilink")[新加坡的經濟活動](../Page/新加坡.md "wikilink")，以及他們對附近[海盜的征剿](../Page/海盜.md "wikilink")，嚴重的損害了[柔佛王族的利益](../Page/柔佛.md "wikilink")，後者眼看國內商業衰退，不但人口減少，土地呈現荒蕪狀態，此外財源出現匱乏情況，此現象便鼓勵[華人前往墾荒](../Page/華人.md "wikilink")。

西元1833年（[清道光](../Page/清朝.md "wikilink")13年），[柔佛-廖内王朝實際統治者](../Page/柔佛苏丹王朝.md "wikilink")[天猛公達因依布拉欣](../Page/天猛公達因依布拉欣.md "wikilink")（Temenggung
Daeng Ibrahim,後被册封爲[拉惹天猛公敦達因依布拉欣](../Page/拉惹天猛公敦達因依布拉欣.md "wikilink")
Raja Temenggung Tun Daeng Ibrahim)創立“**港主制度**（Kang-Chu
System）”，當時柔佛王朝統治者召引大量[新加坡華人種植者移入](../Page/新加坡華人.md "wikilink")，掀起柔佛開墾拓荒之路，廣泛種植起[甘蜜](../Page/甘蜜.md "wikilink")（Gambir）與[胡椒](../Page/胡椒.md "wikilink")。至1880年代是柔佛甘蜜種植的高峰期，當時柔佛境內種植的甘蜜運往新加坡出口，產量位居世界第一位。

當一個[華裔種植者選擇一條河流邊上的荒地時](../Page/華裔.md "wikilink")，他便向統治者申請一份叫[港契](../Page/港契.md "wikilink")（Surat
Sungai）的准證，在這種准證裡，統治者給他一大片土地的保有權，它的範圍是在一條河的支流和另一條支流之間，支流流入主流的地方便是一個“港”，開港者稱為“**港主**”（Tuan
Sungai）。

柔佛港主制度創始於何時，尚待考查。根據英國殖民地官員A. E.
Coope的論述，最早的一份港契是1833年發給士姑來河的港主的。但這項資料恐有誤抄之處。實際上，根據Carl
A.
Trocki在70年代於柔佛檔案局的資料搜索，發現檔案局裡所保有的最早一份港契，是回曆1260年1月26日，即公元1844年10月9日發給[士姑來河](../Page/士姑來河.md "wikilink")
(Sungai Sekudai) 的港主Ah Chun 和Ban Seng。因此，推斷，柔佛的港主制度，應該是從這個時期開始的。

至於柔佛的第二張港契，是1844年10月22日發給港主[陳開順](../Page/陳開順.md "wikilink")，開發[地不佬河](../Page/地不佬河.md "wikilink")
(Sungai
Tebrau)沿岸的土地，即至今仍熟知的[陳厝港](../Page/陳厝港.md "wikilink")。1845年4月，柔佛天猛公又連續發出了3張港契，一張是巫來由河
(Sungai Melayu)的巫許後港港主\[1\]，兩張是發給地南河 (Sungai Tiram)的港主。

由上可知，柔佛天猛公發出的最早一份港契是在士姑來河沿岸，士姑來應是柔佛港主制度下最早開拓的一個港腳。1845年6月《新加坡自由西報》首次報導了柔佛開辟種植園的消息，該報說，當時柔佛有4條河地帶開始發展種植業，種植園有62個，包括士姑來河的20個，巫來由河的12個，登加河
(Sungai Danga) 15個，地不佬河3個，種植人總數約有500人之眾。

有證據顯示[柔佛的港主都是具有](../Page/柔佛.md "wikilink")[秘密會社背景的](../Page/秘密會社.md "wikilink")[華人](../Page/華人.md "wikilink")，他們有的獨資開發，有的合股經營，大家都以自己的店號或姓氏為港名，例如在[砂拉越開墾的](../Page/砂拉越.md "wikilink")[黃乃裳亦稱之為](../Page/黃乃裳.md "wikilink")“黃港主”。作為政府的代理人，港主必須負責處理“[港腳](../Page/港腳.md "wikilink")”—即殖民區的行政和保安工作。

他們所得到的報酬是五項[專利權](../Page/專利權.md "wikilink")，即**（一）經營公共[賭博](../Page/賭博.md "wikilink")，（二）經營[當鋪](../Page/當鋪.md "wikilink")，（三）販賣[酒類](../Page/酒類.md "wikilink")，（四）販賣[豬肉](../Page/豬肉.md "wikilink")，（五）販賣[鴉片](../Page/鴉片.md "wikilink")**。此外他們還可以抽取所有輸出的[甘蜜和](../Page/甘蜜.md "wikilink")[胡椒以及輸入米糧的佣金](../Page/胡椒.md "wikilink")。**港主**每年只需奉納一些[銀兩給](../Page/銀兩.md "wikilink")[蘇丹](../Page/苏丹_\(称谓\).md "wikilink")，每一個港腳都有許多居民，他們大都是港主的雇員，**港主**通常擁有一兩艘船隻，以便向[新加坡輸出土產和輸入日用品](../Page/新加坡.md "wikilink")。

溫士德博士（Dr.Winsted）在著作《柔佛史》中曾經提到1870年代，[華人已經在](../Page/華人.md "wikilink")[柔佛開發了](../Page/柔佛.md "wikilink")19個港，10年後，新開發的港又多了一倍之上，可見[柔佛的開發主要是依靠](../Page/柔佛.md "wikilink")[華人的勞力](../Page/華人.md "wikilink")。

## 參考資料

## 文獻來源

  - 《馬來西亞華人經濟地位之演變》，海外華人經濟叢書第五種，劉文榮，P.51-52。臺北：世華經濟出版社。
  - 參見Carl.A. Trocki, Prince of Pirates: The Temenggongs and the
    Development of Johor and Singapore 1784-1885, Singapore University
    Press, 1979, p.85-117
  - <http://forums.perak.org/cn/simple/t99434.html>

[category:柔佛](../Page/category:柔佛.md "wikilink")
[category:新加坡歷史](../Page/category:新加坡歷史.md "wikilink")
[category:馬來西亞歷史](../Page/category:馬來西亞歷史.md "wikilink")
[category:經濟史](../Page/category:經濟史.md "wikilink")
[category:新加坡经济](../Page/category:新加坡经济.md "wikilink")

[Category:大英帝国](../Category/大英帝国.md "wikilink")

1.  <http://forums.perak.org/cn/simple/t99434.html>