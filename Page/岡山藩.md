**岡山藩**（）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[江戶時代之間的](../Page/江戶時代.md "wikilink")[藩](../Page/藩.md "wikilink")，位於[日本](../Page/日本.md "wikilink")[備前國及](../Page/備前國.md "wikilink")[備中國部份](../Page/備中國.md "wikilink")，藩廳位於[岡山城](../Page/岡山城.md "wikilink")（今[岡山縣](../Page/岡山縣.md "wikilink")[岡山市](../Page/岡山市.md "wikilink")），支藩是[鴨方藩及](../Page/#鴨方藩.md "wikilink")[生坂藩](../Page/#生坂藩.md "wikilink")。[明治維新後](../Page/明治維新.md "wikilink")，在[廢藩置縣政策下](../Page/廢藩置縣.md "wikilink")，此藩被廢除，主要領地被改制為[岡山縣](../Page/岡山縣.md "wikilink")。

## 歷史

最初的藩主是[小早川秀秋](../Page/小早川秀秋.md "wikilink")，當時石高收入為51萬石。1602年秀秋過身，無子繼承而領地被沒收。1603年[姬路藩主](../Page/姬路藩.md "wikilink")[池田輝政二男](../Page/池田輝政.md "wikilink")[忠繼以](../Page/池田忠繼.md "wikilink")28萬石入封岡山藩。1632年池田氏第二任藩主[忠雄病逝後](../Page/池田忠雄.md "wikilink")，幕府不讓年幼的[光仲管理岡山藩](../Page/池田光仲.md "wikilink")，改封池田氏為[鳥取藩](../Page/鳥取藩.md "wikilink")。

後來由鳥取藩池田宗家的[光政繼承岡山藩](../Page/池田光政.md "wikilink")。光政與[德川光圀及](../Page/德川光圀.md "wikilink")[保科正之並列為江戶時代初期三大名君](../Page/保科正之.md "wikilink")。池田家統治到幕府為止。[廢藩置縣後成為了岡山縣的一部份](../Page/廢藩置縣.md "wikilink")。

## 藩主

### 小早川家

外樣　51万石　（1600年 - 1602年）

1.  [秀秋](../Page/小早川秀秋.md "wikilink")

### 池田家

外樣（準[親藩](../Page/親藩.md "wikilink")）　28万石→38万石→31万5千石　（1603年 - 1632年）

1.  [忠繼](../Page/池田忠繼.md "wikilink")
2.  [忠雄](../Page/池田忠雄.md "wikilink")

### 池田家〔宗家〕

外樣　31万5千石　（1632年 - 1871年）

1.  [光政](../Page/池田光政.md "wikilink")
2.  [綱政](../Page/池田綱政.md "wikilink")
3.  [繼政](../Page/池田繼政.md "wikilink")
4.  [宗政](../Page/池田宗政.md "wikilink")
5.  [治政](../Page/池田治政.md "wikilink")
6.  [齊政](../Page/池田齊政.md "wikilink")
7.  [齊敏](../Page/池田齊敏.md "wikilink")
8.  [慶政](../Page/池田慶政.md "wikilink")
9.  [茂政](../Page/池田茂政.md "wikilink")
10. [章政](../Page/池田章政.md "wikilink")

## 支藩

### 鴨方藩

位於備中国[淺口郡](../Page/淺口郡.md "wikilink")、[小田郡及](../Page/小田郡.md "wikilink")[窪屋郡](../Page/窪屋郡.md "wikilink")。1672年設立，初時名為岡山新田藩，1868年改名為鴨方藩。

#### 藩主

池田家 - 外樣 25,000石

1.  [政言](../Page/池田政言.md "wikilink")
2.  [政倚](../Page/池田政倚.md "wikilink")
3.  [政方](../Page/池田政方.md "wikilink")
4.  [政香](../Page/池田政香.md "wikilink")
5.  [政直](../Page/池田政直_\(鴨方藩主\).md "wikilink")
6.  [政養](../Page/池田政養.md "wikilink")
7.  [政共](../Page/池田政共.md "wikilink")
8.  [政善](../Page/池田政善.md "wikilink")
9.  [政詮](../Page/池田章政.md "wikilink")
10. [政保](../Page/池田政保.md "wikilink")

### 生坂藩

位於備中國[窪屋郡生坂](../Page/窪屋郡.md "wikilink")。1672年設立，石高1萬5千石。藩廳位於岡山城內。初時名為岡山新田藩。1868年改阿為生坂藩。

#### 藩主

1.  [輝錄](../Page/池田輝錄.md "wikilink")
2.  [政晴](../Page/池田政晴.md "wikilink")
3.  [政員](../Page/池田政員.md "wikilink")
4.  [政弼](../Page/池田政弼.md "wikilink")
5.  [政恭](../Page/池田政恭.md "wikilink")
6.  [政範](../Page/池田政範.md "wikilink")
7.  [政和](../Page/池田政和.md "wikilink")
8.  [政禮](../Page/池田政禮.md "wikilink")

## 參考資料

  - [岡山藩](http://www.asahi-net.or.jp/~me4k-skri/han/chugoku/okayama.html)

[Category:藩](../Category/藩.md "wikilink")
[Category:美濃池田氏](../Category/美濃池田氏.md "wikilink")
[Category:小早川氏](../Category/小早川氏.md "wikilink")
[Category:岡山藩](../Category/岡山藩.md "wikilink")
[Category:備前國](../Category/備前國.md "wikilink")
[Category:備中國](../Category/備中國.md "wikilink")