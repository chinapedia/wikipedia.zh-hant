**雷蒙·普恩加莱**（，），[法國政治家](../Page/法國.md "wikilink")。1912年－1913年擔任[法國總理和外交部長](../Page/法國總理.md "wikilink")；1913年－1920年擔任[法兰西第三共和国的](../Page/法兰西第三共和国.md "wikilink")[總統](../Page/法國總統.md "wikilink")；1922年－1924年與1926年－1929年再次出任總理。

## 家庭

普恩加莱生于巴勒迪克，其堂兄是著名的數學家[昂利·庞加莱](../Page/昂利·庞加莱.md "wikilink")。他在[巴黎大学学习法律](../Page/巴黎大学.md "wikilink")，1880年成为律师。

## 政坛生涯

### 初入政坛

  - 1887年－1903年擔任众议员。
  - 1893年、1895年任公共教育部长。
  - 1894年－1895年、1906年任财政部长。
  - 1903年－1913年擔任参议员。
  - 1909年當選[法蘭西學院院士](../Page/法蘭西學院.md "wikilink")。

### 总理和总统

  - 1912年－1913年任总理兼外长期间，确立法国对[摩洛哥的殖民统治](../Page/摩洛哥.md "wikilink")。
  - 1913年－1920年任总统期间，积极备战，促使内阁通过3年义务兵役法。

### [一战之后](../Page/一战.md "wikilink")

  - 1920年任赔款委员会主席，复任总理兼外长，主张对德采取强硬政策。
  - 1923年出兵鲁尔，迫使德国继续支付赔款。
  - 1924年在议会选举中失败。

### 晚年

  - 1926年取代E.[赫里欧](../Page/赫里欧.md "wikilink")，另组国民联盟政府，任总理兼财政部长。
  - 1929年因病退出政界。
  - 1934年10月15日卒于[巴黎](../Page/巴黎.md "wikilink")。

## 参考

[P](../Category/法国总统.md "wikilink") [P](../Category/法國總理.md "wikilink")
[P](../Category/法蘭西學院院士.md "wikilink")
[P](../Category/巴黎大學校友.md "wikilink")
[Category:路易大帝中学校友](../Category/路易大帝中学校友.md "wikilink")