**寧波公學**（，縮寫：NPC）為香港一所男女子中學，位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[觀塘區](../Page/觀塘區.md "wikilink")[功樂道](../Page/功樂道.md "wikilink")7號，佔地5000平方米，由寧波旅港同鄉會於1971年創立，現任校長為許善娟女士。

## 辦學宗旨

## 歷史

## 著名/傑出校友

  - [陳兆焯](../Page/陳兆焯.md "wikilink")：[正生書院校長](../Page/正生書院.md "wikilink")
  - [邱騰華](../Page/邱騰華.md "wikilink")：前[特首辦主任](../Page/特首辦.md "wikilink")，現任[商務及經濟發展局局長](../Page/商務及經濟發展局.md "wikilink")
  - [張文采](../Page/張文采.md "wikilink")：[無綫新聞台主播](../Page/無綫新聞台.md "wikilink")（預科班畢業）
  - 黃維揚：[香港浸會大學先進材料研究所副主任及化學系講座教授](../Page/香港浸會大學.md "wikilink")
  - 倫巧潣：[高銀金融首席財務總監](../Page/高銀金融.md "wikilink")\[1\][尤德爵士紀念基金高中學生獎 1992/93，1992年/1993年](https://www.wfsfaa.gov.hk/sfo/pdf/common/Form/sgl/seym2.pdf)</ref>
  - 張遠深：香港作家網創辦人\[2\] \[3\]
    、城市智庫時事評論員小組召集人\[4\]、港台青年創意聯會理事、就是敢言副秘書長\[5\]、前[光大新鴻基合規部助理副總裁](../Page/光大新鴻基.md "wikilink")

## 參考來源

## 外部連結

  - [寧波公學官方網站](http://www.npc.edu.hk)

[Category:觀塘](../Category/觀塘.md "wikilink")
[N](../Category/觀塘區中學.md "wikilink")

1.
2.  [香港01:
    搞小說網港青：有得做](https://www.hk01.com/%E7%A4%BE%E6%9C%83%E6%96%B0%E8%81%9E/132322/%E9%96%B1%E6%96%87%E6%8F%AD%E6%96%87%E5%AD%97%E6%9C%89%E5%83%B9-%E7%B6%B2%E7%B5%A1%E4%BD%9C%E5%AE%B6-%E6%B8%AF%E4%BA%BA%E4%B8%8D%E6%84%9B%E4%BB%98%E6%AC%BE-%E6%90%9E%E5%B0%8F%E8%AA%AA%E7%B6%B2%E6%B8%AF%E9%9D%92-%E6%9C%89%E5%BE%97%E5%81%9A)

3.  [明報:O2O﹕線上曝光「吸粉」
    爭取線下商機](https://ol.mingpao.com/ldy/cultureleisure/culture/20180120/1516385987652/o2o-%E7%B7%9A%E4%B8%8A%E6%9B%9D%E5%85%89%E3%80%8C%E5%90%B8%E7%B2%89%E3%80%8D-%E7%88%AD%E5%8F%96%E7%B7%9A%E4%B8%8B%E5%95%86%E6%A9%9F)

4.  [【就是敢言】曾鈺成與青年分享筆耕心得：叻因為勤力](http://www.orangenews.hk/news/system/2016/12/01/010046128.shtml)

5.  [「就是敢言」新書發布](http://writer.com.hk/article/view.php?id=20)