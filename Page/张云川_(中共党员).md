**张云川**（），[浙江](../Page/浙江省.md "wikilink")[东阳人](../Page/东阳市.md "wikilink")，1973年6月入党，1964年8月参加工作，[哈尔滨军事工程学院舰艇内燃机专业毕业](../Page/中国人民解放军军事工程学院.md "wikilink")，大学学历，工程师。现任第十一届[全国人大环境与资源保护委员会副主任委员](../Page/全国人民代表大会环境与资源保护委员会.md "wikilink")。[中共第十六屆](../Page/中国共产党第十六届中央委员会委员列表.md "wikilink")、[十七屆中央委員](../Page/中国共产党第十七届中央委员会委员列表.md "wikilink")。

张云川1970年毕业于[哈尔滨军事工程学院舰艇](../Page/中国人民解放军军事工程学院.md "wikilink")[内燃机专业](../Page/内燃机.md "wikilink")，后长期在[六机部](../Page/中国船舶工业总公司.md "wikilink")[江洲造船厂工作](../Page/江洲造船厂.md "wikilink")。1973年加入中国共产党。1985年任[江西省](../Page/江西省.md "wikilink")[九江市副市长](../Page/九江市.md "wikilink")，次年调任[赣州地区](../Page/赣州地区.md "wikilink")[行署专员](../Page/行政公署.md "wikilink")。1991年升任江西省省长助理，1993年升任[江西省人民政府副省长](../Page/江西省人民政府.md "wikilink")。1995年，调任[新疆维吾尔自治区人民政府副主席](../Page/新疆维吾尔自治区人民政府.md "wikilink")。1998年，张调任[湖南](../Page/湖南省.md "wikilink")，任[湖南省委副书记兼](../Page/中国共产党湖南省委员会.md "wikilink")[长沙市委书记](../Page/长沙市.md "wikilink")，2001年任[湖南省人民政府代省长](../Page/湖南省人民政府.md "wikilink")，次年正式当选为省长。

2003年3月，张云川调中央政府工作，出任[国防科学技术工业委员会主任](../Page/中华人民共和国国防科学技术工业委员会.md "wikilink")，2007年8月底，又调任中共河北省委书记。2008年1月当选为河北省人大常委会主任。2011年8月不再担任河北省委书记、常委、委员职务，任第十一届[全国人大环境与资源保护委员会副主任委员](../Page/全国人民代表大会环境与资源保护委员会.md "wikilink")。

## 参考文献

{{-}}

[Y](../Category/張姓.md "wikilink") [Z张](../Category/东阳人.md "wikilink")
[Category:中共长沙市委书记](../Category/中共长沙市委书记.md "wikilink")
[Category:中华人民共和国湖南省省长](../Category/中华人民共和国湖南省省长.md "wikilink")
[Category:中共河北省委书记](../Category/中共河北省委书记.md "wikilink")
[Z张](../Category/中华人民共和国政府官员.md "wikilink")
[Category:河北省人大常委会主任](../Category/河北省人大常委会主任.md "wikilink")
[Category:中国人民解放军军事工程学院校友](../Category/中国人民解放军军事工程学院校友.md "wikilink")
[Category:第十一届全国人大常委会委员](../Category/第十一届全国人大常委会委员.md "wikilink")
[Category:第十二届全国人大常委会委员](../Category/第十二届全国人大常委会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")