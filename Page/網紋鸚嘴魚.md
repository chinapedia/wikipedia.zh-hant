**網紋鸚嘴魚**，又名**黃鸚哥魚**，俗名鸚哥、青衣，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[鸚哥魚科的其中一](../Page/鸚哥魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[聖誕島](../Page/聖誕島.md "wikilink")、[萊恩群島](../Page/萊恩群島.md "wikilink")、[迪西島](../Page/迪西島.md "wikilink")、[日本南部](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國沿海](../Page/中國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[拉帕群島](../Page/拉帕群島.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[所羅門群島](../Page/所羅門群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[萬那杜](../Page/萬那杜.md "wikilink")、[法屬玻里尼西亞等海域](../Page/法屬玻里尼西亞.md "wikilink")。

## 深度

水深3至30公尺。

## 特徵

本魚體延長而略側扁。頭部輪廓呈平滑的弧型。開始型雌魚，體棕紅色，各鱗片具暗綠色。體各部份鰭條均為紅色。尾柄部為淡紅色。截形尾略帶雙凹形尾。終端型雄魚，體深藍綠色，各鱗片具不規則之橘紅色斑紋。齒板之外表面平滑，上齒板幾被上唇所覆蓋；大成魚之上齒板具0至2犬齒；每一上咽骨具1列臼齒狀之咽頭齒。頰部及尾柄部較淡色。背鰭、臀鰭和尾鰭具和體同色的寬斑縱紋。具雙凹型尾。背鰭硬棘9枚、背鰭軟條10枚、臀鰭硬棘3枚、臀鰭軟條9枚。體長可達40公分。

## 生態

本魚小魚生活於有珊瑚礁或圓石的礁湖中，大魚喜獨棲於珊瑚礁區離岸面之海域。

## 經濟利用

食用魚類，食用時宜用鹽巴鹽醃一至二小時再行煎時。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[frenatus](../Category/鸚嘴魚屬.md "wikilink")