**王寵惠**（），字**亮疇**，[廣東](../Page/廣東.md "wikilink")[東莞人](../Page/東莞.md "wikilink")，生於[香港](../Page/香港.md "wikilink")。[中华民国](../Page/中华民国.md "wikilink")[政治家](../Page/政治家.md "wikilink")、[外交家](../Page/外交家.md "wikilink")、[法学家](../Page/法学家.md "wikilink")。历任[外交总长](../Page/中華民國外交部.md "wikilink")、[司法总长](../Page/中華民國司法院.md "wikilink")、[國務總理](../Page/中華民國國務總理.md "wikilink")、代理[行政院院長](../Page/行政院院長.md "wikilink")、中華民國第一任[司法院院長](../Page/司法院院長.md "wikilink")、[中央研究院第一屆院士等职务](../Page/中央研究院第一屆院士.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Wang_Chonghui4.jpg "fig:缩略图")

## 生平

### 早年

[The_first_university_diploma_in_Modern_Chinese_History.jpg](https://zh.wikipedia.org/wiki/File:The_first_university_diploma_in_Modern_Chinese_History.jpg "fig:The_first_university_diploma_in_Modern_Chinese_History.jpg")的钦字第一号考凭\]\]

他出生于一个[基督教](../Page/基督教.md "wikilink")[牧師家庭](../Page/牧師.md "wikilink")，自幼学习[英文](../Page/英文.md "wikilink")，是香港[聖保羅書院校友](../Page/聖保羅書院.md "wikilink")。[香港大學首位華人教授](../Page/香港大學.md "wikilink")，病理學家[王寵益](../Page/王寵益.md "wikilink")（1888-1930）是其幼弟。1895年（[光緒](../Page/光緒.md "wikilink")21年），14岁时入[北洋大学堂二等學堂](../Page/北洋大学堂.md "wikilink")（今[天津大学前身](../Page/天津大学.md "wikilink")）法科预科。1900年（光緒26年）以第一名的成績畢業，获钦字第一号考凭，为中国第一个获得国内大学（或专科、大学预科）文凭者。此后他任[上海的](../Page/上海市.md "wikilink")[南洋公学教官](../Page/南洋公学.md "wikilink")。

1901年，他留学日本学习法律、政治。在日本，他发起成立[國民會](../Page/國民會.md "wikilink")，并任《国民報》英文记者，后来他还與[馮斯欒](../Page/馮斯欒.md "wikilink")、[鄭貫一](../Page/鄭貫一.md "wikilink")、[馮自由等發起組織](../Page/馮自由.md "wikilink")[廣東獨立協會](../Page/廣東獨立協會.md "wikilink")。1902年他留学[美國](../Page/美國.md "wikilink")，1905年於[耶鲁大学获](../Page/耶鲁大学.md "wikilink")[民法學博士](../Page/:en:Doctor_of_Civil_Law.md "wikilink")，留学美國期間，曾為[美國律師公會學報編輯](../Page/美國律師協會.md "wikilink")（Co-editor
of the "[Journal of the American Bar
Association](../Page/:en:ABA_Journal.md "wikilink")."）。此后他到欧洲研习法学，於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[中殿律師學院獲](../Page/中殿律師學院.md "wikilink")[英格蘭及威爾斯高等法院](../Page/英格蘭及威爾斯高等法院.md "wikilink")[大律師資格](../Page/大律師.md "wikilink")，成为[德国](../Page/德国.md "wikilink")[柏林比较法学会会员](../Page/柏林比较法学会.md "wikilink")。

1904年（光緒30年），他同訪問[美国](../Page/美国.md "wikilink")[纽约的](../Page/纽约.md "wikilink")[孫文](../Page/孫文.md "wikilink")（孫中山）会面，他协助孫文撰写了《中国问题的真解决》（*The
True Solution of Chinese
Question*）英文稿并发表。翌年，[中国同盟会成立](../Page/中国同盟会.md "wikilink")，王宠惠加入。1907年，他翻譯的（1896年版）德國民法在倫敦出版，\[1\]為英文譯本之首，該譯本不僅比曾留學德國的美籍學者Dr.
Walter
Lowery的譯本更早，其評論亦較佳。\[2\][浦薛鳳教授評王的译文](../Page/浦薛鳳.md "wikilink")「简洁而更为正式，合于法律条款之体裁，而在选字措词，更为确切明白，不容曲解误会。」「句斟字酌，意义明确，气韵平衡，流利自然。易词言之，每句每段，不特毫无洋人执笔之牵强痕迹，而且具有能文老手之高雅风格。」\[3\]
1911年（[宣統](../Page/宣統.md "wikilink")3年）9月，他归国，不久[辛亥革命爆发](../Page/辛亥革命.md "wikilink")，王任上海都督[陳其美的顧問](../Page/陳其美.md "wikilink")。在[南京召开的](../Page/南京市_\(中华民国\).md "wikilink")[各省都督府代表联合会会议中](../Page/各省都督府代表联合会.md "wikilink")，王作为广東代表出席，被推举为会議的副議長。

### 北京政府时期

1912年（[民国元年](../Page/民国紀元.md "wikilink")）1月[南京臨時政府成立](../Page/南京臨時政府.md "wikilink")，他被任命为外交总長。3月，[袁世凱任](../Page/袁世凱.md "wikilink")[中华民国臨時大总統](../Page/中华民国臨時大总統.md "wikilink")，组织[第一次唐紹儀内閣](../Page/第一次唐紹儀内閣.md "wikilink")，王转任司法总長。后来唐因同袁对立而辞任，王也随同辞職。此後，他任孫文創設的鉄路总公司顧問，并任[復旦大学副校長](../Page/復旦大学.md "wikilink")，研究憲法。1915年（民国4年），袁世凯称帝，企图引诱他支持，被他拒絶。

袁世凱死後，他赴[北京](../Page/北京市.md "wikilink")。1917年（民国6年），他就任[法律編纂会会長](../Page/法律編纂会.md "wikilink")。1920年（民国9年）他任[大理院院長](../Page/中华民国大理院.md "wikilink")、北京法官刑法委員会会長、法理委員会会長。1921年（民国10年）10月，他和[施肇基](../Page/施肇基.md "wikilink")、[顧維鈞作为北京政府全权代表](../Page/顧維鈞.md "wikilink")，出席了[华盛顿会議](../Page/华盛顿会議.md "wikilink")。

归国後的同年12月，他任[梁士詒内閣司法总長](../Page/梁士詒内閣.md "wikilink")。翌年5月，他和[胡適等人在](../Page/胡適.md "wikilink")《[努力周报](../Page/努力周报.md "wikilink")》上共同发表《我们的政治主張》一文，说明憲政的实行需要有好人组成政府（「好人政府」）。同年9月，他在[直系](../Page/直系.md "wikilink")[吴佩孚的支持下署理](../Page/吴佩孚.md "wikilink")[国務院总理](../Page/中华民国国务总理.md "wikilink")，组成[王宠惠内閣](../Page/王宠惠内閣.md "wikilink")，人称“好人政府”。当时直系内部「[津保派](../Page/津保派.md "wikilink")」的[曹锟同](../Page/曹锟.md "wikilink")「[洛派](../Page/洛派.md "wikilink")」的吴佩孚对立激化，王宠惠遭到津保派责难。同年11月，財政总長[羅文幹被总統](../Page/羅文幹.md "wikilink")[黎元洪命令逮捕](../Page/黎元洪.md "wikilink")，王宠惠内閣上台不久就倒台了。

此後，1923年（民国12年），他任[海牙](../Page/海牙.md "wikilink")[常设国际法院法官](../Page/常设国际法院.md "wikilink")。1924年（民国13年），他任[孫宝琦内閣司法总長](../Page/孫宝琦内閣.md "wikilink")。翌年，他任[修訂法律館总裁](../Page/修訂法律館.md "wikilink")。後来，[中国国民党](../Page/中国国民党.md "wikilink")[北伐机会高涨](../Page/北伐.md "wikilink")，他重回国民党方面，任中国国民党第二届中央監察委員（以後，他曾当选中国国民党第五届中央監察委員）。

### 国民党执政时期

1928年，他任[国民政府的首任](../Page/国民政府.md "wikilink")[司法院院長](../Page/中華民国司法院.md "wikilink")。翌年，他再度当选[国際联盟的](../Page/国際联盟.md "wikilink")[常设国际法院法官](../Page/常设国际法院.md "wikilink")。他留在国内，11月兼任内外債整理委員会委員長。1931年他奉[蒋介石之命](../Page/蒋介石.md "wikilink")，制定《[中華民国訓政時期約法](../Page/中華民国訓政時期約法.md "wikilink")》。其後，国民党内蒋介石同[胡漢民的对立激化](../Page/胡漢民.md "wikilink")，王寵惠为了回避，到[海牙就任](../Page/海牙.md "wikilink")[常设国际法院法官](../Page/常设国际法院.md "wikilink")。1936年，他辞去法官职务。1936年底，他回国，赞成[西安事变和平解决](../Page/西安事变.md "wikilink")。

1937年1月13日，中國國民黨中央常務會議補選[馬相伯](../Page/馬相伯.md "wikilink")、王寵惠為國民政府委員\[4\]。3月8日，外交部長王寵惠到部視事，演說稱：「國家之領土與主權必須保其完整，國際關係必須以平等互惠為基礎的原則下」，對日「循和平路線，力謀友誼之增進」\[5\]。4月1日，中國國民黨中常會決議，准蔣續假兩月，由外交部長王寵惠代理行政院長\[6\]。4月5日，王寵惠在中國國民黨中央紀念周宣布外交方針稱：「在平等互惠的原則之下，我們誠意的希望與各國增進國際關係。」\[7\]4月6日，行政院代院長王寵惠視事；秘書長[翁文灝出國期間](../Page/翁文灝.md "wikilink")，[魏道明暫代行政院秘書長](../Page/魏道明.md "wikilink")\[8\]。5月15日，英國駐華大使許閣森訪外交部長王寵惠，告知英日會談情形\[9\]。[中国抗日战争爆发前的在任期间](../Page/中国抗日战争.md "wikilink")，以[横滨正金銀行](../Page/横滨正金銀行.md "wikilink")[兒玉謙次为团長的日本实业家访问中国](../Page/兒玉謙次.md "wikilink")，同中国实业家会談。兒玉訪华团归国後，兒玉謙次向[日本外相](../Page/日本外相.md "wikilink")[佐藤尚武提出取消](../Page/佐藤尚武.md "wikilink")[冀東政权并废止冀東特殊貿易](../Page/冀东防共自治政府.md "wikilink")；兒玉訪华团成员[大日本製糖社長](../Page/大日本製糖.md "wikilink")[藤山愛一郎也向任](../Page/藤山愛一郎.md "wikilink")[大藏大臣的岳父](../Page/大藏大臣.md "wikilink")[結城豐太郎转达了信息](../Page/結城豐太郎.md "wikilink")。[淞沪会战爆发后的](../Page/淞沪会战.md "wikilink")1937年8月14日，王寵惠领导外交部放弃了被揶揄为「不抵抗政策」的對日宥和政策，转而发表抗日声明。1941年4月，王寵惠担任了抗日战争期间的国民政府外交舵手；此後，他改任[国防最高委員会秘書長](../Page/国防最高委員会.md "wikilink")。1943年11月，王寵惠随蒋介石出席[开罗会议](../Page/开罗会议.md "wikilink")。1945年，在[旧金山召开的](../Page/旧金山.md "wikilink")《[联合国憲章](../Page/联合国憲章.md "wikilink")》制定会議上，王寵惠作为[中华民國代表出席](../Page/中华民國.md "wikilink")。

1946年11月，王寵惠出席[制憲國民大會](../Page/制憲國民大會.md "wikilink")，參與《[中華民國憲法](../Page/中華民國憲法.md "wikilink")》的制訂工作。

1947年3月13日，中國國民黨中央常務委員會決議組織外交研究委員會，推定委員23人，計有王寵惠、孫科等，旨在研究當前中國外交問題\[10\]。同年，王寵惠以[廣東省第一高票](../Page/廣東省.md "wikilink")，在家鄉[東莞縣當選](../Page/東莞縣.md "wikilink")[第一屆國民大會代表](../Page/第一屆國民大會代表.md "wikilink")。

1948年，王寵惠当选[中央研究院院士](../Page/中央研究院院士.md "wikilink")；同年6月，蔣介石提名王寵惠為司法院院長，[石志泉為副院長](../Page/石志泉.md "wikilink")，獲[監察院投票同意](../Page/監察院.md "wikilink")\[11\]。翌年，[國府迁台](../Page/國府迁台.md "wikilink")，他经香港到[台湾](../Page/台湾.md "wikilink")。
1952年王寵惠先生任東吳大學董事長。
[TW_SCU_Wang_Memorial_Hall_20130928.jpg](https://zh.wikipedia.org/wiki/File:TW_SCU_Wang_Memorial_Hall_20130928.jpg "fig:TW_SCU_Wang_Memorial_Hall_20130928.jpg")
1958年3月15日，王寵惠在[台北病逝](../Page/台北市.md "wikilink")，享年78岁（満76歳）。王寵惠安葬於[東吳大學外雙溪校區後山墓園](../Page/東吳大學_\(台灣\).md "wikilink")\[12\]。東吳大學外雙溪校區行政大樓「寵惠堂」即是紀念王寵惠而得名。
[王寵惠墓碑.jpg](https://zh.wikipedia.org/wiki/File:王寵惠墓碑.jpg "fig:王寵惠墓碑.jpg")

## 著作

  - [謝瀛洲編](../Page/謝瀛洲.md "wikilink")：《困学斋文存》，中華叢書委員会，1957年。
  - 《憲法芻議》
  - 《譯德國民法》

## 家庭

第一任妻子杨兆良，生子[王大閎](../Page/王大閎.md "wikilink")；第二任妻子朱学勤。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 鄭則民《王寵惠》，

  - 邵桂花《羅文幹》，

  -
  -
## 延伸阅读

  - 刘宝东：[开罗会议上王宠惠为国争权益](http://www.humencn.com/wenhuashalong/newshow.asp?id=787&bigclass=%CF%E7%CF%CD%C1%D0%B4%AB)，《世纪》2009年03期

## 外部链接

## 参见

  - [王宠惠内阁](../Page/王宠惠内阁.md "wikilink")

{{-}}

<table>
<thead>
<tr class="header">
<th><p>（<a href="../Page/北京政府.md" title="wikilink">北京政府</a>）        </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>（<a href="../Page/国民政府.md" title="wikilink">国民政府</a>）     </p></td>
</tr>
</tbody>
</table>

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中华民国法學家](../Category/中华民国法學家.md "wikilink")
[Category:各省都督府代表聯合會代表](../Category/各省都督府代表聯合會代表.md "wikilink")
[Category:中華民國教育總長](../Category/中華民國教育總長.md "wikilink")
[Category:中華民國外交總長](../Category/中華民國外交總長.md "wikilink")
[Category:中華民國司法總長](../Category/中華民國司法總長.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:常設國際法院法官](../Category/常設國際法院法官.md "wikilink")
[Category:中華民國外交部部長](../Category/中華民國外交部部長.md "wikilink")
[Category:司法院院長](../Category/司法院院長.md "wikilink")
[Category:制憲國民大會代表](../Category/制憲國民大會代表.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:常设国际法院法官](../Category/常设国际法院法官.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:聖保羅書院校友](../Category/聖保羅書院校友.md "wikilink")
[Category:上海法政学院院长](../Category/上海法政学院院长.md "wikilink")
[Category:中国留美学生](../Category/中国留美学生.md "wikilink")
[Category:中國留日學生](../Category/中國留日學生.md "wikilink")
[Category:台灣戰後香港移民](../Category/台灣戰後香港移民.md "wikilink")
[Category:台灣戰後廣東移民](../Category/台灣戰後廣東移民.md "wikilink")
[Category:中華民國新教徒](../Category/中華民國新教徒.md "wikilink")
[Category:七七事变人物](../Category/七七事变人物.md "wikilink")
[Category:東莞人](../Category/東莞人.md "wikilink")
[Category:香港人](../Category/香港人.md "wikilink")
[C](../Category/王姓.md "wikilink")

1.  Wang, Chung Hui (1907). The German Civil Code, Translated and
    Annotated, with an Historical Introduction and Appendices. London:
    Stevens and Sons, Ltd.

2.  浦薛鳳:《忆王宠惠博士及其英译〈德国民法〉》,台湾《传记文学》第 38 卷第 3 期, 1981 年 3 月。

3.  浦薛风:《忆王宠惠博士及其英译〈德国民法〉》,台湾《传记文学》第 38 卷第 3 期, 1981 年 3 月。

4.

5.
6.
7.
8.
9.
10.
11.

12.