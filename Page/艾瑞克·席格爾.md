**艾瑞克·伍爾夫·席格爾**（，）是一位[美國](../Page/美國.md "wikilink")[猶太人](../Page/猶太人.md "wikilink")[作家](../Page/作家.md "wikilink")、[編劇以及](../Page/編劇.md "wikilink")[教育家](../Page/教育家.md "wikilink")，因寫下《[愛的故事](../Page/愛的故事.md "wikilink")》的劇本而聞名。

於美國[紐約](../Page/紐約.md "wikilink")[布魯克林出生](../Page/布魯克林.md "wikilink")。

## 劇本

  - 《[黃色潛水艇](../Page/黃色潛水艇.md "wikilink")》 (1968)
  - *[The Games](../Page/The_Games_\(1970_movie\).md "wikilink")* (1970)
  - *[R.P.M.](../Page/R.P.M..md "wikilink")* (1970)
  - 《[愛的故事](../Page/愛的故事.md "wikilink")》（Love Story） (1970)
  - *[Jennifer on My Mind](../Page/Jennifer_on_My_Mind.md "wikilink")*
    (1971)
  - 《[愛的故事續集](../Page/愛的故事續集.md "wikilink")》（Oliver's Story） (1978)
  - 《[變幻季節](../Page/變幻季節.md "wikilink")》（A Change of Seasons） (1980)
  - 《[巴黎來的私生子](../Page/巴黎來的私生子.md "wikilink")》（Man, Woman and Child）
    (1983)

## 著作

  - *[The Comedy of
    Plautus](../Page/The_Comedy_of_Plautus.md "wikilink")* (1968)

  - *[Love Story](../Page/Love_Story_\(novel\).md "wikilink")* (1970)

  - *[Fairy
    Tales](../Page/Fairy_Tales_\(Erich_Segal_book\).md "wikilink")*
    (1973)

  - *[Oliver's Story](../Page/Oliver's_Story.md "wikilink")* (1977)

  - *[Man, Woman and Child](../Page/Man,_Woman_and_Child.md "wikilink")*
    (1980)

  -
  - 《[同班同學](../Page/同班同學.md "wikilink")》 (1985)

  - *[Doctors](../Page/Doctors_\(novel\).md "wikilink")* (1988)

  - *[Acts of Faith](../Page/Acts_of_Faith.md "wikilink")* (1992)

  - *[Prizes](../Page/Prizes_\(novel\).md "wikilink")* (1995)

  - *[Only Love](../Page/Only_Love_\(TV\).md "wikilink")* (1998)

  - *[The Death of Comedy](../Page/The_Death_of_Comedy.md "wikilink")*
    (2001)

  - *[Oxford Readings in Greek
    Tragedy](../Page/Oxford_Readings_in_Greek_Tragedy.md "wikilink")*
    (2001)

  - *[Oxford Readings in Menander, Plautus, and
    Terence](../Page/Oxford_Readings_in_Menander,_Plautus,_and_Terence.md "wikilink")*
    (2002)

[Category:美國古典學學者](../Category/美國古典學學者.md "wikilink")
[Category:美國小說家](../Category/美國小說家.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:普林斯頓大學教師](../Category/普林斯頓大學教師.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:耶鲁大学教师](../Category/耶鲁大学教师.md "wikilink")