[Lorenzo_Costa_-_Un_concerto_(National_Gallery,_London).jpg](https://zh.wikipedia.org/wiki/File:Lorenzo_Costa_-_Un_concerto_\(National_Gallery,_London\).jpg "fig:Lorenzo_Costa_-_Un_concerto_(National_Gallery,_London).jpg")
**歌手**是對於歌曲和其他声乐作品演唱者的稱呼，也作為職業名使用。在[中國大陸則被定義為演員類](../Page/中國大陸.md "wikilink")\[1\]的**歌唱演員**\[2\]。那些符合[關注度和](../Page/關注度.md "wikilink")[知名度的歌手又被称作](../Page/知名度.md "wikilink")「**歌星**」。達到一定艺术造诣和成就者則尊称為「**歌唱家**（歌唱藝術家）」或樂隊的「**聲樂家**（聲樂藝術家）」。根據唱法，歌手又分為美聲、民族、流行等四大演唱派系。

## 美聲歌手

使用西洋[美聲唱法](../Page/美聲唱法.md "wikilink")（）的技術來進行歌唱表演的專家稱為「聲樂家」（Vocalist,
Opera Singer, Classical
Singer）。然而，「[美聲歌手](../Page/美聲歌手.md "wikilink")」一詞，大都被使用在[流行音樂中](../Page/流行音樂.md "wikilink")，並非[古典音樂的領域](../Page/古典音樂.md "wikilink")。「歌手」或「歌星」的稱呼用於[流行音樂之中](../Page/流行音樂.md "wikilink")，「聲樂家」一詞多用於[古典音樂的領域](../Page/古典音樂.md "wikilink")。

「聲樂家」來自於義大利語Cantante，原意指的就是用歌唱來當成職業的音樂家，但是後來被廣泛使用。於是，產生了另一個專用術語來稱呼聲樂家，就是義大利文的Vocalist，德文的Vokalist。\[3\]

在[西方古典音樂和](../Page/西方古典音樂.md "wikilink")[歌劇中](../Page/歌劇.md "wikilink")，[人聲](../Page/人聲.md "wikilink")（[聲樂](../Page/聲樂.md "wikilink")）也被作為[樂器的一種](../Page/樂器.md "wikilink")，可以按照[音域的高低將其](../Page/音域.md "wikilink")。人聲通常劃分為三大類。

  - 男聲：[閹人歌手](../Page/閹人歌手.md "wikilink")、[男性女高音歌手](../Page/男性女高音歌手.md "wikilink")、[男聲最高音歌手](../Page/男聲最高音歌手.md "wikilink")、[假聲男高音](../Page/假聲男高音.md "wikilink")、[男高音](../Page/男高音.md "wikilink")、[男中音](../Page/男中音.md "wikilink")、[低男中音](../Page/低男中音.md "wikilink")、[男低音](../Page/男低音.md "wikilink")

<!-- end list -->

  - 女聲：[女高音](../Page/女高音.md "wikilink")、[抒情女高音](../Page/抒情女高音.md "wikilink")、[戲劇女高音](../Page/戲劇女高音.md "wikilink")、[花腔女高音](../Page/花腔女高音.md "wikilink")、[女中音](../Page/女中音.md "wikilink")、[女低音](../Page/女低音.md "wikilink")

<!-- end list -->

  - 童聲

## 民族歌手

「民族歌手」一詞廣泛用於流行音樂領域，也就是說用[方言演唱的歌星或歌手可稱為](../Page/方言.md "wikilink")「民族歌手」。

「民族唱腔」或稱「民族唱法」皆正式被[兩岸三地的音樂學系以及音樂學院所使用](../Page/兩岸三地.md "wikilink")。使用「民族唱腔」或稱「民族唱法」的技術來進行歌唱表演的專家，不論使用[官話或](../Page/官話.md "wikilink")[方言來進行演唱皆可稱為](../Page/方言.md "wikilink")「歌唱家」。

## 流行音樂歌手

包括[流行樂歌手](../Page/流行_\(音樂類型\).md "wikilink")、[摇滚乐歌手](../Page/摇滚乐.md "wikilink")、[爵士乐歌手](../Page/爵士乐.md "wikilink")、[乡村音乐歌手等等](../Page/乡村音乐.md "wikilink")。

## 相關條目

  - [舞蹈演員](../Page/舞蹈演員.md "wikilink")
  - [電影電視演員](../Page/電影電視演員.md "wikilink")
  - [話劇演員](../Page/話劇演員.md "wikilink")
  - [一碟歌手](../Page/一碟歌手.md "wikilink")
  - [藝人](../Page/藝人.md "wikilink")

## 參考文獻

[fi:Laulu\#Laulaja](../Page/fi:Laulu#Laulaja.md "wikilink")

[藝人](../Category/藝人.md "wikilink")
[Category:声乐](../Category/声乐.md "wikilink")
[Category:文化及創意產業](../Category/文化及創意產業.md "wikilink")

1.
2.
3.  Un cantante è un musicista che usa la propria voce come strumento
    musicale e professionale per produrre una successione di suoni. Un
    cantante solista è colui che canta la melodia principale di un brano
    musicale. Un corista canta in un coro, o accompagna il solista con
    seconde voci o voci complementari. In questo ultimo caso il termine
    entrato nell'uso comune negli ambienti musicali diversi dalla musica
    classica è Vocalist.