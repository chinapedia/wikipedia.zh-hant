[Li_chengjiang.jpg](https://zh.wikipedia.org/wiki/File:Li_chengjiang.jpg "fig:Li_chengjiang.jpg")
**李成江**（），[中國](../Page/中華人民共和國.md "wikilink")[花樣滑冰單人滑小項運動員](../Page/花樣滑冰.md "wikilink")。

## 生涯

李成江[籍貫](../Page/籍貫.md "wikilink")[吉林](../Page/吉林省.md "wikilink")[長春](../Page/長春.md "wikilink")，父母都是國家隊中的[速度滑冰運動員](../Page/速度滑冰.md "wikilink")，其母親曾擔任過速度滑冰教練。李成江於4歲開始接受速度滑冰的訓練。当时条件艰苦设备简陋，花样滑冰运动员和速度滑冰运动员在长春的同一块冰场上训练。由于经常观看[陈露在冰场中央的训练](../Page/陈露.md "wikilink")，李成江於5歲時改為學習花樣滑冰，到12歲之時又再次接受速度滑冰的訓練，但不到一年就再次成為花樣滑冰運動員。

李成江的跳跃能力非常突出。在2001年国际滑联的四大洲锦标赛上，李成江成为第一位在比赛中采取单足起跳的标准动作并成功完成[后内四周跳的运动员](../Page/后内跳.md "wikilink")。他也能成功完成[后外点冰四周跳](../Page/后外点冰跳.md "wikilink")。

在李成江成為花樣滑冰運動員的第15年中，他贏得了男子單人滑的全國冠軍，翌年在四大洲花樣滑冰賽的男子單人滑小項中得到[亞軍](../Page/亞軍.md "wikilink")。2000年，李成江再次於四大洲花樣滑冰賽獲亞，並在[日本NHK杯中的男子單人滑得到季軍](../Page/日本.md "wikilink")，並且在首次參與的世界錦標賽中排名第5位，令人眼前一亮。

在世錦賽得到第5後的1年，李成江再次給人一個驚喜，他在四大洲花樣滑冰賽中首度奪得[冠軍](../Page/冠軍.md "wikilink")，但卻在世界錦標賽中名掛第7位。2002年，李成江在[鹽湖城冬季奧運中的花樣滑冰單人滑小項中得第](../Page/2002年冬季奧林匹克運動會.md "wikilink")9位。

2003年，李成江在[世界錦標賽中得第](../Page/世界.md "wikilink")4名，及後1年，先後在世錦賽得第5名、[十運會的男子個人滑小項奪](../Page/中華人民共和國第十屆運動會.md "wikilink")[金以及在世界花樣滑冰大獎賽中國站得第](../Page/金牌.md "wikilink")8。

2006年2月，李成江於[都靈冬季奧運的花樣滑冰個人滑中](../Page/2006年冬季奥林匹克运动会.md "wikilink")，排各第21，同年的花樣滑冰世錦賽上，李成江以229.03分排名第9位。翌年的[長春冬亞運](../Page/2007年亞洲冬季運動會.md "wikilink")，李成江取得了男子單人滑小項的[銀牌](../Page/銀牌.md "wikilink")。

[L李](../Category/长春籍运动员.md "wikilink")
[L李](../Category/中國花樣滑冰運動員.md "wikilink")
[L李](../Category/中国奥运花样滑冰运动员.md "wikilink")
[C成江](../Category/李姓.md "wikilink")
[Category:2002年冬季奧林匹克運動會花式滑冰選手](../Category/2002年冬季奧林匹克運動會花式滑冰選手.md "wikilink")
[Category:2006年冬季奧林匹克運動會花式滑冰選手](../Category/2006年冬季奧林匹克運動會花式滑冰選手.md "wikilink")
[Category:四大洲花式滑冰錦標賽獎牌得主](../Category/四大洲花式滑冰錦標賽獎牌得主.md "wikilink")
[Category:亞洲運動會花式滑冰獎牌得主](../Category/亞洲運動會花式滑冰獎牌得主.md "wikilink")
[Category:1999年亞洲冬季運動會花式滑冰選手](../Category/1999年亞洲冬季運動會花式滑冰選手.md "wikilink")
[Category:2003年亞洲冬季運動會花式滑冰選手](../Category/2003年亞洲冬季運動會花式滑冰選手.md "wikilink")
[Category:2007年亞洲冬季運動會花式滑冰選手](../Category/2007年亞洲冬季運動會花式滑冰選手.md "wikilink")