[a64fxlogo.gif](https://zh.wikipedia.org/wiki/File:a64fxlogo.gif "fig:a64fxlogo.gif")

**Athlon 64 FX**是[AMD](../Page/AMD.md "wikilink")
[K8系列](../Page/AMD_K8.md "wikilink")[中央處理器](../Page/中央處理器.md "wikilink")（CPU）之一，專為追求極限的愛好者而設。該處理器特意不設鎖頻，用家可透過自由改變倍頻來把CPU[超頻或](../Page/超頻.md "wikilink")[降頻](../Page/降頻.md "wikilink")。它可在單核心或[雙核心處理器的](../Page/雙核心.md "wikilink")[PC上執行](../Page/个人计算机.md "wikilink")，支援[x86及](../Page/x86.md "wikilink")[AMD64的指令集](../Page/AMD64.md "wikilink")。在產品特色方面，它設有1MB的L2快取及128位[雙通道記憶體控制器](../Page/雙通道.md "wikilink")。在時脈方面，它的速度介乎2.2至2.8GHz之間。一如其他K8處理器，它擁有800MHz的[HyperTransport資料傳輸技術](../Page/HyperTransport.md "wikilink")，後期更增至1000MHz。

該處理器共分為兩款插座，分別為[Socket 940和](../Page/Socket_940.md "wikilink")[Socket
939](../Page/Socket_939.md "wikilink")，前者主要用在[伺服器上](../Page/伺服器.md "wikilink")，後者則供愛好者使用。而型號方面，計有FX-51（2.2 GHz）、FX-53（2.4 GHz）、FX-55（2.6 GHz）、FX-57（2.8 GHz）、FX-60（2.6 GHz）和FX-62（2.8 GHz）。FX-60是AMD
Athlon FX系列的首款雙核心處理器，FX-57則是x86平台上性能最強的單核心處理器。

## 型號

### Sledgehammer核心 (130 nm SOI)

  - L1缓存：64 + 64 KB（Data + Instructions）
  - L2缓存：1024 KB、fullspeed
  - [MMX](../Page/MMX.md "wikilink")、Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")、[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")、[SSE2](../Page/SSE2.md "wikilink")、[AMD64](../Page/AMD64.md "wikilink")
  - [Socket 940](../Page/Socket_940.md "wikilink")、[800 MHz
    HyperTransport](../Page/HyperTransport.md "wikilink")
  - Registered DDR-SDRAM required
  - 電壓：1.50V
  - 推出日期：2003年9月23日
  - 時脈：2200 MHz（FX-51）、2400 MHz（FX-53）

### Clawhammer核心（130 nm SOI）

  - L1缓存：64 + 64 KB（Data + Instructions）
  - L2缓存：1024 KB、fullspeed
  - [MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")、[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")、[SSE2](../Page/SSE2.md "wikilink")、[AMD64](../Page/AMD64.md "wikilink")、[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - [Socket 939](../Page/Socket_939.md "wikilink")、[1000 MHz
    HyperTransport](../Page/HyperTransport.md "wikilink")
  - 電壓：1.50 V
  - 推出日期：2004年6月1日
  - 时钟频率：2400 MHz（FX-53）、2600 MHz（FX-55）

### San Diego核心（90 nm SOI）

  - 处理器步进：E4
  - 一级缓存：64 + 64 千[字节](../Page/字节.md "wikilink")（数据+指令）
  - 二级缓存：1024 KB、全速
  - MMX、扩展式3DNow\!、SSE、SSE2、SSE3、AMD64、Cool'n'Quiet、NX Bit
  - Socket 939、1000 MHz HyperTransport
  - 核心电压：1.35 V
  - 功率需求（TDP）：最大 104 W
  - 最初版本：TBA
  - 时钟频率：2600 MHz（FX-55）、2800 MHz（FX-57）

### Toledo核心（90 nm SOI）

#### 雙核心處理器

  - 处理器步进：E6
  - 一级缓存：64 + 64 KB（Data + Instructions），每一核心
  - 二级缓存：1024 KB,fullspeed，每一核心
  - MMX，扩展式3DNow\!、SSE、SSE2、SSE3、AMD64、Cool'n'Quiet、NX Bit
  - Socket 939、1000 MHz HyperTransport
  - 核心电压：1.30-1.35 V
  - 功率需求（TDP）：最大 110 W
  - 时钟频率：2600 MHz（FX-60）

### Windsor核心（90 nm SOI）

#### 雙核心處理器

  - 处理器步进：F2
  - [L1 Cache](../Page/L1_Cache.md "wikilink")：64 + 64 KB（Data +
    Instructions），每一核心
  - L2 Cache：1024 KB，fullspeed，每一核心
  - MMX、SSE、SSE2、SSE3、3DNOW\! Professional、AMD64、Cool'n'Quiet、NX
    Bit、Pacifica
  - Socket AM2, 2000 MHz HyperTransport
  - 核心电压：1.35 / 1.40 V
  - 功率需求（TDP）：最大 125 W
  - 时钟频率：2800 MHz (FX-62)

## 參見

  - [AMD處理器列表](../Page/AMD處理器列表.md "wikilink")

[en:Athlon 64\#Athlon 64
FX](../Page/en:Athlon_64#Athlon_64_FX.md "wikilink")

[Category:AMD处理器](../Category/AMD处理器.md "wikilink")
[Category:X86架構](../Category/X86架構.md "wikilink")