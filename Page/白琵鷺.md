[Lepelaar_in_goud_licht_bij_zonsondergang-4961759.webm](https://zh.wikipedia.org/wiki/File:Lepelaar_in_goud_licht_bij_zonsondergang-4961759.webm "fig:Lepelaar_in_goud_licht_bij_zonsondergang-4961759.webm")
**白琵鹭**（學名：*Platalea
leucorodia*），屬於[鹮科](../Page/鹮科.md "wikilink")[琵鹭属](../Page/琵鹭属.md "wikilink")。

## 生态环境

喜欢在泥泞水塘、湖泊或泥滩等生境中活动，常在水中缓慢前进，嘴往两旁甩动以寻找食物。

## 分布地域

分布于[欧亚大陆](../Page/欧亚大陆.md "wikilink")；[非洲](../Page/非洲.md "wikilink")；夏季或许繁殖于[新疆西北部](../Page/新疆.md "wikilink")[天山至东北各省](../Page/天山.md "wikilink")。冬季南迁经中国中部至[云南](../Page/云南.md "wikilink")、东南沿海省份、[台湾及](../Page/台湾.md "wikilink")[澎湖列岛](../Page/澎湖列岛.md "wikilink")。

## 特征

特有的琵琶型的大嘴是琵鹭类鸟类共有的特征，長得很像可能混群的[黑脸琵鹭](../Page/黑脸琵鹭.md "wikilink")，與黑脸琵鹭比较，白琵鹭显得体型稍大一点，而且脸部黑色少，琵琶型的嘴末端黄色，面部裸露的皮肤黄色，由眼先到眼睛有一条幼细的黑纹。繁殖期的白琵鹭有明显的冠羽，冠羽和胸前的羽毛为黄色。

## 食物

滩涂湿地各种细小生物，琵琶型的嘴有如探雷器，帮助他们发现食物，相比于[鸻鹬类细长的](../Page/鸻鹬类.md "wikilink")[喙琵鹭的嘴更适合发掘浅层的食物](../Page/喙.md "wikilink")。

## 繁殖与保护

  - CITES濒危等级：附录II（生效年代：1997年）
  - 中国国家重点保护等级： 二级 生效年代：1989年
  - 中国濒危动物红皮书等级： 易危 生效年代：1996年

目前全世界超過一萬隻。

[Category:琵鹭属](../Category/琵鹭属.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")