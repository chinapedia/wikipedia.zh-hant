**-{台}-**（）是[香港](../Page/香港.md "wikilink")[無綫網絡電視](../Page/無綫網絡電視.md "wikilink")（2013年前身為[無綫收費電視](../Page/無綫收費電視.md "wikilink")）旗下一條綜藝頻道。

本頻道前身為「**TVB生活台**」，而生活台的前身則為「**無綫健康台**」。**健康台**時期主要播放健康資訊節目，後期加入飲食時尚節目，於2006年演化並改名為**生活台**。

## 歷史與發展

### 健康台時期

2005年5月30日，為配合「[銀河衛視exTV](../Page/銀河衛視exTV.md "wikilink")」易名「SuperSun
新電視」，本頻道以**無綫健康台**之名義正式開播。

2006年3月1日，「新電視」再次更名為「無綫收費電視」，**無綫健康台**改革整合為新「無綫八大收費電視頻道」作推廣，更改台徽，增加健康資訊節目及娛樂飲食節目。及後，2006年6月1日晚上8点，**無綫健康台**改名為**無綫生活台**。

### 生活台時期

在2006年初，無綫電視已計劃設立無綫生活台，並邀請[曾志偉為生活台台長](../Page/曾志偉.md "wikilink")。同年3月9日，「SuperSun
新電視」易名「無綫收費電視」，並同時宣布建立無綫八大粵語頻道，包括以「無綫生活台」取代「無綫健康台」。

自公布成立生活台以後，無綫電視多次為此頻道宣傳，在2006年5月初宣布生活台將播放《[志雲飯局](../Page/志雲飯局.md "wikilink")》\[1\]。

在2006年5月27日，藝員[王喜攀登](../Page/王喜.md "wikilink")[珠穆朗瑪峰](../Page/珠穆朗瑪峰.md "wikilink")，並為無綫生活台拍攝節目\[2\]。5月31日[端午節](../Page/端午節.md "wikilink")，無綫派出的龍舟隊以「無綫生活台」名義、取代多年以「無綫電視」的名義在[沙田](../Page/沙田.md "wikilink")[龍舟競渡出賽](../Page/龍舟.md "wikilink")，當天出賽的健兒包括無綫生活台的主持，而電視廣播有限公司總經理[陳志雲也到場支持](../Page/陳志雲.md "wikilink")。

2006年6月1日[香港時間晚上](../Page/香港時間.md "wikilink")8時開始在[無綫收費電視第](../Page/無綫收費電視.md "wikilink")10頻道和[now寬頻電視的無綫收費電視特選組合第](../Page/now寬頻電視.md "wikilink")810頻道播出，每日約有三小時首播時間，其餘時間重播當日或早一天的電視節目。

2013年4月1日，無綫網絡電視部分頻道號碼正式更改，其中該頻道將使用原有的第10頻道更改為現有的第2頻道。\[3\]

2013年4月29日，該頻道將中文名稱的「**無綫生活-{台}-**」更名為「**生活-{台}-**」。

### Good Show時期

2013年9月28日，該頻道更名為**TVB Good Show**，改為播放亞洲綜藝節目為主，惟海外版本依然維持生活台名稱。

2015年4月1日早上6時，香港**TVB Good
Show**終止播映，其功能被往後開設的**[TVB綜藝旅遊台](../Page/TVB綜藝旅遊台.md "wikilink")**繼承。而海外版本**生活台**則依然維持播放至2016年4月30日。5月1日，海外版本生活台與[TVB為食台合併成為](../Page/TVB為食台.md "wikilink")**星級生活台**。

## 播放節目

舊有無綫生活台節目包括健康資訊、娛樂飲食和生活時尚。平日播放節目包括《[诡异档案](../Page/诡异档案.md "wikilink")》(逢星期六)、《[潮遊珠三角](../Page/潮遊珠三角.md "wikilink")》(逢星期一、二)、《[潮遊泰國](../Page/潮遊泰國.md "wikilink")》(逢星期三、四)、《》(逢星期三)、《[同車生活](../Page/同車生活.md "wikilink")》、《[更上一層樓](../Page/更上一層樓.md "wikilink")》（逢星期五）、與《[全民開講](../Page/全民開講_\(香港\).md "wikilink")》(逢星期四)、《[星級廚房](../Page/星級廚房.md "wikilink")》及由[TVBS-G製作](../Page/TVBS-G.md "wikilink")，同時在無綫收費電視另一頻道[TVBS-ASIA播放的節目](../Page/無線衛星亞洲台.md "wikilink")《[女人我最大](../Page/女人我最大.md "wikilink")》。另外重播無綫節目《[世界那麼大](../Page/世界那麼大.md "wikilink")》(逢星期一)。週六播放《[志雲飯局](../Page/志雲飯局.md "wikilink")》、《[食遍中港台](../Page/食遍中港台.md "wikilink")》、《[百家講壇](../Page/百家講壇.md "wikilink")》、《[超級星光大道](../Page/超級星光大道.md "wikilink")》、《[玄學天王08運程大預測](../Page/玄學天王08運程大預測.md "wikilink")》、《[星級廚房精華](../Page/星級廚房精華.md "wikilink")》(週六篇)，及重播[無綫電視翡翠台劇](../Page/無綫電視翡翠台.md "wikilink")。週日則播放《[星級廚房精華](../Page/星級廚房精華.md "wikilink")》(週日篇)。

基於[TVB為食台在](../Page/TVB為食台.md "wikilink")2010年9月24日開播，原有播放的飲食節目全部轉往[TVB為食台繼續播映](../Page/TVB為食台.md "wikilink")。

在2013年9月28日更名為TVB Good
Show台之後，星期六、日主力播放來自亞洲地區的綜藝節目，而星期一至五則會重播曾於[翡翠台](../Page/翡翠台.md "wikilink")、[J2台及](../Page/J2台.md "wikilink")[明珠台的自製及外購旅遊和綜藝節目](../Page/明珠台.md "wikilink")，以及前身無綫生活台的節目。另外，每逢星期五更會播放本地及各地的演藝盛事和頒獎典禮。

### 星級廚房（已轉往TVB為食台播映）

星級廚房多數時段(為星期一、三、五)都由[周中師傅主持](../Page/周中.md "wikilink")，並帶領星級主持烹調食物；而部份時段(為星期二、四)會由[杉內馨](../Page/杉內馨.md "wikilink")(Kei)主持星級廚房，並帶領星級主持烹調食物；近期亦出現主題式星級廚房，例如由溏心風暴演員為主的「溏心風暴爭「鏟」升級版」。

### TVB Good Show台節目

  - [另一片天空](../Page/另一片天空.md "wikilink")

  - [女人我最大](../Page/女人我最大.md "wikilink")

  - （第一至二輯）

  - [花樣爺爺](../Page/花樣爺爺.md "wikilink")

  - [爸爸\! 我們去哪兒?](../Page/爸爸_你去哪兒.md "wikilink")

  - [超級歌喉讚](../Page/超級歌喉讚.md "wikilink")

  - [中國好聲音II](../Page/中國好聲音_\(第二季\).md "wikilink")

  - [中國好聲音 II - 中秋晚會](../Page/中國好聲音_\(第二季\).md "wikilink")

  - [超級偶像](../Page/超級偶像7.md "wikilink")（第七屆）

  - [矛盾對決](../Page/矛盾對決.md "wikilink")

  - [寵物大本營IX](../Page/寵物當家.md "wikilink")

  - [6號訪](../Page/陸浩明.md "wikilink")

  - [星夢.語](../Page/星夢傳奇.md "wikilink")

  -
  - [星和無綫電視大獎2013 萬千星輝頒獎典禮](../Page/星和無綫電視大獎2013_萬千星輝頒獎典禮.md "wikilink")

  - [新城國語力頒獎禮2013](../Page/新城國語力頒獎禮.md "wikilink")

  -
  - （第二輯）

  - [第24屆流行音樂金曲獎頒獎典禮](../Page/第24屆金曲獎.md "wikilink")

  - [左麟右李左Friend右Band大派對](../Page/左麟右李.md "wikilink")

  - [Music Bank智利韓流瘋](../Page/Music_Bank.md "wikilink")

  - [第四十八屆電視金鐘獎頒獎典禮](../Page/第48屆金鐘獎.md "wikilink")

  - [2013 Mnet Asian Music
    Awards](../Page/Mnet_Asian_Music_Awards.md "wikilink")

  - [K-POP悉尼韓流瘋](../Page/K-POP悉尼韓流瘋.md "wikilink")

  - [ABU亞太廣播聯盟音樂節2013](../Page/亞太廣播聯盟.md "wikilink")

  - [第十屆亞洲音樂節](../Page/亞洲音樂節#第十屆.md "wikilink")

  - [新加坡金曲獎2013](../Page/新加坡金曲獎.md "wikilink")

  - [2013年度TVB全球華人新秀歌唱大賽](../Page/TVB全球華人新秀歌唱大賽#第32屆（2013年）.md "wikilink")

  - [新城勁爆頒獎禮 2013](../Page/2013年度新城勁爆頒獎禮得獎名單.md "wikilink")

  - [TVB 馬來西亞星光薈萃頒獎典禮2013](../Page/TVB_馬來西亞星光薈萃頒獎典禮2013.md "wikilink")

  - [第五十六屆亞太影展頒獎典禮](../Page/第56屆亞太影展.md "wikilink")

  - [與導演對話](../Page/TVB娛樂新聞台.md "wikilink")

  - [中國夢之聲](../Page/中國夢之聲_\(第一季\).md "wikilink")

  - [發現地中海](../Page/發現地中海.md "wikilink")

  - [第25屆CASH流行曲創作大賽](../Page/CASH流行曲創作大賽#第25屆（2013年）.md "wikilink")

  - [Music Bank伊斯坦布爾韓流瘋](../Page/Music_Bank.md "wikilink")

  - [2013年度勁歌金曲頒獎典禮](../Page/2013年度勁歌金曲頒獎典禮得獎名單.md "wikilink")

  - [WOW高雄！ 2014不思議港都跨年夜](../Page/WOW高雄！_2014不思議港都跨年夜.md "wikilink")

  - [2014夢圓東方一生一世跨年狂歡](../Page/東方衛視.md "wikilink")

  - [晴暖東方 - 2014群星新春大聯歡](../Page/東方衛視.md "wikilink")

  - [我的導遊是明星7](../Page/我的導遊是明星7.md "wikilink")

  - [型格瘋](../Page/型格瘋.md "wikilink")（第四季）

  - [2013亞洲偶像盛典](../Page/2013亞洲偶像盛典.md "wikilink")

  - [2013華劇大賞](../Page/2013華劇大賞.md "wikilink")

  - [2014超級巨星紅白藝能大賞](../Page/2014超級巨星紅白藝能大賞.md "wikilink")

  - [爸爸去哪兒](../Page/爸爸去哪兒.md "wikilink")（[第一季](../Page/爸爸去哪兒_\(第一季\).md "wikilink")）

  - [2013國劇盛典](../Page/2013國劇盛典.md "wikilink")

  - [世界好聲音](../Page/世界好聲音.md "wikilink")

  - [SBS人氣歌謠2013](../Page/人氣歌謠.md "wikilink")

  - [花樣姐姐](../Page/花樣姐姐.md "wikilink")

  - [華人星光大道3](../Page/華人星光大道3.md "wikilink")

  - [第二季中國好聲音全國巡演首戰澳門站](../Page/中國好聲音_\(第二季\).md "wikilink")

  - [第二季中國好聲音全國巡演香港站](../Page/中國好聲音_\(第二季\).md "wikilink")

  - [全能星戰](../Page/全能星戰.md "wikilink")

  - [千奇百趣大挑戰](../Page/千奇百趣大挑戰.md "wikilink")（第一至二輯）

  - [愛丁堡不思議之旅](../Page/愛丁堡不思議之旅.md "wikilink")

  - [2013 KBS演技大賞](../Page/KBS演技大賞.md "wikilink")

  - [2013 MBC演技大賞](../Page/MBC演技大賞.md "wikilink")

  -
  - [蔚藍海岸嘉年華](../Page/蔚藍海岸嘉年華.md "wikilink")

  - [香港亞洲流行音樂節2014](../Page/香港亞洲流行音樂節.md "wikilink")

  - [小資台灣遊](../Page/小資台灣遊.md "wikilink")

  - [2013 SBS演技大賞](../Page/SBS演技大賞.md "wikilink")

  - [馬上唱響 - 2014 中國好聲音春節演唱會](../Page/中國好聲音.md "wikilink")

  - [空姐愛旅行](../Page/空姐愛旅行.md "wikilink")

  - [熱情西班牙](../Page/熱情西班牙.md "wikilink")

  - [2013 KBS 歌謠大賞](../Page/Music_Bank.md "wikilink")

  - [第21屆東方風雲榜頒獎盛典](../Page/第21屆東方風雲榜頒獎盛典.md "wikilink")

  - [第50屆百想藝術大賞](../Page/第50屆百想藝術大賞.md "wikilink")

  - [2013 MBC 歌謠祭](../Page/Show!_音樂中心.md "wikilink")

  - [寵物大本營X](../Page/寵物當家.md "wikilink")

  - [超級偶像](../Page/超級偶像8.md "wikilink")（第八屆）

  - [第25屆流行音樂金曲獎頒獎典禮](../Page/第25屆金曲獎.md "wikilink")

  - [中國好聲音2014年新年演唱會](../Page/中國好聲音.md "wikilink")

  - [夢想星搭檔](../Page/夢想星搭檔.md "wikilink")

  - [日本人妻在遠方](../Page/日本人妻在遠方.md "wikilink")

  - [Music Bank巴西韓流瘋](../Page/Music_Bank.md "wikilink")

  - [韓流夢幻演唱會2014](../Page/Dream_Concert.md "wikilink")

  - [華語榜中榜亞洲影響力大典](../Page/華語榜中榜亞洲影響力大典.md "wikilink")

  - [新城國語力頒獎禮2014](../Page/新城國語力頒獎禮.md "wikilink")

  -
  - [李敏鎬Good Day](../Page/李敏鎬.md "wikilink")

  - [我的導遊是明星8](../Page/我的導遊是明星8.md "wikilink")

  - [正妹大連線](../Page/正妹大連線.md "wikilink")

  - [超級偶像](../Page/超級偶像9.md "wikilink")（第九屆）

  - [星和無綫電視大獎2014 萬千星輝頒獎典禮](../Page/星和無綫電視大獎2014.md "wikilink")

  - [第四十九屆電視金鐘獎頒獎典禮](../Page/第49屆金鐘獎.md "wikilink")

  - [新加坡金曲獎2014](../Page/第十九屆新加坡金曲獎.md "wikilink")

  - [第十一屆亞洲音樂節](../Page/亞洲音樂節#第十一屆.md "wikilink")

  - [ABU亞太廣播聯盟音樂節2014](../Page/亞太廣播聯盟.md "wikilink")

  - [新城勁爆頒獎禮2014](../Page/2014年度新城勁爆頒獎禮得獎名單.md "wikilink")

  - [2014華劇大賞](../Page/華劇大賞#第三屆\(2014年\).md "wikilink")

  - [爸爸\! 我們去哪兒? 2](../Page/爸爸_你去哪兒.md "wikilink")

  -
  - [中國夢之聲II](../Page/中國夢之聲_\(第二季\).md "wikilink")

  - [Music Bank墨西哥韓流瘋](../Page/Music_Bank.md "wikilink")

  - [花樣爺爺](../Page/花樣爺爺.md "wikilink")（第二季）

  - [2014 SBS演技大賞](../Page/2014_SBS演技大賞.md "wikilink")

  - [夢圓東方我們的夢跨年盛典](../Page/東方衛視.md "wikilink")

  - [阿順歷險記](../Page/阿順歷險記.md "wikilink")

  - [2014 MBC 歌謠祭](../Page/Show!_音樂中心.md "wikilink")

  - [2014 MBC演技大賞](../Page/2014_MBC演技大賞.md "wikilink")

  - [2014 KBS演技大賞](../Page/2014_KBS演技大賞.md "wikilink")

  - [2014 KBS 歌謠大賞](../Page/Music_Bank.md "wikilink")

  - [香港亞洲流行音樂節2015](../Page/香港亞洲流行音樂節.md "wikilink")

**重播節目**

  - [瑞士天巒](../Page/瑞士天巒.md "wikilink")

  - [愛玩客](../Page/愛玩客.md "wikilink")（第一至二輯）

  - [型城大解構](../Page/型城大解構.md "wikilink")

  - [去吧\! 墨西哥\!](../Page/去吧!_墨西哥!.md "wikilink")

  - [叻哥遊世界](../Page/叻哥遊世界.md "wikilink")

  - [擺家樂](../Page/擺家樂.md "wikilink")

  - [舊歡心愛](../Page/舊歡心愛.md "wikilink")

  - [廉遊韓國](../Page/廉遊韓國.md "wikilink")

  - [浪遊愛爾蘭](../Page/浪遊愛爾蘭.md "wikilink")

  - [優遊天下](../Page/優遊天下.md "wikilink")

  - [與星同行](../Page/與星同行.md "wikilink")（第一至三輯）

  - [走過浮華大地](../Page/走過浮華大地.md "wikilink")

  - [走過浮華大地 亞洲篇](../Page/走過浮華大地_亞洲篇.md "wikilink")

  - [福井初體驗](../Page/福井初體驗.md "wikilink")

  - [流行天裁](../Page/流行天裁.md "wikilink")

  - [大腳走天下](../Page/大腳走天下.md "wikilink")

  - [以諾遊蹤 - 關西.歲月的洗禮](../Page/以諾遊蹤.md "wikilink")

  - [流行速遞](../Page/流行速遞.md "wikilink")

  - [設計之城](../Page/設計之城.md "wikilink")

  - [民峰學院](../Page/民峰學院.md "wikilink")

  - [週末型樂遊](../Page/週末型樂遊.md "wikilink")

  - （第一輯）

  - [美眉向前行](../Page/美眉向前行.md "wikilink")

  - [流行別注](../Page/流行別注.md "wikilink")

  - [戀戀沖繩](../Page/戀戀沖繩.md "wikilink")

  - [爾巒品味意國](../Page/爾巒品味意國.md "wikilink")

  - [古都心動遊](../Page/古都心動遊.md "wikilink")

  - [塔斯曼尼亞的日與夜](../Page/塔斯曼尼亞的日與夜.md "wikilink")

  - [Woman 愛旅行](../Page/Woman_愛旅行.md "wikilink")（第一至二輯）

  - [自游瘋](../Page/自游瘋.md "wikilink")

  - [度身訂造旅行團](../Page/度身訂造旅行團.md "wikilink")

  - [瀛風絕色](../Page/瀛風絕色.md "wikilink")

  - [櫻紅醉了](../Page/櫻紅醉了.md "wikilink")

  - [世界正美麗](../Page/世界正美麗.md "wikilink")

  - [闖關孖寶](../Page/闖關孖寶.md "wikilink")

  - [一筆OUT消](../Page/一筆OUT消.md "wikilink")

  - [頂尖品味屋](../Page/頂尖品味屋.md "wikilink")

  - [in生活](../Page/in生活.md "wikilink")

  - [情迷越南](../Page/情迷越南.md "wikilink")

  - [浪遊法國](../Page/浪遊法國.md "wikilink")（）

  - [小鎮風情](../Page/小鎮風情.md "wikilink")

  - [駕到沖繩](../Page/駕到沖繩.md "wikilink")

  -
  -
  - [廉遊達人](../Page/廉遊達人.md "wikilink")

  - [流行盛典](../Page/流行盛典.md "wikilink")

  - [印加古國秘魯行](../Page/印加古國秘魯行.md "wikilink")

  - [深圳闖蕩](../Page/深圳闖蕩.md "wikilink")

  - [三個小生去旅行](../Page/三個小生去旅行.md "wikilink")

  - [志村動物園](../Page/志村動物園.md "wikilink")（第一至二輯）

  - [關西風雨．晴](../Page/關西風雨．晴.md "wikilink")

  - [美利堅精華遊](../Page/美利堅精華遊.md "wikilink")（第一季）

  - [我要「玩」屋](../Page/我要「玩」屋.md "wikilink")

  - [杜德偉･巨星同學會](../Page/杜德偉.md "wikilink")

  - [快樂地圖](../Page/快樂地圖.md "wikilink")

  - [紅葉瀛風](../Page/紅葉瀛風.md "wikilink")

  - [DANCING 9](../Page/DANCING_9.md "wikilink")（第一季）

  - [型男設計](../Page/型男設計.md "wikilink")（第五季）

  - [非常·杜拜](../Page/非常·杜拜.md "wikilink")

  - [驚奇大冒險](../Page/驚奇大冒險.md "wikilink")（第一輯）

  - [創意無疆界](../Page/創意無疆界.md "wikilink")

  - [後現代設計](../Page/後現代設計.md "wikilink")

  - [瓹窿瓹罅遊東京](../Page/瓹窿瓹罅遊東京.md "wikilink")（第一至三輯）

  - \-{[激Fun新西蘭](../Page/激Fun新西蘭.md "wikilink")}-

  - [設計達人](../Page/設計達人.md "wikilink")

  - [生活品味](../Page/生活品味.md "wikilink")（第一至二輯）

  - [茜嘉團遊 印度的十日十夜](../Page/茜嘉團遊_印度的十日十夜.md "wikilink")

  - [就是愛旅行](../Page/就是愛旅行.md "wikilink")

  - [日本東北咁過祭](../Page/日本東北咁過祭.md "wikilink")

  - [2014時尚年結](../Page/2014時尚年結.md "wikilink")

  - [回鄉](../Page/回鄉.md "wikilink")

  - [浪遊捷克](../Page/浪遊捷克.md "wikilink")

  - [寵物大本營VII](../Page/寵物當家.md "wikilink")

  - [漫遊南美](../Page/漫遊南美.md "wikilink")

  - [薰衣美麗遊](../Page/薰衣美麗遊.md "wikilink")

## 海外版本

## 參考資料

## 外部連結

[音](../Category/電視廣播有限公司電視頻道.md "wikilink")
[Category:2013年成立的電視台或電視頻道](../Category/2013年成立的電視台或電視頻道.md "wikilink")
[\*](../Category/香港已停播的電視頻道.md "wikilink")
[Category:粤语电视频道](../Category/粤语电视频道.md "wikilink")

1.  [陳寶珠向陳志雲飯局吐心事](http://hk.news.yahoo.com/060504/10/1njtg.html)
2.  [王喜勇闖珠穆朗瑪峰](http://hk.news.yahoo.com/060527/12/1o83f.html)
3.