**胡斯托·何塞·德·乌尔基萨·伊·加西亚**（****，），[阿根廷军人和政治家](../Page/阿根廷.md "wikilink")。他从1854年到1860年任[阿根廷邦联总统](../Page/阿根廷邦联.md "wikilink")。

1841年成为[恩特雷里奥斯省省长](../Page/恩特雷里奥斯省.md "wikilink")。他与地方首领结成联盟，于1852年2月3日在[卡塞罗斯战役中推翻了布宜诺斯艾利斯省省长](../Page/卡塞罗斯战役.md "wikilink")[罗萨斯](../Page/胡安·曼努埃尔·德·罗萨斯.md "wikilink")，并于5月1日召开制宪会议，制订一部美國式的宪法，成立了新的联邦共和国，僅布宜諾斯艾利斯省拒絕加入新成立的聯邦，乌尔基萨出任首任[总统](../Page/阿根廷总统.md "wikilink")。1860年離開總統職務，帶領阿根廷軍隊在[帕翁戰役中獲勝](../Page/帕翁戰役.md "wikilink")。1870年4月11日他和他的兒子被一個政敵的支持者所暗殺。

[category:阿根廷軍事人物](../Page/category:阿根廷軍事人物.md "wikilink")

[Category:阿根廷總統](../Category/阿根廷總統.md "wikilink")
[Category:阿根廷遇刺身亡者](../Category/阿根廷遇刺身亡者.md "wikilink")
[Category:軍人出身的總統](../Category/軍人出身的總統.md "wikilink")
[Category:考迪羅](../Category/考迪羅.md "wikilink")