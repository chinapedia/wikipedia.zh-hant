[Chandra_X-ray_Observatory.jpg](https://zh.wikipedia.org/wiki/File:Chandra_X-ray_Observatory.jpg "fig:Chandra_X-ray_Observatory.jpg")
**钱德拉X射线天文台**（，缩写为），是[美国宇航局](../Page/美国宇航局.md "wikilink")（NASA）于1999年发射的一颗[X射线天文卫星](../Page/X射线天文卫星.md "wikilink")，以[美国籍](../Page/美国.md "wikilink")[印度物理学家](../Page/印度.md "wikilink")[苏布拉马尼扬·钱德拉塞卡命名](../Page/苏布拉马尼扬·钱德拉塞卡.md "wikilink")，為[大型轨道天文台计划的第三颗卫星](../Page/大型轨道天文台计划.md "wikilink")，目的是观测天体的[X射线辐射](../Page/X射线.md "wikilink")。其特点是兼具极高的空间分辨率和谱分辨率，被认为是[X射线天文学上具有里程碑意义的空间望远镜](../Page/X射线天文学.md "wikilink")，标志着X射线天文学从测光时代进入了光谱时代。

## 歷史

钱德拉X射线天文台源于1976年美籍意大利裔天文学家[里卡尔多·贾科尼等人的设想](../Page/里卡尔多·贾科尼.md "wikilink")。1978年，美国发射了第一颗能成像的X射线天文卫星——[爱因斯坦卫星](../Page/爱因斯坦卫星.md "wikilink")，为钱德拉X射线天文台的研制积累了经验。20世纪80年代和90年代，钱德拉X射线天文台的研制工作一直在进行。1992年，为了削减预算，不得不对其进行重新设计，主镜数目由12个减为4个，终端设备也减掉2台，轨道改为椭圆形，最远距离达到地月距离的三分之一，这样做目的是避开[地球辐射带的影响](../Page/地球辐射带.md "wikilink")，但一旦发生故障，将无法用[航天飞机对其进行维修](../Page/航天飞机.md "wikilink")。

此天文衛星的制造耗资15.5亿美元，原名为「先进X射线天文设备」（），1998年，为纪念美籍印度裔天体物理家钱德拉塞卡而更名。1999年7月23日，钱德拉X射线天文台由[哥伦比亚号航天飞机搭载升空](../Page/哥伦比亚号航天飞机.md "wikilink")，运行在一条椭圆轨道上，近地点为1万公里，远地点为14万公里，轨道周期为64小时。卫星在轨期间由[史密松天体物理台负责操控和运作](../Page/史密松天体物理台.md "wikilink")。

## 規格與設備

钱德拉X射线天文台总重约4.8吨，主镜为四台套筒式[掠射望远镜](../Page/掠射望远镜.md "wikilink")，每台口径1.2米，焦距10米，接受面积0.04平方米，采用沃尔特型光路。终端设备有：

  - 高新CCD成像频谱仪（），由10台[CCD组成](../Page/CCD.md "wikilink")，观测能段为0.2-10 keV。
  - 高分辨率照相机（），主要部件是2台[微通道板探测器](../Page/微通道板探测器.md "wikilink")，观测能段为0.1-10
    keV，时间分辨率达到0.016秒。
  - 高能透射光栅摄谱仪（），观测能段为0.4 - 10 keV，谱分辨率为60-1000。
  - 低能透射光栅摄谱仪（），观测能段为0.09 - 3
    keV，谱分辨率为40-2000，两台摄谱仪都能够与高新CCD成像摄谱仪和高分辨率相机联合工作。

## 觀測成果

钱德拉X射线天文台取得了大量的成果，包括在[星遽增星系](../Page/星遽增星系.md "wikilink")[M82中发现了中等质量](../Page/雪茄星系.md "wikilink")[黑洞的证据](../Page/黑洞.md "wikilink")\[1\]、发现[伽玛射线暴](../Page/伽玛射线暴.md "wikilink")[GRB
991216中的X射线发射](../Page/GRB_991216.md "wikilink")\[2\]、观测到了[银河系中心超大质量黑洞](../Page/银河系.md "wikilink")[人馬座A的X射线辐射](../Page/人馬座A.md "wikilink")\[3\]、物质从[原恒星盘落入](../Page/原恒星盘.md "wikilink")[恒星时发出的X射线](../Page/恒星.md "wikilink")\[4\]等等。

## 外部链接

  - [钱德拉X射线天文台网站](http://chandra.harvard.edu/)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:X射线天文卫星](../Category/X射线天文卫星.md "wikilink")
[Category:空间望远镜](../Category/空间望远镜.md "wikilink")
[Category:望远镜](../Category/望远镜.md "wikilink")

1.
2.
3.
4.
    [<DOI:10.1038/nature02747>](../Page/DOI:10.1038/nature02747.md "wikilink")