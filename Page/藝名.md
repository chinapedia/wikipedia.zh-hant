**藝名**，指的是[藝人](../Page/藝人.md "wikilink")、[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[模特](../Page/模特.md "wikilink")、[音樂家](../Page/音樂家.md "wikilink")、[配音員](../Page/配音員.md "wikilink")（特別是[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")）等[演藝圈人士以及某些](../Page/演藝圈.md "wikilink")[運動員等在](../Page/運動員.md "wikilink")[本名以外所取](../Page/本名.md "wikilink")，在演藝事業時供[觀眾稱呼的](../Page/觀眾.md "wikilink")[化名](../Page/化名.md "wikilink")，包括了[廣播電臺或](../Page/廣播電臺.md "wikilink")[電視臺](../Page/電視臺.md "wikilink")[記者](../Page/記者.md "wikilink")、[主播的](../Page/主播.md "wikilink")[播音名](../Page/播音名.md "wikilink")。

## 參見

  - [姓名](../Page/姓名.md "wikilink")
  - [筆名](../Page/筆名.md "wikilink")

[藝人](../Category/藝人.md "wikilink")
[Category:化名](../Category/化名.md "wikilink")
[Category:演出](../Category/演出.md "wikilink")