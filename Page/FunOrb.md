**FunOrb**
是一個以[Java為遊戲引擎的網頁遊戲平台](../Page/Java.md "wikilink")，開發公司為[Jagex](../Page/Jagex.md "wikilink")
Ltd.。
FunOrb的遊戲類型與[Runescape略為不同](../Page/Runescape.md "wikilink")，前者為小型遊戲而後者則為[大型多人在線角色扮演遊戲](../Page/大型多人在線角色扮演遊戲.md "wikilink")。現時Funorb總共有4個版本，分別是[英文](../Page/英文.md "wikilink")、[德文](../Page/德文.md "wikilink")、[法文及](../Page/法文.md "wikilink")[葡萄牙文](../Page/葡萄牙文.md "wikilink")。於2009年12月3日，Jagex推出了第一款[手機遊戲](../Page/手機遊戲.md "wikilink")，Bouncedown，予[iPhone及](../Page/iPhone.md "wikilink")[iPod
touch](../Page/iPod_touch.md "wikilink")。

## 基本

Funorb每約兩周更新一次\[1\]，包括增加遊戲或更新網站。

如玩家能夠在遊戲內，達到特定的分數或完成特定的挑戰（即是Achievements 或"成就"），便會獲得一些小獎座。玩家亦會得到"Orb
Coins" 及 "Orb Points" 以作獎勵。擁有足夠的"Orb Coins"，玩家可以在"Orb
downloads"裡下載桌布、遊戲音樂及圖示等。"Orb Points"則用以比較玩家們在各遊戲的實力。

## 付費會員

**收費**（$ 為美金）

  - £2/$3／€2.50
  - £1.40/$2／€2（持有[Runescape](../Page/Runescape.md "wikilink")
    membership\[2\]）只需每月繳付以上的費用便能夠成為會員

**付費會員的好處**

  - 享有更多遊戲的內容及關卡
  - 能夠以全螢幕進行遊戲
  - 進行遊戲時頁面的廣告被收起
  - 能夠額外添加一百名好友（非會員的限額為一百個）

## 遊戲

[Torquing.jpg](https://zh.wikipedia.org/wiki/File:Torquing.jpg "fig:Torquing.jpg")
[Starcannon.jpg](https://zh.wikipedia.org/wiki/File:Starcannon.jpg "fig:Starcannon.jpg")

現時共有39個遊戲：

| 名稱                           | 類別    | 多玩家？ |
| ---------------------------- | ----- | ---- |
| *36 Card Trick*              | 智力    | 不是   |
| *Arcanists*                  | 策略    | 是    |
| '' Armies of Gielinor ''     | 策略    | 是    |
| *Brick-À-Brac*               | 街機    | 是    |
| *Bachelor Fridge*            | 策略    | 是    |
| *Bouncedown*                 | 動作    | 是+   |
| *Chess*                      | 策略    | 是    |
| *Crazy Crystals*             | 智力    | 是+   |
| *Deko Bloko*                 | 街機    | 是    |
| *Dr. P. Saves the Earth*     | 射擊    | 不是   |
| '' Dungeon Assault''         | 策略    | 是    |
| '' Escape Vector''           | 技巧    | 不是   |
| *Flea Circus*                | 智力    | 不是   |
| *Geoblox*                    | 智力    | 不是   |
| *Hold the Line*              | 智力    | 不是   |
| *Hostile Spawn*              | 射擊    | 不是   |
| *Kickabout League*           | 體育/動作 | 是    |
| *Lexiconimos*                | 拼字    | 不是   |
| *Miner Disturbance*          | 智力    | 不是   |
| *Monkey Puzzle 2*            | 智力    | 不是   |
| '' Orb Defence''             | 策略    | 不是   |
| '' Pixelate ''               | 拼圖    | 是    |
| '' Pool ''                   | 體育    | 是    |
| *Shattered Plans*            | 策略    | 是    |
| *Sol-Knight*                 | 射擊    | 不是   |
| *StarCannon*                 | 射擊    | 不是   |
| *Steel Sentinels*            | 戰略    | 是    |
| *Stellar Shard*              | 射擊    | 不是   |
| *TerraPhoenix*               | 策略    | 不是   |
| *TetraLink*                  | 智力    | 是    |
| *The Track Controller*       | 智力    | 不是   |
| *Tor Challenge*              | 動作    | 不是   |
| *Torquing\!*                 | 動作    | 不是   |
| *Transmogrify*               | 拼字    | 不是   |
| *Vertigo 2*                  | 動作    | 是    |
| *Virogrid*                   | 策略    | 是    |
| *Wizard Run*                 | 動作    | 不是   |
| *Zombie Dawn*                | 策略    | 不是   |
| '' Zombie Dawn Multiplayer'' | 策略    | 是    |

註：+ 代表只能夠在單一的電腦內

## 節日

為了配合節日氣氛，Funorb在2008年10月22日至11月5日期間推出了一連串的節日活動；包括在10個遊戲新增萬聖節"成就"，而每一個遊戲都有暫時性的萬聖節主題。\[3\]

## 參考文獻

## 相關條目

  - [Jagex](../Page/Jagex.md "wikilink")
  - [RuneScape](../Page/RuneScape.md "wikilink")

## 外部連結

  - 官方網站

<!-- end list -->

  - [FunOrb](http://www.funorb.com)
  - [Jagex](http://www.jagex.com)
  - [FunOrb at
    Facebook](http://www.facebook.com/pages/FunOrb-Games/97187290522)

<!-- end list -->

  - Wikis

<!-- end list -->

  - [*FunOrb* Wikia English](http://funorb.wikia.com/wiki/FunOrb_Wiki)
  - [*FunOrb* Wikia German](http://de.funorb.wikia.com/wiki/FunOrb_Wiki)
  - [*FunOrb* Wikia French](http://fr.funorb.wikia.com/wiki/FunOrb_Wiki)

[Category:2008年电子游戏](../Category/2008年电子游戏.md "wikilink")
[Category:網頁遊戲](../Category/網頁遊戲.md "wikilink")

1.  [Next-Gen
    Biz](http://www.next-gen.biz/index.php?option=com_content&task=view&id=8858&Itemid=50&limit=1&limitstart=1)
2.  <http://funorb.com> FunOrb.com
3.