**銅將**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。有在[平安大將棋](../Page/平安大將棋.md "wikilink")、[中將棋](../Page/中將棋.md "wikilink")、[大將棋](../Page/大將棋.md "wikilink")、[大大將棋](../Page/大大將棋.md "wikilink")、[天竺大將棋](../Page/天竺大將棋.md "wikilink")、[摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")、[泰將棋和](../Page/泰將棋.md "wikilink")[大局將棋出現](../Page/大局將棋.md "wikilink")。

## 銅將在平安大將棋

可升級成[金將](../Page/金將.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>銅將</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><span style="color:white">■</span></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><span style="color:white">■</span></p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>○</p></td>
<td><p><strong>銅</strong></p></td>
<td><p>○</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向、橫向走一格。</p></td>
<td><p>金將</p></td>
</tr>
</tbody>
</table>

## 銅將在中將棋、大將棋、天竺大將棋、大局將棋

在中將棋簡稱為**銅**。可升級為[橫行](../Page/橫行.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>銅將</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><span style="color:white">■</span></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><span style="color:white">■</span></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>銅</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向、前斜向走一格。</p></td>
<td><p>橫行</p></td>
</tr>
</tbody>
</table>

## 銅將在大大將棋

不得升級。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>銅將</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><span style="color:white">■</span></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><span style="color:white">■</span></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>銅</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 銅將在摩訶大大將棋、泰將棋

可升級成**奔銅**（）（與[白駒和](../Page/白駒.md "wikilink")[大局將棋的](../Page/大局將棋.md "wikilink")[雜將的走法相同](../Page/犬_\(日本將棋\).md "wikilink")）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>銅將</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><span style="color:white">■</span></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><span style="color:white">■</span></p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p><strong>銅</strong></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向、前斜向走一格。</p></td>
<td><p>奔銅</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [中將棋](../Page/中將棋.md "wikilink")
  - [大將棋](../Page/大將棋.md "wikilink")
  - [大大將棋](../Page/大大將棋.md "wikilink")
  - [天竺大將棋](../Page/天竺大將棋.md "wikilink")
  - [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")
  - [泰將棋](../Page/泰將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")
  - [平安大將棋](../Page/平安大將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:中將棋棋子](../Category/中將棋棋子.md "wikilink")
[Category:大將棋棋子](../Category/大將棋棋子.md "wikilink")
[Category:大大將棋棋子](../Category/大大將棋棋子.md "wikilink")
[Category:天竺大將棋棋子](../Category/天竺大將棋棋子.md "wikilink")
[Category:摩訶大大將棋棋子](../Category/摩訶大大將棋棋子.md "wikilink")
[Category:泰將棋棋子](../Category/泰將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")
[Category:平安大將棋棋子](../Category/平安大將棋棋子.md "wikilink")