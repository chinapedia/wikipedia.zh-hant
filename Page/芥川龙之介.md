[Kikuchi_Kan,_Akutagawa_Ryunosuke,_and_so_on.jpg](https://zh.wikipedia.org/wiki/File:Kikuchi_Kan,_Akutagawa_Ryunosuke,_and_so_on.jpg "fig:Kikuchi_Kan,_Akutagawa_Ryunosuke,_and_so_on.jpg")，攝於1919年。\]\]

**芥川龙之介**（，），[号](../Page/号.md "wikilink")“**澄江堂主人**”，[俳号](../Page/俳号.md "wikilink")“**我鬼**”，[日本知名](../Page/日本.md "wikilink")[小说家](../Page/小说家.md "wikilink")，博通[漢學](../Page/漢學.md "wikilink")、[日本文學](../Page/日本文學.md "wikilink")、[英國文學](../Page/英國文學.md "wikilink")，但一生為多種疾病、憂慮所苦而[自鴆](../Page/自鴆.md "wikilink")，年僅卅五。其名作甚多，以[極短篇為主](../Page/極短篇.md "wikilink")，如《[竹林中](../Page/竹林中.md "wikilink")》、《[罗生门](../Page/羅生門_\(小說\).md "wikilink")》、《[蜘蛛之絲](../Page/蜘蛛之絲.md "wikilink")》等三篇翻譯為[漢語](../Page/漢語.md "wikilink")，選入[中華民國的高中](../Page/中華民國.md "wikilink")[國文課文](../Page/國文.md "wikilink")、選讀教材，其中最為人所熟知的作品是《竹林中》。

## 生平

芥川龙之介原姓名新原龍之助，1892年（明治廿五年）生于[東京](../Page/東京.md "wikilink")。父親[新原敏三在](../Page/新原敏三.md "wikilink")[京橋區入船町](../Page/京橋區.md "wikilink")8丁目以販賣[牛奶為生](../Page/牛奶.md "wikilink")。為家中長子，有2个姐姐，不過，大姐在龍之介出生之前一年病死，當時只有6歲。他出生7個月後，由於母親精神異常，他被送到母親的娘家抚养，後來母親在他11歲離世後，[過繼為兄長](../Page/過繼.md "wikilink")[芥川道章](../Page/芥川道章.md "wikilink")（芥川龙之介之舅舅）的[養子](../Page/養子.md "wikilink")，遂改姓芥川。

芥川一家鍾好[文學與](../Page/文學.md "wikilink")[戲劇](../Page/戲劇.md "wikilink")，充滿濃厚的[江戶文人氣息](../Page/江戶.md "wikilink")，龍之介受此薰陶，故有深厚之文藝底蘊。11歲時就與同學發行手抄雜誌，並自己寫作、編輯，甚至自繪插圖。1913年他进入[东京帝国大学](../Page/东京帝国大学.md "wikilink")，学习[英国文学](../Page/英国文学.md "wikilink")，期间开始写作。毕业后通过英文教學和报纸编辑维生。早在1912年廿二歲時，他已完成了處女作《老年》，兩年後又发表了短篇小說《[罗生门](../Page/羅生門_\(小說\).md "wikilink")》，但並未受到重視。

1916年芥川畢業，論文題為「威廉·莫理斯研究」，其成績位列同屆二十人中之第二名。他在《新思潮》雜誌发表短篇小說《[鼻子](../Page/鼻子_\(小說\).md "wikilink")》，[夏目漱石读到後非常讚赏](../Page/夏目漱石.md "wikilink")，对他多方关怀。这段时间他也开始创作[俳句](../Page/俳句.md "wikilink")。1918年他发表《[地狱变](../Page/地狱变.md "wikilink")》，讲述一个[日本战国时期的残酷故事](../Page/日本战国.md "wikilink")，通过画师、画师女儿等人的遭遇，反映了纯粹的艺术和无辜的底层人民受邪恶的统治者的摧残。

1921年芥川龙之介作为[大阪](../Page/大阪.md "wikilink")[每日新闻报社的](../Page/每日新闻.md "wikilink")[记者前往](../Page/记者.md "wikilink")[中国四个月](../Page/中国.md "wikilink")，在結束訪問之際與胡適會面，7月返抵日本，之後寫成《支那遊記》一書。这次任务非常繁重。在任务的压力和自身压抑作用下，他染上了多种疾病，一生為[胃腸病](../Page/胃腸病.md "wikilink")、[痔瘡](../Page/痔瘡.md "wikilink")、[神經衰弱](../Page/神經衰弱.md "wikilink")、[失眠症所苦](../Page/失眠.md "wikilink")。回到日本後，1922年他发表了《[竹林中](../Page/竹林中.md "wikilink")》，与[安布罗斯·比尔斯的](../Page/安布罗斯·比尔斯.md "wikilink")《[月光小路](../Page/月光小路.md "wikilink")》结构类似，都是在一件案子的调查采集的各方的证词与说法。不同的是《月光小路》最后澄清了事实，而《竹林中》中各方的证词某些地方重合却有很大矛盾，但是又都能自圆其说。整个作品弥漫着压抑、彷徨、不定向的气氛。这反映了作者本人迷茫的思想。自此以後，由於病情恶化，芥川龙之介常出现[幻觉](../Page/幻觉.md "wikilink")，当时的社會形勢也[右傾](../Page/右傾.md "wikilink")，[言论自由受到從政府到普通人的打壓](../Page/言论自由.md "wikilink")。这使得他此時期的作品更加压抑，如《[河童](../Page/河童_\(小說\).md "wikilink")》。

1925年（大正14年）在文化學院文學部擔任講師。隔年，由於胃潰瘍、失眠、神經衰弱的復發再次入住[神奈川縣](../Page/神奈川縣.md "wikilink")[湯河原療養](../Page/湯河原.md "wikilink")。另一方面，妻子冢本文（婚後改為芥川文）搬到了[鹄沼的別墅](../Page/鹄沼.md "wikilink")。2月22日，龍之介也來到了鹄沼並住在旅館東屋。7月20日租下了東屋i-4號別墅，讓妻子和三男居住。在此期間，完成了小品《租房子之後》、《鹄沼雜紀》、《點鬼簿》。

1927年（昭和二年）芥川龙之介继续写作随想集《[侏儒的话](../Page/侏儒的话.md "wikilink")》，作品短小精悍，每段只有一两句话，但意味深长。姐夫卧軌自殺，芥川為二姐一家債務奔走導致身心俱疲。7月24日因「恍惚的不安」而[仰藥自盡](../Page/仰藥.md "wikilink")，年僅35歲。

## 家族

  - 父：新原敏三
  - 母：フク（阿福）
  - 養父：芥川道章
  - 妻：文（海軍少佐塚本善五郎之女）
  - 長子：（演員）
  - 次子：（戰死）
  - 三子：[芥川也寸志](../Page/芥川也寸志.md "wikilink")（作曲家）

## 主要著作

芥川龍之介在他短暂的一生中，写了超过150篇[小说](../Page/小说.md "wikilink")。他的[極短篇小說篇幅很短](../Page/極短篇小說.md "wikilink")，取材新颖，情节新奇甚至诡异。作品关注社会丑恶现象，但很少直接评论，而仅以冷峻的文字和简洁有力的语言来陈述，让读者深深感觉到其丑恶性，这使得他的小说即具有高度的艺术性又成为当时社会的缩影，《[竹林中](../Page/竹林中.md "wikilink")》、《[罗生门](../Page/羅生門_\(小說\).md "wikilink")》、《[蜘蛛之絲](../Page/蜘蛛之絲.md "wikilink")》、《[地獄變](../Page/地獄變.md "wikilink")》、《[杜子春](../Page/杜子春_\(芥川龍之介\).md "wikilink")》、《[鼻子](../Page/鼻子.md "wikilink")》、《[南京的基督](../Page/南京的基督.md "wikilink")》等皆是经典之作，時常被選入課文、小說選集，或改編為戲劇。

<table>
<thead>
<tr class="header">
<th><p>發表年代</p></th>
<th><p>中文名稱</p></th>
<th><p>日文名稱</p></th>
<th><p>英文名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1914年</p></td>
<td><p><a href="../Page/老年.md" title="wikilink">老年</a></p></td>
<td><p><em>Rōnen</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1915年</p></td>
<td><p><a href="../Page/羅生門_(小說).md" title="wikilink">羅生門</a></p></td>
<td><p><em>Rashōmon</em></p></td>
<td><p><em>Rashōmon</em></p></td>
</tr>
<tr class="odd">
<td><p>1916年</p></td>
<td><p><a href="../Page/鼻子_(小說).md" title="wikilink">鼻子</a></p></td>
<td><p>鼻 <em>Hana</em></p></td>
<td><p><em>The Nose</em></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/芋粥.md" title="wikilink">芋粥</a></p></td>
<td><p>芋粥 <em>Imogayu</em></p></td>
<td><p><em>Yam Gruel</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/煙草與悪魔.md" title="wikilink">煙草與悪魔</a></p></td>
<td><p>煙草と悪魔 <em>Tabako to Akuma</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1917年</p></td>
<td><p><a href="../Page/戲作三昧.md" title="wikilink">戲作三昧</a></p></td>
<td><p>戯作三昧 <em>Gesakuzanmai</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1918年</p></td>
<td><p><a href="../Page/蜘蛛之絲.md" title="wikilink">蜘蛛之絲</a></p></td>
<td><p><em>Kumo no Ito</em></p></td>
<td><p><em>The Spider's Thread</em></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/地獄變.md" title="wikilink">地獄變</a></p></td>
<td><p>地獄変 <em>Jigokuhen</em></p></td>
<td><p><em>Hell Screen</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邪宗門.md" title="wikilink">邪宗門</a></p></td>
<td><p>邪宗門 <em>Jashūmon</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1919年</p></td>
<td><p><a href="../Page/魔法.md" title="wikilink">魔法</a></p></td>
<td><p>魔術 <em>Majutsu</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1920年</p></td>
<td><p><a href="../Page/南京的基督.md" title="wikilink">南京的基督</a></p></td>
<td><p><em>Nankin no Kirisuto</em></p></td>
<td><p><em>Christ in Nanking</em></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杜子春_(芥川龍之介).md" title="wikilink">杜子春</a></p></td>
<td><p><em>Toshishun</em></p></td>
<td><p><em>Tu Tze-chun</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火神.md" title="wikilink">火神</a></p></td>
<td><p>アグニの神 <em>Aguni no Kami</em></p></td>
<td><p><em>Vulcan</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1921年</p></td>
<td><p><a href="../Page/竹林中.md" title="wikilink">竹林中</a></p></td>
<td><p><em>Yabu no Naka</em></p></td>
<td><p><em>In a Grove</em></p></td>
</tr>
<tr class="odd">
<td><p>1922年</p></td>
<td></td>
<td><p>トロッコ <em>Torokko</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1927年</p></td>
<td><p><a href="../Page/玄鶴山房.md" title="wikilink">玄鶴山房</a></p></td>
<td><p>玄鶴山房 <em>Genkakusanbō</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Shuju no Kotoba</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>文藝的、太文藝的</p></td>
<td><p>文芸的な、あまりに文芸的な <em>Bungeiteki na, amarini Bungeiteki na</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/河童_(小說).md" title="wikilink">河童</a></p></td>
<td><p><em>Kappa</em></p></td>
<td><p><em>Kappa</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/齒輪_(小說).md" title="wikilink">齒輪</a></p></td>
<td><p><em>Haguruma</em></p></td>
<td><p><em>Cogwheel</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/某阿呆的一生.md" title="wikilink">某阿呆的一生</a></p></td>
<td><p><em>Aru Ahō no Isshō</em></p></td>
<td><p><em>A Fool's Life</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西方的人.md" title="wikilink">西方的人</a></p></td>
<td><p>西方の人 <em>Seihō no Hito</em></p></td>
<td><p><em>The Man of the West</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 评价

  - [鲁迅评价其作品](../Page/鲁迅.md "wikilink")：“所用的主题最多的是希望之后的不安，或者正不安时之心情。”
  - [室生犀星评价其人](../Page/室生犀星.md "wikilink")：“这个作家好像从书籍之间变出来的，在世上只活了三十几年，谈笑一通，马上又隐身于自己出来的书籍之间，不再出来。”

## 其他

  - 1935年芥川龙之介自杀去世8年后，他的毕生好友[菊池寬设立了以他的名字命名的文学新人奖](../Page/菊池寬.md "wikilink")“[芥川賞](../Page/芥川賞.md "wikilink")”，现已成为日本最重要文学奖之一，与“[直木賞](../Page/直木賞.md "wikilink")”齐名，作為獎勵新進純文學作家的重要獎項之一，影響深遠。
  - 1950年，日本著名[导演](../Page/导演.md "wikilink")[黑泽明](../Page/黑泽明.md "wikilink")，将芥川的作品《竹林中》与《罗生门》合而为一，改编为电影《[罗生门](../Page/羅生門_\(電影\).md "wikilink")》，在国际上获得包括[威尼斯影展](../Page/威尼斯影展.md "wikilink")[金獅獎在內的多个大奖](../Page/金獅獎.md "wikilink")，使[日本电影走向世界](../Page/日本电影.md "wikilink")。此后，“[罗生门](../Page/罗生门.md "wikilink")”更成为華語地區对于扑朔迷离的、各方说法不一的事件的[代名词](../Page/代名词.md "wikilink")。

## 外部链接

  -
  - [芥川龍之介『トロッコ』：中文翻譯](https://bearshosetsu.blogspot.com/2018/11/truck-1.html)
    (日本小說翻譯室)

  - [芥川龍之介『魔術』：中文翻譯](https://bearshosetsu.blogspot.com/2018/07/blog-post_12.html)
    (日本小說翻譯室)

[Category:日本小说家](../Category/日本小说家.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")
[Category:自殺作家](../Category/自殺作家.md "wikilink")
[Category:日本记者](../Category/日本记者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:每日新聞社人物](../Category/每日新聞社人物.md "wikilink")