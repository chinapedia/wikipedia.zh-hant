**普布利乌斯·科尔内利乌斯·雷恩图卢斯**（[拉丁语](../Page/拉丁语.md "wikilink")：Publius
Cornelius Lentulus
Sura，？～前63年12月5日）[古罗马政治家](../Page/罗马共和国.md "wikilink")，因参与[喀提林阴谋而被](../Page/喀提林阴谋.md "wikilink")[处死](../Page/死刑.md "wikilink")。

科尔内利乌斯·雷恩图卢斯的绰号Sura的意思是“小腿”。前81年，他被控制罗马的独裁者[卢基乌斯·科尔内利乌斯·苏拉指控挪用公款](../Page/卢基乌斯·科尔内利乌斯·苏拉.md "wikilink")（科尔内利乌斯·雷恩图卢斯是那一年的[财务官](../Page/财务官.md "wikilink")）。科尔内利乌斯·雷恩图卢斯拒绝承认任何指控，反而高傲地向法官露出小腿表示蔑视（古罗马的男孩在玩球类游戏而闯祸时会被抽打小腿）。

普布利乌斯·科尔内利乌斯·雷恩图卢斯在前75年当选为[裁判官](../Page/裁判官.md "wikilink")，随后于次年以[资深长官的身份被派往](../Page/资深长官.md "wikilink")[西西里任总督](../Page/西西里.md "wikilink")。前71年，他被选举为[执政官](../Page/执政官_\(罗马\).md "wikilink")。

前70年，刚刚卸任的科尔内利乌斯·雷恩图卢斯和另外几个政客一起因“不道德”行为而被逐出[元老院](../Page/罗马元老院.md "wikilink")。这其中可能有政治斗争的因素，因为共和国末期的元老院已极其腐化，许多元老贪婪淫荡。在这种失意的情况下，科尔内利乌斯·雷恩图卢斯加入了[喀提林的集团](../Page/喀提林.md "wikilink")。喀提林是一个[平民派的政客](../Page/平民派.md "wikilink")，其活动的许多性质现在仍无法判断，但总而言之他是反对当权派的。科尔内利乌斯·雷恩图卢斯则沉醉于一个[西比拉](../Page/西比拉.md "wikilink")[神谕](../Page/神谕.md "wikilink")（西比拉是一种女先知），该神谕说“将有三位[科尔内利乌斯氏族的成员统治](../Page/科尔内利乌斯氏族.md "wikilink")[罗马城](../Page/罗马.md "wikilink")”，雷恩图卢斯认为这指的是卢基乌斯·科尔内利乌斯·苏拉、[卢基乌斯·科尔内利乌斯·秦纳和他自己](../Page/卢基乌斯·科尔内利乌斯·秦纳.md "wikilink")，并因此而飘飘然起来。不久他成为喀提林集团的主要人物之一。

前63年[夏天喀提林第](../Page/夏天.md "wikilink")3次竞选执政官失败。不久[西塞罗即发表第一篇](../Page/西塞罗.md "wikilink")[反喀提林演说](../Page/反喀提林演说.md "wikilink")，指控喀提林企图武力夺权。喀提林被迫逃离罗马，并在[意大利各地聚集力量准备武装起事](../Page/意大利.md "wikilink")。雷恩图卢斯负责领导喀提林集团中留在罗马城的人，他与另一个重要头目[盖乌斯·科尔内利乌斯·凯泰古斯一起](../Page/盖乌斯·科尔内利乌斯·凯泰古斯.md "wikilink")，准备谋杀西塞罗并在罗马城中放火。结果这个阴谋因雷恩图卢斯自己的卤莽和轻率而败露。

当时有一个[阿洛布羅基人](../Page/阿洛布羅基人.md "wikilink")（[高卢的一个](../Page/高卢.md "wikilink")[凯尔特民族](../Page/凯尔特人.md "wikilink")）的使节正在罗马。阿洛布羅基人派使者到罗马是为了向元老院控诉当地总督对他们的压迫。科尔内利乌斯·雷恩图卢斯企图得到外部武力支持，于是与该使者接近，希望与其部落结盟，并草率地吐露了自己的计划。结果该使者另有打算，他假装支持阴谋者的计划，设法弄到了几个喀提林集团领袖亲笔签名的文件，随后就向昆图斯·费边·桑加（阿洛布羅基人在罗马的庇护人）告发了这起阴谋。昆图斯·费边·桑加随即通知了西塞罗，后者正不遗余力地向元老院鼓吹喀提林阴谋的严重性并主张镇压喀提林集团。西塞罗得知此事后立刻采取行动，很快罗马城中的喀提林集团成员便全数被捕。西塞罗因为急于行事，未经正式审判而只通过[元老院决议就将被抓获的](../Page/元老院决议.md "wikilink")5名喀提林集团领袖判处死刑。这在当时可能是必要的（因为其他阴谋者可能援救他们的同伙），但后来终于给西塞罗带来了政治灾难（不经审判而处死罗马公民属于违法行为）。雷恩图卢斯于前63年12月5日在[图利安努姆监狱被处死](../Page/图利安努姆监狱.md "wikilink")。

## 资料来源

  - [卡西乌斯·迪奥](../Page/卡西乌斯·迪奥.md "wikilink")，xxxvii. 30，xlvi. 20

  - [普鲁塔克](../Page/普鲁塔克.md "wikilink")，《西塞罗》，17

  - [萨卢斯特](../Page/萨卢斯特.md "wikilink")，《喀提林》

  - [西塞罗](../Page/西塞罗.md "wikilink")，《[反对喀提林](../Page/反对喀提林.md "wikilink")》，iii.；iv.

  -
[Category:前63年逝世](../Category/前63年逝世.md "wikilink")
[Category:罗马执政官](../Category/罗马执政官.md "wikilink")
[Category:古罗马政治人物](../Category/古罗马政治人物.md "wikilink")
[Category:罗马共和国执政官](../Category/罗马共和国执政官.md "wikilink")
[Category:生年不詳](../Category/生年不詳.md "wikilink")