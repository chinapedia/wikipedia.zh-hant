**ADAMS**（的缩写，也称MD ADAMS，即），是由[美国](../Page/美国.md "wikilink")[Mechanical
Dynamics
Inc.公司开发的集](../Page/MSC_Software.md "wikilink")[建模](../Page/建模.md "wikilink")、求解、[可视化技术于一体的](../Page/可视化技术.md "wikilink")[虚拟样机软件](../Page/虚拟样机软件.md "wikilink")，是目前世界上使用最多的机械系统仿真分析软件。这套软件可以产生复杂机械系统的[虚拟样机](../Page/虚拟样机.md "wikilink")，真实地仿真其运动过程，并快速地分析比较多参数方案，以获得优化的工作性能，从而减少物理样机制造及试验次数，提高产品设计质量并缩短产品研制周期和费用。这套软件在[机械制造](../Page/机械制造.md "wikilink")、[汽车](../Page/汽车.md "wikilink")[交通](../Page/交通.md "wikilink")、[航空](../Page/航空.md "wikilink")[航天](../Page/航天.md "wikilink")、[铁道](../Page/铁道.md "wikilink")、[兵器](../Page/兵器.md "wikilink")、[石油化工等领域都得到应用](../Page/石油化工.md "wikilink")。

ADAMS软件包含[FORTRAN和](../Page/FORTRAN.md "wikilink")[C++两个版本的求解器](../Page/C++.md "wikilink")，前者兼容性较好，而后者功能较全面。其标准版包含以下几个基本模块\[1\]：

  - Adams/Solver
  - Adams/View
  - Adams/Postprocessor
  - Adams/Insight
  - Adams/Exchange
  - Adams/Linear
  - Shared Memory Parallel (SMP)

以及进阶模块

  - Adams/Flex
  - Adams/ViewFlex
  - Adams/Durability
  - Adams/Vibration
  - Adams/Vibration solver
  - Adams/Controls
  - Adams/Machinery

## 参考文献

[Category:三维图像软件](../Category/三维图像软件.md "wikilink")
[Category:電腦輔助設計軟體](../Category/電腦輔助設計軟體.md "wikilink")
[Category:電腦輔助工程軟體](../Category/電腦輔助工程軟體.md "wikilink")
[Category:電腦輔助製造軟體](../Category/電腦輔助製造軟體.md "wikilink")
[Category:產品生命周期管理](../Category/產品生命周期管理.md "wikilink")

1.