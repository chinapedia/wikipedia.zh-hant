**R语言**，一種[自由軟體](../Page/自由軟體.md "wikilink")[程式語言與操作環境](../Page/程式語言.md "wikilink")，主要用于[统计分析](../Page/统计.md "wikilink")、绘图、[数据挖掘](../Page/数据挖掘.md "wikilink")。R本來是由來自[新西蘭](../Page/新西蘭.md "wikilink")[奧克蘭大學的](../Page/奧克蘭大學.md "wikilink")[罗斯·伊哈卡和](../Page/罗斯·伊哈卡.md "wikilink")[罗伯特·杰特曼開發](../Page/罗伯特·杰特曼.md "wikilink")（也因此稱為R），現在由“R開發核心團隊”負責開發。R基于[S语言的一个](../Page/S语言.md "wikilink")[GNU計劃项目](../Page/GNU計劃.md "wikilink")，所以也可以当作S语言的一种实现，通常用S语言编写的代码都可以不作修改的在R环境下运行。R的語法是來自[Scheme](../Page/Scheme.md "wikilink")。

R的[原始碼可自由下載使用](../Page/原始碼.md "wikilink")，亦有已編譯的[執行檔版本可以下載](../Page/執行檔.md "wikilink")，可在多种平台下运行，包括UNIX（也包括FreeBSD和Linux）、Windows和MacOS。R主要是以[命令行操作](../Page/命令行界面.md "wikilink")，同時有人開發了幾種[圖形用戶界面](../Page/圖形用戶界面.md "wikilink")，其中[RStudio是最为广泛使用的](../Page/RStudio.md "wikilink")[整合開發環境](../Page/整合開發環境.md "wikilink")。

## 功能

R內建多種統計學及數字分析功能。R的功能也可以透過安裝[套件](../Page/軟體套件.md "wikilink")（Packages，用戶撰寫的功能）增強。因為S的血緣，R比其他統計學或數學專用的編程語言有更強的[物件導向](../Page/物件導向.md "wikilink")（面向对象程序设计,
S3, S4等）功能。

R的另一強項是繪圖功能，製圖具有印刷的素質，也可加入數學符號。

雖然R主要用於統計分析或者開發統計相關的軟體，但也有人用作[矩陣計算](../Page/矩陣.md "wikilink")。其分析速度可媲美专用于矩阵计算的自由软件[GNU
Octave和商業軟件](../Page/GNU_Octave.md "wikilink")[MATLAB](../Page/MATLAB.md "wikilink")。\[1\]

## 套件

R的功能能夠透過由用戶撰寫的套件增強。增加的功能有特殊的統計技術、繪圖功能，以及編程介面和數據輸出／輸入功能。這些軟件包是由R語言、[LaTeX](../Page/LaTeX.md "wikilink")、[Java及最常用](../Page/Java.md "wikilink")[C語言和](../Page/C語言.md "wikilink")[Fortran撰寫](../Page/Fortran.md "wikilink")。下載的執行檔版本會連同一批核心功能的軟件包，而根據CRAN紀錄有七千多種不同的軟件包。其中有幾款較為常用，例如用於[經濟計量](../Page/計量經濟學.md "wikilink")、[財經分析](../Page/財經分析.md "wikilink")、[人文科學與社會科學研究以及](../Page/人文科學.md "wikilink")[人工智能](../Page/人工智能.md "wikilink")。\[2\]

## 發展

[生物信息学社群時常使用R進行分子生物學數據分析](../Page/生物信息学.md "wikilink")。計劃就是讓R作為基因圖譜分析工具。
[Gnumeric開發者正和R開發者合作](../Page/Gnumeric.md "wikilink")，改善Gnumeric計算結果的精確度。\[3\]

## CRAN

CRAN為Comprehensive R Archive
Network（R綜合典藏網）的簡稱。它除了收藏了R的執行檔下載版、原始碼和說明文件，也收錄了各種用戶撰寫的軟件包。現時，全球有超過一百個CRAN鏡像站。

## R新聞雜誌

《R新聞雜誌》（*R
Newsletter*）每年會出版兩至三次，為一份免費的電子雜誌，內容有關統計學軟件發展及R語言開發資訊。第一期在2001年一月出版。\[4\]

## 参考文献

## 外部链接

  - [R語言官方主頁](http://www.r-project.org/)

## 参见

  - [SPSS](../Page/SPSS.md "wikilink") - 另一種統計分析軟件
  - [Stata](../Page/Stata.md "wikilink") - 另一種統計分析軟件
  - [SAS系統](../Page/SAS系統.md "wikilink") - 另一種統計分析軟件
  - [S-PLUS](../Page/S-PLUS.md "wikilink") - 同樣建基於S語言的統計分析軟件

{{-}}

[Category:程序设计语言](../Category/程序设计语言.md "wikilink")
[Category:统计软件](../Category/统计软件.md "wikilink")
[Category:统计编程语言](../Category/统计编程语言.md "wikilink")
[Category:GNU计划软件](../Category/GNU计划软件.md "wikilink")
[Category:R語言](../Category/R語言.md "wikilink")
[Category:自由圖表軟件](../Category/自由圖表軟件.md "wikilink")

1.
2.
3.
4.  <http://cran.r-project.org/doc/Rnews/>