**希尤縣**（，或稱）是[愛沙尼亞西北部的一個縣](../Page/愛沙尼亞.md "wikilink")，主要由爱沙尼亚第二大岛[希烏馬島和附近小島組成](../Page/希烏馬島.md "wikilink")。面積1,023平方公里。2004年人口為10,289人，兩者皆為全國之末。首府[凱爾德拉](../Page/凱爾德拉.md "wikilink")。南接[萨雷县](../Page/萨雷县.md "wikilink")，东邻[莱内县](../Page/莱内县.md "wikilink")。

## 行政区划

[Hiiu_municipalities.png](https://zh.wikipedia.org/wiki/File:Hiiu_municipalities.png "fig:Hiiu_municipalities.png")
希尤县下属原有一个市（linn）和四个镇（vallad），但2013年10月20日后，[凯尔德拉市和](../Page/凯尔德拉.md "wikilink")[科尔格萨雷](../Page/科尔格萨雷.md "wikilink")（Kõrgessaare）镇合并，成立了新的[希尤镇](../Page/希尤镇.md "wikilink")（Hiiu
vald）。\[1\]现有四个镇。

## 参考资料

[Category:愛沙尼亞行政區劃](../Category/愛沙尼亞行政區劃.md "wikilink")

1.  [Siseministeerium](https://www.siseministeerium.ee/tekib-seitse-tugevat-valda/)
     Tekib seitse tugevat valda. 2013