**壳斗目**（[學名](../Page/學名.md "wikilink")：Fagales），又名**山毛櫸目**，在[生物分类学上属](../Page/生物分类学.md "wikilink")[真双子叶植物之下的](../Page/真双子叶植物.md "wikilink")[豆类植物分支](../Page/豆类植物.md "wikilink")，包括很多最常见的树木。[被子植物的一](../Page/被子植物.md "wikilink")[目](../Page/目.md "wikilink")，旧属木兰纲金缕梅亚纲，含7科\[1\]，均为乔木或灌木。特征为坚果外包有壳斗。单叶，通常互生。花单性，雌雄同株。雄花聚生成柔荑花序。雌花单生或簇生，外包一轮鳞片，后鳞片发育为壳斗。

## 分布

多数种类原产于温带和亚热带地区，但少数种类，主要是桦木科和种类，分布到北极和阿尔卑斯气候带木本植物的特征极限区域。與[松柏門類似](../Page/松柏門.md "wikilink")，演化出許多耐寒樹種。

## 分类

在[克朗奎斯特分类法上是](../Page/克朗奎斯特分类法.md "wikilink")[双子叶植物纲](../Page/双子叶植物纲.md "wikilink")[金縷梅亞綱中的一个目](../Page/金縷梅亞綱.md "wikilink")。

在[APG
分类法中](../Page/APG_分类法.md "wikilink")，**壳斗目**是[真双子叶植物分支中](../Page/真双子叶植物.md "wikilink")[蔷薇类植物分支中](../Page/蔷薇类植物.md "wikilink")[豆类植物分支中的一目](../Page/豆类植物.md "wikilink")。\[2\]

## 参考文献

[壳斗目](../Category/壳斗目.md "wikilink")
[Category:金缕梅亚纲](../Category/金缕梅亚纲.md "wikilink")

1.
2.