**2月28日**是[公历一年中的第](../Page/公历.md "wikilink")59天，离全年结束还有306天（[闰年则还有](../Page/闰年.md "wikilink")307天）。

## 大事记

### 前3世紀

  - [前202年](../Page/前202年.md "wikilink")：[刘邦称帝](../Page/刘邦.md "wikilink")，是為[汉高祖](../Page/汉高祖.md "wikilink")，国號為[汉](../Page/汉朝.md "wikilink")，史稱[西漢](../Page/西漢.md "wikilink")，初定都[雒陽](../Page/雒陽.md "wikilink")，後遷都[长安](../Page/长安.md "wikilink")。

### 9世紀

  - [833年](../Page/833年.md "wikilink")：[淳和天皇退位](../Page/淳和天皇.md "wikilink")，[仁明天皇即位](../Page/仁明天皇.md "wikilink")。

### 18世紀

  - [1784年](../Page/1784年.md "wikilink")：[美以美會成立](../Page/美以美會.md "wikilink")。

### 19世紀

  - [1893年](../Page/1893年.md "wikilink")：[美国海军仿造其他国家的战舰所建造的](../Page/美国海军.md "wikilink")[印第安纳级首艘战舰](../Page/印第安纳级战舰.md "wikilink")[印第安纳号战舰正式下水](../Page/印第安纳号战列舰_\(BB-1\).md "wikilink")。

### 20世紀

  - [1919年](../Page/1919年.md "wikilink")：[阿富汗宣布独立](../Page/阿富汗.md "wikilink")。
  - [1922年](../Page/1922年.md "wikilink")：[英国承认](../Page/英国.md "wikilink")[埃及獨立](../Page/埃及.md "wikilink")，但仍保留在[国防](../Page/国防.md "wikilink")、[外交等方面的特权](../Page/外交.md "wikilink")。
  - [1935年](../Page/1935年.md "wikilink")：[美国](../Page/美国.md "wikilink")[杜邦公司](../Page/杜邦公司.md "wikilink")[华莱士·卡罗瑟斯的研究组首次合成](../Page/华莱士·卡罗瑟斯.md "wikilink")[尼龍](../Page/尼龍.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[臺灣在](../Page/臺灣.md "wikilink")[國民政府接管後因種種倒行逆施與貪腐導致民不聊生引發民怨](../Page/國民政府.md "wikilink")，由一起[緝菸血案引爆](../Page/緝菸血案.md "wikilink")[二二八事件](../Page/二二八事件.md "wikilink")，各地發生軍民衝突，政府派遣軍隊進行鎮壓屠殺與[清鄉導致臺籍菁英傷亡慘重](../Page/清鄉.md "wikilink")，海外[臺灣獨立運動由此展開](../Page/臺灣獨立運動.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[尼克松在结束](../Page/理查德·尼克松.md "wikilink")[访华之前](../Page/1972年尼克松访华.md "wikilink")，在[上海与](../Page/上海.md "wikilink")[中国国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")[周恩来共同发布](../Page/周恩来.md "wikilink")《[联合公报](../Page/中美三个联合公报.md "wikilink")》。
  - [1972年](../Page/1972年.md "wikilink")：[日本警方攻入](../Page/日本.md "wikilink")[輕井澤淺間山莊](../Page/輕井澤.md "wikilink")，逮捕[聯合赤軍成員並解救全部人質](../Page/日本赤軍.md "wikilink")，[淺間山莊事件落幕](../Page/淺間山莊事件.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[倫敦地鐵](../Page/倫敦地鐵.md "wikilink")[沼澤門站發生](../Page/沼澤門站.md "wikilink")[嚴重撞車事故](../Page/沼澤門地鐵撞車事故.md "wikilink")，造成43人死亡。
  - [1980年](../Page/1980年.md "wikilink")：[臺灣省議員](../Page/台灣省議會.md "wikilink")[林義雄住處發生](../Page/林義雄.md "wikilink")[林宅滅門血案](../Page/林宅滅門血案.md "wikilink")，其母與其雙胞胎女兒慘遭殺害，大女兒身受重傷。
  - [1984年](../Page/1984年.md "wikilink")：[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[CT270型及](../Page/台鐵CT270型蒸汽機車.md "wikilink")[DT650型](../Page/台鐵DT650型蒸汽機車.md "wikilink")[蒸氣機車正式報廢](../Page/蒸氣機車.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[瑞典首相](../Page/瑞典首相.md "wikilink")[奥洛夫·帕尔梅在](../Page/奥洛夫·帕尔梅.md "wikilink")[斯德哥尔摩看完](../Page/斯德哥尔摩.md "wikilink")[电影回家途中](../Page/电影.md "wikilink")[遭枪击身亡](../Page/帕爾梅遇刺案.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[武陵源](../Page/武陵源.md "wikilink")、[九寨沟](../Page/九寨沟.md "wikilink")、[黄龙被列入](../Page/黄龙风景名胜区.md "wikilink")[世界遗产名录](../Page/世界遗产名录.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[美國](../Page/美國.md "wikilink")[德州爆發](../Page/德州.md "wikilink")[韋科慘案](../Page/韋科慘案.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：美国发生[北好莱坞抢劫案](../Page/北好莱坞抢劫案.md "wikilink")，两名全副武装的银行抢匪与[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[洛杉矶警察局警员交火](../Page/洛杉矶警察局.md "wikilink")。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[印度爆发](../Page/印度.md "wikilink")[宗教冲突](../Page/2002年古吉拉特騷亂.md "wikilink")，[印度教徒烧毁](../Page/印度教.md "wikilink")[穆斯林房屋](../Page/穆斯林.md "wikilink")，造成55人死亡。
  - [2006年](../Page/2006年.md "wikilink")：[中華民國總統](../Page/中華民國總統.md "wikilink")[陳水扁凍結](../Page/陳水扁.md "wikilink")[國家統一綱領](../Page/國家統一綱領.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[梵蒂岡第](../Page/梵蒂岡.md "wikilink")265任[教宗](../Page/教宗.md "wikilink")[本篤十六世卸任](../Page/本篤十六世.md "wikilink")。
  - [2018年](../Page/2018年.md "wikilink")：[美國](../Page/美國.md "wikilink")[參議院一致通過](../Page/美國參議院.md "wikilink")[台灣旅行法](../Page/台灣旅行法.md "wikilink")，送交[美國](../Page/美國.md "wikilink")[白宮](../Page/白宮.md "wikilink")。[美國總統](../Page/美國總統.md "wikilink")[唐納·川普於](../Page/唐納·川普.md "wikilink")3月16日正式簽署成為美國法律，[台灣旅行法正式生效](../Page/台灣旅行法.md "wikilink")。

## 出生

  - [1119年](../Page/1119年.md "wikilink")：[金熙宗完颜亶](../Page/金熙宗.md "wikilink")，[金朝第三位皇帝](../Page/金朝.md "wikilink")（逝於[1150年](../Page/1150年.md "wikilink")）
  - [1833年](../Page/1833年.md "wikilink")：[阿尔弗雷德·冯·施里芬](../Page/阿尔弗雷德·冯·施里芬.md "wikilink")，[德国軍事家](../Page/德国.md "wikilink")（逝於[1913年](../Page/1913年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[马连良](../Page/马连良.md "wikilink")，[中國](../Page/中國.md "wikilink")[京剧表演](../Page/京剧.md "wikilink")[艺术家](../Page/艺术家.md "wikilink")（逝於[1966年](../Page/1966年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[阿卜杜勒·薩塔·伊迪](../Page/阿卜杜勒·薩塔·伊迪.md "wikilink")，[巴基斯坦著名慈善家](../Page/巴基斯坦.md "wikilink")（[2016年去世](../Page/2016年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[法蘭克·蓋瑞](../Page/法蘭克·蓋瑞.md "wikilink")，[美國](../Page/美國.md "wikilink")[後現代主義](../Page/後現代主義.md "wikilink")、[解構主義](../Page/解構主義.md "wikilink")[建築師](../Page/建築師.md "wikilink")
  - [1931年](../Page/1931年.md "wikilink")：[司徒華](../Page/司徒華.md "wikilink")，[香港政治人物](../Page/香港.md "wikilink")（逝於[2011年](../Page/2011年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：，[韓國女歌手](../Page/韓國.md "wikilink")
  - [1941年](../Page/1941年.md "wikilink")：[曹宏威](../Page/曹宏威.md "wikilink")，[香港科学家](../Page/香港.md "wikilink")、政治家
  - [1948年](../Page/1948年.md "wikilink")：[朱棣文](../Page/朱棣文.md "wikilink")，美国华裔物理学家，国家能源部长，97年[诺贝尔物理奖得主](../Page/诺贝尔物理奖.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[高凌風](../Page/高凌風.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")（逝於[2014年](../Page/2014年.md "wikilink")[2月17日](../Page/2月17日.md "wikilink")）
  - [1960年](../Page/1960年.md "wikilink")：[大川透](../Page/大川透.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[高坂希太郎](../Page/高坂希太郎.md "wikilink")，[日本動畫監督](../Page/日本動畫.md "wikilink")、[人物設計師](../Page/人物設計師.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[奧田美香](../Page/奧田美香.md "wikilink")，[日本前女子偶像團體](../Page/日本.md "wikilink")[小貓俱樂部成員](../Page/小貓俱樂部.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[崔秉默](../Page/崔秉默.md "wikilink")，[韓國男](../Page/韓國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[阿桑](../Page/阿桑.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")（逝於[2009年](../Page/2009年.md "wikilink")）
  - [1975年](../Page/1975年.md "wikilink")：[金惠珍](../Page/金惠珍.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[早水理沙](../Page/早水理沙.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[KAORI](../Page/KAORI.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[潘慧如](../Page/潘慧如.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[泰夏安·普林斯](../Page/泰夏安·普林斯.md "wikilink")，[美國籃球運動員](../Page/美國.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[崔成希](../Page/Bada_\(歌手\).md "wikilink")，[韓國女歌手](../Page/韓國.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[娜塔莉亞·沃迪亞諾娃](../Page/娜塔莉亞·沃迪亞諾娃.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")[超級名模](../Page/超級名模.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[嘉露蓮娜·庫高娃](../Page/嘉露蓮娜·庫高娃.md "wikilink")，[捷克](../Page/捷克.md "wikilink")[超級名模](../Page/超級名模.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[凌琮](../Page/凌琮.md "wikilink")，[中國足球運動員](../Page/中國.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[莊鵑瑛](../Page/莊鵑瑛.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[三瓶由布子](../Page/三瓶由布子.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[奧莉維亞·巴勒莫](../Page/奧莉維亞·巴勒莫.md "wikilink")，[美國社交名媛](../Page/美國.md "wikilink")、模特兒、演員
  - [1986年](../Page/1986年.md "wikilink")：[具在伊](../Page/具在伊.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[王子文](../Page/王子文.md "wikilink")，[中國](../Page/中國.md "wikilink")[大陸女演員](../Page/大陸.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[麥美恩](../Page/麥美恩.md "wikilink")，[香港無線電視藝員](../Page/香港.md "wikilink")、[2011年度香港小姐競選殿軍](../Page/2011年度香港小姐競選.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[Angelababy](../Page/Angelababy.md "wikilink")，[香港模特兒及藝人](../Page/香港.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[山本光](../Page/山本光.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[山北早紀](../Page/山北早紀.md "wikilink")，[日本女子組合](../Page/日本.md "wikilink")[i☆Ris成員](../Page/i☆Ris.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[莎拉·寶嘉](../Page/莎拉·寶嘉.md "wikilink")，[愛爾蘭女演員](../Page/愛爾蘭.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[孫瑜](../Page/孫瑜_\(羽毛球運動員\).md "wikilink")，[中國女子羽毛球運動員](../Page/中國.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[张凌峰](../Page/张凌峰.md "wikilink")，[中國足球运动员](../Page/中國.md "wikilink")
  - [2000年](../Page/2000年.md "wikilink")：[上白石萌歌](../Page/上白石萌歌.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")

## 逝世

  - [1648年](../Page/1648年.md "wikilink")：[克里斯蒂安四世](../Page/克里斯蒂安四世.md "wikilink")，[丹麥國王和](../Page/丹麥國王.md "wikilink")[挪威國王](../Page/挪威國王.md "wikilink")（[1577年出生](../Page/1577年.md "wikilink")）
  - [1772年](../Page/1772年.md "wikilink")：[劉星煒](../Page/劉星煒.md "wikilink")，[清朝官員](../Page/清朝.md "wikilink")，[學政](../Page/學政.md "wikilink")、[禮部侍郎](../Page/禮部侍郎.md "wikilink")、[工部侍郎](../Page/工部侍郎.md "wikilink")（生於[1718年](../Page/1718年.md "wikilink")）
  - [1775年](../Page/1775年.md "wikilink")：[孝儀純皇后](../Page/孝儀純皇后.md "wikilink")，[清朝皇后](../Page/清朝.md "wikilink")，[嘉慶帝生母](../Page/嘉慶帝.md "wikilink")（生於[1727年](../Page/1727年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[亨利·詹姆斯](../Page/亨利·詹姆斯.md "wikilink")，[美國](../Page/美國.md "wikilink")[作家](../Page/作家.md "wikilink")（[1843年出生](../Page/1843年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[弗里德里希·艾伯特](../Page/弗里德里希·艾伯特.md "wikilink")，[德國首任](../Page/德國.md "wikilink")[联邦大總統](../Page/联邦大總統.md "wikilink")（[1871年出生](../Page/1871年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[璧利南](../Page/璧利南.md "wikilink")，英國外交官（[1847年出生](../Page/1847年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[刘震东](../Page/刘震东.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")（[1893年出生](../Page/1893年.md "wikilink")）
  - [1941年](../Page/1941年.md "wikilink")：[阿方索十三世](../Page/阿方索十三世.md "wikilink")，[西班牙國王](../Page/西班牙國王.md "wikilink")（[1886年出生](../Page/1886年.md "wikilink")）
  - [1975年](../Page/1975年.md "wikilink")：[焦菊隱](../Page/焦菊隱.md "wikilink")，[中國戲劇家](../Page/中國.md "wikilink")（[1905年出生](../Page/1905年.md "wikilink")）
  - [1986年](../Page/1986年.md "wikilink")：[奥洛夫·帕尔梅](../Page/奥洛夫·帕尔梅.md "wikilink")，[瑞典首相](../Page/瑞典.md "wikilink")，遭暗殺（[1927年出生](../Page/1927年.md "wikilink")）
  - [1987年](../Page/1987年.md "wikilink")：[瓊·格林伍德](../Page/瓊·格林伍德.md "wikilink")，[英國女演員](../Page/英國.md "wikilink")（[1921年出生](../Page/1921年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[本多豬四郎](../Page/本多豬四郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[導演](../Page/導演.md "wikilink")（[1911年出生](../Page/1911年.md "wikilink")）
  - [1994年](../Page/1994年.md "wikilink")：[爱新觉罗·溥杰](../Page/爱新觉罗·溥杰.md "wikilink")，[清朝末代皇帝](../Page/清朝.md "wikilink")[爱新觉罗·溥仪的弟弟](../Page/爱新觉罗·溥仪.md "wikilink")（[1907年出生](../Page/1907年.md "wikilink")）
  - [1999年](../Page/1999年.md "wikilink")：[冰心](../Page/冰心.md "wikilink")，中國作家（[1900年出生](../Page/1900年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[于若木](../Page/于若木.md "wikilink")，[中國營養學家](../Page/中國.md "wikilink")，[陳雲夫人](../Page/陳雲.md "wikilink")（[1919年出生](../Page/1919年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[畢特利](../Page/畢特利.md "wikilink")，[香港足球界人士](../Page/香港.md "wikilink")（[1932年出生](../Page/1932年.md "wikilink")）

## 节日、风俗习惯

[228_memorial_stone_blocks,_228_Memorial_Park,_Chiayi_City_(Taiwan).jpg](https://zh.wikipedia.org/wiki/File:228_memorial_stone_blocks,_228_Memorial_Park,_Chiayi_City_\(Taiwan\).jpg "fig:228_memorial_stone_blocks,_228_Memorial_Park,_Chiayi_City_(Taiwan).jpg")二二八紀念公園內刻上228數字的刻石群\]\]

  - 的[二二八和平紀念日](../Page/二二八和平紀念日.md "wikilink")：紀念發生於[1947年](../Page/1947年.md "wikilink")2月28日的[二二八事件](../Page/二二八事件.md "wikilink")，全國放假一天（自[2006年起](../Page/2006年.md "wikilink")，每年二二八和平紀念日當天，全國各級機關學校、團體都要[降半旗](../Page/降半旗.md "wikilink")，以表示對二二八事件受難者最深的哀悼之意）。