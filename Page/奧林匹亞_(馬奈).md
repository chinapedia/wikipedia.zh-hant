《**奥林匹亚**》是[法国写实派画家](../Page/法国.md "wikilink")[愛德華·馬奈创作于](../Page/愛德華·馬奈.md "wikilink")1863年的一副油画。该画受乔尔乔内《[沉睡的维纳斯](../Page/沉睡的维纳斯.md "wikilink")》，[提香的](../Page/提香.md "wikilink")《[乌尔比诺的维纳斯](../Page/乌尔比诺的维纳斯.md "wikilink")》及[戈雅的](../Page/戈雅.md "wikilink")《[裸体的马哈](../Page/裸体的马哈.md "wikilink")》的影响。现藏于[巴黎的](../Page/巴黎.md "wikilink")[奥塞美术馆](../Page/奥塞美术馆.md "wikilink")。

马奈展出这幅画以后受到大众的批判，因为大众认为这不仅仅是思想自由或者表现了女性裸体的问题，而是将[维纳斯画成了娼妓](../Page/维纳斯.md "wikilink")\[1\]。所有画中女子的装饰，如头上插的兰花、身上的首饰都暗示着她是一名妓女而非女神。实际上“奥林匹亚”就是1860年代巴黎娼妓的代名词。\[2\]

## 批评

[Paul_Cezanne_A_Modern_Olympia_painting.jpg](https://zh.wikipedia.org/wiki/File:Paul_Cezanne_A_Modern_Olympia_painting.jpg "fig:Paul_Cezanne_A_Modern_Olympia_painting.jpg")，《现代奥林匹亚》，1873/74\]\]
《奥林匹亚》在1865年[巴黎沙龙展出之后](../Page/巴黎沙龙.md "wikilink")，引发了比两年前的《[草地上的午餐](../Page/草地上的午餐.md "wikilink")》更为激烈的社会反响，保守派人士为其打上“不道德”、“俗气”的标签。记者[安東尼·普魯斯特后来回忆道](../Page/安東尼·普魯斯特.md "wikilink")：“（在沙龙上）奥林匹亚没被人们毁掉的唯一原因是主办方的保护措施太好了”。\[3\]

## 参考文献

## 外部链接

  - [Culture Shock: The TV Series and Beyond: The Shock of the Nude:
    Manet's
    Olympia](http://www.pbs.org/wgbh/cultureshock/beyond/manet.html)

[Category:愛德華·馬奈畫作](../Category/愛德華·馬奈畫作.md "wikilink")
[Category:奧塞美術館藏品](../Category/奧塞美術館藏品.md "wikilink")
[Category:1863年畫作](../Category/1863年畫作.md "wikilink")

1.
2.  [Clark, T.J.](../Page/T._J._Clark_\(art_historian\).md "wikilink")
    (1999) *The Painting of Modern Life: Paris in the Art of Manet and
    His Followers.* Revised edition. Princeton: Princeton University
    Press, p.86.
3.  Quoted in [Honour, H.](../Page/Hugh_Honour.md "wikilink") and J.
    Fleming, (2009) *A World History of Art*. 7th edn. London: Laurence
    King Publishing, p. 708.