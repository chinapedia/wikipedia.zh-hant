**贺炳炎**（），原名**向明炎**，[湖北](../Page/湖北省_\(中華民國\).md "wikilink")[宜都江家湾人](../Page/宜都.md "wikilink")\[1\]。[中国人民解放军高级将领](../Page/中国人民解放军.md "wikilink")，人称“**独臂将军**”。

贺炳炎1929年随父賀學文加入[中国工农红军](../Page/中国工农红军.md "wikilink")，历任[红三军手枪大队区队长](../Page/红三军.md "wikilink")、大队长、襄北独立团团长、红八师二十二团团长、红二十团团长、红二十九团团长、新兵大队队长、黔东独立师师长、红六师十八团团长、[红二军团五师师长](../Page/红二军团.md "wikilink")、六师师长，参加[长征](../Page/长征.md "wikilink")。长征期间身负重伤，右臂截肢。[抗日战争时期](../Page/抗日战争.md "wikilink")，任[八路军](../Page/八路军.md "wikilink")[120师](../Page/120师.md "wikilink")[358旅](../Page/358旅.md "wikilink")716团团长、120师第3支队支队长、358旅副旅长。[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，任[晋北野战军副司令员](../Page/晋北野战军.md "wikilink")、晋绥军区第三纵队副司令员兼第五旅旅长、[西北野战军第](../Page/西北野战军.md "wikilink")1纵队司令员、[中国人民解放军第1军军长](../Page/中国人民解放军第1军.md "wikilink")、[青海军区司令员](../Page/青海军区.md "wikilink")。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，任[西南军区副司令员兼](../Page/西南军区.md "wikilink")[四川军区司令员](../Page/四川军区.md "wikilink")、[成都军区司令员等职](../Page/成都军区.md "wikilink")。

## 生平

### 早年革命生涯

母亲晏兰儿早年[守寡](../Page/守寡.md "wikilink")，离异的父亲贺学文[入赘晏兰儿的夫家](../Page/入赘.md "wikilink")——向家，故贺炳炎原名向明言。母亲晏兰儿逝世后改随父姓，更名贺炳炎\[2\]\[3\]。9岁时即随父在[煤矿背煤](../Page/煤矿.md "wikilink")，后当过[裁缝和](../Page/裁缝.md "wikilink")[铁匠](../Page/铁匠.md "wikilink")。1929年随父賀學文加入[中国工农红军贺龙部](../Page/中国工农红军.md "wikilink")，任[贺龙警卫员](../Page/贺龙.md "wikilink")。1930年6月加入[中国共产党](../Page/中国共产党.md "wikilink")。1932年，入湘鄂西红军学校学习并任区队长，毕业后任[红三军手枪大队区队长](../Page/红三军.md "wikilink")、大队长。贺炳炎参加[湘鄂西苏区第四次反围剿作战](../Page/湘鄂西苏区.md "wikilink")，主动率部冲入国军包围圈，接应红8师突出重围，受到总指挥部嘉奖，所部被授予“模范大队”称号\[4\]。

[Red_Army_2.jpg](https://zh.wikipedia.org/wiki/File:Red_Army_2.jpg "fig:Red_Army_2.jpg")、贺炳炎、[关向应](../Page/关向应.md "wikilink")、[王震](../Page/王震.md "wikilink")、[李井泉](../Page/李井泉.md "wikilink")、[朱瑞](../Page/朱瑞_\(1905年\).md "wikilink")、[贺龙](../Page/贺龙.md "wikilink")。后排左起[张子意](../Page/张子意.md "wikilink")、[刘亚球](../Page/刘亚球.md "wikilink")、[廖汉生](../Page/廖汉生.md "wikilink")、[朱明](../Page/朱明.md "wikilink")、[陈伯钧](../Page/陈伯钧.md "wikilink")、[卢冬生](../Page/卢冬生.md "wikilink")\]\]
1932年10月，贺炳炎随红三军从[洪湖苏区突围](../Page/洪湖苏区.md "wikilink")，向湘鄂边转移。途中担任襄北独立团团长，以后又任第22、第25、第19团团长。1933年，[肃反扩大化时](../Page/肃反.md "wikilink")，曾被指为“[改组派](../Page/改组派.md "wikilink")”而遭关押，后被贺龙放出\[5\]，获释后任新兵大队大队长。同年，父亲贺学文在[鹤峰牺牲](../Page/鹤峰.md "wikilink")\[6\]。1934年起任沿河独立团团长、黔东独立师师长、红6师第18团团长等职。1935年，在[后坪战斗中曾只身突破国军阵地](../Page/后坪战斗.md "wikilink")，被誉为“孤胆英雄”\[7\]\[8\]。

1935年11月，贺炳炎任[红二军团第](../Page/红二军团.md "wikilink")5师师长，率部参加[长征](../Page/长征.md "wikilink")。12月，在湖南省[绥宁县](../Page/绥宁县.md "wikilink")\[9\][瓦屋塘战斗中](../Page/瓦屋塘战斗.md "wikilink")，贺炳炎右臂被汤姆子弹击中，在无麻醉剂的情况下，被锯掉右臂\[10\]。1936年7月，[红二方面军成立后](../Page/红二方面军.md "wikilink")，贺炳炎改任第6师师长，担任全军后卫，率部到达陕北\[11\]。[会宁会师后](../Page/会宁.md "wikilink")，贺炳炎因左臂曾经负伤，被送到[西安](../Page/西安.md "wikilink")[广仁医院治疗](../Page/广仁医院.md "wikilink")。

### 抗日战争时期

[抗日战争爆发后](../Page/抗日战争_\(中国\).md "wikilink")，贺任[八路军](../Page/八路军.md "wikilink")[120师](../Page/120师.md "wikilink")[358旅](../Page/358旅.md "wikilink")716团团长，前往[山西参战](../Page/山西.md "wikilink")。11月，指挥[雁门关伏击战](../Page/雁门关伏击战.md "wikilink")，歼灭日军500余人，获得国民政府通令嘉奖\[12\]\[13\]。1938年12月，贺出任120师第3支队支队长，同政委[余秋里合作](../Page/余秋里.md "wikilink")，率队到[大清河北岸扩大根据地](../Page/大清河.md "wikilink")，很快将部队发展起来\[14\]。是年冬，贺炳炎又率支队进入冀中，先后进行了莲子口、北板桥等战斗，粉碎了日军的3路合围。

1940年，贺炳炎奉命率队返回晋绥参加[百团大战和秋季反扫荡](../Page/百团大战.md "wikilink")\[15\]，在[米峪战斗中歼灭日军一个中队](../Page/米峪战斗.md "wikilink")\[16\]。百团大战后，贺炳炎升任358旅副旅长。1941年，贺调回[延安](../Page/延安.md "wikilink")，先后在[延安军政学院](../Page/延安军政学院.md "wikilink")、[延安军事学院和](../Page/延安军事学院.md "wikilink")[中共中央党校学习](../Page/中共中央党校.md "wikilink")。1944年11月，奉命率数百名干部南下，到洪湖地区开辟抗日根据地，担任[新四军鄂豫皖湘赣军区第](../Page/新四军.md "wikilink")3军分区司令员\[17\]。

### 第二次国共内战时期

[1947shanbei.jpg](https://zh.wikipedia.org/wiki/File:1947shanbei.jpg "fig:1947shanbei.jpg")、[刘景范](../Page/刘景范.md "wikilink")、[马文瑞](../Page/马文瑞.md "wikilink")、贺炳炎。\]\]
抗战胜利后，贺炳炎任江汉军区司令员\[18\]。1946年3月，贺炳炎由[董必武从](../Page/董必武.md "wikilink")[宣化店带到](../Page/宣化店.md "wikilink")[重庆](../Page/重庆.md "wikilink")，后经[北平](../Page/北平.md "wikilink")、[张家口回到晋绥](../Page/张家口.md "wikilink")\[19\]。1946年7月，贺炳炎任晋北野战军副司令员，参加[晋北战役](../Page/晋北战役.md "wikilink")。11月，改任晋绥野战军第3纵队副司令员兼第5旅旅长\[20\]。1947年后，任[西北野战军第](../Page/西北野战军.md "wikilink")1纵队副司令员、司令员，率部参加青化砭、羊马河、蟠龙\[21\]、榆林、[沙家店](../Page/沙家店战役.md "wikilink")、岔口、延清等战役\[22\]。

1948年2月，贺炳炎在[宜川战役中](../Page/宜川战役.md "wikilink")，指挥所部攻占瓦子街，封闭友邻部队未能堵住的缺口，保证解放军取得战役的全胜，受到西北野战军司令部通令嘉奖\[23\]。同年10月在[荔北战役中](../Page/荔北战役.md "wikilink")，率部楔入国军防御纵深，对解放军取得战役胜利起了重要作用\[24\]。1949年1月，贺炳炎任[中国人民解放军第1军军长](../Page/中国人民解放军第1军.md "wikilink")\[25\]，率部连克[三原](../Page/三原.md "wikilink")、[岐山](../Page/岐山.md "wikilink")、[陇县](../Page/陇县.md "wikilink")、[秦安等地](../Page/秦安.md "wikilink")。尔后进军[青海](../Page/青海.md "wikilink")。9月攻占[西宁后](../Page/西宁.md "wikilink")，兼任青海军区司令员\[26\]。

### 中华人民共和国成立后

[Pengshaohui_and_hebingyan.jpg](https://zh.wikipedia.org/wiki/File:Pengshaohui_and_hebingyan.jpg "fig:Pengshaohui_and_hebingyan.jpg")合影。\]\]
1952年，贺炳炎调任[西南军区副司令员兼](../Page/西南军区.md "wikilink")[四川军区司令员](../Page/四川军区.md "wikilink")、[四川省体育运动委员会主任](../Page/四川省体育运动委员会.md "wikilink")\[27\]。1955年，出任[成都军区司令员](../Page/成都军区.md "wikilink")\[28\]，被授予上将军衔，获一级[八一勋章](../Page/八一勋章_\(1955年\).md "wikilink")、一级[独立自由勋章](../Page/独立自由勋章.md "wikilink")、一级[解放勋章](../Page/解放勋章.md "wikilink")。贺炳炎同时是第一、第二届[中华人民共和国国防委员会委员](../Page/中华人民共和国国防委员会.md "wikilink")、[第三届全国政协常委](../Page/第三届全国政协.md "wikilink")\[29\]。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，贺炳炎因伤病缠身，长期接受治疗。他有伤16处，并患有高血压、关节炎、支气管哮喘和肾脏疾病，但仍带病坚持工作\[30\]。1960年7月1日，因积劳成疾，贺病逝于[成都](../Page/成都.md "wikilink")\[31\]。

## 家庭

贺炳炎在15岁左右与幼时定亲的兰良秀结婚，两人育有一女贺雷珍。贺炳炎随父参军后，兰良秀留于家乡宜都县，一人抚养女儿，于1935年改嫁\[32\]。

贺炳炎的第二任妻子[姜平](../Page/姜平.md "wikilink")，是[中国人民解放军开国少将](../Page/中国人民解放军开国少将.md "wikilink")[姜齐贤之女](../Page/姜齐贤.md "wikilink")\[33\]。二人于1942年结婚，曾任成都军区门诊部、总参门诊部主任，1955年授[少校军衔](../Page/少校.md "wikilink")，1960年晋升[中校](../Page/中校.md "wikilink")。有子[贺雷生](../Page/贺雷生.md "wikilink")、[贺陵生](../Page/贺陵生.md "wikilink")、[贺贺生](../Page/贺贺生.md "wikilink")；有女[贺北生](../Page/贺北生.md "wikilink")、[贺燕生](../Page/贺燕生.md "wikilink")\[34\]。

## 参考文献

{{-}}

[Category:中国人民解放军开国上将](../Category/中国人民解放军开国上将.md "wikilink")
[Category:成都军区司令员](../Category/成都军区司令员.md "wikilink")
[Category:中华人民共和国国防委员会委员](../Category/中华人民共和国国防委员会委员.md "wikilink")
[Category:第三届全国政协常务委员](../Category/第三届全国政协常务委员.md "wikilink")
[Category:中国共产党党员
(1930年入党)](../Category/中国共产党党员_\(1930年入党\).md "wikilink")
[Category:宜都人](../Category/宜都人.md "wikilink")
[B](../Category/贺姓.md "wikilink")
[Category:中国残疾人](../Category/中国残疾人.md "wikilink")

1.

2.
3.

4.
5.

6.
7.
8.
9.
10.

11.

12.
13.

14.

15.
16.

17.

18.

19.

20.

21.

22.
23.
24.

25.

26.
27.
28.

29.

30.
31.

32.
33.

34.