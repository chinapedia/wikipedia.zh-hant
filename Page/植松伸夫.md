**植松伸夫**（1959年3月21日－）是日本[电子游戏作曲家](../Page/电子游戏音乐.md "wikilink")，以为大多[最终幻想系列作品配乐而知名](../Page/最终幻想系列.md "wikilink")\[1\]\[2\]。植松是一名半自学音乐家，在十一二岁时开始弹钢琴，而[艾尔顿·约翰是对他影响最大的人](../Page/艾尔顿·约翰.md "wikilink")。

植松在1986年加入[史克威尔](../Page/史克威尔.md "wikilink")（后[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")），并遇到了最终幻想创作者[坂口博信](../Page/坂口博信.md "wikilink")。他们一起合作创作了许多电子游戏，其中最为知名的是最终幻想系列。在就职约20年后的2004年，他离开史克威尔艾尼克斯，建立了自己的公司[Smile
Please和音乐制作公司](../Page/Smile_Please.md "wikilink")[Dog Ear
Records](../Page/Dog_Ear_Records.md "wikilink")。他开始以自由音乐作曲人身份，主要为史克威尔艾尼克斯和坂口的开发工作室[雾行者的电子游戏作曲](../Page/雾行者.md "wikilink")。

植松的游戏配乐还有原声和[改编专辑发行](../Page/改编_\(音乐\).md "wikilink")。他创作的一些游戏曲目还在最终幻想音乐会上演出。他的乐曲数次由[格莱美奖得主](../Page/格莱美奖.md "wikilink")，指挥家[阿尼·罗斯在音乐会上指挥](../Page/阿尼·罗斯.md "wikilink")。在2002年至2010年间，他和[福井健一郎](../Page/福井健一郎.md "wikilink")、[关户刚等同事组成了摇滚乐团](../Page/关户刚.md "wikilink")[黑魔导士](../Page/黑魔导士.md "wikilink")，以[电子管风琴等键盘乐器演奏音乐](../Page/电子管风琴.md "wikilink")。乐队以改编摇滚版演绎了植松伸夫的最终幻想配乐作品。他还参与了乐队Earthbound
Papas。

## 传记

### 早年生活

植松伸夫出生于日本[高知县](../Page/高知县.md "wikilink")[高知市](../Page/高知市.md "wikilink")\[3\]。他是一名半自学的音乐家\[4\]，在十一二岁时开始弹钢琴，且未受过任何正式钢琴教育\[5\]。他有一个姐姐也弹钢琴\[6\]。在[神奈川大学主修英语毕业后](../Page/神奈川大学.md "wikilink")，植松在几个业余乐队担任键盘手，并为电视广告作曲\[7\]。在植松于东京一家音乐出租店工作时，一位史克威尔雇员问他是否有兴趣为他们的一些作品创作音乐。尽管植松答应了，却将其当成一份兼职，他也没有想到这将会成为他的全职工作。他称，这是当兼职赚一些钱的方法，并继续他在音乐租赁店的兼职工作\[8\]。

### 史克威尔与黑魔导士时期

植松为史克威尔配乐的首个游戏是1986年的《Cruise Chaser
Blassty》。在史克威尔工作期间，他遇到[坂口博信](../Page/坂口博信.md "wikilink")，坂口问植松是否愿意为他的一些游戏创作音乐，植松答应了\[9\]。在1986和1987年，他为包括[成人游戏](../Page/日本成人游戏.md "wikilink")《Alpha》等数部游戏作曲，但这些游戏都没取得什么成就\[10\]。1987年，植松和坂口合作创作了获得巨大成功的《[最终幻想](../Page/最终幻想_\(游戏\).md "wikilink")》，而坂口原以为这是他在史克威尔的最后一部作品\[11\]。

《最终幻想》的流行激发了植松的电子游戏音乐事业，他之后又为30多部游戏谱曲，其中以最终幻想系列的后续游戏最为杰出。他在1989年为[沙加系列首作](../Page/沙加系列.md "wikilink")《[魔界塔士
沙加](../Page/魔界塔士_沙加.md "wikilink")》谱曲，之后又在[伊藤贤治的协助下](../Page/伊藤贤治.md "wikilink")，为第二作《[沙加2
秘宝传说](../Page/沙加2_秘宝传说.md "wikilink")》（1990年）以及第五作《[浪漫沙加2](../Page/浪漫沙加2.md "wikilink")》（1993年）谱曲\[12\]。在广受好评的1995年游戏《[时空之轮](../Page/时空之轮.md "wikilink")》作曲者[光田康典患消化性溃疡后](../Page/光田康典.md "wikilink")，植松接替完成了配乐\[13\]。1996年植松与他人合作完成了《[前线任务
枪之危机](../Page/前线任务_枪之危机.md "wikilink")》的原声，并独自创作了《[DynamiTracer](../Page/DynamiTracer.md "wikilink")》的配乐。他还创作了[半熟英雄系列三部游戏的音乐](../Page/半熟英雄系列.md "wikilink")\[14\]。

除了电子游戏，他还为2000年动画电影《[剧场版
我的女神](../Page/幸运女神#劇場版.md "wikilink")》创作主题曲，并与最终幻想配器师[滨口史郎合作为动画](../Page/滨口史郎.md "wikilink")《[最终幻想：无限](../Page/最终幻想：无限.md "wikilink")》（2001年）作曲。他还创作了概念专辑《Ten
Plants》，并在1994年发行了个人专辑《[Phantasmagoria](../Page/Phantasmagoria.md "wikilink")》。植松感到逐渐不满与缺乏灵感，便请作曲家[滨涡正志和](../Page/滨涡正志.md "wikilink")[仲野顺也协助创作](../Page/仲野顺也.md "wikilink")《[最终幻想X](../Page/最终幻想X.md "wikilink")》（2001年）的配乐。这是首个非植松独自配乐的最终幻想本传。《[最终幻想XI](../Page/最终幻想XI.md "wikilink")》（2002年）的配乐由植松，创作大多数原声的[水田直志](../Page/水田直志.md "wikilink")，以及[谷冈久美完成](../Page/谷冈久美.md "wikilink")；其中植松只负责11首曲目\[15\]。2003年，他协助[崎元仁为](../Page/崎元仁.md "wikilink")《[最终幻想战略版Advance](../Page/最终幻想战略版Advance.md "wikilink")》配乐，并创作了主题曲\[16\]。

2002年，史克威尔同事[福井健一郎和](../Page/福井健一郎.md "wikilink")[关户刚邀请植松加入他们组建的摇滚乐队](../Page/关户刚.md "wikilink")——一个专注于重新演绎并扩展植松作品的乐团。植松最开始因工作太忙，拒绝了他们的提议；但在以键盘手身份同福井与关户参与一次现场演出后，他决定加入加入他们建立一个乐队\[17\]\[18\]。史克威尔一名雇员松下为乐队选择了名称“[黑魔导士](../Page/黑魔导士.md "wikilink")”\[19\]。2003年，[河盛庆次](../Page/河盛庆次.md "wikilink")、羽入田新和冈宫道生也加入了乐队\[20\]。黑魔导士发行了三张录音室专辑，并在多场音乐会上演出推广专辑。

### 自由作曲家时期

植松在2004年离开史克威尔艾尼克斯，并建立了自己的公司[Smile
Please](../Page/Smile_Please.md "wikilink")\[21\]；他还在2006年创立了音乐制作公司[Dog
Ear
Records](../Page/Dog_Ear_Records.md "wikilink")\[22\]。植松离开的原因是，公司将他办公室从[东京](../Page/东京.md "wikilink")[目黑區搬迁至](../Page/目黑區.md "wikilink")[新宿区](../Page/新宿区.md "wikilink")，而他对新位置不满意.\[23\]。另外他还称，他到了应该逐渐自己把握自己生活的年龄\[24\]。然而他继续以自由作曲家身份为史克威尔艾尼克斯作曲。2005年植松和几位黑魔导士成员[CGI电影](../Page/计算机成像.md "wikilink")《[最终幻想VII
降临之子](../Page/最终幻想VII_降临之子.md "wikilink")》创作配乐。植松仅为《[最终幻想XII](../Page/最终幻想XII.md "wikilink")》（2006年）创作了主题曲\[25\]；植松本获得创作全部配乐的工作，但崎元最终被任命为主作曲家\[26\]。植松最初要创作《[最终幻想XIII](../Page/最终幻想XIII.md "wikilink")》（2010年）的主题曲，但在获得创作《[最终幻想XIV](../Page/最终幻想XIV.md "wikilink")》全配乐的任务后，他决定将此工作交给《最终幻想XIII》的主作曲者滨涡正志\[27\]。

植松还和坂口的开发工作室[雾行者密切合作](../Page/雾行者.md "wikilink")，为他们的[蓝龙系列](../Page/蓝龙.md "wikilink")、《[失落的奥德赛](../Page/失落的奥德赛.md "wikilink")》（2007年）和《[逃离随机迷宫](../Page/逃离随机迷宫.md "wikilink")》（2008年）作曲；他还为取消的游戏《Cry
on》作曲\[28\]。

他为2007年[PlayStation
Portable游戏](../Page/PlayStation_Portable.md "wikilink")《[不可原谅事件档案](../Page/不可原谅事件档案.md "wikilink")》及2008年[街机游戏](../Page/街机.md "wikilink")《[红莲之王](../Page/红莲之王.md "wikilink")》配乐。植松还为2008年游戏《[任天堂明星大乱斗X](../Page/任天堂明星大乱斗X.md "wikilink")》创作了主题曲\[29\]。他为2009年动画《[豹头王传说](../Page/豹头王传说.md "wikilink")》创作音乐，这是植松首度独自为动画连续剧配乐\[30\]。植松还为2009年[任天堂DS游戏](../Page/任天堂DS.md "wikilink")《[樱花笔记](../Page/樱花笔记.md "wikilink")》，以及[Level-5和](../Page/Level-5.md "wikilink")[Brownie
Brown共同开发的](../Page/Brownie_Brown.md "wikilink")2012年任天堂DS游戏《[幻想生活](../Page/幻想生活.md "wikilink")》配乐.\[31\]。植松还参与了电子书《Bilk-0
1946》音乐与剧情的创作\[32\]。他还为2013年[iOS游戏](../Page/iOS.md "wikilink")《[海之号角：未知的海洋怪物](../Page/海之号角：未知的海洋怪物.md "wikilink")》以及预定于2015年发行的《[凤凰计划](../Page/凤凰计划.md "wikilink")》创作配乐。

在2012年古典调频（英国）的最佳300“荣誉殿堂”年度榜单中，植松伸夫为《[最终幻想VII](../Page/最终幻想VII.md "wikilink")》创作的一曲《Aerith's
Theme》票选获得第16名\[33\]。这是电子游戏乐曲首次进入榜单。2013年，“最终幻想系列的原声”票选获得古典调频荣誉殿堂的第3名\[34\]。2014年，「最終幻想系列的原聲」票選獲得古典調頻榮譽殿堂的第7名\[35\]。

2018年9月，植松伸夫宣布因身体原因暂停目前的一切活动，专心于身体恢复。\[36\]

### 个人生活

植松现与他大学时结识的妻子，以及[小猎犬Pao居住在东京](../Page/小猎犬.md "wikilink")。他们在[山梨县](../Page/山梨县.md "wikilink")[山中湖有一套避暑小屋](../Page/山中湖.md "wikilink")\[37\]。在喜欢业余时间看[职业摔跤](../Page/职业摔跤.md "wikilink")、喝啤酒与骑单车\[38\]。植松称他原本想成为一名职业摔跤手\[39\]，这是他年轻时的职业梦想\[40\]。

## 音乐会

[Nobuo.jpg](https://zh.wikipedia.org/wiki/File:Nobuo.jpg "fig:Nobuo.jpg")上，2009年7月11日摄于华盛顿西雅图\]\]

植松伸夫的音乐谱曲在多场音乐会上演出，同时召开的还有各种最终幻想音乐会。2003年德国莱比锡的[交响游戏音乐会是植松最终幻想音乐首次在日本外现场演出](../Page/交响游戏音乐会.md "wikilink")\[41\]。2004年、2006年和2006年的交响游戏音乐会也演出了最终幻想音乐\[42\]。2004年的音乐会世界首演了《最终幻想VII》的《斗殴者》曲目。日本钢琴家本田征治应邀和管弦乐同奏编曲\[43\]。另一首世界首演曲目为《最终幻想VI》的《妖星乱舞》，歌曲以管弦乐、唱诗班和管风琴演奏\[44\]。2007年的音乐会演奏了《最终幻想XI》的《遥远世界》，歌曲由日本歌剧女高音增田泉演唱\[45\]。

日本举行了一系列成功的音乐会演出，如最终幻想音乐会系列“Tour de Japon”。第一场在美国本土的音乐会是“Dear Friends -
Music from Final
Fantasy”，音乐会于2004年5月10日在加利福尼亚洛杉矶的[华特·迪士尼音乐厅举办](../Page/华特·迪士尼音乐厅.md "wikilink")，并由[洛杉矶爱乐与团和](../Page/洛杉矶爱乐与团.md "wikilink")[洛杉矶大师合唱团演奏](../Page/洛杉矶大师合唱团.md "wikilink")。音乐会由[沃斯堡交响乐团指挥家](../Page/沃斯堡交响乐团.md "wikilink")[米格尔·哈斯-贝多亚指挥](../Page/米格尔·哈斯-贝多亚.md "wikilink")\[46\]。因获得积极反响，美国随后又举办了一系列音乐会\[47\]。2005年5月16日，洛杉矶的[吉布森圆形剧场继续举办了音乐会](../Page/吉布森圆形剧场.md "wikilink")“More
Friends: Music from Final
Fantasy”，音乐会由[格莱美奖得主](../Page/格莱美奖.md "wikilink")[阿罗·尼斯指挥](../Page/阿罗·尼斯.md "wikilink")\[48\]。

植松还在幻想曲之夜2004，2004年10月由[卓越交响管弦乐团演奏的首场音乐会中客串登场](../Page/卓越交响管弦乐团.md "wikilink")，这也是他在史克威尔艾尼克斯就职的最后一天\[49\]。

植松的最终幻想音乐还在2006年2月18日横滨太平洋会展中心的“[Voices - Music from Final
Fantasy](../Page/Voices_-_Music_from_Final_Fantasy.md "wikilink")”音乐会上演奏。明星嘉宾有[白鸟英美子](../Page/白鸟英美子.md "wikilink")、[Rikki](../Page/Rikki.md "wikilink")、[增田泉和](../Page/增田泉.md "wikilink")[安潔拉亞季](../Page/安潔拉亞季.md "wikilink")。音乐会专注于最终幻想系列歌曲，并由阿尼·罗斯指挥\[50\]。植松和他的几名作曲家同事均出席2006年5月27日在芝加哥举办的“[Play\!
A Video Game
Symphony](../Page/Play!_A_Video_Game_Symphony.md "wikilink")”世界首演\[51\]；他创作了音乐会的开场鼓号曲\[52\]。他还出席了2006年6月14日瑞典[斯德哥尔摩的欧洲首演](../Page/斯德哥尔摩.md "wikilink")\[53\]，2006年9月30日在加拿大多伦多\[54\]，以及2007年10月10日在意大利佛洛伦萨的演出。植松参加了2007年12月4日在斯德哥尔摩举办的“[Distant
Worlds: Music from Final
Fantasy](../Page/Distant_Worlds:_Music_from_Final_Fantasy.md "wikilink")”世界巡演，演出由[斯德哥尔摩皇家爱乐乐团演奏](../Page/斯德哥尔摩皇家爱乐乐团.md "wikilink")，阿尼·罗斯指挥\[55\]。他还参加了2008年3月1日在芝加哥附近[罗斯芒特剧院举办的第二场音乐会巡演](../Page/罗斯芒特剧院.md "wikilink")\[56\]。之后植松又参加了2010年7月24日在德克萨斯[休斯敦举办的音乐会](../Page/休斯敦.md "wikilink")。在2009年9月科隆的交响幻想音乐会中，四分之一的音乐来自最终幻想，该音乐会由交响游戏音乐会创作者制作，阿尼·罗斯指挥。

2010年2月，植松宣布他会在东海岸最大动漫展之一的[动漫波士顿出现](../Page/动漫波士顿.md "wikilink")。植松不但如期在动漫波士顿出现，还和[Video
Game Orchestra](../Page/Video_Game_Orchestra.md "wikilink")（VGO）演奏了《[One
Winged
Angel](../Page/One_Winged_Angel.md "wikilink")》。他短暂访问了伯克利音乐学院，并应VGO创始人、伯克利校友仲間将太的请求，进行了一个简短的问答环节。2012年1月，植松和它的乐队Earthbound
Papas在马里兰州国家港湾举行的[MAGFest
X上演出](../Page/MAGFest_X.md "wikilink")\[57\]\[58\]。

在2012年11月24日[阿德莱德娱乐中心举办的](../Page/阿德莱德娱乐中心.md "wikilink")“Final Fantasy
Distant
Worlds”音乐会上，在由阿罗·尼斯指挥，[阿德莱德交响乐团](../Page/阿德莱德交响乐团.md "wikilink")、阿德莱德爱乐乐团合唱团，以及独唱者演奏的演奏下，植松演奏了曲目\[59\]在2013年6月14和15日[维也纳音乐厅举行的](../Page/维也纳音乐厅.md "wikilink")“Final
Fantasy Distant Worlds”中，植松在由阿罗·尼斯指挥，维也纳人民歌剧院和维也纳室内合唱团的演奏下，植松弹奏了曲目。

在2013年8月18日日本[川崎冠名的奇幻摇滚音乐节上](../Page/川崎.md "wikilink")，植松和Earthbound
Papas告诉听众，他们原先计划以专辑收录的最终幻想曲目《妖星乱舞》（Dancing
Mad）为第二张专辑命名。然而他以“某‘S’公司”间接指代史克威尔艾尼克斯，称公司已经打电话并通知他“不能使用这个名称”。相较于让步，他决定以“Dancing
Dad”为专辑命名，以示对乐队名称的致敬。他还告诉听众，他想制作一部纯原创歌曲专辑，但感叹道“只是如果里面没有游戏歌曲，它可能会卖不出去！”

## 音乐风格与所受影响

植松的作曲风格多样，从庄严的[古典交响和](../Page/古典音乐.md "wikilink")[重金属到](../Page/重金属音乐.md "wikilink")[新时代和超冲击](../Page/新时代.md "wikilink")[铁克诺](../Page/铁克诺.md "wikilink")[电子](../Page/电子音乐.md "wikilink")。比如在《失落的奥德赛》中，他就使用了从古典交响编曲到当代[爵士和铁克诺的曲目](../Page/爵士乐.md "wikilink")\[60\]。植松曾表示，他是[凯尔特和](../Page/凯尔特音乐.md "wikilink")[爱尔兰音乐的忠实爱好者](../Page/爱尔兰音乐.md "wikilink")，他在一些作品中也使用了这种音乐风格元素\[61\]。植松的最终幻想配乐类型从欢快、黑暗愤怒到犹豫，变化多样。比如《最终幻想VIII》的音乐晦暗，而《最终幻想IX》的原声则悠然乐观\[62\]。人们称他的音乐能够表达场景的真实情感，比如《最终幻想VII》的《爱丽丝主题曲》\[63\]
。在《日美时报》记者的采访中，植松称，“我真没自觉地为日本或世界谱写音乐，但我觉得，我那带着明显日本音色的忧郁曲目，是有着相当价值的”\[64\]。他在《[时代](../Page/时代_\(杂志\).md "wikilink")》杂志“时代100：次浪潮－音乐”选出的“创新者”中占有一席\[65\]。他还被称为“电子游戏世界的约翰·威廉姆斯”\[66\]；人们认为他“提升”了电子游戏音乐的“价值和知晓度”\[67\]。

植松在音乐上受到很多英国和美国影响\[68\]。他称[埃尔顿·约翰是对他音乐影响最大的人](../Page/埃尔顿·约翰.md "wikilink")，并表示他希望能像他一样\[69\]。[披头士](../Page/披头士.md "wikilink")、[愛默生、雷克與帕瑪](../Page/愛默生、雷克與帕瑪.md "wikilink")\[70\]、[賽門與葛芬柯和一些](../Page/賽門與葛芬柯.md "wikilink")[前衛搖滾乐队也对他产生了主要影响](../Page/前衛搖滾.md "wikilink")\[71\]。在古典流派上，他称[彼得·伊里奇·柴可夫斯基是他的一大影响](../Page/彼得·伊里奇·柴可夫斯基.md "wikilink")\[72\]。植松称，[平克·弗洛伊德和](../Page/平克·弗洛伊德.md "wikilink")[国王柯里姆森等](../Page/国王柯里姆森.md "wikilink")1970年代乐队影响了他的最终幻想作品\[73\]。《最终幻想VII》曲目《One-Winged
Angel》的前奏受到[吉米·亨德里克斯歌曲](../Page/吉米·亨德里克斯.md "wikilink")《[Purple
Haze](../Page/Purple_Haze.md "wikilink")》的启发；歌词取自[卡尔·奥尔夫以自己](../Page/卡尔·奥尔夫.md "wikilink")[康塔塔](../Page/康塔塔.md "wikilink")《[布兰诗歌](../Page/布兰诗歌.md "wikilink")》——特别是歌曲《怒火中烧》、《[哦，命运女神](../Page/哦，命运女神.md "wikilink")》、《来吧，来吧，来吧》和《赞美这绝代美人》——改编的中世纪诗歌\[74\]。反过来，植松也对电子游戏音乐产生重大影响，同时也在电子游戏产业以外产生了影响。比如在雅典举办的2004年夏季奥运会上，一只女子花样游泳队就是用了《最终幻想VIII》的音乐《Liberi
Fatali》\[75\]\[76\]。同样是《最终幻想VIII》，由中文流行歌手[王菲演唱的](../Page/王菲.md "wikilink")《Eyes
on
Me》创下销量40万的记录，成为首个赢得[日本金唱片大奖](../Page/日本金唱片大奖.md "wikilink")——2000年“年度歌曲（国际）”奖\[77\]——的电子游戏音乐\[78\]。2010年植松接受采访时称，“与其通过聆听其他音乐得到灵感，我更愿在我遛狗时获得灵感”\[79\]。

## 音乐作品

<table>
<thead>
<tr class="header">
<th><p>电子游戏</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史克威尔电子游戏列表#游戏.md" title="wikilink">Alpha</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/国王的骑士.md" title="wikilink">国王的骑士</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水晶之龙.md" title="wikilink">水晶之龙</a></p></td>
</tr>
<tr class="even">
<td><p>1987</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小小电脑人#苹果城故事.md" title="wikilink">苹果城故事</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈奥君的不思议之旅.md" title="wikilink">哈奥君的不思议之旅</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史克威尔电子游戏列表#游戏.md" title="wikilink">Genesis</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/List_of_Alien_and_Predator_games#Aliens.md" title="wikilink">Aliens</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史克威尔电子游戏列表#游戏.md" title="wikilink">魔宝</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中山美穗的心跳高校.md" title="wikilink">中山美穗的心跳高校</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/JJ.md" title="wikilink">JJ</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/最终幻想_(游戏).md" title="wikilink">最终幻想</a></p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/最终幻想II.md" title="wikilink">最终幻想II</a></p></td>
</tr>
<tr class="even">
<td><p>1989</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魔界塔士_沙加.md" title="wikilink">魔界塔士 沙加</a></p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高速之星II.md" title="wikilink">高速之星II</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙加2_秘宝传说.md" title="wikilink">沙加2 秘宝传说</a></p></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/前线任务_枪之危机.md" title="wikilink">前线任务 枪之危机</a></p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/半熟英雄对3D.md" title="wikilink">半熟英雄对3D</a></p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蛋兽英雄.md" title="wikilink">蛋兽英雄</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蓝龙.md" title="wikilink">蓝龙</a></p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/失落的奥德赛.md" title="wikilink">失落的奥德赛</a></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/红莲之王.md" title="wikilink">红莲之王</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蓝龙Plus.md" title="wikilink">蓝龙Plus</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/离开_混乱迷宫.md" title="wikilink">离开 混乱迷宫</a></p></td>
</tr>
<tr class="even">
<td><p>Cry On（取消）</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/樱花笔记.md" title="wikilink">樱花笔记</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沙加2_秘宝传说I.md" title="wikilink">沙加2秘宝传说 命运女神</a></p></td>
</tr>
<tr class="even">
<td><p>库鲁林 融合</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/最终幻想XIV.md" title="wikilink">最终幻想XIV</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/核石之王.md" title="wikilink">核石之王</a></p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
</tr>
<tr class="odd">
<td><p>UnchainBlades ReXX</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神次元游戏_海王星V.md" title="wikilink">神次元游戏 海王星V</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幻想生活.md" title="wikilink">幻想生活</a></p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/家乡物语.md" title="wikilink">家乡物语</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/海之号角_怪物的未知的海域.md" title="wikilink">海之号角 怪物的未知的海域</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/碧蓝幻想.md" title="wikilink">碧蓝幻想</a></p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
</tr>
<tr class="odd">
<td><p>其它作品</p></td>
</tr>
<tr class="even">
<td><p>年份</p></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蓝龙_天界七龙.md" title="wikilink">蓝龙 天界七龙</a></p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Play_For_Japan:_The_Album.md" title="wikilink">Play For Japan: The Album</a></p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
</tr>
<tr class="even">
<td><p>Earthbound Papas: Dancing Dad</p></td>
</tr>
</tbody>
</table>

## 相关条目

  - [黑魔导士](../Page/黑魔导士.md "wikilink")

## 参考文献

## 外部链接

  -
  - [植松伸夫](http://www.square-enix.co.jp/music/sem/page/uematsu/index.html)在
    史克威尔艾尼克斯 的官方页面

  - [植松伸夫](http://na.square-enix.com/uematsu)在 史克威尔艾尼克斯 的官方页面

  -
  - [植松伸夫](http://www.discogs.com/artist/169898-Nobuo-Uematsu)在 Discogs
    的页面

[Category:1959年出生](../Category/1959年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:動畫音樂作曲家](../Category/動畫音樂作曲家.md "wikilink")
[Category:遊戲音樂作曲家](../Category/遊戲音樂作曲家.md "wikilink")
[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:神奈川大学校友](../Category/神奈川大学校友.md "wikilink")
[Category:高知縣出身人物](../Category/高知縣出身人物.md "wikilink")
[Category:史克威尔艾尼克斯人物](../Category/史克威尔艾尼克斯人物.md "wikilink")
[Category:晶片音樂作曲家](../Category/晶片音樂作曲家.md "wikilink")

1.
2.

3.

4.
5.

6.

7.

8.

9.
10.

11.

12.
13.

14.
15.
16.

17.
18.

19.
20.

21.

22.

23.
24.

25.

26.
27.

28.

29.

30.

31.

32. <http://www.destructoid.com/final-fantasy-composer-talks-to-us-about-upcoming-e-book-258380.phtml>

33. [Classic FM Hall of Fame (retrieved 9
    April 2012)](http://halloffame2012.classicfm.co.uk/individual/?position=16)

34. [Classic FM Hall of Fame (retrieved 6
    April 2013)](http://halloffame.classicfm.com/2013/chart/position/3/)

35. <http://halloffame.classicfm.com/2014/chart/position/7/>

36. [《最终幻想》系列作曲家植松伸夫因病暂停活动](http://www.vgtime.com/topic/958611.jhtml).游戏时光.2018-09-20.\[2018-09-29\].

37.
38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.
64.

65.

66.

67.
68.

69.
70.

71.
72.
73.
74.

75.

76.

77.

78.

79.