**韓高恩**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。

## 演出作品

### 電視劇

  - 1999年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[Happy
    Together](../Page/Happy_Together_\(SBS\).md "wikilink")》飾演 尹采琳
  - 2001年：[KBS](../Page/韓國放送公社.md "wikilink")《[討厭像爸爸一樣地生活](../Page/討厭像爸爸一樣地生活.md "wikilink")》飾演
    徐華妍
  - 2001年：[OCN](../Page/OCN.md "wikilink")《[沒有太陽](../Page/沒有太陽.md "wikilink")》
  - 2002年：SBS《[那女人的家](../Page/那女人的家.md "wikilink")》
  - 2003年：KBS《[保鑣](../Page/保鑣_\(韓國電視劇\).md "wikilink")》飾演 朴侑鎮
  - 2004年：SBS《[張吉山](../Page/張吉山.md "wikilink")》
  - 2004年：KBS《[比花還美](../Page/比花還美.md "wikilink")》飾演 金美秀
  - 2005年：SBS《[春日](../Page/春日.md "wikilink")》飾演 金敏貞
  - 2005年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[律師們](../Page/律師們.md "wikilink")》飾演
    楊夏穎
  - 2006年：SBS《[愛情與野望](../Page/愛情與野望#2006年.md "wikilink")》飾演 金美慈
  - 2007年：KBS《[京城醜聞](../Page/京城醜聞.md "wikilink")》飾演 車頌珠
  - 2008年：MBC《[天下絕色-{朴}-貞今](../Page/天下絕色朴貞今.md "wikilink")》飾演 司孔柔菈
  - 2009年：SBS《[不是誰都能愛](../Page/不是誰都能愛.md "wikilink")》飾演 吳錦蘭
  - 2010年：KBS《[名家](../Page/名家_\(電視劇\).md "wikilink")》飾演 韓丹伊
  - 2010年：MBC《[被稱為神的男子](../Page/被稱為神的男子.md "wikilink")》飾演 薇薇安
  - 2011年：MBC《[我也是花](../Page/我也是花.md "wikilink")》飾演 朴花影
  - 2012年：[MBN](../Page/MBN.md "wikilink")《[可疑的家族](../Page/可疑的家族.md "wikilink")》飾演
    天智仁
  - 2013年：MBC《[火之女神井兒](../Page/火之女神井兒.md "wikilink")》飾演
    [仁嬪金氏](../Page/仁嬪金氏.md "wikilink")
  - 2013年：MBC《[愛能給別人嗎](../Page/愛能給別人嗎.md "wikilink")》飾演 鄭友拉
  - 2015年：KBS Drama《[Miss
    Mammamia](../Page/Miss_Mammamia.md "wikilink")》飾演 吳珠琍
  - 2018年：SBS《[能先接吻嗎？](../Page/能先接吻嗎？.md "wikilink")》飾演 姜錫英
  - 2018年：MBN《[心動警報](../Page/心動警報.md "wikilink")》飾演 韓在景

### 電影

  - 1998年：《[City of the Rising
    Sun](../Page/City_of_the_Rising_Sun.md "wikilink")》
  - 2009年：《[很悲傷的城市](../Page/很悲傷的城市.md "wikilink")》

### MV

  - 1999年：Position《Blue Day》（與[韓載錫](../Page/韓載錫.md "wikilink")）
  - 2000年：元泰源《淚流滿面》（與[劉智泰](../Page/劉智泰.md "wikilink")）
  - 2001年：[李秀英](../Page/李秀英_\(韓國藝人\).md "wikilink")《Never
    Again》\[1\]（[鍾麗緹](../Page/鍾麗緹.md "wikilink")、[申鉉濬](../Page/申鉉濬.md "wikilink")）
  - 2003年：M.C The MAX《愛情詩》
  - 2004年：趙恩《悲傷戀歌》（與[宋一國](../Page/宋一國.md "wikilink")）
  - 2005年：Sol《傷心獨白》（與[張錫賢](../Page/張錫賢.md "wikilink")）
  - 2010年：朴慧京《新男友》
  - 2011年：Beige《不能喝酒》（與[張泰勳](../Page/張泰勳.md "wikilink")）

## 獲獎

  - 2001年：KBS演技大賞 - 人氣獎《不與父親般居住》
  - 2001年：KBS演技大賞 - 上鏡獎《不與父親般居住》
  - 2002年：SBS演技大賞－人氣獎
  - 2003年：KBS演技大賞－人氣獎《保鏢》
  - 2004年：KBS演技大賞－人氣獎
  - 2007年：KBS演技大賞－女配角獎

## 腳註

## 外部連結

  -
  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%ED%95%9C%EA%B3%A0%EC%9D%80&sm=tab_etc&ie=utf8&key=PeopleService&os=95175)

[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:韓國女性模特兒](../Category/韓國女性模特兒.md "wikilink")
[\~Go Eun](../Category/韓姓.md "wikilink")

1.  [辛曉琪](../Page/辛曉琪.md "wikilink")《永遠》之原曲