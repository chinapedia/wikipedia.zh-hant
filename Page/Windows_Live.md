[Mslivelogo.png](https://zh.wikipedia.org/wiki/File:Mslivelogo.png "fig:Mslivelogo.png")

**Windows
Live**是[微軟推出的包含一系列線上應用服務的套裝](../Page/微軟.md "wikilink")[軟體](../Page/軟體.md "wikilink")，也是[Windows的網絡延伸](../Page/Windows.md "wikilink")。這些服務透過[網路瀏覽器的介面來提供像](../Page/網路瀏覽器.md "wikilink")[Microsoft
Office這一類的服務](../Page/Microsoft_Office.md "wikilink")。2005年11月1日，這套產品開始[Beta版本的測試](../Page/軟件版本週期#Beta.md "wikilink")，並已經完成对大部分語言的開發。

目前Windows Live的大部分項目已經結束服務，包括使用率甚高的[Windows Live
Messenger](../Page/Windows_Live_Messenger.md "wikilink")（一款[即時通訊軟體](../Page/即時通訊軟體.md "wikilink")）等，又或者轉換另一個名稱繼續存在，例如[Windows
Live
Hotmail更名為](../Page/Windows_Live_Hotmail.md "wikilink")[Outlook.com](../Page/Outlook.com.md "wikilink")。

Windows Live ID的密碼與[Xbox
Live是相通的](../Page/Xbox_Live.md "wikilink")，故擁有Windows
Live帳號也可以登入至[Xbox Live](../Page/Xbox_Live.md "wikilink")。Windows
Live品牌由[Microsoft
Account取代](../Page/Microsoft_Account.md "wikilink")。

## 服務總覽

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 20%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>服務</p></th>
<th><p>描述</p></th>
<th><p>網站</p></th>
<th><p>狀態</p></th>
<th><p>替代品</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Search_Academic.md" title="wikilink">Windows Live Academic</a></p></td>
<td><p>從學術研究期刊中搜索學術文章</p></td>
<td><p><a href="http://academic.live.com">http://academic.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Account.md" title="wikilink">Windows Live Account</a></p></td>
<td><p>為用戶提供整理<a href="../Page/Windows_Live_ID.md" title="wikilink">Windows Live ID帳號資料的服務及相關功能</a></p></td>
<td><p><a href="http://account.live.com">http://account.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Microsoft_Account.md" title="wikilink">Microsoft Account</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Agents.md" title="wikilink">Windows Live Agents</a></p></td>
<td><p><a href="../Page/Windows_Live_Messenger.md" title="wikilink">Windows Live Messenger中的互動性聊天機械人</a>（chatterbot）</p></td>
<td></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Alerts.md" title="wikilink">Windows Live Alerts</a></p></td>
<td><p>電郵、手機和Messenger的資訊訂閱服務</p></td>
<td><p><a href="http://alerts.live.com">http://alerts.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Barcode.md" title="wikilink">Windows Live Barcode</a></p></td>
<td><p><a href="../Page/二維條碼.md" title="wikilink">二維條碼服務</a></p></td>
<td><p><a href="https://barcode.ideas.live.com">https://barcode.ideas.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Search_Books.md" title="wikilink">Windows Live Books</a></p></td>
<td><p>图书搜尋及線上購買服務</p></td>
<td><p><a href="http://books.live.com">http://books.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Calendar.md" title="wikilink">Windows Live Calendar</a></p></td>
<td><p>行事曆，<a href="../Page/Windows_Live_Hotmail.md" title="wikilink">Windows Live Hotmail的一部分</a></p></td>
<td><p><a href="http://calendar.live.com">http://calendar.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Outlook.com.md" title="wikilink">Outlook.com</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Call.md" title="wikilink">Windows Live Call</a></p></td>
<td><p><a href="../Page/Windows_Live_Messenger.md" title="wikilink">Windows Live Messenger的一部分</a></p></td>
<td></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Contacts.md" title="wikilink">Windows Live Contacts</a></p></td>
<td><p>連絡人，<a href="../Page/Windows_Live_Hotmail.md" title="wikilink">Windows Live Hotmail的一部分</a></p></td>
<td><p><a href="http://contacts.live.com">http://contacts.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Outlook.com.md" title="wikilink">Outlook.com</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Custom_Domains.md" title="wikilink">Windows Live Custom Domains</a></p></td>
<td><p>為網站經營者提供電子郵件寄存服務</p></td>
<td><p><a href="http://arquivo.pt/wayback/20131108014006/http%3A//domains.live.com/">http://arquivo.pt/wayback/20131108014006/http%3A//domains.live.com/</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Windows Live Dashboard</p></td>
<td><p>檢視服務狀態</p></td>
<td><p><a href="https://web.archive.org/web/20150510004025/https://status.live.com/">https://web.archive.org/web/20150510004025/https://status.live.com/</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Dev.md" title="wikilink">Windows Live Dev</a></p></td>
<td><p>Windows Live開發人員的資源庫</p></td>
<td><p><a href="http://dev.live.com">http://dev.live.com</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live程式集.md" title="wikilink">Windows Live程式集</a></p></td>
<td><p>Windows Live程式的總集</p></td>
<td><p><a href="http://essentials.live.com">http://essentials.live.com</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Expo.md" title="wikilink">Windows Live Expo</a></p></td>
<td><p>線上廣告分類</p></td>
<td><p><a href="https://web.archive.org/web/20060716013141/http://expo.live.com/">https://web.archive.org/web/20060716013141/http://expo.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Favorites.md" title="wikilink">Windows Live Favorites</a></p></td>
<td><p>線上寄存“我的最愛”網站</p></td>
<td><p><a href="http://favorites.live.com">http://favorites.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/FolderShare.md" title="wikilink">Windows Live FolderShare</a></p></td>
<td><p>程式設計用來同步資料夾以及允許遠距存取存放在基於Windows及<a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X電腦上的檔案</a></p></td>
<td><p><a href="https://web.archive.org/web/20110515150459/https://www.foldershare.com/">https://web.archive.org/web/20110515150459/https://www.foldershare.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Gallery.md" title="wikilink">Windows Live Gallery</a></p></td>
<td><p><a href="../Page/Windows_Vista.md" title="wikilink">Windows Vista資訊看板工具總集</a></p></td>
<td><p><a href="http://gallery.live.com">http://gallery.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Windows Live Guides</p></td>
<td><p><a href="../Page/Windows_Live_Shopping.md" title="wikilink">Windows Live Shopping的一部分</a></p></td>
<td><p><a href="http://shopguides.live.com">http://shopguides.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Healthcare.md" title="wikilink">Windows Live Healthcare</a></p></td>
<td></td>
<td></td>
<td><p>被取代</p></td>
<td><p><a href="http://health.msn.com">MSN Health</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Home.md" title="wikilink">Windows Live Home</a></p></td>
<td><p>提供了一個中心位置來訪問Windows Live服務</p></td>
<td><p><a href="http://home.live.com">http://home.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Help_Community.md" title="wikilink">Windows Live Help Community</a></p></td>
<td><p>Windows Live產品相關討論區的總集</p></td>
<td><p><a href="https://web.archive.org/web/20080827173707/http://helpcommunity.live.com/">https://web.archive.org/web/20080827173707/http://helpcommunity.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Hotmail.md" title="wikilink">Windows Live Hotmail</a></p></td>
<td><p>Hotmail風格的電子郵箱</p></td>
<td><p><a href="http://mail.live.com">http://mail.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Outlook.com.md" title="wikilink">Outlook.com</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Ideas.md" title="wikilink">Windows Live Ideas</a></p></td>
<td><p>Windows Live服務總集</p></td>
<td><p><a href="http://ideas.live.com">http://ideas.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Live_Search_Maps.md" title="wikilink">Live Search Maps</a></p></td>
<td><p>進階地圖系統包括衛星覆蓋、道路地圖、駕駛指引及路線計畫，並包括鳥瞰視角（Birds Eye™）及<a href="../Page/3D.md" title="wikilink">3D地圖</a></p></td>
<td><p><a href="http://maps.live.com">http://maps.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Bing_Maps.md" title="wikilink">Bing Maps</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Mail.md" title="wikilink">Windows Live Mail</a></p></td>
<td><p>桌面郵件程式，類似<a href="../Page/Windows_XP.md" title="wikilink">Windows XP上的</a><a href="../Page/Outlook_Express.md" title="wikilink">Outlook Express或</a><a href="../Page/Windows_Vista.md" title="wikilink">Windows Vista上的</a><a href="../Page/Windows_Mail.md" title="wikilink">Windows Mail</a>，並包含對RSS的完整支援，亦是唯一一個在<a href="../Page/Windows_Vista.md" title="wikilink">Windows Vista中能連接到Hotmail</a>/Live Hotmail的郵件程式。</p></td>
<td><p><a href="http://windows.microsoft.com/zh-tw/windows-live/essentials-install-offline-faq">直接下載點</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Windows Live Marketplace</p></td>
<td></td>
<td><p><a href="https://web.archive.org/web/20061004051422/http://windowsmarketplace.com/">https://web.archive.org/web/20061004051422/http://windowsmarketplace.com/</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Windows_Store.md" title="wikilink">Windows Store</a></p></td>
</tr>
<tr class="odd">
<td><p>Windows Live Max</p></td>
<td></td>
<td></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Windows Live Message Boards</p></td>
<td><p>Windows Live服務的討論區</p></td>
<td><p><a href="https://web.archive.org/web/20080821045757/http://boards.live.com/">https://web.archive.org/web/20080821045757/http://boards.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Messenger.md" title="wikilink">Windows Live Messenger</a></p></td>
<td><p>即時通訊、個人文件分享、語言通話通訊軟體</p></td>
<td><p><a href="http://messenger.live.com">http://messenger.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Skype.md" title="wikilink">Skype</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_OneCare.md" title="wikilink">Windows Live OneCare</a></p></td>
<td><p>電腦安全服務，為<a href="../Page/微軟.md" title="wikilink">微軟自家的防毒軟體</a>、系統維護及軟體防火牆</p></td>
<td><p><a href="https://web.archive.org/web/20110523172039/http://onecare.live.com/">https://web.archive.org/web/20110523172039/http://onecare.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_OneCare_Family_Safety.md" title="wikilink">Windows Live OneCare Family Safety</a></p></td>
<td><p>家長監護服務，類似<a href="../Page/Windows_Vista.md" title="wikilink">Windows Vista的家長監護功能</a>，它是<a href="../Page/Windows_Live_Essentials.md" title="wikilink">Windows Live Essentials的一部分</a></p></td>
<td><p><a href="http://fss.live.com">http://fss.live.com</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_OneCare_Safety_Scanner.md" title="wikilink">Windows Live OneCare Safety Scanner</a></p></td>
<td><p>免費線上病毒掃瞄及相關電腦安全服務</p></td>
<td><p><a href="http://safety.live.com">http://safety.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Microsoft_Live_Labs_Photosynth.md" title="wikilink">Microsoft Live Labs Photosynth</a></p></td>
<td></td>
<td><p><a href="https://web.archive.org/web/20070205184512/http://labs.live.com/photosynth/">https://web.archive.org/web/20070205184512/http://labs.live.com/photosynth/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Search_Product_Upload.md" title="wikilink">Windows Live Product Upload</a></p></td>
<td><p>允許商家將商品資料上傳到<a href="../Page/Windows_Live_Search_Products.md" title="wikilink">Windows Live Products</a></p></td>
<td><p><a href="http://productupload.live.com">http://productupload.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Search_Products.md" title="wikilink">Windows Live Products</a></p></td>
<td><p>商品搜尋服務</p></td>
<td><p><a href="http://products.live.com">http://products.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_SkyDrive.md" title="wikilink">Windows Live SkyDrive</a>（前身Windows Live Driver）</p></td>
<td><p>透過虛擬硬碟進行遙距數據寄存服務[1][2]</p></td>
<td><p><a href="http://skydrive.com">http://skydrive.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/OneDrive.md" title="wikilink">OneDrive</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Publisher.md" title="wikilink">Windows Live Publisher</a></p></td>
<td><p>允許出版社將書本資料上傳到<a href="../Page/Windows_Live_Search_Books.md" title="wikilink">Windows Live Books</a></p></td>
<td><p><a href="https://web.archive.org/web/20080509130123/http://publisher.live.com/">https://web.archive.org/web/20080509130123/http://publisher.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_QnA.md" title="wikilink">Windows Live QnA</a></p></td>
<td><p>知識答問服務，與<a href="../Page/Yahoo!知識+.md" title="wikilink">Yahoo!知識+</a>、<a href="../Page/百度知道.md" title="wikilink">百度知道類似</a></p></td>
<td><p><a href="https://web.archive.org/web/20061017032152/http://qna.live.com/">https://web.archive.org/web/20061017032152/http://qna.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Microsoft_Live_Labs_Relay_Service.md" title="wikilink">Microsoft Live Labs Relay Service</a></p></td>
<td></td>
<td><p><a href="https://web.archive.org/web/20061212014826/http://relay.labs.live.com/">https://web.archive.org/web/20061212014826/http://relay.labs.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Search.md" title="wikilink">Windows Live Search</a></p>
<ul>
<li><a href="../Page/Windows_Live_Search_Academic.md" title="wikilink">Academic</a></li>
<li><a href="../Page/Windows_Live_Search_Books.md" title="wikilink">Books</a></li>
<li><a href="../Page/Windows_Live_Expo.md" title="wikilink">Classifieds</a></li>
<li>Feeds</li>
<li>Image</li>
<li>Macros</li>
<li><a href="../Page/Live_Search_Maps.md" title="wikilink">Maps</a></li>
<li>News</li>
<li><a href="../Page/Windows_Live_Search_Products.md" title="wikilink">Products</a></li>
<li><a href="../Page/Windows_Live_QnA.md" title="wikilink">QnA</a></li>
<li>Translation</li>
<li><a href="../Page/Windows_Live_Video.md" title="wikilink">Videos</a></li>
</ul></td>
<td><p>搜索服務—在特定主題中搜尋有關資料</p></td>
<td><p><a href="http://search.live.com">http://search.live.com</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Bing.md" title="wikilink">Bing</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Search_Center.md" title="wikilink">Windows Live Search Center</a></p></td>
<td></td>
<td></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Microsoft_Live_Labs_Security_Token_Service.md" title="wikilink">Microsoft Live Labs Security Token Service</a></p></td>
<td></td>
<td><p><a href="http://sts.labs.live.com">http://sts.labs.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Shopping.md" title="wikilink">Windows Live Shopping</a></p></td>
<td></td>
<td><p><a href="http://arquivo.pt/wayback/20080223072630/http://shopping.live.com/">http://arquivo.pt/wayback/20080223072630/http://shopping.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_Spaces.md" title="wikilink">Windows Live Spaces</a></p></td>
<td><p>基於<a href="../Page/AJAX.md" title="wikilink">AJAX的網誌社群</a></p></td>
<td><p><a href="http://spaces.live.com">http://spaces.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Toolbar.md" title="wikilink">Windows Live Toolbar</a></p></td>
<td><p><a href="../Page/Windows_Internet_Explorer.md" title="wikilink">Windows Internet Explorer的工具列讓使用者能連接到Windows</a> Live的各種服務</p></td>
<td><p><a href="https://web.archive.org/web/20120304152431/http://toolbar.live.com/">https://web.archive.org/web/20120304152431/http://toolbar.live.com/</a></p></td>
<td><p>被取代</p></td>
<td><p><a href="../Page/Bing_Bar.md" title="wikilink">Bing Bar</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_TV.md" title="wikilink">Windows Live TV</a></p></td>
<td><p><a href="../Page/Windows_Media_Center.md" title="wikilink">Windows Media Center的附加電視程式</a></p></td>
<td><p><a href="http://mtview.live.com/orbit/setup/orbit.msi">直接下載點</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_Video.md" title="wikilink">Windows Live Video</a></p></td>
<td></td>
<td></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Windows_Live_WiFi_Center.md" title="wikilink">Windows Live WiFi Center</a></p></td>
<td></td>
<td><p><a href="http://arquivo.pt/wayback/20080218144532/http%3A//www.live.com/">http://arquivo.pt/wayback/20080218144532/http%3A//www.live.com/</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Windows_Live_WiFi_Hotspot_Locator.md" title="wikilink">Windows Live WiFi Hotspot Locator</a></p></td>
<td></td>
<td><p><a href="http://hotspot.live.com">http://hotspot.live.com</a></p></td>
<td><p>已結束</p></td>
<td></td>
</tr>
</tbody>
</table>

### \-{zh-hans:移动; zh-hant:行動;}-服務

| \-{zh-hans:移动; zh-hant:行動;}-服務                                                       | 描述                                                                          | 網站                                                                          |
| ------------------------------------------------------------------------------------ | --------------------------------------------------------------------------- | --------------------------------------------------------------------------- |
| [Live.com Mobile](../Page/Live.com_Mobile.md "wikilink")                             | [Live.com的手機版本](../Page/Live.com.md "wikilink")                             | <https://web.archive.org/web/20070207082833/http://mobile.live.com/portal/> |
| [Windows Live Mail Mobile](../Page/Windows_Live_Mail_Mobile.md "wikilink")           | [Windows Live Hotmail的手機版本](../Page/Windows_Live_Hotmail.md "wikilink")     | <https://archive.is/20130103161109/http://mobile.live.com/hm>               |
| [Windows Live Messenger Mobile](../Page/Windows_Live_Messenger_Mobile.md "wikilink") | [Windows Live Messenger的手機版本](../Page/Windows_Live_Messenger.md "wikilink") | <https://web.archive.org/web/20061210160302/http://mobile.live.com/wlm/>    |
| [Windows Live Search Mobile](../Page/Windows_Live_Search_Mobile.md "wikilink")       | [Windows Live Search的手機版本](../Page/Windows_Live_Search.md "wikilink")       | <http://arquivo.pt/wayback/20080224193823/http://mobile.search.live.com/>   |
| [Windows Live Spaces Mobile](../Page/Windows_Live_Spaces_Mobile.md "wikilink")       | [Windows Live Spaces的手機版本](../Page/Windows_Live_Spaces.md "wikilink")       | <https://web.archive.org/web/20110225134940/http://mobile.spaces.live.com/> |

## 雲端運算服務

Windows Live將是[Windows
Azure雲端運算平台的一部份](../Page/Windows_Azure.md "wikilink")，稱為Live
Services，並提供一個Live Services
Framework供開發人員發展[雲端運算服務的應用程式](../Page/雲端運算.md "wikilink")，目前Live
Services支援16種服務：

  - Windows Live Admin Center
  - Windows Live Alerts
  - Windows Live App Storage
  - Windows Live Contacts
  - FeedSync
  - Live Framework
  - Windows Live Messenger
  - Windows Live Photo Gallery
  - Windows Live Photos
  - Live Search
  - Windows Live Silverlight Streaming
  - Windows Live Spaces
  - Virtual Earth
  - Web Gadgets
  - Windows Live ID
  - Windows Live Writer

## [Microsoft Account取代](../Page/Microsoft_Account.md "wikilink")

以下為Windows 10以後的作業系統和Windows Phone中的Windows Live服務取代方式\[3\]

<table>
<thead>
<tr class="header">
<th><p>服務</p></th>
<th><p>Windows 8取代方式</p></th>
<th><p>Windows Phone</p></th>
<th><p>網址</p></th>
<th><p>前稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>户口（Account）</p></td>
<td><p>Microsoft Account</p></td>
<td><p>Microsoft Account</p></td>
<td><p>Account.live.com</p></td>
<td><p>Windows Live ID, Microsoft Passport, .NET Passport</p></td>
</tr>
<tr class="even">
<td><p>雲端（Storage）</p></td>
<td><p>OneDrive app,<br />
OneDrive Desktop</p></td>
<td><p>OneDrive app,<br />
Office app</p></td>
<td><p>OneDrive.com</p></td>
<td><p>FolderShare, Live Mesh, Windows Live Mesh, SkyDrive</p></td>
</tr>
<tr class="odd">
<td><p>電郵（Email）</p></td>
<td><p>Mail app</p></td>
<td><p>Mail app</p></td>
<td><p>Outlook.com</p></td>
<td><p>Hotmail, Windows Live Mail, Outlook Express, Windows Mail</p></td>
</tr>
<tr class="even">
<td><p>行事曆（Calendar）</p></td>
<td><p>Calendar app</p></td>
<td><p>Calendar app</p></td>
<td><p>整合在Outlook.com中</p></td>
<td><p>Windows Live Calendar</p></td>
</tr>
<tr class="odd">
<td><p>聯絡人（Contacts）</p></td>
<td><p>People app</p></td>
<td><p>People app</p></td>
<td><p>整合在Outlook.com中</p></td>
<td><p>Windows Live Contacts</p></td>
</tr>
<tr class="even">
<td><p>即時通訊（Messaging）</p></td>
<td><p>Messaging app</p></td>
<td><p>Messaging app</p></td>
<td><p>整合在Outlook.com及OneDrive.com中(Skype尚有Microsoft Account選項)</p></td>
<td><p>Windows Messenger, MSN Messenger, Windows Live Messenger</p></td>
</tr>
<tr class="odd">
<td><p>照片和影片<br />
（Photos / Videos）</p></td>
<td><p>Photos app, Photo Gallery,<br />
Movie Maker</p></td>
<td><p>Photos app,<br />
Camera Roll</p></td>
<td><p>整合在OneDrive.com中</p></td>
<td><p>Windows Live Photo Gallery, Movie Maker</p></td>
</tr>
</tbody>
</table>

## 特殊域名

[w.cn](https://web.archive.org/web/20151005114813/http://www.w.cn/)和[9.cn](http://www.9.cn)这两个超短域名由微软购得，均指向Windows
Live相关服务。（现在这两个域名分别转向至生活服务网站“[达步溜](../Page/达步溜.md "wikilink")”和游戏平台。）

## 审查

2009年6月2日下午，Windows Live服务、以及一天前刚刚上线取代Windows Live
Search的[Bing搜索引擎被](../Page/Bing.md "wikilink")[中国大陆封锁](../Page/中国大陆.md "wikilink")。据报道，此次网络封锁还涉及[Flickr](../Page/Flickr.md "wikilink")、[Twitter等网站](../Page/Twitter.md "wikilink")，有人估计与两天后的[六四事件](../Page/六四事件.md "wikilink")20周年有关。\[4\]\[5\]\[6\]\[7\]

MSN中国副总裁刘振宇则表示，微软将严格按照中国政策和法律对搜索内容进行监管和过滤。6月3日，对Windows
Live的封锁被解除。6月5日，对Bing的封锁被解除。\[8\]

## 註釋

<div class="references-small">

<references />

</div>

## 參見

  - [Microsoft Office Live](../Page/Microsoft_Office_Live.md "wikilink")
  - [Xbox Live](../Page/Xbox_Live.md "wikilink")

## 外部連結

  - [Windows Live程式集](http://essentials.live.com/)

[Category:Windows Live](../Category/Windows_Live.md "wikilink")
[Category:Web 2.0](../Category/Web_2.0.md "wikilink")
[Category:微軟網站](../Category/微軟網站.md "wikilink")
[Category:MSN](../Category/MSN.md "wikilink")
[Category:網路服務提供者](../Category/網路服務提供者.md "wikilink")
[Category:微軟](../Category/微軟.md "wikilink")
[Category:帕羅奧圖公司](../Category/帕羅奧圖公司.md "wikilink")

1.  [Windows Live
    Drive](http://www.microsoft-watch.com/article2/0,2180,1951173,00.asp)
2.  [CNN](http://money.cnn.com/magazines/fortune/fortune_archive/2006/05/01/8375454/index.htm)
3.  www.blogs.msdn.com/b/b8/archive/2012/05/02/cloud-services-for-windows-8-and-windows-phone-windows-live-reimagined.aspx
4.
5.
6.
7.
8.