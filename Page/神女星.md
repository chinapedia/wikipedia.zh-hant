**神女星**（Angelina）是一顆中型的[小行星](../Page/小行星.md "wikilink")，也是一顆不常見的E型小行星。神女星為第三大的E型小行星，僅次於[侍神星與](../Page/侍神星.md "wikilink")[禍神星](../Page/禍神星.md "wikilink")。[坦普爾於](../Page/坦普爾.md "wikilink")1861年3月4日發現神女星，也是他發現的5個小行星中的第1個。

E型小行星於衝的位置會發出不尋常的光輝，這個現象也在[木星的衛星](../Page/木星.md "wikilink")[木卫一](../Page/木卫一.md "wikilink")、[木卫二與](../Page/木卫二.md "wikilink")[木卫三及](../Page/木卫三.md "wikilink")[土星的衛星](../Page/土星.md "wikilink")[土卫八上發現過](../Page/土卫八.md "wikilink")。


[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1861年发现的小行星](../Category/1861年发现的小行星.md "wikilink")