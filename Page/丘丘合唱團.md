**丘丘合唱團**，台灣[流行樂團](../Page/流行樂團.md "wikilink")，成立於1981年，在1984年解散。成員包括團長[吉他手邱晨](../Page/吉他手.md "wikilink")（邱憲榮）
、主唱[金智娟](../Page/金智娟.md "wikilink")（娃娃）、[鼓手林俊修](../Page/鼓手.md "wikilink")、[鍵盤手李應錄及](../Page/鍵盤手.md "wikilink")[貝斯手李世傑](../Page/貝斯手.md "wikilink")。1982年推出同名專輯，為台灣首支發行專輯的流行樂團，作品以專輯中的〈就在今夜〉最為人熟知。流暢的旋律、強勁的節奏、沙啞的唱腔、活潑的演唱方式給當時流行[校園民歌的台灣樂壇帶來全然不同的音樂風格](../Page/校園民歌.md "wikilink")；並在他們的影響之下，台灣流行樂壇才開始出現了樂隊團體。

1983年，邱晨因與[新格唱片理念不合而離團](../Page/新格唱片.md "wikilink")。1984年，因為團員相繼入伍，而於發行第三張專輯後解散。

## 專輯列表

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行年份</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>《丘丘合唱團Ｉ 就在今夜》</p></td>
<td style="text-align: left;"><p>1982年</p></td>
<td style="text-align: left;"><p>新格唱片</p></td>
<td style="text-align: left;"><ol>
<li>就在今夜</li>
<li>為何夢見他</li>
<li><p>（翻唱）</p></li>
<li>大公雞</li>
<li>旋轉木馬</li>
<li>河堤上的傻瓜</li>
<li>不明白</li>
<li><p>（翻唱）</p></li>
<li>十個太陽（演奏曲）</li>
<li>橋</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《丘丘合唱團Ⅱ 陌生的人》</p></td>
<td style="text-align: left;"><p>1983年</p></td>
<td style="text-align: left;"><p>新格唱片</p></td>
<td style="text-align: left;"><ol>
<li>陌生的人</li>
<li>雨滴灑落我臉</li>
<li>宇宙鼓手</li>
<li>夢中的水手</li>
<li><p>（翻唱）</p></li>
<li>對不對</li>
<li>緊急煞車</li>
<li>虎姑婆</li>
<li><p>（翻唱）</p></li>
<li>什麼歌</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>《丘丘合唱團Ⅲ 告別20歲》</p></td>
<td style="text-align: left;"><p>1984年</p></td>
<td style="text-align: left;"><p>新格唱片</p></td>
<td style="text-align: left;"><ol>
<li>刻痕</li>
<li>離開你</li>
<li>停留</li>
<li>搖搖搖</li>
<li>失落的歲月</li>
<li>Come On Shake</li>
<li>星空TEL</li>
<li>只要一分鐘</li>
<li>永恆的夏天</li>
<li>倒影</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

## 参考资料

<references/>

[Category:台灣之最](../Category/台灣之最.md "wikilink")
[Category:台灣流行音樂團體](../Category/台灣流行音樂團體.md "wikilink")
[Category:華語流行音樂團體](../Category/華語流行音樂團體.md "wikilink")
[Category:1981年成立的音樂團體](../Category/1981年成立的音樂團體.md "wikilink")