**周松崗**爵士，[GBS](../Page/金紫荊星章.md "wikilink")，[JP](../Page/太平紳士.md "wikilink")，[FREng](../Page/皇家工程學院.md "wikilink")，[FCGI](../Page/倫敦城市行業專業學會.md "wikilink")，[FIChemE](../Page/化學工程師學會_\(英國\).md "wikilink")（，），[香港](../Page/香港.md "wikilink")[國際企業](../Page/國際企業.md "wikilink")[行政管理人員](../Page/行政管理人員.md "wikilink")，早年於[英國及](../Page/英國.md "wikilink")[澳洲等地的企業工作](../Page/澳洲.md "wikilink")，2000年授勳為[爵士](../Page/爵士.md "wikilink")，於2003年12月至2011年12月31日任[香港鐵路有限公司](../Page/香港鐵路有限公司.md "wikilink")（前稱地鐵有限公司）[行政總裁](../Page/行政總裁.md "wikilink")，任內促成[兩鐵合併](../Page/兩鐵合併.md "wikilink")。

2012年4月24日，他獲委任為[香港交易及結算所有限公司董事會主席](../Page/香港交易及結算所有限公司.md "wikilink")，於同年6月29日獲委任為[行政會議非官守議員](../Page/香港特別行政區行政會議.md "wikilink")。

## 生平

### 早年生涯

周松崗在1950年9月9日於[香港出生](../Page/香港.md "wikilink")，曾就讀土瓜灣的[農圃道官立小學](../Page/農圃道官立小學.md "wikilink")，年青時在著名中文學校[香港培正中學讀書](../Page/香港培正中學.md "wikilink")，是1968年仁社同學，父親周公諒（？—1973年2月19日）則是該校有名的[中文](../Page/中文.md "wikilink")[老師](../Page/老師.md "wikilink")。周松崗後來前往外國留學，曾先後在[威斯康星大學和](../Page/威斯康星大學.md "wikilink")[加洲大學取得](../Page/加利福尼亞大學.md "wikilink")[化學工程之](../Page/化學工程.md "wikilink")[學士和](../Page/學士.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")。另外，他亦是[香港中文大學的工商管理碩士](../Page/香港中文大學.md "wikilink")。於1970年代，曾經任職[香港氧氣公司的](../Page/香港氧氣公司.md "wikilink")[推銷員](../Page/推銷員.md "wikilink")，而且被公司保送[美國](../Page/美國.md "wikilink")[哈佛商學院修讀高等管理進修課程](../Page/哈佛商學院.md "wikilink")。

### 商業生涯

周松崗是特許[工程師](../Page/工程師.md "wikilink")，他早年在[英國氧氣集團](../Page/英國氧氣集團.md "wikilink")（[The
BOC
Group](../Page/:en:The_BOC_Group.md "wikilink")）工作了廿年，曾被派到[香港](../Page/香港.md "wikilink")、[日本](../Page/日本.md "wikilink")、[美國和](../Page/美國.md "wikilink")[英國各地工作](../Page/英國.md "wikilink")，至1993年獲擢升為該集團的[董事](../Page/董事.md "wikilink")，並擔任集團在氣體業務方面的總裁。至1997年，周松崗改到英國的[GKN公共有限公司](../Page/GKN公共有限公司.md "wikilink")（[GKN](../Page/:en:GKN.md "wikilink")）擔任第一位華人總裁。至2001年復轉到[澳洲的](../Page/澳洲.md "wikilink")[布萊堡工業集團](../Page/布萊堡工業集團.md "wikilink")（[Brambles
Industries](../Page/:en:Brambles_Industries.md "wikilink")）出掌行政總裁之職，完成三次大規模公司[合併](../Page/合併.md "wikilink")。香港有媒體曾形容周松崗是「西方社會最具影響力的中國人」\[1\]，而英廷亦曾於2000年冊封他為[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")，以表彰他對英國[工業的貢獻](../Page/工業.md "wikilink")。作為中文中學畢業生的他曾表示，自己得以在西方有所成就，是因為他在海外不忘閱讀父親送贈的《[論語](../Page/論語.md "wikilink")》，讓他學習到不少人生道理。\[2\]

[Merger_of_KCR_and_MTR_operations_2007-12-02_05h29m58s_SN208338.JPG](https://zh.wikipedia.org/wiki/File:Merger_of_KCR_and_MTR_operations_2007-12-02_05h29m58s_SN208338.JPG "fig:Merger_of_KCR_and_MTR_operations_2007-12-02_05h29m58s_SN208338.JPG")手持號誌旗，為[兩鐵合併後首班港鐵](../Page/兩鐵合併.md "wikilink")[東鐵綫列車主持啟航儀式](../Page/東鐵綫.md "wikilink")。\]\]
可是在2002年的[全年股東大會上](../Page/全年股東大會.md "wikilink")，周松崗被一群[股東指責他拖累公司業績](../Page/股東.md "wikilink")\[3\]\[4\]。事後在2003年9月25日，他離開布萊堡工業集團，並於同年12月1日正式轉到香港的[地鐵有限公司出任行政總裁](../Page/地鐵有限公司.md "wikilink")。在2004年7月至2011年1月，周松崗進而兼任[渣打銀行(香港)有限公司的非執行主席](../Page/渣打銀行_\(香港\).md "wikilink")，另外自1997年至2008年也是[渣打集團有限公司之非執行董事](../Page/渣打集團有限公司.md "wikilink")。\[5\]
在2007年8月8日，周松崗獲港府委任在[兩鐵合併後出任](../Page/兩鐵合併.md "wikilink")[香港鐵路有限公司](../Page/香港鐵路有限公司.md "wikilink")（簡稱「港鐵公司」）行政總裁，有關委任在2007年12月2日生效。周松崗於2011年12月31日約滿後退休\[6\]，其職位由來自[美國的](../Page/美國.md "wikilink")[韋達誠](../Page/韋達誠.md "wikilink")（Jay
Walder）接任。

### 公職事務

在公職方面，周松崗現時為[策略發展委員會委員](../Page/策略發展委員會.md "wikilink")、[香港會計師公會理事會成員](../Page/香港會計師公會.md "wikilink")、[香港旅遊發展局成員](../Page/香港旅遊發展局.md "wikilink")、[香港總商會理事會成員](../Page/香港總商會.md "wikilink")，以及自2004年起獲[香港中文大學監督委任為校董](../Page/香港中文大學.md "wikilink")。\[7\]另外，他也是[中國人民政治協商會議](../Page/中國人民政治協商會議.md "wikilink")[深圳市常務委員會之委員](../Page/深圳市.md "wikilink")。周松崗在2004年出任[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")，2008年成為遴選會員，2011年3月4日獲馬會董事局補選成為新任馬會董事局董事，接替年滿70歲而卸任的[布魯士](../Page/布魯士_\(香港會計師\).md "wikilink")（Iain
F. Bruce）。\[8\]

### 陳淑莊事件

在2007年[區議會選舉](../Page/香港區議會.md "wikilink")，周松崗以個人身份支持[公民黨的](../Page/公民黨.md "wikilink")[陳淑莊](../Page/陳淑莊.md "wikilink")，在[中西區山頂選區出戰](../Page/中西區_\(香港\).md "wikilink")[自由黨時任區議員](../Page/自由黨_\(香港\).md "wikilink")[林文傑](../Page/林文傑.md "wikilink")，有關事件結果受到自由黨主席[田北俊的嚴厲批評他](../Page/田北俊.md "wikilink")「沒有政治智慧」。田北俊公開表示自由黨一直以來對[地鐵公司都很支持和表現忠誠](../Page/地鐵公司.md "wikilink")，故他被周松崗的不友善舉動而感到氣憤，並揚言周松崗要為此承擔一切政治後果。另外，田北俊又暗示地鐵在將來[中西區區議會有關地鐵的事宜上](../Page/中西區區議會.md "wikilink")，可能會受到更多自由黨[區議員的異議](../Page/區議員.md "wikilink")。

田北俊發出有關言論後，引起了社會的關注，有意見更認為他干擾選舉的公正性。周松崗事後在2007年10月10日專程致電田北俊道歉，田北俊在翌日表示接受其道歉。到2007年10月12日，田北俊突然就事件向公眾和周松崗公開道歉，並撤回其言論，希望事件告一段落。\[9\]\[10\]\[11\]而陳淑莊最後也當選山頂區議員，成為公民黨新增三席區議會議席之一。

<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>英國氧氣集團董事<br />
<span style="color: blue;">(1993年-1997年)</span></li>
<li>GKN公共有限公司總裁<br />
<span style="color: blue;">(1997年-2001年)</span></li>
<li>渣打集團有限公司非執行董事<br />
<span style="color: blue;">(1997年-2008年)</span></li>
<li>布萊堡工業集團行政總裁<br />
<span style="color: blue;">(2001年-2003年)</span></li>
<li>香港鐵路有限公司行政總裁<br />
（2007年前稱地鐵有限公司）<br />
<span style="color: blue;">(2003年-2011年)</span></li>
<li>渣打銀行(香港)有限公司的非執行主席<br />
<span style="color: blue;">(2004年-2011年)</span></li>
<li>香港中文大學校董<br />
<span style="color: blue;">(2004年-)</span></li>
<li>香港賽馬會董事局董事<br />
<span style="color: blue;">(2011年-)</span></li>
<li>香港交易及結算所有限公司董事會主席<br />
<span style="color: blue;">(2012年-2018年)</span></li>
</ul>
</div></td>
</tr>
</tbody>
</table>

## 榮譽

### 殊榮

  - [下級勳位爵士](../Page/下級勳位爵士.md "wikilink") （K.t.）（2000年元旦授勳名單\[12\]）
  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（J.P.） （2014年7月1日）
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")（G.B.S.）（2015年香港特別行政區政府授勳名單）

### 院士

  - [F.R.Eng.](../Page/皇家工程科學院.md "wikilink")
  - [F.C.G.I.](../Page/倫敦城市行業專業學會.md "wikilink")
  - [F.I.Chem.E.](../Page/化學工程師學會.md "wikilink")
  - [HonFIET](../Page/國際工程技術學會.md "wikilink")
  - [香港工程科學院](../Page/香港工程科學院.md "wikilink")
  - [香港運輸物流學會](../Page/香港運輸物流學會.md "wikilink")
  - [香港工程師學會](../Page/香港工程師學會.md "wikilink") （榮譽）
  - [香港中文大學](../Page/香港中文大學.md "wikilink") （榮譽，2011年\[13\]）

### 榮譽博士

  - [工程學](../Page/工程學.md "wikilink")[博士](../Page/博士.md "wikilink")
    （英國[巴斯大學](../Page/巴斯大學.md "wikilink")）

## 參見

  - [農圃道官立小學](../Page/農圃道官立小學.md "wikilink")
  - [香港培正中學](../Page/香港培正中學.md "wikilink")
  - [香港鐵路有限公司](../Page/香港鐵路有限公司.md "wikilink")
  - [兩鐵合併](../Page/兩鐵合併.md "wikilink")
  - [爵士](../Page/爵士.md "wikilink")

## 註腳

<div class="references-small">

</div>

## 參考資料

<div class="references-small">

  - *[MTR: Serving Hong Kong and Beyond by Sir C K Chow( 周松崗 ) Chief
    Executive Officer, MTR
    Corporation](http://www.hku.hk/hkuf/hkult/subpages/year06/talks/main_oct06.htm)*，[香港大學](../Page/香港大學.md "wikilink")，2006年10月6日。
  - *[公司管治](http://www.mtr.com.hk/chi/investrelation/governance_c.php#04)*，[地鐵公司](../Page/香港地鐵.md "wikilink")，2007年8月11日造訪。
  - *[先賢小傳](https://web.archive.org/web/20070928064813/http://www.puiching.org/database/whos_who.htm)*，培正同學總會，2007年8月11日造訪。
  - *[策略發展委員會名單](https://web.archive.org/web/20070823114000/http://www.cpu.gov.hk/tc/2007_csd_membership.htm)*，香港政府中央政策組，2007年8月11日造訪。
  - *[錢果豐周松崗任香港鐵路主席及行政總裁](http://app.hkatvnews.com/content/2007/08/08/atvnews_107211.html)*，[亞洲電視](../Page/亞洲電視.md "wikilink")，2007年8月8日。
  - *[Message 8](https://www.webcitation.org/query?id=1256565044604122&url=www.geocities.com/sydneypuiching/Message_8.htm)*，[雪梨培正同學會會長葉喬生](../Page/雪梨.md "wikilink")，2003年9月5日。

</div>

## 外部連結

  - [香港鐵路有限公司簡歷](http://www.mtr.com.hk/chi/investrelation/governance.php#04)

{{-}}

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:港鐵公司](../Category/港鐵公司.md "wikilink")
[Category:渣打银行人物](../Category/渣打银行人物.md "wikilink")
[Category:農圃道官立小學校友](../Category/農圃道官立小學校友.md "wikilink")
[Category:香港培正中學校友](../Category/香港培正中學校友.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:威斯康辛大學麥迪遜分校校友](../Category/威斯康辛大學麥迪遜分校校友.md "wikilink")
[Category:戴維斯加州大學校友](../Category/戴維斯加州大學校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:香港交易所](../Category/香港交易所.md "wikilink")
[Category:香港行政會議成員](../Category/香港行政會議成員.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[C松](../Category/周姓.md "wikilink")

1.  [四面楚歌布萊堡主帥請辭](http://business.guardian.co.uk/story/0,3604,1049856,00.html)
    - 英國衛報 2003年 9月26日
2.  葉喬生，《[Message 8](https://www.webcitation.org/query?id=1256565044604122&url=www.geocities.com/sydneypuiching/Message_8.htm)》，雪梨培正同學會，2003年。
3.  [周先生占卜前程](http://www.smh.com.au/articles/2003/09/25/1064083129922.html?from=storyrhs)
    - 雪梨晨鋒報 2003年 9月25日
4.  [布萊堡向周松崗說再見](http://www.smh.com.au/articles/2003/09/25/1064083129919.html?from=storyrhs)
    雪梨晨鋒報 2003年 9月25日
5.  關懿婷，〈[渣打銀行（香港）委任曾璟璇出任新主席](http://www.standardchartered.com.hk/news/2011/c_press_20110110.pdf)
    〉，《即時發佈》，香港：渣打銀行（香港）有限公司公共事務處，2011年1月10日。
6.  [港鐵行政總裁周松崗2011年底退休](http://www.mtr.com.hk/eng/corporate/file_rep/PR-10-123-C.pdf)
7.  〈[書院校董周松崗爵士及友好梁雄姬女士獲頒榮譽院士](http://mmlab.csc.cuhk.edu.hk/eNewsASP/app/article-details.aspx/58AFDC16880259C72C509BD92F85FF4F/)〉，《Shaw
    Net》，香港：香港中文大學逸夫書院，2011年6月號。
8.  〈[周松崗爵士加入香港賽馬會董事局](http://www.hkjc.com/chinese/corporate/racing_news_item.asp?in_file=/chinese/news/2011-03/news_2011030401105.html)〉，《賽馬新聞》，香港賽馬會，2011年3月4日。
9.  [田北俊:
    接受周松崗道歉](http://www.rthk.org.hk/rthk/news/clocal/news.htm?clocal&20071011&55&438164)
     - [香港電台](../Page/香港電台.md "wikilink")
10. [只有一人一黨之私 公衆利益哪裏去了](http://www.mingpaonews.com/20071012/mra.htm) -
    明報
11. [田北俊撤回對周松崗有關區議會提名言論](http://www.rthk.org.hk/rthk/news/expressnews/news.htm?expressnews&20071012&55&438479)
     - 香港電台
12. "[Issue 55710](http://www.london-gazette.co.uk/issues/55710/supplements/1)",
    *London Gazette*, 31 December 1999, p.1.
13. 〈[香港中文大學第十屆榮譽院士頒授典禮](http://www.cpr.cuhk.edu.hk/tc/press_detail.php?id=1029&s=)〉，《新聞稿》，香港：香港中文大學傳訊及公共關係處，2011年5月16日。