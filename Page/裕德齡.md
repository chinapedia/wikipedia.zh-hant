**裕德龄**（），[汉军正白旗人](../Page/汉军正白旗.md "wikilink")，因曾担任[慈禧太后的御前](../Page/慈禧太后.md "wikilink")[女官并用英文写作了这段经历而闻名](../Page/女官.md "wikilink")。

## 早年

1885年（存疑），德龄出生于[武昌](../Page/武昌.md "wikilink")，在兄妹五人中排行第三，在[荆州](../Page/荆州.md "wikilink")、[沙市长大](../Page/沙市.md "wikilink")。其父[裕庚为](../Page/裕庚.md "wikilink")[外交官](../Page/外交官.md "wikilink")，母亲是[法国人](../Page/法国人.md "wikilink")。家族本姓徐，以[随名姓的方式称裕德齡](../Page/随名姓.md "wikilink")。1895年，父亲任出使日本特命全权大臣三年，后又任[驻法公使三年](../Page/中国驻法国大使列表.md "wikilink")。德龄随父在[日本](../Page/日本.md "wikilink")、法国生活六年，不但会外语，還具有开阔的视野和渊博的学识，精通各国国情，曾是现代舞蹈大师[邓肯的弟子](../Page/艾莎道拉·邓肯.md "wikilink")。

## 女官生涯

[The_Qing_Dynasty_Cixi_Imperial_Dowager_Empress_of_China_On_Throne_5.JPG](https://zh.wikipedia.org/wiki/File:The_Qing_Dynasty_Cixi_Imperial_Dowager_Empress_of_China_On_Throne_5.JPG "fig:The_Qing_Dynasty_Cixi_Imperial_Dowager_Empress_of_China_On_Throne_5.JPG")、德龄、[慈禧太后](../Page/慈禧太后.md "wikilink")、[裕容龄](../Page/裕容龄.md "wikilink")、容龄母、[光绪皇后,静芬](../Page/隆裕太后.md "wikilink")\]\]
1902年冬，裕庚任满归国，被赏给[太仆寺卿衔](../Page/太仆寺.md "wikilink")，留京养病。17岁的德龄随父回京。此时列強入侵，[慈禧太后急欲讨好各国使节和他们的夫人](../Page/慈禧太后.md "wikilink")，她从[庆亲王奕劻口中得知裕庚的女儿通晓外文及西方](../Page/奕劻.md "wikilink")[礼仪](../Page/礼仪.md "wikilink")，便下旨召裕庚夫人带同德齡、容齡姊妹入宫觐见，後來德齡与妹妹[容龄一起成为](../Page/裕容龄.md "wikilink")[紫禁城八](../Page/紫禁城.md "wikilink")[女官之一](../Page/女官.md "wikilink")，为慈禧与西方国家使节夫人们交往担任[翻译](../Page/翻译.md "wikilink")。直到1905年3月因父病才离宫到上海。期间慈禧太后曾有意将其许配给[荣禄之子巴龙](../Page/荣禄.md "wikilink")，但为[光绪帝设计解脱](../Page/光绪帝.md "wikilink")。

## 出宫之后

1907年5月21日，德龄与[美国驻沪领事馆副领事](../Page/美国.md "wikilink")[迪厄斯·怀特结婚](../Page/迪厄斯·怀特.md "wikilink")。1915年，随夫赴美。

在美开始用[英文撰写回忆录和纪实文学作品](../Page/英文.md "wikilink")，披露了许多慈禧及清宫的生活情景和晚清政局见闻，都是第一手资料，很受西方读者欢迎(但亦有批評稱德齡所著謬誤頗多，如把將[珍妃推入井中之事歸罪於](../Page/珍妃.md "wikilink")[李連英而非](../Page/李連英.md "wikilink")[崔玉貴](../Page/崔玉貴.md "wikilink"))，也具有丰富的历史价值。著作被[顾秋心](../Page/顾秋心.md "wikilink")、[秦瘦鸥等译为中文](../Page/秦瘦鸥.md "wikilink")，在《[申报](../Page/申报.md "wikilink")》等媒体上刊载，影响较大。

抗战期间，参与[宋庆龄发起的](../Page/宋庆龄.md "wikilink")[保卫中国同盟活动](../Page/保卫中国同盟.md "wikilink")。

晚期則一個人獨居加州柏克萊市，擔任柏克萊大學的漢語教師。1944年11月22日早晨，在加州伯克利死于车祸，於Bancroft
Way和Telegraph Ave交叉口被疾駛而來的雜貨卡車撞上。

## 親人

  - 大哥 名号不详，《清代野记·卷下》称奎龄
  - 二哥 勛齡
  - 四弟 [馨齡](../Page/馨齡.md "wikilink")
    (曾任[自强学堂](../Page/自强学堂.md "wikilink")（[武汉大学前身](../Page/武汉大学.md "wikilink")）提调)
  - 妹妹 [容齡](../Page/裕容齡.md "wikilink")（壽山[郡君](../Page/郡君.md "wikilink")）

## 著作

  - 《[清宫二年记](../Page/清宫二年记.md "wikilink")》
  - 《[清末政局回忆录](../Page/清末政局回忆录.md "wikilink")》
  - 《[御苑兰馨记](../Page/御苑兰馨记.md "wikilink")》
  - 《[瀛台泣血记](../Page/瀛台泣血记.md "wikilink")》
  - 《[御香缥缈录](../Page/御香缥缈录.md "wikilink")》

## 以德齡為主角的作品

  - 話劇《[德齡與慈禧](../Page/德齡與慈禧.md "wikilink")》
  - 2002年电视剧《十三格格》中，[李小璐饰毓琳](../Page/李小璐.md "wikilink")，原型即德龄
  - 2006年在[中国](../Page/中国.md "wikilink")[电视连续剧](../Page/电视连续剧.md "wikilink")《[德齡公主](../Page/德龄公主_\(電視劇\).md "wikilink")》中[張晶晶飾裕德齡](../Page/張晶晶.md "wikilink")
  - 2014年在[中国](../Page/中国.md "wikilink")[电视连续剧](../Page/电视连续剧.md "wikilink")《[末代皇帝传奇](../Page/末代皇帝传奇.md "wikilink")》中[孫耀琦飾德宁格格](../Page/孫耀琦.md "wikilink")
  - 2016年在香港[無綫](../Page/無綫.md "wikilink")[电视连续剧](../Page/电视连续剧.md "wikilink")《[末代御醫](../Page/末代御醫.md "wikilink")》中[蔚雨芯飾德齡](../Page/蔚雨芯.md "wikilink")

## 外部链接

  - [历史上真实的德龄公主](http://qkzz.net/article/a8ecc528-9cce-4fad-bd88-27b437fd7aa2.htm)

[Category:中国现代作家](../Category/中国现代作家.md "wikilink")
[Category:中国小说家](../Category/中国小说家.md "wikilink")
[Category:满洲正白旗人](../Category/满洲正白旗人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:法國裔混血兒](../Category/法國裔混血兒.md "wikilink")
[Category:满洲裔美国人](../Category/满洲裔美国人.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:美国车祸身亡者](../Category/美国车祸身亡者.md "wikilink")
[Category:武昌人](../Category/武昌人.md "wikilink")
[Category:清朝女官](../Category/清朝女官.md "wikilink")