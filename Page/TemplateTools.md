\-{H|zh-cn:信息; zh-tw:資訊}-

<table>
<tbody>
<tr class="odd">
<td><p>工具箱<br />
<br />
<a href="../Page/Template:Tools.md" title="wikilink">-{zh-hans:<span title="查看这个模板">;zh-hant:<span title="檢視這個模板">}--{zh-hans:查;zh-hant:閱}-</span></a> <a href="../Page/Template_talk:Tools.md" title="wikilink">-{zh-hans:<span title="关于这个模板的讨论页面">;zh-hant:<span title="關於這個模板的討論頁面">}-論</span></a> [?action=edit -{zh-hans:<span title="您可以编辑这个模板。请在保存变更之前先预览">;zh-hant:<span title="您可以編輯這個模板。請在儲存變更之前先預覽">}-<span style="color:#002bb8;">編</span></span>] [?action=history -{zh-hans:<span title="查看这个模板的编辑历史">;zh-hant:<span title="查詢這個模板的編輯歷史">}-<span style="color:#002bb8;">歷</span></span>]</p></td>
<td><div style="margin: 0.4em; text-align: center; font-weight: bold;">
<p>下列各项的文字格式及显示效果都在<a href="../Page/Wikipedia:模板消息.md" title="wikilink">模板-{zh-cn:消息; zh-tw:訊息}-列表中</a>，請-{zh-cn:点击; zh-tw:點選}-展開</p>
</div>
<p><strong>（以下带*号者，请加签名~~~~作结）</strong></p>
<ul>
<li><strong>欢迎</strong>：、、*</li>
</ul>
<ul>
<li><strong>提示</strong>：、*、 *、*、、移除無關內容提示、、</li>
</ul>
<ul>
<li><strong>警告</strong>：*、*、*、*、*、<a href="../Page/Special:Blockip.md" title="wikilink">禁封</a>、[?action=unblock&amp;ip= 解封]</li>
</ul>
<ul>
<li><strong>协作</strong>：、、、<a href="../Category/維基百科編輯被保護頁面請求.md" title="wikilink">编辑被保护页面请求</a></li>
</ul>
<ul>
<li><strong>條目常见问题</strong>：（用于完全没有来源）、（用于来源不足）、（用于段落中的来源请求）、、、 、、 、易名及移動、、、、、、 、、、、原创研究、可能违反生者传记、缺少页面分类 、、、、、、裸網址、新聞動態</li>
</ul>
<ul>
<li><strong>争议</strong>：不中立、、、争议条目对话、</li>
</ul>
<ul>
<li><strong>-{zh-cn:文件; zh-tw:檔案;}-</strong>：*、*、*、、、</li>
</ul>
<ul>
<li><strong>提删</strong>：、、</li>
</ul>
<p>:*<strong>限用於<a href="../Page/MediaWiki_talk:Uploadtext-default/zh#重整上載手續.md" title="wikilink">2006年5月11日後新上載的問題圖像</a>，七日以後免討論即可刪除</strong>：圖像出處不明、图片版权不明</p>
<p>::*（更早上載的問題圖像請用 ，例如可標示沒有註明出處版權的圖像，先討論後刪除。）</p>
<ul>
<li><strong><a href="../Page/Wikipedia:頁面存廢討論.md" title="wikilink">删除</a></strong>：辞典、文库、语录、教科书、条目/分类/图像/模板/-{zh-hant:使用者頁面;zh-hans:用户页;}-/杂项、-{zh-cn:重定向; zh-tw:重新導向;}-、、</li>
</ul>
<ul>
<li><strong>合并</strong>：、、</li>
</ul>
<ul>
<li><strong>其他</strong>：正在經歷修改、正在翻譯、<a href="../Page/tools:~river/cgi-bin/count_edits.md" title="wikilink">檢查-{zh-hant:使用者;zh-hans:用户;}-編輯次數</a>、版本差異連結新版本id{{!}}舊版本id}}、添加日期参数、需要翻譯模版說明、[?profile=advanced -{zh-hant:進階搜尋;zh-hans:高级搜索}-]</li>
</ul>
<ul>
<li><strong><a href="../Page/Wikipedia:管理员通告板.md" title="wikilink">管理员</a></strong>：<a href="../Category/快速删除候选.md" title="wikilink">快速删除候选</a>、<a href="../Page/Wikipedia:頁面存廢討論.md" title="wikilink">頁面存廢討論</a>、<a href="../Page/Wikipedia:頁面存廢討論/疑似侵權.md" title="wikilink">侵权</a>、<a href="../Page/Wikipedia:檔案存廢討論.md" title="wikilink">檔案存廢討論</a>、<a href="../Page/Wikipedia:檔案存廢討論/無版權訊息或檔案來源.md" title="wikilink">缺乏來源檔案</a>、<a href="../Page/Wikipedia:移動請求.md" title="wikilink">移动请求</a>、保護（包括争议保护、高風險模板保护、破壞頁保護）、<a href="../Page/Special:链入页面.md" title="wikilink">链入页面</a>、<a href="../Page/Special:日志.md" title="wikilink">公开日志</a>、<a href="../Page/Special:标签.md" title="wikilink">标签</a>、<a href="../Page/Special:特殊页面.md" title="wikilink">特殊页面</a></li>
</ul>
<ul>
<li><strong><a href="../Page/Wikipedia:新页面巡查.md" title="wikilink">巡查员</a></strong>：<a href="../Page/Special:最新页面.md" title="wikilink">新条目</a>（请巡查员注意除了条目以外的<span class="plainlinks">[?namespace=0&amp;invert=1&amp;tagfilter=&amp;username=&amp;hidepatrolled=1&amp;hideredirs= 其他名称空间]</span>）、<a href="../Page/维基百科:Twinkle.md" title="wikilink">Twinkle</a>（<a href="../Page/Wikipedia:Twinkle/参数设置.md" title="wikilink">-{zh-tw:偏好設定;zh-cn:参数设置;}-</a>）、<a href="../Page/Special:滥用日志.md" title="wikilink">防滥用过滤器日志</a>、<a href="../Page/Special:Log/patrol.md" title="wikilink">巡查記錄</a>、<a href="../Page/Special:短页面.md" title="wikilink">短条目</a></li>
</ul></td>
</tr>
<tr class="even">
<td><ul>
<li><strong><a href="../Page/Help:编辑手册.md" title="wikilink">编辑</a>（<a href="../Page/Wikipedia:備忘單.md" title="wikilink">語法摘要</a>）</strong>：<a href="../Page/Wikipedia:链接.md" title="wikilink">連結</a>、<a href="../Page/Wikipedia:表格.md" title="wikilink">表格</a>、<a href="../Page/Help:魔術字.md" title="wikilink">魔术字</a>、<a href="../Page/Wikipedia:图像版权标志.md" title="wikilink">图像版权标志</a>、<a href="../Page/Wikipedia:如何编辑页面.md" title="wikilink">Wiki語法大全</a>、<a href="../Page/:Template:Wide_image.md" title="wikilink">闊圖像</a>、<a href="../Page/:Template:Tall_image.md" title="wikilink">高圖像</a>、<a href="../Page/:Template:NoteTA.md" title="wikilink">全文字词转换</a></li>
<li><strong><a href="../Page/Wikipedia:模板消息.md" title="wikilink">模板</a>（<a href="../Category/維基百科模板.md" title="wikilink">分類</a>）</strong>：<a href="../Category/格式模板.md" title="wikilink">格式</a>、<a href="../Category/语言模板.md" title="wikilink">语言</a>、<a href="../Category/文字模板.md" title="wikilink">文字</a>、<a href="../Page/Wikipedia:消歧义.md" title="wikilink">消歧義</a>、<a href="../Category/目錄模板.md" title="wikilink">目錄靠右</a>、主條目、參見、譯名請求</li>
</ul>
<ul>
<li><strong>翻译</strong>：进行中、如何翻译[[<a href="../Page/Template:_HowToTranslate.md" title="wikilink">Template talk:HowToTranslate#「」的译法</a>]]、已翻译（放置于讨论页）{{[[template:_Translated_page|translated page]]&lt;nowiki&gt;}}、请求翻译{{[[Template:Request_translation|Request translation]]]]&lt;nowiki&gt;}}、<a href="../Page/Wikipedia:翻译请求.md" title="wikilink">翻译请求</a></li>
</ul>
<ul>
<li><a href="../Page/Wikipedia:列明来源.md" title="wikilink"><strong>參考文獻引用</strong></a>：</li>
</ul>
<ol>
<li>書籍引用{{<a href="../Page/Template:cite_book.md" title="wikilink">cite book</a>{{!}}author=作者名 {{!}}coauthors=聯名作者 {{!}}title=書名 {{!}}year=年份 {{!}}publisher=出版商 {{!}}location=出版商所在地 {{!}}isbn=書籍編號 {{!}}date=出版日期 {{!}}accessdate=查閱日期 }}</li>
<li>期刊引用{{<a href="../Page/Template:cite_journal.md" title="wikilink">cite journal</a>{{!}}last=名 {{!}}first=姓 {{!}}authorlink= {{!}}coauthors= 共同编写人{{!}}year= 年 {{!}}title=文獻標題 {{!}}journal=期刊名 {{!}}volume=卷號 {{!}}issue=期號 {{!}}publisher=出版商 {{!}}pages=參考頁數 {{!}}id= {{!}}url=網址 {{!}}date=出版日期 {{!}}accessdate=查閱日期 {{!}}quote= }}</li>
<li>可靠的新聞媒體引用{{<a href="../Page/Template:cite_news.md" title="wikilink">cite news</a>{{!}}title=網頁標題 {{!}}url=網址 {{!}}language=語言 {{!}}author=作者 {{!}}publisher=發表該新聞的公司 {{!}}date=出版日 {{!}}accessdate=检索日期 }}</li>
<li>媒體除外的參考網址引用{{<a href="../Page/Template:cite_web.md" title="wikilink">cite web</a>{{!}}url=網址 {{!}}language=語言 {{!}}title=網頁標題 {{!}}publisher=引自網站 {{!}} date=網頁日期 {{!}}accessdate=检索日期 }}</li>
<li>尚未內文引註的外部連結：[1]</li>
</ol>
<dl>
<dt></dt>
<dd>請於引文兩頭加上 &lt;ref&gt;...&lt;/ref&gt; 標簽（或&lt;ref name="A"&gt; &lt;/ref&gt;同項參考標籤），並於條目尾加上  以自動列出引用
</dd>
</dl>
<ul>
<li><strong>跨语言</strong>：未创建条目链接至英文（其中 en 可改为其它语言，参见<a href="../Page/ISO_639-1代码表.md" title="wikilink">ISO 639-1代码表</a>）</li>
<li>碰到<a href="../Page/維基百科:失效連結.md" title="wikilink">失效链接</a>{{<a href="../Page/Template:dead_link.md" title="wikilink">dead link</a>{{!}}date=失效日期}}時，可以<a href="../Page/維基百科:使用時光機.md" title="wikilink">使用時光機作為替代方案</a>。</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><ul>
<li><strong>SVG制图</strong>：<a href="../Page/SVG.md" title="wikilink">说明与工具下载</a>、<a href="../Page/meta:SVG_fonts.md" title="wikilink">-{zh-cn:支持字体; zh-tw:支援字型}-</a>、<a href="../Page/颜色列表.md" title="wikilink">颜色列表</a></li>
<li><strong>工具</strong>：<a href="../Page/Wikipedia:维基百科工具.md" title="wikilink">維基百科工具</a>、<a href="../Page/Special:Export.md" title="wikilink">-{zh-tw:匯出;zh-cn:导出;}-頁面</a></li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><strong><a href="../Page/Wikipedia:投票.md" title="wikilink">投票</a></strong>：<a href="../Page/Wikipedia:条目评选.md" title="wikilink">条目评选</a>、<a href="../Page/維基百科:新條目推薦/候選.md" title="wikilink">DYK</a>、<a href="../Page/維基百科:優良條目評選.md" title="wikilink">優良條目</a>、<a href="../Page/Wikipedia:典范条目评选.md" title="wikilink">典範條目</a>、<a href="../Page/維基百科:特色列表評選.md" title="wikilink">列表</a>、<a href="../Page/Wikipedia:特色圖片評選.md" title="wikilink">圖片</a>，<a href="../Page/Wikipedia:申请成为管理员.md" title="wikilink">管理员投票</a>、<a href="../Page/Wikipedia:維基獎勵/授獎提名投票.md" title="wikilink">維基獎勵</a>、<a href="../Page/Wikipedia:机器人.md" title="wikilink">-{zh-tw:機器人;zh-hk:機械人;zh-cn:机器人;}-</a>、<a href="../Category/進行中的投票.md" title="wikilink">更多投票</a>、<a href="../Category/訊息圖示模板.md" title="wikilink">-{zh-hant:訊息圖示;zh-hans:信息图标;}-</a></li>
<li><strong>统计</strong>：<a href="../Page/Special:Statistics.md" title="wikilink">站点统计</a>、[//www.wikipedia.org/wikistats/ZH/Sitemap.htm 统计图表]、<a href="../Page/Special:Wantedcategories.md" title="wikilink">需要的分类</a></li>
<li><strong>转换</strong>：<a href="../Page/Wikipedia:简繁一多对应校验表.md" title="wikilink">简繁校验</a>、<a href="../Page/Wikipedia:字词转换请求或候选.md" title="wikilink">转换请求或候选</a>、<a href="../Page/Wikipedia:字詞轉換處理.md" title="wikilink">字词转换处理</a></li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><ul>
<li><strong>守则</strong>：<a href="../Page/Wikipedia:方针与指引.md" title="wikilink">方针与指引</a>、<a href="../Page/Wikipedia:中立的观点.md" title="wikilink">中立</a>、<a href="../Page/Wikipedia:简明版权信息.md" title="wikilink">版权指导</a>、<a href="../Page/Help:目录.md" title="wikilink">使用手册</a>、<a href="../Page/Wikipedia:合理使用.md" title="wikilink">合理使用指引</a>、<a href="../Page/Wikipedia:合理使用準則.md" title="wikilink">判断准则</a></li>
<li><strong>栏目</strong>：<a href="../Page/Wikipedia:典范条目/存档.md" title="wikilink">典范条目</a>、<a href="../Page/Wikipedia:优良条目/存档.md" title="wikilink">优良条目</a>、<a href="../Page/Template:{{CURRENTMONTHNAME}}{{CURRENTDAY}}日.md" title="wikilink">历史上的今天</a>（<a href="../Page/历史上的今天.md" title="wikilink">总目录</a>）、<a href="../Page/Template:Dyk.md" title="wikilink">你知道吗？</a>、<a href="../Page/Template:Itn.md" title="wikilink">新闻动态</a>、<a href="../Page/Template:Toptss.md" title="wikilink">关注焦点</a>、<a href="../Page/Template:Featurepic.md" title="wikilink">特色图片</a>、<a href="../Page/Special:Newimages.md" title="wikilink">新圖像顯示</a></li>
<li><strong><a href="../Page/Wikipedia:社区主页.md" title="wikilink">-{zh-hant:社群;zh-hans:社区;}-</a></strong>：<a href="../Page/Wikipedia:行事曆.md" title="wikilink">行事历</a>、<a href="../Page/Template:Bulletin.md" title="wikilink">公告栏</a>、<a href="../Page/Wikipedia:聊天.md" title="wikilink">聊天</a>、<a href="../Page/Wikipedia:互助客栈.md" title="wikilink">互助客栈</a>、<a href="../Page/Wikipedia:維基專題.md" title="wikilink">专题</a>、<a href="../Page/Wikipedia:维基荣誉与奖励.md" title="wikilink">荣誉与奖励</a></li>
<li><strong>條目請求</strong>：<a href="../Page/Wikipedia:條目請求.md" title="wikilink">一般</a>、<a href="../Page/Special:Wantedpages.md" title="wikilink">最多被征求的条目</a>、<a href="../Page/Wikipedia:翻譯請求.md" title="wikilink">翻譯</a>、<a href="../Page/Wikipedia:專題/傳統百科全書條目.md" title="wikilink">傳統百科</a>、<a href="../Page/Wikipedia:最多語言版本的待撰條目.md" title="wikilink">最多語言版本</a>、<a href="../Page/Wikipedia:图片请求.md" title="wikilink">圖片</a></li>
<li><strong><a href="../Page/Special:Log.md" title="wikilink">日誌</a></strong>：<a href="../Page/Special:Log/delete.md" title="wikilink">刪除日誌</a>、<a href="../Page/Special:Log/move.md" title="wikilink">移動日誌</a>、<a href="../Page/Special:Log/protect.md" title="wikilink">頁面保護日誌</a>、<a href="../Page/Special:Log/upload.md" title="wikilink">上載紀錄</a>、<a href="../Page/Special:Log/newusers.md" title="wikilink">新進-{zh-hant:使用者;zh-hans:用户;}-名冊</a>、<a href="../Page/Special:Log/block.md" title="wikilink">查封日誌</a>、<a href="../Page/Special:Log/rights.md" title="wikilink">-{zh-hant:使用者;zh-hans:用户;}-權限日誌</a></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

<noinclude>  </noinclude>

[工具](../Category/維基百科管理模板.md "wikilink")
[Category:維護導航](../Category/維護導航.md "wikilink")

1.  [某網站](http://example.com/)