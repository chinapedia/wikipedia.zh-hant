**丁香属**（[学名](../Page/学名.md "wikilink")：**），又稱**紫丁香屬**，该属多種植物的統稱为丁香，别名“百结”、“情客”、“紫丁香”，生长于温带及寒带，落叶灌木或小乔木，圆球形树冠。单叶对生，卵圆形，圆锥花序，白色、紫色，花冠筒状，芳香。蒴果9月成熟。同属包括[白花丁香](../Page/白花丁香.md "wikilink")、[红花丁香](../Page/红花丁香.md "wikilink")、[紫花丁香](../Page/紫花丁香.md "wikilink")、[荷花丁香](../Page/荷花丁香.md "wikilink")、[小叶丁香](../Page/小叶丁香.md "wikilink")、[花叶丁香](../Page/花叶丁香.md "wikilink")、[四季丁香等](../Page/四季丁香.md "wikilink")。

在法国，“丁香花开的时候”意指气候最好的时候。 生日是2月10日，5月17日或者6月12日的人的幸运花是丁香花。
在西方，该花象征着“年轻人纯真无邪，初恋和谦逊”。
[Syringia_julianae0.jpg](https://zh.wikipedia.org/wiki/File:Syringia_julianae0.jpg "fig:Syringia_julianae0.jpg")
[Xian'e_Changchun_Album_05.jpg](https://zh.wikipedia.org/wiki/File:Xian'e_Changchun_Album_05.jpg "fig:Xian'e_Changchun_Album_05.jpg")绘制的紫白丁香图\]\]

另外，[桃金娘科](../Page/桃金娘科.md "wikilink")[蒲桃属的一种](../Page/蒲桃属.md "wikilink")[热带香料植物也被称作](../Page/热带.md "wikilink")[丁香](../Page/丁香.md "wikilink")。

## 圖片

<File:Syringa> microphylla C.jpg| <File:Syringa> emodi 002.jpg|
<File:Syringa> vulgaris Sarah Sands.jpg| <File:Syringa> meyeri
Palibin.jpg| <File:Lilacs> 2.jpg|

## 分类

  - 紫丁香亚属 Subg. *Eusyringa*
      - 红丁香组 Sect. *Villosa*
          - [紅丁香](../Page/紅丁香.md "wikilink") *Syringa villosa*
          - [辽东丁香](../Page/辽东丁香.md "wikilink") *Syringa wolfii*
          - [喜馬拉雅丁香](../Page/喜馬拉雅丁香.md "wikilink") *Syringa emodi*
          - [雲南丁香](../Page/雲南丁香.md "wikilink") *Syringa yunnanensis*
          - [垂丝丁香](../Page/垂丝丁香.md "wikilink") *Syringa reflexa*
          - [匈牙利丁香](../Page/匈牙利丁香.md "wikilink") *Syringa josikaea*
          - [西蜀丁香](../Page/西蜀丁香.md "wikilink") *Syringa komarowii*（异名*S.
            reflexa*）
          - [毛丁香](../Page/毛丁香.md "wikilink") *Syringa tomentella*
          - [四川丁香](../Page/四川丁香.md "wikilink") *Syringa sweginzowii*
          - [梯氏丁香](../Page/梯氏丁香.md "wikilink") *Syringa tigerstedtii*
          - [亨利丁香](../Page/亨利丁香.md "wikilink") *Syringa henryi*（*S.
            josikaea* × *S. villosa*）
      - 毛丁香组 Sect. *Pubescentes*
          - [秦岭丁香](../Page/秦岭丁香.md "wikilink") *Syringa giraldiana*
          - [巧玲花](../Page/巧玲花.md "wikilink") *Syringa pubescens*（异名*S.
            julianae*、*S. patula*）
              - [紫丁香](../Page/紫丁香.md "wikilink") *Syringa juliana*
              - [关东丁香](../Page/关东丁香.md "wikilink") *Syringa velutina*
          - [山丁香](../Page/山丁香.md "wikilink") *Syringa potaninii*
          - [皱叶丁香](../Page/皱叶丁香.md "wikilink") *Syringa mairei*
          - [松林丁香](../Page/松林丁香.md "wikilink") *Syringa pinetorum*
          - [四季丁香](../Page/四季丁香.md "wikilink") *Syringa microphylla*
          - [小叶丁香](../Page/小叶丁香.md "wikilink") *Syringa pubescens*
          - [藍丁香](../Page/藍丁香.md "wikilink") *Syringa meyeri*
              - [小叶蓝丁香](../Page/小叶蓝丁香.md "wikilink") *Syringa spontanea*
          - [圆叶丁香](../Page/圆叶丁香.md "wikilink") *Syringa wardii*
      - 紫丁香组 Sect. *Vulgaris*
          - [歐洲丁香](../Page/歐洲丁香.md "wikilink") *Syringa vulgaris*
          - [華北丁香](../Page/華北丁香.md "wikilink") *Syringa oblata*
          - [朝鲜丁香](../Page/朝鲜丁香.md "wikilink") *Syringa dilatata*
          - [甘肃丁香](../Page/甘肃丁香.md "wikilink") *Syringa buxifolia*
          - [阿富汗丁香](../Page/阿富汗丁香.md "wikilink") *Syringa afghanica*
          - [什锦丁香](../Page/什锦丁香.md "wikilink") *Syringa chinensis*
      - 羽叶丁香组 Sect. *Pinnatifoliae*
          - [羽葉丁香](../Page/羽葉丁香.md "wikilink") *Syringa pinnatifolia*
  - 拟女贞亚属 Subg. *Ligustina*
      - [北京丁香](../Page/北京丁香.md "wikilink") *Syringa pekinesis*
      - [暴马丁香](../Page/暴马丁香.md "wikilink") *Syringa reticulata*
      - [日本丁香](../Page/日本丁香.md "wikilink") *Syringa japonica*
  - [华丁香](../Page/华丁香.md "wikilink") *Syringa protolaciniata*
  - [藏南丁香](../Page/藏南丁香.md "wikilink") *Syringa tibetica*

### 雜交品種

  - *Syringa × diversifolia*（*S. oblata* × *S. pinnatifolia*）
  - *Syringa × henryi*（*S. josikaea* × *S. villosa*）
  - *Syringa × hyacinthiflora*（*S. oblata* × *S. vulgaris*）
  - *Syringa × josiflexa*（*S. josikaea* × *S. komarowii*）
  - *Syringa × laciniata*（*S. protolaciniata* × *S. vulgaris*）
  - [花叶丁香](../Page/花叶丁香.md "wikilink")*Syringa × persica*（*S.
    protolaciniata* × 未知）
  - *Syringa × prestoniae*（*S. komarowii* × *S. villosa*）
  - *Syringa × swegiflexa*（*S. komarowii* × *S. sweginzowii*）

[Syringa_microphylla_C.jpg](https://zh.wikipedia.org/wiki/File:Syringa_microphylla_C.jpg "fig:Syringa_microphylla_C.jpg")
*Syringa microphylla*\]\]

英文參考資料：\[1\]\[2\]\[3\]\[4\]

中文參考資料：\[5\]

## 参考文献

## 外部链接

[\*](../Category/丁香属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")

1.  Flora Europaea:
    [*Syringa*](http://rbg-web2.rbge.org.uk/cgi-bin/nph-readbtree.pl/feout?FAMILY_XREF=&GENUS_XREF=Syringa&SPECIES_XREF=&TAXON_NAME_XREF=&RANK=)
2.  Flora of China:
    [*Syringa*](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=132143)
3.  Flora of Pakistan:
    [*Syringa*](http://www.efloras.org/florataxon.aspx?flora_id=5&taxon_id=132143)
4.  Germplasm Resources Information Network:
    [*Syringa*](http://www.ars-grin.gov/cgi-bin/npgs/html/splist.pl?11814)

5.  中國食品百科 [1](http://www.foodbk.com/wiki/%E4%B8%81%E9%A6%99)