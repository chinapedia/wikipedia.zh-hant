[Phoneme-allophone-determination-chart.svg](https://zh.wikipedia.org/wiki/File:Phoneme-allophone-determination-chart.svg "fig:Phoneme-allophone-determination-chart.svg")

**同位異音**（）是[語言學術語](../Page/語言學.md "wikilink")，指的是一個[音位可以表示多於一個音](../Page/音位.md "wikilink")。又稱為**同位音**、**音位變體**。\[1\]

## 範例

例如[英語中](../Page/英語.md "wikilink")，国际音标可以用來表示“top”中[送氣的](../Page/送氣.md "wikilink")或者“stop”中不送氣的。换而言之，和在英語中为同位音，它們的共同音位是。

又如[普通話中](../Page/普通話.md "wikilink")，可以用來表示“德”的韻母，又可以表示[感嘆詞](../Page/感嘆詞.md "wikilink")“呢”的韻母，這樣和在普通話中就是同位音，它們的共同音位是。

[聲調語言的同位異音有時稱為](../Page/聲調.md "wikilink")[同位異調](../Page/同位異調.md "wikilink")（allotone），比如在[中文里的](../Page/中文.md "wikilink")[輕聲調](../Page/輕聲調.md "wikilink")（neutral
tone）。

## 參見

  -
  -
  - [音位](../Page/音位.md "wikilink")

  - [自由變異](../Page/自由變異.md "wikilink")

## 註釋

## 外部連結

  - [Phonemes and
    allophones](https://web.archive.org/web/20110212012436/http://www.elloandfriends.uni-osnabrueck.de/wikis/1/show?n=PhoneticsandPhonology.PhonemesAndAllophones)

[Category:語音學](../Category/語音學.md "wikilink")

1.