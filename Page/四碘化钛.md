**四碘化钛**是[钛的](../Page/钛.md "wikilink")[碘化物](../Page/碘化物.md "wikilink")，[化学式为](../Page/化学式.md "wikilink")[Ti](../Page/钛.md "wikilink")[I](../Page/碘.md "wikilink")<sub>4</sub>。它为分子晶体，分子为正四面体构型，钛(IV)占中心。\[1\]它可在常压下蒸馏而不分解，并且它与[TiCl<sub>4</sub>的熔点差异](../Page/四氯化钛.md "wikilink")(150°C与-24°C)也与[CCl<sub>4</sub>和](../Page/四氯化碳.md "wikilink")[CI<sub>4</sub>的熔点差异](../Page/四碘化碳.md "wikilink")(-23°C与168°C)类似，表明碘化物中存在的强分子间作用力。

## 生产

四碘化钛可通过以下三种方法生产：

1\) 单质425°C下在管式炉中化合：\[2\]

  -

      -
        Ti + 2 I<sub>2</sub> → TiI<sub>4</sub>

相应的可逆反应可用于生产高纯的金属钛。\[3\]

2\)
[四氯化钛与](../Page/四氯化钛.md "wikilink")[HI进行复分解反应](../Page/碘化氢.md "wikilink")：

  -

      -
        TiCl<sub>4</sub> + 4 HI → TiI<sub>4</sub> + 4 HCl

3\)
[二氧化钛与](../Page/二氧化钛.md "wikilink")[碘化铝进行复分解反应](../Page/碘化铝.md "wikilink")：

  -

      -
        3 TiO<sub>2</sub> + 4 AlI<sub>3</sub> → 3 TiI<sub>4</sub> + 2
        Al<sub>2</sub>O<sub>3</sub>

## 反应

四碘化钛与四氯化钛和四溴化钛类似，作为[路易斯酸](../Page/路易斯酸.md "wikilink")，与[路易斯碱生成加合物](../Page/路易斯碱.md "wikilink")，也可发生还原反应。金属钛存在下还原，得到Ti(III)和Ti(II)的聚合物，如CsTi<sub>2</sub>I<sub>7</sub>及链状的CsTiI<sub>3</sub>。\[4\]其[CH<sub>2</sub>Cl<sub>2</sub>溶液与](../Page/二氯甲烷.md "wikilink")[烯烃和](../Page/烯烃.md "wikilink")[炔烃反应](../Page/炔烃.md "wikilink")，生成有机碘化物。\[5\]

## 参考资料

[Category:钛化合物](../Category/钛化合物.md "wikilink")
[Category:碘化物](../Category/碘化物.md "wikilink")
[Category:金属卤化物](../Category/金属卤化物.md "wikilink")

1.   [DOI](../Page/:doi:10.1021/ic50197a013.md "wikilink")
2.  Lowery, R. N.; Fay, R. C. "Titanium(IV) Iodide" Inorganic Syntheses
    1967, volume X, pages 1-6.
3.   [DOI](../Page/:doi:10.1021/ie50482a016.md "wikilink")
4.  Jongen, L.; Gloger, T.; Beekhuizen, J.; Meyer, G. "Divalent
    Titanium: The Halides ATiX<sub>3</sub> (A = K, Rb, Cs; X = Cl, Br,
    I)" Zeitschrift fur Anorganische und Allgemeine Chemie 2005, volume
    631, pages 582-586.
5.  Shimizu, Makoto; Toyoda, Tadahiro; Baba, Toru. An Intriguing
    Hydroiodination of Alkenes and Alkynes with Titanium Tetraiodide.
    Synlett 2005, volume 16, pages 2516-2518.