**[安哥拉](../Page/安哥拉.md "wikilink")**一級行政區為[省](../Page/省.md "wikilink")（[葡萄牙語](../Page/葡萄牙語.md "wikilink")：），共有18個。

[Angola_Provinces_numbered_300px.png](https://zh.wikipedia.org/wiki/File:Angola_Provinces_numbered_300px.png "fig:Angola_Provinces_numbered_300px.png")

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>省份</p></th>
<th><p>葡语</p></th>
<th><p><a href="../Page/ISO_3166-2.md" title="wikilink">代碼</a></p></th>
<th><p>省府</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/本哥省.md" title="wikilink">本哥省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/卡西托.md" title="wikilink">卡西托</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/本吉拉省.md" title="wikilink">本吉拉省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/本吉拉.md" title="wikilink">本吉拉</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/比耶省.md" title="wikilink">比耶省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/奎托.md" title="wikilink">奎托</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/喀丙達省.md" title="wikilink">喀丙達省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/卡宾达.md" title="wikilink">-{zh-hans:卡宾达; zh-hant:喀丙達;}-</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/庫安多古班哥省.md" title="wikilink">庫安多古班哥省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/梅農蓋.md" title="wikilink">梅農蓋</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/北廣薩省.md" title="wikilink">北廣薩省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/恩達拉坦多.md" title="wikilink">恩達拉坦多</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/南廣薩省.md" title="wikilink">南廣薩省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/孫貝.md" title="wikilink">孫貝</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/庫內納省.md" title="wikilink">庫內納省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/翁吉瓦.md" title="wikilink">翁吉瓦</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/萬博省.md" title="wikilink">萬博省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/萬博.md" title="wikilink">萬博</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/威拉省.md" title="wikilink">威拉省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/盧班戈.md" title="wikilink">盧班戈</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><a href="../Page/羅安達省.md" title="wikilink">羅安達省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/羅安達.md" title="wikilink">羅安達</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/北倫達省.md" title="wikilink">北倫達省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/盧卡帕.md" title="wikilink">盧卡帕</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/南倫達省.md" title="wikilink">南倫達省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/紹里木.md" title="wikilink">紹里木</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/馬蘭哲省.md" title="wikilink">馬蘭哲省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/馬蘭哲.md" title="wikilink">馬蘭哲</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/莫希科省.md" title="wikilink">莫希科省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/盧埃納.md" title="wikilink">盧埃納</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/納米貝省.md" title="wikilink">-{zh-hant:納米貝省; zh-hans:纳米贝省; zh-tw:納米貝省}-</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/納米貝.md" title="wikilink">-{zh-hant:納米貝; zh-hans:纳米贝; zh-tw:納米貝}-</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><a href="../Page/威熱省.md" title="wikilink">威熱省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/威熱.md" title="wikilink">威熱</a></p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/薩伊省.md" title="wikilink">薩伊省</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/姆班扎剛果.md" title="wikilink">姆班扎剛果</a></p></td>
</tr>
</tbody>
</table>

## 參見

[安哥拉行政區劃](../Category/安哥拉行政區劃.md "wikilink")