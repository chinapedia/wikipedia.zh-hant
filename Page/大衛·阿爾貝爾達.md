**大衛·艾比達**（，），已退役的[西班牙足球運動員](../Page/西班牙.md "wikilink")，司職[防守中場](../Page/防守中場.md "wikilink")，其職業生涯大部分時間效力[西甲球會](../Page/西甲.md "wikilink")[華倫西亞](../Page/華倫西亞足球會.md "wikilink")。

## 生平

### 球會

艾比達是世界一級防守中場，有球評家這樣說過，意大利有加圖索，西班牙有艾比達。艾比達職業生涯由中堅開始，後轉打防守中場，其活動範圍甚廣，踢法勇悍，具有出色的防守能力。艾比達出色的防守表現，成為防線上的堅固屏障。多年來，華倫西亞的駭人的防守能力，艾比達實是功不可沒。

2001年，有華倫西亞之神之稱的[門迭塔離隊後](../Page/門迭塔.md "wikilink")，艾比達接替隊長一職，並帶領球會兩奪西甲冠軍及歐洲足協杯冠軍。這位土生土長的華倫西亞人成為了當地的傳奇人物，多人的偶像。

2007年球季上半季，[華倫西亞表現欠佳](../Page/華倫西亞足球會.md "wikilink")，艾比達因而在12月中被新任主教練[罗纳德·科曼棄用](../Page/罗纳德·科曼.md "wikilink")<small>\[1\]</small>。艾比達當時仍然是華倫西亞的球員，但不可與球隊一起操練及比賽。故他採取了法律行動，與球會對簿公堂。結果他敗訴，且嚴重損害了他與球會的關係及球迷們對他的印象。2008年4月21日，[罗纳德·科曼被球會解僱](../Page/罗纳德·科曼.md "wikilink")，艾比達重新得到上陣機會，但不再擔任球隊隊長。

2010年8月1日，華倫西亞作客[馬賽的季前熱身賽](../Page/马赛足球俱乐部.md "wikilink")，艾比達在賽事末段重新戴上了隊長臂章。

### 國家隊

他曾出席[2002年日韓世界杯及](../Page/2002年世界杯足球赛.md "wikilink")[2006年德國世界杯](../Page/2006年世界杯足球赛.md "wikilink")，也曾於2000年[悉尼奧運贏得銀牌](../Page/悉尼奧運.md "wikilink")。曾是西班牙國家隊主力成員之一。

## 榮譽

  - [西班牙](../Page/西班牙國家足球隊.md "wikilink")
      - [2000年悉尼奧運會](../Page/2000年悉尼奧運會.md "wikilink")
          - 銀牌

<!-- end list -->

  - [華倫西亞](../Page/華倫西亞足球會.md "wikilink")
      - [西班牙足球甲級聯賽冠軍](../Page/西班牙足球甲級聯賽.md "wikilink")（2次）
          - 2001/02, 2003/04
      - [西班牙盃冠軍](../Page/西班牙盃.md "wikilink")（1次）
          - 2007/08
      - [西班牙超級盃冠軍](../Page/西班牙超級盃.md "wikilink")（1次）
          - 2004
      - [歐洲足協杯冠軍](../Page/歐洲足協杯.md "wikilink")（1次）
          - 2003/04
      - [歐洲超級盃冠軍](../Page/歐洲超級盃.md "wikilink")（1次）
          - 2004

## 參考連結

## 註腳

<div class="references-small">

<references />

</div>

[Category:西班牙足球運動員](../Category/西班牙足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:西班牙奧林匹克運動會銀牌得主](../Category/西班牙奧林匹克運動會銀牌得主.md "wikilink")
[Category:2000年夏季奥林匹克运动会奖牌得主](../Category/2000年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:維拉利爾球員](../Category/維拉利爾球員.md "wikilink")
[Category:華倫西亞球員](../Category/華倫西亞球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2000年夏季奧林匹克運動會足球運動員](../Category/2000年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:奧林匹克運動會足球獎牌得主](../Category/奧林匹克運動會足球獎牌得主.md "wikilink")

1.  [華倫西亞進行大清洗](http://china.goal.com/hk/Articolo.aspx?ContenutoId=521255)