**M線第六大道慢車**（），又稱**紐約地鐵M線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的一條[地鐵路線](../Page/地鐵.md "wikilink")。由於該線在[曼哈頓使用](../Page/曼哈頓.md "wikilink")[IND第六大道線](../Page/IND第六大道線.md "wikilink")，因此其路線徽號為橙色\[1\]。

M線因為[BMT默特爾大道線短暫關閉而分為兩段](../Page/BMT默特爾大道線.md "wikilink")，正常情況下來往[中村-大都會大道](../Page/中村-大都會大道車站_\(BMT默特爾大道線\).md "wikilink")，直至2018年4月。其中一段以[接駁線來往](../Page/紐約地鐵S線.md "wikilink")的[大都會大道與](../Page/中村-大都會大道車站_\(BMT默特爾大道線\).md "wikilink")[布什維克的](../Page/布什維克_\(布魯克林\).md "wikilink")[默特爾-威克奧夫大道](../Page/默特爾-威克奧夫大道車站_\(BMT默特爾大道線\).md "wikilink")，任何時段都營運。另一段則除深夜外任何時段都營運。平日日間來往[森林小丘的](../Page/森林小丘.md "wikilink")[71大道與](../Page/森林小丘-71大道車站_\(IND皇后林蔭路線\).md "wikilink")的[百老匯交匯](../Page/百老匯交匯車站_\(BMT牙買加線\).md "wikilink")，停靠沿途所有車站；有限度服務以[東村的](../Page/東村_\(曼哈頓\).md "wikilink")[第二大道為總站而不是百老匯交匯車站](../Page/第二大道車站_\(IND第六大道線\).md "wikilink")；平日深夜列車由百老匯交匯開往[曼哈頓下城的](../Page/曼哈頓下城.md "wikilink")[錢伯斯街](../Page/錢伯斯街車站_\(BMT納蘇街線\).md "wikilink")而不是71大道；週末日間列車來往百老匯交匯與曼哈頓[下東城的](../Page/下東城.md "wikilink")[埃塞克斯街](../Page/埃塞克斯街車站_\(BMT納蘇街線\).md "wikilink")。

2010年以前M線途經[BMT納蘇街線](../Page/BMT納蘇街線.md "wikilink")、[BMT第四大道線](../Page/BMT第四大道線.md "wikilink")、[BMT西城線](../Page/BMT西城線.md "wikilink")、布魯克林南部到[第九大道或](../Page/第九大道車站_\(BMT西城線\).md "wikilink")[海灣公園道](../Page/海灣公園道車站_\(BMT西城線\).md "wikilink")。M線沿[BMT布萊頓線前往](../Page/BMT布萊頓線.md "wikilink")[康尼島-斯提威爾大道車站直至](../Page/康尼島-斯提威爾大道車站.md "wikilink")1987年為止。BMT默特爾大道線在以西路段拆除前曾經有MJ線行走全線。

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
<td><div class="thumbcaption">
<p><a href="../Page/R27_(紐約地鐵車輛).md" title="wikilink">R27</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
</tr>
</tbody>
</table>

</div>

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（圓形）</p>
</div></td>
<td></td>
</tr>
</tbody>
</table>

</div>

1924年，行於[BMT麥朵大道線的麥朵大道](../Page/BMT麥朵大道線.md "wikilink")-欽伯斯街線和麥朵大道線被編為10以及11號線。前者經由麥特大道東半部及[BMT百老匯-布魯克林線](../Page/BMT牙買加線.md "wikilink")，訖於欽伯斯街車站；後者則終於公園路車站。

1925至1931年間，10號線改為快車，自艾塞克斯街車站至麥朵大道車站，週日列車於1933年6月停駛。

1944年3月5日，麥朵大道線關閉橋街-傑里街車站\[2\]以西線路，11號線列車亦以橋街-傑里街車站為終點。

1952年6月28日10號線列車停駛週六快車；1958年6月28日停駛尖峰以外的列車。

1960年起，M線與MJ線取代前述兩線，重編為10號以及11號線。列車從未顯示過MJ線，其只存於存於地圖及車站標示中。

1960年2月23日，尖峰方面的10號線列車增停瑪西大道車站。

1968年7月1日，M線延駛至廣街。曾在此期間有提出經[IND第六大道線至](../Page/IND第六大道線.md "wikilink")57街車站的MM線，但無疾而終。

隔年10月3日麥朵大道線西半部關閉，MJ線停駛，增開SS線區間服務。

1972年區間線停駛，麥朵大道線剩M線列車行駛。

1976年8月27日[K線停駛](../Page/紐約地鐵K線.md "wikilink")，M線調整為慢車。

1986年4月26日布魯克林的訖站調至95街車站，並於[BMT第四大道線行快車](../Page/BMT第四大道線.md "wikilink")。隔年改行西側線，尖峰訖於海灣園道車站。

1994年M線在BMT第四大道線的區間改為慢車，則為快車。

隔年五月，[曼哈頓橋北邊軌道維修](../Page/曼哈頓橋.md "wikilink")，為讓線列車行駛蒙塔格街隧道，M線中午列車訖站改回欽伯斯街車站。

1999年4月30日起[威廉斯堡橋關閉](../Page/威廉斯堡橋.md "wikilink")，將M線一分為二，直到9月1日才再度開放。此期間北半部全天自大主教車站駛至瑪西大道車站，南半部只於尖峰開行自伯斯街車站往海灣園道車站的列車。

2001年7月23日至2004年2月20日期間[曼哈頓橋再度開放](../Page/曼哈頓橋.md "wikilink")，M線增駛中午及晚上到10點的列車，訖於第九大道車站；尖峰列車則訖於海灣園道車站。

2001年[911事件後幾天](../Page/911事件.md "wikilink")，M線改為區間線型態駛至麥朵大道車站。9月17日N線停駛，M線改為全天經[BMT海灘線駛至斯提威爾大道車站](../Page/BMT海灘線.md "wikilink")。10月28日兩線恢復正常行駛。

2006年8月底，M線改行駛[BMT海灘線](../Page/BMT海灘線.md "wikilink")。

2010年6月28日，M線不再行駛原先行經的BMT納蘇街線，改行駛IND第六大道線。路線標誌色因此從褐色改為橙色。

## 路線

### 服務形式

以下表格顯示M線所使用路線，特定時段在有陰影的格的路段內營運：\[3\]

<table>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>起點</p></th>
<th><p>終點</p></th>
<th><p>軌道</p></th>
<th><p>時段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>平日</p></td>
<td><p>繁忙時段</p></td>
<td><p>晚上</p></td>
<td><p>週末</p></td>
<td><p>深夜</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/IND皇后林蔭路線.md" title="wikilink">IND皇后林蔭路線</a></p></td>
<td><p><a href="../Page/森林小丘-71大道車站_(IND皇后林蔭路線).md" title="wikilink">森林小丘-71大道</a></p></td>
<td><p><a href="../Page/皇后廣場車站_(IND皇后林蔭路線).md" title="wikilink">皇后廣場</a></p></td>
<td><p>慢車</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法庭廣場-23街車站_(IND皇后林蔭路線).md" title="wikilink">法庭廣場-23街</a></p></td>
<td><p><a href="../Page/第五大道／53街車站_(IND皇后林蔭路線).md" title="wikilink">第五大道／53街</a></p></td>
<td><p>全部</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a></p></td>
<td><p><a href="../Page/47-50街-洛克斐勒中心車站_(IND第六大道線).md" title="wikilink">47-50街-洛克斐勒中心</a></p></td>
<td><p><a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a></p></td>
<td><p>慢車</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第二大道車站_(IND第六大道線).md" title="wikilink">第二大道</a></p></td>
<td><p>中央</p></td>
<td><p> </p></td>
<td><p>有限度服務</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>全部</p></td>
<td><p> </p></td>
<td><p>大部分列車</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a></p></td>
<td><p><a href="../Page/布利車站_(BMT納蘇街線).md" title="wikilink">布利</a></p></td>
<td><p><a href="../Page/錢伯斯街車站_(BMT納蘇街線).md" title="wikilink">錢伯斯街</a></p></td>
<td><p>慢車</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埃塞克斯街車站_(BMT納蘇街線).md" title="wikilink">埃塞克斯街</a></p></td>
<td><p>慢車</p></td>
<td><p> </p></td>
<td><p>大部分列車</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威廉斯堡大橋.md" title="wikilink">威廉斯堡大橋</a></p></td>
<td><p>全部</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BMT牙買加線.md" title="wikilink">BMT牙買加線</a></p></td>
<td><p><a href="../Page/馬爾西大道車站_(BMT牙買加線).md" title="wikilink">馬爾西大道</a></p></td>
<td><p><a href="../Page/百老匯交匯車站_(BMT牙買加線).md" title="wikilink">百老匯交匯</a></p></td>
<td><p>慢車</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BMT默特爾大道線.md" title="wikilink">BMT默特爾大道線</a></p></td>
<td><p><a href="../Page/默特爾-威克奧夫大道車站_(BMT默特爾大道線).md" title="wikilink">默特爾-威克奧夫大道</a></p></td>
<td><p><a href="../Page/中村-大都會大道車站_(BMT默特爾大道線).md" title="wikilink">中村-大都會大道</a></p></td>
<td><p>全部</p></td>
<td><p>只限接駁線</p></td>
</tr>
</tbody>
</table>

### 車站

更詳細的車站列表參見上方列出的路線。

主路段沿以下路線營運：

<table style="width:100%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-M.svg" title="fig:NYCS-bull-trans-M.svg">NYCS-bull-trans-M.svg</a><br />
</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-M.svg" title="fig:NYCS-bull-trans-M.svg">NYCS-bull-trans-M.svg</a><br />
</p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/皇后區.md" title="wikilink">皇后區</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND皇后林蔭路線.md" title="wikilink">皇后林蔭路線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/森林小丘-71大道車站_(IND皇后林蔭路線).md" title="wikilink">森林小丘-71大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/67大道車站_(IND皇后林蔭路線).md" title="wikilink">67大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/63徑-雷哥公園車站_(IND皇后林蔭路線).md" title="wikilink">63徑-雷哥公園</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>巴士往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/伍德哈文林蔭路車站_(IND皇后林蔭路線).md" title="wikilink">伍德哈文林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/格蘭大道-新城車站_(IND皇后林蔭路線).md" title="wikilink">格蘭大道-新城</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/埃姆赫斯特大道車站_(IND皇后林蔭路線).md" title="wikilink">埃姆赫斯特大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/傑克遜高地-羅斯福大道車站_(IND皇后林蔭路線).md" title="wikilink">傑克遜高地-羅斯福大道</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）</p></td>
<td><p>線巴士往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a>（只限）<br />
<br />
往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/65街車站_(IND皇后林蔭路線).md" title="wikilink">65街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/北方林蔭路車站_(IND皇后林蔭路線).md" title="wikilink">北方林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/46街車站_(IND皇后林蔭路線).md" title="wikilink">46街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/斯坦威街車站_(IND皇后林蔭路線).md" title="wikilink">斯坦威街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/36街車站_(IND皇后林蔭路線).md" title="wikilink">36街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/皇后廣場車站_(IND皇后林蔭路線).md" title="wikilink">皇后廣場</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/法庭廣場-23街車站_(IND皇后林蔭路線).md" title="wikilink">法庭廣場-23街</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IND跨城線.md" title="wikilink">IND跨城線</a>）<br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/萊辛頓大道-53街車站_(IND皇后林蔭路線).md" title="wikilink">萊辛頓大道-53街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/51街車站_(IRT萊辛頓大道線).md" title="wikilink">51街</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/第五大道／53街車站_(IND皇后林蔭路線).md" title="wikilink">第五大道／53街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND第六大道線.md" title="wikilink">第六大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/47-50街-洛克斐勒中心車站_(IND第六大道線).md" title="wikilink">47-50街-洛克斐勒中心</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/42街-布萊恩特公園車站_(IND第六大道線).md" title="wikilink">42街-布萊恩特公園</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>，<a href="../Page/第五大道車站_(IRT法拉盛線).md" title="wikilink">第五大道</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/34街-先驅廣場車站_(IND第六大道線).md" title="wikilink">34街-先驅廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）</p></td>
<td><p><br />
<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/33街車站_(PATH).md" title="wikilink">33街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/23街車站_(IND第六大道線).md" title="wikilink">23街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/23街車站_(PATH).md" title="wikilink">23街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/14街車站_(IND第六大道線).md" title="wikilink">14街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>，<a href="../Page/14街車站_(IRT百老匯-第七大道線).md" title="wikilink">14街</a>）<br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>，<a href="../Page/第六大道車站_(BMT卡納西線).md" title="wikilink">第六大道</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/14街車站_(PATH).md" title="wikilink">14街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/西四街-華盛頓廣場車站_(IND第六大道線).md" title="wikilink">西四街-華盛頓廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/第9街車站_(PATH).md" title="wikilink">第9街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/布利克街車站_(IRT萊辛頓大道線).md" title="wikilink">布利克街</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>往<strong>百老匯交匯</strong>及<strong>第二大道</strong>列車分岔</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND第六大道線.md" title="wikilink">第六大道線</a></strong>（僅繁忙時段有限度服務）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>align=center </p></td>
<td></td>
<td><p><a href="../Page/第二大道車站_(IND第六大道線).md" title="wikilink">第二大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a></strong>（僅平日深夜）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p> ↑</p></td>
<td><p>align=center </p></td>
<td><p><a href="../Page/錢伯斯街車站_(BMT納蘇街線).md" title="wikilink">錢伯斯街</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
 （<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/布魯克林橋-市政府車站_(IRT萊辛頓大道線).md" title="wikilink">布魯克林橋-市政府</a>）</p></td>
<td><p>車站只有以此站為總站的平日深夜列車停靠</p></td>
</tr>
<tr class="odd">
<td><p> ↑</p></td>
<td><p>align=center </p></td>
<td><p><a href="../Page/堅尼街車站_(BMT納蘇街線).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）</p></td>
<td><p>車站只有往錢伯斯街方向的平日深夜列車停靠</p></td>
</tr>
<tr class="even">
<td><p> ↑</p></td>
<td><p>align=center </p></td>
<td><p><a href="../Page/布利車站_(BMT納蘇街線).md" title="wikilink">布利</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>車站只有往錢伯斯街方向的平日深夜列車停靠</p></td>
</tr>
<tr class="odd">
<td><p>由<strong>71大道</strong>及<strong>錢伯斯街</strong>開出的列車匯合</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>納蘇街線</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>align=center </p></td>
<td><p><a href="../Page/埃塞克斯街車站_(BMT納蘇街線).md" title="wikilink">埃塞克斯街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>）</p></td>
<td><p>週末列車的順時針總站</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT牙買加線.md" title="wikilink">牙買加線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>align=center rowspan = 10 </p></td>
<td><p><a href="../Page/馬爾西大道車站_(BMT牙買加線).md" title="wikilink">馬爾西大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/休斯街車站_(BMT牙買加線).md" title="wikilink">休斯街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/羅里默街車站_(BMT牙買加線).md" title="wikilink">羅里默街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/法拉盛大道車站_(BMT牙買加線).md" title="wikilink">法拉盛大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>巴士往<a href="../Page/約翰·甘迺迪國際機場.md" title="wikilink">JFK國際機場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/默特爾大道車站_(BMT牙買加線).md" title="wikilink">默特爾大道-百老匯</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>M線接駁巴士往<a href="../Page/默特爾-威克奧夫大道車站_(BMT默特爾大道線).md" title="wikilink">默特爾-威克奧夫大道的總站</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/科斯丘什科街車站_(BMT牙買加線).md" title="wikilink">科斯丘什科街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/蓋茲大道車站_(BMT牙買加線).md" title="wikilink">蓋茲大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/哈爾西街車站_(BMT牙買加線).md" title="wikilink">哈爾西街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/昌西街車站_(BMT牙買加線).md" title="wikilink">昌西街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/百老匯交匯車站_(BMT牙買加線).md" title="wikilink">百老匯交匯</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND福爾頓街線.md" title="wikilink">IND福爾頓街線</a>）<br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）</p></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
<td></td>
</tr>
</tbody>
</table>

M線接駁巴士沿默特爾大道由百老匯開往威克奧夫大道，而M線接駁列車由威克奧夫大道開往大都會大道，兩條軌道各有一列列車來回運行。以下列表顯示接駁巴士和接駁列車停靠的車站：\[4\]

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-M.svg" title="fig:NYCS-bull-trans-M.svg">NYCS-bull-trans-M.svg</a></p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-M.svg" title="fig:NYCS-bull-trans-M.svg">NYCS-bull-trans-M.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>路線/備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/BMT默特爾大道線.md" title="wikilink">BMT默特爾大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>align=center rowspan = 3 </p></td>
<td></td>
<td><p><a href="../Page/默特爾大道車站_(BMT牙買加線).md" title="wikilink">默特爾大道-百老匯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/中央大道車站_(BMT默特爾大道線).md" title="wikilink">中央大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/尼克柏克大道車站_(BMT默特爾大道線).md" title="wikilink">尼克柏克大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/默特爾-威克奧夫大道車站_(BMT默特爾大道線).md" title="wikilink">默特爾-威克奧夫大道</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/皇后區.md" title="wikilink">皇后區</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>align=center rowspan = 4 </p></td>
<td><p><a href="../Page/塞尼卡大道車站_(BMT默特爾大道線).md" title="wikilink">塞尼卡大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/森林大道車站_(BMT默特爾大道線).md" title="wikilink">森林大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/鮮潭路車站_(BMT默特爾大道線).md" title="wikilink">鮮潭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/中村-大都會大道車站_(BMT默特爾大道線).md" title="wikilink">中村-大都會大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

## 參考文獻

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20061206061413/http://mta.info/nyct/service/mline.htm)
  - [M線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    M線時刻表（PDF）](https://web.archive.org/web/20081221064845/http://www.mta.info/nyct/service/pdf/tmcur.pdf)

[M](../Category/紐約地鐵服務路線.md "wikilink")

1.
2.  橋街-傑里街車站已於1969年10月4日廢止
3.
4.