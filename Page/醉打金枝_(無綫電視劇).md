**[醉打金枝](../Page/醉打金枝.md "wikilink")**（**Taming of the
Princess**）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司於](../Page/電視廣播有限公司.md "wikilink")1996年製作、1997年[農曆新年首播的古裝賀歲喜劇](../Page/農曆新年.md "wikilink")，由[歐陽震華](../Page/歐陽震華.md "wikilink")、[關詠荷](../Page/關詠荷.md "wikilink")、[魏駿傑](../Page/魏駿傑.md "wikilink")、[傅明憲主演](../Page/傅明憲.md "wikilink")。1月27日至2月21日逢星期一至五晚上7時30分至8時30分於[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台首播](../Page/翡翠台.md "wikilink")，2月20日因電視台製作「[鄧小平逝世特輯](../Page/鄧小平.md "wikilink")」而停播一天，大結局被安排在隨後的星期六晚上播出。曾經在1998年及2000年農曆新年期間重播。

劇集內容輕鬆惹笑，適合一家大細收看，收視率一直維持在85%以上，結局篇錄得平均34點收視，199萬人次收看；大結局錄得平均31點收視，182萬人次收看。男、女主角歐陽震華和關詠荷憑此劇奪得該年度《[萬千星輝賀台慶](../Page/萬千星輝賀台慶.md "wikilink")》最惹笑冤家獎。

## 劇情簡介

故事從[唐朝](../Page/唐朝.md "wikilink")[代宗年間說起](../Page/唐代宗.md "wikilink")。劇裡的主角[昇平公主](../Page/齊國昭懿公主.md "wikilink")
( [關詠荷飾](../Page/關詠荷.md "wikilink") )
出生後不久，困擾朝廷多年的[安史之亂終告結束](../Page/安史之亂.md "wikilink")。所以，她被代宗視為吉祥象徵，寵愛有加。如此寵愛，昇平公主的婚事自然也是代宗最為擔心的事。多年來，昇平一直有一種「皇帝女唔憂嫁」（皇帝的女兒不愁嫁不出）的態度。但是，代宗想昇平早日嫁出。所以，代宗決定向全國招親。最後有四個人可以入宮參與最後一回招親比賽。可是，那四個候選人裡沒有一個是昇平公主喜歡的（一個候選人面有刺青、一個候選人年屆六十，行將入木、另外一個候選人就行為霸道粗魯）。但是，代宗要昇平公主選一個作為駙馬。最後，昇平公主在無路可走的地步下，決定以一場三天三夜的[馬吊](../Page/馬吊.md "wikilink")（麻將）賽來選擇[駙馬](../Page/駙馬.md "wikilink")。

最後，馬吊賽是由自認不懂馬吊的回紇王子阿洛安得勝出。他是四個候選人裡，昇平公主比較喜歡的那位。但是，當昇平公主發現阿洛安得利用[黃金買通其中一個候選人時](../Page/黃金.md "wikilink")，她決定逃婚。在[長樂坊裡](../Page/長樂坊.md "wikilink")，她遇到酒館小開[郭曖](../Page/郭曖.md "wikilink")
( [歐陽震華飾](../Page/歐陽震華.md "wikilink") )，兩人從而由冤家變成情侶，最後更共諧連理，成為夫妻。

但是，宮中規條繁多，人事傾軋，婆媳轇轕等等，都令二人誤會重重。但由於有一天郭曖喝酒打了昇平，使得二人感情生變……但後來夫婦不和竟被佞臣利用，郭險遭抄家，最後被貶為[平民](../Page/平民.md "wikilink")。

## 與歷史不符的部份

### 郭家險被“誅九族”

野史記載，郭曖與昇平公主吵架後，說出了一些不敬言論。當郭子儀把郭曖囚禁起來，等待皇上治罪時，代宗李豫向郭子儀說：「鄙諺有之：『不痴不聾，不作家翁』。兒女子閨房之言，何足聽也！（俗話說『不痴不聾，不作家翁』。夫妻倆在閨房之言，怎能夠當真的？！）」。那個故事也是京劇《醉打金枝》的故事中心。但是，在本劇裡，郭曖說的話被西宮淑妃加鹽加醋，到了代宗耳邊的時候，代宗龍顏大怒，把郭子儀全家「[誅九族](../Page/誅九族.md "wikilink")」。到最後郭家才被平反，免於一死。

### 郭子仪妻

郭子仪妻王氏，[天宝年间初封琅琊县君](../Page/天宝_\(唐朝\).md "wikilink")。进封太原郡君、霍国夫人。王氏生六子八女，另外郭子仪次子[郭旰和四子](../Page/郭旰.md "wikilink")[郭昢非她所生](../Page/郭昢.md "wikilink")。

### 李白一生

[李白的生平在本劇裡全屬虛構](../Page/李白.md "wikilink")。因為[唐代宗](../Page/唐代宗.md "wikilink")[李豫在公元](../Page/李豫.md "wikilink")762年登基為皇，而當年正好是李白病逝那年。所以，李白沒有可能在那時出現。

### 玉真公主

劇中的李白最後與一名玉真公主成婚，當為駙馬。但根據歷史記載，只有比代宗早七十多年執政的[唐睿宗有一個名為](../Page/唐睿宗.md "wikilink")[玉真的公主](../Page/玉真公主.md "wikilink")，而此位公主跟李白一樣，於唐代宗登基當年逝世。

### 馬弔/-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-

劇中角色常會打[-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-](../Page/麻將.md "wikilink")，而-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-在劇中被稱為[馬弔](../Page/馬吊.md "wikilink")。事實上，馬弔與-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-乃兩種不同遊戲。TVB其他古裝劇集，例如後來的《[皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink")》等等，也有以-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-為馬弔這種錯誤。

此外，民間相傳，馬弔發明於宋代，可靠的馬弔文獻記載，更遲至晚明才出現，-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-甚至出現得更晚，故此唐時人根本不可能打馬弔或者-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-。不過，歷史記載唐朝時期民間有玩[葉子戲的習慣](../Page/葉子戲.md "wikilink")，而-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-就是從葉子戲演變出來的遊戲，但葉子戲玩法與-{zh-hans:麻将;zh-hk:麻雀;zh-tw:麻將;}-截然不同。

## 演員表

### 郭家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/梁家仁.md" title="wikilink">梁家仁</a></strong></p></td>
<td><p><strong><a href="../Page/郭子儀.md" title="wikilink">郭子儀</a></strong></p></td>
<td><p>化名<strong><a href="../Page/元宵.md" title="wikilink">元宵</a></strong><br />
將軍<br />
方小玉之夫<br />
郭曖之父<br />
昇平公主之老爺</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/朱咪咪.md" title="wikilink">朱咪咪</a></strong></p></td>
<td><p><strong>方小玉</strong></p></td>
<td><p>郭子儀之妻<br />
郭曖之母<br />
昇平公主之奶奶</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></strong></p></td>
<td><p><strong><a href="../Page/郭曖.md" title="wikilink">郭曖</a></strong></p></td>
<td><p>前名：元曖<br />
郭子儀、方小玉之子<br />
昇平公主之夫，<a href="../Page/駙馬.md" title="wikilink">駙馬</a><br />
<a href="../Page/李白.md" title="wikilink">李白</a>、王大力、卜一卜、華十八好友<br />
長樂坊五虎之一</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李龍基.md" title="wikilink">李龍基</a></p></td>
<td><p>吉　祥</p></td>
<td><p>郭家下人</p></td>
</tr>
</tbody>
</table>

### 皇宮

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關菁.md" title="wikilink">關　菁</a></p></td>
<td><p><a href="../Page/唐代宗.md" title="wikilink">代　宗</a></p></td>
<td><p>李豫<br />
昇平公主、玉真公主、李適、李逸之父</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韓馬利.md" title="wikilink">韓馬利</a></p></td>
<td><p>淑　妃</p></td>
<td><p>唐代宗之妃<br />
李逸、玉真公主之母</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/關詠荷.md" title="wikilink">關詠荷</a></strong></p></td>
<td><p><strong>昇　平</strong></p></td>
<td><p>宮外化名為：<a href="../Page/恭喜.md" title="wikilink">恭喜</a>(第1集)<br />
<a href="../Page/唐代宗.md" title="wikilink">唐代宗及已故皇后之女</a>、大公主<br />
<a href="../Page/郭曖.md" title="wikilink">郭曖之妻</a><br />
<a href="../Page/郭子儀.md" title="wikilink">郭子儀</a>、方小玉之媳</p></td>
</tr>
<tr class="odd">
<td><p><strong>回紇王妃</strong></p></td>
<td><p>於大結局出現，為阿諾安德的妻子<br />
樣貌與昇平一模一樣<br />
不懂漢語</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳彥行.md" title="wikilink">陳彥行</a></strong></p></td>
<td><p><strong>玉　真</strong></p></td>
<td><p>唐代宗、淑妃之女<br />
喜歡<a href="../Page/李白.md" title="wikilink">李白</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧兆尊.md" title="wikilink">鄧兆尊</a></p></td>
<td><p><a href="../Page/李适.md" title="wikilink">李适</a></p></td>
<td><p>李豫及已故皇后之子、太子<br />
昇平公主之弟、李逸及玉真公主之同父異母兄</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫家堯.md" title="wikilink">莫家堯</a></p></td>
<td><p>李　逸</p></td>
<td><p>李豫及淑妃之子、二皇子<br />
昇平公主、李適之同父異母弟<br />
玉真公主之兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安德尊.md" title="wikilink">安德尊</a></p></td>
<td><p>阿諾安德</p></td>
<td><p><a href="../Page/回鶻.md" title="wikilink">回紇王子</a><br />
昇平公主駙馬人選之一</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/薛崇基.md" title="wikilink">薛崇基</a></p></td>
<td><p>張公公</p></td>
<td><p>唐代宗的近身太監</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉家輝.md" title="wikilink">劉家輝</a></p></td>
<td><p>李　健</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a></p></td>
<td><p>凌　坤</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳安瑩.md" title="wikilink">陳安瑩</a></p></td>
<td><p>如　意</p></td>
<td><p>昇平公主貼身侍婢</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張鴻昌.md" title="wikilink">張鴻昌</a></p></td>
<td><p>國　舅</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張英才.md" title="wikilink">張英才</a></p></td>
<td><p>陳丞相</p></td>
<td></td>
</tr>
</tbody>
</table>

### 長樂坊居民

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a></strong></p></td>
<td><p><strong><a href="../Page/李白.md" title="wikilink">李　白</a></strong></p></td>
<td><p>小白<br />
長樂坊五虎之一，飽讀詩書<br />
花雕之夫<br />
郭曖、王大力、卜一卜、華十八好友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/傅明憲.md" title="wikilink">傅明憲</a></strong></p></td>
<td><p><strong>花　雕</strong></p></td>
<td><p>花花<br />
李白之妻</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/羅莽.md" title="wikilink">羅　莽</a></strong></p></td>
<td><p><strong>王大力</strong></p></td>
<td><p>長樂坊五虎之一，力大無窮<br />
郭曖、李白、卜一卜、華十八好友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/李子奇.md" title="wikilink">李子奇</a></strong></p></td>
<td><p><strong>卜一卜</strong></p></td>
<td><p>長樂坊五虎之一，精通術數<br />
郭曖、李白、王大力、華十八好友</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/王偉樑.md" title="wikilink">王偉樑</a></strong></p></td>
<td><p><strong>華十八</strong></p></td>
<td><p>華佗第十八代後人<br />
長樂坊五虎之一，精通醫術<br />
郭曖、李白、王大力、卜一卜好友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/康華.md" title="wikilink">康　華</a></strong></p></td>
<td><p><strong>綠　翹</strong></p></td>
<td><p>長樂坊居民<br />
妓女<br />
暗戀郭曖</p></td>
</tr>
</tbody>
</table>

### 其他演員

|                                         |         |           |
| --------------------------------------- | ------- | --------- |
| **演員**                                  | **角色**  | **關係/暱稱** |
| [羅　蘭](../Page/羅蘭.md "wikilink")         | 王大媽     |           |
| [余慕蓮](../Page/余慕蓮.md "wikilink")        | 王細媽     |           |
| [劉桂芳](../Page/劉桂芳.md "wikilink")        | 王大嫂     |           |
| [蘇恩磁](../Page/蘇恩磁.md "wikilink")        | 王二嫂     |           |
| [林嘉麗](../Page/林嘉麗.md "wikilink")        | 王三嫂     |           |
| [溫雙燕](../Page/溫雙燕.md "wikilink")        | 風四娘     |           |
| [葉振聲](../Page/葉振聲.md "wikilink")        | 如　虎     |           |
| [游　飈](../Page/游飈.md "wikilink")         | 添　翼     |           |
| \-{[黃鳳瓊](../Page/黃鳳瓊.md "wikilink")}-   | 良　辰     |           |
| [陳楚翹](../Page/陳楚翹.md "wikilink")        | 美　景     |           |
| [鍾潔怡](../Page/鍾潔怡.md "wikilink")        | 花　好     |           |
| [陸婉芬](../Page/陸婉芬.md "wikilink")        | 月　圓     |           |
| [華忠男](../Page/華忠男.md "wikilink")        | 招　財     |           |
| [姚傑龍](../Page/姚傑龍.md "wikilink")        | 進　寶     |           |
| [黃成想](../Page/黃成想.md "wikilink")        | 王一成     |           |
| [陳狄克](../Page/陳狄克.md "wikilink")        | 陳大利     |           |
| [焦　雄](../Page/焦雄.md "wikilink")         | 德　叔     |           |
| [方　傑](../Page/方傑_\(演員\).md "wikilink")  | 賈　珍     |           |
| [俞　明](../Page/俞明.md "wikilink")         | 錢可通     |           |
| [梁少狄](../Page/梁少狄.md "wikilink")        | 大哥雄     |           |
| [盧卓南](../Page/盧卓南.md "wikilink")        | \-      | 大哥雄手下     |
| [張俊華](../Page/張俊華.md "wikilink")        | \-      |           |
| [羅雪玲](../Page/羅雪玲.md "wikilink")        | 珠       |           |
| [何美好](../Page/何美好.md "wikilink")        | 苑       |           |
| [馬蹄露](../Page/馬蹄露_\(藝人\).md "wikilink") | 潤       |           |
| [郭德信](../Page/郭德信.md "wikilink")        | 羅　石     |           |
| [梁秀玲](../Page/梁秀玲.md "wikilink")        | \-      | 婦人        |
| [曾玉娟](../Page/曾玉娟.md "wikilink")        | \-      |           |
| [梁慶騷](../Page/梁慶騷.md "wikilink")        | 陳　七     |           |
| [麥少華](../Page/麥少華.md "wikilink")        | 苦　力     |           |
| [陳向瑩](../Page/陳向瑩.md "wikilink")        | 方小玉(年青) |           |
| [黃文標](../Page/黃文標.md "wikilink")        | 巫　仁     |           |
| [邱萬城](../Page/邱萬城.md "wikilink")        | 巫　良     |           |
| [曹　濟](../Page/曹濟.md "wikilink")         | 骨　叔     |           |
| [梁欽棋](../Page/梁欽棋.md "wikilink")        | 郭　暉     |           |
| [梁建平](../Page/梁建平.md "wikilink")        | 郭　昕     |           |
| [關可維](../Page/關可維.md "wikilink")        | 常　開     |           |
| [陳勉良](../Page/陳勉良.md "wikilink")        | 冬　叔     |           |
| [張漢斌](../Page/張漢斌.md "wikilink")        | 哈       |           |
| [卓　凡](../Page/卓凡.md "wikilink")         | 蜜       |           |
| [羅國維](../Page/羅國維.md "wikilink")        | 朱學士     |           |
| [古明華](../Page/古明華.md "wikilink")        | 宋學士     |           |
| [魏惠文](../Page/魏惠文.md "wikilink")        | 方學士     |           |
| [伍慧珊](../Page/伍慧珊.md "wikilink")        | 歡　歡     |           |
| [湯盈盈](../Page/湯盈盈.md "wikilink")        | 欣　欣     |           |
| [王維德](../Page/王維德.md "wikilink")        | 狀　元     |           |
| [黃煒林](../Page/黃煒林.md "wikilink")        | 榜　眼     |           |
| [文嘉永](../Page/文嘉永.md "wikilink")        | 探　花     |           |
| [黃天鐸](../Page/黃天鐸.md "wikilink")        | 琉球使者    |           |
| [蔣　克](../Page/蔣克.md "wikilink")         | 使者隨從    |           |
| [湯俊明](../Page/湯俊明.md "wikilink")        |         |           |
| [陳燕航](../Page/陳燕航.md "wikilink")        | 承　恩     | 三公主       |
| [劉美珊](../Page/劉美珊.md "wikilink")        | 端　桂     |           |
| [何嘉麗](../Page/何嘉麗_\(演員\).md "wikilink") | 昭　陽     |           |
| [何璧堅](../Page/何璧堅.md "wikilink")        | 大　臣     |           |
| [李煌生](../Page/李煌生.md "wikilink")        | 二駙馬     |           |
| [麥嘉倫](../Page/麥嘉倫.md "wikilink")        | 三駙馬     |           |
| [羅君左](../Page/羅君左.md "wikilink")        | 四駙馬     |           |
| [劉永晉](../Page/劉永晉.md "wikilink")        | 勇       |           |
| [鄧煜榮](../Page/鄧煜榮.md "wikilink")        | 古　叔     |           |
| [龍志成](../Page/龍志成.md "wikilink")        | 吉　叔     |           |
| [劉勁松](../Page/劉勁松.md "wikilink")        | 士兵      |           |
| [李志偉](../Page/李志偉.md "wikilink")        |         |           |
| [沈星銘](../Page/沈星銘.md "wikilink")        |         |           |
| [郭　淨](../Page/郭淨.md "wikilink")         |         |           |
| [張智軒](../Page/張智軒.md "wikilink")        |         |           |
| [朱樂輝](../Page/朱樂輝.md "wikilink")        | 侍　衛     |           |
| [羅立勤](../Page/羅立勤.md "wikilink")        | 太　監     |           |
| [黎秀英](../Page/黎秀英.md "wikilink")        | 月　娥     |           |
| [譚一清](../Page/譚一清.md "wikilink")        | 御　醫     |           |
| [張松枝](../Page/張松枝.md "wikilink")        | 卓大人     |           |
| [雷穎怡](../Page/雷穎怡.md "wikilink")        | 飄　紅     |           |
| [邵卓堯](../Page/邵卓堯.md "wikilink")        | 大　臣     |           |
| [石　-{云}-](../Page/石云.md "wikilink")     | 張　虎     |           |
| [簡瑞超](../Page/簡瑞超.md "wikilink")        | 趙　龍     |           |
| [溫裕紅](../Page/溫裕紅.md "wikilink")        | 昕　妻     |           |
| [王啟德](../Page/王啟德.md "wikilink")        | 郭子儀之子   |           |
| [彭國樑](../Page/彭國樑.md "wikilink")        |         |           |
| [甘子正](../Page/甘子正.md "wikilink")        |         |           |
| [劉倫寶](../Page/劉倫寶.md "wikilink")        | 郭子儀之媳   |           |
| [伍思鈴](../Page/伍思鈴.md "wikilink")        |         |           |
| [汪曉文](../Page/汪曉文.md "wikilink")        |         |           |
| [溫文英](../Page/溫文英.md "wikilink")        | 將　領     |           |
| [招石文](../Page/招石文.md "wikilink")        | 回紇國師    |           |
| [鍾志光](../Page/鍾志光.md "wikilink")        | 蹴鞠比賽旁述  |           |

## 獎項

1997年《[萬千星輝賀台慶](../Page/萬千星輝賀台慶.md "wikilink")》「最惹笑冤家獎」：[歐陽震華](../Page/歐陽震華.md "wikilink")、[關詠荷](../Page/關詠荷.md "wikilink")

## 電視節目的變遷

### 首播

### 重播

## 参考资料

[Category:1997年無綫電視劇集](../Category/1997年無綫電視劇集.md "wikilink")
[Category:無綫電視唐朝背景劇集](../Category/無綫電視唐朝背景劇集.md "wikilink")
[Category:公主主角題材連續劇](../Category/公主主角題材連續劇.md "wikilink")
[Category:郭子仪家族](../Category/郭子仪家族.md "wikilink")
[Category:無綫電視賀歲劇集](../Category/無綫電視賀歲劇集.md "wikilink")