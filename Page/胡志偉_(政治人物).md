**胡志偉**（，），[香港出生](../Page/香港.md "wikilink")，現任[香港](../Page/香港.md "wikilink")[黃大仙區議會](../Page/黃大仙區議會.md "wikilink")[瓊富選區議員](../Page/瓊富.md "wikilink")、[香港](../Page/香港.md "wikilink")[九龍東選區](../Page/九龍東選區.md "wikilink")[立法會議員及](../Page/立法會議員.md "wikilink")[香港民主黨主席](../Page/香港民主黨.md "wikilink")。[皇仁書院畢業](../Page/皇仁書院.md "wikilink")，[美國](../Page/美國.md "wikilink")[威斯康辛大學密爾沃基分挍經濟學碩士](../Page/威斯康辛大學密爾沃基分挍.md "wikilink")。

## 簡歷

在香港土生土長的胡志偉，出身於草根階層，曾在皇仁書院就讀，同窗包括前任[律政司司長](../Page/律政司司長.md "wikilink")[黃仁龍](../Page/黃仁龍.md "wikilink")。1981年[中學會考後](../Page/中學會考.md "wikilink")，胡志偉便投身於社會工作。1985年，他決定重拾書本，前往[澳門修讀](../Page/澳門.md "wikilink")[副學士學位](../Page/副學士.md "wikilink")，並於1987年負笈美國威斯康辛大學密爾沃基分校繼續學業，主修經濟，副修數學。1991年，完成碩士課程後，他放棄原本計劃修讀的[博士課程](../Page/博士.md "wikilink")，返回香港，擔任[立法局議員](../Page/立法局.md "wikilink")[林鉅成的議員助理](../Page/林鉅成.md "wikilink")。

[1994年首次投身於選舉](../Page/1994年香港區議會選舉.md "wikilink")，在[黃大仙上邨參選](../Page/黃大仙上邨.md "wikilink")[區議會但落敗](../Page/區議會.md "wikilink")，後來於1995年當選[市政局議員](../Page/市政局.md "wikilink")。[1999年在黃大仙瓊富選區參選區議員並當選晉身區議會](../Page/1999年香港區議會選舉.md "wikilink")，[2003年香港區議會選舉以](../Page/2003年香港區議會選舉.md "wikilink")4,480票連任，所得票數為全港第二，僅次於[葵芳選區的](../Page/葵芳_\(選區\).md "wikilink")[梁耀忠](../Page/梁耀忠.md "wikilink")\[1\]，[2007年香港區議會選舉以](../Page/2007年香港區議會選舉.md "wikilink")4,370票連任，所得票數為全港最高，傳媒稱之為當屆「票王」\[2\]\[3\]。[2011年香港區議會選舉以](../Page/2011年香港區議會選舉.md "wikilink")4,300票連任\[4\]。[2015年香港區議會選舉](../Page/2015年香港區議會選舉.md "wikilink")，胡志偉再以3,907票第四度連任黃大仙區區議員\[5\]。

[1998年香港立法會選舉](../Page/1998年香港立法會選舉.md "wikilink")，循[體育、演藝、文化及出版界功能界別出選落敗](../Page/體育、演藝、文化及出版界功能界別.md "wikilink")。[2000年立法會選舉參與](../Page/2000年香港立法會選舉.md "wikilink")[九龍東地區直選](../Page/九龍東.md "wikilink")，排在以[司徒華](../Page/司徒華.md "wikilink")、[李華明為首的](../Page/李華明.md "wikilink")[民主黨參選名單第三位](../Page/民主黨.md "wikilink")；[2004年司徒華宣佈不再參選](../Page/2004年香港立法會選舉.md "wikilink")，胡志偉排在民主黨參選名單第二位，再次夥拍李華明出選九龍東，但未能當選。[2008年以民主黨名義](../Page/2008年香港立法會選舉.md "wikilink")，在九龍東以個人名單參選，同樣未能當選。

2011年，胡志偉於[香港城市大學完成仲裁及爭議解決學課程](../Page/香港城市大學.md "wikilink")，取得深造文憑\[6\]。

[2012年香港立法會選舉](../Page/2012年香港立法會選舉.md "wikilink")，代表民主黨於九龍東選區勝出。

[2016年香港立法會選舉](../Page/2016年香港立法會選舉.md "wikilink")，再度代表[民主黨出選](../Page/民主黨_\(香港\).md "wikilink")[九龍東選區](../Page/九龍東選區.md "wikilink")，最後取得該區票數排名第二的50,309票，成功連任。\[7\]

2016年12月民主黨領導層改選，胡志偉以92%信任票接替劉慧卿，當選為民主黨新主席。\[8\]

2017年8月11日，民主黨召開緊急記者會，講述黨員林子健於8月10日被懷疑強力部門擄走禁錮，[林卓廷](../Page/林卓廷.md "wikilink")、[何俊仁](../Page/何俊仁.md "wikilink")、[李柱銘及](../Page/李柱銘.md "wikilink")[李永達等人高調參與記者會](../Page/李永達.md "wikilink")，協助及發佈黨友[林子健了聲稱被擄事件](../Page/林子健.md "wikilink")，胡志偉並無出席。次日，胡志偉在香港電台促請警方盡快查明林子健事件\[9\]。8月14
日，《傳真社》發放及公開旺角砵蘭街商戶的閉路電視片段，發現林子健的講法矛盾，及後，林子健於深夜因涉嫌誤導警務人員被拘捕。\[10\]在8月15日下午12:30分，民主黨再次召開記者招待會，唯只有胡志偉一人會見傳媒，[林卓廷](../Page/林卓廷.md "wikilink")、[何俊仁](../Page/何俊仁.md "wikilink")、[李柱銘及](../Page/李柱銘.md "wikilink")[李永達等人並無出現](../Page/李永達.md "wikilink")，胡指由於事件正在調查，故民主黨暫不宜作進一步評論，亦拒絕就事件道歉\[11\]。

## 公職

  - [立法會議員](../Page/香港立法會.md "wikilink")（2012年-）
  - [黃大仙區議員](../Page/黃大仙區議會.md "wikilink")（2000年-）
  - 市政局議員（1995-1999年）
  - 中央政策組非全職顧問（2009年）
  - 氣體安全諮詢委員會委員（2009-2011年）
  - 大嶼山發展諮詢委員會委員（2014-）
  - [市區重建局非執行董事](../Page/市區重建局.md "wikilink")（2014-）

## 社會服務

  - [藝穗會董事](../Page/藝穗會.md "wikilink")（1997年-）
  - 富山居民協會主席
  - 分區委員會
  - 黃大仙上村及鳳凰新村老人服務中心督導主任

## 争议

### 大和解​論

2017年4月17日胡志偉對記者表示，希望候任行政長官[林鄭月娥運用](../Page/林鄭月娥.md "wikilink")《[香港特別行政區基本法](../Page/中華人民共和國香港特別行政區基本法.md "wikilink")》第48條的權力，[特赦](../Page/特赦.md "wikilink")[雨傘運動中所有被檢控的人士](../Page/雨傘運動.md "wikilink")，包括所有參與者以及[七警](../Page/七警.md "wikilink")、[朱經緯等](../Page/朱經緯.md "wikilink")；建議香港政府成立獨立調查委員會，就"雨傘運動"事件的本質作詳細分析及背景研究並深切檢討，避免往後施政時重複犯錯，通過“政治手段解決政治問題”，以修補社會撕裂。該言論在香港社會引發巨大反響，[建制派和](../Page/建制派.md "wikilink")[泛民主派人士均反駁了胡志偉的言論](../Page/泛民主派.md "wikilink")。
[民建聯主席](../Page/民主建港協進聯盟.md "wikilink")[李慧瓊認為認為特赦會損害](../Page/李慧瓊.md "wikilink")[法治精神](../Page/法治.md "wikilink")；[經民聯主席](../Page/經民聯.md "wikilink")[盧偉國認為特赦的做法雖然可以舒緩社會撕裂氣氛](../Page/盧偉國.md "wikilink")，令社會和諧，但會違反香港秉持的法治精神；而[田北辰認為胡志偉的建議是](../Page/田北辰.md "wikilink")“把玩政治”，會“教壞下一代”。泛民方面，部分[民主黨黨員告訴記者胡在向傳媒提出建議前並未知會民主黨立法會黨團](../Page/民主黨_\(香港\).md "wikilink")，批評其說法把立場當成貨幣交易，破壞民主運動和法治原則；民主黨中央委員[許智峯認為社會撕裂的源頭並不是佔中](../Page/許智峯.md "wikilink")，而是民間對政府的不信任，大和解與法治理念背道而馳；[香港眾志主席](../Page/香港眾志.md "wikilink")[羅冠聰認為該建議不實際](../Page/羅冠聰.md "wikilink")，在政改之間便做出和解是不負責任，而特赦違法的警察可能會造成更大的撕裂\[12\]。4月18日胡志偉表示正式收回有關言論，並對特赦論表示深切道歉，承認發表有關言論前未經黨內深思熟慮討論，亦未有與其他民主派政黨交換意見\[13\]\[14\]。

## 參考

<references/>

## 外部連結

  - [胡志偉Facebook專頁](https://www.facebook.com/chiwaioffice/)
  - [胡志偉社區博客](http://wuchiwai.wordpress.com/)

[category:黃大仙區議員](../Page/category:黃大仙區議員.md "wikilink")
[category:香港民主黨成員](../Page/category:香港民主黨成員.md "wikilink")
[category:潮安人](../Page/category:潮安人.md "wikilink")

[Category:威斯康辛大學密爾瓦基分校校友](../Category/威斯康辛大學密爾瓦基分校校友.md "wikilink")
[Category:前市政局議員](../Category/前市政局議員.md "wikilink")
[Category:香港立法會議員](../Category/香港立法會議員.md "wikilink")
[Category:香港泛民主派人士](../Category/香港泛民主派人士.md "wikilink")

1.
2.  <http://specials.mingpao.com/cfm/News.cfm?SpecialsID=131&Page=2&Ne>
3.
4.  <http://www.elections.gov.hk/dc2011/chi/results_wong_tai_sin.html>
5.  <http://www.elections.gov.hk/dc2015/chi/results_wong_tai_sin.html?1492574084667>
6.   香港城市大學
7.  [2016年立法會選舉-
    九龍東選舉結果](http://www.elections.gov.hk/legco2016/chi/rs_gc_LC3.html?1473089116779)
8.  [胡志偉以92%信任票當選民主黨新主席](http://hk.apple.nextmedia.com/realtime/news/20161204/560038581)，蘋果日報，2016-12-04
9.  [胡志偉促請警方盡快查明林子健事件，香港電台新聞網站，2017年8月12日。](http://news.rthk.hk/rthk/ch/component/k2/1347787-20170812.htm?nsukey=QWiMGLdIpYGk7jMKleqUOnaH7oOE0DUiOyNlscjdZcoNJ2iPF%2B6Uxn%2BX9fIpcWD%2FJzJauIBI2jJrGuebzkqvXKqLVYUqFbzB8%2B3507SFA%2Bi8F13%2FMUmcucHfcSm1BJptJljdg5AMOBZA%2FfLXVNyBXEKCtFTxO0p2UwJkWHfLPZwHjrcbgPEEiYAn10XFDj%2B6)
10. [《重組閉路電視的調查
    是一步一步走出來》，香港《傳真社》，2017年8月15日](https://www.factwire.org/single-post/2017/08/15/%E9%87%8D%E7%B5%84%E9%96%89%E8%B7%AF%E9%9B%BB%E8%A6%96%E7%9A%84%E8%AA%BF%E6%9F%A5-%E6%98%AF%E4%B8%80%E6%AD%A5%E4%B8%80%E6%AD%A5%E8%B5%B0%E5%87%BA%E4%BE%86)
11. [林子健案：卸責李柱銘開記招
    胡志偉拒道歉](http://hk.on.cc/hk/bkn/cnt/news/20170815/bkn-20170815111514077-0815_00822_001.html?nsukey=uO0UA1V2CRKwMR4FY3c8hjlUyugMEczD4CzlJMHjYIzoYO0B0WRZTGrpHcjGTMmwOXAxG%2BlsJARpq0rfuVdpWRwKW%2FBHECaQm55Tl6ICBnqPnX2KokJyScDoYPHJw%2FBPcGQ3Bd7SwJwGfJpOTnXxSB7no3DnbYoiyWnVK7vFx33VijPTidRgSUSBcqMDsHFV)，東方報業集團網站，2017年8月15日。
12.
13.
14. [東方日報A1：全城怒轟胡志偉踐踏法治](http://hk.on.cc/hk/bkn/cnt/news/20170419/bkn-20170419033014729-0419_00822_001.html?eventid=4028828d47d3fe7e01482b1c75912433&eventsection=hk_news)，東方報業集團網站，2017-04-19