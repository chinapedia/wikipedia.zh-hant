[Jeongbang_Waterfall.jpg](https://zh.wikipedia.org/wiki/File:Jeongbang_Waterfall.jpg "fig:Jeongbang_Waterfall.jpg")\]\]
[Korea-Jejudo-Coast-03.jpg](https://zh.wikipedia.org/wiki/File:Korea-Jejudo-Coast-03.jpg "fig:Korea-Jejudo-Coast-03.jpg")
**西歸浦市**（）是一個位於[韓國](../Page/韓國.md "wikilink")[濟州特別自治道南部](../Page/濟州特別自治道.md "wikilink")、韓國最南端的城市。

## 地理

西歸浦市位於[濟州島的南部](../Page/濟州島.md "wikilink")，北面與[濟州市及](../Page/濟州市.md "wikilink")[漢拏山接壤](../Page/漢拏山.md "wikilink")，南面面向[朝鮮海峽](../Page/朝鮮海峽.md "wikilink")。

### 屬下島嶼

  - [加波島](../Page/加波島.md "wikilink")
  - [馬羅島](../Page/馬羅島.md "wikilink")
  - [離於島](../Page/離於島.md "wikilink") - 雖然主張擁有其主權，但不為中國政府所承認。

## 氣候

年平均氣溫為15.7℃，為韓國國內擁有最暖氣候的地方。此外，西歸浦市也是韓國降雨量最高的地方。

## 歷史

  - 1946年，道制開始在濟州島實施，[南濟州郡西歸浦面及中文面成立](../Page/南濟州郡.md "wikilink")
  - 1956年，西歸浦面獲升格為[邑](../Page/邑_\(韓國\).md "wikilink")
  - 1981年7月，西歸浦邑與中文面合併，並成為西歸浦市
  - 2006年7月，整合南濟州郡

## 行政

西歸浦市合共由3邑、2面、12洞組成。

  - [大靜邑](../Page/西歸浦市大靜邑.md "wikilink")
  - [南元邑](../Page/西歸浦市南元邑.md "wikilink")
  - [城山邑](../Page/西歸浦市城山邑.md "wikilink")
  - [安德面](../Page/西歸浦市安德面.md "wikilink")
  - [表善面](../Page/西歸浦市表善面.md "wikilink")
  - [松山洞](../Page/西歸浦市松山洞.md "wikilink")
  - [正房洞](../Page/西歸浦市正房洞.md "wikilink")
  - [中央洞](../Page/西歸浦市中央洞.md "wikilink")
  - [天地洞](../Page/西歸浦市天地洞.md "wikilink")
  - [孝敦洞](../Page/西歸浦市孝敦洞.md "wikilink")
  - [靈泉洞](../Page/西歸浦市靈泉洞.md "wikilink")
  - [東烘洞](../Page/西歸浦市東烘洞.md "wikilink")
  - [西烘洞](../Page/西歸浦市西烘洞.md "wikilink")
  - [大倫洞](../Page/西歸浦市大倫洞.md "wikilink")
  - [大川洞](../Page/西歸浦市大川洞.md "wikilink")
  - [中文洞](../Page/西歸浦市中文洞.md "wikilink")
  - [猊來洞](../Page/西歸浦市猊來洞.md "wikilink")

## 觀光

西歸浦是每年有超過400萬人造訪的國際性旅遊點。

市內自然環境豐富，在漢拏山及海岸有不少極好的觀景地點。此外，遊客亦可乘坐[遊覽船及](../Page/遊覽船.md "wikilink")[潛水艇觀賞近海島嶼及其海岸](../Page/潛水艇.md "wikilink")。

西部的中文觀光團地是韓國最好的集積地，該處有不少觀光設施及名勝。

### 旅遊設施

  - 泰迪熊博物館（Teddy Bear Museum）
  - 如美地植物園
  - 奇堂美術館
  - 李仲燮展示館
  - 韓國棒球榮譽殿堂
  - 中文民俗博物館
  - 城邑民俗村
  - 太平洋樂園（Pacific Land）

### 名勝

[Yakchunsa_Temple.jpg](https://zh.wikipedia.org/wiki/File:Yakchunsa_Temple.jpg "fig:Yakchunsa_Temple.jpg")\]\]
[Jeju_Songbangsan.jpg](https://zh.wikipedia.org/wiki/File:Jeju_Songbangsan.jpg "fig:Jeju_Songbangsan.jpg")

  - [藥泉寺](../Page/藥泉寺.md "wikilink")

  -
  - [李仲燮街](../Page/李仲燮.md "wikilink")

  - [漢拏山](../Page/漢拏山.md "wikilink")

  - [柱狀節理](../Page/節理.md "wikilink")

  - 天帝淵瀑布

  - 天地淵瀑布

  - [正房瀑布](../Page/正房瀑布.md "wikilink")

  - Eongtto（大坑）瀑布（）

## 體育

[Jeju_World_Cup_Stadium,_Jeju_Island.jpg](https://zh.wikipedia.org/wiki/File:Jeju_World_Cup_Stadium,_Jeju_Island.jpg "fig:Jeju_World_Cup_Stadium,_Jeju_Island.jpg")\]\]
西歸浦市因為氣候溫暖、體育競賽場地充裕，有不少韓國國內外的體育比賽選擇在此舉行。其中的[濟州世界杯競技場為](../Page/濟州世界杯競技場.md "wikilink")[2002年世界盃足球賽的會場之一](../Page/2002年世界盃足球賽.md "wikilink")。

此外，亦有不少旅客在西歸浦市觀光期間，進行[高爾夫球](../Page/高爾夫球.md "wikilink")、[騎馬](../Page/騎馬.md "wikilink")、[海上活動等運動](../Page/水上活動.md "wikilink")。

## 友好城市

**國內：**

  - [首爾特別市](../Page/首爾特別市.md "wikilink")[龍山區](../Page/龍山區.md "wikilink")（1996年11月1日）
  - [京畿道](../Page/京畿道.md "wikilink")[安陽市](../Page/安陽市.md "wikilink")（2001年10月12日）
  - [京畿道](../Page/京畿道.md "wikilink")[安城市](../Page/安城市.md "wikilink")（2011年5月6日）
  - [京畿道](../Page/京畿道.md "wikilink")[利川市](../Page/利川市.md "wikilink")（2005年5月19日）
  - [全羅北道](../Page/全羅北道.md "wikilink")[郡山市](../Page/郡山市.md "wikilink")（2009年12月8日）
  - [全羅南道](../Page/全羅南道.md "wikilink")[麗水市](../Page/麗水市.md "wikilink")（2009年10月29日）
  - [全羅南道](../Page/全羅南道.md "wikilink")[長興郡](../Page/長興郡.md "wikilink")（2011年5月13日）
  - [全羅南道](../Page/全羅南道.md "wikilink")[高興郡](../Page/高興郡.md "wikilink")（2011年11月15日）
  - [江原道](../Page/江原道.md "wikilink")[鐵原郡](../Page/鐵原郡.md "wikilink")（1996年2月2日）
  - [江原道](../Page/江原道.md "wikilink")[太白市](../Page/太白市.md "wikilink")（2014年12月22日）

**國外：**

  - [和歌山縣](../Page/和歌山縣.md "wikilink")[紀之川市](../Page/紀之川市.md "wikilink")（1987年2月20日）

  - [佐賀縣](../Page/佐賀縣.md "wikilink")[唐津市](../Page/唐津市_\(日本\).md "wikilink")（1994年9月14日）

  - [茨城縣](../Page/茨城縣.md "wikilink")[鹿嶋市](../Page/鹿嶋市.md "wikilink")（2003年11月26日）

  - [山東省](../Page/山東省.md "wikilink")[龍口市](../Page/龍口市.md "wikilink")（1999年4月20日）

  - [遼寧省](../Page/遼寧省.md "wikilink")[興城市](../Page/興城市.md "wikilink")（1996年11月12日）

  - [浙江省](../Page/浙江省.md "wikilink")[杭州市](../Page/杭州市.md "wikilink")（2000年5月2日）

  - [海南省](../Page/海南省.md "wikilink")[三亞市](../Page/三亞市.md "wikilink")（1999年11月19日）

  - [河北省](../Page/河北省.md "wikilink")[秦皇岛市](../Page/秦皇岛市.md "wikilink")（2009年7月7日）

## 外部連結

  - [西歸浦市政府](http://www.seogwipo.go.kr/)官方網站

  -
  -
  - [西歸浦市政府](http://blog.naver.com/seogwipo-si)的[Naver部落格](../Page/Naver.md "wikilink")

[Category:韓國城市](../Category/韓國城市.md "wikilink")
[Category:濟州特別自治道](../Category/濟州特別自治道.md "wikilink")