**韵母**是一個音節中除[声母外的部分](../Page/声母.md "wikilink")。它必须包含[響音](../Page/響音.md "wikilink")，一般是[元音](../Page/元音.md "wikilink")。由一个元音构成的[韵母称为](../Page/韵母.md "wikilink")**单元音韵母**，由多个元音构成的称为**複元音韵母**。

## 現代標準漢語

[現代標準漢語中有](../Page/現代標準漢語.md "wikilink")13個普通韻母，由五個基本母音（/a/、/ə/、/i/、/u/和/y/）及兩個[鼻音](../Page/鼻音_\(辅音\).md "wikilink")[韻尾](../Page/韻尾.md "wikilink")（/n/和/ŋ/）組合而成，另外還有[空韻](../Page/空韻.md "wikilink")（/ɨ/和/ɯ/）和一个特殊音節韻母/aɚ̯/。

  -
    {|class=wikitable style="text-align:center" width=90%

\!rowspan=2|
\!\!colspan=4|開口呼（無介音）\!\!colspan=4|齊齒呼（介音/i/）\!\!colspan=4|合口呼（介音/u/）\!\!colspan=4|撮口呼（介音/y/）
|- \![國際
音標](../Page/國際音標.md "wikilink")\!\![漢語
拼音](../Page/漢語拼音.md "wikilink")\!\![注音
符號](../Page/注音符號.md "wikilink")\!\!範例 \![國際
音標](../Page/國際音標.md "wikilink")\!\![漢語
拼音](../Page/漢語拼音.md "wikilink")\!\![注音
符號](../Page/注音符號.md "wikilink")\!\!範例 \![國際
音標](../Page/國際音標.md "wikilink")\!\![漢語
拼音](../Page/漢語拼音.md "wikilink")\!\![注音
符號](../Page/注音符號.md "wikilink")\!\!範例 \![國際
音標](../Page/國際音標.md "wikilink")\!\![漢語
拼音](../Page/漢語拼音.md "wikilink")\!\![注音
符號](../Page/注音符號.md "wikilink")\!\!範例 |- |rowspan=3 bgcolor=\#E6E6FA
style="line-height: 1em"|開
尾
韻 |/a/||a||ㄚ||阿||/i̯a/||ia||ㄧㄚ||鸭||/u̯a/||ua||ㄨㄚ||瓦||colspan=4
bgcolor=\#CCCCCC| |-
|/ɤ/||e||ㄜ||俄||/i̯e/||ie||ㄧㄝ||也||/u̯o/||uo||ㄨㄛ||我||/y̯e/||üe||ㄩㄝ||乐
|-
|/ɨ/、/ɯ/||-i||ㄭ[U+312D.svg](https://zh.wikipedia.org/wiki/File:U+312D.svg "fig:U+312D.svg")||
師與私的韵母<small><sup>1</sup></small>||/i/||i||ㄧ||亿||/u/||u||ㄨ||五||/y/||ü||ㄩ||与
|- style="border-top: 2px solid black;" |rowspan=5 bgcolor=\#E6E6FA
style="line-height: 1em"|元
音
尾
韻 | style="border-top: 2px solid black;" |/ai̯/||ai||style="border-top:
2px solid black;"|ㄞ||艾
|/i̯ai̯/||iai||ㄧㄞ||崖<small><sup>2</sup></small>||/u̯ai̯/||uai||ㄨㄞ||外||colspan=4
bgcolor=\#CCCCCC| |- |/ei̯/||ei||ㄟ||欸||colspan=4 bgcolor=\#CCCCCC|
|/u̯ei̯/||ui||ㄨㄟ||卫||colspan=4 bgcolor=\#CCCCCC| |-
|/au̯/||ao||ㄠ||凹||/i̯au̯/||iao||ㄧㄠ||幺 |colspan=4 bgcolor=\#CCCCCC|
||colspan=4 bgcolor=\#CCCCCC| |- |/ou̯/||ou||ㄡ||欧||/i̯ou̯/||iu||ㄧㄡ||又
|colspan=4 bgcolor=\#CCCCCC| ||colspan=4 bgcolor=\#CCCCCC| |-
|/aɚ̯/||er||ㄦ||二||colspan=4 bgcolor=\#CCCCCC| ||colspan=4
bgcolor=\#CCCCCC| ||colspan=4 bgcolor=\#CCCCCC| |- style="border-top:
2px solid black;" |rowspan=5 bgcolor=\#E6E6FA style="line-height:
1em"|鼻
尾
韻 |/an/||an||ㄢ||安||/i̯ɛn/||ian||ㄧㄢ||言
|/u̯an/||uan||ㄨㄢ||万||/y̯ɛn/||üan||ㄩㄢ||元 |-
|/ən/||en||ㄣ||恩||/in/||in||ㄧㄣ||尹
|/u̯ən/||un||ㄨㄣ||文||/yn/||ün||ㄩㄣ||云 |-
|/aŋ/||ang||ㄤ||昂||/i̯aŋ/||iang||ㄧㄤ||央
|/u̯aŋ/||uang||ㄨㄤ||亡||colspan=4 bgcolor=\#CCCCCC| |-
|/əŋ/||eng||ㄥ||鞥 |/iŋ/||ing||ㄧㄥ||英 |/u̯əŋ/||ueng||ㄨㄥ||瓮 |colspan=4
bgcolor=\#CCCCCC| |- |/ʊŋ/||ong||ㄨㄥ||中的韵母||/i̯ʊŋ/||iong||ㄩㄥ||永
|colspan=4 bgcolor=\#CCCCCC| ||colspan=4 bgcolor=\#CCCCCC| |}
<small><sup>1</sup> /ɯ/用在c（ㄘ）, s（ㄙ）和z（ㄗ）後，/ɨ/用在ch（ㄔ）, r（ㄖ）,
sh（ㄕ）和zh（ㄓ）後。</small>
<small><sup>2</sup>
Iai（ㄧㄞ）僅存在于臺灣的[國語中](../Page/中華民國國語.md "wikilink")「崖」等七字，中國大陆普通話讀音已改為yá。</small>
其中，只有一个元音或元音带鼻[辅音时](../Page/辅音.md "wikilink")，该元音是[韵腹](../Page/韵腹.md "wikilink")，所带的鼻辅音是韵尾。两个元音构成的韵母，则是开口度较大的是韵腹，韵腹前的元音是[韵头](../Page/韵头.md "wikilink")，韵腹后的是韵尾。两个元音带一个鼻辅音，则中间的元音是韵腹，第一个元音是韵头，韵腹后的元音或鼻辅音是韵尾。所有韵母可根据韵头分为四类，称为[四呼](../Page/四呼.md "wikilink")。二十世紀以來之拼音方案多半已將可作為韵头的/i/、/u/和/y/稱為介音或介母，可進一步簡化韻母數量。

  - 汉语拼音的韵母表依照順序為（i, u和ü可作為介音與其他韻母拼音）：

|   |   |   |   |   |   |    |    |    |    |    |    |    |    |    |    |    |    |    |    |     |     |     |     |
| - | - | - | - | - | - | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | --- | --- | --- | --- |
| a | o | e | i | u | ü | ai | ei | ui | ao | ou | iu | ie | üe | er | an | en | in | un | ün | ang | eng | ing | ong |

  - 注音符號的韻母依照順序為（ㄧ、ㄨ和ㄩ可作為介母與其他韻母拼音，空韻ㄭ[U+312D.svg](https://zh.wikipedia.org/wiki/File:U+312D.svg "fig:U+312D.svg")則多半不寫出）：

|                                                                                 |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |
| ------------------------------------------------------------------------------- | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - |
| ㄭ([U+312D.svg](https://zh.wikipedia.org/wiki/File:U+312D.svg "fig:U+312D.svg")) | ㄚ | ㄛ | ㄜ | ㄝ | ㄞ | ㄟ | ㄠ | ㄡ | ㄢ | ㄣ | ㄤ | ㄥ | ㄦ | ㄧ | ㄨ | ㄩ |

此外，還有[兒化韻母](../Page/元音的儿化现象.md "wikilink")；漢語拼音中在韻母原始寫法後加r，不同韻母的[兒化音可能簡併](../Page/兒化音.md "wikilink")，如ar,
anr和air發音相同，也就是說，“牌兒”的发音等于“盤兒”。

## 粵語

粵語有94個[韻母](../Page/韻母.md "wikilink")（包括自成音節的\[m̩ ŋ̍\]）\[1\]：

  -
    {| class="wikitable" style="width:80%; text-align:center"

\!||colspan=5|開尾韻||colspan=10|[元音](../Page/元音.md "wikilink")[尾韻](../Page/韻尾.md "wikilink")
|- \![開口呼](../Page/開口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![齊齒呼](../Page/齊齒呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![合口呼](../Page/合口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![撮口呼](../Page/撮口呼.md "wikilink") ||||||||||||||||||||||||||||| |}

  -
    {| class="wikitable" style="width:80%; text-align:center"

\!||colspan=15|[鼻](../Page/鼻音_\(辅音\).md "wikilink")[尾韻](../Page/韻尾.md "wikilink")
|- \![開口呼](../Page/開口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![齊齒呼](../Page/齊齒呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![合口呼](../Page/合口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![撮口呼](../Page/撮口呼.md "wikilink") ||||||||||||||||||||||||||||| |}

  -
    {| class="wikitable" style="width:80%; text-align:center"

\!||colspan=15|[塞音](../Page/塞音.md "wikilink")[尾韻](../Page/韻尾.md "wikilink")
|- \![開口呼](../Page/開口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![齊齒呼](../Page/齊齒呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![合口呼](../Page/合口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![撮口呼](../Page/撮口呼.md "wikilink") ||||||||||||||||||||||||||||| |}

  -
    {| class="wikitable" style="width:15%; text-align:center"

\!其他 ||| |}

## 閩南語

[閩南語的韻母](../Page/闽南语.md "wikilink")（以[白話字拼寫](../Page/白話字.md "wikilink")）如下表所示：

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>韻腹</p></th>
<th><p>陰聲韻</p></th>
<th><p>陽聲韻</p></th>
<th><p><a href="../Page/入聲.md" title="wikilink">入聲韻</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[m]</p></td>
<td><p>[n]</p></td>
<td><p>[ŋ]</p></td>
<td><p>[p̚]</p></td>
</tr>
<tr class="even">
<td><p>[a]</p></td>
<td><p>a</p></td>
<td><p>aⁿ</p></td>
<td><p>am</p></td>
</tr>
<tr class="odd">
<td><p>[aɪ]</p></td>
<td><p>ai</p></td>
<td><p>aiⁿ</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>[aʊ]</p></td>
<td><p>au</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[e]</p></td>
<td><p>e</p></td>
<td><p>eⁿ</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>[i]</p></td>
<td><p>i</p></td>
<td><p>iⁿ</p></td>
<td><p>im</p></td>
</tr>
<tr class="odd">
<td><p>[ɪa]</p></td>
<td><p>ia</p></td>
<td><p>iaⁿ</p></td>
<td><p>iam</p></td>
</tr>
<tr class="even">
<td><p>[ɪaʊ]</p></td>
<td><p>iau</p></td>
<td><p>iauⁿ</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[ɪə]</p></td>
<td><p>io</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>[ɪɔ]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[iu]</p></td>
<td><p>iu</p></td>
<td><p>iuⁿ</p></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>韻腹</p></th>
<th><p>陰聲韻</p></th>
<th><p>陽聲韻</p></th>
<th><p><a href="../Page/入聲.md" title="wikilink">入聲韻</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>[m]</p></td>
<td><p>[n]</p></td>
<td><p>[ŋ]</p></td>
<td><p>[p̚]</p></td>
</tr>
<tr class="even">
<td><p>[ə]</p></td>
<td><p>o</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[ɔ]</p></td>
<td><p>o͘</p></td>
<td><p>oⁿ</p></td>
<td><p>om</p></td>
</tr>
<tr class="even">
<td><p>[ua]</p></td>
<td><p>oa</p></td>
<td><p>oaⁿ</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[uai]</p></td>
<td><p>oai</p></td>
<td><p>oaiⁿ</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>[ue]</p></td>
<td><p>oe</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[u]</p></td>
<td><p>u</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>[ui]</p></td>
<td><p>ui</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>[m̩]</p></td>
<td><p>m</p></td>
<td><p>－</p></td>
<td><p>mh</p></td>
</tr>
<tr class="even">
<td><p>[ŋ̍]</p></td>
<td><p>ng</p></td>
<td><p>－</p></td>
<td><p>ngh</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 参看

  - [國語元音圖](../Page/國語元音圖.md "wikilink")
  - [元音](../Page/元音.md "wikilink")
  - [介音](../Page/介音.md "wikilink")
  - [韵尾](../Page/韵尾.md "wikilink")
  - [韵腹](../Page/韵腹.md "wikilink")
  - [聲母](../Page/聲母.md "wikilink")
  - [开音节](../Page/开音节.md "wikilink")
  - [闭音节](../Page/闭音节.md "wikilink")

## 外部連結

  - [Basic Rules of Hanyu Pinyin
    Orthography](http://pinyin.info/readings/zyg/rules.html) by [Zhou
    Youguang](../Page/Zhou_Youguang.md "wikilink") (Pinyin.info. Now
    superseded by GB/T 16159-2012 below.)
  - [Basic rules of the Chinese phonetic alphabet
    orthography](http://www.moe.gov.cn/ewebeditor/uploadfile/2012/08/21/20120821100233165.pdf)
    (The official standard GB/T 16159-2012 in Chinese. PDF version from
    the Chinese Ministry of Education. — [HTML
    version](http://pinyin.info/rules/GBT16159-2012.html) on
    pinyin.info)
  - [Chinese phonetic alphabet spelling rules for Chinese
    names](http://www.moe.gov.cn/ewebeditor/uploadfile/2012/06/01/20120601104529410.pdf)
    (The official standard GB/T 28039-2011 in Chinese. PDF version from
    the Chinese Ministry of Education.)
  - [Free Pinyin Tutorial](http://chineseandbeyond.com/pinyin) (Chinese
    & Beyond)
  - [Pinyin Chart with
    Audio](http://www.ichineselearning.com/learn/pinyin-chart.html)
  - [Pinyin Tone Recognition
    Test](http://pinyin.quickmandarin.com/learn_chinese_quiz/blue/)
  - [Pinyin.info](http://www.pinyin.info)

[Category:漢語音韻學](../Category/漢語音韻學.md "wikilink")

1.