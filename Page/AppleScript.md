{{ infobox programming language | developer =
[苹果公司](../Page/苹果公司.md "wikilink") | logo =
[<File:AppleScript>
Editor.png](https://zh.wikipedia.org/wiki/File:AppleScript_Editor.png "fig:File:AppleScript Editor.png")
| latest_release_version = 2.7 | latest_release_date =  |
operating_system = [System 7](../Page/System_7.md "wikilink")、[Mac OS
8](../Page/Mac_OS_8.md "wikilink")、[Mac OS
9](../Page/Mac_OS_9.md "wikilink")、[Mac OS
X](../Page/Mac_OS_X.md "wikilink") | genre =
[脚本语言](../Page/脚本语言.md "wikilink") | license = Apple
[EULA](../Page/EULA.md "wikilink")（parts available under
[APSL](../Page/Apple_Public_Source_License.md "wikilink")） | website =
[developer.apple.com/applescript/](http://developer.apple.com/applescript/)
}}

**AppleScript**是[苹果公司开发的一种](../Page/苹果公司.md "wikilink")[脚本语言](../Page/脚本语言.md "wikilink")，可以用来控制运行于[Mac
OS上的程序](../Page/Mac_OS.md "wikilink")，也可以写成独立运行的[Applet](../Page/Applet.md "wikilink")。最早版本在1993年十月推出，在System
7（System 7.1.1）運行。

## 歷史

AppleScript的前身是[Hypercard所使用的腳本語言](../Page/Hypercard.md "wikilink")[Hypertalk](../Page/Hypertalk.md "wikilink")。蘋果發現Hypertalk類似[英語的語法](../Page/英語.md "wikilink")，可用於操控其他軟件，於是開發出AppleScript，成為System
7的一部份。

AppleScript在1993年十月隨system
7.1.1推出。[桌面排版軟件](../Page/桌面排版.md "wikilink")[QuarkXpress是為首支援AppleScript的軟件](../Page/QuarkXpress.md "wikilink")。而以AppleScript編程自動化QuarkXpress排版作業，是當時流行的做法。就算後期QuarkXpress移殖到Windows，但AppleScript易於編程簡化QuarkXpress複雜的排版作業，这就是[麥金塔至今仍穩奪排版作業機器標準的主要原因](../Page/麥金塔.md "wikilink")。

AppleScript亦從[Classic Mac
OS過渡到](../Page/Classic_Mac_OS.md "wikilink")[Mac OS
X及](../Page/Mac_OS_X.md "wikilink")[Cocoa架構](../Page/Cocoa.md "wikilink")。蘋果更隨Mac
OS X 10.2推出AppleScript Studio，可完全使用AppleScript製作具有圖像界面的Cocoa軟件。

蘋果在2005年推出Mac OS X 10.4
Tiger時，連同新功能[Automator](../Page/Automator.md "wikilink")，是繼AppleScript後另一款自動化作業流程的工具。

## Hello World

有多種寫法可以編寫[Hello World程式](../Page/Hello_World.md "wikilink")：

``` AppleScript
display dialog "Hello, world!" -- a modal window with “OK” and “Cancel” buttons
-- or
display alert "Hello, world!"  -- a modal window with a single “OK” button and an icon representing the app displaying the alert
-- or
say "Hello, world!" -- an audio message using a synthesized computer voice
```

## 外部連結

### 非官方

  - [MacScripter](http://macscripter.net/)

{{-}}

[Category:腳本語言](../Category/腳本語言.md "wikilink")
[Category:MacOS開發](../Category/MacOS開發.md "wikilink")