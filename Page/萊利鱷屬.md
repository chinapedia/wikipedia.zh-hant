**萊利鱷屬**（屬名：*Rileyasuchus*）是[植龍目的一屬](../Page/植龍目.md "wikilink")，生存於晚[三疊紀](../Page/三疊紀.md "wikilink")[瑞提階的](../Page/瑞提階.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")。萊利鱷的歷史非常複雜，過去曾經與*Palaeosaurus*與[槽齒龍有關連](../Page/槽齒龍.md "wikilink")，原本的屬名為*Rileya*，但已有一種[膜翅目名為](../Page/膜翅目.md "wikilink")*Rileya*，因此改名為萊利鱷（*Rileyasuchus*）\[1\]\[2\]。

## 歷史與分類學

在1902年，[德國古生物學家](../Page/德國.md "wikilink")[弗雷德里克·馮·休尼博士根據在](../Page/弗雷德里克·馮·休尼.md "wikilink")[布里斯托沉積層發現的兩個](../Page/布里斯托.md "wikilink")[脊椎骨與一個](../Page/脊椎骨.md "wikilink")[肱骨](../Page/肱骨.md "wikilink")，建立為新屬*Rileya*，屬於[植龍目](../Page/植龍目.md "wikilink")，並以[英國](../Page/英國.md "wikilink")[古生物學家Henry](../Page/古生物學家.md "wikilink")
Riley為名，這些化石與*Palaeosaurus cylindrodon*、*Palaeosaurus
platyodon*、以及[槽齒龍發現於同一地點](../Page/槽齒龍.md "wikilink")\[3\]。稍後在1908年，休尼認為*P.
platyodon*的牙齒類似植龍類，因此歸類於*Rileya platyodon*\[4\]。

在1961年，Oskar
Kuhn將*Rileya*重新命名為萊利鱷（*Rileyasuchus*）\[5\]。在1994年，[阿德里安·亨特](../Page/阿德里安·亨特.md "wikilink")（Adrian
Hunt）與提出萊利鱷應屬於[艾雷拉龍科](../Page/艾雷拉龍科.md "wikilink")，但並未正式公佈\[6\]。在2000年，[麥可·班頓](../Page/麥可·班頓.md "wikilink")（Michael
Benton）等人指出萊利鱷的[模式標本其實是個](../Page/模式標本.md "wikilink")[嵌合體](../Page/嵌合體.md "wikilink")，由一個植龍類的肱骨，與槽齒龍的脊椎骨構成\[7\]。萊利鱷的目前狀態為[疑名](../Page/疑名.md "wikilink")。

## 古生物學

如同其他植龍目，萊利鱷是種半水生、類似[鱷魚的掠食動物](../Page/鱷魚.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [*Rileyasuchus* as
    herrerasaurid](http://dml.cmnh.org/2000Feb/msg00297.html)
  - <https://web.archive.org/web/20070526192951/http://www.reference.com/browse/wiki/Palaeosaurus>

[Category:植龍目](../Category/植龍目.md "wikilink")

1.  Howard, L.O. (1888). The chalcid genus *Rileya*. Canadian Entomology
    20:191-195.
2.  Ashmead, W.H. (1888). A revised generic table of the Euryrtominae,
    with descriptions of new species. Pt I. Entomol. Am. 4:41-43.
3.  von Huene, F. (1902). Überischt über die Reptilien der Trias.
    *Geologische und Paläontologie Abhandlungen, Neu Folge* 8:97-156.
    \[German\]
4.  von Huene, F. (1908). On phytosaurian remains from the Magnesian
    Conglomerate of Bristol (*Rileya platyodon*). *Annals and Magazine
    of Natural History*, series 8 1:228-230.
5.  Kuhn, O. (1961). *Die Familien der rezenten und fossilen Amphibien
    und Reptilien.* Meisenbach:Bamberg, 79 p.
6.  Hunt, A.P. (1994). Unpublished doctoral dissertation, [discussed
    here](http://dml.cmnh.org/2000Feb/msg00297.html)
7.  Benton, M.J, Juul, L., Storrs, G.W., and Galton, P.M. (2000).
    Anatomy and systematics of the prosauropod dinosaur
    *Thecodontosaurus antiquus* from the upper Triassic of southwest
    England. *Journal of Vertebrate Paleontology* 20(1):77-108.