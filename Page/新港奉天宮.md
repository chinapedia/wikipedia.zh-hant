**新港奉天宮**，是[臺灣著名的](../Page/臺灣.md "wikilink")[媽祖廟](../Page/媽祖廟.md "wikilink")，位於[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉新民路](../Page/新港鄉.md "wikilink")53號，座落於新民路與中山路口，坐北朝南略為偏西，明定為祠廟類三級古蹟，主要奉祀[天上聖母](../Page/天上聖母.md "wikilink")。

新港奉天宮是全臺灣少數把[虎爺供奉於桌上的廟宇](../Page/虎爺.md "wikilink")\[1\]，也是全國金虎爺會的大本營。

## 歷史

1622年（[明朝](../Page/明朝.md "wikilink")[天啟二年](../Page/天啟.md "wikilink")）[福建船戶劉定國奉請](../Page/福建.md "wikilink")[湄洲天后宮](../Page/湄洲天后宮.md "wikilink")[媽祖](../Page/媽祖.md "wikilink")（俗稱船仔媽）金身神像，橫渡[黑水溝航經](../Page/黑水溝.md "wikilink")[笨港時媽祖顯聖](../Page/笨港.md "wikilink")，指示永駐此地，從此笨港十寨輪流奉祀，永保台疆\[2\]。

1700年（[清](../Page/清.md "wikilink")[康熙三十九年](../Page/康熙.md "wikilink")）與外九庄合建[笨港天妃廟於笨港](../Page/笨港天妃廟.md "wikilink")\[3\]，1730年（清[雍正八年](../Page/雍正.md "wikilink")）始稱「笨港天后宮」\[4\]。

1742年湄洲祖廟史料記載，3尊神像中1尊「大媽」留在湄洲祖廟朝天閣正殿，惜已於[文化大革命期間被毀](../Page/文化大革命.md "wikilink")；第2尊「二媽」流向南洋，不知去向；第3尊「三媽」則為今日[新竹市](../Page/新竹市.md "wikilink")[新竹長和宮的軟身真髮媽祖](../Page/新竹長和宮.md "wikilink")，稱「湄洲祖廟正三媽」。

1765年（[乾隆年間](../Page/乾隆.md "wikilink")）三槺榔庄（現今[雲林縣](../Page/雲林縣.md "wikilink")[元長鄉客仔厝](../Page/元長鄉.md "wikilink")）的笨港溪邊由上游漂流來樟木，每逢夜晚即會出毫光，被庄民撈起後，而獻給笨港天后宮，依[神諭雕為三尊媽祖神像](../Page/神諭.md "wikilink")，樹頭那節雕成之神像稱為「祖媽－大媽」中節雕成之神像稱為「二媽」，末節雕成之神像稱為「三媽」，此三神像與「船仔媽－開台媽祖」同奉祀於笨港天后宮內\[5\]。

1799年（[嘉慶四年](../Page/嘉慶.md "wikilink")）笨港溪洪水橫溢氾濫，吞淹沒了四千多戶住家，笨港天后宮同時也遭沖毀，據傳神像沖走下落不明，亦有一說住持景瑞和尚護持的廟中神像、文物，而將東移至麻園寮土地公廟肇慶堂（後稱笨新南港，後改稱新港）\[6\]\[7\]。

1801年（[嘉慶六年](../Page/嘉慶.md "wikilink")），住持景瑞和尚發起建廟，經[福建](../Page/福建.md "wikilink")[水師提督](../Page/水師提督.md "wikilink")[王得祿捐俸](../Page/王得祿.md "wikilink")及十八庄人士共同捐獻\[8\]。

1811年（[嘉慶十六年](../Page/嘉慶.md "wikilink")）新廟落成，王得祿提督奏請[嘉慶帝御賜宮名為](../Page/嘉慶帝.md "wikilink")「奉天宮」，自稱為古笨港天后宮香火之分支及延續，據傳天后宮四尊神像亦暫奉於奉天宮。\[9\]。

1837年於笨港天后宮原址之[北港朝天宮修建](../Page/北港朝天宮.md "wikilink")，相傳建廟後向新港奉天宮請求天后宮媽祖神像應返奉於北港朝天宮，最後由[王得祿將軍出面協議仲裁](../Page/王得祿.md "wikilink")，決定一尊留在奉天宮，一尊供奉朝天宮，至於順位則尚有爭論(（奉天宮說法為大媽在奉天宮，二媽至朝天宮，且是否為真亦已不可考）)，「三媽」由他本人請回牛稠溪（現今溪北）暫供於提督公館「奉茶」，在[王得祿逝世後](../Page/王得祿.md "wikilink")，則歸入溪北六興宮奉祀\[10\]。

1839年（[道光十九年](../Page/道光.md "wikilink")）[溪北六興宮落成](../Page/溪北六興宮.md "wikilink")，[王得祿](../Page/王得祿.md "wikilink")[提督親自恭請](../Page/提督.md "wikilink")「正三媽」神像入殿安座\[11\]。

## 奉祀神祇

[Singang_Fengtian_Temple.jpg](https://zh.wikipedia.org/wiki/File:Singang_Fengtian_Temple.jpg "fig:Singang_Fengtian_Temple.jpg")

  - 正殿：[天上聖母](../Page/天上聖母.md "wikilink")
  - 後殿：[觀世音菩薩](../Page/觀世音菩薩.md "wikilink")，配祀[福德正神](../Page/福德正神.md "wikilink")、[註生娘娘及](../Page/註生娘娘.md "wikilink")[十八羅漢](../Page/十八羅漢.md "wikilink")
  - 左配殿：奉祀[文昌帝君](../Page/文昌帝君.md "wikilink")。
  - 右配殿：奉祀[關聖帝君](../Page/關聖帝君.md "wikilink")。
  - 東廂配祀：[笨港](../Page/笨港.md "wikilink")[城隍爺殿](../Page/城隍.md "wikilink")。
  - 西廂配祀：[虎爺殿](../Page/虎爺.md "wikilink")、長生祿位殿。
  - 凌霄寶殿：三樓奉[三寶佛](../Page/三寶佛.md "wikilink")、配祀[西秦王爺](../Page/西秦王爺.md "wikilink")、聖父母（媽祖之父母）、六十[太歲星君](../Page/太歲.md "wikilink")、頂樓奉[玉皇大帝及](../Page/玉皇大帝.md "wikilink")[三官大帝](../Page/三官大帝.md "wikilink")、[南斗星君](../Page/南斗星君.md "wikilink")、[北斗星君](../Page/北斗星君.md "wikilink")、[田都元帥](../Page/田都元帥.md "wikilink")

[新港奉天宮虎爺殿.jpg](https://zh.wikipedia.org/wiki/File:新港奉天宮虎爺殿.jpg "fig:新港奉天宮虎爺殿.jpg")
[新港奉天宮金虎爺.jpg](https://zh.wikipedia.org/wiki/File:新港奉天宮金虎爺.jpg "fig:新港奉天宮金虎爺.jpg")
台灣民間流傳「笨港媽祖，麻園寮老虎」，意謂媽祖、虎爺神威顯赫佑民；奉天宮「虎爺殿」的建築於民國60年所興建，是由蔡玉誠先生為了感謝虎爺，以獨資捐款重修，也在壁畫上撰文虎爺顯靈護佑其全家事蹟，並於每年農曆6月6日為新港虎爺聖誕千秋，而在虎爺聖誕千秋前，所有分靈出去的虎爺都要回來祖廟謁祖進香。新港奉天宮虎爺殿有一尊全台唯一供奉置神桌上的金虎將軍，神像頭插金花，代表有如狀元一般的官職之位；而頭朝著天上看，代表張口昂首看天虎。目前新港虎爺分靈遍佈全台各地，已有上千尊，甚至到土耳其；位於埔里的「埔里青天堂」虎爺將軍是目前全台唯一供有供奉開基虎爺的廟宇，此廟正是新港奉天宮所分靈。

## 其他附屬設施與建築

  - 新港奉天宮香客大樓
  - 新港奉天宮香客大樓餐廳部
  - 新港奉天宮全國金虎爺總會商品專賣店攤位
  - 新港奉天宮新港媽ㄟ柑仔店-27號
  - 新港奉天宮全國金虎爺總會會館
  - 新港奉天宮四街祖媽會會館
  - 新港奉天宮金虎爺發財金服務處

## 活動

### 大甲媽祖遶境進香活動

大甲媽祖遶境進香活動，為[台灣](../Page/台灣.md "wikilink")[臺中市廟宇](../Page/臺中市.md "wikilink")[大甲鎮瀾宮](../Page/大甲鎮瀾宮.md "wikilink")，於每年[農曆三月間舉行](../Page/農曆.md "wikilink")、長達九天八夜的[大甲媽出巡遶境](../Page/媽祖.md "wikilink")，行經駐駕地點有(依時間排序)彰化南瑤宮、西螺福興宮、新港奉天宮、返程增加北斗奠安宮、彰化天后宮、清水朝興宮，終點在嘉義縣新港奉天宮。

### 其他例行活動

  - 每年[農曆](../Page/農曆.md "wikilink")[正月初一子時](../Page/正月.md "wikilink")，會舉行開廟門、[搶頭香活動](../Page/搶頭香.md "wikilink")。

<!-- end list -->

  - 每年[農曆正月十五日](../Page/農曆.md "wikilink")（[元宵節](../Page/台灣元宵節.md "wikilink")），當天依例舉行「媽祖[遶境](../Page/遶境.md "wikilink")」活動，其出巡範圍為新港鄉市區四村，然而每次董監事會改選後的隔年農曆正月十四及十五日，就擴大舉行其媽祖出巡遶境活動，其出巡範圍，為古笨港南堡十八庄
    ，並包括嘉義縣溪口鄉和雲林縣北港鎮、元長鄉。無論是每年單只遶境新港市區，或是十八庄繞境，都給新港及鄰近地區，帶來空前熱烈的媽祖信仰氣氛。

<!-- end list -->

  - 於2009年第一次擴大舉辦8天7夜「歡喜‧會香‧迓媽祖」，遶境範圍為[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉](../Page/新港鄉.md "wikilink")、[民雄鄉](../Page/民雄鄉.md "wikilink")、[溪口鄉](../Page/溪口鄉_\(台灣\).md "wikilink")、[六腳鄉與](../Page/六腳鄉.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[元長鄉](../Page/元長鄉.md "wikilink")、[北港鎮](../Page/北港鎮_\(臺灣\).md "wikilink")，並與[北港朝天宮於正月初十展開](../Page/北港朝天宮.md "wikilink")60年來首度會香的破冰之旅。

<!-- end list -->

  - 於2010年擴大舉辦9天8夜「山海遊香迎媽祖」，遶境範圍為[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉](../Page/新港鄉.md "wikilink")、[民雄鄉](../Page/民雄鄉.md "wikilink")、[竹崎鄉](../Page/竹崎鄉.md "wikilink")、[水上鄉](../Page/水上鄉.md "wikilink")、[鹿草鄉](../Page/鹿草鄉.md "wikilink")、[太保市](../Page/太保市.md "wikilink")、[朴子市](../Page/朴子市.md "wikilink")、[東石鄉](../Page/東石鄉_\(台灣\).md "wikilink")、[布袋鎮](../Page/布袋鎮.md "wikilink")、[義竹鄉](../Page/義竹鄉.md "wikilink")、[六腳鄉與](../Page/六腳鄉.md "wikilink")[嘉義市和](../Page/嘉義市.md "wikilink")[臺南市](../Page/臺南市.md "wikilink")[鹽水區](../Page/鹽水區.md "wikilink")；並於正月初七、初八300來頭一次蒞臨[嘉義市為台灣燈會加持祈福並於正月初八早上集結](../Page/嘉義市.md "wikilink")128尊[千里眼](../Page/千里眼.md "wikilink")、[順風耳神將創下金氏世界紀錄](../Page/順風耳.md "wikilink")。

<!-- end list -->

  - 於2010年國曆12月18、19日迎奉[四街祖媽遶境](../Page/四街祖媽.md "wikilink")[中埔鄉](../Page/中埔鄉.md "wikilink")、[大埔鄉](../Page/大埔鄉.md "wikilink")，並與[大埔北極殿](../Page/大埔北極殿.md "wikilink")[玄天上帝一同遶境](../Page/玄天上帝.md "wikilink")[曾文水庫](../Page/曾文水庫.md "wikilink")。

<!-- end list -->

  - 於2011年擴大舉辦9天8夜「山海遊香迎媽祖」，遶境範圍為[嘉義縣](../Page/嘉義縣.md "wikilink")[新港鄉](../Page/新港鄉.md "wikilink")、[溪口鄉](../Page/溪口鄉_\(台灣\).md "wikilink")、[大林鎮](../Page/大林鎮_\(台灣\).md "wikilink")、[民雄鄉](../Page/民雄鄉.md "wikilink")、[竹崎鄉](../Page/竹崎鄉.md "wikilink")、[水上鄉](../Page/水上鄉.md "wikilink")、[太保市](../Page/太保市.md "wikilink")、[朴子市](../Page/朴子市.md "wikilink")、[東石鄉](../Page/東石鄉_\(台灣\).md "wikilink")、[布袋鎮](../Page/布袋鎮.md "wikilink")、[六腳鄉與](../Page/六腳鄉.md "wikilink")[嘉義市](../Page/嘉義市.md "wikilink")；這年正逢民國100年奉天宮邀及100家宮廟一同參與賜福，並與[溪北六興宮](../Page/溪北六興宮.md "wikilink")[正三媽百年首次在元宵節當天於奉天宮內會香](../Page/正三媽.md "wikilink")。

<!-- end list -->

  - 每年元旦至農曆3月23日媽祖聖誕千秋日都會舉辦「新港奉天宮國際媽祖文化節」

<!-- end list -->

  - 每年[農曆二月底至三月初間](../Page/農曆.md "wikilink")（時間不固定），[大甲鎮瀾宮的](../Page/大甲鎮瀾宮.md "wikilink")[大甲媽祖遶境進香活動](../Page/大甲媽祖遶境進香活動.md "wikilink")，會遶境進香至新港奉天宮。

<!-- end list -->

  - 每年[農曆三月二十三日](../Page/農曆.md "wikilink")（媽祖聖誕）期間，會舉辦一系列相關慶祝活動。

<!-- end list -->

  - 2015年，媽祖指示**乙未年啟建金籙慶成祈安護國七朝[清醮](../Page/清醮.md "wikilink")**。

<!-- end list -->

  - 2016年，媽祖指示**丙申年啟建叩謝天恩祈安三朝[圓醮](../Page/圓醮.md "wikilink")**。

<!-- end list -->

  - 每年舉辦『跑出金虎爺』全國路跑活動。

<!-- end list -->

  - 2017年10月2日，湄洲媽祖來台巡安，將駐駕新港奉天宮遶境祈安，由財團法人新港奉天宮、湄洲媽祖祖廟董事會與台灣媽祖聯誼會共同舉辦。

## 正統之爭

笨港正統之爭在北港新港兩地爭執已久，主要論點在於笨港天妃廟是否遭到沖毀：

### 笨港毀滅論

嘉慶四年（[1799年](../Page/1799年.md "wikilink")）笨港溪([北港溪](../Page/北港溪.md "wikilink"))暴漲，自古笨港街中將市街一分為二，沖毀大半市街及水仙宮、協天宮、天后宮三大廟，從此之後，笨港天后宮之「開台媽祖」及另外三尊媽祖神像之下落便無確切證據，現新港奉天宮宣稱該宮持有「開台媽祖」及原天后宮之「大媽」，但是亦無確實證據可供證明，僅為民間相傳，且北港方面亦不承認。形成兩宮相爭正統的情形，北港朝天宮因重建於原天后宮舊址以「笨港天后宮正統承繼」自居，而新港奉天宮則以「笨港天后宮開台媽祖」為號召，自稱為古天后宮香火之延續，兩宮相爭之風波至今未息，且在大甲鎮瀾宮因用詞問題而棄北港改來新港之後更為嚴重。於2009年與北港朝天宮於正月初十展開60年來首度會香的破冰之旅。

## 分靈廟宇、分靈神尊

有的廟為以[新港奉天宮分靈媽祖而創建的新廟](../Page/新港奉天宮.md "wikilink")，係「分靈廟宇」；有的則為原有廟堂增奉[新港奉天宮之分靈媽祖](../Page/新港奉天宮.md "wikilink")，係「分靈神尊」，在此均通列如下：

  - [天津天后宮](../Page/天津天后宮.md "wikilink")
  - [丹東大孤山天后宮](../Page/丹東大孤山天后宮.md "wikilink")
  - [賢良港天后祖祠的](../Page/賢良港天后祖祠.md "wikilink")[天后聖殿](../Page/天后聖殿.md "wikilink")
  - [福建永春陳坂宮](../Page/福建永春陳坂宮.md "wikilink")
  - [廣西桂林榕津天后宮](../Page/廣西桂林榕津天后宮.md "wikilink")
  - [遼寧錦州天后宮](../Page/遼寧錦州天后宮.md "wikilink")
  - [南竿津沙天后宮](../Page/南竿津沙天后宮.md "wikilink")
  - [江蘇泗陽媽祖文化園](../Page/江蘇泗陽媽祖文化園.md "wikilink")
  - [南京天妃宮](../Page/南京天妃宮.md "wikilink")
  - [福鼎前岐媽祖天后宮](../Page/福鼎前岐媽祖天后宮.md "wikilink")
  - [福建霞浦松山天后行宮](../Page/福建霞浦松山天后行宮.md "wikilink")
  - [浙江蒼南坑尾媽祖廟](../Page/浙江蒼南坑尾媽祖廟.md "wikilink")
  - [嘉義濟佛堂](../Page/嘉義濟佛堂.md "wikilink")
  - [澳門珠海橫琴奉天宮](../Page/澳門珠海橫琴奉天宮.md "wikilink")
  - [嘉義民雄牛斗山廣濟宮](../Page/嘉義民雄牛斗山廣濟宮.md "wikilink")
  - [嘉義新港林家三媽天上聖母](../Page/嘉義新港林家三媽天上聖母.md "wikilink")
  - [雲林西螺永興宮](../Page/雲林西螺永興宮.md "wikilink")
  - [新北板橋玉后宮](../Page/新北板橋玉后宮.md "wikilink")
  - [南投埔里青天堂](../Page/南投埔里青天堂.md "wikilink")
  - [漳州龍海榜山平寧林邊媽祖廟](../Page/漳州龍海榜山平寧林邊媽祖廟.md "wikilink")
  - [日本宇治黃蘖山萬福寺](../Page/日本宇治黃蘖山萬福寺.md "wikilink")
  - [日本京都臨濟宗妙心寺靈雲院](../Page/日本京都臨濟宗妙心寺靈雲院.md "wikilink")
  - [日本京都臨濟宗妙心寺天球院](../Page/日本京都臨濟宗妙心寺天球院.md "wikilink")
  - [日本京都臨濟宗妙心寺本光寺](../Page/日本京都臨濟宗妙心寺本光寺.md "wikilink")
  - [漳州南靖龍山寶斗玉皇宮](../Page/漳州南靖龍山寶斗玉皇宮.md "wikilink")
  - [馬來西亞吉打慈后媽祖閣](../Page/馬來西亞吉打慈后媽祖閣.md "wikilink")（金虎將軍）
  - [中國大陸](../Page/中國大陸.md "wikilink")、[德國](../Page/德國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[日本等都有新港奉天宮新港媽的分靈](../Page/日本.md "wikilink")

## 相關條目

  - [台灣媽祖信仰](../Page/台灣媽祖信仰.md "wikilink")
  - [湄洲媽祖祖廟](../Page/湄洲媽祖祖廟.md "wikilink")
  - [大甲媽祖遶境進香活動](../Page/大甲媽祖遶境進香活動.md "wikilink")
  - [媽祖](../Page/媽祖.md "wikilink")
  - [天后宮](../Page/天后宮.md "wikilink")
  - [全國金虎爺會](../Page/全國金虎爺會.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [財團法人嘉義縣新港奉天宮全球資訊網](https://web.archive.org/web/20100612110823/http://hsinkangmazu.org.tw/)

  - [新港奉天宮官方Facebook粉絲專頁](https://www.facebook.com/hsinkangmazu/?locale2=zh_TW)

  - [郭長成老師的作品集－新港奉天宮](http://www.ttvs.cy.edu.tw/kcc/kcc57/default.htm)

  - 張珣：〈[宗教與文化創意產業：新港奉天宮與香藝文化園區](http://www.rsd.fju.edu.tw/Upload/29-03-%E5%BC%B5%E7%8F%A3-%E5%AE%97%E6%95%99%E8%88%87%E6%96%87%E5%89%B5%E7%94%A2%E6%A5%AD%E9%A6%99%E8%97%9D%E5%9C%92%E5%8D%80\(1\).pdf)〉。

  - 张珣：〈[妈祖信仰与文化产业：人类学的个案研究——以台湾嘉义新港奉天宫为例](http://www.nssd.org/articles/article_read.aspx?id=42663987)〉。

[Category:嘉義縣古蹟](../Category/嘉義縣古蹟.md "wikilink")
[Category:佛教僧侶建立之媽祖廟](../Category/佛教僧侶建立之媽祖廟.md "wikilink")
[Category:嘉義媽祖廟](../Category/嘉義媽祖廟.md "wikilink")
[Category:新港鄉](../Category/新港鄉.md "wikilink")

1.  [〈新港奉天宮虎爺〉，《台灣封神榜》節目影片，YouTube網站](https://m.youtube.com/watch?v=vcpRPzIOf1w)

2.  [新港鄉農會](http://www.fast.org.tw/q08/play.htm)但無相關文物可證明，且朝天宮早年已有記載至縣誌內，且有文物佐證，所以1622年到台灣之說法疑點重重


3.
4.
5.
6.  [新港奉天宮](http://outdoor.cyc.edu.tw/outdoor/singang/b4.htm)

7.  [太保古調工作室](http://library.taiwanschoolnet.org/cyberfair2004/C0411620091/theme_8.html)

8.
9.
10.
11. [太保古調工作室](http://library.taiwanschoolnet.org/cyberfair2004/C0411620091/theme_8.html)