****是個簡單的[文書處理器](../Page/文書處理器.md "wikilink")，內建於微軟的、和系列作業系統。繁體中文版稱之為**小作家**，简体中文版称之为**书写器**。其由始至終的變動很少，而且功能上和十分相像。早期的版本只支援檔案，但到了1989年，更新到支援的格式，並內建在翌年的。從此，開始支援對早期的文件，進行開啟和建立有關的檔案。有了，開始支援[对象连接与嵌入](../Page/对象连接与嵌入.md "wikilink")。

以取代。與相同的是功能比[記事本強大](../Page/Windows記事本.md "wikilink")。一般認為，是較現代的文本编辑器。但是，的功能遠比少。而有一些功能是（至少到
Windows 7 版本）沒有的，最明顯的是文字[對齊功能](../Page/對齊#左右對齊.md "wikilink")。

預設的圖示是一枝筆，剛寫完一個大階的字母。所以一些用戶稱為。

在之後的作業系統中，執行（執行檔）的話會開啟。

出于安全方面的考虑，在[Windows XP](../Page/Windows_XP.md "wikilink") SP2
以后的版本中，默认去除了对 .wri 格式的关联，但仍可以打开。

## 另見

  - [小畫家](../Page/小畫家.md "wikilink")

## 外部链接

  - [An edited version of Write on this page which is compatible with
    Windows XP](http://toastytech.com/guis/misc.html)

[Category:Windows组件](../Category/Windows组件.md "wikilink")
[Category:文書處理器](../Category/文書處理器.md "wikilink")