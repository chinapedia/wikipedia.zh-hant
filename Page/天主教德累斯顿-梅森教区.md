**天主教德累斯頓-梅森教區**（、）是[羅馬天主教在](../Page/羅馬天主教.md "wikilink")[德國](../Page/德國.md "wikilink")[薩克森州設立的一個教區](../Page/薩克森州.md "wikilink")，受[天主教柏林總教區監管](../Page/天主教柏林總教區.md "wikilink")。

## 現況

[Karte_Bistum_Dresden-Meißen.png](https://zh.wikipedia.org/wiki/File:Karte_Bistum_Dresden-Meißen.png "fig:Karte_Bistum_Dresden-Meißen.png")
該教區包括的面積為16,934平方千米。2011年，有教友140,436人，佔轄區總人口3.3%、97個堂區、223名司鐸、11名執事、58名修士、57名修女。現任主教為[約阿希姆·弗里德里·內爾特](../Page/約阿希姆·弗里德里·內爾特.md "wikilink")。

## 歷史

1921年6月24日成立梅森教區，1979年11月15日更名為德累斯頓-梅森教區，1980年，重建[德累斯頓的](../Page/德累斯頓.md "wikilink")[宫廷主教座堂後](../Page/宫廷教堂.md "wikilink")，主教座堂也遷到那裡。

[D](../Category/德国天主教教区.md "wikilink")
[Category:德累斯顿](../Category/德累斯顿.md "wikilink")