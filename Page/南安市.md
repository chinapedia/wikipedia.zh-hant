**南安市**（），雅称**武荣**，为[中国](../Page/中華人民共和國.md "wikilink")[福建省下辖的一个](../Page/福建省.md "wikilink")[县级市](../Page/县级市.md "wikilink")，由[泉州市代管](../Page/泉州市.md "wikilink")，位于福建省东南沿海，[晋江中游](../Page/晋江_\(河流\).md "wikilink")。

面积2,036平方公里，常住人口141万多人（2010年第六次全国人口普查结果）。人口以汉族为主，此外有[畲](../Page/畲族.md "wikilink")、[满](../Page/满族.md "wikilink")、[回等少数民族](../Page/回族.md "wikilink")。南安为极具实力的[县市](../Page/县级行政区.md "wikilink")，其综合竞争力位居福建省第3位，[2011年位居全国百强县（市）第32位](../Page/中国百强县#县域经济论坛评价.md "wikilink")。2010年，南安市[GDP总量](../Page/国内生产总值.md "wikilink")480.21亿元，较上年增长13.6%，人均GDP3.2万元\[1\]。

南安是[明](../Page/明朝.md "wikilink")[清風雲人物](../Page/清朝.md "wikilink")[鄭成功](../Page/鄭成功.md "wikilink")、[洪承疇的故鄉](../Page/洪承疇.md "wikilink")。南安也属[清代](../Page/清朝.md "wikilink")[泉州三邑之一](../Page/泉州三邑.md "wikilink")。由于近代有大量南安人遷居海外，南安也被成为中国著名僑鄉。

## 历史沿革

  - [三国](../Page/三国.md "wikilink")[东吴](../Page/晋安县_\(西晋\).md "wikilink")[永安三年](../Page/永安_\(孫休\).md "wikilink")（公元260年）置[东安县](../Page/东安县_\(东吴\).md "wikilink")
  - [西晋](../Page/西晋.md "wikilink")[太康三年](../Page/太康_\(西晋\).md "wikilink")（公元282年）更名为[晋安县](../Page/晋安县_\(西晋\).md "wikilink")，属[晋安郡](../Page/晋安郡.md "wikilink")
  - [南朝](../Page/南北朝.md "wikilink")[梁更名为梁安县](../Page/南梁.md "wikilink")。[天监年间](../Page/天监.md "wikilink")，析晋安郡南部置[南安郡](../Page/南安郡_\(南梁\).md "wikilink")，下领三县：晋安、兰水（今莆田）、龙溪（今漳州北部），郡治设于晋安（今南安市丰州镇），以保持闽疆南部安定取称
  - [隋](../Page/隋朝.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年），天下废[郡](../Page/郡.md "wikilink")，置[南安县](../Page/南安县_\(隋朝\).md "wikilink")，辖地包括今莆田、晋江、惠安、同安、安溪、永春等地。
  - [唐](../Page/唐朝.md "wikilink")[武德五年](../Page/武德.md "wikilink")（622年）置丰州。[贞观元年](../Page/貞觀_\(唐朝\).md "wikilink")（公元627年）丰州并入泉州（州治今福州）。贞观九年（公元635年）再次并丰州入泉州（今福州）。[嗣圣元年](../Page/嗣圣.md "wikilink")（公元684年）分出南安、莆田、龙溪三县置武荣州，南安县城丰州为武荣州治（故南安又别称武荣）。[久视元年](../Page/久视.md "wikilink")（700年）迁治今泉州，丰州仍为南安县治
  - [五代期间曾改名晋平县](../Page/五代.md "wikilink")、晋安县、梁安县
  - 五代后南安县历属清源军（下辖今泉州、莆田）、平海军（下辖今泉州、莆田）、泉州路、泉州府
  - [清朝前期属兴泉永道](../Page/清朝.md "wikilink")，下辖莆田（兴化）、[泉州府](../Page/泉州府.md "wikilink")、永春州
  - [中华民国时期属厦门道](../Page/中华民国.md "wikilink")（辖同安、晋江、南安、金门、安溪、莆田、仙游、永春、德化、惠安、十县，驻同安）
  - 1935年4月，以厦门及鼓浪屿等7个岛屿设厦门市，撤销思明县设禾山特种区，与同安县同属第四行政督察区（辖同安、晋江、南安、金门、安溪、莆田、仙游、永春、德化、惠安、十县，驻同安），第四行政督察区驻地由永春移至泉州
  - 1937年7月，县治迁今址（即今溪美街道）
  - 1949年8月14日，解放军攻占南安县，历属[泉州专区](../Page/泉州专区.md "wikilink")（辖晋江、惠安、南安、安溪、永春、莆田、仙游、金门、同安九县，后又改名为[晋江专区](../Page/晋江专区.md "wikilink")）、[晋江专区](../Page/晋江专区.md "wikilink")（地区）、泉州市
  - 1993年，撤县设省辖县级市，由泉州市代管

## 地理

### 位置

南安南北宽82公里，东西长45公里，地理坐标为北纬24°34′30″-25°19′25″，东经118°08′30″-118°36′20″。辖境东接[鲤城区](../Page/鲤城区.md "wikilink")、[丰泽区](../Page/丰泽区.md "wikilink")、[洛江区](../Page/洛江区.md "wikilink")，东南与[晋江市毗邻](../Page/晋江市.md "wikilink")，南部与[厦门](../Page/厦门市.md "wikilink")[同安区的](../Page/同安区.md "wikilink")[大](../Page/大嶝岛.md "wikilink")、[小嶝岛及](../Page/小嶝岛.md "wikilink")[金门县隔海相望](../Page/金门县.md "wikilink")，西南与[翔安区交界](../Page/翔安区.md "wikilink")，西通[安溪县](../Page/安溪县.md "wikilink")，北连[永春县](../Page/永春县.md "wikilink")，东北与[仙游县接壤](../Page/仙游县.md "wikilink")。辖区最南为石井镇的大佰岛，最北为向阳乡的洋坪自然村，最东端是洪濑镇的大洋村，最西端是翔云乡的椒岭村，南北最大距离82千米，东西最大距离45千米。

### 地势

南安地势西北高、东南低，由中山、低山渐次过渡到丘陵台地、平原、坡麓、海滩、明显阶状倾斜。丘陵山地占全市总面积的73%，水域占6.3%，平原占20.7%。晋江上源西溪由西向东横贯中部，东溪斜贯北东，于双溪口汇合为晋江干流。南部滨海，海岸线长30多千米。

### 气候

属南亚热带海洋性季风气候，年均温度20.9℃，七月平均温度28.6℃，一月平均温度12.1℃，年降雨量1600毫米，无霜期330天。

## 行政区划

轄3個街道、21個鎮、2個鄉、1个经济开发区，共有32个社区、384个行政村，市政府驻溪美街道。

  - 街道：[溪美街道](../Page/溪美街道.md "wikilink")、[柳城街道](../Page/柳城街道_\(南安市\).md "wikilink")、[美林街道](../Page/美林街道.md "wikilink")。
  - 鎮：[官桥镇](../Page/官桥镇_\(南安市\).md "wikilink")、[省新镇](../Page/省新镇.md "wikilink")、[仑苍镇](../Page/仑苍镇.md "wikilink")、[东田镇](../Page/东田镇_\(南安市\).md "wikilink")、[英都镇](../Page/英都镇.md "wikilink")、[翔云镇](../Page/翔云镇.md "wikilink")、[金淘镇](../Page/金淘镇.md "wikilink")、[诗山镇](../Page/诗山镇.md "wikilink")、[蓬华镇](../Page/蓬华镇.md "wikilink")、[码头镇](../Page/码头镇_\(南安市\).md "wikilink")、[九都镇](../Page/九都镇_\(南安市\).md "wikilink")、[乐峰镇](../Page/乐峰镇.md "wikilink")、[罗东镇](../Page/罗东镇_\(南安市\).md "wikilink")、[梅山镇](../Page/梅山镇_\(南安市\).md "wikilink")、[洪濑镇](../Page/洪濑镇.md "wikilink")、[洪梅镇](../Page/洪梅镇_\(南安市\).md "wikilink")、[康美镇](../Page/康美镇_\(南安市\).md "wikilink")、[丰州镇](../Page/丰州镇_\(南安市\).md "wikilink")、[霞美镇](../Page/霞美镇_\(南安市\).md "wikilink")、[水头镇](../Page/水头镇_\(南安市\).md "wikilink")、[石井镇](../Page/石井镇_\(南安市\).md "wikilink")。
  - 鄉：[眉山乡](../Page/眉山乡.md "wikilink")、[向阳乡](../Page/向阳乡_\(南安市\).md "wikilink")。
  - 开发区：[南安经济开发区](../Page/南安经济开发区.md "wikilink")。

## 文化

### 方言

全市通行[闽南语](../Page/闽南语.md "wikilink")。

## 教育

  - [闽南科技学院](../Page/闽南科技学院.md "wikilink")
  - [泉州工程职业技术学院](../Page/泉州工程职业技术学院.md "wikilink")
  - [泉州师范学院诗山校区](../Page/泉州师范学院.md "wikilink")

## 歷史名人

  - [唐代](../Page/唐代.md "wikilink")[詩人](../Page/詩人.md "wikilink")[歐陽詹](../Page/歐陽詹.md "wikilink")
  - 唐末五代[雪峰義存](../Page/雪峰義存.md "wikilink")
  - [宋代](../Page/宋代.md "wikilink")[科學家](../Page/科學家.md "wikilink")[蘇頌](../Page/蘇頌.md "wikilink")
  - [明代](../Page/明代.md "wikilink")[思想家](../Page/思想家.md "wikilink")[李贄](../Page/李贄.md "wikilink")
  - 明末降清名將[洪承疇](../Page/洪承疇.md "wikilink")、[洪建家](../Page/洪建家.md "wikilink")
  - 明末海盜[鄭芝龍](../Page/鄭芝龍.md "wikilink")。
  - [南明延平王](../Page/南明.md "wikilink")[鄭成功](../Page/鄭成功.md "wikilink")
  - 當代名將[葉飛](../Page/葉飛.md "wikilink")
  - 知名爱国华侨[侯西反](../Page/侯西反.md "wikilink")、[李光前](../Page/李光前_\(慈善家\).md "wikilink")、[黄仲咸等](../Page/黄仲咸.md "wikilink")。

## 物產

境內[花崗岩儲量豐富](../Page/花崗岩.md "wikilink")，有「磐石」、「輝綠巖」等品種。農業主產[水稻](../Page/水稻.md "wikilink")、[小麥](../Page/小麥.md "wikilink")、[甘薯](../Page/甘薯_\(薯蕷屬\).md "wikilink")、[花生](../Page/花生.md "wikilink")、水產品等，特產[烏龍茶](../Page/烏龍茶.md "wikilink")、[綠茶](../Page/綠茶.md "wikilink")，盛產[龍眼](../Page/龍眼.md "wikilink")、[柑橘](../Page/柑橘.md "wikilink")、[荔枝](../Page/荔枝.md "wikilink")、[香蕉](../Page/香蕉.md "wikilink")、[楊梅等諸多](../Page/楊梅.md "wikilink")[水果](../Page/水果.md "wikilink")，有「水果之鄉」的美譽。鄉鎮經濟發達，有[建材](../Page/建材.md "wikilink")、[五金](../Page/五金件.md "wikilink")、[機械](../Page/機械.md "wikilink")、[化工](../Page/化工原料.md "wikilink")、[紡織](../Page/紡織.md "wikilink")、[食品等工業門類](../Page/食品.md "wikilink")，是全國綜合實力百強縣之一。

## 文化和旅遊

现有国家级重点文物保护单位5处、省级重点文物保护单位14处、市级重点文物保护单位60处。

金雞港是古代「[海上絲綢之路](../Page/海上絲綢之路.md "wikilink")」的起點之一。目前南安市境內有[鄭成功墓](../Page/鄭成功墓.md "wikilink")、[安平橋](../Page/安平橋.md "wikilink")（五里橋）、[九日山摩崖石刻](../Page/九日山摩崖石刻.md "wikilink")、[蔡氏古民居建築群](../Page/蔡氏古民居建築群.md "wikilink")、陀羅尼經幢、鳳山寺、雪峰寺等數十處重要古跡。

境内有[安平桥](../Page/安平桥.md "wikilink")（五里桥）、[郑成功陵墓](../Page/郑成功陵墓.md "wikilink")、[九日山摩崖石刻](../Page/九日山摩崖石刻.md "wikilink")、[蔡浅古民居建筑群等四处](../Page/蔡浅古民居建筑群.md "wikilink")[国家重点文物保护单位](../Page/国家重点文物保护单位.md "wikilink")，以及[陀罗尼经幢](../Page/陀罗尼经幢.md "wikilink")（丰州镇桃源宫内）、[龙水山](../Page/龙水山.md "wikilink")[五塔岩寺石塔](../Page/五塔岩寺.md "wikilink")（官桥镇竹口村）、[南坑古窑址](../Page/南坑古窑址.md "wikilink")（东田镇）、[延平郡王祠](../Page/延平郡王祠.md "wikilink")（石井镇鳌峰北麓）、[莲花峰石](../Page/莲花峰石.md "wikilink")（丰州镇桃源村）、[开化洞阿弥陀佛造像](../Page/开化洞阿弥陀佛造像.md "wikilink")（柳城街道祥塘村）、[波罗门式石塔](../Page/波罗门式石塔.md "wikilink")（诗山镇山二村）、[南安中宪第](../Page/南安中宪第.md "wikilink")（石井镇）等8处省级文物保护单位。南安市灵应森林公园（[灵应寺](../Page/灵应寺.md "wikilink")）为省级森林公园。南安康美杨梅山
林府元帥祖廟- 南安石馬宮
南安市的闽南传统民居营造技艺及南安英都拔拔灯被列入[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

境内拥有南安蓬华镇天柱山香草世界风景区

## 友好城市

截至2007年10月31日，南安市已经与1个国家建立了1对国际友好城市关系\[2\]。

<table>
<thead>
<tr class="header">
<th><p>友好城市</p></th>
<th><p>结好时间</p></th>
<th><p>签字地点</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/平户市.md" title="wikilink">平户市</a>（<a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/长崎县.md" title="wikilink">长崎县</a>）</p></td>
<td><p>1995年10月20日</p></td>
<td><p>平户市</p></td>
</tr>
</tbody>
</table>

## 参考文献

[泉州](../Page/category:福建县级市.md "wikilink")

[南安市](../Category/南安市.md "wikilink")
[市](../Category/泉州区县市.md "wikilink")

1.  概况相关人口与经济数据指标来源于[南安市2011年政府工作报告](http://www.nanan.gov.cn/dir_infor.aspx?id=1055)
2.