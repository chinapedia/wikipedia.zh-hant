**真犬齒獸下目**（Eucynodontia）是一個[似哺乳爬行動物](../Page/似哺乳爬行動物.md "wikilink")[演化支](../Page/演化支.md "wikilink")，屬於[合弓綱](../Page/合弓綱.md "wikilink")[獸孔目的](../Page/獸孔目.md "wikilink")[犬齒獸亞目](../Page/犬齒獸亞目.md "wikilink")，包含現代[哺乳動物的直系祖先與其旁系近親](../Page/哺乳動物.md "wikilink")。其成員可分成[肉食性與](../Page/肉食性.md "wikilink")[草食性](../Page/草食性.md "wikilink")。存在時間可能從晚[二疊紀或早](../Page/二疊紀.md "wikilink")[三疊紀](../Page/三疊紀.md "wikilink")，直到現代。關於真犬齒獸類的論述集中在晚[三疊紀到晚](../Page/三疊紀.md "wikilink")[白堊紀的原始哺乳類上](../Page/白堊紀.md "wikilink")。

## 犬頜獸科

真犬齒獸類中最早最基礎的成員是[犬頜獸](../Page/犬頜獸.md "wikilink")。這種[狼大小的掠食者幾乎分布在全世界](../Page/狼.md "wikilink")。下頜的90%是獨自承受牙齒的骨頭，稱為[齒骨](../Page/齒骨.md "wikilink")（Dentary）。牙齒經過特化，使它們有不同的作用，包括撕裂與咀嚼。[鱷魚撕裂獵物但不能咀嚼](../Page/鱷魚.md "wikilink")。牠們是有效率的掠食動物，但進食雜亂且大量耗費。犬頜獸的耳朵有一塊獨立骨頭可協助聽力([鐙骨](../Page/鐙骨.md "wikilink"))。下頜與頭骨間由稱為[關節骨](../Page/關節骨.md "wikilink")（Articular）與[方骨](../Page/方骨.md "wikilink")（Quadrate）的關節連結。

犬頜獸是[犬頜獸科中的唯一代表](../Page/犬頜獸科.md "wikilink")。然而，目前已知更多的衍生近親。

  - [犬頜獸科](../Page/犬頜獸科.md "wikilink")
      - 犬頜獸
  - Probainognathidae科
      - *Probainognathus*
      - *Lepagia*（可能）
  - 不清楚分類傾向
      - *Ecteninion*
      - *Hahnia*

## 奇尼瓜齒獸超科

更多的非哺乳真犬齒獸類被發現。主要的肉食性支系是[奇尼瓜齒獸超科](../Page/奇尼瓜齒獸超科.md "wikilink")（Chiniquodontoidea）。有些人計劃將這超科分為兩科。

### 奇尼瓜齒獸科

[奇尼瓜齒獸科](../Page/奇尼瓜齒獸科.md "wikilink")（Chiniquodontidae）的化石最主要來自[南美洲晚](../Page/南美洲.md "wikilink")[三疊紀的下層地層](../Page/三疊紀.md "wikilink")。有些難以分辯的化石(主要是牙齒)從[歐洲](../Page/歐洲.md "wikilink")[三疊紀中到晚期的地層發現](../Page/三疊紀.md "wikilink")。*Aleodon*是從非洲中[三疊紀的地層發現](../Page/三疊紀.md "wikilink")，也被歸於這科中，但是其分類還有問題。牠們的大小從迷你的*Gaumia*，到狗大小的[奇尼瓜齒獸](../Page/奇尼瓜齒獸.md "wikilink")（*Chiniquodon*）。

奇尼瓜齒獸科 Chiniquodontidae

  - [奇尼瓜齒獸](../Page/奇尼瓜齒獸.md "wikilink") *Chiniquodon*
  - ''Aleodon ''
  - ''Belesodon ''
  - ''Gaumia ''
  - *Probelesodon*

### Dromatheriidae科

[Dromatherium.jpg](https://zh.wikipedia.org/wiki/File:Dromatherium.jpg "fig:Dromatherium.jpg")
Dromatheriidae科是基於很小的牙齒所建立起的科，生存在晚[三疊紀的](../Page/三疊紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")、[北美](../Page/北美.md "wikilink")、可能還有[印度](../Page/印度.md "wikilink")。雖然化石很稀少，但是化石很像[哺乳類](../Page/哺乳類.md "wikilink")。哺乳類祖先的牙齒應該類似這科動物的牙齒，但這些化石還不足以代表人類的祖先。需要更多的化石來證明。這一科應該是個非正式的分類，而非天然分類。

Dromatheriidae科：

  - *Dromatherium*
  - [小椎獸](../Page/小椎獸.md "wikilink") ''Microconodon ''
  - *Pseudotriconodon*
  - *Tricuspes*

### Therioherpetidae科

另一個可能被包括在奇尼瓜齒獸超科裡的是Therioherpetidae科，生存在晚[三疊紀的](../Page/三疊紀.md "wikilink")[南美](../Page/南美.md "wikilink")，可能還有[歐洲](../Page/歐洲.md "wikilink")。*Therioherpeton*曾被歸類為Dromatheriidae科。也曾被歸類為[三稜齒獸科](../Page/三稜齒獸科.md "wikilink")。目前還需要更多的發現與研究。住在[南美的成員約](../Page/南美.md "wikilink")[老鼠大小](../Page/老鼠.md "wikilink")，[歐洲的](../Page/歐洲.md "wikilink")*Meurthodon*更小。

  - Therioherpetidae科：
      - *Therioherpeton*
      - *Charruodon*
      - *Meurthodon*

<!-- end list -->

  - 分類傾向不確定：
      - *Eoraetia*
      - [昆明獸](../Page/昆明獸.md "wikilink") *Kunminia*

### 三稜齒獸科

奇尼瓜齒獸超科中唯一在[三疊紀之後還存活的是](../Page/三疊紀.md "wikilink")[三稜齒獸科](../Page/三稜齒獸科.md "wikilink")（Tritheledontidae或Trithelodontidae）。牠們是小型[食蟲性生物](../Page/食蟲性.md "wikilink")，長達20公分長，而牠們的生活方式推定是極度類似第一批[哺乳類](../Page/哺乳類.md "wikilink")。這能解釋它們的滅絕。化石在晚[三疊紀的](../Page/三疊紀.md "wikilink")[南美](../Page/南美.md "wikilink")、早[侏儸紀的](../Page/侏儸紀.md "wikilink")[南美與](../Page/南美.md "wikilink")[北美發現](../Page/北美.md "wikilink")。

  - [三稜齒獸科](../Page/三稜齒獸科.md "wikilink")：
      - [三稜齒獸](../Page/三稜齒獸.md "wikilink") *Tritheledon*
      - [雙節頜獸](../Page/雙節頜獸.md "wikilink") *Diarthrognathus*
      - *Pachygenelus*
      - *Pattsia*
      - [里奧格蘭德獸](../Page/里奧格蘭德獸.md "wikilink") *Riograndia*

## Gomphodonts、三瘤齒獸科

傳統上，奇尼瓜齒獸超科的[草食性部分都被列入Gomphodonts](../Page/草食性.md "wikilink")（意為釘狀牙齒）項目中。牠們的生存在早[三疊紀到早](../Page/三疊紀.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")，化石在除了[澳洲以外的每個大陸都有發現](../Page/澳洲.md "wikilink")。牠們也被歸類於[三瘤齒獸科](../Page/三瘤齒獸科.md "wikilink")。這個分法比較像是權宜的作法，而非有系統的分類。

### 冕齒獸科

[Diademodon_mastacus.jpg](https://zh.wikipedia.org/wiki/File:Diademodon_mastacus.jpg "fig:Diademodon_mastacus.jpg")的頭顱骨，[柏林自然史博物館](../Page/柏林.md "wikilink")\]\]
這科動物中最基本的代表是[冕齒獸科](../Page/冕齒獸科.md "wikilink")（Diademodontidae）。大部份化石來自早[三疊紀的](../Page/三疊紀.md "wikilink")[南美](../Page/南美.md "wikilink")。其他來自[亞洲](../Page/亞洲.md "wikilink")，可能還有[南極洲](../Page/南極洲.md "wikilink")。

冕齒獸科 Diademodontidae：

  - [冕齒獸](../Page/冕齒獸.md "wikilink") *Diademodon*
  - *Hazhenia*
  - *Ordosiodon*
  - *Titanogomphodon*

### Trirachodontidae科

更衍生演化出的是Trirachodontidae科，生存在[非洲](../Page/非洲.md "wikilink")、[亞洲](../Page/亞洲.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、可能還有[北美](../Page/北美.md "wikilink")。有些物種類似同時代的冕齒獸科，而這分支似乎存活到中[侏儸紀](../Page/侏儸紀.md "wikilink")。Trirachodon共同生存在擁擠的地方。它們的化石，與它們居住的洞穴，在[南美發現](../Page/南美.md "wikilink")。

Trirachodontidae科：

  - *Trirachodon*
  - *Cricodon*
  - *Neotrirachodon*
  - [中國頜獸](../Page/中國頜獸.md "wikilink") *Sinognathus*

### 橫齒獸科

[三疊紀Gomphodonts中](../Page/三疊紀.md "wikilink")，最多樣性的是[橫齒獸科](../Page/橫齒獸科.md "wikilink")（Traversodontidae）。這科動物在早[三疊紀出現](../Page/三疊紀.md "wikilink")，並存活到[三疊紀結束](../Page/三疊紀.md "wikilink")。原始的代表相當小，較晚的品種可達50公分長。最近發現的化石來自於[三疊紀末的](../Page/三疊紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，發現的是牙齒，來自於類似[鼩鼱大小的動物](../Page/鼩鼱.md "wikilink")。化石以在各大洲發現，除了[澳洲與](../Page/澳洲.md "wikilink")[南極洲以外](../Page/南極洲.md "wikilink")，而最好的化石來自於[阿根廷與](../Page/阿根廷.md "wikilink")[巴西的晚](../Page/巴西.md "wikilink")[三疊紀的下層地層](../Page/三疊紀.md "wikilink")，可能是橫齒獸類的全盛時期。

橫齒獸科 Traversodontidae：

  - [橫齒獸](../Page/橫齒獸.md "wikilink")（*Traversodon*）、*Andescynodon*、*Arctotraversodon*、*Boreogomphodon*、*Colbertosaurus*、*Dadadon*、*Exaeretodon*、*Gomphodontosuchus*、*Habayia*、*Ischignathus*、[盧安瓜獸](../Page/盧安瓜獸.md "wikilink")（*Luangwa*）、[囓頜獸](../Page/囓頜獸.md "wikilink")（*Massetognathus*）、*Maubeugia*、*Menadon*、*Microscalenodon*、*Pascualgnathus*、[磚臼齒獸](../Page/磚臼齒獸.md "wikilink")（*Plinthogomphodon*）、*Rosieria*、*Rusconiodon*、*Scalenodon*、*Scalenodontoide*s、*Theropsodon*

### 三瘤齒獸科

[三瘤齒獸科可能是從橫齒獸科演化而來](../Page/三瘤齒獸科.md "wikilink")。ㄧ般認定非似哺乳類的犬齒獸類生物在[三疊紀末滅絕](../Page/三疊紀.md "wikilink")。由[三稜齒獸科可證明這是不正確的](../Page/三稜齒獸科.md "wikilink")，三稜齒獸科是食蟲性動物。[三瘤齒獸科外型類似](../Page/三瘤齒獸科.md "wikilink")[三稜齒獸科](../Page/三稜齒獸科.md "wikilink")，是草食性生物。牠們較大（可達50公分長）而且存活的更久；至少存活到早[白堊紀](../Page/白堊紀.md "wikilink")。牠們的化石顯示出牠們是穴居動物，在[科羅拉多州發先一個與遺骨大小相符合的洞穴](../Page/科羅拉多州.md "wikilink")，與足跡還有生理遺骨一起發現。*Chronoperates*是後[白堊紀證據有限的代表](../Page/白堊紀.md "wikilink")，但這被通常是為某種哺乳類。三瘤齒獸科極度類似哺乳類，1920年代以前經常被歸類於哺乳類。然而，牠們的化石保留重要的爬蟲類特徵，特別是下頜與耳部。

目前發現最早的化石起源於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")。大多數代表性品種來自於晚[三疊紀](../Page/三疊紀.md "wikilink")，這時這科生物幾乎已分布於全世界。其中的[小駝獸屬已經在](../Page/小駝獸.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")、[中國](../Page/中國.md "wikilink")、[北美發現](../Page/北美.md "wikilink")。破碎的三瘤齒獸遺骨曾經在[南極洲發現](../Page/南極洲.md "wikilink")。最近的無爭議化石來自於[西伯利亞與](../Page/西伯利亞.md "wikilink")[日本](../Page/日本.md "wikilink")。三瘤齒獸科的絕種可能與[哺乳類的](../Page/哺乳類.md "wikilink")[多瘤齒獸的興起有關](../Page/多瘤齒獸.md "wikilink")，尤其是北半球。

  - [三瘤齒獸科](../Page/三瘤齒獸科.md "wikilink")：
      - [三瘤齒獸](../Page/三瘤齒獸.md "wikilink") *Tritylodon*
      - [卞氏獸](../Page/卞氏獸.md "wikilink") *Bienotherium*
      - [似卞氏獸](../Page/似卞氏獸.md "wikilink") *Bienotheroides*
      - *Bocatherium*
      - [滇中獸](../Page/滇中獸.md "wikilink") *Dianzhongia*
      - *Dinnebitodon*
      - *Kayentatherium*
      - [祿豐獸](../Page/祿豐獸.md "wikilink") *Lufengia*
      - [小駝獸](../Page/小駝獸.md "wikilink") *Oligokyphus*
      - *Stereognathus*
      - *Xenocretosuchus*
      - [雲南獸](../Page/雲南獸.md "wikilink") *Yunnanodon*

## 可能的非哺乳類

根據較寬鬆的定義，[哺乳類開始於](../Page/哺乳類.md "wikilink")[摩爾根獸](../Page/摩爾根獸.md "wikilink")（*Morganucodon*）。真犬齒獸類中少數幾個屬被歸類於哺乳類，但應該稱為[爬蟲類](../Page/爬蟲類.md "wikilink")。[中國尖齒獸](../Page/中國尖齒獸.md "wikilink")（*Sinoconodon*）是確切的例子。它的牙齒位置與成長環境並非[哺乳類](../Page/哺乳類.md "wikilink")。同樣的例子也適用於晚[三疊紀最早期](../Page/三疊紀.md "wikilink")([卡尼階](../Page/卡尼階.md "wikilink"))岩石的*Adelobasileus*與*Gondwanadon*。*Haramiyida*也是同樣的例子。然而，身為一個例外，*Haramiyida*較為人知的是它的迷你牙齒。除非等到重要的的化石出土，*Haramiyida*的分類傾向仍有待解決中。

## 過渡性的化石

ㄧ般來說，非哺乳類的陸地脊椎動物，下頜的齒骨只有一種牙齒，與犬齒獸相比較不佔優勢。然而，[犬頜獸內耳裡只有一塊骨頭](../Page/犬頜獸.md "wikilink")，而且兩者的頜部關節是一樣的。

在哺乳類之中，除了最早期己的代表以外，[齒骨是唯一的下頜骨頭](../Page/齒骨.md "wikilink")；牙齒非常特化；內耳有三塊協助聽力的骨頭，[砧骨](../Page/砧骨.md "wikilink")、[錘骨](../Page/锤骨.md "wikilink")、[鐙骨](../Page/鐙骨.md "wikilink")；頜部關節是由[關節骨與](../Page/關節骨.md "wikilink")[方骨構成](../Page/方骨.md "wikilink")。

所有這些特徵出現在[三疊紀到](../Page/三疊紀.md "wikilink")[侏儸紀的過度性化石紀錄中](../Page/侏儸紀.md "wikilink")。非哺乳的真犬齒獸類變的更類似哺乳類，更多樣化的物種與早期的哺乳類之間只有生理結構上的層級差異。

在[三疊紀原始哺乳類之中](../Page/三疊紀.md "wikilink")，可負載牙齒的齒骨變的更具有優勢，直到下頜骨變的迷你。牙齒長得更複雜、更有效率。哺乳類的頜部關節（齒骨-方軛骨）逐漸取代非哺乳類式的下頜。內耳的骨頭仍然只有一塊，其他相關的重要變化仍在進行中；[耳蝸管](../Page/耳蝸管.md "wikilink")（*cochlear
canal*）出現，例如：[雲南獸](../Page/雲南獸.md "wikilink")（*Yunnanodon*）。

## 參考文献

  - Much of this article is derived with permission from
    [中生代真犬齒獸類](https://web.archive.org/web/20070203012013/http://home.arcor.de/ktdykes/meseucaz.htm)

[真犬齒獸下目](../Category/真犬齒獸下目.md "wikilink")