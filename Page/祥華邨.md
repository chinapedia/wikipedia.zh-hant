[Cheung_Wah_Estate_Volleyball,_Badminton_and_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Volleyball,_Badminton_and_Basketball_Court.jpg "fig:Cheung_Wah_Estate_Volleyball,_Badminton_and_Basketball_Court.jpg")
[Cheung_Wah_Estate_Football_Field.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Football_Field.jpg "fig:Cheung_Wah_Estate_Football_Field.jpg")
[Cheung_Wah_Shopping_Centre_Level_3.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Shopping_Centre_Level_3.jpg "fig:Cheung_Wah_Shopping_Centre_Level_3.jpg")
[Cheung_Wah_Market_(overlook).JPG](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Market_\(overlook\).JPG "fig:Cheung_Wah_Market_(overlook).JPG")
[Cheung_Wah_Estate_Restaurant_Stalls.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Restaurant_Stalls.jpg "fig:Cheung_Wah_Estate_Restaurant_Stalls.jpg")茶餐廳\]\]
[Cheung_Wah_Estate_Children_Play_Area.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Children_Play_Area.jpg "fig:Cheung_Wah_Estate_Children_Play_Area.jpg")
[Cheung_Wah_Estate_Fitness_Area.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Fitness_Area.jpg "fig:Cheung_Wah_Estate_Fitness_Area.jpg")
[Cheung_Wah_Estate_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Pebble_Walking_Trail.jpg "fig:Cheung_Wah_Estate_Pebble_Walking_Trail.jpg")
[Cheung_Wah_Estate_Coverway_201504.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Coverway_201504.jpg "fig:Cheung_Wah_Estate_Coverway_201504.jpg")
[Cheung_Wah_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Cheung_Wah_Estate_Car_Park.jpg "fig:Cheung_Wah_Estate_Car_Park.jpg")

**祥華邨**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，於1984年3月入伙，位於[新界](../Page/新界.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[粉嶺的中部](../Page/粉嶺.md "wikilink")、[東鐵綫路軌的東面](../Page/東鐵綫.md "wikilink")，其後於1998年在[租者置其屋計劃](../Page/租者置其屋計劃.md "wikilink")（第六期甲）把單位出售給所屬租戶，現已成立業主立案法團。

## 屋邨資料

### 樓宇

| 樓宇名稱       | 樓宇類型                               | 落成年份 |
| ---------- | ---------------------------------- | ---- |
| 祥和樓（第1座南翼） | [雙工字型](../Page/雙工字型.md "wikilink") | 1984 |
| 祥樂樓（第1座北翼） |                                    |      |
| 祥豐樓（第2座南翼） |                                    |      |
| 祥裕樓（第2座北翼） |                                    |      |
| 祥順樓（第3座北翼） |                                    |      |
| 祥景樓（第4座南翼） |                                    |      |
| 祥德樓（第4座）   | [Y2型](../Page/Y2型.md "wikilink")   |      |
| 祥智樓（第5座）   |                                    |      |
| 祥禮樓（第6座）   | [舊長型](../Page/舊長型.md "wikilink")   |      |
| 祥頌樓（第7座）   |                                    |      |
|            |                                    |      |

## 命名

祥華邨在規劃及入伙前，曾以所在地[粉嶺](../Page/粉嶺.md "wikilink")[黃崗山命名為](../Page/黃崗山_\(香港\).md "wikilink")**黃崗山邨**，但因被聯想與1910年[廣州起義](../Page/廣州起義.md "wikilink")（俗稱黃花崗起義）及[黃花崗七十二烈士陵園有點關係](../Page/黃花崗七十二烈士陵園.md "wikilink")，故被更名為「祥華邨」。

而祥華邨一名同樣具有典故，根據[饒玖才](../Page/饒玖才.md "wikilink")《香港地名探索》一書表示：「粉嶺有公共屋邨名曰祥華邨，因其所在早年建有[藏霞精舍](../Page/藏霞精舍.md "wikilink")，用藏霞二字諧音祥華為新邨名。」藏霞精舍為一位處粉嶺區之[道教](../Page/道教.md "wikilink")[先天道](../Page/先天道.md "wikilink")[道觀](../Page/道觀.md "wikilink")。

## 教育及福利設施

### 幼稚園

  - [基督教香港信義會祥華幼稚園](http://www.elchkcw.edu.hk)（1984年創辦）（位於祥裕樓地下102-107室）
  - [基督教香港信義會祥華幼稚園](http://www.elchkcw.edu.hk)（1984年創辦）（位於祥豐樓地下101-108室及113-115室）

### 專上及職業教肓

  - [明愛社區書院](../Page/明愛社區書院.md "wikilink") - 粉嶺 （位於一號停車場三樓）

### 家長資源中心

  - [協康會粉嶺家長資源中心](https://www.heephong.org/center/detail/fanling-parents-resource-centre)（位於祥智樓B翼地下）

## 「祥華」康樂設施

[區域市政局於](../Page/區域市政局.md "wikilink")[藏霞精舍外旁建饒富書卷文化氣息之](../Page/藏霞精舍.md "wikilink")[園林公園乙座](../Page/園林.md "wikilink")，與[盆栽園藝景緻特色之](../Page/盆栽園藝.md "wikilink")[東華三院李嘉誠中學連仨中華傳統者合為一體也](../Page/東華三院李嘉誠中學.md "wikilink")，名曰[粉嶺康樂公園](../Page/粉嶺康樂公園.md "wikilink")，達人傑地靈之果。

### 商場

祥華邨內有一商場，樓高三層，內有超級市場、酒樓、街市等設施。領展接管祥華商場後，已於2013年對祥華商場進行提升工程。

Cheung Wah Shopping Centre Level 2.jpg|祥華商場2樓 Cheung Wah Shopping Centre
Ground Level.jpg|祥華商場地下內觀 Cheung Wah Shopping Centre Ground Level
Outside.jpg|祥華商場地下外觀（2017年） Cheung Wah Estate Shopping
Centre.JPG|祥華商場地下外觀（2014年） Cheung Wah Market.JPG|祥華街市西面

### 停車場

祥華邨內共有兩個多層停車場，分別位於祥華村商場內及祥順樓、祥景樓側。停車場的管理由領展管理有限公司委派。

### 其他設施

Cheung Wah Estate Children Play Area (2).jpg|以火車為題的遊樂場 Cheung Wah Estate
Children Play Area (3).jpg|以[奧運項目為題的遊樂場](../Page/奧運.md "wikilink")
Cheung Wah Estate Children Play Area (4).jpg|以天氣為題的遊樂場 Cheung Wah Estate
Fitness Area (2).jpg|健體區（2） Cheung Wah Estate Bicycle Parking
Area.jpg|單車停泊處

## 天價換水喉工程

2015年，粉嶺祥華邨大業主房屋署建議使用4,700萬儲備基金更換屋苑地底水管工程，於同年6月通過有關工程，唯小業主指造價太貴及懷疑有圍標之嫌。業主先在2015年8月16舉行特別業主大會，通過「暫停工程」決議，再於同月25日另一個特別業主大會通過「重選法團」決議。但在8月25日同一個特別業主大會另一個議程「取消工程」在房署代表投下反對票下，以56,141對101,215業權份數否決此決議，意味工程將會保持在「暫停」階段。

小業主不滿房屋署「忽然」投票，違反房署保持中立的決定。房署在會上指，由於工程已經簽約，若違約小業主及房署或會面臨龐大索償，故在保障房署利益前提下，房署投反對票。\[1\]

## 居民

  - [劉國勳](../Page/劉國勳.md "wikilink")：香港立法會議員、北區區議員，曾居於該邨\[2\]

## 區議會選區及區議員

祥華邨全邨所有樓宇的[區議會選區](../Page/香港區議會.md "wikilink")，均屬於[北區](../Page/北區區議會.md "wikilink")[祥華選區](../Page/祥華_\(選區\).md "wikilink")（N03），而祥華選區的現任[區議員為](../Page/區議員.md "wikilink")[民主黨成員](../Page/民主黨_\(香港\).md "wikilink")[陳旭明](../Page/陳旭明_\(政治人物\).md "wikilink")。

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{東鐵綫色彩}}">█</font><a href="../Page/東鐵綫.md" title="wikilink">東鐵綫</a>：<a href="../Page/粉嶺站.md" title="wikilink">粉嶺站</a></li>
</ul>
<dl>
<dt><a href="../Page/祥華巴士總站.md" title="wikilink">祥華巴士總站</a></dt>

</dl>
<dl>
<dt><a href="../Page/新運路.md" title="wikilink">新運路</a></dt>

</dl>
<dl>
<dt><a href="../Page/馬會路.md" title="wikilink">馬會路</a><br />
<a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/上水.md" title="wikilink">上水至</a><a href="../Page/旺角.md" title="wikilink">旺角線</a> (通宵服務)[3]</li>
<li>上水至<a href="../Page/青山道.md" title="wikilink">青山道線</a>[4]</li>
<li>上水至<a href="../Page/佐敦道.md" title="wikilink">佐敦道線</a> (24小時服務)[5]</li>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至</a><a href="../Page/落馬洲.md" title="wikilink">落馬洲線</a>[6]</li>
<li>上水至<a href="../Page/銅鑼灣.md" title="wikilink">銅鑼灣線</a> (晚上至通宵服務)[7]</li>
</ul>
<dl>
<dt><a href="../Page/沙頭角公路.md" title="wikilink">沙頭角公路</a></dt>

</dl>
<dl>
<dt><a href="../Page/粉嶺車站路.md" title="wikilink">粉嶺車站路</a></dt>

</dl>
<dl>
<dt><a href="../Page/百和路.md" title="wikilink">百和路</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  - [房屋署祥華邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2776)
  - [公共屋邨原擬命名](http://www.hk-place.com/db.php?post=d005003)

[en:Public housing estates in Fanling\#Cheung Wah
Estate](../Page/en:Public_housing_estates_in_Fanling#Cheung_Wah_Estate.md "wikilink")

[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:租者置其屋計劃屋邨](../Category/租者置其屋計劃屋邨.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:香港使用中央石油氣的屋苑](../Category/香港使用中央石油氣的屋苑.md "wikilink")

1.  [祥華邨動用半億儲備換喉 房署擁9萬業權票通過繼續工程
    蘋果日報 2015-08-25](http://hk.apple.nextmedia.com/realtime/news/20150825/54131020)
2.  [天生鬈毛　劉國勳好煩惱](http://hk.apple.nextmedia.com/news/art/20160503/19595962)
3.  [旺角亞皆老街　—　大埔及上水](http://www.16seats.net/chi/rmb/r_kn73.html)
4.  [青山道欽州街　—　大埔及上水](http://www.16seats.net/chi/rmb/r_kn71.html)
5.  [上水及大埔　＞　佐敦道](http://www.16seats.net/chi/rmb/r_kn70.html)
6.  [觀塘同仁街　—　落馬洲](http://www.16seats.net/chi/rmb/r_kn77.html)
7.  [銅鑼灣鵝頸橋　—　大埔及上水](http://www.16seats.net/chi/rmb/r_hn73.html)