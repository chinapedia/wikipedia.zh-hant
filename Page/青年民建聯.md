**青年民建聯**（Young
DAB），簡稱**青民**，是[香港政黨](../Page/香港.md "wikilink")[民主建港協進聯盟](../Page/民主建港協進聯盟.md "wikilink")（民建聯）的[青年翼](../Page/青年翼.md "wikilink")。

民建聯規定：凡加入該黨而年齡在三十五歲以下的會員，均自動成為青年民建聯會員。

## 簡介

青年民建聯原為民建聯屬下的青年小組。2004年6月，民建聯中央委員會決定將原有之「青年小組」升格為直屬聯盟常委的「青年民建聯」，由青年民建聯委員會負責統籌民建聯三十五歲或以下青年會員的工作。

青年民建聯成立之初，由[張國鈞出任主席](../Page/張國鈞.md "wikilink")，[黃大仙區議會議員](../Page/黃大仙區議會.md "wikilink")[黎榮浩及](../Page/黎榮浩.md "wikilink")[荃灣區議會議員](../Page/荃灣區議會.md "wikilink")[陳恆鑌出任副主席](../Page/陳恆鑌.md "wikilink")。由民建聯常委[蘇錦樑出任總監](../Page/蘇錦樑.md "wikilink")，[立法會議員](../Page/香港立法會.md "wikilink")[劉江華出任副總監](../Page/劉江華.md "wikilink")。由第二屆開始，青年民建聯委員會任期屆數設為兩年一屆，設有主席、副主席及委員職位，主席透過民建聯中委會委任，副主席交由青年民建聯委員會成員互選產生。由第三屆開始，為強化青年民建聯與各個界別的深入聯繫，青年民建聯副主席人數由4名增加至5名，並以之設為恆常化。直到第六屆青年民建聯委員會之成立，新增設秘書長及副秘書長一職，青年民建聯委員會成員分工更具明顯，5名副主席分別專責地區聯絡、政策研究、學校聯絡、工商專業及會員義工等工作項目，新增秘書長及副秘書長之職位，主要負責統籌整個青年民建聯委員會秘書處的工作，涉及各「行政」、「組織」及「文宣」的工作。及後，第七屆青年民建聯委員會的成員架構及人數更具豐富，青年民建聯副秘書長職位由原來2名增加至6名，除了延續過往為青年人發聲的宗旨及為香港各界青年搭建平台，讓青年人可以參與其中並議政論政外，第七屆青年民建聯委員會更將青年工作深入全港十八區，將青年民建聯的組織建設力量全面深入地區並設有青民辯論隊。全面準確落實民建聯深耕細作的地區工作理念及強化青年人參政、議政及論政的政策論述能力。青年民建聯更新增FACEBOOK專頁，由第七屆青年民建聯委員會開始，各項青年民建聯工作更具細化及專業到位。

## 主要成員

青年民建聯成立最初：

  - 主席：[張國鈞](../Page/張國鈞.md "wikilink")
  - 副主席：[黎榮浩](../Page/黎榮浩.md "wikilink")，[黃大仙區議會議員](../Page/黃大仙區議會.md "wikilink")；[陳恆鑌](../Page/陳恆鑌.md "wikilink")，[荃灣區議會議員](../Page/荃灣區議會.md "wikilink")
  - 總監：[蘇錦樑](../Page/蘇錦樑.md "wikilink")，民建聯常委
  - 副總監：[劉江華](../Page/劉江華.md "wikilink")，前[立法會議員](../Page/香港立法會.md "wikilink")

## 活動

  - 青民足球隊
  - 青民辯論隊
  - 青民籃球隊
  - 青年眼講座
  - 2011第一屆兩岸四地青年高峰會
  - 2011青民武廣基建考察團
  - 2010青民上海世博考察團
  - 2010青民惠州省運會考察團
  - 2010青年房屋政策問卷調查
  - 香港學生暑期實習計劃
  - 青民到民主黨總部促請支持政改
  - 青民專業司儀培訓班
  - 青民逢周一在星島日報的「青民眼」專欄

## 現有成員

### 第七屆青年民建聯委員會成員（2017-2019）

  - 總　監：[周浩鼎](../Page/周浩鼎.md "wikilink")
  - 主　席：[顏汶羽](../Page/顏汶羽.md "wikilink")
  - 副主席：[關浩洋](../Page/關浩洋.md "wikilink")、[招文亮](../Page/招文亮.md "wikilink")、[林琳](../Page/林琳.md "wikilink")、[張思穎](../Page/張思穎.md "wikilink")、[胡綽謙](../Page/胡綽謙.md "wikilink")
  - 秘書長：[李均強](../Page/李均強.md "wikilink")
  - 副秘書長：[陳壇丹](../Page/陳壇丹.md "wikilink")、[植潔鈴](../Page/植潔鈴.md "wikilink")、[蕭煒忠](../Page/蕭煒忠.md "wikilink")、[賴嘉汶](../Page/賴嘉汶.md "wikilink")、[鍾秀賢](../Page/鍾秀賢.md "wikilink")、[鍾健峰](../Page/鍾健峰.md "wikilink")
  - 委員：[方嘉偉](../Page/方嘉偉.md "wikilink")、[司徒駿軒](../Page/司徒駿軒.md "wikilink")、[李國偉](../Page/李國偉.md "wikilink")、[周福澄](../Page/周福澄.md "wikilink")、[林政良](../Page/林政良.md "wikilink")、[梁小碩](../Page/梁小碩.md "wikilink")、[梁展鏗](../Page/梁展鏗.md "wikilink")、[郭詠健](../Page/郭詠健.md "wikilink")、[陳敬維](../Page/陳敬維.md "wikilink")、[傅紫曦](../Page/傅紫曦.md "wikilink")、[曾柏淇](../Page/曾柏淇.md "wikilink")、[黃國偉](../Page/黃國偉.md "wikilink")、[葉文斌](../Page/葉文斌.md "wikilink")、[劉碧堯](../Page/劉碧堯.md "wikilink")、[鄭卓賢](../Page/鄭卓賢.md "wikilink")、[鄺星宇](../Page/鄺星宇.md "wikilink")

### 第六屆青年民建聯委員會成員（2015-2017）

  - 總　監：[周浩鼎](../Page/周浩鼎.md "wikilink")
  - 主　席：[顏汶羽](../Page/顏汶羽.md "wikilink")
  - 副主席：[關浩洋](../Page/關浩洋.md "wikilink")、[招文亮](../Page/招文亮.md "wikilink")、[林琳](../Page/林琳.md "wikilink")、[蕭震然](../Page/蕭震然.md "wikilink")、[張思穎](../Page/張思穎.md "wikilink")
  - 秘書長：[羅崑](../Page/羅崑.md "wikilink")
  - 副秘書長：[丘健和](../Page/丘健和.md "wikilink")、[杜礎圻](../Page/杜礎圻.md "wikilink")
  - 委　員：[李均強](../Page/李均強.md "wikilink")、[李國偉](../Page/李國偉.md "wikilink")、[胡綽謙](../Page/胡綽謙.md "wikilink")、[甄健宇](../Page/甄健宇.md "wikilink")、[鄺星宇](../Page/鄺星宇.md "wikilink")、[譚肇卓](../Page/譚肇卓.md "wikilink")、[劉明藝](../Page/劉明藝.md "wikilink")、[郭詠健](../Page/郭詠健.md "wikilink")、[劉雪盈](../Page/劉雪盈.md "wikilink")、[劉鎮海](../Page/劉鎮海.md "wikilink")、[黃進昇](../Page/黃進昇.md "wikilink")、[葉文斌](../Page/葉文斌.md "wikilink")、[黃冰芬](../Page/黃冰芬.md "wikilink")、[潘卓斌](../Page/潘卓斌.md "wikilink")、[紀浩然](../Page/紀浩然.md "wikilink")、[李澄幸](../Page/李澄幸.md "wikilink")、[楊鎮華](../Page/楊鎮華.md "wikilink")、[郭芙蓉](../Page/郭芙蓉.md "wikilink")

### 第五屆青年民建聯委員會成員（2013-2015）

  - 總　監：[張國鈞](../Page/張國鈞.md "wikilink")
  - 主　席：[周浩鼎](../Page/周浩鼎.md "wikilink")
  - 副主席：[張思晉](../Page/張思晉.md "wikilink")、[陳威雄](../Page/陳威雄.md "wikilink")、[李世榮](../Page/李世榮.md "wikilink")、[關浩洋](../Page/關浩洋.md "wikilink")、[丁彥文](../Page/丁彥文.md "wikilink")
  - 委　員：[陳智偉](../Page/陳智偉.md "wikilink")、[張思穎](../Page/張思穎.md "wikilink")、[招文亮](../Page/招文亮.md "wikilink")、[郭芙蓉](../Page/郭芙蓉.md "wikilink")、[林琳](../Page/林琳.md "wikilink")、[羅崑](../Page/羅崑.md "wikilink")、[李國偉](../Page/李國偉.md "wikilink")、[顏汶羽](../Page/顏汶羽.md "wikilink")、[潘志輝](../Page/潘志輝.md "wikilink")、[蘇晁鋒](../Page/蘇晁鋒.md "wikilink")、[杜礎圻](../Page/杜礎圻.md "wikilink")、[蔡東洲](../Page/蔡東洲.md "wikilink")、[黃可欣](../Page/黃可欣.md "wikilink")、[黃冰芬](../Page/黃冰芬.md "wikilink")、[甄健宇](../Page/甄健宇.md "wikilink")、[丘健和](../Page/丘健和.md "wikilink")

### 第四屆青年民建聯委員會成員（2011-2013）

  - 總　監：張國鈞
  - 主　席：周浩鼎
  - 副主席：張思晉、[楊鎮華](../Page/楊鎮華.md "wikilink")、[葉傲冬](../Page/葉傲冬.md "wikilink")、[鄭泳舜](../Page/鄭泳舜.md "wikilink")、陳威雄
  - 委　員：[朱兆麟](../Page/朱兆麟.md "wikilink")、李世榮、顏汶羽、[梁家麒](../Page/梁家麒.md "wikilink")、[莊澤權](../Page/莊澤權.md "wikilink")、關浩洋、[陳文豪](../Page/陳文豪.md "wikilink")、郭芙蓉、陳智偉、[曾錦銓](../Page/曾錦銓.md "wikilink")、[葉文斌](../Page/葉文斌.md "wikilink")、[廖佩儀](../Page/廖佩儀.md "wikilink")、丁彥文、[潘卓斌](../Page/潘卓斌.md "wikilink")、羅崑、潘志輝、黃冰芬

### 第三屆青年民建聯委員會成員（2009-2011）

  - 總　監：張國鈞
  - 主　席：周浩鼎
  - 副主席：張思晉、楊鎮華、葉傲冬、鄭泳舜、朱兆麟
  - 委　員：陳威雄、李世榮、[季霆剛](../Page/季霆剛.md "wikilink")、梁家麒、莊澤權、[趙鈺銘](../Page/趙鈺銘.md "wikilink")、陳文豪、[陳文偉](../Page/陳文偉.md "wikilink")、陳智偉、曾錦銓、葉文斌、廖佩儀、潘卓斌、羅崑

### 第二屆青年民建聯委員會成員（2007-2009）

  - 總　監：[蘇錦樑](../Page/蘇錦樑.md "wikilink")
  - 主　席：張國鈞
  - 副主席：[陳恒鑌](../Page/陳恒鑌.md "wikilink")、[黎榮浩](../Page/黎榮浩.md "wikilink")、周浩鼎、[徐英偉](../Page/徐英偉.md "wikilink")
  - 委　員：[王宏康](../Page/王宏康.md "wikilink")、[周志威](../Page/周志威.md "wikilink")、[區永佳](../Page/區永佳.md "wikilink")、張思晉、[陳筆](../Page/陳筆.md "wikilink")、陳文豪、[陳思敏](../Page/陳思敏.md "wikilink")、[陳學鋒](../Page/陳學鋒.md "wikilink")、[陸偉傑](../Page/陸偉傑.md "wikilink")、[黃才立](../Page/黃才立.md "wikilink")、[黃家維](../Page/黃家維.md "wikilink")、[趙志豪](../Page/趙志豪.md "wikilink")、[劉國勳](../Page/劉國勳.md "wikilink")、[張瑞鋒](../Page/張瑞鋒.md "wikilink")、[劉龍飛](../Page/劉龍飛.md "wikilink")

### 第一屆青年民建聯委員會成員（2004-2007）

  - 總　監：蘇錦樑
  - 主　席：張國鈞
  - 副主席：陳恒鑌、黎榮浩
  - 委　員：陸偉傑、周志威、[謝興](../Page/謝興.md "wikilink")、劉龍飛、張瑞鋒、周浩鼎、陳思敏、陳筆、[梁子穎](../Page/梁子穎.md "wikilink")、趙志豪、[梁嘉銘](../Page/梁嘉銘.md "wikilink")

## 外部連結

  - [青年民建聯 官方網頁](http://www.youngdab.org.hk/)
  - [facebook 連結](http://www.facebook.com/youngdab/)

[\*](../Category/民主建港協進聯盟.md "wikilink")