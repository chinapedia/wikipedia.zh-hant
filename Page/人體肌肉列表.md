[1105_Anterior_and_Posterior_Views_of_Muscles.jpg](https://zh.wikipedia.org/wiki/File:1105_Anterior_and_Posterior_Views_of_Muscles.jpg "fig:1105_Anterior_and_Posterior_Views_of_Muscles.jpg")

[Skeletal_muscles_homo_sapiens.JPG](https://zh.wikipedia.org/wiki/File:Skeletal_muscles_homo_sapiens.JPG "fig:Skeletal_muscles_homo_sapiens.JPG")主要[肌肉圖片](../Page/肌肉.md "wikilink")'''

|                                             |                                           |
| ------------------------------------------- | ----------------------------------------- |
| 1\. [枕額肌](../Page/枕額肌.md "wikilink")        | 2\. [顳肌](../Page/顳肌.md "wikilink")        |
| 3\. [眼輪匝肌](../Page/眼輪匝肌.md "wikilink")      | 4\. [上脣舉肌](../Page/上脣舉肌.md "wikilink")    |
| 5\. [嚼肌](../Page/嚼肌.md "wikilink")          | 6\. [胸鎖乳突肌](../Page/胸鎖乳突肌.md "wikilink")  |
| 7\. [口輪匝肌](../Page/口輪匝肌.md "wikilink")      | 8\. [三角肌](../Page/三角肌.md "wikilink")      |
| 9\. [斜方肌](../Page/斜方肌.md "wikilink")        | 10\. [胸大肌](../Page/胸大肌.md "wikilink")     |
| 11\. [背闊肌](../Page/背闊肌.md "wikilink")       | 12\. [肱三頭肌](../Page/肱三頭肌.md "wikilink")   |
| 13\. [肱二頭肌](../Page/肱二頭肌.md "wikilink")     | 14\. [前鋸肌](../Page/前鋸肌.md "wikilink")     |
| 15\. [腹直肌](../Page/腹直肌.md "wikilink")       | 16\. [腹外斜肌](../Page/腹外斜肌.md "wikilink")   |
| 17\. [闊筋膜張肌](../Page/闊筋膜張肌.md "wikilink")   | 18\. [股直肌](../Page/股直肌.md "wikilink")     |
| 19\. [臀大肌](../Page/臀大肌.md "wikilink")       | 20\. [旋前方肌](../Page/旋前方肌.md "wikilink")   |
| 21\. [尺腕掌側韌帶](../Page/尺腕掌側韌帶.md "wikilink") | 22\. [指深屈肌](../Page/指深屈肌.md "wikilink")   |
| 23\. [縫匠肌](../Page/縫匠肌.md "wikilink")       | 24\. [股四頭肌](../Page/股四頭肌.md "wikilink")   |
| 25\. [股後肌](../Page/股後肌.md "wikilink")       | 26\. [腓腸肌](../Page/腓腸肌.md "wikilink")     |
| 27\. [脛骨前肌](../Page/脛骨前肌.md "wikilink")     | 28\. [比目魚肌](../Page/比目魚肌.md "wikilink")   |
| 29\. [伸肌下支持帶](../Page/伸肌下支持帶.md "wikilink") | 30\. [腓骨第三肌](../Page/腓骨第三肌.md "wikilink") |
|                                             |                                           |

\]\]
這裡是所有人體的肌肉列表，正常人體大約有650條[骨骼肌](../Page/骨骼肌.md "wikilink")。然而正確的肌肉數目是很難肯定的，因為不同來源的肌肉會分別被聚集成不同的肌肉束。

## 頭部和頸部的肌肉

[頭和頸部的](../Page/頭.md "wikilink")[肌肉在人體的肌肉中發揮著重要的作用](../Page/肌肉.md "wikilink")，它們支撐著人體的[頭部](../Page/頭部.md "wikilink")，保護著人體重要的[感覺器官](../Page/感覺器官.md "wikilink")。人體頭部的肌肉能夠使人做出各種臉部表情。人體頸部的肌肉有很多條，它們支撐著人體的頭部，並使頭部做出各種動作。

### 頭部的肌肉

頭部的肌肉可以分為[面肌和](../Page/面肌.md "wikilink")[咀嚼肌](../Page/咀嚼肌.md "wikilink")。其中面肌為扁又薄的皮肌，位置表淺，大多起自於[頭顱的不同部位](../Page/頭顱.md "wikilink")，止於臉部[皮膚](../Page/皮膚.md "wikilink")。人類的面肌較其他動物發達，這與人類[大腦](../Page/大腦.md "wikilink")[皮質的高度進化](../Page/皮質.md "wikilink")、思維和語言活動有關。人類的咀嚼肌主要包括[顳肌](../Page/顳肌.md "wikilink")、[咬肌](../Page/咬肌.md "wikilink")……等肌肉，它們分布於[下頜關節周圍](../Page/下頜關節.md "wikilink")，收縮時使[下頜骨運動](../Page/下頜骨.md "wikilink")，參與[咀嚼](../Page/咀嚼.md "wikilink")。

### 頭部肌肉列表

下表列出了人體[頭部所有的肌肉](../Page/頭部.md "wikilink")，底色為白色的是[面肌](../Page/面肌.md "wikilink")，底色為紫色的是[咀嚼肌](../Page/咀嚼肌.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>頭部位置</p></th>
<th><p>肌肉的種類</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/顱頂.md" title="wikilink">顱頂</a></p></td>
<td><p><a href="../Page/枕額肌.md" title="wikilink">枕額肌</a>（<a href="../Page/枕肌.md" title="wikilink">枕肌</a>、<a href="../Page/額肌.md" title="wikilink">額肌</a>）。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/眼瞼.md" title="wikilink">眼瞼</a></p></td>
<td><p><a href="../Page/眼輪匝肌.md" title="wikilink">眼輪匝肌</a>、<a href="../Page/皺眉肌.md" title="wikilink">皺眉肌</a>、<a href="../Page/降眉肌.md" title="wikilink">降眉肌</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鼻子.md" title="wikilink">鼻子</a></p></td>
<td><p><a href="../Page/鼻錐肌.md" title="wikilink">鼻錐肌</a>、<a href="../Page/鼻肌.md" title="wikilink">鼻肌</a>、<a href="../Page/降鼻隔肌.md" title="wikilink">降鼻隔肌</a>、<a href="../Page/後鼻張肌.md" title="wikilink">後鼻張肌</a>、<a href="../Page/前鼻張肌.md" title="wikilink">前鼻張肌</a>、<a href="../Page/鼻脣舉肌.md" title="wikilink">鼻脣舉肌</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/口腔.md" title="wikilink">口部</a></p></td>
<td><p><a href="../Page/上脣舉肌.md" title="wikilink">上脣舉肌</a>、<a href="../Page/口角舉肌.md" title="wikilink">口角舉肌</a>、<a href="../Page/顴肌.md" title="wikilink">顴肌</a>（<a href="../Page/大顴肌.md" title="wikilink">大顴肌</a>、<a href="../Page/小顴肌.md" title="wikilink">小顴肌</a>）、<a href="../Page/頦肌.md" title="wikilink">頦肌</a>、<a href="../Page/下脣掣肌.md" title="wikilink">下脣掣肌</a>、<a href="../Page/口角掣肌.md" title="wikilink">口角掣肌</a>、<a href="../Page/頰肌.md" title="wikilink">頰肌</a>、<a href="../Page/口輪匝肌.md" title="wikilink">口輪匝肌</a>、<a href="../Page/笑肌.md" title="wikilink">笑肌</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/咀嚼肌.md" title="wikilink">咀嚼肌</a></p></td>
<td><p><a href="../Page/嚼肌.md" title="wikilink">嚼肌</a>、<a href="../Page/顳肌.md" title="wikilink">顳肌</a>、<a href="../Page/翼肌.md" title="wikilink">翼肌</a>（<a href="../Page/外翼肌.md" title="wikilink">外翼肌</a>、<a href="../Page/內翼肌.md" title="wikilink">內翼肌</a>）。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/眼睛.md" title="wikilink">眼睛</a></p></td>
<td><p><a href="../Page/眼外肌.md" title="wikilink">眼外肌</a>：<a href="../Page/上瞼板肌.md" title="wikilink">上瞼板肌</a>、<a href="../Page/提上瞼肌.md" title="wikilink">提上瞼肌</a>、<a href="../Page/眼直肌.md" title="wikilink">眼直肌</a>（<a href="../Page/上直肌.md" title="wikilink">上直肌</a>、<a href="../Page/下直肌.md" title="wikilink">下直肌</a>、<a href="../Page/內直肌.md" title="wikilink">內直肌</a>、<a href="../Page/外直肌.md" title="wikilink">外直肌</a>）、<a href="../Page/眼斜肌.md" title="wikilink">眼斜肌</a>（<a href="../Page/上斜肌.md" title="wikilink">上斜肌</a>、<a href="../Page/下斜肌.md" title="wikilink">下斜肌</a>）。<br />
<a href="../Page/人眼.md" title="wikilink">眼內肌</a>：<a href="../Page/睫狀肌.md" title="wikilink">睫狀肌</a>、<a href="../Page/瞳孔開大肌.md" title="wikilink">瞳孔開大肌</a>、<a href="../Page/瞳孔括約肌.md" title="wikilink">瞳孔括約肌</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/耳朵.md" title="wikilink">耳朵</a></p></td>
<td><p><a href="../Page/耳肌.md" title="wikilink">耳肌</a>、<a href="../Page/鐙骨肌.md" title="wikilink">鐙骨肌</a>、<a href="../Page/鼓膜張肌.md" title="wikilink">鼓膜張肌</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/喉嚨.md" title="wikilink">喉肌</a></p></td>
<td><p><a href="../Page/環盾肌.md" title="wikilink">環盾肌</a>、<a href="../Page/後環飄肌.md" title="wikilink">後環飄肌</a>、<a href="../Page/側環飄肌.md" title="wikilink">側環飄肌</a>、<a href="../Page/飄肌.md" title="wikilink">飄肌</a>、<a href="../Page/甲杓肌.md" title="wikilink">甲杓肌</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/舌頭.md" title="wikilink">舌頭</a></p></td>
<td><p>舌體外：（<a href="../Page/頦肌.md" title="wikilink">頦肌</a>、<a href="../Page/舌骨舌肌.md" title="wikilink">舌骨舌肌</a>、<a href="../Page/小角舌肌.md" title="wikilink">小角舌肌</a>、<a href="../Page/莖突舌肌.md" title="wikilink">莖突舌肌</a>）。<br />
舌體內：（<a href="../Page/上舌縱肌.md" title="wikilink">上舌縱肌</a>、<a href="../Page/下舌縱肌.md" title="wikilink">下舌縱肌</a>、<a href="../Page/舌橫肌.md" title="wikilink">舌橫肌</a>、<a href="../Page/舌垂直肌.md" title="wikilink">舌垂直肌</a>）。<br />
</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/顎部.md" title="wikilink">顎部</a></p></td>
<td><p><a href="../Page/軟顎舉肌.md" title="wikilink">軟顎舉肌</a>、<a href="../Page/顎帆張肌.md" title="wikilink">顎帆張肌</a>、<a href="../Page/懸雍垂肌.md" title="wikilink">懸雍垂肌</a>、<a href="../Page/顎舌肌.md" title="wikilink">顎舌肌</a>、<a href="../Page/顎咽肌.md" title="wikilink">顎咽肌</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/咽部.md" title="wikilink">咽部</a></p></td>
<td><p><a href="../Page/咽縮肌.md" title="wikilink">咽縮肌</a>（<a href="../Page/下咽縮肌.md" title="wikilink">下咽縮肌</a>、<a href="../Page/中咽縮肌.md" title="wikilink">中咽縮肌</a>、<a href="../Page/上咽縮肌.md" title="wikilink">上咽縮肌</a>。）、<a href="../Page/莖咽肌.md" title="wikilink">莖咽肌</a>、<a href="../Page/耳咽管肌.md" title="wikilink">耳咽管肌</a>。</p></td>
</tr>
</tbody>
</table>

### 頸部的肌肉

[頸部位於](../Page/頸部.md "wikilink")[頭部](../Page/頭部.md "wikilink")、[胸部和](../Page/胸部.md "wikilink")[上肢之間](../Page/上肢.md "wikilink")。頸部的[肌肉主要由前方的](../Page/肌肉.md "wikilink")[舌骨下肌群](../Page/舌骨下肌群.md "wikilink")，外側的[胸鎖乳突肌](../Page/胸鎖乳突肌.md "wikilink")，和後方的[斜角肌群組成](../Page/斜角肌群.md "wikilink")。其中胸鎖乳突肌位於頸部兩側，當胸鎖乳突肌一側收縮時，它能夠使頭部左右擺動，當胸鎖乳突肌兩側同時收縮時，能夠使人的頭部後仰。[斜角肌群位於](../Page/斜角肌群.md "wikilink")[脊柱頸段的兩側](../Page/脊柱.md "wikilink")，包括[前斜角肌](../Page/前斜角肌.md "wikilink")、[中斜角肌和](../Page/中斜角肌.md "wikilink")[後斜角肌](../Page/後斜角肌.md "wikilink")。

### 頸部肌肉列表

下表列出了人體[頸部所有的肌肉](../Page/頸部.md "wikilink")。

| 頸部位置                              | 肌肉的種類                                                                                                                                          |
| --------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| 淺頸部                               | [闊肌](../Page/闊肌.md "wikilink")。                                                                                                                |
| 側頸部                               | [胸鎖乳突肌](../Page/胸鎖乳突肌.md "wikilink")。                                                                                                          |
| [舌骨上部](../Page/舌骨.md "wikilink")  | [二腹肌](../Page/二腹肌.md "wikilink")、[莖突舌骨肌](../Page/莖突舌骨肌.md "wikilink")、[下頜舌骨肌](../Page/下頜舌骨肌.md "wikilink")、[頦舌骨肌](../Page/頦舌骨肌.md "wikilink")。 |
| [舌骨下部](../Page/舌骨.md "wikilink")  | [胸舌骨肌](../Page/胸舌骨肌.md "wikilink")、[胸盾肌](../Page/胸盾肌.md "wikilink")、[盾舌肌](../Page/盾舌肌.md "wikilink")、[肩胛舌骨肌](../Page/肩胛舌骨肌.md "wikilink")。     |
| [脊椎](../Page/脊椎.md "wikilink")⇒脊前 | [頸長肌](../Page/頸長肌.md "wikilink")、[頭長肌](../Page/頭長肌.md "wikilink")、[頭前直肌](../Page/頭前直肌.md "wikilink")、[頭側直肌](../Page/頭側直肌.md "wikilink")。       |
| [脊椎](../Page/脊椎.md "wikilink")⇒脊後 | [前斜角肌](../Page/前斜角肌.md "wikilink")、[中斜角肌](../Page/中斜角肌.md "wikilink")、[後斜角肌](../Page/後斜角肌.md "wikilink")。                                      |

## 軀幹的肌肉

[軀幹是連結](../Page/軀幹.md "wikilink")[頭](../Page/頭.md "wikilink")[頸](../Page/頸.md "wikilink")、[上肢和](../Page/上肢.md "wikilink")[下肢的部分](../Page/下肢.md "wikilink")，分為上方的[胸部和下方的](../Page/胸部.md "wikilink")[腹部](../Page/腹部.md "wikilink")。軀幹前面的淺層[肌肉主要是](../Page/肌肉.md "wikilink")[胸大肌和](../Page/胸大肌.md "wikilink")[腹外斜肌](../Page/腹外斜肌.md "wikilink")，[胸大肌](../Page/胸大肌.md "wikilink")\[1\]是一塊大而薄的肌肉，它可以使[手臂向前擺動](../Page/手臂.md "wikilink")、旋內和彎曲，另外它還可以在人們吊單槓時，幫助人們把身體向上拉。軀幹前面的深層[肌肉主要是](../Page/肌肉.md "wikilink")[肋間外肌和](../Page/肋間外肌.md "wikilink")[腹直肌](../Page/腹直肌.md "wikilink")。軀幹背部的淺層肌肉主要是[斜方肌和](../Page/斜方肌.md "wikilink")[背闊肌](../Page/背闊肌.md "wikilink")，[斜方肌呈三角形](../Page/斜方肌.md "wikilink")，其左右兩側相合成為斜方形，斜方肌的收縮能促使[肩胛骨向](../Page/肩胛骨.md "wikilink")[脊柱靠近](../Page/脊柱.md "wikilink")，另外斜方肌能夠向後拉伸[頭和](../Page/頭.md "wikilink")[肩膀](../Page/肩膀.md "wikilink")，使肩膀保持平穩。[背闊肌是全身最大的](../Page/背闊肌.md "wikilink")[扁闊肌](../Page/扁闊肌.md "wikilink")，它可以使[肱骨內收](../Page/肱骨.md "wikilink")、旋內和後伸的作用，當人們做引體向上運動時，[背闊肌可以幫助我們提升](../Page/背闊肌.md "wikilink")[軀幹](../Page/軀幹.md "wikilink")。軀幹背部的深層肌肉主要是[小菱形肌和](../Page/小菱形肌.md "wikilink")[大菱形肌](../Page/大菱形肌.md "wikilink")，[菱形肌呈菱形扁平狀](../Page/菱形肌.md "wikilink")，它包括小菱形肌和大菱形肌，菱形肌可以使肩胛骨向脊柱靠近並稍微向上提。

### 軀幹前面的肌肉

[肋骨圍成的籠狀](../Page/肋骨.md "wikilink")[胸廓構成了軀幹上部的](../Page/胸廓.md "wikilink")[支架](../Page/支架.md "wikilink")。[腹壁則僅由扁平的](../Page/腹壁.md "wikilink")[腹肌形成](../Page/腹肌.md "wikilink")，腹肌的[肌腱在腹部正中央融合形成](../Page/肌腱.md "wikilink")[腹白線](../Page/腹白線.md "wikilink")。

### 軀幹背部的肌肉

背部的淺層[肌肉可以帶動人體的](../Page/肌肉.md "wikilink")[手臂和](../Page/手臂.md "wikilink")[肩膀的運動](../Page/肩膀.md "wikilink")。背部深層的[豎脊肌兩側收縮時可以拉伸](../Page/豎脊肌.md "wikilink")[脊柱](../Page/脊柱.md "wikilink")、抗重力和維持平衡，如果只有單側[豎脊肌收縮](../Page/豎脊肌.md "wikilink")，就可以使人體的脊柱彎曲。[豎脊肌為](../Page/豎脊肌.md "wikilink")[背部的肌肉中最長最大的肌肉](../Page/背部.md "wikilink")，其作用是使脊柱後伸和仰頭，它對維持人體的直立姿勢起著重要的作用。

### 腹壁

[腹外斜肌](../Page/腹外斜肌.md "wikilink")：覆盖腹部两侧最外层的[肌肉](../Page/肌肉.md "wikilink")，它扁平而宽，呈不规则的四边形。从下面八根[肋骨起](../Page/肋骨.md "wikilink")，斜下向前附着在[髂骨的前外嵴和穿过腹直肌鞘到达白线](../Page/髂骨.md "wikilink")。

[腹内斜肌](../Page/腹内斜肌.md "wikilink")：呈三角形，并且比它上面的腹外斜肌小而薄。它起自和[髂骨前内嵴](../Page/髂骨.md "wikilink")。它的下三分之二和腹外斜肌以及内面的腹横肌纤维附着于白线。上三分之一则终于最下面的六根肋骨。

[腹横肌](../Page/腹横肌.md "wikilink")：扁平成三角形。其纤维水平走向。位于腹内斜肌和下层的横筋膜之间。起于Poupart韧带，髂骨内唇，腰筋膜和下六根肋骨软骨部分的内侧。终于白线，在腹直肌后面。

[腹直肌](../Page/腹直肌.md "wikilink")：扁平而长。它们起于[耻骨](../Page/耻骨.md "wikilink")，在白线的两侧通过腹部向上附着于第5，6和7肋骨的软骨部分。该肌肉被三腱划通过。腹直肌为上面提到的侧腹壁的三种肌肉的纤维形成的厚实鞘所包绕。

[錐肌](../Page/錐肌.md "wikilink")：呈小的三角形，位于下腹部腹直肌的前方。它位于下腹，在腹直肌的前面。它起于耻骨，终于白线，途中经过[脐](../Page/脐.md "wikilink")
。

腹膜腔的後壁內，有著[髂肌](../Page/髂肌.md "wikilink")（或稱胯肌，）、[腰方肌](../Page/腰方肌.md "wikilink")（）、[腰大肌與](../Page/腰大肌.md "wikilink")[腰小肌等](../Page/腰小肌.md "wikilink")。髂肌與腰大肌統稱（）。

### 軀幹肌肉列表

下表列出了人體[軀幹所有的肌肉](../Page/軀幹.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>軀幹位置</p></th>
<th><p>肌肉的種類</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/背部.md" title="wikilink">背部</a></p></td>
<td><p><a href="../Page/夾肌.md" title="wikilink">夾肌</a>（<a href="../Page/頭夾肌.md" title="wikilink">頭夾肌</a>、<a href="../Page/頸夾肌.md" title="wikilink">頸夾肌</a>）、<a href="../Page/豎脊肌.md" title="wikilink">豎脊肌</a>（<a href="../Page/髂肋肌.md" title="wikilink">髂肋肌</a>、<a href="../Page/最長肌.md" title="wikilink">最長肌</a>、<a href="../Page/棘肌.md" title="wikilink">棘肌</a>）、<a href="../Page/背闊肌.md" title="wikilink">背闊肌</a>、<a href="../Page/橫突棘肌.md" title="wikilink">橫突棘肌</a>（<a href="../Page/背半棘肌.md" title="wikilink">背半棘肌</a>、<a href="../Page/頸半棘肌.md" title="wikilink">頸半棘肌</a>、<a href="../Page/頭半棘肌.md" title="wikilink">頭半棘肌</a>、<a href="../Page/多裂肌.md" title="wikilink">多裂肌</a>、<a href="../Page/迴旋肌.md" title="wikilink">迴旋肌</a>）、<a href="../Page/棘間肌.md" title="wikilink">棘間肌</a>、<a href="../Page/橫突間肌.md" title="wikilink">橫突間肌</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/枕骨.md" title="wikilink">枕骨下部</a></p></td>
<td><p><a href="../Page/頭後直肌.md" title="wikilink">頭後直肌</a>（<a href="../Page/頭後大直肌.md" title="wikilink">頭後大直肌</a>、<a href="../Page/頭後小直肌.md" title="wikilink">頭後小直肌</a>）、<a href="../Page/頭斜肌.md" title="wikilink">頭斜肌</a>（<a href="../Page/頭下斜肌.md" title="wikilink">頭下斜肌</a>、<a href="../Page/頭上斜肌.md" title="wikilink">頭上斜肌</a>）。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/胸部.md" title="wikilink">胸部</a></p></td>
<td><p><a href="../Page/肋間肌.md" title="wikilink">肋間肌</a>（<a href="../Page/肋間外肌.md" title="wikilink">肋間外肌</a>、<a href="../Page/肋間內肌.md" title="wikilink">肋間內肌</a>、<a href="../Page/最內肋間肌.md" title="wikilink">最內肋間肌</a>）、<a href="../Page/肋下肌.md" title="wikilink">肋下肌</a>、<a href="../Page/胸橫肌.md" title="wikilink">胸橫肌</a>、<a href="../Page/提肋肌.md" title="wikilink">提肋肌</a>、<a href="../Page/前鋸肌.md" title="wikilink">前鋸肌</a>[2]（<a href="../Page/下前鋸肌.md" title="wikilink">下前鋸肌</a>、<a href="../Page/上前鋸肌.md" title="wikilink">上前鋸肌</a>）、<a href="../Page/橫膈膜.md" title="wikilink">橫膈膜</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/腹部.md" title="wikilink">腹部</a></p></td>
<td><p>前面：<a href="../Page/腹斜肌.md" title="wikilink">腹斜肌</a>（<a href="../Page/腹外斜肌.md" title="wikilink">腹外斜肌</a>、<a href="../Page/腹內斜肌.md" title="wikilink">腹內斜肌</a>）、<a href="../Page/腹橫肌.md" title="wikilink">腹橫肌</a>、<a href="../Page/腹直肌.md" title="wikilink">腹直肌</a>、<a href="../Page/錐肌.md" title="wikilink">錐肌</a>。<br />
後面：<a href="../Page/腰方肌.md" title="wikilink">腰方肌</a>、<a href="../Page/腰大肌.md" title="wikilink">腰大肌</a>[3]、<a href="../Page/腰小肌.md" title="wikilink">腰小肌</a>[4]、<a href="../Page/胯肌.md" title="wikilink">胯肌</a>。<br />
特殊：<a href="../Page/提睪肌.md" title="wikilink">提睪肌</a>[5]。<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/骨盤.md" title="wikilink">骨盤</a></p></td>
<td><p><a href="../Page/提肛肌.md" title="wikilink">提肛肌</a>（<a href="../Page/髂尾肌.md" title="wikilink">髂尾肌</a>、<a href="../Page/恥尾肌.md" title="wikilink">恥尾肌</a>、<a href="../Page/恥骨直腸肌.md" title="wikilink">恥骨直腸肌</a>）、<a href="../Page/尾骨肌.md" title="wikilink">尾骨肌</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/會陰.md" title="wikilink">會陰</a></p></td>
<td><p>臀三角：<a href="../Page/肛門括約肌.md" title="wikilink">肛門括約肌</a>（<a href="../Page/肛門外括約肌.md" title="wikilink">肛門外括約肌</a>、<a href="../Page/肛門內括約肌.md" title="wikilink">肛門內括約肌</a>）。<br />
會陰淺隙：<a href="../Page/會陰淺橫肌.md" title="wikilink">會陰淺橫肌</a>、<a href="../Page/球海綿體肌.md" title="wikilink">球海綿體肌</a>、<a href="../Page/坐骨海綿體肌.md" title="wikilink">坐骨海綿體肌</a>。<br />
會陰深隙：<a href="../Page/會陰深橫肌.md" title="wikilink">會陰深橫肌</a>、<a href="../Page/尿道括約肌.md" title="wikilink">尿道括約肌</a>。<br />
</p></td>
</tr>
</tbody>
</table>

## 上肢的肌肉

[上肢的肌肉可以分為](../Page/上肢.md "wikilink")[手臂的肌肉和](../Page/手臂.md "wikilink")[手部的肌肉](../Page/手部.md "wikilink")。其中手臂的肌肉可以分為上臂的肌肉和前臂的肌肉。上臂的肌肉以內側肌和外側肌間隔為界，分前、後兩群。前臂的肌肉分布於[尺骨和](../Page/尺骨.md "wikilink")[橈骨的周圍](../Page/橈骨.md "wikilink")，分前、後兩群，數目眾多，大多為[長肌](../Page/長肌.md "wikilink")，其中肌腹多集中在前臂的上半部，下半部則形成細長的[肌腱](../Page/肌腱.md "wikilink")。手部的肌肉皆為短小的肌肉，全部都集中於手的掌面，能讓[手指運動](../Page/手指.md "wikilink")，完成精細的技巧性動作，可以分為外側群、中間群和內側群。

### 上臂的肌肉

上臂的肌肉可以分為前、後兩群。上臂肌前群的淺層的肌肉主要是[肱二頭肌](../Page/肱二頭肌.md "wikilink")，[肱二頭肌屬於](../Page/肱二頭肌.md "wikilink")[長肌](../Page/長肌.md "wikilink")，呈梭形，它的起端有兩個頭，因此而得名，肱二頭肌可以使[肘關節屈伸](../Page/肘關節.md "wikilink")，此外肱二頭肌還可以協助[肩關節屈伸](../Page/肩關節.md "wikilink")。深層的肌肉主要是[肱肌](../Page/肱肌.md "wikilink")，[肱肌位於肱二頭肌的深面](../Page/肱肌.md "wikilink")，它可以使肘關節屈伸，從而進一步促使[前臂屈伸](../Page/前臂.md "wikilink")。上臂肌後群的肌肉主要是[肱三頭肌](../Page/肱三頭肌.md "wikilink")，[肱三頭肌的作用是使](../Page/肱三頭肌.md "wikilink")[肘關節伸展](../Page/肘關節.md "wikilink")。

### 前臂肌前群

[手臂的肌肉可以分為上臂的肌肉和](../Page/手臂.md "wikilink")[前臂的肌肉](../Page/前臂.md "wikilink")。前臂的肌肉可以分為前、後兩群。其中前臂的前群肌肉為屈肌群，位於[前臂的前面](../Page/前臂.md "wikilink")，總共有9塊，分為淺、深兩層。淺層的肌肉有[肱橈肌](../Page/肱橈肌.md "wikilink")、[旋前圓肌](../Page/旋前圓肌.md "wikilink")、[橈側腕屈肌](../Page/橈側腕屈肌.md "wikilink")、[掌長肌](../Page/掌長肌.md "wikilink")、[指淺屈肌和](../Page/指淺屈肌.md "wikilink")[尺側腕屈肌](../Page/尺側腕屈肌.md "wikilink")。深層的肌肉有[拇長屈肌](../Page/拇長屈肌.md "wikilink")、[指深屈肌和](../Page/指深屈肌.md "wikilink")[旋前方肌](../Page/旋前方肌.md "wikilink")。前臂肌前群的肌肉可以讓手臂屈肘、屈腕、屈指和前臂旋前……等功能。

### 前臂肌後群

前臂的後群[肌肉為伸肌群](../Page/肌肉.md "wikilink")，位於[前臂的後面](../Page/前臂.md "wikilink")，總共有10塊，也分為淺、深兩層。淺層的肌肉有[橈側腕長伸肌](../Page/橈側腕長伸肌.md "wikilink")、[橈側腕短伸肌](../Page/橈側腕短伸肌.md "wikilink")、[指伸肌](../Page/指伸肌.md "wikilink")、[小指伸肌和](../Page/小指伸肌.md "wikilink")[尺側腕伸肌](../Page/尺側腕伸肌.md "wikilink")。深層的肌肉有[旋後肌](../Page/旋後肌.md "wikilink")、[拇長展肌](../Page/拇長展肌.md "wikilink")、[拇短伸肌](../Page/拇短伸肌.md "wikilink")、[拇長伸肌和](../Page/拇長伸肌.md "wikilink")[食指伸肌](../Page/食指伸肌.md "wikilink")。前臂肌後群的肌肉可以讓手臂伸肘、伸腕、伸指和前臂旋後……等功能。

### 手部的肌肉

[手部的](../Page/手部.md "wikilink")[肌肉都十分短小](../Page/肌肉.md "wikilink")，其功能是使手部運動。手部的肌肉可以分為外側、內側和中間。內側的肌肉主要有[拇短展肌](../Page/拇短展肌.md "wikilink")、[拇短屈肌](../Page/拇短屈肌.md "wikilink")、[小指展肌和](../Page/小指展肌.md "wikilink")[小指短屈肌](../Page/小指短屈肌.md "wikilink")……等。外側和中間的肌肉主要有[拇指對掌肌](../Page/拇指對掌肌.md "wikilink")、[拇收肌](../Page/拇收肌.md "wikilink")、[小指對掌肌](../Page/小指對掌肌.md "wikilink")、[指間掌側肌和](../Page/指間掌側肌.md "wikilink")[骨間背側肌](../Page/骨間背側肌.md "wikilink")。這些肌肉可以使手掌和[手指的](../Page/手指.md "wikilink")[關節彎曲和伸展](../Page/關節.md "wikilink")。

### 上肢肌肉列表

下表列出了人體[上肢所有的肌肉](../Page/上肢.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>上肢位置</p></th>
<th><p>肌肉的種類</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/脊柱.md" title="wikilink">脊柱</a></p></td>
<td><p><a href="../Page/斜方肌.md" title="wikilink">斜方肌</a>、<a href="../Page/背闊肌.md" title="wikilink">背闊肌</a>、<a href="../Page/大菱形肌.md" title="wikilink">大菱形肌</a>、<a href="../Page/小菱形肌.md" title="wikilink">小菱形肌</a>、<a href="../Page/肩胛提肌.md" title="wikilink">肩胛提肌</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胸腔.md" title="wikilink">胸腔</a></p></td>
<td><p><a href="../Page/胸大肌.md" title="wikilink">胸大肌</a>、<a href="../Page/胸小肌.md" title="wikilink">胸小肌</a>、<a href="../Page/鎖骨下肌.md" title="wikilink">鎖骨下肌</a>、<a href="../Page/前鋸肌.md" title="wikilink">前鋸肌</a>（<a href="../Page/下前鋸肌.md" title="wikilink">下前鋸肌</a>、<a href="../Page/上前鋸肌.md" title="wikilink">上前鋸肌</a>）。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/肩膀.md" title="wikilink">肩膀</a></p></td>
<td><p><a href="../Page/三角肌.md" title="wikilink">三角肌</a>、<a href="../Page/肩旋板.md" title="wikilink">肩旋板</a>（<a href="../Page/肩胛下肌.md" title="wikilink">肩胛下肌</a>、<a href="../Page/棘上肌.md" title="wikilink">棘上肌</a>、<a href="../Page/棘下肌.md" title="wikilink">棘下肌</a>、<a href="../Page/小圓肌.md" title="wikilink">小圓肌</a>）、<a href="../Page/大圓肌.md" title="wikilink">大圓肌</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上臂.md" title="wikilink">上臂</a></p></td>
<td><p><a href="../Page/喙肱肌.md" title="wikilink">喙肱肌</a>、<a href="../Page/肱二頭肌.md" title="wikilink">肱二頭肌</a>、<a href="../Page/肱肌.md" title="wikilink">肱肌</a>、<a href="../Page/肱三頭肌.md" title="wikilink">肱三頭肌</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/前臂.md" title="wikilink">前臂</a></p></td>
<td><p>手掌淺層：<a href="../Page/旋前圓肌.md" title="wikilink">旋前圓肌</a>、<a href="../Page/掌長肌.md" title="wikilink">掌長肌</a>、<a href="../Page/橈側腕屈肌.md" title="wikilink">橈側腕屈肌</a>、<a href="../Page/尺側腕屈肌.md" title="wikilink">尺側腕屈肌</a>、<a href="../Page/指淺屈肌.md" title="wikilink">指淺屈肌</a>。<br />
手掌深層：<a href="../Page/指深屈肌.md" title="wikilink">指深屈肌</a>、<a href="../Page/拇長屈肌.md" title="wikilink">拇長屈肌</a>、<a href="../Page/旋前方肌.md" title="wikilink">旋前方肌</a>。<br />
手背淺層：<a href="../Page/肱橈肌.md" title="wikilink">肱橈肌</a>、<a href="../Page/指伸肌.md" title="wikilink">指伸肌</a>、<a href="../Page/橈側腕長伸肌.md" title="wikilink">橈側腕長伸肌</a>、<a href="../Page/小指伸肌.md" title="wikilink">小指伸肌</a>、<a href="../Page/橈側腕短伸肌.md" title="wikilink">橈側腕短伸肌</a>、<a href="../Page/尺側腕伸肌.md" title="wikilink">尺側腕伸肌</a>、<a href="../Page/肘肌.md" title="wikilink">肘肌</a>。<br />
手背深層：<a href="../Page/旋後肌.md" title="wikilink">旋後肌</a>、<a href="../Page/拇長展肌.md" title="wikilink">拇長展肌</a>、<a href="../Page/拇短伸肌.md" title="wikilink">拇短伸肌</a>、<a href="../Page/拇長伸肌.md" title="wikilink">拇長伸肌</a>、<a href="../Page/食指伸肌.md" title="wikilink">食指伸肌</a>。<br />
</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/手部.md" title="wikilink">手部</a></p></td>
<td><p>手掌外側：「<a href="../Page/魚際肌.md" title="wikilink">魚際肌</a>」=<a href="../Page/拇外短展肌.md" title="wikilink">拇外短展肌</a>、<a href="../Page/拇指對掌肌.md" title="wikilink">拇指對掌肌</a>、<a href="../Page/拇短屈肌.md" title="wikilink">拇短屈肌</a>、<a href="../Page/拇收肌.md" title="wikilink">拇收肌</a>。<br />
手掌內側：「<a href="../Page/小魚際肌.md" title="wikilink">小魚際肌</a>」=<a href="../Page/掌短肌.md" title="wikilink">掌短肌</a>、<a href="../Page/小指展肌.md" title="wikilink">小指展肌</a>、<a href="../Page/小指短屈肌.md" title="wikilink">小指短屈肌</a>、<a href="../Page/小指對肌.md" title="wikilink">小指對肌</a>。<br />
手掌中間：<a href="../Page/蚓狀肌.md" title="wikilink">蚓狀肌</a>、<a href="../Page/骨間背側肌.md" title="wikilink">骨間背側肌</a>、<a href="../Page/指間掌側肌.md" title="wikilink">指間掌側肌</a>。<br />
</p></td>
</tr>
</tbody>
</table>

## 下肢的肌肉

[人體](../Page/人體.md "wikilink")[下肢的肌肉主要包括](../Page/下肢.md "wikilink")[腿部的肌肉和](../Page/腿部.md "wikilink")[腳部的肌肉](../Page/腳部.md "wikilink")。其中人體腿部的肌肉能完成行走、跑動和爬行……等各種動作，而人體腳部的肌肉則主要起著維持[足弓和行走的作用](../Page/足弓.md "wikilink")。腿部的肌肉可以分為[髖肌](../Page/髖肌.md "wikilink")、[大腿肌和](../Page/大腿肌.md "wikilink")[小腿肌](../Page/小腿肌.md "wikilink")。腳部的肌肉可以分為[足背肌和](../Page/足背肌.md "wikilink")[足底肌](../Page/足底肌.md "wikilink")。

### 髖肌前群

[髖肌多起使於](../Page/髖肌.md "wikilink")[骨盆內面或外面](../Page/骨盆.md "wikilink")，跨越[髖關節](../Page/髖關節.md "wikilink")，止於[股骨](../Page/股骨.md "wikilink")，分為前、後兩群。髖肌前群主要包括[髂腰肌和](../Page/髂腰肌.md "wikilink")[闊筋膜張肌](../Page/闊筋膜張肌.md "wikilink")。[髂腰肌由](../Page/髂腰肌.md "wikilink")[髂肌和](../Page/髂肌.md "wikilink")[腰大肌合併而成](../Page/腰大肌.md "wikilink")，它可以使髖關節前屈和旋外的作用。當人們做[仰臥起坐](../Page/仰臥起坐.md "wikilink")[下肢固定時](../Page/下肢.md "wikilink")，髂腰肌可以使[人體的](../Page/人體.md "wikilink")[軀幹和](../Page/軀幹.md "wikilink")[骨盆前屈](../Page/骨盆.md "wikilink")。

### 大腿肌前群

[大腿肌分布於](../Page/大腿肌.md "wikilink")[股骨周圍](../Page/股骨.md "wikilink")，分為前、後和內側3群。其中大腿肌前群的肌肉有[縫匠肌和](../Page/縫匠肌.md "wikilink")[股四頭肌](../Page/股四頭肌.md "wikilink")。[縫匠肌是全身最大的肌肉](../Page/縫匠肌.md "wikilink")，它不僅能使[大腿旋轉](../Page/大腿.md "wikilink")，還能使大腿屈伸。[股四頭肌是全身](../Page/股四頭肌.md "wikilink")[體積最大的肌肉](../Page/體積.md "wikilink")，它能使人在跑步、攀登和踢動時讓[膝蓋伸直](../Page/膝蓋.md "wikilink")。事實上，股四頭肌並非只是一塊肌肉，它是由[股直肌](../Page/股直肌.md "wikilink")、[股內側肌](../Page/股內側肌.md "wikilink")、[股外側肌和](../Page/股外側肌.md "wikilink")[股中間肌組成](../Page/股中間肌.md "wikilink")。

### 小腿肌前群

[小腿肌與](../Page/小腿肌.md "wikilink")[上肢的](../Page/上肢.md "wikilink")[前臂的肌肉比較](../Page/前臂.md "wikilink")，粗大但是數目少，分為前、後和外側3群，它參與維持人體的直立姿勢和行走。小腿肌前群主要包括[脛骨前肌](../Page/脛骨前肌.md "wikilink")、[拇長伸肌和](../Page/拇長伸肌.md "wikilink")[趾長伸肌](../Page/趾長伸肌.md "wikilink")。[脛骨前肌能產生足內翻](../Page/脛骨前肌.md "wikilink")，[拇長伸肌和](../Page/拇長伸肌.md "wikilink")[趾長伸肌能使](../Page/趾長伸肌.md "wikilink")[腳趾屈伸](../Page/腳趾.md "wikilink")，此外[趾長伸肌還能產生屈踝運動](../Page/趾長伸肌.md "wikilink")。

### 髖肌後群

髖肌後群位於[臀部](../Page/臀部.md "wikilink")，又稱為臀肌，主要包括[臀大肌](../Page/臀大肌.md "wikilink")、[臀中肌](../Page/臀中肌.md "wikilink")、[臀小肌](../Page/臀小肌.md "wikilink")、[梨狀肌](../Page/梨狀肌.md "wikilink")……等[肌肉](../Page/肌肉.md "wikilink")。這些豐滿的肌肉隆起形成臀部，起著支撐[人體的作用](../Page/人體.md "wikilink")。[臀大肌位於臀部淺層](../Page/臀大肌.md "wikilink")，其作用是使[髖關節旋外](../Page/髖關節.md "wikilink")，同時它還能伸直[軀幹](../Page/軀幹.md "wikilink")，防止軀幹前傾，以維持[身體的平衡](../Page/身體.md "wikilink")。[臀中肌位於](../Page/臀中肌.md "wikilink")[臀大肌的深面](../Page/臀大肌.md "wikilink")，其作用是使[髖關節外展](../Page/髖關節.md "wikilink")。[梨狀肌的作用是使髖關節旋外](../Page/梨狀肌.md "wikilink")。

### 大腿肌後群和大腿肌內側群

大腿肌後群的肌肉包括[股二頭肌](../Page/股二頭肌.md "wikilink")、[半腱肌和](../Page/半腱肌.md "wikilink")[半膜肌](../Page/半膜肌.md "wikilink")，這些肌肉可以使[大腿和](../Page/大腿.md "wikilink")[小腿彎曲](../Page/小腿.md "wikilink")，而且還可以使已經彎曲的小腿內旋。大腿肌內側群的肌肉位於大腿的內側，總共有5塊，為[恥骨肌](../Page/恥骨肌.md "wikilink")、[內收長肌](../Page/內收長肌.md "wikilink")、[內收短肌](../Page/內收短肌.md "wikilink")、[內收大肌和](../Page/內收大肌.md "wikilink")[股薄肌](../Page/股薄肌.md "wikilink")，這些肌肉可以使大腿向外旋和向內收。

### 小腿肌後群和小腿肌外側群

小腿肌後群位於[脛骨](../Page/脛骨.md "wikilink")、[腓骨及](../Page/腓骨.md "wikilink")[骨間膜的後面](../Page/骨間膜.md "wikilink")，可以分為淺、深兩層。淺層的肌肉主要有[腓腸肌和](../Page/腓腸肌.md "wikilink")[比目魚肌](../Page/比目魚肌.md "wikilink")，[腓腸肌的功能是彎曲](../Page/腓腸肌.md "wikilink")、伸直和扭轉腳跟，當我們坐著並且輕拍[小腿時](../Page/小腿.md "wikilink")，就可以看到這個部位的肌肉在晃動。[比目魚肌聯結小腿的肌肉和足骨](../Page/比目魚肌.md "wikilink")，其功能是強化[腳踝](../Page/腳踝.md "wikilink")，以防止身體向前傾倒。深層的肌肉主要有[趾長屈肌](../Page/趾長屈肌.md "wikilink")、[脛骨後肌和](../Page/脛骨後肌.md "wikilink")[拇長屈肌](../Page/拇長屈肌.md "wikilink")，[趾長屈肌和](../Page/趾長屈肌.md "wikilink")[拇長屈肌的作用是使](../Page/拇長屈肌.md "wikilink")[腳趾跖屈](../Page/腳趾.md "wikilink")。小腿肌外側群包括[腓骨長肌和](../Page/腓骨長肌.md "wikilink")[腓骨短肌](../Page/腓骨短肌.md "wikilink")，[腓骨短肌的作用是使腳心外翻並且協助腳趾跖屈](../Page/腓骨短肌.md "wikilink")。

### 腳部的肌肉

[腳部的肌肉分為](../Page/腳部.md "wikilink")[足背肌和](../Page/足背肌.md "wikilink")[足底肌](../Page/足底肌.md "wikilink")。其中足背肌比較弱小，主要包括[趾短伸肌和](../Page/趾短伸肌.md "wikilink")[拇短伸肌](../Page/拇短伸肌.md "wikilink")。足底肌與手掌的肌肉相似，也分為內側、外側和中間3群，但是卻沒有[對掌肌](../Page/對掌肌.md "wikilink")。足底肌除了能配合小腿肌運動各個[足關節外](../Page/足關節.md "wikilink")，還有維持[足弓的作用](../Page/足弓.md "wikilink")。

### 下肢肌肉列表

下表列出了人體[下肢所有的肌肉](../Page/下肢.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>下肢位置</p></th>
<th><p>肌肉的種類</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/髂部.md" title="wikilink">髂部</a>[6]</p></td>
<td><p><a href="../Page/腰大肌.md" title="wikilink">腰大肌</a>、<a href="../Page/腰小肌.md" title="wikilink">腰小肌</a>、<a href="../Page/髂肌.md" title="wikilink">髂肌</a>[7]。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大腿.md" title="wikilink">大腿</a></p></td>
<td><p><a href="../Page/屁股.md" title="wikilink">屁股前面</a>：<a href="../Page/縫匠肌.md" title="wikilink">縫匠肌</a>、<a href="../Page/股四頭肌.md" title="wikilink">股四頭肌</a>（<a href="../Page/股直肌.md" title="wikilink">股直肌</a>、<a href="../Page/股外側肌.md" title="wikilink">股外側肌</a>、<a href="../Page/股中間肌.md" title="wikilink">股中間肌</a>、<a href="../Page/股內側肌.md" title="wikilink">股內側肌</a>）、<a href="../Page/膝關節肌.md" title="wikilink">膝關節肌</a>。<br />
<a href="../Page/屁股.md" title="wikilink">屁股中間</a>：「<a href="../Page/內收臀肌.md" title="wikilink">內收臀肌</a>」=<a href="../Page/股薄肌.md" title="wikilink">股薄肌</a>、<a href="../Page/恥骨肌.md" title="wikilink">恥骨肌</a>、<a href="../Page/內收短肌.md" title="wikilink">內收短肌</a>、<a href="../Page/內收長肌.md" title="wikilink">內收長肌</a>、<a href="../Page/內收大肌.md" title="wikilink">內收大肌</a>。<br />
<a href="../Page/臀部.md" title="wikilink">臀部</a>：<a href="../Page/臀肌.md" title="wikilink">臀肌</a>（<a href="../Page/臀大肌.md" title="wikilink">臀大肌</a>、<a href="../Page/臀中肌.md" title="wikilink">臀中肌</a>、<a href="../Page/臀小肌.md" title="wikilink">臀小肌</a>）、<a href="../Page/闊筋膜張肌.md" title="wikilink">闊筋膜張肌</a>。<br />
側旋部：<a href="../Page/梨狀肌.md" title="wikilink">梨狀肌</a>、<a href="../Page/外閉孔肌.md" title="wikilink">外閉孔肌</a>、<a href="../Page/內閉孔肌.md" title="wikilink">內閉孔肌</a>、<a href="../Page/下孖肌.md" title="wikilink">下孖肌</a>、<a href="../Page/上孖肌.md" title="wikilink">上孖肌</a>、<a href="../Page/股方肌.md" title="wikilink">股方肌</a>。<br />
<a href="../Page/屁股.md" title="wikilink">屁股後面</a>：「<a href="../Page/大腿後肌.md" title="wikilink">大腿後肌</a>[8]」=<a href="../Page/股二頭肌.md" title="wikilink">股二頭肌</a>、<a href="../Page/半腱肌.md" title="wikilink">半腱肌</a>、<a href="../Page/半膜肌.md" title="wikilink">半膜肌</a>。<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小腿.md" title="wikilink">小腿</a></p></td>
<td><p><a href="../Page/脛骨.md" title="wikilink">脛骨前面</a>：<a href="../Page/脛骨前肌.md" title="wikilink">脛骨前肌</a>、<a href="../Page/拇長伸肌.md" title="wikilink">拇長伸肌</a>、<a href="../Page/趾長伸肌.md" title="wikilink">趾長伸肌</a>、<a href="../Page/腓骨第三肌.md" title="wikilink">腓骨第三肌</a>、<a href="../Page/趾短伸肌.md" title="wikilink">趾短伸肌</a>、<a href="../Page/拇短伸肌.md" title="wikilink">拇短伸肌</a>。<br />
<a href="../Page/脛骨.md" title="wikilink">脛骨上後部</a>：<a href="../Page/腓肌.md" title="wikilink">腓肌</a>（<a href="../Page/腓腸肌.md" title="wikilink">腓腸肌</a>、<a href="../Page/比目魚肌.md" title="wikilink">比目魚肌</a>）、<a href="../Page/蹠肌.md" title="wikilink">蹠肌</a>。<br />
<a href="../Page/脛骨.md" title="wikilink">脛骨深後部</a>：<a href="../Page/膕肌.md" title="wikilink">膕肌</a>[9]、<a href="../Page/拇長屈肌.md" title="wikilink">拇長屈肌</a>、<a href="../Page/趾長屈肌.md" title="wikilink">趾長屈肌</a>、<a href="../Page/脛骨後肌.md" title="wikilink">脛骨後肌</a>。<br />
側脛部：<a href="../Page/腓骨長肌.md" title="wikilink">腓骨長肌</a>、<a href="../Page/腓骨短肌.md" title="wikilink">腓骨短肌</a>。<br />
</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/腳部.md" title="wikilink">腳部</a></p></td>
<td><p>第一層：<a href="../Page/拇外展肌.md" title="wikilink">拇外展肌</a>、<a href="../Page/趾短屈肌.md" title="wikilink">趾短屈肌</a>、<a href="../Page/小指展肌.md" title="wikilink">小指展肌</a>。<br />
第二層：<a href="../Page/蹠方肌.md" title="wikilink">蹠方肌</a>、<a href="../Page/蚓狀肌.md" title="wikilink">蚓狀肌</a>。<br />
第三層：<a href="../Page/拇短屈肌.md" title="wikilink">拇短屈肌</a>、<a href="../Page/拇收肌.md" title="wikilink">拇收肌</a>、<a href="../Page/小指短屈肌.md" title="wikilink">小指短屈肌</a>。<br />
第四層：<a href="../Page/骨間背側肌.md" title="wikilink">骨間背側肌</a>、<a href="../Page/骨間足底肌.md" title="wikilink">骨間足底肌</a>。<br />
</p></td>
</tr>
</tbody>
</table>

## 備註

<references group="注"/>

## 參見

  - [肌肉](../Page/肌肉.md "wikilink")
  - [骨骼肌](../Page/骨骼肌.md "wikilink")
  - [人體骨骼列表](../Page/人體骨骼列表.md "wikilink")
  - [人體器官列表](../Page/人體器官列表.md "wikilink")

## 参考文献

  - [國立編譯館](http://terms.nict.gov.tw/search1.php)

  - <http://www.meddean.luc.edu/lumen/MedEd/GrossAnatomy/dissector/muscles/muscles.html>

  - <https://web.archive.org/web/20090930203009/http://www.ptcentral.com/muscles/>

  - <http://www.rad.washington.edu/atlas2/>

## 外部連結

  - [解剖學(Anatomy)-肌肉(muscle)-全部肌肉列表](http://smallcollation.blogspot.com/2013/02/anatomy-muscle.html)

[Category:人体解剖学](../Category/人体解剖学.md "wikilink")

1.  [胸大肌位於](../Page/胸大肌.md "wikilink")[軀幹](../Page/軀幹.md "wikilink")[胸部的位置](../Page/胸部.md "wikilink")，正確來講胸大肌應該被列為[軀幹的肌肉](../Page/軀幹.md "wikilink")。可是用[解剖學的角度來講](../Page/解剖學.md "wikilink")，胸大肌的尾端附著在[肩膀的](../Page/肩膀.md "wikilink")[鎖骨](../Page/鎖骨.md "wikilink")，胸大肌的腹內脊肌還延伸到[手臂的](../Page/手臂.md "wikilink")[肱骨](../Page/肱骨.md "wikilink")，所以在醫學上來說，[胸大肌是位於](../Page/胸大肌.md "wikilink")[上肢的肌肉](../Page/上肢.md "wikilink")，而不是[軀幹的肌肉](../Page/軀幹.md "wikilink")。參考資料：[胸大肌的正確位置](http://www.jirou.com/jiroulilun/lixue/2015/0910/10226.html)
2.  [前鋸肌位於](../Page/前鋸肌.md "wikilink")[胸部以及](../Page/胸部.md "wikilink")[肩膀之間](../Page/肩膀.md "wikilink")，是屬於[軀幹以及](../Page/軀幹.md "wikilink")[上肢的區域](../Page/上肢.md "wikilink")。所以在[軀幹肌肉列表和](../Page/人體肌肉列表#軀幹的肌肉#軀幹肌肉列表.md "wikilink")[上肢肌肉列表當中](../Page/人體肌肉列表#上肢的肌肉#上肢肌肉列表.md "wikilink")，都會出現[前鋸肌](../Page/前鋸肌.md "wikilink")。
3.  [腰大肌位於](../Page/腰大肌.md "wikilink")[腰部以及](../Page/腰部.md "wikilink")[胯部之間](../Page/胯部.md "wikilink")，是屬於[軀幹以及](../Page/軀幹.md "wikilink")[下肢的區域](../Page/下肢.md "wikilink")。所以在[軀幹肌肉列表和](../Page/人體肌肉列表#軀幹的肌肉#軀幹肌肉列表.md "wikilink")[下肢肌肉列表當中](../Page/人體肌肉列表#下肢的肌肉#下肢肌肉列表.md "wikilink")，都會出現[腰大肌](../Page/腰大肌.md "wikilink")。
4.  [腰小肌位於](../Page/腰小肌.md "wikilink")[腰部以及](../Page/腰部.md "wikilink")[胯部之間](../Page/胯部.md "wikilink")，是屬於[軀幹以及](../Page/軀幹.md "wikilink")[下肢的區域](../Page/下肢.md "wikilink")。所以在[軀幹肌肉列表和](../Page/人體肌肉列表#軀幹的肌肉#軀幹肌肉列表.md "wikilink")[下肢肌肉列表當中](../Page/人體肌肉列表#下肢的肌肉#下肢肌肉列表.md "wikilink")，都會出現[腰小肌](../Page/腰小肌.md "wikilink")。
5.  [提睪肌是屬於](../Page/提睪肌.md "wikilink")[腹部的肌肉](../Page/腹部.md "wikilink")，不過它不是腹部前面的肌肉，也不是腹部後面的肌肉，它是屬於腹部下方的肌肉\!所以它被歸類為腹部特殊的肌肉。
6.  [髂部又稱為](../Page/髂部.md "wikilink")[髖部](../Page/髖部.md "wikilink")。
7.  [髂肌又稱為](../Page/髂肌.md "wikilink")[胯肌](../Page/胯肌.md "wikilink")，是屬於靠近[腰部以及](../Page/腰部.md "wikilink")[胯部的](../Page/胯部.md "wikilink")[肌肉](../Page/肌肉.md "wikilink")。
8.  [大腿後肌又稱為膕旁肌](../Page/大腿後肌.md "wikilink")（Hamstrings），因為這些[肌肉位於](../Page/肌肉.md "wikilink")[膕肌的旁邊](../Page/膕肌.md "wikilink")，所以大腿後肌才會又稱為"膕旁肌"。參考資料：[認識膕旁肌群](http://m.xuite.net/blog/im_minijessica/TristaLoveTaiwan/226666019)
9.  。