[Diencephalon_small.gif](https://zh.wikipedia.org/wiki/File:Diencephalon_small.gif "fig:Diencephalon_small.gif")
**间脑（diencephalon）**位於[端脑与](../Page/端脑.md "wikilink")[中脑之间](../Page/中脑.md "wikilink")，大部份被大脑两侧半球所遮盖，间脑呈楔形，下部与中脑相连。间脑主要分为[丘脑和](../Page/丘脑.md "wikilink")[下丘脑](../Page/下丘脑.md "wikilink")，细分可分为背侧丘脑、上丘脑、后丘脑、下丘脑和底丘脑。\[1\]\[2\]

## 功能

间脑是脊椎动物胚胎神经管中，产生后前脑结构的部分，包括[丘脑](../Page/丘脑.md "wikilink")、[下丘脑](../Page/下丘脑.md "wikilink")、[垂体后部和](../Page/垂体.md "wikilink")[松果体](../Page/松果体.md "wikilink")。下丘脑执行大量的重要功能，其中大部分直接或间接与通过其他脑区和[自主神经系统调节與内脏活动有关](../Page/自主神经系统.md "wikilink")\[3\]。

## 間腦症候群

[1310_Diencephalon.jpg](https://zh.wikipedia.org/wiki/File:1310_Diencephalon.jpg "fig:1310_Diencephalon.jpg")、底丘腦、[下丘腦以及上丘腦](../Page/下丘腦.md "wikilink")。\]\]
間腦症候群的特徵是一種天生罕見擁有發育停滯[神經系統的](../Page/神經系統.md "wikilink")[疾病](../Page/疾病.md "wikilink")。儘管身理正常或略有減少熱量攝入、運動機能亢進、和亢奮感，皆有嚴重消瘦的現象。皮膚蒼白不太常見，無[貧血](../Page/貧血.md "wikilink")、[低血糖和](../Page/低血糖.md "wikilink")[低血壓](../Page/低血壓.md "wikilink")\[4\]。間腦症候群可能會影響到自身的成長，也可能導致致命的原因，未能成長茁壯的幼兒平均只有七個月的年齡\[5\]。值得注意的是該症狀不與發育遲緩有關，而是與腦積水有關\[6\]。

間腦症候群於1951年由[罗素首次介紹](../Page/罗素.md "wikilink")\[7\]。雖然因為有過多的β促脂解素的分泌\[8\]和[代謝需求整體上升](../Page/代謝.md "wikilink")\[9\]，使不適當高的[生長激素釋放已被提出](../Page/生長激素.md "wikilink")\[10\]，但是位於下丘腦
-
光視交叉區，通常會引起如低級別膠質瘤或星形細胞瘤的[腦腫瘤](../Page/腦腫瘤.md "wikilink")，然而尚未了解到如何影響到食慾和新陳代謝所引起的間腦症候群。

## 相關圖片

Image:EmbryonicBrain.svg |圖片描繪脊椎動物胚胎大腦的主要結構的細節。這些區域分成大腦、中腦和後腦的結構。
Image:Gray645.png |10.2毫米人類周圍神經的構造（標籤間腦位於左側）。

## 參考資料

### 書籍、網站資源

### 醫學資源

[间脑](../Category/间脑.md "wikilink")
[Category:發育生物學](../Category/發育生物學.md "wikilink")

1.

2.

3.
4.

5.
6.

7.

8.
9.

10.