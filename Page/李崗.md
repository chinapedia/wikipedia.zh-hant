**李崗**（，），[電影導演](../Page/電影導演.md "wikilink")[李安的胞弟](../Page/李安.md "wikilink")，[台灣電影](../Page/台灣電影.md "wikilink")[導演](../Page/導演.md "wikilink")，電影編劇、作家、電視節目主持人
。[國立臺灣海洋大學航海系畢業](../Page/國立臺灣海洋大學.md "wikilink")，1996年以《今天不回家》獲[亞太影展最佳編劇獎](../Page/亞太影展.md "wikilink")，1999年執導《條子阿不拉》，2002年成立[雷公電影](../Page/雷公電影.md "wikilink")（Zeus
International Production
Ltd.）。現任[安可電影股份有限公司董事長](../Page/安可電影.md "wikilink")。

## 個人得獎紀錄

  - 《新老殘遊記》：八十三年度優良電影劇本
  - 《上岸》：八十五年度優良電影劇本，拍成電影《今天不回家》[張艾嘉導演](../Page/張艾嘉.md "wikilink")，獲第四十一屆（1996）亞太影展最佳編劇
  - 《[條子阿不拉](../Page/條子阿不拉.md "wikilink")》：八十七年度優良電影劇本；八十七年度國片一千萬元輔導金補助；1999台北電影節商業推薦評估獎評審團特別推薦，第四十四屆（1999）亞太影展參賽片，第二屆（2000）杜維爾影展亞洲電影節競賽

## 其他電影作品

  - 《[星光傳奇](../Page/星光傳奇.md "wikilink")》2008 ，製片人
  - 《[陽陽](../Page/陽陽.md "wikilink")》2009，製片人；2009
    德國[柏林影展Panorama電影大觀](../Page/柏林影展.md "wikilink")，2009
    33th香港國際電影節「台灣新生代」單元

，2009 第11屆台北電影節劇情長片類 評審團特別獎,劇情長片類
最佳女演員([張榕容](../Page/張榕容.md "wikilink")),劇情長片類
最佳音樂([林強](../Page/林強.md "wikilink"))

  - 《[茱麗葉](../Page/茱麗葉.md "wikilink")》 2010，製片人，
    第47屆金馬獎「最佳新演員([李千娜](../Page/李千娜.md "wikilink"))」
  - 《野蓮香》(藍光) 2012
  - 《吉林的月光》(藍光) 2012
  - 《[阿罩霧風雲：抉擇](../Page/阿罩霧風雲：抉擇.md "wikilink")》2013 ，監製
  - 《[想飛](../Page/想飛_\(2014年電影\).md "wikilink")》2014 ，導演
  - 《[阿罩霧風雲II：落子](../Page/阿罩霧風雲II：落子.md "wikilink")》2015 ，監製

## 其他作品

2001年(公視人生劇展)《情人的眼淚》導演

2003年[中視電視劇](../Page/中視.md "wikilink")《[粉紅教父小甜甜](../Page/粉紅教父小甜甜.md "wikilink")》導演

## 外部链接

  - [台灣電影筆記：李崗](https://web.archive.org/web/20091223021419/http://movie.cca.gov.tw/files/15-1000-1452,c179-1.php)

  -
  -
  -
  -
[L李岗](../Category/台湾导演.md "wikilink") [G岗](../Category/李姓.md "wikilink")
[L](../Category/國立臺灣海洋大學校友.md "wikilink")
[L](../Category/江西裔台灣人.md "wikilink")
[Category:亞太影展獲獎者](../Category/亞太影展獲獎者.md "wikilink")
[Category:李安](../Category/李安.md "wikilink")