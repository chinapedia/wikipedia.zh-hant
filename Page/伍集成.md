[HK_Night_Wan_Chai_C_C_Wu_Building_Foundation_Stone_a.jpg](https://zh.wikipedia.org/wiki/File:HK_Night_Wan_Chai_C_C_Wu_Building_Foundation_Stone_a.jpg "fig:HK_Night_Wan_Chai_C_C_Wu_Building_Foundation_Stone_a.jpg")的基石，由其[基金會尊立](../Page/基金會.md "wikilink")\]\]
**伍集成**()，是原籍[雲南](../Page/雲南.md "wikilink")，[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")，[灣仔](../Page/灣仔.md "wikilink")[集成中心大業主](../Page/集成中心.md "wikilink")，並被譽為棉紗大王\[1\]。

## 生平

伍集成生於雲南滇西[騰衝縣](../Page/騰衝縣.md "wikilink")，祖父輩為典型的[儒學](../Page/儒學.md "wikilink")[家庭](../Page/家庭.md "wikilink")，父親與[李根源是永昌府試同榜人](../Page/李根源.md "wikilink")，有四位長輩都是讀書人，大家族人，四代同堂。伍集成少年時常看《[三國演義](../Page/三國演義.md "wikilink")》、《[紅樓夢](../Page/紅樓夢.md "wikilink")》、《[水滸](../Page/水滸.md "wikilink")》、《岳飛傳》等，並此書中故事激勵同輩。他行事不做違心事、忠厚留餘地。

在[昆明](../Page/昆明.md "wikilink")，伍集成跟[實業家](../Page/實業家.md "wikilink")[繆雲台學習](../Page/繆雲台.md "wikilink")，心懷[國家](../Page/國家.md "wikilink")[民族情懷](../Page/民族.md "wikilink")。[第二次世界大戰結束](../Page/第二次世界大戰.md "wikilink")，於1946年伍集成舉家離[昆明](../Page/昆明.md "wikilink")，移居香港，先在灣仔買下一幅[地皮作為商業基地](../Page/地皮.md "wikilink")，創立瑞成國際貿易有限公司，從事[棉花](../Page/棉花.md "wikilink")[棉紗貿易](../Page/棉紗.md "wikilink")，發展[東南亞花紗供應網](../Page/東南亞.md "wikilink")，於[越南](../Page/越南.md "wikilink")、[印度](../Page/印度.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國等地設立](../Page/泰國.md "wikilink")[子公司](../Page/子公司.md "wikilink")，由[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[埃及等產地買入](../Page/埃及.md "wikilink")[棉花](../Page/棉花.md "wikilink")，供應香港[紡織廠](../Page/紡織廠.md "wikilink")，當年[香港紡織業興盛](../Page/香港紡織業.md "wikilink")，[工廠林立](../Page/工廠.md "wikilink")，[上海幫](../Page/上海幫.md "wikilink")、[廣東幫](../Page/廣東.md "wikilink")[廠家](../Page/廠家.md "wikilink")[顧客達](../Page/顧客.md "wikilink")3百多家，每年[銷售棉花和棉紗各幾萬包](../Page/銷售.md "wikilink")，因此興家立業，成為香港實業家。

1980年，伍集成在[公司灣仔地皮上](../Page/公司.md "wikilink")，建築了一座商業大廈，1983年落成，名為[集成中心](../Page/集成中心.md "wikilink")，作為[出租](../Page/出租.md "wikilink")[投資物業](../Page/投資物業.md "wikilink")。

伍集成育有[女兒](../Page/女兒.md "wikilink")[伍宗琳](../Page/伍宗琳.md "wikilink")，兒子[伍達觀](../Page/伍達觀.md "wikilink")。

## 職稱

  - [集成文化教育基金會董事長](../Page/集成文化教育基金會.md "wikilink")
  - 雲南香港同鄉會前會長
  - 香港瑞成有限公司[董事長](../Page/董事長.md "wikilink")
  - 雲南海外交流協會名譽會長
  - 雲南省政府顧問

## 外部参考

  - 香港《鏡報》

[W伍](../Category/腾冲人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港慈善家](../Category/香港慈善家.md "wikilink")
[J](../Category/伍姓.md "wikilink")

1.  [反價至7850萬沽"](https://news.mingpao.com/pns/dailynews/web_tc/article/20170920/s00004/1505844124325%22BELGRAVIA四房),*明報專訊*,2017年9月19日