[CronquistPlantSystemTree.PNG](https://zh.wikipedia.org/wiki/File:CronquistPlantSystemTree.PNG "fig:CronquistPlantSystemTree.PNG")
**克朗奎斯特分类法**是1958年[美国学者](../Page/美国.md "wikilink")[阿瑟·約翰·克朗奎斯特](../Page/阿瑟·約翰·克朗奎斯特.md "wikilink")（Arthur
John
Cronquist，1919年3月19日－1992年3月22日）发表的一种对[有花植物进行分类的体系](../Page/被子植物门.md "wikilink")，1981年在他的著作《[有花植物的综合分类系统](../Page/有花植物的综合分类系统.md "wikilink")》中最终完善\[1\]。

克朗奎斯特分类法将被子植物分为木兰纲（[双子叶植物纲](../Page/双子叶植物纲.md "wikilink")）及百合纲（[单子叶植物纲](../Page/单子叶植物纲.md "wikilink")）两大纲，是數個常被使用的被子植物分類系統之一。當時流行的高等植物分類系統有克朗奎斯特
(1981年、1983年、1988年)、[塔赫塔江](../Page/亚美因·塔赫塔江.md "wikilink")
(1969年、1980年、1983年、1991年)、 Stebbins
(1974年)、Dahlgren等相關的系統\[2\]。但因為[分子生物学的發展](../Page/分子生物学.md "wikilink")，目前學者倾向採納《[被子植物APG分类法](../Page/被子植物APG_III分类法.md "wikilink")》。

克朗奎斯特分类法中，木兰纲（[双子叶植物纲](../Page/双子叶植物纲.md "wikilink")）包括64个[目和](../Page/目_\(生物\).md "wikilink")383个[科](../Page/科_\(生物\).md "wikilink")，百合纲（[单子叶植物纲](../Page/单子叶植物纲.md "wikilink")）包括19个[目](../Page/目_\(生物\).md "wikilink")：

## [木兰纲](../Page/双子叶植物纲.md "wikilink") Magnoliopsida

1.  [木兰亚纲](../Page/木兰亚纲.md "wikilink") Magnoliidae （最原始的双子叶植物）
    1.  [木兰目](../Page/木兰目.md "wikilink") Magnoliales
        1.  [林仙科](../Page/林仙科.md "wikilink") Winteraceae
        2.  [单室木兰科](../Page/单室木兰科.md "wikilink") Degeneriaceae
        3.  [舌蕊花科](../Page/舌蕊花科.md "wikilink") Himantandraceae
        4.  [帽花木科](../Page/帽花木科.md "wikilink") Eupomatiaceae
        5.  [木兰藤科](../Page/木兰藤科.md "wikilink") Austrobaileyaceae
        6.  [木兰科](../Page/木兰科.md "wikilink") Magnoliaceae
        7.  [短蕊花科](../Page/短蕊花科.md "wikilink") Lactoridaceae
        8.  [番荔枝科](../Page/番荔枝科.md "wikilink") Annonaceae
        9.  [肉豆蔻科](../Page/肉豆蔻科.md "wikilink") Myristicaceae
        10. [白桂皮科](../Page/白桂皮科.md "wikilink") Canellaceae
    2.  [樟目](../Page/樟目.md "wikilink") Laurales
        1.  [无油樟科](../Page/无油樟科.md "wikilink") Amborellaceae
        2.  [腺齿木科](../Page/腺齿木科.md "wikilink") Trimeniaceae
        3.  [杯轴花科](../Page/杯轴花科.md "wikilink") Monimiaceae
        4.  [腺蕊花科](../Page/腺蕊花科.md "wikilink") Gomortegaceae
        5.  [腊梅科](../Page/腊梅科.md "wikilink") Calycanthaceae
        6.  [奇子树科](../Page/奇子树科.md "wikilink") Idiospermaceae
        7.  [樟科](../Page/樟科.md "wikilink") Lauraceae
        8.  [莲叶桐科](../Page/莲叶桐科.md "wikilink") Hernandiaceae
    3.  [胡椒目](../Page/胡椒目.md "wikilink") Piperales
        1.  [金粟兰科](../Page/金粟兰科.md "wikilink") Chloranthaceae
        2.  [三白草科](../Page/三白草科.md "wikilink") Saururaceae
        3.  [胡椒科](../Page/胡椒科.md "wikilink") Piperaceae
    4.  [马兜铃目](../Page/马兜铃目.md "wikilink") Aristolochiales
        1.  [马兜铃科](../Page/马兜铃科.md "wikilink") Aristolochiaceae
    5.  [八角茴香目](../Page/八角茴香目.md "wikilink") Illiciales
        1.  [八角茴香科](../Page/八角茴香科.md "wikilink") Illiciaceae
        2.  [五味子科](../Page/五味子科.md "wikilink") Schisandraceae
    6.  [睡莲目](../Page/睡莲目.md "wikilink") Nymphaeales
        1.  [莲科](../Page/莲科.md "wikilink") Nelumbonaceae
        2.  [睡莲科](../Page/睡莲科.md "wikilink") Nymphaeaceae
        3.  [合瓣莲科](../Page/合瓣莲科.md "wikilink") Barclayaceae
        4.  [莼菜科](../Page/莼菜科.md "wikilink") Cabombaceae
        5.  [金鱼藻科](../Page/金鱼藻科.md "wikilink") Ceratophyllaceae
    7.  [毛茛目](../Page/毛茛目.md "wikilink") Ranunculales
        1.  [毛茛科](../Page/毛茛科.md "wikilink") Ranunculaceae
        2.  [星叶科](../Page/星叶科.md "wikilink") Circaeasteraceae
        3.  [小檗科](../Page/小檗科.md "wikilink") Berberidaceae
        4.  [大血藤科](../Page/大血藤科.md "wikilink") Sargentodoxaceae
        5.  [木通科](../Page/木通科.md "wikilink") Lardizabalaceae
        6.  [防己科](../Page/防己科.md "wikilink") Menispermaceae
        7.  [马桑科](../Page/马桑科.md "wikilink") Coriariaceae
        8.  [清风藤科](../Page/清风藤科.md "wikilink") Sabiaceae
    8.  [罂粟目](../Page/罂粟目.md "wikilink") Papaverales
        1.  [罂粟科](../Page/罂粟科.md "wikilink") Papaveraceae
        2.  [紫堇科](../Page/紫堇科.md "wikilink") Fumariaceae

<!-- end list -->

1.  [金缕梅亚纲](../Page/金缕梅亚纲.md "wikilink") Hamamelidae *Hamamelididae*
    1.  [昆栏树目](../Page/昆栏树目.md "wikilink") Trochodendrales
        1.  [水青树科](../Page/水青树科.md "wikilink") Tetracentraceae
        2.  [昆栏树科](../Page/昆栏树科.md "wikilink") Trochodendraceae
    2.  [金缕梅目](../Page/金缕梅目.md "wikilink") Hamamelidales
        1.  [连香树科](../Page/连香树科.md "wikilink") Cercidiphyllaceae
        2.  [领春木科](../Page/领春木科.md "wikilink") Eupteleaceae
        3.  [悬铃木科](../Page/悬铃木科.md "wikilink") Platanaceae
        4.  [金缕梅科](../Page/金缕梅科.md "wikilink") Hamamelidaceae
        5.  [香灌木科](../Page/香灌木科.md "wikilink") Myrothamnaceae
    3.  [交让木目](../Page/交让木目.md "wikilink") Daphniphyllales
        1.  [交让木科](../Page/交让木科.md "wikilink") Daphniphyllaceae
    4.  [对药树目](../Page/对药树目.md "wikilink") Didymelales
        1.  [对药树科](../Page/对药树科.md "wikilink") Didymelaceae
    5.  [杜仲目](../Page/杜仲目.md "wikilink") Eucommiales
        1.  [杜仲科](../Page/杜仲科.md "wikilink") Eucommiaceae
    6.  [荨麻目](../Page/荨麻目.md "wikilink") Urticales
        1.  [钩毛叶科](../Page/钩毛叶科.md "wikilink") Barbeyaceae
        2.  [榆科](../Page/榆科.md "wikilink") Ulmaceae
        3.  [大麻科](../Page/大麻科.md "wikilink") Cannabaceae
        4.  [桑科](../Page/桑科.md "wikilink") Moraceae
        5.  [伞树科](../Page/伞树科.md "wikilink") Cecropiaceae
        6.  [荨麻科](../Page/荨麻科.md "wikilink") Urticaceae
    7.  [塞子木目](../Page/塞子木目.md "wikilink") Leitneriales
        1.  [塞子木科](../Page/塞子木科.md "wikilink") Leitneriaceae
    8.  [胡桃目](../Page/胡桃目.md "wikilink") Juglandales
        1.  [马尾树科](../Page/马尾树科.md "wikilink") Rhoipteleaceae
        2.  [胡桃科](../Page/胡桃科.md "wikilink") Juglandaceae
    9.  [杨梅目](../Page/杨梅目.md "wikilink") Myricales
        1.  [杨梅科](../Page/杨梅科.md "wikilink") Myricaceae
    10. [山毛榉目](../Page/壳斗目.md "wikilink") Fagales
        1.  [槲树果科](../Page/槲树果科.md "wikilink") Balanopaceae
        2.  [太果木科](../Page/太果木科.md "wikilink") Ticodendraceae
        3.  [山毛榉科](../Page/山毛榉科.md "wikilink") Fagaceae
        4.  [南青冈科](../Page/南青冈科.md "wikilink") Nothofagaceae
        5.  [桦木科](../Page/桦木科.md "wikilink") Betulaceae
    11. [木麻黄目](../Page/木麻黄目.md "wikilink") Casuarinales
        1.  [木麻黄科](../Page/木麻黄科.md "wikilink") Casuarinaceae

<!-- end list -->

1.  [石竹亚纲](../Page/石竹亚纲.md "wikilink") Caryophyllidae
    1.  [石竹目](../Page/石竹目.md "wikilink") Caryophyllales
        1.  [商陆科](../Page/商陆科.md "wikilink") Phytolaccaceae
        2.  [玛瑙果科](../Page/玛瑙果科.md "wikilink") Achatocarpaceae
        3.  [紫茉莉科](../Page/紫茉莉科.md "wikilink") Nyctaginaceae
        4.  [番杏科](../Page/番杏科.md "wikilink") Aizoaceae
        5.  [龙树科](../Page/龙树科.md "wikilink") Didiereaceae
        6.  [仙人掌科](../Page/仙人掌科.md "wikilink") Cactaceae
        7.  [藜科](../Page/藜科.md "wikilink") Chenopodiaceae
        8.  [苋科](../Page/苋科.md "wikilink") Amaranthaceae
        9.  [马齿苋科](../Page/马齿苋科.md "wikilink") Portulacaceae
        10. [落葵科](../Page/落葵科.md "wikilink") Basellaceae
        11. [粟米草科](../Page/粟米草科.md "wikilink") Molluginaceae
        12. [石竹科](../Page/石竹科.md "wikilink") Caryophyllaceae
    2.  [蓼目](../Page/蓼目.md "wikilink") Polygonales
        1.  [蓼科](../Page/蓼科.md "wikilink") Polygonaceae
    3.  [蓝雪目](../Page/蓝雪目.md "wikilink") Plumbaginales
        1.  [蓝雪科](../Page/蓝雪科.md "wikilink") Plumbaginaceae

<!-- end list -->

1.  [五桠果亚纲](../Page/五桠果亚纲.md "wikilink") Dilleniidae
    1.  [五桠果目](../Page/五桠果目.md "wikilink") Dilleniales
        1.  [五桠果科](../Page/五桠果科.md "wikilink") Dilleniaceae
        2.  [芍药科](../Page/芍药科.md "wikilink") Paeoniaceae
    2.  [山茶目](../Page/山茶目.md "wikilink") Theales
        1.  [金莲木科](../Page/金莲木科.md "wikilink") Ochnaceae
        2.  [球萼树科](../Page/球萼树科.md "wikilink") Sphaerosepalaceae
        3.  [旋花树科](../Page/旋花树科.md "wikilink") Sarcolaenaceae
        4.  [龙脑香科](../Page/龙脑香科.md "wikilink") Dipterocarpaceae
        5.  [油桃木科](../Page/油桃木科.md "wikilink") Caryocaraceae
        6.  [山茶科](../Page/山茶科.md "wikilink") Theaceae
        7.  [猕猴桃科](../Page/猕猴桃科.md "wikilink") Actinidiaceae
        8.  [木果树科](../Page/木果树科.md "wikilink") Scytopetalaceae
        9.  [五列木科](../Page/五列木科.md "wikilink") Pentaphylacaceae
        10. [四籽树科](../Page/四籽树科.md "wikilink") Tetrameristaceae
        11. [假红树科](../Page/假红树科.md "wikilink") Pellicieraceae
        12. [五蕊茶科](../Page/五蕊茶科.md "wikilink") Oncothecaceae
        13. [蜜囊花科](../Page/蜜囊花科.md "wikilink") Marcgraviaceae
        14. [羽叶树科](../Page/羽叶树科.md "wikilink") Quiinaceae
        15. [沟繁缕科](../Page/沟繁缕科.md "wikilink") Elatinaceae
        16. [八蕊树科](../Page/八蕊树科.md "wikilink") Paracryphiaceae
        17. [伞果树科](../Page/伞果树科.md "wikilink") Medusagynaceae
        18. [藤黄科](../Page/藤黄科.md "wikilink") Clusiaceae
    3.  [锦葵目](../Page/锦葵目.md "wikilink") Malvales
        1.  [杜英科](../Page/杜英科.md "wikilink") Elaeocarpaceae
        2.  [椴树科](../Page/椴树科.md "wikilink") Tiliaceae
        3.  [梧桐科](../Page/梧桐科.md "wikilink") Sterculiaceae
        4.  [木棉科](../Page/木棉科.md "wikilink") Bombacaceae
        5.  [锦葵科](../Page/锦葵科.md "wikilink") Malvaceae
    4.  [玉蕊目](../Page/玉蕊目.md "wikilink") Lecythidales
        1.  [玉蕊科](../Page/玉蕊科.md "wikilink") Lecythidaceae
    5.  [猪笼草目](../Page/猪笼草目.md "wikilink") Nepenthales
        1.  [瓶子草科](../Page/瓶子草科.md "wikilink") Sarraceniaceae
        2.  [猪笼草科](../Page/猪笼草科.md "wikilink") Nepenthaceae
        3.  [茅膏菜科](../Page/茅膏菜科.md "wikilink") Droseraceae
    6.  [堇菜目](../Page/堇菜目.md "wikilink") Violales
        1.  [大风子科](../Page/大风子科.md "wikilink") Flacourtiaceae
        2.  [围盘树科](../Page/围盘树科.md "wikilink") Peridiscaceae
        3.  [红木科](../Page/红木科.md "wikilink") Bixaceae
        4.  [半日花科](../Page/半日花科.md "wikilink") Cistaceae
        5.  [蒜树科](../Page/蒜树科.md "wikilink") Huaceae
        6.  [裂药花科](../Page/裂药花科.md "wikilink") Lacistemataceae
        7.  [杯盖花科](../Page/杯盖花科.md "wikilink") Scyphostegiaceae
        8.  [旌节花科](../Page/旌节花科.md "wikilink") Stachyuraceae
        9.  [堇菜科](../Page/堇菜科.md "wikilink") Violaceae
        10. [柽柳科](../Page/柽柳科.md "wikilink") Tamaricaceae
        11. [瓣鳞花科](../Page/瓣鳞花科.md "wikilink") Frankeniaceae
        12. [双钩叶科](../Page/双钩叶科.md "wikilink") Dioncophyllaceae
        13. [钩枝藤科](../Page/钩枝藤科.md "wikilink") Ancistrocladaceae
        14. [时钟花科](../Page/时钟花科.md "wikilink") Turneraceae
        15. [王冠草科](../Page/王冠草科.md "wikilink") Malesherbiaceae
        16. [西番莲科](../Page/西番莲科.md "wikilink") Passifloraceae
        17. [钟花科](../Page/钟花科.md "wikilink") Achariaceae
        18. [番木瓜科](../Page/番木瓜科.md "wikilink") Caricaceae
        19. [福桂花科](../Page/福桂花科.md "wikilink") Fouquieriaceae
        20. [单柱花科](../Page/单柱花科.md "wikilink") Hoplestigmataceae
        21. [葫芦科](../Page/葫芦科.md "wikilink") Cucurbitaceae
        22. [四数木科](../Page/四数木科.md "wikilink") Datiscaceae
        23. [秋海棠科](../Page/秋海棠科.md "wikilink") Begoniaceae
        24. [刺莲花科](../Page/刺莲花科.md "wikilink") Loasaceae
    7.  [杨柳目](../Page/杨柳目.md "wikilink") Salicales
        1.  [杨柳科](../Page/杨柳科.md "wikilink") Salicaceae
    8.  [白花菜目](../Page/白花菜目.md "wikilink") Capparales
        1.  [烈味三叶草科](../Page/烈味三叶草科.md "wikilink") Tovariaceae
        2.  [白花菜科](../Page/白花菜科.md "wikilink") Capparaceae
        3.  [十字花科](../Page/十字花科.md "wikilink") Brassicaceae
        4.  [辣木科](../Page/辣木科.md "wikilink") Moringaceae
        5.  [木犀草科](../Page/木犀草科.md "wikilink") Resedaceae
    9.  [肉穗果目](../Page/肉穗果目.md "wikilink") Batales
        1.  [环蕊科](../Page/环蕊科.md "wikilink") Gyrostemonaceae
        2.  [肉穗果科](../Page/肉穗果科.md "wikilink") Bataceae
    10. [杜鹃花目](../Page/杜鹃花目.md "wikilink") Ericales
        1.  [翅萼树科](../Page/翅萼树科.md "wikilink") Cyrillaceae
        2.  [山柳科](../Page/山柳科.md "wikilink") Clethraceae
        3.  [假石南科](../Page/假石南科.md "wikilink") Grubbiaceae
        4.  [岩高兰科](../Page/岩高兰科.md "wikilink") Empetraceae
        5.  [尖苞树科](../Page/尖苞树科.md "wikilink") Epacridaceae
        6.  [杜鹃花科](../Page/杜鹃花科.md "wikilink") Ericaceae
        7.  [鹿蹄草科](../Page/鹿蹄草科.md "wikilink") Pyrolaceae
        8.  [水晶兰科](../Page/水晶兰科.md "wikilink") Monotropaceae
    11. [岩梅目](../Page/岩梅目.md "wikilink") Diapensiales
        1.  [岩梅科](../Page/岩梅科.md "wikilink") Diapensiaceae
    12. [柿树目](../Page/柿树目.md "wikilink") Ebenales
        1.  [山榄科](../Page/山榄科.md "wikilink") Sapotaceae
        2.  [柿树科](../Page/柿树科.md "wikilink") Ebenaceae
        3.  [野茉莉科](../Page/野茉莉科.md "wikilink") Styracaceae
            （[安息香科](../Page/安息香科.md "wikilink")）
        4.  [光果科](../Page/光果科.md "wikilink") Lissocarpaceae
            （[尖药科](../Page/尖药科.md "wikilink")）
        5.  [山矾科](../Page/山矾科.md "wikilink") Symplocaceae
    13. [报春花目](../Page/报春花目.md "wikilink") Primulales
        1.  [假轮叶科](../Page/假轮叶科.md "wikilink") Theophrastaceae
        2.  [紫金牛科](../Page/紫金牛科.md "wikilink") Myrsinaceae
        3.  [报春花科](../Page/报春花科.md "wikilink") Primulaceae

<!-- end list -->

1.  [蔷薇亚纲](../Page/蔷薇亚纲.md "wikilink") Rosidae
    1.  [蔷薇目](../Page/蔷薇目.md "wikilink") Rosales
        1.  [瓣裂果科](../Page/瓣裂果科.md "wikilink") Brunelliaceae
        2.  [牛拴藤科](../Page/牛拴藤科.md "wikilink") Connaraceae
        3.  [船形果科](../Page/船形果科.md "wikilink") Eucryphiaceae
        4.  [火把树科](../Page/火把树科.md "wikilink") Cunoniaceae
        5.  [澳楸科](../Page/澳楸科.md "wikilink") Davidsoniaceae
        6.  [毛枝树科](../Page/毛枝树科.md "wikilink") Dialypetalanthaceae
        7.  [海桐花科](../Page/海桐花科.md "wikilink") Pittosporaceae
        8.  [腺毛草科](../Page/腺毛草科.md "wikilink") Byblidaceae
        9.  [八仙花科](../Page/八仙花科.md "wikilink") Hydrangeaceae
        10. [弯药树科](../Page/弯药树科.md "wikilink") Columelliaceae
        11. [茶藨子科](../Page/茶藨子科.md "wikilink") Grossulariaceae
        12. [鞘叶树科](../Page/鞘叶树科.md "wikilink") Greyiaceae
        13. [鳞叶树科](../Page/鳞叶树科.md "wikilink") Bruniaceae
        14. [四柱木科](../Page/四柱木科.md "wikilink") Anisophylleaceae
        15. [假海桐科](../Page/假海桐科.md "wikilink") Alseuosmiaceae
        16. [景天科](../Page/景天科.md "wikilink") Crassulaceae
        17. [土瓶草科](../Page/土瓶草科.md "wikilink") Cephalotaceae
        18. [虎耳草科](../Page/虎耳草科.md "wikilink") Saxifragaceae
        19. [蔷薇科](../Page/蔷薇科.md "wikilink") Rosaceae
        20. [沙莓科](../Page/沙莓科.md "wikilink") Neuradaceae
        21. [燧体木科](../Page/燧体木科.md "wikilink") Crossosomataceae
        22. [金壳果科](../Page/金壳果科.md "wikilink") Chrysobalanaceae
        23. [海人树科](../Page/海人树科.md "wikilink") Surianaceae
        24. [棒木科](../Page/棒木科.md "wikilink") Rhabdodendraceae
    2.  [豆目](../Page/豆目.md "wikilink") Fabales
        1.  [含羞草科](../Page/含羞草科.md "wikilink") Mimosaceae
        2.  [云实科](../Page/云实科.md "wikilink") Caesalpiniaceae
        3.  [豆科](../Page/豆科.md "wikilink") Fabaceae
    3.  [山龙眼目](../Page/山龙眼目.md "wikilink") Proteales
        1.  [胡颓子科](../Page/胡颓子科.md "wikilink") Elaeagnaceae
        2.  [山龙眼科](../Page/山龙眼科.md "wikilink") Proteaceae
    4.  [川苔草目](../Page/川苔草目.md "wikilink") Podostemales
        1.  [川苔草科](../Page/川苔草科.md "wikilink") Podostemaceae
    5.  [小二仙草目](../Page/小二仙草目.md "wikilink") Haloragales
        1.  [小二仙草科](../Page/小二仙草科.md "wikilink") Haloragaceae
        2.  [洋二仙草科](../Page/洋二仙草科.md "wikilink") Gunneraceae
    6.  [桃金娘目](../Page/桃金娘目.md "wikilink") Myrtales
        1.  [海桑科](../Page/海桑科.md "wikilink") Sonneratiaceae
        2.  [千屈菜科](../Page/千屈菜科.md "wikilink") Lythraceae
        3.  [管萼科](../Page/管萼科.md "wikilink") Penaeaceae
        4.  [隐翼科](../Page/隐翼科.md "wikilink") Crypteroniaceae
        5.  [瑞香科](../Page/瑞香科.md "wikilink") Thymelaeaceae
        6.  [菱科](../Page/菱科.md "wikilink") Trapaceae
        7.  [桃金娘科](../Page/桃金娘科.md "wikilink") Myrtaceae
        8.  [石榴科](../Page/石榴科.md "wikilink") Punicaceae
        9.  [柳叶菜科](../Page/柳叶菜科.md "wikilink") Onagraceae
        10. [方枝树科](../Page/方枝树科.md "wikilink") Oliniaceae
        11. [野牡丹科](../Page/野牡丹科.md "wikilink") Melastomataceae
        12. [使君子科](../Page/使君子科.md "wikilink") Combretaceae
        13. [双翼果科](../Page/双翼果科.md "wikilink") Alzateaceae
        14. [谷木科](../Page/谷木科.md "wikilink") Memecylaceae
        15. [喙萼花科](../Page/喙萼花科.md "wikilink") Rhynchocalycaceae
    7.  [红树目](../Page/红树目.md "wikilink") Rhizophorales
        1.  [红树科](../Page/红树科.md "wikilink") Rhizophoraceae
    8.  [山茱萸目](../Page/山茱萸目.md "wikilink") Cornales
        1.  [八角枫科](../Page/八角枫科.md "wikilink") Alangiaceae
        2.  [珙桐科](../Page/珙桐科.md "wikilink") Nyssaceae
        3.  [山茱萸科](../Page/山茱萸科.md "wikilink") Cornaceae
        4.  [绞木科](../Page/绞木科.md "wikilink") Garryaceae
    9.  [檀香目](../Page/檀香目.md "wikilink") Santalales
        1.  [毛丝花科](../Page/毛丝花科.md "wikilink") Medusandraceae
        2.  [十齿花科](../Page/十齿花科.md "wikilink") Dipentodontaceae
        3.  [铁青树科](../Page/铁青树科.md "wikilink") Olacaceae
        4.  [山柚子科](../Page/山柚子科.md "wikilink") Opiliaceae
        5.  [檀香科](../Page/檀香科.md "wikilink") Santalaceae
        6.  [羽毛果科](../Page/羽毛果科.md "wikilink") Misodendraceae
        7.  [桑寄生科](../Page/桑寄生科.md "wikilink") Loranthaceae
        8.  [槲寄生科](../Page/槲寄生科.md "wikilink") Viscaceae
        9.  [房底珠科](../Page/房底珠科.md "wikilink") Eremolepidaceae
        10. [蛇菰科](../Page/蛇菰科.md "wikilink") Balanophoraceae
    10. [大花草目](../Page/大花草目.md "wikilink") Rafflesiales
        1.  [菌花科](../Page/菌花科.md "wikilink") Hydnoraceae
        2.  [帽蕊草科](../Page/帽蕊草科.md "wikilink") Mitrastemonaceae
        3.  [大花草科](../Page/大花草科.md "wikilink") Rafflesiaceae
    11. [卫矛目](../Page/卫矛目.md "wikilink") Celastrales
        1.  [四棱果科](../Page/四棱果科.md "wikilink") Geissolomataceae
        2.  [卫矛科](../Page/卫矛科.md "wikilink") Celastraceae
        3.  [翅子藤科](../Page/翅子藤科.md "wikilink") Hippocrateaceae
        4.  [木根草科](../Page/木根草科.md "wikilink") Stackhousiaceae
        5.  [刺茉莉科](../Page/刺茉莉科.md "wikilink") Salvadoraceae
        6.  [冬青科](../Page/冬青科.md "wikilink") Aquifoliaceae
        7.  [茶茱萸科](../Page/茶茱萸科.md "wikilink") Icacinaceae
        8.  [鳞枝树科](../Page/鳞枝树科.md "wikilink") Aextoxicaceae
        9.  [苦皮树科](../Page/苦皮树科.md "wikilink") Tepuianthaceae
        10. [心翼果科](../Page/心翼果科.md "wikilink") Cardiopteridaceae
        11. [棒果木科](../Page/棒果木科.md "wikilink") Corynocarpaceae
        12. [毒鼠子科](../Page/毒鼠子科.md "wikilink") Dichapetalaceae
    12. [大戟目](../Page/大戟目.md "wikilink") Euphorbiales
        1.  [黄杨科](../Page/黄杨科.md "wikilink") Buxaceae
        2.  [油蜡树科](../Page/油蜡树科.md "wikilink") Simmondsiaceae
        3.  [小盘木科](../Page/小盘木科.md "wikilink") Pandaceae
        4.  [大戟科](../Page/大戟科.md "wikilink") Euphorbiaceae
    13. [鼠李目](../Page/鼠李目.md "wikilink") Rhamnales
        1.  [鼠李科](../Page/鼠李科.md "wikilink") Rhamnaceae
        2.  [火筒树科](../Page/火筒树科.md "wikilink") Leeaceae
        3.  [葡萄科](../Page/葡萄科.md "wikilink") Vitaceae
    14. [亚麻目](../Page/亚麻目.md "wikilink") Linales
        1.  [古柯科](../Page/古柯科.md "wikilink") Erythroxylaceae
        2.  [香膏科](../Page/香膏科.md "wikilink") Humiriaceae
        3.  [粘木科](../Page/粘木科.md "wikilink") Ixonanthaceae
        4.  [亚麻藤科](../Page/亚麻藤科.md "wikilink") Hugoniaceae
        5.  [亚麻科](../Page/亚麻科.md "wikilink") Linaceae
    15. [远志目](../Page/远志目.md "wikilink") Polygalales
        1.  [金虎尾科](../Page/金虎尾科.md "wikilink") Malpighiaceae
        2.  [蜡烛树科](../Page/蜡烛树科.md "wikilink") Vochysiaceae
        3.  [三角果科](../Page/三角果科.md "wikilink") Trigoniaceae
        4.  [孔药花科](../Page/孔药花科.md "wikilink") Tremandraceae
        5.  [远志科](../Page/远志科.md "wikilink") Polygalaceae
        6.  [黄叶树科](../Page/黄叶树科.md "wikilink") Xanthophyllaceae
        7.  [刺球果科](../Page/刺球果科.md "wikilink") Krameriaceae
    16. [无患子目](../Page/无患子目.md "wikilink") Sapindales
        1.  [省沽油科](../Page/省沽油科.md "wikilink") Staphyleaceae
        2.  [蜜花科](../Page/蜜花科.md "wikilink") Melianthaceae
        3.  [钟萼木科](../Page/钟萼木科.md "wikilink") Bretschneideraceae
        4.  [叠珠树科](../Page/叠珠树科.md "wikilink") Akaniaceae
        5.  [无患子科](../Page/无患子科.md "wikilink") Sapindaceae
        6.  [七叶树科](../Page/七叶树科.md "wikilink") Hippocastanaceae
        7.  [槭树科](../Page/槭树科.md "wikilink") Aceraceae
        8.  [橄榄科](../Page/橄榄科.md "wikilink") Burseraceae
        9.  [漆树科](../Page/漆树科.md "wikilink") Anacardiaceae
        10. [三柱草科](../Page/三柱草科.md "wikilink") Julianiaceae
        11. [苦木科](../Page/苦木科.md "wikilink") Simaroubaceae
        12. [叶柄花科](../Page/叶柄花科.md "wikilink") Cneoraceae
        13. [楝科](../Page/楝科.md "wikilink") Meliaceae
        14. [芸香科](../Page/芸香科.md "wikilink") Rutaceae
        15. [蒺藜科](../Page/蒺藜科.md "wikilink") Zygophyllaceae
    17. [牻牛儿苗目](../Page/牻牛儿苗目.md "wikilink") Geraniales
        1.  [酢浆草科](../Page/酢浆草科.md "wikilink") Oxalidaceae
        2.  [牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink") Geraniaceae
        3.  [沼花科](../Page/沼花科.md "wikilink") Limnanthaceae
        4.  [旱金莲科](../Page/旱金莲科.md "wikilink") Tropaeolaceae
        5.  [凤仙花科](../Page/凤仙花科.md "wikilink") Balsaminaceae
    18. [伞形目](../Page/伞形目.md "wikilink") Apiales
        1.  [五加科](../Page/五加科.md "wikilink") Araliaceae
        2.  [伞形科](../Page/伞形科.md "wikilink") Apiaceae

<!-- end list -->

1.  [菊亚纲](../Page/菊亚纲.md "wikilink") Asteridae
    1.  [龙胆目](../Page/龙胆目.md "wikilink") Gentianales
        1.  [马钱科](../Page/马钱科.md "wikilink") Loganiaceae
        2.  [轮叶科](../Page/轮叶科.md "wikilink") Retziaceae
        3.  [龙胆科](../Page/龙胆科.md "wikilink") Gentianaceae
        4.  [囊叶木科](../Page/囊叶木科.md "wikilink") Saccifoliaceae
        5.  [夹竹桃科](../Page/夹竹桃科.md "wikilink") Apocynaceae
        6.  [萝摩科](../Page/萝摩科.md "wikilink") Asclepiadaceae
    2.  [茄目](../Page/茄目.md "wikilink") Solanales
        1.  [核果木科](../Page/核果木科.md "wikilink") Duckeodendraceae
        2.  [假茄科](../Page/假茄科.md "wikilink") Nolanaceae
        3.  [茄科](../Page/茄科.md "wikilink") Solanaceae
        4.  [旋花科](../Page/旋花科.md "wikilink") Convolvulaceae
        5.  [菟丝子科](../Page/菟丝子科.md "wikilink") Cuscutaceae
        6.  [睡菜科](../Page/睡菜科.md "wikilink") Menyanthaceae
        7.  [花荵科](../Page/花荵科.md "wikilink") Polemoniaceae
        8.  [田基麻科](../Page/田基麻科.md "wikilink") Hydrophyllaceae
    3.  [唇形目](../Page/唇形目.md "wikilink") Lamiales
        1.  [盖裂寄生科](../Page/盖裂寄生科.md "wikilink") Lennoaceae
        2.  [紫草科](../Page/紫草科.md "wikilink") Boraginaceae
        3.  [马鞭草科](../Page/马鞭草科.md "wikilink") Verbenaceae
        4.  [唇形科](../Page/唇形科.md "wikilink") Lamiaceae
    4.  [水马齿目](../Page/水马齿目.md "wikilink") Callitrichales
        1.  [杉叶藻科](../Page/杉叶藻科.md "wikilink") Hippuridaceae
        2.  [水马齿科](../Page/水马齿科.md "wikilink") Callitrichaceae
        3.  [水穗草科](../Page/水穗草科.md "wikilink") Hydrostachyaceae
    5.  [车前目](../Page/车前目.md "wikilink") Plantaginales
        1.  [车前科](../Page/车前科.md "wikilink") Plantaginaceae
    6.  [玄参目](../Page/玄参目.md "wikilink") Scrophulariales
        1.  [醉鱼草科](../Page/醉鱼草科.md "wikilink") Buddlejaceae
        2.  [木犀科](../Page/木犀科.md "wikilink") Oleaceae
        3.  [玄参科](../Page/玄参科.md "wikilink") Scrophulariaceae
        4.  [肾药花科](../Page/肾药花科.md "wikilink") Globulariaceae
        5.  [苦槛蓝科](../Page/苦槛蓝科.md "wikilink") Myoporaceae
        6.  [列当科](../Page/列当科.md "wikilink") Orobanchaceae
        7.  [苦苣苔科](../Page/苦苣苔科.md "wikilink") Gesneriaceae
        8.  [爵床科](../Page/爵床科.md "wikilink") Acanthaceae
        9.  [胡麻科](../Page/胡麻科.md "wikilink") Pedaliaceae
        10. [紫葳科](../Page/紫葳科.md "wikilink") Bignoniaceae
        11. [对叶藤科](../Page/对叶藤科.md "wikilink") Mendonciaceae
        12. [狸藻科](../Page/狸藻科.md "wikilink") Lentibulariaceae
    7.  [桔梗目](../Page/桔梗目.md "wikilink") Campanulales
        1.  [五膜草科](../Page/五膜草科.md "wikilink") Pentaphragmataceae
        2.  [楔瓣花科](../Page/楔瓣花科.md "wikilink") Sphenocleaceae
        3.  [桔梗科](../Page/桔梗科.md "wikilink") Campanulaceae
        4.  [花柱草科](../Page/花柱草科.md "wikilink") Stylidiaceae
        5.  [陀螺果科](../Page/陀螺果科.md "wikilink") Donatiaceae
        6.  [蓝针花科](../Page/蓝针花科.md "wikilink") Brunoniaceae
        7.  [草海桐科](../Page/草海桐科.md "wikilink") Goodeniaceae
    8.  [茜草目](../Page/茜草目.md "wikilink") Rubiales
        1.  [茜草科](../Page/茜草科.md "wikilink") Rubiaceae
        2.  [假牛繁缕科](../Page/假牛繁缕科.md "wikilink") Theligonaceae
    9.  [川续断目](../Page/川续断目.md "wikilink") Dipsacales
        1.  [忍冬科](../Page/忍冬科.md "wikilink") Caprifoliaceae
        2.  [五福花科](../Page/五福花科.md "wikilink") Adoxaceae
        3.  [败酱科](../Page/败酱科.md "wikilink") Valerianaceae
        4.  [川续断科](../Page/川续断科.md "wikilink") Dipsacaceae
    10. [头花草目](../Page/头花草目.md "wikilink") Calycerales
        1.  [头花草科](../Page/头花草科.md "wikilink") Calyceraceae
    11. [菊目](../Page/菊目.md "wikilink") Asterales
        1.  [菊科](../Page/菊科.md "wikilink") Asteraceae

## [百合纲](../Page/单子叶植物纲.md "wikilink") Liliopsida

1.  [泽泻亚纲](../Page/泽泻亚纲.md "wikilink") Alismatidae
    1.  [泽泻目](../Page/泽泻目.md "wikilink") Alismatales
        1.  [花蔺科](../Page/花蔺科.md "wikilink") Butomaceae
        2.  [黄花绒叶草科](../Page/黄花绒叶草科.md "wikilink") Limnocharitaceae
        3.  [泽泻科](../Page/泽泻科.md "wikilink") Alismataceae
    2.  [水鳖目](../Page/水鳖目.md "wikilink") Hydrocharitales
        1.  [水鳖科](../Page/水鳖科.md "wikilink") Hydrocharitaceae
    3.  [茨藻目](../Page/茨藻目.md "wikilink") Najadales
        1.  [水雍科](../Page/水雍科.md "wikilink") Aponogetonaceae
        2.  [芝菜科](../Page/芝菜科.md "wikilink") Scheuchzeriaceae
        3.  [水麦冬科](../Page/水麦冬科.md "wikilink") Juncaginaceae
        4.  [眼子菜科](../Page/眼子菜科.md "wikilink") Potamogetonaceae
        5.  [川蔓藻科](../Page/川蔓藻科.md "wikilink") Ruppiaceae
        6.  [茨藻科](../Page/茨藻科.md "wikilink") Najadaceae
        7.  [角果藻科](../Page/角果藻科.md "wikilink") Zannichelliaceae
        8.  [波喜荡草科](../Page/波喜荡草科.md "wikilink") Posidoniaceae
        9.  [丝粉藻科](../Page/丝粉藻科.md "wikilink") Cymodoceaceae
        10. [甘藻科](../Page/甘藻科.md "wikilink") Zosteraceae
    4.  [霉草目](../Page/霉草目.md "wikilink") Triuridales
        1.  [无叶莲科](../Page/无叶莲科.md "wikilink") Petrosaviaceae
        2.  [霉草科](../Page/霉草科.md "wikilink") Triuridaceae

<!-- end list -->

1.  [槟榔亚纲](../Page/槟榔亚纲.md "wikilink") Arecidae
    1.  [槟榔目](../Page/槟榔目.md "wikilink") Arecales
        1.  [槟榔科](../Page/槟榔科.md "wikilink") Arecaceae
            ([棕榈科](../Page/棕榈科.md "wikilink"))
    2.  [环花草目](../Page/环花草目.md "wikilink") Cyclanthales
        1.  [环花草科](../Page/环花草科.md "wikilink") Cyclanthaceae
    3.  [露兜树目](../Page/露兜树目.md "wikilink") Pandanales
        1.  [露兜树科](../Page/露兜树科.md "wikilink") Pandanaceae
    4.  [天南星目](../Page/天南星目.md "wikilink") Arales
        1.  [菖蒲科](../Page/菖蒲科.md "wikilink") Acoraceae
        2.  [天南星科](../Page/天南星科.md "wikilink") Araceae
        3.  [浮萍科](../Page/浮萍科.md "wikilink") Lemnaceae

<!-- end list -->

1.  [鸭跖草亚纲](../Page/鸭跖草亚纲.md "wikilink") Commelinidae
    1.  [鸭跖草目](../Page/鸭跖草目.md "wikilink") Commelinales
        1.  [偏穗草科](../Page/偏穗草科.md "wikilink") Rapateaceae
        2.  [黄眼草科](../Page/黄眼草科.md "wikilink") Xyridaceae
        3.  [苔草科](../Page/苔草科.md "wikilink") Mayacaceae
        4.  [鸭跖草科](../Page/鸭跖草科.md "wikilink") Commelinaceae
    2.  [谷精草目](../Page/谷精草目.md "wikilink") Eriocaulales
        1.  [谷精草科](../Page/谷精草科.md "wikilink") Eriocaulaceae
    3.  [帚灯草目](../Page/帚灯草目.md "wikilink") Restionales
        1.  [须叶藤科](../Page/须叶藤科.md "wikilink") Flagellariaceae
        2.  [拟苇科](../Page/拟苇科.md "wikilink") Joinvilleaceae
        3.  [帚灯草科](../Page/帚灯草科.md "wikilink") Restionaceae
        4.  [刺鳞草科](../Page/刺鳞草科.md "wikilink") Centrolepidaceae
    4.  [灯芯草目](../Page/灯芯草目.md "wikilink") Juncales
        1.  [灯芯草科](../Page/灯芯草科.md "wikilink") Juncaceae
        2.  [梭子草科](../Page/梭子草科.md "wikilink") Thurniaceae
    5.  [莎草目](../Page/莎草目.md "wikilink") Cyperales
        1.  [莎草科](../Page/莎草科.md "wikilink") Cyperaceae
        2.  [禾本科](../Page/禾本科.md "wikilink") Poaceae
    6.  [独蕊草目](../Page/独蕊草目.md "wikilink") Hydatellales
        1.  [独蕊草科](../Page/独蕊草科.md "wikilink") Hydatellaceae
    7.  [香蒲目](../Page/香蒲目.md "wikilink") Typhales
        1.  [黑三棱科](../Page/黑三棱科.md "wikilink") Sparganiaceae
        2.  [香蒲科](../Page/香蒲科.md "wikilink") Typhaceae

<!-- end list -->

1.  [薑亞綱](../Page/薑亞綱.md "wikilink") Zingiberidae
    1.  [凤梨目](../Page/凤梨目.md "wikilink") Bromeliales
        1.  [凤梨科](../Page/凤梨科.md "wikilink") Bromeliaceae
    2.  [薑目](../Page/薑目.md "wikilink") Zingiberales
        1.  [鹤望兰科](../Page/鹤望兰科.md "wikilink") Strelitziaceae
        2.  [蝎尾蕉科](../Page/蝎尾蕉科.md "wikilink") Heliconiaceae
        3.  [芭蕉科](../Page/芭蕉科.md "wikilink") Musaceae
        4.  [兰花蕉科](../Page/兰花蕉科.md "wikilink") Lowiaceae
        5.  [薑科](../Page/薑科.md "wikilink") Zingiberaceae
        6.  [闭鞘姜科](../Page/闭鞘姜科.md "wikilink") Costaceae
        7.  [美人蕉科](../Page/美人蕉科.md "wikilink") Cannaceae
        8.  [竹芋科](../Page/竹芋科.md "wikilink") Marantaceae

<!-- end list -->

1.  [百合亚纲](../Page/百合亚纲.md "wikilink") Liliidae
    1.  [百合目](../Page/百合目.md "wikilink") Liliales
        1.  [田葱科](../Page/田葱科.md "wikilink") Philydraceae
        2.  [雨久花科](../Page/雨久花科.md "wikilink") Pontederiaceae
        3.  [血皮草科](../Page/血皮草科.md "wikilink") Haemodoraceae
        4.  [蓝星科](../Page/蓝星科.md "wikilink") Cyanastraceae
        5.  [百合科](../Page/百合科.md "wikilink") Liliaceae
        6.  [鸢尾科](../Page/鸢尾科.md "wikilink") Iridaceae
        7.  [翡若翠科](../Page/翡若翠科.md "wikilink") Velloziaceae
        8.  [芦荟科](../Page/芦荟科.md "wikilink") Aloeaceae
        9.  [龙舌兰科](../Page/龙舌兰科.md "wikilink") Agavaceae
        10. [刺叶树科](../Page/刺叶树科.md "wikilink") Xanthorrhoeaceae
        11. [匍茎草科](../Page/匍茎草科.md "wikilink") Hanguanaceae
        12. [蒟蒻薯科](../Page/蒟蒻薯科.md "wikilink") Taccaceae
        13. [百部科](../Page/百部科.md "wikilink") Stemonaceae
        14. [菝葜科](../Page/菝葜科.md "wikilink") Smilacaceae
        15. [薯蓣科](../Page/薯蓣科.md "wikilink") Dioscoreaceae
    2.  [兰目](../Page/兰目.md "wikilink") Orchidales
        1.  [地蜂草科](../Page/地蜂草科.md "wikilink") Geosiridaceae
        2.  [水玉簪科](../Page/水玉簪科.md "wikilink") Burmanniaceae
        3.  [白玉簪科](../Page/白玉簪科.md "wikilink") Corsiaceae
        4.  [兰科](../Page/兰科.md "wikilink") Orchidaceae

## 注釋

<references />

## 參考文獻

  - Cronquist, A. 1988. The evolution and classification of flowering
    plants. Bronx, NY : New York Botanical Garden. 555 pp. ISBN
    0893273325
  - Zomlefer, W. B. 1995. Guide to Flowering Plant Families. 430 p.p.
    University of North Carolina Press. ISBN 0-8078-2160-5

## 參考條目

  - [被子植物APG III分类法](../Page/被子植物APG_III分类法.md "wikilink")
  - [阿瑟·約翰·克朗奎斯特](../Page/阿瑟·約翰·克朗奎斯特.md "wikilink")
  - [亞美因·塔赫塔江](../Page/亞美因·塔赫塔江.md "wikilink")

## 外部链接

  - [克朗奎斯特分类法](http://herba.msu.ru/shipunov/else/cronqst.txt)

[Category:被子植物](../Category/被子植物.md "wikilink")
[Category:植物分類學](../Category/植物分類學.md "wikilink")

1.  [克朗奎斯特分类法](http://herba.msu.ru/shipunov/else/cronqst.txt)，此網頁之分類系統主要乃參考克朗奎斯特於1988出版之《開花植物的演化及分類》(The
    evolution and classification of flowering plants)。
2.  \[Zomlefer, W. B. 1995.\]，Zomlefer在書中(第2頁)提到，當時流行的高等植物分類系統有克朗奎斯特
    (1981年、1983年、1988年)、[塔赫塔江](../Page/亚美因·塔赫塔江.md "wikilink")
    (1969年、1980年、1983年、1991年)、[Stebbins](http://en.wikipedia.org/wiki/G._Ledyard_Stebbins)
    (1974年)、與[Dahlgren](http://en.wikipedia.org/wiki/Rolf_Dahlgren)相關的[系統等](../Page/系統.md "wikilink")