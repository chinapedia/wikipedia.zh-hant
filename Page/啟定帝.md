**啓定帝**（；）即**阮弘宗**（），名**阮福晙**（），[越南](../Page/越南.md "wikilink")[阮朝第](../Page/阮朝.md "wikilink")12任[皇帝](../Page/皇帝.md "wikilink")，1916年—1925年在位。

## 生平

啓定帝原名**阮福宝嶹**（）。咸宜元年秋九月初一日（1885年10月8日）生于[顺化皇城](../Page/顺化皇城.md "wikilink")，是[同庆帝与和嫔](../Page/同庆帝.md "wikilink")[杨氏熟](../Page/杨氏熟.md "wikilink")（即后来的佑天純皇后）的长子。同庆帝有6个儿子，但只有他活至成年。

同慶三年十二月二十七日（1889年1月28日），同庆帝逝世。当时阮福寶嶹年仅4岁，法国殖民政府认为他太年幼，不适宜担任皇帝。而[育德帝的儿子阮福寶嶙](../Page/育德帝.md "wikilink")，不仅被人认为是神童，而且有亲法倾向，因此被殖民政府拥上皇位，是为[成泰帝](../Page/成泰帝.md "wikilink")。寶嶹则因是前任皇帝的儿子而受到礼待。成泰十八年十月初十日（1906年11月25日），成泰帝册封寶嶹为**奉化郡公**（）。維新三年二月十三日（1909年3月4日），晉爵為**奉化公**（）\[1\]，在顺化附近的[安旧社營建府邸](../Page/安旧社.md "wikilink")，并納辅政大臣[张如冈的女儿](../Page/张如冈.md "wikilink")[张如氏静為](../Page/张如氏静.md "wikilink")[府妾](../Page/府妾.md "wikilink")。

1914年，法国卷入[第一次世界大战](../Page/第一次世界大战.md "wikilink")，无暇东顾。1916年5月3日，[维新帝密谋反抗法国的殖民统治](../Page/维新帝.md "wikilink")，逃出顺化城。5月6日，维新帝被法国殖民者抓获，其拒绝重回顺化城当傀儡皇帝，随后被送往头顿，后与父亲成泰帝一起被流放到了法属[留尼汪岛](../Page/留尼汪岛.md "wikilink")。5月18日（四月十七日），寶嶹被法国殖民者立为越南皇帝，改年號為[启定](../Page/启定.md "wikilink")。

啓定帝即位後，他誓言要重建皇室的威望；然而就他與法國殖民政府之間的深厚關係來看，顯然是不可能的。雖然啟定帝並不滿意其處境，但是他仍舊頒布了許多有利於法國人的法令。因此，啓定帝普遍不受越南人的歡迎。

1918年，第一次世界大戰結束。在1919年召開的[巴黎和會上](../Page/巴黎和會_\(1919年\).md "wikilink")，美國總統[威爾遜提出](../Page/威爾遜.md "wikilink")[十四點和平原則](../Page/十四點和平原則.md "wikilink")。受此影響，化名為「阮愛國」的阮必成（即后来的[胡志明](../Page/胡志明.md "wikilink")），與[潘周桢](../Page/潘周桢.md "wikilink")、[潘文祥等人一起](../Page/潘文祥.md "wikilink")，以越南愛國人士身份，向和會提交出各民族权利的八项要求，要求法国政府承认越南人的自由、民主、平等和自决权。但當時只侧重在处理欧洲的民族自决，并不理睬殖民地人民的独立要求，因此這個提議被忽略了。

同年，啓定帝下令，来年（1919年）举行最后一场[会试](../Page/会试.md "wikilink")，此后废除科举制度。

1922年6月，啓定帝前往法國[馬賽](../Page/馬賽.md "wikilink")，參加在該城市舉辦的。這是越南歷史上皇帝第一次出國訪問，但這次法國之旅卻飽受非議。啓定帝在馬賽港登陸之後，便遭到一群留法越南人的抗議。[潘周桢向啓定帝邮寄](../Page/潘周桢.md "wikilink")《》（又名《七條陳》），批評當一般的越南人民遭受到法國的剝削時，他卻過著奢華的生活；阮愛國寫了一齣名為《[竹龍](../Page/竹龍.md "wikilink")》（）的戲劇，嘲諷啓定帝表面上看起來很威武，但充其量只是法國人的掌中傀儡。

1923年，因財政拮据，啓定帝無力繼續營建自己豪華的陵寢，便下令將農民的稅賦增加30%，更讓反對他的聲浪達到最高峰。為了平息這些抗議，他又簽署了許多律令，造成更多的異議人士被逮捕入獄。

啓定帝自幼體弱多病，且患有[藥癮](../Page/藥癮.md "wikilink")，最終在1925年11月6日（九月二十日）因[結核病去世](../Page/結核病.md "wikilink")，年僅四十歲。[庙号](../Page/庙号.md "wikilink")**弘宗**（），[谥号](../Page/谥号.md "wikilink")**嗣天嘉運聖明神智仁孝誠敬貽謀承烈宣皇帝**（）\[2\]。葬於[应陵](../Page/应陵.md "wikilink")。

## 家族

<center>

</center>

### 后宮

根據其中一位妾室的說法，「他對[性並無多大興趣](../Page/性愛.md "wikilink")」。

<table>
<thead>
<tr class="header">
<th><p>封號/諡號</p></th>
<th><p>姓名</p></th>
<th><p>生卒年</p></th>
<th><p>生平</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>皇貴妃</p></td>
<td><p><a href="../Page/張如氏靜.md" title="wikilink">張如氏靜</a></p></td>
<td><p>1889年4月7日—1968年6月20日</p></td>
<td><p>輔政大臣<a href="../Page/張如岡.md" title="wikilink">張如岡之女</a></p></td>
</tr>
<tr class="even">
<td><p>一階恩妃</p></td>
<td><p><a href="../Page/胡氏芷.md" title="wikilink">胡氏芷</a></p></td>
<td><p>1902年—1985年</p></td>
<td><p>輔政大臣<a href="../Page/胡得忠.md" title="wikilink">胡得忠之女</a></p></td>
</tr>
<tr class="odd">
<td><p>一階厚妃<br />
端徽皇太后</p></td>
<td><p><a href="../Page/黃氏菊.md" title="wikilink">黃氏菊</a></p></td>
<td><p>1890年1月27日—1980年11月9日</p></td>
<td><p>恩封宜國公<a href="../Page/黃文錫.md" title="wikilink">黃文錫之女</a>，生<a href="../Page/保大帝.md" title="wikilink">保大帝阮福晪</a>。</p></td>
</tr>
<tr class="even">
<td><p>三階妙嬪</p></td>
<td><p><a href="../Page/范氏懷.md" title="wikilink">范氏懷</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>四阶裕嫔</p></td>
<td><p><a href="../Page/武氏蓉.md" title="wikilink">武氏蓉</a></p></td>
<td></td>
<td><p>春和男<a href="../Page/武濂.md" title="wikilink">武濂之女</a>，其母<a href="../Page/阮氏靖.md" title="wikilink">阮氏靖是镇定郡公</a><a href="../Page/阮福綿𡩈.md" title="wikilink">阮福綿𡩈的孙女</a>。</p></td>
</tr>
<tr class="even">
<td><p>五階恬嬪</p></td>
<td><p><a href="../Page/阮廷氏蓮.md" title="wikilink">阮廷氏蓮</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>七階貴人</p></td>
<td><p><a href="../Page/陳登氏松.md" title="wikilink">陳登氏松</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>九階才人</p></td>
<td><p><a href="../Page/吳氏莊.md" title="wikilink">吳氏莊</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 子女

啓定帝只有一子阮福永瑞，即阮朝的末代皇帝[保大帝](../Page/保大帝.md "wikilink")。

| 排行     | 封號  | 諡號 | 姓名                               | 生卒年                    | 生母  | 生平 |
| ------ | --- | -- | -------------------------------- | ---------------------- | --- | -- |
| **皇子** |     |    |                                  |                        |     |    |
| 長子     | 保大帝 |    | [阮福晪](../Page/阮福晪.md "wikilink") | 1913年10月22日－1997年7月30日 | 黃氏菊 |    |

## 圖片

<File:越南阮朝弘宗宣皇帝阮福晙（启定1916年—1925年）31岁登基时的冕服照片（1916年>）.jpg|啟定帝登基時的冕服照
[File:Roi-Khai-Dinh.jpg|啟定帝戎裝照](File:Roi-Khai-Dinh.jpg%7C啟定帝戎裝照)
[File:7tien.jpg|啟定帝在位期間發行刻有](File:7tien.jpg%7C啟定帝在位期間發行刻有)[飛龍圖案的貨幣](../Page/飛龍.md "wikilink")

## 注釋

{{-}}  |-style="text-align: center; background: \#FFE4E1;"
|align="center" colspan="3"|**啓定帝**

[Category:阮朝君主](../Category/阮朝君主.md "wikilink")
[Category:阮景宗皇子](../Category/阮景宗皇子.md "wikilink")
[Category:阮朝郡公](../Category/阮朝郡公.md "wikilink")
[Category:阮朝親公](../Category/阮朝親公.md "wikilink")
[Category:男同性恋皇族](../Category/男同性恋皇族.md "wikilink")

1.  據順化香水市社啟定帝應陵《應陵聖德神功碑》。
2.  據啟定帝應陵前保大帝所作應陵聖德神功碑[碑文](../Page/:s:應陵聖德神功碑.md "wikilink")，啟定帝諡號第14字應為“謀”字（越南文：Mưu），但《[大南寔錄正編第七紀](../Page/大南寔錄.md "wikilink")》作“謨”字（越南文：Mô）。