**纹银**，又稱**松紋**\[1\]，中國[明朝](../Page/明朝.md "wikilink")[货币单位名称](../Page/货币.md "wikilink")，明代所謂的紋銀成色在98%以上\[2\]。

明代后期，由于成色优良的银子在铸造中会出现细密的纹路，人们常把足色的银锭称为“纹银”。纹银的地位深入人心，后来成为市场上公认的计量单位。但造假者使用摇丝、画丝、吹丝等手段，也能在成色不足的银锭上制造出纹银的特征。所以纹银不再是成色的保证，但作为计量单位保留了下来，即[虚银两](../Page/虚银两.md "wikilink")，包括[关平](../Page/关平银.md "wikilink")、[库平](../Page/库平银.md "wikilink")、市平、漕平等种类。

**纹银**也是一种合金，通常含有92.5%的银和7.5%的其它金属，如[铜](../Page/铜.md "wikilink")。加入这些金属可以增加银的硬度和强度。\[3\]\[4\]

## 起源

采用纹银作为货币单位的原因在于，中国古代实际流通的银两[成色不一](../Page/成色.md "wikilink")，名目众多，有“元丝”、“青丝”、“白丝”、“水丝”、“西鏪”、“石鏪”、“柳鏪”、“茶花”、“茴香”、“单倾”、“双倾”等多种名色，这些种类不同的金属银需要折算为全国统一的计量单位，以便于统计税收。

## 換算

按照清朝的官定标准，纹银的成色为93.5374%，称“十足成纹”\[5\]。
清制规定纹银一两等于[制钱](../Page/制钱.md "wikilink")1000文\[6\]，，[嘉慶年間對](../Page/嘉慶.md "wikilink")[英](../Page/英國.md "wikilink")[法的](../Page/法國.md "wikilink")[鴉片貿易查禁較嚴](../Page/鴉片貿易.md "wikilink")，中國工藝品及農產加工製品大量輸出，保持很長一段出超的貿易環境，使得白銀大量淨流入而出現銀賤錢貴的情形，18世紀末曾達到一兩銀換七、八百[銅錢的情況](../Page/銅錢.md "wikilink")，此後的四十年因大量鴉片叩關，道光年間則需要一千六百文才能換一兩白銀的錢賤银贵的现象。\[7\]

### 其他流通银

在实际流通领域中的金属银，成锭者称“宝银”，即铸成[元宝形式的银锭](../Page/元宝.md "wikilink")。宝银的成色通常全都高于纹银。由于各地宝银成色不一，因此在其前面冠以地名，或[升水标准](../Page/升水.md "wikilink")，如苏宝银、武昌宝银，足宝、二四宝、二五宝、二六宝等等。

“二四宝”即每五十两升水（即溢价）二两四钱，一锭五十两的“二四宝”银锭折算为五十二两四钱标准纹银。

## 沒落

[鸦片战争之后](../Page/鸦片战争.md "wikilink")，[银两](../Page/银两.md "wikilink")、[银元在中国同时流通](../Page/银元.md "wikilink")，但官方统计[赋税](../Page/赋税.md "wikilink")、制订[预算](../Page/预算.md "wikilink")、对外[赔款时仍使用](../Page/赔款.md "wikilink")[库平两和](../Page/库平两.md "wikilink")[关平两纹银作为计量单位](../Page/关平两.md "wikilink")。1933年[国民政府宣布](../Page/国民政府.md "wikilink")[废两改元](../Page/废两改元.md "wikilink")，纹银正式退出流通领域。

## 注譯

<references/>

[Category:中国古代货币](../Category/中国古代货币.md "wikilink")
[Category:虚拟货币](../Category/虚拟货币.md "wikilink")
[Category:银合金](../Category/银合金.md "wikilink")

1.  《金瓶梅》第五十七回：「取出一封銀子，准准三十兩足色松紋，便交付薛姑子與那王姑子。」
2.  《初刻拍案驚奇》卷十五：「他卻把九六七銀子，充作紋銀。」
3.  ["The Care of Silver"](http://www.hermansilver.com/care.htm); Web
    article by Jeffrey Herman, silversmith, specialist in silver
    restoration and conservation. Retrieved 28 Nov 2017.
4.
5.  张惠信，《中国银锭》，第187 页。
6.  楊端六，《清代貨幣金融史稿》，頁13。
7.  [白銀外流：鴉片對中國經濟的打擊](http://culture.edu.tw/history/smenu_photomenu.php?smenuid=1670)