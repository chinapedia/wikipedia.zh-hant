**男科學**（，來自、*anēr*，[屬格](../Page/屬格.md "wikilink")、*andros*「男人」和：「學」）是處理男性健康的[醫學專業人士](../Page/醫學.md "wikilink")，特別是與[男性特有的](../Page/男性.md "wikilink")[男性生殖系統和](../Page/男性生殖系統.md "wikilink")[泌尿外科等相關的問題](../Page/泌尿外科.md "wikilink")。與針對女性生殖系統相關的醫療問題的[婦產科互相對應](../Page/婦產科.md "wikilink")。自1960年代後期以來，男科學只做為一個獨特的專業來研究：第一個有關該課題的專業期刊是德國期刊「Andrologie（現稱「Andrologia」）」，自1969年起出版\[1\]。

男性特有的醫學和[手術程序包括](../Page/手術.md "wikilink")[輸精管結紮術和](../Page/輸精管結紮術.md "wikilink")（輸精管結紮的逆轉程序之一）以及應付男性泌尿生殖系統紊亂的介入程序，例如以下列表：
[Male_anatomy_zh.svg](https://zh.wikipedia.org/wiki/File:Male_anatomy_zh.svg "fig:Male_anatomy_zh.svg")

## 参考文献

## 外部連結

  - [美國男科學協會](http://www.andrologysociety.com/)
  - [國際男科學學會](http://andrology.org/)

## 參見

  - [男性生殖系统](../Page/男性生殖系统.md "wikilink")、[泌尿外科](../Page/泌尿外科.md "wikilink")
  - [妇科学](../Page/妇科学.md "wikilink")、[产科学](../Page/产科学.md "wikilink")

{{-}}

[Category:医学专业](../Category/医学专业.md "wikilink")
[男科學](../Category/男科學.md "wikilink")

1.  *Social Studies of Science* (1990) **20**, p. 32