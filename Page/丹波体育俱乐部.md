**丹波体育俱乐部**（**Dempo Sports
Club**）是[印度的体育俱乐部](../Page/印度.md "wikilink")，位于[果阿邦首府](../Page/果阿邦.md "wikilink")[帕纳吉](../Page/帕纳吉.md "wikilink")。其中的足球队是印度最出色的足球队之一。俱乐部的拥有者和赞助商皆为[丹波集团公司](../Page/:en:Dempo.md "wikilink")。2004-05赛季球队夺得了历史上首座[印度全国足球联赛超级组冠军头衔](../Page/印度全国足球联赛.md "wikilink")。俱乐部主教练Armando
Colaco同时也是全印度足球联盟的秘书长。

球队中的核心球员包括了印度国家队成员Climax Lawrence, Ishfaq Ahmed,
[尼日利亚的Ranty](../Page/尼日利亚.md "wikilink") Martins Soleye
和Clifford Miranda。

## 球員名單

## 球队荣誉

  - [印度全国足球联赛超级组冠军](../Page/印度全国足球联赛超级组冠军.md "wikilink") 1次
      - [2004-05印度足球聯賽](../Page/2004-05印度足球聯賽.md "wikilink")
  - [联邦杯冠军](../Page/印度聯合會盃.md "wikilink") 1次
      - 2004年
  - [流浪者杯冠军](../Page/流浪者杯.md "wikilink") 4次
  - [印度全国足球联赛第二级别冠军](../Page/印度全国足球联赛.md "wikilink")
      - 2001-03年
  - [果阿联赛冠军](../Page/果阿联赛.md "wikilink") 11次

### 亞洲足聯盃

  - [2008年](../Page/2008年亞洲足協盃.md "wikilink") - 四強
  - [2009年](../Page/2009年亞洲足協盃.md "wikilink") - 八強

## 外部链接

  - [Dempo Sports Indian Football
    Club](http://www.iloveindia.com/sports/football/clubs/dempo.html)
  - [Dempo Sports Club: Looking
    ahead](http://www.goa-fa.com/index.php?q=node/10)

[Category:印度足球俱乐部](../Category/印度足球俱乐部.md "wikilink")