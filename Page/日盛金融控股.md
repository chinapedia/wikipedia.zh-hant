**日盛金融控股公司**（英語：Jih Sun Financial Holding Co.,
Ltd.，簡稱：**日盛金控**、**日盛金**）是以[日盛證券為主體的一家](../Page/日盛證券.md "wikilink")[台灣](../Page/台灣.md "wikilink")[金融控股公司](../Page/金融控股公司.md "wikilink")，成立於2002年2月5日，並於同日股票上櫃。

該金控的原本主要獲利來源為日盛證券，因此目前該金控的運作模式是以結合日盛證券的通路，成立跨公司之功能性組織，將原有證券及銀行之相關服務，透過跨業商品組合之搭配共同行銷。

目前主要股東為持股36%的[日本](../Page/日本.md "wikilink")[新生銀行](../Page/新生銀行.md "wikilink")（透過旗下[私募基金SIPF](../Page/私募基金.md "wikilink")
B.
V.投資）和持股24%的[美國](../Page/美國.md "wikilink")[建高控股](../Page/建高控股.md "wikilink")。原經營者[陳國和家族已於](../Page/陳國和.md "wikilink")2009年退出經營。

## 沿革

[Jih_Sun_Financial_Holding_1st_logo_20130903.jpg](https://zh.wikipedia.org/wiki/File:Jih_Sun_Financial_Holding_1st_logo_20130903.jpg "fig:Jih_Sun_Financial_Holding_1st_logo_20130903.jpg")
2002年2月5日，[日盛證券與](../Page/日盛證券.md "wikilink")[日盛國際商業銀行以](../Page/日盛國際商業銀行.md "wikilink")1:2.5之換股比率共同以股份轉換方式成立**日盛金融控股公司**\[1\]，以當時日盛證券的紅底白字「盛」字商標為第一代[企業識別標誌](../Page/企業識別.md "wikilink")。

2006年，由於日盛銀行的[不良債權已累積高達新台幣](../Page/不良債權.md "wikilink")98億元，日盛金控決定引入[日本](../Page/日本.md "wikilink")[新生銀行投資](../Page/新生銀行.md "wikilink")，金額為新台幣113.4億元；新生銀行因此取得日盛金控31.8%股權及3席董事席位、日盛金控[風控長與日盛銀行信用審查主管的提名權](../Page/風控長.md "wikilink")。\[2\]

2007年，日盛金控原經營者陳國和退出經營，改由[建高控股接手主導](../Page/建高控股.md "wikilink")。\[3\]

[JihSun_Financial_Building_20131001.jpg](https://zh.wikipedia.org/wiki/File:JihSun_Financial_Building_20131001.jpg "fig:JihSun_Financial_Building_20131001.jpg")

2010年12月14日，日盛金控總部自台北市中山區[松江路](../Page/松江路.md "wikilink")68號「日盛金融大樓」搬遷至台北市中山區南京東路二段85號「自強大樓」（原[中國農民銀行總行大樓](../Page/中國農民銀行.md "wikilink")）10樓，同時啟用紅橘兩色的第二代企業識別標誌。第二代企業識別標誌以橘色及紅色為主色系，以「JS」為主要結構，形成「[$](../Page/$.md "wikilink")」的抽象視覺。\[4\]

## 金控成員及關係

{{ familytree/start }} {{ familytree | level0 |v| level1 |-| level2 |
level0 = **日盛金融控股** | level1 =
[日盛國際商業銀行](../Page/日盛國際商業銀行.md "wikilink")
| level2 = [日盛人身保險代理人](../Page/日盛人身保險代理人.md "wikilink") }} {{ familytree
| | | |\!| }} {{ familytree | | | |)| level1 |v| level2 |v| level3 |v|
level4 | level1 = [日盛證券](../Page/日盛證券.md "wikilink") | level2 =
[日盛國際投資控股](../Page/日盛國際投資控股.md "wikilink") | level3 =
日盛嘉富證券國際 | level4 = 日盛嘉富資本 }} {{ familytree | | | |\!| | | |\!| |
| |\!| | | |\!| }} {{ familytree | | | |\`| level1 |)| level2 |)| level3
|\`| level4 | level1 = [日盛國際產物保險代理人](../Page/日盛國際產物保險代理人.md "wikilink")
| level2 = [日盛證券投資顧問](../Page/日盛證券投資顧問.md "wikilink") | level3 =
日盛財務服務（開曼） | level4 = 日盛投資諮詢（上海） }} {{ familytree | | |
| | | | |\!| | | |\!| }} {{ familytree | | | | | | | |)| level2 |\`|
level3 | level2 = [日盛期貨](../Page/日盛期貨.md "wikilink") | level3 = 日盛科技管理顧問
}} {{ familytree | | | | | | | |\!| }} {{ familytree | | | | | | | |\`|
level2 | level2 = [日盛創業投資](../Page/日盛創業投資.md "wikilink") }} {{
familytree/end }}

## 參考資料

## 外部連結

  - [日盛金控](http://www.jsun.com.tw/)

[Category:2002年成立的公司](../Category/2002年成立的公司.md "wikilink")
[R](../Category/臺灣的金融控股公司.md "wikilink")
[5](../Category/臺灣證券交易所上市公司.md "wikilink")
[Category:總部位於臺北市中山區的工商業機構](../Category/總部位於臺北市中山區的工商業機構.md "wikilink")
[日盛金控](../Category/日盛金控.md "wikilink")

1.
2.
3.
4.