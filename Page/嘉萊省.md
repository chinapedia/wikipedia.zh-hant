**嘉萊省**（\[1\]）是[越南的一個省](../Page/越南.md "wikilink")。

## 地理

嘉萊省位於[西原高原北部](../Page/西原.md "wikilink")，北與[崑嵩省相鄰](../Page/崑嵩省.md "wikilink")，南接[多樂省](../Page/多樂省.md "wikilink")，西臨[柬埔寨](../Page/柬埔寨.md "wikilink")，東鄰[廣義省](../Page/廣義省.md "wikilink")、[富安省](../Page/富安省.md "wikilink")。

## 历史

嘉莱省地处西原北部，是越南少数民族[嘉莱族等民族的传统领地](../Page/嘉莱族.md "wikilink")。东部地区在阮朝时成为越南的羁縻领土。

20世纪初，法国殖民者开拓西原，将嘉莱省一带正式纳入越南版图。1904年7月25日，法属印度支那政府议定设立[坡离俱低省](../Page/坡离俱低省.md "wikilink")（Plei
Ku Der），分为崑嵩代理座和岧嶚（Cheo
Reo）代理座，管辖今[崑嵩省和嘉莱省地区](../Page/崑嵩省.md "wikilink")。

1907年4月25日，殖民政府撤销坡离俱低省，将管辖嘉莱省地区的岧嶚代理座划入富安省。

1913年2月9日，殖民政府在西原地区设立崑嵩省，管辖崑嵩代理座和岧嶚代理座。

1917年，成立安溪代理座，并设新安县。

1932年5月25日，殖民政府议定设立嘉莱省（法文称[坡离俱省](../Page/坡离俱省.md "wikilink")），坡离俱代理座和岧嶚代理座划归嘉莱省。

1945年八月革命爆发时，嘉莱省共辖坡离俱市社（波来古市社）、安溪县、坡离克里县、诸丝县、岧嶚县等。

1946年5月27日，殖民政府在西原高原成立自治政权[南印度支那上游地区](../Page/南印度支那上游地区.md "wikilink")，下辖西原5省。次月，殖民政府将省名定为[波来古省](../Page/波来古省.md "wikilink")（坡离俱省），而[北越则继续称为嘉莱省](../Page/北越.md "wikilink")。

1949年5月30日，南印度支那上游地区政权解散，西原5省并入[越南国](../Page/越南国.md "wikilink")。

1950年4月15日，越南国国长[保大在西原高原成立](../Page/保大帝.md "wikilink")[皇朝疆土](../Page/皇朝疆土.md "wikilink")，波来古省被纳入其中。

1955年3月11日，越南国首相吴廷琰废除“皇朝疆土”，将波来古省直接纳入中央政府管辖。

1958年，[越南共和国](../Page/越南共和国.md "wikilink")[吴廷琰政府将波来古省划分为丽忠郡](../Page/吴廷琰.md "wikilink")、丽清郡和岧嶚郡3郡。

1962年，南越政府将岧嶚郡划出，成立[富本省](../Page/富本省.md "wikilink")。波来古省重新划分为丽忠郡、清安郡和富仁郡3郡。

1975年3月，[越南南方共和国解放嘉莱省](../Page/越南南方共和国.md "wikilink")。10月，嘉莱省和崑嵩省合并为[嘉莱-崑嵩省](../Page/嘉莱-崑嵩省.md "wikilink")。富本省最初并入[多乐省](../Page/多乐省.md "wikilink")，后又划归嘉莱-崑嵩省。嘉莱省区域包括[波来古市社和](../Page/波来古市社.md "wikilink")[安溪县](../Page/安溪县.md "wikilink")、[阿云巴县](../Page/阿云巴县.md "wikilink")、[诸巴县](../Page/诸巴县.md "wikilink")、[诸博容县和](../Page/诸博容县.md "wikilink")[芒杨县](../Page/芒杨县.md "wikilink")5县。

1979年4月23日，阿云巴县析置[克容巴县](../Page/克容巴县.md "wikilink")\[2\]。

1981年8月17日，芒杨县和诸博容县析置[诸色县](../Page/诸色县.md "wikilink")\[3\]。

1984年12月28日，安溪县析置[克邦县](../Page/克邦县.md "wikilink")\[4\]。

1988年5月30日，安溪县析置[公则若县](../Page/公则若县.md "wikilink")\[5\]。

1991年8月12日，嘉莱-崑嵩省重新分设为嘉莱省和崑嵩省\[6\]。嘉莱省下辖波来古市社和芒杨县、安溪县、克邦县、公则若县、诸色县、诸巴县、诸博容县、阿云巴县、克容巴县9县。

1991年10月15日，诸巴县和诸博容县析置[德基县](../Page/德基县.md "wikilink")\[7\]。

1996年11月11日，诸巴县析置[亚格来县](../Page/亚格来县.md "wikilink")\[8\]。

1999年4月24日，波来古市社改制为波来古市\[9\]。

2000年8月21日，芒杨县析置得都阿县\[10\]。

2002年12月18日，阿云巴县析置亚巴县\[11\]。

2003年12月9日，安溪县分设为[安溪市社和](../Page/安溪市社.md "wikilink")[得婆县](../Page/得婆县.md "wikilink")\[12\]。

2007年3月30日，阿云巴县分设为[阿云巴市社和](../Page/阿云巴市社.md "wikilink")[富善县](../Page/富善县.md "wikilink")\[13\]。

2009年8月27日，诸色县析置[诸蒲县](../Page/诸蒲县.md "wikilink")\[14\]。至此，嘉莱省下辖1市2市社14县。

## 行政區劃

嘉萊省下轄1市2市社14縣：

  - [波來古市](../Page/波來古市.md "wikilink")（Thành phố Pleiku）
  - [安溪市社](../Page/安溪市社.md "wikilink")（Thị xã An Khê）
  - [阿雲巴市社](../Page/阿雲巴市社.md "wikilink")（Thị xã Ayun Pa）
  - [諸巴縣](../Page/諸巴縣.md "wikilink")（Huyện Chư Păh）
  - [諸博容縣](../Page/諸博容縣.md "wikilink")（Huyện Chư Prông）
  - [諸蒲縣](../Page/諸蒲縣.md "wikilink")（Huyện Chư Pưh）
  - [諸色縣](../Page/諸色縣.md "wikilink")（Huyện Chư Sê）
  - [得都阿縣](../Page/得都阿縣.md "wikilink")（Huyện Đắk Đoa）
  - [得婆縣](../Page/得婆縣.md "wikilink")（Huyện Đắk Pơ）
  - [德基縣](../Page/德基縣.md "wikilink")（Huyện Đức Cơ）
  - [亞格來縣](../Page/亞格來縣.md "wikilink")（Huyện Ia Grai）
  - [亞巴縣](../Page/亞巴縣.md "wikilink")（Huyện Ia Pa）
  - [克邦縣](../Page/克邦縣.md "wikilink")（Huyện K'Bang）
  - [公則若縣](../Page/公則若縣.md "wikilink")（Huyện Kông Chro）
  - [克容巴縣](../Page/克容巴縣.md "wikilink")（Huyện Krông Pa）
  - [芒楊縣](../Page/芒楊縣.md "wikilink")（Huyện Mang Yang）
  - [富善縣](../Page/富善縣.md "wikilink")（Huyện Phú Thiện）

## 注釋

## 外部連結

  - [嘉萊省人民委员会](https://web.archive.org/web/20090918090237/http://www.ubgialai.gov.vn/)

[Category:越南省份](../Category/越南省份.md "wikilink")
[Category:嘉萊省](../Category/嘉萊省.md "wikilink")

1.  [范瓊在連載於](../Page/范瓊.md "wikilink")《[南風](../Page/南風_\(越南雜誌\).md "wikilink")》雜誌第186-189期的《御駕南巡[慶和](../Page/慶和省.md "wikilink")、[寧順](../Page/寧順省.md "wikilink")、[平順](../Page/平順省.md "wikilink")、嘉萊、[崑嵩](../Page/崑嵩省.md "wikilink")、[班迷屬](../Page/邦美蜀市.md "wikilink")、[同狔上行程記](../Page/同狔上省.md "wikilink")》一文中將該省譯作**嘉萊**。
2.  [178-CP：阿云巴县析置克容巴县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-178-CP-chia-huyen-A-Yun-Pa-tinh-Gia-Lai-Kon-Tum-thanh-huyen-A-Yun-Pa-Krong-Pa-57436.aspx)
3.  [34-HĐBT：芒杨县和诸博容县析置诸色县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-34-HDBT-thanh-lap-huyen-Chu-Se-tinh-Gia-Lai-Kon-Tum-43115.aspx)
4.  [181-HĐBT：安溪县析置克邦县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-181-HDBT-dieu-chinh-dia-gioi-huyen-Kon-Plong-chia-huyen-An-Khe-tinh-Gia-Lai-Kon-Tum-43536.aspx)
5.  [96-HĐBT：安溪县析置公则若县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-96-HDBT-phan-vach-lai-dia-gioi-hanh-chinh-huyen-An-Khe-mot-so-xa-thi-tran-thuoc-tinh-Gia-Lai-Kon-Tum/37567/noi-dung.aspx)
6.  [国会决议：嘉莱-崑嵩省恢复设立嘉莱省和崑嵩省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-dieu-chinh-dia-gioi-hanh-chinh-mot-so-tinh-thanh-pho-truc-thuoc-Trung-uong-42808.aspx)
7.  [315-HĐBT：嘉莱省成立德基县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-315-HDBT-thanh-lap-huyen-moi-Duc-Co-tinh-Gia-Lai/38196/noi-dung.aspx)
8.  [70-CP：诸巴县析置亚格来县](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-70-CP-dieu-chinh-dia-gioi-hanh-chinh-thanh-lap-mot-so-xa-thi-tran-va-huyen-Ia-Grai-thuoc-tinh-Gia-Lai-40216.aspx)
9.  [29/1999/NĐ-CP：波来古市社改制为波来古市](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-29-1999-ND-CP-thanh-lap-thanh-pho-Pleiku-thuoc-tinh-Gia-Lai-45244.aspx)
10. [37/2000/NĐ-CP：芒杨县析置得都阿县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-dinh-37-2000-ND-CP-doi-ten-xa-thi-tran-lap-xa-thuoc-huyen-Mang-Yang-tinh-Gia-Lai-va-chia-huyen-Mang-Yang-thanh-hai-huyen-Dak-Doa-va-Mang-yang-46708.aspx)
11. [104/2002/NĐ-CP：阿云巴县析置亚巴县](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-104-2002-ND-CP-chia-tach-huyen-Ayun-Pa-thanh-hai-huyen-Ia-Pa-Ayun-Pa-tinh-Gia-Lai-6634.aspx)
12. [155/2003/NĐ-CP：安溪县分设为安溪市社和得婆县](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-155-2003-ND-CP-thanh-lap-thi-xa-An-Khe-huyen-Dak-Po-thanh-lap-xa-Dak-Po-thuoc-huyen-Dak-Po-tinh-Gia-Lai-6265.aspx)
13. [50/2007/NĐ-CP：阿云巴县分设为阿云巴市社和富善县](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-50-2007-ND-CP-dieu-chinh-dia-gioi-hanh-chinh-huyen-Ayun-Pa-lap-thi-xa-Ayun-Pa-huyen-Phu-Thien-phuong-Cheo-Reo-Hoa-Binh-Doan-Ket-Song-Bo-17980.aspx)
14. [43/NQ-CP：诸色县析置诸蒲县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-43-NQ-CP-dieu-chinh-dia-gioi-hanh-chinh-xa-de-lap-xa-thuoc-huyen-Chu-Se-huyen-Chu-Se-de-thanh-lap-huyen-Chu-Puh-Gia-Lai-93958.aspx)