**格雷迪縣**（**Grady County,
Georgia**）是[美國](../Page/美國.md "wikilink")[喬治亞州西南部的一個縣](../Page/喬治亞州.md "wikilink")，南鄰[佛羅里達州](../Page/佛羅里達州.md "wikilink")。面積1,192平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口24,466人。縣治[開羅](../Page/開羅_\(喬治亞州\).md "wikilink")（Cairo）。

成立於1905年8月17日，1906年1月1日生效。縣名紀念記者、演說家[亨利·W·格雷迪](../Page/亨利·W·格雷迪.md "wikilink")（Henry
Woodfin Grady）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/佐治亚州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.