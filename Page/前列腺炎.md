**前列腺炎**（）指发生于[前列腺](../Page/前列腺.md "wikilink")[组织的](../Page/组织_\(生物学\).md "wikilink")[炎症](../Page/炎症.md "wikilink")。由于前列腺在一定水平[血睾丸酮作用下形成](../Page/血睾丸酮.md "wikilink")，女性仅找到[组织胚胎学意义上的前列腺痕迹](../Page/组织胚胎学.md "wikilink")；没有[人体解剖学意义上的前列腺](../Page/人体解剖学.md "wikilink")，前列腺炎属于男性[疾病](../Page/疾病.md "wikilink")。前列腺炎在医学上有以下分类：

  - [急性细菌性前列腺炎](../Page/急性细菌性前列腺炎.md "wikilink")
  - [慢性细菌性前列腺炎](../Page/慢性细菌性前列腺炎.md "wikilink")
  - [慢性非细菌性前列腺炎](../Page/慢性非细菌性前列腺炎.md "wikilink")
  - [前列腺增生](../Page/前列腺增生.md "wikilink")

前列腺位于男子盆腔的底部——[膀胱下](../Page/膀胱.md "wikilink")，[尿道上](../Page/尿道.md "wikilink")；[耻骨后](../Page/耻骨.md "wikilink")，[直肠前](../Page/直肠.md "wikilink")。男子[发育后](../Page/发育.md "wikilink")，在血[睾丸酮作用下形成底部横径](../Page/睾丸酮.md "wikilink")4厘米，前后径2厘米；纵径3厘米，外形如同倒放的栗子样圆锥体。它位于[膀胱颈的下方](../Page/膀胱颈.md "wikilink")，包绕着膀胱口与尿道结合部位，即：前列腺中间形成的管道构成尿道的上口部分。

在世界范围内，[慢性前列腺炎](../Page/慢性前列腺炎.md "wikilink")（除急性类型）患者在自然成年男性人群中约占1.7～25.4％，占年轻人最高五项生殖尿道诊断之首；其中，慢性细菌性前列腺炎仅占慢性前列腺炎患者5～10％，自[氟喹诺酮类药物应用于临床以来](../Page/氟喹诺酮类药物.md "wikilink")，慢性细菌性前列腺炎患者占门诊慢性前列腺炎患者比例降至约1.5％。[慢性非细菌性前列腺炎和](../Page/慢性非细菌性前列腺炎.md "wikilink")[前列腺痛占慢性前列腺炎患者的比例高达](../Page/前列腺痛.md "wikilink")95％以上。

## 參見

  - [間質性膀胱炎](../Page/間質性膀胱炎.md "wikilink")—相關的疾病。
  - [肉芽腫性前列腺炎](../Page/肉芽腫性前列腺炎.md "wikilink")（Granulomatous
    prostatitis）

## 外部連結

  -
  - [Prostatitis Self Assessment
    Calculator](http://www.ucpps.org/?tab=5)

[Category:泌尿科疾病](../Category/泌尿科疾病.md "wikilink")
[炎](../Category/前列腺.md "wikilink")
[Category:炎症](../Category/炎症.md "wikilink")