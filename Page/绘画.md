[Mona_Lisa,_by_Leonardo_da_Vinci,_from_C2RMF_retouched.jpg](https://zh.wikipedia.org/wiki/File:Mona_Lisa,_by_Leonardo_da_Vinci,_from_C2RMF_retouched.jpg "fig:Mona_Lisa,_by_Leonardo_da_Vinci,_from_C2RMF_retouched.jpg")畫家[李奧納多·達文西繪畫的](../Page/李奧納多·達文西.md "wikilink")[蒙娜麗莎](../Page/蒙娜麗莎.md "wikilink")，為[西方世界最著名的藝術畫之一](../Page/西方世界.md "wikilink")。|right\]\]
[Fan_Kuan_-_Travelers_Among_Mountains_and_Streams_-_Google_Art_Project.jpg](https://zh.wikipedia.org/wiki/File:Fan_Kuan_-_Travelers_Among_Mountains_and_Streams_-_Google_Art_Project.jpg "fig:Fan_Kuan_-_Travelers_Among_Mountains_and_Streams_-_Google_Art_Project.jpg")[范寬所繪](../Page/范寬.md "wikilink")[谿山行旅圖](../Page/谿山行旅圖.md "wikilink")，其成就被譽為中國[山水畫裡最高的一座山](../Page/山水畫.md "wikilink")。\]\]

**繪畫**在[技术層面上](../Page/技术.md "wikilink")，是一個以[表面作為支撐面](../Page/表面.md "wikilink")，再在其之上加上[顏色的行為](../Page/顏色.md "wikilink")，那些表面可以是[紙張](../Page/紙張.md "wikilink")、[油畫布](../Page/帆布.md "wikilink")、[木材](../Page/木材.md "wikilink")、[玻璃](../Page/玻璃.md "wikilink")、[漆器或](../Page/漆器.md "wikilink")[混凝土等](../Page/混凝土.md "wikilink")，加顏色的工具可以是畫筆、也可以是刀、海綿或是等。

在藝術用語的層面上，繪畫的意義亦包含利用此藝術行為再加上[圖形](../Page/圖形.md "wikilink")、[構圖及其他](../Page/構圖.md "wikilink")[美學方法去達到表現出從事者希望表達的概念及意思这样一个目的](../Page/美學.md "wikilink")。

## 元素

[Chen_Hongshou,_leaf_album_painting.jpg](https://zh.wikipedia.org/wiki/File:Chen_Hongshou,_leaf_album_painting.jpg "fig:Chen_Hongshou,_leaf_album_painting.jpg")[陳洪綬繪製的](../Page/陳洪綬.md "wikilink")《玉堂柱石圖》\]\]

### 亮度

亮度的意思就是色彩明暗的程度（深淺的程度）比較淺 就是明度高 比較深
就是明度低，繪畫中的一個元素是亮度的感受及效果。空間中的每一個點都有不同的亮度，在繪畫中可以用黑色、白色及中間各種的灰色表示。在實務上，畫家可以用並列不同亮度的點來表達物體的形狀，相對的，若只用相同亮度的不同顏色，可能只能表示圖像的形狀而已。繪畫的基本手法會包括[幾何圖案](../Page/幾何.md "wikilink")、許多不同的觀點及組織（[透視法](../Page/透視法.md "wikilink")）及符號。例如畫家在看某一片特定的白牆時，因為陰影及周圍物體反射的影響，每一個點都會有不同的亮度，但在[理想上](../Page/柏拉图主义.md "wikilink")，即使在一個全黑無光的房間中，白牆仍然是白色的。因為即使房間中沒有光，是黑暗的，物體依然存在，並沒有消失不見，所以可證明其理論是正確的。

### 色彩和色调

色彩和色调是繪畫的基礎，正如[音樂中的](../Page/音樂.md "wikilink")[音高和](../Page/音高.md "wikilink")[节奏一様](../Page/节奏.md "wikilink")。顏色是高度主觀的，但其帶來的心理效果是可以觀測的，只是其效果可能會隨文化不同而不同，例如在西方文化中，黑色表示死亡及哀悼，但在東方文化中則是用白色表示
\[1\]。有些畫家、理論家、作家及科學家（包括[歌德](../Page/约翰·沃尔夫冈·冯·歌德.md "wikilink")、[瓦西里·康丁斯基及](../Page/瓦西里·康丁斯基.md "wikilink")[牛顿](../Page/艾萨克·牛顿.md "wikilink")）都提出了他們的。而且其中的語言也只能表示一種抽象的顏色，例如[紅這個字可以表示](../Page/紅.md "wikilink")[可見光譜中在純紅光附近一個範圍內的光](../Page/可見光譜.md "wikilink")。像在音樂上，不同的音樂家對於[中央C或是](../Page/C_\(音名\).md "wikilink")[C♯<sub>4</sub>有共同的定義](../Page/C♯_\(音名\).md "wikilink")，但不同的畫家對於顏色沒有一個公認的定義。對畫家而言，顏色不止是分為基本色或衍生色（如紅色、藍色、綠色、咖啡色等）而已。

在繪畫時，畫家是用顏料上色，因此畫家的「藍色」可能是以下顏色中的一種：[午夜藍](../Page/午夜藍.md "wikilink")、[靛色](../Page/靛色.md "wikilink")、[鈷藍色](../Page/鈷藍色.md "wikilink")、[群青等](../Page/群青.md "wikilink")。顏色在心理上及符號上的含意不一定和繪畫上的手法有關。顏色可以增加一些潛在的、衍生的意義，因此對於一幅畫的感覺是非常主觀的。這部分可以用音樂來類比．音樂中的音高（例如中央C）類似繪畫中的光，陰影類似[強弱](../Page/強弱法.md "wikilink")，顏色則類似不同[樂器的](../Page/樂器.md "wikilink")[音色](../Page/音色.md "wikilink")，雖然這些不一定可以形成旋律，但可以在音樂中加入一些不同的內容。

[Georges_Seurat_066.jpg](https://zh.wikipedia.org/wiki/File:Georges_Seurat_066.jpg "fig:Georges_Seurat_066.jpg")
(1859–91)的《馬戲團雜耍》\]\]

### 非传统元素

現代的畫家已經將繪畫加以延伸，例如由[立体主义衍生的](../Page/立体主义.md "wikilink")[珂拉琪](../Page/珂拉琪.md "wikilink")，嚴格來說不算是繪畫。有些現代畫家會因為材料的[紋理而整合不同的材料](../Page/紋理.md "wikilink")，例如[砂](../Page/砂.md "wikilink")、[水](../Page/水.md "wikilink")、[稻草或是](../Page/稻草.md "wikilink")[木材等](../Page/木材.md "wikilink")。像及[安塞尔姆·基弗的作品就屬於這一類](../Page/安塞尔姆·基弗.md "wikilink")。也有越來越多的藝術家社群利用電腦及[Adobe
Photoshop或](../Page/Adobe_Photoshop.md "wikilink")[Corel
Painter等軟體將顏色畫在數位的畫布上](../Page/Corel_Painter.md "wikilink")，若有需要也可以印在真正的畫布上。

### 节奏

繪畫中的节奏和音樂中的节奏一樣重要，若节奏定義為「結合到一個序列中的暫停」，則繪畫中也有其节奏。這樣的暫停可以讓想像力參與在其中，產生新的創作，型式、旋律及上色等。這些形式（或是任何種類資訊）的分佈在藝術工作上相當重要，會直接影響作品的審美價值。在審美價值上，感知的自由流動即視為是美。不但在藝術或是科學中，能量的自由流動會提昇其審美價值。

## 歷史

[Lascaux,_replica_05.JPG](https://zh.wikipedia.org/wiki/File:Lascaux,_replica_05.JPG "fig:Lascaux,_replica_05.JPG")（Lascaux）描繪[原牛的石洞壁畫](../Page/原牛.md "wikilink")\]\]

### 史前繪畫

已知最古老的繪畫位於法國[肖維岩洞](../Page/肖維岩洞.md "wikilink")（Grotte
Chauvet），部分[歷史學家認為它可以追溯至](../Page/歷史學家.md "wikilink")32000年前\[2\]。那些畫經由[紅赭石](../Page/紅赭石.md "wikilink")（）和黑色顏料作雕刻及繪畫，主題有[馬](../Page/馬.md "wikilink")、[犀牛](../Page/犀牛.md "wikilink")、[獅子](../Page/獅子.md "wikilink")、[水牛](../Page/水牛.md "wikilink")、[猛獁象或是打獵歸來的人類](../Page/猛獁象.md "wikilink")\[3\]。[石洞壁畫在世界各地均十分常見](../Page/石洞壁畫.md "wikilink")，例如法國、[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[中国](../Page/中国.md "wikilink")、[澳大利亞及](../Page/澳大利亞.md "wikilink")[印度等](../Page/印度.md "wikilink")。這些繪畫的發現對其存在目的引起了眾多不同的猜測，有些假設指出那些繪畫是當時的人認為繪畫可以捕捉動物的[靈魂](../Page/靈魂.md "wikilink")，令他們狩獵更為順利；一些假設則認為那是[泛靈論的一種現象](../Page/泛靈論.md "wikilink")，表達他們對[自然的尊敬](../Page/自然.md "wikilink")；亦有另一假設指人類與生俱來已有自我表達的需要，而那些繪畫正是其結果；此外亦有那是作為應用資訊的傳播的觀點\[4\]。

### 東西方傳統

西方文化主要是以[油畫及](../Page/油畫.md "wikilink")[水彩畫為主](../Page/水彩畫.md "wikilink")，在形式及主題上都有豐富的傳統。東方主要是以[墨及礦物顏料的畫為主](../Page/墨.md "wikilink")，在形式及主題上都有同様豐富的傳統。

### 近代至今

[攝影的發明對繪畫有很大的衝擊](../Page/攝影.md "wikilink")，第一張[照片在](../Page/照片.md "wikilink")1829年問世，在數十年後，攝影技術持續改進且廣為使用，因此繪畫原來用來記錄真實世界的功能也受到挑戰。19世紀末和20世紀初的一連串藝術活動，包括[印象派](../Page/印象派.md "wikilink")、[後印象派](../Page/後印象派.md "wikilink")、[野獸派](../Page/野獸派.md "wikilink")、[表現主義](../Page/表現主義.md "wikilink")、[立體主義及](../Page/立體主義.md "wikilink")[達達主義](../Page/達達主義.md "wikilink")，挑戰文藝復興時代對藝術的觀點。不過東方藝術及非洲藝術仍維持以往的風格，未受到攝影的影響。

[现代艺术和](../Page/现代艺术.md "wikilink")[當代藝術的觀點和傳統藝術的歷史價值及](../Page/當代藝術.md "wikilink")[概念有不少差異](../Page/概念.md "wikilink")。因此在1960年曾有人說嚴謹的繪畫型式已死\[5\]。不過這沒有阻止大多數的畫家繼續以繪畫作為其創作的部份或是全部。二十一世紀繪畫的活力及多變性蓋過了之前曾有人提出嚴謹藝術已死的宣告。在以為特點的時代，對於時代性的代表風格沒有共識。藝術家繼續以不同的風格及美學氣質進行創作，而其成就則留待市場的評價。

在二十一世紀初，仍在繼續的繪畫派別有[單色畫](../Page/單色畫.md "wikilink")、、、[挪用](../Page/挪用_\(艺术\).md "wikilink")、、[照相寫實主義](../Page/照相寫實主義.md "wikilink")、[表現主義](../Page/表現主義.md "wikilink")、[極簡主義](../Page/極簡主義.md "wikilink")、、[普普藝術](../Page/普普藝術.md "wikilink")、[奧普藝術](../Page/奧普藝術.md "wikilink")、[抽象表現主義](../Page/抽象表現主義.md "wikilink")、[色域繪畫](../Page/色域繪畫.md "wikilink")、[新表現主義](../Page/新表現主義.md "wikilink")、[珂拉琪](../Page/珂拉琪.md "wikilink")、繪畫、繪畫、繪畫、[後現代繪畫](../Page/後現代主義.md "wikilink")、繪畫、繪畫、環境[壁畫](../Page/壁畫.md "wikilink")、傳統的、[風景畫](../Page/風景畫.md "wikilink")、[肖像畫及](../Page/肖像畫.md "wikilink")[玻璃繪畫動畫](../Page/玻璃繪畫動畫.md "wikilink")。

## 繪畫類型

繪畫可以根據很多不同方法分類，例如按媒介、題材、文化等。一個畫種不一定只屬一個類型，而可以同屬多個分類，例如白描畫既可以按文化，也可以按形式分類。

### 媒介分類

  - [炭筆畫](../Page/炭筆畫.md "wikilink")
  - [油畫](../Page/油畫.md "wikilink")：用[乾性油調配的顏料的繪畫](../Page/乾性油.md "wikilink")，一般會將亞麻籽油煮沸後，再加入松脂等材料。油畫顏料乾後不變色，多種顏色調和不會變得骯髒，而且油畫顏料不透明，因此適合多層次的色彩，是西方繪畫史中的主體繪畫方式，在[文藝復興時期取代了之前使用的](../Page/文藝復興.md "wikilink")[蛋彩畫](../Page/蛋彩畫.md "wikilink")。[油畫在過去一百年左右在亞洲](../Page/中國油畫.md "wikilink")，尤其是[中國](../Page/中國油畫.md "wikilink")，也得到了長足而豐富的發展。
  - [水彩畫](../Page/水彩畫.md "wikilink")：用透明顏料和水調的一種繪畫。水彩畫主要是在紙上繪製，也可以在[莎草紙](../Page/莎草紙.md "wikilink")、[皮革](../Page/皮革.md "wikilink")、[帆布等表面繪製](../Page/帆布.md "wikilink")。在東方的水彩畫包括有以墨為原料的[水墨畫等](../Page/水墨畫.md "wikilink")。水彩畫顏料是透明的，一層顏色覆蓋另一層可以產生特殊的效果，但但調和顏色過多或覆蓋過多會使色彩骯髒，不適宜製作大幅作品，適合製作風景等清新明快的小幅畫作。
  - [水墨畫](../Page/水墨畫.md "wikilink")
  - [水粉畫](../Page/水粉畫.md "wikilink")：水粉又稱廣告彩。
  - [丙烯畫](../Page/丙烯畫.md "wikilink")：丙烯顏料又稱塑膠彩。
  - [粉彩畫](../Page/粉彩_\(顏料\).md "wikilink")：粉彩是一種固態的顏料，一般為棒狀，是用粉末狀的顏料和粘合劑加工而成\[6\]，粘合劑本身有中性的色澤，色彩飽和度較低，其顏色的效果較接近天然顏料的效果\[7\]。因為粉彩畫的表面脆弱，一般上面會用玻璃或是進行保護。
  - [木顏色筆畫](../Page/木顏色筆畫.md "wikilink")
  - [數位藝術](../Page/數位藝術.md "wikilink")
      - [像素畫](../Page/像素畫.md "wikilink")
  - [版畫](../Page/版畫.md "wikilink")：用印刷方式製作的圖畫。根據版材的不同，可分為[木版畫](../Page/木版畫.md "wikilink")、[銅版畫](../Page/銅版畫.md "wikilink")、[紙版畫](../Page/紙版畫.md "wikilink")、[石版畫](../Page/石版畫.md "wikilink")、[絲網版畫等](../Page/絲網版畫.md "wikilink")；版畫還依製版方法和印色技法分類，常見的有[腐蝕版畫](../Page/腐蝕版畫.md "wikilink")、[油印木刻](../Page/油印木刻.md "wikilink")、[浮水印木刻](../Page/浮水印木刻.md "wikilink")、[黑白版畫](../Page/黑白版畫.md "wikilink")、[套色版畫等](../Page/套色版畫.md "wikilink")。
  - [帛畫](../Page/帛畫.md "wikilink")
  - [壁畫](../Page/壁畫.md "wikilink")
  - 特別媒介：
      - [稻田畫](../Page/稻田畫.md "wikilink")

### 文化分類

  - [中國畫](../Page/中國畫.md "wikilink")
      - 按技法的細緻或粗放，可分為：
          - [寫意畫](../Page/寫意畫.md "wikilink")
          - [工筆畫](../Page/工筆畫.md "wikilink")
      - [白描畫](../Page/白描畫.md "wikilink")
  - [中國油畫](../Page/中國油畫.md "wikilink")
  - [西洋畫](../Page/西洋畫.md "wikilink")
  - [印度繪畫](../Page/印度繪畫.md "wikilink")
  - [日本畫](../Page/日本畫.md "wikilink")
      - [浮世繪](../Page/浮世繪.md "wikilink")

### 題材分類

  - [風景畫山水](../Page/風景畫.md "wikilink") 山 瀑布 房子
  - [靜物畫石膏像](../Page/靜物畫.md "wikilink")
  - [人物畫人類](../Page/人物畫.md "wikilink")
      - [人體畫](../Page/人體藝術.md "wikilink")（裸體畫）通常是畫女生為多
      - [肖像畫](../Page/肖像畫.md "wikilink")（頭像畫）半身像
      - [自畫像畫家畫自己](../Page/自畫像.md "wikilink")
      - [誇飾畫頭大身體小](../Page/誇飾畫.md "wikilink") 頭小身體大

### 風格分類

  - [寫實畫](../Page/寫實.md "wikilink") 就是畫的跟真的一樣 就像相片
  - [抽象畫](../Page/抽象藝術.md "wikilink") 就是腦袋裡的幾何圖形 詳見畢卡索

### 形式分類

  - [素描就是用黑色的鉛筆或是炭筆](../Page/素描.md "wikilink") 去畫出只有黑白灰的靜物或是人物人像 人類 動植物
  - [寫生就是去外面](../Page/寫生.md "wikilink") 家裡外面 把生態 寫實記錄下來 寫實記錄生態 詳見林經哲水彩作品
    簡忠威
  - [速寫快速畫圖](../Page/速寫.md "wikilink") 跟素描不一樣 素描畫面較細緻 速寫畫面較粗糙
  - [漫畫詳見日本已故漫畫家](../Page/漫畫.md "wikilink") 手塚治蟲 世界第一人採取把單張畫面分格 一個四條直線
    連成的方塊 裡面有人物 建築物 靜物 詳見日本漫畫
  - [插畫單張的彩色素描作品](../Page/插畫.md "wikilink") 也有一點文字 或是裝飾 詳見西洋美術 達文西 慕夏
    克林姆 天野喜孝 小島文美

## 审美与理论

[美學是有關](../Page/美學.md "wikilink")[藝術和](../Page/藝術.md "wikilink")[美的研究](../Page/美.md "wikilink")，是[康德或](../Page/伊曼努爾·康德.md "wikilink")[黑格爾等十八世紀及十九世紀的哲學家探討的主題之一](../Page/格奧爾格·威廉·弗里德里希·黑格爾.md "wikilink")。像[柏拉圖及](../Page/柏拉圖.md "wikilink")[亞里斯多德等希臘哲學家也對藝術及繪畫有特別的探討](../Page/亞里斯多德.md "wikilink")。在柏拉圖的哲學系統中，畫家和雕刻家的地位不高，他認為繪畫無法描述[真理](../Page/真理.md "wikilink")，繪畫只是複製實際事物，其中除了[工藝](../Page/工藝.md "wikilink")，沒有其他的成份在內，就像鞋匠及鑄鐵一様。不過在[達文西的時期](../Page/達文西.md "wikilink")，對繪畫的認知和柏拉圖的認知不同，當時認為「La
Pittura è cosa
mentale」（繪畫是心靈的事物）\[8\]。康德區分[美和](../Page/美.md "wikilink")[壯美兩種概念](../Page/崇高.md "wikilink")，分析兩者的相同處及不同處\[9\]。雖然他沒有提到繪畫，但像[约瑟夫·玛罗德·威廉·透纳及](../Page/约瑟夫·玛罗德·威廉·透纳.md "wikilink")[卡斯帕·弗里德里希等畫家將其概念延伸到繪畫上](../Page/卡斯帕·弗里德里希.md "wikilink")。

黑格爾認為無法對美給予一個共通性的定義，在他的美學研究中認為繪畫、詩歌和音樂是三項「浪漫」的藝術，其中有[符号化](../Page/符号.md "wikilink")，高度智力的目的\[10\]\[11\]。

## 绘画风格

繪畫是一個捕捉、記錄及表現不同創意目的的形式，它及其參與者的數量同樣地為數眾多。繪畫的本質可以是[自然及](../Page/自然主義美學.md "wikilink")[具像派的](../Page/具像派.md "wikilink")（如[靜物畫](../Page/靜物畫.md "wikilink")（still
life）或[風景畫](../Page/風景畫.md "wikilink")）、[影像繪畫](../Page/影像繪畫.md "wikilink")（photographic
painting）、[抽象畫](../Page/抽象藝術.md "wikilink")、有敘事性質的、[象徵主義](../Page/象徵主義.md "wikilink")、情感的或[政治性質的](../Page/政治.md "wikilink")。歷史上有一大部分的繪畫被[靈性主題及概念所主導](../Page/靈性.md "wikilink")，這些繪畫形式由[陶器上的](../Page/陶器.md "wikilink")[神話角色到](../Page/神話.md "wikilink")[西絲汀教堂的內壁及天花板上的](../Page/西絲汀教堂.md "wikilink")[聖經故事場景都有](../Page/聖經.md "wikilink")，它們會透過人類身體本身去表達靈性的主題。

## 擴展閱讀

  - [美術](../Page/美術.md "wikilink")
  - [繪畫理論](../Page/繪畫理論.md "wikilink")
  - [中國油畫](../Page/中國油畫.md "wikilink")

## 參考資料

## 延伸閱讀

  - Daniel, H., (1971) "*Encyclopedia of Themes and Subjects in
    Painting; Mythological, Biblical, Historical, Literary, Allegorical,
    and Topical*". New York, Harry N. Abrams Inc.

## 外部連結

  - [WikiPaintings](http://www.wikipaintings.org)
  - [A Treatise on Painting by Leonardo da Vinci (Kessinger
    Publishing)](http://www.kessinger.net/searchresults-orderthebook.php?ISBN=1417948353)
  - [Alberti, Leone
    Battista](../Page/Leone_Battista_Alberti.md "wikilink"), De Pictura
    (On Painting), 1435. [On Painting, in
    English](http://www.noteaccess.com/Texts/Alberti/), [De Pictura, in
    Latin](https://web.archive.org/web/20061102203241/http://www.liberliber.it/biblioteca/a/alberti/de_pictura/html/depictur.htm)
  - [Doerner, Max](../Page/Max_Doerner_\(artist\).md "wikilink") – [The
    Materials of the Artist and Their Use in Painting: With Notes on the
    Techniques of the Old
    Masters](http://www.amazon.com/Materials-Artist-Their-Use-Painting/dp/015657716X)
  - [Kandinsky – Concerning the Spiritual in Art (Dover
    Publications)](http://store.doverpublications.com/0486234118.html)
  - [The Journal of Eugene Delacroix (Phaidon
    Press)](https://web.archive.org/web/20090529060803/http://www.phaidon.com/Default.aspx/Web/the-journal-of-eugene-delacroix-9780714833590)
  - [Masterpieces of painting](http://www.fineartlib.info/).
  - [The Letters of Vincent van Gogh (Penguin
    Classics)](http://us.penguinclassics.com/nf/Book/BookDisplay/0,,9780140446746,00.html)
  - [Russian painters art in very high definition](http://lookatart.ru).
  - [Most famous painters art in very high
    definition](http://lookatart.ru/index2.html).

[繪畫](../Category/繪畫.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.
2.
3.
4.
5.  [After Modernist Painting: The History of a Contemporary
    Practice](http://www.ibtauris.com/Books/The%20arts/Art%20forms/Painting%20%20paintings/After%20Modernist%20Painting%20The%20History%20of%20a%20Contemporary%20Art.aspx)

6.  Mayer, Ralph,*The Artist's Handbook of Materials and Techniques*,
    Third Edition, New York: Viking, 1970, p. 312.
7.  Mayer, Ralph. The Artist's Handbook of Materials and Techniques.
    Viking Adult; 5th revised and updated edition, 1991. ISBN
    978-0-670-83701-4
8.  Rollason, C., & Mittapalli, R. (2002). *Modern criticism*. New
    Delhi: Atlantic Publishers and Distributors. p. 196. ISBN
    978-81-269-0187-6
9.  [康德對「壯美」的分析](http://faculty.ndhu.edu.tw/~shtang/classical/fruit/lecture/20051026.htm)
10.
11. "Painting and music are the specially romantic arts. Lastly, as a
    union of painting and music comes poetry, where the sensuous element
    is more than ever subordinate to the spirit." [Excerpted from
    Encyclopædia
    Britannica 1911](http://www.hegel.net/en/eb1911.htm#331)