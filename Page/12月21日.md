**12月21日**是[公历一年中的第](../Page/公历.md "wikilink")355天（[闰年第](../Page/闰年.md "wikilink")356天），离全年结束还有10天。

## 大事记

### 1世紀

  - [69年](../Page/69年.md "wikilink")：[韦斯巴芗即位为](../Page/韦帕芗.md "wikilink")[羅馬帝國皇帝](../Page/羅馬帝國.md "wikilink")。

### 12世紀

  - [1124年](../Page/1124年.md "wikilink")：[霍诺留斯二世当选为](../Page/霍诺留斯二世.md "wikilink")[教宗](../Page/教宗.md "wikilink")。

### 17世紀

  - [1620年](../Page/1620年.md "wikilink")：[五月花号抵达今](../Page/五月花号.md "wikilink")[美國](../Page/美國.md "wikilink")[马萨诸塞州](../Page/麻薩諸塞州.md "wikilink")[普利茅斯](../Page/普利茅斯.md "wikilink")，开始建设[英国在北美的第一个殖民地](../Page/英国.md "wikilink")。

### 20世紀

  - [1919年](../Page/1919年.md "wikilink")：鼓吹抵制征兵制度的[无政府主义支持者](../Page/无政府主义.md "wikilink")[艾玛·高德曼在服刑](../Page/艾玛·高德曼.md "wikilink")2年后，被美国政府驱逐出境而前往俄罗斯。
  - [1937年](../Page/1937年.md "wikilink")：改编自《[格林童话](../Page/格林童话.md "wikilink")》的美国[迪士尼首部](../Page/迪士尼.md "wikilink")[动画长片](../Page/迪士尼经典动画长片.md "wikilink")《[-{zh-cn:白雪公主与七个小矮人;zh-tw:白雪公主;zh-hk:雪姑七友;zh-mo:雪姑七友;}-](../Page/白雪公主_\(1937年電影\).md "wikilink")》首映。
  - [1962年](../Page/1962年.md "wikilink")：[挪威首个](../Page/挪威.md "wikilink")[国家公园](../Page/国家公园.md "wikilink")[龙达讷国家公园正式成立](../Page/龙达讷国家公园.md "wikilink")。
  - [1965年](../Page/1965年.md "wikilink")：在经由68个[联合国成员国签署](../Page/联合国.md "wikilink")《[消除一切形式种族歧视国际公约](../Page/消除一切形式种族歧视国际公约.md "wikilink")》后该公约正式获得通过。
  - [1968年](../Page/1968年.md "wikilink")：美国[阿波罗8号在](../Page/阿波罗8号.md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")[肯尼迪航天中心发射升空](../Page/肯尼迪航天中心.md "wikilink")，执行人类首次绕[月航行的太空任务](../Page/月球.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：从[德国](../Page/德国.md "wikilink")[法兰克福飞往美国](../Page/法兰克福.md "wikilink")[底特律的泛美航空](../Page/底特律.md "wikilink")103航班在[苏格兰](../Page/苏格兰.md "wikilink")[洛克比上空因](../Page/洛克比.md "wikilink")[恐怖分子预先放置的](../Page/恐怖分子.md "wikilink")[炸弹爆炸而坠毁](../Page/炸弹.md "wikilink")，导致270人罹難。史稱「[洛克比空難](../Page/洛克比空難.md "wikilink")」。
  - [1991年](../Page/1991年.md "wikilink")：[中華民國舉辦第二屆](../Page/中華民國.md "wikilink")[國民大會代表選舉](../Page/國民大會.md "wikilink")，為中華民國「國會全面改選」踏出第一步。
  - 1991年：[苏联](../Page/苏联.md "wikilink")11个加盟共和国的领导人在[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")[阿拉木圖签署](../Page/阿拉木圖.md "wikilink")《[阿拉木图宣言](../Page/阿拉木图宣言.md "wikilink")》，成立[独联体](../Page/独联体.md "wikilink")，宣布苏联停止存在。
  - [1992年](../Page/1992年.md "wikilink")：[中国](../Page/中国.md "wikilink")[西昌卫星发射中心负责发射的](../Page/西昌卫星发射中心.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")[澳普图斯B2卫星在升空约](../Page/澳普图斯B2卫星.md "wikilink")48秒后爆炸。
  - [1993年](../Page/1993年.md "wikilink")：[博羅什當選](../Page/博羅什.md "wikilink")[匈牙利新總理](../Page/匈牙利.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[以色列國防軍从](../Page/以色列國防軍.md "wikilink")[伯利恒撤军](../Page/伯利恒.md "wikilink")，伯利恒成为[巴勒斯坦的自治城市](../Page/巴勒斯坦.md "wikilink")。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[香港](../Page/香港.md "wikilink")[九廣鐵路](../Page/九廣鐵路.md "wikilink")[馬鞍山鐵路通車](../Page/馬鞍山鐵路.md "wikilink")（現時為[港鐵](../Page/港鐵.md "wikilink")[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")）。
  - [2008年](../Page/2008年.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[台北](../Page/台北市.md "wikilink")[ING台北馬拉松最後一次在](../Page/ING台北國際馬拉松比賽.md "wikilink")[台北市政府前廣場比賽之後](../Page/台北市政府.md "wikilink")，正式走入歷史。
  - [2009年](../Page/2009年.md "wikilink")：第四次[江陳會在](../Page/江陳會.md "wikilink")[台中市舉行](../Page/台中市.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：古[馬雅文化曆法的最後一天](../Page/馬雅.md "wikilink")。壬辰年[冬至](../Page/冬至.md "wikilink")。因電影[2012的影響](../Page/2012_\(电影\).md "wikilink")，訛傳[2012年預言的日期](../Page/2012年預言.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[中華民國](../Page/中華民國.md "wikilink")[法務部長](../Page/法務部.md "wikilink")[曾勇夫簽署](../Page/曾勇夫.md "wikilink")[死刑執行令](../Page/死刑.md "wikilink")，於[法務部矯正署](../Page/法務部矯正署.md "wikilink")[台北監獄](../Page/台北監獄.md "wikilink")[刑場](../Page/刑場.md "wikilink")[槍決](../Page/槍決.md "wikilink")：[曾思儒和](../Page/曾思儒.md "wikilink")[洪明聰二名](../Page/洪明聰.md "wikilink")[死刑犯](../Page/死刑犯.md "wikilink")。於[法務部矯正署](../Page/法務部矯正署.md "wikilink")[台中監獄](../Page/台中監獄.md "wikilink")[刑場](../Page/刑場.md "wikilink")[槍決](../Page/槍決.md "wikilink")：[陳金火和](../Page/陳金火.md "wikilink")[廣德強二名](../Page/廣德強.md "wikilink")[死刑犯](../Page/死刑犯.md "wikilink")。於[法務部矯正署](../Page/法務部矯正署.md "wikilink")[台南監獄](../Page/台南監獄.md "wikilink")[刑場](../Page/刑場.md "wikilink")[槍決](../Page/槍決.md "wikilink")：[黃賢正一名](../Page/黃賢正.md "wikilink")[死刑犯](../Page/死刑犯.md "wikilink")。於[法務部矯正署](../Page/法務部矯正署.md "wikilink")[高雄第二監獄](../Page/高雄第二監獄.md "wikilink")[刑場](../Page/刑場.md "wikilink")[槍決](../Page/槍決.md "wikilink")：[戴德穎一名](../Page/戴德穎.md "wikilink")[死刑犯](../Page/死刑犯.md "wikilink")。一共於四處[刑場](../Page/刑場.md "wikilink")[槍決六名](../Page/槍決.md "wikilink")[死刑犯](../Page/死刑犯.md "wikilink")。

## 出生

  - [1401年](../Page/1401年.md "wikilink")：[马萨乔](../Page/马萨乔.md "wikilink")，[義大利](../Page/義大利.md "wikilink")[文藝復興畫家](../Page/文藝復興.md "wikilink")，第一位使用滅點透視法的畫家（逝於[1428年](../Page/1428年.md "wikilink")）
  - [1795年](../Page/1795年.md "wikilink")：[利奧波德·馮·蘭克](../Page/利奧波德·馮·蘭克.md "wikilink")，[德國歷史學家](../Page/德國.md "wikilink")，被譽為「近代史學之父」（逝於[1886年](../Page/1886年.md "wikilink")）
  - [1804年](../Page/1804年.md "wikilink")：[本傑明·迪斯雷利](../Page/本傑明·迪斯雷利.md "wikilink")，第39、41任[英国首相](../Page/英国首相.md "wikilink")（逝於[1881年](../Page/1881年.md "wikilink")）
  - [1805年](../Page/1805年.md "wikilink")：[托馬斯·格雷姆](../Page/托馬斯·格雷姆.md "wikilink")，[蘇格蘭化學家](../Page/蘇格蘭.md "wikilink")（逝於[1869年](../Page/1869年.md "wikilink")）
  - [1815年](../Page/1815年.md "wikilink")：[托马·库蒂尔](../Page/托马·库蒂尔.md "wikilink")，[法國歷史畫家](../Page/法國.md "wikilink")、教師（逝於[1879年](../Page/1879年.md "wikilink")）
  - [1850年](../Page/1850年.md "wikilink")：[兹德涅克·菲比赫](../Page/兹德涅克·菲比赫.md "wikilink")，[捷克作曲家](../Page/捷克.md "wikilink")（逝於[1900年](../Page/1900年.md "wikilink")）
  - [1866年](../Page/1866年.md "wikilink")：[昴德·冈昂](../Page/昴德·冈昂.md "wikilink")，[英國出生的](../Page/英國.md "wikilink")[愛爾蘭革命家](../Page/愛爾蘭.md "wikilink")、女權主義者和演員（逝於[1953年](../Page/1953年.md "wikilink")）
  - [1878年](../Page/1878年.md "wikilink")：[扬·武卡谢维奇](../Page/扬·武卡谢维奇.md "wikilink")，[波兰數學家](../Page/波兰.md "wikilink")（逝於[1956年](../Page/1956年.md "wikilink")）
  - [1890年](../Page/1890年.md "wikilink")：[赫尔曼·约瑟夫·马勒](../Page/赫尔曼·约瑟夫·马勒.md "wikilink")，[美国遺傳學家及教育家](../Page/美国.md "wikilink")，[1946年](../Page/1946年.md "wikilink")[诺贝尔生理学或医学奖得主](../Page/诺贝尔生理学或医学奖.md "wikilink")（逝於[1967年](../Page/1967年.md "wikilink")）
  - [1909年](../Page/1909年.md "wikilink")：[松本清张](../Page/松本清张.md "wikilink")，[日本推理小說作家](../Page/日本.md "wikilink")（逝於[1992年](../Page/1992年.md "wikilink")）
  - [1914年](../Page/1914年.md "wikilink")：[林百欣](../Page/林百欣.md "wikilink")，[香港企業家](../Page/香港.md "wikilink")（逝於[2005年](../Page/2005年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[海因里希·伯尔](../Page/海因里希·伯尔.md "wikilink")，德国作家，[1972年](../Page/1972年.md "wikilink")[诺贝尔文学奖得主](../Page/诺贝尔文学奖.md "wikilink")（逝於[1985年](../Page/1985年.md "wikilink")）
  - [1918年](../Page/1918年.md "wikilink")：[库尔特·瓦尔德海姆](../Page/库尔特·瓦尔德海姆.md "wikilink")，[奥地利第](../Page/奥地利.md "wikilink")9任[總統](../Page/奥地利总统.md "wikilink")，[联合国第四任](../Page/联合国.md "wikilink")[秘书长](../Page/联合国秘书长.md "wikilink")（逝於[2007年](../Page/2007年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[王方定](../Page/王方定.md "wikilink")，[中国核化学家](../Page/中国.md "wikilink")
  - [1935年](../Page/1935年.md "wikilink")：[約翰·艾維森](../Page/約翰·艾維森.md "wikilink")，美國電影導演，1976年以《[洛基](../Page/洛基.md "wikilink")》獲[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")（逝於[2017年](../Page/2017年.md "wikilink")）
  - 1935年：[菲尔·唐纳修](../Page/菲尔·唐纳修.md "wikilink")，美國作家、製片人，第一個脫口秀節目《菲爾·唐納修秀》原創者與主持人
  - [1937年](../Page/1937年.md "wikilink")：[珍·芳達](../Page/珍·芳達.md "wikilink")，美國女演員，1972年以《[柳巷芳草](../Page/柳巷芳草.md "wikilink")》獲[奧斯卡影后](../Page/奧斯卡影后.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[弗兰克·扎帕](../Page/弗兰克·扎帕.md "wikilink")，美國作曲家、創作歌手、電吉他手、唱片製作人、電影導演（逝於[1993年](../Page/1993年.md "wikilink")）
  - [1941年](../Page/1941年.md "wikilink")：[盧海鵬](../Page/盧海鵬.md "wikilink")，香港演員
  - [1942年](../Page/1942年.md "wikilink")：[胡锦涛](../Page/胡锦涛.md "wikilink")，前[中國共產黨中央委員會總書記](../Page/中國共產黨中央委員會總書記.md "wikilink")、[中华人民共和国主席](../Page/中华人民共和国主席.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[邁可·提爾森·湯瑪斯](../Page/邁可·提爾森·湯瑪斯.md "wikilink")，美國指挥家、鋼琴家及作曲家
  - 1944年：[鄭筱萸](../Page/鄭筱萸.md "wikilink")，前[中華人民共和國國家食品藥品監督管理局局長](../Page/中華人民共和國國家食品藥品監督管理局.md "wikilink")（逝於[2007年](../Page/2007年.md "wikilink")）
  - [1947年](../Page/1947年.md "wikilink")：[帕科·德卢西亚](../Page/帕科·德卢西亚.md "wikilink")，[西班牙弗拉明戈吉他手](../Page/西班牙.md "wikilink")，被稱為弗拉明戈之神
  - [1948年](../Page/1948年.md "wikilink")：[森姆·L·積遜](../Page/森姆·L·積遜.md "wikilink")，美國演員及監製
  - [1949年](../Page/1949年.md "wikilink")：[托马·桑卡拉](../Page/托马·桑卡拉.md "wikilink")，[上沃爾特第五任總統](../Page/上沃爾特.md "wikilink")、[布吉納法索第一任總統](../Page/布吉納法索.md "wikilink")（逝於[1987年](../Page/1987年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[徐立之](../Page/徐立之.md "wikilink")，[香港大學校長](../Page/香港大學.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[-{zh-hans:克里斯·埃弗特;zh-hant:姬絲·艾華特;zh-tw:克里斯·艾芙特;}-](../Page/克里斯·埃弗特.md "wikilink")，美國女子網球運動員
  - [1959年](../Page/1959年.md "wikilink")：[弗洛伦斯·格里菲斯-乔伊娜](../Page/弗洛伦斯·格里菲斯-乔伊娜.md "wikilink")，美國田徑運動員
  - 1959年：[黃和聯](../Page/黃和聯.md "wikilink")，馬來西亞政治人物（逝於[2014年](../Page/2014年.md "wikilink")）
  - [1960年](../Page/1960年.md "wikilink")：[雪莉·雷曼](../Page/雪莉·雷曼.md "wikilink")，[巴基斯坦政治人物](../Page/巴基斯坦.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[中嶋敦子](../Page/中嶋敦子.md "wikilink")，[日本动画](../Page/日本动画.md "wikilink")[人物設計師](../Page/人物設計.md "wikilink")
  - 1961年：[吳鎮宇](../Page/吳鎮宇.md "wikilink")，香港演員，第37屆[金馬獎最佳男主角](../Page/金馬獎.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[堀內博之](../Page/堀內博之.md "wikilink")，日本动画人物設計師
  - [1965年](../Page/1965年.md "wikilink")：[本木雅弘](../Page/本木雅弘.md "wikilink")，日本演員
  - [1966年](../Page/1966年.md "wikilink")：[-{zh-hans:基弗·萨瑟兰;;zh-cn:基弗·萨瑟兰;zh-tw:基佛·蘇德蘭;zh-hk:基夫·修打蘭;zh-hant:基佛·蘇德蘭;zh-mo:基夫·修打蘭;}-](../Page/基夫·修打蘭.md "wikilink")，蘇格蘭裔[加拿大美劇演員](../Page/加拿大.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[米哈伊爾·薩卡什維利](../Page/米哈伊爾·薩卡什維利.md "wikilink")，第3任[格魯吉亞總統](../Page/格魯吉亞總統.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[茱莉·蝶兒](../Page/茱莉·蝶兒.md "wikilink")，法裔美籍女演員、編劇、導演、歌手
  - [1972年](../Page/1972年.md "wikilink")：[克劳迪娅·波尔](../Page/克劳迪娅·波尔.md "wikilink")，[哥斯大黎加游泳運動員](../Page/哥斯大黎加.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[伊曼努爾·馬克龍](../Page/埃马纽埃尔·马克龙.md "wikilink")，[法國第](../Page/法国.md "wikilink")25任[總統](../Page/法国总统.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[孙宁](../Page/孙宁.md "wikilink")，中国演员
  - [1979年](../Page/1979年.md "wikilink")：[艾莉絲](../Page/艾莉絲_\(主持人\).md "wikilink")，台灣[主持人](../Page/主持人.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[克里斯蒂安·扎卡尔多](../Page/克里斯蒂安·扎卡尔多.md "wikilink")，義大利足球運動員
  - [1982年](../Page/1982年.md "wikilink")：[黃智雯](../Page/黃智雯.md "wikilink")，香港女演員
  - 1982年：[瑪希拉·罕](../Page/瑪希拉·罕.md "wikilink")，[巴基斯坦女演員](../Page/巴基斯坦.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[杰克逊·拉斯波恩](../Page/杰克逊·拉斯波恩.md "wikilink")，美国男演员
  - [1988年](../Page/1988年.md "wikilink")：[狄以達](../Page/狄以達.md "wikilink")，香港男歌手
  - [1989年](../Page/1989年.md "wikilink")：[塔曼娜·巴提亞](../Page/塔曼娜·巴提亞.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[Bobby](../Page/金知元_\(饶舌者\).md "wikilink")，韩国男子团体[iKON成员](../Page/iKON.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[馬嘉伶](../Page/馬嘉伶.md "wikilink")，台灣、日本偶像，[AKB48首位海外成員](../Page/AKB48.md "wikilink")

## 逝世

  - [1375年](../Page/1375年.md "wikilink")：[薄迦丘](../Page/薄迦丘.md "wikilink")，[意大利文学家](../Page/意大利.md "wikilink")
  - [1807年](../Page/1807年.md "wikilink")：[约翰·牛顿](../Page/约翰·牛顿.md "wikilink")，英国[牧师](../Page/牧师.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[弗兰克·B·凯洛格](../Page/弗兰克·B·凯洛格.md "wikilink")，[美国国务卿](../Page/美国国务卿.md "wikilink")，[诺贝尔和平奖得主](../Page/诺贝尔和平奖.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[斯科特·菲茨杰拉德](../Page/斯科特·菲茨杰拉德.md "wikilink")，美国文学家
  - [1945年](../Page/1945年.md "wikilink")：[乔治·巴顿](../Page/乔治·巴顿.md "wikilink")，[美国陆军上将](../Page/美国陆军.md "wikilink")（[1885年出生](../Page/1885年.md "wikilink")）

## 节日、风俗习惯

  - [世界篮球日](../Page/世界篮球日.md "wikilink")