`| mapy                    = {{ #expr:17 + 14 / 60.0 }}`
`| locator_position        = `
`| native_name             = Alberobello`
`| name                    =  阿尔贝罗贝洛`
`| website                 = `<http://www.comune.alberobello.ba.it/>

}}

[Toits_Alberobello_(1024).jpg](https://zh.wikipedia.org/wiki/File:Toits_Alberobello_\(1024\).jpg "fig:Toits_Alberobello_(1024).jpg")

[Alberobello_BW_2016-10-16_13-43-03.jpg](https://zh.wikipedia.org/wiki/File:Alberobello_BW_2016-10-16_13-43-03.jpg "fig:Alberobello_BW_2016-10-16_13-43-03.jpg")

**阿尔贝罗贝洛**（Alberobello）是[意大利](../Page/意大利.md "wikilink")[普利亚大区](../Page/普利亚.md "wikilink")[巴里省的一個小城](../Page/巴里省.md "wikilink")，有人口約一萬一千人。城內的[特鲁洛](../Page/特鲁洛.md "wikilink")（trullo，复数为trulli）建築世界知名，1996年被[联合国教科文组织列为](../Page/联合国教科文组织.md "wikilink")[世界文化遗产](../Page/世界文化遗产.md "wikilink")。

[特鲁洛的特色是牆壁用石灰塗成白色](../Page/特鲁洛.md "wikilink")，屋頂則用灰色的扁平石塊堆成圓錐形。據說是當地人為了逃稅而造，收稅時就把屋頂拆掉，表示這裡沒有人居住。

## 友好城市

  - [蒙泰圣安杰洛](../Page/蒙泰圣安杰洛.md "wikilink") 2013年

  - [安德里亞](../Page/安德里亞.md "wikilink") 2013年

[A](../Category/義大利世界遺產.md "wikilink")