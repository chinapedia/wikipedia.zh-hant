[Lei_Cheong_Lap.jpg](https://zh.wikipedia.org/wiki/File:Lei_Cheong_Lap.jpg "fig:Lei_Cheong_Lap.jpg")
**李祥立**（）生於[澳門](../Page/澳門.md "wikilink")，[澳門](../Page/澳門.md "wikilink")[教育工作者](../Page/教育工作者.md "wikilink")，現任[廣東省](../Page/廣東省.md "wikilink")[政協委員](../Page/政協委員.md "wikilink")，前任[澳門培正中學校長](../Page/澳門培正中學.md "wikilink")。

他為[臺灣師範大學理學士](../Page/臺灣師範大學.md "wikilink")、澳門[東亞大學工商管理碩士](../Page/東亞大學.md "wikilink")；他也曾任[澳門大學學術部部長](../Page/澳門大學.md "wikilink")、第31-35屆[國際數學奧林匹克競賽評判及澳門代表隊領隊](../Page/國際數學奧林匹克競賽.md "wikilink")。

2006年12月20日，李祥立獲[澳門特區政府授予](../Page/澳門特區政府.md "wikilink")[教育功績勳章以表揚他對澳門教育界的貢獻](../Page/教育功績勳章.md "wikilink")。

2007年5月30日，李祥立在澳門培正中學級社聯絡員代表大會上宣布會在學年結束後退休，原澳門培正校長之職由[高錦輝接任](../Page/高錦輝.md "wikilink")。\[1\]

## 其他職務

  - 澳門科技大學第二屆咨詢委員會委員
  - 澳門中華文化交流協會副會長
  - 澳門特別行政區第一任行政長官推選委員會委員
  - 澳門特別行政區第二任行政長官選舉委員會委員
  - 澳門特別行政區第9, 10, 11屆全國人大代表選舉會議成員
  - 廣東省政協第7,8,9屆委員
  - [廣州培正中學校董](../Page/廣州培正中學.md "wikilink")（1996-2007）
  - [澳門紅藍體育會名譽會長](../Page/澳門紅藍體育會.md "wikilink")（1995-2007）
  - 澳門中華教育會監事長（2001-2007）
  - [澳門日報讀者公益基金會名譽顧問](../Page/澳門日報.md "wikilink")（1995-2006）
  - 澳門市販互助會名譽顧問
  - 澳門戒煙保健總會名譽顧問

及其他多個功能團體的顧問

主要著作有《**數學漫談**》等書籍5冊及多篇論文。

## 參見

  - [澳門培正中學](../Page/澳門培正中學.md "wikilink")
  - [澳門教育](../Page/澳門教育.md "wikilink")

## 参考資料

<div class="references-small">

<references />

</div>

[Category:澳門文化界人士](../Category/澳門文化界人士.md "wikilink")
[Category:中国教育家](../Category/中国教育家.md "wikilink")
[X](../Category/李姓.md "wikilink")
[Category:澳門大學校友](../Category/澳門大學校友.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")

1.  [級社聯絡員代表大會](http://www3.puiching.edu.mo/modules/news/article.php?storyid=225)
     - 澳門培正中學校園網