**[尼日利亚](../Page/尼日利亚.md "wikilink")**全国下分36个[州](../Page/州.md "wikilink")（[英语](../Page/英语.md "wikilink")：）与1个[特区](../Page/特区.md "wikilink")。每个州都设有一[单院制的](../Page/单院制.md "wikilink")[州民大会](../Page/州民大會_\(奈及利亞\).md "wikilink")（）以及一个[民选的](../Page/民选.md "wikilink")[州长](../Page/州长.md "wikilink")。这些一级[行政区划分別为](../Page/行政区划.md "wikilink")：

<table>
<tbody>
<tr class="odd">
<td><ol>
<li><a href="../Page/阿比亚州.md" title="wikilink">阿比亚州</a>（）</li>
<li><a href="../Page/阿达马瓦州.md" title="wikilink">阿达马瓦州</a>（）</li>
<li><a href="../Page/阿夸伊博姆州.md" title="wikilink">阿夸伊博姆州</a>（）</li>
<li><a href="../Page/阿南布拉州.md" title="wikilink">阿南布拉州</a>（）</li>
<li><a href="../Page/包奇州.md" title="wikilink">包奇州</a>（）</li>
<li><a href="../Page/巴耶尔萨州.md" title="wikilink">巴耶尔萨州</a>（）</li>
<li><a href="../Page/贝努埃州.md" title="wikilink">贝努埃州</a>（）</li>
<li><a href="../Page/博尔诺州.md" title="wikilink">博尔诺州</a>（）</li>
<li><a href="../Page/克里斯河州.md" title="wikilink">克里斯河州</a>（）</li>
<li><a href="../Page/三角州.md" title="wikilink">三角州</a>（）</li>
<li><a href="../Page/埃邦伊州.md" title="wikilink">埃邦伊州</a>（）</li>
<li><a href="../Page/埃多州.md" title="wikilink">埃多州</a>（）</li>
<li><a href="../Page/埃基蒂州.md" title="wikilink">埃基蒂州</a>（）</li>
<li><a href="../Page/埃努古州.md" title="wikilink">埃努古州</a>（）</li>
<li><a href="../Page/贡贝州.md" title="wikilink">贡贝州</a>（）</li>
<li><a href="../Page/伊莫州.md" title="wikilink">伊莫州</a>（）</li>
<li><a href="../Page/吉加瓦州.md" title="wikilink">吉加瓦州</a>（）</li>
<li><a href="../Page/卡杜纳州.md" title="wikilink">卡杜纳州</a>（）</li>
<li><a href="../Page/卡诺州.md" title="wikilink">卡诺州</a>（）</li>
<li><a href="../Page/卡齐纳州.md" title="wikilink">卡齐纳州</a>（）</li>
<li><a href="../Page/凯比州.md" title="wikilink">凯比州</a>（）</li>
<li><a href="../Page/科吉州.md" title="wikilink">科吉州</a>（）</li>
<li><a href="../Page/夸拉州.md" title="wikilink">夸拉州</a>（）</li>
<li><a href="../Page/拉各斯州.md" title="wikilink">拉各斯州</a>（）</li>
<li><a href="../Page/纳萨拉瓦州.md" title="wikilink">纳萨拉瓦州</a>（）</li>
<li><a href="../Page/尼日尔州.md" title="wikilink">尼日尔州</a>（）</li>
<li><a href="../Page/奥贡州.md" title="wikilink">奥贡州</a>（）</li>
<li><a href="../Page/翁多州.md" title="wikilink">翁多州</a>（）</li>
<li><a href="../Page/奥孙州.md" title="wikilink">奥孙州</a>（）</li>
<li><a href="../Page/奥约州.md" title="wikilink">奥约州</a>（）</li>
<li><a href="../Page/高原州.md" title="wikilink">高原州</a>（）</li>
<li><a href="../Page/河流州.md" title="wikilink">河流州</a>（）</li>
<li><a href="../Page/索科托州.md" title="wikilink">索科托州</a>（）</li>
<li><a href="../Page/塔拉巴州.md" title="wikilink">塔拉巴州</a>（）</li>
<li><a href="../Page/约贝州.md" title="wikilink">约贝州</a>（）</li>
<li><a href="../Page/扎姆法拉州.md" title="wikilink">扎姆法拉州</a>（）</li>
<li><a href="../Page/聯邦首都特區_(奈及利亞).md" title="wikilink">联邦首都特区</a>（）</li>
</ol></td>
<td><p><a href="../Page/图像:NigeriaNumbered.png.md" title="wikilink">right</a></p></td>
</tr>
</tbody>
</table>

## 奈及利亞州一览表

|                                                                                                                                      | 州名                                     | 英文                        | 首府                                   | 英文            |
| ------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------- | ------------------------- | ------------------------------------ | ------------- |
| [Abia_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Abia_State_Nigeria.png "fig:Abia_State_Nigeria.png")                   | [阿比亚州](../Page/阿比亚州.md "wikilink")     | Abia State                | [乌穆阿希亚](../Page/乌穆阿希亚.md "wikilink") | Umuahia       |
| [Adamawa_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Adamawa_State_Nigeria.png "fig:Adamawa_State_Nigeria.png")          | [阿达马瓦州](../Page/阿达马瓦州.md "wikilink")   | Adamawa State             | [约拉](../Page/约拉.md "wikilink")       | Yola          |
| [AkwaIbom_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:AkwaIbom_State_Nigeria.png "fig:AkwaIbom_State_Nigeria.png")       | [阿夸伊博姆州](../Page/阿夸伊博姆州.md "wikilink") | Akwa-Ibom State           | [乌约](../Page/乌约.md "wikilink")       | Uyo           |
| [Anambra_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Anambra_State_Nigeria.png "fig:Anambra_State_Nigeria.png")          | [阿南布拉州](../Page/阿南布拉州.md "wikilink")   | Anambra State             | [奥卡](../Page/奥卡.md "wikilink")       | Awka          |
| [Bauchi_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Bauchi_State_Nigeria.png "fig:Bauchi_State_Nigeria.png")             | [包奇州](../Page/包奇州.md "wikilink")       | Bauchi State              | [包奇](../Page/包奇.md "wikilink")       | Bauchi        |
| [Bayelsa_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Bayelsa_State_Nigeria.png "fig:Bayelsa_State_Nigeria.png")          | [巴耶尔萨州](../Page/巴耶尔萨州.md "wikilink")   | Bayelsa State             | [耶诺亚](../Page/耶诺亚.md "wikilink")     | Yenagoa       |
| [Benue_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Benue_State_Nigeria.png "fig:Benue_State_Nigeria.png")                | [贝努埃州](../Page/贝努埃州.md "wikilink")     | Benue State               | [马库尔迪](../Page/马库尔迪.md "wikilink")   | Makurdi       |
| [Borno_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Borno_State_Nigeria.png "fig:Borno_State_Nigeria.png")                | [博尔诺州](../Page/博尔诺州.md "wikilink")     | Borno State               | [迈杜古里](../Page/迈杜古里.md "wikilink")   | Maiduguri     |
| [CrossRiver_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:CrossRiver_State_Nigeria.png "fig:CrossRiver_State_Nigeria.png") | [克里斯河州](../Page/克里斯河州.md "wikilink")   | Cross River State         | [卡拉巴尔](../Page/卡拉巴尔.md "wikilink")   | Calabar       |
| [Delta_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Delta_State_Nigeria.png "fig:Delta_State_Nigeria.png")                | [三角州](../Page/三角州.md "wikilink")       | Delta State               | [阿萨巴](../Page/阿萨巴.md "wikilink")     | Asaba         |
| [Ebonyi_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Ebonyi_State_Nigeria.png "fig:Ebonyi_State_Nigeria.png")             | [埃邦伊州](../Page/埃邦伊州.md "wikilink")     | Ebonyi State              | [阿巴卡利基](../Page/阿巴卡利基.md "wikilink") | Abakaliki     |
| [Edo_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Edo_State_Nigeria.png "fig:Edo_State_Nigeria.png")                      | [埃多州](../Page/埃多州.md "wikilink")       | Edo State                 | [贝宁城](../Page/贝宁城.md "wikilink")     | Benin City    |
| [Ekiti_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Ekiti_State_Nigeria.png "fig:Ekiti_State_Nigeria.png")                | [埃基蒂州](../Page/埃基蒂州.md "wikilink")     | Ekiti State               | [阿多埃基蒂](../Page/阿多埃基蒂.md "wikilink") | Ado-Ekiti     |
| [Enugu_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Enugu_State_Nigeria.png "fig:Enugu_State_Nigeria.png")                | [埃努古州](../Page/埃努古州.md "wikilink")     | Enugu State               | [埃努古](../Page/埃努古.md "wikilink")     | Enugu         |
| [Gombe_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Gombe_State_Nigeria.png "fig:Gombe_State_Nigeria.png")                | [贡贝州](../Page/贡贝州.md "wikilink")       | Gombe State               | [贡贝](../Page/贡贝.md "wikilink")       | Gombe         |
| [Imo_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Imo_State_Nigeria.png "fig:Imo_State_Nigeria.png")                      | [伊莫州](../Page/伊莫州.md "wikilink")       | Imo State                 | [奥韦里](../Page/奥韦里.md "wikilink")     | Owerri        |
| [Jigawa_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Jigawa_State_Nigeria.png "fig:Jigawa_State_Nigeria.png")             | [吉加瓦州](../Page/吉加瓦州.md "wikilink")     | Jigawa State              | [杜切](../Page/杜切.md "wikilink")       | Dutse         |
| [Kaduna_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Kaduna_State_Nigeria.png "fig:Kaduna_State_Nigeria.png")             | [卡杜纳州](../Page/卡杜纳州.md "wikilink")     | Kaduna State              | [卡杜纳](../Page/卡杜纳.md "wikilink")     | Kaduna        |
| [Kano_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Kano_State_Nigeria.png "fig:Kano_State_Nigeria.png")                   | [卡诺州](../Page/卡诺州.md "wikilink")       | Kano State                | [卡诺](../Page/卡诺.md "wikilink")       | Kano          |
| [Katsina_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Katsina_State_Nigeria.png "fig:Katsina_State_Nigeria.png")          | [卡齐纳州](../Page/卡齐纳州.md "wikilink")     | Katsina State             | [卡齐纳](../Page/卡齐纳.md "wikilink")     | Katsina       |
| [Kebbi_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Kebbi_State_Nigeria.png "fig:Kebbi_State_Nigeria.png")                | [凯比州](../Page/凯比州.md "wikilink")       | Kebbi State               | [比尔宁凯比](../Page/比尔宁凯比.md "wikilink") | Birnin Kebbi  |
| [Kogi_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Kogi_State_Nigeria.png "fig:Kogi_State_Nigeria.png")                   | [科吉州](../Page/科吉州.md "wikilink")       | Kogi State                | [洛科贾](../Page/洛科贾.md "wikilink")     | Lokoja        |
| [Kwara_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Kwara_State_Nigeria.png "fig:Kwara_State_Nigeria.png")                | [夸拉州](../Page/夸拉州.md "wikilink")       | Kwara State               | [伊洛林](../Page/伊洛林.md "wikilink")     | Ilorin        |
| [Lagos_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Lagos_State_Nigeria.png "fig:Lagos_State_Nigeria.png")                | [拉各斯州](../Page/拉各斯州.md "wikilink")     | Lagos State               | [伊凯贾](../Page/伊凯贾.md "wikilink")     | Ikeja         |
| [Nassarawa_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Nassarawa_State_Nigeria.png "fig:Nassarawa_State_Nigeria.png")    | [纳萨拉瓦州](../Page/纳萨拉瓦州.md "wikilink")   | Nassawara State           | [拉菲亚](../Page/拉菲亚.md "wikilink")     | Lafia         |
| [Niger_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Niger_State_Nigeria.png "fig:Niger_State_Nigeria.png")                | [尼日尔州](../Page/尼日尔州.md "wikilink")     | Niger State               | [明纳](../Page/明纳.md "wikilink")       | Minna         |
| [Ogun_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Ogun_State_Nigeria.png "fig:Ogun_State_Nigeria.png")                   | [奥贡州](../Page/奥贡州.md "wikilink")       | Ogun State                | [阿贝奥库塔](../Page/阿贝奥库塔.md "wikilink") | Abeokuta      |
| [Ondo_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Ondo_State_Nigeria.png "fig:Ondo_State_Nigeria.png")                   | [翁多州](../Page/翁多州.md "wikilink")       | Ondo State                | [阿库雷](../Page/阿库雷.md "wikilink")     | Akure         |
| [Osun_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Osun_State_Nigeria.png "fig:Osun_State_Nigeria.png")                   | [奥孙州](../Page/奥孙州.md "wikilink")       | Osun State                | [奥绍博](../Page/奥绍博.md "wikilink")     | Oshogbo       |
| [Oyo_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Oyo_State_Nigeria.png "fig:Oyo_State_Nigeria.png")                      | [奥约州](../Page/奥约州.md "wikilink")       | Oyo State                 | [伊巴丹](../Page/伊巴丹.md "wikilink")     | Ibadan        |
| [Plateau_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Plateau_State_Nigeria.png "fig:Plateau_State_Nigeria.png")          | [高原州](../Page/高原州.md "wikilink")       | Plateau State             | [乔斯](../Page/乔斯.md "wikilink")       | Jos           |
| [Rivers_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Rivers_State_Nigeria.png "fig:Rivers_State_Nigeria.png")             | [河流州](../Page/河流州.md "wikilink")       | Rivers State              | [哈科特港](../Page/哈科特港.md "wikilink")   | Port Harcourt |
| [Sokoto_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Sokoto_State_Nigeria.png "fig:Sokoto_State_Nigeria.png")             | [索科托州](../Page/索科托州.md "wikilink")     | Sokoto State              | [索科托](../Page/索科托.md "wikilink")     | Sokoto        |
| [Taraba_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Taraba_State_Nigeria.png "fig:Taraba_State_Nigeria.png")             | [塔拉巴州](../Page/塔拉巴州.md "wikilink")     | Taraba State              | [扎里亚](../Page/扎里亚.md "wikilink")     | Jalingo       |
| [Yobe_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Yobe_State_Nigeria.png "fig:Yobe_State_Nigeria.png")                   | [约贝州](../Page/约贝州.md "wikilink")       | Yobe State                | [达马图鲁](../Page/达马图鲁.md "wikilink")   | Damaturu      |
| [Zamfara_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Zamfara_State_Nigeria.png "fig:Zamfara_State_Nigeria.png")          | [扎姆法拉州](../Page/扎姆法拉州.md "wikilink")   | Zamfara State             | [古绍](../Page/古绍.md "wikilink")       | Gusau         |
| [Abuja_State_Nigeria.png](https://zh.wikipedia.org/wiki/File:Abuja_State_Nigeria.png "fig:Abuja_State_Nigeria.png")                | [联邦首都特区](../Page/阿布贾.md "wikilink")    | Federal Capital Territory | [阿布贾](../Page/阿布贾.md "wikilink")     | Abuja         |

## 州人口

<table>
<thead>
<tr class="header">
<th></th>
<th><p>州</p></th>
<th><p>英文</p></th>
<th><p>首府</p></th>
<th><p>英文</p></th>
<th><p>州人口<br />
2006年</p></th>
<th><p>州面积<br />
km²</p></th>
<th><p>人口密度<br />
2006年<br />
/km²</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿比亚州.md" title="wikilink">阿比亚州</a></p></td>
<td><p>Abia</p></td>
<td><p><a href="../Page/乌穆阿希亚.md" title="wikilink">乌穆阿希亚</a></p></td>
<td><p>Umuahia</p></td>
<td><p>2,833,999</p></td>
<td><p>6,320</p></td>
<td><p>450</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阿达马瓦州.md" title="wikilink">阿达马瓦州</a></p></td>
<td><p>Adamawa</p></td>
<td><p><a href="../Page/约拉.md" title="wikilink">约拉</a></p></td>
<td><p>Yola, Nigeria</p></td>
<td><p>3,168,101</p></td>
<td><p>36,917</p></td>
<td><p>86</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿夸伊博姆州.md" title="wikilink">阿夸伊博姆州</a></p></td>
<td><p>Akwa-Ibom</p></td>
<td><p><a href="../Page/乌约.md" title="wikilink">乌约</a></p></td>
<td><p>Uyo</p></td>
<td><p>3,920,208</p></td>
<td><p>7,081</p></td>
<td><p>560</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阿南布拉州.md" title="wikilink">阿南布拉州</a></p></td>
<td><p>Anambra</p></td>
<td><p><a href="../Page/奥卡.md" title="wikilink">奥卡</a></p></td>
<td><p>Awka</p></td>
<td><p>4,182,032</p></td>
<td><p>4,844</p></td>
<td><p>871</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/包奇州.md" title="wikilink">包奇州</a></p></td>
<td><p>Bauchi</p></td>
<td><p><a href="../Page/包奇.md" title="wikilink">包奇</a></p></td>
<td><p>Bauchi</p></td>
<td><p>4,676,465</p></td>
<td><p>45,837</p></td>
<td><p>102</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/巴耶尔萨州.md" title="wikilink">巴耶尔萨州</a></p></td>
<td><p>Bayelsa</p></td>
<td><p><a href="../Page/耶诺亚.md" title="wikilink">耶诺亚</a></p></td>
<td><p>Yenagoa</p></td>
<td><p>1,703,358</p></td>
<td><p>10,773</p></td>
<td><p>158</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/贝努埃州.md" title="wikilink">贝努埃州</a></p></td>
<td><p>Benue</p></td>
<td><p><a href="../Page/马库尔迪.md" title="wikilink">马库尔迪</a></p></td>
<td><p>Makurdi</p></td>
<td><p>4,219,244</p></td>
<td><p>34,059</p></td>
<td><p>124</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/博尔诺州.md" title="wikilink">博尔诺州</a></p></td>
<td><p>Borno</p></td>
<td><p><a href="../Page/迈杜古里.md" title="wikilink">迈杜古里</a></p></td>
<td><p>Maiduguri</p></td>
<td><p>4,151,193</p></td>
<td><p>70,898</p></td>
<td><p>58</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/克里斯河州.md" title="wikilink">克里斯河州</a></p></td>
<td><p>Cross River</p></td>
<td><p><a href="../Page/卡拉巴尔.md" title="wikilink">卡拉巴尔</a></p></td>
<td><p>Calabar</p></td>
<td><p>2,888,966</p></td>
<td><p>20,156</p></td>
<td><p>144</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/三角州.md" title="wikilink">三角州</a></p></td>
<td><p>Delta</p></td>
<td><p><a href="../Page/阿萨巴.md" title="wikilink">阿萨巴</a></p></td>
<td><p>Asaba, Nigeria</p></td>
<td><p>4,098,391</p></td>
<td><p>17,698</p></td>
<td><p>228</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/埃邦伊州.md" title="wikilink">埃邦伊州</a></p></td>
<td><p>Ebonyi</p></td>
<td><p><a href="../Page/阿巴卡利基.md" title="wikilink">阿巴卡利基</a></p></td>
<td><p>Abakaliki</p></td>
<td><p>2,173,501</p></td>
<td><p>5,670</p></td>
<td><p>381</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/埃多州.md" title="wikilink">埃多州</a></p></td>
<td><p>Edo</p></td>
<td><p><a href="../Page/贝宁城.md" title="wikilink">贝宁城</a></p></td>
<td><p>Benin City</p></td>
<td><p>3,218,332</p></td>
<td><p>17,802</p></td>
<td><p>181</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/埃基蒂州.md" title="wikilink">埃基蒂州</a></p></td>
<td><p>Ekiti</p></td>
<td><p><a href="../Page/阿多.md" title="wikilink">阿多</a></p></td>
<td><p>Ado-Ekiti</p></td>
<td><p>2,384,212</p></td>
<td><p>6,353</p></td>
<td><p>372</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/埃努古州.md" title="wikilink">埃努古州</a></p></td>
<td><p>Enugu</p></td>
<td><p><a href="../Page/埃努古.md" title="wikilink">埃努古</a></p></td>
<td><p>Enugu</p></td>
<td><p>3,257,298</p></td>
<td><p>7,161</p></td>
<td><p>459</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿布贾.md" title="wikilink">联邦首都特区</a></p></td>
<td><p>FCT</p></td>
<td><p><a href="../Page/阿布贾.md" title="wikilink">阿布贾</a></p></td>
<td><p>Abuja</p></td>
<td><p>1,405,201</p></td>
<td><p>7,315</p></td>
<td><p>192</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/贡贝州.md" title="wikilink">贡贝州</a></p></td>
<td><p>Gombe</p></td>
<td><p><a href="../Page/贡贝.md" title="wikilink">贡贝</a></p></td>
<td><p>Gombe</p></td>
<td><p>2,353,879</p></td>
<td><p>18,768</p></td>
<td><p>125</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/伊莫州.md" title="wikilink">伊莫州</a></p></td>
<td><p>Imo</p></td>
<td><p><a href="../Page/奥韦里.md" title="wikilink">奥韦里</a></p></td>
<td><p>Owerri</p></td>
<td><p>3,934,899</p></td>
<td><p>5,100</p></td>
<td><p>772</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/吉加瓦州.md" title="wikilink">吉加瓦州</a></p></td>
<td><p>Jigawa</p></td>
<td><p><a href="../Page/杜切.md" title="wikilink">杜切</a></p></td>
<td><p>Dutse</p></td>
<td><p>4,348,649</p></td>
<td><p>23,154</p></td>
<td><p>188</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡杜纳州.md" title="wikilink">卡杜纳州</a></p></td>
<td><p>Kaduna</p></td>
<td><p><a href="../Page/卡杜纳.md" title="wikilink">卡杜纳</a></p></td>
<td><p>Kaduna</p></td>
<td><p>6,066,562</p></td>
<td><p>46,053</p></td>
<td><p>131</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/卡诺州.md" title="wikilink">卡诺州</a></p></td>
<td><p>Kano</p></td>
<td><p><a href="../Page/卡诺.md" title="wikilink">卡诺</a></p></td>
<td><p>Kano</p></td>
<td><p>9,383,682</p></td>
<td><p>20,131</p></td>
<td><p>467</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡齐纳州.md" title="wikilink">卡齐纳州</a></p></td>
<td><p>Katsina</p></td>
<td><p><a href="../Page/卡齐纳.md" title="wikilink">卡齐纳</a></p></td>
<td><p>Katsina</p></td>
<td><p>5,792,578</p></td>
<td><p>24,192</p></td>
<td><p>239</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/凯比州.md" title="wikilink">凯比州</a></p></td>
<td><p>Kebbi</p></td>
<td><p><a href="../Page/比尔宁凯比.md" title="wikilink">比尔宁凯比</a></p></td>
<td><p>Birnin Kebbi</p></td>
<td><p>3,238,628</p></td>
<td><p>36,800</p></td>
<td><p>88</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/科吉州.md" title="wikilink">科吉州</a></p></td>
<td><p>Kogi</p></td>
<td><p><a href="../Page/洛科贾.md" title="wikilink">洛科贾</a></p></td>
<td><p>Lokoja</p></td>
<td><p>3,278,487</p></td>
<td><p>29,833</p></td>
<td><p>110</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/夸拉州.md" title="wikilink">夸拉州</a></p></td>
<td><p>Kwara</p></td>
<td><p><a href="../Page/伊洛林.md" title="wikilink">伊洛林</a></p></td>
<td><p>Ilorin</p></td>
<td><p>2,371,089</p></td>
<td><p>36,825</p></td>
<td><p>64</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/拉各斯州.md" title="wikilink">拉各斯州</a></p></td>
<td><p>Lagos</p></td>
<td><p><a href="../Page/伊凱賈.md" title="wikilink">伊凱賈</a></p></td>
<td><p>Ikeja</p></td>
<td><p>9,013,534</p></td>
<td><p>3,345</p></td>
<td><p>2730</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/纳萨拉瓦州.md" title="wikilink">纳萨拉瓦州</a></p></td>
<td><p>Nassawara</p></td>
<td><p><a href="../Page/拉菲亚.md" title="wikilink">拉菲亚</a></p></td>
<td><p>Lafia</p></td>
<td><p>1,863,275</p></td>
<td><p>27,117</p></td>
<td><p>69</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/尼日尔州.md" title="wikilink">尼日尔州</a></p></td>
<td><p>Niger</p></td>
<td><p><a href="../Page/明纳.md" title="wikilink">明纳</a></p></td>
<td><p>Minna</p></td>
<td><p>3,950,249</p></td>
<td><p>76,363</p></td>
<td><p>52</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/奥贡州.md" title="wikilink">奥贡州</a></p></td>
<td><p>Ogun</p></td>
<td><p><a href="../Page/阿贝奥库塔.md" title="wikilink">阿贝奥库塔</a></p></td>
<td><p>Abeokuta</p></td>
<td><p>3,728,098</p></td>
<td><p>16,762</p></td>
<td><p>223</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/翁多州.md" title="wikilink">翁多州</a></p></td>
<td><p>Ondo</p></td>
<td><p><a href="../Page/阿库雷.md" title="wikilink">阿库雷</a></p></td>
<td><p>Akure</p></td>
<td><p>3,441,024</p></td>
<td><p>15,500</p></td>
<td><p>222</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/奥孙州.md" title="wikilink">奥孙州</a></p></td>
<td><p>Osun</p></td>
<td><p><a href="../Page/奥绍博.md" title="wikilink">奥绍博</a></p></td>
<td><p>Oshogbo</p></td>
<td><p>3,423,535</p></td>
<td><p>9,251</p></td>
<td><p>372</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/奥约州.md" title="wikilink">奥约州</a></p></td>
<td><p>Oyo</p></td>
<td><p><a href="../Page/伊巴丹.md" title="wikilink">伊巴丹</a></p></td>
<td><p>Ibadan</p></td>
<td><p>5,591,589</p></td>
<td><p>28,454</p></td>
<td><p>197</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/高原州.md" title="wikilink">高原州</a></p></td>
<td><p>Plateau</p></td>
<td><p><a href="../Page/乔斯.md" title="wikilink">乔斯</a></p></td>
<td><p>Jos</p></td>
<td><p>3,178,712</p></td>
<td><p>30,913</p></td>
<td><p>103</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/河流州.md" title="wikilink">河流州</a></p></td>
<td><p>Rivers</p></td>
<td><p><a href="../Page/哈科特港.md" title="wikilink">哈科特港</a></p></td>
<td><p>Port Harcourt</p></td>
<td><p>5,185,400</p></td>
<td><p>11,077</p></td>
<td><p>471</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/索科托州.md" title="wikilink">索科托州</a></p></td>
<td><p>Sokoto</p></td>
<td><p><a href="../Page/索科托.md" title="wikilink">索科托</a></p></td>
<td><p>Sokoto</p></td>
<td><p>3,696,999</p></td>
<td><p>25,973</p></td>
<td><p>143</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/塔拉巴州.md" title="wikilink">塔拉巴州</a></p></td>
<td><p>Taraba</p></td>
<td><p><a href="../Page/扎里亚.md" title="wikilink">扎里亚</a></p></td>
<td><p>Jalingo</p></td>
<td><p>2,300,736</p></td>
<td><p>54,473</p></td>
<td><p>42</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/约贝州.md" title="wikilink">约贝州</a></p></td>
<td><p>Yobe</p></td>
<td><p><a href="../Page/达马图鲁.md" title="wikilink">达马图鲁</a></p></td>
<td><p>Damaturu</p></td>
<td><p>2,321,591</p></td>
<td><p>45,502</p></td>
<td><p>51</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/扎姆法拉州.md" title="wikilink">扎姆法拉州</a></p></td>
<td><p>Zamfara</p></td>
<td><p><a href="../Page/古绍.md" title="wikilink">古绍</a></p></td>
<td><p>Gusau</p></td>
<td><p>3,259,846</p></td>
<td><p>39,762</p></td>
<td><p>82</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [1](https://web.archive.org/web/20080827183907/http://www.population.gov.ng/pop_figure.pdf)

## 参见

  - [:en:List of Nigerian states by
    area](../Page/:en:List_of_Nigerian_states_by_area.md "wikilink")

  - [:en:List of Nigerian states by
    population](../Page/:en:List_of_Nigerian_states_by_population.md "wikilink")

{{-}}

[尼日利亚行政区划](../Category/尼日利亚行政区划.md "wikilink")