[ECMO_in_H1N1_patient_in_Santa_Cruz_Hospital_-_Lisbon.jpg](https://zh.wikipedia.org/wiki/File:ECMO_in_H1N1_patient_in_Santa_Cruz_Hospital_-_Lisbon.jpg "fig:ECMO_in_H1N1_patient_in_Santa_Cruz_Hospital_-_Lisbon.jpg")

**體外膜氧合**（，縮寫****，[台灣地區音譯俗稱](../Page/台灣地區.md "wikilink")「**[葉克膜](../Page/葉克膜.md "wikilink")**」，[香港稱](../Page/香港.md "wikilink")「人工肺」）是一種醫療急救設備，用以暫時協助大部份醫療方法皆無效的重度[心肺衰竭患者進行體外的呼吸與循環](../Page/心肺衰竭.md "wikilink")，ECMO最早由美國[密西根大學醫學院成功利用](../Page/密西根大學.md "wikilink")。ECMO除了能暫時替代患者的心肺功能，減輕患者心肺負擔之外，也能為醫療人員爭取更多救治時間。ECMO用於孩童患者因容易產生[併發症](../Page/併發症.md "wikilink")，所以可用輔助期一般僅約一週至十數日而已，而用於成人患者的輔助期則較長。ECMO使用指導及規範由相關領域專家成立的來規範及訂立醫療指導原則。

## 歷史

ECMO 最早於1950年代由by John Gibbon發明，及Lillehei繼續開發。
1965年，第一次使用於新生兒上。\[1\]\[2\]1972年，[美國](../Page/美國.md "wikilink")[密西根大學外科醫生羅伯特](../Page/密西根大學.md "wikilink")·巴列特（Robert
H.Bartlett）首次成功應用在[急性呼吸窘迫症患者的治療](../Page/急性呼吸窘迫症.md "wikilink")。

## 相關議題

隨著醫學的進步，許多因使用葉克膜而引發的併發症也更進一步得到有效克服，技術更加成熟，應用範圍也擴及重度昏迷的急難傷病，而葉克膜團隊也會面臨[無效醫療的倫理問題](../Page/無效醫療.md "wikilink")。

## 台灣地區

台灣地區針對葉克膜的技術引進，主要由時任[臺大醫院外科醫生](../Page/臺大醫院.md "wikilink")[柯文哲引入臺灣](../Page/柯文哲.md "wikilink")。
位於台北市的[台大醫院曾經有案例在](../Page/台大醫院.md "wikilink")2008年1月30日創下117天使用葉克膜再移除，成功復原的紀錄\[3\]。

## 構造

[Ecmo_schema-1-.jpg](https://zh.wikipedia.org/wiki/File:Ecmo_schema-1-.jpg "fig:Ecmo_schema-1-.jpg")
葉克膜由

  - 血液-{zh-hant:幫浦; zh-hans:泵}-
  - 氧合器
  - 氣體混合器
  - 加熱器
  - 各種動靜脈導管與監視器

等部件所構成，其中血液-{zh-hant:幫浦; zh-hans:泵}-和氧合器為葉克膜核心部件，血液-{zh-hant:幫浦;
zh-hans:泵}-扮演代替患者[心臟](../Page/心臟.md "wikilink")，氧合器則扮演代替[肺臟的功能](../Page/肺臟.md "wikilink")。

## 類型

葉克膜的種類可分為[靜脈](../Page/靜脈.md "wikilink")-靜脈（veno-venous，縮寫**VV**）和靜脈-[動脈](../Page/動脈.md "wikilink")（veno-arterial，縮寫**VA**）兩種。

  - **VV型**：代替肺臟呼吸機能，只用於肺部疾病。
  - **VA型**：除代替肺臟呼吸機能之外，也能代替心臟的血液循環機能。

## 併發症

使用葉克膜可能會有[併發症發生](../Page/併發症.md "wikilink")，常見的有：

  - [血栓](../Page/血栓.md "wikilink")
  - 出血
  - [溶血](../Page/溶血.md "wikilink")
  - 末端肢體缺血
  - [感染](../Page/感染.md "wikilink")
  - [腎衰竭](../Page/腎衰竭.md "wikilink")

依照病人病情來調整葉克膜的各種設定、及處理各種併發症，關係到病人的存活率，病人需要安裝的時間越長、越考驗相關專業人員的能力及經驗。

## 参考文献

<div class="references-small">

<references />

</div>

## 相關條目

  - [密西根大學](../Page/密西根大學.md "wikilink")

  -
[Category:重症医学](../Category/重症医学.md "wikilink")
[Category:醫療設備](../Category/醫療設備.md "wikilink")
[Category:膜技术](../Category/膜技术.md "wikilink")

1.
2.
3.