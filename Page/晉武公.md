**晉武公**（），**[姬](../Page/姬.md "wikilink")**姓，名**稱**，谥为“武”，[曲沃桓叔成師之孫](../Page/曲沃桓叔.md "wikilink")、[曲沃莊伯鱓之子](../Page/曲沃莊伯.md "wikilink")。前716年繼承父位，成為[曲沃的](../Page/曲沃.md "wikilink")[君主](../Page/君主.md "wikilink")，在吞併[晉國前稱](../Page/晉國.md "wikilink")**曲沃君**、**曲沃伯**。

晉哀侯八年（前710年），曲沃與陘廷聯兵攻晉，與晉國對抗。更在哀侯九年（前709年）春，曲沃攻伐晉[京師](../Page/京師.md "wikilink")[翼城](../Page/翼城.md "wikilink")，久留在陘廷，以[公子萬駕戎車](../Page/韓萬.md "wikilink")、梁弘為車右，追逐哀侯到[汾河](../Page/汾河.md "wikilink")，大敗晉軍。是夜[俘虜了](../Page/俘虜.md "wikilink")[晉哀侯](../Page/晉哀侯.md "wikilink")，\[1\]後來又指使公子萬殺了哀侯。

[晉小子四年](../Page/晉小子侯.md "wikilink")，曲沃誘殺了[晉小子侯](../Page/晉小子侯.md "wikilink")，由於[周桓王使](../Page/周桓王.md "wikilink")[虢仲出兵討伐](../Page/虢仲.md "wikilink")[曲沃](../Page/曲沃.md "wikilink")。武公回曲沃，[周天子遂立晉哀侯之弟](../Page/周天子.md "wikilink")[晉侯緡為晉的國君](../Page/晉侯緡.md "wikilink")，曲沃未能吞併晉國。

晉侯緡二十八年（前679年），曲沃出兵終於滅了晉，是為[曲沃克晉](../Page/曲沃克晉.md "wikilink")。[魯莊公十六年](../Page/魯莊公.md "wikilink")（前678年），武公把掠奪來的晉國寶器，[賄賂](../Page/賄賂.md "wikilink")[周僖王](../Page/周僖王.md "wikilink")，天子喜，封曲沃公為晉君，列為[諸侯](../Page/諸侯.md "wikilink")。\[2\]武公統治晉國後，封[公子萬為](../Page/韓萬.md "wikilink")[韓](../Page/韓.md "wikilink")[大夫](../Page/大夫.md "wikilink")，是為韓武子，為[戰國時代](../Page/戰國時代.md "wikilink")[韓國之祖](../Page/戰國韓國.md "wikilink")。

受周僖王[敕封為晉侯後不久](../Page/敕.md "wikilink")，晉武公便出兵攻打東周[朝廷](../Page/朝廷.md "wikilink")，殺害周夷邑大夫詭諸，周朝執政大臣周公忌父逃奔[虢國](../Page/虢國.md "wikilink")。

## 子女

  - [晉獻公](../Page/晉獻公.md "wikilink")
  - 公子[伯侨](../Page/伯侨.md "wikilink")，中国春秋时期晋国的公子，他是晋武公的儿子。姬姓，羊舌氏。

## 参见

  - [曲沃克晉](../Page/曲沃克晉.md "wikilink")

## 參考資料

  - 《[史記](../Page/史記.md "wikilink")》晉世家第九
  - 《[左傳](../Page/左傳.md "wikilink")·桓公二年》
  - 《左傳·桓公三年》

### 腳注

[Category:晋国君主](../Category/晋国君主.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")

1.  《春秋左传注·桓公三年》
2.  《左傳·莊公十六年》：「王使虢公命曲沃伯，以一軍為晉侯。」