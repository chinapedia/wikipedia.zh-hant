**安德烈斯·帕斯特拉纳·阿朗戈**（[西班牙语](../Page/西班牙语.md "wikilink")：****，）[哥倫比亞](../Page/哥倫比亞.md "wikilink")[右派政治家](../Page/右派.md "wikilink")，前[哥倫比亞总统](../Page/哥倫比亞.md "wikilink")。

## 早年

帕斯特拉纳是前总统[米萨埃尔·帕斯特拉纳·博雷罗](../Page/米萨埃尔·帕斯特拉纳·博雷罗.md "wikilink")（1970年～1974年）之子。就读[波哥大罗萨里奥圣母学院](../Page/波哥大.md "wikilink")，后赴美国[哈佛大学国际问题中心进修并获硕士学位](../Page/哈佛大学.md "wikilink")。回国后创建一本叫Guión的杂志和Noticiero
TV Hoy电视台任职。

## 1998-2002年总统

1982年他开始正式政治生涯，他被选为波哥大市政委员至1986年，两次任市政委员会主席。

1988年1月8日他在安提奥基亚被绑架，绑匪向哥倫比亞政府施压阻止毒枭[帕布洛·埃斯科瓦尔](../Page/帕布洛·埃斯科瓦尔.md "wikilink")(Pablo
Escobar)被引渡到美国。一周后他被警察找到，3月他当选波哥大市长，任期至1990年。

1994年競选总统失败，1998年5月再次参加大选，6月成功当选，結束[哥倫比亞自由黨](../Page/哥倫比亞自由黨.md "wikilink")12年的管治，8月7日就任总统。

[Category:哥伦比亚总统](../Category/哥伦比亚总统.md "wikilink")
[Category:总统儿子](../Category/总统儿子.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:波哥大人](../Category/波哥大人.md "wikilink")