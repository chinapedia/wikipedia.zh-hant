**康海**（），[字](../Page/表字.md "wikilink")**德涵**，[號](../Page/號.md "wikilink")**對山**、**沜東漁父**，[中國](../Page/中國.md "wikilink")[陝西](../Page/陝西.md "wikilink")[武功县人](../Page/武功县.md "wikilink")，明朝狀元、文学家。

## 生平

成化十一年（1475年）六月二十日出生，自幼慧敏，以[冯寅为师](../Page/冯寅.md "wikilink")，弘治七年（1494年）入县学。弘治十一年（1498年）陝西鄉試第七名。弘治十五年（1502年）壬戌科中式一甲第一名進士（[狀元](../Page/狀元.md "wikilink")）\[1\]\[2\]\[3\]，歷任[翰林院修撰](../Page/翰林院.md "wikilink")、經筵講官。宦官[劉瑾亂政](../Page/劉瑾.md "wikilink")，愛慕同鄉康海之才，意欲網羅，康海不肯。[李夢陽下獄](../Page/李夢陽.md "wikilink")，寫紙條向康海求救。康海於是拜謁劉瑾，劉瑾大喜，為倒屣迎。康海藉機為李夢陽求情，夢陽得釋。正德五年（1510年）八月，劉瑾事敗，康海反被李梦阳歸為[劉瑾黨](../Page/劉瑾.md "wikilink")，卸職。此後，康海寓居[扬州](../Page/扬州.md "wikilink")，置女乐，自操[琵琶创家乐班子](../Page/琵琶.md "wikilink")，人称“康家班社”，“主盟艺苑，垂四十年”，為[秦腔之霸主](../Page/秦腔.md "wikilink")。康山以此得名\[4\]。因谴责李梦阳写成杂剧《[中山狼](../Page/中山狼_\(康海\).md "wikilink")》。

## 文學

康海是明朝知名文學家，又曾编纂《武功县志》，史載“乡国之史，莫良于此。”因為文學理念相近，加上同時尊崇復古文風，與[李夢陽等人並列為](../Page/李夢陽.md "wikilink")[前七子之一](../Page/前七子.md "wikilink")，又与[李梦阳](../Page/李梦阳.md "wikilink")、[何景明](../Page/何景明.md "wikilink")、[徐祯卿](../Page/徐祯卿.md "wikilink")、[边贡](../Page/边贡.md "wikilink")、[朱应登](../Page/朱应登.md "wikilink")、[顾璘](../Page/顾璘.md "wikilink")、[陈沂](../Page/陈沂.md "wikilink")、[郑善夫](../Page/郑善夫.md "wikilink")、[王九思等号称](../Page/王九思.md "wikilink")“十才子”。

## 佚事

《四友齋叢說》載康海以状元登第，在翰林院中頗負盛名。因[丁憂回鄉](../Page/丁憂.md "wikilink")，按慣例應請內閣諸公作墓志銘。時[李夢陽秉海內文柄](../Page/李夢陽.md "wikilink")，康海卻僅僅請其書寫墓碑，由同官[王九思](../Page/王九思.md "wikilink")、[段炅作墓誌與傳](../Page/段炅.md "wikilink")。李夢陽大為不滿。此後劉瑾逆案發，康海因此遂落籍\[5\]。

## 家族

曾祖父[康爵](../Page/康爵.md "wikilink")，曾任南京太常寺少卿；祖父[康健](../Page/康健.md "wikilink")，曾任通政司知事；父親[康鏞](../Page/康鏞.md "wikilink")，曾任平阳府知事。母張氏\[6\]。

## 参考文献

  - 《翰林院修撰康公海行狀》，[張治道](../Page/張治道.md "wikilink")

{{-}}

[Category:弘治十一年戊午科舉人](../Category/弘治十一年戊午科舉人.md "wikilink")
[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林院修撰](../Category/明朝翰林院修撰.md "wikilink")
[Category:明朝作家](../Category/明朝作家.md "wikilink")
[Category:明朝音乐家](../Category/明朝音乐家.md "wikilink")
[Category:琵琶演奏家](../Category/琵琶演奏家.md "wikilink")
[Category:武功人](../Category/武功人.md "wikilink")
[H海](../Category/康姓.md "wikilink")

1.
2.
3.
4.  嘉庆《重修扬州府志·山川·江都县》记载：“康山……以武功失职后来此地。”
5.  《四友齋叢說·[卷十五·史十一](../Page/s:四友齋叢說/卷15.md "wikilink")》：康對山以狀元登第，在館中聲望籍甚。臺省諸公得其謦咳以為榮。不久以憂去，大率翰林官丁憂。其墓文皆請之內閣諸公，此舊例也。對山聞喪即行，求李空同作墓碑，王渼陂段德光作墓誌與傳。時李西涯方秉海內文柄，大不平之。值逆瑾事起，對山遂落籍。
6.