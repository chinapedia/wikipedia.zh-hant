**鄭兆行**（[阿美語](../Page/阿美語.md "wikilink")：，[音譯](../Page/音譯.md "wikilink")：**阿浪·卡洛**，），[臺灣](../Page/臺灣.md "wikilink")[花蓮縣人](../Page/花蓮縣.md "wikilink")，原名**鄭兆岳**，[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")，曾效力於[中華職棒](../Page/中華職棒.md "wikilink")[興農牛和](../Page/興農牛.md "wikilink")[義大犀牛隊](../Page/義大犀牛.md "wikilink")，守備位置為[游擊手](../Page/游擊手.md "wikilink")[三壘手](../Page/三壘手.md "wikilink")。

## 經歷

  - [花蓮縣](../Page/花蓮縣.md "wikilink")[北富國小少棒隊](../Page/北富國小.md "wikilink")(現為太巴塱國小)
  - [台北市](../Page/台北市.md "wikilink")[華興中學青少棒隊](../Page/華興中學.md "wikilink")
  - [台北市](../Page/台北市.md "wikilink")[華興中學青棒隊](../Page/華興中學.md "wikilink")
  - [文化大學棒球隊](../Page/文化大學.md "wikilink")（美孚巨人）
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛](../Page/興農牛.md "wikilink")（2000年-2012年12月16日）
  - 台灣職棒球員工會第二屆理事（2008年12月6日-2010年2月6日）
  - 中華民國台灣原住民棒球運動發展協會理事（2009年11月20日-）
  - 台灣職棒球員工會補選監事（2010年2月6日-2011年10月30日）
  - 台灣職棒球員工會常務監事（2011年10月30日-2015年3月6日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛兼任守備教練](../Page/興農牛.md "wikilink")（2011年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[義大犀牛](../Page/義大犀牛.md "wikilink")（[2012年](../Page/2012年.md "wikilink")12月17日-2015年12月1日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[義大犀牛守備教練](../Page/義大犀牛.md "wikilink")（2015年12月2日-2016年10月31日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[富邦悍將守備教練](../Page/富邦悍將.md "wikilink")（2016年11月1日-）

## 職棒生涯成績

**粗體字**為[全聯盟當年最佳或最高成績](../Page/中華職棒.md "wikilink")

| 年度    | 球隊                                 | 出賽   | 打數   | 安打   | 全壘打 | 打點  | 盜壘     | 四死  | 三振  | 壘打數  | 雙殺打 | 打擊率   |
| ----- | ---------------------------------- | ---- | ---- | ---- | --- | --- | ------ | --- | --- | ---- | --- | ----- |
| 2000年 | [興農牛](../Page/興農牛.md "wikilink")   | 88   | 320  | 78   | 2   | 30  | 8      | 28  | 31  | 106  | 2   | 0.244 |
| 2001年 | [興農牛](../Page/興農牛.md "wikilink")   | 81   | 300  | 74   | 3   | 26  | 7      | 28  | 35  | 105  | 4   | 0.247 |
| 2002年 | [興農牛](../Page/興農牛.md "wikilink")   | 81   | 291  | 91   | 6   | 53  | 9      | 33  | 46  | 126  | 3   | 0.313 |
| 2003年 | [興農牛](../Page/興農牛.md "wikilink")   | 96   | 371  | 105  | 12  | 58  | 16     | 33  | 39  | 161  | 7   | 0.283 |
| 2004年 | [興農牛](../Page/興農牛.md "wikilink")   | 99   | 384  | 107  | 7   | 54  | **31** | 27  | 56  | 156  | 2   | 0.279 |
| 2005年 | [興農牛](../Page/興農牛.md "wikilink")   | 66   | 196  | 49   | 4   | 20  | 5      | 17  | 38  | 74   | 4   | 0.250 |
| 2006年 | [興農牛](../Page/興農牛.md "wikilink")   | 34   | 75   | 18   | 1   | 7   | 2      | 5   | 7   | 22   | 1   | 0.240 |
| 2007年 | [興農牛](../Page/興農牛.md "wikilink")   | 94   | 366  | 110  | 9   | 44  | 3      | 37  | 54  | 158  | 9   | 0.301 |
| 2008年 | [興農牛](../Page/興農牛.md "wikilink")   | 69   | 238  | 66   | 1   | 21  | 2      | 19  | 37  | 79   | 5   | 0.277 |
| 2009年 | [興農牛](../Page/興農牛.md "wikilink")   | 110  | 391  | 115  | 3   | 59  | 11     | 26  | 32  | 148  | 13  | 0.294 |
| 2010年 | [興農牛](../Page/興農牛.md "wikilink")   | 108  | 365  | 110  | 0   | 35  | 8      | 30  | 41  | 132  | 10  | 0.301 |
| 2011年 | [興農牛](../Page/興農牛.md "wikilink")   | 72   | 191  | 46   | 0   | 18  | 3      | 14  | 24  | 55   | 6   | 0.241 |
| 2012年 | [興農牛](../Page/興農牛.md "wikilink")   | 74   | 240  | 64   | 1   | 24  | 0      | 15  | 27  | 80   | 9   | 0.267 |
| 2013年 | [義大犀牛](../Page/義大犀牛.md "wikilink") | 6    | 20   | 5    | 0   | 2   | 1      | 2   | 0   | 6    | 1   | 0.250 |
| 合計    | 14年                                | 1074 | 3748 | 1038 | 49  | 451 | 106    | 314 | 467 | 1408 | 76  | 0.277 |

## 特殊事蹟

  - 2004年於[雅典奧運共發生](../Page/雅典奧運.md "wikilink")5次失誤，全隊最多；[打擊率一成](../Page/打擊率.md "wikilink")，全隊[先發選手中最低](../Page/先發.md "wikilink")。
  - 2007年4月於台中體院棒球場對決[統一獅比賽](../Page/統一獅.md "wikilink")，三局下從[投手](../Page/投手.md "wikilink")[張志強手中擊出強襲球擊中](../Page/張志強.md "wikilink")[張志強腿上](../Page/張志強.md "wikilink")，導致其退場。
  - 2008年10月10日球季最後一戰因[阿讓的部落格上提到](../Page/阿讓.md "wikilink")，[黃忠義在最後球賽後就要高掛球鞋的消息被網友誤以為是要引退的烏龍](../Page/黃忠義.md "wikilink")，20日證實此事情為真實。
  - 2009年3月29日於[澄清湖棒球場完成生涯第](../Page/澄清湖棒球場.md "wikilink")700安紀錄。
  - 2009年9月17日於台中體院棒球場對戰[統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink")，生涯第800場出賽。
  - 2009年9月24日於[澄清湖球場完成生涯第](../Page/澄清湖球場.md "wikilink")800安紀錄。
  - 2010年7月30日於[新竹棒球場對戰](../Page/新竹棒球場.md "wikilink")[兄弟象完成個人生涯](../Page/兄弟象.md "wikilink")[百盜](../Page/百盜.md "wikilink")。
  - 2010年8月3日於[斗六棒球場對戰](../Page/斗六棒球場.md "wikilink")[La
    New熊](../Page/La_New熊.md "wikilink")，九局下從[救援投手](../Page/救援投手.md "wikilink")[耿伯軒手中擊出](../Page/耿伯軒.md "wikilink")[再見安打](../Page/再見安打.md "wikilink")。
  - 2010年8月13日於[屏東棒球場對戰](../Page/屏東棒球場.md "wikilink")[La
    New熊](../Page/La_New熊.md "wikilink")，於4局上完成個人生涯第400分[打點](../Page/打點.md "wikilink")。
  - 2010年8月15日於[澄清湖棒球場對戰](../Page/澄清湖棒球場.md "wikilink")[La
    New熊](../Page/La_New熊.md "wikilink")，生涯第900場出賽。
  - 2010年8月20日對戰[La
    New熊](../Page/La_New熊.md "wikilink")，再度從[耿伯軒手中擊出](../Page/耿伯軒.md "wikilink")[再見安打](../Page/再見安打.md "wikilink")，本季第二支[再見安打](../Page/再見安打.md "wikilink")，兩支都從[耿伯軒手中擊出](../Page/耿伯軒.md "wikilink")。
  - 2010年8月22日於[羅東棒球場對戰](../Page/羅東棒球場.md "wikilink")[La
    new熊](../Page/La_new熊.md "wikilink")，敲出生涯第900支[安打](../Page/安打.md "wikilink")。
  - 2010年12月27日起兼任[興農牛隊](../Page/興農牛隊.md "wikilink")[內野守備教練職務](../Page/教練.md "wikilink")。
  - 2012年5月11日於[斗六棒球場對戰](../Page/斗六棒球場.md "wikilink")[統一7-ELEVEn獅](../Page/統一7-ELEVEn獅.md "wikilink")，擔任先發三壘手，為職棒生涯第1000場出賽，為聯盟第11為達成此紀錄的球員。
  - 2012年7月29日於[新莊棒球場對戰](../Page/新莊棒球場.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")，於延長賽十一局上半從[湯瑪仕手中擊出生涯千安](../Page/湯瑪仕B.T.md "wikilink")，為聯盟第11人達成此紀錄的球員。

## 外部連結

[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:台灣原住民運動員](../Category/台灣原住民運動員.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:興農牛隊球員](../Category/興農牛隊球員.md "wikilink")
[Category:中華職棒千安選手](../Category/中華職棒千安選手.md "wikilink")
[Category:中華職棒百盜選手](../Category/中華職棒百盜選手.md "wikilink")
[Category:中華職棒盜壘王](../Category/中華職棒盜壘王.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")
[Category:阿美族人](../Category/阿美族人.md "wikilink")
[Category:花蓮人](../Category/花蓮人.md "wikilink")
[Category:光復人](../Category/光復人.md "wikilink")
[Category:鄭姓](../Category/鄭姓.md "wikilink")