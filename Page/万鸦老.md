**萬鴉老**（[印度尼西亚语](../Page/印度尼西亚语.md "wikilink")：****）或譯萬雅佬，位于[印度尼西亚](../Page/印度尼西亚.md "wikilink")[苏拉威西岛北端的海灣](../Page/苏拉威西岛.md "wikilink")，是[北苏拉威西省的首府](../Page/北苏拉威西省.md "wikilink")。1658年[荷蘭東印度公司於此建立軍事基地](../Page/荷蘭東印度公司.md "wikilink")，因此本地居民以信仰[基督教為多](../Page/基督教.md "wikilink")。万雅佬發展至今，乃蘇拉威西的第二大城市，人口近42万（2005年），共有9個市轄區，為群山所環繞，素以海岸美景著稱。\[1\]

## 交通

[薩姆·拉圖蘭吉機場](../Page/薩姆·拉圖蘭吉機場.md "wikilink")

## 友好城市

  - [亚罗士打](../Page/亚罗士打.md "wikilink")

  - [关丹](../Page/关丹.md "wikilink")

  - [科羅爾](../Page/科羅爾.md "wikilink")

  - [宿霧省](../Page/宿霧省.md "wikilink")

  - [达沃市](../Page/达沃市.md "wikilink")

  - [安汶](../Page/安汶.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [萬鴉老官方網站](http://www.manadokota.go.id)
  - [myManado\!](http://manado.travel.takol.tw/)

[Category:印尼城市](../Category/印尼城市.md "wikilink")

1.  丁楓峻(2010), 東南亞自助潛水趣. 華都文化. ISBN 978-986-6860-93-5