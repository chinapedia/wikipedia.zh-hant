**承玄**（428年六月-431年十二月），是[十六国時期](../Page/十六国.md "wikilink")[北涼君主](../Page/北涼.md "wikilink")，太祖武宣王[沮渠蒙逊的](../Page/沮渠蒙逊.md "wikilink")[年號](../Page/年號.md "wikilink")，共計3年餘。[甘肅出土的造經](../Page/甘肅.md "wikilink")[塔殘石有與史書記載不同的](../Page/塔.md "wikilink")“**承玄**二年嵗在戊辰”字樣。\[1\]\[2\]。

## 纪年

| 承玄                               | 元年                             | 二年                             | 三年                             | 四年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 428年                           | 429年                           | 430年                           | 431年                           |
| [干支](../Page/干支纪年.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [元嘉](../Page/元嘉_\(南朝宋文帝\).md "wikilink")（424年八月—453年十二月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋文帝刘义隆的年号](../Page/宋文帝.md "wikilink")
      - [勝光](../Page/勝光.md "wikilink")（428年二月-431年六月）：[夏国政权](../Page/夏国.md "wikilink")[赫连定年号](../Page/赫连定.md "wikilink")
      - [永弘](../Page/永弘.md "wikilink")（428年五月—431年正月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏暮末年号](../Page/乞伏暮末.md "wikilink")
      - [太平](../Page/太平_\(冯跋\).md "wikilink")（409年十月-430年）：[北燕政权](../Page/北燕.md "wikilink")[冯跋年号](../Page/冯跋.md "wikilink")
      - [太興](../Page/太兴_\(冯弘\).md "wikilink")（431年正月-436年五月）：[北燕政权](../Page/北燕.md "wikilink")[冯弘年号](../Page/冯弘.md "wikilink")
      - [神䴥](../Page/神䴥.md "wikilink")（428年二月-431年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")

## 参考文献

<references/>

[Category:北凉年号](../Category/北凉年号.md "wikilink")
[C承玄](../Category/有爭議的年號.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129
2.