<span style="font-size:smaller;">\*<span style="font-size:smaller;">此排名以参赛国家或地区获得的金牌数量为排名顺序依据，金牌之后依次以银牌，铜牌的获得数为排名顺序依据。如有多于一个以上的国家或地区所获同类奖牌数目相同，则依照这些国家或地区的英文名称字母顺序在同一名次内进行排名。</span></span>

[Category:夏季奥林匹克运动会奖牌榜](../Category/夏季奥林匹克运动会奖牌榜.md "wikilink")
[Category:1972年夏季奥林匹克运动会](../Category/1972年夏季奥林匹克运动会.md "wikilink")