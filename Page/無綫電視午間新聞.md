《**午間新聞**》（），[香港](../Page/香港.md "wikilink")[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")[新聞及資訊部製作的新聞節目](../Page/無綫新聞.md "wikilink")。每天[香港時間中午於旗下頻道](../Page/香港時間.md "wikilink")[翡翠台](../Page/翡翠台.md "wikilink")、[無綫新聞台播出](../Page/無綫新聞台.md "wikilink")。
香港其他電視台同類節目有[有線電視的](../Page/有線電視.md "wikilink")《[有線新聞](../Page/有線新聞.md "wikilink")》（中午時段）及[now寬頻電視的](../Page/now寬頻電視.md "wikilink")《[Now午間新聞](../Page/Now午間新聞.md "wikilink")》。

## 播放時間

### [翡翠台](../Page/翡翠台.md "wikilink")

《午間新聞》在[香港時間星期一至星期五](../Page/香港時間.md "wikilink")13:00-13:20及星期六、日13:00-13:10於[翡翠台播出](../Page/翡翠台.md "wikilink")。

遇有大型特別事件時，播放時間會延長至半小時及分為每十五分鐘一節播映，而香港回歸後首日《午間新聞》更以史無前例的方式播映一小時及分為四節播映。

[2008年](../Page/2008年.md "wikilink")[1月1日起](../Page/1月1日.md "wikilink")，由於[高清翡翠台啟播](../Page/高清翡翠台.md "wikilink")，《午間新聞》在[翡翠台及](../Page/翡翠台.md "wikilink")[高清翡翠台聯播](../Page/高清翡翠台.md "wikilink")。

[2008年](../Page/2008年.md "wikilink")[7月13日起](../Page/7月13日.md "wikilink")，《午間新聞》節目改用16:9技術製作。

[2011年](../Page/2011年.md "wikilink")[3月7日起](../Page/3月7日.md "wikilink")，為配合[香港交易所提早港股開市時間至](../Page/香港交易所.md "wikilink")13:00，港股交易日[高清翡翠台提供](../Page/高清翡翠台.md "wikilink")[新聞跑馬燈](../Page/新聞跑馬燈.md "wikilink")，顯示港股及[恒生指數最新報價](../Page/恒生指數.md "wikilink")。但[2013年起取消有關安排](../Page/2013年.md "wikilink")。

[2015年](../Page/2015年.md "wikilink")[1月4日及之前](../Page/1月4日.md "wikilink")[互動新聞台及](../Page/互動新聞台.md "wikilink")[TVB新聞台會在非港股交易日及星期六](../Page/TVB新聞台.md "wikilink")、日聯播該節《午間新聞》，但從[2015年](../Page/2015年.md "wikilink")[1月10日起則停止聯播](../Page/1月10日.md "wikilink")，改為13:00-13:15播出《[新聞報道](../Page/無綫電視新聞報道.md "wikilink")》。

[2016年](../Page/2016年.md "wikilink")[2月22日起](../Page/2月22日.md "wikilink")，[高清翡翠台改名為](../Page/高清翡翠台.md "wikilink")[J5且與](../Page/J5.md "wikilink")[翡翠台完全分途廣播](../Page/翡翠台.md "wikilink")，翡翠台《午間新聞》取消聯播安排。

[2017年](../Page/2017年.md "wikilink")[10月16日起](../Page/10月16日.md "wikilink")，因應[翡翠台取消播映港台節目](../Page/翡翠台.md "wikilink")《[左右紅藍綠](../Page/左右紅藍綠.md "wikilink")》，星期一至五的《午間新聞》延長至13:20。

現時，每日的《午間新聞》會使用正常廠景，新聞結束會以高空鏡頭作結。

[2018年](../Page/2018年.md "wikilink")[6月1日起](../Page/6月1日.md "wikilink")，無綫新聞網站「專題節目」分類及myTV
Super提供《午間新聞》重溫。

### [無綫新聞台](../Page/無綫新聞台.md "wikilink")

正常情況下，[無綫新聞台](../Page/無綫新聞台.md "wikilink")《午間新聞》在[香港時間星期一至五](../Page/香港時間.md "wikilink")12:00-12:55及星期六、日12:00-13:00播出。2019年1月15日，[TVB新聞台停播](../Page/TVB新聞台.md "wikilink")，並只在[無綫新聞台播出](../Page/無綫新聞台.md "wikilink")。

## 節目調動

  - [1997年](../Page/1997年.md "wikilink")[6月30日](../Page/6月30日.md "wikilink")：倒數[香港回歸](../Page/香港回歸.md "wikilink")，當日延至13:30結束。
  - [1997年](../Page/1997年.md "wikilink")[7月1日](../Page/7月1日.md "wikilink")：[香港回歸](../Page/香港回歸.md "wikilink")，當日延長至一小時至14:00結束。
  - [2003年](../Page/2003年.md "wikilink")[3月20日](../Page/3月20日.md "wikilink")：因報道美國[攻打伊拉克消息](../Page/伊拉克戰爭.md "wikilink")，當日延至13:30結束。
  - [2005年](../Page/2005年.md "wikilink")[9月12日](../Page/9月12日.md "wikilink")：因報道[香港迪士尼樂園開幕](../Page/香港迪士尼樂園.md "wikilink")，當日提早至12:55播出。
  - [2008年北京奧運](../Page/2008年夏季奧林匹克運動會.md "wikilink")（[8月8日至](../Page/8月8日.md "wikilink")[8月24日](../Page/8月24日.md "wikilink")）舉行期間：《午間新聞》更加入體育主播，報道奧運消息。
  - [2011年](../Page/2011年.md "wikilink")[7月28日](../Page/7月28日.md "wikilink")：因要直播國務院總理[溫家寶在](../Page/溫家寶.md "wikilink")[溫州高鐵動車追撞事故現場舉行的中外記者會](../Page/2011年甬台溫鐵路列車追尾事故.md "wikilink")，當日延至13:30結束。
  - [2011年](../Page/2011年.md "wikilink")[9月29日](../Page/9月29日.md "wikilink")：因報導有關[颱風納沙襲港的消息](../Page/颱風納沙_\(2011年\).md "wikilink")，當日延至約13:20結束。
  - [2012年](../Page/2012年.md "wikilink")[3月25日](../Page/3月25日.md "wikilink")：因為直播參選[2012年香港特別行政區行政長官選舉的](../Page/2012年香港特別行政區行政長官選舉.md "wikilink")[梁振英勝利宣言](../Page/梁振英.md "wikilink")，當日延至約14:02結束。
  - [2012年倫敦奧運](../Page/2012年夏季奧林匹克運動會.md "wikilink")（[7月28日至](../Page/7月28日.md "wikilink")[8月13日](../Page/8月13日.md "wikilink")）舉行期間：《午間新聞》再次加入體育主播，報道奧運消息。而[互動新聞台及](../Page/互動新聞台.md "wikilink")[TVB新聞台星期六](../Page/TVB新聞台.md "wikilink")、日沒有聯播《午間新聞》，改為播放《[新聞報道](../Page/無綫電視新聞報道.md "wikilink")》報道奧運消息。
  - [2013年](../Page/2013年.md "wikilink")[1月16日](../Page/1月16日.md "wikilink")：因[梁振英發表任內](../Page/梁振英.md "wikilink")[首份施政報告至約](../Page/2013年度香港行政長官施政報告.md "wikilink")13:13，當日直播《[行政長官施政報告](../Page/行政長官施政報告.md "wikilink")》播放中途於13:00隨即插播《午間新聞》片頭，並繼續直播發表施政報告現場，之後才於13:13開始報道新聞，並延至13:30結束。
  - [2014年](../Page/2014年.md "wikilink")[9月27日](../Page/9月27日.md "wikilink")：因報道[罷課集會人士衝入公民廣場](../Page/2014年香港學界大罷課.md "wikilink")，以及警方相關清場情況，當日延至約13:18結束。而[互動新聞台及](../Page/互動新聞台.md "wikilink")[TVB新聞台亦因直播政總現場情況而沒有與](../Page/TVB新聞台.md "wikilink")[翡翠台聯播](../Page/翡翠台.md "wikilink")《午間新聞》。
  - [2016年](../Page/2016年.md "wikilink")[1月13日](../Page/1月13日.md "wikilink")：因[梁振英發表任內](../Page/梁振英.md "wikilink")[第四份施政報告至約](../Page/2016年度香港行政長官施政報告.md "wikilink")13:18，當日直播《[行政長官施政報告](../Page/行政長官施政報告.md "wikilink")》播放中途於13:00隨即插播《午間新聞》片頭，並繼續直播發表施政報告現場，之後才於13:18開始報道新聞，並延至約13:36結束。
  - [2016年](../Page/2016年.md "wikilink")[2月9日](../Page/2月9日.md "wikilink")：因報道[2016年農曆新年旺角騷亂](../Page/2016年農曆新年旺角騷亂.md "wikilink")，當日延至約13:25結束。
  - [2016年](../Page/2016年.md "wikilink")[9月5日](../Page/9月5日.md "wikilink")：因報道[2016立法會選舉點票消息](../Page/2016年香港立法會選舉.md "wikilink")，當日延至約13:25結束。
  - [2017年](../Page/2017年.md "wikilink")[1月18日](../Page/1月18日.md "wikilink")：因[梁振英發表任內](../Page/梁振英.md "wikilink")[第五份施政報告至約](../Page/2017年度香港行政長官施政報告.md "wikilink")13:18，當日直播《[行政長官施政報告](../Page/行政長官施政報告.md "wikilink")》播放中途於13:00隨即插播《午間新聞》片頭，並繼續直播發表施政報告現場，之後才於13:28開始報道新聞，並延至約13:45結束。
  - [2017年](../Page/2017年.md "wikilink")[3月26日](../Page/3月26日.md "wikilink")：因直播[2017年行政長官選舉](../Page/2017年香港特別行政區行政長官選舉.md "wikilink")，當日直播《2017行政長官選舉》途中於13:00插播《午間新聞》片頭，並繼續直播特首選舉點票現場，其後因直播當選人[林鄭月娥的勝利宣言](../Page/林鄭月娥.md "wikilink")，延至約15:05結束。
  - [2017年](../Page/2017年.md "wikilink")[7月1日](../Page/7月1日.md "wikilink")：因直播[訪港三天的中共總書記](../Page/習近平總書記視察香港.md "wikilink")[習近平乘坐](../Page/習近平.md "wikilink")[專機離港](../Page/中華人民共和國專機.md "wikilink")，《午間新聞》延至13:04播出，並順延至13:24結束。
  - [2018年](../Page/2018年.md "wikilink")[3月11日](../Page/3月11日.md "wikilink")：因報道[2018立法會補選點票消息](../Page/2018年3月香港立法會補選.md "wikilink")，當日延至約13:18結束。
  - [2018年](../Page/2018年.md "wikilink")[9月16日](../Page/9月16日.md "wikilink")：因報導有關[強颱風山竹襲港的消息](../Page/颱風山竹_\(2018年\).md "wikilink")，當日延至13:23結束。

## 片頭變遷

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

歷年開場畫面

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

<File:TVB> Jade Noon News
2009.jpg|[2009年](../Page/2009年.md "wikilink")[2月2日至](../Page/2月2日.md "wikilink")[2014年](../Page/2014年.md "wikilink")[5月31日](../Page/5月31日.md "wikilink")
<File:TVB> Jade Noon
News.jpg|[2016年](../Page/2016年.md "wikilink")[2月22日至今](../Page/2月22日.md "wikilink")

</div>

</div>

## 主播

正常情況下，《午間新聞》只有一位主播負責整節新聞，惟在特別日子曾出現由兩位主播負責。

現職新聞主播：[梁凱寧](../Page/梁凱寧.md "wikilink")、[李卓謙](../Page/李卓謙.md "wikilink")、[黃建瑩](../Page/黃建瑩.md "wikilink")、[張文采](../Page/張文采.md "wikilink")

現職體育主播（奧運期間）：[文宇軒](../Page/文宇軒.md "wikilink")

現職前任主播：[袁志偉](../Page/袁志偉_\(記者\).md "wikilink")、[黄淑明](../Page/黄淑明.md "wikilink")、[許方輝](../Page/許方輝.md "wikilink")、[方東昇](../Page/方東昇.md "wikilink")、[潘蔚林](../Page/潘蔚林.md "wikilink")、[陳嘉欣](../Page/陳嘉欣.md "wikilink")、[李明娜](../Page/李明娜.md "wikilink")、[李曉欣](../Page/李曉欣.md "wikilink")、[黃珊](../Page/黃珊.md "wikilink")、[黃曉瑩](../Page/黃曉瑩.md "wikilink")、[簡惠宜](../Page/簡惠宜.md "wikilink")

已離職新聞主播：[梁家榮](../Page/梁家榮.md "wikilink")、[蘇凌峰](../Page/蘇凌峰.md "wikilink")、[黄慶州](../Page/黄慶州.md "wikilink")、[李汶靜](../Page/李汶靜.md "wikilink")、[何潔貞](../Page/何潔貞.md "wikilink")、[梁潔娥](../Page/梁潔娥.md "wikilink")、[周潔儀](../Page/周潔儀.md "wikilink")、[趙海珠](../Page/趙海珠.md "wikilink")、[吳先瑩](../Page/吳先瑩.md "wikilink")、[吳雪文](../Page/吳雪文.md "wikilink")、[呂秉權](../Page/呂秉權.md "wikilink")、[廖忠平](../Page/廖忠平.md "wikilink")、[陸嘉敏](../Page/陸嘉敏.md "wikilink")、[何富豪](../Page/何富豪.md "wikilink")、[鄺美鳳](../Page/鄺美鳳.md "wikilink")、[黃章翹](../Page/黃章翹.md "wikilink")、[邱文華](../Page/邱文華.md "wikilink")、[潘錫欽](../Page/潘錫欽.md "wikilink")、[徐蕙儀](../Page/徐蕙儀.md "wikilink")、[李臻](../Page/李臻.md "wikilink")、[李瑞冰](../Page/李瑞冰.md "wikilink")、[楊玉珍](../Page/楊玉珍.md "wikilink")、[李燦榮](../Page/李燦榮.md "wikilink")、[何錦欣](../Page/何錦欣.md "wikilink")、[馮佩樂](../Page/馮佩樂.md "wikilink")、[陳珮瑩](../Page/陳珮瑩.md "wikilink")、[郭詠琴](../Page/郭詠琴.md "wikilink")、[邱振明](../Page/邱振明.md "wikilink")、[柳俊江](../Page/柳俊江.md "wikilink")、[劉肇邦](../Page/劉肇邦.md "wikilink")、[蔡富華](../Page/蔡富華.md "wikilink")、[趙麗如](../Page/趙麗如.md "wikilink")、[張婉玲](../Page/張婉玲.md "wikilink")、[魏綺珊](../Page/魏綺珊.md "wikilink")、[黃家俊](../Page/黃家俊.md "wikilink")、[楊潔恩](../Page/楊潔恩.md "wikilink")、[黃大鈞](../Page/黃大鈞.md "wikilink")、[方健儀](../Page/方健儀.md "wikilink")、[盤翠瑩](../Page/盤翠瑩.md "wikilink")、[郭詠嘉](../Page/郭詠嘉.md "wikilink")、[林燕玲](../Page/林燕玲.md "wikilink")、[陳偉利](../Page/陳偉利.md "wikilink")、[李家文](../Page/李家文.md "wikilink")、[談美琪](../Page/談美琪.md "wikilink")、[林子豪](../Page/林子豪.md "wikilink")、[劉芷欣](../Page/劉芷欣.md "wikilink")、[葉昇瓚](../Page/葉昇瓚.md "wikilink")、[彭國柱](../Page/彭國柱.md "wikilink")、[鄭詩亭](../Page/鄭詩亭.md "wikilink")、[馮堅成](../Page/馮堅成.md "wikilink")、[劉晉安](../Page/劉晉安.md "wikilink")、[鄭萃雯](../Page/鄭萃雯.md "wikilink")、[吳璟儁](../Page/吳璟儁.md "wikilink")、[李朗怡](../Page/李朗怡.md "wikilink")、[藍可盈](../Page/藍可盈.md "wikilink")

## 內容

現時《午間新聞》的節目內容包括：

  - **一般新聞**（約十分鐘）：包括港聞、內地時事、國際時事、軟性新聞、體育
  - **午間財經**（約三分鐘）：報告[股市狀況](../Page/股市.md "wikilink")、貨幣兌換價（只限港股開市日）
  - **[天氣](../Page/天氣.md "wikilink")**：包括[溫度](../Page/溫度.md "wikilink")、[濕度等](../Page/濕度.md "wikilink")；[香港空氣質素健康指數及](../Page/香港空氣質素健康指數.md "wikilink")[紫外線指數](../Page/紫外線指數.md "wikilink")

另外，新聞播放完畢後，會即時轉播《[瞬間看地球](../Page/瞬間看地球.md "wikilink")》。

## 海外版

另外，無綫電視亦有製作海外版的《午間新聞》，内容大致錄取自香港版的《午間新聞》，但部分採用由國際通行社提供片段的新聞内容則被刪除。節目片頭中不設新聞提要，主播在節目開始時亦會特別歡迎各位海外觀眾收看這節新聞。主播則與同日香港版《午間新聞》的主播相同。

[加拿大的](../Page/加拿大.md "wikilink")[新時代電視現時以](../Page/新時代電視.md "wikilink")《香港衛星新聞
- 早晨第一線》的名義，每天於當地時間07:30和07:50播放海外版《午間新聞》。\[1\]

## 參考資料

## 外部連結

  - [無綫新聞網站 -
    《午間新聞》重溫](http://news.tvb.com/programmes/noonnewsjade0001/)

[Category:無綫新聞節目](../Category/無綫新聞節目.md "wikilink")

1.  [新時代電視節目表](http://www.fairchildtv.com/schedule_ftv.php)