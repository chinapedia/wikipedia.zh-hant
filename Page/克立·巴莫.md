**克立·巴莫蒙拉查翁亲王**（；[RTGS](../Page/RTGS.md "wikilink")：Khuekrit
Pramot；），[泰国政治家和作家](../Page/泰国.md "wikilink")，[泰國第](../Page/泰國.md "wikilink")13任[泰國首相](../Page/泰國首相.md "wikilink")。

克立出生于[信武里府](../Page/信武里府.md "wikilink")，祖父是泰王[拉玛二世之子](../Page/拉玛二世.md "wikilink")，祖母有華裔血统，父亲Khamrob亲王准将（Brigadier
General Prince
Khamrob）是泰国首任警察总监。他的哥哥[社尼·巴莫和他一样曾任泰國首相](../Page/社尼·巴莫.md "wikilink")。他早年留学英国[牛津大学](../Page/牛津大学.md "wikilink")9年，攻读[哲学](../Page/哲学.md "wikilink")、[经济学和政治学](../Page/经济学.md "wikilink")。

1946年起从政，1973年12月当选议长，1974年7月创立社会行动党，1975年3月至1976年1月，任泰国政府首相兼任内政部长，期間於1975年7月1日，泰國與[中華人民共和國正式建立外交關係](../Page/中華人民共和國.md "wikilink")，他本人之後亦正式訪問中華人民共和國，成為首位訪问中華人民共和國的泰國總理。

1990年荣获[福冈亚洲文化奖](../Page/福冈亚洲文化奖.md "wikilink")。

## 作品

·《[四朝代](../Page/四朝代.md "wikilink")》（小说，写于1953年）

## 外部参考

1.  [三十五年的起步——写在中泰建交35周年纪念前夕](http://www.thaiwind.net/index.php?option=com_content&view=article&id=56:sm-jjlh&catid=2:panorama-on-thailand&Itemid=48)
2.  [剪報 7/6/1975 克立·巴莫
    訪中國](http://daj.by.gov.cn/by/pic_paper/detail_news.jsp?id=421)
3.  [**克立·巴莫** 1995年10月9日，泰國前首相
    享年84歲](https://web.archive.org/web/20060503205320/http://www.epochtimes.com/b5/1/10/9/c6539.htm)

<!-- end list -->

  - [泰国人怎么过春节](http://www.cass.net.cn/file/2007021687843.html)

[Category:福岡亞洲文化獎獲得者](../Category/福岡亞洲文化獎獲得者.md "wikilink")
[Category:泰国总理](../Category/泰国总理.md "wikilink")
[Category:泰國內政部長](../Category/泰國內政部長.md "wikilink")
[Category:泰国王室](../Category/泰国王室.md "wikilink")
[Category:泰国作家](../Category/泰国作家.md "wikilink")
[Category:牛津大學王后學院校友](../Category/牛津大學王后學院校友.md "wikilink")
[Category:伊朗裔泰國人](../Category/伊朗裔泰國人.md "wikilink")
[Category:泰國華人](../Category/泰國華人.md "wikilink")
[Category:信武里府人](../Category/信武里府人.md "wikilink")