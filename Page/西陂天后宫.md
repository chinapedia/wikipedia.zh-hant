**西陂天后宫**位于[福建](../Page/福建.md "wikilink")[永定县高陂乡西陂村](../Page/永定县.md "wikilink")，主祀[妈祖女神](../Page/妈祖.md "wikilink")。2006年被列为[第六批全国重点文物保护单位](../Page/第六批全国重点文物保护单位.md "wikilink")。

## 历史

西陂天后宫始建于[明](../Page/明.md "wikilink")[嘉靖二十一年](../Page/嘉靖.md "wikilink")（1542年），落成于[清](../Page/清.md "wikilink")[康熙元年](../Page/康熙.md "wikilink")（1662年）。西陂村民多为林姓，因乡民渡海移民往[东南亚等地](../Page/东南亚.md "wikilink")，因此修建天后宫祈求妈祖保佑。

## 建筑

西陂天后宫由大门、戏台、大宝殿和登云馆组成，占地6435平方米。主体建筑为7层塔式结构，通高40余米，俗称“文塔”。底层为主殿，供奉主神[天后](../Page/天后.md "wikilink")，二至五层，分祀[关帝](../Page/关帝.md "wikilink")、[文昌帝君](../Page/文昌帝君.md "wikilink")、[魁星和](../Page/魁星.md "wikilink")[仓颉](../Page/仓颉.md "wikilink")。

## 参考文献

[category:福建妈祖庙](../Page/category:福建妈祖庙.md "wikilink")

[Category:永定区 (龙岩市)](../Category/永定区_\(龙岩市\).md "wikilink")
[Category:福建全国重点文物保护单位](../Category/福建全国重点文物保护单位.md "wikilink")