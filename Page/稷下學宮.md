**稷下學宮**，又稱**稷下之學**，是中國最早的國立高等學府，始建於[戰國時期](../Page/戰國.md "wikilink")(西元前四世紀)的[田齊桓公](../Page/田齊桓公.md "wikilink")。[稷下位於齊國國都](../Page/稷下.md "wikilink")[臨淄](../Page/臨淄.md "wikilink")（今[山东省](../Page/山东省.md "wikilink")[淄博市](../Page/淄博市.md "wikilink")）稷門附近。[齊宣王之時](../Page/齊宣王.md "wikilink")，在稷下擴置學宮，招致天下名士：[儒家](../Page/儒家.md "wikilink")、[道家](../Page/道家.md "wikilink")、[法家](../Page/法家.md "wikilink")、[名家](../Page/名家.md "wikilink")、[兵家](../Page/兵家.md "wikilink")、[農家](../Page/農家.md "wikilink")、[陰陽家等百家之學](../Page/陰陽家.md "wikilink")，會集於此，自由講學、著書論辯。

## 歷史

前4世紀中葉，齊國創立稷下學宮，供養資助許多思想家和學者，維持至前3世紀前期，是當時最重要的學術中心\[1\]。[戰國中後期各主要學派的重要人物](../Page/戰國.md "wikilink")，如：[荀子](../Page/荀子.md "wikilink")、[宋钘](../Page/宋钘.md "wikilink")、[尹文](../Page/尹文.md "wikilink")、[鲁仲连](../Page/鲁仲连.md "wikilink")、田巴、貌说、邹奭，幾乎都來到過稷下，《[史記](../Page/史記.md "wikilink")》描述當時的盛況：“宣王喜文學遊說之士，自[鄒衍](../Page/鄒衍.md "wikilink")、[淳于髡](../Page/淳于髡.md "wikilink")、[田駢](../Page/田駢.md "wikilink")、[接輿](../Page/接輿.md "wikilink")、[慎到](../Page/慎到.md "wikilink")、[環淵之徒七十六人](../Page/環淵.md "wikilink")，皆賜列第，為[上大夫](../Page/上大夫.md "wikilink")，不治而議論。是以齊稷下學士復盛，且數百千人……”。稷下學宮是一种由官方主办、私人主持的办学模式。不问学术派别、思想观点、政治倾向，以及国别、年龄、资历的学术自由气氛，約与同时代[雅典的](../Page/雅典.md "wikilink")[亚里斯多德于公元前](../Page/亚里斯多德.md "wikilink")335/334年所创[吕克昂学院东西并尊](../Page/吕克昂学院.md "wikilink")。

## 講學名家

  - 陰陽家學派

<!-- end list -->

  - [鄒衍](../Page/鄒衍.md "wikilink")：[戰國末期](../Page/戰國.md "wikilink")[齊國人](../Page/齊國.md "wikilink")，[陰陽家學派創始者與代表人物](../Page/陰陽家.md "wikilink")。主要[學說是](../Page/學說.md "wikilink")「[五德終始說](../Page/五德終始.md "wikilink")」和「[大九州說](../Page/大九州.md "wikilink")」，當時人們稱他「談天衍」，又稱鄒子。
  - [騶奭](../Page/騶奭.md "wikilink")：[戰國](../Page/戰國.md "wikilink")[齊國稷下學宮學者](../Page/齊國.md "wikilink")，採用鄒衍學說入自己之文，人稱雕龍奭。受到[齊王的賞識](../Page/齊王.md "wikilink")，在稷下學宮與[慎到](../Page/慎到.md "wikilink")、[田駢齊名](../Page/田駢.md "wikilink")。[齊王為他們修建](../Page/齊王.md "wikilink")[豪宅](../Page/豪宅.md "wikilink")。騶奭著《[騶奭](../Page/鄒奭_\(著作\).md "wikilink")》12篇。

<!-- end list -->

  - 偏向道家學派

<!-- end list -->

  - [尹文](../Page/尹文.md "wikilink")：[齊國稷下名家學者](../Page/齊國.md "wikilink")，著有《[尹文子](../Page/尹文子.md "wikilink")》。
  - [倪說](../Page/倪說.md "wikilink")：[齊國稷下名家學者](../Page/齊國.md "wikilink")，曾為薛公[靖郭君說服](../Page/靖郭君.md "wikilink")[齊宣王](../Page/齊宣王.md "wikilink")，使得薛公恩眷不衰。
  - [田駢](../Page/田駢.md "wikilink")：又稱陳駢，[戰國](../Page/戰國.md "wikilink")[齊國人](../Page/齊國.md "wikilink")，早年學[黃老之術](../Page/黃老之術.md "wikilink")。有[辯才](../Page/辯才.md "wikilink")，尤好[爭論](../Page/爭論.md "wikilink")，人稱「天口駢」。
  - [彭蒙](../Page/彭蒙.md "wikilink")：[齐国人](../Page/齐国.md "wikilink")，黄老道家学者，是田骈的老师。
  - [慎到](../Page/慎到.md "wikilink")：[趙國人](../Page/趙國.md "wikilink")。[戰國時](../Page/戰國.md "wikilink")[道家](../Page/道家.md "wikilink")、[法家](../Page/法家.md "wikilink")[思想家](../Page/思想家.md "wikilink")。
  - [環淵](../Page/環淵.md "wikilink")：[楚國人](../Page/楚國.md "wikilink")，[戰國時](../Page/戰國.md "wikilink")[道家學者](../Page/道家.md "wikilink")。環淵學[黃老道德之術](../Page/黃老.md "wikilink")，因闡發說明敘述黃老道德學說的旨意。環淵著上下篇，與[騶衍](../Page/騶衍.md "wikilink")、[淳于髡](../Page/淳于髡.md "wikilink")、[慎到](../Page/慎到.md "wikilink")、[接子](../Page/接子.md "wikilink")、[田駢](../Page/田駢.md "wikilink")、[騶奭一起遊學於稷下學宮](../Page/騶奭.md "wikilink")。
  - [宋鈃](../Page/宋鈃.md "wikilink")：是遊學於[齊國稷下學宮的學者](../Page/齊國.md "wikilink")。有授徒講學。《[莊子](../Page/莊子_\(書\).md "wikilink")·天下》將宋鈃和尹文歸類為一派。但《[荀子](../Page/荀子_\(書\).md "wikilink")·非十二子》將[墨子和宋鈃劃為一流](../Page/墨子.md "wikilink")，荀子是根據宋鈃思想在提倡克制慾望講究節儉不重視等級等方面與墨子的類似之處，而將他們劃為一流。
  - [接子](../Page/接子.md "wikilink")：[戰國](../Page/戰國.md "wikilink")[齊國稷下學宮學者](../Page/齊國.md "wikilink")，曹姓，接氏。受到[齊宣王的賞識](../Page/齊宣王.md "wikilink")，在稷下學宮與[鄒衍](../Page/鄒衍.md "wikilink")、[騶奭](../Page/騶奭.md "wikilink")、[田駢齊名](../Page/田駢.md "wikilink")。齊王為他們修建[豪宅](../Page/豪宅.md "wikilink")。著《[接子](../Page/接子_\(著作\).md "wikilink")》二篇。

<!-- end list -->

  - 遊說家

<!-- end list -->

  - [淳于髡](../Page/淳于髡.md "wikilink")，[戰國時期](../Page/戰國.md "wikilink")[齊國人](../Page/齊國.md "wikilink")，[身高不足七](../Page/身高.md "wikilink")[尺](../Page/尺.md "wikilink")，是齊國的[入贅](../Page/入贅.md "wikilink")[女婿](../Page/女婿.md "wikilink")。齊國[大臣](../Page/大臣.md "wikilink")，[外交官](../Page/外交官.md "wikilink")。善於[辯論](../Page/辯論.md "wikilink")，經常代表齊國[出使各國](../Page/出使.md "wikilink")。
  - [魯仲連](../Page/魯仲連.md "wikilink")：[戰國時代](../Page/戰國.md "wikilink")[齊國](../Page/齊國.md "wikilink")[茌平人](../Page/茌平.md "wikilink")（今[山東省](../Page/山東省.md "wikilink")[茌平縣](../Page/茌平縣.md "wikilink")[王老鄉](../Page/王老鄉.md "wikilink")[望魯店村](../Page/望魯店村.md "wikilink")），為[遊說名士](../Page/遊說.md "wikilink")。有時簡稱魯連，曾就學於稷下學宮，不願出任[官職](../Page/官職.md "wikilink")。由於他的遊說技巧卓越，有著名的「義不帝秦」[辯論](../Page/辯論.md "wikilink")。成為現代「和事佬」的代名詞。《[漢書](../Page/漢書.md "wikilink")·藝文志》有《[魯仲連子](../Page/魯仲連子_\(著作\).md "wikilink")》14篇。

<!-- end list -->

  - 儒家

<!-- end list -->

  - [荀子](../Page/荀子.md "wikilink")：[戰國末期](../Page/戰國.md "wikilink")[趙國](../Page/趙國.md "wikilink")[猗氏](../Page/猗氏.md "wikilink")（今[山西](../Page/山西.md "wikilink")[安澤](../Page/安澤.md "wikilink")）人，名**況**，時人尊而號為卿。著名[思想家](../Page/思想家.md "wikilink")，[教育家](../Page/教育家.md "wikilink")，[儒家代表人物之一](../Page/儒家.md "wikilink")，對儒家思想有所發展，提倡[性惡論](../Page/性惡論.md "wikilink")，常被與所謂的[孟子](../Page/孟子.md "wikilink")「[性善論](../Page/性善論.md "wikilink")」比較。荀況對重整儒家[典籍也有相當的貢獻](../Page/典籍.md "wikilink")。

## 参考文献

{{-}}

[Category:春秋战国文化](../Category/春秋战国文化.md "wikilink")
[Category:齐国](../Category/齐国.md "wikilink")
[Category:山东教育史](../Category/山东教育史.md "wikilink")
[Category:諸子百家](../Category/諸子百家.md "wikilink")

1.