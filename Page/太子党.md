**太子党**指因其父母权势而获得了重要职位的人。\[1\]

其词义近似于“官二代”“高干子弟”

## 例子

### 中华民国

20世纪初，该词特指[袁世凯的儿子](../Page/袁世凯.md "wikilink")[袁克定及其亲信或革命元老的子女](../Page/袁克定.md "wikilink")，后来特指[蒋宋孔陈四大家族内依靠父兄的势力](../Page/蒋宋孔陈四大家族.md "wikilink")，获取高层政治职务的子女亲属；[民國政府遷台后](../Page/民國政府.md "wikilink")，在[台湾特指](../Page/台湾.md "wikilink")[蔣中正之子](../Page/蔣中正.md "wikilink")[蔣經國](../Page/蔣經國.md "wikilink")、[蔣緯國及其同僚](../Page/蔣緯國.md "wikilink")。

[民主進步黨主席](../Page/民主進步黨.md "wikilink")[蔡英文在](../Page/蔡英文.md "wikilink")[兩岸經濟協議ECFA電視辯論中指出有](../Page/兩岸經濟協議ECFA電視辯論.md "wikilink")「兩岸太子黨」的存在；一般的解讀是國共兩黨的政商權貴；在[台湾的例子如](../Page/台湾.md "wikilink")：[中華民國總統](../Page/中華民國.md "wikilink")[馬英九之父](../Page/馬英九.md "wikilink")[馬鶴凌](../Page/馬鶴凌.md "wikilink")，[台北市市長](../Page/台北市.md "wikilink")[郝龍斌之父](../Page/郝龍斌.md "wikilink")[郝柏村](../Page/郝柏村.md "wikilink")，[桃園縣縣長](../Page/桃園市.md "wikilink")[吳志揚之父](../Page/吳志揚_\(政治人物\).md "wikilink")[吳伯雄](../Page/吳伯雄.md "wikilink")，[李慶華与](../Page/李慶華.md "wikilink")[李慶安兄妹之父](../Page/李慶安.md "wikilink")[李煥](../Page/李煥.md "wikilink")，[連勝文之父](../Page/連勝文.md "wikilink")[連戰](../Page/連戰.md "wikilink")。

### 中华人民共和国

[中華人民共和國成立後](../Page/中華人民共和國.md "wikilink")，在[中國大陸](../Page/中國大陸.md "wikilink")，一般特指[中国共产党高层官员的子女以及亲属](../Page/中国共产党.md "wikilink")，1970年代一般指[林彪的儿子](../Page/林彪.md "wikilink")[林立果及其](../Page/林立果.md "wikilink")[同党](../Page/林彪反革命集团.md "wikilink")。[改革开放以后](../Page/改革开放.md "wikilink")，该词指高层领导干部的获得政治、商业利益的子女亲戚\[2\]，特別是指自中华人民共和国建立至1980年代期间之[中共元老的子女辈](../Page/中共元老.md "wikilink")，如现任[中共中央总书记](../Page/中国共产党中央委员会总书记.md "wikilink")、[国家主席](../Page/中华人民共和国主席.md "wikilink")、[中央军委主席](../Page/中国共产党中央军事委员会主席.md "wikilink")[习近平](../Page/习近平.md "wikilink")（[习仲勋之子](../Page/习仲勋.md "wikilink")），原任中共中央政治局常委、[全国政协主席](../Page/全国政协主席.md "wikilink")[俞正声](../Page/俞正声.md "wikilink")（[俞启威之子](../Page/俞启威.md "wikilink")），原任中共中央政治局常委、[中纪委书记](../Page/中纪委书记.md "wikilink")、现任[国家副主席](../Page/中华人民共和国副主席.md "wikilink")[王岐山](../Page/王岐山.md "wikilink")（[姚依林之女婿](../Page/姚依林.md "wikilink")），原任中共中央政治局常委、[中共中央书记处书记](../Page/中共中央书记处.md "wikilink")、[国家副主席](../Page/中华人民共和国副主席.md "wikilink")[曾庆红](../Page/曾庆红.md "wikilink")（[曾山之子](../Page/曾山.md "wikilink")）\[3\]，原任[中共中央政治局委员](../Page/中共中央政治局.md "wikilink")、[中共重庆市委书记](../Page/中国共产党重庆市委员会.md "wikilink")[薄熙来](../Page/薄熙来.md "wikilink")（[薄一波之子](../Page/薄一波.md "wikilink")），原任[全国政协副主席](../Page/全国政协副主席.md "wikilink")[叶选平](../Page/叶选平.md "wikilink")（[叶剑英之子](../Page/叶剑英.md "wikilink")），原任全国政协副主席[邓朴方](../Page/邓朴方.md "wikilink")（[邓小平之子](../Page/邓小平.md "wikilink")），现任全国政协副主席[陈元](../Page/陈元.md "wikilink")（[陈云之子](../Page/陈云.md "wikilink")），现任[交通运输部部长](../Page/交通运输部.md "wikilink")[李小鹏](../Page/李小鹏_\(政治人物\).md "wikilink")([李鹏之子](../Page/李鹏.md "wikilink"))，原任[中共内蒙古区委副书记暨](../Page/中共内蒙古自治区党委.md "wikilink")[自治区政府主席](../Page/内蒙古自治区人民政府.md "wikilink")[布赫](../Page/布赫.md "wikilink")（[乌兰夫之子](../Page/乌兰夫.md "wikilink")），原任[中信集团董事长](../Page/中国中信集团公司.md "wikilink")[王军](../Page/王军_\(企业家\).md "wikilink")（[王震之子](../Page/王震.md "wikilink")）\[4\]。

有研究认为中国的县级政府中普遍有由亲戚子女所构成的政治家族。\[5\]

### 美国

在[美国](../Page/美国.md "wikilink")，有很多通过家族实力以及[裙带关系取得高官的人物](../Page/裙带关系.md "wikilink")\[6\]，如来自[布什家族的](../Page/布什家族.md "wikilink")[乔治·H·W·布什](../Page/乔治·H·W·布什.md "wikilink")、[乔治·W·布什](../Page/乔治·W·布什.md "wikilink")、[肯尼迪家族的](../Page/肯尼迪家族.md "wikilink")[罗伯特·肯尼迪](../Page/罗伯特·弗朗西斯·肯尼迪.md "wikilink")、[约翰·肯尼迪](../Page/约翰·肯尼迪.md "wikilink")，来自[罗斯福家族的](../Page/罗斯福家族.md "wikilink")[西奥多·罗斯福](../Page/西奥多·罗斯福.md "wikilink")、[富兰克林·罗斯福](../Page/富兰克林·罗斯福.md "wikilink")，来自[唐纳德·特朗普家族的](../Page/唐纳德·特朗普.md "wikilink")[伊万卡·特朗普](../Page/伊万卡·特朗普.md "wikilink")、[贾里德·库什纳](../Page/贾里德·库什纳.md "wikilink")，[理查德·切尼家族的](../Page/理查德·切尼.md "wikilink")[伊丽莎白·切尼](../Page/伊丽莎白·切尼.md "wikilink")，[柯林·鲍威尔家族的](../Page/柯林·鲍威尔.md "wikilink")[麦克林·鲍威尔](../Page/麦克林·鲍威尔.md "wikilink")\[7\]。

美国作家亚当·贝娄忧心忡忡的对美国人民警告“看看这些世家子弟，我们也得担心它的另外一些方面\[8\]”

### 日本

[日本政坛也有太子党](../Page/日本.md "wikilink")，如近年当过[总理大臣](../Page/日本内阁总理大臣.md "wikilink")（首相）的[安倍晋三以及](../Page/安倍晋三.md "wikilink")[福田康夫在内的很多政治人物都有着在政商界的家族亲缘关系](../Page/福田康夫.md "wikilink")，政治联姻是日本政坛的普遍现象\[9\]\[10\]。

### 其它

在世界其他国家和地区，例如[-{zh-hans:朝鲜;
zh-hant:北韓;}-与](../Page/北韓.md "wikilink")[古巴](../Page/古巴.md "wikilink")\[11\]、[以色列](../Page/以色列.md "wikilink")\[12\]，[新加坡](../Page/新加坡.md "wikilink")、[印度等](../Page/印度.md "wikilink")，太子党也普遍存在。如[朝鲜劳动党委员长](../Page/朝鲜劳动党委员长.md "wikilink")[金正恩](../Page/金正恩.md "wikilink")、他已故父亲[金正日](../Page/金正日.md "wikilink")、[新加坡总理](../Page/新加坡总理.md "wikilink")[李显龙等](../Page/李显龙.md "wikilink")，就是凭借父荫获得[政治资本的典型人物](../Page/政治资本.md "wikilink")，而印度的[尼赫鲁-甘地家族则长期把持着](../Page/尼赫鲁-甘地家族.md "wikilink")[印度国大党](../Page/印度国大党.md "wikilink")。

还有伊拉克[薩達姆·海珊](../Page/薩達姆·海珊.md "wikilink")、利比亞[-{zh-cn:卡扎菲;zh-hk:卡達菲;zh-tw:格達費;}-](../Page/穆阿邁爾·卡扎菲.md "wikilink")、[-{zh-hans:突尼斯;
zh-hant:突尼西亞;}-](../Page/突尼斯.md "wikilink")[班阿里](../Page/宰因·阿比丁·本·阿里.md "wikilink")、埃及[穆巴拉克](../Page/胡斯尼·穆巴拉克.md "wikilink")、敘利亞[阿薩德父子](../Page/巴沙爾·阿薩德.md "wikilink")、[-{zh-hans:也门;
zh-hant:葉門;}-](../Page/也门.md "wikilink")[沙雷等獨裁領袖](../Page/阿里·阿卜杜拉·薩利赫.md "wikilink")，皆以「[家天下](../Page/世襲專政.md "wikilink")」的世襲體系，讓子女掌控國家權力核心，因而引起濫權、貪污等不當行徑。

## 参见

  - [高干子弟](../Page/高干子弟.md "wikilink")、[官二代](../Page/官二代.md "wikilink")、[红二代](../Page/红二代.md "wikilink")/[红五类](../Page/红五类.md "wikilink")/[红色贵族](../Page/红色贵族.md "wikilink")、[富二代](../Page/富二代.md "wikilink")
  - [裙带资本主义](../Page/裙带资本主义.md "wikilink")、[官僚资本主义](../Page/官僚资本主义.md "wikilink")、[赵家人](../Page/赵家人.md "wikilink")
  - [天龙人](../Page/天龙人.md "wikilink")、
  - [共济会](../Page/共济会.md "wikilink")、[骷髅会](../Page/骷髅会.md "wikilink")

## 參考文献

## 外部链接

  -
  -
  - [太子党关系网络](https://github.com/programthink/zhao)

[Category:政治人物](../Category/政治人物.md "wikilink") [Category:中华人民共和国特定人群称谓
(政治术语)](../Category/中华人民共和国特定人群称谓_\(政治术语\).md "wikilink")
[Category:貶義詞](../Category/貶義詞.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.

11.

12.