**露叶毛毡苔科**又名[粘虫草科](../Page/粘虫草科.md "wikilink")，只有1[属](../Page/属.md "wikilink")1[种](../Page/种.md "wikilink")，是单种[科](../Page/科.md "wikilink")，分布在[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙和](../Page/葡萄牙.md "wikilink")[摩洛哥](../Page/摩洛哥.md "wikilink")。
[Drosophyllum_lusitanicum_Darwin14.png](https://zh.wikipedia.org/wiki/File:Drosophyllum_lusitanicum_Darwin14.png "fig:Drosophyllum_lusitanicum_Darwin14.png")
本科[植物是一种食虫植物](../Page/植物.md "wikilink")，生长在干燥的碱性土壤地带，[叶长](../Page/叶.md "wikilink")20-40厘米，伸展，叶子可以分泌甜味的黏液，吸引[昆虫](../Page/昆虫.md "wikilink")，昆虫被粘住后由消化液消化吸收；[花相当大](../Page/花.md "wikilink")，[直径有](../Page/直径.md "wikilink")4厘米，[黄色](../Page/黄色.md "wikilink")，花期为2月至5月；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，[种子直径为](../Page/种子.md "wikilink")2.5毫米。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[茅膏菜科中](../Page/茅膏菜科.md "wikilink")，属于[猪笼草目](../Page/猪笼草目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，放到[石竹目之下](../Page/石竹目.md "wikilink")，
2003年经过修订的[APG II 分类法维持原分类](../Page/APG_II_分类法.md "wikilink")。

[Drosophyllum_lusitanicum_ne.jpg](https://zh.wikipedia.org/wiki/File:Drosophyllum_lusitanicum_ne.jpg "fig:Drosophyllum_lusitanicum_ne.jpg")

## 外部链接

  - [APG网站中的露叶毛毡苔科](http://www.mobot.org/MOBOT/Research/APWeb/orders/caryophyllalesweb.htm)
  - [NCBI中的露叶毛毡苔科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=232384)

[\*](../Category/露叶毛毡苔科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")