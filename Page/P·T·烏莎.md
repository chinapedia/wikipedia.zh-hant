**披拉伍拉坎蒂·梯克瓦蘭比·烏莎**（**Pilavullakandi Thekkevarambil
Usha**）（）是活躍在[二十世紀八十至九十年代的](../Page/二十世紀.md "wikilink")[印度短跑運動員](../Page/印度.md "wikilink")，在印度人們時常稱她為**P.T.烏莎**。由于她1964年5月20日出生於南印度[喀拉拉邦](../Page/喀拉拉邦.md "wikilink")[科芝科德縣的帕由里村](../Page/科芝科德縣.md "wikilink")（Payyoli），所以在她成名后人們又叫她*帕由里快車*（Payyoli
Express）。

烏莎十二歲時，喀拉拉邦政府興辦[體育運動學校](../Page/喀拉拉邦體育運動學校.md "wikilink")，她被選拔代表全縣入校，每月獲得250[印度盧比的人才培養津貼](../Page/印度盧比.md "wikilink")。1979年，十五歲的烏莎參加[全印度中學生運動會](../Page/全印度中學生運動會.md "wikilink")，她被[瑪哈萬·納馬比爾](../Page/瑪哈萬·納馬比爾.md "wikilink")（Madhavan
Nambiar）教練發現並進行專業訓練。次年即代表印度參加了[莫斯科奧運會](../Page/莫斯科奧運會.md "wikilink")。

1982年[新德裡亞運會](../Page/新德裡亞運會.md "wikilink")，她得到女子100米和200米銀牌。1983年參加在[科威特城舉行的](../Page/科威特城.md "wikilink")[亞洲田徑錦標賽](../Page/亞洲田徑錦標賽.md "wikilink")，烏莎獲得女子400米金牌並打破[亞洲記錄](../Page/亞洲田徑記錄.md "wikilink")。此後一直到1989年，她總共在亞洲田徑錦標賽上獲得13塊金牌，3塊銀牌，1塊銅牌。1984年的[洛杉磯奧運會](../Page/洛杉磯奧運會.md "wikilink")，烏莎以最佳成績進入女子400米欄的決賽，可惜在決賽最後階段後勁不繼，以百分之一秒之差取得第四，與獎牌失之交臂。

1985年的亞洲田徑錦標賽，她獲得六塊獎牌（5塊金牌和1塊銅牌），至今這仍是世界上任何田徑運動員在同一次洲際運動會上取得獎牌的記錄。1986年[首爾](../Page/首爾.md "wikilink")（當時叫漢城）[亞運會](../Page/漢城亞運會.md "wikilink")，烏莎得到4塊金牌和1塊銀牌，在她參加的所有項目里都刷新了亞運會記錄。

烏莎在整個運動生涯中總共為印度獲得了101塊國際比賽的獎牌，為此她獲得了[議會頒發的獎章](../Page/印度議會.md "wikilink")。今天的烏莎是[印度南方鐵路局的僱員](../Page/印度南方鐵路局.md "wikilink")。

## 參考資料

## 外部連結

  -
[Category:印度運動員](../Category/印度運動員.md "wikilink")
[Category:1986年亞洲運動會金牌得主](../Category/1986年亞洲運動會金牌得主.md "wikilink")
[Category:1986年亞洲運動會銀牌得主](../Category/1986年亞洲運動會銀牌得主.md "wikilink")
[Category:1990年亞洲運動會銀牌得主](../Category/1990年亞洲運動會銀牌得主.md "wikilink")
[Category:1994年亞洲運動會銀牌得主](../Category/1994年亞洲運動會銀牌得主.md "wikilink")
[Category:亞洲運動會田徑獎牌得主](../Category/亞洲運動會田徑獎牌得主.md "wikilink")
[Category:1984年夏季奧林匹克運動會田徑運動員](../Category/1984年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:1988年夏季奧林匹克運動會田徑運動員](../Category/1988年夏季奧林匹克運動會田徑運動員.md "wikilink")