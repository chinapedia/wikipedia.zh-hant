**自貢龍屬**（[學名](../Page/學名.md "wikilink")：**）是[蜥腳下目的一](../Page/蜥腳下目.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，化石發現於[中國](../Page/中國.md "wikilink")[四川省](../Page/四川省.md "wikilink")[自貢市的](../Page/自貢市.md "wikilink")[沙溪廟地層](../Page/沙溪廟地層.md "wikilink")，年代為[侏羅紀中至晚期](../Page/侏羅紀.md "wikilink")。由於對中國的侏羅紀蜥腳下目所知有限，對自貢龍的分析也有困難，有些學者提出自貢龍應屬於[峨嵋龍或](../Page/峨嵋龍.md "wikilink")[馬門溪龍](../Page/馬門溪龍.md "wikilink")，有些則認為牠們是獨立的屬。

## 歷史及分類

自貢龍的[正模標本](../Page/正模標本.md "wikilink")（編號CV
00261）包括一根部份下頜、上頜及[基枕骨](../Page/基枕骨.md "wikilink")。其他來自於身體各部份的其他[骨骼](../Page/骨骼.md "wikilink")，同樣被編入自貢龍屬中。[侯連海等人認為牠很像](../Page/侯連海.md "wikilink")[峨嵋龍](../Page/峨嵋龍.md "wikilink")，但在[脊椎上有所差異](../Page/脊椎.md "wikilink")\[1\]。早期的研究認為牠是屬於[腕龍科](../Page/腕龍科.md "wikilink")\[2\]\[3\]\[4\]。

於1980年代，[中國的](../Page/中國.md "wikilink")[蜥腳下目分類出現了很多混淆的情況](../Page/蜥腳下目.md "wikilink")。在1983年，[董枝明](../Page/董枝明.md "wikilink")、[張奕宏及](../Page/張奕宏.md "wikilink")[周世武從與斧溪自貢龍](../Page/周世武.md "wikilink")（*Z.
fuxiensis*）的不同化石命名了[斧溪峨嵋龍](../Page/斧溪峨嵋龍.md "wikilink")（*Omeisaurus
fuxiensis*），但後來卻指牠們是相同動物\[5\]。之後，自貢龍被認為是屬於峨嵋龍\[6\]，或可能是[榮縣峨嵋龍](../Page/榮縣峨嵋龍.md "wikilink")（*O.
junghsiensis*）的[異名](../Page/異名.md "wikilink")\[7\]。在1990年代中期，學者們改將自貢龍歸類於[馬門溪龍](../Page/馬門溪龍.md "wikilink")。他們指出自貢龍的化石是來自在峨嵋龍與馬門溪龍之間的[地層帶](../Page/地層帶.md "wikilink")，但特徵則與馬門溪龍最為相似。而且兩者的脊椎[神經棘都有輕微的分叉](../Page/神經棘.md "wikilink")，這個特點卻在峨嵋龍中看不到。於是他們將自貢龍重新命名為[斧溪馬門溪龍](../Page/斧溪馬門溪龍.md "wikilink")（*Mamenchisaurus
fuxiensis*）\[8\]。這個主張在很多有關蜥腳下目的文獻獲得贊同\[9\]，但仍有一些學者指出自貢龍是獨立的屬\[10\]。

## 古生物學

無論自貢龍被分類為類似[馬門溪龍或](../Page/馬門溪龍.md "wikilink")[峨嵋龍的](../Page/峨嵋龍.md "wikilink")[蜥腳下目](../Page/蜥腳下目.md "wikilink")，自貢龍是大型的四足[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，並有著長的[頸部](../Page/頸部.md "wikilink")\[11\]。自貢龍被認為是中至大型的蜥腳下目\[12\]，長約15米\[13\]。

## 參考

[Category:真蜥腳類](../Category/真蜥腳類.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.
12.
13.