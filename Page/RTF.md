**富文本格式**（**Rich Text
Format**）即**RTF格式**，又称**多文本格式**，是由[微软公司开发的跨平台](../Page/微软.md "wikilink")[文档格式](../Page/文档.md "wikilink")。大多数的[文字处理软件都能读取和保存RTF文档](../Page/文字处理.md "wikilink")。

## RTF文档示例

以下RTF代码：

`{\rtf1\ansi`
`Hello!\par`
`This is some {\b bold} text.\par`
`}`

在文字处理软件中将显示为如下效果：

> Hello\!
> This is some **bold** text.

反斜线符号（`\`）标志着RTF控制代码开始。代码`\par`表示开始新的一行，代码`\b`将文本以粗体显示。花括号`{`和`}`定义一个群组。上述例子中使用了一个群组来限制代码`\b`的作用范围。合法的RTF文档是一个以代码`\rtf`开始的群组。

## RTF的標準

作為微軟公司的標準文件，早期外間需要向微軟付款數十美元，才能購買一本薄薄的RTF標準文件。不過随着採用RTF格式標準的軟件愈來愈多，RTF格式也愈來愈普遍，微軟公司就把標準文件公開，放在網上供開發者下載。現時可供下載的各個RTF版本標準文件如下：

  - [RTF 1.9.1
    specification](http://www.microsoft.com/downloads/details.aspx?familyid=DD422B8D-FF06-4207-B476-6B5396A18A2B&displaylang=en)
    (March 2008)
  - [RTF 1.8
    specification](http://www.microsoft.com/downloads/details.aspx?FamilyID=ac57de32-17f0-4b46-9e4e-467ef9bc5540&displaylang=en)
    (April 2004)
  - [RTF 1.6
    specification](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnrtfspec/html/rtfspec.asp)
    (May 1999)
  - [RTF 1.5 specification](http://www.biblioscape.com/rtf15_spec.htm)
    (April 1997)
  - [RTF 1.3 and 1.5 specifications](http://www.snake.net/software/RTF/)
  - [RTF 1.0
    specification](http://latex2rtf.sourceforge.net/RTF-Spec-1.0.txt)
    (June 1992)

## 支持RTF的软件

  - [WordPad](../Page/WordPad.md "wikilink")（[Microsoft
    Windows](../Page/Microsoft_Windows.md "wikilink")）/
    [文本编辑](../Page/文本编辑.md "wikilink")（[Mac
    OS](../Page/Mac_OS.md "wikilink")）
  - [Microsoft Word](../Page/Microsoft_Word.md "wikilink")
  - [Apache OpenOffice](../Page/Apache_OpenOffice.md "wikilink") /
    [LibreOffice](../Page/LibreOffice.md "wikilink")
  - [WPS Office](../Page/WPS_Office.md "wikilink")
  - [EIOffice](../Page/EIOffice.md "wikilink")

[Category:文件格式](../Category/文件格式.md "wikilink")