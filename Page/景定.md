**景定**（1260年－1264年）是[宋理宗趙昀的最後一個年号](../Page/宋理宗.md "wikilink")。[南宋使用这个年号共](../Page/南宋.md "wikilink")5年。

景定五年十月[宋度宗即位沿用](../Page/宋度宗.md "wikilink")\[1\]。

## 纪年

| 景定                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1260年                          | 1261年                          | 1262年                          | 1263年                          | 1264年                          |
| [干支](../Page/干支纪年.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [紹隆](../Page/紹隆.md "wikilink")（1258年至1278年）：[陳朝](../Page/陳朝_\(越南\).md "wikilink")—[陳聖宗陳晃之年號](../Page/陳聖宗.md "wikilink")
      - [正元](../Page/正元_\(後深草天皇\).md "wikilink")（1259年三月二十六日至1260年四月十三日）：日本後深草天皇與[龜山天皇年號](../Page/龜山天皇.md "wikilink")
      - [中統](../Page/中统_\(年号\).md "wikilink")（1260年五月—1264年八月）：[元](../Page/元朝.md "wikilink")—元世祖[忽必烈之年號](../Page/忽必烈.md "wikilink")
      - [至元](../Page/至元_\(元世祖\).md "wikilink")（1264年八月－1294年十二月）：元—元世祖忽必烈之年號
      - [文應](../Page/文應.md "wikilink")（1260年四月十三日至1261年二月二十日）：日本[龜山天皇年號](../Page/龜山天皇.md "wikilink")
      - [弘長](../Page/弘長.md "wikilink")（1261年二月二十日至1264年二月二十八日）：日本[龜山天皇](../Page/龜山天皇.md "wikilink")
      - [文永](../Page/文永.md "wikilink")（1264年二月廿八至1275年四月廿五）：日本龜山天皇與[後宇多天皇年號](../Page/後宇多天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:南宋年号](../Category/南宋年号.md "wikilink")
[Category:13世纪中国年号](../Category/13世纪中国年号.md "wikilink")
[Category:1260年代中国政治](../Category/1260年代中国政治.md "wikilink")

1.