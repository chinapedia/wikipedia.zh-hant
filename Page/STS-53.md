****是历史上第五十二次航天飞机任务，也是[发现号航天飞机的第十五次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[大卫·沃克](../Page/大卫·沃克.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[罗伯特·卡巴纳](../Page/罗伯特·卡巴纳.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[圭恩·布鲁福德](../Page/圭恩·布鲁福德.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[詹姆斯·沃斯](../Page/詹姆斯·沃斯.md "wikilink")**（，曾执行、、、、、以及任务），任务专家
  - **[迈克尔·克利福德](../Page/迈克尔·克利福德.md "wikilink")**（，曾执行、以及任务），任务专家

## 參考資料

  - [Mission
    Summary](http://science.ksc.nasa.gov/shuttle/missions/sts-53/mission-sts-53.html)

[Category:1992年佛罗里达州](../Category/1992年佛罗里达州.md "wikilink")
[Category:1992年科學](../Category/1992年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1992年12月](../Category/1992年12月.md "wikilink")