[Blair_G8_July7th05.jpg](https://zh.wikipedia.org/wiki/File:Blair_G8_July7th05.jpg "fig:Blair_G8_July7th05.jpg")
**第31届八国集团首脑会议**（**31rd G8
summit**）于2005年7月6日至8日在[英国](../Page/英国.md "wikilink")[苏格兰](../Page/苏格兰.md "wikilink")[格伦伊格尔斯举行](../Page/格伦伊格尔斯.md "wikilink")。

## 与会领导人

### 八国集团首脑

  -   - [总理](../Page/加拿大总理.md "wikilink")：[保罗·马丁](../Page/保罗·马丁.md "wikilink")

  -   - [总统](../Page/法国总统.md "wikilink")：[雅克·希拉克](../Page/雅克·希拉克.md "wikilink")

  -   - [总理](../Page/德国总理.md "wikilink")：[格哈德·施罗德](../Page/格哈德·施罗德.md "wikilink")

  -   - [总理](../Page/意大利总理.md "wikilink")：[西尔维奥·贝卢斯科尼](../Page/西尔维奥·贝卢斯科尼.md "wikilink")

  -   - [首相](../Page/日本首相.md "wikilink")：[小泉纯一郎](../Page/小泉纯一郎.md "wikilink")

  -   - [总统](../Page/俄罗斯总统.md "wikilink")：[弗拉基米尔·普京](../Page/弗拉基米尔·普京.md "wikilink")

  -   - [首相](../Page/英国首相.md "wikilink")：[托尼·布莱尔](../Page/托尼·布莱尔.md "wikilink")

  -   - [总统](../Page/美国总统.md "wikilink")：[乔治·W·布什](../Page/乔治·W·布什.md "wikilink")

  -   - [欧洲委员会主席](../Page/欧洲委员会主席.md "wikilink")：[若泽·曼努埃尔·杜朗·巴罗索](../Page/若泽·曼努埃尔·杜朗·巴罗索.md "wikilink")

### 其他领导人

  -   - [外交大臣](../Page/英国外交大臣.md "wikilink")：[杰克·斯特劳](../Page/杰克·斯特劳.md "wikilink")

  -   - [欧洲议会议长](../Page/欧洲议会议长.md "wikilink")：[何塞普·博雷利](../Page/何塞普·博雷利.md "wikilink")

  -   - [总统](../Page/墨西哥总统.md "wikilink")：[比森特·福克斯](../Page/比森特·福克斯.md "wikilink")

  -   - [总统](../Page/巴西总统.md "wikilink")：[路易斯·伊纳西奥·卢拉·达席尔瓦](../Page/路易斯·伊纳西奥·卢拉·达席尔瓦.md "wikilink")

  -   - [主席](../Page/中华人民共和国主席.md "wikilink")：[胡锦涛](../Page/胡锦涛.md "wikilink")

  -   - [总理](../Page/印度总理.md "wikilink")：[曼莫汉·辛格](../Page/曼莫汉·辛格.md "wikilink")

  - [Flag_of_South_Africa.svg](https://zh.wikipedia.org/wiki/File:Flag_of_South_Africa.svg "fig:Flag_of_South_Africa.svg")[南非](../Page/南非.md "wikilink")

      - [总统](../Page/南非总统.md "wikilink")：[塔博·姆贝基](../Page/塔博·姆贝基.md "wikilink")

  - [Flag_of_the_United_Nations.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Nations.svg "fig:Flag_of_the_United_Nations.svg")[联合国](../Page/联合国.md "wikilink")

      - [秘书长](../Page/联合国秘书长.md "wikilink")：[科菲·安南](../Page/科菲·安南.md "wikilink")

  - [世界银行](../Page/世界银行.md "wikilink")

      - [行长](../Page/世界银行行长.md "wikilink")：[保罗·沃尔福威茨](../Page/保罗·沃尔福威茨.md "wikilink")

  - [国际货币基金组织](../Page/国际货币基金组织.md "wikilink")

      - [总裁](../Page/国际货币基金组织总裁.md "wikilink")：[罗德里戈·拉托](../Page/罗德里戈·拉托.md "wikilink")

## 外部链接

  - [官方网站](http://webarchive.loc.gov/all/20080913235039/http%3A//www.g8.gov.uk/servlet/Front?pagename%3DOpenMarket/Xcelerate/ShowPage%26c%3DPage%26cid%3D1078995902703)

[Category:八国集团首脑会议](../Category/八国集团首脑会议.md "wikilink")
[Category:2005年苏格兰](../Category/2005年苏格兰.md "wikilink")