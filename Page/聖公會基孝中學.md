[Lam_Tin_Kei_Hau_Secondary_School_SKH_New_Look_1.jpg](https://zh.wikipedia.org/wiki/File:Lam_Tin_Kei_Hau_Secondary_School_SKH_New_Look_1.jpg "fig:Lam_Tin_Kei_Hau_Secondary_School_SKH_New_Look_1.jpg")
[SKH_Kei_Hau_Secondary_School_2.jpg](https://zh.wikipedia.org/wiki/File:SKH_Kei_Hau_Secondary_School_2.jpg "fig:SKH_Kei_Hau_Secondary_School_2.jpg")活動\]\]

**聖公會基孝中學**（）是[香港一間以](../Page/香港.md "wikilink")[中文為教學語言的津貼中學](../Page/中文.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[藍田啟田道](../Page/藍田_\(香港\).md "wikilink")5號，鄰近[匯景花園](../Page/匯景花園.md "wikilink")。該校為[聖公宗（香港）中學委員會有限公司轄下一所文法中學](../Page/聖公宗（香港）中學委員會有限公司.md "wikilink")，於1972年創辦。佔地6,800平方米，男女生俱收，並設有家長教師會、校友會及學生會。「基」代表基督之愛，「孝」乃表揚孝道。該校力求學生在德、智、體、群、美、靈各方面臻於完善。現有校舍於1976年落成，而新翼則於2000年落成。

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[藍田站A出口](../Page/藍田站.md "wikilink")（步行約6分鐘）

## 設施

所有標準課室及特別室均配備電腦、投影機、銀幕。所有課室、特別室及禮堂裝置冷氣系統，另設有學生休憩區包括：飯堂、籃球場、花園、閱讀角等。

  - 課室35間
  - 實驗室4間
  - 多媒體學習中心2間
  - 視藝室1間
  - 圖書館1間
  - 語言學習室1間
  - 音樂室間
  - 學生活動中心1間
  - 多媒體製作室1間
  - 禮堂1個
  - 教員室1間
  - 教員室/教員休息室1間
  - 教師資源中心1間
  - 伺服器控制室1間
  - 學生會室1間
  - 學生輔導室1間
  - 會議室1間
  - 社工室1間
  - 創新科技室1間
  - 宗教輔導室1間
  - 校長宿舍1間

## 學生會

[學生會由該校學生一人一票選出](../Page/學生會.md "wikilink")，並按照《學生會會章》工作。該會會負責統籌校內各學生組織之課外活動，其轄下有各學科學會及興趣小組達20多個。該會希望透過舉辦不同活動，培養同學各方面的才能，減輕同學學習壓力。該會本著為同學服務的理念，希望令同學更團結，更有凝聚力，而該會亦負責促進同學福利的工作。該會於2010年通過修改會章，廢除規定校內領袖須由中六生出任的有關條文，讓不同年級學生都能夠「有權選會長」。

## 外部連結

  - [基孝訊息庫](http://www.keihau.edu.hk)
  - [聖公會基孝中學校友會FACEBOOK](https://zh-hk.facebook.com/%E8%81%96%E5%85%AC%E6%9C%83%E5%9F%BA%E5%AD%9D%E4%B8%AD%E5%AD%B8%E6%A0%A1%E5%8F%8B%E6%9C%83-249677838483540/)
  - [OpenSchool聖公會基孝中學介紹](https://www.openschool.hk/school/details/%E4%B8%AD%E5%AD%B8/%E8%81%96%E5%85%AC%E6%9C%83%E5%9F%BA%E5%AD%9D%E4%B8%AD%E5%AD%B8)

[基](../Category/香港聖公會中學.md "wikilink") [Category:藍田
(香港)](../Category/藍田_\(香港\).md "wikilink")
[S聖公會](../Category/觀塘區中學.md "wikilink")