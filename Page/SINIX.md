**SINIX**（之後改稱**Reliant
UNIX**）是[西門子利多富資訊系統公司所提出的](../Page/西門子利多富資訊系統.md "wikilink")[UNIX](../Page/UNIX.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，該作業系統以SINIX為名的最末一個版本為1995年發表的5.43版。

為了讓[X/Open公司能合乎擁有UNIX這個註冊商標的需要SINIX必須進行改名](../Page/X/Open.md "wikilink")，因此SINIX從5.44版開始（也包含5.44版）由[富士通-西門子電腦公司改以](../Page/富士通-西門子電腦公司.md "wikilink")**Reliant
UNIX**之名接續，而最末的一版為5.45版。

過去最原初的SINIX作業系統，其實是從一套[Xenix作業系統](../Page/Xenix.md "wikilink")修改而來，之後的版本則是改以[System
V為基礎所發展成](../Page/System_V.md "wikilink")，並針對不同型款的電腦而有不同的對應版本。

例如用在SNI公司的RM-200型、RM-300型、RM-400型、RM-600型的系列伺服器（也稱：伺服器）上時，所對應的分別是SINIX-N、SINIX-O、SINIX-P、SINIX-Y。此外採行[Intel
80386處理器的PC](../Page/Intel_80386.md "wikilink")-MXi電腦則是用SINIX-L作業系統。

另外，有些版本的SINIX能讓用戶實效模擬（Emulate）出多種不同架構特性、不同版本取向的UNIX，此也被稱為“**[universes](../Page/universes.md "wikilink")，複數**”，SINIX可模擬出System
V.3、[UNIX System III](../Page/UNIX_System_III.md "wikilink")（有時簡稱：System
3）或[BSD等](../Page/BSD.md "wikilink")[類UNIX作業系統的特性表現](../Page/Unix-like.md "wikilink")，且每種特性的作業系統（也稱“**universe，單數**”）都能有其自屬的命令集（command
set）、[函式庫](../Page/库.md "wikilink")（libraries，也稱：庫）以及[含括檔](../Page/含括檔.md "wikilink")（header
files）等。

## 附註

1.  – Xenix作業系統也有多種不同的硬體對應版本，SINIX所用的那一套Xenix是專供在採行[Intel
    80186處理器的電腦上所執行的版本](../Page/Intel_80186.md "wikilink")。

2.  – 該系列伺服器使用[MIPS架構的處理器](../Page/MIPS科技公司.md "wikilink")。

## 外部連結

  - [Reliant
    UNIX/SINIX說明手冊](http://manuals.fujitsu-siemens.com/softbooks/unix/us/relux_us.htm)
    - **（英文）**
  - [西門子商務服務（Siemens Business
    Services）](https://web.archive.org/web/20050503180415/http://serviceportal.sbs.de/)
    - SINIX作業系統的補強程式（也稱：補丁）及相關支援**（德文）**

[Category:System V](../Category/System_V.md "wikilink")
[Category:Unix](../Category/Unix.md "wikilink")