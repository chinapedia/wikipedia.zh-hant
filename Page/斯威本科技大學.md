<table>
<caption><strong>斯威本理工大学</strong></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Logoswin.gif" title="fig:Logoswin.gif">Logoswin.gif</a><br />
斯威本理工大学</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/校訓.md" title="wikilink">校訓</a>：<em>Factum per Litteras</em> ("Achievement through learning")<br />
</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>現任校長</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>學校類型</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>大學建立時間</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>所在地</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>校園環境</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>运动队</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>創辦人</p></td>
</tr>
</tbody>
</table>

**斯威本理工大学**（）是一所位於[澳洲](../Page/澳洲.md "wikilink")[維多利亞州](../Page/維多利亞州.md "wikilink")[墨爾本市的公立](../Page/墨爾本市.md "wikilink")[綜合大學](../Page/綜合大學.md "wikilink")，也是[澳大利亞大學之中唯一被](../Page/澳大利亞大學列表.md "wikilink")**歐洲創新大學聯合會**\[1\]邀請的成員，亦是澳洲今日同時設有技職學院（TAFE）的大學之一。斯威本理工大學的研究產出許多商業公司\[2\]及年度校際盛事為《創業\[3\]盃》比賽。\[4\]
斯威本理工大學的教學特色為[新加坡人力部](../Page/新加坡人力部.md "wikilink")\[5\]認可的工作綜合學習(Work
Integrated Learning)導向的學習成果。\[6\]\[7\]\[8\]\[9\]
斯威本理工大學表現優異的設計學院\[10\]\[11\]\[12\]的學生，及商學院\[13\]\[14\]\[15\]\[16\]的創業及創新碩士班的學生，和職場治療專業的學生，可以申請親自前往參加聯盟校美國[史丹佛大學的ME](../Page/史丹佛大學.md "wikilink")310-SUGAR的[矽谷的產品開發課程](../Page/矽谷.md "wikilink")\[17\]\[18\]，以及瑞士[日內瓦的](../Page/日內瓦.md "wikilink")[歐洲核子研究組織的IdeaSquare的知識轉移的商品開發課程](../Page/歐洲核子研究組織.md "wikilink")。\[19\]\[20\]\[21\]
斯威本理工大學的微[光子學研究具有國際頂尖的水準](../Page/光子學.md "wikilink")，\[22\]\[23\]
同時也是[電機電子工程師學會的學生支部的會員](../Page/電機電子工程師學會.md "wikilink")。\[24\]
斯威本理工大學累積的學術背景\[25\]所培養的博士生是全球少數參加過[計算機協會](../Page/計算機協會.md "wikilink")[人工智慧聯盟的大學](../Page/人工智慧.md "wikilink")，\[26\]並且和[聯邦科學與工業研究組織](../Page/聯邦科學與工業研究組織.md "wikilink")\[27\]合作，是第一個在美國[矽谷成立代表處的](../Page/矽谷.md "wikilink")[澳洲大學](../Page/澳洲.md "wikilink")。\[28\]\[29\]
斯威本理工大學的學術評定方式是採用接近英國學術研究導向的嚴格的合格與否的制度，\[30\]著名的研究成果於雜誌定期線上發表，\[31\]另外斯威本大學的科研應用事例也定期予以發表。\[32\]

## 學校概況

[喬治·斯文本在](../Page/喬治·斯文本.md "wikilink")1908年于東郊的技術學院的基礎上，建立了斯威本理工大學的前身──斯威本學院。1913年，這位有顯著名望的的人將學校更改名稱為斯威本理工學院。

在1976年至1978年期間，[維多利亞州政府變革了](../Page/維多利亞州政府.md "wikilink")[學位體制](../Page/學位.md "wikilink")，斯威本理工大學奉-{准}-授予[學士學位課程](../Page/學士學位.md "wikilink")，遂於1981年起正式頒發學士學位：於1982年-{升}-格為以理工為主的綜合型大學。於1992年，斯威本理工大学正式建立成為一所[綜合大學](../Page/綜合大學.md "wikilink")。

斯威本理工大学在墨爾本有4個主要校區，另外在[馬來西亞](../Page/馬來西亞.md "wikilink")[砂拉越設置一所國際分校](../Page/砂拉越.md "wikilink")。斯威本理工大学的發展在[澳大利亞教育界趨于獨立](../Page/澳大利亞教育.md "wikilink")，因為校方趨於與[歐洲大學交流和合作](../Page/歐洲.md "wikilink")，近年來斯威本理工大學也積極地投入\[33\]\[34\]革新教育方式\[35\]\[36\]\[37\]及與澳洲本地\[38\]和其他國際知名大學的合作案中。\[39\]

[法庭心理學研究學者](../Page/法庭心理學.md "wikilink") Stephane Michel Shepherd
於2015-2016學季為[傅爾布萊特計畫](../Page/傅爾布萊特計畫.md "wikilink")[獎學金得主](../Page/獎學金.md "wikilink")。\[40\]\[41\]

Dimity Hawkins共同创立国际废除核武器运动，在2017年获得了诺贝尔和平奖。\[42\]

## 大學排名

2016年澳大利亚政府和 “最佳大学指南 （The Good Universities Guide
2017）”先后将斯威本评选为一流的大学，研究实力获5星的3所墨尔本地区的大学之一，也是澳大利亚研究水平高于世界平均水平的10所大学之一(Thomson
ISI 2016)，55个研究领域的引用量等于或高于澳大利亚平均水平(Thomson ISI
2016)。2017年，斯威本大學的商法學院被eduniversal評3Palmes(Top200)，\[43\]
同時也是Australian Business Deans Council的成員校。\[44\]\[45\]
從2018年，開始和[KPMG合作提供課程](../Page/KPMG.md "wikilink")，\[46\]
職場評價及線上商管/其他課程也處於世界排名前列，\[47\]另外在2016/2017年的[Leiden世界大學排名為Top](../Page/Leiden_University.md "wikilink")
200。\[48\]

## 學術研究及合作

斯威本理工大學的研究成果\[49\]和科研[實驗室](../Page/實驗室.md "wikilink")，分布於數個不同的校區內。正如一些正在發展的大學一樣，斯威本理工大学正努力采取一些相應策略來取得有限的土地，好發展研究實力。斯威本理工大學力求在全方位\[50\]及科學技術方面具有堅韌的基礎，包括智慧服務（公司位於澳洲科技園區\[51\]）\[52\]、[無線電學](../Page/無線電.md "wikilink")(進階互聯網架構-[物聯網](../Page/物聯網.md "wikilink"))\[53\]\[54\]、[量子電腦](../Page/量子電腦.md "wikilink")[物理](../Page/物理.md "wikilink")[材料](../Page/材料.md "wikilink")\[55\]、新[電池開發](../Page/電池.md "wikilink")\[56\]\[57\]、[電信](../Page/電信.md "wikilink")\[58\]、[天文學](../Page/天文學.md "wikilink")\[59\]\[60\]\[61\]、[天體物理學](../Page/天體物理學.md "wikilink")\[62\]\[63\]、[可持續性](../Page/可持續性.md "wikilink")[基礎設施](../Page/基礎設施.md "wikilink")\[64\]、數位技術創新\[65\](包括[軟體研發](../Page/軟體.md "wikilink"))、虛擬服務\[66\]、[統一塑模語言](../Page/統一塑模語言.md "wikilink")\[67\]、[電子遊戲開發](../Page/電子遊戲.md "wikilink")
\[68\]\[69\]\[70\]\[71\]\[72\]、[工業4.0](../Page/工業4.0.md "wikilink")\[73\]、[工學](../Page/工學.md "wikilink")
\[74\]\[75\]、[工業設計](../Page/工業設計.md "wikilink")\[76\]\[77\]、[材料科學](../Page/材料科學.md "wikilink")\[78\]\[79\]\[80\]、[冶金學](../Page/冶金學.md "wikilink")\[81\]、[光子學和](../Page/光子學.md "wikilink")[光學](../Page/光學.md "wikilink")\[82\]\[83\]、[量子光學](../Page/量子光學.md "wikilink")\[84\]\[85\]、[奈米技術](../Page/奈米技術.md "wikilink")\[86\]、[創新](../Page/創新.md "wikilink")\[87\]\[88\]、[設計](../Page/設計.md "wikilink")\[89\]\[90\]、航空學\[91\]\[92\]\[93\]、[海洋學](../Page/海洋學.md "wikilink")\[94\]、[社會學](../Page/社會學.md "wikilink")\[95\]\[96\]、數位健康科技\[97\]、[生物醫學工程](../Page/生物醫學工程.md "wikilink")\[98\]、[生物工程學](../Page/生物工程學.md "wikilink")\[99\]、醫療材料學\[100\]、[醫療器械](../Page/醫療器械.md "wikilink")
學\[101\]、[營養學](../Page/營養學.md "wikilink")\[102\]、[聯合國](../Page/聯合國.md "wikilink")[永續發展目標](../Page/永續發展目標.md "wikilink")\[103\]、[節約能源](../Page/節約能源.md "wikilink")\[104\]、[法學](../Page/法學.md "wikilink")\[105\]\[106\]、[心理學](../Page/心理學.md "wikilink")\[107\]\[108\]\[109\]、分子模擬學\[110\]、[醫學](../Page/醫學.md "wikilink")\[111\]、[神經科學](../Page/神經科學.md "wikilink")\[112\]、[商學](../Page/商學.md "wikilink")\[113\]\[114\]\[115\]\[116\]和[數據科學](../Page/數據科學.md "wikilink")\[117\]。就如許多典型的歐洲大陸的研究型大學，斯威本大學也有許多產官學合作的研究研發中心。\[118\]\[119\]\[120\]\[121\]\[122\]

大學的*天體物理中心*和*超級計算研發中心*，亦是澳洲同業中的指導中心研究小組的駐地。斯威本大學在[天文學研究領域和美國](../Page/天文學.md "wikilink")[加州理工學院有著長久的合作關係](../Page/加州理工學院.md "wikilink")。\[123\]

斯威本在[藝術設計](../Page/藝術設計.md "wikilink")\[124\]\[125\]\[126\]方面，在全澳居于領先的地位，學校培養過多位設計大師及[漫畫家等](../Page/漫畫家.md "wikilink")。2000年開始，斯威本理工大学開始注重電影科技設計方面的研究，而且設立了專業[實驗室](../Page/實驗室.md "wikilink")。
在2017年，大学畢業生的作品獲邀參加第74屆[威尼斯影展的競賽項目](../Page/威尼斯影展.md "wikilink")。\[127\]斯威本理工大學是全球首位和[Adobe系統公司進行策略合作](../Page/奧多比系統公司.md "wikilink")，\[128\]藝術、媒體、商業、溝通及ICT領域的學生畢業前擁有相關的數位技術Know-how。\[129\]

## 國際合作及校際連結

以一個中型規模科研大學的角度而言，斯威本理工大學的國際化程度\[130\]\[131\]\[132\]\[133\](包括國際獎學金\[134\])及國際交流合作享譽盛名，包括下列著名學術機構：

  - :
    [矽谷](../Page/矽谷.md "wikilink")\[135\]、[哈佛大學](../Page/哈佛大學.md "wikilink")（天文學領域）\[136\]\[137\]、[帕羅奧多研究中心](../Page/帕羅奧多研究中心.md "wikilink")(科技)\[138\]、[加州理工學院](../Page/加州理工學院.md "wikilink")(天文學)\[139\]、[加州大學聖塔克魯茲分校](../Page/加州大學聖塔克魯茲分校.md "wikilink")\[140\]\[141\]、[范德堡大學](../Page/范德堡大學.md "wikilink")(腦科學)\[142\]、[東北大學](../Page/東北大學_\(美國\).md "wikilink")(商學)\[143\]\[144\]\[145\]
    、[辛辛納提大學](../Page/辛辛納提大學.md "wikilink")\[146\]、[普渡大學](../Page/普渡大學.md "wikilink")\[147\]\[148\]\[149\]\[150\]、[伊利諾大學厄巴納-香檳分校](../Page/伊利諾大學厄巴納-香檳分校.md "wikilink")(媒體)\[151\]\[152\]、[塔夫茲大學](../Page/塔夫茲大學.md "wikilink")(科研合作聯盟)\[153\]、[南加州大學](../Page/南加州大學.md "wikilink")(網際網路)\[154\]\[155\]、紐約州立大學布拉克伯特學院\[156\]、[卓克索大學](../Page/卓克索大學.md "wikilink")\[157\]、[舊金山州立大學](../Page/舊金山州立大學.md "wikilink")\[158\]、[貝勒大學](../Page/貝勒大學.md "wikilink")(商學)\[159\]、貝拉明大學\[160\]\[161\]\[162\]\[163\]\[164\]、[加州理工州立大學](../Page/加州理工州立大學.md "wikilink")(工程和人文)\[165\]、[壬色列理工學院](../Page/壬色列理工學院.md "wikilink")(管理學院)\[166\]
    、[聖路易斯華盛頓大學](../Page/聖路易斯華盛頓大學.md "wikilink")(心理學)\[167\]、[加州大學柏克萊分校](../Page/加州大學柏克萊分校.md "wikilink")(天文學)\[168\]、[加州大學洛杉磯分校](../Page/加州大學洛杉磯分校.md "wikilink")\[169\]、[康乃爾大學](../Page/康乃爾大學.md "wikilink")(醫學)\[170\]、[麻省理工學院](../Page/麻省理工學院.md "wikilink")(關於獲得最新的國際的頂尖關於[資訊系統的研究成果](../Page/資訊系統.md "wikilink"))\[171\]\[172\]和[史丹佛大學](../Page/史丹佛大學.md "wikilink")(產品設計領域)\[173\]\[174\]。

  - :
    巴黎規範管理及法律研究中心(PRIMAL)\[175\]、博士雙聯學制\[176\]、ESSCA管理學院\[177\]、[南巴黎大學](../Page/南巴黎大學.md "wikilink")\[178\]、貢比涅技術大學\[179\]、諾歐商學院\[180\]。

  - :
    歐洲管理和科技學院（ESMT）（會計）\[181\]、夫朗和斐應用研究促進協會（工業4.0）\[182\]、[慕尼黑工業大學](../Page/慕尼黑工業大學.md "wikilink")\[183\]、[斯圖加特大學](../Page/斯圖加特大學.md "wikilink")（製造）\[184\]、[明斯特大學](../Page/明斯特大學.md "wikilink")、[多特蒙德工業大學](../Page/多特蒙德工業大學.md "wikilink")、ESB商學院\[185\]、HRW應用大學\[186\]。

  - :
    [大阪大學](../Page/大阪大學.md "wikilink")（工程）\[187\]\[188\]、[東京工業大學](../Page/東京工業大學.md "wikilink")（奈米科技）\[189\]、[東京理科大學](../Page/東京理科大學.md "wikilink")（光學）\[190\]、[岡山大學](../Page/岡山大學.md "wikilink")\[191\]、[京都工藝纖維大學](../Page/京都工藝纖維大學.md "wikilink")（設計）\[192\]、[明治大學](../Page/明治大學.md "wikilink")\[193\]、[菲利斯女學院大學](../Page/菲利斯女學院大學.md "wikilink")\[194\]、[中央大學](../Page/中央大學_\(日本\).md "wikilink")\[195\]、[青山學院大學](../Page/青山學院大學.md "wikilink")\[196\]、[國際教養大學](../Page/國際教養大學.md "wikilink")\[197\]、[大阪工業大學](../Page/大阪工業大學.md "wikilink")、[關西外國語大學](../Page/關西外國語大學.md "wikilink")。

  - : [印度理工學院馬德拉斯分校](../Page/印度理工學院.md "wikilink")\[198\]\[199\]。

  - : [哥本哈根商學院](../Page/哥本哈根商學院.md "wikilink")、\[200\]
    [丹麥技術大學](../Page/丹麥技術大學.md "wikilink")\[201\]\[202\]、丹麥皇家藝術學院\[203\]、[南丹麥大學](../Page/南丹麥大學.md "wikilink")\[204\]。

  - :
    [鹿特丹伊拉斯姆斯大學](../Page/鹿特丹伊拉斯姆斯大學.md "wikilink")（經濟）\[205\]、[特文特大學](../Page/特文特大學.md "wikilink")。\[206\]

  - : [米蘭理工大學](../Page/米蘭理工大學.md "wikilink")、\[207\]聖心天主教大學\[208\]。

  - : [南洋理工大學](../Page/南洋理工大學.md "wikilink")。\[209\]\[210\]

  - :
    [KAIST](../Page/KAIST.md "wikilink")\[211\]\[212\]\[213\]、[高麗大學](../Page/高麗大學.md "wikilink")\[214\]、[延世大學](../Page/延世大學.md "wikilink")（設計）
    \[215\]\[216\]、[成均館大學](../Page/成均館大學.md "wikilink")\[217\]、[漢陽大學及](../Page/漢陽大學.md "wikilink")[釜山大學](../Page/釜山大學.md "wikilink")\[218\]、[全北大學](../Page/全北大學.md "wikilink")\[219\]、[弘益大學](../Page/弘益大學.md "wikilink")\[220\]。

  - :
    [香港大學專業進修學院](../Page/香港大學專業進修學院.md "wikilink")（航空）\[221\]、[香港生產力促進局](../Page/香港生產力促進局.md "wikilink")（產品）\[222\]、[香港城市大學](../Page/香港城市大學.md "wikilink")（設計）\[223\]、[香港理工大學](../Page/香港理工大學.md "wikilink")（紡織和流行服飾）\[224\]\[225\]\[226\]、[香港浸會大學](../Page/香港浸會大學.md "wikilink")（商學）\[227\]\[228\]。

  - :
    [哥德堡大學](../Page/哥德堡大學.md "wikilink")（商學）\[229\]、瑞典工藝美術與設計大學(設計)\[230\]、延雪平大學\[231\]。

  - : 巴塞隆納自治大學\[232\]。

  - :
    [清華大學](../Page/清華大學.md "wikilink")（工程）\[233\]、[北京外國語大學](../Page/北京外國語大學.md "wikilink")（商學）\[234\]、[華中科技大學](../Page/華中科技大學.md "wikilink")\[235\]、[同濟大學](../Page/同濟大學.md "wikilink")（設計和商學等）\[236\]\[237\]\[238\]\[239\]、[山東大學](../Page/山東大學.md "wikilink")（製造）\[240\]、[中國地質大學](../Page/中國地質大學.md "wikilink")\[241\]、[深圳大學](../Page/深圳大學.md "wikilink")\[242\]。

  - :
    [歐洲核子研究組織](../Page/歐洲核子研究組織.md "wikilink")（設計）\[243\]、Fribourg管理學院\[244\]、琉森應用科學及藝術大學（設計）\[245\]。

  - :
    [阿爾托大學](../Page/阿爾托大學.md "wikilink")（設計）\[246\]、[拉彭蘭塔理工大學](../Page/拉彭蘭塔理工大學.md "wikilink")\[247\]。

  - : [塔林理工大學](../Page/塔林理工大學.md "wikilink")\[248\]。

  - : 歐洲創新聯盟大學論壇 (European Consortium of Innovative
    Universities)。\[249\]\[250\]

  - :
    [牛津大學](../Page/牛津大學.md "wikilink")（轉型理論和社會學）\[251\]\[252\]、[曼徹斯特大學](../Page/曼徹斯特大學.md "wikilink")（心理學）\[253\]、[倫敦藝術大學](../Page/倫敦藝術大學.md "wikilink")\[254\]、[斯特拉斯克萊德大學](../Page/斯特拉斯克萊德大學.md "wikilink")(商學等)\[255\]\[256\]、[薩里大學](../Page/薩里大學.md "wikilink")。

  - : [特拉維夫大學](../Page/特拉維夫大學.md "wikilink")（數據科學）。\[257\]\[258\]

  - :
    [麥基爾大學](../Page/麥基爾大學.md "wikilink")（都市規劃）\[259\]、[皇后大學](../Page/皇后大學.md "wikilink")（社會企業）\[260\]、[康考迪亞大學](../Page/康考迪亞大學.md "wikilink")。（商業資訊系統）\[261\]\[262\]

  - \[263\]\[264\] :
    [國立成功大學](../Page/國立成功大學.md "wikilink")(工學院)\[265\]\[266\]\[267\]、[國立台北大學](../Page/國立台北大學.md "wikilink")\[268\]、[國立臺灣海洋大學](../Page/國立臺灣海洋大學.md "wikilink")\[269\]、[國立雲林科技大學](../Page/國立雲林科技大學.md "wikilink")\[270\]、[元智大學](../Page/元智大學.md "wikilink")、[中原大學](../Page/中原大學.md "wikilink")、[大葉大學](../Page/大葉大學.md "wikilink")、[世新大學](../Page/世新大學.md "wikilink")、[聖約翰科技大學](../Page/聖約翰科技大學.md "wikilink")、[崑山科技大學](../Page/崑山科技大學.md "wikilink")。

  - : 利墨瑞克大學\[271\]。

  - 國際聯盟及學術組織 :
    聯盟校共同授予博士制\[272\]、另有提供可列計學分的學習旅遊\[273\]\[274\]\[275\]\[276\]及聯盟校學習\[277\]給有興趣參加的學生。斯威本理工大學是Universities
    Australia\[278\]\[279\]、ORCID\[280\]、AAPBS\[281\]\[282\]、UMAP\[283\]、ASAIHL\[284\]、AIPS\[285\]、AAL\[286\]\[287\]、Dexigner\[288\]、Cumulus\[289\]和Design
    Factory Network 的成員，\[290\] 也是CIS\[291\]\[292\]、ISIR
    \[293\]、PacSurf\[294\]、MelNet\[295\]、[亞歐會議的ASEFSU](../Page/亞歐會議.md "wikilink")21\[296\]\[297\]、[亞太經合組織](../Page/亞太經合組織.md "wikilink")(和GBPN合作\[298\])\[299\]和亞太創新論壇的成員。\[300\]
    OzGrav\[301\],FLEET\[302\],及CAASTRO\[303\]為斯威本理工大學所主辦/協辦。\[304\]

## 校區

斯威本在[維多利亞州擁有眾多的校區](../Page/維多利亞州.md "wikilink")，包括[山楂區](../Page/山楂區.md "wikilink")（Hawthorn）、[克羅伊登區](../Page/克羅伊登區.md "wikilink")、普拉仁（Prahan）、莉莉戴爾（Lilydale）、萬禔納（Wantirna）和Healesville。澳洲海外，斯威本亦在馬來西亞砂拉越的[古晉設置一個校區](../Page/古晉.md "wikilink")。其中山楂區為大學創立之初至今的校總區，距離墨爾本的中央商業區僅有7公里，大學的語言學校亦位于校總區內。經申請後\[305\]，Hawthorn校區提供學員24小時使用圖書館的服務。斯威本理工大學圖書館是OCLC\[306\]和CAUL的會員。\[307\]\[308\]

### 學校設施

[墨爾本軌道交通百合谷路線之格連菲利車站實際上位於在斯威本理工大學校區之內](../Page/墨爾本城市鐵路.md "wikilink")，亦讓該校實質上成為墨爾本所有大學中，其中一所擁有獨立火車站的大學。除此之外，斯威本理工大学在校園內設置有許多生活設施，包括圖書館、體育館、運動場、餐廳和咖啡館等。值得注意的是該校的餐廳，酒店的廚房工作人員幾乎來自于TAFE學院的西廚以及西點專業的學生，而管理者則出自飯店管理專業的在校生。設於校內的大學飯店的廣告牌中亦提及了這一點，成為校園內一個特點。

## 學院設置

  - 大學由「數位水族館」教學模式設備，\[309\]多個學院、科系及多個獨特的高度國際競爭力的研究中心組成，其中包括下列學院：\[310\]\[311\]\[312\]\[313\]

### 高等教育

  - [企業及商業管理學院](http://www.swin.edu.au/business)
  - [設計學院](http://www.hed.swin.edu.au/design)
  - [工程學院](https://web.archive.org/web/20080308123203/http://www.swinburne.edu.au/feis/)
  - [信息傳播技術學院（ICT）](http://www.swin.edu.au/ict/)
  - [社會科學學院](http://www.swinburne.edu.au/lss/)
  - [高等教育學院（Lilydale校區）](http://www.ld.swin.edu.au/)
  - 数学学院
  - 斯威本管理學院
  - 脑科研究学院
  - 天体物理学和计算机中心
  - 超速分光镜中心
  - 工业研究学院
  - 社会研究学院
  - 计算机人类互作用实验室

### 技職學院

  - [藝術管理設計學院](http://www.tafe.swin.edu.au/ahs/)
  - [商學院](http://www.tafe.swinburne.edu.au/business/)
  - [工程學院](http://www.tafe.swin.edu.au/eng/)
  - [社會學院](http://www.tafe.swin.edu.au/socsci/)

### 知名校友

斯威本理工大學校友名列全球Top
200及已任職的校友，\[314\][加州大學戴維斯分校](../Page/加州大學戴維斯分校.md "wikilink")、[塔夫茲大學](../Page/塔夫茲大學.md "wikilink")、[亞利桑那大學](../Page/亞利桑那大學.md "wikilink")、[科羅拉多大學波德分校](../Page/科羅拉多大學波德分校.md "wikilink")、[聖路易斯華盛頓大學](../Page/聖路易斯華盛頓大學.md "wikilink")、[印地安那大學布魯明頓校區](../Page/印地安那大學.md "wikilink")、[喬治華盛頓大學](../Page/喬治華盛頓大學.md "wikilink")、[凱斯西儲大學](../Page/凱斯西儲大學.md "wikilink")、[萊頓大學](../Page/萊頓大學.md "wikilink")、[赫爾辛基大學](../Page/赫爾辛基大學.md "wikilink")、[阿爾托大學](../Page/阿爾托大學.md "wikilink")、[卡迪夫大學](../Page/卡迪夫大學.md "wikilink")、[雷丁大學](../Page/雷丁大學.md "wikilink")、[中國人民大學](../Page/中國人民大學.md "wikilink")、[橫濱國立大學](../Page/橫濱國立大學.md "wikilink")、[浦項工科大學](../Page/浦項工科大學.md "wikilink")、[漢陽大學](../Page/漢陽大學.md "wikilink")、[慶熙大學](../Page/慶熙大學.md "wikilink")、[香港城市大學](../Page/香港城市大學.md "wikilink")。\[315\]

  - Kathlyn Ballard: 著名藝術家
  - Graeme Base: 著名兒童作家，漫畫家
  - Mark Bolton: 澳式足球大聯盟[AFL著名運動員](../Page/AFL.md "wikilink")
  - Kathy Bowlen: ABC頻道著名播音員及記者
  - Lara Cameron: 著名設計師
  - Alisa Camplin:
    空中技巧運動員，曾經贏得[2002年冬季奧運會空中技巧項目金牌](../Page/2002年冬季奧運會.md "wikilink")
  - Mary Covatta: 圖形設計家
  - Ray Crooke: 著名藝術家
  - Mark Korda:[澳洲航空公司行政管理者](../Page/澳洲航空.md "wikilink")
  - Michael Leunig: 著名漫畫家
  - Richard Lowenstein: 著名電影導演
  - Sidney Nolan: 著名藝術家
  - Philomena Tan: 著名作家
  - Sarah Watt: 著名電影導演及編劇

## 外部連結

  - [斯威本理工大學網站](http://www.swinburne.edu.au)
  - [斯威本理工大學Youtube頻道](http://www.youtube.com/user/swinburne/)
  - [斯威本理工大學創新園區](http://www.swinburne.edu.au/innovation-precinct/our-people/)
    \[316\]
  - [斯威本理工大學學生會 Swine 雜誌](https://swinemagazine.org/)\[317\]
  - [斯威本理工大學學生俱樂部](https://unione.swin.edu.au/clubs/search)

## 参考文献

<div class="references-small">

<references />

</div>

[S](../Category/墨爾本的大學.md "wikilink")
[Category:1908年創建的教育機構](../Category/1908年創建的教育機構.md "wikilink")
[Category:科技大學](../Category/科技大學.md "wikilink")

1.

2.

3.

4.

5.  <http://www.mom.gov.sg/passes-and-permits/training-employment-pass/list-of-acceptable-institutions#/view/Swinburne>
    University of Technology -Swinburne Institute of Technology

6.

7.

8.

9.

10. <https://researchbank.swinburne.edu.au/items/2aae1909-268b-4940-878e-68fffb83251d/1/>

11.

12. <https://www.dexigner.com/directory/detail/12813>

13.

14.

15.
16. <https://www.swinburne.edu.au/events/departments/current-students/2017/04/apple-store-leader-program.php>

17. <https://doresearch.stanford.edu/policies/research-policy-handbook/intellectual-property/inventions-patents-and-licensing>

18. <https://doresearch.stanford.edu/policies/research-policy-handbook/conflicts-commitment-and-interest/equity-acquisition-technology-licensing-and-distance-learning-agreements>

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30. <http://www.unco.edu/center-international-education/study-abroad/pdf/departing/grade-equivalencies.pdf>

31. <http://www.swinburne.edu.au/research/research-impact/>

32. <https://www.swinburne.edu.au/news/latest-news/news-rooms/venture-magazine/>

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48. <http://www.universityrankings.ch/results/Leiden/2016>

49. <https://researchbank.swinburne.edu.au/home.do>

50. <http://www.swinburne.edu.au/news/latest-news/2018/03/qs-ranks-swinburne-design-in-the-top-40.php>

51.

52.

53.

54.

55.

56.

57.

58. <http://www.swinburne.edu.au/news/latest-news/2018/03/swinburne-launches-11-m-cisco-networking-academy-teaching-facility.php>

59.

60.

61. <http://www.ifa.hawaii.edu/info/press-releases/HydrogenMolecules/>

62.

63. List_of_Collaborators_and_Institutions

64.

65.

66.

67.

68.

69.

70.

71.

72. <https://hevga.org/about/>

73.

74.

75.

76.

77. <https://www.eonreality.com/swinburne-university-selects-eon-reality-for-virtual-prototyping-2/>

78.

79.

80. <http://www.electromaterials.edu.au/news/aces-partner-profile-swinburne-university-of-technology/>

81.

82.

83.

84.

85.

86.

87.

88.

89.

90.
91.

92.

93. <https://www.aviationaerospace.org.au/partners>

94.

95.
96. <https://www.icpsr.umich.edu/icpsrweb/membership/administration/institutions/167>

97.

98.

99.

100.

101.

102. <https://swisse.com/en-au/science/scientific-partnerships>

103.

104. <http://www.lowcarbonlivingcrc.com.au/partners/research/swinburne-university-technology>

105.

106. <https://www.leocussen.edu.au/practical-legal-training/swinburne-program/>

107.

108.

109. <https://pages.wustl.edu/apl/alumni>

110.

111. <http://vivo.med.cornell.edu/display/org-10000204>

112.

113.

114.
115.

116. <https://www.genosinternational.com/genos/>

117.

118.

119.

120.

121.

122.

123.

124.

125. <http://www.swinburne.edu.au/news/latest-news/2018/04/swinburne-to-transform-art-industry-using-blockchain-technology.php>

126. <https://www.artchain.world/why-acg/>

127.

128. <https://adobe-pbd.com/us/experience?filterSchool=swinburne>

129.

130. <https://www.timeshighereducation.com/student/best-universities/international-student-table-2018-top-200-universities>

131.

132.

133.

134.

135. <http://www.swinburne.edu.au/research/research-degrees/degrees-programs/partnered-and-offshore-phd-programs/offshore/industry-embedded/>

136. <https://www.cfa.harvard.edu/~afialkov/ParticipantList.html>

137. <https://www.cfa.harvard.edu/events/2016/sackler/index/participants.html>

138.

139.
140.

141.

142.

143.

144.

145. <https://www.gooduniversitiesguide.com.au/course-provider/swinburne-university-of-technology/master-of-professional-accounting-global-leadership-program>

146. <http://www.uc.edu/content/dam/uc/international/docs/Exchange_Programs.pdf>

147.

148.

149.

150. <https://engineering.purdue.edu/ENE/Academics/Undergrad/IDE/explore-interdisciplinary-engineering>

151. <https://www.linkedin.com/pulse/sandage-department-advertising-university-illinois-usa-david-w-l-reid>

152.

153.

154.

155. {{cite
     web|url=[http://www.worldinternetproject.com/members.html|title=World](http://www.worldinternetproject.com/members.html%7Ctitle=World)
     Internet Project|website=www.worldinternetproject.com

156. <https://brockport.studioabroad.com/index.cfm?FuseAction=programs.ViewProgram&Program_ID=10082>

157. <https://studyabroad.drexel.edu/index.cfm?FuseAction=Programs.ViewProgram&Program_ID=10034>

158.

159. <https://www.baylor.edu/business/international/index.php?id=66732>

160. <https://www.bellarmine.edu/docs/default-source/psychology-docs/StudyAbroadSocialSciences.pdf>

161. <https://www.bellarmine.edu/docs/default-source/international/StudyAbroadCommunication.pdf>

162. <https://www.bellarmine.edu/docs/default-source/international/StudyAbroadMathandComputerScience.pdf>

163. <https://www.bellarmine.edu/docs/default-source/international-docs/StudyAbroadEconandFinance.pdf>

164. <https://www.bellarmine.edu/international/partnercampuses/>

165. <https://abroad.calpoly.edu/index.cfm?FuseAction=Programs.ViewProgram&Program_ID=10011>

166. <https://lallyschool.rpi.edu/programs/undergraduate-programs/study-abroad>

167. <https://pages.wustl.edu/apl/alumni>

168. <https://casper.berkeley.edu/wiki/List_of_Collaborators_and_Institutions>

169. <https://grad.ucla.edu/gss/postdoc/aboutpdinst.pdf>

170. <http://vivo.med.cornell.edu/display/org-10000204>

171. <https://cisr.mit.edu/community/our-sponsors-and-patrons/>

172. <https://cisr.mit.edu/community/sponsor-and-patron-benefits/become-a-sponsor/>

173.

174. <https://sugar-network.org/about#university>

175. <http://www.swinburne.edu.au/business-law/schools-departments/swinburne-business-school/business-technology-entrepreneurship/research/social-environmental-sustainability/partners/>

176.

177. <https://www.essca.fr/international-univers-essca/universites-partenaires/liste-des-universites-partenaires>

178.

179. <https://www.utc.fr/fileadmin/user_upload/SITE-UTC/documents/Documentations/GuideEtudiants_2017-2018_web.pdf>

180. <http://www.neoma-bs.com/docs/students/our-network/partners-list.pdf>

181. <http://files.iaaer.org/news/ACC_2018_Call_for_Papers.pdf>

182. <http://www.manmonthly.com.au/tag/fraunhofer-institute-for-manufacturing-engineering-and-automation/>

183. <https://www.international.tum.de/fileadmin/w00bwe/www/Wege_ins_Ausland/Studierende/TUMexchange/Auswahlquote_TUMexchange_nach_Unis_eng.pdf>

184.

185.

186. <https://www.hs-weingarten.de/en/web/international-office/partnerhochschulen>

187.

188. <http://www.swinburne.edu.au/science-engineering-technology/courses-study-areas/>

189.
190. <http://www.swinburne.edu.au/engineering/cmp/research_onspa.php>

191. <http://www.okayama-u.ac.jp/eng/about_okayama_university/International_Exchange.html#university>

192. <http://www.d-lab.kit.ac.jp/about-network/>

193. <https://www.meijigakuin.ac.jp/en/about/international/ic/exchange/isp.html>

194. <http://www.ferris.ac.jp/en/academics/international_activities/Study_Abroad_Program.html>

195. <http://global.chuo-u.ac.jp/english/unicms/wp-content/uploads/2017/08/List-of-Partner-Institutions-of-Chuo-Universityas-of-Aug2017-1.pdf>

196. <https://www.aoyama.ac.jp/en/outline/partner_institutions.html>

197. <http://web.aiu.ac.jp/en/about/partners/oceania/australia/swinburne-university-of-technology/>

198. <https://research.iitm.ac.in/brochure/jdp/SwinburneUniversityAustralia.pdf>

199. <http://www.oir.iitm.ac.in/foreign-universities-2/>

200.

201. <https://beyondborders.dtu.dk/pages/university.aspx?uid=47fc8ca2-0d63-e711-80e6-005056965456>

202. <http://www.dtu.dk/english/-/media/DTU-endk/Education/Exchange%20students/DTU_exchange_agreements_by_country.ashx>

203. <https://kadk.dk/en/partner-schools>

204. <https://www.swinburne.edu.au/media/swinburneeduau/current-students/docs/pdf/SDU_ExchangeGuide2017.pdf>

205.

206.

207.

208. <https://ucscinternational.unicatt.it/ucsc-international-Scienze_linguistiche.pdf>

209. <http://global.ntu.edu.sg/GMP/gemexplorer/BeforeApplying/Documents/281015%20GEM%20EX%20Forecast%20Exercise%20for%20AY2016-17%20FY_v3.pdf>

210.

211. <https://www.swinburne.edu.au/media/swinburneeduau/study-abroad/docs/pdfs/Exchange-Partners-by-Faculty.pdf>

212.

213.

214.

215. <https://uic.yonsei.ac.kr/upload/brochure/2016070116650479.pdf>

216.

217. <http://www.studyinkorea.go.kr/file/srcFileDown.do?filename=27_530670_201611020355047090.pdf&orgfilename=Brochure(Full%20version_English).pdf&fileStorePath=fileStorePath>

218.

219. <http://www.jbnu.ac.kr/eng/?menuID=282>

220. <http://en.hongik.ac.kr/contents/www/eng/introduction.html>

221. <https://ic.hkuspace.hku.hk/about-us/our-international-partners>

222. <https://www.hkpc.org/en/corporate-info/media-centre/press-releases/7095-hkpc-swinburne-university-jointly-offer-offshore-phd-programme>

223.

224. <https://www.polyu.edu.hk/fast/en/programmes/current-students/student-life/exchange-opportunities/index.html>

225. <https://www.polyu.edu.hk/international/partner-map>

226. <https://www.swinburne.edu.au/current-students/study-abroad-exchange/swinburne-students/semester-abroad/partner-universities/polyu-hong-kong-polytechnic-university/>

227. <http://iibd.hkbu.edu.hk/eng/exchange-partners/index.jsp>

228. <https://intl.hkbu.edu.hk/student-exchange/outgoing-students/where-can-i-go/search?region>\[\]=australasia

229.

230. <https://www.konstfack.se/en/Education/International-Exchange/Partner-schools/>

231. <http://ju.se/en/about-us/internationalisation/partner-universities.html>

232.

233. <https://www.swinburne.edu.au/current-students/study-abroad-exchange/swinburne-students/faculty-led-study-programs/tsinghua-university-summer-school/>

234.

235.

236. <http://tjdi.tongji.edu.cn/uploadfile/file/201506/2314350527701571531.pdf>

237. <http://tjdi.tongji.edu.cn/NewsDetail.do?ID=4350&lang=_en>

238.

239. <https://www.swinburne.edu.au/current-students/study-abroad-exchange/swinburne-students/semester-abroad/partner-universities/tongji-university/>

240.

241.

242.

243.
244. <http://www.heg-fr.ch/EN/International/International-Strategy/Pages/International-Collaborations.aspx>

245. <https://www.hslu.ch/-/media/campus/common/files/dokumente/dk/dk-studium/dk-ba/dk-dmi/dmi-study-programmes-worldwide.pdf>

246.

247. <https://www.lut.fi/web/en/get-to-know-us/introducing-the-university/international-co-operation-partners>

248.

249.

250.

251.

252.

253. <https://www.research.manchester.ac.uk/portal/en/activities/swinburne-university-of-technology(174e6015-7220-43fa-a83e-0f6e6a926669).html>

254. [https://artslondon.moveon4.com/publisher/institution/1/249/eng?relTypes=4\&frmTypes=2|7\&acadYears=\&acadPeriods=\&directions=2|3\&defaultRelStatus=2\&inst_int_settings_filter=\&acad_year_display=\&acad_period_display=\&document_types=\&restriction_types=\&restriction_id_filter=\&inst_document_types=\&inst_restriction_types=\&keyword=\&institution_internal=\&degree_programme=\&country=13\&institution_external=\&instance=null\&publisherId=1](https://artslondon.moveon4.com/publisher/institution/1/249/eng?relTypes=4&frmTypes=2%7C7&acadYears=&acadPeriods=&directions=2%7C3&defaultRelStatus=2&inst_int_settings_filter=&acad_year_display=&acad_period_display=&document_types=&restriction_types=&restriction_id_filter=&inst_document_types=&inst_restriction_types=&keyword=&institution_internal=&degree_programme=&country=13&institution_external=&instance=null&publisherId=1)

255. <https://www.strath.ac.uk/studywithus/studyabroad/goingabroad/internationalexchange/partneruniversities/>

256. <https://www.strath.ac.uk/business/undergraduate/international/comingtostrathclyde/internationalexchange/>

257.

258. <https://rector.tau.ac.il/student-exchange-agreements-original>

259.

260.

261.

262.

263.

264. <http://english.moe.gov.tw/ct.asp?xItem=13849&CtNode=11406&mp=1>

265. <http://www.eng.ncku.edu.tw/files/14-1283-177935,r1347-1.php?Lang=zh-tw>

266.

267. <https://www.swinburne.edu.au/media/swinburneeduau/study-abroad/docs/pdfs/Exchange-Partners-by-Faculty.pdf>

268.

269.

270. <http://tdx.yuntech.edu.tw/english/index.php?option=com_content&task=view&id=1152>

271.

272.

273.

274.

275.

276.

277.

278. <https://www.universitiesaustralia.edu.au/global-engagement/international-collaboration/international-agreements-and-agreements#.WpPe_YF-XqC>

279. <https://academicimpact.un.org/content/partners>

280.

281. <http://www.aapbs.org/AAPBS_map_2015.pdf>

282. <http://www.aapbs.org/password/yellow.html>

283.

284. <http://asaihl.stou.ac.th/page/Showdata.aspx?idindex=19552>

285. <http://www.aips.net.au/supporters/current-supporters/>

286.
287.
288. <https://www.dexigner.com/directory/detail/12813>

289.

290.

291. <http://www.cisaustralia.com.au/university-partnerships/australian-university-partners>

292. <http://www.cisaustralia.com.au/university-partnerships/overseas-university-partners#anchor>

293.
294.

295.

296.

297.

298. <http://www.gbpn.org/about/our-partners>

299. <https://www.apec.org/-/media/APEC/Publications/2017/10/Opportunities-for-Collaboration-to-Improve-Building-Energy-Codes-in-APEC-Economies/217_EWG_Asia-Pacific-Building-Codes-Forum.pdf>

300.

301.

302.

303.

304.

305.

306.

307. <http://www.caul.edu.au/caul-programs/ulanz/ulanz-participants>

308.

309.

310.

311.

312.

313.
314.

315.

316.

317.