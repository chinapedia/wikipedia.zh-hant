[Windows98.gif](https://zh.wikipedia.org/wiki/File:Windows98.gif "fig:Windows98.gif")
****是[美国](../Page/美国.md "wikilink")[微软公司发行於](../Page/微软.md "wikilink")1998年6月25日的混合[16位](../Page/16位.md "wikilink")／[32位的](../Page/32位.md "wikilink")[Windows系统](../Page/Windows.md "wikilink")，其版本號為4.1，[開發代號為](../Page/開發代號.md "wikilink")****。

## 特點

这个新的系统是基于[Windows
95上编写的](../Page/Windows_95.md "wikilink")，它改良硬件标准的支持，例如[MMX和](../Page/MMX.md "wikilink")[AGP](../Page/AGP.md "wikilink")。其它特性包括对[FAT32文件系统的支持](../Page/FAT32.md "wikilink")、多显示器、[Web
TV的支持和整合到Windows图形用户界面的](../Page/Web_TV.md "wikilink")[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")，称为[活动桌面](../Page/活动桌面.md "wikilink")（Active
Desktop）。

此外并在[内存管理中](../Page/内存管理.md "wikilink")，改进Windows
95在同一记忆体区段混合存放的16位元程式码与32位元程式码，易导致一个程式发生错误就会连带造成整个系统崩溃的问题。从Windows
98开始，记忆体管理上有革新的进步，即是将16位元与32位元程式码放在不同记忆体区段执行，一旦某一應用程式發生错误，可以单独关闭该程式，不影响整个系统持续正常的运作。

从Windows
98开始的[多任务](../Page/多任务.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，也与Windows
95的[先占性多工有着完全不同的改善](../Page/先占性多工.md "wikilink")。它可以由使用者决定是平均分配系统资源进行多工，或是将某一个较不急于达成、或完成时间较长的程式设为背景（后台）执行，高度的增加多工作业的实用性。

在Windows 98中，微软新增一个名为「[Windows
Update](../Page/Windows_Update.md "wikilink")」的元件，此元件可以自动从微软官网下载系统的重大更新。

## 第二版

**Windows 98
SE**（第二版）发行於1999年5月5日（也有1999年6月10日这样的说法，可能是中文版的发布时间）。它包括一系列的改进，例如[Internet
Explorer 5](../Page/Internet_Explorer_5.md "wikilink")、Windows
Netmeeting 3、Internet Connection
Sharing、对[DVD-ROM和对](../Page/DVD.md "wikilink")[USB的支持](../Page/USB.md "wikilink")。另外Windows
98 SE的核心部分比Windows
98多出支援影音[流媒体的接收能力以及](../Page/流媒体.md "wikilink")5.1声道的支援。

## 配備需求

| 硬件   | 要求配置                           |
| ---- | ------------------------------ |
| CPU  | 486DX/66MHz或更高的处理器             |
| 内存   | 至少16MB                         |
| 硬盘   | 300MB以上                        |
| 其他设备 | CD-ROM或DVD-ROM、VGA或更好的显卡以及鼠标键盘 |

## 市場反應

Windows 98以其良好的兼容性、易用的界面受到消费者欢迎。然而，由于Windows
98是一个16位和32位的混合操作系统以及基于Windows
9x内核，它运行中仍然容易出现问题，例如[蓝屏死机發生機率過高](../Page/蓝屏死机.md "wikilink")。

Windows
98被人批评为没有足够的革新，第二版被批评为不能在第一版的基础上自由升级，但这仅限于[OEM版的](../Page/OEM.md "wikilink")98SE，只要是98SE正式销售版，依然可以在98第一版上进行直接升级。

Windows 98的后续产品是[Windows Me](../Page/Windows_Me.md "wikilink")。

## 產品生命週期

Windows 98已經於2006年7月11日起停止重大安全更新。

## 参見

  - [Windows 98开发历史](../Page/Windows_98开发历史.md "wikilink")
  - [微软视窗历史](../Page/微软视窗历史.md "wikilink")
  - [操作系统](../Page/操作系统.md "wikilink")
  - [操作系统列表](../Page/操作系统列表.md "wikilink")
  - [微軟操作系统列表](../Page/微軟操作系统列表.md "wikilink")

## 參考資料

[Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")