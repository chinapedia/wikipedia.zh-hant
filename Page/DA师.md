《**DA师**》是一部[中国军事](../Page/中国军事.md "wikilink")[电视剧](../Page/电视剧.md "wikilink")，由[中国中央电视台](../Page/中国中央电视台.md "wikilink")2002年出品\[1\]。2003年作为央视的开年大戏而播出，并在广东南方电视台同步首播。[郑方南执导](../Page/郑方南.md "wikilink")。由[王志文](../Page/王志文.md "wikilink")、[許晴](../Page/許晴.md "wikilink")、[巍子](../Page/巍子.md "wikilink")、[陶慧敏](../Page/陶慧敏.md "wikilink")、[徐洪浩主演](../Page/徐洪浩.md "wikilink")。\[2\]2007年的《[狼煙](../Page/狼烟_\(2006年电视剧\).md "wikilink")》是DA師續集。

## 简介

**DA**是英文Digital
Army(数字化部队)的简称。本剧中，解放军组建了一支多兵种混编的數位化合成作战师，代号DA师，故事围绕此而展开。原本中央軍委評定的兩位師長人選，在考核中發現並不適任未來新型戰爭概念，所以後來由資歷較淺的特種兵大隊長龙凯峰出任代理師長，暫時在觀察磨合期中完成DA师編成任務和E5W數位戰場指揮系統的開發，然而事實並不如預計中的順利....。

## 主要演员

  - [王志文饰龙凯峰](../Page/王志文.md "wikilink")
  - [许晴饰林晓燕](../Page/许晴.md "wikilink")
  - [巍子饰赵梓明](../Page/巍子.md "wikilink")
  - [陶慧敏饰韩雪](../Page/陶慧敏.md "wikilink")
  - 张世会饰陆云鹤
  - [徐洪浩饰景晓书](../Page/徐洪浩.md "wikilink")
  - [郭广平饰桂平原](../Page/郭广平.md "wikilink")
  - 翟万臣饰钟元年
  - 吴冕饰杨芬芬
  - 宁晓志饰王强
  - 陈国典饰韩百川
  - [侯勇饰高达](../Page/侯勇.md "wikilink")
  - [傅淼饰赵楚楚](../Page/傅淼.md "wikilink")
  - 高兰村饰吴义文

## 参考

<div class="references-small">

<references />

</div>

  - [心戰101](../Page/心戰101.md "wikilink")
  - [解放軍電視宣傳中心](../Page/解放軍電視宣傳中心.md "wikilink")

[D](../Category/2003年中國電視劇集.md "wikilink")
[D](../Category/中国人民解放军题材电视剧.md "wikilink")

1.  [明星青睐国防绿
    联手共铸《DA师》](http://www.people.com.cn/GB/paper39/6792/661930.html)
2.  [《DA师》](http://ent.sina.com.cn/v/f/DAshi/index.html)