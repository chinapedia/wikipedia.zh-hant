## 摘要

## Fair use in [阿基尔，上帝的愤怒](../Page/阿基尔，上帝的愤怒.md "wikilink")

This image, AguirreGermanPoster .jpg, is being linked here; although the
picture is subject to copyright I feel it is covered by the U.S. fair
use laws for the *[阿基尔，上帝的愤怒](../Page/阿基尔，上帝的愤怒.md "wikilink")* article
because:

  - According to the [Wikipedia:Fair
    use](../Page/Wikipedia:Fair_use.md "wikilink") tag for movie
    posters, "it is believed that the use of scaled-down, low-resolution
    images of movie posters, to illustrate the movie in question,
    qualifies as fair use under United States copyright law." The image
    has been added to the article to assist in critical
    commentary/discussion of *Aguirre*.

Also:

1.  It is of much lower resolution than the original (copies made from
    it will be of very inferior quality).
2.  It does not limit the copyright owner's rights to market or sell the
    work in any way.
3.  The image is being used in an informative way and should not detract
    from the original work.
4.  The image is reproduced in the article to illustrate how the film
    was promoted in Germany during its original theatrical release
    there.-[<span style="color: orange">-{冰热海风}-</span>](../Page/User:冰热海风.md "wikilink")([<span style="color: Aqua;">talk</span>](../Page/User_talk:冰热海风.md "wikilink"))
    2008年4月30日 (三) 11:18 (UTC)

## 许可协议