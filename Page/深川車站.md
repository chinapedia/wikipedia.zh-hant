**深川車站**（）是一位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[深川市一條](../Page/深川市.md "wikilink")、隸屬於[北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")（JR北海道）的[鐵路車站](../Page/鐵路車站.md "wikilink")。深川車站是北海道主力幹線[函館本線沿線的大站之一](../Page/函館本線.md "wikilink")，所在地深川市的主車站，也是轉乘地方支線[留萌本線的分岔點](../Page/留萌本線.md "wikilink")。

## 車站構造

站務上，深川車站是一[站務員配置車站](../Page/站務員.md "wikilink")（），設置有[綠窗口](../Page/綠窗口.md "wikilink")（），以及[自動售票機](../Page/自動售票機.md "wikilink")、[自動補票機等設施](../Page/自動補票機.md "wikilink")。

在月台配置方面，深川車站設有三座月台共四條乘車線。其中編號六號的乘車線除了是行駛於留萌線上的觀光[蒸汽列車](../Page/蒸汽機車.md "wikilink")[SL鈴蘭號](../Page/SL鈴蘭號列車.md "wikilink")（）的停靠月台之外，也是過去[深名線](../Page/深名線.md "wikilink")（已廢線）所使用的月台。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>函館本線</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/瀧川站.md" title="wikilink">瀧川</a>、<a href="../Page/岩見澤站.md" title="wikilink">岩見澤</a>、<a href="../Page/札幌站.md" title="wikilink">札幌方向</a></p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/旭川站.md" title="wikilink">旭川</a>、<a href="../Page/稚內站.md" title="wikilink">稚內</a>、<a href="../Page/網走站.md" title="wikilink">網走方向</a>（主要為特急列車）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>4</strong></p></td>
<td><p>函館本線</p></td>
<td><p>下行</p></td>
<td><p>旭川方向（主要為普通列車）</p></td>
</tr>
<tr class="even">
<td><p>留萌本線</p></td>
<td></td>
<td><p><a href="../Page/留萌站.md" title="wikilink">留萌方向</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>留萌本線</p></td>
<td></td>
<td><p>留萌方面（只限一部分列車）</p></td>
</tr>
</tbody>
</table>

## 歷史

  - 1898年7月16日 - 以[北海道官設鐵道上川線的沿線車站之姿啟用](../Page/北海道官設鐵道.md "wikilink")。
  - 1905年4月1日 - 移交由[官設鐵道](../Page/日本國鐵.md "wikilink")（也就是日本國鐵的前身）管轄。
  - 1924年10月25日 - 雨龍線（也就是後來的[深名線](../Page/深名線.md "wikilink")）通車。
  - 1930年12月1日 - （也就是後來的[留萌本線](../Page/留萌本線.md "wikilink")）通車。
  - 1987年4月1日 -
    [日本國鐵分割民營化](../Page/日本國鐵分割民營化.md "wikilink")，深川車站的營運由JR北海道繼承。
  - 1995年9月4日 -
    深名線（**深川**至[名寄](../Page/名寄車站.md "wikilink")）廢線，原路線沿線的交通服務改由長途巴士替代。

## 相鄰車站

  - 北海道旅客鐵道（JR北海道）

:\*
特急「[神威](../Page/超級神威號列車.md "wikilink")」、「[丁香](../Page/超級神威號列車.md "wikilink")」、「[鄂霍茨克號](../Page/鄂霍茨克號列車.md "wikilink")」、「[宗谷](../Page/宗谷號列車.md "wikilink")」停靠站

  -

    函館本線

      -

          -
            [妹背牛](../Page/妹背牛站.md "wikilink")（A23）－**深川（A24）**－[納內](../Page/納內站.md "wikilink")（A25）

    留萌本線

      -

          -
            （⭐）（函館本線） →
            **深川（A24）**－（※）[北一已](../Page/北一已站.md "wikilink")－[石狩沼田](../Page/石狩沼田站.md "wikilink")
            （※）下行4921D通過北一已、[秩父別與](../Page/秩父別站.md "wikilink")[北秩父別](../Page/北秩父別站.md "wikilink")。
            （⭐）下行4929D為旭川站始發、旭川站－此站之間途經函館本線運行。

## 參考文獻與註釋

  - 本久公洋，《》，北海道新聞社 出版（2008年），ISBN 978-4-89453-464-3
  - 《》，學習研究社 出版（2003年2月），ISBN 978-4054018167
  - 《道內時刻表》，交通新聞社（日本札幌）出版

## 相關條目

  - [日本鐵路車站列表 Fu](../Page/日本鐵路車站列表_Fu.md "wikilink")

## 外部連結

  - [深川車站（JR北海道旭川支社）](http://www.jrasahi.co.jp/contents/facilities/station/a24_fukagawa.html)

[Kagawa](../Category/日本鐵路車站_Fu.md "wikilink")
[Category:空知管內鐵路車站](../Category/空知管內鐵路車站.md "wikilink")
[Category:函館本線車站](../Category/函館本線車站.md "wikilink")
[Category:留萌本線車站](../Category/留萌本線車站.md "wikilink")
[Category:1898年启用的铁路车站](../Category/1898年启用的铁路车站.md "wikilink")
[Category:深名線車站](../Category/深名線車站.md "wikilink")
[Category:深川市](../Category/深川市.md "wikilink")
[Category:北海道官設鐵道車站](../Category/北海道官設鐵道車站.md "wikilink")