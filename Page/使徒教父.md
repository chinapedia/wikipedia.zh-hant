**使徒教父**（），又譯為**宗徒教父**，指[基督教](../Page/基督教.md "wikilink")[使徒以後的初代至二代的教會作家](../Page/使徒.md "wikilink")，是生活於[一世紀後半至](../Page/一世紀.md "wikilink")[二世纪中葉前的基督教人物](../Page/二世纪.md "wikilink")。他們之所以被稱為使徒教父，是因為他們曾直接接觸過，或受教於，[耶穌的](../Page/耶穌.md "wikilink")[使徒](../Page/使徒.md "wikilink")，或是使徒的門徒。使徒教父這一名稱始自十七世紀，起初有五位，後來另加了三位，共八位。\[1\]

基督教從[猶太人的文化中出生](../Page/猶太人.md "wikilink")。在第[一世紀結束時](../Page/一世紀.md "wikilink")，許多教會已經是以[希利尼人為骨幹](../Page/希利尼人.md "wikilink")，但是教會的教導與行為模式仍帶有濃厚的猶太氣息。這些使徒教父是一些帶領教會過渡到希臘文化環境的教會領袖。他們的著作是繼[新約聖經書卷之後](../Page/新約.md "wikilink")，基督教最早的著作。内容很少强调教義，著重的是道德教訓。從他們的著作中，我們可以瞥見第二世紀早期的教會生活。

## 著名使徒教父

  - [羅馬的克勉](../Page/羅馬的克勉.md "wikilink")（[教宗](../Page/教宗.md "wikilink")[聖克勉一世](../Page/聖人.md "wikilink")）
  - [安提阿的伊格那丢](../Page/伊格那丢.md "wikilink") （Ignatius），約67-110年
  - [示每拿的坡旅甲](../Page/坡旅甲.md "wikilink")（Polycarp），約69-156年年
  - [羅馬的黑馬](../Page/羅馬的黑馬.md "wikilink")（Hermas），約100-140年
  - [帕皮亞](../Page/帕皮亞.md "wikilink")（Papias），約60-130年

## 著作

  - 《[丟格那妥書](../Page/丟格那妥書.md "wikilink")》（*The Epistle to Diognetus*）
  - 《[革利免一書](../Page/革利免一書.md "wikilink")》
  - 《[革利免二書](../Page/革利免二書.md "wikilink")》
  - 《[十二使徒遺訓](../Page/十二使徒遺訓.md "wikilink")》（*Didache*）
  - 《伊格那丢書信》（七封）
  - 《坡旅甲致腓立比書》
  - 《坡旅甲殉道記》
  - 《[巴拿巴書](../Page/巴拿巴書.md "wikilink")》（*The Epistle of Barnabas*）
  - 《[黑馬牧人書](../Page/黑馬牧人書.md "wikilink")》（The *Sheperd* of Hermas）
  - 帕皮亞殘篇集

## 参考文献

<div class="references-small">

<references />

</div>

## 参见

  - [使徒 (基督教)](../Page/使徒_\(基督教\).md "wikilink")
  - [使徒时代](../Page/使徒时代.md "wikilink")
  - [教父 (基督教歷史)](../Page/教父_\(基督教歷史\).md "wikilink")

{{-}}

[使徒教父](../Category/使徒教父.md "wikilink")
[Category:基督教头衔](../Category/基督教头衔.md "wikilink")

1.  Justo L. González, *A History of Christian Thought*, vol. 1, *From
    the Beginnings to the Council of Chalcedon*, rev. ed. (Nashville:
    Abingdon Press, 1987), 61.