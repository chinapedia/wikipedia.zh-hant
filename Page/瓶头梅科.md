**瓶头梅科**又名[山醋李科或](../Page/山醋李科.md "wikilink")[扁子木科](../Page/扁子木科.md "wikilink")，共有3[属](../Page/属.md "wikilink")4[种](../Page/种.md "wikilink")，全部生长在[热带](../Page/热带.md "wikilink")[东非](../Page/东非.md "wikilink")、[非洲西南部和](../Page/非洲.md "wikilink")[马达加斯加岛](../Page/马达加斯加岛.md "wikilink")。

本科[植物为](../Page/植物.md "wikilink")[灌木或小](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")；单[叶互生或对生](../Page/叶.md "wikilink")，无托叶；[花单性](../Page/花.md "wikilink")，雌雄异株，雌花单生，[花瓣](../Page/花瓣.md "wikilink")6-10，雄花生于聚合花序中，[花瓣](../Page/花瓣.md "wikilink")8-10；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，[种子两侧压扁](../Page/种子.md "wikilink")，有翅或无翅。

1981年的[克朗奎斯特分类法将这些属放到](../Page/克朗奎斯特分类法.md "wikilink")[田基麻科中](../Page/田基麻科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，但将[扁果树属单独分为一个](../Page/扁果树属.md "wikilink")[扁果树科](../Page/扁果树科.md "wikilink")（Kaliphoraceae），没有列入任何一个[目](../Page/目.md "wikilink")，2003年经过修订的[APG
II
分类法将其合并入本](../Page/APG_II_分类法.md "wikilink")[科](../Page/科.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[瓶头梅科](http://delta-intkey.com/angio/www/montinia.htm)

[\*](../Category/瓶头梅科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")
[Category:中井猛之進命名的生物分類](../Category/中井猛之進命名的生物分類.md "wikilink")