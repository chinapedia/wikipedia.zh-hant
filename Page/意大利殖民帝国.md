[Italian_Colonial_Empire_(orthographic_projection).svg](https://zh.wikipedia.org/wiki/File:Italian_Colonial_Empire_\(orthographic_projection\).svg "fig:Italian_Colonial_Empire_(orthographic_projection).svg")

**義大利殖民帝國**（）是由近代[義大利所建立的](../Page/義大利王國_\(1861年-1946年\).md "wikilink")[殖民地](../Page/殖民地.md "wikilink")[帝國](../Page/帝國.md "wikilink")。而這個[殖民帝國在](../Page/殖民帝國.md "wikilink")[第二次世界大戰中除了擁有意大利外](../Page/第二次世界大戰.md "wikilink")，更支配著[地中海和](../Page/地中海.md "wikilink")[東非的其他屬土諸如](../Page/東非.md "wikilink")[厄立特里亞](../Page/厄立特里亞.md "wikilink")、[衣索匹亞等東非殖民地](../Page/衣索匹亞.md "wikilink")。帝國勢力在1940年達到頂峰，而后随着[意大利投降以及](../Page/意大利投降.md "wikilink")[第二次世界大战结束](../Page/第二次世界大战.md "wikilink")，意大利殖民帝国也逐渐成为历史。

就如[德意志帝國一樣](../Page/德意志帝國.md "wikilink")，義大利比起其他歐洲強國較遲達成統一。所以，其殖民帝國也比歐洲列強的殖民地帝國來得要遲。亦因如此，它並不能和其他歐洲列強競逐[亞](../Page/亞洲.md "wikilink")[美兩洲的勢力](../Page/美洲.md "wikilink")（[中國的](../Page/中國.md "wikilink")[天津租界例外](../Page/天津租界.md "wikilink")），儘管在[新大陸有很多意大利裔移民](../Page/新大陸.md "wikilink")。

## [義大利王國和](../Page/義大利王國.md "wikilink")[法西斯主義](../Page/法西斯主義.md "wikilink")

1929年[經濟大衰退之後](../Page/經濟大衰退.md "wikilink")，[帝國主義成為義大利](../Page/帝國主義.md "wikilink")[法西斯主義者](../Page/法西斯主義.md "wikilink")[墨索里尼演講的主題](../Page/墨索里尼.md "wikilink")。他認為殖民地的建立對義大利人口和經濟的振興有很大的作用。而帝國主義也是墨索里尼為了讓義大利取代[英](../Page/英國.md "wikilink")[法兩國](../Page/法國.md "wikilink")，成為[地中海主要勢力不可或缺的一部份](../Page/地中海.md "wikilink")。

## 義屬東非

意属东非（）是意大利王国在东非的一个殖民地。于1936年到1941年期间存在。由阿比西尼亚、意属索马里兰和厄立特里亚三个区域组成。1936年，总理墨索里尼下令侵略阿比西尼亚。意属东非宣告建立。下设6个州。1940年8月，意大利又侵占了英属索马里兰。但进入1941年，意大利战局趋于被动。1941年，阿比西尼亚恢复独立，意属东非解体。

### 義屬索馬里蘭（今[索馬里](../Page/索馬里.md "wikilink")）（1889～1960年）

在1889至1890年間，[義大利在](../Page/義大利.md "wikilink")[東非的](../Page/東非.md "wikilink")[索馬里蘭建立了殖民地](../Page/索馬里蘭.md "wikilink")。1925年，因[肯亞得到](../Page/肯亞.md "wikilink")[朱巴蘭](../Page/朱巴蘭.md "wikilink")，其殖民地得以擴張。

### 義屬厄利垂亞（1889～1941年）

[ST-Massowa.jpg](https://zh.wikipedia.org/wiki/File:ST-Massowa.jpg "fig:ST-Massowa.jpg")\]\]

1889年，義大利在[厄利垂亞建立殖民地](../Page/厄利垂亞.md "wikilink")。這個殖民地被法西斯義大利視作為一個驕傲。而今時今日在厄利垂亞的首都[阿斯馬拉](../Page/阿斯馬拉.md "wikilink")，許多被保留下來的[古蹟和](../Page/古蹟.md "wikilink")[紀念碑都可追溯到百多年前由義大利所統治的時期](../Page/紀念碑.md "wikilink")。由於為數眾多的建築遺產，阿斯馬拉也許能成為一個[世界遺產](../Page/世界遺產.md "wikilink")。

### [阿比西尼亞](../Page/阿比西尼亞.md "wikilink")（今[衣索匹亞](../Page/衣索匹亞.md "wikilink")）（1935～1941年）

[Soldatietiopia.jpg](https://zh.wikipedia.org/wiki/File:Soldatietiopia.jpg "fig:Soldatietiopia.jpg")中士兵撤离。\]\]

儘管義大利在1895至1896年間的[第一次義大利－衣索匹亞戰爭中被](../Page/第一次義大利－衣索匹亞戰爭.md "wikilink")[阿比西尼亞打敗](../Page/阿比西尼亞.md "wikilink")，但當墨索里尼執政後，義大利便在1935至1936年間發動[第二次義大利－衣索匹亞戰爭](../Page/第二次義大利－衣索匹亞戰爭.md "wikilink")，成功擊敗阿比西尼亞。1936年5月9日，義大利國王[維克托·伊曼紐爾三世宣佈成為](../Page/維托里奧·埃馬努埃萊三世.md "wikilink")[衣索匹亞皇帝](../Page/衣索匹亞.md "wikilink")。

而墨索里尼欲把數以百萬計的移民者送到東非的殖民地中落地生根，以取得在東非的豐厚經濟利潤。但是，義大利入侵衣索匹亞一事已經受到國際間的敵視。因此，義大利對這個新殖民地的統治權大約只有五年。

### 利比亞（1912～1943年）

[Visita_del_RE_a_Bengasi.JPG](https://zh.wikipedia.org/wiki/File:Visita_del_RE_a_Bengasi.JPG "fig:Visita_del_RE_a_Bengasi.JPG")

義大利在1911年與[奧斯曼土耳其帝國發生戰爭](../Page/奧斯曼土耳其帝國.md "wikilink")，其後土耳其戰敗，雙方簽訂了《[洛桑條約](../Page/洛桑條約.md "wikilink")》。條約使得義大利得到了[北非的](../Page/北非.md "wikilink")[的黎波里及](../Page/的黎波里.md "wikilink")[昔蘭尼加的領土](../Page/昔蘭尼加.md "wikilink")，並與[法國協議](../Page/法國.md "wikilink")，共同分割北非的海岸。義大利對此舉感到十分驕傲，並稱之為「義大利第四個海岸」。事實上，義大利在20年代中花了整整近十年的時間來「平定」這個殖民地。

1934年，的黎波里及昔蘭尼加殖民地合併為「[利比亞](../Page/利比亞.md "wikilink")」殖民地。墨索里尼採用了這個一千八百多年前[羅馬帝國君主](../Page/羅馬帝國.md "wikilink")[戴克里先沿用的名字](../Page/戴克里先.md "wikilink")，就是欲揚威海外，建立非洲帝國，以圖重振羅馬帝國時代的雄風。

1943年，[纳粹德國](../Page/纳粹德國.md "wikilink")[非洲軍團及義大利部隊被](../Page/非洲軍團.md "wikilink")[英](../Page/英國.md "wikilink")[美盟軍趕出北非](../Page/美國.md "wikilink")，義大利對利比亞的統治權也隨之而逝。

## 亚洲

### 中國[天津意租界](../Page/天津意租界.md "wikilink")（1902～1943年）

[马可波罗广场近景.jpg](https://zh.wikipedia.org/wiki/File:马可波罗广场近景.jpg "fig:马可波罗广场近景.jpg")[马可波罗广场](../Page/马可波罗广场.md "wikilink")\]\]

1900年，[清朝爆發](../Page/清朝.md "wikilink")[八國聯軍之役](../Page/八國聯軍之役.md "wikilink")，義大利也參與其中。事後，中國與列強求和，義大利因此分得中國的[天津港](../Page/天津港.md "wikilink")，以作為一個貿易集散地。而這個只得46公頃的城市可算是歐洲殖民者中得到的最小一個殖民地。但天津意租界却是意大利在境外的唯一一处租界，也是意大利在亚洲的唯一一块领地.

1943年，英美盟軍擊敗義大利，墨索里尼遭國王軟禁，墨索里尼便臨時建立起[意大利社会共和国](../Page/意大利社会共和国.md "wikilink")，並與中國[汪精衛政權成立的政權達成協議](../Page/汪精衛政權.md "wikilink")，取消義大利在天津的統治權。

## 歐洲

### 阿爾巴尼亞

在[第一次世界大戰結束後一段時間](../Page/第一次世界大戰.md "wikilink")，義大利佔領了[阿爾巴尼亞](../Page/阿爾巴尼亞.md "wikilink")。其後，兩國在1920年9月2日簽署協定，義大利部隊可以撤出阿爾巴尼亞，條件就是該國把[薩贊島割讓給義大利](../Page/薩贊島.md "wikilink")。

1939年，阿爾巴尼亞完全成為了義大利殖民帝國的勢力範圍。意大利獨裁者對此感到莫大成功，並將此成功與德國吞併[奧地利及](../Page/德奧合併.md "wikilink")[捷克斯洛伐克相提並論](../Page/捷克斯洛伐克.md "wikilink")。1939年4月7日，義大利正式入侵阿爾巴尼亞，五天後阿軍完全喪失抵抗能力，阿爾巴尼亞國王[索格一世逃亡至倫敦](../Page/索格一世_\(阿尔巴尼亚\).md "wikilink")。而義大利國王[維托里奧·埃馬努埃萊三世則被加冕為阿爾巴尼亞國王](../Page/維托里奧·埃馬努埃萊三世.md "wikilink")，墨索里尼更架設一個阿爾巴尼亞的法西斯政府，由阿爾巴尼亞前首相擔任政府首腦。此後，阿爾巴尼亞軍隊直屬於義大利麾下，直至墨索里尼下台。

1941年，德義兩國發動[巴爾幹戰役](../Page/巴爾幹戰役.md "wikilink")，[南斯拉夫陷落](../Page/南斯拉夫.md "wikilink")，境內的[科索沃及](../Page/科索沃.md "wikilink")[馬其頓一部份土地與阿爾巴尼亞合併](../Page/馬其頓.md "wikilink")，成為義大利的領地。而在1942至1943年間，阿爾巴尼亞也不斷發生一些抵抗義大利統治的運動。1943年夏，反抗勢力已經完全控制阿爾巴尼亞。1943年9月，隨著墨索里尼政權的倒台，義大利完全地退出了阿爾巴尼亞。

### 希臘

[多德卡尼斯群島同是義大利在](../Page/多德卡尼斯群島.md "wikilink")1911～12年間的[義土戰爭中](../Page/義土戰爭.md "wikilink")，打敗衰弱的奧斯曼土耳其帝國而奪回來的土地。而當時的義大利正欲奪得[罗得岛及其週邊的島嶼](../Page/罗得岛.md "wikilink")，以挑戰英國在[地中海的統治地位](../Page/地中海.md "wikilink")。

而作為第一次世界大戰戰勝國的義大利，有權可以獲得戰敗國土耳其的割地。在1919年簽訂的《[色佛尔条约](../Page/色佛尔条约.md "wikilink")》中，羅得島及多德卡尼斯群島劃入義大利。1923年的洛桑條約更確立了國際間的承認。

1943年9月，義大利把多德卡尼斯群島的控制權交予德國。

### 蒙特內哥羅

在1941年，[蒙特內哥羅重建起](../Page/蒙特內哥羅.md "wikilink")[君主立憲制](../Page/君主立憲制.md "wikilink")，隨後更宣稱成為義大利的保護國。1943年9月，蒙特內哥羅被德军控制。

## 新羅馬帝國的構思

[Meharisti.jpg](https://zh.wikipedia.org/wiki/File:Meharisti.jpg "fig:Meharisti.jpg")

**[新羅馬帝國](../Page/新羅馬帝國.md "wikilink")**（[義大利語](../Page/義大利語.md "wikilink")：****；[拉丁語](../Page/拉丁語.md "wikilink")：****）是墨索里尼創造出來，以形容意大利殖民地帝國，特別是指義大利佔領下的衣索匹亞。這個構想建立於[第二次世界大戰前義大利](../Page/第二次世界大戰.md "wikilink")[民族主義的高峰時期](../Page/民族主義.md "wikilink")，並與羅馬帝國有很大關係：

  - [亞得里亞海改稱為](../Page/亞得里亞海.md "wikilink")「****」（義大利語，意為「我們的海洋」）。在義大利入侵阿爾巴尼亞後，亞得里亞海落入了義大利手上。而這個名字在羅馬帝國時代是指整個地中海，因為羅馬帝國的勢力曾經遍佈整個地中海，墨索里尼打算重新建立羅馬帝國的輝煌時代。
  - 義大利的政權名曰「[法西斯](../Page/法西斯.md "wikilink")」，取材自羅馬帝國時代執政官手握的束棒（），代表至高無上的權力。
  - 首都為[羅馬](../Page/羅馬.md "wikilink")，與羅馬帝國時代一樣。
  - [伊曼紐爾三世加冕為皇帝](../Page/伊曼紐爾三世.md "wikilink")。

## 帝國的野心

義大利軍隊曾經短暫地佔領[英屬索馬利蘭](../Page/英屬索馬利蘭.md "wikilink")。1941年，義大利佔領了今[克罗地亚境內的](../Page/克罗地亚.md "wikilink")[達爾馬提亞及](../Page/達爾馬提亞.md "wikilink")[科托爾灣和](../Page/科托爾灣.md "wikilink")[斯洛文尼亚西南部地区](../Page/斯洛文尼亚.md "wikilink")。墨索里尼更欲進一步佔領[馬耳他](../Page/馬耳他.md "wikilink")、[突尼西亞](../Page/突尼西亞.md "wikilink")、[法屬索馬里蘭及](../Page/法屬索馬里蘭.md "wikilink")[科西嘉等地](../Page/科西嘉.md "wikilink")。法國陷落後，墨索里尼更希望當戰勝英國後，北非軍隊可以一舉佔領[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")、[埃及和](../Page/埃及.md "wikilink")[蘇丹等地](../Page/苏丹共和国.md "wikilink")，但遭德國的冷淡回應。

[Amadeo_Aosta3rd_01.jpg](https://zh.wikipedia.org/wiki/File:Amadeo_Aosta3rd_01.jpg "fig:Amadeo_Aosta3rd_01.jpg")率部在[Amba-Alagi英勇抵抗](../Page/Amba-Alagi.md "wikilink")，受到英军的尊敬\]\]

## 帝國的终结

當英美聯軍從東、北非進攻義大利時，義大利殖民帝國終於迎向完結。1943年12月，墨索里尼的政府被推翻，義軍撤出了阿爾巴尼亞及多德卡尼斯群島。其後在1947年簽訂的《[義大利和平條約](../Page/義大利和平條約.md "wikilink")》中，義大利正式宣告喪失所有殖民地，儘管在1949年時，[聯合國決定把](../Page/聯合國.md "wikilink")[義屬索馬利蘭繼續置於義大利托管之下](../Page/義屬索馬利蘭.md "wikilink")。直至1960年7月1日，[英屬索馬利蘭與](../Page/英屬索馬利蘭.md "wikilink")[義屬索馬利蘭合併](../Page/義屬索馬利蘭.md "wikilink")，並形成今天的獨立國家——[索馬里](../Page/索馬里.md "wikilink")。

[Keren_Battlefield_008.jpg](https://zh.wikipedia.org/wiki/File:Keren_Battlefield_008.jpg "fig:Keren_Battlefield_008.jpg")意大利战争公墓\]\]

## 参考文献

### 引用

### 来源

  - 网页

<!-- end list -->

  - [義大利領土資料](http://www.worldstatesmen.org/COLONIES.html)
  - [義大利殖民帝國](http://www.allempires.com/article/index.php?q=italian_colonial)

## 参见

  - [威尼斯共和国](../Page/威尼斯共和国.md "wikilink")
  - [热那亚共和国](../Page/热那亚共和国.md "wikilink")
  - [大意大利](../Page/大意大利.md "wikilink")
  - [第三罗马](../Page/第三罗马.md "wikilink")
  - [地中海联盟](../Page/地中海联盟.md "wikilink")

{{-}}

[前意大利殖民地](../Category/前意大利殖民地.md "wikilink")
[Category:现代欧洲殖民史](../Category/现代欧洲殖民史.md "wikilink")