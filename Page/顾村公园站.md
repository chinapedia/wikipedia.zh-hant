**顾村公园站**\[1\]，前称顾村站，位于[上海](../Page/上海.md "wikilink")[宝山区](../Page/宝山区_\(上海市\).md "wikilink")[顾村镇](../Page/顾村镇.md "wikilink")[陆翔路](../Page/陆翔路.md "wikilink")[陈富路北侧](../Page/陈富路.md "wikilink")，为[上海轨道交通7号线二期的车站](../Page/上海轨道交通7号线.md "wikilink")。同时该站也是未来[上海轨道交通15号线的车站](../Page/上海轨道交通15号线.md "wikilink")，与7号线车站通道换乘。

## 車站週邊

  - [顾村公园](../Page/顾村公园.md "wikilink")

## 公交换乘

宝山区社区巴士（陆翔路镜泊湖路-菊盛路宝安公路）

## 参考资料

[顾村站建设用地规划许可证](https://web.archive.org/web/20070927192756/http://www.shghj.gov.cn/Ct_3.aspx?ct_id=00060613E09344)

[Category:2010年启用的铁路车站](../Category/2010年启用的铁路车站.md "wikilink")
[Category:以公園命名的鐵路車站](../Category/以公園命名的鐵路車站.md "wikilink")

1.  [本市在建的7号线与11号线53座车站确定名称](http://sh.xinmin.cn/shehui/2008/12/06/1461376.html)