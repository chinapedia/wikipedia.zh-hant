**誠品股份有限公司**（英語：The Eslite
Corporation，簡稱：**誠品書店**，[英語](../Page/英語.md "wikilink")：Eslite，）
大型連鎖[書店](../Page/書店.md "wikilink")，由[吳清友於](../Page/吳清友.md "wikilink")1989年1月24日創辦於[臺北市](../Page/臺北市.md "wikilink")，初期以販售藝術人文方面的書籍為主，之後轉型為綜合性書店，同時結合[商場經營](../Page/商場.md "wikilink")，旗下事業涵蓋[百貨](../Page/百貨.md "wikilink")[零售業](../Page/零售業.md "wikilink")、[文化](../Page/文化.md "wikilink")[藝術](../Page/藝術.md "wikilink")、[旅館與](../Page/旅館.md "wikilink")[不動產經營等](../Page/不動產.md "wikilink")。

## 紀錄

  - 2004年獲《[時代](../Page/時代_\(雜誌\).md "wikilink")》雜誌亞洲版評選為「亞洲最佳書店」。
  - 2011年獲選為「臺灣百大品牌」文創服務類別企業。
  - 2015年8月4日：[CNN評選](../Page/CNN.md "wikilink")「全球最酷書店」，共有17家書店入選，臺北敦南誠品書店為其一。\[1\]

## 歷史

  - 1974年：成立**誠建股份有限公司**，進口代理全套[歐美廚具](../Page/歐美.md "wikilink")。
  - 1989年：成立**誠品股份有限公司**，第一家誠品書店開幕於臺北市[仁愛敦南圓環](../Page/仁愛敦南圓環.md "wikilink")。
  - 1995年：誠品總店遷至[敦化南路現址](../Page/敦化南路.md "wikilink")。
  - 1996年：成立誠品零售事業部
  - 1998年：誠品臺南店開業（後搬至文化中心店）
  - 1999年: 誠品桃園遠百店開業。誠品敦南店開始24小時營業。
  - 2000年：成立誠品流通事業部，擴大到餐飲食品、用品、設備通路銷售，成立**誠品全球網路股份有限公司**。
  - 2001年：於桃園南崁成立**誠品開發物流股份有限公司**。誠品高雄大遠百店開業。
  - 2002年：誠品零售事業部和流通事業部合併為生活事業部，並提出誠品的經營理念為人文、藝術、創意、生活。
  - 2005年9月：成立“誠新股份有限公司”，統籌經營誠品商場等項目。
  - 2006年1月：誠品信義旗艦店開業。
  - 2007年9月：誠品臺東故事館開業。
  - 2008年5月：與[勤美集團合作](../Page/勤美集團.md "wikilink")，採聯合招商及委託誠品經營管理方式成立「**[勤美誠品綠園道](../Page/勤美誠品綠園道.md "wikilink")**」。5月24日，改建完成正式開幕。
  - 2008年8月：誠品網路書店全新改版上線，此為誠品網路書店最大的一次改版；9月1日：誠品敦南店封館改裝，同年10月11日重新營運。
  - 2009年10月：誠新股份有限公司更名為**誠品生活股份有限公司**；同月誠品網站上線；誠品與[臺北市立美術館合辦](../Page/臺北市立美術館.md "wikilink")《蔡國強泡美術館》當代藝術個展。
  - 2010年9月：誠品生活以新發行[股份做為](../Page/股份.md "wikilink")[對價](../Page/對價.md "wikilink")，取得誠品分割出讓之複合商場事業、餐旅事業、不動產事業相關營業、資產及負債；同月誠品書店取得[臺北新世界購物中心經營權](../Page/臺北新世界購物中心.md "wikilink")。
  - 2011年5月：誠品接手經營臺北車站K區地下街，以「誠品站前店」之名重新開業。
  - 2012年8月：誠品的第一家香港分店「誠品生活香港銅鑼灣店」在[希慎廣場開業](../Page/希慎廣場.md "wikilink")；11月，誠品生活新板店開業。
  - 2013年1月：30日公開發行[股票上櫃](../Page/股票.md "wikilink")。
  - 2013年8月：誠品生活[松菸店開業](../Page/松山文化創意園區.md "wikilink")，也是誠品首度跨足經營電影及表演等新經營業態。
  - 2014年1月：誠品生活虎尾店開業。
  - 2015年4月：誠品[蘇州](../Page/蘇州.md "wikilink")[房地產投資](../Page/房地產.md "wikilink")，金雞湖畔的住宅「誠品居所」。
  - 2015年7月10日：[高雄市誠品生活](../Page/高雄市.md "wikilink")[駁二店開業](../Page/駁二.md "wikilink")。
  - 2015年10月：誠品的第二家香港分店「誠品生活香港尖沙咀店」開業；誠品生活屏東店重新開幕；誠品書店SOGO店開幕。
  - 2015年11月29日：誠品生活在中國大陸首家旗艦店誠品生活蘇州開幕，建築面積逾13萬平方米，位於蘇州工業園區月廊街8號。
  - 2016年1月6日：誠品在香港的第三家分店「誠品生活香港太古店」開業。
  - 2016年7月28日：誠品生活[臺中大遠百店開幕](../Page/遠東百貨.md "wikilink")。
  - 2016年10月27日：誠品生活[高雄SOGO店開幕](../Page/太平洋崇光百貨.md "wikilink")。
  - 2018年5月13日：誠品書店[大統店因租約到期結束營業](../Page/大統百貨.md "wikilink")\[2\]\[3\]。
  - 2018年5月26日：誠品生活花蓮店於[遠百花蓮店開幕](../Page/遠東百貨.md "wikilink")。
  - 2018年9月15日：誠品生活桃園統領店試營運。
  - 2018年9月20日：誠品生活南西店試營運，其僅次於誠品信義店、誠品松菸店，為誠品第三大店。9月30日正式開幕
  - 2018年10月16日：誠品生活開記者會宣布跟《[三井不動產](../Page/三井不動產.md "wikilink")》合作，2019年秋天進軍日本東京都[日本橋室町三井塔大樓設點營業](../Page/日本橋室町三井塔大樓.md "wikilink")\[4\]。
  - 2018年12月15日：诚品生活[深圳店开幕](../Page/深圳.md "wikilink")\[5\]。

### 預定時程

  - 2019年：《誠品生活》跟《[三井不動產](../Page/三井不動產.md "wikilink")》及百年書店《有隣堂》，將於2019年秋天正式進駐位於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[日本橋室町三井塔大樓](../Page/日本橋室町三井塔大樓.md "wikilink")。

<!-- end list -->

  - 2020年：誠品敦南店2020年前照常營運，因為誠品敦南店租約到期並且將歇業於2020年\[6\]，誠品敦南店將於2020年改建飯店\[7\]。

## 關係企業組織圖

<center>

</center>



## 營業據點

###

誠品書店在臺灣各地區共設有41間分店。誠品內部將營運模式分為獨立大店、購物中心與百貨店型、交通樞紐與醫院校園、區域特色與社區生活四種主要類型。\[8\]

#### 商場模式

|                                         |                                                                                                                                                                                                                                                                                                                                                 |                                                                                                                                                                   |                                                                                                                                        |
| --------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| **店名**                                  | **圖片**                                                                                                                                                                                                                                                                                                                                          | **地址**                                                                                                                                                            | **簡介**                                                                                                                                 |
| 信義店                                     | [Eslite_Xinyi_Store_20061125_night.jpg](https://zh.wikipedia.org/wiki/File:Eslite_Xinyi_Store_20061125_night.jpg "fig:Eslite_Xinyi_Store_20061125_night.jpg")[Eslite_SinYi_store_Magazine_Zone_2015.jpg](https://zh.wikipedia.org/wiki/File:Eslite_SinYi_store_Magazine_Zone_2015.jpg "fig:Eslite_SinYi_store_Magazine_Zone_2015.jpg") | [臺北市](../Page/臺北市.md "wikilink")[信義區](../Page/信義區.md "wikilink")[松高路](../Page/松高路.md "wikilink")11號                                                               | 2006年1月位於臺北[統一國際大樓的的B](../Page/統一國際大樓.md "wikilink")2-6F信義店開業，書店面積達2500坪。                                                              |
| 松菸店                                     | [Eslite_Living_songyan_201507.JPG](https://zh.wikipedia.org/wiki/File:Eslite_Living_songyan_201507.JPG "fig:Eslite_Living_songyan_201507.JPG")                                                                                                                                                                                               | [臺北市](../Page/臺北市.md "wikilink")[信義區菸廠路](../Page/信義區.md "wikilink")88號                                                                                            | 位於[松山文創園區](../Page/松山文創園區.md "wikilink")[台北文創大樓的B](../Page/台北文創大樓.md "wikilink")2-3F，除了書店和商場外，還有誠品電影院、誠品表演廳、誠品行旅。                      |
| 誠品R79                                   |                                                                                                                                                                                                                                                                                                                                                 | [臺北市](../Page/臺北市.md "wikilink")[大同區](../Page/大同區.md "wikilink")[南京西路](../Page/南京西路.md "wikilink")16號B1之[中山地下街B](../Page/中山地下街.md "wikilink")1-B48號店鋪（不含B24、B26號） | 位於台北市中山地下書街，全長約270公尺，是串聯[中山站](../Page/中山站_\(台北市\).md "wikilink")、[雙連站的樞紐](../Page/雙連站.md "wikilink")，也是全台最長書街。                         |
| 武昌店                                     |                                                                                                                                                                                                                                                                                                                                                 | [臺北市](../Page/臺北市.md "wikilink")[萬華區武昌街](../Page/萬華區.md "wikilink")2段77號                                                                                          | 位於台北市[西門町商圈](../Page/西門町.md "wikilink")。                                                                                               |
| 南西店                                     | [Eslite_spectrum_Nanxi.jpg](https://zh.wikipedia.org/wiki/File:Eslite_spectrum_Nanxi.jpg "fig:Eslite_spectrum_Nanxi.jpg")                                                                                                                                                                                                                     | [臺北市](../Page/臺北市.md "wikilink")[中山區](../Page/中山區.md "wikilink")[南京西路](../Page/南京西路.md "wikilink")14號                                                             | 位於台北市[中山站](../Page/中山站_\(台北市\).md "wikilink")[南西商圈](../Page/南西商圈.md "wikilink")。                                                       |
| 板橋店                                     |                                                                                                                                                                                                                                                                                                                                                 | [新北市](../Page/新北市.md "wikilink")[板橋區中山路一段](../Page/板橋區.md "wikilink")46號                                                                                          | 位於板橋[府中站附近](../Page/府中站.md "wikilink")。                                                                                                |
| 新板店                                     |                                                                                                                                                                                                                                                                                                                                                 | [新北市](../Page/新北市.md "wikilink")[板橋區縣民大道二段](../Page/板橋區.md "wikilink")66號1F-3F（[板信雙子星花園廣場](../Page/板信雙子星花園廣場.md "wikilink")1F-3F）                                 | 誠品生活新板店位於板橋[新板特區](../Page/新板特區.md "wikilink")[板信雙子星花園廣場](../Page/板信雙子星花園廣場.md "wikilink")，與[板橋站共構](../Page/板橋車站_\(臺灣\).md "wikilink")。 |
| 亞東醫院店                                   |                                                                                                                                                                                                                                                                                                                                                 | [新北市](../Page/新北市.md "wikilink")[板橋區](../Page/板橋區.md "wikilink")[南雅南路二段](../Page/南雅南路.md "wikilink")21號1F/B1（[亞東醫院](../Page/亞東醫院.md "wikilink")1F/B1\]）           | 誠品生活亞東醫院店位於板橋亞東醫院一樓、地下一樓，鄰近捷運[板南線](../Page/板南線.md "wikilink")[亞東醫院站](../Page/亞東醫院站.md "wikilink")。                                     |
| [勤美綠園道店](../Page/勤美誠品綠園道.md "wikilink") |                                                                                                                                                                                                                                                                                                                                                 | [台中市](../Page/台中市.md "wikilink")[西區公益路](../Page/西區_\(臺中市\).md "wikilink")68號                                                                                      |                                                                                                                                        |
| [站前店](../Page/誠品站前店.md "wikilink")      |                                                                                                                                                                                                                                                                                                                                                 | [臺北市](../Page/臺北市.md "wikilink")[中正區](../Page/中正區_\(臺北市\).md "wikilink")[忠孝西路一段](../Page/忠孝西路.md "wikilink")47號B1（臺北車站K區地下街）                                      |                                                                                                                                        |

#### 書店模式

<table>
<tbody>
<tr class="odd">
<td><p><strong>店名</strong></p></td>
<td><p><strong>圖片</strong></p></td>
<td><p><strong>地址</strong></p></td>
<td><p><strong>簡介</strong></p></td>
</tr>
<tr class="even">
<td><p>東湖店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/內湖區.md" title="wikilink">內湖區</a><a href="../Page/康寧路.md" title="wikilink">康寧路三段</a>72號1F/B1（<a href="../Page/東湖哈拉影城.md" title="wikilink">東湖哈拉影城</a>1F/B1）</p></td>
<td><p>誠品東湖店位於<a href="../Page/東湖哈拉影城.md" title="wikilink">東湖哈拉影城</a>1樓/B1，臨近捷運<a href="../Page/文湖線.md" title="wikilink">文湖線</a><a href="../Page/東湖站_(臺北市).md" title="wikilink">東湖站</a>，以社區書店為定位。</p></td>
</tr>
<tr class="odd">
<td><p>實踐店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/中山區_(臺北市).md" title="wikilink">中山區</a><a href="../Page/大直街.md" title="wikilink">大直街</a>70號1F/2F（<a href="../Page/實踐大學.md" title="wikilink">實踐大學綜合大樓</a>1F/2F）</p></td>
<td><p>誠品大直實踐店位於<a href="../Page/大直.md" title="wikilink">大直的</a><a href="../Page/實踐大學.md" title="wikilink">實踐大學校園內</a>。</p></td>
</tr>
<tr class="even">
<td><p>美麗華店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/中山區_(臺北市).md" title="wikilink">中山區</a><a href="../Page/敬業三路.md" title="wikilink">敬業三路</a>20號3F（<a href="../Page/美麗華百樂園.md" title="wikilink">美麗華百樂園漾館</a>3F）</p></td>
<td><p>誠品美麗華店位於<a href="../Page/美麗華百樂園.md" title="wikilink">美麗華百樂園</a>3樓，臨近捷運<a href="../Page/文湖線.md" title="wikilink">文湖線</a><a href="../Page/劍南路站.md" title="wikilink">劍南路站</a>。</p></td>
</tr>
<tr class="odd">
<td><p>士林店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/士林區.md" title="wikilink">士林區</a><a href="../Page/文林路.md" title="wikilink">文林路</a>338、340~1號1F（士林區農會大樓1F）</p></td>
<td><p>誠品士林店位於<a href="../Page/士林區農會大樓.md" title="wikilink">士林區農會大樓</a>1樓，臨近捷運<a href="../Page/淡水信義線.md" title="wikilink">淡水信義線</a><a href="../Page/士林站.md" title="wikilink">士林站</a>，附近學校聚集，鄰近夜市。</p></td>
</tr>
<tr class="even">
<td><p>敦南店</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Eslite_Bookstore_Dunnan_20130324.jpg" title="fig:Eslite_Bookstore_Dunnan_20130324.jpg">Eslite_Bookstore_Dunnan_20130324.jpg</a><a href="https://zh.wikipedia.org/wiki/File:Eslite_Bookstore_Dunnan_Interior_201507.jpg" title="fig:Eslite_Bookstore_Dunnan_Interior_201507.jpg">Eslite_Bookstore_Dunnan_Interior_201507.jpg</a></p></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/大安區.md" title="wikilink">大安區</a><a href="../Page/敦化南路.md" title="wikilink">敦化南路一段</a>245號</p></td>
<td><p>目前實行24小時營業。<br />
店租約將於2020年到期並且將歇業[9]，原址將改建飯店[10]。</p></td>
</tr>
<tr class="odd">
<td><p>忠孝SOGO店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/大安區.md" title="wikilink">大安區</a><a href="../Page/忠孝東路.md" title="wikilink">忠孝東路四段</a>45號12F（太平洋SOGO百貨臺北忠孝館12F）</p></td>
<td><p>誠品忠孝SOGO店位於太平洋SOGO百貨忠孝館12樓，以<a href="../Page/台北東區.md" title="wikilink">東區藏書閣為形象</a>，鄰近捷運<a href="../Page/忠孝復興站.md" title="wikilink">忠孝復興站</a>。</p></td>
</tr>
<tr class="even">
<td><p>台大店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/大安區.md" title="wikilink">大安區</a><a href="../Page/新生南路.md" title="wikilink">新生南路三段</a>98號B1~3F</p></td>
<td><p>誠品台大店位於<a href="../Page/台灣大學.md" title="wikilink">台灣大學正對面的</a><a href="../Page/公館商圈.md" title="wikilink">公館商圈</a>，可以眺望台灣大學。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/臺北車站地下商場.md" title="wikilink">臺北車站捷運店</a></p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/中正區_(臺北市).md" title="wikilink">中正區</a><a href="../Page/忠孝西路.md" title="wikilink">忠孝西路一段</a>49號B1（B、C、D室）</p></td>
<td><p>誠品臺北車站捷運店位於<a href="../Page/臺北車站.md" title="wikilink">臺北車站以南</a>、忠孝西路下層B1商區。</p></td>
</tr>
<tr class="even">
<td><p>西門店</p></td>
<td></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a><a href="../Page/萬華區.md" title="wikilink">萬華區</a><a href="../Page/峨嵋街.md" title="wikilink">峨嵋街</a>52號</p></td>
<td><p>誠品生活西門店位於老臺北鬧區文化特色的<a href="../Page/西門町.md" title="wikilink">西門町</a>，原為老電影院。</p></td>
</tr>
<tr class="odd">
<td><p>林口店</p></td>
<td></td>
<td><p><a href="../Page/新北市.md" title="wikilink">新北市</a><a href="../Page/林口區.md" title="wikilink">林口區</a><a href="../Page/文化三路.md" title="wikilink">文化三路一段</a>356號1F（<a href="../Page/MITSUI_OUTLET_PARK_林口.md" title="wikilink">MITSUI OUTLET PARK 林口</a>1F）</p></td>
<td><p>誠品生活林口店位於<a href="../Page/MITSUI_OUTLET_PARK_林口.md" title="wikilink">MITSUI OUTLET PARK 林口</a>1樓。</p></td>
</tr>
<tr class="even">
<td><p>雙和比漾店</p></td>
<td></td>
<td><p><a href="../Page/新北市.md" title="wikilink">新北市</a><a href="../Page/永和區.md" title="wikilink">永和區中山路一段</a>238號B1（比漾廣場B1）</p></td>
<td><p>誠品雙和比漾店位於<a href="../Page/比漾廣場.md" title="wikilink">比漾廣場B</a>1樓。</p></td>
</tr>
<tr class="odd">
<td><p>桃園統領店</p></td>
<td></td>
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a><a href="../Page/桃園區.md" title="wikilink">桃園區中正路</a>61號B1（<a href="../Page/桃園統領廣場.md" title="wikilink">桃園統領廣場B</a>1）</p></td>
<td><p>誠品桃園統領店位於<a href="../Page/桃園統領廣場.md" title="wikilink">桃園統領廣場B</a>1樓。</p></td>
</tr>
<tr class="even">
<td><p>桃園台茂店</p></td>
<td></td>
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a><a href="../Page/蘆竹區.md" title="wikilink">蘆竹區南崁路一段</a>112號6F（<a href="../Page/台茂購物中心.md" title="wikilink">台茂購物中心</a>6F）</p></td>
<td><p>誠品桃園台茂店位於台茂購物中心6樓，以「家居憩所 享讀時光」為定位。</p></td>
</tr>
<tr class="odd">
<td><p>中壢SOGO店</p></td>
<td></td>
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a><a href="../Page/中壢區.md" title="wikilink">中壢區元化路</a>357號10F（中壢SOGO百貨10F）</p></td>
<td><p>誠品中壢SOGO店位於中壢SOGO百貨10樓。</p></td>
</tr>
<tr class="even">
<td><p>中壢大江店</p></td>
<td></td>
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a><a href="../Page/中壢區.md" title="wikilink">中壢區中園路二段</a>501號4F（中壢<a href="../Page/大江國際購物中心.md" title="wikilink">大江國際購物中心</a>4F）</p></td>
<td><p>誠品中壢大江店位於<a href="../Page/大江國際購物中心.md" title="wikilink">大江國際購物中心</a>4樓。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Big_City遠東巨城購物中心.md" title="wikilink">新竹巨城店</a></p></td>
<td></td>
<td><p><a href="../Page/新竹市.md" title="wikilink">新竹市中央路</a>229號（Big City 遠東巨城購物中心5F）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>臺中大遠百店</p></td>
<td></td>
<td><p><a href="../Page/臺中市.md" title="wikilink">臺中市</a><a href="../Page/西屯區.md" title="wikilink">西屯區</a><a href="../Page/臺灣大道.md" title="wikilink">臺灣大道三段</a>251號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>臺中<a href="../Page/中友百貨.md" title="wikilink">中友店</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:ChungyoEslite_fullsize.png" title="fig:ChungyoEslite_fullsize.png">ChungyoEslite_fullsize.png</a>]]</p></td>
<td><p><a href="../Page/臺中市.md" title="wikilink">臺中市三民路三段</a>161號10F、11F（台中<a href="../Page/中友百貨.md" title="wikilink">中友百貨C棟</a>）</p></td>
<td><p>1997年開幕，2015年重新裝潢並引進咖啡黑膠專門店。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/MITSUI_OUTLET_PARK_台中港.md" title="wikilink">臺中三井店</a></p></td>
<td></td>
<td><p><a href="../Page/臺中市.md" title="wikilink">臺中市</a><a href="../Page/梧棲區.md" title="wikilink">梧棲區</a><a href="../Page/臺灣大道.md" title="wikilink">臺灣大道十段</a>168號</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金典綠園道.md" title="wikilink">臺中金典店</a></p></td>
<td></td>
<td><p>|<a href="../Page/臺中市.md" title="wikilink">臺中市</a><a href="../Page/西區_(臺中市).md" title="wikilink">西區健行路</a>1049號 (金典綠園道B1)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>虎尾店</p></td>
<td></td>
<td><p><a href="../Page/雲林縣.md" title="wikilink">雲林縣</a><a href="../Page/虎尾鎮.md" title="wikilink">虎尾鎮林森路一段</a>491號</p></td>
<td><p>位於歷史建築<a href="../Page/虎尾合同廳舍.md" title="wikilink">虎尾合同廳舍內</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德安百貨台南店.md" title="wikilink">文化中心店</a></p></td>
<td></td>
<td><p><a href="../Page/臺南市.md" title="wikilink">臺南市</a><a href="../Page/東區_(臺南市).md" title="wikilink">東區中華東路三段</a>360號</p></td>
<td><p>誠品生活文化中心店位於台南<a href="../Page/德安百貨台南店.md" title="wikilink">德安百貨</a>，左傍台南市立文化中心、右依巴克禮公園。</p></td>
</tr>
<tr class="even">
<td><p>臺南安平店</p></td>
<td></td>
<td><p>|<a href="../Page/臺南市.md" title="wikilink">臺南市</a><a href="../Page/安平區.md" title="wikilink">安平區文平路</a>207號（慈濟中小學旁）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南紡購物中心.md" title="wikilink">南紡店</a></p></td>
<td></td>
<td><p>|<a href="../Page/臺南市.md" title="wikilink">臺南市</a><a href="../Page/東區_(臺南市).md" title="wikilink">東區中華東路一段</a>366號2樓</p></td>
<td><p>誠品生活南紡店位於<a href="../Page/南紡購物中心.md" title="wikilink">南紡購物中心中</a>。</p></td>
</tr>
<tr class="even">
<td><p>高雄大遠百店</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Eslite_Bookstore_Kaohsiung_Chiayi_Store_2015.jpg" title="fig:Eslite_Bookstore_Kaohsiung_Chiayi_Store_2015.jpg">Eslite_Bookstore_Kaohsiung_Chiayi_Store_2015.jpg</a></p></td>
<td><p><a href="../Page/高雄市.md" title="wikilink">高雄市</a><a href="../Page/苓雅區.md" title="wikilink">苓雅區</a><a href="../Page/三多四路.md" title="wikilink">三多四路</a>21號17F（大遠百百貨17F）</p></td>
<td><p>誠品生活高雄遠百店設立於高雄大遠百百貨的最高樓層17樓，佔地1,000坪。鄰近<a href="../Page/高雄捷運.md" title="wikilink">高雄捷運</a><a href="../Page/三多商圈站.md" title="wikilink">三多商圈站</a>。</p></td>
</tr>
<tr class="odd">
<td><p>高雄SOGO店</p></td>
<td></td>
<td><p><a href="../Page/高雄市.md" title="wikilink">高雄市</a><a href="../Page/前鎮區.md" title="wikilink">前鎮區</a><a href="../Page/三多三路.md" title="wikilink">三多三路</a>217號12F ((高雄SOGO百貨12F)</p></td>
<td><p>誠品生活高雄SOGO店位於高雄SOGO百貨中，鄰近<a href="../Page/高雄捷運.md" title="wikilink">高雄捷運</a><a href="../Page/三多商圈站.md" title="wikilink">三多商圈站</a>。</p></td>
</tr>
<tr class="even">
<td><p>高雄醫學院店</p></td>
<td></td>
<td><p><a href="../Page/高雄市.md" title="wikilink">高雄市</a><a href="../Page/三民區.md" title="wikilink">三民區自由一路</a>100號B1/1F（<a href="../Page/高雄醫學大學.md" title="wikilink">高雄醫學大學附設中和紀念醫院B</a>1/1F）</p></td>
<td><p>誠品生活高雄醫學院店位於<a href="../Page/高雄醫學大學.md" title="wikilink">高雄醫學大學附設中和紀念醫院中</a>。</p></td>
</tr>
<tr class="odd">
<td><p>高雄駁二店</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Eslite_pier-2_Kaohsiung_store_201507.JPG" title="fig:Eslite_pier-2_Kaohsiung_store_201507.JPG">Eslite_pier-2_Kaohsiung_store_201507.JPG</a></p></td>
<td><p><a href="../Page/高雄市.md" title="wikilink">高雄市</a><a href="../Page/鹽埕區.md" title="wikilink">鹽埕區大勇路</a>3號 (駁二藝術特區C4倉庫)</p></td>
<td><p>於2015年7月開業，為臺灣最靠近海港的書店。</p></td>
</tr>
<tr class="even">
<td><p>屏東太百店</p></td>
<td></td>
<td><p><a href="../Page/屏東縣.md" title="wikilink">屏東縣</a><a href="../Page/屏東市.md" title="wikilink">屏東市中正路</a>72號7F（太平洋百貨7F）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>宜蘭店</p></td>
<td></td>
<td><p>|<a href="../Page/宜蘭縣.md" title="wikilink">宜蘭縣</a><a href="../Page/宜蘭市.md" title="wikilink">宜蘭市民權路二段</a>38巷6號3F（宜蘭<a href="../Page/新月廣場.md" title="wikilink">新月廣場</a>3F）</p></td>
<td><p>誠品生活宜蘭店位於宜蘭火車站前的新月廣場三樓，由建築師黃聲遠與田中央工作群所設計，大量採用紅磚、磨石子及枕木等當地素材。</p></td>
</tr>
<tr class="even">
<td><p>花蓮遠百店</p></td>
<td></td>
<td><p><a href="../Page/花蓮縣.md" title="wikilink">花蓮縣</a><a href="../Page/花蓮市.md" title="wikilink">花蓮市和平路</a>581號2F（遠東百貨2F）</p></td>
<td><p>2018年Q2期開幕</p></td>
</tr>
<tr class="odd">
<td><p>臺東店</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:臺東地政事務所舊址.jpg" title="fig:臺東地政事務所舊址.jpg">臺東地政事務所舊址.jpg</a></p></td>
<td><p><a href="../Page/台東縣.md" title="wikilink">台東縣</a><a href="../Page/台東市.md" title="wikilink">台東市博愛路</a>478號</p></td>
<td><p>於2007年9月開業，是臺東縣第一家連鎖書店，由<a href="../Page/臺東縣政府.md" title="wikilink">臺東縣政府文化局和誠品書店合作經營</a>。</p></td>
</tr>
</tbody>
</table>

#### 過往分店

| 店名                                        | 介紹                                                                                                            |
| ----------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| 天母中山店                                     | 原本位於[中山北路七段](../Page/中山北路.md "wikilink")，2002年歇業。                                                             |
| 站前店                                       | 為首家站前店，位於當時[大亞百貨](../Page/大亞百貨.md "wikilink")4樓，於2004年歇業。                                                     |
| 三重介壽店                                     | 2007年因租約到期歇業。                                                                                                 |
| 台大校園店                                     | 簡稱小福店；承租台灣大學小福利社三樓空間，2007年因客源受限於台大師生而不再續約。                                                                    |
| [信義A11店](../Page/新光三越百貨.md "wikilink")    | 2008年因租約到期歇業。                                                                                                 |
| [高雄漢神店店](../Page/漢神百貨.md "wikilink")      | 2008年因租約到期歇業。                                                                                                 |
| 永和店                                       | 2008年因租約到期歇業。                                                                                                 |
| [宜蘭友愛店](../Page/友愛百貨.md "wikilink")       | 2008年因租約到期歇業                                                                                                  |
| 誠品臺南店                                     | 位於[東方巨人大樓地下一層](../Page/東方巨人大樓.md "wikilink")～地上二層，因租約到期於2013年4月結束營業。                                          |
| 誠品Focus店                                  | 位於[focus時尚流行館](../Page/focus時尚流行館.md "wikilink")，因租約到期於2008年11月結束營業。                                          |
| 臺大醫院店                                     | 屬於醫院型商場，包含書店、美食街與商店街，2014年2月由[微風集團標得經營權](../Page/微風集團.md "wikilink")，現址為[微風臺大醫院](../Page/微風廣場.md "wikilink")。 |
| [台中龍心店](../Page/龍心百貨.md "wikilink")       | 2009年6月因績效不佳歇業                                                                                                |
| [嘉義三越店](../Page/新光三越百貨.md "wikilink")     | 2009年歇業                                                                                                       |
| 基隆東岸店                                     | 2010年因基隆市市政府片面解約，而被迫歇業。                                                                                       |
| [大統新世紀店](../Page/大統新世紀百貨.md "wikilink")   | 2014年12月31日隨著大統新世紀百貨熄燈而結束營業。                                                                                  |
| 天母忠誠店                                     | 2015年下半年租約到期結束長達19年的營業。                                                                                       |
| 劇場生活店                                     | 2016年6月結束營業                                                                                                   |
| [台中三越店](../Page/新光三越百貨.md "wikilink")     | 2016年7月結束營業                                                                                                   |
| 誠品新竹店                                     | 2016年下半年租約到期結束長達19年的營業                                                                                        |
| [高雄大立店](../Page/大立百貨.md "wikilink")       | 2017年8月27日因租約到期歇業                                                                                             |
| [高雄夢時代店](../Page/統一夢時代購物中心.md "wikilink") | 2017年8月31日因租約到期正式歇業                                                                                           |
| [高雄大統店](../Page/大統百貨.md "wikilink")       | 2018年因績效不佳於5月13日租約到期後結束營業                                                                                     |
| [桃園遠百店](../Page/遠東百貨.md "wikilink")       | 於2018年6月24日結束營業                                                                                               |
| 京華城店                                      | 位於[京華城內](../Page/京華城.md "wikilink")，結束營業時間待查                                                                  |

###

|        |                                                                                                                                                                                    |                                                                                                                                                                                                                                                                                                             |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **店名** | **圖片**                                                                                                                                                                             | **簡介**                                                                                                                                                                                                                                                                                                      |
| 香港太古店  | [Eslite_store_Cityplaza_201601.jpg](https://zh.wikipedia.org/wiki/File:Eslite_store_Cityplaza_201601.jpg "fig:Eslite_store_Cityplaza_201601.jpg")                               | 2015年2月26日，[太古城中心公佈指](../Page/太古城中心.md "wikilink")，誠品生活於2016年1月6日落戶一期[永安百貨舊址](../Page/永安百貨.md "wikilink")，佔地兩層超過4.9萬平方呎，以「家」為概念，有47個生活用品專櫃品牌及首設的手作互動空間「Handmade Studio」，成為誠品生活在香港的旗艦店。此店是繼銅鑼灣及尖沙咀店後，誠品於香港開設的第三間分店。到2017年第4季，1樓空間有大幅改動，原有的手作互動空間「Handmade Studio」取消、兒童館亦大幅縮細，部分書籍放在地下。原有範圍改為專櫃品牌。            |
| 香港尖沙咀店 | [Eslite_Tsim_Sha_Tsui_Store_201510.jpg](https://zh.wikipedia.org/wiki/File:Eslite_Tsim_Sha_Tsui_Store_201510.jpg "fig:Eslite_Tsim_Sha_Tsui_Store_201510.jpg")                 | 星光行2樓及3樓，星光城位於[星光行](../Page/星光行.md "wikilink")2至3樓（[海港城星光城](../Page/海港城.md "wikilink")）共3.9萬方呎樓面已於2015年10月1日開業，標榜有超過42個生活用品專櫃品牌。\[11\]                                                                                                                                                                      |
| 香港銅鑼灣店 | [Eslite_HK_Store_Level_8_Enterance_2012.jpg](https://zh.wikipedia.org/wiki/File:Eslite_HK_Store_Level_8_Enterance_2012.jpg "fig:Eslite_HK_Store_Level_8_Enterance_2012.jpg") | 2012年8月11日開業，為誠品的第一家香港分店，位於[銅鑼灣](../Page/銅鑼灣.md "wikilink")[希慎廣場](../Page/希慎廣場.md "wikilink")（前身為[興利中心與](../Page/興利中心.md "wikilink")[香港](../Page/香港.md "wikilink")[三越百貨舊址](../Page/三越百貨.md "wikilink")）8樓至10樓，合共4.1萬平方呎。在8月11日至9月16日逢周四至周六試行24小時營業，平日則於晚上11時關門。零售面積達3.6萬平方呎的香港誠品，其中85%賣書，其餘為餐廳、茶室及設計家品的售賣點。 |

###

|                                 |                                                                                                                                                      |                                                                                                                                                                          |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **店名**                          | **圖片**                                                                                                                                               | **簡介**                                                                                                                                                                   |
| 大陸蘇州店                           | [Eslite_Bookstore_Suzhou_Store.jpg](https://zh.wikipedia.org/wiki/File:Eslite_Bookstore_Suzhou_Store.jpg "fig:Eslite_Bookstore_Suzhou_Store.jpg") | 大陸[蘇州旗艦店座落於](../Page/蘇州.md "wikilink")[蘇州工業園區](../Page/蘇州工業園區.md "wikilink")[金雞湖](../Page/金雞湖.md "wikilink")，於2015年11月29日開幕，建築面積逾13萬平方米，更推出「誠品居所」，將其中5.3萬平方米分拆成292個單位出售。 |
| [上海店](../Page/上海.md "wikilink") |                                                                                                                                                      | [上海店選址世界第二高樓](../Page/上海.md "wikilink")[上海中心大廈地下](../Page/上海中心大廈.md "wikilink")、52及53樓。地下會定位為針對大眾的文化生活聚落，52及53樓則定位為針對白領與會員的文化主題大堂，預計每日可吸引3至4萬人次。                         |
| [深圳店](../Page/深圳.md "wikilink") |                                                                                                                                                      | 位於[深圳](../Page/深圳.md "wikilink")[南山華潤城](../Page/南山區.md "wikilink")「萬象天地」，於2018年12月15日開幕，總體面積逾33000平方米\[12\]。                                                             |

###

|          |        |                                                                                                                    |                                                                                          |
| -------- | ------ | ------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------- |
| **店名**   | **圖片** | **地址**                                                                                                             | **簡介**                                                                                   |
| 誠品生活日本橋店 |        | [東京都](../Page/東京都.md "wikilink")[中央區](../Page/中央區_\(東京都\).md "wikilink")[日本橋室町](../Page/日本橋室町.md "wikilink")２丁目1-1 | 2019年秋季月位於[日本橋室町三井塔大樓的](../Page/日本橋室町三井塔大樓.md "wikilink")1-3F誠品生活日本橋店開業，書店面積達900坪\[13\]。 |

## 獎項

### 誠品書店閱讀職人大賞

2012年起設立，分別針對成人圖書、青少年兒童圖書與作家設立獎項。

2015年起，由誠品書店的門市工作者與出版社推薦建立年度入選書單，再採誠品書店的門市工作者一人一票的方式，票選出心中「最想賣」、「最想幫孩子說服爸媽買」的2本書與年度「最期待作家」。\[14\]

|- | rowspan="3"|2012年||閱讀職人『最想買』 只是孩子
[帕蒂·史密斯](../Page/帕蒂·史密斯.md "wikilink")
[新經典文化](../Page/新經典文化.md "wikilink")||第一屆誠品書店閱讀職人大賞|| |- |
閱讀職人『最想賣』 飛踢，醜哭，白鼻毛：第一次開出版社就大賣 騙你的 陳夏民 明日工作室||第一屆誠品書店閱讀職人大賞|| |-
| 閱讀職人『最想幫孩子說服爸媽買』 祖父的祖父的祖父的祖父 長谷川義史 遠流出版公司||第一屆誠品書店閱讀職人大賞|| |- |
rowspan="3"|2013年||閱讀職人『最想買』 書店不死 石橋毅史
[時報文化](../Page/時報文化.md "wikilink")||第二屆誠品書店閱讀職人大賞|| |- |
閱讀職人『最想賣』 南風 鐘聖雄、許震唐 衛城出版||第二屆誠品書店閱讀職人大賞|| |- | 閱讀職人『最想幫孩子說服爸媽買』 這不是我的帽子
雍．卡拉森 Jon Klassen 親子天下||第二屆誠品書店閱讀職人大賞|| |- |
rowspan="4"|2014年||閱讀職人『最想賣』 S.
[J·J·亞柏拉罕](../Page/J·J·亞柏拉罕.md "wikilink")，道格‧道斯特 Doug Dorst
寂寞出版||第三屆誠品書店閱讀職人大賞|| |- | 閱讀職人『最想幫孩子說服爸媽買』 蠟筆大罷工 祖兒‧戴沃特 Drew Daywalt
遠流出版公司||第三屆誠品書店閱讀職人大賞|| |- | 閱讀職人臺灣『年度最期待作家』 女兒、小兒子
[駱以軍](../Page/駱以軍.md "wikilink") 印刻出版||第三屆誠品書店閱讀職人大賞|| |- |
閱讀職人香港『年度最期待作家』 微喜重行 [黃碧雲](../Page/黃碧雲.md "wikilink") 天地圖書
||第三屆誠品書店閱讀職人大賞|| |- | rowspan="4"|2015年||閱讀職人『最想賣』
敵人的櫻花 [王定國](../Page/王定國_\(作家\).md "wikilink") 印刻||第四屆誠品書店閱讀職人大賞|| |- |
閱讀職人『最想幫孩子說服爸媽買』 全世界最窮的總統爺爺來演講！ 艸場 よしみ 如何出版||第四屆誠品書店閱讀職人大賞|| |- |
閱讀職人臺灣『年度最期待作家』 單車失竊記 [吳明益](../Page/吳明益.md "wikilink")
[麥田出版](../Page/麥田出版.md "wikilink")||第四屆誠品書店閱讀職人大賞|| |- |
閱讀職人香港『年度最期待作家』 13•67、S.T.E.P 陳浩基
[皇冠文化](../Page/皇冠文化.md "wikilink") ||第四屆誠品書店閱讀職人大賞|| |-
| rowspan="9"|2016年||台灣閱讀職人『最想賣』 Design by wangzhihong.com
[王志弘](../Page/王志弘.md "wikilink") 臉譜出版|| 第五屆誠品書店閱讀職人大賞|| |- |
香港閱讀職人『最想賣』 博物學家的自然創世紀 安德列雅‧沃爾芙 果力文化||第五屆誠品書店閱讀職人大賞|| |- |
大陸閱讀職人『最想賣』 東京本屋 吉井忍 上海人民出版 ||第五屆誠品書店閱讀職人大賞|| |- |
台灣閱讀職人『最想幫孩子說服爸媽買』 小狐狸與星星 柯洛莉‧畢克佛史密斯
三采文化||第五屆誠品書店閱讀職人大賞|| |- | 香港閱讀職人『最想幫孩子說服爸媽買』 脫不下來啊！ 吉竹伸介 三采文化
||第五屆誠品書店閱讀職人大賞|| |- | 大陸閱讀職人『最想幫孩子說服爸媽買』 獨生小孩 郭婧 中信出版
||第五屆誠品書店閱讀職人大賞|| |- | 台灣閱讀職人『年度最期待作家』 第一人稱
[夏宇](../Page/夏宇.md "wikilink") 夏宇||第五屆誠品書店閱讀職人大賞|| |- |
香港閱讀職人『年度最期待作家』 龍頭鳳尾 [馬家輝](../Page/馬家輝.md "wikilink")
新經典文化||第五屆誠品書店閱讀職人大賞|| |- | 大陸閱讀職人『年度最期待作家』 繭 張悅然
人民文學出版||第五屆誠品書店閱讀職人大賞|| |}

## 爭議

  - 2014年6月，疑為了進軍中華人民共和國大陸市場，將部份涉及[西藏人權的書籍在香港分店下架](../Page/中華人民共和國人權.md "wikilink")，[佔領中環書籍亦於稍後下架](../Page/雨傘革命.md "wikilink")。\[15\]
  - 其中中國大陸流亡作家[袁紅冰的作品](../Page/袁紅冰.md "wikilink")《殺佛》在臺灣無法上架，包含[桃園](../Page/桃園機場.md "wikilink")、[松山機場和](../Page/松山機場.md "wikilink")[自由廣場書店都把書退回出版社](../Page/中正紀念堂.md "wikilink")。部份員工向誠品董事長吳清友發聯署信、辭職抗議。\[16\]而后誠品員工對政大教授徐世榮爆料，资方6月24日發出工聯單，主旨為「重申同仁言論發表限制」，誠品還將言論發表限制條文納入「聘僱契約書」中，明文規定限制員工對外發表「與公司有關」的言論或文章。\[17\]
  - 報橘評論文章指誠品向資本低頭的同時也跟著對統治威權低頭，形容“面對資本與政治交纏緊縛的中華人民共和國，我們不需要一紙服務貿易協定，就足以失去一個社會曾引以為傲的文化靈魂。”\[18\]
  - 2014年7月，[國立政治大學教授](../Page/國立政治大學.md "wikilink")[徐世榮在Facebook發表一張由誠品員工提供的內部公文](../Page/徐世榮.md "wikilink")\[19\]，將員工對外發言規範寫入聘雇契約書中，禁止對媒體發言的舉動，引發員工不滿和質疑\[20\]。
  - 2015年5月，誠品在[松山文創園區經營的商場](../Page/松山文創園區.md "wikilink")、展演空間等營業收入，並未納入市政府權利金\[21\]\[22\]，甚至引發一連串的「松菸文創爭議案\[23\]」。
  - 2015年11月，誠品生活在大陸第一個據點[蘇州](../Page/蘇州.md "wikilink")，除了商場之外、還附帶了高級住宅「誠品居所」販售\[24\]，被當地媒體質疑炒房\[25\]。

## 參見

  - [叶壹堂](../Page/叶壹堂.md "wikilink")
  - [方所](../Page/方所.md "wikilink")

## 參考文獻

## 外部連結

  -
  -
  -
  -
  -
  - [誠品畫廊
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/1864)

[2](../Category/中華民國證券櫃檯買賣中心上櫃公司.md "wikilink")
[T](../Category/臺北市書店.md "wikilink")
[T](../Category/台北市商場.md "wikilink")
[T](../Category/台中市商場.md "wikilink")
[T](../Category/台灣書店.md "wikilink")
[T](../Category/台灣音樂零售商.md "wikilink")
[T](../Category/蘇州書店.md "wikilink")
[T](../Category/上海書店.md "wikilink")
[T](../Category/深圳書店.md "wikilink")
[T](../Category/香港書店.md "wikilink")
[T](../Category/1989年台灣建立.md "wikilink")
[T](../Category/1989年成立的公司.md "wikilink")
[T](../Category/總部在台灣的跨國公司.md "wikilink")

1.

2.

3.
    蘋果日報|url=[https://tw.appledaily.com/new/realtime/20180412/1332859/|work=蘋果日報|date=2018年4月12日|language=zh-TW](https://tw.appledaily.com/new/realtime/20180412/1332859/%7Cwork=蘋果日報%7Cdate=2018年4月12日%7Clanguage=zh-TW)}}

4.

5.

6.  [將吹熄燈號？誠品敦南發聲明：2020年前照常營運](http://www.setn.com/News.aspx?NewsID=122348)。

7.  [首頁 新聞總覽
    敦南誠品確定掰掰了　書店將變飯店](https://news.housefun.com.tw/news/article/139199197204.html)。

8.

9.  [將吹熄燈號？誠品敦南發聲明：2020年前照常營運](http://www.setn.com/News.aspx?NewsID=122348)。

10. [首頁 新聞總覽
    敦南誠品確定掰掰了　書店將變飯店](https://news.housefun.com.tw/news/article/139199197204.html)。

11.

12.
13.

14. [王定國敵人的櫻花
    店員最想賣](http://www.chinatimes.com/newspapers/20151223000503-260115)，中國時報，2015年12月23日。

15.

16. [誠品限制員工言論
    學者嘆讓人失望](http://www.stormmediagroup.com/opencms/news/detail/042f8150-019e-11e4-8645-ef2804cba5a1/?uuid=042f8150-019e-11e4-8645-ef2804cba5a1)
    ，風傳媒 2014-07-02（2014-07-05查閱）。

17.
18. [再見。誠品書店。再見。](http://buzzorange.com/2014/07/04/wheres-the-old-eslite-bookstore/)，BuzzOrange
    2014-07-04（2014-07-05查阅）

19.

20.

21.

22.

23.

24.

25.