[ASEAN_Nations_Flags_in_Jakarta_3.jpg](https://zh.wikipedia.org/wiki/File:ASEAN_Nations_Flags_in_Jakarta_3.jpg "fig:ASEAN_Nations_Flags_in_Jakarta_3.jpg")东盟秘书处的各成员国国旗\]\]
**东南亚国家联盟**（，英文簡稱**ASEAN**），中文简称**东-{}-盟**、**亚-{}-细-{}-安**、**東-{}-南-{}-亞-{}-國-{}-協**或**东-{}-協**，是集合[东南亚区域](../Page/东南亚.md "wikilink")[国家的一个政府性](../Page/国家.md "wikilink")[国际组织](../Page/国际组织.md "wikilink")。\[1\]

東盟成立初期，基于[冷战背景立場](../Page/冷战.md "wikilink")[反共](../Page/反共.md "wikilink")，主要任務之一為防止區域內[共產主義勢力擴張](../Page/共產主義.md "wikilink")，合作側重在軍事安全與政治中立。[冷戰結束後东南亚各國政經情勢趨穩](../Page/冷戰.md "wikilink")，並接納[越南社會主義共和國等加入](../Page/越南社會主義共和國.md "wikilink")。

## 成员国

东盟目前共有10个正式的成员国，另外还有一个候选国和一个观察国：

  - 成员国（合称东盟十国）
      - （创始国）

      - （创始国）

      - （创始国）

      - （创始国）

      - （创始国）\[2\]

      -
      -
      -
      -
      -
  - 候选国
      -
  - 观察国
      - （自1976年起）

## 歷史

  - 1961年7月31日，[印尼](../Page/印尼.md "wikilink")、[马来亚](../Page/马来亚联合邦.md "wikilink")、[泰国和](../Page/泰国.md "wikilink")[菲律宾在](../Page/菲律宾.md "wikilink")[曼谷成立](../Page/曼谷.md "wikilink")“东南亚联盟”。
  - 1963年，[马来西亚和](../Page/马来西亚.md "wikilink")[菲律宾因为领土问题断交](../Page/菲律宾.md "wikilink")。1965年8月，[新加坡](../Page/新加坡.md "wikilink")、[马来西亚分治](../Page/马来西亚.md "wikilink")，联盟由此陷于瘫痪。
  - 1967年8月6日，立場[反共的](../Page/反共.md "wikilink")[印尼](../Page/印尼.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[泰国五国外长在](../Page/泰国.md "wikilink")[曼谷举行会议](../Page/曼谷.md "wikilink")，于8月8日发表了《[东盟宣言](../Page/东盟宣言.md "wikilink")》，正式宣告東盟恢復運作并更名為“東南亞國家聯盟”。
  - 1976年，在[峇里島举行的东南亚国家联盟第一次首脑会议签署了](../Page/峇里島.md "wikilink")《[东南亚友好合作条约](../Page/东南亚友好合作条约.md "wikilink")》和《[亚细安协调一致宣言](../Page/東盟協調一致宣言.md "wikilink")》，也就是《[峇里第一協約](../Page/峇里第一協約.md "wikilink")》，确定了東盟的宗旨和原则，成為发展的重要里程碑。
  - 1977年8月，第二次东盟首脑会议在吉隆坡举行。\[3\]
  - 1984年1月8日，[文莱独立后加入東盟](../Page/文莱.md "wikilink")，至此，東盟有6个成员国。由于后来其他东南亚4个国家加入东盟，这6个成员称为原東盟成员，或東盟老成员。
  - 1995年7月28日，[越南加入](../Page/越南.md "wikilink")。
  - 1997年7月23日，[缅甸加入](../Page/缅甸.md "wikilink")。
  - 1997年7月23日，[老挝加入](../Page/老挝.md "wikilink")。
  - 1999年4月30日，[柬埔寨加入](../Page/柬埔寨.md "wikilink")。
  - 2006年7月，[东帝汶提出申請加入東盟](../Page/东帝汶.md "wikilink")。另[巴布亞新畿內亞為東盟觀察員](../Page/巴布亞新畿內亞.md "wikilink")。
  - 2007年8月8日，為慶祝東盟成立40週年，特定當天為东盟日。11月20日，東協十國元首在新加坡簽署《[東盟憲章](../Page/東盟憲章.md "wikilink")》。
  - 2009年4月11日，[芭堤雅東盟會議成員所下榻兩間酒店遭親](../Page/芭堤雅.md "wikilink")[-{zh-hans:他信;zh-hk:他信;zh-tw:塔克辛;}-反](../Page/他信.md "wikilink")[-{zh-hk:阿披實;zh-hans:阿披实;zh-tw:艾比希;}-](../Page/阿披實.md "wikilink")[紅衫軍群眾嚴重干擾而腰斬](../Page/紅衫軍.md "wikilink")。
  - 2010年，与中国建立[中国-东盟自由贸易区](../Page/中国-东盟自由贸易区.md "wikilink")，形成「東協加一」規模為全球人囗數最多，開發中國家最大的自由貿易區。
  - 2012年11月18日，东盟十国签署了象征[人权领域合作成果的](../Page/人权.md "wikilink")《亚细安人权宣言》。《宣言》旨在保障本区域人民的人权，以作为未来人权合作的基本框架。\[4\]
  - 2015年11月22日，東盟十國於馬來西亞召開的第二十七屆高峰會簽署共同聲明，成立[東南亞經濟共同體](../Page/東南亞經濟共同體.md "wikilink")（AEC）。共同體將於同年12月31日正式上路。\[5\]

## 東盟會議

### 正式會議

<table>
<caption>東盟正式會議</caption>
<thead>
<tr class="header">
<th><p>屆</p></th>
<th><p>日期</p></th>
<th><p>國家</p></th>
<th><p>城市/地點</p></th>
<th><p>主持</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1976年2月23－24日</p></td>
<td></td>
<td><p><a href="../Page/峇里.md" title="wikilink">峇里</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1977年8月4－5日</p></td>
<td></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>1987年12月14－15日</p></td>
<td></td>
<td><p><a href="../Page/馬尼拉.md" title="wikilink">馬尼拉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>1992年1月27－29日</p></td>
<td></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>1995月12月14－15日</p></td>
<td></td>
<td><p><a href="../Page/曼谷.md" title="wikilink">曼谷</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>1998年12月15－16日</p></td>
<td></td>
<td><p><a href="../Page/河內.md" title="wikilink">河內</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2001年11月5－6日</p></td>
<td></td>
<td><p><a href="../Page/斯里巴加灣.md" title="wikilink">斯里巴加灣</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2002年11月4－5日</p></td>
<td></td>
<td><p><a href="../Page/金邊.md" title="wikilink">金邊</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2003年10月7－8日</p></td>
<td></td>
<td><p><a href="../Page/峇里.md" title="wikilink">峇里</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2004年11月29－30日</p></td>
<td></td>
<td><p><a href="../Page/永珍.md" title="wikilink">永珍</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2005年12月12－14日</p></td>
<td></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2007年1月11－14日<sup>1</sup></p></td>
<td><p><sup>2</sup></p></td>
<td><p><a href="../Page/曼達維市.md" title="wikilink">曼達維市</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2007年11月18－22日</p></td>
<td></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14<sup>3</sup></p></td>
<td><p>2009年2月27日－3月1日<br />
2009年4月10－11日</p></td>
<td></td>
<td><p><a href="../Page/清邁.md" title="wikilink">清邁</a>、<a href="../Page/華欣.md" title="wikilink">華欣</a><br />
<a href="../Page/芭達雅.md" title="wikilink">芭達雅</a></p></td>
<td><p><a href="../Page/阿披实·威差奇瓦.md" title="wikilink">阿披实·威差奇瓦</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2009年10月23日</p></td>
<td></td>
<td><p><a href="../Page/清邁.md" title="wikilink">清邁</a>、<a href="../Page/華欣.md" title="wikilink">華欣</a></p></td>
<td><p><a href="../Page/阿披实·威差奇瓦.md" title="wikilink">阿披实·威差奇瓦</a></p></td>
</tr>
<tr class="even">
<td><p>16<sup>3</sup></p></td>
<td><p>2010年4月8－9日</p></td>
<td></td>
<td><p><a href="../Page/河内.md" title="wikilink">河内</a></p></td>
<td><p><a href="../Page/阮晉勇.md" title="wikilink">阮晉勇</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>2010年10月28－31日</p></td>
<td></td>
<td><p><a href="../Page/河内.md" title="wikilink">河内</a></p></td>
<td><p><a href="../Page/阮晉勇.md" title="wikilink">阮晉勇</a></p></td>
</tr>
<tr class="even">
<td><p>18<sup>4</sup></p></td>
<td><p>2011年5月7－8日</p></td>
<td></td>
<td><p><a href="../Page/雅加达.md" title="wikilink">雅加达</a></p></td>
<td><p><a href="../Page/苏西洛·班邦·尤多约诺.md" title="wikilink">苏西洛·班邦·尤多约诺</a></p></td>
</tr>
<tr class="odd">
<td><p>19<sup>4</sup></p></td>
<td><p>2011年10月21－23日</p></td>
<td></td>
<td><p><a href="../Page/峇里.md" title="wikilink">峇里</a></p></td>
<td><p><a href="../Page/苏西洛·班邦·尤多约诺.md" title="wikilink">苏西洛·班邦·尤多约诺</a></p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>2012年4月3－4日</p></td>
<td></td>
<td><p><a href="../Page/金邊.md" title="wikilink">金邊</a></p></td>
<td><p><a href="../Page/洪森.md" title="wikilink">洪森</a></p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>2012年11月17－20日</p></td>
<td></td>
<td><p><a href="../Page/金邊.md" title="wikilink">金邊</a></p></td>
<td><p><a href="../Page/洪森.md" title="wikilink">洪森</a></p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>2013年4月24－25日</p></td>
<td></td>
<td><p><a href="../Page/斯里巴加湾.md" title="wikilink">斯里巴加湾</a></p></td>
<td><p><a href="../Page/哈桑纳尔·博尔基亚.md" title="wikilink">哈桑纳尔·博尔基亚</a></p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>2013年10月9日</p></td>
<td></td>
<td><p><a href="../Page/斯里巴加湾.md" title="wikilink">斯里巴加湾</a></p></td>
<td><p><a href="../Page/哈桑纳尔·博尔基亚.md" title="wikilink">哈桑纳尔·博尔基亚</a></p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>2014年5月10－13日</p></td>
<td></td>
<td><p><a href="../Page/奈比多.md" title="wikilink">奈比多</a></p></td>
<td><p><a href="../Page/登盛.md" title="wikilink">登盛</a></p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>2014年11月10－12日</p></td>
<td></td>
<td><p><a href="../Page/奈比多.md" title="wikilink">奈比多</a></p></td>
<td><p><a href="../Page/登盛.md" title="wikilink">登盛</a></p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>2015年4月26－27日</p></td>
<td></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a>、<a href="../Page/浮羅交怡.md" title="wikilink">浮羅交怡</a></p></td>
<td><p><a href="../Page/纳吉·阿都拉萨.md" title="wikilink">纳吉·阿都拉萨</a></p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>2015年11月18－22日</p></td>
<td></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a></p></td>
<td><p><a href="../Page/纳吉·阿都拉萨.md" title="wikilink">纳吉·阿都拉萨</a></p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>2016年9月6－8日</p></td>
<td></td>
<td><p><a href="../Page/永珍.md" title="wikilink">永珍</a></p></td>
<td><p><a href="../Page/通伦·西苏里.md" title="wikilink">通伦·西苏里</a></p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>2016年9月6－8日</p></td>
<td></td>
<td><p><a href="../Page/永珍.md" title="wikilink">永珍</a></p></td>
<td><p><a href="../Page/通伦·西苏里.md" title="wikilink">通伦·西苏里</a></p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p>2017年4月28－29日</p></td>
<td></td>
<td><p><a href="../Page/帕賽市.md" title="wikilink">帕賽市</a></p></td>
<td><p><a href="../Page/杜特蒂.md" title="wikilink">杜特蒂</a></p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p>2017年11月10－14日</p></td>
<td></td>
<td><p><a href="../Page/帕賽市.md" title="wikilink">帕賽市</a></p></td>
<td><p><a href="../Page/杜特蒂.md" title="wikilink">杜特蒂</a></p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p>2018年4月25－28日</p></td>
<td></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td><p><a href="../Page/李顯龍.md" title="wikilink">李顯龍</a></p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p>2018年11月11－15日</p></td>
<td></td>
<td><p><a href="../Page/新加坡.md" title="wikilink">新加坡</a></p></td>
<td><p><a href="../Page/李顯龍.md" title="wikilink">李顯龍</a></p></td>
</tr>
<tr class="even">
<td><p><sup>1</sup> 原訂於2006年12月10－14日舉行，後因<a href="../Page/颱風尤特_(2006年).md" title="wikilink">颱風尤特而推遲</a>。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>2</sup> 由於緬甸受美國及歐盟巨大壓力而退出，改由菲律賓主辦會議</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>3</sup> 會議由兩部分組成。<br />
第一部分由原訂2008年12月12－17日改期，因為<a href="../Page/2008年泰國政治危機.md" title="wikilink">2008年泰國政治危機</a>。<br />
第二部分在4月11日取消，因為有示威者進入會場。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>4</sup> 由於2013年要主辦<a href="../Page/亞太經合組織.md" title="wikilink">亞太經合組織會議</a>（以及有可能主辦<a href="../Page/二十國集團.md" title="wikilink">二十國集團會議</a>，儘管最終落入<a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a>），透過與對調年份，連續主辦兩次。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 區域論壇

東盟區域論壇（英文:ASEAN Regional
Forum，簡稱ARF）是亞太地區正式的，官方的，多邊的對話。截至2007年7月，它包括二十七名參加者。
ARF的目標是促進對話和磋商，並促進該地區的建立信任和預防性外交。\[6\]
ARF第一次在1994年舉行會議。ARF目前的參與者是：所有東盟成員國，澳大利亞，孟加拉國，加拿大，中華人民共和國，歐盟，印度，日本，朝鮮，韓國，蒙古，新西蘭，巴基斯坦，巴布亞新幾內亞，俄羅斯，東帝汶，美國和斯里蘭卡。\[7\]

[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）自ARF成立以來就被排除在外，[台灣海峽問題既沒有在ARF會議討論](../Page/台灣海峽問題.md "wikilink")，也沒有在ARF主席聲明中說明。

## 東盟十加三

東盟成立之初，視正處於“[文革](../Page/文革.md "wikilink")”浪潮中的[中华人民共和国為](../Page/中华人民共和国.md "wikilink")[共產主義擴展的嚴重威脅](../Page/共產主義.md "wikilink")，而中华人民共和国則視東盟為[反共集團](../Page/反共.md "wikilink")\[8\]\[9\]\[10\]，但在1972年[美國總統](../Page/美國總統.md "wikilink")[尼克森正式訪问中华人民共和国](../Page/尼克森.md "wikilink")，[中美關係正常化後](../Page/中美關係.md "wikilink")，東盟各成員國亦開始陸續追随美国與中华人民共和国建交，並解除對華貿易禁令。隨著[東歐劇變](../Page/東歐劇變.md "wikilink")、[蘇聯解體與](../Page/蘇聯解體.md "wikilink")[冷戰結束](../Page/冷戰.md "wikilink")，中华人民共和国於1996年成为東盟全面对话伙伴，与[日本](../Page/日本.md "wikilink")、[韩国和一样透过](../Page/大韩民国.md "wikilink")“[東協十加三会议](../Page/東協十加三.md "wikilink")”与东盟成员国进行共同协商。

## 東亞自由貿易區

2004年11月29日中华人民共和国與東盟在[老挝首都](../Page/老挝.md "wikilink")[永珍簽署](../Page/永珍.md "wikilink")「中國－東盟全面經濟合作框架協議貨物貿易協議」，朝推動成立自由貿易協議區（東盟十加一）的方向推進。為達到2010年[中國―东盟自由贸易区物流零關稅的目標](../Page/中國―东盟自由贸易区.md "wikilink")，雙方決定自2005年開始，針對部分貨品開始協商免稅，再逐漸擴大到2010年時達到全面免稅的目標。另一方面，[日本與](../Page/日本.md "wikilink")[韓國也宣布將自](../Page/韓國.md "wikilink")2005年開始，與東協十國協商自由貿易區談判，以作為成立[東亞自由貿易區](../Page/東亞自由貿易區.md "wikilink")（十加三）的起步。

## 体育

### 東南亞運動會

东南亚运动会是一项每两年举办一次的大型综合性地区[体育赛事](../Page/体育.md "wikilink")，参赛的国家包括了东南亚地区的11个国家，赛事由[国际奥委会和](../Page/国际奥委会.md "wikilink")[亚洲奥林匹克理事会监督的](../Page/亚洲奥林匹克理事会.md "wikilink")[东南亚运动会联盟管理](../Page/东南亚运动会联盟.md "wikilink")。东南亚运动会目前已具有相当规模，对整个东南亚地区体育运动的开展，起到了积极的推动作用。

### 東南亞足球錦標賽

东南亚足球锦标赛是每两年举办一次的东南亚地区[足球锦标赛](../Page/足球.md "wikilink")，由[亚细安足球协会主办](../Page/亚细安足球协会.md "wikilink")，参赛国来自东南亚地区，首届赛事于1996年举行，当时獲得啤酒品牌[虎牌啤酒的贊助](../Page/虎牌啤酒.md "wikilink")，因此賽事被冠名為[虎牌杯](../Page/虎牌杯.md "wikilink")，直至2006年起賽事更名为东南亚足球锦标赛。

## 成效與局限

至20世紀末，東盟在地區經濟合作方面成效不算明顯，因為各成員國主要關心對自己國家有利的經濟計劃，對本國無利益的計畫都不大關心，事實上，東盟部份成員國財富差距甚大（例如新加坡、汶萊和緬甸、老撾、柬埔寨的經濟狀況各走極端），因而難以制訂一個能顧全全部成員國的經濟合作計畫。

此外，東盟規條規定不干涉成員國內政，1990年緬甸軍政府不承認選舉落敗的結果，更軟禁了昂山素姬，但是緬甸仍可於1997年加入東盟。

東盟再維護東南亞地區和平方面取得較大成效，早在1978年越南入侵柬埔寨時，東盟五個始創成員國（印尼，泰國，新加坡，菲律賓，馬來西亞）已不定時舉行聯合軍事演習，最終更成功使越南於1989年從柬埔寨撤兵。

1984年1月1日，汶萊獨立，1月7日加入東盟，成為東盟第六個成員國，六國於1986年合作，舉辦東盟旅遊年，推廣東南亞旅遊業，其後合作增大，容納過往關係疏離的成員國（老撾，緬甸，柬埔寨）甚至是曾經敵對的越南，至今除東帝汶外，所有東南亞國家已經加入東盟，東盟也不再滿足於區域合作，找尋新的合作夥伴，例如印度，中國，日本，韓國，美國，歐盟等，東南亞國家也因而受到世界尊重，此為東盟的成效。

## 注釋

## 参考文献

## 外部链接

  - [ASEAN Secretariat - official website](http://www.asean.org)
  - [ASEAN News
    Network](https://web.archive.org/web/20140814191745/http://www.aseannewsnetwork.com/)
  - [东-{}-盟概况 - 新华网](http://www.xinhuanet.com/world/wjb0411/)

## 参见

  - [东盟成员国列表](../Page/东盟成员国列表.md "wikilink")
  - [中国－东盟自由贸易区](../Page/中国－东盟自由贸易区.md "wikilink")
  - [中國—東協科技產業合作委員會](../Page/中國—東協科技產業合作委員會.md "wikilink")
  - [东南亚条约组织](../Page/东南亚条约组织.md "wikilink")
  - [北美自由貿易協定](../Page/北美自由貿易協定.md "wikilink")
  - [歐盟](../Page/歐盟.md "wikilink")
  - [北大西洋公約組織](../Page/北大西洋公約組織.md "wikilink")
  - [世界貿易組織](../Page/世界貿易組織.md "wikilink")

{{-}}

[Category:1967年建立的組織](../Category/1967年建立的組織.md "wikilink")
[亞細安](../Category/亞細安.md "wikilink")
[Category:亚洲国家政府间经济组织](../Category/亚洲国家政府间经济组织.md "wikilink")
[Category:麥格塞塞獎獲得者](../Category/麥格塞塞獎獲得者.md "wikilink")
[Category:超国家联盟](../Category/超国家联盟.md "wikilink")

1.  [東協峰會月底在泰召開](http://udn.com/NEWS/WORLD/BREAKINGNEWS5/4728941.shtml)
2.
3.  陆伟：“当代亚洲地区国家政治与经济”，Dangdai Yazhoudiqu Guojiazhengzhi Yu
    Jingji，北京：中国社会科学出版社，2012.12，p.227
4.
5.  <http://udn.com/news/story/6811/1330109-%E6%9D%B1%E5%8D%97%E4%BA%9E%E7%B6%93%E6%BF%9F%E5%85%B1%E5%90%8C%E9%AB%94%E4%BB%8A%E6%88%90%E7%AB%8B-%E5%B9%B4%E5%BA%95%E4%B8%8A%E8%B7%AF>
6.  [About Us](http://www.aseanregionalforum.org/Default.aspx?tabid=49)
    , [ASEAN Regional Forum official
    website](http://www.aseanregionalforum.org/). Retrieved 12 June
    2006.
7.
8.  [「中國大陸－東-{}-盟自由貿易區」的政治經濟分析](http://www.crossstrait.org/version1/subpage7/200503/digest_list.php?number=14)

9.  [中華人民共和國東盟15年不僅是消除敵意](http://zhgpl.com/doc/1002/4/0/9/100240922.html?coluid=7&kindid=0&docid=100240922)

10. 中華人民共和國與東南亞關係研究 區域安全系列分析之二：從對立到協作的發展路徑