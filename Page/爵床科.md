**爵床科**是[双子叶植物纲中一个大](../Page/双子叶植物纲.md "wikilink")[科](../Page/科.md "wikilink")，大约包括250個[属](../Page/属.md "wikilink")，2500餘[种植物](../Page/种.md "wikilink")。爵床科的模式屬為[老鼠簕屬](../Page/老鼠簕屬.md "wikilink")（*Acanthus*）。

爵床科[植物大部分生长在](../Page/植物.md "wikilink")[热带地区](../Page/热带.md "wikilink")，主要是[草本](../Page/草本.md "wikilink")、[灌木](../Page/灌木.md "wikilink")，也有一些[藤本或多刺植物](../Page/藤本.md "wikilink")，只有少数种类可以生长在[温带地区](../Page/温带.md "wikilink")，分布最广的地区为[南亚](../Page/南亚.md "wikilink")、[中南半岛](../Page/中南半岛.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[巴西和](../Page/巴西.md "wikilink")[中美洲](../Page/中美洲.md "wikilink")，在各种生境中都有分布，例如密林中、开阔[森林中](../Page/森林.md "wikilink")、灌木丛中、[湿地中](../Page/湿地.md "wikilink")、峡谷中、[沼泽中](../Page/沼泽.md "wikilink")、海岸边、沿海[红树林中等都能找到它们的踪迹](../Page/红树.md "wikilink")。

爵床科植物一般为单[叶对生](../Page/叶.md "wikilink")，叶中一般含有[钙质](../Page/钙.md "wikilink")[钟乳体](../Page/钟乳体.md "wikilink")，所以看起来叶表面都有条纹。[花是完整的](../Page/花.md "wikilink")，但花为两侧对称或辐射对称的，一般为[总状花序或](../Page/总状花序.md "wikilink")[伞状花序](../Page/伞状花序.md "wikilink")，尤其是有典型的带[颜色的苞片](../Page/颜色.md "wikilink")，有的品种苞片相当大和鲜艳。[花萼](../Page/花萼.md "wikilink")4裂或5裂，管状[花冠](../Page/花冠.md "wikilink")5裂或2裂，雄蕊成对，子房两室，一般朔果有两个种子，容易爆裂。大部分种类种子有一个种钩，用于从朔果中弹出。种子没有胚乳，具大胚芽。

温带最常见的是生长在[欧洲南部的虾蟆花](../Page/欧洲.md "wikilink")（也叫莨力花、叶蓟），高达2[米](../Page/米.md "wikilink")，大叶。

[海榄雌属](../Page/海榄雌属.md "wikilink")（*Avicennia*）有时放到[马鞭草科或自成](../Page/马鞭草科.md "wikilink")[海欖雌科](../Page/海欖雌科.md "wikilink")，但根据[分子生物学APG的分类体系](../Page/分子生物学.md "wikilink")，将其分入本科。

## 属

据[美国种质资源信息网络](../Page/美国种质资源信息网络.md "wikilink")（Germplasm Resources
Information Network，簡稱GRIN）的分类，該科共有246[属](../Page/属.md "wikilink")：

  - [刺林草属](../Page/刺林草属.md "wikilink") *Acanthopale* C. B. Clarke
  - *[Acanthopsis](../Page/Acanthopsis.md "wikilink")* Harv.
  - *[Acanthostelma](../Page/Acanthostelma.md "wikilink")* Bidgood &
    Brummitt
  - *[Acanthura](../Page/Acanthura.md "wikilink")* Lindau
  - [老鼠簕属](../Page/老鼠簕属.md "wikilink") *Acanthus* L.
  - *[Achyrocalyx](../Page/Achyrocalyx.md "wikilink")* Benoist
  - [鸭咀花属](../Page/鸭咀花属.md "wikilink") *Adhatoda* Mill.
  - *[Afrofittonia](../Page/Afrofittonia.md "wikilink")* Lindau
  - *[Ambongia](../Page/Ambongia.md "wikilink")* Benoist
  - *[Ancistranthus](../Page/Ancistranthus.md "wikilink")* Lindau
  - *[Ancistrostylis](../Page/Ancistrostylis.md "wikilink")* T. Yamaz.
  - [穿心莲属](../Page/穿心莲属.md "wikilink") *Andrographis* Wall. ex Nees
  - *[Angkalanthus](../Page/Angkalanthus.md "wikilink")* Balf. f.
  - *[Anisacanthus](../Page/Anisacanthus.md "wikilink")* Nees
  - *[Anisosepalum](../Page/Anisosepalum.md "wikilink")* E. Hossain
  - *[Anisostachya](../Page/Anisostachya.md "wikilink")* Nees
  - *[Anisotes](../Page/Anisotes.md "wikilink")* Nees
  - *[Anomacanthus](../Page/Anomacanthus.md "wikilink")* R. D. Good
  - *[Apassalus](../Page/Apassalus.md "wikilink")* Kobuski
  - *[Aphanosperma](../Page/Aphanosperma.md "wikilink")* T. F. Daniel
  - [担药花属](../Page/担药花属.md "wikilink") *Aphelandra* R. Br.
  - *[Aphelandrella](../Page/Aphelandrella.md "wikilink")* Mildbr.
  - *[Ascotheca](../Page/Ascotheca.md "wikilink")* Heine
  - [十万错属](../Page/十万错属.md "wikilink") *Asystasia* Blume
  - [白接骨属](../Page/白接骨属.md "wikilink") *Asystasiella* Lindau
  - *[Ballochia](../Page/Ballochia.md "wikilink")* Balf. f.
  - [假杜鹃属](../Page/假杜鹃属.md "wikilink") *Barleria* L.
  - *[Barleriola](../Page/Barleriola.md "wikilink")* Oerst.
  - *[Benoicanthus](../Page/Benoicanthus.md "wikilink")* Heine & A.
    Raynal
  - [赛山蓝属](../Page/赛山蓝属.md "wikilink") *Blechum* P. Browne
  - [百簕花属](../Page/百簕花属.md "wikilink") *Blepharis* Juss.
  - *[Borneacanthus](../Page/Borneacanthus.md "wikilink")* Bremek.
  - *[Boutonia](../Page/Boutonia.md "wikilink")* DC.
  - *[Brachystephanus](../Page/Brachystephanus.md "wikilink")* Nees
  - *[Bravaisia](../Page/Bravaisia.md "wikilink")* DC.
  - *[Brillantaisia](../Page/Brillantaisia.md "wikilink")* P. Beauv.
  - *[Calacanthus](../Page/Calacanthus.md "wikilink")* T. Anderson ex
    Benth. & Hook. f.
  - [杜根藤属](../Page/杜根藤属.md "wikilink") *Calophanoides* (C. B. Clarke)
    Ridl.
  - *[Calycacanthus](../Page/Calycacanthus.md "wikilink")* K. Schum.
  - *[Camarotea](../Page/Camarotea.md "wikilink")* Scott-Elliot
  - *[Carlowrightia](../Page/Carlowrightia.md "wikilink")* A. Gray
  - *[Celerina](../Page/Celerina.md "wikilink")* Benoist
  - *[Cephalacanthus](../Page/Cephalacanthus.md "wikilink")* Lindau
  - *[Chaetacanthus](../Page/Chaetacanthus.md "wikilink")* Nees
  - *[Chalarothyrsus](../Page/Chalarothyrsus.md "wikilink")* Lindau
  - *[Chamaeranthemum](../Page/Chamaeranthemum.md "wikilink")* Nees
  - [梭果爵床属](../Page/梭果爵床属.md "wikilink") *Championella* Bremek.
  - *[Chileranthemum](../Page/Chileranthemum.md "wikilink")* Oerst.
  - *[Chlamydacanthus](../Page/Chlamydacanthus.md "wikilink")* Lindau
  - *[Chlamydocardia](../Page/Chlamydocardia.md "wikilink")* Lindau
  - *[Chlamydostachya](../Page/Chlamydostachya.md "wikilink")* Mildbr.
  - [色萼花属](../Page/色萼花属.md "wikilink") *Chroesthes* Benoist
  - [鳄嘴花属](../Page/鳄嘴花属.md "wikilink")（[扭序花属](../Page/扭序花属.md "wikilink")）*Clinacanthus*
    Nees
  - *[Clistax](../Page/Clistax.md "wikilink")* Mart.
  - [钟花草属](../Page/钟花草属.md "wikilink") *Codonacanthus* Nees
  - *[Conocalyx](../Page/Conocalyx.md "wikilink")* Benoist
  - *[Corymbostachys](../Page/Corymbostachys.md "wikilink")* Lindau
  - *[Cosmianthemum](../Page/Cosmianthemum.md "wikilink")* Bremek.
  - *[Crabbea](../Page/Crabbea.md "wikilink")* Harv.
  - [十字爵床屬](../Page/十字爵床屬.md "wikilink")
    *[Crossandra](../Page/Crossandra.md "wikilink")*
  - *[Crossandrella](../Page/Crossandrella.md "wikilink")* C. B. Clarke
  - *[Cyclacanthus](../Page/Cyclacanthus.md "wikilink")* S. Moore
  - *[Cylindrosolenium](../Page/Cylindrosolenium.md "wikilink")* Lindau
  - *[Cyphacanthus](../Page/Cyphacanthus.md "wikilink")* Leonard
  - *[Dactylostegium](../Page/Dactylostegium.md "wikilink")* Nees
  - *[Danguya](../Page/Danguya.md "wikilink")* Benoist
  - *[Dasytropis](../Page/Dasytropis.md "wikilink")* Urb.
  - *[Dichazothece](../Page/Dichazothece.md "wikilink")* Lindau
  - *[Dicladanthera](../Page/Dicladanthera.md "wikilink")* F. Muell.
  - [狗肝菜属](../Page/狗肝菜属.md "wikilink") *Dicliptera* Juss.
  - *[Didyplosandra](../Page/Didyplosandra.md "wikilink")* Wight ex
    Bremek.
  - [双翅爵床属](../Page/双翅爵床属.md "wikilink") *Dipteracanthus* Nees
  - *[Dischistocalyx](../Page/Dischistocalyx.md "wikilink")* T. Anderson
    ex Benth. & Hook. f.
  - *[Dolichostachys](../Page/Dolichostachys.md "wikilink")* Benoist
  - *[Drejera](../Page/Drejera.md "wikilink")* Nees
  - [虾衣花属](../Page/虾衣花属.md "wikilink") *Drejerella* Lindau
  - *[Duosperma](../Page/Duosperma.md "wikilink")* Dayton
  - [安龙花属](../Page/安龙花属.md "wikilink") *Dyschoriste* Nees
  - *[Ecbolium](../Page/Ecbolium.md "wikilink")* Kurz
  - [恋岩花属](../Page/恋岩花属.md "wikilink") *Echinacanthus* Nees
  - *[Elytraria](../Page/Elytraria.md "wikilink")* Michx. : Scalystem
  - *[Encephalosphaera](../Page/Encephalosphaera.md "wikilink")* Lindau
  - *[Epiclastopelma](../Page/Epiclastopelma.md "wikilink")* Lindau
  - [可爱花属](../Page/可爱花属.md "wikilink") *Eranthemum* L.
  - *[Eremomastax](../Page/Eremomastax.md "wikilink")* Lindau
  - *[Eusiphon](../Page/Eusiphon.md "wikilink")* Benoist
  - *[Filetia](../Page/Filetia.md "wikilink")* Miq.
  - [網紋草屬](../Page/網紋草屬.md "wikilink")*Fittonia* Coem.
  - *[Forcipella](../Page/Forcipella.md "wikilink")* Baill.
  - *[Forsythiopsis](../Page/Forsythiopsis.md "wikilink")* Baker
  - *[Geissomeria](../Page/Geissomeria.md "wikilink")* Lindl.
  - *[Glossochilus](../Page/Glossochilus.md "wikilink")* Nees
  - *[Golaea](../Page/Golaea.md "wikilink")* Chiov.
  - *[Graphandra](../Page/Graphandra.md "wikilink")* J. B. Imlay
  - [紫叶属](../Page/紫叶属.md "wikilink") *Graptophyllum* Nees
  - *[Gymnophragma](../Page/Gymnophragma.md "wikilink")* Lindau
  - [裸柱花属](../Page/裸柱花属.md "wikilink") *Gymnostachyum* Nees
  - *[Gynocraterium](../Page/Gynocraterium.md "wikilink")* Bremek.
  - *[Gypsacanthus](../Page/Gypsacanthus.md "wikilink")* E. J. Lott et
    al.
  - *[Haplanthodes](../Page/Haplanthodes.md "wikilink")* Kuntze
  - *[Harpochilus](../Page/Harpochilus.md "wikilink")* Nees
  - [小狮子草属](../Page/小狮子草属.md "wikilink") *Hemiadelphis* Nees
  - [半柱花属](../Page/半柱花属.md "wikilink") *Hemigraphis* Nees
  - *[Henrya](../Page/Henrya.md "wikilink")* Nees
  - *[Herpetacanthus](../Page/Herpetacanthus.md "wikilink")* Nees
  - *[Heteradelphia](../Page/Heteradelphia.md "wikilink")* Lindau
  - *[Holographis](../Page/Holographis.md "wikilink")* Nees
  - *[Hoverdenia](../Page/Hoverdenia.md "wikilink")* Nees
  - *[Hulemacanthus](../Page/Hulemacanthus.md "wikilink")* S. Moore
  - [水蓑衣属](../Page/水蓑衣属.md "wikilink") *Hygrophila* R. Br.
    (參考[水蓑衣](../Page/水蓑衣.md "wikilink"))
  - [枪刀药属](../Page/枪刀药属.md "wikilink") *Hypoestes* Sol. ex R. Br.
  - *[Ionacanthus](../Page/Ionacanthus.md "wikilink")* Benoist
  - [叉序草属](../Page/叉序草属.md "wikilink") *Isoglossa* Oerst.
  - *[Isotheca](../Page/Isotheca.md "wikilink")* Turrill
  - *[Jadunia](../Page/Jadunia.md "wikilink")* Lindau
  - *[Juruasia](../Page/Juruasia.md "wikilink")* Lindau
  - [洋爵床属](../Page/洋爵床属.md "wikilink") *Justicia* L.
  - *[Kalbreyeriella](../Page/Kalbreyeriella.md "wikilink")* Lindau
  - *[Kosmosiphon](../Page/Kosmosiphon.md "wikilink")* Lindau
  - [银脉爵床属](../Page/银脉爵床属.md "wikilink") *Kudoacanthus* Hosok.
  - *[Lankesteria](../Page/Lankesteria.md "wikilink")* Lindl.
  - *[Lasiocladus](../Page/Lasiocladus.md "wikilink")* Bojer ex Nees
  - *[Leandriella](../Page/Leandriella.md "wikilink")* Benoist
  - [鳞花草属](../Page/鳞花草属.md "wikilink") *Lepidagathis* Willd.
  - [纤穗爵床属](../Page/纤穗爵床属.md "wikilink") *Leptostachya* Nees
  - *[Liberatia](../Page/Liberatia.md "wikilink")* Rizzini
  - *[Linariantha](../Page/Linariantha.md "wikilink")* B. L. Burtt & R.
    M. Sm.
  - *[Lophostachys](../Page/Lophostachys.md "wikilink")* Pohl
  - *[Louteridium](../Page/Louteridium.md "wikilink")* S. Watson
  - *[Lychniothyrsus](../Page/Lychniothyrsus.md "wikilink")* Lindau
  - *[Marcania](../Page/Marcania.md "wikilink")* J. B. Imlay
  - *[Megalochlamys](../Page/Megalochlamys.md "wikilink")* Lindau
  - *[Megalostoma](../Page/Megalostoma.md "wikilink")* Leonard
  - *[Megaskepasma](../Page/Megaskepasma.md "wikilink")* Lindau
  - *[Melittacanthus](../Page/Melittacanthus.md "wikilink")* S. Moore
  - *[Mellera](../Page/Mellera.md "wikilink")* S. Moore
  - *[Mendoncia](../Page/Mendoncia.md "wikilink")* Vand.
  - *[Metarungia](../Page/Metarungia.md "wikilink")* Baden
  - *[Mexacanthus](../Page/Mexacanthus.md "wikilink")* T. F. Daniel
  - *[Meyenia](../Page/Meyenia.md "wikilink")* Nees
  - *[Mimulopsis](../Page/Mimulopsis.md "wikilink")* Schweinf.
  - *[Mirandea](../Page/Mirandea.md "wikilink")* Rzed.
  - *[Monechma](../Page/Monechma.md "wikilink")* Hochst.
  - *[Monothecium](../Page/Monothecium.md "wikilink")* Hochst.
  - *[Morsacanthus](../Page/Morsacanthus.md "wikilink")* Rizzini
  - [瘤子草属](../Page/瘤子草属.md "wikilink") *Nelsonia* R. Br.
  - *[Neohallia](../Page/Neohallia.md "wikilink")* Hemsl.
  - *[Neriacanthus](../Page/Neriacanthus.md "wikilink")* Benth.
  - *[Neuracanthus](../Page/Neuracanthus.md "wikilink")* Nees
  - [紅樓花屬](../Page/紅樓花屬.md "wikilink")
    *[Odontonema](../Page/Odontonema.md "wikilink")*\[1\]
  - [蛇根草属](../Page/蛇根草属.md "wikilink") *Ophiorrhiziphyllon* Kurz
  - *[Oplonia](../Page/Oplonia.md "wikilink")* Raf.
  - *[Oreacanthus](../Page/Oreacanthus.md "wikilink")* Benth.
  - *[Orophochilus](../Page/Orophochilus.md "wikilink")* Lindau
  - [金苞花屬](../Page/金苞花屬.md "wikilink") *Pachystachys* Nees
  - *[Pelecostemon](../Page/Pelecostemon.md "wikilink")* Leonard
  - *[Pentstemonacanthus](../Page/Pentstemonacanthus.md "wikilink")*
    Nees
  - *[Perenideboles](../Page/Perenideboles.md "wikilink")* Ram. Goyena
  - *[Pericalypta](../Page/Pericalypta.md "wikilink")* Benoist
  - *[Periestes](../Page/Periestes.md "wikilink")* Baill.
  - [观音草属](../Page/观音草属.md "wikilink")（[九头狮子草属](../Page/九头狮子草属.md "wikilink")）*Peristrophe*
    Nees
  - *[Petalidium](../Page/Petalidium.md "wikilink")* Nees
  - [肾苞草属](../Page/肾苞草属.md "wikilink") *Phaulopsis* Willd.
  - *[Phialacanthus](../Page/Phialacanthus.md "wikilink")* Benth.
  - *[Phidiasia](../Page/Phidiasia.md "wikilink")* Urb.
  - [火焰花属](../Page/火焰花属.md "wikilink") *Phlogacanthus* Nees
  - *[Physacanthus](../Page/Physacanthus.md "wikilink")* Benth.
  - *[Podorungia](../Page/Podorungia.md "wikilink")* Baill.
  - *[Poikilacanthus](../Page/Poikilacanthus.md "wikilink")* Lindau
  - *[Polylychnis](../Page/Polylychnis.md "wikilink")* Bremek.
  - *[Populina](../Page/Populina_\(genus\).md "wikilink")* Baill.
  - *[Pranceacanthus](../Page/Pranceacanthus.md "wikilink")* Wassh.
  - [山壳骨属](../Page/山壳骨属.md "wikilink") *Pseuderanthemum* Radlk.
  - *[Pseudocalyx](../Page/Pseudocalyx.md "wikilink")* Radlk.
  - *[Pseudodicliptera](../Page/Pseudodicliptera.md "wikilink")* Benoist
  - *[Pseudoruellia](../Page/Pseudoruellia.md "wikilink")* Benoist
  - *[Psilanthele](../Page/Psilanthele.md "wikilink")* Lindau
  - *[Ptyssiglottis](../Page/Ptyssiglottis.md "wikilink")* T. Anderson
  - *[Pulchranthus](../Page/Pulchranthus.md "wikilink")* V. M. Baum et
    al.
  - *[Pupilla](../Page/Pupilla.md "wikilink")* Rizzini
  - *[Razisea](../Page/Razisea.md "wikilink")* Oerst.
  - [灵芝草属](../Page/灵芝草属.md "wikilink") *Rhinacanthus* Nees
  - *[Rhombochlamys](../Page/Rhombochlamys.md "wikilink")* Lindau
  - *[Ritonia](../Page/Ritonia.md "wikilink")* Benoist
  - [爵床属](../Page/爵床属.md "wikilink") *Rostellularia* Rchb.
  - [双翅爵床属](../Page/双翅爵床属.md "wikilink") *Ruellia* L. : Wild Petunia
  - *[Ruelliopsis](../Page/Ruelliopsis.md "wikilink")* C. B. Clarke
  - [孩儿草属](../Page/孩儿草属.md "wikilink") *Rungia* Nees
  - *[Ruspolia](../Page/Ruspolia.md "wikilink")* Lindau
  - *[Ruttya](../Page/Ruttya.md "wikilink")* Harv.
  - *[Saintpauliopsis](../Page/Saintpauliopsis.md "wikilink")* Staner
  - *[Salpinctium](../Page/Salpinctium.md "wikilink")* T. J. Edwards
  - *[Salpixantha](../Page/Salpixantha.md "wikilink")* Hook.
  - *[Samuelssonia](../Page/Samuelssonia.md "wikilink")* Urb. & Ekman
  - *[Sanchezia](../Page/Sanchezia.md "wikilink")* Ruiz & Pav.
  - *[Santapaua](../Page/Santapaua.md "wikilink")* N. P. Balakr. & Subr.
  - *[Sapphoa](../Page/Sapphoa.md "wikilink")* Urb.
  - *[Satanocrater](../Page/Satanocrater.md "wikilink")* Schweinf.
  - *[Sautiera](../Page/Sautiera.md "wikilink")* Decne.
  - *[Schaueria](../Page/Schaueria.md "wikilink")* Nees
  - *[Schwabea](../Page/Schwabea.md "wikilink")* Endl. & Fenzl
  - *[Sclerochiton](../Page/Sclerochiton.md "wikilink")* Harv.
  - *[Sebastiano-schaueria](../Page/Sebastiano-schaueria.md "wikilink")*
    Nees
  - *[Sericospora](../Page/Sericospora.md "wikilink")* Nees
  - *[Siphonoglossa](../Page/Siphonoglossa.md "wikilink")* Oerst.
  - *[Spathacanthus](../Page/Spathacanthus.md "wikilink")* Baill.
  - *[Sphacanthus](../Page/Sphacanthus.md "wikilink")* Benoist
  - [小苞爵床属](../Page/小苞爵床属.md "wikilink") *Sphinctacanthus* Benth.
  - *[Spirostigma](../Page/Spirostigma.md "wikilink")* Nees
  - *[Standleyacanthus](../Page/Standleyacanthus.md "wikilink")* Leonard
  - [叉柱花属](../Page/叉柱花属.md "wikilink") *Staurogyne* Wall.
  - *[Steirosanchezia](../Page/Steirosanchezia.md "wikilink")* Lindau
  - *[Stenandriopsis](../Page/Stenandriopsis.md "wikilink")* S. Moore
  - *[Stenandrium](../Page/Stenandrium.md "wikilink")* Nees
  - *[Stenostephanus](../Page/Stenostephanus.md "wikilink")* Nees
  - *[Streblacanthus](../Page/Streblacanthus.md "wikilink")* Kuntze
  - *[Streptosiphon](../Page/Streptosiphon.md "wikilink")* Mildbr.
  - [马蓝属](../Page/马蓝属.md "wikilink")（即[紫云菜属](../Page/紫云菜属.md "wikilink")）
    *Strobilanthes* Blume
  - *[Strobilanthopsis](../Page/Strobilanthopsis.md "wikilink")* S.
    Moore
  - *[Styasasia](../Page/Styasasia.md "wikilink")* S. Moore
  - *[Suessenguthia](../Page/Suessenguthia.md "wikilink")* Merxm.
  - *[Synchoriste](../Page/Synchoriste.md "wikilink")* Baill.
  - *[Taeniandra](../Page/Taeniandra.md "wikilink")* Bremek.
  - [肖笼鸡属](../Page/肖笼鸡属.md "wikilink") *Tarphochlamys* Bremek.
  - *[Teliostachya](../Page/Teliostachya.md "wikilink")* Nees
  - *[Tessmanniacanthus](../Page/Tessmanniacanthus.md "wikilink")*
    Mildbr.
  - *[Tetramerium](../Page/Tetramerium.md "wikilink")* Nees
  - *[Theileamea](../Page/Theileamea.md "wikilink")* Baill.
  - *[Thomandersia](../Page/Thomandersia.md "wikilink")* Baill.
  - [老鸦嘴属](../Page/老鸦嘴属.md "wikilink")（[山牵牛属](../Page/山牵牛属.md "wikilink")）
    *Thunbergia* Retz.
  - *[Thysanostigma](../Page/Thysanostigma.md "wikilink")* J. B. Imlay
  - *[Tremacanthus](../Page/Tremacanthus.md "wikilink")* S. Moore
  - *[Triaenanthus](../Page/Triaenanthus.md "wikilink")* Nees
  - *[Trichanthera](../Page/Trichanthera.md "wikilink")* Kunth
  - *[Trichaulax](../Page/Trichaulax.md "wikilink")* Vollesen
  - *[Trichocalyx](../Page/Trichocalyx.md "wikilink")* Balf. f.
  - *[Trichosanchezia](../Page/Trichosanchezia.md "wikilink")* Mildbr.
  - *[Ulleria](../Page/Ulleria.md "wikilink")* Bremek.
  - *[Vavara](../Page/Vavara.md "wikilink")* Benoist
  - *[Vindasia](../Page/Vindasia.md "wikilink")* Benoist
  - *[Warpuria](../Page/Warpuria.md "wikilink")* Stapf
  - *[Whitfieldia](../Page/Whitfieldia.md "wikilink")* Hook.
  - *[Xantheranthemum](../Page/Xantheranthemum.md "wikilink")* Lindau
  - *[Xerothamnella](../Page/Xerothamnella.md "wikilink")* C. T. White
  - *[Yeatesia](../Page/Yeatesia.md "wikilink")* Small : Bractspike
  - *[Zygoruellia](../Page/Zygoruellia.md "wikilink")* Baill.

## 图片

[File:Acanthaceae_leaf.jpg|红网纹草的叶子](File:Acanthaceae_leaf.jpg%7C红网纹草的叶子)，*Fittonia
verschaffeltii* <File:Starr> 011104 0065 asystasia
gangetica.jpg|十万错，(*Asystasia gangetica*) <File:Starr> 021122
0083 justicia aurea.jpg|*Justicia aurea* <File:Peristrophe>
speciosa1.jpg|*Peristrophe speciosa*

## 参考文献

## 外部链接

  - [爵床科](http://delta-intkey.com/angio/www/acanthac.htm)

[\*](../Category/爵床科.md "wikilink")

1.