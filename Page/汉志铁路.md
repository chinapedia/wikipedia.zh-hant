**汉志铁路**（土耳其語：Hicaz
Demiryolu），[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")[苏丹兼](../Page/苏丹_\(称谓\).md "wikilink")[哈里发](../Page/哈里发.md "wikilink")[阿卜杜勒·哈米德二世下令修建的一条铁路](../Page/阿卜杜勒·哈米德二世.md "wikilink")，起自[大马士革](../Page/大马士革.md "wikilink")，终点至[汉志首府](../Page/汉志.md "wikilink")[麦地那](../Page/麦地那.md "wikilink")，中途包括一條通往[海法的](../Page/海法.md "wikilink")[支線](../Page/耶斯列谷鐵路.md "wikilink")，总长1,300公里。原设计终点为麦地那以南400公里的麦加，并在将来向南延伸到[也门的](../Page/也门.md "wikilink")[萨那](../Page/萨那.md "wikilink")。

汉志铁路的轨距为1,050毫米，属于单线[窄轨铁路](../Page/轨距.md "wikilink")。

根据奥斯曼帝国的官方说法，修建汉志铁路的目的是为了方便全世界[穆斯林去](../Page/穆斯林.md "wikilink")[麦加朝觐](../Page/麦加.md "wikilink")，但该铁路的军事意义比宗教意义更为重要(防止阿拉伯人独立)。在其300万土耳其镑的建造经费中，有三分之一来自各国穆斯林的捐款。但是实际上德国人为这条铁路提供了工程设计和指导，他们希望通过这条铁路将德国的影响力渗透到汉志和也门地区。这与同时期德国计划修建的[巴格達鐵路](../Page/巴格達鐵路.md "wikilink")（[柏林至](../Page/柏林.md "wikilink")[巴格达](../Page/巴格达.md "wikilink")）属于同一目的。由于这一因素的影响，汉志铁路的修建遭到了[英国及汉志地方封建贵族的阻挠](../Page/英国.md "wikilink")。1908年9月1日，铁路修至麦地那，后续工作被迫终止。1913年，在大马士革修建了汉志铁路的终点站——汉志车站。

[Hejaz_railway.jpg](https://zh.wikipedia.org/wiki/File:Hejaz_railway.jpg "fig:Hejaz_railway.jpg")

汉志铁路通车之后就一直受到[阿拉伯部落的袭击](../Page/阿拉伯.md "wikilink")，奥斯曼帝国从未能有效控制铁路周边的地区。为了保护铁路的安全，奥斯曼帝国在铁路沿途设立了很多兵站、加水站和哨所。1916年[阿拉伯起义爆发后](../Page/阿拉伯起义.md "wikilink")，英国军官[劳伦斯率领阿拉伯部队对汉志铁路展开了破坏性的游击战](../Page/托马斯·爱德华·劳伦斯.md "wikilink")。至[第一次世界大战结束时](../Page/第一次世界大战.md "wikilink")，汉志铁路在汉志境内的路段已完全停止运营。1919年之后[汉志王国与](../Page/汉志王国.md "wikilink")[内志王国展开战争](../Page/内志与哈萨酋长国.md "wikilink")，也无暇重开铁路。汉志铁路在汉志境内的路段基本全被拆除，只留下了路基。

汉志铁路在[叙利亚和](../Page/叙利亚.md "wikilink")[约旦境内的路段至今仍在运营](../Page/约旦.md "wikilink")，一段从[大马士革至](../Page/大马士革.md "wikilink")[安曼](../Page/安曼.md "wikilink")，另外一段原来是汉志铁路的支线，从马安通往[亚喀巴](../Page/亚喀巴.md "wikilink")。沙特阿拉伯境内还有残存的若干小段铁路、车站、路基，作为旅游观光项目。某些地方还可找到阿拉伯起义时被炸毁的机车残骸。

## 参见

  - [汉志王国](../Page/汉志王国.md "wikilink")
  - [托马斯·爱德华·劳伦斯](../Page/托马斯·爱德华·劳伦斯.md "wikilink")
  - [奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")
  - [游击战](../Page/游击战.md "wikilink")

## 外部連結

  - [漢志鐵路](http://cht2.visitjordan.com/generalinformation/gettingaround/railroad.aspx)

[Category:鐵路線](../Category/鐵路線.md "wikilink")
[Category:中东历史](../Category/中东历史.md "wikilink")