[Point_mutations-en.png](https://zh.wikipedia.org/wiki/File:Point_mutations-en.png "fig:Point_mutations-en.png")

**點突變**（）是[突變的一種類型](../Page/突變.md "wikilink")，在遗传材料[DNA或](../Page/脱氧核糖核酸.md "wikilink")[RNA中](../Page/核糖核酸.md "wikilink")，會使單一個[鹼基](../Page/鹼基.md "wikilink")[核苷酸替換成另一種](../Page/核苷酸.md "wikilink")[核苷酸](../Page/核苷酸.md "wikilink")。通常这个术语也包括只有作用於單一鹼基對的[插入或](../Page/插入_\(遺傳學\).md "wikilink")[刪除](../Page/刪除_\(遺傳學\).md "wikilink")。

## 点突变分类

點突變可依發生位置對基因功能的影響而作以下分類：

### 转换/颠换的分类

[TsTvMutation.jpg](https://zh.wikipedia.org/wiki/File:TsTvMutation.jpg "fig:TsTvMutation.jpg")

在1959年恩斯特·弗里斯(Ernst Freese)创造了术语“转换”或“颠换”对不同类型的点突变进行分类。\[1\]\[2\]

  - **转换**：[嘌呤替换另一个](../Page/嘌呤.md "wikilink")[嘌呤](../Page/嘌呤.md "wikilink"),
    或[嘧啶替换另一个](../Page/嘧啶.md "wikilink")[嘧啶](../Page/嘧啶.md "wikilink").
  - **颠换**：[嘌呤替换](../Page/嘌呤.md "wikilink")[嘧啶](../Page/嘧啶.md "wikilink"),
    或反之亦然。

转换（Alpha）和颠换（Beta）在突变速率有系统的差异。转换突变大于颠换突变约10倍以上。

### 功能的分类

  - （nonsense mutation）：使原本可製造[蛋白質的密碼變成停止密碼](../Page/蛋白質.md "wikilink")。

  - [错义突变](../Page/错义突变.md "wikilink")（missense mutation）：使密碼所對應的氨基酸改變。

  - （silent mutation）：密碼改變，但對應的氨基酸不變。

## 参考资料

## 外部連結

  -
[Category:突變](../Category/突變.md "wikilink")
[Category:分子生物学](../Category/分子生物学.md "wikilink")

1.
2.