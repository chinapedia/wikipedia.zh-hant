[Mmnmzz.png](https://zh.wikipedia.org/wiki/File:Mmnmzz.png "fig:Mmnmzz.png")
**明朝民變**收录了[明代主要的起事运动](../Page/明代.md "wikilink")。其中最大的一次是從[明熹宗天启七年](../Page/明熹宗.md "wikilink")(1627年)叛亂军与明军作战开始，直至[清朝](../Page/清朝.md "wikilink")[顺治年间才结束的一场战争](../Page/顺治.md "wikilink")，被称为**明末农民战争**。

## 明前期民变

[明太祖洪武三年](../Page/明太祖.md "wikilink")（1370年），[广西阳山县山民十万余反](../Page/广西.md "wikilink")。同年，[福建泉州陈同反](../Page/福建.md "wikilink")，进攻[永安](../Page/永安市.md "wikilink")，[德化和](../Page/德化.md "wikilink")[安溪三县](../Page/安溪.md "wikilink")。同年，[山东青州](../Page/山东.md "wikilink")([益都](../Page/益都.md "wikilink"))孙古朴聚众反，自号黄巾，袭击[莒州](../Page/莒州.md "wikilink")，杀[同知](../Page/同知.md "wikilink")。

[明太祖洪武十四年](../Page/明太祖.md "wikilink")（1381年），[广州的](../Page/广州.md "wikilink")[曹真和](../Page/曹真_\(明朝\).md "wikilink")[苏文卿等叛亂](../Page/苏文卿.md "wikilink")，众数万，战船一千八百艘；明廷派[南雄](../Page/南雄.md "wikilink")[赵庸率步骑舟一万五千人](../Page/赵庸.md "wikilink")，再加上[广东由参政阎屯](../Page/广东.md "wikilink")，千户[张惠率领的本省军队](../Page/张惠.md "wikilink")，联合镇压。同年，[福建福安县民](../Page/福建.md "wikilink")[江志贤聚众八千反](../Page/江志贤.md "wikilink")。

[明太祖洪武十五年](../Page/明太祖.md "wikilink")（1382年），[广东铲平王叛亂](../Page/广东.md "wikilink")，[赵庸再次前去镇压](../Page/赵庸.md "wikilink")，斩首八千八百余。

明太祖[洪武十八年](../Page/洪武.md "wikilink")（1385年），[湖广铲平王](../Page/湖广.md "wikilink")[吴奤儿继洪武十一年](../Page/吴奤儿.md "wikilink")(1378年)叛亂逃脱后再次造反，明廷派信国公汤和率领大军号称二十万，前往镇压。

明太祖洪武二十二年（1389年），[江西赣州夏三聚众数万人反](../Page/江西.md "wikilink")。明廷派出[胡海](../Page/胡海.md "wikilink")，[陈桓](../Page/陈桓.md "wikilink")，[叶昇](../Page/叶昇.md "wikilink")，兵33500人，动用三个侯这么大的阵势，在明初也是少见的。

明太祖洪武二十八年（1395年），[广西的](../Page/广西.md "wikilink")[瑶族](../Page/瑶族.md "wikilink")、[壮族数万人叛亂](../Page/壮族.md "wikilink")，明廷派征南将军[杨文率军镇压](../Page/杨文.md "wikilink")，斩叛亂军18360，斩家属8280人。

明太祖洪武三十年（1397年），[陕西沔县](../Page/陕西.md "wikilink")[高福兴等叛亂](../Page/高福兴.md "wikilink")，打败汉中卫明军，攻陷[略阳](../Page/略阳.md "wikilink")，[徽州](../Page/徽州.md "wikilink")，文县。明廷派长兴侯[耿炳文](../Page/耿炳文.md "wikilink")，武定侯[郭英统明军数万镇压](../Page/郭英.md "wikilink")\[1\]。

[明太宗永乐十八年](../Page/明太宗.md "wikilink")（1420年），[山东青州](../Page/山东.md "wikilink")[唐赛儿](../Page/唐赛儿.md "wikilink")[白莲教叛亂](../Page/白莲教.md "wikilink")。

## 明中叶民变

[明英宗正统时](../Page/明英宗.md "wikilink")，[浙江矿工](../Page/浙江.md "wikilink")[叶宗留叛亂](../Page/叶宗留.md "wikilink")，[福建佃农](../Page/福建.md "wikilink")[邓茂七叛亂](../Page/邓茂七.md "wikilink")。

[明英宗](../Page/明英宗.md "wikilink")[天顺](../Page/天顺.md "wikilink")、[明宪宗成化时](../Page/明宪宗.md "wikilink")，在荆襄山区爆发[刘通](../Page/刘通.md "wikilink")、[李原的开荒流民叛亂](../Page/李原.md "wikilink")。叛亂最终失败，但明廷不得不在荆襄山区设立郧阳府，增置竹溪、郧西等七县，允许流民开垦荒地，成为合法编户良民。

[明武宗正德年间](../Page/明武宗.md "wikilink")，[河北出现](../Page/河北.md "wikilink")[刘六刘七起義](../Page/刘六刘七起義.md "wikilink")。叛亂军攻占北直隶、[山东](../Page/山东.md "wikilink")、[河南](../Page/河南.md "wikilink")、[山西许多州县](../Page/山西.md "wikilink")，并曾三次逼进[北京](../Page/北京.md "wikilink")。

[明武宗正德五年](../Page/明武宗.md "wikilink")（1510年）前后，[江西各地爆发叛亂](../Page/江西.md "wikilink")。其中有抚州[王钰五叛亂](../Page/王钰五.md "wikilink")，饶州[汪澄二叛亂](../Page/汪澄二.md "wikilink")，瑞州[罗光权叛亂](../Page/罗光权.md "wikilink")，赣州[何积钦叛亂](../Page/何积钦.md "wikilink")，靖安县[胡雷二叛亂](../Page/胡雷二.md "wikilink")。他们在山谷间据险立寨，遥相呼应，声势甚盛。[明武宗正德六年](../Page/明武宗.md "wikilink")(1511年)二月，明廷派右都御史[陈金总制军务](../Page/陈金.md "wikilink")，调动[中國南方数省军队前往镇压](../Page/中國南方.md "wikilink")。叛亂軍转入低潮。但到[明武宗正德十二年](../Page/明武宗.md "wikilink")(1517年)，在[江西南部与](../Page/江西.md "wikilink")[福建](../Page/福建.md "wikilink")、[广东交界的山区](../Page/广东.md "wikilink")，叛亂军又趋活跃。明廷派遣右佥都御史[王守仁为南赣巡抚](../Page/王守仁.md "wikilink")，提督军务，前去围剿。[正德十三年](../Page/正德.md "wikilink")(1518年)，叛亂被镇压。

此外，在[四川](../Page/四川.md "wikilink")、[贵州以及](../Page/贵州.md "wikilink")[广西等地](../Page/广西.md "wikilink")，还先后爆发许多[土著叛亂](../Page/土著.md "wikilink")。

## 明后期民变

明神宗[万历二十九年](../Page/万历.md "wikilink")（1601年）6月初，苏州市民[葛贤领导苏州机工包围税署](../Page/葛贤.md "wikilink")，打死税吏，吓得税监逃跑。明廷被迫撤回税监。

[明神宗万历年间](../Page/明神宗.md "wikilink")，蓟州人[王森传播](../Page/王森_\(闻香教\).md "wikilink")[白莲教](../Page/白莲教.md "wikilink")，自称闻香教主，其教徒遍布北南直隶、[山东](../Page/山东.md "wikilink")、[山西](../Page/山西.md "wikilink")、[河南](../Page/河南.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[四川等省](../Page/四川.md "wikilink")。[万历四十二年](../Page/万历.md "wikilink")（1614年），王森在[京师传教时被捕](../Page/京师.md "wikilink")，死于狱中。之后，其弟子巨野人[徐鸿儒继续组织](../Page/徐鸿儒.md "wikilink")[白莲教](../Page/白莲教.md "wikilink")。

明熹宗[天启二年](../Page/天启.md "wikilink")（1622年），[白莲教徐鸿儒联合景州](../Page/白莲教.md "wikilink")（今河北[景县](../Page/景县.md "wikilink")）于弘志、曹州（今山东[曹县](../Page/曹县.md "wikilink")）[张世佩等](../Page/张世佩.md "wikilink")，图谋起事，约定中秋起兵。但因计划泄露，遂于五月先期发动。徐鸿儒自称中兴福烈帝，改年号为大乘兴胜元年。叛亂军很快攻克了郓城、邹县、滕县、[峄县](../Page/峄县.md "wikilink")，“众至数万”，屡败官军。其时，于弘志在[河北武邑](../Page/河北.md "wikilink")、[枣强](../Page/枣强.md "wikilink")、[衡水等地起兵响应](../Page/衡水.md "wikilink")。又有[刘永明聚众二万人](../Page/刘永明.md "wikilink")，不久与徐鸿儒队伍汇合，叛亂声势大振。他们计划“南通徐淮、陈、颍、蕲、黄，中截粮运，北达神京，为帝为王”。同年十一月，徐鸿儒被部下出卖，在邹县被捕押至京城被杀害，叛亂失败。

自[明熹宗天启七年](../Page/明熹宗.md "wikilink")（1627年）陕西王二叛亂开始，有一次最大的叛亂，直至清[顺治十五年](../Page/顺治.md "wikilink")（1658年）失败，前后历时31年。战争推翻了[明王朝政权](../Page/明朝.md "wikilink")，期间使处于辽东的清军有机可乘，趁机入关打败叛亂军，征服了[中国](../Page/中国.md "wikilink")。

明思宗崇禎四年（1631年）八月，[皇太極率清兵攻大淩河城](../Page/皇太極.md "wikilink")（今遼寧錦縣），[祖大壽圍於城內](../Page/祖大壽.md "wikilink")。孫元化急令孔有德以八百騎趕赴前線增援，然登州遼東兵與山東兵素不和，孔有德抵達吳橋時，因遇大雨春雪，部隊給養不足，士兵搶劫嘩變。孔有德在登州發動[吳橋兵變](../Page/吳橋兵變.md "wikilink")，自号都[元帅](../Page/元帅.md "wikilink")，孫元化忠於朝廷不願稱王，孔有德放他逃離登州。明廷派兵镇压后，投降[后金](../Page/后金.md "wikilink")。

## 明末民变的起因

  - 进入[小冰期](../Page/小冰期.md "wikilink")，气候持续恶劣。皇权旁落，离心离德，期间又不合时宜地增收[三饷](../Page/三饷.md "wikilink")，激起民变。
  - 大部分官方史料認為晚明民变是由於拖欠軍餉、給養不足及強迫徵兵所致，加上連串的饑荒，促成民變越演越烈。

## 参考文献

### 引用

### 来源

  - [顾诚](../Page/顾诚.md "wikilink")：《[明末农民战争史](../Page/明末农民战争史.md "wikilink")》，中国社会科学出版社，1982年10月.
  - 顾诚：《[南明史](../Page/南明史.md "wikilink")》，中国青年出版社，1997年5月.
  - 徐进、赵鼎新：〈[政府能力和万历年间的民变发展](http://www.nssd.org/articles/article_read.aspx?id=23703818)〉。
  - 山根幸夫：〈[明末农民起义与紳士阶层的反应](http://www.nssd.org/articles/article_read.aspx?id=1003107540)〉。

[Category:中国各朝代民变](../Category/中国各朝代民变.md "wikilink")
[明朝民變](../Category/明朝民變.md "wikilink")
[Category:明朝歷史事件](../Category/明朝歷史事件.md "wikilink")

1.  《明初重典治国对靖难之役结果的影响》：“并不轻松的赋税，加上严苛的统治，所带来的一个直接后果就是明初所发生的农民起义较任何一个王朝建立初期都多，从洪武元年到洪武三十年，各类起义层出不穷。这些起义中大部分都是为了反抗苛重的德役与赋税，如洪武三年泉州惠安民陈同率众起义；广西阳山县十万山寨人民聚众起义；洪武五年潮州民起义；洪武十二年四川眉县人民起义；洪武十四年广州人民起义；洪武十六年广东瑶族人民起义；洪武二十二年江西赣州人民起义；洪武二十九年会同县人民起义；直到洪武三十年，也就是朱元璋死前一年，陕西、四川交界处还爆发大规模农民起义。如此频繁的农民起义，充分说明了明初重典治国政策下，百姓生活的艰难与社会矛盾的尖锐，而这一现实状况对靖难之役的结果也产生了一定影响。”《南阳师范学院学报》，2009年11月，第8卷，第11期。