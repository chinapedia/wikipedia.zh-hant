**樱木JR**（，），原名**米尔顿·J·R·亨德森**（**Milton "J.R."
Henderson**），生于[美国](../Page/美国.md "wikilink")[贝克斯菲尔德](../Page/贝克斯菲尔德_\(加利福尼亚州\).md "wikilink")，[日本籍职业篮球运动员](../Page/日本.md "wikilink")，司职前锋。

亨德森就读于[加利福尼亚大学洛杉矶分校](../Page/加利福尼亚大学洛杉矶分校.md "wikilink")，在1998年NBA选秀中第二轮第56顺位被[温哥华灰熊选中](../Page/温哥华灰熊.md "wikilink")，在他大二和大三赛季的时候入选了大西洋大十联盟的第一阵容。並打了一個賽季，之後兩年他碾轉於[拉斯維加斯](../Page/拉斯維加斯.md "wikilink")，[法國](../Page/法國.md "wikilink")，[波多黎各和](../Page/波多黎各.md "wikilink")[菲律賓的夏季聯賽](../Page/菲律賓.md "wikilink")，现效力于**JBL**日本篮球联赛的爱信海马，2006年每場平均獲得21.5分和11.6個籃板。

2007年7月16日，亨德森宣布他拿到了[日本国籍](../Page/日本.md "wikilink")，为了可以代表日本国家篮球队出征[2008年北京奥运会](../Page/2008年北京奥运会.md "wikilink")。\[1\]

2012年男籃亞錦資格賽，櫻木JR無預期選入日本國家隊12人之中，對上中華台北隊，共獲得19分，10籃板，助日本國家隊以90:78擊退中華台北隊。\[2\]

櫻木稱其為了讓自己能夠早點融入日本環境，目前正練習學習基本的[日本語](../Page/日本語.md "wikilink")，並計劃長期留在日本，短期內還沒打算會回[美國](../Page/美國.md "wikilink")。

## 姓氏的由來

之所以會取名為櫻木，是因為想讓人感到自己身為日本人的歸化存在，並感受到日本櫻花希望的感覺，曾有人認為之所以取名同時令人聯想到知名流行漫畫[灌籃高手主角櫻木花道](../Page/灌籃高手.md "wikilink")，也姓櫻木剛好足以對應聯想，然而櫻木稱在他取名前並未知道這部漫畫，取相同名完全是一種純粹的偶然。

## 参考资料

<references/>

[Category:非洲裔美国人](../Category/非洲裔美国人.md "wikilink")
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:日本男子篮球运动员](../Category/日本男子篮球运动员.md "wikilink")
[Category:孟菲斯灰熊队球员](../Category/孟菲斯灰熊队球员.md "wikilink")
[Category:歸化日本公民](../Category/歸化日本公民.md "wikilink")

1.  "American Henderson Has A Yen to Play For Japan," Reuters, July 17,
    2007.
2.  <http://mag.udn.com/mag/sports/storypage.jsp?f_MAIN_ID=109&f_SUB_ID=5263&f_ART_ID=413001>