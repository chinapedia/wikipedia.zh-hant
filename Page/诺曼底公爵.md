[Blason_duche_fr_Normandie.svg](https://zh.wikipedia.org/wiki/File:Blason_duche_fr_Normandie.svg "fig:Blason_duche_fr_Normandie.svg")
**诺曼底公爵**（[法语](../Page/法语.md "wikilink")：**ducs de
Normandie**）是一个[法国爵位称号](../Page/法国.md "wikilink")。在历史上，大多数强大的诺曼底公爵都只是名义上效忠于法国国王。

[诺曼底公国本身是法国国王](../Page/诺曼底公国.md "wikilink")[查理三世](../Page/查理三世_\(法兰西\).md "wikilink")（昏庸者）于911年为与入侵及占据[加来海峡一带的](../Page/加来海峡.md "wikilink")[诺曼人妥协而建立的](../Page/诺曼人.md "wikilink")，第一任公爵是诺曼人领袖[羅洛](../Page/羅洛_\(諾曼第\).md "wikilink")。但是在[理查二世公爵第一个正式自称为](../Page/理查二世_\(诺曼底\).md "wikilink")[公爵之前](../Page/公爵.md "wikilink")，历任统治者实际上仍然是使用着[北欧传统的贵族称号](../Page/北欧.md "wikilink")[雅尔](../Page/雅尔.md "wikilink")（Jarl）作为自己的头衔。1066年，诺曼底公爵威廉一世（即[征服者威廉](../Page/征服者威廉.md "wikilink")）征服[英格兰](../Page/英格兰.md "wikilink")，从此诺曼底公爵之位一直由英格兰王室成员担任。

1204年，强大的法国国王[腓力二世](../Page/腓力二世_\(法兰西\).md "wikilink")（奥古斯都）兼併了诺曼底公国的全部领土，但是各英格兰国王仍然自称为诺曼底公爵，直到1259年在[巴黎条约中永远放弃了对诺曼底公国的领土要求](../Page/1259年巴黎条约.md "wikilink")。此后这个公国就成为法兰西王国的一部分。

法国王子[路易—夏尔](../Page/路易十七.md "wikilink")（[路易十六之子](../Page/路易十六.md "wikilink")）在其兄长去世前也曾拥有诺曼底公爵的称号。但在他的兄长去世后，保王党人即宣布他为国王**路易十七**（1789年）。

## 诺曼底公爵列表

### [诺曼王朝](../Page/诺曼王朝.md "wikilink")

  - [羅洛](../Page/羅洛_\(諾曼第\).md "wikilink")（又名諾曼第的罗贝尔） 911年-927年
  - [吉约姆一世](../Page/吉约姆一世_\(诺曼底\).md "wikilink") 927年-942年
  - [理查一世](../Page/理查一世_\(諾曼第\).md "wikilink") 942年-996年
  - [理查二世](../Page/理查二世_\(諾曼第\).md "wikilink") 996年-1027年
  - [理查三世](../Page/理查三世_\(諾曼第\).md "wikilink") 1027年-1028年
  - [罗贝尔一世](../Page/罗贝尔一世_\(诺曼底\).md "wikilink") 1028年-1035年
  - [吉约姆二世](../Page/威廉一世_\(英格兰\).md "wikilink") 1035年-1087年
  - [罗贝尔二世](../Page/罗贝尔二世_\(诺曼底\).md "wikilink") 1087年-1106年
  - [亨利一世](../Page/亨利一世_\(英格兰\).md "wikilink") 1106年-1135年
  - [布卢瓦的艾蒂安](../Page/斯蒂芬_\(英格兰\).md "wikilink") 1135年-1144年

### [金雀花王朝](../Page/金雀花王朝.md "wikilink")

  - [若弗鲁瓦](../Page/若弗鲁瓦五世_\(安茹\).md "wikilink") 1144年-1150年
  - [亨利二世](../Page/亨利二世_\(英格兰\).md "wikilink") 1150年-1189年
  - [理查四世](../Page/理查一世.md "wikilink") 1189年-1199年
  - [让](../Page/约翰_\(英格兰\).md "wikilink") 1199年-1204年
      - [让](../Page/约翰_\(英格兰\).md "wikilink") 1204年-1216年 （自称）
      - [亨利三世](../Page/亨利三世_\(英格兰\).md "wikilink") 1216年-1259年 （自称）

## 参考文献

## 参见

  - [诺曼底](../Page/诺曼底.md "wikilink")
  - [诺曼底公国](../Page/诺曼底公国.md "wikilink")

[Category:欧洲公爵](../Category/欧洲公爵.md "wikilink")
[诺曼底公爵](../Category/诺曼底公爵.md "wikilink")