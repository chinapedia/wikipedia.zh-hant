**堀江由衣**（本名：，），是一名[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。出生於[東京都](../Page/東京都.md "wikilink")[葛飾區](../Page/葛飾區.md "wikilink")。[血型為B型](../Page/血型.md "wikilink")。現屬[VIMS](../Page/VIMS.md "wikilink")（聲優相關）／[Starchild](../Page/Starchild.md "wikilink")（歌手相關）。

她自幼的夢想是成為一名聲優。讀短期大學時，適逢[ARTSVISION舉辦特別優待生的招生試](../Page/ARTSVISION.md "wikilink")，成功從三千多名考生中脫穎而出，成為5名合格者之一。1996年她在日本播音演技研究所修讀[聲優課程](../Page/聲優.md "wikilink")。畢業後，先後於電台工作和為動畫配音。

代表作有《[青春紀行](../Page/青春紀行.md "wikilink")》（加賀香子）、《[單色小姐 -The
Animation-](../Page/單色小姐_-The_Animation-.md "wikilink")》（單色小姐）、《[Little
Busters\!](../Page/Little_Busters!.md "wikilink")》（直枝理樹）、《[物語系列](../Page/物語系列.md "wikilink")》（羽川翼）等\[1\]。

## 个人簡歷

  - 1997年，正式以聲優身份出道，其後於1998年推出首張單曲唱片「my best friend」。
  - 2000年，與聲優[田村由香里結成組合](../Page/田村由香里.md "wikilink")「やまとなでしこ」。
  - 2002年，舉辦First Live Tour，個人電台節目「堀江由衣の天使のたまご」於同年開播。
  - 2005年，與[淺野真澄](../Page/淺野真澄.md "wikilink")、[神田朱未](../Page/神田朱未.md "wikilink")、[高橋智秋](../Page/高橋智秋.md "wikilink")、[木村圓組成](../Page/木村圓.md "wikilink")「[Aice<sup>5<sup>](../Page/Aice5.md "wikilink")」組合，堀江小姐為此組合的發起人和隊長。
  - 2006年，舉辦Second Live Tour「堀江由衣をめぐる冒険」。
  - 2006年，加入[17歲教](../Page/17歲教.md "wikilink")。
  - 2007年，成為綜藝節目《HEY\!HEY\!HEY\!》的嘉賓，是該節目第三個聲優嘉賓。
  - 2007年6月，因為前所屬事務所[ARTSVISION社長](../Page/ARTSVISION.md "wikilink")[松田咲實的](../Page/松田咲實.md "wikilink")[猥褻事件](../Page/猥褻.md "wikilink")，于6月5日在個人官方BLOG發表澄清聲明。\[2\]
  - 2007年11月，加入[I'm
    Enterprise所屬的](../Page/I'm_Enterprise.md "wikilink")[VIMS事務所](../Page/VIMS.md "wikilink")。
  - 2009年9月19和20日在[日本武道館舉辦個人演唱會](../Page/日本武道館.md "wikilink")。是繼[椎名碧流](../Page/椎名碧流.md "wikilink")、[水樹奈奈和](../Page/水樹奈奈.md "wikilink")[田村由香里後第](../Page/田村由香里.md "wikilink")4個在此場館舉辦個人演唱會的聲優。

## 個人資料

  - 家中的獨生女。視力並不好需要戴眼鏡（左眼1.0，右眼0.1），自曝于2013年[黄金周期间接受了](../Page/黄金周_\(日本\).md "wikilink")[激光视力矫正手术](../Page/ICL.md "wikilink")。身高155cm。
  - 因為景仰《[搞怪拍檔](../Page/搞怪拍檔.md "wikilink")》的（當時是**[島津冴子](../Page/島津冴子.md "wikilink")**擔任配音），而立志當聲優。
  - 自从出演『光子』以后、工作方面多了很多接触面、主要开始拉（一种美少女类型作品）流派相关作品的出演
    。所以、美少女动画片和萌系游戏作品比较多、黄金时段和早上面向儿童的作品，一般向的作品的出演機会就比较少。但是、最近增加了在洋画风类型的作品的配音工作、比如在[大地丙太郎](../Page/大地丙太郎.md "wikilink")（『[幻影天使](../Page/幻影天使.md "wikilink")』、『[反斗小王子](../Page/反斗小王子.md "wikilink")』、『[十兵衛2](../Page/十兵衛.md "wikilink")』）、[庵野秀明](../Page/庵野秀明.md "wikilink")（『[甜心戰士OVA](../Page/甜心戰士.md "wikilink")』）、[富野由悠季](../Page/富野由悠季.md "wikilink")（[麟光之翼](../Page/麟光之翼.md "wikilink")）裡都得到出演、开始获得了出名监督作品的出演机会。不仅仅作为[偶像声優获得关注](../Page/偶像声優.md "wikilink")、[演技方面也渐渐得到高度的评价](../Page/演技.md "wikilink")。
  - 刚出道的时候、几乎全是男性风作品。在出演了原作中女性角色人气很高的
    『[幻影天使](../Page/幻影天使.md "wikilink")』和『[鬼眼狂刀](../Page/鬼眼狂刀.md "wikilink")』后、女性风的作品也开始变多。
  - 非常討厭[飛機](../Page/飛機.md "wikilink")，在各地間移動多半是坐[火車](../Page/火車.md "wikilink")。
  - 曾說過喜歡的歌手是[GLAY](../Page/GLAY.md "wikilink")，有次看過他們的演唱會DVD後就著迷了。其他還有舉出[B'z](../Page/B'z.md "wikilink")、[Tommy
    february<sup>6</sup>](../Page/川瀬智子.md "wikilink")、[ケツメイシ](../Page/ケツメイシ.md "wikilink")、[彩虹樂團等](../Page/彩虹樂團.md "wikilink")。另外她也喜歡[傑尼斯事務所旗下的男偶像](../Page/傑尼斯事務所.md "wikilink")。
  - 最喜歡的季节是[冬天](../Page/冬天.md "wikilink")。在冷颼颼的空氣中跑出去，觉得皮膚绷紧，评价说特别的喜欢。
  - 與[中原麻衣大概是因為角色性質近似的緣故](../Page/中原麻衣.md "wikilink")，較少同台演出的機會。一起工作的作品只有《[推理之絆](../Page/推理之絆.md "wikilink")》、《[暮蟬悲鳴時
    解](../Page/暮蟬悲鳴時.md "wikilink")》、《[魔偵探洛基](../Page/魔偵探洛基.md "wikilink")》、《[吸血鬼騎士](../Page/吸血鬼騎士.md "wikilink")》、《[半妖少女綺麗譚](../Page/半妖少女綺麗譚.md "wikilink")》、《[FAIRY
    TAIL](../Page/FAIRY_TAIL.md "wikilink")》等。
  - 因为喜欢《[愛麗絲夢遊仙境](../Page/愛麗絲夢遊仙境.md "wikilink")》裡的愛麗絲，在第2及第5張專輯名也用「」（即愛麗絲）標示。
  - 家里东西摆设零乱的事情很出名。以前在『天使のたまご』的介绍网站主页上以打上[马赛克的形式进行公開](../Page/马赛克.md "wikilink")。像是家裡的冷气设备坏掉了，因为家里一直很凌亂，不好意思找人修理。大掃除后、当天是不会进行最后的清理的。在「[Aice<sup>5<sup>](../Page/Aice5.md "wikilink")」官方网站『QandA』被问关于室内装饰问题的时候，就以「这是室内装饰以前的问题」来回答，几句话就带过。
  - 經常出演[赤松健原作的動畫](../Page/赤松健.md "wikilink")。在『[純情房東俏房客](../Page/純情房東俏房客.md "wikilink")』裡出演以東大为目標的女主角[成濑川奈留时](../Page/成濑川奈留.md "wikilink")，參加被称为東大的学園祭的活动。而且在那个时候有进行小提琴的表演（曲目是きらきら星）（這把小提琴是拍專輯『水たまりに映るセカイ』的第1曲目『桜』的PV时使用过的小道具）。
  - 於[東京巨蛋裡進行的](../Page/東京巨蛋.md "wikilink")[職业棒球比賽的时候](../Page/職业棒球.md "wikilink")，由于动画《[六門天外](../Page/六門天外.md "wikilink")》的关系，作为的声優，擔任開球嘉賓。那个时候堀江穿著背號67的球衣出場，背號67號取自於自身所配角色－柊六奈中的『六奈』（）。另外，堀江由衣本身是[讀賣巨人隊的球迷](../Page/讀賣巨人隊.md "wikilink")。
  - 相當怕生，以人氣聲優而言，可以說是客氣過頭。從交友關係來看首先要提到聲優的[田村由香里](../Page/田村由香里.md "wikilink")，其他據說關係良好的聲優有[桑谷夏子](../Page/桑谷夏子.md "wikilink")、[田中理惠](../Page/田中理惠_\(聲優\).md "wikilink")、[淺野真澄](../Page/淺野真澄.md "wikilink")、[神田朱未](../Page/神田朱未.md "wikilink")、[水樹奈奈等人](../Page/水樹奈奈.md "wikilink")，還有隸屬同唱片公司的[angela等](../Page/angela.md "wikilink")。另外由於她喜歡傑尼斯偶像，她曾與[野川櫻一同去看](../Page/野川櫻.md "wikilink")[瀧與翼的演唱會](../Page/瀧與翼.md "wikilink")。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 1998年

<!-- end list -->

  - [秋葉原電腦組](../Page/秋葉原電腦組.md "wikilink")（法藍西斯卡）
  - [快傑蒸氣偵探團](../Page/快傑蒸氣偵探團.md "wikilink")（吉娜）
  - [星际牛仔](../Page/星际牛仔.md "wikilink")（第18話：少女A）
  - [鋼鐵新世紀](../Page/鋼鐵新世紀.md "wikilink")（鉄コミュニケイション）（**小遙**）
  - 聖路美娜女学院（麗塔·福特）
  - [爆走兄弟Let's GO\!\! MAX](../Page/爆走兄弟.md "wikilink")（ケンジ）
  - [魔術師奧菲](../Page/魔術師奧菲.md "wikilink")（フィエナ）

<!-- end list -->

  - 1999年

<!-- end list -->

  - [妖精戰士](../Page/亞克傳承.md "wikilink")（**麗莎**）
  - [To Heart](../Page/To_Heart.md "wikilink")（**HMX-12マルチ**、學生）
  - [無限的未知](../Page/無限的未知.md "wikilink")（蜜雪兒·凱）

<!-- end list -->

  - 2000年

<!-- end list -->

  - [沉默的未知](../Page/沉默的未知.md "wikilink")（蘇·哈里斯）
  - [臣士魔法劇場 死神天使](../Page/臣士魔法劇場_死神天使.md "wikilink")（夏目鈴子）
  - [銀裝騎攻オーディアン](../Page/銀裝騎攻オーディアン.md "wikilink")（卡洛兒）
  - [Sci-Fi HARRY](../Page/Sci-Fi_HARRY.md "wikilink")（**凱薩琳·查普曼**）
  - [純情房東俏房客](../Page/純情房東俏房客.md "wikilink")（**成瀨川奈留**）
  - [六門天外](../Page/六門天外.md "wikilink")（**柊六奈**）
  - 人造人間キカイダー THE ANIMATION（光明寺ミツコ）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [反斗小王子](../Page/反斗小王子.md "wikilink")（根津、小鰺、猿）
  - [天使領域](../Page/天使領域.md "wikilink")（藤森弘美）
  - [妹妹公主](../Page/妹妹公主.md "wikilink")（**咲耶**）
  - [通靈王](../Page/通靈王.md "wikilink")（鋼鐵處女·梅登貞德、リリー）
  - [新白雪姬傳說](../Page/新白雪姬傳說.md "wikilink")（美景/災妃）
  - Tales of Eternia（コリーナ・ソルジェンテ）
  - [フィギュア17](../Page/フィギュア17.md "wikilink") つばさ&ヒカル（茨城櫻）
  - [-{zh-tw:魔法水果籃;zh-cn:水果篮子}-](../Page/幻影天使.md "wikilink")（**本田透**）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [朝霧的巫女](../Page/朝霧的巫女.md "wikilink")（獨樂）
  - [阿倍野橋魔法商店街](../Page/阿倍野橋魔法商店街.md "wikilink")（あみりゅん）
  - [王ドロボウJING](../Page/王ドロボウJING.md "wikilink")（蜜拉貝爾）
  - [Kanon](../Page/Kanon.md "wikilink")（[東映動畫版](../Page/東映動畫.md "wikilink")）（**月宮亞由**）
  - [SAMURAI DEEPER KYO](../Page/鬼眼狂刀.md "wikilink")（鬼眼狂刀）（**椎名由夜**）
  - [妹妹公主 ～Re Pure～](../Page/妹妹公主_RePure.md "wikilink")（**咲耶**）
  - [少年偵探～推理之絆～](../Page/推理之絆.md "wikilink")（竹內理緒）
  - [神奇寶貝](../Page/神奇寶貝.md "wikilink")（第243話，千尋）
  - [陸上防衛隊](../Page/陸上防衛隊.md "wikilink")（**丸山席維亞**、七瀨川奈奈）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [戀愛小魔女](../Page/戀愛小魔女.md "wikilink")（**立石亞由**）
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")（木下沙也加）
  - [七華6/17](../Page/七華6/17.md "wikilink")（雨宮百合子）
  - [初音島](../Page/初音島.md "wikilink")（**白河小鳥**）
  - [瓶詰妖精](../Page/瓶詰妖精.md "wikilink")（**さらら**）
  - [魔偵探洛基](../Page/魔偵探洛基.md "wikilink")（**大堂寺繭良**）
  - ポップンベリーのダンスダンス占い（メロリーナ）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [絢爛舞踏祭](../Page/絢爛舞踏祭.md "wikilink")（東原惠）
  - [十兵衞2 - 西伯利亞柳生的逆襲](../Page/十兵衛.md "wikilink") （**菜乃花自由**）
  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")（**澤近愛理**）
  - [To Heart ～Remember my
    Memories～](../Page/To_Heart.md "wikilink")（**HMX-12マルチ**）
  - [雙戀](../Page/雙戀.md "wikilink")（**一條薰子**）
  - [勇午](../Page/勇午.md "wikilink")～交涉人～2nd
    Negotiation「ロシア編」（第13話，ナージェンカ）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [IGPX](../Page/IGPX.md "wikilink")（傅安婷·萬強）
  - [植木的法則](../Page/植木的法則.md "wikilink")（ペコル）
  - [AIR](../Page/AIR.md "wikilink")（第二集（客串）：女學生B）
  - [初音島第二季](../Page/初音島.md "wikilink")（**白河小鳥**）
  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink")（**上原都**）
  - [雙戀](../Page/雙戀.md "wikilink")（一條薰子） \*第6話のみ
  - [魔法老師](../Page/魔法老師_\(動畫\).md "wikilink")（[佐佐木蒔繪](../Page/佐佐木蒔繪.md "wikilink")）
  - [我們的仙境](../Page/我們的仙境.md "wikilink")（**茶之畑珠實**）
  - [LOVELESS](../Page/LOVELESS.md "wikilink")（銀華）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [犬神！](../Page/犬神！.md "wikilink")（**陽子**）
  - [女生愛女生](../Page/女生愛女生.md "wikilink")（**神泉安菜**）
  - [校園迷糊大王 二學期](../Page/校園迷糊大王.md "wikilink")（**澤近愛理**）
  - [RAY THE
    ANIMATION](../Page/RAY_THE_ANIMATION.md "wikilink")（小堇）\*第7話、第11話、第13話登場
  - [零之使魔](../Page/零之使魔.md "wikilink")（**謝絲塔**）
  - [少女愛上姊姊](../Page/處女愛上姊姊.md "wikilink")（**宮小路瑞穗**）
  - [Kanon](../Page/Kanon.md "wikilink")（[京都動畫版](../Page/京都動畫.md "wikilink")）（**[月宮亞由](../Page/月宮亞由.md "wikilink")**）
  - [魔法老師\!?](../Page/魔法老師_\(動畫\).md "wikilink")（佐佐木蒔繪）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [東京魔人學園劍風帖 龍龍](../Page/東京魔人學園劍風帖.md "wikilink")（**美-{里}-葵**）
  - [東京魔人學園劍風帖 龍龍 第二幕](../Page/東京魔人學園劍風帖.md "wikilink")（**美-{里}-葵**）
  - [藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")（**小鈴**）
  - [校園烏托邦 學美向前衝！](../Page/校園烏托邦_學美向前衝！.md "wikilink")（**天宮學美**）
  - [光明之淚Ｘ風](../Page/光明之淚Ｘ風.md "wikilink")（**[吳羽冬華](../Page/吳羽冬華.md "wikilink")**）
  - [偶像大師 XENOGLOSSIA](../Page/偶像大師_XENOGLOSSIA.md "wikilink")（**荻原雪步**）
  - [天翔少女](../Page/天翔少女.md "wikilink")（藤枝七惠）
  - [驅魔少年](../Page/驅魔少年.md "wikilink")（美玲）
  - [零之使魔～雙月之騎士～](../Page/零之使魔.md "wikilink")（**謝絲塔**）
  - [寒蝉鸣泣之时解](../Page/暮蟬悲鳴時.md "wikilink")（**羽入**）
  - 蜜糖邦尼（白兔）
  - [初音島II](../Page/初音島II.md "wikilink")（**朝倉由夢**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [吸血鬼騎士](../Page/吸血鬼騎士.md "wikilink")（**黑主優姬**）
  - [吸血鬼騎士Guilty](../Page/吸血鬼騎士.md "wikilink")（**黑主優姬**）
  - [迷宮塔～烏魯克之盾～](../Page/迷宮塔～烏魯克之盾～.md "wikilink")（**法蒂娜**）
  - [初音島II 第二季](../Page/初音島II.md "wikilink")（**朝倉由夢**）
  - [屍姬 赫](../Page/屍姬.md "wikilink")（轟旗神佳）
  - [我家有個狐仙大人](../Page/我家有個狐仙大人.md "wikilink")（高上美夜子）
  - [一騎當千 Great Guardians](../Page/一騎當千.md "wikilink")（**孫權仲謀**）
  - [零之使魔～三美姬的輪舞～](../Page/零之使魔.md "wikilink")（**謝絲塔**）
  - [TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink")（**栉枝实乃梨**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [海猫悲鳴時](../Page/海猫悲鳴時.md "wikilink")（**右代宮真里亞、瑪利亞**）
  - [迷宮塔～烏魯克之劍～](../Page/迷宮塔～烏魯克之劍～.md "wikilink")（**法蒂娜**）
  - [夏之嵐](../Page/夏之嵐.md "wikilink")（**山崎加奈子**）
  - [天才麻將少女](../Page/天才麻將少女.md "wikilink")（**福路美穗子**）
  - [旋风管家 第二季](../Page/旋风管家.md "wikilink")（索尼婭・夏芙羅娜茲）
  - [青花](../Page/青花_\(漫畫\).md "wikilink")（井汲京子）
  - [化物語](../Page/化物語.md "wikilink")（**羽川翼**）
  - [魯邦三世VS名偵探柯南](../Page/魯邦三世VS名偵探柯南.md "wikilink")（**米菈王女**）
  - [屍姬 玄](../Page/屍姬.md "wikilink")（轟旗神佳、黒猫、理子）
  - [GA 藝術科美術設計班](../Page/GA_藝術科美術設計班.md "wikilink")（**野崎奈三子**）
  - [加奈日記](../Page/加奈日記.md "wikilink")（**西田春華**）
  - [海物語](../Page/海物語.md "wikilink")（**烏琳**）
  - [肯普法](../Page/肯普法.md "wikilink")（**美嶋紅音**）
  - [信蜂](../Page/信蜂.md "wikilink")（露達）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [一騎當千 XTREME XECUTOR](../Page/一騎當千.md "wikilink")（孫權仲謀）
  - [Battle
    Spirits少年激霸彈](../Page/Battle_Spirits少年激霸彈.md "wikilink")（セイル）
  - [發現、體驗、最喜歡！巧虎](../Page/可愛巧虎島.md "wikilink")（新幹線之声）
  - [B型H系](../Page/B型H系.md "wikilink")（**竹下美春**）
  - [嬌蠻貓娘大橫行](../Page/嬌蠻貓娘大橫行.md "wikilink")（鳴子-{叶}-繪）
  - [大神與七位夥伴](../Page/大神與…系列.md "wikilink")（**桐木愛麗斯**）
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")（**雪女（及川冰麗）**、雪麗）
  - [半妖少女綺麗譚](../Page/半妖少女綺麗譚.md "wikilink")（**鬼燈**）
  - [信蜂Reverse](../Page/信蜂.md "wikilink")（露達）
  - [FAIRY TAIL魔導少年](../Page/FAIRY_TAIL魔導少年.md "wikilink")（夏璐璐）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [龍之界點](../Page/龍之界點.md "wikilink")（**瑪露卡**）
  - [肯普法 für die Liebe](../Page/肯普法.md "wikilink")（**美嶋紅音**）
  - [放浪男孩](../Page/放浪男孩.md "wikilink")（末廣安那）
  - [DOG DAYS](../Page/DOG_DAYS.md "wikilink")（**米爾希奧蕾・F・比斯科迪**）
  - [亞斯塔蘿黛的後宮玩具](../Page/蘿黛的後宮玩具.md "wikilink")（**艾爾芙蕾妲·謬亞斯德托**）
  - [眾神中的貓神](../Page/眾神中的貓神.md "wikilink")（**古宮柚子**）
  - [妖怪少爺 千年魔京](../Page/百鬼小當家.md "wikilink")（**雪女（及川冰麗）**、雪麗）
  - [快盜天使雙胞胎](../Page/快盜天使雙胞胎.md "wikilink")（特斯菈）
  - [迴轉企鵝罐](../Page/迴轉企鵝罐.md "wikilink")（夏芽真砂子、愛絲梅拉達）
  - [日常](../Page/日常.md "wikilink")（第23話預告旁白）
  - [女神異聞錄4](../Page/女神異聞錄4.md "wikilink")（**里中千枝**）
  - [便·當](../Page/便·當.md "wikilink")（澤桔鏡）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [偽物語](../Page/偽物語.md "wikilink")（羽川翼）
  - [零之使魔F](../Page/零之使魔.md "wikilink")（**謝絲塔**）
  - [猛烈宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")（庫里爾）
  - [Battle Spirits Sword
    Eyes](../Page/Battle_Spirits_Sword_Eyes.md "wikilink")（**黃纓**）
  - [要聽爸爸的話！](../Page/要聽爸爸的話！.md "wikilink")（**織田萊香**）
  - [在盛夏等待](../Page/在盛夏等待.md "wikilink")（貴月繪美香）
  - [AKB0048](../Page/AKB0048.md "wikilink")（**第六代 柏木由紀**）
  - [光明之心 ～幸福的麵包～](../Page/光明之心_～幸福的麵包～.md "wikilink")（露菲娜）
  - [翡翠森林狼與羊](../Page/翡翠森林狼與羊.md "wikilink")（小咩）
  - [DOG DAYS'](../Page/DOG_DAYS.md "wikilink")（**米爾希奧蕾・F・比斯科迪**）
  - [輕鬆百合♪♪](../Page/輕鬆百合.md "wikilink")（赤座茜）
  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（妄想優）
  - [天才麻將少女 阿知賀篇](../Page/天才麻將少女.md "wikilink")（福路美穗子）
  - [戰國Collection](../Page/戰國Collection.md "wikilink")（劉備）
  - [境界線上的地平線II](../Page/境界線上的地平線.md "wikilink")（瑪麗）
  - [K](../Page/K_\(動畫\).md "wikilink")（**櫛名安娜**）
  - [Little Busters\!](../Page/Little_Busters!.md "wikilink")（**直枝理樹**）
  - [櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")（**赤坂龍之介**、女僕）
  - [元氣少女緣結神](../Page/元氣少女緣結神.md "wikilink")（沼皇女）
  - [來自新世界](../Page/來自新世界.md "wikilink")（天野麗子）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（雅姆萊哈）
  - [旋風管家！CAN'T TAKE MY EYES OFF
    YOU](../Page/旋風管家.md "wikilink")（索尼婭・夏芙羅娜茲）
  - [貓物語（黑）](../Page/貓物語.md "wikilink")（**羽川翼**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [AKB0048 Next Stage](../Page/AKB0048.md "wikilink")（**第六代 柏木由紀**）
  - [革命機Valvrave](../Page/革命機Valvrave.md "wikilink")（七海理音，皇子）
  - [YUYU式](../Page/YUYU式.md "wikilink")（松本賴子）
  - [DD北斗神拳](../Page/DD北斗神拳.md "wikilink")（**尤莉亞**、豬）
  - [最強銀河 究極ZERO Battle
    Spirits](../Page/最強銀河_究極ZERO_Battle_Spirits.md "wikilink")（**啟明星之愛莉絲**）
  - [戰姬絕唱SYMPHOGEAR
    G](../Page/戰姬絕唱SYMPHOGEAR.md "wikilink")（莎麗娜·卡登查瓦納·伊娃）
  - [物語系列 第二季](../Page/物語系列_第二季.md "wikilink")（**羽川翼**）
  - [超次元戰記 戰機少女](../Page/超次元戰記_戰機少女.md "wikilink")（**涅普姬雅**）
  - [單色小姐 -The
    Animation-](../Page/單色小姐_-The_Animation-.md "wikilink")（**單色小姐**）
  - [核爆末世錄](../Page/核爆末世錄.md "wikilink")（**小津歌音**）
  - [青春紀行](../Page/青春紀行.md "wikilink")（**加賀香子**）
  - [Little Busters\!
    ～Refrain～](../Page/Little_Busters!.md "wikilink")（**直枝理樹**）
  - [魔奇少年 The Kingdom of Magic](../Page/魔奇少年.md "wikilink")（雅姆萊哈）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [天才麻將少女 全國篇](../Page/天才麻將少女.md "wikilink")（福路美穗子）
  - [T寶的悲慘日常 覺醒篇](../Page/T寶的悲慘日常.md "wikilink")（單色小姐）
  - [FAIRY TAIL 新系列](../Page/FAIRY_TAIL.md "wikilink")（夏璐璐）
  - [黑色子彈](../Page/黑色子彈.md "wikilink")（**天童木更**）
  - [RAIL
    WARS\!](../Page/RAIL_WARS!_-日本國有鐵道公安隊-.md "wikilink")（**飯田奈奈**）
  - [生存遊戲社](../Page/生存遊戲社.md "wikilink")（佐倉江那）
  - [女神異聞錄4 黃金版](../Page/女神異聞錄4.md "wikilink")（**里中千枝**）
  - [CROSSANGE
    天使與龍的輪舞](../Page/CROSSANGE_天使與龍的輪舞.md "wikilink")（**莎拉曼蒂涅/莎拉**）
  - [巴哈姆特之怒 GENESIS](../Page/巴哈姆特之怒.md "wikilink")（**蒂娜**）
  - [-{zh-cn:临时女友;zh-tw:女友伴身邊}-](../Page/臨時女友.md "wikilink")（單色小姐）
  - [RAIL WARS\!
    -日本國有鐵道公安隊-](../Page/RAIL_WARS!_-日本國有鐵道公安隊-.md "wikilink")（**飯田奈奈**）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [絕對雙刃](../Page/絕對雙刃.md "wikilink")（九十九朔夜）
  - [大家集合！Falcom 學園](../Page/大家集合！Falcom_學園.md "wikilink")
    第二季（亞莉莎·萊恩福爾特）
  - [艦隊Collection -艦Colle-](../Page/艦隊收藏.md "wikilink")（間宮）
  - [血型小將ABO 2](../Page/血型小將ABO.md "wikilink")（B型女）
  - [卡片鬥爭\!\! 先導者 G](../Page/卡片鬥爭!!_先導者.md "wikilink")（新導未來）
  - [DOG DAYS''](../Page/DOG_DAYS.md "wikilink")（**米爾希奧蕾·F·比斯科迪**）
  - [關於完全聽不懂老公在說什麼的事情](../Page/關於完全聽不懂老公在說什麼的事情.md "wikilink") 第2期（木村柚子）
  - [放學後的昴星團](../Page/放學後的昴星團.md "wikilink")（艾爾娜特）
  - [青春×機關槍](../Page/青春×機關槍.md "wikilink")（矢島鼎）
  - [單色小姐 -The Animation-
    2](../Page/單色小姐_-The_Animation-.md "wikilink")（**單色小姐**）
  - [T寶的悲慘日常 夢幻篇](../Page/T寶的悲慘日常.md "wikilink")（單色小姐、美女A）
  - [Classroom☆Crisis](../Page/Classroom☆Crisis.md "wikilink")（**小鳥遊薰子**）
  - [下流梗不存在的灰暗世界](../Page/下流梗不存在的灰暗世界.md "wikilink")（鬼頭鼓修理）
  - [那就是聲優！](../Page/那就是聲優！.md "wikilink")（堀江由衣）
  - [K RETURN OF KINGS](../Page/K_\(動畫\).md "wikilink")（**櫛名安娜**）\[3\]
  - [終物語](../Page/終物語.md "wikilink")（**羽川翼**）
  - [輕鬆百合 3☆High！](../Page/輕鬆百合.md "wikilink")（赤座茜）
  - [緋彈的亞莉亞AA](../Page/緋彈的亞莉亞.md "wikilink")（**夾竹桃**）
  - [DD北斗神拳2](../Page/DD北斗神拳.md "wikilink")（**尤莉亞**）
  - [全部成為F THE PERFECT INSIDER](../Page/全部成為F.md "wikilink")（儀同世津子）
  - 血型小將ABO 3（B型女）

<!-- end list -->

  - 2016年

<!-- end list -->

  - 血型小將ABO 4（B型女）
  - 卡片鬥爭\!\! 先導者 G 齒輪危機篇（新導未來）
  - [為美好的世界獻上祝福！](../Page/為美好的世界獻上祝福！.md "wikilink")（維茲）
  - [魔法使 光之美少女！](../Page/魔法使_光之美少女！.md "wikilink")（**十六夜理子／魔法天使**）
  - [Re:從零開始的異世界生活](../Page/Re:從零開始的異世界生活.md "wikilink")（菲利克斯·阿蓋爾）
  - [HUNDRED百武裝戰記](../Page/HUNDRED百武裝戰記.md "wikilink")（**夏洛特·迪曼迪鄂斯**）
  - [在下坂本，有何貴幹？](../Page/在下坂本，有何貴幹？.md "wikilink")（黑沼愛菜）
  - [初戀怪獸](../Page/初戀怪獸.md "wikilink")（**二階堂夏步**）

<!-- end list -->

  - 2017年

<!-- end list -->

  - （奧村文代）

  - 為美好的世界獻上祝福！2（維茲）

  - [數碼暴龍宇宙-應用怪獸](../Page/數碼暴龍宇宙-應用怪獸.md "wikilink")（神樂坂泉）

  - 戰姬絕唱SYMPHOGEAR AXZ（莎麗娜·卡登查瓦納·伊娃）

  - [便利商店情人](../Page/便利商店情人.md "wikilink")（飛鳥井愛姬）

  - [帶著智慧型手機闖蕩異世界。](../Page/帶著智慧型手機闖蕩異世界。.md "wikilink")（戀愛神）

  - [悠久持有者：魔法老師！ 第2部](../Page/悠久持有者.md "wikilink")（佐佐木蒔繪）

  - [國王遊戲 The
    Animation](../Page/國王遊戲_The_Animation.md "wikilink")（**本多奈津子**）

  - [如果有妹妹就好了。](../Page/如果有妹妹就好了。.md "wikilink")（ヒルデ・レッドフィールド）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [BASILISK～櫻花忍法帖～](../Page/BASILISK～櫻花忍法帖～.md "wikilink")（夜叉至）

  - [HUG\! 光之美少女](../Page/HUG!_光之美少女.md "wikilink")（十六夜理子／魔法天使）

  - [棒球大聯盟2nd](../Page/棒球大聯盟.md "wikilink")（眉村道壘）

  - [甜心戰士 Universe](../Page/甜心戰士_Universe.md "wikilink")（**秋夏子**\[4\]）

  - （夏離江）

  - [京都寺町三條商店街的福爾摩斯](../Page/京都寺町三條商店街的福爾摩斯.md "wikilink")（宮下佐織\[5\]）

  - [LORD of VERMILION
    紅蓮之王](../Page/LORD_of_VERMILION_紅蓮之王.md "wikilink")（一條樹里亞\[6\]）

  - [我喜歡的妹妹不是妹妹](../Page/我喜歡的妹妹不是妹妹.md "wikilink")（咲耶）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [流汗吧！健身少女](../Page/流汗吧！健身少女.md "wikilink")（**立花里美**）

### OVA

  - [人造人間キカイダー](../Page/人造人間キカイダー.md "wikilink") THE ANIMATION（光明寺ミツコ）
  - [ありさ☆GOOD LUCK](../Page/ありさ☆GOOD_LUCK.md "wikilink")（西崎ありさ）
  - [エクソダスギルティー](../Page/エクソダスギルティー.md "wikilink")（スィー）
  - [カナリア ～この想いを歌に乗せて～](../Page/カナリア_～この想いを歌に乗せて～.md "wikilink")（音羽まどか）
  - [Re: 甜心战士](../Page/Re:_甜心战士.md "wikilink")（如月甜心／甜心战士）
  - [超神姫ダンガイザー3](../Page/超神姫ダンガイザー3.md "wikilink")（ピクシス）
  - [フォトン](../Page/フォトン.md "wikilink")（アウン・フレイヤ）
  - [純情房東俏房客 Again](../Page/純情房東俏房客.md "wikilink")（**成瀨川奈留**）
  - [校園迷糊大王OVA 一学期補習](../Page/校園迷糊大王.md "wikilink")（**澤近愛理**）
  - [天翔少女](../Page/天翔少女.md "wikilink")（藤枝七惠）
  - [初音島if](../Page/初音島.md "wikilink")（**白河小鳥**）
  - [寒蟬鳴泣之時礼](../Page/寒蝉鸣泣之时_\(动画\).md "wikilink")（**羽入**）
  - [寒蟬鳴泣之時煌](../Page/寒蝉鸣泣之时_\(动画\).md "wikilink")（**羽入**）
  - [寒蟬鳴泣之時扩](../Page/寒蝉鸣泣之时_\(动画\).md "wikilink")（**羽入**）

**2004年**

  - [Re甜心戰士](../Page/Re甜心戰士.md "wikilink")（**如月甜心**）

**2008年**

  - [魔法老師～白之翼～](../Page/魔法老師.md "wikilink")（佐佐木蒔繪）

**2009年**

  - [魔法老師～另一个世界～](../Page/魔法老師.md "wikilink")（佐佐木蒔繪）

**2013年**

  - [暗殺教室](../Page/暗殺教室.md "wikilink")（**伊莉娜·葉拉維琪**）
  - [翠星上的加爾岡緹亞](../Page/翠星上的加爾岡緹亞.md "wikilink")（斯托莉亞）

**2014年**

  - [Little Busters\!
    EX](../Page/Little_Busters!#OVA「Little_Busters!_EX」.md "wikilink")（**直枝理树**）
  - [三人舞妓](../Page/三人舞妓.md "wikilink")（**一之瀨麻衣**）

**2016年**

  - [FAIRY TAIL 妖精們的懲罰遊戲](../Page/FAIRY_TAIL_\(動畫\).md "wikilink")（夏璐璐）
  - [為美好的世界獻上祝福！](../Page/為美好的世界獻上祝福！.md "wikilink")（維茲）※小說第9卷限定版附贈BD
  - FAIRY TAIL 納茲vs.梅比斯（**夏璐璐**）
  - FAIRY TAIL 妖精們的聖誕節（**夏璐璐**）

**2017年**

  - [YUYU式 給人添麻煩、被人添麻煩](../Page/YUYU式.md "wikilink")（松本賴子）

### 劇場版動畫

  - [幸運女神](../Page/幸運女神.md "wikilink")（クロノ）
  - [欢迎来到Pia Carrot\!\!](../Page/欢迎来到Pia_Carrot!!系列.md "wikilink")
    （天野織江）
  - [六門天外](../Page/六門天外.md "wikilink")（柊六奈）
  - [超劇場版Keroro軍曹3-Keroro對Keroro
    空中大決戰](../Page/Keroro軍曹.md "wikilink")（ミルル）
  - [大雄的太阳王传说](../Page/大雄的太阳王传说.md "wikilink")（少女）
  - [新·大雄的宇宙開拓史](../Page/新·大雄的宇宙開拓史.md "wikilink")（莫莉娜10歲）
  - [大雄的秘密道具博物馆](../Page/大雄的秘密道具博物馆.md "wikilink") （金嘉）
  - [蠟筆小新：風起雲湧！金矛之勇者](../Page/蠟筆小新.md "wikilink")（瑪塔·塔米）
  - [劇場版 猛烈宇宙海賊](../Page/迷你裙宇宙海賊.md "wikilink")（庫里爾）
  - [傷物語](../Page/傷物語.md "wikilink")（**羽川翼**）
  - [劇場版 K MISSING KINGS](../Page/K_\(動畫\).md "wikilink")（**櫛名安娜**）
  - 剧场版！[Happiness Charge
    光之美少女！人偶王國的芭蕾舞者](../Page/Happiness_Charge_光之美少女！.md "wikilink")！（**紡**)
  - FAIRY TAIL 魔導少年劇場版 : 鳳凰巫女 (夏璐璐)
  - 電影 [光之美少女 All Stars
    大家歌唱吧♪奇跡的魔法！](../Page/光之美少女_All_Stars_大家歌唱吧♪奇跡的魔法！.md "wikilink")
    （**理子／魔法天使**)
  - 電影 [光之美少女 Dream
    Stars\!](../Page/光之美少女_Dream_Stars!.md "wikilink")（**理子／魔法天使**）

### 遊戲

  - [桃色大戰ぱいろん](../Page/桃色大戰ぱいろん.md "wikilink")（姬花）

  - [女神Online線上遊戲](../Page/女神Online.md "wikilink")（蜜兒）

  - [飄流幻境線上遊戲](../Page/飄流幻境.md "wikilink")（愛麗絲）

  - [EVE burst error
    PLUS](../Page/EVE_burst_error_PLUS.md "wikilink")（御堂真彌子）

  - [EVE The Fatal
    Attraction](../Page/EVE_The_Fatal_Attraction.md "wikilink")（藤井由香）

  - [Lの季節～A piece of
    memories～](../Page/Lの季節～A_piece_of_memories～.md "wikilink")（舞波優希）

  - [小魔女帕妃2](../Page/小魔女帕妃2.md "wikilink")（露提爾）

  - [天使演唱會](../Page/天使演唱會.md "wikilink")（瑟菲·史威尼）

  - [AS～天使小夜曲](../Page/AS～天使小夜曲.md "wikilink")（思多法）

  - [Kanon](../Page/Kanon.md "wikilink")（月宮亞由）

  - Dead or Alive（Hitomi）

  - 機動戰士ガンダム クライマックスU.C.（愛倫·羅許菲爾）

  - 九龍妖魔學園紀（雛川亞柚子）

  - 仙界通錄正史（碧雲）

  - グローランサーIII（ラミィ）

  - GENJI（皆鶴姬）

  - 西遊記（三藏法師）

  - SAKURA～雪月華～（草薙小雪）

  - 式神之城II（結城小夜）

  - [妹妹公主](../Page/妹妹公主.md "wikilink")（咲耶）

  - [Shining Force Neo](../Page/Shining_Force_Neo.md "wikilink")（メリル）

  - [SHINING
    WIND](../Page/SHINING_WIND.md "wikilink")（[吳羽冬華](../Page/吳羽冬華.md "wikilink")）

  - 新世紀エヴァンゲリオン 碇シンジ育成計畫（最上葵）

  - ゼーガペイン XOR（ミオ・レディネス）

  - センチメンタルプレリュード（仁科あゆみ）

  - [初音島Plus Situation](../Page/初音島Plus_Situation.md "wikilink")（白河小鳥）

  - [D.C.F.S. ～ダ・カーポ～
    フォーシーズンズ](../Page/D.C.F.S._～ダ・カーポ～_フォーシーズンズ.md "wikilink")（白河小鳥）

  - 探偵神宮寺三郎 燈火が消えぬ間に（松澤祥香）

  - デッドオアアライブ（ヒトミ）

  - 東京魔人學園劍風帖（美-{里}-葵）

  - 東京魔人學園外法帖（美-{里}-藍）

  - [To Heart](../Page/To_Heart.md "wikilink")（HMX-12マルチ）

  - ドキドキプリティリーグ Lovely Star（新野未來）

  - エターナルアルカディア（ファイナ）

  - （宗像尚美）

  - 鋼の鍊金術師3 神を繼ぐ少女（ジャニス）

  - ビストロ・きゅーぴっと（ラベンダ・スウィート）

  - 雙戀-フタコイ-（一條薰子）

  - フタコイ オルタナティブ \~戀と少女とマシンガン\~（一條薰子）

  - 雙戀島 ～戀と水著のサバイバル～（一條薰子）

  - Blade dancer<ブレイドダンサー>《劍舞者：千年之約定》（フェリス・リヒテル）

  - マグナカルタ（リース）

  - [魔法老師\!1時間目 這個小孩子老師是魔法使\!](../Page/魔法老師#遊戲.md "wikilink")（佐佐木蒔繪）

      - 魔法老師\!2時間目 戰鬥吧少女們\! 麻帆良大運動會SP\!（佐佐木蒔繪）
      - 魔法老師\!課外授業 少女們心動的海灘風情（佐佐木蒔繪）
      - 魔法老師\!3時間目 戀愛和魔法和世界樹傳說（佐佐木蒔繪）
      - 魔法老師\!どりーむたくてぃっく -夢見る乙女はプリンセス♥-（佐佐木蒔繪）

  - まぼろし月夜/まぼろし月夜～月夜野綺譚～（朝霧あやめ）

  - モンスターキングダム・ジュエルサモナー（エリシア）

  - [光明之風](../Page/光明之風.md "wikilink")（**[吳羽冬華](../Page/吳羽冬華.md "wikilink")**）

  - 悠久幻想曲3 Perpetual Blue（フローネ・トリーティア）

  - ラクガキ王國2 魔王城の戰い（パステル）

  - ラジアータ ストーリーズ（ナツメ・ナギ）

  - [Love Hina ～愛在言語中～](../Page/純情房東俏房客.md "wikilink")（成瀨川奈留）

  - ランブルローズ（藍原誠/ザ・ブラック・ベルト・デーモン）

  - エンジェリック・コンサート（サフィ・スィーニー）

  - エンジェリック・コンサート アンコール（サフィ・スィーニー）

  - [武裝神姬](../Page/武裝神姬.md "wikilink")（アーク）

  - [女神異聞錄4](../Page/女神異聞錄4.md "wikilink")（里中千枝）

  - [艾爾之光](../Page/艾爾之光.md "wikilink")（蕾娜）

  - [幻月之歌-Divina-](../Page/幻月之歌.md "wikilink")（**克羅莉絲**、玩家角色 A、B）

  - [超次元戰記 戰機少女mk2](../Page/超次元戰記_戰機少女mk2.md "wikilink")（**涅普姬雅**）

  - [神次元戰記 戰機少女 V](../Page/神次元戰記_戰機少女_V.md "wikilink")（**涅普姬雅**）

  - [快盜天使～時間與世界迷宮～](../Page/快盜天使～時間與世界迷宮～.md "wikilink")（テスラ）

  - [第七龍神2020](../Page/第七龍神2020.md "wikilink")

  - [少女射擊 GALGUN](../Page/少女射擊_GALGUN.md "wikilink")（えころ）

  - [現在就想告訴哥哥，我是妹妹！](../Page/現在就想告訴哥哥，我是妹妹！.md "wikilink")（三谷良子）

  - [英雄傳說 閃之軌跡](../Page/英雄傳說_閃之軌跡.md "wikilink")（**亞莉莎·萊恩福爾特**）

  - [學園K -Wonderful School Days-](../Page/K_\(動畫\).md "wikilink")（櫛名安娜）

  - [光明之刃](../Page/光明之刃.md "wikilink")（[吴羽冬华](../Page/吴羽冬华.md "wikilink")）

  - [女友伴身邊](../Page/女友伴身邊.md "wikilink")（單色小姐、真白透子）

  - [落櫻散華抄](../Page/落櫻散華抄.md "wikilink")（**千櫻院麗子**、優子）

  - [英雄傳說 閃之軌跡II](../Page/英雄傳說_閃之軌跡II.md "wikilink")（**亞莉莎·萊恩福爾特**）

  - [Hortensia
    SAGA-蒼之騎士團](../Page/Hortensia_SAGA-蒼之騎士團.md "wikilink")（**馬利歐斯·卡斯特雷德**）

  - [Divina
    Cute](../Page/Divina_Cute.md "wikilink")（[玉藻前](../Page/玉藻前.md "wikilink")、[布倫希爾德](../Page/布倫希爾德.md "wikilink")）

  - [白貓Project](../Page/白貓Project.md "wikilink")（**愛麗絲**、**凱朵拉**）

  - [少女前線](../Page/少女前線.md "wikilink")（春田）

  - [CROSSANGE
    天使與龍的輪舞tr.](../Page/CROSSANGE_天使與龍的輪舞tr..md "wikilink")（**莎拉曼蒂涅/莎拉**）

  - [超級機器人大戰V](../Page/超級機器人大戰V.md "wikilink")（莎拉曼蒂涅/莎拉）

  - [碧藍航線](../Page/碧藍航線.md "wikilink")（貝爾法斯特）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [閃耀幻想曲](../Page/閃耀幻想曲.md "wikilink")（松本賴子、野崎奈三子）
  - [永遠的7日之都](../Page/永遠的7日之都.md "wikilink")（麗）
  - [英雄傳說 閃之軌跡III](../Page/英雄傳說_閃之軌跡III.md "wikilink")（**亞莉莎·萊恩福爾特**）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [魔法紀錄 魔法少女小圓外傳](../Page/魔法紀錄_魔法少女小圓外傳.md "wikilink")（貞德、八雲御魂、羽川翼）
  - [Fate/Grand
    Order](../Page/Fate/Grand_Order.md "wikilink")（[阿龍](../Page/楢崎龍.md "wikilink")）
  - [閃之軌跡IV -THE END OF
    SAGA-](../Page/閃之軌跡IV_-THE_END_OF_SAGA-.md "wikilink")（**亞莉莎·萊恩福爾特**）
  - [超異域公主連結 Re:Dive](../Page/超異域公主連結_Re:Dive.md "wikilink")（佐佐木 咲戀）
  - [夢幻模擬戰](../Page/夢幻模擬戰.md "wikilink") (雪莉）

### 外語影片配音

  - 鬼教師ミセス・ティングル（トルーディ・タッカー）
  - エヴァとステファンとすてきな家族（エヴァ）
  - キス☆キス☆バン☆バン（マルティン・マカッチョン）
  - キャロルの初恋（キャロル）
  - 金魚のしずく（P）
  - ザ・リング（サマラ）※フジテレビ『プレミアムステージ』放映版のみ
  - 鬼水怪談西洋篇（莎茜）
  - 太極神拳（クリスティー・チェン）
  - 20 30 40の恋（アンジェリカ・リー）
  - フェリシティーの青春（役名不明）
  - ベイウォッチIV（役名不明）

### 廣播劇CD

  - 廣播劇CD『Weiß kreuzDramatic CollectionII ENDLESS RAIN』（藤宮彩）
  - 廣播劇CD『天然女子高物語』（中野ちとせ）
  - 廣播劇CD『逆境ナイン』（月田明子）
  - 『コミックスイメージミニアルバム スクールランブル』（澤近愛理）
  - 電台CD『D.C.～ダ・カーポ～初音島放送局1』（白河ことり）
  - 電台CD『D.C.～ダ・カーポ～初音島放送局2』（白河ことり）
  - 電台CD『D.C.～ダ・カーポ～初音島放送局4』（白河ことり）
  - 電台CD『初音島放送局S.S. Vol.1』（白河ことり）
  - 《SAMURAI DEEPER KYO》シリーズ（椎名ゆや）
  - 廣播劇CD『D.C. ～ダ・カーポ～初音島ドラマシアター』（白河ことり）
  - 廣播劇CD『天正やおよろず』（迅伐）
  - 『ながされて藍蘭島Vol.1～2』（すず）
  - 廣播劇CD『ぱにぽに Vol.1～3』（上原都）
  - 廣播劇CD『ぱにぽにセカンドシーズン Vol.2～3』（上原都）（Vol.1には出演せず）
  - 廣播劇CD『封殺鬼』シリーズ（秋川佐穗子）
  - 廣播劇CD『[薔薇少女](../Page/薔薇少女.md "wikilink")』（真紅）
  - 廣播劇CD《[魔法水果籃](../Page/魔法水果籃.md "wikilink")》（本田透）
  - 廣播劇CD《戀愛小魔女》（立石亞由）
  - 廣播劇CD『スパイラル～推理の絆～ もうパズルなんて解かない』（竹内理緒）
  - 廣播劇CD『撲殺天使ドクロちゃん』（ドクロちゃん）
  - 廣播劇CD『ちとせげっちゅ\!\!』（桜庭ちとせ）
  - 『ワイルドアームズ 2ndイグニッション オリジナルドラマ』（マリアベル・アーミティッジ）
  - 廣播劇CD《灼眼的夏娜》（夏娜）
  - 『銀のヴァルキュリアス』Vol.1～2（ルカ）
  - 『お姉ちゃんの3乗』（???）
  - [守護月天](../Page/守護月天.md "wikilink")\! 再逢（フェイ（飛））
  - 廣播劇CD《WILD LIFE》Vol.1（瀬能みか）
  - 廣播劇CD [GOSICK](../Page/GOSICK.md "wikilink") -ゴシック-（セシル・ラフィット）
  - 廣播劇CD《紳士同盟†》（天宮潮）
  - 廣播劇CD《[萌·怪盜蕾莉絲](../Page/萌·怪盜蕾莉絲.md "wikilink")》（蕾莉娜、蕾莉絲）
  - 廣播劇CD《[月夜的奶酪](../Page/月夜的奶酪.md "wikilink")》（芽-{里}-）
  - BD特典廣播劇《[輕鬆百合](../Page/輕鬆百合.md "wikilink")》（赤座茜）
  - 廣播劇CD《[這間教室被不回家社占領了](../Page/這間教室被不回家社占領了.md "wikilink")》（**木瀧恋子**）
  - 廣播劇CD《[櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")》（赤坂龍之介）
  - 廣播劇CD《[寄生彼女砂奈](../Page/寄生彼女砂奈.md "wikilink")》（**砂奈**）
  - 廣播劇CD《[戀愛研究所](../Page/戀愛研究所.md "wikilink")》（**真木夏緒**）
  - 廣播劇CD《[奮鬥吧！系統工程師](../Page/奮鬥吧！系統工程師.md "wikilink")》（海鷗（賀茂芽衣））

### 網絡動畫

  - リーンの翼（エレボス）

**2017年**

  - [玉家當鋪](../Page/玉家當鋪.md "wikilink")（**Nyamashita**\[7\]、狐婆、光）
  - [梦王国与沉睡的100王子 短篇动画](../Page/梦王国与沉睡的100王子.md "wikilink")（高修）

### 寫實

  - 千年王国三銃士ヴァニーナイツ（アリス・ラ・ゾアニス※声のみ/君島セリア/有栖川ゆう）
  - 魔法先生ネギま\!麻帆良学園中等部2-A:三学期特典DVD 麻帆良学園中等部2-A:二学期終業式
  - School Rumble presents "Come\! Come\! Well-Come? party"

### 廣播節目

  - VOICE CREW（1998年4月5日～9月27日）
  - SOMETHING DREAMS マルチメディアカウントダウン（1998年10月3日～2002年10月5日）
  - レギュラー出演は上記の期間。ゲスト出演は「ドリカンクラブ」として1996年9月28日より。
  - ラジオどっとあい「脱！やまとなでしこ宣言」（2000年4月～6月）
  - オレたちやってま～す木曜日（2000年10月～2002年3月）
  - 堀江由衣の天使のたまご（2002年10月6日～）
  - マグナカルタRADIO（2004年7月4日～9月26日）
  - 初音島放送局S.S.（2005年7月15日～10月7日）
  - 佳奈・由衣・ゆかりのかしましらじお（2005年9月30日～2006年10月27日）
  - ほっちゃん ますみんのめろメロラジオ～まほ・ぱに番外編～（ドワンゴパケットラジオ）
  - Aice5 In Wonder RADIO（2006年8月21日～2007年9月14日）

### CM

  - ライコスジャパン『LYCOSメールで約束』編・クリスマス編
  - 白泉社『花とゆめ』
  - プリンセスソフト『～SAKURA～雪月華』
  - 講談社『週刊少年マガジン』2005年3月度（『スクールランブル』沢近愛理として）
  - 角川書店『D.C.Four Seasons』
  - メディアワークス『双恋』（一条薫子として菫子である「小清水亜美」と一緒に）
  - バンプレスト『マグナカルタ』（カリンツ役の保志総一朗と一緒にリースで）
  - 萬代影視『十兵衛ちゃん～ラブリー眼帯の秘密～』DVD-BOX（自由として）
  - ジェネオンエンタテインメント『To Heart Remenber my memories』DVDシリーズ（マルチとして）
  - キングレコード『ラブひな』DVD-BOX（成瀬川なるとして）

### 其他

  - Tech Win OSアイドルWinちゃん
  - 迷？探偵クリス（安城市文化センター内プラネタリウムで上映）（クリス）

## 歌手活動

堀江由衣是Starchild所属非常活躍的歌手。詳細請參閱[堀江由衣的作品](../Page/堀江由衣的作品.md "wikilink")。

### 單曲

  - **于[波丽佳音](../Page/波丽佳音.md "wikilink")（Pony Canyon）旗下**
    1.  my best friend

    2.
  - **于[Starchild Records旗下](../Page/Starchild.md "wikilink")**
    1.  Love
        Destiny（2001年5月16日）動畫《[妹妹公主](../Page/妹妹公主.md "wikilink")》片頭曲・片尾曲
    2.  キラリ☆宝物（2002年2月28日）動畫《》片頭曲・片尾曲
    3.  ALL MY LOVE（2002年7月24日）動畫《》片頭曲・片尾曲
    4.  心晴れて 夜も明けて（2004年2月4日）動畫《》主題曲
    5.  スクランブル（2004年10月27日，以**堀江由衣 with
        [UNSCANDAL](../Page/UNSCANDAL.md "wikilink")**的名义）動畫《[校园迷糊大王](../Page/校园迷糊大王.md "wikilink")》片頭曲
    6.  ヒカリ（2006年5月24日）動畫《[犬神！](../Page/犬神！.md "wikilink")》片頭曲
    7.  Days（2007年5月2日）動畫《[藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")》片頭曲・片尾曲，獲得Oricon週間排名第8位。
    8.  恋する天気図（2007年8月17日）動畫《[藍蘭島漂流記](../Page/藍蘭島漂流記.md "wikilink")》片尾曲2
    9.  バニラソルト（2008年10月1日）動畫《[虎與龍](../Page/虎與龍.md "wikilink")》片尾曲
    10. silky
        heart（2009年1月28日）動畫《[虎與龍](../Page/虎與龍.md "wikilink")》片頭曲2，在2009年度oricon單曲年榜top500中，排名300。
    11. YAHHO\!\!（2009年8月26日）動畫《[加奈日記](../Page/加奈日記.md "wikilink")》片尾曲，在2009年度oricon單曲年榜top500中，排名468。
    12. インモラリスト（2011年2月2日）動畫《[龍之界點](../Page/龍之界點.md "wikilink")》片頭曲。
    13. PRESENTER（2011年5月25日）動畫《[DOG
        DAYS](../Page/DOG_DAYS.md "wikilink")》片尾曲。
    14. Coloring（2012年1月18日）動畫《要聽爸爸的話！》片尾曲。
    15. 夏の約束（2012年7月25日）動畫《DOG DAYS'》片尾曲。
    16. Golden
        Time（2013年11月13日）動畫《[青春紀行](../Page/青春紀行.md "wikilink")》片頭曲&片尾曲。
    17. The♡World's♡End（2014年3月12日）動畫《[青春紀行](../Page/青春紀行.md "wikilink")》片頭曲&片尾曲2。
    18. Stay With Me（2015年3月4日）動畫《DOG DAYS''》片尾曲。
    19. アシンメトリー（2015年11月4日）動畫《[K RETURN OF
        KINGS](../Page/K_RETURN_OF_KINGS.md "wikilink")》片頭曲。

### 專輯

  - **原創專輯**
    1.  （2000年12月21日）

    2.  （2001年11月29日）

    3.  sky（2003年7月24日）

    4.  （2004年4月28日）

    5.  （2005年11月23日）

    6.  Darling（2008年1月30日）

    7.  HONEY JET\!\!（2009年7月15日）

    8.  秘密（2012年2月22日）

    9.  ワールドエンドの庭（2015年1月7日）
  - **精選專輯**
      - （2003年3月26日）

      - BEST ALBUM（2012年9月20日）

### DVD

  - yui horie CLIPS 1（2004年4月28日）

  - （2006年7月26日）

  - （2008年6月4日）

  - yui horie CLIPS 2（2010年1月1日）

  - （2010年5月12日）

  - （2012年12月26日）

  - （2013年9月20日）

  - （2013年12月25日）

  - （2016年4月27日）

### [VHS](../Page/VHS.md "wikilink")

  - Yui Horie CLIPS 0 〜since'00〜'01〜（2002年7月29日）

## 相關條目

  - [Aice5](../Page/Aice5.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")

## 注釋

## 外部連結

  - [Cyber Little Metro
    Line](http://www.starchild.co.jp/artist/horie/)（[Starchild的堀江由衣公式網頁](../Page/Starchild.md "wikilink")）

  - [「Aice<sup>5</sup>」的公式網頁](http://www.starchild.co.jp/artist/aice5/)

  - [ナタリー - \[Power Push](http://natalie.mu/music/pp/horieyui)
    堀江由衣\]，[Comic
    Natalie的專訪](../Page/Comic_Natalie.md "wikilink")。

  - [堀江由衣希望在2013年結婚、長篇專訪全文完整刊載中！](http://ccsx.tw/2011/05/28/yui-horie-2013/)，Comic
    Natalie專訪的中文翻譯。

  - [堀江由衣 Official Channel](https://www.youtube.com/user/YuiHorieJP)

  -
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:日本廣播主持人](../Category/日本廣播主持人.md "wikilink")
[Category:聲優獎助演女優獎得主](../Category/聲優獎助演女優獎得主.md "wikilink")
[Category:堀江由衣](../Category/堀江由衣.md "wikilink")
[Category:Aice5](../Category/Aice5.md "wikilink")
[Category:VIMS所属声优](../Category/VIMS所属声优.md "wikilink")

1.
2.  [來源：聲優官方網](http://www.seigura.com/artist/detail/463&date=20070610062347)

3.
4.
5.
6.
7.