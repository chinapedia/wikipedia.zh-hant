**英国的大学**是根据[1988年教育改革法令在](../Page/1988年教育改革法令.md "wikilink")[英国创建的](../Page/英国.md "wikilink")[大学](../Page/大学.md "wikilink")，通常拥有[皇家特许状](../Page/皇家特许状.md "wikilink")、[教宗诏书](../Page/教宗诏书.md "wikilink")、[议会法案](../Page/议会法案.md "wikilink")。通常情况下，大学的设立在[英国枢密院备案](../Page/英国枢密院.md "wikilink")，只有这样的机构才能颁发学位。申请英国所有大学的[本科学位课程都要通过](../Page/本科.md "wikilink")[UCAS](../Page/UCAS.md "wikilink")。

## 分类

英国的大学可以分为大致六类:

  - [古典大学](../Page/古典大学.md "wikilink")：1800以前创办的七所大学。
  - [伦敦大学](../Page/伦敦大学.md "wikilink")，[杜伦大学以及其前附属学院](../Page/杜伦大学.md "wikilink")[纽卡斯尔大学](../Page/纽卡斯尔大学.md "wikilink")，前[威尔士大学的组成学院](../Page/威尔士大学.md "wikilink")。
  - [红砖大学](../Page/红砖大学.md "wikilink")：[第一次世界大战开始以前创建的大型民政大学](../Page/第一次世界大战.md "wikilink")。
  - [平板玻璃大学](../Page/平板玻璃大学.md "wikilink")：1966开始创建的大学（也称[罗宾斯报告扩展大学](../Page/罗宾斯报告.md "wikilink")）。
  - [英国公开大学](../Page/英国公开大学.md "wikilink")：英国的[远程教育大学](../Page/远程教育.md "wikilink")。
  - [新大学](../Page/新大学.md "wikilink")：一些[92后大学](../Page/92后大学.md "wikilink")，之前是理工学院。

英国大学的中央协调机构是 [Universities UK](../Page/Universities_UK.md "wikilink")。

## 缩写

和世界許多国家一样，英国大学的毕业生通常不但在名片上标明学术内经，也标出毕业大学的名字，通常写在括号中。例如，[谢菲尔德大学的毕业生可能自称为](../Page/谢菲尔德大学.md "wikilink")*John
Smith, BSc (Sheffield)*.

最古老的英国大学通常使用[拉丁文缩写表示](../Page/拉丁文.md "wikilink")，比如“Oxon”（[牛津](../Page/牛津大学.md "wikilink")）、“Cantab”（[剑桥](../Page/剑桥大学.md "wikilink")）和“Dunelm”（[杜伦](../Page/杜伦大学.md "wikilink")）都是十分常见的例子。有时候，[伦敦大学的毕业生也会使用拉丁文和英文缩写](../Page/伦敦大学.md "wikilink")'Lond'。有一些历史较短的大学也使用拉丁缩写，通常是因为他们位于主教辖区之内，使用主教的签名的拉丁缩写。

最常见的缩写有:

  - Aber (*Aberdonensis*) 代表[阿伯丁大学](../Page/阿伯丁大学.md "wikilink")
  - Cantab (*Cantabrigiensis*) 代表[剑桥大学](../Page/剑桥大学.md "wikilink")
  - Cantuar (*Cantuariensis*)
    代表[坎特伯雷大主教](../Page/坎特伯雷大主教.md "wikilink")，常用來指
    [蘭貝斯學位和](../Page/蘭貝斯學位.md "wikilink")[肯特大學](../Page/肯特大學.md "wikilink")
  - Cicest (*Cicestensis*) 代表[奇切斯特大學](../Page/奇切斯特大學.md "wikilink")
  - Dunelm (*Dunelmensis*) 代表[杜伦大学](../Page/杜伦大学.md "wikilink")
  - Edin (*Edinburgensis*) 代表[爱丁堡大学](../Page/爱丁堡大学.md "wikilink")
  - Exon (*Exoniensis*) 代表[埃克塞特大學](../Page/埃克塞特大學.md "wikilink")
  - Glas (*Glasguensis*) 代表[格拉斯哥大学](../Page/格拉斯哥大学.md "wikilink")
  - Lond (*Londiniensis*) 代表[伦敦大学](../Page/伦敦大学.md "wikilink")
  - Manc (*Mancuniensis*) 代表[曼彻斯特大学](../Page/曼彻斯特大学.md "wikilink")
  - Oxon or Oxf (*Oxoniensis*) 代表[牛津大学](../Page/牛津大学.md "wikilink")
  - St And (*Sancti Andreae*) 代表[圣安德鲁斯大学](../Page/圣安德鲁斯大学.md "wikilink")
  - Ebor (*Eboracensis*) 代表[约克大学](../Page/约克大学.md "wikilink")
  - Winton (*Wintonensis*) 代表[溫徹斯特大學](../Page/溫徹斯特大學.md "wikilink")

代表[威尔士大学的拉丁缩写](../Page/威尔士大学.md "wikilink")
(*Cambrensis*)通常会和代表[剑桥大学的英文缩写混淆](../Page/剑桥大学.md "wikilink").

2007年3月 ，[牛津大学发布公告](../Page/牛津大学.md "wikilink")，名为：“Oxford University
Calendar: Notes on
Style”，开始在出版物中推行新的缩写制度。缩写使用英文的第一个音节和后面一个字母，于是牛津和[剑桥就成了](../Page/剑桥大学.md "wikilink")“Oxf”和“Camb”。这项改革很有争议，(p. 2,
n. 1)但是被认为很有必要，文件同时提倡不要使用括号。

## 排名

2014年英国大学综合排名

| 排名  | 学校名称                                                                               |
| --- | ---------------------------------------------------------------------------------- |
| 1   | [剑桥大学](../Page/剑桥大学.md "wikilink")（Cambridge）                                      |
| 2   | [牛津大学](../Page/牛津大学.md "wikilink")（Oxford ）                                        |
| 3   | [伦敦政治经济学院](../Page/伦敦政治经济学院.md "wikilink")（LSE ）                                   |
| 4   | [圣安德鲁斯大学](../Page/圣安德鲁斯大学.md "wikilink")（St Andrews）                               |
| 5   | [帝国理工学院](../Page/帝国理工学院.md "wikilink")（Imperial）                                   |
| 6   | [杜伦大学](../Page/杜伦大学.md "wikilink")（Durham）                                         |
| 7   | [巴斯大学](../Page/巴斯大学.md "wikilink")（Bath）                                           |
| 8   | [埃克塞特大学](../Page/埃克塞特大学.md "wikilink")（Exeter）                                     |
| 9   | [伦敦大学学院](../Page/伦敦大学学院.md "wikilink")（UCL）                                        |
| 10  | [华威大学](../Page/华威大学.md "wikilink")（Warwick）                                        |
| 11  | [约克大学](../Page/约克大学.md "wikilink")（York）                                           |
| 12  | [兰卡斯特大学](../Page/兰卡斯特大学.md "wikilink")（Lancaster）                                  |
| 13  | [萨里大学](../Page/萨里大学.md "wikilink")（Surrey）                                         |
| 14  | [莱斯特大学](../Page/莱斯特大学.md "wikilink")（Leicester）                                    |
| 15  | [布里斯托大学](../Page/布里斯托大学.md "wikilink")（Bristol）                                    |
| 16  | [伯明翰大学](../Page/伯明翰大学.md "wikilink")（Birmingham）                                   |
| 17  | [東英吉利亞大學](../Page/東英吉利亞大學.md "wikilink")（UEA）                                      |
| 18  | [纽卡斯尔大学](../Page/纽卡斯尔大学.md "wikilink")（Newcastle）                                  |
| 19  | [谢菲尔德大学](../Page/谢菲尔德大学.md "wikilink")（Sheffield）                                  |
| 20  | [南安普顿大学](../Page/南安普顿大学.md "wikilink")（Southampton）                                |
| 21  | [拉夫堡大学](../Page/拉夫堡大学.md "wikilink")（Loughborough）                                 |
| 22  | [爱丁堡大学](../Page/爱丁堡大学.md "wikilink")（Edinburgh）                                    |
| 23  | [诺丁汉大学](../Page/诺丁汉大学.md "wikilink")（Nottingham）                                   |
| 24  | [伦敦大学亚非学院](../Page/伦敦大学亚非学院.md "wikilink")（School of Oriental and African Studies） |
| 25  | [格拉斯哥大学](../Page/格拉斯哥大学.md "wikilink")（Glasgow）                                    |
| 26  | [曼彻斯特大学](../Page/曼彻斯特大学.md "wikilink")（Manchester）                                 |
| 27  | [伦敦大学国王学院](../Page/伦敦大学国王学院.md "wikilink")（King's College）                         |
| 28  | [倫敦大學皇家賀洛唯學院](../Page/皇家哈洛威學院.md "wikilink")（RHUL）                                 |
| 29  | [阿斯顿大学](../Page/阿斯顿大学.md "wikilink")（Aston）                                        |
| 29  | [利兹大学](../Page/利兹大学.md "wikilink")（Leeds）                                          |
| 29  | [贝尔法斯特女王大学](../Page/贝尔法斯特女王大学.md "wikilink")（Queen’s）                              |
| 32  | [薩塞克斯大學](../Page/薩塞克斯大學.md "wikilink")（Sussex）                                     |
| 33  | [卡迪夫大学](../Page/卡迪夫大学.md "wikilink")（Cardiff）                                      |
| 33  | [肯特大学](../Page/肯特大学.md "wikilink")（Kent）                                           |
| 35  | [雷丁大学](../Page/雷丁大学.md "wikilink")（Reading ）                                       |
| 36  | [利物浦大学](../Page/利物浦大学.md "wikilink")（Liverpool）                                    |
| 37  | [伦敦玛丽王后大学](../Page/伦敦玛丽王后大学.md "wikilink")（Queen Mary）                             |
| 38  | [赫瑞瓦特大学](../Page/赫瑞瓦特大学.md "wikilink")（Heriot-Watt）                                |
| 39  | [埃塞克斯大学](../Page/埃塞克斯大学.md "wikilink")（Essex）                                      |
| 40  | [阿伯丁大学](../Page/阿伯丁大学.md "wikilink")（Aberdeen）                                     |
| 41  | [白金汉大学](../Page/白金汉大学.md "wikilink")（Buckingham）                                   |
| 42  | [斯特拉思克莱德大学](../Page/斯特拉思克莱德大学.md "wikilink")（Strathclyde）                          |
| 43  | [倫敦大學城市學院](../Page/倫敦大學城市學院.md "wikilink")（City）                                   |
| 44  | [基尔大学](../Page/基尔大学.md "wikilink")（Keele）                                          |
| 45  | [考文垂大学](../Page/考文垂大学.md "wikilink")（Coventry）                                     |
| 46  | [布鲁内尔大学](../Page/布鲁内尔大学.md "wikilink")（Brunel）                                     |
| 47  | [斯旺西大学](../Page/斯旺西大学.md "wikilink")（Swansea）                                      |
| 48  | [伦敦大学金匠學院](../Page/伦敦大学金匠學院.md "wikilink")（Goldsmiths）                             |
| 49  | [邓迪大学](../Page/邓迪大学.md "wikilink")（Dundee）                                         |
| 50  | [牛津布鲁克斯大学](../Page/牛津布鲁克斯大学.md "wikilink")（Oxford Brookes）                         |
| 51  | [斯特靈大學](../Page/斯特靈大學.md "wikilink")（Stirling）                                     |
| 52  | [伯恩茅斯藝術大學](../Page/伯恩茅斯藝術大學.md "wikilink")（Arts University Bournemouth）            |
| 52  | [切斯特大学](../Page/切斯特大学.md "wikilink")（Chester）                                      |
| 52  | [罗伯特戈登大学](../Page/罗伯特戈登大学.md "wikilink")（Robert Gordon）                            |
| 55  | [朴茨茅斯大学](../Page/朴茨茅斯大学.md "wikilink")（Portsmouth）                                 |
| 56  | [班戈大学](../Page/班戈大学.md "wikilink")（Bangor）                                         |
| 57  | [林肯大学](../Page/林肯大学.md "wikilink")（Lincoln）                                        |
| 57  | [溫徹斯特大學](../Page/溫徹斯特大學.md "wikilink")（Winchester）                                 |
| 59  | [北安普顿大学](../Page/北安普顿大学.md "wikilink")（Northampton）                                |
| 60  | [西英格兰大学](../Page/西英格兰大学.md "wikilink")（UWE）                                        |
| 61  | [诺丁汉特伦特大学](../Page/诺丁汉特伦特大学.md "wikilink")（NTU）                                    |
| 62  | [諾桑比亞大學](../Page/諾桑比亞大學.md "wikilink")（Northumbria）                                |
| 63  | [赫尔大学](../Page/赫尔大学.md "wikilink")（Hull）                                           |
| 64  | [哈珀亞當斯大學](../Page/哈珀亞當斯大學.md "wikilink")（Harper Adams）                             |
| 64  | [约克圣约翰大学](../Page/约克圣约翰大学.md "wikilink")（York St John）                             |
| 66  | [哈德斯菲尔德大学](../Page/哈德斯菲尔德大学.md "wikilink")（Huddersfield）                           |
| 67  | [伯恩茅斯大學](../Page/伯恩茅斯大學.md "wikilink")（Bournemouth）                                |
| 68  | [奇切斯特大学](../Page/奇切斯特大学.md "wikilink")（Chichester）                                 |
| 69  | [边山大学](../Page/边山大学.md "wikilink")（Edge Hill）                                      |
| 70  | [巴斯思巴大學](../Page/巴斯思巴大學.md "wikilink")（Bath Spa）                                   |
| 71  | [瑪格麗特皇后大學](../Page/瑪格麗特皇后大學.md "wikilink")（Queen Margaret Edinburgh）               |
| 71  | [聖馬可和聖約翰大學](../Page/聖馬可和聖約翰大學.md "wikilink")（St Mark & St John, Plymouth）          |
| 73  | [紐曼大學](../Page/紐曼大學_\(伯明翰\).md "wikilink")（Newman）                                 |
| 73  | [普利茅斯大学](../Page/普利茅斯大学.md "wikilink")（Plymouth）                                   |
| 73  | [阿尔斯特大学](../Page/阿尔斯特大学.md "wikilink")（Ulster）                                     |
| 76  | [布莱顿大学](../Page/布莱顿大学.md "wikilink")（Brighton）                                     |
| 77  | [伦敦艺术大学](../Page/伦敦艺术大学.md "wikilink")（Arts London）                                |
| 77  | [法爾茅斯大學](../Page/法爾茅斯大學.md "wikilink")（Falmouth）                                   |
| 77  | [谢菲尔德哈勒姆大学](../Page/谢菲尔德哈勒姆大学.md "wikilink")（Sheffield Hallam）                     |
| 80  | [罗汉普顿大学](../Page/罗汉普顿大学.md "wikilink")（Roehampton）                                 |
| 81  | [格拉斯哥卡利多尼安大學](../Page/格拉斯哥卡利多尼安大學.md "wikilink")（Glasgow Caledonian）               |
| 82  | [亞伯大學](../Page/亞伯大學.md "wikilink")（Aberystwyth）                                    |
| 83  | [利物浦約翰摩爾斯大學](../Page/利物浦約翰摩爾斯大學.md "wikilink")（LJMU）                               |
| 84  | [布拉德福德大学](../Page/布拉德福德大学.md "wikilink")（Bradford）                                 |
| 84  | [德比大学](../Page/德比大学.md "wikilink")（Derby ）                                         |
| 86  | [德蒙福特大學](../Page/德蒙福特大學.md "wikilink")（De Montfort）                                |
| 87  | [卡迪夫都會大學](../Page/卡迪夫都會大學.md "wikilink")（Cardiff Metropolitan）                     |
| 88  | [中央兰开夏大学](../Page/中央兰开夏大学.md "wikilink")（Central Lancashire）                       |
| 89  | [曼彻斯特城市大学](../Page/曼彻斯特城市大学.md "wikilink")（Manchester Metropolitan）                |
| 90  | [坎特伯雷基督教會大學](../Page/坎特伯雷基督教會大學.md "wikilink")（Canterbury Christ Church）           |
| 91  | [伯明翰城市大学](../Page/伯明翰城市大学.md "wikilink")（BCU）                                      |
| 91  | [格羅斯特郡大學](../Page/格羅斯特郡大學.md "wikilink")（Gloucestershire）                          |
| 93  | [提賽德大學](../Page/提賽德大學.md "wikilink")（Teesside）                                     |
| 94  | [密德薩斯大學](../Page/密德薩斯大學.md "wikilink")（Middlesex）                                  |
| 95  | [坎布里亞大學](../Page/坎布里亞大學.md "wikilink")（Cumbria）                                    |
| 96  | [赫特福德大學](../Page/赫特福德大學.md "wikilink")（Hertfordshire）                              |
| 96  | [桑德兰大学](../Page/桑德兰大学.md "wikilink")（Sunderland）                                   |
| 98  | [索尔福德大学](../Page/索尔福德大学.md "wikilink")（Salford）                                    |
| 99  | [创意艺术大学](../Page/创意艺术大学.md "wikilink")（Creative Arts）                              |
| 100 | [龙比亚大学](../Page/龙比亚大学.md "wikilink")（Edinburgh Napier）                             |
| 101 | [格林威治大学](../Page/格林威治大学.md "wikilink")（Greenwich）                                  |
| 102 | [伍斯特大学](../Page/伍斯特大学.md "wikilink")（Worcester）                                    |
| 103 | [利茲都會大學](../Page/利茲都會大學.md "wikilink")（Leeds Metropolitan）                         |
| 104 | [利兹三一大學](../Page/利兹三一大學.md "wikilink")（Leeds Trinity）                              |
| 105 | [亞伯泰丹地大學](../Page/亞伯泰丹地大學.md "wikilink")（Abertay Dundee）                           |
| 106 | [格羅斯泰特主教大學](../Page/格羅斯泰特主教大學.md "wikilink")（Bishop Grosseteste）                   |
| 106 | [威斯敏斯特大学](../Page/威斯敏斯特大学.md "wikilink")（Westminster）                              |
| 108 | [史丹福郡大學](../Page/史丹福郡大學.md "wikilink")（Staffordshire）                              |
| 109 | [格林多大学](../Page/格林多大学.md "wikilink")（Glyndwr）                                      |
| 110 | [安格利亚鲁斯金大学](../Page/安格利亚鲁斯金大学.md "wikilink")（Anglia Ruskin）                        |
| 111 | [金斯顿大学](../Page/金斯顿大学.md "wikilink")（Kingston）                                     |
| 112 | [西伦敦大学](../Page/西伦敦大学.md "wikilink")（West London）                                  |
| 113 | [白金漢郡新大學](../Page/白金漢郡新大學.md "wikilink")（Buckinghamshire New）                      |
| 114 | [南安普顿索伦特大学](../Page/南安普顿索伦特大学.md "wikilink")（Southampton Solent）                   |
| 115 | [贝德福德大学](../Page/贝德福德大学.md "wikilink")（Bedfordshire）                               |
| 116 | [高地和群岛大学](../Page/高地和群岛大学.md "wikilink")（Highlands and Islands）                    |
| 117 | [西苏格兰大学](../Page/西苏格兰大学.md "wikilink")（West of Scotland）                           |
| 118 | [伦敦南岸大学](../Page/伦敦南岸大学.md "wikilink")（London South Bank）                          |
| 119 | [博爾頓大學](../Page/博爾頓大學.md "wikilink")（Bolton）                                       |
| 120 | [东伦敦大学](../Page/东伦敦大学.md "wikilink")（UEL）                                          |
| 121 | [倫敦都會大學](../Page/倫敦都會大學.md "wikilink")（London Metropolitan）                        |

## 参考文献

## 外部链接

  -
  -
  - [Guardian Special Report - UK Higher
    Education](http://www.guardian.co.uk/education/higher-education)

## 参见

  - [GuildHE](../Page/GuildHE.md "wikilink")
  - [英国教育](../Page/英国教育.md "wikilink")
  - [英国大学学院](../Page/英国大学学院.md "wikilink")
  - [爱尔兰大学](../Page/爱尔兰大学.md "wikilink")
  - [苏格兰古典大学](../Page/苏格兰古典大学.md "wikilink")

{{-}}

[UK](../Category/各国大学.md "wikilink")
[英国大学](../Category/英国大学.md "wikilink")