**伊達秀宗**（）是[江戶時代初期的](../Page/江戶時代.md "wikilink")[大名](../Page/大名.md "wikilink")，[伊予國](../Page/伊予國.md "wikilink")[宇和島藩的初代藩主](../Page/宇和島藩.md "wikilink")。他是[伊達政宗的長男](../Page/伊達政宗.md "wikilink")（[庶長子](../Page/庶出子女.md "wikilink")），母親是側室貓御前（飯坂氏）。正室是[井伊直政女京姬](../Page/井伊直政.md "wikilink")，其他還有幾位側室，子女分別有[伊達宗實](../Page/伊達宗實.md "wikilink")（長男）、[伊達宗時](../Page/伊達宗時.md "wikilink")（次男）、[伊達宗利](../Page/伊達宗利.md "wikilink")（三男）、[桑折宗臣](../Page/桑折宗臣.md "wikilink")（四男）、[伊達宗純](../Page/伊達宗純.md "wikilink")（五男）、伊達宗職（七男）、伊達宗則（九男）、女兒（渡邊某室）。幼名兵五郎，官位是從四位下、侍從、[遠江守](../Page/遠江.md "wikilink")。

## 經歷

生於陸奧國，1596年（[慶長元年](../Page/慶長.md "wikilink")）時，在[豐臣秀吉處元服](../Page/豐臣秀吉.md "wikilink")，得到秀吉下賜一字，命名秀宗。此時因為秀吉的關係，敘任為從五位下侍從，並且成為[豐臣秀賴身邊的小姓](../Page/豐臣秀賴.md "wikilink")。秀吉死後的1600年（慶長5年），[五奉行之一的](../Page/五奉行.md "wikilink")[石田三成與](../Page/石田三成.md "wikilink")[五大老之一的](../Page/五大老.md "wikilink")[德川家康對立](../Page/德川家康.md "wikilink")，秀宗被留在三成方面的[宇喜多秀家府邸當做人質](../Page/宇喜多秀家.md "wikilink")。

雖然他是政宗長男，卻因生母飯坂氏是側室，因此不能繼承本家家督之位。1614年（[慶長](../Page/慶長.md "wikilink")19年）的[大坂冬之陣與父親政宗一同參戰](../Page/大坂冬之陣.md "wikilink")，戰後因為建有戰功，德川家康授予政宗伊予宇和島十萬石，而秀宗就繼承這個領地，成為[宇和島藩的初代藩主](../Page/宇和島藩.md "wikilink")，對藩政投入相當大的心力。1657年([明曆](../Page/明曆.md "wikilink")3年)，五男[伊達宗純又分出伊予](../Page/伊達宗純.md "wikilink")[吉田藩](../Page/宇和島藩#支藩.md "wikilink")。1658年過世，享年68歲。

法名是等覺寺殿前遠州太守拾遺義山常信大居士，墓在[愛媛縣](../Page/愛媛縣.md "wikilink")[宇和島市野川的等覺寺](../Page/宇和島市.md "wikilink")，以及[東京都](../Page/東京都.md "wikilink")[港區高輪的佛日山東禪寺](../Page/港區_\(東京都\).md "wikilink")

### 官職位階履歷

※日期＝舊曆

  - 1596年（文祿5）5月9日，元服。由豐臣秀吉之名下賜一字，命名秀宗，敘任從五位下侍從。
  - 1614年（慶長19）12月25日，成為伊予國宇和島十萬石藩主。
  - 1622年（元和8） 12月，兼任遠江守。
  - 1626年（寬永3） 8月19日，昇為從四位下。
  - 1657年（明曆3） 7月21日，隱居。

## 相關條目

  - [伊達氏](../Page/伊達氏.md "wikilink")
  - [宇和島藩](../Page/宇和島藩.md "wikilink")
  - [和靈騷動](../Page/和靈騷動.md "wikilink")
  - [伊達輝宗](../Page/伊達輝宗.md "wikilink")
  - [伊達政宗](../Page/伊達政宗.md "wikilink")
  - [伊達忠宗](../Page/伊達忠宗.md "wikilink")

[Hidemune](../Category/宇和島伊達氏.md "wikilink")
[Hidemune](../Category/豐臣氏.md "wikilink")
[Category:陸奧國出身人物](../Category/陸奧國出身人物.md "wikilink")
[Category:宇和島藩](../Category/宇和島藩.md "wikilink")
[Category:外樣大名](../Category/外樣大名.md "wikilink")