**乳交**（mammary
intercourse），或称**半身性交**，是一种[前戏或者](../Page/前戏.md "wikilink")[非插入式性行为的](../Page/非插入式性行为.md "wikilink")[性動作](../Page/人类性行为.md "wikilink")，由女性[乳房與男性](../Page/乳房.md "wikilink")[阴茎之間的接觸而幫助其性興奮](../Page/阴茎.md "wikilink")。\[1\]

## 方式

[Wiki-mam-intcs.png](https://zh.wikipedia.org/wiki/File:Wiki-mam-intcs.png "fig:Wiki-mam-intcs.png")
乳交涉及将男性[勃起的](../Page/勃起.md "wikilink")[阴茎放入](../Page/阴茎.md "wikilink")[乳沟中](../Page/乳沟.md "wikilink")，而乳房围绕阴茎周围挤压，从而刺激阴茎。\[2\]

说，乳交时，乳房敏感的女性将体验[性高潮](../Page/性高潮.md "wikilink")。\[3\]乳交是九種可取代[插入式性行为的性行为中的一種](../Page/插入式性行为.md "wikilink")，\[4\]可以与[阴茎口交共同进行](../Page/阴茎口交.md "wikilink")。女性可以将乳房上下推动，從而增加對阴茎的刺激程度。

由于乳交是非插入式性行為，故此感染[性传播疾病的风险比較低](../Page/性传播疾病.md "wikilink")，这是因为性传播疾病是通过[黏膜与](../Page/黏膜.md "wikilink")[尿道球腺液或者](../Page/尿道球腺液.md "wikilink")[精液的直接接触而传染](../Page/精液.md "wikilink")。[人类免疫缺陷病毒也需要直接接触](../Page/人类免疫缺陷病毒.md "wikilink")，因此不太可能通过乳交传播。\[5\]关于[新西兰](../Page/新西兰.md "wikilink")[性工作者的](../Page/性工作者.md "wikilink")[避孕套使用习惯的调查显示](../Page/避孕套.md "wikilink")，对于拒绝使用避孕套的顾客，性工作者则提供各种[安全性行为替代](../Page/安全性行为.md "wikilink")[阴道性交](../Page/性交.md "wikilink")。一名性工作者表示，乳交也是其中一种替代方式；而擁有[巨乳的女性與男性乳交](../Page/巨乳.md "wikilink")，感覺允同插入式性行為。\[6\]

乳交有时候被认为是一种[变态行为](../Page/性偏離.md "wikilink")。\[7\]然而，[弗洛伊德认为](../Page/西格蒙德·弗洛伊德.md "wikilink")，这些性兴趣的扩展介于正常范围，除非是坚持只进行乳交，拒绝其他性接触的方式。\[8\]

## 俚语

在美国，乳交被称为、或；而在英国被称为或\[9\]——后者可追溯到1930年代；更诙谐的说法是“乳房小巷之旅”（）。\[10\]

日本[性产业使用](../Page/性产业.md "wikilink")**Paizuri**（）一词；“ *pai*”指“
*oppai*”（乳房的俚语），而“ *zuri*”指“ *zuri*”（摩擦）。\[11\]\[12\]

## 参见

## 注脚

## 参考资料

  -
  -
  -
  -
  -
  -
## 外部链接

  - [乳交](https://web.archive.org/web/20130923012937/http://www.sexinfo101.com/intermammary_intercourse.shtml)

[Category:性姿勢](../Category/性姿勢.md "wikilink")
[Category:乳房](../Category/乳房.md "wikilink")
[Category:性行为](../Category/性行为.md "wikilink")
[Category:性俗語](../Category/性俗語.md "wikilink")
[Category:性取向](../Category/性取向.md "wikilink")

1.  Alex Comfort, *The Joy of Sex* (1972) p. 67-9

2.
3.  Alex Comfort, *The Joy of Sex* (1972) p. 69 and p. 175

4.  Alex Comfort, *The Joy of Sex* (1972) p. 176

5.

6.  Woods, 1996, in Davis, pages 125-127

7.  Clifford Allen, *A Textbook of Psychosexual Disorders* (1969) p. 200

8.  Sigmund Freud, *On Sexuality* (PFL 7) p. 65 and p. 75

9.  Godson, page 96.

10. M. S. Morton, *The Lover's Tongue* (2003) p. 187

11. Bacarr, 2004, p. 150.

12. Constantine, 1992, p. 110.