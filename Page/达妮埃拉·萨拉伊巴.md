**达妮埃拉·萨拉伊巴·费尔南德斯**（，）是一位[巴西](../Page/巴西.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")，出生於[里約熱內盧](../Page/里約熱內盧.md "wikilink")。她的母親Mara
Lúcia
Sarahyba也是一位模特兒\[1\]。她在12歲起開始擔任模特兒至今\[2\]。目前是[H\&M與](../Page/H&M.md "wikilink")[班尼頓等公司的簽約模特兒](../Page/班尼頓.md "wikilink")。

## 參考來源

## 外部連結

  - [Daniella Sarahyba official
    site](https://web.archive.org/web/20050311193613/http://www.daniellasarahyba.com.br/)

  - [Daniella Sarahyba at
    SI.com](http://sportsillustrated.cnn.com/swimsuit/collection/models/daniella_sarahyba.html)

[Category:巴西女性模特兒](../Category/巴西女性模特兒.md "wikilink")
[Category:西班牙裔巴西人](../Category/西班牙裔巴西人.md "wikilink")
[Category:黎巴嫩裔巴西人](../Category/黎巴嫩裔巴西人.md "wikilink")
[Category:里約熱內盧人](../Category/里約熱內盧人.md "wikilink")

1.   [Daniella faz a América - IstoÉ
    Gente](http://www.terra.com.br/istoegente/310/reportagens/daniella_sarahyba_01.htm)
    (retrieved on November 5, 2007)
2.   [Daniella Sarahyba, una chica muy guapa - Revista
    Quem](http://quem.globo.com/edic/20011214/rep_daniela.htm)
    (retrieved on November 5, 2007)