[RyoanJi-Sanmon.jpg](https://zh.wikipedia.org/wiki/File:RyoanJi-Sanmon.jpg "fig:RyoanJi-Sanmon.jpg")
**龍安寺**（）是位於[日本](../Page/日本.md "wikilink")[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區的](../Page/右京區.md "wikilink")[臨濟宗](../Page/臨濟宗.md "wikilink")[妙心寺派的寺院](../Page/妙心寺.md "wikilink")。以石庭而聞名。[山號為大雲山](../Page/山號.md "wikilink")。龍安寺創建于[寶德](../Page/寶德.md "wikilink")2年（1450年）。本尊為[釋迦如來](../Page/釋迦如來.md "wikilink")、創立者為[細川勝元](../Page/細川勝元.md "wikilink")、開山（初代[住持](../Page/住持.md "wikilink")）為[義天玄承](../Page/義天玄承.md "wikilink")。以[古都京都的文化財一部份而列入了](../Page/古都京都的文化財.md "wikilink")[世界遺產](../Page/世界遺產.md "wikilink")。1975年[英女王](../Page/英國.md "wikilink")[伊麗莎白二世訪問日本時](../Page/伊麗莎白二世.md "wikilink")，曾表示希望能參觀龍安寺內的庭園，參觀后英女王對庭園贊不絕口，也讓龍安寺庭園在世界范圍內名聲大噪。有專家研究，園中有隱含的結構，是用來吸引觀眾的無意識視覺敏感度\[1\]。

龍安寺的石庭是日本最有名的[枯山水園林精品](../Page/枯山水.md "wikilink")。

## 連接交通

乘搭[京都市營公車](../Page/京都市營公車.md "wikilink")、[京都公車](../Page/京都公車.md "wikilink")、[JR公車可在](../Page/西日本JR巴士.md "wikilink")「龍安寺前」下車。從[京福電鐵](../Page/京福電鐵.md "wikilink")[龍安寺站下車徒步只需](../Page/龍安寺站.md "wikilink")7分就可到達。

## 注釋

## 參看

  - [日本寺院列表](../Page/日本寺院列表.md "wikilink")
  - [真田信繁](../Page/真田信繁.md "wikilink")
  - [足利義材](../Page/足利義材.md "wikilink")

## 外部連結

  - [龍安寺官方網址](http://www.ryoanji.jp/)

[Category:日本世界遺產](../Category/日本世界遺產.md "wikilink")
[Category:京都市佛寺](../Category/京都市佛寺.md "wikilink")
[Category:臨濟宗妙心寺派寺院](../Category/臨濟宗妙心寺派寺院.md "wikilink")
[Category:京都市重要文化財](../Category/京都市重要文化財.md "wikilink")
[Category:右京區](../Category/右京區.md "wikilink")

1.