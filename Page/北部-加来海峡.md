**北部-加来海峡**（）是[法国北部一個已不存在的](../Page/法国.md "wikilink")[大區](../Page/大區.md "wikilink")，北與[比利時接壤](../Page/比利時.md "wikilink")。面积12,413km²。下轄[諾爾省](../Page/諾爾省.md "wikilink")（59）、[加來海峽省](../Page/加來海峽省.md "wikilink")（62）。其首府是[里尔](../Page/里尔.md "wikilink")。北部-加来海峡位在[皮卡第的北邊](../Page/皮卡第.md "wikilink")，其北邊和東邊和[比利時相連](../Page/比利時.md "wikilink")，北邊是[英吉利海峽](../Page/英吉利海峽.md "wikilink")，西北邊是[北海](../Page/北海_\(大西洋\).md "wikilink")。北部-加来海峡區域的大部份曾是[低地国家中](../Page/低地国家.md "wikilink")[南尼德蘭的一部份](../Page/南尼德蘭.md "wikilink")，在[神圣罗马帝国中](../Page/神圣罗马帝国.md "wikilink")[勃艮第公國的統治下](../Page/勃艮第公國.md "wikilink")，後來先後被西班牙及奧地利統治。在1477年至1678年之間漸漸的成為法國的一部份，尤其是在[路易十四在位的時期](../Page/路易十四.md "wikilink")。歷史上在北部-加来海峡出現前，這個地區的[行省為](../Page/行省_\(法国\).md "wikilink")[阿圖瓦](../Page/阿圖瓦.md "wikilink")、[法國佛蘭德斯地區及](../Page/法國佛蘭德斯地區.md "wikilink")，也有一部份屬於[皮卡第](../Page/皮卡第.md "wikilink")，當時的居民仍常常使用這些以前的名稱。

北部-加来海峡的面積為12,414 km<sup>2</sup>，但其人口密度相當高，人口密度為每平方公里330.8人，總人口410萬人，約佔法國人口的7%，是全法國人口第四多的地區，其中有83%住在城市中。其行政中心和最大城市是[里尔](../Page/里尔.md "wikilink")，第二大城市是[加來](../Page/加來.md "wikilink")，是歐陸重要的經濟及交通中心，離英吉利海峽另一側，[英國的](../Page/英國.md "wikilink")[多佛尔只有](../Page/多佛尔.md "wikilink")，在晴天時還可看到[多佛白色懸崖](../Page/多佛白色懸崖.md "wikilink")，其他的主要市鎮有[瓦朗謝訥](../Page/瓦朗謝訥.md "wikilink")、[朗斯](../Page/朗斯.md "wikilink")、[杜埃](../Page/杜埃.md "wikilink")、[贝蒂讷](../Page/贝蒂讷.md "wikilink")、[敦克爾克](../Page/敦克爾克.md "wikilink")、[莫伯日](../Page/莫伯日.md "wikilink")、[濱海布洛涅](../Page/濱海布洛涅.md "wikilink")、[阿拉斯](../Page/阿拉斯.md "wikilink")、[康布雷及](../Page/康布雷.md "wikilink")[圣奥梅尔](../Page/圣奥梅尔.md "wikilink")。

## 名稱

其名稱Nord-Pas-de-Calais是結合了[諾爾省](../Page/諾爾省.md "wikilink")（Nord）及[加來海峽省](../Page/加來海峽省.md "wikilink")（Pas-de-Calais），不過区域市政局將其名稱拼為Nord-Pas
de Calais\[1\]。

此地區的北部曾經是[佛蘭德伯國的一部份](../Page/佛蘭德伯國.md "wikilink")，該國首都是[杜埃](../Page/杜埃.md "wikilink")，有些人
希望反映此區域和比利時及荷蘭的歷史關係，因此稱此地為「法屬低地國」（法語：*Pays-Bas français*，荷語：*Franse
Nederlanden*或*Franse Lage Landen*），其他的名稱有*Région
Flandre(s)-Artois*、*Hauts-de-France*（上法國）及*Picardie-du-Nord*（北[皮卡第](../Page/皮卡第.md "wikilink")）。已有許多請願書提出希望更改的名稱、其影響及其重要性。

## 歷史

[Lianefleuve.JPG](https://zh.wikipedia.org/wiki/File:Lianefleuve.JPG "fig:Lianefleuve.JPG")的Liane河\]\]

此地在史前時代就有人居住，一直是歐洲的戰略要地之一（因此也是有過最多戰爭的地方之一）。出生在[里爾的法國總統](../Page/里爾.md "wikilink")[夏爾·戴高樂曾因入侵法國的軍隊都會經過此地](../Page/夏爾·戴高樂.md "wikilink")，因此稱此地為「致命大道」。幾個世紀以來，有許多國家或民族曾佔領此地，像是[贝尔盖人](../Page/贝尔盖人.md "wikilink")、[羅馬人](../Page/古羅馬.md "wikilink")、日耳曼的[法蘭克人](../Page/法蘭克人.md "wikilink")、英國、[西屬尼德蘭](../Page/西屬尼德蘭.md "wikilink")、[奧屬尼德蘭及](../Page/奧屬尼德蘭.md "wikilink")[荷蘭共和國](../Page/荷蘭共和國.md "wikilink")。18世紀初此地被法國統治，但在第一次及第二次的世紀大戰中，此區域的部份地區都被德軍占領。

在第九世紀時，大部份里爾的居民說的是中荷蘭的方言，不過此區域南部的居民則說[罗曼语族的方言](../Page/罗曼语族.md "wikilink")，直到如今，在地名的命名上仍可以看出上述的語言邊界。自第九世紀起，語言邊界慢慢的往北及往東移動，在十三世紀末，語言邊界移到利斯河（river
Lys）以南，Cap-Griz-Nez以東\[2\]。
[Cap_Blanc_Nez_winter.jpg](https://zh.wikipedia.org/wiki/File:Cap_Blanc_Nez_winter.jpg "fig:Cap_Blanc_Nez_winter.jpg")

中古時期時，加來海峽地區是由及[阿圖瓦伯國組成](../Page/阿图瓦.md "wikilink")，而諾爾（Nord）主要是由[佛兰德伯国及](../Page/佛兰德伯国.md "wikilink")的南部組成。布洛涅、阿圖瓦及法蘭德斯是法國王室的封地，埃諾屬於[神聖羅馬帝國](../Page/神聖羅馬帝國.md "wikilink")。[加来從](../Page/加来.md "wikilink")1347年到1558年都是英國的屬地，一直到1558年才變為法國的領土。在15世紀時，除了加来之外，此地區其他部份都是受[勃艮第统治者統治](../Page/勃艮第统治者列表.md "wikilink")，他還統治其他法國北部、以及現在[比利時](../Page/比利時.md "wikilink")、[盧森堡及](../Page/盧森堡.md "wikilink")[荷蘭的部份地區](../Page/荷蘭.md "wikilink")。在1477年勃艮第公爵[勇士查理死後](../Page/勇士查理.md "wikilink")，布洛涅及阿圖瓦成為法國的屬地，而法蘭德斯及埃諾由他的女兒[瑪麗繼承](../Page/勃艮第的瑪麗.md "wikilink")。不久之後的1492年，阿圖瓦被割讓給瑪麗的兒子[腓力一世](../Page/腓力一世_\(卡斯蒂利亞\).md "wikilink")，部份原因是希望他父親[神聖羅馬帝國的](../Page/神聖羅馬帝國.md "wikilink")[馬克西米利安一世可以在法皇](../Page/馬克西米利安一世_\(神聖羅馬帝國\).md "wikilink")[查理八世入侵義大利時表示中立](../Page/查理八世_\(法兰西\).md "wikilink")。

[Douai_-_Vue_à_partir_du_Beffroi_(06).JPG](https://zh.wikipedia.org/wiki/File:Douai_-_Vue_à_partir_du_Beffroi_\(06\).JPG "fig:Douai_-_Vue_à_partir_du_Beffroi_(06).JPG")\]\]
因為瑪麗和[哈布斯堡王朝的結婚](../Page/哈布斯堡王朝.md "wikilink")，大部份現今是北部-加来海峡大區的領土都已成為勃艮第公爵的屬地，在腓力的兒子[查理五世即位時](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")，此地區是尼德蘭的[十七省之一](../Page/十七省.md "wikilink")，後來又繼承給[腓力二世](../Page/腓力二世_\(西班牙\).md "wikilink")。在[意大利战争時法國和西班牙在此處有許多的衝突](../Page/意大利战争.md "wikilink")，但1566年[尼德蘭反抗西班牙時](../Page/八十年戰爭.md "wikilink")，這些現在稱為北部-加来海峡的地區是最忠於西班牙皇室的，也是[帕爾馬公爵使尼德蘭南部回到西班牙掌控的重要基礎](../Page/帕爾馬公爵.md "wikilink")。這也是[法国宗教战争時](../Page/法国宗教战争.md "wikilink")，西班牙支持法國的原因。
[TourbièreVredGroupe3Roselière.jpg](https://zh.wikipedia.org/wiki/File:TourbièreVredGroupe3Roselière.jpg "fig:TourbièreVredGroupe3Roselière.jpg")
在十七世紀法國及西班牙的戰爭時（[1635](../Page/三十年戰爭.md "wikilink")–[1659](../Page/法西战争_\(1635年\).md "wikilink")、[1667–1668](../Page/產權轉移戰爭.md "wikilink")、[1672–1678](../Page/法荷战争.md "wikilink")、[1688–1697](../Page/大同盟戰爭.md "wikilink")），此地區是爭議的爭點之一，也漸漸的成為法國的屬地。在1659年開始阿圖瓦吞併，之後現今稱為諾爾的地區都在1678年[奈梅亨條約時確定](../Page/奈梅亨條約.md "wikilink")，邊界在1697年在[賴斯韋克條約時變的更加明確](../Page/賴斯韋克條約.md "wikilink")。

這些地區以往曾被分為法國的[佛蘭德斯](../Page/法國佛蘭德斯地區.md "wikilink")、[阿圖瓦及](../Page/阿圖瓦.md "wikilink")[皮卡第](../Page/皮卡第.md "wikilink")，在1789年[法国大革命時分為現今的二部份](../Page/法国大革命.md "wikilink")。在[拿破仑一世時](../Page/拿破仑一世.md "wikilink")，法國版圖已延伸到佛蘭德斯全地以及現今的[比利時](../Page/比利時.md "wikilink")，一直到1815年[维也纳会议時讓法國領土回到革命前的領土為止](../Page/维也纳会议.md "wikilink")。

在十九世紀時，此地區是主要的工業地區，是法國領先的工業化區域之一，僅次於[阿爾薩斯-洛林](../Page/阿爾薩斯-洛林.md "wikilink")。1870年的[普法戰爭幾乎沒有影響到此一地區](../Page/普法戰爭.md "wikilink")，戰後因為阿爾薩斯和洛林都割讓給德國，北部-加来海峡大區成為法國的首要工業化地區，但在二十世紀的二次大戰中，受到了災難性的破壞。

  - 第一次世界大戰

[第一次世界大戰時](../Page/第一次世界大戰.md "wikilink")，此區域因為煤及礦產而成為[協約國及](../Page/協約國.md "wikilink")[同盟國爭奪的戰略要地](../Page/同盟國_\(第一次世界大戰\).md "wikilink")。當德軍從比利時入侵時，此地區是最早被攻陷的區域之一。不過當協約國在[第一次馬恩河戰役打敗同盟國後](../Page/第一次馬恩河戰役.md "wikilink")，協約國前進到[阿拉斯](../Page/阿拉斯.md "wikilink")。後面的四年，此區域分為二部份，德軍佔領[法國佛蘭德斯及](../Page/法國佛蘭德斯地區.md "wikilink")[康布雷](../Page/康布雷.md "wikilink")，而協約國控制阿拉斯及[朗斯](../Page/朗斯.md "wikilink")，雙方都希望完全控制此區域。

最後是由加拿大軍打敗此區域的同盟軍，但全區損失慘重，阿拉斯90%的地區被毀。北部-加来海峡是一次大戰的重要衝突點之一，在1914年到1918年發生了多次的戰役，包括[維米嶺戰役](../Page/維米嶺戰役.md "wikilink")、、及，現今在當地有650個軍人紀念碑，大部份是英軍及加拿大軍的，也有像及等大型的紀念碑。

  - 第二次世界大戰

在[二戰大戰德軍佔領法國時間](../Page/二戰大戰德軍佔領法國.md "wikilink")，此地區是由以下，是由位在布魯塞爾的[德意志國防軍統治](../Page/德意志國防軍.md "wikilink")。當時北部-加来海峡用來架設，包括攻擊英國的[V-1飛彈及生產](../Page/V-1飛彈.md "wikilink")[V-2火箭及](../Page/V-2火箭.md "wikilink")[V-3炮的](../Page/V-3炮.md "wikilink")[地堡](../Page/地堡.md "wikilink")。在[同盟國的](../Page/同盟國_\(第二次世界大戰\).md "wikilink")[十字弓行動中有轟炸此地區許多的村莊](../Page/十字弓行動.md "wikilink")。北部-加来海峡的大部份地區在1944年九月已被同盟國收復，而敦克爾克到1945年5月才收復，是最後收復的法國村莊。

  - 戰後

戰後此地區曾經歷幾次經濟的困境，在[英法海底隧道開通](../Page/英法海底隧道.md "wikilink")，交通量上昇後，經濟已獲好轉。

## 人口

此區域主要語言是[法語](../Page/法語.md "wikilink")，主要的[少數語言族群有二種](../Page/少數族群語言.md "wikilink")：西[佛萊明人](../Page/佛萊明人.md "wikilink")，其證據是此處有許多荷式的地區名稱，而且有族群的語言是[西佛萊明語](../Page/西佛蘭德語.md "wikilink")，一種[荷語的方言](../Page/荷語.md "wikilink")（在[敦刻尔克区附近的北部](../Page/敦刻尔克区.md "wikilink")-加来海峡大區及佛蘭德約有二萬人每天說西佛蘭德語，估計有四萬人偶爾使用\[3\]）。另一個族群是說[庇卡底語](../Page/庇卡底語.md "wikilink")（Picard，自1980年代還存留的幾近絕滅語言）。雖然鄰近的比利時認可佛萊明語及庇卡底語
，但法國政府的仍獨尊法語\[4\]，忽略這兩種語言及其他國內語言的存在。

此地種族的複雜是受到多波移民潮的影響：1910年前的[比利時人](../Page/比利時.md "wikilink")、1920年代及1930年代的[波蘭人及](../Page/波蘭.md "wikilink")[義大利人](../Page/義大利.md "wikilink")、1945年起的[德國人](../Page/德國.md "wikilink")、1960年代後的[北非人及](../Page/北非.md "wikilink")[葡萄牙人](../Page/葡萄牙.md "wikilink")\[5\]。

此地區的居民，約85%信仰[天主教](../Page/天主教.md "wikilink")，不過有些沒有固定參加[彌撒](../Page/彌撒.md "wikilink")，此區域也有人信仰[基督教](../Page/基督教.md "wikilink")，[新教在此地有建立教會](../Page/新教.md "wikilink")。來自北非的移民引入了[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")，也有移民信仰[佛教](../Page/佛教.md "wikilink")，人數不多，但正在增加當中。二次大戰時[納粹佔領法國](../Page/納粹.md "wikilink")，有不少法國的[猶太人當時被殺害](../Page/猶太人.md "wikilink")，但此地有少量猶太人社群仍然活躍，而且已在此地有數百年之久。

## 經濟

2007年時北部-加来海峡的[国内生产总值](../Page/国内生产总值.md "wikilink")（GDP）到達965億歐元，是法國第四大的經濟體，但這大區的人口也相當多，若考慮2007年的人均国内生产总值，此大區只在法國24大區中排名第20名。

2009年的失業率為12,8%，較全國平均失業率要高，尤其是18歲到25歲之間的失業最為顯著。

此大區的經濟主要是以[第三产业為主](../Page/第三产业.md "wikilink")，佔就業人口的75%，其他產業有製造業（23%）及農業。

### 農業及漁業

北部-加来海峡大區鄰近[北海](../Page/北海_\(大西洋\).md "wikilink")，有很強的[水產業](../Page/水產業.md "wikilink")。

若以船隻容納量來看，[滨海布洛涅港是法國第一大港](../Page/滨海布洛涅.md "wikilink")，可容納150艘船，在2012年漁獲量超過45000噸。滨海布洛涅港也是歐洲重要的[海鮮處理中心](../Page/海鮮.md "wikilink")，每年交易38萬噸貝類，魚類及[藻類](../Page/藻類.md "wikilink")\[6\]。有超過140家公司在這裡。

農業部份有13800個农场经营，其土地面積到，此大區的氣候溫和，土壤肥沃，也是重要的農業地區\[7\]。北部-加来海峡大區年產小麥數量有二千萬噸，約佔全法國產量的7%，馬鈴薯產量則佔全法國的1/3。

### 工業

此大區的工業原來是以煤炭及紡織業為主，是歐陸[工業革命的發源地之一](../Page/工業革命.md "wikilink")。[第二次世界大戰後時](../Page/第二次世界大戰.md "wikilink")，許多來自歐洲的移民移入，補入戰爭時流失的當地人力。在1970年代時，煤炭及紡織業開始走下坡，失業率快速上昇，因此當地的產業開始轉型，一直到今天仍在轉型中，現今此大區的工業是以[汽車業為主](../Page/汽車業.md "wikilink")。

#### 汽車業

在1970年代，汽車業在此區產值的比重還不大。四十年後，汽車業是此區的主力產業，就業人口超過五萬五千人。

全球重要車廠中，有三個在此區設有工廠：[豐田汽車在](../Page/豐田汽車.md "wikilink")[瓦朗謝訥有](../Page/瓦朗謝訥.md "wikilink")[豐田Yaris工廠](../Page/豐田Yaris.md "wikilink")\[8\]、[雷諾汽車在](../Page/雷諾汽車.md "wikilink")[杜埃有](../Page/杜埃.md "wikilink")車廠
，[标致雪铁龙集团在](../Page/标致雪铁龙集团.md "wikilink")[略圣阿芒設有工廠](../Page/略圣阿芒.md "wikilink")，製造、、、及等車輛\[9\]。此外，像等車輛零組件商有6200人在此工作。

北部-加来海峡大區在法國的汽車業排名第二名，僅次於巴黎區。當地汽車業的展會，里尔地区欧洲汽车论坛（FEAL）\[10\]，每二年進行一次，展示法國及全歐洲的新車。

#### 食品产业

北部-加来海峡大區的[食品产业是以當地的農業為基礎](../Page/食品产业.md "wikilink")，以收入來看，這是此大區最重要的產業，2006年輸出產值到32億歐元，2007年時此產業的就業人口超過27000人。像、、
及[Boulangeries
Paul等公司都在此地有據點](../Page/Boulangeries_Paul.md "wikilink")\[11\]

### 服務業

#### 米里曳家族

北部-加来海峡大區的服務業主要是由有多位百萬富翁的[米里曳家族](../Page/米里曳家族.md "wikilink")（Mulliez
Family）主控，米里曳家族掌管此區域的[超市及](../Page/超市.md "wikilink")[量販店](../Page/量販店.md "wikilink")，其中著名的有[歐尚](../Page/歐尚.md "wikilink")、[迪卡侬及](../Page/迪卡侬.md "wikilink")，也包括連鎖餐廳。米里曳家族也持有法國及公司及許多其他公司的股票。

## 交通

[Carte_ancienne_du_Nord-Pas-de-Calais.jpg](https://zh.wikipedia.org/wiki/File:Carte_ancienne_du_Nord-Pas-de-Calais.jpg "fig:Carte_ancienne_du_Nord-Pas-de-Calais.jpg")
此區域大部份的人口都住在市區，有高密度及複雜的交通系統，包括[高速公路](../Page/高速公路.md "wikilink")、[鐵路](../Page/鐵路.md "wikilink")、[機場及](../Page/機場.md "wikilink")[海港](../Page/海港.md "wikilink")。

### 高速公路

經過此區域的高速公路有九條，大部份不用收費：

  - [A1高速公路連接](../Page/A1高速公路_\(法国\).md "wikilink")[里爾及](../Page/里爾.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")
  - [A2高速公路通往](../Page/A2高速公路_\(法国\).md "wikilink")[布魯塞爾](../Page/布魯塞爾.md "wikilink")
  - [A16高速公路連接](../Page/A16高速公路.md "wikilink")[敦刻尔克及](../Page/敦刻尔克.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")
  - [A21高速公路連接](../Page/A21高速公路.md "wikilink")[布维尼及](../Page/布维尼.md "wikilink")[佩克庫爾](../Page/佩克庫爾.md "wikilink")
  - [A22高速公路連接](../Page/A22高速公路.md "wikilink")[里爾及](../Page/里爾.md "wikilink")[根特](../Page/根特.md "wikilink")
  - [A23高速公路連接](../Page/A23高速公路.md "wikilink")[莱斯坎及](../Page/莱斯坎.md "wikilink")[瓦朗謝訥](../Page/瓦朗謝訥.md "wikilink")
  - [A25高速公路連接](../Page/A25高速公路.md "wikilink")[敦刻尔克及](../Page/敦刻尔克.md "wikilink")[里爾](../Page/里爾.md "wikilink")
  - [A26高速公路連接](../Page/A26高速公路.md "wikilink")[加来及](../Page/加来.md "wikilink")[特鲁瓦](../Page/特鲁瓦.md "wikilink")
  - [A27高速公路連接](../Page/A27高速公路.md "wikilink")[里爾及](../Page/里爾.md "wikilink")[图尔奈](../Page/图尔奈.md "wikilink")

### 鐵路

#### 英法海底隧道

自1994年起，北部-加来海峡大區和[英國可以由](../Page/英國.md "wikilink")[英法海底隧道相連接](../Page/英法海底隧道.md "wikilink")，其中包括三個隧道（二個單向的隧道，以及一個供維護及緊急使用的隧道），全長，是世界上海底部份最長的隧道\[12\]
。隧道整體結構，連接法國的[科凯勒及英國的](../Page/科凯勒.md "wikilink")[福克斯通](../Page/福克斯通.md "wikilink")，從正式啟用到2012年，已有三億人乘坐高速鐵路[歐洲之星通過隧道](../Page/歐洲之星.md "wikilink")\[13\]。除了歐洲之星外，英法海底隧道也有載運車輛的，以及其他貨運服務。

#### TER-Nord-Nord

[TER](../Page/法国省际列车.md "wikilink")-Nord是此區域的鐵路，由[法国国家铁路營運](../Page/法国国家铁路.md "wikilink")，連結北部-加来海峡大區內的重要城市及市鎮，由[大区委员会管理](../Page/大区委员会_\(法国\).md "wikilink")。

### 航空

北部-加来海峡主要的機場是[里爾附近的](../Page/里爾.md "wikilink")，原本只是區域型的轉運機場，現在有國際航線，飛往歐洲及西北非（[馬格里布](../Page/馬格里布.md "wikilink")）等地區。

### 内陆及国际货运

#### 敦刻爾克港

敦刻爾克港是法國最大的海港之一，以總貨運量來算是第三大，若以[水果及](../Page/水果.md "wikilink")[銅的進口量來看](../Page/銅.md "wikilink")，是全法國第一大。而[道达尔石油公司有修築](../Page/道达尔.md "wikilink")[液化天然氣載運船專用的碼頭](../Page/液化天然氣載運船.md "wikilink")\[14\]。

#### 塞纳-北部欧洲运河

是正在規劃中的高運量運河，連接[塞纳河和](../Page/塞纳河.md "wikilink")[阿爾勒](../Page/阿爾勒.md "wikilink")，可以連接[比利時](../Page/比利時.md "wikilink")、[荷蘭及](../Page/荷蘭.md "wikilink")[德國的其他運河](../Page/德國.md "wikilink")。此運河預計2016年開始營運。此計劃因為其高昂的費用（46億歐元）而有許多的爭議
。

## 體育

### 奧運的訓練中心

將此地區規劃為[奧運前](../Page/2012年倫敦奧運.md "wikilink")，參賽選手的訓練基地。在奧運前一個月，許多國家將其選手送到這裡進行訓練，其中有英國的[体操隊在](../Page/体操.md "wikilink")[阿尔克訓練](../Page/阿尔克.md "wikilink")，[紐西蘭的](../Page/紐西蘭.md "wikilink")[赛艇隊在](../Page/赛艇.md "wikilink")[格拉沃利讷訓練](../Page/格拉沃利讷.md "wikilink")，此外還有[法國國家男子籃球隊及](../Page/法國國家男子籃球隊.md "wikilink")[法國國家手球隊等](../Page/法國國家手球隊.md "wikilink")。

### 此大區的運動

[足球是此處最盛行的運動](../Page/足球.md "wikilink")，足球俱樂部的成員超過145,000人，其中有四個俱樂部是職業的：[里尔足球俱乐部及](../Page/里爾奧林匹克體育會.md "wikilink")[朗斯競賽會是](../Page/朗斯競賽會.md "wikilink")[法国足球甲级联赛的成員](../Page/法国足球甲级联赛.md "wikilink")，[瓦朗謝訥足球俱樂部是](../Page/瓦朗謝訥足球俱樂部.md "wikilink")[法國足球乙級聯賽的成員](../Page/法國足球乙級聯賽.md "wikilink")，而[保洛尼足球俱樂部有參加](../Page/保洛尼足球俱樂部.md "wikilink")[法國全國聯賽](../Page/法國全國聯賽.md "wikilink")。的女子足球已參加[法國甲組女子足球聯賽](../Page/法國甲組女子足球聯賽.md "wikilink")。

## 主要的城市及市鎮

[Lille_-_Vue_aérienne_02.JPG](https://zh.wikipedia.org/wiki/File:Lille_-_Vue_aérienne_02.JPG "fig:Lille_-_Vue_aérienne_02.JPG")，北部-加来海峡大區中最大的城市\]\]
[Jardins_familiaux_Tourcoing_J1.JPG](https://zh.wikipedia.org/wiki/File:Jardins_familiaux_Tourcoing_J1.JPG "fig:Jardins_familiaux_Tourcoing_J1.JPG")，圖的後面是Notre-Dame
de la Marlière教堂\]\]

  - [里爾有超過一百五十萬人居住](../Page/里爾.md "wikilink")。
  - [阿拉斯](../Page/阿拉斯.md "wikilink")
  - [滨海布洛涅](../Page/滨海布洛涅.md "wikilink")
  - [加来](../Page/加来.md "wikilink")
  - [康布雷](../Page/康布雷.md "wikilink")
  - [杜埃](../Page/杜埃.md "wikilink")
  - [敦克爾克](../Page/敦克爾克.md "wikilink")
  - [朗斯](../Page/朗斯.md "wikilink")
  - [列萬](../Page/列萬.md "wikilink")
  - [马尔康巴勒尔](../Page/马尔康巴勒尔.md "wikilink")
  - [莫伯日](../Page/莫伯日.md "wikilink")
  - [鲁贝](../Page/鲁贝.md "wikilink")
  - [圣奥梅尔](../Page/圣奥梅尔.md "wikilink")
  - [图尔宽](../Page/图尔宽.md "wikilink")
  - [瓦朗謝訥](../Page/瓦朗謝訥.md "wikilink")
  - [阿斯克新城](../Page/阿斯克新城.md "wikilink")
  - [瓦特勒洛](../Page/瓦特勒洛.md "wikilink")

## 教育

里爾學區的教育系統包括有約一百萬名學生，[法國北部里爾大學進行高等教育及相關研究](../Page/法國北部里爾大學.md "wikilink")。

## 參考資料

## 外部連結

  - [Nord-Pas-de-Calais : between yesterday's resistance and today's
    hospitality](https://web.archive.org/web/20130317114436/http://www.france.fr/en/regions-and-cities/nord-pas-de-calais-between-yesterdays-resistance-and-todays-hospitality)-
    Official French website (in English)
  - [Regional Council of
    Nord-Pas-de-Calais](http://www.nordpasdecalais.fr/)
  - [Official website: Tourism in
    Nord-Pas-deCalais](http://www.northernfrance-tourism.com/)

{{-}}

[Category:法國已撤銷的大區](../Category/法國已撤銷的大區.md "wikilink")
[Category:2016年废除的行政区划](../Category/2016年废除的行政区划.md "wikilink")
[Category:上法蘭西大區](../Category/上法蘭西大區.md "wikilink")

1.  [Région Nord-Pas de Calais: *Qu'est ce que la
    Région?*](http://www.nordpasdecalais.fr/institution/quEstCe/intro.asp)
     Retrieved 4 January 2011
2.
3.
4.  [法國憲法第二條提到](../Page/法國憲法.md "wikilink")「法語為共和國的語言」，原文為La langue de
    la République est le français
5.  [Chronologie de l'immigration en
    Nord-Pas-de-Calais](http://www.insee.fr/fr/themes/document.asp?ref_id=17070&reg_id=19&page=pages_de_profils/P11_89/P11_89.htm),
    INSEE
6.
7.  [En quelques
    lignes](http://www.insee.fr/fr/regions/nord-pas-de-calais/reg-dep.asp?theme=10&suite=1)
8.  [toyota motor manufacturing
    france](http://www.toyota-valenciennes.com)
9.  [DÉCOUVRIR LE SITE DE
    PRODUCTION](http://www.sevelnord.psa.fr/index.php?id=536)
10.
11. [L'industrie agroalimentaire en
    Nord-Pas-de-Calais](http://www.insee.fr/fr/regions/nord-pas-de-calais/default.asp?page=themes/dossiers_de_profils/dp_96/dp_96.htm)
12.
13. [Eurotunnel celebrates 300 million
    passengers](http://www.breakingtravelnews.com/news/article/eurotunnel-celebrates-300-million-passengers/)
14. [L'actualité en
    détail](http://www.dunkerque-port.fr/fr/presse/actualites/2011-06-02-terminal-methanier-demarrage-des-travaux-preparatoires-fr-14080.html)