|group2 = [大元](../Page/元朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")
[大蒙古国](../Page/蒙古帝国.md "wikilink")[大汗](../Page/大汗.md "wikilink") |list2
= [显宗](../Page/甘麻剌.md "wikilink")

|group2 = 统治 |list2 =
[世祖](../Page/忽必烈.md "wikilink")[成宗](../Page/元成宗.md "wikilink")[武宗](../Page/元武宗.md "wikilink")[仁宗](../Page/元仁宗.md "wikilink")[英宗](../Page/元英宗.md "wikilink")[泰定帝](../Page/泰定帝.md "wikilink")[天順帝](../Page/天順帝.md "wikilink")[文宗](../Page/元文宗.md "wikilink")[明宗](../Page/元明宗.md "wikilink")[文宗](../Page/元文宗.md "wikilink")[宁宗](../Page/元宁宗.md "wikilink")[惠宗（順帝）](../Page/元惠宗.md "wikilink")
}} |group2 = [北元](../Page/北元.md "wikilink") |list2 =
[惠宗](../Page/元惠宗.md "wikilink")[昭宗](../Page/元昭宗.md "wikilink")[天元帝](../Page/脱古思帖木儿.md "wikilink")
}}

|belowstyle = background-color: \#ffe6a1; |below =
[后宫](../Page/元朝后宫.md "wikilink")<small>（[皇后](../Page/Template:元朝皇后.md "wikilink")）</small>{{.w}}皇宫<small>（[京师](../Page/元大都.md "wikilink")）</small>

<hr />

}}<noinclude>

</noinclude>

[\*](../Category/元朝皇帝.md "wikilink")
[Category:元朝人模板](../Category/元朝人模板.md "wikilink")
[元](../Category/中国君主导航模板.md "wikilink")