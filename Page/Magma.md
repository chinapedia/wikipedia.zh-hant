**Magma**是成立於1969年的法國前衛搖滾團體，由出身古典科班的鼓手[Christian
Vander主導](../Page/Christian_Vander.md "wikilink")。他說他對於未來世界的預見，無論是人性或是自然都深深擾動其心靈。第一張專輯訴說著一段故事，一群人民逃離了行將毀滅的地球到Kobaia星球上。然而沒有多久，首先移居Kobaia的人們與新遷到的移民發生摩擦進而衝突。Magma專輯特別的地方在於Vander構思出一種稱為Kobaian的[人造語言](../Page/人造語言.md "wikilink")，絕大多數的歌曲皆以此語言演出。之後出版的專輯陸續訴說著更古老的傳說，Kobaian語則在歌曲中繼續扮演關鍵的角色。

在音樂上極富創作與想像力\[1\]\[2\]\[3\]，Magma運用了極多元的和唱方式，專輯*Mekanïk Destruktïw
Kommandöh*是對作曲家[Carl
Orff致敬](../Page/Carl_Orff.md "wikilink")\[4\]，*Wurdah
Itah*則表現出受到[貝拉·巴托克的鋼琴音樂與](../Page/貝拉·巴托克.md "wikilink")[伊戈爾·斯特拉文斯基的](../Page/伊戈爾·斯特拉文斯基.md "wikilink")"Les
Noces"的影響。爵士樂[薩克斯風手](../Page/薩克斯風.md "wikilink")[約翰·柯川對Magma亦有著強大的影響力](../Page/約翰·柯川.md "wikilink")。\[5\]

許多曾經和Magma合作樂手，在離開後組成了新的以及樂風雷同的團體，眾人便以Kobain語中的Zeuhl來稱呼這種樂風以及當時與他們有密切關係的法國前衛搖滾與爵士融合樂。除了[Christian
Vander外](../Page/Christian_Vander.md "wikilink")，Magma中最為人熟悉的應該是貝斯手[Jannick
Top](../Page/Jannick_Top.md "wikilink")（後來曾製作過Celine Dion的專輯）。

雖然Magma的專輯不大容易找的到，Christian Vander與Jannick
Top兩人的知名度讓Magma能夠出版並重新製作[黑膠唱片的cd版本](../Page/黑膠唱片.md "wikilink")。

## 音樂唱片分類目錄

不同於許多合輯，"Spiritual"這張合輯是由其他合輯拼湊而城的海賊版，沒有任何新歌曲，Magma也沒有從這張唱片獲得任何利潤。

**錄音專輯**

  - 1970年 Kobaïa
  - 1971年 1001° Centigrades
  - 1972年 The Unnamables *（as [Univeria
    Zekt](../Page/Univeria_Zekt.md "wikilink")）*
  - 1973年 Mekanïk Kommandöh *（not officially released until 1989）*
  - 1973年 Mekanïk Destruktïw Kommandöh
  - 1974年 Wurdah Ïtah
  - 1974年 Köhntarkösz
  - 1976年 Üdü Wüdü
  - 1978年 Attahk
  - 1984年 Merci
  - 2004年 K.A. (Kohntarkosz Anteria)

**其他專輯**

  - 1975年 Live/Hhaï
  - 1977年 Inédits
  - 1981年 Retrospektiw (Parts I+II)
  - 1981年 Retrospektiw (Part III)
  - 1986年 Mythes Et Legendes Vol. I (compilation)
  - 1992年 Les Voix de Magma (live)
  - 1994年 Akt IV (Theatre Du Taur Concert, 1975)
  - 1995年 Akt V (Concert Bobino, 1981)
  - 1996年 Akt VIII (Bruxelles - Theatre 140, 1971)
  - 1998年 Floë Ëssi/Ëktah (EP)
  - 1998年 Simples
  - 2000年 DVD Concert du Trianon, january 2000

## 外部連結

  - [Magma site, includes interview with Christian
    Vander](https://web.archive.org/web/20091003132005/http://www.furious.com/perfect/magma.html)
  - [Lyrics to *Mekanïk Destruktïw Kommandöh* in
    Kobaian](https://web.archive.org/web/19981207022716/http://members.aol.com/sleeplessz/mdklyrics.htm)
  - [Utopic Records, Jannick Top's record
    label](https://web.archive.org/web/20040124181214/http://www.utopic-records.com/)
  - [Seventh Records, Christian Vander's record
    label](http://www.seventhrecords.com/)
  - [Partial Kobaian to English
    dictionary](https://web.archive.org/web/20000424055756/http://www.geocities.com/SunsetStrip/Alley/8366/MAGMA-KOBAIAN-dictionary.html)
  - [Aquarius Records, US seller of many Seventh Records
    releases](http://www.aquariusrecords.org/)

## 参考来源

[Category:法國樂團](../Category/法國樂團.md "wikilink")
[Category:前衛搖滾樂團](../Category/前衛搖滾樂團.md "wikilink")
[Category:摇滚乐团](../Category/摇滚乐团.md "wikilink")
[Category:日本富士搖滾音樂祭參加歌手](../Category/日本富士搖滾音樂祭參加歌手.md "wikilink")

1.
2.
3.
4.
5.