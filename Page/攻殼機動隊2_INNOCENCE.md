**攻殼機動隊2
INNOCENCE**（）（港譯《**攻殻機動隊2之無邪**》，大陸通譯《**攻殻機動隊2：無罪**》，另有譯名《**攻殻機動隊2：無垢**》）是[攻殼機動隊系列的其中一部動畫電影](../Page/攻殼機動隊.md "wikilink")。劇情大致延續第一部電影。2004年推出，耗資20億[日圓](../Page/日圓.md "wikilink")。由[押井守導演](../Page/押井守.md "wikilink")。本片由[Production
I.G.主持](../Page/Production_I.G..md "wikilink")，與多家公司合作。並且為了籌資20億，Production
I.G.還與[吉卜力工作室的鈴木敏夫合作](../Page/吉卜力工作室.md "wikilink")。獲選為2004年第八回[日本](../Page/日本.md "wikilink")[文部省](../Page/文部省.md "wikilink")[文化廳媒體藝術祭動畫部門推薦的作品](../Page/文化廳媒體藝術祭.md "wikilink")。本片入圍[第57屆坎城影展正式競賽單元](../Page/第57届戛纳电影节.md "wikilink")，與[史瑞克2並列為史上第一次入圍坎城影展正式競賽單元之動畫長片](../Page/史瑞克2.md "wikilink")。

## 劇情簡介

本片根據士郎正宗之原著漫畫，《[攻殼機動隊](../Page/攻殼機動隊.md "wikilink")》第一部之第六章〈Robot
Rondo〉改編，並且延續[第一部電影中](../Page/攻殼機動隊_\(電影\).md "wikilink")[草薙素子與](../Page/草薙素子.md "wikilink")「傀儡師」融合後下落不明的劇情。

本片由[日本未來西元](../Page/日本.md "wikilink")2032年的新港市中高級人造女奴[機器人失控殺人事件開始](../Page/機器人.md "wikilink")，[公安九課派出](../Page/公安九課.md "wikilink")**巴特**和**德古沙**以私人身分調查生產女奴的[跨國企業洛氏公司](../Page/跨國企業.md "wikilink")。在業餘時候，巴特很想念離開九課的**草薙素子**。他的髮型改為馬尾，並且養了一隻[巴吉度犬](../Page/巴吉度犬.md "wikilink")。巴特和德古沙沿線追查，和黑幫與一名[駭客對抗搏鬥之後](../Page/駭客.md "wikilink")，得到足夠線索與資源讓巴特潛入洛氏公司的工廠船。最後巴特在突然出現的草薙素子協助下，控制工廠船，查出洛氏公司傷害真人來生產人造女奴的罪行。

## 軼事

  - 除了以上所說之劇情結構之外，本片的主題受到[維利耶·德·利爾-阿達姆於](../Page/維利耶·德·利爾-阿達姆.md "wikilink")1886年所寫之[科幻小說](../Page/科幻小說.md "wikilink")《明日的夏娃》影響。許多項細節設定都可對應到那部小說。
  - 片中的女奴機器人的造型是來自德國藝術家[汉斯·贝尔默製作的](../Page/汉斯·贝尔默.md "wikilink")[球體關節人形](../Page/球體關節人形.md "wikilink")。巴特搜查時翻開的書本上就有他的名字。押井守為本片舉辦的紀念活動也是球體關節人形展覽。
  - 在本片中，德古沙與巴特在日本北方[擇捉島上看到的廟會一景](../Page/擇捉島.md "wikilink")，是參考[臺灣民間的廟會](../Page/臺灣.md "wikilink")[陣頭](../Page/陣頭.md "wikilink")。
  - 在本片中講的[粵語與](../Page/粵語.md "wikilink")[印尼語比第一部更多](../Page/印尼語.md "wikilink")。
  - 在片中曾出現多次的「生死去來，棚頭傀儡，一線斷時，落落磊磊。」是出自於日本能劇大師[世阿彌所寫的漢文詩](../Page/世阿彌.md "wikilink")。（日譯文：「生死の去来するは棚頭の傀儡たり一線断ゆる時落落磊磊」，出自日本僧侶月庵宗光(1326－1389)）
  - 在片中有遵守由[科幻小說家](../Page/科幻小說.md "wikilink")[艾西莫夫所創造出來的](../Page/艾西莫夫.md "wikilink")「[機器人學三大法則](../Page/機器人學三大法則.md "wikilink")」，第一法則：機器人不得傷害人類；而人造女奴攻擊人類的原因，導演押井守提出的解釋是：「人造女奴們以自己故障為由，創造出攻擊人類的許可，這個邏輯上的推論，也解開了倫理法第三項的束縛。」

## 參考文獻

## 外部連結

  - [官方網站](https://web.archive.org/web/20071005184314/http://www.innocence-movie.jp/)(連結失效)

  - [INNOCENCE](https://web.archive.org/web/20060220014032/http://prowaremedia.com.tw/innocence-DM.htm)（中文），普威爾公司提供。(連結失效)

  - [攻殼機動隊 Ghost in the Shell
    Section9](http://section9.seezone.net/)（中文），Jacky製作。

  -
[Category:2004年日本劇場動畫](../Category/2004年日本劇場動畫.md "wikilink")
[Innocence](../Category/攻殼機動隊.md "wikilink")
[Category:黑色科幻片](../Category/黑色科幻片.md "wikilink")
[Category:虛擬世界背景動畫電影](../Category/虛擬世界背景動畫電影.md "wikilink")
[Category:東寶動畫電影](../Category/東寶動畫電影.md "wikilink")
[Category:押井守電影](../Category/押井守電影.md "wikilink")
[Category:日本原創動畫電影](../Category/日本原創動畫電影.md "wikilink")
[Category:日本科幻片](../Category/日本科幻片.md "wikilink")
[Category:日本動作片](../Category/日本動作片.md "wikilink")
[Category:2000年代科幻片](../Category/2000年代科幻片.md "wikilink")
[Category:2000年代動作片](../Category/2000年代動作片.md "wikilink")
[Category:科幻動作片](../Category/科幻動作片.md "wikilink")
[Category:2032年背景電影](../Category/2032年背景電影.md "wikilink")
[Category:吉卜力工作室](../Category/吉卜力工作室.md "wikilink")
[Category:夢工廠電影](../Category/夢工廠電影.md "wikilink")
[Category:賽博格題材動畫電影](../Category/賽博格題材動畫電影.md "wikilink")
[Category:赛博朋克电影](../Category/赛博朋克电影.md "wikilink")
[Category:警察主角題材動畫電影](../Category/警察主角題材動畫電影.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")