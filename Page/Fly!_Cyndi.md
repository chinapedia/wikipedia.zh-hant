《**Fly\!
Cyndi**》，是歌手[王心凌在](../Page/王心凌.md "wikilink")2007年的專輯。共有兩版，一版為《Fly\!
Cyndi（初回限定盤）》，在2007年11月13日開始預購，於2007年11月30日正式發行。「Fly\!Cyndi（初回限定盤）」，共有10首歌以及內附40頁超豪華Fly旅行寫真書，心凌工作情況的幕後花絮DVD。預購附送2008王心凌溫泉之旅自拍私密日誌，在預購簽唱會上憑預購單兌領限量『飄飄加油扇』並可憑扇可上台簽名
。

一版為「Fly Cyndi (CD+DVD)（甜蜜聖誕版）」，在2007年12月21日發行，共有10首歌以及心凌之旅 2
「輕航機飛行體驗全記錄」、熱愛MV完整版以及附贈「這就是愛紙上MV」寫真冊

## 曲目

|                                    |                                    |                                    |                                    |                                    |
| ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| <font color=darkblue>**曲序**</font> | <font color=darkblue>**曲名**</font> | <font color=darkblue>**作曲**</font> | <font color=darkblue>**作詞**</font> | <font color=darkblue>**注釋**</font> |
| 01                                 | **飄飄**                             | [曹格](../Page/曹格.md "wikilink")     | 崔惟楷                                | 首波主打                               |
| 02                                 | '''熱愛 '''                          | [李榮浩](../Page/李榮浩.md "wikilink")   | 廖瑩如                                | 第二主打                               |
| 03                                 | **這就是愛**                           | [Tank](../Page/Tank.md "wikilink") | Midori                             | 第三主打（抒情主打）                         |
| 04                                 | 瞬間                                 | [柯貴民](../Page/柯貴民.md "wikilink")   | 廖瑩如                                |                                    |
| 05                                 | 雙人舞                                | 王心凌                                | 林白                                 |                                    |
| 06                                 | 日落前七分鐘                             | 古皓／旺旺                              | 蕭旭甫／古皓                             |                                    |
| 07                                 | 幸福神偷                               | [小安](../Page/小安.md "wikilink")     | 馬嵩惟                                |                                    |
| 08                                 | 還是好朋友                              | Min Myeong Gi                      | 陳彥輔                                |                                    |
| 09                                 | 泡泡糖                                | KIM SE-JIN                         | 馬嵩惟                                | OT：MALGWALRYANGI                   |
| 10                                 | 睜開眼                                | 饒善強                                | [易桀齊](../Page/易桀齊.md "wikilink")   |                                    |

### 初回限定版 Bonus DVD

  - 心凌工作情況的幕後花絮DVD
  - 飄飄MV完整版

### 甜蜜聖誕版 Bonus DVD

  - 心凌之旅 2 「輕航機飛行體驗全記錄」
  - 熱愛MV完整版

[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")
[Category:王心凌音樂專輯](../Category/王心凌音樂專輯.md "wikilink")