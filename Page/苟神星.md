****（）是第111颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1870年8月14日发现。的[直径为](../Page/直径.md "wikilink")134.6千米，[质量为](../Page/质量.md "wikilink")2.6×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1526.712天。

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")