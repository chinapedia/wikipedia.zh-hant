**伊拉諾·加德納**（Elanor Gardner），即是**美貌伊拉諾**（Elanor the
Fair）是[托爾金奇幻小說](../Page/托爾金.md "wikilink")[中土大陸](../Page/中土大陸.md "wikilink")（J.
R. R. Tolkien）的虛構角色。她是[山姆·詹吉](../Page/山姆·詹吉.md "wikilink")（Samwise
Gamgee）及妻子[小玫·卡頓](../Page/小玫·卡頓.md "wikilink")（Rosie Cotton）的長女。

伊拉諾·加德納生於[夏爾曆](../Page/夏爾曆.md "wikilink")1421年3月25日（第三紀元3021年），在[剛鐸](../Page/剛鐸.md "wikilink")（Gondor），這一天是[第四紀元的第一天](../Page/第四紀元.md "wikilink")。伊拉諾·加德納是[魔戒持有者](../Page/魔戒持有者.md "wikilink")[佛羅多·巴金斯](../Page/佛羅多·巴金斯.md "wikilink")（Frodo
Baggins）唯一知道的山姆子女。伊拉諾這名字也是佛羅多所取的，以在[羅斯洛立安](../Page/羅斯洛立安.md "wikilink")（Lothlórien）盛開的花卉[伊拉諾](../Page/伊拉諾.md "wikilink")（Elanor）命名，山姆很滿意這個精靈名字，以花卉作為人名在哈比人的女性當中很普遍。這名字頗適合，伊拉諾長得有點像精靈，她擁有一頭金髮，這對哈比人來說極為罕見。伊拉諾·加德納的美貌深為人知，因此人稱美貌伊拉諾。

夏爾曆1436年，伊拉諾擔任皇后[亞玟](../Page/亞玟.md "wikilink")（Arwen）的[首席伴娘](../Page/首席伴娘.md "wikilink")。夏爾曆1451年，伊拉諾·加德納嫁給格林何姆的法斯拉（Fastred）。四年後，在山姆的提名下，[哈比人統領](../Page/哈比人統領.md "wikilink")（Thain）任命法斯拉為[西境](../Page/西境.md "wikilink")（Westmarch）的市長。伊拉諾·加德納及法斯拉遂移居到[塔丘](../Page/塔丘.md "wikilink")（Tower
Hills），他們的家族在那裡繁衍下去。

夏爾曆1482年，山姆前往[海外仙境](../Page/海外仙境.md "wikilink")（Undying
Lands），伊拉諾成為[西境紅皮書](../Page/西境紅皮書.md "wikilink")（Red
Book of Westmarch）的持有者。伊拉諾有兩名孩子，分別是精靈坦（Elfstan）及費瑞爾（Fíriel）。

伊拉諾在《[魔戒](../Page/魔戒.md "wikilink")》的最後一節及附錄裡登場。在兩個沒有出版的《魔戒》版本裡，伊拉諾擔當著更重要的角色，這部分被收錄在《[中土大陸的歷史](../Page/中土大陸的歷史.md "wikilink")》裡。在那些版本裡，伊拉諾父親山姆談話，提到故事裡一些重要部分，以及提到[伊力薩王](../Page/伊力薩王.md "wikilink")（Elessar）及皇后亞玟曾來訪。

在[彼得·傑克遜](../Page/彼得·傑克遜.md "wikilink")（Peter
Jackson）執導的電影《魔戒》系列裡，三歲的伊拉諾由[亞歷桑德拉·奧斯汀](../Page/亞歷桑德拉·奧斯汀.md "wikilink")（Alexandra
Astin）飾演，她正是飾演山姆的演員[肖恩·奧斯汀](../Page/肖恩·奧斯汀.md "wikilink")（Sean
Astin）的女兒。

[pl:Lista hobbitów
Śródziemia\#Elanor](../Page/pl:Lista_hobbitów_Śródziemia#Elanor.md "wikilink")

[Category:中土大陸的哈比人](../Category/中土大陸的哈比人.md "wikilink")