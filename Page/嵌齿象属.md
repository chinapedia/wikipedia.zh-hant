**嵌齿象属**（[學名](../Page/學名.md "wikilink")：*Gomphotherium*），又名**三稜齒象**或**四偏齒象**，是一[屬已](../Page/屬.md "wikilink")[滅絕的](../Page/滅絕.md "wikilink")[長鼻目](../Page/長鼻目.md "wikilink")，生存於[中新世早期至](../Page/中新世.md "wikilink")[上新世早期的](../Page/上新世.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")（包括[法國](../Page/法國.md "wikilink")、[德國及](../Page/德國.md "wikilink")[奧地利](../Page/奧地利.md "wikilink")）、[北美洲](../Page/北美洲.md "wikilink")（[美國](../Page/美國.md "wikilink")[堪薩斯州](../Page/堪薩斯州.md "wikilink")）、[亞洲](../Page/亞洲.md "wikilink")（[巴基斯坦](../Page/巴基斯坦.md "wikilink")）及[非洲](../Page/非洲.md "wikilink")（[肯雅](../Page/肯雅.md "wikilink")）。

嵌齒象高3米，外觀像現今的[象](../Page/象.md "wikilink")，但有四顆象牙，兩顆在[上顎及兩顆在](../Page/上顎.md "wikilink")[下顎](../Page/下顎.md "wikilink")。下顎的象牙互相平行，形狀像剷子，用途也有可能一樣。不像現今的象，嵌齒象的上象牙有一層[牙釉質所覆蓋](../Page/牙釉質.md "wikilink")。嵌齒象的[頭顱骨較現今象的頭顱骨長而低](../Page/頭顱骨.md "wikilink")。牠有可能生活在[沼澤或近](../Page/沼澤.md "wikilink")[湖泊的地區](../Page/湖泊.md "wikilink")，利用牠們的象牙來挖起水中[植物](../Page/植物.md "wikilink")。與早期的長鼻目比較，嵌齒象只有很少的[臼齒](../Page/臼齒.md "wikilink")，臼齒上有脊來增加磨擦面。

嵌齒象的完整[骨骼是於](../Page/骨骼.md "wikilink")1971年在[德國的](../Page/德國.md "wikilink")[因河畔米爾多夫縣發現](../Page/因河畔米爾多夫縣.md "wikilink")。

## 外部連結

  - <http://www.enchantedlearning.com/subjects/mammals/Iceagemammals.shtml>
  - <http://www.elephant.se/gomphotherium.php>

[\*](../Category/嵌齒象科.md "wikilink")
[G](../Category/中新世生物.md "wikilink")