**抱卵亞目**（[学名](../Page/学名.md "wikilink")：），又称**腹胚亚目**，是[甲壳亚门](../Page/甲壳亚门.md "wikilink")[十足目中的一个亚目](../Page/十足目.md "wikilink")，它是1963年由[马丁·布尔肯罗德](../Page/马丁·布尔肯罗德.md "wikilink")（）提出的\[1\]。通过引入抱卵亞目，布尔肯罗德使用单系群的[枝鰓亞目](../Page/枝鰓亞目.md "wikilink")（[明蝦](../Page/明蝦.md "wikilink")）和抱卵亞目取代了过去分类中的[爬行亚目和](../Page/爬行亚目.md "wikilink")[游泳亚目](../Page/游泳亚目.md "wikilink")。抱卵亞目包括了此前的爬行亚目中的所有动物以及[蝟蝦下目和](../Page/蝟蝦下目.md "wikilink")[真蝦下目中的所有动物](../Page/真蝦下目.md "wikilink")，即：各种[虾类](../Page/虾.md "wikilink")、[寄居蟹类](../Page/寄居蟹.md "wikilink")、[蟹类](../Page/蟹.md "wikilink")。這些物種多数可供食用，是经济意义最大的一类甲壳动物。

## 分類

抱卵亞目包括以下10個下目\[2\]：

  - [蝟蝦下目](../Page/蝟蝦下目.md "wikilink") Stenopodidea
  - [真蝦下目](../Page/真蝦下目.md "wikilink") Caridea
  - [螯蝦下目](../Page/螯蝦下目.md "wikilink") Astacidea
  - [雕虾下目](../Page/雕虾下目.md "wikilink") Glypheidea
  - [甲蝦下目](../Page/甲蝦下目.md "wikilink")
    Axiidea：又名[阿蛄虾下目](../Page/阿蛄虾下目.md "wikilink")\[3\]
  - [蝼蛄虾下目](../Page/蝼蛄虾下目.md "wikilink") Gebiidea\[4\]
  - [無螯下目](../Page/無螯下目.md "wikilink") Achelata
  - [鞘虾下目](../Page/鞘虾下目.md "wikilink") Polychelida
  - [異尾下目](../Page/異尾下目.md "wikilink") Anomura
  - [短尾下目](../Page/短尾下目.md "wikilink") Brachyura

甲蝦下目與蝼蛄虾下目是從原來的[海蛄蝦下目](../Page/海蛄蝦下目.md "wikilink")（）分拆出來的，原來是海蛄蝦的總科\[5\]\[6\]。

抱卵亞目的动物拥有一系列共同特征，其中最重要的是雌性孵化[受精后的](../Page/受精.md "wikilink")[蛋](../Page/蛋.md "wikilink")，这些蛋在孵出前沾在雌性的[腹足上](../Page/腹足.md "wikilink")，这也是这个亚目名称的来源。

至於[化石方面](../Page/化石.md "wikilink")，最早期的代表是[泥盆紀時的](../Page/泥盆紀.md "wikilink")*[Palaeopalaemon](../Page/Palaeopalaemon.md "wikilink")*\[7\]。

## 参考资料

## 外部連結

  -
[抱卵亞目](../Category/抱卵亞目.md "wikilink")

1.

2.

3.

4.
5.

6.
7.