**小林麻央**（），是一名[日本女演員及播音員](../Page/日本.md "wikilink")，[cent.
Force經紀公司旗下藝人](../Page/cent._Force.md "wikilink")。丈夫為[歌舞伎演員](../Page/歌舞伎.md "wikilink")[市川海老藏](../Page/市川海老藏_\(11代目\).md "wikilink")，兩人於2010年結婚，婚後改姓**堀越**，育有一子一女。姐姐為自由新聞報導員。

2016年6月，市川海老藏於[記者會表示小林麻央罹患](../Page/記者會.md "wikilink")[乳癌](../Page/乳癌.md "wikilink")；同年9月，小林麻央開設[網誌分享抗癌歷程](../Page/網誌.md "wikilink")。2017年6月22日，小林麻央在[東京都家中病逝](../Page/東京都.md "wikilink")，得年34歲。\[1\]\[2\]

## 基本資料

  - 暱稱：「」
  - 出生地點：[日本](../Page/日本.md "wikilink")[新潟縣](../Page/新潟縣.md "wikilink")[小千谷市](../Page/小千谷市.md "wikilink")
  - 出道經過：
      - [國學院高等學校畢業](../Page/國學院高等學校.md "wikilink")
      - [上智大學文學院心理學系畢業](../Page/上智大學.md "wikilink")（2005年3月）
      - 加入cent. Force（2003年10月1日）

## 作品

### 日劇

  - 2005年:（愛情慢舞）-広瀨步美
  - 2006年:[Happy！2](../Page/Happy！2.md "wikilink")-竜ヶ崎蝶子
  - 2006年:[非關正義](../Page/非關正義.md "wikilink")-松本理恵子
  - 2006年:[美味求婚](../Page/美味求婚.md "wikilink")-島崎沙織
  - 2006年:[東京朋友](../Page/東京朋友.md "wikilink")-我孫子真希

### 電影

  - 2006年：[東京朋友](../Page/東京朋友.md "wikilink")（東京フレンズ The Movie）-我孫子真希
  - 2007年：[心動奇蹟](../Page/心動奇蹟.md "wikilink")（マリと子犬の物語）-関根博美

## 參考資料

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本自由播報員](../Category/日本自由播報員.md "wikilink")
[K](../Category/上智大學校友.md "wikilink")
[Category:BBC巾帼百名](../Category/BBC巾帼百名.md "wikilink")
[Category:罹患乳腺癌逝世者](../Category/罹患乳腺癌逝世者.md "wikilink")

1.  [34歲美女主播小林麻央癌逝
    星夫悲泣](http://www.cna.com.tw/news/firstnews/201706230091-1.aspx)
2.  [海老蔵さん会見「『愛してる』と言って旅立ちました」](http://www3.nhk.or.jp/news/html/20170623/k10011028001000.html)