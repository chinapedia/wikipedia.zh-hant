**葡屬印度** （[葡文](../Page/葡文.md "wikilink")：**** 或 ****
）由過去[葡萄牙於](../Page/葡萄牙.md "wikilink")[印度的](../Page/印度.md "wikilink")[殖民地所組成](../Page/殖民地.md "wikilink")。1947年[印度獨立時](../Page/印度獨立.md "wikilink")，葡萄牙於印度的殖民地包括位於其西岸的[果阿](../Page/果阿.md "wikilink")、[達曼-第烏](../Page/達曼-第烏.md "wikilink")，以及位於達曼內陸的[達德拉-納加爾哈維利](../Page/達德拉-納加爾哈維利.md "wikilink")。葡屬印度有時也會被統稱為果阿。

## 歷史

### 葡萄牙人登陸及佔領

首位抵達印度的葡萄牙人是[瓦斯科·達·伽馬](../Page/瓦斯科·達·伽馬.md "wikilink")。他於1498年5月20日在[卡利卡特](../Page/卡利卡特.md "wikilink")（即現今的[科澤科德](../Page/科澤科德.md "wikilink")）登陸。此後葡萄牙一直控制該地區。
[Map_of_Portuguese_India.png](https://zh.wikipedia.org/wiki/File:Map_of_Portuguese_India.png "fig:Map_of_Portuguese_India.png")

### 印度獨立

當印度於1947年正式獨立後，即要求把葡屬印度的[主權移交印度](../Page/主權.md "wikilink")，但即使[國際法庭及](../Page/國際法庭.md "wikilink")[聯合國大會於](../Page/聯合國大會.md "wikilink")1950年代皆就此作出對印度有利的議決，葡萄牙均沒有答應。

1954年，一群印度义民佔據了達德拉-納加爾哈維利。葡萄牙要求印度允許葡國部隊穿越印度國土進入這塊轄地恢復管治，但印度拒絕。1961年8月，[印度正式佔領該地](../Page/印度收復果阿.md "wikilink")，成為印度的一個聯邦屬地。1961年12月12日，印度向果阿、達曼及第烏发动进军，經過26小時的交火，成功佔領該三處地方，将其納入印度版圖。初時三地共組成為印度的一個聯邦屬地，至1987年5月30日果阿成為印度的一個邦，而[達曼-第烏則繼續保持為一個聯邦屬地](../Page/達曼-第烏.md "wikilink")。

葡屬印度被佔領後，葡萄牙的薩拉查政權一直拒絕承認印度對該地的主權，並繼續保留該地於葡萄牙國民議會中的席位。1974年，葡萄牙「[四二五革命](../Page/四二五革命.md "wikilink")」後，新政府宣佈放棄所有[殖民地](../Page/殖民地.md "wikilink")，因而同時承認了印度對原葡屬印度的主權，並恢復與印度的外交關係。然而，葡萄牙政府同時承諾繼續保留前葡屬印度人民作為葡萄牙公民的權利。

## 外部連結

  - [印度果阿：欧洲客的后院](https://web.archive.org/web/20071027082829/http://news.xinhuanet.com/herald/2005-01/07/content_2428560.htm)

  -
[Category:印度歷史](../Category/印度歷史.md "wikilink")
[Category:葡萄牙歷史](../Category/葡萄牙歷史.md "wikilink")
[Category:果阿邦历史](../Category/果阿邦历史.md "wikilink")