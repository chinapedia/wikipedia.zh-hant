**Pico**(**PI**ne
**CO**mposer)\[1\]是[Unix操作系统中最常见的三种文字处理软件之一](../Page/Unix.md "wikilink")\[2\]，具有文字编辑、搜索、[拼写检查](../Page/拼写检查.md "wikilink")、文件浏览和段对齐功能\[3\]，适合高效地编辑短小的文件\[4\]。

Pico是由华盛顿大学开发的免费软件，随着[pine电子邮件处理软件发布](../Page/pine.md "wikilink")\[5\]。它是在[Emacs的基础上以pine的邮件编辑为目标而开发的](../Page/Emacs.md "wikilink")\[6\]，所以其指令集是Emacs的子集\[7\]，但是由于在界面上有提示[快捷键](../Page/快捷键.md "wikilink")，相对于[vi和](../Page/vi.md "wikilink")[Emacs来说更加容易使用](../Page/Emacs.md "wikilink")\[8\]。

由于Pico虽然是免费软件，但是它并不是开源软件，所以很多Linux版本并不包含Pico。这些版本通常提供一个界面类似的开源软件[nano](../Page/nano.md "wikilink")\[9\]。

## 参考和引用

## 外部链接

  - [pine信息中心](http://www.washington.edu/pine/)

[Category:文本编辑器](../Category/文本编辑器.md "wikilink")
[Category:Unix文本编辑器](../Category/Unix文本编辑器.md "wikilink")

1.  [PICO TUTORIAL](http://www.usd.edu/trio/tut/pico/)

2.  [Editor choices for
    Unix](http://pangea.stanford.edu/computerinfo/unix/editing/editorchoices.html)

3.
4.  Linux教程（配光盘），萨瓦尔著，清华大学出版社，第41页

5.
6.  [Using the Pico Text
    Editor](http://www.helpdesk.umd.edu/documents/4/4795/)

7.
8.  Linux C编程/程序员书库，郭军著，清华大学出版社，第23页

9.  Ubuntu Linux Toolbox，Christopher Negus、Chris Negus、Francois
    Caen著，John Wiley and Sons出版社，第94页