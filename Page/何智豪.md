**何智豪**，[香港](../Page/香港.md "wikilink")[滑浪風帆運動員](../Page/滑浪風帆.md "wikilink")，妻子[陳慧琪亦是滑浪風帆香港代表](../Page/陳慧琪.md "wikilink")。何智豪曾參加過兩屆[奧運](../Page/奧運.md "wikilink")，在2006年[多哈亞運的男子滑浪風帆米氏板重量級中得到一面銀牌](../Page/多哈亞運.md "wikilink")。

何智豪出身於風帆世家，父親[何德明是香港八十年代風帆名將](../Page/何德明.md "wikilink")，叔叔[何德洪曾奪得香港滑浪風帆公開賽重量級冠軍](../Page/何德洪.md "wikilink")，曾於1988年代表香港參加[漢城奧運](../Page/漢城奧運.md "wikilink")，與[黃德森份屬同期師兄弟](../Page/黃德森.md "wikilink")\[1\]。何智豪於1999年世界青年帆船錦標賽勇奪冠軍後，開始嶄露頭角。

2002年[釜山亞運](../Page/釜山亞運.md "wikilink")，於男子滑浪風帆賽獲得第4名。

2006年[多哈亞運](../Page/多哈亞運.md "wikilink")，於男子米氏板重量級比賽，不敵中國選手[姚欣浩](../Page/姚欣浩.md "wikilink")，獲得一面銀牌\[2\]\[3\]。

何智豪曾經參加過2000年及2004年兩屆奧運會，2008年於奧運選拔賽不敵[陳敬然](../Page/陳敬然.md "wikilink")，無緣第三次出席奧運\[4\]。

2008年10月，在[印尼舉行亞洲風帆錦標賽](../Page/印尼.md "wikilink")，勇奪男子組冠軍\[5\]。

2009年12月，在[汕頭舉行的亞洲風帆錦標賽男子RS](../Page/汕頭.md "wikilink")：X組，不敵隊友[陳敬然](../Page/陳敬然.md "wikilink")，屈居亞軍\[6\]。

何智豪曾於2003年及2007年，先後兩次當選[香港傑出運動員](../Page/香港傑出運動員.md "wikilink")\[7\]
\[8\]。

## 主要成績

  - 1999年 世界青年帆船賽 冠軍
  - 2001年 亞洲錦標賽 米氏板輕量級亞軍
  - 2006年 [多哈亞運米氏板重量級](../Page/多哈亞運.md "wikilink") 銀牌
  - 2006年 亞洲錦標賽 米氏板重量級冠軍
  - 2008年 亞洲錦標賽 冠軍
  - 2009年 亞洲錦標賽 亞軍

## 榮譽

  - **香港特區政府嘉獎**

<!-- end list -->

  -
    [行政長官社區服務獎狀](../Page/行政長官社區服務獎狀.md "wikilink")（2007年）\[9\]

<!-- end list -->

  - **[香港傑出運動員選舉](../Page/香港傑出運動員選舉.md "wikilink")**

<!-- end list -->

  -
    「香港傑出運動員」（2003年、2007年 - 共2次當選）

## 參考資料

[Category:香港滑浪風帆運動員](../Category/香港滑浪風帆運動員.md "wikilink")
[Category:香港傑出運動員](../Category/香港傑出運動員.md "wikilink")
[C](../Category/何姓.md "wikilink")

1.  [風帆組織「暴風一族」被指霸地20年](http://the-sun.on.cc/channels/news/20021023/20021023023238_0001.html)
2.  [帆板：男子重型米氏帆板姚欣浩再夺一金](http://sports.qq.com/a/20061212/000021.htm)
3.  [香港在多哈亞運暫奪5金](http://www.rthk.org.hk/rthk/news/expressnews/20061212/news_20061212_55_363803.htm)
4.  [無緣京奧何智豪陪妻特訓](http://hk.news.yahoo.com/article/080406/4/6tvf.html)
5.  [何智豪膺亞洲賽冠軍](http://hk.news.yahoo.com/article/081013/3/8p26.html)
6.  [「風帆雙陳」亞錦賽稱霸](http://www.singtao.com.hk/yesterday/spo/1224co11.html)
7.  [2003年度香港傑出運動員選舉(得獎者名單)](http://sportstar2009.hkolympic.org/c_2003.php)

8.  [2007中銀香港傑出運動員選舉(得獎者名單)](http://sportstar2009.hkolympic.org/c_2007.php)

9.  [2007年度獲授勳人士紀念獎章頒贈典禮](http://www.hkolympic.org/article/c_articles_archive_view/1201/1/11/)
    中國香港體育協會暨奧林匹克委員會 2007-07-27