**天生桥一级水电站**位于[中国](../Page/中国.md "wikilink")[贵州省](../Page/贵州省.md "wikilink")[安龙县和](../Page/安龙县.md "wikilink")[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[隆林各族自治县交界处的](../Page/隆林各族自治县.md "wikilink")[南盘江干流上](../Page/南盘江.md "wikilink")，是[红水河第一级水电站](../Page/红水河.md "wikilink")。工程于1991年6月开工，1994年底截流，1997年底蓄水，1998年12月首台机组发电，2000年12月竣工。

坝址以上流域面积50139平方公里，多年平均流量612立方米/秒，年径流量193亿立方米。年平均输沙量1574万吨，平均含沙量0.81公斤/立方米。[水库总库容](../Page/水库.md "wikilink")102.6亿立方米，调节库容57.96亿立方米，为不完全多年调节水库。电站装机容量120万千瓦（4台30万千瓦机组），年发电量52.26亿度。
[万峰湖-2007.jpg](https://zh.wikipedia.org/wiki/File:万峰湖-2007.jpg "fig:万峰湖-2007.jpg")
**万峰湖**是天生桥一级水电站建成后形成的人工湖，面积约600平方公里。\[1\]

## 参见

  - [中华人民共和国水电站列表](../Page/中华人民共和国水电站列表.md "wikilink")
  - [中国最高水坝列表](../Page/中国最高水坝列表.md "wikilink")

## 參考文獻

[Category:贵州水电站](../Category/贵州水电站.md "wikilink")
[Category:广西水电站](../Category/广西水电站.md "wikilink")
[Category:黔西南水利](../Category/黔西南水利.md "wikilink")
[Category:百色水利](../Category/百色水利.md "wikilink")
[Category:安龙县](../Category/安龙县.md "wikilink")
[Category:隆林各族自治县](../Category/隆林各族自治县.md "wikilink")
[T](../Category/紅水河水系.md "wikilink")

1.  <http://www.qxn.gov.cn/Detail/Article.4.2/1929.html>