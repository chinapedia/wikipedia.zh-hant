**《懷風藻》**，[日本現存最古老](../Page/日本.md "wikilink")[漢詩選集](../Page/漢詩.md "wikilink")\[1\]。

## 编撰背景

《怀风藻》编辑时，日本正处在全面模仿中国大陆的时期，政治上实行律令制，文化上大规模创作汉诗。根据《怀风藻》的序言，由于近江朝（因飞鸟时代天智天皇定都[近江国](../Page/近江国.md "wikilink")，故称近江朝）政治安定，民生平和，促进了诗文创作的发展。

## 編者

《懷風藻》於751年（[天平勝寶三年](../Page/天平勝寶.md "wikilink")）編纂完成，編者不詳。有指編者是[大友皇子的曾孫](../Page/大友皇子.md "wikilink")[淡海三船](../Page/淡海三船.md "wikilink")，理由是詩集的「詩人略傳」中編者對[近江朝有著同情筆調](../Page/近江朝.md "wikilink")。然而也有謂編者是[葛井廣成](../Page/葛井廣成.md "wikilink")、藤原刷雄或[石上宅嗣等](../Page/石上宅嗣.md "wikilink")，至今未有定論。

## 內容簡介

詩集名為《懷風藻》，其序文写道：“余撰此文意者、為将不忘先哲遺風、故以懐風名之云爾“。意即「緬懷先哲遺風」。「藻」字則可能典出[陸機](../Page/陸機.md "wikilink")《文賦》：「藻，水草之有文者，故以喻文焉」。

全詩集收錄64位作者共120首作品。作者都是當時的皇族顯貴，例如[文武天皇](../Page/文武天皇.md "wikilink")、大友皇子、[川島皇子](../Page/川島皇子.md "wikilink")、[大津皇子和其他官吏](../Page/大津皇子.md "wikilink")、[儒生](../Page/儒生.md "wikilink")、[僧侶等](../Page/僧侶.md "wikilink")（其中18人兼為《[萬葉集](../Page/萬葉集.md "wikilink")》收錄的[和歌作者](../Page/和歌.md "wikilink")）。詩歌以五言八句為主，內容方面包括侍宴從駕、宴遊、述懷、詠物等，借用儒道老莊典故，文風浮華，講求對仗，似是深受[中國](../Page/中國.md "wikilink")[六朝文學影響](../Page/六朝.md "wikilink")。

《懷風藻》的出現，象徵自[奈良時代起日本文壇對漢文學的重視](../Page/奈良時代.md "wikilink")。而且值得注意的是，當中的詩歌有很多是宮廷詩宴這種官式場合的酬唱，這也反映當時尊尚漢風文化的潮流。

## 注釋

<div style="font-size: 85%">

<references />

</div>

## 參考資料

### 書籍

1.  葉渭渠（2005）。《日本文化史》，桂林：廣西師範大學出版社，頁71-73。 ISBN 7563338797

### 網頁

1.  [《奈良文化～日本通史㈤》](https://web.archive.org/web/20070311002911/http://www.hanhuncn.com/Html/Rbxj/20051211171211163_7.html)
2.  [<日本的漢詩>](http://www.xinghui.com/big5/guojiwenhuajiaoliu/zi/rbdhs.htm)，《新民晚報》，轉載於《北京星輝翻譯中心》網頁。
3.  宿久高、尹允镇。[\<《懷風藻》與中國古典文學的關聯\>](https://web.archive.org/web/20070311024422/http://www.wanfangdata.com.cn/qikan/periodical.Articles/ryxxyyj/ryxx2005/0503/050312.htm)，《日語學習與研究》，2005年03期。

## 相關條目

  - [萬葉集](../Page/萬葉集.md "wikilink")
  - [漢詩](../Page/漢詩.md "wikilink")

## 外部連結

  - [《懷風藻》](http://miko.org/~uraki/kuon/furu/text/waka/kaifuu/kaifuusou.htm)

[Category:日本漢詩集](../Category/日本漢詩集.md "wikilink")
[Category:8世紀書籍](../Category/8世紀書籍.md "wikilink")

1.  在《懷風藻》出現前，已有個人漢詩集《藤原宇合集》（[藤原宇合著](../Page/藤原宇合.md "wikilink")）、《銜悲藻》（[石上乙麻呂著](../Page/石上乙麻呂.md "wikilink")）問世，但現在均已散佚。