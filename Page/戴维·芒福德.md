[Mumford2.jpg](https://zh.wikipedia.org/wiki/File:Mumford2.jpg "fig:Mumford2.jpg")
**戴维·布赖恩特·芒福德**（David Bryant
Mumford，），又译**大衛·曼福德**，[美國數學家](../Page/美國.md "wikilink")。

在[哈佛大學](../Page/哈佛大學.md "wikilink")，他上[奥斯卡·扎里斯基的課時](../Page/奥斯卡·扎里斯基.md "wikilink")，引起了對[代數幾何學的興趣](../Page/代數幾何學.md "wikilink")。芒福德曾研究[模空間](../Page/模空間.md "wikilink")，出版了[幾何不變量理論一書](../Page/幾何不變量理論.md "wikilink")。現在任教於[布朗大學](../Page/布朗大學.md "wikilink")，研究[模式科学](../Page/模式科学.md "wikilink")。

1974年獲[費爾茲獎](../Page/費爾茲獎.md "wikilink")，2006年獲[邵逸夫獎](../Page/邵逸夫獎.md "wikilink")，2008年获得[沃尔夫奖](../Page/沃尔夫奖.md "wikilink")。

[Category:美国数学家](../Category/美国数学家.md "wikilink")
[Category:菲尔兹奖获得者](../Category/菲尔兹奖获得者.md "wikilink")
[Category:沃尔夫数学奖得主](../Category/沃尔夫数学奖得主.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:邵逸夫奖得主](../Category/邵逸夫奖得主.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:布朗大學教師](../Category/布朗大學教師.md "wikilink")