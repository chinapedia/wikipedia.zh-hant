**商法**（），或稱**商事法**，是關於[企業與](../Page/企業.md "wikilink")[商業](../Page/商業.md "wikilink")[交易規範的總稱](../Page/交易.md "wikilink")。

## 立法體系

### 民商分離制

民商分離是在[民法典之外制定商法典](../Page/民法典.md "wikilink")，商事上的規定皆在商法典中加以規定。1807年的[法国商法典](../Page/法国.md "wikilink")、1817年的[卢森堡商法典](../Page/卢森堡.md "wikilink")、1829年的[西班牙商法典](../Page/西班牙.md "wikilink")、1888年的[葡萄牙商法典](../Page/葡萄牙.md "wikilink")、1838年的[希腊商法典](../Page/希腊.md "wikilink")、1838年的[荷兰商法典](../Page/荷兰.md "wikilink")、1850年的[比利时商法典](../Page/比利时.md "wikilink")、1865年和1883年的[意大利商法典](../Page/意大利.md "wikilink")、1899年的[日本商法典](../Page/日本.md "wikilink")、1900年的[德国商法典等](../Page/德国.md "wikilink")，都是民商分離立法模式。

### 民商合一制

民商合一制是不在民法典之外制定商法典，許多的商事上的規定皆在民法典中加以規定。換言之，民商合一制是在規定私人間權利義務關係之外，連商人間之商業活動的相關規定亦制定於民法典之中。\[1\]

## 的商法

中华人民共和国司法管辖区内，没有独立的商法典，商法以单行民事法律方式实施，如[中华人民共和国公司法](../Page/中华人民共和国公司法.md "wikilink")、[中华人民共和国全民所有制工业企业法](../Page/中华人民共和国全民所有制工业企业法.md "wikilink")、[中华人民共和国票据法](../Page/中华人民共和国票据法.md "wikilink")、[中华人民共和国商法](../Page/中华人民共和国商法.md "wikilink")、[中华人民共和国保险法等](../Page/中华人民共和国保险法.md "wikilink")。

## 的商法

中華民國的民法編撰採「民商合一制」的方式，所以在民法典之外沒有另訂商法典，許多的商事活動上的規定皆在民法典中加以規定，另外也透過其它的特別法來加以補充，而這些和商業有關的特別法一般通稱為「商事法」。廣義的商事法包含所有與商業運作有關的[法律](../Page/法律.md "wikilink")、[命令](../Page/命令.md "wikilink")、[判例以及](../Page/判例.md "wikilink")[解釋等](../Page/解釋.md "wikilink")，狹義商事法則僅指[公司法](../Page/公司法.md "wikilink")、[票據法](../Page/票據法.md "wikilink")、[海商法和](../Page/海商法.md "wikilink")[保險法而已](../Page/保險法.md "wikilink")。[證券交易法自民國一百年起](../Page/證券交易法.md "wikilink")，列入中華民國[公務人員特種考試](../Page/公務人員特種考試.md "wikilink")[司法人員考試三等考試商法考科之一](../Page/司法人員考試.md "wikilink")。

職是之故，商事法則為[公司法](../Page/公司法.md "wikilink")、[證券交易法](../Page/證券交易法.md "wikilink")、[票據法](../Page/票據法.md "wikilink")、[保險法](../Page/保險法.md "wikilink")、[海商法等五科](../Page/海商法.md "wikilink")。

## 参考文献

## 参见

  - [民法](../Page/民法.md "wikilink")
  - [民法
    (中華民國)](../Page/民法_\(中華民國\).md "wikilink")、[六法全書](../Page/六法全書.md "wikilink")
  - [明治维新](../Page/明治维新.md "wikilink")
  - [日本商法典](../Page/日本商法典.md "wikilink")
  - [范馨香](../Page/范馨香.md "wikilink")
  - [美国法律文库](../Page/美国法律文库.md "wikilink")

{{-}}

[uk:Господарське право](../Page/uk:Господарське_право.md "wikilink")

[商法學](../Category/商法學.md "wikilink")

1.