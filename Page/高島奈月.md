**高島奈月**，日本[BL](../Page/BL.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。主要在[芳文社的漫畫月刊](../Page/芳文社.md "wikilink")《[花音](../Page/花音.md "wikilink")》與[角川書店旗下的](../Page/角川書店.md "wikilink")《[Ciel](../Page/Ciel.md "wikilink")》、《[TresTres](../Page/TresTres.md "wikilink")》活動。

## 作品

  - [我們的王國](../Page/我們的王國.md "wikilink")、5冊待續（[東立出版](../Page/東立.md "wikilink")）、原名《》
  - 護的憂鬱、全1冊（[東販出版](../Page/東販.md "wikilink")）、原名《》
  - 《GP學園》系列

:\* [GP學園學生會執行部](../Page/GP學園學生會執行部.md "wikilink")、全2冊（東販出版）、原名《》

:\* GP學園情報處理部、全5冊（東販出版）、原名《》

  - 赭紅的深情之絆、全1冊（[尚禾出版](../Page/尚禾.md "wikilink")）
  - 幸福的標記、全1冊（尚禾出版）
  - 喜歡與討厭的界線、全1冊（尚禾出版）、原名《》
  - 零距離的溫柔接觸、全1冊（尚禾出版）
  - 野蠻情人溫柔心、全1冊（尚禾出版）
  - 小兒科病房的騷動、全1冊（東立出版）、原名《》
  - 來場紳士協定吧！、2冊待續（[台灣角川出版](../Page/台灣角川.md "wikilink")）、原名《》
  - 風紀獨裁者、3冊待續（台灣角川出版）、原名《》
  - 來場主從契約吧！、全1冊（台灣角川出版）、原名《》

## 外部連結

[EAST
PROJECT--こうじま奈月HP](https://web.archive.org/web/20130926132244/http://www3.ocn.ne.jp/~eastpro/)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:BL漫画家](../Category/BL漫画家.md "wikilink")