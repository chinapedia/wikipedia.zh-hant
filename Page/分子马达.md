[Atp_synthase.PNG](https://zh.wikipedia.org/wiki/File:Atp_synthase.PNG "fig:Atp_synthase.PNG")(ATP
synthase / ATPase)分子模型\]\]

**分子马达**（Molecular
motor）是分布于[细胞内部或细胞表面的一类](../Page/细胞.md "wikilink")[蛋白质](../Page/蛋白质.md "wikilink")，它负责细胞内的一部分物质或者整个细胞的宏观运动。[生物体内的各种](../Page/生物.md "wikilink")[组织](../Page/组织.md "wikilink")、[器官乃至整个个体的运动最终都归结为分子马达在](../Page/器官.md "wikilink")[微观尺度上的运动](../Page/微观.md "wikilink")。分子马达將化学键中的能量耦合转化为[动能](../Page/动能.md "wikilink")。而[化学键中的能量最终来自](../Page/化学键.md "wikilink")[细胞膜或](../Page/细胞膜.md "wikilink")[线粒体膜内外的电化学梯度](../Page/线粒体.md "wikilink")。

## 例子

一些在[生物学中较为重要的分子马达](../Page/生物学.md "wikilink")\[1\]：

  - [细胞骨架马达蛋白](../Page/马达蛋白.md "wikilink")：
      - [肌球蛋白主要功能之一是使](../Page/肌球蛋白.md "wikilink")[肌肉收缩](../Page/肌肉.md "wikilink")。在非肌肉组织中维持细胞的弹性，以及参与[细胞质分裂](../Page/细胞质.md "wikilink")。
      - [驱动蛋白以](../Page/驱动蛋白.md "wikilink")[微管为轨道的动力蛋白](../Page/微管.md "wikilink")，运动的极性由[三磷酸腺苷水解酶](../Page/三磷酸腺苷水解酶.md "wikilink")（ATP水解酶）活性域在蛋白一级结构中的位置决定。
      - [动力蛋白一般是以](../Page/动力蛋白.md "wikilink")[超分子集成体的形式存在](../Page/超分子集成体.md "wikilink")，往[微管的负极运动](../Page/微管.md "wikilink")。

<!-- end list -->

  - [聚合马达蛋白](../Page/聚合马达蛋白.md "wikilink")：
      - [肌动蛋白聚合作用产生用于推进的力](../Page/肌动蛋白.md "wikilink")。该过程需要利用[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）[水解释放的能量](../Page/水解.md "wikilink")。
      - 微管聚合作用需要利用[三磷酸鸟苷](../Page/三磷酸鸟苷.md "wikilink")（GTP）。
      - [发动蛋白主要功能是从细胞膜中分离](../Page/发动蛋白.md "wikilink")[网格蛋白芽](../Page/网格蛋白.md "wikilink")。该过程需要利用GTP。

<!-- end list -->

  - [转动马达蛋白](../Page/转动马达蛋白.md "wikilink"):
      - F<sub>0</sub>F<sub>1</sub>[三磷酸腺苷合成酶](../Page/三磷酸腺苷合成酶.md "wikilink")(ATPase)位于细胞膜或线粒体膜上。利用[电化学质子的梯度合成ATP](../Page/电化学质子.md "wikilink")\[2\]。
      - 转动马达蛋白令[大肠杆菌](../Page/大肠杆菌.md "wikilink")（*E.
        coli*）和其他一些[细菌的](../Page/细菌.md "wikilink")[鞭毛像](../Page/鞭毛.md "wikilink")[螺旋桨一般转动](../Page/螺旋桨.md "wikilink")，使它们完成游动、翻滚或转向的动作。这种蛋白质由[跨膜质子流驱动](../Page/跨膜质子流.md "wikilink")，运作机制可能与中利用到的F<sub>o</sub>马达相似。

<!-- end list -->

  - [核酸马达蛋白](../Page/核酸马达蛋白.md "wikilink")：
      - [DNA聚合酶以一条](../Page/DNA聚合酶.md "wikilink")[单链DNA为模板合成与之互补的](../Page/单链DNA.md "wikilink")[DNA单链](../Page/DNA.md "wikilink")\[3\]\[4\]。
      - [RNA聚合酶](../Page/RNA聚合酶.md "wikilink")(RNAP)以一条单链DNA为模版转录合成与之互补的[RNA单链](../Page/RNA.md "wikilink")\[5\]。
      - [解旋酶将双链的](../Page/解旋酶.md "wikilink")[核酸分解为在](../Page/核酸.md "wikilink")[复制和](../Page/复制.md "wikilink")[转录中需要利用到的单链](../Page/转录.md "wikilink")。该过程需要利用ATP。
      - [拓扑异构酶降低细胞中DNA的](../Page/拓扑异构酶.md "wikilink")[超螺旋数目](../Page/超螺旋.md "wikilink")。该过程需要利用ATP。
      - [RSC和](../Page/染色质结构重塑复合物.md "wikilink")[SWI/SNF复合物重塑](../Page/SWI/SNF.md "wikilink")[真核细胞中的](../Page/真核细胞.md "wikilink")[染色质](../Page/染色质.md "wikilink")。该过程需要利用ATP。
      - [染色体结构维持蛋白被利用于真核细胞中的](../Page/染色体结构维持蛋白.md "wikilink")[染色体凝聚](../Page/染色体凝聚.md "wikilink")\[6\]。
      - [病毒DNA的](../Page/病毒.md "wikilink")[包装马达蛋白在病毒的](../Page/包装马达蛋白.md "wikilink")[复制周期内将其DNA注入其](../Page/复制周期.md "wikilink")[衣壳中](../Page/衣壳.md "wikilink")，使该DNA分子被衣壳蛋白紧密包裹起来\[7\]。

<!-- end list -->

  - [微丝聚合可产生推动力](../Page/微丝.md "wikilink")。
  - 人工合成的动力分子已被一些[化学家制出](../Page/化学家.md "wikilink")。它们通过改变分子的[手性产生](../Page/手性.md "wikilink")[扭矩](../Page/扭矩.md "wikilink")，从而在宏观上产生转向力。

## 实验方法

  - 高清晰度活体显微镜技术

<!-- end list -->

  - 广视野显微镜与三维去卷积(decovolution)技术
      - 共焦距显微镜(confocal microscopy)技术
          - 扫描共焦距激光显微技术(laser-scanning microscopy)
          - 转碟共焦距显微镜技术(spinning-disc confocal microscopy)
      - 全反射(Total Internal Reflection)与倾斜入射(Oblique illumination)激光显微镜技术

<!-- end list -->

  - 动力分子的主要研究方法还包括[分子物理学中的一些技术](../Page/分子物理学.md "wikilink")：
      - 荧光共振能转技术(FRET)，以及荧光交互波谱分析(FCS)
      - 单分子电生理分析（Electrophysiology），用于测量单个离子通道(ion channel)的动力学
      - 单分子滑动化验法（single molecule gliding
        assay），用于测量动力分子的运动速度，弹簧常数，以及processivity.
      - [光鑷](../Page/光鑷.md "wikilink")，用于测量动力分子的力学性质

## 参考文献

## 外部链接

  - [Cymobase](http://www.cymobase.org/cymobase) - 关于细胞骨架及分子马达蛋白序列资料的数据库

## 参见

  - [分子力学](../Page/分子力学.md "wikilink")
  - [蛋白质动力学](../Page/蛋白质动力学.md "wikilink")
  - [马达蛋白](../Page/马达蛋白.md "wikilink")
  - [细胞骨架](../Page/细胞骨架.md "wikilink")

[Category:分子机器](../Category/分子机器.md "wikilink")
[Category:生物物理学](../Category/生物物理学.md "wikilink")
[Category:细胞运动](../Category/细胞运动.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  Robert Sanders, [Molecular motor powerful enough to pack DNA into
    viruses at greater than champagne pressures, researchers
    report](http://www.berkeley.edu/news/media/releases/2001/10/18_motor.html),
    Press release, University of California