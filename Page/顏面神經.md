**面神經**是第七對[腦神經](../Page/腦神經.md "wikilink")。主要掌管[臉部表情及](../Page/臉部表情.md "wikilink")[眼皮開閉](../Page/眼皮.md "wikilink")。顏面神經受損可能導致眼皮不能閉合、[臉部](../Page/臉.md "wikilink")[肌肉](../Page/肌肉.md "wikilink")[癱瘓](../Page/癱瘓.md "wikilink")、口歪、[味覺異常等症狀](../Page/味觉.md "wikilink")。

臨床上顏面麻痺（歪斜）可簡單分為：周邊型及中樞型，周邊型顏面神經麻痺中，最常見就是貝爾氏麻痺（Bell's
palsy），俗稱「鬼吹風」，絕大多數的顏面神經麻痺都屬於良性疾病，恢復時間有時需長達六到八周，早期使用類固醇可以有效降低顏面神經的發炎。

## 外部連結

[解剖學(Anatomy)-腦神經(cranial nerve)-CN
VII(顏面神經)](http://smallcollation.blogspot.tw/2013/01/anatomy-cranial-nerve-cn-vii.html)

## 附圖

[`File:Gray507.png|表面解剖右側頸，顯示頸動脈及鎖骨下動脈`](File:Gray507.png%7C表面解剖右側頸，顯示頸動脈及鎖骨下動脈)`。`
[`File:Gray567.png|Dura`](File:Gray567.png%7CDura)` mater and its processes exposed by removing part of the right half of the skull, and the brain.`
[`File:Gray689.png|表面解剖腦幹。腹面觀點`](File:Gray689.png%7C表面解剖腦幹。腹面觀點)`。`
[`File:Gray719.png|後腿和中期的腦袋`](File:Gray719.png%7C後腿和中期的腦袋)`; postero-lateral view.`
[`File:Gray780.png|蝶節及其分支`](File:Gray780.png%7C蝶節及其分支)`。`
[`File:Gray781.png|下頜部的三叉神經痛的神經`](File:Gray781.png%7C下頜部的三叉神經痛的神經)`。`
[`File:Gray782.png|下頜部三叉神經痛神經`](File:Gray782.png%7C下頜部三叉神經痛神經)`, seen from the middle line. `
[`File:Gray788.png|Plan`](File:Gray788.png%7CPlan)` of the facial and intermediate nerves and their communication with other nerves.`
<File:Gray789.png>` |The course and connections of the facial nerve in the `[`temporal``
 ``bone`](../Page/temporal_bone.md "wikilink")`. `
[`File:Gray792.png|上部脊髓和-hind和中間的腦Upper`](File:Gray792.png%7C上部脊髓和-hind和中間的腦Upper)` part of medulla spinalis and hind- and mid-brains; posterior aspect, exposed in situ.`
[`File:Gray911.png|鼓室（enlarged）的內壁View`](File:Gray911.png%7C鼓室（enlarged）的內壁View)` of the inner wall of the tympanum (enlarged.)`
[`File:Gray912.png|右側膜與鼓錘和鼓索`](File:Gray912.png%7C右側膜與鼓錘和鼓索)`, viewed from within, from behind, and from above. `
[`File:Gray922.png|Position`](File:Gray922.png%7CPosition)` of the right bony labyrinth of the ear in the skull, viewed from above. `
[`File:Gray1209.png|Left`](File:Gray1209.png%7CLeft)` temporal bone showing surface markings for the tympanic antrum (red), transverse sinus (blue), and facial nerve (yellow).`
[`File:Gray1210.png|側頸部`](File:Gray1210.png%7C側頸部)`, showing chief surface markings.`
<File:Illu>` cranial nerves.jpg|腦神經`
<File:Head>` facial nerve branches.jpg|頭面部神經分支`

[Category:神經內科](../Category/神經內科.md "wikilink")
[Category:解剖学](../Category/解剖学.md "wikilink")