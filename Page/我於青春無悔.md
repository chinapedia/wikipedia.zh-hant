《**我於青春無悔**》（）又譯作《**我對青春無悔**》，乃[日本知名](../Page/日本.md "wikilink")[導演](../Page/導演.md "wikilink")[黑澤明於](../Page/黑澤明.md "wikilink")1946年所執導的黑白[電影](../Page/電影.md "wikilink")，片長110分鐘。

該電影取材自「[京大事件](../Page/京大事件.md "wikilink")」，由於在[第二次世界大戰前日本為了反](../Page/第二次世界大戰.md "wikilink")[共產主義](../Page/共產主義.md "wikilink")，而認為[自由主義是共產主義的溫床](../Page/自由主義.md "wikilink")，對於提倡自由主義的人當成共產份子來看待，甚至干涉了校園內的思想教育。黑澤明在戰後拍攝這部電影，被認為是一部反戰之作。

## 故事內容

八木原教授是個提倡自由思想的[大學](../Page/大學.md "wikilink")[教授](../Page/教授.md "wikilink")，他與妻子膝下只有一女幸枝，個性極為剛強獨立。教授有一群學生，也都很支持自由主義，但也因此倍受警方關注。為此八木原教授離開教職，而他有一名大力主張自由主義的學生野毛隆吉則被警方逮捕。另一位得力學生糸川則為了自己的前程與現實妥協，畢業後考上[檢察官](../Page/檢察官.md "wikilink")。

糸川原本愛慕幸枝，想要與之結婚。但在這時野毛出獄，一向欣賞野毛的幸枝決定離家過著獨立的生活，不想與糸川結婚。三年之後，幸枝從糸川口中得到野毛的所在，前往尋找野毛，兩人決定共同生活。然而好景不常，野毛為自由主義奔走，卻被當成是[共產黨](../Page/共產黨.md "wikilink")[間諜而被捕下獄](../Page/間諜.md "wikilink")，幸枝也不能倖免而被捉走。在獄中受盡折磨的幸枝，雖然得到父親的搭救而得以回家，但野毛卻在獄中身亡。幸枝自認已是野毛的妻子，便帶他的[骨灰回到他的家鄉](../Page/骨灰.md "wikilink")。

到了野毛的家鄉，雖然找到他的父母並安葬了骨灰，但因為野毛身負間諜的罪名，使得村裡的人連帶不能諒解他的家人。野毛的家成為村人攻擊的對象，而稻田也時常遭到破壞。幸枝並不在乎村人的冷嘲熱諷，雖然她本來是個千金小姐，卻仍然一鋤一鋤的幫野毛家耕作，讓野毛的父母都接受了這個[媳婦](../Page/媳婦.md "wikilink")。

戰後，幸枝一度回到娘家，此時野毛的罪名已經被洗清了，村人不再攻擊野毛家，而幸枝也決定投身於鄉村的[女權運動](../Page/女權主義.md "wikilink")，再次離開八木原家前往鄉下，受到村民的歡迎。

## 工作人員

  - 製作責任：竹井諒
  - 製作：松崎啓次
  - 導演：[黑澤明](../Page/黑澤明.md "wikilink")
  - 副導演：堀川弘通
  - 劇本：久板榮二郎
  - 攝影：中井朝一
  - 音樂：服部正
  - 美術：北川惠笥
  - 錄音：鈴木勇
  - 音響效果：三繩一郎
  - 燈光：石井長四郎
  - 編輯：後藤敏男

## 演員

  - 八木原幸枝：[原節子](../Page/原節子.md "wikilink")
  - 野毛隆吉：[藤田進](../Page/藤田進.md "wikilink")
  - 八木原教授：[大河内傳次郎](../Page/大河内傳次郎.md "wikilink")
  - 野毛之母：[杉村春子](../Page/杉村春子.md "wikilink")
  - 八木原夫人：[三好榮子](../Page/三好榮子.md "wikilink")
  - 糸川：[河野秋武](../Page/河野秋武.md "wikilink")
  - 野毛之父：[高堂國典](../Page/高堂國典.md "wikilink")
  - 毒草莓：[志村喬](../Page/志村喬.md "wikilink")
  - 文部大臣：[深見泰三](../Page/深見泰三.md "wikilink")
  - 筥崎教授：[清水將夫](../Page/清水將夫.md "wikilink")
  - 學生：[田中春男](../Page/田中春男.md "wikilink")、千葉一郎、米倉勇、高木昇、佐野宏
  - 刑警：光一、[岬洋二](../Page/岬洋二.md "wikilink")
  - 糸川之母：[原緋紗子](../Page/原緋紗子.md "wikilink")
  - 檢察官：武村新
  - 小使：河崎堅男
  - 老太太：藤間房子
  - 千金小姐：谷間小百合、河野糸子、[中北千枝子](../Page/中北千枝子.md "wikilink")

### 劇照

## 插曲

  - 《逍遥之歌》
  - 《戰友》

## 內部連結

  - [瀧川幸辰](../Page/瀧川幸辰.md "wikilink")：「八木原教授」之原型人物
  - [尾崎秀實](../Page/尾崎秀實.md "wikilink")：「野毛」之原型人物

[Category:黑澤明電影](../Category/黑澤明電影.md "wikilink")
[Category:東寶電影](../Category/東寶電影.md "wikilink")
[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:日語電影](../Category/日語電影.md "wikilink")
[Category:1946年电影](../Category/1946年电影.md "wikilink")
[Category:黑白電影](../Category/黑白電影.md "wikilink")
[Category:京都市背景電影](../Category/京都市背景電影.md "wikilink")