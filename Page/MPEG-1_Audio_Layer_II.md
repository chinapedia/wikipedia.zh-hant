**MPEG-1 Audio Layer II**（有時簡稱**MP2**）是ISO/IEC
11172-3（[MPEG-1音訊部份](../Page/MPEG-1.md "wikilink")）中訂立之[有損性](../Page/有损数据压缩.md "wikilink")[音訊壓縮格式](../Page/音訊壓縮.md "wikilink")。此標準還制訂了[MPEG-1
Audio Layer I](../Page/MPEG-1_Audio_Layer_I.md "wikilink")（MP1）和[MPEG-1
Audio Layer
III](../Page/MP3.md "wikilink")（MP3）。個人電腦和互聯網音樂流行MP3，MP2則多用於廣播。

## 技術規格

定義於ISO/IEC 11172-3

  - [取樣率](../Page/取樣率.md "wikilink"): 32, 44.1及48 kHz
  - [位元率](../Page/位元率.md "wikilink"): 32, 48, 56, 64, 80, 96, 112, 128,
    160, 192, 224, 256, 320及384 kbit/s

ISO/IEC 13818-3定義了用於MPEG-2 Layer II之延伸部份

  - 額外取樣率: 16, 22.05及24 kHz
  - 額外位元率: 8, 16, 24, 40及144 kbit/s
  - [多聲道](../Page/MPEG_Multichannel.md "wikilink") - 最多五個全頻聲道及一個超低音聲道

## 應用

[DAB及](../Page/DAB.md "wikilink")[DVB](../Page/DVB.md "wikilink")。

[PAL地區出售之](../Page/PAL.md "wikilink")[DVD-Video播放機需內置立體聲MP](../Page/DVD-Video.md "wikilink")2解碼器，[NTSC地區則不需要](../Page/NTSC.md "wikilink")。

[Video CD及](../Page/Video_CD.md "wikilink")[Super Video
CD之標準音軌](../Page/Super_Video_CD.md "wikilink")（SVCD另支援5.1多聲道〔〕）。

[HDV攝錄機音軌](../Page/HDV.md "wikilink")。

[香港數碼電視廣播](../Page/香港數碼地面電視廣播#技術標準.md "wikilink")。

## 另見

  - [MPEG-1](../Page/MPEG-1.md "wikilink")

      - [MPEG-1 Audio Layer
        I](../Page/MPEG-1_Audio_Layer_I.md "wikilink")
      - [MPEG-1 Audio Layer III](../Page/MP3.md "wikilink")

  - [MPEG-2](../Page/MPEG-2.md "wikilink")

  - [MP4 (容器格式)](../Page/MPEG-4_Part_14.md "wikilink")

  -
  - [Musepack](../Page/Musepack.md "wikilink") originally MP2-based,
    with numerous improvements

## 注釋

## 外部連結

  - [The history of MP3 from Fraunhofer
    IIS](https://web.archive.org/web/20070430233709/http://www.iis.fraunhofer.de/fhg/iis/EN/bf/amm/mp3history/index.jsp)
  - [MPEG Audio Resources and
    Software](https://web.archive.org/web/20021214015841/http://www.mpeg.org/MPEG/audio.html)
  - [TooLAME](http://toolame.sourceforge.net/) - an MP2 encoder
  - [TwoLAME](http://www.twolame.org/) - a fork of the tooLAME code
  - [RFC 3003](http://www.faqs.org/rfcs/rfc3003.html) - The document
    defining MIME type for MPEG-1 Audio Layer II
  - [A MPEG Audio Layer II decoder
    in 4k](https://web.archive.org/web/20090214074004/http://keyj.s2000.ws/?p=50)
    Source code for small open source decoder.
  - [Official MPEG web
    site](https://web.archive.org/web/20081109215716/http://www.chiariglione.org/mpeg/)
  - [Patent Status of MPEG-1, H.261 and
    MPEG-2](http://www.kuro5hin.org/story/2008/7/18/232618/312) - some
    informations about patents

[Category:Audio codecs](../Category/Audio_codecs.md "wikilink")
[Category:MPEG](../Category/MPEG.md "wikilink")