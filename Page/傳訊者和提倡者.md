**《傳訊者和提倡者》**（英文：Messenger and Advocate）（註：先前稱為《Latter Day Saints'
Messenger and
Advocate（後期聖徒的傳訊者和提倡者）》）是一份[後期聖徒的月刊](../Page/後期聖徒.md "wikilink")，自1834年10月到1837年9月止發行於[美國](../Page/美國.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")
Kirtland （嘉德蘭）。它的前身是《The Evening and Morning Star
（[夜與晨星](../Page/夜與晨星.md "wikilink")）》，它後來由《Elders'
Journal（[長老期刊](../Page/長老期刊.md "wikilink")）》和《Times and
Seasons（[時代與季節](../Page/時代與季節.md "wikilink")）》接替。

《傳訊者和提倡者》是在1833年7月20日後期聖徒在[密蘇里州](../Page/密蘇里州.md "wikilink")
Independence （獨立城）的印刷廠被摧毀後成立的。第一期發行於1834年10月，主編是 Oliver Cowdery
（[考得里奧利佛](../Page/考得里奧利佛.md "wikilink")）。

在1835年5月，William W. Phelps （斐普威廉）和 John Whitmer
（惠特茂大衛）從考得里奧利佛手中接下編輯的工作。1836年3月考得里奧利佛再次成為編輯，直到1837年2月出版的工作交到
Joseph Smith, Jr. （[小斯密約瑟](../Page/小斯密約瑟.md "wikilink")）和 Sidney Rigdon
（[雷格登瑟耐](../Page/雷格登瑟耐.md "wikilink")）手中。1837年10月，《傳訊者和提倡者》被《[長老期刊](../Page/長老期刊.md "wikilink")》取代。

## 雷格登派的《傳訊者和提倡者》

1844年[雷格登瑟耐宣稱他是](../Page/雷格登瑟耐.md "wikilink")[小斯密約瑟的繼承人並在](../Page/小斯密約瑟.md "wikilink")[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[匹茲堡組織了一個後期聖徒的團體](../Page/匹茲堡.md "wikilink")（見[繼承危機（後期聖徒運動）](../Page/繼承危機（後期聖徒運動）.md "wikilink")）。這個團體於1845年開始出版一份期刊，並再次使用這個《Latter
Day Saints' Messenger and Advocate（後期聖徒的傳訊者和提倡者）》的名字。當時的編輯是 Ebenezer
Robinson
（他也是《[時代與季節](../Page/時代與季節.md "wikilink")》的創辦人）。在雷格登瑟耐將這個團體的名字改回早期使用的「Church
of Christ（基督的教會）」後，這份期刊更名為《Messenger and Advocate of the Church of
Christ（基督的教會的傳訊者和提倡者）》。

## 請參見

  - [夜與晨星](../Page/夜與晨星.md "wikilink")
  - [長老期刊](../Page/長老期刊.md "wikilink")
  - [時代與季節](../Page/時代與季節.md "wikilink")

## 外部連結

  - [《傳訊者和提倡者星》線上資源（英文）](http://www.centerplace.org/history/ma/default.htm)

[Category:摩爾門教期刊](../Category/摩爾門教期刊.md "wikilink")