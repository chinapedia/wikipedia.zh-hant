**高氯酸钾**，也称**过氯酸钾**，是[高氯酸的](../Page/高氯酸.md "wikilink")[钾盐](../Page/钾盐.md "wikilink")，化学式为[K](../Page/钾.md "wikilink")[Cl](../Page/氯.md "wikilink")[O](../Page/氧.md "wikilink")<sub>4</sub>，具强[氧化性](../Page/氧化性.md "wikilink")。它是无色晶体或白色粉末，熔点约为610 °C。常用在[烟火和](../Page/烟火.md "wikilink")[闪光粉中作氧化剂](../Page/闪光粉.md "wikilink")，也用作[起爆药](../Page/起爆药.md "wikilink")。它可以作[固体火箭推进剂材料之一](../Page/固体火箭.md "wikilink")，但此应用基本上已被[高氯酸铵所取代](../Page/高氯酸铵.md "wikilink")。

所有[碱金属](../Page/碱金属.md "wikilink")[高氯酸盐中](../Page/高氯酸盐.md "wikilink")，高氯酸钾的[溶解度最低](../Page/溶解度.md "wikilink")（1.5g/100g，水，25 °C）。\[1\]

## 反应

KClO<sub>4</sub>可与很多[还原剂发生反应](../Page/还原剂.md "wikilink")，如与[葡萄糖](../Page/葡萄糖.md "wikilink")（C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>）的反应如下：

  -
    3 KClO<sub>4</sub> + C<sub>6</sub>H<sub>12</sub>O<sub>6</sub> → 6
    H<sub>2</sub>O + 6 CO<sub>2</sub> + 3 KCl

高氯酸钾与[蔗糖的混合物还算稳定](../Page/蔗糖.md "wikilink")，但与其它还原剂混合很可能发生[爆燃](../Page/爆燃.md "wikilink")，发生剧烈[氧化还原反应](../Page/氧化还原反应.md "wikilink")，火焰呈钾的[焰色紫色](../Page/焰色反应.md "wikilink")。[鞭炮中用高氯酸钾与](../Page/鞭炮.md "wikilink")[铝粉的混合物以制造闪光](../Page/铝.md "wikilink")。

[氯酸钾不可与](../Page/氯酸钾.md "wikilink")[硫混用](../Page/硫.md "wikilink")，但高氯酸钾可以。一个通用的解释为：硫会被氧化为[亚硫酸和](../Page/亚硫酸.md "wikilink")[硫酸](../Page/硫酸.md "wikilink")，后者与氯酸钾反应，生成[氯酸](../Page/氯酸.md "wikilink")。高浓度的氯酸很不稳定，会发生自燃，但是由高氯酸钾生成的[高氯酸则相对稳定](../Page/高氯酸.md "wikilink")，不会分解。

## 生产

高氯酸钾由[高氯酸钠与](../Page/高氯酸钠.md "wikilink")[氯化钾的](../Page/氯化钾.md "wikilink")[复分解反应制备](../Page/复分解反应.md "wikilink")，高氯酸钠则由[氯化钠的](../Page/氯化钠.md "wikilink")[阳极氧化制取](../Page/阳极氧化.md "wikilink")。\[2\]

## 医药

高氯酸钾可与其他治疗方法连用，以治疗[甲状腺功能亢进症](../Page/甲状腺功能亢进.md "wikilink")。

## 参考资料

<div class="references-small">

<references/>

</div>

## 外部链接

  - [闪光粉](http://www.wikipyro.com/Flash_Powder)
  - [MSDS](http://www.jtbaker.com/msds/englishhtml/P5983.htm)
  - [KClO<sub>4</sub>—Webbook](http://webbook.nist.gov/cgi/cbook.cgi?ID=C7778747)

[Category:高氯酸盐](../Category/高氯酸盐.md "wikilink")
[Category:钾化合物](../Category/钾化合物.md "wikilink")
[Category:易制爆化学品](../Category/易制爆化学品.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.
2.  [Perchlorate
    Production](https://web.archive.org/web/20080206095358/http://www.geocities.com/CapeCanaveral/Campus/5361/chlorate/encyperc.html)