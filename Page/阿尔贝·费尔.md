**艾尔伯·费尔**（，），[法国](../Page/法国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，2007年以[巨磁阻效應与](../Page/巨磁阻效應.md "wikilink")[彼得·格林贝格共同获得](../Page/彼得·格林贝格.md "wikilink")[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")\[1\]。

## 生平

艾尔伯·费尔於1962年畢業於巴黎高等師範學院。他之後在[巴黎第十一大学獲得了博士學位](../Page/巴黎第十一大学.md "wikilink")。

1988年，法國艾尔伯·费尔和德國[彼得·格林貝格同時並獨立地發現](../Page/彼得·格林貝格.md "wikilink")[巨磁阻效應](../Page/巨磁阻效應.md "wikilink")（GMR）。這一發現被公認為自旋電子學誕生，這個研究領域通常描述一種新型電子學，不僅利用電子的[電荷](../Page/電荷.md "wikilink")，而且也利用電子的磁性（自旋）。自旋電子學目前已經有重要應用。硬盤中引入巨磁阻效應讀取磁頭將使磁碟記錄密度顯著增加。

艾尔伯·费尔對自旋電子學的發展做出了許多貢獻，在2007年獲得諾貝爾獎之後，他正在探索自旋電子學的拓撲性質。他最近的貢獻是新型磁自旋材料，稱為斯格明子（skyrmions）
，拓撲絕緣體轉換電荷和自旋電流。

## 參考資料

<references/>

[Category:法国物理学家](../Category/法国物理学家.md "wikilink")
[Category:巴黎高等師範學院校友](../Category/巴黎高等師範學院校友.md "wikilink")
[Category:密西根州立大學教師](../Category/密西根州立大學教師.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:沃爾夫物理獎得主](../Category/沃爾夫物理獎得主.md "wikilink")
[Category:日本国际奖获得者](../Category/日本国际奖获得者.md "wikilink")

1.  [2007年諾貝爾物理獎公佈](http://nobelprize.org/nobel_prizes/physics/laureates/2007/press.html)-諾貝爾獎官方網頁