**忠武公李舜臣級驅逐艦**（）（KDX-II）是[大韓民國海军第二阶段开发研制的新型](../Page/大韓民國海军.md "wikilink")[驱逐舰](../Page/驱逐舰.md "wikilink")，由[大宇重工玉浦船厂建造](../Page/大宇集團.md "wikilink")，2002年5月22日首舰下水，2003年12月服役。首舰舷号975。

## 歷史

忠武公李舜臣級是「韓國驅逐艦實驗」(Korean Destroyer
Experimental，KDX)計畫的第二階段，忠武公李舜臣級擁有區域防空飛彈系統，主要任務為艦隊防空，由於進行時逢亞洲金融風暴，重創韓國經濟，因此計畫是在嚴格的成本控管下完成。最初預計建造三艘，爾後則增至六艘，分成兩批各三艘；在2004年中，據報韓國考慮增購6\~7艘KDX-2。基本設計由現代重工(Hyundai
Heavy Industries)負責，合約於1996年簽訂，而建造工作由現代重工與大宇重工(Daewoo Heavy
Industries)一同執行，

艦名以韓國史上君王、名將名為命名準則，首艦忠武公李舜臣號(DDH-975)冠上16世紀朝鮮名將李舜臣的名字。
首艦忠武公李舜臣號在2002年5月22日下水，翌年年初展開為時約一年的海上測試後交艦成軍；而後續的KDX-2則以平均每年一艘的速率陸續成軍。

## 同型艦

<table>
<thead>
<tr class="header">
<th><p>艦隻編號</p></th>
<th><p>艦名</p></th>
<th><p>原文及拼音</p></th>
<th><p>艦名由來</p></th>
<th><p>下水日期</p></th>
<th><p>服役日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>DDH 975</p></td>
<td></td>
<td><p><br />
Choongmoogong Yi Soon Shin</p></td>
<td><p><a href="../Page/李氏朝鮮.md" title="wikilink">李氏朝鮮海軍將領</a><a href="../Page/李舜臣_(朝鮮).md" title="wikilink">李舜臣</a>。<a href="../Page/壬辰衛國戰爭.md" title="wikilink">壬辰衛國戰爭中多次擊敗日本海軍</a>。常被韓國人視為民族英雄。</p></td>
<td><p>2002年5月22日</p></td>
<td><p>2003年12月2日</p></td>
</tr>
<tr class="even">
<td><p>DDH 976</p></td>
<td></td>
<td><p><br />
Munmu the Great</p></td>
<td><p><a href="../Page/新羅.md" title="wikilink">新羅第三十代君主</a><a href="../Page/金法敏.md" title="wikilink">金法敏</a>，<a href="../Page/史書.md" title="wikilink">史書稱</a><a href="../Page/文武王_(新羅).md" title="wikilink">文武王</a>。在位時統一當時被分裂為三個<a href="../Page/國家.md" title="wikilink">國家的</a><a href="../Page/朝鮮半島.md" title="wikilink">朝鮮半島</a>。</p></td>
<td><p>2003年4月11日</p></td>
<td><p>2004年9月30日</p></td>
</tr>
<tr class="odd">
<td><p>DDH 977</p></td>
<td><p><a href="../Page/大祚榮號驅逐艦.md" title="wikilink">大祚榮號</a></p></td>
<td><p><br />
Daejoyoung</p></td>
<td><p><a href="../Page/渤海國.md" title="wikilink">渤海國開國君主</a><a href="../Page/大祚榮.md" title="wikilink">大祚榮</a>，史稱<strong>大震國太祖高王</strong>。</p></td>
<td><p>2003年12月2日</p></td>
<td><p>2005年6月30日</p></td>
</tr>
<tr class="even">
<td><p>DDH 978</p></td>
<td><p><a href="../Page/王建號驅逐艦.md" title="wikilink">王建號</a></p></td>
<td><p><br />
Wang Gun</p></td>
<td><p><a href="../Page/高麗.md" title="wikilink">高麗國開國君主</a><a href="../Page/高麗太祖.md" title="wikilink">王建</a>，史稱<strong>高麗太祖</strong>。</p></td>
<td><p>2005年5月4日</p></td>
<td><p>2006年11月10日</p></td>
</tr>
<tr class="odd">
<td><p>DDH 979</p></td>
<td><p><a href="../Page/姜邯贊號驅逐艦.md" title="wikilink">姜邯贊號</a></p></td>
<td><p><br />
Gang Gamchan</p></td>
<td><p><a href="../Page/高麗.md" title="wikilink">高麗國時期軍事將領</a><a href="../Page/姜邯贊.md" title="wikilink">姜邯贊</a>，在<a href="../Page/高麗契丹戰爭.md" title="wikilink">第三次高麗契丹戰爭時擊退入侵的</a><a href="../Page/契丹.md" title="wikilink">契丹軍隊</a>。</p></td>
<td><p>2006年3月16日</p></td>
<td><p>2007年10月1日</p></td>
</tr>
<tr class="even">
<td><p>DDH 981</p></td>
<td></td>
<td><p><br />
Chae Yeon</p></td>
<td><p><a href="../Page/高麗.md" title="wikilink">高麗國時期軍事將領</a><a href="../Page/崔瑩.md" title="wikilink">崔瑩</a>，戰績包括擊退<a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/海盜.md" title="wikilink">海盜</a>。晚年時被手下將領（其後成為<a href="../Page/朝鮮王朝.md" title="wikilink">朝鮮王朝開國君主</a>）<a href="../Page/李成桂.md" title="wikilink">李成桂背叛</a>。</p></td>
<td><p>2006年10月20日</p></td>
<td><p>2008年9月4日</p></td>
</tr>
</tbody>
</table>

## 戰史

2010年韓國參與了多國聯軍在[亞丁灣打擊](../Page/亞丁灣.md "wikilink")[索馬利亞海盜的任務](../Page/索馬利亞海盜.md "wikilink")「」。2011年1月，韓籍化學運輸船「」遭到[索馬利亞海盜的小艇劫持](../Page/索馬利亞海盜.md "wikilink")，而總統[李明博下令巡邏亞丁灣的驅逐艦執行救援任務](../Page/李明博.md "wikilink")，派遣了艦上的直升機載運[海軍特戰旅隊員登船解救人質](../Page/大韓民國海軍特戰戰團.md "wikilink")，並俘獲了部分索馬利亞海盜成員。被韓軍俘虜的索馬利亞海盜被押回韓國法庭受審，最重判處無期徒刑。

## 関連項目

  - [大韓民國海軍](../Page/大韓民國海軍.md "wikilink")
  - [广开土大王级驱逐舰](../Page/广开土大王级驱逐舰.md "wikilink")
  - [世宗大王级驱逐舰](../Page/世宗大王级驱逐舰.md "wikilink")

[分類:韩国海军舰艇](../Page/分類:韩国海军舰艇.md "wikilink")
[分類:2002年下水](../Page/分類:2002年下水.md "wikilink")

[Category:驱逐舰](../Category/驱逐舰.md "wikilink")