**At
Last**是[美國](../Page/美國.md "wikilink")[洛杉磯一隊小眾樂隊](../Page/洛杉磯.md "wikilink")，由華人Justin
Fong、韓裔人Hans Cho和Michael H.
Lee、組成，主打[嘻哈音樂](../Page/嘻哈音樂.md "wikilink")，演唱時沒有伴奏樂器，節拍和配樂全由聲藝配合，其樂風深受Stevie
Wonder、Al Green、The Temptations 、Boyz II Men及Alicia Keys的影響。

在成立At
Last之前，Hans原為[雅虎僱員](../Page/雅虎.md "wikilink")，他與Michael原先在校園演出，隨後到洛杉磯尋找發展機會，認識Justin，組成At
Last樂隊，並從德州找來黑人DJ Say加盟，但DJ在2007年已退出。

At Last的演出受到[溫明娜及其丈夫Eric](../Page/溫明娜.md "wikilink")
Zee賞識，簽約成為他們的經理人，安排樂隊在[Justin
Timberlake](../Page/Justin_Timberlake.md "wikilink")、[Destiny's
Child及](../Page/Destiny's_Child.md "wikilink")[Boyz II
Men的演唱會上演出](../Page/Boyz_II_Men.md "wikilink")。直至2006年，At
Last參加美國NBC電視台節目[America's Got
Talent](../Page/America's_Got_Talent.md "wikilink")，其具有強烈節拍感的唱腔受到評判讚賞，開始為大眾認識。
[atlast_slowitdown.jpg](https://zh.wikipedia.org/wiki/File:atlast_slowitdown.jpg "fig:atlast_slowitdown.jpg")》唱片大量運用[無伴奏合唱的](../Page/無伴奏合唱.md "wikilink")[嘻哈](../Page/嘻哈音樂.md "wikilink")，成為At
Last的典型唱腔\]\] 在參與比賽前，該樂隊已完成首張大碟《Slow It Down》，唱片運用一種稱為Hip-hopapella，即hip
hop及a cappella二字組合，指
[無伴奏合唱的](../Page/無伴奏合唱.md "wikilink")[嘻哈](../Page/嘻哈音樂.md "wikilink")。At
Last指他們正在練習《Killing Me Softly》一曲，Michael突然以口技打上節拍，效果出奇地好，這亦成為日後At
Last的典型唱腔。

## 唱片

  - *Driven* (EP) – 2002
  - *A Holiday Gift* (CD Single) – 2002
  - *The Arrival* (CD Single) – 2003
  - *Slow It Down* - 2006

## 參考

  - [Jizo-Entertainment.com訪問](https://web.archive.org/web/20070928085126/http://jizo-entertainment.com/interviews/At_Last.htm)
  - [加州大學洛杉磯分校亞洲研究院視像訪問](https://web.archive.org/web/20060225220413/http://www.asiaarts.ucla.edu/052303/atlast1.ram)
    2003年

## 連結

  - [官方網頁](https://web.archive.org/web/20070521184745/http://www.atlastmusic.com/)
  - [MySpace頁](http://www.myspace.com/atlast)
  - [At Last的網上廣播](http://www.youtube.com/watch?v=6r_MLto2kUc)（Youtube）
  - [America's Got
    Talent上的演出](http://www.youtube.com/watch?v=hqjPFa0wPvA&mode=related&search=)

[Category:美國樂團](../Category/美國樂團.md "wikilink")