**郭銘仁**（），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，前[中華職棒](../Page/中華職棒.md "wikilink")[米迪亞暴龍隊](../Page/米迪亞暴龍.md "wikilink")，守備位置為[游擊手](../Page/游擊手.md "wikilink")。現效力於業餘球隊桃園航空成棒球隊。

## 經歷

  - [台東縣豐年國小](../Page/台東縣.md "wikilink")
  - [台東縣卑南國小](../Page/台東縣.md "wikilink")
  - [台東縣新生國中青少棒隊](../Page/台東縣.md "wikilink")
  - [台東縣台東體中青棒隊](../Page/台東縣.md "wikilink")
  - [台灣大聯盟](../Page/台灣大聯盟.md "wikilink")[台中金剛隊](../Page/台中金剛.md "wikilink")（2001年－2002年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS隊](../Page/誠泰COBRAS.md "wikilink")（2005年－2007年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[米迪亞暴龍隊](../Page/米迪亞暴龍.md "wikilink")（2008年）
  - 桃園航空城棒球隊（2009年－）

## 職棒生涯成績

| 年度    | 球隊                                         | 出賽  | 打數   | 安打  | 全壘打 | 打點  | 盜壘 | 四死  | 三振  | 壘打數 | 雙殺打 | 打擊率   |
| ----- | ------------------------------------------ | --- | ---- | --- | --- | --- | -- | --- | --- | --- | --- | ----- |
| 2005年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 82  | 283  | 72  | 1   | 19  | 2  | 23  | 60  | 97  | 2   | 0.254 |
| 2006年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 83  | 265  | 73  | 1   | 20  | 7  | 27  | 38  | 85  | 2   | 0.275 |
| 2007年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 100 | 412  | 121 | 2   | 36  | 6  | 43  | 66  | 145 | 3   | 0.294 |
| 2008年 | [米迪亞暴龍](../Page/米迪亞暴龍.md "wikilink")       | 89  | 320  | 100 | 4   | 39  | 4  | 51  | 58  | 143 | 5   | 0.313 |
| 合計    | 4年                                         | 354 | 1280 | 366 | 8   | 114 | 19 | 144 | 222 | 394 | 12  | 0.286 |

## 特殊事蹟

  - 2008年黑米事件過後，與[周思齊成為唯二參加](../Page/周思齊.md "wikilink")[中華職棒二十年頒獎典禮的](../Page/中華職棒.md "wikilink")[米迪亞暴龍球員](../Page/米迪亞暴龍.md "wikilink")。

## 外部連結

[M](../Category/郭姓.md "wikilink") [G](../Category/台灣棒球選手.md "wikilink")
[G](../Category/高屏雷公隊球員.md "wikilink")
[G](../Category/米迪亞暴龍隊球員.md "wikilink")
[G](../Category/誠泰COBRAS隊球員.md "wikilink")
[Category:國立臺東體育高中校友](../Category/國立臺東體育高中校友.md "wikilink")
[Category:臺東人](../Category/臺東人.md "wikilink")