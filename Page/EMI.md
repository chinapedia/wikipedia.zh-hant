**EMI集團**（，简称EMI），原名「**電子與音樂工業公司**」（）（香港曾譯作「電氣音樂有限公司」），是一家跨國的的音樂製作及唱片公司，總部位於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")。其唱片部門及版權部門已於2012年分別被[環球唱片及](../Page/環球唱片.md "wikilink")[索尼/聯合電視音樂出版领导的财团收購](../Page/索尼/聯合電視音樂出版.md "wikilink")。

## 歷史

EMI於1931年3月成立，後來參與廣播，特別是提供了第一個電視發射機給[BBC](../Page/BBC.md "wikilink")，EMI在2006\~2007兩年間虧損了2億6千萬英鎊，而在英國市占率由16%大跌到9%。2007年8月，EMI被[Terra
Firma資金伙伴有限公司以](../Page/Terra_Firma資金伙伴有限公司.md "wikilink")32億英鎊買下。在此次大動作後，數個重要旗下台柱歌手出走，如[電台司令](../Page/電台司令.md "wikilink")、[保羅麥卡尼等](../Page/保羅麥卡尼.md "wikilink")。而[滾石樂團也在](../Page/滾石樂團.md "wikilink")2008年7月與[環球唱片另簽了新約](../Page/環球唱片.md "wikilink")。

2007年，[Terra
Firma資金伙伴有限公司的](../Page/Terra_Firma資金伙伴有限公司.md "wikilink")[CEO](../Page/CEO.md "wikilink")[Guy
Hands宣布EMI將大幅裁撤約](../Page/Guy_Hands.md "wikilink")1500\~2000名員工，約為EMI總員工數5500人的三分之一。此措施可望減少每年約2億英鎊的人事支出。

### 被收购

2011年11月12日，[環球音樂和](../Page/環球音樂.md "wikilink")[索尼公司从](../Page/索尼公司.md "wikilink")[花旗集团手中以共](../Page/花旗集团.md "wikilink")41亿美元收购了EMI的音乐业务\[1\]，具体为环球音乐以19亿美元购得所有EMI录制的音乐唱片，索尼公司以22亿美元购得EMI的出版发行部，美国以及欧洲反垄断监管机构批准了此次收购。\[2\]

2012年6月30日，索尼与[迈克尔·杰克逊各持](../Page/迈克尔·杰克逊.md "wikilink")50%股权的合资公司[Sony/ATV以](../Page/索尼/聯合電視音樂出版.md "wikilink")22亿美元收购EMI，Sony/ATV将拥有EMI的200多万首歌曲版权。\[3\]

## 集團其他業務

  - [EMI音樂版權](../Page/EMI音樂版權.md "wikilink")

  - [EMI基督教音樂](../Page/EMI基督教音樂.md "wikilink")

  - [EMI電影](../Page/EMI電影.md "wikilink")

  -
## 其他海外地區業務

### 歐洲

  - [EMI奧地利](../Page/EMI奧地利.md "wikilink")
  - [EMI比利時](../Page/EMI比利時.md "wikilink")
  - [EMI捷克](../Page/EMI捷克.md "wikilink")
  - [EMI丹麥](../Page/EMI丹麥.md "wikilink")
  - [EMI法國](../Page/EMI法國.md "wikilink")
  - [EMI德國](../Page/EMI德國.md "wikilink")
  - [EMI希臘](../Page/EMI希臘.md "wikilink")
  - [EMI意大利](../Page/EMI意大利.md "wikilink")
  - [EMI荷蘭](../Page/EMI荷蘭.md "wikilink")

### 太平洋

  - [EMI紐西蘭](../Page/EMI紐西蘭.md "wikilink")

### 北美洲

  - [EMI加拿大](../Page/EMI加拿大.md "wikilink")

### 南美洲

  - [EMI阿根廷](../Page/EMI阿根廷.md "wikilink")
  - [EMI巴西](../Page/EMI巴西.md "wikilink")
  - [EMI墨西哥](../Page/EMI墨西哥.md "wikilink")

### 澳洲

  - [EMI澳大利亞](../Page/EMI澳大利亞.md "wikilink")

### 亞洲

  - [EMI音樂日本](../Page/EMI音樂日本.md "wikilink")（EMI Music Japan）
  - [Black board](../Page/Black_board.md "wikilink")（EMI印尼地區業務）
  - [百代唱片 (台灣)](../Page/百代唱片_\(台灣\).md "wikilink")（舊稱「科藝百代」）
  - [百代唱片 (香港)](../Page/百代唱片_\(香港\).md "wikilink")

## 旗下藝人

### 西洋

### 東洋

  - [EMI Music Japan](../Page/EMI音樂_\(日本\).md "wikilink")

### 華語

2008年3月，市場傳出**EMI**將結束[亞洲地區業務](../Page/亞洲.md "wikilink")，最終在同年中將[科藝百代](../Page/科藝百代.md "wikilink")、**步升大風**等[大中華地區業務售予](../Page/大中華.md "wikilink")[香港](../Page/香港.md "wikilink")**金牌娛樂**並合併為[金牌大風](../Page/金牌大風.md "wikilink")（該公司其後再在2010年初被商人[龐維仁旗下之](../Page/龐維仁.md "wikilink")“太平洋資本投資”收購），而其他**EMI**音像產品之代理發行權仍交由[金牌大風公司負責](../Page/金牌大風.md "wikilink")。2011年美國[環球音樂併購](../Page/環球音樂.md "wikilink")**EMI**後，**EMI**音像產品及版權正式交由各地區[環球音樂管理](../Page/環球音樂.md "wikilink")。2014年6月，[環球音樂以旗下音樂子品牌之名](../Page/環球唱片_\(台灣\).md "wikilink")，重新將**EMI**品牌引進華語樂壇，由[張惠妹擔任大中華區品牌總監](../Page/張惠妹.md "wikilink")，唯過往[科藝百代旗下歌手的歌曲和專輯版權乃歸](../Page/科藝百代.md "wikilink")[華納音樂所有](../Page/華納音樂.md "wikilink")。

(2014-)旗下藝人參見：[環球唱片
(台灣)\#旗下藝人之](../Page/環球唱片_\(台灣\)#旗下藝人.md "wikilink")**EMI
(台灣)**

## 外部連結

  - [EMI集團主頁](https://web.archive.org/web/20100521022448/http://www.emimusic.com/)
  - [EMI音樂版權主頁](https://web.archive.org/web/20090228203106/http://www.emimusicpub.com/worldwide/index.html)

## 参考文献

[E](../Category/EMI.md "wikilink")
[E](../Category/1931年成立的公司.md "wikilink")
[E](../Category/跨國公司.md "wikilink")
[Category:国际唱片业协会成员](../Category/国际唱片业协会成员.md "wikilink")
[Category:環球唱片](../Category/環球唱片.md "wikilink")
[Category:索尼音樂娛樂](../Category/索尼音樂娛樂.md "wikilink")
[Category:英国已结业公司](../Category/英国已结业公司.md "wikilink")

1.

2.

3.