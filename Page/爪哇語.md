**爪哇語**是[印尼四大主島之一的](../Page/印尼.md "wikilink")[爪哇島上東](../Page/爪哇島.md "wikilink")、中部居民主要採用的語言。爪哇語屬[南島語系中](../Page/南島語系.md "wikilink")[马来-波利尼西亚语族的一個語支](../Page/马来-波利尼西亚语族.md "wikilink")，和[印尼語及](../Page/印尼語.md "wikilink")[馬來語算是近親](../Page/馬來語.md "wikilink")，有語言人口7550萬人。不少講爪哇語的人都同時會說[印尼語](../Page/印尼語.md "wikilink")，但日常生活主要採用爪哇語。

現時爪哇語採用[拉丁字母](../Page/拉丁字母.md "wikilink")，但過去曾經採用一種很特別的[爪哇字母來記錄他們的語言](../Page/爪哇字母.md "wikilink")。

## 簡介

[Javanese_language_distribution.png](https://zh.wikipedia.org/wiki/File:Javanese_language_distribution.png "fig:Javanese_language_distribution.png")

爪哇语是一种南岛语系的语言，属于[马来-波利尼西亚语族](../Page/马来-波利尼西亚语族.md "wikilink")[核心马来-波利尼西亚语支的](../Page/核心马来-波利尼西亚语支.md "wikilink")[巽他語支](../Page/巽他語支.md "wikilink")，是[爪哇島東部](../Page/爪哇島.md "wikilink")、中部和西部北岸的主要語言。它與[馬來語](../Page/馬來語.md "wikilink")、[巽他語](../Page/巽他語.md "wikilink")、[峇里語](../Page/巴厘语.md "wikilink")、[曼德勒語等語言的關係較密切](../Page/馬都拉語.md "wikilink")，與[蘇門答臘語](../Page/蘇門答臘語.md "wikilink")、[婆羅洲諸語](../Page/婆羅洲諸語.md "wikilink")、[菲律賓語等的關係較疏](../Page/菲律賓語.md "wikilink")。

## 語音系統

現代標準爪哇語的[音素](../Page/音素.md "wikilink")。

### 元音

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/前元音.md" title="wikilink">前元音</a></p></th>
<th><p><a href="../Page/央元音.md" title="wikilink">央元音</a></p></th>
<th><p><a href="../Page/後元音.md" title="wikilink">後元音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>   </p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

### 辅音

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/唇音.md" title="wikilink">唇音</a></p></th>
<th><p><a href="../Page/齿音.md" title="wikilink">齿音</a>/<br />
<a href="../Page/齿龈音.md" title="wikilink">齿龈音</a></p></th>
<th><p><a href="../Page/卷舌音.md" title="wikilink">卷舌音</a></p></th>
<th><p><a href="../Page/硬颚音.md" title="wikilink">硬颚音</a></p></th>
<th><p><a href="../Page/软腭音.md" title="wikilink">软腭音</a></p></th>
<th><p><a href="../Page/声门音.md" title="wikilink">声门音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a>/<a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/近音.md" title="wikilink">近音</a></p></td>
<td><p><small><a href="../Page/中央辅音.md" title="wikilink">中央辅音</a></small></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small><a href="../Page/边音.md" title="wikilink">边音</a></small></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</center>

## 參考文獻

  - W. van der Molen. 1993. *Javaans schrift*. Leiden: Vakgroep Talen en
    Culturen van Zuidoost-Azië en Oceanië. ISBN 90-73084-09-1
  - S.A. Wurm and Shiro Hattori, eds. 1983. *Language Atlas of the
    Pacific Area, Part II. (Insular South-east Asia)*. Canberra.
  - P.J. Zoetmulder. 1982. *Old Javanese-English Dictionary*.
    's-Gravenhage: Martinus Nijhoff. ISBN 90-247-6178-6

## 外部連結

  - [Javanese Writing
    System](http://www.omniglot.com/writing/javanese.htm)
  - [Ethnologue report on
    Javanese](http://www.sil.org/iso639-3/documentation.asp?id=jav)
  - [Javanese in Suriname strive to preserve origins *Jakarta Post*
    article](http://home.planet.nl/~koeso002/articles/Javanese%20Suriname.htm)

[Category:马来-波利尼西亚语族](../Category/马来-波利尼西亚语族.md "wikilink")
[Category:印尼語言](../Category/印尼語言.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")