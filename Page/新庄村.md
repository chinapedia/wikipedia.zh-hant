**新庄村**（）是位于日本[岡山縣西北部](../Page/岡山縣.md "wikilink")，與[鳥取縣接壤的一](../Page/鳥取縣.md "wikilink")[村](../Page/村.md "wikilink")，為岡山縣內人口最少的行政區劃。為[日本最美麗的村莊聯盟成員之一](../Page/日本最美麗的村莊聯盟.md "wikilink")。

## 地理

  - 山：毛無山（1218m）、金谷山（1164m）、白馬山（1060m）
  - 河川：新庄川、野土路川
  - 水壩：土用壩

## 歷史

[Shinjo-juku_Waki-Honjin_-01.jpg](https://zh.wikipedia.org/wiki/File:Shinjo-juku_Waki-Honjin_-01.jpg "fig:Shinjo-juku_Waki-Honjin_-01.jpg")
過去以[出雲街道的](../Page/出雲街道.md "wikilink")[本陣和](../Page/本陣.md "wikilink")[宿場町而繁榮](../Page/宿場.md "wikilink")。

1872年8月17日設置**新庄村**至今。

## 交通

轄區內無[鐵路和](../Page/鐵路.md "wikilink")[快速道路通過](../Page/快速道路.md "wikilink")，距離最近的[車站為位於](../Page/車站.md "wikilink")[鳥取縣](../Page/鳥取縣.md "wikilink")[日野郡](../Page/日野郡.md "wikilink")[日野町的](../Page/日野町.md "wikilink")[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[伯備線](../Page/伯備線.md "wikilink")[根雨車站](../Page/根雨車站.md "wikilink")

## 觀光資源

[Triumphal_Return_Cherry_Blossom_Street.jpg](https://zh.wikipedia.org/wiki/File:Triumphal_Return_Cherry_Blossom_Street.jpg "fig:Triumphal_Return_Cherry_Blossom_Street.jpg")

  - 凱旋櫻道路：為1906年[日俄戰爭日本戰勝後](../Page/日俄戰爭.md "wikilink")，再舊出雲街道宿場町兩側種植的櫻花樹的通稱，每年開花期間會同時舉辦「凱旋櫻花祭」。
  - 新庄宿

## 姊妹、友好都市

### 日本

1986年8月三個同樣名為「新庄」的行政區劃簽訂友好自治体盟約。

  - [新庄市](../Page/新庄市.md "wikilink")（[山形縣](../Page/山形縣.md "wikilink")）
  - [新庄町](../Page/新庄町.md "wikilink")（[奈良縣](../Page/奈良縣.md "wikilink")[北葛城郡](../Page/北葛城郡.md "wikilink")）：已於2004年合併為[葛城市](../Page/葛城市.md "wikilink")

## 外部連結

  - [ひめっ子 Blog](http://himekkolog.exblog.jp/)

  - [ひめっ子 Facebook](http://www.facebook.com/himekko.shinjo)