**TurboLinux**是一個[Linux發行版本](../Page/Linux發行版本.md "wikilink")。其主要市場為採用的[亞洲市場](../Page/亞洲.md "wikilink")，如[日本](../Page/日本.md "wikilink")、[中國大陸和](../Page/中華人民共和國.md "wikilink")[韓國](../Page/大韓民國.md "wikilink")。\[1\]

## 背景

TurboLinux公司由克里夫·米勒和艾里什·米勒兄弟於1992年成立於美國[猶他州](../Page/猶他州.md "wikilink")[桑迪市](../Page/桑迪_\(猶他州\).md "wikilink")，原名Pacific
HiTech。1999年6月，Pacific HiTech為讓用戶能夠更好地識別其旗艦產品，遂更名為TurboLinux。\[2\]

2000年7月，米勒兄弟因與公司的與[風險投資}創投家產生重大分歧](../Page/風險投資}創投家.md "wikilink")，被公司以“無原因”為由終止關係。\[3\]\[4\]在此之後，TurboLinux公司經歷了幾輪裁員，最終於2002年9月將其品牌名稱與發行版本出售給日本軟件公司SRA。\[5\]其位於[加州布里斯班市的總部被迫關閉](../Page/布里斯班_\(加利福尼亞州\).md "wikilink")。所有運營業務轉移至日本[東京都](../Page/東京都.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")。\[6\]

2002年，TurboLinux正式加入[United
Linux聯盟](../Page/United_Linux.md "wikilink")。\[7\]

2005年9月，TurboLinux在大阪證券交易所正式[公開上市](../Page/首次公開募股.md "wikilink")。由、[活力門證券股份有限公司與](../Page/活力門.md "wikilink")[瑞穗金融集團承保](../Page/瑞穗金融集團.md "wikilink")。

## 軟體

早期版本的TurboLinux基於2.6版本的[Linux內核](../Page/Linux內核.md "wikilink")，現今則主要從[Red
Hat發行版衍生而成](../Page/Red_Hat.md "wikilink")。由於[Ubuntu](../Page/Ubuntu.md "wikilink")、[Fedora和](../Page/Fedora.md "wikilink")[CentOS等知名Linux發行版得天獨厚的優勢](../Page/CentOS.md "wikilink")，TurboLinux的使用率相對低迷\[8\]。

早期版本的TurboLinux曾經包含[專有組件](../Page/專有軟體.md "wikilink")，但在之後的版本中被分離出去並專門設立非自由軟體倉庫。早期版本的TurboLinux採用由[Anaconda衍生的Mongoose](../Page/Anaconda.md "wikilink")[安裝程式](../Page/安裝程式.md "wikilink")\[9\]，之後的版本則直接採用Anaconda安裝程式。

由於商標許可的緣故，[Firefox](../Page/Firefox.md "wikilink") 3.0衍生的Turbolinux
WebNavi被設定為TurboLinux的缺省[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，[媒體播放器則為Kaffein衍生的媒體播放器](../Page/媒體播放器.md "wikilink")（支持[Windows
Media媒體格式](../Page/Windows_Media.md "wikilink")，但不支持[DVD和](../Page/DVD.md "wikilink")[DRM](../Page/DRM.md "wikilink")）。TurboLinux同時也擁有自己的[硬盤分區工具TFDisk](../Page/硬盤分區.md "wikilink")。

## 教育培訓中心

Turbolinux在[中國大陸專設有Linux教育培訓中心](../Page/中國大陸.md "wikilink")，正式命名為**“拓林思
(中國)
教育培訓中心”**，是中國大陸首家正式開展Linux培訓的機構。該機構聲稱曾為[IBM](../Page/IBM.md "wikilink")、[惠普](../Page/惠普公司.md "wikilink")、[戴爾](../Page/戴爾.md "wikilink")、[英特爾](../Page/英特爾.md "wikilink")、[聯想](../Page/聯想.md "wikilink")、[宏碁](../Page/宏碁.md "wikilink")、[浪潮](../Page/浪潮.md "wikilink")、[曙光](../Page/曙光.md "wikilink")、[搜狐等](../Page/Sohu.md "wikilink")[IT企業培養大批Linux技術人才](../Page/IT.md "wikilink")，並向中國大陸的各機關部門及公司（如[中國氣象局](../Page/中國氣象局.md "wikilink")、[國家質檢總局](../Page/中華人民共和國國家質量監督檢驗檢疫總局.md "wikilink")、[鐵道部](../Page/中華人民共和國鐵道部.md "wikilink")、[中國石油化工集團](../Page/中國石油化工集團.md "wikilink")、[中國人民銀行](../Page/中國人民銀行.md "wikilink")、[中國工商銀行](../Page/中國工商銀行.md "wikilink")、[中國農業銀行](../Page/中國農業銀行.md "wikilink")、[中國電信](../Page/中國電信.md "wikilink")、[中興通訊等](../Page/中興通訊.md "wikilink")）提供專業的企業定製培訓。

## 參見

  - [Linux发行版列表](../Page/Linux发行版列表.md "wikilink")

## 参考資料

## 外部連結

  - [TurboLinux官網](http://www.turbolinux.com/)

[Category:Linux發行版](../Category/Linux發行版.md "wikilink")

1.  <http://www.desktoplinux.com/news/NS9651224512.html>, archived at
    <https://web.archive.org/web/20120415163951/http://desktoplinux.com/news/NS9651224512.html>
2.
3.
4.
5.  [SRA buys Turbolinux product and
    name](http://www.turbolinux.com/company/news/2002/020820.html)
6.
7.  <http://www.techworld.com/opsys/news/index.cfm?NewsID=3582>
8.  [Linux基金會](../Page/Linux基金會.md "wikilink")[2007年桌面/客戶端調查結果](https://wiki.linuxfoundation.org/desktop/2007clientsurvey)
9.  [Turbolinux GUI インストーラ Mongoose
    (マングース：コードネーム)](http://web-old.turbolinux.co.jp/press_room/material/mongoose/)