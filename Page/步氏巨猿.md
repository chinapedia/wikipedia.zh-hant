**步氏巨猿**（[学名](../Page/学名.md "wikilink")：**）是一種已[滅絕的](../Page/滅絕.md "wikilink")[猿](../Page/猿.md "wikilink")。

## 特徵

[缩略图](../Page/文件:Gigantopithecus.jpeg.md "wikilink")
步氏巨猿的[化石只有在](../Page/化石.md "wikilink")[東南亞發現的少量](../Page/東南亞.md "wikilink")[牙齒及](../Page/牙齒.md "wikilink")[下頜骨](../Page/下頜骨.md "wikilink")。牠的身型比現存的[大猩猩更為大](../Page/大猩猩.md "wikilink")，因沒有更多的[骨骼化石而未能作出確實計算](../Page/骨骼.md "wikilink")，而[古人類學家根據化石紀錄推測成年](../Page/古人類學家.md "wikilink")[巨猿超過](../Page/巨猿.md "wikilink")高，重。這些化石可追溯至約100萬年前，而牠們在10萬年前[滅絕](../Page/滅絕.md "wikilink")。這可見牠們與現今[人類共存約幾千年](../Page/人類.md "wikilink")。\[1\]

步氏巨猿分佈在[亞洲](../Page/亞洲.md "wikilink")，由於其化石與已滅絕的[熊貓祖先一同被發現](../Page/熊貓.md "wikilink")，故估計牠可能棲息在[竹林中](../Page/竹.md "wikilink")。大部份證據都指向巨猿是吃[草食性的](../Page/草食性.md "wikilink")。一些學者相信步氏巨猿是因與人類競爭而步向滅亡的。

由於只發現了少量的步氏巨猿的化石，故不得而知牠的外觀。牠有可能像現今的大猩猩，但是一些學者認為牠可能更似其近親的[红猩猩](../Page/红猩猩.md "wikilink")。按照巨猿的巨大的身型，相信成年的牠們並沒有甚麼天敵。但幼少、弱小或受傷的巨猿可能會被[虎及](../Page/虎.md "wikilink")[直立人攻擊](../Page/直立人.md "wikilink")。

因為沒有發現巨猿的[盆骨或肢骨](../Page/盆骨.md "wikilink")，對牠的行走方式並不清楚。主流意見是牠們以四足行走，像大猩猩及[黑猩猩](../Page/黑猩猩.md "wikilink")。但是，小部份意見，如[格羅佛·克蘭茨](../Page/格羅佛·克蘭茨.md "wikilink")（Grover
Krantz）指牠們是雙足行走的。他們發現其下頜骨呈U形及向後闊大，足以容納[氣管](../Page/氣管.md "wikilink")，而[頭顱骨則可以像人類般由直立的](../Page/頭顱骨.md "wikilink")[脊柱所支撐](../Page/脊柱.md "wikilink")，而非在像類人猿般是在脊柱之前。

## 分類

以往基於其[臼齒](../Page/臼齒.md "wikilink")，認為步氏巨猿是[人類的祖先](../Page/人類.md "wikilink")。但這現已被認為是[趨同演化的結果](../Page/趨同演化.md "wikilink")。步氏巨猿現時與[红猩猩被分類在](../Page/红猩猩.md "wikilink")[猩猩亞科](../Page/猩猩亞科.md "wikilink")。

## 神秘生物

有指[雪人或](../Page/雪人_\(傳說生物\).md "wikilink")[大腳就是步氏巨猿](../Page/大腳.md "wikilink")。但由於沒有證據，故這一直都指是推測。由於步氏巨猿有可能與現今[人類一同生活了一段時間](../Page/人類.md "wikilink")\[2\]，故這類生物有可能是經過千百年的傳說。\[3\]

## 參考

## 外部連結

  - [University of Iowa Museum of Natural History: How Gigantopithecus
    was
    discovered](https://web.archive.org/web/20071012201552/http://www.uiowa.edu/~nathist/Site/giganto.html)
  - [From the Teeth of the Dragon: Gigantopithecus
    Blacki](http://www.wynja.com/arch/gigantopithecus.html)
  - [The Bigfoot Giganto
    Theory](http://www.bfro.net/REF/THEORIES/MJM/whatrtha.asp)
  - [Bigfoot Research Organization](http://www.bfro.net)

[en:Gigantopithecus](../Page/en:Gigantopithecus.md "wikilink")

[Category:巨猿屬](../Category/巨猿屬.md "wikilink")

1.

2.
3.