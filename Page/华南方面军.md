**華南方面軍**（）是[大日本帝國陸軍的一個](../Page/大日本帝國陸軍.md "wikilink")[方面軍](../Page/方面軍.md "wikilink")。[昭和](../Page/昭和.md "wikilink")15年（1940年）2月9日編制成軍，編入[中国派遣軍](../Page/中国派遣軍.md "wikilink")[作戰序列](../Page/作戰序列.md "wikilink")，同年6月22日廣九作戰之後，自7月23日起成為[大本營直轄軍](../Page/大本營_\(大日本帝國\).md "wikilink")，9月5日入侵[法屬印度支那北部](../Page/法屬印度支那.md "wikilink")，翌年[昭和](../Page/昭和.md "wikilink")16年（1941年）3月3日進行[雷州半島方面作戰](../Page/雷州半島.md "wikilink")，4月19日進行福州作戰，同年6月28日廢軍。[司令部轄有](../Page/司令部.md "wikilink")[防疫給水部](../Page/防疫給水部.md "wikilink")，廣州第8604部隊又稱呼為*波第8604部隊*。

## 基本情報

  - [通稱號](../Page/通稱號.md "wikilink")：波
  - 編成時期：[昭和](../Page/昭和.md "wikilink")15年（1940年）2月9日
  - 廢止時期：[昭和](../Page/昭和.md "wikilink")16年（1941年）6月28日
  - 廢軍時之上級部隊：[大本營直隷](../Page/大本營_\(大日本帝國\).md "wikilink")
  - 最終位置：[廣州](../Page/廣州市.md "wikilink")

## 华南方面軍之人事

### 司令官

  - [安藤利吉中將](../Page/安藤利吉.md "wikilink")（[陸士](../Page/日本陸軍士官學校.md "wikilink")16期：[昭和](../Page/昭和.md "wikilink")15年（1940年）2月10日～昭和15年10月5日）
  - [後宮淳中將](../Page/後宮淳.md "wikilink")（陸士17期：[昭和](../Page/昭和.md "wikilink")15年（1940年）10月5日～昭和16年6月28日）

### 参謀長

  - 根本博少將（[陸士](../Page/日本陸軍士官學校.md "wikilink")23期：[昭和](../Page/昭和.md "wikilink")15年（1940年）2月10日～昭和16年3月1日）
  - 加藤鑰平少將（陸士25期：[昭和](../Page/昭和.md "wikilink")16年（1941年）3月1日～昭和16年6月28日）

### 參謀副長

  - [佐藤賢了大佐](../Page/佐藤賢了.md "wikilink")（[陸士](../Page/日本陸軍士官學校.md "wikilink")29期：[昭和](../Page/昭和.md "wikilink")15年（1940年）2月10日～昭和16年2月5日）
  - 樋口敬七郎大佐（陸士27期：[昭和](../Page/昭和.md "wikilink")16年（1941年）2月5日～昭和16年6月28日）

### 高級參謀

  - 今田新太郎（[陸士](../Page/日本陸軍士官學校.md "wikilink")30期：[昭和](../Page/昭和.md "wikilink")15年（1940年）2月10日～昭和15年3月9日）
  - 藤原武（陸士31期：[昭和](../Page/昭和.md "wikilink")15年（1940年）3月9日～昭和15年12月24日）

## 最終隷下部隊

  - [印度支那派遣軍](../Page/印度支那派遣軍.md "wikilink")
  - [近衛師團](../Page/近衛師團.md "wikilink")
  - [第18師團](../Page/第18師團.md "wikilink")
  - [第38師團](../Page/第38師團.md "wikilink")
  - [第104師團](../Page/第104師團.md "wikilink")
  - 獨立混成第19旅團
  - 第1獨立歩兵隊

## 關連項目

  - [軍事單位](../Page/軍事單位.md "wikilink")
  - [第22軍](../Page/第22軍_\(日本陸軍\).md "wikilink")
  - [第23軍](../Page/第23軍_\(日本陸軍\).md "wikilink")
  - [华北方面軍](../Page/华北方面軍.md "wikilink")
  - [华中方面軍](../Page/华中方面軍.md "wikilink")
  - [中国駐屯軍](../Page/中国駐屯軍.md "wikilink")
  - [大日本帝國陸軍師團列表](../Page/大日本帝國陸軍師團列表.md "wikilink")

## 參考文獻

  - [秦郁彦編](../Page/秦郁彦.md "wikilink")『[日本陸海軍總合事典](../Page/日本陸海軍總合事典.md "wikilink")』第2版、[東京大學出版會](../Page/東京大學出版會.md "wikilink")、2005年。
  - 外山操・森松俊夫編著『[帝國陸軍編制總覽](../Page/帝國陸軍編制總覽.md "wikilink")』[芙蓉書房出版](../Page/芙蓉書房出版.md "wikilink")、1987年。

## 外部連結

  -
  - [帝國陸軍～その制度と人事～](https://web.archive.org/web/20080411094145/http://imperialarmy.hp.infoseek.co.jp/)

  - [日本陸海軍事典](https://web.archive.org/web/20100516180653/http://homepage1.nifty.com/kitabatake/rikukaiguntop.html)

[Category:方面軍 (日本陸軍)](../Category/方面軍_\(日本陸軍\).md "wikilink")