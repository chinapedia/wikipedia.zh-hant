### 合理使用

本圖以**合理使用**條目使用，理由為：

1.  本圖**並非**僅僅作為對該人長相的辨識、
2.  本圖是一幅獨一無二的**官式圖片**，放於相關的「Office Info Box
    Template」內，描述楊鐵樑**作為首席按察司**之官位的辨識、
3.  楊鐵樑已不再任首席按察司，其衣著服飾僅可於首席按察司任內穿戴，因此**不可能有自由版權圖片存在**、
4.  楊鐵樑是**首位華人首席按察司**，此身份對香港歷史和政治等方面有重要意義、
5.  圖片僅用於教育用途，存放在位於美國，非牟利的維基基金會伺服器內，以及
6.  條目質素在加入圖片後得到大大提高。

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>楊鐵樑爵士在<a href="../Page/1988年.md" title="wikilink">1988年至</a><a href="../Page/1996年.md" title="wikilink">1996年擔任首席大法官任內之</a><strong>官式圖片</strong><br />
頸前所戴為下級勳位爵士勳章，其餘領環與胸前星章皆汶萊爵位之裝飾。</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.csb.gov.hk/hkgcsb/csn/csn59/59c/issue6pg1.html"><a href="http://www.csb.gov.hk/hkgcsb/csn/csn59/59c/issue6pg1.html">http://www.csb.gov.hk/hkgcsb/csn/csn59/59c/issue6pg1.html</a></a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>c. 1988年至1996年期間</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p><a href="../Page/香港政府.md" title="wikilink">香港政府</a></p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p><br />
 {{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>