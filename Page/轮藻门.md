**輪藻門**是[綠藻的一門](../Page/綠藻.md "wikilink")，包含了最親近[有胚植物的親戚](../Page/有胚植物.md "wikilink")。因為排除了有胚植物，輪藻門是個[併系群](../Page/併系群.md "wikilink")（然而有時會限定成單純只有[輪藻目](../Page/輪藻目.md "wikilink")，其為單系群）。

藻体构造较复杂，有类似根、茎、叶的分化，大小约为10－50厘米，外形很像[金鱼藻](../Page/金鱼藻.md "wikilink")；“茎”节上轮生侧“枝”，“枝”上具有“叶”的生殖器官；[有性生殖为卵式生殖](../Page/有性生殖.md "wikilink")；卵囊生于“叶”腋中，通常呈卵形，外有5个螺旋状缠绕的管细胞，在顶端形成5个冠细胞，卵囊初为绿色，成熟时为深褐色；球形精子囊生于卵囊下面，外有8个盾形细胞，初为绿色，成熟后为橘红色。

轮藻属都没有无性生殖，而进行[卵配生殖](../Page/卵配生殖.md "wikilink")。雌、雄生殖器官结构复杂，具藏精器和藏卵器，由两个遗传性、形状、大小和结构等方面都不相同的配子融合。轮藻的营养体、生殖器官和轮藻细胞的有丝分裂皆与陆生的植物相似\[1\]。

## 參考文獻

<references />

## 另見

  - [鏈形植物](../Page/鏈形植物.md "wikilink")

[輪藻門](../Page/分類:輪藻門.md "wikilink") [L](../Page/分類:綠藻.md "wikilink")

1.