**聂卫平**（），[河北](../Page/河北.md "wikilink")[深县](../Page/深县.md "wikilink")（今[深州市](../Page/深州市.md "wikilink")）人。[中国围棋协会副主席兼技术委员会主任](../Page/中国围棋协会.md "wikilink")，[中国棋院技术顾问](../Page/中国棋院.md "wikilink")。1982年被授予[九段](../Page/围棋九品制.md "wikilink")，1988年3月26日被中国围棋协会授予围棋“棋圣”称号。1999年被评为“新中国棋坛十大杰出人物”。1980年代末在四届[中日围棋擂台赛中](../Page/中日围棋擂台赛.md "wikilink")11连胜。这个时期是他个人巅峰时期，其胜利也为围棋在中国大陆的普及产生了重要影响。

## 国内比赛成绩

  - 1975年、1977年-79年、1981年、1983年全国个人赛冠军
  - 1979年-83年、1988年-90年新体育杯冠军
  - 1981年国手战冠军
  - 1987年-89年全国十强赛冠军
  - 1990年王座赛冠军、国手赛冠军、全国八强赛冠军
  - 第2届棋王，第5、6届中国天元
  - 1995年宝胜电缆杯冠军
  - 1998年海天杯元老赛冠军
  - 2003年与[于梅岭搭档获全国围棋混双赛冠军](../Page/于梅岭.md "wikilink")

## 世界大賽決賽全紀錄

  - 1989年 [應氏杯](../Page/應氏杯.md "wikilink") 2-3 負
    [曹薰鉉](../Page/曹薰鉉.md "wikilink")
  - 1990年 [富士通杯](../Page/富士通杯.md "wikilink") 負
    [林海峰](../Page/林海峰.md "wikilink")
  - 1995年 [東洋證券杯](../Page/東洋證券杯.md "wikilink") 1-3 負
    [馬曉春](../Page/馬曉春.md "wikilink")

## 国际比赛成绩

| 赛事                                        | 1988 | 1989  | 1990 | 1991 | 1992 | 1993  | 1994 | 1995 | 1996  | 1997  | 1998 | 1999 | 2000 | 2001 | 2002 | 2003 | 2004 | 2005  | 2006 | 2007 | 2008 | 2009 | 2010 | 2011 | 2012 |
| ----------------------------------------- | ---- | ----- | ---- | ---- | ---- | ----- | ---- | ---- | ----- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| [应氏杯](../Page/应氏杯.md "wikilink")          | 亚军   | \-    | ×    | \-   | ×    | \-    | ×    | \-   | ×     | \-    | ×    | \-   | ×    |      |      |      |      |       |      |      |      |      |      |      |      |
| [富士通杯](../Page/富士通杯.md "wikilink")        | 季军   | *16强* | 亚军   | ×    | 8强   | *16强* | ×    | 16强  | *16强* | ×     | ×    | ×    | ×    | ×    | ×    | ×    | ×    | ×     | ×    | ×    | ×    | ×    | ×    | ×    | *停办* |
| [三星杯](../Page/三星杯.md "wikilink")          | \-   | 16强   | 16强  | 16强  | ×    | ×     | ×    | ×    | ×     | *32强* | ×    | ×    | ×    | ×    | ×    | ×    | ×    | *32强* |      |      |      |      |      |      |      |
| [LG盃](../Page/LG盃.md "wikilink")          | \-   | 16强   | ×    | ×    | ×    | ×     | ×    | ×    | ×     | ×     | ×    | ×    | ×    | ×    | ×    | ×    | ×    | ×     |      |      |      |      |      |      |      |
| [春兰杯](../Page/春兰杯.md "wikilink")          | \-   | *16强* | ×    | ×    | \-   | ×     | \-   | ×    | \-    | ×     | \-   | ×    | \-   | ×    | \-   | ×    |      |       |      |      |      |      |      |      |      |
| [东洋杯](../Page/東洋證券杯世界圍棋錦標賽.md "wikilink") | \-   | 8强    | \-   | 4强   | 4强   | 亚军    | 8强   | \-   | *16强* | ×     | *停办* |      |      |      |      |      |      |       |      |      |      |      |      |      |      |
| [亚洲杯](../Page/亚洲杯.md "wikilink")          | \-   | 4强    | ×    | ×    | *1轮* | 4强    | ×    | *1轮* | ×     | 4强    | ×    | ×    | ×    | ×    | ×    | ×    | ×    | ×     | ×    | ×    | ×    | ×    | ×    | ×    | ×    |
|                                           |      |       |      |      |      |       |      |      |       |       |      |      |      |      |      |      |      |       |      |      |      |      |      |      |      |

  - 注：*斜体字*意味着进入本赛后未赢一场

<!-- end list -->

  - 1985年第一届中日擂台赛3连胜
  - 1986年第二届中日擂台赛5连胜
  - 1987年第三届中日擂台赛1胜
  - 1988年第四届中日擂台赛2连胜，避免被剃光头。
  - 第5届中日天元战优胜

## 弟子

[常昊九段](../Page/常昊.md "wikilink")、[周鹤洋九段](../Page/周鹤洋.md "wikilink")、[古力九段](../Page/古力.md "wikilink")、[刘菁八段](../Page/刘菁.md "wikilink")、[王磊八段](../Page/王磊.md "wikilink")、[刘世振七段](../Page/刘世振.md "wikilink")、[王煜辉七段](../Page/王煜辉.md "wikilink")、[鲁佳二段](../Page/鲁佳.md "wikilink")、[唐莉初段](../Page/唐莉.md "wikilink")

## 个人生活

聂卫平经历过三次婚姻：1980年3月与[孔祥明结婚](../Page/孔祥明.md "wikilink")，育有一子[孔令文](../Page/孔令文.md "wikilink")（原名聂云骢）；1991年离婚，与歌唱演员[王静结婚](../Page/王静.md "wikilink")，随即次子聂云青出生，八年后离婚；2001年与比他年少二十三岁的兰莉娅结婚，2004年育有一女聂云菲。

2013年8月因患[直肠癌住院治疗](../Page/直肠癌.md "wikilink")，11月初进行手术\[1\]。

2014年4月13日下午，湖北洪湖三民队征战2014围甲联赛新闻发布会在武汉琴台大剧院举行，棋圣聂卫平时隔9个月首次公开亮相。

## 爭議言行

  - 我站在高山峻嶺之上\[2\]
  - 我可以輕取李昌鎬\[3\]
  - 李世石有什麼好欣賞\[4\]
  - 宋泰坤不怎麼會下棋\[5\]
  - 陳耀燁你牛你牛\[6\]
  - Master布局不行\[7\]

## 主要著作

  - 《我的围棋之路》
  - 《聂卫平自战百局》
  - 《聂卫平揭秘围棋大局观》

## 参考文献

[N聂](../Category/中國圍棋棋手.md "wikilink")
[N聂](../Category/中国棋院围棋棋手.md "wikilink")
[N聂](../Category/深州人.md "wikilink") [W](../Category/聂姓.md "wikilink")
[Category:中国围棋协会副主席](../Category/中国围棋协会副主席.md "wikilink")

1.
2.  <http://sports.sina.com.cn/s/2005-06-15/0705589179s.shtml>
3.  <https://www.youtube.com/watch?v=G8Q-pkiTT-k>
4.  <http://sports.sina.com.cn/s/2005-06-15/0705589179s.shtml>
5.  <http://sports.sina.com.cn/s/2005-06-15/0705589179s.shtml>
6.  <http://sports.sina.com.cn/go/2013-06-20/16446629874.shtml>
7.  <https://wxn.qq.com/cmsid/SPO2017010403578305>