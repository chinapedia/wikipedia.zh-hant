于[岐阜縣由觀光客及縣民公開評選](../Page/岐阜縣.md "wikilink")**岐阜縣之櫻花**，在2003年3月選出**飛騨、美濃櫻花三十三選**。

## 33選一覽

1.  [岐阜公園](../Page/岐阜公園.md "wikilink")·長良川堤·鵜飼櫻（[岐阜市大宮町](../Page/岐阜市.md "wikilink")·御手洗　[岐阜護國神社内](../Page/岐阜護國神社.md "wikilink")）
2.  [中將姫誓願櫻](../Page/中將姫誓願櫻.md "wikilink")（岐阜市大洞1丁目2-14　[願成寺境内](../Page/願成寺_\(岐阜市\).md "wikilink")）
3.  [奥之細道末尾之地](../Page/奥之細道.md "wikilink")（平行[大垣市船町水門川](../Page/大垣市.md "wikilink")）
4.  [飛騨高山中橋周邊](../Page/中橋_\(宮川\).md "wikilink")（[高山市上三之町](../Page/高山市.md "wikilink")）
5.  城山公園（高山市城山）
6.  [虎溪公園](../Page/虎溪公園.md "wikilink")（[多治見市辯天町](../Page/多治見市.md "wikilink")）
7.  吉田川之櫻（[關市吉田川畔](../Page/關市.md "wikilink")）
8.  苗木櫻花公園（[中津川市苗木](../Page/中津川市.md "wikilink")）
9.  小倉公園（[美濃市泉町](../Page/美濃市.md "wikilink")）
10. [新境川](../Page/新境川.md "wikilink")·[百十郎櫻](../Page/百十郎櫻.md "wikilink")（[各務原市那加櫻町](../Page/各務原市.md "wikilink")）
11. [奈良津堤](../Page/奈良津堤.md "wikilink")
    之櫻（[羽島郡](../Page/羽島郡.md "wikilink")[笠松町奈良津](../Page/笠松町.md "wikilink")）
12. 羽根谷台階公園（[海津市](../Page/海津市.md "wikilink")[南濃町羽澤](../Page/南濃町.md "wikilink")）
13. [養老公園](../Page/養老公園.md "wikilink")（[養老郡](../Page/養老郡.md "wikilink")[養老町養老](../Page/養老町.md "wikilink")）
14. [墨俣一夜城](../Page/墨俣一夜城.md "wikilink")·[犀川堤之櫻](../Page/犀川_\(岐阜縣\).md "wikilink")（大垣市[墨俣町墨俣](../Page/墨俣町.md "wikilink")）
15. [谷汲山門前之櫻](../Page/華嚴寺_\(谷汲德積\).md "wikilink")（[揖斐郡](../Page/揖斐郡.md "wikilink")[揖斐川町](../Page/揖斐川町.md "wikilink")[谷汲德積](../Page/谷汲村.md "wikilink")）
16. [揖斐二度櫻](../Page/揖斐二度櫻.md "wikilink")（揖斐郡[大野町南方二度櫻](../Page/大野町.md "wikilink")）
17. [霞間溪](../Page/霞間溪.md "wikilink")（揖斐郡[池田町藤代](../Page/池田町_\(岐阜縣\).md "wikilink")）
18. [淡墨櫻](../Page/淡墨櫻.md "wikilink")（[本巢市](../Page/本巢市.md "wikilink")[根尾板所](../Page/根尾村.md "wikilink")995）
19. 寺尾原千本櫻公園（關市武藝川町寺尾）
20. 愛宕公園（[郡上市](../Page/郡上市.md "wikilink")[八幡町島谷](../Page/八幡町_\(岐阜縣\).md "wikilink")）
21. 明建神社（郡上市大和町牧）
22. 藤路之櫻（郡上市白鳥町向小駄良藤路）
23. [國道156號線兩旁林蔭櫻樹](../Page/國道156號.md "wikilink")（郡上市[美並町深戸地内](../Page/美並村.md "wikilink")）
24. ひよもの枝垂櫻（[惠那市串原](../Page/惠那市.md "wikilink")5323番地）
25. 新田櫻（惠那市上矢作町2858番地）
26. 凈福寺枝垂櫻花（[下呂市](../Page/下呂市.md "wikilink")[小坂町小坂](../Page/小坂町_\(岐阜縣\).md "wikilink")　凈福寺境内）
27. 苗代櫻（下呂市和佐）
28. [下呂站周邊櫻花下呂市幸田](../Page/下呂站.md "wikilink")）
29. [莊川櫻](../Page/莊川櫻.md "wikilink")（高山市[莊川町中野](../Page/莊川村.md "wikilink")）
30. おおた櫻（[大野郡](../Page/大野郡_\(岐阜縣\).md "wikilink")[白川村荻町](../Page/白川村.md "wikilink")　本覺寺境内）
31. [臥龍櫻](../Page/臥龍櫻.md "wikilink")（高山市[一之宮町山下](../Page/宮村.md "wikilink")）
32. 藥師堂枝垂櫻花（高山市[朝日町万石](../Page/朝日村_\(岐阜縣\).md "wikilink")）
33. 櫻野公園（高山市[國府町廣瀬町櫻野](../Page/國府町_\(岐阜縣\).md "wikilink")）

## 相關條目

  - [飛驒、美濃紅葉三十三選](../Page/飛驒、美濃紅葉三十三選.md "wikilink")

[Category:名数33](../Category/名数33.md "wikilink")
[Category:岐阜縣觀光地](../Category/岐阜縣觀光地.md "wikilink")
[Category:櫻花](../Category/櫻花.md "wikilink")