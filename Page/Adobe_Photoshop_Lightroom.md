**Adobe Photoshop Lightroom** 是由[Adobe
Systems公司发布的一款](../Page/Adobe_Systems.md "wikilink")[软件](../Page/软件.md "wikilink")，旨在帮助专业[摄影师的](../Page/摄影师.md "wikilink")[后期制作](../Page/后期制作.md "wikilink")。它可以同时在[OS
X和](../Page/OS_X.md "wikilink")[Microsoft
Windows系统上运行](../Page/Microsoft_Windows.md "wikilink")，2006年1月9日发布Mac系统试用版（Beta），7月18日发布Windows系统的试用版本，经过4个版本的测试以后，于2007年2月正式发布。它是[Photoshop产品系列的一部分](../Page/Photoshop.md "wikilink")，这个系列还包括Photoshop
CC和Photoshop Elements。 2017年開始Adobe Photoshop Lightroom CC更名為Lightroom
Classic
CC，並推出可跨桌面、行動裝置和網頁運作，使用[雲端相片服務的Lightroom](../Page/雲端.md "wikilink")
CC。

## 测试开发

Lightroom早在2002年就开始開发，早期的[开发代码为Shadowland](../Page/開發代號.md "wikilink")。而值得一提的是，该程式大约40%是使用[Lua](../Page/Lua.md "wikilink")[脚本语言编写的](../Page/脚本语言.md "wikilink")。2006年1月9日，Photoshop
Lightroom的一个早期版本——直接命名为Lightroom的[苹果电脑](../Page/苹果电脑.md "wikilink")[麦金塔系统版本在Adobe](../Page/麦金塔.md "wikilink")
Labs网站上发布\[1\]。这是Adobe公司第一次向公众发布[公开测试版本征求反馈意见](../Page/公开测试.md "wikilink")。这个方法后来也被用于Adobe
Photoshop CS3版本的开发。之后测试版本不断更新，2006年7月18日公布*Beta
3*，开始加入对[微软](../Page/微软.md "wikilink")[视窗系统的支持](../Page/视窗.md "wikilink")。9月25日公布*Beta
4*，将其加入Photoshop产品家族，之后的10月19日公布了局部修改的4.1[测试版本](../Page/测试版本.md "wikilink")。

## 正式发布

2007年1月29日，Adobe公司宣布Lightroom将于2007年2月19日上市，公开零售价格是299[美元](../Page/美元.md "wikilink")。第一次同期发布的除了[英语版本以外](../Page/英语.md "wikilink")，还有[法语](../Page/法语.md "wikilink")、[德语和](../Page/德语.md "wikilink")[日语版本](../Page/日语.md "wikilink")。

## 版本

### 3.0

Photoshop Lightroom 3.0 測試版本於2009年10月22日發佈。新功能如下：

  - 新的[雜訊控制功能](../Page/雜訊.md "wikilink")
  - 改善敏銳化相片功能
  - 新的匯入相片模組
  - 能加入[浮水印](../Page/浮水印.md "wikilink")
  - 使相片顆粒化
  - 發佈功能
  - 自行選擇列印包

Photoshop Lightroom 3.0 第二個測試版本於2010年3月23日發佈。新功能如下：

  - 新的光度雜訊控制功能
  - 支援部份[Nikon](../Page/Nikon.md "wikilink") and
    [Canon相機作拴繩拍攝](../Page/佳能.md "wikilink")（Tethered
    shooting）
  - 基本影片支援Basic video file support
  - 點曲線

Photoshop Lightroom 3.0
正式版本於2010年6月8日發佈，而此版本並無加進其他主要的新功能。所有在測試版本中包含的功能亦有包括內，新增了小功能如鏡頭修正、透視變換、一些功能上的改進及整體優化。

### 4.0

Photoshop Lightroom 4.0 版本於2012年3月5日發佈。由此版本開始，Lightroom 不再支援微軟作業系統
XP（[Windows XP](../Page/Windows_XP.md "wikilink")）。 新功能如下：

  - 重建強光及陰影，以帶出在黑影及強光中的細節
  - 以內建模版建立相簿
  - 以拍攝地點來組織相片，將拍攝地點加到相片，並能顯示支援[GPS相機的資料](../Page/GPS.md "wikilink")
  - 用[白平衡點選工具去微調相片局部的白平衡](../Page/白平衡.md "wikilink")
  - 更多選項去調整雜訊及移除相片局部的moiré
  - 額外支援影片組織、瀏覽、微調及編輯
  - 影片輸出工具以編輯及分享影片至[Facebook及](../Page/Facebook.md "wikilink")[Flickr](../Page/Flickr.md "wikilink")
  - 能預覽從有顏色調整的[打印機所打印的相片效果](../Page/打印機.md "wikilink")
  - 從Lightroom直接發送[電郵](../Page/電郵.md "wikilink")

### 5.0

Adobe Photoshop Lightroom 5.0 于2013年6月9日正式公开发布，其中于2013年4月15日发布过测试版。\[2\]
此版本需要[OSX](../Page/OSX.md "wikilink") 10.7或更高, [Windows
7或](../Page/Windows_7.md "wikilink")[Windows
8](../Page/Windows_8.md "wikilink")。 功能改进如下:

  - 径向渐变可以使用椭圆形绘图
  - 高级修复/克隆笔刷可与斑点去除工具用在同一区域
  - 智能预览可允许离线查看图像修改结果
  - 书本模块中能保存自定义输出
  - [PNG文件支持](../Page/PNG.md "wikilink")
  - 在幻灯片模式下支持视频文件
  - 其他更新，包含自动透视校正并增强智能集合

## 工作流程模式和特征

  - 图片库：图片收藏管理
  - 开发：[RAW和](../Page/RAW.md "wikilink")[JPEG文件的无损编辑](../Page/JPEG.md "wikilink")
  - 幻灯片：工具和导出性能
  - 打印：排版选项和参照
  - 网络：自动创建上传图库

## 參見

  - [Adobe Bridge](../Page/Adobe_Bridge.md "wikilink")
  - [Adobe Photoshop](../Page/Adobe_Photoshop.md "wikilink")
  - [Aperture](../Page/Aperture.md "wikilink")

## 参考资料

## 外部連結

  - [Adobe公司產品網頁Adobe Photoshop
    Lightroom](http://www.adobe.com/cn/products/photoshoplightroom/)
  - [Adobe公司產品網頁Adobe Photoshop
    Lightroom](http://www.adobe.com/tw/products/photoshoplightroom/)
  - [Adobe Photoshop Lightroom 3
    最新參考書籍](http://www.facebook.com/pages/%E8%B7%9F%E8%91%97%E6%94%9D%E5%BD%B1%E5%B8%AB%E5%AD%B8Lightroom-3/147430761996607/)

[Category:位圖編輯軟件](../Category/位圖編輯軟件.md "wikilink")
[Category:Adobe软件](../Category/Adobe软件.md "wikilink")
[Category:Adobe Photoshop](../Category/Adobe_Photoshop.md "wikilink")

1.  [Adobe Labs](http://labs.adobe.com)
2.