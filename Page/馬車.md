[Horse_and_buggy_1910.jpg](https://zh.wikipedia.org/wiki/File:Horse_and_buggy_1910.jpg "fig:Horse_and_buggy_1910.jpg")

**馬車**，是一種以[馬匹拉動的](../Page/馬匹.md "wikilink")[交通工具](../Page/交通工具.md "wikilink")，而[馬車夫當](../Page/馬車夫.md "wikilink")[司機](../Page/司機.md "wikilink")。

在[蒸氣機與](../Page/蒸氣機.md "wikilink")[內燃機](../Page/內燃機.md "wikilink")[發明之前](../Page/發明.md "wikilink")，馬車、[牛車](../Page/牛車.md "wikilink")、[鹿車和](../Page/鹿車.md "wikilink")[驢車等都是平常的](../Page/驢車.md "wikilink")[私家車](../Page/私家車.md "wikilink")，用於[貨物](../Page/貨物.md "wikilink")[運輸和](../Page/運輸.md "wikilink")[貴族代步](../Page/貴族.md "wikilink")。在[法國](../Page/法國.md "wikilink")，[街車也曾有過以](../Page/街車.md "wikilink")[馬力來拉車的](../Page/馬力.md "wikilink")[概念](../Page/概念.md "wikilink")，如早期的[巴黎路面電車](../Page/巴黎路面電車.md "wikilink")。因為具有快速、靈活的特點，所以馬車在[畜力車中佔有重要的地位](../Page/畜力車.md "wikilink")。

在古代，牛耕田、馬拉車是想當然的現事。但今日不少[已開發國家的市區已經很難找到馬車的蹤影](../Page/已開發國家.md "wikilink")，通常只在一些特定[節慶表演或](../Page/節慶.md "wikilink")[觀光區才會出現](../Page/觀光.md "wikilink")。在一些[君主制國家如](../Page/君主制.md "wikilink")[日本](../Page/日本.md "wikilink")、[英國等國的王室仍有馬車做為隆重場合載送國賓及王室要員之用](../Page/英國.md "wikilink")。

## 各種西式馬車

  - Buggy：一匹馬拉的輕裝馬車，現在這個英文單字通常用來指[沙灘車](../Page/沙灘車.md "wikilink")。
  - Cabriolet：一匹馬拉的兩輪帶篷馬車。
  - Caravan：大型的帶篷馬車。
  - Carriage：兩匹到四匹馬拉的四輪馬車。
  - Cariole：一匹馬拉的小型馬車。
  - Chariot：古代的雙輪[馬戰車或](../Page/馬戰車.md "wikilink")[十八世紀的四輪輕馬車](../Page/十八世紀.md "wikilink")。
  - Coach：四匹馬拉的四輪大型馬車。
  - Coupe：乘坐兩人的四輪箱型馬車。
  - Jaunting：一種結構極簡易馬車，乘客背靠背側座。
  - Quadriga：四匹馬拉起的[雙輪戰車](../Page/雙輪戰車.md "wikilink")
  - Wagon：是種由牛、馬或馬騾拉動的運貨馬車。
  - [篷車（Covered wagon）](../Page/篷車.md "wikilink")：Wagon的一種，裝有篷布以遮日擋雨。
  - [驛車（Stagecoach）](../Page/驛站馬車.md "wikilink")：是種四輪的、運送乘客和郵件，用作[公共交通工具的馬車](../Page/公共交通工具.md "wikilink")

## 图片

<center>

<File:Cabriolet> (PSF).jpg|Cabriolet兩輪帶蓬馬車 <File:Chariot>
(PSF).jpg|Chariot馬拉戰車 <File:Coach> (PSF).jpg|Coach四輪車廂馬車 <File:Le> Royal
Mews de Londres-007.JPG|Coach馬車 <File:Covered> wagon.jpg|Wagon馬車
<File:Horse> and buggy.jpg|Buggy馬車 <File:Irish> jaunting car, ca
1890-1900.jpg|Jaunting馬車

</center>

  - 夏含夷：〈[中國馬車的起源及其歷史意義](http://ccsdb.ncl.edu.tw/ccs/image/01_007_001_01_04.pdf)〉。

[马车](../Category/马车.md "wikilink")