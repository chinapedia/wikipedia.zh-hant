**乙酸**，也叫**醋酸**、**冰醋酸**，化学式CH<sub>3</sub>COOH，是一种[有机](../Page/有机化学.md "wikilink")[一元酸和](../Page/一元酸.md "wikilink")[短链饱和脂肪酸](../Page/短链饱和脂肪酸.md "wikilink")，为[食醋内](../Page/醋.md "wikilink")[酸味及](../Page/酸味.md "wikilink")[刺激性](../Page/刺激性.md "wikilink")[气味的来源](../Page/气味.md "wikilink")。纯正而且无水的乙酸（冰醋酸）是无色的吸湿性[固体](../Page/固体.md "wikilink")，[凝固点为](../Page/凝固点.md "wikilink")16.7[℃](../Page/摄氏温标.md "wikilink")（62[℉](../Page/华氏温标.md "wikilink")），凝固后为无色[晶体](../Page/晶体.md "wikilink")。尽管乙酸是一种[弱酸](../Page/弱酸.md "wikilink")，但是它具有[腐蚀性](../Page/腐蚀性.md "wikilink")，其[蒸汽对](../Page/蒸汽.md "wikilink")[眼和](../Page/眼.md "wikilink")[鼻有刺激性作用](../Page/鼻.md "wikilink")，聞起來有一股刺鼻的酸臭味。

乙酸是一种简单的[羧酸](../Page/羧酸.md "wikilink")，由一個[甲基一個](../Page/甲基.md "wikilink")[羧基組成](../Page/羧基.md "wikilink")，是一种重要的化学试剂。在[化学工业中](../Page/化学工业.md "wikilink")，它被用来制造[聚对苯二甲酸乙二酯](../Page/聚对苯二甲酸乙二酯.md "wikilink")，后者即饮料瓶的主要部分。乙酸也被用来制造[电影](../Page/电影.md "wikilink")[胶片所需要的](../Page/底片.md "wikilink")[醋酸纤维素和木材用](../Page/醋酸纤维素.md "wikilink")[胶粘剂中的](../Page/胶粘剂.md "wikilink")[聚乙酸乙烯酯](../Page/聚醋酸乙烯酯.md "wikilink")，以及很多合成[纤维和织物](../Page/纤维.md "wikilink")。家庭中，乙酸稀溶液常被用作[除垢剂](../Page/除垢剂.md "wikilink")。[食品工业方面](../Page/食品产业.md "wikilink")，在食品添加剂列表E260中，乙酸是规定的一种[酸度调节剂](../Page/酸度调节剂.md "wikilink")。

每年世界范围内的乙酸需求量在650万[吨左右](../Page/吨.md "wikilink")。其中大约150万吨是循环再利用的，剩下的500万吨是通过[石化原料直接制取或通过生物](../Page/石油化学.md "wikilink")[发酵制取](../Page/发酵.md "wikilink")。

## 命名

**乙酸**（ethanoic
acid）既是常用的名称，也是[国际纯粹与应用化学联合会](../Page/國際純粹與應用化學聯合會.md "wikilink")（IUPAC）规定的官方名称。俗称**醋酸**（acetic
acid），该名称来自于拉丁文中的表示醋的词“acetum”。无水的乙酸在略低于室温的温度下（16.7℃），能够转化为一种具有腐蚀性的冰状[晶体](../Page/晶体.md "wikilink")，故常称幾乎不含水的醋酸为**冰醋酸**（glacial
acetic acid）。

乙酸的实验式（最简式）为CH<sub>2</sub>O，分子式为C<sub>2</sub>H<sub>4</sub>O<sub>2</sub>，结构简式为CH<sub>3</sub>-COOH、CH<sub>3</sub>COOH来突出其中的[羧基](../Page/羧酸.md "wikilink")，表明更加准确的结构。失去[H<sup>+</sup>后形成的离子为乙酸根阴离子](../Page/氢正离子.md "wikilink")。乙酸最常用的正式缩写是AcOH
或
HOAc，其中Ac代表了乙酸中的乙酰基（CH<sub>3</sub>CO）。[酸](../Page/酸.md "wikilink")[碱](../Page/碱.md "wikilink")[中和反应中也可以用HAc表示乙酸](../Page/中和反应.md "wikilink")，其中Ac代表了乙酸根[阴离子](../Page/阴离子.md "wikilink")（CH<sub>3</sub>COO<sup>−</sup>），但很多人认为这样容易造成误解。上述两种情况中，Ac都不应与[化学元素中](../Page/元素.md "wikilink")[锕的缩写混淆](../Page/锕.md "wikilink")。

## 历史

[AceticAcid012.jpg](https://zh.wikipedia.org/wiki/File:AceticAcid012.jpg "fig:AceticAcid012.jpg")乙酸发酵细菌（[醋酸杆菌](../Page/醋杆菌属.md "wikilink")）能在世界的每个角落发现，每个[民族在](../Page/民族.md "wikilink")[酿酒的时候](../Page/酿酒.md "wikilink")，不可避免的会发现醋——它是这些酒精饮料暴露于空气后的自然产物。如中国就有[杜康的儿子黑塔因酿酒时间过长得到醋的说法](../Page/杜康.md "wikilink")。

乙酸在化学中的运用可以追溯到很古老的年代。在公元前3世纪，希腊哲学家[泰奥弗拉斯托斯详细描述了乙酸是如何与](../Page/泰奥弗拉斯托斯.md "wikilink")[金属发生反应生成美术上要用的](../Page/金属.md "wikilink")[颜料的](../Page/色素.md "wikilink")，包括白铅（[碳酸铅](../Page/碳酸铅.md "wikilink")）、铜绿（铜盐的混合物包括[乙酸铜](../Page/乙酸铜.md "wikilink")）。[古罗马的人们将发酸的酒放在铅制容器中煮沸](../Page/古罗马.md "wikilink")，能得到一种高甜度的糖浆，叫做“sapa”。“sapa”富含一种有[甜味的铅糖](../Page/甜味.md "wikilink")，即[乙酸铅](../Page/乙酸铅.md "wikilink")，这导致了罗马[贵族间的](../Page/貴族.md "wikilink")[铅中毒](../Page/鉛中毒.md "wikilink")。8世纪时，[波斯炼金术士](../Page/波斯.md "wikilink")[贾比尔](../Page/贾比尔.md "wikilink")，用[蒸馏法浓缩了醋中的乙酸](../Page/蒸馏.md "wikilink")。

[文艺复兴时期](../Page/文艺复兴.md "wikilink")，人们通过[金属醋酸盐的](../Page/金属.md "wikilink")[干馏制备冰醋酸](../Page/干馏.md "wikilink")。16世纪德国炼金术士[安德烈亚斯·利巴菲乌斯就描述了这种方法](../Page/安德烈亚斯·利巴菲乌斯.md "wikilink")，并且拿由这种方法产生的冰醋酸来和由醋中提取的酸相比较。仅仅是因为水的存在，导致了醋酸的性质发生如此大的改变，以至于在几个世纪里，化学家们都认为这是两个截然不同的物质。法国化学家阿迪（Pierre
Adet）证明了它们两个是相同的。

1847年，德国科学家[阿道夫·威廉·赫尔曼·科尔贝第一次通过](../Page/阿道夫·威廉·赫尔曼·科尔贝.md "wikilink")[无机原料合成了乙酸](../Page/无机化学.md "wikilink")。这个反应的历程首先是[二硫化碳经过](../Page/二硫化碳.md "wikilink")[氯化转化为](../Page/氯化.md "wikilink")[四氯化碳](../Page/四氯化碳.md "wikilink")，接着是[四氯乙烯的高温分解后](../Page/四氯乙烯.md "wikilink")[水解](../Page/水解.md "wikilink")，并氯化，从而产生[三氯乙酸](../Page/三氯乙酸.md "wikilink")，最后一步通过[电解](../Page/电解.md "wikilink")[还原产生乙酸](../Page/还原.md "wikilink")\[1\]。

1910年时，大部分的冰醋酸提取自[干馏](../Page/干馏.md "wikilink")[木材得到的](../Page/木材.md "wikilink")[煤焦油](../Page/煤焦油.md "wikilink")。首先是将煤焦油通过[氢氧化钙处理](../Page/氢氧化钙.md "wikilink")，然后将形成的[乙酸钙用](../Page/乙酸钙.md "wikilink")[硫酸酸化](../Page/硫酸.md "wikilink")，得到其中的乙酸。在这个时期，德国生产了约10000吨的冰醋酸，其中30%被用来制造[靛青](../Page/蓼藍.md "wikilink")[染料](../Page/染料.md "wikilink")\[2\]\[3\]。

## 物理性质

[AceticAcid010.png](https://zh.wikipedia.org/wiki/File:AceticAcid010.png "fig:AceticAcid010.png")

  - 乙酸在[常温下是一种有强烈](../Page/常温.md "wikilink")[刺激性酸味的](../Page/刺激性.md "wikilink")[无色](../Page/颜色.md "wikilink")[液体](../Page/液体.md "wikilink")。
  - 乙酸的[熔点为](../Page/熔点.md "wikilink")16.5℃（289.6
    [K](../Page/热力学温标.md "wikilink")）。[沸点](../Page/沸点.md "wikilink")118.1℃（391.2
    [K](../Page/热力学温标.md "wikilink")）。相对[密度](../Page/密度.md "wikilink")1.05，闪点39℃，爆炸极限4%～17%（体积）\[4\]。纯的乙酸在低于熔点时会冻结成[冰状](../Page/冰.md "wikilink")[晶体](../Page/晶体.md "wikilink")，所以无水乙酸又称为**冰醋酸**。
  - 乙酸易[溶于](../Page/溶解.md "wikilink")[水和](../Page/水.md "wikilink")[乙醇](../Page/乙醇.md "wikilink")，其水溶液呈弱[酸性](../Page/酸.md "wikilink")。乙酸盐也易溶于水。

## 化学性质

### 酸性

[羧酸中](../Page/羧酸.md "wikilink")，例如乙酸，的羧基[氢](../Page/氢.md "wikilink")[原子能够部分电离变为氢](../Page/原子.md "wikilink")[离子](../Page/离子.md "wikilink")（[质子](../Page/質子.md "wikilink")）而释放出来，导致羧酸的酸性。乙酸在水溶液中是一元弱酸，[酸度系数为](../Page/酸度系数.md "wikilink")4.8，pK<sub>a</sub>=4.75（25℃），浓度为1mol/L的醋酸溶液（类似于家用醋的浓度）的pH为2.4，也就是说仅有0.4%的醋酸[分子是解离的](../Page/分子.md "wikilink")。

[Acetic_acid_deprotonation.png](https://zh.wikipedia.org/wiki/File:Acetic_acid_deprotonation.png "fig:Acetic_acid_deprotonation.png")

乙酸的酸性促使它还可以与[碳酸钠](../Page/碳酸钠.md "wikilink")、[氢氧化铜](../Page/氢氧化铜.md "wikilink")、[苯酚钠等物质反应](../Page/苯酚.md "wikilink")。乙酸还可以使紫色石蕊试液变红。

2CH<sub>3</sub>COOH + Na<sub>2</sub>CO<sub>3</sub> \(\rightarrow\)
2CH<sub>3</sub>COONa + CO<sub>2</sub> + H<sub>2</sub>O

2CH<sub>3</sub>COOH + Cu(OH)<sub>2</sub> \(\rightarrow\)
(CH<sub>3</sub>COO)<sub>2</sub>Cu + 2H<sub>2</sub>O

CH<sub>3</sub>COOH + C<sub>6</sub>H<sub>5</sub>ONa \(\rightarrow\)
C<sub>6</sub>H<sub>5</sub>OH （[苯酚](../Page/苯酚.md "wikilink")）+
CH<sub>3</sub>COONa

### 二聚物（偶合）

[Acetic_Acid_Hydrogenbridge_V.1.svg](https://zh.wikipedia.org/wiki/File:Acetic_Acid_Hydrogenbridge_V.1.svg "fig:Acetic_Acid_Hydrogenbridge_V.1.svg")

乙酸的[晶体结构显示](../Page/晶体结构.md "wikilink")\[5\]
，分子间通过[氢键结合为](../Page/氢键.md "wikilink")[二聚体](../Page/二聚体.md "wikilink")（亦称二缔结物），二聚体也存在于120℃的[蒸汽状态](../Page/蒸汽.md "wikilink")。二聚体有较高的稳定性，现在已经通过冰点降低测定分子量法以及**X**光衍射证明了分子量较小的羧酸如[甲酸](../Page/甲酸.md "wikilink")、乙酸在固态及液态，甚至气态以二聚体形式存在。当乙酸与水溶和的时候，二聚体间的氢键会很快的断裂。其它的[羧酸](../Page/羧酸.md "wikilink")（乃至结构相似的[碳酸氢盐](../Page/碳酸氢盐.md "wikilink")）也有类似的二聚现象。

\(\Delta{} H_m^{\ominus}=-58.6\mathrm {kJ{\cdot}mol^{-1}}\)（两端连接**H**）

### 溶剂

液态乙酸是一个[亲水](../Page/親水性.md "wikilink")（[极性](../Page/极性.md "wikilink")）[质子化溶剂](../Page/质子溶剂.md "wikilink")，与[乙醇和](../Page/乙醇.md "wikilink")[水类似](../Page/水.md "wikilink")。因为[介电常数为](../Page/电容率.md "wikilink")6.2，它不仅能溶解极性化合物，比如[无机盐和](../Page/无机盐.md "wikilink")[糖](../Page/糖.md "wikilink")，也能够溶解非极性化合物，比如[油类或一些](../Page/油.md "wikilink")[元素的](../Page/元素.md "wikilink")[分子](../Page/分子.md "wikilink")，比如[硫和](../Page/硫.md "wikilink")[碘](../Page/碘.md "wikilink")。它也能与许多极性或非极性溶剂混合，比如水，[氯仿](../Page/三氯甲烷.md "wikilink")，[己烷](../Page/己烷.md "wikilink")。乙酸的溶解性和可混合性使其成为了化工中广泛运用的化学品。

### 化学反应

对于许多[金属](../Page/金属.md "wikilink")，乙酸是有[腐蚀性的](../Page/腐蚀.md "wikilink")，例如[铁](../Page/铁.md "wikilink")、[镁和](../Page/镁.md "wikilink")[锌](../Page/锌.md "wikilink")，反应生成[氢气和金属乙酸盐](../Page/氢.md "wikilink")。因为[铝在](../Page/铝.md "wikilink")[空气中表面会形成](../Page/空气.md "wikilink")[氧化铝保护层](../Page/氧化铝.md "wikilink")，所以铝制容器能用来运输乙酸。金属的乙酸盐也可以用乙酸和相应的[碱性物质反应](../Page/碱.md "wikilink")，比如最著名的例子：[小苏打与](../Page/碳酸氢钠.md "wikilink")[醋的反应](../Page/醋.md "wikilink")。除了醋酸[铬](../Page/铬.md "wikilink")（II），几乎所有的醋酸盐能溶于水。

  -
    [Mg](../Page/镁.md "wikilink")<sub>([s](../Page/固体.md "wikilink"))</sub>+
    2 CH<sub>3</sub>COOH<sub>([aq](../Page/溶液.md "wikilink"))</sub> →
    (CH<sub>3</sub>COO)<sub>2</sub>Mg<sub>(aq)</sub> +
    [H<sub>2</sub>](../Page/氢.md "wikilink")<sub>([g](../Page/气体.md "wikilink"))</sub>

<!-- end list -->

  -
    [NaHCO<sub>3</sub>](../Page/碳酸氢钠.md "wikilink")<sub>(s)</sub> +
    CH<sub>3</sub>COOH<sub>(aq)</sub> →
    [CH<sub>3</sub>COONa](../Page/乙酸钠.md "wikilink")<sub>(aq)</sub>
    + [CO<sub>2</sub>](../Page/二氧化碳.md "wikilink")<sub>(g)</sub> +
    [H<sub>2</sub>O](../Page/水.md "wikilink")<sub>([l](../Page/液体.md "wikilink"))</sub>

[Acetic_acid_organic_reactions.png](https://zh.wikipedia.org/wiki/File:Acetic_acid_organic_reactions.png "fig:Acetic_acid_organic_reactions.png")乙酸能发生普通羧酸的典型化学反应，特别注意的是，可以还原生成[乙醇](../Page/乙醇.md "wikilink")，通过[亲核取代机理生成乙酰氯](../Page/亲核取代反应.md "wikilink")，也可以双分子脱水生成[酸酐](../Page/酸酐.md "wikilink")。

同样，乙酸也可以成[酯或氨基化合物](../Page/酯.md "wikilink")。如乙酸可以与[乙醇在](../Page/乙醇.md "wikilink")[浓硫酸存在并加热的条件下生成](../Page/硫酸.md "wikilink")[乙酸乙酯](../Page/乙酸乙酯.md "wikilink")。

CH<sub>3</sub>COOH + CH<sub>3</sub>CH<sub>2</sub>OH →
CH<sub>3</sub>COOCH<sub>2</sub>CH<sub>3</sub> + H<sub>2</sub>O

440℃的高温下，乙酸分解生成[甲烷和](../Page/甲烷.md "wikilink")[二氧化碳或](../Page/二氧化碳.md "wikilink")[乙烯酮和](../Page/乙烯酮.md "wikilink")[水](../Page/水.md "wikilink")。在[甲烷菌的作用下](../Page/甲烷菌.md "wikilink")，乙酸也可以歧化分解产生[甲烷和](../Page/甲烷.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")\[6\]\[7\]。

### 鉴别

乙酸可以通过其气味进行鉴别。若加入[氯化铁](../Page/三氯化铁.md "wikilink")（III），生成产物“[乙酸铁](../Page/乙酸铁.md "wikilink")”为深红色并且会在酸化后消失，通过此颜色反应也能鉴别乙酸。

## 生物化学

乙酸中的[乙酰基](../Page/乙酰.md "wikilink")，是[生物化学中所有生命的基础](../Page/生物化学.md "wikilink")。当它与[辅酶A结合后](../Page/辅酶A.md "wikilink")，就成为了[碳水化合物和](../Page/糖.md "wikilink")[脂肪](../Page/脂肪.md "wikilink")[新陈代谢的中心](../Page/新陈代谢.md "wikilink")。然而，乙酸在[细胞中的浓度是被严格控制在一个很低的范围内](../Page/细胞.md "wikilink")，避免使得细胞质的[pH发生破坏性的改变](../Page/pH值.md "wikilink")。与其它长链羧酸不同，乙酸并不存在于甘油三酸脂中。但是，人造含乙酸的甘油三酸脂，又叫甘油醋酸酯（[三乙酸甘油酯](../Page/三乙酸甘油酯.md "wikilink")），则是一种重要的食品添加剂，也被用来制造化妆品和局部性药物。

乙酸由一些特定的[细菌生产或分泌](../Page/细菌.md "wikilink")。值得注意的是醋菌类梭菌属的丙酮丁醇梭杆菌，这个细菌广泛存在于全世界的食物、水和土壤之中。在水果或其他食物腐败时，醋酸也会自然生成。乙酸也是包括人类在内的所有[灵长类生物的](../Page/灵长类.md "wikilink")[阴道润滑液的一个组成部分](../Page/阴道.md "wikilink")，被当作一个温和的抗菌剂\[8\]。

## 制备

[Acetic_acid_1884_plant.jpg](https://zh.wikipedia.org/wiki/File:Acetic_acid_1884_plant.jpg "fig:Acetic_acid_1884_plant.jpg")
乙酸的制备可以通过人工合成和细菌发酵两种方法。现在，生物合成法，即利用细菌发酵，仅占整个世界产量的10%，但是仍然是生产[醋的最重要的方法](../Page/醋.md "wikilink")，因为很多国家的食品安全法规规定食物中的醋必须是由生物制备的。75%的工业用乙酸是通过[甲醇的羰基化制备](../Page/甲醇.md "wikilink")，具体方法见下。空缺部分由其他方法合成\[9\]。

整个世界生产的纯乙酸每年大概有500万[吨](../Page/吨.md "wikilink")，其中一半是由[美国生产的](../Page/美國.md "wikilink")。[欧洲现在的产量大约是每年](../Page/欧洲.md "wikilink")100万吨，但是在不断减少。[日本每年也要生产](../Page/日本.md "wikilink")70万吨纯乙酸。每年世界消耗量为650万吨，除了上面的500万吨，剩下的150万吨都是回收利用的\[10\]\[11\]
。

### 发酵法

  - 有氧发酵

在人类历史中，以醋的形式存在的乙酸，一直是用[醋杆菌属](../Page/醋杆菌属.md "wikilink")[细菌制备](../Page/细菌.md "wikilink")。在氧气充足的情况下，这些细菌能够从含有[酒精的](../Page/乙醇.md "wikilink")[食物中生产出乙酸](../Page/食物.md "wikilink")。通常使用的是[苹果](../Page/苹果.md "wikilink")[酒或](../Page/酒.md "wikilink")[葡萄酒混合](../Page/葡萄酒.md "wikilink")[谷物](../Page/粮食.md "wikilink")、[麦芽](../Page/麥芽糖.md "wikilink")、[米或](../Page/稻.md "wikilink")[马铃薯捣碎后](../Page/马铃薯.md "wikilink")[发酵](../Page/发酵.md "wikilink")。有这些细菌达到的化学方程式为：

  -
    [C<sub>2</sub>H<sub>5</sub>OH](../Page/乙醇.md "wikilink") +
    [O<sub>2</sub>](../Page/氧气.md "wikilink") → CH<sub>3</sub>COOH +
    [H<sub>2</sub>O](../Page/水.md "wikilink")

做法是将醋菌属的细菌接种于稀释后的酒精溶液并保持一定温度，放置于一个通风的位置，在几个月内就能够变为醋。工业生产醋的方法通过提供[氧气使得此过程加快](../Page/氧气.md "wikilink")。

现在商业化生产所用方法其中之一被称为“快速方法”或“[德国方法](../Page/德国.md "wikilink")”，因为首次成功是在1823年的德国。此方法中，发酵是在一个塞满了木屑或木炭的[塔中进行](../Page/塔.md "wikilink")。含有酒精的原料从塔的上方滴入，新鲜空气从他的下方自然进入或强制对流。改进后的空气供应使得此过程能够在几个星期内完成，大大缩短了制醋的时间。

现在的大部分醋是通过液态的细菌培养基制备的，由Otto Hromatka和Heinrich
Ebner在1949年首次提出。在此方法中，酒精在持续的搅拌中发酵为乙酸，空气通过气泡的形式被充入溶液。通过这个方法，含乙酸15%的醋能够在两至三天制备完成。

  - 无氧发酵

部分[厌氧细菌](../Page/厭氧生物.md "wikilink")，包括[梭菌属的部分成员](../Page/梭菌属.md "wikilink")，能够将[糖类直接转化为乙酸而不需要乙醇作为中间体](../Page/糖.md "wikilink")。总体反应方程式如下：

  -
    [C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>](../Page/葡萄糖.md "wikilink")
    → 3 CH<sub>3</sub>COOH

更令工业化学感兴趣的是，许多细菌能够从仅含单碳的化合物中生产乙酸，例如[甲醇](../Page/甲醇.md "wikilink")，[一氧化碳或](../Page/一氧化碳.md "wikilink")[二氧化碳与](../Page/二氧化碳.md "wikilink")[氢气的混合物](../Page/氢气.md "wikilink")。

  -
    2 [CO<sub>2</sub>](../Page/二氧化碳.md "wikilink") + 4
    [H<sub>2</sub>](../Page/氢气.md "wikilink") → CH<sub>3</sub>COOH + 2
    [H<sub>2</sub>O](../Page/水.md "wikilink")

梭菌属因为有能够直接使用糖类的能力，减少了成本，这意味着这些细菌有比醋菌属细菌的乙醇氧化法生产乙酸更有效率的潜力。然而，梭菌属细菌的耐酸性不及醋菌属细菌。耐酸性最大的梭菌属细菌也只能生产不到10%的乙酸，而有的醋酸菌能够生产20%的乙酸。到现在为止，使用醋酸属细菌制醋仍然比使用梭菌属细菌制备后浓缩更经济。所以，尽管梭菌属的细菌早在1940年就已经被发现，但它的工业应用仍然被限制在一个狭小的范围。

### 甲醇羰基化法

大部分乙酸是通过甲基羰基化合成的。此反应中，[甲醇和](../Page/甲醇.md "wikilink")[一氧化碳反应生成乙酸](../Page/一氧化碳.md "wikilink")，方程式如下

  -
    [CH<sub>3</sub>OH](../Page/甲醇.md "wikilink") +
    [CO](../Page/一氧化碳.md "wikilink") → CH<sub>3</sub>COOH

这个过程是以碘代甲烷为中间体，分三个步骤完成，并且需要一个一般由多种金属构成的催化剂（第二部中）

  -
    (1) CH<sub>3</sub>OH + [HI](../Page/碘化氢.md "wikilink") →
    [CH<sub>3</sub>I](../Page/碘甲烷.md "wikilink") + H<sub>2</sub>O

<!-- end list -->

  -
    (2) CH<sub>3</sub>I + [CO](../Page/一氧化碳.md "wikilink") →
    CH<sub>3</sub>COI

<!-- end list -->

  -
    (3) CH<sub>3</sub>COI + H<sub>2</sub>O → CH<sub>3</sub>COOH + HI

通过控制反应条件，也可以通过同样的反应生成[乙酸酐](../Page/乙酸酐.md "wikilink")。因为一氧化碳和甲醇均是常用的化工原料，所以甲基羰基化一直以来备受青睐。早在1925年，英国塞拉尼斯公司的Henry
Drefyus已经开发出第一个甲基羰基化制乙酸的试点装置。然而，由于缺少能耐高压（200[atm或更高](../Page/气压.md "wikilink")）和耐腐蚀的容器，此法一度受到抑制\[12\]
。直到1963年，德国[巴斯夫化学公司用](../Page/巴斯夫.md "wikilink")[钴作催化剂](../Page/钴.md "wikilink")，开发出第一个适合工业生产的办法。到了1968年，以[铑为基础的催化剂的](../Page/铑.md "wikilink")（*cis*−\[Rh(CO)<sub>2</sub>I<sub>2</sub>\]<sup>−</sup>）被发现，使得反映所需压力减到一个较低的水平并且几乎没有副产物。1970年，美国[孟山都公司建造了首个使用此催化剂的设备](../Page/孟山都.md "wikilink")，此后，铑催化甲基羰基化制乙酸逐渐成为支配性的[孟山都法](../Page/蒙山都法.md "wikilink")。90年代后期，[英国石油成功的将](../Page/英国石油.md "wikilink")[Cativa催化法商业化](../Page/Cativa催化法.md "wikilink")，此法是基于[铱](../Page/铱.md "wikilink")，使用（\[Ir(CO)<sub>2</sub>I<sub>2</sub>\]<sup>−</sup>）\[13\]
，它比孟山都法更加绿色也有更高的效率，很大程度上排挤了孟山都法。

### 乙醇氧化法

由[乙醇在有](../Page/乙醇.md "wikilink")[催化剂的条件下和](../Page/催化剂.md "wikilink")[氧气发生](../Page/氧气.md "wikilink")[氧化反应制得](../Page/氧化.md "wikilink")。

C<sub>2</sub>H<sub>5</sub>OH + O<sub>2</sub> \(\rightarrow\)
CH<sub>3</sub>COOH + H<sub>2</sub>O

### 乙醛氧化法

在孟山都法商业生产之前，大部分的乙酸是由[乙醛](../Page/乙醛.md "wikilink")[氧化制得](../Page/氧化.md "wikilink")。尽管不能与甲基羰基化相比，此法仍然是第二种工业制乙酸的方法。乙醛可以通过氧化[丁烷或轻](../Page/丁烷.md "wikilink")[石脑油制得](../Page/石腦油.md "wikilink")，也可以通过[乙炔水合后生成](../Page/乙炔.md "wikilink")。当丁烷或轻石脑油在空气中加热，并有多种[金属](../Page/金属.md "wikilink")[离子包括](../Page/离子.md "wikilink")[镁](../Page/镁.md "wikilink")，[钴](../Page/钴.md "wikilink")，[铬以及过氧根离子催化](../Page/铬.md "wikilink")，会分解出乙酸。化学方程式如下：

  -
    2 [C<sub>4</sub>H<sub>10</sub>](../Page/丁烷.md "wikilink") + 5
    [O<sub>2</sub>](../Page/氧气.md "wikilink") → 4 CH<sub>3</sub>COOH + 2
    [H<sub>2</sub>O](../Page/水.md "wikilink")

此反应可以在能使丁烷保持液态的最高温度和压力下进行，一般的反应条件是150℃和55[ atm](../Page/气压.md "wikilink")。副产物包括[丁酮](../Page/丁酮.md "wikilink")，[乙酸乙酯](../Page/乙酸乙酯.md "wikilink")，[甲酸和](../Page/甲酸.md "wikilink")[丙酸](../Page/丙酸.md "wikilink")。因为部分副产物也有经济价值，所以可以调整反应条件使得副产物更多的生成，不过分离乙酸和副产物使得反应的成本增加。

在类似条件下，使用上述催化剂，乙醛能被空气中的氧气氧化生成乙酸

  -
    2 [CH<sub>3</sub>CHO](../Page/乙醛.md "wikilink") +
    [O<sub>2</sub>](../Page/氧气.md "wikilink") → 2 CH<sub>3</sub>COOH

使用新式催化剂，此反应能获得95%以上的乙酸产率。主要的副产物为[乙酸乙酯](../Page/乙酸乙酯.md "wikilink")，[甲酸和](../Page/甲酸.md "wikilink")[甲醛](../Page/甲醛.md "wikilink")。因为副产物的[沸点都比乙酸低](../Page/沸点.md "wikilink")，所以很容易通过[蒸馏除去](../Page/蒸馏.md "wikilink")。

### 乙烯氧化法

由乙烯在催化剂（所用催化剂为[氯化钯](../Page/氯化钯.md "wikilink")：PdCl<sub>2</sub>、[氯化铜](../Page/氯化铜.md "wikilink")：CuCl<sub>2</sub>和[乙酸锰](../Page/乙酸锰\(III\).md "wikilink")：(CH<sub>3</sub>COO)<sub>2</sub>Mn）存在的条件下，与氧气发生反应生成。此反应可以看作先将乙炔氧化成乙醛，再通过乙醛氧化法制得。

### 丁烷氧化法

丁烷氧化法又称为直接氧化法，这是用[丁烷为主要原料](../Page/丁烷.md "wikilink")，通过[空气氧化而制得乙酸的一种方法](../Page/空气.md "wikilink")，也是主要的乙酸合成方法。

2CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>CH<sub>3</sub> +
5O<sub>2</sub> \(\rightarrow\) 4CH<sub>3</sub>COOH + 2H<sub>2</sub>O

### 其他方法

除上述方法之外，还有许多制取乙酸的方法和途径。

例如：甲烷和一氧化碳或二氧化碳在催化作用下生成乙酸\[14\]

\[\rm CH_4 + CO + \tfrac12O_2 \xrightarrow[CF_3COOH]{Pd/Cu} CH_3COOH \,\]

\[\rm CH_4 + CO_2 \xrightarrow[CF_3COOH]{Pd/Cu,\ K_2S_2O_3} CH_3COOH \,\]

\[\rm CH_4 + CO \xrightarrow[CF_3COOH]{HF-SbF_5\ or\ FSO_3H-SbF_5,\ H_2O} CH_3COOH \,\]

## 用途

[Acetic_acid_winchester.JPG](https://zh.wikipedia.org/wiki/File:Acetic_acid_winchester.JPG "fig:Acetic_acid_winchester.JPG")内一瓶2.5[升乙酸](../Page/升.md "wikilink")\]\]
乙酸是制备很多化合物所需要使用的基本化学试剂。最大的单一使用乙酸的是制备乙酸乙烯酯单体，接下来是制备乙酸酐和其他酯。在醋中的乙酸仅占了所有乙酸中的很小一部分。

### 乙酸乙烯酯单体

乙酸的最主要用途是制备乙酸乙烯酯单体，消耗了大概40%到45%的世界乙酸产量。这个反应是通过乙烯和乙酸在钯催化下与氧气反应。

  -
    2 CH<sub>3</sub>COOH + 2
    [C<sub>2</sub>H<sub>4</sub>](../Page/乙烯.md "wikilink") +
    [O<sub>2</sub>](../Page/氧气.md "wikilink") → 2
    [CH<sub>3</sub>COOC=CH<sub>2</sub>](../Page/乙酸乙烯酯.md "wikilink") + 2
    [H<sub>2</sub>O](../Page/水.md "wikilink")

乙酸乙烯酯可以聚合形成[聚乙酸乙烯酯或其他聚合物](../Page/聚醋酸乙烯酯.md "wikilink")，这些聚合物被使用于颜料及粘合剂。

### 乙酸酐

两分子乙酸的缩合产物是[乙酸酐](../Page/乙酸酐.md "wikilink")，每年全世界生产乙酸酐消耗了大概25%-30%的乙酸。乙酸酐也可以直接通过甲醇羰基化制备。Cativa的设备也可以用来生产乙酸酐。

[Acetic_acid_condensation.svg](https://zh.wikipedia.org/wiki/File:Acetic_acid_condensation.svg "fig:Acetic_acid_condensation.svg")

乙酸酐是一个很强的乙酰化试剂。因此，它的主要用途就是制[乙酸纤维素酯](../Page/醋酸纤维素.md "wikilink")，这个合成织物主要用于制作[电影](../Page/电影.md "wikilink")[胶片](../Page/底片.md "wikilink")。乙酸酐也用来制备[阿司匹林和](../Page/阿司匹林.md "wikilink")[海洛因等其他化合物](../Page/海洛因.md "wikilink")。

### 醋

以醋的形式，乙酸溶液（一般含5%到18%（质量分数）的乙酸）被用作[调味品](../Page/调味料.md "wikilink")，也被用来[腌蔬菜和其他食物](../Page/醃.md "wikilink")。一般来说，腌菜用的醋在浓度上比一般调味品醋浓度更大。食用醋的总量在世界乙酸年产量中只占一个很小的比例，不过在历史上，这却是一个悠久的应用。

醋的製作方法分為2種: 1.釀造法 最傳統的方法，以酒精發酵製成。 2.化學合成法
以冰醋酸稀釋後，加入香料調味而成。此方法成本低廉，但風味較差，也容易為不良商人使用。

### 溶剂

冰醋酸是一个良好的[极性](../Page/极性.md "wikilink")[质子溶剂](../Page/质子溶剂.md "wikilink")，常常被用来作为[重结晶提纯](../Page/重结晶.md "wikilink")[有机化合物的溶剂](../Page/有机化合物.md "wikilink")。纯的溶融状态的乙酸是生产[对苯二甲酸的溶液](../Page/对苯二甲酸.md "wikilink")，对苯二甲酸是制备[聚对苯二甲酸乙二酯的重要原料](../Page/聚对苯二甲酸乙二酯.md "wikilink")。尽管现在仅有5%-10%的乙酸作此用途，不过据预测，它在今后几十年内将有显著的增长，因为聚对苯二甲酸乙二酯的产量正在增加。

在有[碳正离子参与的反应中](../Page/碳正离子.md "wikilink")，常常使用乙酸作为溶液，例如[傅-克反应](../Page/傅－克反应.md "wikilink")。

冰醋酸在[分析化学上被用来与弱](../Page/分析化学.md "wikilink")[碱反应](../Page/碱.md "wikilink")，比如有机氨基化合物。冰醋酸比水的酸性更强而碱性更弱，因此氨基化合物在中间过程中类似于强碱，可以被溶于乙酸中的强酸滴定，比如溶于乙酸的[高氯酸](../Page/高氯酸.md "wikilink")；而在水中为强酸的[氢卤酸在冰醋酸中只能部分电离](../Page/氢卤酸.md "wikilink")，从而体现出其酸性的差别。

### 其他应用

稀释的醋酸溶液因为它温和的酸性也常常被用来作为一种[除锈的试剂](../Page/除垢剂.md "wikilink")。它的酸性也被用来治疗被[立方水母纲水母刺伤](../Page/立方水母纲.md "wikilink")，如果使用及时，可以通过使水母的刺细胞失去效果达到防止严重受伤甚至死亡的效果。也可以用来为使用Vosol治疗[外耳炎做准备](../Page/外耳炎.md "wikilink")。同样，乙酸也被用来做成喷射[防腐剂](../Page/防腐剂.md "wikilink")，抑制[细菌和](../Page/细菌.md "wikilink")[真菌的生长](../Page/真菌.md "wikilink")。

几种用乙酸制备的有机或无机[盐](../Page/盐_\(化学\).md "wikilink")：

  - [乙酸钠](../Page/乙酸钠.md "wikilink")：用于[纺织业和食品](../Page/紡織.md "wikilink")[防腐剂](../Page/防腐剂.md "wikilink")（[E262](../Page/E编码.md "wikilink")）。
  - [乙酸铜](../Page/乙酸铜.md "wikilink")：用于[色素和](../Page/色素.md "wikilink")[抗真菌剂](../Page/抗真菌剂.md "wikilink")。
  - [乙酸铝和](../Page/乙酸铝.md "wikilink")[乙酸亚铁](../Page/乙酸亚铁.md "wikilink")：用于媒染剂和[染料](../Page/染料.md "wikilink")
  - [乙酸钯](../Page/乙酸钯.md "wikilink")：用于有机反应的[催化剂](../Page/催化剂.md "wikilink")，例如[Heck反应](../Page/Heck反应.md "wikilink")
  - [乙酸鉛](../Page/乙酸鉛.md "wikilink")：油漆顏料，俗稱**鉛白**。

乙酸的取代产物：

  - [氯乙酸](../Page/一氯乙酸.md "wikilink")：二氯乙酸（通常被认为是副产物）及三氯乙酸，被用来生产[靛青](../Page/蓼藍.md "wikilink")[染料](../Page/染料.md "wikilink")。
  - [溴乙酸](../Page/溴乙酸.md "wikilink")：酯化后生成[溴乙酸乙酯](../Page/溴乙酸乙酯.md "wikilink")。
  - [三氟乙酸](../Page/三氟乙酸.md "wikilink")：一种有机合成中常用试剂。

## 安全

浓度较高的乙酸具有腐蚀性，能导致[皮肤烧伤](../Page/皮膚.md "wikilink")，[眼睛永久](../Page/眼.md "wikilink")[失明以及黏膜发炎](../Page/失明.md "wikilink")，因此需要适当的防护。上述烧伤或水泡不一定马上出现，很大部份情況是暴露后几个小时出现。[乳胶](../Page/乳胶.md "wikilink")[手套不能起保护作用](../Page/手套.md "wikilink")，所以在处理乙酸的时候应该带上特制的手套，例如[丁腈橡胶手套](../Page/丁腈橡胶.md "wikilink")。浓缩乙酸在实验室中燃烧比较困难，但是当环境温度达到39℃(102℉)的时候，它便具有可燃的威胁，在此温度以上，乙酸可与空气混合爆炸（爆炸极限4%～17%体积濃度）。

乙酸的危害和乙酸溶液的浓度有关。下表中例举了乙酸溶液的[欧盟分级](../Page/欧盟分级.md "wikilink")：

[Hazard_C.svg](https://zh.wikipedia.org/wiki/File:Hazard_C.svg "fig:Hazard_C.svg")

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/浓度.md" title="wikilink">浓度</a><br />
（质量）</p></th>
<th><p>莫耳浓度</p></th>
<th><p>分级</p></th>
<th><p><a href="../Page/警示性質標準詞列表.md" title="wikilink">R-Phrases</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>10%–25%</p></td>
<td><p>1.67–4.16 mol/L</p></td>
<td><p>刺激 (<strong>Xi</strong>)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>25%–90%</p></td>
<td><p>4.16–14.99 mol/L</p></td>
<td><p>腐蚀 (<strong>C</strong>)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>&gt;90%</p></td>
<td><p>&gt;14.99 mol/L</p></td>
<td><p>腐蚀 (<strong>C</strong>)</p></td>
<td><p>, </p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

因为强烈的刺激性气味及腐蚀性[蒸汽](../Page/蒸汽.md "wikilink")，操作浓度超过25%的乙酸要在眼罩下进行。稀乙酸溶液，例如醋，是无害的。然而，摄入高浓度的乙酸溶液是有害[人及](../Page/人.md "wikilink")[动物健康的](../Page/动物.md "wikilink")。它能导致[消化系统的严重伤害](../Page/消化系统.md "wikilink")，以及潜在的致死性[血液酸性变化](../Page/血液.md "wikilink")。

## 参见

  - 用途

<!-- end list -->

  - [醋](../Page/醋.md "wikilink")
  - [腌](../Page/醃.md "wikilink")，一种食物防腐方法
  - [醋杆菌属](../Page/醋杆菌属.md "wikilink")，从含酒精溶液中产生乙酸
  - [除垢剂](../Page/除垢剂.md "wikilink")，常含有乙酸

<!-- end list -->

  - 化学

<!-- end list -->

  - [羧酸](../Page/羧酸.md "wikilink")，含有-COOH的化合物
  - [脂肪酸](../Page/脂肪酸.md "wikilink")，直链羧酸
  - [乙酸盐](../Page/乙酸盐.md "wikilink")，CH<sub>3</sub>COO<sup>−</sup>，简写
    *AcO<sup>−</sup>*
  - [乙酰基](../Page/乙酰基.md "wikilink")，CH<sub>3</sub>-CO<sub>–</sub>基团，简写
    *Ac*
  - [辅酶A](../Page/辅酶A.md "wikilink")，细胞中重要的含乙酰基的酶

<!-- end list -->

  - 相关化合物

<!-- end list -->

  - [甲酸](../Page/甲酸.md "wikilink")，仅含有一个碳的羧酸
  - [丙酸](../Page/丙酸.md "wikilink")，含有三个碳的羧酸
  - [酒精](../Page/乙醇.md "wikilink")，乙醇
  - [乙醛](../Page/乙醛.md "wikilink")
  - [乙酸酐](../Page/乙酸酐.md "wikilink")
  - [乙酸乙酯](../Page/乙酸乙酯.md "wikilink")，一个重要的溶剂
  - [乙酰乙酸乙酯](../Page/乙酰乙酸乙酯.md "wikilink")，一个重要的有机合成原料
  - [氯乙酸](../Page/一氯乙酸.md "wikilink")
  - [去水醋酸](../Page/去水醋酸.md "wikilink")，一个吡喃的衍生物

## 参考文献

## 外部链接

  - [扬子石化](https://web.archive.org/web/20070206203524/http://www.ypc.com.cn/products/products.php?prod_id=188&prodtype1=2)

  - [化工世界-乙酸页面](https://web.archive.org/web/20060702013525/http://www.jshlzx.net/klh/2/2046/text/zk46_105.htm)

  - [乙酸](http://chemical.qdxy.net/chemical/%D2%D2%CB%E1.htm)

  -
  - [计算化学维基](https://web.archive.org/web/20061002175737/http://www.compchemwiki.org/index.php?title=Acetic_acid)

  - [物质安全数据](http://www.hazard.com/msds/f2/bkk/bkktd.html)

  - [美国国家污染物质列表-乙酸现状](https://web.archive.org/web/20061114044207/http://www.npi.gov.au/database/substance-info/profiles/2.html)

  - [国立职业安全与健康研究所化学品危害手册](http://www.cdc.gov/niosh/npg/npgd0002.html)

  - [29 CFR 1910.1000, Table
    Z-1](http://www.osha.gov/pls/oshaweb/owadisp.show_document?p_table=STANDARDS&p_id=9992)（美国允许暴露极限值）

  - [美国国家防火协会职业训练表](https://web.archive.org/web/20061025181958/http://www.otrain.com/nfpa/a-b.html)

  - [塞拉尼斯官方网站](http://www.celanese.com/index/productsmarkets_index/products_markets_acetate.htm)

  - 乙酸的用法
    [有机合成](https://web.archive.org/web/20061229005504/http://www.orgsyn.org/orgsyn/chemname.asp?nameID=32786)

  - 乙酸的[pH和](../Page/pH值.md "wikilink")[滴定](../Page/滴定.md "wikilink") [-
    数据分析，模拟及生成图表的免费软件](http://www2.iq.usp.br/docente/gutz/Curtipot_.html)

  - [ChemSub Online: CAS号 64-19-7,
    乙酸](http://chemsub.online.fr/name/乙酸.html)

[Category:二碳有机物](../Category/二碳有机物.md "wikilink")
[乙酸](../Category/乙酸.md "wikilink")
[Category:有机溶剂](../Category/有机溶剂.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")
[Category:照相药品](../Category/照相药品.md "wikilink")
[Category:耳科用药](../Category/耳科用药.md "wikilink")
[Category:家用化学品](../Category/家用化学品.md "wikilink")
[Category:葡萄酒中的酸](../Category/葡萄酒中的酸.md "wikilink")

1.  [Goldwhite, Harold (2003). *New Haven Sect. Bull. Am. Chem. Soc.*
    (September
    2003)](http://membership.acs.org/N/NewHaven/bulletins/Bulletin_2003-09.pdf)
    .
2.  Martin, Geoffrey (1917). *Industrial and Manufacturing Chemistry*,
    Part 1, Organic. London: Crosby Lockwood, pp. 330–31.
3.  Schweppe, Helmut (1979). ["Identification of dyes on old
    textiles"](http://aic.stanford.edu/jaic/articles/jaic19-01-003_1.html).
    *J. Am. Inst. Conservation* **19**(1/3), 14–23.
4.
5.  Jones, R.E.; Templeton, D.H. (1958). "The crystal structure of
    acetic acid". *Acta Crystallogr.* **11**(7), 484–87.
6.
7.
8.  *Dictionary of Organic Compounds (6th Edn.)*, Vol. 1 (1996). London:
    Chapman & Hall. ISBN 978-0-412-54090-5
9.  Yoneda, Noriyki; Kusano, Satoru; Yasui, Makoto; Pujado, Peter;
    Wilcher, Steve (2001). *Appl. Catal. A: Gen.* **221**, 253–265.
10. "Production report". *Chem. Eng. News* (July 11, 2005), 67–76.
11. Suresh, Bala (2003). ["Acetic
    Acid"](http://www.sriconsulting.com/CEH/Public/Reports/602.5000/).
    CEH Report 602.5000, SRI International.
12. Wagner, Frank S. (1978) "Acetic acid." In: Grayson, Martin (Ed.)
    *Kirk-Othmer Encyclopedia of Chemical Technology*, 3rd edition, New
    York: John Wiley & Sons.
13. Lancaster, Mike (2002) *Green Chemistry, an Introductory Text*,
    Cambridge: Royal Society of Chemistry, pp. 262–266. ISBN
    978-0-85404-620-1.
14. 王晓红(2002). “CH<sub>4</sub> CO<sub>2</sub>低温转化合成含氧有机物的研究” 17-18
    太原理工大学(2002年5月)