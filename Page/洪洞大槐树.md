**洪洞大槐树**，又称**古大槐树**，**山西大槐树**，位于[中国](../Page/中华人民共和国.md "wikilink")[山西省](../Page/山西省.md "wikilink")[洪洞县城西北二公里的贾村西侧的大槐树公园内](../Page/洪洞县.md "wikilink")，是[明代的一处移民基地](../Page/明朝.md "wikilink")。

## 移民历史

[元朝末年](../Page/元朝.md "wikilink")，自然灾害频有发生，[黄河地区水患尤其严重](../Page/黄河.md "wikilink")。同时统治者的高压统治，导致[红巾军起义](../Page/红巾军.md "wikilink")，战乱纷争，民不聊生，人口大量减少。明初[洪武年间开始从山西移民垦荒](../Page/洪武.md "wikilink")，使农业有所恢复。[明惠帝](../Page/明惠帝.md "wikilink")[建文元年](../Page/建文.md "wikilink")（1399年）又发生了“[靖难之变](../Page/靖难之变.md "wikilink")”战乱四年，又一次造成河北、山东、河南、皖北、淮北等地的荒凉局面，严重破坏了社会经济。

明代时，当时将山西境内的许多移民集中到此地领取[户部发的](../Page/户部.md "wikilink")[勘合](../Page/勘合.md "wikilink")，再分批迁编队往其他[省份](../Page/省份.md "wikilink")。根据《[明史](../Page/明史.md "wikilink")》、《[明实录](../Page/明实录.md "wikilink")》等史书记载，自洪武六年（1373年）到[永乐十五年](../Page/永乐_\(明\).md "wikilink")（1417年）近50年内，先后共计从山西移民移民18次，其中洪武年间10次，永乐年间8次。这些移民迁往[北直隶](../Page/北直隶.md "wikilink")、[河南](../Page/河南.md "wikilink")、[山东](../Page/山东.md "wikilink")、[南直](../Page/南直.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[甘肅等](../Page/甘肅.md "wikilink")[行省](../Page/行省.md "wikilink")，500多个府、县。

## 影响

在中国北方地区，大量的民间家谱、碑文资料有详细记载，在地方志如《温县志》、《宝丰县志》、《宁阳县志》、《丹风县志》、《商南县志》、《山阳县志》等都明确记载了在山西洪洞大槐树下集中移民。至今在河北、河南、山东、东北等地区仍流传着一句民谣：“问我老家在何处，山西洪洞大槐树。祖先故居叫什么，大槐树下[老鸹窝](../Page/乌鸦.md "wikilink")”。

## 旅游及文物保护

现在古大槐树为[中国国家5A级旅游景区](../Page/中国国家5A级旅游景区.md "wikilink")，[山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")。“大槐树”是移民史实的见证者，也是移民心目中的老家，
余亚飞诗云：“拔地巨槐冲碧汉，相承一脉密分枝；树身即使高千丈，落叶归根也有期”，每年有20余万人前往景区祭祖。

## 参考文献

## 外部链接

  - [洪洞大槐树移民的考证](http://www.huaxia.com/zhsx/xgsx/ymls/2008/12/1245589.html)
  - [永乐朝迁民的原因和历史记载](https://web.archive.org/web/20091018135020/http://www.sx.xinhuanet.com/ztjn/2007-03/28/content_9637369.htm)

{{-}}

[Category:明朝人口流动](../Category/明朝人口流动.md "wikilink")
[Category:洪洞历史](../Category/洪洞历史.md "wikilink")
[Category:民系认同](../Category/民系认同.md "wikilink")