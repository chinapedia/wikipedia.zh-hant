《**百合星人奈緒子美眉**》（），是[kashmir的漫畫作品](../Page/kashmir.md "wikilink")。

## 作品簡介

本作自2005年5月號起開始在《[月刊Comic電擊大王](../Page/月刊Comic電擊大王.md "wikilink")》（[MediaWorks出版](../Page/MediaWorks.md "wikilink")）連載。目前出版單行本第四集。連載時一回六頁，作品中充滿了kashmir風格的[惡搞和低級笑點](../Page/惡搞.md "wikilink")。此外在本作第14話中有出現作者另一作品《[○本的住人](../Page/○本的住人.md "wikilink")》裡面的角色「典子」與「小T」，兩作品共用一個世界觀。

在連載時幾乎每回都在雜誌最後面連載。另外，連載標題所用字體每回皆不相同。

## 登場人物

  -
    來自遙遠的「百合星」，企圖讓地球完全[百合化的](../Page/百合_\(同人\).md "wikilink")「百合星人」，強調「百合之下才有真正的[和平](../Page/和平.md "wikilink")」。由於不明原因，以寄宿者的身分跑來地球並寄宿在美鈴家。外表除了耳朵以外都和一般人一樣。喜愛百合、[色情與](../Page/色情.md "wikilink")[蘿莉](../Page/蘿莉.md "wikilink")。擁有各種不可思議的機械，說話亂七八糟，並會隨意改變話題。本作主要笨蛋角色。喜歡吃[零食和](../Page/零食.md "wikilink")[西餅](../Page/西餅.md "wikilink")，常看到她在吃。基本上不記得美鈴以外角色的名字，而隨便用「眼鏡」「弟弟」等方式稱呼他們。

  -
    就讀茉莉野女子學園中等部的國中女生。角色中最正常的角色，主要負責吐槽，但有喜歡廢墟、廢棄道路這種特殊喜好。姓氏為。個性內向、不愛說話，台詞沒有寫在對話框裡。

  -
    美鈴的弟弟，小學男生。在學校受到女生輕微的欺負。擔任被奈緒子玩弄的角色。具有[戀姐情結](../Page/戀姐情結.md "wikilink")。

  -
    美鈴的同學，眼鏡少女，有錢人家的千金小姐。喜歡外星人和宇宙等神秘事物，對美鈴有夾雜友情與戀情的強烈執著，美鈴沒依約前來參加學習會便精神崩壞。當美玲要來家裡拜訪時，興奮地成立專用[部落格](../Page/部落格.md "wikilink")，並且每天更新二十次。家裡有個[眼鏡娘](../Page/眼鏡娘.md "wikilink")[女僕](../Page/女僕.md "wikilink")。從初次見面起便被奈緒子稱呼為「眼鏡」。

  -
    涼太的同學，[傲嬌少女](../Page/傲嬌.md "wikilink")。在學校因為傲嬌中的傲部分而會欺負涼太，實際上喜歡涼太。胸部正在發育當中。

  -
    美鈴與涼太的母親，重度[天然呆](../Page/天然呆.md "wikilink")。

  -
    來自「男男星」的男男星人，在第19話首度登場，企圖將地球完全[BL化以征服地球](../Page/BL.md "wikilink")。比起同樣是外星人的奈緒子算是正常許多，擔任美鈴以外的第二吐槽角色。有喜歡[正太的傾向](../Page/正太.md "wikilink")。戴著眼鏡。

  -
    被封印在海底的古代邪神，是一隻巨大[頭足綱怪獸](../Page/頭足綱.md "wikilink")。在第17話中因奈緒子的[人造衛星](../Page/人造衛星.md "wikilink")[雷射光誤射入海底而解開封印](../Page/雷射.md "wikilink")，登上陸地襲擊人類。

  -
    美鈴的姊姊，並非來自百合星的奈緒子。

  -

## 出版書籍

  - 日文版

<!-- end list -->

  - 第1卷 2006年12月25日發行 ISBN 4-8402-3699-2、ISBN 978-4-8402-3699-7
  - 第2卷 2008年12月10日發行 ISBN 978-4-04-867503-1
  - 第3卷 2010年12月18日發行 ISBN 978-4-04-870208-9
  - 第4卷 2012年3月27日發行 ISBN 978-4-04-886498-5

<!-- end list -->

  - 中文版

<!-- end list -->

  - 第1卷 2007年12月20日發行 ISBN 978-986-174-551-0

## 主題曲

除第3卷外，收錄在[單行本初回限定版的特典](../Page/單行本.md "wikilink")[CD當中](../Page/CD.md "wikilink")，有別於動畫化主題曲。

**第1卷**

  - 主題曲「」樂曲提供：MOSAIC.WAV、歌： ／作詞、作曲、編曲：柏森進
  - 插曲「」樂曲提供：MOSAIC.WAV、歌： ／作詞、作曲、編曲：柏森進

**第2卷**

  - 第2季主題曲「」 樂曲制作：MOSAIC.WAV、歌：／作詞、作曲、編曲：柏森進

**第3卷**（附[OVA](../Page/OVA.md "wikilink")）

  - 片頭曲「」MOSAIC.WAV
  - 片尾曲「」MOSAIC.WAV

## OVA

2010年12月10日發售的日文版[單行本第](../Page/單行本.md "wikilink")3卷附贈[OVA](../Page/OVA.md "wikilink")。後納入「」品牌作品，並於2012年2月15日發行[Blu-ray](../Page/Blu-ray.md "wikilink")
& [DVD](../Page/DVD.md "wikilink")。

  - 工作人員

<!-- end list -->

  - 原作：kashmir
  - 導演、[人物設定](../Page/人物設定.md "wikilink")、編劇、分鏡、演出、作畫監督、原畫：[竹內哲也](../Page/竹內哲也.md "wikilink")
  - 色彩設計、色指定、上色檢査：千葉繪美
  - 美術監督、背景：三宅昌和
  - 攝影監督、攝影：吉川冴
  - 音樂：[MOSAIC.WAV](../Page/MOSAIC.WAV.md "wikilink")
  - [音響監督](../Page/音響監督.md "wikilink")：[岩浪美和](../Page/岩浪美和.md "wikilink")
  - 編集：神野學
  - 動畫製作：[ufotable](../Page/ufotable.md "wikilink")

## 參考資料

## 外部連結

  - [](http://www.animebunko.com/)

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:月刊Comic電擊大王連載作品](../Category/月刊Comic電擊大王連載作品.md "wikilink")
[Category:百合 (類型)](../Category/百合_\(類型\).md "wikilink")
[Category:電擊萌王](../Category/電擊萌王.md "wikilink")
[Category:人與外星生物接觸題材作品](../Category/人與外星生物接觸題材作品.md "wikilink")
[Category:2010年日本OVA動畫](../Category/2010年日本OVA動畫.md "wikilink")
[Category:ufotable](../Category/ufotable.md "wikilink")