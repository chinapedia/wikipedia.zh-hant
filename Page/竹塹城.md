**竹塹城**（**淡水廳城**），是指[臺灣在](../Page/臺灣.md "wikilink")[清治時代於今日](../Page/臺灣清治時期.md "wikilink")[新竹市中心所建的](../Page/新竹市.md "wikilink")[城廓](../Page/城郭.md "wikilink")，創立於[雍正元年](../Page/雍正.md "wikilink")（[1723年](../Page/1723年.md "wikilink")），是當時[淡水廳廳治](../Page/淡水廳.md "wikilink")(
北台灣行政中心
)所在。現存的磚石城池完成於[道光九年](../Page/道光.md "wikilink")（[1829年](../Page/1829年.md "wikilink")）。因此有時也用「竹塹城」一詞來代指新竹市。迎曦門是竹塹城現今唯一留存的城門，是中華民國[國定古蹟](../Page/國定古蹟.md "wikilink")。

## 歷史

[竹塹城迎曦門城樓.jpg](https://zh.wikipedia.org/wiki/File:竹塹城迎曦門城樓.jpg "fig:竹塹城迎曦門城樓.jpg")\]\]
[竹塹城迎曦門匾額.jpg](https://zh.wikipedia.org/wiki/File:竹塹城迎曦門匾額.jpg "fig:竹塹城迎曦門匾額.jpg")，左書「[道光](../Page/道光.md "wikilink")[戊子季冬](../Page/戊子.md "wikilink")」，右書「署[同知](../Page/同知.md "wikilink")[李慎彝監造](../Page/李慎彝.md "wikilink")」，[1828年](../Page/1828年.md "wikilink")\]\]
[竹塹城迎曦門城門洞.jpg](https://zh.wikipedia.org/wiki/File:竹塹城迎曦門城門洞.jpg "fig:竹塹城迎曦門城門洞.jpg")
[竹塹城迎曦門圓環樂池.jpg](https://zh.wikipedia.org/wiki/File:竹塹城迎曦門圓環樂池.jpg "fig:竹塹城迎曦門圓環樂池.jpg")
「竹塹」為[新竹的古稱](../Page/新竹.md "wikilink")，最早是[平埔族](../Page/平埔族.md "wikilink")[道卡斯族](../Page/道卡斯族.md "wikilink")「竹塹社」的所在地\[1\]。漢人約於[康熙五十年](../Page/康熙.md "wikilink")（[1711年](../Page/1711年.md "wikilink")）間，由[泉州移民](../Page/泉州.md "wikilink")[王世傑](../Page/王世傑_\(金門\).md "wikilink")（[閩南籍](../Page/閩南.md "wikilink")）來此開發，而在他率領親族鄉民來此屯墾後，此地逐漸成為清代[北台灣最重要的政經與文化中心](../Page/北台灣.md "wikilink")。當時竹塹隸屬於[臺灣府](../Page/臺灣府.md "wikilink")[諸羅縣](../Page/諸羅縣.md "wikilink")。在康熙六十年（[1721年](../Page/1721年.md "wikilink")）[朱一貴事件平息後](../Page/朱一貴事件.md "wikilink")，[清朝鑑於原諸羅縣轄區過大](../Page/清朝.md "wikilink")，統治鞭長莫及，於[雍正元年](../Page/雍正.md "wikilink")（[1723年](../Page/1723年.md "wikilink")）分[虎尾溪以北設](../Page/虎尾溪.md "wikilink")[彰化縣](../Page/彰化縣_\(清朝\).md "wikilink")\[2\]，同時設置[淡水捕盜同知專司北台灣事務](../Page/臺灣府淡水捕盜同知.md "wikilink")，竹塹被劃於其轄區內。直至雍正九年（[1731年](../Page/1731年.md "wikilink")）正式於[大甲溪以北至](../Page/大甲溪.md "wikilink")[雞籠分劃轄區](../Page/雞籠.md "wikilink")，設置[淡水廳](../Page/淡水廳.md "wikilink")，並改長官為[淡水撫民同知](../Page/臺灣府淡水撫民同知.md "wikilink")。

[清朝治臺初期](../Page/清朝.md "wikilink")，為了避免流民造反，曾經禁止台灣建造石磚城，因此，竹塹城早期的雛型，是以種植竹圍的方式代替磚牆。雍正十一年（1733年）[淡水海防廳從](../Page/淡水海防廳.md "wikilink")[彰化縣城正式移來竹塹時](../Page/彰化縣城.md "wikilink")，淡水同知[徐治民在四週遍植莿竹](../Page/徐治民.md "wikilink")，整個區域呈圓形，週長440丈（約1408公尺），有東、西、南、北四個門，為竹塹建城之始。到了1759年，莿竹均已腐杇。1806年，因[蔡牽等人侵擾臺灣沿岸](../Page/蔡牽.md "wikilink")，同知[胡應魁諭民造土城](../Page/胡應魁.md "wikilink")。七年後，同知[查廷華又再加高加寬土圍](../Page/查廷華.md "wikilink")。
1827年，進士[鄭用錫](../Page/鄭用錫.md "wikilink")（[閩南籍](../Page/閩南.md "wikilink")）等向前來巡視的[閩浙總督](../Page/閩浙總督.md "wikilink")[孫爾準倡議興建磚石城及四城門樓](../Page/孫爾準.md "wikilink")，獲清廷同意後，於次年（1828年）開始築造磚石城垣，周長為860丈（約2752公尺），牆高1丈5尺（4.8公尺），高度加[雉堞則為](../Page/雉堞.md "wikilink")1丈8尺（5.76公尺）。該城以[城隍廟為中心](../Page/城隍廟.md "wikilink")，東門為迎曦門、西門為挹爽門、南門為歌薰門、北門為拱宸門，而在東西南三門都設有砲臺一座，北門則為兩座。東門街、西門街、南門街、[北門街通往四門](../Page/北門街_\(新竹市\).md "wikilink")，加上上述的土圍，就構成了新竹街區，此外在北門外還有臺灣唯一的牌坊群，而西門石坊街上還有新竹歷史最悠久的楊氏節孝坊（1824年建）。磚石城牆於1829年秋完工，名為淡水廳城，又名竹塹城。竹塹城外挖築壕溝為護城河，並設吊橋兩座，長8.2公尺，寬1.6公尺，與[鳳山縣舊城東門段護城河同為目前臺灣僅存的護城河](../Page/鳳山縣舊城東門.md "wikilink")。此外由於磚石城周長比起之前的土城短很多，所以原本在土城裡的鄭氏[北郭園與竹蓮聚落等都變成城外](../Page/北郭園.md "wikilink")，後來又因附近的水田莊、湳雅莊等地興起，所以道光十九年（1839年）臺灣道[姚瑩便命淡水同知](../Page/姚瑩.md "wikilink")[龍大惇調查是否有增建土城的必要](../Page/龍大惇.md "wikilink")，而三年後（1842年）在[鴉片戰爭期間](../Page/第一次鴉片戰爭.md "wikilink")，有英國軍艦侵擾附近的大安港，故當時的同知[曹謹與仕紳在磚石城外加築一圈土城](../Page/曹謹.md "wikilink")，周長1495丈，城外植竹開溝，溝寬二丈，城高為一丈，另建有大小城門共八座，分別是大東門「賓暘門」、大西門「告成門」、大南門「解阜門」、大北門「承恩門」、小東門「卯耕門」、小西門「觀海門」、小南門「耀文門」與小北門「天樞門」。

進入[日治時期](../Page/臺灣日治時期.md "wikilink")，[1901年](../Page/1901年.md "wikilink")[北門大街](../Page/北門大街.md "wikilink")[金德美商號大火](../Page/金德美.md "wikilink")，將整個北門付之一炬。[1902年](../Page/1902年.md "wikilink")，[總督府實施](../Page/臺灣總督府.md "wikilink")[市區改正](../Page/市區改正.md "wikilink")，開始拓寬道路並拆掉城牆與城樓\[3\]，南門、西門也被拆除，只剩東門（迎曦門）存留至今。原來以防禦為用的[護城河](../Page/護城河.md "wikilink")，河水成為[灌溉用](../Page/灌溉.md "wikilink")[圳道](../Page/圳道.md "wikilink")——[振利圳](../Page/振利圳.md "wikilink")。

## 迎曦門

[竹塹城迎曦門遠景.jpg](https://zh.wikipedia.org/wiki/File:竹塹城迎曦門遠景.jpg "fig:竹塹城迎曦門遠景.jpg")
[竹塹城護城河.jpg](https://zh.wikipedia.org/wiki/File:竹塹城護城河.jpg "fig:竹塹城護城河.jpg")
[竹塹城護城河橋墩.jpg](https://zh.wikipedia.org/wiki/File:竹塹城護城河橋墩.jpg "fig:竹塹城護城河橋墩.jpg")
迎曦門（東門）是竹塹城僅存的城樓，位於[新竹火車站沿中正路直行約](../Page/新竹火車站.md "wikilink")400公尺處\[4\]，為[國定古蹟](../Page/國定古蹟.md "wikilink")。而[護城河經過整治後](../Page/護城河_\(新竹市\).md "wikilink")，結合現代建築，現為具休憩功能的親水公園\[5\]。護城河中有各種魚悠游水中，一旁的草地上也不時有文化藝術表演。城門為一幢二層樓的建築。城牆[雉堞為燕子磚砌成](../Page/雉堞.md "wikilink")，城樓下段為城座，以[唐山石及條形](../Page/唐山石.md "wikilink")[花崗石石塊疊砌而成](../Page/花崗石.md "wikilink")，城牆上鐫刻著時任署理[淡水撫民同知](../Page/臺灣府淡水撫民同知.md "wikilink")[李慎彝所題的](../Page/李慎彝.md "wikilink")「迎曦」二字。上層城樓原為木構建築，現已改為[混凝土造](../Page/混凝土.md "wikilink")，城樓結構共二十四根立柱，[屋簷為](../Page/屋簷.md "wikilink")[歇山重簷式建築](../Page/歇山頂.md "wikilink")，屋脊燕尾起翹，簷下懸掛著精雕[吊筒](../Page/吊筒.md "wikilink")。城門門洞採半圓拱型的造型\[6\]。

迎曦門除了本身為古蹟之外，城前的廣場也於1999年\[7\]被時任[新竹市長](../Page/新竹市長.md "wikilink")[蔡仁堅打造成](../Page/蔡仁堅.md "wikilink")[新竹之心](../Page/新竹之心.md "wikilink")，為一個結合傳統與現代科技的露天市民廣場，贏得了[2000年](../Page/2000年.md "wikilink")[台灣建築獎首獎與第二屆](../Page/台灣建築獎.md "wikilink")[遠東建築獎首獎兩項建築獎](../Page/遠東建築獎.md "wikilink")。廣場設計成地上地下各一層的展演場所\[8\]，一般民眾皆可向政府申請，提供表演或發展文藝空間（例如樂團的發表、慈善的捐款活動、政府推展的文藝活動等），每隔周禮拜周末都有由[頂尖音樂家族所舉辦的音樂表演](../Page/頂尖音樂家族.md "wikilink")。其中新竹之心為圓環中心，竹塹城歷史光廊將其與[護城河連接](../Page/護城河_\(新竹市\).md "wikilink")，讓人們了解竹塹城的由來。除了在假日的夜晚都會有活動之外，平日的夜晚偶爾也會有提供一些休閒娛樂。近年來更用[光雕的方式](../Page/光雕.md "wikilink")，將迎曦門與新竹之心打造為藝術作品，成為新竹的文化象徵地標\[9\]。

## 建築特色

  - [新竹迎曦門門樓使用重簷歇山頂](../Page/歇山頂.md "wikilink")
  - 閩南式建築列入古蹟

<!-- end list -->

  - [梁家輝於](../Page/梁家輝.md "wikilink")[1997年在此拍攝過香港電影](../Page/1997年.md "wikilink")《[情義之西西里島](../Page/黑金_\(電影\).md "wikilink")》，當時與[孫佳君演出](../Page/孫佳君.md "wikilink")[立法委員選舉造勢會場](../Page/1998年中華民國立法委員選舉.md "wikilink")。

## 淡水廳築城案卷

[新建台灣府淡水廳城碑記.jpg](https://zh.wikipedia.org/wiki/File:新建台灣府淡水廳城碑記.jpg "fig:新建台灣府淡水廳城碑記.jpg")
聯名具呈的有[鄭用錫](../Page/鄭用錫.md "wikilink")、[林長青](../Page/林長青.md "wikilink")、[郭成金](../Page/郭成金.md "wikilink")、[鄭用鑑](../Page/鄭用鑑.md "wikilink")、[溫斌元](../Page/溫斌元.md "wikilink")、[林國寶](../Page/林國寶.md "wikilink")、[劉獻廷](../Page/劉獻廷_\(台灣\).md "wikilink")、[鄭廷珪](../Page/鄭廷珪.md "wikilink")、[王奠邦](../Page/王奠邦.md "wikilink")、[林茂堂](../Page/林茂堂.md "wikilink")、[陳鳳鳴等其中除了竹塹在地主要的家族個人或舖戶代表外](../Page/陳鳳鳴.md "wikilink")，[板橋林家的](../Page/板橋林家.md "wikilink")[林平侯與](../Page/林平侯.md "wikilink")[大龍峒陳家的](../Page/大龍峒陳家.md "wikilink")[陳維藻也聯名參與](../Page/陳維藻.md "wikilink")，可見竹塹之建城不僅關係城內百姓的身家性命，也牽涉到整個[北臺灣社會與經濟之發展](../Page/北臺灣.md "wikilink")。

### 新建臺灣府淡水廳城碑記

  - [賜進士出身誥授](../Page/賜進士出身.md "wikilink")[朝議大夫](../Page/朝議大夫.md "wikilink")、[福建](../Page/福建.md "wikilink")[臺灣府知府](../Page/臺灣府知府.md "wikilink")、前[鹿仔港理番同知](../Page/鹿仔港理番同知.md "wikilink")、[閩縣](../Page/閩縣.md "wikilink")[武平](../Page/武平.md "wikilink")[羅源三縣](../Page/羅源.md "wikilink")[知縣](../Page/知縣.md "wikilink")、充嘉慶戊辰己卯兩科[鄉試同考官](../Page/鄉試.md "wikilink")，[浮梁](../Page/浮梁.md "wikilink")[鄧傳安撰](../Page/鄧傳安.md "wikilink")。
  - 賜進士出身誥授[奉直大夫](../Page/奉直大夫.md "wikilink")、候補[知州](../Page/知州.md "wikilink")、借補福建[臺灣府](../Page/臺灣府.md "wikilink")[澎湖通判署](../Page/澎湖通判.md "wikilink")[臺防同知事](../Page/臺防同知.md "wikilink")、前[連江縣知縣](../Page/連江縣.md "wikilink")、充嘉慶丁卯癸酉丙子道光辛巳四科鄉試同考官，[黃梅](../Page/黃梅.md "wikilink")[蔣鏞書](../Page/蔣鏞.md "wikilink")。
  - 賜進士出身敕授[承德郎](../Page/承德郎.md "wikilink")、福建臺灣府[噶瑪蘭通判署](../Page/噶瑪蘭通判.md "wikilink")[淡水同知事](../Page/淡水同知.md "wikilink")、前[臺灣](../Page/臺灣縣_\(1684年-1887年\).md "wikilink")[晉江](../Page/晉江縣.md "wikilink")[沙縣](../Page/沙縣.md "wikilink")[清流等縣知縣](../Page/清流縣.md "wikilink")、充嘉慶戊辰癸酉道光壬午三科鄉試同考官，[威遠](../Page/威遠.md "wikilink")[李慎彝篆額](../Page/李慎彝.md "wikilink")。

## 參見

  - [北門街商圈](../Page/北門街商圈.md "wikilink")
  - [李慎彝](../Page/李慎彝.md "wikilink")
  - [塹港富美宮](../Page/塹港富美宮.md "wikilink")

## 附註

<div class="references-small">

</div>

## 參考資料

  - 新竹文化地圖，潘國正編，1997

## 外部連結

  - [新竹市政府全球資訊網](http://www.hccg.gov.tw)
  - [新竹文化歷史古蹟](http://cyberfair.taiwanschoolnet.org/cyberfair2000/cf0001/26300031/index.htm)
  - [中央氣象局24H全天監視竹塹城即時影像](http://www.cwb.gov.tw/V6/observe/webcam/webcam_video_40.php)
  - [新竹的發展](http://disp.cc/b/215-1t7M)

<onlyinclude></onlyinclude>

[Category:新竹市古蹟](../Category/新竹市古蹟.md "wikilink")
[Category:新竹市旅遊景點](../Category/新竹市旅遊景點.md "wikilink")
[Category:臺灣城池](../Category/臺灣城池.md "wikilink")
[Category:台灣清治時期建築](../Category/台灣清治時期建築.md "wikilink")
[Category:東區 (新竹市)](../Category/東區_\(新竹市\).md "wikilink")
[Category:1723年台灣建立](../Category/1723年台灣建立.md "wikilink")

1.

2.
3.

4.

5.

6.
7.
8.
9.