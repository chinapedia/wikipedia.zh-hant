在**[足球](../Page/足球.md "wikilink")**運動中，**隊長**（[英文](../Page/英文.md "wikilink")：**Captain**，又稱**Skipper**）是一位被委任作為球場上球隊領袖的球員。隊長通常由陣中經驗豐富的球員擔任。在比賽中，隊長會佩帶隊長臂章作賽以資識別。

根據[足球比賽規則規定](../Page/足球比賽規則.md "wikilink")，隊長的職責為在賽前及互射[-{zh-cn:点球;zh-hk:十二碼球}-前參與](../Page/点球.md "wikilink")[掷硬币](../Page/掷硬币.md "wikilink")（以此確定比賽哪一方開球）。隊長並無權力挑戰[-{zh-cn:裁判;zh-hk:球證}-的判決](../Page/裁判.md "wikilink")，但-{zh-cn:裁判;zh-hk:球證}-不時會對球隊隊長講述球隊的紀律問題。

在職業球會中，隊長的任期通常為一個-{zh-cn:赛季;zh-hk:球季}-，但如果隊長某一場比賽不在陣中，則副隊長會代替隊長作賽。

## [FIFA世界盃冠軍球隊隊長](../Page/世界盃足球賽.md "wikilink")

  - [1930](../Page/1930年世界盃足球賽.md "wikilink") -
    [-{zh-cn:纳萨兹;zh-hk:拿沙斯}-](../Page/拿沙斯.md "wikilink")（ ,
    [烏拉圭](../Page/烏拉圭國家足球隊.md "wikilink")）
  - [1934](../Page/1934年世界盃足球賽.md "wikilink") -
    [詹皮耶羅·孔比](../Page/詹皮耶羅·孔比.md "wikilink")（Gianpiero
    Combi , [意大利](../Page/意大利國家足球隊.md "wikilink")）
  - [1938](../Page/1938年世界盃足球賽.md "wikilink") -
    [-{zh-cn:朱塞佩阿查;zh-hk:基奧斯比·梅亞沙;zh-tw:基奧斯比·梅亞沙;}-](../Page/朱塞佩·梅阿查.md "wikilink")（Giuseppe
    Meazza , [意大利](../Page/意大利國家足球隊.md "wikilink")）
  - [1950](../Page/1950年世界盃足球賽.md "wikilink") -
    [-{zh-cn:巴雷拉;zh-hk:維利拿}-](../Page/維利拿.md "wikilink")（，[烏拉圭](../Page/烏拉圭國家足球隊.md "wikilink")）
  - [1954](../Page/1954年世界盃足球賽.md "wikilink") -
    [-{zh-cn:弗雷茨·沃尔特;zh-hk:費斯·禾達;zh-tw:弗里茨·瓦爾特;}-](../Page/弗里茨·瓦爾特.md "wikilink")（Fritz
    Walter , [西德](../Page/西德國家足球隊.md "wikilink")）
  - [1958](../Page/1958年世界盃足球賽.md "wikilink") -
    [-{zh-cn:贝里尼;zh-hk:比連尼;zh-tw:貝里尼;}-](../Page/比連尼.md "wikilink")（，[巴西](../Page/巴西國家足球隊.md "wikilink")）
  - [1962](../Page/1962年世界盃足球賽.md "wikilink") -
    [-{zh-cn:毛罗;zh-hk:摩爾奴}-](../Page/摩爾奴.md "wikilink")
    （，[巴西](../Page/巴西國家足球隊.md "wikilink")）
  - [1966](../Page/1966年世界盃足球賽.md "wikilink") -
    [-{zh-cn:博比·摩尔;zh-hk:卜比·摩亞;zh-tw:博比·摩爾;}-](../Page/博比·摩爾.md "wikilink")（Bobby
    Moore , [英格蘭](../Page/英格蘭國家足球隊.md "wikilink")）
  - [1970](../Page/1970年世界盃足球賽.md "wikilink") -
    [-{zh-cn:卡洛斯·阿尔伯托;zh-hk:卡路士·阿爾拔圖;zh-tw:卡洛斯·阿爾貝托;}-](../Page/卡洛斯·阿尔贝托.md "wikilink")（Carlos
    Alberto，[巴西](../Page/巴西國家足球隊.md "wikilink")）
  - [1974](../Page/1974年世界盃足球賽.md "wikilink") -
    [-{zh-cn:贝肯鲍尔;zh-hk:碧根鮑華;zh-tw:貝肯鮑爾;}-](../Page/贝肯鲍尔.md "wikilink")（Franz
    Beckenbauer , [西德](../Page/西德國家足球隊.md "wikilink")）
  - [1978](../Page/1978年世界盃足球賽.md "wikilink") -
    [-{zh-cn:帕萨雷拉;zh-hk:巴薩里拉;zh-tw:帕薩雷拉;}-](../Page/達尼埃爾·帕薩雷拉.md "wikilink")（Daniel
    Passarella , [阿根廷](../Page/阿根廷國家足球隊.md "wikilink")）
  - [1982](../Page/1982年世界盃足球賽.md "wikilink") -
    [-{zh-cn:佐夫;zh-hk:索夫;zh-tw:佐夫;}-](../Page/佐夫.md "wikilink")（Dino
    Zoff , [意大利](../Page/意大利國家足球隊.md "wikilink")）
  - [1986](../Page/1986年世界盃足球賽.md "wikilink") -
    [-{zh-cn:马拉多纳;zh-hk:馬勒當拿;zh-tw:馬拉度納;}-](../Page/马拉多纳.md "wikilink")（Diego
    Maradona , [阿根廷](../Page/阿根廷國家足球隊.md "wikilink")）
  - [1990](../Page/1990年世界盃足球賽.md "wikilink") -
    [-{zh-cn:马特乌斯;zh-hk:馬圖斯;zh-tw:馬特烏斯;}-](../Page/马特乌斯.md "wikilink")（Lothar
    Matthäus , [西德](../Page/西德國家足球隊.md "wikilink")）
  - [1994](../Page/1994年世界盃足球賽.md "wikilink") -
    [鄧加](../Page/鄧加.md "wikilink")（Dunga ,
    [巴西](../Page/巴西國家足球隊.md "wikilink")）
  - [1998](../Page/1998年世界盃足球賽.md "wikilink") -
    [-{zh-cn:德尚;zh-hk:迪甘斯;zh-tw:德尚;}-](../Page/迪迪埃·德尚.md "wikilink")（Didier
    Deschamps , [法國](../Page/法國國家足球隊.md "wikilink")）
  - [2002](../Page/2002年世界盃足球賽.md "wikilink") -
    [-{zh-cn:卡福;zh-hk:卡富;zh-tw:卡富;}-](../Page/卡福.md "wikilink")（Cafu
    , [巴西](../Page/巴西國家足球隊.md "wikilink")）
  - [2006](../Page/2006年世界盃足球賽.md "wikilink") -
    [-{zh-cn:卡纳瓦罗;zh-hk:簡拿華路;zh-tw:卡納瓦羅;}-](../Page/卡纳瓦罗.md "wikilink")（Fabio
    Cannavaro[意大利](../Page/意大利國家足球隊.md "wikilink")）
  - [2010](../Page/2010年世界盃足球賽.md "wikilink") -
    [-{zh-cn:卡西利亚斯;zh-hk:卡斯拿斯;zh-tw:卡西利亞斯;}-](../Page/卡斯拿斯.md "wikilink")（Iker
    Casillas , [西班牙](../Page/西班牙國家足球隊.md "wikilink"))
  - [2014](../Page/2014年世界盃足球賽.md "wikilink") -
    [-{zh-cn:拉姆;zh-hk:拿姆;zh-tw:拉姆;}-](../Page/菲利普·拉姆.md "wikilink")（Philipp
    Lahm , [德国](../Page/德国國家足球隊.md "wikilink"))
  - [2018](../Page/2018年世界盃足球賽.md "wikilink") - [-{zh-hans:洛里斯;
    zh-hk:洛里斯;}-](../Page/雨果·洛里斯.md "wikilink")（Hugo Lloris,
    [法國](../Page/法國國家足球隊.md "wikilink")）

## 參見

  - [隊長](../Page/隊長.md "wikilink")

[Category:足球術語](../Category/足球術語.md "wikilink")