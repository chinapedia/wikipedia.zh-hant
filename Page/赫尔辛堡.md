**赫尔辛堡**（****）位于[瑞典最南部](../Page/瑞典.md "wikilink")[斯科讷省](../Page/斯科讷省.md "wikilink")，一共有101,600人口。整个赫尔辛堡市有122,000人口，是瑞典第九大市。

赫尔辛堡是瑞典距离[丹麦的最近点](../Page/丹麦.md "wikilink")，隔着[厄勒海峡和丹麦城市](../Page/厄勒海峡.md "wikilink")[赫尔辛格相望](../Page/赫尔辛格.md "wikilink")。

有很多古老的建筑的赫尔辛堡，历史悠久，是一个海滨风景胜地。城市的建筑是旧风格石头建造的教堂，市中心的边塞和更多现代的商业建筑混合而成。街道类型很多，从宽广的大道到狭小的小径。

赫尔辛堡有一支顶级的足球队，[赫尔辛堡足球俱乐部](../Page/赫尔辛堡足球俱乐部.md "wikilink")，主场是奥林匹克球场。[亨里克·拉尔森是俱乐部最著名的球员](../Page/亨里克·拉尔森.md "wikilink")。

## 介绍

赫尔辛堡是一个主要的贸易，海运，商业中心。在2001年，在亨利·邓肯资助的下，一座以前的橡胶工厂的建筑，成为[隆德大学分部的赫尔辛堡校区](../Page/隆德.md "wikilink")。

三家轮渡公司昼夜不停在瑞典和丹麦之间运送人和卡车。据说依然有很多人花费20分钟的旅程去赫尔辛格，以便在那里享受更好的酒精的税率，尽管最近汇率的改变使这个好处变小了。的确，可以说丹麦城市赫尔辛格的财政收入的很大一部分来自瑞典的酒精旅游者。现在更主要是作为[哥本哈根的一日游路线](../Page/哥本哈根.md "wikilink")，或者仅仅是在渡船上欣赏美景。

[宜家](../Page/宜家.md "wikilink")，家具和家庭日用品的零售商，在赫尔辛堡建立了全球运作总部。

[Nicorette](../Page/Nicorette.md "wikilink")，著名的针对有毒瘾者特制的口香糖，在这里有一个制造工厂。

## 政治

### 街区

赫尔辛堡分成32个街区。

| 赫尔辛堡街区 <small>(2006年1月9日分类和人口调查)</small> |
| ---------------------------------------- |
| 1                                        |
| 2                                        |
| 3                                        |
| 4                                        |
| 5                                        |
| 6                                        |
| 7\*                                      |
| 8                                        |
| 9                                        |
| 10                                       |
| 11                                       |

## 著名人物

  - [迪特里希·柏格茲特胡德](../Page/迪特里希·柏格茲特胡德.md "wikilink")， 作曲家，风琴演奏家
  - [亨里克·拉尔森](../Page/亨里克·拉尔森.md "wikilink")， 足球运动员
  - [彭圖斯·法內魯德](../Page/彭圖斯·法內魯德.md "wikilink")， 足球运动员
  - [漢斯·艾爾佛森](../Page/漢斯·艾爾佛森.md "wikilink")， 喜剧演员
  - [亨利·邓肯](../Page/亨利·邓肯.md "wikilink")，实业家
  - [瑪雅·古爾斯特蘭德](../Page/瑪雅·古爾斯特蘭德.md "wikilink"), 音乐家
  - [鲍伯·海倫芬](../Page/鲍伯·海倫芬.md "wikilink")， 诗人，作家
  - [卡尔·Kruszelnicki](../Page/卡尔·Kruszelnicki.md "wikilink"), Dr Karl
  - [安德斯·莉亞](../Page/安德斯·莉亞.md "wikilink")， 冰球运动员
  - [蒂娜·努德斯特倫](../Page/蒂娜·努德斯特倫.md "wikilink")， 电视制作人
  - Peps Persson, 音乐家
  - [Ruben Rausing](../Page/Ruben_Rausing.md "wikilink") ，利乐包装创始人
  - Östen Warnerbring, 音乐家
  - Johan Wissman，田径运动员
  - [The Sounds](../Page/The_Sounds.md "wikilink")，摇滚乐队
  - [Darkane](../Page/Darkane.md "wikilink")， 金属乐队
  - [Soilwork](../Page/Soilwork.md "wikilink")， 金属乐队

## 风景

[Helsingborg-300px.jpg](https://zh.wikipedia.org/wiki/File:Helsingborg-300px.jpg "fig:Helsingborg-300px.jpg")
[Helsingborg_port.jpg](https://zh.wikipedia.org/wiki/File:Helsingborg_port.jpg "fig:Helsingborg_port.jpg")

[Helsingborg_Stairs_to_Fortress.jpg](https://zh.wikipedia.org/wiki/File:Helsingborg_Stairs_to_Fortress.jpg "fig:Helsingborg_Stairs_to_Fortress.jpg"),
赫尔辛堡市中心\]\]
[Sankta_Maria_kyrka_(Helsingborg,_Sweden).jpg](https://zh.wikipedia.org/wiki/File:Sankta_Maria_kyrka_\(Helsingborg,_Sweden\).jpg "fig:Sankta_Maria_kyrka_(Helsingborg,_Sweden).jpg")

## 外部链接

  - [赫尔辛堡官方主页](http://www.helsingborg.se/)
  - [赫尔辛堡官方主页](https://web.archive.org/web/20070109235740/http://www.helsingborg.se/Main.aspx?id=5)

[H](../Category/瑞典城市.md "wikilink")