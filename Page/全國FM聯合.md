**全國FM聯合**（）是由[日本全國所有加盟](../Page/日本.md "wikilink")[日本民間放送聯盟的](../Page/日本民間放送聯盟.md "wikilink")52家民營[FM](../Page/調頻廣播.md "wikilink")[廣播電台組成的](../Page/廣播電台.md "wikilink")[廣播網](../Page/廣播網.md "wikilink")。

## 加盟局

※
本表按「[北海道](../Page/北海道.md "wikilink")-[東北](../Page/東北地方.md "wikilink")-[關東](../Page/關東.md "wikilink")-[甲信越](../Page/甲信越.md "wikilink")-[北陸](../Page/北陸.md "wikilink")-[靜岡](../Page/靜岡.md "wikilink")-[東海](../Page/東海.md "wikilink")-[近畿](../Page/近畿.md "wikilink")-[中](../Page/中国地方.md "wikilink")[四国](../Page/四国.md "wikilink")-[九州](../Page/九州.md "wikilink")[沖繩](../Page/沖繩.md "wikilink")」的順序記載。
※
所属系列（加盟局）的表示記號：[JFN](../Page/JFN.md "wikilink")＝（●）、[JFL](../Page/JAPAN_FM_LEAGUE.md "wikilink")＝（▲）、[MegaNet](../Page/MegaNet.md "wikilink")＝（■）、独立局＝（★）

<table>
<thead>
<tr class="header">
<th><p><strong><a href="../Page/放送對象地區.md" title="wikilink">放送對象地區</a></strong></p></th>
<th><p><strong>通称</strong> <small>（愛称または略称）</p></th>
<th><p><strong>放送局名</strong></p></th>
<th><p><strong>所属系列</strong></p></th>
<th><p><strong>備考</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td><p><strong>AIR-G</strong>'</p></td>
<td></td>
<td><p>●</p></td>
<td><p>頻率為 FM 80.4（札幌）</p></td>
</tr>
<tr class="even">
<td><p><strong>NORTH WAVE</strong></p></td>
<td><p><a href="../Page/FM・NORTH_WAVE.md" title="wikilink">FM・NORTH WAVE</a></p></td>
<td><p>▲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/青森縣.md" title="wikilink">青森縣</a></p></td>
<td><p><strong>FM青森</strong></p></td>
<td></td>
<td><p>●</p></td>
<td><p>頻率為 FM 80.0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岩手縣.md" title="wikilink">岩手縣</a></p></td>
<td><p><strong>FM岩手</strong></p></td>
<td><p><a href="../Page/FM岩手.md" title="wikilink">FM岩手</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a></p></td>
<td><p><strong>Date fm</strong></p></td>
<td><p><a href="../Page/FM仙台.md" title="wikilink">FM仙台</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秋田縣.md" title="wikilink">秋田縣</a></p></td>
<td><p><strong>AFM</strong></p></td>
<td><p><a href="../Page/FM秋田.md" title="wikilink">FM秋田</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><strong>Boy-FM</strong></p></td>
<td><p><a href="../Page/FM山形.md" title="wikilink">FM山形</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a></p></td>
<td><p><strong>ふくしまFM</strong></p></td>
<td><p><a href="../Page/FM福島.md" title="wikilink">FM福島</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/栃木縣.md" title="wikilink">栃木縣</a></p></td>
<td><p><strong>RADIO BERRY</strong></p></td>
<td><p><a href="../Page/FM栃木.md" title="wikilink">FM栃木</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/群馬縣.md" title="wikilink">群馬縣</a></p></td>
<td><p><strong>FMぐんま</strong></p></td>
<td><p><a href="../Page/FM群馬.md" title="wikilink">FM群馬</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/埼玉縣.md" title="wikilink">埼玉縣</a></p></td>
<td><p><strong>NACK5</strong></p></td>
<td><p><a href="../Page/FM_NACK_FIVE.md" title="wikilink">FM NACK FIVE</a></p></td>
<td><p>★</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/千葉縣.md" title="wikilink">千葉縣</a></p></td>
<td><p><strong>bayfm</strong></p></td>
<td><p><a href="../Page/Bay_FM.md" title="wikilink">Bay FM</a></p></td>
<td><p>★</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p><strong>TOKYO FM</strong></p></td>
<td><p><a href="../Page/FM東京.md" title="wikilink">FM東京</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>J-WAVE</strong></p></td>
<td><p><a href="../Page/J-WAVE.md" title="wikilink">J-WAVE</a></p></td>
<td><p>▲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>InterFM</strong></p></td>
<td><p><a href="../Page/FM_INTER_WAVE.md" title="wikilink">FM INTER WAVE</a></p></td>
<td><p>■</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
<td><p><strong>Fm yokohama</strong></p></td>
<td><p><a href="../Page/横濱FM放送.md" title="wikilink">横濱FM放送</a></p></td>
<td><p>★</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山梨縣.md" title="wikilink">山梨縣</a></p></td>
<td><p><strong>FM-FUJI</strong></p></td>
<td><p><a href="../Page/FM富士.md" title="wikilink">FM富士</a></p></td>
<td><p>★</p></td>
<td><p>1992年前為●</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/長野縣.md" title="wikilink">長野縣</a></p></td>
<td><p><strong>FM長野</strong></p></td>
<td><p><a href="../Page/長野FM放送.md" title="wikilink">長野FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><strong>FM-NIIGATA</strong></p></td>
<td><p><a href="../Page/FM廣播新潟.md" title="wikilink">FM廣播新潟</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>FM PORT</strong></p></td>
<td><p><a href="../Page/新潟縣民FM放送.md" title="wikilink">新潟縣民FM放送</a></p></td>
<td><p>★</p></td>
<td><p>開局直後は▲だった</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p><strong>FMとやま</strong></p></td>
<td><p><a href="../Page/富山FM放送.md" title="wikilink">富山FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石川縣.md" title="wikilink">石川縣</a></p></td>
<td><p><strong>HELLO FIVE</strong></p></td>
<td><p><a href="../Page/FM石川.md" title="wikilink">FM石川</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a></p></td>
<td><p><strong>FM福井</strong></p></td>
<td><p><a href="../Page/福井FM放送.md" title="wikilink">福井FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/静岡縣.md" title="wikilink">静岡縣</a></p></td>
<td><p><strong>K-MIX</strong></p></td>
<td><p><a href="../Page/静岡FM放送.md" title="wikilink">静岡FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛知縣.md" title="wikilink">愛知縣</a></p></td>
<td><p><strong>FM AICHI</strong></p></td>
<td><p><a href="../Page/FM愛知.md" title="wikilink">FM愛知</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>ZIP-FM</strong></p></td>
<td><p><a href="../Page/ZIP-FM.md" title="wikilink">ZIP-FM</a></p></td>
<td><p>▲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>RADIO-i</strong></p></td>
<td></td>
<td><p>■</p></td>
<td><p>2010年9月30日停播，自動退會</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岐阜縣.md" title="wikilink">岐阜縣</a></p></td>
<td><p><strong>Radio 80</strong></p></td>
<td><p><a href="../Page/岐阜FM放送.md" title="wikilink">岐阜FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三重縣.md" title="wikilink">三重縣</a></p></td>
<td><p><strong>radio CUBE FM三重</strong></p></td>
<td><p><a href="../Page/三重FM放送.md" title="wikilink">三重FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/滋賀縣.md" title="wikilink">滋賀縣</a></p></td>
<td><p><strong>e-radio</strong></p></td>
<td><p><a href="../Page/FM滋賀.md" title="wikilink">FM滋賀</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a></p></td>
<td><p><strong>α-STATION</strong></p></td>
<td><p><a href="../Page/FM京都.md" title="wikilink">FM京都</a></p></td>
<td><p>★</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a></p></td>
<td><p><strong>fm osaka</strong></p></td>
<td><p><a href="../Page/FM大阪.md" title="wikilink">FM大阪</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>FM802</strong></p></td>
<td><p><a href="../Page/FM802.md" title="wikilink">FM802</a></p></td>
<td><p>▲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>FM CO・CO・LO</strong></p></td>
<td><p><a href="../Page/關西Inter_Media.md" title="wikilink">關西Inter Media</a></p></td>
<td><p>■</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a></p></td>
<td><p><strong>Kiss-FM KOBE</strong></p></td>
<td><p><a href="../Page/Kiss-FM_KOBE.md" title="wikilink">Kiss-FM KOBE</a></p></td>
<td><p>●</p></td>
<td><p>2003年3月31日前為★</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鳥取縣.md" title="wikilink">鳥取縣</a></p></td>
<td><p><strong>V-air</strong></p></td>
<td><p><a href="../Page/FM山陰.md" title="wikilink">FM山陰</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/島根縣.md" title="wikilink">島根縣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岡山縣.md" title="wikilink">岡山縣</a></p></td>
<td><p><strong>FM岡山</strong></p></td>
<td><p><a href="../Page/岡山FM放送.md" title="wikilink">岡山FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廣島縣.md" title="wikilink">廣島縣</a></p></td>
<td><p><strong>HFM</strong></p></td>
<td><p><a href="../Page/廣島FM放送.md" title="wikilink">廣島FM放送</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山口縣.md" title="wikilink">山口縣</a></p></td>
<td><p><strong>FMY</strong></p></td>
<td><p><a href="../Page/FM山口.md" title="wikilink">FM山口</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德島縣.md" title="wikilink">德島縣</a></p></td>
<td><p><strong>FMとくしま</strong></p></td>
<td><p><a href="../Page/FM德島.md" title="wikilink">FM德島</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香川縣.md" title="wikilink">香川縣</a></p></td>
<td><p><strong>FM香川</strong></p></td>
<td><p><a href="../Page/FM香川.md" title="wikilink">FM香川</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛媛縣.md" title="wikilink">愛媛縣</a></p></td>
<td><p><strong>JOEU-FM</strong></p></td>
<td><p><a href="../Page/FM愛媛.md" title="wikilink">FM愛媛</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高知縣.md" title="wikilink">高知縣</a></p></td>
<td><p><strong>Hi-Six</strong></p></td>
<td><p><a href="../Page/FM高知.md" title="wikilink">FM高知</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福岡縣.md" title="wikilink">福岡縣</a></p></td>
<td><p><strong>FM　FUKUOKA</strong></p></td>
<td><p><a href="../Page/FM福岡.md" title="wikilink">FM福岡</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>CROSS FM</strong></p></td>
<td><p><a href="../Page/FM九州.md" title="wikilink">FM九州</a></p></td>
<td><p>▲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>LOVE FM</strong></p></td>
<td><p><a href="../Page/九州國際FM.md" title="wikilink">九州國際FM</a></p></td>
<td><p>■</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐賀縣.md" title="wikilink">佐賀縣</a></p></td>
<td><p><strong>FMS</strong></p></td>
<td><p><a href="../Page/FM佐賀.md" title="wikilink">FM佐賀</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a></p></td>
<td><p><strong>fm nagasaki</strong></p></td>
<td><p><a href="../Page/FM長崎.md" title="wikilink">FM長崎</a></p></td>
<td><p>●</p></td>
<td><p>2007年3月31日前愛称是Smile-FM</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊本縣.md" title="wikilink">熊本縣</a></p></td>
<td><p><strong>FMK</strong></p></td>
<td><p><a href="../Page/FM熊本.md" title="wikilink">FM熊本</a></p></td>
<td><p>●</p></td>
<td><p>2005年3月31日前公司名為<strong>FM中九州</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大分縣.md" title="wikilink">大分縣</a></p></td>
<td><p><strong>Air-Radio FM88</strong></p></td>
<td><p><a href="../Page/FM大分.md" title="wikilink">FM大分</a></p></td>
<td><p>●</p></td>
<td><p>1991年9月30日前為★</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮崎縣.md" title="wikilink">宮崎縣</a></p></td>
<td><p><strong>JOY FM</strong></p></td>
<td><p><a href="../Page/FM宮崎.md" title="wikilink">FM宮崎</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鹿兒島縣.md" title="wikilink">鹿兒島縣</a></p></td>
<td><p><strong>μFM</strong></p></td>
<td><p><a href="../Page/FM鹿兒島.md" title="wikilink">FM鹿兒島</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沖繩縣.md" title="wikilink">沖繩縣</a></p></td>
<td><p><strong>FM沖繩</strong></p></td>
<td><p><a href="../Page/FM沖繩.md" title="wikilink">FM沖繩</a></p></td>
<td><p>●</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:日本媒體](../Category/日本媒體.md "wikilink")
[Category:電台網](../Category/電台網.md "wikilink")