**H**, **h**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")8个[字母](../Page/字母.md "wikilink")。由希腊字母[Η演变而成](../Page/Η.md "wikilink")。拉丁字母Η和西里爾字母Η，除了形狀相似外，並無其他關係。

闪族语字母[ח](../Page/ח.md "wikilink")（Ħêt）可能表示[音位](../Page/音位.md "wikilink")/χ/（咽部辅音摩擦音，[国际音标为](../Page/国际音标.md "wikilink")\[ħ\]）。wh这个字母的形式可能表示一个篱笆。在早期的[希腊语中H表示](../Page/希腊语.md "wikilink")/h/，但是后来，[Η或](../Page/Η.md "wikilink")*η*（Êta）表示了/E:/。在现代希腊语中
，这个音位与/i/结合起来，类似于英语中EA /E:/和EE
/e:/都表示发音/i:/。在[伊特鲁里亚语和](../Page/伊特鲁里亚语.md "wikilink")[拉丁语中](../Page/拉丁语.md "wikilink")，仍然保留着/h/的发音，但是在所有的罗曼语系语言中，发音都丢失了，只有[罗马尼亚语从邻近的斯拉夫语言中借来了](../Page/罗马尼亚语.md "wikilink")/h/这个发音，以及在一些使用西班牙语的国家，[西班牙语的](../Page/西班牙语.md "wikilink")/x/发展成为\[h\]的[语音变体](../Page/语音变体.md "wikilink")。

## 字符编码

| 字符编码                              | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| --------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写H](../Page/大写字母.md "wikilink") | 72                                   | 0048                                     | 200                                    | `····`                             |
| [小写h](../Page/小写字母.md "wikilink") | 104                                  | 0068                                     | 136                                    |                                    |

Gjgc

## 其他表示方法

## 参看

### h的变体

  - （半h）

  - \[\] [清會厭擦音](../Page/清會厭擦音.md "wikilink")

  - \[\] [唇硬顎無擦通音](../Page/唇硬顎無擦通音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母Eta](../Page/希腊字母.md "wikilink")）

  - （[西里尔字母Shheta](../Page/西里尔字母.md "wikilink")，在[哈萨克语使用](../Page/哈萨克语.md "wikilink")）

### 与H相似但无任何关系的字母

  - （西里尔字母Nu）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")