**-{zh-hans:尤西比奥·达席尔瓦·费雷拉;
zh-hant:尤西比奧·達施華·費利拿;}-**（，）\[1\]，前[葡萄牙著名足球員](../Page/葡萄牙.md "wikilink")，是20世紀60年代一位具傳奇性而出色的射手。是世界球壇中的傳奇巨星之一。2004年，他位列球王[比利評選的](../Page/比利.md "wikilink")[125位最出色的在世球星（FIFA
100）中。](../Page/FIFA_100.md "wikilink")

有「黑豹」之稱的尤西比奧出生於[葡萄牙管辖的](../Page/葡萄牙.md "wikilink")[非洲](../Page/非洲.md "wikilink")[莫桑比克的首都洛倫索](../Page/莫桑比克.md "wikilink")－馬貴斯（Lourenco
Marques，現在稱為[馬普托](../Page/馬普托.md "wikilink")），後來成為了[葡萄牙國家足球隊的前鋒](../Page/葡萄牙國家足球隊.md "wikilink")，而尤西比奧最著名的是他的速度驚人。

他不但為葡萄牙奪得[1966年世界杯的季軍](../Page/1966年世界杯.md "wikilink")，更是該屆的神射手。在該屆世界杯，葡萄牙在八強賽中，對手是當時表現驚人的[北韓](../Page/北韓國家足球隊.md "wikilink")，葡萄牙先落後三球，但憑著尤西比奧的力挽狂瀾最終以5:3反勝，繼續晉級。

2014年，尤西比奥因心脏衰竭当地时间1月5日凌晨在[里斯本去世](../Page/里斯本.md "wikilink")。\[2\]\[3\]\[4\]葡萄牙宣布全国为他的逝世哀悼三天\[5\]。
在2007年票選中，他排名第15。

## 榮譽

  - 1961: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [洲際盃亞軍](../Page/洲際盃足球賽.md "wikilink")
  - 1962: [歐洲冠軍球會盃](../Page/歐洲聯賽冠軍盃.md "wikilink"),
    [葡萄牙盃](../Page/葡萄牙盃.md "wikilink"),
    [洲際盃亞軍](../Page/洲際盃足球賽.md "wikilink")
  - 1963: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [歐洲冠軍球會盃亞軍](../Page/歐洲聯賽冠軍盃.md "wikilink")
  - 1964: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [葡萄牙盃](../Page/葡萄牙盃.md "wikilink")
  - 1965: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [歐洲冠軍球會盃亞軍](../Page/歐洲聯賽冠軍盃.md "wikilink"),
    [歐洲足球先生](../Page/歐洲足球先生.md "wikilink")
  - 1966: [世界盃](../Page/世界盃足球賽.md "wikilink") 第三名
  - 1966: [世界盃金靴獎](../Page/世界盃金靴獎.md "wikilink")（9球）
  - 1967: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink")
  - 1968: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [歐洲冠軍球會盃亞軍](../Page/歐洲聯賽冠軍盃.md "wikilink")
  - 1969: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [葡萄牙盃](../Page/葡萄牙盃.md "wikilink")
  - 1970: [葡萄牙盃](../Page/葡萄牙盃.md "wikilink")
  - 1971: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink")
  - 1972: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink"),
    [葡萄牙盃](../Page/葡萄牙盃.md "wikilink")
  - 1973: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink")
  - 1974: [舊葡甲冠軍](../Page/葡萄牙足球超级联赛.md "wikilink")
  - 1976: Mexican Championship
  - 1976: [北美足球聯賽](../Page/美國職業足球大聯盟.md "wikilink")（NASL）冠軍

## 參考資料

## 外部链接

  -
[Category:葡萄牙國家足球隊球員](../Category/葡萄牙國家足球隊球員.md "wikilink")
[Category:葡萄牙旅外足球運動員](../Category/葡萄牙旅外足球運動員.md "wikilink")
[Category:葡萄牙足球运动员](../Category/葡萄牙足球运动员.md "wikilink")
[Category:莫桑比克足球運動員](../Category/莫桑比克足球運動員.md "wikilink")
[Category:賓菲加球員](../Category/賓菲加球員.md "wikilink") [Category:FIFA
100](../Category/FIFA_100.md "wikilink")
[Category:歐洲足球先生](../Category/歐洲足球先生.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:比拉馬球員](../Category/比拉馬球員.md "wikilink")
[Category:蒙特雷球員](../Category/蒙特雷球員.md "wikilink")
[Category:1966年世界盃足球賽球員](../Category/1966年世界盃足球賽球員.md "wikilink")
[Category:非洲裔葡萄牙人](../Category/非洲裔葡萄牙人.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:墨甲球員](../Category/墨甲球員.md "wikilink")
[Category:馬普托人](../Category/馬普托人.md "wikilink")

1.  [葡足壇巨星
    黑豹尤西比奧辭世](http://www.cna.com.tw/news/asam/201401060081-1.aspx)，中央社，2014年1月6日
2.  [葡足壇巨星
    黑豹尤西比奧辭世](http://www.cna.com.tw/news/asam/201401060081-1.aspx)，中央社，2014年1月6日
3.  [葡萄牙传奇球星尤西比奥心脏骤停去世](http://www.apdnews.com/photo/59507.html)
    ，亚太日报，2014年1月6日
4.  [葡足球传奇巨星“黑豹”尤西比奥去世
    享年71岁](http://sports.sina.com.cn/g/2014-01-05/16556967109.shtml)，新浪体育，2014年01月05日
5.