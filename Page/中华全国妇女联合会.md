**中华全国妇女联合会**简称**全国妇联**，是1949年3月由[中国共产党成立的妇女组织](../Page/中国共产党.md "wikilink")，其基本功能是代表及捍卫妇女权益、促进[男女平等](../Page/男女平等.md "wikilink")，亦同时维护少年儿童权益，以及在全国女性中组织对中国共产党和[中华人民共和国政府](../Page/中华人民共和国.md "wikilink")、政策的支持。

从1995年开始，全国妇联被定性为[非政府组织](../Page/非政府组织.md "wikilink")。在[中华人民共和国政治体制中](../Page/中华人民共和国政治.md "wikilink")，各级妇联与[工会](../Page/中华全国总工会.md "wikilink")、[共青团都属于](../Page/中国共产主义青年团.md "wikilink")[群众组织](../Page/群众组织.md "wikilink")，接受各级[党委的领导](../Page/党委.md "wikilink")，党委设有“党群副书记”（现在叫做“专职副书记”）是这三大群众组织的直接上级领导。各级妇联的人事任免权限在各级党委常委会。上一级妇联对下一级妇联仅有业务指导关系，并无直接领导权力。妇联自身设有[党组](../Page/党组.md "wikilink")，作为实际领导决策机构。

## 历史

[Bundesarchiv_Bild_183-1988-0518-304,_LPG_Mühlhausen,_Besuch_einer_chinesischen_Delegation.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-1988-0518-304,_LPG_Mühlhausen,_Besuch_einer_chinesischen_Delegation.jpg "fig:Bundesarchiv_Bild_183-1988-0518-304,_LPG_Mühlhausen,_Besuch_einer_chinesischen_Delegation.jpg")
[Bundesarchiv_Bild_183-1988-0518-303,_LPG_Mühlhausen,_Besuch_einer_chinesischen_Delegation.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-1988-0518-303,_LPG_Mühlhausen,_Besuch_einer_chinesischen_Delegation.jpg "fig:Bundesarchiv_Bild_183-1988-0518-303,_LPG_Mühlhausen,_Besuch_einer_chinesischen_Delegation.jpg")

“中华全国民主妇女联合会”成立于1949年3月，1957年改为“中华人民共和国妇女联合会”，1978年又改为“中华全国妇女联合会”。简称一直用“全国妇联”。

## 历任主席

| 姓名                               | 籍贯   | 任职          | 曾任党和国家最高领导职务     | 备注            |
| -------------------------------- | ---- | ----------- | ---------------- | ------------- |
| [蔡畅](../Page/蔡畅.md "wikilink")   | 湖南双峰 | 1949年－1978年 | 第四、五届全国人大常委会副委员长 | 前中共領導人李富春夫人   |
| [康克清](../Page/康克清.md "wikilink") | 江西万安 | 1978年－1988年 | 第五、六、七届全国政协副主席   | 中國人民解放軍元帥朱德夫人 |
| [陈慕华](../Page/陈慕华.md "wikilink") | 浙江青田 | 1988年－1998年 | 第七、八届全国人大常委会副委员长 | 全国妇联名誉主席      |
| [彭珮云](../Page/彭珮云.md "wikilink") | 湖南浏阳 | 1998年－2003年 | 第九届全国人大常委会副委员长   | 中共前高级领导人王汉斌夫人 |
| [顾秀莲](../Page/顾秀莲.md "wikilink") | 江苏南通 | 2003年－2008年 | 第十届全国人大常委会副委员长   | 新中国首位女省长      |
| [陈至立](../Page/陈至立.md "wikilink") | 福建仙游 | 2008年－2013年 | 第十一届全国人大常委会副委员长  | 前国务委员、教育部部长   |
| [沈跃跃](../Page/沈跃跃.md "wikilink") | 浙江宁波 | 2013年－      | 第十二届全国人大常委会副委员长  | 中共中央组织部前常务副部长 |

**全国妇联历任主席**

## 历届领导机构

### 第一届全国妇联领导机构

  - 名誉主席：[宋庆龄](../Page/宋庆龄.md "wikilink")、[何香凝](../Page/何香凝.md "wikilink")
  - 主席：[蔡畅](../Page/蔡畅.md "wikilink")
  - 副主席：[邓颖超](../Page/邓颖超.md "wikilink")、[李德全](../Page/李德全.md "wikilink")、[许广平](../Page/许广平.md "wikilink")
  - 秘书长：[区梦觉](../Page/区梦觉.md "wikilink")
  - 副秘书长：[曾宪植](../Page/曾宪植.md "wikilink")

### 第二届全国妇联领导机构

  - 名誉主席：[宋庆龄](../Page/宋庆龄.md "wikilink")、[何香凝](../Page/何香凝.md "wikilink")
  - 主席：[蔡畅](../Page/蔡畅.md "wikilink")
  - 副主席：[邓颖超](../Page/邓颖超.md "wikilink")、[李德全](../Page/李德全.md "wikilink")、[许广平](../Page/许广平.md "wikilink")、[史良](../Page/史良.md "wikilink")、[章蕴](../Page/章蕴.md "wikilink")
  - 秘书长：[章蕴](../Page/章蕴.md "wikilink")（兼）
  - 副秘书长：[罗琼](../Page/罗琼.md "wikilink")、[曹孟君](../Page/曹孟君.md "wikilink")、[曾宪植](../Page/曾宪植.md "wikilink")

### 第三届全国妇联领导机构

  - 名誉主席：[宋庆龄](../Page/宋庆龄.md "wikilink")、[何香凝](../Page/何香凝.md "wikilink")
  - 主席：[蔡畅](../Page/蔡畅.md "wikilink")
  - 副主席：[邓颖超](../Page/邓颖超.md "wikilink")、[李德全](../Page/李德全.md "wikilink")、[许广平](../Page/许广平.md "wikilink")、[史良](../Page/史良.md "wikilink")、[章蕴](../Page/章蕴.md "wikilink")、[杨之华](../Page/杨之华.md "wikilink")、[刘清扬](../Page/刘清扬.md "wikilink")、[康克清](../Page/康克清.md "wikilink")
  - 书记处第一书记：[罗琼](../Page/罗琼.md "wikilink")
  - 书记：[田秀涓](../Page/田秀涓.md "wikilink")、[李宝光](../Page/李宝光.md "wikilink")、[吴綪](../Page/吴綪.md "wikilink")、[吴全衡](../Page/吴全衡.md "wikilink")、[胡耐秋](../Page/胡耐秋.md "wikilink")、[曹孟君](../Page/曹孟君.md "wikilink")、[曹冠群](../Page/曹冠群.md "wikilink")、[曾宪植](../Page/曾宪植.md "wikilink")、[杨蕴玉](../Page/杨蕴玉.md "wikilink")、[董边](../Page/董边.md "wikilink")、[郭建](../Page/郭建.md "wikilink")（1961年5月任职）、[黄甘英](../Page/黄甘英.md "wikilink")（1961年5月任职）、
    [赵烽](../Page/赵烽.md "wikilink")（1963年5月-1966年6月任职）

### 第四届全国妇联领导机构

  - 名誉主席：[宋庆龄](../Page/宋庆龄.md "wikilink")、[蔡畅](../Page/蔡畅.md "wikilink")、[邓颖超](../Page/邓颖超.md "wikilink")
  - 主席：[康克清](../Page/康克清.md "wikilink")
  - 副主席：[史良](../Page/史良.md "wikilink")、[罗琼](../Page/罗琼.md "wikilink")、[吴贻芳](../Page/吴贻芳.md "wikilink")、[曾宪植](../Page/曾宪植.md "wikilink")、[雷洁琼](../Page/雷洁琼.md "wikilink")、[林巧稚](../Page/林巧稚.md "wikilink")、[李宝光](../Page/李宝光.md "wikilink")、[郝建秀](../Page/郝建秀.md "wikilink")、[黄甘英](../Page/黄甘英.md "wikilink")、[林雨韫](../Page/林雨韫.md "wikilink")、[关建](../Page/关建.md "wikilink")、[阿沛·才旦卓嘎](../Page/阿沛·才旦卓嘎.md "wikilink")、[玛依努尔·哈斯木](../Page/玛依努尔·哈斯木.md "wikilink")
  - 书记处第一书记：[罗琼](../Page/罗琼.md "wikilink")（1978年9月-1982年5月任职）、[郭力文](../Page/郭力文.md "wikilink")（1980年4月任书记，1982年5月任第一书记）
  - 书记：[李琴](../Page/李琴.md "wikilink")（未到任)、[李宝光](../Page/李宝光.md "wikilink")（1978年9月-1979年任职）、[黄甘英](../Page/黄甘英.md "wikilink")、[吴全衡](../Page/吴全衡.md "wikilink")、[田秀涓](../Page/田秀涓.md "wikilink")（1978年9月-1982年任职）、[李淑铮](../Page/李淑铮.md "wikilink")（未到任)、[董边](../Page/董边.md "wikilink")、[林丽韫](../Page/林丽韫.md "wikilink")（1978年9月-1982年任职）、[王云](../Page/王云.md "wikilink")（1979年任职）、[张洁珣](../Page/张洁珣.md "wikilink")（1980年任职）、[柳勉之](../Page/柳勉之.md "wikilink")（1980年-1982年任职）、[梁柯平](../Page/梁柯平.md "wikilink")（1980年-1982年5月任职）、[胡德华](../Page/胡德华.md "wikilink")（1981年任职）、[于淑琴](../Page/于淑琴.md "wikilink")（1982年任职）、[王立威](../Page/王立威.md "wikilink")（1982年任职）

### 第五届全国妇联领导机构

  - 主席：[康克清](../Page/康克清.md "wikilink")
  - 副主席：[罗琼](../Page/罗琼.md "wikilink")、[吴贻芳](../Page/吴贻芳.md "wikilink")、[雷洁琼](../Page/雷洁琼.md "wikilink")、[谭惕吾](../Page/谭惕吾.md "wikilink")、[李文宜](../Page/李文宜.md "wikilink")、[张帼英](../Page/张帼英.md "wikilink")、[郭力文](../Page/郭力文.md "wikilink")、[黄甘英](../Page/黄甘英.md "wikilink")、[林丽韫](../Page/林丽韫.md "wikilink")、[阿沛·才旦卓嘎](../Page/阿沛·才旦卓嘎.md "wikilink")、[玛依努尔·哈斯木](../Page/玛依努尔·哈斯木.md "wikilink")、[王琇瑛](../Page/王琇瑛.md "wikilink")、[谭茀芸](../Page/谭茀芸.md "wikilink")
  - 书记处第一书记：[张帼英](../Page/张帼英.md "wikilink")
  - 书记处书记：[胡德华](../Page/胡德华.md "wikilink")、[王庆淑](../Page/王庆淑.md "wikilink")、[王淑贤](../Page/王淑贤.md "wikilink")、[王德意](../Page/王德意.md "wikilink")、[于淑琴](../Page/于淑琴.md "wikilink")、[范崇嬿](../Page/范崇嬿.md "wikilink")、[王立威](../Page/王立威.md "wikilink")、[关涛](../Page/关涛.md "wikilink")（1986年任职）

### 第六届全国妇联领导机构

  - 名誉主席：[康克清](../Page/康克清.md "wikilink")
  - 主席：[陈慕华](../Page/陈慕华.md "wikilink")
  - 副主席：[张帼英](../Page/张帼英.md "wikilink")、[黄启璪](../Page/黄启璪.md "wikilink")、[林丽韫](../Page/林丽韫.md "wikilink")、[阿沛·才旦卓嘎](../Page/阿沛·才旦卓嘎.md "wikilink")、[玛依努尔·哈斯木](../Page/玛依努尔·哈斯木.md "wikilink")、[郝诒纯](../Page/郝诒纯.md "wikilink")、[张素我](../Page/张素我.md "wikilink")、[卢乐山](../Page/卢乐山.md "wikilink")、[聂力](../Page/聂力.md "wikilink")、[韦钰](../Page/韦钰.md "wikilink")、[赵地](../Page/赵地.md "wikilink")、[杨衍银](../Page/杨衍银.md "wikilink")
  - 书记处第一书记：[张帼英](../Page/张帼英.md "wikilink")（1988年9月-1990年2月任职）、[黄启璪](../Page/黄启璪.md "wikilink")（1988年9月任书记，1990年2月任第一书记）
  - 书记处书记：[黄启璪](../Page/黄启璪.md "wikilink")、[王淑贤](../Page/王淑贤.md "wikilink")、[关涛](../Page/关涛.md "wikilink")、[康泠](../Page/康泠.md "wikilink")、[赵地](../Page/赵地.md "wikilink")（1990年12月任职）、[杨衍银](../Page/杨衍银.md "wikilink")（1990年12月-1993年6月任职）

### 第七届全国妇联领导机构

  - 主席：[陈慕华](../Page/陈慕华.md "wikilink")
  - 副主席：[黄启璪](../Page/黄启璪.md "wikilink")、[顾秀莲](../Page/顾秀莲.md "wikilink")（1998年6月任职）、[张帼英](../Page/张帼英.md "wikilink")、[林丽韫](../Page/林丽韫.md "wikilink")、[赵地](../Page/赵地.md "wikilink")、[聂力](../Page/聂力.md "wikilink")、[阿沛·才旦卓嘎](../Page/阿沛·才旦卓嘎.md "wikilink")、[玛依努尔·哈斯木](../Page/玛依努尔·哈斯木.md "wikilink")、[郝诒纯](../Page/郝诒纯.md "wikilink")、[何鲁丽](../Page/何鲁丽.md "wikilink")、[孔令仁](../Page/孔令仁.md "wikilink")、[韦钰](../Page/韦钰.md "wikilink")、[王淑贤](../Page/王淑贤.md "wikilink")、[刘海荣](../Page/刘海荣.md "wikilink")
  - 书记处第一书记：[黄启璪](../Page/黄启璪.md "wikilink")（1993年9月-1998年3月任职）、[顾秀莲](../Page/顾秀莲.md "wikilink")（1998年3月任职）
  - 书记处书记：[赵地](../Page/赵地.md "wikilink")、[王淑贤](../Page/王淑贤.md "wikilink")、[刘海荣](../Page/刘海荣.md "wikilink")、[康泠](../Page/康泠.md "wikilink")、[田淑兰](../Page/田淑兰.md "wikilink")、[华福周](../Page/华福周.md "wikilink")、[张连珍](../Page/张连珍.md "wikilink")（1993年9月-1995年10月任职）、[冯淬](../Page/冯淬.md "wikilink")（1996年11月任职）

### 第八届全国妇联领导机构

  - 名誉主席：[陈慕华](../Page/陈慕华.md "wikilink")
  - 主席：[彭珮云](../Page/彭珮云.md "wikilink")
  - 副主席：[顾秀莲](../Page/顾秀莲.md "wikilink")、[黄晴宜](../Page/黄晴宜.md "wikilink")、[巴桑](../Page/巴桑_\(政治人物\).md "wikilink")、[沈淑济](../Page/沈淑济.md "wikilink")、[孔令仁](../Page/孔令仁.md "wikilink")、[谢丽娟](../Page/谢丽娟.md "wikilink")、[钱易](../Page/钱易_\(院士\).md "wikilink")、[韦钰](../Page/韦钰.md "wikilink")、[刘海荣](../Page/刘海荣.md "wikilink")、[彭钢](../Page/彭钢.md "wikilink")、[刘雅芝](../Page/刘雅芝.md "wikilink")、[田淑兰](../Page/田淑兰.md "wikilink")、[华福周](../Page/华福周.md "wikilink")
  - 书记处第一书记：[顾秀莲](../Page/顾秀莲.md "wikilink")
  - 书记处书记：[黄晴宜](../Page/黄晴宜.md "wikilink")、[沈淑济](../Page/沈淑济.md "wikilink")、[刘海荣](../Page/刘海荣.md "wikilink")（1998年9月-2001年12月任职）、[田淑兰](../Page/田淑兰.md "wikilink")（1998年9月-2002年3月任职）、[华福周](../Page/华福周.md "wikilink")、[冯淬](../Page/冯淬.md "wikilink")（1998年9月-2001年12月任职）、[莫文秀](../Page/莫文秀.md "wikilink")、[李秋芳](../Page/李秋芳.md "wikilink")、[赵少华](../Page/赵少华.md "wikilink")

### 第九届全国妇联领导机构

  - 名誉主席：[陈慕华](../Page/陈慕华.md "wikilink")、[彭珮云](../Page/彭珮云.md "wikilink")
  - 主席：[顾秀莲](../Page/顾秀莲.md "wikilink")
  - 副主席：[黄晴宜](../Page/黄晴宜.md "wikilink")、[巴桑](../Page/巴桑_\(政治人物\).md "wikilink")（兼）、[沈淑济](../Page/沈淑济.md "wikilink")、[谢丽娟](../Page/谢丽娟.md "wikilink")（兼）、[刘雅芝](../Page/刘雅芝.md "wikilink")（兼）、[汪纪戎](../Page/汪纪戎.md "wikilink")（兼）、[张梅颖](../Page/张梅颖.md "wikilink")（兼）、[陈秀榕](../Page/陈秀榕.md "wikilink")、[黄彦蓉](../Page/黄彦蓉.md "wikilink")（兼）、[吴启迪](../Page/吴启迪.md "wikilink")（兼）、[莫文秀](../Page/莫文秀.md "wikilink")、[赵少华](../Page/赵少华.md "wikilink")、[刘晓连](../Page/刘晓连.md "wikilink")（兼）
  - 书记处第一书记：[黄晴宜](../Page/黄晴宜.md "wikilink")
  - 书记处书记：[沈淑济](../Page/沈淑济.md "wikilink")、[陈秀榕](../Page/陈秀榕.md "wikilink")、[莫文秀](../Page/莫文秀.md "wikilink")、[赵少华](../Page/赵少华.md "wikilink")、[洪天慧](../Page/洪天慧.md "wikilink")、[甄砚](../Page/甄砚.md "wikilink")、[张世平](../Page/张世平.md "wikilink")、[王乃坤](../Page/王乃坤.md "wikilink")

### 第十届全国妇联领导机构

  - 名誉主席：[陈慕华](../Page/陈慕华.md "wikilink")、[彭珮云](../Page/彭珮云.md "wikilink")
  - 主席：[沈跃跃](../Page/沈跃跃.md "wikilink")（2013年5月任职）、[陈至立](../Page/陈至立.md "wikilink")（2008年10月-2013年5月）
  - 副主席：
    [宋秀岩](../Page/宋秀岩.md "wikilink")（2010年1月任职）、[黄晴宜](../Page/黄晴宜.md "wikilink")（2008年10月-2010年1月）、[陈秀榕](../Page/陈秀榕.md "wikilink")、[孟晓驷](../Page/孟晓驷.md "wikilink")、[潘贵玉](../Page/潘贵玉.md "wikilink")（兼）、[赵实](../Page/赵实.md "wikilink")（兼）、[乔传秀](../Page/乔传秀.md "wikilink")（兼）、[汪纪戎](../Page/汪纪戎.md "wikilink")（兼）、[姜力](../Page/姜力.md "wikilink")（兼）、[徐莉莉](../Page/徐莉莉.md "wikilink")（兼）、[唐晓青](../Page/唐晓青.md "wikilink")（兼）、[洪天慧](../Page/洪天慧.md "wikilink")（2008年10月-2011年12月）、[热孜万·艾拜](../Page/热孜万·艾拜.md "wikilink")（兼）、[张世平](../Page/张世平.md "wikilink")（2011年12月任职，兼）、[甄砚](../Page/甄砚.md "wikilink")、[赵东花](../Page/赵东花.md "wikilink")（2011年12月任职）
  - 书记处第一书记：[宋秀岩](../Page/宋秀岩.md "wikilink")（2010年1月任职）、[黄晴宜](../Page/黄晴宜.md "wikilink")（2008年10月-2010年1月）
  - 书记处书记：[陈秀榕](../Page/陈秀榕.md "wikilink")、[孟晓驷](../Page/孟晓驷.md "wikilink")、[洪天慧](../Page/洪天慧.md "wikilink")（2008年10月-2011年12月）、[甄砚](../Page/甄砚.md "wikilink")、[赵东花](../Page/赵东花.md "wikilink")、[范继英](../Page/范继英.md "wikilink")、[张静](../Page/张静.md "wikilink")（2010年1月任职）、[崔郁](../Page/崔郁.md "wikilink")（2011年12月任职）

### 第十一届全国妇联领导机构

  - 主席：[沈跃跃](../Page/沈跃跃.md "wikilink")
  - 副主席：[靳诺](../Page/靳诺.md "wikilink")（2013年10月-2018年10月，兼）、[程红](../Page/程红.md "wikilink")（2013年10月至今，兼）、[谢茹](../Page/谢茹.md "wikilink")（2013年10月-2018年10月，兼）、[崔丽](../Page/崔丽.md "wikilink")（2013年10月-2018年10月，兼）、[赵东花](../Page/赵东花.md "wikilink")（2013年10月-2016年8月）、[喻红秋](../Page/喻红秋.md "wikilink")（2013年10月至2015年3月）、[印红](../Page/印红.md "wikilink")（2013年10月-2018年10月，兼）、[范继英](../Page/范继英.md "wikilink")（2013年10月-2018年10月，兼）、[崔郁](../Page/崔郁.md "wikilink")（2013年10月至2017年2月）、[夏杰](../Page/夏杰.md "wikilink")（2017年2月任职）、[焦扬](../Page/焦扬.md "wikilink")（2015年7月-2016年10月）、[邓丽](../Page/邓丽.md "wikilink")（2016年8月-2018年10月）、[谭琳](../Page/谭琳.md "wikilink")（2017年2月任职）、[董尤心](../Page/董尤心.md "wikilink")（2013年10月-2016年1月，兼）、[宋鱼水](../Page/宋鱼水.md "wikilink")（2013年10月至今，兼）、[刘洋](../Page/刘洋_\(航天员\).md "wikilink")（2016年8月至今，兼）
  - 书记处第一书记：[宋秀岩](../Page/宋秀岩.md "wikilink")（2013年10月-2018年9月）、[黄晓薇](../Page/黄晓薇.md "wikilink")（2018年9月至今）
  - 书记处书记：[张晓兰](../Page/张晓兰.md "wikilink")（2016年8月至今）、[孟晓驷](../Page/孟晓驷.md "wikilink")（2013年10月-2016年8月）、[赵东花](../Page/赵东花.md "wikilink")（2013年10月-2016年8月）、[喻红秋](../Page/喻红秋.md "wikilink")（2013年10月-2015年3月）、[崔郁](../Page/崔郁.md "wikilink")（2013年10月-2017年2月）、[夏杰](../Page/夏杰.md "wikilink")（2017年2月任职）、[焦扬](../Page/焦扬.md "wikilink")（2013年10月-2016年10月）、[邓丽](../Page/邓丽.md "wikilink")（2013年10月-2018年10月）、[谭琳](../Page/谭琳.md "wikilink")（2013年10月至今）、[杨柳](../Page/杨柳.md "wikilink")（2018年6月-2018年10月，挂职）

### 第十二届全国妇联领导机构

  - 主席：[沈跃跃](../Page/沈跃跃.md "wikilink")
  - 副主席：[黄晓薇](../Page/黄晓薇.md "wikilink")、[桑顶·多吉帕姆·德庆曲珍](../Page/桑顶·多吉帕姆·德庆曲珍.md "wikilink")（兼）、[张晓兰](../Page/张晓兰_\(官员\).md "wikilink")、[程红](../Page/程红.md "wikilink")（兼）、[夏杰](../Page/夏杰.md "wikilink")、[谭琳](../Page/谭琳.md "wikilink")、[曹淑敏](../Page/曹淑敏.md "wikilink")（兼）、[余艳红](../Page/余艳红.md "wikilink")（兼）、[吴海鹰](../Page/吴海鹰.md "wikilink")、[石岱](../Page/石岱.md "wikilink")（兼）、[宋鱼水](../Page/宋鱼水.md "wikilink")（兼）、[刘洋](../Page/刘洋_\(航天员\).md "wikilink")（兼）、[陈化兰](../Page/陈化兰.md "wikilink")（兼）、[蒙曼](../Page/蒙曼.md "wikilink")（兼）
  - 书记处第一书记：[黄晓薇](../Page/黄晓薇.md "wikilink")
  - 书记处书记：[张晓兰](../Page/张晓兰.md "wikilink")、[夏杰](../Page/夏杰.md "wikilink")、[谭琳](../Page/谭琳.md "wikilink")、[吴海鹰](../Page/吴海鹰.md "wikilink")、[赵雯](../Page/赵雯.md "wikilink")、[蔡淑敏](../Page/蔡淑敏.md "wikilink")、[章冬梅](../Page/章冬梅.md "wikilink")

## 组织

最高权力机构为全国妇女代表大会，每5年举行一次。大会选举产生全国妇联执行委员会，后者每年开会一次。执行委员会选举主席、副主席和常务委员组成常务委员会，是执行委员会闭会期间的领导机构。常务委员会下设主持日常工作的书记处。

与全国一级类似，省、市、[县级行政区以上的行政单位有自己的地方妇女代表大会](../Page/县级行政区.md "wikilink")，每5年举行一次，并选举执行委员会，后者选出主席一人、副主席若干人、常务委员若干人，组成常务委员会，领导本地区妇女联合会的工作。

在乡、镇、街道的妇女代表大会每三至五年举行一次。妇女代表大会闭会期间的领导机构是执行委员会。执行委员会全体会议选举主席一人、副主席若干人，负责日常工作。乡镇妇联主席常被俗称或误称为“乡镇妇女主任”\[1\]。

在行政村，由本村妇女代表大会或妇女大会选出村妇女主任。村妇女主任是村民委员会的当然成员。由于中国的农村基层民主政权建设，村妇女主任与村委会其它成员一般都要由选举产生。

在乡镇企业、农（林牧渔）场、城市的居民委员会和专业市场等，与行政村类似，由妇女代表会议选出妇女主任一名、副主任若干名。

企业基层工会女职工委员会是所在地域妇联的团体会员。凡在民政部门合规注册的女性为主体会员的社会团体，可成为妇联的团体会员。

## 机构设置

### 内设机构

  - [办公厅](../Page/办公厅.md "wikilink")
  - [组织部](../Page/组织部.md "wikilink")（联络部）
  - [宣传部](../Page/宣传部.md "wikilink")
  - 妇女发展部
  - 权益部
  - 儿童工作部
  - 国际联络部
  - [机关党委](../Page/机关党委.md "wikilink")
  - [国务院妇女儿童工作委员会办公室](../Page/国务院妇女儿童工作委员会.md "wikilink")
  - 离退休干部处

### 直属[事业单位](../Page/事业单位.md "wikilink")

  - [妇女研究所](../Page/全国妇联妇女研究所.md "wikilink")
  - [中国妇女杂志社](../Page/中国妇女杂志社.md "wikilink")
  - [中国儿童中心](../Page/中国儿童中心.md "wikilink")
  - [中国妇女出版社](../Page/中国妇女出版社.md "wikilink")
  - [中国妇女报社](../Page/中国妇女报社.md "wikilink")
  - [中国妇女外文期刊社](../Page/中国妇女外文期刊社.md "wikilink")
  - [中华女子学院](../Page/中华女子学院.md "wikilink")
  - [中国妇女活动中心](../Page/中国妇女活动中心.md "wikilink")
  - 机关服务中心
  - [全国妇联人才开发与培训中心](../Page/全国妇联人才开发培训中心.md "wikilink")
  - 交流与合作中心
  - [法律援助中心](../Page/全国妇联法律帮助中心.md "wikilink")
  - [婚姻与家庭杂志社](../Page/婚姻与家庭杂志社.md "wikilink")
  - [中国儿童少年基金会](../Page/中国儿童少年基金会.md "wikilink")
  - [中国妇女发展基金会](../Page/中国妇女发展基金会.md "wikilink")
  - [中国妇女儿童博物馆](../Page/中国妇女儿童博物馆.md "wikilink")

## 对外宣传

全国妇联主办“一报一刊”，分别是《中国妇女报》和《中国妇女》杂志。

## 荣誉

  - 1994年4月，被[联合国儿童基金会授予年度](../Page/联合国儿童基金会.md "wikilink")“**莫里斯·佩特奖**”；
  - 1995年，被[联合国授予年度](../Page/联合国.md "wikilink")“**国际扫盲奖──世宗王奖**”。

## 参考文献

## 參見

  - [中華民國婦女聯合會](../Page/中華民國婦女聯合會.md "wikilink")

[Category:参加中国人民政治协商会议的人民团体](../Category/参加中国人民政治协商会议的人民团体.md "wikilink")
[中华全国妇女联合会](../Category/中华全国妇女联合会.md "wikilink")
[Category:中华人民共和国女性组织](../Category/中华人民共和国女性组织.md "wikilink")
[Category:女性主義組織](../Category/女性主義組織.md "wikilink")

1.  [海南关于乡镇妇联主席配备问题之调查](http://www.women.org.cn/allnews/02/38.html)