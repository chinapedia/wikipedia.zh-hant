**延平王**（）為[南明](../Page/南明.md "wikilink")[永历帝於](../Page/永历帝.md "wikilink")1655年[敕封](../Page/冊封.md "wikilink")[鄭成功的](../Page/鄭成功.md "wikilink")[郡王](../Page/郡王.md "wikilink")[爵位](../Page/爵位.md "wikilink")。鄭成功在1662年[攻占台灣不久後薨殂於](../Page/鄭成功攻台之役.md "wikilink")[承天府](../Page/承天府_\(台灣\).md "wikilink")[安平鎮](../Page/安平鎮_\(明鄭\).md "wikilink")。其子[鄭經在沒有](../Page/鄭經.md "wikilink")[明朝皇帝冊封的情况下](../Page/明朝皇帝.md "wikilink")，接受了部分[明鄭家臣的擁戴](../Page/明鄭.md "wikilink")，自行以[王世子的身分於](../Page/王世子.md "wikilink")[思明州繼位](../Page/思明州_\(明郑\).md "wikilink")；隨後又率軍征討並擊敗人在台灣「護理」王[璽的叔父](../Page/玉璽.md "wikilink")[鄭襲](../Page/鄭襲_\(明鄭\).md "wikilink")。鄭經在鞏固王位之後，持續使用昭宗的[永曆年號](../Page/永曆_\(南明\).md "wikilink")，並以此身分於南台灣開創二十餘年的[鄭氏王朝](../Page/鄭氏王朝.md "wikilink")。1683年，末代[鄭克塽舉國降清](../Page/鄭克塽.md "wikilink")，除去延平王爵，降封[海澄公](../Page/海澄公.md "wikilink")。

## 地位

[中國](../Page/中國.md "wikilink")[明朝的](../Page/明朝.md "wikilink")[諸侯](../Page/諸侯.md "wikilink")[王爵制度](../Page/王爵.md "wikilink")，[福王](../Page/福王.md "wikilink")、[秦王等一字王號者為](../Page/秦王.md "wikilink")[親王](../Page/親王.md "wikilink")；二字王號者则為[郡王](../Page/郡王.md "wikilink")。在[明代](../Page/明代.md "wikilink")，親王子除世子外均封[郡王](../Page/郡王.md "wikilink")、世襲罔替。[郡王的](../Page/郡王.md "wikilink")[封號一般為兩個字](../Page/封號.md "wikilink")，以古郡縣名為多見，如渭南王（秦王系）、項城王（周王系）等。[明朝也會封功臣為郡王](../Page/明朝.md "wikilink")，多為追諡；其中南明時期有直接授予的，如延平王。

## 经历

### 南明時期（1655年－1662年）

西元1654年（南明[永曆八年八月](../Page/永曆.md "wikilink")），明昭宗敕封鄭成功為延平王，鄭成功謙辭不受。九年四月（1655年）昭宗再次下詔並齎延平王冊印至廈門，鄭成功乃不再推辭受封為延平王。

### 台灣明鄭時期（1662年－1683年）

西元1662年（明[永曆十五年](../Page/永曆.md "wikilink")），流亡[緬甸的永曆帝遭](../Page/緬甸.md "wikilink")[縊死](../Page/縊死.md "wikilink")，南明勢力走向瓦解。在此之後，歷代延平王雖然在法理上依舊奉大明為正朔（年號襲用「永曆」），但當時的[南台灣已然成為獨立行政且以延平王為最高統治者的王國](../Page/南台灣.md "wikilink")。1664年，第二代延平王鄭經從廈門撤退[承天](../Page/承天府.md "wikilink")；撤退後，鄭經專心鞏固王權並強化對台灣的控制，先後與[荷蘭東印度公司以及](../Page/荷蘭東印度公司.md "wikilink")[大肚王國發生武裝衝突](../Page/大肚王國.md "wikilink")。1681年，鄭經因痔瘡（台灣外記）或另有人稱狩獵摔傷不治，傳位予身兼[監國的](../Page/監國.md "wikilink")[世子](../Page/世子.md "wikilink")[鄭克臧](../Page/鄭克臧.md "wikilink")；然而，繼位不及三日，鄭克臧即遭鄭氏宗親與[外戚](../Page/外戚.md "wikilink")[馮錫範聯手刺殺](../Page/馮錫範.md "wikilink")，改立[鄭克塽承嗣王位](../Page/鄭克塽.md "wikilink")。鄭克塽繼位後，追謚王祖父[鄭成功為](../Page/鄭成功.md "wikilink")「潮武王」、父王[鄭經謚為](../Page/鄭經.md "wikilink")「文王」\[1\]。兩年後，清國將領[施琅於](../Page/施琅.md "wikilink")[澎湖海戰中大敗](../Page/澎湖海戰.md "wikilink")[劉國軒](../Page/劉國軒.md "wikilink")，鄭克塽詔告降清，結束了三代延平王於南台灣的統治。

## 後世影響

台灣民間主要紀念與祭祀的「延平郡王」通常指的僅是鄭成功一人。鄭氏向清朝[投降之後](../Page/投降.md "wikilink")，**延平王**的稱號隨之消滅。在[台灣清治時期之初](../Page/台灣清治時期.md "wikilink")，民間不敢以「延平郡王」之名義奉祀**鄭成功**，直到清[欽差大臣](../Page/欽差大臣.md "wikilink")[沈葆楨在](../Page/沈葆楨.md "wikilink")[牡丹社事件時以](../Page/牡丹社事件.md "wikilink")「欽差辦理台灣等處海防兼理各國事務大臣」身分前來台灣，上書請建[延平郡王祠](../Page/延平郡王祠.md "wikilink")，鄭成功才名正言順得以此稱號紀念。及至[台灣日治時期](../Page/台灣日治時期.md "wikilink")，因鄭成功有[日本人血統](../Page/日本人.md "wikilink")，亦頗重視鄭成功，並未將祠廢除，僅將其改為**開山[神社](../Page/神社.md "wikilink")**，或許因民眾仍習稱其為**開山王**所致。直至[二戰結束](../Page/第二次世界大戰.md "wikilink")，[中華民國統治台灣之後](../Page/中華民國.md "wikilink")，再將開山神社改回**延平郡王祠**，並將原有[福州式](../Page/福州.md "wikilink")[封火山牆廟宇](../Page/封火山牆.md "wikilink")，改建為[北方宮殿式](../Page/北方宮殿.md "wikilink")。[臺南市](../Page/臺南市.md "wikilink")[中西區](../Page/中西區_\(臺南市\).md "wikilink")[郡王里因位於](../Page/郡王里.md "wikilink")**延平郡王祠**所在地，故命此名。

## 延平王家族世系

## 参考文献

## 参见

  - [鄭氏王朝宗室列表](../Page/鄭氏王朝宗室列表.md "wikilink")

{{-}}

[Category:南明异姓郡王](../Category/南明异姓郡王.md "wikilink")
[延平王](../Category/延平王.md "wikilink")
[Category:鄭氏王朝人物](../Category/鄭氏王朝人物.md "wikilink")
[Category:台湾君主](../Category/台湾君主.md "wikilink")

1.  《台灣人的台灣史》：「追謚鄭成功為潮武王，鄭經謚為文王。」