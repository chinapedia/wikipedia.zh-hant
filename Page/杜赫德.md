[DescriptionDeLaChineJeanBaptisteDuHalde1736.jpg](https://zh.wikipedia.org/wiki/File:DescriptionDeLaChineJeanBaptisteDuHalde1736.jpg "fig:DescriptionDeLaChineJeanBaptisteDuHalde1736.jpg")
**杜赫德**（Jean-Baptiste Du
Halde，）是[法国](../Page/法国.md "wikilink")[神父](../Page/神父.md "wikilink")，著名[汉学家](../Page/汉学家.md "wikilink")，“杜赫德”是他的[汉语名字](../Page/汉语.md "wikilink")。虽然他终身未曾到过[中国](../Page/中国.md "wikilink")，但却出版了非常詳实的介绍中国[历史](../Page/历史.md "wikilink")、[文化](../Page/文化.md "wikilink")、风土人情的著作，1735年出版《[中华帝国全志](../Page/中华帝国.md "wikilink")》，全名为《中华帝国及其所属[鞑靼地区的地理](../Page/鞑靼.md "wikilink")、历史、编年纪、政治和博物》，被誉为“法国汉学三大奠基作之一”，\[1\]
该书轰动了[欧洲](../Page/欧洲.md "wikilink")，几年之内便出版了3次[法文版](../Page/法文.md "wikilink")、2次[英文版](../Page/英文.md "wikilink")，另外[俄文和](../Page/俄文.md "wikilink")[德文本也出版发行](../Page/德文.md "wikilink")。\[2\]

## 注释

<div class="references-small">

<references />

</div>

  - Jean-Baptiste Du Halde,
    \[<http://www.archive.org/download/descriptiongog01duha/descriptiongog01duha.pdf>
    *Description géographique, historique, chronologique, politique, et
    physique de l'empire de la Chine et de la Tartarie chinoise,
    enrichie des cartes générales et particulieres de ces pays, de la
    carte générale et des cartes particulieres du Thibet, & de la Corée;
    & ornée d'un grand nombre de figures & de vignettes gravées en
    tailledouce\]* (Paris: J-B Mercier, 1735 ; La Haye: H. Scheurleer,
    1736)

[D杜](../Category/法国汉学家.md "wikilink")
[D杜](../Category/法国历史学家.md "wikilink")

1.  计翔翔，“西方早期汉学试析”，《[浙江大学学报](../Page/浙江大学.md "wikilink")（人文社会科学版）》2002年第1期，第95页。
2.  张云江，[“法国启蒙运动中的“儒学”镜像”](http://www.housebook.com.cn/200609/20.htm)
    ，《[书屋](../Page/书屋.md "wikilink")》2006年第9期。