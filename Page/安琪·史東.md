**安琪·史東**（，），本名**安琪拉·拉薇妮·布朗**（*Angela Laverne
Brown*），是一位美國[靈魂爵士](../Page/靈魂爵士.md "wikilink")、[R\&B](../Page/R&B.md "wikilink")、[新靈魂樂](../Page/新靈魂樂.md "wikilink")、[嘻哈歌手](../Page/嘻哈.md "wikilink")，出生於[南卡羅萊納州的](../Page/南卡羅萊納州.md "wikilink")[哥倫比亞市](../Page/哥倫比亞_\(南卡羅萊那州\).md "wikilink")，同時兼具演員身份。史東的音樂被譽為具有早期靈魂樂的沉靜、學院風格，並使人聯想起另一位靈魂爵士大師[艾瑞莎·弗蘭克林](../Page/艾瑞莎·弗蘭克林.md "wikilink")。

## 簡介

1980年代，史東曾是[序列樂團](../Page/序列樂團.md "wikilink")（*[:en:The
Sequence](../Page/:en:The_Sequence.md "wikilink")*）（美國第一個女子嘻哈樂團）的一員。該樂團在1980年代以〈*Funk
You
Up*〉為成名代表作。離開樂團後，史東在Mantronix唱片公司工作，並隨後與[薩克斯風手](../Page/薩克斯風.md "wikilink")[藍尼·克羅維茲組團表演](../Page/藍尼·克羅維茲.md "wikilink")。

在1990年代，史東加入[垂直支撐樂團](../Page/垂直支撐樂團.md "wikilink")，發行了一張相當廣受歡迎的單曲〈*你似乎太忙*〉。

1996年，史東與前團員藍尼的兄弟蓋瑞·迪維茲合作，並與查理·摩爾成立了Devox樂團，發行了一張唱片（僅在[日本銷售](../Page/日本.md "wikilink")），此唱片中史東也跨刀寫曲。

在[迪·安傑羅](../Page/迪·安傑羅.md "wikilink")（*D'Angelo*）1995年的《紅糖》和2000年的《巫毒》專輯中，史東也參與了作曲的工作，她與迪·安傑羅共有歌曲的[版權](../Page/版權.md "wikilink")。

### 1999年

1999年，史東終於發行了自己的單曲唱片，名為《黑色鑽石》，這張由Arista發行的單曲增加了史東的曝光率和知名度，並讓史東拿到了[告示牌排行榜潛力新人榜冠軍以及節奏藍調專輯榜第九名](../Page/告示牌.md "wikilink")。隨後她又在Clive
Davis’ J唱片公司發行了《紅木》（2001年）和《史東之愛》（2004年）兩張專輯。這兩張專輯都上了美國告示牌節奏藍調榜第四名。

史東的影響力可從她發行的獨唱單曲中看出，其具有重音節奏和節律唱頌的音樂特色相當明顯，不需依賴合聲來美化全曲，在1999年的單曲〈別再下雨〉可說重現了1973年〈我們兩個都不想第一個說再見〉的靈魂爵士精華（由[葛蕾蒂絲·奈特與種子合唱團所唱](../Page/葛蕾蒂絲·奈特與種子合唱團.md "wikilink")）。
2002年史東發行的《把你忘記》也重現了1972年O’Jays樂團經典名曲〈Back Stabbers〉的經典。

史東也曾為UPN電視公司的影集《女朋友》唱過片頭主題曲。她已育有兩位孩子，其中一位是1990年她與迪·安傑羅相戀所生的。

### 2006年

史東近期轉向戲劇發展，她參與的戲劇「Issues: We've Got Them
All」，已有成功的巡迴演出。另外，史東也與J-Records唱片公司約滿，不再是其下的唱片歌手。

## 音樂作品

### 專輯

|                       年份                       |                          專輯名稱                          |                            排行榜                            |
| :--------------------------------------------: | :----------------------------------------------------: | :-------------------------------------------------------: |
| <small>[US](../Page/告示牌.md "wikilink")</small> | <small>[US R\&B](../Page/US_R&B.md "wikilink")</small> | <small>[UK Albums](../Page/英國專輯排行榜.md "wikilink")</small> |
|                      1999                      |                 黑色鑽石（*Black Diamond*）                  |                            46                             |
|                      2001                      |                  紅木（*Mahogany Soul*）                   |                            22                             |
|                      2004                      |                   史東之愛（*Stone Love*）                   |                            14                             |
|                      2005                      |  石至名歸醇金選（*Stone Hits: The Very Best Of Angie Stone*）   |                            \-                             |
|                                                |                                                        |                                                           |

### EP與單曲

|                       年份                       |                                                                 單曲名稱                                                                 |                            排行榜                             | 所屬專輯 |
| :--------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------: | :--: |
| <small>[US](../Page/告示牌.md "wikilink")</small> |                                        <small>[US R\&B](../Page/US_R&B.md "wikilink")</small>                                        | <small>[UK Singles](../Page/英國單曲排行榜.md "wikilink")</small> |      |
|                      1999                      |                                                         別再下雨（*No More Rain*）                                                         |                             56                             |  9   |
|                      2000                      |                                                          生活故事（*Life Story*）                                                          |                             \-                             |  \-  |
|                      2000                      |                                                            每天（*Everyday*）                                                            |                             \-                             |  52  |
|                      2000                      |                             保持你的憂愁（*Keep Your Worries*），與[Guru的Jazzmatazz合作](../Page/Guru.md "wikilink")                             |                             \-                             |  99  |
|                      2001                      |                                你是我的陽光（*U Make My Sun Shine*），與[王子合唱](../Page/王子_\(歌手\).md "wikilink")                                |                             \-                             |  \-  |
|                      2001                      |                 我的黑人兄弟（*Brotha*），與[艾莉西亞·凱斯](../Page/艾莉西亞·凱斯.md "wikilink")、[夏娃合作](../Page/夏娃_\(饒舌歌手\).md "wikilink")                 |                             52                             |  13  |
|                      2002                      |                                                    把你忘記（*Wish I Didn't Miss You*）                                                    |                             79                             |  31  |
|                      2002                      |                                 不只是女人（*More Than A Woman*），與[卡文·理查森合唱](../Page/卡文·理查森.md "wikilink")                                 |                             \-                             |  63  |
|                      2003                      |                                                         瓶罐（*Bottles & Cans*）                                                         |                             \-                             |  \-  |
|                      2003                      | 簽名，上印,郵寄(我是你的)（*Signed, Sealed, Delivered (I'm Yours)*），與[Blue](../Page/Blue.md "wikilink")、[史提夫·汪達合唱](../Page/史提夫·汪達.md "wikilink") |                             \-                             |  \-  |
|                      2004                      |               謝你（''I Wanna Thank Ya" ''），與[史奴比狗狗合唱](../Page/史奴比狗狗.md "wikilink")，[美國舞曲榜冠軍](../Page/告示牌.md "wikilink")                |                             \-                             |  61  |
|                      2004                      |                                 留下來（*Stay For A While*），與[安東尼·漢彌頓合唱](../Page/安東尼·漢彌頓.md "wikilink")                                  |                             \-                             |  70  |
|                      2004                      |                                                            搬家公司（*U-Haul*）                                                            |                             \-                             |  68  |
|                      2005                      |                                                     我不是開玩笑 （*I Wasn't Kidding*）                                                      |                             \-                             |  \-  |
|                                                |                                                                                                                                      |                                                            |      |

## 參考文獻

## 相關網站

  - [官方網站](https://web.archive.org/web/20090118094743/http://angiestoneonline.net/)

  -
  -
[Category:非洲裔美國音樂家](../Category/非洲裔美國音樂家.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:非裔美國女歌手](../Category/非裔美國女歌手.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:非裔美國歌手演員](../Category/非裔美國歌手演員.md "wikilink")
[Category:美國鍵盤手](../Category/美國鍵盤手.md "wikilink")
[Category:美國音樂劇演員](../Category/美國音樂劇演員.md "wikilink")
[Category:美國專輯製作人](../Category/美國專輯製作人.md "wikilink")
[Category:美國節奏藍調歌手](../Category/美國節奏藍調歌手.md "wikilink")
[Category:美國靈魂樂歌手](../Category/美國靈魂樂歌手.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:英文歌手](../Category/英文歌手.md "wikilink")
[Category:南卡羅萊那州哥倫比亞市人](../Category/南卡羅萊那州哥倫比亞市人.md "wikilink")
[Category:南卡羅萊那州演員](../Category/南卡羅萊那州演員.md "wikilink")
[Category:南卡羅萊那州音樂家](../Category/南卡羅萊那州音樂家.md "wikilink")