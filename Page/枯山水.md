[Zuihou-in2.JPG](https://zh.wikipedia.org/wiki/File:Zuihou-in2.JPG "fig:Zuihou-in2.JPG")瑞峯院的枯山水\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Lightmatter_zen_garden.jpg "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Harima-ankokuji-sekitei01.jpg "fig:缩略图")
**枯山水**是[日本式](../Page/日本.md "wikilink")[园林的一种](../Page/园林.md "wikilink")，但也是日本畫的一種形式。一般是指由细沙碎石铺地，再加上一些叠放有致的石组所构成的缩微式园林景观，偶尔也包含[苔藓](../Page/苔藓.md "wikilink")、草坪或其他自然元素。枯山水並沒有水景，其中的「水」通常由砂石表現，而「山」通常用石块表現。有時也會在沙子的表面畫上紋路來表現水的流動。枯山水字面上的意思為「乾枯的景觀」或「乾枯的山與水」，通常出現在[室町時代](../Page/室町時代.md "wikilink")、[桃山時代以及](../Page/桃山時代.md "wikilink")[江戶時代的庭園中](../Page/江戶時代.md "wikilink")。\[1\]

枯山水常被认为是日本僧侣用于[冥想的辅助工具](../Page/冥想.md "wikilink")，所以几乎不使用开花植物，这些静止不变的元素被认为具有使人宁静的效果。

## 历史

  - [平安时代](../Page/平安时代.md "wikilink")（794年－1192年）-
    [镰仓时代](../Page/镰仓时代.md "wikilink")（1192年－1333年）、中国[唐朝](../Page/唐朝.md "wikilink")（618年－907年）水墨画传入日本。
  - 平安时代末期，世界第一部园林典籍《作庭记》中首次记录了枯山水。
  - 镰仓时代末期，与[禅宗相应的以追求自然意义和佛教意义的写意园林发展固定为枯山水形式](../Page/禅宗.md "wikilink")。
  - [南北朝](../Page/南北朝.md "wikilink")（1333年－1392年）时代，枯山水的实践时期，一般与真山水（池泉部分）同存于一个园林中，以真山水为主体，枯山水为辅。
  - [室町时代](../Page/室町时代.md "wikilink")（1393年－1573年），枯山水逐渐从寺社园林走入武家和皇家园林，并逐步与真山水分离开来。枯山水的颠峰时期，出现了枯山水的双璧[大德寺和](../Page/大德寺.md "wikilink")[龙安寺](../Page/龙安寺.md "wikilink")。
  - [桃山时代](../Page/桃山时代.md "wikilink")（1573年－1603年），以茶道宗匠[千利休所创立的草庵风茶室为代表的日式茶庭兴起](../Page/千利休.md "wikilink")。
  - [江户时代](../Page/江户时代.md "wikilink")（1603年－1867年），多以小型枯山水为主，以枯山水、真山水和茶庭相互融合园林形式出现。

## 形式

枯山水的設計往往有著不同的表達形式和主題，諸如：

  - 以碎石作為海洋，以石塊作為[島嶼](../Page/島嶼.md "wikilink")。
  - 以岩石代表一只母[虎带着她的幼崽](../Page/虎.md "wikilink")，游向一条龙。
  - 以岩石构成[日本汉字](../Page/日本汉字.md "wikilink")「心」、「[灵](../Page/灵.md "wikilink")」。

一项由[京都大学的](../Page/京都大学.md "wikilink") [Gert van
Tonder和](../Page/Gert_van_Tonder.md "wikilink")[ATR智能机器人学和传播实验室的](../Page/ATR智能机器人学和传播实验室.md "wikilink")[Michael
J.
Lyons](http://www.kasrl.org/michael.lyons.html)作出的研究表明，这些岩石构成了[树的抽象形式](../Page/树.md "wikilink")。这些画面无法在观看它们的时候被具体感知；研究人员认为[下意识可以发现岩石之间微妙的联系](../Page/下意识.md "wikilink")。他们认为这有助于园林的宁静。

## 世界各地的枯山水

[北投文物館澄心庭.jpg](https://zh.wikipedia.org/wiki/File:北投文物館澄心庭.jpg "fig:北投文物館澄心庭.jpg")[澄心庭](../Page/澄心庭.md "wikilink")。\]\]
[北投文物館水葉庭.jpg](https://zh.wikipedia.org/wiki/File:北投文物館水葉庭.jpg "fig:北投文物館水葉庭.jpg")[水葉庭](../Page/水葉庭.md "wikilink")。\]\]
15世纪建于[京都](../Page/京都.md "wikilink")[龙安寺的枯山水庭园](../Page/龙安寺.md "wikilink")，是日本最有名的园林精品。

枯山水的设计理念是回归自然。[旧金山](../Page/旧金山.md "wikilink")[金门公园的日式茶园的部分是一个小的枯水山](../Page/金门公园.md "wikilink")。图片中看不到的部分是一些大石头，在石头左边是一个鹅卵石滩。

[台灣因曾經歷](../Page/中華民國.md "wikilink")[日治時期](../Page/日治時期.md "wikilink")，在[宜蘭市的宜蘭設治紀念館](../Page/宜蘭市.md "wikilink")（即日治時期之宜蘭廳長官舍）亦有頗為精美的枯山水庭園。

## 参考文献

## 外部链接

  - [神經系統科學揭開禪花園神秘的一面](https://web.archive.org/web/20041213081907/http://www.lauralee.com/news/zensecrets.htm)
  - [佛教花園及聖地](https://web.archive.org/web/20060215200211/http://www.gardenvisit.com/garden_history/sacred_gardens/buddhist_gardens.htm)
  - [禪花園的照片及意義](https://web.archive.org/web/20060428044916/http://zen.thetao.info/perceive/zengarden.htm)
  - [日本枯山水的圖片廊](https://web.archive.org/web/20051127032306/http://phototravels.net/kyoto/zen-gardens-index.html)
  - [對'禪花園'一詞的批評](http://www.rothteien.com/superbait/zenviewpoints.htm)
  - [史丹福大學發表有關一些日本花園歷史及意思的文章](http://www.stanford.edu/group/sjeaa/journal42/japan2.pdf)

[Category:日本庭園](../Category/日本庭園.md "wikilink")
[Category:景觀建築](../Category/景觀建築.md "wikilink")

1.  [藝術與建築索引典—枯山水](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300310703)
    於2010年5月14日查閱