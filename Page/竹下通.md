[Takeshita_street_view.jpg](https://zh.wikipedia.org/wiki/File:Takeshita_street_view.jpg "fig:Takeshita_street_view.jpg")竹下通\]\]

**竹下通**是[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")[原宿一條](../Page/原宿.md "wikilink")[行人專用街道](../Page/行人專用街道.md "wikilink")，以潮流[服裝聞名](../Page/服裝.md "wikilink")，兩旁以[時裝店](../Page/時裝.md "wikilink")、[首飾店](../Page/首飾.md "wikilink")、[咖啡室與](../Page/咖啡室.md "wikilink")[餐廳為主](../Page/餐廳.md "wikilink")，大部份為小型商店，吸引年輕人及遊客到訪。

在1980年代初期至2004年，這裡也是[冒牌](../Page/冒牌.md "wikilink")[街頭服飾的售賣地](../Page/日本街头时尚.md "wikilink")。2004年起，由於政府的掃蕩態度轉為強硬，令仿冒日本及[美國服裝的非法活動較前減少](../Page/美國.md "wikilink")。

竹下通位於[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")[原宿站對面](../Page/原宿站.md "wikilink")，只相隔一條馬路。在周末時都會人山人海，可見到很多年輕人及學生在此購物。

## 外部連結

  - [原宿
    JNTO](http://www.jnto.go.jp/chc/RI/kanto/tokyo/harajuku_omotesandou_aoyama/harajuku_omotesandou_aoyama.html)

  - [竹下通情報](https://web.archive.org/web/20061009133414/http://www.harajuku.jp/takeshita/index.html)

[Category:日本商店街](../Category/日本商店街.md "wikilink")
[Category:東京都觀光地](../Category/東京都觀光地.md "wikilink")
[Category:東京都道路](../Category/東京都道路.md "wikilink")
[Category:原宿](../Category/原宿.md "wikilink")