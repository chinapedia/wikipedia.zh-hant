**象鳥**是生活在[馬達加斯加內一種巨型](../Page/馬達加斯加.md "wikilink")、不會飛的[科](../Page/科_\(生物\).md "wikilink")，其下有「[隆鳥屬](../Page/隆鳥屬.md "wikilink")」和「Mullerornis」兩屬，至少在16世紀以前便已[滅絕](../Page/滅絕.md "wikilink")。象鳥曾被認為是世界所存在過最大的[鳥類](../Page/鳥類.md "wikilink")，身高可超過三[公尺](../Page/公尺.md "wikilink")、體重達半[公噸](../Page/公噸.md "wikilink")\[1\]，直到2006年的十月被[駭鳥的化石](../Page/駭鳥.md "wikilink")（[Phorusrhacidae](../Page/:en:Phorusrhacidae.md "wikilink")）比下為止\[2\]。
「隆鳥」的成鳥和蛋都有陸續地被發現，某些蛋長軸甚至有1[英尺長](../Page/英尺.md "wikilink")（34cm長）。現今有四個種被歸類於「隆鳥屬」之下：*A.
hildebrandti*、*A. gracilis*、*A. medius*和*A. maximus* (Brodkorb,
1963)，但這種分類並不是完全沒有爭論的，有些的作者會將其歸類在同一個種*A.
maximus*裡。隆鳥屬於[古顎總目](../Page/古顎總目.md "wikilink")，和[鴕鳥的關係較近](../Page/鴕鳥.md "wikilink")，不能飛，且其胸骨沒有[龍骨脊](../Page/龍骨脊.md "wikilink")。

[華盛頓的](../Page/華盛頓哥倫比亞特區.md "wikilink")[國家地理學會中擺有一顆由](../Page/國家地理學會.md "wikilink")[路易斯·馬登在](../Page/路易斯·馬登.md "wikilink")1967年所發現的一顆隆鳥蛋。這顆蛋絲毫未損，而且裡面還有一隻未生小鳥的胚胎骨架。

象鳥的滅絕與人類有關，但不是被他們獵殺，而是由於來自馬達加斯加的新移民仍然過着[刀耕火種的生活](../Page/刀耕火種.md "wikilink")，不斷破壞森林來開拓農地，令象鳥可棲息的地方變得愈來愈少\[3\]。

<center>

<File:Aepyornis> foot.JPG|足 <File:Aepyornis> front.JPG|前視
<File:Aepyornis> side.JPG|側視 <File:Aepyornis> skull.JPG|顱
<File:Aepyornis> eggs.jpg|蛋

</center>

## 參考資料

[A](../Category/已滅絕鳥類.md "wikilink") [A](../Category/古颚总目.md "wikilink")
[A](../Category/馬達加斯加動物.md "wikilink")
[A](../Category/不会飞的鸟.md "wikilink")

1.  《[巨蛋尋根](../Page/巨蛋尋根.md "wikilink")》（[*Attenborough and the Giant
    Egg*](../Page/:en:Attenborough_and_the_Giant_Egg.md "wikilink")），明珠台，2012年9月25日。

2.  <http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2006/10/26/wfossil26.xml>

3.