**Winny**是一種利用[P2P技术](../Page/P2P.md "wikilink")，在[Microsoft
Windows系统平台上操作的](../Page/Microsoft_Windows.md "wikilink")[共享软件](../Page/共享软件.md "wikilink")。随后，强化改良版的Winny以**Winnyp**的名称发布。

## 概要

2002年5月6日在[日本最大的](../Page/日本.md "wikilink")[BBS](../Page/BBS.md "wikilink")「[第2頻道](../Page/2ch.md "wikilink")（）」軟體下載板上發佈並[公測](../Page/公測.md "wikilink")。軟體開發者是前[東京大學大學院情報理工學系研究科助手](../Page/東京大學.md "wikilink")[金子勇](../Page/金子勇.md "wikilink")。因為在第2頻道宣告開發本軟體的文章編號為47號，因此也被稱作「47氏」。本軟體為了替代當時網路上很流行的另一個P2P軟體「[WinMX](../Page/WinMX.md "wikilink")」，所以用「Winny」命名（N和Y分別是M和X的後一個字母）。

2003年4月7日，「Winny 1.14」完成開發。2003年5月5日，「Winny 2」發佈\[1\]。

金子勇被[京都府警察](../Page/京都府警察.md "wikilink")[逮捕時](../Page/逮捕.md "wikilink")，Winny的最新版本為「Winny
2.0β7.1」，此外也流傳有破解版等各種非正規版本。據日本[網路安全業者](../Page/網路安全.md "wikilink")「NetAgent」統計，至2006年4月為止，Winny用戶數已在44萬到53萬之間\[2\]。
[Winny_files.png](https://zh.wikipedia.org/wiki/File:Winny_files.png "fig:Winny_files.png")

## 特征

[Winny_2.0b7.1_JPN_at_Windows_7_Ultimate_SP1_JPN.png](https://zh.wikipedia.org/wiki/File:Winny_2.0b7.1_JPN_at_Windows_7_Ultimate_SP1_JPN.png "fig:Winny_2.0b7.1_JPN_at_Windows_7_Ultimate_SP1_JPN.png")
Winny不需[服务器](../Page/服务器.md "wikilink")，因此不像其他共享软件常见的會因服务器瘫痪而无法运行。独特的[加密方式](../Page/加密.md "wikilink")，其特征为在一定的传输[概率下使一部分运行Winny的计算机成为](../Page/概率.md "wikilink")[节点（node）](../Page/节点_\(电信网络\).md "wikilink")，数据在到达目的地前需经过多次转送。[集群（cluster）技术使节点与节点间更快速连接](../Page/计算机集群.md "wikilink")。实现了匿名度和文件共享的高度平衡。更具备了[匿名](../Page/匿名.md "wikilink")[聊天功能](../Page/聊天.md "wikilink")。Winny不支援[繁體中文與](../Page/繁體中文.md "wikilink")[簡體中文](../Page/簡體中文.md "wikilink")，所以中文版Windows用戶启动Winny时会出现[乱码的问题](../Page/乱码.md "wikilink")；唯一的解决办法是将电脑语言預設为[日文](../Page/日文.md "wikilink")，或使用特制的小外掛“W2kXpCJK”（[Windows
2000](../Page/Windows_2000.md "wikilink")/[XP区域模拟和化繁为简界面程序](../Page/Windows_XP.md "wikilink")）来解决Winny乱码问题。Winny被众多[電腦安全软件视为](../Page/電腦安全.md "wikilink")[木马或](../Page/特洛伊木马_\(电脑\).md "wikilink")[電腦病毒](../Page/電腦病毒.md "wikilink")，並强行隔离或删除。

Winny 2.0β7.1無法在[Windows 7
SP1日文旗艦版](../Page/Windows_7.md "wikilink")[64位元版上正常運作](../Page/64位元.md "wikilink")（見右圖）：日文界面全都變成亂碼，大多數的按鍵都無作用。

## 法律問題

2004年5月31日，[京都地方檢察廳將金子勇](../Page/京都地方檢察廳.md "wikilink")[起訴](../Page/起訴.md "wikilink")。

2006年12月13日，[京都地方裁判所一審宣判](../Page/京都地方裁判所.md "wikilink")，以「幫助侵害著作權」罪名判處金子勇150萬[日圓](../Page/日圓.md "wikilink")[罰金](../Page/罰金.md "wikilink")；金子勇於當日提起上訴。

2009年10月8日，經過約四年的訴訟，[大阪高等裁判所二審宣判](../Page/大阪高等裁判所.md "wikilink")，推翻一審判決，金子勇獲判無罪；[審判長](../Page/審判長.md "wikilink")[小倉正三指稱](../Page/小倉正三.md "wikilink")，無明確證據顯示金子勇是以幫助侵權為目的而開發Winny，若以不清晰的基準而將軟件開發者入罪，會令他們對開發軟件卻步。\[3\]\[4\]

2009年10月21日，[大阪高等檢察廳因不服二審判決而向](../Page/大阪高等檢察廳.md "wikilink")[最高裁判所提起上訴](../Page/最高裁判所.md "wikilink")。\[5\]

2011年12月10日，最高裁判所三審宣判金子勇無罪定讞。\[6\]

## 資料來源

## 外部链接

  - [Winny资源库（Winnyファイル検索）](http://www.nyhash.info/)

  - [Winny？（ウィニー）](https://web.archive.org/web/20070117225703/http://winny.cool.ne.jp/)

<!-- end list -->

  - [Winnyリンク集+](https://web.archive.org/web/20081013141201/http://denpla.cool.ne.jp/ny/)

<!-- end list -->

  - [Winnyまとめページ](http://magic3.net/winny.html)

  - [日本最火P2P软件Winny带毒 开发者坐牢](http://www.ppcn.net/n3323c1.aspx)

[Category:P2P](../Category/P2P.md "wikilink")
[Category:下載工具](../Category/下載工具.md "wikilink")
[Category:匿名網路](../Category/匿名網路.md "wikilink")

1.
2.
3.
4.
5.
6.