<table>
<thead>
<tr class="header">
<th><p>| 2,6-二丁基羥基甲苯</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Butylated_hydroxytoluene_(BHT)_Structural_Formula_V1.svg" title="fig:Butylated_hydroxytoluene_(BHT)_Structural_Formula_V1.svg">Butylated_hydroxytoluene_(BHT)_Structural_Formula_V1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>| 常規</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/IUPAC命名法.md" title="wikilink">IUPAC命名法</a></p></td>
</tr>
<tr class="even">
<td><p>其他名稱</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/化學式.md" title="wikilink">化學式</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SMILES.md" title="wikilink">SMILES</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/摩爾質量.md" title="wikilink">摩爾質量</a></p></td>
</tr>
<tr class="even">
<td><p>外表</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/CAS號.md" title="wikilink">CAS號</a></p></td>
</tr>
<tr class="even">
<td><p>| 特性</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/密度.md" title="wikilink">密度和</a><a href="../Page/相態.md" title="wikilink">相態</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/水.md" title="wikilink">水溶性</a></p></td>
</tr>
<tr class="odd">
<td><p>其他<a href="../Page/溶劑.md" title="wikilink">溶劑</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熔點.md" title="wikilink">熔點</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沸點.md" title="wikilink">沸點</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酸度係數.md" title="wikilink">酸度係數</a> (p<em>K</em><sub>a</sub>)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/酸度係數.md" title="wikilink">鹼度係數</a> (p<em>K</em><sub>b</sub>)</p></td>
</tr>
<tr class="even">
<td><p>| 危險性</p></td>
</tr>
<tr class="odd">
<td><p>MSDS</p></td>
</tr>
<tr class="even">
<td><p>主要危險性</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NFPA_704.md" title="wikilink">NFPA 704</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/閃點.md" title="wikilink">閃點</a></p></td>
</tr>
<tr class="odd">
<td><p>R/S數</p></td>
</tr>
<tr class="even">
<td><p>RTECS數</p></td>
</tr>
<tr class="odd">
<td><p>| 相關化合物</p></td>
</tr>
<tr class="even">
<td><p>相關<a href="../Page/化合物.md" title="wikilink">化合物</a></p></td>
</tr>
<tr class="odd">
<td><p>| <small>所有數據均在標準環境下量度(25°C, 100kPa)</small></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

**2,6-二丁基羥基甲苯**，通常叫作**BHT**，是一種[油溶性的](../Page/油.md "wikilink")[有機化合物](../Page/有機化合物.md "wikilink")。它主要在[食品添加劑中被用作為](../Page/食品添加劑.md "wikilink")[抗氧化劑](../Page/抗氧化劑.md "wikilink")（[E編碼為E](../Page/E編碼.md "wikilink")321）。它也在[化妝品](../Page/化妝品.md "wikilink")、[藥物](../Page/藥物.md "wikilink")、[飛機](../Page/飛機.md "wikilink")[燃料](../Page/燃料.md "wikilink")、[橡膠](../Page/橡膠.md "wikilink")、[石油製品和](../Page/石油.md "wikilink")[標本中有抗氧化作用](../Page/標本.md "wikilink")。

## 使用情況

**BHT**在1947年被[專利](../Page/專利.md "wikilink")。1954年被[美國食品藥品監督管理局准許在食物中加入BHT來保鮮](../Page/美國食品藥品監督管理局.md "wikilink")。BHT和[自由基產生化學反應](../Page/自由基.md "wikilink")，並減慢食物中[氧化還原反應](../Page/氧化還原反應.md "wikilink")（即[酸敗反應](../Page/酸敗反應.md "wikilink")）的速度。由此可保持食物的[顏色](../Page/顏色.md "wikilink")、[氣味和味道](../Page/氣味.md "wikilink")。

在生產某些物質如[四氫呋喃及](../Page/四氫呋喃.md "wikilink")[乙醚時](../Page/乙醚.md "wikilink")，亦可加入BHT來避免產生有害的[過氧化](../Page/過氧化物.md "wikilink")[有機物](../Page/有機物.md "wikilink")。

## 管制

**BHT**的使用引起了世界的關注。[日本於](../Page/日本.md "wikilink")1958年，以及[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[瑞典和](../Page/瑞典.md "wikilink")[澳洲等地](../Page/澳洲.md "wikilink")，BHT都被禁止加在食物中。[美國則禁止在某些食物中加入BHT](../Page/美國.md "wikilink")。不過，有些食品商自願不使用BHT，例如[麥當勞於](../Page/麥當勞.md "wikilink")1986年開始不使用BHT。

## 參見

  - [酚](../Page/酚.md "wikilink")
  - [BHA](../Page/BHA.md "wikilink")

## 外部連結

  - [為何食物中有BHA和BHT?
    它們安全嗎？（英文）](http://chemistry.about.com/library/weekly/aa082101a.htm)
  - [BHT的生物論文（英文）](https://web.archive.org/web/20051227185835/http://ntp.niehs.nih.gov/index.cfm?objectid=070510F7-946E-0334-8C3427E3D9734FD0)
  - [BHT
    (ICSC)（英文）](http://www.inchem.org/documents/icsc/icsc/eics0841.htm)

[Category:抗氧化剂](../Category/抗氧化剂.md "wikilink")
[Category:甲酚](../Category/甲酚.md "wikilink")
[Category:燃料抗氧化剂](../Category/燃料抗氧化剂.md "wikilink")
[Category:内分泌干扰物](../Category/内分泌干扰物.md "wikilink")
[Category:食品抗氧化剂](../Category/食品抗氧化剂.md "wikilink")
[Category:环境激素](../Category/环境激素.md "wikilink")