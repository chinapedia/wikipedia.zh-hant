**夷隅市**（）是[千葉縣](../Page/千葉縣.md "wikilink")[房總半島東南部的一](../Page/房總半島.md "wikilink")[市](../Page/市.md "wikilink")。往[茂原市的通勤率為](../Page/茂原市.md "wikilink")10.2%（平成22年國勢調査）。

## 概要

2005年12月5日，[夷隅郡](../Page/夷隅郡.md "wikilink")、、合併，為千葉縣第34個市，也是縣內唯一使用平假名的自治體。

## 地理

位於千葉縣[房總半島東部](../Page/房總半島.md "wikilink")，市內主要河川有及其支流落合川、桑田川。

南端的位於本市東北部，也是[九十九里濱的終點](../Page/九十九里濱.md "wikilink")。其南方是小型丘陵地。西南部屬平緩的。中央地區是廣大的水田。南部至海岸之間為丘陵地。

本市外海海底是日本最大的岩礁群。稱「機械根」。1900年代後半也被稱作「寶之海」（宝の海）。

此地西部富含[碘礦](../Page/碘.md "wikilink")。

由於地處半島東部，交通並不便利。國道128號線在觀光季節的交通阻塞已影響居民的生活。遠離[外房線的市西部人口正在流失](../Page/外房線.md "wikilink")。

搭乘一般列車往[千葉站需要](../Page/千葉站.md "wikilink")65分鐘，搭乘特急前往[蘇我站需要](../Page/蘇我站.md "wikilink")40分鐘、前往[東京站需要](../Page/東京站.md "wikilink")70分鐘。

### 鄰接自治體

  - [勝浦市](../Page/勝浦市.md "wikilink")
  - [長生郡](../Page/長生郡.md "wikilink")
      - [一宮町](../Page/一宮町.md "wikilink")
      - [睦澤町](../Page/睦澤町.md "wikilink")
  - 夷隅郡
      - [大多喜町](../Page/大多喜町.md "wikilink")
      - [御宿町](../Page/御宿町.md "wikilink")

## 歷史

### 沿革

  - 1899年（[明治](../Page/明治.md "wikilink")32年）12月13日 -
    房總鐵道（現在的[外房線](../Page/外房線.md "wikilink")）一之宮 -
    大原路段通車。
  - 1950年（[昭和](../Page/昭和.md "wikilink")25年）11月 - 設置。
  - 1953年（昭和28年）5月18日 - 制定。
  - 1988年（昭和63年）3月24日 - JR木原線廢止。[夷隅鐵道夷隅線開業](../Page/夷隅線.md "wikilink")。
  - 1993年（[平成](../Page/平成.md "wikilink")5年）4月1日 - 制定。
  - 2005年（平成17年）12月5日 - 、、合併成立**夷隅市**。

### 地名由來

[古事記記載為](../Page/古事記.md "wikilink")「伊自牟（いじむ）」。[日本書紀寫作](../Page/日本書紀.md "wikilink")「伊甚（いじみ）」，此地的朝廷直轄地為「伊甚屯倉（いじむのみやけ）」。江戶時代將漢字定為「夷隅（いすみ）」

### 行政區域變遷

  - **變遷年表**

| 夷隅市市域變遷（年表）  |
| ------------ |
| 年            |
| 1889年（明治22年） |
| 1893年（明治26年） |
| 1899年（明治32年） |
| 1900年（明治33年） |
| 1953年（昭和28年） |
| 1954年（昭和29年） |
| 12月1日        |
| 1955年（昭和30年） |
| 1961年（昭和36年） |
| 2005年（平成17年） |

  - **變遷表**

<table>
<thead>
<tr class="header">
<th><p>夷隅市市域變遷表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1868年<br />
以前</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夷隅郡.md" title="wikilink">夷隅郡</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

## 行政

### 市長

  - 太田 洋（おおた ひろし，第三任）

### 市役所

  - 夷隅市役所
      - 本廳舍（舊大原町役場）
      - 夷隅廳舍（舊夷隅町役場）
      - 岬廳舍（舊岬町役場）

### 警察

  - （管轄夷隅市、御宿町）

### 消防

  - （夷隅市、勝浦市、大多喜町、御宿町）

  - 大原消防署

      - 夷隅分署
      - 岬分署

  - 夷隅市消防團

### 其他

  - 千葉地方法務局夷隅出張所

  - 大原（茂原公共職業安定所夷隅支所）

  - 夷隅地域整備中心

  - 夷隅農林振興中心

  - 南部漁港事務所大原支所

## 經濟

市內國道128號沿線有[大型量販店](../Page/超級廣場.md "wikilink")（）、[居家賣場](../Page/五金.md "wikilink")（）、家電量販店（）等，是夷隅郡市地區的經濟中心。

### 產業

  - 農業
      - 水田集中在流域與大原浪花地區，生產的房總乙女（フサオトメ）、[越光米以](../Page/越光米.md "wikilink")「螢米」（ほたる米）的名義上市。
      - 岬町谷上地區至鄰接的一宮町，以[梨栽培為副業](../Page/梨.md "wikilink")。
      - 山田地區至勝浦市也栽種[奇異果](../Page/奇異果.md "wikilink")、[香菇](../Page/香菇.md "wikilink")、[筍](../Page/筍.md "wikilink")、[藍莓](../Page/藍莓.md "wikilink")、[蕎麥等](../Page/蕎麥.md "wikilink")。
  - 漁業
      - 夷隅川水系有河魚放流的活動。

      - 等籌組的「夷隅東部漁業協同組合」，其[伊勢蝦捕獲量位居日本各漁協第一](../Page/日本龍蝦.md "wikilink")。

## 姊妹、友好都市

### 國內

  - [南魚沼市](../Page/南魚沼市.md "wikilink")（[新潟縣](../Page/新潟縣.md "wikilink")）

<!-- end list -->

  -
    舊岬町與舊[南魚沼郡](../Page/南魚沼郡.md "wikilink")。

<!-- end list -->

  - [長野原町](../Page/長野原町.md "wikilink")（[群馬縣](../Page/群馬縣.md "wikilink")[吾妻郡](../Page/吾妻郡.md "wikilink")）

<!-- end list -->

  -
    舊大原町與長野原町。

### 海外

  - 沃潘（Waupun）（[美國](../Page/美國.md "wikilink")[威斯康辛州](../Page/威斯康辛州.md "wikilink")）

  - [杜魯斯](../Page/杜魯斯.md "wikilink")（美國[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")）

## 教育

### 小學校

  - 大原地區
      - 夷隅市立大原小學校

      - 夷隅市立東小學校

      - 夷隅市立東海小學校

      - 夷隅市立浪花小學校

      -
  - 夷隅地區
      - 夷隅市立國吉小學校
      - 夷隅市立千町小學校
      - 夷隅市立中川小學校
  - 岬地區
      - 夷隅市立太東小學校
      - 夷隅市立長者小學校
      - 夷隅市立中根小學校
      - 夷隅市立古澤小學校

### 中學校

  - 大原地區
      -
  - 夷隅地區
      - 夷隅市立國吉中學校
  - 岬地區
      - 夷隅市立岬中學校

### 高校

  -
### 特別支援學校

  -
### 其他

  - 夷隅市鄉土資料館
      - 夷隅地區歷史資料與[日本畫](../Page/日本畫.md "wikilink")[狩野派繪畫等](../Page/狩野派.md "wikilink")。
  - 夷隅市水彩畫廊
      - 日本少數[水彩畫專門畫廊](../Page/水彩畫.md "wikilink")。

## 公共設施

### 文化設施

  - 夷隅文化會館
  - 大原文化中心
  - 岬接觸會館文化中心

### 公民館

  - 夷隅公民館（夷隅文化會館內）
  - 大原公民館（大原文化中心內）
  - 岬公民館

## 交通

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）

<!-- end list -->

  - [外房線](../Page/外房線.md "wikilink")： -  -  -  -

<!-- end list -->

  - [夷隅鐵道](../Page/夷隅鐵道.md "wikilink")

<!-- end list -->

  - [夷隅線](../Page/夷隅線.md "wikilink")： -  -  -  -  -

### 路線巴士

  - （[小湊鐵道](../Page/小湊鐵道.md "wikilink")、運行）

      - 夷隅接駁巴士
      - 市內循環線
      - 大原巡迴線

[File:IsumicityBus.JPG|夷隅市民巴士](File:IsumicityBus.JPG%7C夷隅市民巴士)
[File:IsumiShuttle.JPG|夷隅接駁巴士](File:IsumiShuttle.JPG%7C夷隅接駁巴士)

### 道路

  - **[一般國道](../Page/一般國道.md "wikilink")**
      - （外房黑潮線）

      -
  - **[主要地方道](../Page/主要地方道.md "wikilink")**
      -
      -
  - **[一般縣道](../Page/都道府縣道.md "wikilink")**
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
  - **[自行車道](../Page/自行車道.md "wikilink")**
      -
  - **其他主要道路**
      - 南總廣域農道

## 名勝、古蹟、觀光地、祭典、活動

### 祭典、活動

  -
  - 萬木城祭

  - 岬SURF TOWN FESTA（岬サーフタウンフェスタ）

  - 港之朝市

### 名勝、古蹟

[Isumi_Kiyomizu-dera.jpg](https://zh.wikipedia.org/wiki/File:Isumi_Kiyomizu-dera.jpg "fig:Isumi_Kiyomizu-dera.jpg")

  - 萬木城跡公園（）

  - （）

  -
  - \-不動堂－別名波切不動。賞櫻、紫陽花景點。

  - 岩船地藏尊

  - 發坂峠

  - （[曹洞宗](../Page/曹洞宗.md "wikilink")）

  - （，欄間雕刻）

  - 飯縄寺 （波之伊八，本堂外陣欄間雕刻「波與飛龍」）

  - （坂東三十三觀音靈場32號札所）

  - （[日蓮宗](../Page/日蓮宗.md "wikilink")）

### 觀光地

  -
  - 椿公園

  - 童謠之里

  - 椿之里

  - 源氏螢之里（源氏ぼたるの里）

  - 太東海濱植物群落

  - 蜻蜓沼（トンボの沼）

  - [海水浴場](../Page/海水浴場.md "wikilink")

      - 太東海水浴場
      - 大原海水浴場

  - 小濱八幡神社

  - 大原公園

  - [麻雀博物館](../Page/麻雀博物館.md "wikilink")（一年一度的岬町接觸麻雀大會是日本國內最大規模的大會）

  - 萬木城跡公園

  - 大原（おおはら）海鮮市場

  - 椿之里

  - 隘路

## 知名人士

  - [森葉子](../Page/森葉子.md "wikilink")（[朝日電視台播音員](../Page/朝日電視台.md "wikilink")）

## 備註

## 外部連結

  - [夷隅市](http://www.city.isumi.lg.jp/)