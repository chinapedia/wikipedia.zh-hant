**甚大天线阵**（，缩写为）是由27台25米口径的天线组成的[射电望远镜阵列](../Page/射电望远镜.md "wikilink")，位于[美国](../Page/美国.md "wikilink")[新墨西哥州的圣阿古斯丁平原上](../Page/新墨西哥州.md "wikilink")，海拔2124米，是世界上最大的[综合孔径射电望远镜](../Page/综合孔径射电望远镜.md "wikilink")。

甚大天线阵每个天线重230吨，架设在铁轨上，可以移动，所有天线呈Y形排列，每臂长21千米，组合成的最长基线可达36千米。甚大天线阵隶属于[美国国家射电天文台](../Page/美国国家射电天文台.md "wikilink")（），于1981年建成，工作于6个波段，最高分辨率可以达到0.05角秒，与地面大型[光学望远镜的分辨率相当](../Page/光学望远镜.md "wikilink")。

天文学家使用甚大天线阵做出了一系列重要的发现，例如发现[银河系内的](../Page/银河系.md "wikilink")[微类星体](../Page/微类星体.md "wikilink")、遥远星系周围的[爱因斯坦环](../Page/爱因斯坦环.md "wikilink")、[伽玛射线暴的射电波段对应体等等](../Page/伽玛射线暴.md "wikilink")。

甚大天线阵曾出现在多部影视作品中，包括《[威震太陽神](../Page/威震太陽神.md "wikilink")》、《[接触](../Page/超时空接触.md "wikilink")》、《[独立日](../Page/ID4星際終結者.md "wikilink")》等。

## 參看

  - [帕瑞纳天文台](../Page/帕瑞纳天文台.md "wikilink")
  - [拉西拉天文台](../Page/拉西拉天文台.md "wikilink")
  - [托洛洛山美洲际天文台](../Page/托洛洛山美洲际天文台.md "wikilink")
  - [甚大望远镜](../Page/甚大望远镜.md "wikilink")
  - [欧洲极大望远镜](../Page/欧洲极大望远镜.md "wikilink")
  - [500米口徑球面射電望遠鏡](../Page/500米口徑球面射電望遠鏡.md "wikilink")

## 外部链接

  - [甚大天线阵网站](http://www.vla.nrao.edu/)

[V](../Category/射电望远镜.md "wikilink")
[V](../Category/干涉儀望遠鏡.md "wikilink")
[V](../Category/新墨西哥州天文台.md "wikilink")