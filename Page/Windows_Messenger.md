**Windows
Messenger**是微软公司开发的[即时消息](../Page/即时消息.md "wikilink")[客户端软件](../Page/客户端.md "wikilink")，內建於[Windows
XP中](../Page/Windows_XP.md "wikilink")。能够同时登录到[SIP和](../Page/SIP.md "wikilink")[.NET
Messenger
Service](../Page/.NET_Messenger_Service.md "wikilink")。從版本5開始，它能在[Windows
2000](../Page/Windows_2000.md "wikilink")、[Windows
XP和](../Page/Windows_XP.md "wikilink")[Windows Server
2003中运行](../Page/Windows_Server_2003.md "wikilink")，但不包括[Windows
Vista](../Page/Windows_Vista.md "wikilink")。

Windows Messenger與[Windows Live
Messenger](../Page/Windows_Live_Messenger.md "wikilink")（前稱MSN
Messenger）是不同的，縱使名稱相似。後者可以從微軟的網站下載。此外，Windows
Messenger與[信差服務並沒有關係](../Page/信差服務.md "wikilink")，後者是[Windows
NT的](../Page/Windows_NT.md "wikilink")[LAN
Manager組件中的一部分](../Page/LAN_Manager.md "wikilink")。

在商業應用環境中，Windows Messenger已經被[Office
Communicator取代](../Page/Microsoft_Office_Communicator.md "wikilink")。在[.NET
Messenger
Service中](../Page/.NET_Messenger_Service.md "wikilink")，亦被[Windows
Live Messenger取代](../Page/Windows_Live_Messenger.md "wikilink")，

## 概要

Windows Messenger與Windows
XP一同發佈，亦預設安裝於系統中。它有數個功能，例如[即时消息](../Page/即时消息.md "wikilink")、在線顯示、檔案傳輸，程式共用和白板。而後來的版本亦新增了手寫功能寫入，與[Live
Communication
Server整合](../Page/Microsoft_Live_Communication_Server.md "wikilink")。此外，Windows
Messenger亦與[Microsoft
Exchange](../Page/Microsoft_Exchange.md "wikilink")、[Microsoft
Outlook](../Page/Microsoft_Outlook.md "wikilink")、[Microsoft Outlook
Express和Windows](../Page/Microsoft_Outlook_Express.md "wikilink")
XP中的遠端協助整合。Windows Messenger亦與Windows XP Media Center
Edition中的Media Center整合。Windows Messenger可以與[Exchange Server 2000
Instant Messaging
Service和](../Page/Exchange_Server_2000_Instant_Messaging_Service.md "wikilink")[.NET
Messenger Service溝通](../Page/.NET_Messenger_Service.md "wikilink")。

Windows Messenger的開發在版本5.1之後停止了，而後續者是[Windows Live
Messenger和](../Page/Windows_Live_Messenger.md "wikilink")[Office
Communicator](../Page/Microsoft_Office_Communicator.md "wikilink")。Windows
Messenger的插件，例如登入Hotmail戶口收件箱的插件，亦不能繼續使用。用戶可視乎需要，選擇使用Windows Live
Messenger或者Office Communicator。

## 用戶介面

與[Windows Live
Messenger不同](../Page/Windows_Live_Messenger.md "wikilink")，Windows
Messenger的用戶介面較簡單。後者的功能亦較少，例如動畫快遞、來電振動、自定義的表情符號，都不被支援。Windows
Messenger的介面，與標準Windows XP的Luna主題相似。

## 版本和功能列表

### Windows Messenger 4.x

  - Version 4.0
      - 在Windows XP發佈後，Windows Messenger立刻更新到版本4.5。
  - Version 4.5
  - Version 4.6

[WM4.7ConversationWindow.png](https://zh.wikipedia.org/wiki/File:WM4.7ConversationWindow.png "fig:WM4.7ConversationWindow.png")

  - Version 4.7（4.7.2009）
  - Version 4.7（4.7.2010）

對於未使用Windows XP Service Pack
2的用戶，版本4.7.2009修正了一個重大的[安全性問題](https://web.archive.org/web/20071023180902/http://www.microsoft.com/technet/security/Bulletin/MS05-009.mspx)，亦是最後一個支援插件功能的版本。

  - Version 4.7（4.7.3000）

利用了Windows Messenger 5的用戶介面，亦增強了安全性。它會禁止傳輸某些檔案，與Windows XP Service Pack
2一同發佈。

  - Version 4.7（4.7.3001）

修正了一個[安全性問題](https://web.archive.org/web/20071023180902/http://www.microsoft.com/technet/security/Bulletin/MS05-009.mspx)，與[Windows
XP Media Center
Edition](../Page/Windows_XP_Media_Center_Edition.md "wikilink")
2005一同發佈，也是WIndows XP Service Pack 2中版本4.7.3000的一個更新。

  - Version 4.8

新增支援手寫功能寫入，與[Windows XP Tablet PC
Edition](../Page/Windows_XP_Tablet_PC_Edition.md "wikilink")2005一同發佈。

### Windows Messenger 5.x

版本5.x亦可在[Windows 2000和](../Page/Windows_2000.md "wikilink")[Windows
Server 2003使用](../Page/Windows_Server_2003.md "wikilink")。

  - Version 5.0
      - 第一個支援[LCS的版本](../Page/Microsoft_Live_Communication_Server.md "wikilink")。
  - Version 5.1.0639（2004年12月1日）。新功能
    [1](http://support.microsoft.com/kb/899283):
      - 第一個支援[LCS](../Page/Microsoft_Live_Communication_Server.md "wikilink")
        2005的版本。
      - 增強對Tablet PC和手寫功能寫入的支援。
      - 當使用者執行全屏程式，或者Windows Installer的安裝程式時，狀態可以自動調較為「忙碌」。
  - Version 5.1.0680（2005年5月13日）。
      - 增強多網絡連接時的檔案傳輸功能。
      - 狀態顯示數據，可傳送到Microsoft Outlook和Windows SharePoint。
      - 用於x64的64-bit版本
  - Version 5.1.0700（2005年9月16日）。
      - 透過最新的安全部件，增強安全性。
      - 修正視像通訊時，聲音與影像的同步性。
  - Version 5.1.0701（2007年4月12日）。
      - 增強多網絡連接時的檔案傳輸功能。
      - 用於有線連接的配置
      - 用於無線網絡連接的配置
      - 用於虛擬專用網絡（VPN）連線的配置
  - Version 5.1.0706（2007年6月4日）
      - 透過最新的安全部件，增強安全性。
  - Version 5.1.0.715（2008年8月12日）
      - 通过使用最新安全组件提高安全性。

## 参见

  - [即时通讯软件列表](../Page/即时通讯软件列表.md "wikilink")
  - [即时通讯软件比较](../Page/即时通讯软件比较.md "wikilink")
  - [Comparison of instant messaging
    protocols](../Page/Comparison_of_instant_messaging_protocols.md "wikilink")
  - [Windows Live
    Messenger](../Page/Windows_Live_Messenger.md "wikilink")
  - [Microsoft Office
    Communicator](../Page/Microsoft_Office_Communicator.md "wikilink")

[Category:即时通讯软件](../Category/即时通讯软件.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:Windows组件](../Category/Windows组件.md "wikilink")