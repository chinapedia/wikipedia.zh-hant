**長萬部車站**（）是[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[山越郡](../Page/山越郡.md "wikilink")[長萬部町的](../Page/長萬部町.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")，隸屬於[北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")（JR北海道），實際位置位於長萬部町字長萬部。

## 車站構造

長萬部車站是室蘭本線下行方向的起站，室蘭本線在此處與函館本線交會，因此主要列車在此站皆有停靠。目前正在興建中的[北海道新幹線預計會通過長萬部車站](../Page/北海道新幹線.md "wikilink")，並計畫將此站列入停車車站之一。

在1987年廢線之前，行駛於[瀨棚線上的列車也會自](../Page/瀨棚線.md "wikilink")[國縫車站](../Page/國縫車站.md "wikilink")（瀨棚線與函館本線的交會點）處繼續延伸路線到長萬部車站處，以便利旅客接駁。

長萬部車站全站都位於地面上，由於是[函館本線與](../Page/函館本線.md "wikilink")[室蘭本線的分歧點](../Page/室蘭本線.md "wikilink")，長萬部車站內設置有兩座[島式月台共四條乘車線以符合轉運需求](../Page/島式月台.md "wikilink")。

自最靠近車站站體方面起依序是第一、二、三與四號月台，其中駛往[函館方向的快車或特急列車使用一號月台](../Page/函館車站.md "wikilink")，駛往[札幌方向的則使用二號月台](../Page/札幌車站.md "wikilink")。至於普通的列車除了使用三號與四號月台外，偶爾也會有部分車次使用二號月台。

車站內設至有[綠窗口](../Page/綠窗口.md "wikilink")（，JR系統的票務櫃臺），旅行諮詢室，與[KIOSK](../Page/Kiosk_\(便利商店\).md "wikilink")（，連鎖便利商店）賣店。

此站預定為[北海道新幹線的車站](../Page/北海道新幹線.md "wikilink")。

### 月台配置

<table>
<caption>月台配置</caption>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>類別</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>函館本線</p></td>
<td><p>上行</p></td>
<td><p>特急</p></td>
<td><p><a href="../Page/新函館北斗站.md" title="wikilink">新函館北斗</a>、<a href="../Page/函館站.md" title="wikilink">函館方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>室蘭本線</p></td>
<td><p> </p></td>
<td><p>特急</p></td>
<td><p><a href="../Page/東室蘭站.md" title="wikilink">東室蘭</a>、<a href="../Page/苫小牧站.md" title="wikilink">苫小牧</a>、<a href="../Page/札幌站.md" title="wikilink">札幌方向</a></p></td>
</tr>
<tr class="odd">
<td><p>3・4</p></td>
<td><p>函館本線</p></td>
<td><p>上行</p></td>
<td><p>普通</p></td>
<td><p><a href="../Page/八雲站.md" title="wikilink">八雲</a>、<a href="../Page/森站_(北海道).md" title="wikilink">森</a>、函館方向</p></td>
</tr>
<tr class="even">
<td><p>函館本線</p></td>
<td><p>下行</p></td>
<td><p>普通</p></td>
<td><p><a href="../Page/俱知安站.md" title="wikilink">俱知安</a>、<a href="../Page/小樽站.md" title="wikilink">小樽方向</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>室蘭本線</p></td>
<td><p> </p></td>
<td><p>普通</p></td>
<td><p>東室蘭、苫小牧方向</p></td>
<td></td>
</tr>
</tbody>
</table>

## 歷史

  - 1903年（[明治](../Page/明治.md "wikilink")36年）11月3日：以函館本線的車站身份開始營運。
  - 1923年（[大正](../Page/大正.md "wikilink")12年）12月10日：長輪線（今日的室蘭本線）開始營運。
  - 1987年（[昭和](../Page/昭和.md "wikilink")62年）4月1日：[日本國鐵分割民營化](../Page/日本國鐵分割民營化.md "wikilink")，長萬部車站的擁有與經營權由北海道旅客鐵道繼承。

## 車站周邊

  - [長萬部溫泉](../Page/長萬部溫泉.md "wikilink")
  - [北海道長萬部高等學校](../Page/北海道長萬部高等學校.md "wikilink")
  - [東京理科大學長萬部校區](../Page/東京理科大學.md "wikilink")
  - [太平洋海岸](../Page/太平洋.md "wikilink")

## 相鄰車站

特急列車的停靠站參見各列車條目。

  - 北海道旅客鐵道（JR北海道）

    函館本線

      -
        普通
          -
            [中之澤](../Page/中之澤站.md "wikilink")（H48）－**長萬部（H47）**－[二股](../Page/二股站.md "wikilink")（S32）

    室蘭本線

      -
        普通
          -
            **長萬部（H47）**－[靜狩](../Page/靜狩站.md "wikilink")（H46）

    北海道新幹線（建設中）

      -

          -

            （臨時名稱）－**長萬部**－[俱知安](../Page/俱知安站.md "wikilink")

<!-- end list -->

  -

      -
        註：長萬部與靜狩之間原本還存在著一名為[旭濱的車站](../Page/旭濱車站.md "wikilink")，但該站已於2006年3月18日廢站。

## 參考資料

  - 《道內時刻表》2006年8月號，交通新聞社（日本札幌）。

## 外部連結

  - [JR北海道官方網站](http://www.jrhokkaido.co.jp/)（日文，中文繁體，韓文）
  - [長萬部町官方網站](http://www.town.oshamambe.lg.jp/)（日文）
  - [北海道故鄉的車站（北海道新聞社）](https://web.archive.org/web/20070929083249/http://www5.hokkaido-np.co.jp/nature/station/HAKODATE/OSYAMANB/OSYAMANB.HTML)（日文）

[Category:渡島管內鐵路車站](../Category/渡島管內鐵路車站.md "wikilink")
[Category:函館本線車站](../Category/函館本線車站.md "wikilink")
[Category:室蘭本線車站](../Category/室蘭本線車站.md "wikilink")
[Shamanbe](../Category/日本鐵路車站_O.md "wikilink")
[Category:1903年啟用的鐵路車站](../Category/1903年啟用的鐵路車站.md "wikilink")
[Category:長萬部町](../Category/長萬部町.md "wikilink")
[Category:1903年日本建立](../Category/1903年日本建立.md "wikilink")