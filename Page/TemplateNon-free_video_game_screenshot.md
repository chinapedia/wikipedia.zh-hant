}|yes| {{\#ifeq:|File|}} |
**致管理員與巡查員**：如果本圖像有一份**合適的**合理使用依據，請在本模板增加`|image
has rationale=yes`參數。}} {{\#ifeq:|File|{{\#ifeq:|yes|}}}}}}

-----

<div class="NavFrame collapsed" style="border-style: none; padding: 0px;">

<div class="NavHead" style="background: transparent; text-align: left; padding: 0px;">

**用法：**

</div>

<div class="NavContent" style="text-align: left; padding: 0px;">

本模板接受下述的作業系統平臺參數，以讓本圖像歸類於較合適之分類：

    {{Non-free video game screenshot|Windows|Macintosh|Linux}}

或

    {{Non-free video game screenshot|Windows}}

如果不知道其所屬作業系統平臺，可以只用

    {{Non-free video game screenshot}}

本模板適用的作業系統平臺類型，請參考[:Category:电子游戏屏幕截图的子分類](:../Category/电子游戏屏幕截图.md "wikilink")。

</div>

</div>

{{\#ifeq:|PC|
<span class="error">Possible tagging error: this image has been tagged
as a "PC" game screenshot. This should only be done if it is for an
*unidentifiable* personal computer platform.</span>}}
}}<includeonly>}}|街機||\[\[Category:{{\#if:

`|`

}}{{\#if:

`|`

}}{{\#if:

`|`

}}{{\#if:

`|`

}}{{\#if:

`|`

}}{{\#if:

`|`

}}{{\#if:

`|`

}}}}}</includeonly><noinclude>  </noinclude>

[Category:合理使用理據檢查完成影像](../Category/合理使用理據檢查完成影像.md "wikilink")
[Category:合理使用理據待檢查影像](../Category/合理使用理據待檢查影像.md "wikilink")
[Category:合理使用理據檢查完成影像
(機器人標註)](../Category/合理使用理據檢查完成影像_\(機器人標註\).md "wikilink")
[Category:街机游戏屏幕截图](../Category/街机游戏屏幕截图.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")
[}}游戏屏幕截图](../Category/{{Non-free_video_game_screenshot/platform.md "wikilink")