[缩略图](https://zh.wikipedia.org/wiki/File:Anglo_Japanese_Alliance_30_January_1902.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:PanamaCanal1913a.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:RUSSOJAPANESEWARIMAGE.jpg "fig:缩略图")

## 大事记

  - [1900年](../Page/1900年.md "wikilink")——[奈及利亞成為](../Page/奈及利亞.md "wikilink")[英国的保護國](../Page/英国.md "wikilink")。
  - 1900年，八国聯軍攻入北京，慈禧與光绪帝出逃西安。
  - [1901年](../Page/1901年.md "wikilink")——[諾貝爾獎正式創立](../Page/諾貝爾獎.md "wikilink")。
  - [1901年](../Page/1901年.md "wikilink")——张刘二人联名三次上奏呼吁改革，4月，清廷成立[督办政务处](../Page/督办政务处.md "wikilink")，任[荣禄](../Page/荣禄.md "wikilink")、[庆亲王及](../Page/庆亲王.md "wikilink")[李鸿章为主管](../Page/李鸿章.md "wikilink")，张之洞及刘坤一为协同办理，就全面改革作整体规划，[清末新政正式启动](../Page/清末新政.md "wikilink")。
  - [1902年](../Page/1902年.md "wikilink")——[英日同盟正式締結](../Page/英日同盟.md "wikilink")。
  - [1903年](../Page/1903年.md "wikilink")——[巴拿馬運河開始建設](../Page/巴拿馬運河.md "wikilink")。
  - [1904年至](../Page/1904年.md "wikilink")[1905年](../Page/1905年.md "wikilink")——[日俄戰爭爆發](../Page/日俄戰爭.md "wikilink")。
  - [1906年](../Page/1906年.md "wikilink")——[英國](../Page/英國.md "wikilink")[工黨建黨](../Page/工黨.md "wikilink")。
  - [1907年](../Page/1907年.md "wikilink")——《[和平解決國際爭端公約](../Page/和平解決國際爭端公約.md "wikilink")》（海牙公約）於[荷蘭](../Page/荷蘭.md "wikilink")[海牙簽定](../Page/海牙.md "wikilink")。
  - [1908年](../Page/1908年.md "wikilink")——[保加利亞獨立獲得承認](../Page/保加利亞.md "wikilink")。
  - [1909年](../Page/1909年.md "wikilink")——日本首相[伊藤博文在滿州視察訪問時在](../Page/伊藤博文.md "wikilink")[哈爾濱車站被韓國人](../Page/哈爾濱.md "wikilink")[安重根暗殺身亡](../Page/安重根.md "wikilink")。

## 出生

  - [1900年](../Page/1900年.md "wikilink")——[栗原浩](../Page/栗原浩.md "wikilink")：[日本第](../Page/日本.md "wikilink")3任[埼玉縣知事](../Page/埼玉縣知事.md "wikilink")。
  - [1901年](../Page/1901年.md "wikilink")——[昭和天皇](../Page/昭和天皇.md "wikilink")：[日本第](../Page/日本.md "wikilink")124代天皇。
  - [1902年](../Page/1902年.md "wikilink")——[沙特·伊本·阿卜杜勒·阿齊茲·伊本·沙特](../Page/沙特·伊本·阿卜杜勒·阿齊茲·伊本·沙特.md "wikilink")：[沙烏地阿拉伯第](../Page/沙烏地阿拉伯.md "wikilink")2代國王。
  - [1903年](../Page/1903年.md "wikilink")——[東姑阿都拉曼](../Page/東姑阿都拉曼.md "wikilink")：[馬來西亞第](../Page/馬來西亞.md "wikilink")1任首相。
  - [1904年](../Page/1904年.md "wikilink")——[鄧小平](../Page/鄧小平.md "wikilink")：[中國共產黨第二代中央領導集體核心](../Page/中國共產黨.md "wikilink")。
  - [1905年](../Page/1905年.md "wikilink")——[福田赳夫](../Page/福田赳夫.md "wikilink")：[日本第](../Page/日本.md "wikilink")67任首相；[陳雲](../Page/陳雲.md "wikilink")：第二任[中央顧問委員會主任](../Page/中央顧問委員會.md "wikilink")
  - [1906年](../Page/1906年.md "wikilink")——[溥儀](../Page/溥儀.md "wikilink")：[中國](../Page/中國.md "wikilink")[清朝第](../Page/清朝.md "wikilink")11代皇帝；[迪米崔·迪米崔耶維奇·蕭士塔高維奇](../Page/迪米崔·迪米崔耶維奇·蕭士塔高維奇.md "wikilink")：[前蘇聯作曲家](../Page/前蘇聯.md "wikilink")。
  - [1907年](../Page/1907年.md "wikilink")——[三木武夫](../Page/三木武夫.md "wikilink")：[日本第](../Page/日本.md "wikilink")66任首相。
  - [1907年](../Page/1907年.md "wikilink")——[邵逸夫爵士](../Page/邵逸夫.md "wikilink")，[邵氏兄弟電影公司創辦人](../Page/邵氏兄弟.md "wikilink")、前[電視廣播有限公司行政主席](../Page/電視廣播有限公司.md "wikilink")、[邵逸夫獎創立人](../Page/邵逸夫獎.md "wikilink")、大[慈善家](../Page/慈善家.md "wikilink")。
  - [1908年](../Page/1908年.md "wikilink")——[林登·強森](../Page/林登·強森.md "wikilink")：[美國第](../Page/美國.md "wikilink")36任總統。
  - [1909年](../Page/1909年.md "wikilink")——[朱麗安娜女王](../Page/朱麗安娜女王.md "wikilink")：[荷蘭](../Page/荷蘭.md "wikilink")[奧蘭治王朝第](../Page/奧蘭治王朝.md "wikilink")5代女王。

## 逝世

  - [1900年](../Page/1900年.md "wikilink")——[黑田清隆](../Page/黑田清隆.md "wikilink")：[日本第](../Page/日本.md "wikilink")2任首相。
  - [1901年](../Page/1901年.md "wikilink")——[維多利亞女王](../Page/維多利亞女王.md "wikilink")：[英國](../Page/英國.md "wikilink")[漢諾瓦王朝第](../Page/漢諾瓦王朝.md "wikilink")4代君主。
  - [1902年](../Page/1902年.md "wikilink")——[黑田長知](../Page/黑田長知.md "wikilink")：[日本](../Page/日本.md "wikilink")[福岡藩第](../Page/福岡藩.md "wikilink")12代藩主。
  - [1903年](../Page/1903年.md "wikilink")——[古瑟貝·札納爾代利](../Page/古瑟貝·札納爾代利.md "wikilink")：[義大利第](../Page/義大利.md "wikilink")24任首相。
  - [1904年](../Page/1904年.md "wikilink")——[伊莎貝拉二世](../Page/伊莎貝拉二世.md "wikilink")：[西班牙](../Page/西班牙.md "wikilink")[波旁王朝第](../Page/波旁王朝.md "wikilink")10代君主。
  - [1905年](../Page/1905年.md "wikilink")——[立花種恭](../Page/立花種恭.md "wikilink")：[日本](../Page/日本.md "wikilink")[下手渡藩第](../Page/下手渡藩.md "wikilink")3代藩主、[三池藩第](../Page/三池藩.md "wikilink")8代藩主、[華族暨](../Page/華族_\(日本\).md "wikilink")[學習院第](../Page/學習院.md "wikilink")1任院長。
  - [1906年](../Page/1906年.md "wikilink")——[基士揚九世](../Page/基士揚九世.md "wikilink")：[丹麥](../Page/丹麥.md "wikilink")[格呂克斯堡王朝第](../Page/格呂克斯堡王朝.md "wikilink")1代國王。
  - [1907年](../Page/1907年.md "wikilink")——[奧斯卡二世](../Page/奧斯卡二世.md "wikilink")：[瑞典](../Page/瑞典.md "wikilink")[貝爾南頓提王朝第](../Page/貝爾南頓提王朝.md "wikilink")4代國王。
  - [1908年](../Page/1908年.md "wikilink")——[格羅弗·克利夫蘭](../Page/格羅弗·克利夫蘭.md "wikilink")：[美國第](../Page/美國.md "wikilink")22、24任總統。
  - [1908年](../Page/1908年.md "wikilink")——[清德宗愛新覺羅載湉](../Page/清德宗.md "wikilink")，[光緒帝](../Page/光緒帝.md "wikilink")（1871年出生）
  - [1908年](../Page/1908年.md "wikilink")——[慈禧太后](../Page/慈禧太后.md "wikilink")
  - [1909年](../Page/1909年.md "wikilink")——[立花鑑寬](../Page/立花鑑寬.md "wikilink")：[日本](../Page/日本.md "wikilink")[柳河藩第](../Page/柳河藩.md "wikilink")12代藩主。

[\*](../Category/1900年代.md "wikilink")
[終](../Category/19世纪各年代.md "wikilink")
[0](../Category/20世纪各年代.md "wikilink")