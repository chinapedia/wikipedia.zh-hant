[Ohm's_Law_with_Voltage_source.svg](https://zh.wikipedia.org/wiki/File:Ohm's_Law_with_Voltage_source.svg "fig:Ohm's_Law_with_Voltage_source.svg")構成的簡單電路。應用[歐姆定律](../Page/歐姆定律.md "wikilink")，\(v=iR\)
。\]\]
[Timer555_internal_zh.jpg](https://zh.wikipedia.org/wiki/File:Timer555_internal_zh.jpg "fig:Timer555_internal_zh.jpg")\]\]

**电路**（）或稱电子迴路，是由电气设备和**-{zh-hans:元器件;
zh-hant:元件;}-**，按一定方式連接起来，为[电荷流通提供了路径的總體](../Page/电荷.md "wikilink")，也叫电子线路或稱電氣迴路，簡稱网络或迴路。如[電源](../Page/電源.md "wikilink")、[电阻](../Page/电阻.md "wikilink")、[电容](../Page/电容.md "wikilink")、[电感](../Page/电感.md "wikilink")、[二极管](../Page/二极管.md "wikilink")、[電晶體](../Page/電晶體.md "wikilink")、[集成電路和](../Page/集成電路.md "wikilink")[电键等](../Page/电键.md "wikilink")，构成的网络、[硬體](../Page/硬體.md "wikilink")。负电荷可以在其中运动。

## 術語定義

幾個元件通過導線互相連接，形成「電路」，也可以稱為「網路」。更特定地，電路是可以形成閉合迴路的網路。「支路」是電路的一部分，每一個元件都有它獨屬的支路。任意兩條或多條支路的相交點，稱為「節點」。

## 電路種類

电路的大小，可以相差很大，小到硅片上的[集成电路](../Page/集成电路.md "wikilink")，大到高低壓[输电网](../Page/输电.md "wikilink")。

### 電子電路

根据所处理信号的不同，电子电路可以分为[模拟电路和](../Page/模拟电路.md "wikilink")[數位電路](../Page/數位電路.md "wikilink")。

#### 模拟电路

  - 自然界產生的連續性物理自然量，將連續性物理自然量轉換為[連續性電信號](../Page/連續性電信號.md "wikilink")，運算[連續性電信號的電路即稱為類比電路](../Page/連續性電信號.md "wikilink")。

<!-- end list -->

  - 模拟电路对電信号的連續性[电壓](../Page/电壓.md "wikilink")、[电流进行处理](../Page/电流.md "wikilink")。

最典型的模拟电路应用包括：[放大电路](../Page/放大电路.md "wikilink")、[振盪电路](../Page/振盪电路.md "wikilink")、[线性运算电路](../Page/线性运算电路.md "wikilink")（加法、减法、乘法、除法、微分和积分电路）。運算[連續性電信號](../Page/連續性電信號.md "wikilink")。

#### 数字电路

數位電路又名邏輯電路，是一種將連續性的電訊號，轉換為[不連續性定量電信號](../Page/不連續性定量電信號.md "wikilink")，並運算[不連續性定量電信號的電路](../Page/不連續性定量電信號.md "wikilink")。数字电路中，
信号大小為不連續並定量化的[電壓状态](../Page/電壓.md "wikilink")。多数采用-{zh-hans:[布尔代数](../Page/布尔代数.md "wikilink");
zh-hant:[布林代數](../Page/布林代數.md "wikilink");}-逻辑電路对定量後信号进行处理。典型数字电路有，[正反器](../Page/正反器.md "wikilink")、-{zh-hans:[寄存器](../Page/寄存器.md "wikilink");
zh-hant:[暫存器](../Page/暫存器.md "wikilink");}-、[加法器](../Page/加法器.md "wikilink")、[减法器等](../Page/减法器.md "wikilink")，來運算[不連續性定量電信號](../Page/不連續性定量電信號.md "wikilink")。

### 積體電路

  - [積體電路亦稱為IC](../Page/積體電路.md "wikilink")。
  - 運用積體電路設計程式（IC設計），將一般電路設計到[半導體材料裡的半導體電路](../Page/半導體.md "wikilink")（一般為矽片），稱為[積體電路](../Page/積體電路.md "wikilink")。
  - 利用[半導體技術製造出](../Page/半導體.md "wikilink")[積體電路](../Page/積體電路.md "wikilink")。

## 電路定律

所有的电路都遵循一些基本电路定律。

  - \-{zh-hans:[基尔霍夫电流定律](../Page/基尔霍夫电流定律.md "wikilink");
    zh-hant:[克希荷夫電流定律](../Page/克希荷夫電流定律.md "wikilink");}-：流入一个节点的电流总和，等于流出节点的电流总和。
  - \-{zh-hans:[基尔霍夫电压定律](../Page/基尔霍夫电压定律.md "wikilink");
    zh-hant:[克希荷夫電壓定律](../Page/克希荷夫電壓定律.md "wikilink");}-：环路电压的总和为零。
  - [欧姆定律](../Page/欧姆定律.md "wikilink")：线性元件（如[电阻](../Page/电阻.md "wikilink")）两端的电压，等于元件的阻值和流过元件的电流的乘积。

以下两条定理仅适用于线性电路

  - [诺顿定理](../Page/诺顿定理.md "wikilink")：任何由独立源，线性受控源与线性元件构成的两端网络，总可以等效为一个理想电流源与一个电阻的并联网络。
  - [戴维宁定理](../Page/戴维宁定理.md "wikilink")：任何由独立源，线性受控源与线性元件构成的两端网络，总可以等效为一个理想电压源与一个电阻的串联网络。

分析包含非线性器件的电路，则需要一些更复杂的定律。实际电路设计中，电路分析更多的通过计算机分析模擬来完成。

## 電路功率

  - 所有的電路在工作時，每一個元件或線路都會有[能量的工作運用](../Page/能量.md "wikilink")，即[電能運用](../Page/電能.md "wikilink")，而所有電路裡的[電能工作運用即稱為電路](../Page/電能.md "wikilink")[功率](../Page/功率.md "wikilink")。
  - 電路或電路元件的[功率定義為](../Page/功率.md "wikilink")：[功率](../Page/功率.md "wikilink")=[電壓](../Page/電壓.md "wikilink")\*[電流](../Page/電流.md "wikilink")（P=I\*U）。
  - 自然界裡能量不會消滅，固有一定律[能量不滅定律](../Page/能量不滅定律.md "wikilink")。
  - 電路總功率=電路功率+各電路元件功率。例如：電源（I\*U）=電路（I\*U）+ 各元件（I\*U）.
  - 在電路中的能量有時會變為[熱能或](../Page/熱能.md "wikilink")[輻射能等其他能量到空氣中](../Page/輻射.md "wikilink")，這就是電路或電路元件會發熱的原因，不會全部形成電能於電路中，根據能量不滅總能量=電能+熱能+輻射能+其他能量。

## 概括種類

  - [電源電路](../Page/電源電路.md "wikilink")：產生各種[電子電路的所需求](../Page/電子電路.md "wikilink")[電源](../Page/電源.md "wikilink")、如:[電池](../Page/電池.md "wikilink")、[直流電](../Page/直流電.md "wikilink")、[交流電](../Page/交流電.md "wikilink")、降電壓電路、升電壓電路。
  - [電子電路亦稱電氣迴路](../Page/電子電路.md "wikilink")。
  - [保護電路](../Page/保護電路.md "wikilink")：ESD、EMC、EMI。

## 頻率種類

  - [基頻電路](../Page/基頻電路.md "wikilink")，[基頻](../Page/基頻.md "wikilink")，低頻率，使用[基頻元件](../Page/基頻元件.md "wikilink")。
  - [高頻電路](../Page/高頻電路.md "wikilink")，[高頻](../Page/高頻.md "wikilink")，高頻率，使用[高頻元件](../Page/高頻元件.md "wikilink")。
  - [基頻、高頻混合電路](../Page/基頻、高頻混合電路.md "wikilink")

## 電路元件種類

  - [被動元件](../Page/被動元件.md "wikilink")：如[電阻](../Page/電阻.md "wikilink")、[電容](../Page/電容.md "wikilink")、[電感](../Page/電感.md "wikilink")、[二極體等](../Page/二極體.md "wikilink")，有分基頻被動元件、高頻被動元件。
  - [主動元件](../Page/主動元件.md "wikilink")：如[電晶體](../Page/電晶體.md "wikilink")、[微處理器](../Page/微處理器.md "wikilink")，[IC等有分基頻主動元件](../Page/IC.md "wikilink")、高頻主動元件。

## 用途種類

  - 微處理器電路：亦稱微控制器電路，形成[計算機](../Page/計算機.md "wikilink")、[遊戲機](../Page/遊戲機.md "wikilink")、[印表機](../Page/印表機.md "wikilink")、[投影機](../Page/投影機.md "wikilink")、([播放器影](../Page/播放器.md "wikilink")、音)、[監視器](../Page/監視器.md "wikilink")、各式各樣[家電](../Page/家電.md "wikilink")、[滑鼠](../Page/滑鼠.md "wikilink")、[鍵盤](../Page/鍵盤.md "wikilink")、[觸控等](../Page/觸控.md "wikilink")。
  - 電腦電路：為[微處理器電路進階電路](../Page/微處理器.md "wikilink")，形成[桌上型電腦](../Page/桌上型電腦.md "wikilink")、[筆記型電腦](../Page/筆記型電腦.md "wikilink")、[掌上型電腦](../Page/掌上型電腦.md "wikilink")、[工業電腦等各种](../Page/工業電腦.md "wikilink")[電腦](../Page/電腦.md "wikilink")。
  - 通訊電路：形成[電話](../Page/電話.md "wikilink")、[手機](../Page/手機.md "wikilink")、[對講機](../Page/對講機.md "wikilink")、[有線網路](../Page/有線網路.md "wikilink")、[有線傳送](../Page/有線傳送.md "wikilink")、[無線網路](../Page/無線網路.md "wikilink")、[無線傳送](../Page/無線傳送.md "wikilink")、[光通訊](../Page/光通訊.md "wikilink")、[紅外線](../Page/紅外線.md "wikilink")、[光纖](../Page/光纖.md "wikilink")、[微波通訊](../Page/微波通訊.md "wikilink")、[衛星通訊等](../Page/衛星通訊.md "wikilink")。
  - 顯示器電路：形成[螢幕](../Page/螢幕.md "wikilink")、[電視](../Page/電視.md "wikilink")、[儀表](../Page/儀表.md "wikilink")、[平面顯示器等各類](../Page/平面顯示器.md "wikilink")[顯示器](../Page/顯示器.md "wikilink")。
  - 光電電路：如[太陽能](../Page/太陽能.md "wikilink")、[電子顯微鏡](../Page/電子顯微鏡.md "wikilink")、電子[望遠鏡](../Page/望遠鏡.md "wikilink")、[相機](../Page/相機.md "wikilink")、[攝影機等各類電路](../Page/攝影機.md "wikilink")。
  - 電機電路：常運用於大電源設備、如電力設備、運輸設備、醫療設備、工業設備等。

## 電路拓樸

  - 電路是由各式光、電、磁零件，操控信號組合而成。
  - 設計各種電路的架構，為電路的[拓樸](../Page/拓樸.md "wikilink")。
  - 一般電路設計至積體元件（IC）即為IC電路拓樸。

## 参考文献

## 外部链接

## 参见

  - [电路图](../Page/电路图.md "wikilink")、[电阻](../Page/电阻.md "wikilink")、[阻容网络](../Page/阻容网络.md "wikilink")、[直流](../Page/直流電.md "wikilink")、[交流](../Page/交流電.md "wikilink")、[负载](../Page/负载.md "wikilink")、[電路學](../Page/電路學.md "wikilink")、[電子學](../Page/電子學.md "wikilink")、[電路板](../Page/電路板.md "wikilink")

{{-}}

[电路](../Category/电路.md "wikilink")
[Category:電子工程](../Category/電子工程.md "wikilink")
[Category:自動控制](../Category/自動控制.md "wikilink")
[Category:电力学](../Category/电力学.md "wikilink")