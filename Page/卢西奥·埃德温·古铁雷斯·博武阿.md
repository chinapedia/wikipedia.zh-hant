|                                                                                                          |
| :------------------------------------------------------------------------------------------------------: |
| [Lucio_Gutiérrez.jpg](https://zh.wikipedia.org/wiki/File:Lucio_Gutiérrez.jpg "fig:Lucio_Gutiérrez.jpg") |
|                                                  在任时间:                                                   |
|                                                   前任:                                                    |
|                                                   继任:                                                    |
|                                                   副总统:                                                   |
|                                                  出生日期:                                                   |
|                                                  出生地方:                                                   |
|                                                  去世日期:                                                   |
|                                                   夫人:                                                    |
|                                                   政党:                                                    |

<font size="+1">**卢西奥·古铁雷斯**</font>

**卢西奥·埃德温·古铁雷斯·博武阿**（**Lucio Edwin Gutiérrez
Borbúa**，）是前[厄瓜多爾總統](../Page/厄瓜多爾總統.md "wikilink")，[厄瓜多爾陆军理工大学毕业](../Page/厄瓜多爾.md "wikilink")，在陆军服役，获陆军上校军衔，2000年1月21日曾发动政变，但因失败入狱。后获国会特赦出狱。2002年11月当选总统，\[1\]2005年4月因干预国家司法被议会解职，流亡[巴西](../Page/巴西.md "wikilink")，10月14日回国被逮捕，\[2\]2006年獲釋。

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [Presidencia de la
    República](https://web.archive.org/web/20080506095458/http://www.presidencia.gov.ec/)
  - [厄瓜多尔前总统古铁雷斯](http://news.xinhuanet.com/ziliao/2002-11/26/content_641256.htm)
  - [Seeking asylum in
    Colombia](http://news.bbc.co.uk/2/hi/americas/4270854.stm)
  - [Arrest](http://news.bbc.co.uk/2/hi/americas/4341092.stm)

[G](../Category/1957年出生.md "wikilink")
[G](../Category/厄瓜多尔总统.md "wikilink")
[Category:考迪羅](../Category/考迪羅.md "wikilink")

1.  <http://news.bbc.co.uk/chinese/trad/hi/newsid_2500000/newsid_2509500/2509573.stm>
2.  <http://news.bbc.co.uk/chinese/trad/hi/newsid_4340000/newsid_4344300/4344340.stm>