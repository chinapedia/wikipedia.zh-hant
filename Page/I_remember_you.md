“**I remember
you**”是日本唱作女歌手[YUI的第六張單曲碟](../Page/YUI.md "wikilink")，於2006年9月20日推出。並且首次推出了附有DVD的初回限定版。

## 收錄歌曲

1.  **I remember you**
      -
        作詞・作曲：YUI　編曲：[northa+](../Page/northa+.md "wikilink")
    <!-- end list -->
      - 同樣是以 YUI
        參演的電影「[太陽之歌](../Page/太陽之歌.md "wikilink")」為主題，但以戀人[藤代孝治的角度而寫的歌曲](../Page/藤代孝治.md "wikilink")，另收錄於第二張專輯「[CAN'T
        BUY MY LOVE](../Page/CAN'T_BUY_MY_LOVE.md "wikilink")」。
2.  '''Cloudy '''
      -
        作詞・作曲：YUI　編曲：northa+
    <!-- end list -->
      - 在創作首張專輯「[FROM ME TO YOU](../Page/FROM_ME_TO_YOU.md "wikilink")」
        時未被收錄之歌曲。
3.  **Good-bye days ～YUI Acoustic Version～**
      -
        作詞・作曲：YUI　編曲：Akihisa Matsuura
    <!-- end list -->
      - 每張新單曲會收錄前一單曲的Acoustic Version。
4.  **I remember you ～Instrumental～**

## 銷售紀錄

  - 累積發售量：68781
  - [Oricon最高排名](../Page/Oricon.md "wikilink")：第2名
  - [Oricon上榜次數](../Page/Oricon.md "wikilink")：7次

| 發行         | 排行榜       | 最高位    | 總銷量 |
| ---------- | --------- | ------ | --- |
| 2006年9月20日 | Oricon 日榜 | 1      |     |
| Oricon 週榜  | 2         | 68,781 |     |

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:電影主題曲](../Category/電影主題曲.md "wikilink")
[Category:2006年單曲](../Category/2006年單曲.md "wikilink")