**縣道158號
海口－石榴班**，西起[雲林縣](../Page/雲林縣.md "wikilink")[臺西鄉海口](../Page/臺西鄉.md "wikilink")，東至雲林縣[斗南鎮文安](../Page/斗南鎮.md "wikilink")
，全長共計33.67公里（公路總局資料）。有兩條支線

## 行經行政區域

  - 全線均位於[雲林縣境內](../Page/雲林縣.md "wikilink")

<!-- end list -->

  - [臺西鄉](../Page/臺西鄉.md "wikilink")：海口0.0k（[TW_PHW17.svg](https://zh.wikipedia.org/wiki/File:TW_PHW17.svg "fig:TW_PHW17.svg")[台17線叉路](../Page/台17線.md "wikilink")）→臺西0.98k（[TW_CHW155.svg](https://zh.wikipedia.org/wiki/File:TW_CHW155.svg "fig:TW_CHW155.svg")[縣道155號叉路](../Page/縣道155號.md "wikilink")）
  - [東勢鄉](../Page/東勢鄉.md "wikilink")：東勢厝7.6k（[TW_CHW153.svg](https://zh.wikipedia.org/wiki/File:TW_CHW153.svg "fig:TW_CHW153.svg")[縣道153號叉路](../Page/縣道153號.md "wikilink")）
  - [褒忠鄉](../Page/褒忠鄉.md "wikilink")：褒忠14.2k→褒忠外環15.3k（[TW_PHW19.svg](https://zh.wikipedia.org/wiki/File:TW_PHW19.svg "fig:TW_PHW19.svg")[台19線叉路](../Page/台19線.md "wikilink")）
  - [土庫鎮](../Page/土庫鎮.md "wikilink")：馬光18.7k（[TW_CHW158a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158a.svg "fig:TW_CHW158a.svg")**縣道158甲線**叉路）
  - [虎尾鎮](../Page/虎尾鎮.md "wikilink")：廉使26.k→小東31.9k（[TW_CHW158b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158b.svg "fig:TW_CHW158b.svg")**縣道158乙線**叉路）
  - [斗南鎮](../Page/斗南鎮.md "wikilink")：斗南交流道32.8k（[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號](../Page/中山高速公路.md "wikilink")）→斗南文安32.956k（終點，[TW_PHW1.svg](https://zh.wikipedia.org/wiki/File:TW_PHW1.svg "fig:TW_PHW1.svg")[台1線叉路](../Page/台1線.md "wikilink")）

公路總局公路資訊終點為「石榴班」，但「石榴班」遠在[斗六市東北方](../Page/斗六市.md "wikilink")。

## 支線

### 甲線

[TW_CHW158a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158a.svg "fig:TW_CHW158a.svg")
**縣道158甲線
崙子頂－桶頭**，西起[雲林縣](../Page/雲林縣.md "wikilink")[臺西鄉崙子頂](../Page/臺西鄉.md "wikilink")，東至[南投縣](../Page/南投縣.md "wikilink")[竹山鎮桶頭](../Page/竹山鎮_\(台灣\).md "wikilink")，全長共計52.694公里，是目前[台灣第二最長的縣道支線](../Page/台灣.md "wikilink")。

  - [雲林縣](../Page/雲林縣.md "wikilink")

<!-- end list -->

  - [臺西鄉](../Page/臺西鄉.md "wikilink")：崙子頂0.0k（起點，[TW_PHW17.svg](https://zh.wikipedia.org/wiki/File:TW_PHW17.svg "fig:TW_PHW17.svg")[台17線岔路](../Page/台17線.md "wikilink")）
  - [東勢鄉](../Page/東勢鄉.md "wikilink")：阿坤厝[TW_CHW153.svg](https://zh.wikipedia.org/wiki/File:TW_CHW153.svg "fig:TW_CHW153.svg")[縣道153號岔路](../Page/縣道153號.md "wikilink")）
  - [褒忠鄉](../Page/褒忠鄉.md "wikilink")：龍岩（[TW_PHW19.svg](https://zh.wikipedia.org/wiki/File:TW_PHW19.svg "fig:TW_PHW19.svg")[台19線岔路](../Page/台19線.md "wikilink")）
  - [土庫鎮](../Page/土庫鎮.md "wikilink")：馬光15.047k（[TW_CHW158.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158.svg "fig:TW_CHW158.svg")**縣道158號**岔路）→土庫市區19.935k（與[TW_CHW145.svg](https://zh.wikipedia.org/wiki/File:TW_CHW145.svg "fig:TW_CHW145.svg")[縣道145號共線起點](../Page/縣道145號.md "wikilink")）
  - [虎尾鎮](../Page/虎尾鎮.md "wikilink")：下湳仔（與[TW_CHW145.svg](https://zh.wikipedia.org/wiki/File:TW_CHW145.svg "fig:TW_CHW145.svg")[縣道145號共線終點](../Page/縣道145號.md "wikilink")）
  - [斗南鎮](../Page/斗南鎮.md "wikilink")：斗南市區28.638k（與[TW_CHW158b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158b.svg "fig:TW_CHW158b.svg")**縣道158乙線**共線）
  - [古坑鄉](../Page/古坑鄉.md "wikilink")：古坑市區38.339k→古坑交流道南匝道（[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道3號](../Page/福爾摩沙高速公路.md "wikilink")）→土匪寮（與[TW_CHW149a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW149a.svg "fig:TW_CHW149a.svg")[縣道149甲共線起點](../Page/縣道149號.md "wikilink")）

<!-- end list -->

  - [南投縣](../Page/南投縣.md "wikilink")

<!-- end list -->

  - [竹山鎮](../Page/竹山鎮_\(台灣\).md "wikilink")：大埔42.439k→割菜園47.097k（[TW_CHW149a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW149a.svg "fig:TW_CHW149a.svg")[縣道149甲線共線終點](../Page/縣道149號.md "wikilink")）→桶頭52.694k（終點，[TW_CHW149.svg](https://zh.wikipedia.org/wiki/File:TW_CHW149.svg "fig:TW_CHW149.svg")[縣道149號岔路](../Page/縣道149號.md "wikilink")）

### 乙線

[TW_CHW158b.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158b.svg "fig:TW_CHW158b.svg")
**縣道158乙線
小東－永光**，西起[雲林縣](../Page/雲林縣.md "wikilink")[斗南鎮小東](../Page/斗南鎮.md "wikilink")，東至[雲林縣](../Page/雲林縣.md "wikilink")[古坑鄉永光](../Page/古坑鄉.md "wikilink")，全長共計11.508公里。

  - 全線均位於[雲林縣境內](../Page/雲林縣.md "wikilink")

<!-- end list -->

  - [斗南鎮](../Page/斗南鎮.md "wikilink")：小東0.0k（起點，接[TW_CHW158.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158.svg "fig:TW_CHW158.svg")**縣道158號**）→斗南市區（[TW_PHW1.svg](https://zh.wikipedia.org/wiki/File:TW_PHW1.svg "fig:TW_PHW1.svg")[台1線岔路](../Page/台1線.md "wikilink")）→福德陸橋（與[TW_CHW158a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW158a.svg "fig:TW_CHW158a.svg")**縣道158甲線**共線）
  - [古坑鄉](../Page/古坑鄉.md "wikilink")：永光11.508k（終點，接[TW_PHW3.svg](https://zh.wikipedia.org/wiki/File:TW_PHW3.svg "fig:TW_PHW3.svg")[台3線](../Page/台3線.md "wikilink")）

## 沿線風景

## 交流道

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號](../Page/中山高速公路.md "wikilink")[斗南交流道](../Page/斗南交流道.md "wikilink")
  - [TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道三號](../Page/福爾摩沙高速公路.md "wikilink")[古坑交流道](../Page/古坑交流道_\(國道3號\).md "wikilink")（南入北出，連接縣道158甲線）
  - [TW_PHW61.svg](https://zh.wikipedia.org/wiki/File:TW_PHW61.svg "fig:TW_PHW61.svg")[崙豐交流道](../Page/崙豐交流道.md "wikilink")（只設南出北入，連接縣道158甲線。）

## 參見

## 相關連結

[Category:雲林縣道路](../Category/雲林縣道路.md "wikilink")
[Category:台灣縣道](../Category/台灣縣道.md "wikilink")