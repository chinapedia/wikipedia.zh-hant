[Linux_Distribution_Timeline.svg](https://zh.wikipedia.org/wiki/File:Linux_Distribution_Timeline.svg "fig:Linux_Distribution_Timeline.svg")
**Linux发行版列表**，列舉[Linux发行版](../Page/Linux发行版.md "wikilink")，包括按[軟體包管理系統劃分](../Page/軟體包管理系統.md "wikilink")，以及按发行方式划分兩種列表。

## 按打包方式划分

### 基于Dpkg（Debian系）

Debian GNU /
Linux是一種強調使用自由軟件的发行版。它支持多種硬件平台。Debian及其衍生发行版使用[deb软件包格式](../Page/deb.md "wikilink")，并使用dpkg及其前端作为包管理器。

  - [Adamantix](../Page/Adamantix.md "wikilink")：基于[Debian](../Page/Debian.md "wikilink")，特别关注安全。
  - [Amber
    Linux](../Page/Amber_Linux.md "wikilink")：基于Debian，针对[拉脱维亚用户作了一些定制](../Page/拉脱维亚.md "wikilink")。
  - [ASLinux
    Desktop](../Page/ASLinux_Desktop.md "wikilink")：[西班牙语](../Page/西班牙语.md "wikilink")，基于Debian与[KDE](../Page/KDE.md "wikilink")，针对各种桌面用途，包括家用、办公、教育、游戏、科学、软件开发，最大的卖点在于其丰富的可用性。
  - [Anthon
    GNU/Linux](../Page/Anthon_GNU/Linux.md "wikilink")：即[安同OS](../Page/安同OS.md "wikilink")，是直接从源码构建的开源Linux操作系统，但采用[Dpkg包管理系统](../Page/Dpkg.md "wikilink")，遵循[LGPL授权协议](../Page/LGPL.md "wikilink")，使用[KDE桌面环境](../Page/KDE.md "wikilink")，由安同开源操作系统社区社区成员共同开发。
  - [B2D
    Linux](../Page/B2D_Linux.md "wikilink")：基於Debian，希望可以由“做中學”來產生一個小而美的中文Linux套件的計劃。
  - [Debian
    GNU/Linux](../Page/Debian_GNU/Linux.md "wikilink")：由大批社区志愿者收集的套件，拥有庞大的软件包可供选择（29000个以上），支持大量的硬件平台（12个[计算机系统结构](../Page/计算机系统结构.md "wikilink")）。Debian强调开源和自由。
  - [Deepin](../Page/Deepin.md "wikilink")：现基于Debian，使用自行开发的Deepin
    DE桌面环境的发行版，启动迅速，简洁美观，开发了深度文件管理器，深度音乐，深度截图，深度终端等特色软件，还与软件厂商合作开发了有道词典、网易云音乐等Linux原生应用。
  - [Grml](../Page/Grml.md "wikilink")：進行系統救援的[Live
    CD](../Page/Live_CD.md "wikilink")。
  - [Guadalinex](../Page/Guadalinex.md "wikilink")：由[西班牙的](../Page/西班牙.md "wikilink")[安达卢西亚地方政府推动](../Page/安达卢西亚.md "wikilink")，基于Debian，针对西班牙语的家庭用户以及学校。
  - [Knoppix](../Page/Knoppix.md "wikilink")：第一张Debian的自启动运行光盘。包含的软件非常多，启动时会自动进行硬件监测。从4.0起，用DVD作光盘。
      - 以下基于[Knoppix](../Page/Knoppix.md "wikilink")：
          - [Gnoppix](../Page/Gnoppix.md "wikilink")：[Knoppix的](../Page/Knoppix.md "wikilink")[GNOME版](../Page/GNOME.md "wikilink")，该套件发行周期较长，未来会跟[Ubuntu进行整合](../Page/Ubuntu.md "wikilink")。
          - [Kanotix](../Page/Kanotix.md "wikilink")：[自启动运行光盘](../Page/自启动运行光盘.md "wikilink")，基于[Knoppix](../Page/Knoppix.md "wikilink")，也可以安装到硬盘上。有很好的硬件支持，桌面与[笔记本电脑的整合也很出色](../Page/笔记本电脑.md "wikilink")。
          - [Kurumin](../Page/Kurumin.md "wikilink")：针对[巴西用户的Knoppix](../Page/巴西.md "wikilink")。
  - [LinEx](../Page/LinEx.md "wikilink")：由西班牙的[埃斯特雷马杜拉地方政府推动的套件](../Page/埃斯特雷马杜拉.md "wikilink")。
  - [Loco
    Linux](../Page/Loco_Linux.md "wikilink")：基于Debian的[阿根廷Linux](../Page/阿根廷.md "wikilink")。
  - [MEPIS](../Page/MEPIS.md "wikilink"): 基于Debian的桌面和伺服器。
  - [Rays
    Linux](../Page/Rays_Linux.md "wikilink")（华镭）：基于Debian，针对亚洲市场，由[新华科技（南京）系统软件有限公司](https://archive.is/20130101212505/http://www.sw-linux.com.cn/)开发。
  - [Skolelinux](../Page/Skolelinux.md "wikilink")：在[挪威发起](../Page/挪威.md "wikilink")，旨在打造适合于学校的轻便套件。
  - [Symphony
    OS](../Page/Symphony_OS.md "wikilink")：基于Debian，与众不同地采用[Mezzo桌面](../Page/Mezzo桌面.md "wikilink")。
  - [Ubuntu](../Page/Ubuntu.md "wikilink")：知名Linux发行版之一，由[Canonical有限公司赞助](../Page/Canonical有限公司.md "wikilink")，基于Debian，使用自己的軟體套件庫，与Debian的有所不同，旨在开发出更加友好的桌面。
      - 以下基于[Ubuntu](../Page/Ubuntu.md "wikilink")：
          - [Linux
            Mint](../Page/Linux_Mint.md "wikilink")：基於Ubuntu，人气与Ubuntu不相上下的發行版。
          - [Edubuntu](../Page/Edubuntu.md "wikilink")：是Ubuntu的教育发行版。
          - [Elementary
            OS](../Page/elementary_\(操作系统\).md "wikilink")：基於Ubuntu，使用基於[GNOME名為Pantheon的桌面環境](../Page/GNOME.md "wikilink")。
          - [Kubuntu](../Page/Kubuntu.md "wikilink")：使用[KDE桌面环境的Ubuntu套件](../Page/KDE.md "wikilink")。
          - [Lubuntu](../Page/Lubuntu.md "wikilink")：使用[LXDE桌面环境的Ubuntu套件](../Page/LXDE.md "wikilink")。
          - [PUD
            GNU/Linux](../Page/PUD_GNU/Linux.md "wikilink")：基於Ubuntu的小型Linux，可安裝於光碟或256
            MB以上的USB隨身碟。
          - [Ubuntu
            Kylin](../Page/Ubuntu_Kylin.md "wikilink")：添加了少量中国化定制的
            Ubuntu 发行版。
          - [Xubuntu](../Page/Xubuntu.md "wikilink")：使用[Xfce桌面环境的Ubuntu套件](../Page/Xfce.md "wikilink")。
          - [Start OS](../Page/Start_OS.md "wikilink")：原名Ylmf
            OS，基于Ubuntu发行版，已停止维护。
          - [Ubuntu
            GNOME](../Page/Ubuntu_GNOME.md "wikilink")：基於Ubuntu是Linux發行版，但使用的是桌面環境是[Gnome](../Page/Gnome.md "wikilink")。
          - [Zorin
            OS](../Page/Zorin_OS.md "wikilink")：基于Ubuntu的Linux发行版，目的是尽可能的模拟[Windows及操作习惯](../Page/Windows.md "wikilink")。

### 基于RPM（Red Hat系）

[Red Hat Linux和](../Page/Red_Hat_Linux.md "wikilink")[SUSE
Linux是最早使用RPM格式软件包的发行版](../Page/SUSE_Linux.md "wikilink")，如今RPM格式已广泛运用于众多的发行版。这两种发行版后来都分为商业版本和社区支持版本。Red
Hat Linux的社区支持版本现称为[Fedora](../Page/Fedora.md "wikilink")，商業版本则称为[Red
Hat Enterprise Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")。

  - [aLinux](../Page/aLinux.md "wikilink")：原名Peanut Linux，针对家庭用户。
  - [ALT
    Linux](../Page/ALT_Linux.md "wikilink")：[东欧版本](../Page/东欧.md "wikilink")。
  - [Ark Linux](../Page/Ark_Linux.md "wikilink")：强调易学易用。
  - [ASPLinux](../Page/ASPLinux.md "wikilink")：提供俄语等东欧语言的支持。
  - [Asianux
    Server](../Page/Asianux_Server.md "wikilink")：由中国红旗、日本Miracle、韩国Hannsoft三家联合开发，主要市场针对亚洲地区，对中文、日文、韩文的支持比较好。
  - [Blag Linux](../Page/Blag_Linux.md "wikilink")：体积小，但功能较多。
  - [Caixa
    Mágica](../Page/Caixa_Mágica.md "wikilink")：[葡萄牙语的Linux](../Page/葡萄牙语.md "wikilink")。
  - [cAos Linux](../Page/cAos_Linux.md "wikilink")：由社群创建的套件，功能通用、培植简单。
  - [CentOS](../Page/CentOS.md "wikilink")：由社群支持的套件，旨在100%地与[Red Hat
    Linux企业版兼容](../Page/Red_Hat_Linux.md "wikilink")，但不包含Red Hat的商业软件。
  - [Cobind](../Page/Cobind.md "wikilink")：桌面。
  - [Conectiva](../Page/Conectiva.md "wikilink")：一个[巴西套件](../Page/巴西.md "wikilink")，曾经是[United
    Linux的创建成员](../Page/United_Linux.md "wikilink")，现在该公司已经并入到[Mandriva
    Linux](../Page/Mandriva_Linux.md "wikilink")。
  - [EduLinux](../Page/EduLinux.md "wikilink")：用于教育的套件。
  - [Fedora](../Page/Fedora.md "wikilink")：可用作工作站、桌面以及服务器，由[红帽公司及其社群开发](../Page/红帽.md "wikilink")。
  - [Linux Mobile
    System](../Page/Linux_Mobile_System.md "wikilink")：基于Fedora
    Core的套件，设计成从USB存储设备启动，比如U盘。
  - [Linpus
    Linux](../Page/Linpus_Linux.md "wikilink")：來自台灣廠商發行的Linux版本。是一套通過LSB
    3.1認證、GB18030-2000編碼檢驗測試及支援CNS11643中文標準交換碼全字庫的Linux桌上型系統。在中文支援能力上較為完善。
  - [Magic
    Linux](../Page/Magic_Linux.md "wikilink")：一个易用的中文套件，基于[Fedora和](../Page/Fedora.md "wikilink")[KDE桌面环境](../Page/KDE.md "wikilink")。
  - [Mandriva
    Linux](../Page/Mandriva_Linux.md "wikilink")：最初是红帽的一个变种，针对[奔腾级](../Page/奔腾.md "wikilink")[CPU作了优化](../Page/CPU.md "wikilink")，后来在保持兼容性的同时，衍生成为更友好的套件。Mandriva中所有的软件仍然免费，还有活跃的社区支持，另外通过注册以及销售盒装产品，Mandriva还提供企业级的支持与服务，还有针对付费用户的俱乐部。
  - [Novell Linux
    Desktop](../Page/Novell_Linux_Desktop.md "wikilink")：由于[Novell收购了](../Page/Novell.md "wikilink")[SUSE](../Page/SUSE.md "wikilink")，他们的Linux产品对原来的套件有所继承。
  - [PCLinuxOS](../Page/PCLinuxOS.md "wikilink")：一个易用的自启动运行光盘，以良好的观感著称；硬盘安装也同样轻而易举。最初基于[Mandrake](../Page/Mandrake.md "wikilink")
    9.2，而后PCLinuxOS针对桌面用户，开始自己的开发道路。在保留基于RPM套件的同时，PCLinuxOS别出心裁地使用自己的[APT包管理工具](../Page/APT.md "wikilink")（受Debian影响），但图形前端仍然用的是[Synaptic](../Page/Synaptic.md "wikilink")。
  - [PCQLinux2004](../Page/PCQLinux2004.md "wikilink")：由[印度的](../Page/印度.md "wikilink")[PCQuest杂志生产](../Page/PCQuest.md "wikilink")，基于[Fedora
    Core](../Page/Fedora_Core.md "wikilink")。
  - [PLD
    Linux](../Page/PLD_Linux.md "wikilink")：来自[波兰的套件](../Page/波兰.md "wikilink")，针对较高级别的用户，比[Slackware](../Page/Slackware.md "wikilink")、[Gentoo更加易用](../Page/Gentoo.md "wikilink")。
  - [QiLinux](../Page/QiLinux.md "wikilink")：[意大利生产](../Page/意大利.md "wikilink")，包括桌面版、光盘自启动版，还有服务器版、高级服务器版。
  - [Qomo
    Linux](../Page/Qomo_Linux.md "wikilink")：以Linux人社区作为依托开发，目标是提供一款最新、最酷、最快，轻量级、模块化的Linux操作系统。
  - [Red Flag
    Linux](../Page/Red_Flag_Linux.md "wikilink")：即[红旗Linux](../Page/紅旗Linux.md "wikilink")，由北京中科红旗软件技术有限公司开发，主要针对中国市场。
  - [Red Hat Enterprise
    Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")：红帽Linux家族中唯一的商业分支。
  - [Scientific
    Linux](../Page/Scientific_Linux.md "wikilink")：基于红帽Linux企业版，遵循[GPL的软件重新](../Page/GPL.md "wikilink")[编译而成](../Page/编译.md "wikilink")。
  - [SUSE](../Page/SUSE.md "wikilink")／[openSUSE](../Page/openSUSE.md "wikilink")：来自德国，是欧洲最流行的套件之一。跟红帽一样，也包括大量的软件，需要7张以上的[CD](../Page/CD.md "wikilink")，现在则用双[DVD](../Page/DVD.md "wikilink")。这个套件有独特的配置工具[YaST](../Page/YaST.md "wikilink")。也是[United
    Linux的创立者之一](../Page/United_Linux.md "wikilink")，已经被[Novell公司收购](../Page/Novell.md "wikilink")。openSUSE是一个新的版本，基于社区，完全[开源](../Page/开源.md "wikilink")。
  - [Tinfoil Hat
    Linux](../Page/Tinfoil_Hat_Linux.md "wikilink")：对安全格外关注的套件。
  - [Trustix](../Page/Trustix.md "wikilink")：专注于安全与稳定性的套件。
  - [Turbo
    Linux](../Page/TurboLinux.md "wikilink")：在[亚洲较流行的一个套件](../Page/亚洲.md "wikilink")，基于Red
    Hat，是United Linux的成员。
  - [Vine Linux](../Page/Vine_Linux.md "wikilink")：基于Red
    Hat的一个[日本套件](../Page/日本.md "wikilink")。
  - [White Box Enterprise
    Linux](../Page/White_Box_Enterprise_Linux.md "wikilink")：意在兼容Red
    Hat企业版第三版。
  - [Yellow Dog](../Page/Yellow_Dog.md "wikilink")：基于Red
    Hat，针对[PowerPC平台](../Page/PowerPC.md "wikilink")。
  - [YOPER](../Page/YOPER.md "wikilink")："Your Operating
    System"（你的操作系统），来自[新西兰的桌面套件](../Page/新西兰.md "wikilink")。

### Slackware系

Slackware走了一條同其他的發行版本（Red Hat、Debian、Gentoo、SuSE、
Mandriva、Ubuntu等）不同的道路，它力圖成為「UNIX風格」的Linux發行版本。它的方針是只吸收穩定版本的應用程序，並且缺少其他Linux版本中那些為發行版本定製的配置工具。

  - [Slackware](../Page/Slackware.md "wikilink")：一个老牌套件，由维护，特别注重简洁与安全。
  - [Kate
    OS](../Page/Kate_OS.md "wikilink")：基于[Slackware的设计理念](../Page/Slackware.md "wikilink")，一个轻便的[波兰语套件](../Page/波兰语.md "wikilink")。
  - [Zenwalk
    Linux](../Page/Zenwalk_Linux.md "wikilink")（以前是MiniSlack）：基于Slackware作了优化，注重简便、快捷。
  - [Plamo
    Linux](../Page/Plamo_Linux.md "wikilink")：基于Slackware的[日语套件](../Page/日语.md "wikilink")。
  - [Ultima
    Linux](../Page/Ultima_Linux.md "wikilink")：基于Slackware，由[Martin
    Ultima作了优化](../Page/Martin_Ultima.md "wikilink")。
  - [SLAX](../Page/SLAX.md "wikilink")：一个基于Slackware的自启动运行光盘，由[Tomas
    Matejicek维护](../Page/Tomas_Matejicek.md "wikilink")。
  - [Frugalware](../Page/Frugalware.md "wikilink")：通用Linux套件，面向中级用户。

### 其他打包方式的套件

  - [ArchLinux](../Page/ArchLinux.md "wikilink")：基于[KISS原则](../Page/KISS原则.md "wikilink")，针对[x86-64的](../Page/x86-64.md "wikilink")[CPU做了优化](../Page/CPU.md "wikilink")，以.pkg.tar.xz格式打包并由包管理器进行跟踪维护，特别适合动手能力强的Linux用户。
  - [Chakra
    GNU/Linux](../Page/Chakra_GNU/Linux.md "wikilink")：原先基於[ArchLinux](../Page/ArchLinux.md "wikilink")，後來獨立成有自己特色的發行版，如：官方套件庫不含依賴[GTK+的軟體包](../Page/GTK+.md "wikilink")、只使用[KDE桌面環境等](../Page/KDE.md "wikilink")。
  - [Calculate
    Linux](../Page/Calculate_Linux.md "wikilink")：基于[Gentoo](../Page/Gentoo.md "wikilink")，来自[俄罗斯](../Page/俄罗斯.md "wikilink")。
  - [CRUX](../Page/CRUX.md "wikilink")：採用類BSD
    Port包管理系统，針對[i686的](../Page/i686.md "wikilink")[CPU做了最佳化](../Page/CPU.md "wikilink")，適合狂熱愛好者以及專業人士使用。
  - [Foresight
    Linux](../Page/Foresight_Linux.md "wikilink")：采用[Conary包管理系统](../Page/Conary.md "wikilink")，引入了[GNOME中的许多最新技术](../Page/GNOME.md "wikilink")，比如[beagle](../Page/beagle.md "wikilink")、[f-spot](../Page/f-spot.md "wikilink")、[howl以及最新的](../Page/howl.md "wikilink")[hal等](../Page/hal.md "wikilink")，这个套件在保持易用的同时，更注重革新。
  - [Gentoo](../Page/Gentoo.md "wikilink")：这个套件采用自己独特的[Portage包管理系统](../Page/Portage.md "wikilink")，吸引了许多狂热爱好者以及专业人士，由於能自己编译及調整源码依賴等選項，而獲得至高的自訂性及優化的軟體，在源码包也有相當多新舊版本的選擇，是個強調能自由選擇的發行版。
  - [GoboLinux](../Page/GoboLinux.md "wikilink")：构建了新的[目录结构](../Page/目录结构.md "wikilink")，比如[GCC放在](../Page/GCC.md "wikilink")`/Programs/GCC/`这样的目录，为了让系统能找到这些文件，在`/System/Links/Executables`这样的目录下归组，这样就包含了`/Programs`目录下所有可执行文件的[符号链接](../Page/符号链接.md "wikilink")。
  - [Heretix](../Page/Heretix.md "wikilink")：以前叫做RubyX，套件的管理，包括包管理，都是通过由[Ruby写的脚本来完成](../Page/Ruby.md "wikilink")，所有的包都安装在`/pkg`目录下。
  - [ImpiLinux](../Page/ImpiLinux.md "wikilink")：来自[南非的套件](../Page/南非.md "wikilink")，主要针对[非洲用户](../Page/非洲.md "wikilink")。
  - [Jedi
    GNU/Linux](../Page/Jedi_GNU/Linux.md "wikilink")：使用[force-get包管理器](../Page/force-get.md "wikilink")，允许源码、二进制软件包共存。
  - [Linux From
    Scratch](../Page/Linux_From_Scratch.md "wikilink")：这是一份文档，介绍如何自己动手，如何白手起家编译打造自己独一无二的Linux系统。
  - [Lunar Linux](../Page/Lunar_Linux.md "wikilink")，基于源码，由[Sorcerer
    GNU/Linux所衍生](../Page/Sorcerer_GNU/Linux.md "wikilink")。
  - [MkLinux](../Page/MkLinux.md "wikilink")："Microkernel
    Linux"的缩写，旨在将Linux移植到跑[Mach微核的](../Page/Mach微核.md "wikilink")[PowerPC机器上](../Page/PowerPC.md "wikilink")。
  - [Onebase
    Linux](../Page/Onebase_Linux.md "wikilink")：采用[OLM包管理器](../Page/OLM.md "wikilink")，对二进制、源码进行管理。
  - [Sabayon
    Linux](../Page/Sabayon_Linux.md "wikilink")：基于[Gentoo](../Page/Gentoo.md "wikilink")，来自[意大利](../Page/意大利.md "wikilink")。
  - [Sorcerer GNU/Linux](../Page/Sorcerer_GNU/Linux.md "wikilink")：基于源码。
  - [Source Mage
    GNU/Linux](../Page/Source_Mage_GNU/Linux.md "wikilink")：也是基于源码，由Sorcerer
    GNU/Linux所衍生。
  - [Ututo](../Page/Ututo.md "wikilink")：基于[Gentoo](../Page/Gentoo.md "wikilink")，来自[阿根廷](../Page/阿根廷.md "wikilink")。
  - [Open Client](../Page/Open_Client.md "wikilink")：基于[Red Hat
    Enterprise
    Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")，来自[IBM](../Page/IBM.md "wikilink")，提供有Fedora、Ubuntu、SLED的Layer。

### 给老机器订制的小型套件

一般的迷你套件，除了可以硬盘安装，也可以安装在隨身碟上。

  - [Austrumi](../Page/Austrumi.md "wikilink")：基于[Slackware](../Page/Slackware.md "wikilink")，来自拉脱维亚的自启动CD套件，支持英语，功能比较丰富。
  - [cAos Linux](../Page/cAos_Linux.md "wikilink")：有社区维护，功能通用。
  - [Damn Small
    Linux](../Page/Damn_Small_Linux.md "wikilink")（DSL）：这是小型套件的老祖宗，放在迷你CD上，原先设计是想看看一张50M的CD可以放多少桌面程序，原来是作为个人玩具，但不久Damn
    Small
    Linux周围就聚成了一个社区，不少人加入进来，参与改进，包括一个远程、本地的程序安装系统，多功能的备份、还原系统，另外还加入了[Knoppix的硬件检测](../Page/Knoppix.md "wikilink")，使用自己的\*.dsl软件包系统，默认的窗口管理器是[Fluxbox](../Page/Fluxbox.md "wikilink")。可以在微软虚拟机软件里运行，无须关闭WINDOWS系统专门进入LINUX系统，特别适合初学者。
  - [Feather Linux](../Page/Feather_Linux.md "wikilink")：类似于[Damn Small
    Linux](../Page/Damn_Small_Linux.md "wikilink")，但总容量是115MB，兼容Debian的软件包。
  - [Flonix
    USB版](../Page/Flonix_USB版.md "wikilink")：放在USB设备上的可移动桌面套件，这是个商业版，只能给购买的U盘作预装。
  - [Knopperdisk](../Page/Knopperdisk.md "wikilink")：为U盘设计的套件，基于[Gentoo](../Page/Gentoo.md "wikilink")。
  - [Puppy
    Linux](../Page/Puppy_Linux.md "wikilink")：启动特别地快，在配置较低的PC上（[内存小于](../Page/内存.md "wikilink")48M），也可以运行自如。包含的工具都是特别精简的，使用[Fvwm95作为窗口管理器](../Page/Fvwm95.md "wikilink")（现在是[JWM](../Page/JWM.md "wikilink")）。
  - [Kuppy
    Linux](../Page/Kuppy_Linux.md "wikilink")：基于Puppy发展的发行版，拥有启动特别地快，在配置较低的PC上等特点外，将桌面置换为XFCE4，更适合习惯Windows操作的用户。
  - [Tiny Core
    Linux](../Page/Tiny_Core_Linux.md "wikilink")：是一个仅有10MB的桌面操作系统，甚至可以在486上运行，但它并不是一个完整的桌面，而且只支持有线局域网。
  - [Stem桌面](../Page/Stem桌面.md "wikilink")：一个混合产品，使用标准的[Debian来编译桌面](../Page/Debian.md "wikilink")（[Fvwm95](../Page/Fvwm95.md "wikilink")）。针对老机器（[CPU小于等于](../Page/CPU.md "wikilink")266Mhz，[内存小于等于](../Page/内存.md "wikilink")64M）设计，从Debian软件仓库种选择的包都是最轻巧的。与众不同的是，该套件没有自己的安装光盘，用户得先安装Debian，然后运行文本界面的安装脚本，然后通过网络连接，编译剩余的软件。这个套件100%跟Debian兼容。
  - [SPBLinux](../Page/SPBLinux.md "wikilink")：用于[软盘](../Page/软盘.md "wikilink")、[U盘的迷你版本](../Page/U盘.md "wikilink")。
  - [Vector
    Linux](../Page/Vector_Linux.md "wikilink")：中小型套件，针对新老机器，采用小而快的应用程序，以及简化的安装程序。该套件有多个版本，包括大小为2G的[SOHO](../Page/SOHO.md "wikilink")（Small
    Office, Home
    Office）版，以及800M的"Dynamite"版。SOHO版提供[KDE](../Page/KDE.md "wikilink")／[IceWM窗口管理器](../Page/IceWM.md "wikilink")，外观很专业；Dynamite版只采用IceWM，以及部分工具。该套件包括了简单的升级包管理程序，基于[Slackware](../Page/Slackware.md "wikilink")。
  - [eMoviX](../Page/eMoviX.md "wikilink")：小型套件，專門用作媒體播放用途。

## 按发行方式划分

### 部分或全部的商业版

  - [BlueCat
    Linux](../Page/BlueCat_Linux.md "wikilink")：Linux嵌入式系统，可用于小型客户定制的设备，乃至大规模多[CPU的系统](../Page/CPU.md "wikilink")。
  - [Libranet](../Page/Libranet.md "wikilink")：基于[Debian的桌面套件](../Page/Debian.md "wikilink")，与Debian保持100%的兼容。安装过程有硬件自动检测，桌面有一个管理员菜单（文字模式与图形模式），这样简化了硬件与软件的配置。
  - [Linspire](../Page/Linspire.md "wikilink")：另一个桌面套件，以前叫Lindows，基于Debian。可以通过Linspire或者Debian的apt命令，添加额外的软件，但不保证跟Debian的兼容性。该套件包含不少的[专属软件](../Page/专属软件.md "wikilink")。
  - [Mandriva
    Linux](../Page/Mandriva_Linux.md "wikilink")：[Mandrakesoft跟](../Page/Mandrakesoft.md "wikilink")[Conectiva合并以后](../Page/Conectiva.md "wikilink")，更名为[Mandriva
    Linux](../Page/Mandriva_Linux.md "wikilink")。对于[菜鸟来说](../Page/菜鸟.md "wikilink")，该套件的各种产品，包括服务器、工作站、小型商用以及个人版，是最容易维护的Linux套件之一。原来是[Red
    Hat的一个变种](../Page/Red_Hat.md "wikilink")，针对[奔腾级](../Page/奔腾.md "wikilink")[CPU作了优化](../Page/CPU.md "wikilink")，进而发展出了更加方便的套件。在保留完全自由软件的同时，Mandriva商业模式，通过捐献，以及对非会员延迟发布政策，增加了企业级的支持与服务。
  - [MEPIS](../Page/MEPIS.md "wikilink")：一个基于Debian的套件，也可以作为自启动光盘来运行，这样可以在决定硬盘安装前，尝尝鲜。
  - [Nitix](../Page/Nitix.md "wikilink")：第一个基于Linux服务器的自治操作系统，具有自管理、自恢复、自配置和自优化的能力。
  - [Novell Linux
    桌面](../Page/Novell_Linux_桌面.md "wikilink")：[Novell收购](../Page/Novell.md "wikilink")[SUSE后](../Page/SUSE.md "wikilink")，两家的套件就互相融合了。
  - [Progeny
    Debian](../Page/Progeny_Debian.md "wikilink")：由[Progeny开发](../Page/Progeny.md "wikilink")，基于[Debian](../Page/Debian.md "wikilink")，使用从[Red
    Hat移植过来的](../Page/Red_Hat.md "wikilink")[Anaconda安装器](../Page/Anaconda.md "wikilink")，该套件又称为Progeny
    Componentized Linux。
  - [Red Hat Enterprise
    Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")：从Red Hat
    Linux衍生出的纯商业版。
  - [SUSE](../Page/SUSE.md "wikilink")：基于[德国的](../Page/德国.md "wikilink")[纽伦堡](../Page/纽伦堡.md "wikilink")，以前叫SuSE，是[欧洲最流行的Linux套件](../Page/欧洲.md "wikilink")，由自己独特的配置工具[YaST](../Page/YaST.md "wikilink")，用户可以[下载](http://www.novell.com/products/linuxprofessional/downloads/suse_linux/index.html)到体验版（跟专业版类似）。该公司是[United
    Linux的创建者](../Page/United_Linux.md "wikilink")，被[Novell收购](../Page/Novell.md "wikilink")。
  - [Xandros](../Page/Xandros.md "wikilink")：来自[加拿大](../Page/加拿大.md "wikilink")，基于过去的[Corel
    Linux](../Page/Corel_Linux.md "wikilink")，专注于桌面市场，以及跟[Windows的兼容性](../Page/Windows.md "wikilink")。该套件包含一些[专属软件](../Page/专属软件.md "wikilink")，跟Debian的[dpkg包管理系统兼容](../Page/dpkg.md "wikilink")。
  - [YES Linux](../Page/YES_Linux.md "wikilink")：针对小型公司，提供建议的网络环境。

### 专用套件

  - [Mobilinux](../Page/Mobilinux.md "wikilink")：针对手机，由[MontaVista出品](../Page/MontaVista.md "wikilink")。
  - [Android](../Page/Android.md "wikilink")：来自[Google](../Page/Google.md "wikilink")，以apk格式打包，采用[Android
    Runtime虚拟机提供类似](../Page/Android_Runtime.md "wikilink")[Java
    (编程语言)的](../Page/Java_\(编程语言\).md "wikilink")[应用程序接口](../Page/应用程序接口.md "wikilink")，目前已成为使用者最多的智能手机系统之一。
  - [Maemo](../Page/Maemo.md "wikilink")：来自[诺基亚](../Page/诺基亚.md "wikilink")，基于[Debian](../Page/Debian.md "wikilink")，目前专用于诺基亚的[N770](../Page/诺基亚770网络终端.md "wikilink")、[N800](../Page/诺基亚_N800.md "wikilink")、[N810网络终端](../Page/N810.md "wikilink")，以及[N900手机](../Page/N900.md "wikilink")。
  - [Moblin](../Page/Moblin_project.md "wikilink")︰来自[英特尔](../Page/英特尔.md "wikilink")，现已捐给[Linux基金会](../Page/Linux基金会.md "wikilink")，用于[Eee
    PC或其他上网本](../Page/Eee_PC.md "wikilink")，支持快速启动。
  - [MeeGo](../Page/MeeGo.md "wikilink")：[Maemo与](../Page/Maemo.md "wikilink")[Moblin计划结合的产物](../Page/Moblin.md "wikilink")。
  - [Bada](../Page/Bada_\(操作系统\).md "wikilink")：针对手机，由[三星電子出品](../Page/三星電子.md "wikilink")。
  - [LiMo
    平台](../Page/LiMo_平台.md "wikilink")：由[LiMo基金會主导开发的](../Page/LiMo基金會.md "wikilink")[移动电话发行版](../Page/移动电话.md "wikilink")。
  - [Tizen](../Page/Tizen.md "wikilink")：[MeeGo](../Page/MeeGo.md "wikilink")、[LiMo
    平台与](../Page/LiMo_平台.md "wikilink")[Bada计划结合的产物](../Page/Bada_\(操作系统\).md "wikilink")。
  - [Meltemi](../Page/Meltemi.md "wikilink")：[MeeGo的另一个后继者](../Page/MeeGo.md "wikilink")，面向低端[智能手机](../Page/智能手机.md "wikilink")。
  - [webOS](../Page/webOS.md "wikilink")：针对手机和平板电脑，由[Palm公司出品](../Page/Palm公司.md "wikilink")，后被[惠普公司开源](../Page/惠普公司.md "wikilink")，最终被[LG收购](../Page/LG集团.md "wikilink")。

### 其它平台

iPodLinux基于修改版[uClinux内核](../Page/uClinux.md "wikilink")，并编写有所谓的“podzilla”简单用户界面。目前仅支持第四代之前的[iPod](../Page/iPod.md "wikilink")。

## 选择合适的Linux发行版

[Zegenie
Studios](https://web.archive.org/web/20090523090806/http://www.zegeniestudios.net/ldc/)提供了[Linux发行版选择器](../Page/Linux发行版选择器.md "wikilink")，通过完成一个不超过十六题的问卷测试，你就可以选择合适的Linux发行版了。

## 参见

  - [Linux发行版](../Page/Linux发行版.md "wikilink")
  - [Linux套件比較](../Page/Linux套件比較.md "wikilink")

## 外部链接

  - [DistroWatch - Linux套件的最新发展。](http://www.distrowatch.com/)
  - [DistroList -
    Linux/BSD发行版资讯](https://web.archive.org/web/20100429221927/http://www.distrolist.com/)
  - [Linux发行版选择器（Linux Distribution
    Chooser）](https://web.archive.org/web/20070508235601/http://www.zegeniestudios.net/ldc/)
  - [Linux发行版选择器（Linux Distribution
    Chooser）用户指南及中文翻译。](http://lothlorien.du.googlepages.com/home)
  - [OSMSG -
    Linux软件最新资讯](https://web.archive.org/web/20120409180241/http://osmsg.com/)

[\*](../Category/Linux發行版.md "wikilink")
[Category:软件列表](../Category/软件列表.md "wikilink")