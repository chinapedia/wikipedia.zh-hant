**桐柏县**在[中国](../Page/中华人民共和国.md "wikilink")[河南省南部](../Page/河南省.md "wikilink")，是[南阳市下属的一个县](../Page/南阳市.md "wikilink")\[1\]。為宛西咽喉，中共革命老區，風景雄奇秀美。

## 地理

面积1941平方公里，人口約42.4万人。

## 历史沿革

[汉朝时为复阳县](../Page/汉朝.md "wikilink")，[南朝梁置](../Page/南朝梁.md "wikilink")[淮安县](../Page/淮安县_\(南梁\).md "wikilink")。[隋朝](../Page/隋朝.md "wikilink")[开皇初年](../Page/开皇.md "wikilink")，郡废，更名县曰桐柏，以境内的[桐柏山为名](../Page/桐柏山.md "wikilink")。属[淮安郡](../Page/淮安郡_\(隋朝\).md "wikilink")。

## 地理

境內有桐柏山，属[秦岭](../Page/秦岭.md "wikilink")[大别山系](../Page/大别山.md "wikilink")。

位于[桐柏山](../Page/桐柏山.md "wikilink")，是古“[四渎](../Page/四渎.md "wikilink")”之一[淮河的发源地和江淮两大水系的天然分界线](../Page/淮河.md "wikilink")。

## 资源

該縣盛產[金礦和](../Page/金礦.md "wikilink")[碱矿](../Page/碱矿.md "wikilink")。之前有亚洲第一的[吴城碱矿](../Page/吴城碱矿.md "wikilink")，现在有世界第一的安棚碱矿。

## 行政区划

桐柏县下辖9镇7乡(214个行政村)：黄岗乡、新集乡、城郊乡、朱庄乡、程湾乡、回龙乡、安棚镇、淮源镇、月河镇、毛集镇、城关镇、平氏镇、固县镇、吴城镇、大河镇、埠江镇。

## 名胜

有豫南第一高峰海拔1140米的桐柏山主峰[太白顶](../Page/太白顶.md "wikilink")，享誉海内外的[佛教禅宗白云系祖庭](../Page/佛教禅宗.md "wikilink")[云台禅寺](../Page/云台禅寺.md "wikilink")，誉为天下“三十六洞天”之一的[水帘洞](../Page/水帘洞.md "wikilink")。

[淮河的源头就在桐柏县淮源镇](../Page/淮河.md "wikilink")[淮渎祠](../Page/淮渎祠.md "wikilink")，有美丽的[桐柏山淮源风景名胜区](../Page/桐柏山淮源风景名胜区.md "wikilink")，当地很多人都喜欢去祖师顶烧香还愿。

[桐柏山淮源风景名胜区位于豫南鄂北交界的桐柏山脉北麓中段](../Page/桐柏山淮源风景名胜区.md "wikilink")，景区总面积266平方公里，核心区为108平方公里，距桐柏县城3公里，内分[淮源](../Page/淮源.md "wikilink")、[太白顶](../Page/太白顶.md "wikilink")、桃花洞、[水帘洞四大各具特色的景区](../Page/水帘洞.md "wikilink")，各类景观一百余处。

桐柏山淮源风景名胜区旅游文化内涵十分丰富，具有源远流长的[淮源文化](../Page/淮源文化.md "wikilink")，影响深远的佛道文化，历史悠久的[盘古文化和在国内革命战争时期具有重要地位的](../Page/盘古.md "wikilink")[苏区文化](../Page/苏区文化.md "wikilink")。景区具有完整的过渡带[森林生态系统和优良的自然生态环境](../Page/森林生态系统.md "wikilink")，被专家誉为中原地区天然生物[基因库和自然博物馆](../Page/基因库.md "wikilink")。

## 教育

  - [河南佛教学院](../Page/河南佛教学院.md "wikilink")

[桐柏县](../Category/桐柏县.md "wikilink")
[县](../Category/南阳区县市.md "wikilink")
[南阳](../Category/河南省县份.md "wikilink")
[豫](../Category/国家级贫困县.md "wikilink")

1.  位處[湖北省](../Page/湖北省.md "wikilink")[隨州市正北](../Page/隨州市.md "wikilink")，河南省[信陽市西北](../Page/信陽市.md "wikilink")。