[Loberty_Day_Commemorative_0.40Yuan_Stamp_in_taiwan.Jpg](https://zh.wikipedia.org/wiki/File:Loberty_Day_Commemorative_0.40Yuan_Stamp_in_taiwan.Jpg "fig:Loberty_Day_Commemorative_0.40Yuan_Stamp_in_taiwan.Jpg")於1955年所發行的「[反共義士](../Page/反共.md "wikilink")」[郵票](../Page/郵票.md "wikilink")\]\]
**世界自由日**，舊稱**一二三自由日**，定於1月23日，源於[中華民國紀念](../Page/中華民國.md "wikilink")[1954年](../Page/1954年.md "wikilink")[1月23日](../Page/1月23日.md "wikilink")[朝鲜战争反共戰俘獲釋](../Page/朝鲜战争战俘遣返问题.md "wikilink")、具有[反共意義的](../Page/反共.md "wikilink")[節慶](../Page/節慶.md "wikilink")，1993年改為現名，每年[中華民國](../Page/中華民國.md "wikilink")、世界各地反共人士以及國際上[反共陣營皆有一系列慶祝活動](../Page/反共.md "wikilink")。

## 背景

1950年6月，[朝鲜战争開始](../Page/朝鲜战争.md "wikilink")。10月，[中國人民志願軍](../Page/中國人民志願軍.md "wikilink")30多萬人參與韓戰，全線反撲，[聯合國軍被迫後撤](../Page/聯合國軍.md "wikilink")。\[1\]11月26日，志愿军參與朝鲜战争。12月，聯合國軍自[平壤敗退](../Page/平壤.md "wikilink")。\[2\]1951年10月，韓境停戰談判於[板門店舉行](../Page/板門店.md "wikilink")。\[3\]

中共方面一直堅持要求聯合國軍無條件遣返所有戰俘，無論他們是否願意回到中共統治下；中共立場被聯合國軍所拒絕。1952年2月，韓境聯軍總部堅持自由遣返戰俘原則，蓋聯軍在韓境所獲17萬餘戰俘中，有10萬人不願被遺返，尤其在2萬餘名華籍戰俘中，誓死不願遺返者達1萬6千名\[4\]。朝鲜战争期間联合国军俘獲戰俘及在陣前易帜之軍人共173,700人，其中中国人民志愿军有21,300人被俘，被俘人员中[连](../Page/连.md "wikilink")[排级](../Page/排.md "wikilink")[军官有](../Page/军官.md "wikilink")600人左右、[营级](../Page/营.md "wikilink")30餘人、[团级](../Page/团.md "wikilink")5人、[师级](../Page/师.md "wikilink")1人，主要被关押在[巨济岛等地](../Page/巨济岛.md "wikilink")。

1953年6月，韓國[釜山戰俘營](../Page/釜山.md "wikilink")14,235名華藉反共戰俘上書請求釋放並准其到[台灣參加](../Page/台灣.md "wikilink")[中華民國國軍行列](../Page/中華民國國軍.md "wikilink")\[5\]。最後雙方達成協議，戰俘可以自由決定自己的去向，戰俘的意向由[中立國](../Page/中立國.md "wikilink")[印度等國的代表負責鑑定](../Page/印度.md "wikilink")\[6\]。

中共官方認為，表面上看战俘由印度主持对战俘进行了甄别以确定其本人的意愿。實際上，聯合國軍停战谈判代表团首任团长乔埃提出「自願遣返」的方案，试图用“大量朝中战俘拒绝被遣返”的結果来宣扬“共产主义的失败”。志願軍戰俘中也有一部分人是于[第二次国共内战中被俘从而加入](../Page/第二次国共内战.md "wikilink")[中國人民解放軍的前中華民國國軍](../Page/中國人民解放軍.md "wikilink")（被称为「[解放战士](../Page/解放战士.md "wikilink")」），這一類戰俘中有不少人不願意在停戰後回到中國大陸。最終共有14,235名志願軍戰俘選擇前往台灣，而选择回大陆的战俘大多受到政治审查、被开除党/团籍或拒绝其入党\[7\]。组织干事韩子建后来提出重新入党（他在[济州岛是升](../Page/济州岛.md "wikilink")[中华人民共和国国旗的组织者之一](../Page/中华人民共和国国旗.md "wikilink")），被别人要求重新念一遍在归管处写下的交待材料去评定是否够格入党，他无话可说，之后就[自杀了](../Page/自杀.md "wikilink")\[8\]。

[中華民國政府及海內外](../Page/中華民國政府.md "wikilink")448個民眾團體為歡迎「[反共義士](../Page/反共義士.md "wikilink")」來歸，經過多次集會商討，事先向聯軍統帥呼籲，務須如期終止監管。1954年1月4日，聯合國軍統帥將軍，為「表明[人道與](../Page/人道.md "wikilink")[正義立場](../Page/正義.md "wikilink")」再鄭重聲明，請負責戰俘事宜的印度代表[蒂邁雅必須於](../Page/蒂邁雅.md "wikilink")1月24日午夜將全體反共戰俘無條件釋放。1月23日清晨7時20分起，14,850名中國反共戰俘、7,650名韓國反共戰俘，終於奔向南方，得到释放\[9\]。前往台灣的反共戰俘分成三批，於1954年1月23日由[基隆上岸抵台](../Page/基隆.md "wikilink")，接受全台灣民眾的熱烈歡迎，被称为「韓戰義士」。他們在[臺北市遊行](../Page/臺北市.md "wikilink")，市民張燈結綵表示慶祝。全台灣各大城市自由鐘，都敲響23下。並定1月23日為自由日，以紀念「反共義士」重獲自由。一面通電全世界民主國家，籲請一致響應。此後每年集會慶祝，並有全世界熱愛自由、主持正義人士來臺參加盛會。\[10\]

中華民國政府特設「一·二三自由日」以紀念這個「從中共手中解救大陸同胞」的事件；其後並在[泰國](../Page/泰國.md "wikilink")[曼谷舉行的](../Page/曼谷.md "wikilink")「第一屆[世界反共聯盟大會](../Page/世界反共聯盟.md "wikilink")」中通過。這即是一二三自由日的由來。這些反共戰俘來到台灣之後，都被編入中華民國國軍、並参加了诸如[八二三砲戰的](../Page/八二三砲戰.md "wikilink")[戰役](../Page/戰役.md "wikilink")，大多终老[眷村](../Page/眷村.md "wikilink")。

隨著[冷戰氣氛轉淡以及](../Page/冷戰.md "wikilink")[兩岸局勢的穩定](../Page/臺海現狀.md "wikilink")，中華民國政府終止[動員戡亂](../Page/動員戡亂.md "wikilink")，各[共產主義國家政權也於](../Page/共產主義國家.md "wikilink")20世紀末逐一崩解，此[紀念日於](../Page/紀念日.md "wikilink")1993年改名為**世界自由日**。后来有的台湾战俘经商致富后回大陆投资，受到当地政府热情接待，如战俘[张城垣就曾见到这种场面](../Page/张城垣.md "wikilink")\[11\]。

## 参考文献

## 參見

  - [朝鲜战争](../Page/朝鲜战争.md "wikilink")
  - [反共主义](../Page/反共主义.md "wikilink")
  - [反共義士](../Page/反共義士.md "wikilink")
  - [中国人民志愿军](../Page/中国人民志愿军.md "wikilink")

## 外部連結

  - [數位典藏聯合目錄，衝破鐵幕
    紀念一二三自由日專輯](http://catalog.digitalarchives.tw/item/00/3a/18/4e.html)

[Category:中華民國節日](../Category/中華民國節日.md "wikilink")
[Category:臺灣節日](../Category/臺灣節日.md "wikilink")
[Category:中國反共主義](../Category/中國反共主義.md "wikilink")
[Category:1月節日](../Category/1月節日.md "wikilink")
[Category:朝鲜战争战俘](../Category/朝鲜战争战俘.md "wikilink")
[Category:朝鲜战争纪念](../Category/朝鲜战争纪念.md "wikilink")

1.
2.
3.
4.

5.
6.
7.

8.

9.

10. [自由日的由來](http://gtes.tnc.edu.tw/updata/holiday_updata/0123.php)

11. 志愿军战俘纪事续集：生命只有一次 P24