*' 苏门答腊犀牛*' 简称 **苏门犀**（[学名](../Page/学名.md "wikilink")：*Dicerorhinus
sumatrensis*），**双角犀属**的[唯一一种](../Page/单属种.md "wikilink")，是现存最小的[犀牛](../Page/犀牛.md "wikilink")，肩高大约130厘米，体长约240-315厘米，体重约700[公斤](../Page/公斤.md "wikilink")。苏门犀长有两个突角，这一点与[非洲的](../Page/非洲.md "wikilink")[白犀和](../Page/白犀.md "wikilink")[黑犀相似](../Page/黑犀.md "wikilink")，而不同于亚洲的[印度犀和](../Page/印度犀.md "wikilink")[爪哇犀](../Page/爪哇犀.md "wikilink")。前角较大，通常为15-25厘米，後角较小，通常是少于10厘米。身披厚厚的红棕色的长毛。苏门犀是独居动物，仅在发情与扶养幼仔时相聚。它们是犀牛中最吵的，除了声音外，它们还会在泥中做记号或排便。

苏门犀是世界上最后生存的披毛犀牛，曾经广布于[印度](../Page/印度.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[孟加拉国](../Page/孟加拉国.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")、[老挝](../Page/老挝.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[马来西亚和](../Page/马来西亚.md "wikilink")[印度尼西亚的](../Page/印度尼西亚.md "wikilink")[热带雨林与](../Page/热带雨林.md "wikilink")[沼泽中](../Page/沼泽.md "wikilink")。在中国，苏门犀曾经广布于[华南地区](../Page/华南.md "wikilink")，尤其是[四川](../Page/四川.md "wikilink")。\[1\]\[2\]
现已是[极危动物](../Page/极危.md "wikilink")，于1916年在中国灭绝。现在野外仅存6个种群，其中4个在[苏门答腊](../Page/苏门答腊.md "wikilink")，另外两个分别在[婆罗洲与](../Page/婆罗洲.md "wikilink")[马来半岛](../Page/马来半岛.md "wikilink")，总数不到100隻。\[3\]2008年[美国](../Page/美国.md "wikilink")[网站](../Page/网站.md "wikilink")《[生活科学](../Page/生活科学.md "wikilink")》评出苏门答腊犀牛为全球十大最濒危的稀有动物物种之一。苏门犀濒危的主要原因是为其角而引发的[偷猎](../Page/偷猎.md "wikilink")。犀角在中医药材中极其珍贵，每公斤值3万美元，不過藥材店現在很多都偷偷改用水牛角了。反而現在大規模伐木与森林的开发才是另外一个重要威胁。苏门犀在人工饲养环境下适应较差。

## 分类与命名

西方对苏门犀的最初记载是在1793年，当时一头犀牛在[苏门答腊的Fort](../Page/苏门答腊.md "wikilink")
Marlborough十六公里外的郊野被枪杀。该标本的画像与描述记载被送到自然学家Joseph
Banks的手里，当时英国[皇家学会的总裁](../Page/皇家学会.md "wikilink")，他出版了一张关于此标本的文献。不过直到1814年这个物种才被德国科学家Johann
Fischer von Waldheim正式命名。\[4\]\[5\]

苏门犀的学名(*Dicerorhinus
sumatrensis*)源于古[希腊语的](../Page/希腊语.md "wikilink")*Di*(两个),
*cero*(角), *rhinus* (鼻), *sumatrensis*
（苏门答腊的），其意思就是“苏门答腊的双鼻角犀”。\[6\][卡尔·林奈本将所有的犀牛分到](../Page/卡尔·林奈.md "wikilink")[犀牛属](../Page/犀牛属.md "wikilink")(''Rhinoceros
*)，所以苏门犀最初的学名是(*Rhinoceros
sumatrensis*)。后来的学家认为苏门犀的双角比较特别，并把它分到独立的属，[双角犀属](../Page/双角犀属.md "wikilink")(*Dicerorhinus'')。

目前有三个[亚种被认同](../Page/亚种.md "wikilink")：

  - 苏门答腊亚种或西部亚种***D.s.
    sumatrensis***分布于[苏门答腊岛与](../Page/苏门答腊岛.md "wikilink")[马来半岛](../Page/马来半岛.md "wikilink")。虽然仅剩275头，但仍是三个亚种数量最多的。大约75头存活在[马来半岛](../Page/马来半岛.md "wikilink")，其余的生活在苏门答腊西部。马来半岛的种群曾被列为独特的亚种*D.s.
    niger*，但之后发现它们与苏门答腊岛的犀牛十分相似。\[7\]

<!-- end list -->

  - 婆罗洲亚种或东部亚种***D.s.
    harrissoni***体型较小，分布于[婆罗洲的](../Page/婆罗洲.md "wikilink")[沙巴](../Page/沙巴.md "wikilink")，在[砂拉越与](../Page/砂拉越.md "wikilink")[加里曼丹有未证实的记录](../Page/加里曼丹.md "wikilink")，现野外仅存25头。

<!-- end list -->

  - 北方亚种***D.s.
    lasiotis***体型较大，曾广布于[印度](../Page/印度.md "wikilink")，[印度支那](../Page/印度支那.md "wikilink")，与中国[华南地区](../Page/华南.md "wikilink")。现已灭绝，其中在中国是1916年灭绝的。现在[缅甸与](../Page/缅甸.md "wikilink")[云南仍有未证实的目击报告](../Page/云南.md "wikilink")。\[8\]
    近期在[神农架发现石化程度很轻的骸骨](../Page/神农架.md "wikilink")，说明直到近代，这里依然有犀牛的存在。

近期的基因研究发现了三种不同的种族。其中马来半岛与苏门答腊东部虽然被[马六甲海峡分开](../Page/马六甲海峡.md "wikilink")，但它们的血缘关系较接。而苏门答腊西部的种群被巴里散山脉隔离的时间较长，婆罗洲的种群则更独特。亚种之间的交配不成问题。

### 进化

犀牛最早出现于早[始新世](../Page/始新世.md "wikilink")。DNA的对比表明犀牛与[马在](../Page/马.md "wikilink")5千万年前"分家"。\[9\]\[10\]现存的[犀科在晚](../Page/犀科.md "wikilink")[始新世的](../Page/始新世.md "wikilink")[欧亚大陆出现](../Page/欧亚大陆.md "wikilink")，现存犀牛的共同祖先生存在[中新世](../Page/中新世.md "wikilink")。\[11\]\[12\]

苏门犀是现存犀牛中最原始的一种，因为它保留了许多祖先的特征。最早的[双角犀属的化石记录出现在中新世早期](../Page/双角犀属.md "wikilink")，大约2300-1600万年前。基因研究表明双角犀在2590±190万年前就已与其他犀牛分歧。目前有三种对于苏门犀与其他现存犀牛的亲缘关系的假说。一说苏门犀与非洲的[黑犀与](../Page/黑犀.md "wikilink")[白犀是近亲](../Page/白犀.md "wikilink")，因为它们都有两只角。\[13\]其他分类学者认为苏门犀与[印度犀和](../Page/印度犀.md "wikilink")[爪哇犀更接近](../Page/爪哇犀.md "wikilink")，因为它们的分布区很接近。\[14\]\[15\]近期研究则表明两种非洲犀牛，两种亚洲独脚犀和苏门犀各自代表三种自2590万年前分裂的不同的世系，而哪一支先分歧目前仍不确定。\[16\]\[17\]

由于形态的接近，苏门犀与著名的灭绝种类[披毛犀](../Page/披毛犀.md "wikilink")(*Coelodonta
antiquitatis*)被认为是近亲。近期的基因研究也支持这一说。披毛犀是著名的[冰期动物](../Page/冰期.md "wikilink")，最早出现在中国。在晚[更新世它们已广布于](../Page/更新世.md "wikilink")[欧亚大陆](../Page/欧亚大陆.md "wikilink")，东起[朝鲜西至](../Page/朝鲜.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")，并于1万年前灭绝。\[18\][双角犀属的化石有很多](../Page/双角犀属.md "wikilink")，但苏门犀是唯一的近代物种。\[19\]

## 特征

[Sumatran_Rhino_skeleton.jpg](https://zh.wikipedia.org/wiki/File:Sumatran_Rhino_skeleton.jpg "fig:Sumatran_Rhino_skeleton.jpg")
一头典型的成年苏门犀肩高大约125厘米，体长在240-315厘米，平均体重大约700[公斤](../Page/公斤.md "wikilink")，不过最大的个体可以达到1100公斤。苏门犀长有两个突角，这一点与[非洲的](../Page/非洲.md "wikilink")[白犀和](../Page/白犀.md "wikilink")[黑犀相似](../Page/黑犀.md "wikilink")，而不同于亚洲的[印度犀和](../Page/印度犀.md "wikilink")[爪哇犀](../Page/爪哇犀.md "wikilink")。前角较大，通常为15-25厘米，不过最大的记录据说有81厘米，后角较小，通常是少于10厘米。\[20\]角的颜色呈黑灰色或黑色，雄性的角大于雌性，不过此外两性没有区别。野外寿命大约为30-45年，饲养环境中的记录是一头雌性的北方苏门犀，活了32年与8个月并在1900年死于伦敦动物园。

四腿之后有两片褶皱的皮肤，颈部有一片较小的皱肤。皮肤较薄，有2-3厘米厚。野外个体没有皮下脂肪。毛有浓有稀，幼仔的毛较密，毛色为红棕色。野外的毛较难观察因为它们身上通常覆盖着泥。饲养个体的毛较粗农因为与草木的摩擦较少。耳中的毛较长，尾尖有一簇浓密的毛。与其他犀牛一样，苏门犀的视力很差。虽然它们看似笨重，但它们可以轻松的翻山越岭。\[21\]\[22\]\[23\]

## 栖息与分布

[Cloud_forest_mount_kinabalu.jpg](https://zh.wikipedia.org/wiki/File:Cloud_forest_mount_kinabalu.jpg "fig:Cloud_forest_mount_kinabalu.jpg")[沙巴的](../Page/沙巴.md "wikilink")[雨林](../Page/雨林.md "wikilink")\]\]
苏门犀在[低地与](../Page/低地.md "wikilink")[高原都有出现](../Page/高原.md "wikilink")，生活在[雨林](../Page/雨林.md "wikilink")，[沼泽和](../Page/沼泽.md "wikilink")[雨林中](../Page/雨林.md "wikilink")。栖息于接近水源的丘陵地带，尤其是灌木较浓的山坡地带。苏门犀曾经遍布于[苏门答腊](../Page/苏门答腊.md "wikilink")，[婆罗洲](../Page/婆罗洲.md "wikilink")，[印度](../Page/印度.md "wikilink")，[印度支那和中国](../Page/印度支那.md "wikilink")[华南甚至](../Page/华南.md "wikilink")[中原地区](../Page/中原.md "wikilink")（中国早期的记载并没有鉴别种类，所以并不确定是否苏门犀和爪哇犀两种都有。从现有文物来看，如[小臣艅犀尊](../Page/小臣艅犀尊.md "wikilink")
、[战国错金银云纹铜犀尊均为双角](../Page/战国错金银云纹铜犀尊.md "wikilink")，似以苏门犀为主）。现仅在[马来半岛](../Page/马来半岛.md "wikilink")，[苏门答腊与](../Page/苏门答腊.md "wikilink")[婆罗洲有零星分布](../Page/婆罗洲.md "wikilink")。未证实的记载在各地都有，不过[缅甸有存活种群的证据](../Page/缅甸.md "wikilink")，可惜该国的政治混乱阻止了进一步的调查。\[24\]

## 习性

苏门犀是独居动物，雄性的生活范围可达50平方公里，雌性为10-15平方公里。雌雄的地盘经常重叠。目前没有发现因争夺地盘而搏斗的行为。它们会挖泥土，排便，或把树苗弯成独特的样式来表示它们的地盘。通常黄昏和清晨出来觅食，白天待在泥池里休息。在雨季会迁徙到海拔较高的地区，在较寒冷的季节迁回山谷\[25\]。

[Sumatran_Rhino_001.jpg](https://zh.wikipedia.org/wiki/File:Sumatran_Rhino_001.jpg "fig:Sumatran_Rhino_001.jpg")
苏门犀花很长一段时间泡泥澡。当泥坑变小时，它们会用角与脚去挖深。泥坑会被用上2-12周，然后被抛弃。它们通常在中午泡上2-3个小时。泥澡能帮助它们维持体温并去除皮外[寄生虫](../Page/寄生虫.md "wikilink")。苏门犀的饲养之所以比较容易失败，正是因为泥澡的缺乏，导致皮肤[发炎与](../Page/发炎.md "wikilink")[化脓](../Page/化脓.md "wikilink")，[眼病](../Page/眼病.md "wikilink")，掉毛以及最终的死亡。\[26\]

饲养的苏门犀的主要死于[虱子与](../Page/虱子.md "wikilink")[胃蝇](../Page/胃蝇.md "wikilink")\[27\]。它们也会受[苏拉病的影响](../Page/苏拉病.md "wikilink")，寄生[锥体虫受到](../Page/锥体虫.md "wikilink")[苍蝇的传播](../Page/苍蝇.md "wikilink")。在2004年的18天内，有5头犀牛得此病而陆续死亡。除了人类之外，成年个体没有天敌。幼仔会受到[虎](../Page/虎.md "wikilink")、[豺和野](../Page/豺.md "wikilink")[狗的攻击](../Page/狗.md "wikilink")。虽然苏门犀与[亚洲象和](../Page/亚洲象.md "wikilink")[马来貘的分布重叠](../Page/马来貘.md "wikilink")，它们之间并没有食物竞争。它们甚至会公用一条道路。\[28\]\[29\]

### 食性

|                                                                                                                                                                         |                                                                                                                                            |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| [MallotesPhilipensis.jpg](https://zh.wikipedia.org/wiki/File:MallotesPhilipensis.jpg "fig:MallotesPhilipensis.jpg")                                                     | [Garcinia_mangostana_fruit1.jpg](https://zh.wikipedia.org/wiki/File:Garcinia_mangostana_fruit1.jpg "fig:Garcinia_mangostana_fruit1.jpg") |
| [Eugenia1.jpg](https://zh.wikipedia.org/wiki/File:Eugenia1.jpg "fig:Eugenia1.jpg")                                                                                      | [Ardisia_crenata6.jpg](https://zh.wikipedia.org/wiki/File:Ardisia_crenata6.jpg "fig:Ardisia_crenata6.jpg")                                |
| 苏门犀的食性很广，包括（从左上角顺时针）：[野桐属](../Page/野桐属.md "wikilink")、[山竹](../Page/山竹.md "wikilink")、[紫金牛属和](../Page/紫金牛属.md "wikilink")[番樱桃属](../Page/番樱桃属.md "wikilink")\[30\]\[31\]。 |                                                                                                                                            |

通常在黄昏和清晨出来觅食。以小嫩叶、树苗、水果、嫩芽和细枝为食。\[32\]每天可吃掉50公斤。\[33\]从粪便标本可看出，苏门犀的以超过100种植物为食。最常见的食物是树干直径1-6厘米的树苗。它们通常将这些树苗推倒，并吃掉上面的树叶。最常见的被食种类为[番樱桃属的植物](../Page/番樱桃属.md "wikilink")。

苏门犀的食物含大量的[膳食纤维与适中的](../Page/膳食纤维.md "wikilink")[蛋白质](../Page/蛋白质.md "wikilink")。\[34\]盐是很重要的营养来源，进行盐添的地方包括小热泉，咸水的渗漏或泥热泉。盐添也存在着社交目的，雄性会巡视盐田来寻找雌性。有些犀牛生存的地区不存在盐田，它们靠吃些含许多矿物的植物来获得必需的营养。\[35\]\[36\]

### 交流

<table>
<thead>
<tr class="header">
<th><p>Sumatran Rhinoceros<br />
vocalizations (.wav files)[37]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="ftp://ftp.aip.org/epaps/acoust_res_lett/E-ARLOFJ-4-005303/MM.1.wav">“咦”</a></li>
<li><a href="ftp://ftp.aip.org/epaps/acoust_res_lett/E-ARLOFJ-4-005303/MM.2.wav">鲸歌</a></li>
<li><a href="ftp://ftp.aip.org/epaps/acoust_res_lett/E-ARLOFJ-4-005303/MM.3.wav">尖哨</a></li>
</ul></td>
</tr>
</tbody>
</table>

苏门犀是犀牛中最健谈的，\[38\]饲养和野外的观察发现它们几乎不停的发声。\[39\]
最常见的三种声音包括“咦”，[鲸歌与尖哨声](../Page/鲸歌.md "wikilink")。“咦”是一种长约1秒的尖叫，是最常见的声音。它们也会发出与[露脊鲸相似的声音](../Page/露脊鲸.md "wikilink")，故名鲸歌。尖哨声是三种声音中最响的，响到能让动物园的铁栏震动。这些声音的目的目前还不清楚，不过应该与其他有蹄类一样，用来警报、表达位置和性问题。尖哨声可以在很远的距离被听见。亚洲象发出的响声相近地声音可散播至9.8km以外，因此苏门犀的尖哨声应该也能传播至相似的距离。\[40\]苏门犀会将没有吃掉的树苗扭曲，以表明自己的地盘或小径的交叉口。\[41\]

### 繁殖

雌性在6-7岁达到性成熟，雄性则要达到10岁以后。孕期为15-16个月。幼仔体重通常为40-60公斤，在15个月后断奶，并在1-3岁时待在母亲身边。在野外生殖间歇期为4-5年。目前我们对幼仔抚养的行为并不了解。

苏门犀的性习性在饲养环境中被了解。性关系之前会出现一段恋爱期，导致声音交流的增加，尾部的提高，小便以及更多的肉体接触，包括雌雄体用口鼻部抚摸互相的头部与生殖器。恋爱的习性与[黑犀较相似](../Page/黑犀.md "wikilink")。年轻的雄犀牛往往会对雌性施暴，导致雌性的受伤甚至死亡。在野外雌性会逃离过度粗暴的雄性，在饲养的笼圈中则不行，导致较低的饲养生殖率。\[42\]\[43\]\[44\]

发情期长约24小时，并会在21-25天后复发。辛辛纳提动物园的个体交配时间大约为30-50分钟，与其他犀牛相似，在[马来西亚的苏门犀保护中心的个体则较短暂](../Page/马来西亚.md "wikilink")。苏门犀较长的交配时间与对其他犀牛的观察，表明其交配季节应该也比较长。\[45\]2001年之前的饲养繁殖都因各种原因而失败。研究发现苏门犀的排卵靠交配的催生且有不稳定的[孕酮数量](../Page/孕酮.md "wikilink")。\[46\]2001年的成功是因为[孕酮的补给](../Page/孕酮.md "wikilink")。\[47\]

## 濒危与保护

苏门犀在[东南亚的数量曾经很多](../Page/东南亚.md "wikilink")，现在野外仅存300头。虽然综合数量比[爪哇犀要多](../Page/爪哇犀.md "wikilink")，但苏门犀灭绝的威胁更大因为当地较严重的捕猎与栖息地的丧失，而且它们的种群相隔比较远。相反几乎所有的爪哇犀都生活在[爪哇岛的Ujung](../Page/爪哇岛.md "wikilink")
Kulon半岛。另外苏门犀的数量正在下降，而爪哇犀的数量则比较稳定。两种犀牛都被列为[极危动物](../Page/极危.md "wikilink")。偷猎与栖息地的丧失导致它们仅存于人类几乎无法到达的地域。\[48\]\[49\]

虽然针对苏门犀的捕猎没有非洲犀牛那么严重（至少在捕猎的数量上），但它依然是个问题。犀牛角的价格极其昂贵，可达每公斤3万美元，且价格会随着数量的减少而提高。\[50\]苏门犀并不像非洲犀样受到大规模的捕杀，因为它们很难被直接捕猎或研究（一名研究者在一盐田蹲了7个星期才遇到活的犀牛）。陷阱的使用则更常见。当地人用犀角当护身符，认为它可防止疾病与中毒。干犀肉则被用来当治[腹泻](../Page/腹泻.md "wikilink")，[麻风与](../Page/麻风.md "wikilink")[肺结核的药材](../Page/肺结核.md "wikilink")。“犀油”会在犀牛头骨在[椰油泡上数星期后生产](../Page/椰油.md "wikilink")，并用来治皮肤病。这些材料在[中医药学中使用甚广](../Page/中医.md "wikilink")。\[51\]\[52\]\[53\]
犀牛部位曾被当做[春药使用](../Page/春药.md "wikilink")。\[54\]

[印度尼西亚与](../Page/印度尼西亚.md "wikilink")[马来西亚的](../Page/马来西亚.md "wikilink")[热带雨林是多数合法与违法的伐木目标](../Page/热带雨林.md "wikilink")，由于其硬木树制的可贵性。稀有树种像[印茄木](../Page/印茄木.md "wikilink")，[梅兰蒂木与](../Page/梅兰蒂木.md "wikilink")[胶木是珍贵的国际市场产品](../Page/胶木.md "wikilink")，其价格可达每立方米1800美元。非法伐木业的组织很难进行，因为许多人类生活与犀牛同样的森林。

## 參考文献

## 外部連結

  - [IUCN Red
    List](http://www.redlist.org/search/details.php?species=6553)
  - [蘇門答臘犀牛繁殖項目](https://web.archive.org/web/20070927184526/http://www.ecologyasia.com/news-archives/2003/dec-03/thestar_20031202_1.htm)
  - [亞洲犀牛基金會](https://web.archive.org/web/20090623000322/http://www.asianrhino.nl/English/Home)
  - [Going, Going, Gone: Sumatran
    Rhino](http://lastshotatlife.blogspot.com/2007/01/sumatran-rhino.html)

[Category:印尼動物](../Category/印尼動物.md "wikilink")
[Category:馬來西亞動物](../Category/馬來西亞動物.md "wikilink")
[Category:犀科](../Category/犀科.md "wikilink")
[Category:活化石](../Category/活化石.md "wikilink")

1.  *The Art of Rhinoceros Horn Carving in China* (1999), p. 27. Jan
    Chapman. Christie’s Books, London.

2.  *The Golden Peaches of Samarkand: A study of T’ang Exotics* (1963),
    p 83. Edward H. Schafer. University of California Press. Berkeley
    and Los Angeles. First paperback edition: 1985.

3.  \[[http://e-info.org.tw/node/84876\]蘇門答臘犀牛不到100頭](http://e-info.org.tw/node/84876%5D蘇門答臘犀牛不到100頭)
    印尼、馬來西亞合力搶救

4.

5.

6.

7.

8.  Asian Rhino Specialist Group (1996). [*Dicerorhinus sumatrensis*
    ssp.''
    lasiotis''](http://www.iucnredlist.org/search/details.php/6554).
    *2007 [IUCN Red List of Threatened
    Species](../Page/IUCN_Red_List_of_Threatened_Species.md "wikilink")*.
    IUCN 2007. Retrieved on January 13, 2008.

9.

10.

11.

12.

13.
14.
15.

16.
17.

18.

19.
20.

21.

22.
23.

24.

25.
26.

27.
28.
29.
30.
31.
32.
33.
34.

35.
36.

37.
38.
39.
40.

41.
42.

43.

44.

45.
46.

47.

48.

49.

50.
51.
52.
53.

54.