**顏鳴皋**（），[中国著名](../Page/中国.md "wikilink")[材料学家](../Page/材料学家.md "wikilink")，[中国](../Page/中国.md "wikilink")[航空](../Page/航空.md "wikilink")[航天](../Page/航天.md "wikilink")[材料的创始人之一](../Page/材料.md "wikilink")，[中国科学院](../Page/中国科学院.md "wikilink")[院士](../Page/院士.md "wikilink")。原[浙江](../Page/浙江.md "wikilink")[慈溪县](../Page/慈溪县.md "wikilink")（今属浙江省[宁波市](../Page/宁波市.md "wikilink")[江北区](../Page/江北区.md "wikilink"))人。

## 简历

1920年6月12日出生。

1938年，入读重庆[国立中央大学](../Page/国立中央大学_\(南京\).md "wikilink")[工学院机械系](../Page/东南大学机械工程学院.md "wikilink")。

1942年7月获[学士学位](../Page/学士.md "wikilink")。毕业后，任经济部中央工业实验所助理工程师。

1946年，远赴[美国留学](../Page/美国.md "wikilink")，入读美国[耶鲁大学冶金系](../Page/耶鲁大学.md "wikilink")，1947年获[硕士学位](../Page/硕士.md "wikilink")，1949年获[博士学位](../Page/博士.md "wikilink")。

2014年12月24日，在北京逝世，享年95岁。\[1\]

## 贡献

1957年，主持建立中国第一个航空[钛合金实验室](../Page/钛.md "wikilink")，先后研制出钛合金12种，建立了中国自己的航空钛合金系列。

## 任职

  - 1987年，被选为第五届国际材料力学行为会议理事会主席。
  - 1999年，被选为国际疲劳大会终身荣誉会员和国际材料力学行为会议名誉主席。
  - 中国航空工业总公司航空材料研究所研究员、高级技术顾问。
  - 北京航空材料研究院高级技术顾问、学位评定委员会主任
  - 《航空材料学报》主编

## 荣誉

  - 1991年，获中国航空航天部航空金奖。
  - 1991年，中国科学院院士。
  - 2001年，获何梁何利科学与技术进步奖。

## 著作

《材料科学前沿研究》、《金属的疲劳与断裂》、《颜鸣皋院士论文选集》等。

## 参考文献

[Y颜](../Category/中国化学家.md "wikilink")
[Y颜](../Category/慈溪人.md "wikilink")
[工](../Category/中央大学校友.md "wikilink")
[2](../Category/东南大学校友.md "wikilink")
[Y颜](../Category/耶鲁大学校友.md "wikilink")
[Category:颜姓](../Category/颜姓.md "wikilink")

1.