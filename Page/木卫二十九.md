**木卫二十九**墨塞勒（****，**Thyone**，，；希腊语：*Θυώνη*），又名****，是[木星的一颗](../Page/木星.md "wikilink")[逆行的](../Page/順行和逆行.md "wikilink")[不規則衛星](../Page/不規則衛星.md "wikilink")。由[斯科特·谢泼德所領導的](../Page/斯科特·谢泼德.md "wikilink")[夏威夷大學研究小組於](../Page/夏威夷大學.md "wikilink")2000年發現。\[1\]\[2\]

其直徑約為4公里，軌道平均半徑為21.406
Mm，[軌道周期為](../Page/軌道周期.md "wikilink")639.803[地球日](../Page/地球日.md "wikilink")，與[黃道間的](../Page/黃道.md "wikilink")[軌道傾角為](../Page/軌道傾角.md "wikilink")147°（与木星赤道147°），運轉方向為[逆行](../Page/順行和逆行.md "wikilink")，[軌道離心率为](../Page/軌道離心率.md "wikilink")0.2526。它的平均轨道速度是2.43
km/s。

它在2003年8月被命名为Thyone，指[希腊神话中](../Page/希腊神话.md "wikilink")[狄俄倪索斯的母亲](../Page/狄俄倪索斯.md "wikilink")[塞墨勒](../Page/塞墨勒.md "wikilink")。\[3\]

它是中的成员，而阿南刻衛星群是一群環繞木星[逆行的](../Page/順行和逆行.md "wikilink")[不規則衛星](../Page/不規則衛星.md "wikilink")，它們的軌道[半長軸在](../Page/半長軸.md "wikilink")19.3與22.7
Gm之間，且軌道傾角都在150°左右。

## 参考资料

[Category:木星的卫星](../Category/木星的卫星.md "wikilink")
[Category:不規則衛星](../Category/不規則衛星.md "wikilink")
[Category:2001年发现的天体](../Category/2001年发现的天体.md "wikilink")

1.  [IAUC 7900: *Satellites of
    Jupiter*](http://cfa-www.harvard.edu/iauc/07900/07900.html) 2002 May
    16 (discovery)
2.  [MPEC 2002-J54: *Eleven New Satellites of
    Jupiter*](http://cfa-www.harvard.edu/mpec/K02/K02J54.html) 2002 May
    (discovery and ephemeris)
3.  [IAUC 8177: *Satellites of Jupiter, Saturn,
    Uranus*](http://cfa-www.harvard.edu/iauc/08100/08177.html)  2003
    August (naming the moon)