**克鲁西布剧院**（**Crucible
Theatre**）位于[英国](../Page/英国.md "wikilink")[谢菲尔德市中心](../Page/谢菲尔德.md "wikilink")，1971年正式开放使用。剧院拥有一个具有980个座位的伸出式舞台（Thrust
Stage）的主剧场，此外还有一个400个座位的克鲁西布工作室（Crucible Studio）。

剧院演出的戏剧通常由谢菲尔德剧院集团(the Sheffield Theatres Group)监制（overseen）。

除了用于戏剧的演出，克鲁西布剧院为大多数人所了解还是因为每年在此举行的[斯诺克台球最重要的赛事](../Page/斯诺克.md "wikilink")——[世界锦标赛](../Page/斯诺克世界锦标赛.md "wikilink")。自1976年香烟品牌Embassy赞助世界锦标赛以来，漂泊不定的世界锦标赛从1977年开始便固定在克鲁西布剧院举行。克鲁西布剧院也一度成为斯诺克的代名词。

克鲁西布剧院还举行一些其它室内运动的比赛，例如[乒乓球](../Page/乒乓球.md "wikilink")，[壁球等](../Page/壁球.md "wikilink")。

## 外部链接

  - [克鲁西布剧院官方网上订票站点](http://www.sheffieldtheatres.co.uk/)（英文）

[Category:斯诺克](../Category/斯诺克.md "wikilink")
[Category:英國劇院](../Category/英國劇院.md "wikilink")
[Category:斯诺克世界锦标赛](../Category/斯诺克世界锦标赛.md "wikilink")