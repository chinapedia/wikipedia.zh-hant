**成武县**在[中国](../Page/中国.md "wikilink")[山东省西南部](../Page/山东省.md "wikilink")，是[菏泽市所辖的一个](../Page/菏泽市.md "wikilink")[县](../Page/县.md "wikilink")。成武是[伯乐的故里](../Page/伯乐.md "wikilink")。

## 行政区划

成武县辖：[成武镇](../Page/成武镇.md "wikilink") [大田集镇](../Page/大田集镇.md "wikilink")
[伯乐集镇](../Page/伯乐集镇.md "wikilink") [天宫庙镇](../Page/天宫庙镇.md "wikilink")
[汶上集镇](../Page/汶上集镇.md "wikilink") [孙寺镇](../Page/孙寺镇.md "wikilink")
[白浮图镇](../Page/白浮图镇.md "wikilink") [苟村集镇](../Page/苟村集镇.md "wikilink")
[南鲁集镇](../Page/南鲁集镇.md "wikilink")
[九女集镇](../Page/九女集镇.md "wikilink")
[党集镇](../Page/党集镇.md "wikilink")
[张楼镇](../Page/张楼镇.md "wikilink")。

## 历史沿革

[秦始皇时始置成武县](../Page/秦始皇.md "wikilink")，“成武”一名因[汉高祖刘邦在此](../Page/汉高祖.md "wikilink")“成就武功”而得名；556年，升成武为[永昌郡](../Page/永昌郡.md "wikilink")，治[郜城](../Page/郜城.md "wikilink")；[隋朝](../Page/隋朝.md "wikilink")[开皇初年](../Page/开皇.md "wikilink")，废永昌郡，复置成武县；[明改城武县](../Page/明.md "wikilink")；1735年，成武改属[曹州府](../Page/曹州府.md "wikilink")；1958年复名成武县。

## 著名人物

[:Category: 成武人](../Category/_成武人.md "wikilink")

著名人物有[伯乐](../Page/伯乐.md "wikilink")（孙阳），[伯乐集镇因此得名](../Page/伯乐集镇.md "wikilink")，其中心大街名为相马街。

## 外部链接

  - [菏泽市成武县政府网站](http://www.chengwu.gov.cn/)

[成武县](../Category/成武县.md "wikilink") [县](../Category/菏泽区县.md "wikilink")
[菏泽](../Category/山东省县份.md "wikilink")