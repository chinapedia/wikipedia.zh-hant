**迪納摩體育協會**或**迪納摩体协**（[德語](../Page/德語.md "wikilink")：**，缩写：**），是[民主德国的一个行业性体育组织](../Page/民主德国.md "wikilink")，它的會員主要是來自保安部隊諸如[德國人民警察](../Page/德國人民警察.md "wikilink")（、VP）與[國家安全部](../Page/史塔西.md "wikilink")（、Stasi）的成員。協會成立於1953年，1990年德國統一后解散，而協會主席一直由[埃里希·米爾克](../Page/埃里希·米爾克.md "wikilink")（Erich
Mielke）擔任。總部設在[東柏林](../Page/東柏林.md "wikilink")[霍恩施豪森](../Page/霍恩施豪森.md "wikilink")（）靠近[史塔西的](../Page/史塔西.md "wikilink")[總部](../Page/總部.md "wikilink")。

## 参考資料

[Bundesarchiv_Bild_183-48465-0050,_Leipzig,_Turn-_und_Sporttreffen,_Massenübung.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-48465-0050,_Leipzig,_Turn-_und_Sporttreffen,_Massenübung.jpg "fig:Bundesarchiv_Bild_183-48465-0050,_Leipzig,_Turn-_und_Sporttreffen,_Massenübung.jpg")

  - [檔案館與迪納摩](https://web.archive.org/web/20130224004216/http://www.sportmuseum-leipzig.de/)

<div class="references-small">

<references />

</div>

[Category:迪纳摩体育协会](../Category/迪纳摩体育协会.md "wikilink")
[Category:东德体育](../Category/东德体育.md "wikilink")
[Category:1953年建立](../Category/1953年建立.md "wikilink")