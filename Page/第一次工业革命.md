**工业革命**（），又称**产业革命**，更準確的說是**第一次工业革命**，約於1760年代興起，持續到1830年代至1840年代，而後產生所謂的[第二次工業革命](../Page/第二次工业革命.md "wikilink")(1870年)和現在(20世紀)的[數字化革命](../Page/數字化革命.md "wikilink")。在這段時間裡，人類生產與製造方式逐漸轉為機械化，出現了以[机器取代](../Page/机器.md "wikilink")[人力](../Page/人力.md "wikilink")、[獸力的趨勢](../Page/獸力.md "wikilink")，以大规模的[工廠生产取代手工生产的一场革命](../Page/工廠.md "wikilink")，引發了現代的[科技革命](../Page/科技革命.md "wikilink")。由於機器的發明及運用成為了這個時代的標誌，因此[歷史學家稱這個時代為](../Page/歷史學家.md "wikilink")**機器時代**（the
Age of Machines）。

工业革命在1759年左右已经开始，但直到1830年前尚未真正蓬勃地發展。大多数观点认为，工业革命发源于[英格兰中部地区](../Page/英格兰.md "wikilink")。1769年，[英国人](../Page/英国人.md "wikilink")[瓦特改良](../Page/詹姆斯·瓦特.md "wikilink")[蒸汽机之后](../Page/蒸汽机.md "wikilink")，由一系列技术革命引起了从手工劳动向动力机器生产转变的重大飞跃。随后自[英格兰扩散到整个](../Page/英格兰.md "wikilink")[欧洲大陆](../Page/欧洲.md "wikilink")，19世纪传播到[北美地区](../Page/北美.md "wikilink")。一般认为，[蒸汽机](../Page/蒸汽机.md "wikilink")、[煤](../Page/煤.md "wikilink")、[铁和](../Page/铁.md "wikilink")[钢是促成](../Page/钢.md "wikilink")**工业革命**技术加速发展的四项主要因素。英國為最早開始工業革命也是最早結束工業革命的國家。

在瓦特改良蒸汽机之前，整个生产所需动力依靠人力，畜力，水力和風力。伴随蒸汽机的发明和改进，工厂不再依河或溪流而建，很多以前依赖人力与手工完成的工作，自蒸汽机发明后被机械化生产取代。工业革命是一般[政治革命不可比拟的巨大变革](../Page/革命.md "wikilink")，與1萬年前[農業革命一般](../Page/農業革命.md "wikilink")，革命其影响涉及[人类社会生活的各个方面](../Page/人类社会.md "wikilink")，使社会发生了巨大的变革，对人类的现代化进程推动起到不可替代的作用，把人們推向了一个崭新的「蒸汽时代」。

## 背景

在尚未發生工業革命的時候，大部分西方人都在鄉間居住，並在細小的田地以[耕種和](../Page/耕種.md "wikilink")[畜牧維生](../Page/畜牧.md "wikilink")。他們以人力輔以[牛隻及簡單](../Page/牛.md "wikilink")[工具](../Page/工具.md "wikilink")[耕種](../Page/耕種.md "wikilink")，耕種的方法則仍沿用[中世紀的](../Page/中世紀.md "wikilink")[三田制](../Page/三田制.md "wikilink")（the
Three-field
System）。根據這種方法，[農民把土地劃分出三塊田](../Page/農民.md "wikilink")，每年只在其中兩塊田耕種，另外的一塊田則[休耕](../Page/休耕.md "wikilink")，收穫量不多。

这时只有一小部分的西方人在[城市居住](../Page/城市.md "wikilink")，當時城市的人口平均不到一萬人。除了貴族與教會之外，大部分的城市平民都是[商人和](../Page/商人.md "wikilink")[工匠出身](../Page/工匠.md "wikilink")。商人以轉售和運送工匠的製品謀生，而工匠則多在家中用手動的工具和細小的機器，來生產[衣服和一些木製與金屬製日用品維持生計](../Page/衣.md "wikilink")。這種以人手及簡單工具在坊間作業的生產方式稱為「[家庭手工業制](../Page/家庭手工業制.md "wikilink")」（the
Domestic System）。

在交通方面，因为此时鄉間和城市的居民都还是以[馬匹和](../Page/馬.md "wikilink")[馬車為主要的](../Page/馬車.md "wikilink")[運輸工具](../Page/運輸.md "wikilink")，但當時的[道路網絡設計並不完善](../Page/道路.md "wikilink")，道路多是崎嶇不平的；在雨天時，很多道路會被水淹沒，所以[交通会出现瘫痪的情况](../Page/交通.md "wikilink")，因此交通運輸[效率極低](../Page/效率.md "wikilink")。相對於當時擁有大型[運河的](../Page/運河.md "wikilink")[中國](../Page/中國.md "wikilink")，或有完善道路網的美洲文明，歐洲的城市規模無法達到同樣水準。由于当时无论鄉間或城市的生產動力都以[人力](../Page/人力.md "wikilink")、[獸力](../Page/獸力.md "wikilink")、[風力和](../Page/風力.md "wikilink")[水力等為主](../Page/水力.md "wikilink")，所以产量有限，當後來航海時代贸易、人口增加時，這種以[原始動力來](../Page/原始動力.md "wikilink")[生產物品的方法](../Page/生產.md "wikilink")，已不能再滿足日益增加的需求。

## 起因

大不列顛在法律與文化上，有著開啟工業革命的基礎。\[1\]以下是英國提供此種環境的關鍵因素：

  - 在英格蘭與蘇格蘭統一後的和平與穩定時期。\[2\]
  - 英國境內沒有貿易障礙、封建規費和關稅等，這類的障礙甚至不存在於英格蘭與蘇格蘭之間，而這使得英國成為當時「全歐洲最大的單一市場」\[3\]
  - [法治](../Page/法治.md "wikilink")（包括對財產權的執行及對合約的尊重）\[4\]
  - 直觀的法律系統，這法律系統使得成立合股公司是可能的\[5\]
  - 自由市場（資本主義）\[6\]
  - 英國也有著地理和自然資源方面的優勢。在水運是最便捷的運輸方式的時代，英國有著大量的海岸線及可航行河道；另外英國有著全歐洲最高品質的煤炭。英國在當時同時也有著大量利用水力能源的裝置。\[7\]

### 农业革命与人口增加

進入了[大航海时代後](../Page/大航海时代.md "wikilink")，多產的新[美洲作物以及礦石財富被送進](../Page/美洲作物.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，繁榮的發展使得歐洲人口數出現爆炸性躍升。到了18世紀，[英國人口不斷增加](../Page/英國.md "wikilink")，以及[英国农业革命和](../Page/英国农业革命.md "wikilink")[圈地运动的后果](../Page/圈地运动.md "wikilink")，以致农业[勞動力过剩](../Page/勞動力.md "wikilink")，不得不寻求新的就业机会，有助于推动工商業的發展；再加上人們的消费需求亦日漸增加，为批量化生产的商品提供了销路。

### 自然资源的因素

工业革命首先发生在英国中北部，那里拥有丰富的浅层[煤矿和](../Page/煤矿.md "wikilink")[铁矿资源](../Page/铁矿.md "wikilink")，但缺乏木材资源。工业革命下产生的[蒸汽机](../Page/蒸汽机.md "wikilink")，以及利用焦煤而不是木材炼钢的[冶金技术革新](../Page/冶金技术.md "wikilink")，这些使得英国的煤矿和铁矿有了广阔的用武之地。

### 貿易限制的解除

1623年[英國國王](../Page/英國.md "wikilink")[詹姆士一世允許設立](../Page/詹姆士一世.md "wikilink")[專利權保護新發明的權利](../Page/專利權.md "wikilink")，開始刺激許多新發明的產生。工业革命約於1750年开始于大英地区。工业革命的展開有多種原因，其中，[封建制度的結束就是一個明顯的工业革命](../Page/歐洲封建制度.md "wikilink")[導火線](../Page/導火線.md "wikilink")。隨著封建制度於18世紀初在西方消失，[貴族及](../Page/貴族.md "wikilink")[大地主所享有的各種特權](../Page/大地主.md "wikilink")（例如[貿易的專利](../Page/貿易.md "wikilink")）也隨之消失。這些改變推動了[自由貿易](../Page/自由貿易.md "wikilink")，形成了更大规模的市场，使[工](../Page/工業.md "wikilink")[商業的發展更為蓬勃](../Page/商業.md "wikilink")。在這種改變下，舊有的[家庭式工業生產模式已不能滿足貿易發展的需要](../Page/家庭式工業.md "wikilink")，所以人們便致力改進[生產技術和生產模式以增加產量](../Page/生產技術.md "wikilink")，因而引發了工業革命。

### 殖民地市场和原料

此外，[世界貿易的發展亦扮演著一個相當重要的角色](../Page/世界貿易.md "wikilink")。從15世紀發現[新航路起](../Page/新航路.md "wikilink")\[8\]，許多歐洲國家在[亞](../Page/亞洲.md "wikilink")、[非](../Page/非洲.md "wikilink")、[美三洲各自建立](../Page/美洲.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。至18世紀，這些殖民地不僅為它們提供[原料和商品出口](../Page/原料.md "wikilink")[市場](../Page/市場.md "wikilink")，還推動了世界貿易的發展。為了滿足因世界貿易所引致的強劲需求，人們便開始採用機器和其他方法來提高產量，從而引發了工業革命。

### 金融革命、 资本与新技术

工业革命发生前，英国实际上已经历过一场[金融革命](../Page/金融革命.md "wikilink")（西方的经济史学家将[英格兰银行的创立](../Page/英格兰银行.md "wikilink")、公债的发行和稳固以及其他金融业的变革称为“金融革命”），金融革命不断为工业革命注入资本燃料和动力。英国经济学家、诺贝尔奖得主[约翰·希克斯曾经详细考察了金融对工业革命的刺激作用](../Page/约翰·希克斯.md "wikilink")。他指出，工业革命不是技术创新的结果，或至少不是其直接作用的结果，而是金融革命的结果。工业革命早期使用的技术创新，大多数在工业革命之前早已有之。然而，[技术革命既没有引发经济持续增长](../Page/技术革命.md "wikilink")，也未导致工业革命。因为早已存在的技术发明缺乏大规模资金以及长期资金的资本土壤，便不能使其从作坊阶段走向诸如钢铁、纺织、铁路等大规模工业产业阶段，“工业革命不得不等候金融革命”\[9\]。工业革命兴起的新产业，其[工业原料成本](../Page/工业原料.md "wikilink")、[研发成本](../Page/研发成本.md "wikilink")、人力成本、厂房成本、设备成本等开支都十分巨大，如机械制造，冶金，铁路等等都属[资金密集型行业](../Page/资金密集型.md "wikilink")，对廉价资本依赖性大。英国[光荣革命](../Page/光荣革命.md "wikilink")，建立的文官制度和自上而下垂直征税税体系以及宪政改革对约束政府行为产生了可信承诺，使得英国国会能够严格督导政府的财政支出，使私人债务清偿方面具有较高的信誉保证。高效、透明、有序的税收体系，不仅令新兴的工业企业免受[苛捐杂税困扰](../Page/苛捐杂税.md "wikilink")，且令英国国债在世界范围内享有极高信誉，其长期公债利率一降再降，最后仅3%利率水平，银行利率降得更低（公债利率一般高于同期银行存款利率），英国在发展国债市场之后，进一步发展了股票市场、企业债券市场等等，如[伦敦证券交易所](../Page/伦敦证券交易所.md "wikilink")。这些都为工业革命提供了“廉价资金”。

### 科學的发展

承继了[文艺复兴和](../Page/文艺复兴.md "wikilink")[启蒙运動重视科学的精神](../Page/启蒙运動.md "wikilink")，西方的科学因而得到重大的发展。而那时的科学家的重要发明和发现，为工业革命的發展带来不少重要的助益。

## 重要的技术革新

工业革命的开始与18世纪下半叶开始的少量发明密切相关。在19世纪30年代已取得了以下重要领域的进步：

  - **纺织业** -
    一開始由[水車](../Page/水車.md "wikilink")、之後由[蒸汽機驱动的机械化纺纱大大增加了工人的产量](../Page/蒸汽機.md "wikilink")。[织机将工人的产量提高了](../Page/织机.md "wikilink")40%。\[10\]棉花[辘轳使去除棉花中种子的效率提高了](../Page/辘轳.md "wikilink")50%。羊毛和[亚麻的纺织和编织也产生了生产率的大幅提高](../Page/亚麻.md "wikilink")，但它们没有棉花那么显著。\[11\][Watt_James_von_Breda.jpg](https://zh.wikipedia.org/wiki/File:Watt_James_von_Breda.jpg "fig:Watt_James_von_Breda.jpg")在[汤玛斯·纽科门](../Page/汤玛斯·纽科门.md "wikilink")1712年蒸汽机的基础上在1781年发明了[蒸汽机](../Page/蒸汽机.md "wikilink")。\]\]
  - **蒸汽动力** - 蒸汽机的效率提高，使他们只需原先五分之一至十分之一的燃料
    。固定蒸汽发动机对旋转运动的适应使它们适合于工业用途。\[12\]高压发动机具有高的功率重量比，使其适合安在运输工具上，長距離陸上交通的火車隨之誕生。\[13\]
    1800年后，蒸汽动力迅速普及。蒸汽机改变了以往的生产只能依赖人力和畜力的局限，为工业生产、交通运输提供了廉价而充足的动力。
  - **钢铁生产工法** -
    用[焦炭代替木炭大大降低了生铁和锻铁生产的燃料成本](../Page/焦炭.md "wikilink")，也增大了钢铁的韧性和强度。\[14\]
    使用焦炭还增大了高炉的最大大小\[15\]<ref>

</ref>，扩大了经济规模。铸铁吹塑[气缸首先在](../Page/气缸.md "wikilink")1760年使用。其后通过使其双重作用得到改进，这使更高的炉温变为可能。钢铁业的进步，为技术革新生产的各种新机器提供了必要的原材料，同时也将英国大量的[煤炭资源利用了起来](../Page/煤炭.md "wikilink")。工业革命首先发生在[英国中北部](../Page/英国.md "wikilink")，和英国中北部拥有大量的[煤炭和](../Page/煤炭.md "wikilink")[铁资源是分不开的](../Page/铁.md "wikilink")。

## 生产制度的变革

航海時代的殖民公司制度逐漸成熟，並擴及到其他行業，資金的積聚及新機器的發明也是引發工業革命的其中兩個主要原因。18世紀時，歐洲本土的貿易發展蓬勃，使商人累積了大量[財富](../Page/財富.md "wikilink")。為了獲得更豐厚的[利潤](../Page/利潤.md "wikilink")，他們便致力[投資開設](../Page/投資.md "wikilink")[工廠](../Page/工廠.md "wikilink")、購置原料和發明新機器。加上隨著各類型機器的發明及應用，舊有以人力為主的生產工序逐漸被由[蒸汽推動的機器取代](../Page/蒸汽.md "wikilink")。生產工序的[機械化](../Page/機械化.md "wikilink")，提高了工農業的產量。結合以上的種種原因，就成就了工業革命的辉煌。

### 行会师徒制的式微

旧有的经济模式，[行会的师徒](../Page/同業公會.md "wikilink")[作坊式生产制度](../Page/作坊.md "wikilink")，控制了从生产技术到销售价格等多方面的因素，虽然保证了产品质量，但也阻碍了技术革新。而工业革命相对于行会师徒作坊式的生产，最大的变化就是采用了新技术，生产更加集中，工人们在大规模的工厂工作，而不是小作坊。由掌握销售渠道的商人开始组织生产。这种变化，大大提高了生产效率降低了成本，从而能够满足世界各地的广大市场需要。以往行会作坊生产的东西，相对工厂大规模生产的东西，更为精致，而可以称为“奢侈品”。但是工厂化的大规模制造，可以生产出众多价廉物美的商品，于是把各种所谓的“奢侈品”挤出了市场，行销整个世界。于是行会作坊式生产模式在工业革命产生的竞争下，走向了下坡路，越来越无法应付工业革命带来的变化。

### 圈地运动

而农村土地制度的变化，也就是首先在英国发生的[圈地运动](../Page/圈地运动.md "wikilink")，同样是一个生产集中化的过程。工业革命前的土地制度，使农民只能分别在许多分散的小块土地进行耕作。这些土地采用落后的[三圃制耕作技术](../Page/三圃制.md "wikilink")，每三年就要休耕一年，休耕的土地作为公共牧场供家畜使用，亩产低，土地浪费严重。但由于[敞地制土地犬牙交错互相连在一起](../Page/敞地制.md "wikilink")，单独小块土地无法改变耕作方式。比如若自己的土地不打算休耕，但也无法避免由于旁边的土地休耕后家畜对其的践踏。[圈地运动虽然对许多农民来说并不公平](../Page/圈地运动.md "wikilink")，许多农民被迫分得更贫瘠的土地，也无资本采用新的耕作技术，同时失去了休耕的公共土地放牧家畜的好处。但是土地集中后可以进行彻底的技术改良，生产更多的粮食供养更多的人口，尤其是可以为纺织工业生产大量的羊毛。不少失去土地的农民，为工业革命提供了大量的劳动力，成为了工厂的工人，也为新产生的[工人阶级埋下了伏笔](../Page/工人阶级.md "wikilink")。圈地运动在英国[伊丽莎白一世时期发展得比较缓慢](../Page/伊丽莎白一世.md "wikilink")，但自[光荣革命后的](../Page/光荣革命.md "wikilink")18世纪，开始愈演愈烈，从而引发了众多[农民起义](../Page/农民起义.md "wikilink")。

古老敞地制土地犬牙交錯互相連在一起，單獨小塊土地無法改變耕作方式。圈地運動在英國伊莉莎白一世時期發展得比較緩慢，但自光榮革命後的18世紀，開始愈演愈烈，從而引發了眾多農民叛亂。為首的一些人則主張把地主和農民二元對立實為過度簡單化歷史。他指出當時很多生活有著落的農民同樣積極地參與圈地。「我們應當小心一點，不要把這一切（圈地）歸於一個龐大複雜的歷史轉變的必然結果」
「18-19世紀的圈地狀況被嚴重誇大了」。圈地運動配合工業革命，為工業革命提供了充足的勞動力和工業原材料，以及能供養更多的非農業人口，雖然產生了許多不公和暴動，客觀上確實是工業化的一個必要前提，提供了土地集中後的規模耕作增加產量，被驅趕出的農民則投入工商業成為勞動力的來源。

## 技术革新

18到19世纪的英国，由于专利制度的保障，也由于能提高生产力的新技术会带来的十分可观的利润，于是有许多有钱人投资于各种发明创造。虽然不是所有的发明都能取得预期的成功，但是若某一种新技术发明能应用在生产中而提高生产效率，就会带来十分可观的利润，就如现在的风险投资一样。技术发明如滚雪球一般，一种新的技术发明出来，就会刺激发明另一种新的技术。比如[珍妮纺纱机这种新技术](../Page/珍妮纺纱机.md "wikilink")，使得能纺出更多物美价廉的纱，那么将会产生更多地对棉花的需求，又可为提高织布技术带来了激励。生产上每个环节技术上的革新，都会刺激相应环节不断寻找新的技术革新，以便适应新的生产效率下的需求，如同链式反应一般。

工业革命前后共有很多種不同的[發明](../Page/發明.md "wikilink")，除了較為著名且最具代表性的[蒸汽机外](../Page/蒸汽机.md "wikilink")，還有很多發明对后世影響深遠。以下是一些較重要的發明列表：

| 年份    | 发明者                                                | 发明                                     |
| ----- | -------------------------------------------------- | -------------------------------------- |
| 1733年 | [約翰·凱](../Page/約翰·凱.md "wikilink")                 | [飛梭](../Page/飛梭.md "wikilink")         |
| 1765年 | [詹姆士·哈格-{里}-夫斯](../Page/詹姆士·哈格里夫斯.md "wikilink")   | [珍妮纺纱机](../Page/珍妮纺纱机.md "wikilink")   |
| 1778年 | [约翰·哈林頓](../Page/约翰·哈林頓.md "wikilink")             | [抽水马桶](../Page/抽水马桶.md "wikilink")     |
| 1781年 | [詹姆斯·瓦特](../Page/詹姆斯·瓦特.md "wikilink")             | 改良[蒸汽机](../Page/蒸汽机.md "wikilink")     |
| 1798年 | [阿羅斯·塞尼菲爾德](../Page/阿羅斯·塞尼菲爾德.md "wikilink")       | [平版印刷](../Page/平版印刷.md "wikilink")     |
| 1797年 | [亨利·莫兹莱](../Page/亨利·莫兹莱.md "wikilink")             | [螺丝切削机床](../Page/螺丝切削机床.md "wikilink") |
| 1807年 | [羅伯特·富爾頓](../Page/羅伯特·富爾頓.md "wikilink")           | [蒸汽輪船](../Page/蒸汽輪船.md "wikilink")     |
| 1812年 | [理查德·特里維西克](../Page/理查德·特里維西克.md "wikilink")       | [科尔尼锅炉](../Page/科尔尼锅炉.md "wikilink")   |
| 1814年 | [喬治·斯蒂芬生](../Page/喬治·斯蒂芬生.md "wikilink")           | [铁路](../Page/铁路.md "wikilink")         |
| 1815年 | [漢弗萊·戴維](../Page/漢弗萊·戴維.md "wikilink")             | [礦工燈](../Page/礦工燈.md "wikilink")       |
| 1829年 | [理查德·特里维西克](../Page/理查德·特里维西克.md "wikilink")       | [蒸汽火車](../Page/蒸汽火車.md "wikilink")     |
| 1837年 | [摩斯](../Page/摩斯.md "wikilink")                     | [电报机](../Page/电报机.md "wikilink")       |
| 1844年 | [威廉·费阿柏恩](../Page/威廉·费阿柏恩.md "wikilink")           | [兰开斯特锅炉](../Page/兰开斯特锅炉.md "wikilink") |
| 1876年 | [亚历山大·格拉汉姆·贝尔](../Page/亚历山大·格拉汉姆·贝尔.md "wikilink") | [电话](../Page/电话.md "wikilink")         |
| 1885年 | [卡尔·本茨](../Page/卡尔·本茨.md "wikilink")               | [汽车](../Page/汽车.md "wikilink")         |

**工业革命前后的一些重要发明**

## 社会影响

[KUKA_robot_for_flat_glas_handling.jpg](https://zh.wikipedia.org/wiki/File:KUKA_robot_for_flat_glas_handling.jpg "fig:KUKA_robot_for_flat_glas_handling.jpg")
工业革命对19世纪科学發展及社會變遷产生了極為重要的影响，以前的科学研究很少用于工业生产，随着工业革命的发展壮大，[工程师与](../Page/工程师.md "wikilink")[科学家的界限越来越小](../Page/科学家.md "wikilink")，更多的工程师埋头做科学研究。以前的科学家多是贵族或富人的子弟，现在则有许多来自工业发达地区和[工人阶级的子弟成为了科学家](../Page/工人阶级.md "wikilink")。他们更加对[化学和](../Page/化学.md "wikilink")[电学感兴趣](../Page/电学.md "wikilink")，这也促进了这些学科的发展。

由於圈地运动和农业技术的改良导致乡村许多剩余的人口大舉移入都市，欧洲主导的[资本主义经济的世界大规模贸易](../Page/资本主义.md "wikilink")，使得城市和工厂能够吸收这些大量人口，由此造成了[都市化的現象及](../Page/都市化.md "wikilink")[都會區的出現](../Page/都會區.md "wikilink")；都会化的生活，让知识与资讯沟通更为便利。而工业化使得出现现代化的交通工具，出行更加便利，使得人们更加见多识广。在[商品经济下利己的生活习惯里](../Page/商品经济.md "wikilink")，于是人们的思想发生了许多改变，更多人追求个人的幸福，而非[来世的幸福或集体的利益](../Page/来世.md "wikilink")。也因為[自由經濟主義的興起](../Page/資本主義.md "wikilink")，世界大规模自由贸易导致出现了一个新富阶层，再加上人们思想上的变化，進而使得更多的人，当时主要是[中产阶级對](../Page/中产阶级.md "wikilink")[民主政治的參與的兴趣](../Page/民主政治.md "wikilink")，由此导致欧洲各国[选举權与](../Page/选举權.md "wikilink")[被选举权](../Page/被选举权.md "wikilink")，不断扩大到社会上更多的人群中。

大量工廠的成立，工人悲慘的生活及工作環境也逐漸為人重視，許多的慈善機構於是成立，主張以[社會福利制度改善窮人生活](../Page/社會福利.md "wikilink")，也免費提供糧食及住所。由于资本主义的周期性经济危机，也由于当时没有任何政府提供的保障，许多工人在因经济危机而失业的情况下过着食不果腹的生活。在正常的经济环境裡，生产环境也十分恶劣收入也很微薄，这些有限的[社会福利并没有多大程度上改善工人的状况](../Page/社会福利.md "wikilink")，由此导致劳资双方也就是所谓[资产阶级与](../Page/资产阶级.md "wikilink")[工人阶级的对立](../Page/工人阶级.md "wikilink")。如1811年，一个名叫卢德的英国工人捣毁机器，从而引发了反对机械化的[卢德运动](../Page/卢德运动.md "wikilink")。[馬克思為首的](../Page/馬克思.md "wikilink")[左派學說正是在这样的环境下产生](../Page/左派.md "wikilink")，衍生出了[共產主義的思想](../Page/共產主義.md "wikilink")，對日後的人類社會影響甚鉅，有正面也有负面。不过也由于工业革命，才可能产生大量的工人，而[农业社会中则不会出现这种情况](../Page/农业社会.md "wikilink")。工人阶级在[欧洲一些国家如](../Page/欧洲.md "wikilink")[英国](../Page/英国.md "wikilink")，通过与资方的有限的斗争以及恰当的妥协，而不是马克思主义学说鼓励的彻底斗争与破坏，为自己争取到了更多的利益，同时对社会经济的破坏也不那么大。改善了自己的生活环境，不讓物質的大量富餘和人民的相對貧困，使社會矛盾加劇，革命思潮洶湧，英國等國通過改革實行“[民主](../Page/民主.md "wikilink")”，后来欧洲社会的稳定进步。

工业革命时期严酷的经济环境造成了当时人类平均[身高的降低](../Page/身高.md "wikilink")，人类的平均身高并非是一成不变的，科学界普遍认为6000年前[农业社会的形成与](../Page/农业社会.md "wikilink")[渔猎的生活方式被](../Page/渔猎.md "wikilink")[农耕取代](../Page/农耕.md "wikilink")，造成了人类平均身高的下降，[土耳其境内](../Page/土耳其.md "wikilink")8000年前遗迹所留下的尸骨平均身高超过今日土耳其的平均身高。\[16\]英国[牛津大学里查德教授在分析了几千具从](../Page/牛津大学.md "wikilink")[丹麦](../Page/丹麦.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[英国和](../Page/英国.md "wikilink")[冰岛挖掘出来的古代人类男性的遗骨后发现](../Page/冰岛.md "wikilink")，人类成年男性平均身高在[中世纪期间的公元](../Page/中世纪.md "wikilink")9世纪到12世纪之间到达了一个自10万年前现代[智人诞生以来到](../Page/智人.md "wikilink")20世纪中期前的平均最大值，平均身高为1.73米，然后逐渐变矮变低，到了[工业革命前夕的](../Page/工业革命.md "wikilink")18世纪和19世纪，欧洲成年男性平均身高降低到1.67米，比9世纪到12世纪间减小了6厘米之多。一直到了20世纪中期，人类男性平均身高才重新恢复达到9世纪到12世纪的最大值。[罗马帝国时期的遗址](../Page/罗马帝国.md "wikilink")[赫库兰尼姆城](../Page/赫库兰尼姆.md "wikilink")（在今土耳其境内）前后共出土了近二百余具遗骸，美国[史密森大学物理考古学家](../Page/史密森大学.md "wikilink")[萨拉·比西尔对他们进行了深入细致的研究](../Page/萨拉·比西尔.md "wikilink")，了解到古罗马男子一般身高一点七米，女子一点五五米”
\[17\]
从1920年开始，特别是从20世纪中期以后，人类平均身高迅速增长，欧洲成年男子平均身高从167厘米增长到177厘米，其中比利时成年男子的平均身高从1920年的166厘米增长到1970年的174厘米，丹麦则从169厘米增加到178厘米。\[18\]　

## 参见

  - [德国产业革命](../Page/德国产业革命.md "wikilink")
  - [英国产业革命](../Page/英国产业革命.md "wikilink")
  - [日本产业革命](../Page/日本产业革命.md "wikilink")
  - [美国产业革命](../Page/美国产业革命.md "wikilink")
  - [農業革命](../Page/農業革命.md "wikilink")
  - [信息革命](../Page/信息革命.md "wikilink")
  - [後工業社會](../Page/後工業社會.md "wikilink")
  - [第二次工业革命](../Page/第二次工业革命.md "wikilink")
  - [第三次工業革命](../Page/第三次工業革命.md "wikilink")
  - [工業4.0](../Page/工業4.0.md "wikilink")

## 參考資料及註釋

### 其他參考文獻

<div class="references-small">

  - [法保尔](../Page/法.md "wikilink")·芒图：《十八世纪产业革命》，[商务印书馆](../Page/商务印书馆.md "wikilink")，1997年9月
  - [美戴维](../Page/美.md "wikilink")·罗伯兹：《英国史：1688年至今》，[中山大学出版社](../Page/中山大学出版社.md "wikilink")，1990年9月
  - 杨小凯：《[为什么工业革命在英国而不在西班牙发生？](http://www.douban.com/group/topic/1115844/)》
  - 刘淑敏：《工业革命为什么首先发生在英国》，载《齐鲁学刊》，1984年第5期
  - 扬豫：《英国近代工业化的道路》，载《英国政治经济和社会现代化》，南京大学出版社1989年版
  - 辛淑玲：《浅谈社会观念的变化对工业革命的影响》，载《东北师范大学学报》，1988年第2期
  - 陈晓律：《试论英国工业民族精神形成的社会历史条件》，载《南京大学学报》，1991年第4期
  - 赵松鹏：《农业革命与产业革命的开始》，载《内蒙古大学学报》，1987年第4期
  - 许洁明：《工业文明为什么起源于英国》，载《世界历史》，1993年第2期
  - 王章辉：《试从比较研究的角度看工业革命研究中的几个问题》，载《史学理论研究》，1997年第2期
  - 王志乐：《“产业革命”和“工业革命”的含义和译法》，载《东北师范大学学报》，1981年第4期
  - 许永璋：《有关工业革命史研究的几个问题》，载《河南大学学报》，1988年第4期
  - 王章辉：《英国工业革命中的人口问题》，载《世界历史》，1986年第4期
  - 项翔等：《20世纪中国的世界史研究》，载《学术月刊》，1999年7—8期（连载）
  - 《中外近代历史上的改革》，中央党校出版社1991年版，第126—129页
  - 屈连壁主编：《世界近代史》，北京师范大学出版社1990年版，第262—272页
  - 周呈芳：《论工业革命的社会后果》，载《内蒙古大学学报》，1989年第1期
  - 孙炳辉：《工业革命与资产阶级革命》，载《华东师范大学学报》，1981年第4期
  - 孔繁刚：《关于英国工业革命的几个问题》，载《历史教学》，1998年第8期
  - 王章辉：《欧美大国工业革命对世界历史进程的影响》，载《世界历史》1994年第5期
  - 王觉非：《对英国工业革命历史意义的再认识》，载《南京大学学报》，1986年第2期
  - 王曾才：《西洋近代史》(大學館9)，正中書局出版，第2章 第33-67頁

</div>

## 外部連結

  - [工业革命](https://web.archive.org/web/20040105211206/http://www.nhyz.org/zdk/new_page_33001.htm)存於[網際網路檔案館上的資料](../Page/網際網路檔案館.md "wikilink")（2004年1月5日存檔）

[工业革命](../Category/工业革命.md "wikilink")
[Category:技術史](../Category/技術史.md "wikilink")
[Category:社会文化进化](../Category/社会文化进化.md "wikilink")
[Category:历史理论](../Category/历史理论.md "wikilink")

1.  Julian Hoppit, "The Nation, the State, and the First Industrial
    Revolution," *Journal of British Studies* (April 2011) 50\#2
    pp. 307–31

2.
3.
4.
5.
6.
7.
8.  註釋：此處指的新航路是指由[狄亞士](../Page/巴爾托洛梅烏·迪亞士.md "wikilink")、[哥倫布](../Page/哥倫布.md "wikilink")、[達伽馬和](../Page/達伽馬.md "wikilink")[麥哲倫等航海家發現通往東方及](../Page/麥哲倫.md "wikilink")[美洲的航線](../Page/美洲.md "wikilink")

9.  from John Hicks，1969

10.

11.

12.

13.

14.

15.

16. [（美国）格里高利·克拉克：应该读点经济史](http://book.forex.com.cn/html/357/8873.html)

17. [是谁造谣说罗马人个子矮的？](http://tieba.baidu.com/p/896201343)

18. [工业革命后的19世纪是人类身高最矮时期](http://zlk.wyzxsx.com/e/DoPrint/index.php?classid=27&id=233882)