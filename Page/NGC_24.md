**NGC
24**是[玉夫座的一個](../Page/玉夫座.md "wikilink")[漩渦星系](../Page/漩渦星系.md "wikilink")。[星等為](../Page/星等.md "wikilink")11.5，[赤經為](../Page/赤經.md "wikilink")9分56.1秒，[赤緯為](../Page/赤緯.md "wikilink")-24°57'52"。在1785年10月27日首次被[弗里德里希·威廉·赫歇爾發現](../Page/弗里德里希·威廉·赫歇爾.md "wikilink")。該星系直徑約4萬光年。

## 圖集

[File:NGC_24.png|哈伯太空望遠鏡拍攝可見光與近紅外線影像](File:NGC_24.png%7C哈伯太空望遠鏡拍攝可見光與近紅外線影像)
<File:NGC> 0024
GALEX.jpg|[星系演化探测器](../Page/星系演化探测器.md "wikilink")（GALEX）拍攝紫外線影像
<File:NGC> 0024 2MASS.jpg|[2MASS拍攝近紅外線影像](../Page/2微米全天巡天.md "wikilink")
<File:NGC_0024SST.jpg>|[史匹哲太空望遠鏡拍攝紅外線影像](../Page/史匹哲太空望遠鏡.md "wikilink")

## 參見

  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 參考資料

## 外部連結

  -
[24](../Category/玉夫座NGC天体.md "wikilink")
[701](../Category/PGC天體.md "wikilink")
[Category:無棒螺旋星系](../Category/無棒螺旋星系.md "wikilink")