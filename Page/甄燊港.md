**甄燊港**（**Thomas Yan Sun
Kong**）原籍[廣東](../Page/廣東.md "wikilink")[臺山](../Page/臺山.md "wikilink")，[香港國情專家](../Page/香港.md "wikilink")、傳媒人及政治人物，香港政黨[人民力量前副主席兼前成員](../Page/人民力量.md "wikilink")、政治組織[前綫召集人](../Page/前綫_\(政黨\).md "wikilink")。

[The_Frontier_2006_07-01.jpg](https://zh.wikipedia.org/wiki/File:The_Frontier_2006_07-01.jpg "fig:The_Frontier_2006_07-01.jpg")。甄燊港，[劉慧卿](../Page/劉慧卿.md "wikilink")。\]\]
[The_Frontier_2012-07-15_IMG_0716.JPG](https://zh.wikipedia.org/wiki/File:The_Frontier_2012-07-15_IMG_0716.JPG "fig:The_Frontier_2012-07-15_IMG_0716.JPG")召集人、[人民力量副主席甄燊港](../Page/人民力量.md "wikilink")，站台支持「慢必」[陳志全出選新界東立法會選舉](../Page/陳志全.md "wikilink")。2012年7月15日，大埔。\]\]

## 生平

甄燊港畢業於[國立台灣大學政治系](../Page/國立台灣大學.md "wikilink")，1974年底回港，1975年入職[商業電台新聞部任採訪](../Page/商業電台.md "wikilink")[記者](../Page/記者.md "wikilink")，1976年擢升至管理層，負責管理新聞部日常工作。他亦曾任職週刊管理層。\[1\]

甄燊港是首位華人出任[香港記者協會主席](../Page/香港記者協會.md "wikilink")（1978-1979）。

甄燊港雖常批評[民主黨](../Page/民主黨_\(香港\).md "wikilink")，但整體與[泛民主派友善](../Page/泛民主派.md "wikilink")，曾是[民主倒董力量](../Page/民主倒董力量.md "wikilink")（2003年）的執委。

他曾支持[柳玉成於](../Page/柳玉成.md "wikilink")2004年出選[香港立法會](../Page/香港立法會.md "wikilink")，也曾為南方民主同盟兩週年紀念贈辭，亦曾到[龍緯汶主持的網上電台節目接受訪問](../Page/龍緯汶.md "wikilink")。

2008年，[前綫主要成員劉慧卿等投靠民主黨](../Page/前綫_\(1996-2009\).md "wikilink")，甄燊港及部分其他成員堅持留下來，重組[前綫](../Page/前綫_\(政黨\).md "wikilink")。

2011年4月開始，甄燊港和歷蘇主持[香港人網節目](../Page/香港人網.md "wikilink")〈國情燊知〉。\[2\]

2013年10月23日開始，甄燊港和蘇浩主持[謎米香港節目](../Page/謎米香港.md "wikilink")〈國情揭露〉。\[3\]

## 個人生活

甄燊港太太在葛亮洪醫院擔任行政工作，2016年2月，甄太的平治房車在將軍澳馬游塘村被人淋潑紅油及起漆水，\[4\]

## 参考資料

<references />

[S](../Page/category:甄姓.md "wikilink")

[Category:香港政治人物](../Category/香港政治人物.md "wikilink")
[Category:香港傳媒工作者](../Category/香港傳媒工作者.md "wikilink")
[Category:香港记者](../Category/香港记者.md "wikilink")
[Category:前綫成員](../Category/前綫成員.md "wikilink")
[Category:人民力量前成員](../Category/人民力量前成員.md "wikilink")
[Category:國立臺灣大學校友](../Category/國立臺灣大學校友.md "wikilink")

1.  ，[陳偉業](../Page/陳偉業.md "wikilink")、甄燊港：[MyRadio](../Page/MyRadio.md "wikilink")《毓民踩場》，第256集，2010月12月16日。
2.  [國情燊知](http://www.hkreporter.com/channel_list.php?channelid=325)
3.  [國情揭露](http://memehk.com/index.php?page=program&id=38)
4.