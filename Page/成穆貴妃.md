**成穆贵妃**（）孙氏，真名失考，中國[明朝女性](../Page/明朝.md "wikilink")[皇族](../Page/皇族.md "wikilink")，為明太祖[朱元璋](../Page/朱元璋.md "wikilink")[贵妃](../Page/贵妃.md "wikilink")。孫氏出身[陈州](../Page/陈州.md "wikilink")，父[孫和卿](../Page/孫和卿.md "wikilink")，義父[馬世熊](../Page/馬世熊.md "wikilink")，母晁氏，有兩個兄長[孫瑛](../Page/孫瑛.md "wikilink")、[孫藩](../Page/孫藩.md "wikilink")，孫氏生有四女：[临安公主](../Page/临安公主.md "wikilink")、[怀庆公主和兩位早殇的公主](../Page/怀庆公主.md "wikilink")。

## 生平

孫氏的父親孫和卿曾在元朝任官，並舉家從陳州遷至[常州](../Page/常州.md "wikilink")\[1\]。孫氏的父母在[元朝末年的戰亂中身亡](../Page/元朝.md "wikilink")，长兄孫瑛有才幹，辞家远游，久久未归\[2\]；孫氏十三歲時跟随次兄孙蕃於[江都製作鼙鼓](../Page/江都.md "wikilink")\[3\]、或於[揚州躲避戰亂](../Page/揚州.md "wikilink")\[4\]，後來被元帅马世熊收為义女。孫氏十八岁时因美貌出眾，被尚在紅巾軍中的朱元璋纳為妾室\[5\]\[6\]。朱元璋登基建立明朝後，冊封孫氏为贵妃，地位在後宮眾妃之上\[7\]。

孫貴妃傷感自己的父母雙亡、兄長亦不知所蹤，因此向朱元璋請求尋訪兄長，朱元璋便命人尋找，過了很久之後才尋訪到孫瑛，朱元璋授予孫瑛[參省的官職](../Page/參省.md "wikilink")\[8\]。孫貴妃除了貌美，還嫻於禮法，舉止規範，協助[马皇后管理後宮](../Page/孝慈高皇后_\(明朝\).md "wikilink")：當時馬皇后慈善地管理後宮之事，孫貴妃則以法制協助馬皇后，史書稱：「高-{后}-（馬皇后）以慈，妃（孫貴妃）以法，皆相濟得治。」\[9\]。為此馬皇后還曾向朱元璋称赞孙貴妃是少有的贤女\[10\]。

孫貴妃於洪武七年（[1374年](../Page/1374年.md "wikilink")）九月逝世，[諡號](../Page/諡號.md "wikilink")**成穆貴妃**，享年三十二歲\[11\]。朱元璋相當哀悼孫貴妃之死\[12\]，且由於孫貴妃沒有兒子，因此朱元璋命周王[朱橚為孙贵妃的义子](../Page/朱橚.md "wikilink")，由朱橚為孫貴妃服孝三年，皇太子及诸皇子也要服喪\[13\]\[14\]。朱元璋又命儒臣作《[孝慈录](../Page/孝慈录.md "wikilink")》。從此開始，庶子除了为生母服喪三年，其餘的兒子們也要為庶母服喪\[15\]\[16\]。孫貴妃初葬朝陽門的[褚冈之原](../Page/褚冈.md "wikilink")，後來入葬[孝陵](../Page/明孝陵.md "wikilink")\[17\]\[18\]。孫貴妃死後，朱元璋又赐孙瑛田租三百石，每年供礼\[19\]\[20\]。

## 参考文献

<div class="references-small">

<references />

</div>

[S](../Category/明太祖贵妃.md "wikilink") [S](../Category/河南人.md "wikilink")
[\~](../Category/孙姓.md "wikilink")

1.  《[勝朝彤史拾遺記](../Page/勝朝彤史拾遺記.md "wikilink")》：貴妃孫氏，陳州人，父和卿，以仕元偕妻晁氏，至常州家焉。
2.  《勝朝彤史拾遺記》：元末兵亂，妃父母相繼死，獨長兄瑛有材幹，辭家遠游，久未歸。
3.  《勝朝彤史拾遺記》：妃年十三，隨其次兄冶鼙江都。
4.  《[明史](../Page/明史.md "wikilink")》（卷113）：成穆贵妃孙氏......元末兵乱......从仲兄蕃避兵扬州。
5.  《明史》（卷113）：年十八，太祖纳焉。
6.  《勝朝彤史拾遺記》：年十八，未字也。上求有容德者納宮中，人或以妃告，及按果然，遂納之。
7.  《勝朝彤史拾遺記》：上即位，冊貴妃，位冠諸妃上。
8.  《勝朝彤史拾遺記》：妃痛無外家，間一請上，求兄瑛所在。久之，得瑛，官參省。
9.  出自《勝朝彤史拾遺記》
10. 《勝朝彤史拾遺記》：妃敏慧端麗而嫻禮法，言動皆中矩。高-{后}-嘗謂上曰：「古賢女也。」
11. 《明史》（卷113）：洪武七年九月薨，年三十有二
12. 《勝朝彤史拾遺記》：洪武七年九月癸未一作庚寅死，年三十二。上震悼，賜謚成穆。
13. 《明史》（卷113）：帝以妃无子，命周王橚行慈母服三年，东宫、诸王皆期。
14. 《勝朝彤史拾遺記》：妃無子，只生四女。上命周王肅主其喪，服慈母三年，皇太子諸王皆期
15. 《明史》（卷113）：敕儒臣作《孝慈录》。庶子为生母服三年，众子为庶母期，自妃始。
16. 《勝朝彤史拾遺記》：並敕詞臣撰《孝慈錄》。凡庶子為生母服三年，眾子為庶母皆期，推妃恩也。
17. 《明史》（卷113）：葬褚冈......後附葬孝陵。
18. 《勝朝彤史拾遺記》：時命有司營厝於朝陽門外，褚岡之原
19. 《明史》（卷113）：赐兄瑛田租三百石，岁供礼。
20. 《勝朝彤史拾遺記》：賜兄瑛田租以供歲祀