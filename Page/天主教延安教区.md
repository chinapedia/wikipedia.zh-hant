**天主教延安教区**（**Dioecesis
Iennganensis**）是[罗马天主教在中国](../Page/罗马天主教.md "wikilink")[陕西省设立的一个](../Page/陕西省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1911年4月12日，陕西北境代牧区分为陕西中境代牧区和陕西北境代牧区，陕西北境代牧区神父都为[西班牙籍](../Page/西班牙.md "wikilink")[方济会士](../Page/方济会.md "wikilink")，第一任主教是西班牙人[易兴化](../Page/易兴化.md "wikilink")，来华后先在[山东传教](../Page/山东.md "wikilink")。当时陕西北境代牧区没有一座教堂，于是易兴化主教回国募捐，3年后回到陕北，选定[延安作为主教驻地](../Page/延安.md "wikilink")，购得东郊桥儿沟村[延长石油厂旧址](../Page/延长石油厂.md "wikilink")70多[亩土地](../Page/亩.md "wikilink")。1924年12月3日，陕西北境代牧区更名为延安府代牧区。到1934年，教区初具规模，有大小教堂20多处，于是兴建[主教座堂](../Page/主教座堂.md "wikilink")。尚在内部装修阶段，陕北爆发土地革命，传教工作无法进行。主教及大部分神职人员退出延安，分别到[山东](../Page/山东.md "wikilink")、[武汉](../Page/武汉.md "wikilink")、[山西和](../Page/山西.md "wikilink")[关中等教区协助传教](../Page/关中.md "wikilink")，只有[榆林](../Page/榆林.md "wikilink")、[富县和](../Page/富县.md "wikilink")[宜君还有几位神父住守](../Page/宜君.md "wikilink")，主要的传教活动基本停止。

1946年4月11日更名为延安教区。

## 教堂

  - [靖边县河东天主堂](../Page/靖边县河东天主堂.md "wikilink")

## 历任主教

  - 陝西宗座代牧
      - 主教[林奇愛](../Page/林奇愛.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Pasquale Pagnucci, O.F.M. （1884年4月12日-1887年8月2日）
      - 主教[魏明德](../Page/魏明德.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Pio Vidi, O.F.M. （1886年8月24日-1887年8月2日）

<!-- end list -->

  - 陝西北境宗座代牧
      - 主教[魏明德](../Page/魏明德.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Pio Vidi, O.F.M. （1887年8月2日-1900年4月19日）
      - 主教[林奇愛](../Page/林奇愛.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Pasquale Pagnucci, O.F.M. （1887年8月2日-1901年1月29日）
      - 主教[郭德禮](../Page/郭德禮.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Clemente Coltelli, O.F.M. （1900年4月19日-1901年1月28日）
      - 主教[穆理思](../Page/穆理思.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Auguste-Jean-Gabriel Maurice, O.F.M.
        (1908年8月1日-1911年4月12日）
      - 主教[易兴化](../Page/易兴化.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Celestino Ibáñez y Aparicio, O.F.M.
        (1911年4月12日-1934年12月3日）

<!-- end list -->

  - 延安府宗座代牧
      - 主教[易兴化](../Page/易兴化.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Celestino Ibáñez y Aparicio, O.F.M.
        (1934年12月3日-1946年4月11日）

<!-- end list -->

  - 延安主教
      - 主教[易兴化](../Page/易兴化.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Celestino Ibáñez y Aparicio, O.F.M.
        (1946年4月11日-1949年1月13日）
      - 主教[李宣德](../Page/李宣德.md "wikilink")，[方济各会](../Page/方济各会.md "wikilink")
        Bishop Pacific Li Huan-de, O.F.M.（1951年12月13日-1973年）

<!-- end list -->

  -   - 主教[王振业](../Page/王振业.md "wikilink")（1991年-1999年12月27日）
      - 主教[童辉](../Page/童辉_\(主教\).md "wikilink")（1999年12月28日-2011年3月24日）
      - 主教[杨晓亭](../Page/杨晓亭.md "wikilink")（2011年3月25日-）

### 主教简介

  - [易兴化](../Page/易兴化.md "wikilink")（Bishop Celestino Ibáñez y Aparicio,
    O.F.M.）1873年生于西班牙，1911年4月12日任职，1911年9月10日在济南祝圣，1946年回国
    ，1949年退休，1951年逝世
  - [李宣德](../Page/李宣德.md "wikilink") 1904年.03.01
    生于陕西省高陵县，1951年12月13任延安教区主教，但无法上任，1952年兼任西安总教区宗座署理，1953年祝圣，1972年去世
  - [王振业](../Page/王振业.md "wikilink") 陕西省子洲县人 ，1991年祝圣 1999年逝世。
  - [童辉](../Page/童辉_\(主教\).md "wikilink")
    1934年生于陕西省临潼县，1994年3月19日祝圣为延安教区助理主教，1999年12月28日任教区正权主教。
  - [杨晓亭](../Page/杨晓亭.md "wikilink") (Bishop John Baptist Yang Xiaoting)
    1964年4月9日生于陕西周至县二曲镇渭泉村。1991年8月28日由盩厔教区[范宇飞主教在大营天主堂祝圣为神父](../Page/范宇飞.md "wikilink")。1993年10月去意大利深造，1999年获得神学博士学位。2009年7月9日当选为助理主教。2010年7月15日由汉中教区[余润深主教祝圣](../Page/余润深.md "wikilink")，2011年3月25日升任[正权主教](../Page/正权主教.md "wikilink")。

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  - [延安教区童辉主教金庆大典](http://www.cncatholic.org/zxpd/2233477667.html)
  - [桥儿沟天主教堂的红色足迹](http://www.mzb.com.cn/onews.asp?id=28231)
  - [瞰看西北崛起的苦难教会－延安教区](http://www.cncatholic.org/zxpd/2238575721.html)

{{-}}

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:陕西天主教](../Category/陕西天主教.md "wikilink")
[Category:延安宗教](../Category/延安宗教.md "wikilink")