[PPAPER_logo_first.jpg](https://zh.wikipedia.org/wiki/File:PPAPER_logo_first.jpg "fig:PPAPER_logo_first.jpg")
[PPAPER_logo_第二版.jpg](https://zh.wikipedia.org/wiki/File:PPAPER_logo_第二版.jpg "fig:PPAPER_logo_第二版.jpg")
《**PPAPER**》為台灣一本設計學半月刊雜誌，於2004年12月15日創刊，由包益民創辦（前發行人）。現任發行人為[胡至宜](../Page/胡至宜.md "wikilink")，總編輯為簡兢谷。
**PPAPER**目前為月刊，每月5日出刊，不定時發行特刊。

## 內容

以創意導向發展新媒體─ppaper創意月刊。根據統計，約有50,000人就讀於設計相關的科系，另外約有50,000人從事創意相關產業，PPAPER明確地和這100,000位意見領袖對話，並建立每個月二次的深度溝通。ppaper提供國內設計相關人士們設計理論及設計類相關報導等，冀望提供精簡實用的設計觀點和創意啟發。2004年9月將在全省7-11、[誠品及](../Page/誠品書局.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")、[香港各大書店販售](../Page/香港.md "wikilink")。

## 參見

以下為PPAPER發行的其他設計類雜誌

  - [PPAPER BUSINESS商業雜誌](../Page/PPAPER_BUSINESS商業雜誌.md "wikilink")
  - [PPAPER FASHION時尚雜誌](../Page/PPAPER_FASHION時尚雜誌.md "wikilink")
  - [AANGEL](../Page/AANGEL.md "wikilink")
  - [SHOP](../Page/SHOP.md "wikilink")
  - [PPAPER MAN](../Page/PPAPER_MAN.md "wikilink")

## 外部連結

  - [ppaper](http://www.ppaper.net)

[Category:臺灣雜誌](../Category/臺灣雜誌.md "wikilink")