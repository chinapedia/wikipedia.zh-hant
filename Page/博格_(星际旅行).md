**博格人**（）是《[星际旅行](../Page/星际旅行.md "wikilink")》虚构的一個[宇宙](../Page/宇宙.md "wikilink")[种族](../Page/种族.md "wikilink")，星际旅行系列中最大的[反派](../Page/反派.md "wikilink")。
[Logo_Borg.svg](https://zh.wikipedia.org/wiki/File:Logo_Borg.svg "fig:Logo_Borg.svg")
[Picard_as_Locutus.jpg](https://zh.wikipedia.org/wiki/File:Picard_as_Locutus.jpg "fig:Picard_as_Locutus.jpg")舰长\]\]

## 概说

博格人生活在[银河系的](../Page/银河系.md "wikilink")[第四象限](../Page/银河系象限.md "wikilink")\[1\]，是半有机物半机械的[生化人](../Page/生化人.md "wikilink")。博格个体的身体上装配有大量人造器官及机械，大脑为人造的处理器。

### 社会与生理

博格人是严格奉行[集体意识的种族](../Page/集体意识.md "wikilink")，从生理上完全剥夺了个体的[自由意识](../Page/自由意志.md "wikilink")。博格人的社会系统由“博格集合体”组成，每个集合体中的个体成员被称为“Drone”。集合体内的博格个体通过某种复杂的[子空间通信网络相互连接](../Page/子空间.md "wikilink")。在博格集合体中，博格个体没有自我意识，而是通过一个被称为博格女皇（Borg
Queen）的[程序对整个集合体进行控制](../Page/程序.md "wikilink")。因此在战略上显示出高度的智慧与适应能力。博格女皇安装在一个独特的博格个体上，也是集合体内唯一一个与其他个体不同并拥有自我意识的个体。

博格人没有体毛。无论之前的种族如何，在同化之後皮肤均是灰色。一只眼睛被调换成电子眼。另一只胳膊也被截肢更换成机械臂。纳米探针的注射机构安装在机械臂上。博格人的生存不需要食物、水甚至空气。但是他们会将生存环境改造为39.1℃、92%[湿度](../Page/湿度.md "wikilink")，以及102千帕的[大气压](../Page/大气压.md "wikilink")。据推测这有利于博格的子空间通信系统工作。

博格人依然需要有机的生理结构来维持身体运作。因此摧毁一个博格人的有机结构也可以有效的将这个博格个体消灭。如果被消灭的个体是博格女皇，那么整个博格集合体都会因此而停止运作。不过如果这个博格集合体没有与其他的博格集合体失去联系，那么被摧毁的博格女皇程序依然会上传到其他博格集合体中，并不会因此被真正消灭。

### 同化

博格个体可以将一种[纳米探针植入其他种族个体的体内](../Page/纳米探针.md "wikilink")，并在纳米探针的作用下从分子层上将对方改造为博格个体，以实现[同化的目的](../Page/同化_\(星际旅行\).md "wikilink")。经过同化的个体会被安装上博格的机械组件，并安装上不可移除的子空间通信设备，从而完全加入博格集合体。同化是博格人唯一获取知识与技术的方式\[2\]。博格人会忽略一般经过他们的外星种族和飞船，除非他们表现出了威胁或者值得被同化。

博格人不能[繁殖](../Page/繁殖.md "wikilink")。因此同化是他们维持种族存续的唯一方式。

博格人的最终目标是通过同化一切可以遇到的种族，使自己的种族达到最完美。\[3\]

## 与星际联邦的接触

### 下一代

[星际联邦与博格人的第一次接触发生在](../Page/星际联邦.md "wikilink")2365年，当时[企業號-D被](../Page/聯邦星艦企業號_\(NCC-1701-D\).md "wikilink")[Q推到了第四象限的一艘](../Page/Q连续体.md "wikilink")[博格方块旁边](../Page/博格方块.md "wikilink")。\[4\]2366年博格人第一次入侵联邦，并在[沃夫359战役中摧毁了](../Page/沃夫359战役.md "wikilink")39艘联邦星舰，后被企業號-D阻止。\[5\]2373年博格人又对联邦发起了进攻，并通过[时间旅行回到了](../Page/时间旅行.md "wikilink")21世纪中期企图同化当时的地球，被企業號-E阻止\[6\]。

### 航海家号

博格人在同化[8472種族失败并遭到对方反击的时候](../Page/8472種族.md "wikilink")，遇到了迷航于[第四象限的](../Page/第四象限.md "wikilink")[联邦星舰航海家号](../Page/联邦星舰航海家号.md "wikilink")。通过将一个博格个体[九之七加入航海家号的船员中](../Page/九之七.md "wikilink")，博格人得到了航海家号的帮助，免于遭到灭族危机。但随后，九之七试图将航海家号的船员同化为博格人，结果为航海家号的船员所制止，并将她重新改造为[人类](../Page/人类.md "wikilink")。

此后，又有多个博格集合体与航海家号遭遇，并爆发了一些冲突。直到最后，航海家号利用並同時摧毀了一處博格人的[超曲速中繼站](../Page/超曲速中繼站.md "wikilink")，從第四象限返回第一象限。

### 进取号

第一类接触的剧情中，有两名博格个体随[博格球体坠落到](../Page/博格球体.md "wikilink")21世纪的地球北极而幸存下来。在2153年3月，这两名博格个体被一群研究人员解冻后被纳米探针修复。而后在逃离地球的过程中被[地球星舰企業號（NX-01）阻止](../Page/地球星艦企業號_\(NX-01\).md "wikilink")。\[7\]

### 平行宇宙

在平行宇宙中，博格人最终征服了[星际联邦](../Page/星际联邦.md "wikilink")。仅剩下[进取号-E在内的几艘星舰幸存](../Page/聯邦星艦企業號_\(NCC-1701-E\).md "wikilink")。

## 博格人的戰鬥特色

### 科技优势

博格人作戰有兩大特色武器，一個是可以穿透任何防護力場的奈米探針，另外一個則是能修正適應敵人攻擊的防護罩，博格人的透過奈米探針同化目標來取得目標的所有資訊，防護罩在受攻擊傷害時會同時同化敵方攻擊的能量來取得目標能量的資訊藉以適應，一旦適應了對方武器的資訊，則就會完全不受到傷害，星聯能對付博格的方法只能靠不斷變化武器頻率來消極的持續傷害敵人，或是生機型態病毒來感染攻擊。

藉由同化能力，博格人在作戰往往能取得絕對的優勢，但相對的如果無法同化對方，則博格人對敵人會一無所知無法有效攻擊和防禦，在[星艦奇航記：重返地球年代博格人在攻擊](../Page/星艦奇航記：重返地球.md "wikilink")[8472種族就踢到鐵板](../Page/8472種族.md "wikilink")，[8472種族本身基因具有最複雜的排列組合和侵蝕能力能抵抗博格人奈米探針的同化並同時吞噬奈米同化細胞](../Page/8472種族.md "wikilink")，加上[8472種族的船艦和武器都是身體組織的一部分](../Page/8472種族.md "wikilink")，博格人也無法對此產生適應，因此博格人慘遭[8472種族的報復攻擊](../Page/8472種族.md "wikilink")，差點導致滅族危機。

博格人的另一大优势是他们的超曲速技术，博格人使用的超曲速导管（transwarp
conduits）来调动他们的舰队，而导管由超曲速中继站（transwarp
hub）来相互连接，从而形成一个巨大的网络通往银河系的各个角落。使用超曲速导管的飞船的航速是[银河级星舰最高曲速速度的二十倍](../Page/银河级星舰.md "wikilink")，这使得博格人在战场上拥有强大机动能力并可在战略战术上达成突然性。[航海家号就是利用超曲速中继站在短时间内穿过了整个象限返回了地球](../Page/联邦星舰航海家号.md "wikilink")。

[LocutusBorgQueen.jpg](https://zh.wikipedia.org/wiki/File:LocutusBorgQueen.jpg "fig:LocutusBorgQueen.jpg")
在与其他种族接触的时候，博格有时会将某个被俘获的对方种族个体同化为一种特殊的形态“代言人”（Locutus，罗丘特斯），用来劝降仍然在抵抗的对方，或者在以后留做沟通的渠道（如皮卡尔舰长）。经如此改造的博格个体依然保留有很多原本的器官，因此通过手术还是可以恢复原本的生理状态。

### 反抗无用

博格人在与其他种族接触并交手之前，會使用对方的语言发表准备同化对方的宣言。

这句话原出自《[星际旅行：下一代](../Page/星际旅行：下一代.md "wikilink")》中[博格人出场的一集](../Page/博格人.md "wikilink")。在这一集中，本片的主角[皮卡尔船长被博格人同化为罗丘特斯用以和地球人交涉](../Page/让-吕克·皮卡尔.md "wikilink")。在皮卡尔船长出场时有一句台词“I
am Locutus of Borg.Resistance is
futile.(我是博格人罗丘特斯，反抗无用。)”。之后在[1996年电影](../Page/1996年电影.md "wikilink")《[星际旅行：第一类接触](../Page/星际旅行：第一类接触.md "wikilink")》中，这个桥段再次出现，并且成为本片的宣传语。

这里面的最后一句“反抗无用”（Resistance is
futile），成为博格人的标志之一，并经常于其他地方使用或被[恶搞](../Page/恶搞.md "wikilink")。

这句话曾经被评为最伟大的100句电视流行语（The 100 Greatest TV Quotes and
Catchphrases）之一。\[8\]

不过早在[星际旅行之前](../Page/星际旅行.md "wikilink")，英国电视剧《[神秘博士](../Page/神秘博士.md "wikilink")》1976年的一集中就出现了这句台词，只是并没有获得注意。\[9\]此外还有另一剧集中曾出现类似的台词“Resistance
is useless”\[10\]

  - 大眾文化

<!-- end list -->

  - 2007年剧集《[双面法医](../Page/双面法医.md "wikilink")》第二季的第九集名为“Resistance Is
    Futile”\[11\]

## 出场

  - [星际旅行：下一代](../Page/星际旅行：下一代.md "wikilink")
  - [星际旅行：航海家号](../Page/星际旅行：航海家号.md "wikilink")
  - [星际旅行：深空九号](../Page/星际旅行：深空九号.md "wikilink")
  - [星际旅行：进取号](../Page/星际旅行：进取号.md "wikilink")
  - [星际旅行：第一次接触](../Page/星艦奇航記VIII：戰鬥巡航.md "wikilink")

## 参见

  - [赛博格](../Page/赛博格.md "wikilink")（cyborg，机械化有机体，生化人）
  - [Resistance is futile](../Page/Resistance_is_futile.md "wikilink")

## 外部链接

  - [Borg](http://memory-alpha.org/wiki/Borg)--[阿尔法记忆中关于博格人的介绍](../Page/阿尔法记忆.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[de:Völker und Gruppierungen im
Star-Trek-Universum\#Borg](../Page/de:Völker_und_Gruppierungen_im_Star-Trek-Universum#Borg.md "wikilink")
[sv:Lista över utomjordiska raser i Star
Trek\#Borgerna](../Page/sv:Lista_över_utomjordiska_raser_i_Star_Trek#Borgerna.md "wikilink")

[Category:星际旅行种族](../Category/星际旅行种族.md "wikilink")
[Category:虛構賽博格](../Category/虛構賽博格.md "wikilink")
[Category:虚构超个体](../Category/虚构超个体.md "wikilink")

1.  [Star Trek Borg](http://www.startrek.com/database_article/borg)

2.  《[蝎与狐](../Page/蝎与狐_\(航海家号剧情\).md "wikilink")》（**），[官网上的《Scorpion,
    Part
    I》剧情简介](http://www.startrek.com/startrek/view/series/VOY/episode/69018.html)，[官网上的《Scorpion,
    Part
    II》剧情简介](http://www.startrek.com/startrek/view/series/VOY/episode/69024.html)

3.
4.  **，[官网上的《Q
    Who?》剧情简介](http://www.startrek.com/startrek/view/series/TNG/episode/68390.html)

5.  《[地球保卫战](../Page/地球保卫战_\(下一代剧情\).md "wikilink")》（**），[官网上的《The Best
    of Both Worlds, Part
    I》剧情简介](http://www.startrek.com/startrek/view/series/TNG/episode/68454.html)，[官网上的《The
    Best of Both Worlds, Part
    II》剧情简介](http://www.startrek.com/startrek/view/series/TNG/episode/68456.html)

6.  [星际旅行VIII：第一次接触](../Page/星际旅行VIII：第一类接触.md "wikilink")，[官方网站上的剧情介绍](http://www.startrek.com/startrek/view/series/MOV/008/synopsis.html)

7.  《[重生](../Page/重生_\(进取号剧情\).md "wikilink")》（**，[官网上的《Regeneration》剧情简介](http://www.startrek.com/startrek/view/series/ENT/episode/128643.html)

8.

9.

10.

11. [Dexter on IMDB](http://www.imdb.com/title/tt1013988/)