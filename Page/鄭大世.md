**郑大世**（[朝鮮文](../Page/朝鮮文.md "wikilink")：**정대세**；[英文](../Page/英文.md "wikilink")：**Jong
Tae-Se**；），出生于[日本的](../Page/日本.md "wikilink")[朝鮮民主主義人民共和國和](../Page/朝鮮民主主義人民共和國.md "wikilink")[大韓民國特殊](../Page/大韓民國.md "wikilink")[双重国籍的](../Page/双重国籍.md "wikilink")[足球运动员](../Page/足球.md "wikilink")，是一名[前鋒](../Page/前锋_\(足球\).md "wikilink")，现效力於[日本J聯賽](../Page/日本職業足球聯賽.md "wikilink")[清水心跳](../Page/清水心跳.md "wikilink")。

2010年郑大世代表[朝鮮民主主義人民共和國參加](../Page/朝鮮民主主義人民共和國.md "wikilink")[世界盃足球賽](../Page/2010年世界盃足球賽.md "wikilink")，在首次亮相唱[国歌时因泪流满面而给世人深刻的印象](../Page/愛國歌_\(朝鮮民主主義人民共和國\).md "wikilink")，鄭大世在世界盃比赛中表现出色赢得了对手的尊重。郑大世的身体和速度俱佳，在朝鮮被稱為「人民的[魯尼](../Page/韋恩·魯尼.md "wikilink")」（），而在日本被稱為「人肉[推土機](../Page/推土機.md "wikilink")」（）。鄭大世在世界盃後獲德乙球隊[波鸿邀請加盟](../Page/波鸿足球俱乐部.md "wikilink")，首度在歐洲作賽。

## 經歷

郑大世的身世有很多不同的说法，他出生在日本[爱知县的](../Page/爱知县.md "wikilink")[名古屋市](../Page/名古屋.md "wikilink")，父親是南韓人，母親則是在日朝鮮人。最近接受采访的时候，郑大世表明说自己的爷爷是朝鲜半岛北部人，也就是籍贯来自现在的[朝鮮](../Page/朝鮮民主主義人民共和國.md "wikilink")，所以代表朝鮮出场参加比赛是理所当然的。他父母為了不讓他忘記[母語](../Page/母語.md "wikilink")，从小郑大世就被送入了[在日本朝鲜人总联合会下属的学校学习](../Page/在日本朝鲜人总联合会.md "wikilink")，因此他对朝鮮拥有了更加深厚的感情，梦想为[朝鲜国家足球队效力](../Page/朝鲜国家足球队.md "wikilink")。他后来曾申请放弃[韓國國籍加入](../Page/韓國國籍.md "wikilink")[朝鮮國籍](../Page/朝鮮人.md "wikilink")，但由于[韓國政府不承认朝鲜是一个](../Page/韓國政府.md "wikilink")[主权国家](../Page/主权国家.md "wikilink")，因此拒绝了他的这一请求，但仍獲朝鮮政府發放[護照](../Page/護照.md "wikilink")。他的这一特殊的双重身份被[国际足联所认可](../Page/国际足联.md "wikilink")，[2007年6月](../Page/2007年6月.md "wikilink")，已是J1联赛职业球员的他终于得偿所愿，被[朝鮮國家足球隊招入队中参加在](../Page/朝鮮國家足球隊.md "wikilink")[澳门举行的](../Page/澳门.md "wikilink")[东亚四强赛的预选赛](../Page/2008年东亚足球锦标赛.md "wikilink")，结果在3场比赛中攻入8球，成为预选赛阶段的最佳射手。在2008年2月举行的这项赛事的决赛圈比赛中，又分别在对自己的出生国[日本以及](../Page/日本国家足球队.md "wikilink")[韩国时分别攻入一球](../Page/韩国国家足球队.md "wikilink")，在对[中国的比赛中也贡献了一个助攻](../Page/中国国家足球队.md "wikilink")，这些成就使郑大世声名大噪。

從小鄭大世的夢想原是當「雞仔性別鑑別師」，並且害怕[蟑螂](../Page/蟑螂.md "wikilink")，之後把足球視為精神糧食，練球認真。[韓國足壇原認為如果能網羅鄭大世加入](../Page/韓國.md "wikilink")[K聯賽能夠表現優異](../Page/K聯賽.md "wikilink")，不過他一直視朝鮮為自己的祖國。

2010年的[南非世界杯赛场上郑大世表现抢眼](../Page/2010年世界盃足球賽.md "wikilink")，而6月15日首次登场世界杯对阵[巴西队的赛前唱国歌仪式上郑大世更是泪流满面震撼了全世界](../Page/巴西国家足球队.md "wikilink")。7月2日郑大世被[德乙球会](../Page/德乙.md "wikilink")[波鸿以](../Page/波鸿足球俱乐部.md "wikilink")25万[欧元签下](../Page/欧元.md "wikilink")，正式登陆欧洲。\[1\]

2012年1月30日，德甲球隊[科隆以](../Page/科隆足球俱乐部.md "wikilink")50萬歐元簽下鄭大世，科隆正陷入護級及首席射手[普度斯基傷患等不利情況](../Page/卢卡斯·波多尔斯基.md "wikilink")。\[2\]

2013年至2015年，鄭大世加入[經典K聯賽的](../Page/經典K聯賽.md "wikilink")[水原三星藍翼足球俱樂部](../Page/水原三星藍翼足球俱樂部.md "wikilink")。郑大世称，希望自己效力韩国联赛这件事能够加速[祖国统一进程](../Page/朝鲜半岛统一问题.md "wikilink")\[3\]。

2015年7月，郑大世回到[日本J聯賽](../Page/日本職業足球聯賽.md "wikilink")，加入[清水心跳](../Page/清水心跳.md "wikilink")\[4\]。

郑大世一直視朝鮮為祖國。在前任[朝鮮最高領導人](../Page/朝鮮最高領導人.md "wikilink")[金正日](../Page/金正日.md "wikilink")[逝世后](../Page/金正日之死.md "wikilink")，他曾到自己的出生地日本名古屋出席相关悼念活动\[5\]。在韩国联赛效力期间，郑大世曾在媒体前公开表达对[金正日的尊敬之情](../Page/金正日.md "wikilink")，引发轩然大波\[6\]。

## 求學經歷

  - 愛知朝鮮第二初級學校
  - 東春朝鮮初中級學校 中級部
  - 愛知朝鮮中高級学校 高級部
  - [朝鮮大學](../Page/朝鮮大學_\(日本\).md "wikilink")

## 職業生涯統計

|-
|[2006](../Page/2006年日本職業足球聯賽.md "wikilink")||rowspan="5"|[川崎前鋒](../Page/川崎前鋒.md "wikilink")||rowspan="5"|[日本職業足球甲級聯賽](../Page/日本職業足球甲級聯賽.md "wikilink")||16||1||2||2||4||0||colspan="2"|–||22||3
|-
|[2007](../Page/2007年日本職業足球聯賽.md "wikilink")||24||12||4||2||5||2||7||2||40||18
|-
|[2008](../Page/2008年日本職業足球聯賽.md "wikilink")||33||14||2||0||4||1||colspan="2"|–||39||15
|-
|[2009](../Page/2009年日本職業足球聯賽.md "wikilink")||29||15||4||3||5||2||9||2||47||22
|-
|[2010](../Page/2010年日本職業足球聯賽.md "wikilink")||10||5||colspan="2"|–||colspan="2"|–||3||1||13||6
 |-
|[2010–11](../Page/2010年至2011年德國足球乙級聯賽.md "wikilink")||rowspan="2"|[波琴](../Page/波琴足球會.md "wikilink")||rowspan="2"|[德國足球乙級聯賽](../Page/德國足球乙級聯賽.md "wikilink")||25||10||1||0||colspan="2"|N/A||colspan="2"|–||26||10
|-
|[2011–12](../Page/2011年至2012年德國足球乙級聯賽.md "wikilink")||14||4||1||1||colspan="2"|N/A||colspan="2"|–||15||5
|-
|[2011–12](../Page/2011年至2012年德國足球甲級聯賽.md "wikilink")||rowspan="2"|[科隆](../Page/科隆足球會.md "wikilink")||rowspan="1"|[德國足球甲級聯賽](../Page/德國足球甲級聯賽.md "wikilink")||5||0||0||0||colspan="2"|N/A||colspan="2"|–||5||0
|-
|[2012–13](../Page/2012年至2013年德國足球乙級聯賽.md "wikilink")||rowspan="1"|[德國足球乙級聯賽](../Page/德國足球乙級聯賽.md "wikilink")||5||0||1||0||colspan="2"|N/A||colspan="2"|–||6||0
 |-
|[2013](../Page/2013年經典K聯賽.md "wikilink")||rowspan="3"|[水原藍翼](../Page/水原藍翼.md "wikilink")||rowspan="3"|[經典K聯賽](../Page/經典K聯賽.md "wikilink")||23||10||colspan="2"|–||colspan="2"|N/A||4||0||27||10
|-
|[2014](../Page/2014年經典K聯賽.md "wikilink")||28||7||1||0||colspan="2"|N/A||colspan="2"|–||29||7
|-
|[2015](../Page/2015年經典K聯賽.md "wikilink")||20||6||1||2||colspan="2"|N/A||7||3||28||11
 |-
|[2015](../Page/2015年日本職業足球聯賽.md "wikilink")||rowspan="4"|[清水心跳](../Page/清水心跳.md "wikilink")||rowspan="1"|[日本職業足球甲級聯賽](../Page/日本職業足球甲級聯賽.md "wikilink")||13||4||0||0||colspan="2"|–||colspan="2"|–||13||4
|-
|[2016](../Page/2016年日本職業足球聯賽.md "wikilink")||rowspan="1"|[日本職業足球乙級聯賽](../Page/日本職業足球乙級聯賽.md "wikilink")||37||26||1||1||colspan="2"|–||colspan="2"|–||38||27
|-
|[2017](../Page/2017年日本職業足球聯賽.md "wikilink")||rowspan="2"|[日本職業足球甲級聯賽](../Page/日本職業足球甲級聯賽.md "wikilink")||23||10||1||0||2||0||colspan="2"|–||26||10
|-
|[2018](../Page/2018年日本職業足球聯賽.md "wikilink")||18||3||2||0||6||3||colspan="2"|–||26||6
|- 203||89||16||8||26||8||19||5||264||110
49||14||3||1||colspan="2"|N/A||colspan="2"|–||54||15
71||23||2||2||colspan="2"|N/A||11||3||84||28
303||120||21||10||26||8||30||8||380||146

## 國際賽入球

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>場地</p></th>
<th><p>對手</p></th>
<th><p>進球</p></th>
<th><p>比數</p></th>
<th><p>比賽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>2007年6月19日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>7-0</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>2007年6月19日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>7-0</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2007年6月19日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>4-0</p></td>
<td><p>7-0</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2007年6月19日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>7-0</p></td>
<td><p>7-0</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2007年6月21日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>2-0</p></td>
<td><p>7-1</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2007年6月21日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>3-0</p></td>
<td><p>7-1</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2007年6月21日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>5-0</p></td>
<td><p>7-1</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2007年6月21日</p></td>
<td><p><a href="../Page/澳門特别行政区.md" title="wikilink">澳門</a></p></td>
<td></td>
<td><p>6-1</p></td>
<td><p>7-1</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2008年2月17日</p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中國</a><a href="../Page/重慶市.md" title="wikilink">重慶</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>1-1</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2008年2月20日</p></td>
<td><p><a href="../Page/中華人民共和國.md" title="wikilink">中國</a><a href="../Page/重慶市.md" title="wikilink">重慶</a></p></td>
<td></td>
<td><p>1-1</p></td>
<td><p>1-1</p></td>
<td><p><a href="../Page/2008年東亞足球錦標賽.md" title="wikilink">2008年東亞足球錦標賽</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2008年10月15日</p></td>
<td><p><a href="../Page/伊朗.md" title="wikilink">伊朗</a><a href="../Page/德黑蘭.md" title="wikilink">德黑蘭</a></p></td>
<td></td>
<td><p>1-2</p></td>
<td><p>1-2</p></td>
<td><p><a href="../Page/2010年世界盃外圍賽_(亞洲區).md" title="wikilink">2010年世界盃外圍賽 (亞洲區)</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2009年8月27日</p></td>
<td><p><a href="../Page/中华民国.md" title="wikilink">臺灣</a><a href="../Page/高雄市.md" title="wikilink">高雄</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>2-1</p></td>
<td><p><a href="../Page/2010年東亞足球錦標賽.md" title="wikilink">2010年東亞足球錦標賽資格賽</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2010年5月25日</p></td>
<td><p><a href="../Page/奧地利.md" title="wikilink">奧地利</a><a href="../Page/費爾德基希縣.md" title="wikilink">費爾德基希</a></p></td>
<td></td>
<td><p>1-1</p></td>
<td><p>2-2</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>2010年5月25日</p></td>
<td><p><a href="../Page/奧地利.md" title="wikilink">奧地利</a><a href="../Page/費爾德基希縣.md" title="wikilink">費爾德基希</a></p></td>
<td></td>
<td><p>2-2</p></td>
<td><p>2-2</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2010年6月6日</p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/東蘭德.md" title="wikilink">東蘭德</a></p></td>
<td></td>
<td><p>1-2</p></td>
<td><p>1-3</p></td>
<td><p><a href="../Page/友誼賽.md" title="wikilink">友誼賽</a></p></td>
</tr>
</tbody>
</table>

## 榮譽

### 國家隊

  - 朝鮮

<!-- end list -->

  - 東亞足球錦標賽預賽冠軍：[2007年](../Page/2008年東亞足球錦標賽.md "wikilink")；

### 個人

  - 東亞足球錦標賽最佳射手：[2008年](../Page/2008年東亞足球錦標賽.md "wikilink")；
  - 東亞足球錦標賽預賽最佳射手：[2007年](../Page/2008年東亞足球錦標賽.md "wikilink")；
  - 日本職業足球乙組聯賽每月最有價值球員（2）：2016年6月、10月；
  - 日本職業足球乙組聯賽神射手 : 2016年(26球)；

## 参考文献

## 外部链接

  - [Kawasaki Frontale
    official](http://www.frontale.co.jp/profile/2009/mem_09.html)
    郑大世官方网站
  - [FIFA.com - Jong straddles the 38th
    parallel](http://www.fifa.com/worldcup/news/newsid=736455.html)
  - [鄭大世的部落格](http://ameblo.jp/jongtaese9/)

[Category:歸化朝鮮民主主義人民共和國公民](../Category/歸化朝鮮民主主義人民共和國公民.md "wikilink")
[Category:在日韓國-朝鮮人](../Category/在日韓國-朝鮮人.md "wikilink")
[Category:名古屋市出身人物](../Category/名古屋市出身人物.md "wikilink")
[Category:朝鲜足球運動员](../Category/朝鲜足球運動员.md "wikilink")
[Category:朝鮮國家足球隊球員](../Category/朝鮮國家足球隊球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:川崎前鋒球員](../Category/川崎前鋒球員.md "wikilink")
[Category:波琴球員](../Category/波琴球員.md "wikilink")
[Category:科隆球員](../Category/科隆球員.md "wikilink")
[Category:水原三星蓝翼球员](../Category/水原三星蓝翼球员.md "wikilink")
[Category:清水心跳球員](../Category/清水心跳球員.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:韓職球員](../Category/韓職球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:朝鮮旅外足球運動員](../Category/朝鮮旅外足球運動員.md "wikilink")
[Category:日本外籍足球運動員](../Category/日本外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:2011年亞洲盃足球賽球員](../Category/2011年亞洲盃足球賽球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:朝鲜人民运动员](../Category/朝鲜人民运动员.md "wikilink")
[Category:日本朝鮮大學校友](../Category/日本朝鮮大學校友.md "wikilink")

1.  [郑大世登陆欧洲梦成真 25万欧元转会德国波鸿队
    新浪体育](http://2010.sina.com.cn/prk/2010-07-02/212762020.shtml)

2.  [鄭大世過檔科隆做普多斯基副手
    now體育](http://news.now.com/home/sports/player?newsId=1635843873)

3.
4.

5.  [朝鲜第一球星现身日本
    郑大世低调哀悼金正日](http://sports.qq.com/a/20111230/000958.htm)，腾讯.南方日报

6.  [郑大世效忠金正日言论曝光
    韩民众反对入全明星](http://sports.sohu.com/20130531/n377677439.shtml)，搜狐