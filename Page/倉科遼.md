**倉科遼**（），本名**大場敬司**，尚有另一個筆名**司敬**，是[日本的](../Page/日本.md "wikilink")[漫画原作者](../Page/漫画原作者.md "wikilink")。出身於[栃木县](../Page/栃木县.md "wikilink")[那須鹽原市](../Page/那須鹽原市.md "wikilink")。

## 簡歷

1971年以「司敬」的名義參與漫畫家面試。

## 作品列表

### 以「倉科遼」名義發表

### 以「司敬」名義發表

## 外部連結

  - [フリーハンド](https://web.archive.org/web/20061231044752/http://www.free-hand.co.jp/)

[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:栃木縣出身人物](../Category/栃木縣出身人物.md "wikilink")