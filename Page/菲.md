**菲**旧称**芠**（[分子式](../Page/分子式.md "wikilink")：C<sub>14</sub>H<sub>10</sub>），是一个[多环芳香烃](../Page/多环芳香烃.md "wikilink")，由三个[苯环稠合而成](../Page/苯.md "wikilink")，与[蒽为](../Page/蒽.md "wikilink")[同分异构体](../Page/同分异构体.md "wikilink")。无色片状结晶，有蓝色[螢光](../Page/螢光.md "wikilink")，易[昇华](../Page/昇华.md "wikilink")，具刺激性。存在于吸烟时的烟雾中。[折射率](../Page/折射率.md "wikilink")(n<sub>D</sub><sup>20</sup>)为1.6415。

## 化学性质

菲的[化学性质介于](../Page/化学性质.md "wikilink")[萘和](../Page/萘.md "wikilink")[蒽之间](../Page/蒽.md "wikilink")。反应主要在9和10位上。可以被[铬酸](../Page/铬酸.md "wikilink")[氧化生成](../Page/氧化.md "wikilink")[菲醌](../Page/菲醌.md "wikilink")\[1\]，被[氢气](../Page/氢气.md "wikilink")（与[雷尼镍](../Page/雷尼镍.md "wikilink")）还原为9,10-二氢菲\[2\]，发生[臭氧化反应生成](../Page/臭氧化反应.md "wikilink")2-[联苯醛的衍生物](../Page/联苯.md "wikilink")\[3\]，也可发生[溴化](../Page/溴化.md "wikilink")\[4\]
和[磺化](../Page/磺化.md "wikilink")\[5\]
等[取代反应](../Page/取代反应.md "wikilink")。

## 制备及应用

由[煤焦油的](../Page/煤焦油.md "wikilink")[蒽油](../Page/蒽油.md "wikilink")[馏分中分离](../Page/馏分.md "wikilink")。也可由某些[二苯乙烯衍生物受光环化得到](../Page/二苯乙烯.md "wikilink")。用作[有机合成原料](../Page/有机合成.md "wikilink")，用于合成[农药](../Page/农药.md "wikilink")、[合成树脂](../Page/合成树脂.md "wikilink")、[染料](../Page/染料.md "wikilink")、[医药](../Page/医药.md "wikilink")、[防霉剂](../Page/防霉剂.md "wikilink")、[鞣革剂等](../Page/鞣革剂.md "wikilink")，还可作为[炸药的](../Page/炸药.md "wikilink")[稳定剂](../Page/稳定剂.md "wikilink")。

一个合成菲的人名反应是“**Bardhan-Sengupta菲合成**”：\[6\]\[7\]\[8\]\[9\]

  -
    [Bardhan-SenguptaPhenanthreneSynthesis.png](https://zh.wikipedia.org/wiki/File:Bardhan-SenguptaPhenanthreneSynthesis.png "fig:Bardhan-SenguptaPhenanthreneSynthesis.png")

[邻二氮菲是菲的氮杂环衍生物](../Page/邻二氮菲.md "wikilink")。

## 参考资料

[Category:多环芳香烃](../Category/多环芳香烃.md "wikilink")

1.
2.
3.
4.
5.
6.  J. C. Bardhan and S. C. Sengupta, *J. Chem. Soc.* **1932**, 2520,
    2798.
7.  R. P. Linstead, *Ann. Repts.* (*Chem. Soc.*, London) 33, 319 (1936)
8.  L. F. Fieser and M. Fieser, *Natural Products Related to
    Phenanthrene* (New York, 1949), p 88.
9.  W. B. Renfrow *et al.*, *[J. Am. Chem.
    Soc.](../Page/J._Am._Chem._Soc..md "wikilink")* 73, 317 (1951).