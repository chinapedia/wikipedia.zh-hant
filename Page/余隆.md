**余隆**（），[中国指挥家](../Page/中国.md "wikilink")。

## 生平

余隆出生于一个音乐世家，其父余立勋曾在[中央民族歌舞团担任舞蹈编导](../Page/中央民族歌舞团.md "wikilink")\[1\]\[2\]，外祖父是著名作曲家[丁善德](../Page/丁善德.md "wikilink")。后来余隆到[上海音乐学院在中国著名音乐教育家](../Page/上海音乐学院.md "wikilink")[黄晓同门下学习音乐](../Page/黄晓同.md "wikilink")，后来留学[德国](../Page/德国.md "wikilink")。1992年余隆出任[北京中央歌剧院常任指挥](../Page/北京中央歌剧院.md "wikilink")。参与创办了首届[北京新年音乐会并在接下来的三年里担任指挥](../Page/北京新年音乐会.md "wikilink")。1998年起，余隆担任他参与创办的[北京国际音乐节艺术总监](../Page/北京国际音乐节.md "wikilink")。2000年起，余隆担任他参与组建的[中国爱乐乐团的艺术总监](../Page/中国爱乐乐团.md "wikilink")。
2003年起，余隆加入[广州交响乐团担任艺术总监](../Page/广州交响乐团.md "wikilink")。

他与很多国外国际乐团合作过：德国汉堡歌剧院、[柏林广播交响乐团](../Page/柏林广播交响乐团.md "wikilink")、[莱比锡广播乐团](../Page/莱比锡广播乐团.md "wikilink")、[不莱梅爱乐乐团](../Page/不莱梅爱乐乐团.md "wikilink")、[法国尼斯大剧院](../Page/法国尼斯大剧院.md "wikilink")、[圣马丁室内乐团](../Page/圣马丁室内乐团.md "wikilink")、匈牙利[布达佩斯广播交响乐团](../Page/布达佩斯广播交响乐团.md "wikilink")、[爱尔兰国家爱乐乐团](../Page/爱尔兰国家爱乐乐团.md "wikilink")、[澳大利亚ABC交响乐团](../Page/澳大利亚ABC交响乐团.md "wikilink")。

他指挥的曲目范围非常广泛，从交响乐到歌剧等各个不同的时代和领域的作品他均有涉猎。从目前他演出的曲目来看，他比较偏重于浪漫派的作品，如[马勒的作品](../Page/古斯塔夫·馬勒.md "wikilink")。他曾指挥北京、广州和上海三地的交响乐团和合唱团演绎马勒第八交响曲“千人”。同时，余隆也不忘宣传中国音乐。他在[德国唱片公司录下一套两张的中国民乐CD](../Page/德国唱片公司.md "wikilink")，很受欢迎,并在1999年指挥了《中国唐宋诗篇音乐朗诵会》的演出。

但余隆以指挥[歌剧著名](../Page/歌剧.md "wikilink")。他在世界各地指挥的歌剧包括《[茶花女](../Page/茶花女.md "wikilink")》、《[图兰多](../Page/图兰多.md "wikilink")》、《[阿伊达](../Page/阿伊达.md "wikilink")》、《[卡门](../Page/卡门.md "wikilink")》、《[拉美莫尔的露契亚](../Page/拉美莫尔的露契亚.md "wikilink")》、《[罗米欧与朱利叶](../Page/罗米欧与朱利叶.md "wikilink")》、《[风流寡妇](../Page/风流寡妇.md "wikilink")》、《[唐·帕斯夸里](../Page/唐·帕斯夸里.md "wikilink")》等。

2008年，当选第十一届全国政协委员\[3\]，代表文化艺术界，分入第二十六组。\[4\]

## 荣誉

2016年6月27日，获得德意志联邦共和国十字勋章。\[5\]

2016年10月9日，荣膺[美国文理科学院外籍荣誉院士称号](../Page/美国文理科学院.md "wikilink")。\[6\]

## 参考来源

## 外部链接

  - [CAMI Music agency biography of Long
    Yu](https://web.archive.org/web/20160729200226/http://www.camimusic.com/long-yu)
  - [Compose 20:20 Project](http://www.compose2020.com/)
  - [Shanghai Symphony Orchestra Chinese-language biography of Long
    Yu](http://www.shsymphony.com/page-index-id-32.html)
  - [Guangzhou Symphony biography of Long
    Yu](http://www.gso.org.cn/en/portfolio/artist-long-yu/)
  - [Beijing Music Festival page on Long
    Yu](http://www.bmf.org.cn/en/yishuweiyuan_yulong.html)
  - [Music in the Summer Air page on Long
    Yu](http://www.misa.org.cn/director/index/en)

[Category:中国指挥家](../Category/中国指挥家.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Long](../Category/余姓.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:中國音樂家協會副主席](../Category/中國音樂家協會副主席.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:上海音乐学院校友](../Category/上海音乐学院校友.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.
2.
3.
4.
5.
6.