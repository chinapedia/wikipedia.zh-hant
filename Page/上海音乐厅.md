[Nanking_Theatre.jpg](https://zh.wikipedia.org/wiki/File:Nanking_Theatre.jpg "fig:Nanking_Theatre.jpg")电影《泰山情侣》\]\]
[Shanghai_Concert_Hall.jpg](https://zh.wikipedia.org/wiki/File:Shanghai_Concert_Hall.jpg "fig:Shanghai_Concert_Hall.jpg")

**上海音乐厅**位于[上海市](../Page/上海市.md "wikilink")[黄浦区](../Page/黄浦区.md "wikilink")[延安东路](../Page/延安东路_\(上海\).md "wikilink")[西藏南路](../Page/西藏南路.md "wikilink")。

## 历史

建于1930年，原名**南京大戏院**，1949年改名为北京电影院，1959年再更名为上海音乐厅至今。

上海音乐厅共有观众席1122座，其中楼下640座，楼上482
座。镜框舞台深8.35米、宽16米，舞台可使用面积约100[平方米](../Page/平方米.md "wikilink")。舞台上方有可调控反响板，备有斯坦威D
-274[三角钢琴一架](../Page/三角钢琴.md "wikilink")、[美国](../Page/美国.md "wikilink")[鲍德温SD](../Page/鲍德温.md "wikilink")-10
[三角钢琴一架](../Page/三角钢琴.md "wikilink")、60路调光台、24路[雅马哈调音音响一套](../Page/雅马哈.md "wikilink")、台口话筒插口16只，冷暖气齐备。

上海音乐厅由[华人建築師](../Page/华人.md "wikilink")[范文照设计](../Page/范文照.md "wikilink")，建筑风格属于上海地区少有的[欧洲传统风格](../Page/欧洲.md "wikilink")。休息大厅十六根合抱的赭色[大理石圆柱气度不凡](../Page/大理石.md "wikilink")，观众厅的构图简介规范，复杂又不显零乱，富有层次变化，色彩庄重淡雅，与其演绎的古典音乐有着惊人的统一。自然音响之佳，得到建筑学专家及众多的中外艺术家认同。

著名的小提琴家[斯特恩](../Page/艾萨克·斯特恩.md "wikilink")、[阿卡多](../Page/阿卡多.md "wikilink")、；钢琴家[拉箩查](../Page/拉箩查.md "wikilink")、[傅聪](../Page/傅聪.md "wikilink")、[殷承忠](../Page/殷承忠.md "wikilink")，[费城交响乐团的室内乐团](../Page/费城交响乐团.md "wikilink")、[香港管弦乐团](../Page/香港管弦乐团.md "wikilink")、[中国交响乐团等都曾来此表演](../Page/中国交响乐团.md "wikilink")，并获得巨大成功。

为配合[延安路高架的拓宽建设](../Page/延安路_\(上海\).md "wikilink")，2003年4月15日上午10时，上海音乐厅平移工程开始，工程耗资5000万[人民幣](../Page/人民幣.md "wikilink")，先在原地顶升1.7米，然后向南移动66.46米，再在新址往上顶升1.68米，搬移到原先[龙门路](../Page/龙门路.md "wikilink")（金陵中路—延安西路）的位置。2005年1月1日，平移工程完成，上海音乐厅以崭新的面貌出现在世人面前。

2019年3月起，上海音乐厅开启历时300多天的整体閉門修缮工程，期間不對外開放\[1\]。

## 交通

  - 轨道交通
      - [8号线](../Page/上海地铁8号线.md "wikilink")[大世界站徒步](../Page/大世界站.md "wikilink")4分钟
  - 公交
      - 18路
      - 48路
      - 71路
      - 127路
      - 64路

## 附近

  - [上海博物馆](../Page/上海博物馆.md "wikilink")
  - [上海电信大厦](../Page/上海电信大厦.md "wikilink")

## 参考来源

<div class="reference-small">

  -

</div>

## 参见

[Category:上海文化场馆](../Category/上海文化场馆.md "wikilink")

1.  [上海音乐厅下月修缮
    本周日开放日活动预约爆满](http://www.kankanews.com/a/2019-02-18/0038757104.shtml)