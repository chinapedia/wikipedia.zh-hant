[Tuen_Mun_Fu_Tei_2013.jpg](https://zh.wikipedia.org/wiki/File:Tuen_Mun_Fu_Tei_2013.jpg "fig:Tuen_Mun_Fu_Tei_2013.jpg")
[Fu_tei_village_office.JPG](https://zh.wikipedia.org/wiki/File:Fu_tei_village_office.JPG "fig:Fu_tei_village_office.JPG")
[Lingnan_University_Campus_Overview_201410.jpg](https://zh.wikipedia.org/wiki/File:Lingnan_University_Campus_Overview_201410.jpg "fig:Lingnan_University_Campus_Overview_201410.jpg")
**虎地**（，舊稱**Bowring**\[1\]\[2\]）是[香港地方](../Page/香港地方.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[屯門區](../Page/屯門區.md "wikilink")[桃園圍以南](../Page/桃園圍.md "wikilink")、[屯門河以東](../Page/屯門河.md "wikilink")、[青田路以北](../Page/青田路.md "wikilink")，[香港政府於](../Page/香港殖民地時期#香港政府.md "wikilink")1970年代宣佈將[屯門](../Page/屯門.md "wikilink")／[青山發展為](../Page/青山.md "wikilink")[新市鎮後](../Page/新市鎮.md "wikilink")，虎地成為其部份，訂為[屯門新市鎮第](../Page/屯門新市鎮.md "wikilink")52區，位處[屯門新市鎮東北端](../Page/屯門新市鎮.md "wikilink")，鄰近[藍地](../Page/藍地.md "wikilink")。

## 歷史

在發展為新市鎮以前，虎地是[屯門區鄉郊地方](../Page/屯門區.md "wikilink")，只有村落、寺廟和佔地6.87公頃的[虎地軍營](../Page/虎地軍營.md "wikilink")，由[慶平路連接](../Page/慶平路.md "wikilink")[兆康苑對岸一段](../Page/兆康苑.md "wikilink")[青山公路（新墟段）](../Page/青山公路.md "wikilink")（現[屯門公路](../Page/屯門公路.md "wikilink")）。

自1990年代初期，香港政府開始發展虎地。根據新市鎮發展進程，首先截斷[屯門醫院對岸](../Page/屯門醫院.md "wikilink")[一段青山公路](../Page/青山公路#新墟段.md "wikilink")，靠近河岸一段易名為屯門公路，以及建造新道路繞進第52區範圍內，即前[虎地難民禁閉中心](../Page/虎地難民禁閉中心.md "wikilink")、[虎地軍營及](../Page/虎地軍營.md "wikilink")[虎地中村旁](../Page/虎地中村.md "wikilink")，最後於道路兩旁大興土木。

高等教育辦學團體[嶺南學院](../Page/嶺南大學_\(香港\).md "wikilink")（現[嶺南大學](../Page/嶺南大學_\(香港\).md "wikilink")）於1992年獲准選址虎地建新校舍。教學大樓和宿舍等主要設施於1995年落成後，從[灣仔](../Page/灣仔.md "wikilink")[司徒拔道搬遷至新校舍](../Page/司徒拔道.md "wikilink")，而貫通虎地南北的新道路，則命名為[青山公路（嶺南段）](../Page/青山公路.md "wikilink")。該校的室外運動場於2007年落成，座落於軍營原址。

隨後，有更多住宅於虎地興建，計有公營房屋[富泰邨](../Page/富泰邨.md "wikilink")、私人發展商樓盤[叠茵庭](../Page/叠茵庭.md "wikilink")、[聚康山莊等](../Page/聚康山莊.md "wikilink")，也是在2000年代初相繼落成，完全淹沒了昔日的難民禁閉中心、軍營和虎地中村。虎地過往的主要通道[慶平路亦經過改建](../Page/慶平路.md "wikilink")，部份路段成為了[屯富路](../Page/屯富路.md "wikilink")，往來虎地的市民可通過新路到達[港鐵](../Page/港鐵.md "wikilink")[兆康站](../Page/兆康站_\(西鐵綫\).md "wikilink")。整個屯門第52區的西南端，則保留了該區最多原始建築，包括獲[古物諮詢委員會評為](../Page/古物諮詢委員會.md "wikilink")「[三級歷史建築](../Page/香港三級歷史建築列表.md "wikilink")」的[清涼法苑佛殿](../Page/清涼法苑.md "wikilink")，在其旁的淨恩小築、[極樂寺及](../Page/極樂寺_\(香港\).md "wikilink")[虎地新村](../Page/虎地新村.md "wikilink")。

至於同屬虎地範圍，但靠向山而未被納入新市鎮發展的部份，仍保留着兩條村落，分別是位於[頌皇台的](../Page/頌皇台.md "wikilink")[虎地上村和](../Page/虎地上村.md "wikilink")[老虎坑的](../Page/老虎坑.md "wikilink")[虎地下村](../Page/虎地下村.md "wikilink")，主要靠[富地路和](../Page/富地路.md "wikilink")[虎坑路進出村內外](../Page/虎坑路.md "wikilink")。虎地的水源主要來自[九徑山西北麓的山谷河溪](../Page/九徑山.md "wikilink")：虎地上村和虎地下村各自有溪流流經，前者有九徑山西北端的岬角向下流的溪水，後者則有從九徑山山谷流往[藍地水塘](../Page/藍地水塘.md "wikilink")（前稱[老虎坑水塘](../Page/藍地水塘.md "wikilink")）集水區，再流進[老虎坑](../Page/老虎坑.md "wikilink")，兩地的溪流最後均會流進[屯門河道](../Page/屯門河.md "wikilink")，兩村村民現已不需依賴溪水過活。

## 過往建築

  - [虎地中村](../Page/虎地中村.md "wikilink")
      - 即現今[富泰邨](../Page/富泰邨.md "wikilink")、[倚嶺南庭](../Page/倚嶺南庭.md "wikilink")、[聚康山莊及](../Page/聚康山莊.md "wikilink")[嶺南大學](../Page/嶺南大學_\(香港\).md "wikilink")
  - [虎地軍營](../Page/虎地軍營.md "wikilink")
      - 即現今[富泰邨及](../Page/富泰邨.md "wikilink")[嶺南大學室外運動場](../Page/嶺南大學_\(香港\).md "wikilink")
  - 虎地難民禁閉中心
      - 即現今[嶺南大學](../Page/嶺南大學_\(香港\).md "wikilink")
  - [清涼里](../Page/清涼里.md "wikilink")：連接[慶平路至清涼法苑的道路](../Page/慶平路.md "wikilink")
      - 即現今[叠茵庭](../Page/叠茵庭.md "wikilink")
  - [虎地交匯處](../Page/藍地交匯處.md "wikilink")：位於虎地以北
      - 即現今[藍地交匯處](../Page/藍地交匯處.md "wikilink")

## 現今建築

[HK_Tuen_Mun_Fu_Tei_Fu_Tai_Estate.JPG](https://zh.wikipedia.org/wiki/File:HK_Tuen_Mun_Fu_Tei_Fu_Tai_Estate.JPG "fig:HK_Tuen_Mun_Fu_Tei_Fu_Tai_Estate.JPG")仰望[富泰邨](../Page/富泰邨.md "wikilink")，基座為[富泰商場](../Page/富泰商場.md "wikilink")\]\]
[Parkland_Villa_201212.jpg](https://zh.wikipedia.org/wiki/File:Parkland_Villa_201212.jpg "fig:Parkland_Villa_201212.jpg")
[South_Hillcrest_2014.jpg](https://zh.wikipedia.org/wiki/File:South_Hillcrest_2014.jpg "fig:South_Hillcrest_2014.jpg")

### 住宅

  - [富泰邨](../Page/富泰邨.md "wikilink")
  - [聚康山莊](../Page/聚康山莊.md "wikilink")
  - [倚嶺南庭](../Page/倚嶺南庭.md "wikilink")
  - [名賢居](../Page/名賢居.md "wikilink")
  - [叠茵庭](../Page/叠茵庭.md "wikilink")
  - [虎地上村](../Page/虎地上村.md "wikilink")
  - [虎地下村](../Page/虎地下村.md "wikilink")
  - [虎地新村](../Page/虎地新村.md "wikilink")

### 學校、社區設施

  - [嶺南大學](../Page/嶺南大學_\(香港\).md "wikilink")
  - [神召神學院](../Page/神召神學院.md "wikilink")
  - [興德學校](../Page/興德學校.md "wikilink")
  - [虎地消防局](../Page/虎地消防局.md "wikilink")
  - [富泰商場](../Page/富泰商場.md "wikilink")

### 道路

[Lam_Tei_View_201309.jpg](https://zh.wikipedia.org/wiki/File:Lam_Tei_View_201309.jpg "fig:Lam_Tei_View_201309.jpg")

  - [青山公路（嶺南段）](../Page/青山公路.md "wikilink")
  - [屯門公路](../Page/屯門公路.md "wikilink")
  - [元朗公路](../Page/元朗公路.md "wikilink")
  - [藍地交匯處](../Page/藍地交匯處.md "wikilink")
  - [屯富路](../Page/屯富路.md "wikilink")
  - [屯貴路](../Page/屯貴路.md "wikilink")
  - [屯安里](../Page/屯安里.md "wikilink")
  - [慶平路](../Page/慶平路.md "wikilink")
  - [富地路](../Page/富地路.md "wikilink")
  - [虎坑路](../Page/虎坑路.md "wikilink")

## 公共交通

  - [港鐵](../Page/港鐵.md "wikilink")[西鐵綫](../Page/西鐵綫.md "wikilink")　　：[兆康站](../Page/兆康站_\(西鐵綫\).md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[輕便鐵路](../Page/輕便鐵路.md "wikilink")　：[兆康站](../Page/兆康站_\(輕鐵\).md "wikilink")
  - [公共運輸交匯處](../Page/公共運輸交匯處.md "wikilink")：[兆康站（南）](../Page/兆康站（南）公共運輸交匯處.md "wikilink")、[兆康站（北）](../Page/兆康站（北）公共運輸交匯處.md "wikilink")、[富泰邨](../Page/富泰公共運輸交匯處.md "wikilink")

### 途經綫路

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")
  - [輕鐵](../Page/輕鐵.md "wikilink")：505、610、614P、615、615P、751

<!-- end list -->

  - [巴士](../Page/巴士.md "wikilink")

<!-- end list -->

  - [港鐵巴士](../Page/港鐵巴士.md "wikilink")：K51、K58
  - [九龍巴士](../Page/九龍巴士.md "wikilink")：53、63X、67M、67X、68A、258P、261、267X、960P、960S、N260
  - [龍運巴士](../Page/龍運巴士.md "wikilink")：A33P、A33X、E33P、NA33
  - [城巴](../Page/城巴.md "wikilink")：B3A
  - [專線小巴](../Page/專線小巴.md "wikilink")：46

## 區議會議席分佈

為方便比較，以下列表以[屯門河以東](../Page/屯門河.md "wikilink")、北至[元朗公路](../Page/元朗公路.md "wikilink")，包括[青山公路嶺南段沿線地區為範圍](../Page/青山公路.md "wikilink")。

| 年度/範圍                                                                      | 2000-2003                                  | 2004-2007                                                                       | 2008-2011                               | 2012-2015 | 2016-2019 | 2020-2023 |
| -------------------------------------------------------------------------- | ------------------------------------------ | ------------------------------------------------------------------------------- | --------------------------------------- | --------- | --------- | --------- |
| [青山公路嶺南段及](../Page/青山公路.md "wikilink")[嶺南大學一帶](../Page/嶺南大學.md "wikilink") | [景峰選區](../Page/景峰_\(選區\).md "wikilink")    |                                                                                 |                                         |           |           |           |
| [元朗公路以南至](../Page/元朗公路.md "wikilink")[富泰邨](../Page/富泰邨.md "wikilink")      | [景峰選區一部分](../Page/景峰_\(選區\).md "wikilink") | 分散在[富泰選區及](../Page/富泰_\(選區\).md "wikilink")[屯門鄉郊選區](../Page/屯門鄉郊.md "wikilink") | [富泰選區](../Page/富泰_\(選區\).md "wikilink") |           |           |           |

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參考文獻

  - 香港街道地方指南1990（通用圖書有限公司出版）ISBN 962-7262-70-8
  - 香港街道地方指南1997（通用圖書有限公司出版）ISBN 962-7262-97-8
  - 香港街道地方指南2002（通用圖書有限公司出版）ISBN 962-7262-83-8
  - 香港街道大廈詳圖2008（通用圖書有限公司出版）ISBN 978-962-8797-23-3

## 註釋

## 外部連結

  - [香港特區政府規劃署
    部門內部草圖索引圖](http://www.pland.gov.hk/pland_tc/info_serv/tp_plan/index_pdf/cir_tm-yl-tsw.pdf)
  - [屯門區議會會議07-07-2009
    文物保育工作匯報、維修資助計劃與1,444幢歷史建築評估工作](http://www.districtcouncils.gov.hk/tm_d/chinese/doc/min_DC%2009/11th_DC_minutes_A2.pdf)
  - [屯門虎地新村的清涼法苑](http://www.fushantang.com/1005b/e2028.html)

{{-}}

[虎地](../Category/虎地.md "wikilink")
[Category:屯門區](../Category/屯門區.md "wikilink")

1.  [英屬香港第](../Page/英屬香港.md "wikilink")4任總督名稱即爲John
    Bowring（[約翰·寶寧](../Page/寶寧.md "wikilink")）。香港[油尖旺區亦有一街道據其姓氏命名爲](../Page/油尖旺區.md "wikilink")“Bowring
    Street”([寶靈街](../Page/寶靈街.md "wikilink"))。
2.  源於1980年代前位於當地的虎地軍營（Bowring
    Camp），雖然根據往例可以肯定“Bowring”原來只是該軍營的名稱，但後來[港英政府因應](../Page/港英政府.md "wikilink")[越南難民問題而於軍營地闢建的](../Page/越南難民問題.md "wikilink")「屯門（虎地）難民中心」仍然使用“Bowring”一詞對應「虎地」，故可推定當時港英政府認可“Bowring”是「虎地」的其中一個官方英文名稱，但隨着[屯門新市鎮於](../Page/屯門新市鎮.md "wikilink")1990年代後的發展，虎地軍營的建築被完全清拆並進行市鎮發展，回歸後的[香港特區政府已經完全放棄](../Page/香港特區政府.md "wikilink")“Bowring”之名，而目前當地以虎地命名的公共設施的英文名稱亦統一使用“Fu
    Tei”對應中文「虎地」二字。