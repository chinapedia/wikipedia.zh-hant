**东低地德语**是一系列[低地德语](../Page/低地德语.md "wikilink")[方言的总称](../Page/方言.md "wikilink")，分布于[德国东北部](../Page/德国.md "wikilink")、[波兰北部的少数族裔区](../Page/波兰.md "wikilink")。它与[西低地德语一起构成了低地德语](../Page/西低地德语.md "wikilink")。

## 相关语言

东低地德语是大陆[西日耳曼语的](../Page/西日耳曼语支.md "wikilink")[方言连续体的一部分](../Page/方言连续体.md "wikilink")。

在东低地德语区的西部，东低地德语逐渐地变为[西低地德语](../Page/西低地德语.md "wikilink")。两者一般以[动词的](../Page/动词.md "wikilink")[复数结尾的不同来区分](../Page/复数.md "wikilink")：东低地德语基于古第一／三人称结尾
-e(n)，而西低地德语则基于古第二人称结尾 -e(t)。

在东低地德语区的南部，东低地德语逐渐地变为[East Central
German](../Page/East_Central_German.md "wikilink")。两者的区别在于ECG受[High
German consonant
shift的影响](../Page/High_German_consonant_shift.md "wikilink")，而东低地德语则没有。受HGCS影响的区域仍在扩大，特别是
Berlinerisch
dialect（一种[岛方言](../Page/方言岛.md "wikilink")）正在受围绕着它的Brandenburgisch
dialect的影响。

## 方言

  - 在[德国](../Page/德国.md "wikilink")：
      - [Brandenburgisch](../Page/Brandenburgisch.md "wikilink")（在[Brandenburg](../Page/Brandenburg.md "wikilink")）
      - [Mecklenburgisch-Pommersch](../Page/Mecklenburgisch-Pommersch.md "wikilink")（东部）
  - 在[波兰](../Page/波兰.md "wikilink")：
      - [Low
        Prussian](../Page/Low_Prussian.md "wikilink")（波蘭北部[Gdańsk的少數族裔使用](../Page/Gdańsk.md "wikilink")。在1945年后几近灭绝，有些观察者认为包括Plautdietsch在内）
      - [East
        Pomeranian](../Page/East_Pomeranian.md "wikilink")（[Pomerania和](../Page/Pomerania.md "wikilink")[Brazil的少數族裔使用](../Page/Brazil.md "wikilink")。）

It also includes
[Plautdietsch](../Page/Plautdietsch.md "wikilink")（originating from
[Danzig](../Page/Gdansk.md "wikilink")）, which is spoken by
[Mennonites](../Page/Mennonite.md "wikilink") in [North
America](../Page/North_America.md "wikilink") and a few other places in
the world. [Berlinerisch](../Page/Berlinerisch.md "wikilink")（in
[Berlin](../Page/Berlin.md "wikilink")）was a version of Brandenburgisch
in medieval times until they split off; it is now seen as an East High
German dialect. *Baltendeutsch* is a [High
German](../Page/High_Germanic_languages.md "wikilink") variety
influenced by East Low German formerly spoken by Germans in the Baltic
states.

## 样品

  - The Lord's Prayer in Plautdietsch
    Ons Voda em Himmel,
    lot dien Nome jeheilicht woare;
    lot dien Ritjdom kome;
    lot dien Welle jedone woare,
    uck hia oppe Ed, soo aus em Himmel;
    jeff ons Dach fe Dach daut Broot, daut ons fehlt;
    en vejeff ons onse Schult,
    soo aus wie den vejewe, dee sich jeajen ons veschuldicht ha;
    en brinj ons nich en Vesetjunk nenn,
    oba rad ons von Beeset.

## 作家

东低地德语的著名作家有Fritz Reuter等人。

[Category:低地德语](../Category/低地德语.md "wikilink")