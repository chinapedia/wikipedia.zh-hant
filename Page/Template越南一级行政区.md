[胡志明市](胡志明市.md "wikilink"){{\!w}}[海防市](海防市.md "wikilink"){{\!w}}[岘港市](岘港市.md "wikilink"){{\!w}}[芹苴市](芹苴市.md "wikilink")

|group2 = 58[省](省.md "wikilink") |list2 =
[萊州省](萊州省.md "wikilink"){{\!w}}[奠邊省](奠邊省.md "wikilink"){{\!w}}[老街省](老街省.md "wikilink"){{\!w}}[河江省](河江省.md "wikilink"){{\!w}}[高平省](高平省.md "wikilink"){{\!w}}[山羅省](山羅省.md "wikilink"){{\!w}}[安沛省](安沛省.md "wikilink"){{\!w}}[宣光省](宣光省.md "wikilink"){{\!w}}[北𣴓省](北𣴓省.md "wikilink"){{\!w}}[太原省](太原省.md "wikilink"){{\!w}}[諒山省](諒山省.md "wikilink"){{\!w}}[富壽省](富壽省.md "wikilink"){{\!w}}[永福省](永福省.md "wikilink"){{\!w}}[北江省](北江省.md "wikilink"){{\!w}}[北寧省](北寧省.md "wikilink"){{\!w}}[廣寧省](廣寧省.md "wikilink"){{\!w}}[和平省](和平省.md "wikilink"){{\!w}}[海陽省](海陽省.md "wikilink"){{\!w}}[興安省](興安省_\(越南\).md "wikilink"){{\!w}}[寧平省](寧平省.md "wikilink"){{\!w}}[南定省](南定省.md "wikilink"){{\!w}}[河南省](河南省_\(越南\).md "wikilink"){{\!w}}[太平省](太平省.md "wikilink"){{\!w}}[清化省](清化省.md "wikilink"){{\!w}}[乂安省](乂安省.md "wikilink"){{\!w}}[河靜省](河靜省.md "wikilink"){{\!w}}[廣平省](廣平省.md "wikilink"){{\!w}}[廣治省](廣治省.md "wikilink"){{\!w}}[承天順化省](承天順化省.md "wikilink"){{\!w}}[廣南省](廣南省.md "wikilink"){{\!w}}[崑嵩省](崑嵩省.md "wikilink"){{\!w}}[廣義省](廣義省.md "wikilink"){{\!w}}[嘉萊省](嘉萊省.md "wikilink"){{\!w}}[富安省](富安省.md "wikilink"){{\!w}}[平定省](平定省.md "wikilink"){{\!w}}[多樂省](多樂省.md "wikilink"){{\!w}}[得農省](得農省.md "wikilink"){{\!w}}[慶和省](慶和省.md "wikilink"){{\!w}}[林同省](林同省.md "wikilink"){{\!w}}[寧順省](寧順省.md "wikilink"){{\!w}}[西寧省](西寧省.md "wikilink"){{\!w}}[平陽省](平陽省.md "wikilink"){{\!w}}[平福省](平福省.md "wikilink"){{\!w}}[同奈省](同奈省.md "wikilink"){{\!w}}[隆安省](隆安省.md "wikilink"){{\!w}}[平順省](平順省.md "wikilink"){{\!w}}[巴地頭頓省](巴地頭頓省.md "wikilink"){{\!w}}[安江省](安江省.md "wikilink"){{\!w}}[同塔省](同塔省.md "wikilink"){{\!w}}[前江省](前江省.md "wikilink"){{\!w}}[堅江省](堅江省.md "wikilink"){{\!w}}[後江省](後江省.md "wikilink"){{\!w}}[永隆省](永隆省.md "wikilink"){{\!w}}[檳椥省](檳椥省.md "wikilink"){{\!w}}[茶榮省](茶榮省.md "wikilink"){{\!w}}[朔莊省](朔莊省.md "wikilink"){{\!w}}[薄遼省](薄遼省.md "wikilink"){{\!w}}[金甌省](金甌省.md "wikilink")

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[越南县级以上行政区列表](越南县级以上行政区列表.md "wikilink")
}}<noinclude>

</noinclude>

[\*](../Category/越南行政区划.md "wikilink")
[Category:越南一级行政区模板](../Category/越南一级行政区模板.md "wikilink")
[Category:各国一级行政区划导航模板](../Category/各国一级行政区划导航模板.md "wikilink")