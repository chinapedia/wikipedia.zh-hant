**岳阳东站**，又称**新岳阳站**，位于[中国](../Page/中国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[岳阳市](../Page/岳阳市.md "wikilink")[岳阳楼区巴陵路的最东端](../Page/岳阳楼区.md "wikilink")，距[107国道](../Page/107国道.md "wikilink")2.4公里，为[武广客运专线湖南段的客运站](../Page/武广客运专线.md "wikilink")，于2009年建成启用。计划中的[常岳九铁路也会经过此站](../Page/常岳九铁路.md "wikilink")。

## 車站設施

岳阳东站设有基本站台1座、[岛式候车站台](../Page/岛式站台.md "wikilink")2座，正线2条、到发线5条。

## 參見

  - [岳阳站](../Page/岳阳站.md "wikilink")

## 邻近车站

|-    |-

## 参考資料

1.  [城市规划网：规划锦绣蓝图
    创建秀美岳阳](http://www.upla.cn/news/_contents/2006/09/69-4868.shtml)
2.  [岳阳网：武广铁路岳阳站设计招标即将启动](http://www.803.com.cn/Article/yyxw803/jryy/200612/20061208162009_9885.html)
3.  [【快讯】武广客运专线岳阳站竣工](http://hn.rednet.cn/c/2009/12/17/1873884.htm)
4.  [武广岳阳东站交付使用
    湘最早落成武广站](https://web.archive.org/web/20160304211706/http://www.hn.xinhuanet.com/newscenter/2009-12/18/content_18531901.htm)

[Category:岳阳市铁路车站](../Category/岳阳市铁路车站.md "wikilink")
[Category:岳阳楼区](../Category/岳阳楼区.md "wikilink")
[Category:中国铁路广州局集团车站](../Category/中国铁路广州局集团车站.md "wikilink")
[Category:2009年启用的铁路车站](../Category/2009年启用的铁路车站.md "wikilink")