**馬臨**（），[浙江](../Page/浙江.md "wikilink")[鄞县人](../Page/鄞县.md "wikilink")，[生物化學家](../Page/生物化學.md "wikilink")，[香港中文大學第二任校長](../Page/香港中文大學.md "wikilink")，曾創立香港中文大學[逸夫書院](../Page/逸夫書院.md "wikilink")，並獲多個國際學術成就獎項，及中外多間[大學之榮譽](../Page/大學.md "wikilink")[博士學位](../Page/博士學位.md "wikilink")。

## 生平

馬臨的父親為[香港大學中文系前系主任](../Page/香港大學.md "wikilink")[馬鑑教授](../Page/馬鑑.md "wikilink")。曾就讀[嶺英中學](../Page/嶺英中學.md "wikilink")（1942年英社），畢業於[英皇書院](../Page/英皇書院.md "wikilink")（[預科](../Page/預科.md "wikilink")）。由於[太平洋戰爭爆發](../Page/太平洋戰爭.md "wikilink")，馬氏返內地。1947年獲[成都](../Page/成都.md "wikilink")[華西大學](../Page/華西大學.md "wikilink")[理學士銜](../Page/理學士.md "wikilink")。後赴[英国](../Page/英国.md "wikilink")[列斯大學深造](../Page/列斯大學.md "wikilink")，硏究蛋白化學，1955年獲哲學博士銜。繼入[倫敦大學醫學院及里兹聖占姆斯醫院](../Page/倫敦大學.md "wikilink")，從事資深博士硏究工作。

1957年任[香港大學病理學系講師](../Page/香港大學.md "wikilink")。1964年加入[香港中文大學](../Page/香港中文大學.md "wikilink")，1973年生物化學系成立，馬臨出任首任講座教授，並兼任系主任，專長[蛋白質](../Page/蛋白質.md "wikilink")[化學](../Page/化學.md "wikilink")。

1978年應聘出任香港中文大學[校長](../Page/校長.md "wikilink")， 在任校長期間，香港中文大學的發展包括：\[1\]

  - 1980年，開設博士學位課程
  - 1981年，醫學院招收第一批醫科學生
  - 1981年，推出本科兼讀課程
  - 1985年，本科課程招收首批暫取生
  - 1986年，改革本科課程，推行學分制

馬臨於1987年退休後，繼續擔任中大[逸夫書院校董會主席](../Page/逸夫書院.md "wikilink")，籌建逸夫書院。

1982年5月馬臨曾訪[北京獲](../Page/北京.md "wikilink")[鄧小平接見](../Page/鄧小平.md "wikilink")。\[2\][香港回歸前他獲委任為](../Page/香港回歸.md "wikilink")[香港基本法起草委員會委員](../Page/香港基本法起草委員會.md "wikilink")，亦是委員會轄下教科文小組及區旗、區徽小組召集人。他亦是[全國人民政治協商會議第九屆委員](../Page/中国人民政治协商会议第九届全国委员会委员名单.md "wikilink")。\[3\]\[4\]

2017年10月16日離世，終年93歲。\[5\]

## 榮譽

  - 英國[薩塞克斯大學榮譽博士](../Page/薩塞克斯大學.md "wikilink")
  - [澳門東亞大學榮譽博士](../Page/澳門東亞大學.md "wikilink")
  - [紐約州立大學榮譽博士](../Page/紐約州立大學.md "wikilink")
  - [天津大學榮譽博士](../Page/天津大學.md "wikilink")
  - 1978年，獲委為非官守[太平紳士](../Page/太平紳士.md "wikilink")（JP）
  - 1983年，獲[英帝國司令勳章](../Page/英帝國司令勳章.md "wikilink")（CBE）
  - 1987年，獲香港中文大學頒授榮休講座教授
  - 1987年，獲香港中文大學頒授榮譽法學博士
  - 2003年，獲香港中文大學頒授榮譽院士

## 家庭

馬臨的父親[馬鑑和胞兄](../Page/馬鑑.md "wikilink")[馬蒙都曾任香港大學中文系系主任](../Page/馬蒙.md "wikilink")。

馬臨的妻子[陳萌華是](../Page/陳萌華.md "wikilink")[病理學的專科醫生](../Page/病理學.md "wikilink")，1957年於香港大學醫學士畢業。馬臨外孫女林慧思指馬臨和陳萌華二人當年於聚會中一見鍾情，結婚近60年愛意未變。馬臨早年因健康問題住院時，曾表示家中有任職醫生的妻子，要求回家醫治。\[6\]

## 參考文獻

[M](../Category/香港中文大學校長.md "wikilink")
[M](../Category/香港中文大學教授.md "wikilink")
[M](../Category/鄞县人.md "wikilink")
[M](../Category/香港寧波人.md "wikilink")
[M](../Category/CBE勳銜.md "wikilink")
[L](../Category/馬姓.md "wikilink")
[Category:英皇書院校友](../Category/英皇書院校友.md "wikilink")
[Category:嶺英中學校友](../Category/嶺英中學校友.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")

1.

2.

3.
4.

5.

6.