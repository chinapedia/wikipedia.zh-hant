**天策上將**是[唐高祖李淵为嘉奖次子](../Page/唐高祖.md "wikilink")[李世民而开设的官爵](../Page/李世民.md "wikilink")。天策，[星名](../Page/恒星.md "wikilink")，即[傅说星](../Page/傅说.md "wikilink")，传说为[武丁的大臣](../Page/武丁.md "wikilink")[傅说死后托神](../Page/傅说.md "wikilink")\[1\]。

## 李世民的天策上將

[武德四年](../Page/武德.md "wikilink")，秦王[李世民在](../Page/李世民.md "wikilink")[虎牢之战中連破夏王](../Page/虎牢之战.md "wikilink")[竇建德](../Page/竇建德.md "wikilink")、鄭王[王世充两大割据势力](../Page/王世充.md "wikilink")，並俘获二人至首都[長安](../Page/長安.md "wikilink")，为唐王朝统一了中国北方。李淵认为李世民已经位列秦王、[太尉](../Page/太尉.md "wikilink")（[三公之首](../Page/三公.md "wikilink")，主管全国[军事](../Page/军事.md "wikilink")）兼[尚书令](../Page/尚书令.md "wikilink")（[尚书省长官](../Page/尚书省.md "wikilink")，[宰相之首](../Page/宰相.md "wikilink")），封无可封，且已有的官职無法彰顯其榮耀，而特设此职位，并加领[司徒](../Page/司徒.md "wikilink")（三公的第二位，主管全国教化，此时[三师和太尉之职空缺](../Page/三师.md "wikilink")，司徒实为百官之首），同时仍兼尚书令。

天策上将職位在[親王](../Page/親王.md "wikilink")、三公之上，仅次于名义上的文官之首[三师](../Page/三师.md "wikilink")（即[太师](../Page/太师.md "wikilink")、[太傅](../Page/太傅.md "wikilink")、[太保](../Page/太保.md "wikilink")）；[天策府则是武官官府之首](../Page/幕府.md "wikilink")，在[十四卫府之上](../Page/十四卫府.md "wikilink")；天策上将可以自己招募人才作为天策府中官员，即所谓的“许自置官属”。

因为当时百官之首的[三师空缺](../Page/三师.md "wikilink")，其次的天策上将为李世民；天策上将下面是亲王，秦王李世民又居首位\[2\]；亲王下面是三公，太尉空缺，司徒李世民又居首位\[3\]；三公下面、实际上的文官之首[宰相中的首位尚书令也由李世民担任](../Page/宰相.md "wikilink")，使得任天策上將兼秦王兼司徒兼尚书令的李世民不管是[爵位上还是在](../Page/爵位.md "wikilink")[职官上](../Page/职官.md "wikilink")、也不管在文臣还是武将中地位都是最高，仅次于[皇帝李渊和](../Page/皇帝.md "wikilink")[皇太子](../Page/皇太子.md "wikilink")[李建成](../Page/李建成.md "wikilink")（皇帝为君，皇太子是[储君](../Page/储君.md "wikilink")，对臣下而言都是[君主](../Page/君主.md "wikilink")）。

### 成员

天策上将这一职位是李世民与皇太子李建成角力的重要筹码，尤其是自置官属的权力给予他招募人才许多方便，比如他就曾经在[房玄龄建议下](../Page/房玄龄.md "wikilink")，将[杜淹招募到天策府](../Page/杜淹.md "wikilink")，以免他投入太子李建成阵营\[4\]。

李世民的天策府裏也聚集了各路人才。其中文臣包括：[长孙无忌](../Page/长孙无忌.md "wikilink")、[杜如晦](../Page/杜如晦.md "wikilink")、[房玄龄](../Page/房玄龄.md "wikilink")、[虞世南](../Page/虞世南.md "wikilink")、[孔颖达](../Page/孔颖达.md "wikilink")、[姚思廉](../Page/姚思廉.md "wikilink")、[薛元敬](../Page/薛元敬.md "wikilink")、[薛收](../Page/薛收.md "wikilink")、[杜淹](../Page/杜淹.md "wikilink")、[许敬宗](../Page/许敬宗.md "wikilink")、[盖文达](../Page/盖文达.md "wikilink")、[陆德明](../Page/陆德明.md "wikilink")、[蔡允恭](../Page/蔡允恭.md "wikilink")、[苏勖](../Page/苏勖.md "wikilink")、[颜师古](../Page/颜师古.md "wikilink")、[颜相时](../Page/颜相时.md "wikilink")、[于志宁](../Page/于志宁.md "wikilink")、[苏世长](../Page/苏世长.md "wikilink")、[李守素](../Page/李守素.md "wikilink")、[阎立本](../Page/阎立本.md "wikilink")、[高士廉](../Page/高士廉.md "wikilink")、[温大雅](../Page/温大雅.md "wikilink")、[刘政会等](../Page/刘政会.md "wikilink")。武将包括：[尉迟敬德](../Page/尉迟敬德.md "wikilink")、[侯君集](../Page/侯君集.md "wikilink")、[段志玄](../Page/段志玄.md "wikilink")、[程咬金](../Page/程咬金.md "wikilink")、[罗士信](../Page/罗士信.md "wikilink")、[长孙顺德](../Page/长孙顺德.md "wikilink")、[秦叔宝](../Page/秦叔宝.md "wikilink")、[丘行恭](../Page/丘行恭.md "wikilink")、[丘师利](../Page/丘师利.md "wikilink")、[史万宝](../Page/史万宝.md "wikilink")、[刘弘基](../Page/刘弘基.md "wikilink")、[柴绍](../Page/柴绍.md "wikilink")、[张亮](../Page/张亮.md "wikilink")、[刘师立](../Page/刘师立.md "wikilink")、[屈突通](../Page/屈突通.md "wikilink")、[张士贵](../Page/张士贵.md "wikilink")、[独孤彦云](../Page/独孤彦云_\(溧阳县公\).md "wikilink")、[张公谨](../Page/张公谨.md "wikilink")、[史大奈](../Page/史大奈.md "wikilink")、[公孙武达](../Page/公孙武达.md "wikilink")、[杜君绰](../Page/杜君绰.md "wikilink")、[郑仁泰](../Page/郑仁泰.md "wikilink")、[殷开山](../Page/殷开山.md "wikilink")、[长孙安业等](../Page/长孙安业.md "wikilink")。

[武德九年](../Page/武德.md "wikilink")[玄武门之变后](../Page/玄武门之变.md "wikilink")，李世民为皇太子，已经不需要这个臣子的职位，因此六月廿九乙酉日，天策府被废除，有唐一代未再置。

### 李世民的天策上将府官制

（参考《[旧唐书](../Page/旧唐书.md "wikilink")》卷四十二 职官志一）

  - 天策上将：一人，正第一品，负责唐对内外的军事作战，府主，总判府事。
  - 天策府长史：一人，从第三品，管理府中各类事务。
  - 天策府司马：一人，从第三品，管理府中各类事务。
  - 天策府从事中郎；二人，从第四品下，协助长史、司马管理府中各类事务。
  - 天策上将军谘祭酒：二人，正第五品下，军事参谋人员，同时负责指挥礼仪、接待宾客事务。
  - 天策府主簿：从第五品下，二人，负责起草天策上将的教令、命令。
  - 天策府记室参军事：从第五品下，二人，负责公文（书、疏、表、启）往来，负责发出天策上将的教令、命令。
  - 天策府诸曹参军事：正第六品下，每曹二人，共十二人，分别为：

<!-- end list -->

  -

      -
        功曹参军事二人，掌管府中官员请假、出差、礼仪、医药、选拔、考课、工资、福利、铺设等事。
        仓曹参军事二人，掌管府中库、食堂、厨房、和证件“[过所](../Page/过所.md "wikilink")”的发放等事。
        兵曹参军事二人，掌管府中士兵名册、考勤。
        骑曹参军事二人，掌管府中牲畜畜牧。
        铠曹参军事二人，掌管府中兵器。
        士曹参军事二人，掌管府中建造和处罚府中士兵。

<!-- end list -->

  - 天策府参军事：正第七品下，六人，掌管出差及其他检校之事。
  - 天策府典签：正第八品上，四人，协助掌管传达教命、导引宾客之事。
  - 天策府录事：正第九品上，二人，协助掌管书疏表启，传达、执行教命。

## 后世的天策上将军

[天策府宝.jpg](https://zh.wikipedia.org/wiki/File:天策府宝.jpg "fig:天策府宝.jpg")
[五代](../Page/五代.md "wikilink")[后梁](../Page/后梁.md "wikilink")[太祖](../Page/朱溫.md "wikilink")[开平四年](../Page/開平_\(年號\).md "wikilink")（910年）六月，[马殷求授天策上将](../Page/马殷.md "wikilink")，詔加天策上将军，亦开天策府\[5\]。马殷曾于[-{乾}-化元年](../Page/乾化.md "wikilink")（911年）铸[天策府宝大钱](../Page/天策府宝.md "wikilink")，有铜钱、铁钱二种，铜钱有背龙铜钱及鎏金铜钱等特殊种类，非常罕见。

[宋真宗](../Page/宋真宗.md "wikilink")[大中祥符八年](../Page/大中祥符.md "wikilink")（1015年），封宋真宗兄、楚王[赵元佐为天策上将军](../Page/赵元佐.md "wikilink")，不开[幕府](../Page/幕府.md "wikilink")，后荆王[赵元俨死](../Page/赵元俨.md "wikilink")，亦赠天策上将军\[6\]。

## 腳注

[Category:唐朝官制](../Category/唐朝官制.md "wikilink")
[Category:唐太宗](../Category/唐太宗.md "wikilink")
[Category:中国古代将军称号](../Category/中国古代将军称号.md "wikilink")
[TCSJ](../Category/中國特殊官爵.md "wikilink")

1.  《左传》僖公五年疏
2.  当时的亲王除了秦王李世民外，还有吴王[杜伏威](../Page/杜伏威.md "wikilink")（赐姓李，位在齐王李元吉之上）、齐王[李元吉](../Page/李元吉.md "wikilink")、赵王[李元景](../Page/李元景.md "wikilink")、鲁王[李元昌](../Page/李元昌.md "wikilink")、韩王[李元嘉](../Page/李元嘉.md "wikilink")、霍王[李元轨等](../Page/李元轨.md "wikilink")。
3.  齐王[李元吉则任](../Page/李元吉.md "wikilink")[司空](../Page/司空.md "wikilink")
4.  《[資治通鑑](../Page/資治通鑑.md "wikilink")》卷一百八十九·唐紀五·高祖武德四年
5.  《资治通鉴》卷二百六十七·后梁纪二·太祖开平四年
6.  《[宋史](../Page/宋史.md "wikilink")》列传第四