**濁軟顎塞音**是[輔音的一種](../Page/輔音.md "wikilink")，用於一些[口語中](../Page/口語.md "wikilink")。濁軟顎塞音在國際音標的符號是，X-SAMPA音標的符號則是。

嚴格來說，國際音標裡表示此音的符號是尾巴打開的[Opentail_g.svg](https://zh.wikipedia.org/wiki/File:Opentail_g.svg "fig:Opentail_g.svg")，但是尾巴關閉的[Looptail_g.svg](https://zh.wikipedia.org/wiki/File:Looptail_g.svg "fig:Looptail_g.svg")亦可以接受。[Unicode中的字碼](../Page/Unicode.md "wikilink")「[Latin
small letter
G](../Page/G.md "wikilink")」(U+0067)會因應字型的不同而顯示出尾巴打開或者關閉的g。字碼「Latin
small letter script G」(U+0261)則不論字體，一律為尾巴打開的ɡ，但此字碼只會出現在國際音標的延伸字型。

在六個最普遍的六個[塞音](../Page/塞音.md "wikilink")中，[清雙唇塞音](../Page/清雙唇塞音.md "wikilink")和濁軟顎塞音在不少語言中都已經消失，數量是有這些輔音的語言的百分之十。消失的原因是地域性的影響，但消失卻並非集中於某一區，而是廣佈在世界各地。（有幾種語言兩個音都缺乏，例如[現代標準阿拉伯語和西伯利亞的](../Page/阿拉伯語.md "wikilink")[凱特語](../Page/葉尼塞語系.md "wikilink")*(Ket)*）有人認為這是因為比其餘五個塞音都難發音。發[濁音時氣流從肺流出](../Page/濁音.md "wikilink")，而[軟顎音軟顎關閉](../Page/軟顎音.md "wikilink")，喉嚨和軟顎間的空間較小，空氣很快就填滿該位置，令到清音的聲音聽起來就像濁音。故此和兩者的分別沒有其餘兩對的那麼明顯。這限制令原本有、之分的語言因無法分辨兩個音，以致逐漸消失；或者令語言成形時就忽略了這個音（如[侗台语族里大多数语言](../Page/侗台语族.md "wikilink")[泰语](../Page/泰语.md "wikilink")、[壮语](../Page/壮语.md "wikilink")、[老挝语等](../Page/老挝语.md "wikilink")）。留意像[清小舌塞音](../Page/清小舌塞音.md "wikilink")和[濁小舌塞音](../Page/濁小舌塞音.md "wikilink")兩者中，因為喉門（即小舌所在的地方）更接近喉嚨，空間更小，兩者分別更不明顯，令比罕有得多。

漢語中[北京語和](../Page/北京語.md "wikilink")[粵語均無此音](../Page/粵語.md "wikilink")。[吳語中有](../Page/吳語.md "wikilink")，文讀全濁日常口語時全濁時半濁音，或可被視為濁音清化之兆，受上海等地學者及社會青年影響，得以止清化，漸取成效。

## 特徵

濁軟顎塞音的特點：

## 濁軟顎塞音的變種

<table>
<thead>
<tr class="header">
<th><p>音標</p></th>
<th><p>種類</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>原位 g</p></td>
</tr>
<tr class="even">
<td><p><small>或</small> </p></td>
<td><p><a href="../Page/漏氣濁音.md" title="wikilink">漏氣</a> g</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/顎音化.md" title="wikilink">顎音</a> g</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/圓唇音.md" title="wikilink">圓唇</a> g</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/唯閉音.md" title="wikilink">唯閉</a> g</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/清音.md" title="wikilink">清音化</a> g</p></td>
</tr>
</tbody>
</table>

## 該音見於英語

[英語中有此音](../Page/英語.md "wikilink")，以字母g表示。但g並不一定是表示此音的，只有單獨出現在a、o、u前才會讀/g/。如果g出現在e,
i,
y前，會讀[濁齒齦後破擦音](../Page/濁齒齦後破擦音.md "wikilink")/dʒ/（這項有例外，如*finger*）；如果和n一起成ng出現在[語素末](../Page/語素.md "wikilink")，則讀成[軟顎鼻音](../Page/軟顎鼻音.md "wikilink")/ŋ/，如*singing*，兩個ng都讀/ŋ/，因為sing和ing兩個都表示一個意思的單位（sing是「唱歌」；ing是表示現在分詞的後綴。)；*ginger*的ng則分開讀，成/ndʒ/，因為ging並不是一個完整的語素，只有ginger才是。

## 該音見於其他語言

因发音较困难容易和/k/混淆的缘故，该音位在一些语言中已不复存在，但/g/相對其他音仍然頗為普遍，許多語言都有原位/g/音，有的甚至有多於一種/g/的變種。許多[印度的語言](../Page/印度.md "wikilink")，尤其是[印地語](../Page/印地語.md "wikilink")，都有區分原位/g/和送氣/gʰ/。

[Category:軟齶音](../Category/軟齶音.md "wikilink")
[Category:塞音](../Category/塞音.md "wikilink")