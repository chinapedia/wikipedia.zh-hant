《**美麗人生**》（，意為「人生是美好」），1997年[電影](../Page/電影.md "wikilink")，由導演[羅貝托·貝尼尼自編自演](../Page/羅貝托·貝尼尼.md "wikilink")，榮獲[奧斯卡最佳男主角](../Page/奥斯卡金像奖.md "wikilink")、[欧洲电影奖最佳影片及多個國際大奬](../Page/欧洲电影奖.md "wikilink")。電影講述[意大利一對](../Page/意大利.md "wikilink")[猶太父子被送進](../Page/猶太人.md "wikilink")[納粹集中營](../Page/納粹集中營.md "wikilink")，父親Guido
Orefice（導演[羅貝托·貝尼尼飾](../Page/羅貝托·貝尼尼.md "wikilink")）不忍年僅五歲的兒子飽受驚恐，利用自己豐富的想像力扯謊說他們正身處一個遊戲當中，必須接受集中營中種種規矩以換得分數贏取最後大奬。影片笑中有淚，將一個大時代小人物的故事，轉化為一個扣人心弦的[悲喜劇](../Page/悲喜劇.md "wikilink")。

## 劇情

電影內容敘述[二次大戰時期](../Page/二次世界大戰.md "wikilink")，德國將[猶太人送往集中營](../Page/猶太人.md "wikilink")，對[猶太人管制](../Page/猶太人.md "wikilink")、虐待、屠殺的故事。故事並沒有以血腥的畫面呈現德國納粹的殘暴，而是以喜劇的方式讓觀眾領悟生命的真諦。

一名義大利猶太裔青年基多（Guido），來到了[義大利的Arezzo](../Page/義大利.md "wikilink")。基多是一名幽默風趣而吸引人的青年，他愛上了一名教師朵拉（Dora），基多使用各種方式希望追到朵拉，而朵拉則是已經有了婚約，不過基多仍是在朵拉的生活中添加許多驚奇，朵拉也漸漸愛上基多，基多更是在朵拉的訂婚宴上搶婚。兩人多年之後有了第一個兒子－約書亞（Joshua），基多也開了他夢寐以求的書店，只是基多夢寐以求的日子沒有持續太久，一切夢想在一家人送往納粹集中營後幻滅。

天性幽默的基多不願毀了兒子的天真無邪，由於兩人都有猶太人的血統，兩人被送往了集中營，朵拉则要求一同前往而被送到女子集中營。基多故意編故事遊戲來騙約書亞，只要誰先能得到1000分就能得到一部真的坦克，使得約書亞不但適應集中營的生活，更逃過了被送去毒氣室毒死的命運。基多也要讓朵拉知道兩人還活著，讓朵拉能夠有活下去的希望。在德軍投降的前一晚，基多為了不讓兒子受害，再度編遊戲讓兒子躲到箱子當中，慢慢等待美軍的到來，約書亞照做了，但是基多在尋找朵拉時卻被德軍發現了，在被處死之前基多仍是要讓孩子以為這一切是個遊戲，基多最後則是死在納粹守衛的亂槍之下，翌日，美軍抵達，約書亞按照與爸爸的約定從箱子中出來，他見到了美軍的[M4雪曼坦克](../Page/M4雪曼坦克.md "wikilink")，認為自己贏得了遊戲，最後約書亞也與母親重逢。

## 角色

  - [Roberto Benigni](../Page/Roberto_Benigni.md "wikilink") - Guido
    Orefice
  - [Nicoletta Braschi](../Page/Nicoletta_Braschi.md "wikilink") - Dora
  - [Giorgio Cantarini](../Page/Giorgio_Cantarini.md "wikilink") -
    Joshua，儿子
  - [Giustino Durano](../Page/Giustino_Durano.md "wikilink") - Uncle
    Eliseo
  - [Horst Buchholz](../Page/Horst_Buchholz.md "wikilink") - 德国医生
    Lessing，喜欢[猜谜语](../Page/猜谜语.md "wikilink")
  - [Marisa Paredes](../Page/Marisa_Paredes.md "wikilink") - Dora's
    mother
  - [Sergio Bustric](../Page/Sergio_Bustric.md "wikilink") - Ferruccio
  - [Amerigo Fontani](../Page/Amerigo_Fontani.md "wikilink") - Rodolfo

## 奖项

  - 1998年[坎城影展评审团大奖](../Page/坎城影展.md "wikilink")
  - [意大利电影金像奖最佳导演](../Page/意大利电影金像奖.md "wikilink")、最佳男主角
  - [銀緞帶獎最佳男主角](../Page/銀緞帶獎.md "wikilink")
  - [欧洲电影奖最佳影片](../Page/欧洲电影奖.md "wikilink")、最佳男主角
  - [广播影评人协会奖最佳外语片](../Page/广播影评人协会奖.md "wikilink")
  - [凯撒电影奖最佳外语片](../Page/凯撒电影奖.md "wikilink")
  - [AFI电影奖最佳外语片](../Page/AFI电影奖.md "wikilink")
  - [英国电影学院奖最佳男主角](../Page/英国电影学院奖.md "wikilink")
  - [美国演员工会奖电影类最佳男主角](../Page/美国演员工会奖.md "wikilink")

第71屆[奥斯卡奖](../Page/奥斯卡奖.md "wikilink")

  - [最佳电影](../Page/奥斯卡最佳电影奖.md "wikilink") 提名
  - [最佳导演](../Page/奥斯卡最佳导演奖.md "wikilink") 提名 罗伯托·贝尼尼
  - '''[最佳男主角](../Page/奥斯卡最佳男主角奖.md "wikilink") 获奖
    [罗伯托·贝尼尼](../Page/罗伯托·贝尼尼.md "wikilink") '''
  - [最佳剪辑](../Page/奥斯卡最佳剪辑奖.md "wikilink") 提名 Simona Paggi
  - [最佳原創劇本](../Page/奧斯卡最佳原創劇本獎.md "wikilink") 提名 文森佐·克拉米、罗伯托·贝尼尼
  - **[奥斯卡最佳原创音乐奖](../Page/奥斯卡最佳原创音乐奖.md "wikilink") 获奖
    [尼古拉·皮奥瓦尼](../Page/尼古拉·皮奥瓦尼.md "wikilink")**

## 票房成绩

全美首映票房：$3,244,284.00 (单位:美元) 　　

全美累计票房：$57,598,247.00 (单位:美元) 　　

海外累计票房：$171,600,000.00 (单位:美元)

## 花絮

  - 在片中扮演罗伯托·贝尼尼(Roberto
    Benigni)妻子的[尼可莱塔·布拉斯基](../Page/尼可莱塔·布拉斯基.md "wikilink")(Nicoletta
    Braschi)，在生活中也是他的妻子。

<!-- end list -->

  - 贝尼尼在狱中穿的衣服的号码跟[卓别林在](../Page/卓别林.md "wikilink")《[大独裁者](../Page/大独裁者.md "wikilink")》中的一样。

<!-- end list -->

  - 贝尼尼说片名来自利昂·托洛茨基(Leon
    Trotsky)说所的话。[托洛茨基在墨西哥流放时期](../Page/托洛茨基.md "wikilink")，得知他要被[斯大林暗杀时](../Page/斯大林.md "wikilink")，他看着花园中的妻子写出了这样的话，无论如何：“人生是美丽的。”

<!-- end list -->

  - 贝尼尼凭此片获得奥斯卡最佳男主角奖。这是奥斯卡历史上第二次由自导自演的人荣获表演奖，第一次是[勞倫斯·奧立佛](../Page/勞倫斯·奧立佛.md "wikilink")1948年的电影《[哈姆雷特](../Page/哈姆雷特_\(1948年电影\).md "wikilink")》。

## 外部連結

  -
  -
  -
  - *[Life is
    Beautiful](https://web.archive.org/web/20060422151505/http://www.artsandfaith.com/t100/2005/entry.php?film=95)*
    at the [Arts & Faith Top100 Spiritually Significant
    Films](https://web.archive.org/web/20060720142105/http://artsandfaith.com/top100/)
    list

[Category:1997年電影](../Category/1997年電影.md "wikilink")
[Category:意大利語電影](../Category/意大利語電影.md "wikilink")
[Category:德语电影](../Category/德语电影.md "wikilink")
[Category:1990年代劇情片](../Category/1990年代劇情片.md "wikilink")
[Category:1990年代喜剧片](../Category/1990年代喜剧片.md "wikilink")
[Category:義大利劇情片](../Category/義大利劇情片.md "wikilink")
[Category:意大利喜剧片](../Category/意大利喜剧片.md "wikilink")
[Category:猶太人大屠殺電影](../Category/猶太人大屠殺電影.md "wikilink")
[Category:1990年代喜劇劇情片](../Category/1990年代喜劇劇情片.md "wikilink")
[Category:意大利背景电影](../Category/意大利背景电影.md "wikilink")
[Category:意大利取景电影](../Category/意大利取景电影.md "wikilink")
[Category:米拉麦克斯电影](../Category/米拉麦克斯电影.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:奧斯卡最佳外語片獲獎電影](../Category/奧斯卡最佳外語片獲獎電影.md "wikilink")
[Category:奥斯卡最佳原创配乐获奖电影](../Category/奥斯卡最佳原创配乐获奖电影.md "wikilink")
[Category:欧洲电影奖最佳影片](../Category/欧洲电影奖最佳影片.md "wikilink")
[Category:美國演員工會獎電影類最佳男主角獲獎作品](../Category/美國演員工會獎電影類最佳男主角獲獎作品.md "wikilink")
[Category:凯撒电影奖最佳外语片](../Category/凯撒电影奖最佳外语片.md "wikilink")
[Category:英国电影学院奖最佳男主角获奖电影](../Category/英国电影学院奖最佳男主角获奖电影.md "wikilink")
[Category:欧洲电影奖最佳男主角获奖电影](../Category/欧洲电影奖最佳男主角获奖电影.md "wikilink")