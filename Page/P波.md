**P波**（）是在地震发生时最先被地震仪记录下来的地震体波。穿越[地球內部的波](../Page/地球.md "wikilink")（例如，地震波）被稱為[體波](../Page/體波.md "wikilink")。相對於體波的是[表面波](../Page/表面波.md "wikilink")。体波可以分为P波和[S波](../Page/S波.md "wikilink")。P波意指（primary
wave）或是[壓力波](../Page/壓力.md "wikilink")（pressure
wave）。在所有[地震波中](../Page/地震波.md "wikilink")，P波傳遞速度最快。因此發生地震時，P波最早抵達測站，並被[地震儀紀錄下來](../Page/地震儀.md "wikilink")，這也是P波名稱的由來。P波的P也能代表[壓力](../Page/壓力.md "wikilink")（pressure），來自於其震動傳遞類似[聲波](../Page/聲波.md "wikilink")，屬於[縱波的一種](../Page/縱波.md "wikilink")（或[疏密波](../Page/疏密波.md "wikilink")），傳遞時介質的震動方向與震波能量的傳播方向[平行](../Page/平行.md "wikilink")。

[Earthquake_wave_shadow_zone.png](https://zh.wikipedia.org/wiki/File:Earthquake_wave_shadow_zone.png "fig:Earthquake_wave_shadow_zone.png")）\]\]

對於地球內部構造的瞭解和推論，大部分是藉由觀測[地震波中的體波](../Page/地震波.md "wikilink")。地震波在不同[介質有不同傳播時間和路徑](../Page/介質.md "wikilink")，在介質交界面時會引起[反射](../Page/反射.md "wikilink")、[折射](../Page/折射.md "wikilink")，以及[相位的改變](../Page/相位.md "wikilink")。地震學家利用這些特性來獲得地球內部資訊。當體波穿越地球液態層時，P波在經過下部[地函與外](../Page/地函.md "wikilink")[地核時會稍許折射](../Page/地核.md "wikilink")。造成P波在104°至140°間\[1\]會有陰影區，令地震儀無法記錄。

## 速度

[均质](../Page/均质.md "wikilink")[各向同性介质的P波速度由下列公式给出](../Page/各向同性.md "wikilink")：

\[v_p= \sqrt{ \frac {K+\frac{4}{3}\mu} {\rho}}= \sqrt{ \frac{\lambda+2\mu}{\rho}}\]
其中*K*是[体积模量](../Page/体积模量.md "wikilink")（不可压缩性的模量），*\(\mu\)*是[剪切模量](../Page/剪切模量.md "wikilink")（刚性度的模量，常记为*G*，又称第二[拉梅参数](../Page/拉梅参数.md "wikilink")），*\(\rho\)*是波传播介质材料的[密度](../Page/密度.md "wikilink")，*\(\lambda\)*是第一[拉梅参数](../Page/拉梅参数.md "wikilink")。

由于密度的变化很小，速度主要由*K*和*μ*决定。

[弹性模量中的](../Page/弹性模量.md "wikilink")[P波模量](../Page/P波模量.md "wikilink")\(M\)定义为\(M = K + 4\mu/3\)，因此

\[v_p = \sqrt{M/\rho}\\]

地震中P波速度典型值的范围是5至8 km/s。\[2\]精确速度以地球内部区域的不同而变化，范围从地壳的不到6 km/s至地核的13
km/s。\[3\]

## 參考文獻

<references/>

## 參見

  - [S波](../Page/S波.md "wikilink")
  - [縱波](../Page/縱波.md "wikilink")

## 外部連結

[中華民國中央氣象局全球資訊網](http://www.cwb.gov.tw/index.htm)

[da:P-bølger](../Page/da:P-bølger.md "wikilink")

[Category:地球物理学](../Category/地球物理学.md "wikilink")
[Category:地震學](../Category/地震學.md "wikilink")

1.  [Earthquake Hazards Program](http://earthquake.usgs.gov/), USGS
2.  <http://hypertextbook.com/facts/2001/PamelaSpiegel.shtml>
3.