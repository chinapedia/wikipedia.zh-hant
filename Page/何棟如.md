**何棟如**（），[字](../Page/表字.md "wikilink")**充符**，一字**子極**，[號](../Page/號.md "wikilink")**天玉**，[明代官员](../Page/明代.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

何棟如之先祖世居[江蘇無錫](../Page/江蘇.md "wikilink")，明[洪武三十年](../Page/洪武.md "wikilink")（1397年）徙居京師（今南京市），祖父[何汝健於](../Page/何汝健.md "wikilink")[嘉靖三十二年](../Page/嘉靖.md "wikilink")（1553年）中進士，父[何湛之於](../Page/何湛之.md "wikilink")[萬曆十七年](../Page/萬曆.md "wikilink")（1589年）中進士。何棟如出生於[隆慶六年](../Page/隆慶_\(明\).md "wikilink")（1572年）[七月初八](../Page/七月初八.md "wikilink")，隨父寄寓山寺讀書，[萬曆二十六年](../Page/萬曆.md "wikilink")（1598年）中進士，三代皆為進士，後來他被任命為[湖广](../Page/湖广.md "wikilink")[襄阳府](../Page/襄阳府.md "wikilink")[推官](../Page/推官.md "wikilink")，與[张宾王等素有往來](../Page/张宾王.md "wikilink")。萬曆三十二年（1604年）九月二十二，因被[陳奉誣陷](../Page/陳奉.md "wikilink")“击杀天子税使”，皇帝下詔“星夜扭解来京究问”，襄阳人赴阙为何栋如诉冤，出狱后削籍回鄉。

萬曆四十八年（1620年），[後金兵连克](../Page/後金.md "wikilink")[开原](../Page/开原.md "wikilink")、[铁岭](../Page/铁岭.md "wikilink")（今[辽宁铁岭](../Page/辽宁.md "wikilink")），何棟如上《[越纓疏](../Page/越纓疏.md "wikilink")》請籌兵，自稱“系[常州府無錫縣軍籍](../Page/常州府.md "wikilink")”人。[天启元年](../Page/天启.md "wikilink")（1621年）四月十一，起用为[南京](../Page/南京.md "wikilink")[兵部](../Page/兵部.md "wikilink")[主事](../Page/主事.md "wikilink")，到[杭州一带招募新兵](../Page/杭州.md "wikilink")，获得六千七百余人。此时广宁失陷，何栋如继续请求出关考查形势。乃进太仆少卿，充军前赞画。天启二年（1622年）至[山海关](../Page/山海关.md "wikilink")，稳定军民情绪，重新布置防线，後金兵未敢妄動，但所召新兵惧怕出关，多逃亡。天启五年（1625年）三月六日，因上《[清诛奸党疏](../Page/清诛奸党疏.md "wikilink")》得罪[魏忠賢](../Page/魏忠賢.md "wikilink")，以侵吞[军饷罪被逮进京](../Page/军饷.md "wikilink")，“五彪”之一的[许显纯對他滥施酷刑](../Page/许显纯.md "wikilink")，他寧死不屈，最後被發配滁阳。

崇禎時，魏忠賢伏誅，何栋如赦還，闲居南京。[崇禎六年](../Page/崇禎.md "wikilink")（1633年），何栋如上《[恢复辽东六议](../Page/恢复辽东六议.md "wikilink")》；崇禎十年（1637年）又上《[留都增设城垣疏](../Page/留都增设城垣疏.md "wikilink")》，受到[兵部尚书](../Page/兵部尚书.md "wikilink")[范景文的重视](../Page/范景文.md "wikilink")，[刘宗周亦推薦他](../Page/刘宗周.md "wikilink")。然而不久後，何棟如終病逝南京。著作有《文庙雅乐考》两卷和《皇明四大法》十二卷

## 参考资料

  - 明史·列传第一百二十五·何栋如

[Category:明朝襄陽府推官](../Category/明朝襄陽府推官.md "wikilink")
[Category:南京兵部主事](../Category/南京兵部主事.md "wikilink")
[Category:無錫人](../Category/無錫人.md "wikilink")
[D棟](../Category/何姓.md "wikilink")