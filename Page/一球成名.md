《**-{zh-hans:一球成名;zh-hk:一球成名;zh-tw:疾風禁區;}-**》（***Goal\!***，美國上映名稱為***Goal\!
The Dream
Begins***）是一套2005年上映的[美國](../Page/美國.md "wikilink")[電影](../Page/電影.md "wikilink")，由[丹尼·卡农負責擔任電影](../Page/丹尼·卡农.md "wikilink")[導演](../Page/導演.md "wikilink")。

電影故事講述一位年輕的[墨西哥足球員](../Page/墨西哥.md "wikilink") Santiago Muñez
夢想成為一位偉大足球員以及進軍國際球壇的三部曲電影中的首部曲，電影主角紐尼斯是一個偷渡至[美國的](../Page/美國.md "wikilink")[墨西哥家庭中的小伙子](../Page/墨西哥.md "wikilink")，居住於[洛杉磯的他擁有天賦的球技](../Page/洛杉磯.md "wikilink")，在他加入的球隊中擔任主力得分射手，曾經是[英超球會](../Page/英超.md "wikilink")[纽卡斯尔联射手的球探](../Page/纽卡斯尔联足球俱乐部.md "wikilink")
Glen Foy 在一場球賽無意中發現他精湛的球技，結果 Glen Foy
幾經辛苦獲得現任紐卡素領隊同意讓紐尼斯加入球會，及後成功地在英超為球隊上陣，並射入一個奠定勝的入球。

本電影獲得[國際足協全力支持拍攝](../Page/國際足協.md "wikilink")，並耗資多達一億美元完成電影，原因是將真實的比賽環境及球隊加入電影內以及電影續集會移師西班牙球會[皇家馬德里為電影籃本](../Page/皇家馬德里.md "wikilink")。

## 演員表

  - [库诺·贝克](../Page/库诺·贝克.md "wikilink")（Kuno Becker）- Santiago Munez
  - [亚力桑德罗·尼沃拉](../Page/亚力桑德罗·尼沃拉.md "wikilink")（Alessandro Nivola）-
    Gavin Harris
  - [马塞尔·尤里斯](../Page/马塞尔·尤里斯.md "wikilink")（Marcel Iures）- Erik
    Dornhelm
  - [斯蒂芬·迪兰](../Page/斯蒂芬·迪兰.md "wikilink")（Stephen Dillane）- Glen Foy
  - [安娜·弗瑞尔](../Page/安娜·弗瑞尔.md "wikilink")（Anna Friel）- Roz Harmison
  - [柯瑞安·奥布伦](../Page/柯瑞安·奥布伦.md "wikilink")（Kieran O'Brien）- Hughie
    McGowan
  - [西恩·帕特维](../Page/西恩·帕特维.md "wikilink")（Sean Pertwee）- Barry Rankin

## 客串演出

本電影獲得多位世界級球星及領隊客串演出，包括：

  - [-{zh-hans:齐达内;zh-hk:施丹;zh-tw:席丹;}-](../Page/齐内丁·齐达内.md "wikilink")
  - [-{zh-hans:劳尔; zh-hk:魯爾;}-](../Page/劳尔·冈萨雷斯·布兰科.md "wikilink")
  - [-{zh-hans:贝克汉姆;zh-hk:碧咸;zh-tw:貝克漢;}-](../Page/大卫·贝克汉姆.md "wikilink")
  - [-{zh-hans:希勒; zh-hk:舒利亞;}-](../Page/阿兰·希勒.md "wikilink")
  - [-{zh-hans:埃里克松; zh-hk:艾歷臣;}-](../Page/斯文·约兰·埃里克松.md "wikilink")
  - [-{zh-hans:罗纳尔迪尼奥; zh-hk:朗拿甸奴;}-](../Page/罗纳尔迪尼奥.md "wikilink")
  - [-{zh-hans:基隆·代爾; zh-hk:戴亞;}-](../Page/基隆·代尔.md "wikilink")
  - [-{zh-hans:贝尼特斯; zh-hk:賓尼迪斯;}-](../Page/拉法埃尔·贝尼特斯.md "wikilink")
  - [-{zh-hans:克鲁伊维特;zh-hk:古華特;zh-tw:克魯伊維特;}-](../Page/帕特里克·克鲁伊维特.md "wikilink")
  - [卡卡](../Page/卡卡.md "wikilink")
  - [-{zh-hans:保罗·孔切斯基; zh-hk:高哲斯基;}-](../Page/保罗·孔切斯基.md "wikilink")
  - [-{zh-hans:巴罗什; zh-hk:巴路士;}-](../Page/米兰·巴罗什.md "wikilink")
  - [-{zh-hans:謝拉特; zh-hk:謝拉特;}-](../Page/謝拉特.md "wikilink")
  - [-{zh-hans:亨利; zh-hk:亨利;}-](../Page/亨利.md "wikilink")

## 上映日期

  - 2005年9月8日（電影節首映場）- [法國](../Page/法國.md "wikilink")
  - 2005年9月29日－[以色列](../Page/以色列.md "wikilink")
  - 2005年9月30日－[丹麥](../Page/丹麥.md "wikilink")，[愛沙尼亞](../Page/愛沙尼亞.md "wikilink")，[芬蘭](../Page/芬蘭.md "wikilink")，[英國](../Page/英國.md "wikilink")
  - 2005年10月6日－[匈牙利](../Page/匈牙利.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")
  - 2005年10月7日－[拉脫維亞](../Page/拉脫維亞.md "wikilink")，[瑞典](../Page/瑞典.md "wikilink")，[土耳其](../Page/土耳其.md "wikilink")
  - 2005年10月9日－[墨西哥](../Page/墨西哥.md "wikilink")
  - 2005年10月12日－[法國](../Page/法國.md "wikilink")
  - 2005年10月13日－[瑞士](../Page/瑞士.md "wikilink")（德文版本）
  - 2005年10月14日－[西班牙](../Page/西班牙.md "wikilink")
  - 2005年10月19日－[瑞士](../Page/瑞士.md "wikilink")（法文版本）,
    [比利時](../Page/比利時.md "wikilink")
  - 2005年10月20日－[荷蘭](../Page/荷蘭.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")
  - 2005年10月21日－[意大利](../Page/意大利.md "wikilink")，[瑞士](../Page/瑞士.md "wikilink")（意大利文版本）
  - 2005年10月27日－[德國](../Page/德國.md "wikilink")
  - 2005年10月28日－[哥倫比亞](../Page/哥倫比亞.md "wikilink")
  - 2005年11月2日－[菲律賓](../Page/菲律賓.md "wikilink")
  - 2006年2月16日－[澳洲](../Page/澳洲.md "wikilink")
  - 2006年3月11日－[中国大陆](../Page/中国大陆.md "wikilink")
  - 2006年5月12日－[美國](../Page/美國.md "wikilink")
  - 2006年5月27日－[日本](../Page/日本.md "wikilink")

## 外部連結

  - [疾風禁區官方網站](https://web.archive.org/web/20050926155947/http://goalthemovie.com/)

  - [Official 疾風禁區美國版官方網站](http://goal.movies.go.com/main.html?lang=us)

  -
  - [紐卡素官方網站](http://www.nufc.co.uk/)

  - [BBC對疾風禁區的評論](http://www.bbc.co.uk/films/2005/09/05/goal_2005_review.shtml)

  - [歐洲足協對疾風禁區的評論](http://www.uefa.com/magazine/news/Kind=128/newsId=375388.html)

[Category:2005年電影](../Category/2005年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:足球電影](../Category/足球電影.md "wikilink")
[Category:迪士尼電影](../Category/迪士尼電影.md "wikilink")
[Category:上海电影译制厂译制作品](../Category/上海电影译制厂译制作品.md "wikilink")
[Category:丹尼·坎南電影](../Category/丹尼·坎南電影.md "wikilink")