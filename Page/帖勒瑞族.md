在[托爾金](../Page/托爾金.md "wikilink")（J. R. R.
Tolkien）的作品裡，帖勒瑞族（Teleri）是最具歌唱天赋的精靈族群，他们亦自称林达（Lindar，歌咏者），著名的[海上精靈](../Page/海上精靈.md "wikilink")（Falmari）。帖勒瑞族是精靈三族裡最大的部族，是最后抵達[阿門洲](../Page/阿門洲.md "wikilink")（Aman）的精靈族群。傳說中，帖勒瑞族是由埃奈爾（Enel）建立的，他是第三位在[庫維因恩](../Page/庫維因恩.md "wikilink")（Cuiviénen）甦醒的精靈，他的妻子叫埃奈雅（Enelyë）。首位跟隨[歐羅米](../Page/歐羅米.md "wikilink")（Oromë）的帖勒瑞族精靈是[埃爾威](../Page/埃盧·庭葛.md "wikilink")（Elwë），埃爾威成為帖勒瑞族君王，帶領帖勒瑞族人前往[維林諾](../Page/維林諾.md "wikilink")。[大远行](../Page/大远行.md "wikilink")（Great
Journey）时期，他的兄弟[奥爾维](../Page/奥爾维.md "wikilink")（Olwë）接替他成為帖勒瑞族君王，帶領大部分帖勒瑞族人前往維林諾。

大远行途中，埃爾威在艾莫斯谷（Nan
Elmoth）的森林中与迈雅（Maia）美丽安（Melian）相遇，双方相恋，他们走出森林时已经过去了很多岁月，在这期间大部分帖勒瑞族已经跟随埃爾威的兄弟奥爾维登上伊瑞西亚岛（Tol
Eressëa）前往阿门洲，埃爾威的追随者和其他不愿离去的帖勒瑞精灵则留在中土大陆。埃爾威走出森林后就成为这些留在中土的帖勒瑞精靈之王，

帖勒瑞族的語言是[帖勒瑞語](../Page/帖勒瑞語.md "wikilink")（Telerin），帖勒瑞語是源自[帖勒瑞通用語](../Page/帖勒瑞通用語.md "wikilink")（Common
Telerin）的分支语言。

帖勒瑞（Teleri）在精靈語中解作「后来者」，因第三个來到[阿門洲](../Page/阿門洲.md "wikilink")（Aman）。他們亦稱為尼爾雅（Nelyar），意思是「第三」，表示是最后到達阿門洲的精靈。

帖勒瑞族也特指维林诺帖勒瑞族。[辛達族](../Page/辛達族.md "wikilink")（Sindar）、[綠精靈](../Page/綠精靈.md "wikilink")（Laiquendi）、[南多族](../Page/南多族.md "wikilink")（Nandor）的祖先，大部分[亚维瑞族](../Page/亚维瑞族.md "wikilink")（Avari）精靈原本也是帖勒瑞族。

## 历史

根據傳說，帖勒瑞族是[埃奈爾](../Page/精靈甦醒.md "wikilink")（Enel）的後裔，埃奈爾是第三位在[庫維因恩](../Page/庫維因恩.md "wikilink")（Cuiviénen）甦醒的精靈。他和配偶及七十二位同伴也隨之甦醒，首位到達維林諾的帖勒瑞族精靈是[埃盧·庭葛](../Page/埃盧·庭葛.md "wikilink")（Elu
Thingol），他成為帖勒瑞族的領袖。帖勒瑞族自稱為**林達**（Lindar）或歌詠者，因為他們有優美的聲線。他們又稱為**尼爾雅**（Nelyar），意思是「第三」。

### 西迁

西迁的三支精灵队伍分别为[诺多](../Page/诺多.md "wikilink")，[凡雅和帖勒瑞](../Page/凡雅.md "wikilink")。[诺多和](../Page/诺多.md "wikilink")[凡雅行进最快](../Page/凡雅.md "wikilink")，也最早到达[维林诺](../Page/维林诺.md "wikilink")。而帖勒瑞精灵有部分在[迷霧山脈](../Page/迷霧山脈.md "wikilink")（Misty
Mountains）前結束旅程，他們就是[南多精靈](../Page/南多精靈.md "wikilink")（Nandor），后来的[西爾凡精靈](../Page/西爾凡精靈.md "wikilink")（Silvan）很多是南多精灵的后裔。其他的帖勒瑞穿過[伊利雅德](../Page/伊利雅德.md "wikilink")（Eriador），越過了[伊瑞德隆](../Page/伊瑞德隆.md "wikilink")（Ered
Luin），有一些留在東貝爾蘭尋找失蹤的[埃盧·庭葛](../Page/埃盧·庭葛.md "wikilink")，而其余的帖勒瑞則利用一個島嶼抵達維林諾。[邁雅](../Page/邁雅.md "wikilink")[歐希](../Page/歐希.md "wikilink")（Ossë）視他們如朋友。[貝爾蘭](../Page/貝爾蘭.md "wikilink")（Beleriand）的辛達族精靈自稱*Edhil*，意思是「精靈」，等同昆雅語的*Eldar*。

帖勒瑞族對大海甚為熱愛，歐希把运送帖勒瑞的島嶼固定在[艾爾達瑪灣](../Page/艾爾達瑪灣.md "wikilink")，歐希勸說帖勒瑞族留在岛上。帖勒瑞族長期居住在[伊瑞西亞島](../Page/伊瑞西亞島.md "wikilink")（Tol
Eressëa）。

### 天鹅港

帖勒瑞后来迁居阿门洲的海岸。诺多流亡时，在[澳闊隆迪](../Page/澳闊隆迪.md "wikilink")（Alqualondë）爆發上第一次同胞殘殺，因此，帖勒瑞族並沒有參與討伐[魔苟斯](../Page/魔苟斯.md "wikilink")（Morgoth）。帖勒瑞族最後饒恕了諾多族精靈殘殺的過失，兩族恢復和平。

## 文化

帖勒瑞擅长造船，对银的热爱甚于金子。他们的银匠工艺十分出色。

### 语言

帖勒瑞族的語言被認為是[昆雅語](../Page/昆雅語.md "wikilink")（Quenya）的一種方言，但帖勒瑞族卻認為這是獨立的語言，這似乎比較可信，昆雅語及帖勒瑞族語言的分別如同[義大利語及](../Page/義大利語.md "wikilink")[西班牙語的分別](../Page/西班牙語.md "wikilink")。

## 國王

1.  [埃奈爾](../Page/精靈始祖.md "wikilink")（Enel）
2.  [埃盧·庭葛](../Page/埃盧·庭葛.md "wikilink")（Elu Thingol）
3.  [迪歐](../Page/迪歐.md "wikilink")（Dior），埃盧·庭葛之孫
4.  [奧爾維](../Page/奧爾維.md "wikilink")（Olwë），埃盧·庭葛的兄弟

## 其他版本

在托爾金的早期著作，帖勒瑞族稱為*Solosimpi*，帖勒瑞族這名字則出現在《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》

## 外部連結

  - [Ardalambion article on
    Telerin](http://www.uib.no/People/hnohf/telerin.htm)

[de:Figuren in Tolkiens
Welt\#Teleri](../Page/de:Figuren_in_Tolkiens_Welt#Teleri.md "wikilink")
[pl:Quendi\#Teleri](../Page/pl:Quendi#Teleri.md "wikilink") [sv:Alver
(Tolkien)\#Teleri](../Page/sv:Alver_\(Tolkien\)#Teleri.md "wikilink")

[Category:托爾金作品](../Category/托爾金作品.md "wikilink")