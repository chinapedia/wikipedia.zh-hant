**乌尔里希·弗里德里希·威廉·约阿希姆·冯·里宾特洛甫**（[德語](../Page/德語.md "wikilink")：**Ulrich
Friedrich Wilhelm Joachim von
Ribbentrop**，）。[德國外交部长](../Page/纳粹德国.md "wikilink")。[德國二戰投降後被](../Page/德國二戰投降.md "wikilink")[英军拘捕](../Page/英军.md "wikilink")。1946年10月1日，[纽伦堡国际军事法庭判处](../Page/纽伦堡审判.md "wikilink")[絞刑](../Page/絞刑.md "wikilink")，15天后受刑而死。

## 生平

### 早年

里賓特洛甫出生於[德意志帝國](../Page/德意志帝國.md "wikilink")[萊茵州的](../Page/莱茵兰.md "wikilink")[韋塞爾](../Page/韋塞爾.md "wikilink")，父親理夏德·乌尔里希·弗里德里希·約阿希姆·馮·里賓特洛甫（Richard
Ulrich Friedrich Joachim Ribbentrop）是一名軍官，而母親則是约翰娜·索菲·赫特維希（Johanne
Sophie
Hertwig）\[1\]。里賓特洛甫小時候一直接受非正規的教育，直到他十多歲後才到[德國與](../Page/德國.md "wikilink")[瑞士的](../Page/瑞士.md "wikilink")[私立學校唸書](../Page/私立學校.md "wikilink")\[2\]。里賓特洛甫的父親於1908年從軍中撤職，原因是他曾發表德皇[威廉二世是](../Page/威廉二世_\(德国\).md "wikilink")[同性戀的詆毀言論](../Page/同性戀.md "wikilink")，里賓特洛甫家中也因此缺乏了經濟來源。里賓特洛甫藉由一口流利的[英語和](../Page/英語.md "wikilink")[法語](../Page/法語.md "wikilink")，先後在[法國](../Page/法國.md "wikilink")[格勒諾布爾和](../Page/格勒諾布爾.md "wikilink")[英國](../Page/英國.md "wikilink")[倫敦住過一段時間](../Page/倫敦.md "wikilink")，接著在1910年前往[加拿大](../Page/加拿大.md "wikilink")\[3\]，他先在[蒙特婁史丹利街](../Page/蒙特婁.md "wikilink")（Stanley
Street）的莫爾森銀行（Molson）工作，接著換到重建[魁北克橋的一家工程公司](../Page/魁北克橋.md "wikilink")，之後又換到曾修路連接[蒙克顿和](../Page/蒙克顿.md "wikilink")[温尼伯的加拿大](../Page/温尼伯.md "wikilink")[國家跨洲鐵路公司](../Page/國家跨洲鐵路公司.md "wikilink")（National
Transcontinental
Railway），還曾在[紐約與](../Page/紐約.md "wikilink")[波士頓當過記者](../Page/波士頓.md "wikilink")。之後里賓特洛甫雖然曾因[結核而返德休養](../Page/結核.md "wikilink")，但後又回到加拿大，於[渥太華經營一個小公司](../Page/渥太華.md "wikilink")，進口德國[酒與](../Page/酒.md "wikilink")[香檳來銷售](../Page/香檳.md "wikilink")\[4\]。1914年2月，里賓特洛甫參加舉辦於波士頓的埃利斯紀念盃比賽，與渥太華著名的[明托](../Page/明托滑冰俱乐部.md "wikilink")（Minto）溜冰隊競爭\[5\]\[6\]。因為[第一次世界大戰的爆發](../Page/第一次世界大戰.md "wikilink")，里賓特洛甫離開加拿大返回德國參戰\[7\]，留下了他罹患[肺結核的弟弟歐塔](../Page/肺結核.md "wikilink")·里賓特洛甫於加拿大，之後遭[英國拘捕](../Page/英國.md "wikilink")（1918年去世）。1914年8月15日經由[霍博肯開往](../Page/霍博肯_\(新泽西州\).md "wikilink")[鹿特丹的荷蘭](../Page/鹿特丹.md "wikilink")-美國船波茨坦號\[8\]回到歐洲，到了1914年秋天，他終於回到祖國，並加入第125輕騎兵團\[9\]。里賓特洛甫後晉升為中尉，並獲得[一級鐵十字勳章](../Page/鐵十字勳章.md "wikilink")。起初他服役於[東線](../Page/東線.md "wikilink")，後到[西線作戰](../Page/西線.md "wikilink")。1918年，里賓特洛甫以參謀身分派駐[奧斯曼帝國的](../Page/奧斯曼帝國.md "wikilink")[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")\[10\]。里賓特洛甫於一戰外駐[土耳其期間](../Page/土耳其.md "wikilink")，與另一位軍官[弗朗茨·馮·帕彭成為好友](../Page/弗朗茨·馮·帕彭.md "wikilink")\[11\]與認識了[汉斯·冯·塞克特將軍](../Page/汉斯·冯·塞克特.md "wikilink")。德國戰敗後，作為塞克特將軍的副官參加[巴黎和会](../Page/巴黎和会_\(1919年\).md "wikilink")，對於當時德國簽下[凡尔赛条约的屈辱場景](../Page/凡尔赛条约.md "wikilink")，印象非常深刻。

### 家庭

1919年，里賓特洛甫與安娜·伊麗莎白·亨克爾（Anna Elisabeth
Henkell）相識，朋友都稱她為安内利斯（Annelies）\[12\]。安娜是德國香檳製造廠老闆奧托·亨克爾（Otto
Henkell）的千金，里賓特洛甫於1920年7月5日與她在[威斯巴登結婚](../Page/威斯巴登.md "wikilink")，並開始在歐洲銷售他們酒廠的產品\[13\]。1921年至1940年期間，安娜共生下了5個孩子：

  - [魯道夫](../Page/魯道夫·馮·里賓特洛甫.md "wikilink")（Rudolf von
    Ribbentrop，生於1921年5月11日的[威斯巴登](../Page/威斯巴登.md "wikilink")），1960年與伊爾莎（Ilse-Marie
    Freiin von Münchhausen，1914-2010）結婚。
  - [贝蒂娜](../Page/贝蒂娜·馮·里賓特洛甫.md "wikilink")（Bettina von
    Ribbentrop，生於1922年7月20日的[柏林](../Page/柏林.md "wikilink")）
  - [烏蘇拉](../Page/烏蘇拉·馮·里賓特洛甫.md "wikilink")（Ursula von
    Ribbentrop，生於1932年12月29日的柏林）
  - [阿道夫](../Page/阿道夫·馮·里賓特洛甫.md "wikilink")（Adolf von
    Ribbentrop，生於1935年9月2日的柏林），育有：
      - 約阿希姆（Joachim von Ribbentrop，生於1963年7月5日）
      - 多米尼克（Dominik von Ribbentrop，生於1965年9月25日）
      - 魯道夫（Rudolf von
        Ribbentrop，生於1989年6月5日的[美因河畔法兰克福](../Page/美因河畔法兰克福.md "wikilink")）
      - 弗里德里希（Friedrich von Ribbentrop
        ，生於1990年6月28日的[美因河畔法兰克福](../Page/美因河畔法兰克福.md "wikilink")）
  - [巴托尔德·亨克爾](../Page/巴托尔德·亨克爾·馮·里賓特洛甫.md "wikilink")（Barthold Henkell
    von Ribbentrop，生於1940年12月19日的柏林），育有：
      - 塞巴斯蒂安（Sebastian von Ribbentrop，生於1971年2月3日）

安娜·伊麗莎白·亨克爾被認為是一個傲慢、控制欲強的一個女性，並常挨批為[麦克白夫人一般主宰自己丈夫的女人](../Page/麦克白.md "wikilink")\[14\]。里賓特洛甫之後說服丈夫受封為貴族的姨媽，在他姓中加上了貴族姓「馮」，然而里賓特洛甫從未受到[貴族接納](../Page/貴族.md "wikilink")，也沒有受到過承認。在[威瑪共和國的大部分時間裡](../Page/威瑪共和國.md "wikilink")，里賓特洛甫對政治不怎麼關心，也沒有[反猶情節](../Page/反犹太主义.md "wikilink")\[15\]。里賓特洛甫作為他岳父香檳公司的助手處理了許多業務，並和猶太銀行家經常有往來，他成立一個進口公司的資金就是向猶太人貸款\[16\]。

### 早期政治生涯

1928年，里賓特洛甫經人介紹給[希特勒](../Page/希特勒.md "wikilink")，希特勒記述他為「能以別人買法國香檳的價格買下德國香檳的男人」，以及為一個能與外國聯繫的商人\[17\]，之後里賓特洛甫在妻子的催促下於1932年5月1日加入了[納粹黨](../Page/納粹黨.md "wikilink")\[18\]。1933年1月，[弗朗茨·馮·帕彭與](../Page/弗朗茨·馮·帕彭.md "wikilink")[奧斯卡·馮·興登堡](../Page/奧斯卡·馮·興登堡.md "wikilink")（Oskar
von Hindenburg

，[興登堡總統之子](../Page/保罗·冯·兴登堡.md "wikilink")）一些朋友與希特勒協商，取代掉總理[库尔特·冯·施莱谢尔](../Page/库尔特·冯·施莱谢尔.md "wikilink")\[19\]。

這場會談的結果是使得希特勒於1933年1月30日透過任命成為[總理](../Page/德國總理.md "wikilink")，而提供自家作為秘密會談場所、納粹黨黨員和身為帕彭好友的里賓特洛甫就因此受到希特勒的愛戴。因為里賓特洛甫是納粹黨的一名新黨員，並不受到黨內老成員的歡迎，[戈培爾還在日記中將他寫為](../Page/戈培爾.md "wikilink")：「馮·里賓特洛甫的名字是買來的，錢是藉由婚姻娶來的、仕途則是騙來的\[20\]」為了彌補在黨中影響力不足的問題，里賓特洛甫開始將[納粹主義也逐漸融入他的生活中](../Page/納粹主義.md "wikilink")，高調的提倡[反猶主義](../Page/反猶主義.md "wikilink")\[21\]，成為一名狂熱的納粹份子。他之後部份憑藉著對於德國外國家的知識而成為希特勒的外交顧問，但大部分是因為對希特勒的諂媚與奉承之詞才有此地位\[22\]。專職的外交部人員會將國外實際情況告訴希特勒，而里賓特洛甫則總是告訴希特勒所想聽到的\[23\]。特別是里賓特洛甫會紀錄下希特勒對一些事物的想法與觀點，再提出與其相似的發言，藉此使希特勒認為自己是一名忠於國家社會主義的外交官\[24\]。里賓特洛甫也發現希特勒常以極端、偏激的方式來解決問題，他也因此常將自己的意見轉為此方向。

里賓特洛甫是希特勒的崇拜者之一，他的情緒很大一部份取於希特勒的好惡\[25\]。1933年，里賓特洛甫獲得黨衛隊上校的頭銜，一時間裡他還與[黨衛隊全國領袖](../Page/黨衛隊全國領袖.md "wikilink")[海因里希·希姆萊友好](../Page/海因里希·希姆萊.md "wikilink")，但最後彼此還是成為了敵人，主要是因為對於黨衛隊堅持自己的外交獨立性，而非倚靠里賓特洛甫\[26\]。

### 外交官生涯

1933年11月，里賓特洛甫開始進行他的外交生涯，他以一個非官方的外交官身份拜訪了倫敦，與[拉姆齊·麥克唐納首相和](../Page/拉姆齊·麥克唐納.md "wikilink")[約翰·西蒙外交及聯邦事務大臣會面](../Page/約翰·西蒙.md "wikilink")\[27\]，但這場會談沒有任何實質上的內容\[28\]。里賓特洛甫在取得外交辦公室的職位之前，大部分時間都花在攻擊現任外交部長[康斯坦丁·馮·紐賴特上](../Page/康斯坦丁·馮·紐賴特.md "wikilink")\[29\]，積極爭取該職位。起初紐賴特對里賓特洛甫這位對手保持輕視的態度，認為他這種連寫德文都會用字粗俗且[文法出錯的人](../Page/文法.md "wikilink")，更不用說[英文和](../Page/英文.md "wikilink")[法文了](../Page/法文.md "wikilink")，對里賓特洛甫的競爭與攻擊不怎麼擔心\[30\]。

當中國[抗日戰爭正式打響](../Page/抗日戰爭.md "wikilink")，與[日本友好的](../Page/大日本帝國.md "wikilink")[德國起初還在觀望](../Page/納粹德國.md "wikilink")，但在里賓特洛甫主導的外交部隨即下令駐華的德國僑民和武官撤出，進而導致[日軍變本加厲的侵華](../Page/日軍.md "wikilink")。隨後更將預定出售給中國的武器彈藥直接取消訂單，讓[蔣介石](../Page/蔣介石.md "wikilink")、[李宗仁等人驚愕](../Page/李宗仁.md "wikilink")，[德制師和中央軍因此無法得到充分的彈藥對抗日本](../Page/德制師.md "wikilink")。之後中國對抗日本的武器只能向[蘇聯和法國訂製](../Page/蘇聯.md "wikilink")，然而隨著納粹建立[維琪法國](../Page/維琪法國.md "wikilink")、與[蘇聯簽訂](../Page/蘇聯.md "wikilink")[互不侵犯條約並](../Page/互不侵犯條約.md "wikilink")[瓜分波蘭](../Page/瓜分波蘭.md "wikilink")，最後只剩下美國提供援助協同作戰，苦撐到[美軍正式加入](../Page/美軍.md "wikilink")[第二次世界大戰之後](../Page/第二次世界大戰.md "wikilink")，英國和加拿大才提供部分的武器援助[國軍](../Page/國民革命軍.md "wikilink")，直到日本[戰敗投降](../Page/日本投降.md "wikilink")。因此，[中國政府](../Page/中華民國國民政府.md "wikilink")[要員如](../Page/要員.md "wikilink")[蔣介石等人痛恨里賓特洛甫](../Page/蔣介石.md "wikilink")，在其[日記上對此的記載的文字甚是憤恨](../Page/蔣介石日記.md "wikilink")。

[Deadjoachimribbentrop.jpg](https://zh.wikipedia.org/wiki/File:Deadjoachimribbentrop.jpg "fig:Deadjoachimribbentrop.jpg")

## 参考文献

## 外部链接

  -
  -
{{-}}

[Category:最神聖報喜勳章爵士](../Category/最神聖報喜勳章爵士.md "wikilink")
[Category:鐵十字勳章授勛者](../Category/鐵十字勳章授勛者.md "wikilink")
[Category:德國外交部長](../Category/德國外交部長.md "wikilink")
[Category:德國駐英國大使](../Category/德國駐英國大使.md "wikilink")
[Category:德国外交官](../Category/德国外交官.md "wikilink")
[Category:納粹德國官員](../Category/納粹德國官員.md "wikilink")
[Category:第二次世界大戰德國戰犯](../Category/第二次世界大戰德國戰犯.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:德國第一次世界大戰人物](../Category/德國第一次世界大戰人物.md "wikilink")
[Category:被處決的德國人](../Category/被處決的德國人.md "wikilink")
[Category:德國天主教徒](../Category/德國天主教徒.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:被判反人类罪的德国人](../Category/被判反人类罪的德国人.md "wikilink")
[Category:戰前昭和時代外交](../Category/戰前昭和時代外交.md "wikilink")

1.  Bloch, *Ribbentrop*，第1頁至第2頁

2.  Bloch, *Ribbentrop*，第3頁至第4頁

3.  Bloch, *Ribbentrop*，第6頁

4.  Bloch, *Ribbentrop*，第7頁

5.
6.  Lawson, Robert, "Ribbentrop in Canada 1910 to 1914: A Note",
    *International History Review* 第29期，發行於：2007年12月4日，第821頁至第832頁

7.  Bloch, *Ribbentrop*，第8頁

8.
9.  *Current Biography 1941*，第707頁至第709頁

10. Bloch, *Ribbentrop*，第8頁至第9頁

11. Bloch, *Ribbentrop*，第9頁

12. Bloch, *Ribbentrop*，第12頁

13. Bloch, *Ribbentrop*，第12頁至第13頁

14.
15. Bloch, *Ribbentrop*，第16頁和第20至第21頁

16. *Current Biography 1941*，第708頁

17.
18. Bloch, *Ribbentrop*，第26頁

19. 1月22日，奧托·邁斯納和興登堡於里賓特洛甫家中會見希特勒、戈林和威廉·弗利克。經過短暫的閒聊後，希特勒請小興登堡在一間私室里交談，並在那裡講了超過1小時。
    經過這次會談後，小興登堡改變了原有的想法，轉而說服興登堡總統任用希特勒為總理。

20. Synder, Louis *Encyclopedia of the Third Reich*, New York:
    McGraw-Hill，1976年出版，第295頁

21. Bloch, *Ribbentrop*，第47頁

22. Craig 1953，第420頁至第421頁

23. Craig 1953，第420頁至第425頁

24. Craig 1953，第420頁

25. Bloch 1992，第51頁至第52頁

26. Bloch 1992，第83頁、第208頁至第209頁、第215頁、第230頁、第254頁和第365頁至第367頁

27. Bloch, *Ribbentrop*，第40頁至第44頁

28. Bloch, *Ribbentrop*，第44頁

29. Michalka 1993，第167頁

30. Craig 1953，第423頁至第424頁