是一款由[DMA Design](../Page/Rockstar_North.md "wikilink")（Rockstar
North的前身）制作的[電視與電腦](../Page/電子遊戲.md "wikilink")[動作遊戲](../Page/動作遊戲.md "wikilink")，由[Rockstar
Games於](../Page/Rockstar_Games.md "wikilink")2001年10月在[遊戲機](../Page/遊戲機.md "wikilink")[PlayStation
2上發行](../Page/PlayStation_2.md "wikilink")，2002年5月發售基於[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[電腦的版本](../Page/電腦.md "wikilink")，2012年发售支持[IOS及](../Page/IOS.md "wikilink")[Android操作系统的移动平台版](../Page/Android.md "wikilink")。2003年11月發售[Xbox版](../Page/Xbox.md "wikilink")。該作是[俠盜獵車手系列的一部分](../Page/俠盜獵車手系列.md "wikilink")，前作為《[俠盜獵車手II](../Page/俠盜獵車手II.md "wikilink")》（）。《俠盜獵車手III》亦是該系列第一次在PlayStation
2上發行。

該遊戲圍繞一個在一次銀行搶劫中被自己女友背叛的無名罪犯，在主角對抗她並獲得最終復仇機會之前，他需要靠自己以及其他黑幫中人提供的任務和工作來逐步晉身為城市中的犯罪頭子。像前作一樣，《俠盜獵車手III》實現了像[沙盒的](../Page/沙盒.md "wikilink")[遊戲性](../Page/遊戲性.md "wikilink")，玩家可在一個大城市中自由地實行各種活動，例如完成任務、探索城市、犯罪或完成次要任務。

遊戲的概念與遊戲性，以及該系列第一次結合[3D](../Page/3D.md "wikilink")[遊戲引擎](../Page/遊戲引擎.md "wikilink")[RenderWare](../Page/RenderWare.md "wikilink")，令《俠盜獵車手III》在發售後極受歡迎並成為2001年最暢銷遊戲。\[1\]《俠盜獵車手III》的成功是該系列續作大受歡迎的重要因素。

## 故事结构

### 游戏设定

遊戲發生地為[自由城](../Page/自由城_\(俠盜獵車手\).md "wikilink")（與[俠盜獵車手：自由城故事相同](../Page/俠盜獵車手：自由城故事.md "wikilink")），時間設為2001年，即《俠盜獵車手：自由城故事》的三年後，特意設定為接近遊戲發行的時間
。主角為[克勞迪](../Page/克勞迪.md "wikilink")（），不過在遊戲中又被稱為。克劳德是一位沉默的人，除了在淹死的时候发出一声叹息之外，从不说话。一些玩家認為主角不說話是因為射中了喉嚨和頸部區域因而令他失去說話能力，但也有其他玩家認為这是因为现实中没有声优为克劳德配音。因為在續作[俠盜獵車手：聖安地列斯](../Page/俠盜獵車手：聖安地列斯.md "wikilink")（該作故事發生在《俠盜獵車手III》之前）中，他與Catalina在一個賽車場的時候亦不說話。

GTA
III的情節借用了有名的電影、電影節目與流行精神文化的主題。最為影響遊戲故事的是《[教父](../Page/教父.md "wikilink")》、《[角頭風雲/Carlito's
Way](../Page/角頭風雲/Carlito's_Way.md "wikilink")》、《[黑道家族](../Page/黑道家族.md "wikilink")》與《[四海好傢伙](../Page/四海好傢伙.md "wikilink")》。

### 剧情

克劳德和他的女友凯特琳娜（《侠盗猎车手：圣安地列斯（以下简称SA）》中卡尔的前女友）在自由城策划了一场抢劫，当抢劫接近成功的时候女友却把他出卖了，凯特琳娜杀死另一个同伙后将子弹射向了克劳德。警察找到克劳德后将他送进医院治疗，但也顺便判了他有期徒刑十年。克劳德和另外两个犯人——汽车炸弹专家“8号球”和某东方老人一起被关进警方的囚车，囚车将他们从斯唐顿区（城区二）往波特兰区（城区一）转移。途径连接两区的克拉汉大桥时，一伙计划带走东方老人的劫狱团伙窜了出来并甩下一颗定时炸弹，将囚车团团围住，劫持了警察，带走东方老人之后扬长而去。克劳德和8号球因此获得自由，在他们刚刚喘了一口气时，桥上的定时炸弹突然爆炸，克拉汉大桥因此被炸断。

克劳德跟随8号球到波特兰岛红灯区的安全屋逃避警方追捕，小歇一段之后两人决定去8号球的朋友路易基（Luigi）的7号脱衣舞俱乐部找点活计做。在为路易基的卖命生涯中，克劳德结识了老练的汽车维修工人乔伊（Joey）、黄色杂志出版商兼惡魔幫首領埃尔布罗（El
Burro）、黑手党干部托尼（Toni，[侠盗猎车手：自由城故事的主角](../Page/侠盗猎车手：自由城故事.md "wikilink")）和里昂家族黑手党教父萨尔瓦托·里昂（曾在SA中和星达克家族合伙开了一家卡里古拉赌场）。克劳德为他们做各种各样的事情，以至于和波特兰的华人帮派三合会结下了不共戴天之仇。可是当克劳德为萨尔瓦托干了最大的一票之后，萨尔瓦托决定暗杀克劳德。克劳德因为萨尔瓦托的女人玛丽亚（SA中卡里古拉赌场服务生）的泄密逃过一劫，跟玛丽亚和日本帮派山口组头目的妹妹明日香一起驾船离开波特兰岛，前往斯唐顿，到达时连接两岛的克拉汉大桥刚好修缮完工，与此同时克劳德对玛丽拉情愫暗生。

在斯唐顿岛，克劳德出于报恩的原因，为明日香及其哥哥夏川健治做了一些活计，并回到波特兰岛杀掉了背信弃义的萨尔瓦托，导致自己的项上人头被黑手党定为第一悬赏。在新的故事中克劳德逐渐认识了牙买加帮的金·科特尼、腐败警察雷、军火商菲尔（在《[侠盗猎车手：罪恶城市](../Page/侠盗猎车手：罪恶城市.md "wikilink")（以下简称VC）》中是主角汤米的朋友）和房地产大亨唐纳德·爱（在VC中是房产商艾利夫·卡林顿的学徒）。在几次为科特尼卖命之后，科特尼委托他到一个停车场去做任务，克劳德到达那里后才发现自己中了科特尼和凯特琳娜的圈套，尽管陷入危险，不过克劳德仍然化险为夷，自此牙买加帮和克劳德结仇。之后他在唐纳德的一次委托中，坐上哥伦比亚帮的车暗杀了山口组头目夏川健治，成功令山口组与哥伦比亚帮开战。另一边，唐纳德要克劳德去帮他接货，在克劳德追击货物的中途再次遭遇凯特琳娜和哥伦比亚帮头目米格尔，凯特琳娜逃走，闻讯赶来的明日香囚禁了米格尔，试图从米格尔的口中套出关于她哥哥的死的事情。

后来通往海岸之谷的路重新开启。雷行迹暴露，克劳德护送他到机场，让雷登上了前往迈阿密（VC故事发生地）的班机。雷为了表示感谢，送给克劳德一辆防弹的悍马。另一边唐纳德在交给克劳德一个危险重重的任务之后也神秘失踪。凯特琳娜杀死了明日香和米格尔，绑架了玛丽亚，威胁他交出50万美金。主角去凯特琳娜的别墅，被她的手下搜身后，凯特琳娜对主角说了很多侮辱人的话，随后叫手下杀了他。尽管陷入危险，克劳德依旧逃出伏击圈，之后一路追到水电站，与凯特琳娜展开了一场惊险的决斗，最后凯特琳娜的直升机爆炸，凯特琳娜被炸死。玛丽亚被救后，一直表示自己的恐慌和对克劳德的感谢，突然画面一黑加上一下枪声，整个故事至此结束。

### 角色

故事並不是連貫的，在每次任務開始前會根據玩家的遊戲進度來使用場景表達角色與不同的人和首領的發展。大部分遇見以貪污、犯罪和一種開始威脅城市的虛構毒品"SPANK"為主。

憑著《俠盜獵車手III》與其繼作的成功，數個角色擴充了個人背景並在之後的俠盜獵車手系列中以主要和次要角色的身分出現，特別是黑手黨[Leone's的領導](../Page/Leone's.md "wikilink")
[Salvatore Leone](../Page/Salvatore_Leone.md "wikilink")、媒體大王[Donald
Love](../Page/Donald_Love.md "wikilink")、[Phil, the One-Armed
Bandit](../Page/Phil,_the_One-Armed_Bandit.md "wikilink")、[8-Ball與](../Page/8-Ball.md "wikilink")[Catalina](../Page/Catalina.md "wikilink")。

遊戲角色的[配音員很多為的名人](../Page/配音員.md "wikilink")。例如[Frank
Vincent](../Page/Frank_Vincent.md "wikilink"), [Michael
Madsen](../Page/Michael_Madsen.md "wikilink")、[Michael
Rapaport](../Page/Michael_Rapaport.md "wikilink")、[Joe
Pantoliano](../Page/Joe_Pantoliano.md "wikilink")、[Debi
Mazar](../Page/Debi_Mazar.md "wikilink")、[Kyle
MacLachlan與說唱歌手](../Page/Kyle_MacLachlan.md "wikilink")[Guru](../Page/Guru_\(rapper\).md "wikilink")。

## 遊戲玩法

### 介紹

[gta3-pc-walking.jpeg](https://zh.wikipedia.org/wiki/File:gta3-pc-walking.jpeg "fig:gta3-pc-walking.jpeg")
俠盜獵車手III的遊戲核心結合了第三人稱射擊與駕駛的元素，繼承前作《[俠盜獵車手](../Page/俠盜獵車手.md "wikilink")》與《俠盜獵車手II》的特點。移動時玩家可以選擇步行、跑步和跳躍的方式（但不能游泳），亦能使用武器與基本的[徒手格鬥技巧](../Page/徒手格鬥.md "wikilink")；也可以駕駛各種的交通工具。上一版俠盜獵車手的[多人連線遊戲模式被取消](../Page/多人連線遊戲.md "wikilink")，只支援單人遊戲。（不過玩家可以利用[Multi
Theft Auto在GTA](../Page/Multi_Theft_Auto.md "wikilink") III作線上對戰。）

俠盜獵車手III的遊戲結構沒太大改變。玩家的角色身處一個大型與開放的地圖，自由選擇完成主線故事的任務、次要任務（義務警員、醫務人員與計程車任務等）或自由遊覽。完成任務會獎勵金錢、少量酬勞或在自由城中解封一個新的街道與區域讓玩家探索。執法部門分為6個等級，包括了警察、[SWAT](../Page/特種武器和戰術部隊.md "wikilink")、[FBI探員與軍人](../Page/聯邦調查局.md "wikilink")，對於玩家在遊戲中的違法行為作出處理；躲避他們的方法和《俠盜獵車手》與《俠盜獵車手II》相同。《俠盜獵車手III》中的大部分交通工具外型的設計與現實中很類似。偷車、謀殺與偷竊等犯罪行為同樣可以在本作實施。

[HUD被重大修改](../Page/HUD.md "wikilink")。改為以玩家為中心的指南針，由一個單獨的小地圖來導向，同時顯示在該城的關鍵地點（如藏身點和聯絡地點）或目標的地圖。裝甲與生命值由數字顯示，並加入了一個24小時的時鐘。幫派尊重值不再獨立顯示；改為以玩家的進度來影響幫派分子的尊重值。當玩家為不同幫派完成任務后，如果經過敵對幫派地盤，幫派手下將主動攻擊玩家。

一旦玩家的角色死亡或被捕，會分別立刻送到當地的醫院或警局，並會損失手上所有武器與一定數量的金錢用作「醫療費用」或「行賂」之用，但不限定死亡或逮捕的次數。

### 任務、自由探索與故事

[Gta3casino.jpg](https://zh.wikipedia.org/wiki/File:Gta3casino.jpg "fig:Gta3casino.jpg")

主線任務由進入標記了有顏色的光圏的接觸點開始，由過場動畫描述故事發展。
在遊戲中任務的更新與訊息分別由主角隨身攜帶的[傳呼機顯示](../Page/傳呼機.md "wikilink")（一個自《俠盜獵車手》引入的功能）。《俠盜獵車手III》亦包括了一次的[教程來使玩家熟悉遊戲的操縱與特色](../Page/教程.md "wikilink")。

與上作俠盜獵車手不同，《俠盜獵車手III》中玩家不需有擁有特定金額的金錢來開啓新的地點。改為以完成任務與展開遊戲劇情來負責。另外玩家可以通過各種運輸路線回到所有已解開了的區域。不過當一個新的地區開啓後，一旦玩家完成了以前容易接近的地區中的目的後會因為敵對幫派的敵意而變得比較危險或較難接近。

像上一部俠盜獵車手一樣，《俠盜獵車手III》相當的非線性。當遊戲包括一個線性的任務（例如恐嚇當地的生意來收取“保護費”、清洗毒品交易的街道或暗殺對手幫派的首領），並完成它們來為他現時的幫派增加等級，大多數不需強制執行。實際上可以無視主任務來玩次要的任務，玩家亦可以選擇探索城市、偷車、在行人間走動與逃避（或對抗）警察。如果玩家取得一部[計程车](../Page/計程车.md "wikilink")，可以以載客的形式載特定的[非玩家角色並在城市的另一位置來獲取金錢回報](../Page/非玩家角色.md "wikilink")；取得一輛[救護車可允許玩家接載受傷的非玩家角色並送他們到醫院來取得金錢回報](../Page/救護車.md "wikilink")。救火與義務警察任務跟此類似。由於開放的遊戲設計玩家角色擁有行動上的自由，雖然從[Rockstar
North](../Page/Rockstar_North.md "wikilink")（當時為DMA Design）早期的[Nintendo
64遊戲](../Page/Nintendo_64.md "wikilink")《[Body
Harvest](../Page/Body_Harvest.md "wikilink")》中得到大量啓發，但在2001年中是一個大突破只在以後被該遊戲的繼作打破。

### 金錢系統

與前作相似，在《俠盜獵車手III》中金錢在一些部分非常重要，例如可以用來購買武器與裝甲、為玩家的汽車重新塗漆來逃避警察和允許玩家通過兩個任務(用來購買爆破裝備與作為贖金)。玩家角色主要通過任務來獲得金錢，不過次要任務也會給予玩家金錢獎勵。次要任務的獎金為分級制的，實際上是由小金額開始並當玩家繼續任務進程就會逐漸加大數目。根據次要任務在一個等級中取得一系列勝利後會有[combo獎金](../Page/combo.md "wikilink")。

《俠盜獵車手III》中像前作一樣由小罪行得到金錢回報的能力被降低，包括只撞擊汽車、損毀汽車與殺死行人。

### 武器

[GTAIII_PS2_Beta_Drive-by.jpg](https://zh.wikipedia.org/wiki/File:GTAIII_PS2_Beta_Drive-by.jpg "fig:GTAIII_PS2_Beta_Drive-by.jpg")

遊戲中的武器包括了[火器與](../Page/火器.md "wikilink")[炸藥和兩種形式的格鬥](../Page/炸藥.md "wikilink")（[徒手和使用](../Page/徒手.md "wikilink")[棒球棍](../Page/球棒.md "wikilink")）。武器的選擇本身與《[俠盜獵車手](../Page/俠盜獵車手.md "wikilink")》和《[俠盜獵車手II](../Page/俠盜獵車手II.md "wikilink")》相似，例如《俠盜獵車手》的[手槍](../Page/手槍.md "wikilink")、[機關槍](../Page/機關槍.md "wikilink")（在《俠盜獵車手III》中發展為包括了一枝[烏茲衝鋒槍](../Page/烏茲衝鋒槍.md "wikilink")、一枝[AK-47步槍與一枝](../Page/AK-47突擊步槍.md "wikilink")[M16步槍](../Page/M16突擊步槍.md "wikilink")），[火箭推進榴彈與](../Page/火箭推進榴彈.md "wikilink")[噴火器](../Page/火焰噴射器.md "wikilink")，《俠盜獵車手II》的[霰彈槍與投擲武器](../Page/霰彈槍.md "wikilink")[燃燒彈和](../Page/燃燒彈.md "wikilink")[手榴彈](../Page/手榴彈.md "wikilink")。《俠盜獵車手III》的持槍變為三維的環境並允許包括[狙擊步槍與使用M](../Page/狙擊步槍.md "wikilink")16步槍與飛彈發射器時的[第一身瞄準](../Page/第一人稱射擊遊戲.md "wikilink")。另外，現在可以使用烏茲衝鋒槍在駕駛時射擊。引進了使用[彈匣的武器在槍膛的彈藥耗盡後需要從新上彈](../Page/彈匣.md "wikilink")。武器可從當地的軍火商購買、免費從死去的幫派分子中、特定的任務人物與執法人員、或在城中的特定地點撿到。

遊戲中普遍的武器都有不同的特性：每一種提供猛烈破壞力的武器都會為個人或物件加大負擔，當使用某些武器可能會在攜帶或開火時減低行動能力，某些做成廣闊傷害的武器，特別是那些利用火或爆炸性的在使用時會令玩家處於傷害自己的危險當中。

家用機版本允許玩家按一個鍵來令大部分火器的槍口[自動瞄準並鎖定人類目標](../Page/自動瞄準.md "wikilink")，除了以第一人稱瞄準的狙擊槍、M16與飛彈發射器，當玩家按自動瞄準鍵時會變為由[類比搖桿控制](../Page/類比搖桿.md "wikilink")。由於Windows版允許玩家使用[滑鼠來瞄準目標](../Page/滑鼠.md "wikilink")，因此移除了自動瞄準系統。《[俠盜獵車手：罪惡城市](../Page/俠盜獵車手：罪惡城市.md "wikilink")》和《[俠盜獵車手：聖安地列斯](../Page/俠盜獵車手：聖安地列斯.md "wikilink")》的Windows版與家用機版本亦有此操控上的差別。

### 環境模擬

[gta3-pc-stealing.jpeg](https://zh.wikipedia.org/wiki/File:gta3-pc-stealing.jpeg "fig:gta3-pc-stealing.jpeg")

《俠盜獵車手III》擁有一個大型且富有生力的城市，并由三個不同人口聚居的街區組成。經過的汽車和行人不只是環境裝飾的一部分，也在遊戲中扮演真正重要的角色。玩家可以偷竊汽車，也可以痛打、搶劫、衝撞或射殺行人，并榨取金錢或武器。[救護車中的醫護人員會到達現場將已被謀殺致死的受害者救醒](../Page/救護車.md "wikilink"),消防車的消防員也會到達救火。遊戲也因為[NPC的意外行為而廣為人知](../Page/NPC.md "wikilink")。行人有時會打架，也會出現[NPC造成的交通事故](../Page/NPC.md "wikilink")；在城中甚至有其他的偷車賊。遊戲採用了24小時的時間制，有日夜交替，還擁有週期性的天氣變化，包括陰天、下雨和濃霧。

### 電台

遊戲其中一個特色是包含了各種不同風格的電台。電台播放的音樂是專為遊戲而寫的（而部份音樂是來是來自前兩作的俠盜獵車手），但也包括了版權的音樂，其中一些來自數張真實的唱片；這與前作只播放原創配樂不同。其中一個電台全為清談節目，而且大部分打電話的人實際上是故事任務的角色，經常證明了與任務時相同和已顯然易見的意見和怪癖。

每一個電台會在節目之間的休息時間播放不同的廣告。某些廣告會提到廣告客戶的網址，例如[Petsovernight.com](http://www.petsovernight.com)。那些網址全為真實存在，雖然那些看起來非常像真正的網上商店，但所有的購買或訂購的連結實際上會重定向到[Rockstargames.com](http://www.rockstargames.com)。電台的廣告也有提到廣告客戶的電話，但事實上那些都是由Rockstar登記的正式電話號碼；有些好奇的遊戲玩家嘗試撥打那些電話號碼但通話的另一端都是電話答錄機。

## 圖像

《俠盜獵車手III》的圖像完全與前作不同，最顯著為使用前方作為預設視角，與多數[第三人稱射擊和](../Page/第三人稱射擊.md "wikilink")[賽車遊戲相似](../Page/賽車.md "wikilink")，並引進了街道高度的圖像。遊戲亦包括了數個額外的視角，包括了一個用在《俠盜獵車手III》全部前作的[上至下遠視](../Page/上至下遠視.md "wikilink")，和一個電影視角。本遊戲是除了《[俠盜獵車手Advance](../Page/俠盜獵車手Advance.md "wikilink")》外主要俠盜獵車手系列中最後一隻包含上至下遠視視角。家用機版本的《俠盜獵車手III》在較低的[螢幕解析度下運行](../Page/螢幕解析度.md "wikilink")，而Windows版本容許最高1280×1024像素。

遊戲環境主要通過[細節等級](../Page/細節等級.md "wikilink")（LOD）來顯示。LOD是一種遊戲應用允許玩家附近區域顯示的物件擁有更高的[多邊計算](../Page/多邊.md "wikilink")（特別是大型物件如汽車、建築物和地形）或較少支撐（例如公物）；遠離玩家的區域由較低的多邊形顯示和需要不包括公物或汽車，假定玩家不能發現像這種常規的手法。同樣地LOD幫助《俠盜獵車手III》在確定遊戲者現時最適宜的效能後的配置顯示一個較大的環境和較遠的[繪圖距離](../Page/繪圖距離.md "wikilink")。當玩家在城市中一個島的一個地點移到另一個地點，遊戲會一致性地交換不同的細節模型。不過當玩家移動到另一個島，遊戲需要載入整個目的地島嶼的詳細模型檔案同時載入玩家離開的島嶼的低細節模型，實際上要求更多的處理時間；處理的時候遊戲會暫停並在可以從前開始前短暫顯示一個“Welcome
to...”的畫面。

就像其環境一樣，汽車和行人與前作上至下的[Sprite方式相比全為三維表現](../Page/Sprite.md "wikilink")。汽車和行人全擁有一個由個別的中央“核心”（汽車的車輪、引擎、底盤和車身，行人的[軀幹](../Page/軀幹.md "wikilink")）多邊形部分組成。汽車上損壞系統基於[損壞察覺類型](../Page/損壞察覺.md "wikilink")，由汽車的數個未損毀、已損毀或消失的汽車較小的部分(車門、前方四分之一嵌板和保險捍)來表示；另一方面每一輛車的核心即使嚴重損毀視覺上亦完全不會改變。《俠盜獵車手III》的行人在由六個單獨的多邊部件組成(四肢、頭部和軀幹)，可以分離行人的肢體或有效地令行人的頭部“爆開”（通過重型武器或爆炸品）。這個是Windows版上的顯著特點，但不包括在PlayStation
2版本中（但可通過一個密技“nasty limbs”來開啓。與其他密技不同，在成功輸入後螢幕上沒有任何確定而該密技仍是啓動了）。

由於遊戲使用24小時週期和包含了天氣的變換，遊戲的圖像引擎亦需要模擬日與夜的週期和一天各個時間的天氣效果。某些特殊的日子和天氣顏色會有沿著靜止灰色的細微改變，在所有元素上漸漸出現或消失。另外，陰天、下雨和霧會不同程度的減低繪畫距離，當天氣緩和時會該效果會減少。大部分的視覺效果例如火和煙，下雨和近距離的霧等天氣效果全由sprites描繪而成。天空和雲的顏色亦是相當於當時的時間和天氣。其他效果包括了下雨時偶然的閃電、下雨後的彩虹，太陽會在早上由城市的東北升起並移動到城市的西南邊直至黃昏。

本遊戲是系列中第一次使用[動態模糊](../Page/動態模糊.md "wikilink")（Windows版本混合了“Trails”），遊戲內提供最近的半透明[視像框架到現在的框架](../Page/視像框架.md "wikilink")。該功能亦有包括在Windows版中，但可選擇開啓或關閉。

## 開發

### 删減、修改與9/11事件影響

2001年《俠盜獵車手III》正式版遊戲發售前曾作過數次修改。那些改變非常明顯，因為數個在以前展示的宣傳材料沒有在正式版中出現。儘管刪減與修改經常在[遊戲開發中出現](../Page/遊戲開發.md "wikilink")，但《俠盜獵車手III》中做出的那些修改依然是一個有趣的議題，因為他們在2001年的[911襲擊發生後作出](../Page/911襲擊.md "wikilink")；這令一些玩家推測至少有一部分修改動機是因為911襲擊。儘管小部分揭露修改的原因是因為他們收到指引，引用Rockstar
Games（位於紐約[曼哈頓](../Page/曼哈頓.md "wikilink")）總裁[Sam
Houser在](../Page/Sam_Houser.md "wikilink")2001年9月11日提起《俠盜獵車手III》會經過一次重新檢查并確定遊戲將會推遲三週發行。\[2\]\[3\]\[4\]（最初由Houser提議大約的發行日期為10月2日）：\[5\]

[GTAIII_LCPD_copcar_change.jpg](https://zh.wikipedia.org/wiki/File:GTAIII_LCPD_copcar_change.jpg "fig:GTAIII_LCPD_copcar_change.jpg")，幾個遊戲網站中的一個，與來自《俠盜獵車手III》官方網站（**下**）的對比。GameSpot的版本繪圖類似2000年前的[紐約市警察局](../Page/紐約市警察局.md "wikilink")，而官方的修改版類似[洛杉磯市警察局](../Page/洛杉磯市警察局.md "wikilink")。\]\]

遊戲最明顯的改變為[警車的塗料樣式](../Page/警車.md "wikilink")；遊戲中LCPD（自由市警察局）新的塗料樣式模仿[洛杉磯市警察局熊貓一樣的黑白設計](../Page/洛杉磯市警察局.md "wikilink")，而舊的藍色加白色條紋塗料樣式(在預告和說明書地圖上看到)類似[紐約市警察局](../Page/紐約市警察局.md "wikilink")。遊戲官方網站上的發售前截圖繪圖的亦在襲擊後一週左右遭到修改。\[6\]像NYPD的塗料樣式最終在*俠盜獵車手：自由城故事中*中重新引入，但使用了黑色來代替了NYPD的藍色。

另一個在最終版中取消了的是Darkel，遊戲內的一個人物。數個電子遊戲出版物與網站提及Darkel是一個發誓要打倒市內經濟的頑童。其中一個任務為偷雪糕車來引起行人注意然後再引爆\[7\]（這個任務最終由[El
Burro給予](../Page/El_Burro.md "wikilink")，在最終版改為殺死一群幫派分子）。Darkel原先預定為給予暴力一類的任務\[8\]並已為他這部分錄了音。\[9\]Rockstar後來決定沿用《俠盜獵車手》和《俠盜獵車手II》的系統來給予暴力任務。儘管Darkel和他的任務不明確被移除，該角色仍留在說明書上的功勞列表上，而且角色的結構保留在遊戲的資料檔案中。\[10\]

[gta3_character_darkel_01.jpg](https://zh.wikipedia.org/wiki/File:gta3_character_darkel_01.jpg "fig:gta3_character_darkel_01.jpg")
儘管經常有傳言沒有飛機任務是跟隨9/11而修改或改變，就像沒有任務取消一樣。Dodo飛機，遊戲中唯一的飛機。儘管可以接連不斷地在自由城中飛行，但其操控相當困難，而一部全長機翼的版本可看到在城中飛行。

### 移植與重製

[Gta3pc.jpg](https://zh.wikipedia.org/wiki/File:Gta3pc.jpg "fig:Gta3pc.jpg")與愛爾蘭發行（在此為Windows版）的封面設計，突然改為一個意味深長的不同封面設計。\]\]

在最初[PlayStation
2上](../Page/PlayStation_2.md "wikilink")《俠盜獵車手III》發行之後，[個人電腦](../Page/個人電腦.md "wikilink")（PC）與[Xbox版跟隨著發行](../Page/Xbox.md "wikilink")。《俠盜獵車手III》特別值得注意因為是該系列首次在家用機上原創的作品；以往的《俠盜獵車手》在移植到家用機與手持裝置前會先推出Windows版本。根據《俠盜獵車手III》，PlayStation
2的俠盜獵車手遊戲一般沿着《俠盜獵車手III》的發行形式，在PlayStation
2發行七到八個月後移植Windows發行。\[11\]\[12\]\[13\]

遊戲的Windows版在2002年5月20日發行，因其執行問題而被批評，特別是根據執行效能更好的《[俠盜獵車手：罪惡城市](../Page/俠盜獵車手：罪惡城市.md "wikilink")》。這是因為技術上的問題；[遊戲引擎表現出在](../Page/遊戲引擎.md "wikilink")[繪圖距離的所有物件](../Page/繪圖距離.md "wikilink")，甚至是隱藏在建築物或樹後的物件，而《罪惡城市》只表現出實際上可看到的。\[14\]不過Windows版支援更高的[屏幕解析度和一個自訂的](../Page/屏幕解析度.md "wikilink")[MP3播放選項](../Page/MP3.md "wikilink")。

Xbox版最初應在2002年春季發行，但因[Take-Two
Interactive](../Page/Take-Two_Interactive.md "wikilink")（Rockstar
Games的總公司）和[索尼簽定協議而擱置](../Page/索尼電腦娛樂.md "wikilink")，令俠盜獵車手系列直到2004年11月仍為PlayStation
2獨佔。不過該協議在2003年修改，2003年10月包含了《俠盜獵車手III》與《俠盜獵車手：罪惡城市》兩者的《俠盜獵車手: Double
Pack》在PS2和Xbox上發行。該《Double
Pack》沒有在PC上發行。2005年10月《俠盜獵車手III》再次在Xbox上重新發行，這次與《俠盜獵車手：罪惡城市》和《俠盜獵車手：聖安地列斯》三部同捆，名為《俠盜獵車手：三部曲》。其中沒有新的改變，該套裝保留了《Double
Pack》對《俠盜獵車手III》的圖像改進。一個[任天堂GameCube移植版的](../Page/任天堂GameCube.md "wikilink")《俠盜獵車手III》曾計劃與Xbox版共同發行，\[15\]但由於未知的原因而取消。

兩個基於《俠盜獵車手III》的手提機版本已經發行。《[俠盜獵車手Advance](../Page/俠盜獵車手Advance.md "wikilink")》為首次最初計劃將《俠盜獵車手III》移植到[Game
Boy
Advance](../Page/Game_Boy_Advance.md "wikilink")，但現在在自由市引入新的劇情，大致上為《俠盜獵車手III》事件的一年前。**俠盜獵車手：自由城故事**在[PlayStation
Portable上發行](../Page/PlayStation_Portable.md "wikilink")，其地點與《俠盜獵車手III》的設定一樣，時間為1998年，即《俠盜獵車手III》事件發生的三年前。

而在2011年12月15日发布[iOS及](../Page/iOS.md "wikilink")[Android版本](../Page/Android.md "wikilink")，該版本為10周年版，iOS版本適用於iPhone、iPad及iPod
touch。

## 發行與評價

[gta3-pc-taxi.jpeg](https://zh.wikipedia.org/wiki/File:gta3-pc-taxi.jpeg "fig:gta3-pc-taxi.jpeg")

在它發行時，《俠盜獵車手III》出乎意料在最初49.95美元的價格中形成一種潮流並成為2001年[美國銷量第一的電子遊戲](../Page/美國.md "wikilink")。後期成為索尼的“Greatest
Hits”計劃一部分減價至$19.95，繼續熱銷並在2002年成為銷量第二最好的電子遊戲，緊接着其繼作《[俠盜獵車手：罪惡城市](../Page/俠盜獵車手：罪惡城市.md "wikilink")》。在大部分遊戲經歷強烈的滯銷與降價下，這是一個產業上驚人的成績，像玩家有只購買“下一個新事物”的強烈傾向。《俠盜獵車手III》繼續享受Xbox上《Double
Pack》部分的強勁銷售額，即使《Double Pack》在兩年後的2003年10月24日上架。*Double
Pack*'在Xbox的成功因為數個因素，高度評價（不只是俠盜獵車手系列而是Xbox版的改進）與受爭議的遊戲內容、圖像改進和一次包含兩個遊戲。

遊戲因革命性被數個遊戲評論網站與出版物評分，並獲得數個獎項，例如[Gamespot](../Page/Gamespot.md "wikilink")、[GameSpy與Cheat](../Page/GameSpy.md "wikilink")
Code
Central的年度遊戲和[IGN的](../Page/IGN.md "wikilink")2001年最佳動作遊戲，平均從95%評論網站與出版物獲得獎項。\[16\]\[17\]

它擁有一個特點使它能在[電子遊戲歷史上留一席位](../Page/電子遊戲歷史.md "wikilink")：暴力與性內容。這個特色使[GamePro在俠盜獵車手III發行的](../Page/GamePro.md "wikilink")6年後，將它排在歷來最重要電子遊戲排行榜中的第1位\[18\]。

## 爭議

[gta3-pc-police.jpeg](https://zh.wikipedia.org/wiki/File:gta3-pc-police.jpeg "fig:gta3-pc-police.jpeg")

### 衍生社會問題

《俠盜獵車手III》因其暴力與性內容受到[爭議](../Page/爭議.md "wikilink")，並在發行前做成了[道德恐慌](../Page/道德恐慌.md "wikilink")。舉例電視遊戲的暴力，大多數電視新聞頻道經常展示《俠盜獵車手III》主角開槍射殺行人和令警車爆炸的遊戲階斷。玩家因不同的非法與不道德的行為而獲得金錢的回報：其中一個主張，經常被新聞界引用，就是遊戲裏玩家可以偷竊一輛汽車，接載一個[妓女並與其](../Page/妓女.md "wikilink")（暗示的）性交，然後殺死她並偷取她的金錢[1](https://web.archive.org/web/20060812101635/http://www.brettdouville.com/mt-archives/2005/12/comparative_med.html)。雖然這個行為被允許，（「性交」可以回復玩家的生命值和增加玩家的生命值最多可以增加到125）但卻不是必要的。另外，遊戲內的罪行會招致警察的通緝，而且有可能在遊玩的時候不犯上述罪行。不同的評論家假設如果小孩玩了該遊戲，他們可能養成以[反社會的態度來對待他人](../Page/反社會.md "wikilink")。在[美國數個因偷車而被捕的未成年人聲稱他們的動機來自玩該遊戲](../Page/美國.md "wikilink")。

### 遊戲遭審查

由於[ESRB將](../Page/Entertainment_Software_Rating_Board.md "wikilink")《俠盜獵車手III》評為“M”級，[沃爾瑪零售連鎖店宣佈會檢查那些看起來類似於](../Page/沃爾瑪.md "wikilink")17歲以下購買者的駕照以及醫療保險卡。

最初在[澳洲發行之後](../Page/澳洲.md "wikilink")，該遊戲在一個時期被禁止而一個經删改的版本發行來代替它的位置。\[19\]採取這行動的關鍵原因主要是因為Rockstar沒有提交《俠盜獵車手III》到[影片與文學分類辦公室](../Page/影片與文學分類辦公室.md "wikilink")（OFLC），主要除以之外，根據在澳洲內他們的內容評估電子遊戲。缺乏適當的R18+等級（最高的為MA15+），該遊戲被“拒絶分類”並因認為不適合大於15歲而少於18歲的玩家而被禁止銷售。澳洲仍沒有為遊戲設置R等級，就像對待電影一樣。

### 內容含有未删改版本

有趣的是其繼作《[俠盜獵車手：罪惡城市](../Page/俠盜獵車手：罪惡城市.md "wikilink")》經過OFLC的删改，而下一個繼作《[俠盜獵車手：聖安地列斯](../Page/俠盜獵車手：聖安地列斯.md "wikilink")》則沒有，不顧其提供更多「成熟」的內容（雖然後來《聖安地列斯》曾在“[热咖啡](../Page/热咖啡.md "wikilink")”爭論中被評為拒絶分級等級），導致很多推繼遊戲被禁止，唯一被禁只的原因是OFLC因Rockstar沒有提交該遊戲給他們檢查而生氣。除此之外，删改版移除接載妓女和流血的能力；不過後來被發現可通過將電腦時區改為美國來玩未删改的版本。

### 被法律訴訟

在2003年10月20日，Aaron Hamel與Kimberly Bede，兩個被青年William與Josh
Buckner（他們向調查人員聲稱行動靈感來自《俠盜獵車手III》）射殺的年輕人的家人，提出二億四千六百萬美金控訟發行人[Rockstar
Games](../Page/Rockstar_Games.md "wikilink")、[Take-Two
Interactive軟件](../Page/Take-Two_Interactive.md "wikilink")、零售商沃濔瑪和PlayStation
2的製造者[索尼電腦娛樂美國](../Page/索尼電腦娛樂.md "wikilink")。\[20\]\[21\]Rockstar與其總公司Take-Two駁回該訴訟，正式於2003年10月29日在[美國區域法院聲明](../Page/美國區域法院.md "wikilink")“Buckners的計劃與概念和‘聲稱造成的心理效果’受到[第一修正案的言論自由條款所保護](../Page/第一修正案.md "wikilink")。”受害者的律師[Jack
Thompson拒絕嘗試將控訴移到州法庭並以](../Page/Jack_Thompson.md "wikilink")[田納西州的消費者保護法提出訴訟](../Page/田納西州.md "wikilink")。\[22\]這訴訟2004年結束時仍未解決。由於相似的原因，一個類似訴訟也對《罪惡城市》提出。

## 參考文獻

## 外部連結

  -
  - [Grand Theft
    Wiki《俠盜獵車手III》页面](http://www.GrandTheftWiki.com/Grand_Theft_Auto_III)
    on Grand Theft Wiki

  - [Playstation.com《俠盜獵車手III》页面](https://web.archive.org/web/20080616001835/http://www.us.playstation.com/PS2/Games/Grand_Theft_Auto_III)

  -
{{-}}

[3](../Category/俠盜獵車手系列電子遊戲.md "wikilink")
[Category:2001年电子游戏](../Category/2001年电子游戏.md "wikilink")
[Category:Android遊戲](../Category/Android遊戲.md "wikilink")
[Category:Games for
Windows認證遊戲](../Category/Games_for_Windows認證遊戲.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink") [Category:Xbox
360遊戲](../Category/Xbox_360遊戲.md "wikilink")
[Category:Xbox遊戲](../Category/Xbox遊戲.md "wikilink")
[Category:成人電子遊戲](../Category/成人電子遊戲.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.  [Bill Fiore](http://www.imdb.com/title/tt0100263/) at [The Internet
    Movie Database](../Page/The_Internet_Movie_Database.md "wikilink").
    URL accessed on [May 13](../Page/May_13.md "wikilink"), 2006.
10.
11. [《俠盜獵車手III》's release
    information](http://www.mobygames.com/game/grand-theft-auto-iii/release-info)
    at MobyGames. URL accessed on [June
    11](../Page/June_11.md "wikilink"), 2006.
12. [*GTA: Vice City*'s release
    information](http://www.mobygames.com/game/grand-theft-auto-vice-city/release-info)
    at MobyGames. URL accessed on [June
    11](../Page/June_11.md "wikilink"), 2006.
13. [*GTA: San Andreas*'s release
    information](http://www.mobygames.com/game/grand-theft-auto-san-andreas/release-info)
    at MobyGames. URL accessed on [June
    11](../Page/June_11.md "wikilink"), 2006.
14.
15.
16.
17.
18.
19.
20.
21.
22.