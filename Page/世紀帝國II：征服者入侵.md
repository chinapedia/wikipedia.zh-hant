是[微软](../Page/微软.md "wikilink")[即時戰略遊戲](../Page/即時戰略遊戲.md "wikilink")《[世紀帝國II：帝王世紀](../Page/世紀帝國II：帝王世紀.md "wikilink")》的官方擴充[资料片](../Page/资料片.md "wikilink")，曾榮獲2000年E3最佳戰略性遊戲資料片。

征服者入侵除了《[世紀帝國II：帝王世紀](../Page/世紀帝國II：帝王世紀.md "wikilink")》的原有文明，再行引入5種新文明及相關戰役，其中2种代表[新大陆](../Page/新大陆.md "wikilink")：[阿兹特克和](../Page/阿兹特克.md "wikilink")[玛雅](../Page/玛雅文明.md "wikilink")，另3种分别為[匈奴](../Page/匈奴.md "wikilink")、[西班牙和](../Page/西班牙.md "wikilink")[韓國](../Page/韓國.md "wikilink")；另有一战役系列是互不相干的独立战役。此外標準遊戲模式即新增世界奇觀競賽、防禦世界奇觀和至尊王3種，並新增19張地圖（9張隨機和10張仿自真實世界據點）、11種軍事單位和26種科技。該資料片推出後，曾發布官方唯一和最終版本的1.0c[修正檔](../Page/修正檔.md "wikilink")。

超過十年後，《帝王世紀》第二個官方擴充資料片──《[失落的帝國](../Page/世紀帝國II：失落的帝國.md "wikilink")》在[SteamHD版推出](../Page/Steam.md "wikilink")，於2015年秋季推出第三個官方擴充資料片──《[非洲王國](../Page/世紀帝國II：非洲王國.md "wikilink")》，於2016年年底推出第四個官方擴充資料片──《[王者崛起](../Page/世紀帝國II：拉者崛起.md "wikilink")》。

## 資料片特色

《世紀帝國II：征服者入侵》針對主程式《世紀帝國II：帝王世紀》擴充，遊戲進行條件趨向多元化，但在型態上並沒有相當的更動。

### 文明

資料片於保留原13種文明之下，再新增5種文明，而其中2种代表[新大陆](../Page/新大陆.md "wikilink")：[阿茲特克和](../Page/阿茲特克.md "wikilink")[馬雅](../Page/馬雅文明.md "wikilink")，另有[西班牙](../Page/西班牙帝國.md "wikilink")、[韓國和](../Page/李氏朝鮮.md "wikilink")[匈奴](../Page/匈人.md "wikilink")，依舊擁有彼此的優點和缺點，並有獨道之處，及設計不同語言的角色配音。

| 建筑风格 | 文明                                 | 文明定位    | 特色兵种                                                                     | 特色科技                                  | 奇观原型                                      |
| ---- | ---------------------------------- | ------- | ------------------------------------------------------------------------ | ------------------------------------- | ----------------------------------------- |
| 西欧   | [西班牙](../Page/西班牙帝国.md "wikilink") | 火器和僧侣民族 | [傳教士](../Page/傳教士.md "wikilink")、-{[西班牙征服者](../Page/征服者.md "wikilink")}- | 霸权                                    | [黄金塔](../Page/黄金塔.md "wikilink")          |
| 中美洲  | [阿兹特克](../Page/阿兹特克.md "wikilink") | 步兵和僧侣民族 | [豹勇士](../Page/豹勇士.md "wikilink")                                         | [榮冠戰爭](../Page/榮冠戰爭.md "wikilink")    | [特诺奇提特兰金字塔](../Page/特诺奇提特兰.md "wikilink") |
| 中美洲  | [马雅](../Page/马雅文明.md "wikilink")   | 弓兵民族    | [羽毛箭射手](../Page/羽毛箭射手.md "wikilink")                                     | [黄金国](../Page/黄金国.md "wikilink")      | [蒂卡尔金字塔](../Page/蒂卡尔.md "wikilink")       |
| 中欧   | [匈奴](../Page/匈人.md "wikilink")     | 骑兵民族    | [鞑靼骑兵](../Page/鞑靼骑兵.md "wikilink")                                       | [无神论](../Page/无神论.md "wikilink")      | [君士坦丁凯旋门](../Page/君士坦丁凯旋门.md "wikilink")  |
| 東亞   | [韩国](../Page/李氏朝鲜.md "wikilink")   | 箭塔和海军民族 | [馬戰車](../Page/馬戰車.md "wikilink")、[龜甲船](../Page/龜甲船.md "wikilink")        | [投擲器](../Page/神機箭.md "wikilink")（神機箭） | [皇龙寺](../Page/皇龙寺.md "wikilink")          |

### 科技

普通科技部分加入七種：血統、拇指環、安息人戰術、商隊、草藥、異端邪說和神權政治，供玩家強化自己文明的經濟或軍事能力（有無上述科技仍因文明而異）。
且所有文明都能設定農田自動耕種；並各有獨特科技，但皆至帝王時代才得以在城堡研發（哥德的-{zh-hans:無政府;
zh-hant:無政府狀態}-除外）。

### 單位

新增了長槍兵的升级版[戟兵和斥候騎兵的升级版](../Page/斧槍.md "wikilink")[匈牙利輕騎兵](../Page/驃騎兵.md "wikilink")；對抗建築物也有全新選擇：一次性使用並攻擊力高的炸藥桶。源自美洲的阿茲特克和馬雅，其軍營可以訓練[鷹勇士](../Page/鵰戰士.md "wikilink")；因無產馬，亦取代斥候騎兵的地位。

村民修建磨坊、伐木場或採礦營地完畢後，村民將會從最近的相關資源，自動進行採集工作（城鎮中心則皆可）。

且玩家可以經過語音，傳送同盟電腦玩家攻擊或提供資源貢品等指令。軍事方面也能替戰船編排隊形和衝撞車駐軍。

### 戰役

除了原有的帝王世紀戰役：[威廉華勒斯](../Page/威廉·華勒斯.md "wikilink")（教學戰役）、[聖女貞德](../Page/聖女貞德.md "wikilink")、[沙拉丁](../Page/沙拉丁.md "wikilink")、[成吉思汗和](../Page/成吉思汗.md "wikilink")[巴巴羅薩](../Page/腓特烈一世_\(神聖羅馬帝國\).md "wikilink")，資料片也有新的征服者戰役:[阿提拉](../Page/阿提拉.md "wikilink")(匈奴)，[席德](../Page/席德.md "wikilink")(西班牙/薩拉森)，蒙特蘇馬(阿茲特克)，征服者的戰役(8個不同故事的綜合章節，见下表)，但事實上部分戰役與史實有明顯差異。

在[蒙特蘇馬战役中](../Page/蒙特蘇馬.md "wikilink")，[西班牙人看似以排山倒海之勢而來](../Page/西班牙人.md "wikilink")，但[阿茲特克人在該戰役的結局中徹底地擊敗了西班牙人](../Page/阿茲特克人.md "wikilink")。然而歷史上[科爾特茲隨行的部隊只有不到](../Page/荷南·科爾蒂斯.md "wikilink")1000人，而且[阿茲特克人最後戰敗了](../Page/阿茲特克人.md "wikilink")。而且事實上主角應為蒙特蘇馬二世之侄子－[瓜特穆斯](../Page/瓜特穆斯.md "wikilink")。

| 征服者的战役                                   | 年代    | 使用文明 |
| ---------------------------------------- | ----- | ---- |
| [圖爾戰役](../Page/圖爾戰役.md "wikilink")       | 732年  | 法蘭西  |
| [大地的传奇](../Page/红胡子埃里克.md "wikilink")    | 1000年 | 維京   |
| [黑斯廷斯战役](../Page/黑斯廷斯战役.md "wikilink")   | 1066年 | 法蘭西  |
| [曼齐刻尔特战役](../Page/曼齐刻尔特战役.md "wikilink") | 1071年 | 土耳其  |
| [阿金库尔战役](../Page/阿金库尔战役.md "wikilink")   | 1415年 | 不列顛  |
| [勒班陀戰役](../Page/勒班陀戰役.md "wikilink")     | 1571年 | 西班牙  |
| [山崎之戰](../Page/山崎之戰.md "wikilink")       | 1582年 | 日本   |
| [露梁海戰](../Page/露梁海戰.md "wikilink")       | 1598年 | 韓國   |

## 音樂

<center>

<table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><ol>
<li>Pork Parts (3:06)</li>
<li>Pudding Pie (estimated 3:11)</li>
<li>Tide Me Over Warm 'em Ups (2:51)</li>
<li>Voodoodoodoo (2:55)</li>
<li>The Bovinian Derivative (3:10)</li>
<li>Case in Point: Paste (2:58)</li>
<li>Mountain Lie On / Seamus and Chamois (3:24)</li>
<li>Subotai Defeats the Knights Templar (estimated 3:06)</li>
<li>Roi-r! / Basura! Basura! (2:42)</li>
<li>Neep Ninny-Bod (3:16)</li>
</ol></td>
</tr>
</tbody>
</table>

</center>

## 註釋

## 外部連結

  - [世紀帝國II：征服者入侵官方網站](https://web.archive.org/web/20151102053826/http://www.ageofempires.com/news/games/aoeii/)

## 參見

  - [即時戰略](../Page/即時戰略.md "wikilink")
  - [戰略遊戲](../Page/戰略遊戲.md "wikilink")

{{-}}

[sv:Age of Empires II: The Age of Kings\#Age of Empires II: The
Conquerors](../Page/sv:Age_of_Empires_II:_The_Age_of_Kings#Age_of_Empires_II:_The_Conquerors.md "wikilink")

[21](../Category/世紀帝國系列.md "wikilink")
[Category:2000年电子游戏](../Category/2000年电子游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:製作中止的Dreamcast遊戲](../Category/製作中止的Dreamcast遊戲.md "wikilink")
[Category:制作中止的PlayStation
2游戏](../Category/制作中止的PlayStation_2游戏.md "wikilink")
[Category:资料片](../Category/资料片.md "wikilink")