**短腳龍屬**（[學名](../Page/學名.md "wikilink")：*Brachypodosaurus*）意為「短腳的蜥蜴」，是[甲龍下目](../Page/甲龍下目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，化石被發現於[印度的](../Page/印度.md "wikilink")[拉米塔組](../Page/拉米塔組.md "wikilink")，地質年代屬於[上白堊紀的](../Page/白堊紀.md "wikilink")[麥斯特裡希特階](../Page/麥斯特裡希特階.md "wikilink")。唯一被發現的[化石是一塊](../Page/化石.md "wikilink")[肱骨](../Page/肱骨.md "wikilink")，在1934年由Chakravati命名為**沉重短腳龍**（*B.
gravis*），最初被歸類於[劍龍下目](../Page/劍龍下目.md "wikilink")\[1\]。

短腳龍可能是屬於[甲龍下目或](../Page/甲龍下目.md "wikilink")[劍龍下目](../Page/劍龍下目.md "wikilink")。由於只有很少化石被發現，短腳龍被認為是[疑名](../Page/疑名.md "wikilink")\[2\]。在2003年，有科學家質疑短腳龍與劍龍類之間的關係\[3\]。另一個在印度發現的上白堊紀恐龍，原先亦被認為是劍龍，稱為[南印度龍](../Page/南印度龍.md "wikilink")，其實也是疑名。

## 參考資料

<span style="font-size:smaller;">

<references/>

</span>

[Category:印度與馬達加斯加恐龍](../Category/印度與馬達加斯加恐龍.md "wikilink")
[Category:甲龍下目](../Category/甲龍下目.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  Chakravarti, D. K., 1934, "On a stegosaurian humerus from the Lameta
    beds of Jubbulpore", Q.J. MINERAL. METALLURG. SOC. INDIA, 30; 75-79
2.  Maryanska T., 1977. "Ankylosauridae (Dinosauria) from Mongolia",
    Palaeontologia Polonica 37:85-151
3.  Wilson, J. A., P. C. Sereno, S. Srivastava, D. K. Bhatt, A. Khosla,
    and A. Sahni. 2003. "A new abelisaurid (Dinosauria, Theropoda) from
    the Lameta Formation (Cretaceous, Maastrichtian) of India",
    Contributions from the Museum of Paleontology of the University of
    Michigan, 31:1–42