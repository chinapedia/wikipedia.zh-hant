[_TOKYOPOP_Logo.png](https://zh.wikipedia.org/wiki/File:_TOKYOPOP_Logo.png "fig:_TOKYOPOP_Logo.png")
**TOKYOPOP**是一家成立於[東京的](../Page/東京.md "wikilink")[漫畫發行公司](../Page/漫畫.md "wikilink")，其主要業務是經取得漫畫出版權後在[日本以外地區翻譯](../Page/日本.md "wikilink")、出版發行[日本漫畫的](../Page/日本漫畫.md "wikilink")[英文及](../Page/英文.md "wikilink")[德文版](../Page/德文.md "wikilink")。TOKYOPOP最大的海外分部位於[美國的](../Page/美國.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")，此外在[英國和](../Page/英國.md "wikilink")[德國也設有分部](../Page/德國.md "wikilink")。TOKYOPOP漫畫發行地區包括[北美](../Page/北美.md "wikilink")（[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")）、[歐洲](../Page/歐洲.md "wikilink")（[英國](../Page/英國.md "wikilink")、[德國和](../Page/德國.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")）和[大洋洲](../Page/大洋洲.md "wikilink")（[澳大利亞](../Page/澳大利亞.md "wikilink")）。除了日本漫畫外，TOKYOPOP也發行其他地區的漫畫，如[英語原創日本風格漫畫](../Page/英語原創日本風格漫畫.md "wikilink")（）、[韩國漫畫等](../Page/韩國漫畫.md "wikilink")。

目前，TOKYOPOP還翻譯發行[輕小說和](../Page/輕小說.md "wikilink")[film
comic等](../Page/film_comic.md "wikilink")。

## 出版的部份書目

  - [BECK](../Page/BECK.md "wikilink")
  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")
  - [聖石小子](../Page/聖石小子.md "wikilink")
  - [女孩萬歲](../Page/女孩萬歲.md "wikilink")
  - [純情房東俏房客](../Page/純情房東俏房客.md "wikilink")
  - [鬼眼狂刀](../Page/鬼眼狂刀.md "wikilink")
  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")
  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")
  - [百變小櫻](../Page/百變小櫻.md "wikilink")
  - [他與她的事情](../Page/他與她的事情.md "wikilink")
  - [橘子醬男孩](../Page/橘子醬男孩.md "wikilink")
  - [美少女戰士](../Page/美少女戰士.md "wikilink")
  - [東京喵喵](../Page/東京喵喵.md "wikilink")
  - [青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")
  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
  - [Chobits](../Page/Chobits.md "wikilink")
  - [大逃殺](../Page/大逃殺.md "wikilink")
  - [惑星奇航](../Page/惑星奇航.md "wikilink")
  - [Undertown](../Page/Undertown.md "wikilink")
  - [問題提起作品集](../Page/問題提起作品集.md "wikilink")

## 參考資料

  - [Tokyopop discusses the consequence of Borders' bankruptcy on
    publishers](http://asiapacificarts.usc.edu/w_apa/showarticle.aspx?articleID=16492&AspxAutoDetectCookieSupport=1)
  - [Tokyopop Splits into Two
    Companies](http://www.icv2.com/articles/news/12677.html)
  - [Manga Publisher Tokyopop Lays Off Eight More
    Staffers](http://www.animenewsnetwork.com/news/2008-12-12/manga-publisher-tokyopop-lays-off-eight-more-staffers)
  - [Tokyopop to Close North American Publishing Division
    (Update 3)](http://www.animenewsnetwork.com/news/2011-04-15/tokyopop-to-close-north-american-publishing-division)
  - [Tokyopop Going Public
    ?](http://www.animenewsnetwork.com/news/2005-01-13/tokyopop-going-public)

## 外部連結

  - [TOKYOPOP日本](http://www.tokyopop.co.jp/)
  - [TOKYOPOP 美國](http://www.tokyopop.com/)
  - [TOKYOPOP 英國](http://www.tokyopop.co.uk/)
  - [TOKYOPOP 德國](http://www.tokyopop.de/)

[Tokyopop](../Category/日本出版社.md "wikilink")
[Category:漫畫出版社](../Category/漫畫出版社.md "wikilink")