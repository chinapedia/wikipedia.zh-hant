**陸穀孫**（）\[1\]，[浙江](../Page/浙江.md "wikilink")[余姚人](../Page/余姚.md "wikilink")，[翻译家](../Page/翻译家.md "wikilink")，[英语](../Page/英语.md "wikilink")[文学研究专家](../Page/文学.md "wikilink")，英语教学权威，尤精于[莎士比亚文学研究和英汉](../Page/莎士比亚.md "wikilink")[词典的编纂](../Page/词典.md "wikilink")。[复旦大学外语学院教授](../Page/复旦大学.md "wikilink")、博士生导师，曾经担任过[复旦大学外文学院院长](../Page/复旦大学.md "wikilink")。

## 简历

1965年，于[复旦大学外文系](../Page/复旦大学.md "wikilink")[研究生毕业](../Page/研究生.md "wikilink")。

1970年，参加《[新英汉词典](../Page/新英汉词典.md "wikilink")》编写，是词典的主要设计者和定稿人。

1976年，参加《[英汉大词典](../Page/英汉大词典.md "wikilink")》（著名的国家哲学社会科学项目）的编写和筹备工作。

1978年，由助教破格提升为副教授。

1985年，提升为教授。

1986年11月，被任命为《英汉大词典》主编。

1984年-1985年，以[高级富布赖特学者身份赴](../Page/高级富布赖特学者.md "wikilink")[美国访问](../Page/美国.md "wikilink")，从事英美语方文学之翻译、教学、研究工作。

1990年成为博士生导师。

1994年9月被授予复旦大学杰出教授。

2016年7月28日，在上海新华医院去世。\[2\]

## 荣誉

《英汉大词典》曾获得上海市哲学社会科学优秀成果特等奖，中国图书一等奖，上海市优秀图书特等奖，“五个一工程”优秀图书奖等多项奖项。

## 著作

论文：

  - 《逾越空间和时间的哈姆雷特》
  - 《莎士比亚概览》

共同主编：

  - 《新英汉词典》

译作：

  - 美国作家[欧文·肖](../Page/欧文·肖.md "wikilink")《幼狮》
  - 英国作家[达穆里埃](../Page/达穆里埃.md "wikilink")《[蝴蝶梦](../Page/蝴蝶梦.md "wikilink")》

校订：

  - 《[他改变了中国：江泽民传](../Page/他改变了中国：江泽民传.md "wikilink")》\[3\]

## 参考

  - [人文复旦
    如果有选择我还做老师——陸穀孫先生印象](https://web.archive.org/web/20070912041324/http://www.rwfd.fudan.edu.cn/xueshu/xuezhe/wenke/if.htm)
  - [人文复旦
    正字无已廿春秋——记我校外文系教授、《英汉大词典》主编陸穀孫](https://web.archive.org/web/20070702102533/http://www.rwfd.fudan.edu.cn/xueshu/xuezhe/wenke/zhengzi.htm)

[L陆](../Category/中国翻译家.md "wikilink")
[L陆](../Category/中国语言学家.md "wikilink")
[L陆](../Category/复旦大学校友.md "wikilink")
[L陆](../Category/复旦大学学者.md "wikilink")
[L陆](../Category/余姚人.md "wikilink")
[L陆](../Category/宁波裔上海人.md "wikilink")
[G谷](../Category/陆姓.md "wikilink")
[L陆](../Category/中国字词典编纂者.md "wikilink")
[L路](../Category/中国语言学家.md "wikilink")

1.
2.
3.  [《江泽民传》成为热门畅销书
    披露珍贵史实](http://www.china.com.cn/chinese/RS/788977.htm).[中国网](../Page/中国网.md "wikilink").