**文法**即文章的書寫法規，一般用來指以[文字](../Page/中文字.md "wikilink")、[詞語](../Page/詞語.md "wikilink")、[短句](../Page/短句.md "wikilink")、[句子](../Page/句子.md "wikilink")\[1\]編排而成的完整語句和文章的合理性組織。

## 文句組成的規律

  - 最簡單的語句組合一：[主詞及](../Page/主詞.md "wikilink")[動詞](../Page/動詞.md "wikilink")：

<!-- end list -->

1.  「我」「哭了」
2.  「天氣」「改變了」
3.  「哭笑」「難分」

<!-- end list -->

  - 最簡單的語句組合二：[主詞](../Page/主詞.md "wikilink")、[動詞及](../Page/動詞.md "wikilink")[受詞](../Page/受詞.md "wikilink")：（括弧內為隱藏語）

<!-- end list -->

1.  我愛你
2.  「道」「可」「道」，「(這個道字)」「〈並〉非」「常道」。《[道德經](../Page/道德經.md "wikilink")》[老子](../Page/老子.md "wikilink")
3.  「打羽毛球」「是」「最好的(運動)」
4.  「駕駛」「〈使我〉」「樂趣無窮」

<!-- end list -->

  - 比較複雜的語句組合一：[主語](../Page/主語.md "wikilink")〈[短句](../Page/短句.md "wikilink")〉、[謂語及](../Page/謂語.md "wikilink")[賓語](../Page/賓語.md "wikilink")：
      - 例：「我這個從來沒有戀愛經驗的少年人」、「竟然不知為了甚麼原因」而「喜歡了一個黃毛小丫頭」

<!-- end list -->

  - 複雜的語句組合二：主語及複式謂語：
      - 例：「有些頭腦守舊而自以為是的讀書人」、「認為如廁的動作不雅」，「所以寫成[出恭](../Page/出恭.md "wikilink")。」

在口語中，因為說話者或受話人都知道相方的關係和說話[主題](../Page/主題.md "wikilink")，所以大都隱去主詞或受詞。

但在書寫文章時，必須要清楚表明**主**和**賓**双方。

## 一般句子結构

### 动宾结构

一般口語，多是动宾结构的，即前面是[动词](../Page/动词.md "wikilink")，后面是[名词组成的](../Page/名词.md "wikilink")。這是因為受話人明知語句的[主語而不用說出來的關係](../Page/主語.md "wikilink")。

  - 〈你正在──主語不用說出來〉拍马屁、
  - 〈你真的是〉[痴線](../Page/痴線.md "wikilink")（廣東[慣用語](../Page/慣用語.md "wikilink")）、〈你是在〉敲竹杠、〈你在〉泼冷水、〈你不要〉耍花招等。

### 主谓结构

主谓（陈述）结构，即前面是[主詞或](../Page/主詞.md "wikilink")[主語](../Page/主語.md "wikilink")，后面加上一个描述或動作的單**字**、[词語或](../Page/词語.md "wikilink")[片語组成的](../Page/片語.md "wikilink")。這是因為說話語句中的[受詞或](../Page/受詞.md "wikilink")[賓語不用說出來的關係](../Page/賓語.md "wikilink")
。

  - 多為說話語句中的動詞部分，主語和賓語也不用說出了。 例如：（這個）天晓得，、〈他的〉嘴巴软〈了〉等

## 複詞結構

### 偏正结构

1.  由兩組〈或以上〉字詞所組成的〈複合〉名詞──用作主語部分，或賓語部分。

<!-- end list -->

  - **形容詞**〈相當於英語的[形容詞](../Page/形容詞.md "wikilink")〉及[名词](../Page/名词.md "wikilink")。
      - 替罪羊、耳边风、糊涂虫、绊脚石等。
      - 鬼画〈形容詞〉符

<!-- end list -->

1.  由兩組〈或以上〉字詞所組成的[短句](../Page/短句.md "wikilink")，如謂語部分。

<!-- end list -->

  - [動詞及](../Page/動詞.md "wikilink")**副词**。
      - 跳得很遠、大聲哭叫、清早起來

## 参阅

  - 漢字組合
      - [文字](../Page/文字.md "wikilink")、[詞語](../Page/詞語.md "wikilink")、[句子](../Page/句子.md "wikilink")、[文章](../Page/文章.md "wikilink")、[書本](../Page/書本.md "wikilink")、[編集](../Page/編集.md "wikilink")
  - 中文舊式[體裁](../Page/體裁.md "wikilink")
      - [楚辭](../Page/楚辭.md "wikilink")、秦漢[駢文](../Page/駢文.md "wikilink")、[詩](../Page/詩.md "wikilink")、[詞](../Page/詞_\(文學\).md "wikilink")、[歌](../Page/歌.md "wikilink")、[賦](../Page/賦.md "wikilink")、唐宋[古文](../Page/古文.md "wikilink")
      - [民謠](../Page/民謠.md "wikilink")、[元曲](../Page/元曲.md "wikilink")、明清[八股](../Page/八股.md "wikilink")、[文言文](../Page/文言文.md "wikilink")、[白話文](../Page/白話文.md "wikilink")
      - [曆](../Page/曆.md "wikilink")、[記事或](../Page/記.md "wikilink")[紀錄](../Page/紀錄.md "wikilink")、[經](../Page/經.md "wikilink")、[史](../Page/史.md "wikilink")、子
  - 中文[體裁](../Page/體裁.md "wikilink")
  - [抒情文](../Page/抒情文.md "wikilink")、[描寫文](../Page/描寫文.md "wikilink")
      - [散文](../Page/散文.md "wikilink")、[小說](../Page/小說.md "wikilink")
  - [記敘文](../Page/記敘文.md "wikilink")
      - [歷史](../Page/歷史.md "wikilink")、[回憶錄](../Page/回憶錄.md "wikilink")
  - [議論文](../Page/議論文.md "wikilink")
      - [社論](../Page/社論.md "wikilink")、[評述](../Page/評述.md "wikilink")、
  - [報告](../Page/報告.md "wikilink")
      - [論文](../Page/論文.md "wikilink")、[會議記錄](../Page/會議記錄.md "wikilink")
  - [說明文](../Page/說明文.md "wikilink")
      - [通告](../Page/通告.md "wikilink")、產品說明書、[書序](../Page/書序.md "wikilink")
  - [應用文](../Page/應用文.md "wikilink")
      - [合約](../Page/合約.md "wikilink")、[租約](../Page/租約.md "wikilink")
  - 文法[詞性分類](../Page/詞性.md "wikilink")

## 参考文献

{{-}}

[Category:中國文學](../Category/中國文學.md "wikilink")
[Category:文學理論](../Category/文學理論.md "wikilink")

1.  <http://www.books.com.tw/exep/prod/booksfile.php?item=0010438043>
    看到複雜長句，常不知其所以然……連看都看不懂，更遑論要寫出這樣的句子。