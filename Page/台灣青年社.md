**台灣青年社**（1960年2月28日－1970年1月1日）是[明治大學講師兼作家](../Page/明治大學.md "wikilink")[王育德成立於](../Page/王育德.md "wikilink")[日本的政治團體](../Page/日本.md "wikilink")。

1959年，[明治大學講師](../Page/明治大學.md "wikilink")[王育德擔任](../Page/王育德.md "wikilink")《台灣》雜誌[主筆後](../Page/主筆.md "wikilink")，與[吳枝鐘](../Page/吳枝鐘.md "wikilink")、[張春興](../Page/張春興.md "wikilink")、[林啓旭](../Page/林啓旭.md "wikilink")、[李元琳等人成立台灣社](../Page/李元琳.md "wikilink")\[1\]（與2006年成立之[台灣社並無關聯](../Page/台灣社.md "wikilink")），開始進行對[留學](../Page/留學.md "wikilink")[日本之台灣](../Page/日本.md "wikilink")[學生的](../Page/學生.md "wikilink")[啟蒙工作以及對](../Page/啟蒙.md "wikilink")[日本人的國際宣傳工作](../Page/日本人.md "wikilink")。並展開[二次大戰後的](../Page/二次大戰.md "wikilink")[台灣獨立運動](../Page/台灣獨立運動.md "wikilink")，發行[日文雙月刊](../Page/日文.md "wikilink")《台灣青年》來從事宣傳。不久，[許世楷](../Page/許世楷.md "wikilink")、[張國興](../Page/張國興.md "wikilink")、[周英明](../Page/周英明.md "wikilink")、[金美齡](../Page/金美齡.md "wikilink")、[林啟旭](../Page/林啟旭.md "wikilink")、[侯榮邦等台灣留日學生相繼加入](../Page/侯榮邦.md "wikilink")。1960年2月28日，台灣社改名為台灣青年社。

1962年，台灣青年社發行官方刊物《獨立通訊》，展開台灣內部啟蒙宣傳。1963年5月，台灣青年社改名為台灣青年會，展開組織留學生運動。1965年9月23日，王育德、[黃昭堂](../Page/黃昭堂.md "wikilink")、[許世楷](../Page/許世楷.md "wikilink")、[周英明](../Page/周英明.md "wikilink")、[金美齡](../Page/金美齡.md "wikilink")、[廖春榮等人將台灣青年會發展為](../Page/廖春榮.md "wikilink")[台灣青年獨立聯盟](../Page/台灣青年獨立聯盟.md "wikilink")，並發表綱領。1965年9月24日，台灣青年獨立聯盟在[東京都](../Page/東京都.md "wikilink")[銀座發動](../Page/銀座.md "wikilink")[示威](../Page/示威.md "wikilink")，要求[聯合國支持台灣](../Page/聯合國.md "wikilink")[自決](../Page/民族自決.md "wikilink")。[邱永漢](../Page/邱永漢.md "wikilink")、[辜寬敏加入後](../Page/辜寬敏.md "wikilink")，台灣青年獨立聯盟更擴大組織。1970年1月1日，台灣青年獨立聯盟併入[台獨聯盟](../Page/台獨聯盟.md "wikilink")。

## 參見

  - [台獨聯盟](../Page/台獨聯盟.md "wikilink")
  - [台灣共和國臨時政府](../Page/台灣共和國臨時政府.md "wikilink")

## 參考資料

[T台](../Category/台灣獨立運動組織.md "wikilink")
[T台](../Category/日本政治组织.md "wikilink")
[T台](../Category/1960年建立的組織.md "wikilink")
[T台](../Category/1970年廢除.md "wikilink")

1.  [わたし達の青春は勇敢な歌
    許千恵（台湾）２００２年８月３日](http://www.taiwannation.org.tw/jpn/chie1.htm)