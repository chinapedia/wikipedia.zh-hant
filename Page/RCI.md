**RCI**（前稱**RCI集團**、**國際度假公寓**或**Resort Condominiums
International**）是一家全球[分時渡假的度假酒店及權益交易組織](../Page/分時渡假.md "wikilink")，[溫德姆酒店集團的一個分支](../Page/溫德姆酒店集團.md "wikilink")。

創立於1974年，這家由[喬恩和](../Page/喬恩·迪翰.md "wikilink")[克里斯特爾·迪翰建立的公司已變成世界其中一家大型的](../Page/克里斯特爾·迪翰.md "wikilink")[分時分享交易拆家](../Page/分時分享.md "wikilink")。RCI有超過6,300家伙伴度假屋，遍布全球超過100個國家\[1\]，並在全球有會員約380萬名。

## 公司架構

RCI總裁兼首席執行官是Geoffrey A.
Ballotti。RCI公司的總部設在[美國](../Page/美國.md "wikilink")[新澤西州的](../Page/新澤西州.md "wikilink")[帕西帕尼](../Page/帕西帕尼.md "wikilink")。在[北美洲的會員辦事處位於](../Page/北美洲.md "wikilink")[印第安納州的](../Page/印第安納州.md "wikilink")[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")，而其對應的歐洲辦事處則位於[英國](../Page/英國.md "wikilink")[凱特林](../Page/凱特林.md "wikilink")。在其他城市，RCI也設有辦事處，例如：[墨西哥城](../Page/墨西哥城.md "wikilink")、[愛爾蘭的](../Page/愛爾蘭.md "wikilink")[科克](../Page/科克.md "wikilink")、[菲律賓的](../Page/菲律賓.md "wikilink")[馬尼拉](../Page/馬尼拉.md "wikilink")、[印度](../Page/印度.md "wikilink")[班加羅爾](../Page/班加羅爾.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[阿拉伯聯合酋長國的](../Page/阿拉伯聯合酋長國.md "wikilink")[迪拜和](../Page/迪拜.md "wikilink")[烏拉圭](../Page/烏拉圭.md "wikilink")[蒙得維的亞](../Page/蒙得維的亞.md "wikilink")。其位於[希臘](../Page/希臘.md "wikilink")[雅典的辦事處同時服務希臘及](../Page/雅典.md "wikilink")[土耳其兩個的國民](../Page/土耳其.md "wikilink")。

2007年7月，RCI與溫德姆酒店集團旗下的渡假別墅集團（Holiday Cottages Group）合併。

## 獎項

  - Jeff
    Parker代表RCI集團取得由[美國渡假村發展協會](../Page/美國渡假村發展協會.md "wikilink")（the
    American Resort Development Association）舉辦的2007年度"ACE"僱員獎\[2\]。
  - 從2009年到2011年，RCI被[印第安納州商會指名為其中一家最佳工作地點](../Page/印第安納州商會.md "wikilink")\[3\]\[4\]\[5\]。
  - 2009年，RCI集團墨西哥被指名為[墨西哥](../Page/墨西哥.md "wikilink")\[6\]
    及整個[拉丁美洲](../Page/拉丁美洲.md "wikilink")\[7\] 的最佳工作地點。

## 參考資料

## 參看

  - [分時度假](../Page/分時度假.md "wikilink")
  - [多重收入](../Page/多重收入.md "wikilink")
  - [QSTR](../Page/QSTR.md "wikilink")

## 外部連結

  - [RCI會員網站](http://www.rci.com/)
  - [GroupRCI網站](https://web.archive.org/web/20160118045340/http://www.grouprci.com./)
  - [Holiday Cottages Group](http://www.holidaycottagesgroup.co.uk/)
  - [Platinum Interchange](http://www.platinuminterchange.com/)

[Category:溫德姆環球公司](../Category/溫德姆環球公司.md "wikilink")

1.
2.
3.  [2011 Best Places to Work in
    Indiana](http://www.indianachamber.com/index.php/2011-best-places-to-work-in-indiana-rankings-announced)

4.  [2010 Best Places to Work in
    Indiana](http://www.indianachamber.com/index.php/2010-best-places-to-work-in-indiana-rankings-announced)

5.  [2009 Best Places to Work in
    Indiana](http://www.indianachamber.com/index.php/annual-best-places-to-work-in-indiana-list-announced-rankings-revealed-may-5)

6.  [Great Place to Work Institute
    Mexico](http://www.greatplacetowork.com.au/best/lists.php?year=2009&idListName=mexico500plus&detail=1&order=rank)
7.  [2009 List of Best Companies to Work for in Latin
    America](http://www.greatplacetowork.com/what_we_do/lists-la-la-2009.htm)