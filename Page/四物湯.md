**四物湯**是傳統[中醫流傳下來的藥方](../Page/中醫.md "wikilink")，最早出現記載於[宋朝的](../Page/宋朝.md "wikilink")「[太平惠民和劑局方](../Page/太平惠民和劑局方.md "wikilink")」。而四物湯就是使用[熟地黃](../Page/熟地黃.md "wikilink")、[白芍](../Page/白芍.md "wikilink")、[當歸及](../Page/當歸.md "wikilink")[川芎這四種中藥材](../Page/川芎.md "wikilink")。一般來說，它具有補血調經的效果，可減緩女性的[經痛](../Page/經痛.md "wikilink")。事實上，四物湯只要有血虛症狀，男女都可以喝，血虛症狀是中醫名詞，就是[貧血](../Page/貧血.md "wikilink")，因此只要有類似血虛的症狀，像是血紅素偏低、感覺到疲勞或是心悸等，都可以考慮使用四物湯來調理身體。應注意四物湯與**[四神汤](../Page/四神汤.md "wikilink")**的區別。

2007年，[台灣的](../Page/台灣.md "wikilink")[國家衛生研究院研究團隊發表於](../Page/國家衛生研究院.md "wikilink")《PLoS
ONE》網路期刊的论文証實四物湯的確有減緩經痛的功效。\[1\]\[2\]

### 加味四物湯

  - 桃紅四物湯：除四物外再加[桃仁跟](../Page/桃仁.md "wikilink")[紅花](../Page/紅花.md "wikilink")，可主治瘀血。
  - 聖愈湯：除四物外再加人參跟黃耆，可主治氣虛，此藥方出自《[醫宗金鑒](../Page/醫宗金鑒.md "wikilink")》。
  - 除四物外再加[秦艽](../Page/秦艽.md "wikilink")、[羌活可防頭風以及頭目暈眩](../Page/羌活.md "wikilink")。
  - 除四物外再加炮姜，主治產後陰血暴傷，陰無所附，而致發熱。\[3\]

## 參考資料

  - [四物湯 Si Wu
    Tang](http://lib-nt2.hkbu.edu.hk/database/cmed/cmfid/details.asp?lang=cht&id=F00073)
    中藥方劑圖像數據庫 (香港浸會大學中醫藥學院)

[Category:方劑](../Category/方劑.md "wikilink")
[Category:涼茶](../Category/涼茶.md "wikilink")

1.
2.
3.