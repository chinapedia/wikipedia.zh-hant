**橫溝正史**（，），[日本](../Page/日本.md "wikilink")[小說家暨](../Page/小說家.md "wikilink")[推理](../Page/推理.md "wikilink")[作家](../Page/作家.md "wikilink")，出生於[日本](../Page/日本.md "wikilink")[兵庫縣](../Page/兵库县.md "wikilink")[神戶市](../Page/神户市.md "wikilink")[中央區東川崎町](../Page/中央區_\(神戶市\).md "wikilink")，以[金田一耕助為主角的一系列小說而聞名於世](../Page/金田一耕助.md "wikilink")。

## 經歷

[Yokomizo-seitan3479.JPG](https://zh.wikipedia.org/wiki/File:Yokomizo-seitan3479.JPG "fig:Yokomizo-seitan3479.JPG")
1902年生於神戶市。父親的故鄉在岡山縣。

1921年，在《[新青年](../Page/新青年_\(日本\).md "wikilink")》雜志上發表處女作《》。

1924年，從大阪藥學專門學校（現[大阪大學藥學部](../Page/大阪大学.md "wikilink")）畢業後，一度從事[藥劑師工作](../Page/藥劑師.md "wikilink")。後於1926年受到[江戶川亂步的邀請前往](../Page/江戶川亂步.md "wikilink")[東京並加入了](../Page/东京.md "wikilink")[博文館](../Page/博文館.md "wikilink")。1927年出任《新青年》的总编辑，之后在担任《[文藝俱樂部](../Page/文藝俱樂部.md "wikilink")》《[偵探小說](../Page/推理小說.md "wikilink")》等刊物总编辑的同时从事创作和翻译活动。1932年借停刊的机会退出博文馆成为职业作家。他在戰前執筆的代表作《鬼火》、《倉庫》及《蠟人》等，極富濃郁的[耽美主義色彩](../Page/耽美主義.md "wikilink")。

但是，因為[肺結核的惡化](../Page/肺結核.md "wikilink")，作家前往[信州養病並停止創作](../Page/信州.md "wikilink")。由於戰爭時期對偵探小說的嚴格限制，而作家又没有把握住当时的潮流，可以說是一段懷才不遇的時期。由於作家活動受到限制經濟上也十分困難，病況一度惡化到本人也準備好後事的程度。戰後由於治療的藥物價格急劇崩潰，病況逐漸好轉。

1945年4月之後，搬到岡山縣居住三年。第二次世界大戰結束後，作家借[偵探小說可以自由發表之際](../Page/推理小說.md "wikilink")，充分發揮自己才能。1946年他率先推出《本陣殺人事件》及《蝴蝶殺人事件》兩部純解謎長篇推理小說，大大提升了日本推理小說水準，縮短與歐美的差距，從而改變了戰前[變格推理小說為主流的趨勢](../Page/變格推理小說.md "wikilink")（指不是以解謎為重點的推理小說）。1948年凭《本陣殺人事件》穫得第一屆日本偵探作家俱樂部獎（之後更名為[日本推理作家協會獎](../Page/日本推理作家协会奖.md "wikilink")）長篇部門獎。1957年以后，「變格推理小說」在術語上已甚少使用。

橫溝氏的創作量驚人，如《獄門島》、《八墓村》及《惡魔的手毯歌》等都是膾炙人口的傑作。其中《獄門島》被公認為日本推理文學史上的經典名著。

其戰後推出的名作《本陣殺人事件》以名探金田一耕助為主角
，獲得壓倒性成功。後來他便以金田一耕助為主角，撰寫一系列解謎推理小說，金田一耕助登場的長短篇總共有77部。在小說中，偵探金田一耕助活躍的主要舞台是東京及周邊地區，以及作者筆下充滿恐怖傳聞的岡山縣地方。前者表現的是戰後都市的頹廢和倒錯，後者多以鄉村和血緣關係為主軸。一般讀者對後者的評價較高。由於作者的妙筆生花，金田一耕助遂成為日本推理小說史上三大名探之一。1968年，[講談社的](../Page/讲谈社.md "wikilink")《[周刊少年](../Page/周刊少年.md "wikilink")》發表了改編自《八墓村》的漫畫連載（作畫：[影丸讓也](../Page/影丸讓也.md "wikilink")）。由此為契機而掀起了一股橫溝正史的熱潮。因為電影系列和電視系列的熱播，即使是推理小說迷以外的人也知道金田一這個名字。

橫溝氏本人患有[乘車恐懼症](../Page/恐懼症.md "wikilink")，舉凡[車](../Page/車.md "wikilink")、[船](../Page/船.md "wikilink")、[飛機都在避忌之列](../Page/飛機.md "wikilink")，萬不得已必須外出應酬，則叫特約大轎車出門。

1981年12月28日病逝於寓所，享年79歲。

## 紀念

1980年開始由[角川書店主持設立專門鼓勵新人的長篇推理小說獎](../Page/角川書店.md "wikilink")：[橫溝正史獎](../Page/橫溝正史獎.md "wikilink")。

## 部份作品列表

  - 1921年 [令人恐怖的四月](../Page/令人恐怖的四月.md "wikilink")
    ([恐ろしき四月馬鹿](../Page/恐ろしき四月馬鹿.md "wikilink"))
  - 1927年 [山名耕作不可思議的生活](../Page/山名耕作不可思議的生活.md "wikilink")
    ([山名耕作の不思議な生活](../Page/山名耕作の不思議な生活.md "wikilink"))
  - 1928年 [悪魔の家](../Page/悪魔の家.md "wikilink")
  - 1928年 [悪魔の設計図](../Page/悪魔の設計図.md "wikilink")
  - 1930年 [芙蓉公館的秘密](../Page/芙蓉公館的秘密.md "wikilink")
    ([芙蓉屋敷の秘密](../Page/芙蓉屋敷の秘密.md "wikilink"))
  - 1931年 [殺人日曆](../Page/殺人日曆.md "wikilink")
    ([殺人暦](../Page/殺人暦.md "wikilink"))
  - 1932年 [場侯爵一家](../Page/場侯爵一家.md "wikilink")
    ([塙侯爵一家](../Page/塙侯爵一家.md "wikilink"))
  - 1932年 [詛咒之塔](../Page/詛咒之塔.md "wikilink")
    ([呪いの塔](../Page/呪いの塔.md "wikilink"))
  - 1933年 [附體女魔](../Page/附體女魔.md "wikilink")
    ([憑かれた女](../Page/憑かれた女.md "wikilink"))
  - 1935年 [鬼火](../Page/鬼火_\(横溝正史\).md "wikilink")
  - 1935年 [藏之中](../Page/藏之中.md "wikilink")
    ([蔵の中](../Page/蔵の中.md "wikilink"))
  - 1936年 [真珠郎](../Page/真珠郎.md "wikilink")
  - 1936年 [夜光虫](../Page/夜光蟲_\(横溝正史\).md "wikilink")
  - 1937年 [女人的幻影](../Page/女人的幻影.md "wikilink")
    ([幻の女](../Page/幻の女.md "wikilink"))
  - 1937年 [花髑髏](../Page/花髑髏.md "wikilink")
  - 1937年 [穿青色外套的女人](../Page/穿青色外套的女人.md "wikilink")
    ([青い外套を着た女](../Page/青い外套を着た女.md "wikilink"))
  - 1937年 [誘蛾燈](../Page/誘蛾燈.md "wikilink")
  - 1938年 [兩張面具](../Page/兩張面具.md "wikilink")
    ([双仮面](../Page/双仮面.md "wikilink"))
  - 1938年 [假面劇場](../Page/假面劇場.md "wikilink")
    ([仮面劇場](../Page/仮面劇場.md "wikilink"))
  - 1939年 [血蝙蝠](../Page/血蝙蝠.md "wikilink")
  - 1946年 [刺青的男人](../Page/刺青的男人.md "wikilink")
    ([刺青された男](../Page/刺青された男.md "wikilink"))
  - 1946年 [本陣殺人事件](../Page/本陣殺人事件.md "wikilink")（譯者：高詹燦）
  - 1946年 [蝴蝶殺人事件](../Page/蝴蝶殺人事件.md "wikilink")（蝶々殺人事件）（譯者：張智淵）
  - 1946年 [空蝉処女](../Page/空蝉処女.md "wikilink")
  - 1946年 [抱波斯猫的女人](../Page/抱波斯猫的女人.md "wikilink")
    ([ペルシャ猫を抱く女](../Page/ペルシャ猫を抱く女.md "wikilink"))
  - 1947年 [獄門島](../Page/獄門島.md "wikilink")（）（譯者：李美惠）
  - 1947年 [殺人鬼](../Page/殺人鬼_\(横溝正史\).md "wikilink")
  - 1947年 [黑猫亭事件](../Page/黑猫亭事件.md "wikilink")
  - 1948年 [驚嚇盒殺人事件](../Page/驚嚇盒殺人事件.md "wikilink")
    ([びっくり箱殺人事件](../Page/びっくり箱殺人事件.md "wikilink"))
  - 1948年 [夜行](../Page/夜行.md "wikilink")（）（譯者：高詹燦）
  - 1949年 [八墓村](../Page/八墓村.md "wikilink")（）（譯者：吳得智）
  - 1949年 [女人看見了](../Page/女人看見了.md "wikilink")
    ([女が見ていた](../Page/女が見ていた_\(横溝正史\).md "wikilink"))
  - 1949年 [死見具](../Page/死見具.md "wikilink")
    ([死仮面](../Page/死仮面.md "wikilink"))
  - 1950年 [犬神家一族](../Page/犬神家一族.md "wikilink")（）（譯者：吳得智）
  - 1950年 [迷路的新娘](../Page/迷路的新娘.md "wikilink")
    ([迷路の花嫁](../Page/迷路の花嫁.md "wikilink"))
  - 1951年
    [女王蜂](../Page/女王蜂.md "wikilink")（譯者：[鍾蕙淳](../Page/鍾蕙淳.md "wikilink")）
  - 1951年 [惡魔前來吹笛](../Page/惡魔前來吹笛.md "wikilink")（）（譯者：婁美蓮）
  - 1952年 [幽霊座](../Page/幽霊座.md "wikilink")
  - 1953年 [不死蝶](../Page/不死蝶.md "wikilink")
  - 1954年 [花園的惡魔](../Page/花園的惡魔.md "wikilink")
    ([花園の悪魔](../Page/花園の悪魔.md "wikilink"))
  - 1954年 [幽霊男](../Page/幽霊男.md "wikilink")
  - 1955年 [吸血蛾](../Page/吸血蛾.md "wikilink")
  - 1955年 [毒箭](../Page/毒箭.md "wikilink")
    ([毒の矢](../Page/毒の矢.md "wikilink"))
  - 1955年 [三首塔](../Page/三首塔.md "wikilink")（三つ首塔）（譯者：鍾蕙淳）
  - 1956年 [死神之箭](../Page/死神之箭.md "wikilink")
    ([死神の矢](../Page/死神の矢.md "wikilink"))
  - 1956年 [魔女的日曆](../Page/魔女的日曆.md "wikilink")
    ([魔女の暦](../Page/魔女の暦.md "wikilink"))
  - 1956年 [七張面具](../Page/七張面具.md "wikilink")
    ([七つの仮面](../Page/七つの仮面.md "wikilink"))
  - 1956年 [迷路莊慘劇](../Page/迷路莊慘劇.md "wikilink")（迷路荘の惨劇）（譯者：高詹燦）
  - 1956年 [華麗的野戰](../Page/華麗的野戰.md "wikilink")
    ([華やかな野獣](../Page/華やかな野獣.md "wikilink"))
  - 1957年 [惡魔的手毬歌](../Page/惡魔的手毬歌.md "wikilink")（悪魔の手毬唄）（譯者：吳得智）
  - 1957年 [出租船十三號](../Page/出租船十三號.md "wikilink")
    ([貸しボート十三号](../Page/貸しボート十三号.md "wikilink"))
  - 1957年 [壺中美人](../Page/壺中美人.md "wikilink")
  - 1957年 [手持中國扇的女人](../Page/手持中國扇的女人.md "wikilink")
    ([支那扇の女](../Page/支那扇の女.md "wikilink"))
  - 1957年 [門後的女人](../Page/門後的女人.md "wikilink")
    (又譯：[扉影之女](../Page/扉影之女.md "wikilink"))
    ([扉の影の女](../Page/扉の影の女.md "wikilink"))
  - 1958年 [惡魔的聖誕節](../Page/惡魔的聖誕節.md "wikilink")
    ([悪魔の降誕祭](../Page/悪魔の降誕祭.md "wikilink"))
  - 1958年 [黑桃皇后](../Page/黑桃皇后_\(横溝正史\).md "wikilink")（）
  - 1958年 [惡魔的寵兒](../Page/惡魔的寵兒.md "wikilink")（）（譯者：高詹燦）
  - 1960年 [白與黑](../Page/白與黑.md "wikilink")（）（譯者：高詹燦）
  - 1962年 [惡魔的百唇譜](../Page/惡魔的百唇譜.md "wikilink")
    ([悪魔の百唇譜](../Page/悪魔の百唇譜.md "wikilink"))
  - 1962年 [假面舞會](../Page/假面舞會.md "wikilink")（仮面舞踏会）（譯者：高詹燦）
  - 1963年 [暗夜里的黑豹](../Page/暗夜里的黑豹.md "wikilink")
    ([夜の黒豹](../Page/夜の黒豹.md "wikilink"))
  - 1975年 [醫院坡上吊之家](../Page/醫院坡上吊之家.md "wikilink")（）（譯者：高詹燦）
  - 1979年 [惡靈島](../Page/惡靈島.md "wikilink")（惡霊島）（譯者：高詹燦）

## 參考

  - [橫溝正史百科](http://homepage3.nifty.com/kakeya/ys_pedia/ys_pedia_index.html)

## 外部連結

  - [橫溝正史館](https://web.archive.org/web/20110515144627/http://www.city.yamanashi.yamanashi.jp/sight/tour/culture/yokomizo.html)
  - [橫溝正史百科](http://homepage3.nifty.com/kakeya/ys_pedia/ys_pedia_index.html)
  - [橫溝正史疏開的地方](http://yakageniche.com/kindaichi.html)

[Category:日本推理小說作家](../Category/日本推理小說作家.md "wikilink")
[Category:日本推理作家協會獎獲得者](../Category/日本推理作家協會獎獲得者.md "wikilink")
[Category:日本编辑](../Category/日本编辑.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")
[Category:勳三等瑞寶章獲得者](../Category/勳三等瑞寶章獲得者.md "wikilink")
[Category:大阪大學校友](../Category/大阪大學校友.md "wikilink")
[Category:瑞穗金融集團人物](../Category/瑞穗金融集團人物.md "wikilink")