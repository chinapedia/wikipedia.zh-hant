**马约特大區**（；[斯馬奧萊語](../Page/斯馬奧萊語.md "wikilink")：*Maore*；[馬拉加斯語](../Page/馬拉加斯語.md "wikilink")：*Mahori*）
通稱**馬約特**，位于[莫桑比克海峡](../Page/莫桑比克海峡.md "wikilink")，与[大科摩罗岛](../Page/大科摩罗岛.md "wikilink")、[昂儒昂岛](../Page/昂儒昂岛.md "wikilink")、[莫埃利岛共同组成](../Page/莫埃利岛.md "wikilink")[科摩罗群岛](../Page/科摩罗群岛.md "wikilink")。面积为374平方公里，经济以农业为主，主要生产香子兰等香料，是法国的海外大区，下辖一个省，即马约特省。马约特岛包括大陆地岛、小陆地岛以及周围一些小岛，首府和最大城市[马穆楚](../Page/马穆楚.md "wikilink")。

2009年3月29日，进行了一次关于在2011年成为法国海外省的公民投票\[1\]，结果以95.2%赞成而通过\[2\]。

## 地理

[Cn-map.png](https://zh.wikipedia.org/wiki/File:Cn-map.png "fig:Cn-map.png")
主要岛屿包括大陆地岛（Grande-Terre）和小陆地岛（La
Petite-Terre），地质学上来说，大陆地岛是科摩罗地区最古老的岛屿，有39公里长，22公里宽，最高点为海拔660米高的贝拿勒斯山（*Mont
Bénara*）。由于是火山岩构成的岛屿，一些地区的土地特别的肥沃。珊瑚礁围绕着一些岛屿起到了保护船只和栖息鱼群的作用。

[藻德济是](../Page/藻德济.md "wikilink")1977年前馬約特的首都，位于小陆地岛上，这座岛屿有着10公里长，是大陆地岛周边几个零星小岛中最大的一个。马约特是独立的印度洋委员会成员。

## 政治

马约特的政治框架为法国海外领地的议会民主制，马约特省委員會主席（President of the General
Council）是岛内的政府首脑，实行多党制，政府拥有最大的行政权力。

在法国国民议会中马约特拥有一个代表资格以及两个法国参议院议员席位。

## 历史和现今地位

马约特的地位与一个省相当，拥有一个省委员会。根据2001年7月11日法律，它确切上应为“省级行政区域”（collectivité
départementale），并且有权利在2010年选择成为全权的法国省。马约特下辖17个市镇，组成同样数量的乡。

### 历史

在1500年，马约特岛（阿拉伯语: جزيرة الموت）成为了伊斯兰教的君主领地。

1503年，马约特岛被葡萄牙探险者发现。但未侵占 。
16世纪起[马尔加什人开始统治马约特岛和科摩罗](../Page/梅里纳人.md "wikilink")，直到1843年割讓給[法國](../Page/法國.md "wikilink")。1974年和1976年，马约特岛的两次公民投票分別以63.8%和99.4%表決維持與法國的关系而不獨立。科摩罗则宣称拥有马约特岛的主权。在1976年[联合国安理会一份关于承認科摩罗對马约特主權的決議草案中](../Page/聯合國安全理事會.md "wikilink")，雖獲14个會員國支持，但最终被身為[安理会常任理事国的法國一票否决](../Page/联合国安全理事会常任理事国.md "wikilink")。

岛屿的防務完全由法国政府承担，[法国军队在马约特岛设有一支駐軍](../Page/法国军队.md "wikilink")，即[法國外籍兵團马约特特遣队](../Page/法國外籍兵團马约特特遣队.md "wikilink")。

|align=left|

  - [马约特省委員會主席](../Page/马约特省委員會主席.md "wikilink")
      - 主席: Saïd Omar Oili
  - [马约特政党列表](../Page/马约特政党列表.md "wikilink")
  - [选举](../Page/马约特选举.md "wikilink"):
    [2008](../Page/马约特省议会选举,_2008.md "wikilink")

## 行政

马约特划分为17个市镇行政区，也被划分成了19个行政乡(图中未标示)每个市镇有着对应的一个行政乡，而首府兼最大城市[马穆楚有着三个行政乡](../Page/马穆楚.md "wikilink")，这些行政单位都不属于法国21个大区(Région)的范畴。

<table>
<tbody>
<tr class="odd">
<td><ol>
<li><a href="../Page/藻德济.md" title="wikilink">藻德济</a>（Dzaoudzi）</li>
<li><a href="../Page/帕芒济.md" title="wikilink">帕芒济</a>（Pamandzi）</li>
<li><a href="../Page/马穆楚.md" title="wikilink">马穆楚</a>（Mamoudzou）</li>
<li><a href="../Page/当伯尼.md" title="wikilink">当伯尼</a>（Dembeni）</li>
<li><a href="../Page/班代莱.md" title="wikilink">班代莱</a>（Bandrele）</li>
<li><a href="../Page/卡尼-凯利.md" title="wikilink">卡尼-凯利</a>（Kani-Kéli）</li>
<li><a href="../Page/布埃尼.md" title="wikilink">布埃尼</a>（Bouéni）</li>
<li><a href="../Page/希龙居伊.md" title="wikilink">希龙居伊</a>（Chirongui）</li>
<li><a href="../Page/萨达.md" title="wikilink">萨达</a>（Sada）</li>
<li><a href="../Page/乌昂加尼.md" title="wikilink">乌昂加尼</a>（Ouangani）</li>
<li><a href="../Page/希科尼.md" title="wikilink">希科尼</a>（Chiconi）</li>
<li><a href="../Page/辛戈尼.md" title="wikilink">辛戈尼</a>（Tsingoni）</li>
<li><a href="../Page/姆特桑冈乌日.md" title="wikilink">姆特桑冈乌日</a>（M'Tsangamouji）</li>
<li><a href="../Page/阿库阿.md" title="wikilink">阿库阿</a>（Acoua）</li>
<li><a href="../Page/姆特桑博罗.md" title="wikilink">姆特桑博罗</a>（Mtsamboro）</li>
<li><a href="../Page/邦德拉布阿.md" title="wikilink">邦德拉布阿</a>（Bandraboua）</li>
<li><a href="../Page/库恩古.md" title="wikilink">库恩古</a>（Koungou）</li>
</ol></td>
</tr>
</tbody>
</table>

[Mayotte_administrative1.PNG](https://zh.wikipedia.org/wiki/File:Mayotte_administrative1.PNG "fig:Mayotte_administrative1.PNG")

## 经济

马约特的官方货币为[欧元](../Page/欧元.md "wikilink")。

据[INSEE的评估](../Page/INSEE.md "wikilink")，2001年马约特的[GDP总计为](../Page/国内生产总值.md "wikilink")6.1亿欧元
(根据2001年的汇率约等于5.47亿美元；根据2008年的汇率约等于9.03亿美元)。\[3\] 同一时期的人均国内生产总值为3,960欧元
(2001年时期的3,550美元;
2008年时期的5,859美元)，\[4\]这个数据比同期的[科摩罗高出了](../Page/科摩罗.md "wikilink")9倍之多，但也只是临近法国海外省[留尼汪GDP的三分之一和法国本土的](../Page/留尼汪.md "wikilink")16%。\[5\]

## 交通

马约特的铁路全长10.5公里，有着总长度为93公里的[公路](../Page/公路.md "wikilink")，其中72公里为铺装公路。马约特岛内没有水道，最大港口是[藻德济](../Page/藻德济.md "wikilink")，并有着一座飞机场。

## 人口统计

[Vue-Sada.jpg](https://zh.wikipedia.org/wiki/File:Vue-Sada.jpg "fig:Vue-Sada.jpg")\]\]
[Résidences_SIM_depuis_Mandzarisoa.jpg](https://zh.wikipedia.org/wiki/File:Résidences_SIM_depuis_Mandzarisoa.jpg "fig:Résidences_SIM_depuis_Mandzarisoa.jpg")
在2012年的[人口普查中](../Page/人口普查.md "wikilink")，马约特有着216,452名居民。\[6\]而在2012年的普查中人口有65%出生在本地，4%出身在法兰西共和国的其他地方，28%为[科摩罗的移民](../Page/科摩罗.md "wikilink")，2.8%为[马达加斯加移民](../Page/马达加斯加.md "wikilink")，另外还有0.2%来自其他国家。

### 历史上的人口数

## 參見

  - [法國](../Page/法國.md "wikilink")
  - [葛摩](../Page/葛摩.md "wikilink")

## 参考资料

## 外部链接

  - [CIA World Factbook -
    *Mayotte*](https://web.archive.org/web/20120921084632/https://www.cia.gov/library/publications/the-world-factbook/geos/mf.html)

  -
  - [WorldStatesmen- Mayotte](http://www.worldstatesmen.org/Mayotte.htm)

  -
  - [Analysis of the linguistic situation on
    Mayotte](http://www.tlfq.ulaval.ca/axl/afrique/mayotte.htm)

  - ["Voyages...Visages" - Another way of travelling and
    seeing](http://www.patricia-cardet.net/pages_techniques/intro_mayotte_e.php)

  - [Comité du tourisme de Mayotte](http://www.mayotte-tourisme.com/)
    Official tourism website

[Category:非洲地理](../Category/非洲地理.md "wikilink")
[Category:法国省份](../Category/法国省份.md "wikilink")
[Category:科摩罗](../Category/科摩罗.md "wikilink")
[Category:马约特](../Category/马约特.md "wikilink")

1.

2.

3.

4.
5.
6.