**翡若翠科**共有9[属约](../Page/属.md "wikilink")240[种](../Page/种.md "wikilink")，主要分布在[南美洲](../Page/南美洲.md "wikilink")、[非洲和](../Page/非洲.md "wikilink")[马达加斯加岛](../Page/马达加斯加岛.md "wikilink")，少数在[阿拉伯半岛和](../Page/阿拉伯半岛.md "wikilink")[中国](../Page/中国.md "wikilink")。[中国只有](../Page/中国.md "wikilink")1[属几种](../Page/属.md "wikilink")。

本[科](../Page/科.md "wikilink")[植物都是旱生型的](../Page/植物.md "wikilink")。

  - 属:

<!-- end list -->

  - [芒苞草属](../Page/芒苞草属.md "wikilink") *Acanthochlamys* P.C.Kao
  - *[Aylthonia](../Page/Aylthonia.md "wikilink")* N.L.Menezes
  - *[Barbacenia](../Page/Barbacenia.md "wikilink")* Vand.
  - *[Barbaceniopsis](../Page/Barbaceniopsis.md "wikilink")* L.B.Sm.
  - *[Burlemarxia](../Page/Burlemarxia.md "wikilink")* N.L.Menezes &
    Semir
  - *[Nanuza](../Page/Nanuza.md "wikilink")* L.B.Sm. & Ayensu
  - *[Pleurostima](../Page/Pleurostima.md "wikilink")* Raf.
  - [翡若翠属](../Page/翡若翠属.md "wikilink") *Vellozia* Vand.
  - [旱生丛菔属](../Page/旱生丛菔属.md "wikilink") *Xerophyta* Juss.

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[翡若翠科](http://delta-intkey.com/angio/www/vellozia.htm)
  - [APG II
    网站中的翡若翠科](http://www.mobot.org/MOBOT/Research/APweb/orders/pandanalesweb.htm#Velloziaceae)
  - [NCBI中的翡若翠科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=16376&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL中的翡若翠科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Velloziaceae)

[category:植物科名](../Page/category:植物科名.md "wikilink")

[\*](../Category/翡若翠科.md "wikilink")