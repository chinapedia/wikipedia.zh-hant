**Favorskii重排反应**（**Favorskii重排**），常误写为**Favorski重排反应**，在[醇钠](../Page/醇钠.md "wikilink")、[氢氧化钠](../Page/氢氧化钠.md "wikilink")、[氨基钠等碱性催化剂存在下](../Page/氨基钠.md "wikilink")，α-[卤代酮](../Page/卤代酮.md "wikilink")（α-[氯代酮或α](../Page/氯代酮.md "wikilink")-[溴代酮](../Page/溴代酮.md "wikilink")）失去[卤离子](../Page/卤离子.md "wikilink")，[重排成具有相同碳原子数的](../Page/重排.md "wikilink")[羧酸酯](../Page/羧酸酯.md "wikilink")、[羧酸](../Page/羧酸.md "wikilink")、[酰胺的反应](../Page/酰胺.md "wikilink")。\[1\]
[Favorskii_Rearrangement.png](https://zh.wikipedia.org/wiki/File:Favorskii_Rearrangement.png "fig:Favorskii_Rearrangement.png")

环酮反应得到少一个碳的环烷基羧酸。\[2\]
使用的碱可以是[氢氧根离子](../Page/氢氧根.md "wikilink")、[醇盐负离子或](../Page/醇盐.md "wikilink")[胺](../Page/胺.md "wikilink")，产物分别为羧酸、酯和酰胺。α,α'-二卤代酮在反应条件下[消除HX生成](../Page/消除.md "wikilink")[α,β-不饱和羰基化合物](../Page/α,β-不饱和羰基化合物.md "wikilink")。\[3\]\[4\]\[5\]\[6\]\[7\]\[8\]\[9\]

环酮的反应如下，常用于合成张力较大的四元环体系。

[Favorskii_Rearrangement_Scheme.png](https://zh.wikipedia.org/wiki/File:Favorskii_Rearrangement_Scheme.png "fig:Favorskii_Rearrangement_Scheme.png")

## 反应机理

Favorskii重排反应的[反应机理为](../Page/反应机理.md "wikilink")：首先在氯原子另一侧形成[烯醇负离子](../Page/烯醇.md "wikilink")，负离子进攻另一侧的碳原子，氯离子离去，形成一个[环丙酮并五元环的中间体](../Page/环丙酮.md "wikilink")。受[氢氧根离子进攻](../Page/氢氧根.md "wikilink")，羰基打开，打开三元环，得到羧基邻位的碳负离子，最后获得一个质子得到产物。

[Favorskii_rearrangement_mechanism.svg](https://zh.wikipedia.org/wiki/File:Favorskii_rearrangement_mechanism.svg "fig:Favorskii_rearrangement_mechanism.svg")

## Favorskii光反应

该重排反应也可以是通过[自由基机理进行的](../Page/自由基.md "wikilink")[光化学反应](../Page/光化学反应.md "wikilink")。某些被对羟基苯乙酰基保护的[磷酸酯](../Page/磷酸酯.md "wikilink")（如[ATP](../Page/三磷酸腺苷.md "wikilink")）反应，经过三线态[双自由基](../Page/双自由基.md "wikilink")**3**和具[二酮结构的](../Page/二酮.md "wikilink")[螺环化合物](../Page/螺环化合物.md "wikilink")**4**\[10\]，最终重排得到对羟基[苯乙酸以及磷酸基团](../Page/苯乙酸.md "wikilink")。\[11\]

[PhotoFavorskiiGivens2008.png](https://zh.wikipedia.org/wiki/File:PhotoFavorskiiGivens2008.png "fig:PhotoFavorskiiGivens2008.png")

## 参见

  - [立方烷的经典合成路线涉及两个Favorskii重排反应](../Page/立方烷.md "wikilink")
  - [化学反应列表](../Page/化学反应列表.md "wikilink")

## 外部链接

  - [反应机理:
    gif动画](https://commons.wikimedia.org/wiki/File%3AFavorskii_r_startAnimGif.gif)

## 参考资料

[Category:重排反应](../Category/重排反应.md "wikilink")
[Category:人名反应](../Category/人名反应.md "wikilink")

1.  [基础有机化学](../Page/基础有机化学.md "wikilink")
    [刑其毅等著](../Page/刑其毅.md "wikilink")
    [高等教育出版社](../Page/高等教育出版社.md "wikilink") 第三版（上册）542页
2.  [Organic Syntheses](../Page/Organic_Syntheses.md "wikilink"), Coll.
    Vol. 4, p.594 (1963); Vol. 39, p.37 (1959).
    ([Article](http://www.orgsyn.org/orgsyn/prep.asp?prep=cv4p0594))
3.  Favorskii, A. E. *J. Russ. Phys. Chem. Soc.* **1894**, *26*, 590.
4.  Favorskii, A. E. *J. Russ. Phys. Chem. Soc.* **1905**, *37*, 643.
5.  Favorskii, A. E. *J. Prakt. Chem.* **1913**, *88*, 658.
6.  Kende, A. S. *Org. React.* **1960**, *11*, 261-316. (Review)
7.  *[Organic Syntheses](../Page/Organic_Syntheses.md "wikilink")*,
    Coll. Vol. 6, p.368 (1988); Vol. 56, p.107 (1977).
    ([Article](http://www.orgsyn.org/orgsyn/prep.asp?prep=cv6p0368))
8.  Shioiri, T.; Kawai, N. *[J. Org.
    Chem.](../Page/J._Org._Chem..md "wikilink")* **1978**, *43*, 2936.
9.  [Organic Syntheses](../Page/Organic_Syntheses.md "wikilink"), Coll.
    Vol. 7, p.135 (1990); Vol. 62, p.191 (1984).
    ([Article](http://www.orgsyn.org/orgsyn/prep.asp?prep=cv7p0135))
10. *The Photo-Favorskii Reaction of p-Hydroxyphenacyl Compounds Is
    Initiated by Water-Assisted, Adiabatic Extrusion of a Triplet
    Biradical* Richard S. Givens, Dominik Heger, Bruno Hellrung, Yavor
    Kamdzhilov, Marek Mac, Peter G. Conrad, II, Elizabeth Cope, Jong I.
    Lee, Julio F. Mata-Segreda, Richard L. Schowen and Jakob Wirz. [J.
    Am. Chem. Soc.](../Page/J._Am._Chem._Soc..md "wikilink") **2008**,
    130, 3307-3309
11. *New Photoactivated Protecting Groups. 6. p-Hydroxyphenacyl: A
    Phototrigger for Chemical and Biochemical Probes* Chan-Ho Park and
    Richard S. Givens [J. Am. Chem.
    Soc.](../Page/J._Am._Chem._Soc..md "wikilink"); **1997**; 119(10) pp
    2453 - 2463; (Article)