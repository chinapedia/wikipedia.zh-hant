**威廉·巴芬**（）是16世紀的[英國](../Page/英國.md "wikilink")[航海家](../Page/航海家.md "wikilink")。在[倫敦出生](../Page/倫敦.md "wikilink")。生前曾尋找“[西北航道](../Page/西北航道.md "wikilink")”。命名[北美洲北部的一個](../Page/北美洲.md "wikilink")[島為](../Page/島.md "wikilink")[巴芬島](../Page/巴芬島.md "wikilink")，[巴芬島與](../Page/巴芬島.md "wikilink")[格陵蘭島之間的](../Page/格陵蘭.md "wikilink")[海灣為](../Page/海灣.md "wikilink")[巴芬灣](../Page/巴芬灣.md "wikilink")，並且是[世界第一個圍繞巴芬島航行一周的航海家](../Page/世界.md "wikilink")。據說航海時通過觀測[月球確定](../Page/月球.md "wikilink")[經緯度的方法由威廉](../Page/經緯度.md "wikilink")·巴芬首創。

## 生平

1584年出生于倫敦。早年的生活不詳。

成年後曾與Robert Bylot
船長乘「發現號」海船考察北美洲與巴芬島之間的[哈得遜海峽](../Page/哈得遜海峽.md "wikilink")。

1616年以「發現號」領航員的身份，再次航行北美洲，深入[哈得遜灣內](../Page/哈得遜灣.md "wikilink")，其所達之處比在1587年也探索哈得遜灣的英國航海家John
Davis，多遠了483[公里](../Page/公里.md "wikilink")。

而後，在[不列顛東印度公司任職](../Page/不列顛東印度公司.md "wikilink")，期間曾測量[紅海和](../Page/紅海.md "wikilink")[波斯灣](../Page/波斯灣.md "wikilink")。

1622年是他生平最後一次在波斯灣航行，航行期間于1月23日死於英國-[波斯同盟軍攻占](../Page/波斯.md "wikilink")[葡萄牙要塞](../Page/葡萄牙.md "wikilink")[克什姆岛之役](../Page/克什姆岛.md "wikilink")。

[B](../Category/英国航海家.md "wikilink")
[B](../Category/戰爭身亡者.md "wikilink")
[B](../Category/北極探險家.md "wikilink")