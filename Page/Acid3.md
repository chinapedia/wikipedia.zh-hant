**Acid3**由[網頁標準計劃設計](../Page/網頁標準計劃.md "wikilink")，是一份[網頁瀏覽器及設計軟體之標準相容性的測試網頁](../Page/網頁瀏覽器.md "wikilink")，Acid3開發始於2007年4月\[1\]，2008年3月3日正式發布\[2\]。其測試焦點集中在[ECMAScript](../Page/ECMAScript.md "wikilink")、[DOM](../Page/DOM.md "wikilink")
Level 3、Media Queries和data: URL。以瀏覽器開啟此測試網頁後，頁面會不斷載入功能、直接給予分數，滿分為100分。

## 歷史

Google員工[伊恩·希克森於](../Page/伊恩·希克森.md "wikilink")2007年4月開始進行測試，但開發進展緩慢。2007年12月，工作重新啟動，該項目於2008年1月10日受到公眾關注，當時Anne
van Kesteren在部落格中提到了該項目\[3\]。

在Acid3推出時，沒有任何一款軟體能通過測試\[4\]\[5\]。及至推出後不到一個月，[Presto](../Page/Presto.md "wikilink")\[6\]\[7\]和[WebKit](../Page/WebKit.md "wikilink")\[8\]\[9\]引擎的測試版本已成功取得100分。

2008年4月1日，[Opera曾聲稱其測試版的Acid](../Page/Opera电脑浏览器.md "wikilink")3分數達到106分，不過後來證實是[愚人節的玩笑](../Page/愚人節.md "wikilink")\[10\]。

2011年9月17日，伊恩·希克森與[哈肯·維姆·萊](../Page/哈肯·維姆·萊.md "wikilink")（Opera軟體公司）宣布針對Acid3測試作了調整和變化，這將讓更多的瀏覽器可以通過測試\[11\]。

Acid3測試調整之後，Firefox 4和Internet Explorer 9拿到了100分，然而Internet Explorer
9的測試結果與真正滿分的效能和外觀有些差異，直到Internet Explorer 10才正式通過測試\[12\]。

## 分數一覽

<table>
<thead>
<tr class="header">
<th><p>nowrap|排版引擎</p></th>
<th><p>代表浏览器或显示软件</p></th>
<th><p>最新穩定版截圖</p></th>
<th><p>註解</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Trident_(排版引擎).md" title="wikilink">Trident</a><br />
<a href="../Page/EdgeHTML.md" title="wikilink">EdgeHTML</a></p></td>
<td><p><a href="../Page/Internet_Explorer.md" title="wikilink">Internet Explorer</a>（Trident）<br />
<a href="../Page/Microsoft_Edge.md" title="wikilink">Microsoft Edge</a>（EdgeHTML）</p></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Ie10_acid3.jpg" title="fig:Ie10_acid3.jpg">Ie10_acid3.jpg</a><br />
IE 10.0</p>
</center></td>
<td><p>在IE9時的Acid 3測試，雖為100/100分，但其所顯示的圖像仍與正式結果有所差異（例如上方的Acid 3字樣缺少陰影）。</p>
<p>IE 10才完全正確呈現通過測試。</p>
<p>Edge 用于测试的版本为 38.14393.0.0。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/KHTML.md" title="wikilink">KHTML</a></p></td>
<td></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Konqueror_4.10.2.png" title="fig:Konqueror_4.10.2.png">Konqueror_4.10.2.png</a><br />
Konqueror 4.10.2[13]</p>
</center></td>
<td><p>引擎為<a href="../Page/Konqueror.md" title="wikilink">Konqueror</a>（内置于<a href="../Page/Linux.md" title="wikilink">Linux的</a><a href="../Page/KDE.md" title="wikilink">KDE</a>）所使用。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Gecko.md" title="wikilink">Gecko</a></p></td>
<td><p><a href="../Page/Mozilla_Firefox.md" title="wikilink">Mozilla Firefox</a></p></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Acid3_test_on_Firefox_64.0.png" title="fig:Acid3_test_on_Firefox_64.0.png">Acid3_test_on_Firefox_64.0.png</a><br />
Firefox 64.0</p>
</center></td>
<td><p>引擎為<a href="../Page/Mozilla_Firefox.md" title="wikilink">Mozilla Firefox所使用</a>。在Firefox 7時，曾經獲得100/100的成績，但隨著Firefox的更新，Acid3測試結果也漸漸變差。[14]使用相同引擎的行動版<a href="../Page/Firefox_for_Mobile.md" title="wikilink">Firefox for Mobile在</a><a href="../Page/Windows_Mobile.md" title="wikilink">Windows Mobile平台上的得分是</a>72分。（<a href="../Page/Firefox_for_Mobile.md" title="wikilink">Firefox for Mobile的</a><a href="../Page/Windows_Mobile.md" title="wikilink">Windows Mobile自</a>2010年2月即不再更新。）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/WebKit.md" title="wikilink">WebKit</a></p></td>
<td><p><a href="../Page/Safari.md" title="wikilink">Safari</a></p></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:safari_4_pass_acid3.PNG" title="fig:safari_4_pass_acid3.PNG">safari_4_pass_acid3.PNG</a><br />
Safari 4.0 (530.17)</p>
</center></td>
<td><p>引擎為<a href="../Page/Safari.md" title="wikilink">Safari所使用</a>，在2008年3月26日推出的r31342公開測試版本已取得100分，但仍有一些技術問題有待解決。這些問題在2008年9月25日完全解決。<ref name="url"> {{cite web</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Presto.md" title="wikilink">Presto</a></p></td>
<td><p><a href="../Page/Opera电脑浏览器.md" title="wikilink">Opera</a>（12之前）</p></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Acid3-Opera-Stable.png" title="fig:Acid3-Opera-Stable.png">Acid3-Opera-Stable.png</a><br />
Opera 10.50</p>
</center></td>
<td><p>引擎為<a href="../Page/Opera电脑浏览器.md" title="wikilink">Opera所使用</a>，行動版Opera Mobile9.7也能获得100分的成绩。<ref name="url2"> {{cite web</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Blink.md" title="wikilink">Blink</a></p></td>
<td><p><a href="../Page/Opera电脑浏览器.md" title="wikilink">Opera</a>（14之后）</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Opera_acid_test.png" title="fig:Opera_acid_test.png">Opera_acid_test.png</a></p></td>
<td><p>WebKit的分支，由<a href="../Page/Google.md" title="wikilink">Google和</a><a href="../Page/Opera_Software.md" title="wikilink">Opera Software开发</a>，作为<a href="../Page/Chromium.md" title="wikilink">Chromium计划的一部分</a>。Chrome 28现最新版本仍有不完全匹配的部分，然而Opera 15则完全通过。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Chrome.md" title="wikilink">Chrome</a>（28之后）</p></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Chrome28_acid3.png" title="fig:Chrome28_acid3.png">Chrome28_acid3.png</a><br />
Chrome 28.0.1500.95 m</p>
</center></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

[de:Acid
(Browsertests)\#Acid3](../Page/de:Acid_\(Browsertests\)#Acid3.md "wikilink")
[sv:Acid
webbläsartester\#Acid3](../Page/sv:Acid_webbläsartester#Acid3.md "wikilink")

[Category:2008年面世](../Category/2008年面世.md "wikilink")
[Category:Acid測試](../Category/Acid測試.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.