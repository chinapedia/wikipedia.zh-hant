**諧魚科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目的其中一個](../Page/鱸形目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分布

本[科魚類分布於](../Page/科.md "wikilink")[熱帶及](../Page/熱帶.md "wikilink")[溫帶的](../Page/溫帶.md "wikilink")[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")、東[大西洋區及](../Page/大西洋.md "wikilink")[加勒比海](../Page/加勒比海.md "wikilink")。

## 深度

水深100至400公尺。

## 特徵

本[科於體呈紡錘型](../Page/科.md "wikilink")，略側扁。口斜裂，下頷突出；前上頷骨強度伸縮。體被中型櫛鱗；頭部除吻端外被細鱗。背鰭1或2枚，臀鰭基底較背鰭軟條部為短。尾鰭凹入，或分叉，上下葉如剪刀狀重疊。

## 分類

**諧魚科**其下分3個屬，如下：

### 諧魚屬(*Emmelichthys*)

  -   - [長身諧魚](../Page/長身諧魚.md "wikilink")(*Emmelichthys elongatus*)
      - [卡氏諧魚](../Page/卡氏諧魚.md "wikilink")(*Emmelichthys karnellai*)
      - [諧魚](../Page/諧魚.md "wikilink")(*Emmelichthys nitidus nitidus*)
      - [紅背諧魚](../Page/紅背諧魚.md "wikilink")(*Emmelichthys ruber*)
      - [史氏諧魚](../Page/史氏諧魚.md "wikilink")(*Emmelichthys
        struhsakeri*)：又稱史氏。

### 紅諧魚屬(*Erythrocles*)

  -   - [細紅諧魚](../Page/細紅諧魚.md "wikilink")(*Erythrocles acarina*)
      - [小頭紅諧魚](../Page/小頭紅諧魚.md "wikilink")(*Erythrocles microceps*)
      - [大西洋紅諧魚](../Page/大西洋紅諧魚.md "wikilink")(*Erythrocles monodi*)
      - [史氏紅諧魚](../Page/史氏紅諧魚.md "wikilink")(*Erythrocles schlegelii*)
      - [火花紅諧魚](../Page/火花紅諧魚.md "wikilink")(*Erythrocles
        scintillans*)：又稱夏威夷紅諧魚。
      - [紫帶紅諧魚](../Page/紫帶紅諧魚.md "wikilink")(*Erythrocles taeniatus*)

### 斜諧魚屬(*Plagiogeneion*)

  -   - [菲氏斜諧魚](../Page/菲氏斜諧魚.md "wikilink")(*Plagiogeneion fiolenti*)
      - [雙生斜諧魚](../Page/雙生斜諧魚.md "wikilink")(*Plagiogeneion geminatum*)
      - [大鱗斜諧魚](../Page/大鱗斜諧魚.md "wikilink")(*Plagiogeneion macrolepis*)
      - [玫瑰斜諧魚](../Page/玫瑰斜諧魚.md "wikilink")(*Plagiogeneion
        rubiginosum*)
      - [單棘斜諧魚](../Page/單棘斜諧魚.md "wikilink")(*Plagiogeneion unispina*)

## 生態

本[科魚種幼魚多群游於沿岸岩礁區海域](../Page/科.md "wikilink")，成魚則會移棲至較深之海域。屬肉食性，以動物性浮游生物為主食。

## 經濟利用

屬於中型食用魚，肉質細，適合煮湯或[紅燒](../Page/紅燒.md "wikilink")。

[\*](../Category/𩷓科.md "wikilink")