[Choy-sum.jpg](https://zh.wikipedia.org/wiki/File:Choy-sum.jpg "fig:Choy-sum.jpg")
**菜心**（又叫**菜薹**，英文名，拉丁學名*Brassica rapa* var. parachinensis 或 *Brassica
chinensis* var.
parachinensis），為[華人經常食用的](../Page/華人.md "wikilink")[蔬菜物種](../Page/蔬菜.md "wikilink")，特徵是為莖部綠色，有細小的菜葉，葉片為深綠色，而葉片間有黃色的小花，是一個單獨的品種。

在台灣現多稱呼**菜心**指的是非十字花科的[萵苣或是萵苣筍或是萵筍](../Page/萵苣.md "wikilink")。

由於西方人一向不太懂得分辨菜心與白菜等作物，在名稱上經常把各種菜搞混，直到因為各地對亞洲菜蔬的輸入量大增，才開始為各種菜蔬作詳細及嚴謹的分類\[1\]。

在[廣東省出產的](../Page/廣東省.md "wikilink")**遲菜心**（又稱**高腳菜心**）亦是菜心的一種，較一般菜心大棵和較甜\[2\]。因其產於冬季，產時較一般的菜心晚而得名。可清炒或跟[鯽魚一起滾湯](../Page/鯽魚.md "wikilink")\[3\]\[4\]\[5\]。

## 參考資料

## 參看

  - [芥蘭](../Page/芥蘭.md "wikilink")

[Category:蕓薹](../Category/蕓薹.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")

1.
2.
3.
4.
5.