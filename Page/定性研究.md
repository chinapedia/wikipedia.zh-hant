**定性研究**，或稱**質性研究**，是一種在[社會科學及](../Page/社會科學.md "wikilink")[教育學領域常使用的](../Page/教育學.md "wikilink")[研究方法](../Page/研究方法.md "wikilink")，通常是相對[定量研究而言](../Page/定量研究.md "wikilink")。定性研究實際上並不是指一種方法，而是許多不同研究方法的統稱，由於他們都不屬於量化研究，被歸成同一類探討。其中包含但不限於[民族誌研究](../Page/民族誌.md "wikilink")、[論述分析](../Page/論述分析.md "wikilink")、[訪談研究等](../Page/訪談研究.md "wikilink")。定性研究者的目的是更深入瞭解人類行為、及其理由。定性研究方法調查人類決策制定的理由和方法，而不只是人做出什麼決定、在何時何處做出決定而已。
因此，相對於定量研究，定性研究專注於更小但更集中的樣本，產生關於特定研究個案的資訊或知識。

## 發展歷史

直到1970年代，「定性研究」這個[詞都還被視為](../Page/詞.md "wikilink")[人類學或](../Page/人類學.md "wikilink")[社會學的](../Page/社會學.md "wikilink")[研究方法](../Page/研究方法.md "wikilink")。而1970年代至1980年代，其他學科開始採用定性研究，定性研究逐漸變成[教育研究](../Page/教育研究.md "wikilink")、[社會工作研究](../Page/社會工作.md "wikilink")、[女性研究](../Page/女性研究.md "wikilink")、[殘疾研究](../Page/殘疾.md "wikilink")、[資訊研究](../Page/資訊.md "wikilink")、[管理研究](../Page/管理.md "wikilink")、[護理服務研究](../Page/護理服務.md "wikilink")、[政治科學](../Page/政治科學.md "wikilink")、[心理學](../Page/心理學.md "wikilink")、[傳播](../Page/傳播.md "wikilink")、[人机交互](../Page/人机交互.md "wikilink")、[计算机支持的协同工作](../Page/计算机支持的协同工作.md "wikilink")、[软件及系统设计研究和其他領域的重要研究類型](../Page/软件及系统设计.md "wikilink")。在這個階段，[研究者透過定性研究來調查新](../Page/研究者.md "wikilink")[消費商品的](../Page/消費商品.md "wikilink")[產品定位或](../Page/產品定位.md "wikilink")[行銷機會](../Page/行銷機會.md "wikilink")。1980年代晚期至1990年代，企業花費在傳統媒體[廣告的開支開始減少](../Page/廣告.md "wikilink")，故必須研究和尋求更有效率進行廣告的方式。

關於定量研究與定性研究之間孰者較適當的爭論一直存在。定性研究強調，[數據分析可能產生](../Page/數據分析.md "wikilink")[可信度的問題](../Page/可信度.md "wikilink")，或者[模型不夠精確的問題](../Page/概念模型.md "wikilink")。

## 特色

### 評量量表\[1\]

  - 非比較式量表
      - 範例：性別、宗教傾向、興趣喜好
  - 逐項式量表
      - 痛苦程度量表、服務滿意程度

## 相關理論及方法

  - [紮根理論](../Page/紮根理論.md "wikilink")（Grounded Theory）
  - [現象學](../Page/現象學.md "wikilink")（Phenomenology）
  - [認識論](../Page/認識論.md "wikilink")（Epistemology）
  - [批判理論](../Page/批判理論.md "wikilink")（Critical Theory）
  - [個案研究](../Page/個案研究.md "wikilink")（Case Study）
  - [田野調查](../Page/田野調查.md "wikilink")（Fieldwork）
  - [參與觀察](../Page/參與觀察.md "wikilink")（Participant Observation）
  - [主題分析](../Page/主題分析.md "wikilink")（Thematic analysis）
  - [視覺分析](../Page/視覺分析.md "wikilink")（Visual Analysis）
  - [論述分析](../Page/論述分析.md "wikilink") （Discourse Analysis）
  - [民族誌](../Page/民族誌.md "wikilink") （Ethnography）

## 参看

  - [MAXQDA定性研究软件](../Page/MAXQDA.md "wikilink")

## 參考書目

  - Adler, P. A. & Adler, P. (1987). Membership roles in field research.
    Newbury Park, CA: Sage.
  - Boas, F. (1943). Recent anthropology. Science, 98, 311-314, 334-337.
  - DeWalt, K. M. & DeWalt, B. R. (2002). Participant observation.
    Walnut Creek, CA: AltaMira Press.
  - Giddens, A. (1990). The consequences of modernity. Stanford, CA:
    Stanford University Press.
  - Malinowski, B. (1922/1961). Argonauts of the Western Pacific. New
    York: E. P. Dutton.
  - Wolcott, H. F. (1995). The art of fieldwork. Walnut Creek, CA:
    AltaMira Press.
  - Wolcott, H. F. (1999). Ethnography: A way of seeing. Walnut Creek,
    CA: AltaMira Press.

[\*](../Category/定性研究.md "wikilink")
[Category:研究方法](../Category/研究方法.md "wikilink")
[Category:科學方法](../Category/科學方法.md "wikilink")
[Category:評價方法](../Category/評價方法.md "wikilink")
[Category:社會學方法論](../Category/社會學方法論.md "wikilink")

1.  圖解研究方法 榮泰生博士 五南出版