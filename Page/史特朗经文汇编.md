[James_Strong_theologian_-_Brady-Handy.jpg](https://zh.wikipedia.org/wiki/File:James_Strong_theologian_-_Brady-Handy.jpg "fig:James_Strong_theologian_-_Brady-Handy.jpg")

《**史特朗经文汇编**》（*Strong's Exhaustive Concordance of the
Bible*）是一部由美国圣经学者、[卫理宗信徒](../Page/卫理宗.md "wikilink")（1822年–1894年）主持编纂的英文[钦定版圣经](../Page/钦定版圣经.md "wikilink")（KJV）的经文汇编，初版于1890年。史特朗是当时[德鲁神学院](../Page/德鲁大学.md "wikilink")（*Drew
Theological
Seminary*，今[新泽西州的德鲁大学](../Page/新泽西州.md "wikilink")）的解经学[神学教授](../Page/神学.md "wikilink")。这是一本钦定版圣经中的每个词汇与原文词汇的详尽对照\[1\]。它已经成为钦定版圣经最广泛使用的经文汇编，也是一本在圣经研究者中间得到广泛使用的的工具书。

与其他所有圣经参考书的目的不同，史特朗经文汇编不是要提供圣经的内容或注释，而是提供圣经经文索引。它可以帮助读者找到出现在圣经中不同地方中提到的同一主题。

## 作者

史特朗经文汇编不是由詹姆士·史特朗个人完成的，而是由超过100名同事参与编纂。

## 内容

史特朗经文汇编的内容包括：

  - 在[旧约圣经中使用的](../Page/旧约圣经.md "wikilink")8674个[希伯来语原文词汇](../Page/希伯来语.md "wikilink")。
  - [新约圣经中使用的](../Page/新约圣经.md "wikilink")5523个[希腊语原文词汇](../Page/希腊语.md "wikilink")。

## 史特朗号码

在词典中，每个原文词汇都被编上一个号码，称为“史特朗号码”。在主要部分中，列出钦定版圣经中出现的每个词汇按照字母顺序，以及按照在圣经中出现的各章节的顺序，并附带前后经文片段。标在经文右侧的就是史特朗号码，这使得经文汇编的使用者查找出原文词汇的意义，因此显示原文词汇如何在钦定版圣经中翻译成英文词汇。

尽管史特朗经文汇编中的希腊文词汇编号为 1–5624，但是其中的 2717号和3203–3302号由于一些原因而空缺。

## 应用

史特朗经文汇编既不是[圣经译本](../Page/圣经译本.md "wikilink")，也不是翻译工具。史特朗号码不是用来代替接受古代语言正式训练者的从[希伯来语和](../Page/希伯来语.md "wikilink")[希腊语译成](../Page/希腊语.md "wikilink")[英语的专业的圣经](../Page/英语.md "wikilink")[翻译](../Page/翻译.md "wikilink")。

今天，史特朗号码的编号系统在许多研究著作中得到广泛应用，在[www.blueletterbible.org](http://www.blueletterbible.org)网站上，史特朗经文汇编，和[乔治·魏格伦](../Page/乔治·魏格伦.md "wikilink")（*George
Vicesimus
Wigram*，19世纪[普利茅斯弟兄会的圣经学者](../Page/普利茅斯弟兄会.md "wikilink")）的《英人希伯来文和迦勒底文的旧约汇编》、《英人新约希腊文汇编》（*Wigram's
Englishman's Concordances*），以及Thayer新约希腊文词典，一同配合使用\[2\]。

2007年，史特朗经文汇编仍然在出版经过修订的新版本（史特朗经文汇编21世纪版）。此外，其他作者也将史特朗经文汇编的号码用于其他圣经译本，例如“新国际版”和“美国标准本”，也常被称为“史特朗经文汇编”。许多学术性的希腊文和希伯来文词典（如Brown
Driver Briggs 希伯来语词典）也使用史特朗号码来参照。

由于史特朗经文汇编确定了希伯来语和希腊语中的原文词汇，不少未经过这些古代语言足够训练的研究者，通过史特朗号码来深入研究圣经原文的意义，对圣经做出重新解释。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [史特朗经文汇编的新旧约钦定版圣经逐字对照](http://www.htmlbible.com/sacrednamebiblecom/kjvstrongs/)
  - [几种英文译本的新旧约圣经，链接到史特朗经文汇编中的希伯来文和希腊文词汇](http://www.blueletterbible.org/index.html)
  - [在史特朗经文汇编中查找号码](http://www.blueletterbible.org/search.html#strongs)
  - [史特朗号码的字母索引](http://www.htmlbible.com/sacrednamebiblecom/kjvstrongs/STRINDEX.htm)
  - [史特朗经文汇编在线搜寻](http://christsassembly.info/online_strongs_concordance.htm)
  - [查找史特朗经文汇编中的希伯来文词汇](http://www.biblestudytools.net/Lexicons/OldTestamentHebrew/)
  - [查找史特朗经文汇编中的希腊文词汇](http://www.biblestudytools.net/Lexicons/NewTestamentGreek/)

## 参见

  - [杨格氏汇编](../Page/杨格氏汇编.md "wikilink")

[Category:圣经相关文献](../Category/圣经相关文献.md "wikilink")

1.
2.