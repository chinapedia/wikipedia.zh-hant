[缩略图](https://zh.wikipedia.org/wiki/File:台灣省西螺鎮台西客運旁租書店.jpg "fig:缩略图")

**租書店**是購入[書籍並以其](../Page/書籍.md "wikilink")[出租來獲得利潤的店家](../Page/租賃.md "wikilink")，租金通常是以一定比例的定價來訂。讀者支付租金後可直接在店內閱讀，或是帶出閱讀，在規定期限之前歸還。

一般租書店內的書籍以[漫畫](../Page/漫畫.md "wikilink")、[小說或](../Page/小說.md "wikilink")[雜誌等休閒性書刊為主](../Page/雜誌.md "wikilink")，由於休閒性書刊大多看過一兩次之後即很少再看，因此讀者為省錢便會在租書店租借。連鎖租書店的通路被認為是一筆不小的利潤，許多出版社靠出租店的訂單維生，而出租店也藉此壓低進書的成本。

除了租書外，部分店家亦利用通路之便幫客人訂書或者販售相關產品（如:書套），近來租書店也越來越多元化，還提供[VCD](../Page/VCD.md "wikilink")、[DVD-Video及](../Page/DVD-Video.md "wikilink")[藍光光碟之租借](../Page/藍光光碟.md "wikilink")，也可網路預定書籍。租書店還提供二手漫畫的買賣以及交流，過期的漫畫週刊租書店會以蒐集成一個月或是一季的方式販售，價錢通常都便宜許多。

但[網際網路普及之後](../Page/網際網路.md "wikilink")，網路上有許多合法或非法的[電子書可以在線上或](../Page/電子書.md "wikilink")[下載閱讀](../Page/下載.md "wikilink")，逐漸影響到租書店的生意。\[1\]\[2\]

## 參見

  - [Culture Convenience
    Club](../Page/Culture_Convenience_Club.md "wikilink")
  - [漫畫店](../Page/漫畫店.md "wikilink")
  - [影碟出租店](../Page/影碟出租店.md "wikilink")
  - [圖書館](../Page/圖書館.md "wikilink")

## 參考資料

[\*](../Category/書店.md "wikilink")
[Category:漫畫產業](../Category/漫畫產業.md "wikilink")
[Category:小說](../Category/小說.md "wikilink")
[Category:雜誌](../Category/雜誌.md "wikilink")
[Category:閱讀](../Category/閱讀.md "wikilink")
[Category:租賃](../Category/租賃.md "wikilink")

1.
2.