**弘前車站**（）是一位于[青森縣](../Page/青森縣.md "wikilink")[弘前市表町](../Page/弘前市.md "wikilink")1丁目1番地的[鐵路車站](../Page/鐵路車站.md "wikilink")，由[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）、[日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")（JR貨物）與[弘南鐵道所共用](../Page/弘南鐵道.md "wikilink")。2002年（平成14年）入選[東北車站百選](../Page/東北車站百選.md "wikilink")。

## 車站結構

### JR東日本

[側式月台](../Page/側式月台.md "wikilink")1面1線與[島式月台](../Page/島式月台.md "wikilink")1面2線，合計2面3線的[地面車站](../Page/地面車站.md "wikilink")。

#### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1－3</p></td>
<td><p>奧羽本線（下行）</p></td>
<td><p><a href="../Page/川部站.md" title="wikilink">川部</a>、<a href="../Page/浪岡站.md" title="wikilink">浪岡</a>、<a href="../Page/新青森站.md" title="wikilink">新青森</a>、<a href="../Page/青森站.md" title="wikilink">青森方向</a></p></td>
</tr>
<tr class="even">
<td><p>奧羽本線（上行）</p></td>
<td><p><a href="../Page/大鰐站.md" title="wikilink">大鰐溫泉</a>、、<a href="../Page/秋田站.md" title="wikilink">秋田方向</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>五能線</p></td>
<td><p><a href="../Page/五所川原站.md" title="wikilink">五所川原</a>、<a href="../Page/鰺澤站.md" title="wikilink">鰺澤</a>、<a href="../Page/深浦站.md" title="wikilink">深浦方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 弘南鐵道

[港灣式月台](../Page/港灣式月台.md "wikilink")1面2線的地面車站。

#### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1、2</p></td>
<td><p>弘南線</p></td>
<td><p>、方向</p></td>
</tr>
</tbody>
</table>

## 相鄰車站

※跨越奧羽本線與五能線運行的臨時快速「」的停靠站參見列車條目。

  - 東日本旅客鐵道

    奧羽本線

      - 特急「」、臨時快速「」「」停車站

    <!-- end list -->

      -

        快速（但是 弘前站沒有跨線運行的列車。）

          -
            [大鰐溫泉](../Page/大鰐站.md "wikilink")－**弘前**－[川部](../Page/川部站.md "wikilink")

        普通

          -
            [石川](../Page/石川站_\(JR東日本\).md "wikilink")－**弘前**－[撫牛子](../Page/撫牛子站.md "wikilink")

    五能線（至川部站為止為奧羽本線）

      -

          -
            **弘前**－撫牛子

  - 弘南鐵道

    弘南線

      -

          -
            **弘前**－

## 外部連結

  - [JR東日本
    弘前車站](http://www.jreast.co.jp/estation/station/info.aspx?StationCd=1344)

[Rosaki](../Category/日本鐵路車站_Hi.md "wikilink")
[Category:青森縣鐵路車站](../Category/青森縣鐵路車站.md "wikilink")
[Category:奧羽本線車站](../Category/奧羽本線車站.md "wikilink")
[Category:日本貨物鐵道車站](../Category/日本貨物鐵道車站.md "wikilink")
[Category:弘南線車站](../Category/弘南線車站.md "wikilink")
[Category:1894年启用的铁路车站](../Category/1894年启用的铁路车站.md "wikilink")
[Category:弘前市](../Category/弘前市.md "wikilink")