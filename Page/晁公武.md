**晁公武**，字**子止**，號**昭德先生**。南宋著名目录学家、藏书家。宋朝钜野（今山东巨野县）人。靖康末年入蜀避乱，宋高宗绍兴二年举进士第，绍兴十一年至十七年（1141～1147）任四川转运使井度的从官。自幼喜读群书，初为四川总领财赋司，办事有才干。

## 生平

出身世宦家族，七世祖晁迥官至礼部尚书\[1\]，再從父[晁補之](../Page/晁補之.md "wikilink")，从父[晁說之](../Page/晁說之.md "wikilink")，均为著名学者。[晁沖之之子](../Page/晁沖之.md "wikilink")，金人南侵，舉家避難[四川](../Page/四川.md "wikilink")，定居嘉定（今樂山），家藏书，在战乱中损失殆尽。紹興二年（1132年），登進士第\[2\]。公武後來成為四川轉運使[井度的屬官](../Page/井度.md "wikilink")，紹興十七年知恭州（今重慶），又知榮州（今四川榮縣）、合州（今重慶合川）、泸州（今四川泸州），隆兴初年（1163年），入为吏部郎中、监察御史。隆兴二年（1164年）兼枢密院检详文字，侍御史\[3\]。乾道四年（1168年），以敷文阁待制为四川安抚制置使。乾道五年（1169年），在四川重建广惠仓，赈济饥民\[4\]。累官至吏部侍郎。

晁公武曾大力協助南阳[井度寫書](../Page/井度.md "wikilink")、刻書和[校書](../Page/校書.md "wikilink")。井度好藏書，“歷二十年，所有甚富”，晚年以五十篋藏書贈送公武，連同晁公家藏，“除其重复，得二万四千五百卷有奇”，绍兴二十一年，又在知荣州任上，利用闲暇之餘，“日夕躬以[朱黄](../Page/朱黄.md "wikilink")，[雠校舛误](../Page/雠校.md "wikilink")，终篇辄撮其大旨论之”\[5\]，完成《[郡齋讀書志](../Page/郡齋讀書志.md "wikilink")》初稿，晚年定居四川嘉定府符文乡，建有“郡齋”藏書處，不斷進行修订和补充。目录学家[陈振孙稱此書](../Page/陈振孙.md "wikilink")：“其所发明，有足观者。”\[6\]。另著有《易诂训传》18卷、《尚书诂训传》46卷、《毛诗诂训传》20卷、《中庸大传》1卷、《春秋诂训传》30卷、《石经考异》1卷、《稽古后录》35卷、《通鉴评》10卷、《老子通述》2卷、《昭德堂稿》60卷、《嵩高樵唱》2卷等。

## 注釋

<div class="references-small">

<references />

</div>

[H](../Category/宋朝藏書家.md "wikilink")
[H](../Category/南宋政治人物.md "wikilink")
[H](../Category/巨野人.md "wikilink")
[Category:晁姓](../Category/晁姓.md "wikilink")

1.  《直斋书录解题》卷1“昭德易诂训传”条；《宋史》卷305《晁迥传》。
2.  明嘉靖《清丰县志》卷四
3.  《宋会要辑稿》职官71之8、77之75、78之49
4.  《宋会要辑稿》食货68之67、68
5.  《郡斋读书志》自序
6.  《直斋书录解题》卷8“晁氏读书志”条