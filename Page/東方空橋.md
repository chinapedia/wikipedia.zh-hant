**東方空橋**是一家[日本](../Page/日本.md "wikilink")[航空公司](../Page/航空公司.md "wikilink")，總部位於[日本](../Page/日本.md "wikilink")[九州](../Page/九州.md "wikilink")[長崎市](../Page/長崎市.md "wikilink")。公司主要經營區域航線，來往[長崎機場](../Page/長崎機場.md "wikilink")。

## 歷史

東方空橋於1961年10月9日成立，當時命名為**長崎航空**，經營長崎縣内的離島機場與長崎、福岡之間的定期航班。以前由縣長擔任社長一職，及後招聘民間的航空業人士擔任，並推行企業改善措施，例如如減少縣級投資比例，2001年3月1日易名為東方空橋。

同時亦引入較大型的新機種[龐巴迪](../Page/龐巴迪宇航公司.md "wikilink")[Dash
8-200](../Page/Dash_8.md "wikilink")，之後，在開設長崎 -
吉島福井，津島，宮崎，鹿兒島等航線時，使用福岡航線更換出來的小型飛機，上五島・小値賀航點被廢除或停止運作。

国际民航组织航空公司代码在長崎航空時期為**NGK**，現在為**ORC**。

2013年12月11日，東方空橋宣佈，旗下兩架客機將於2019及2020年到達壽命，2016年需決定更換的機種，但[龐巴迪](../Page/龐巴迪宇航公司.md "wikilink")[Dash
8-200經已停產](../Page/Dash_8.md "wikilink")，現時正考慮引入[ATR
42客機](../Page/ATR_42.md "wikilink")。

2016年，国土交通省成立了可持續區域航空研究小組，會議期間曾討論租用[全日空之翼航空的](../Page/全日空之翼航空.md "wikilink")[龐巴迪DHC-8-Q400](../Page/Dash_8.md "wikilink")，開辦福岡ｰ宮崎航線則明朗化。

## 機隊

[Orc_dash8_3.jpg](https://zh.wikipedia.org/wiki/File:Orc_dash8_3.jpg "fig:Orc_dash8_3.jpg")

  - 2架[龐巴迪](../Page/龐巴迪宇航公司.md "wikilink")[Dash
    8](../Page/Dash_8.md "wikilink")-200

## 外部連結

  - [官方網頁](http://www.orc-air.co.jp)

[分類:1961年成立的航空公司](../Page/分類:1961年成立的航空公司.md "wikilink")

[Category:日本航空公司](../Category/日本航空公司.md "wikilink")
[Category:長崎縣公司](../Category/長崎縣公司.md "wikilink")