[Evora10.jpg](https://zh.wikipedia.org/wiki/File:Evora10.jpg "fig:Evora10.jpg")
**人骨礼拜堂**（Capela dos
Ossos），位於[葡萄牙](../Page/葡萄牙.md "wikilink")[埃武拉市](../Page/埃武拉.md "wikilink")（Évora），建于十七世纪，属于[天主教](../Page/天主教.md "wikilink")[聖方濟天主堂](../Page/聖方濟天主堂_\(埃武拉\).md "wikilink")（Igreja
de São
Francisco）的一部分。其长度约为3×18.70[米](../Page/米_\(單位\).md "wikilink")、宽度为11米，是[巴洛克风格的](../Page/巴洛克.md "wikilink")[建筑](../Page/建筑物.md "wikilink")。

## 起源

创建者为一位半盲的修士，1511年時他为了传达“人生短暂”的[宗教意念用教堂墓地里大约五千具尸骨搭建起了这座](../Page/宗教.md "wikilink")[礼拜堂](../Page/教堂.md "wikilink")。
1703 - 1710年間，當時購得了該小堂的業主僱用了專人改建，使得個小堂的人骨裝飾擴大了許多。
在礼拜堂入口的横梁上，更刻著一条令人心寒的标语「我们尸骨在此等待你们的尸骨」（“Nós
ossos que aqui estamos pelos vossos esperamos”）。

## 外觀

[Muro_de_la_capilla_de_los_huesos,_Évora,_Portugal.jpg](https://zh.wikipedia.org/wiki/File:Muro_de_la_capilla_de_los_huesos,_Évora,_Portugal.jpg "fig:Muro_de_la_capilla_de_los_huesos,_Évora,_Portugal.jpg")被人用鐵鏈懸掛着\]\]
阴暗的礼拜堂内更令人毛骨悚然：两壁和八条厅柱完全由人骨镶成。白色的拱形天花绘有以[死亡为主题的图案并由](../Page/死亡.md "wikilink")[颅骨排列点缀](../Page/颅骨.md "wikilink")。墙壁主要由[肢骨镶嵌水泥而成](../Page/肢骨.md "wikilink")，分别有两种排法：肢骨插入墙体或横镶在墙上，也有许多颅骨不规则镶嵌其中。厅柱为四方柱体，柱面由肢骨横向垒成，柱边颅骨纵向排列。室内右壁吊有一具大人和一具男婴的[骷髅](../Page/骷髅.md "wikilink")。

## 圖片

Image:Capela dos ossos entrada.jpg|人骨礼拜堂的入口 Image:Capela dos ossos
inscricao.jpg|礼拜堂入口的警告語句（我们尸骨在此等待你们的尸骨） Image:Capilla de los huesos,
Évora.jpg|礼拜堂內部 Image:Evora14.jpg|藏骨堂

## 外部連結

  - [Capela dos Ossos](http://igrejadesaofrancisco.pt/)官方網站

[Category:葡萄牙天主教堂](../Category/葡萄牙天主教堂.md "wikilink")