**羌语**主要分布在[中国](../Page/中国.md "wikilink")[四川](../Page/四川.md "wikilink")[阿坝州的](../Page/阿坝州.md "wikilink")[汶川县](../Page/汶川县.md "wikilink")、[茂县](../Page/茂县.md "wikilink")、[理县](../Page/理县.md "wikilink")、[松潘县和](../Page/松潘县.md "wikilink")[黑水县等地](../Page/黑水县.md "wikilink")，是当地[羌族人和](../Page/羌族.md "wikilink")[藏族人操的语言](../Page/藏族.md "wikilink")。羌语属[汉藏语系的](../Page/汉藏语系.md "wikilink")[羌语支](../Page/羌语支.md "wikilink")，藏缅语北部语群，和[西夏文关系比较密切](../Page/西夏文.md "wikilink")。有两种方言。北部方言行于茂县沙坝、赤不苏区、松潘县镇江、热务区和黑水大部份地区及[北川县](../Page/北川县.md "wikilink")；南部方言行于茂县的凤仪、土门区、汶川县威州、绵虒、理县通化、薛城区。各方言内又有若干土语。北部方言无声调，南部方言有声调，土语中声调数量不一，多者达六个，但是声调承担的音位对立的负担并不大。属于比较保守的藏缅语，保留了大量的复辅音声母。羌語支語言在語音系統及語法方面有明顯的共同特點。

## 語音系統

  - 語音方面[複輔音豐富](../Page/複輔音.md "wikilink")，單輔音聲母有[小舌](../Page/小舌.md "wikilink")[塞音和](../Page/小舌塞音.md "wikilink")[擦音](../Page/小舌擦音.md "wikilink")，[塞擦音有四套](../Page/塞擦音.md "wikilink")，元音有長短、捲舌、鼻化，但很少有[鬆緊](../Page/鬆緊元音對立.md "wikilink")，韻尾大體已丟失，聲調的作用不大。

## 羌語語法

  - 語法方面，人稱代詞有格，量詞與數詞結合為數量型，但不如彝語支豐富，動詞有人稱、數、體、態、式、趨向等語法範疇，用前後綴方式表達，各語言表示相同語法意義的前後綴有明顯起源上的共同性，形容詞沒有級的範疇，結構助詞比藏語支語言豐富。
  - 詞彙方面，有較多的漢語-{A|zh-hant:借詞;zh-hans:借词}-和藏語借詞，各語言之間的同源詞一般在20%左右，最多達30%。

## 參見

  - [西夏語](../Page/西夏語.md "wikilink")

## 外部連結

  - [北羌語ISO代碼](http://www.ethnologue.com/show_language.asp?code=cng)
  - [南羌語ISO代碼](http://www.ethnologue.com/show_language.asp?code=qxs)

[Category:羌语支](../Category/羌语支.md "wikilink")
[Category:中国语言](../Category/中国语言.md "wikilink")