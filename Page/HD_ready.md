\-{H|zh-hans:支持;zh-hant:支援}-
[HD_ready.jpg](https://zh.wikipedia.org/wiki/File:HD_ready.jpg "fig:HD_ready.jpg")
**HD
ready**是[歐洲資訊通訊技術協會](../Page/歐洲資訊通訊技術協會.md "wikilink")（EICTA）在2004年1月宣佈的[標準](../Page/標準.md "wikilink")。他們提出了這標準，是為了區別出[顯示設備的](../Page/顯示設備.md "wikilink")[品質](../Page/品質.md "wikilink")，能夠處理和顯示[高清晰度電視的訊號](../Page/高清晰度電視.md "wikilink")。

事實上，那些支援HD Ready的產品，並無必要加上這標籤。HD
Ready的原意，是顯示設備能夠從HD訊號上顯示完整的影像[解像度](../Page/解像度.md "wikilink")，從而展現像素較高的影像。而大部分的HD
Ready的電視，都沒有足夠的解像度（1920x1080[像素或](../Page/像素.md "wikilink")1280x720像素，通常是1366x768非完整16:9比例象素），來顯示HD訊號的完整像素圖像。理論上，除了這些像素不足外，抽樣理論反映出，適當的重建[濾波器需要比數碼抽樣更多的像素於螢幕上](../Page/濾波器.md "wikilink")，來提供最大的銳利度；這就是為何普通清晰度的電視（尤其是[DVD](../Page/DVD.md "wikilink")），於高清晰度電視下，看起來比起最好的標準清晰度的電視質素更為良好。

[HD
compatible](../Page/HD_compatible.md "wikilink")（相容高清晰度電視訊號）這標準，亦於[英國所採用](../Page/英國.md "wikilink")；這標準表示顯示設備設有[HDMI功能](../Page/HDMI.md "wikilink")，但畫面解像度仍然停於HD
Ready的解像度。

許多[桌上型電腦以及](../Page/桌上型電腦.md "wikilink")[手提電腦的解像度](../Page/手提電腦.md "wikilink")，其實已經高於HD
Ready的水平；但是，除非它們同時接駁到其他設備，否則它們沒有HD Ready的標籤資格。

任何電腦配備1280x720或更高解像度的螢幕，已經完全能夠播放HD影片，事實上不適用於大部分電腦。

## 必備條件

貼有「HD ready」標記的顯示產品要包含以下條件：

  - 顯示器和顯示引擎

:\* 顯示器（e.g. LCD, PDP）或顯示引擎（e.g. DLP）的最低自然解析度是在寬螢幕比例中有720條物理線。

  - 視像界面

:\*顯示信號只需支援以下介面輸入高清訊號：

:\*\* 類比色差，“HD
ready”代表完全支援現今市面上類比色差高清訊號。而且高清信號應能透過普及的連接方式連接或使用配接器簡單連接到訊號

:\*
[DVI或](../Page/DVI.md "wikilink")[HDMI高清訊號輸入支援下列高清視像格式](../Page/HDMI.md "wikilink")：

:\*\* 1280x720 @ 50 and 60Hz
[逐行掃描](../Page/逐行掃描.md "wikilink")（[720p](../Page/720p.md "wikilink")）

:\*\* 1920x1080 @ 50 and 60Hz
[隔行掃描](../Page/隔行掃描.md "wikilink")（[1080i](../Page/1080i.md "wikilink")）

:\*DVI 或 HDMI
輸入支援[高頻寬數位內容保護](../Page/高頻寬數位內容保護.md "wikilink")（[HDCP](../Page/HDCP.md "wikilink")）
另外，「HD ready 1080p」，並非[Full HD](../Page/Full_HD.md "wikilink")，但與HD
Ready有差別，及畫質上的提升。它的標誌是反白的黑底白字「[HD Ready
1080p](../Page/HD_Ready_1080p.md "wikilink")」。

## 外部連結

  - [HD ready official UK website](http://www.hdready.org.uk/)
  - [EICTA's
    website](https://web.archive.org/web/20060207144453/http://www.eicta.org/press.asp?level2=24&level1=6&level0=1&docid=398)

[Category:影片和電影技術](../Category/影片和電影技術.md "wikilink")
[Category:電視技術](../Category/電視技術.md "wikilink")
[Category:高清晰度电视](../Category/高清晰度电视.md "wikilink")