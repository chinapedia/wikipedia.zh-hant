\\; \\exp\\left(-\\frac{\\left(x-\\mu\\right)^2}{2\\sigma^2} \\right)
\\\!</math> |cdf =
\(\frac12 \left(1 + \operatorname{erf} \frac{x-\mu}{\sigma\sqrt2}\right) \!\)
|mean = \(\mu\) |median =\(\mu\) |mode = \(\mu\) |variance =\(\sigma^2\)
|skewness = 0 |kurtosis = 0 |entropy
=\(\ln\left(\sigma\sqrt{2\,\pi\,e}\right)\!\) |mgf =
\(M_X(t)= \exp\left(\mu\,t+\sigma^2 \frac{t^2}{2}\right)\) |char =
\(\phi_X(t)=\exp\left(\mu\,i\,t-\frac{\sigma^2 t^2}{2}\right)\) }}
**常態分布**（）又名**高斯分布**（），是一個非常常見的[連續機率分布](../Page/概率分布.md "wikilink")。常態分布在[统计学上十分重要](../Page/统计学.md "wikilink")，經常用在[自然和](../Page/自然科学.md "wikilink")[社会科学來代表一個不明的隨機變量](../Page/社会科学.md "wikilink")。\[1\]\[2\]

若[隨機變量](../Page/隨機變量.md "wikilink")\(X\)服從一個位置參數為\(\mu\)、尺度參數為\(\sigma\)的常態分布，記為：

\[X \sim N(\mu,\sigma^2),\]\[3\]
則其[機率密度函數為](../Page/機率密度函數.md "wikilink")

\[f(x) = {1 \over \sigma\sqrt{2\pi} }\,e^{- {{(x-\mu )^2 \over 2\sigma^2}}}\]\[4\]

常態分布的[數學期望值或](../Page/數學期望.md "wikilink")[期望值](../Page/期望值.md "wikilink")\(\mu\)等於位置參數，決定了分布的位置；其[方差](../Page/方差.md "wikilink")\(\sigma^2\)的開平方或[標準差](../Page/標準差.md "wikilink")\(\sigma\)等於尺度參數，決定了分布的幅度。

常態分布的機率密度函數曲線呈鐘形，因此人們又經常稱之為**鐘形曲線**（类似于寺庙里的大钟，因此得名）。我們通常所說的**標準常態分布**是位置參數\(\mu = 0\)，尺度參數\(\sigma^2 = 1\)的常態分布\[5\]（見右圖中紅色曲線）。

## 概要

常態分布是[自然科學與](../Page/自然科學.md "wikilink")[行為科學中的定量現象的一個方便模型](../Page/行為科學.md "wikilink")。各種各樣的[心理學測試分數和](../Page/心理學.md "wikilink")[物理現象比如](../Page/物理.md "wikilink")[光子計數都被發現近似地服從常態分布](../Page/光子.md "wikilink")。儘管這些現象的根本原因經常是未知的，理論上可以證明如果把許多小作用加起來看做一個變量，那麼這個變量服從常態分布（在R.N.Bracewell的Fourier
transform and its
application中可以找到一種簡單的證明）。常態分布出現在許多區域[統計](../Page/統計.md "wikilink")：例如，[採樣分布](../Page/採樣分佈.md "wikilink")[均值是近似地常態的](../Page/均值.md "wikilink")，即使被採樣的樣本的原始群體分布並不服從常態分布。另外，常態分布[信息熵在所有的已知均值及方差的分布中最大](../Page/信息熵.md "wikilink")，這使得它作為一種[均值以及](../Page/均值.md "wikilink")[方差已知的分布的自然選擇](../Page/方差.md "wikilink")。常態分布是在統計以及許多統計測試中最廣泛應用的一類分布。在[概率論](../Page/概率論.md "wikilink")，常態分布是幾種連續以及離散分布的[極限分布](../Page/極限分佈.md "wikilink")。

### 歷史

常態分布最早是[棣莫弗在](../Page/亞伯拉罕·棣莫弗.md "wikilink")1718年著作的書籍的（），及1734年發表的一篇關於[二項分布文章中提出的](../Page/二項分佈.md "wikilink")，當二項隨機變數的位置參數n很大及形狀參數p為1/2時，則所推導出二項分布的近似分布函數就是常態分布。[拉普拉斯在](../Page/拉普拉斯.md "wikilink")1812年发表的《分析概率论》（）中對棣莫佛的結論作了擴展到二項分布的位置參數為n及形狀參數為1\>p\>0時。現在这一结论通常被稱為[棣莫佛－拉普拉斯定理](../Page/中央極限定理#棣莫佛－拉普拉斯定理.md "wikilink")。

拉普拉斯在[誤差分析試驗中使用了常態分布](../Page/誤差分析.md "wikilink")。[勒讓德於](../Page/勒讓德.md "wikilink")1805年引入[最小二乘法這一重要方法](../Page/最小二乘法.md "wikilink")**；而**[高斯則宣稱他早在](../Page/高斯.md "wikilink")1794年就使用了該方法，並通過假設誤差服從常態分布給出了嚴格的證明。

「鐘形曲線」這個名字可以追溯到[Jouffret他在](../Page/Jouffret.md "wikilink")1872年首次提出這個術語"鐘形曲面"，用來指代[二元常態分布](../Page/多元常態分佈.md "wikilink")（[bivariate
normal](../Page/multivariate_normal_distribution.md "wikilink")）。正态分布這個名字還被[Charles
S. Peirce](../Page/Charles_S._Peirce.md "wikilink")、[Francis
Galton](../Page/Francis_Galton.md "wikilink")、[Wilhelm
Lexis在](../Page/Wilhelm_Lexis.md "wikilink")1875分别獨立地使用。這個術語是不幸的，因為它反映和鼓勵了一種謬誤，即很多概率分布都是常態的。（請參考下面的「實例」）

這個分布被稱為「常態」或者「高斯」正好是[Stigler名字由來法則的一個例子](../Page/Stigler名字由來法則.md "wikilink")，這個法則說「沒有科學發現是以它最初的發現者命名的」。

## 正态分布的定義

有幾種不同的方法用來說明一個隨機變量。最直觀的方法是[概率密度函數](../Page/概率密度函數.md "wikilink")，這種方法能夠表示隨機變量每個取值有多大的可能性。[累積分布函數是一種概率上更加清楚的方法](../Page/累積分佈函數.md "wikilink")，請看下邊的例子。還有一些其他的等價方法，例如cumulant、[特徵函數](../Page/特徵函數.md "wikilink")、[動差生成函數以及cumulant](../Page/動差生成函數.md "wikilink")-[生成函數](../Page/生成函數.md "wikilink")。這些方法中有一些對於理論工作非常有用，但是不夠直觀。請參考關於[概率分布的討論](../Page/概率分佈.md "wikilink")。

### 概率密度函數

[Normal_Distribution_PDF.svg](https://zh.wikipedia.org/wiki/File:Normal_Distribution_PDF.svg "fig:Normal_Distribution_PDF.svg")

**常態分布**的[概率密度函數均值為](../Page/概率密度函數.md "wikilink")\(\mu\)
[方差為](../Page/方差.md "wikilink")\(\sigma^2\)
(或[標準差](../Page/標準差.md "wikilink")\(\sigma\))是[高斯函數的一個實例](../Page/高斯函數.md "wikilink")：

\[f(x;\mu,\sigma)
=
\frac{1}{\sigma\sqrt{2\pi}} \, \exp \left( -\frac{(x- \mu)^2}{2\sigma^2} \right)\]。

(*請看[指數函數以及](../Page/指數函數.md "wikilink")\(\pi\).*)

如果一個[隨機變量](../Page/隨機變量.md "wikilink")\(X\)服從這個分布，我們寫作 \(X\) \~
\(N(\mu, \sigma^2)\).
如果\(\mu = 0\)並且\(\sigma = 1\)，這個分布被稱為**標準正态分布**，這個分布能夠簡化為

\[f(x) = \frac{1}{\sqrt{2\pi}} \, \exp\left(-\frac{x^2}{2} \right)\]。

右邊是給出了不同參數的正态分布的函數圖。

正态分布中一些值得注意的量：

  - 密度函數關於平均值對稱
  - 平均值與它的[眾數](../Page/眾數_\(數學\).md "wikilink")（statistical
    mode）以及[中位數](../Page/中位數.md "wikilink")（median）同一數值。
  - 函數曲線下68.268949%的面積在平均數左右的一個[標準差範圍內](../Page/標準差.md "wikilink")。
  - 95.449974%的面積在平均數左右兩個標準差\(2 \sigma\)的範圍內。
  - 99.730020%的面積在平均數左右三個標準差\(3 \sigma\)的範圍內。
  - 99.993666%的面積在平均數左右四個標準差\(4 \sigma\)的範圍內。
  - 函數曲線的[拐點](../Page/拐點.md "wikilink")（inflection
    point）為離平均數一個標準差距離的位置。

### 累積分布函數

[Normal_Distribution_CDF.svg](https://zh.wikipedia.org/wiki/File:Normal_Distribution_CDF.svg "fig:Normal_Distribution_CDF.svg")

[累積分布函數是指隨機變數](../Page/累積分佈函數.md "wikilink")\(X\)小於或等於\(x\)的機率，用機率密度函數表示為

\[F(x;\mu,\sigma)
=
\frac{1}{\sigma\sqrt{2\pi}}
\int_{-\infty}^x
 \exp
 \left( -\frac{(t - \mu)^2}{2\sigma^2}
\ \right)\, dt.\]

常態分布的累積分布函数能够由一個叫做[误差函数的](../Page/误差函数.md "wikilink")[特殊函数表示](../Page/特殊函数.md "wikilink")：

\[\Phi(z)=
\frac12 \left[1 + \operatorname{erf}\left(\frac{z-\mu}{\sigma\sqrt2}\right)\right] .\]

**標準常態分布**的累積分布函數習慣上記為\(\Phi\)，它僅僅**是指\(\mu=0\)，\(\sigma=1\)時**的值，

\[\Phi(x)
=F(x;0,1)=
\frac{1}{\sqrt{2\pi}}
\int_{-\infty}^x
\exp\left(-\frac{t^2}{2}\right)
\, dt.\]

將一般常態分布用[誤差函數表示的公式简化](../Page/誤差函數.md "wikilink")，可得：

\[\Phi(z)
=
\frac{1}{2} \left[ 1 + \operatorname{erf} \left( \frac{z}{\sqrt{2}} \right) \right]
.\]

它的[反函數被稱為反誤差函數](../Page/反函數.md "wikilink")，為：

\[\Phi^{-1}(p)
=
\sqrt2
\;
\operatorname{erf}^{-1} \left(2p - 1 \right)
.\]

該分位數函數有時也被稱為[probit函數](../Page/probit.md "wikilink")。[probit函數已被證明沒有初等原函数](../Page/probit.md "wikilink")。

**常態分布的[分布函數](../Page/分佈函數.md "wikilink")\(\Phi(x)\)沒有解析表達式**，它的值可以通過[數值積分](../Page/數值積分.md "wikilink")、[泰勒級數或者](../Page/泰勒級數.md "wikilink")[漸進序列近似得到](../Page/漸進序列.md "wikilink")。

### 生成函數

#### 矩母函数

[動差生成函數或矩生成函數或動差產生函數被定義為](../Page/動差生成函數.md "wikilink")\(\exp(tX)\)的期望值。

常態分布的動差產生函數如下：

  -
    {|

|- | \(M_X(t)\,\) || \(=
\mathrm{E}
\left(
 e^{tX}
\right)\) |- |   || \(=
\int_{-\infty}^{\infty}
 \frac
 {1}
 {\sigma \sqrt{2\pi} }
 e^{\left( -\frac{(x - \mu)^2}{2 \sigma^2} \right)}
 e^{tx}
\, dx\) |- |   || \(=
e^{
\left(
 \mu t + \frac{\sigma^2 t^2}{2}
\right)}\) |} 可以通過在指數函數內配平方得到。

#### 特徵函數

[特徵函數被定義為](../Page/特征函数_\(概率论\).md "wikilink")\(\exp (i t X)\)的[期望值](../Page/期望值.md "wikilink")，其中\(i\)是虛數單位.
對於一個常态分布來講，特徵函數是：

  -
    {|

|- | \(\phi_X(t;\mu,\sigma)\!\) || \(=
\mathrm{E}
\left[
 \exp(i t X)
\right]\) |- |   || \(=
\int_{-\infty}^{\infty}
 \frac{1}{\sigma \sqrt{2\pi}}
 \exp
 \left(- \frac{(x - \mu)^2}{2\sigma^2}
 \right)
 \exp(i t x)
\, dx\) |- |   || \(=
\exp
\left(
 i \mu t - \frac{\sigma^2 t^2}{2}
\right)
.\) |} 把矩生成函數中的\(t\)換成\(i t\)就能得到特徵函數。

## 性質

常態分布的一些性質：

1.  如果\(X \sim N(\mu, \sigma^2) \,\)且\(a\)與\(b\)是[實數](../Page/實數.md "wikilink")，那麼\(a X + b \sim N(a \mu + b, (a \sigma)^2)\)
    (參見[期望值和](../Page/期望值.md "wikilink")[方差](../Page/方差.md "wikilink")).
2.  如果\(X \sim N(\mu_X, \sigma^2_X)\)與\(Y \sim N(\mu_Y, \sigma^2_Y)\)是[統計獨立的常態](../Page/統計獨立.md "wikilink")[隨機變量](../Page/隨機變量.md "wikilink")，那麼：
      - 它們的和也滿足常態分布\(U = X + Y \sim N(\mu_X + \mu_Y, \sigma^2_X + \sigma^2_Y)\)
        ().
      - 它們的差也滿足常態分布\(V = X - Y \sim N(\mu_X - \mu_Y, \sigma^2_X + \sigma^2_Y)\).
      - \(U\)與\(V\)兩者是相互獨立的。（要求X与Y的方差相等）
3.  如果\(X \sim N(0, \sigma^2_X)\)和\(Y \sim N(0, \sigma^2_Y)\)是獨立常態隨機變量，那麼：
      - 它們的積\(X Y\)服從機率密度函數為\(p\)的分布
        \[p(z) = \frac{1}{\pi\,\sigma_X\,\sigma_Y} \; K_0\left(\frac{|z|}{\sigma_X\,\sigma_Y}\right),\]其中\(K_0\)是修正貝塞爾函數（modified
        Bessel function）
      - 它們的比符合[柯西分布](../Page/柯西分佈.md "wikilink")，滿足\(X/Y \sim \mathrm{Cauchy}(0, \sigma_X/\sigma_Y)\).
4.  如果\(X_1, \cdots, X_n\)為獨立標準常態隨機變量，那麼\(X_1^2 + \cdots + X_n^2\)服從自由度為*n*的[卡方分布](../Page/卡方分佈.md "wikilink")。

### 標準化常態隨機變量

### 動差或矩（）

一些常態分布的一階動差如下：

| 階數 | 原點矩                                       | 中心矩            | 累積量          |
| -- | ----------------------------------------- | -------------- | ------------ |
| 0  | 1                                         | 0              |              |
| 1  | \(\mu\)                                   | 0              | \(\mu\)      |
| 2  | \(\mu^2 + \sigma^2\)                      | \(\sigma^2\)   | \(\sigma^2\) |
| 3  | \(\mu^3 + 3\mu\sigma^2\)                  | 0              | 0            |
| 4  | \(\mu^4 + 6 \mu^2 \sigma^2 + 3 \sigma^4\) | \(3 \sigma^4\) | 0            |

標準常態的所有二階以上的[累積量為零](../Page/累積量.md "wikilink")。

### 生成常態隨機變量

### 中心極限定理

[Normal_approximation_to_binomial.svg](https://zh.wikipedia.org/wiki/File:Normal_approximation_to_binomial.svg "fig:Normal_approximation_to_binomial.svg")的概率質量函數。\]\]

常態分布有一個非常重要的性質：在特定條件下，大量**[統計獨立的隨機變量的平均值的分布趨於正态分布](../Page/統計獨立.md "wikilink")，這就是[中心極限定理](../Page/中心極限定理.md "wikilink")**。中心極限定理的重要意義在於，根據這一定理的結論，其他概率分布可以用正态分布作為近似。

  - **參數為\(n\)和\(p\)的[二項分布](../Page/二項分佈.md "wikilink")，在\(n\)相當大而且\(p\)接近0.5時近似於正态分布**（有的參考書建議僅在\(n p\)與\(n(1 - p)\)至少為5時才能使用這一近似）。

近似正态分布平均數為\(\mu = n p\)且方差為\(\sigma^2 = n p (1 - p)\).

  - **一[泊松分布帶有參數](../Page/泊松分佈.md "wikilink")\(\lambda\)當取樣樣本數很大時將近似正态分布\(\lambda\)**.

近似正态分布平均數為\(\mu = \lambda\)且方差為\(\sigma^2 = \lambda\).

這些近似值是否完全充分正確取決於使用者的使用需求

### 無限可分性

正态分布是[無限可分的概率分布](../Page/無限可分.md "wikilink")。

### 穩定性

正态分布是嚴格[穩定的概率分布](../Page/穩定.md "wikilink")。

### 標準偏差

[Standard_deviation_diagram.svg](https://zh.wikipedia.org/wiki/File:Standard_deviation_diagram.svg "fig:Standard_deviation_diagram.svg")中，此範圍所佔比率為全部數值之**68%**，根據常態分布，兩個標準差之內的比率合起來為**95%**；三個標準差之內的比率合起來為**99%**。\]\]

在實際應用上，常考慮一組數據具有近似於[常態分布的機率分布](../Page/常態分佈.md "wikilink")。若其假設正確，則約**68.3%**數值分布在距離平均值有1個標準差之內的範圍，約**95.4%**數值分布在距離平均值有2個標準差之內的範圍，以及約**99.7%**數值分布在距離平均值有3個標準差之內的範圍。稱為「**[68-95-99.7法則](../Page/68–95–99.7原則.md "wikilink")**」或「**經驗法則**」。

<table>
<thead>
<tr class="header">
<th><p>數字比率<br />
標準差值</p></th>
<th><p>機率</p></th>
<th><p>包含之外比例</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>百分比</p></td>
<td><p>百分比</p></td>
<td><p>比例</p></td>
</tr>
<tr class="even">
<td><p>0.318 639<em>σ</em></p></td>
<td><p>25%</p></td>
<td><p>75%</p></td>
</tr>
<tr class="odd">
<td><p>0.318 639<em>σ</em></p></td>
<td><p>25%</p></td>
<td><p>75%</p></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>68%</p></td>
<td><p>32%</p></td>
</tr>
<tr class="even">
<td><p>1<em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>80%</p></td>
<td><p>20%</p></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>90%</p></td>
<td><p>10%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>95%</p></td>
<td><p>5%</p></td>
</tr>
<tr class="even">
<td><p>2<em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>99%</p></td>
<td><p>1%</p></td>
</tr>
<tr class="even">
<td><p>3<em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>99.9%</p></td>
<td><p>0.1%</p></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>99.99%</p></td>
<td><p>0.01%</p></td>
</tr>
<tr class="odd">
<td><p>4<em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>99.999%</p></td>
<td><p>0.001%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p>5<em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/六標準差#西格玛等级.md" title="wikilink">{{val</a></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="even">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="odd">
<td><p><em>σ</em></p></td>
<td><p>%</p></td>
<td><p>%</p></td>
</tr>
<tr class="even">
<td><p>7<em>σ</em></p></td>
<td></td>
<td><p>%</p></td>
</tr>
</tbody>
</table>

## 相關分布

  - \(R \sim \mathrm{Rayleigh}(\sigma)\)是[瑞利分布](../Page/瑞利分布.md "wikilink")，如果\(R = \sqrt{X^2 + Y^2}\)，这里\(X \sim N(0, \sigma^2)\)和\(Y \sim N(0, \sigma^2)\)是两个独立正态分布。
  - \(Y \sim \chi_{\nu}^2\)是[卡方分布具有](../Page/卡方分布.md "wikilink")\(\nu\)[自由度](../Page/自由度.md "wikilink")，如果\(Y = \sum_{k=1}^{\nu} X_k^2\)这里\(X_k \sim N(0,1)\)其中\(k=1,\dots,\nu\)是独立的。
  - \(Y \sim \mathrm{Cauchy}(\mu = 0, \theta = 1)\)是[柯西分布](../Page/柯西分布.md "wikilink")，如果\(Y = X_1/X_2\)，其中\(X_1 \sim N(0,1)\)并且\(X_2 \sim N(0,1)\)是两个独立的正态分布。
  - \(Y \sim \mbox{Log-N}(\mu, \sigma^2)\)是[对数正态分布如果](../Page/对数正态分布.md "wikilink")\(Y = e^X\)并且\(X \sim N(\mu, \sigma^2)\).
  - 与[Lévy skew
    alpha-stable分布相关](../Page/稳定分布.md "wikilink")：如果\(X\sim \textrm{Levy-S}\alpha\textrm{S}(2,\beta,\sigma/\sqrt{2},\mu)\)因而\(X \sim N(\mu,\sigma^2)\).

## 參量估計

### 參數的極大似然估計

#### 概念一般化

[多元正态分布的](../Page/多元正态分布.md "wikilink")[協方差矩陣的估計的推導是比較難於理解的](../Page/協方差矩陣.md "wikilink")。它需要瞭解[譜原理](../Page/譜原理.md "wikilink")（spectral
theorem）以及為什麼把一個[標量看做一個](../Page/標量.md "wikilink")1×1[矩阵](../Page/矩阵.md "wikilink")(matrix)的迹(trace)而不僅僅是一個標量更合理的原因。請參考[協方差矩陣的估計](../Page/協方差矩陣的估計.md "wikilink")（estimation
of covariance matrices）.

### 參數的矩估計

## 常見實例

### 光子計數

### 計量誤差

#### 飲料裝填量不足與超量的機率

某飲料公司裝瓶流程嚴謹，每罐飲料裝填量符合平均600毫升，標準差3毫升的常態分配法則。隨機選取一罐，求（1）容量超過605毫升的機率；（2）容量小於590毫升的機率。

容量超過605毫升的機率 = p ( X \> 605)= p ( ((X-μ) /σ) \> ( (605 – 600) / 3) )= p
( Z \> 5/3) = p( Z \> 1.67) = 1 - 0.9525 = 0.0475

容量小於590毫升的機率 = p (X \< 590) = p ( ((X-μ) /σ) \< ( (590 – 600) / 3) )= p
( Z \< -10/3) = p( Z \< -3.33) = 0.0004

[6-標準差](../Page/六標準差.md "wikilink")(6-sigma或6-σ)的品質管制標準

6-標準差(6-sigma或6-σ)，是製造業流行的品質管制標準。在這個標準之下，一個標準常態分配的變數值出現在正負三個標準差之外，只有2\*
0.0013= 0.0026 (p (Z \< -3) = 0.0013以及p(Z \> 3) =
0.0013)。也就是說，這種品質管制標準的產品不良率只有萬分之二十六。假設例中的飲料公司裝瓶流程採用這個標準，而每罐飲料裝填量符合平均600毫升，標準差3毫升的常態分配。那么預期裝填容量的範圍應該多少？

6-標準差的範圍 = p ( -3 \< Z \< 3)= p ( - 3 \< (X-μ) /σ \< 3) = p ( -3 \< (X-
600) / 3 \< 3)= p ( -9 \< X – 600 \< 9) = p (591 \< X \< 609)
因此，預期裝填容量應該介於591至609毫升之間。

### 生物標本的物理特性

### 金融變量

### 壽命

### 測試和智力分布

#### 計算學生智商高低的機率

假設某校入學新生的智力測驗平均分數與标准差分別為100與12。那麼隨機抽取50個學生，他們智力測驗平均分數大於105的機率？小於90的機率？

本例沒有常態分配的假設，還好中央極限定理提供一個可行解，那就是當隨機樣本長度超過30，樣本平均數\(\bar{x}\)近似於一個常態變數，

因此標準常態變數\(Z=\frac{\bar{X}-\mu}{\sigma/\sqrt{n}}\)。

平均分數大於105的機率
\(P(Z>\frac{105-100}{12/\sqrt{50}}) =P(Z>5/1.7) =P(Z>2.94) =0.0016\)

平均分數小於90的機率 \(P(Z<\frac{90-100}{12/\sqrt{50}}) =P(Z<-5.88) =0.0000\)

## 计算统计应用

### 生成正态分布随机变量

在计算机模拟中，经常需要生成正态分布的数值。最基本的一个方法是使用标准的正态累积分布函数的反函数。除此之外还有其他更加高效的方法，Box-Muller变换就是其中之一。另一个更加快捷的方法是ziggurat算法。下面将介绍这两种方法。一个简单可行的并且容易编程的方法是：求12个在（0,1）上均匀分布的和，然后减6(12的一半)。这种方法可以用在很多应用中。这12个数的和是Irwin-Hall分布；选择一个方差12。这个随即推导的结果限制在（-6,6）之间，并且密度为12，是用11次多项式估计正态分布。

Box-Muller方法是以两组独立的随机数U和V，这两组数在(0,1\]上均匀分布，用U和V生成两组独立的标准常态分布随机变量X和Y:

\[X = \sqrt{- 2 \ln U} \, \cos(2 \pi V) ,\]

\[Y = \sqrt{- 2 \ln U} \, \sin(2 \pi V)\]。
这个方程的提出是因为二自由度的[卡方分布](../Page/卡方分布.md "wikilink")（见性质4）很容易由指数随机变量（方程中的lnU）生成。因而通过随机变量V可以选择一个均匀环绕圆圈的角度，用指数分布选择半径然后变换成（正态分布的）x,y坐标。

## 参考文献

  - John Aldrich. [Earliest Uses of Symbols in Probability and
    Statistics](https://web.archive.org/web/20000610213020/http://members.aol.com/jeff570/stat.html).網上材料，2006年6月3日存在.(*See
    "Symbols associated with the Normal Distribution".*)
  - [Abraham de Moivre](../Page/Abraham_de_Moivre.md "wikilink")
    (1738年). *[The Doctrine of
    Chances](../Page/The_Doctrine_of_Chances.md "wikilink")*.
  - [Stephen Jay Gould](../Page/Stephen_Jay_Gould.md "wikilink")
    (1981年). *[The Mismeasure of
    Man](../Page/The_Mismeasure_of_Man.md "wikilink")*. First edition.
    W. W. Norton. ISBN 978-0-393-01489-1.
  - [R. J. Herrnstein](../Page/Richard_Herrnstein.md "wikilink") and
    [Charles Murray](../Page/Charles_Murray.md "wikilink") (1994年).
    *[The Bell Curve](../Page/The_Bell_Curve.md "wikilink"):
    Intelligence and Class Structure in American Life*. [Free
    Press](../Page/Free_Press.md "wikilink"). ISBN 978-0-02-914673-6.
  - [Pierre-Simon Laplace](../Page/Pierre-Simon_Laplace.md "wikilink")
    (1812年). *[Analytical Theory of
    Probabilities](../Page/Analytical_Theory_of_Probabilities.md "wikilink")*.
  - Jeff Miller, John Aldrich, et al. [Earliest Known Uses of Some of
    the Words of
    Mathematics](https://web.archive.org/web/19990117033417/http://members.aol.com/jeff570/mathword.html).
    In particular, the entries for ["bell-shaped and bell
    curve"](https://web.archive.org/web/19991001182725/http://members.aol.com/jeff570/b.html),
    ["normal"
    (distribution)](https://web.archive.org/web/19991003084940/http://members.aol.com/jeff570/n.html),
    ["Gaussian"](https://web.archive.org/web/19990508225359/http://members.aol.com/jeff570/g.html),
    and ["Error, law of error, theory of errors,
    etc."](https://web.archive.org/web/19990508224238/http://members.aol.com/jeff570/e.html).網上材料，2006年6月3日存在
  - S. M. Stigler (1999年). *Statistics on the Table*, chapter 22.
    Harvard University Press. (*History of the term "normal
    distribution".*)
  - [Eric W. Weisstein](../Page/Eric_W._Weisstein.md "wikilink") et al.
    [Normal
    Distribution](http://mathworld.wolfram.com/NormalDistribution.html)
    at [MathWorld](../Page/MathWorld.md "wikilink").網上材料，2006年6月3日存在。
  - Marvin Zelen and Norman C. Severo (1964年). Probability Functions.
    Chapter 26 of *[Handbook of Mathematical Functions with Formulas,
    Graphs, and Mathematical
    Tables](../Page/Abramowitz_and_Stegun.md "wikilink")*, ed, by
    [Milton Abramowitz](../Page/Milton_Abramowitz.md "wikilink") and
    [Irene A. Stegun](../Page/Irene_A._Stegun.md "wikilink"). [National
    Bureau of
    Standards](../Page/National_Bureau_of_Standards.md "wikilink").

## 外部链接

  - [Interactive Distribution Modeler (incl. Normal
    Distribution)](http://socr.stat.ucla.edu/htmls/SOCR_Distributions.html).
  - [basic tools for sixsigma](http://www.sixsigmafirst.com/proba.htm)
  - [PlanetMath: normal random
    variable](http://planetmath.org/encyclopedia/NormalRandomVariable.html)
  - [GNU Scientific Library – Reference Manual – The Gaussian
    Distribution](http://www.gnu.org/software/gsl/manual/gsl-ref_19.html#SEC288)
  - [Distribution
    Calculator](http://www.vias.org/simulations/simusoft_distcalc.html)
    – Calculates probabilities and critical values for normal,
    *[t](../Page/Student's_t-distribution.md "wikilink")*,
    [chi-square](../Page/chi-square_distribution.md "wikilink") and
    [*F*-distribution](../Page/F-distribution.md "wikilink").
  - [Inverse Cumulative Standard Normal Distribution
    Function](https://web.archive.org/web/20090321025914/http://home.online.no/~pjacklam/notes/invnorm/impl/misra/normsinv.html)
  - [Is normal distribution due to Karl Gauss? Euler, his family of
    gamma functions, and place in history of
    statistics](https://web.archive.org/web/20060210125807/http://www.visualstatistics.net/Statistics/Euler/Euler.htm)
  - [Maxwell demons: Simulating probability distributions with functions
    of propositional
    calculus](https://web.archive.org/web/20060405193854/http://www.visualstatistics.net/Statistics/Maxwell%20Demons/Maxwell%20Demons.htm)
  - [Normal distribution table](https://www.ztable.net)
  - [The Doctrine of
    Chance](http://www.mathpages.com/home/kmath642/kmath642.htm) at
    MathPages.
  - [正态分布的前世今生(上)](http://www.flickering.cn/%E6%95%B0%E5%AD%A6%E4%B9%8B%E7%BE%8E/2014/06/%E7%81%AB%E5%85%89%E6%91%87%E6%9B%B3%E6%AD%A3%E6%80%81%E5%88%86%E5%B8%83%E7%9A%84%E5%89%8D%E4%B8%96%E4%BB%8A%E7%94%9F%E4%B8%8A/)
  - [正态分布的前世今生(下)](http://www.flickering.cn/%E6%95%B0%E5%AD%A6%E4%B9%8B%E7%BE%8E/2014/06/%E7%81%AB%E5%85%89%E6%91%87%E6%9B%B3%E6%AD%A3%E6%80%81%E5%88%86%E5%B8%83%E7%9A%84%E5%89%8D%E4%B8%96%E4%BB%8A%E7%94%9F%E4%B8%8B/)
  - [在线计算器
    正态分布](http://www.elektro-energetika.cz/calculations/no.php?language=zh)

## 參見

  - [中心極限定理](../Page/中心極限定理.md "wikilink")
  - [概率論](../Page/概率論.md "wikilink")
  - [伽玛分布](../Page/伽玛分布.md "wikilink")

{{-}}

[Category:连续分布](../Category/连续分布.md "wikilink")
[Category:共轭先验分布](../Category/共轭先验分布.md "wikilink")
[Category:带共轭先验值的分布](../Category/带共轭先验值的分布.md "wikilink")
[正态分布](../Category/正态分布.md "wikilink")
[Category:指数族分布](../Category/指数族分布.md "wikilink")
[Category:稳定分布](../Category/稳定分布.md "wikilink")
[Category:概率分布](../Category/概率分布.md "wikilink")

1.  [*Normal
    Distribution*](http://www.encyclopedia.com/topic/Normal_Distribution.aspx#3),
    Gale Encyclopedia of Psychology

2.

3.

4.
5.