[Vector_norms.png](https://zh.wikipedia.org/wiki/File:Vector_norms.png "fig:Vector_norms.png")\]\]
**範數**（），是具有“长度”概念的函數。在[線性代數](../Page/線性代數.md "wikilink")、[泛函分析及相關的數學領域](../Page/泛函分析.md "wikilink")，是一個[函數](../Page/函數.md "wikilink")，其為[向量空間內的所有](../Page/向量空間.md "wikilink")[向量賦予非零的正](../Page/向量.md "wikilink")**長度**或**大小**。另一方面，**半範數**（）可以為非零的[向量賦予零長度](../Page/向量.md "wikilink")。

舉一個簡單的例子，一個二維度的歐氏幾何空間\(\R^2\)就有歐氏範數。在這個[向量空間的元素](../Page/向量空間.md "wikilink")（譬如：(3,7)）常常在[笛卡兒座標系統被畫成一個從原點出發的箭號](../Page/直角坐标系.md "wikilink")。每一個[向量的歐氏範數就是箭號的長度](../Page/向量.md "wikilink")。

擁有範數的[向量空間就是](../Page/向量空間.md "wikilink")[賦範向量空間](../Page/賦範向量空間.md "wikilink")。同樣，擁有半範數的[向量空間就是賦半範向量空間](../Page/向量空間.md "wikilink")。

## 定義

假設*V*是域*F*上的[向量空間](../Page/向量空間.md "wikilink")；*V*的**半範數**是一個函數\(p:V\to\mathbb{R}; x\mapsto{}p(x)\)，满足：

\(\forall a \in F,\forall u,v \in V\),

1.  \(p(v) \ge 0\)（具有半正定性）
2.  \(p(a v) = |a| p(v)\)（具有绝对一次齐次性）
3.  \(p(u + v) \le p(u) + p(v)\)
    （满足[三角不等式](../Page/三角不等式.md "wikilink")）

**範數**是一個**半範數**加上額外性质：

  -
    4\.
    \(p(v)=0\),当且仅当\(v\)是[零向量](../Page/零向量.md "wikilink")（[正定性](../Page/正定性.md "wikilink")）

如果拓撲向量空間的[拓撲可以被範數導出](../Page/拓撲.md "wikilink")，這個[拓撲向量空間被稱為](../Page/拓撲向量空間.md "wikilink")[賦範向量空間](../Page/賦範向量空間.md "wikilink")。

## 例子

  - 所有范数都是半范数。
  - *平凡半范数*,即\(p(x) = 0, \forall x \in V\)。
  - [绝对值是](../Page/绝对值.md "wikilink")[实数集上的一个范数](../Page/实数.md "wikilink")。
  - 对向量空间上的[线性型](../Page/线性映射.md "wikilink")*f*可定义一个半范数：\(\boldsymbol x \to |f(\boldsymbol x)|\)。

### 绝对值范数

[绝对值](../Page/绝对值.md "wikilink")

\[\|\boldsymbol{x}\|=|x|\] 是在由实数或虚数构成的一维向量空间中的范数。

绝对值范数是[曼哈顿范数的特殊形式](../Page/曼哈頓距離.md "wikilink")。

### 欧几里德范数

在*n*维[欧几里德空间](../Page/欧几里德空间.md "wikilink")\(\mathbb R ^n\)上，向量\(\boldsymbol x = (x_1,x_2,\,\ldots\,,x_n)^{\mathrm T}\)的最符合直觉的长度由以下公式给出

\[\|\boldsymbol{x}\|_2 := \sqrt{x_1^2 + \cdots + x_n^2}.\]
根据[勾股定理](../Page/勾股定理.md "wikilink")，它给出了从原点到点\(\boldsymbol x\)之间的（通常意义下的）距离。欧几里德范数是\(\mathbb R ^n\)上最常用的范数，但正如下面举出的，\(\mathbb R ^n\)上也可以定义其他的范数。然而，以下定义的范数都定义了同一个拓扑结构，因此它们在某种意义上都是等价的。

在一个*n*维复数空间\(\mathbb C ^n\)中，最常见的范数是：

\[\|\boldsymbol{z}\| := \sqrt{|z_1|^2 + \cdots + |z_n|^2}= \sqrt{z_1 \bar z_1 + \cdots + z_n \bar z_n}.\]

以上两者又可以以向量与其自身的[内积的](../Page/内积.md "wikilink")[平方根表示](../Page/平方根.md "wikilink")：

\[\|\boldsymbol{x}\| := \sqrt{\boldsymbol{x}^* \boldsymbol{x}},\]
其中***x***是一个[列向量](../Page/列向量.md "wikilink")(\([x_1,x_2,\,\ldots\,,x_n]^{\mathrm T}\))，而\(\boldsymbol x ^ *\)表示其[共轭转置](../Page/共轭转置.md "wikilink")。

以上公式适用于任何[内积空间](../Page/内积空间.md "wikilink")，包括欧式空间和复空间。在欧几里得空间里，内积等价于点积，因此公式可以写成以下形式：

\[\|\boldsymbol{x}\| := \sqrt{\boldsymbol{x} \cdot \boldsymbol{x}}.\]

特别地，\(\mathbb R ^{n+1}\)中所有的欧几里得范数为同一个给定正实数的向量的集合是一个[*n*维球面](../Page/N维球面.md "wikilink")。

#### 复数的欧几里得范数

如果将[复平面看作](../Page/复平面.md "wikilink")[欧几里得平面](../Page/欧几里得平面.md "wikilink")\(\mathbb R ^2\)，那么[复数的欧几里得范数是其](../Page/复数.md "wikilink")[绝对值](../Page/复数_\(数学\)#绝对值、共轭与距离.md "wikilink")（又称为**模**）。这样，我们可把\(x+i\,y\)视为欧几里得平面上的一个向量，由此，这个向量的欧几里得范数即为\(\sqrt{x^2 +y^2}\)（最初由欧拉提出）。

## 參見

  - [內積](../Page/內積.md "wikilink")
  - [賦範向量空間](../Page/賦範向量空間.md "wikilink")
  - [矩陣範數](../Page/矩陣範數.md "wikilink")
  - [曼哈頓距離](../Page/曼哈頓距離.md "wikilink")

## 參考文獻

  -
  -
  -
  -
[F](../Category/線性代數.md "wikilink") [F](../Category/泛函分析.md "wikilink")
[F](../Category/度量几何.md "wikilink") [\*](../Category/范数.md "wikilink")