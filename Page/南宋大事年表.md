**南宋**（[1127年](../Page/1127年.md "wikilink")--[1279年](../Page/1279年.md "wikilink")）是[中国](../Page/中国.md "wikilink")[历史上偏安於淮水以南的](../Page/中国历史.md "wikilink")[北宋后續](../Page/北宋.md "wikilink")[朝代](../Page/朝代.md "wikilink")，与[金朝東沿淮水](../Page/金朝.md "wikilink")（今[淮河](../Page/淮河.md "wikilink")），西以[大散关为界](../Page/大散关.md "wikilink")。南宋与[西夏和金朝为并存政权](../Page/西夏.md "wikilink")。

<onlyinclude> [1127年](../Page/1127年.md "wikilink")
康王[赵构在](../Page/赵构.md "wikilink")[南京應天府称帝](../Page/南京應天府.md "wikilink")，为宋高宗，重建宋朝。史称「南宋」

[1129年](../Page/1129年.md "wikilink")
[金兀朮攻破临安](../Page/金兀朮.md "wikilink")（今[杭州](../Page/杭州.md "wikilink")）、明州（今[寧波](../Page/寧波.md "wikilink")），高宗從海上逃到溫州

[1130年](../Page/1130年.md "wikilink")
[韓世忠在黄天荡擊敗金兀朮](../Page/韓世忠.md "wikilink")

[1130年](../Page/1130年.md "wikilink")—[1135年](../Page/1135年.md "wikilink")
[钟相](../Page/钟相.md "wikilink")、[杨幺起义](../Page/杨幺.md "wikilink")

[1140年](../Page/1140年.md "wikilink")
[劉錡的](../Page/劉錡.md "wikilink")「八字軍」在顺昌，[岳飛的岳家軍在郾城](../Page/岳飛.md "wikilink")、穎昌先后大敗金兀朮

[1142年](../Page/1142年.md "wikilink")
趙構、秦桧杀岳飞于除夕夜，[绍兴和议達成](../Page/绍兴和议.md "wikilink")，趙構生母[韋氏回宋](../Page/韋賢妃_\(宋朝\).md "wikilink")

[1161年](../Page/1161年.md "wikilink")
金海陵王[完顏亮伐宋](../Page/完顏亮.md "wikilink")，[虞允文](../Page/虞允文.md "wikilink")[采石大捷](../Page/采石大捷.md "wikilink")，完顏亮被部下所殺

[1162年](../Page/1162年.md "wikilink")
宋高宗退位，[宋孝宗即位](../Page/宋孝宗.md "wikilink")

[1163年](../Page/1163年.md "wikilink")
宋進行[隆興北伐](../Page/隆興北伐.md "wikilink")，于[符离之战大败](../Page/符离之战.md "wikilink")

[1164年](../Page/1164年.md "wikilink") [隆興和议](../Page/隆興和议.md "wikilink")

[1189年](../Page/1189年.md "wikilink")
宋孝宗退位，[宋光宗即位](../Page/宋光宗.md "wikilink")

[1194年](../Page/1194年.md "wikilink")
宋孝宗死，宋光宗不加理會，[韩侂胄逼宋光宗禪位給](../Page/韩侂胄.md "wikilink")[宋宁宗](../Page/宋宁宗.md "wikilink")

[1206年](../Page/1206年.md "wikilink")
韩侂胄[開禧北伐](../Page/開禧北伐.md "wikilink")，失败被殺，函首金國

[1208年](../Page/1208年.md "wikilink") 嘉定和议

[1224年](../Page/1224年.md "wikilink")
宋宁宗死，[宋理宗即位](../Page/宋理宗.md "wikilink")

[1233年](../Page/1233年.md "wikilink") [端平更化](../Page/端平更化.md "wikilink")

[1234年](../Page/1234年.md "wikilink")
宋蒙联军灭金，[端平入洛](../Page/端平入洛.md "wikilink")。蒙古反攻南宋，宋蒙战争爆发

[1260年](../Page/1260年.md "wikilink")
[贾似道开始专權](../Page/贾似道.md "wikilink")

[1264年](../Page/1264年.md "wikilink")
宋理宗死，[宋度宗即位](../Page/宋度宗.md "wikilink")

[1267年](../Page/1267年.md "wikilink")—[1273年](../Page/1273年.md "wikilink")
宋蒙襄樊战争，宋败

[1275年](../Page/1275年.md "wikilink")
宋度宗死，[宋恭帝立](../Page/宋恭帝.md "wikilink")，谢太后临朝。宋廷杀贾似道

[1276年](../Page/1276年.md "wikilink") 元軍攻取临安，虏宋恭帝、谢太后

[1279年](../Page/1279年.md "wikilink")
宋元[厓山海战](../Page/厓山海战.md "wikilink")，宋败。陆秀夫背幼主跳海而死，宋亡
</onlyinclude>

## 参见

  - [北宋大事年表](../Page/北宋大事年表.md "wikilink")

[Category:南宋](../Category/南宋.md "wikilink")
[Category:中国史年表](../Category/中国史年表.md "wikilink")