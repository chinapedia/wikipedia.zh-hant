**皇家伊斯柏納體育會**（**Real Club Deportivo España**，简称 **Real España** 或 El
**España**），是[洪都拉斯职业足球俱乐部](../Page/洪都拉斯.md "wikilink")，现为甲级联赛球队，球队位于[洪都拉斯的](../Page/洪都拉斯.md "wikilink")[圣佩德罗苏拉](../Page/圣佩德罗苏拉.md "wikilink")。

## 历史

皇家伊斯柏納建於1929年7月14日 at *Escuela Ramón Rosa, de San Pedro Sula*，創辦人是Pastor
Reyes, Juan Banegas, "Teco" Lardizábal, Hugo Escoto Soto及Leonardo
Muñoz.\[1\] 在西班牙語的"Real" - 是由西班牙國王 [Juan Carlos
I在](../Page/Juan_Carlos_I_of_Spain.md "wikilink")1977年給1。球隊是西班牙以外球隊少數使用**皇家**（Real）名字的球隊。

皇家伊斯柏納體育會是其中一支成功的洪都拉斯球隊，其受歡迎程度很高，以至于在洪都拉斯以外地區亦有支持者。

## 队色和队标

球隊的顏色是黑和黃色，而球隊的綽號稱為「The
Aurinegros」，意思是黃色及黑色的混成語。隊在2006年更換了隊徽，更引更多年輕人留意。球隊的吉祥物更變了多次。它曾經是黃黑色貓頭鷹、一輛火車最近是一台機器人。王冠代表了「皇家」，是獲得西班牙國王加許。這包括了著名球隊[皇家馬德里等](../Page/皇家馬德里.md "wikilink")。

## 体育场

*主條目：[Estadio Francisco
Morazán](../Page/Estadio_Francisco_Morazán.md "wikilink"), [San Pedro
Sula](../Page/San_Pedro_Sula.md "wikilink")*

球隊的主場是Estadio Francisco Morazán.

## 支持者

## 球队荣誉

### 国内赛场

  - **[洪都拉斯職業足球聯賽冠軍](../Page/洪都拉斯職業足球聯賽.md "wikilink"): 10**

<!-- end list -->

  -

      -
        1974-1975, 1975-1976, 1976-1977, 1980-1981, 1988-1989, 1990-1991
        , 1993-1994, 2003-2004 Ap., 2006-2007 Cl., 2010-2011 Ap.

<!-- end list -->

  - **Liga Nacional de Fútbol de Honduras亞軍: 10**

<!-- end list -->

  -

      -
        1977-1978, 1978-1979, 1986-1987, 1989-1990, 1991-1992,
        1995-1996, 1997-1998 Ap., 1998-1999, 2007-2008 Ap., 2008-2009
        Ap.

<!-- end list -->

  - **[洪都拉斯杯冠軍](../Page/洪都拉斯杯.md "wikilink"): 2**

<!-- end list -->

  -

      -
        1972, 1992

<!-- end list -->

  - **[洪都拉斯杯亞軍](../Page/洪都拉斯杯.md "wikilink"): 1**

<!-- end list -->

  -

      -
        1968

### 国际比賽

  - **[CONCACAF Champions'
    Cup季軍](../Page/CONCACAF_Champions'_Cup.md "wikilink"): 1**

<!-- end list -->

  -

      -
        1991

<!-- end list -->

  - **[CONCACAF Cup Winners
    Cup亞軍](../Page/CONCACAF_Cup_Winners_Cup.md "wikilink"): 1**

<!-- end list -->

  -

      -
        1993

<!-- end list -->

  - **[Torneo
    Fraternidad冠軍](../Page/Copa_Interclubes_UNCAF.md "wikilink"):
    1**

<!-- end list -->

  -

      -
        1982

<!-- end list -->

  - **[Torneo
    Fraternidad亞軍](../Page/Copa_Interclubes_UNCAF.md "wikilink"):
    1**

<!-- end list -->

  -

      -
        1979

<!-- end list -->

  - **[Torneo Grandes de
    Centroamerica四強](../Page/UNCAF_Club_Tournament.md "wikilink"):
    1**

<!-- end list -->

  -

      -
        1998

<!-- end list -->

  - **[Copa Interclubes
    UNCAF季軍](../Page/Copa_Interclubes_UNCAF.md "wikilink"): 1**

<!-- end list -->

  -

      -
        2000

### 其他

  - 首个联赛 "Tri-Campeón" (连续三个赛季夺得冠军)

## 参考消息

<div class="references-small">

<references/>

</div>

## 外部链接

  - [Website](http://www.realcdespana.com/Official)

  - [Official Supporters Website](http://www.comando12.com/)

[Real España](../Category/洪都拉斯足球俱乐部.md "wikilink")

1.