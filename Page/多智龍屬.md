**多智龍屬**（[屬名](../Page/屬.md "wikilink")：）又譯**腦龍**，是[甲龍科](../Page/甲龍科.md "wikilink")[恐龍一屬](../Page/恐龍.md "wikilink")，是已知生存年代最晚的[亞洲甲龍科恐龍](../Page/亞洲.md "wikilink")。目前發現了至少5個標本，包含兩個完整頭顱骨，與一個接近完整的顱後骨骸。多智龍同時也是已知最大型的亞洲[甲龍下目恐龍](../Page/甲龍下目.md "wikilink")，身長估計為4到6.2公尺，頭顱骨長度為40公分，寬度為45公分，重量可能為3100公斤。

## 敘述

[Tarchia_skull_0467.JPG](https://zh.wikipedia.org/wiki/File:Tarchia_skull_0467.JPG "fig:Tarchia_skull_0467.JPG")
[Tarchia_SIZE.jpg](https://zh.wikipedia.org/wiki/File:Tarchia_SIZE.jpg "fig:Tarchia_SIZE.jpg")
多智龍的屬名在[蒙古語意為](../Page/蒙古語.md "wikilink")「腦部」，是以牠們的大型頭部為名。[模式種是](../Page/模式種.md "wikilink")**巨大多智龍**（*T.
gigantea*），是多智龍的唯一種。多智龍的化石發現於[蒙古的](../Page/蒙古.md "wikilink")[巴魯恩戈約特組](../Page/巴魯恩戈約特組.md "wikilink")（原先名為[下奈莫格特層](../Page/下奈莫格特層.md "wikilink")），年代可能為[坎潘階到](../Page/坎潘階.md "wikilink")[馬斯垂克階](../Page/馬斯垂克階.md "wikilink")。發現多智龍的地層在該年代可能為風成沙丘或丘間地環境，擁有間歇性湖泊與季節性溪流。因此多智龍是種居住於沙漠的動物。多智龍的頭頂由球根狀、多邊形的鱗甲構成，類似[美甲龍的頭頂](../Page/美甲龍.md "wikilink")，美甲龍是巴魯恩戈約特組所發現的另一種甲龍科恐龍。多智龍與美甲龍的差別為：[頭蓋骨基部](../Page/頭蓋骨基部.md "wikilink")（Basicranium）較大、[副枕突](../Page/副枕突.md "wikilink")（Paroccipital
process）與[方骨間未固定](../Page/方骨.md "wikilink")、[前上頜骨的喙寬度大於](../Page/前上頜骨.md "wikilink")[上頜骨的兩排齒列間的距離](../Page/上頜骨.md "wikilink")。

## 分類學與系統發生學

Vickaryous等人的2004年研究發現晚[白堊紀的](../Page/白堊紀.md "wikilink")[甲龍科分為兩個](../Page/甲龍科.md "wikilink")[演化支](../Page/演化支.md "wikilink")，分別為[北美洲演化支](../Page/北美洲.md "wikilink")（[甲龍](../Page/甲龍.md "wikilink")、[包頭龍](../Page/包頭龍.md "wikilink")），與[亞洲演化支](../Page/亞洲.md "wikilink")（[繪龍](../Page/繪龍.md "wikilink")、[美甲龍](../Page/美甲龍.md "wikilink")、[天鎮龍](../Page/天鎮龍.md "wikilink")、[籃尾龍](../Page/籃尾龍.md "wikilink")）。[倍甲龍](../Page/倍甲龍.md "wikilink")（*Dyoplosaurus
giganteus*）被認為是多智龍的一個[次同物異名](../Page/次同物異名.md "wikilink")，與多智龍的第二個假設種*T.
kielanae*是同種動物。

## 參考資料

  - Barret, P. M. 2001. "Tooth wear and possible jaw action in
    *Scelidosaurus harrisonii* and a review of feeding mechanisms in
    other thyreophoran dinoaurs", in Carpenter, K. (ed.) *The Armored
    Dinosaurs*. Indiana University Press, Bloomington. pp.25-52.
  - Carpenter, K., Kirkland, J. I., Birge, D., and Bird, J. 2001.
    "Disarticulated skull of a new primitive anklyosaurid from the Lower
    Cretaceous of Utah", in Carpenter, K. (editor) 2001, *The Armored
    Dinosaurs*. Indiana University Press
  - Maryanska, T. 1977. "Ankylosauridae (Dinosauria) from Mongolia",.
    *Palaeontologia Polonica* 37:85-151
  - Tumanova, T. A. 1978. "New data on the ankylosaur *Tarchia
    gigantea*", ''Paleontological Journal ''11: 480-486.
  - Vickaryous, Maryanska, and Weishampel 2004. "Chapter Seventeen:
    Ankylosauria", in *The Dinosauria* (2nd edition), Weishampel, D. B.,
    Dodson, P., and Osmólska, H., editors. University of California
    Press.

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [*Saichania* in the Dino
    Directory](https://web.archive.org/web/20071210141416/http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Saichania)

[Category:甲龍科](../Category/甲龍科.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")