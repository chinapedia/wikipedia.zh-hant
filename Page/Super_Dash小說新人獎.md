**Super
Dash小說新人獎**是[集英社舉辦的](../Page/集英社.md "wikilink")[輕小說新人獎](../Page/輕小說.md "wikilink")。一年發表一次（每年的10月25日截止，隔年4月25日發表）。得獎作品由評審委員共同決議，大賞得獎者可以得到正賞的盾和副賞100萬日圓，佳作得獎者可以得到正賞的盾和副賞50萬日圓，同時出版後能夠得到版稅。
此外，得獎者的作品會在[Super Dash文庫出版](../Page/Super_Dash文庫.md "wikilink")。

## 評審委員

  - 第1回（2001年） - 第5回（2005年）
    [新井素子](../Page/新井素子.md "wikilink")・[高橋良輔](../Page/高橋良輔.md "wikilink")・[堀井雄二](../Page/堀井雄二.md "wikilink")・[阿部和重](../Page/阿部和重.md "wikilink")
  - 第6回（2006年） - 第7回（2007年）
    新井素子・高橋良輔・堀井雄二・[中村航](../Page/中村航.md "wikilink")
  - 第8回（2008年） - 至今
    新井素子・高橋良輔・堀井雄二・中村航・[稻垣理一郎](../Page/稻垣理一郎.md "wikilink")

## 得獎作品一覽

<table>
<colgroup>
<col style="width: 10%" />
<col style="width: 15%" />
<col style="width: 55%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>第1回<br />
（2001年）</p></th>
<th><p>投稿總數/453点</p></th>
<th><p>標題（→刊行時表題）〔原文標題〕</p></th>
<th><p>著者（→刊行時<a href="../Page/筆名.md" title="wikilink">筆名</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>大賞</p></td>
<td><p><a href="../Page/世界征服物語.md" title="wikilink">世界征服物語 〜優瑪的大冒險〜</a></p></td>
<td><p><a href="../Page/神代明.md" title="wikilink">神代明</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p>[[D.I.Speed</p></td>
<td><p>|D.I.Speed</p></td>
<td><p>]]</p></td>
</tr>
<tr class="odd">
<td><p>第2回<br />
（2002年）</p></td>
<td><p>投稿總數/321点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="even">
<td><p>大賞</p></td>
<td><p><a href="../Page/銀盤萬花筒.md" title="wikilink">銀盤萬花筒</a> vol.1&amp;vol.2</p></td>
<td><p>EL星くーりっじ（→<a href="../Page/海原零.md" title="wikilink">海原零</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p>地下鉄女王（→<a href="../Page/地底世界_(小說).md" title="wikilink">地底世界</a>）〔〕</p></td>
<td><p><a href="../Page/東佐紀.md" title="wikilink">東佐紀</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第3回<br />
（2003年）</p></td>
<td><p>投稿總數/361点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p>電波日和（→<a href="../Page/電波系彼女.md" title="wikilink">電波系彼女</a>）</p></td>
<td><p>片山大介（→<a href="../Page/片山憲太郎.md" title="wikilink">片山憲太郎</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/殿がくる!.md" title="wikilink">殿がくる!</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第4回<br />
（2004年）</p></td>
<td><p>投稿總數/427点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="even">
<td><p>大賞</p></td>
<td><p><a href="../Page/戰鬥司書系列.md" title="wikilink">戰鬥司書系列</a></p></td>
<td><p><a href="../Page/山形石雄.md" title="wikilink">山形石雄</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>その仮面をはずして（→<a href="../Page/滅びのマヤウェル.md" title="wikilink">滅びのマヤウェル</a>）</p></td>
<td><p><a href="../Page/岡崎裕信.md" title="wikilink">岡崎裕信</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p>Shadow&amp;Light（→）</p></td>
<td><p><a href="../Page/影名淺海.md" title="wikilink">影名淺海</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5回<br />
（2005年）</p></td>
<td><p>投稿總數/641点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="even">
<td><p>大賞</p></td>
<td><p><a href="../Page/黄色い花の紅.md" title="wikilink">黄色い花の紅</a></p></td>
<td><p><a href="../Page/アサウラ.md" title="wikilink">アサウラ</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p>Beurre・Noisette（→）</p></td>
<td><p><a href="../Page/藍上陸.md" title="wikilink">藍上陸</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6回<br />
（2006年）</p></td>
<td><p>投稿總數/515点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="odd">
<td><p>大賞</p></td>
<td><p><a href="../Page/鐵球公主艾蜜莉.md" title="wikilink">鐵球公主艾蜜莉</a>〔鉄球姫エミリー〕</p></td>
<td><p><a href="../Page/八薙玉造.md" title="wikilink">八薙玉造</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p>警極魔道課車陸比老師〔〕</p></td>
<td><p><a href="../Page/横山忠.md" title="wikilink">横山忠</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ガン×スクール＝パラダイス!.md" title="wikilink">ガン×スクール＝パラダイス!</a></p></td>
<td><p>やまだゆうすけ（→<a href="../Page/穂邑正裕.md" title="wikilink">穂邑正裕</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第7回<br />
（2007年）</p></td>
<td><p>投稿總數/590点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p><a href="../Page/超人間・岩村.md" title="wikilink">超人間・岩村</a></p></td>
<td><p><a href="../Page/滝川廉治.md" title="wikilink">滝川廉治</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/スイーツ!.md" title="wikilink">スイーツ!</a></p></td>
<td><p><a href="../Page/しなな泰之.md" title="wikilink">しなな泰之</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>〔〕</p></td>
<td><p><a href="../Page/弥生翔太.md" title="wikilink">弥生翔太</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8回<br />
（2008年）</p></td>
<td><p>投稿總數/811点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="odd">
<td><p>佳作</p></td>
<td><p><a href="../Page/アンシーズ.md" title="wikilink">アンシーズ</a></p></td>
<td><p><a href="../Page/宮澤周.md" title="wikilink">宮澤周</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逆理の魔女.md" title="wikilink">逆理の魔女</a></p></td>
<td><p>雪叙静（→<a href="../Page/雪野静.md" title="wikilink">雪野静</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第9回<br />
（2009年）</p></td>
<td><p>投稿總數/799点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="even">
<td><p>大賞</p></td>
<td><p>うさパン! 私立戦車小隊／首なしラビッツ（→<a href="../Page/妮娜與兔子與魔法戰車.md" title="wikilink">妮娜與兔子與魔法戰車</a>）〔〕</p></td>
<td><p>うさぎ鍋竜之介（→<a href="../Page/兔月龍之介.md" title="wikilink">兔月龍之介</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>オワ・ランデ 〜夢魔の貴族は焦らし好き〜（→<a href="../Page/オワ・ランデ.md" title="wikilink">オワ・ランデ</a> ヤレない貴族のオトシ方）</p></td>
<td><p><a href="../Page/神秋昌史.md" title="wikilink">神秋昌史</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佳作</p></td>
<td><p><a href="../Page/ライトノベルの神さま.md" title="wikilink">ライトノベルの神さま</a></p></td>
<td><p>青青（→）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>二年四組 暴走中!（→<a href="../Page/二年四組_交換日記.md" title="wikilink">二年四組 交換日記</a> 腐ったリンゴはくさらない）</p></td>
<td><p>片山禾域（→<a href="../Page/朝田雅康.md" title="wikilink">朝田雅康</a>）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第10回<br />
（2010年）</p></td>
<td><p>投稿總數/977点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="odd">
<td><p>大賞</p></td>
<td><p><a href="../Page/くずばこに箒星.md" title="wikilink">くずばこに箒星</a></p></td>
<td><p><a href="../Page/石原宙.md" title="wikilink">石原宙</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/覇道鋼鉄テッカイオー.md" title="wikilink">覇道鋼鉄テッカイオー</a></p></td>
<td><p><a href="../Page/八針来夏.md" title="wikilink">八針来夏</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>優秀賞</p></td>
<td><p><a href="../Page/サカサマホウショウジョ.md" title="wikilink">サカサマホウショウジョ</a></p></td>
<td><p><a href="../Page/大澤誠.md" title="wikilink">大澤誠</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>特別賞</p></td>
<td><p>嘘つき天使は死にました!（嘘）（→<a href="../Page/嘘つき天使は死にました!.md" title="wikilink">嘘つき天使は死にました!</a> ）</p></td>
<td><p><a href="../Page/葉巡明治.md" title="wikilink">葉巡明治</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第11回<br />
（2011年）</p></td>
<td><p>投稿總數/945点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="even">
<td><p>大賞</p></td>
<td><p><a href="../Page/暗号少女が解読できない.md" title="wikilink">暗号少女が解読できない</a></p></td>
<td><p>新保静波（→<a href="../Page/神保静波.md" title="wikilink">神保静波</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>優秀賞</p></td>
<td><p>Draglight/5つ星と7つ星（→<a href="../Page/エンド・アステリズム.md" title="wikilink">エンド・アステリズム</a>　なぜその機械と少年は彼女が不動で宇宙の中心であると考えたか）</p></td>
<td><p>篠宜曜（→<a href="../Page/下村智惠理.md" title="wikilink">下村智惠理</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>特別賞</p></td>
<td><p>終わる世界の物語（→<a href="../Page/伊月の戦争_～終わる世界の物語～.md" title="wikilink">伊月の戦争　～終わる世界の物語～</a>）</p></td>
<td><p>宇野涼平（→<a href="../Page/涼野遊平.md" title="wikilink">涼野遊平</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>君の勇者に俺はなる!（→<a href="../Page/God_Bravers_君の勇者に俺はなる！.md" title="wikilink">God Bravers　君の勇者に俺はなる！</a>）</p></td>
<td><p><a href="../Page/永原十茂.md" title="wikilink">永原十茂</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第12回<br />
（2012年）</p></td>
<td><p>投稿總數/985点</p></td>
<td><p>標題（→刊行時表題）〔原文標題〕</p></td>
<td><p>著者（→刊行時筆名）</p></td>
</tr>
<tr class="odd">
<td><p>優秀賞</p></td>
<td><p>代償のギルタオン</p></td>
<td><p><a href="../Page/神高槍矢.md" title="wikilink">神高槍矢</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>付喪神は青春をもてなさんと欲す</p></td>
<td><p><a href="../Page/慶野由志.md" title="wikilink">慶野由志</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [Super
    Dash小說新人獎網頁](https://web.archive.org/web/20081218020655/http://dash.shueisha.co.jp/sinjin/)

[Category:日本輕小說文學獎](../Category/日本輕小說文學獎.md "wikilink")
[Category:集英社主導的獎](../Category/集英社主導的獎.md "wikilink")
[\*](../Category/Super_Dash文庫.md "wikilink")