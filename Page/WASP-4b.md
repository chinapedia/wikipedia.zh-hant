\[1\] | eccentricity = 0.0\[2\] | period =
1.3382324{{±|0.0000017|0.0000029}}\[3\] | inclination =
89.35{{±|0.64|0.49}}\[4\] | t_transit_no_jd =
2454383.313070{{±|0.000045|0.000074}}
[HJD](../Page/Heliocentric_Julian_Date.md "wikilink")\[5\] | semi-amp =
247.6{{±|13.9|6.8}}\[6\] }} \[7\] | mass = 1.21{{±|0.13|0.08}}\[8\] |
temperature = 1650 ± 30\[9\] }}

**WASP-4b**是位於[鳳凰座](../Page/鳳凰座.md "wikilink")，距離大約1,000光年的一顆[系外行星](../Page/系外行星.md "wikilink")\[10\]，這顆環繞著[WASP-4的行星是在](../Page/WASP-4.md "wikilink")2007年10月發現的。這顆行星的質量和半徑顯示它是類似[木星的](../Page/木星.md "wikilink")[氣體巨星](../Page/氣體巨星.md "wikilink")
\[11\]。WASP-4b非常靠近母恆星，因此歸類為[熱木星](../Page/熱木星.md "wikilink")，使得大氣層的溫度大約是1650[K](../Page/熱力學溫標.md "wikilink")\[12\]\[13\]。

[WASP-4_b_rv.pdf](https://zh.wikipedia.org/wiki/File:WASP-4_b_rv.pdf "fig:WASP-4_b_rv.pdf")

這顆行星是使用[SuperWASP專案的照相機在](../Page/SuperWASP.md "wikilink")[南非發現的](../Page/南非.md "wikilink")\[14\]。在發現之後，使用WASP-4的徑向速度測量出WASP-4b的質量，並且經由凌日法確認這是一顆行星。

## 參考資料

## 相關條目

  - [SuperWASP](../Page/超廣角尋找行星.md "wikilink")
  - [WASP-4](../Page/WASP-4.md "wikilink")

## 外部鍊結

  -
  -
[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")
[Category:2007年發現的系外行星](../Category/2007年發現的系外行星.md "wikilink")
[Category:类木行星](../Category/类木行星.md "wikilink")
[Category:熱木星](../Category/熱木星.md "wikilink")
[Category:凌日系外行星](../Category/凌日系外行星.md "wikilink")
[Category:鳳凰座](../Category/鳳凰座.md "wikilink")

1.  Table 3, Improved parameters for the transiting hot Jupiters WASP-4b
    and WASP-5b, M. Gillon et al., *Astronomy and Astrophysics* **496**,
    \#1 (2009), pp. 259–267, , .

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.