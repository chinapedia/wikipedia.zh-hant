[Flashingsecond.gif](https://zh.wikipedia.org/wiki/File:Flashingsecond.gif "fig:Flashingsecond.gif")
**秒**是[國際單位制中](../Page/國際單位制.md "wikilink")[時間的基本單位](../Page/時間.md "wikilink")\[1\]，符號是**s**。有時也會借用英文缩写標示為**sec**\[2\]。秒在英文裡的原始詞義是計算[小時的六十分之一](../Page/小時.md "wikilink")（[分鐘](../Page/分鐘.md "wikilink")）後，再計算六十分之一\[3\]。在西元1000至1960年之間，秒的定義是平均[太陽日的](../Page/太陽日.md "wikilink")1/86,400（在一些天文及法律的定義中仍然適用）\[4\]。在1960至1967年之間，定義為1960年地球自轉一周時間的1/86,400\[5\]，現在則是用原子的特性來定義。秒也可以用機械[鐘](../Page/鐘.md "wikilink")、電子鐘或原子鐘來計時。

[國際單位制詞頭經常與秒結合以做更細微的劃分](../Page/國際單位制詞頭.md "wikilink")，例如ms（毫秒，千分之一秒）、µs（微秒，百萬分之一秒）和ns（奈秒，十億分之一秒）。雖然國際單位制詞頭也可以用於擴增時間，例如ks（千秒）、Ms（百萬秒）和Gs（十億秒），但實際上很少這樣子使用，大家都還是習慣用60進位的分、時和24進位的日做為秒的擴充。

秒不但是國際單位制中時間的基本單位，也是[公分-克-秒制](../Page/公分-克-秒制.md "wikilink")、[米-公斤-秒制](../Page/米-公斤-秒制.md "wikilink")、[米-公噸-秒制及](../Page/米-公噸-秒制.md "wikilink")[英制單位下的時間基本單位](../Page/英制單位.md "wikilink")。

## 世界公認的秒

在現行國際單位制下，在1967年召開的第13屆國際度量衡大會對秒的定義是：[銫](../Page/銫.md "wikilink")133原子[基態的兩個超精細](../Page/基態.md "wikilink")[能階間](../Page/能階.md "wikilink")[躍遷對應輻射的](../Page/躍遷.md "wikilink")9,192,631,770個週期的持續時間。\[6\]這個定義提到的銫原子必須在[絕對零度時是靜止的](../Page/絕對零度.md "wikilink")，而且在地面上的環境是零[磁場](../Page/磁場.md "wikilink")。\[7\]在這樣的情況下被定義的秒，與天文學上的[曆書時所定義的秒是等效的](../Page/曆書時.md "wikilink")。

秒的國際標準符號可以參考。

2019年，[国际时间频率咨询委员会将讨论](../Page/國際度量衡委員會.md "wikilink")“秒”的重新定义问题。\[8\]

## 與其他時間單位的關係

1 國際秒相當於：

  - 1/60 分
  - 1/3,600 [時](../Page/時.md "wikilink")
  - 1/86,400 [日](../Page/日.md "wikilink")
    （[國際天文聯合會的時間單位](../Page/國際天文聯合會.md "wikilink")）
  - 1/31,557,600
    [儒略年](../Page/儒略年.md "wikilink")（[國際天文聯合會的時間單位](../Page/國際天文聯合會.md "wikilink")）

## 歷史的起源

### 在機械鐘錶之前

本來，時被分割為60分，分又被分割為60秒。在有些語系中，像是波蘭語（*tercja*）和阿拉伯語（ثانية），**秒**也以60進位制被再細分，但在現代，都是以十進位法來細分小數點以下的時間。

[六十進位制來自](../Page/六十進制.md "wikilink")[巴比倫](../Page/巴比倫.md "wikilink")，他們以六十這個因素做為計算數量的單位。但是巴比倫人並沒有將[時分割為](../Page/時.md "wikilink")60分，而是[古埃及將一](../Page/古埃及.md "wikilink")[日分為](../Page/日.md "wikilink")12時的白天和12時的夜晚，他們也這樣子來區分四季。[古希臘天文學家](../Page/古希臘.md "wikilink")，包括[希巴谷和](../Page/喜帕恰斯.md "wikilink")[托勒密](../Page/托勒密.md "wikilink")，定義[太陽日的](../Page/太陽日.md "wikilink")24分之一為[時](../Page/時.md "wikilink")。
以六十進位細分時，使得秒是一太陽日的86,400分之一。古希臘的時間週期，像是平[朔望月定義得非常精確](../Page/朔望月.md "wikilink")，因為他們不是觀察單一的朔望月，而是以相距數百年的[食來測量朔望月的平均長度](../Page/日食.md "wikilink")（日數）。

### 利用機械鐘錶計時

最早可以顯示秒的時鐘出現在十六世紀的後半。最早用發條驅動，有秒針的鐘是一個沒有刻度，上面有[奧菲斯的鐘](../Page/奧菲斯.md "wikilink")，是Fremersdorf收藏的一部份，年代約在1560年至1570年之間\[9\]\[10\]。在1550至1575年之間，作了一個鐘，每1/5分就有一個刻度\[11\]。1579年時，Jost
Bürgi為作了一個鐘，上面有秒的刻度\[12\]。1581年時[第谷·布拉赫在他的天文台中重新設計一個時鐘](../Page/第谷·布拉赫.md "wikilink")，可以顯示分及秒，但秒的計時不夠精確。1587年時第谷·布拉赫抱怨他的四個鐘差了正負四秒\[13\]。

[擺鐘的出現使得人們首次可以精確的計時到秒](../Page/擺鐘.md "wikilink")。[馬蘭·梅森在](../Page/馬蘭·梅森.md "wikilink")1644年計算長度39.1英吋（(0.994
m）的單擺在[標準重力下週期為](../Page/標準重力.md "wikilink")2秒，單擺從最低點擺動，一秒後會回到原點，因此可以精準的計時到秒。

秒擺的擺長在1660年被[倫敦皇家學會提出作為長度的單位](../Page/倫敦皇家學會.md "wikilink")，在地球表面，擺長約一米的單擺，一次擺動或是半週期（沒有反複的一次擺動）的時間大約是一秒。
\[14\]。[擺鐘的發明可以計算平時](../Page/擺鐘.md "wikilink")（相對於日晷所顯示的視時），使得秒成為可測量的時間單位。

### 現在的量測

「秒」這個時間單位的英文second在16世紀末出現在英文中，約比精確測量到秒的時間要晚一百年。用拉丁文寫作的人，包括[罗吉尔·培根](../Page/罗吉尔·培根.md "wikilink")、[第谷·布拉赫及](../Page/第谷·布拉赫.md "wikilink")[約翰內斯·克卜勒等科學家仍使用拉丁文的secunda](../Page/約翰內斯·克卜勒.md "wikilink")，意思和1200年時的意思相同。

1832年時[高斯提議用秒作為](../Page/卡爾·弗里德里希·高斯.md "wikilink")[厘米-克-秒制的時間單位](../Page/厘米-克-秒制.md "wikilink")。（BAAS）在1862年表示「所有科學界的人都同意用平均太陽日計算的秒為時間單位。」\[15\]。BAAS在1874年正式的提出[公分-克-秒制](../Page/公分-克-秒制.md "wikilink")，不過在70年後被[米-公噸-秒制取代](../Page/米-公噸-秒制.md "wikilink")。公分-克-秒制及米-公噸-秒制都以秒為時間單位，定義為平均太陽日的1/86,400。

在1956年，秒被以特定[曆元下的地球公轉週期來定義](../Page/曆元.md "wikilink")，因為當時天文學家知道地球在自轉軸上的自轉不夠穩定，不足以作為時間的標準。[紐康的太陽表以](../Page/紐康的太陽表.md "wikilink")1900年的曆元描述太陽的運動，所依據的是1750年至1892年的觀測。在1956年，\[16\]秒的定義如下：

  -
    自[曆書時](../Page/曆書時.md "wikilink")1900年1月0日12時起算的回歸年的31,556,925.9747分之一為一秒\[17\]

在1960年，這個定義由第十一次的國際度量衡會議通過。雖然這個定義中的回歸年的長度不能進行實測，但可以經由線性關係的平回歸年的算式推導，因此，有一個具體的瞬時回歸年長度可以參考。因為秒是用於大半個20世紀太陽和月球的[星曆表中的獨立時間變數](../Page/星曆表.md "wikilink")（紐康的太陽表從1900年使用至1983年，的月球表從1920年使用至1983年），因此這個秒被稱為[曆書秒](../Page/曆書時.md "wikilink")。\[18\]

隨著[原子鐘的發展](../Page/原子鐘.md "wikilink")，秒的定義決定改採用原子時做為新的定義基準，而不再採用地球公轉太陽定義的曆書秒。

經過多年的努力，[英國國家物理實驗室的](../Page/英國國家物理實驗室.md "wikilink")和[美國海軍天文台的](../Page/美國海軍天文台.md "wikilink")測量出[銫原子的超精細躍遷週期和曆書秒的關係](../Page/銫.md "wikilink")。\[19\]使用過去普通的測量方法，接收來自[無線電台](../Page/無線電台.md "wikilink")、[WWV的訊號](../Page/WWV.md "wikilink")，\[20\]再使用一個原子鐘來測量時間，他們確定了[月球相對於地球的軌道運動](../Page/月球.md "wikilink")，也推斷出太陽表面可能有相對於地球的運動。結果，在1967年的第13屆國際度量衡會議上決定以[原子時定義的秒作為時間的國際標準單位](../Page/原子時.md "wikilink")：

  -
    [銫](../Page/銫.md "wikilink")133原子[基態的兩個超精細](../Page/基態.md "wikilink")[能階間](../Page/能階.md "wikilink")[躍遷對應輻射的](../Page/躍遷.md "wikilink")9,192,631,770個週期的持續時間。\[21\]

在70年代體認到[引力時間膨脹會導致在不同](../Page/引力時間膨脹.md "wikilink")[高度的原子鐘有不同的秒](../Page/高度.md "wikilink")，因此每個原子鐘都必須改正為在[平均海平面的高度](../Page/海平面.md "wikilink")，以取得一致的秒（大地水平面的自轉約改變的秒長，在1977年開始修正並且在1980年已經制度化了。）。用相對論的術語來說，秒被定義成在轉動的大地水平面上[原時](../Page/原時.md "wikilink")。\[22\]

在1977年，在[BIPM的會議中又重新定義](../Page/國際度量衡局.md "wikilink")，加進了新的陳述：

  -
    銫原子在0[k下是靜止不動的](../Page/熱力學溫標.md "wikilink")。（''This definition
    refers to a caesium atom at rest at a temperature of 0 K. ''）

修正過的定義似乎暗示理想的原子鐘將只有靜止的一個銫原子發射出單一的頻率。在實務上，無論如何，這個定義意味着在那些原子鐘之內的運作和外推的數值，秒的高精密度應該如上所述的考量到周圍溫度的補償（[黑體輻射](../Page/黑體.md "wikilink")）。

## 秒的各級單位

[國際單位制的前置詞可以放在秒的前面](../Page/國際單位制.md "wikilink")，表示比秒要短的時間，但很少用此方式表示比一秒要長的時間（）。比一秒要長的時間一般會用非國際單位制的單位，如[分鐘](../Page/分鐘.md "wikilink")、[小時](../Page/小時.md "wikilink")、[日](../Page/日.md "wikilink")、[儒略年](../Page/儒略年.md "wikilink")、世紀或千年等單位表示。

<div style="float:center; margin-left: 1em;">

</div>

## 其他的定義方式

有時需要用其他和國際單位制不同的定義方法來定義秒。像[國際天文聯合會定義](../Page/國際天文聯合會.md "wikilink")[世界時的UT](../Page/世界時.md "wikilink")0、UT1及UT2系統是以地球自轉為基準得到的時間尺度，就是這様的系統。McCarthy和Seidelmann沒有說明SI制的秒是世界上時間計算的法定標準，他們只說「在這些年來UTC或是成為許多國家法定時間的基礎，或是成為事實上的常用時間基準。」\[23\]

## 汉字“秒”的古义

汉字**秒**，在古代指的是10<sup>-4</sup>，比如在《[隋书](../Page/隋书.md "wikilink")·律历志》中，记载：宋末，南徐州从事史[祖冲之](../Page/祖冲之.md "wikilink")，更开密法，以圆径一亿为一丈，圆周盈数三丈一尺四寸一分五厘九毫二**秒**七忽，朒数三丈一尺四寸一分五厘九毫二**秒**六忽，正数在盈朒二限之间。密率，圆径一百一十三，圆周三百五十五。约率，圆径七，周二十二。

## 參見

  - [国际单位制](../Page/国际单位制.md "wikilink")
  - [时间单位](../Page/时间单位.md "wikilink")
  - [时间长度比较](../Page/时间长度比较.md "wikilink")
  - [小时](../Page/小时.md "wikilink")
  - [分钟](../Page/分_\(时间\).md "wikilink")
  - [数量级 (时间)](../Page/数量级_\(时间\).md "wikilink")
  - [单位转换](../Page/单位转换.md "wikilink")
  - [原子时](../Page/原子时.md "wikilink")
  - [世界时](../Page/世界时.md "wikilink")
  - [閏秒](../Page/閏秒.md "wikilink")
  - [協調世界時](../Page/協調世界時.md "wikilink")
  - [赫茲](../Page/赫茲.md "wikilink")
  - [贝可勒尔](../Page/贝可勒尔.md "wikilink")
  - [比衝](../Page/比衝.md "wikilink")：單位流量燃料產生的推力，若燃料以重量計算，單位即為秒
  - [時間標準](../Page/時間標準.md "wikilink")
  - [毫秒](../Page/毫秒.md "wikilink")
  - [微秒](../Page/微秒.md "wikilink")
  - [纳秒](../Page/纳秒.md "wikilink")
  - [皮秒](../Page/皮秒.md "wikilink")
  - [飛秒](../Page/飛秒.md "wikilink")

## 參考資料

## 外部連結

  - [Official BIPM definition of the
    second](http://www.bipm.org/en/si/si_brochure/chapter2/2-1/second.html)
  - [Seconds and leap seconds by the
    USNO](http://tycho.usno.navy.mil/leapsec.html)
  - [The leap second: its history and possible
    future](http://www.cl.cam.ac.uk/~mgk25/time/metrologia-leapsecond.pdf)
  - [National Physical Laboratory: *Trapped ion optical frequency
    standards*](http://www.npl.co.uk/server.php?show=ConWebDoc.1086)
  - [*High-accuracy strontium ion optical clock*; National Physical
    Laboratory
    (2005)](http://resource.npl.co.uk/docs/networks/time/meeting3/klein.pdf)
  - [NIST: *Definition of the second*; notice the cesium atom must be in
    its ground state at 0
    K](http://physics.nist.gov/cuu/Units/second.html)

[M](../Category/时间单位.md "wikilink")
[M](../Category/国际单位制基本单位.md "wikilink")
[Category:厘米-克-秒制](../Category/厘米-克-秒制.md "wikilink")

1.

2.

3.

4.

5.

6.
7.
8.

9.

10.  full page color photo: 4th caption page, 3rd photo thereafter
    (neither pages nor photos are numbered).

11.

12.
13.
14.

15.

16.
17.
18.
19.
20.

21.
22. R. A. Nelson *et al.*, "", *Metrologia* **38** (2000) 509-529, p.
    515.

23.