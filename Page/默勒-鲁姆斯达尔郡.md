[Møre_og_Romsdal_kart.png](https://zh.wikipedia.org/wiki/File:Møre_og_Romsdal_kart.png "fig:Møre_og_Romsdal_kart.png")
**默勒-鲁姆斯达尔**（
）是[挪威西部的一个](../Page/挪威.md "wikilink")[郡](../Page/郡.md "wikilink")，首府为[莫尔德](../Page/莫尔德.md "wikilink")。默勒-鲁姆斯达尔郡面积15,121[平方公里](../Page/平方公里.md "wikilink")，人口245,385（2007年），是挪威人口第7大郡。

## 自治市

默勒-鲁姆斯达尔郡現有36个自治市，如下：

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Møre_og_Romsdal_Municipalities.png" title="fig:Møre_og_Romsdal_Municipalities.png">Møre_og_Romsdal_Municipalities.png</a></p></td>
<td><ol>
<li><a href="../Page/奥勒松.md" title="wikilink">奥勒松</a>（）</li>
<li><a href="../Page/艾于克拉.md" title="wikilink">艾于克拉</a>（）</li>
<li><a href="../Page/艾于勒.md" title="wikilink">艾于勒</a>（）</li>
<li><a href="../Page/阿沃島.md" title="wikilink">阿沃島</a>（）</li>
<li><a href="../Page/埃德_(默勒-鲁姆斯达尔郡).md" title="wikilink">埃德</a>（）</li>
<li><a href="../Page/弗雷納_(挪威).md" title="wikilink">弗雷納</a>（）</li>
<li><a href="../Page/弗雷_(挪威).md" title="wikilink">弗雷</a>（，於2008年1月1日併入克里斯蒂安松）</li>
<li><a href="../Page/伊斯克_(挪威).md" title="wikilink">伊斯克</a>（）</li>
<li><a href="../Page/耶姆內斯.md" title="wikilink">耶姆內斯</a>（）</li>
<li><a href="../Page/哈爾薩.md" title="wikilink">哈爾薩</a>（）</li>
<li><a href="../Page/哈拉姆.md" title="wikilink">哈拉姆</a>（）</li>
<li><a href="../Page/哈雷德.md" title="wikilink">哈雷德</a>（）</li>
<li><a href="../Page/海略_(默勒-魯姆斯達爾郡).md" title="wikilink">海略</a>（）</li>
<li><a href="../Page/克里斯蒂安松.md" title="wikilink">克里斯蒂安松</a>（）</li>
<li><a href="../Page/米迪松.md" title="wikilink">米迪松</a>（）</li>
<li><a href="../Page/莫尔德.md" title="wikilink">莫尔德</a>（）</li>
<li><a href="../Page/內瑟特.md" title="wikilink">內瑟特</a>（）</li>
<li><a href="../Page/努達爾.md" title="wikilink">努達爾</a>（）</li>
<li><a href="../Page/厄什庫格.md" title="wikilink">厄什庫格</a>（）</li>
<li><a href="../Page/厄什塔.md" title="wikilink">厄什塔</a>（）</li>
<li><a href="../Page/賴于馬.md" title="wikilink">賴于馬</a>（）</li>
<li><a href="../Page/林達爾.md" title="wikilink">林達爾</a>（）</li>
<li><a href="../Page/桑訥.md" title="wikilink">桑訥</a>（）</li>
<li><a href="../Page/桑島_(挪威).md" title="wikilink">桑島</a>（）</li>
<li><a href="../Page/斯科迪耶.md" title="wikilink">斯科迪耶</a>（）</li>
<li><a href="../Page/斯莫拉島.md" title="wikilink">斯莫拉島</a>（）</li>
<li><a href="../Page/斯圖達爾.md" title="wikilink">斯圖達爾</a>（）</li>
<li><a href="../Page/斯特蘭達.md" title="wikilink">斯特蘭達</a>（）</li>
<li><a href="../Page/敘拉.md" title="wikilink">敘拉</a>（）</li>
<li><a href="../Page/孙达尔.md" title="wikilink">孙达尔</a>（）</li>
<li><a href="../Page/敘納達爾.md" title="wikilink">敘納達爾</a>（）</li>
<li><a href="../Page/敘許爾文.md" title="wikilink">敘許爾文</a>（）</li>
<li><a href="../Page/廷沃爾.md" title="wikilink">廷沃爾</a>（）</li>
<li><a href="../Page/蒂斯特納.md" title="wikilink">蒂斯特納</a>（，於2008年1月1日併入艾于勒）</li>
<li><a href="../Page/乌尔斯泰因.md" title="wikilink">乌尔斯泰因</a>（）</li>
<li><a href="../Page/瓦尼爾文.md" title="wikilink">瓦尼爾文</a>（）</li>
<li><a href="../Page/韋斯特內斯.md" title="wikilink">韋斯特內斯</a>（）</li>
<li><a href="../Page/沃爾達.md" title="wikilink">沃爾達</a>（）</li>
</ol></td>
</tr>
</tbody>
</table>

## 外部链接

  - [默勒-鲁姆斯达尔郡官方网站](http://www.mrfylke.no)

[Møre og Romsdal](../Category/挪威行政区划.md "wikilink")