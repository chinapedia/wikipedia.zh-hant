**巨型短面熊**（*Arctodus
simus*），又名**巨型熊齒獸**、**噬牛熊**、**育空短面熊**，是一種已[滅絕的](../Page/滅絕.md "wikilink")[熊](../Page/熊.md "wikilink")，屬於[短面熊屬](../Page/短面熊屬.md "wikilink")。牠是已知曾生存的熊中最大的，生存於80-1.25萬年前的[北美洲](../Page/北美洲.md "wikilink")，尤其是[加州](../Page/加州.md "wikilink")。\[1\]巨型短面熊有兩個已知的種：育空亞種
*A. s. yukonensis* 與巨型亞種 *A. s. simus*，其中巨型亞種被認為是被認為是目前已知最大型的陸生肉食哺乳動物。
[Arctodus_simus_skull_Cleveland.jpg](https://zh.wikipedia.org/wiki/File:Arctodus_simus_skull_Cleveland.jpg "fig:Arctodus_simus_skull_Cleveland.jpg")

## 分類

巨型短面熊屬於源自[新世界的](../Page/新世界.md "wikilink")[眼鏡熊亞科](../Page/眼鏡熊.md "wikilink")。眼鏡熊亞科最早的成員是生存於[上新世](../Page/上新世.md "wikilink")（500-200萬年前）[德克薩斯州的](../Page/德克薩斯州.md "wikilink")*Plionarctos*。牠是熊齒獸及現今[眼鏡熊的祖先](../Page/眼鏡熊.md "wikilink")。雖然對熊齒獸的早期歷史所知甚少，牠後來於80萬年前[堪薩冰川作用時期廣泛分佈在](../Page/堪薩冰川作用.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。

## 生理學

巨型短面熊四肢站立時肩高1.8米，若以後肢站立則高3.3米，它的巨爪达20厘米长。估計牠的體重達1200公斤，差不多是同期[灰熊的兩倍](../Page/灰熊.md "wikilink")。\[2\]最大的標本出土自[阿拉斯加及](../Page/阿拉斯加.md "wikilink")[育空](../Page/育空.md "wikilink")。\[3\]雄性較雌性大20%。\[4\]牠是[冰河時期](../Page/冰河時期.md "wikilink")[北美洲最大的陸上掠食者](../Page/北美洲.md "wikilink")。\[5\]牠的[頭顱骨很特別](../Page/頭顱骨.md "wikilink")，沒有明顯的前額及吻闊而短，彷彿更像[豹](../Page/豹.md "wikilink")，眼窩比灰熊的大兩倍。牠的顎骨在較後的位置\[6\]，加上[頰骨](../Page/頰骨.md "wikilink")[肌肉發育極度良好](../Page/肌肉.md "wikilink")，適合咬碎骨頭以獲取[骨髓](../Page/骨髓.md "wikilink")。巨型短面熊的[下顎與](../Page/下顎.md "wikilink")[熊屬的差別在於牠有分隔肌肉連接的斜脊](../Page/熊屬.md "wikilink")。\[7\]巨型短面熊是向前直線行走的，而非像現今的熊行走時左搖右擺。巨型短面熊有[內上髁孔](../Page/內上髁孔.md "wikilink")。\[8\]

巨型短面熊有兩個已知的[亞種](../Page/亞種.md "wikilink")。從阿拉斯加、育空、[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")、[加利福尼亞州及](../Page/加利福尼亞州.md "wikilink")[猶他州的較大標本稱為育空亞種](../Page/猶他州.md "wikilink")（*A.
s.
yukonensis*），而較細小從[拉布雷亞瀝青坑發現的稱為巨型亞種](../Page/拉布雷亞瀝青坑.md "wikilink")（*A.
s. simus*）。\[9\]

巨型短面熊的近親及可能祖先是*Arctodus
pristinus*，其體型較窄小，[牙齒數量較少](../Page/牙齒.md "wikilink")，面較長，四肢則較短。\[10\]

就巨型短面熊的骨頭進行的分析顯示牠會患上[骨髓炎](../Page/骨髓炎.md "wikilink")、像[結核及](../Page/結核.md "wikilink")[梅毒等的感染](../Page/梅毒.md "wikilink")。\[11\]

## 分佈

巨型短面熊是[北美洲](../Page/北美洲.md "wikilink")[原住民](../Page/原住民.md "wikilink")，分佈在[阿拉斯加的中北平原](../Page/阿拉斯加.md "wikilink")，由[加拿大至中](../Page/加拿大.md "wikilink")[墨西哥](../Page/墨西哥.md "wikilink")，[加利福尼亞州至](../Page/加利福尼亞州.md "wikilink")[維珍尼亞州](../Page/維珍尼亞州.md "wikilink")。牠是早期北美洲熊中最普遍的，在加利福尼亞州最為豐富。\[12\]

## 食性及習性

[Mammut_americanum_humerus_with_tooth_marks.jpg](https://zh.wikipedia.org/wiki/File:Mammut_americanum_humerus_with_tooth_marks.jpg "fig:Mammut_americanum_humerus_with_tooth_marks.jpg")的肱骨化石，上頭帶有巨型短面熊的齒痕\]\]
巨型短面熊骨頭的[同位素測試發現有高濃度的](../Page/同位素.md "wikilink")[氮15](../Page/氮15.md "wikilink")，[氮同位素會在食肉動物中積聚](../Page/氮.md "wikilink")。由於沒有吃[植物的證據](../Page/植物.md "wikilink")，相信巨型短面熊是完全[肉食性的](../Page/肉食性.md "wikilink")。成年的巨型短面熊每日最少需要的肉來維持生命。\[13\]\[14\]\[15\]然而也有觀點認為巨型短面熊依然擁有十分廣泛的食性\[16\]，尤其是其粗短的顱骨與許多草食性的熊擁有類似的特徵，因此認為巨型短面熊仍然會攝取植物為食。\[17\]

巨型短面熊的強壯體型令人認為牠是掠食巨大動物的。但這個想法卻存在問題，原因是雖然巨型短面熊非常強大，但構造卻很脆弱。若要殺死大型的動物，巨型短面熊需要更為巨大，及其[骨骼結構需要更為密集](../Page/骨骼.md "wikilink")。其他學者則認為巨型短面熊可以追捕細小的[草食性的動物](../Page/草食性.md "wikilink")，如[高鼻羚羊](../Page/高鼻羚羊.md "wikilink")。但是巨型短面熊的轉彎能力很低，牠的骨骼結構不容許牠輕易的轉彎。\[18\]有指巨型短面熊的行走模式像[駱駝](../Page/駱駝.md "wikilink")，著重耐久而非速度。\[19\]牠亦沒有裝備來主動獵殺獵物，故相信牠其實會搶奪其他動物（如[恐狼](../Page/恐狼.md "wikilink")、[斯劍虎及](../Page/斯劍虎.md "wikilink")[美洲擬獅](../Page/美洲擬獅.md "wikilink")）所捕獲的食物。\[20\]雖然有認為巨型短面熊是吃[腐肉的](../Page/腐肉.md "wikilink")，牠亦有可能攻擊行動緩慢的動物，如[大地懶](../Page/大地懶.md "wikilink")。

## 滅絕

巨型短面熊約於12000年前[滅絕](../Page/滅絕.md "wikilink")，部份原因可能是其巨大的獵物首先滅絕，另外亦因其競爭者[棕熊從](../Page/棕熊.md "wikilink")[歐亞大陸進入了](../Page/歐亞大陸.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。巨型短面熊的消失亦正值是[克洛維斯文化的興起](../Page/克洛維斯文化.md "wikilink")，估計牠們亦被獵殺至滅絕。

## 參見

[更新世巨型動物群](../Page/更新世巨型動物群.md "wikilink")

## 參考

## 外部連結

  - [U. of Alaska-Fairbanks: Matheus, Ancient fossil offers new clues to
    brown bears
    past](https://web.archive.org/web/20080314193110/http://www.uaf.edu/news/a_news/20041111132847.html)
  - [An artist's impression of an
    Arctodus](http://www.igorn.com/trip2001/pics/092901/beringia3.jpg)
  - [beringia.com](https://web.archive.org/web/20080130135014/http://www.beringia.com/02/02maina4.html)

[Category:眼鏡熊亞科](../Category/眼鏡熊亞科.md "wikilink")
[Category:古動物](../Category/古動物.md "wikilink")
[Category:更新世哺乳類](../Category/更新世哺乳類.md "wikilink")
[Category:北美洲史前哺乳動物](../Category/北美洲史前哺乳動物.md "wikilink")

1.

2.

3.
4.

5.
6.
7.
8.
9.
10.
11.
12.
13.

14.

15. National Geographic Channel, 16 September 2007, *Prehistoric
    Predators: Short-faced bear*, interview with Dr. Paul Matheus

16.

17.

18.
19.
20.