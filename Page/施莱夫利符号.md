[數學中](../Page/數學.md "wikilink")，**施萊夫利符號**（Schläfli
symbol）是一個可以表示一特定[正多胞形或](../Page/正多胞形.md "wikilink")[密鋪圖案若干重要特性的符號](../Page/密鋪.md "wikilink")。其命名是為了紀念19世紀數學家[路德維希·施萊夫利在](../Page/路德維希·施萊夫利.md "wikilink")[幾何和其他領域的許多重要貢獻](../Page/幾何.md "wikilink")。

另見[正多胞形列表](../Page/正多胞形列表.md "wikilink")。

## 正多邊形

一個有*n*個邊的[正多邊形](../Page/正多邊形.md "wikilink")，其施萊夫利符號為\(\{n\}\)。例如，施萊夫利符號為\(\{5\}\)的多邊形即為[正五邊形](../Page/正五邊形.md "wikilink")。

### 正星形多邊形

正指的是正非凸多邊形，即邊長相等的凹多邊形或複雜多邊形。正星形多邊形的施萊夫利符號若為{<sup>*p*</sup>/<sub>*q*</sub>}，表示此一星形多邊形有*p*個角，每一個角都和次*q*的角相連。因此\(\{^{5}/_{2}\}\)即代表的是正[五芒星](../Page/五芒星.md "wikilink")。

### 正星芒形

當*p*和*q*不互質時，此時的正星形多邊形即稱為正星芒形（star
figure）。若*p*跟*q*的最大公因數為*n*，此一正星芒形即是由*n*個\(\{^{^p/_n}/_{^q/_n}\}\)相互旋繞而成。例如，\(\{^6/_2\}\)，即正[六角星](../Page/六角星.md "wikilink")，便是由兩個[正三角形](../Page/正三角形.md "wikilink")\(\{^3/_1\}\)所組成的，而\(\{^{10}/_4\}\)則是由兩個正[五角星所組成](../Page/五角星.md "wikilink")。

## 正多面體

[正多面體的施萊夫利符號計做](../Page/正多面體.md "wikilink"){*p*,*q*}，其中p代表每個[面的边数](../Page/面.md "wikilink")，而q代表[顶点图的边数](../Page/顶点图.md "wikilink")，即每个顶点连接多少条棱。此外，還有三個二維空間[歐氏正堆砌](../Page/鑲嵌_\(幾何學\).md "wikilink")（honeycomb），它們的施萊夫利符號如下:

  - [正四面體](../Page/正四面體.md "wikilink"): {3,3}
  - [正六面體](../Page/正六面體.md "wikilink"): {4,3}
  - [正八面體](../Page/正八面體.md "wikilink"): {3,4}
  - [正十二面體](../Page/正十二面體.md "wikilink"): {5,3}
  - [正二十面體](../Page/正二十面體.md "wikilink"): {3,5}
  - [正三角形鑲嵌](../Page/正三角形鑲嵌.md "wikilink"): {3,6}
  - [正四邊形鑲嵌](../Page/正方形鑲嵌.md "wikilink"): {4,4}
  - [正六邊形鑲嵌](../Page/正六邊形鑲嵌.md "wikilink"): {6,3}

## 四维及以上正多胞形

高维空间多胞形的施莱夫利符号可以通过类比得出，一个n维正多胞形的施莱夫利符号包含n-1个数字。

### 四维正多胞体

[四维正多胞体的施莱夫利符号记做](../Page/四维正多胞体.md "wikilink"){p,q,r},其中{p}为二维面，{p,q}为胞，{q,r}为[顶点图](../Page/顶点图.md "wikilink")，{r}为[棱图](../Page/棱图.md "wikilink")。
四维凸正多胞体共有6种，另有一个三维空间[欧氏正堆砌](../Page/鑲嵌_\(幾何學\).md "wikilink")（honeycomb），它们的施莱夫利符号如下:

  - [正五胞體](../Page/正五胞體.md "wikilink"): {3,3,3}
  - [正八胞體](../Page/超正方體.md "wikilink"): {4,3,3}
  - [正十六胞體](../Page/正十六胞體.md "wikilink"): {3,3,4}
  - [正二十四胞體](../Page/正二十四胞體.md "wikilink"): {3,4,3}
  - [正一百二十胞體](../Page/正一百二十胞體.md "wikilink"): {5,3,3}
  - [正六百胞體](../Page/正六百胞體.md "wikilink"): {3,3,5}
  - [正六面體堆砌](../Page/立方体堆砌.md "wikilink"): {4,3,4}

### 五维及以上正多胞形

在五维及以上空间中只存在三种凸正多胞形，并且五維及以上空間只有一种欧氏正堆砌，其中[单纯形](../Page/单纯形.md "wikilink")（正n+1胞體）的施莱夫利符号为{3,3,3,...,3,3,3}（共n-1个3），[超方形](../Page/超方形.md "wikilink")（正2n胞體）的施莱夫利符号为{4,3,3,...,3,3,3}（共n-2个3），[正轴形](../Page/正轴形.md "wikilink")（正2<sup>n</sup>胞體）的施莱夫利符号为{3,3,3,...,3,3,4}（共n-2个3），[超立方体堆砌的施莱夫利符号为](../Page/超立方体堆砌.md "wikilink"):
{4,3,3,...,3,3,4}（中间共n-3个3）。此外，存在三个四维空間欧氏正堆砌，分别是[正八胞体堆砌](../Page/正八胞体堆砌.md "wikilink"):{4,3,3,4}，[正十六胞体堆砌](../Page/正十六胞体堆砌.md "wikilink"):{3,3,4,3}和[正二十四胞体堆砌](../Page/正二十四胞体堆砌.md "wikilink"):{3,4,3,3}。

## 參考文獻

  - Coxeter, Longuet-Higgins, Miller, *Uniform polyhedra*, **Phil.
    Trans.** 1954, 246 A, 401-50.（Extended Schläfli notation used）

## 外部連結

  - [Wythoff Symbol and generalized Schläfli
    Symbols](https://web.archive.org/web/20060522051432/http://bbs.sachina.pku.edu.cn/Stat/Math_World/math/w/w166.htm)
  - [polyhedral names et
    notations](https://web.archive.org/web/20061216081717/http://www.ac-noumea.nc/maths/amc/polyhedr/names_.htm)

[Category:多胞形](../Category/多胞形.md "wikilink")
[Category:数学表示法](../Category/数学表示法.md "wikilink")
[Category:数学符号](../Category/数学符号.md "wikilink")