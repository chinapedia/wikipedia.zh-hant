**彭丽媛**，[山东](../Page/山东省.md "wikilink")[郓城人](../Page/郓城县.md "wikilink")\[1\]，[中华人民共和国著名](../Page/中华人民共和国.md "wikilink")[女高音](../Page/女高音.md "wikilink")[歌唱家](../Page/歌唱家.md "wikilink")、[歌剧表演艺术家](../Page/歌剧.md "wikilink")，国家[一级演员](../Page/一级演员.md "wikilink")，[解放军文职二级军衔](../Page/中国人民解放军文职干部.md "wikilink")（正军职），[国务院政府特殊津贴专家](../Page/国务院政府特殊津贴.md "wikilink")，当代[民族声乐代表人](../Page/民族声乐.md "wikilink")、[中国民族声乐学派创建者之一](../Page/中国民族声乐学派.md "wikilink")。彭丽媛是现任[中国共产党中央委员会总书记](../Page/中国共产党中央委员会总书记.md "wikilink")[习近平的第二任妻子](../Page/习近平.md "wikilink")，两人1987年在[厦门结婚](../Page/厦门.md "wikilink")，1992年生下独生女儿[习明泽](../Page/习明泽.md "wikilink")。

彭丽媛是中国大陸第一位民族声乐硕士，[北京大学兼职教授](../Page/北京大学.md "wikilink")，[国防大学军事文化学院及](../Page/中国人民解放军国防大学军事文化学院.md "wikilink")[中国音乐学院](../Page/中国音乐学院.md "wikilink")[博士生导师](../Page/教授.md "wikilink")，曾任[中国人民解放军艺术学院院长](../Page/中国人民解放军国防大学军事文化学院.md "wikilink")。

彭丽媛於1980年代初期以《[在希望的田野上](../Page/在希望的田野上.md "wikilink")》成名，其他首唱、主唱曲目包括《[父老乡亲](../Page/父老乡亲.md "wikilink")》、《[珠穆朗玛](../Page/珠穆朗瑪.md "wikilink")》、《[我爱你塞北的雪](../Page/我爱你塞北的雪.md "wikilink")》、《[我们是黄河泰山](../Page/我们是黄河泰山.md "wikilink")》《[沂蒙山小调](../Page/沂蒙山小调.md "wikilink")》《[高天上流云](../Page/高天上流云.md "wikilink")》《[数九寒冬下大雪](../Page/数九寒冬下大雪.md "wikilink")》《[五洲人民齐欢笑](../Page/五洲人民齐欢笑.md "wikilink")》等。2017年4月7日，美國總統[特朗普與習近平會面期間](../Page/特朗普.md "wikilink")，特朗普曾以出色的歌唱家（
talented singer ) 形容彭麗媛。

## 经历

彭丽媛的父亲曾经为[郓城县文化馆馆长](../Page/郓城县.md "wikilink")，母亲则为郓城县豫剧团一位主要戏曲演员。受到母亲影响，她在4至5岁时，就能夠演唱[豫剧选段](../Page/豫剧.md "wikilink")\[2\]。1976年，14岁的彭丽媛考入[山东省郓城第一中学](../Page/山东省郓城第一中学.md "wikilink")，之后又投考加入[山东五七艺术学校](../Page/山东艺术学院.md "wikilink")（1978年底更名为[山东艺术学院](../Page/山东艺术学院.md "wikilink")）中专部，专攻民族声乐\[3\]。

1980年，在[北京参加一次文艺汇演时](../Page/北京市.md "wikilink")，以歌曲《包楞调》和《我的家乡沂蒙山》震动了北京音乐界\[4\]。同年，彭丽媛加入[中国人民解放军](../Page/中国人民解放军.md "wikilink")，成为文艺兵\[5\]。

1982年，彭丽媛开始被中国观众熟悉。当年，在参加中国中央电视台举办的春节文艺晚会时，她演唱了《在希望的田野上》和《我爱你，塞北的雪》，赢得了观众的喜爱，从而也奠定了在中国民族声乐界的地位。\[6\]

1983年，时任[朝鲜](../Page/朝鲜民主主义人民共和国.md "wikilink")[劳动党](../Page/朝鲜劳动党.md "wikilink")[中央政治局常委](../Page/朝鲜劳动党中央委员会政治局常务委员会.md "wikilink")[金正日第一次访华时](../Page/金正日.md "wikilink")，彭丽媛为用中朝双语演唱了朝鲜家喻户晓的歌曲《[卖花姑娘](../Page/卖花姑娘.md "wikilink")》\[7\]。

1984年，彭丽媛由[济南军区](../Page/中国人民解放军济南军区.md "wikilink")[政治部](../Page/中国人民解放军济南军区政治部.md "wikilink")[前卫歌舞团调入](../Page/中国人民解放军济南军区政治部前卫歌舞团.md "wikilink")[总政歌舞团](../Page/中国人民解放军文化艺术中心文艺部.md "wikilink")\[8\]，次年赶赴[老山战役前线慰问将士](../Page/兩山戰役.md "wikilink")\[9\]。不久，彭丽媛投考加入[中国音乐学院声乐系](../Page/中国音乐学院.md "wikilink")；从[大专读起](../Page/專科學校.md "wikilink")，先后师从声乐教育家[王音旋和](../Page/王音旋.md "wikilink")[金铁霖教授](../Page/金铁霖.md "wikilink")\[10\]。

1985年，彭丽媛获得[文化部举办的第一届全国](../Page/中华人民共和国文化部.md "wikilink")[聂耳](../Page/聂耳.md "wikilink")、星海声乐作品比赛民族唱法组金奖。同年7月，她加入中国共产党\[11\]。1986年，她又在[中央电视台主办的第二届全国青年歌手大奖赛获得专业民族歌唱一等奖](../Page/中国中央电视台.md "wikilink")。\[12\]

1990年5月，通过论文答辩，获得硕士学位，成为中国培养的首位民族声乐硕士\[13\]。

2002年，[山东艺术学院聘彭丽媛为客座教授](../Page/山东艺术学院.md "wikilink")。2004年9月，[中国音乐学院也聘请她为该校的客座教授](../Page/中国音乐学院.md "wikilink")。\[14\]

2005年9月，应[联合国成立](../Page/联合国.md "wikilink")60周年组委会邀请，彭丽媛在[美国](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")[林肯艺术中心首次演出中国歌剧](../Page/林肯表演藝術中心.md "wikilink")《[木兰诗篇](../Page/木兰诗篇.md "wikilink")》，获得林肯艺术中心艺术委员会颁发杰出艺术家奖。\[15\]

2007年11月，[中共中央宣传部](../Page/中共中央宣传部.md "wikilink")、[人事部和](../Page/中华人民共和国人事部.md "wikilink")[中国文联授予彭丽媛等](../Page/中国文学艺术界联合会.md "wikilink")56人“全国中青年德艺双馨文艺工作者”的荣誉称号\[16\]。

2011年6月，彭丽媛被[世界卫生组织任命为该组织的](../Page/世界卫生组织.md "wikilink")“[结核病和](../Page/结核病.md "wikilink")[艾滋病防治亲善大使](../Page/艾滋病.md "wikilink")”。\[17\]此外，彭丽媛为第8至11届[全国政协委员](../Page/中国人民政治协商会议全国委员会.md "wikilink")\[18\]、[中华全国青年联合会常委和](../Page/中华全国青年联合会.md "wikilink")[中国音乐家协会副主席](../Page/中国音乐家协会.md "wikilink")。

2012年起，彭丽媛担任[解放军艺术学院院长](../Page/解放军艺术学院.md "wikilink")。2017年4月中旬，该院正式更名为[国防大学军事文化学院](../Page/国防大学军事文化学院.md "wikilink")，彭丽媛不担任更名后的国防大学军事文化学院院长，而是调往中央军委政治工作部评审委员会任高级评委。新任院长传由原军艺副院长[张启超担任](../Page/张启超.md "wikilink")。\[19\]

目前，她是“世界卫生组织爱滋病防治亲善大使”及“中国控烟形象大使”。近年，彭丽媛也多次陪同[习近平外访](../Page/习近平.md "wikilink")。

## 经典作品

|- |

  - 《[天时地利人和](../Page/天时地利人和.md "wikilink")》
  - 《[在希望的田野上](../Page/在希望的田野上.md "wikilink")》
  - 《[沂蒙山小调](../Page/沂蒙山小调.md "wikilink")》
  - 《[我爱你塞北的雪](../Page/我爱你塞北的雪.md "wikilink")》
  - 《[高天上流云](../Page/高天上流云.md "wikilink")》
  - 《[我们是黄河泰山](../Page/我们是黄河泰山.md "wikilink")》
  - 《[父老乡亲](../Page/父老乡亲.md "wikilink")》
  - 《[白发亲娘](../Page/白发亲娘.md "wikilink")》
  - 《[珠穆朗玛](../Page/珠穆朗玛.md "wikilink")》
  - 《[说聊斋](../Page/说聊斋.md "wikilink")》
  - 《[江山](../Page/江山\(歌曲\).md "wikilink")》

|

  - 《[阳光路上](../Page/阳光路上.md "wikilink")》
  - 《[红梅赞](../Page/红梅赞.md "wikilink")》
  - 《[珊瑚颂](../Page/珊瑚颂.md "wikilink")》
  - 《[旗帜颂](../Page/旗帜颂.md "wikilink")》
  - 《[清粼粼的水蓝莹莹的天](../Page/清粼粼的水蓝莹莹的天.md "wikilink")》
  - 《[嘎达梅林](../Page/嘎达梅林.md "wikilink")》
  - 《[当那一天来临](../Page/当那一天来临.md "wikilink")》
  - [歌剧](../Page/歌剧.md "wikilink")《[木兰诗篇](../Page/木兰诗篇.md "wikilink")》
  - [豫剧](../Page/豫剧.md "wikilink")《[花木兰](../Page/花木兰_\(豫剧\).md "wikilink")》

|}

## 奖项

  - 首届全国[聂耳](../Page/聂耳.md "wikilink")、[冼星海声乐作品比赛民族唱法组金牌](../Page/冼星海.md "wikilink")\[20\]
  - 第二届[CCTV全国青年歌手电视大奖赛专业组民族唱法第一名](../Page/全国青年歌手电视大奖赛.md "wikilink")\[21\]
  - 第三屆[梅花奖](../Page/中國戲劇梅花獎.md "wikilink")\[22\]
  - [林肯艺术中心艺术委员会颁发杰出艺术家奖](../Page/林肯中心.md "wikilink")\[23\]
  - 第26届[世界青年联欢节金奖](../Page/世界青年联欢节.md "wikilink")，声乐排名首位。

## 相关争议与评论

1989年[六四事件以武力镇压后](../Page/六四事件.md "wikilink")，彭丽媛身着军装、手持麦克风在[天安门广场为持枪及戴头盔的戒严部队献唱](../Page/天安门广场.md "wikilink")，当时的情景成了《解放军画报》的封底照片。这张照片在互联网与海外流传，香港媒体评论说「中共六四屠杀后，彭丽媛去唱歌慰问戒严部队，《[开放杂志](../Page/开放杂志.md "wikilink")》发表过这张照片」，但在新浪微博贴出不久就被删除，贴图者被关闭帐号，国外媒体说「令中国尴尬的照片」\[24\]\[25\]\[26\]\[27\]。美联社报道，儿子死于六四的中国音乐学院退休教师王范地说：“她是体制内的人，如果军队要她演唱，她不得不去。”“重要的是未来发生什么。”[香港城市大學政治学者](../Page/香港城市大學.md "wikilink")[鄭宇碩表示](../Page/鄭宇碩.md "wikilink")，这张照片的负面影响大概在国际上会比在中国国内大，这种形象可能引起人们对习近平是否想要改革的疑问。\[28\]\[29\]

2013年6月，[美國第一夫人](../Page/美國第一夫人.md "wikilink")[米歇爾·奧巴馬留在華府而未赴加州會見彭麗媛](../Page/米歇爾·奧巴馬.md "wikilink")，《[外交政策](../Page/外交政策_\(杂志\).md "wikilink")》期刊認為米歇爾「選擇正確」（made
the right
choice），因為彭麗媛不過是個唱宣傳政令的解放軍少將，米歇爾根本不該與在血腥鎮壓後於天安門廣場以歌唱支持軍隊的她見面。\[30\]\[31\]\[32\]然而翌年3月下旬，米歇尔·奥巴马受彭丽媛邀请访华，长达一周时间\[33\]。
[缩略图](https://zh.wikipedia.org/wiki/File:Liyuan_Primary_School.jpg "fig:缩略图")[菏泽市](../Page/菏泽市.md "wikilink")[郓城县](../Page/郓城县.md "wikilink")[张营镇后彭庄村捐建的丽媛小学](../Page/张营镇_\(郓城县\).md "wikilink")（2015年5月）\]\]

## 家庭成员

  - 丈夫：[中国共产党现任](../Page/中国共产党.md "wikilink")[中央委员会总书记](../Page/中国共产党中央委员会总书记.md "wikilink")[习近平](../Page/习近平.md "wikilink")。是习近平第二任妻子，两人於1986年年底經朋友介紹而相識，於1987年9月1日在[廈門结婚](../Page/厦门市.md "wikilink")，
      - 女儿：[习明泽](../Page/习明泽.md "wikilink")，生于1992年6月27日，曾就讀[杭州外国语学校](../Page/杭州外国语学校.md "wikilink")、[浙江大学](../Page/浙江大学.md "wikilink")，后转入[美国](../Page/美国.md "wikilink")[哈佛大学](../Page/哈佛大学.md "wikilink")。哈佛大學退休教授[傅高義透露習明澤已於](../Page/傅高义.md "wikilink")2014年學成歸國\[34\]。
  - 父亲：[彭龙坤](../Page/彭龙坤.md "wikilink")，曾經在[郓城县文化馆工作](../Page/郓城县.md "wikilink")，2009年病逝。
  - 母亲：為郓城县[豫剧团主要戏曲演员](../Page/豫剧.md "wikilink")。2009年過世\[35\]\[36\]
      - 妹妹：彭丽娟，曾经为彭丽媛的经纪人，传闻为[吉利汽车老板](../Page/吉利汽车.md "wikilink")[李书福之妻](../Page/李书福.md "wikilink")（此传闻无任何依据，另据港交所最新股權披露，揭曉李書福妻子的姓名實為「Li
        WANG」，有別於早前內地傳出的「彭麗娟」）；
      - 弟弟：彭蕾（彭雷）。
  - 舅舅：李新凱，山東流亡學生，出生於1930年，具有[荣民身份](../Page/榮譽國民.md "wikilink")。經歷[澎湖七一三事件](../Page/澎湖七一三事件.md "wikilink")；之後入伍擔任[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[士官](../Page/士官.md "wikilink")、退伍後擔任過嘉義梅山及竹崎等多所國小教師、後再轉任嘉義市[精忠國小美術科專任教師](../Page/精忠國小.md "wikilink")。退休後，與女兒及女婿一起居住在嘉義市[精忠一村附近](../Page/精忠一村.md "wikilink")、臨近精忠國小。2016年11月10日於[嘉義基督教醫院](../Page/嘉義基督教醫院.md "wikilink")[肺炎病逝](../Page/肺炎.md "wikilink")，享壽86歲，在嘉義[長庚醫院設立靈堂](../Page/長庚醫療財團法人.md "wikilink")，並[嘉義市殯葬管理所](../Page/嘉義市殯葬管理所.md "wikilink")（水上殯儀館）辦理身後事。李新凱長兄之子李強、以及彭麗媛弟弟彭蕾（磊）於11月22日赴台湾奔喪；由李強為靈柩[封丁](../Page/封丁.md "wikilink")。）\[37\]\[38\]\[39\]\[40\]\[41\]\[42\]

## 参见

  - [中国音乐学院](../Page/中国音乐学院.md "wikilink")、[中国人民解放军总政治部歌舞团](../Page/中国人民解放军文化艺术中心文艺部.md "wikilink")、[中国人民解放军艺术学院](../Page/中国人民解放军国防大学军事文化学院.md "wikilink")
  - [民族唱法](../Page/民族唱法.md "wikilink")、[金铁霖](../Page/金铁霖.md "wikilink")、[宋祖英](../Page/宋祖英.md "wikilink")
  - 《[在希望的田野上](../Page/在希望的田野上.md "wikilink")》
  - [习近平](../Page/习近平.md "wikilink")、《[习大大爱着彭麻麻](../Page/习大大爱着彭麻麻.md "wikilink")》
  - 中国名画《[青年女歌手](../Page/青年女歌手.md "wikilink")》

## 注释

## 参考文献

## 外部链接

  -
  -
{{-}}

[彭丽媛](../Category/彭丽媛.md "wikilink") [L丽媛](../Category/彭姓.md "wikilink")
[P](../Category/中國女歌手.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[P](../Category/一级演员.md "wikilink")
[P](../Category/中国人民解放军女性少将.md "wikilink")
[P](../Category/中国人民解放军二级文职干部.md "wikilink")
[Category:中国人民解放军艺术学院院长](../Category/中国人民解放军艺术学院院长.md "wikilink")
[P](../Category/第八屆全國政協委員.md "wikilink")
[P](../Category/第九屆全國政協委員.md "wikilink")
[P](../Category/第十屆全國政協委員.md "wikilink")
[P](../Category/第十一届全国政协委员.md "wikilink")
[P](../Category/北京大學教授.md "wikilink")
[Category:山东艺术学院校友](../Category/山东艺术学院校友.md "wikilink")
[P](../Category/中国音乐学院校友.md "wikilink")
[P](../Category/中国共产党中央委员会总书记夫人.md "wikilink")
[6](../Category/中华人民共和国主席夫人.md "wikilink")
[Category:習近平](../Category/習近平.md "wikilink")
[Category:习仲勋家族](../Category/习仲勋家族.md "wikilink")
[P](../Category/郓城人.md "wikilink")
[Category:中國音樂家協會副主席](../Category/中國音樂家協會副主席.md "wikilink")
[Category:茱莉亞學院校友](../Category/茱莉亞學院校友.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")

1.
2.  [从彭丽媛的歌唱谈气质在声乐作品演唱中的作用](http://www.riversong.cn/asp/articleview.asp?7665)
    黄河之声

3.

4.
5.

6.
7.

8.  [彭丽媛专辑《我的士兵兄弟》出版发行](http://news.xinhuanet.com/mil/2007-11/28/content_7161203.htm)
    新华网

9.

10.

11.

12.
13.

14.

15.

16.

17. [世卫组织任命彭丽媛为结核病和艾滋病防治亲善大使](http://news.xinhuanet.com/2011-06/04/c_121494531.htm)
    新华网

18.

19. [彭丽媛卸任军艺院长
    专注做“第一夫人”](https://www.zaobao.com/realtime/china/story20170726-782119)
    联合早报

20.
21.
22.

23.
24.

25.

26.

27.

28.

29.

30.

31.

32.

33. [米歇尔访华](http://news.ifeng.com/mainland/special/michelle/)，[凤凰网专题报道](../Page/凤凰网.md "wikilink")

34.

35.

36.

37.

38.

39.

40.

41.

42.