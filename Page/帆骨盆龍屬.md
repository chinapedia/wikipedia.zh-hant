**帆骨盆龍屬**（屬名：*Pteropelyx*）是[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[疑名](../Page/疑名.md "wikilink")，化石發現於[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[朱迪斯河組](../Page/朱迪斯河組.md "wikilink")，年代為晚[白堊紀](../Page/白堊紀.md "wikilink")，由[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward
Drinker Cope）在1889年所命名。[模式種是](../Page/模式種.md "wikilink")*P. grallipes*。

在分類歷史中，有數個種被歸類於帆骨盆龍，但全部都是根據破碎的化石，沒有足夠證據可支持這些歸類。大部分的種可能屬於其他著名鴨嘴龍類，例如[賴氏龍](../Page/賴氏龍.md "wikilink")、[格理芬龍](../Page/格理芬龍.md "wikilink")。帆骨盆龍的[模式標本是一個缺乏頭顱骨的骨骸](../Page/模式標本.md "wikilink")，可能來自於[冠龍](../Page/冠龍.md "wikilink")，使得帆骨盆龍成為[首同物異名](../Page/首同物異名.md "wikilink")，但該標本缺乏頭顱骨，所以不能確定帆骨盆龍是否為[異名](../Page/異名.md "wikilink")。

## 參考資料

  - Brett-Surman, M.K., 1989. A revision of the Hadrosauridae (Reptilia:
    Ornithischia) and their evolution during the Campanian and
    Maastrichtian. Ph.D. dissertation, George Washington University,
    Washington, D.C.. pp.1-272.
  - Cope, E.D. 1889. Notes on the Dinosauria of the Laramie. *The
    American Naturalist* 23:904-906.

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [Entry from The Dinosaus
    Encyclopaedia](http://www.dinoruss.com/de_4/5a92793.htm)

[Category:鴨嘴龍科](../Category/鴨嘴龍科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")