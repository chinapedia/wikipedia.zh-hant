[Nuclear_family_member_older_brother.svg](https://zh.wikipedia.org/wiki/File:Nuclear_family_member_older_brother.svg "fig:Nuclear_family_member_older_brother.svg")
**兄**，[漢語口語多稱](../Page/漢語.md "wikilink")**哥**或**哥哥**，是對[父或](../Page/父.md "wikilink")[母所生而比自己年齡大的](../Page/母.md "wikilink")[男性](../Page/男性.md "wikilink")[血親稱呼](../Page/血親.md "wikilink")，有時泛指一切男性同[辈兄长](../Page/辈.md "wikilink")（如[堂兄弟](../Page/堂兄弟.md "wikilink")、[表兄弟](../Page/表兄弟.md "wikilink")），为了[谦虚也称所有男性同辈为](../Page/谦虚.md "wikilink")「兄」\[1\]（事实上年龄可能比自己小，如「謝兄」、「周兄」等）。由于男性兄长具有的[权威](../Page/权威.md "wikilink")，引申出[领导者含义](../Page/领导者.md "wikilink")，**大哥**，**老大**，**大佬**，在民间普遍使用。当父亲死亡或失势的时候，有长兄为父的习俗。

## 「哥」词源

**哥**在汉语中本来是“歌”（[唱歌](../Page/唱歌.md "wikilink")）的本字，而从[甲骨文起](../Page/甲骨文.md "wikilink")“兄”便是表示现在的“哥”义。在[东汉前尚未见](../Page/东汉.md "wikilink")“歌”字。

“哥”字不作“歌”是从[唐朝开始的](../Page/唐朝.md "wikilink")。《[旧唐书](../Page/旧唐书.md "wikilink")·王（土局）传》“玄宗泣曰：‘四哥孝仁……’”，四哥是指[玄宗之父](../Page/唐玄宗.md "wikilink")[睿宗](../Page/李旦.md "wikilink")。玄宗的儿[李琰也称其父为](../Page/李琰_\(唐棣王\).md "wikilink")“三哥”。玄宗称[李宪](../Page/李宪.md "wikilink")（睿宗长子）为大哥，又是以“哥”称兄。而《[淳化阁帖](../Page/淳化阁帖.md "wikilink")》中[唐太宗居然对其儿子](../Page/唐太宗.md "wikilink")[李治自称](../Page/李治.md "wikilink")“哥哥[敕](../Page/敕.md "wikilink")”，这又是以“哥”作为父亲的自称。同时代的[白居易在](../Page/白居易.md "wikilink")《祭浮梁大兄文》中也出现了以“哥”称兄。由此可见当时“哥”可以兼指父兄。有意思的是，这种用法至今在[晋语](../Page/晋语.md "wikilink")[并州片的](../Page/并州片.md "wikilink")[平遥](../Page/平遥.md "wikilink")、[文水等地方言中还存在](../Page/文水.md "wikilink")（读音略有区别）。

[南北朝](../Page/南北朝.md "wikilink")、唐是民族大杂居时期，[北方民族的一些习俗称谓也潜入汉语](../Page/北方民族.md "wikilink")。过去北方民族与汉族交战，并未进入中原与汉族混合生活，惟有[北魏鲜卑族是第一次](../Page/北魏.md "wikilink")，而且历时较久，[拓跋政权前后共](../Page/拓跋.md "wikilink")165年。

[鲜卑语中有](../Page/鲜卑语.md "wikilink")“阿干”一词，父与兄鲜卑语都可以用“阿干”相称。现在的[哈萨克语比较接近古](../Page/哈萨克语.md "wikilink")[突厥语](../Page/突厥语.md "wikilink")，在面称时，哈萨克语
agha一词既可称父也可称兄，与鲜卑一致。在突厥语族甚至[阿尔泰语系中称兄都是同一系列的语言形式](../Page/阿尔泰语系.md "wikilink")：agha(a)、aka(a)、aqa(a)，其中鼻尾是不稳定的。如蒙古语akan,
axan,
ax、满语age、维吾尔语aka\[2\]。由此可以推测，在北魏时这种称呼已经渗入汉族，经过一段时间的融合，变为汉语中活生生的词，“哥”在后世已经汉化，成为完全汉语化的根词形式。

对于“哥”兼指父兄，有学者指出这可能和[游牧民族的](../Page/游牧民族.md "wikilink")[婚姻制度有关](../Page/婚姻.md "wikilink")。他们的传统是，父死则妻其后母，兄死则妻其嫂，草原文化中的长幼观念和中原也不太一样。中原传统对此一直难以接受，一概归之以“乱伦”。

“哥”现在已是最普通的兄称，“兄”反居其次，只有[朝鮮語還保留以](../Page/朝鮮語.md "wikilink")“兄”作主要的稱謂。当然“兄”和“哥”也是各有分配的，“兄”比较书面化，“哥”，“哥哥”以及“阿哥”则比较口语化。民间仍普遍有「[老兄](../Page/老兄.md "wikilink")」的稱呼。

## 家庭角色

## 其他

  - 由嫡亲哥哥、到[表哥](../Page/表哥.md "wikilink")、[堂哥之类亲戚不胜枚举](../Page/堂哥.md "wikilink")。

<!-- end list -->

  - **阿哥**（），是某些汉语方言区（[吳](../Page/吳語.md "wikilink")、[闽](../Page/闽.md "wikilink")、[客家](../Page/客家.md "wikilink")、[廣東等](../Page/廣東.md "wikilink")）对兄长称谓，「[阿](../Page/阿.md "wikilink")」是[昵称前缀](../Page/昵称.md "wikilink")。但[閩南語亦常用](../Page/閩南語.md "wikilink")「阿兄」；阿哥也可以作为一般称呼。
  - **大哥**，不少民间团体特别是[黑社会对其领导者的称呼](../Page/黑社会.md "wikilink")。
  - **大哥哥**，小孩稱呼陌生少年少年男子。
  - [太平天國以](../Page/太平天國.md "wikilink")[耶稣为天兄](../Page/耶稣.md "wikilink")，称**大哥**，在定都天京后禁止任何人称大哥，犯禁者[斩首不留](../Page/斩首.md "wikilink")。

### 现代变异

  - **哥**，中國大陸網路用語，用哥來自稱或稱其他男性，使用時單稱一字“哥”。「哥」也常被使用於韓國文化中，作為稱呼年長男性（多見女性稱呼男性）的綴詞。
  - **哥哥**（gorgor），歌迷影迷对[张国荣的尊称](../Page/张国荣.md "wikilink")。
  - **哥哥**，**哥**，在某些情况下指男[同性戀](../Page/同性戀.md "wikilink")。
  - **葛格**，哥哥的音變，台灣常見網路用語。也被[男同性戀普遍使用](../Page/男同性戀.md "wikilink")。
  - **大佬**，見於[粵語使用區](../Page/粵語.md "wikilink")，佬是對成年男性的稱呼，並無貶義。大佬用作[黑社会对其领导者的称呼](../Page/黑社会.md "wikilink")。
  - **大老**，台湾常用政党术语，指党内派系主要人物。也可指[黑社会领导者](../Page/黑社会.md "wikilink")。

## 相關條目

  - [平輩](../Page/平輩.md "wikilink")
  - [弟](../Page/弟.md "wikilink")、[姊](../Page/姊.md "wikilink")、[妹](../Page/妹.md "wikilink")
  - [兄貴](../Page/兄貴.md "wikilink")
  - [大哥大](../Page/大哥大.md "wikilink")
  - [老大哥](../Page/老大哥.md "wikilink")

## 附註

[X](../Category/男性亲属称谓.md "wikilink")

1.  如[元好問詩](../Page/元好問.md "wikilink")《[寄欽止李兄](../Page/s:寄欽止李兄.md "wikilink")》
2.  有漢譯作“阿卡”，見[阿凡提的故事](../Page/阿凡提的故事.md "wikilink")。