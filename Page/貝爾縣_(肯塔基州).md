**貝爾縣**（**Bell County,
Kentucky**）位[美國](../Page/美國.md "wikilink")[肯塔基州東南角的一個縣](../Page/肯塔基州.md "wikilink")，東鄰[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")，南鄰[田納西州](../Page/田納西州.md "wikilink")。面積936平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口30,060人。縣治[派恩維爾](../Page/派恩維爾_\(肯塔基州\).md "wikilink")（Pineville）。

成立於1867年2月28日，當時稱為**Josh
Bell**縣，1873年改名。縣名紀念第三任[聯邦眾議員](../Page/美國眾議院.md "wikilink")[約書亞·F·貝爾](../Page/約書亞·F·貝爾.md "wikilink")
（Joshua Fry Bell）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/肯塔基州行政区划.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.