**爱尔兰共和军**（[英文](../Page/英文.md "wikilink")：Irish Republican
Army，簡稱**IRA**；[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：**），曾为[爱尔兰独立](../Page/爱尔兰独立.md "wikilink")，後为统一[北爱尔兰而战斗的组织](../Page/北爱尔兰.md "wikilink")。中文圈常指[臨時派愛爾蘭共和軍](../Page/臨時派愛爾蘭共和軍.md "wikilink")，但以下組織皆稱為愛爾蘭共和軍：

  - （老共和軍）

  - （反條約共和軍）

  -
  - [臨時愛爾蘭共和軍](../Page/臨時愛爾蘭共和軍.md "wikilink")（北愛共和軍）

  -
  -
## 歷史

1919年，旨在建立独立的[爱尔兰共和国的](../Page/爱尔兰共和国.md "wikilink")[民族主义](../Page/民族主义.md "wikilink")[军事组织](../Page/军事.md "wikilink")改制成爱尔兰共和军，並發動[愛爾蘭獨立戰爭](../Page/爱尔兰独立战争.md "wikilink")（1919年1月21日—1921年7月11日）。

1922年，反对將爱尔兰分割成愛爾蘭共和國與北愛爾蘭的《[英爱条约](../Page/英爱条约.md "wikilink")》的新爱尔兰共和军宣布继续为实现南北统一而斗争，并实行[暴力活动](../Page/暴力.md "wikilink")；而支持條約的愛爾蘭共和軍則演變為。雙方隨後展開[愛爾蘭內戰](../Page/爱尔兰内战.md "wikilink")（1922年6月28日—1923年5月24日）。

1939年，爱尔兰共和军开始在[英国制造](../Page/英国.md "wikilink")[爆炸事件](../Page/爆炸.md "wikilink")，被爱尔兰共和國政府和[英国政府取缔](../Page/英国政府.md "wikilink")。

1969年，爱尔兰共和军分为和[临时派](../Page/臨時派愛爾蘭共和軍.md "wikilink")。正式派自称信奉[马克思主义并放弃武裝抗爭](../Page/马克思主义.md "wikilink")，转向非暴力斗争；其政治組織正統新芬黨后改组為[工人黨](../Page/工人黨_\(愛爾蘭\).md "wikilink")。临时派坚持武裝抗爭，攻擊活動至1997年停止，且思想漸左並活躍於政治活動上，其政治組織為[新芬黨](../Page/新芬黨.md "wikilink")。

由于爱尔兰共和军使用恐怖袭击手段，被许多[国家视为](../Page/国家.md "wikilink")[恐怖组织](../Page/恐怖组织.md "wikilink")。

1980年代，爱尔兰共和军曾遭首相[撒切尔夫人镇压及強硬應對衝突](../Page/玛格丽特·撒切尔.md "wikilink")，她也曾被爱尔兰共和军襲擊，但避過一劫。

1997年，對北愛爾蘭問題態度溫和的工黨上台後，工黨允諾北愛爾蘭可以組建自治政府並簽下[貝爾法斯特協議](../Page/贝尔法斯特协议.md "wikilink")，共和軍隨後宣佈停火，並由[美國](../Page/美國.md "wikilink")、[加拿大和](../Page/加拿大.md "wikilink")[芬蘭組織的監督隊伍監察愛爾蘭共和軍的棄械進程](../Page/芬蘭.md "wikilink")。

2005年7月28日，愛爾蘭共和軍正式下令终止武装斗争。它要求所有的爱尔兰共和军单位必须放弃使用武器，只通过[和平的方式](../Page/和平.md "wikilink")，协助发展纯[政治和](../Page/政治.md "wikilink")[民主计划](../Page/民主.md "wikilink")。2005年9月26日，監督組織宣布愛爾蘭共和軍已經完成所有棄械過程，全部武器被列為「永久不可使用」或「永久無用」。

目前包括從臨時派分裂出來的（Continuity IRA）、（Real IRA）等仍有零星活動。

## 愛爾蘭共和軍的分裂與演變

  - ''' '''(1917–22)，俗稱「老共和軍」（Old
    IRA），由發展而來。在[愛爾蘭獨立戰爭結束並簽訂](../Page/爱尔兰独立战争.md "wikilink")《英愛條約》之後，分裂為：
      - ****，在[愛爾蘭內戰期間改組為](../Page/爱尔兰内战.md "wikilink")（National
        Army），為[愛爾蘭國防軍](../Page/愛爾蘭國防軍.md "wikilink")（Defence
        Forces）之前身。其支持團體之後整合成為今日的[愛爾蘭統一黨](../Page/愛爾蘭統一黨.md "wikilink")。
      - ****，其支持者為當時[新芬黨內的由](../Page/新芬黨.md "wikilink")[埃蒙·德·瓦莱拉所領導的反停戰協議派](../Page/埃蒙·德·瓦莱拉.md "wikilink")。（瓦莱拉於1926年離開新芬黨，並建立[愛爾蘭共和黨](../Page/共和黨_\(愛爾蘭\).md "wikilink")。）反條約派共和軍與部分老共和軍於1930年代整合，並於愛爾蘭和英國進行軍事活動。新芬黨亦在此期間與共和軍重建關係。反英愛條約派共和軍在1969年再度分裂為：
          - ****（Official Irish Republican
            Army，OIRA）與正統新芬黨，思想左傾走向[馬克思主義並主張與英國停戰](../Page/马克思主义.md "wikilink")。正統共和軍於1972年停火，而正統新芬黨則改名為新芬工人黨，後又於1982年再度改名為[愛爾蘭工人黨](../Page/工人党_\(爱尔兰\).md "wikilink")。
              - 1974年，正統共和軍與正統新芬黨內的極左派出走並分別組成[愛爾蘭民族解放軍](../Page/爱尔兰民族解放军.md "wikilink")（Irish
                National Liberation
                Army，INLA）與[愛爾蘭共和社會黨](../Page/爱尔兰共和社会党.md "wikilink")。
                  - 1987年，INLA再度分裂出（Irish People's Liberation
                    Organisation，IPLO；1986–1992）。愛爾蘭工人黨則於1992年分裂出[民主左派](../Page/民主左派.md "wikilink")。（民主左派於1999年合併至[愛爾蘭工黨](../Page/工党_\(爱尔兰\).md "wikilink")。）
          - **[臨時派愛爾蘭共和軍](../Page/臨時派愛爾蘭共和軍.md "wikilink")**（Provisional
            Irish Republican
            Army，PIRA；一般稱為IRA，通常稱為**北愛爾蘭共和軍**或簡稱**北愛共和軍**）與[新芬黨](../Page/新芬黨.md "wikilink")，相較於正統派的馬克思主義，臨時派承襲了傳統的北愛爾蘭共和運動。北愛共和軍主要活動範圍在北愛爾蘭，直到2005年才停火。其領導核心團體為。
              - 1986年，反對新芬黨放棄的人士出走，另組****（Continuity Irish Republican
                Army，CIRA）與[共和新芬黨](../Page/共和新芬黨.md "wikilink")。之後又於2002年分裂出（Irish
                Republican Liberation Army，IRLA）。
              - 1997年，反對和談的人士出走，另組****（Real Irish Republican
                Army，RIRA），其政治組織為（32 County Sovereignty
                Movement，32CSM）。

## 參見

  - [北愛爾蘭問題](../Page/北愛爾蘭問題.md "wikilink")
  - [赌注之刀](../Page/赌注之刀.md "wikilink")

## 外部連結

  -
[爱尔兰共和军](../Category/爱尔兰共和军.md "wikilink")
[Category:反英情緒](../Category/反英情緒.md "wikilink")
[Category:同類索引](../Category/同類索引.md "wikilink")