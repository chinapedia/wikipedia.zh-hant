**2000年國際足協世界足球先生**，由[法国球员](../Page/法国.md "wikilink")[齐内丁·齐达内获得](../Page/齐内丁·齐达内.md "wikilink")，这是他代表[法国队获得](../Page/法国国家足球队.md "wikilink")2000年[欧洲杯冠军后](../Page/欧洲杯.md "wikilink")，第二次当选[世界足球先生](../Page/世界足球先生.md "wikilink")。

## 投票结果

|    |                                                                                                                                                                                                     |     |     |     |     |
| -- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --- | --- | --- | --- |
|    | 球员姓名                                                                                                                                                                                                | 5分票 | 3分票 | 1分票 | 总分  |
| 1  | [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") [齐内丁·齐达内](../Page/齐内丁·齐达内.md "wikilink")（Zinedine Zidane）                                    | 59  | 21  | 12  | 370 |
| 2  | [Flag_of_Portugal.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Portugal.svg "fig:Flag_of_Portugal.svg") [路易斯·菲戈](../Page/路易斯·菲戈.md "wikilink")（Luis Figo）                                      | 38  | 36  | 31  | 329 |
| 3  | [Flag_of_Brazil.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Brazil.svg "fig:Flag_of_Brazil.svg") [里瓦尔多](../Page/里瓦尔多.md "wikilink")（Rivaldo）                                                  | 22  | 43  | 24  | 263 |
| 4  | [Flag_of_Argentina.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Argentina.svg "fig:Flag_of_Argentina.svg") [巴蒂斯图塔](../Page/加布里埃尔·巴蒂斯图塔.md "wikilink")（Gabriel Batistuta）                       | 7   | 6   | 4   | 57  |
| 5  | [Flag_of_Ukraine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg "fig:Flag_of_Ukraine.svg") [安德烈·舍甫琴科](../Page/安德烈·舍甫琴科.md "wikilink")（Andriy Shevchenko）                             | 5   | 6   | 5   | 48  |
| 6  | [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg") [大卫·贝克汉姆](../Page/大卫·贝克汉姆.md "wikilink")（David Beckham）                                   | 4   | 3   | 12  | 41  |
| 7  | [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg") [蒂埃里·亨利](../Page/蒂埃里·亨利.md "wikilink")（Thierry Henry）                                        | 3   | 3   | 11  | 35  |
| 8  | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [亚历山德罗·内斯塔](../Page/亚历山德罗·内斯塔.md "wikilink")（Alessandro Nesta）                                  | 0   | 6   | 5   | 23  |
| 9  | [Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg") [帕特里克·克鲁伊维特](../Page/帕特里克·克鲁伊维特.md "wikilink")（Patrick Kluivert） | 1   | 5   | 2   | 22  |
| 10 | [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [-{zh-hans:弗朗切斯科·托蒂; zh-hant:托迪;}-](../Page/托迪.md "wikilink")（Francesco Totti）                  | 2   | 0   | 4   | 14  |
| 并列 | [Flag_of_Spain.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Spain.svg "fig:Flag_of_Spain.svg") [劳尔·冈萨雷斯](../Page/劳尔·冈萨雷斯.md "wikilink")（Raul Gonzalez）                                         | 1   | 1   | 6   | 14  |

## 参见

  - [世界足球先生](../Page/世界足球先生.md "wikilink")

[Category:世界足球先生](../Category/世界足球先生.md "wikilink")
[Category:2000年足球](../Category/2000年足球.md "wikilink")