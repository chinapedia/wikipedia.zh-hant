**濟陰郡**，[中国古](../Page/中国.md "wikilink")[郡名](../Page/郡.md "wikilink")，[治所](../Page/治所.md "wikilink")[濟陰县在今](../Page/濟陰县.md "wikilink")[山东省](../Page/山东省.md "wikilink")[定陶县](../Page/定陶县.md "wikilink")。辖境约当今山东省菏泽、定陶、东明、鄄城等市县地及成武、巨野等县部分地区，北至濮阳地区。

## 漢代濟陰郡

[西汉时为](../Page/西汉.md "wikilink")[梁国](../Page/梁国_\(汉朝\).md "wikilink")。[汉景帝](../Page/汉景帝.md "wikilink")[中元六年](../Page/中元.md "wikilink")（前144年），分梁国置**[济阴国](../Page/济阴国.md "wikilink")**，治所在[定陶县](../Page/定陶县.md "wikilink")（今山东省定陶县西北）。旋国除，[建元三年](../Page/建元_\(西汉\).md "wikilink")（前138年）改濟陰郡，後又改為**[定陶國](../Page/定陶國.md "wikilink")**，为[兖州四郡之一](../Page/兖州_\(古代\).md "wikilink")，辖六县。汉景帝立济阴太守之职。

[三国时](../Page/三国.md "wikilink")[曹操起兵反](../Page/曹操.md "wikilink")[董卓之地](../Page/董卓.md "wikilink")。[晋朝時仍处於兖州治下](../Page/晋朝.md "wikilink")\[1\]。[北魏移治左城](../Page/北魏.md "wikilink")（今山东曹县西北），辖境缩小。[北周改称](../Page/北周.md "wikilink")[曹州](../Page/曹州.md "wikilink")。

## 隋代濟陰郡

[隋炀帝时](../Page/隋炀帝.md "wikilink")，實行廢州改郡制，曹州改為濟陰郡。户十四万九百四十八，下领九县：[济阴县](../Page/济阴县.md "wikilink")、[外黄县](../Page/外黄县.md "wikilink")、[济阳县](../Page/济阳县.md "wikilink")、[成武县](../Page/成武县.md "wikilink")、[冤句县](../Page/冤句县.md "wikilink")、[乘氏县](../Page/乘氏县_\(北魏\).md "wikilink")、定陶县、[单父县](../Page/单父县.md "wikilink")、[金乡县](../Page/金乡县.md "wikilink")。

[唐朝初年又改回曹州](../Page/唐朝.md "wikilink")，后又改为郡。土贡：绢、绵、大蛇粟、葶历。户十万三百五十二，口七十一万六千八百四十八。县六：济阴县、[考城县](../Page/考城县.md "wikilink")、[宛句县](../Page/宛句县.md "wikilink")、乘氏县、[南华县](../Page/南华县_\(唐朝\).md "wikilink")、成武县。[乾元元年](../Page/乾元_\(唐朝\).md "wikilink")（758年），天下郡改州，复为曹州。

## 注释

<div class="references-small">

<references />

</div>

## 參考文獻

  - 《[隋书](../Page/隋书.md "wikilink")·志第二十五·地理中》
  - 《[新唐书](../Page/新唐书.md "wikilink")·志第二十八·地理二》

## 参见

  - [济阴王](../Page/济阴王.md "wikilink")
  - [定陶王](../Page/定陶王.md "wikilink")

[Category:汉朝的郡](../Category/汉朝的郡.md "wikilink")
[Category:曹魏的郡](../Category/曹魏的郡.md "wikilink")
[Category:西晋的郡](../Category/西晋的郡.md "wikilink")
[Category:北朝的郡](../Category/北朝的郡.md "wikilink")
[Category:隋朝的郡](../Category/隋朝的郡.md "wikilink")
[Category:唐朝的郡](../Category/唐朝的郡.md "wikilink")
[Category:山东的郡](../Category/山东的郡.md "wikilink")
[Category:菏泽行政区划史](../Category/菏泽行政区划史.md "wikilink")
[Category:前130年代建立的行政区划](../Category/前130年代建立的行政区划.md "wikilink")
[Category:758年废除的行政区划](../Category/758年废除的行政区划.md "wikilink")

1.  參考　《晉書》卷十四〈地理上．總敘．司州．兖州．豫州．冀州．幽州．平州．幷州．雍州．涼州．秦州．梁州．益州．寧州〉（房玄齡
    等　1974：419）