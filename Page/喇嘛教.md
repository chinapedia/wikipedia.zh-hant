**喇嘛教**（；）是对[藏傳佛教的一个不标准的民间称呼](../Page/藏傳佛教.md "wikilink")。[喇嘛](../Page/喇嘛.md "wikilink")（）一詞原是對西藏藏傳佛教僧侶或上師的尊稱，如[達賴喇嘛](../Page/達賴喇嘛.md "wikilink")（，藏语拼音：Dalai
Lama)。不過，喇嘛教这个称呼在[藏语中和](../Page/藏语.md "wikilink")[学术界被广泛认为是一个輕蔑語或](../Page/学术界.md "wikilink")[貶義詞](../Page/貶義.md "wikilink")（即暗示这种宗教是喇嘛捏造，而非佛教）\[1\]\[2\]，[中国或相關當局打算弃用这一词](../Page/中国.md "wikilink")，“目前多采用**藏传佛教**代称喇嘛教”\[3\]。

## 词语釋義

**[喇嘛](../Page/喇嘛.md "wikilink")**，音译自[藏语](../Page/藏语.md "wikilink")
bla-ma、lama，[藏傳佛教術語](../Page/藏傳佛教.md "wikilink")，意為[上師](../Page/上師.md "wikilink")、[上人](../Page/上人.md "wikilink")，為梵文Guru的譯名。為对佛教僧侶之尊称，長老、[上座](../Page/上座.md "wikilink")、高僧之稱號。中文（
漢文）**喇嘛教**一词来源于“喇嘛”一词，西藏佛教是由顯入密的大乘佛教，密教依止師尊第一，故被外人稱為喇嘛教。有证据显示欧洲语言中“喇嘛教”（；）一词来源于汉文。\[4\]

历史上，“喇嘛教”一词被用来描绘：

  - [藏傳佛教](../Page/藏傳佛教.md "wikilink")；
  - [密宗](../Page/密宗.md "wikilink")；
  - 流传于[卡爾梅克人與](../Page/卡爾梅克人.md "wikilink")[布里亚特地区的藏傳佛教](../Page/布里亚特共和國.md "wikilink")。\[5\]

## 歷史源流

唐代以前未見西藏佛教於中國史籍。其地原有一種巫術信仰，藏人稱為[苯教](../Page/苯教.md "wikilink")（Bon）。到藏王[松贊幹布時](../Page/松贊幹布.md "wikilink")，先與[尼泊爾公主通婚](../Page/尼泊爾.md "wikilink")，再娶唐之[文成公主](../Page/文成公主.md "wikilink")，兩位公主始將佛法經像，隨往西藏，藏地始聞佛教正法。西藏亦派人至[印度學習梵文](../Page/印度.md "wikilink")、求佛經，歸而仿梵文而造藏字，並翻譯佛經。當時佛教雖在藏王及藏-{后}-之提倡下，實際影響不大，所譯寶雲、寶篋等經，今亦無傳。當[赤松德贊](../Page/赤松德贊.md "wikilink")（743年～797年）在位時，從印度請來持律比丘二十人，派藏族才俊子第，赴印度求法，佛教事業繼起有人。西藏佛教於此時真正建立，由顯入密。

自[元朝開始](../Page/元朝.md "wikilink")，藏傳佛教随[蒙古入侵者傳入中原](../Page/蒙古.md "wikilink")，[漢人習慣上以喇嘛來泛稱所有的藏傳佛教僧侶](../Page/漢人.md "wikilink")，藏傳佛教因此也被稱為喇嘛教。中文之中“喇嘛教”一词现在可查的最早出现在[明代](../Page/明代.md "wikilink")[张居正所撰](../Page/张居正.md "wikilink")[北京番经厂碑文中](../Page/北京.md "wikilink")。\[6\]
在[清朝](../Page/清朝.md "wikilink")，汉人用于将藏傳佛教从[汉传佛教中区分出来](../Page/汉传佛教.md "wikilink")。\[7\]
有观点认为，汉人用一个新教名来区分与[汉传佛教相同](../Page/汉传佛教.md "wikilink")，但是在中国藏蒙等[少数民族地区流行的佛教](../Page/少数民族.md "wikilink")“并不确切”，因为相同的标准并没有用于称呼汉传佛教中不同的派别。\[8\]
这样的称呼就意味着他们的宗教不应当称作“佛教”，是喇嘛捏造。\[9\]
这个称呼还意味着藏傳佛教与[印度佛教的断裂](../Page/印度佛教.md "wikilink")。\[10\]

欧洲文献裡，1822年在德国博物学家比德·西蒙·帕拉斯（Peter Simon
Pallas）的报告中第一次出现“喇嘛教”一词\[11\]。书中提及“喇嘛的宗教”和“喇嘛教的信条”。1825年，雷姆萨（Jean　Pierre　Abeil　Remusat）在他的《论喇嘛教的起源》中，使用了“喇嘛教的”术语。历史上一些包括[黑格爾](../Page/黑格爾.md "wikilink")\[12\]、[喬瑪](../Page/喬瑪.md "wikilink")\[13\]、[斯文·赫定](../Page/斯文·赫定.md "wikilink")\[14\]、[安德烈亞斯·格魯施克](../Page/安德烈亞斯·格魯施克.md "wikilink")\[15\]，[韓素音](../Page/韓素音.md "wikilink")\[16\]在内的西方和西藏学者都曾经用过这个词。

## 注释

## 参考书目

  -
  -
[Category:佛教術語](../Category/佛教術語.md "wikilink")
[Category:贬义词](../Category/贬义词.md "wikilink")

1.
2.
3.
4.  Donald S. Lopez, Jr., p6

5.  Giuseppe Tucci, Walther Heissig: *Die Religionen Tibets und der
    Mongolei*. Kohlhammer, 1970. (darin: Die Religionen der Mongolei
    (Heissig), S. 296-427)

6.  高文德,卢勋,史金波,白滨等.《中国少数民族史大辞典》, 吉林教育出版社1995年. ISBN 978-7-5383-2805-9:
    第2571頁

7.
8.  周正国,高奉仁,常锐伦;梁焕国,丘岱安,姚今迈. 《中国中学教学百科全书》（体音美卷）ISBN
    978-7-80556-411-1：喇嘛教

9.
10. Edward Conze

11.
12.
13. *Collected works of Alexander Csoma de Kőrös*. Akadémiai Kiadó,
    1984.

14. Sven Hedin: *Transhimalaja. Entdeckungen und Abenteuer in Tibet*. F.
    A. Brockhaus, 1923.

15. Andreas Gruschke: *Mythen und Legenden der Tibeter. Von Kriegern,
    Mönchen, Dämonen und dem Ursprung der Welt*. E. Diederichs, 1996.

16. Han Suyin: *Lhasa, the Open City. A Journey to Tibet*. Putnam, 1977.