[Yishankeshi.jpg](https://zh.wikipedia.org/wiki/File:Yishankeshi.jpg "fig:Yishankeshi.jpg")摹刻，[北宋](../Page/北宋.md "wikilink")[淳化四年](../Page/淳化.md "wikilink")（公元993年）\]\]

**峄山刻石**高218厘米、宽84厘米
。两面刻文，共15行，满行15字，相传为[李斯所书](../Page/李斯.md "wikilink")。《峄山刻石》高218厘米、宽84厘米。峄山刻石书体是[小篆](../Page/小篆.md "wikilink")，也就是仅存不多的小篆刻石，传说是[李斯的书法手迹](../Page/李斯.md "wikilink")，但于史无据。

## 历史

  - 秦王政二十八年（前219年），[秦始皇出巡](../Page/秦始皇.md "wikilink")[峄山](../Page/峄山.md "wikilink")（今山东[邹县东南](../Page/邹县.md "wikilink")）时所刻。《史记·秦始皇本纪》载：“始皇二十八年东行郡县，上邹绎山，与鲁诸儒生议刻石、颂秦德、议封禅，望祭山川之事”。
  - 原石已被后来[曹操登山时毁掉](../Page/曹操.md "wikilink")，但留下了碑文。（一说为[北周武帝所为](../Page/周武帝.md "wikilink")）。
  - 现在的摹本比较有名的是“长安本”。[宋太宗](../Page/宋太宗.md "wikilink")[淳化四年](../Page/淳化.md "wikilink")（公元993年）[郑文宝根据](../Page/郑文宝.md "wikilink")[五代](../Page/五代.md "wikilink")[南唐](../Page/南唐.md "wikilink")[徐铉的拓本重刻于](../Page/徐铉.md "wikilink")[长安](../Page/长安.md "wikilink")，碑阴有郑文宝题记。現存[西安碑林](../Page/西安碑林.md "wikilink")。\[1\]

## 全文

玆据[王昶](../Page/王昶_\(清朝\).md "wikilink")《[金石萃編](../Page/金石萃編.md "wikilink")》錄其全文如下，原文無標點：

## 註釋

## 參考文獻

[Category:秦朝书法碑刻](../Category/秦朝书法碑刻.md "wikilink")
[Category:山东书法碑刻](../Category/山东书法碑刻.md "wikilink")
[Category:西安碑林博物館](../Category/西安碑林博物館.md "wikilink")
[Category:济宁文物](../Category/济宁文物.md "wikilink")
[Category:邹城市](../Category/邹城市.md "wikilink")
[Category:小篆](../Category/小篆.md "wikilink")

1.  [嶧山刻石，西安碑林博物館](http://www.beilin-museum.com/view.asp?id=209)