[Agile-vs-iterative-flow.jpg](https://zh.wikipedia.org/wiki/File:Agile-vs-iterative-flow.jpg "fig:Agile-vs-iterative-flow.jpg")
Royce提倡重复地使用瀑布模型，以一种迭代的方式。但是，大多数人并不知道这一点，一些人也不相信它能被應用在現實生活中，因為过程很少能够以連續由上而下的方式进行。經常會需要回到前面的阶段，或改變前一阶段的结果。讽刺的是，在Royce
1970年的那篇文章中他提到：这种模型的目的是作为用来说明这种模式有缺陷，而不適用。事实上，软件开发相关文章中对这个名词的大量引用正是对这个广泛流行的软體开发做法的一种评判。

瀑布模型（Waterfall
Model）最早強調系統開發應有完整之週期，且必須完整的經歷週期之每一開發階段，並系統化的考量分析與設計的技術、時間與資源之投入等，因此瀑布模型又可以稱為『系統發展生命週期』（System
Development Life Cycle,
SDLC）。由於該模式強調系統開發過程需有完整的規劃、分析、設計、測試及文件等管理與控制，因此能有效的確保系統品質，它已經成為软體業界大多數軟體開發的標準（Boehm,
1988）。

## 模型（階段）

1.  需求定義（Requirements）
2.  設計（Design）
3.  實作（Implementation）
4.  整合與測試（Verification）
5.  移交與維護（Maintenance）

## 参见

  - [项目生命周期](../Page/项目生命周期.md "wikilink")
  - [螺旋模型](../Page/螺旋模型.md "wikilink")
  - [混沌模型](../Page/混沌模型.md "wikilink")
  - [敏捷软件开发](../Page/敏捷软件开发.md "wikilink")
  - [归纳与总结](../Page/归纳与总结.md "wikilink")

## 参考文献

《系統分析與設計–理論與實務應用》三版，吳仁和、林信惠 著，智勝出版社

## 外部链接

  - [在c2.com上的讨论](http://c2.com/cgi/wiki?WaterFall)

[Category:软件工程](../Category/软件工程.md "wikilink")
[Category:軟體開發](../Category/軟體開發.md "wikilink")