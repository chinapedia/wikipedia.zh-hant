**喉盤魚科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目的其中一個](../Page/鱸形目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分類

**喉盤魚科**其下分42個屬，如下：

### 綠喉盤魚屬（*Acyrtops*）

  -   - [福氏綠喉盤魚](../Page/福氏綠喉盤魚.md "wikilink")（*Acyrtops amplicirrus*）
      - [綠喉盤魚](../Page/綠喉盤魚.md "wikilink")（*Acyrtops beryllinus*）

### 緣盤魚屬（*Acyrtus*）

  -   - [緣盤魚](../Page/緣盤魚.md "wikilink")（*Acyrtus artius*）
      - [少鰭緣盤魚](../Page/少鰭緣盤魚.md "wikilink")（*Acyrtus pauciradiatus*）
      - [銹色緣盤魚](../Page/銹色緣盤魚.md "wikilink")（*Acyrtus rubiginosus*）

### 鰭鱔屬（*Alabes*）

  -   - [貝氏鰭鱔](../Page/貝氏鰭鱔.md "wikilink")（*Alabes bathys*）
      - [短鰭鱔](../Page/短鰭鱔.md "wikilink")（*Alabes brevis*）
      - [背鰭鱔](../Page/背鰭鱔.md "wikilink")（*Alabes dorsalis*）：又稱唇鰓鱔。
      - [長體鰭鱔](../Page/長體鰭鱔.md "wikilink")（*Alabes elongata*）
      - [隆背鰭鱔](../Page/隆背鰭鱔.md "wikilink")（*Alabes gibbosa*）
      - [霍氏鰭鱔](../Page/霍氏鰭鱔.md "wikilink")（*Alabes hoesei*）
      - [鈍吻鰭鱔](../Page/鈍吻鰭鱔.md "wikilink")（*Alabes obtusirostris*）
      - [西方鰭鱔](../Page/西方鰭鱔.md "wikilink")（*Alabes occidentalis*）
      - [小鰭鱔](../Page/小鰭鱔.md "wikilink")（*Alabes parvulus*）
      - [史考氏鰭鱔](../Page/史考氏鰭鱔.md "wikilink")（*Alabes scotti*）
      - [斯氏鰭鱔](../Page/斯氏鰭鱔.md "wikilink")（*Alabes springeri*）

### 圓喉盤魚屬（*Apletodon*）

  -   - [伯塞斯庫圓喉盤魚](../Page/伯塞斯庫圓喉盤魚.md "wikilink")（*Apletodon bacescui*）
      - [鬍圓喉盤魚](../Page/鬍圓喉盤魚.md "wikilink")（*Apletodon barbatus*）
      - [圓喉盤魚](../Page/圓喉盤魚.md "wikilink")（*Apletodon
        dentatus*）：又稱小頭圓喉盤魚。
      - [未知圓喉盤魚](../Page/未知圓喉盤魚.md "wikilink")（*Apletodon incognitus*）
      - [佩氏圓喉盤魚](../Page/佩氏圓喉盤魚.md "wikilink")（*Apletodon pellegrini*）
      - [沃氏圓喉盤魚](../Page/沃氏圓喉盤魚.md "wikilink")（*Apletodon wirtzi*）

### 阿科斯喉盤魚屬（*Arcos*）

  -   - [艷阿科斯喉盤魚](../Page/艷阿科斯喉盤魚.md "wikilink")（*Arcos decoris*）
      - [阿科斯喉盤魚](../Page/阿科斯喉盤魚.md "wikilink")（*Arcos erythrops*）
      - [大眼阿科斯喉盤魚](../Page/大眼阿科斯喉盤魚.md "wikilink")（*Arcos
        macrophthalmus*）
      - [雜色阿科斯喉盤魚](../Page/雜色阿科斯喉盤魚.md "wikilink")（*Arcos
        poecilophthalmos*）
      - [玫斑阿科斯喉盤魚](../Page/玫斑阿科斯喉盤魚.md "wikilink")（*Arcos rhodospilus*）

### 姥魚屬（*Aspasma*）

  -   - [小姥魚](../Page/小姥魚.md "wikilink")（*Aspasma minima*）

### 鶴姥魚屬（*Aspasmichthys*）

  -   - [鶴姥魚](../Page/鶴姥魚.md "wikilink")（*Aspasmichthys
        ciconiae*）：又稱鶴喉盤魚。

### *Aspasmodes*屬

  -   - *Aspasmodes briggsi*

### 擬姥魚屬（*Aspasmogaster*）

  -   - [隆線擬姥魚](../Page/隆線擬姥魚.md "wikilink")（*Aspasmogaster costata*）
      - [光吻擬姥魚](../Page/光吻擬姥魚.md "wikilink")（*Aspasmogaster
        liorhynchus*）
      - [西方擬姥魚](../Page/西方擬姥魚.md "wikilink")（*Aspasmogaster
        occidentalis*）
      - [澳洲擬姥魚](../Page/澳洲擬姥魚.md "wikilink")（*Aspasmogaster
        tasmaniensis*）

### *Briggsia*屬

  -   - *Briggsia hastingsi*

### 巨喉盤魚屬（*Chorisochismus*）

  -   - [巨喉盤魚](../Page/巨喉盤魚.md "wikilink")（*Chorisochismus dentex*）

### 寬頭喉盤魚屬（*Cochleoceps*）

  -   - [巴塞寬頭喉盤魚](../Page/巴塞寬頭喉盤魚.md "wikilink")（*Cochleoceps
        bassensis*）
      - [雙色寬頭喉盤魚](../Page/雙色寬頭喉盤魚.md "wikilink")（*Cochleoceps bicolor*）
      - [東方寬頭喉盤魚](../Page/東方寬頭喉盤魚.md "wikilink")（*Cochleoceps
        orientalis*）
      - [寬頭喉盤魚](../Page/寬頭喉盤魚.md "wikilink")（*Cochleoceps spatula*）
      - [綠寬頭喉盤魚](../Page/綠寬頭喉盤魚.md "wikilink")（*Cochleoceps viridis*）

### 錐齒喉盤魚屬（*Conidens*）

  -   - [黑紋頭錐齒喉盤魚](../Page/黑紋頭錐齒喉盤魚.md "wikilink")（*Conidens
        laticephalus*）：又稱黑紋錐齒喉盤魚。
      - [薩摩錐齒喉盤魚](../Page/薩摩錐齒喉盤魚.md "wikilink")（*Conidens samoensis*）

### 瘤喉盤魚屬（*Creocele*）

  -   - [頭瘤喉盤魚](../Page/頭瘤喉盤魚.md "wikilink")（*Creocele cardinalis*）

### 頸滑喉盤魚屬（*Derilissus*）

  -   - [阿氏頸滑喉盤魚](../Page/阿氏頸滑喉盤魚.md "wikilink")（*Derilissus altifrons*）
      - [晶鬚眼頸滑喉盤魚](../Page/晶鬚眼頸滑喉盤魚.md "wikilink")（*Derilissus
        kremnobates*）
      - [矮頸滑喉盤魚](../Page/矮頸滑喉盤魚.md "wikilink")（*Derilissus nanus*）
      - [梳頸滑喉盤魚](../Page/梳頸滑喉盤魚.md "wikilink")（*Derilissus vittiger*）

### 環盤魚屬（*Diademichthys*）

  -   - [線紋環盤魚](../Page/線紋環盤魚.md "wikilink")（*Diademichthys
        lineatus*）：又稱頭冠喉盤魚。

### 褶腹喉盤魚屬（*Diplecogaster*）

  -   - [雙點褶腹喉盤魚](../Page/雙點褶腹喉盤魚.md "wikilink")（*Diplecogaster
        bimaculata bimaculata*）
      - [黑褶腹喉盤魚](../Page/黑褶腹喉盤魚.md "wikilink")（*Diplecogaster bimaculata
        euxinica*）
      - [雙斑褶腹喉盤魚](../Page/雙斑褶腹喉盤魚.md "wikilink")（*Diplecogaster
        bimaculata pectoralis*）
      - [梳褶腹喉盤魚](../Page/梳褶腹喉盤魚.md "wikilink")（*Diplecogaster
        ctenocrypta*）
      - [大眼褶腹喉盤魚](../Page/大眼褶腹喉盤魚.md "wikilink")（*Diplecogaster
        megalops*）

### 新西蘭喉盤魚屬（*Diplocrepis*）

  -   - [新西蘭喉盤魚](../Page/新西蘭喉盤魚.md "wikilink")（*Diplocrepis puniceus*）

### 盤孔喉盤魚屬（*Discotrema*）

  -   - [盤孔喉盤魚](../Page/盤孔喉盤魚.md "wikilink")（*Discotrema crinophila*）
      - [概要盤孔喉盤魚](../Page/概要盤孔喉盤魚.md "wikilink")（*Discotrema
        monogrammum*）
      - [帶紋盤孔喉盤魚](../Page/帶紋盤孔喉盤魚.md "wikilink")（*Discotrema zonatum*）

### 艾氏喉盤魚屬（*Eckloniaichthys*）

  -   - [艾氏喉盤魚](../Page/艾氏喉盤魚.md "wikilink")（*Eckloniaichthys
        scylliorhiniceps*）

### 腹杯喉盤魚屬（*Gastrocyathus*）

  -   - [腹杯喉盤魚](../Page/腹杯喉盤魚.md "wikilink")（*Gastrocyathus gracilis*）

### 腹盞喉盤魚屬（*Gastrocymba*）

  -   - [腹盞喉盤魚](../Page/腹盞喉盤魚.md "wikilink")（*Gastrocymba
        quadriradiata*）：又稱四輻新西蘭喉盤魚。

### 似腹杯喉盤魚屬（*Gastroscyphus*）

  -   - [似腹杯喉盤魚](../Page/似腹杯喉盤魚.md "wikilink")（*Gastroscyphus hectoris*）

### 喉盤魚屬（*Gobiesox*）

  -   - [野喉盤魚](../Page/野喉盤魚.md "wikilink")（*Gobiesox adustus*）
      - [淡黑喉盤魚](../Page/淡黑喉盤魚.md "wikilink")（*Gobiesox aethus*）
      - [垂唇喉盤魚](../Page/垂唇喉盤魚.md "wikilink")（*Gobiesox barbatulus*）
      - [犬齒喉盤魚](../Page/犬齒喉盤魚.md "wikilink")（*Gobiesox canidens*）
      - [糙體喉盤魚](../Page/糙體喉盤魚.md "wikilink")（*Gobiesox crassicorpus*）
      - [美喉盤魚](../Page/美喉盤魚.md "wikilink")（*Gobiesox daedaleus*）
      - [線紋喉盤魚](../Page/線紋喉盤魚.md "wikilink")（*Gobiesox eugrammus*）
      - [河棲喉盤魚](../Page/河棲喉盤魚.md "wikilink")（*Gobiesox fluviatilis*）
      - [溪喉盤魚](../Page/溪喉盤魚.md "wikilink")（*Gobiesox fulvus*）
      - [朱氏喉盤魚](../Page/朱氏喉盤魚.md "wikilink")（*Gobiesox juniperoserrai*）
      - [哥倫比亞喉盤魚](../Page/哥倫比亞喉盤魚.md "wikilink")（*Gobiesox juradoensis*）
      - [斑點喉盤魚](../Page/斑點喉盤魚.md "wikilink")（*Gobiesox lucayanus*）
      - [條紋喉盤魚](../Page/條紋喉盤魚.md "wikilink")（*Gobiesox maeandricus*）
      - [寂喉盤魚](../Page/寂喉盤魚.md "wikilink")（*Gobiesox marijeanae*）
      - [石紋喉盤魚](../Page/石紋喉盤魚.md "wikilink")（*Gobiesox marmoratus*）
      - [密歇根喉盤魚](../Page/密歇根喉盤魚.md "wikilink")（*Gobiesox mexicanus*）
      - [米勒喉盤魚](../Page/米勒喉盤魚.md "wikilink")（*Gobiesox milleri*）
      - [多鬚喉盤魚](../Page/多鬚喉盤魚.md "wikilink")（*Gobiesox multitentaculus*）
      - [黑鰭喉盤魚](../Page/黑鰭喉盤魚.md "wikilink")（*Gobiesox nigripinnis*）
      - [裸喉盤魚](../Page/裸喉盤魚.md "wikilink")（*Gobiesox nudus*）：又稱裸阿科斯喉盤魚。
      - [鬚喉盤魚](../Page/鬚喉盤魚.md "wikilink")（*Gobiesox papillifer*）
      - [翼喉盤魚](../Page/翼喉盤魚.md "wikilink")（*Gobiesox pinniger*）
      - [河口喉盤魚](../Page/河口喉盤魚.md "wikilink")（*Gobiesox potamius*）
      - [帶紋喉盤魚](../Page/帶紋喉盤魚.md "wikilink")（*Gobiesox punctulatus*）
      - [加州喉盤魚](../Page/加州喉盤魚.md "wikilink")（*Gobiesox rhessodon*）
      - [斯氏喉盤魚](../Page/斯氏喉盤魚.md "wikilink")（*Gobiesox schultzi*）
      - [窄頭喉盤魚](../Page/窄頭喉盤魚.md "wikilink")（*Gobiesox stenocephalus*）
      - [肥壯喉盤魚](../Page/肥壯喉盤魚.md "wikilink")（*Gobiesox strumosus*）
      - [伍茲氏喉盤魚](../Page/伍茲氏喉盤魚.md "wikilink")（*Gobiesox woodsi*）

### 軟喉盤魚屬（*Gouania*）

  -   - [威氏軟喉盤魚](../Page/威氏軟喉盤魚.md "wikilink")（*Gouania willdenowi*）

### *Gymnoscyphus*屬

  -   - *Gymnoscyphus ascitus*

### 單杯喉盤魚屬（*Haplocylix*）

  -   - [單杯喉盤魚](../Page/單杯喉盤魚.md "wikilink")（*Haplocylix littoreus*）

### 科普喉盤魚屬（*Kopua*）

  -   - [科普喉盤魚](../Page/科普喉盤魚.md "wikilink")（*Kopua kuiteri*）
      - [新西蘭科普喉盤魚](../Page/新西蘭科普喉盤魚.md "wikilink")（*Kopua nuimata*）

### 盆腹喉盤魚屬（*Lecanogaster*）

  -   - [金色盆腹喉盤魚](../Page/金色盆腹喉盤魚.md "wikilink")（*Lecanogaster chrysea*）

### 連鰭喉盤魚屬（*Lepadichthys*）

  -   - [博林連鰭喉盤魚](../Page/博林連鰭喉盤魚.md "wikilink")（*Lepadichthys bolini*）
      - [淺色連鰭喉盤魚](../Page/淺色連鰭喉盤魚.md "wikilink")（*Lepadichthys caritus*）
      - [眼帶連鰭喉盤魚](../Page/眼帶連鰭喉盤魚.md "wikilink")（*Lepadichthys
        coccinotaenia*）
      - [梳連鰭喉盤魚](../Page/梳連鰭喉盤魚.md "wikilink")（*Lepadichthys ctenion*）
      - [淡紅連鰭喉盤魚](../Page/淡紅連鰭喉盤魚.md "wikilink")（*Lepadichthys
        erythraeus*）
      - [連鰭喉盤魚](../Page/連鰭喉盤魚.md "wikilink")（*Lepadichthys
        frenatus*）：又稱黃喉盤魚、三崎姥魚。
      - [雙紋連鰭喉盤魚](../Page/雙紋連鰭喉盤魚.md "wikilink")（*Lepadichthys
        lineatus*）：又稱尖吻盤孔喉盤魚。
      - [小連鰭喉盤魚](../Page/小連鰭喉盤魚.md "wikilink")（*Lepadichthys minor*）
      - [西澳連鰭喉盤魚](../Page/西澳連鰭喉盤魚.md "wikilink")（*Lepadichthys
        sandaracatus*）
      - [史氏連鰭喉盤魚](../Page/史氏連鰭喉盤魚.md "wikilink")（*Lepadichthys
        springeri*）

### 叉鼻喉盤魚屬（*Lepadogaster*）

  -   - [坎氏叉鼻喉盤魚](../Page/坎氏叉鼻喉盤魚.md "wikilink")（*Lepadogaster
        candolii*）
      - [叉鼻喉盤魚](../Page/叉鼻喉盤魚.md "wikilink")（*Lepadogaster
        lepadogaster*）
      - [紫色叉鼻喉盤魚](../Page/紫色叉鼻喉盤魚.md "wikilink")（*Lepadogaster
        purpurea*）
      - [條紋叉鼻喉盤魚](../Page/條紋叉鼻喉盤魚.md "wikilink")（*Lepadogaster zebrina*）

### 滑鰓喉盤魚（*Liobranchia*）

  -   - [條紋滑鰓喉盤魚](../Page/條紋滑鰓喉盤魚.md "wikilink")（*Liobranchia stria*）

### 無耙喉盤魚屬（*Lissonanchus*）

  -   - [路氏無耙喉盤魚](../Page/路氏無耙喉盤魚.md "wikilink")（*Lissonanchus lusheri*）

### 尋常喉盤魚屬（*Modicus*）

  -   - [尋常喉盤魚](../Page/尋常喉盤魚.md "wikilink")（*Modicus minimus*）
      - [坦加羅亞尋常喉盤魚](../Page/坦加羅亞尋常喉盤魚.md "wikilink")（*Modicus tangaroa*）

### 鑽頰喉盤魚屬（*Opeatogenys*）

  -   - [凱氏鑽頰喉盤魚](../Page/凱氏鑽頰喉盤魚.md "wikilink")（*Opeatogenys cadenati*）
      - [細鑽頰喉盤魚](../Page/細鑽頰喉盤魚.md "wikilink")（*Opeatogenys gracilis*）

### 微鰭喉盤魚屬（*Parvicrepis*）

  -   - [微鰭喉盤魚](../Page/微鰭喉盤魚.md "wikilink")（*Parvicrepis parvipinnis*）

### 擬喉盤魚屬（*Pherallodichthys*）

  -   - [擬喉盤魚](../Page/擬喉盤魚.md "wikilink")（*Pherallodichthys
        meshimaensis*）

### 喉異盤魚屬（*Pherallodiscus*）

  -   - [喉異盤魚](../Page/喉異盤魚.md "wikilink")（*Pherallodiscus funebris*）
      - [多變喉異盤魚](../Page/多變喉異盤魚.md "wikilink")（*Pherallodiscus varius*）

### 細喉盤魚屬（*Pherallodus*）

  -   - [印度細喉盤魚](../Page/印度細喉盤魚.md "wikilink")（*Pherallodus
        indicus*）：又稱印度異齒喉盤魚。
      - [史氏細喉盤魚](../Page/史氏細喉盤魚.md "wikilink")（*Pherallodus smithi*）

### 海神喉盤魚屬（*Posidonichthys*）

  -   - [海神喉盤魚](../Page/海神喉盤魚.md "wikilink")（*Posidonichthys hutchinsi*）

### 前鰭喉盤魚屬（*Propherallodus*）

  -   - [前鰭喉盤魚](../Page/前鰭喉盤魚.md "wikilink")（*Propherallodus briggsi*）

### 長喉盤魚屬（*Rimicola*）

  -   - [海峽群島長喉盤魚](../Page/海峽群島長喉盤魚.md "wikilink")（*Rimicola cabrilloi*）
      - [雙形長喉盤魚](../Page/雙形長喉盤魚.md "wikilink")（*Rimicola dimorpha*）
      - [艾氏長喉盤魚](../Page/艾氏長喉盤魚.md "wikilink")（*Rimicola eigenmanni*）
      - [斑點長喉盤魚](../Page/斑點長喉盤魚.md "wikilink")（*Rimicola muscarum*）
      - [瓜達盧佩長喉盤魚](../Page/瓜達盧佩長喉盤魚.md "wikilink")（*Rimicola sila*）

### 杯吸盤魚屬（*Sicyases*）

  -   - [短吻杯吸盤魚](../Page/短吻杯吸盤魚.md "wikilink")（*Sicyases brevirostris*）
      - [希氏杯吸盤魚](../Page/希氏杯吸盤魚.md "wikilink")（*Sicyases hildebrandi*）
      - [杯吸盤魚](../Page/杯吸盤魚.md "wikilink")（*Sicyases sanguineus*）

### 割齒喉盤魚屬（*Tomicodon*）

  -   - [野割齒喉盤魚](../Page/野割齒喉盤魚.md "wikilink")（*Tomicodon absitus*）
      - [哥斯達黎加割齒喉盤魚](../Page/哥斯達黎加割齒喉盤魚.md "wikilink")（*Tomicodon
        abuelorum*）
      - [南方割齒喉盤魚](../Page/南方割齒喉盤魚.md "wikilink")（*Tomicodon australis*）
      - [雙牙割齒喉盤魚](../Page/雙牙割齒喉盤魚.md "wikilink")（*Tomicodon bidens*）
      - [博氏割齒喉盤魚](../Page/博氏割齒喉盤魚.md "wikilink")（*Tomicodon boehlkei*）
      - [伯氏割齒喉盤魚](../Page/伯氏割齒喉盤魚.md "wikilink")（*Tomicodon briggsi*）
      - [智利割齒喉盤魚](../Page/智利割齒喉盤魚.md "wikilink")（*Tomicodon chilensis*）
      - [克氏割齒喉盤魚](../Page/克氏割齒喉盤魚.md "wikilink")（*Tomicodon clarkei*）
      - [隱割齒喉盤魚](../Page/隱割齒喉盤魚.md "wikilink")（*Tomicodon cryptus*）
      - [東方割齒喉盤魚](../Page/東方割齒喉盤魚.md "wikilink")（*Tomicodon eos*）
      - [橫帶割齒喉盤魚](../Page/橫帶割齒喉盤魚.md "wikilink")（*Tomicodon fasciatus*）
      - [上臂割齒喉盤魚](../Page/上臂割齒喉盤魚.md "wikilink")（*Tomicodon humeralis*）
      - [勒氏割齒喉盤魚](../Page/勒氏割齒喉盤魚.md "wikilink")（*Tomicodon
        lavettsmithi*）
      - [盧氏割齒喉盤魚](../Page/盧氏割齒喉盤魚.md "wikilink")（*Tomicodon
        leurodiscus*）
      - [邁氏割齒喉盤魚](../Page/邁氏割齒喉盤魚.md "wikilink")（*Tomicodon myersi*）
      - [佩氏割齒喉盤魚](../Page/佩氏割齒喉盤魚.md "wikilink")（*Tomicodon petersii*）
      - [割齒喉盤魚](../Page/割齒喉盤魚.md "wikilink")（*Tomicodon prodomus*）
      - [芮氏割齒喉盤魚](../Page/芮氏割齒喉盤魚.md "wikilink")（*Tomicodon reitzae*）
      - [棒狀割齒喉盤魚](../Page/棒狀割齒喉盤魚.md "wikilink")（*Tomicodon rhabdotus*）
      - [魯氏割齒喉盤魚](../Page/魯氏割齒喉盤魚.md "wikilink")（*Tomicodon rupestris*）
      - [蟲紋割齒喉盤魚](../Page/蟲紋割齒喉盤魚.md "wikilink")（*Tomicodon
        vermiculatus*）
      - [斑馬割齒喉盤魚](../Page/斑馬割齒喉盤魚.md "wikilink")（*Tomicodon zebra*）

### 喉載魚屬（*Trachelochismus*）

  -   - [新西蘭喉載魚](../Page/新西蘭喉載魚.md "wikilink")（*Trachelochismus
        melobesia*）
      - [喉載魚](../Page/喉載魚.md "wikilink")（*Trachelochismus pinnulatus*）

[\*](../Category/喉盤魚科.md "wikilink")