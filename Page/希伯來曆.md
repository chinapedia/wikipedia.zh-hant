[DetailOfMedievalHebrewCalendar.jpg](https://zh.wikipedia.org/wiki/File:DetailOfMedievalHebrewCalendar.jpg "fig:DetailOfMedievalHebrewCalendar.jpg")、[桃金娘和](../Page/桃金娘.md "wikilink")[柳枝](../Page/柳.md "wikilink")，另一手持[枸櫞的果實](../Page/枸櫞.md "wikilink")\]\]

**希伯來曆**又稱**猶太曆**（、）是由[猶太教創造的古老](../Page/猶太教.md "wikilink")[曆法](../Page/曆法.md "wikilink")，也是[以色列目前使用的](../Page/以色列.md "wikilink")[國曆](../Page/國曆.md "wikilink")（與[公曆並用](../Page/公曆.md "wikilink")）。是一種[陰陽合曆](../Page/陰陽合曆.md "wikilink")，每[月以月相為准](../Page/月.md "wikilink")，但設置[閏月](../Page/閏月.md "wikilink")，使每曆[年和](../Page/年.md "wikilink")[太陽年週期一致](../Page/太陽年.md "wikilink")。[曆日與曆月認定和](../Page/曆日.md "wikilink")[伊斯蘭曆一樣](../Page/伊斯蘭曆.md "wikilink")，以[日落為一日之始](../Page/日落.md "wikilink")，[新月](../Page/新月.md "wikilink")（[眉月](../Page/眉月.md "wikilink")）初升為一月之始。以每年[春分後的第一個新月為一年的開始](../Page/春分.md "wikilink")，設置閏月方式和[傣曆與](../Page/傣曆.md "wikilink")[農曆一樣](../Page/農曆.md "wikilink")，採[十九年七閏](../Page/章歲.md "wikilink")，但閏月統一放到閏年的第六個月之後。

[猶太教傳統與](../Page/猶太教.md "wikilink")[舊約聖經認為上帝創世的第一天是](../Page/舊約聖經.md "wikilink")[以祿月](../Page/以祿月.md "wikilink")25號——創造的存在、[時間](../Page/時間.md "wikilink")、物體、黑暗和[光脫離](../Page/光.md "wikilink")“虛空”，相當於[公元](../Page/公元.md "wikilink")[前3761年](../Page/前38世紀.md "wikilink")[9月30日](../Page/9月30日.md "wikilink")；第六天，[提斯利月的第一天](../Page/提斯利月.md "wikilink")，“上帝就照著自己的形像造人”---人類始祖亞當和夏娃被創造出來，稱為[創世紀元](../Page/世界紀元.md "wikilink")，相當於公元[前3761年](../Page/前38世紀.md "wikilink")[10月7日](../Page/10月7日.md "wikilink")---這就是希伯來曆的起始日。

目前[猶太教以](../Page/猶太教.md "wikilink")[2017年](../Page/2017年.md "wikilink")9月21日為希伯來曆5778年的開始，並於2018年9月9日結束。

## 簡史

古代猶太人向來習慣以口傳方式來傳授知識，所以曆法沒有文字記載。直到以色列亡國猶太人流散各地後，才有人意識到要用文字將猶太人的知識與文化記載下來。

猶太曆一般認為是在西元[前359年由](../Page/前359年.md "wikilink")的學者所制定。\[1\]當時猶太人因亡國而成為[巴比倫之囚](../Page/巴比倫之囚.md "wikilink")，在受[巴比倫的文化影響下](../Page/新巴比倫王國.md "wikilink")，所制定的正式曆法揉合了[巴比倫曆的太陽計法](../Page/巴比倫曆.md "wikilink")。制定出來的猶太曆在[每19年加入7個閏年的計法](../Page/默冬章.md "wikilink")（雖兩曆置閏位置有異），連每個月份的名稱都與巴比倫曆近似。

以往猶太曆是根據[月亮週期而定](../Page/月亮.md "wikilink")，在吸收了巴比倫曆的太陽計法後，猶太曆的月份就按月亮計算，而年份就按太陽計算，結果就成為一套陰陽合曆的曆法。全世界的猶太教徒都依據猶太曆計算猶太教的節日。

目前的猶太曆在固定的時候加添閏月，他們以19年為一個周期（[默冬週期](../Page/默冬週期.md "wikilink")）插入7個閏月，在第3、6、8、11、14、17和19年加添閏月。而與中國[農曆不同的是](../Page/農曆.md "wikilink")，農曆閏月是根據朔望月與[節氣偏移的位置置放](../Page/節氣.md "wikilink")，一般出現於春夏兩季\[2\]，但猶太曆直接置放於曆年之末。不過即使採用這種插入法，猶太曆曆年平均長度仍比當前平均回歸年長6分40秒，因此繼續下去每216年就將落後一天;
大約每231年就落後[格里曆平均曆年一天](../Page/格里曆.md "wikilink")。

而在[耶穌基督的日子](../Page/耶穌基督.md "wikilink")，猶太人仍然沒有固定的月曆。他們純粹憑經驗去觀察，使每個新的月份開始於新月出現的時候，並且同樣憑着觀察，在有需要時加添閏月。\[3\]据《圣经》记载推算，[耶稣在上帝创世后](../Page/耶稣.md "wikilink")3760年诞生。由于耶稣诞生之年被视为公元纪元，犹太历与公元相差3759年。犹太历3760年即为西元元年。不過由於[西元並無](../Page/西元.md "wikilink")[0年](../Page/0年.md "wikilink")，所以實際上應多算1年即[西元紀年](../Page/西元.md "wikilink")+3761年=犹太历紀年。考虑到犹太历是在公历9月换年，因此在换算犹太历与公历时需分两段计算，在[猶太新年前需减少一年](../Page/猶太新年.md "wikilink")。当换算成犹太历时，在猶太新年后则需增加一年。

犹太民族在历史上对犹太历[歲首有过两种分法](../Page/歲首.md "wikilink")：寺历和民历。寺历指第二圣殿时期(始于公元[前516年](../Page/前516年.md "wikilink"))之前，以《圣经》中记载[摩西率领以色列人离开](../Page/摩西.md "wikilink")[埃及的尼散月为元月的历法](../Page/埃及.md "wikilink")。在犹太人看来，离开埃及是民族摆脱奴役、获得新生的标志，以它为一年之首同時具有文化上與宗教上的意义，寺历有时亦称为教历，即現今所通稱的**犹太教历**。民历指第二圣殿时期以后，一直在民间通用的，以提斯利月为元月的历法。提斯历月是巴勒斯坦地区收获季节，而收获被普遍认为是新的一年生活的开始。现在民历已成为犹太人的通用历法即**犹太民历**或**犹太國历**，提斯利月的第一天成为犹太新年，也是目前的[以色列國曆版本](../Page/以色列.md "wikilink")。

## 猶太曆法及宗教節期圖表

<div style="font-size: 100%;">

| 猶太曆                                                                                      | [英文轉寫名稱](../Page/英文.md "wikilink") | 巴比倫名稱        | 含義     | 缺年日數 | 常年日數                                                                                                                                              | 滿年日數         | 節期                                         | 西曆      |
| ---------------------------------------------------------------------------------------- | ---------------------------------- | ------------ | ------ | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------- | ------------ | ------------------------------------------ | ------- |
| <span style="font-size: small;">1、[尼散月](../Page/尼散月.md "wikilink")                       | Nisan/Nissan                       | *Nisanu*     | 樂月     | 30   | 14日落前 - 21 [逾越節](../Page/逾越節.md "wikilink")                                                                                                       | 三至四月間</span> |                                            |         |
| 2、[以珥月](../Page/以珥月.md "wikilink")                                                       | Iyar                               | *Ayaru*      | 愛月     | 29   | 14 [補逾越節](../Page/補逾越節.md "wikilink")                                                                                                             | 四至五月間        |                                            |         |
| 3、[西彎月](../Page/西彎月.md "wikilink")                                                       | Sivan                              | *Simanu*     | 築月     | 30   | 6 七七節（[五旬節](../Page/五旬節.md "wikilink")）                                                                                                           | 五至六月間        |                                            |         |
| 4、[搭模斯月](../Page/搭模斯月.md "wikilink")                                                     | Tammuz                             | *Du\`uzu*    | 穫月     | 29   |                                                                                                                                                   | 六至七月間        |                                            |         |
| 5、[埃波月](../Page/埃波月.md "wikilink")                                                       | Av                                 | *Abu*        | 熟月     | 30   |                                                                                                                                                   | 七至八月間        |                                            |         |
| 6、[以祿月](../Page/以祿月.md "wikilink")                                                       | Elul                               | *Ululu*      | 酹月     | 29   |                                                                                                                                                   | 八至九月間        |                                            |         |
| 7、[提斯利月](../Page/提斯利月.md "wikilink")                                                     | Tishrei                            | *Tashritu*   | 施月     | 30   | 1 [吹角節](../Page/吹角節.md "wikilink")、10 [贖罪日](../Page/贖罪日.md "wikilink")、15-21 [住棚節](../Page/住棚節.md "wikilink")、22 [嚴肅會](../Page/嚴肅會.md "wikilink") | 九至十月間        |                                            |         |
| 8、[瑪西班月](../Page/瑪西班月.md "wikilink")<sup>**1**</sup>                                     | Cheshvan                           | *Arakhsamna* | 甦月     | 29   | 29                                                                                                                                                | 30           |                                            | 十至十一月間  |
| 9、[基斯流月](../Page/基斯流月.md "wikilink")<sup>**1**</sup>                                     | Kislev                             | *Kislimu*    | 孕月     | 29   | 30                                                                                                                                                | 30           | 25 - 2 提別月[修殿節](../Page/修殿節.md "wikilink") | 十一至十二月間 |
| 10、[提別月](../Page/提別月.md "wikilink")                                                      | Tevet                              | *Tebetu*     | 息月     | 29   |                                                                                                                                                   | 十二至一月間       |                                            |         |
| 11、[細罷特月](../Page/細罷特月.md "wikilink")                                                    | Shevat                             | *Shabatu*    | 洪月     | 30   |                                                                                                                                                   | 一月至二月間       |                                            |         |
| 12L [第一亞達月](../Page/第一亞達月.md "wikilink")<sup>**2**</sup>                                 | Adar I                             | *Adaru*      | 首魔月（閏） | 30   |                                                                                                                                                   | 二至三月間        |                                            |         |
| 12 [亞達月](../Page/亞達月.md "wikilink")／[第二亞達月](../Page/第二亞達月.md "wikilink")<sup>**2**</sup> | Adar / Adar II                     | *Adaru*      | 魔月／次魔月 | 29   | 14 [普珥節](../Page/普珥節.md "wikilink")                                                                                                               | 二至三月間        |                                            |         |

  -
    **註1**：每年的瑪西班月和基斯流月的日數可以不同，視乎該年所需的日數而定。
    **註2**：「第一亞達月」只在[閏年才有](../Page/閏年.md "wikilink")。一般將「第二亞達月」稱為「亞達月」。當閏年時，第6個月之後加插這個閏月，共30日，並稱該月為「第一亞達月」，之前的6月由亞達月改稱為「第二亞達月」。設置閏月的作用是使猶太曆和[太陽曆大致上沒有太大偏差](../Page/太陽曆.md "wikilink")，尤其是確保將尼散月安排在[春天](../Page/春天.md "wikilink")。

</div>

## 曆法表記

犹太历表記為先寫日期，再寫何月，最後寫年份（此處與[西方世界相同](../Page/西方世界.md "wikilink")），例如：**12th
of Iyyar,
5775**（中譯：以珥月12日，5775年/中式寫法：5775年以珥月12日）\[4\]。不過月份並不以數字順序表示，而是直接以名稱表示。

## 注释

## 参考文献

## 外部链接

  - [Mathematics of the Jewish
    Calendar](https://en.wikibooks.org/wiki/Mathematics_of_the_Jewish_Calendar)
  - [互動猶太曆](http://www.hebcal.com/hebcal/)
  - [猶太曆FAQ](http://www.jewfaq.org/calendar.htm)
  - [Perpetual Hebrew / Civil Calendar](http://elkind.net/calendar/)
  - [Jewish
    Calendar](http://www.angelfire.com/pa2/passover/jewish-calendar-hebrew.html)
    Details various Jewish points-of-view about the history of the
    Jewish calendar/Hebrew calendar. Includes several charts.
  - [Hebrew Calendar Science and
    Myth](https://www.webcitation.org/query?id=1256463089170466&url=www.geocities.com/Athens/1584/)
    gives complete rules of the Hebrew calendar and a lot more.
  - [The Jewish Controversy about Calendar
    Postponements](http://www.abcog.org/saadia.htm)
  - [Jewish Calendar with Zmanim - Halachic times and date
    converter](http://www.chabad.org/calendar/) chabad.org
  - [Jewish
    calendar](http://www-spof.gsfc.nasa.gov/stargaze/Sjewcale.htm)
    scientific explanation at the [NASA](../Page/NASA.md "wikilink") web
    site
  - [Article](http://www.kehillaton.com/en/articles_aspects_of_judaic_calendar.asp)
    on Judaic Calendar with reference to seasonal prayers, Mar Shmuel
    and Rav Adda year calculations and comparisons with Julian and
    Gregorian calendars
  - [Jewish Encyclopedia:
    Calendar](http://jewishencyclopedia.com/view.jsp?artid=44&letter=C&search=Year%20of%20Creation)
  - [Calendar](http://www.hebrew4christians.com/Holidays/Calendar/calendar.html)
    Hebrew for Christians website
  - [Karaite
    Holidays](http://www.karaite-korner.org/holiday_dates.shtml)
    [Karaite](../Page/Karaite.md "wikilink") website
  - [Hebrew
    Calendar](https://web.archive.org/web/20061202225942/http://www.cgate.co.il/calendar/calendar_diaspora.asp)Dates
    and Holydays（Diaspora or Israel）
  - [The Hebrew Calendar](http://www.sym454.org/hebrew/)（astronomical
    analysis）
  - [The *Molad* of the Hebrew
    Calendar](http://www.sym454.org/hebrew/molad.htm)（astronomical
    analysis）

## 参见

  - [阴阳历](../Page/阴阳历.md "wikilink")
  - [禧年](../Page/禧年.md "wikilink")

{{-}}

[Category:阴阳历](../Category/阴阳历.md "wikilink")
[希伯来历](../Category/希伯来历.md "wikilink")

1.  [The Jewish calendar](http://www.jewfaq.org/calendar.htm)
2.  [中國農曆與猶太曆的異同](http://winnieil.pixnet.net/blog/post/32282483-中國農曆與猶太曆的異同)
3.  A History of the Jewish People in the Time of Jesus Christ---Emil
    Schürer
4.  [互動猶太曆（英）](http://www.hebcal.com/converter)