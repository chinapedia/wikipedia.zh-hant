[thumb的史密森尼学会总部大楼](../Page/文件:Smithsonian_Building_NR.jpg.md "wikilink")--[史密森尼古堡](../Page/史密森尼古堡.md "wikilink")\]\]
**史密森尼学会**（
）是美国一系列博物馆和研究机构的集合组织。该组织囊括19座博物馆、9座研究中心\[1\]、美术馆和国家动物园以及1.365亿件艺术品和标本\[2\]。也是美国唯一一所由[美国政府资助](../Page/美国政府.md "wikilink")、半官方性质的[第三部門](../Page/第三部門.md "wikilink")[博物馆机构](../Page/博物馆.md "wikilink")，同时也拥有世界最大的博物馆系统和研究联合体\[3\]。管理和经费来源于由美国政府拨款，其他捐助以及自身商店和杂志销售盈利也在其中。该机构大多数设施位于[华盛顿特区](../Page/华盛顿特区.md "wikilink")，此外还有部分设施散布在从[纽约到](../Page/纽约.md "wikilink")[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")，甚至[巴拿马的广阔区域](../Page/巴拿马.md "wikilink")。该机构于1846年成立，资金源于[英国科学家](../Page/英国.md "wikilink")[詹姆斯·史密森对美国的遗赠](../Page/詹姆斯·史密森.md "wikilink")\[4\]。该机构的诸多博物馆除[圣诞节外](../Page/圣诞节.md "wikilink")，全年对公众免费开放\[5\]。

## 历史

由[英国科学家](../Page/英国.md "wikilink")[詹姆斯·史密森价值](../Page/詹姆斯·史密森.md "wikilink")50.8万美元的遗赠捐款\[6\]，根据[美国国会法令于](../Page/美国国会.md "wikilink")1846年创建于美国首都[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")。

史密森本人一生并未到访美国，然而于1828年逝世时，把他的财产遗赠给美国政府，旨在建立一个增进和传播人类知识的学会。美国政府于公元1838年得到了他的财产，[约瑟夫·亨利于](../Page/约瑟夫·亨利.md "wikilink")1846年任史密森学会的第一任会长。亨利指出：

董事会由首席大法官、副总统、3名[参议员](../Page/参议员.md "wikilink")、3名[众议员和](../Page/众议员.md "wikilink")6名非官方人士组成。

## 所設機構

学会下设有這些博物馆和1所国立[动物园](../Page/动物园.md "wikilink")。

[华盛顿特区](../Page/华盛顿特区.md "wikilink")：

  - [弗里尔美术馆](../Page/弗瑞爾藝廊.md "wikilink")（Freer Gallery of Art）

  - [阿瑟·M·萨克勒美术馆](../Page/阿瑟·M·萨克勒美术馆.md "wikilink")（Arthur M. Sackler
    Gallery）

  - [国立美国历史博物馆](../Page/国立美国历史博物馆.md "wikilink")（National Museum of
    American History）

  - [国立自然历史博物馆](../Page/美国国立自然历史博物馆.md "wikilink")（National Museum of
    Natural History）

  - [美國國家肖像畫廊](../Page/美國國家肖像畫廊.md "wikilink")（National Portrait
    Gallery）

  - [國家航空和太空博物館](../Page/國家航空和太空博物館.md "wikilink")（National Air and
    Space Museum）

      - [史蒂文·烏德沃爾哈齊中心](../Page/史蒂文·烏德沃爾哈齊中心.md "wikilink")（Steven F.
        Udvar-Hazy Center）

  - （Hirshhorn Museum）

  - （Arts and Industries Building）

  - [伦威克美术馆](../Page/伦威克美术馆.md "wikilink")（Renwick Gallery）

  - （National Museum of African Art）

  - （Anacostia Museum）

  - （National Museum of the American Indian）

  - [美國藝術博物館](../Page/美國藝術博物館.md "wikilink")（Smithsonian American Art
    Museum）

  - [國立美國郵政博物館](../Page/國立美國郵政博物館.md "wikilink")（National Postal Museum）

  - [美国国家动物园](../Page/美国国家动物园.md "wikilink")（Smithsonian National
    Zoological Park）

此外，在史密森尼内待建的最新博物馆是（National Museum of African American History and
Culture,
NMAAHC），新館的館址同樣選定在国家广场中。實際上已自2003年起運作的該博物館，在自有的館場完工之前是暫時借用美國歷史博物館的場地進行展覽。

[纽约市](../Page/纽约市.md "wikilink")：

  - （Cooper-Hewitt, National Design Museum）

  - （George Gustav Heye Center）：是國立美國原住民博物館的分館。

此外，学会还领导着[威尔逊国际学者中心](../Page/威尔逊国际学者中心.md "wikilink")、[肯尼迪表演艺术中心](../Page/肯尼迪表演艺术中心.md "wikilink")、[史密森尼民俗與文化傳統中心](../Page/史密森尼民俗與文化傳統中心.md "wikilink")（Smithsonian
Center for Folklife and Cultural
Heritage）和若干分布在美国其他地区及一些国家的研究中心、[天文台和科学实验室等机构](../Page/天文台.md "wikilink")\[7\]。

## 著名研究

證實[斑驢並非獨立的物種](../Page/斑驢.md "wikilink")，而是[平原斑馬的亞種](../Page/平原斑馬.md "wikilink")。

## 流行文化

史密森尼學會或其旗下的博物館機構，曾在下述流行文化作品中被提及或被作為劇情場景：

  - 電影
      - 《[博物館驚魂夜2：決戰史密森尼](../Page/博物館驚魂夜2.md "wikilink")》：2009年時上映的美國喜劇電影，全片主要以學會旗下的[美国国立自然历史博物馆為場景](../Page/美国国立自然历史博物馆.md "wikilink")。該片獲史密森史密森尼研究中心拍攝許可，是該中心的首次開放電影實景拍攝，不過必須在開放時間進行，因此拍攝過程中，得面對許多遊客現場圍觀。
      - 《[變形金剛：復仇之戰](../Page/變形金剛：復仇之戰.md "wikilink")》：2009年時上映的美國動作電影。在片中主角一行人為了尋找藏匿在地球的初代變形金剛[天火](../Page/天火_\(角色\).md "wikilink")（JetFire，在片中是一架[SR-71黑鳥式偵察機](../Page/SR-71.md "wikilink")），而潛入[華盛頓杜勒斯國際機場附近的國立航空和太空博物館分館](../Page/華盛頓杜勒斯國際機場.md "wikilink")——[史蒂芬·烏德瓦-哈茲中心](../Page/史蒂芬·烏德瓦-哈茲中心.md "wikilink")（Steven
        F. Udvar-Hazy Center）。
      - 《[美国队长2](../Page/美国队长2.md "wikilink")》：2014年上映的美国超级英雄电影，片中有一个关于美国队长的展览的举办地位于史密森博物馆。
  - 小說
      - 《[失落的秘符](../Page/失落的秘符.md "wikilink")》

## 参考文献

## 外部链接

  - [史密森学会网站](https://www.si.edu)

{{-}}

[Category:美國學會](../Category/美國學會.md "wikilink")
[史密森尼學會](../Category/史密森尼學會.md "wikilink")
[Category:美國博物館](../Category/美國博物館.md "wikilink")
[Category:華盛頓哥倫比亞特區組織](../Category/華盛頓哥倫比亞特區組織.md "wikilink")
[Category:學術出版公司](../Category/學術出版公司.md "wikilink")
[Category:1846年建立的组织](../Category/1846年建立的组织.md "wikilink")

1.

2.

3.

4.
5.
6.
7.