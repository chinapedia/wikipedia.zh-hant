\[1\] \[2\] \[3\] \[4\]

**鄧亞萍**[河南](../Page/河南.md "wikilink")[郑州人](../Page/郑州.md "wikilink")，人民日报社副秘书长,
是一名退役女[乒乓球运动员](../Page/乒乓球.md "wikilink")。从1990年至1997年，她占据[国际乒联的世界女子排行榜首位](../Page/国际乒联.md "wikilink")8年之久。

## 生平

### 运动员生涯

邓亚萍16岁时就夺得了首个世界冠军。从1989年到1997年，她获得了6次[世界乒乓球锦标赛冠军和](../Page/世界乒乓球锦标赛.md "wikilink")4枚[奥运会](../Page/奥运会.md "wikilink")[金牌](../Page/金牌.md "wikilink")。早期打球時的口頭禪是：“漂亮！”

### 退役后

1997年退役之后，邓亚萍在[清华大学获得学士学位](../Page/清华大学.md "wikilink")，[诺丁汉大学获得硕士学位](../Page/诺丁汉大学.md "wikilink")。2006年她开始在[剑桥大学](../Page/剑桥大学.md "wikilink")（[剑桥大学耶稣学院](../Page/剑桥大学耶稣学院.md "wikilink"))攻读博士学位，并于2008年11月29日获得剑桥大学[土地经济学](../Page/土地经济学.md "wikilink")
(Land Economy)博士学位。她的博士答辩论文的题目是：The impact of the Olympic Games on
Chinese development: A multi-disciplinary
analysis（奥林匹克运动会对中国发展的影响：多领域分析）。她说：“当时是这样一个梦想，但觉得自己没有这个机会。后来真的在剑桥读博士，也确实付出了非常多的代价。回过头来想，是非常值得的。对11年的求学生涯作个总结，我觉得只要敢想，并且脚踏实地去做，你还是可以成功的。”\[5\]

1997年，邓亚萍成为[国际奥委会道德委员会的一名代表](../Page/国际奥委会.md "wikilink")。2003年，邓亚萍入选[国际乒联名人堂](../Page/国际乒联名人堂.md "wikilink")。

2009年4月，邓亚萍就任共青团北京市委副书记。\[6\]2010年10月，邓亚萍出任[人民日报社副秘书长](../Page/人民日报.md "wikilink")、人民搜索总经理。她随后接受媒体采访时称“[百度现在是众矢之的](../Page/百度.md "wikilink")，地位和我那时候打球的感觉一样，第一和第二差距比较远。”她表示，百度应该学习中国乒乓球队的养狼计划，让对手强大起来再去打败，“我们本身代表的是国家，你不用打败我们，你应该多帮助我们，多给我们出主意。我们最重要的不是赚钱，而是履行国家职责。”\[7\]之后新闻表明她在试图为人民网打造“搜索国家队”引擎失败，即刻搜索关闭。\[8\]

2010年，由于她的评论引起了争议。一名学生问她“怎样才能迅速晋升？”，她回答说“当你的个人价值与国家利益重叠时，你的价值将无限制地扩大。”。后来，她还说“中华人民共和国成立62年来，人民日报尚未发布一条假新闻。”\[9\]

2016年11月，成为[清华大学苏世民学院的导师](../Page/清华大学.md "wikilink")。\[10\]

## 家庭

邓亚萍2004年与乒乓球运动员[林志刚结婚](../Page/林志刚.md "wikilink")，2006年在[法国](../Page/法国.md "wikilink")[巴黎产下一子](../Page/巴黎.md "wikilink")。

## 参考文献

## 外部链接

  - [新浪资料](http://sports.sina.com.cn/star/deng_yaping/)
  - [中国奥委会资料](https://web.archive.org/web/20061116151937/http://www.olympic.cn/athletes/search_D/2003-11-03/3523.html)
  - [剑桥大学土地经济系](https://web.archive.org/web/20100102091027/http://www.landecon.cam.ac.uk/courses/postgradstudy/introduction.htm#phd)

## 参见

  - [乔红](../Page/乔红.md "wikilink")

[Category:中国奥运乒乓球运动员](../Category/中国奥运乒乓球运动员.md "wikilink")
[Category:中国退役乒乓球运动员](../Category/中国退役乒乓球运动员.md "wikilink")
[Category:清華大學校友](../Category/清華大學校友.md "wikilink")
[Category:剑桥大学耶稣学院校友](../Category/剑桥大学耶稣学院校友.md "wikilink")
[Category:郑州籍运动员](../Category/郑州籍运动员.md "wikilink")
[Yaping亚萍](../Category/邓姓.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奥林匹克运动会乒乓球奖牌得主](../Category/奥林匹克运动会乒乓球奖牌得主.md "wikilink")
[Category:1992年夏季奥林匹克运动会乒乓球运动员](../Category/1992年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:1996年夏季奥林匹克运动会乒乓球运动员](../Category/1996年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:亞洲運動會桌球獎牌得主](../Category/亞洲運動會桌球獎牌得主.md "wikilink")
[Category:1994年亚洲运动会金牌得主](../Category/1994年亚洲运动会金牌得主.md "wikilink")
[Category:1994年亞洲運動會銀牌得主](../Category/1994年亞洲運動會銀牌得主.md "wikilink")
[Category:1990年亞洲運動會金牌得主](../Category/1990年亞洲運動會金牌得主.md "wikilink")
[Category:1990年亞洲運動會銀牌得主](../Category/1990年亞洲運動會銀牌得主.md "wikilink")
[Category:国际奥林匹克委员会委员](../Category/国际奥林匹克委员会委员.md "wikilink")
[Category:诺丁汉大学校友](../Category/诺丁汉大学校友.md "wikilink")

1.  [1992年巴塞罗那奥运会乒乓球女单决赛](http://space.tv.cctv.com/video/VIDE1208274517151180)

2.  [1992年巴塞罗那奥运会乒乓球女双决赛](http://v.youku.com/v_show/id_XNTI1NDI0NDg=.html)
3.  [1996年亚特兰大奥运会乒乓球女单决赛](http://v.youku.com/v_show/id_XNTMzNDYxMjA=.html)
4.  [1996年亚特兰大奥运会乒乓球女双决赛](http://v.youku.com/v_show/id_XNTMzNDYxMDA=.html)
5.  [邓亚萍十年圆梦
    戴博士帽如拿奥运金牌一样激动](http://news.xinhuanet.com/sports/2008-11/30/content_10432366.htm)

6.
7.
8.  <http://roll.sohu.com/20140328/n397386875.shtml>
9.
10.