[Franz_von_Paula_Schrank_-_Jesuit_und_Botaniker.jpg](https://zh.wikipedia.org/wiki/File:Franz_von_Paula_Schrank_-_Jesuit_und_Botaniker.jpg "fig:Franz_von_Paula_Schrank_-_Jesuit_und_Botaniker.jpg")
**弗蘭茲·馮·保拉·施蘭克**（[德語](../Page/德語.md "wikilink")：；），[德國](../Page/德國.md "wikilink")[植物學家暨](../Page/植物學.md "wikilink")[昆蟲學家](../Page/昆蟲學.md "wikilink")。

施蘭克於1809年到1832年期間出任首位[慕尼黑植物園主任](../Page/慕尼黑.md "wikilink")。

## 著作

[Titel_Beyträge_zur_Naturgeschichte.jpg](https://zh.wikipedia.org/wiki/File:Titel_Beyträge_zur_Naturgeschichte.jpg "fig:Titel_Beyträge_zur_Naturgeschichte.jpg")

  - *Beiträge zur Naturgeschichte* （Augsburg, 1776）
  - *Vorlesungen über die Art die Naturgeschichte zu studieren*
    （Ratisbohn, 1780）
  - *Enumeratio insectorum Austriæ indigenorum* （Wien, 1781）
  - *Anleitung die Naturgeschichte zu studieren* （München, 1783）
  - *Naturhistorishce Briefe über Österreich, Salzburg, Passau und
    Berchtsgaden* （gesammelte Werke, zusammen mit , Salzburg, 1784-1785）
  - *Anfangsgründe der Botanik* （München, 1785）
  - *Baiersche Reise …* （1786）
  - *Verzichniss der bisher hinlaneglich bekannten Eingeweidewürmer,
    nebts einer Abhandlungen über ihre Anverwandschaften* （München,
    1787）
  - *Bayerische Flora* （München, 1789）
  - *Primitiæ floræ salisburgensis, cum dissertatione prævia de
    discrimine plantarum ab animalibus* （Frankfurt, 1792）
  - *Abhandlungen einer Privatgesellschaft vom Naturforschern und
    Ökonomen in Oberteutschland* （München, 1792）
  - *Anfangsgründe der Bergwerkskunde* （Ingolstadt, 1793）
  - *Reise nach den südlichen Gebirgen von Bayern, in Hinsicht auf
    botanische und ökonomische Gegenstände* （München, 1793）
  - *Flora monacensis* （München, 1811-1820）
  - *Plantæ rariores horti academici Monacensis descriptæ et iconibus
    illustratæ* （1819）
  - *Sammlung von Zierpflanzen* （1819）

## 注釋

## 外部連結

  - [Entry in the Catholic
    Encyclopedia](http://www.newadvent.org/cathen/13590c.htm)

[Category:德國昆蟲學家](../Category/德國昆蟲學家.md "wikilink")
[Category:德國植物學家](../Category/德國植物學家.md "wikilink")
[Category:巴伐利亞人](../Category/巴伐利亞人.md "wikilink")