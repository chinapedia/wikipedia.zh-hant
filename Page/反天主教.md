{{ RoughTranslation|time=2013-05-08}}

**反天主教**是一种针对[天主教徒的](../Page/天主教徒.md "wikilink")[歧视](../Page/歧视.md "wikilink")、敌意或[偏见](../Page/偏见.md "wikilink")，也可用来指针对[天主教會的](../Page/天主教會.md "wikilink")[宗教迫害](../Page/宗教迫害.md "wikilink")。

在16世紀初期，天主教會在欧洲长期的权势受到了挑战和竞争，激起针对[教宗和天主教](../Page/教宗.md "wikilink")[神职人员权力的敌对态度](../Page/神职人员.md "wikilink")。这种敌意称为[反教权主义](../Page/反教权主义.md "wikilink")。[宗教改革又带来对其属灵权威提出异议](../Page/宗教改革.md "wikilink")，面临前所未有的危机。此後[新教徒為主的國家曾禁止天主教活動](../Page/新教徒.md "wikilink")。

当代的反天主教表现为许多形式，包括对天主教少数派的迫害，政府攻击天主教信仰、歧视，和恶意攻击神职人员與信徒。加上[世俗主義的興起](../Page/世俗主義.md "wikilink")，反天主教的行動包括批評教廷的保守取態，如反對使用[保險套等](../Page/保險套.md "wikilink")[避孕行為](../Page/避孕.md "wikilink")、[墮胎權和](../Page/墮胎.md "wikilink")[同性戀權利](../Page/同性戀權利.md "wikilink")，也有人籍天主教神職人員[性侵犯兒童等醜聞批評天主教](../Page/性侵犯.md "wikilink")。

## 起源

### 新教国家

[Lucas_Cranach_-_Antichrist.png](https://zh.wikipedia.org/wiki/File:Lucas_Cranach_-_Antichrist.png "fig:Lucas_Cranach_-_Antichrist.png")，指教皇是敵基督。\]\]

从[马丁·路德开始](../Page/马丁·路德.md "wikilink")，[新教就攻击教皇象征着](../Page/新教.md "wikilink")[敌基督的权势](../Page/敌基督.md "wikilink")，而罗马天主教则象征《[启示录](../Page/启示录.md "wikilink")》中的[巴比伦大淫妇](../Page/巴比伦大淫妇.md "wikilink")。许多新教宗派的信条中明确地将教皇视为敌基督：

[路德宗](../Page/路德宗.md "wikilink")《[西敏斯特信仰信条](../Page/西敏斯特信仰信条.md "wikilink")》：

  -
    The London Baptist Confession of 1689:
    26.4. 主耶稣基督 is the Head of the church, in whom, by the appointment
    of the Father, all power for the calling, institution, order or
    government of the church, is invested in a supreme and sovereign
    manner; neither can the Pope of Rome in any sense be head thereof,
    but is that antichrist, that man of sin, and son of perdition, that
    exalteth himself in the church against Christ.

译:主耶稣基督是教会的元首，在他有从圣天父来的允诺，所有教会的传唤，职权，命令或是教会政府般的权力，都是基础在唯一至上的主权者(译注:此处显然强调人世权力根本在神的任凭，不是人自己为权威)。从没有一个罗马教宗(有权)可以在或任何条件下去代之，只是就是那[敌基督](../Page/敌基督.md "wikilink")，正是一个罪人又是灭亡之子的，其自封为教会崇高的人正是敌对着基督。

#### 英格兰

[英格兰的反天主教起源于](../Page/英格兰.md "wikilink")[亨利八世的](../Page/亨利八世.md "wikilink")[英格兰宗教改革](../Page/英格兰宗教改革.md "wikilink")，而由否定教宗權威的[英國君主領導的](../Page/英國君主.md "wikilink")[英國國教派最後演變成一個具有強烈排外及統治性質的宗教](../Page/英國國教派.md "wikilink")，對天主教徒和[清教徒加以迫害](../Page/清教徒.md "wikilink")，並促使十七世紀初清教徒乘坐[五月花號前往新大陸](../Page/五月花號.md "wikilink")。

十七世紀，英王[查理一世雖然表示信仰新教](../Page/查理一世.md "wikilink")，但他下令全國採用接近天主教的儀式，最後引爆了[清教徒革命](../Page/清教徒革命.md "wikilink")，信仰清教的[圓顱黨首腦](../Page/圓顱黨.md "wikilink")[克倫威爾最後戰勝了查理一世的](../Page/克倫威爾.md "wikilink")[騎士黨](../Page/騎士黨.md "wikilink")，並將查理一世斬首示眾；其子[查理二世於](../Page/查理二世.md "wikilink")1660年復辟。後來篤信天主教的[詹姆斯二世因為](../Page/詹姆斯二世.md "wikilink")[光榮革命遭到推翻](../Page/光榮革命.md "wikilink")。

自光榮革命後，英国皇室成员一旦加入天主教或与天主教徒结婚，即视为自动放弃继承王位的机会。\[1\]但在2011年10月28日，國會修改王位繼承法，取消了對皇室成員與天主教徒結婚的限制，不過王位繼承人依然必須是英國聖公會的成員。\[2\]

#### 爱尔兰

在爱尔兰，罗马天主教徒占多数，但在[亨利八世的](../Page/亨利八世.md "wikilink")[英格兰宗教改革以后](../Page/英格兰宗教改革.md "wikilink")，遭受迫害。一直到十九世紀，信奉天主教的愛爾蘭移民也遭到當地的[白人盎格魯-撒克遜新教徒上流階層迫害](../Page/白人盎格魯-撒克遜新教徒.md "wikilink")，譬如[北愛爾蘭的愛裔造船工人和美國的愛裔鐵路工人曾受到英美雇主的壓迫](../Page/北愛爾蘭.md "wikilink")。

二十世紀的[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")，由於信仰天主教的激進政黨[新芬黨和激進武裝組織](../Page/新芬黨.md "wikilink")[愛爾蘭共和軍](../Page/愛爾蘭共和軍.md "wikilink")（Irish
Republican Army,
IRA）為了脫離英國並與[愛爾蘭共和國統一](../Page/愛爾蘭共和國.md "wikilink")，遂與英國當局爭鬥許久，最後IRA于1998年簽署停火協議。

#### 苏格兰

16世纪的[苏格兰](../Page/苏格兰.md "wikilink")[宗教改革使得苏格兰转向了](../Page/宗教改革.md "wikilink")[喀爾文主義](../Page/喀爾文主義.md "wikilink")（[苏格兰长老会](../Page/苏格兰长老会.md "wikilink")），革命导致对罗马天主教强烈的敌意。
igh Anglicism came under intense persecution also after [Charles
I](../Page/Charles_I.md "wikilink") attempted to reform the Church of
Scotland. However, the attempted reforms caused chaos as they were seen
as being too Catholic- based heavily on sacraments and ritual.

Over the course of later medieval and early modern history violence
against Catholics has broken out, often resulting in deaths, such as the
torture of [Saint John
Ogilvie](../Page/Saint_John_Ogilvie.md "wikilink") and execution of a
[Jesuit](../Page/Jesuit.md "wikilink") priest.

In the last 150 years, Irish immigration to Scotland increased
dramatically and at the beginning of the immigration period Catholics
were treated like second class citizens. However, as time has gone on
Scotland has become much more open to other religions and Catholics have
seen the formation of separate schools which still receive council
funding. The [Orange Order](../Page/Orange_Order.md "wikilink") has
grown in numbers in recent times however it is attributed to the rivalry
between [Rangers](../Page/Rangers_F.C..md "wikilink") and
[Celtic](../Page/Celtic_F.C..md "wikilink") football clubs as opposed to
actual hatred of Catholicism \[3\].

Cooperation between [The Kirk](../Page/The_Kirk.md "wikilink") and the
Catholic Church in Scotland has grown greatly in recent times as both
churches are moving to eradicate sectarian violence from football and
work together to fight poverty. The Moderator of the [Church of
Scotland](../Page/Church_of_Scotland.md "wikilink") and Cardinal Patrick
O'Brien both attended a follow up to the [G8](../Page/G8.md "wikilink")
summit in [May](../Page/May.md "wikilink")
[2007](../Page/2007.md "wikilink").

Scotland is considered by some to be the historic Catholic heartland of
the modern day United Kingdom. \[4\]

#### 美国

自從美國建國以來，雖然[美國憲法規定保障](../Page/美國憲法.md "wikilink")[宗教自由和平等](../Page/宗教自由.md "wikilink")，但[美國總統在就職典禮上握住](../Page/美國總統.md "wikilink")[聖經宣誓就職](../Page/聖經.md "wikilink")，是以新教的儀軌進行。

在19世紀，隨著大量歐洲移民的進入，包含信仰天主教的[德國](../Page/德國.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[拉丁美洲移民](../Page/拉丁美洲.md "wikilink")，他們除了備受美國本土信仰新教的主流族群排擠和[歧視外](../Page/歧視.md "wikilink")，也造成了貧困、犯罪、失業等生活問題，更在1929年[經濟大蕭條時期受害最深](../Page/經濟大蕭條.md "wikilink")，因為他們的社經地位比信仰新教的美國主流族群來得低。

### 天主教国家

反天主教
是一个反对以天主教为主的宗教权力制度的历史运动，该运动在公众政治的所有方面都有显著影响影响。该运动代表着更加积极的反对和抗议而不是仅仅[laïcité](../Page/laïcité.md "wikilink").
The goal of anti-clericalism is sometimes to reduce religion to a purely
private belief-system with no public profile or influence. However, many
times it has included outright suppression of all aspects of faith.

`Anti-clericalism has at times been violent, leading to murders and the desacration, destruction and seizure of church property.`

Anti-clericalism in one form or another has existed through most of
Christian history, and is considered to be one of the major popular
forces underlying the 16th Century
[reformation](../Page/reformation.md "wikilink"). Some of the
philosophers of the
[Enlightenment](../Page/Age_of_Enlightenment.md "wikilink"), including
[Voltaire](../Page/Voltaire.md "wikilink"), continually attacked the
Catholic Church, its leadership and priests claiming moral corruption of
many of its clergy. These assaults in part led to the suppression of the
[Jesuits](../Page/Jesuits.md "wikilink"), and played a major part in the
wholesale attacks on the very existence of the Church during the [French
Revolution](../Page/French_Revolution.md "wikilink") in the [Reign of
Terror](../Page/Reign_of_Terror.md "wikilink") and the program of
[dechristianization](../Page/Dechristianisation_of_France_during_the_French_Revolution.md "wikilink").
Similar attacks on the Church occurred in
[Mexico](../Page/Cristero_War.md "wikilink") and in
[Spain](../Page/Red_Terror_\(Spain\).md "wikilink") in20世纪。

[`France`](../Page/France.md "wikilink")`'s `[`Third``
 ``Republic`](../Page/French_Third_Republic.md "wikilink")` was cemented by anti-clericalism, the desire to secularise the State and social life, faithful to the `[`French``
 ``Revolution`](../Page/French_Revolution.md "wikilink")`.`\[5\]

Mexico's [Cristero War](../Page/Cristero_War.md "wikilink") of 1926-1929
stemmed from [Plutarco Elías
Calles](../Page/Plutarco_Elías_Calles.md "wikilink")'s denial of
priests rights and martyred many [Saints of the Cristero
War](../Page/Saints_of_the_Cristero_War.md "wikilink"). Events relating
to this were famously portrayed in the novel [The Power and the
Glory](../Page/The_Power_and_the_Glory.md "wikilink") by [Graham
Greene](../Page/Graham_Greene.md "wikilink"). The persecution of
Catholics was most severe in the state of Tabasco under the Governor
[Tomás Garrido Canabal](../Page/Tomás_Garrido_Canabal.md "wikilink").
Under the rule of Garrido many priests were killed, all Churches in the
state were closed and priests were forced to marry or flee at risk of
their lives.

[François](../Page/François_Duvalier.md "wikilink") and [Jean-Claude
Duvalier](../Page/Jean-Claude_Duvalier.md "wikilink")'s [family
dictatorship](../Page/family_dictatorship.md "wikilink") of
[Haiti](../Page/Haiti.md "wikilink") wanted to weaken or control the
Roman Catholic Church by bringing [Vodou](../Page/Vodou.md "wikilink")
"openly into the political process", according to Michel S. LaGuerre in
*Voodoo and Politics in Haiti*.

Anti-clericalism in Spain at the start of the [Spanish Civil
War](../Page/Spanish_Civil_War.md "wikilink") resulted in the killing of
almost 7,000 clergy, the destruction of hundreds of churches and the
persecution of lay people in Spain's [Red
Terror](../Page/Red_Terror_\(Spain\).md "wikilink").

### 波兰

Catholicism in Poland, the religion of the vast majority of the
population, was persecuted under the Communist regime from the 1950s.
Current Stalinist ideology claimed the Church and religion in general
was about to disintegrate. To begin with [Archbishop
Wyszyński](../Page/Stefan_Cardinal_Wyszyński.md "wikilink") entered
into an agreement with the Communist authorities, which was signed on
[14 February](../Page/14_February.md "wikilink")
[1950](../Page/1950.md "wikilink") by the Polish episcopate and the
government. The Agreement regulated the matters of the Church in Poland.
However in May of that year, the Sejm breached the Agreement by passing
a law for the confiscation of Church property.

On [12 January](../Page/12_January.md "wikilink")
[1953](../Page/1953.md "wikilink"), Wyszyński was elevated to the rank
of cardinal by Pius XII as another wave of persecution began in Poland.
When the bishops voiced their opposition to state interference in
ecclesiastical appointments, mass trials and the internment of priests
began - the cardinal being among the number of its victims. On [25
September](../Page/25_September.md "wikilink")
[1953](../Page/1953.md "wikilink") he was imprisoned at Grudziądz, and
later placed under house arrest in monasteries in Prudnik near Opole and
in Komańcza in the Bieszczady Mountains. He was not released until [26
October](../Page/26_October.md "wikilink")
[1956](../Page/1956.md "wikilink").

## 当代的反天主教

### 文学与通俗媒体

`Anti-Catholic stereotypes are a long-standing feature of Anglo-Saxon literary, sub-literary and even pornographic traditions. `[`Gothic``
 ``fiction`](../Page/Gothic_fiction.md "wikilink")` is particularly rich in this regard with the figure of the lustful priest, the cruel abbess, the immured nun and the sadistic inquisitor appearing in such works as `*[`The``
 ``Italian`](../Page/The_Italian.md "wikilink")*` by `[`Anne``
 ``Radcliffe`](../Page/Anne_Radcliffe.md "wikilink")`, `*[`The``
 ``Monk`](../Page/The_Monk.md "wikilink")*` by `[`Matthew``
 ``Lewis`](../Page/Matthew_Lewis.md "wikilink")`, `*[`Melmoth``
 ``the``
 ``Wanderer`](../Page/Melmoth_the_Wanderer.md "wikilink")*` by `[`Charles``
 ``Maturin`](../Page/Charles_Maturin.md "wikilink")` and "`[`The``
 ``Pit``   ``and``   ``the``
 ``Pendulum`](../Page/The_Pit_and_the_Pendulum.md "wikilink")`" by `[`Edgar``
 ``Allan``   ``Poe`](../Page/Edgar_Allan_Poe.md "wikilink")`. `\[6\]` `

` Such gothic fiction may have inspired `[`Rebecca``
 ``Reed`](../Page/Rebecca_Reed.md "wikilink")`'s `*`Six``   ``Months``
 ``in``   ``a``
 ``Convent`*` which describes her alleged captivity by an Ursuline order near Boston in 1832 `\[7\]` `\[8\]`Her claims inspired an angry mob to burn down the convent, and her narrative, released three years later as the rioters were tried, famously sold 200,000 copies in one month.  In another bestselling fraudulent exposé, `*`Awful``
 ``Disclosures``   ``of``   ``the``   ``Hotel-Dieu``
 ``Nunnery`*`, `[`Maria``
 ``Monk`](../Page/Maria_Monk.md "wikilink")` claimed that the convent served as a harem for Roman Catholic `[`priests`](../Page/priest.md "wikilink")`, and that any resulting children were murdered after baptism.  Col. William Stone, a New York city newspaper editor, along with a team of Protestant investigators, made inquiry into the claims of Monk, inspecting the convent in the process.  Col. Stone's investigation concluded there was no evidence that Maria Monk "had ever been within the walls of the cloister". `

`Reed's book became a best-seller, and Monk or her handlers hoped to cash in on the evident market for anti-Catholic horror fiction by their offering. The tale of Maria Monk was, in fact, clearly modelled on the gothic novels that were popular in the early 19th century, a literary genre that had already been used for anti-Catholic sentiments in works such as Matthew Lewis' The Monk. Monk's story explores the genre-defining elements of a young, innocent woman being trapped in a remote, old, and gloomily picturesque estate; she learns the dark secrets the place contains, and after harrowing adventures makes her escape. `\[9\]` `\[10\]

[Pornography](../Page/Pornography.md "wikilink") has been the vehicle
for anti-Catholic sentiments from [Denis
Diderot](../Page/Denis_Diderot.md "wikilink")'s *[La
Religieuse](../Page/La_Religieuse.md "wikilink")* (1798), to
contemporary [nunsploitation](../Page/nunsploitation.md "wikilink")
films.

`In a chapter of `[`Fyodor``
 ``Dostoevsky`](../Page/Fyodor_Dostoevsky.md "wikilink")`'s `*[`The``
 ``Brothers``
 ``Karamazov`](../Page/The_Brothers_Karamazov.md "wikilink")*` called `*[`The``
 ``Grand``
 ``Inquisitor`](../Page/The_Grand_Inquisitor.md "wikilink")*`, the Church convicts a resurrected Jesus Christ of heresy and is portrayed as a servant of Satan.  (Interestingly the book is said to be well-liked by `[`Pope``
 ``Benedict``
 ``XVI`](../Page/Pope_Benedict_XVI.md "wikilink")`, former head of the `[`Congregation``
 ``for``   ``the``   ``Doctrine``   ``of``   ``the``
 ``Faith`](../Page/Congregation_for_the_Doctrine_of_the_Faith.md "wikilink")`, a successor office to the `[`Supreme``
 ``Sacred``   ``Congregation``   ``of``   ``the``   ``Roman``   ``and``
 ``Universal``
 ``Inquisition`](../Page/Supreme_Sacred_Congregation_of_the_Roman_and_Universal_Inquisition.md "wikilink")`.)`` In  `*[`Notes``
 ``from``
 ``Underground`](../Page/Notes_from_Underground.md "wikilink")*` the main character thinks about making the world a better place by eliminating or overthrowing the Pope. `
` `

[Dan Brown](../Page/Dan_Brown.md "wikilink")'s best-selling novel *[The
Da Vinci Code](../Page/The_Da_Vinci_Code.md "wikilink")* depicts the
Roman Catholic Church as determined to hide the truth about Jesus
Christ. An article in an April 2004 issue of *National Catholic
Register* maintains that the "The Da Vinci Code claims that Catholicism
is a big, bloody, woman-hating lie created out of pagan cloth by the
manipulative Emperor of Rome". An earlier book by Brown *[Angels and
Demons](../Page/Angels_and_Demons.md "wikilink")*, depicts the Church as
involved in an elemental battle with
[Freemasonry](../Page/Freemasonry.md "wikilink").

### 现代反天主教辩论

除了标准的新教论战的比喻，天主教反基督和巴比伦等现代反天主教爭議主题的妓女列入其中，指责谋求世界霸权的教会异端，批評偶像崇拜和阴谋论的指责。

标准新教的论战是由美国[福音派作家代表牧师约翰](../Page/福音派.md "wikilink")·道林，在他最畅销的書中，他指责罗马天主教会是“基督的全真教会的恶毒的敌人
- 她拥有无人认领被称为基督教堂 -
但是，随着长期腐败，邪恶的人也看穿她的三冠王，她是[敵基督的](../Page/敵基督.md "wikilink")。\[11\]

[希斯录的](../Page/希斯录.md "wikilink")《[两个巴比伦](../Page/两个巴比伦.md "wikilink")》（1858年）宣称罗马天主教发源于[巴比伦神秘宗教](../Page/巴比伦.md "wikilink")，其实行带有异教徒的色彩（偶像崇拜）。

背弃天主教的神父Charles Chiniquy著有50 Years In The Church of Rome和The Priest, the
Woman and the Confessional（1885年），也将罗马天主教描绘成异教。

Avro的曼哈顿的书“梵蒂冈的大屠杀”（1986）“，”梵蒂冈十亿”（1983）和”梵蒂冈莫斯科联盟”（1982）认为，教会的工程师战争统治世界。

希斯洛普的和Chiniquy的十九世纪​​论战和一系列的基础上由著名现代反天主教和漫画书的Avro曼哈顿工作的组成部分[传道杰克小鸡谁也指责教皇配套使用的](../Page/传道.md "wikilink")[共产主义](../Page/共产主义.md "wikilink")，[耶稣会士煽动革命和策划的](../Page/耶稣会.md "wikilink")[大屠杀的](../Page/大屠杀.md "wikilink")。据小鸡，天主教会是“巴比伦妓女”中的启示录的图书提及，并会带来撒旦[新世界秩序之前](../Page/新世界秩序.md "wikilink")，它是由耶稣基督破坏。小鸡声称天主教浸润和企图摧毁或损坏的一切其他宗教和教会，以及它使用各种手段，包括诱惑，取景和谋杀压制批评。画上的[阿尔贝托·里维拉的想法](../Page/阿尔贝托·里维拉.md "wikilink")，小鸡还声称，天主教会有助于塑造伊斯兰教作为一种工具来引诱人们远离基督教在他所谓的伊斯兰教的阴谋。

[理查德·道金斯在他的畅销书](../Page/理查德·道金斯.md "wikilink")[上帝错觉](../Page/上帝错觉.md "wikilink")（2006年）断言，一个天主教教养促进内疚人（167页）的“半永久状态病态内疚由罗马天主教遭受具备正常的人性弱点和不智力正常的。讨论文书的性虐待在爱尔兰的后果，他还建议，“可怕的性虐待毫无疑问是，损害是可以说是比把孩子送進天主教会，在第一时间造成长期的心理伤害少”（317頁）
。

### 反天主教讽刺

从宗教改革的时候到今天，天主教会一直是被讽刺和幽默的目标。这样的讽刺和幽默的范围从轻微艳舞恶性攻击。天主教神职人员组织，如[天主教联盟监视器](../Page/天主教联盟_\(美国\).md "wikilink")，特别是圍攻和贬义的指責，表达對他们的反对和抗议。

## 各国反天主教

### 美国

[美国圣公会史学家](../Page/美国圣公会.md "wikilink")[菲利浦·詹金斯](../Page/菲利浦·詹金斯.md "wikilink")，在書中反天主教：最后的可接受偏见（牛津大学出版社2005年国际标准书号0-19-515480-0）认为，一些人谁否则避免种族，宗教，种族或性别群体得罪成员都毫无保留的宣泄教徒的仇恨。在20世纪早期，美国哈佛大学教授老阿瑟·施莱辛格米反对对天主教徒的偏见，“最深的偏见在美国人民的历史”的\[12\]耶鲁大学教授彼得Viereck曾评论说：“反天主教是自由派的反犹太主义。”\[13\]

2006年5月12日的[盖洛普民意测验表明有](../Page/盖洛普民意测验.md "wikilink")30%的美国人对罗马天主教信仰持不赞成态度，57%的美国人对罗马天主教信仰持赞成态度。不赞成的比率高于2000年，但低于2002年。當中白人新教徒及天主教徒對天主教持肯定態度，而无宗教信仰者及非基督徒則多數持否定態度，他們對罗马天主教的教义持負面看法，而神職人員的性醜聞及贪婪，罗马天主教对同性恋的观点，和独身神职人员使那些對罗马天主教持負面看法的名单上。\[14\]2012年一个较新的盖洛普民意调查显示只有4％的美国人對罗马天主教徒持“非常负面”的看法\[15\]

2000年，莱斯Balsiger在[俄勒冈州多个城市进行反天主教运动](../Page/俄勒冈州.md "wikilink")\[16\]\[17\]
。

#### 性、避孕和堕胎

许多[女权运动和](../Page/女权运动.md "wikilink")[LGBT](../Page/LGBT.md "wikilink")（[女同性恋者](../Page/女同性恋者.md "wikilink")、[男同性恋者](../Page/男同性恋者.md "wikilink")、[双性恋者与](../Page/双性恋者.md "wikilink")[跨性别者](../Page/跨性别者.md "wikilink")）激进分子批评天主教对于性、避孕和堕胎的观点。与传统的反天主教形式不同，反天主教者并不仅仅是因为它是天主教而反对，而是针对其特别的立場，即使天主教相對於其他更保守的新教教派來說相對開明，但仍持反對看法。

反天主教活动人士，如[威廉·多诺霍引用这是反天主教偏执](../Page/威廉·多诺霍.md "wikilink")，反对者争辩说，一个组织的实际位置上的问题，批评不构成偏执。

在2007年1月30日，美國民主黨候選人[约翰·爱德华兹](../Page/约翰·爱德华兹.md "wikilink")[2008年美國总统选舉初選期間聘请了阿曼达作为广告活动的顧問](../Page/2008年美國总统选舉.md "wikilink")\[18\]反對團體對她采取了进攻，以她的一些早期作品，她声称教会试图“证明公司厌女症与\[...\]上古神话”\[19\]，并公开要求爱德华兹竞选團體结束马科特的任命。马科特随后辞职，理由是受到“性暴力，威胁邮件”。\[20\]

一些同性恋激進活动家与罗马天主教关系十分惡劣。在1989年艾滋病联盟成员和妇女組織动员人士打乱周日[弥撒](../Page/弥撒.md "wikilink")，在[圣帕特里克大教堂抗议教会对](../Page/圣帕特里克大教堂.md "wikilink")[同性恋](../Page/同性恋.md "wikilink")，[安全性行為和使用](../Page/安全性行為.md "wikilink")[避孕套的保守態度](../Page/避孕套.md "wikilink")。抗议者其後被捕，至少一名抗议者扔已使用的安全套在教堂祭坛，[亵渎](../Page/亵渎.md "wikilink")[彌撒](../Page/彌撒.md "wikilink")。\[21\]

#### 娱乐业

按照耶稣会神父詹姆士·马丁的说法，美国娱乐业对于天主教有“两个头脑”，他说道：

马丁又说，虽然天主教有不可抵御的魅力，但是娱乐业也最为显著地轻视天主教。"好像制片、导演、编剧和电影制片人的感觉迫使被鼓吹他们与他们保持在这样的奴隶制度的差异建立自己的知识产权的诚意。不过，马丁认为，“正是电视已经证明了反天主教的題材是写作中最肥沃的土壤。当牧师他们出现在电视节目，通常显示为恋童癖者或白痴，也很少看到在做自己的工作。”\[22\]

在巴西度假題材的電影'我们的阿帕雷西达圣母'，在被称为一个小插曲普世教会王国神＃的“踢圣的”“踢了圣徒”的主教五旬节普世教会的神的王国​​多次击败说守护神的雕像。\[23\]

已经带来了积极的反天主教问题中脱颖而出的一组是關於天主教联盟的宗教和民权。成立于1975年，由维吉尔百隆，[威廉·多诺霍的领导下](../Page/威廉·多诺霍.md "wikilink")，组织天主教聯盟，反擊那些电影對天主教的批評及嘲諷。天主教联盟已经对这种娱乐产品如神圣的没什么，牧师和科珀斯克里斯蒂提出訴訟。1999年10月，他们在[纽约时报购买了一整版的广告](../Page/纽约时报.md "wikilink")，声讨[名利场杂志对其涉嫌向反天主教的團體倾斜](../Page/名利场（杂志）.md "wikilink")。

2004年多诺霍宣称“好莱坞被仇恨基督教、特别是仇恨天主教的犹太人长期控制。\[...\]好莱坞像喜歡[肛交](../Page/肛交.md "wikilink")\[24\]

2006年10月，明尼苏达州的天主教上层领袖呼吁明尼苏达大学校长罗伯特·Bruininks重新考虑大学容許一部反天主教的爭議舞台劇上映，他们被视为反天主教争议发挥了难得的一步。
“教皇和女巫”，讽刺教皇的偏执，药物腐坏的白痴和梵蒂冈的腐败，引起了愤怒，秋天全国天主教团体和一些当地的教會、圣保罗和明尼阿波利斯总教区大主教哈里·弗林，以及来自克鲁克斯顿主教薇诺娜，写信给州的160万天主教徒。他们敦促Bruininks重新思考應否播出有關戏剧。丹尼斯·麦格拉思，圣保罗和明尼阿波利斯总教区的发言人，说他不记得国家的主教曾提出这样的要求。该大学表示，它没有计划停止舞台劇播放。\[25\]

### 苏格兰

近年来，足球一直是很多在苏格兰的宗派偏见的一幕。通过格拉斯哥对手[凯尔特人足球俱乐部](../Page/凯尔特人足球俱乐部.md "wikilink")（罗马天主教有关）和[格拉斯哥流浪](../Page/格拉斯哥流浪.md "wikilink")（新教）的宗教背景对鸿沟的两边​​引发了更大的愤怒，变成宗派偏见和衝突。

### 俄罗斯

天主教會被認为在社会中發揮了宗教的作用，但在其他时间，指[俄罗斯东正教会被國家操纵](../Page/俄罗斯东正教会.md "wikilink")，这是一个用以打击罗马天主教。

### 以色列

以色列的反天主教起源于1948年这个犹太人国家成立之时。当时有几个天主教徒占多数的村庄，例如Kafr
Bir'im和Iqrit，都被以色列国防军用强制手段减少人口。\[26\]1948年以后，天主教神父被驱逐出这个国家，许多教堂被占用、关闭或强迫出售。最近以色列拒绝并试图阻止任命天主教會任命巴勒斯坦人為主教。\[27\]
以色列政府1998努力阻止圣座任命加利利总主教被梵蒂冈和其他国家的谴责。以色列怀疑和敌视天主教神职人员，导致事故，如2002年10月的拘留和骚扰希臘大主教埃利亚斯Chacour和大主教布特罗斯Mouallem，防止他离开耶路撒冷，以防止他参加在伦敦的宗教会议。\[28\]
\[29\]近年以色列政府與教廷關係改善。

## 参考文献

## 延伸阅读

<div class="references-small" style="-moz-column-count:2; column-count:2;">

  - Anbinder; Tyler *Nativism and Slavery: The Northern Know Nothings
    and the Politics of the 1850s* 1992
  - Bennett; David H. *The Party of Fear: From Nativist Movements to the
    New Right in American History* University of North Carolina Press,
    1988
  - Billingon, Ray. *The Protestant Crusade, 1830-1860* (1938)
  - Blanshard; Paul.*American Freedom and Catholic Power* Beacon Press,
    1949
  - Thomas M. Brown, "The Image of the Beast: Anti-Papal Rhetoric in
    Colonial America", in Richard O. Curry and Thomas M. Brown, eds.,
    *Conspiracy: The Fear of Subversion in American History* (1972),
    1-20.
  - Steve Bruce, *No Pope of Rome: Anti-Catholicism in Modern Scotland*
    (Edinburgh, 1985).
  - [Elias Chacour](../Page/Elias_Chacour.md "wikilink"): "*Blood
    Brothers. A Palestinian Struggles for Reconciliation in the Middle
    East*" ISBN 978-0-8007-9321-0 with Hazard, David, and [Baker III,
    James A.](../Page/James_Baker.md "wikilink"), Secretary (Foreword
    by) 2nd Expanded ed. 2003. (Archbishop of Galilee, born in Kafr
    Bir'im, the book covers his childhood growing up in the town. (The
    first six chapters of *Blood Brothers* can be downloaded [here (the
    Nov 08, 2005
    link)](http://web.archive.org/web/*/http://twelvedaystojerusalem.org/chacour/pdf/BloodBrothers.pdf).
  - Robin Clifton, "Popular Fear of Catholics during the English
    Revolution", Past and Present, 52 ( 1971), 23-55.
  - Cogliano; Francis D. *No King, No Popery: Anti-Catholicism in
    Revolutionary New England* Greenwood Press, 1995
  - David Brion Davis, "Some Themes of Counter-subversion: An Analysis
    of Anti-Masonic, Anti-Catholic and Anti-Mormon Literature",
    *Mississippi Valley Historical Review*, 47 (1960), 205-224.
  - Andrew M. Greeley, *An Ugly Little Secret: Anti-Catholicism in North
    America* 1977.
  - Henry, David. "Senator John F. Kennedy Encounters the Religious
    Question: I Am Not the Catholic Candidate for President."
    Contemporary American Public Discourse. Ed. H. R. Ryan. Prospect
    Heights, IL: Waveland Press, Inc., 1992. 177-193.
  - Higham; John. *Strangers in the Land: Patterns of American Nativism,
    1860-1925* 1955
  - Hinckley, Ted C. "American Anti-catholicism During the Mexican War"
    '' Pacific Historical Review'' 1962 31(2): 121-137. ISSN 0030-8684
  - Hostetler; Michael J. "Gov. Al Smith Confronts the Catholic
    Question: The Rhetorical Legacy of the 1928 Campaign" Communication
    Quarterly. Volume: 46. Issue: 1. 1998. Page Number: 12+.
  - Philip Jenkins, *The New Anti-Catholicism: The Last Acceptable
    Prejudice* (Oxford University Press, New ed. 2004). ISBN
    978-0-19-517604-9
  - Jensen, Richard. *The Winning of the Midwest: Social and Political
    Conflict, 1888-1896* (1971)
  - [Jensen, Richard. "'No Irish Need Apply': A Myth of Victimization,"
    *Journal of Social History* 36.2 (2002)
    405-429](http://tigger.uic.edu/~rjensen/no-irish.htm), with
    illustrations
  - Karl Keating, *Catholicism and Fundamentalism — The Attack on
    "Romanism" by "Bible Christians"* (Ignatius Press, 1988). ISBN
    978-0-89870-177-7
  - Kenny; Stephen. "Prejudice That Rarely Utters Its Name: A
    Historiographical and Historical Reflection upon North American
    Anti-Catholicism." *American Review of Canadian Studies.* Volume:
    32. Issue: 4. 2002. pp : 639+.
  - Khalidi, Walid. "*All that Remains: The Palestinian Villages
    Occupied and Depopulated by Israel in 1948.*" 1992. ISBN
    978-0-88728-224-9.
  - McGreevy, John T. "Thinking on One's Own: Catholicism in the
    American Intellectual Imagination, 1928-1960." *The Journal of
    American History*, 84 (1997): 97-131.
  - J.R. Miller, "Anti-Catholic Thought in Victorian Canada" in
    *Canadian Historical Review* 65, no.4. (December 1985), p. 474+
  - Moore; Edmund A. *A Catholic Runs for President* 1956.
  - Moore; Leonard J. *Citizen Klansmen: The Ku Klux Klan in Indiana,
    1921-1928* University of North Carolina Press, 1991
  - E. R. Norman, *Anti-Catholicism in Victorian England* (1968).
  - D. G. Paz, "Popular Anti-Catholicism in England, 1850-1851",
    *Albion* 11 (1979), 331-359.
  - Thiemann, Ronald F. *Religion in Public Life* Georgetown University
    Press, 1996.
  - Carol Z. Wiener, "The Beleaguered Isle. A Study of Elizabethan and
    Early Jacobean Anti-Catholicism", *Past and Present*, 51 (1971),
    27-62.
  - Wills, Garry. *Under God* 1990.
  - White, Theodore H. *The Making of the President 1960* 1961.
    </div>

## 参见

<div style="-moz-column-count:2; column-count:2;">

  - [反基督教](../Page/反基督教.md "wikilink")
  - [反教权主义](../Page/反教权主义.md "wikilink")

</div>

{{-}}

[反天主教](../Category/反天主教.md "wikilink")

1.  1701 Act of Settlement
2.
3.  **Scotland On Sunday**: Nov. 2006: "Football rivalry boosts
    religious orders"
4.  **The Times Newspaper May 2006**:''A new link has been made between
    the Old Kirk and Scotland's Catholics...Scotland the heartland of
    the UK's Catholics.
5.
6.  `Patrick``   ``R``   ``O'Malley``   ``(2006)``   `*`Catholicism,``
     ``sexual``   ``deviance,``   ``and``   ``Victorian``   ``Gothic``
     ``culture`*`.``   ``Cambridge``   ``University``   ``Press`
7.
8.
9.  `Franchot,``   ``Jenny``   ``(1994).``   ``"Two``   ``Escaped``
     ``Nuns:``   ``Rebecca``   ``Reed``   ``and``   ``Maria``
     ``Monk",``   ``Roads``   ``to``   ``Rome:``   ``The``
     ``Antebellum``   ``Protestant``   ``Encounter``   ``with``
     ``Catholicism.``   ``Berkeley,``   ``California``   ``(USA):``
     ``The``   ``University``   ``of``   ``California``   ``Press.``
     ``ISBN``   ``978-0-520-07818-5`
10.
11. 羅馬主义歷史”“第2版，1852年，页646-47约翰·道林
12. \[HTTP：//www.cermusa.francis埃杜/ CSD/ Pilot.htm
    不，你不会，波普先生！”：简史反天主教在美国\]中，在距离项目提供的圣弗朗西斯大学天主教研究由三个部分组成系列交付宗教研究系助理教授亚瑟Remillard。圣弗朗西斯大学CERMUSA网站，2007年5月
13. 赫伯格，威尔。 “宗教的世俗化的社会：美国的三大宗教的多元化若干问题”，第一卷的宗教研究的回顾。1962年，页37
14. [22783](http://poll.gallup.com/content/?ci=)
15. [1](http://www.galluppoll.com/content/default.aspx?ci=24385&pg=2)
16. \[HTTP：//www.sentinel .ORG/文章/2000-17/3870.html俄勒冈运行在复活节反天主教广告\]
17. [色彩鲜艳活泼，广告牌标志开始的新“教皇是反基督者”活动](http://www.sentinel.org/articles/2001-30/5478.html)
18.
19.
20.
21. [300故障奥康纳角色在艾滋病委员会](http://query.nytimes.com/gst/fullpage.html?sec=health&res=9B0DE2D6133EF934A15754C0A961948260)
22. [最后可接受的偏见](http://www.americamagazine.org/gettext.cfm?articleTypeID=1&textID=606&issueID=281)

23. [QuickTime影片](http://edition.cnn.com/WORLD/9512/virgin_bashing/kicking.mov884K)
24. [2](http://mediamatters.org/items/200412210001)
25. [3](http://www.twincities.com/mld/twincities/news/local/15916107.htm)
26.
27.
28.
29.