**基尔大学**，全名**克里斯蒂安-阿尔伯特基尔大学**（CAU），坐落于[波罗的海畔的](../Page/波罗的海.md "wikilink")[基尔市](../Page/基尔.md "wikilink")，是[德国](../Page/德国.md "wikilink")[石勒苏益格-荷尔斯泰因州唯一的一所综合大学](../Page/石勒苏益格-荷尔斯泰因.md "wikilink")。该校始建于公元1665年，历史悠久。目前在校学生约有20000人，教授600名左右。学校设有约140个专业方向。2001年新建的大学图书馆是目前整个德国最先进的图书馆之一，共有藏书440余万册。学校目前的重点专业方向有：[海洋生物学](../Page/海洋生物学.md "wikilink")，[国民经济学和](../Page/国民经济学.md "wikilink")[斯堪的纳维亚](../Page/斯堪的纳维亚.md "wikilink")[文化](../Page/文化.md "wikilink")。此外学校的[国际经济系和](../Page/国际经济.md "wikilink")[自然灾害研究所亦享有国际声誉](../Page/自然灾害.md "wikilink")。

## 外部連結

  - [University of Kiel Website](http://www.uni-kiel.de/)
  - [University of Kiel International
    Affairs](http://www.uni-kiel.de/internationales/index-e.shtml)
  - [Students' Association at Kiel
    University](https://web.archive.org/web/20170612024454/https://www.asta.uni-kiel.de/)

[K](../Category/基爾大學.md "wikilink")
[Category:1660年代創建的教育機構](../Category/1660年代創建的教育機構.md "wikilink")