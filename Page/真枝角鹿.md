**真枝角鹿**（*Eucladoceros*），又名**真枝角獸**，是已[滅絕的](../Page/滅絕.md "wikilink")[鹿](../Page/鹿.md "wikilink")，其[化石在](../Page/化石.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")、[中東及](../Page/中東.md "wikilink")[中亞都有發現](../Page/中亞.md "wikilink")。

最早的真枝角鹿生存於[上新世早期的](../Page/上新世.md "wikilink")[中國](../Page/中國.md "wikilink")。到了[更新世早期](../Page/更新世.md "wikilink")，真枝角鹿在歐洲及中國最為豐富。歐洲形態的真枝角鹿分類很混淆，只有達12種簡單定義的[物種](../Page/物種.md "wikilink")。大部份真枝角鹿物種都有[異名](../Page/異名.md "wikilink")，當中只有2或3種已確認，包括：

  - [雙叉真枝角鹿](../Page/雙叉真枝角鹿.md "wikilink")：來自[英格蘭](../Page/英格蘭.md "wikilink")、[義大利及](../Page/義大利.md "wikilink")[俄羅斯南部的](../Page/俄羅斯.md "wikilink")[亞速海](../Page/亞速海.md "wikilink")；
  - *E.
    ctenoides*：從[希臘](../Page/希臘.md "wikilink")、義大利、[法國](../Page/法國.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[荷蘭及英格蘭發現](../Page/荷蘭.md "wikilink")；及
  - *E. teguliensis*：來自法國、荷蘭及英格蘭。一些學者將牠看為*E.
    ctenoides*的[亞種](../Page/亞種.md "wikilink")，因為從一些標本看到*E.
    ctenoides*及*E. teguliensis*的過渡特徵。

真枝角鹿的鹿角像梳子。雙叉真枝角鹿是最進化的物種，其鹿角有叉狀分枝。真枝角鹿是最先有大型複雜鹿角的鹿，但其頭顱形狀及[牙齒形態卻像](../Page/牙齒.md "wikilink")[水鹿般原始](../Page/水鹿.md "wikilink")。在[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[印度有發現一些不完整的真枝角鹿遺骸](../Page/印度.md "wikilink")。牠們是大型的動物，長2.5米及肩高1.8米。只有[大角鹿屬及現今的](../Page/大角鹿屬.md "wikilink")[駝鹿比牠大](../Page/駝鹿.md "wikilink")。真枝角鹿有一組壯觀的[鹿角](../Page/鹿角.md "wikilink")，分成12個角端，闊達1.7米。牠們在冰期出現前滅絕。

## 物種

  - [布氏真枝角鹿](../Page/布氏真枝角鹿.md "wikilink")（*E.
    boulei*）：屬於[上新世末期至](../Page/上新世.md "wikilink")[更新世早期](../Page/更新世.md "wikilink")，在[中國](../Page/中國.md "wikilink")[泥河灣盆地發現](../Page/泥河灣盆地.md "wikilink")。
  - "E. proboulei"：屬於上新世早期，在中國發現。
  - *E. ctenoides*：以往稱為*E.
    teguliensis*，屬於更新世早期，在[義大利](../Page/義大利.md "wikilink")[托斯卡納發現](../Page/托斯卡納.md "wikilink")。
  - *E.
    dichotomus*：屬於更新世早期，在中國的泥河灣盆地發現，有可能不是真枝角鹿的[物種](../Page/物種.md "wikilink")。
  - [雙叉真枝角鹿](../Page/雙叉真枝角鹿.md "wikilink")（*E.
    dicranios*）：屬於更新世早期，在義大利托斯卡納發現，是最早發現的真枝角鹿，亦是屬下的[模式種](../Page/模式種.md "wikilink")。
  - *E. senezensis*：一些學者認為牠是*E.
    ctenoides*的[亞種](../Page/亞種.md "wikilink")。
  - *E. teguliensis*：參*E. ctenoides*。
  - [四角真枝角鹿](../Page/四角真枝角鹿.md "wikilink")（*E.
    tetraceros*）：屬於更新世早期，可能是*E.
    ctenoides*的[異名](../Page/異名.md "wikilink")。

## 外部連結

  - [更新世動物介紹（英文）](https://web.archive.org/web/20080403094313/http://id-archserve.ucsb.edu/Anth3/Courseware/Pleistocene/6_Bestiary.html#Eucladoceros)

[Category:真枝角鹿屬](../Category/真枝角鹿屬.md "wikilink")
[Category:古動物](../Category/古動物.md "wikilink")