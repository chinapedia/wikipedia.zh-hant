**盧多爾夫人**（[学名](../Page/学名.md "wikilink")：**），又名**魯道夫人**，是[人科下的](../Page/人科.md "wikilink")[化石](../Page/化石.md "wikilink")[物種](../Page/物種.md "wikilink")，是於1972年在[肯雅](../Page/肯雅.md "wikilink")[庫比福勒發現的](../Page/庫比福勒.md "wikilink")。其標本為一個[頭顱骨](../Page/頭顱骨.md "wikilink")（編號KNM
ER 1470），估計是屬於190萬年前的。

盧多爾夫人化石原先被認為是[能人](../Page/能人.md "wikilink")，但卻錯誤估年老了300萬年。由於這個錯誤，這個頭顱骨比任何已知的[南方古猿類](../Page/南方古猿類.md "wikilink")（能人祖先）更為古老。

盧多爾夫人的頭顱骨與能人的比較有很明顯的分別，故被編為自己一種，是與能人同時期的。但是卻不清楚盧多爾夫人或能人是否後來[人屬物種的祖先](../Page/人屬.md "wikilink")。

盧多爾夫人被分類在人屬中有著很多爭論。雖然沒有任何顱後遺骸被發現，但一般相信盧多爾夫人像能人般缺乏後期人科的獨有特徵，如修長的[臀部](../Page/臀部.md "wikilink")、複雜的排汗系統、狹窄的生殖道、腳長於手臂、[眼睛上的眼白](../Page/眼睛.md "wikilink")、幼小的毛髮露出[皮膚等](../Page/皮膚.md "wikilink")。很多[科學家相信盧多爾夫人較為像](../Page/科學家.md "wikilink")[猿](../Page/猿.md "wikilink")，但[腦部較大及是雙足行走的](../Page/腦部.md "wikilink")。

就盧多爾夫人頭顱骨的重組發現他非常像猿，而顱腔卻細小了很多，只有約526立方厘米。這個研究加入了發現頭顱骨時未知的[生物學原理](../Page/生物學.md "wikilink")，如眼睛、[耳朵及口的確實位置是互相影響的原理](../Page/耳朵.md "wikilink")。\[1\]

2012年，在在肯尼亚北部同一地区又发现了两块年代在195万年前至178万年前的下颌化石和一块脸骨化石，其特征与头骨化石非常相似\[2\]。从而确认了鲁道夫人确实存在，也显示出古人类的进化比原有认识更加复杂\[3\]。

## 參考

## 外部連結

  - [Archaeology
    Info](http://www.archaeologyinfo.com/homorudolfensis.htm)
  - [Science Daily
    article](http://www.sciencedaily.com/releases/2001/03/010322232234.htm)
  - [Talk Origins - Skull
    KNM-ER 1470](http://www.talkorigins.org/faqs/homs/1470.html)
  - [Smithsonian](http://www.mnh.si.edu/anthro/humanorigins/ha/rud.html)

[Category:早期人類](../Category/早期人類.md "wikilink")
[Category:人屬](../Category/人屬.md "wikilink")

1.
2.
3.