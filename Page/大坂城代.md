**大坂城代**是江戶幕府的職稱，大坂城代是幕府在[大坂城的代表](../Page/大坂城.md "wikilink")，負責大坂城的城防，並統轄在大坂值班的幕府役人。除此之外，大坂城代的職能也遠及整個關西地區，如對關西大名的監視，手握關西緊急時的軍事指揮權及訴訟裁斷權。江戸時代中期以來，大坂城代和[京都所司代多由老中就任](../Page/京都所司代.md "wikilink")。編制上大坂城代定員一人，役高有一萬石，由五至十萬石領地的[譜代大名中選任](../Page/譜代大名.md "wikilink")。

## 歷代大坂城代

1.  [内藤信正](../Page/内藤信正.md "wikilink")(1619年－1626年)
2.  [阿部正次](../Page/阿部正次.md "wikilink")(1626年－1647年)
3.  [永井直清](../Page/永井直清.md "wikilink")(1648年)
4.  [稲垣重綱](../Page/稲垣重綱.md "wikilink")(1648年－1649年)
5.  [内藤信照](../Page/内藤信照.md "wikilink")(1649年－1652年)
6.  [水野忠職](../Page/水野忠職.md "wikilink")(1652年－1654年)
7.  [内藤忠興](../Page/内藤忠興.md "wikilink")(1654年－1656年)
8.  [松平光重](../Page/松平光重.md "wikilink")(1656年－1658年)
9.  [水野忠職](../Page/水野忠職.md "wikilink")(1658年－1659年)
10. [内藤忠興](../Page/内藤忠興.md "wikilink")(1659年－1660年)
11. [松平光重](../Page/松平光重.md "wikilink")(1660年－1661年)
12. [水野忠職](../Page/水野忠職.md "wikilink")(1661年－1662年)
13. [青山宗俊](../Page/青山宗俊.md "wikilink")(1662年－1673年)
14. [太田資次](../Page/太田資次.md "wikilink")(1673年－1684年)
15. [水野忠春](../Page/水野忠春.md "wikilink")(1684年)
16. [土屋政直](../Page/土屋政直.md "wikilink")(1684年－1685年)
17. [内藤重頼](../Page/内藤重頼.md "wikilink")(1685年－1687年)
18. [松平信興](../Page/松平信興.md "wikilink")(1687年－1690年)
19. [土岐頼殷](../Page/土岐頼殷.md "wikilink")(1691年－1712年)
20. [内藤弌信](../Page/内藤弌信.md "wikilink")(1712年－1718年)
21. [安藤信友](../Page/安藤信友.md "wikilink")(1718年－1722年)
22. [松平乗邑](../Page/松平乗邑.md "wikilink")(1722年－1723年)
23. [酒井忠音](../Page/酒井忠音.md "wikilink")(1723年－1728年)
24. [堀田正虎](../Page/堀田正虎.md "wikilink")(1728年－1729年)
25. [松平信祝](../Page/松平信祝.md "wikilink")(1729年－1730年)
26. [土岐賴稔](../Page/土岐賴稔.md "wikilink")(1730年－1734年)
27. [稲葉正親](../Page/稲葉正親.md "wikilink")(1734年)
28. [太田資晴](../Page/太田資晴.md "wikilink")(1734年－1740年)
29. [酒井忠恭](../Page/酒井忠恭.md "wikilink")(1740年－1744年)
30. [堀田正亮](../Page/堀田正亮.md "wikilink")(1744年－1745年)
31. [阿部正福](../Page/阿部正福.md "wikilink")(1745年－1747年)
32. [酒井忠用](../Page/酒井忠用.md "wikilink")(1747年－1752年)
33. [松平輝高](../Page/松平輝高.md "wikilink")(1752年－1756年)
34. [井上正經](../Page/井上正經.md "wikilink")(1756年－1758年)
35. [青山忠朝](../Page/青山忠朝.md "wikilink")(1758年－1760年)
36. [松平康福](../Page/松平康福.md "wikilink")(1760年－1762年)
37. [阿部正允](../Page/阿部正允.md "wikilink")(1762年－1764年)
38. [松平乗祐](../Page/松平乗祐.md "wikilink")(1764年－1769年)
39. [久世廣明](../Page/久世廣明.md "wikilink")(1769年－1777年)
40. [牧野貞長](../Page/牧野貞長.md "wikilink")(1777年－1781年)
41. [土岐定経](../Page/土岐定経.md "wikilink")(1781年－1782年）
42. [戸田忠寬](../Page/戸田忠寬.md "wikilink")(1782年－1784年)
43. [阿部正敏](../Page/阿部正敏.md "wikilink")(1784年－1787年)
44. [堀田正順](../Page/堀田正順.md "wikilink")(1787年－1792年)
45. [牧野忠精](../Page/牧野忠精.md "wikilink")(1792年－1798年)
46. [松平輝和](../Page/松平輝和.md "wikilink")(1798年－1800年)
47. [青山忠裕](../Page/青山忠裕.md "wikilink")(1800年－1802年)
48. [稲葉正謁](../Page/稲葉正謁.md "wikilink")(1802年－1804年)
49. [阿部正由](../Page/阿部正由.md "wikilink")(1804年－1806年)
50. [松平乗保](../Page/松平乗保.md "wikilink")(1806年－1810年)
51. [大久保忠真](../Page/大久保忠真.md "wikilink")(1810年－1815年)
52. [松平輝延](../Page/松平輝延.md "wikilink")(1815年－1822年)
53. [松平康任](../Page/松平康任.md "wikilink")(1822年－1825年)
54. [水野忠邦](../Page/水野忠邦.md "wikilink")(1825年－1826年)
55. [松平宗発](../Page/松平宗発.md "wikilink")(1826年－1828年)
56. [太田資始](../Page/太田資始.md "wikilink")(1828年－1831年)
57. [松平信順](../Page/松平信順.md "wikilink")(1831年－1834年)
58. [土井利位](../Page/土井利位.md "wikilink")(1834年－1837年)
59. [堀田正篤](../Page/堀田正篤.md "wikilink")(1837年)
60. [間部詮勝](../Page/間部詮勝.md "wikilink")(1837年－1838年)
61. [井上正春](../Page/井上正春.md "wikilink")(1838年－1840年)
62. [青山忠良](../Page/青山忠良.md "wikilink")(1840年－1844年)
63. [松平乗全](../Page/松平乗全.md "wikilink")(1844年－1845年)
64. [松平忠優](../Page/松平忠優.md "wikilink")(1845年－1848年)
65. [内藤信親](../Page/内藤信親.md "wikilink")(1848年－1850年)
66. [土屋寅直](../Page/土屋寅直.md "wikilink")(1850年－1858年)
67. [松平信篤](../Page/松平信篤.md "wikilink")(1858年－1860年)
68. [松平宗秀](../Page/松平宗秀.md "wikilink")(1860年－1862年)
69. [松平信古](../Page/松平信古.md "wikilink")(1862年－1865年)
70. [牧野貞明](../Page/牧野貞明.md "wikilink")(1864年－1868年)

[Category:江戶幕府官制](../Category/江戶幕府官制.md "wikilink")
[Category:攝津國](../Category/攝津國.md "wikilink")
[Category:大阪市歷史](../Category/大阪市歷史.md "wikilink")