**八神健**，日本[漫畫家](../Page/漫畫家.md "wikilink")。[廣島縣](../Page/廣島縣.md "wikilink")[廣島市出身](../Page/廣島市.md "wikilink")。代表作是《[生死戀](../Page/生死戀_\(漫畫\).md "wikilink")》。

## 作品

  - [生死戀](../Page/生死戀_\(漫畫\).md "wikilink")、全7冊、原名《》、1995年 -
    1996年、[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")、[集英社](../Page/集英社.md "wikilink")
  - [獨角獸少年](../Page/獨角獸少年.md "wikilink")、全3冊、原名《》、1997年 -
    1998年、[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")
  - [6/17秀逗美眉](../Page/6/17秀逗美眉.md "wikilink")、全12冊、原名《》、2000年 -
    2003年、[週刊少年Champion](../Page/週刊少年Champion.md "wikilink")、[秋田書店](../Page/秋田書店.md "wikilink")
      - 6/17秀逗美眉傑作集、全1冊、原名《》
  - 原名《》、全1冊、2004年、[月刊少年エース](../Page/月刊少年エース.md "wikilink")、[角川書店](../Page/角川書店.md "wikilink")）
  - 原名《》、全2冊、2006年、ヤングアニマル、[白泉社](../Page/白泉社.md "wikilink")）
  - [心跳魔女神判！](../Page/心跳魔女神判！.md "wikilink")、全2冊、原名《》 2007年 - 2008年
    [秋田書店](../Page/秋田書店.md "wikilink")
  - [心跳魔女神判2](../Page/心跳魔女神判2.md "wikilink")、全2冊、 2008年 - 2009年
  - 原名《》、2011年 - [竹書房](../Page/竹書房.md "wikilink")(連載中)
  - 原名《-八神健傑作集1》、全1冊、[集英社](../Page/集英社.md "wikilink")
  - 原名《》、全1冊、[角川書店](../Page/角川書店.md "wikilink")

## 外部連結

  - [公式サイト](https://web.archive.org/web/20140717222455/http://homepage3.nifty.com/8th-gods/)

  -
  - [Ｊコミ無料配信中作品](http://www.j-comi.jp/title/index/query:八神健) -
    [Jコミ](../Page/Jコミ.md "wikilink")

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")