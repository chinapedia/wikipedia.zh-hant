**紙包蛋糕**（）是源於[香港的](../Page/香港.md "wikilink")[蛋糕](../Page/蛋糕.md "wikilink")，屬於一種[海綿蛋糕](../Page/海綿蛋糕.md "wikilink")，以烘焙紙包裹著，其大小只有兩、三口份量\[1\]。

## 起源

紙包蛋糕約於[1950年代在香港出現](../Page/香港1950年代.md "wikilink")，當時香港居民的收入普遍不多，但又希望能夠品嚐西式甜點，可是當時製作蛋糕所需的[麵粉及](../Page/麵粉.md "wikilink")[牛油都很昂貴](../Page/牛油.md "wikilink")，所以[西餅的售價並非一般居民所能負擔](../Page/西餅.md "wikilink")，於是便有售賣西式食品的[冰室利用較便宜的本地食材製作分量細小的海綿蛋糕出售](../Page/冰室.md "wikilink")。紙包蛋糕在[1960年代初期仍是較為昂貴的零食](../Page/香港1960年代.md "wikilink")，當時買一條[油炸鬼只要一個](../Page/油炸鬼.md "wikilink")[五仙港幣（斗零）](../Page/香港五仙硬幣.md "wikilink")，買紙包蛋糕卻要[一毫港幣](../Page/香港一毫硬幣.md "wikilink")\[2\]，並非能夠輕易負擔的零食，及後香港經濟快速發展，大部分居民都較過往富裕後，吃紙包蛋糕對部分香港居民而言成為一種童年回憶\[3\]。

## 製作

當[麵粉與](../Page/麵粉.md "wikilink")[雞蛋打成麵糊後](../Page/雞蛋.md "wikilink")，再倒入捲成杯狀的烘焙紙內[烘焙](../Page/烘焙.md "wikilink")。在香港，[烘焙紙又常稱為](../Page/烘焙紙.md "wikilink")「[牛油紙](../Page/牛油紙.md "wikilink")」。紙包蛋糕在香港的[麵包和](../Page/麵包.md "wikilink")[西餅店都有供應](../Page/西餅店.md "wikilink")，也有部份[茶餐廳發售](../Page/茶餐廳.md "wikilink")\[4\]。此外，在歐美和澳洲的[唐人街也可找到這種蛋糕](../Page/唐人街.md "wikilink")\[5\]。

## 參見

  - [梳乎厘](../Page/梳乎厘.md "wikilink")
  - [海綿蛋糕](../Page/海綿蛋糕.md "wikilink")
  - [雪芳蛋糕](../Page/雪芳蛋糕.md "wikilink")
  - [蛋糕列表](../Page/蛋糕列表.md "wikilink")

## 參考資料

[Category:蛋糕](../Category/蛋糕.md "wikilink")
[Category:香港麵包西餅](../Category/香港麵包西餅.md "wikilink")
[Category:香港小吃](../Category/香港小吃.md "wikilink")

1.
2.
3.
4.  [紙包蛋糕 -
    情濃香軟的甜品](http://www.christinesrecipes.com/2008/07/cake-wrapped-in-paper.html)，簡易食譜，2008年7月2日
5.  [港式纸包蛋糕](http://www.xiachufang.com/recipe/267510/)，下厨房，2013-4-25