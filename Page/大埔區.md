**大埔區**（）是[香港十八區的其中一區](../Page/香港行政區劃.md "wikilink")，位於[新界東部](../Page/新界.md "wikilink")，範圍包括[大埔](../Page/大埔_\(香港\).md "wikilink")、[東平洲](../Page/東平洲.md "wikilink")、[大埔滘](../Page/大埔滘.md "wikilink")、[汀角](../Page/汀角.md "wikilink")、[船灣](../Page/船灣.md "wikilink")、[林村谷](../Page/林村谷.md "wikilink")，[白石角及它的](../Page/白石角.md "wikilink")[外飛地為](../Page/外飛地.md "wikilink")[赤門海峽兩岸的](../Page/赤門海峽.md "wikilink")[西貢半島北部](../Page/西貢半島.md "wikilink")（[西貢北](../Page/西貢北.md "wikilink")）。它的總面積約為148[平方公里](../Page/平方公里.md "wikilink")，是[香港第二大行政區域](../Page/香港.md "wikilink")。根據2016年中統計，大埔區人口為303,926人\[1\]。

大埔區人口主要聚居於[大埔新市鎮內七大出租](../Page/大埔新市鎮.md "wikilink")[公共屋邨和](../Page/香港公共房屋.md "wikilink")[租者置其屋計劃屋邨](../Page/租者置其屋.md "wikilink")，以及[太和站附近的](../Page/太和站.md "wikilink")[大埔舊墟和](../Page/大埔舊墟.md "wikilink")[大埔墟站附近的大埔新墟的私人屋苑](../Page/大埔墟站.md "wikilink")。其餘不少居民散居於大埔區內133條大小[村落](../Page/村落.md "wikilink")。

2005年，[世界衛生組織確認大埔為](../Page/世界衛生組織.md "wikilink")[安全社區](../Page/安全社區.md "wikilink")，是全球第90個安全社區\[2\]，也是香港第3個安全社區（第1是[屯門區](../Page/屯門區.md "wikilink")，第2是[葵青區](../Page/葵青區.md "wikilink")）。

## 氣侯

<div style="width:80%;">

</div>

## 地理

[HK_PatSinLeng.JPG](https://zh.wikipedia.org/wiki/File:HK_PatSinLeng.JPG "fig:HK_PatSinLeng.JPG")
大埔區位處新界東部，南起[大埔滘](../Page/大埔滘.md "wikilink")[大埔尾坑及](../Page/大埔尾坑.md "wikilink")[中文大學地界線](../Page/中文大學.md "wikilink")，北至[八仙嶺](../Page/八仙嶺.md "wikilink")，西瀕[大刀屻](../Page/大刀屻.md "wikilink")，東跨[吐露港](../Page/吐露港.md "wikilink")，包括對岸[西貢北的](../Page/西貢北.md "wikilink")[十四鄉](../Page/十四鄉.md "wikilink")、[黃竹塱及離島](../Page/黃竹塱.md "wikilink")[塔門](../Page/塔門.md "wikilink")、[平洲](../Page/東平洲.md "wikilink")、[赤洲等](../Page/赤洲.md "wikilink")。

大埔區內有香港的著名內港——[吐露港](../Page/吐露港.md "wikilink")，該港在大埔新市鎮的東面、[沙田的東北面](../Page/沙田.md "wikilink")，海港呈西南-東北走向，其出口為[大赤門](../Page/大赤門.md "wikilink")，之後海水便流進[大鵬灣](../Page/大鵬灣_\(南海\).md "wikilink")。

位於大尾篤及[沙頭角半島的](../Page/沙頭角半島.md "wikilink")[船灣淡水湖](../Page/船灣淡水湖.md "wikilink")，是香港第一個劃自海域的大型儲水水塘，也是大眾騎單車的好去處。淡水湖原本是一個大海灣——船灣，灣內沿岸曾經有多條鄉村，不過在1960年代建淡水湖時遭拆遷，部份鄉村遺址已經埋在淡水湖水底。

大埔區較著名的高山有北邊的[八仙嶺](../Page/八仙嶺.md "wikilink")，這個山嶺亦是大埔區和北區的天然分界，而附近[九龍坑山山頂則設有數個發射站](../Page/九龍坑山.md "wikilink")。

大埔區天氣非常潮濕，在春夏期間濕度經常都高達100%，能見度最低時可以只有數米距離。大埔區的中心是[大埔新市鎮](../Page/大埔新市鎮.md "wikilink")，包括舊區[大埔墟](../Page/大埔墟.md "wikilink")，以及新區[大埔中心一帶](../Page/大埔中心.md "wikilink")。

### 地質

香港最古老的[岩石是名為](../Page/岩石.md "wikilink")「黃竹角咀組」（Bluff Head
Formation）的沉積地層，是[泥盆紀的](../Page/泥盆紀.md "wikilink")[沉積岩](../Page/沉積岩.md "wikilink")，主要在[赤門兩岸出露](../Page/赤門.md "wikilink")，在北岸從白沙頭洲（Pak
Sha Tau Chau）到黃竹角咀（Wong Chuk Kok Tsui）一帶清晰易見，受到地質構造的摺皺及錯位運動，露出的岩層幾近直立。

## 歷史

[TaiPo_New_Town.jpg](https://zh.wikipedia.org/wiki/File:TaiPo_New_Town.jpg "fig:TaiPo_New_Town.jpg")
[Tai_Po_New_Town_overview_2017.jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_New_Town_overview_2017.jpg "fig:Tai_Po_New_Town_overview_2017.jpg")
大埔古稱「大步」，直至[清朝](../Page/清朝.md "wikilink")[光緒年間才稱為](../Page/光緒.md "wikilink")「大埔」。大埔在中國史籍中的記載可以上溯至[五代的](../Page/五代.md "wikilink")[南漢](../Page/南漢.md "wikilink")，大寶六年（963年），後主[劉鋹招募](../Page/劉鋹.md "wikilink")
3,000
人於「大步海」（即吐露港）採集[珍珠](../Page/珍珠.md "wikilink")，並設置「媚川都」。直至[康熙三十五年](../Page/康熙.md "wikilink")（1695年）因長期採集網羅淨盡，才明詔罷採，大埔的採珠業自此式微。

[新界五大氏族的](../Page/新界五大氏族.md "wikilink")[鄧族](../Page/香港新界鄧氏.md "wikilink")、[文族等自](../Page/香港新界文氏.md "wikilink")[宋](../Page/宋朝.md "wikilink")[明期間分別遷到大埔的](../Page/明朝.md "wikilink")[大埔頭及](../Page/大埔頭.md "wikilink")[泰亨定居](../Page/泰亨.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[康熙元年](../Page/康熙.md "wikilink")（1662年）實行遷界，使大埔區頓成荒域。康熙八年（1669年）批准復界，為恢復經濟，朝廷鼓勵外地人士入遷墾荒，其中以由[江西](../Page/江西.md "wikilink")、[福建](../Page/福建.md "wikilink")、[惠州](../Page/惠州.md "wikilink")、[潮州](../Page/潮州.md "wikilink")、[嘉應等地遷入的](../Page/嘉應.md "wikilink")[客家人最多](../Page/客家人.md "wikilink")，建立圍屋聚族而居。

[明朝](../Page/明朝.md "wikilink")[萬曆年間](../Page/萬曆.md "wikilink")，大埔頭鄧氏在今日天后廟後的汀角路建立「孝子鄧師孟祠」紀念孝子鄧師孟，於[康熙十一年](../Page/康熙.md "wikilink")
（1672年）向[新安知縣申請建墟](../Page/新安.md "wikilink")，在已圮毀的「孝子鄧師孟祠」原址建立「大步墟」。泰亨文氏於1892年乃聯合[粉嶺](../Page/粉嶺.md "wikilink")[彭氏及大埔附近五個非鄧氏的村落](../Page/彭姓.md "wikilink")，組成「[大埔七約](../Page/大埔七約.md "wikilink")」（泰亨約、林村約、翕和約、集和約、樟樹灘約、汀角約、粉嶺約），於大步墟隔河另設「太和市」（即現富善街一帶），在[光緒二十二年](../Page/光緒.md "wikilink")（1896年）修建廣福橋聯結太和市和林村河北岸的大步墟。[英國租借新界以後](../Page/英國.md "wikilink")，在1913年於太和市興建[大埔墟火車站](../Page/香港鐵路博物館.md "wikilink")，太和市逐漸取代大埔墟的地位，所以太和市後來被稱為大埔墟，而原來的大埔墟（大步墟）則改稱為「大埔舊墟」。

光緒二十四年（1898年），清廷與英國簽訂《[展拓香港界址專條](../Page/展拓香港界址專條.md "wikilink")》，翌年4月，英國軍艦駛入[大埔海登陸](../Page/大埔海.md "wikilink")，舉行接管新界的升旗儀式，由於大埔為新界的中心地區，英國人以此作為新界的行政中心，設立[理民府](../Page/舊北區理民府.md "wikilink")、警察總部及鄉議局。位於大埔區的[北約理民府管理的地域包括了今日的大埔區](../Page/北約\(香港\).md "wikilink")、[北區](../Page/北區_\(香港\).md "wikilink")、[元朗區](../Page/元朗區.md "wikilink")、[屯門區及](../Page/屯門區.md "wikilink")[沙田區](../Page/沙田區.md "wikilink")，但不包括[九龍水塘以西的地域及](../Page/九龍水塘.md "wikilink")[西貢北](../Page/西貢北.md "wikilink")（當時屬[南約](../Page/南約.md "wikilink")）。1948年，元朗區從[北約分裂出來](../Page/北約\(香港\).md "wikilink")；到後來1970年代，沙田區在政府發展沙田新市鎮之時自立成為單獨一區；最後是北區於1980年獨立成區。

根據[香港行政區劃](../Page/香港行政區劃.md "wikilink")，大埔區也包括西貢北，以及靠近[深圳](../Page/深圳.md "wikilink")[大鵬半島的](../Page/大鵬半島.md "wikilink")[東平洲](../Page/東平洲.md "wikilink")。

|      |                                                                                                                                                                                                                                                                 |
| ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 大埔：  | [大埔滘](../Page/大埔滘.md "wikilink")、[大埔墟](../Page/大埔墟.md "wikilink")、[大埔市](../Page/大埔市.md "wikilink")、[太和](../Page/太和.md "wikilink")、[林村](../Page/林村.md "wikilink")、[汀角](../Page/汀角.md "wikilink")、[大尾篤](../Page/大尾篤.md "wikilink")、[船灣](../Page/船灣.md "wikilink") |
| 西貢北： | [北潭坳](../Page/北潭坳.md "wikilink")、[黃石碼頭](../Page/黃石碼頭.md "wikilink")、[海下灣](../Page/海下灣.md "wikilink")、[十四鄉](../Page/十四鄉.md "wikilink")、[泥涌](../Page/泥涌.md "wikilink")、[塔門](../Page/塔門.md "wikilink")                                                             |

## 行政區劃

[HK_SaiKungNorth_Sign.JPG](https://zh.wikipedia.org/wiki/File:HK_SaiKungNorth_Sign.JPG "fig:HK_SaiKungNorth_Sign.JPG")

### 民政事務專員

現任大埔區[民政事務專員為](../Page/民政事務專員.md "wikilink")[陳巧敏女士](../Page/陳巧敏.md "wikilink")，於2018年10月25日接替[呂少珠出任該區專員](../Page/呂少珠.md "wikilink")\[3\]。

  - 歷任大埔民政事務專員

<!-- end list -->

  - 呂建勳 先生(任期：1997年-2003年12月7日)
  - 陳貴春 先生(任期：2003年12月8日-2006年2月28日)
  - 潘太平 先生(任期：2006年3月1日-2009年10月11日)
  - 李國彬 先生(任期：2009年10月12日-2012年4月25日)
  - 鄭青雲 先生(任期：2012年4月26日-2013年9月8日)
  - 蘇植良 先生(任期：2013年9月9日-2016年10月10日)
  - 呂少珠 女士(任期：2016年10月11日-2018年9月26日)
  - 陳巧敏 女士(任期：2018年10月25日至今)

### 區議會

大埔區議會是香港的[區議會之一](../Page/香港區議會.md "wikilink")，共有21個議席，包括19個民選議席及2個當然議席（[大埔及](../Page/大埔_\(香港\).md "wikilink")[西貢北](../Page/西貢北.md "wikilink")[鄉事委員會主席](../Page/鄉事委員會.md "wikilink")）。

### 分區委員會

大埔區議會屬下設有5個委員會，負責區內的工商業、環境工程、文娛康體、社會服務及交通運輸等各方面的事務。

### 統計資料

根據2011年的[人口普查資料](../Page/人口普查.md "wikilink")\[4\]，大埔區的人口資料如下：

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>人口特徵</dt>

</dl>
<ul>
<li>人口： 296,853人
<ul>
<li>15歲以下： 10.2%</li>
<li>15至64歲： 79.0%</li>
<li>65歲及以上： 10.7%</li>
</ul></li>
<li>年齡中位數： 41.4歲</li>
<li>15歲及以上從未結婚人口比例
<ul>
<li>男性： 34.8%</li>
<li>女性： 31.2%</li>
</ul></li>
</ul>
<dl>
<dt>教育</dt>

</dl>
<ul>
<li>6至18歲人口就學比率： 不詳</li>
<li>20歲及以上非就學人口具專上教育程度比例：24.1%</li>
</ul>
<dl>
<dt>勞動人口特徵</dt>

</dl>
<ul>
<li>勞動人口： 163,905人</li>
<li>勞動人口參與率
<ul>
<li>男性： 69.5%</li>
<li>女性： 54.5%</li>
<li>合計： 61.5%</li>
</ul></li>
<li>工作人口每月主業收入中位數：11,000港元</li>
</ul></td>
<td><dl>
<dt>住戶特徵</dt>

</dl>
<ul>
<li>家庭住戶數目：94,481戶</li>
<li>家庭住戶平均人數： 3.1人</li>
<li>家庭住戶每月收入中位數：22,340港元</li>
</ul>
<dl>
<dt>房屋特徵</dt>

</dl>
<ul>
<li>有人居住的屋宇單位數目：94,245個</li>
<li>每個屋宇單位的平均家庭住戶數目：1.003戶</li>
<li>自置居所住戶在家庭住戶總數目中所佔的比例：63.7%</li>
<li>家庭住戶每月按揭供款及借貸還款中位數：6,340港元</li>
<li>按揭供款及借貸還款與收入比率中位數：18.1%</li>
<li>家庭住戶每月租金中位數：1,300港元</li>
<li>租金與收入比率中位數：13.4%</li>
</ul></td>
</tr>
</tbody>
</table>

## 社區環境

### 公共屋邨

  - [大元邨](../Page/大元邨.md "wikilink")：大埔區首個公共屋邨
  - [廣福邨](../Page/廣福邨.md "wikilink")
  - [寶鄉邨](../Page/寶鄉邨.md "wikilink")

<!-- end list -->

  - [富善邨](../Page/富善邨.md "wikilink")
  - [太和邨](../Page/太和邨.md "wikilink")

<!-- end list -->

  - [富亨邨](../Page/富亨邨.md "wikilink")
  - [運頭塘邨](../Page/運頭塘邨.md "wikilink")

### [居屋屋苑](../Page/居者有其屋.md "wikilink")

  - [汀雅苑](../Page/汀雅苑.md "wikilink")
  - [宏福苑](../Page/宏福苑.md "wikilink")
  - [大埔廣場](../Page/大埔廣場.md "wikilink")
  - [明雅苑](../Page/明雅苑.md "wikilink")

<!-- end list -->

  - [新興花園](../Page/新興花園.md "wikilink")
  - [寶雅苑](../Page/寶雅苑.md "wikilink")
  - [富雅花園](../Page/富雅花園.md "wikilink")
  - [頌雅苑](../Page/頌雅苑.md "wikilink")

<!-- end list -->

  - [逸雅苑](../Page/逸雅苑.md "wikilink")
  - [景雅苑](../Page/景雅苑.md "wikilink")
  - [德雅苑](../Page/德雅苑.md "wikilink")
  - [怡雅苑](../Page/怡雅苑.md "wikilink")

### 主要私人屋苑

  - [大埔中心](../Page/大埔中心.md "wikilink")
  - [翠林閣](../Page/翠林閣.md "wikilink")
  - [昌運中心](../Page/昌運中心.md "wikilink")
  - [八號花園](../Page/八號花園.md "wikilink")
  - [海寶花園](../Page/海寶花園.md "wikilink")
  - [寶湖花園](../Page/寶湖花園.md "wikilink")
  - [翠屏花園](../Page/翠屏花園.md "wikilink")
  - [新達廣場](../Page/新達廣場.md "wikilink")
  - [富·盈門](../Page/富·盈門.md "wikilink")
  - [菁泉雅居](../Page/菁泉雅居.md "wikilink")
  - [樂賢居](../Page/樂賢居.md "wikilink")

<!-- end list -->

  - [太湖花園](../Page/太湖花園.md "wikilink")
  - [南山花園](../Page/南山花園.md "wikilink")
  - [太湖山莊](../Page/太湖山莊.md "wikilink")
  - [太和中心](../Page/太和中心.md "wikilink")
  - [大埔花園](../Page/大埔花園.md "wikilink")
  - [帝欣苑](../Page/帝欣苑.md "wikilink")
  - [嵐山](../Page/嵐山_\(香港\).md "wikilink")
  - [倚龍山莊](../Page/倚龍山莊.md "wikilink")
  - [淺月灣](../Page/淺月灣.md "wikilink")
  - [比華利山別墅](../Page/比華利山別墅.md "wikilink")
  - [海景山莊](../Page/海景山莊.md "wikilink")
  - [新峰花園](../Page/新峰花園.md "wikilink")
  - [大埔寶馬山](../Page/大埔寶馬山.md "wikilink")
  - [盈峰翠邸](../Page/盈峰翠邸.md "wikilink")
  - [龍成堡](../Page/龍成堡.md "wikilink")

<!-- end list -->

  - [悠然山莊](../Page/悠然山莊.md "wikilink")
  - [翡翠花園](../Page/翡翠花園.md "wikilink")
  - [鹿茵山莊](../Page/鹿茵山莊.md "wikilink")
  - [新翠山莊](../Page/新翠山莊.md "wikilink")
  - [聚豪天下](../Page/聚豪天下.md "wikilink")
  - [雍怡雅苑](../Page/雍怡雅苑.md "wikilink")
  - [康樂園](../Page/康樂園.md "wikilink")
  - [帝琴灣](../Page/帝琴灣.md "wikilink") （[西沙路](../Page/西沙路.md "wikilink")）
  - [滌濤山](../Page/滌濤山.md "wikilink")

### 鄉村

[HK_TaiHang_FuiShaWai.JPG](https://zh.wikipedia.org/wiki/File:HK_TaiHang_FuiShaWai.JPG "fig:HK_TaiHang_FuiShaWai.JPG")是大埔區現存最完整及最宏偉的古老圍村。\]\]

按[明朝史籍記載在當時已存在的大埔古村有](../Page/明朝.md "wikilink")[塔門](../Page/塔門.md "wikilink")、[白沙澳](../Page/白沙澳.md "wikilink")、[茭塘村](../Page/茭塘村.md "wikilink")、[榕樹澳](../Page/榕樹澳.md "wikilink")、[荔枝莊](../Page/荔枝莊.md "wikilink")、[泮涌及](../Page/泮涌.md "wikilink")[陶子峴](../Page/陶子峴.md "wikilink")，但現時的居民多是[清朝遷界後才移徙到來](../Page/清朝.md "wikilink")。大埔區內現存在清朝建立的鄉村120多條。

[六零年代因建造](../Page/香港1960年代.md "wikilink")[船灣淡水湖的關係](../Page/船灣淡水湖.md "wikilink")，將[船灣海沿岸原有六條村落](../Page/船灣海.md "wikilink")（小滘、大滘、金竹排、橫嶺背連橫嶺頭、涌尾及涌背），近千名村民搬遷到[大埔墟的](../Page/大埔墟.md "wikilink")[陸鄉里](../Page/六鄉新村.md "wikilink")。白沙頭洲三門仔村的村民希望維持原有鄉村社群，選擇在大埔[鹽田仔](../Page/鹽田仔_\(大埔區\).md "wikilink")[三門仔新村重新安居立戶](../Page/三門仔.md "wikilink")。

### 大型商場

  - 大型商場：

<!-- end list -->

  - [大埔超級城](../Page/大埔超級城.md "wikilink")
  - [大埔廣場](../Page/大埔廣場.md "wikilink")
  - [翠屏商場](../Page/翠屏商場.md "wikilink")
  - [寶湖商場](../Page/寶湖商場.md "wikilink")
  - [昌運中心](../Page/昌運中心.md "wikilink")
  - [新興商場](../Page/新興商場.md "wikilink")
  - [新達廣場](../Page/新達廣場.md "wikilink")

<!-- end list -->

  - 屋邨商場：

<!-- end list -->

  - [大元街市](../Page/大元街市.md "wikilink") 及
    [大元美食廣場](../Page/大元美食廣場.md "wikilink")
  - [廣福商場](../Page/廣福商場.md "wikilink")
  - [富善中心](../Page/富善中心.md "wikilink")
  - [太和廣場](../Page/太和廣場.md "wikilink")
  - [富亨商場](../Page/富亨商場.md "wikilink")
  - [運頭塘邨商場](../Page/運頭塘邨商場.md "wikilink")

## 公共設施

[KingLawKaShuk_Frontview.jpg](https://zh.wikipedia.org/wiki/File:KingLawKaShuk_Frontview.jpg "fig:KingLawKaShuk_Frontview.jpg")

### 教育

#### 大專院校

[HK_HKIEd_TaiPoCampus_AcademicBldg.JPG](https://zh.wikipedia.org/wiki/File:HK_HKIEd_TaiPoCampus_AcademicBldg.JPG "fig:HK_HKIEd_TaiPoCampus_AcademicBldg.JPG")

  - [香港教育大學](../Page/香港教育大學.md "wikilink")

#### 中學教育

#### 小學教育

#### 幼兒教育

#### 特殊教育

### 醫療

  - [雅麗氏何妙齡那打素醫院](../Page/雅麗氏何妙齡那打素醫院.md "wikilink")
  - [大埔醫院](../Page/大埔醫院.md "wikilink")

<!-- end list -->

  - [大埔賽馬會診所](../Page/大埔賽馬會診所.md "wikilink")
  - [大埔王少清診療所](../Page/大埔王少清診療所.md "wikilink")

### 文康娛樂

[Tai_Po_Complex.jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_Complex.jpg "fig:Tai_Po_Complex.jpg")
[HK_LungMeiBeach.JPG](https://zh.wikipedia.org/wiki/File:HK_LungMeiBeach.JPG "fig:HK_LungMeiBeach.JPG")


  - 博物館及古蹟

<!-- end list -->

  - [上碗窰樊仙宮](../Page/上碗窰樊仙宮.md "wikilink")
  - [文武二帝廟](../Page/文武二帝廟.md "wikilink")
  - [大埔碗窰窰址](../Page/大埔碗窰窰址.md "wikilink")
  - [舊北區理民府](../Page/舊北區理民府.md "wikilink")
  - [前政務司官邸](../Page/前政務司官邸.md "wikilink")
  - [敬羅家塾](../Page/敬羅家塾.md "wikilink")
  - [香港鐵路博物館](../Page/香港鐵路博物館.md "wikilink")
  - [人類民俗館](../Page/人類民俗館.md "wikilink")

<!-- end list -->

  - 戲院

<!-- end list -->

  - [金都戲院](../Page/金都戲院.md "wikilink")（已結業）
  - [寶華戲院](../Page/寶華戲院.md "wikilink")（已結業）
  - [銀星戲院](../Page/銀星戲院.md "wikilink")（已結業）
  - [鴻基戲院](../Page/鴻基戲院.md "wikilink")（已結業）
  - [星輝戲院](../Page/星輝戲院.md "wikilink")（已結業）

<!-- end list -->

  - 圖書館

<!-- end list -->

  - [大埔公共圖書館](../Page/大埔公共圖書館.md "wikilink")

<!-- end list -->

  - 社區中心及會堂

<!-- end list -->

  - [大埔文娛中心](../Page/大埔文娛中心.md "wikilink")
  - [大埔社區中心](../Page/大埔社區中心.md "wikilink")
  - [富亨鄰里社區中心](../Page/富亨鄰里社區中心.md "wikilink")
  - [太和鄰里社區中心](../Page/太和鄰里社區中心.md "wikilink")
  - [運頭塘鄰里社區中心](../Page/運頭塘鄰里社區中心.md "wikilink")
  - [富善社區會堂](../Page/富善社區會堂.md "wikilink")
  - [廣福社區會堂](../Page/廣福社區會堂.md "wikilink")
  - [大元社區會堂](../Page/大元社區會堂.md "wikilink")

<!-- end list -->

  - 康樂場地

<!-- end list -->

  - [大埔舊墟遊樂場](../Page/大埔舊墟遊樂場.md "wikilink")

<!-- end list -->

  - 游泳池

<!-- end list -->

  - [大埔游泳池](../Page/大埔游泳池.md "wikilink")
  - [大埔賽馬會泳池](../Page/大埔賽馬會泳池.md "wikilink")（已停用）

<!-- end list -->

  - 泳灘

<!-- end list -->

  - [龍尾灘](../Page/龍尾灘.md "wikilink") ( 未刊憲 )

<!-- end list -->

  - 草地球場

<!-- end list -->

  - [大埔運動場](../Page/大埔運動場.md "wikilink")
  - [廣福球場](../Page/廣福球場.md "wikilink")
  - [廣福公園足球場](../Page/廣福公園足球場.md "wikilink")

<!-- end list -->

  - 草地滾球場

<!-- end list -->

  - [大埔海濱公園](../Page/大埔海濱公園.md "wikilink")

<!-- end list -->

  - 體育館

<!-- end list -->

  - [李福林體育館](../Page/李福林體育館.md "wikilink")
  - [富亨體育館](../Page/富亨體育館.md "wikilink")
  - [富善體育館](../Page/富善體育館.md "wikilink")
  - [大埔墟體育館](../Page/大埔墟體育館.md "wikilink")
  - [大埔體育館](../Page/大埔體育館.md "wikilink")
  - [太和體育館](../Page/太和體育館.md "wikilink")

<!-- end list -->

  - 公園

<!-- end list -->

  - [八仙嶺郊野公園](../Page/八仙嶺郊野公園.md "wikilink")
  - [船灣郊野公園](../Page/船灣郊野公園.md "wikilink")
  - [西貢東郊野公園](../Page/西貢東郊野公園.md "wikilink")
  - [西貢西郊野公園](../Page/西貢西郊野公園.md "wikilink")
  - [馬鞍山郊野公園](../Page/馬鞍山郊野公園.md "wikilink")
  - [林村郊野公園](../Page/林村郊野公園.md "wikilink")
  - [大帽山郊野公園](../Page/大帽山郊野公園.md "wikilink")
  - [大埔滘自然護理區](../Page/大埔滘自然護理區.md "wikilink")
  - [馬屎洲特別地區](../Page/馬屎洲特別地區.md "wikilink")
  - [大埔海濱公園](../Page/大埔海濱公園.md "wikilink")
  - [元洲仔公園](../Page/元洲仔公園.md "wikilink")
  - [完善公園](../Page/完善公園.md "wikilink")
  - [梅樹坑公園](../Page/梅樹坑公園.md "wikilink")

<!-- end list -->

  - 家樂徑

<!-- end list -->

  - [鶴藪水塘家樂徑](../Page/鶴藪水塘家樂徑.md "wikilink")
  - [大美督家樂徑](../Page/大美督家樂徑.md "wikilink")
  - [黃石家樂徑](../Page/黃石家樂徑.md "wikilink")

<!-- end list -->

  - 自然教育徑

<!-- end list -->

  - [八仙嶺自然教育徑](../Page/八仙嶺自然教育徑.md "wikilink")
  - [灣仔自然教育徑](../Page/灣仔自然教育徑.md "wikilink")
  - [馬屎洲自然教育徑](../Page/馬屎洲自然教育徑.md "wikilink")
  - [大埔滘自然教育徑](../Page/大埔滘自然教育徑.md "wikilink")

<!-- end list -->

  - 郊遊徑

<!-- end list -->

  - [船灣淡水湖郊遊徑](../Page/船灣淡水湖郊遊徑.md "wikilink")
  - [平洲環島郊遊徑](../Page/平洲環島郊遊徑.md "wikilink")
  - [大灘郊遊徑](../Page/大灘郊遊徑.md "wikilink")
  - [嶂上郊遊徑](../Page/嶂上郊遊徑.md "wikilink")

<!-- end list -->

  - 水上活動中心

<!-- end list -->

  - [大美篤水上活動中心](../Page/大美篤水上活動中心.md "wikilink")
  - [賽馬會黃石水上活動中心](../Page/賽馬會黃石水上活動中心.md "wikilink")
  - [香港女童軍總會](../Page/香港女童軍總會.md "wikilink")
      - [梁省德海上活動訓練中心](../Page/梁省德海上活動訓練中心.md "wikilink")
  - [香港青年協會](../Page/香港青年協會.md "wikilink")
      - [大美篤戶外活動中心](../Page/大美篤戶外活動中心.md "wikilink")

<!-- end list -->

  - 度假營及露營場地

<!-- end list -->

  - [香港童軍總會](../Page/香港童軍總會.md "wikilink")
      - [洞梓童軍中心](../Page/洞梓童軍中心.md "wikilink")
  - [大尾篤](../Page/大尾篤.md "wikilink")[白普理賽馬會青年旅舍](../Page/白普理賽馬會青年旅舍.md "wikilink")
  - [鶴藪營地](../Page/鶴藪營地.md "wikilink")
  - [涌背營地](../Page/涌背營地.md "wikilink")
  - [東平洲營地](../Page/東平洲營地.md "wikilink")
  - [北潭凹營地](../Page/北潭凹營地.md "wikilink")
  - [黃石營地](../Page/黃石營地.md "wikilink")
  - [大灘營地](../Page/大灘營地.md "wikilink")
  - [灣仔半島(南)營地](../Page/灣仔半島\(南\)營地.md "wikilink")
  - [灣仔半島(西)營地](../Page/灣仔半島\(西\)營地.md "wikilink")
  - [嶂上營地](../Page/嶂上營地.md "wikilink")
  - [猴塘溪營地](../Page/猴塘溪營地.md "wikilink")
  - [水浪窩營地](../Page/水浪窩營地.md "wikilink")

<!-- end list -->

  - 寺觀廟宇

<!-- end list -->

  - [香港定慧寺](../Page/香港定慧寺.md "wikilink")
  - [大光園](../Page/大光園.md "wikilink")
  - [半春園](../Page/半春園.md "wikilink")
  - [長霞精舍](../Page/長霞精舍.md "wikilink")
  - [省躬草堂](../Page/省躬草堂.md "wikilink")
  - [萬德苑](../Page/萬德苑.md "wikilink")
  - [忠和精舍](../Page/忠和精舍.md "wikilink")
  - [大埔舊墟天后宮](../Page/大埔舊墟天后廟.md "wikilink")
  - [樟樹灘協天宮](../Page/樟樹灘協天宮.md "wikilink")
  - [元洲仔大王爺神廟](../Page/大王爺廟_\(元洲仔\).md "wikilink")
  - [官坑七聖古廟](../Page/官坑七聖古廟.md "wikilink")
  - [泰亨天后宮與文帝古廟](../Page/泰亨天后宮與文帝古廟.md "wikilink")
  - [泰亨觀音古廟](../Page/泰亨觀音古廟.md "wikilink")

<!-- end list -->

  - 燒烤

<!-- end list -->

  - 大美督嘉年華燒烤場\[5\]

## 旅遊景點

[Tai_Po_Waterfront_Park_(D09_28).jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_Waterfront_Park_\(D09_28\).jpg "fig:Tai_Po_Waterfront_Park_(D09_28).jpg")內的[香港回歸紀念塔紀念香港主權的移交](../Page/香港回歸紀念塔.md "wikilink")\]\]
[Hong_Kong_Railway_Museum_Overview2_201208.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Railway_Museum_Overview2_201208.jpg "fig:Hong_Kong_Railway_Museum_Overview2_201208.jpg")
[PloverCoveReservoir_BicycleLane.jpg](https://zh.wikipedia.org/wiki/File:PloverCoveReservoir_BicycleLane.jpg "fig:PloverCoveReservoir_BicycleLane.jpg")主壩頂單車徑\]\]
[Tai_Po_Kau_Nature_Reserve_Waterfall_2013.jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_Kau_Nature_Reserve_Waterfall_2013.jpg "fig:Tai_Po_Kau_Nature_Reserve_Waterfall_2013.jpg")\]\]
[HK_HoiHaWan.JPG](https://zh.wikipedia.org/wiki/File:HK_HoiHaWan.JPG "fig:HK_HoiHaWan.JPG")

  - [黃石碼頭](../Page/黃石碼頭.md "wikilink")、賽馬會黃石水上活動中心
  - [海下灣海岸公園](../Page/海下灣.md "wikilink")
  - [北潭坳](../Page/北潭坳.md "wikilink")（[麥理浩徑第三段起點](../Page/麥理浩徑.md "wikilink")）
  - [水浪窩](../Page/水浪窩.md "wikilink")
  - [大埔滘自然護理區](../Page/大埔滘自然護理區.md "wikilink")
  - [馬屎洲特別地區](../Page/馬屎洲特別地區.md "wikilink")
  - [嘉里白鷺湖互動中心](../Page/嘉里白鷺湖互動中心.md "wikilink")
  - [大尾篤及](../Page/大尾篤.md "wikilink")[船灣淡水湖](../Page/船灣淡水湖.md "wikilink")
  - [新娘潭](../Page/新娘潭.md "wikilink")
  - [吐露港公路單車徑](../Page/吐露港公路單車徑.md "wikilink")
  - [大埔海濱公園](../Page/大埔海濱公園.md "wikilink")
    (園內設有[香港回歸紀念塔](../Page/香港回歸紀念塔.md "wikilink"))
  - [林村許願樹](../Page/林村許願樹.md "wikilink")
  - [嘉道理農場](../Page/嘉道理農場.md "wikilink")
  - [塔門](../Page/塔門.md "wikilink")\[6\]
  - [東平洲](../Page/東平洲.md "wikilink")
  - [慈山寺](../Page/慈山寺.md "wikilink")
  - [大埔農墟](../Page/大埔農墟.md "wikilink")
  - [香港鐵路博物館](../Page/香港鐵路博物館.md "wikilink")
  - [綠匯學苑](../Page/綠匯學苑.md "wikilink")
  - [文武二帝廟](../Page/文武二帝廟.md "wikilink")

## 經濟發展

### 工業

大埔的工業大部分設於[大埔工業邨](../Page/大埔工業邨.md "wikilink")（第26區內約62公頃填海闢拓的土地）和第8區位於[汀角路的混合式工業和辦公室用途的建築物內](../Page/汀角路.md "wikilink")，而其餘的工業則設於少數傳統的地下物業和寮屋構築物內。大埔工業邨位於大埔新市鎮東邊的填海地，是香港首個工業邨。另外，[白石角一幅面積約為](../Page/白石角.md "wikilink")22公頃的公眾填土土地正進行興建[香港科學園](../Page/香港科學園.md "wikilink")，科學園分三期發展，第一，第二期和第三期已經完成。

### 漁農業

漁農業是昔日大埔的主要經濟活動，居民主要以務農或捕魚為生。[1950年代成立協助農業發展的](../Page/香港1950年代.md "wikilink")[嘉道理農場座落大埔的](../Page/嘉道理農場.md "wikilink")[白牛石](../Page/白牛石.md "wikilink")。

以往的耕地部分改為種植蔬菜、花卉及養魚之用，但亦有很多耕地被荒廢。務農人士由於綠色產品成為時尚，有機耕作乘時而起，於「[大埔農墟](../Page/大埔農墟.md "wikilink")」直銷有機蔬菜<small>\[7\]</small>。「大埔農墟」原為「七約農產品擺賣場」，是當年[太和市](../Page/太和市.md "wikilink")（大埔新墟）的一部份，由[七約鄉公所管理](../Page/文武二帝廟.md "wikilink")，作為批發議價的公秤手，後因新市鎮發展和農地減少而式微<small>\[8\]</small>。

傳統漁業主要是深海捕魚，漁民以漁船為家，大埔漁民主要的捕魚區是[塔門](../Page/塔門.md "wikilink")、[較流灣](../Page/較流灣.md "wikilink")、[深灣及](../Page/深灣.md "wikilink")[鹽田仔](../Page/鹽田仔_\(大埔區\).md "wikilink")。隨著海水污染導致魚獲減少，很多漁民已轉業或上岸居住。大埔現時有七個政府劃定的海魚養殖區，分別是塔門、較流灣、深灣、[老虎笏](../Page/老虎笏.md "wikilink")、[榕樹凹](../Page/榕樹凹.md "wikilink")、鹽田仔及鹽田仔東。

## 體育

### 足球

  - [大埔足球會](../Page/大埔足球會.md "wikilink")

## 交通

### [港鐵](../Page/港鐵.md "wikilink")

  - <font color={{東鐵綫色彩}}>█</font>
    [東鐵綫](../Page/東鐵綫.md "wikilink")：[大埔墟站](../Page/大埔墟站.md "wikilink")
    - [太和站](../Page/太和站_\(香港\).md "wikilink")

### 道路

  - 幹線道路

<!-- end list -->

  - [HK_Route9.svg](https://zh.wikipedia.org/wiki/File:HK_Route9.svg "fig:HK_Route9.svg")[9號幹線](../Page/香港9號幹線.md "wikilink")：[吐露港公路](../Page/吐露港公路.md "wikilink")、[粉嶺公路](../Page/粉嶺公路.md "wikilink")

<!-- end list -->

  - 市區道路

<!-- end list -->

  - [大埔公路](../Page/大埔公路.md "wikilink")（近大埔新市鎮一段）
  - [南運路](../Page/南運路.md "wikilink")
  - [汀角路](../Page/汀角路.md "wikilink")（近大埔新市鎮一段）
  - [大埔太和路](../Page/大埔太和路.md "wikilink")
  - [廣福道](../Page/廣福道.md "wikilink")
  - [完善路](../Page/完善路.md "wikilink")
  - [寶鄉街](../Page/寶鄉街.md "wikilink")

<!-- end list -->

  - 郊區道路

<!-- end list -->

  - [汀角路](../Page/汀角路.md "wikilink")
  - [大埔公路](../Page/大埔公路.md "wikilink")
  - [林錦公路](../Page/林錦公路.md "wikilink")
  - [西沙路](../Page/西沙路.md "wikilink")
  - [北潭路](../Page/北潭路.md "wikilink")
  - [新娘潭路](../Page/新娘潭路.md "wikilink")
  - [海下路](../Page/海下路.md "wikilink")

### 巴士

[Tai_Po_Central_Bus_Terminus_200905.jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_Central_Bus_Terminus_200905.jpg "fig:Tai_Po_Central_Bus_Terminus_200905.jpg")

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 公共小巴

<!-- end list -->

  - [大埔中心](../Page/大埔中心.md "wikilink")（大埔太和路） ↔
    [旺角](../Page/旺角.md "wikilink")[西洋菜街](../Page/西洋菜街.md "wikilink")
  - [上水](../Page/上水.md "wikilink") ↔ [佐敦道](../Page/佐敦道.md "wikilink")
  - [大埔](../Page/大埔_\(香港\).md "wikilink") ↔
    [上水](../Page/上水.md "wikilink")
  - [大埔](../Page/大埔_\(香港\).md "wikilink") ↔
    [元朗](../Page/元朗.md "wikilink")
  - [大埔](../Page/大埔_\(香港\).md "wikilink") ↔
    [荃灣](../Page/荃灣.md "wikilink")
  - [觀塘](../Page/觀塘.md "wikilink") ↔ [落馬洲](../Page/落馬洲.md "wikilink")
  - [上水](../Page/上水.md "wikilink") ↔ [灣仔大丸](../Page/灣仔.md "wikilink")

## 未來發展

2014年1月，[香港政府將大埔區](../Page/香港政府.md "wikilink")8幅土地全部改劃為住宅用途，預計可興建15,410個公營及2360個私人住宅。\[9\]。

## 参考文献

### 引用

### 来源

  - 書籍

<!-- end list -->

  - 《大埔風物志》，[大埔區議會](../Page/大埔區議會.md "wikilink")
  - 《香港地質考察指引》，[香港土木工程拓展署](../Page/香港土木工程拓展署.md "wikilink")，ISBN
    978-962-02-0372-5

## 外部連結

  - [大埔區議會](https://web.archive.org/web/20120614181942/http://www.districtcouncils.gov.hk/tp/tc/welcome.html)

  - 大埔區議會選區分界圖

      - [1](http://www.eac.gov.hk/pdf/distco/maps/dc2011p1.pdf)（[市中心](../Page/大埔新市鎮.md "wikilink")、[林村](../Page/林村.md "wikilink")、[大埔滘](../Page/大埔滘.md "wikilink")、[汀角](../Page/汀角.md "wikilink")）
      - [2](http://www.eac.gov.hk/pdf/distco/maps/dc2011p2.pdf)（[船灣](../Page/船灣.md "wikilink")、[西貢北](../Page/西貢北.md "wikilink")、[塔門](../Page/塔門.md "wikilink")、[平洲](../Page/東平洲.md "wikilink")）

  - [大埔新市鎮的發展](https://web.archive.org/web/20041112151946/http://www.cedd.gov.hk/tc/about/achievements/regional/regi_taipo.htm)

  - [《新界規劃小冊子——大埔》](http://www.pland.gov.hk/pland_tc/press/publication/nt_pamphlet02/tp_html/index.html)
    - 規劃署

  - [大埔鄉事委員會](../Page/大埔鄉事委員會.md "wikilink")

  - [大埔討論區](http://www.tpfans.com/)

  -
{{-}}  {{-}}

[Category:香港行政區劃](../Category/香港行政區劃.md "wikilink")
[大埔區](../Category/大埔區.md "wikilink")

1.
2.

3.  [新任大埔區民政事務專員履新](https://www.info.gov.hk/gia/general/201810/24/P2018102400320.htm)

4.

5.

6.

7.  [大埔農墟](http://market.fedvmcs.org/about_us/about_us.htm)

8.  [新界居民的集體回憶—趁墟](http://market.fedvmcs.org/about_us/history/history.htm)


9.  [大埔八地建萬單位](http://orientaldaily.on.cc/cnt/news/20140107/00174_001.html)
    《東方日報》 2014年1月7日