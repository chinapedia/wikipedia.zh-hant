[Shakuhachi_and_Paixiao_of_theTang_Dynasty_2011-07.JPG](https://zh.wikipedia.org/wiki/File:Shakuhachi_and_Paixiao_of_theTang_Dynasty_2011-07.JPG "fig:Shakuhachi_and_Paixiao_of_theTang_Dynasty_2011-07.JPG")\]\]
[Imperial_Encyclopaedia_-_Music_-_pic012_-_排簫.svg](https://zh.wikipedia.org/wiki/File:Imperial_Encyclopaedia_-_Music_-_pic012_-_排簫.svg "fig:Imperial_Encyclopaedia_-_Music_-_pic012_-_排簫.svg")
**排簫**是[中國古代一種](../Page/中國.md "wikilink")[吹奏樂器](../Page/吹奏樂器.md "wikilink")，與[編鐘](../Page/編鐘.md "wikilink")、[編磬均是上古時期中國重要的樂器](../Page/編磬.md "wikilink")。從春秋到秦漢的排簫在民間廣泛流傳，在漢代音樂扮演更重要的角色，但這樂器隨後失傳，直到近代被重新製造。[韓國的簫](../Page/韓國.md "wikilink")（소）正是從排簫演變過來，並用於祭祠儀式上。在歷史上，它的名字分別有底簫、雅簫、頌簫、舜簫、參差、鳳翼、短簫、雲簫、秦簫，其形象或有差異，但用法大致相同。

## 歷史

排簫的前身名叫[籥](../Page/籥.md "wikilink")，《詩經》中已有記述。傳說虞舜時曾出現一部稱為「簫韶」的樂舞，主要是用排簫演奏。\[1\]到了南北朝及隋唐年間，更成為宮廷雅樂的重要樂器，並流傳至[高麗](../Page/高麗.md "wikilink")、[龜茲](../Page/龜茲.md "wikilink")、[日本等地使用](../Page/日本.md "wikilink")。目前日本[奈良](../Page/奈良.md "wikilink")[東大寺仍保存唐代的兩支排簫遺器](../Page/東大寺.md "wikilink")，但當時在《東大寺獻物帳》上，則稱作「甘竹律」。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:東亞管樂器](../Category/東亞管樂器.md "wikilink")
[Category:木管乐器](../Category/木管乐器.md "wikilink")

1.  [排簫的歷史](http://www.chinamedley.com/langyuan/paixiao/)
2.  [排簫的歷史](http://www.chinamedley.com/langyuan/paixiao/)