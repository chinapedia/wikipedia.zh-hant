**石原立也**（，），日本動畫[监督](../Page/监督.md "wikilink")（[導演](../Page/導演.md "wikilink")）、[演出](../Page/演出.md "wikilink")（副导演），[京都府](../Page/京都府.md "wikilink")[舞鶴市出身](../Page/舞鶴市.md "wikilink")。[京都動畫的董事](../Page/京都動畫.md "wikilink")。

代表作有電視動畫《[AIR](../Page/AIR_\(遊戲\).md "wikilink")》、《[Kanon](../Page/Kanon.md "wikilink")》、《[CLANNAD](../Page/CLANNAD.md "wikilink")》、《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》、《[中二病也要談戀爱](../Page/中二病也要談戀爱.md "wikilink")》、《[奏響吧！上低音號](../Page/奏響吧！上低音號～歡迎加入北宇治高中吹奏樂社～.md "wikilink")》等。

## 經歷

在畢業後進入[京都動畫工作室](../Page/京都動畫.md "wikilink")（京都動畫的前身）。

在2005年時第一次擔任電視動畫《[AIR](../Page/AIR_\(動畫\).md "wikilink")》的導演，由於高品質的作畫及演出而廣受好評。

## 參與作品

### 電視動畫

  - [妙手小廚師](../Page/妙手小廚師.md "wikilink")（1987-1989年）動畫
  - [美味大挑戰](../Page/美味大挑戰.md "wikilink")（1988年）動畫、動畫檢查
  - [大耳鼠](../Page/大耳鼠.md "wikilink")（1989-1991年）動畫
  - [江戸っ子ボーイ がってん太助](../Page/江戸っ子ボーイ_がってん太助.md "wikilink")（1990年）原畫
  - [21衛門](../Page/21衛門.md "wikilink")（1991年）動畫
  - [蜡笔小新](../Page/蜡笔小新.md "wikilink")（1992年）動畫
  - [哆啦A夢](../Page/哆啦A夢_\(電視動畫\).md "wikilink")（1979-2005年版）分鏡、演出
  - [霸王大系龍騎士](../Page/霸王大系龍騎士.md "wikilink")（1994-1995年）分鏡
  - [夢幻遊戲](../Page/夢幻遊戲.md "wikilink")（1995年）演出
  - [天才寶貝](../Page/天才寶貝.md "wikilink")（1996年）分鏡、演出
  - [天地無用\!](../Page/天地無用!.md "wikilink")（1997年）分鏡、演出
  - [貓狐警探](../Page/貓狐警探.md "wikilink")（1997年）分鏡、演出
  - [神奇寶貝動畫版](../Page/神奇寶貝動畫版.md "wikilink")（1997年）130話分鏡、演出
  - [夢幻拉拉](../Page/夢幻拉拉.md "wikilink")（1998年）分鏡、演出
  - [天使不設防\!](../Page/天使不設防!.md "wikilink")（1999年）分鏡、演出
  - [POWER STONE](../Page/POWER_STONE.md "wikilink")（1999年）分鏡、演出
  - [犬夜叉](../Page/犬夜叉.md "wikilink")（2000-2004年）分鏡、演出
  - [OH\!スーパーミルクチャン](../Page/OH!スーパーミルクチャン.md "wikilink") （2000年）分鏡、演出
  - [我們這一家](../Page/我們這一家.md "wikilink") （2003年放映分）分鏡、演出
  - [驚爆危機?校園篇](../Page/驚爆危機.md "wikilink")（2003年）分鏡、演出
  - [AIR](../Page/AIR_\(動畫\).md "wikilink")（2005年）**導演**、分鏡、演出／OPED分鏡、演出
  - [涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")（2006年版）**導演**、腳本、分鏡、演出／OP分鏡、演出
  - [Kanon](../Page/Kanon.md "wikilink")（2006-2007年）**導演**、分鏡／OPED分鏡、演出
  - [幸運☆星](../Page/幸運☆星.md "wikilink")（2007年）分鏡、演出
  - [CLANNAD](../Page/CLANNAD.md "wikilink")（2007年）**導演**、分鏡、演出／OP分鏡、演出
  - [CLANNAD ～AFTER
    STORY～](../Page/CLANNAD.md "wikilink")（2008年）**導演**、分鏡、演出／OPED分鏡、演出
  - [K-ON\!](../Page/K-ON!.md "wikilink")（2009年）分鏡、演出／OP分鏡、演出
  - [涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")（2009年版）**導演**、腳本、分鏡、演出
  - [日常](../Page/日常.md "wikilink")（2011年）**導演**、分鏡、演出、腳本
  - [中二病也想談戀愛！](../Page/中二病也想談戀愛！.md "wikilink")（2012年）**導演**、分鏡、演出／OPED分鏡、演出
  - [玉子市場](../Page/玉子市場.md "wikilink")（2013年）分鏡、演出
  - [境界的彼方](../Page/境界的彼方.md "wikilink")（2013年）分鏡
  - 中二病也想談戀愛！戀（2014年）**導演**、分鏡、演出／OP分鏡、演出
  - [Free\!-Eternal Summer-](../Page/Free!.md "wikilink")（2014年）分鏡
  - [甘城輝煌樂園救世主](../Page/甘城輝煌樂園救世主.md "wikilink")（2014年）分鏡、演出
  - [奏響吧！上低音號](../Page/奏響吧！上低音號～歡迎加入北宇治高中吹奏樂社～.md "wikilink")（2015年）**導演**

### 其他

  - [動感超人VS高叉魔王](../Page/蠟筆小新：動感超人大戰泳裝魔王.md "wikilink")（1993年）動畫
  - [酸梅星王子 宇宙の果てからパンパロパン\!](../Page/酸梅星王子.md "wikilink")（1994年）原畫
  - [魔天戰神 ～After War～](../Page/魔天戰神.md "wikilink")（1995年）分鏡、演出
  - [古靈精怪 接著，那個夏天開始](../Page/古靈精怪.md "wikilink")（1996年）演出
  - [櫻花大戰 櫻華絢爛](../Page/櫻花大戰.md "wikilink")（1997年）演出
  - [Dancing Blade
    任性桃天使\!](../Page/Dancing_Blade_任性桃天使!.md "wikilink")（1998年）導演、演出
  - [Dancing Blade 任性桃天使\!II 〜Tears of
    Eden〜](../Page/Dancing_Blade_任性桃天使!.md "wikilink")（1999年）導演、演出
  - [大雄與機器人王國](../Page/大雄與機器人王國.md "wikilink")（2002年）OP分鏡、演出 ※**石原達也**名義

## 相關條目

  - [京都動畫](../Page/京都動畫.md "wikilink")

[Category:日本動畫導演](../Category/日本動畫導演.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:京都動畫人物](../Category/京都動畫人物.md "wikilink")