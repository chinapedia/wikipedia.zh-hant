**無鬚鱈科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱈形目的其中一科](../Page/鱈形目.md "wikilink")。

## 分類

**無鬚鱈科**下分5個屬，如下：

### 節狼鱈屬(*Lyconodes*)

  - [銀節狼鱈](../Page/銀節狼鱈.md "wikilink")(*Lyconodes argenteus*)

### 狼鱈屬(*Lyconus*)

  - [短狼鱈](../Page/短狼鱈.md "wikilink")(*Lyconus brachycolus*)
  - [羽狼鱈](../Page/羽狼鱈.md "wikilink")(*Lyconus pinnatus*)

### 尖尾無鬚鱈屬(*Macruronus*)

  - [南非尖尾無鬚鱈](../Page/南非尖尾無鬚鱈.md "wikilink")(*Macruronus capensis*)
  - [尖尾無鬚鱈](../Page/尖尾無鬚鱈.md "wikilink")(*Macruronus maderensis*)
  - [南美尖尾無鬚鱈](../Page/南美尖尾無鬚鱈.md "wikilink")(*Macruronus magellanicus*)
  - [藍尖尾無鬚鱈](../Page/藍尖尾無鬚鱈.md "wikilink")(*Macruronus novaezelandiae*)

### 無鬚鱈屬(*Merluccius*)

  - [大鱗無鬚鱈](../Page/大鱗無鬚鱈.md "wikilink")(*Merluccius albidus*)
  - [巴拿馬無鬚鱈](../Page/巴拿馬無鬚鱈.md "wikilink")(*Merluccius angustimanus*)
  - [澳洲無鬚鱈](../Page/澳洲無鬚鱈.md "wikilink")(*Merluccius australis*)
  - [雙線無鬚鱈](../Page/雙線無鬚鱈.md "wikilink")(*Merluccius bilinearis*)
  - [南非無鬚鱈](../Page/南非無鬚鱈.md "wikilink")(*Merluccius capensis*)
  - [智利無鬚鱈](../Page/智利無鬚鱈.md "wikilink")(*Merluccius gayi gayi*)
  - [秘魯無鬚鱈](../Page/秘魯無鬚鱈.md "wikilink")(*Merluccius gayi peruanus*)
  - [亨氏無鬚鱈](../Page/亨氏無鬚鱈.md "wikilink")(*Merluccius hernandezi*)
  - [赫氏無鬚鱈](../Page/赫氏無鬚鱈.md "wikilink")(*Merluccius hubbsi*)
  - [歐洲無鬚鱈](../Page/歐洲無鬚鱈.md "wikilink")(*Merluccius merluccius*)
  - [深水無鬚鱈](../Page/深水無鬚鱈.md "wikilink")(*Merluccius paradoxus*)
  - [潘太無鬚鱈](../Page/潘太無鬚鱈.md "wikilink")(*Merluccius patagonicus*)
  - [波氏無鬚鱈](../Page/波氏無鬚鱈.md "wikilink")(*Merluccius polli*)
  - [北太平洋無鬚鱈](../Page/北太平洋無鬚鱈.md "wikilink")(*Merluccius productus*)
  - [塞內加爾無鬚鱈](../Page/塞內加爾無鬚鱈.md "wikilink")(*Merluccius senegalensis*)
  - [塔斯曼尼亞無鬚鱈](../Page/塔斯曼尼亞無鬚鱈.md "wikilink")(*Merluccius tasmanicus*)

### 斯氏無鬚鱈屬(*Steindachneria*)

  - [發光斯氏無鬚鱈](../Page/發光斯氏無鬚鱈.md "wikilink")(*Steindachneria argentea*)

## 參考資料

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/AjaxTree/tree.php)

[\*](../Category/無鬚鱈科.md "wikilink")