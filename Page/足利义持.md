**足利义持**（）是[室町幕府第](../Page/室町幕府.md "wikilink")4代[將軍](../Page/征夷大將軍.md "wikilink")。父亲是[足利義滿](../Page/足利義滿.md "wikilink")，母亲是[安藝法眼的女儿](../Page/安藝法眼.md "wikilink")[藤原庆子](../Page/藤原庆子.md "wikilink")。有同母弟[足利义教](../Page/足利义教.md "wikilink")、異母弟[足利义嗣等](../Page/足利义嗣.md "wikilink")。正室为[日野資康的女儿](../Page/日野資康.md "wikilink")[日野荣子](../Page/日野荣子.md "wikilink")。子为[足利義量](../Page/足利義量.md "wikilink")。

## 生平

足利義持是第三代將軍[足利義滿的庶子](../Page/足利義滿.md "wikilink")。由於足利義滿與正室[日野業子和繼室](../Page/日野業子.md "wikilink")[日野康子都沒有生下兒子](../Page/日野康子.md "wikilink")，因此將足利義持立為嗣子，並讓日野康子收他為養子。1394年（[應永元年](../Page/應永.md "wikilink")），足利義滿將將軍之位讓給了只有9歲的義持，義滿轉任[太政大臣仍掌握實權](../Page/太政大臣.md "wikilink")，室町幕府的所有評定都在義滿居住的[北山第舉行](../Page/北山第.md "wikilink")，義持沒有參與政務的權力。

就在義持就任將軍的這一年，義滿的次子[義嗣](../Page/足利義嗣.md "wikilink")（也是義持的同父異母弟）出生。足利義滿非常寵愛義嗣，因此義持和義滿之間的關係不好。由於偏愛的緣故，1408年（應永15年），足利義滿在死去之前曾向[後小松天皇引見自己的愛子足利義嗣](../Page/後小松天皇.md "wikilink")，試圖讓義嗣獲得天皇的支持；此時的足利義持則被義滿支出府邸，負責京都的警衛事務。但足利義滿死後，足利義持掌握了實權，義嗣一派的勢力受到打壓。

足利義滿死後，義持居住在[花之御所](../Page/花之御所.md "wikilink")（室町第，位於今[京都市](../Page/京都市.md "wikilink")[上京區](../Page/上京區.md "wikilink")），翌年遷往三條坊門邸（原[足利義詮邸宅](../Page/足利義詮.md "wikilink")，位於今京都市[中京區](../Page/中京區.md "wikilink")）居住。義滿生前的居所北山第則被冷落。1419年[日野康子死後](../Page/日野康子.md "wikilink")，[北山第除了金閣以外的建築物全被拆毀](../Page/北山第.md "wikilink")。

在政治上，足利義持有擔任[管領一職的幕府宿老](../Page/管領.md "wikilink")[斯波義將等人的輔佐](../Page/斯波義將.md "wikilink")，一改義滿的開放政策，恢復了「武家政權」在政治上的特色，變得較為保守。足利義持疏遠了義滿所庇護的[世阿彌](../Page/世阿彌.md "wikilink")（一說是由於義持本人討厭他的緣故），辭退了[後小松天皇給足利義滿所上的](../Page/後小松天皇.md "wikilink")「[太上天皇](../Page/太上天皇.md "wikilink")」的追號，並且停止了[對明朝的貿易](../Page/明日貿易.md "wikilink")。足利義持自封為後小松上皇的[院別當](../Page/院別當.md "wikilink")。同時又讓弟弟[法尊出家於皇族的寺院](../Page/法尊.md "wikilink")[仁和寺](../Page/仁和寺.md "wikilink")，以增強幕府在朝廷中的影響力。這是繼[鐮倉時代](../Page/鐮倉時代.md "wikilink")[九條道家派兒子](../Page/九條道家.md "wikilink")[法助出家於仁和寺後](../Page/法助.md "wikilink")，第二例非皇族成員出家於該寺，也是[近世以前的最後一例](../Page/近世.md "wikilink")。

1410年（應永17年），[後龜山法皇](../Page/後龜山天皇.md "wikilink")（也就是原[南朝的最後一任天皇](../Page/南朝_\(日本\).md "wikilink")）出奔[吉野](../Page/吉野.md "wikilink")，抗議幕府和朝廷違背了[明德和約中](../Page/明德和約.md "wikilink")[兩統迭立的規定](../Page/兩統迭立.md "wikilink")，立實仁親王（後來的[稱光天皇](../Page/稱光天皇.md "wikilink")）為皇太子。1412年稱光天皇即位後，1414年南朝的支持者[北畠滿雅發動叛亂](../Page/北畠滿雅.md "wikilink")，法皇重申了兩統迭立的要求，最終在[上野親王的調停下雙方達成和解](../Page/說成親王.md "wikilink")。1414年，義持脅迫[斯波滿種](../Page/斯波滿種.md "wikilink")（斯波義將的侄兒）退隱[高野山](../Page/高野山.md "wikilink")，使[斯波氏失權](../Page/斯波氏.md "wikilink")。1416年，[關東發生](../Page/關東.md "wikilink")[上杉禪秀之亂](../Page/上杉禪秀之亂.md "wikilink")，足利義持將參與此事的弟弟[足利義嗣關押在](../Page/足利義嗣.md "wikilink")[相國寺](../Page/相國寺.md "wikilink")。

上杉禪秀之亂後，[富樫滿成誣稱](../Page/富樫滿成.md "wikilink")[管領](../Page/管領.md "wikilink")[細川滿元以及](../Page/細川滿元.md "wikilink")[斯波義重聯合許多](../Page/斯波義重.md "wikilink")[守護大名和](../Page/守護大名.md "wikilink")[公卿](../Page/公卿.md "wikilink")，試圖推翻室町幕府。（也有人認為這是足利義持的授意，試圖借此契機來打倒一些有力的地方守護大名。）不久義持命令富樫滿成殺害了義嗣，並流放了許多大名和公卿。但不久細川滿元等人便稱富樫滿成謀反，義持命令將滿成誅殺。

在經歷了這一系列事件後，1423年（應永30年）義持退位，由16歲的兒子[義量接任將軍職](../Page/足利義量.md "wikilink")。翌年6月出家於[等持院](../Page/等持院.md "wikilink")，但幕府的實權仍在義持掌握之中。但兩年後即1425年（應永32年）義量因病死去，享年18歲。

1427年（應永34年），兼任[播磨](../Page/播磨.md "wikilink")、[備前](../Page/備前.md "wikilink")、[美作三國](../Page/美作.md "wikilink")[守護大名的](../Page/守護大名.md "wikilink")[赤松義則逝世](../Page/赤松義則.md "wikilink")，在[赤松持貞的干涉下](../Page/赤松持貞.md "wikilink")，義持剝奪了義則之子[滿祐的繼承權](../Page/赤松滿祐.md "wikilink")，導致赤松滿祐出奔播磨，準備叛亂。不久足利義持誣稱赤松持貞同[高橋殿](../Page/高橋殿.md "wikilink")（[足利義滿的一名側室](../Page/足利義滿.md "wikilink")）通姦，迫令持貞[切腹](../Page/切腹.md "wikilink")。這次事件削弱了[赤松氏的勢力](../Page/赤松氏.md "wikilink")，強化了幕府中央集權。翌年正月，義持在浴室裡撓傷了臀部並受到了感染，不久死去，享年43歲。由於義持死後沒有立繼承人，幕府決定讓義持的弟弟[梶井義承](../Page/梶井義承.md "wikilink")、[大覺寺義昭](../Page/大覺寺義昭.md "wikilink")、[虎山永隆](../Page/虎山永隆.md "wikilink")、義圓四人，由抽籤方式選出一人擔任將軍職。最後義圓被選中，改名[義教](../Page/足利義教.md "wikilink")，成為新任將軍。

## 評價

足利義持統治期間，原來被[懷良親王所壓制的](../Page/懷良親王.md "wikilink")[九州地區](../Page/九州島.md "wikilink")[大名](../Page/大名_\(稱謂\).md "wikilink")[大友氏](../Page/大友氏.md "wikilink")、[大內氏](../Page/大內氏.md "wikilink")、[菊池氏歸附幕府](../Page/菊池氏.md "wikilink")；但[關東地區卻由](../Page/關東.md "wikilink")[鐮倉公方統治](../Page/鐮倉公方.md "wikilink")，處於半獨立狀態之下。雖然仍有一些不安定的因素，但義持能承繼[義滿的政策](../Page/足利義滿.md "wikilink")，令政治的小康狀態可以維持，是室町時代中比較安定的時代。

足利義持與父親足利義滿不和，但仍沿襲了義滿的一些基本政策。義持本人是由[斯波義將等有力的](../Page/斯波義將.md "wikilink")[守護大名擁上將軍之位的](../Page/守護大名.md "wikilink")，但義持掌權後，清洗了身邊的[富樫滿成和有力守護](../Page/富樫滿成.md "wikilink")[赤松持貞的勢力](../Page/赤松持貞.md "wikilink")。因此與父親足利義滿一樣，義持也對將軍絕對權威的確立起著一定作用。

## 水墨畫與藝能

足利義持統治的時期是日本[詩畫軸的全盛期](../Page/詩畫軸.md "wikilink")，現被認定為日本國寶的「[瓢鮎圖](../Page/瓢鮎圖.md "wikilink")」（[如拙畫](../Page/如拙.md "wikilink")）就是這個時期所繪的。義持本人也頗為愛好詩畫。義持也酷愛[傳統藝能中的](../Page/日本傳統藝能.md "wikilink")[田樂](../Page/田樂.md "wikilink")，曾支持[增阿彌對田樂的普及](../Page/增阿彌.md "wikilink")。但他對父親義滿所愛好的[猿樂卻頗為反感](../Page/猿樂.md "wikilink")。

足利義持的[水墨畫作品](../Page/水墨畫.md "wikilink")：

  - 布袋圖（[重要文化財](../Page/重要文化財.md "wikilink")，藏於[福岡市美術館](../Page/福岡市美術館.md "wikilink")）
  - [白衣觀音圖](../Page/白衣觀音.md "wikilink")（[京都](../Page/京都.md "wikilink")[相國寺長得院藏](../Page/相國寺.md "wikilink")）
  - [寒山圖](../Page/寒山.md "wikilink")（[重要美術品](../Page/重要美術品.md "wikilink")，[岡山縣立美術館藏](../Page/岡山縣立美術館.md "wikilink")，紙本墨畫）
  - 杜子美圖（[西雅圖藝術博物館藏](../Page/西雅圖藝術博物館.md "wikilink")）

## 義持時代的外交

  - 與[明朝的關係](../Page/明朝.md "wikilink")：[足利義滿執掌政權期間](../Page/足利義滿.md "wikilink")，接受[明朝的](../Page/明朝.md "wikilink")[冊封](../Page/冊封.md "wikilink")，成為明朝的[藩屬](../Page/藩屬.md "wikilink")，並推動同明朝的[勘合貿易](../Page/勘合貿易.md "wikilink")。1408年義滿逝世，足利義持掌握實權，曾以「日本國世子源義持」的名義，遣使向明朝報告父喪；[明成祖派](../Page/明成祖.md "wikilink")[中官周全前往弔唁](../Page/中官.md "wikilink")，並冊封義持為「[日本國王](../Page/日本國王.md "wikilink")」。\[1\]但對明朝的稱臣引起[管領](../Page/管領.md "wikilink")[斯波義將等人的強烈不滿](../Page/斯波義將.md "wikilink")，最終義持於1411年斷絕了[同明朝的貿易](../Page/明日貿易.md "wikilink")。其給明朝解釋則是「本國開闢以來，百事皆聽諸神」、「靈神託人謂曰：我國自古不向外國稱臣」，並且聲稱要改變義滿的外交政策，「今後無受外國使命，因垂戒子孫，固守勿墜」。\[2\]義持斷絕了對明朝的貿易後，對騷擾明朝海岸的[倭寇持縱容態度](../Page/倭寇.md "wikilink")，對明朝要求取締倭寇的要求置之不理。\[3\]
  - 與[朝鮮王朝的關係](../Page/朝鮮王朝.md "wikilink")：1419年，由於日本[對馬島發生饑荒](../Page/對馬島.md "wikilink")，當地倭寇襲擊了明朝沿岸，途經[朝鮮時順帶襲擊了](../Page/朝鮮半島.md "wikilink")[忠清道庇仁](../Page/忠清道.md "wikilink")（今[南韓](../Page/南韓.md "wikilink")[忠清南道](../Page/忠清南道.md "wikilink")[舒川郡](../Page/舒川郡.md "wikilink")）和[黃海道海州](../Page/黃海道.md "wikilink")（今[北韓](../Page/北韓.md "wikilink")[黃海南道](../Page/黃海南道.md "wikilink")[海州市](../Page/海州市.md "wikilink")）一帶海岸。這使當時執掌朝鮮政權的[太上王](../Page/太上王.md "wikilink")[李芳遠非常震怒](../Page/李芳遠.md "wikilink")，派[李從茂攻打對馬島以清剿倭寇](../Page/李從茂.md "wikilink")（[應永外寇](../Page/應永外寇.md "wikilink")）。此後幕府轄下的對馬島與朝鮮關係一度陷入低谷，直到1423年太上王李芳遠逝世後才恢復貿易。1443年[對馬](../Page/對馬國.md "wikilink")[守護](../Page/守護.md "wikilink")[宗貞盛與朝鮮訂立](../Page/宗貞盛.md "wikilink")[嘉吉條約](../Page/嘉吉條約.md "wikilink")，此後[日朝貿易更加活躍](../Page/日朝貿易.md "wikilink")。

## 履歷

※以下日期為日本舊曆。

  - 1394年（[應永元](../Page/應永.md "wikilink")）12月17日：敘任[正五位下](../Page/正五位.md "wikilink")[左近衛中將](../Page/左近衛中將.md "wikilink")，並被封為[征夷大将軍](../Page/征夷大将軍.md "wikilink")。
  - 1395年（應永2）6月3日：升任[從四位下](../Page/從四位.md "wikilink")。
  - 1396年（應永3）1月28日：兼任[美作權守](../Page/美作國.md "wikilink")；4月20日:升任[正四位下](../Page/正四位.md "wikilink")；9月12日：補任[参議](../Page/参議.md "wikilink")。
  - 1397年（應永4）1月5日：升任[從三位](../Page/從三位.md "wikilink")；3月29日:轉任[權中納言](../Page/權中納言.md "wikilink")。
  - 1398年（應永5）1月5日：升任[正三位](../Page/正三位.md "wikilink")。
  - 1400年（應永7）1月5日：升任[從二位](../Page/從二位.md "wikilink")。
  - 1401年（應永8）3月24日：轉任[權大納言](../Page/權大納言.md "wikilink")。
  - 1402年（應永9）1月6日：升任[正二位](../Page/正二位.md "wikilink")；11月19日:升任[從一位](../Page/從一位.md "wikilink")。
  - 1406年（應永13）8月17日：兼任[右近衛大將](../Page/右近衛大將.md "wikilink")。
  - 1407年（應永14）1月5日：兼任[右馬寮御監事務](../Page/右馬寮御監.md "wikilink")。
  - 1409年（應永16）3月23日：轉任[内大臣](../Page/内大臣.md "wikilink")。
  - 1412年（應永19）5月：辭去右近衛大將職務。
  - 1413年（應永20）10月22日：兼任[淳和獎學院兩院別當事務](../Page/源氏長者.md "wikilink")。
  - 1419年（應永26）8月29日：辭去[内大臣](../Page/内大臣.md "wikilink")。
  - 1422年（應永29）3月18日：辭去征夷大将軍；4月25日：出家。
  - 1428年（應永35）1月18日：薨去；1月23日：贈[太政大臣](../Page/太政大臣.md "wikilink")。

## 接受義持偏諱的人物

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [二条持基](../Page/二条持基.md "wikilink")
  - [二条持通](../Page/二条持通.md "wikilink")
  - [日野持光](../Page/日野持光.md "wikilink")
  - [赤松持貞](../Page/赤松持貞.md "wikilink")
  - [足利持氏](../Page/足利持氏.md "wikilink")
  - [有馬持家](../Page/有馬持家.md "wikilink")
  - [一色持信](../Page/一色持信.md "wikilink")
  - [一色持範](../Page/一色持範.md "wikilink")
  - [上杉持房](../Page/上杉持房.md "wikilink")
  - [上野持賴](../Page/上野持賴.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [大内持盛](../Page/大内持盛.md "wikilink")
  - [大内持世](../Page/大内持世.md "wikilink")
  - [大館持房](../Page/大館持房.md "wikilink")
  - [小笠原持長](../Page/小笠原持長.md "wikilink")
  - [京極持清](../Page/京極持清.md "wikilink")
  - [京極持高](../Page/京極持高.md "wikilink")（持光）
  - [河野持通](../Page/河野通久.md "wikilink")（通久）
  - [斯波持種](../Page/斯波持種.md "wikilink")
  - [伊達持宗](../Page/伊達持宗.md "wikilink")
  - [富樫持春](../Page/富樫持春.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [土岐持益](../Page/土岐持益_\(守護\).md "wikilink")
  - [土岐持賴](../Page/土岐持賴.md "wikilink")
  - [仁木持尹](../Page/仁木持尹.md "wikilink")
  - [仁木持長](../Page/仁木持長.md "wikilink")
  - [肥田持重](../Page/肥田持重.md "wikilink")
  - [肥田持直](../Page/肥田持直.md "wikilink")
  - [畠山持国](../Page/畠山持国.md "wikilink")
  - [畠山持富](../Page/畠山持富.md "wikilink")
  - [細川持有](../Page/細川持有.md "wikilink")
  - [細川持賢](../Page/細川持賢.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [細川持常](../Page/細川持常.md "wikilink")
  - [細川持春](../Page/細川持春.md "wikilink")
  - [細川持元](../Page/細川持元.md "wikilink")
  - [細川持之](../Page/細川持之.md "wikilink")
  - [桃井持信](../Page/桃井持信.md "wikilink")
  - [山名持豐](../Page/山名持豐.md "wikilink")（宗全）
  - [山名持熙](../Page/山名持熙.md "wikilink")
  - [六角持綱](../Page/六角持綱.md "wikilink")

</div>

## 腳註

## 参考文献

  - 伊藤喜良，「」（收錄於伊藤所著的《》（思文閣出版、1993年） ISBN 978-4-7842-0781-7）
  - 小林保夫，「」（收錄於上横手雅敬所編的《》（吉川弘文館、2001年） ISBN 978-4-642-02805-9）
  - 伊藤喜良，「足利義持 (人物叢書) 」（吉川弘文館、2008年） ISBN 978-4-642-05246-7

|-    |-  |-

[Yoshimochi](../Category/室町幕府將軍.md "wikilink")
[Yoshimochi](../Category/將軍足利氏.md "wikilink")

1.
2.  《日本歷史》，第六章第三節。浙江大學日本文化研究所著，高等教育出版社2003年出版，第120頁。ISBN 7-04-012147-6

3.  參見[《明史·日本傳》](http://www.guoxue.com/shibu/24shi/mingshi/ms_322.htm)