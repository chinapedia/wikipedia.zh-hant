**肽类激素**（）由[氨基酸通过](../Page/氨基酸.md "wikilink")[肽键连接而成](../Page/肽键.md "wikilink")，最小的肽类激素可由三个氨基酸组成，如[促甲状腺激素释放激素](../Page/促甲状腺激素释放激素.md "wikilink")（，**TRH**）。多数肽类激素可由十几个，几十个或乃至上百及几百个氨基酸组成。肽类激素的主要分泌器官是[下丘脑及](../Page/下丘脑.md "wikilink")[脑垂体](../Page/脑垂体.md "wikilink")，在其他一些器官中，如[胃肠道](../Page/胃肠道.md "wikilink")、[脑组织](../Page/脑组织.md "wikilink")、[肺及](../Page/肺.md "wikilink")[心脏中也发现一些](../Page/心脏.md "wikilink")[内源性肽类激素](../Page/内源性.md "wikilink")，多数处于研究阶段。\[1\]

## 分类

肽类激素按[分子量大小分为](../Page/分子量.md "wikilink")[多肽激素和](../Page/多肽激素.md "wikilink")[蛋白质激素](../Page/蛋白质激素.md "wikilink")，两者无明显界限，一般认为地把分子量高于5000的称为蛋白质激素。

## 制备

肽类激素可用[脏器为原料提取或用](../Page/脏器.md "wikilink")[全合成法制得](../Page/全合成法.md "wikilink")。大分子的蛋白质激素，目前只能依靠天然来源，20[肽左右的多肽激素的全合成法则越来越重要](../Page/肽.md "wikilink")。[组合化学是肽类激素药物研究的重要方法](../Page/组合化学.md "wikilink")。

## 书写

  - 在书写多肽的[一级结构时](../Page/一级结构.md "wikilink")，按照国际惯例将[氨基酸的英文名缩写](../Page/氨基酸.md "wikilink")，自左至右排列，左为[氨基末端](../Page/氨基.md "wikilink")([*N*](../Page/氮.md "wikilink")-末端)，右边为[羰基末端](../Page/羰基.md "wikilink")([*C*](../Page/碳.md "wikilink")-末端)。
  - 不少多肽类激素独有[环状结构](../Page/环状结构.md "wikilink")，它们通过[肽键或通过](../Page/肽键.md "wikilink")[半胱氨酸的](../Page/半胱氨酸.md "wikilink")[巯基成](../Page/巯基.md "wikilink")[二硫键而](../Page/二硫键.md "wikilink")[环合](../Page/环合.md "wikilink")，从环状接头的氨基酸作为第一个氨基酸。
  - 参见[图示](../Page/:File:Peptide_Hormones_Name_Writing.png.md "wikilink")

## 性质

  - 当构成多肽激素中有多[官能团氨基酸时](../Page/官能团.md "wikilink")，分子中会有自由[氨基](../Page/氨基.md "wikilink")、[胍基或](../Page/胍基.md "wikilink")[羧基](../Page/羧基.md "wikilink")，此时整个多肽激素表现出有[碱性或](../Page/碱性.md "wikilink")[酸性](../Page/酸性.md "wikilink")。有时分子中有[巯基](../Page/巯基.md "wikilink")、[酚羟基存在](../Page/酚羟基.md "wikilink")，可表现出与[金属](../Page/金属.md "wikilink")[离子](../Page/离子.md "wikilink")[螯合的倾向](../Page/螯合.md "wikilink")。
  - 氨基酸形成[肽键后分子中的](../Page/肽键.md "wikilink")[苯环](../Page/苯环.md "wikilink")(如[Tyr](../Page/w:Tyr.md "wikilink")[酪氨酸](../Page/酪氨酸.md "wikilink"))或[杂环](../Page/杂环.md "wikilink")(如[Trp](../Page/w:Trp.md "wikilink")[色氨酸](../Page/色氨酸.md "wikilink")、[His](../Page/w:His.md "wikilink")[组氨酸](../Page/组氨酸.md "wikilink"))可以表现出[紫外吸收](../Page/紫外吸收.md "wikilink")，可用以鉴定。组成多肽的天然氨基酸是L-[构型](../Page/构型.md "wikilink")，它们有[旋光性](../Page/旋光性.md "wikilink")。
  - 多肽激素药物在[胃肠道中难以吸收](../Page/胃肠道.md "wikilink")，且会受[酶的作用而](../Page/酶.md "wikilink")[失活](../Page/失活.md "wikilink")，一般不做口服用药。

## 常见多肽类激素

  - [胰岛素](../Page/胰岛素.md "wikilink")
    [Insulin](../Page/w:Insulin.md "wikilink")
      - 由51个氨基酸组成，[化学结构](../Page/:File:Insulin.png.md "wikilink")
      - 有降[血糖作用](../Page/血糖.md "wikilink")，用於治疗[糖尿病](../Page/糖尿病.md "wikilink")
  - [降钙素](../Page/降钙素.md "wikilink")
    [Calcitonin](../Page/w:Calcitonin.md "wikilink")
      - 由32个氨基酸组成
      - 可降低[血钙](../Page/血钙.md "wikilink")，用於治疗[骨质疏鬆症](../Page/骨质疏鬆症.md "wikilink")
  - [人類絨毛膜促性腺素](../Page/人類絨毛膜促性腺素.md "wikilink") [Human **C**horionic
    **G**onadotropin](../Page/w:'''H'''uman_Chorionic_Gonadotropin.md "wikilink")
      - 一种[糖蛋白](../Page/糖蛋白.md "wikilink")
      - [促性腺激素](../Page/促性腺激素.md "wikilink")，用於[不孕症](../Page/不孕症.md "wikilink")、功能性[子宫出血](../Page/子宫出血.md "wikilink")、先兆[流产及](../Page/流产.md "wikilink")[男性性功能减退](../Page/男性性功能减退.md "wikilink")
  - [促黄体激素释放激素](../Page/促黄体激素释放激素.md "wikilink")([戈纳瑞林](../Page/戈纳瑞林.md "wikilink"))
    [**L**uteinizing **H**ormone **R**eleasing
    **H**ormone](../Page/w:Luteinizing_Hormone_Releasing_Hormone.md "wikilink")([Gonadorelin](../Page/w:Gonadorelin.md "wikilink"))
      - 特异性刺激[垂体前葉腺体释放和合成](../Page/垂体.md "wikilink")[促黄体激素](../Page/促黄体激素.md "wikilink")([**L**uteinizing
        **H**ormone](../Page/w:Luteinizing_Hormone.md "wikilink"))
      - 大剂量时刺激[垂体释放和合成](../Page/垂体.md "wikilink")[促卵泡激素](../Page/促卵泡激素.md "wikilink")([**F**ollicle-**S**timulating
        **H**ormone](../Page/w:Follicle_Stimulating_Hormone.md "wikilink"))
  - [催产素](../Page/催产素.md "wikilink")([缩宫素](../Page/缩宫素.md "wikilink"))
    [Oxytocin](../Page/w:Oxytocin.md "wikilink")
      - 能收缩[子宫](../Page/子宫.md "wikilink")，促进[排卵](../Page/排卵.md "wikilink")
      - 用于[引产](../Page/引产.md "wikilink")、產後出血和[子宫复原及](../Page/子宫复原.md "wikilink")[催乳](../Page/催乳.md "wikilink")
  - [加压素](../Page/加压素.md "wikilink")
    [Vasopressin](../Page/w:Vasopressin.md "wikilink")
      - 能收缩小[动脉和](../Page/动脉.md "wikilink")[毛细血管](../Page/毛细血管.md "wikilink")，升高[血压](../Page/血压.md "wikilink")
      - 用於[產後出血](../Page/產後出血.md "wikilink")、[消化道出血及](../Page/消化道出血.md "wikilink")[尿崩等](../Page/尿崩.md "wikilink")
  - [促肾上腺皮质激素](../Page/促肾上腺皮质激素.md "wikilink")
    [**A**dreno**C**ortico**T**ropic
    **H**ormone](../Page/w:AdrenoCorticoTropic_Hormone.md "wikilink")([Corticotropin](../Page/w:Corticotropin.md "wikilink"))
      - 刺激[肾上腺皮质合成和分泌](../Page/肾上腺.md "wikilink")[皮質類固醇](../Page/皮質類固醇.md "wikilink")
  - [生长激素](../Page/生长激素.md "wikilink")
    [Somatopropin](../Page/w:Somatopropin.md "wikilink")
      - 刺激生长，用於内源性生长激素分泌不足的[侏儒儿童](../Page/侏儒.md "wikilink")

## 参考文献

## 参见

  - [激素](../Page/激素.md "wikilink")
  - [药物列表](../Page/药物列表.md "wikilink")

{{-}}

[肽类激素](../Category/肽类激素.md "wikilink")

1.