**天眷**（元年：1138年 -
末年：1140年）是[金熙宗的](../Page/金熙宗.md "wikilink")[年号](../Page/年号.md "wikilink")。金熙宗使用天眷这个年号一共三年。

天會十五年十二月[癸未日](../Page/癸未.md "wikilink")（公元1138年2月7日），詔改陰曆第二年（1138年）為天眷元年，大赦\[1\]。

1141年正月[癸丑日](../Page/癸丑.md "wikilink")（陽曆2月21日），謝太廟，大赦，改元皇統。

## 大事记

## 出生

## 逝世

## 纪年

| 天眷                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 1138年                          | 1139年                          | 1140年                          |
| [干支](../Page/干支纪年.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权的同期年号
      - [紹興](../Page/紹興_\(宋高宗\).md "wikilink")（1131年正月至1162年十二月）：[宋](../Page/南宋.md "wikilink")—[宋高宗趙構之年號](../Page/宋高宗.md "wikilink")
      - [天彰寶嗣](../Page/天彰寶嗣.md "wikilink")（1133年至1138年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李陽煥之年號](../Page/李陽煥.md "wikilink")
      - [紹明](../Page/紹明.md "wikilink")（1138年至1140年）：李朝—[李天祚之年號](../Page/李天祚.md "wikilink")
      - [大定](../Page/大定_\(李天祚\).md "wikilink")（1140年至1162年）：李朝—李天祚之年號
      - [康國](../Page/康國_\(耶律大石\).md "wikilink")（1134年正月至1143年十二月）：西遼—德宗耶律大石之年號
      - [大德](../Page/大德_\(夏崇宗\).md "wikilink")（1135年正月至1139年十二月）：西夏—夏崇宗李乾順之年號
      - [大慶](../Page/大慶_\(夏仁宗\).md "wikilink")（1140年正月至1143年十二月）：[西夏](../Page/西夏.md "wikilink")—夏仁宗[李仁孝之年號](../Page/李仁孝.md "wikilink")
      - [保延](../Page/保延.md "wikilink")（1135年四月二十七日至1141年七月十日）:日本崇德天皇

## 參考資料

<div class="references-small">

<references />

</div>

[Category:金朝年號](../Category/金朝年號.md "wikilink")
[Category:12世纪中国年号](../Category/12世纪中国年号.md "wikilink")
[Category:1130年代中国](../Category/1130年代中国.md "wikilink")
[Category:1140年代中国](../Category/1140年代中国.md "wikilink")

1.  《[金史](../Page/金史.md "wikilink")·熙宗本纪》