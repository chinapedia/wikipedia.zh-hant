**烟管鱼科**或**馬鞭魚科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[棘背魚目的其中一科](../Page/棘背魚目.md "wikilink")。刺鱼目烟管鱼科约4种热带海域鱼类的统称。

## 分布

本[科魚類分布於各大洋](../Page/科.md "wikilink")[熱帶至](../Page/熱帶.md "wikilink")[溫帶地區](../Page/溫帶.md "wikilink")。

## 深度

由表層至水深100公尺左右。

## 特徵

本[科魚體延長](../Page/科.md "wikilink")，前方平扁，吻部延伸為管狀，頭後有發達的頸板。口小，開於吻端。在身體背面及腹面正中線往往有1列稜線。側線完全，自胸鰭上方向後下方沿體側中央縱走，直達尾端，前部不明顯，後方呈鋸棘狀。背鰭1枚，無棘，偏於體之後方。尾鰭凹入，中央二鰭條延長為絲狀，稱為尾絲。

## 分類

**烟管鱼科**其下僅有一屬：

  - 煙管魚屬(*Fistularia*)
      - [無鱗煙管魚](../Page/棘烟管鱼.md "wikilink")(*Fistularia
        commersonii*)：又稱康氏馬鞭魚, 棘烟管鱼。
      - [角煙管魚](../Page/角煙管魚.md "wikilink")(*Fistularia corneta*)
      - [扁煙管魚](../Page/扁煙管魚.md "wikilink")(*Fistularia depressa*)
      - [鱗煙管魚](../Page/鱗煙管魚.md "wikilink")(*Fistularia petimba*)：又稱馬鞭魚。
      - [藍斑煙管魚](../Page/藍斑煙管魚.md "wikilink")(*Fistularia tabacaria*)
      - [毛煙管魚](../Page/毛煙管魚.md "wikilink")(*Fistularia villosa*)

## 生態

本[科魚棲息在沿岸淺水域或礁石區](../Page/科.md "wikilink")，多半單獨或三兩成群，很少游動，加上體色不鮮豔，容易欺敵，以管狀吻部吸食獵物。

## 經濟利用

食用魚，可用煎食或[紅燒](../Page/紅燒.md "wikilink")。

## 参考文献

  - [臺灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/烟管鱼科.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")