**GBS**可以指：

  - [Game Boy音樂格式](../Page/Game_Boy.md "wikilink")（Game Boy Sound
    System）
  - [胃繞道手術](../Page/胃繞道手術.md "wikilink")（gastric bypass
    surgery）：一種[減肥](../Page/減肥.md "wikilink")[手術](../Page/手術.md "wikilink")。
  - [蕭伯納](../Page/蕭伯納.md "wikilink")（George Bernard
    Shaw）：[愛爾蘭](../Page/愛爾蘭.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")。
  - [岐阜放送](../Page/岐阜放送.md "wikilink")（Gifu Broadcasting
    System）：[日本](../Page/日本.md "wikilink")[岐阜縣的一家](../Page/岐阜縣.md "wikilink")[電視台兼](../Page/電視台.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")。
  - 十億位元每秒（[gigabits](../Page/gigabit.md "wikilink") per
    second）：[儲存裝置](../Page/儲存裝置.md "wikilink")[速率單位](../Page/速率.md "wikilink")。
  - 十億位元組（[gigabytes](../Page/gigabyte.md "wikilink")）：儲存裝置容量單位。
  - [全球廣播服務](../Page/全球廣播服務.md "wikilink")（Global Broadcast
    Service）：[美軍的一種](../Page/美軍.md "wikilink")[人造衛星通訊系統](../Page/人造衛星.md "wikilink")。
  - [GNU构建系统](../Page/GNU构建系统.md "wikilink")（GNU build
    system）：一套[GNU](../Page/GNU.md "wikilink")[軟體開發工具](../Page/軟體開發工具.md "wikilink")，又名[Autotools](../Page/Autotools.md "wikilink")。
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")（Gold Bauhinia
    Star）：[香港特別行政區政府頒授的一種](../Page/香港特別行政區政府.md "wikilink")[勳章](../Page/勳章.md "wikilink")。
  - [湖南廣播影視集團](../Page/湖南廣播影視集團.md "wikilink")（Golden Eagle Broadcasting
    System）：[中華人民共和國](../Page/中華人民共和國.md "wikilink")[湖南省的官方](../Page/湖南省.md "wikilink")[傳媒集團](../Page/傳媒.md "wikilink")，于2010年撤销改制成[湖南广播电视台](../Page/湖南广播电视台.md "wikilink")（HBS）。
  - [重力基礎](../Page/重力基礎.md "wikilink")（gravity base
    structure）：一種[建築結構](../Page/建築.md "wikilink")，通常用於海上[鑽油平台與近海](../Page/鑽油平台.md "wikilink")[風力發電廠](../Page/風力發電.md "wikilink")。
  - [陸上爆炸模擬器](../Page/陸上爆炸模擬器.md "wikilink")（ground burst simulator）
  - [B型鏈球菌](../Page/B型鏈球菌.md "wikilink")（Group B
    Streptococcus）：一種常在於[腸胃道下部和生殖](../Page/腸胃道.md "wikilink")[泌尿道的](../Page/泌尿道.md "wikilink")[細菌](../Page/細菌.md "wikilink")，常造成新生兒和孕婦疾病。
  - [Google Browser
    Sync](../Page/Google_Browser_Sync.md "wikilink")：[Google開發的](../Page/Google.md "wikilink")[Mozilla
    Firefox專用](../Page/Mozilla_Firefox.md "wikilink")[外掛程式](../Page/外掛程式.md "wikilink")。
  - [導向巴士](../Page/導向巴士.md "wikilink")（guided
    bus）：接受外來媒介引導行駛的[巴士系統](../Page/巴士.md "wikilink")。
  - [格林-巴利症候群](../Page/格林-巴利症候群.md "wikilink")（Guillain-Barré
    syndrome）：一種自身免疫疾病，影響末梢神經系統。
  - [基耶爾莫·巴羅斯·謝洛托](../Page/基耶爾莫·巴羅斯·謝洛托.md "wikilink")（Guillermo Barros
    Schelotto）：[阿根廷](../Page/阿根廷.md "wikilink")[足球員](../Page/足球.md "wikilink")。
  - [京畿北科學高等學校](../Page/京畿北科學高等學校.md "wikilink")（Gyeonggibuk Science
    High
    School）：[大韓民國](../Page/大韓民國.md "wikilink")[京畿道](../Page/京畿道.md "wikilink")[議政府市的一所](../Page/議政府市.md "wikilink")[高級中學](../Page/高級中學.md "wikilink")。
  - [幾內亞比索的](../Page/幾內亞比索.md "wikilink")[IOC國家代碼](../Page/IOC.md "wikilink")。