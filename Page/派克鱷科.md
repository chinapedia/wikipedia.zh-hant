**派克鱷科**（Euparkeriidae）是原始[主龍形下綱的一科](../Page/主龍形下綱.md "wikilink")，是群小型、[肉食性動物](../Page/肉食性.md "wikilink")，生存於[三疊紀的](../Page/三疊紀.md "wikilink")[安尼阶到](../Page/安尼阶.md "wikilink")[卡尼阶](../Page/卡尼阶.md "wikilink")。派克鱷科的化石已在[南非](../Page/南非.md "wikilink")、[俄羅斯發現](../Page/俄羅斯.md "wikilink")。不同於[引鱷科](../Page/引鱷科.md "wikilink")，派克鱷科體型很小、身體修長，在身理機能上可以二足行走。

派克鱷科是以著名的[派克鱷為名](../Page/派克鱷.md "wikilink")，是在1913年由[南非古生物學家](../Page/南非.md "wikilink")[羅伯特·布魯姆](../Page/羅伯特·布魯姆.md "wikilink")（Robert
Broom）所命名。在1920年，[德國古生物學家](../Page/德國.md "wikilink")[弗雷德里克·馮·休尼](../Page/弗雷德里克·馮·休尼.md "wikilink")（
Friedrich von
Huene）建立派克鱷科，被分類於[偽鱷亞目](../Page/偽鱷亞目.md "wikilink")；當時偽鱷亞目包含許多三疊紀的鱷魚遠親。近年[種系發生學研究](../Page/種系發生學.md "wikilink")，多將派克鱷科分類於[主龍形類的基礎位置](../Page/主龍形類.md "wikilink")。派克鱷科與[鱷魚](../Page/鱷魚.md "wikilink")、[鳥鱷科](../Page/鳥鱷科.md "wikilink")、[恐龍的共同祖先類似](../Page/恐龍.md "wikilink")，但並非直系祖先。

派克鱷科目前的有效屬只有派克鱷，其他的可能屬包含：*Dorosuchus*、*Halazaisuchus*、[歐斯莫斯卡鱷](../Page/歐斯莫斯卡鱷.md "wikilink")（*Osmolskina*）、[王氏鱷](../Page/王氏鱷.md "wikilink")。

## 外部連結

  - [Euparkeriidae](https://web.archive.org/web/20050409174045/http://www.palaeos.com/vertebrates/Units/270Archosauromorpha/270.400.html#Euparkeriidae#Euparkeriidae)
    from Palaeos.com (technical)

[\*](../Category/主龍形下綱.md "wikilink")