**臺北馬拉松**（）是於每年12月中旬在[中華民國](../Page/中華民國.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")[臺北市舉辦的城市](../Page/臺北市.md "wikilink")[馬拉松競賽](../Page/馬拉松.md "wikilink")，於1986年3月9日舉辦首屆至今，並自2016年起精緻化賽事，僅保留半馬與全馬之比賽項目；其全程馬拉松比賽參加人數為7千人，半程馬拉松則有約2萬人參加。

## 歷史

### 先鋒賽

[中華民國田徑協會在首屆臺北馬拉松之前](../Page/中華民國田徑協會.md "wikilink")，於1985年12月22日，舉行了一場「先鋒賽」讓各國好手體驗在台北市路跑比賽的感覺，當時是以[介壽路](../Page/介壽路.md "wikilink")（現為[凱達格蘭大道](../Page/凱達格蘭大道.md "wikilink")）作為活動地點，比賽分為12公里路跑組與23公里半程馬拉松組，廣受中外好手的好評\[1\]。

### 首屆

由[台北市政府主辦](../Page/台北市政府.md "wikilink")、[中華民國田徑協會協辦](../Page/中華民國田徑協會.md "wikilink")，於1986年3月9日比賽，[總統府前廣場起跑](../Page/中華民國總統府_\(臺北\).md "wikilink")，全程
\[2\]，限制參賽者須有在4小時完成馬拉松的成績才能參賽\[3\]。除了台灣本地選手外，另有[日本](../Page/日本.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[菲律賓與](../Page/菲律賓.md "wikilink")[香港等地選手參加](../Page/香港.md "wikilink")，合共有2,269名選手\[4\]。

### 停辦與應變

1990年—2000年，因為台北市進行[台北捷運的施工](../Page/台北捷運.md "wikilink")，在市區道路舉辦的「臺北馬拉松」因而停辦。在這段期間，多年主辦該項比賽的中華民國田徑協會也和相關單位商討因應措施。

  - [台北國際國道馬拉松](../Page/台北國際國道馬拉松比賽.md "wikilink")：適逢台北市進行台北捷運施工，臺北馬拉松在經歷停辦兩屆後，於1992年起移師國道進行，稱之「**[台北國際國道馬拉松](../Page/台北國際國道馬拉松比賽.md "wikilink")**」。

<!-- end list -->

  - [高雄國際馬拉松](../Page/1995年高雄國際暨區運馬拉松比賽.md "wikilink")：民國84年（1995年），由於[台灣區運動會在](../Page/台灣區運動會.md "wikilink")[高雄市舉辦](../Page/高雄市.md "wikilink")，該項比賽移到高雄市舉行，加上[三陽工業與](../Page/三陽工業.md "wikilink")[慶豐集團聯手](../Page/慶豐集團.md "wikilink")[贊助獲得冠名權利](../Page/贊助.md "wikilink")，故稱之「[慶豐高雄國際馬拉松](../Page/慶豐高雄國際馬拉松.md "wikilink")」。但此時主導馬拉松比賽行政工作的是[中華民國路跑協會](../Page/中華民國路跑協會.md "wikilink")，而非中華民國田徑協會。

### 復辦

2001年11月4日，經台北市政府、[家樂福文教基金會和中華民國路跑協會發起](../Page/家樂福文教基金會.md "wikilink")，臺北馬拉松恢復舉辦，在[臺北市市民廣場舉辦](../Page/市民廣場_\(臺北市\).md "wikilink")，賽程分全程馬拉松42點195公里組，半程馬拉松22公里組，以及11公里及3公里組，參加人數近
5,000名 \[5\]\[6\]。

### 冠名贊助

  - 2004年12月17日至2008年12月21日，[ING安泰人壽提供全額贊助](../Page/ING安泰人壽.md "wikilink")，該期間比賽冠名為**[ING臺北馬拉松](../Page/ING臺北國際馬拉松比賽.md "wikilink")**，並且首度於此賽會中大量招募[志工參與賽會行政工作](../Page/志工.md "wikilink")。但2008年10月ING安泰人壽被[富邦金控併購後](../Page/富邦金控.md "wikilink")，ING臺北馬拉松正式走入歷史。

<!-- end list -->

  - 2009年12月20日至2012年12月16日，因富邦金控合併ING安泰人壽，富邦金控同時承接對臺北馬拉松的贊助，這項路跑活動冠名為**[富邦臺北馬拉松](../Page/富邦臺北馬拉松.md "wikilink")**。

<!-- end list -->

  - 2013年12月15日，比賽更名為臺北富邦馬拉松。

<!-- end list -->

  - 2015年，比賽更名為臺北馬拉松，並停止接受各單位冠名贊助。

## 賽制

### 賽程分組

2013年比賽共分為\[7\]：

  - 馬拉松組，賽程長，限時330分鐘完成。

  - 半程馬拉松組，賽程長，限時210分鐘完成。

  - 9公里組，賽程長，限時90分鐘完成。

  - 警消組，賽程長，限時90分鐘完成。

  - （樂趣組），賽程長約，限時30分鐘完成。

  - 兒童組，賽程長約，限時20分鐘完成。

### 參賽資格

2013年比賽，馬拉松組、半程馬拉松組與9公里組，限1995年含以後出生，且年滿17歳者參賽，設男、女性國際、視障、以及國內參賽者依性別、年齡分組別，視障選手須經中華民國路跑協會認可之團體，依競賽報名規定報名，並指定一名陪跑員，陪跑員不另計算成績；國內參賽者男女年齡組別分法略有不同，詳列如下表\[8\]。

| 男性 | 70歲以上 | 60\~69歲 | 50\~59歲 | 40\~49歲 | 30\~39歲 | 20\~29歲 | 19歲以下 |
| -- | ----- | ------- | ------- | ------- | ------- | ------- | ----- |
| 女性 | 60歲以上 | 50\~59歲 | 40\~49歲 | 30\~39歲 | 20\~29歲 | 19歲以下   |       |
|    |       |         |         |         |         |         |       |

警消組報名時須檢討警員或消防員證，並由所屬單位蓋章認可，若600人名額滿，依9公里組報名。兒童組限國小5、6年級在學學生報名。組不計成績，自由參加\[9\]。

### 路線

2012年比賽，各賽程路線如下表所列\[10\]。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>馬拉松組</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/市民廣場_(臺北市).md" title="wikilink">台北市市民廣場</a>→<a href="../Page/仁愛路_(台北市).md" title="wikilink">仁愛路</a>→<a href="../Page/中山南路_(臺北市).md" title="wikilink">中山南路</a>→<a href="../Page/中山北路_(台北市).md" title="wikilink">中山北路</a>→<a href="../Page/新生北路.md" title="wikilink">新生北路</a>→<a href="../Page/林安泰古厝.md" title="wikilink">濱江街（林安泰古厝）</a>→<a href="../Page/大佳河濱公園.md" title="wikilink">大佳河濱公園</a>→<a href="../Page/迎風河濱公園.md" title="wikilink">迎風河濱公園</a>→<a href="../Page/成美左岸河濱公園.md" title="wikilink">成美左岸河濱公園</a>→<a href="../Page/麥帥一橋.md" title="wikilink">麥帥一橋越堤道</a>→<a href="../Page/成美右岸河濱公園.md" title="wikilink">成美右岸河濱公園</a>→<a href="../Page/彩虹河濱公園.md" title="wikilink">彩虹河濱公園</a>→<a href="../Page/美堤河濱公園.md" title="wikilink">美堤河濱公園</a>→<a href="../Page/圓山河濱公園.md" title="wikilink">圓山河濱公園</a>→<a href="../Page/百齡右岸河濱公園.md" title="wikilink">百齡右岸河濱公園</a>→<a href="../Page/百齡橋.md" title="wikilink">百齡橋越堤道</a>→<a href="../Page/百齡左岸河濱公園.md" title="wikilink">百齡左岸河濱公園</a>→大佳河濱公園→迎風河濱公園→塔悠路5號水門→<a href="../Page/塔悠路.md" title="wikilink">塔悠路</a>→<a href="../Page/健康路（臺北市）.md" title="wikilink">健康路匝道上高架</a>→<a href="../Page/麥帥二橋.md" title="wikilink">麥帥二橋接下</a><a href="../Page/基隆路.md" title="wikilink">基隆路匝道</a>→<a href="../Page/信義路_(台北市).md" title="wikilink">信義路</a>→<a href="../Page/光復南路.md" title="wikilink">光復南路</a>→仁愛路→進入終點台北市市民廣場。</p></td>
</tr>
<tr class="even">
<td><p>半程馬拉松組</p></td>
</tr>
<tr class="odd">
<td><p>台北市市民廣場→仁愛路→中山南路→中山北路→<a href="../Page/北安路（臺北市）.md" title="wikilink">北安路</a>→<a href="../Page/明水路.md" title="wikilink">明水路</a>→<a href="../Page/樂群一路.md" title="wikilink">樂群一路</a>→<a href="../Page/堤頂大道.md" title="wikilink">堤頂大道</a>→<a href="../Page/環東大道.md" title="wikilink">環東大道</a>→麥帥二橋接下基隆路匝道→信義路→光復南路→仁愛路→進入終點台北市市廣場。</p></td>
</tr>
<tr class="even">
<td><p>9 公里組、警消組</p></td>
</tr>
<tr class="odd">
<td><p>台北市市民廣場→仁愛路 →中山南路折返→仁愛路→進入終點台北市市民廣場。</p></td>
</tr>
<tr class="even">
<td><p>Fun run組</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/市府路（臺北市）.md" title="wikilink">市府路集合出發</a>→左轉<a href="../Page/松壽路.md" title="wikilink">松壽路</a>→左轉<a href="../Page/松仁路.md" title="wikilink">松仁路</a>→左轉<a href="../Page/松高路.md" title="wikilink">松高路</a>→左轉市府路→進入終點市民廣場。</p></td>
</tr>
<tr class="even">
<td><p>兒童組</p></td>
</tr>
<tr class="odd">
<td><p>台北市市民廣場→仁愛路→<a href="../Page/延吉街.md" title="wikilink">延吉街口折返</a>(不越過)→仁愛路→進入終點台北市市民廣場。</p></td>
</tr>
</tbody>
</table></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Taipei_International_Marathon_2012_Route_map.png" title="fig:Taipei_International_Marathon_2012_Route_map.png">Taipei_International_Marathon_2012_Route_map.png</a></p></td>
</tr>
</tbody>
</table>

### 最佳成績

臺北馬拉松最佳成績記錄如下\[11\]：

  - 男子組，2016年12月18日，，，02:09:59。
  - 女子組，2011年12月18日，，，02:27:36。

## 相關條目

  - [台灣馬拉松比賽列表](../Page/台灣馬拉松比賽列表.md "wikilink")

## 參考資料暨說明

[Category:台北市體育](../Category/台北市體育.md "wikilink")
[Category:台灣馬拉松](../Category/台灣馬拉松.md "wikilink")
[Category:1986年建立](../Category/1986年建立.md "wikilink")
[Category:12月份的活动](../Category/12月份的活动.md "wikilink")
[Category:ING集团](../Category/ING集团.md "wikilink")
[Category:臺灣觀光年曆](../Category/臺灣觀光年曆.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.