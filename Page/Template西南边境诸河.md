<includeonly>{{\#ifeq:|nocat||}}</includeonly><noinclude>

## 使用方法

本模板会自动把引用它的页面加入分类。引用本模板时使用下列语法，可以避免加入分类：<small>（注意nocat必须为英文小写）</small>

{{|nocat}}

[de:Vorlage:Navigationsleiste Flussnetze der südwestlichen
Grenzgebiete](de:Vorlage:Navigationsleiste_Flussnetze_der_südwestlichen_Grenzgebiete.md "wikilink")
</noinclude>

[Category:中国西南边境诸河](../Category/中国西南边境诸河.md "wikilink")
[Category:中国地理导航模板](../Category/中国地理导航模板.md "wikilink")
[Category:中国河流模板](../Category/中国河流模板.md "wikilink")
[\*](../Category/中国西南边境诸河.md "wikilink")