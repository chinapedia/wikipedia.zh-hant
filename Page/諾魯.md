**瑙魯共和國**，簡稱**瑙魯**（，[瑙魯語](../Page/瑙魯語.md "wikilink")：，英語舊稱：\[1\]），是位於[南太平洋](../Page/南太平洋.md "wikilink")[密克羅尼西亞群島的](../Page/密克羅尼西亞群島.md "wikilink")[島國](../Page/島國.md "wikilink")。最接近的國家是位於[巴納巴島的](../Page/巴納巴島.md "wikilink")[吉里巴斯](../Page/基里巴斯.md "wikilink")，在諾魯以東約300公里。諾魯国土面积为21.3平方公里\[2\]，是世界上最小的島國，也是世界上第三小的国家，僅大於[梵蒂冈及](../Page/梵蒂冈.md "wikilink")[摩納哥](../Page/摩納哥.md "wikilink")。瑙鲁的人口約10,500人，位居世界倒數第三，僅多於[圖瓦盧和](../Page/圖瓦盧.md "wikilink")[梵蒂冈](../Page/梵蒂冈.md "wikilink")\[3\]。

瑙魯的原住民是密克羅尼西亞人和[玻利尼西亞人](../Page/玻利尼西亞人.md "wikilink")，在19世纪末被[德意志帝國吞併成殖民地](../Page/德意志帝國.md "wikilink")\[4\]。[第一次世界大战後](../Page/第一次世界大战.md "wikilink")，瑙魯成為[國際聯盟託管地](../Page/國際聯盟託管地.md "wikilink")，由[澳洲](../Page/澳大利亚.md "wikilink")、[新西兰和](../Page/新西兰.md "wikilink")[英国共同管治](../Page/英国.md "wikilink")。[第二次世界大战諾魯被](../Page/第二次世界大战.md "wikilink")[日本佔領](../Page/日本.md "wikilink")，作為入侵太平洋的跳板。戰後瑙魯再被託管\[5\]，直至1968年才獲得獨立。

縱觀20世紀上半葉，瑙魯是一個由執政者掌握主要經濟資本的國家。由於瑙魯是一個由[磷石組成的島嶼](../Page/磷石.md "wikilink")，[沉積接近地面而能進行簡單的](../Page/沉積.md "wikilink")[露天採礦](../Page/露天採礦.md "wikilink")。1907年起太平洋磷酸鹽公司開始在島上採礦\[6\]，1919年組成[英國磷酸鹽委員會](../Page/英國磷酸鹽委員會.md "wikilink")；獨立後由[國營的瑙魯磷酸鹽公司繼續開採](../Page/国有企业.md "wikilink")。直至1980年代沉積耗盡前，瑙魯仍是[磷酸鹽的主要出口國](../Page/磷酸鹽.md "wikilink")；因此，瑙魯政府在1960年代末至1970年代初期間曾自誇其人均收入是所有[主权国家之中最高](../Page/主权国家.md "wikilink")。\[7\]

可惜的是瑙魯原住民並沒有充分運用這些財富，隨著磷酸鹽儲量的枯竭和採礦帶來的環境惡化，再加上管理全島財富基金的減值，瑙魯政府求助於一些不尋常的方法來獲得收入。在1990年代，瑙魯成為一個[避稅天堂和](../Page/避稅港.md "wikilink")[洗錢中心](../Page/洗錢.md "wikilink")\[8\]。自2005年起，瑙魯接受[澳大利亞政府的援助](../Page/澳大利亞.md "wikilink")，並建立一個拘留所處理非法進入澳大利亞的難民以作為回報。

## 詞源

英國人最初把諾魯命名為可愛的島（）。「諾魯」一詞可能源自諾魯語，意即“我去海灘”；德國殖民者把諾魯稱為或。

## 歷史

[Nauru_Island_under_attack_by_Liberator_bombers_of_the_Seventh_Air_Force..jpg](https://zh.wikipedia.org/wiki/File:Nauru_Island_under_attack_by_Liberator_bombers_of_the_Seventh_Air_Force..jpg "fig:Nauru_Island_under_attack_by_Liberator_bombers_of_the_Seventh_Air_Force..jpg")

約3000年前密克羅尼西亞和玻里尼西亞人已在諾魯定居\[9\]，共有12個部落，國旗的十二角星是代表這十二個部落。在18世紀，後期，一位新西蘭船長在海上航行時發現了諾魯，當時只看到島上有很多土著和茅屋，但這船長並未登陸或加以重視。1798年，[英国船長](../Page/英国.md "wikilink")[約翰·費因率領](../Page/約翰·費因.md "wikilink")「獵手」號抵達諾魯，命名為“”（“可愛的島”），開啟西方世界與諾魯交流的歷史。第一個成功到島上定居的白人，是英國的[威廉·哈利斯](../Page/威廉·哈利斯.md "wikilink")（），1842年，29歲的他來到島上為當地姑娘而著迷，流連忘返，定居下來，前任諾魯總統[勒内·哈里斯](../Page/勒内·哈里斯.md "wikilink")（）便是他的後代。

19世紀末，德國向南太平洋擴張，諾魯自1888年起便成為德國的殖民地\[10\]。當時德國對諾魯並不重視，因為島上除了椰子外，就一無所有。1900年英國磷礦公司的職員在諾島上的一塊石頭上，意外發現了全島都有磷礦，便改變了此島的命運\[11\]\[12\]。

[第一次世界大战期間](../Page/第一次世界大战.md "wikilink")，諾魯被[澳大利亚佔領](../Page/澳大利亚.md "wikilink")，並自1919年起由英國、澳大利亞和[新西兰共管](../Page/新西兰.md "wikilink")，其中澳洲代表三國行使職權。[第二次世界大战期間](../Page/第二次世界大战.md "wikilink")，德國人為了報復，擊沉了該島附近的四艘船隻，並砲轟島上的磷礦設施。1942年日軍轟炸諾魯，居民逃往澳大利亞以免遭日軍的屠殺，於在8月26日被[日本佔領](../Page/日本.md "wikilink")\[13\]，成為日軍在南太平洋上的戰術支援基地，派2,000人駐守，並帶來大批日本人和韓國人，在島上佈署重防和建築兩個小型飛機場，但被盟軍發現及轟炸島上15架飛機，日軍於是槍決島上5名未撤走的歐洲人。此外日軍亦把島上1,200名諾魯人放逐到[楚克島工作](../Page/楚克州.md "wikilink")\[14\]。

二戰之後，諾魯由[联合国委託澳大利亞](../Page/联合国.md "wikilink")、英國及新西蘭共同管理，每年須向聯合國報告，但他們實際上更著重於如何開採島上的磷礦，對於諾魯人的福祉並不重視。聯合國多次要求澳洲政府公佈每年開採的磷礦數量、成本和售價，以期使諾魯人在輸出的同時得到合理的回報，但澳洲政府卻不予理會。諾魯人隨著民智漸開，對澳大利亞人的統治深感不滿，尤其是磷礦的開採，於是全力爭取獨立，收回採礦權。澳洲政府在強大的國際壓力下，無可奈何地結束對諾魯的管治，於1968年1月31日，諾魯共和國宣佈正式[独立](../Page/独立.md "wikilink")，由[漢姆·戴羅伯](../Page/漢姆·戴羅伯.md "wikilink")（）出任總統，並於同年11月成為[大英國協特別成員國](../Page/英联邦.md "wikilink")。1989年，諾魯向[國際法院對澳大利亞作出法律行動](../Page/国际法院.md "wikilink")，指控澳洲政府管治期間未能盡力保護環境，減低採礦帶來的環境破壞\[15\]\[16\]。1999年，諾魯成為聯合國會員國。2016年4月12日，瑙鲁成为[国际货币基金组织和](../Page/国际货币基金组织.md "wikilink")[世界银行的第](../Page/世界银行.md "wikilink")189个成员国\[17\]\[18\]。

## 政治

### 行政

諾魯是一個[總統制的](../Page/總統制.md "wikilink")[共和國](../Page/共和制.md "wikilink")，總統既是國家元首，亦是政府首腦，由國會選舉產生，總統候選人必須是國會議員。

現任總統為[巴倫·瓦卡](../Page/巴倫·瓦卡.md "wikilink")，於2016年6月起擔任第2任期諾魯總統，任期3年。

內閣（2016-2019）由6人組成，每一位內閣成員都兼任多部會的首長\[19\]，由總統直接任命。

### 立法

[Nauru-parliament.jpg](https://zh.wikipedia.org/wiki/File:Nauru-parliament.jpg "fig:Nauru-parliament.jpg")
立法機關為[國會](../Page/瑙鲁议会.md "wikilink")，採用[一院制](../Page/一院制.md "wikilink")，諾魯全國共有14個行政區，選舉時全國分8選區，選出19名議員，一任3年。

本屆國會成員（2016-2019）17位為政府派，2位為反對派。

諾魯並沒有一個正式結構的政黨，候選人通常是代表個人或公司，2005年當時國會中15人均是獨立身份，他們在政府的联盟內，是形成大家庭关系的基础\[20\]。

1992年，政府負責諾魯島嶼議會（）。議會的權力很少，其功用就像顧問向國家政府提供地方事務的意見，其角色是專注在與諾魯人相關的活動。民選的議會議員不能同時兼任國會議員。島嶼議會在1999年自行解散，其財產及職責轉歸國家政府\[21\]。諾魯的土地所有權比較特別，國民對由個人或團體所擁有的土地，擁有某些權利。政府或公司並沒有土地，他們必須與地主達成租借協議才能使用。非諾魯國民不能擁有土地\[22\]。

1999至2003年間，一系列的不信任票及選舉在此兩人出現：勒内·哈里斯和[伯纳德·多维约戈](../Page/伯纳德·多维约戈.md "wikilink")，使國家進入交替時期。多维约戈於2003年任內逝世，[路德维格·斯考蒂當選為總統](../Page/路德维格·斯考蒂.md "wikilink")，他於2004年10月重新參選，以取得完整的3年任期。2007年國會對他投下不信任票，總統一職由[馬庫斯·史蒂芬接替](../Page/馬庫斯·史蒂芬.md "wikilink")。

### 司法

諾魯的法律系統十分複雜，最高法院由首席法官統領，主要處理憲法問題；而其它案件能夠上訴至上訴法庭。國會不能推翻法院的裁決，但上诉法院的裁决能夠上訴至[澳洲高等法院](../Page/澳洲高等法院.md "wikilink")，這種情況很少出現。下级法院由地方法院和家事法院組成，由居民县长統轄，他亦是最高法院的司法常务官。此外亦有2個准法院：公共服务上诉委员会和警察上诉委員會，均由终审法院首席法官主持\[23\]。

諾魯沒有自己的軍隊，只有民間的警察部隊，其國防於非正式的協議之下由澳洲負責\[24\]。

## 外交

1968年獨立後，諾魯以特別會員身份加入[英联邦](../Page/英联邦.md "wikilink")，並於2000年成為正式會員\[25\]。此外，瑙魯政府亦在1991年加入[亞洲開發銀行](../Page/亞洲開發銀行.md "wikilink")，於1999年加入[聯合國](../Page/联合国.md "wikilink")，以及於2016年加入[国际货币基金组织和](../Page/國際貨幣基金組織.md "wikilink")[世界银行](../Page/世界银行.md "wikilink")\[26\]。瑙魯是[太平洋島國論壇](../Page/太平洋島國論壇.md "wikilink")、[南太平洋地區環境計劃](../Page/南太平洋地區環境計劃.md "wikilink")、[南太平洋委員會和](../Page/南太平洋委員會.md "wikilink")[南太平洋應用地球科學委員會的成員](../Page/南太平洋應用地球科學委員會.md "wikilink")。[美國的](../Page/美國.md "wikilink")[大氣軸射測量計劃在瑙魯設有一台](../Page/大氣軸射測量計劃.md "wikilink")[氣候監察儀](../Page/氣候監察儀.md "wikilink")。

瑙魯與[澳大利亞外交關係密切](../Page/澳大利亚.md "wikilink")，並以[澳元作為](../Page/澳大利亚元.md "wikilink")[官方貨幣](../Page/貨幣.md "wikilink")。除了正规的國防安排外，在2005年9月更簽訂[谅解备忘录](../Page/諒解備忘錄.md "wikilink")，澳大利亞向瑙魯提供財政及技術支援，包括指派一名財政部長，為瑙魯準備財政預算、以及作為諾魯卫生和教育事務之顾问，而瑙魯政府以建立一個處理非法進入澳大利亞者的拘留所作為回報。

瑙魯不斷地透過承認某些[国家的](../Page/国家.md "wikilink")[主權以獲得](../Page/主權.md "wikilink")[资金上的援助](../Page/资金.md "wikilink")。1980年8月，瑙魯与[阿尔及利亚建交后](../Page/阿尔及利亚.md "wikilink")，又宣布承认[西撒国](../Page/西撒国.md "wikilink")，阿尔及利亚此后每年给予瑙魯1000万美元经濟援助，直至其2000年撤销承认西撒国為止。1980年5月4日，瑙魯與[中華民國建交](../Page/中華民國.md "wikilink")；但是，2002年瑙魯與[中華人民共和國簽署](../Page/中華人民共和國政府.md "wikilink")[協議](../Page/諒解備忘錄.md "wikilink")，宣佈在当年7月21日建立[外交关系](../Page/外交.md "wikilink")，同時獲得1.3億美元捐助\[27\]，兩天後[中華民國宣佈與諾魯斷交](../Page/中華民國.md "wikilink")\[28\]。2005年5月14日，瑙魯與[中華民國恢復外交關係](../Page/中華民國.md "wikilink")，與中華人民共和國斷交。2008年，瑙魯在持续得到[欧盟](../Page/欧洲联盟.md "wikilink")、[美国](../Page/美国.md "wikilink")、澳大利亚和[新西兰的财政支持后承認](../Page/新西兰.md "wikilink")[科索沃獨立](../Page/科索沃.md "wikilink")。2009年，瑙魯承認[阿布哈茲和](../Page/阿布哈茲.md "wikilink")[南奥塞梯](../Page/南奥塞梯.md "wikilink")，是承認此地區主權的第四個國家，而[俄羅斯以](../Page/俄罗斯.md "wikilink")5000萬美元資金作為[人道主義援助之回報](../Page/人道主义.md "wikilink")\[29\]。同年7月15日，瑙魯宣布翻新[港口](../Page/港口.md "wikilink")，並於2011年初完成；此筆900萬美元資金據稱來自俄羅斯，但瑙魯官方聲稱此援助與[南奧塞梯戰爭無關](../Page/南奧塞梯戰爭.md "wikilink")\[30\]。

## 行政區劃

[Nauru-districts-zh-tw.svg](https://zh.wikipedia.org/wiki/File:Nauru-districts-zh-tw.svg "fig:Nauru-districts-zh-tw.svg")
 在1989年至2003年間，諾魯的行政區劃共有17次變更\[31\]，今諾魯行政區共分為14個區\[32\]\[33\]\[34\]：

## 地理

[Nauru_satellite.jpg](https://zh.wikipedia.org/wiki/File:Nauru_satellite.jpg "fig:Nauru_satellite.jpg")

諾魯位在西太平洋赤道以南約42公里，面積只有21平方公里，為一個[橢圓形的](../Page/椭圆.md "wikilink")[珊瑚島](../Page/珊瑚島.md "wikilink")，[海岸陡峭](../Page/海岸.md "wikilink")。沿岸有寬150-300[米](../Page/米_\(单位\).md "wikilink")、海拔30米的[海岸帶](../Page/海岸帶.md "wikilink")，是全國唯一的[農業區](../Page/农业.md "wikilink")。中部為[台地](../Page/台地.md "wikilink")，最高點為[海拔](../Page/海拔.md "wikilink")71米（[司令嶺](../Page/司令嶺.md "wikilink")）\[35\]。全島60%被[磷酸鹽所覆蓋](../Page/磷酸鹽.md "wikilink")，無[河流](../Page/河流.md "wikilink")，西南部僅有一個[鹹水湖](../Page/鹹水湖.md "wikilink")（[布阿達湖](../Page/布阿達湖.md "wikilink")）。

諾魯曾是太平洋上三大磷礦石的生產地（另外是在吉里巴斯和[法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink")），但島上所有礦產均已耗盡，使中部台地留下贫瘠地形，例如高達15米的鋸齒狀石灰岩尖頂。近一個世紀的採礦不但破壞了約80%土地，亦使[專屬經濟區受到影響](../Page/专属经济区.md "wikilink")，約40%海洋生物因泥沙和磷礦的径流而死亡\[36\]。

諾魯缺乏天然淡水資源，雖然屋顶的雨水箱能收集雨水，但居民大多依靠在諾魯公共事業中心的3個[海水淡化廠](../Page/海水淡化.md "wikilink")。諾魯由於遍地皆磷，因此井水鹽分過高，不能飲用或灌溉。

諾魯的原生[維管植物約](../Page/维管植物.md "wikilink")60種，無[特有種](../Page/特有種.md "wikilink")，原生植被受人工种植的[椰子農作物](../Page/椰.md "wikilink")、採礦和其他外來植物破壞，造成嚴重影響\[37\]。諾魯有各種鳥類（包括罕見的[瑙鲁苇莺](../Page/瑙鲁苇莺.md "wikilink")）、昆蟲和[螃蟹](../Page/螃蟹.md "wikilink")，而[哺乳动物都是](../Page/哺乳动物.md "wikilink")[外來物種](../Page/外來物種.md "wikilink")，如[玻里尼西亞鼠](../Page/玻里尼西亞鼠.md "wikilink")、貓、狗、豬、雞等動物均是從外地進口的。

### 天氣與氣候

諾魯接近赤道和海洋，因此全年濕熱，屬[熱帶雨林氣候](../Page/熱帶雨林氣候.md "wikilink")，在每年11月至2月期間均受到[季风雨影響](../Page/季风.md "wikilink")。受到[聖嬰-南方振盪現象影響](../Page/聖嬰-南方振盪現象.md "wikilink")，年降雨量的變化甚大，甚至錄得幾次重要的乾旱\[38\]。由於是島國的關係，因此較易受到[氣候變化和](../Page/氣候變化.md "wikilink")[海平面上升影響](../Page/海平面上升.md "wikilink")，但變化卻難以預測。諾魯的白天氣溫介乎在
攝氏26度（華氏79度）至35度（華氏95度）之間；晚上氣溫介乎攝氏22度（華氏72度）至34度（華氏93度）之間\[39\]。

## 基礎建設

### 交通

#### 航空

[View_of_Nauru_airport.jpg](https://zh.wikipedia.org/wiki/File:View_of_Nauru_airport.jpg "fig:View_of_Nauru_airport.jpg")
[諾魯國際機場是諾魯唯一的](../Page/諾魯國際機場.md "wikilink")[機場](../Page/機場.md "wikilink")，位於其西南方[亞倫區的海岸上](../Page/亞倫區.md "wikilink")，擁有一條雙向跑道長\[40\]，目前僅有[諾魯航空每週兩班航班起降](../Page/諾魯航空.md "wikilink")\[41\]。在2006年1月至9月期間，由於服務島上的唯一飛機被[澳大利亚的](../Page/澳大利亚.md "wikilink")[墨爾本](../Page/墨尔本.md "wikilink")[法庭扣押](../Page/法庭.md "wikilink")，使其對外航空中斷；在得到[台湾援助后](../Page/台湾.md "wikilink")，諾魯航空於同年10月恢復營運，並易名為奧爾航空，2014年再改回諾魯航空。

#### 陸運

全國道路網絡總長，環島沿海公路長，其餘伸入中部台地\[42\]。環島[巴士是路上的主要公共交通](../Page/公共汽車.md "wikilink")，途經[愛和區](../Page/愛和區.md "wikilink")、諾魯國際機場、湄濘酒店、當地醫院和學校，另有出租汽車提供載客服務\[43\]。有窄軌鐵路，連接磷礦礦區與加工工廠\[44\]。

#### 海運

每月有一船班（不確定是否已停駛）往返於澳大利亞的[布里斯本](../Page/布里斯班.md "wikilink")\[45\]。

### 通訊

2009年以前，[電話網絡是由諾魯政府架設與提供服務](../Page/电话.md "wikilink")，但由於財政困難，2003年底起，政府便再不能負擔維修費用，2004年更關閉部份非收費[衛星](../Page/通訊衛星.md "wikilink")[通訊服務](../Page/通信.md "wikilink")，通訊崩潰問題一直至2009年獲得解決，該年年中由公司取得許可執照，在島上架設無線通訊網絡，系統，先後於[2009年9月](../Page/2009年9月.md "wikilink")、[2010年10月開始提供服務](../Page/2010年10月.md "wikilink")\[46\]\[47\]。

### 能源

諾魯早期是使用[柴油](../Page/柴油.md "wikilink")[發電機提供採礦所需要的電力](../Page/發電機.md "wikilink")，後來諾魯磷酸鹽公司安裝了一座能提供15[兆瓦](../Page/瓦特.md "wikilink")（2006年效能減至11.1兆瓦）電力的發電設施，其中約43%電力為採礦使用，其餘供應民生所需，島上另備有獨立發電機組供必要時使用\[48\]。

諾魯的[石油燃料與](../Page/石油.md "wikilink")[產品是由諾魯磷酸鹽公司進口](../Page/石油产品.md "wikilink")，供應包括[汽油](../Page/汽油.md "wikilink")、[柴油](../Page/柴油.md "wikilink")、[液化石油氣](../Page/液化石油气.md "wikilink")、[煤油](../Page/煤油.md "wikilink")、[潤滑劑和](../Page/潤滑劑.md "wikilink")[溶劑等](../Page/溶剂.md "wikilink")，另有小型企業供應數量不大的潤滑劑、溶劑與液化石油氣\[49\]。

## 經濟

諾魯在[第三世界中屬於較富裕的國家](../Page/第三世界.md "wikilink")，國家經濟以磷酸鹽開採和國外房地產為主要支柱，在80年代初達到顛峰。[磷酸鹽礦分佈佔全島面積的](../Page/磷酸鹽.md "wikilink")70%以上，是世界主要磷酸鹽生產和出口國之一，是國家主要的收入來源，磷礦乃繼承自億萬年所累積的海中有機物和鳥糞。採礦公司的礦場和工廠都是聘請外國人操作，大部份是[基里巴斯和中國人](../Page/基里巴斯.md "wikilink")。磷礦主要銷往[澳大利亞](../Page/澳大利亚.md "wikilink")、[英國](../Page/英国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[新西蘭等地](../Page/新西兰.md "wikilink")；以往磷酸鹽外銷所得曾佔政府收入的半數，然而在長期密集開採之下，該項礦藏已面臨耗竭。

除了礦業外，地上還有許多[椰子](../Page/椰.md "wikilink")、[甘蔗](../Page/甘蔗.md "wikilink")、[香蕉和蔬菜等農作物](../Page/香蕉.md "wikilink")，一般農產品和魚类勉強可以自給自足，但是主要的糧食、淡水和日用品都要依賴進口，其中-{米}-和麵粉多由英國、澳大利亚和新西蘭輸入，肉類由澳大利亞供應，日用品多來自台灣、日本和[香港](../Page/香港.md "wikilink")\[50\]。

諾魯居民並不需要交稅，甚至免除進口關稅，國家全部醫療都是由政府負擔，住屋也是由政府修建和保養，如果政府在人民的土地上蓋房子，還需要向地主支付租金。

自90年代起，諾魯成為了一個[避稅天堂](../Page/避稅天堂.md "wikilink")，外國人能向政府購買諾魯護照\[51\]。[國際洗錢防制指出諾魯是在他們黑名單中](../Page/國際洗錢防制.md "wikilink")15個國家的一個。而且，只需要25,000美元便能在諾魯成立一家持牌銀行。在國際洗錢防制的壓力之下，以及随着[熱錢的流出](../Page/熱錢.md "wikilink")，瑙鲁終於在2003年立了反避税法。2005年10月，由於立法後的成效显著，國際洗錢防制把諾魯從黑名單中除名\[52\]。

## 人口

[Nauru_Denigomodu-Nibok.jpg](https://zh.wikipedia.org/wiki/File:Nauru_Denigomodu-Nibok.jpg "fig:Nauru_Denigomodu-Nibok.jpg")

2006年諾魯居民為9,265人\[53\]。以往的諾魯人口數比現今更多，但在2006年約1500名移民工人被遣返至吉里巴斯和[吐瓦魯](../Page/圖瓦盧.md "wikilink")，原因是大規模減少的磷礦開採工業\[54\]造成了大批工人失业。[諾魯語是諾魯的官方語言](../Page/瑙鲁语.md "wikilink")，是一個特別的太平洋島國語言，約96%諾魯人在家中使用\[55\]。由於諾魯語只應用在國內，所以英語亦廣泛地使用，尤其是在政府和商業上。諾魯的[族群包括](../Page/族群.md "wikilink")[諾魯人](../Page/諾魯人.md "wikilink")（58%）、其他太平洋島國人（26%）、歐洲人（8%）及華人（8%）。諾魯本土的歐洲人原本全由英國人組成，但是他們大多在獨立後離開了。諾魯人的主要宗教信仰是[基督教](../Page/基督教.md "wikilink")（2/3是[新教](../Page/新教.md "wikilink")，1/3是[天主教](../Page/天主教.md "wikilink")），另有約10%的人是[巴哈伊信仰](../Page/巴哈伊信仰.md "wikilink")，所佔比例是世界上最高的\[56\]，另有9%信奉[佛教](../Page/佛教.md "wikilink")，2.2%是[穆斯林](../Page/伊斯兰教.md "wikilink")。憲法明确寫明人民享有[宗教自由](../Page/宗教自由.md "wikilink")，但在某些情況下政府會限制此權利，如限制[耶穌基督後期聖徒教會活動和](../Page/耶穌基督後期聖徒教會.md "wikilink")[耶和華見證人會員](../Page/耶和華見證人.md "wikilink")，他們主要都是外勞，受僱於政府的諾魯磷酸鹽公司\[57\]。

諾魯[識字率是](../Page/識字率.md "wikilink")96%，為6至15歲兒童提供義務強制教育，以及2年自願性教育（第11及12年）\[58\]。[南太平洋大學在諾魯設有分校](../Page/南太平洋大學.md "wikilink")，在此校建成之前，學生須赴澳大利亚和新西兰等地就讀大學。

諾魯人是世界上最[肥胖的人](../Page/肥胖症.md "wikilink")\[59\]，逾90%的居民[身高體重指數高出世界平均](../Page/身高體重指數.md "wikilink")\[60\]，約97%男性和93%女性過重\[61\]。諾魯人亦是世上患上[2型糖尿病的比例最高的人群之一](../Page/2型糖尿病.md "wikilink")，約40%的人受影響，而[美属萨摩亚有](../Page/美屬薩摩亞.md "wikilink")47%，[托克勞有](../Page/托克劳.md "wikilink")44%\[62\]。其它相關的重要疾病还有[心臟病和](../Page/心脏病.md "wikilink")[腎病等](../Page/腎功能衰竭.md "wikilink")。2009年諾魯男性的平均寿命為60.6歲，女性為68歲\[63\]。

## 文化

[Linkbelt1999-Finalspiel.jpg](https://zh.wikipedia.org/wiki/File:Linkbelt1999-Finalspiel.jpg "fig:Linkbelt1999-Finalspiel.jpg")\]\]
瑙鲁人是密克羅尼西亞和玻里尼西亞海員的後裔，其祖先信奉名为Eijebong的女神，并以一个名为Buitani的岛屿作为精神圣地。瑙鲁的12個原住民部落中有2個已在20世紀滅絕。每年的10月26日是返鄉日（），用以慶祝兩次世界大戰後諾魯的土著人口从不足1500人的情况下恢复。瑙鲁的土著文化受到了殖民地和当代西方文化的显著影响，旧的习俗多数被取代，只有部分传统音乐、艺术、工艺和捕魚等技艺得以保留并仍在被使用。

諾魯並沒有每日出刊的新聞，但有兩週出版一次的“”，意即“來談論吧”。諾魯電視台由國家經營，播放澳大利亚和新西兰的電視節目。另有非盈利的国有[諾魯電台](../Page/諾魯電台.md "wikilink")，播放[BBC和](../Page/英国广播公司.md "wikilink")[ABC節目](../Page/澳洲廣播電台.md "wikilink")\[64\]。

[澳式足球是諾魯最受歡迎的體育項目](../Page/澳式足球.md "wikilink")，有一個由7支隊伍組成的聯賽。諾魯共有兩個球場，所有澳式足球比賽仅在舉行。其它體育項目如排球、籃網球、舉重、网球和捕魚，均是受歡迎的體育運動。諾魯有參與[大英國協運動會和夏季奧林匹克運動會](../Page/英聯邦運動會.md "wikilink")，並在舉重項目取得成績。[馬庫斯·史蒂芬便是其中一位獎牌得主](../Page/馬庫斯·史蒂芬.md "wikilink")，在2003年進入國會，並在2007年當選為總統。

諾魯其中一個傳統活動是捕捉從海上覓食回來的[玄燕鸥](../Page/玄燕鸥.md "wikilink")。在日落時份，男士在海灘上向玄燕鸥投擲套索。諾魯的套索是柔繩，在繩的尾部負載。當鳥飛過來時，便投擲套索，被擊中的鳥便倒地，經過採摘、清洗和煮熟後便進食\[65\]。

## 參考資料

## 外部連結

  - [中華民國外交部—諾魯國情簡介](https://www.mofa.gov.tw/CountryInfo.aspx?CASN=0984A85A3A9A6677&n=4043244986E87475&sms=26470E539B6FA395&s=07ACE2E316EEE218)

  - [諾魯政府網站](http://www.naurugov.nr/)

[\*](../Category/瑙魯.md "wikilink")
[Category:大洋洲國家](../Category/大洋洲國家.md "wikilink")
[Category:大洋洲島國](../Category/大洋洲島國.md "wikilink")
[Category:微型國家](../Category/微型國家.md "wikilink")
[Category:英語國家地區](../Category/英語國家地區.md "wikilink")
[Category:前德國殖民地](../Category/前德國殖民地.md "wikilink")
[Category:1968年建立的國家或政權](../Category/1968年建立的國家或政權.md "wikilink")

1.

2.

3.

4.

5.

6.  Na Nauru znajdowały się wówczas jedne z największych złóż tych skał
    na świecie

7.
8.
9.  Nauru Department of Economic Development and Environment. 2003.
    [First National Report To the United Nations Convention to Combat
    Desertification
    (UNCCD)](http://www.unccd.int/cop/reports/asia/national/2002/nauru-eng.pdf).
    Retrieved 2006-05-03.

10.
11.

12.

13. Lundstrom, John B., *[The First Team and the Guadalcanal
    Campaign](http://books.google.com/books?id=xtaTS-POl-UC&printsec=frontcover&source=gbs_navlinks_s#v=onepage&q=&f=false)*,
    Naval Institute Press, 1994, p. 175.

14. Haden, J. D. 2000. [Nauru: a middle ground in World War
    II](http://166.122.164.43/archive/2000/April/04-03-19.htm)  *Pacific
    Magazine* URL Accessed 5 May 2006

15. ICJ Pleadings, Oral Arguments, Documents, *Case Concerning Certain
    Phosphate Lands in Nauru (Nauru v. Australia) Application: Memorial
    of Nauru* (January, 2004) ISBN 9789210709361 (United Nations,
    International Court of Justice)

16. Highet, K and Kahale, H. 1993. [Certain Phosphate Lands in
    Nauru](http://www.icj-cij.org/docket/index.php?p1=3&p2=3&code=naus&case=80&k=e2)
    . *The American Journal of International Law* 87:282–288

17.

18.

19.

20. Australian Department of Foreign Affairs and Trade. [Republic of
    Nauru Country Brief –
    November 2005](http://www.dfat.gov.au/geo/nauru/nauru_brief.html)
    URL accessed on 2 May 2006.

21. Hassell, G.
    [1](https://web.archive.org/web/20090604055625/http://www2.hawaii.edu/~ogden/piir/pacific/nauru.html)

22.
23. [State Department Bureau of East Asian and Pacific Affairs
    September 2005](http://www.state.gov/r/pa/ei/bgn/16447.htm) URL
    Accessed 11 May 2006

24.
25.

26.

27.

28.

29.

30.

31.
32.
33.

34.

35.  Republic of Nauru [National Assessment
    Report](http://www.sprep.org/att/IRC/eCOPIES/BPoA+10/NAURU.pdf)

36. Republic of Nauru. 1999. [Climate Change – Response. First National
    Communication
    – 1999](http://unfccc.int/resource/docs/natc/naunc1.pdf). Under
    the United Nations Framework Convention on Climate Change, United
    Nations

37.
38.
39. [Pacific Climate Change Science
    Program](http://www.cawcr.gov.au/projects/PCCSP/pdf/6._Nauru_GH_poster.pdf)


40.

41.

42.

43.
44.

45.

46.

47.

48.

49.
50. [Big tasks for a small
    island](http://news.bbc.co.uk/2/hi/asia-pacific/332164.stm) URL
    Accessed 10 May 2006

51.

52. FATF. 13 October 2005. [Nauru
    de-listed](http://www.fatf-gafi.org/dataoecd/13/36/35497629.pdf)
    URL Accessed 11 May 2006

53.
54.
55.
56.

57. US Department of State. 2003. [International Religious Freedom
    Report 2003 –
    Nauru](http://www.state.gov/g/drl/rls/irf/2003/24314.htm) URL
    accessed 2 May 2005.

58. Waqa, B. 1999. [UNESCO Education for all Assessment Country
    report 1999 Country:
    Nauru](http://www2.unesco.org/wef/countryreports/nauru/contents.html)
     URL Accessed 2 May 2006.

59. [Fat of the land: Nauru tops obesity
    league](http://www.independent.co.uk/life-style/health-and-families/health-news/fat-of-the-land-nauru-tops-obesity-league-2169418.html)
    Article posted on Sunday, 26 December 2010

60. [Obesity in the Pacific: too big to
    ignore](http://www.wpro.who.int/NR/rdonlyres/B924BFA6-A061-43AE-8DCA-0AE82A8F66D2/0/obesityinthepacific.pdf)
    . 2002. Secretariat of the Pacific Community ISBN 978-982-203-925-2

61.
62.
63. [World Health Organization](../Page/世界卫生组织.md "wikilink") World
    health report 2005.
    [Nauru](http://www.who.int/countries/nru/en/index.html) URL

64. BBC News. [Country Profile:
    Nauru](http://news.bbc.co.uk/1/hi/world/asia-pacific/country_profiles/1134221.stm).
    URL Accessed 2 May 2006.

65. [2](https://web.archive.org/web/20060626214942/http://www.banaban.com/natalie.htm)