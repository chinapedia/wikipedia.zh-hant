***O*** 是[Damien
Rice的首張專輯的名稱](../Page/Damien_Rice.md "wikilink")，最早發行於2002年2月1日的[愛爾蘭及](../Page/愛爾蘭共和國.md "wikilink")[英國](../Page/英國.md "wikilink")。曾進入愛爾蘭的前20名排行榜。

## 曲目

1.  "Delicate" – 5:12
2.  "Volcano" – 4:38
3.  "The Blower's Daughter" – 4:44
4.  "Cannonball" – 5:10
5.  "Older Chests" – 4:46
6.  "Amie" – 4:36
7.  "Cheers Darlin'" – 5:50
8.  "Cold Water" – 4:59
9.  "I Remember" – 5:31
10. "Eskimo" – 16:07
      - "Prague" at – 7:09（隱藏曲目）
      - "Silent Night" at – 14:09（隱藏曲目）

## 追加發行

  - 2003 - with bonus DVD
  - 2004 - with extra track "Cannonball (Remix)"
  - 2004 - double album pack: "O" and "B-Sides"
  - 2005 - with extra tracks "Cannonball (Remix)" and "Unplayed Piano"

## 專輯演出人員

  - [Damien Rice](../Page/Damien_Rice.md "wikilink") -
    主唱，低音鍵盤，豎笛，吉他，打擊樂器，鋼琴，爵士鼓，合聲，專輯製作
  - [Lisa Hannigan](../Page/Lisa_Hannigan.md "wikilink") - 背景合聲
  - [Shane Fitzsimons](../Page/Shane_Fitzsimons.md "wikilink") - 低音鍵盤
  - [Tom Osander](../Page/Tom_Osander.md "wikilink") aka Tomo - 打擊樂器，爵士鼓
  - [Vyvienne Long](../Page/Vyvienne_Long.md "wikilink") - 大提琴
  - Nicholas Dodd - 指揮
  - Mark Kelly - 電吉他
  - Caz - 非洲皮鼓
  - Jean Meunier - 即興演奏，鋼琴
  - Colm Mac Con Iomaire - 小提琴
  - Doreen Curran - 女中音

[Category:2003年專輯](../Category/2003年專輯.md "wikilink") [Category:Damien
Rice albums](../Category/Damien_Rice_albums.md "wikilink")
[Category:Albums with hidden
tracks](../Category/Albums_with_hidden_tracks.md "wikilink")