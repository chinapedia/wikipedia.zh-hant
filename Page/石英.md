**石英**（）是[大陆地壳数量第二多的矿石](../Page/大陆地壳.md "wikilink")，仅次于[长石](../Page/长石.md "wikilink")\[1\]，其[晶体结构是SiO](../Page/晶体结构.md "wikilink")<sub>4</sub>[硅](../Page/硅.md "wikilink")-[氧](../Page/氧.md "wikilink")[四面体的连续框架](../Page/四面体形分子构型.md "wikilink")，其中每个氧在两个四面体之间共享，得到SiO<sub>2</sub>的总[化学式](../Page/化学式.md "wikilink")，石英的種類有很多，无色全透明的石英称为**水晶**。有一些被做為半[寶石使用](../Page/寶石.md "wikilink")，自古以来石英被广泛用作制作[珠宝和硬石雕刻](../Page/珠宝.md "wikilink")，尤其在欧洲和中东地区。纯淨的石英能够让一定波长范围的[紫外线](../Page/紫外线.md "wikilink")、可见光和[红外线通过](../Page/红外线.md "wikilink")，具有[旋光性](../Page/旋光性.md "wikilink")、[压电效应和电致伸缩等性质](../Page/压电效应.md "wikilink")。石英的完整晶体产于岩石晶洞中，块状的产于热液脉矿中，粒状的则是[花岗岩](../Page/花岗岩.md "wikilink")、[片麻岩和](../Page/片麻岩.md "wikilink")[砂岩等各种岩石的重要组成部分](../Page/砂岩.md "wikilink")，石英晶体也可用人工方法生长。

## 種類（依微結構）

虽然许多品种名称历史上源自矿物的颜色，目前的科学命名方案主要是指矿物的微观结构。 颜色是隐晶性矿物的次要标识符，虽然它是宏观品种的主要标识符。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>主要的石英类型</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>类型</p></td>
</tr>
<tr class="even">
<td><p>水晶</p></td>
</tr>
<tr class="odd">
<td><p>紫水晶</p></td>
</tr>
<tr class="even">
<td><p>黄水晶</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/玉髓.md" title="wikilink">玉髓</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紅玉隨.md" title="wikilink">紅玉隨</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瑪瑙.md" title="wikilink">瑪瑙</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/縞瑪瑙.md" title="wikilink">縞瑪瑙</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/碧玉.md" title="wikilink">碧玉</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/砂金石.md" title="wikilink">砂金石</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虎眼石.md" title="wikilink">虎眼石</a></p></td>
</tr>
<tr class="even">
<td><p>鈦晶</p></td>
</tr>
<tr class="odd">
<td><p>含藍線石水晶</p></td>
</tr>
<tr class="even">
<td><p>煙水晶</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 種類（依顏色）

[Transparency.jpg](https://zh.wikipedia.org/wiki/File:Transparency.jpg "fig:Transparency.jpg")度\]\]

纯石英传统上被称为岩石水晶或透明石英，是无色的[透明或半透明的](../Page/透明.md "wikilink")，并且经常被用于，如[Lothair水晶](../Page/Lothair水晶.md "wikilink")。石英含有杂质时颜色不一，无色透明的晶体称[水晶](../Page/水晶.md "wikilink")，乳白色的称[乳石英](../Page/乳石英.md "wikilink")，浅红色的称[蔷薇石英](../Page/蔷薇石英.md "wikilink")，紫色的称[紫水晶](../Page/紫水晶.md "wikilink")，黄褐色的称烟晶、茶晶，黑色的则称墨晶。

石英类型之间最重要的区别是大粒晶体（肉眼可见的单个晶体）和[微晶或](../Page/微晶.md "wikilink")品种（仅在高放大率下可见的晶体聚集体）。隐晶变体是半透明的或大部分不透明的，而透明变体倾向于是大粒晶体的。[玉髓是二氧化硅的隐晶形式](../Page/玉髓.md "wikilink")，由石英和其[单斜晶多晶型](../Page/单斜晶系.md "wikilink")的细小共生体组成\[2\]。石英的其他不透明宝石品种或包括石英的混合岩石，通常包括对比带或颜色图案，是[玛瑙](../Page/玛瑙.md "wikilink")，，[缟玛瑙](../Page/缟玛瑙.md "wikilink")，和[碧玉](../Page/碧玉.md "wikilink")。

[Amethyse_de_Guerrero.jpg](https://zh.wikipedia.org/wiki/File:Amethyse_de_Guerrero.jpg "fig:Amethyse_de_Guerrero.jpg")

### 紫水晶

紫水晶（Amethyst）為石英中常見的種類，顏色從淺到深的紫色或是黯淡紫。主要產地有巴西、墨西哥、烏拉圭、俄羅斯、法國、納米比亞以及摩洛哥。有時候可以見到紫水晶和黃水晶生長在同一晶體中，此種晶體被稱為紫黃晶（Ametrine），通常紫水晶常形成於含鐵較高地方。

[Citrine-sample2.jpg](https://zh.wikipedia.org/wiki/File:Citrine-sample2.jpg "fig:Citrine-sample2.jpg")

### 黃水晶

黃水晶（Citrine），又稱黃晶，由於雜質含鐵的關係，顏色從淡黃至褐色。自然的黃水晶非常稀少，大部分市面上常見的黃水晶多為熱處理過的紫水晶或是煙水晶。\[3\]

[Quartz-137772.jpg](https://zh.wikipedia.org/wiki/File:Quartz-137772.jpg "fig:Quartz-137772.jpg")

### 粉晶

粉晶（Rose quartz），顏色從淡粉紅色到玫瑰紅。會造成此種顏色主要為鋁和磷取代部分矽的位置。\[4\]

[Quartz-168661.jpg](https://zh.wikipedia.org/wiki/File:Quartz-168661.jpg "fig:Quartz-168661.jpg")

### 煙水晶

煙水晶（Smoky quartz），又稱煙晶和茶水晶，顏色從半透明灰色一直到全透明的灰褐色都有，有的甚至為黑色，不透明黑色者中文又稱墨晶。
它的颜色是由于含有极微量放射性元素（镭）所引起的。

### 乳石英

乳石英（Milky
quartz），從透明到不透明即顏色呈乳白色的石英。其颜色是由于含细小分散的气态或液态包裹体所致。显著的乳白色或奶油色，得自其充满气体和液体气泡的内含物。产状为六方体带金字塔形的末端棱柱形。通常呈块状产出，见于各种石英脉中和石英岩中。

### 白水晶

白水晶（Rock crystal, Clear quartz），主要為沒有顏色結晶體的石英，廣泛出產於中國，巴西等地。

## 用途

  - 石英粉可以做為有效的金屬拋光劑，常應用於[研磨機](../Page/研磨機.md "wikilink")、[噴砂機中](../Page/噴砂機.md "wikilink")。
  - 半導體產業重要的原料之一，作為矽[晶圓的原料](../Page/晶圓.md "wikilink")。將石英與焦炭混合後，在[電弧爐加熱下即生成粗矽](../Page/電弧爐.md "wikilink")；鹽酸氯化，並經蒸餾後，製成了高純度的[多晶硅](../Page/多晶硅.md "wikilink")，其純度高達99.9999%。
  - 石英石中的辐射量也是测定地质年份的方法之一。
  - 石英粉結合[樹脂可以打造的](../Page/樹脂.md "wikilink")[人造石](../Page/人造石.md "wikilink")，是目前市面上商業廚具檯面的主流。
  - [玻璃及生产](../Page/玻璃.md "wikilink")[耐火砖](../Page/耐火砖.md "wikilink")（硅砖）的製作材料，通常使用石英含量高的岩石（如：石英砂岩等），該種岩石又稱為「硅石」，其二氧化矽含量大于95%，在中國国家标准有96、97、98、98.5四个牌号。

## 压电效应性质

石英晶体具有[压电性能](../Page/压电效应.md "wikilink");
它们在施加机械[应力时产生](../Page/应力.md "wikilink")[电势](../Page/电势.md "wikilink")。早期使用这种石英晶体的性质是在[留声机拾音器](../Page/留声机.md "wikilink")。当今石英最常见的压电应用之一是。[石英钟是使用这种矿物的熟悉的设备](../Page/石英钟.md "wikilink")。石英晶体振荡器的谐振频率通过机械加载而改变，并且该原理用于非常精确地测量和中的非常小的质量变化。

## 产区

[Quartz_gisements.jpg](https://zh.wikipedia.org/wiki/File:Quartz_gisements.jpg "fig:Quartz_gisements.jpg")

## 另見

  - [衝擊石英](../Page/衝擊石英.md "wikilink")
  - [石英晶体谐振器](../Page/石英晶体谐振器.md "wikilink")

## 参考文献

## 外部链接

  - [石英 - Mindat.org](http://www.mindat.org/min-3337.html)
  - [石英 - Webmineral](http://www.webmineral.com/data/Quartz.shtml)

[Category:矿物](../Category/矿物.md "wikilink")
[Category:矽酸鹽礦物](../Category/矽酸鹽礦物.md "wikilink")
[Category:压电材料](../Category/压电材料.md "wikilink")
[Category:电介质](../Category/电介质.md "wikilink")

1.
2.
3.  [Citrine](http://www.mindat.org/min-1054.html). Mindat.org
    (2013-03-01). Retrieved 2015-12-07.
4.  [Rose Quartz](http://www.mindat.org/min-3456.html). Mindat.org
    (2013-03-01). Retrieved 2015-12-07.