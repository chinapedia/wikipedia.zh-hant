[Map-Taiwan-Caoling-Historic-Trail.png](https://zh.wikipedia.org/wiki/File:Map-Taiwan-Caoling-Historic-Trail.png "fig:Map-Taiwan-Caoling-Historic-Trail.png")
[草嶺古道埡口與虎字碑背側.jpg](https://zh.wikipedia.org/wiki/File:草嶺古道埡口與虎字碑背側.jpg "fig:草嶺古道埡口與虎字碑背側.jpg")
**草嶺古道**，是一條連接[台湾](../Page/台湾.md "wikilink")[新北市](../Page/新北市.md "wikilink")[貢寮區遠望坑與](../Page/貢寮區.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")[頭城鎮大里山區的步道](../Page/頭城鎮.md "wikilink")。屬古代[淡蘭古道北路一部分](../Page/淡蘭古道.md "wikilink")，亦為目前僅存的路段之一。

「草嶺」之名得自海拔較高處道旁山嶺[芒草生長茂盛](../Page/芒草.md "wikilink")，幾乎不存其餘樹種。古道長約8.5[公里](../Page/公里.md "wikilink")，途中有[雄鎮蠻煙碑](../Page/雄鎮蠻煙碑.md "wikilink")、[虎字碑等古蹟](../Page/虎字碑_\(貢寮\).md "wikilink")，於埡口及宜蘭縣部分可遠望[龜山島](../Page/龜山島.md "wikilink")。在失去運輸用途後，古道近年來已成為熱門景點，目前由[交通部](../Page/交通部.md "wikilink")[觀光局](../Page/觀光局.md "wikilink")[東北角暨宜蘭海岸國家風景區負責經營管理](../Page/東北角暨宜蘭海岸國家風景區.md "wikilink")。

## 歷史

草嶺古道據聞古代由[台灣原住民](../Page/台灣原住民.md "wikilink")[平埔族逐步闢建](../Page/平埔族.md "wikilink")，方便連絡[雪山尾稜兩側的](../Page/雪山.md "wikilink")[交通](../Page/交通.md "wikilink")。

### 清代以前

據《[台灣省通志](../Page/台灣省通志.md "wikilink")》記載，一位[台灣原住民](../Page/台灣原住民.md "wikilink")[平埔族闢建了](../Page/平埔族.md "wikilink")[臺北與](../Page/臺北市.md "wikilink")[宜蘭間的山路](../Page/宜蘭縣.md "wikilink")，後來的人也都循著這條路線，出入往來兩地，這就是最早見諸文獻上的淡蘭古道。淡即[淡水廳](../Page/淡水廳.md "wikilink")，以[艋舺](../Page/艋舺.md "wikilink")（今台北市萬華區）為其起點。蘭則為[噶瑪蘭廳](../Page/噶瑪蘭廳.md "wikilink")（今宜蘭縣），貫通山區的這條道路就叫做「[淡蘭古道](../Page/淡蘭古道.md "wikilink")」。由艋舺前往噶瑪蘭，必須經過[三貂嶺與草嶺兩座山脈](../Page/三貂嶺.md "wikilink")，因此，淡蘭古道又可分為「[三貂古道](../Page/三貂古道.md "wikilink")」與「**草嶺古道**」。

又有一說淡蘭古道是由[嘉慶12年](../Page/嘉慶12年.md "wikilink")（1807年），台灣知府[楊廷理所開](../Page/楊廷理.md "wikilink")。

### 清代

[清朝](../Page/清朝.md "wikilink")[同治年間](../Page/同治.md "wikilink")，於[同治5年調任](../Page/同治5年.md "wikilink")[台灣的台灣鎮總兵](../Page/台灣.md "wikilink")[劉明燈](../Page/劉明燈.md "wikilink")，就任一年後，在他走過的淡蘭古道上，先後留下了[金字碑](../Page/金字碑.md "wikilink")、[雄鎮蠻煙碑以及](../Page/雄鎮蠻煙碑.md "wikilink")[虎字碑](../Page/虎字碑_\(貢寮\).md "wikilink")，即使[劉明燈對當時的台灣貢獻與影響不多](../Page/劉明燈.md "wikilink")，但也因這些[古蹟而被](../Page/古蹟.md "wikilink")[歷史所記載](../Page/歷史.md "wikilink")。

1881年左右，淡蘭古道是唯一由清政府認定的官道，沿途設有隘寮（募集壯丁保護行旅安全，類似現今[保鏢](../Page/保鏢.md "wikilink")）、遞舖（傳遞信件貨物的轉接站）、驛站，十分方便與安全。因此台灣先民到宜蘭開墾時，大多都是經過淡蘭古道；而官方巡視與交通頻繁，亦造就淡蘭古道上許多[古蹟](../Page/古蹟.md "wikilink")。

### 近代

[日本](../Page/日本.md "wikilink")[文化人類學家](../Page/文化人類學.md "wikilink")[伊能嘉矩在](../Page/伊能嘉矩.md "wikilink")1897年10月8日看到虎字碑，寫出以下的文字：「接近嶺頂時，新路與舊道會合了。這裡有一塊大石，約六尺長，石面平滑，刻有草書體的虎字，筆勢雄渾，右側刻有同治六年冬，左側也刻有臺鎮使者劉明燈書等幾個字。這是由台北進入宜蘭的越嶺道在修復的時候，所建立的歷史紀念物。附近沒多遠的地方就是嶺頂，也就是[基隆郡與](../Page/基隆郡.md "wikilink")[宜蘭郡的分界線](../Page/宜蘭郡.md "wikilink")。」\[1\]

[宜蘭線鐵路](../Page/宜蘭線.md "wikilink")1917年開始興建，1919年3月24日，位於[平原的](../Page/蘭陽平原.md "wikilink")[宜蘭](../Page/宜蘭車站.md "wikilink")＝[蘇澳間先行通車](../Page/蘇澳車站.md "wikilink")。同年5月5日，[八堵](../Page/八堵車站.md "wikilink")＝[瑞芳間亦告峻工](../Page/瑞芳車站.md "wikilink")，但由於[草嶺隧道工事尚在進行](../Page/舊草嶺隧道.md "wikilink")，此段尚不能直通宜蘭線，暫稱「[瑞芳線](../Page/瑞芳線.md "wikilink")」。

宜蘭線全部通車前，草嶺古道仍為兩地間的交通要道。旅客需至鄰近車站下車後，更換[草鞋徒步過山](../Page/草鞋.md "wikilink")。直至1924年隧道完工，列車得以直通運行才漸趨沒落。

草嶺周邊古道經歷約130年的演變，功能早已式微，今日僅存「[三貂嶺古道](../Page/三貂嶺古道.md "wikilink")」、「草嶺古道」與「[隆嶺古道](../Page/隆嶺古道.md "wikilink")」三段，其餘路段已被[鐵路](../Page/鐵路.md "wikilink")、公路所取代。

## 景點

古道途中設有涼亭、解說牌及公共[廁所](../Page/廁所.md "wikilink")，於古道大里端有一處遊客服務中心，提供古道遊客休息與服務。若中途不休息，步道（遠望坑→大里車站）依腳力約2\~3小時可完成。

### 古道

[Xiongzhenmanyan_stele.jpg](https://zh.wikipedia.org/wiki/File:Xiongzhenmanyan_stele.jpg "fig:Xiongzhenmanyan_stele.jpg")
[Hu_stele.JPG](https://zh.wikipedia.org/wiki/File:Hu_stele.JPG "fig:Hu_stele.JPG")

  - 遠望坑親水公園：位於遠望坑口進入草嶺古道約1.1公里處的左側小型河谷平原，原本為農地，現已改建成親水公園。溪流兩旁建有木製步道、橋樑及涼亭，遊客可在漫步其間，欣賞大自然風光，亦可觀賞溪裏成群的臺灣[溪哥魚和](../Page/溪哥.md "wikilink")[苦花魚](../Page/苦花魚.md "wikilink")\[2\]。

<!-- end list -->

  - 跌死馬橋：以前為木製，下過雨後木板濕滑，曾有馬經過時跌落橋下，故此得名。

<!-- end list -->

  - [雄鎮蠻煙碑](../Page/雄鎮蠻煙碑.md "wikilink")（[三級古蹟](../Page/三級古蹟.md "wikilink")）：[清](../Page/清朝.md "wikilink")[同治6年](../Page/同治6年.md "wikilink")（1867年），台灣鎮總兵劉明燈為了鎮壓山魔所題。此碑位於草嶺古道半山腰，寬336[公分](../Page/公分.md "wikilink")、高119公分，每字寬55公分、高70公分。後人有[詩讚美雄鎮蠻煙碑](../Page/詩.md "wikilink")：「『雄』危聳旁其卯星，『鎮』迫龍更抱心胸，『蠻』氣霏霏龍吸雨，『煙』兌亨亨虎吞風。」

<!-- end list -->

  - [虎字碑](../Page/虎字碑.md "wikilink")（[三級古蹟](../Page/三級古蹟.md "wikilink")）：位於離古道最高點約130公尺處，海拔約330公尺。[同治6年](../Page/同治6年.md "wikilink")（1867年），[劉明燈路經之處被風暴所阻](../Page/劉明燈.md "wikilink")，取《[易經](../Page/易經.md "wikilink")》「雲從龍、風從虎」之義，以芒花為筆，就地揮毫。其字寬40公分、高1[公尺](../Page/公尺.md "wikilink")，為[草書字體](../Page/草書.md "wikilink")。

<!-- end list -->

  - 埡口平台及觀景亭：埡口處有一平台，為整條古道地勢最高點，向南可遠眺[太平洋](../Page/太平洋.md "wikilink")、[龜山島及宜蘭縣頭城鎮大里聚落](../Page/龜山島.md "wikilink")。古道西側山丘上建有觀景亭，視野更佳。

[魷魚公廟與草嶺古道埡口.jpg](https://zh.wikipedia.org/wiki/File:魷魚公廟與草嶺古道埡口.jpg "fig:魷魚公廟與草嶺古道埡口.jpg")與埡口\]\]

  - 護管所：原為林地護管所，興建於1970年，防護森林發生火災、盗伐、濫墾之情事。1990年，改建為旅遊解說服務站，以滿足日益增加的遊客需求\[3\]。

<!-- end list -->

  - [大里天公廟](../Page/草嶺慶雲宮.md "wikilink")：本名慶雲宮，建於1836年，主祀由[福州供奉來台的](../Page/福州.md "wikilink")[玉皇大帝](../Page/玉皇大帝.md "wikilink")。平時香火鼎盛，每年[農曆的](../Page/農曆.md "wikilink")[正月初九是當地慶典](../Page/正月初九.md "wikilink")「拜天公」之日，會有從各地來參拜的信徒。廟下方有一大石，上有[何應欽將軍親筆所題](../Page/何應欽.md "wikilink")「蘭陽第一勝景」字蹟。

<!-- end list -->

  - 大里遊客服務中心：位於大里天公廟南側，隸屬[東北角暨宜蘭海岸國家風景區管理處](../Page/東北角暨宜蘭海岸國家風景區.md "wikilink")，提供遊客旅遊諮詢服務。

### 周邊

  - 由福隆前往草嶺古道，除了沿[省道](../Page/省道.md "wikilink")[台2線](../Page/台2線.md "wikilink")、[台2丙前往遠望坑口](../Page/台2線.md "wikilink")，再由遠望坑街進入古道起點之外，亦可由福隆車站後方之虎子山街，翻越虎子山直達遠望坑。該路徑全程需步行約一小時，唯最後一段十餘分鐘路程為下坡山徑，雨天容易路滑，遊客應注意安全性。
  - 由遠望坑口沿[省道](../Page/省道.md "wikilink")[台2丙往西可通往](../Page/台2線.md "wikilink")[貢寮](../Page/貢寮區.md "wikilink")、[雙溪](../Page/雙溪區.md "wikilink")，再轉[縣道102號往北可抵](../Page/縣道102號.md "wikilink")[三貂嶺](../Page/三貂嶺.md "wikilink")、[金瓜石](../Page/金瓜石.md "wikilink")、[九份等地](../Page/九份.md "wikilink")。
  - 由遠望坑口沿[省道](../Page/省道.md "wikilink")[台2丙往東可接](../Page/台2線.md "wikilink")[臺2線](../Page/臺2線.md "wikilink")，往北可抵[鹽寮抗日紀念碑](../Page/鹽寮抗日紀念碑.md "wikilink")、[龍洞](../Page/龍洞.md "wikilink")、[鼻頭角等景點](../Page/鼻頭角.md "wikilink")；往東可至[福隆](../Page/福隆.md "wikilink")，附近有[福隆海水浴場](../Page/福隆海水浴場.md "wikilink")，續前行可抵臺灣島東端—[三貂角](../Page/三貂角.md "wikilink")，附近有[三貂角燈塔](../Page/三貂角燈塔.md "wikilink")。
  - 由大里沿[省道](../Page/省道.md "wikilink")[台2線往西南可抵](../Page/台2線.md "wikilink")[頭城](../Page/頭城鎮.md "wikilink")、[宜蘭](../Page/宜蘭市.md "wikilink")、[羅東](../Page/羅東鎮_\(臺灣\).md "wikilink")、[蘇澳等](../Page/蘇澳鎮_\(臺灣\).md "wikilink")[宜蘭縣各鄉鎮](../Page/宜蘭縣.md "wikilink")。

## 相關條目

  - [國家步道系統](../Page/國家步道系統.md "wikilink")
  - [新北市登山步道](../Page/新北市登山步道.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [東北角暨宜蘭海岸國家風景區管理處－草嶺古道](http://www.necoast-nsa.gov.tw/user/Article.aspx?Lang=1&SNo=04000486)

<!-- end list -->

  - [大里驛青年旅館](https://www.facebook.com/daliyihostel)：台灣東北角暨宜蘭風景區的青年旅舍，位於頭城鎮大里火車站對面，鄰近草嶺古道、三貂角燈塔，環島一號線上。

[Category:台灣古道](../Category/台灣古道.md "wikilink")
[Category:新北市旅遊景點](../Category/新北市旅遊景點.md "wikilink")
[Category:宜蘭縣旅遊景點](../Category/宜蘭縣旅遊景點.md "wikilink")
[Category:東北角暨宜蘭海岸國家風景區](../Category/東北角暨宜蘭海岸國家風景區.md "wikilink")
[Category:新北市道路](../Category/新北市道路.md "wikilink")
[Category:新北市登山步道](../Category/新北市登山步道.md "wikilink")
[Category:貢寮區](../Category/貢寮區.md "wikilink")
[Category:頭城鎮](../Category/頭城鎮.md "wikilink")

1.  《台灣通訊》，[伊能嘉矩著](../Page/伊能嘉矩.md "wikilink")
    楊南郡譯註，[遠流出版](../Page/遠流出版社.md "wikilink")，1996年9月
2.  [《景點導覽－遠望坑親水公園》，東北角暨宜蘭海岸國家風景區網站](http://wayback.archive.org/web/20071001000000*/http://www.necoast-nsa.gov.tw/index.php?act=detail&ID=368)
3.  [《周邊景點
    草嶺古道》，2008草嶺古道芒花季官方網站](https://web.archive.org/web/20081020105722/http://2008sg.necoast-nsa.gov.tw/ScenicSpot/index.asp)