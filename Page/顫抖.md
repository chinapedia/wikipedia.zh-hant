**顫抖**或**震顫**（）是身體部位因不自主[肌肉收縮而造成的震動](../Page/肌肉收縮.md "wikilink")。最常發生在[手部](../Page/手.md "wikilink")；通常是[心脈引致](../Page/脈搏.md "wikilink")，人人都有，是正常生理現象。不過震顫嚴重者可能患有[疾病](../Page/疾病.md "wikilink")。

## 成因

  - [帕金森症的](../Page/帕金森症.md "wikilink")
  - 緊張、焦慮、情緒激動（憤怒）
  - [甲狀腺中毒](../Page/甲狀腺.md "wikilink")（[甲狀腺功能亢進症](../Page/甲狀腺功能亢進症.md "wikilink")）
  - [肝功能嚴重損害](../Page/肝.md "wikilink")（如[肝病](../Page/肝病.md "wikilink")），或[精神錯亂](../Page/精神.md "wikilink")
  - 因[酗酒而](../Page/酗酒.md "wikilink")[酒精中毒](../Page/酒精中毒.md "wikilink")
  - 服藥（如抗精神病藥[鋰鹽](../Page/鋰鹽.md "wikilink")、[支氣管擴張劑等](../Page/支氣管擴張劑.md "wikilink")）之後，手抖是可預期的[不良反應](../Page/不良反應_\(醫學\).md "wikilink")
  - 服用過多的[咖啡因](../Page/咖啡因.md "wikilink")
  - [小腦疾患或功能異常造成的](../Page/小腦.md "wikilink")

## 另見

  - [冷颤](../Page/冷颤.md "wikilink")，因冷凍而出現的顫抖

## 外部連結

  - Some text copied with permission and thanks.

  -
  - <http://www.orthostatictremor.org>

[Category:症狀](../Category/症狀.md "wikilink")
[Category:生理學](../Category/生理學.md "wikilink")