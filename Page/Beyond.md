**Beyond**，[香港](../Page/香港.md "wikilink")[搖滾樂隊](../Page/搖滾樂隊.md "wikilink")，1983年成立，早年被視為地下樂隊，1987年加入香港主流樂壇，是[華人樂壇上最具代表性的搖滾樂隊之一](../Page/華人.md "wikilink")。
樂隊自成立以來一直堅持本地原創及積極採用[粵語歌詞取代當時香港以](../Page/粵語.md "wikilink")[英語為主流的搖滾樂](../Page/英語.md "wikilink")，將在香港被視為「地下音樂」的[重金属搖滾樂帶到主流樂壇](../Page/重金属.md "wikilink")，並通过重新包装使其普及。樂隊無論在作曲、填詞、編曲、演奏及監製等，大部分均由各成員包辦，而作品內容多以寫實為主，當中包括：樂隊成員對未來的憧憬和盼望、表達追求理想與現實世界之間的矛盾、反映社會狀況及諷刺時弊、宣揚和平與愛，還有各成員對世界周邊事情的所見所感等。
2004年末，樂隊宣佈解散，並於2005年舉行《Beyond The Story Live
2005》告別演唱會，之後各成員各自繼續發展自己的音樂事業。

## Beyond名稱由來

樂隊名字「Beyond」（解作「超越」）是由創隊成員兼主音結他手[鄧煒謙所改](../Page/鄧煒謙.md "wikilink")，他指「Beyond」是「Beyond
Rock and Roll」意思，即是樂隊以搖滾樂為主流，但玩的卻是搖滾主流以外的音樂。\[1\]

而在1998年出版的樂隊自傳《[擁抱Beyond歲月](../Page/擁抱Beyond歲月.md "wikilink")》中，鼓手葉世榮解釋，由於樂隊喜愛自己創作，有別於當時其他樂隊多數翻唱外國樂隊的作品，故「Beyond」有超越一般樂隊所涉足的音樂領域的意思，但葉世榮重申，Beyond不是要超越他人，而是要超越自己。\[2\]

## 成員

樂隊早期經過幾次人事變動，成員人數曾經多達五名，以四人時期最廣為人熟悉，當中包括樂隊主音、節奏結他手及創作重心兼靈魂人物[黃家駒](../Page/黃家駒.md "wikilink")、主音結他手[黃貫中](../Page/黃貫中.md "wikilink")、低音結他手[黃家強及鼓手](../Page/黃家強.md "wikilink")[葉世榮](../Page/葉世榮.md "wikilink")。

1985年，[黃貫中加入後](../Page/黃貫中.md "wikilink")，由[黃家駒](../Page/黃家駒.md "wikilink")、[黃貫中](../Page/黃貫中.md "wikilink")、[黃家強及](../Page/黃家強.md "wikilink")[葉世榮四人組成的Beyond成形](../Page/葉世榮.md "wikilink")。1986年，鍵琴手及結他手[劉志遠加入成為第五名隊員](../Page/劉志遠_\(香港音樂人\).md "wikilink")。1988年，劉退出後，樂隊一直維持在四人狀態，亦是樂隊的全盛時期。1993年，年僅31歲的黃家駒於[日本拍攝遊戲節目時發生意外](../Page/日本.md "wikilink")，昏迷6日後離世，此後，樂隊以三人組合繼續發展。

### 樂隊成員

<table>
<tbody>
<tr class="odd">
<td><p><strong>成員</strong></p></td>
<td><p><strong>出生日期</strong></p></td>
<td><p><strong>在籍時期</strong></p></td>
<td><p><strong>主要崗位</strong></p></td>
<td><p><strong>其他崗位</strong></p></td>
<td><p><strong>簡介</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>主要成員</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃家駒.md" title="wikilink">黃家駒</a></p></td>
<td><p><br />
卒：</p></td>
<td><p>1983-1993</p></td>
<td><p><a href="../Page/主音.md" title="wikilink">主音</a></p></td>
<td><p><a href="../Page/節奏結他.md" title="wikilink">節奏結他</a>、<a href="../Page/和音.md" title="wikilink">和音</a></p></td>
<td><p>- 創隊隊員<br />
- 樂隊中的<strong>靈魂人物</strong>及大哥<br />
- 樂隊的創作重心，包括曲、詞、編、監、唱等多個範疇<br />
- 樂隊分支<a href="../Page/Unknown_(樂隊).md" title="wikilink">Unknown隊員</a><br />
- 認識<a href="../Page/李榮潮.md" title="wikilink">李榮潮在先</a>，後因<a href="../Page/葉世榮.md" title="wikilink">葉世榮和</a><a href="../Page/關寶璇.md" title="wikilink">關寶璇召募低音結他手時被帶同應約而認識二人</a>[3]<br />
- 後認識了<a href="../Page/鄧煒謙.md" title="wikilink">鄧煒謙</a>，並介紹<a href="../Page/葉世榮.md" title="wikilink">葉世榮和</a><a href="../Page/李榮潮.md" title="wikilink">李榮潮給他認識</a>，成為了Beyond的雛型[4]<br />
- 家中排行第四，<a href="../Page/黃家強.md" title="wikilink">黃家強為其胞弟</a><br />
- 中五畢業</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃貫中.md" title="wikilink">黃貫中</a></p></td>
<td></td>
<td><p>1985-1993</p></td>
<td><p><a href="../Page/主音結他.md" title="wikilink">主音結他</a></p></td>
<td><p>主音、和音</p></td>
<td><p>- 四子中最遲加入的隊員<br />
- 為樂隊首個演唱會設計海報及場刊而認識<br />
- 樂隊舉辦首個演唱會前2個月才被邀請加入當臨時結他手，頂替離隊的<a href="../Page/陳時安.md" title="wikilink">陳時安</a><br />
- 樂隊分支<a href="../Page/高速啤機.md" title="wikilink">高速啤機隊員</a><br />
- 家中排行最大，有兩弟，二弟為<a href="../Page/亞龍大.md" title="wikilink">亞龍大</a>（Anodize）結他手<a href="../Page/黃貫其.md" title="wikilink">黃貫其</a><br />
- <a href="../Page/中學會考.md" title="wikilink">中學會考一優二良</a>，前<a href="../Page/香港理工學院.md" title="wikilink">香港理工學院</a>（現：<a href="../Page/香港理工大學.md" title="wikilink">香港理工大學</a>）設計系畢業[5]</p></td>
</tr>
<tr class="odd">
<td><p>1993-1999<br />
2003-2005</p></td>
<td><p>主音、<br />
主音結他</p></td>
<td><p>和音</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃家強.md" title="wikilink">黃家強</a></p></td>
<td></td>
<td><p>1984-1993</p></td>
<td><p><a href="../Page/低音結他.md" title="wikilink">低音結他</a></p></td>
<td><p>主音、和音</p></td>
<td><p>- 當<a href="../Page/鄧煒謙.md" title="wikilink">鄧煒謙和</a><a href="../Page/李榮潮.md" title="wikilink">李榮潮相繼離隊後</a>，樂隊缺人時被哥哥<a href="../Page/黃家駒.md" title="wikilink">黃家駒拉進隊裡</a><br />
- 樂隊分支<a href="../Page/Unknown_(樂隊).md" title="wikilink">Unknown隊員</a><br />
- 家中孻子，排行第五，<a href="../Page/黃家駒.md" title="wikilink">黃家駒為其胞兄</a>（二哥）<br />
- 中五畢業</p></td>
</tr>
<tr class="odd">
<td><p>1993-1999<br />
2003-2005</p></td>
<td><p>主音、<br />
低音結他</p></td>
<td><p>和音</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葉世榮.md" title="wikilink">葉世榮</a></p></td>
<td></td>
<td><p>1983-1993</p></td>
<td><p><a href="../Page/鼓.md" title="wikilink">鼓</a></p></td>
<td><p><a href="../Page/敲擊樂器.md" title="wikilink">敲擊樂器</a>、和音</p></td>
<td><p>- 創隊隊員<br />
- 樂隊分支<a href="../Page/高速啤機.md" title="wikilink">高速啤機隊員</a><br />
- 中六時與<a href="../Page/關寶璇.md" title="wikilink">關寶璇召募低音結他手時認識了</a><a href="../Page/李榮潮.md" title="wikilink">李榮潮及</a><a href="../Page/黃家駒.md" title="wikilink">黃家駒</a>[6]<br />
- 後經<a href="../Page/黃家駒.md" title="wikilink">黃家駒介紹下認識了</a><a href="../Page/鄧煒謙.md" title="wikilink">鄧煒謙</a>[7]<br />
- 樂隊Band房<a href="../Page/二樓後座.md" title="wikilink">二樓後座為其家人物業</a>，初期只佔單位中的一個小房間<br />
- 家中排行最大，有兩妹<br />
- 預科畢業（中六）</p></td>
</tr>
<tr class="odd">
<td><p>1993-1999<br />
2003-2005</p></td>
<td><p>主音、鼓</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>早期成員</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉志遠_(香港音樂人).md" title="wikilink">劉志遠</a></p></td>
<td></td>
<td><p>1986-1988</p></td>
<td><p>主音結他</p></td>
<td><p><a href="../Page/鍵盤樂器.md" title="wikilink">鍵盤</a>、和音</p></td>
<td><p>- 最年幼的隊員<br />
- 被視為<strong>第五隊員</strong><br />
- 加入前為二人樂隊<a href="../Page/浮世繪_(樂隊).md" title="wikilink">浮世繪隊員</a>，離隊後不久亦重組<br />
- 與<a href="../Page/黃家強.md" title="wikilink">黃家強意見不合而離隊</a>，但後來已冰釋前嫌<br />
- 離隊後亦曾參與過樂隊的演出及<a href="../Page/驚喜_(專輯).md" title="wikilink">幕後工作</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄧煒謙.md" title="wikilink">鄧煒謙</a><br />
（鄔林）</p></td>
<td><p>-</p></td>
<td><p>1983</p></td>
<td><p>主音結他</p></td>
<td></td>
<td><p>- 創隊隊員<br />
- 為樂隊命名<br />
- 樂隊分支<a href="../Page/高速啤機.md" title="wikilink">高速啤機隊員</a><br />
- 認識<a href="../Page/黃家駒.md" title="wikilink">黃家駒在先</a>，經他介紹下認識了<a href="../Page/葉世榮.md" title="wikilink">葉世榮及</a><a href="../Page/李榮潮.md" title="wikilink">李榮潮</a>[8]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李榮潮.md" title="wikilink">李榮潮</a></p></td>
<td><p>-</p></td>
<td><p>1983</p></td>
<td><p>低音結他</p></td>
<td></td>
<td><p>- 創隊隊員<br />
- 認識<a href="../Page/黃家駒.md" title="wikilink">黃家駒在先</a>，後來<a href="../Page/葉世榮.md" title="wikilink">葉世榮和</a><a href="../Page/關寶璇.md" title="wikilink">關寶璇召募低音結他手時帶同</a><a href="../Page/黃家駒.md" title="wikilink">黃家駒應約</a>[9]<br />
- 後經<a href="../Page/黃家駒.md" title="wikilink">黃家駒介紹下認識了</a><a href="../Page/鄧煒謙.md" title="wikilink">鄧煒謙</a>[10]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關寶璇.md" title="wikilink">關寶璇</a><br />
（Owen Kwan）</p></td>
<td><p>1983-1984</p></td>
<td><p>主音結他</p></td>
<td></td>
<td><p>- 與<a href="../Page/葉世榮.md" title="wikilink">葉世榮召募低音結他手時認識了</a><a href="../Page/李榮潮.md" title="wikilink">李榮潮及</a><a href="../Page/黃家駒.md" title="wikilink">黃家駒</a>[11]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳時安.md" title="wikilink">陳時安</a></p></td>
<td><p>-</p></td>
<td><p>1984-1985<br />
(約1年半)</p></td>
<td><p>主音結他</p></td>
<td></td>
<td><p>- 因精通英語，而創作了多首英語歌曲<br />
- 樂隊舉辦首個演唱會前因往外地升學而離隊</p></td>
</tr>
<tr class="even">
<td><p><strong>支援成員</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃仲賢.md" title="wikilink">黃仲賢</a></p></td>
<td></td>
<td><p>1996、2003、2005演唱會</p></td>
<td><p>主音結他</p></td>
<td></td>
<td><p>-支援1996、2003、2005 Beyond演唱會</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 成員在籍時間線

**※此圖準確度以年為單位** {{\#tag:timeline| ImageSize = width:900 height:400
PlotArea = left:50 bottom:125 top:0 right:0 Alignbars = justify
DateFormat = dd/mm/yyyy Period = from:01/01/1983 till:{{\#time:
01/01/2006 }} TimeAxis = orientation:horizontal format:yyyy Legend =
orientation:vertical position:bottom columns:2 ScaleMajor = increment:2
start:1983 ScaleMinor = increment:2 start:1983

Colors =

`id:vocals      value:red         legend:Vocals`
`id:chorus      value:orange      legend:Chorus`
`id:guitar      value:green       legend:Guitar`
`id:bass        value:blue        legend:Bass`
`id:keyboards   value:purple      legend:keyboards`
`id:drums       value:yellow      legend:Drums`
`id:lines       value:black       legend:Studio_albums`

Legend = orientation:vertical position:bottom columns:3

ScaleMajor = increment:2 start:1983 ScaleMinor = increment:1 start:1984

LineData =

`at:01/03/1986 color:black layer:back`
`at:01/07/1987 color:black layer:back`
`at:01/03/1988 color:black layer:back`
`at:06/09/1988 color:black layer:back`
`at:01/07/1989 color:black layer:back`
`at:01/12/1989 color:black layer:back`
`at:01/09/1990 color:black layer:back`
`at:06/09/1991 color:black layer:back`
`at:01/07/1992 color:black layer:back`
`at:01/05/1993 color:black layer:back`
`at:04/06/1994 color:black layer:back`
`at:15/06/1995 color:black layer:back`
`at:01/04/1997 color:black layer:back`
`at:01/12/1997 color:black layer:back`
`at:01/12/1998 color:black layer:back`
`at:01/11/1999 color:black layer:back`

BarData =

`bar:wongkakui text:"黃家駒"`
`bar:tangwaihim text:"鄧煒謙"`
`bar:kwanposhun text:"關寶璇"`
`bar:chanshion text:"陳時安"`
`bar:wongkwunchung text:"黃貫中"`
`bar:lauchiyuen text:"劉志遠"`
`bar:liwingchiu text:"李榮潮"`
`bar:wongkakeung text:"黃家強"`
`bar:yipsaiwing text:"葉世榮"`

PlotData=

`width:11 textcolor:black align:left anchor:from shift:(10,-4)`
`bar:wongkakui from:01/01/1983 till:01/01/1994 color:Vocals`
`bar:wongkakui from:01/01/1983 till:01/01/1994 color:Guitar width:3`

`bar:tangwaihim from:01/01/1983 till:01/01/1984 color:Guitar`
`bar:tangwaihim from:01/01/1983 till:01/01/1984 color:Vocals width:3`

`bar:kwanposhun from:01/01/1984 till:01/01/1985 color:Guitar`
`bar:chanshion from:01/01/1985 till:01/01/1986 color:Guitar`
`bar:lauchiyuen from:01/01/1986 till:01/01/1989 color:Guitar`
`bar:lauchiyuen from:01/01/1986 till:01/01/1989 color:keyboards width:3`

`bar:wongkwunchung from:01/01/1986 till:01/01/1994 color:Guitar`
`bar:wongkwunchung from:01/01/1986 till:01/01/1994 color:Chorus width:3`
`bar:wongkwunchung from:01/01/1994 till:01/01/2000 color:Vocals`
`bar:wongkwunchung from:01/01/1994 till:01/01/2000 color:Guitar width:3`
`bar:wongkwunchung from:01/01/2003 till:01/01/2006 color:Vocals`
`bar:wongkwunchung from:01/01/2003 till:01/01/2006 color:Guitar width:3`

`bar:liwingchiu from:01/01/1983 till:01/01/1984 color:Bass`

`bar:wongkakeung from:01/01/1984 till:01/01/1994 color:Bass`
`bar:wongkakeung from:01/01/1984 till:01/01/1994 color:Chorus width:3`
`bar:wongkakeung from:01/01/1994 till:01/01/2000 color:Vocals`
`bar:wongkakeung from:01/01/1994 till:01/01/2000 color:Bass width:3`
`bar:wongkakeung from:01/01/2003 till:01/01/2006 color:Vocals`
`bar:wongkakeung from:01/01/2003 till:01/01/2006 color:Bass width:3`

`bar:yipsaiwing from:01/01/1983 till:01/01/1994 color:Drums`
`bar:yipsaiwing from:01/01/1983 till:01/01/1994 color:Chorus width:3`
`bar:yipsaiwing from:01/01/1994 till:01/01/2000 color:Vocals`
`bar:yipsaiwing from:01/01/1994 till:01/01/2000 color:Drums width:3`
`bar:yipsaiwing from:01/01/2003 till:01/01/2006 color:Vocals`
`bar:yipsaiwing from:01/01/2003 till:01/01/2006 color:Drums width:3`

}}

## 發展歷程

八十年代初，[黃家駒與](../Page/黃家駒.md "wikilink")[葉世榮經](../Page/葉世榮.md "wikilink")[土瓜灣嘉林琴行的老闆介紹結識並成為好友](../Page/土瓜灣.md "wikilink")，並發覺彼此都受英國[搖滾樂影響](../Page/搖滾樂.md "wikilink")，音樂取向一致，於是合組樂隊，負責主音結他的[鄧煒謙把樂隊命名](../Page/鄧煒謙.md "wikilink")“**Beyond**”。

### 1983–86：地下時期

#### 1983年

樂隊組建後，參加了《結他雜誌》所舉辦的「Players Festival香港結他／樂隊大賽」，以歌曲《腦部侵襲 BRAIN
ATTACK》獲得冠軍（Best Group
Award）。當時的Beyond尚未成型，他們同時又和其他樂隊合作，像黃家駒、黃家強和[太極樂隊的結他手](../Page/太極樂隊.md "wikilink")[鄧建明](../Page/鄧建明.md "wikilink")，也組成了NASA樂隊。當年Beyond創作的歌曲，如《大廈》、《腦部侵襲》等，以[英文歌詞和](../Page/英文.md "wikilink")[純音樂為主](../Page/純音樂.md "wikilink")，曲風都是[藝術搖滾](../Page/藝術搖滾.md "wikilink")（Art
Rock），較講求技術，著重音樂上的變化。1983年年底，[李榮潮與鄧煒謙相繼離隊](../Page/李榮潮.md "wikilink")，其後[黃家強](../Page/黃家強.md "wikilink")（黃家駒之弟）與[關寶璇加入](../Page/關寶璇.md "wikilink")，分別擔任低音結他手與主音結他手。

#### 1984年

在「香港結他／樂隊大賽」獲獎後，成員一邊工作，一邊創作音樂，樂隊偶爾在一些[酒吧等地方作小型演出](../Page/酒吧.md "wikilink")。

《結他雜誌》集合了結他／樂隊大賽優勝組合灌錄了一張名為《[香港Xiang
Gang](../Page/香港_\(合輯\).md "wikilink")》的黑膠唱片，當中有Beyond的兩首英文原創歌曲《腦部侵襲
BRAIN ATTACK》和《大廈 BUILDING》。

當年Beyond的作品仍是以英文歌曲為主，包括《Long Way Without
Friends》（這歌其後被改成中文歌《東方寶藏》收錄在《亞拉伯跳舞女郎》唱片裡，而英文版本則收錄於《孤單一吻》盒帶內）和《Myth》；之後也開始嘗試用[粵語創作歌曲](../Page/粵語.md "wikilink")，例如《永遠等待》。

#### 1985年

這時Beyond的成員為黃家駒、黃家強、葉世榮和[陳時安](../Page/陳時安.md "wikilink")，四人一起創作了不少歌曲，打算開演唱會。此時陳時安要出國升學，於是Beyond積極尋找結他手頂替。世榮認識阿Paul（[黃貫中](../Page/黃貫中.md "wikilink")）在先，當時他請在大專修讀設計的阿Paul幫Beyond設計海報；阿Paul本身也有參加其他的樂隊，結他技術高超，Beyond便請Paul幫忙，四人一起籌備演唱會的工作。

1985年7月20日，Beyond以地下樂隊的身份，租下香港堅道明愛中心開了一場“永遠等待”演唱會。Beyond邀請多家唱片公司人員來觀看，卻一個也沒見；幸好，有一位與他們簽訂了五年的合作關係，也就是以後與Beyond發生法律糾紛，Beyond的第一位經理人——[陳健添](../Page/陳健添.md "wikilink")。

這次演唱會，Beyond自己出錢出力，台前幕後一切事務皆親力親為，他們更向銀行貸款一萬六千元，才可順利舉行，但最後結算還是虧損了六千元。他們當時的人工只有一千元，那更顯出他們的決心。當時他們的想法是要以一個公開的表演形式，將自己的作品介紹給其他人認識；更樂觀的想法是希望籍這次音樂會賺​​取一筆錢，來應付之後出版盒帶的費用。

在當時而言，可以說是個大膽而自信的嘗試：當時Beyond嘗試了多種音樂風格，包括藝術搖滾、後朋克、新浪潮、重金屬甚至華麗搖滾；也開始嘗試用粵語創作搖滾作品，如《永遠等待》、《舊日的足跡》便是當時的作品。
Beyond以前衛樂隊的形象確立在香港地下樂壇的江湖地位。

#### 1986年

對音樂的狂熱，讓Beyond又完成了另一創舉：租錄音室，將自己創作的歌曲做成名為《[再見理想](../Page/再見理想.md "wikilink")》的專輯盒帶；從唱片的包裝設計，到錄製配唱，到找唱片行寄賣等等，完全一手包辦。

Beyond在這年新增一名成員——[劉志遠](../Page/劉志遠_\(香港音樂人\).md "wikilink")（鍵盤手兼吉他手）。

是年Beyond和小島、達明一派合錄了一張盒帶《勁歌金曲》，其中收錄了他們的四首歌曲；Beyond和小島也在七月份應台北汎亞音樂節之邀到台北參與演出，因為他們的音樂頗受歡迎，他們是當中唯一加場演出的樂隊。

同時，世榮和阿Paul與鄧煒謙、馬永基組成一支重金屬樂隊——高速啤機（這也是Beyond的分支樂隊），以玩票性質參加一些地下音樂會的演出，並參加了八六年度“嘉士伯流行音樂節”。Beyond也於本年正式簽下Kinn's
Music Ltd，為進軍流行樂壇做準備。

### 1986–1988年：五人時期

雖然Beyond已經成立了好幾年，但直至1987年推出首張EP《[永遠等待](../Page/永遠等待.md "wikilink")》及第二張專輯《[亞拉伯跳舞女郎](../Page/亞拉伯跳舞女郎.md "wikilink")》後，才開始踏入主流樂壇為人認識。

#### 1987年

Beyond出第一張EP《永遠等待》，其中《昔日舞曲》、《Water
Boy》及標題歌《永遠等待》隨即成為的士高的熱門歌曲；而《昔日舞曲》還曾走上香港電台流行榜，並被電視台拍成MTV，這也是Beyond第一首作品被拍成MTV；這張EP，成了他們前進流行樂壇的跳板。不過實際上Beyond仍未被大眾接受，他們的裝扮更被評得一文不值；當時的Beyond，就走在這吃力不討好的兩難局面中：舊日的追隨者指責他們背棄理想和原則走向商業化，而初接觸的又批評他們過於前衛；他們在首次接受港台DJ[車淑梅訪問中](../Page/車淑梅.md "wikilink")，亦被取笑是否常被稱作“長毛飛”。

此時樂隊風正在流行，香港爆發勁Band浪潮：達明一派、太極、風雲、Beyond、小島等二十餘支勁旅震撼​​流行樂壇；Beyond稱這不是復蘇，而是一場音樂革命，他們對未來寄予厚望。

是年7月，Beyond發行第一張非自資的專輯大碟《亞拉伯跳舞女郎》（《阿拉伯跳舞女郎》）。唱片封面取景自新加坡，是一張充滿中東風情的專輯；在音樂上他們有特別的表現​​，但在形像上卻再次受到嚴厲的批評。Beyond正嘗試尋找介於商業和搖滾之間的平衡點，不過，這倒讓他們成了非主流中的主流音樂；他們在音樂中加入了多一些的電子元素，使音樂比較柔和，易於讓人接受。除主題曲奪得流行榜冠軍外，《無聲的告別》和《孤單一吻》也相繼打入排行榜。不過銷售成績仍欠理想，Beyond的命運仍是未知數。

是年10月，Beyond在高山劇場舉行了“Beyond超越亞拉伯”演唱會，雖然僅只一場，但Beyond當時只出版了一張專輯，在短短的十個月內便能夠成功舉辦了一個有二千人入坐的買票專場演唱會，成績殊不簡單。自此之後，經理人陳健添為他們接下不少演出工作，各大小商業典禮都有他們的踪跡，這無疑是成功的商業策略，但對於Beyond自身來說，便顯得無奈。

#### 1988年

Beyond的音樂風格和形象仍然未能為大眾接受，唱片銷量並不理想。1988年3月推出專輯《[現代舞台](../Page/現代舞台.md "wikilink")》，重新收錄了《舊日的足跡》，音樂走向較以前柔和，有些歌曲走流行路線，Beyond式的慢板情歌也於這時出現，如《冷雨夜》、《天真的創傷》，諷刺社會的《現代舞台》，是Beyond開始批判社會現象的開始，黃貫中和黃家強首次有了自己的主唱歌曲。不過這張專輯的銷量也欠佳，而經紀人也對他們言明如果專輯再不賣，他們就沒有發片的機會了。

1988年12月，Beyond發行了一張《舊日足跡》精選集，並在大專會堂舉辦了一場音樂會，會上[劉志遠宣布要退出樂隊](../Page/劉志遠_\(香港音樂人\).md "wikilink")，其後更與[梁翹柏重組樂隊](../Page/梁翹柏.md "wikilink")「[浮世繪](../Page/浮世繪_\(樂隊\).md "wikilink")」，樂迷一直猜疑劉志遠離隊的原因。直至2008年6月，劉志遠才坦承當年與低音結他手[黃家強爭吵](../Page/黃家強.md "wikilink")，一時意氣下決定離隊。\[12\]\[13\]

也許是有即將不再發片的憂患意識，其後，Beyond簽約當時成立不久的[新藝寶唱片公司](../Page/新藝寶唱片.md "wikilink")，而Beyond的歌曲走向更加「商業化」。成員作出各種改變，例如把頭髮剪短，務求要一洗反叛青年的形象。第三張專輯《[秘密警察](../Page/秘密警察_\(專輯\).md "wikilink")》更嘗試走向大眾化，重新收錄四人合唱的《再見理想》，而合唱歌曲也成了他們最受歡迎的風格之一；《大地》有著強烈東方色彩的Rock，更是深深的唱入了聽眾的心，成了Beyond的第一首經典名曲；而《喜歡你》成了極受歡迎的情歌之一。這張專輯銷量理想，獲得雙白金的佳績，而專輯內的《喜歡妳》、《大地》亦成為當時的流行歌曲，Beyond更憑藉《大地》一曲獲選[1988年度十大勁歌金曲](../Page/1988年度十大勁歌金曲得獎名單.md "wikilink")，首次奪得[電子傳媒的獎項](../Page/電子傳媒.md "wikilink")，又與達明一派、小島樂隊合作錄製了香港搖滾史上第一張混音作品集，Beyond的混音加長版作品共三首，即《過去與今天Remix》、《孤單一吻Remix》、《昔日舞曲Remix》。

是年2月，香港電台為六十週年台慶推出紀念雜錦大碟，其中收錄了Beyond與達明一派、Blue
Jeans藍戰士、Fundamental、[Raidas](../Page/Raidas.md "wikilink")、太極、CoCos共七支樂隊合唱的《勁Band
super jam（特別即興）》，這是香港搖滾樂壇唯一一首串燒合唱歌；同時Beyond也為達明一派的大碟《你還愛我嗎》擔任兩首歌的和音。

是年10月15日至16日，Beyond前往北京首都體育館舉辦演唱會，成為最早在中國大陸演唱的香港明星。由於Beyond是以唱粵語歌為主，所以一般大陸聽眾並未接受，從原來客滿到終場時走了一半的人，但Beyond仍成功的辦完了這場演唱會。

雖然歌曲漸為大眾受落，但這些並非Beyond真正喜歡的音樂類型，加上當時部分地下時期追隨的樂迷批評他們失去樂隊原有的風格，故被罵為「搖滾叛徒」，這段日子Beyond過得並不容易。

### 1988–1991年：香港輝煌時期

不過Beyond並沒有理會外人的指責，[黃家駒表示](../Page/黃家駒.md "wikilink")，他們知道自己在做甚麼，要在商業化的[香港市場玩自己真正喜歡的](../Page/香港.md "wikilink")[音樂](../Page/音樂.md "wikilink")，就必須先打響樂隊的知名度，更多人聽Beyond的歌之後，就會玩回自己喜歡的音樂。於是，樂隊成員陸續接拍[電視劇及](../Page/電視劇.md "wikilink")[電影](../Page/電影.md "wikilink")，甚至做[節目主持](../Page/節目主持.md "wikilink")，更進一步自行塑造青春健康的形象，吸引不少年輕樂迷。

真正推Beyond上高峰的，是1989年以歌頌母親為題的作品《真的愛妳》。

#### 1989年

Beyond於北京辦完演唱會，返港後參加了電影《黑色迷牆》的配樂工作，並為其演唱主題曲《午夜迷牆》（收錄在EP《[Beyond
四拍四](../Page/Beyond_四拍四.md "wikilink")》）。

4月，推出第四張EP《[Beyond 四拍四](../Page/Beyond_四拍四.md "wikilink")》。

7月，推出第四張專輯《[Beyond
IV](../Page/Beyond_IV.md "wikilink")》，其中以歌頌母愛為題的《真的愛妳》（電影《[開心鬼救開心鬼](../Page/開心鬼救開心鬼.md "wikilink")》
插曲）奪得當年的[十大勁歌金曲及](../Page/1989年度十大勁歌金曲得獎名單.md "wikilink")[十大中文金曲兩大獎項](../Page/第十二屆十大中文金曲得獎名單.md "wikilink")，由於歌曲大熱，其後專輯《Beyond
IV》獲得雙白金獎。此時Beyond的發展更為廣泛，音樂的商業色彩也更顯濃厚；Beyond也開始成為人們眼中的偶像樂隊，香港導演[杜琪峰也邀請他們參演](../Page/杜琪峰.md "wikilink")《吉星拱照》（[周潤發](../Page/周潤發.md "wikilink")、[張艾嘉領銜主演](../Page/張艾嘉.md "wikilink")）及創作和演唱主題歌《午夜怨曲》。

11月，Beyond與寶麗金參加台灣“永遠的朋友”演唱會，第一次演唱國語歌曲。

12月，在伊利沙白體育館舉行七場“真的見證演唱會”，隨後，發行第五張粵語專輯《[真的見證](../Page/真的見證.md "wikilink")》，收錄了多首他們為其他歌手創作的歌曲。

#### 1990年

Beyond樂隊參加了《開心鬼4——開心鬼救開心鬼》的演出，並為其演唱主題曲《戰勝心魔》和《文武英傑宣言》；應邀為“綠色一代新主張”寫了一首《送給不知怎去保護環境的人（包括我）》，又為電影《天若有情》演唱了插曲，還舉辦了澳門萬人勁Rock音樂會。同年9月推出的粵語專輯《命運派對》更獲得三白金佳績（逾15萬張唱片銷量）；專輯內的《光輝歲月》和《俾面派對》亦分別奪得當年的十大勁歌金曲和十大中文金曲，黃家駒更憑《光輝歲月》一曲奪得十大勁歌金曲的「最佳填詞獎」。

這年他們正式向東南亞進軍，發行了首張國語大碟《大地》，值得一提的是《文武英傑宣言》的國語版（並未收錄在此張專輯中）。而同年也出了一張大碟《命運派對》，其中的《俾面派對》是諷刺演藝圈光怪陸離的現象；而在這張專輯中，有不少關懷第三世界的歌曲，如《光輝歲月》就是家駒為南非第一位黑人總統曼德拉致敬所寫的；家駒也憑藉《光輝歲月》成為了年度最佳填詞人。本年Beyond也成為了香港​​世界宣明會的代言人；反對種族歧視，希望世界和平，一直是Beyond的心願。
Beyond也開始一點一滴，朝著國際化邁進。

Beyond一向愛好和平，並以自己的音樂向人們呼喚和爭取和平，指引人們用愛驅散世上罪惡的戰爭。
1990年海灣戰爭後，世界進入了一個相對的短暫和平時期。家駒便寫了《AMANI》這首歌，《AMANI》是Beyond非洲之行回來創作的，歌曲表達一種對戰爭的控訴，感慨戰爭最大的受害者是無辜天真的兒童的情感，呼籲和平。《AMANI》是非洲的語言，大意是和平，當時黃家駒只用十分鐘就寫好了這首歌的副歌部分，抒發了對戰後和平長久的渴望,也警示了人們必須要以自己的努力鬥爭來爭取和平，一味地求助於神靈是不行的。這首歌堪稱是頌和平歌之最。同時，Beyond成為香港世界宣明會的代言人。

#### 1991年

Beyond應世界宣明會之邀到非洲等地去探訪第三世界的窮困人民，並成立了一個第三世界基金；4月發行了第二張國語專輯《光輝歲月》，並在台灣舉行了一個小型的演唱會，這是他們繼1986年演出之後，另一個在台灣的現場演出；同時電影公司為他們量身定做了一部《Beyond日記之莫欺少年窮》的勵志電影，而他們也為電影創作了《誰伴我闖蕩》、《不再猶豫》等多首歌曲；同年，Beyond為無綫電視（香港電視廣播有限公司）主持一個為其度身訂做的暑期綜藝節目《Beyond放暑假》，在其中訪問歌手及演出短劇等；第七張專輯《猶豫》使他們被人批評為最商業的一張專輯，葉世榮也有了一首自己主唱的歌曲，即《完全的擁有》。同年9月，Beyond推出廣東話專輯《猶豫》，獲得雙白金的成績。專輯內Beyond在非洲之旅後由黃家駒創作的《AMANI》，表達對非洲之行後的感想，更流露出他對和平的憧憬。
Beyond亦憑此曲奪得當年的十大中文金曲。他們又踏上香港歌手心目中最佳的演唱會聖地——香港體育館，舉辦了“生命接觸演唱會”，成為繼[太極樂隊後第二隊於香港體育館舉行演唱會的搖滾樂隊](../Page/太極樂隊.md "wikilink")。在年底的一個偶然的機會，他們在NHK（日本放送協會）的節目上出現，簽約為Amuse經紀人，正式進軍日本樂壇。

### 1992–93：開拓日本市場，黃家駒客死異鄉

#### 1992年

年初，Beyond結束了與新藝寶4年合作的關係轉投華納唱片，並簽約Amuse積極開拓日本市場長居日本。與前幾年相比，前半年他們沉寂了不少，香港樂壇似乎少了Beyond的踪跡。成為國際化的樂隊，一直是Beyond的遙遠夢想；如今夢想有了實現的機會，他們便努力抓住。不過日本對於音樂製作上的嚴格要求，加上語言不通，他們頗為消沉了一陣子，但為了讓香港音樂能在日本樂壇上發出一點光，他們仍是十分努力，於是一張與以往Beyond音樂不同的專輯《繼續革命》出現了。他們的形像也從親切溫和而轉變為冷酷高調；《長城》更被邀請到日本音樂大師喜多郎製作片頭音樂；整張專輯編曲華麗漂亮，讓人耳目一新。不過由於沉寂了一段時間，使他們的聲勢有點下墜。

1992年底，Beyond在台湾发行了第三张国语专辑《信念》，重新签约滚石为其发行国语唱片。此外，Beyond还担任嘉宾录制了综艺节目《暑假玩到尽》；7月11日在香港海洋公园停车场举办了“Salem劲Band摇摆夜”；8月11日，在香港荃湾大会堂举办了“继续革命”音乐会。从年中推出的这些专辑中，可见Beyond创作曲风已和新艺宝时期有明显区别。不过Beyond的国语唱片在台湾一直叫好不叫座；他们为打入日本市场积极努力，不过成绩平平。

#### 1993年

Beyond結束了在台灣的宣傳期，赴日本去創作新專輯為著在日本發行新專輯而努力。
5月2日，Beyond舉辦了“我哋呀Unplugged音樂會”；5月28日，在吉隆坡國家室內體育館舉辦了“馬來西亞Unplugged音樂會”；5月底，他們回到了香港，帶回《樂與怒》這張專輯。
Beyond對這張專輯非常滿意，在錄音及編曲上也更為自由；《[海闊天空](../Page/海闊天空_\(Beyond歌曲\).md "wikilink")》這首充滿了Beyond十年心路歷程的歌曲，在本年成了本地最佳原創歌曲。隨後Beyond在香港和馬來西亞各舉辦了一場大型不插電的演出，並在6月底推出了日語單曲[遙かなる夢に～Far
away～](../Page/遙かなる夢に～Far_away～.md "wikilink")。

1993年是他們成立十週年的日子，年底也打算舉辦一場紀念十週年的大型演唱會。可是誰也沒有想到，在1993年6月24日凌晨，樂隊於日本東京富士電視台錄制著名遊戲節目《想做甚麼，就做甚麼》時發生意外，主音歌手黃家駒從2.7米高的舞台不慎直接墜下，頭部著地重傷昏迷，在東京女子醫科大學醫院留醫6日後，於1993年6月30日下午4時15分（該時間為日本當地時間，而香港當地時間是當天下午3時15分）與世長辭，終年31歲。這對於Beyond是一個沉重而悲慘的打擊。家駒一直是樂隊的主要唱作人，整張專輯詞曲，大部分都是由家駒包辦，而他一離開，也為Beyond的命運帶來重大轉折。

收錄於《樂與怒》專輯的歌曲《海闊天空》，成為了Beyond最後一首由黃家駒主唱的派台作品，亦是黃家駒客死異鄉以後的絕響；而其他黃家駒遺作有《情人》、《為了你，為了我》、《早班火車》等作品。
Beyond亦憑《海闊天空》一曲奪得當年的十大中文金曲及叱吒樂壇流行榜 —— “叱吒樂壇我最喜愛的本地創作歌曲大獎”。

Beyond於日本的兩年期間，共計推出兩張日語大碟《超越》、《This Is Love 1》，以及三張日語細碟《The
Wall～長城～》、《リソラバ～International～》、《くちびるを奪いたいc/w
遙かなる夢に～Far away～》。

### 1994–2003：三人時期及個人發展

#### 1994年

1994年，三人陣容的Beyond簽約香港滾石唱片後，開始灌錄第十張粵語專輯《二樓後座》。這張專輯試圖延續之前的曲風，阿Paul的唱腔，也開始轉為憤怒吶喊；家強不自覺的和家駒的唱腔接近；《醒你》這首批評香港民眾盲目崇拜偶像的歌曲使得他們倍受爭議。其後另外推出了兩張日語大碟《Second
Floor》、《遙かなる夢 Beyond
1992-1995》及兩張日語細碟《Paradise》、《アリガトウ》。與Amuse的合約結束後，Beyond沒有推出日語專輯，集中火力在香港及台灣市場。簽約滾石後，推出國語專輯《Paradise》同時在香港發行。
Beyond汲取了與前經理人陳健添發生合約糾紛的教訓，成立了Beyond Publishing Co
Ltd，取回歌曲版權，唱片公司負責發行。他們的形像也開始改變，走另類樂隊的路線。

#### 1995年

Beyond和Jim
Lee开始合作，远赴美国洛杉矶、加拿大温哥华等地录制新专辑，于6月底发行新专辑《[Sound](../Page/Sound.md "wikilink")》，也于香港活动中心举行了一场大型的户外演出，以光纤电缆将演唱会实况传送到各大商场。Beyond极注重結他方面的表现，及音乐上的丰富性；而在专辑中加入一首纯音乐，也是从此时开始。他们留心电子音乐的发展，三人乐队的音乐形态就此成型。《教坏细路》这首抨击TVB的歌曲，也为他们带和TVB间的嫌隙；香港的节目上，他们的宣传大减。11月，Beyond在台湾发行了第五张国语专辑《Love
&
Life》（《[愛與生活](../Page/愛與生活.md "wikilink")》），创作了四首全新的国语歌曲，并且办了三场名为“土洋大战”的音乐会。不过这张专辑的销售量不佳，使Beyond隔两年半才推出新国语大碟。同年Beyond也远赴韩国参加音乐活动，受到当地歌迷的欢迎。之后，乐队开始走向电子音乐。

#### 1996年

3月，Beyond在[香港體育館舉行](../Page/香港體育館.md "wikilink")[黃家駒去世後的首個大型演唱會](../Page/黃家駒.md "wikilink")《Beyond的精彩演唱會》。這四場演唱會展示Beyond依然強大的實力和樂隊頑強不屈的生命力。在最後一場演唱會上，[黃家強含淚清唱](../Page/黃家強.md "wikilink")《祝您愉快》并演奏《海闊天空》給遠方的哥哥，感動台下所有歌迷，成為华语乐坛史上最感人的場面之一。办完演唱会后，乐队沉寂了一段时间。同年Beyond将自己多年以来一直使用的Band房“二楼后座”改建为录音室，使他们成为了一支有自己录音室的乐队，此后他们着手培训新人，拿到本年度的最佳团体金奖，摆脱了长久以来都是拿银奖的命运，成为香港民众票选最爱的乐队。

#### 1997年

Beyond经过漫长的时间后，终于推出了大碟《[请将手放开](../Page/请将手放开.md "wikilink")》。整张大碟充满了试验性，Beyond的音乐也开始溶入迷幻电子音乐；《请将手放开》、《大时代》那种表现香港末世纪心态的歌曲，写得十分出色；他们也为保护动物基金会及聋人基金会写了歌曲，分别是《谁命我名字》和《回响》，不过《回响》被批评有Oasis的味道；《吓！讲乜嘢话》被指模仿Anodize的歌曲；不过整张专辑是在“二楼后座”录音室录制的，这是Beyond的自傲之处。

年底，他们又发行了一张大碟《惊喜》，Beyond的电子音乐风格仍在这张专辑中呈现。Beyond试着在唱腔上改变，想给歌迷全新之感；不过乐评人却觉得Beyond的音乐逐渐走向“黄贯中+黄家强+叶世荣”的感觉，而纯摇滚音乐也似乎不复以往；《回家》是写香港人回归后心态的歌；《雾》、《深》这类印象派歌曲，成了Beyond的新特色；而世荣用Drum
Loop创作的《无事无事》，更展现了他们音乐上的另一种新风貌；在纯音乐方面，Beyond亦交出了精彩作品。而他们也积极的录制国语专辑。同年还在九龙高山剧场举办了“摇摆预备Show”。

#### 1998年

2月，农历年过后，Beyond再度发行新国语大碟《这里那里》。他们在台宣传也比上一张专辑多了一些。

3月14日，Beyond办了场演唱会，歌迷除了台湾的，更有来自香港和韩国的，这让台湾媒体也见识到Beyond的魅力；后于同年4、5月份在台湾参加了几场校园演唱，他们希望能打入台湾市场的心情可见一斑。4月份在香港出了一张精选辑，《管我》这首歌改成了粤语版，并请香港各大学的乐队来参与配唱。

7月，美国华纳电影邀请Beyond为其即将上演的电影《轰天炮4》演唱主题曲。

9月，台湾发行了一张精选辑《Beyond Files》。而年底，他们推出全新粤语大碟《不见不散》。

#### 1999年

3月7日，Beyond在新城电台的主导下举办了一场纪念十五周年的“Beyond
2000”演唱会；不过由于音响不佳，主办单位的失职，歌迷们颇有指责。8月，在台湾台北市立体育馆举办了“夏日音乐高峰会”；11月发行专辑《[Good
Time](../Page/Good_Time.md "wikilink")》；12月圣诞节举行了“Good
Time”演唱会并宣布暂时解散，三人各自开始发展自己的音乐事业。

三人時期的作品中，除了《抗戰二十年》是[黃家駒的作品](../Page/黃家駒.md "wikilink")，三人時期的Beyond作品風格亦有所改變，比四人時期的Beyond更大膽、更創新。

這段期間，三人各有不同的發展。主音結他手[黃貫中與幾位好友組成了自己的樂隊](../Page/黃貫中.md "wikilink")“汗”，且推出四張專輯、兩張EP和一張國語專輯，另外亦是三位成員中唯一一位在[香港體育館舉行個人大型演唱會](../Page/香港體育館.md "wikilink")；至於[黄家強](../Page/黄家強.md "wikilink")，先後推出三張個人專輯和一張現場錄音專輯，並於2004年成立個人音樂品牌“Picasso
Horses”（《毕加索的马》），除了歌唱事業外，黃家強近年也從事幕後製作和經理人的工作，協助旗下樂隊[Kolor製作首張專輯](../Page/Kolor.md "wikilink")；鼓手[葉世榮方面](../Page/葉世榮.md "wikilink")，他分別在2001年和2003年推出個人EP，2005年才推出首張個人專輯《叶子红了》，和另外兩位成員不同的是葉世榮比較少在[香港進行發展](../Page/香港.md "wikilink")，目標放在[中國大陸的市場](../Page/中國大陸.md "wikilink")。葉世榮偶然亦會客串電視劇及電影。

### 2003年至今：半復合後正式解散

2003年4月，Beyond為纪念樂隊成立20周年，再次聚首舉行五場《Beyond超越Beyond演唱會》。雖然演唱會舉行期間正值[SARS肆虐](../Page/SARS.md "wikilink")，演唱會仍然順利舉行，還得到不錯的口碑，同年六月再加開三場。三子並把黃家駒一首遺作重新編曲出版，成為紀念周年作《抗戰二十年》，推出紀念EP《[Together](../Page/Together_\(EP\).md "wikilink")》（《同在》）。樂隊舉辦了首次世界巡迴演唱會，並先後在[中國內地](../Page/中華人民共和國.md "wikilink")、[北美洲和](../Page/北美洲.md "wikilink")[東南亞等地區演出](../Page/東南亞.md "wikilink")。巡迴結束後Beyond再次活躍於樂壇之中，另外並參加了不少音樂會的演出。

2004年，Beyond乐队凭电影《[無間道II](../Page/無間道II.md "wikilink")》主题曲《长空》荣获当年[香港电影金像奖之](../Page/香港電影金像獎.md "wikilink")“最佳电影原创歌曲奖”。然而这首歌曲却成为Beyond解散前最后一首原创音乐。同年11月，乐队公开宣布因三人音乐理念不同、再加上对香港乐坛的不满等问题，决定第二年举办世界巡回演唱会后正式解散。然而当时有些媒体却指出，乐队解散的真正原因是因为黄家强和黄贯中两人之间的恶劣关系。在2014年，黃家強正式在微博證實此事。\[14\]

2005年Beyond舉行“Beyond The Story Live
2005”世界告別巡迴演唱會，最後一站為[新加坡](../Page/新加坡.md "wikilink")。樂隊解散後，三人繼續以「音樂人」姿態發展自己的音樂事業。

2008年6月10日，黄贯中、黄家强、叶世荣於《別了家駒十五載——海闊天空音樂會》上再度同台演出。而盛傳不和的兩位成員黃家強與黃貫中亦在台上擁抱。之後黃貫中於一個活動透露，Beyond是否復合不是自己能決定，亦要顧及另外兩位隊員及其公司意向。黃貫中亦表示自己對於Beyond復合沒有太大的問題。同年，部分只有[黃家駒哼唱作曲而從未出版的遺作得到重新填詞](../Page/黃家駒.md "wikilink")，並由[黃家強主唱](../Page/黃家強.md "wikilink")，製作成《弦續——別了家駒15載》紀念專輯，歌曲有《无人的演奏》、《他的結他》、《奥林匹克》、《We
Are The People》等。

2009年7月24日至26日，[黃家強與](../Page/黃家強.md "wikilink")[黃貫中於](../Page/黃貫中.md "wikilink")[香港體育館開](../Page/香港體育館.md "wikilink")[演唱會](../Page/演唱會.md "wikilink")，這個演唱會被視為Beyond復合的一個好機會。至於另一名戰友[葉世榮則因為檔期不合而沒有加入](../Page/葉世榮.md "wikilink")。三場演唱會氣氛高漲，順利完成。

2010年，Beyond入選華語金曲獎30年經典評選中全部獎項，包括“30年30人”、“30年30碟”（以《樂與怒》入選）及“30年30歌”（以《海闊天空》入選）。這是Beyond解散後以乐队名義所获得的最高獎項。

2015年，[葉世榮有意於年底以三人形態再度站到台上開騷](../Page/葉世榮.md "wikilink")，宣傳[黃家駒對愛的理念](../Page/黃家駒.md "wikilink")，[黃貫中方面看似有意參與](../Page/黃貫中.md "wikilink")，但[黃家強仍毅然否決](../Page/黃家強.md "wikilink")。

2015年5月25日，黃家強再表示，Beyond緣已盡，重組已是無可能。\[15\]

## 影響

Beyond在20世纪80年代後期至90年代初期以創作樂隊形象活躍於香港、[日本等地](../Page/日本.md "wikilink")。在當時的偶像組合團體橫行年代，一直被社會認為是反叛與非主流的一個團體。樂隊成員因為被電視台和唱片公司所要求，被迫改變形象，甚至改變其音樂風格。在充滿不自由的音樂創作及表演環境中，Beyond曾遠走到[台灣及](../Page/台灣.md "wikilink")[日本發展](../Page/日本.md "wikilink")。

1989年，Beyond發表了以母愛為題材的《真的愛妳》，方成功「入屋」，被廣大觀眾接受。收錄《真的愛妳》一曲的大碟《Beyond
IV》內曲風走向商業化，與前一大碟[秘密警察截然不同](../Page/秘密警察_\(專輯\).md "wikilink")。Beyond四子更以短髮及西裝等重新包裝形象，一洗過往反叛的形象，更接拍電影、電視台劇集（如《[淘氣雙子星](../Page/淘氣雙子星.md "wikilink")》）、電視綜藝節目等。於《真的愛妳》一曲大舉成功後，Beyond重拾其搖滾曲風，推出《[真的見證](../Page/真的見證.md "wikilink")》、《[命運派對](../Page/命運派對.md "wikilink")》、《[猶豫](../Page/猶豫.md "wikilink")》、《[繼續革命](../Page/繼續革命.md "wikilink")》及《[樂與怒](../Page/樂與怒.md "wikilink")》大碟，成為樂隊在香港主流樂壇最受歡迎的時期。然而在Beyond的搖滾風格被廣泛接納時，樂隊創作靈魂兼主音[黃家駒不幸於](../Page/黃家駒.md "wikilink")1993年在[日本因錄製遊戲節目意外離世](../Page/日本.md "wikilink")。

[香港電台於](../Page/香港電台.md "wikilink")2008年製作了《不死傳奇》紀念特輯
——《不死傳奇——黃家駒》，當中記錄了原來在中國大陸改革開放的初期，遠至[大中華地區](../Page/大中華.md "wikilink")，縱使Beyond歌曲普遍以[粵語而非](../Page/粵語.md "wikilink")[國語歌唱](../Page/國語.md "wikilink")，Beyond的歌聲已經深深打進了中國內地年青人的心中。他們覺得Beyond的歌曲很能體會當時年輕人的心態，應驗了歌曲是沒有受到語言或地域的限制，只要能感染人的心靈便是好歌。有欣賞黃家駒的內地歌迷更稱呼黃家駒為藝術家，並非單純的偶像歌手。

Beyond的歌曲與其他歌手及樂隊幾乎絕大部分歌曲以愛情為主題不同，歌曲有不少是牽涉時事及社會題材，如《Amani》、《光輝歲月》、《大地》、《長城》等，亦有不少具勵志性質不少歌曲《[海闊天空](../Page/海闊天空_\(Beyond歌曲\).md "wikilink")》、《[抗戰二十年](../Page/Together_\(EP\).md "wikilink")》、《[長城](../Page/長城_\(Beyond\).md "wikilink")》\[16\]\[17\]\[18\]\[19\]\[20\]\[21\]等經常成為香港示威遊行的歌曲之一，於每年[七一大遊行均會由示威者高唱](../Page/七一大遊行.md "wikilink")。而這3首歌與《[光輝歲月](../Page/命運派對.md "wikilink")》、《[不再猶豫](../Page/猶豫.md "wikilink")》更同時成為[2014年香港學界大罷課](../Page/2014年香港學界大罷課.md "wikilink")、「[雨傘運動](../Page/雨傘運動.md "wikilink")」的歌曲之一。

## 音樂作品

Beyond曾發行過[粵語](../Page/粵語.md "wikilink")、[國語](../Page/國語.md "wikilink")、[英語及](../Page/英語.md "wikilink")[日語的歌曲](../Page/日語.md "wikilink")。

樂隊成立初期的歌曲以英語為主，不久便開始創作粵語歌曲以推動香港粵語搖滾樂壇。樂隊成名後為打入華語地區而製作國語歌曲，作品主要是將歌曲的粵語版本翻譯成國語版本。1992年，樂隊進軍日本市場，部份歌曲亦是由粵語版本翻譯成日語版本。

  - 詳細請見[Beyond歌曲對照表](../Page/Beyond歌曲對照表.md "wikilink")

### 專輯

全部專輯為樂隊原創作品。

#### 粵語

<table>
<tbody>
<tr class="odd">
<td><p><strong>次序</strong></p></td>
<td><p><strong>發行日期</strong></p></td>
<td><p><strong>專輯名稱</strong></p></td>
<td><p><strong>發行廠商</strong></p></td>
<td><p><strong>銷量</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>四子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p>1986年3月</p></td>
<td><p><a href="../Page/再見理想.md" title="wikilink">再見理想</a></p></td>
<td><p>自資</p></td>
<td><p>約2000張</p></td>
<td><p><strong>樂隊首張自資專輯</strong>，以盒帶形式發行</p></td>
</tr>
<tr class="even">
<td><p><strong>五子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>1987年7月1日</p></td>
<td><p><a href="../Page/亞拉伯跳舞女郎.md" title="wikilink">亞拉伯跳舞女郎</a></p></td>
<td><p><a href="../Page/KINN&#39;S_MUSIC_LTD.md" title="wikilink">KINN'S MUSIC LTD</a></p></td>
<td><p>約20000張[22]</p></td>
<td><p><strong>樂隊首張專輯</strong><br />
劉志遠加入後首張專輯</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1988年3月</p></td>
<td><p><a href="../Page/現代舞台.md" title="wikilink">現代舞台</a></p></td>
<td><p><a href="../Page/KINN&#39;S_MUSIC_LTD.md" title="wikilink">KINN'S MUSIC LTD</a></p></td>
<td><p>約20000張[23]</p></td>
<td><p>劉志遠最後一張參與的專輯</p></td>
</tr>
<tr class="odd">
<td><p><strong>四子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>1988年9月6日</p></td>
<td><p><a href="../Page/秘密警察_(專輯).md" title="wikilink">秘密警察</a></p></td>
<td><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td><p><a href="../Page/金唱片.md" title="wikilink">金唱片</a>[24]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>1989年7月</p></td>
<td><p><a href="../Page/Beyond_IV.md" title="wikilink">Beyond IV</a></p></td>
<td><p>新藝寶唱片</p></td>
<td><p>雙白金</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>1989年12月5日</p></td>
<td><p><a href="../Page/真的見證.md" title="wikilink">真的見證</a></p></td>
<td><p>新藝寶唱片</p></td>
<td><p>白金</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>1990年9月</p></td>
<td><p><a href="../Page/命運派對.md" title="wikilink">命運派對</a></p></td>
<td><p>新藝寶唱片</p></td>
<td><p>三白金</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>1991年9月6日</p></td>
<td><p><a href="../Page/猶豫.md" title="wikilink">猶豫</a></p></td>
<td><p>新藝寶唱片</p></td>
<td><p>雙白金</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>1992年7月</p></td>
<td><p><a href="../Page/繼續革命.md" title="wikilink">繼續革命</a></p></td>
<td><p><a href="../Page/華納唱片_(香港).md" title="wikilink">華納唱片</a></p></td>
<td><p>白金</p></td>
<td><p>加入<a href="../Page/華納唱片_(香港).md" title="wikilink">華納唱片的第一張專輯</a></p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>1993年5月</p></td>
<td><p><a href="../Page/樂與怒.md" title="wikilink">樂與怒</a></p></td>
<td><p>華納唱片</p></td>
<td></td>
<td><p><strong>黃家駒遺作</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>三子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>1994年6月4日</p></td>
<td><p><a href="../Page/二樓後座.md" title="wikilink">二樓後座</a></p></td>
<td><p><a href="../Page/滾石唱片_(香港).md" title="wikilink">滾石唱片</a></p></td>
<td><p>金唱片</p></td>
<td><p>首張由滾石唱片製作和發行的粵語專輯</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>1995年6月15日</p></td>
<td><p><a href="../Page/Sound_(Beyond專輯).md" title="wikilink">Sound</a></p></td>
<td><p>滾石唱片</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>1997年4月</p></td>
<td><p><a href="../Page/請將手放開.md" title="wikilink">請將手放開</a></p></td>
<td><p>滾石唱片</p></td>
<td><p>金唱片</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>1997年12月</p></td>
<td><p><a href="../Page/驚喜_(專輯).md" title="wikilink">驚喜</a></p></td>
<td><p>滾石唱片</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>1998年12月</p></td>
<td><p><a href="../Page/不見不散_(專輯).md" title="wikilink">不見不散</a></p></td>
<td><p>滾石唱片</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>1999年11月</p></td>
<td><p><a href="../Page/Good_Time_(Beyond專輯).md" title="wikilink">Good Time</a></p></td>
<td><p>滾石唱片</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |             |                                                         |                                    |                   |
| -------- | ----------- | ------------------------------------------------------- | ---------------------------------- | ----------------- |
| **次序**   | **發行日期**    | **專輯名稱**                                                | **發行廠商**                           | **銷量及附註**         |
| **四子時期** |             |                                                         |                                    |                   |
| 1        | 1990年10月    | [大地](../Page/大地_\(Beyond专辑\).md "wikilink")             | 新藝寶唱片                              | 白金                |
| 2        | 1991年4月     | [光輝歲月](../Page/光輝歲月_\(專輯\).md "wikilink")               | 新藝寶唱片                              |                   |
| 3        | 1992年12月4日  | [信念](../Page/信念_\(Beyond\).md "wikilink")               | [滾石唱片](../Page/滾石唱片.md "wikilink") | 首張由滾石唱片製作和發行的國語專輯 |
| 4        | 1993年9月9日   | [海闊天空](../Page/海闊天空_\(Beyond專輯\).md "wikilink")         | 滾石唱片                               | **黃家駒未完成的遺作**     |
| **三子時期** |             |                                                         |                                    |                   |
| 5        | 1994年7月13日  | [Paradise](../Page/Paradise_\(Beyond專輯\).md "wikilink") | 滾石唱片                               |                   |
| 6        | 1995年10月27日 | [愛與生活](../Page/愛與生活.md "wikilink")                      | 滾石唱片                               |                   |
| 7        | 1998年2月     | [這裡那裡](../Page/這裡那裡.md "wikilink")                      | 滾石唱片                               |                   |

#### 日語

|        |            |                                                        |                                              |               |
| ------ | ---------- | ------------------------------------------------------ | -------------------------------------------- | ------------- |
| **次序** | **發行日期**   | **專輯名稱**                                               | **發行廠商**                                     | **附註**        |
| 1      | 1992年9月26日 | [超越](../Page/超越.md "wikilink")                         | [Fun House](../Page/Fun_House.md "wikilink") |               |
| 2      | 1993年7月25日 | [This Is Love 1](../Page/This_Is_Love_1.md "wikilink") | Fun House                                    | **黃家駒未完成的遺作** |
| 3      | 1994年7月    | [Second Floor](../Page/Second_Floor.md "wikilink")     | Fun House                                    |               |

### EP

全部EP為樂隊原創作品。

<table>
<tbody>
<tr class="odd">
<td><p><strong>發行日期</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>發行廠商</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>五子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年1月</p></td>
<td><p><a href="../Page/永遠等待.md" title="wikilink">永遠等待</a></p></td>
<td><p><a href="../Page/寶麗金唱片.md" title="wikilink">寶麗金唱片</a><br />
<a href="../Page/KINN&#39;S_MUSIC_LTD.md" title="wikilink">KINN'S MUSIC LTD</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987年8月</p></td>
<td><p><a href="../Page/新天地_(Beyond_EP).md" title="wikilink">新天地</a></p></td>
<td><p>KINN'S MUSIC LTD</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>四子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989年4月</p></td>
<td><p><a href="../Page/四拍四.md" title="wikilink">四拍四</a></p></td>
<td><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td><p>收錄獲得<a href="../Page/第9屆香港電影金像獎.md" title="wikilink">第9屆香港電影金像獎</a>「最佳電影歌曲」提名的電影《<a href="../Page/黑色迷牆.md" title="wikilink">黑色迷牆</a>》主題曲《午夜迷牆》</p></td>
</tr>
<tr class="odd">
<td><p>1990年6月</p></td>
<td><p><a href="../Page/天若有情_(Beyond_EP).md" title="wikilink">天若有情</a></p></td>
<td><p>新藝寶唱片</p></td>
<td><p>收錄電影《<a href="../Page/天若有情_(1990年電影).md" title="wikilink">天若有情</a>》主題曲及插曲</p></td>
</tr>
<tr class="even">
<td><p>1990年7月</p></td>
<td><p><a href="../Page/戰勝心魔.md" title="wikilink">戰勝心魔</a></p></td>
<td><p>新藝寶唱片</p></td>
<td><p>收錄樂隊主演電影《<a href="../Page/開心鬼救開心鬼.md" title="wikilink">開心鬼救開心鬼</a>》主題曲及插曲</p></td>
</tr>
<tr class="odd">
<td><p>1993年1月</p></td>
<td><p><a href="../Page/無盡空虛.md" title="wikilink">無盡空虛</a></p></td>
<td><p><a href="../Page/華納唱片.md" title="wikilink">華納唱片</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>三子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年6月</p></td>
<td><p><a href="../Page/Beyond得精彩.md" title="wikilink">Beyond得精彩</a></p></td>
<td><p><a href="../Page/滾石唱片.md" title="wikilink">滾石唱片</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年7月</p></td>
<td><p><a href="../Page/Action.md" title="wikilink">Action</a></p></td>
<td><p>滾石唱片</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年4月</p></td>
<td><p><a href="../Page/Together_(Beyond_EP).md" title="wikilink">Together</a></p></td>
<td><p><a href="../Page/EMI.md" title="wikilink">EMI</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 單曲

全部單曲為樂隊原創作品。

<table>
<tbody>
<tr class="odd">
<td><p><strong>發行日期</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>語言</strong></p></td>
<td><p><strong>發行廠商</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>五子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年9月</p></td>
<td><p><a href="../Page/孤單一吻.md" title="wikilink">孤單一吻</a></p></td>
<td><p>粵語</p></td>
<td><p><a href="../Page/KINN&#39;S_MUSIC_LTD.md" title="wikilink">KINN'S MUSIC LTD</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>四子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年7月25日</p></td>
<td><p><a href="../Page/The_Wall～長城～.md" title="wikilink">The Wall c/w<br />
Only Heaven Knows</a></p></td>
<td><p>日語、<br />
粵語</p></td>
<td><p><a href="../Page/Fun_House.md" title="wikilink">Fun House</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年9月2日</p></td>
<td><p><a href="../Page/リゾ・ラバ_～International～_c/w_The_Morning_Train.md" title="wikilink">リゾ・ラバ ～International～ c/w<br />
The Morning Train</a></p></td>
<td><p>日語、<br />
粵語</p></td>
<td><p><a href="../Page/Fun_House.md" title="wikilink">Fun House</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年6月25日</p></td>
<td><p><a href="../Page/我想奪取妳的唇.md" title="wikilink">我想奪取妳的唇</a> c/w<br />
<a href="../Page/遙遠的夢～Far_away～.md" title="wikilink">遙遠的夢～Far away～</a></p></td>
<td><p>日語</p></td>
<td><p>Fun House</p></td>
<td><p>收錄《完全地愛吧》及《海闊天空》日文版<br />
<strong>黃家駒遺作</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>三子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994年11月2日</p></td>
<td><p><a href="../Page/Paradise_(Beyond單曲).md" title="wikilink">Paradise</a> c/w 冷雨沒暫停</p></td>
<td><p>日語</p></td>
<td><p>Fun House</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年3月15日</p></td>
<td><p><a href="../Page/謝謝_(Beyond單曲).md" title="wikilink">謝謝</a> c/w 祝您愉快</p></td>
<td><p>日語</p></td>
<td><p>Fun House</p></td>
<td><p>退出日本市場前的單曲</p></td>
</tr>
</tbody>
</table>

### 合輯

<table>
<tbody>
<tr class="odd">
<td><p><strong>發行日期</strong></p></td>
<td><p><strong>專輯名稱</strong></p></td>
<td><p><strong>語言</strong></p></td>
<td><p><strong>發行廠商</strong></p></td>
<td><p><strong>收錄樂隊作品</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984年9月</p></td>
<td><p><a href="../Page/香港_(合輯).md" title="wikilink">香港</a></p></td>
<td><p>粵語</p></td>
<td><p><a href="../Page/郭達年.md" title="wikilink">郭達年</a></p></td>
<td><p>腦部侵襲、大廈</p></td>
<td><p>香港結他比賽冠軍精英輯</p></td>
</tr>
<tr class="even">
<td><p>1988年3月</p></td>
<td><p><a href="../Page/香港電台.md" title="wikilink">香港電台六十週年紀念</a></p></td>
<td><p>粵語</p></td>
<td><p>-</p></td>
<td><p>勁Band Super Jam</p></td>
<td><p>與<a href="../Page/Blue_Jeans.md" title="wikilink">Blue Jeans</a>、<a href="../Page/太極樂隊.md" title="wikilink">太極樂隊</a>、<a href="../Page/Fundamental.md" title="wikilink">Fundamental</a>、<a href="../Page/Cocos_(樂隊).md" title="wikilink">Cocos</a>、<a href="../Page/Raidas.md" title="wikilink">Raidas</a>、<a href="../Page/達明一派.md" title="wikilink">達明一派合唱</a></p></td>
</tr>
<tr class="odd">
<td><p>1990年4月</p></td>
<td><p><a href="../Page/新藝寶五週年.md" title="wikilink">新藝寶五週年</a></p></td>
<td><p>粵語</p></td>
<td><p><a href="../Page/新藝寶唱片.md" title="wikilink">新藝寶唱片</a></p></td>
<td><p>太陽的心</p></td>
<td><p>1990年7月22日，<a href="../Page/香港電台.md" title="wikilink">香港電台</a>「<a href="../Page/太陽計劃.md" title="wikilink">太陽計劃</a>」主題曲</p></td>
</tr>
<tr class="even">
<td><p>1992年5月</p></td>
<td><p><a href="../Page/華納群星難忘您許冠傑.md" title="wikilink">華納群星難忘您許冠傑</a></p></td>
<td><p>粵語</p></td>
<td><p><a href="../Page/華納唱片.md" title="wikilink">華納唱片</a></p></td>
<td><p>半斤八両</p></td>
<td><p>紀念<a href="../Page/許冠傑.md" title="wikilink">許冠傑榮休翻唱致敬作品</a></p></td>
</tr>
<tr class="odd">
<td><p>1999年9月8日</p></td>
<td><p><a href="../Page/Love_Our_Bay.md" title="wikilink">Love Our Bay</a></p></td>
<td><p>多語</p></td>
<td><p><a href="../Page/滾石唱片.md" title="wikilink">滾石唱片</a></p></td>
<td><p>Love Our Bay（英語）、<br />
Love Our Way（國語）</p></td>
<td><p>日本發行<br />
<strong>Beyond with </strong>名義</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/超級Band_Band_Band.md" title="wikilink">超級Band Band Band</a></p></td>
<td><p>漢語</p></td>
<td><p>滾石唱片</p></td>
<td><p>Run Free、我害怕</p></td>
<td><p>台灣發行</p></td>
</tr>
</tbody>
</table>

### 精選輯

  - 粵語
      - 《[旧日足迹精选集](../Page/旧日足迹精选集.md "wikilink")》（1988年12月）
      - 《[昔日今日明日金曲](../Page/昔日今日明日金曲.md "wikilink")》（1990年11月）
      - 《[Tracking](../Page/Tracking.md "wikilink")》（1991年）
      - 《[Control](../Page/Control.md "wikilink")》（1992年）
      - 《[Recognition](../Page/Recognition.md "wikilink")》（1992年6月）
      - 《[精选辑 ENCORE](../Page/精选辑_ENCORE.md "wikilink")》（1992年10月）
      - 《[遙望黃家駒不死音樂精神
        特別紀念集92\~93](../Page/遙望黃家駒不死音樂精神_特別紀念集92~93.md "wikilink")》（1993年7月）
      - 《[各有各精彩 13周年纪念版](../Page/各有各精彩_13周年纪念版.md "wikilink")》（1996年3月）
      - 《[Play Back 自典
        字典](../Page/Play_Back_自典_字典.md "wikilink")》（1997年4月）
      - 《[Files\!](../Page/Files!.md "wikilink")》（1998年）
      - 《[The Best Of
        Beyond](../Page/The_Best_Of_Beyond.md "wikilink")》（1999年）
      - 《[原來](../Page/原來_\(歌曲专辑\).md "wikilink")》（1999年12月）
      - 《[全部](../Page/全部.md "wikilink")》（2000年）
      - 《[一世](../Page/一世.md "wikilink")》（2000年）
      - 《[Millennium](../Page/Millennium.md "wikilink")》（2000年1月）
      - 《[Beyond精選滾石香港黃金十年](../Page/Beyond精選滾石香港黃金十年.md "wikilink")》（2003年2月）
      - 《[最動聽的Beyond](../Page/最動聽的Beyond.md "wikilink")》（2004年2月）
      - 《[勁Band四鬥士Beyond音樂特輯1990](../Page/勁Band四鬥士Beyond音樂特輯1990.md "wikilink")》（2004年6月）
      - 《[Beyond The Ultimate
        Story](../Page/Beyond_The_Ultimate_Story.md "wikilink")》（2005年1月）
      - 《[Beyond 25th
        Anniversary](../Page/Beyond_25th_Anniversary.md "wikilink")》（2008年3月）
      - 《[大地 20週年](../Page/大地_20週年.md "wikilink")》（2011年1月）
      - 《[Beyond 音樂大全101 (5CD +
        DVD)](../Page/Beyond_音樂大全101_\(5CD_+_DVD\).md "wikilink")》（2011年9月）
      - 《[Beyond 30th
        Anniversary](../Page/Beyond_30th_Anniversary.md "wikilink")》
        (2013年7月)
  - 日語
      - 《[遙かなる夢 Beyond
        1992-1995](../Page/遙かなる夢_Beyond_1992-1995.md "wikilink")》（1995年5月）
      - 《[The Best Of
        Beyond](../Page/The_Best_Of_Beyond.md "wikilink")》（1999年）

### 演唱會專輯

  - 《Beyond 台北演唱會》（1986年演出，1997年发行台北演唱会现场专辑）
  - 《Beyond 超越亞拉伯演唱會》（1987年10月4日演出，1993年发行该演唱会现场专辑）
  - 《Beyond 北京演唱會》（1988年10月15日-16日演出，该演唱会现场专辑有3种不同版本）
  - 《Beyond 真的見證演唱會》（1989年12月5日-11日于伊利沙伯体育馆举行，该演唱会现场专辑发行时间1999年5月）
  - 《Beyond 1990 澳門萬人勁Rock音樂會》（1990年演出，收录于1999年《真的beyond III》）
  - 《Beyond Live 1991 生命接觸演唱會》（1991年9月19日-23日演出，12月发行该演唱会现场专辑）
  - 《Word & Music-Final Live With 家駒》（1993年8月）
  - 《祝您愉快演唱會》（滾石群星懷念家駒）（1994年7月）
  - 《Beyond 的精彩 Live & Basic》(1996年)
  - 《Beyond Good Time Live Concert》（1999年，该演唱会现场专辑2001年发行）
  - 《Beyond超越Beyond Live 2003》（2003年）
  - 《Beyond The Story Live 2005》（2005年）

（注：其中《祝您愉快演唱會》這張演唱會專輯是滾石群星懷念家駒而開設的一場演唱會）

## 派台歌曲紀錄（1987年至今）

| **派台歌曲成績**                                                |
| --------------------------------------------------------- |
| 唱片                                                        |
| **1987年**                                                 |
| [亞拉伯跳舞女郎](../Page/亞拉伯跳舞女郎.md "wikilink")                  |
| 亞拉伯跳舞女郎                                                   |
| **1988年**                                                 |
| [現代舞台](../Page/現代舞台.md "wikilink")                        |
| 現代舞台                                                      |
| 現代舞台                                                      |
| 現代舞台                                                      |
| [香港電台六十週年紀念（雜錦碟）](../Page/香港電台六十週年紀念（雜錦碟）.md "wikilink")  |
| [秘密警察](../Page/秘密警察_\(專輯\).md "wikilink")                 |
| 秘密警察                                                      |
| 秘密警察                                                      |
| **1989年**                                                 |
| [四拍四](../Page/四拍四.md "wikilink")                          |
| [Beyond IV](../Page/Beyond_IV.md "wikilink")              |
| Beyond IV                                                 |
| Beyond IV                                                 |
| Beyond IV                                                 |
| [真的見證](../Page/真的見證.md "wikilink")                        |
| 真的見證                                                      |
| **1990年**                                                 |
| 真的見證                                                      |
| [命運派對](../Page/命運派對.md "wikilink")                        |
| [天若有情](../Page/天若有情_\(Beyond_EP\).md "wikilink")          |
| [戰勝心魔](../Page/戰勝心魔.md "wikilink")                        |
| 命運派對                                                      |
| 命運派對                                                      |
| 命運派對                                                      |
| 命運派對                                                      |
| 命運派對                                                      |
| **1991年**                                                 |
| 命運派對                                                      |
| [猶豫](../Page/猶豫.md "wikilink")                            |
| 猶豫                                                        |
| 猶豫                                                        |
| 猶豫                                                        |
| 猶豫                                                        |
| 猶豫                                                        |
| **1992年**                                                 |
| [華納群星難忘您許冠傑](../Page/華納群星難忘您許冠傑.md "wikilink")            |
| [繼續革命](../Page/繼續革命.md "wikilink")                        |
| 繼續革命                                                      |
| 繼續革命                                                      |
| 繼續革命                                                      |
| [無盡空虛](../Page/無盡空虛.md "wikilink")                        |
| **1993年**                                                 |
| 無盡空虛                                                      |
| [樂與怒](../Page/樂與怒.md "wikilink")                          |
| 樂與怒                                                       |
| 樂與怒                                                       |
| **1994年**                                                 |
| [二樓後座](../Page/二樓後座.md "wikilink")                        |
| 二樓後座                                                      |
| 二樓後座                                                      |
| 二樓後座                                                      |
| **1995年**                                                 |
| [Sound](../Page/Sound_\(Beyond專輯\).md "wikilink")         |
| Sound                                                     |
| Sound                                                     |
| Sound                                                     |
| Sound                                                     |
| [愛與生活](../Page/愛與生活.md "wikilink")                        |
| **1996年**                                                 |
| [BEYOND得精彩](../Page/BEYOND得精彩.md "wikilink")              |
| BEYOND得精彩                                                 |
| BEYOND得精彩                                                 |
| **1997年**                                                 |
| [請將手放開](../Page/請將手放開.md "wikilink")                      |
| 請將手放開                                                     |
| 請將手放開                                                     |
| [驚喜](../Page/驚喜_\(專輯\).md "wikilink")                     |
| 驚喜                                                        |
| **1998年**                                                 |
| 驚喜                                                        |
| [Play Back 自典 字典](../Page/Play_Back_自典_字典.md "wikilink")  |
| [不見不散](../Page/不见不散_\(专辑\).md "wikilink")                 |
| [Action](../Page/Action.md "wikilink")                    |
| Action                                                    |
| 不見不散                                                      |
| **1999年**                                                 |
| 不見不散                                                      |
| 不見不散                                                      |
| [Good Time](../Page/Good_Time_\(Beyond專輯\).md "wikilink") |
| **2000年**                                                 |
| Good Time                                                 |
| **2003年**                                                 |
| [Together](../Page/Together_\(Beyond_EP\).md "wikilink")  |
| [无间道II电影原声大碟](../Page/无间道II电影原声大碟.md "wikilink")          |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **20**      |
| 26          |

  - 仍在榜上（\*）
  - 兩星期冠軍（**(1)**）
  - 四星期冠軍（**{1}**）

## 演唱會

Beyond是繼[溫拿樂隊](../Page/溫拿樂隊.md "wikilink")、[太極樂隊及](../Page/太極樂隊.md "wikilink")[達明一派之後](../Page/達明一派.md "wikilink")，第四隊在[香港體育館舉行演唱會的本地樂隊](../Page/香港體育館.md "wikilink")，亦是[香港體育館舉行演唱會場數最多的本地樂隊之一](../Page/香港體育館.md "wikilink")。

*註：僅列出大型或具特別意義的演唱會，個人時期演唱會請參見各隊員條目*

<table>
<tbody>
<tr class="odd">
<td><p><strong>日期</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>地區</strong></p></td>
<td><p><strong>場地</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>四子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985年7月20日</p></td>
<td><p>Beyond<a href="../Page/永遠等待.md" title="wikilink">永遠等待演唱會</a></p></td>
<td><p>香港</p></td>
<td><p>堅道<a href="../Page/明愛中心.md" title="wikilink">明愛中心</a></p></td>
<td><p>自資，首個演唱會</p></td>
</tr>
<tr class="even">
<td><p><strong>五子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1986年4月</p></td>
<td><p>剖析聚會</p></td>
<td><p>香港</p></td>
<td><p>中環<a href="../Page/藝穗會.md" title="wikilink">藝穗會</a></p></td>
<td><p>自資，2場</p></td>
</tr>
<tr class="even">
<td><p>1986年8月23日-25日</p></td>
<td><p>台北演唱會</p></td>
<td><p>台灣</p></td>
<td><p>台北<a href="../Page/榮星公園.md" title="wikilink">榮星公園</a></p></td>
<td><p>首屆亞太流行音樂節<br />
8月23日-24日與<a href="../Page/小島樂隊.md" title="wikilink">小島樂隊同場演出</a><br />
8月25日為加開的專場演出</p></td>
</tr>
<tr class="odd">
<td><p>1987年10月4日</p></td>
<td><p>Beyond超越<a href="../Page/亞拉伯跳舞女郎.md" title="wikilink">亞拉伯演唱會</a></p></td>
<td><p>香港</p></td>
<td><p>九龍<a href="../Page/高山劇場.md" title="wikilink">高山劇場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988年4月30日</p></td>
<td><p>萍果牌Beyond演唱會</p></td>
<td><p>香港</p></td>
<td><p>九龍<a href="../Page/大專會堂.md" title="wikilink">大專會堂</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>四子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988年10月15日-16日</p></td>
<td><p>香港Beyond演唱會</p></td>
<td><p>中國</p></td>
<td><p>北京<a href="../Page/首都體育館.md" title="wikilink">首都體育館</a></p></td>
<td><p>2場</p></td>
</tr>
<tr class="odd">
<td><p>1988年12月26日</p></td>
<td><p>Beyond連鎖歌迷唱聚<br />
- 心內演唱會<br />
- 心外演唱會</p></td>
<td><p>香港</p></td>
<td><p>九龍<a href="../Page/大專會堂.md" title="wikilink">大專會堂</a></p></td>
<td><p>2場<br />
宣傳樂隊散文集《心內心外》</p></td>
</tr>
<tr class="even">
<td><p>1989年12月5日-11日</p></td>
<td><p>Beyond<a href="../Page/真的見證.md" title="wikilink">真的見證演唱會</a></p></td>
<td><p>香港</p></td>
<td><p><a href="../Page/伊利沙伯體育館.md" title="wikilink">伊利沙伯體育館</a></p></td>
<td><p>7場</p></td>
</tr>
<tr class="odd">
<td><p>1991年9月19日-23日</p></td>
<td><p>Beyond<a href="../Page/猶豫.md" title="wikilink">生命接觸演唱會</a></p></td>
<td><p>香港</p></td>
<td><p><a href="../Page/紅磡體育館.md" title="wikilink">紅磡體育館</a></p></td>
<td><p>5場，首個紅館演唱會</p></td>
</tr>
<tr class="even">
<td><p>1991年11月24日</p></td>
<td><p>Beyond馬來西亞演唱會</p></td>
<td><p>馬來西亞</p></td>
<td><p><a href="../Page/亞通體育館.md" title="wikilink">吉隆坡國家室內體育館</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年8月11日</p></td>
<td><p>Beyond<a href="../Page/繼續革命.md" title="wikilink">繼續革命音樂會</a></p></td>
<td><p>香港</p></td>
<td><p><a href="../Page/荃灣大會堂.md" title="wikilink">荃灣大會堂</a></p></td>
<td><p><a href="../Page/商業二台.md" title="wikilink">商業二台主辦</a></p></td>
</tr>
<tr class="even">
<td><p>1993年5月2日</p></td>
<td><p>Beyond我哋呀！Unplugged音樂會</p></td>
<td><p>香港</p></td>
<td><p>香港電台1號錄音室</p></td>
<td><p><a href="../Page/香港電台第二台.md" title="wikilink">香港電台第二台節目</a>《音樂同志》主辦<br />
<strong>黃家駒最後一個香港演唱會</strong></p></td>
</tr>
<tr class="odd">
<td><p>1993年5月27日</p></td>
<td><p>Beyond Unplugged Live演唱會</p></td>
<td><p>馬來西亞</p></td>
<td><p>吉隆坡國家室內體育館</p></td>
<td><p><strong>黃家駒最後一個演唱會</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>三子時期</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年3月3日-3月6日</p></td>
<td><p><a href="../Page/Beyond得精彩.md" title="wikilink">Beyond的精彩Live</a>&amp;Basic演唱會</p></td>
<td><p>香港</p></td>
<td><p>紅磡體育館</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年5月31日</p></td>
<td><p><a href="../Page/Beyond得精彩.md" title="wikilink">Beyond的精彩大馬演唱會</a></p></td>
<td><p>馬來西亞</p></td>
<td><p>吉隆坡<a href="../Page/默迪卡體育場.md" title="wikilink">默迪卡體育場</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年3月7日</p></td>
<td><p>Beyond"2000"演唱會</p></td>
<td><p>香港</p></td>
<td></td>
<td><p>新城電台主辦</p></td>
</tr>
<tr class="even">
<td><p>1999年12月24日</p></td>
<td><p>Beyond <a href="../Page/Good_Time_(Beyond專輯).md" title="wikilink">Good Time演唱會</a></p></td>
<td><p>香港</p></td>
<td><p><a href="../Page/香港會議展覽中心.md" title="wikilink">香港會議展覽中心新翼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年12月31日</p></td>
<td><p>Beyond世紀末馬來演唱會</p></td>
<td><p>馬來西亞</p></td>
<td><p><a href="../Page/雲頂雲星劇場.md" title="wikilink">雲頂雲星劇場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年4月29日-5月4日</p></td>
<td><p>Beyond超越Beyond演唱會</p></td>
<td><p>香港</p></td>
<td><p>紅磡體育館</p></td>
<td><p>5場</p></td>
</tr>
<tr class="odd">
<td><p>2003年6月21日-6月23日</p></td>
<td><p>Beyond超越Beyond演唱會II</p></td>
<td><p>香港</p></td>
<td><p>紅磡體育館</p></td>
<td><p>3場（加場）</p></td>
</tr>
<tr class="even">
<td><p>2003年8月23日</p></td>
<td><p>Beyond超越Beyond演唱會（世界巡迴）</p></td>
<td><p>中國</p></td>
<td><p>北京<a href="../Page/工人體育場.md" title="wikilink">工人體育場</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年10月11日</p></td>
<td><p>馬來西亞</p></td>
<td><p>吉隆坡默迪卡體育場</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年11月15日</p></td>
<td><p>中國</p></td>
<td><p>廣州<a href="../Page/天河體育場.md" title="wikilink">天河體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年11月21日</p></td>
<td><p>中國</p></td>
<td><p><a href="../Page/上海體育場.md" title="wikilink">上海體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年11月27日</p></td>
<td><p>美國</p></td>
<td><p>大西洋城</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年12月4日</p></td>
<td><p>加拿大</p></td>
<td><p>多倫多 Niagara Arena</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年12月8日</p></td>
<td><p>加拿大</p></td>
<td><p>溫哥華Queen Elizabeth Theatre</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年12月20日</p></td>
<td><p>中國</p></td>
<td><p><a href="../Page/深圳體育場.md" title="wikilink">深圳體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年1月27日-2月1日</p></td>
<td><p>Beyond The Story Live 2005</p></td>
<td><p>香港</p></td>
<td><p>紅磡體育館</p></td>
<td><p>6場，告別演唱會</p></td>
</tr>
<tr class="odd">
<td><p>2005年4月9日</p></td>
<td><p>Beyond The Story Live 2005（世界巡迴）</p></td>
<td><p>美國</p></td>
<td><p>大西洋城</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年4月16日</p></td>
<td><p>加拿大</p></td>
<td><p>多倫多</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年5月27日</p></td>
<td><p>中國</p></td>
<td><p><a href="../Page/北京首都體育館.md" title="wikilink">北京首都體育館</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年5月29日</p></td>
<td><p>中國</p></td>
<td><p>黑龍江<a href="../Page/人民體育場.md" title="wikilink">人民體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年6月3日</p></td>
<td><p>中國</p></td>
<td><p>瀋陽<a href="../Page/五裏河體育場.md" title="wikilink">五裏河體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年6月11日</p></td>
<td><p>中國</p></td>
<td><p><a href="../Page/天津體育中心.md" title="wikilink">天津體育中心</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年7月16日</p></td>
<td><p>中國</p></td>
<td><p>長沙<a href="../Page/賀龍體育場.md" title="wikilink">賀龍體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年7月22日</p></td>
<td><p>中國</p></td>
<td><p>鄭州<a href="../Page/清華園體育館.md" title="wikilink">清華園體育館</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年8月27日</p></td>
<td><p>中國</p></td>
<td><p><a href="../Page/重慶中心體育場.md" title="wikilink">重慶中心體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年9月3日</p></td>
<td><p>中國</p></td>
<td><p><a href="../Page/上海大舞台.md" title="wikilink">上海大舞台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年9月7日</p></td>
<td><p>中國</p></td>
<td><p>武漢<a href="../Page/新華路體育場.md" title="wikilink">新華路體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年9月16日</p></td>
<td><p>中國</p></td>
<td><p>杭州<a href="../Page/黃龍體育中心.md" title="wikilink">黃龍體育中心</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年9月23日</p></td>
<td><p>中國</p></td>
<td><p>昆明<a href="../Page/拓東體育場.md" title="wikilink">拓東體育場</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年10月15日</p></td>
<td><p>新加坡</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 演出

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>日期</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>製作單位</strong></p></td>
<td><p><strong>角色名稱</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>黃家駒</strong></p></td>
<td><p><strong>黃貫中</strong></p></td>
<td><p><strong>黃家強</strong></p></td>
<td><p><strong>葉世榮</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/小說家族.md" title="wikilink">小說家族</a><br />
- 單元故事《對倒》[25]</p></td>
<td><p><a href="../Page/香港電台.md" title="wikilink">香港電台</a></p></td>
<td><p>街頭歌手<br />
（客串）</p></td>
<td><p>街頭樂手<br />
（客串）</p></td>
</tr>
<tr class="even">
<td><p>1987年9月</p></td>
<td><p><a href="../Page/暴風少年.md" title="wikilink">暴風少年</a><br />
- 單元故事《黑仔強》[26]</p></td>
<td><p><a href="../Page/香港電台.md" title="wikilink">香港電台</a></p></td>
<td><p>孿毛<br />
（客串）</p></td>
<td><p>黑仔強<br />
（<strong>主演</strong>）</p></td>
</tr>
<tr class="odd">
<td><p>1989年6月</p></td>
<td><p><a href="../Page/淘氣雙子星.md" title="wikilink">淘氣雙子星</a></p></td>
<td><p><a href="../Page/無綫電視.md" title="wikilink">無綫電視</a></p></td>
<td><p>-</p></td>
<td><p>查星宙<br />
（宙仔）<br />
（<strong>主演</strong>）</p></td>
</tr>
</tbody>
</table>

### 音樂特輯

|          |                                                              |          |                                   |
| -------- | ------------------------------------------------------------ | -------- | --------------------------------- |
| **日期**   | **名稱**                                                       | **製作單位** | **附註**                            |
| 1989年10月 | [夠Hit鬥玩Beyond加草蜢](../Page/夠Hit鬥玩Beyond加草蜢.md "wikilink")     | 無綫電視     | 與[草蜢合演](../Page/草蜢.md "wikilink") |
| 1990年10月 | [BEYOND特輯之勁Band四鬥士](../Page/BEYOND特輯之勁Band四鬥士.md "wikilink") | 無綫電視     | 故事根據樂隊真實歷史改編而成                    |

### 綜藝節目

<table>
<tbody>
<tr class="odd">
<td><p><strong>日期</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>製作單位</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p>1991年7月9日–<br />
1991年9月24日<br />
（逢星期二晚播放）</p></td>
<td><p><a href="../Page/Beyond放暑假.md" title="wikilink">Beyond放暑假</a></p></td>
<td><p>無綫電視</p></td>
<td><p>共12集<br />
主題曲《高溫派對》收錄於1991年專輯《<a href="../Page/猶豫.md" title="wikilink">猶豫</a>》內</p></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/暑假玩到盡.md" title="wikilink">暑假玩到盡</a></p></td>
<td><p>無綫電視</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>日期</strong></p></td>
<td><p><strong>名稱</strong></p></td>
<td><p><strong>角色名稱</strong></p></td>
<td><p><strong>附註</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>黃家駒</strong></p></td>
<td><p><strong>黃貫中</strong></p></td>
<td><p><strong>黃家強</strong></p></td>
<td><p><strong>葉世榮</strong></p></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/半邊人.md" title="wikilink">半邊人</a></p></td>
<td><p>（客串）</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>1987年3月</p></td>
<td><p><a href="../Page/肝膽相照.md" title="wikilink">肝膽相照</a></p></td>
<td><p>樂隊客串演唱</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年10月</p></td>
<td><p><a href="../Page/靚妹正傳.md" title="wikilink">靚妹正傳</a></p></td>
<td><p>樂隊客串演唱</p></td>
<td><p>前成員<a href="../Page/劉志遠.md" title="wikilink">劉志遠有份參與演出</a><br />
演唱了《昔日舞曲》，歌曲收錄於1987年EP《<a href="../Page/永遠等待.md" title="wikilink">永遠等待</a>》內</p></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td><p><a href="../Page/黑色迷牆.md" title="wikilink">黑色迷牆</a></p></td>
<td><p>樂隊客串演唱</p></td>
<td><p>電影主題曲及配樂由樂隊負責製作<br />
<strong>主題曲《午夜迷牆》更獲得<a href="../Page/第9屆香港電影金像獎.md" title="wikilink">第9屆香港電影金像獎</a>「最佳電影歌曲」提名</strong><br />
歌曲同時收錄於1989年EP《<a href="../Page/四拍四.md" title="wikilink">四拍四</a>》及1989年專輯《<a href="../Page/Beyond_IV.md" title="wikilink">Beyond IV</a>》內</p></td>
</tr>
<tr class="odd">
<td><p>1990年1月</p></td>
<td><p><a href="../Page/吉星拱照.md" title="wikilink">吉星拱照</a></p></td>
<td><p>遊向東</p></td>
<td><p>遊向南</p></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/忍者龜.md" title="wikilink">忍者龜</a></p></td>
<td><p>（配音）</p></td>
<td><p>（配音）</p></td>
</tr>
<tr class="odd">
<td><p>1990年6月</p></td>
<td><p><a href="../Page/開心鬼救開心鬼.md" title="wikilink">開心鬼救開心鬼</a></p></td>
<td><p><strong>Behind樂隊</strong></p></td>
<td><p><strong>樂隊主演電影</strong><br />
主題曲《戰勝心魔》及插曲《文武英傑宣言》收錄於1989年EP《<a href="../Page/戰勝心魔.md" title="wikilink">戰勝心魔</a>》內<br />
歌曲《戰勝心魔》同時收錄於1990年專輯《<a href="../Page/命運派對.md" title="wikilink">命運派對</a>》內</p></td>
</tr>
<tr class="even">
<td><p>文</p></td>
<td><p>傑</p></td>
<td><p>英</p></td>
<td><p>武</p></td>
</tr>
<tr class="odd">
<td><p>1991年7月</p></td>
<td><p><a href="../Page/Beyond日記之莫欺少年窮.md" title="wikilink">Beyond日記之莫欺少年窮</a></p></td>
<td><p>吳駒</p></td>
<td><p>譚貫中</p></td>
</tr>
<tr class="even">
<td><p>1991年11月</p></td>
<td><p><a href="../Page/豪門夜宴_(1991年電影).md" title="wikilink">豪門夜宴</a></p></td>
<td><p>樂隊客串演出</p></td>
<td><p>香港演藝界為<a href="../Page/1991年華東水災.md" title="wikilink">1991年華東水災籌款的電影</a><br />
樂隊以結他及鼓為粵曲《<a href="../Page/帝女花.md" title="wikilink">帝女花</a>》 伴奏</p></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/忍者龜II.md" title="wikilink">忍者龜II</a></p></td>
<td><p>（配音）</p></td>
<td><p>（配音）</p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/籠民.md" title="wikilink">籠民</a></p></td>
<td><p>毛仔</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>2004年9月</p></td>
<td><p><a href="../Page/墨斗先生.md" title="wikilink">墨斗先生</a></p></td>
<td><p> </p></td>
<td><p>劫匪<br />
（客串）</p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/同門.md" title="wikilink">同門</a></p></td>
<td><p> </p></td>
<td><p>咖啡<br />
（幫派大佬）</p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/開心魔法.md" title="wikilink">開心魔法</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/狂舞派.md" title="wikilink">狂舞派</a></p></td>
<td><p> </p></td>
<td><p>太極大師</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/古惑仔：江湖新秩序.md" title="wikilink">古惑仔：江湖新秩序</a></p></td>
<td><p> </p></td>
<td><p>鄧智勇<br />
（大佬B）</p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/末日派對.md" title="wikilink">末日派對</a></p></td>
<td><p> </p></td>
<td><p>健豪<br />
（警探）<br />
（<strong>主演</strong>）</p></td>
</tr>
</tbody>
</table>

## 歷年獎項及榮譽

### [無綫電視](../Page/無綫電視.md "wikilink")：[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink")

Beyond曾連續3年奪得「十大勁歌金曲」。

  - [1988年度勁歌金曲](../Page/1988年度十大勁歌金曲得獎名單.md "wikilink")（第四季[季選](../Page/勁歌金曲優秀選.md "wikilink")）：大地
  - [1988年度十大勁歌金曲頒獎典禮](../Page/1988年度十大勁歌金曲得獎名單.md "wikilink")
      - 十大勁歌金曲：大地
  - [1989年度勁歌金曲](../Page/1989年度十大勁歌金曲得獎名單.md "wikilink")（第二季季選）：真的愛妳
  - [1989年度十大勁歌金曲頒獎典禮](../Page/1989年度十大勁歌金曲得獎名單.md "wikilink")
      - 十大勁歌金曲：真的愛妳
  - [1989年度勁歌金曲](../Page/1989年度十大勁歌金曲得獎名單.md "wikilink")（第三季季選）：逝去日子
  - [1989年度勁歌金曲](../Page/1989年度十大勁歌金曲得獎名單.md "wikilink")（第四季季選）：歲月無聲
  - [1990年度勁歌金曲](../Page/1990年度十大勁歌金曲得獎名單.md "wikilink")（第一季季選）：午夜怨曲
  - [1990年度勁歌金曲](../Page/1990年度十大勁歌金曲得獎名單.md "wikilink")（第三季季選）：戰勝心魔
  - [1990年度勁歌金曲](../Page/1990年度十大勁歌金曲得獎名單.md "wikilink")（第四季季選）：光輝歲月
  - [1990年度十大勁歌金曲頒獎典禮](../Page/1990年度十大勁歌金曲得獎名單.md "wikilink")
      - 十大勁歌金曲：光輝歲月
      - 最佳填詞：光輝歲月（黃家駒）
  - [1991年度勁歌金曲](../Page/1991年度十大勁歌金曲得獎名單.md "wikilink")（第二季季選）：[Amani](../Page/Amani.md "wikilink")
  - [1992年度勁歌金曲](../Page/1992年度十大勁歌金曲得獎名單.md "wikilink")（第三季季選）：[長城](../Page/長城_\(Beyond\).md "wikilink")
  - [1993年度十大勁歌金曲](../Page/1993年度十大勁歌金曲得獎名單.md "wikilink")
      - 榮譽大獎：黃家駒

### [香港電台](../Page/香港電台.md "wikilink")：[十大中文金曲頒獎音樂會](../Page/十大中文金曲頒獎音樂會.md "wikilink")

Beyond曾奪得4首「十大中文金曲」。

  - 1987年度[第十屆十大中文金曲頒獎音樂會](../Page/第十屆十大中文金曲得獎名單.md "wikilink")
      - 最有前途新人獎（八強）：Beyond
  - 1989年度[第十二屆十大中文金曲頒獎音樂會](../Page/第十二屆十大中文金曲得獎名單.md "wikilink")
      - 十大中文金曲：真的愛妳
  - 1990年度[第十三屆十大中文金曲頒獎音樂會](../Page/第十三屆十大中文金曲得獎名單.md "wikilink")
      - 十大中文金曲：俾面派對
  - 1991年度[第十四屆十大中文金曲頒獎音樂會](../Page/第十四屆十大中文金曲得獎名單.md "wikilink")
      - 十大中文金曲：[Amani](../Page/Amani.md "wikilink")
  - 1993年度[第十六屆十大中文金曲頒獎音樂會](../Page/第十六屆十大中文金曲得獎名單.md "wikilink")
      - 十大中文金曲：[海闊天空](../Page/海闊天空.md "wikilink")
      - 無休止符紀念獎：黃家駒
  - 2002年度[第二十五屆十大中文金曲頒獎音樂會](../Page/第二十五屆十大中文金曲得獎名單.md "wikilink")
      - 金曲銀禧榮譽大獎：Beyond

### [商業電台](../Page/商業電台.md "wikilink")：[叱咤樂壇流行榜頒獎典禮](../Page/叱咤樂壇流行榜頒獎典禮.md "wikilink")

Beyond曾奪得7次「叱咤樂壇組合：銀獎」（1988-1995，1993年除外），並分別連續4年奪得「叱咤樂壇組合：金獎」（1996-1999）及「叱咤樂壇我最喜愛的組合：金獎」（1996-1999）。

  - [1988年度叱咤樂壇流行榜頒獎典禮](../Page/1988年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：舊日的足跡、天真的創傷、赤紅熱血、喜歡妳、**衝開一切**、**大地**）
  - [1989年度叱咤樂壇流行榜頒獎典禮](../Page/1989年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：午夜迷牆、逝去日子、爆裂都市、原諒我今天、歲月無聲、無悔這一生、**真的愛妳**）
  - [1990年度叱咤樂壇流行榜頒獎典禮](../Page/1990年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：無悔這一生、午夜怨曲、送給不知怎去保護環境的人(包括我)、灰色軌跡、戰勝心魔、俾面派對、懷念您、兩顆心、無淚的遺憾、**光輝歲月**）
  - [1991年度叱咤樂壇流行榜頒獎典禮](../Page/1991年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：無淚的遺憾、可知道、[Amani](../Page/Amani.md "wikilink")、不再猶豫）
  - [1992年度叱咤樂壇流行榜頒獎典禮](../Page/1992年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：遙望、不可一世、繼續沉醉、點解．點解、**半斤八兩**、**[長城](../Page/長城_\(Beyond\).md "wikilink")**）
  - [1993年度叱咤樂壇流行榜頒獎典禮](../Page/1993年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇我最喜愛的本地創作歌曲大獎：[海闊天空](../Page/海闊天空.md "wikilink")
  - [1994年度叱咤樂壇流行榜頒獎典禮](../Page/1994年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：遙遠的Paradise、醒你、打救你、總有愛）
  - [1995年度叱咤樂壇流行榜頒獎典禮](../Page/1995年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：銀獎（上榜歌曲：教壞細路、嘆息、困獸鬥、Love、**缺口**、**門外看**）
  - [1996年度叱咤樂壇流行榜頒獎典禮](../Page/1996年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：金獎（上榜歌曲：**想你**、**活著便精彩**、**太空**）
      - 叱咤樂壇我最喜愛的組合：Beyond
  - [1997年度叱咤樂壇流行榜頒獎典禮](../Page/1997年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：金獎（上榜歌曲：**預備**、**誰命我名字**、**回家**、**霧**）
      - 叱咤樂壇我最喜愛的組合：Beyond
      - 叱咤樂壇十周年大獎：Beyond
      - 叱咤殿堂十大作曲人：黃家駒
  - [1998年度叱咤樂壇流行榜頒獎典禮](../Page/1998年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：金獎（上榜歌曲：別怪我、喜歡一個人、犧牲、**霧**、**驚喜**、**奉信**）
      - 叱咤樂壇我最喜愛的組合：Beyond
  - [1999年度叱咤樂壇流行榜頒獎典禮](../Page/1999年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇組合：金獎（上榜歌曲：不見不散、扯火、**犧牲**、**Good Time**）
      - 叱咤樂壇我最喜愛的組合：Beyond
  - [2000年度叱咤樂壇流行榜頒獎典禮](../Page/2000年度叱咤樂壇流行榜頒獎典禮得獎名單.md "wikilink")
      - 叱咤樂壇我最喜愛的組合：Beyond

### [新城電台](../Page/新城電台.md "wikilink")：[新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")

  - [1994年度新城勁爆頒獎禮](../Page/1994年度新城勁爆頒獎禮得獎名單.md "wikilink")
      - 香港勁爆搖滾歌曲：醒你
      - 大學生眼中最受歡迎樂隊：Beyond
      - 香港勁爆樂隊╱組合：Beyond
  - [1995年度新城勁爆頒獎禮](../Page/1995年度新城勁爆頒獎禮得獎名單.md "wikilink")
      - 香港勁爆搖滾歌曲：教壞細路
  - [1996年度新城勁爆頒獎禮](../Page/1996年度新城勁爆頒獎禮得獎名單.md "wikilink")
      - 搖滾歌曲獎：活着便精彩
  - [1997年度新城勁爆頒獎禮](../Page/1997年度新城勁爆頒獎禮得獎名單.md "wikilink")
      - 新城勁爆組合：Beyond
      - 新城勁爆搖滾：請將手放開
  - [1998年度新城勁爆頒獎禮](../Page/1998年度新城勁爆頒獎禮得獎名單.md "wikilink")
      - 新城勁爆組合：Beyond
  - [1999年度新城勁爆頒獎禮](../Page/1999年度新城勁爆頒獎禮得獎名單.md "wikilink")
      - 新城勁爆組合：Beyond

### [香港電影金像獎](../Page/香港電影金像獎.md "wikilink")

  - 1990年度[第九屆香港電影金像獎](../Page/第9屆香港電影金像獎.md "wikilink")
      - 最佳電影歌曲（提名）：午夜迷牆《[黑色迷牆](../Page/黑色迷牆.md "wikilink")》
  - 2004年度[第廿四屆香港電影金像獎](../Page/第24屆香港電影金像獎.md "wikilink")
      - 最佳原創電影歌曲：長空《[無間道II](../Page/無間道II.md "wikilink")》

### 其他

  - 1983年《結他雜誌》Guitar Players Festival 結他大賽：冠軍\[27\]
  - 1994年第一屆香港唱片設計大賞
      - 傳媒最熱愛封面歌手：[二樓後座](../Page/二樓後座.md "wikilink")
  - 1996年度[有線至尊榜總選](../Page/有線至尊榜總選.md "wikilink")
      - 至尊廣東歌：想你
      - 至尊最愛MTV獎
      - 至尊國語歌：活得精彩
  - 2005年華語音樂傳媒大獎：華語樂壇特別貢獻樂隊
  - 2010年華語金曲獎30年經典評選：30年30人、30年30碟、30年30歌

## 評價

###

  - [劉卓輝](../Page/劉卓輝.md "wikilink")：去了趟非洲回來就能寫出《[光輝歲月](../Page/光輝歲月.md "wikilink")》和《[AMANI](../Page/AMANI.md "wikilink")》這麼好的歌曲，除了天才，還能說什麼。

###

  - [水木年華](../Page/水木年華.md "wikilink")：BEYOND的音樂影響了幾代人，我們上學的時候偶像是BEYOND，我們很喜愛他們的歌。1993年是我們最傷心的一年，因為我們的偶像黃家駒離我們遠去，當我們聽到BEYOND要解散的消息時很心痛。家駒當時的創作條件很艱苦，只能工作一年取得一點經驗和經濟基礎來創作音樂，我們很欽佩。2003年，在首都體育場，「BEYOND二十年」北京演唱會，全場觀眾一起用粵語從頭唱到尾，我相信這是任何一個台灣歌手、香港歌手都做不到的。很多歌手靠很高的曝光率維持人氣，而BEYOND在二十年後殺回北京，還有這麼多人喜歡，足以證明他們的魅力。
  - 趙明義（[黑豹樂隊鼓手](../Page/黑豹樂隊.md "wikilink")）
    ：BEYOND是一支非常了不起的樂隊，很多人喜歡他們的音樂，更欣賞他們的精神。
    1991年的時候，黑豹最初到香港發展，和BEYOND簽的是同一家經紀公司，所以我們已經是10多年的朋友了。BEYOND從來不介意人們爭論他們算不算搖滾，但是我覺得他們是最搖滾的，尤其是個性。
  - [王小峰](../Page/王小峰.md "wikilink")：2013年6月21日發文[《Beyond：撒了一點人文佐料的心靈雞湯》](http://www.lifeweek.com.cn/2013/0621/41155.shtml)評價了beyond的音樂，稱「從大眾文化中尋找心靈慰藉是70後有別於60後的特徵。80後沒什麼心靈，所以也無所謂雞湯。90後無所謂心靈雞湯，只要不是白開水就行」，引發爭議。
    　　

###

  - [羅大佑曾在](../Page/羅大佑.md "wikilink")《黃家駒為什麼會死》這本書中寫到
    ：「不會再出一個黃家駒了，這樣的人降臨人世本來就是奇跡，上帝讓黃家駒下凡，但是凡人沒有珍惜他，反而謾罵他，詛咒他，結果，上帝把黃家駒收回了。上帝不會再派一個音樂天使下凡的。」

###

  - Funky：BEYOND的音樂至今對中國做樂隊的後輩影響都非常大，我接觸過很多樂隊，都是聽BEYOND長大的，我覺得他們就是中國的甲殼蟲樂隊。前兩個月，我去香港看BEYOND的告別演唱會，看完後，我對他們說，這是你們這些年來演出最搖滾的一次。

###

  - [曹格](../Page/曹格.md "wikilink")：黃家駒真的是一個很有才華的人，他不僅是一個歌手，更是一位詩人，他這麼早離開實在是太過可惜了，而且BEYOND在我心目中就是華人世界的[Beatles](../Page/Beatles.md "wikilink")！

## 參見

  - [黃家駒](../Page/黃家駒.md "wikilink")
  - [黃家強](../Page/黃家強.md "wikilink")
  - [黃貫中](../Page/黃貫中.md "wikilink")
  - [葉世榮](../Page/葉世榮.md "wikilink")
  - [鄧煒謙](../Page/鄧煒謙.md "wikilink")
  - [劉志遠](../Page/刘志远_\(香港音乐人\).md "wikilink")
  - [陳健添](../Page/陳健添.md "wikilink")
  - [高速啤機](../Page/高速啤機.md "wikilink")
  - [Beyond歌曲對照表](../Page/Beyond歌曲對照表.md "wikilink")
  - [黃家駒音樂作品](../Page/黃家駒音樂作品.md "wikilink")
  - [Amani](../Page/Amani.md "wikilink")、[長城](../Page/長城_\(Beyond\).md "wikilink")、[海闊天空](../Page/海闊天空_\(Beyond歌曲\).md "wikilink")
  - [香港 (合輯)](../Page/香港_\(合輯\).md "wikilink")
  - [四川衛視侮辱黃家駒事件](../Page/四川衛視侮辱黃家駒事件.md "wikilink")

## 附註

<references/>

## 外部連結

1.  微博 - [黃貫中的微博](http://weibo.com/u/1730454584)
2.  微博 - [黃家強的微博](http://weibo.com/stevewkk)
3.  微博 - [葉世榮的微博](http://weibo.com/yezhirong)
4.  微博 - [香港二樓後座錄音室的微博](http://weibo.com/u/5261357540)

[Category:1983年成立的音樂團體](../Category/1983年成立的音樂團體.md "wikilink")
[Category:2005年解散的音樂團體](../Category/2005年解散的音樂團體.md "wikilink")
[Category:已解散的男子演唱團體](../Category/已解散的男子演唱團體.md "wikilink")
[Category:Beyond](../Category/Beyond.md "wikilink")
[Category:香港摇滚乐团](../Category/香港摇滚乐团.md "wikilink")
[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[Category:香港音樂](../Category/香港音樂.md "wikilink")

1.  《我與BEYOND的日子》2010年 鄧煒謙著

2.  Beyond官方自傳《擁抱Beyond歲月》1998年 黃貫中、黃家強、葉世榮著

3.
4.
5.

6.
7.
8.
9.
10.
11.
12. 《黃家駒逝世15週年專輯香港樂壇憶念家駒15年》《蘋果日報》C10-C11版，2008年6月10日

13.

14. [黃家強爆黃貫中8宗罪　揭Beyond解散真相](http://ent.appledaily.com.tw/realtimenews/article/entertainment/20141011/485534/)

15. [黃家強：Beyond已緣盡](http://www.msn.com/zh-hk/entertainment/topnews/%e9%bb%83%e5%ae%b6%e5%bc%b7beyond%e5%b7%b2%e7%b7%a3%e7%9b%a1/ar-BBkddqN)


16. [歌詞貼切\!
    　Beyond「海闊天空」成佔中曲](http://www.nexttv.com.tw/news/realtime/international/11151047/privacy),
    [壹電視](../Page/壹電視.md "wikilink"), 2014/09/30

17. [BEYOND-海闊天空](http://www.appledaily.com.tw/realtimenews/article/entertainment/20141001/479647/),
    [蘋果日報 (台灣)](../Page/蘋果日報_\(台灣\).md "wikilink"), 2014年10月1日

18. [因為不羈放縱愛自由　Beyond《海闊天空》成佔中主題曲](http://www.ettoday.net/news/20140930/407523.htm?from=easynews),
    [ETtoday](../Page/ETtoday.md "wikilink"), 2014/09/30

19. [香港佔中／「佔中」打死不退！　空拍13萬人遍地開花](http://news.tvbs.com.tw/entry/548590),
    [TVBS](../Page/TVBS.md "wikilink"), 2014/09/30

20. [Beyond挺！
    黃貫中與民眾唱「海闊天空」](http://news.ebc.net.tw/apps/newsList.aspx?id=1412131789)
    , [東森新聞](../Page/東森新聞.md "wikilink"), 2014/10/1

21. [「風雨中抱緊自由」
    香港佔中大V隱秘發聲](http://m.ntdtv.com/xtr/mb5/2014/10/01/a1142560.html),
    [新唐人](../Page/新唐人.md "wikilink"), 2014年10月1日

22. 《真的BEYOND日子》2013年 陳健添著

23.
24.
25. [香港電台 - 文藝港台 (第5集)
    - 1987年《小說家族》之《對倒》](http://podcast.rthk.hk/podcast/item_epi.php?pid=737&id=46471)

26. [香港電台 - 「光影我城」II (第28集)
    - 1987年《暴風少年》之《黑仔強》](http://podcast.rthk.hk/podcast/item_epi.php?pid=847&id=55022)

27.