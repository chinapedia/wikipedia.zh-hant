[thumb](../Page/file:Zeya.png.md "wikilink")

**结雅河**（[俄罗斯语](../Page/俄罗斯语.md "wikilink")：；；[英语](../Page/英语.md "wikilink")：Zeya），中国古称**精奇里江**，是[俄罗斯远东地区](../Page/俄罗斯远东地区.md "wikilink")[阿穆尔州](../Page/阿穆尔州.md "wikilink")[河流](../Page/河流.md "wikilink")，[黑龙江左岸最大](../Page/黑龙江.md "wikilink")[支流](../Page/支流.md "wikilink")。发源于[外兴安岭](../Page/外兴安岭.md "wikilink")，长1242千米，[流域面积](../Page/流域.md "wikilink")23.3万平方公里。結雅河本身亦有8條支流。河上建有大型[水库](../Page/水库.md "wikilink")[結雅水庫](../Page/結雅水庫.md "wikilink")。每年11月到5月期间封冻。其余时间可通航。

## 參見

  - [結雅水庫](../Page/結雅水庫.md "wikilink")

## 註釋

## 參考文獻

  -
  -
## 外部連結

  - [](http://slovari.yandex.ru/dict/geography/article/geo/geo1/geo-1644.htm)

[Category:俄罗斯河流](../Category/俄罗斯河流.md "wikilink")
[Category:黑龙江河流](../Category/黑龙江河流.md "wikilink")