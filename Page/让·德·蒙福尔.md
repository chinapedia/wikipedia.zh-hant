[Armoiries_Jean_de_Montfort.png](https://zh.wikipedia.org/wiki/File:Armoiries_Jean_de_Montfort.png "fig:Armoiries_Jean_de_Montfort.png")
**让·德·蒙福尔**（[法语](../Page/法语.md "wikilink")：Jean de
Montfort，1295年\~1345年9月16日，自稱**布列塔尼的約翰四世**）[法国贵族](../Page/法国.md "wikilink")，[蒙福尔伯爵](../Page/蒙福尔.md "wikilink")（1322年\~1345年）和未被公认的[布列塔尼公爵](../Page/布列塔尼公爵.md "wikilink")（1341年起）。

让·德·蒙福尔是[布列塔尼公爵](../Page/布列塔尼公爵.md "wikilink")[阿尔蒂尔二世与其第二个妻子蒙福尔女伯爵](../Page/阿尔蒂尔二世.md "wikilink")[尤兰德·德·蒙福尔所生的儿子](../Page/尤兰德·德·蒙福尔.md "wikilink")。1322年，他继承了母亲的领地[蒙福尔伯国](../Page/蒙福尔伯国.md "wikilink")。
1329年，他与[佛兰德公主](../Page/佛兰德.md "wikilink")[佛兰德的让娜结婚](../Page/佛兰德的让娜.md "wikilink")。他们生了两个孩子：

1.  [约翰五世](../Page/约翰五世_\(布列塔尼\).md "wikilink")
2.  让娜

在他同父异母的兄长[约翰三世公爵于](../Page/约翰三世_\(布列塔尼\).md "wikilink")1341年无嗣而终后，让·德·蒙福尔与约翰三世已故胞弟居伊之女[让娜·德·庞蒂埃夫勒之间为争夺布列塔尼而爆发了武装冲突](../Page/让娜·德·庞蒂埃夫勒.md "wikilink")（所谓[布列塔尼爵位继承战争](../Page/布列塔尼爵位继承战争.md "wikilink")）。他的主要支持者是[英格兰国王](../Page/英格兰.md "wikilink")[爱德华三世](../Page/爱德华三世.md "wikilink")。让·德·蒙福尔的斗争毫无成效，但在他去世后，他的儿子约翰五世最后因為1364年[歐賴戰役的勝利](../Page/歐賴戰役.md "wikilink")，於1365年成为[布列塔尼公爵](../Page/布列塔尼公爵.md "wikilink")。

[A](../Category/1295年出生.md "wikilink")
[A](../Category/1345年逝世.md "wikilink")
[Category:布列塔尼公爵](../Category/布列塔尼公爵.md "wikilink")