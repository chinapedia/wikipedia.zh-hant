**井手町**（）是位於[京都府南部的行政區劃](../Page/京都府.md "wikilink")，位於的右岸，主要市區位於轄內西側靠木津川的平地，東半部為山林地，約轄區七成面積。\[1\]\[2\]

## 歷史

在[江戶時代現在的井手町範圍多屬皇室的領地](../Page/江戶時代.md "wikilink")，[明治維新後被劃入](../Page/明治維新.md "wikilink")[京都府](../Page/京都府.md "wikilink")；在1889年實施[町村制時](../Page/町村制.md "wikilink")，現在的轄區南半部屬井手村，北半部則屬。

1927年井手村先改制為**井手町**，1958年井手町和多賀村[合併](../Page/市町村合併.md "wikilink")，成為現在的井手町。\[3\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 现在</p></th>
<th><p>现在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>井手村</p></td>
<td><p>1927年1月1日<br />
井手町</p></td>
<td><p>1958年4月1日<br />
井手町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>多贺村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[奈良線以南北向穿過主要市區](../Page/奈良線.md "wikilink")。

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）
      - [奈良線](../Page/奈良線.md "wikilink")：（←[城陽市](../Page/城陽市.md "wikilink")）
        -  -  - （[木津川市](../Page/木津川市.md "wikilink")）

<File:Yamashiro> Taga Station west entrance.jpg| 山城多賀車站 <File:Tamamizu>
Station.jpg| 玉水車站

## 參考資料

## 相關條目

## 外部連結

1.
2.
3.