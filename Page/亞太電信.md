[Aptg_telecom_co.ltd.jpg](https://zh.wikipedia.org/wiki/File:Aptg_telecom_co.ltd.jpg "fig:Aptg_telecom_co.ltd.jpg")
[APT_Taipei_Syntrend_Store_20190112.jpg](https://zh.wikipedia.org/wiki/File:APT_Taipei_Syntrend_Store_20190112.jpg "fig:APT_Taipei_Syntrend_Store_20190112.jpg")1樓亞太電信臺北三創店\]\]
**亞太電信股份有限公司**（[英語](../Page/英語.md "wikilink")：**Asia Pacific
Telecom**，[縮寫](../Page/縮寫.md "wikilink")：****，簡稱**亞太電**），是台灣五大電信業者之一。原名為亞太固網寬頻股份有限公司，成立於2000年。擁有寬頻固網（Broadband
Fixed Lines）、寬頻行動通信（Broadband Wireless）與寬頻網際網路（Broadband
Internet）三大寬頻事業。

## 歷史沿革

### 早期歷史

[Eastern_Broadband_Telecom_landline_tag.jpg](https://zh.wikipedia.org/wiki/File:Eastern_Broadband_Telecom_landline_tag.jpg "fig:Eastern_Broadband_Telecom_landline_tag.jpg")
1998年，[力霸企業集團成立](../Page/力霸企業集團.md "wikilink")「力霸電信籌備處」，籌備固網投標事宜。

1999年12月，「力霸電信籌備處」更名為「東森寬-{}-頻電信股份有限公司籌備處」並參與投標。

### 東森寬頻電信

[Taipei_Guangfu_Service_Center,_Ambit_Corporation_20150912.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Guangfu_Service_Center,_Ambit_Corporation_20150912.jpg "fig:Taipei_Guangfu_Service_Center,_Ambit_Corporation_20150912.jpg")

東森寬頻電信是由[力霸企業集團負責籌組](../Page/力霸企業集團.md "wikilink")，並結合[台灣鐵路管理局與國內外三十多家製造業者](../Page/台灣鐵路管理局.md "wikilink")、[金融機構](../Page/金融機構.md "wikilink")、專業[創投公司及知名企業集團合資](../Page/創投公司.md "wikilink")，為[中華民國首家完成公司設立並取得第一張營運特許執照之民營固網業者](../Page/中華民國.md "wikilink")。

2000年3月，東森寬-{}-頻電信籌備處申請經營「固定通信綜合網路業務」，通過[交通部電信總局審查合格](../Page/交通部電信總局.md "wikilink")。

2000年5月5日，「**東森寬頻電信股份有限公司**」（Eastern Broadband Telecommunications Co.,
Ltd.）正式成立，簡稱EBT。同年12月入主「**亞太線上股份有限公司**」（APOL），與[亞太線-{}-上](../Page/亞太線上.md "wikilink")（APOL）簽訂投資協議，由[仲琦科技釋出亞太線](../Page/仲琦科技.md "wikilink")-{}-上70%股權予東森寬-{}-頻電信；東森寬-{}-頻電信因此取得亞太線上70%股份，跨足第二類電信事業。

2001年，東森寬頻電信轉投資成立「**亞太行動寬頻電信股份有限公司**」（APBW）參與[3G執照競標](../Page/3G.md "wikilink")。同年1月，東森寬-{}-頻電信[總經理王令台及執行副總經理](../Page/總經理.md "wikilink")[鄭俊卿分別接任亞太線上](../Page/鄭俊卿.md "wikilink")[董事長及總經理](../Page/董事長.md "wikilink")，並取得台灣第一張民營電信固網執照（[3G執照](../Page/3G.md "wikilink")，採用[CDMA2000系統](../Page/CDMA2000.md "wikilink")）。\[1\]

2001年3月，東森寬-{}-頻電信宣布開台營運，提供「005國際直撥電話」、「1805國內長途電話」、「Sweet
Card隨意打國際電話卡」、[ADSL寬頻上網](../Page/ADSL.md "wikilink")、[數據專線](../Page/數據專線.md "wikilink")、[虛擬私人網路](../Page/虛擬私人網路.md "wikilink")（VPN）及[數據中心](../Page/數據中心.md "wikilink")（IDC）等全方位寬頻電信服務。

2001年11月，東森寬-{}-頻電信轉投資成立「亞太行動寬-{}-頻電信股份有限公司」（Asia Pacific Broadband
Wireless Communications,
Inc.），簡稱APBW，公司成立實收資本額為新台幣160億2仟萬元，參與國內第三代行動通信業務執照競標作業。

2002年2月，東森寬-{}-頻電信旗下的亞太行動寬-{}-頻以新台幣105億7仟萬元標得第三代行動通信服務（[3G](../Page/3G.md "wikilink")）執照中頻寬最大的編號E執照。

2002年7月，東森寬-{}-頻電信與[和信超媒體簽定意向書](../Page/和信超媒體.md "wikilink")，東森寬-{}-頻電信擬以新台幣4.5億元收購和信超媒體旗下14萬寬頻網路用戶及網路設備；但2002年10月6日，由於東森寬-{}-頻電信無法獲得[和信企業團旗下](../Page/和信企業團.md "wikilink")12家[有線電視系統業者的支持](../Page/有線電視.md "wikilink")，此收購案宣告破局。

2002年12月，東森寬-{}-頻電信旗下的亞太行動寬-{}-頻與[鮮京電信合作推動第三代行動通信服務商用化](../Page/鮮京電信.md "wikilink")。

2003年3月31日，旗下之[亞太網際網路交換中心](../Page/亞太網際網路交換中心.md "wikilink")(Eastern
Broadband Internet
Exchange，[縮寫](../Page/縮寫.md "wikilink")：EBIX）通過[TWNIC認可IX之審核](../Page/TWNIC.md "wikilink")\[2\]，為台灣地區繼[台灣網際網路交換中心](../Page/台灣網際網路交換中心.md "wikilink")(TWIX)、[台北網際網路交換中心](../Page/台北網際網路交換中心.md "wikilink")(TPIX)及[中華民國網際網路交換中心](../Page/中華民國網際網路交換中心.md "wikilink")(TWNAP)後成立的第四家網際網路交換中心。

2003年7月，亞太行動寬-{}-頻開始提供第三代行動通信服務，[行動電話系統為](../Page/行動電話.md "wikilink")[CDMA2000](../Page/CDMA2000.md "wikilink")，為[台灣第一家開台營運的第三代行動通信服務業者](../Page/台灣.md "wikilink")。

### 亞太電信集團

**2004年**

  - 6月，東森寬-{}-頻電信[股東大會通過公司更名案](../Page/股東大會.md "wikilink")，決議更名為「亞太固網寬-{}-頻股份有限公司」（Asia
    Pacific Broadband Telecom Co., Ltd.），簡稱APBT。
  - 8月，亞太固網寬-{}-頻結合亞太行動寬-{}-頻及亞太線-{}-上成立「亞太電信集團」（Asia Pacific Telecom
    Group），簡稱APTG。集團成員包含亞太固網、亞太行動（亞太固網持有99.88%股權）及亞太線上（亞太固網持有73.32%股權）。

**2006年**

  - 8月1日，亞太線-{}-上「線上遊戲事業部」獨立為子公司「猿人在-{}-線股份有限公司」（Asia Pacafic
    Entertainment
    Networks），簡稱APE，首任董事長為[王令興](../Page/王令興.md "wikilink")。

**2007年**

  - 5月：亞太固網寬-{}-頻董事會通過亞太行動寬-{}-頻與亞太固網寬-{}-頻二合一簡易合併案。
  - 6月，亞太行動與亞太固網進行合併作業。
  - 9月：亞太固網寬-{}-頻以「**亞太電信**」[品牌繼續提供固網寬頻與行動寬頻服務](../Page/品牌.md "wikilink")，並以「為你做更好」（Making
    Better
    Changes）為品牌精神，[企業識別系統也由過去的](../Page/企業識別.md "wikilink")「APTG」全面更新為「A+」。\[3\]
  - 10月：亞太固網寬-{}-頻更名為「**亞太電信股份有限公司**」（政府接管後最大股東為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")，但暫委由[東元電機經營](../Page/東元電機.md "wikilink")）。\[4\]

**2008年**

  - 3月：猿人在-{}-線歇業，併回亞太線-{}-上。

**2010年**

  - 2月：亞太電信與[酷派簽定策略合作備忘錄](../Page/酷派.md "wikilink")。
  - 7月：亞太電信推出「全球漫遊一碼通」。

**2011年**

  - 5月，亞太電信合併亞太線-{}-上。12月1日，亞太電信股票登錄興櫃交易，[股票代號](../Page/股票代號.md "wikilink")3682。

**2012年**

  - 10月：亞太電信[EV-DO高速行動數據網路服務正式商業運轉](../Page/EV-DO.md "wikilink")。

**2013年**

  - 3月27日：亞太電信向[臺灣證券交易所申請](../Page/臺灣證券交易所.md "wikilink")[股票上市](../Page/股票上市.md "wikilink")。
  - 8月5日：亞太電信股票以每股新台幣12.3元在[臺灣證券交易所正式掛牌上市](../Page/臺灣證券交易所.md "wikilink")，亞太電信成為台灣第四個股票上市的電信業者，與[中華電信](../Page/中華電信.md "wikilink")、[台灣大哥大及](../Page/台灣大哥大.md "wikilink")[遠傳電信稱為](../Page/遠傳電信.md "wikilink")「電信四雄」。

**2014年**

  - 5月26日：亞太電信與[鴻海集團同時宣布雙方簽訂合作意向書](../Page/鴻海集團.md "wikilink")，鴻海集團旗下的[國碁電子以策略性投資人身分參與亞太電信的私募認股](../Page/國碁電子.md "wikilink")，國碁電子必須在私募案通過後1年內引介經亞太電信認可的國際級策略性投資人以相同條件認購其餘3成股權，最後國碁電子與亞太電信以換股方式進行吸收合併，合併後以亞太電信為存續公司\[5\]。
  - 7月：鴻海集團副總裁[呂芳銘接任亞太電信董事長](../Page/呂芳銘.md "wikilink")。
  - 8月29日：亞太電信召開股東臨時會，通過與國碁電子合併案，合併基準日暫定為2015年6月底\[6\]。
  - 9月：亞太電信與台灣大哥大策略聯盟，將共享網路資源。
  - 10月：亞太電信獲得NCC核發700MHz頻段之4G特許執照。
  - 12月9日：亞太電信發表企業識別標誌「Gt 4G」，並展開4G試營運。

**2015年**

  - 12月31日：亞太電信與國碁電子完成合併，以亞太電信為存續公司\[7\]。

## 服務項目

### 行動通信服務

#### [行動寬頻業務(4G)](../Page/4G.md "wikilink")

2014年12月24日開始提供服務\[8\]，採用[LTE系統](../Page/LTE.md "wikilink")，特許有效期間至2030年12月31日止。\[9\]採用多標準無線技術（Multi-Standard
Radio）利用900MHz中5MHz頻寬提供[2G](../Page/2G.md "wikilink")
[GSM語音服務](../Page/GSM.md "wikilink")。

##### 服務門號(4G)

提供之行動寬頻業務(4G)服務，門號区间可參考以下列表\[10\]：

列表中所列出之门号区间，前方开头之数字为固定号码，**\#**及**%**号可分配、调整之范围请见备注，**\***号代表其可分配、调整之范围为0至9；

除以下列表之门号，亦可保留其他业者所分配之门号，更换服务业者为亚太电信；或将下列表之门号由亚太电信更换为其它业者，此服务称为[号码可携](../Page/携号转网.md "wikilink")。

<table style="width:64%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 8%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th><p>門號(前4碼)[11]</p></th>
<th><p>門號(後6碼)</p></th>
<th><p>門號長度</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0906</p></td>
<td><ul>
<li>******</li>
</ul></td>
<td><p>10位數</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>0907</p></td>
<td><ul>
<li>******</li>
</ul></td>
<td><p>10位數</p></td>
<td><ul>
<li>該門號區間為原国碁电子持有，其與亞太電信合併後門號被持續使用。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>0968</p></td>
<td><ul>
<li>#*****</li>
</ul></td>
<td><p>10位數</p></td>
<td><ul>
<li>#可分配之範圍從2至5。</li>
<li>#從0至1范围为<a href="../Page/远传电信.md" title="wikilink">远传电信持有</a>。</li>
<li>#從6至9范围为<a href="../Page/台湾之星.md" title="wikilink">台湾之星持有</a>。</li>
<li>該門號區間為原<a href="../Page/大眾電信.md" title="wikilink">大眾電信持有</a>，終止服務後門號被分給5家業者。</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

##### LTE頻段

  - FDD-LTE 700Mhz(Band28) (A1 10Mhz + A3 5Mhz)
  - FDD-LTE 900Mhz(Band8) (B3 10Mhz)
  - TDD-LTE 2600Mhz(Band38) (D5 25Mhz)

#### [第三代行動通信(3G)](../Page/3G.md "wikilink")

2003年7月28日開始提供服務\[12\]，開台時為**東森寬頻電信**之子公司**亞太行動寬頻電信**提供服務。
採用[CDMA](../Page/CDMA.md "wikilink")2000系統，為2002年發放第三代行動通信執照(A至E)中之E執照(核配頻率825MHz至845MHz及870MHz至890MHz)，原特許有效期間為2002年2月7日起至2018年12月31日止。4G行動寬頻業務第一次競標後，885MHz至890MHz頻段由[台灣之星得標](../Page/台灣之星.md "wikilink")(需待亞太電信執照到期後才能取得)，2015年11月13日兩家業者協議並經[國家通訊傳播委員會同意](../Page/國家通訊傳播委員會.md "wikilink")，3G
CDMA2000業務提前於2018年1月1日起終止，[台灣之星提早取得使用權](../Page/台灣之星.md "wikilink")。\[13\]\[14\]

##### 服務門號(3G)

提供之第三代行動通信服務，門號区间可參考以下列表\[15\]：

列表中所列出之门号区间，前方开头之数字为固定号码，**\#**及**%**号可分配、调整之范围请见备注，**\***号代表其可分配、调整之范围为0至9；

除以下列表之门号，亦可保留其他业者所分配之门号，更换服务业者为亚太电信；或将下列表之门号由亚太电信更换为其它业者，此服务称为[号码可携](../Page/携号转网.md "wikilink")。

<table style="width:64%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 8%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th><p>門號(前4碼)[16]</p></th>
<th><p>門號(後6碼)</p></th>
<th><p>門號長度</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0977</p></td>
<td><ul>
<li>#*****</li>
</ul></td>
<td><p>10位數</p></td>
<td><ul>
<li>#可分配之範圍從0至7。</li>
<li>#從8至9范围未被分配給任何業者持有。</li>
<li>該門號區間內部分號碼業經<a href="../Page/中华民国国家通讯传播委员会.md" title="wikilink">NCC收回</a>。</li>
</ul></td>
</tr>
<tr class="even">
<td><p>0982</p></td>
<td><ul>
<li>******</li>
</ul></td>
<td><p>10位數</p></td>
<td><ul>
<li>該門號區間內部分號碼業經<a href="../Page/中华民国国家通讯传播委员会.md" title="wikilink">NCC收回</a>。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>0985</p></td>
<td><ul>
<li>******</li>
</ul></td>
<td><p>10位數</p></td>
<td><ul>
<li>該門號區間內部分號碼業經<a href="../Page/中华民国国家通讯传播委员会.md" title="wikilink">NCC收回</a>。</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 語音通信服務

#### 市內電話服務

固定式电话服务，为台湾地区继[中华电信后](../Page/中华电信.md "wikilink")，第二家提供市内电话之电信业者\[17\]\[18\]。

##### 服務門號(市內電話)

提供之市內電話，門號区间可參考以下列表\[19\]：

列表中所列出之门号区间，前方开头之数字为固定号码，**\#**及**%**号可分配、调整之范围请见备注，**\***号代表其可分配、调整之范围为0至9；

除以下列表之门号，亦可保留其他业者所分配之门号，更换服务业者为亚太电信；或将下列表之门号由亚太电信更换为其它业者，此服务称为[号码可携](../Page/携号转网.md "wikilink")。

405開頭之門號，爲全區統一撥接碼，僅可撥入無法撥出\[20\]；

有别于[行动电话拨打有分网内](../Page/行动电话.md "wikilink")(拨打至相同业者门号)与网外(拨打至不同业者门号)，市话电话服务无分网内与网外，例如[亚太电信](../Page/亚太电信.md "wikilink")[市内电话拨打](../Page/市内电话.md "wikilink")[亚太电信市内电话](../Page/亚太电信.md "wikilink")；或亚太电信市内电话拨打[台湾固网室内电话](../Page/台湾固网.md "wikilink")，费率相同。

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 2%" />
</colgroup>
<thead>
<tr class="header">
<th><p>區碼</p></th>
<th><p>區域[21]</p></th>
<th><p>服務縣市</p></th>
<th><p>收费区[22]</p></th>
<th><p>門號長度</p></th>
<th><p>門號区间</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>02</p></td>
<td><p>台北</p></td>
<td><ul>
<li><a href="../Page/台北市.md" title="wikilink">台北市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>8位數</p></td>
<td><ul>
<li>55#*****</li>
<li>405%****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從5至9</li>
<li>%可分配之範圍為0、1、5、8、9</li>
</ul></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/新北市.md" title="wikilink">新北市</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><ul>
<li><a href="../Page/基隆市.md" title="wikilink">基隆市</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>03</p></td>
<td><p>桃園</p></td>
<td><ul>
<li><a href="../Page/桃園市.md" title="wikilink">桃園市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>25#****</li>
<li>405****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至5</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>03</p></td>
<td><p>新竹</p></td>
<td><ul>
<li><a href="../Page/新竹市.md" title="wikilink">新竹市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>60#****</li>
<li>405****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至4</li>
</ul></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/新竹县.md" title="wikilink">新竹县</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>03</p></td>
<td><p>花蓮</p></td>
<td><ul>
<li><a href="../Page/花蓮縣.md" title="wikilink">花蓮縣</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>800****</li>
<li>405****</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p>03</p></td>
<td><p>宜蘭</p></td>
<td><ul>
<li><a href="../Page/宜蘭縣.md" title="wikilink">宜蘭縣</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>900****</li>
<li>405****</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p>037</p></td>
<td><p>苗栗</p></td>
<td><ul>
<li><a href="../Page/苗栗县.md" title="wikilink">苗栗县</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>6位數</p></td>
<td><ul>
<li>28****</li>
<li>40****</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/新竹县.md" title="wikilink">新竹县</a><a href="../Page/峨眉乡.md" title="wikilink">峨眉乡部分</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>04</p></td>
<td><p>台中</p></td>
<td><ul>
<li><a href="../Page/台中市.md" title="wikilink">台中市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>8位數</p></td>
<td><ul>
<li>350*****</li>
<li>405%****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至9</li>
<li>%可分配之範圍為0、1、5、8、9</li>
</ul></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/苗栗县.md" title="wikilink">苗栗县</a><a href="../Page/卓兰镇.md" title="wikilink">卓兰镇</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><ul>
<li><a href="../Page/南投县.md" title="wikilink">南投县</a><a href="../Page/仁爱乡.md" title="wikilink">仁爱乡部分</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/花莲县.md" title="wikilink">花莲县</a><a href="../Page/秀林乡.md" title="wikilink">秀林乡富世村关原地区</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>04</p></td>
<td><p>彰化</p></td>
<td><ul>
<li><a href="../Page/彰化縣.md" title="wikilink">彰化縣</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>80#****</li>
<li>405****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至4</li>
</ul></td>
</tr>
<tr class="even">
<td><p>049</p></td>
<td><p>南投</p></td>
<td><ul>
<li><a href="../Page/南投縣.md" title="wikilink">南投縣</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>500****</li>
<li>405****</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><ul>
<li><a href="../Page/彰化县.md" title="wikilink">彰化县</a><a href="../Page/芬园乡.md" title="wikilink">芬园乡</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/台中市.md" title="wikilink">台中市</a><a href="../Page/乌日区.md" title="wikilink">乌日区溪尾里</a>、<a href="../Page/新社区.md" title="wikilink">新社区福兴里部分</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>05</p></td>
<td><p>嘉义</p></td>
<td><ul>
<li><a href="../Page/嘉义市.md" title="wikilink">嘉义市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>300****</li>
<li>405****</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/嘉义县.md" title="wikilink">嘉义县</a>(<a href="../Page/六脚乡.md" title="wikilink">六脚乡及</a><a href="../Page/新港乡.md" title="wikilink">新港乡部分地区除外</a>)</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>05</p></td>
<td><p>云林</p></td>
<td><ul>
<li><a href="../Page/云林县.md" title="wikilink">云林县</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>550****</li>
<li>405****</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/嘉义县.md" title="wikilink">嘉义县</a><a href="../Page/六脚乡.md" title="wikilink">六脚乡及</a><a href="../Page/新港乡.md" title="wikilink">新港乡部分地区</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>06</p></td>
<td><p>台南</p></td>
<td><ul>
<li><a href="../Page/台南市.md" title="wikilink">台南市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>51#****</li>
<li>405****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至3</li>
</ul></td>
</tr>
<tr class="even">
<td><p>06</p></td>
<td><p>澎湖</p></td>
<td><ul>
<li><a href="../Page/澎湖县.md" title="wikilink">澎湖县</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>950****</li>
<li>405****</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p>07</p></td>
<td><p>高雄</p></td>
<td><ul>
<li><a href="../Page/高雄市.md" title="wikilink">高雄市</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>95****</li>
<li>405****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至9</li>
</ul></td>
</tr>
<tr class="even">
<td><p>08</p></td>
<td><p>屏东</p></td>
<td><ul>
<li><a href="../Page/屏东县.md" title="wikilink">屏东县</a></li>
</ul></td>
<td><p>第2级</p></td>
<td><p>7位數</p></td>
<td><ul>
<li>80#****</li>
<li>405****</li>
</ul></td>
<td><ul>
<li>#可分配之範圍從0至3</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>089</p></td>
<td><p>台东</p></td>
<td><ul>
<li><a href="../Page/台东县.md" title="wikilink">台东县</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>6位數</p></td>
<td><ul>
<li>96****</li>
<li>40****</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p>082</p></td>
<td><p>金门</p></td>
<td><ul>
<li><a href="../Page/金门县.md" title="wikilink">金门县</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>6位數</p></td>
<td><ul>
<li>50****</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p>0836</p></td>
<td><p>马祖</p></td>
<td><ul>
<li><a href="../Page/连江县.md" title="wikilink">连江县</a></li>
</ul></td>
<td><p>第1级</p></td>
<td><p>5位數</p></td>
<td><ul>
<li>6****</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 多功能受話方付費(0809)

有別於一般由撥打方電話支付通話費用，[受话方付费电话由門號申請者](../Page/受话方付费电话.md "wikilink")(即受話方)支付通話費用\[23\]，080開頭之門號僅可撥入無法撥出\[24\]\[25\]。

##### 服務門號(0809)

提供之多功能[受話方付費電話服務](../Page/受話方付費電話.md "wikilink")，門號区间可參考以下列表)\[26\]\[27\]：

列表中所列出之门号区间，前方开头之数字为固定号码，**\#**及**%**号可分配、调整之范围请见备注，**\***号代表其可分配、调整之范围为0至9；

除以下列表之门号，亦可保留其他业者所分配之门号，更换服务业者为亚太电信；或将下列表之门号由亚太电信更换为其它业者，此服务称为[号码可携](../Page/携号转网.md "wikilink")。

<table style="width:64%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 8%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th><p>門號(前4碼)</p></th>
<th><p>門號(後6碼)</p></th>
<th><p>門號長度</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0809</p></td>
<td><ul>
<li>#%****</li>
</ul></td>
<td><p>10位數</p></td>
<td><ul>
<li>#可分配之範圍從0至0。</li>
<li>%可分配之範圍從5至6。</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 付費語音服務(020)

拨打方需支付该通电话相对略高于一般电话之电信费用，門號申請者(即受話方)可从中分得利润，多用于专业咨询(例如法律咨询)、募款、电话投票等，020開頭之門號僅可撥入無法撥出\[28\]\[29\]。

##### 服務門號(020)

提供之多功能付費語音服務，門號区间可參考以下列表\[30\]\[31\]：

列表中所列出之门号区间，前方开头之数字为固定号码，**\#**及**%**号可分配、调整之范围请见备注，**\***号代表其可分配、调整之范围为0至9；

<table style="width:64%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 8%" />
<col style="width: 24%" />
</colgroup>
<thead>
<tr class="header">
<th><p>門號(前3碼)</p></th>
<th><p>門號(後6碼)</p></th>
<th><p>門號長度</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>020</p></td>
<td><ul>
<li>8#****</li>
</ul></td>
<td><p>9位數</p></td>
<td><ul>
<li>#可分配之範圍從0至0。</li>
</ul></td>
</tr>
<tr class="even">
<td><p>020</p></td>
<td><ul>
<li>9#****</li>
</ul></td>
<td><p>9位數</p></td>
<td><ul>
<li>#可分配之範圍從6至9。</li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 國際電話(005/015)

提供之[國際電話冠碼为](../Page/国际冠码列表.md "wikilink")005國際電話及015經濟型國際電話\[32\]\[33\]，用戶於任一業者之市內電話或行動電話，撥打國際電話前輸入该冠码，即可選擇由亞太電信之路由撥出該通国际電話\[34\]\[35\]\[36\]。

005\[37\]与015\[38\]两者之差异，在于前者计费单位为每6秒，后者为每分钟。

#### 國內長途電話(1805)

用戶於任一業者之市內電話(无法于行動電話使用)，撥打长途電話前輸入该冠码，即可選擇由亞太電信之路由撥出該通长途電話\[39\]\[40\]。

#### 其他語音通信服務

  - **PRA語音專線服務**
  - **即時多方通話服務**
  - **050一般費率服務**
  - **指定選接服務**
  - **Cloud Centrex雲端總機服務**

### 數據通信服務

  - **國內乙太專線服務**
  - **國內數據專線服務**
  - **Smart VPN企業虛擬網路服務**
  - **國際數據專線服務**
  - **企業國際虛擬網路**

### 寬頻網路服務

  - **Megalink寬頻上網服務**
  - **IAL企業專線上網服務**
  - **企業FTTX光纖上網服務**
  - **企業ADSL寬頻上務服務**
  - **IP Transit網路訊務轉送服務**
  - **情色守護**

### 網通解決方案

  - **UPM整合頻寬管理服務**
  - **ULB整合負載平衡服務**
  - **UTM資安防護管理服務**
  - **SSL VPN遠端安全存取服務**
  - **Gt BOX 雲儲存服務**
  - **Active OTP雲端行動憑證認證服務**
  - **i-MeetU外勤人員管理暨派遣服務**
  - **專利雲服務**
  - **IVA智能影像分析**

### IDC網路資訊

#### 網域名稱

可受理**[.tw](../Page/.tw.md "wikilink")**之[国家和地区顶级域名](../Page/国家和地区顶级域名.md "wikilink")(ccTLD)和**[.com](../Page/.com.md "wikilink")**、**[.org](../Page/.org.md "wikilink")**、**[.net](../Page/.net.md "wikilink")**、**[.biz](../Page/.biz.md "wikilink")**、**[.info](../Page/.info.md "wikilink")**、**[.cc](../Page/.cc.md "wikilink")**、**[.us](../Page/.us.md "wikilink")**、**[.asia](../Page/.asia.md "wikilink")**等[域名服务註冊申请](../Page/域名.md "wikilink")。[TWNIC自](../Page/台湾网路资讯中心.md "wikilink")2001年3月1日起，不再直接受理新申请**.tw**[域名服务注册](../Page/域名.md "wikilink")，改由授權之受理註冊機構\[41\]負責，亚太电信为授權之受理註冊機構之一。

由亚太电信管理之網域名稱，另提供有[DNS](../Page/DNS.md "wikilink")[代管服務](../Page/互联网托管服务.md "wikilink")。

#### 亞太網際網路交換中心

為台灣地區第四家成立的網際網路交換中心，設址於[台北市](../Page/台北市.md "wikilink")[南港區](../Page/南港區.md "wikilink")[南港軟體園區](../Page/南港軟體園區.md "wikilink")。

#### 其他IDC網路資訊服務

  - **主機代管及機房頻寬服務**
  - **虛擬主機**
  - **實體主機**
  - **企業郵件**
  - **防火牆服務**
  - **Anti-DDoS服務**

## 爭議及斷網事件

以下爭議事件及斷網事件依照發生順序列出：

### 爭議事件

  - 2014年12月26日(事件報導日期)，亞太電信宣佈與台灣大哥大策略聯盟，開始共用核心網路設備，因此引發其他業者不滿，擔心影響用戶權益，引起[國家通訊傳播委員會](../Page/國家通訊傳播委員會.md "wikilink")(NCC)介入調查\[42\]，2017年12月31日結束借網行為。

### 斷網事件

  - 2015年6月28日，[台灣大哥大在中午](../Page/台灣大哥大.md "wikilink")12時30分至下午2時5分，因網路異常突然沒有收訊，斷網95分鐘；台灣大哥大策略合作夥伴亞太電信的訊號也受到影響\[43\]。

<!-- end list -->

  - 2016年6月8日，台灣大哥大於上午9時19分因3G語音交換機軟體出現異常，出現部分訊號異常狀況，出現包括市話撥打台灣大用戶電話不通、變空號等狀況，異常狀況亦已經於上午11時7分排除，但有網友表示實際影響時間到下午4時都未完全恢復；與台灣大哥大共用網路的亞太電信也受到影響\[44\]。

## 參考資料

## 註釋

## 相關條目

  - [臺灣通訊業](../Page/臺灣通訊業.md "wikilink")
  - [台灣網路發展](../Page/台灣網路發展.md "wikilink")
  - [台灣電信業者列表](../Page/台灣電信業者列表.md "wikilink")
  - [網際網路服務提供者](../Page/網際網路服務提供者.md "wikilink")

## 外部連結

  - [Gt亞太電信](https://www.aptg.com.tw/)

  -
  - [亞太網際網路交換中心(EBIX，亞太電信所營運之網際網路交換中心)](http://www.ebix.net.tw/)

  - [台灣公司資料](https://company.g0v.ronny.tw/id/70771579)

  -
[0](../Category/臺灣證券交易所上市公司.md "wikilink")
[A](../Category/臺灣電信業公司.md "wikilink")
[A](../Category/移动电话运营商.md "wikilink")
[A](../Category/總部位於臺北市內湖區的工商業機構.md "wikilink")
[A](../Category/2000年成立的公司.md "wikilink")
[Category:鴻海集團](../Category/鴻海集團.md "wikilink")

1.  宋丁儀.[「電霸」一步一腳印──回顧東森集團從有線電視跨足固網的歷程傳學鬥論壇](http://twmedia.org/scstw/?p=144).傳學鬥電子報第34期.2001-02-20
2.  [EBIX 網際網路交換中心通過TWNIC
    IP委員會認可成為第四家通過認證之網際網路交換中心](http://www.myhome.net.tw/2003_06/twnic_news/main5.htm).台灣網路資訊中心.2003-06-01
3.  [A+亞太電信品牌再造重新出發『Making Better
    Changes為你做更好』](http://www.aptg.com.tw/aptg2/news/news_20070912.html).亞太電信新聞中心.2007-09-12

4.  [亞太固網股東常會通過減、增資案，正式更名亞太電信　重新再出發](http://www.aptg.com.tw/aptg2/news/news_20071026.html).亞太電信新聞中心.2007-10-26
5.
6.
7.
8.  [打造行動智慧家庭，亞太電信 4G 開台](https://share.inside.com.tw/posts/12072)
9.  [頻率資料庫查詢系統](https://freqdbo.ncc.gov.tw/Portal/NCCB07Q_04v1.aspx?cond1=MS&cond2=MS112).國家通訊傳播委員會
10.
11. 通常以前4碼為單位分配給一家業者，但是前4碼相同不等於同一家業者，例如0966及0968原本分配給大眾電信，後被回收分配給亚太电信、台湾大哥大、远传电信、中华电信与台湾之星5家業者。
12. 馮昭.[台灣首家3G電信亞太行動寬頻二十八日開台](http://www.epochtimes.com/b5/3/7/22/n346831.htm).大紀元.2003-07-22
13. [頻率資料庫查詢系統](https://freqdbo.ncc.gov.tw/Portal/NCCB07Q_04v1.aspx?cond1=MS&cond2=MS108).國家通訊傳播委員會
14.
15.
16. 通常以前4碼為單位分配給一家業者，但是前4碼相同不等於同一家業者，例如0966及0968原本分配給大眾電信，後被回收分配給亚太电信、台湾大哥大、远传电信、中华电信与台湾之星5家業者。
17.
18. 其他提供市內電話之業者，包括：[台灣固網](../Page/台灣固網.md "wikilink")、[新世紀資通](../Page/新世紀資通.md "wikilink")、[中華電信](../Page/中華電信.md "wikilink")。
19. [固定通信網路業務市內交換機局碼核配現況](https://www.ncc.gov.tw/chinese/files/13010/1500_27435_130104_1.pdf)
20. 若有以該號碼之來電顯示，屬於竄改造成，多屬於[电信诈骗](../Page/电信诈骗.md "wikilink")。
21. **区域**不等于**服务县市**，例如台北市、新北市與基隆市各為不同之縣市，但划分之区域属于**台北**，在相同之區域(例如從基隆市撥打至台北市)，不需加撥區碼；而反之在不同区域(例如從桃园撥打新竹)，即使区码相同，仍需加撥區碼。
22. **收费区**分为第1级与第2级，可参阅国家通讯传播委员会公告[市內電話月租費/通信費價目表](https://www.ncc.gov.tw/chinese/files/16020/8_34988_160204_1.pdf)。
23.
24. 若有以該號碼之來電顯示，屬於竄改造成，多屬於[电信诈骗](../Page/电信诈骗.md "wikilink")。
25. 其他提供受话方付费电话之業者，包括：[台灣固網](../Page/台灣固網.md "wikilink")(0809開頭)、[新世紀資通](../Page/新世紀資通.md "wikilink")(0809開頭)、[中華電信](../Page/中華電信.md "wikilink")(0800開頭)。
26.
27.
28. 若有以該號碼之來電顯示，屬於竄改造成，多屬於[电信诈骗](../Page/电信诈骗.md "wikilink")。
29. 其他提供付費語音服務之業者，包括：[台灣固網](../Page/台灣固網.md "wikilink")(0209開頭)、[新世紀資通](../Page/新世紀資通.md "wikilink")(0208開頭)、[中華電信](../Page/中華電信.md "wikilink")(0203、0204開頭)。
30.
31.
32. 該冠碼爲2000年4月10日於[交通部電信總局抽籤決定](../Page/交通部電信總局.md "wikilink")，當日抽籤結果爲：東森寬頻電信取得代号为5、[台灣固網取得代号为](../Page/台灣固網.md "wikilink")6、[新世紀資通取得代号为](../Page/新世紀資通.md "wikilink")7、[中華電信取得代号为](../Page/中華電信.md "wikilink")9。
33.
34. [國際電話冠碼](../Page/国际冠码列表.md "wikilink")002原专属于中华电信，后改为中性號碼，非专屬於任何業者(例如从亚太电信之电话拨出002，即由亚太电信之路由连接国外电信业者；从台湾大哥大之电话拨出002，即由其预设之台湾固网之路由连接国外电信业者)；008爲國際直撥受話方付費電話服務(IFPS)共用碼，非专屬於任何業者。
35. 依据，國際通信服務之冠码开头为00\*(3码)；撥號選接網路識別碼开头为18\*\*(4码)，\*为业者之代号。
36.
37.
38.
39. 該冠碼爲2000年4月10日於[交通部電信總局抽籤決定](../Page/交通部電信總局.md "wikilink")，當日抽籤結果爲：東森寬頻電信取得代号为5、[台灣固網取得代号为](../Page/台灣固網.md "wikilink")6、[新世紀資通取得代号为](../Page/新世紀資通.md "wikilink")7、[中華電信取得代号为](../Page/中華電信.md "wikilink")9。
40. 依据，國際通信服務之冠码开头为00\*(3码)；撥號選接網路識別碼开头为18\*\*(4码)，\*为业者之代号。
41.
42.
43.
44.