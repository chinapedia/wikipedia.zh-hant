**Banshee**是[GNOME的一個](../Page/GNOME.md "wikilink")[跨平台的](../Page/跨平台.md "wikilink")[開放原始碼](../Page/開放原始碼.md "wikilink")[媒体播放器](../Page/媒体播放器.md "wikilink")。Banshee建基於[Mono與](../Page/Mono.md "wikilink")[Gtk\#之上](../Page/Gtk_Sharp.md "wikilink")，使用[GStreamer多媒體平台進行編碼](../Page/GStreamer.md "wikilink")，並可對多種音樂格式進行解碼，包括了[Ogg
Vorbis](../Page/Ogg_Vorbis.md "wikilink")、[MP3與](../Page/MP3.md "wikilink")[FLAC](../Page/FLAC.md "wikilink")。Banshee可以播放、匯入與燒錄音頻[光碟以及與數款便攜媒體播放器同步音樂](../Page/光碟.md "wikilink")，包括蘋果公司的[iPod與](../Page/iPod.md "wikilink")[Creative
Zen播放器](../Page/Creative_Zen.md "wikilink")。儲存在iPod上的音樂可直接播放而不需經過同步，而儲存在Banshee資料庫的唱片封面會傳送到iPod。對[MTP協定](../Page/Media_Transfer_Protocol.md "wikilink")、[PlaysForSure裝置以及](../Page/Microsoft_PlaysForSure.md "wikilink")[Rio
Karma播放器的支援預定會在不久的將來加入](../Page/Rio_Karma.md "wikilink")。

Banshee在[Ubuntu](../Page/Ubuntu.md "wikilink") 11.04 Alpha
3中取代[Rhythmbox](../Page/Rhythmbox.md "wikilink")，成為系統的預設媒體播放器\[1\]。

## 插件

Banshee採用一個可使用插件的架構，因此可高度的擴展與用戶化。現時穩定的插件包括：

  - Audioscrobbler：加入將已播放歌曲報告到用戶的[Last.fm播放清單的能力](../Page/Last.fm.md "wikilink")。
  - Podcasting：允許Banshee訂閱Podcast饋送，會定期更新。另有一個利用[Podcast
    Alley](http://www.podcastalley.com)的"Find new podcasts"功能。
  - [DAAP音樂共享](../Page/Digital_Audio_Access_Protocol.md "wikilink")：允許與[iTunes及其他兼容DAAP的音樂軟體共享音樂資料庫](../Page/iTunes.md "wikilink")，當前版本的Banshee只部份地與iTunes
    7兼容。iTunes 7可開啓Banshee的資料庫，但Banshee無法開啓iTunes 7的資料庫。
  - 使用[Musicbrainz的元數據搜尋器](../Page/Musicbrainz.md "wikilink")：為庫料庫中的項目自動擷取缺少與增補的元數據，包括唱片封面。
  - 使用[Last.fm的音樂推介](../Page/Last.fm.md "wikilink")：基於現時播放的歌曲來推介音樂
  - 微型模式插件：提供一個包括最低限度播放控制器與歌曲資訊的小型視窗
  - 於[GNOME支援多媒體按鍵](../Page/GNOME.md "wikilink")：在透過GNOME配置時Banshee可經由多媒體按鍵控制
  - 廣播：提供對互聯網串流廣播電台的支援
  - 通知區域圖示：在GNOME的通知區域加入圖示

## Helix Banshee

Helix
Banshee是一個Banshee的修改版，包含在某些Linux分發版如[openSUSE當中](../Page/openSUSE.md "wikilink")。Helix
Banshee包含了Banshee核心，經過修改以使用[Helix框架](../Page/Helix.md "wikilink")，與Banshee不同，Helix
Banshee使用Helix引擎進行播放。

## 相關條目

  - [Amarok](../Page/Amarok.md "wikilink")
  - [Rhythmbox](../Page/Rhythmbox.md "wikilink")
  - [Songbird](../Page/Songbird.md "wikilink")
  - [媒體播放器比較](../Page/媒體播放器比較.md "wikilink")
  - [媒體播放器列表](../Page/媒體播放器列表.md "wikilink")

## 參考資料

## 外部連結

  - [官方網站](https://web.archive.org/web/20140102111027/http://banshee.fm/)
  - [Helix Banshee](https://helix-banshee.helixcommunity.org/)

[Category:媒體播放器](../Category/媒體播放器.md "wikilink")
[Category:自由跨平台软件](../Category/自由跨平台软件.md "wikilink")
[Category:自由媒体播放器](../Category/自由媒体播放器.md "wikilink")
[Category:自由音讯软件](../Category/自由音讯软件.md "wikilink")

1.