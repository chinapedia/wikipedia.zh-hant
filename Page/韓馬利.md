**韓馬利**（英文名：**Mary Hon**；），人称“Mary
姐”；生于[香港](../Page/香港.md "wikilink")；是本地[甘草演員](../Page/甘草演員.md "wikilink")。現為[TVB部頭合約女藝員](../Page/無線電視.md "wikilink")。

## 簡歷

### 童年及出身

韓馬利生於小康之家，父親是[藥劑師](../Page/藥劑師.md "wikilink")，母親則是[教師](../Page/教師.md "wikilink")，還有一個哥哥和兩個妹妹；家住[銅鑼灣](../Page/銅鑼灣.md "wikilink")[利園山道](../Page/利園山道.md "wikilink")。小時候，她跟祖父母住在位於[石澳的](../Page/石澳.md "wikilink")[別墅](../Page/別墅.md "wikilink")。韓的祖父母和父母都是[基督徒](../Page/基督徒.md "wikilink")，祖父母在她出世時已帶她[受洗](../Page/受洗.md "wikilink")。她的聖名是[以馬內利](../Page/以馬內利.md "wikilink")\[1\]。

自小性格反叛，不喜歡讀書，但對[舞蹈十分有興趣](../Page/舞蹈.md "wikilink")；1970年，曾以學生身份代表香港在[大阪世界博覽會獻舞](../Page/1970年世界博覽會.md "wikilink")。及後中五畢業，家人希望她到[加拿大升學](../Page/加拿大.md "wikilink")；但她卻因怕悶而拒絕，還自行報讀了[麗的電視的舞蹈藝員訓練班](../Page/麗的電視.md "wikilink")，同期的訓練班同學有[劉松仁](../Page/劉松仁.md "wikilink")。此後，她在麗的跳了兩年舞，直至一日，初戀的男朋友決定前往[英國讀書](../Page/英國.md "wikilink")，她不假思索便跟隨他過去，在[倫敦租下一個單位同住](../Page/倫敦.md "wikilink")。可是半年後，她發覺她的男友對自己不忠，一怒之下便獨自買機票回港\[2\]。

### 幕前演出

由於母親其時是[梁淑怡女兒的補習老師](../Page/梁淑怡.md "wikilink")，1974年在梁的介紹下韓馬利加入了[無線電視](../Page/無線電視.md "wikilink")。她初任舞蹈員，原計劃轉職當[節目助理](../Page/節目助理.md "wikilink")，卻被安排報導節目預告，與[謝雪心和](../Page/謝雪心.md "wikilink")[容茱迪每人負責一晚的預告](../Page/容茱迪.md "wikilink")\[3\]。1975年，監製[王天林開拍劇集](../Page/王天林.md "wikilink")《[陸小鳳](../Page/陸小鳳_\(1976年電視劇\).md "wikilink")》，找她飾演[上官飛燕一角](../Page/上官飛燕.md "wikilink")，首次拍劇就能當上女主角；而飾演[陸小鳳的正好是訓練班的同學](../Page/陸小鳳.md "wikilink")[劉松仁](../Page/劉松仁.md "wikilink")。1977年，她又被安排主持《[K-100](../Page/K-100.md "wikilink")》，和[何守信同為該節目的第一代主持人](../Page/何守信.md "wikilink")\[4\]。

1982年離開[無線電視](../Page/無線電視.md "wikilink")，1985年重返[無線電視](../Page/無線電視.md "wikilink")，1990年再度離開[無線電視](../Page/無線電視.md "wikilink")，1992年再度重返[無線電視](../Page/無線電視.md "wikilink")，往後，她參與演出的劇集多不勝數，從廣為人所知的《[京華春夢](../Page/京華春夢.md "wikilink")》飾演的刁蠻小姐，到近年其樣貌可說變化不大，依然如年青時般神采飛揚。她演技精湛，多年來演出不少大獲好評的角色，深得觀眾肯定，包括憑2007年《爸爸闭翳》、2009年《宮心計》、2012年《回到三國》提名當年[萬千星輝頒獎典禮](../Page/萬千星輝頒獎典禮.md "wikilink")“最佳女配角”，其他代表作包括《宮心計》、《公主嫁到》、《名媛望族》等。

2013年，她在《[萬千星輝頒獎典禮2013](../Page/萬千星輝頒獎典禮2013.md "wikilink")》獲頒發「**傑出演員大獎**」，肯定及嘉許她在演藝圈中的貢獻及付出。

### 婚姻

韓馬利先後經歷了三段婚姻。她與首任丈夫陳波比於1975年結婚，但1977年離婚，1981年與[馮寶寶兄長](../Page/馮寶寶.md "wikilink")[馮吉隆結婚](../Page/馮吉隆.md "wikilink")，但婚姻只維持6年；1989年再與比自己年幼3年的無綫電視國語配音員[杜燕歌結婚](../Page/杜燕歌.md "wikilink")\[5\]。

### 其他

除了演戲外，韓馬利曾在[鄭明明的美容公司工作十多年](../Page/鄭明明.md "wikilink")，亦曾為BeautySchool擔任行政工作。她取得國際認可美容牌，對美容產品的知識、效用有深切認識，於2003年投資美容中心「胭脂堂」。現時於[蘇志威開辦的舞蹈學校i](../Page/蘇志威.md "wikilink")
Dance擔任行政總監。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>劇名</p></th>
<th><p>角色</p></th>
<th><p>性質</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1976年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陸小鳳_(1976年電視劇).md" title="wikilink">陸小鳳</a></strong></p></td>
<td><p><strong>上官飛燕/上官丹鳳</strong></p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong>1977年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/近代豪俠傳.md" title="wikilink">近代豪俠傳之秋瑾</a></strong></p></td>
<td><p><strong><a href="../Page/秋瑾.md" title="wikilink">秋　瑾</a></strong></p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong>1978年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/韓嫣翠.md" title="wikilink">韓嫣翠</a></strong></p></td>
<td><p><strong>韓嫣翠</strong></p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/父子樂.md" title="wikilink">父子樂</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1979年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刀神_(電視劇).md" title="wikilink">刀神</a></p></td>
<td><p>謝小玉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/名劍風流_(電視劇).md" title="wikilink">名劍風流</a></strong></p></td>
<td><p><strong>姬靈燕/姬靈風</strong></p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/女人三十.md" title="wikilink">女人三十</a></p></td>
<td><p>水　仙</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英雄無淚_(電視劇).md" title="wikilink">英雄無淚</a></p></td>
<td><p>蝶　舞</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1980年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/京華春夢.md" title="wikilink">京華春夢</a></strong></p></td>
<td><p><strong>洪麗珠</strong></p></td>
<td><p>第二女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上海灘續集.md" title="wikilink">上海灘續集</a></p></td>
<td><p>潘　玲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1981年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火鳳凰_(電視劇).md" title="wikilink">火鳳凰</a></p></td>
<td><p>湯海倫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊門女將_(1981年劇集).md" title="wikilink">楊門女將</a></p></td>
<td><p>楊排風</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1982年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香港八二.md" title="wikilink">香港八二</a></p></td>
<td><p>朱美容（May）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1985年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/光怪陸離.md" title="wikilink">光怪陸離</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大廈_(電視劇).md" title="wikilink">大廈</a></p></td>
<td><p>饒雪玲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1986年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/執位夫妻.md" title="wikilink">執位夫妻</a></p></td>
<td><p>Paula</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛情全盒.md" title="wikilink">愛情全盒</a></p></td>
<td><p>藍夫人</p></td>
<td><p>|-_</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃大仙_(電視劇).md" title="wikilink">黃大仙</a></p></td>
<td><p>柳如嫣</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃金十年_(劇集).md" title="wikilink">黃金十年</a></p></td>
<td><p>阿安</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1987年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭成功_(無綫電視劇).md" title="wikilink">鄭成功</a></p></td>
<td><p><a href="../Page/田川氏_(鄭成功母).md" title="wikilink">田川秀子</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1988年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/太平天國_(1988年電視劇).md" title="wikilink">太平天國</a></p></td>
<td><p><a href="../Page/懿貴妃.md" title="wikilink">懿貴妃</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無名火.md" title="wikilink">無名火</a></p></td>
<td><p>May姐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1989年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大城小警.md" title="wikilink">大城小警</a></p></td>
<td><p>蔣明慧</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/回到唐山.md" title="wikilink">回到唐山</a></p></td>
<td><p>婉　君</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1990年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/笑傲在明天.md" title="wikilink">笑傲在明天</a></p></td>
<td><p>Mary</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1992年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中神通王重陽.md" title="wikilink">中神通王重陽</a></p></td>
<td><p>林靈素</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賭霸_(電視劇).md" title="wikilink">賭霸</a></p></td>
<td><p>方婉盈</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1993年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛生事家庭.md" title="wikilink">愛生事家庭</a></p></td>
<td><p>劉　蓮</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1994年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廉政行動1994.md" title="wikilink">廉政行動1994</a></p></td>
<td><p>李天妻</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/俠女游龍.md" title="wikilink">俠女游龍</a></p></td>
<td><p>太　后</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/婚姻物語.md" title="wikilink">婚姻物語</a></p></td>
<td><p>盧月嫦</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/笑看風雲.md" title="wikilink">笑看風雲</a></p></td>
<td><p>施美芝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/壹號皇庭III.md" title="wikilink">壹號皇庭III</a></p></td>
<td><p>鄧麗芳</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃浦傾情.md" title="wikilink">黃浦傾情</a></p></td>
<td><p>童夫人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿Sir早晨.md" title="wikilink">阿Sir早晨</a></p></td>
<td><p>梁主任</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/烈火狂奔.md" title="wikilink">烈火狂奔</a></p></td>
<td><p>趙素娟</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1995年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刑事偵緝檔案.md" title="wikilink">刑事偵緝檔案</a></p></td>
<td><p>程太太</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/包青天之親子恩仇.md" title="wikilink">包青天之親子恩仇</a></p></td>
<td><p>張媚娘</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/娛樂插班生.md" title="wikilink">娛樂插班生</a></p></td>
<td><p>Laser姐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/總有出頭天_(電視劇).md" title="wikilink">總有出頭天</a></p></td>
<td><p>歐潔珊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/忘情闊少爺.md" title="wikilink">忘情闊少爺</a></p></td>
<td><p>高詠琳</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1996年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/五個醒覺的少年.md" title="wikilink">五個醒覺的少年</a></p></td>
<td><p>李淑蘭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1997年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/醉打金枝.md" title="wikilink">醉打金枝</a></p></td>
<td><p>淑　妃</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廉政追緝令.md" title="wikilink">廉政追緝令</a></p></td>
<td><p>麥燕芳</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鑑證實錄.md" title="wikilink">鑑證實錄</a></p></td>
<td><p>阮佩雲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大鬧廣昌隆.md" title="wikilink">大鬧廣昌隆</a></p></td>
<td><p>阿　儀</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/濟公.md" title="wikilink">濟公</a></p></td>
<td><p>金少枝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1998年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/外父唔怕做.md" title="wikilink">外父唔怕做</a></p></td>
<td><p>梁張笑蘭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1999年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鑑證實錄II.md" title="wikilink">鑑證實錄II</a></p></td>
<td><p>余彩娟</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/全院滿座.md" title="wikilink">全院滿座</a></p></td>
<td><p>杜麗梅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金玉滿堂.md" title="wikilink">金玉滿堂</a></p></td>
<td><p>高佳氏</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/茶是故鄉濃.md" title="wikilink">茶是故鄉濃</a></p></td>
<td><p>鳳利菲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/創世紀_(電視劇).md" title="wikilink">創世紀</a></p></td>
<td><p>馬羅寶珠</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刑事偵緝檔案IV.md" title="wikilink">刑事偵緝檔案IV</a></p></td>
<td><p>葉翠荷</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2000年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大囍之家.md" title="wikilink">大囍之家</a></p></td>
<td><p>程寶雪</p></td>
<td><p>闲角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新鮮人_(電視劇).md" title="wikilink">新鮮人</a></p></td>
<td><p>王婉芬</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夢想成真.md" title="wikilink">夢想成真</a></p></td>
<td><p>唐牡丹</p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="even">
<td><p><strong>2001年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美味情緣.md" title="wikilink">美味情緣</a></p></td>
<td><p>荷</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公私戀事多.md" title="wikilink">公私戀事多</a></p></td>
<td><p>李小柔</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尋秦記_(電視劇).md" title="wikilink">尋秦記</a></p></td>
<td><p>鳳　姐</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/婚前昏後.md" title="wikilink">婚前昏後</a></p></td>
<td><p>Maggie</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/談談情·練練武.md" title="wikilink">談談情·練練武</a></p></td>
<td></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/七號差館_(電視劇).md" title="wikilink">七號差館</a></p></td>
<td><p>朱素蘭</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2002年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皆大歡喜_(古裝電視劇).md" title="wikilink">皆大歡喜（古裝版）</a></p></td>
<td><p>火炎炎</p></td>
<td><p>单元女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彩色世界.md" title="wikilink">彩色世界</a></p></td>
<td><p>馮　太</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/再生緣_(2002年電視劇).md" title="wikilink">再生緣</a></p></td>
<td><p>格米思</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/點指賊賊賊捉賊.md" title="wikilink">點指賊賊賊捉賊</a></p></td>
<td><p>巢丁世妹</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/談判專家_(電視劇).md" title="wikilink">談判專家</a></p></td>
<td><p>徐嘉倩</p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2003年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/洗冤錄II.md" title="wikilink">洗冤錄II</a></p></td>
<td><p>何淑蘭</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美麗在望.md" title="wikilink">美麗在望</a></p></td>
<td><p>卓利紅萍</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/智勇新警界.md" title="wikilink">智勇新警界</a></p></td>
<td><p>洪靜琪</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/衝上雲霄.md" title="wikilink">衝上雲霄</a></strong></p></td>
<td><p><strong>邢佳美（Cammy）</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西關大少.md" title="wikilink">西關大少</a></p></td>
<td><p>曾國邦之母</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Juliet</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><strong>2004年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陀槍師姐IV.md" title="wikilink">陀槍師姐IV</a></p></td>
<td><p>羅劍暉</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/翡翠戀曲.md" title="wikilink">翡翠戀曲</a></p></td>
<td><p>馬彩蓉</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/棟篤神探.md" title="wikilink">棟篤神探</a></p></td>
<td><p>李秀芳</p></td>
<td><p>单元角色</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/水滸無間道.md" title="wikilink">水滸無間道</a></p></td>
<td><p>鄧玉珠</p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜（時裝版）</a></p></td>
<td><p>白　雪</p></td>
<td><p>单元角色</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大唐雙龍傳_(電視劇).md" title="wikilink">大唐雙龍傳</a></p></td>
<td><p>梵清惠</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/天涯俠醫.md" title="wikilink">天涯俠醫</a></p></td>
<td><p>顧映姿</p></td>
<td><p>闲角</p></td>
</tr>
<tr class="even">
<td><p><strong>2005年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同撈同煲.md" title="wikilink">同撈同煲</a></p></td>
<td><p>鄧昭慈</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/酒店風雲.md" title="wikilink">酒店風雲</a></p></td>
<td><p>何潔雲</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奪命真夫.md" title="wikilink">奪命真夫</a></p></td>
<td><p>繆　雪（Rosa）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胭脂水粉.md" title="wikilink">胭脂水粉</a></p></td>
<td><p>陳影紅</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甜孫爺爺.md" title="wikilink">甜孫爺爺</a></p></td>
<td><p>文超紅</p></td>
<td><p>閒角</p></td>
</tr>
<tr class="even">
<td><p><strong>2006年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/施公奇案.md" title="wikilink">施公奇案</a></p></td>
<td><p>蕭九娘</p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潮爆大狀.md" title="wikilink">潮爆大狀</a></p></td>
<td><p>宋綺華（Anna）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/女人唔易做.md" title="wikilink">女人唔易做</a></p></td>
<td><p>唐麗珠</p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法證先鋒.md" title="wikilink">法證先鋒</a></p></td>
<td><p>陳　湄</p></td>
<td><p>单元女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東方之珠_(電視劇).md" title="wikilink">東方之珠</a></p></td>
<td><p>白　璐</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/追魂交易.md" title="wikilink">追魂交易</a></p></td>
<td><p>江鳳蘭</p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高朋滿座.md" title="wikilink">高朋滿座</a></p></td>
<td><p>馬夫人</p></td>
<td><p>单元角色</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/千謊百計.md" title="wikilink">千謊百計</a></p></td>
<td><p>方妙芝</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2007年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我外母唔係人.md" title="wikilink">我外母唔係人</a></p></td>
<td><p>金　鳳</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/突圍行動.md" title="wikilink">突圍行動</a></p></td>
<td><p>何美妍</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亂世佳人_(電視劇).md" title="wikilink">亂世佳人</a></p></td>
<td><p>楊秀蓮</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歲月風雲.md" title="wikilink">歲月風雲</a></p></td>
<td><p>盧皓月</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/通天幹探.md" title="wikilink">通天幹探</a></p></td>
<td><p>董太太</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/爸爸閉翳.md" title="wikilink">爸爸閉翳</a></strong></p></td>
<td><p><strong>李帶彩</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>2008年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/和味濃情.md" title="wikilink">和味濃情</a></p></td>
<td><p>阿　玉</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古靈精探.md" title="wikilink">古靈精探</a></p></td>
<td><p>簡靜怡</p></td>
<td><p>闲角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金石良緣.md" title="wikilink">金石良緣</a></p></td>
<td><p>何寶玲</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/師奶股神.md" title="wikilink">師奶股神</a></p></td>
<td><p>陳慧蘭</p></td>
<td><p>閒角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甜言蜜語.md" title="wikilink">甜言蜜語</a></p></td>
<td><p>唐巧冰（Ivy）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溏心風暴之家好月圓.md" title="wikilink">溏心風暴之家好月圓</a></p></td>
<td><p>娟</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東山飄雨西關晴.md" title="wikilink">東山飄雨西關晴</a></p></td>
<td><p>瑪麗修女</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/Click入黃金屋.md" title="wikilink">Click入黃金屋</a></strong></p></td>
<td><p><strong>顏如玉</strong></p></td>
<td><p><strong>第四女主角</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>2009年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/畢打自己人.md" title="wikilink">畢打自己人</a></p></td>
<td><p>羅雪仙</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ID精英.md" title="wikilink">ID精英</a></p></td>
<td><p><strong>陳美萍</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/碧血鹽梟.md" title="wikilink">碧血鹽梟</a></p></td>
<td><p>聶張氏</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/宮心計.md" title="wikilink">宮心計</a></strong></p></td>
<td><p><strong>鄭太后</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>2010年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/秋香怒點唐伯虎.md" title="wikilink">秋香怒點唐伯虎</a></p></td>
<td><p>殷秀慧</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/女王辦公室.md" title="wikilink">女王辦公室</a></p></td>
<td><p>吴　琳</p></td>
<td><p>单元角色</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/談情說案.md" title="wikilink">談情說案</a></p></td>
<td><p>蔣慧珠（Victoria）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/公主嫁到.md" title="wikilink">公主嫁到</a></strong></p></td>
<td><p><strong>米仁慈</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/囧探查過界.md" title="wikilink">囧探查過界</a></p></td>
<td><p>梁惠娟</p></td>
<td><p>单元女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/誘情轉駁.md" title="wikilink">誘情轉駁</a></p></td>
<td><p>何　歡</p></td>
<td><p>特別演出</p></td>
</tr>
<tr class="odd">
<td><p><strong>2011年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/仁心解碼.md" title="wikilink">仁心解碼</a></p></td>
<td><p>娟　母</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魚躍在花見.md" title="wikilink">魚躍在花見</a></p></td>
<td><p>康秀馨</p></td>
<td><p>特別演出</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/你們我們他們.md" title="wikilink">你們我們他們</a></p></td>
<td><p>朱師奶</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洪武三十二.md" title="wikilink">洪武三十二</a></p></td>
<td><p>甕　妃</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/真相_(無綫電視劇集).md" title="wikilink">真相</a></p></td>
<td><p>康徐笑薇</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/潛行狙擊.md" title="wikilink">潛行狙擊</a></strong></p></td>
<td><p><strong>何秀美（蘇菲）</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法證先鋒III.md" title="wikilink">法證先鋒III</a></p></td>
<td><p>張鳳萍</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2012年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/4_In_Love_(電視劇).md" title="wikilink">4 In Love</a></p></td>
<td><p><strong>何春嬌</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/On_Call_36小時.md" title="wikilink">On Call 36小時</a></p></td>
<td><p><strong>施玉蘭</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/當旺爸爸.md" title="wikilink">當旺爸爸</a></p></td>
<td><p>刁　太</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/回到三國.md" title="wikilink">回到三國</a></p></td>
<td><p><strong>蔡夫人</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/名媛望族.md" title="wikilink">名媛望族</a></strong></p></td>
<td><p><strong>顧心蘭（大太太）</strong></p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法網狙擊.md" title="wikilink">法網狙擊</a></p></td>
<td><p><strong>文　英（Helen）</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幸福摩天輪.md" title="wikilink">幸福摩天輪</a></p></td>
<td><p>施結荷</p></td>
<td><p>单元女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2013年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/老表，你好嘢！_(電視劇).md" title="wikilink">老表，你好嘢！</a></p></td>
<td><p>陳大雯（Mary姐）</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/仁心解碼II.md" title="wikilink">仁心解碼II</a></p></td>
<td><p>歐梁鳳玉</p></td>
<td><p>单元女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛·回家_(第一輯).md" title="wikilink">愛·回家 (第一輯)</a></p></td>
<td><p>龍嬌、畢瑤姿</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/情逆三世緣.md" title="wikilink">情逆三世緣</a></p></td>
<td><p>韓　丹</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法外風雲.md" title="wikilink">法外風雲</a></p></td>
<td><p><strong>程淑馨</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/On_Call_36小時II.md" title="wikilink">On Call 36小時II</a></p></td>
<td><p><strong>施玉蘭</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>2014年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/單戀雙城.md" title="wikilink">單戀雙城</a></p></td>
<td><p>杜惠心</p></td>
<td><p>特別演出</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寒山潛龍.md" title="wikilink">寒山潛龍</a></p></td>
<td><p><strong>公孫璧（太后）</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/載得有情人.md" title="wikilink">載得有情人</a></p></td>
<td><p><strong>方綺鈴（Elanie）</strong></p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/使徒行者.md" title="wikilink">使徒行者</a></p></td>
<td><p>馮慧芳（Rose媽）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/老表，你好hea！.md" title="wikilink">老表，你好hea！</a></p></td>
<td><p><strong>芭芭拉（Barbara）</strong></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>2015年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/師奶MADAM.md" title="wikilink">師奶MADAM</a></p></td>
<td><p>賈美蓉</p></td>
<td><p>特別演出</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四個女仔三個BAR.md" title="wikilink">四個女仔三個BAR</a></p></td>
<td><p>余若菲</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水髮胭脂.md" title="wikilink">水髮胭脂</a></p></td>
<td><p>嚴嘉麗（Winnie）</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陪著你走_(電視劇).md" title="wikilink">陪著你走</a></p></td>
<td><p>高小姐</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無雙譜_(2015年電視劇).md" title="wikilink">無雙譜</a></p></td>
<td><p>段夫人</p></td>
<td><p>閒角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東坡家事.md" title="wikilink">東坡家事</a></p></td>
<td><p>太　-{后}-</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/實習天使.md" title="wikilink">實習天使</a></p></td>
<td><p>戴利群</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>2016年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛·回家之八時入席.md" title="wikilink">愛·回家之八時入席</a></p></td>
<td><p>時天鳳</p></td>
<td><p>固定角色</p></td>
</tr>
<tr class="even">
<td><p><strong>2017年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/味想天開.md" title="wikilink">味想天開</a></p></td>
<td><p>米　桂</p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/同盟.md" title="wikilink">同盟</a></strong></p></td>
<td><p><strong>令　熹</strong></p></td>
<td><p>第四女主角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/老表，畢業喇！.md" title="wikilink">老表，畢業喇！</a></p></td>
<td><p>史　太</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溏心風暴3.md" title="wikilink">溏心風暴3</a></p></td>
<td><p>楊馮美美</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong>2018年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/逆緣.md" title="wikilink">逆緣</a></p></td>
<td><p>翁芷蘭</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宮心計2深宮計.md" title="wikilink">宮心計2深宮計</a></p></td>
<td><p>周雨嫣</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><strong>2019年</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廉政行動2019.md" title="wikilink">廉政行動2019</a></p></td>
<td><p>葉泳心</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><strong>未播映</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/十二傳說.md" title="wikilink">十二傳說</a></p></td>
<td><p>易寶芙</p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/殺手_(無綫電視劇).md" title="wikilink">殺手</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>拍攝中</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/唐人街_(無綫電視劇).md" title="wikilink">唐人街</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

|        |                                                    |         |
| ------ | -------------------------------------------------- | ------- |
| **年份** | **片名**                                             | **角色**  |
| 1978   | [鬼馬狂潮](../Page/鬼馬狂潮.md "wikilink")                 | 補習老師    |
| 1978   | [不擇手段](../Page/不擇手段.md "wikilink")                 |         |
| 1978   | [戇居仔與牛咁眼](../Page/戇居仔與牛咁眼.md "wikilink")           |         |
| 1979   | [鬼馬智多星](../Page/鬼馬智多星.md "wikilink")               |         |
| 1982   | [浣花洗劍](../Page/浣花洗劍.md "wikilink")                 |         |
| 1983   | [瘋狂83](../Page/瘋狂83.md "wikilink")                 |         |
| 1985   | [鬥氣小神仙](../Page/鬥氣小神仙.md "wikilink")               |         |
| 1986   | [皇家戰士](../Page/皇家戰士.md "wikilink")                 |         |
| 1987   | [呷醋大丈夫](../Page/呷醋大丈夫.md "wikilink")               |         |
| 1987   | [香港小姐寫真](../Page/香港小姐寫真.md "wikilink")             |         |
| 1988   | [赤膽情](../Page/赤膽情.md "wikilink")                   |         |
| 1988   | [義膽紅唇](../Page/義膽紅唇.md "wikilink")                 |         |
| 1994   | [花月危情](../Page/花月危情.md "wikilink")                 |         |
| 1995   | [虎猛威龍](../Page/虎猛威龍.md "wikilink")                 |         |
| 1996   | [飛虎雄心2傲氣比天高](../Page/飛虎雄心2傲氣比天高.md "wikilink")     |         |
| 1998   | [新古惑仔之少年激鬥篇](../Page/新古惑仔之少年激鬥篇.md "wikilink")     | |-陳浩南母親 |
| 1999   | [烈火戰車2極速傳說](../Page/烈火戰車2極速傳說.md "wikilink")       | |-sky母親 |
| 2000   | [魔鬼教師](../Page/魔鬼教師.md "wikilink")                 |         |
| 2003   | [低一點的天空](../Page/低一點的天空.md "wikilink")             |         |
| 2006   | [大丈夫2](../Page/大丈夫2.md "wikilink")                 | 校長      |
| 2017   | [超能少年之宿命之戰](../Page/超能少年之宿命之戰.md "wikilink")(網絡電影) | 何母      |
|        |                                                    |         |

### 香港電台節目

|        |                                                |        |
| ------ | ---------------------------------------------- | ------ |
| **年份** | **節目**                                         | **角色** |
| 1998年  | [非常平等任務](../Page/非常平等任務.md "wikilink")：沒有眼神的微笑 | 校長     |
| 2016年  | [女人多自在5](../Page/女人多自在5.md "wikilink")：再次華爾滋   | 林太     |

### 廣告

  - 金寶鐘清潔劑
  - 維他朱古力奶 2017年廣告

### 節目主持

|           |                                      |
| --------- | ------------------------------------ |
| **年份**    | **節目**                               |
| 1977      | [K-100](../Page/K-100.md "wikilink") |
| 1982-1984 | [歡樂明星](../Page/歡樂明星.md "wikilink")   |

## 獎項

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>大會</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>劇集</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/萬千星輝賀台慶.md" title="wikilink">萬千星輝賀台慶</a></p></td>
<td><p>本年度我最喜爱的实力非凡女艺员</p></td>
<td><p>《談判專家》</p></td>
<td><p>徐嘉倩</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>萬千星輝賀台慶</p></td>
<td><p>本年度我最喜爱的实力非凡女艺员</p></td>
<td><p>《洗冤錄II》</p></td>
<td><p>何淑蘭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>萬千星輝賀台慶</p></td>
<td><p>本年度我最喜爱的实力非凡女艺员</p></td>
<td><p>《智勇新警界》</p></td>
<td><p>洪靜琪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>萬千星輝賀台慶</p></td>
<td><p>本年度我最喜爱的实力非凡女艺员</p></td>
<td><p>《衝上雲霄》</p></td>
<td><p>邢佳美</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>萬千星輝賀台慶</p></td>
<td><p>本年度我最喜爱的实力非凡女艺员</p></td>
<td><p>《翡翠戀曲》</p></td>
<td><p>馬彩蓉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>萬千星輝賀台慶</p></td>
<td><p>本年度我最喜爱的实力非凡女艺员</p></td>
<td><p>《大唐雙龍傳》</p></td>
<td><p>梵清惠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/2007年萬千星輝頒獎典禮.md" title="wikilink">2007年萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《爸爸闭翳》</p></td>
<td><p>李帶彩</p></td>
<td><p>（最後五強）</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/2009年萬千星輝頒獎典禮.md" title="wikilink">2009年萬千星輝頒獎典禮</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《宮心計》</p></td>
<td><p>鄭太后</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2012.md" title="wikilink">萬千星輝頒獎典禮2012</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《回到三國》</p></td>
<td><p>蔡夫人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2013.md" title="wikilink">萬千星輝頒獎典禮2013</a></p></td>
<td><p>最佳女配角</p></td>
<td><p>《ON CALL36小時II》</p></td>
<td><p>施玉蘭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/萬千星輝頒獎典禮2013.md" title="wikilink">萬千星輝頒獎典禮2013</a></p></td>
<td><p>傑出演員大獎</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 趣聞

2013年初，韓馬利因連續於多部無綫電視劇演出而人氣急升，有網絡討論區的[網民發現她曾於多齣古裝和時裝電視劇集中拋棄子女並隨後嘗試相認](../Page/網民.md "wikilink")，因而被封為「最狠心媽媽」。\[6\]\[7\]事緣韓馬利在同時間播出的《[幸福摩天輪](../Page/幸福摩天輪.md "wikilink")》和《[法網狙擊](../Page/法網狙擊.md "wikilink")》拋棄親生兒子[陳智燊和](../Page/陳智燊.md "wikilink")[森美](../Page/森美.md "wikilink")，此事被網民追塑至十年前，原來韓馬利在2003年起已陸續拋棄親生子女，由2003年到2015年，十二年內共拋棄十個親生子女。

| 次數 | 年份                                     | 劇集                                                   | 劇集類型                                  | 被拋棄親生子女                                                            |
| -- | -------------------------------------- | ---------------------------------------------------- | ------------------------------------- | ------------------------------------------------------------------ |
| 1  | 2003                                   | 《[智勇新警界](../Page/智勇新警界.md "wikilink")》               | 時裝                                    | 馬孝賢（[馬德鐘](../Page/馬德鐘.md "wikilink")）                              |
| 2  | 《[衝上雲霄](../Page/衝上雲霄.md "wikilink")》   | 時裝                                                   | 唐亦琛（[吳鎮宇](../Page/吳鎮宇.md "wikilink")） |                                                                    |
| 3  | 2006                                   | 《[甜言蜜語](../Page/甜言蜜語.md "wikilink")》                 | 時裝                                    | 唐棠（[徐子珊](../Page/徐子珊.md "wikilink")）                               |
| 4  | 2010                                   | 《[囧探查過界](../Page/囧探查過界.md "wikilink")》               | 時裝                                    | 鍾意得（[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")）                              |
| 5  | 2011                                   | 《[洪武三十二](../Page/洪武三十二.md "wikilink")》               | 古裝（[明朝](../Page/明朝.md "wikilink")）    | [朱　棣](../Page/明成祖.md "wikilink")（[馬德鐘](../Page/馬德鐘.md "wikilink")） |
| 6  | 2012                                   | 《[On Call 36小時](../Page/On_Call_36小時.md "wikilink")》 | 時裝                                    | 范子妤（[楊怡](../Page/楊怡.md "wikilink")）                                |
| 7  | 2013                                   | 《[幸福摩天輪](../Page/幸福摩天輪.md "wikilink")》               | 時裝                                    | 施亦謙（[陳智燊](../Page/陳智燊.md "wikilink")）                              |
| 8  | 《[法網狙擊](../Page/法網狙擊.md "wikilink")》   | 時裝                                                   | 甘波地（[森美](../Page/森美.md "wikilink")）   |                                                                    |
| 9  | 《[情逆三世緣](../Page/情逆三世緣.md "wikilink")》 | 古裝（[宋朝](../Page/宋朝.md "wikilink")）                   | 楊夕雪（[關詠荷](../Page/關詠荷.md "wikilink")） |                                                                    |
| 10 | 2015                                   | 《[師奶MADAM](../Page/師奶MADAM.md "wikilink")》           | 時裝                                    | 羅大樹（[蕭正楠](../Page/蕭正楠.md "wikilink")）                              |
|    |                                        |                                                      |                                       |                                                                    |

## 外部連結

  -
  - [如果太多韓馬利 拍戲唔嫌多 -
    個人專訪(上)](https://www.facebook.com/TVBmytv/videos/933013340098171/)

  - [如果太多韓馬利 拍戲唔嫌多 -
    個人專訪(下)](https://www.facebook.com/TVBmytv/videos/935574123175426)

  -
  -
## 參考資料

## 注釋

[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港甘草演員](../Category/香港甘草演員.md "wikilink")
[Category:福建人](../Category/福建人.md "wikilink")
[Ma馬利](../Category/韓姓.md "wikilink")
[Category:香港真光中學校友](../Category/香港真光中學校友.md "wikilink")

1.  [豪語錄：叛徒　韓馬利《壹周刊》](http://hk.motor.nextmedia.com/moDspContent.cfm?article_ID=%28%22%2CK8QOG%21%20%228%20%0A&CAT_ID=171)

2.
3.
4.
5.

6.  [蘋果日報﹕馮仁昭四圍超：韓馬利封最狠心媽咪](http://hk.apple.nextmedia.com/entertainment/art/20130121/18141415)

7.  [東方日報﹕韓馬利被封「最狠心媽咪」](http://orientaldaily.on.cc/cnt/entertainment/20130121/00282_033.html)