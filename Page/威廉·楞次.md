**威廉·楞次**（，）是一位[德國](../Page/德國.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")。著名的[易辛模型是他發明的](../Page/易辛模型.md "wikilink")\[1\]。他應用[拉普拉斯-龍格-楞次向量於](../Page/拉普拉斯-龍格-楞次向量.md "wikilink")[氫原子的](../Page/氫原子.md "wikilink")[舊量子論](../Page/舊量子論.md "wikilink")，關鍵性地導引出氫原子的[發射光譜](../Page/發射光譜.md "wikilink")\[2\]。

## 生平

1906年，楞次從 Klinger-Oberralschule
中學畢業後，他進入了[哥廷根大學](../Page/哥廷根大學.md "wikilink")，主修數學和物理。1908年，楞次轉到[慕尼黑大學](../Page/慕尼黑大學.md "wikilink")。他在教授[阿諾·索末菲的指導下](../Page/阿諾·索末菲.md "wikilink")，成功地於
1911年得到博士學位\[3\]。畢業後，他仍舊繼續地留在母校，成為索末菲的助理。1914年，他通過了[德語國家教授資格考試](../Page/德語國家教授資格考試.md "wikilink")，正式任職大學講師。

在[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，他被徵召服役，被派往法國當無線電操作專家。1916年他獲得二級[鐵十字勳章](../Page/鐵十字勳章.md "wikilink")。大戰結束後，1920年，他又回到[慕尼黑大學的理論物理學院](../Page/慕尼黑大學.md "wikilink")，繼續當索末菲的助理。那年11月，他升職為副教授。12月，[羅斯托克大學聘請他為副教授](../Page/羅斯托克大學.md "wikilink")。從1921年至他退休的1956年，他是[漢堡大學的理論物理講座教授與理論物理學院主任](../Page/漢堡大學.md "wikilink")\[4\]
\[5\]
\[6\]。因為德國在原子物理與量子力學的高度發展，再加上索末菲的熱心助力，[漢堡大學創立了理論物理學院](../Page/漢堡大學.md "wikilink")，又設立了新的教授職位。

在[漢堡大學](../Page/漢堡大學.md "wikilink")，楞次教導了許多傑出的科學家，像[恩斯特·伊辛和](../Page/恩斯特·伊辛.md "wikilink")[約翰內斯·延森](../Page/約翰內斯·延森.md "wikilink")。在那裡，[沃爾夫岡·包立](../Page/沃爾夫岡·包立.md "wikilink")、[帕斯夸尔·约尔旦和](../Page/帕斯夸尔·约尔旦.md "wikilink")[阿爾布雷希特·翁澤爾德](../Page/阿爾布雷希特·翁澤爾德.md "wikilink")（Albrecht
Unsöld）都曾經是他的助手。楞次、包立、和[奧托·施特恩使](../Page/奧托·施特恩.md "wikilink")[漢堡大學的理論物理學院成為一個核子物理的國際中心](../Page/漢堡大學.md "wikilink")，與在[慕尼黑大學](../Page/慕尼黑大學.md "wikilink")（[索末菲](../Page/索末菲.md "wikilink")）、[哥廷根大學](../Page/哥廷根大學.md "wikilink")（[馬克斯·玻恩](../Page/馬克斯·玻恩.md "wikilink")）、和[哥本哈根大學](../Page/哥本哈根大學.md "wikilink")（[尼爾斯·波耳](../Page/尼爾斯·波耳.md "wikilink")）的理論物理學院，都保持緊密的關係\[7\]\[8\]。

## 著作

  - 威廉·楞次《Einführungsmathematik für Physiker》(Verlagsanstalt
    Wolfenbüttel, 1947)

## 參考文獻

<div class="references-small" >

</div>

<small>

  - Mehra, Jagdish, and Helmut Rechenberg ''The Historical Development
    of Quantum Theory. Volume 1 Part 1 The Quantum Theory of Planck,
    Einstein, Bohr and Sommerfeld 1900 – 1925: Its Foundation and the
    Rise of Its Difficulties. ''(Springer, 2001) ISBN 0-387-95174-1

<!-- end list -->

  - Mehra, Jagdish, and Helmut Rechenberg *The Historical Development of
    Quantum Theory. Volume 1 Part 2 The Quantum Theory of Planck,
    Einstein, Bohr and Sommerfeld 1900 – 1925: Its Foundation and the
    Rise of Its Difficulties.* (Springer, 2001) ISBN 0-387-95175-X

<!-- end list -->

  - Mehra, Jagdish, and Helmut Rechenberg *The Historical Development of
    Quantum Theory. Volume 5 Erwin Schrödinger and the Rise of Wave
    Mechanics. Part 1 Schrödinger in Vienna and Zurich 1887-1925.*
    (Springer, 2001) ISBN 0-387-95179-2

</small>

[Category:德國物理學家](../Category/德國物理學家.md "wikilink")
[Category:漢堡大學教師](../Category/漢堡大學教師.md "wikilink")
[Category:羅斯托克大學教師](../Category/羅斯托克大學教師.md "wikilink")
[Category:慕尼黑大學教師](../Category/慕尼黑大學教師.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:黑森人](../Category/黑森人.md "wikilink")

1.

2.

3.  [Lenz](http://genealogy.math.ndsu.nodak.edu/html/id.phtml?id=66700)
    – Mathematics Genealogy Project. 1911 Dissertation title: *Über
    das elektromagnetische Wechselfeld der Spulen und deren
    Wechselstrom-Widerstand, Selbstinduktion und Kapazität*.

4.  [Author Catalog:
    Lenz](http://www.amphilsoc.org/library/guides/ahqp/)  – American
    Philosophical Society

5.  [Lenz](http://www.ethbib.ethz.ch/exhibit/pauli/lenz.html) - ETH
    Zurich

6.  [Lenz Biography](http://litten.de/fulltext/lenz.htm) – Litten

7.
8.