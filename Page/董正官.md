**董正官**（），[字](../Page/表字.md "wikilink")**鈞伯**，又字**訓之**\[1\]，[號](../Page/號.md "wikilink")**質甫**\[2\]，[雲南太和](../Page/雲南.md "wikilink")（[大理](../Page/大理縣.md "wikilink")）人，[清朝政治人物](../Page/清朝.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

[道光十三年](../Page/道光.md "wikilink")（1833年）登癸巳科[進士](../Page/進士.md "wikilink")，歷任[福建](../Page/福建.md "wikilink")[安溪縣](../Page/安溪縣.md "wikilink")、[長泰縣](../Page/長泰縣.md "wikilink")、[霞浦縣](../Page/霞浦縣.md "wikilink")、[雲霄廳等](../Page/雲霄廳.md "wikilink")[知縣](../Page/知縣.md "wikilink")，期間嚴守海疆、熱心辦學、勤察辦案，[道光十四](../Page/道光.md "wikilink")（1834年）以優異政績提升為[雲霄廳](../Page/雲霄廳.md "wikilink")[同知](../Page/同知.md "wikilink")。[道光二十九年](../Page/道光.md "wikilink")（1849年）十一月十七日，升任[台灣府撫民理番](../Page/台灣府.md "wikilink")[通判](../Page/通判.md "wikilink")，又授[噶瑪蘭廳](../Page/噶瑪蘭廳.md "wikilink")[通判](../Page/通判.md "wikilink")\[3\]。[咸豐三年](../Page/咸豐_\(年號\).md "wikilink")（1853年）[朝廷因軍糧不濟](../Page/朝廷.md "wikilink")，向富紳籌借勸募並向民間收購倉穀，因而刺激米價高漲，引發民怨。且官方收購價格過低，勢同變相徵收，造成官逼民反局勢。當時吳磋、林汶英召集群眾以梅州（今[宜蘭市梅州里](../Page/宜蘭市.md "wikilink")）為據點，抗拒官府賤價蒐購糧食。八月十四日夜，董正官會同營都司[劉紹春圍捕吳磋等義民](../Page/劉紹春.md "wikilink")，當時義首林汶英通知吳磋防範，吳磋乃派王強等人埋伏於斗門頭樹林，待董正官通過時將其刺殺並割斷[首級](../Page/首級.md "wikilink")。之後首級滾落到[韭菜園裡](../Page/韭菜.md "wikilink")，是[台灣](../Page/台灣.md "wikilink")[俗諺](../Page/俗諺.md "wikilink")“董大老吃韭”、“董大老死埤口──有功打無勞
”的來源\[4\]。

## 註釋

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

[Category:清朝安溪縣知縣](../Category/清朝安溪縣知縣.md "wikilink")
[Category:清朝霞浦縣知縣](../Category/清朝霞浦縣知縣.md "wikilink")
[Category:清朝同知](../Category/清朝同知.md "wikilink")
[Category:台灣府撫民理番通判](../Category/台灣府撫民理番通判.md "wikilink")
[Category:大理人](../Category/大理人.md "wikilink")
[Z](../Category/董姓.md "wikilink")

1.  [詩詞索引淸董正官 -
    搜韵](https://sou-yun.com/PoemIndex.aspx?dynasty=Qing&author=董正官&lang=t)
2.  [【行书七言对联纸本】拍卖品_图片_价格_鉴赏_书法_雅昌艺术品拍卖网](https://m-auction.artron.net/search_auction.php?action=detail&artcode=art5085710028)
3.  [臺灣道徐宗幹與通判董正官 1853年以前 -
    Gymnoisland](http://gymnoisland.blogspot.com/2018/06/1853.html?m=1)
4.  [蘭陽俗諺錄-民變械鬥類](http://www.lanyangnet.com.tw/speak/html/link6.htm)