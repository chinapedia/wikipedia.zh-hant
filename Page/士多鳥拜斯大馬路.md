[Avenida_de_Sidónio_Pais.jpg](https://zh.wikipedia.org/wiki/File:Avenida_de_Sidónio_Pais.jpg "fig:Avenida_de_Sidónio_Pais.jpg")

**士多紐拜斯大馬路**（），位於[澳門](../Page/澳門.md "wikilink")[東望洋山](../Page/東望洋山.md "wikilink")[西麓](../Page/西麓.md "wikilink")，長785米，原為[二龍喉馬路的其中一段](../Page/二龍喉馬路.md "wikilink")。呈東北—西南走向。以1917至1918年任[葡萄牙總統的](../Page/葡萄牙總統列表.md "wikilink")[士多紐拜斯](../Page/士多鳥拜斯.md "wikilink")（Sidónio
Pais）命名。北起於[鮑思高圓形地](../Page/鮑思高圓形地.md "wikilink")，南至[東望洋街](../Page/東望洋街.md "wikilink")。周邊以住宅為主。沿途有[二龍喉公園](../Page/二龍喉公園.md "wikilink")、[國父紀念館](../Page/國父紀念館_\(澳門\).md "wikilink")、[得勝花園](../Page/得勝花園.md "wikilink")、[塔石廣場等名勝](../Page/塔石廣場.md "wikilink")。

[Avenida_Sidónio_Pais.JPG](https://zh.wikipedia.org/wiki/File:Avenida_Sidónio_Pais.JPG "fig:Avenida_Sidónio_Pais.JPG")
[Sidónio_Pais.jpg](https://zh.wikipedia.org/wiki/File:Sidónio_Pais.jpg "fig:Sidónio_Pais.jpg")

## 沿途街道 (從北到南)

  - [美副將大馬路](../Page/美副將大馬路.md "wikilink")
  - [雅廉訪大馬路](../Page/雅廉訪大馬路.md "wikilink")
  - [山洞巷](../Page/山洞巷.md "wikilink")（直通[松山市政公園](../Page/松山市政公園.md "wikilink")）
  - [高士德大馬路](../Page/高士德大馬路.md "wikilink")
  - [巴士度街](../Page/巴士度街.md "wikilink")
  - [飛良紹街](../Page/飛良紹街.md "wikilink")
  - [羅利老馬路](../Page/羅利老馬路.md "wikilink")
  - [美的路主教街](../Page/美的路主教街.md "wikilink")
  - [沙嘉都喇賈罷麗街](../Page/沙嘉都喇賈罷麗街.md "wikilink")（沙嘉都喇街）
  - [高偉樂街](../Page/高偉樂街.md "wikilink")

## 註釋

## 參考資料

## 外部連結

  - [士多紐拜斯大馬路](https://macaostreets.iacm.gov.mo/c/parishoface5/photoalbum.aspx?album=0b6c4eac-14dc-4c5d-a939-d6d8bdf41e40)

士多紐拜斯大馬路

[S士](../Category/殖民時代紀念葡萄牙的澳門街道.md "wikilink")