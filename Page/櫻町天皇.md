**櫻町天皇**（；），[中御門天皇第一皇子](../Page/中御門天皇.md "wikilink")，母親是[關白](../Page/關白.md "wikilink")[太政大臣](../Page/太政大臣.md "wikilink")[近衛家熙之女](../Page/近衛家熙.md "wikilink")[近衛尚子](../Page/近衛尚子.md "wikilink")（[中和門院](../Page/近衛尚子.md "wikilink")），幼名若宫，諱**昭仁**。1728年立為太子，1735年接受父皇的讓位。在他任內得到當時的將軍[德川吉宗協助](../Page/德川吉宗.md "wikilink")，恢復了許多古代的朝廷儀式，包括曾在[東山天皇時期復興卻又中斷的](../Page/東山天皇.md "wikilink")[大嘗祭](../Page/大嘗祭.md "wikilink")，以及新嘗祭等其他的祭祀禮儀。同時，他在[歌道的造詣也很高](../Page/歌道.md "wikilink")，留有出色的詩歌。由於他和[聖德太子有許多共同的地方](../Page/聖德太子.md "wikilink")，被人稱為是「聖德太子再世」。

1747年，讓位給太子[遐仁親王](../Page/桃園天皇.md "wikilink")，三年後過世，葬於[月輪陵](../Page/月輪陵.md "wikilink")，其追號「櫻町」是出自於[仙洞御所其中一個宮殿](../Page/仙洞御所.md "wikilink")「櫻町殿」。

## 后妃及子女

  - 女御 [二条舍子](../Page/二条舍子.md "wikilink")
      - 第一皇女 [盛子内亲王](../Page/盛子内亲王.md "wikilink")
      - 第二皇女
        [智子内亲王](../Page/後樱町天皇.md "wikilink")（[後樱町天皇](../Page/後樱町天皇.md "wikilink")）
  - 典侍 [姉小路定子](../Page/姉小路定子.md "wikilink")
      - 第一皇子
        [遐仁亲王](../Page/桃园天皇.md "wikilink")（[桃园天皇](../Page/桃园天皇.md "wikilink")）

## 在位期間年號

  - [享保](../Page/享保.md "wikilink")
  - [元文](../Page/元文.md "wikilink")
  - [延享](../Page/延享.md "wikilink")

[Category:江戶時代天皇](../Category/江戶時代天皇.md "wikilink")