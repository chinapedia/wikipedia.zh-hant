**县**（）是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[喀什地区所辖的一个](../Page/喀什地区.md "wikilink")[县](../Page/县.md "wikilink")。总面积为8969平方公里，2002年人口为65万人，2012年人口81万人。

## 历史

莎车县曾有亚儿岗、鸭儿看、叶尔羌等汉语名称\[1\]。县域在[漢代为](../Page/漢代.md "wikilink")\[\[莎車國|

[莎车县](../Category/莎车县.md "wikilink") [县](../Category/喀什县市.md "wikilink")
[喀什](../Category/新疆县份.md "wikilink")
[新](../Category/国家级贫困县.md "wikilink")

1.