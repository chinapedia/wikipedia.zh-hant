**亚历山大·仲马**（，, 原名,
，），19世纪[法国](../Page/法国.md "wikilink")[浪漫主义](../Page/浪漫主义文学.md "wikilink")[文豪](../Page/文豪.md "wikilink")，世界文学名著《[基度山恩仇記](../Page/基度山恩仇記.md "wikilink")》的作者。

大仲马自学成才，主要以[小说和](../Page/小说.md "wikilink")[剧作著称于世](../Page/剧作.md "wikilink")，最著名的作品除了《[基度山恩仇記](../Page/基度山恩仇記.md "wikilink")》之外，還包括《[三剑客](../Page/三剑客.md "wikilink")》、《[二十年后](../Page/二十年后.md "wikilink")》和《[布拉热洛纳子爵](../Page/布拉热洛纳子爵.md "wikilink")》，即[达尔达尼央浪漫三部曲](../Page/达尔达尼央浪漫三部曲.md "wikilink")。他一生創作甚多，著有150多部小說，90多個劇本等，其實大多數是他為了還債，聘用其他文人所寫，掛名「大仲馬著」以便行銷，因此被攻擊為「小說製造工廠的廠長」。\[1\]

「仲马」的译名，是清末翻译家[林纾依](../Page/林纾.md "wikilink")[福州話的語音所译](../Page/福州話.md "wikilink")。其子「小仲馬」也是法国著名文学家、《[茶花女](../Page/茶花女.md "wikilink")》的作者。为区分兩人，遂稱其子為[小仲馬](../Page/小仲馬.md "wikilink")，称之为大仲马（）。

## 家世背景

大仲马的祖父安东尼-亚历山大·达维·德·拉巴叶特里[侯爵曾在法国政府任职炮兵总军需官](../Page/侯爵.md "wikilink")；1760年移居圣多明各（即现在的[海地](../Page/海地.md "wikilink")）。1762年3月27日，一个名叫塞塞特·仲马的女黑奴为他生下一个男孩，取名托马·亚历山大，即大仲马之父，仲馬是這位女黑奴的姓。

1780年前后，侯爵和他的儿子回到巴黎。1786年托马·亚历山大从军，他以仲马为姓入伍。在不久之后爆发的[法国大革命中](../Page/法国大革命.md "wikilink")，他以自己过人的勇武与胆识，从行伍的最底层，成为了[拿破仑麾下的一名将军](../Page/拿破仑.md "wikilink")。

## 生平

大仲马于1802年7月24日生于[法国的](../Page/法国.md "wikilink")[维莱科特雷](../Page/维莱科特雷.md "wikilink")（靠近[巴黎](../Page/巴黎.md "wikilink")），與母親相依為命，到了十三歲還未能好好就學，只能整天在森林遊蕩，肚子餓了，就射獵野鳥以果腹。後來在公證人事務所當見習生，認識了一個叫阿道夫的貴族朋友，引導他進入文學的殿堂，阿道夫帶著大仲馬認識戲劇，以及[拉馬丁等詩人作品](../Page/拉馬丁.md "wikilink")，於是大仲馬立志要成為一個作家。

大仲馬拿了打[彈子贏來的九十塊](../Page/彈子.md "wikilink")[法郎](../Page/法郎.md "wikilink")，前往巴黎打天下，一位將軍看在他父親的面子上，又見他寫得一手好字，推薦他到奧爾良公爵府裏當[書記員](../Page/書記員.md "wikilink")，使他能勉強糊口。由於書法精良，他经常替[法兰西喜剧院誊写文字](../Page/法兰西喜剧院.md "wikilink")，貼補家用，後來忍不住也自己寫起了劇本來，寫了三年後，他的第一齣劇本《亨利三世與其宮廷》使他在文學界嶄露頭角。

1839至1841年，大仲馬在幾位朋友的幫助下，編寫了《著名犯罪》（Celebrated
Crimes）系列文集，主題為歐洲歷史上著名的罪行和罪犯，全8卷，共18篇文章，講述了包括[貝亞特麗切·倩契](../Page/貝亞特麗切·倩契.md "wikilink")、[馬丹·蓋赫](../Page/馬丹·蓋赫.md "wikilink")、[切薩雷·波吉亞和](../Page/切薩雷·波吉亞.md "wikilink")[盧克雷齊亞·波吉亞等人物](../Page/盧克雷齊亞·波吉亞.md "wikilink")。

1844年的《[基督山伯爵](../Page/基督山伯爵.md "wikilink")》一書，使他成為家喻戶曉的大作家，從此聲名不衰。大仲馬風流之極，情婦無數，私生子也無數，[小仲马也是其私生子](../Page/小仲马.md "wikilink")，直到小仲馬七歲時，大仲馬才將之[認領](../Page/認領.md "wikilink")。

大仲馬原本生性豪爽不羈，在成名後就變本加厲，奢靡不堪，他一擲千金，經常遊歷四方，足迹遍及整個欧洲，他以自己筆下的“[基度山伯爵](../Page/基度山伯爵.md "wikilink")”自居，不惜花費巨資二十萬法郎，在巴黎附近的聖日爾曼昂萊森林裏盖了一座新哥德式的「[基督山城堡](../Page/基督山.md "wikilink")」，在那裡款待朋友和情婦們，舉行盛宴和舞會。1847年7月25日，大仲馬宴請50位客人，也包括法國大作家[巴尔扎克](../Page/巴尔扎克.md "wikilink")。他又在一片森林中建造了另一個城堡式的別墅，一樣命名「[基督山城堡](../Page/基督山.md "wikilink")」。但他揮金如土的生活，使他負債累累，他雖然聘請了許多[門客為他](../Page/門客.md "wikilink")[捉刀](../Page/捉刀.md "wikilink")，刊行了大量作品，得到大量的稿酬，但是晚年的大仲马依然非常贫困，為了抵債，他將兩座基度山城堡都拍賣了。他的最後一位[情婦是一位美國女演員](../Page/情婦.md "wikilink")，後來演戲時墜馬摔死，大仲馬埋葬她以後，撐著一把藍色的雨傘，醉醺醺的來到其子小仲馬家中，喊道：“孩子，我是來你這等死的。”半個月後，大仲馬去世，享年六十八歲，小仲馬整理他的遺物，發現大仲馬遺產只有幾塊錢。

2002年，[法國總統](../Page/法國總統.md "wikilink")[席哈克宣布](../Page/席哈克.md "wikilink")，將大仲馬移靈巴黎的[先賢祠](../Page/先賢祠.md "wikilink")，从而與作家[維克多·雨果](../Page/維克多·雨果.md "wikilink")、[左拉等人共享此殊榮](../Page/左拉.md "wikilink")。

## 子女

大仲马的儿子[小仲马也是](../Page/小仲马.md "wikilink")[法国著名的作家](../Page/法国.md "wikilink")，是大仲馬貧困時和一名女裁缝卡特琳·拉贝（Marie-Catherine
Labay）的私生子，大仲馬成名後，混跡於[上流社會](../Page/上流社會.md "wikilink")，將他們母子拋棄，直到小仲馬七歲時，大仲馬終於良心發現，在法律上承認了這個兒子，雖然大仲马仍担负着拉贝的生活费用，但是他始终没有承认拉贝是他的妻子。小仲馬善于写戏剧，其中《[茶花女](../Page/茶花女.md "wikilink")》等为代表作。

## 政治信仰

大仲马信守[共和政见](../Page/共和.md "wikilink")，反对君主专政。他因為自己的[黑人](../Page/黑人.md "wikilink")、[白人的](../Page/白人.md "wikilink")[混血兒身份](../Page/混血兒.md "wikilink")，一生都受[种族主义的困扰](../Page/种族主义.md "wikilink")\[2\]。

## 文学特色

大仲馬有句名言：「什麼是歷史？就是給我掛小說的釘子啊。」他的小說大多以真实的历史作背景，情节曲折生动，往往出人意料，有历史惊险小说之称。结构清晰明朗，语言生动有力，对话灵活机智等构成了大仲马小说的特色。大仲馬並非小說的唯一筆者，有一位中學教師也參與創作。馬凱經人介紹，把三幕劇《狂歡節之夜》送去請大仲馬修改潤色，改名為《巴蒂爾德》，在巴黎舞臺上公演獲得好評。馬凱又寫歷史小說《老好人杜韋》，將手稿送給大仲馬過目。大仲馬把內容單薄的《老好人杜韋》改寫成《德·阿芒達爾騎士》，在《》上連載大獲成功。大仲馬從此欲罷不能，一生寫下數百部的小說。英國學者、詩人[安德鲁·朗格說過](../Page/安德鲁·朗格.md "wikilink")：「大仲馬在一展歌喉之前，先得有個音叉定一下音；而他一旦認準了音高，就能一瀉千里地唱下去。」

一本大仲馬的傳記說：“如果您只要讀一本大仲馬的小說，那請讀《[三劍客](../Page/三劍客.md "wikilink")》；如果您有時間讀上三本，那麼請加上《[基督山恩仇記](../Page/基督山恩仇記.md "wikilink")》和《[玛戈王后](../Page/玛戈王后.md "wikilink")》；如果您要讀五本，再加上《[二十年後](../Page/二十年後.md "wikilink")》和《》；如果您選擇十冊大仲馬的作品，那麼便再加上《大野心家》、《》、《我的回憶錄》、《安東尼》和《[布拉热洛纳子爵](../Page/布拉热洛纳子爵.md "wikilink")》。如果這十本書您都看過了，那麼您鐵定已經上了癮，毋須我們推薦您閱讀其他的作品了……”

他一生著有150多部小說，90多個劇本，文集250卷，創作量驚人，作品多達兩百七十卷，他也是一位美食家，甚至还写了一部《烹饪大全》，在法國通俗文學的魅力歷久不衰。不過由於經濟問題，他以大量的作品賺取巨額的稿酬，託人代寫、[捉刀](../Page/捉刀.md "wikilink")，成為他謀生必要的手段，甚至有些作品大仲馬自己都沒看過，因此大仲馬的作品普遍良莠不齊，飽受研究者的批評。

## 主要作品

### 历史小说

  - \-{zh-hans:达达尼昂; zh-hant:達太安;}-三部曲(背景为路易十四时代, 法国鼎盛时期)：

<!-- end list -->

1.  《[三个火枪手](../Page/三个火枪手.md "wikilink")》（或譯《[三剑客](../Page/三剑客.md "wikilink")》，旧译《[俠隱記](../Page/俠隱記.md "wikilink")》）（第1及2卷）Les
    Trois Mousquetaires \[法\]，The Three Musketeers\[英\]
2.  《[二十年后](../Page/二十年后.md "wikilink")》（旧译《[續俠隱記](../Page/續俠隱記.md "wikilink")》）（第3至5卷）Vingt
    ans après \[法\]，Twenty Years After \[英\]
3.  《[布拉热洛纳子爵](../Page/布拉热洛纳子爵.md "wikilink")》（又译《[布拉日隆子爵](../Page/布拉日隆子爵.md "wikilink")》，旧译《[鐵面人](../Page/鐵面人.md "wikilink")》《[後續俠隱記](../Page/後續俠隱記.md "wikilink")》《[小俠隱記](../Page/小俠隱記.md "wikilink")》）（第6至11卷）Le
    Vicomte de Bragelome\[法\]，The Vicomte de Bragelonne\[英\]

<!-- end list -->

  - 瓦洛朝三部曲(也作：“三亨利之战”的三部曲, 背景为法国宗教战争时期)

<!-- end list -->

1.  《[瑪歌王后](../Page/瑪歌王后.md "wikilink")》（又译《[玛戈王后](../Page/玛戈王后.md "wikilink")》，1845年，La
    Reine Margot \[法\]）
2.  《[蒙梭罗夫人](../Page/蒙梭罗夫人.md "wikilink")》La Dame de Monsoreau \[法\]
3.  《[四十五卫士](../Page/四十五卫士.md "wikilink")》The Forty-five Guardsmen \[英\]

描写法国君主制崩溃的系列小说(背景为法国大革命时期)：

  - 《[约瑟·巴尔萨莫](../Page/约瑟·巴尔萨莫.md "wikilink")》（又译《[风雨术士巴尔萨摩](../Page/风雨术士巴尔萨摩.md "wikilink")》）Joseph
    Balsamo \[英\]
  - 《[王后的项链](../Page/王后的项链.md "wikilink")》Le Collier de la Reine
    \[法\]，The Queen's Necklace \[英\]
  - 《[红房子骑士](../Page/红房子骑士.md "wikilink")》(又译《[红屋骑士](../Page/红屋骑士.md "wikilink")》）Le
    Chevatier de Maison-Rogue \[法\]，The chevalier de Maison Rouge \[英\]
  - 《[昂热·皮都](../Page/昂热·皮都.md "wikilink")》Ange Pitou \[法\]

描写拿破仑时期的小说：

  - 《[双雄记](../Page/双雄记.md "wikilink")》（《又名[杀手与侠盗](../Page/杀手与侠盗.md "wikilink")》，《[耶羽的伙伴](../Page/耶羽的伙伴.md "wikilink")》）Les
    compagnons de Jéhu \[法\]，The Companions of Jehu \[英\]
  - 《[白与蓝](../Page/白与蓝.md "wikilink")》Les.Blancs.et.les.Bleus.\[法\]
    White and Blue\[英\]
  - 《[最后的骑士](../Page/最后的骑士.md "wikilink")》Le Chevalier de
    Sainte-Hermine\[法\]The.Last.Cavalier\[英\]
  - 《[基督山伯爵](../Page/基督山伯爵.md "wikilink")》（或译《[基度山恩仇記](../Page/基度山恩仇記.md "wikilink")》）Le
    Comte de Monte-Cristo \[法\]，The Count of Monte Cristo \[英\]

为数罕见的爱情小说：

  - 《[黑郁金香](../Page/黑郁金香.md "wikilink")》（又译《[黑色郁金香](../Page/黑色郁金香.md "wikilink")》）Tulipe
    noire \[法\]，The Black Tulip \[英\]

其他小说：

  - 《[阿斯加尼奧](../Page/阿斯加尼奧.md "wikilink")》Ascanio \[英\]
  - 《[末日暴君](../Page/末日暴君.md "wikilink")》
  - 《[裙釵之戰](../Page/裙釵之戰.md "wikilink")》
  - 《[惡狼司令](../Page/惡狼司令.md "wikilink")》
  - 《[俠盜羅賓漢](../Page/俠盜羅賓漢.md "wikilink")》
  - 《[蘇后瑪麗慘史](../Page/蘇后瑪麗慘史.md "wikilink")》

### 戏剧

  - 《[亨利三世及其宫廷](../Page/亨利三世及其宫廷.md "wikilink")》（1829年）：开创了新的文学体裁--历史剧，[浪漫主义](../Page/浪漫主义.md "wikilink")[戏剧](../Page/戏剧.md "wikilink")，破除了[古典主义](../Page/古典主义.md "wikilink")“[三一律](../Page/三一律.md "wikilink")”
  - 《[安东尼](../Page/安东尼.md "wikilink")》（1831年）
  - 《[拿破仑](../Page/拿破仑.md "wikilink")》（1831年）

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  -
[大仲马](../Category/大仲马.md "wikilink")
[Category:法国小说家](../Category/法国小说家.md "wikilink")
[Category:法国剧作家](../Category/法国剧作家.md "wikilink")
[Category:葬於先賢祠](../Category/葬於先賢祠.md "wikilink")
[Category:19世紀小說家](../Category/19世紀小說家.md "wikilink")
[Category:历史小说家](../Category/历史小说家.md "wikilink")

1.  《金庸與池田大作的對話——漫談世界名著》
2.  一日在沙龍裡，巴尔扎克拒绝与大仲马碰杯，傲慢的说：“等我才华用尽时，我就去写剧本了。”大仲马回答：“那你现在就可以开始了！”巴尔扎克恼火的說：“在我写剧本之前，还是先请你给我谈谈你的祖先吧，这倒是个绝妙的题材。”大仲马回答：“我的父亲是个克里奥尔人，我的祖父是个黑人，我的曾祖父是个猴子，我的家就是在你家搬走的地方发源的！”