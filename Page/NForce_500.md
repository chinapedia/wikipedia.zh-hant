**nForce
500**系列，是[NVIDIA推出的第五代](../Page/NVIDIA.md "wikilink")[主機板](../Page/主機板.md "wikilink")[晶片組](../Page/晶片組.md "wikilink")，是[nForce
4的後繼產品](../Page/nForce_4.md "wikilink")。支援[AMD的](../Page/AMD.md "wikilink")[Socket
AM2處理器](../Page/Socket_AM2.md "wikilink")，於2006年3月7日首次公開\[1\]，並於5月23日與Socket
AM2處理器同日推出。

## 產品規格

  - 支援[SLI雙卡](../Page/SLI.md "wikilink")、日後推出的Quad SLI多卡技術及SLI
    [LinkBoost功能](../Page/LinkBoost.md "wikilink")。
  - 支援最多6部[SATA](../Page/SATA.md "wikilink")
    3Gb/s[硬碟機及](../Page/硬碟機.md "wikilink")10組[USB
    2.0裝置](../Page/通用串行总线.md "wikilink")。
  - 只支援一个[IDE接口](../Page/IDE.md "wikilink")。
  - 支援雙重[RAID 5](../Page/RAID.md "wikilink")。
  - 支援五大技術：
      - [LinkBoost技术](../Page/LinkBoost.md "wikilink")
      - [FirstPacket技术](../Page/FirstPacket.md "wikilink")
      - [DualNet技术](../Page/DualNet.md "wikilink")
      - [Teaming技术](../Page/DualNet#Teaming.md "wikilink")
      - [TCP/IP加速技术](../Page/TCP/IP.md "wikilink")
      - [MediaShield技术](../Page/MediaShield.md "wikilink")
      - [nTune](../Page/nTune.md "wikilink") 5.0

## 產品型號

### AMD平臺

#### nForce 590 SLI [MCP](../Page/MCP.md "wikilink")

  - 採用南北桥设计。
  - 南北桥各提供16条[PCI
    Express管线](../Page/PCI_Express.md "wikilink")，支援真正的双PCI-E
    16x，將[SLi效能完全發揮](../Page/SLI.md "wikilink")。
  - 极致的双GPU支持，支持所有特性和Quad SLI技术。
  - [北桥為C](../Page/北桥.md "wikilink")51XE、[南桥為MCP](../Page/南桥.md "wikilink")55XE。南北桥採用[HyperTransport](../Page/HyperTransport.md "wikilink")
    5x連接，頻寬高達8 GB/s。整套芯片组一共提供48条[PCI
    Express管线](../Page/PCI_Express.md "wikilink")。
  - 南桥支援兩個千兆以太网络接口。
  - 提供6个[SATA-2接口](../Page/SATA-2.md "wikilink")，1个[ATA接口](../Page/ATA.md "wikilink")。

#### nForce 570 SLI

  - 性能级双GPU支持，缺少SLI LinkBoost技术。
  - 总共28条PCI Express管线。

#### nForce 570 LT SLI

效能與nForce 570
SLI相近，但功能有所削減。包括PCI-E通道減至19條，只支援4個SATA接口，只有一個網絡接口，不包含TCP/IP加速技術和Teaming网络技术。當然保持支援[SLI技術](../Page/SLI.md "wikilink")，從名稱可推測，這個晶片組是衝著AMD的570X晶片組而來。

#### nForce 570 (Ultra)

  - 性能级单GPU支持，缺少SLI技术。
  - 总共20条PCI Express管线。

#### nForce 550

  - 主流单GPU支持，没有SLI和RAID 5技术，并且只有一个网络接口和4个SATA接口。
  - 总共20条PCI Express管线。

#### nForce 510

它是nForce 550的簡化版本，分別是刪去了一組GbE和一組[PCI-E](../Page/PCI-E.md "wikilink")。

#### nForce 500

是[nForce 4晶片組系列的更名產品](../Page/nForce_4.md "wikilink")。與其他nForce
5產品的分別是它能提供兩組[IDE介面](../Page/IDE.md "wikilink")，但只有四組[SATA介面](../Page/SATA.md "wikilink")。

  - nForce 500 SLI
  - nForce 500 Ultra
  - nForce 500

### Intel平臺

#### nForce 590 SLI

在2006年7月27日发布，支援[Intel的](../Page/Intel.md "wikilink")[Core 2
Duo处理器](../Page/Intel_Core_2.md "wikilink")

#### nForce 570 SLI

在2006年7月27日发布，支援[Intel的](../Page/Intel.md "wikilink")[Core 2
Duo处理器](../Page/Intel_Core_2.md "wikilink")

## 相關條目

  - [NVIDIA晶片組列表](../Page/NVIDIA晶片組列表.md "wikilink")

## 參考鏈接

## 外部連結

  - [nForce 500 AMD平臺官方網頁](http://www.nvidia.com/page/nforce5_amd.html)
  - [nForce 500
    Intel平臺官方網頁](http://www.nvidia.com/page/nforce5_intel.html)

[Category:主板](../Category/主板.md "wikilink")
[Category:英伟达](../Category/英伟达.md "wikilink")

1.  [NVIDIA to Demonstrate Next-Generation NVIDIA® nForce® 500 Family of
    Core-Logic Solutions at
    Cebit](http://www.nvidia.com/object/IO_30177.html)