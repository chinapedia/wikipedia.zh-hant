[Reichsbank_berlin_jaegerstr.jpg](https://zh.wikipedia.org/wiki/File:Reichsbank_berlin_jaegerstr.jpg "fig:Reichsbank_berlin_jaegerstr.jpg")

**德意志帝國銀行**（[德语](../Page/德语.md "wikilink")：****），曾是[德國的](../Page/德國.md "wikilink")[中央銀行](../Page/中央銀行.md "wikilink")，建於1876年，1948年結束營業。

## 歷史

帝國銀行是取代普魯士中央銀行（Central Bank of
Prussia）而建立的。它首任的主席[赫爾曼·馮·戴程德](../Page/赫爾曼·馮·戴程德.md "wikilink")（Hermann
von Dechend）。

在1871年[普鲁士统一德国并建立](../Page/普鲁士.md "wikilink")[德意志帝國之前](../Page/德意志帝國.md "wikilink")，德國境內有31家中央銀行（又稱Notenbanken，Note
Bank）。每一個獨立的[邦國均可發行他們自己的](../Page/邦國.md "wikilink")[紙幣](../Page/紙幣.md "wikilink")。在1870年時，為了方便日後的統一工作，曾有一道法令被通過，禁止再增加中央銀行。1874年德國的銀行法送入德國國會（Reichstag，German
parliament）辯論，在一連串的轉變與折衷後，銀行法在1875年通過。大多數的邦國不得再發行自己的鈔券，除了四個邦國之外，全部使用統一的[德國馬克](../Page/德國馬克.md "wikilink")，至此完成了德國的[貨幣統一工作](../Page/貨幣.md "wikilink")。這四個邦國分別是[巴登大公國](../Page/巴登大公國.md "wikilink")（Baden）、[巴伐利亞王國](../Page/巴伐利亞王國.md "wikilink")（Bayern）、[薩克森王國](../Page/薩克森王國.md "wikilink")（Sachsen）與[符騰堡王國](../Page/符騰堡王國.md "wikilink")（Württemberg），這四個邦國獨立發行國內通貨，直到1935年。
[100MARK.jpg](https://zh.wikipedia.org/wiki/File:100MARK.jpg "fig:100MARK.jpg")

德意志帝國銀行的歷史是起伏不定的，[第一次世界大戰前](../Page/第一次世界大戰.md "wikilink")，它發行的是號稱永不貶值的[金馬克](../Page/金馬克.md "wikilink")；然爾在戰爭支出與[通貨膨脹的壓力下](../Page/通貨膨脹.md "wikilink")，馬克不斷[貶值](../Page/貶值.md "wikilink")。在德國1922年至1923年通貨膨漲的高潮中，馬克變成了紙幣本位馬克（Papiermark，paper
mark），鈔券的面值不斷膨漲，但其[購買力卻繼續探底](../Page/購買力.md "wikilink")。到了通貨膨漲的末期，為了顧及印鈔成本，成千上億面額的鈔券只採單面印刷，至此已不得不進行金融改革。

在金融改革之中，德意志帝國銀行發行了一種新的儲備通貨－地產抵押馬克（Rentenmark，貨幣代號RM），以一地產馬克兌一兆紙幣馬克全面回收已經膨漲到不像話的紙幣馬克。在百廢待舉之際，德國政府在失去一切[準備金的情況下](../Page/準備金.md "wikilink")，想到以[土地為](../Page/土地.md "wikilink")[擔保而發行新貨幣](../Page/擔保.md "wikilink")，實在令人產生敬意。1924年，德國政府重新以帝國馬克（Reichsmark）等值兌換地產抵押馬克，結束了一段空前的惡性通貨膨漲，此次通貨膨漲的速度至今仍是世界金氏紀錄之最。

1924年的帝國馬克一直使用到1948年，與帝國銀行一同滅亡。

## 歷任主席

  - 1876–1890 赫爾曼·馮·戴程德 (Hermann von Dechend)
  - 1890–1908 理查德·科赫 (Richard Koch)
  - 1908–1923 [魯道夫·哈芬史坦](../Page/魯道夫·哈芬史坦.md "wikilink") (Rudolf E. A.
    Havenstein)
  - 1923–1930 [亞爾馬·沙赫特](../Page/亞爾馬·沙赫特.md "wikilink") (Hjalmar Schacht)
  - 1930–1933 [漢斯·路德](../Page/漢斯·路德.md "wikilink") (Hans Luther)
  - 1933–1939 亞爾馬·沙赫特 (Hjalmar Schacht)
  - 1939–1945 瓦爾特·馮克 (Walther Funk)

*有關1945年後自德國央行, 請參見:* [德國聯邦銀行](../Page/德國聯邦銀行.md "wikilink")

[Category:德国已结业银行](../Category/德国已结业银行.md "wikilink")
[Category:德国政府历史](../Category/德国政府历史.md "wikilink")
[Category:已结业中央银行](../Category/已结业中央银行.md "wikilink")
[Category:1876年成立的银行](../Category/1876年成立的银行.md "wikilink")
[Category:1948年廢除](../Category/1948年廢除.md "wikilink")