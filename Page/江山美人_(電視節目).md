《**江山美人**》（[英文](../Page/英语.md "wikilink")：），[香港](../Page/香港.md "wikilink")[電視廣播有限公司製作的一個集旅遊與選美於一身的電視節目](../Page/電視廣播有限公司.md "wikilink")。此節目於2006年10月15日晚上20:30起於[無綫電視旗下頻道](../Page/無綫電視.md "wikilink")[翡翠台播放](../Page/翡翠台.md "wikilink")。而決賽《**江山美人一戰定江山**》則於同年12月23日晚上20:30於[翡翠台播出](../Page/翡翠台.md "wikilink")。節目由[香港](../Page/香港.md "wikilink")[康泰旅行社贊助](../Page/康泰旅行社.md "wikilink")。

## 節目形式

本節目分別於[中国內地八個城市](../Page/中国內地.md "wikilink")[大連](../Page/大連.md "wikilink")、[蘇州](../Page/蘇州.md "wikilink")、[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[西安](../Page/西安.md "wikilink")、[成都](../Page/成都.md "wikilink")、[廣州和](../Page/廣州.md "wikilink")[南寧取景](../Page/南寧.md "wikilink")。每個城市均有四名參賽者，參賽者會於節目中與主持人遊覽該城市並進行問答與才藝比試，其中一名參賽者可獲得出線資格到[香港參加決賽](../Page/香港.md "wikilink")。

於決賽勝出者則入圍參與[國際中華小姐競選](../Page/國際中華小姐競選.md "wikilink")，本節目被視作[國際中華小姐競選引入內地參賽者後的選拔賽](../Page/國際中華小姐競選.md "wikilink")，然而由2008年度[國際中華小姐競選起](../Page/國際中華小姐競選.md "wikilink")，改以[國際中華小姐競選內地選拔賽代替](../Page/國際中華小姐競選#內地選拔賽.md "wikilink")。

## 外部連結

  - [電視廣播有限公司官方網站 -
    江山美人](http://jade.tvb.com/sponsorship/hongthai_0610/)
  - [電視廣播有限公司官方網站 -
    江山美人一戰定江山](http://jade.tvb.com/special/homeland_beauty/)

[Category:2006年無綫電視節目](../Category/2006年無綫電視節目.md "wikilink")
[Category:無綫電視旅遊節目](../Category/無綫電視旅遊節目.md "wikilink")
[Category:國際中華小姐競選](../Category/國際中華小姐競選.md "wikilink")
[Category:真人秀節目](../Category/真人秀節目.md "wikilink")