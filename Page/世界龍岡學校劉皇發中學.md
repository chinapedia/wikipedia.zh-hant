**世界龍岡學校劉皇發中學**（）位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[大角咀](../Page/大角咀.md "wikilink")[詩歌舞街](../Page/詩歌舞街.md "wikilink")66號。

## 著名校友

  - [張益麟](../Page/張益麟.md "wikilink")：興迅實業有限公司集團主席及董事總經理、香港青年工業家2011年得獎人、香港工業總會31组主席、香港社會創業論譠主席
  - [林衍蕙](../Page/林衍蕙.md "wikilink")：[香港女子](../Page/香港.md "wikilink")[劍擊運動員](../Page/劍擊.md "wikilink")\[1\]
  - [林衍聰](../Page/林衍聰.md "wikilink")：2014年亞運會男子個人及團體賽佩劍銅牌得主
  - [聶德寶](../Page/聶德寶.md "wikilink")：前[亞視新聞部助理採訪主任](../Page/亞視新聞.md "wikilink")
  - [袁源隆](../Page/袁源隆.md "wikilink")：2014－2015年度[香港大學學生會](../Page/香港大學學生會.md "wikilink")《學苑》總編輯
  - [鍾沛枝](../Page/鍾沛枝.md "wikilink")：前香港女藝人（曾在校就讀中四及中五）
  - [沈偉銓](../Page/沈偉銓.md "wikilink")：天使投資基金會聯合創辦人兼執行主席
  - [梁雪湄](../Page/梁雪湄.md "wikilink")：前香港女藝人
  - [雷寶宜](../Page/雷寶宜.md "wikilink")：香港女子音樂組合[Honey
    Bees成員](../Page/Honey_Bees.md "wikilink")
  - [蘇皓兒](../Page/蘇皓兒.md "wikilink")：香港女子音樂組合[As
    One成員](../Page/As_One.md "wikilink")、演員

## 與學校相關之人物

  - [劉皇發](../Page/劉皇發.md "wikilink")：前[鄉議局主席](../Page/鄉議局.md "wikilink")
  - [劉盛良](../Page/劉盛良.md "wikilink")：現任校董會顧問，曾任[中華民國立法委員](../Page/中華民國立法委員.md "wikilink")
  - [趙守博](../Page/趙守博.md "wikilink")：現任校董會顧問，前[台灣省政府主席](../Page/台灣省政府.md "wikilink")
  - [關中](../Page/關中_\(人物\).md "wikilink")：前校董（以世界龍岡親義總會理事長身份擔任崗位），曾任[中國國民黨副主席](../Page/中國國民黨.md "wikilink")、前[中華民國考試院院長](../Page/中華民國考試院.md "wikilink")
  - [張平沼](../Page/張平沼.md "wikilink")：前校董會顧問，曾任中華民國立法委員，現任台灣全國總商會理事長、[國立台北大學校友總會理事長](../Page/國立台北大學.md "wikilink")

## 外部連結

  - [學校網址](http://www.lwfss.edu.hk)
  - [Google
    Map](http://maps.google.com/maps?f=q&hl=en&ie=UTF8&z=18&ll=22.326031,114.162417&spn=0.001518,0.002414&t=k&om=0)
  - [龍岡起源 -
    世界龍岡學校劉皇發中學由劉姓、關姓、張姓、趙姓等四姓後人創辦及經營](https://web.archive.org/web/20060505031243/http://www.lwfss.edu.hk/School%20info/history1.htm)

[Category:大角咀](../Category/大角咀.md "wikilink")
[L](../Category/油尖旺區中學.md "wikilink")
[Category:1977年創建的教育機構](../Category/1977年創建的教育機構.md "wikilink")

1.  [《世界龍岡學校劉皇發中學 二零零七至零八年度 學校周年報告》
    過去三年在國際或學校際活動中獲得的獎項，2006年](https://wenku.baidu.com/view/70b2d062783e0912a2162aa4.html)