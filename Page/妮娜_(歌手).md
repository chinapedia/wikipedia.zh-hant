**妮娜**（**Nena**，本名**Gabriele Susanne
Kerner**，）是一位[德國](../Page/德國.md "wikilink")[搖滾流行音樂](../Page/搖滾樂.md "wikilink")[歌手](../Page/歌手.md "wikilink")，出生於[哈根](../Page/哈根.md "wikilink")。Nena原本也是她的團名，成名曲為《》及《》。

## 外部連結

  - [Official Web Site](http://www.nena.de)
  - [Comprehensive Nena fansite](http://www.nenafan.de)
  - [Nena fan community](http://www.nenasound.de/forum)
  - [Comprehensive English fan
    site](https://web.archive.org/web/20071215033403/http://www.freewebs.com/nenafan99/)
  - [Nena CDs + DVDs at
    Amazon](https://web.archive.org/web/20070929175336/http://astore.amazon.de/nena-21)


[Category:德國歌手](../Category/德國歌手.md "wikilink")