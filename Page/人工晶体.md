**人工晶体**（），是一种植入眼内的人工透镜，取代天然[晶状体的作用](../Page/晶状体.md "wikilink")。第一枚人工晶体是由John
Pike,John Holt和Hardold
Ridley共同设计的，于1949年11月29日，Ridley医生在[伦敦St](../Page/伦敦.md "wikilink").Thomas医院为病人植入了首枚人工晶体。

在[二次大战中](../Page/二次大战.md "wikilink")，[科學家观察到某些受伤的](../Page/科學家.md "wikilink")[飞行员眼中有玻璃碎片](../Page/飞行员.md "wikilink")，却没有引起明显的、持续的[炎症反应](../Page/炎症.md "wikilink")，于是想到[玻璃或者一些高分子有机材料可以在眼内保持稳定](../Page/玻璃.md "wikilink")，由此发明了人工晶体。

人工晶体的形态，通常是由一个圆形光学部和周边的支撑袢组成，光学部的直径一般在5.5-6mm左右，这是因为，在夜间或暗光下，人的瞳孔会放大，直径可以达到6mm左右，而过大的人工晶体在制造或者手术中都有一定的困难，因此主要生产厂商都使用5.5-6mm的光学部直径。支撑袢的作用是固定人工晶体，形态就很多了，基本的可以是两个C型的线装支撑袢。

台灣[衛生福利部表示](../Page/衛生福利部.md "wikilink")，一般功能人工水晶體（非特殊功能人工水晶體）具有良好的長期穩定性，且經台灣眼科醫師長達二十年的使用經驗，已足敷百分之九十以上白內障病患使用。\[1\]

## 參考文獻

### 延伸閱讀

  -
  - [人工水晶體-臺大醫院](https://www.ntuh.gov.tw/OPH/DocLib10/%E7%99%BD%E5%85%A7%E9%9A%9C.aspx)

[Category:眼科](../Category/眼科.md "wikilink")
[Category:人工器官](../Category/人工器官.md "wikilink")
[Category:光學](../Category/光學.md "wikilink")

1.