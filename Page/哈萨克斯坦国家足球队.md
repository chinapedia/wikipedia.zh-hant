**哈萨克斯坦国家足球队**(;
)是[哈萨克斯坦足球协会下的国家足球代表队](../Page/哈萨克斯坦足球协会.md "wikilink")，在國家從[蘇聯分裂後成立](../Page/蘇聯.md "wikilink")。

哈萨克斯坦国家足球队自[苏联解体后先加入](../Page/苏联.md "wikilink")[亚洲足球联合会](../Page/亚洲足球联合会.md "wikilink")，2002年转入[欧洲足球联合会](../Page/欧洲足球联合会.md "wikilink")。成立至今國家隊未曾打入任何重要國際大賽。

## 世界杯紀錄

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/世界盃足球賽.md" title="wikilink">世界盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="odd">
<td><p>總結</p></td>
</tr>
</tbody>
</table>

## 歐洲國家杯紀錄

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/欧洲足球锦标赛.md" title="wikilink">歐國盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年歐洲國家盃.md" title="wikilink">1960年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年歐洲國家盃.md" title="wikilink">1964年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年歐洲國家盃.md" title="wikilink">1968年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年歐洲國家盃.md" title="wikilink">1972年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年歐洲國家盃.md" title="wikilink">1976年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年歐洲國家盃.md" title="wikilink">1980年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984年歐洲國家盃.md" title="wikilink">1984年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年歐洲國家盃.md" title="wikilink">1988年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992年歐洲國家盃.md" title="wikilink">1992年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1996年歐洲國家盃.md" title="wikilink">1996年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年歐洲國家盃.md" title="wikilink">2000年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年歐洲國家盃.md" title="wikilink">2004年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年歐洲國家盃.md" title="wikilink">2008年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2012年歐洲國家盃.md" title="wikilink">2012年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年歐洲國家盃.md" title="wikilink">2016年</a></p></td>
</tr>
<tr class="odd">
<td><p>總結</p></td>
</tr>
</tbody>
</table>

## 近賽成績

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽C組](../Page/2014年世界盃外圍賽歐洲區C組.md "wikilink")

### [2016年歐洲國家盃](../Page/2016年歐洲國家盃外圍賽.md "wikilink")

### [2018年世界盃](../Page/2018年世界盃足球賽.md "wikilink")

  - [外圍賽E組](../Page/2018年世界盃外圍賽歐洲區E組.md "wikilink")

## 外部連結

  - [Chempionat.kz](http://chempionat.kz)
  - [Fan Website](http://www.sbornaya.kz)
  - [Kazakh's
    Football](https://web.archive.org/web/20111211224232/http://lyakhov.kz/football/)

  - [RSSSF](http://www.rsssf.com/tablesk/kaz-intres.html)
  - [Transfermarkt.de](https://web.archive.org/web/20100315085757/http://transfermarkt.de/de/nationalmannschaft/9110/kasachstan/uebersicht/startseite.html)


[Category:歐洲國家足球隊](../Category/歐洲國家足球隊.md "wikilink")
[Category:亞洲足球代表隊](../Category/亞洲足球代表隊.md "wikilink")
[哈薩克國家足球隊](../Category/哈薩克國家足球隊.md "wikilink")
[Football](../Category/哈薩克體育國家隊.md "wikilink")