《**机器鸡**》或《**机器肉鸡**》（Robot
Chicken）是一部获得过[艾美奖的美国系列动画](../Page/艾美奖.md "wikilink")，由[塞思·格林](../Page/塞思·格林.md "wikilink")（Seth
Green）与马修·森瑞克（Matthew
Senreich）创作并担任执行制作人。他们也属于编剧组的成员，并导演过一些剧集。格林还为剧中多个角色配音。

该剧使用各种活动玩偶、粘土动画的静态活动场景等来[恶搞当今热门的流行文化](../Page/恶搞.md "wikilink")。机器鸡这个名字是由格林与森瑞克在西好莱坞市的一家中国餐厅用餐时从菜单上的一道菜名想到的。这个系列动画原本准备取名为《Junk
in the Trunk》。\[1\]

该动画片虽然名为“机器鸡”，但每集里机器雞只會出现在片头音乐的时候。在片头里，一个疯狂的科学家在路边捡到一只摔死的鸡，将它带回实验室改造成一只半机器化的鸡，之后将它绑在椅子上，用反射镜强行保持它的眼睛睁开，并强行让它盯住一面有着许多小屏幕的电视墙。然后这面电视墙上的场景渐变为节目的内容。

## 创作团队

### 联席主管作者

  - [Doug Goldstein](../Page/Doug_Goldstein.md "wikilink")
  - [Tom Root](../Page/Tom_Root.md "wikilink")
  - [bob marley](../Page/bob_marley.md "wikilink")
  - [scatman](../Page/scatman.md "wikilink")

### 作者

  - Hugh Davidson
  - Jordan Allen-Dutton
  - [Mike Fasolo](../Page/Mike_Fasolo.md "wikilink")
  - [塞思·格林](../Page/塞思·格林.md "wikilink")
  - [Charles Horn](../Page/Charles_Horn.md "wikilink")
  - [Breckin Meyer](../Page/Breckin_Meyer.md "wikilink")
  - [Dan Milano](../Page/Dan_Milano.md "wikilink")
  - [Pat McCallum](../Page/Pat_McCallum.md "wikilink")
  - Harp Pekin
  - Ben Schwartz
  - [Matthew Senreich](../Page/Matthew_Senreich.md "wikilink")
  - [Kevin Shinick](../Page/Kevin_Shinick.md "wikilink")
  - Erik Weiner
  - [Zeb Wells](../Page/Zeb_Wells.md "wikilink")

### 动画师

  - [Melissa Goodwin](../Page/Melissa_Goodwin.md "wikilink")
  - Joshua A. Jennings
  - Pete Levin
  - Ethan Marak
  - Kelly Mazurowski
  - [Sarah E. Meyer](../Page/Sarah_E._Meyer.md "wikilink")
  - [Michael Wolfe](../Page/Michael_Wolfe.md "wikilink")
  - Cameron Baity
  - John Harvatine IV
  - Liz Harvatine
  - Martin Jimenez
  - Jeff Riley
  - Gabe Sprenger
  - Matt Manning
  - Misha Klein
  - Eileen K. Kohlhepp
  - Joe Mello
  - Sihanouk Mariona
  - [Thomas Smith](../Page/Thomas_Smith.md "wikilink")
  - Alex Kamer
  - Scott Kravitz
  - Suzanne Twining
  - Musa Brooker
  - Thomas Truax

## 获奖情况

《机器鸡》于2006与2007年两度获得[艾美奖](../Page/艾美奖.md "wikilink")“动画片杰出个人贡献奖”，并凭借《Lust
for Puppets》这一集于2007年获得过艾美奖“杰出动画奖”（短于1小时节目）提名。

## 参考文献

## 外部链接

  -
  -
  -
[Category:美國動畫影集](../Category/美國動畫影集.md "wikilink")
[Category:美國成人動畫](../Category/美國成人動畫.md "wikilink")
[Category:黑色幽默](../Category/黑色幽默.md "wikilink")
[Category:搞笑動畫](../Category/搞笑動畫.md "wikilink")
[Category:虚构鸡](../Category/虚构鸡.md "wikilink")
[Category:黑色幽默題材電視節目](../Category/黑色幽默題材電視節目.md "wikilink")
[Category:讽刺类电视节目](../Category/讽刺类电视节目.md "wikilink")

1.  <http://www.g4tv.com/screensavers/episodes/3902/Robot_Chicken_Constantine_Dark_Tip.html>