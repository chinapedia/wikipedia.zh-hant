**劳动教养**，[中华人民共和国](../Page/中华人民共和国.md "wikilink")1957年至2013年间执行的一种所谓“[劳动](../Page/劳动.md "wikilink")、[教育和培养](../Page/教育.md "wikilink")”的制度，简称**劳教**。劳动教养制度是[中华人民共和国从](../Page/中华人民共和国.md "wikilink")[苏联引进的](../Page/苏联.md "wikilink")，但和苏联的相关制度并不完全相同，中华人民共和国形成了独有的[劳动改造和劳动教养两种不同制度](../Page/劳动改造.md "wikilink")。从[法律上讲](../Page/法律.md "wikilink")，中华人民共和国的劳动教养制度始于1957年，它并不是《[中华人民共和国刑法](../Page/中华人民共和国刑法.md "wikilink")》规定的[刑罚](../Page/刑罚.md "wikilink")，而是相关[行政法规规定的一种](../Page/行政法规.md "wikilink")[行政罚](../Page/行政罚.md "wikilink")；[公安机关无须经](../Page/公安机关.md "wikilink")[法院对](../Page/法院.md "wikilink")[疑犯进行审讯定罪](../Page/疑犯.md "wikilink")，即可将疑犯投入劳教场所，实行最高期限为四年的限制[人身自由](../Page/人身自由.md "wikilink")、[强迫劳动](../Page/强迫劳动.md "wikilink")、思想教育等措施。

2013年12月28日，[第十二届全国人大常委会通过了](../Page/第十二届全国人大常委会.md "wikilink")《关于废止有关劳动教养法律规定的决定》，劳动教养制度正式废止\[1\]；同时，对正在被执行劳动教养的人员，解除劳动教养\[2\]。劳动教养工作管理局、劳动教养管理所等机构则陆续摘牌。

## 历史

### 建立

在1950年代[中共中央发动的](../Page/中共中央.md "wikilink")[肃清暗藏反革命分子运动中逐步建立起来的](../Page/镇压反革命运动.md "wikilink")。1955年8月25日，[中共中央发布了](../Page/中共中央.md "wikilink")《关于彻底肃清暗藏的反革命分子的指示》，其中称，

1956年1月10日，[中共中央发布了](../Page/中共中央.md "wikilink")《关于各省市立即筹办劳动教养机构的指示》，强调“把肃反中被审查的，不够判刑的反革命分子、坏分子、而[政治上又不适合留用](../Page/政治.md "wikilink")，把这些人集中起来，送到一定地方，让他们替国家做工，自食其力。并对他们进行政治、思想的改造工作”。随后中共中央又在《转批中央十人小组关于反革命分子和其他坏分子的解释及处理意见的政策界限的暂行规定》文件中规定：“某些直系亲属在[土改](../Page/土改.md "wikilink")、[镇反和](../Page/镇反.md "wikilink")[社会主义改造中](../Page/社会主义改造.md "wikilink")，被杀、被关、被鬥者的[家属](../Page/家属.md "wikilink")……可送劳动教养。”

一般认为，从[法律上讲](../Page/法律.md "wikilink")，劳动教养制度始于1957年。1957年8月3日，[中华人民共和国国务院公布了经过](../Page/中华人民共和国国务院.md "wikilink")1957年8月1日[第一届全国人民代表大会常务委员会第七十八次会议批准的](../Page/第一届全国人民代表大会常务委员会.md "wikilink")《[关于劳动教养问题的决定](../Page/s:国务院关于劳动教养问题的决定.md "wikilink")》。该决定的初衷是为了管理“游手好闲、违反法纪、不务正业的有劳动力的人”，主要针对的对象是“不够逮捕判刑而政治上又不适合继续留用，放到社会上又会增加失业的”人员。当时人们认为这主要是针对划为右派的人员。该决定称：

当时《[中华人民共和国宪法](../Page/中华人民共和国宪法.md "wikilink")》第100条的内容是：“中华人民共和国公民必须遵守宪法和法律，遵守劳动纪律，遵守公共秩序，尊重社会公德。”

在随后一年左右，全国立即建起一百多处劳教场所，开始形成县办劳教、社办劳教、乃至生产队也办劳教。全国劳教人员很快就被收容到近百万。1961年，即[大跃进运动末期](../Page/大跃进运动.md "wikilink")，[中华人民共和国公安部承认](../Page/中华人民共和国公安部.md "wikilink")：“扩大了收容范围和收容对象，错收了一批不够劳动教养的人。在管理上和[劳改犯等同了起来](../Page/劳改.md "wikilink")。生活管理和劳动生产上搞了一些超体力劳动，造成了劳教人员非正常死亡的严重现象。”

直至1979年，中国被劳动教养的人员没有明确的期限，很多人最长劳教长达20多年。1979年11月29日[中华人民共和国国务院颁布](../Page/中华人民共和国国务院.md "wikilink")《[国务院关于劳动教养的补充规定](../Page/s:国务院关于劳动教养的补充规定.md "wikilink")》，明确劳动教养制度可限制和剥夺公民人身自由长达1－3年，必要时可延长一年。但以后在实践中，常出现重复劳教问题。

1980年2月29日，国务院发布[《关于将强制劳动和收容审查两项措施统一于劳动教养的通知》](http://www.law-lib.com/law/law_view.asp?id=44136)，称：一九六一年以来，各地公安机关对轻微违法犯罪的人和流窜作案嫌疑分子采取了**强制劳动**和**收容审查**的措施。从1980年下半年起，对有轻微违法犯罪行为、尚不够刑事处罚需要进行**强制劳动**的人，一律送劳动教养。对于有轻微违法犯罪行为又不讲真实姓名、住址、来历不明的人，或者有轻微违法犯罪行为又有流窜作案、多次作案、结伙作案嫌疑需收容查清罪行的人，送劳动教养场所专门编队进行审查。凡是放在社会上危害不大的，可按照《[刑事诉讼法](../Page/刑事诉讼法.md "wikilink")》规定采取[监视居住](../Page/监视居住.md "wikilink")、[取保候审等方式进行审查](../Page/取保候审.md "wikilink")。

1982年1月21日颁布《[劳动教养试行办法](../Page/s:劳动教养试行办法.md "wikilink")》，针对的对象包括“家居[农村而流窜到](../Page/农村.md "wikilink")[城市](../Page/城市.md "wikilink")、[铁路沿线和大型厂矿作案](../Page/铁路.md "wikilink")，符合劳动教养条件”
的人。1986年，[第六届全国人大常委会第](../Page/第六届全国人大常委会.md "wikilink")17次会议通过了《治安管理处罚条例》；1990年12月，[第七届全国人大常委会第](../Page/第七届全国人大常委会.md "wikilink")17次会议通过了《关于禁毒的决定》；1991年9月，第七届全国人大常委会第21次会议又通过了《关于严禁卖淫嫖娼的决定》等。以上法律使更多的人员相继被纳入劳动教养的对象范畴。同时，其他[行政法规](../Page/行政法规.md "wikilink")、[司法解释](../Page/司法解释.md "wikilink")，甚至一些省级行政区、[较大的市的](../Page/较大的市.md "wikilink")[地方性法规和国务院部门的](../Page/地方性法规.md "wikilink")[部门规章](../Page/部门规章.md "wikilink")，都加剧劳动教养对象扩大化的趋势。实践中主要是针对[小偷](../Page/小偷.md "wikilink")、[卖淫](../Page/卖淫.md "wikilink")[嫖娼](../Page/嫖娼.md "wikilink")、[吸毒](../Page/吸毒.md "wikilink")、破坏[治安等](../Page/治安.md "wikilink")。

劳动教养制度在司法实践中，由于其法理缺陷，也受到越来越多的冲击，在大多数案件中，法院会因为敏感性拒绝立案，但也有法院受理这类案件，并在不触及法理和法律层面概念下，也会有“迂回公平”的判决。\[3\]

### 废除

[北京理工大学教授](../Page/北京理工大学.md "wikilink")[胡星斗在](../Page/胡星斗.md "wikilink")2003年6月21日发出的《对劳动教养的有关规定进行违宪违法审查的建议书》和在2003年11月9日提出的《就废除劳动教养制度致中共中央、全国人大、国务院的建议书》均引起国内外强烈的反响，并引来媒体广泛的报道。这两份建议书被认为是新时期废除劳动教养制度的第一声。

2004年1月下旬，[广东省政协委员联署由](../Page/广东省.md "wikilink")[朱征夫发起的要求废除劳教的提案](../Page/朱征夫.md "wikilink")，要求广东省先行一步废除劳教制度，得到了[深圳市社会科学院院长](../Page/深圳市社会科学院.md "wikilink")[乐正](../Page/乐正.md "wikilink")[教授](../Page/教授.md "wikilink")、[中山大学历史系教授](../Page/中山大学.md "wikilink")[邱捷](../Page/邱捷.md "wikilink")、[广东外语外贸大学教授](../Page/广东外语外贸大学.md "wikilink")[王卫红](../Page/王卫红.md "wikilink")、[广东经济管理学院法律系教授](../Page/广东经济管理学院.md "wikilink")[蓝燕霞](../Page/蓝燕霞.md "wikilink")、[中新社广东分社社长](../Page/中新社.md "wikilink")[陈佳](../Page/陈佳.md "wikilink")、《[羊城晚报](../Page/羊城晚报.md "wikilink")》总编辑[潘伟文等六位政协委员的附议](../Page/潘伟文.md "wikilink")。

鉴于劳动教养制度本身的法理缺陷和广受非议，中国官方把《[违法行为矫治法](../Page/违法行为矫治法.md "wikilink")》列入2005年的立法规划，用以取代劳教制度。但因受到公安部门的抵制，此后多年其前景并不明朗。

2008年3月，全国人大代表、[陕西省人大常委会委员](../Page/陕西省人大常委会.md "wikilink")[马克宁正式提交建议](../Page/马克宁.md "wikilink")，呼吁废除劳动教养制度。马认为，国务院关于劳动教养的行政法规违反《宪法》、《立法法》的规定，也违反了《行政处罚法》和《治安管理处罚法》，应当废除。\[4\]

2010年全国[两会中提到](../Page/两会.md "wikilink")，《[中华人民共和国违法行为教育矫治法](../Page/中华人民共和国违法行为教育矫治法.md "wikilink")》还处在起草修改阶段，已经列入了2010年的立法计划。劳教制度改革也已经列入正在进行的司法改革进程中。\[5\]

2011年，[任建宇劳教案颇具争议](../Page/任建宇劳教案.md "wikilink")。2012年2月[两会后](../Page/两会.md "wikilink")，[薄熙来失势](../Page/薄熙来.md "wikilink")，[重庆政治环境才逐渐改观](../Page/重庆.md "wikilink")。2012年8月，任建宇代理律师[浦志强和任父发起行政诉讼](../Page/浦志强.md "wikilink")。2012年9月，任建宇劳教决定被撤销，终获自由。任建宇案再次引起全国范围内对于劳教制度的广泛关注。《[人民日报](../Page/人民日报.md "wikilink")》亦发表[社评](../Page/社评.md "wikilink")，声援任建宇，质疑劳教制度的合法性。

2012年，[唐慧劳教案发生](../Page/唐慧劳教案.md "wikilink")，全国舆论哗然。在新闻界和互联网舆论的压力下，湖南省劳动教养委员会受理了代理律师甘元春和胡益华提出的行政复议。行政复议获得了成功，唐慧获救。除了一致声讨[永州市公安局](../Page/永州市公安局.md "wikilink")，中国社会要求废除劳教制度的声音也日益高涨。

2013年1月，广东省宣布已做好适时停止劳教的准备。同年2月，[云南省宣布暂停省内全部劳教审批](../Page/云南省.md "wikilink")。同年3月，[湖南省公安厅副厅长](../Page/湖南省公安厅.md "wikilink")[胡旭曦表示](../Page/胡旭曦.md "wikilink")，[湖南省已停止劳动教养审批](../Page/湖南省.md "wikilink")。\[6\]与此同时，不少地方的劳教所也于2013年开始接收强制隔离[戒毒人员](../Page/戒毒.md "wikilink")，劳教所主要职能逐步转向强制戒毒\[7\]\[8\]\[9\]。

2013年1月7日上午，[中共中央政法委员会书记](../Page/中共中央政法委员会.md "wikilink")[孟建柱在全国政法工作会议上宣布](../Page/孟建柱.md "wikilink")，中央已研究，报请[全国人民代表大会常务委员会批准后](../Page/全国人民代表大会常务委员会.md "wikilink")，停止使用劳教制度。\[10\]\[11\]\[12\]\[13\]

3月17日，[十二届全国人大一次会议举行闭幕会](../Page/十二届全国人大一次会议.md "wikilink")。大会闭幕后，[国务院](../Page/国务院.md "wikilink")[总理](../Page/总理.md "wikilink")[李克强在](../Page/李克强.md "wikilink")[人民大会堂三楼中央大厅与中外记者见面并回答记者的提问](../Page/人民大会堂.md "wikilink")。在回答李克强《[中国日报](../Page/中国日报.md "wikilink")》记者的提问时说，有关劳动教养制度的改革方案，有关部门正在抓紧研究制定，年内有望出台。\[14\]

11月15日，在[中共十八届三中全会发布的](../Page/中共十八届三中全会.md "wikilink")《[中共中央关于全面深化改革若干重大问题的决定](../Page/中共中央关于全面深化改革若干重大问题的决定.md "wikilink")》中，第九节34条提出“……废止劳动教养制度，完善对违法犯罪行为的惩治和矫正法律，健全社区矫正制度……”等内容\[15\]\[16\]。

2013年12月28日，[第十二届全国人大常委会通过了关于废止有关劳动教养法律规定的决定](../Page/第十二届全国人大常委会.md "wikilink")，劳动教养制度正式废止。同时，对正在被执行劳动教养的人员，解除劳动教养\[17\]。劳动教养工作管理局、劳动教养管理所等机构则陆续摘牌。

## 机构

在实践中，“大中城市”一般指[地级行政区](../Page/地级行政区.md "wikilink")。几乎所有[地级行政区人民政府皆设立了劳动教养管理委员会](../Page/地级行政区.md "wikilink")。

劳动教养管理委员会之下，分别在司法行政系统和公安系统设立劳动教养相关机构。司法行政系统的劳动教养工作管理局或司法局内设处（室、科）担负劳动教养管理委员会办公室的职能，也负责劳动教养工作的具体管理，包括劳动教养管理所的日常管理。公安系统的公安局劳动教养审批委员会担负劳动教养审批工作。

### 司法行政系统

在[司法行政系统](../Page/司法行政机关.md "wikilink")，[中华人民共和国司法部内设](../Page/中华人民共和国司法部.md "wikilink")[司法部劳动教养工作管理局](../Page/司法部劳动教养工作管理局.md "wikilink")。各[省](../Page/省.md "wikilink")、[直辖市](../Page/直辖市.md "wikilink")、[自治区及](../Page/自治区.md "wikilink")[新疆生产建设兵团的司法厅](../Page/新疆生产建设兵团.md "wikilink")、局均设立了下属的劳动教养工作管理局。[地区](../Page/地区_\(中国行政区划\).md "wikilink")、[地级市](../Page/地级市.md "wikilink")、[自治州等的司法局一般由内设的劳动教养工作管理处](../Page/自治州.md "wikilink")（室、科）等负责劳动教养工作。

**劳动教养工作管理局**是各[省](../Page/省.md "wikilink")、[直辖市](../Page/直辖市.md "wikilink")、[自治区及](../Page/自治区.md "wikilink")[新疆生产建设兵团普遍设立的管理劳动教养工作的政府机构](../Page/新疆生产建设兵团.md "wikilink")，也是劳动教养管理委员会的办事机构，一般为司法厅（直辖市及新疆生产建设兵团为司法局）的下属局，级别一般为[副地厅级](../Page/副地厅级.md "wikilink")。个别[省会城市如](../Page/省会城市.md "wikilink")[广东省省会](../Page/广东省.md "wikilink")[广州市](../Page/广州市.md "wikilink")、[湖北省省会](../Page/湖北省.md "wikilink")[武汉市等也设立了劳动教养工作管理局](../Page/武汉市.md "wikilink")，为市司法局的下属局。

随着2007年《[中华人民共和国禁毒法](../Page/中华人民共和国禁毒法.md "wikilink")》的实施，原属司法行政机关管理的[劳动教养戒毒及原属](../Page/劳动教养戒毒.md "wikilink")[公安机关管理的](../Page/公安机关.md "wikilink")[强制戒毒均被废除](../Page/强制戒毒.md "wikilink")，代之以[强制隔离戒毒](../Page/强制隔离戒毒.md "wikilink")。此后各地劳动教养工作管理局纷纷加挂**戒毒管理局**的牌子。

**劳动教养管理所**是劳动教养工作的基层管理机构，隶属于各级劳动教养工作管理局，负责劳动教养人员的日常监管。

### 公安系统

在公安系统，根据2002年《公安机关办理劳动教养案件规定》第二条的规定，在[省](../Page/省.md "wikilink")、[自治区](../Page/自治区.md "wikilink")、[直辖市公安厅](../Page/直辖市.md "wikilink")（局）、[新疆生产建设兵团公安局和](../Page/新疆生产建设兵团公安局.md "wikilink")[地区](../Page/地区_\(中国行政区划\).md "wikilink")、[地级市](../Page/地级市.md "wikilink")、[自治州](../Page/自治州.md "wikilink")、[盟公安局](../Page/盟.md "wikilink")（处）设立了**劳动教养审批委员会**，为同级劳动教养管理委员会的审批机构。《公安机关办理劳动教养案件规定》第二条称：

## 批评

劳动教养制度受到了来自社会各界的越来越多的批评。这一情况到[收容遣送制度被废除之后变得更为突出](../Page/收容遣送.md "wikilink")。

很多学者认为，劳动教养制度在中国特定的历史下有一定的积极作用，但后期已不再适用了。有学者认为劳动教养存在着“没有法律的授权和规范”、“劳动教养对象不明确”、“处罚过于严厉”、“程序不正当”、“规范不统一和司法解释多元化”等等弊端，而这些弊端就导致了有关部门滥用权力、非法剥夺公民人身自由等行为。\[18\]

有学者认为，劳动教养制度违反了《[宪法](../Page/中华人民共和国宪法.md "wikilink")》、《[立法法](../Page/s:中华人民共和国立法法.md "wikilink")》、《[行政处罚法](../Page/s:中华人民共和国行政处罚法.md "wikilink")》，并与中国政府签署的[国际人权公约相背](../Page/国际人权公约.md "wikilink")。《宪法》第三十七条规定：

《立法法》第8条和第9条规定，对公民限制人身自由的强制措施和处罚，只能通过制定法律来规定，并且全国人大及其常委会不得授权国务院就这类限制公民人身自由的强制措施和处罚在没有正式法律的情况下先行制定行政法规。

《行政处罚法》第九条规定：“限制人身自由的行政处罚，只能由法律设定。”第十条规定：“行政法规可以设定除限制人身自由以外的行政处罚。”，同时规定的处罚种类中不包括劳动教养，最严厉的行政处罚是行政拘留，拘留期限不得超过15天。

## 注释

## 参考文献

## 外部链接

  - [《决定》废止劳动教养制度](http://news.163.com/special/laojiao/)

## 参见

  - [劳动改造](../Page/劳动改造.md "wikilink")
  - [收容遣送](../Page/收容遣送.md "wikilink")
  - [行政法规](../Page/行政法规.md "wikilink")

<!-- end list -->

  - 中共中央文件

<!-- end list -->

1.  [中共中央关于彻底肃清暗藏的反革命分子的指示](../Page/s:中共中央关于彻底肃清暗藏的反革命分子的指示.md "wikilink")（1955年8月25日发布）
2.  [中共中央关于各省市应立即筹办劳动教养机构的指示](../Page/s:中共中央关于各省市应立即筹办劳动教养机构的指示.md "wikilink")（1956年1月10日发布）
3.  [中央十人小组关于反革命分子和其他坏分子的解释及处理的政策界限的暂行规定](../Page/s:中央十人小组关于反革命分子和其他坏分子的解释及处理的政策界限的暂行规定.md "wikilink")（1956年3月10日中共中央转批）

<!-- end list -->

  - 法律法规

<!-- end list -->

1.  [国务院关于劳动教养问题的决定](../Page/s:国务院关于劳动教养问题的决定.md "wikilink")（1957年8月3日公布施行）
2.  [中华人民共和国治安管理处罚条例](../Page/s:中华人民共和国治安管理处罚条例_\(1957年\).md "wikilink")（1957年10月22日公布施行）
3.  [国务院关于劳动教养的补充规定](../Page/s:国务院关于劳动教养的补充规定.md "wikilink")（1979年11月29日公布施行）
4.  [国务院关于将强制劳动和收容审查两项措施统一于劳动教养的通知](../Page/s:国务院关于将强制劳动和收容审查两项措施统一于劳动教养的通知.md "wikilink")（1980年2月29日公布施行）
5.  [全国人民代表大会常务委员会关于处理逃跑或者重新犯罪的劳改犯和劳教人员的决定](../Page/s:全国人民代表大会常务委员会关于处理逃跑或者重新犯罪的劳改犯和劳教人员的决定.md "wikilink")（1981年7月10日公布施行）
6.  [劳动教养试行办法](../Page/s:劳动教养试行办法.md "wikilink")（1982年1月21日公布施行）
7.  [公安部关于修改《劳动教养试行办法》第十九条的通知](../Page/s:公安部关于修改《劳动教养试行办法》第十九条的通知.md "wikilink")（1983年4月30日发布）
8.  [公安部、司法部关于劳动教养和注销劳教人员城市户口问题的通知](../Page/s:公安部、司法部关于劳动教养和注销劳教人员城市户口问题的通知.md "wikilink")（1984年3月26日发布）
9.  [中华人民共和国治安管理处罚条例](../Page/s:中华人民共和国治安管理处罚条例_\(1986年\).md "wikilink")（1986年9月5日公布，1987年1月1日起施行）
10. [公安部关于严格依法办事，执行政策，深入开展除“六害”斗争的通知](../Page/s:关于严格依法办事，执行政策，深入开展除“六害”斗争的通知.md "wikilink")（1990年5月7日发布）
11. [全国人民代表大会常务委员会关于禁毒的决定](../Page/s:全国人民代表大会常务委员会关于禁毒的决定.md "wikilink")（1990年12月28日公布施行）
12. [全国人民代表大会常务委员会关于严禁卖淫嫖娼的决定](../Page/s:全国人民代表大会常务委员会关于严禁卖淫嫖娼的决定.md "wikilink")（1991年9月4日公布施行）
13. [劳动教养管理工作若干制度](../Page/s:劳动教养管理工作若干制度.md "wikilink")（1992年8月10日发布）
14. [劳动教养管理工作执法细则](../Page/s:劳动教养管理工作执法细则.md "wikilink")（1992年8月10日发布）
15. [劳动教养人员守则](../Page/s:劳动教养人员守则.md "wikilink")（1992年8月10日发布）
16. [劳动教养教育工作规定](../Page/s:劳动教养教育工作规定.md "wikilink")（1993年8月9日发布）
17. [中华人民共和国治安管理处罚条例](../Page/s:中华人民共和国治安管理处罚条例_\(1994年\).md "wikilink")（1994年5月12日公布施行）
18. [创建现代化文明劳教所试行办法](../Page/s:创建现代化文明劳教所试行办法.md "wikilink")（1996年5月1日发布）
19. [公安机关办理劳动教养案件规定](../Page/s:公安机关办理劳动教养案件规定.md "wikilink")（2002年4月12日公布，2002年6月1日起施行）
20. [劳动教养戒毒工作规定](../Page/s:劳动教养戒毒工作规定.md "wikilink")（2003年5月20日公布，2003年8月1日起施行）
21. [关于进一步做好刑满释放、解除劳教人员促进就业和社会保障工作的意见](../Page/s:关于进一步做好刑满释放、解除劳教人员促进就业和社会保障工作的意见.md "wikilink")（2004年2月6日公布施行）
22. [司法部关于进一步深化劳教办特色推进管理工作改革的意见](../Page/s:司法部关于进一步深化劳教办特色推进管理工作改革的意见.md "wikilink")（2004年12月13日发布）

以下法律法规去除了此前相关法律法规有关劳动教养的规定：

1.  [中华人民共和国治安管理处罚法](../Page/s:中华人民共和国治安管理处罚法.md "wikilink")（2005年8月28日公布，2006年3月1日起施行）
2.  [中华人民共和国禁毒法](../Page/s:中华人民共和国禁毒法.md "wikilink")（2007年12月29日公布，2008年6月1日起施行）

[中华人民共和国劳动教养制度](../Category/中华人民共和国劳动教养制度.md "wikilink")
[L](../Category/中华人民共和国行政处罚法.md "wikilink")
[Category:中华人民共和国人权史](../Category/中华人民共和国人权史.md "wikilink")
[Category:中华人民共和国羁押场所](../Category/中华人民共和国羁押场所.md "wikilink")
[Category:1957年建立](../Category/1957年建立.md "wikilink")
[Category:2013年废除](../Category/2013年废除.md "wikilink")

1.  [中国人大常委会通过废止劳教决定](http://www.apdnews.com/news/57819.html)
    ，亚太日报，2013年12月30日

2.

3.

4.

5.

6.

7.

8.
9.  [1](http://www.moj.gov.cn/ldjyglj/node_222.htm)

10.

11.

12.

13.

14.

15.

16.

17.

18.