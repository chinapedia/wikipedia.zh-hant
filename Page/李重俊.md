**李重俊**（），[中國](../Page/中國.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")[唐中宗第三子](../Page/唐中宗.md "wikilink")，母不详。遭到[韋皇后](../Page/韋皇后.md "wikilink")、[安樂公主](../Page/安樂公主.md "wikilink")、[武三思等迫害](../Page/武三思.md "wikilink")，707年以[羽林軍發動](../Page/羽林軍.md "wikilink")[重俊之變](../Page/重俊之變.md "wikilink")，殺了武三思、[武崇訓父子](../Page/武崇訓.md "wikilink")，後因士卒倒戈，被[斬殺](../Page/斬殺.md "wikilink")。[唐睿宗以](../Page/唐睿宗.md "wikilink")[唐隆之變](../Page/唐隆之變.md "wikilink")[重祚後](../Page/重祚.md "wikilink")，[追贈重俊為](../Page/追贈.md "wikilink")**節愍太子**。

## 生平

[聖曆元年](../Page/聖曆.md "wikilink")（698年），封為[義興](../Page/義興郡.md "wikilink")[郡王](../Page/郡王.md "wikilink")。[長安年間](../Page/长安_\(年号\).md "wikilink")（701年－705年），任[衛尉員外少卿之職](../Page/衛尉.md "wikilink")。[景龍初年](../Page/景龙.md "wikilink")（707年－710年），進封衛王，拜[洛州牧](../Page/洛州.md "wikilink")，實封千戶，不久又升為左衛[大將軍](../Page/大將軍.md "wikilink")，兼遙授[揚州](../Page/揚州.md "wikilink")[大都督](../Page/都督.md "wikilink")。[神龍二年](../Page/神龍.md "wikilink")（706年）秋，立為[皇太子](../Page/皇太子.md "wikilink")。

重俊頗受後母[韋皇后](../Page/韋皇后_\(唐中宗\).md "wikilink")、[安樂公主](../Page/安樂公主.md "wikilink")、[武三思](../Page/武三思.md "wikilink")、[武崇訓迫害](../Page/武崇訓.md "wikilink")。重俊與[兵部尚書](../Page/兵部尚書.md "wikilink")[魏元忠暗中勾結](../Page/魏元忠.md "wikilink")，準備兵變，[景龍元年](../Page/景龍.md "wikilink")（707年）七月，與成王[李千里](../Page/李千里.md "wikilink")、左羽林大將軍[李多祚](../Page/李多祚.md "wikilink")、右羽林將軍[李思沖](../Page/李思沖.md "wikilink")、[李承況](../Page/李承況.md "wikilink")、[獨孤禕之](../Page/獨孤禕之.md "wikilink")、[沙吒忠义發動兵變](../Page/沙吒忠义.md "wikilink")，殺[武三思](../Page/武三思.md "wikilink")、[武崇訓父子](../Page/武崇訓.md "wikilink")，是為[重俊之變](../Page/重俊之變.md "wikilink")。中宗出面向重俊的部下威脅利誘，士兵半途倒戈，兵變瓦解，重俊逃跑時被自己的親兵所殺。中宗竟以重俊[首級祭祀](../Page/首級.md "wikilink")[太廟](../Page/太廟.md "wikilink")，也祭祀武三思、武崇訓。

[永和縣](../Page/永和縣.md "wikilink")[縣丞](../Page/縣丞.md "wikilink")[甯嘉勗哭悼李重俊](../Page/甯嘉勗.md "wikilink")，被[宗楚客貶死](../Page/宗楚客.md "wikilink")。[魏元忠因為與重俊通謀](../Page/魏元忠.md "wikilink")，亦遭楚客貶謫，卒於[涪陵](../Page/涪陵.md "wikilink")。

## 家庭

[侍女俑_1_SPIA.jpg](https://zh.wikipedia.org/wiki/File:侍女俑_1_SPIA.jpg "fig:侍女俑_1_SPIA.jpg")

### 母

生母不详，当是唐中宗某位不知名的妾室（后宫），可能早亡。

### 妃

  - [杨氏](../Page/杨妃_\(李重俊\).md "wikilink")，[其妹为](../Page/元獻皇后.md "wikilink")[唐肃宗生母](../Page/唐肅宗.md "wikilink")\[1\]。有《节愍太子妃墓志铭》

### 子

  - [李宗晖](../Page/李宗晖.md "wikilink")，睿宗时封湖阳郡王，玄宗天宝中，至太常员外卿。据《节愍太子妃墓志铭》，非杨氏所出

## 注释

<references/>

[3](../Category/唐中宗皇子.md "wikilink")
[Category:唐朝追赠太子](../Category/唐朝追赠太子.md "wikilink")
[卫](../Category/武周异姓国王.md "wikilink")
[Category:武周大将军](../Category/武周大将军.md "wikilink")
[领](../Category/武周扬州大都督.md "wikilink")
[C重俊](../Category/李姓.md "wikilink")
[Category:唐朝遇刺身亡者](../Category/唐朝遇刺身亡者.md "wikilink")
[Category:諡節愍](../Category/諡節愍.md "wikilink")

1.  《[新唐书](../Page/新唐书.md "wikilink") 卷八十九
    列传第一》玄宗元献皇后杨氏......帝即位，为贵嫔。其姊，节愍太子妃也。