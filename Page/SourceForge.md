[Sourceforge_logo.png](https://zh.wikipedia.org/wiki/File:Sourceforge_logo.png "fig:Sourceforge_logo.png")
[Sourceforge_Website_2017.png](https://zh.wikipedia.org/wiki/File:Sourceforge_Website_2017.png "fig:Sourceforge_Website_2017.png")
**SourceForge**是一套合作式[软件开发管理系统](../Page/版本控制.md "wikilink")。SourceForge本身是[VA
Software出售的](../Page/VA_Software.md "wikilink")[专有软件](../Page/专有软件.md "wikilink")。它集成了很多[开源应用程序](../Page/开源.md "wikilink")（例如[PostgreSQL和](../Page/PostgreSQL.md "wikilink")[SVN](../Page/Subversion.md "wikilink")、[CVS](../Page/协作版本系统.md "wikilink")），为软件开发提供了整套[生命周期服务](../Page/生命周期.md "wikilink")。

**SourceForge.net**，又稱**SF.net**，是[开源软件的开发者进行开发管理的集中式场所](../Page/开源软件.md "wikilink")，也是全球最大[开源软件开发平台和仓库](../Page/开源软件.md "wikilink")。SourceForge.net由[VA
Software提供主机](../Page/VA_Software.md "wikilink")，并运行SourceForge软件。大量开源项目在此落户（2005年6月已经達到125,090個專案及1,352,225位註冊用戶），包括[维基百科使用的](../Page/维基百科.md "wikilink")[MediaWiki](../Page/MediaWiki.md "wikilink")，但也包含很多停止開發和一人開發的项目。

## SourceForge軟體授權

SourceForge网站程序的源代码曾经也是开放的，但是后来VA
Software决定不再继续发布开源版本。它可能是从2001年起变为专有软件继续开发的。在封闭前最后的正式的开源版本是2.5，而最后的CVS版本是2.61。目前存在一系列利用SourceForge软件運行的协作式网站。

2003年VA Software首次发布了**SourceForge Enterprise Edition
4.x**，将其做为Java-J2EE程序完全重写。由采用SourceForge
4.x的组织报告说其性能和伸缩性相对SourceForge
3.x或者更早的2.x明显改善。SourceForge
4.x为将来的集成和扩展而支持[SOAP](../Page/SOAP.md "wikilink")、[XML](../Page/XML.md "wikilink")、[Web
Services等](../Page/Web_Services.md "wikilink")[API](../Page/API.md "wikilink")。

## SourceForge的競爭者

从SourceForge开源版本基础上还出现了开源替代物**[Gforge.org](../Page/Gforge.org.md "wikilink")**等。它是SourceForge的一个[程序员在SourceForge最后的CVS版本基础上的重写](../Page/程序员.md "wikilink")，增加了很多新特性并继续保持自由。

[自由软件基金会建立了](../Page/自由软件基金会.md "wikilink")[Savannah网站做为对SourceForge成為专有软件的回应](../Page/GNU_Savannah.md "wikilink")，Savannah是基于2.0版SourceForge软件。

[Google公司推出的](../Page/Google公司.md "wikilink")[Google
Code服务一度由于Google和众多新兴开源项目的大力支持成为SourceForge](../Page/Google_Code.md "wikilink").net在全球范围内最重要的竞争对手。至2009年3月，已有超过50,000个开源项目落户Google
Code。然而Google于2015年3月12日开始停止对Google Code提供技术支持，并于2016年1月25日关闭Google
Code。

SourceForge的其他競爭者還包括了[Bounty
Source](../Page/Bounty_Source.md "wikilink")、[Tigris.org](../Page/Tigris.org.md "wikilink")（由[CollabNet所建立](../Page/CollabNet.md "wikilink")）、[JavaForge](../Page/JavaForge.md "wikilink")（由[JavaLobby採](../Page/JavaLobby.md "wikilink")[CodeBeamer建立](../Page/CodeBeamer.md "wikilink")）、[berlios.de及](../Page/berlios.de.md "wikilink")[GitHub](../Page/GitHub.md "wikilink")。

## 中國大陸封鎖

  - 在2002年SourceForge.net網站曾經被中國大陸封鎖過，其后于2003年解封，但是直到2005年初vhost.sourceforge.net網址仍然處於被中國封鎖的狀態。

  -
  - 2008年6月因[Notepad++在主页上發起](../Page/Notepad++.md "wikilink")「抵制奥运」為抗議[中國政府在](../Page/中國政府.md "wikilink")[西藏对藏独暴乱分子的鎮壓行動](../Page/西藏.md "wikilink")，

  - 2012年8月初再次被[中國电信封锁](../Page/中國电信.md "wikilink")；延至8月5日左右，[中国联通亦进行了封锁](../Page/中国联通.md "wikilink")，封锁方式是URL明文关键词阻断。其后它们均于2012年8月10日前后解除了封锁。有消息称\[1\]Sourceforge被封后旋即也报复性封锁中国大陆用户的IP地址，但其后被证实为谣言。此后，SourceForge在中国大陆的访问恢复正常。

## 参见

  - [自由软件主机服务比较](../Page/自由软件主机服务比较.md "wikilink")

  - [SourceForge计划](../Category/SourceForge專案.md "wikilink")

  - [GNU Savannah](../Page/GNU_Savannah.md "wikilink")

  -
## 参考资料

## 外部链接

  - [SourceForge.net homepage](http://sourceforge.net)
  - [MediaWiki SourceForge.net Project
    Page](http://sourceforge.net/projects/wikipedia/)
  - [GForge
    Project](https://web.archive.org/web/20050717091411/http://gforge.org/)
  - [Novell Forge](http://forge.novell.com/)
  - [Bounty Source](https://www.bountysource.com/)
  - [XOOPS Module Development Forge](http://dev.xoops.org/)

[Category:网站](../Category/网站.md "wikilink")
[Category:开源软件托管网站](../Category/开源软件托管网站.md "wikilink")
[Category:版本控制系统](../Category/版本控制系统.md "wikilink")
[Category:项目管理软件](../Category/项目管理软件.md "wikilink")

1.  [SourceForge互相屏蔽值得反思](http://9.douban.com/site/entry/267414757/view)