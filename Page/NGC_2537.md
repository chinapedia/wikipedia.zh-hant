**NGC
2537**是位於[天貓座的一個](../Page/天貓座.md "wikilink")[棒旋星系](../Page/棒旋星系.md "wikilink")，它也被稱為**熊掌星系**。

## 參考資料

<div class="references-small">

<references/>

[wikisky.org](http://server8.wikisky.org/starview?object_type=2&object_id=3425&object_name=NGC+2537&locale=IT)

[seds.org](https://web.archive.org/web/20041225225312/http://www.seds.org/~spider/ngc/ngc_fr.cgi?2537)

</div>

[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[Category:棒旋星系](../Category/棒旋星系.md "wikilink")
[2537](../Category/天貓座NGC天體.md "wikilink")
[04274](../Category/UGC天體.md "wikilink")
[23040](../Category/PGC天體.md "wikilink")
[006](../Category/阿普天體.md "wikilink")