[Hippokrates.jpg](https://zh.wikipedia.org/wiki/File:Hippokrates.jpg "fig:Hippokrates.jpg")
**希波克拉底誓詞（，）**，俗稱**醫師誓詞**，是西方[医生傳統上行醫前的誓言](../Page/医生.md "wikilink")，[希波克拉底乃古](../Page/希波克拉底.md "wikilink")[希臘醫者](../Page/希臘.md "wikilink")，被譽為西方「醫學之父」。在希波克拉底所立的這份誓詞中，列出了一些特定的[倫理上的規範](../Page/倫理.md "wikilink")。但希波克拉底有可能并不是这份[誓词的原作者](../Page/誓詞.md "wikilink")\[1\]。

## 誓詞簡介

現代醫學界，雖不再採用原始的希波克拉底誓詞原文，它也没有任何法律效力；但是希波克拉底誓詞对现代誓約依然拥有影响。

其中的许多因素对于今天的[医学伦理依然是有效的](../Page/醫學倫理學.md "wikilink")，例如：

  - 不损害病人；
  - 禁止与病人发生[婚前性行为](../Page/婚前性行為.md "wikilink")；

许多原文中的内容也不再适合于今天的情况，例如：

  - 当时「[外科医生](../Page/外科.md "wikilink")」与「医生」的职业还分割着，因此禁止用手术治疗[结石](../Page/结石.md "wikilink")
  - 禁止[堕胎和](../Page/堕胎.md "wikilink")[安乐死](../Page/安乐死.md "wikilink")

一般今天把这样的内容修改为适合今天情况的描述方法——禁止使用医生本人不了解的医术。

## 誓詞內容

### 古典希臘語原文\[2\]

### 現代希臘語譯文\[3\]

### 中文譯文\[4\]

敬禀[醫神](../Page/醫神.md "wikilink")[阿波羅](../Page/阿波羅.md "wikilink")、[阿斯克勒庇俄斯](../Page/阿斯克勒庇俄斯.md "wikilink")、[許癸厄亞](../Page/許癸厄亞.md "wikilink")、[帕那刻亞](../Page/帕那刻亚.md "wikilink")，及天地諸神聖鑒之，鄙人敬謹宣誓：

余願盡己之能力與判斷力之所及，矢守此約。凡授余藝者：余敬如父母，為終身同甘共苦之侶；倘有急需，余必接濟。視彼兒女，猶余手足，如欲受業，余無償、無條件傳授之。凡余之所知，無論口授、書傳俱傳之吾子、吾師之子、及立誓守此約之生徒，此外不傳他人。

余願盡己之能力與判斷力之所及，恪守為病家謀福之信條，並避免一切墮落害人之敗行，余必不以毒物藥品與他人，並不作此項之指導，雖人請求，亦必不與之，尤不為婦人施[墮胎之術](../Page/墮胎.md "wikilink")。余願以此純潔神聖之心，終身執行余之職務。至於[手術](../Page/手術.md "wikilink")，另待高明，余不施之，遇[結石患者亦然](../Page/結石.md "wikilink")，惟使專匠為之。

無論何適何遇，逢男或女，民人奴隸，余之唯一目的，為病家謀福，並檢點吾身，不為種種墮落害人之敗行，尤不為[誘姦之事](../Page/誘姦.md "wikilink")。凡余所見所聞，不論有無業務之牽連，余以為不應洩漏者，願守口如瓶。

倘余嚴守上述之誓詞，願神僅僅使余之生命及醫術，得無上光榮；苟違此誓，天地鬼神共殛之！

### 英语譯文

## 来源和历史

希波克拉底本人从来没有提到过这个誓词，希波克拉底生活在前460年至前370年，这之间没有任何文献提到过这份誓词，也没有任何與他同时代的类似文件被发现，因此该誓词的来源不明。

最早提到这份誓词的是，公元1世纪罗马皇帝[克勞狄一世身边的一名罗马医生](../Page/克勞狄一世.md "wikilink")。

有一种说法是这个誓词是[毕达哥拉斯学派的人创立的](../Page/毕达哥拉斯.md "wikilink")。但这个理论，現基本上没有人支持了，因为没有[毕达哥拉斯学派从医的纪录和论证](../Page/毕达哥拉斯学派.md "wikilink")。

誓词的内容不很明确，而且它的文字在不同的时间里也不断被改变来适合当时的需要。在[近代](../Page/近代.md "wikilink")，早期大学医科授予博士学位以及其它一些医学职业如[助產士或者](../Page/助產士.md "wikilink")[藥劑師等毕业时使用希波克拉底誓詞或者其中的部分内容](../Page/藥劑師.md "wikilink")。从[文艺复兴到](../Page/文艺复兴.md "wikilink")20世纪它被看作是古代医学伦理的经典文献。

1804年，[蒙彼利埃医学院首次使用希波克拉底誓詞全文作为毕业生的誓词](../Page/蒙彼利埃.md "wikilink")。20世纪-{zh-hans:里;
zh-hant:裡;}-许多高校，尤其是[美国的高校在授予](../Page/美国.md "wikilink")[博士学位的仪式上使用希波克拉底誓詞](../Page/博士.md "wikilink")。

今日，許多[醫學院试图使用其它比较适合当今情况的文字](../Page/醫學院.md "wikilink")，来取代希波克拉底誓詞。

舉例如（[日内瓦宣言](../Page/日内瓦宣言.md "wikilink")）：

## 经济意义

希波克拉底誓詞有一定的经济意义，它包含有一点古代的[社会保险的成分](../Page/社会保险.md "wikilink")。

誓词包含养育和教育老师的后代的内容，这样一来在老师无法养育自己的后代时他们有一定的经济保障。

同时，誓词包含同行之间互相无偿治疗，因此有[医疗保险的意义](../Page/醫療保險.md "wikilink")。

## 相關資料

  - [世界衛生組織](../Page/世界衛生組織.md "wikilink")（WHO）的[赫爾辛基宣言](../Page/赫爾辛基宣言.md "wikilink")
  - [世界醫學會](../Page/世界醫學協會.md "wikilink")（WMA）的[日內瓦宣言](../Page/日內瓦宣言.md "wikilink")
  - [纽伦堡守则](../Page/纽伦堡守则.md "wikilink")
  - [中国大陆医学生誓言](../Page/中国大陆医学生誓言.md "wikilink")
  - [病人安全](../Page/病人安全.md "wikilink")

## 延伸閱讀

  -
  - [The Hippocratic Oath Today: Meaningless Relic or Invaluable Moral
    Guide?](http://www.pbs.org/wgbh/nova/doctors/oath.html) – a PBS NOVA
    online discussion with responses from doctors as well as 2 versions
    of the oath. pbs.org

  - Lewis Richard Farnell, *Greek Hero Cults and Ideas of Immortality*,
    1921.

  - ["Codes of Ethics: Some History" by Robert Baker, Union College in
    Perspectives on the Professions, Vol. 19, No. 1,
    Fall 1999](http://ethics.iit.edu/perspective/v19n1%20perspective.pdf),
    ethics.iit.edu

## 外部連結

  - [Hippocratic Oath](http://www.h2g2.com/approved_entry/A1103798),
    [The Hitchhiker's Guide to the
    Galaxy](../Page/银河系漫游指南系列.md "wikilink")
    ([h2g2](../Page/h2g2.md "wikilink")).
  - [Hippocratic
    Oath](http://www.pbs.org/wgbh/nova/doctors/oath_classical.html) –
    Classical version, pbs.org
  - [Hippocratic
    Oath](http://www.pbs.org/wgbh/nova/doctors/oath_modern.html) –
    Modern version, pbs.org
  - [Hippocratis
    jusiurandum](http://web2.bium.univ-paris5.fr/livanc/?cote=00002&p=13&do=page)
    – Image of a 1595 copy of the Hippocratic oath with side-by-side
    original Greek and Latin translation, bium.univ-paris5.fr
  - [Hippocrates | The
    Oath](http://www.nlm.nih.gov/hmd/greek/greek_oath.html) – [National
    Institutes of
    Health](../Page/National_Institutes_of_Health.md "wikilink") page
    about the Hippocratic oath, nlm.nih.gov
  - [Tishchenko P. D. Resurrection of the Hippocratic Oath in
    Russia](http://www.zpu-journal.ru/en/articles/detail.php?ID=330),
    zpu-journal.ru

[Category:医学伦理学](../Category/医学伦理学.md "wikilink")
[Category:偽書](../Category/偽書.md "wikilink")
[公](../Category/誓词.md "wikilink")

1.  [Hippocrates](http://encarta.msn.com/encyclopedia_761576397/Hippocrates.html),
    Microsoft Encarta Online Encyclopedia 2006. Microsoft Corporation.
    [Archived](https://www.webcitation.org/query?id=1257007841924009)
    2009-10-31.
2.
3.
4.  [醫師誓詞，注重醫者道德內涵](https://www.epochtimes.com.tw/n51754/%E9%86%AB%E5%B8%AB%E8%AA%93%E8%A9%9E-%E6%B3%A8%E9%87%8D%E9%86%AB%E8%80%85%E9%81%93%E5%BE%B7%E5%85%A7%E6%B6%B5.html)