[Moke02.png](https://zh.wikipedia.org/wiki/File:Moke02.png "fig:Moke02.png")
**Mini Moke** 是一輛由[英國汽車公司在](../Page/英國.md "wikilink")1964 至1968
年，在[伯明罕的Longbridge](../Page/伯明罕.md "wikilink") 車厰，依據 Alec
Issigonis
[爵士設計的](../Page/爵士.md "wikilink")[柯士甸](../Page/柯士甸.md "wikilink")
迷你 (Mini)
[汽車](../Page/汽車.md "wikilink")，改裝而成的開篷汽車。在[英國生產了](../Page/英國.md "wikilink")15,000
輛。之後，分別在[澳洲](../Page/澳洲.md "wikilink")，[葡萄牙](../Page/葡萄牙.md "wikilink")
及[意大利生產](../Page/意大利.md "wikilink")。

Mini Moke
的誕生，來自一個現實需要。就是英國軍方建議設計一輛，類似美國軍方[吉普車的輕型軍車以應付需要](../Page/吉普車.md "wikilink")。
但後來發覺 Mini Moke 的輪軚胎太細和車身離地太低，以致不適合作為越野車之用。 它後來被推出成為一輛低成本，低維修的民用車輛。
它在英國生產至 1968年，1966 至 1981 年在澳洲生產。它曾經在美國短暫地被推出市場，但銷售量不佳。

英國利蘭車厰在葡萄牙的子公司 Automoveis 曾經生產過其中一個版本，直至1992 年為止。 意大利的 Cagiva 車厰 亦生產了
1,500 輛，把總生產數量推至 50,000輛。

開始出售的初期，坐位和車篷是額外配件，不會和車身一起付運。 車主在收貨後，需要親自將它們裝上。

## 操作者

  - [澳門治安警察局曾經把它作為他們正式的代步交通工具](../Page/澳門治安警察局.md "wikilink")
  - 澳門[文華東方酒店擁有](../Page/文華東方酒店.md "wikilink")43輛
  - 澳門租車公司Avis曾經有一隊叫CUB的汽車供公眾租賃。這些CUB汽車外型跟Moke很相似，但使用的是一枚Austin 1300
    [引擎](../Page/引擎.md "wikilink")。CUB的設計者是來自英國利物浦的Charles Anderson
  - 澳門的 Happy-Rent-a-Car 有3輛 Mini Moke

## 維修

對 Mini Moke 有認識的專家表示，要這輛車正常地運作，需要頗多的定期維修。

## 前途

2006年 2月3日，在[香港電台第](../Page/香港電台.md "wikilink")3台的 Morning Brew
[節目中](../Page/節目.md "wikilink")，一名[嘉賓表示](../Page/嘉賓.md "wikilink")，因為[商業原因](../Page/商業.md "wikilink")，澳門的文華東方酒店將會把它們擁有的43輛
Mini Moke
停止運作。嘉賓還表示，這些車輛相信會被送往劏車場壓碎掉。原因是因為酒店當初是用[免稅方式把它們入口澳門的](../Page/免稅.md "wikilink")，如果要賣出，酒店便要付出稅款。

Happy-Rent-a-Car 有43輛 Mini Moke 供租賃，直至2月底。不能確定2月後的情況。

## 電視

拆下了車篷的 Mini Moke
[計程車](../Page/計程車.md "wikilink")，曾經在[電視劇集](../Page/電視.md "wikilink")*[The
Prisoner](../Page/The_Prisoner.md "wikilink")*和多部 007
[龐德系列](../Page/占士邦.md "wikilink")[電影中](../Page/電影.md "wikilink")，擔當演出。

[Category:各品牌車輛](../Category/各品牌車輛.md "wikilink")
[Category:英國汽車車種](../Category/英國汽車車種.md "wikilink")