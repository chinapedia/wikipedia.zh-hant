**趙申喬**（），[字](../Page/表字.md "wikilink")**慎旃**，又字**松五**，[號](../Page/號.md "wikilink")**白雲舊人**\[1\]，[直隸](../Page/南直隸.md "wikilink")[武進](../Page/武進.md "wikilink")（今[江蘇省](../Page/江蘇省.md "wikilink")[常州市](../Page/常州市.md "wikilink")[武進區](../Page/武進區.md "wikilink")）人，[清朝政治人物](../Page/清朝.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。

## 歷任

[明末](../Page/明末.md "wikilink")[御史](../Page/御史.md "wikilink")[趙繼鼎之子](../Page/趙繼鼎_\(清朝\).md "wikilink")，[狀元](../Page/狀元.md "wikilink")[趙熊詔之父](../Page/趙熊詔.md "wikilink")。[大學士](../Page/大學士.md "wikilink")[李光地的門人](../Page/李光地.md "wikilink")。[康熙八年](../Page/康熙.md "wikilink")（1671年）中[舉人](../Page/舉人.md "wikilink")，次年聯捷[進士](../Page/進士.md "wikilink")。[康熙二十年](../Page/康熙.md "wikilink")（1681年）授[河南](../Page/河南.md "wikilink")[商丘縣](../Page/商邱縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")。[康熙二十七年](../Page/康熙.md "wikilink")（1688年）升任[刑部](../Page/刑部.md "wikilink")[主事](../Page/主事.md "wikilink")。[康熙三十三年](../Page/康熙.md "wikilink")（1694年），遷[刑部](../Page/刑部.md "wikilink")[員外郎](../Page/員外郎.md "wikilink")。

[康熙四十年](../Page/康熙.md "wikilink")（1701年）經[直隸](../Page/直隸.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")[李光地舉薦](../Page/李光地.md "wikilink")，擢[浙江](../Page/浙江.md "wikilink")[布政使](../Page/布政使.md "wikilink")，革除南糧口袋及藩司陋規，以廉名顯於世。次年升[浙江巡撫](../Page/浙江巡撫.md "wikilink")，加[右副都御史銜](../Page/右副都御史.md "wikilink")。不久，改偏沅（[湖南](../Page/湖南.md "wikilink")）[巡撫](../Page/巡撫.md "wikilink")。

[康熙四十九年](../Page/康熙.md "wikilink")（1710年）升[左都御史](../Page/左都御史.md "wikilink")。誥封[資政大夫](../Page/資政大夫.md "wikilink")。[康熙五十年](../Page/康熙.md "wikilink")（1711年）擔任辛卯[順天](../Page/順天府.md "wikilink")[鄉試正考官](../Page/鄉試.md "wikilink")。同年，檢舉《南山集》作者[戴南山](../Page/戴南山.md "wikilink")「狂妄不謹」，引發「[南山集案](../Page/南山集案.md "wikilink")」，名世被斬。

[康熙五十一年](../Page/康熙.md "wikilink")（1712年）以[左都御史充壬辰科](../Page/左都御史.md "wikilink")[會試大總裁](../Page/會試.md "wikilink")。[康熙五十二年](../Page/康熙.md "wikilink")（1713年）改[戶部尚書](../Page/戶部尚書.md "wikilink")。[康熙五十七年](../Page/康熙.md "wikilink")（1718年）以[戶部尚書兼管錢法事務](../Page/戶部尚書.md "wikilink")，充戊戌科[會試副考官](../Page/會試.md "wikilink")。[康熙五十九年](../Page/康熙.md "wikilink")（1720年）卒，[諡](../Page/諡.md "wikilink")**恭毅**。[雍正元年](../Page/雍正.md "wikilink")（1723年）贈[太子太保](../Page/太子太保.md "wikilink")。[乾隆四年](../Page/乾隆.md "wikilink")（1739年）入祀[賢良祠](../Page/賢良祠.md "wikilink")。

## 著作

  - 《趙恭毅剩稿》八卷

## 註釋

## 參考資料

  - 《清史稿》卷二百六十三,列傳五十
  - 《清史館傳稿》5658號,5896號,7795號
  - [錢儀吉](../Page/錢儀吉.md "wikilink")
    編《[碑傳集](../Page/碑傳集.md "wikilink")》二冊,卷十九
  - 《清國史館傳稿》5741號,6603號,7780號
  - 福格《聽雨叢談》卷九
  - [中央研究院](../Page/中央研究院.md "wikilink")[歷史語言研究所](http://www.ihp.sinica.edu.tw/)內閣大庫檔案008659號,017757號,091442號,091479號,119320號,121705號

{{-}}

[Category:清朝商丘縣知縣](../Category/清朝商丘縣知縣.md "wikilink")
[Category:清朝刑部主事](../Category/清朝刑部主事.md "wikilink")
[Category:清朝刑部員外郎](../Category/清朝刑部員外郎.md "wikilink")
[Category:清朝浙江布政使](../Category/清朝浙江布政使.md "wikilink")
[Category:清朝浙江巡撫](../Category/清朝浙江巡撫.md "wikilink")
[Category:清朝左都御史](../Category/清朝左都御史.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:常州人](../Category/常州人.md "wikilink")
[S申](../Category/趙姓.md "wikilink")
[Category:諡恭毅](../Category/諡恭毅.md "wikilink")
[S](../Category/武進西蓋趙氏.md "wikilink")

1.  楊廷福,楊同甫 編《清人室名別稱字號索引》下冊,1606