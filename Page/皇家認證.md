<table>
<caption><big><strong>皇家認證</strong></caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:UK_Royal_Coat_of_Arms.svg" title="fig:UK_Royal_Coat_of_Arms.svg">UK_Royal_Coat_of_Arms.svg</a>女王於<a href="../Page/英格蘭.md" title="wikilink">英格蘭</a>，<a href="../Page/威爾斯.md" title="wikilink">威爾斯和</a><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭的徽章</a>]]</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Royal_Coat_of_Arms_of_the_United_Kingdom_(Scotland).svg" title="fig:Royal_Coat_of_Arms_of_the_United_Kingdom_(Scotland).svg">Royal_Coat_of_Arms_of_the_United_Kingdom_(Scotland).svg</a>的徽章]]</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Philip,_Duke_of_Edinburgh.svg" title="fig:Coat_of_Arms_of_Philip,_Duke_of_Edinburgh.svg">Coat_of_Arms_of_Philip,_Duke_of_Edinburgh.svg</a>的徽章]]</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Prince_of_Wales&#39;s_feathers_Badge.svg" title="fig:Prince_of_Wales&#39;s_feathers_Badge.svg">Prince_of_Wales's_feathers_Badge.svg</a>的徽章]]</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

**皇家委任認證（Royal Warrant of
Appointment）**，或簡稱**皇家認證**，是[英國皇室內有較高地位成員頒授予向他們提供商品及服務的公司和商人的稱號](../Page/英國皇室.md "wikilink")。他們可宣傳自己是擁有認證的皇家御用供應商以增強商業信譽。其他國家例如[荷蘭](../Page/荷蘭.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[泰國和](../Page/泰國.md "wikilink")[瑞典的皇室會允許商人宣傳皇室成員是他們的顧客](../Page/瑞典.md "wikilink")。

大約800名個人和商人，其中包括幾間非英國公司，持有超過1100份皇家認證。這些供應商要至少要提供用品予某位皇室成員至少五年才會被考慮授予認證。作為[皇家商人認證委員會](../Page/皇家商人認證委員會.md "wikilink")（Royal
Household Tradesmen's Warrants
Committee）主席，[宮務大臣握有委任決定權](../Page/宮務大臣.md "wikilink")。認證可每五年延續一次，有些供應商就持有超過一百年。但認證亦可隨時被收回，例如煙草公司[Benson
& Hedges的皇家認證就於](../Page/Benson_&_Hedges.md "wikilink")1999年被收回。

供應商獲得認證並不是要他們免費提供商品及服務，而是用於營銷上增強客戶對商品的信賴。英國商家通常會在大型廣告板、信紙頂部及商品上印上皇室的盾徽或者代表頒授該委任認證的皇室成員的徽章。在盾徽或徽章下方通常會有一段文字，寫上「By
Appointment to 【皇室成員頭銜】」
（由【皇室成員頭銜】委任），然後是供應商所提供的商品和服務，最後是供應商的名字；有時也會寫上該商戶的所在地。

## 歷史

最早的記錄是由[英國國王](../Page/英國國王.md "wikilink")[亨利二世於西元](../Page/亨利二世_\(英格蘭\).md "wikilink")1155年授予織工公司英國皇家的委任認證。

## 各國皇家權證持有人情況

### 獲[英國皇室委任的特許供應商](../Page/英國皇室.md "wikilink")

### 獲[荷蘭王室委任的特許供應商](../Page/荷蘭王室.md "wikilink")

（无）

### 獲[比利時王室委任的特許供應商](../Page/比利時王室.md "wikilink")

  - [Jules
    Destrooper](../Page/:en:Jules_Destrooper.md "wikilink")—[餅乾](../Page/餅乾.md "wikilink")
  - [歌帝梵巧克力](../Page/歌帝梵巧克力.md "wikilink")

### 獲[丹麥王室委任的特許供應商](../Page/丹麥王室.md "wikilink")

（无）

### 獲[瑞典王室委任的特許供應商](../Page/瑞典王室.md "wikilink")

（无）

### 獲[日本皇室委任的特許供應商](../Page/日本皇室.md "wikilink")

皇室御用達

  - [月桂冠](../Page/:ja:月桂冠_\(企業\).md "wikilink")—[清酒](../Page/清酒.md "wikilink")
  - [龜甲萬](../Page/龜甲萬.md "wikilink")—[醬油](../Page/醬油.md "wikilink")
  - [日清食品](../Page/日清食品.md "wikilink")—[食品](../Page/食品.md "wikilink")
  - [虎屋](../Page/:ja:虎屋.md "wikilink")—[和菓子](../Page/和菓子.md "wikilink")
  - [豐田汽車](../Page/豐田汽車.md "wikilink")—[汽車](../Page/汽車.md "wikilink")
  - [萬養軒](../Page/:ja:萬養軒.md "wikilink")—[餐飲](../Page/餐飲.md "wikilink")
  - [山田平安堂](../Page/:ja:山田平安堂.md "wikilink")—[漆器](../Page/漆器.md "wikilink")
  - [日本專賣公社](../Page/日本專賣公社.md "wikilink")—[菸草](../Page/菸草.md "wikilink")
  - [芦田淳](../Page/:ja:芦田淳.md "wikilink")—[時裝設計](../Page/時裝設計.md "wikilink")
  - [植田いつ子](../Page/:ja:植田いつ子.md "wikilink")—時裝設計
  - [大倉陶園](../Page/:ja:大倉陶園.md "wikilink")—[陶瓷](../Page/:ja:陶磁器.md "wikilink")
  - [大塚製靴](../Page/:ja:大塚製靴.md "wikilink")—[鞋](../Page/鞋.md "wikilink")
  - [Kagami](../Page/:ja:カガミクリスタル.md "wikilink")—[玻璃製品](../Page/玻璃.md "wikilink")
  - [Kakuq](../Page/:ja:カクキュー.md "wikilink")—[味噌](../Page/味噌.md "wikilink")
  - [川島織物](../Page/:ja:川島織物セルコン.md "wikilink")—[纖維製品](../Page/纖維.md "wikilink")
  - [木村屋總本店](../Page/:ja:木村屋總本店.md "wikilink")—[糕點](../Page/糕點.md "wikilink")
  - [Parfums
    Caron](../Page/:ja:キャロン_\(香水\).md "wikilink")—[香水](../Page/香水.md "wikilink")
  - [鳩居堂](../Page/:ja:鳩居堂.md "wikilink")—[文具](../Page/文具.md "wikilink")
  - [GINZA YOSHINOYA](../Page/:ja:銀座ヨシノヤ.md "wikilink")—鞋
  - [黑江屋](../Page/:ja:黒江屋.md "wikilink")—漆器
  - [香蘭社](../Page/:ja:香蘭社.md "wikilink")—陶瓷
  - [Colombin](../Page/:ja:コロンバン.md "wikilink")—[西點](../Page/西點.md "wikilink")
  - [Chez matsuo](../Page/:ja:シェ松尾.md "wikilink")—食品
  - [SOMES](../Page/:ja:ソメスサドル.md "wikilink")—[皮革製品](../Page/皮革.md "wikilink")、[馬具用品](../Page/馬具.md "wikilink")
  - [高島屋](../Page/高島屋.md "wikilink")—[百貨](../Page/百貨.md "wikilink")
  - [千總](../Page/:ja:千總.md "wikilink")—[和服](../Page/和服.md "wikilink")
  - [辻常陸十四代目](../Page/:ja:辻常陸_\(十四代\).md "wikilink")—陶瓷
  - [天童木工](../Page/:ja:天童木工.md "wikilink")—家具
  - [百達翡麗](../Page/百達翡麗.md "wikilink")—[鐘錶](../Page/鐘錶.md "wikilink")
  - [東丸醬油](../Page/東丸醬油.md "wikilink")—醬油
  - [Higeta醬油](../Page/:ja:ヒゲタ醤油.md "wikilink")—醬油
  - [風月堂](../Page/:ja:風月堂.md "wikilink")—糕點
  - [深川製瓷](../Page/:ja:深川製磁.md "wikilink")—陶瓷
  - [文明堂](../Page/:ja:文明堂.md "wikilink")—糕點
  - [松坂屋](../Page/松坂屋.md "wikilink")—百貨
  - [三越](../Page/三越百貨.md "wikilink")—百貨
  - [宮本卯之助商店](../Page/:ja:宮本卯之助商店.md "wikilink")—[樂器](../Page/樂器.md "wikilink")、[祭祀用品](../Page/祭祀.md "wikilink")
  - [YAMASA醬油](../Page/:ja:ヤマサ醤油.md "wikilink")—[調味料](../Page/調味料.md "wikilink")、醫藥用品
  - [山本海苔店](../Page/:ja:山本海苔店.md "wikilink")—[海苔](../Page/海苔.md "wikilink")
  - [J. & L. Lobmeyr](../Page/:ja:ロブマイヤー.md "wikilink")—玻璃製品

### 獲[泰國王室委任的特許供應商](../Page/泰國王室.md "wikilink")

（无）

### 獲[摩納哥王室委任的特許供應商](../Page/摩納哥王室.md "wikilink")

（无） Lancaster-护肤品

## 法律意思

皇家認證的英文Royal Warrant在法律上有別的意思，香港中譯為英廷委任狀。

## 另見

  - [御用達](../Page/:ja:御用達.md "wikilink")
  - [御用商人](../Page/:ja:御用商人.md "wikilink")

## 外部連結

  - [皇家認證持有人協會（英語）](http://www.royalwarrant.org/)

## 參考文獻

[Category:王室](../Category/王室.md "wikilink")
[Category:商業](../Category/商業.md "wikilink")