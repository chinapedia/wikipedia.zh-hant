**丹尼尔·路易斯·“丹”·卡斯泰拉内塔**（**Daniel Louis "Dan"
Castellaneta**，）是一位[美国](../Page/美国.md "wikilink")[配音演员](../Page/配音演员.md "wikilink")、[演员及](../Page/演员.md "wikilink")[喜剧演员](../Page/喜剧演员.md "wikilink")，曾获得过三次[艾美奖](../Page/艾美奖.md "wikilink")。他最著名的配音角色是[福克斯](../Page/福克斯广播公司.md "wikilink")[动画节目](../Page/动画节目.md "wikilink")《[辛普森一家](../Page/辛普森一家.md "wikilink")》中的[霍默·辛普森](../Page/霍默·辛普森.md "wikilink")。\[1\]

## 參考文獻

## 外部链接

  -
  - [《辛普森一家》配音演员](https://web.archive.org/web/20071022033006/http://www.snpp.com/guides/castlist.html#civil)

  - [对丹·卡斯泰拉内塔的十个问题](http://movies.ign.com/articles/357/357502p1.html)

[Category:美国电影演员](../Category/美国电影演员.md "wikilink")
[Category:美国舞台剧演员](../Category/美国舞台剧演员.md "wikilink")
[Category:美国电视演员](../Category/美国电视演员.md "wikilink")
[Category:美国男配音演员](../Category/美国男配音演员.md "wikilink")
[Category:意大利裔美国人](../Category/意大利裔美国人.md "wikilink")

1.  Stated in interview on *[Inside the Actors
    Studio](../Page/Inside_the_Actors_Studio.md "wikilink")*