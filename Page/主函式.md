在许多[命令式](../Page/命令式编程.md "wikilink")[程式設計語言裡](../Page/程式設計語言.md "wikilink")，**主函式**（**main
function**）是程式開始執行的地方。而相对地，其它[编程范式的语言中就很少会有这样一个概念](../Page/编程范式.md "wikilink")。

## 程式語言的相異性

### [机器](../Page/机器语言.md "wikilink") / [汇编语言](../Page/汇编语言.md "wikilink")

在现代的计算机体系中，CPU
下一次要执行的指令地址由一个[寄存器指出](../Page/寄存器.md "wikilink")，称为“[指令指针](../Page/指令指针.md "wikilink")”（Instruction
Pointer、IP）或“[程序计数器](../Page/程序计数器.md "wikilink")”（Program
Counter、PC）。该寄存器在系统上电或复位时由硬件电路置为某地址值，位于该地址的程序即可视为整个程序的入口点。

这个概念一般只在[嵌入式程序](../Page/嵌入式.md "wikilink")（[固件](../Page/固件.md "wikilink")）或[操作系统的引导代码中使用](../Page/操作系统.md "wikilink")，因为给非嵌入式系统写的程序通常包含数量庞大的机器指令、以至于关注一两个机器指令是没有意义的。

### [可执行文件](../Page/可执行文件.md "wikilink")

所谓“可执行”，说白了就是这个文件存储了一些信息、好创造某个让程序可以开始运行的内存状态，而操作系统[装载并创建](../Page/加载器.md "wikilink")[进程的步骤就是把这个满是机器指令的内存状态复原出来并开始运行](../Page/进程.md "wikilink")。因此，可执行文件中的“程序入口点”表示把加载进来的哪个地址当作“开始运行”的那一条指令。

### C/C++

在[C語言或](../Page/C語言.md "wikilink")[C++程式語言](../Page/C++.md "wikilink")，`main`的函式原型如下所示：

``` cpp
int main(void)
int main()
int main(int argc, char *argv[])
```

`main`的兩個環境參數：`argc`（*argument count*）與`argv`（*argument
vector*）\[1\]，兩者個別自程式的指令列給予參數的數量與參數陣列的指標位址。`argc`與`argv`這兩個參數的名稱，在程式語言的命名規則底下，雖然可以依照使用者的喜好自行定義，但是一般在使用上，還是會以現有的名稱`argc`與`argv`來進行程式的編寫。其他具有相依平台的格式也可以被C與C++標準所接受；例如，[UNIX](../Page/UNIX.md "wikilink")（非[POSIX.1](../Page/POSIX.md "wikilink")）與[Microsoft
Visual
C++有第三個參數](../Page/Microsoft_Visual_C++.md "wikilink")，是用來接收程式的[環境變數](../Page/環境變數.md "wikilink")，利用其他方法存取環境變數，可以使用`stdlib.h`標頭檔案定義的`getenv`函式：

``` c
int main(int argc, char *argv[], char *envp[])
```

[Mac OS X與](../Page/Mac_OS_X.md "wikilink")[Apple
Darwin有第四個參數](../Page/Apple_Darwin.md "wikilink")，它含有作業系統支援性的資訊，很像是執行二進制檔案的路徑：\[2\]

``` c
int main(int argc, char *argv[], char *envp[], char *apple[])
```

由`main`傳給作業系統的傳回值，代表程序處理的結束狀態，在C的標準裡有定義兩個傳回值：`EXIT_SUCCESS`（通常是整數零
*0*值）與`EXIT_FAILURE`。由於在實作上考量到各種的可能性，所以依照可能會發生的狀態來定義傳回值。

依照一般使用的規則，指令列參數的第一個元素就是程式檔案的名稱，假如程式檔案的名稱為`rm.exe`，當使用者在指令列輸入`rm
file`後，使用者介面的程式shell會初始化`rm.exe`處理程序，設定環境變數`argc = 2`以及`argv =
["rm", "file"]`。

`main`這個名稱是一個特有的名稱；正常來說，使用者在每個C與C++程式自定的函式名稱，必須不同於`main`這個名稱。

在C++裡，`main`一定是在全域的名稱空間內（例如：`::main`），它不可以是類別或是實體的成員函式。

由於前置處理器的關係，以下`main`函式原始碼可適用於[Microsoft Visual
C++與](../Page/Microsoft_Visual_C++.md "wikilink")[Dev-C++](../Page/Dev-C++.md "wikilink")：

``` cpp
#ifndef _MSC_VER
int
#else
void
#endif
main(int argc, char** argv)
{
    // 程式碼
    system("PAUSE");
    return
#ifndef _MSC_VER
    EXIT_SUCCESS
#endif
    ;
}
```

#### WinMain

[Dev_win32.png](https://zh.wikipedia.org/wiki/File:Dev_win32.png "fig:Dev_win32.png")

以[微軟視窗為基礎的程式設計上](../Page/Microsoft_Windows.md "wikilink")，`WinMain`\[3\]函式是視窗程式的進入點，函式原型如下所示：

``` cpp
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
```

### .Net 语言（C\#、VB 等）

以[C\#編寫的程式](../Page/C_Sharp_\(程式語言\).md "wikilink")，在开始執行時[CLR會先去尋找帶有](../Page/CLR.md "wikilink")`.entrypoint`
IL標示的靜態方法，而這個靜態方法可能沒有參數，或是只有單一`string[]`型態的參數，而且還有`void`或是`int`型態的傳回值，找到後才會執行這個方法，而這個方法就是主函式。\[4\]

``` csharp
static void Main();
static void Main(string[] args);
static int Main();
static int Main(string[] args);
```

指令列參數會被引入到`args`變數內，引入的方式與Java相類似。但是，針對有整數傳回值的`Main`而言，引入的方式類似C/C++，而指令列參數會被當成處理程序的結束狀態，傳回給作業系統或是執行它的外部環境。

同为 .net 语言的 [Visual Basic .NET](../Page/Visual_Basic_.NET.md "wikilink")
大体也是一样。不过不同的是，旧版本 [Visual Basic](../Page/Visual_Basic.md "wikilink")
的程序可以选择在执行默认初始化步骤后以一个窗体启动，此时的主函数是不可见的；新版本为了兼容性和简化编码工作而保留了这个特性。

### Java

[Java程式語言是以main](../Page/Java.md "wikilink")
[方法來當做程式開始的起點](../Page/方法_\(電腦科學\).md "wikilink")，[方法如下](../Page/方法_\(電腦科學\).md "wikilink")：

``` java
public static void main(String[] args)
```

命令列指定參數是*args*。如同C和C++一樣，「main」也是唯一的。Java的main方法無任何的傳回值。

### Pascal

[Pascal的主要程序是不需命名的](../Page/Pascal.md "wikilink")。因為Pascal程式的程序和函式在編寫程式上比C、C++或是Java更加嚴格，在程式裡主要程序通常才是最後的程序。Pascal沒有main或是其他相類似的關鍵字。

以下是Hello world範例：

``` pascal
procedure hello() begin
  writeln('Hello world')
end;
begin
  hello()
 end.
```

### Pike

[Pike與C](../Page/Pike.md "wikilink")/C++的語法相類似。從main開始執行。「argc」代表環境[參數的個數](../Page/參數.md "wikilink")。「argv」代表環境參數的值。

舉例如下：

``` java
int main(int argc, array(string) argv)
```

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [Python
    main()函式](http://www.artima.com/weblogs/viewpost.jsp?thread=4829)

[M](../Category/程序架構.md "wikilink")
[M](../Category/带有C++代码示例的条目.md "wikilink")

1.  argv: the vector term in this variable's name is used in traditional
    sense to refer to strings.（基本上這個參數是以傳統的方法對參數的字串進行存取的動作，相當於指標陣列）
2.
3.  <http://msdn.microsoft.com/en-us/library/ms633559%28VS.85%29.aspx>
4.