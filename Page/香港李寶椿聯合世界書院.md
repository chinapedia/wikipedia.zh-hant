[LPCUWC_Front.jpg](https://zh.wikipedia.org/wiki/File:LPCUWC_Front.jpg "fig:LPCUWC_Front.jpg")
**香港李寶椿聯合世界書院**（****）是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")[西沙路落禾沙里](../Page/西沙路.md "wikilink")10號的一所[國際學校](../Page/國際學校.md "wikilink")，為全球17間（第8間開設）[聯合世界書院之一](../Page/聯合世界書院.md "wikilink")，現時約有250名來自80多國的學生\[1\]在這所[寄宿](../Page/寄宿.md "wikilink")[學校就讀](../Page/學校.md "wikilink")。1994年\[2\]獲准加入成為[直接資助計劃](../Page/直接資助計劃.md "wikilink")\[3\]的學校。\[4\]根據“[華爾街日報](../Page/华尔街日报.md "wikilink")”的報導，學校的常春藤盟校大學錄取率是全球最高之一\[5\]。學校最近慶祝了其25週年紀念。

## 歷史

香港李寶椿聯合世界書院的成立源於1978年，由[李寶椿之子李兆增在](../Page/李寶椿.md "wikilink")1987年捐资一亿元在烏溪沙筹建。當時擔任香港「聯合世界書院國家委員會」主席的[利國偉](../Page/利國偉.md "wikilink")[爵士負責挑選香港學生往海外的聯合世界書院就讀](../Page/爵士.md "wikilink")，對聯合世界書院畢業生的才幹及對社會的貢獻深感認同，在1982年離任後仍然繼續支持書院的活動，希望可以在中國建立一所聯合世界書院\[6\]。

當時仍然擔任[恒生銀行主席的利國偉與持有家族成立的](../Page/恒生銀行.md "wikilink")「[李寶椿慈善信託基金](../Page/李寶椿.md "wikilink")」（Li
Po Chun Charitable
Trust）的李兆增於1987年首先提出在香港設立聯合世界書院，獲得當時的[港督](../Page/香港總督.md "wikilink")[衛奕信爵士及](../Page/衛奕信.md "wikilink")「大西洋聯合世界書院」校長大卫·萨特克利夫（David
Sutcliffe）熱烈支持，以衛奕信名義成立的「衛奕信勳爵聯合世界書院獎學金基金」至今仍然為到港就讀的學生提供[獎學金](../Page/獎學金.md "wikilink")。

籌建理事會的成員往海外的聯合世界書院進行考察，與[政府就數個未來書院選址進行漫長而詳細的討論](../Page/港英政府.md "wikilink")，結果選擇了位於[烏溪沙](../Page/烏溪沙.md "wikilink")[白石的大片土地](../Page/白石.md "wikilink")，早年是[船灣淡水湖堤壩的採石場之一](../Page/船灣淡水湖.md "wikilink")（現時室內體育館背後的斜坡見證採石的痕跡），於[政權移交前夕在中英土地委員會](../Page/香港政權移交.md "wikilink")（Sino-British
Land
Commission）同意下獲得50年租地條款。當時的地盤甚為偏遠，面臨[吐露港而背靠](../Page/吐露港.md "wikilink")[馬鞍山郊野公園](../Page/馬鞍山郊野公園.md "wikilink")，附近沒有大型的社區發展及交通配套。

1991年當獲得足夠的建校資金，工程馬上展開。1992年2月12日衛奕信爵士奠基，並於1992年9月落成迎來第一批學生。1992年11月6日由[查理斯王子主持正式揭幕](../Page/查爾斯王子_\(威爾斯親王\).md "wikilink")，在計劃獲得「聯合世界書院國際理事會」（UWC
International Board）批准後不足18個月便完工啟用。

首名獲聘的職員是創校校長大衛·威爾金森博士（Dr David
Wilkinson），最初只能在[沙田一間酒店的套房工作](../Page/沙田區.md "wikilink")，於1992年上半年招聘員工，部分開拓員工至今仍然在任。威尔金森於1994年離任，前往[曼谷建立一所學校](../Page/曼谷.md "wikilink")，及後再被委派為「印度馬軒德拉聯合世界書院」的創校校長。

1994年8月布萊爾·佛斯特（Blair
Forster）履新出任校長，在長達九年的任期中表現出色，直到2003年9月因病去世才結束。在任期間書院按聯合世界書院的理念穩步發展及成長，學業成積逐漸改善到成為全球聯合世界書院[IB考試最佳的書院之一](../Page/國際文憑大學預科課程.md "wikilink")。以已故校長為名的「布莱尔·佛斯特紀念基金」（Blair
Forster Memorial Trust）提供獎學金以協助[東帝汶的年青人](../Page/東帝汶.md "wikilink")。

[郭林同博士](../Page/郭林同.md "wikilink")（Dr Stephen
Codrington）於2004年5月成為新任校長\[7\]。2000年4月利國偉卸任理事會主席，由前[教育署署長李越挺接任](../Page/教育署.md "wikilink")。

### 校董會\[8\]

  - 湯啟康先生<sub>[BBS](../Page/銅紫荊星章.md "wikilink")</sub> （主席）-
    前副[教育署長](../Page/教育局_\(香港\).md "wikilink")
  - 劉劉穎雯女士 - 前[教育統籌局首席教育主任](../Page/教育局_\(香港\).md "wikilink")
  - [陳維安先生](../Page/陳維安.md "wikilink") <sub>SBS</sub> -
    [香港立法會秘書長](../Page/香港立法會.md "wikilink")，曾任[教育局副局長及](../Page/教育局_\(香港\).md "wikilink")[香港賽馬會馬場事務部總監](../Page/香港賽馬會.md "wikilink")
  - 傅鄺穎婷女士 -
    [康宏環球獨立非執行董事](../Page/康宏金融集團.md "wikilink")，[聯合世界書院](../Page/聯合世界書院.md "wikilink")[大西洋書院校友](../Page/大西洋書院.md "wikilink")
  - 李鍔博士 - [香港大學文學院前院長](../Page/香港大學文學院.md "wikilink")
  - 梁少光先生 - 前[香港中文大學秘書長](../Page/香港中文大學.md "wikilink")
  - 梁堅先生 -
    [花旗集團中國區投資銀行業務董事總經理](../Page/花旗集团.md "wikilink")，[聯合世界書院](../Page/聯合世界書院.md "wikilink")[大西洋書院校友](../Page/大西洋書院.md "wikilink")
  - 李瑞智先生 - [李寶椿慈善信託基金委員會成員](../Page/李寶椿.md "wikilink")，李兆增的兒子
  - 梁祥彪先生 -
    [利國偉博士的女婿](../Page/利國偉.md "wikilink")，現任[偉倫有限公司集團行政總](../Page/偉益有限公司.md "wikilink")
  - [呂元祥博士](../Page/呂元祥建築師事務所.md "wikilink") -
    呂元祥建築師事務所（香港）有限公司創辦人及主席兼[香港浸會大學基金董事局副主席](../Page/香港浸會大學.md "wikilink")
  - [沈祖堯醫生](../Page/沈祖堯.md "wikilink")<sub>SBS, JP</sub> -
    前[香港中文大學校長](../Page/香港中文大學.md "wikilink")
  - 譚麗芬醫生<sub>JP</sub> -
    前[衞生署副署長](../Page/衞生署_\(香港\).md "wikilink")，食物環境衞生署的食物安全中心專員，[聯合世界書院](../Page/聯合世界書院.md "wikilink")[大西洋書院校友](../Page/大西洋書院.md "wikilink")
  - 尹葉芊芊教授 - 前[香港中文大学校外进修学院院长](../Page/香港中文大學.md "wikilink")
  - 黃金蓮修女<sub>MBE</sub> - [聖保祿學校校長](../Page/聖保祿學校_\(香港\).md "wikilink")

### 歷任校長

  - **大衛·威爾金森**博士（Dr David Wilkinson，1992-1994）
  - **布萊爾·佛斯特**（Mr Blair Forster，1994-2003）
  - **郭林同**博士（Dr Stephen Codrington，2004-2011 ）
  - **艾德華**（Mr Arnett Edwards，2011- ）

## 校園環境

香港李寶椿聯合世界書院位於[新界](../Page/新界.md "wikilink")[沙田以北約](../Page/沙田.md "wikilink")6[公里的](../Page/公里.md "wikilink")[烏溪沙](../Page/烏溪沙.md "wikilink")，距離[港鐵的](../Page/港鐵.md "wikilink")[馬鞍山綫終站](../Page/馬鞍山綫.md "wikilink")[烏溪沙站約](../Page/烏溪沙站.md "wikilink")5分鐘腳程。書院面向[吐露港而背靠](../Page/吐露港.md "wikilink")[馬鞍山](../Page/馬鞍山_\(香港山峰\).md "wikilink")，正門位於落禾沙里，西面為年華路。

香港李寶椿聯合世界書院校園佔地甚廣，擁有多座不同的校舍及設施。

主要建築物有：

1.  教學大樓 (Academic Block)
      - 內有課室、[實驗室](../Page/實驗室.md "wikilink")、圖書館、室外游泳池、宗教聚會所(Spiritual
        Center)、學生休息室(Common
        Room)及小型演講室。圖書館位於教學大樓的頂層。圖書館內除了有學習區，還有一個休閒區，學生可以瀏覽雜誌和報紙。圖書館還提供打印機，掃描儀和電腦來支持學生的需求。
2.  室內體育館 (Sports Building)
      - 內有攀岩牆、籃球場、排球場、足球場、壁球場、舞蹈室及適合進行有氧運動和舉重訓練的健身室。
3.  利國偉堂 (Assembly Hall)
      - 內有演講廳、戲劇藝術課室。演講廳為所有重要的表演和會議提供舞台。演講廳主要舉行**書院**會議(College
        Meeting)，文化之夜(Cultural Evening)，戲劇表演，全球問題論壇等。
4.  室內飯堂(Canteen)
      - 書院的主餐廳及廚房可以提供多達240人的膳食，供應早、午及晚三餐，有中、西及[素食餐單](../Page/素食.md "wikilink")。
5.  四座宿舍(Residential Block)
      - 每座擁有十六間四床房間，兩個衛生間，還有職員宿舍、醫務室、公用休息室(Day
        Room)及洗衣設備，有蓋通道連結主要建築物。公用休息室是學生聚集，社交和學習的社交空間。這也是每週舉行宿舍會議(Block
        Meeting)的地方。內有沙發，冰箱和小廚房空間
6.  校長宿舍

### 開放日

香港李寶椿聯合世界書院每年年底會安排兩個開放日，分別於十月初和十一月底舉行；公眾可以參觀校園環境、了解課程及與學生對話。\[9\]

## 課程

李寶椿聯合世界書院開設一個兩年制「[國際文憑大學預科課程](../Page/國際文憑大學預科課程.md "wikilink")」（International
Baccalaureate Diploma）。課程要求考生修讀六個科目，其中三科屬於「高級程度」（Higher
Level），其餘則屬於「標準程度」（Standard Level）。

書院允許一年級的學生選修四門「高級程度」的科目。在特殊情況下，學生也可以選修七個科目，或以一個額外的科學取代第三組的科目。

學生必須從以下第一至第五組的每一組別中，選修最少一個科目。學生的第六個科目可以是「[視覺藝術](../Page/視覺藝術.md "wikilink")」、「戲劇藝術」，或者從其他組別中再選一科。除此之外也必須修讀「知識理論」課，完成一篇畢業論文，並完成「全才活動」的要求。

  - 第一組：第一語言（以[母語教授的文學課程](../Page/母語.md "wikilink")）

<!-- end list -->

  - 中文、英文及西班牙文《A1》（HL 及 SL）
  - 其他語言《A1》（只限 SL）

<!-- end list -->

  - 第二組：第二語言（外語課程）

<!-- end list -->

  - 英文《A2》（以專題研習及文學為本）
  - 英文《B》（HL 及 SL）
  - 中文《B》（HL 及 SL）
  - 中文《ab initio》（只限 SL）
  - 法文《B》（HL 及 SL）
  - 法文《ab initio》（只限 SL）

<!-- end list -->

  - 第三組：個人及社會

<!-- end list -->

  - 中國研究（只限 SL）
  - [經濟](../Page/经济学.md "wikilink")（HL 及 SL）
  - [歷史](../Page/歷史學.md "wikilink")（HL 及 SL）
  - [地理](../Page/地理学.md "wikilink")（HL 及 SL）

<!-- end list -->

  - 第四組：實驗科學

<!-- end list -->

  - [生物](../Page/生物.md "wikilink")（HL 及 SL）
  - [化學](../Page/化学.md "wikilink")（HL 及 SL）
  - [物理](../Page/物理学.md "wikilink")（HL 及 SL）
  - 環境系統（只限 SL）

<!-- end list -->

  - 第五組：數學

<!-- end list -->

  - [數學](../Page/數學.md "wikilink")（HL 及 SL）
  - 數學研習（只限 SL）

<!-- end list -->

  - 第六組：藝術及選修科目

<!-- end list -->

  - 視覺藝術（HL 及 SL）
  - 戲劇藝術（HL 及 SL）

## 課外活動 (Education Outside of the Classroom Activities)

香港李寶椿聯合世界書院的課外活動課程側重於體驗式學習， 課外活動課程對於李寶椿聯合世界書院的價值至關重要。

該計劃包括：

  - 全才活動（IB CAS）活動

<!-- end list -->

  - 中國週(China Week)和專題研究週(Project Week)
  - 其他旅行
  - 文化之夜(Cultural Evenings)
  - 迎新週(Orientation Week)
  - 體育和運動隊
  - 戶外活動
  - 急救訓練
  - 學生領導

課外活動課程的目標是：

  - 促進體驗式學習
  - 通過行動來學習
  - 服務他人
  - 學生領導
  - 挑戰和風險承擔
  - 整體個人發展

### 全才活動

全才活動（Quan
Cai）\[10\]共分四個範疇，包括「**社區服務**」、「**校園服務**」、「**藝術**」及「**運動**」。學生需在四個範疇中各選一項參加。
全才活動於週一晚上，週二，週四和周五下午以及傍晚和周末舉行。

李寶椿聯合世界書院特別注重「社區服務」，目的是要學生融入社區及通過創新服務計劃，將知識、技能及各地文化與人共同分享，服務老人、殘疾人士、不幸的年青人等。校園生活中的小賣部、學生電腦系統及校報（BTW）也是由學生主持。

院方提供不同種類的運動及娛樂活動，包括：[羽毛球](../Page/羽毛球.md "wikilink")、[游泳](../Page/游泳.md "wikilink")、[風帆](../Page/風帆.md "wikilink")、[獨木舟](../Page/獨木舟.md "wikilink")、[壁球](../Page/壁球.md "wikilink")、[網球](../Page/網球.md "wikilink")、[足球及](../Page/足球.md "wikilink")[排球](../Page/排球.md "wikilink")，學生亦參與[遠足](../Page/遠足.md "wikilink")、[露營及郊遊等](../Page/露營.md "wikilink")。[美術](../Page/美術.md "wikilink")、[音樂及](../Page/音樂.md "wikilink")[戲劇都很受歡迎](../Page/戲劇.md "wikilink")，很多活動都是學生自行組織的。

總共提供74種不同種類的全才活動。

  - 24種運動
  - 5個以環境為中心的體驗
  - 20個**社區服務**
  - 24個**校園服務**

### 中國週(China Week)和專題研究週(Project Week)

中國週(China Week)在第一學期舉行，專題研究週(Project
Week)在第二學期舉行。中國週專注於赴中國和香港的旅行，而專題研究週專注於東亞地區的旅行。學生通過提出，領導和參與旅行來培養獨立性和領導能力。

中國週(China Week)的地點包括：

  - 廣東
  - 東莞
  - 海南
  - 南寧
  - 陽朔
  - 湖南

專題研究週(Project Week)的地點包括：

  - 柬埔寨
  - 香港
  - 印尼西
  - 中國
  - 泰國
  - 馬來西亞
  - 日本
  - 菲律賓
  - 韓國
  - 斯里蘭卡
  - 越南
  - 北韓

### 文化之夜(Cultural Evenings)

文化之夜包括表演和晚宴，目的是促進國際的理解和欣賞。每年舉辦四次文化之夜。，每個文化之夜都聚焦於不同的地區（非洲地區，亞太地區，歐洲地區，中東和南亞地區，中國地區，北美和南美地區)。

## 學生

聯合世界書院是全球唯一的教育機構在預科程度，將世界各地的學生，不論貧富，聚集在一起學習。學生以自身的優點而被挑選，在一個培養國際諒解、和平及正義的環境下共同生活。聯合世界書院是由不同國籍及文化的學生組成的社區，所有書院有共同使命，但每間書院亦有其獨特的功能，自1962年首間聯合世界書院成立以來，約有30,000名學生入讀。

香港李寶椿聯合世界書院位處香港，成為來自不同地區的學生了解中國文化的橋樑。現時約有四成為香港本地學生，其餘來自世界各地，大部分的學生也獲得不同的獎學金或助學金資助學費。

香港李寶椿聯合世界書院的平均IB成績近年來一直徘徊在38分左右，為全球最高平均水平之一。
2017年，3名學生獲得45分的滿分。由於這些學術成就，香港李寶椿聯合世界書院被認為是世界頂尖高中之一。
2007年，“華爾街日報”將香港李寶椿聯合世界書院定為全球前50名學校之一，香港李寶椿聯合世界書院成功地幫助學生進入常春藤大學，這是美國境外僅有的兩所學校之一，也是唯一一家在美國以外的聯合世界書院進入名單。

### 報讀甄選

香港李寶椿聯合世界書院每年招收120名新生，其中50個名額為香港學生，其餘70個名額給海外學生，海外學生的申請，需由當地的「聯合世界書院國家委員會」負責人遞交。

香港申請人一般需於[中學會考取得](../Page/香港中學會考.md "wikilink")22分以上\[11\]、或於GCSE取得3A3B成績，入讀申請先由校長及由資深教師組成的審查團批閱，主要審查學業成績及相關資料、申請人原讀學校校長的推薦信及學生申請入讀的理由，從中甄選出一份名單參加每年二月舉行的「挑戰日」\[12\]。在「挑戰日」申請學員被分派到不同小組參加不同有趣的項目，從而了解學員的團體精神、創造能力、積極性及其他入讀的因素。最後申請學員需與「聯合世界書院國家委員會」的成員進行面對面會談，成員會對學員的時事觸角、服務社群及生活理念作詳盡探討。

## 文化傳統

香港李寶椿聯合世界書院提供了多元化的 [中國文化](../Page/中华文化.md "wikilink")
活動\[13\]，於課程內提供不同程度的「中國語文科」，並提供「中國研究科」。課餘活動亦包括舞龍、舞獅、[中國舞](../Page/中國舞蹈.md "wikilink")、[書法](../Page/中国书法.md "wikilink")
及
[烹飪](../Page/中国菜.md "wikilink")。於每年舉行的「專題研究週」及「中國週」，安排學生到中國內地旅行，令學生有機會實地體驗生活、遊覽歷史及地理名勝\[14\]。

## 香港傑出學生選舉

直至2018年(第33屆)，在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")1名傑出學生。

## 著名校友

### 政界

  - [李梓敬](../Page/李梓敬.md "wikilink")：深水埗區議員
  - [黎穎瑜](../Page/黎穎瑜.md "wikilink")：勞工及福利局政治助理，[文藝復興基金會副理事長](../Page/文藝復興基金會.md "wikilink")
  - Niki Ashton： [加拿大國會議員](../Page/加拿大國會.md "wikilink") (馬尼托巴省 )

### 商界

  - 伍穎莊：賽馬會獎學金首對孿生獲獎者\[15\]
  - 伍穎妍：賽馬會獎學金首對孿生獲獎者\[16\]

### 專業界

  - 鍾志豪：香港中文大學賽馬會公共衛生及基層醫療學院助理教授、香港中西醫結合醫學研究所副主任
  - [袁瑋熙](../Page/袁瑋熙.md "wikilink")：香港政治學家、[嶺南大學政治學系教授](../Page/嶺南大學_\(香港\).md "wikilink")

### 演藝界

  - [梁彥宗](../Page/梁彥宗.md "wikilink")：[香港無綫電視基本合約男藝員](../Page/香港無綫電視.md "wikilink")，澳洲絕世筍工最後三強
  - [王嘉儀](../Page/王嘉儀.md "wikilink")：[香港無綫電視巨聲幫](../Page/香港無綫電視.md "wikilink")
  - [吳芷寧](../Page/吳芷寧.md "wikilink")：[無綫新聞](../Page/無綫新聞.md "wikilink")[主播](../Page/主播.md "wikilink")
  - [劉晉安](../Page/劉晉安.md "wikilink")：[無綫新聞](../Page/無綫新聞.md "wikilink")[記者](../Page/記者.md "wikilink")
  - 克萊門·比亨特·克斯古 : 丹麥著名電視節目主持人，2011年丹麥媒體人獎獲獎者

### 體育界

  - 麥素寧：2008 北京奧運會香港三項鐡人女子代表\[17\]

### 文化界

  - 陳以琳：[倫敦交響樂團助理指揮](../Page/倫敦交響樂團.md "wikilink") ，
    唐娜泰拉．弗里克指揮大賽（Donatella Flick LSO
    Conducting Competition）首名女得獎人

## 參見

  - [國際文憑大學預科課程](../Page/國際文憑大學預科課程.md "wikilink")

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [香港李寶椿聯合世界書院正式網站](http://www.lpcuwc.edu.hk/)
  - [聯合世界書院官方網站](http://www.uwc.org/)

[Category:聯合世界書院](../Category/聯合世界書院.md "wikilink")
[Category:香港預科書院](../Category/香港預科書院.md "wikilink")
[Category:香港直資學校](../Category/香港直資學校.md "wikilink")
[Category:烏溪沙](../Category/烏溪沙.md "wikilink")
[Category:1992年創建的教育機構](../Category/1992年創建的教育機構.md "wikilink")

1.  [LPCUWC：Introduction to UWC
    Movement](http://www.lpcuwc.edu.hk/en/about/about_intro.php)
2.  [財務委員會討論文件](http://www.legco.gov.hk/yr95-96/chinese/fc/fc/papers/fc240514.htm)
3.  [教育局：直接資助計劃](http://www.edb.gov.hk/index.aspx?nodeid=1475&langno=2)
4.  [專題：直資名校搞一校兩制](http://hk.apple.nextmedia.com/news/art/20121028/18050893)
5.
6.  [LPCUWC：History](http://www.lpcuwc.edu.hk/en/about/about_history.php)

7.  [LPCUWC：About the
    Principal](http://www.lpcuwc.uwc.org/en/about/about_principalbiography.php)

8.
9.  [LPCUWC：Open
    Days](http://www.lpcuwc.edu.hk/en/admission/admission_openday.php)
10. [LPCUWC：Quan Cai](http://www.lpcuwc.edu.hk/en/qc/)
11.
12. [LPCUWC：Challenge
    Day 2004](http://www.lpcuwc.edu.hk/en/events/events_showall.php?event_id=87)

13. [LPCUWC：Chinese
    Dimension](http://www.lpcuwc.edu.hk/en/about/about_chinese.php)
14. [世界書院“中國周活動”寧德行。](http://www.icn.cn/Fujian_w/news/bc/big5/20061112/jryw103840.html)

15. [港大尖子孿生姊妹同獲獎學金](http://paper.wenweipo.com/2006/03/24/HK0603240055.htm)
16. [港大尖子孿生姊妹同獲獎學金](http://paper.wenweipo.com/2006/03/24/HK0603240055.htm)
17. [誓憑三鐵闖奧運
    麥素寧夢繫北京](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20061009&sec_id=25391&subsec_id=25392&art_id=6387026)