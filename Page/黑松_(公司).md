**黑松股份有限公司**（簡稱：**黑松**，，），是[臺灣一家知名的](../Page/台灣.md "wikilink")[飲料生產商](../Page/飲料.md "wikilink")，以家族企業的形式由張文杞、張有盛等人創立於1925年。該公司初名為**進馨商會**。

## 歷史

黑松公司創辦人[張文杞](../Page/張文杞.md "wikilink")，在家中排行第三，其父親務農。張文杞曾在菸草工廠擔任機械修理工人，20歲時辭職回家，改而從商，在今[臺北市](../Page/臺北市.md "wikilink")[鄭州路附近販賣過](../Page/鄭州路.md "wikilink")[木炭及水糕](../Page/木炭.md "wikilink")。1924年，張文杞店面附近一間製造[彈珠汽水的](../Page/彈珠汽水.md "wikilink")「尼可尼可商會」（）有意出售，張文杞與其弟[張有盛與其他五名](../Page/張有盛.md "wikilink")[堂兄弟合資頂下這間工廠](../Page/堂兄弟.md "wikilink")。1925年（[大正](../Page/大正.md "wikilink")14年）4月14日，進馨商會成立，工廠設於臺北市[長安西路](../Page/長安西路.md "wikilink")，負責人為張文杞和張有盛。
[缩略图](https://zh.wikipedia.org/wiki/File:西門町走一圈_-_panoramio_-_Tianmu_peter_\(227\).jpg "fig:缩略图")
進馨商會創立時，產品為「富士牌」、「富貴牌」及「三手牌」的彈珠汽水。1931年，[黑松汽水上市](../Page/黑松汽水.md "wikilink")，進馨商會開始使用「黑松」[商標](../Page/商標.md "wikilink")。1936年，進馨商會更名為「進馨飲料合名會社」，「黑松」商標在[日本註冊](../Page/日本.md "wikilink")。1937年，位於臺北市[中崙的進馨商會臺北廠完工](../Page/中崙_\(臺北市\).md "wikilink")。

1938年，進馨飲料被[臺灣總督府強迫併入](../Page/臺灣總督府.md "wikilink")「臺灣清涼飲料水統-{制}-組合」，被編為「第八工廠」，喪失經營自主權。

1946年，日本殖民統治結束，臺灣清涼飲料水統-{制}-組合解散。1946年7月26日，進馨飲料恢復營業。1946年12月，進馨飲料改組為「進馨汽水有限公司」。1950年，[黑松沙士上市](../Page/黑松沙士.md "wikilink")。1958年，進馨汽水開始使用懸掛於各[商店門口的黑松](../Page/商店.md "wikilink")[琺瑯](../Page/琺瑯.md "wikilink")[招牌](../Page/招牌.md "wikilink")。1968年，因應[可口可樂進入臺灣市場](../Page/可口可樂.md "wikilink")，進馨汽水推出[黑松可樂](../Page/黑松可樂.md "wikilink")，雖佔可樂市場三成市佔率但黑松可樂最終因銷售不佳而停售。1976年，為對抗[蘋果西打](../Page/蘋果西打.md "wikilink")，進馨汽水推出[吉利果汽水](../Page/吉利果汽水.md "wikilink")。

1970年1月1日，因黑松汽水、黑松沙士等品牌頗受歡迎，進馨汽水改組為「黑松飲料股份有限公司」。1981年，黑松飲料更名為「黑松股份有限公司」至今。

1981年，有消費者飲用吉利果汽水後發生[食物中毒](../Page/食物中毒.md "wikilink")，影響了黑松公司的聲譽。1985年，黑松沙士被檢驗出含有可能致癌的[黃樟素](../Page/黃樟素.md "wikilink")，嚴重打擊了黑松沙士的銷售。黑松公司雖然快速將黑松沙士下架，並推出黑松沙士新配方，但銷售量及市場佔有率仍受到影響。為挽回市場，黑松公司推出[歐香咖啡](../Page/歐香咖啡.md "wikilink")，聘請[葉璦菱擔任廣告代言人](../Page/葉璦菱.md "wikilink")。

[HeySong_Chungli_Plant_20131028.jpg](https://zh.wikipedia.org/wiki/File:HeySong_Chungli_Plant_20131028.jpg "fig:HeySong_Chungli_Plant_20131028.jpg")
1987年5月，黑松公司總部由臺北廠遷移至臺北市[大安區信義路四段](../Page/大安區_\(臺北市\).md "wikilink")「黑-{松}-通商大樓」。黑松公司在[桃園縣](../Page/桃園縣.md "wikilink")[中壢市](../Page/中壢市.md "wikilink")([桃園市](../Page/桃園市.md "wikilink")[中壢區](../Page/中壢區.md "wikilink"))[中壢工業區及](../Page/中壢工業區.md "wikilink")[雲林縣](../Page/雲林縣.md "wikilink")[斗六市有設廠](../Page/斗六市.md "wikilink")。

1994年，[韋恩咖啡](../Page/韋恩咖啡.md "wikilink")（Wincafe）上市。

1999年3月12日，黑松公司[股票上市](../Page/股票上市.md "wikilink")（）。此後，黑松公司並擴展到[百貨業](../Page/百貨.md "wikilink")，利用臺北廠（已廢廠）土地轉型成[微風廣場](../Page/微風廣場.md "wikilink")，為臺北市知名百貨公司。

2001年設立「黑松世界」在[微風廣場](../Page/微風廣場.md "wikilink")2樓。

## 商標起源

1931年註冊為黑松。標誌中的黑色勁松是[日本皇室最愛的](../Page/日本皇室.md "wikilink")「珍貴植物」。早期的黑松標誌，還有紅色八角菱鏡圍繞，更顯貴氣。實物在[新竹神社](../Page/新竹神社.md "wikilink")。

## 產品

  - [黑松沙士](../Page/黑松沙士.md "wikilink")
  - [黑松汽水](../Page/黑松汽水.md "wikilink")
  - [黑松可樂](../Page/黑松可樂.md "wikilink")
  - [Hi-plus](../Page/Hi-plus.md "wikilink")
  - [韋恩咖啡](../Page/韋恩咖啡.md "wikilink")
  - [黑松茶花](../Page/黑松茶花.md "wikilink")
  - [好茶哉](../Page/好茶哉.md "wikilink")
  - 黑松茶˳尋味
  - [黑松茶](../Page/黑松茶.md "wikilink")
  - [綠洲果汁](../Page/綠洲果汁.md "wikilink")
  - [黑松果汁](../Page/黑松果汁.md "wikilink")

## 參考資料

## 外部連結

  - [黑松HeySong](http://www.heysong.com.tw/)

<https://www.storm.mg/lifestyle/308686>

[H](../Category/台灣品牌.md "wikilink")
[H](../Category/台灣食品公司.md "wikilink")
[H](../Category/台灣飲料公司.md "wikilink")
[H](../Category/1925年台灣建立.md "wikilink")
[H](../Category/1925年成立的公司.md "wikilink")
[H](../Category/總部位於臺北市大安區的工商業機構.md "wikilink")