[唐元.jpg](https://zh.wikipedia.org/wiki/File:唐元.jpg "fig:唐元.jpg")
**唐元**（），[字](../Page/表字.md "wikilink")**长孺**，[号](../Page/号.md "wikilink")**敬堂**，学者称**筠轩先生**。元朝[江浙行省](../Page/江浙行省.md "wikilink")[徽州路](../Page/徽州路.md "wikilink")[歙县](../Page/歙县.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")[黄山市](../Page/黄山市.md "wikilink")[歙县](../Page/歙县.md "wikilink")）人。元朝著名学者、理学家、文学家，仕至[徽州路儒学教授](../Page/徽州路.md "wikilink")。他是徽州**唐氏三先生**之一，也是[明朝开国谋士](../Page/明朝.md "wikilink")[朱升的老师](../Page/朱升.md "wikilink")。卒后入乡贤词祭祀。\[1\]

## 生平与交游

唐元既出诗书之家，自幼受[程朱理学熏陶](../Page/程朱理学.md "wikilink")。少时与[洪焱祖](../Page/洪焱祖.md "wikilink")、[俞魏卿为笔砚交](../Page/俞魏卿.md "wikilink")，里人并称“**新安三俊**”。三人“徜徉山水间，洗濯磨淬，婆娑嬉游，未始不孳孳以蒞学为务。戏谑亵狎，不一出诸口。”蒙元灭宋后，[科举废止达三十余年](../Page/科举.md "wikilink")，江南文人多成游士。三人在干谒名公硕儒之余，恒心嗜学，闻名乡里。时徽州路总管孟淳初见唐元，十分器重，赠诗曰：“新安三俊子其一，气貌清腴文字工”。因为早年“不识治生理”，唐元“屡阨衣食”。三十六岁，以所作诗五十四篇投谒被誉为“江西诗派殿军”的[方回](../Page/方回.md "wikilink")，方回欣然命其集为《艺圃小集》，并在序文中称赞其诗“所以可人意者，格高也……近人之学[许浑](../Page/许浑.md "wikilink")、[姚合者](../Page/姚合.md "wikilink")，长孺扫之如秕糠，而以陶、杜、黄、陈为师者也”。此后，唐元“梦寐间不敢忘先生之训”，“奋迅劘切以诗自鸣”。延佑二年（1315），元廷恢复科举。消息传到徽州，四十六岁的唐元一度“萤窗雪案”，学习应试文章，却四试有司不利，于是愤弃举子业。元廷科举规模不大，对汉人、南人又存有歧视，这使得当时大量文人转而从事吏事。当时，虽有数举不第而恩授教授、学正和山长之例，但对汉人、南人也存在年龄上的限制。唐元最初训导乡校，生活较为清苦。五十八岁时，得[江浙行省授](../Page/江浙行省.md "wikilink")[平江路儒学学录之职](../Page/平江路.md "wikilink")，在职兴文教，交游东南名士。六十五岁，迁[建德路](../Page/建德路.md "wikilink")[分水县儒学教谕](../Page/分水县.md "wikilink")。六十八岁，任[集庆路](../Page/集庆路.md "wikilink")[南轩书院山长](../Page/南轩书院.md "wikilink")，声名已盛于金陵。寻以[徽州路儒学教授致仕](../Page/徽州路.md "wikilink")，退居[歙县乌聊山下](../Page/歙县.md "wikilink")，“益以文章自任”，名重东南，被誉为“**东南学者师**”。“每侯伯下车，必先请见。四方大夫士子过其境者，必询其起居，即其庐而礼焉。”
上门求文者也是络绎不绝，填塞阡陌。

唐元自谓“晚始知学，而圣言浩若烟海，常惧精力就衰，卒未有成”，“恨平生未见之书尚多，吾岂甘衰老而自弃哉？”因此，他好学老而不倦，晚年“须眉纷白，神采粹温。老于其乡，一人而已”。至正九年以病卒，得年八十有一。临终嘱薄葬，又作“视死如归，无忧无辱。海水还源，本来面目”十六字诗，端坐而逝。其自作像赞曰：“生不忮物，晚而劬书。庶几谨畏，遗体无污”，可以说是对其一生的写照。

唐元平生交游广泛。仅《筠轩集》诗文所及者，即有：[方回](../Page/方回.md "wikilink")、[虞集](../Page/虞集.md "wikilink")、[曹泾](../Page/曹泾.md "wikilink")、[龚璛](../Page/龚璛.md "wikilink")、[张起岩](../Page/张起岩.md "wikilink")、[王士熙](../Page/王士熙.md "wikilink")、[吴师道](../Page/吴师道.md "wikilink")、[杨刚中](../Page/杨刚中.md "wikilink")、[贡师泰](../Page/贡师泰.md "wikilink")、[李桓](../Page/李桓.md "wikilink")、[杜本](../Page/杜本.md "wikilink")、[郑元佑](../Page/郑元佑.md "wikilink")、[汪巽元](../Page/汪巽元.md "wikilink")、[陈栎](../Page/陈栎.md "wikilink")、[汤炳龙](../Page/汤炳龙.md "wikilink")、[杨敬惪](../Page/杨敬惪.md "wikilink")、[洪焱祖](../Page/洪焱祖.md "wikilink")、[俞魏卿](../Page/俞魏卿.md "wikilink")、[郑奕夫](../Page/郑奕夫.md "wikilink")、[郑玉](../Page/郑玉.md "wikilink")、[程文](../Page/程文.md "wikilink")、[危素](../Page/危素.md "wikilink")、[毕祈凤](../Page/毕祈凤.md "wikilink")、[牟应复](../Page/牟应复.md "wikilink")、[马昂夫](../Page/马昂夫.md "wikilink")（薛昂夫）、[郭麟孙](../Page/郭麟孙.md "wikilink")、[夏溥](../Page/夏溥.md "wikilink")、[刘致](../Page/刘致.md "wikilink")、[盛则轩](../Page/盛则轩.md "wikilink")、[程益](../Page/程益.md "wikilink")、[陈方](../Page/陈方.md "wikilink")、[艾庭晖](../Page/艾庭晖.md "wikilink")、[俞肇](../Page/俞肇.md "wikilink")、[鲜于去矜](../Page/鲜于去矜.md "wikilink")、[蒋师文](../Page/蒋师文.md "wikilink")、[江光启](../Page/江光启.md "wikilink")、[黄志斋](../Page/黄志斋.md "wikilink")、[朱克用](../Page/朱克用.md "wikilink")、[朱文选](../Page/朱文选.md "wikilink")、[赵孟威](../Page/赵孟威.md "wikilink")、[夏希贤](../Page/夏希贤.md "wikilink")、[夏泰亨](../Page/夏泰亨.md "wikilink")、[徐舫](../Page/徐舫.md "wikilink")、[胡初翁](../Page/胡初翁.md "wikilink")、[孟淳](../Page/孟淳.md "wikilink")、[卢挚](../Page/卢挚.md "wikilink")、[于泰来](../Page/于泰来.md "wikilink")、[孙国瑞](../Page/孙国瑞.md "wikilink")、[庄蒙](../Page/庄蒙.md "wikilink")、[吕广文](../Page/吕广文.md "wikilink")、[汪逢辰](../Page/汪逢辰.md "wikilink")、[尤拔](../Page/尤拔.md "wikilink")、[吴彬](../Page/吴彬.md "wikilink")、[汪德玉](../Page/汪德玉.md "wikilink")、[程国宝](../Page/程国宝.md "wikilink")、[鲍元康](../Page/鲍元康.md "wikilink")、[汪幼凤](../Page/汪幼凤.md "wikilink")、[周彦明](../Page/周彦明.md "wikilink")、[鲍椿](../Page/鲍椿.md "wikilink")、[许洪寿](../Page/许洪寿.md "wikilink")、[程植](../Page/程植.md "wikilink")、[孙岩等](../Page/孙岩.md "wikilink")。这些人大多是当时身处东南一带的名士，以及诸路儒学教官或书院山长。唐元的交游情况，一定程度上反映出元代士人在干谒交际和从事教业上的真实状况，对考证相关人物的生平也有一定的价值。
唐元生平所著，计有《敬堂杂著》、《思乐杂著》、《吴门杂著》、《分阳杂著》、《金陵杂著》和《老学藂稿》凡“三千余篇”，以及理学著作《易传义大意》十卷、《见闻录》二十帙，可谓著作等身。今仅存《筠轩集》诗文十三卷，有《唐氏三先生集》本和清《[四库全书](../Page/四库全书.md "wikilink")》本。从版本上看，前者即后者的底本。《唐氏三先生集》的序跋和附录中，保留了大量有关唐氏三先生的文献，是今人研究三先生和元明徽州文学的重要材料。

## 理学

元代是新安理学迅速发展的时期。当时徽州士人视程朱之学为家邦之学，又都重视家学渊源，因此几乎无人不浸染理学。唐元幼承过庭之训，私淑朱熹，尤精于《易》。他认为，“《易》至朱程无余蕴矣”，但是“二家多不及象”；“儒先於一爻二爻间有总论”，然而“六爻每无总论”。因此，悉数考证陈备，凡“互有不同，则疏于卦末”，成《易传义大意》十卷\[3\]。此书今已不见传本，清朱彝尊《经义考》卷四十四即将其列为佚书。

唐元平生喜竹，谓其“霜凌雪厉，独正不惧，即吾志操之坚贞也”。他在《董氏存诚堂记》中以“乾之九二”论诚心之道，在《徽州路重建谯楼记》中以山水卦形记建楼始终；在《山林读书所记》中以“物得其所”引出“一心在理”、“持敬”修诚之论，在《一斋记》中由“天一”之数推及人伦终始、修心敬静之说；其《歙县儒学修造记》以“天下惟理最大”开篇，《徐至刚字说》以“义”制刚直收尾……皆持论中正，不一而足。这种“正色坐紫阳”、不为异说的态度，是当时从事教职的教官和山长们所共有的。唐元晚年著《见闻录》二十余秩，“理学渊源，名物巨细，事无不考，问无不知”
。虽然此书亦已散佚，但从书名可以看出，唐元当是重视以主观经验和客观名物来阐述理学的，这自然是难能可贵的正途。至于探究“理学渊源”、“事无不考”，从治《易》的角度看，不由令人想起清人[王夫之的易学](../Page/王夫之.md "wikilink")。《四库总目》称唐元“于经术颇深，其议论亦不诡于正”。可惜其理学著作今皆亡佚，已难深入探究了。

## 文章

唐元是以[程朱理学出身而治文的](../Page/程朱理学.md "wikilink")。在经历了科举的失利后，他转而攻古文，“紬绎经史百家，沉潜韩柳欧曾，于鹤山爱其博洽，自以为临邛衣钵”。每每“五更孤枕，潜思密运，不竢笔札，以腹为稿”，于是“文从字顺，滔滔汩汩”
。唐元以“临邛衣钵”自居，道学文章皆以[魏了翁为师](../Page/魏了翁.md "wikilink")。魏了翁之文醇正有法，且立意高远、思想深刻；语言流畅，而纡徐曲折、出乎自然。今观唐元《筠轩集》中《金陵祭杨待制文》写得高古整齐，《贽见梦臣张侍御书》则纡徐而典雅，其他序记铭跋也都议论平实而富有文采，尤可谓得鹤山真传。此外，其《太原王才塑工赞》描摹生动传神，《四库总目》谓其“亦可以补史所未备”；而《跋李伯时摹刘商观弈图》和《舟喻示儿桂芳》都写得短小凝练，后者全文不足160字：

唐元白天所乘小舟，因船家贪利多载，以致舱内污浊，旅客叫苦不迭。夜晚所登大船，人少舒适，因而坐卧甚安。这是生活中的一件小事，唐元却从中看出了“善用大者不知其为大，而器小者自不可掩也”的事实，并悟出了“务学”应当“深藏不市而恢乎有容”的道理。《易·序卦》云：“有大而能谦，必豫。”唐元以舟所喻的道理，正是君子的重要品德。文章前半部分叙事，在描述了小舟的狭窄肮脏后，仅用“深房高榻，枕簟悉安”八字便衬托了大船的宽敞惬意。后半部分抓住“大”和“小”的不同表现，点到即止。在说理的方法上，因事生教，寓理于物，完全不是空洞呆板的说教。最后以“且将以自箴”结尾，使得父子处于平等地位，更显谨慎自然，可以说一篇是难得一见的说理文章。

## 诗歌

唐元晚年在《艺圃后稿自序》中回忆谒诗方回一事时说：“遂闻古今作者格有高卑之异，知其说当自得之心，然博读静思其梯级也……窃谓自得于心者，无法之法；博读精思者，有法之法。”其《艾幼清汝东樵唱诗跋》又云：“昔人有言，读书万卷而不用于诗，畏其义博而辞溢也。夫诗有别材，本于性情，触物而发。故曰：‘言之精者为文，文之精者为诗。’然观少陵言‘读书破万卷，下笔如有神’，益知学诗人腑肺，非得古今灌溉，理义融会，则如貌枯语涩，于善养生人不类。”唐元悟出诗格有高卑、诗情需学问、理义灌溉的道理，遂注重以精思治学入诗文，以品格治心求道理。其《读子敬龚先生江东小稿》云：“谢韦向上无圭角，濂洛方来以道鸣”、“坡公门下无多客，工部毫端有万牛”、“五更孤枕六旬客，二句三年双泪流”，意识到东坡之诗难学，杜甫笔力则更是雄浑，向他们学习诗法，不仅需要博学精思，更需要苦吟精进。他的这种“兼学唐宋”而有所侧重的观点，在一定程度上避免了当时部分文人“宗唐”却沿袭了中晚唐纤弱之风的弊病。杨刚中以“诗思腾涌,如万斛泉不择地而出”来称赞唐元的诗\[3\]，而作诗达到诗思腾涌而又冲澹雄厚的境界，无疑是需要苦寒磨练的。

[唐元书法.jpg](https://zh.wikipedia.org/wiki/File:唐元书法.jpg "fig:唐元书法.jpg")[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")。\]\]

## 评价

《四库总目》：“盖其始终当元盛时，故所作多和平温厚之音，极为虞集所推许。又尝着《易大义》、《见闻録》诸书，于经术颇深，其议论亦不诡于正。”

[唐桂芳](../Page/唐桂芳.md "wikilink")：“紬绎经史百家，沉潜韩柳欧曾，于鹤山爱其博洽，自以为临邛衣钵。”

[方回](../Page/方回.md "wikilink")：“诗以格高为第一……（唐元诗）所以可人意者，格髙也。何以谓之格高？近人之学许浑、姚合者，长孺扫之如粃糠，而以陶、杜、黄、陈为师者也。”

[虞集](../Page/虞集.md "wikilink")：“近覩《郑夫人行实》，佳甚。吾恨不识唐公之为人。”

[朱升挽诗](../Page/朱升.md "wikilink")：“紫阳山下小柴门，每忆童年拜隐君。乔岳岩岩瞻寿相，长江浩浩读雄文。衮衣不独缘稽古，宦学何时见策勋。千载藤溪风月在,直须骥子表高坟。”

[贡师泰](../Page/贡师泰.md "wikilink")：“公文字有法度，诸人不可及。”

[夏漙](../Page/夏漙.md "wikilink")：“唐君之文高古整齐，令人读之不休。邻邦何幸，有此手笔！”

[杨刚中](../Page/杨刚中.md "wikilink")：“诗思腾涌，如万斛泉不择地而出。”

[杜本](../Page/杜本.md "wikilink")：“公之诗文霶霈敷腴，不事险涩。诗慕陶杜黄陈，文入欧曾而卒于临卭。”

[程敏政](../Page/程敏政.md "wikilink")“筠轩之文纡徐而典雅，有汴宋前辈之风。故元名公张起岩、王士熙、吴师道诸君子皆盛称之。诗则含蓄而隽永，不作近代人语。虚谷方公为之序，美其格髙，世以为知言。”

[舒頔](../Page/舒頔.md "wikilink")：“文章学问为时所宗……所谓充然浑然者，当不在汉唐下。诗尤高，丰缛清润，有台阁风。”

[朱文选](../Page/朱文选.md "wikilink")：“先生之为诗，盘折老硬，无纤巧态。为文和平霶霈，无险涩语。沉浸乎礼义之中，优游乎古今之际。”

[朱同](../Page/朱同.md "wikilink")：“诗文滂沛敷腴，不事险涩。”

[孟淳](../Page/孟淳.md "wikilink")：“新安三俊子其一，气貌清腴文字工。”

[王士熙](../Page/王士熙.md "wikilink")：“乘槎深夜问支机，天女流梭舞凤飞。回首人间金粟尺，剪灯愁制五铢衣。”

[陈浩](../Page/陈浩.md "wikilink")：“筠轩先生以文章行义为士轨式。”

[王达](../Page/王达.md "wikilink")：“予阅筠轩先生长孺唐公诗文，辞理条畅，不假雕镂，浩瀚滂沛，浑然天成。得临邛衣钵，有宋季诸儒之气象。”

[翁方纲](../Page/翁方纲.md "wikilink")：“新安为朱子之乡，故其（唐元）议论绪言颇津逮朱门。在元人著作中颇为近正。”

歙县民间谚语：“凡入城府，不之东郭见潜夫（洪焱祖），则之南门见长孺（唐元）。”

## 参考文献

{{-}}

[Category:元朝诗人](../Category/元朝诗人.md "wikilink")
[Category:歙县人](../Category/歙县人.md "wikilink")
[Category:1269年出生](../Category/1269年出生.md "wikilink")
[Category:1349年去世](../Category/1349年去世.md "wikilink")
[Category:元朝书法家](../Category/元朝书法家.md "wikilink")
[Category:元朝理学家](../Category/元朝理学家.md "wikilink")
[Category:中国思想家](../Category/中国思想家.md "wikilink")
[G](../Category/唐姓.md "wikilink")

1.  唐宸《元代新安理学家唐元考论》，《黄山学院学报》2012年第4期。