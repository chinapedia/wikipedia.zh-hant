[Strieła-1_NTW_9_93.jpg](https://zh.wikipedia.org/wiki/File:Strieła-1_NTW_9_93.jpg "fig:Strieła-1_NTW_9_93.jpg")
**9K31 "箭（Strela）-1"**（俄语：9К31 "Стрела-1" ,
[北約代號](../Page/北約代號.md "wikilink")：**SA-9
"灯笼裤(Gaskin)"**）是一种高机动低空短程红外寻的地对空导弹系统。车载平台为BRDM-2两栖轮式装甲车。每辆发射车在炮塔两侧各配有一组双联9M31导弹，导弹密封包装。

## 导弹与制导

由于使用第一代1-3μm非制冷硫化铅红外导引头，当导弹用于尾追时实际射程更大，至少有11千米。
对头攻击时有效射程为性能规格里所述那样。鸭翼与尾翼可动以调整飞行轨迹。导弹发射仰角20-80度，跟踪角速度15-20度／秒，

重新装填导弹手工进行，需时5分钟。关闭舱门时驾驶员与车长用潜望镜对外观察。

## 车辆

除了新的炮塔外，发射车与BRDM-2的主要区别是取出了车腹部的车轮（本来是用于改善越野性能的）。驾驶员与车长有红外夜视系统。有核生化超压防护系统。行使时导弹向炮塔下折叠以减低车高。发动机功率104千瓦，有轮胎压力中央控制系统。

载车最大公路行驶速度90KM/H，最大水上行驶速度8-9KM/H。

## 型号发展

9K31于1968年列装。

1970年研制出改进型号**9K31M(SA-9B
"Gaskin-Mod0")**系统。有效射程扩展到560-8000m，有效射高10-6100m。尾追攻击时射高11,000米。红外导引头加装了制冷系统，探测范围扩展到1-5μm。

## 战斗使用

每个导弹连包括一辆指挥车，一连安装雷达新型号被动探测系统的发射车、三辆普通发射车。雷达信号被动探测系统是9S16“扁盒子flat
box”，由安装在载车四周的四根探测天线组成，实现360度全向覆盖，提供飞机临近告警可以辅助光学观瞄设备截获锁定飞机。

典型战法是对每个目标发射两枚导弹以提高摧毁概率。使用中炮手必须不停旋转炮塔以搜索敌机，这使得9K31的作战效能很差。

俄罗斯军队的9K31系统被已被**[9K35 "箭-10"（SA-13
"金花鼠"）](../Page/SA-13.md "wikilink")**取代。

## 部署

[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")，[安哥拉](../Page/安哥拉.md "wikilink")，[贝宁](../Page/贝宁.md "wikilink")，[保加利亚](../Page/保加利亚.md "wikilink")，[古巴](../Page/古巴.md "wikilink")，[克罗地亚](../Page/克罗地亚.md "wikilink")，[捷克](../Page/捷克.md "wikilink")，[埃及](../Page/埃及.md "wikilink")，[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")，
[朝鲜](../Page/朝鲜.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")，[印度](../Page/印度.md "wikilink")，[伊拉克](../Page/伊拉克.md "wikilink")，[利比亚](../Page/利比亚.md "wikilink")，[毛里塔尼亚](../Page/毛里塔尼亚.md "wikilink")，[莫桑比克](../Page/莫桑比克.md "wikilink")，[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")，[波兰](../Page/波兰.md "wikilink")，[罗马尼亚](../Page/罗马尼亚.md "wikilink")，[斯洛伐克](../Page/斯洛伐克.md "wikilink")，[叙利亚](../Page/叙利亚.md "wikilink")，[乌干达](../Page/乌干达.md "wikilink")，[乌克兰](../Page/乌克兰.md "wikilink")，[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")，[越南](../Page/越南.md "wikilink")，[也门与](../Page/也门.md "wikilink")[前南斯拉夫](../Page/前南斯拉夫.md "wikilink")

9K31参加了1981年叙利亚与以色列在黎巴嫩的冲突，以及[两伊战争](../Page/两伊战争.md "wikilink")、[海湾战争](../Page/海湾战争.md "wikilink")、[安哥拉战乱](../Page/安哥拉.md "wikilink")、北约对[前南斯拉夫的军事行动](../Page/前南斯拉夫.md "wikilink")，以及[伊拉克战争](../Page/伊拉克战争.md "wikilink")。

1983年11月，叙利亚的一个9K31导弹连击落了一架美国海军的[A-6](../Page/A-6.md "wikilink")"入侵者"飞机。

[en:SA-9 Gaskin](../Page/en:SA-9_Gaskin.md "wikilink")

[Category:蘇聯面對空飛彈](../Category/蘇聯面對空飛彈.md "wikilink")
[Category:點防禦防空飛彈](../Category/點防禦防空飛彈.md "wikilink")
[Category:俄羅斯飛彈](../Category/俄羅斯飛彈.md "wikilink")