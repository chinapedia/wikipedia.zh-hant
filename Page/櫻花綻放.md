《**櫻花綻放**（）》是[日本](../Page/日本.md "wikilink")[聲優兼](../Page/聲優.md "wikilink")[歌手](../Page/歌手.md "wikilink")[林原惠的第](../Page/林原惠.md "wikilink")24張[單曲](../Page/單曲.md "wikilink")，2000年5月24日發售。發行公司為[King
Records](../Page/King_Records.md "wikilink")。

## 概要

  - 由於兩首曲目都是[電視動畫](../Page/電視動畫.md "wikilink")《[純情房東俏房客](../Page/純情房東俏房客.md "wikilink")》的主題曲，因此受到該作品愛好者的廣泛支持，累計發行數量突破10萬，初次登上[Oricon排行榜時就排在第](../Page/Oricon.md "wikilink")7位（共有7次上榜）。
  - 不過，這已經是以「林原惠」名義出版的第10張排名進入前十的單曲。
  - 由詞曲原作者[岡崎律子自我翻唱的](../Page/岡崎律子.md "wikilink")『櫻花綻放』和『如果有你』收錄在岡崎的[專輯](../Page/專輯.md "wikilink")「LOVE
    HINA OKAZAKI COLLECTION」裡。
  - 另外還有[堀江由衣](../Page/堀江由衣.md "wikilink")（純情房東俏房客女主角**成瀨川奈留**的聲優）的翻唱版本，收錄在堀江由衣的[精選專輯](../Page/精選專輯.md "wikilink")「[ほっ?](../Page/ほっ?.md "wikilink")」裡。

## 内容

1.  **櫻花綻放**（）
      - 作詞・作曲：[岡崎律子](../Page/岡崎律子.md "wikilink")、編曲：[十川知司](../Page/十川知司.md "wikilink")
      - 演奏時間 - 3:12
      - [電視動畫](../Page/電視動畫.md "wikilink")《[純情房東俏房客](../Page/純情房東俏房客.md "wikilink")》片頭主題歌
2.  **如果有你**（）
      - 作詞・作曲：岡崎律子、編曲：十川知司
      - 演奏時間 - 4:15
      - 電視動畫《純情房東俏房客》片尾主題歌
3.  **櫻花綻放**（OFF VOCAL VERSION）
4.  **如果有你**（OFF VOCAL VERSION）

## 收錄專輯

  - **櫻花綻放**
      -
        「[feel well](../Page/feel_well.md "wikilink")」
  - **如果有你**
      -
        「[center color](../Page/center_color.md "wikilink")」

[Category:東京電視台動畫主題曲](../Category/東京電視台動畫主題曲.md "wikilink")
[Category:2000年單曲](../Category/2000年單曲.md "wikilink")
[Category:純情房東俏房客](../Category/純情房東俏房客.md "wikilink")
[Category:林原惠單曲](../Category/林原惠單曲.md "wikilink")
[Category:櫻花題材歌曲](../Category/櫻花題材歌曲.md "wikilink")