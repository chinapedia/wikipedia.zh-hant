**民航公司10號班機**曾是[台灣](../Page/台灣.md "wikilink")[民航空運公司從](../Page/民航空運公司.md "wikilink")[香港啟德國際機場](../Page/啟德機場.md "wikilink")（現已關閉）至[台灣](../Page/台灣.md "wikilink")[台北松山機場的定期班機](../Page/台北松山機場.md "wikilink")，航班編號CT-010。於1968年2月16日，由被稱為「超級翠華號」的B-1018[波音](../Page/波音.md "wikilink")[727-92C執行此航班](../Page/波音727.md "wikilink")\[1\]，墜毀於臺北縣林口鄉（今[新北市](../Page/新北市.md "wikilink")[林口區](../Page/林口區.md "wikilink")）\[2\]，共造成21人死亡，42人受傷。

## 經過

當天晚上，「超級翠華號」從香港返回[台北松山機場](../Page/台北.md "wikilink")。台北[近場管制許可客機以](../Page/航空交通管制.md "wikilink")[儀表著陸系統直接進場](../Page/儀表著陸系統.md "wikilink")，隨即將10號班機交給松山[塔台](../Page/塔台.md "wikilink")，飛機航向、航跡均正常；天氣狀況亦良好。可是，[機長Stuart](../Page/機長.md "wikilink")
E.
Dew突然發覺飛機進場高度過低，立即將[節流閥拉起企圖](../Page/節流閥.md "wikilink")[重飛](../Page/重飛.md "wikilink")。其後[黑盒子的通話記錄器亦記錄了機長在飛機觸地剎那驚叫](../Page/黑盒子.md "wikilink")：「Go
to hell！」飛機隨即觸及地面的樹木及房屋，並起火燃燒。

駐松山機場的[消防隊及](../Page/消防隊.md "wikilink")[駐台美軍立即展開搜救](../Page/駐台美軍.md "wikilink")，在火海及泥濘中拯救傷者。最終，機上63人中，21人死亡。

## 調查

3月4日，[交通部民用航空局的調查小組公佈調查報告](../Page/交通部民用航空局.md "wikilink")，指出事件主因乃機員失誤。因為：

  - 機員在發現飛機高度過低時，即時加速離地，顯示當時機件正常；
  - 當時的天氣狀況良好；
  - 空難前後2小時，先後有6架飛機進場並正常著陸，證明機場之降落系統正常運作；
  - 機員跟[航空交通管制員的通話亦正常](../Page/航空交通管制.md "wikilink")。

## 事故之後

肇事的727客機是由民航空運公司向美國租用的，是當時民航空運公司機隊中唯一的噴射客機；該727客機亦是民航空運公司唯一飛行國際線的航機\[3\]。當空難發生後，民航空運公司的國際航線再沒有復航，其在台灣的地位亦被[中華航空所取代](../Page/中華航空.md "wikilink")。直至1975年，民航空運公司亦遭清盤結業。

30年後（1998年）的同一天，[中華航空676號班機由](../Page/中華航空676號班機.md "wikilink")[印尼](../Page/印尼.md "wikilink")[峇里飛返台灣](../Page/峇里.md "wikilink")，於[台灣](../Page/台灣.md "wikilink")[桃園縣](../Page/桃園市.md "wikilink")[大園鄉](../Page/大園區.md "wikilink")（今桃園市大園區）墜毀，202人死亡。

## 其他

由於當年航空交通管制的[雷達未能顯示飛機的飛行高度](../Page/雷達.md "wikilink")，因此未能了解為何10號班機為何該飛機的高度會無故降低。直至1972年12月29日，[東方航空401號班機於](../Page/東方航空401號班機.md "wikilink")[美國](../Page/美國.md "wikilink")[邁阿密失事](../Page/邁阿密.md "wikilink")，美國當局才開始引進改良過的雷達，顯示航班的飛行高度資料。

## 參考文獻

## 外部連結

  - [AirDisaster.Com對此事件的紀錄](http://www.airdisaster.com/cgi-bin/view_details.cgi?date=02161968&reg=B-1018&airline=Civil+Air+Transport)

[分類:1968年台灣](../Page/分類:1968年台灣.md "wikilink")

[Category:機員失誤造成的航空事故](../Category/機員失誤造成的航空事故.md "wikilink")
[Category:1968年航空事故](../Category/1968年航空事故.md "wikilink")
[Category:可控飛行撞地](../Category/可控飛行撞地.md "wikilink")
[Category:新北市歷史](../Category/新北市歷史.md "wikilink")
[Category:波音727航空事故](../Category/波音727航空事故.md "wikilink")

1.
2.
3.