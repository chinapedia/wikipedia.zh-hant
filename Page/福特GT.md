[Warwick_(Rhode_Island,_USA),_Ford_GT_--_2006_--_1.jpg](https://zh.wikipedia.org/wiki/File:Warwick_\(Rhode_Island,_USA\),_Ford_GT_--_2006_--_1.jpg "fig:Warwick_(Rhode_Island,_USA),_Ford_GT_--_2006_--_1.jpg")
[GT40_at_Goodwood.jpg](https://zh.wikipedia.org/wiki/File:GT40_at_Goodwood.jpg "fig:GT40_at_Goodwood.jpg")

**福特GT**（Ford
GT）是[福特汽車旗下的一個](../Page/福特汽車.md "wikilink")[超級跑車系列](../Page/超級跑車.md "wikilink")，其開發的起源可以回溯至2002年時，原本是在車展中展出以作為車廠創廠百年紀念的GT40[概念車](../Page/概念車.md "wikilink")，但因意外獲得熱烈迴響因此車廠決定將之付諸量產，並於2004年至2007年間，限量生產了4千餘輛量產版車型。GT是以福特在1960年代稱霸[利曼24小時耐久賽賽場的經典賽車](../Page/利曼24小時耐久賽.md "wikilink")[GT40為樣本](../Page/福特GT40.md "wikilink")，保持古典的車身造型與車身比例但換用現代化的機械結構所設計出來的車款，因此在概念車版本時同樣也命名為**GT40**，但在實際量產上市時，將車名簡化為**GT**。

在第一代的福特GT上市10年之後，福特於2015年的[底特律車展中公開第二代車型的上市消息](../Page/底特律車展.md "wikilink")。第二代車型將自2016年12月以2017的名義開始量產交車，以作為GT40在中奪冠的50週年誌慶。

## 第一代（2004-2007年）

首批量產版於2003年首度亮相，它們使用了5.4升的V8機械增壓引擎、擁有550匹的最大馬力（410 kW），最大扭力為678
Nm，其極速也高達322km/h。

福特於2004年春季開始大幅度生產GT跑車，預計在三年內，每年能生產1,500部，首批跑車已於於2004年9月交付予客戶。跑車的噴漆工序於（Saleen）的廠房進行，而引擎、變速箱及車內裝置則在福特位於（Wixom,
MI）的進行。在三年間生產的4,500部GT跑車，只有101部會出口運往[歐洲](../Page/歐洲.md "wikilink")，付運於2005年底開始。但在2007年之后，因为此廠房的关闭，逼使此車全面停產。

作為當代美國跑車的象徵性車款，福特GT經常被收錄在各類平台的賽車遊戲之中，曾經出現在包括《[GT賽車](../Page/GT賽車.md "wikilink")》（Gran
Turismo）系列、《[極速快感](../Page/極速快感系列.md "wikilink")》（Need for
Speed）與《[極限競速](../Page/極限競速系列.md "wikilink")》（Forza）等在內的賽車遊戲中，除此之外，在動作遊戲《[俠盜獵車手](../Page/俠盜獵車手系列.md "wikilink")》（Grand
Theft Auto）系列中，也存在一款名為「Bullet GT」、以福特GT為藍本所創作出的虛構車款。

值得一提，福特生產了兩部開篷版本的GT，名為GTX1，目前為福特唯一一款有開篷版本的福特GT。

## 第二代（2016年-2020年）

[2017_Ford_GT_front.JPG](https://zh.wikipedia.org/wiki/File:2017_Ford_GT_front.JPG "fig:2017_Ford_GT_front.JPG")中展出的2017年版福特GT預產原型車。\]\]
在2015年1月舉行的底特律車展中，福特汽車與[微軟舉辦了第二代福特GT與賽車遊戲](../Page/微軟.md "wikilink")《》（Forza
Motorsport
6）的聯合發表會，首次公開展示新車款的原型車並揭露相關規格數據，並公佈新車款將在2016年推出以作為GT40在1966年利曼大賽中奪得前三名的50週年紀念。第二代的GT採用了[碳纖維材質的](../Page/碳纖維.md "wikilink")車體，搭載一具3.5升的[EcoBoost](../Page/福特EcoBoost引擎.md "wikilink")[V6引擎](../Page/V6引擎.md "wikilink")，能輸出600匹[馬力](../Page/馬力.md "wikilink")。新車款將在2016年12月開始以2017年式的名義生產交車，預計生產至2020年。

## 參考文獻

## 外部連結

  - [官方網站](https://web.archive.org/web/20060118081310/http://www.fordvehicles.com/fordgt/)

[Category:2003年面世的汽車](../Category/2003年面世的汽車.md "wikilink")
[Category:福特車輛](../Category/福特車輛.md "wikilink")
[Category:超级跑車](../Category/超级跑車.md "wikilink")
[Category:雙座敞篷車](../Category/雙座敞篷車.md "wikilink")