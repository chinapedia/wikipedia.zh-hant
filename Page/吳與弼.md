**吳與弼**（），字子博，號康齋，江西[崇仁縣人](../Page/崇仁縣.md "wikilink")。

父親[吳溥](../Page/吳溥.md "wikilink")，[建文時為](../Page/建文.md "wikilink")[國子監司業](../Page/國子監.md "wikilink")。[靖難之變](../Page/靖難之變.md "wikilink")，[燕王](../Page/燕.md "wikilink")[朱棣攻入](../Page/朱棣.md "wikilink")[應天府時](../Page/應天府.md "wikilink")，[王敬止](../Page/王敬止.md "wikilink")、胡廣、[解缙](../Page/解缙.md "wikilink")、[吳溥四人聚會](../Page/吳溥.md "wikilink")，胡、解各有慷慨陳詞。獨敬止哭泣不言。吳與弼以為胡、解會[自殺](../Page/自殺.md "wikilink")[身殉](../Page/殉葬.md "wikilink")[建文帝](../Page/建文帝.md "wikilink")。[吳溥認為胡](../Page/吳溥.md "wikilink")、解只會講空話，真正忠君愛國的是[王敬止](../Page/王敬止.md "wikilink")。話還沒講完，卻聽到胡廣大聲對家人喊叫，「外面很喧鬧，小心看好豬啊。」吳溥笑著說：「連一隻[豬都捨不得](../Page/豬.md "wikilink")，難道捨得生命嗎？」不久，敬止[自鴆而死](../Page/自鴆.md "wikilink")，胡廣、[解缙迎附朱棣](../Page/解缙.md "wikilink")\[1\]。

與弼年十九歲時，見《伊洛淵源圖》，放棄[科舉](../Page/科舉.md "wikilink")，苦讀[閩學語錄](../Page/閩學.md "wikilink")，數年不下樓。推崇[程朱理學](../Page/程朱理學.md "wikilink")，注重“靜時涵養，動時省察”。中年家贫，躬耕讀書，“本之以[小學](../Page/小學.md "wikilink")、[四書](../Page/四書.md "wikilink")，持之以躬行實踐”，“非其义，一介不取”，學者稱康齋先生。有弟子[胡居仁](../Page/胡居仁.md "wikilink")、[陳獻章](../Page/陳獻章.md "wikilink")、[娄谅](../Page/娄谅.md "wikilink")。娄谅不屑细务，一日与门人共耕，吴与弼一边挥锄，一边对娄谅说：“学者，须亲细务。”娄谅立即醒悟。

[天順初授左春坊右諭德](../Page/天順.md "wikilink")\[2\]，以“少贱多病，杜迹山林，本无商行”为由请辞，回鄉講學。英宗三次挽留，坚辞。嘗言“宦官、釋氏不除，而欲天下之治，難矣。吾庸出為？”\[3\]吳與弼自稱經常夢見[孔子](../Page/孔子.md "wikilink")、[周文王二聖](../Page/周文王.md "wikilink")。\[4\]。他開始透過《日錄》記載夢中的情境。其一生言行皆體現於《日錄》，[黄宗羲撰](../Page/黄宗羲.md "wikilink")《[明儒学案](../Page/明儒学案.md "wikilink")》，以“崇仁之学”为首卷，称：“微康斋，焉得后世之盛哉？”宪宗成化五年卒，享年七十九。其墓位于崇仁县六家桥乡曹坊熊家村西南。著作有《康齋文集》、《日錄》等。

## 注釋

  - 鍾彩鈞：〈[吳康齋的生活與學術](http://www.litphil.sinica.edu.tw/home/publish/PDF/Bulletin/10/10-269-315.pdf.pdf)〉。

{{-}}

[Category:明朝理學家](../Category/明朝理學家.md "wikilink")
[Category:中国哲学家](../Category/中国哲学家.md "wikilink")
[Category:中国思想家](../Category/中国思想家.md "wikilink")
[Category:吳姓](../Category/吳姓.md "wikilink")
[Category:明朝儒學學者](../Category/明朝儒學學者.md "wikilink")
[Category:崇仁人](../Category/崇仁人.md "wikilink")

1.  《[明史](../Page/明史.md "wikilink")》（卷143）：“燕兵薄京城，艮與妻子訣曰：「食人之祿者，死人之事。吾不可復生矣。」解縉、吳溥與艮、靖比舍居。城陷前一夕，皆集溥舍。縉陳說大義，靖亦奮激慷慨，艮獨流涕不言。三人去，溥子與弼尚幼，嘆曰：「胡叔能死，是大佳事。」溥曰：「不然，獨王叔死耳。」語未畢，隔墻聞靖呼：「外喧甚，謹視豚。」溥顧與弼曰：「一豚尚不能舍，肯舍生乎？」須臾艮舍哭，飲鴆死矣。縉馳謁，成祖甚喜。明日薦靖，召至，叩頭謝。貫亦迎附。”
2.  周楫《西湖二集·巧书生金銮失对》：“……征聘吴与弼进京，加官进爵，将隆以伊傅之礼。”
3.  〈聘君吴康斋先生与弼〉，《明儒学案》
4.  黃進興《十八世紀中國哲學、語言學和政治》，頁二十九