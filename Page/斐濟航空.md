</ref></small> |lounge = Tabua Club |website =
[www.fijiairways.com](http://www.fijiairways.com) }}

**斐濟航空**（），前身為**太平洋航空**（Air Pacific），由Air Pacific Limited營運\[1\]
\[2\]，是一間以[斐濟](../Page/斐濟.md "wikilink")[納迪為基地的](../Page/納迪.md "wikilink")[國家航空公司](../Page/國家航空公司.md "wikilink")，主要提供往來於[太平洋島國地區](../Page/太平洋.md "wikilink")、澳紐地區、[北美洲](../Page/北美洲.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[香港的航班](../Page/香港.md "wikilink")。

2012年5月，為了鞏固其為斐濟的國家航空地位，**太平洋航空**正式更名为斐济航空，更換航空公司標誌，更購買新[A330-200客機](../Page/空中巴士A330.md "wikilink")，作為整體品牌提升。

2018年12月5日，斐濟航空正式以 oneworld connect
成員的身份加入[寰宇一家](../Page/寰宇一家.md "wikilink")，推薦人為[國泰航空](../Page/國泰航空.md "wikilink")、[美國航空](../Page/美國航空.md "wikilink")、[英國航空](../Page/英國航空.md "wikilink")、[澳洲航空](../Page/澳洲航空.md "wikilink")。\[3\]

[Air_Pacific_Boeing_747_Sydney_Airport.jpg](https://zh.wikipedia.org/wiki/File:Air_Pacific_Boeing_747_Sydney_Airport.jpg "fig:Air_Pacific_Boeing_747_Sydney_Airport.jpg")。\]\]

## 歷史

斐濟航空由[澳洲](../Page/澳洲.md "wikilink")[機師](../Page/機師.md "wikilink")於1951年成立，當時直至1971年仍稱為斐濟航空。1983年起，太平洋航空開始開辦往來[美國的航線](../Page/美國.md "wikilink")。除2001年外，從1995年至2004年，太平洋航空每年都有盈利。2004年，太平洋航空客運量超過50萬人次。

2007年，太平洋航空收購了[斐濟國内航空公司](../Page/斐濟.md "wikilink")[太陽航空之後](../Page/太陽航空.md "wikilink")，成立了[太平洋太陽航空並成為太平洋航空國內航線的子公司](../Page/太平洋太陽航空.md "wikilink")。

斐濟政府擁有斐濟航空51%的股權，[澳洲航空擁有](../Page/澳洲航空.md "wikilink")46.32%的股權。其他如[紐西蘭航空](../Page/紐西蘭航空.md "wikilink")、[基里巴斯](../Page/基里巴斯.md "wikilink")、[湯加](../Page/湯加.md "wikilink")、[瑙魯和](../Page/瑙魯.md "wikilink")[薩摩亞等國家政府均擁有少量股權](../Page/薩摩亞.md "wikilink")。截至2007年3月，斐濟航空共有771名僱員。

## 航點與代碼共享

### 航點

截至2017年12月，斐濟航空營運以下航線：

<table>
<thead>
<tr class="header">
<th><p><strong>斐濟航空航點一覧表</strong>（2017年12月）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>國家／地區</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大洋洲.md" title="wikilink">大洋洲</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蘇瓦.md" title="wikilink">蘇瓦</a></strong></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/墨爾本.md" title="wikilink">墨爾本</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/悉尼.md" title="wikilink">悉尼</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿德莱德.md" title="wikilink">阿得雷德</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塔拉瓦.md" title="wikilink">塔拉瓦</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/基督城.md" title="wikilink">基督城</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/惠灵顿.md" title="wikilink">威靈頓</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/內亞富.md" title="wikilink">內亞富</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞洲.md" title="wikilink">亞洲</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北美洲.md" title="wikilink">北美洲</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/旧金山.md" title="wikilink">舊金山</a></p></td>
</tr>
</tbody>
</table>

### 代碼共享

斐濟航空與以下航空公司實施代碼共享：

  - [紐西蘭航空](../Page/紐西蘭航空.md "wikilink")
  - [萬那杜航空](../Page/萬那杜航空.md "wikilink")
  - [美國航空](../Page/美國航空.md "wikilink")
  - [國泰航空](../Page/國泰航空.md "wikilink")
  - [澳洲航空](../Page/澳洲航空.md "wikilink")
  - [印度捷特航空](../Page/印度捷特航空.md "wikilink")
  - [新加坡航空](../Page/新加坡航空.md "wikilink")
  - [勝安航空](../Page/勝安航空.md "wikilink")

## 機隊

[DQ-FJT@HKG_(20190302142447).jpg](https://zh.wikipedia.org/wiki/File:DQ-FJT@HKG_\(20190302142447\).jpg "fig:DQ-FJT@HKG_(20190302142447).jpg")
截至2017年8月，機隊平均機齡7.6年，詳情如下：

<table>
<caption><strong>斐濟航空機隊</strong></caption>
<thead>
<tr class="header">
<th><p>機型</p></th>
<th><p>數量</p></th>
<th><p>已訂購</p></th>
<th><p>載客量</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><abbr title="Tabua商務艙">J</abbr></p></td>
<td><p><abbr title="Pacific Voyager經濟艙">Y</abbr></p></td>
<td><p>總數</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/空中客车A330.md" title="wikilink">空中巴士A330-243</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>24</p></td>
<td><p>249</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/空中客车A330.md" title="wikilink">空中巴士A330-343</a></p></td>
<td><p>1</p></td>
<td><p>－</p></td>
<td><p>24</p></td>
<td><p>289</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-7X2</a></p></td>
<td><p>1</p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>114</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-8X2</a></p></td>
<td><p>4</p></td>
<td><p>—</p></td>
<td><p>8</p></td>
<td><p>154<br />
162</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737MAX8</a></p></td>
<td><p>—</p></td>
<td><p>5</p></td>
<td><p>即將公佈</p></td>
<td><p>即將公佈</p></td>
</tr>
<tr class="even">
<td><p><strong>總數</strong></p></td>
<td><p>9</p></td>
<td><p>6</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 爭議

### 撒尿事件

太平洋航空被迫就一名乘坐該公司由[日本往](../Page/日本.md "wikilink")[斐济的航班上的斐濟士兵](../Page/斐济.md "wikilink")，向一名同機的[日本女乘客射尿的事件道歉](../Page/日本.md "wikilink")。雖然機組人員向該名日本女乘客免費提供了一套從機上免稅購物服務中的服裝替換、紙巾及殺菌肥皂\[4\]，但此事件在日本國內[媒體造成很大轟動](../Page/媒體.md "wikilink")，並成為了[頭條新聞](../Page/頭條新聞.md "wikilink")。亦由於此事件，日本人抵制前往[斐濟旅遊](../Page/斐濟.md "wikilink")，令太平洋航空來往斐濟及日本的航線及斐濟的旅行業遭到嚴重打擊\[5\]，而東京航線也因此而於2009年停辦，直至2018年7月3日才恢復。

### 機師跳槽

大量太平洋航空的[機師跳槽到一些](../Page/機師.md "wikilink")[中東](../Page/中東.md "wikilink")、[澳洲及](../Page/澳洲.md "wikilink")[亞洲的](../Page/亞洲.md "wikilink")[航空公司以賺取更高收入](../Page/航空公司.md "wikilink")\[6\]，以致太平洋航空[機師數量出現巨大缺口](../Page/機師.md "wikilink")\[7\]。

## 盈利

於2007財政年度，斐濟航空公布稅後淨利為2500萬[斐濟元](../Page/斐濟元.md "wikilink")，折合1700萬[美元](../Page/美元.md "wikilink")。2006財政年度稅後收益只有1700萬[斐濟元](../Page/斐濟元.md "wikilink")，折合1150萬[美元](../Page/美元.md "wikilink")。\[8\]

## 參考資料

<references/>

## 外部連結

  - [斐濟航空](http://www.fijiairways.com/)

  -
  -
[Category:斐濟航空公司](../Category/斐濟航空公司.md "wikilink")
[Category:寰宇一家](../Category/寰宇一家.md "wikilink") [Category:澳洲航空
(公司)](../Category/澳洲航空_\(公司\).md "wikilink")
[Category:1947年成立的航空公司](../Category/1947年成立的航空公司.md "wikilink")

1.  "[Fiji Airways Terms and Conditions of
    Carriage](http://www.fijiairways.com/media/54804/fiji-airways-terms-and-conditions-of-carriage.pdf)."
    ([Archive](http://www.webcitation.org/6IZtuhHpk)) Fiji Airways.
    Retrieved on August 2, 2013. "Air Pacific Limited including Air
    Pacific Limited trading as Fiji Airways and\[...\]"
2.
3.
4.
5.
6.
7.
8.