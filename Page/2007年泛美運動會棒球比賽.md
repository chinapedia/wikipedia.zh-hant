**[2007年泛美運動會](../Page/2007年泛美運動會.md "wikilink")**的棒球比賽是自2007年7月14日至7月20日於[巴西](../Page/巴西.md "wikilink")[里約熱內盧的](../Page/里約熱內盧.md "wikilink")[搖滾城市體育場](../Page/搖滾城市體育場.md "wikilink")（Complexo
Esportivo Cidade do Rock）舉行。

本屆賽事共有8個國家參賽，分別為[2006年泛美棒球錦標賽的前七名及主辦國為](../Page/2006年泛美棒球錦標賽.md "wikilink")[古巴](../Page/古巴.md "wikilink")、[美國](../Page/美國.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[多明尼加](../Page/多明尼加共和國.md "wikilink")（加拿大沒有參賽由多明尼加遞補）、[巴西](../Page/巴西.md "wikilink")（為主辦國）、[巴拿馬與](../Page/巴拿馬.md "wikilink")[委內瑞拉](../Page/委內瑞拉.md "wikilink")。賽制則是分為兩組進行[單循環賽](../Page/循環賽.md "wikilink")，取各組前兩名晉級四強賽。

[古巴於本屆賽事獲得金牌](../Page/古巴.md "wikilink")，也是古巴連續10屆於[泛美運動會棒球比賽獲得金牌](../Page/泛美運動會棒球比賽.md "wikilink")。

## 預賽

### A組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>已賽</p></th>
<th><p>勝</p></th>
<th><p>覆</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>勝率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>20</p></td>
<td><p>10</p></td>
<td><p>1.000</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
<td><p>0.333</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>15</p></td>
<td><p>9</p></td>
<td><p>0.333</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>8</p></td>
<td><p>21</p></td>
<td><p>0.333</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 詳細賽況

##### 7月15日

7月15日09:00（[UTC-3](../Page/UTC-3.md "wikilink")）第一球場（Campo 1）
7月15日12:10（UTC-3）第二球場（Campo 2）

##### 7月16日

7月16日09:00（UTC-3）第一球場（Campo 1）  7月16日12:45（UTC-3）第一球場（Campo 1）

##### 7月18日

7月18日09:00（UTC-3）第二球場（Campo 2）  7月18日10:15（UTC-3）第一球場（Campo 1）

### B組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th><p>已賽</p></th>
<th><p>勝</p></th>
<th><p>敗</p></th>
<th><p>得分</p></th>
<th><p>失分</p></th>
<th><p>勝率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>15</p></td>
<td><p>8</p></td>
<td><p>0.667</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>13</p></td>
<td><p>10</p></td>
<td><p>0.667</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
<td><p>14</p></td>
<td><p>0.667</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
<td><p>11</p></td>
<td><p>0.000</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 詳細賽況

##### 7月14日

7月14日10:00（UTC-3）第一球場（Campo 1）  7月14日13:30（UTC-3）第二球場（Campo
2）因雨延到7月18日09:00（UTC-3）第二球場（Campo 2）

##### 7月15日

7月15日09:00（UTC-3）第二球場（Campo 2）  7月15日12:00（UTC-3）第二球場（Campo 2）

##### 7月16日

7月16日09:00（UTC-3）第二球場（Campo 2）  7月16日12:30（UTC-3）第一球場（Campo 1）

##### 7月18日

7月18日09:00（UTC-3）第二球場（Campo 2）

## 複賽

### 詳細賽況

#### 四強賽

7月18日14:15（UTC-3）第一球場（Campo 1）  7月18日14:15（UTC-3）第二球場（Campo 2）

#### 銅牌賽

原定於7月19日舉行的[尼加拉瓜與](../Page/尼加拉瓜棒球代表隊.md "wikilink")[墨西哥的銅牌戰](../Page/墨西哥棒球代表隊.md "wikilink")，因雨延到7月20日，但7月20日仍因雨無法進行比賽，[國際棒球總會宣布取消比賽](../Page/國際棒球總會.md "wikilink")，由兩隊共同得到銅牌\[1\]。

#### 金牌賽

7月20日09:00（UTC-3）第一球場（Campo 1）

## 獎牌榜

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>獎牌／</strong></p></td>
<td><p><strong>獲獎者</strong></p></td>
<td style="text-align: center;"><p><strong>獎牌／</strong></p></td>
<td><p><strong>獲獎者</strong></p></td>
<td style="text-align: center;"><p><strong>獎牌／</strong></p></td>
<td><p><strong>獲獎者</strong></p></td>
<td style="text-align: center;"><p><strong>獎牌／</strong></p></td>
<td><p><strong>獲獎者</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Med_1.png" title="fig:File:Med 1.png"><a href="File:Med">File:Med</a> 1.png</a></p></td>
<td><p><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></p></td>
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Med_2.png" title="fig:File:Med 2.png"><a href="File:Med">File:Med</a> 2.png</a></p></td>
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Med_3.png" title="fig:File:Med 3.png"><a href="File:Med">File:Med</a> 3.png</a></p></td>
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Med_3.png" title="fig:File:Med 3.png"><a href="File:Med">File:Med</a> 3.png</a></p></td>
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td><p><a href="../Page/Giorvis_Duvergel.md" title="wikilink">Giorvis Duvergel</a><br />
<a href="../Page/Eduardo_Paret.md" title="wikilink">Eduardo Paret</a><br />
<a href="../Page/Luis_Miguel_Navas.md" title="wikilink">Luis Miguel Navas</a><br />
<a href="../Page/Eriel_Sánchez.md" title="wikilink">Eriel Sánchez</a><br />
<a href="../Page/Ariel_Pestano.md" title="wikilink">Ariel Pestano</a><br />
<a href="../Page/Yuliesky_Gourriel.md" title="wikilink">Yuliesky Gourriel</a><br />
<a href="../Page/Yoandry_Urgelles.md" title="wikilink">Yoandry Urgelles</a><br />
<a href="../Page/Adiel_Palma.md" title="wikilink">Adiel Palma</a><br />
<a href="../Page/Norge_Luis_Vera.md" title="wikilink">Norge Luis Vera</a><br />
<a href="../Page/Alexei_Ramírez.md" title="wikilink">Alexei Ramírez</a><br />
<a href="../Page/Frederich_Cepeda.md" title="wikilink">Frederich Cepeda</a><br />
<a href="../Page/Norberto_González.md" title="wikilink">Norberto González</a><br />
<a href="../Page/Jonder_Martínez.md" title="wikilink">Jonder Martínez</a><br />
<a href="../Page/Yoennis_Cespedes.md" title="wikilink">Yoennis Cespedes</a><br />
<a href="../Page/Aroldis_Chapman_de_la_Cruz.md" title="wikilink">Aroldis de la Cruz</a><br />
<a href="../Page/Elier_Sánchez.md" title="wikilink">Elier Sánchez</a><br />
<a href="../Page/Alexander_Mayeta.md" title="wikilink">Alexander Mayeta</a><br />
<a href="../Page/Osmani_Urrutia.md" title="wikilink">Osmani Urrutia</a><br />
<a href="../Page/Yunieski_Maya.md" title="wikilink">Yunieski Maya</a><br />
<a href="../Page/Pedro_Luis_Lazo.md" title="wikilink">Pedro Luis Lazo</a></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/Josh_Romanski.md" title="wikilink">Josh Romanski</a><br />
<a href="../Page/Ryan_Flaherty.md" title="wikilink">Ryan Flaherty</a><br />
<a href="../Page/Roger_Kieschnick.md" title="wikilink">Roger Kieschnick</a><br />
<a href="../Page/Cody_Satterwhite.md" title="wikilink">Cody Satterwhite</a><br />
<a href="../Page/Danny_Espinosa.md" title="wikilink">Danny Espinosa</a><br />
<a href="../Page/Jordy_Mercer.md" title="wikilink">Jordy Mercer</a><br />
<a href="../Page/Justin_Smoak.md" title="wikilink">Justin Smoak</a><br />
<a href="../Page/Logan_Forsythe.md" title="wikilink">Logan Forsythe</a><br />
<a href="../Page/Brett_Hunter.md" title="wikilink">Brett Hunter</a><br />
<a href="../Page/Jordan_Danks.md" title="wikilink">Jordan Danks</a><br />
<a href="../Page/Brian_Matusz.md" title="wikilink">Brian Matusz</a><br />
<a href="../Page/Tommy_Medica.md" title="wikilink">Tommy Medica</a><br />
<a href="../Page/Jacob_Thompson.md" title="wikilink">Jacob Thompson</a><br />
<a href="../Page/Pedro_Alvarez.md" title="wikilink">Pedro Alvarez</a><br />
<a href="../Page/Ryan_Berry.md" title="wikilink">Ryan Berry</a><br />
<a href="../Page/Brett_Wallace.md" title="wikilink">Brett Wallace</a><br />
<a href="../Page/Petey_Paramore.md" title="wikilink">Petey Paramore</a><br />
<a href="../Page/Joe_Kelly_(beisebol).md" title="wikilink">Joe Kelly</a><br />
<a href="../Page/Lance_Lynn.md" title="wikilink">Lance Lynn</a><br />
<a href="../Page/Tyson_Ross.md" title="wikilink">Tyson Ross</a></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/Rafael_Díaz.md" title="wikilink">Rafael Díaz</a><br />
<a href="../Page/Jose_Luis_Sandoval.md" title="wikilink">Jose Sandoval</a><br />
<a href="../Page/Oscar_Rivera.md" title="wikilink">Oscar Rivera</a><br />
<a href="../Page/Ivan_Terrazas.md" title="wikilink">Ivan Terrazas</a><br />
<a href="../Page/Gustavo_García.md" title="wikilink">Gustavo García</a><br />
<a href="../Page/Geronimo_Gil.md" title="wikilink">Geronimo Gil</a><br />
<a href="../Page/Luis_Alfonso_García.md" title="wikilink">Luis García</a><br />
<a href="../Page/Jorge_Campillo.md" title="wikilink">Jorge Campillo</a><br />
<a href="../Page/Efren_Espinoza.md" title="wikilink">Efren Espinoza</a><br />
<a href="../Page/Francisco_Campos_(beisebol).md" title="wikilink">Francisco Campos</a><br />
<a href="../Page/Luis_Mauricio_Suárez.md" title="wikilink">Luis Suárez</a><br />
<a href="../Page/Mario_Alejandro_Valdéz.md" title="wikilink">Mario Valdéz</a><br />
<a href="../Page/Roberto_Ramírez.md" title="wikilink">Roberto Ramírez</a><br />
<a href="../Page/Mauricio_Tequida.md" title="wikilink">Mauricio Tequida</a><br />
<a href="../Page/Noe_de_Jesus_Muñoz.md" title="wikilink">Noe Muñoz</a><br />
<a href="../Page/Benjamin_Gil.md" title="wikilink">Benjamin Gil</a><br />
<a href="../Page/Pablo_Javier_Ortega.md" title="wikilink">Pablo Ortega</a><br />
<a href="../Page/Jose_Leonez.md" title="wikilink">Jose Leonez</a></p></td>
<td style="text-align: center;"></td>
<td><p><a href="../Page/Jorge_Avellan.md" title="wikilink">Jorge Avellan</a><br />
<a href="../Page/Sandor_Guido.md" title="wikilink">Sandor Guido</a><br />
<a href="../Page/Jose_Luis_Saenz.md" title="wikilink">Jose Saenz</a><br />
<a href="../Page/Larry_Galeano.md" title="wikilink">Larry Galeano</a><br />
<a href="../Page/Yasmir_García.md" title="wikilink">Yasmir García</a><br />
<a href="../Page/Eddy_Talavera.md" title="wikilink">Eddy Talavera</a><br />
<a href="../Page/Norman_Cardoze.md" title="wikilink">Norman Cardoze</a><br />
<a href="../Page/Edgard_López.md" title="wikilink">Edgard López</a><br />
<a href="../Page/Danilo_Sotelo.md" title="wikilink">Danilo Sotelo</a><br />
<a href="../Page/Armando_Hernández.md" title="wikilink">Armando Hernández</a><br />
<a href="../Page/Hesse_Loaisiga.md" title="wikilink">Hesse Loaisiga</a><br />
<a href="../Page/Julio_Cesar_Raude.md" title="wikilink">Julio Cesar Raude</a><br />
<a href="../Page/Justo_Cesar_Rivas.md" title="wikilink">Justo Rivas</a><br />
<a href="../Page/Pedro_Rayo_Rojas.md" title="wikilink">Pedro Rojas</a><br />
<a href="../Page/Jairo_Pineda.md" title="wikilink">Jairo Pineda</a><br />
<a href="../Page/Oswaldo_Mairena.md" title="wikilink">Oswaldo Mairena</a><br />
<a href="../Page/Luz_de_Jesús_Portobanco.md" title="wikilink">Luz Portobanco</a><br />
<a href="../Page/Jenrry_Roa_Miranda.md" title="wikilink">Jenrry Miranda</a><br />
<a href="../Page/Sergio_Mena.md" title="wikilink">Sergio Mena</a><br />
<a href="../Page/Jose_Pérez.md" title="wikilink">Jose Pérez</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td></td>
<td style="text-align: center;"></td>
<td></td>
<td style="text-align: center;"></td>
<td></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 總名次

<table>
<thead>
<tr class="header">
<th><p>名次</p></th>
<th><p>國家</p></th>
<th><p>總戰績（勝-敗）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Med_1.png" title="fig:金牌">金牌</a></p></td>
<td><p><a href="../Page/古巴棒球代表隊.md" title="wikilink">古巴</a></p></td>
<td><p>(4-1)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Med_2.png" title="fig:銀牌">銀牌</a></p></td>
<td><p><a href="../Page/美國棒球代表隊.md" title="wikilink">美國</a></p></td>
<td><p>(4-1)</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Med_3.png" title="fig:銅牌">銅牌</a></p></td>
<td><p><a href="../Page/墨西哥棒球代表隊.md" title="wikilink">墨西哥</a></p></td>
<td><p>(2-2)</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Med_3.png" title="fig:銅牌">銅牌</a></p></td>
<td><p><a href="../Page/尼加拉瓜棒球代表隊.md" title="wikilink">尼加拉瓜</a></p></td>
<td><p>(1-3)</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/巴拿馬棒球代表隊.md" title="wikilink">巴拿馬</a></p></td>
<td><p>(2-1)</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/多明尼加棒球代表隊.md" title="wikilink">多明尼加</a></p></td>
<td><p>(1-2)</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/巴西棒球代表隊.md" title="wikilink">巴西</a></p></td>
<td><p>(1-2)</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/委內瑞拉棒球代表隊.md" title="wikilink">委內瑞拉</a></p></td>
<td><p>(0-3)</p></td>
</tr>
</tbody>
</table>

## 注釋與參考文獻

## 外部連結

  - [2007年泛美運動會棒球比賽官方網站](https://web.archive.org/web/20070929111655/http://www.rio2007.org.br/data/pages/8CA3C78913B9C1210113BA412AD3412E.htm)

  - [2007年泛美運動會棒球比賽比賽結果與數據](https://web.archive.org/web/20080112210342/http://www.rio2007.org.br/data/pages/8CA3C78713B9BC7F0113BA44D3C734F8.htm)

[Category:泛美運動會棒球比賽](../Category/泛美運動會棒球比賽.md "wikilink")
[Category:棒球競賽](../Category/棒球競賽.md "wikilink")
[Category:2007年棒球](../Category/2007年棒球.md "wikilink")
[Category:2007年巴西](../Category/2007年巴西.md "wikilink")
[Category:巴西体育史](../Category/巴西体育史.md "wikilink")
[Category:2007年7月](../Category/2007年7月.md "wikilink")

1.  [RIO 2007: México e Nicarágua dividem a medalha de bronze no
    beisebol
    - 20/07/2007](http://www.rio2007.org.br/data/pages/8CA3C78813D331F10113E06F7ABF6DD0.htm)