**金門酒廠籃球隊**為臺灣[超級籃球聯賽的一支球隊](../Page/超級籃球聯賽.md "wikilink")，之前為九太科技，後來陸續改名為**東森羚羊**、**米迪亞精靈**，最終於2008年年底改為現名。

## 歷史

原**大華建設籃球隊**，當時領隊為[閻家驊](../Page/閻家驊.md "wikilink")。宏福解散後，接收了原先宏福大部分的球員，包括一代中華隊主力中鋒[朱志清和宏福新人](../Page/朱志清.md "wikilink")[尚韋帆](../Page/尚韋帆.md "wikilink")，2000年-2001年，易主**九太科技**。一直都待在甲組打比賽，如總統盃籃球賽。2003年半職業聯賽[SBL成立](../Page/SBL.md "wikilink")，九太遂加入之。2004年，[東森購物想入主SBL](../Page/東森購物.md "wikilink")，便併購九太科技的籃球隊，並在其後更名為**東森羚羊**，旗下教練班底也多數來自前[中華職籃](../Page/中華職籃.md "wikilink")[宏福公羊隊的教練群](../Page/宏福公羊隊.md "wikilink")，而知名主播[傅達仁也在其中](../Page/傅達仁.md "wikilink")。後來在2007年由車用導航器（[GPS](../Page/GPS.md "wikilink")）製造商[米迪亞系統科技接手並更名](../Page/米迪亞系統科技.md "wikilink")**米迪亞精靈**。米迪亞母企業[賽亞科技於](../Page/賽亞科技.md "wikilink")2008年2月接手[中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS隊](../Page/誠泰COBRAS.md "wikilink")，成立[米迪亞暴龍隊](../Page/米迪亞暴龍.md "wikilink")，成為台灣第一個同時擁有籃球隊與棒球隊的企業體。2008年底由於米迪亞暴龍隊爆發簽賭風暴，之後由[鴻亞國際企業和](../Page/鴻亞國際企業.md "wikilink")[得亨行銷接手](../Page/得亨行銷.md "wikilink")、[金門酒廠冠名贊助](../Page/金門酒廠.md "wikilink")，改名**金門酒廠籃球隊**。2012年得亨行銷退出球隊經營，由鴻亞國際獨立管理。2013年金門酒廠表示直接接手管理權，同時教練團改組，以期改變球隊風格。直到第12季改變季後賽賽制後，終於打進季後賽，最終第一輪以直落三遭到[台灣啤酒籃球隊橫掃](../Page/台灣啤酒籃球隊.md "wikilink")。

2013年7月，金門酒廠實業股份有限公司（金酒公司）正式取得金門酒廠籃球隊之經營管理權，同時指派金門縣籃球協會，做為金門酒廠籃球隊之經營管理單位，金酒公司為督導檢核單位。
期望透過金門縣政府及金酒公司的整合，直接經營管理球隊，並與金門縣各鄉鎮結合，推動籃球種子培育計畫，培育更多優秀的籃球選手。

SBL金門酒廠籃球隊為第13季SBL超級籃球聯賽拍攝球員形象照時，也發佈球隊最新視覺LOGO以及新球季口號「WE
CAN」，一切做足準備就是希望為新球季全力以赴，證明彼此。金酒球隊邀請曾獲選台灣百大設計師的新穎設計師跨刀合作，展現金門人文、歷史、在地文化特色為主軸，金門高粱英文縮寫（KINMEN
KAOLIANG）的KKL，背後鑲著金門古厝的屋頂造型以及金門人俗稱大山的太武山。設計理念利用創新思考，結合多種元素，除了讓金門在地文化特色與金門酒廠籃球隊有更深連結，也象徵金門酒廠籃球隊獨特形象。

金門酒廠籃球隊於2015年11月18日公佈新球季吉祥物，是他們日前遇到的走失狗「高粱」，不僅發揮愛心，也希望大家能共同關懷流浪動物，並且善待牠們。

2016年2月26日金酒確定成為唯一七搶六中無緣季後賽的球隊。至於金門酒廠籃球隊每年不只流失球員，甚至不定期更換總教練。金酒今年也是以[台灣師大班底為主](../Page/台灣師大.md "wikilink")。

2018年4月15日金酒第六戰輸給璞園隊後遭到淘汰，這也是金酒改名後隊史最佳季後賽成績。

## 歷年戰績

### 例行賽

| 年度                               | 球隊名稱  | 總教練                                                               | 名次  | 出賽數   | 勝      | 敗      | 勝率        | 備註         |
| -------------------------------- | ----- | ----------------------------------------------------------------- | --- | ----- | ------ | ------ | --------- | ---------- |
| 2003-2004                        | 九太科技  | [陳永樂](../Page/陳永樂.md "wikilink")                                  | 4   | 24    | 12     | 12     | 0.500     | 中廣晉級       |
| 2004-2005                        | 東森羚羊  | [劉華林](../Page/劉華林.md "wikilink")→[李雲翔](../Page/李雲翔.md "wikilink") | 4   | 30    | 16     | 14     | 0.500     | 台啤晉級       |
| 2005-2006                        | 東森羚羊  | [李雲翔](../Page/李雲翔.md "wikilink")                                  | 6   | 30    | 11     | 19     | 0.366     | 李雲翔離職      |
| 2006-2007                        | 東森羚羊  | [邱大宗](../Page/邱大宗.md "wikilink")                                  | 5   | 30    | 15     | 15     | 0.500     | 傳出易主（賽亞科技） |
| 2007-2008                        | 米迪亞精靈 | [吳建國](../Page/吳建國_\(籃球\).md "wikilink")                           | 3   | 30    | **19** | 11     | 0.633     | 首度進四強      |
| 2008-2009                        | 金門酒廠  | [羅天金](../Page/羅天金.md "wikilink")                                  | 5   | 30    | 13     | 17     | 0.433     | 易主（金門酒廠）   |
| 2009-2010                        | 金門酒廠  | [羅天金](../Page/羅天金.md "wikilink")                                  | 6   | 30    | 14     | 16     | 0.467     |            |
| 2010-2011                        | 金門酒廠  | [羅天金](../Page/羅天金.md "wikilink")                                  | 7   | 30    | 10     | 20     | 0.333     |            |
| 2011-2012                        | 金門酒廠  | [許智超](../Page/許智超_\(台灣\).md "wikilink")                           | 5   | 30    | 14     | 16     | 0.467     |            |
| 2012-2013                        | 金門酒廠  | [羅天金](../Page/羅天金.md "wikilink")                                  | 6   | 30    | 9      | 21     | 0.300     |            |
| 2013-2014                        | 金門酒廠  | [楊志豪](../Page/楊志豪.md "wikilink")                                  | 6   | 30    | 12     | 18     | 0.400     |            |
| 2014-2015                        | 金門酒廠  | [吳俊雄](../Page/吳俊雄.md "wikilink")                                  | 6   | 30    | 11     | 19     | 0.367     |            |
| 2015-2016                        | 金門酒廠  | \*[許智超](../Page/許智超_\(台灣\).md "wikilink")                         | 7   | 30    | 4      | **26** | **0.133** |            |
| 2016-2017                        | 金門酒廠  | [楊哲宜](../Page/楊哲宜.md "wikilink")                                  | 6   | 30    | 9      | 21     | 0.300     |            |
| 2017-2018                        | 金門酒廠  | [吳俊雄](../Page/吳俊雄.md "wikilink")                                  | 5   | 29    | 16     | 13     | 0.551     |            |
| [邱繼緯](../Page/邱繼緯.md "wikilink") | 1     | 0                                                                 | 1   | 0.000 |        |        |           |            |
| 2018-2019                        | 金門酒廠  | [吳俊雄](../Page/吳俊雄.md "wikilink")                                  | 5   | 36    | 18     | 18     | 0.500     |            |
| 合計:16年                           | 480   | 203                                                               | 277 | 0.423 | 一次進四強  |        |           |            |

備註

  -
    總教練
      - 第二季[李雲翔於](../Page/李雲翔.md "wikilink")2月20日起接任總教練
      - 第十三季[許智超掛名總教練](../Page/許智超_\(台灣\).md "wikilink")，但訓練與調兵遣將由執行教練[James
        Duncan負責](../Page/James_Duncan.md "wikilink")。
    名次
      - 第一季戰績與[中廣戰神相同](../Page/中廣戰神.md "wikilink")，但比較例行賽對戰成績後為[中廣戰神](../Page/中廣戰神.md "wikilink")3：1九太科技
      - 第二季戰績與[台灣啤酒相同](../Page/台灣啤酒.md "wikilink")，但比較例行賽對戰成績後為[台灣啤酒](../Page/台灣啤酒.md "wikilink")3：2東森羚羊

:\***粗體黑字**為該隊史最高，<span style="color:red">**紅字**</span>為SBL紀錄

### 季後賽

| 年度                                    | 球隊    | 總教練                                     | 對手                                      | 出賽數  | 勝     | 敗 | 勝率    | 備註    |
| ------------------------------------- | ----- | --------------------------------------- | --------------------------------------- | ---- | ----- | - | ----- | ----- |
| 2007-2008                             | 米迪亞精靈 | [吳建國](../Page/吳建國_\(籃球\).md "wikilink") | [台灣啤酒](../Page/台灣啤酒籃球隊.md "wikilink")   | 3    | 0     | 3 | 0.000 | 準決賽失利 |
| 2014-2015                             | 金門酒廠  | [吳俊雄](../Page/吳俊雄.md "wikilink")        | [台灣啤酒](../Page/台灣啤酒籃球隊.md "wikilink")   | 2    | 0     | 2 | 0.000 | 第一輪失利 |
| 2016-2017                             | 金門酒廠  | [楊哲宜](../Page/楊哲宜.md "wikilink")        | [台灣啤酒](../Page/台灣啤酒籃球隊.md "wikilink")   | 2    | 0     | 2 | 0.000 | 第一輪失利 |
| 2017-2018                             | 金門酒廠  | [吳俊雄](../Page/吳俊雄.md "wikilink")        | [台灣啤酒](../Page/台灣啤酒籃球隊.md "wikilink")   | 3    | 2     | 1 | .667  | 贏得第一輪 |
| [桃園璞園](../Page/璞園建築籃球隊.md "wikilink") | 6     | 2                                       | 4                                       | .333 | 準決賽失利 |   |       |       |
| 2018-2019                             | 金門酒廠  | [吳俊雄](../Page/吳俊雄.md "wikilink")        | [裕隆納智捷](../Page/裕隆納智捷籃球隊.md "wikilink") | 2    | 0     | 2 | .000  | 第一輪失利 |

## 現役球員

<table>
<tbody>
<tr class="odd">
<td><h3 id="後衛">後衛</h3>
<ul>
<li>07 <a href="../Page/于煥亞.md" title="wikilink">于煥亞</a></li>
<li>11 <a href="../Page/柯旻豪.md" title="wikilink">柯旻豪</a></li>
<li>13 <a href="../Page/王泰傑.md" title="wikilink">王泰傑</a></li>
<li>42 <a href="../Page/盧冠軒.md" title="wikilink">盧冠軒</a></li>
<li>82 <a href="../Page/吳敏賢.md" title="wikilink">吳敏賢</a></li>
</ul>
<h3 id="前鋒">前鋒</h3>
<ul>
<li>02 <a href="../Page/林柏偉.md" title="wikilink">林柏偉</a></li>
<li>09 <a href="../Page/張容軒.md" title="wikilink">張容軒</a></li>
<li>11 <a href="../Page/陳文宏.md" title="wikilink">陳文宏</a></li>
<li>20 <a href="../Page/陳冠毅.md" title="wikilink">陳冠毅</a></li>
<li>24 <a href="../Page/周暐宸.md" title="wikilink">周暐宸</a></li>
<li>35 <a href="../Page/余純安.md" title="wikilink">余純安</a></li>
<li>41 <a href="../Page/蘇奕晉.md" title="wikilink">蘇奕晉</a></li>
<li>77 <a href="../Page/邱金龍.md" title="wikilink">邱金龍</a></li>
<li>95 <a href="../Page/陳靖寰.md" title="wikilink">陳靖寰</a></li>
<li>— <a href="../Page/Zoran_Vrkić.md" title="wikilink">Zoran Vrkić</a></li>
</ul>
<h3 id="中鋒">中鋒</h3>
<ul>
<li>00 <a href="../Page/李家瑞.md" title="wikilink">李家瑞</a></li>
<li>12 <a href="../Page/勤明慶.md" title="wikilink">勤明慶</a></li>
<li>18 <a href="../Page/吳怡斌.md" title="wikilink">吳怡斌</a></li>
<li>31 <a href="../Page/李俊緯.md" title="wikilink">李俊緯</a></li>
<li>— <a href="../Page/Walter_Sharpe.md" title="wikilink">Walter Sharpe</a></li>
</ul></td>
<td><p> </p></td>
<td><h4 id="教練管理團隊">教練管理團隊</h4>
<p><strong>領隊</strong></p>
<ul>
<li>領隊：<a href="../Page/蔡春生.md" title="wikilink">蔡春生</a></li>
<li>副領隊：<a href="../Page/羅富銘.md" title="wikilink">羅富銘</a></li>
</ul>
<p><strong>教練團</strong></p>
<ul>
<li>總教練：<a href="../Page/吳俊雄.md" title="wikilink">吳俊雄</a></li>
<li>助理教練：<a href="../Page/邱繼緯.md" title="wikilink">邱繼緯</a></li>
<li>體能教練：吳俊穎</li>
<li>防護員：翁健愷</li>
<li>顧問：卜美可</li>
</ul>
<p><strong>管理團隊</strong></p>
<ul>
<li>管理：張庭睿</li>
<li>行銷公關：余柏翰</li>
<li>翻譯：劉文和</li>
</ul></td>
</tr>
</tbody>
</table>

## 歷史名人

  - 2 [尚恩](../Page/尚恩.md "wikilink")**平均成績**得分：26.7籃板：10.1助攻：2.0
  - 12 [鄭人維](../Page/鄭人維.md "wikilink")**平均成績**得分：21.1籃板：8.5助攻：2.5
  - 尚韋帆
  - 楊玉明
  - 陳建州
  - [張文平](../Page/張文平.md "wikilink")

## 外部連結

[Category:超級籃球聯賽球隊](../Category/超級籃球聯賽球隊.md "wikilink")
[Category:台灣菸酒事業](../Category/台灣菸酒事業.md "wikilink")
[Category:2003年建立的體育俱樂部](../Category/2003年建立的體育俱樂部.md "wikilink")