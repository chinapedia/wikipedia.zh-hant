**NBA
Live**是一款以[NBA為主題的](../Page/NBA.md "wikilink")[電子遊戲系列](../Page/電子遊戲.md "wikilink")。由[EA加拿大研发](../Page/EA.md "wikilink")，EA
SPORTS发行。主要的競爭者有[Take-Two的](../Page/Take-Two_Interactive.md "wikilink")[NBA
2K系列](../Page/NBA_2K系列.md "wikilink")。

## 歷史

這個遊戲的起初藍本是根據[NBA季後賽設計](../Page/NBA季後賽.md "wikilink")，名為**Lakers versus
Celtics**，即當時[NBA](../Page/NBA.md "wikilink")[東](../Page/東部聯盟_\(NBA\).md "wikilink")、[西兩大聯盟的強隊](../Page/西部聯盟_\(NBA\).md "wikilink")[洛杉磯湖人隊與](../Page/洛杉磯湖人隊.md "wikilink")[波士頓塞爾特人隊](../Page/波士頓塞爾特人隊.md "wikilink")。於1989年推出。後繼的版本為：

| 年份    | 遊戲名稱（英文）              |
| ----- | --------------------- |
| 1989年 | Lakers versus Celtics |
| 1991年 | Bulls versus Lakers   |
| 1992年 | Team USA Basketball   |
| 1993年 | Bulls versus Blazers  |
| 1994年 | NBA Showdown 94       |

而1995年開始，遊戲系統正式命名為**NBA Live**，中文译名为**勁爆美國職籃**。2010年起，游戏名称改为NBA Elite
11。但由于試玩版出現「耶穌事件」使得Elite 11 PS3/Xbox
360版的發售计划被取消，游戏仅在iOS平台发售。\[1\]2011年EA并没有发布新的NBA系列游戏。

2012年，EA又把遊戲名稱改回NBA Live，並且計畫推出NBA Live13，讓《NBA Live》系列捲土重來，但受到老對手《NBA
2K13》的影響，於是，NBA Live13的上市計畫依舊被取消。

2013年11月，EA带着LIVE系列的新作品NBA LIVE 14重新归来。游戏率先在北美区上市，运作于[Xbox
One和](../Page/Xbox_One.md "wikilink")[PlayStation
4平台](../Page/PlayStation_4.md "wikilink")。\[2\]，但作为Live系列的最新作，Live
14的表现并不理想。IGN仅仅给出了此部作品4.3的低分。\[3\]雖然後續的NBA LIVE 15和NBA LIVE
16有些許進步，但評價依然不如《NBA 2K》。而且銷售量也不如《NBA
2K》。根據2015年10月份NPD的遊戲銷售量報告，《NBA 2K16》在北美獲得100萬的銷量，而NBA LIVE
16僅獲得不到1萬的銷量。\[4\]

2016年5月，EA宣布，NBA LIVE 17將延期至2017年推出，打破以往該系列於NBA賽季開季之前推出的傳統，並且負責人Sean
O'Brien表示，他們會在今年晚些時候推出NBA LIVE Mobile。然而，直到[2016-17
NBA賽季結束](../Page/2016-17_NBA賽季.md "wikilink")，NBA LIVE
17並沒有推出，EA也沒有發布任何有關NBA LIVE 17的消息。

2016年7月6日，EA意識到了2K16手機版失去了部分市佔率，於是將NBA LIVE
Mobile上架到全球市場（除中國大陸、台灣、香港、澳門和日本），在該遊戲推出6個小時以來，下載量名列第一，並且在iTunes首頁上被推薦1次。有可能會改變以往NBA
LIVE與2K競爭中總是處於劣勢的局面。雖然是在NBA休賽期間推出，但NBA LIVE
Mobile還是成功吸引玩家。該遊戲也在同年的11月1日於台灣、香港、澳門和日本上架，推出亞洲區版本。\[5\]\[6\]

2017年6月，EA在E3 2017前夕舉辦的EA Play記者會中，正式公開[NBA LIVE
18](../Page/NBA_LIVE_18.md "wikilink")，該作將跟以往一樣只登陸PS4和Xbox
One平台，並且於同年8月11日推出試玩版。\[7\]

## 各年份的封面

| 名稱                                                       | 球員．球隊                                                                              |
| -------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| [勁爆美國職籃95](../Page/勁爆美國職籃95.md "wikilink")               | [休士頓火箭與](../Page/休士頓火箭.md "wikilink")[紐約尼克在](../Page/紐約尼克.md "wikilink")94年冠軍賽爭冠場面 |
| 勁爆美國職籃96                                                 | [-{zh-hans:沙奎尔·奥尼尔; zh-hk:沙奎爾·奧尼爾;}-](../Page/沙奎爾·奧尼爾.md "wikilink")               |
| 勁爆美國職籃97                                                 | [-{zh-hans:米奇·里奇蒙德; zh-hk:米奇·列治文;}-](../Page/米奇·里奇蒙德.md "wikilink")                |
| 勁爆美國職籃98                                                 | [-{zh-hans:蒂姆·哈达威; zh-hk:添·夏達威;}-](../Page/蒂姆·哈达威.md "wikilink")                   |
| 勁爆美國職籃99                                                 | [安東尼·沃克](../Page/安東尼·沃克.md "wikilink")                                             |
| 勁爆美國職籃2000                                               | [-{zh-hans:蒂姆·邓肯; zh-hk:添·鄧肯;}-](../Page/蒂姆·邓肯.md "wikilink")                      |
| 勁爆美國職籃2001                                               | [-{zh-hans:凯文·加内特; zh-hk:奇雲·加納特;}-](../Page/凯文·加内特.md "wikilink")                  |
| 勁爆美國職籃2002                                               | [史蒂夫·弗朗西斯](../Page/史蒂夫·弗朗西斯.md "wikilink")                                         |
| 勁爆美國職籃2003                                               | [-{zh-hans:贾森·基德; zh-hk:賈森·傑特;}-](../Page/賈森·傑特.md "wikilink")                     |
| 勁爆美國職籃2004                                               | [文斯·卡特](../Page/文斯·卡特.md "wikilink")                                               |
| 勁爆美國職籃2005                                               | [卡梅隆·安東尼](../Page/卡梅隆·安東尼.md "wikilink")                                           |
| 勁爆美國職籃06                                                 | [-{zh-hans:德怀恩·韦德; zh-hk:德韋恩·韋德;}-](../Page/德韋恩·韋德.md "wikilink")                  |
| 勁爆美國職籃07                                                 | [特雷西·麥克格雷迪](../Page/特雷西·麥克格雷迪.md "wikilink")                                       |
| [勁爆美國職籃08](../Page/勁爆美國職籃08.md "wikilink")               | [吉爾伯特·阿里納斯](../Page/吉爾伯特·阿里納斯.md "wikilink")                                       |
| [勁爆美國職籃09](../Page/勁爆美國職籃09.md "wikilink")               | [托尼·帕克](../Page/托尼·帕克.md "wikilink")                                               |
| [勁爆美國職籃10](../Page/勁爆美國職籃10.md "wikilink")               | [德怀特·霍华德](../Page/德怀特·霍华德.md "wikilink")                                           |
| [NBA Elite 11](../Page/NBA_Elite_11.md "wikilink")       | [凯文·杜兰特](../Page/凯文·杜兰特.md "wikilink")                                             |
| [NBA LIVE 14](../Page/NBA_LIVE_14.md "wikilink")         | [凯里·欧文](../Page/凯里·欧文.md "wikilink")                                               |
| [NBA LIVE 15](../Page/NBA_LIVE_15.md "wikilink")         | [達米恩·李拉德](../Page/達米恩·李拉德.md "wikilink")                                           |
| [NBA LIVE 16](../Page/NBA_LIVE_16.md "wikilink")         | [拉塞爾·威斯布魯克](../Page/拉塞爾·威斯布魯克.md "wikilink")                                       |
| [NBA LIVE Mobile](../Page/NBA_LIVE_Mobile.md "wikilink") | [詹姆士·哈登](../Page/詹姆士·哈登.md "wikilink")                                             |
| [NBA LIVE 18](../Page/NBA_LIVE_18.md "wikilink")         | [詹姆士·哈登](../Page/詹姆士·哈登.md "wikilink")\[8\]\[9\]                                   |
| [NBA LIVE 19](../Page/NBA_LIVE_19.md "wikilink")         | [喬爾·恩比德](../Page/喬爾·恩比德.md "wikilink")                                             |
|                                                          |                                                                                    |

## 用于PC的版本

EA SPORTS在2008年发布NBA LIVE 09时并没有发布相应的PC版本。官方没有给出具体原因，但外界人士疑为EA
SPORTS出于下述考虑：此前PC版本的NBA LIVE系列游戏被大量地盗版，而北美地区的合法玩家多使用XBOX
360或PLAYSTAION 3等游戏机，取消PC版本对大部分合法用户没有很大的影响。

一年后，NBA LIVE 10同样没有出现PC的版本。

加之NBA 2K自2K9起第一次推出PC版，这次改变直接导致很多PC版的用户转向NBA 2K系列。在NBA
LIVE系列一年的发售空白期后EA同样没有在PC上推出NBA LIVE 14的打算。

而有傳言說即將於2017年推出的NBA LIVE 17將會推出PC版本，並且確定於2017年1月登陸PS4、Xbox
One和PC平台(Win10獨佔)，不過，這項消息並未獲得EA官方證實，因此，NBA
LIVE系列在短時間之內不會在PC平台上推出新作。\[10\]\[11\]\[12\]\[13\]

## 参考资料

[NBA_Live系列](../Category/NBA_Live系列.md "wikilink")
[Category:1995年首发的电子游戏系列](../Category/1995年首发的电子游戏系列.md "wikilink")
[Category:電子遊戲系列](../Category/電子遊戲系列.md "wikilink")
[Category:艺电电子游戏系列](../Category/艺电电子游戏系列.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [【E3 17】《勁爆美國職籃 18》亮相 預定今年 8
    月開放試玩](https://gnn.gamer.com.tw/7/148127.html)
8.  [受肯定！Harden成《NBA
    LIVE 18》封面人物，大鬍子：我感到很榮幸](http://www.dongtw.com/nba/20170811/552600.html)
9.  ['NBA LIVE 18' Cover Athlete And Art
    Revealed](https://www.forbes.com/sites/brianmazique/2017/08/10/nba-live-18-cover-athlete-and-art-revealed)
10. [9年了！NBA
    LIVE 17宣布正式重回PC平台！WIN10独占！](http://bbs.hupu.com/17041109.html)
11. [NBA LIVE 17 (PC)](http://www.gamesdeal.com/nba-live-17-pc.html)
12. [NBA LIVE 17 for
    PC?\!](http://forums.nba-live.com/viewtopic.php?f=162&t=101919)
13. [辟谣！nbalive17重回pc？证实为假消息](http://bbs.hupu.com/17043273.html)