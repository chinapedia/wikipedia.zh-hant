**李世恩**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/演員.md "wikilink")。1999年MBC第28期公開選拔演員。
2015年3月6日與小3歲的未婚夫金英允在首爾凱悅大酒店舉行了婚禮，其未婚夫為富國證券老闆的兒子。

## 演出作品

### 電視劇

  - 2002年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[野人時代](../Page/野人時代.md "wikilink")》
  - 2003年：[KBS](../Page/韓國放送公社.md "wikilink")《[保鏢](../Page/保鏢_\(韓國電視劇\).md "wikilink")》
  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[大長今](../Page/大長今_\(電視劇\).md "wikilink")》飾
    朴阿烈
  - 2005年：MBC《[加油！金順](../Page/加油！金順.md "wikilink")》飾 張恩珠
  - 2005年：SBS《[愛需要奇蹟](../Page/愛需要奇蹟.md "wikilink")》
  - 2006年：SBS《[淵蓋蘇文](../Page/淵蓋蘇文_\(電視劇\).md "wikilink")》飾 高素媛
  - 2007年：SBS《[起飛](../Page/起飛_\(韓國連續劇\).md "wikilink")》飾 車宥利
  - 2010年：KBS《[近肖古王](../Page/近肖古王_\(電視劇\).md "wikilink")》
  - 2012年：[TV朝鮮](../Page/TV朝鮮.md "wikilink")《[好運來](../Page/好運來.md "wikilink")》
  - 2014年：KBS《[天上女人](../Page/天上女人.md "wikilink")》飾 李珍瑜（客串）

### 電影

  - 2000年：《[到海邊去](../Page/到海邊去.md "wikilink")》
  - 2004年：《[筆仙](../Page/筆仙.md "wikilink")》
  - 2006年：《[愛在那年盛夏](../Page/愛在那年盛夏.md "wikilink")》

## 外部連結

  - [EPG](https://web.archive.org/web/20070830194754/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=3309)


  -
[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國舞台演員.md "wikilink")
[L](../Category/世宗大學校友.md "wikilink")
[L](../Category/高麗大學校友.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[L](../Category/李姓.md "wikilink")