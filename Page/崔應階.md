**崔應階**（），字**吉升**，[湖北省](../Page/湖北省.md "wikilink")[武昌府](../Page/武昌府.md "wikilink")[江夏縣](../Page/江夏縣.md "wikilink")（今屬[武漢市](../Page/武漢市.md "wikilink")[江夏區](../Page/江夏區.md "wikilink")）人，[清朝政治人物](../Page/清朝.md "wikilink")。

## 生平

[浙江](../Page/浙江.md "wikilink")[處州鎮](../Page/處州鎮.md "wikilink")[總兵](../Page/總兵.md "wikilink")[崔相國之子](../Page/崔相國.md "wikilink")。[廕生出身](../Page/廕生.md "wikilink")。[康熙五十九年](../Page/康熙.md "wikilink")（1720年）初授[順天府](../Page/順天府.md "wikilink")[通判](../Page/通判.md "wikilink")，遷[順天府西路廳](../Page/順天府西路廳.md "wikilink")[同知](../Page/同知.md "wikilink")。[雍正九年](../Page/雍正.md "wikilink")（1731年），擢[山西](../Page/山西.md "wikilink")[汾州府](../Page/汾州府.md "wikilink")[知府](../Page/知府.md "wikilink")。

[乾隆十二年](../Page/乾隆.md "wikilink")（1747年）遷[河南](../Page/河南.md "wikilink")[南陽府](../Page/南陽府.md "wikilink")[知府](../Page/知府.md "wikilink")。十五年（1750年），升授河南[驛鹽道](../Page/驛鹽道.md "wikilink")。十六年（1751年），擢[安徽](../Page/安徽.md "wikilink")[按察使](../Page/按察使.md "wikilink")。十七年（1752年）[丁母憂](../Page/丁母憂.md "wikilink")，離職。

二十年（1755年）服闋，補授[貴州按察使](../Page/貴州.md "wikilink")。二十一年（1756年），擢[湖南](../Page/湖南.md "wikilink")[布政使](../Page/布政使.md "wikilink")，隨即以布政使署理[湖南巡撫](../Page/湖南巡撫.md "wikilink")。崔應階之子時任甘肅[東樂縣](../Page/東樂縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")[崔琇私下把家書交給傳驛附帶寄送](../Page/崔琇.md "wikilink")，崔應階不檢舉而遭[湖廣總督](../Page/湖廣總督.md "wikilink")[碩色彈劾](../Page/碩色.md "wikilink")，[乾隆帝特諭降調處分](../Page/乾隆帝.md "wikilink")。

二十二年（1757年），補授[江蘇](../Page/江蘇.md "wikilink")[常鎮揚道](../Page/常鎮揚道.md "wikilink")，隨即擢江蘇按察使。二十四年（1759年），再升遷[山東布政使](../Page/山東.md "wikilink")。

二十八年（1763年），遷[貴州巡撫](../Page/貴州巡撫.md "wikilink")，隨後調任[山東巡撫](../Page/山東巡撫.md "wikilink")，任上奏請疏濬荊山橋舊河，宣洩積水。二十九年（1764年），奏言：「武城運河東岸牛蹄窩、祝官屯，西岸蔡河陂水匯注，俱為隄隔，浸灌民田，請各建閘啟閉。」均如所議採納實施。

三十一年（1766年），上疏：「各州縣民壯有名無實，飭屬汰老弱，選精壯，改習鳥鎗，與營伍無二。不增糧餉，省得精壯三千三百餘名。」得旨嘉獎。三十二年（1767年），上疏：「武定濱海，屢有水患：一在徒駭尾閭不暢，一在鉤盤淤塞未開。徒駭上游寬百餘丈，至霑化入海處僅十餘丈，紆回曲折，歸海遲延。徒駭舊有漫口，徑二十五里，寬至四五十丈，水漲賴以宣洩。若就此開濬，庶歸海得以迅速。又有八方泊為眾水所匯，伏秋霖雨，下游阻滯，淹及民田。泊東北為古鉤盤河，經一百三十餘里，久成湮廢。若就此開濬，引水入海，則上游不致停蓄，積水亦可順流而下。」全都獲准施行。

三十二年（1767年），調任[福建巡撫](../Page/福建巡撫.md "wikilink")。三十三年（1768年），擢[閩浙總督](../Page/閩浙總督.md "wikilink")，加[太子太保](../Page/太子太保.md "wikilink")。三十四年（1769年），以總督署理[福州將軍](../Page/福州將軍.md "wikilink")、兼署福建巡撫；彈劾[興泉永道](../Page/興泉永道.md "wikilink")[蔡琛貪鄙](../Page/蔡琛.md "wikilink")，審判後依律論處。

三十五年（1770年），調任[漕運總督](../Page/漕運總督.md "wikilink")；奏陳糧道專職管理漕務，無治理地方的責任，命令糧道親自押運漕米前往[淮安而不得推諉](../Page/淮安.md "wikilink")。三十七年（1772年），召京，授[刑部尚書](../Page/刑部尚書.md "wikilink")。三十八年（1773年），賜紫禁城騎馬。四十一年（1776年），遷[都察院](../Page/都察院.md "wikilink")[左都御史](../Page/左都御史.md "wikilink")。

四十五年（1780年），以原品休致，不久即卒。

## 參考文獻

  - 《清史稿》卷三百九·列傳九十六
  - 《國朝耆獻類徵初編》冊十八·卷七十四
  - [國立故宮博物院圖書文獻館藏](../Page/國立故宮博物院.md "wikilink")《清國史館傳稿》701005723號
  - 《福建省志》，福建省地方誌編纂委員會編，1992年。

[Category:清朝順天府通判](../Category/清朝順天府通判.md "wikilink")
[Category:清朝順天府西路廳同知](../Category/清朝順天府西路廳同知.md "wikilink")
[Category:清朝汾州府知府](../Category/清朝汾州府知府.md "wikilink")
[Category:清朝南陽府知府](../Category/清朝南陽府知府.md "wikilink")
[Category:清朝河南驛鹽道](../Category/清朝河南驛鹽道.md "wikilink")
[Category:清朝安徽按察使](../Category/清朝安徽按察使.md "wikilink")
[Category:清朝貴州按察使](../Category/清朝貴州按察使.md "wikilink")
[Category:清朝湖南布政使](../Category/清朝湖南布政使.md "wikilink")
[Category:清朝常鎮揚道](../Category/清朝常鎮揚道.md "wikilink")
[Category:清朝江蘇按察使](../Category/清朝江蘇按察使.md "wikilink")
[Category:清朝山東布政使](../Category/清朝山東布政使.md "wikilink")
[Category:清朝貴州巡撫](../Category/清朝貴州巡撫.md "wikilink")
[Category:清朝山東巡撫](../Category/清朝山東巡撫.md "wikilink")
[Category:清朝福建巡撫](../Category/清朝福建巡撫.md "wikilink")
[Category:清朝閩浙總督](../Category/清朝閩浙總督.md "wikilink")
[Category:清朝太子三師](../Category/清朝太子三師.md "wikilink")
[Category:清朝漕運總督](../Category/清朝漕運總督.md "wikilink")
[Category:清朝刑部尚書](../Category/清朝刑部尚書.md "wikilink")
[Category:清朝左都御史](../Category/清朝左都御史.md "wikilink")
[Category:賜紫禁城騎馬](../Category/賜紫禁城騎馬.md "wikilink")
[Category:江夏人](../Category/江夏人.md "wikilink")
[Y](../Category/崔姓.md "wikilink")