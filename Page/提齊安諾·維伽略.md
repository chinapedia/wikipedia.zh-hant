[thumb](../Page/file:Frari_\(Venice\)_nave_right_-_Monument_of_Titian.jpg.md "wikilink")
**提齊安諾·維伽略**（**Tiziano Vecelli**或**Tiziano
Vecellio**，1488年（一說為1490年）\[1\]\[2\]\[3\]–
1576年8月27日\[4\]），英語系國家常稱呼為**提香**（Titian），他是[意大利](../Page/意大利.md "wikilink")[文艺复兴后期](../Page/文艺复兴.md "wikilink")[威尼斯画派的代表](../Page/威尼斯画派.md "wikilink")[画家](../Page/画家.md "wikilink")。提香出生于意大利东北部[阿尔卑斯山地区的卡多列](../Page/阿尔卑斯山.md "wikilink")，10岁时随兄长到威尼斯，在[乔瓦尼·贝利尼的画室学画](../Page/乔瓦尼·贝利尼.md "wikilink")，与画家[乔尔乔内是同学](../Page/乔尔乔内.md "wikilink")。

在提香所處的時代，他被稱為“群星中的太陽”，是意大利最有才能的畫家之一，兼工[肖像畫](../Page/肖像畫.md "wikilink")、[風景畫及神話](../Page/風景畫.md "wikilink")、宗教主題的[歷史畫](../Page/歷史畫.md "wikilink")。他對色彩的運用不僅影響了文藝復興時代的意大利畫家，更对[西方藝術產生了深遠的影響](../Page/西方藝術.md "wikilink")。\[5\]

## 生平

提香的早期作品受[拉斐爾和](../Page/拉斐爾.md "wikilink")[米开朗基罗影响很深](../Page/米开朗基罗.md "wikilink")，以后他的作品比起文艺复兴鼎盛时期画家的作品，更重视[色彩的运用](../Page/色彩.md "wikilink")，对后来的画家如[鲁本斯和](../Page/鲁本斯.md "wikilink")[普桑都有很大的影响](../Page/普桑.md "wikilink")。他的作品构思大胆，气势雄伟，[构图严谨](../Page/构图.md "wikilink")，色彩丰富、鲜艳。

提香青年时代在[人文主义思想的主导下](../Page/人文主义.md "wikilink")，继承和发展了威尼斯派的绘画艺术，把油画的色彩、造型和笔触的运用推进到新的阶段，画中所含的情感饱满而深刻，作为乔尔乔涅的助手帮助画了《[沉睡的维纳斯](../Page/沉睡的维纳斯.md "wikilink")》后面的风景。在宗教画《[纳税银](../Page/献金_\(提香\).md "wikilink")》和《[圣母蒙召升天](../Page/聖母蒙召升天_\(提香\).md "wikilink")》中反映了新兴资产阶级的道德观念。《[爱神节](../Page/爱神节.md "wikilink")》、《[酒神与阿丽亚德尼公主](../Page/酒神与阿丽亚德尼公主.md "wikilink")》等神话题材的作品，洋溢着欢欣的情调和旺盛的生命力。但在1533年查理五世封他为授以贵族称号后，则画了《[西班牙拯救了宗教](../Page/西班牙拯救了宗教.md "wikilink")》和《[菲力二世把初生的太子唐·斐迪南献给胜利之神](../Page/菲力二世把初生的太子唐·斐迪南献给胜利之神.md "wikilink")》等趋逢权贵的作品。

提香曾应教皇保罗三世和[神圣罗马帝国皇帝查理五世的邀请到](../Page/神圣罗马帝国.md "wikilink")[罗马和皇帝的宫廷中画了许多肖像画](../Page/罗马.md "wikilink")。但他漫长的一生主要都在威尼斯度过的，留下了大量的作品。他主要的作品是一些[宗教和古](../Page/宗教.md "wikilink")[罗马神话的题材](../Page/罗马神话.md "wikilink")，充满[戏剧性的气氛和动感的人体线条](../Page/戏剧.md "wikilink")。

提香的肖像画能揭示人物内心世界。中年画风细致，稳健有力，色彩明亮；晚年则笔势豪放，色调单纯而富于变化。在油画技法和绘画风格上对后期欧洲油画的发展，有较大影响。

## 遺產

[Titian_-_Diana_and_Actaeon_-_1556-1559.jpg](https://zh.wikipedia.org/wiki/File:Titian_-_Diana_and_Actaeon_-_1556-1559.jpg "fig:Titian_-_Diana_and_Actaeon_-_1556-1559.jpg")》，1556–1559。這幅[歷史畫不僅描繪神話傳説](../Page/歷史畫.md "wikilink")，也涉及了[風景畫和](../Page/風景畫.md "wikilink")[靜物畫要素](../Page/靜物畫.md "wikilink")\]\]
提香一生的作品大約有400幅，現存300餘\[6\]。其中的《[戴安娜與阿克泰翁](../Page/戴安娜與阿克泰翁_\(提香\).md "wikilink")》在2008年由[蘇格蘭國立美術館和](../Page/蘇格蘭國立美術館.md "wikilink")[國家美術館以](../Page/国家美术馆_\(伦敦\).md "wikilink")5000万英鎊的價格買下，成爲成為[世界上最昂貴的畫作之一](../Page/最昂贵绘画列表.md "wikilink")\[7\]。他筆下作品常先以紅色打底，然後再塗上其他顏色，使得其油畫隱約泛出一種金紅色，此色被稱為「提香紅」\[8\]。

## 參考文獻

### 引用

### 来源

  - [Gould, Cecil](../Page/Cecil_Gould.md "wikilink"), *The Sixteenth
    Century Italian Schools*, National Gallery Catalogues, London 1975,
    ISBN 0-947645-22-5
  - Jaffé, David (ed), *Titian*, The National Gallery Company/Yale,
    London 2003, ISBN 1-85709-903-6
  - Landau, David, in Jane Martineau (ed), *The Genius of Venice,
    1500–1600*, 1983, Royal Academy of Arts, London.
  - [Penny, Nicholas](../Page/Nicholas_Penny.md "wikilink"), National
    Gallery Catalogues (new series): *The Sixteenth Century Italian
    Paintings, Volume II, Venice 1540–1600*, 2008, National Gallery
    Publications Ltd, ISBN 1-85709-913-3
  - [Ridolfi, Carlo](../Page/Carlo_Ridolfi.md "wikilink") (1594–1658);
    *The Life of Titian*, translated by Julia Conaway Bondanella and
    Peter E. Bondanella, Penn State Press, 1996, ISBN 0-271-01627-2,
    ISBN 978-0-271-01627-6 [Google
    Books](http://books.google.co.uk/books?id=nNxVU2khKfcC)

## 外部連結

  - [A closer Look at the Madonna of the
    Rabbit](http://musee.louvre.fr/oal/viergeaulapinTitien/viergeaulapinTitien_acc_en.html)
    multimedia feature, Musée du Louvre official site (English version)
  - [The Titian Foundation](http://www.titian-tizianovecellio.org/)
    Images of 168 paintings by the artist.
  - [Titian's
    paintings](http://www.artcyclopedia.com/artists/titian.html)
  - [Tiziano Vecellio at Web Gallery of
    Art](http://www.wga.hu/frames-e.html?/html/t/tiziano/index.html)
  - [Christies' sale blurb for the recently restored 'Mother and
    Child'](http://www.christies.com/presscenter/pdf/09162005/09162005b.pdf)
  - Bell, Malcolm [*The early work of
    Titian*](http://www.archive.org/details/earlyworkoftitia00belluoft),
    at [Internet Archive](../Page/Internet_Archive.md "wikilink")
  - [Titian at Panopticon Virtual Art
    Gallery](https://web.archive.org/web/20080321163554/http://www.aiwaz.net/panopticon/titian/gc207)
  - [How to Paint Like Titian](http://www.nybooks.com/articles/22353)
    [James Fenton](../Page/James_Fenton.md "wikilink") essay on Titian
    from *[The New York Review of
    Books](../Page/The_New_York_Review_of_Books.md "wikilink")*
  - [Tiziano Vecellio - one of the greatest artists of all
    time](http://www.art-drawing.ru/biographies/brief-biographies/1457-tiziano-vecellio)
  - [Interactive high resolution scientific imagery of Titian's
    *Portrait of a Woman with a
    Mirror*](http://merovingio.c2rmf.cnrs.fr/iipimage/ENI_Tiziano/) from
    the [C2RMF](../Page/C2RMF.md "wikilink")

[提香](../Category/提香.md "wikilink")
[Category:1480年代出生](../Category/1480年代出生.md "wikilink")
[Category:1576年逝世](../Category/1576年逝世.md "wikilink")
[Category:15世紀意大利畫家](../Category/15世紀意大利畫家.md "wikilink")
[Category:16世紀意大利畫家](../Category/16世紀意大利畫家.md "wikilink")
[Category:意大利文藝復興畫家](../Category/意大利文藝復興畫家.md "wikilink")
[Category:意大利罗马天主教徒](../Category/意大利罗马天主教徒.md "wikilink")
[Category:威尼斯畫家](../Category/威尼斯畫家.md "wikilink")

1.  [Getty Union Artist Name
    List](http://www.getty.edu/vow/ULANFullDisplay?find=Titian&role=&nation=&prev_page=1&subjectid=500031075)
2.  [Metropolitan Museum of Art
    timeline](http://www.metmuseum.org/toah/hd/tita/hd_tita.htm),
    retrieved 11 February 2009
3.  [When Was Titian
    Born?](http://lafrusta.homestead.com/riv_tiziano.html)
4.
5.  Fossi, Gloria, *Italian Art: Painting, Sculpture, Architecture from
    the Origins to the Present Day*, p. 194. Giunti, 2000. ISBN
    88-09-01771-4
6.  Mark Hudson, *Titian: The Last Days*, Walker and Company, NY, 2000,
    p.10-11.
7.  Severin Carrell ["Titian's Diana and Actaeon saved for the
    nation"](http://www.guardian.co.uk/artanddesign/2009/feb/02/titian-diana-actaeon-saved),
    *The Guardian*, 2 February 2009
8.  游守中，[〈歐洲歷代重要油畫技法的探討與研究〉](http://www.fhk.ndu.edu.tw/mediafile/833/fdownload/253/339/2009-12-1-1-50-9-339-nf1.pdf)，復興崗學報，民
    95，86 期，335-376。