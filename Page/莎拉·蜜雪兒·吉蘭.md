**-{zh-hans:莎拉·米歇爾·吉蘭-普林兹;zh-hk:莎拉·米雪·吉蘭·賓斯;zh-tw:莎拉·蜜雪兒·吉蘭·普林茲;}-**（，），[美國女演員](../Page/美國.md "wikilink")。

## 個人生活

1997年，莎拉於拍攝電影《I know what you did last
summer》認識了現在的丈夫，[小弗雷迪·普林斯](../Page/小弗雷迪·普林斯.md "wikilink")，但他們一直都沒有約會，直至2000年。莎拉和費迪於2001年4月訂婚，2002年9月1日在[墨西哥舉行婚禮](../Page/墨西哥.md "wikilink")。2007年，莎拉向男士雜誌《Maxim》透露，雖然她跟丈夫已結婚多年，但她仍然沒有準備成為母親。同年提出申請，在自己的姓氏後加上夫姓。2009年9月，莎拉的女兒夏綠蒂（Charlotte
Grace Prinze）誕生。2012年9月，兒子洛基（Rocky James Prinze）誕生。

莎拉亦是[跆拳道黑帶](../Page/跆拳道.md "wikilink")。
[Freddie_Prinze_Jr_and_Sarah_Michelle_Gellar_by_David_Shankbone.jpg](https://zh.wikipedia.org/wiki/File:Freddie_Prinze_Jr_and_Sarah_Michelle_Gellar_by_David_Shankbone.jpg "fig:Freddie_Prinze_Jr_and_Sarah_Michelle_Gellar_by_David_Shankbone.jpg")

## 演出作品

  - [天鵝過境](../Page/天鵝過境.md "wikilink")（暫譯）（*Swans Crossing* 1992年）電視劇
  - [我所有的孩子](../Page/我所有的孩子.md "wikilink")（暫譯）（*All My Children*
    1993年）電視劇
  - [比佛利山魯賓遜家族](../Page/比佛利山魯賓遜家族.md "wikilink")（*Beverly Hills Family
    Robinson* 1997年）
  - [是誰搞的鬼](../Page/是誰搞的鬼.md "wikilink")（*I Know What You Did Last
    Summer* 1997年）
  - [驚聲尖叫2](../Page/驚聲尖叫2.md "wikilink")（*Scream 2* 1997年）
  - [魔法奇兵](../Page/魔法奇兵.md "wikilink")（*Buffy the Vampire Slayer*
    1997\~2003年）
  - [魔法一點靈](../Page/魔法一點靈.md "wikilink")（*Simply Irresistible* 1999年）
  - [危險性遊戲](../Page/危險性遊戲.md "wikilink")（*Cruel Intentions* 1999年）
  - [窈窕美眉](../Page/窈窕美眉.md "wikilink")（*She's All That*
    1999年），由[蓮兒與啷噹六便士演唱](../Page/蓮兒與啷噹六便士.md "wikilink")[Kiss
    Me](../Page/Kiss_Me.md "wikilink")
  - [哈佛人](../Page/哈佛人.md "wikilink")（*Harvard Man* 2001年）
  - [史酷比](../Page/史酷比.md "wikilink")（*Scooby-Doo* 2002年）
  - [史酷比2：怪獸偷跑](../Page/史酷比2：怪獸偷跑.md "wikilink")（*Scooby-Doo 2: Monsters
    Unleashed* 2004年）
  - [不死咒怨](../Page/不死咒怨.md "wikilink")（*The Grudge* 2004年）
  - [南方傳奇](../Page/南方傳奇.md "wikilink")（*Southland Tales* 2006年）
  - [鬼謎藏](../Page/鬼謎藏.md "wikilink")（*The Grudge 2* 2006年）
  - [回魂](../Page/回魂.md "wikilink")（*The Return* 2006年）
  - [女孩向錢行](../Page/女孩向錢行.md "wikilink")（*Suburban Girl* 2007年）
  - [交錯效應](../Page/交錯效應.md "wikilink")（*The Air I Breathe* 2008年）
  - [錯位佔據](../Page/錯位佔據.md "wikilink")（*Possession* 2009年）
  - [薇若妮卡決定去死](../Page/薇若妮卡決定去死.md "wikilink")（*Veronika Decides to Die*
    2009年）
  - [美麗疾病](../Page/美麗疾病.md "wikilink")（暫譯）（*The Wonderful Maladys*
    2009年）
  - [替身姐妹](../Page/替身姐妹.md "wikilink")（台灣AXN頻道譯為「[雙生情謎](../Page/雙生情謎.md "wikilink")」）(*Ringer*
    2011 年)

也在遊戲[決勝時刻：黑色行動的殭屍地圖](../Page/決勝時刻：黑色行動.md "wikilink")(Call of The
Dead)中出現

## 外部連結

  -
  - [莎拉·蜜雪兒·吉蘭的Twitter](https://mobile.twitter.com/RealSMG)

[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:美國女配音演員](../Category/美國女配音演員.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")