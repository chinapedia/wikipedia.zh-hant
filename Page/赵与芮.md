**趙與芮**，[宋理宗趙昀之弟](../Page/宋理宗.md "wikilink")，立為榮王。他亦是[宋度宗的生父](../Page/宋度宗.md "wikilink")，[宋恭帝](../Page/宋恭帝.md "wikilink")、[宋端宗及](../Page/宋端宗.md "wikilink")[宋帝昺祖父](../Page/宋帝昺.md "wikilink")。

## 生平

趙與芮本不是皇子，而只是[宋寧宗的遠房堂侄](../Page/宋寧宗.md "wikilink")。他是宋太祖[趙匡胤次子](../Page/趙匡胤.md "wikilink")[趙德昭的九世孫](../Page/趙德昭.md "wikilink")，由於先前宋室帝位並非由趙德昭一脈繼承，至趙與芮父親[趙希瓐這一代](../Page/趙希瓐.md "wikilink")，與皇室血緣十分疏遠。趙希瓐在生時並沒有任何封爵，只當過小官，生活與平民無異，趙與芮也因此在平民家庭出生及成長。趙與芮五歲時，父趙希瓐逝世，生母全氏帶他及兄[趙與莒](../Page/趙與莒.md "wikilink")（即後來的宋理宗）返娘家，三母子在全氏在[紹興當保長的兄長家寄居](../Page/紹興.md "wikilink")，一直到趙與芮十四歲。

宋寧宗因八名親生子皆幼年夭折，故立其堂姪沂王[趙竑為太子](../Page/趙竑.md "wikilink")，沂王王位於是懸空。寧宗命宰相[史彌遠找尋品行端正的宗室繼承沂王王位](../Page/史彌遠.md "wikilink")，而史彌遠將此任務交給其幕僚[余天錫](../Page/余天錫.md "wikilink")。余天錫回鄉應考科舉，途經紹興遇着大雨，在全保長家中避雨，於是認識了趙與莒與趙與芮兄弟。余天錫得知他們為趙氏宗族，也覺得兄弟二人行為得體，認為是繼承沂王的合適人選，故向史彌遠推薦。史彌遠接兩兄弟往[臨安親自考量](../Page/臨安.md "wikilink")，也認為兄長趙與莒為繼承沂王的合適人選，故於[嘉定十四年](../Page/嘉定.md "wikilink")（1221年）將趙與莒選入宮內，賜名貴誠，繼承沂王王位。

太子趙竑一向不滿史彌遠專權，聲言繼位後立即要貶抑史彌遠，史彌遠有意另立新君。嘉定十七年（1224年）宋寧宗駕崩，史彌遠聯同[楊皇后假傳寧宗遺詔](../Page/恭聖皇后.md "wikilink")，廢太子趙竑為濟王，立趙與芮兄長沂王趙貴誠為新帝，是為[宋理宗](../Page/宋理宗.md "wikilink")。

理宗登基為帝後，追封已故的生父趙希瓐為榮王，並由趙與芮繼承王位。由於趙與芮與兄長理宗一同來自民間，故兩兄弟關係十分密切。

理宗的子女不多，兩名兒子皆在幼年夭折，故須要在宗室中尋找繼任人，由於理宗與榮王趙與芮感情深厚，故立他的兒子[趙禥為太子](../Page/趙禥.md "wikilink")。[景定五年](../Page/景定.md "wikilink")（1264年），趙禥在理宗駕崩後繼位，是為[宋度宗](../Page/宋度宗.md "wikilink")。[咸淳十年](../Page/咸淳.md "wikilink")（1274年），度宗駕崩，趙與芮孫（度宗子）趙㬎繼位，是為[宋恭帝](../Page/宋恭帝.md "wikilink")。

[德祐二年](../Page/德祐.md "wikilink")（1276年），临安城被元军攻克，赵与芮和皇嫂、太皇太后[谢道清](../Page/谢道清.md "wikilink")，孙[宋恭帝被带至](../Page/宋恭帝.md "wikilink")[元大都](../Page/元大都.md "wikilink")。趙與芮為宋室中輩份最高，主動將其財產獻給元政府，因此被擄至大都的宋宗室皆被善待。[元世祖忽必烈授宋福王赵与芮金紫光禄大夫](../Page/元世祖.md "wikilink")、检校大司农、平原郡公。[元朝](../Page/元朝.md "wikilink")[至元二十五年](../Page/至元.md "wikilink")（1287年），赵与芮薨逝，享寿八十。

## 與廣東的關係

趙與芮雖然生長在北方，然而他卻和廣東人頗有關係，他有兩個妹妹，一嫁佛山人梁節，一嫁南海西江人冼文溪。一女適廣州番禺石壁區元杰字天行（生卒：1229至元朝，區貢端之子，天行和趙氏生有五名兒子），一女適新會象山聶家積
(子聶桂芳、聶桂華），一女嫁廣東澄海西門蔡豐湖（趙氏郡主真名趙倩）。孫女嫁開平人方道盛。區元杰妻趙氏在1262年嫁給區元杰，時元杰33歲，由於是記載為「宋理宗侄女、趙與芮之女」，因此區元杰妻趙氏年齡上應該比聶趙氏、蔡趙氏大，有可能是錢氏所出。（南海區氏族譜）

蔡豐湖是在宋恭帝時被封為駙馬都尉，潮陽西門蔡氏族譜記載，趙氏是榮王趙與芮與黃氏次女，子蔡彬，孫蔡景、蔡吳，曾孫有五，蔡基、蔡均等人。因蔡氏載，她是宋度宗親妹，榮王趙與芮次姬所出，這應該是他們忘記了黃氏之前還有李氏，黃氏實際上應該是第三妻子
。

至於聶家積妻趙氏，由於她侄孫媳婦容康成（聶家積哥哥聶世積之孫聶思明元配），是約在1290年左右出生，侄曾孫女聶氏，即聶子照（名聶思明）之女，是在1303年出生。加上聶趙氏是宋度宗親妹，因此聶家積與趙氏夫婦，應是在1241至1250左右出生的。聶趙氏是黃定喜所出，因為記載為「宋度宗胞妹」。
(《新會容氏族譜》)

趙與芮接近32歲才有[宋度宗這個兒子](../Page/宋度宗.md "wikilink")，而側室李氏僅入門三年即去世（估計是1237年至1240年），沒留下子嗣。因此不排除趙與芮和元配錢氏可能還有女兒。

潮汕地區還有一名郡馬，叫謝壺山，諱升一，娶宋理宗三皇叔之女。由於宋理宗之父親只有兩名兒子。而《浙江諸暨趙氏族譜》中的確發現趙希瓐中間原來尚有一個空了、沒有記載名字的人，估計這可能是宋理宗之弟、趙與芮之兄，如此則有三皇叔之理，則謝壺山之妻趙氏極可能是趙與芮之女。

由於廣東吳川民間說，原籍新會鶴山的易士熊，生於1258年，1276年娶宋度宗親侄女，晉王女兒趙貽謀，因此趙與芮很可能還有一個兒子，名不詳，必在1240年後出生，因為至1240年時，趙與芮仍未有兒子，是[齊國夫人黃氏在](../Page/齊國夫人.md "wikilink")1240年才生下第一個兒子，即[宋度宗](../Page/宋度宗.md "wikilink")，度宗可能是因為趙與芮長子，獲[宋理宗封為太子](../Page/宋理宗.md "wikilink")。這晉王應該也是黃氏所出，此時錢氏大概已過生育年齡，而李氏亦已去世，民間又說其女兒是度宗親侄女。《浙江諸暨趙氏族譜》載，趙與芮還有一個兒子，但他們記載是沂王，晉王應是後來改封的。除廣東吳川上杭外，中國最少有三個地方，均說趙與芮還有一個兒子，一個是南京應天府，一個是貴州威寧，一個是浙江諸暨，似乎反映廣東吳川上杭所說的不是孤證\[1\]

## 祖先

<center>

</center>

## 註釋與參考文獻

[Category:宋朝宗室](../Category/宋朝宗室.md "wikilink")

1.  吳川和貴州的說法最大分別是，吳川只說晉王女、宋度宗親侄女，但沒說明晉王名稱等資料。而貴州威寧卻說，這王子名叫趙敏，娶姚氏為妻，是宋度宗的哥哥，母親是全氏和黃氏。但這說法只有兩個可以引證，就是榮王趙與芮還有一個兒子，以及母親是黃氏，全氏應該是祖母才對，又或者錢氏和全氏音近誤載，但應可反映這王子是黃氏所出。不過史實上，宋度宗沒有哥哥，因為至1240年前，榮王趙與芮都沒有兒子。