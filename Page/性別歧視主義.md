[National_Association_Against_Woman_Suffrage.jpg](https://zh.wikipedia.org/wiki/File:National_Association_Against_Woman_Suffrage.jpg "fig:National_Association_Against_Woman_Suffrage.jpg")
**性別歧視主義**（Sexism）一般是指基于他人的[性别差异而非他人优缺点所造成的厭惡或是](../Page/性别.md "wikilink")[歧視](../Page/歧視.md "wikilink")，但也可用來指稱任何因為性別所造成的差別待遇。

## 定義

**性別歧視主義**一般是指以下幾種信念或態度：

  - 相信某一种性别比其他种性別优越的信念（[重男輕女](../Page/重男輕女.md "wikilink")）。
  - 相信一個人必定屬於男性或女性的信念（[性別二元論](../Page/性別二元論.md "wikilink")）。
  - 指稱憎恨男性（[男性貶抑](../Page/男性貶抑.md "wikilink")）或憎恨女性（[女性貶抑](../Page/女性貶抑.md "wikilink")）的態度。

## 概述

性別歧視主義可粗略的視為本質主義的產物。本質主義者堅持可以根據個體所屬群體之特徵理解或判斷該個體，而性別歧視主義者則是認為可依照個體所屬的性別群體（男性或女性）來理解或判斷該個體。這樣的觀點假定了所有的人都可以清楚地被劃分成「男性」或「女性」，而忽略了某些同時擁有兩性特質的跨性別者的存在。此外，這種觀點也因群體中的同質性，而忽略了群體中的個別差異。在很多國家中，某些特定的性別歧視是違法的，不過幾乎所有國家都有立法保障某幾種性別群體的權利。性別歧視主義可以分成三個部分來看：對女性的性別歧視主義、對男性的性別歧視主義以及對跨性別者的性別歧視主義。

### 对女性的性别歧视主义

性別歧視主義作用於女性的極端形式就是女性貶抑。由於作用於女性的性別歧視主義最先被廣泛的認識，故成為「性別主義」一詞最常指涉的範圍。這種形式的性別歧視主義也常被稱為男性沙文主義，但沙文主義事實上是個涵義更廣泛的詞，指的是對於競爭性團體的不合理且極端的憎恨與敵意。另一個稍有關係的名詞是「恐女主義」（gynophobia），指的是對女性或是女性氣質的恐懼。

歷史上，在許多父系社會中，女性被視為「較軟弱的一群」。 「矛盾的性別歧視主義」（ambivalent
sexism）指的是對於女性的憎惡態度及基於施捨的仁慈態度的綜合，這種矛盾的文化態度顯示在女性較低的社會地位以及男性對女性角色(妻子、母親、情人)的依賴上。女權運動透過提倡女權(像是法律平等權、參政權、受教權、工作權以及身體自主權)來反​​對性別歧視主義。

### 制度上性别歧视女性的国家

大多数[世俗国家在制度上禁止性别歧视](../Page/世俗国家.md "wikilink")。但一些極端保守及維持傳統宗教教義的信奉伊斯兰教的国家，如[伊朗](../Page/伊朗.md "wikilink")，[沙特阿拉伯等的妇女人权问题仍然非常严重](../Page/沙特阿拉伯.md "wikilink")。

### 对男性的性别歧视主义

[2010-09-14-JobOpeningInformation-IllegalFemaleLimitation.jpg](https://zh.wikipedia.org/wiki/File:2010-09-14-JobOpeningInformation-IllegalFemaleLimitation.jpg "fig:2010-09-14-JobOpeningInformation-IllegalFemaleLimitation.jpg")
性别歧视主义作用于男性的极端形式就是男性贬抑（misandry），指的是对男性的厌恶甚至是憎恨）。另一个稍有关系的名词是「恐男症」（androphobia），指的是对男性或男性气质的恐惧。虽说认为女性比男性更优秀的观点也是性别歧视，但公共论述一直到最近几年才注意到这种歧視形式，Misandry这一单词在20世纪下半叶还没有出现在大多数词典中直到1970年代还被认为是一个新词。\[1\]

### 制度上性别歧视男性的国家和地区

有部分國家和地區在法律和制度上被認為歧視男性，部分法律訴訟如強姦、謀殺、通姦中男性往往會被重判，而女性卻會被輕判甚至無罪釋放，比如中国的法律规定强奸罪只适用于男性，直到2012年美国[联邦调查局才重新界定强奸](../Page/联邦调查局.md "wikilink")，将强奸主体从男性推广到女性并且引发社会讨论\[2\]
。

[香港法律規定](../Page/香港.md "wikilink")，對13歲以下年幼女性作出性行為是非法，而對13歲以下年幼男性則無實際法律。在2006年之前，男性與21歲以下男性之間的[肛交](../Page/肛交.md "wikilink")[同性性行為在香港被視為非法](../Page/同性性行為.md "wikilink")。

與此同時，在[香港中學學位分配辦法裡](../Page/香港中學學位分配辦法.md "wikilink")，男生和女生的派位方式以往是分開的，但2002年開始，因應[平等機會委員會認為此派位辦法違反性別歧視條例](../Page/平等機會委員會.md "wikilink")，則改為男生和女生合併處理，導致女生比男生派位理想。\[3\]另一方面，[香港警務處對男女入職警務人員的要求亦有不同](../Page/香港警務處.md "wikilink")，對女性應徵者採取較寬鬆的體格標準，例如男性應徵者身高須高於163公分，而女性只須高於152公分便可。

## 性別歧視與職場

### 常見案例

  - 對於[女性生理假的不合理限制](../Page/女性.md "wikilink")\[4\]
  - 因其[生理性別對於其專業能力懷抱質疑](../Page/生理性別.md "wikilink")\[5\]\[6\]\[7\]\[8\]
  - 職場[性騷擾](../Page/性騷擾.md "wikilink")\[9\]
  - 對於[懷孕](../Page/懷孕.md "wikilink")\[10\]或育有幼年子女者提出不合理要求\[11\]\[12\]
  - 因其生理性別予以不平等之待遇\[13\]\[14\]\[15\]\[16\]
  - 因其[性傾向予以不平等之待遇](../Page/性傾向.md "wikilink")\[17\]

## 性別歧視與媒體

### 性別歧視與大眾傳媒

許多大眾媒體經常有意或無意使用帶有性別歧視意味的字眼撰寫報導；如此一來，報導中的陳述又多少加深了社會大眾對於性別歧視的態度。\[18\]

### 性别歧视与色情刊物

有些人认为色情刊物是种性别歧视，因为一般来说，在色情刊物以男性读者市场为主，所以女人所扮演的角色被限制在男性读者的「欢娱物品」。

另一些人认为，这纯粹是因为女性不好此物以致以女性读者市场为主的色情刊物欠缺生存空间，随着[BL的风潮](../Page/BL_\(和製英語\).md "wikilink")，认同色情刊物是种性别歧视的人士正遂渐减少。

德国女性主义者Alice Schwarzer是持有这类观点的代表者。她在1970年代一次又一次的提到这个议题，特别是在女性主义杂志，"Emma"
。

另一方面，有些出名的色情演员，如Teresa Orlowski和Timea Vagvoelgyi则公开宣称他们并不觉得自己是性别歧视下的受害者。

## 性別歧視與公眾場所

在許多地區的公眾場所，其設施設置對於性別有一定預設立場，如廁所與育嬰室。\[19\]\[20\]

## 影響

據研究與報導指出，(對於女性的)性別歧視在某些地區造成當地男性難以尋得女性配偶。\[21\]此外，性別歧視也會對企業或組織等團體之營運造成一定影響。\[22\]

## 相关题目

  - [性別歧視性語言](../Page/性別歧視性語言.md "wikilink")
  - [男性贬抑](../Page/男性贬抑.md "wikilink")
  - [女性贬抑](../Page/女性贬抑.md "wikilink")
  - [男性主義](../Page/男性主義.md "wikilink")
  - [女性主義](../Page/女性主義.md "wikilink")
  - [性別角色](../Page/性別角色.md "wikilink")
  - [異性戀主義](../Page/異性戀主義.md "wikilink")
  - [恐同症](../Page/恐同症.md "wikilink")
  - [恐跨症](../Page/恐跨症.md "wikilink")
  - [厭跨女症](../Page/厭跨女症.md "wikilink")
  - [強姦](../Page/強姦.md "wikilink")
  - [墮胎](../Page/墮胎.md "wikilink")
  - [刻板印象](../Page/刻板印象.md "wikilink")
  - [紅顏禍水](../Page/紅顏禍水.md "wikilink")
  - [致命女郎](../Page/致命女郎.md "wikilink")

## 参考文献

## 延伸閱讀

  - Éléonore Pourriat：[《弱勢的多數》](https://womany.net/read/article/4042)
  - 陳滿鴻：[《從社會學角度看性別歧視》](http://archive.hsscol.org.hk/Archive/periodical/spirit/S024g.htm)
  - [你我都可能是受害者或加害者！從科學家的裸女襯衫看 Casual Sexism 的無所不在 | 女人迷
    Womany](https://womany.net/read/article/6270?ref=read1)

[性別歧視](../Category/性別歧視.md "wikilink")
[Category:基于性和性别的偏见](../Category/基于性和性别的偏见.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.

9.
10.
11.

12.

13.
14.

15.
16.

17.
18.

19.

20.

21.

22.