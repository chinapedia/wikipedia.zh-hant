[200111418531M249withM15A2BFA.jpg](https://zh.wikipedia.org/wiki/File:200111418531M249withM15A2BFA.jpg "fig:200111418531M249withM15A2BFA.jpg")的[M249輕機槍](../Page/M249班用自動武器.md "wikilink")\]\]
[MG4.jpg](https://zh.wikipedia.org/wiki/File:MG4.jpg "fig:MG4.jpg")裝備的|[黑克勒-科赫](../Page/黑克勒-科赫.md "wikilink")[HK
MG4](../Page/HK_MG4輕機槍.md "wikilink")。\]\] **輕機槍**（Light machine
gun，簡稱**LMG**）是相較於[通用機槍輕型的一種](../Page/通用機槍.md "wikilink")[机枪](../Page/机枪.md "wikilink")，可以由一個士兵所操作使用，由於輕機槍一般裝備到步兵分隊或步兵班，有些國家軍隊定位為**班用機槍**。

## 歷史

輕機槍是由十九世紀末和二十世紀初的重管[自动步枪發展而來](../Page/自动步枪.md "wikilink")，主要是因為早期的自動步槍使用當時[手動步槍和](../Page/手動槍機.md "wikilink")[中型機槍相同的彈藥](../Page/中型機槍.md "wikilink")，並且有厚重和長大的槍管，適合高精度和較長時間的連射，其重量在5至10[公斤左右](../Page/公斤.md "wikilink")，雖然可以單人攜帶，但在[第一次世界大战中證明只適合支援持](../Page/第一次世界大战.md "wikilink")[沖鋒槍或](../Page/沖鋒槍.md "wikilink")[霰彈槍的隊友突擊](../Page/霰彈槍.md "wikilink")，而且早期的自動步槍外形不符合[人體工學和無](../Page/人因工程学.md "wikilink")[槍口制退器](../Page/砲口制動器.md "wikilink")，所以亦不適合作直接突擊，於是被裝上[兩腳架專責進攻支援或陣地防衛的任務](../Page/兩腳架.md "wikilink")。

而同時在一次大戰時的[路易士機槍](../Page/路易士機槍.md "wikilink")（Lewis machine
gun）等原定被設計成[中型機槍](../Page/中型機槍.md "wikilink")，實踐成功可以陪同輕裝步兵突擊，而產生了把專用的[機槍](../Page/機槍.md "wikilink")（實際上當時是指[重機槍和](../Page/重機槍.md "wikilink")[中型機槍](../Page/中型機槍.md "wikilink")）小型化，使用[彈鏈或大型](../Page/彈鏈.md "wikilink")[彈鼓供彈](../Page/彈鼓.md "wikilink")，並可以安裝在[三腳架上的另一種類型輕機槍](../Page/三腳架.md "wikilink")。第一款成功的轻机枪設計是[丹麥的](../Page/丹麥.md "wikilink")[麥德森輕機槍](../Page/麥德森輕機槍.md "wikilink")。

## 輕機槍定義

所謂輕機槍並不是指一些外觀和構造相似的槍械，這個詞語是指槍的大小和用途上定義。

上文第一種概念的輕機槍現代稱為**輕型支援武器**(Light Support
Weapon)，具有較輕巧和使用普通自動步槍相似的結構而易訓練射手，但一般只可以用[彈匣供彈和不能快速更換槍管](../Page/彈匣.md "wikilink")。第二種現代意義上的輕機槍稱**班用自動武器**(Squad
Automatic
Weapon)，是由一戰時的[中型機槍和](../Page/中型機槍.md "wikilink")[重機槍輕量化而來的](../Page/重機槍.md "wikilink")，因為有專門連發的設計與以[彈鏈](../Page/彈鏈.md "wikilink")，所以威力更大但較難訓練射手。第三種輕機槍便是[通用機槍](../Page/通用機槍.md "wikilink")，有專屬條目介紹。

輕機槍通常使用制式步槍相同的彈藥，由其是中[小口徑步槍彈](../Page/小口徑步槍.md "wikilink")，重量比及[通用機槍輕](../Page/通用機槍.md "wikilink")，一般裝有[槍托](../Page/槍托.md "wikilink")，能[全自動射擊及可提供步槍不能做出的支援用途及持久壓制火力](../Page/自動火器.md "wikilink")，[彈匣](../Page/彈匣.md "wikilink")、[彈鼓或](../Page/彈鼓.md "wikilink")[彈鏈容量由](../Page/彈鏈.md "wikilink")30發以上至200發，附有[兩腳架](../Page/兩腳架.md "wikilink")、重槍管，部分亦可裝在[三腳架或固定支架甚至](../Page/三腳架.md "wikilink")[遙控武器系統](../Page/遙控武器系統.md "wikilink")（RWS）上。

現代輕機槍一般在戰場上作為支援及陣地防衛武器，能夠由單兵攜帶、射擊、佈置等，是個人武器中火力較強的一種。

## 各國輕機槍年份列表

### 1900年—1920年

  - [麥德森輕機槍](../Page/麥德森輕機槍.md "wikilink")（Madsen machine gun）—多种口徑
  - [MG08/15](../Page/MG08/15.md "wikilink")—7.92×57毫米毛瑟
  - [MG 15nA](../Page/MG_15nA.md "wikilink")—7.92×57毫米毛瑟
  - [哈奇开斯M1909轻机枪](../Page/哈奇开斯M1909轻机枪.md "wikilink")—[.30-06](../Page/.30-06春田步槍彈.md "wikilink")
  - [Hotchkiss Mk I](../Page/Hotchkiss_Mk_I.md "wikilink")—[.303
    British](../Page/.303_British.md "wikilink")
  - [路易士機槍](../Page/路易士機槍.md "wikilink")（Lewis machine gun）— .303
    British
  - [Mle 1915（Chauchat）輕機槍型](../Page/紹沙輕機槍.md "wikilink")— 8×50毫米Lebel
  - [白朗寧自動步槍](../Page/白朗寧自動步槍.md "wikilink")— .30-06

### 1920年—1950年

  - [MG42通用機槍](../Page/MG42通用機槍.md "wikilink")—[7.92×57mm毛瑟](../Page/7.92×57mm毛瑟.md "wikilink")
  - [MG34通用機槍](../Page/MG34通用機槍.md "wikilink")—[7.92×57mm毛瑟](../Page/7.92×57mm毛瑟.md "wikilink")

[Bren1.jpg](https://zh.wikipedia.org/wiki/File:Bren1.jpg "fig:Bren1.jpg")\]\]
[Machine_Gun_RPD.jpg](https://zh.wikipedia.org/wiki/File:Machine_Gun_RPD.jpg "fig:Machine_Gun_RPD.jpg")\]\]

  - [大正十一式轻机枪](../Page/大正十一式轻机枪.md "wikilink")—
    [6.5×50毫米有坂](../Page/6.5×50mm有坂子彈.md "wikilink")
  - [白朗寧Wz.
    1928自動步槍](../Page/白朗寧Wz._1928自動步槍.md "wikilink")—[7.92×57毫米](../Page/7.92×57mm毛瑟.md "wikilink")
  - [Solothurn MG30](../Page/Solothurn_MG30.md "wikilink")—7.92×57毫米
  - [FM—24](../Page/FM-24/29.md "wikilink")、[FM-29](../Page/FM-24/29#FM-29.md "wikilink")—7.5×54毫米、7.5×57毫米
  - [ZB vz.26](../Page/ZB26式輕機槍.md "wikilink")—7.92×57毫米
  - [Bren](../Page/布倫輕機槍.md "wikilink")—.303
    British、[7.62×51毫米](../Page/7.62×51mm_NATO.md "wikilink")
  - [MG35/36](../Page/Kg_M/40輕機槍.md "wikilink")—[7.92×57毫米毛瑟子彈](../Page/7.92×57mm毛瑟.md "wikilink")
  - [Kg
    M/40](../Page/Kg_M/40輕機槍.md "wikilink")—[6.5×55毫米瑞典子彈](../Page/6.5×55mm瑞典子彈.md "wikilink")
  - [RPD](../Page/RPD輕機槍.md "wikilink")、[RPDM](../Page/RPD輕機槍#型号.md "wikilink")、[56式](../Page/56式班用機槍.md "wikilink")—
    [7.62×39毫米](../Page/7.62×39mm.md "wikilink")
  - [九六式輕機槍](../Page/九六式輕機槍.md "wikilink")—[6.5×50毫米有坂](../Page/6.5×50mm有坂子彈.md "wikilink")
  - [九九式輕機槍](../Page/九九式輕機槍.md "wikilink")—[7.7×58毫米有坂](../Page/7.7×58mm有坂子彈.md "wikilink")
  - [DP輕機槍](../Page/DP輕機槍.md "wikilink")—[7.62×54mmR](../Page/7.62×54mmR.md "wikilink")

### 1950年—1970年

[Machine_Gun_RPK.jpg](https://zh.wikipedia.org/wiki/File:Machine_Gun_RPK.jpg "fig:Machine_Gun_RPK.jpg")\]\]

  - [RPK、RPKS、RPKM](../Page/RPK輕機槍.md "wikilink")、[81式班用機槍](../Page/81式自动步枪#衍生型號.md "wikilink")—
    7.62×39毫米
  - [FAL50-41](../Page/FN_FAL自動步槍#FAL_50A_&_50B.md "wikilink")—[7.62×51毫米](../Page/7.62×51mm_NATO.md "wikilink")
  - [KK 62](../Page/KK_62.md "wikilink")—7.62×39毫米

### 1970年—1990年

[Soviet_RPK-74.JPEG](https://zh.wikipedia.org/wiki/File:Soviet_RPK-74.JPEG "fig:Soviet_RPK-74.JPEG")\]\]
[M249_FN_MINIMI_DA-SC-85-11586_c1.jpg](https://zh.wikipedia.org/wiki/File:M249_FN_MINIMI_DA-SC-85-11586_c1.jpg "fig:M249_FN_MINIMI_DA-SC-85-11586_c1.jpg")\]\]

  - [FN
    Minimi](../Page/FN_Minimi輕機槍.md "wikilink")—[5.56×45毫米](../Page/5.56×45mm_NATO.md "wikilink")
      - [M249 SAW](../Page/M249班用自動武器.md "wikilink")—5.56×45毫米
      - [大宇K3輕機槍](../Page/大宇K3輕機槍.md "wikilink")— 5.56×45毫米
  - [IMI Negev](../Page/內蓋夫輕機槍.md "wikilink")—5.56×45毫米
  - [T75班用機槍](../Page/T75班用機槍.md "wikilink")— 5.56×45毫米
  - [HK23](../Page/HK23.md "wikilink")—5.56×45毫米
  - [HK13](../Page/HK13.md "wikilink")—5.56×45毫米
  - [HK73](../Page/HK73.md "wikilink")—5.56×45毫米
  - [L86A1 LSW](../Page/SA80突擊步槍.md "wikilink")—5.56×45毫米
  - [RPK-74](../Page/RPK-74輕機槍.md "wikilink")、[RPKS-74](../Page/RPK-74輕機槍#型號.md "wikilink")、RPK-74M—[5.45×39毫米](../Page/5.45×39mm.md "wikilink")
  - [斯太爾AUG LMG](../Page/斯泰爾AUG突擊步槍#衍生型.md "wikilink")—5.56×45毫米
  - [MG36](../Page/HK_G36突擊步槍#衍生型.md "wikilink")—5.56×45毫米裝備[兩腳架](../Page/兩腳架.md "wikilink")、重槍管、100發[C-Mag彈鼓](../Page/C-Mag彈鼓.md "wikilink")（可改用30發[彈匣](../Page/彈匣.md "wikilink")），參見[HK
    G36](../Page/HK_G36突擊步槍.md "wikilink")
  - [Ultimax 100](../Page/Ultimax_100輕機槍.md "wikilink")—5.56×45毫米
  - [CETME](../Page/CETME輕機槍.md "wikilink")—5.56×45毫米
  - [斯通納63/63A輕機槍](../Page/斯通納63武器系統#衍生型.md "wikilink")— 5.56×45毫米
  - [斯通納86輕機槍](../Page/斯通納輕機槍.md "wikilink")— 5.56×45毫米

### 1990年—現在

[Machine_gun_Type95.jpg](https://zh.wikipedia.org/wiki/File:Machine_gun_Type95.jpg "fig:Machine_gun_Type95.jpg")\]\]

  - [T75班用機槍](../Page/T75班用機槍.md "wikilink")—[5.56×45毫米](../Page/5.56×45mm_NATO.md "wikilink")
  - [RPK-201輕機槍](../Page/RPK-201輕機槍.md "wikilink")—5.56×45毫米
  - [RPK-203輕機槍](../Page/RPK-203輕機槍.md "wikilink")—7.62×39毫米
  - [95式班用機槍](../Page/95式班用機槍.md "wikilink")—[5.8×42毫米](../Page/5.8×42mm.md "wikilink")（DBP87式槍彈）
  - [阿瑞斯伯勞鳥5.56輕機槍](../Page/阿瑞斯伯勞鳥5.56輕機槍.md "wikilink")—5.56×45毫米
  - [GatMalite](../Page/GatMalite.md "wikilink")—5.56×45毫米
  - [Kbkm wz. 2003](../Page/Kbkm_wz._2003.md "wikilink")—5.56×45毫米
  - [HK
    MG4](../Page/HK_MG4輕機槍.md "wikilink")—[5.56×45毫米](../Page/5.56×45mm_NATO.md "wikilink")
  - [HK M27 IAR](../Page/HK_M27步兵自動步槍.md "wikilink")—5.56×45毫米
  - [LSAT輕機槍](../Page/LSAT輕機槍.md "wikilink")—塑料殼[埋頭彈藥](../Page/埋頭彈藥.md "wikilink")、[LSAT](../Page/LSAT無殼彈藥.md "wikilink")[无壳弹药](../Page/无壳弹药.md "wikilink")

## 參見

  - [机枪](../Page/机枪.md "wikilink")
  - [通用機槍](../Page/通用機槍.md "wikilink")
  - [中型機槍](../Page/中型機槍.md "wikilink")
  - [重機槍](../Page/重機槍.md "wikilink")
  - [班用自動武器](../Page/班用自動武器.md "wikilink")

## 注释

<div class="references-small">

</div>

## 參考文獻

  - [1](https://www.google.co.uk/patents/US4066000)

[\*](../Category/輕機槍.md "wikilink")