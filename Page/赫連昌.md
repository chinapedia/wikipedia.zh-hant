**赫連昌**（），一名**折**，字**還國**，[十六國時期](../Page/十六國.md "wikilink")[夏國皇帝](../Page/夏_\(十六国\).md "wikilink")，[匈奴](../Page/匈奴.md "wikilink")[鐵弗部人](../Page/鐵弗.md "wikilink")，[赫連勃勃三子](../Page/赫連勃勃.md "wikilink")，赫連勃勃在位時被封**太原公**。

夏[真興六年](../Page/真興.md "wikilink")（424年），赫連勃勃欲廢[太子](../Page/太子.md "wikilink")[赫連璝](../Page/赫連璝.md "wikilink")，改立酒泉公[赫連倫](../Page/赫連倫.md "wikilink")，赫連璝發兵攻殺赫連倫，後赫連昌再襲殺赫連璝平亂，赫連勃勃遂以赫連昌為太子。真興七年（425年）赫連勃勃去世，赫連昌繼位，改元[承光](../Page/承光.md "wikilink")。

承光二年（426年），[北魏大舉攻夏](../Page/北魏.md "wikilink")，克[長安](../Page/長安.md "wikilink")。次年（427年），占領夏國都城[統萬](../Page/統萬城.md "wikilink")（今[內蒙古](../Page/內蒙古.md "wikilink")[烏審旗南白城子](../Page/烏審旗.md "wikilink")），赫連昌逃往上邽（今[甘肅](../Page/甘肅.md "wikilink")[天水](../Page/天水.md "wikilink")）。承光四年（428年），北魏攻上邽，會戰中赫連昌因馬失前蹄墜地而被生擒。

赫連昌被俘後，[北魏太武帝拓跋燾十分禮遇他](../Page/北魏太武帝.md "wikilink")，不僅使其住在西宮，更把皇妹嫁給他，並封會稽公。拓跋燾亦常命赫連昌隨待在側，打獵時二人有時亦單獨並騎，赫連昌素有勇名，因此拓跋燾可說對赫連昌十分信任。北魏[神䴥三年](../Page/神䴥.md "wikilink")（430年）又被封為秦王。

北魏[延和三年](../Page/延和_\(北魏\).md "wikilink")（434年），赫連昌叛魏西逃，途中被抓獲格殺。

## 家庭

### 夫人

  - [赫連昌皇后](../Page/赫連昌皇后.md "wikilink")
  - 北魏[始平公主](../Page/始平公主_\(拓跋嗣\).md "wikilink")，428年娶

### 女儿

  - 赫连氏，嫁[罗提](../Page/罗提.md "wikilink")\[1\]
  - 赫连氏，嫁北魏司徒公、平阳王[长孙翰](../Page/长孙翰.md "wikilink")\[2\]

## 参考资料

[H赫](../Page/category:匈奴人.md "wikilink")

[H赫](../Category/夏国皇帝.md "wikilink")
[Category:赫連姓](../Category/赫連姓.md "wikilink")
[Category:北魏公爵](../Category/北魏公爵.md "wikilink")

1.  《魏书·卷四十四·列传第三十二》：结从子渥，渥子提，并历通显。提从世祖讨赫连昌有功，赐昌女为妻。
2.