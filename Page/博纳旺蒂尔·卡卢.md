**博纳旺蒂尔·卡卢**（****，），[科特迪瓦足球运动员](../Page/科特迪瓦.md "wikilink")，司职前锋，目前效力於[荷甲聯賽球會](../Page/荷甲.md "wikilink")[SC海倫芬](../Page/SC海倫芬.md "wikilink")。

## 生平

### 球會

卡盧之前曾在[荷甲聯賽踢球](../Page/荷甲.md "wikilink")，效力球會是[飛燕諾](../Page/飛燕諾.md "wikilink")，期間曾外借至低組別球會。在飛燕諾期間主要司職翼鋒，但也會客串前鋒。

2003年他轉赴[法甲](../Page/法国足球甲级联赛.md "wikilink")，起先效力[欧塞尔](../Page/欧塞尔足球俱乐部.md "wikilink")，後再轉到[巴黎圣日耳門](../Page/巴黎圣日耳曼足球俱乐部.md "wikilink")。在法甲期間卡盧已轉化成一個前鋒，單在巴黎聖日耳門期間，已在28場聯賽中攻入9球。

2007年曾短暫效力[朗斯](../Page/朗斯足球俱乐部.md "wikilink")，可惜球會隨後隨即因成績差劣而降級，卡盧輾轉下，回到[荷甲效力](../Page/荷甲.md "wikilink")[SC海倫芬](../Page/SC海倫芬.md "wikilink")。

### 國家隊

卡盧早於1997年已代表國家隊出賽世青杯，亦出席過[2006年世界杯](../Page/2006年世界杯.md "wikilink")，惜因與世界級勁旅[阿根廷和](../Page/阿根廷國家足球隊.md "wikilink")[荷蘭同組](../Page/荷蘭國家足球隊.md "wikilink")，而於分組賽出局。

## 個人

卡盧有一個弟弟──[萨洛蒙·卡卢](../Page/萨洛蒙·卡卢.md "wikilink")，也是職業足球員，效力[英超球會](../Page/英超.md "wikilink")[車路士](../Page/車路士.md "wikilink")。

[Category:科特迪瓦足球運動員](../Category/科特迪瓦足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:飛燕諾球員](../Category/飛燕諾球員.md "wikilink")
[Category:歐塞爾球員](../Category/歐塞爾球員.md "wikilink")
[Category:巴黎聖日門球員](../Category/巴黎聖日門球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:米莫薩球員](../Category/米莫薩球員.md "wikilink")
[Category:SBV精英隊球員](../Category/SBV精英隊球員.md "wikilink")
[Category:朗斯球員](../Category/朗斯球員.md "wikilink")
[Category:海倫芬球員](../Category/海倫芬球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:1998年非洲國家盃球員](../Category/1998年非洲國家盃球員.md "wikilink")
[Category:2000年非洲國家盃球員](../Category/2000年非洲國家盃球員.md "wikilink")
[Category:2006年非洲國家盃球員](../Category/2006年非洲國家盃球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")