**Celeron
D**，或者称为**赛扬D**，是属于[Intel](../Page/Intel.md "wikilink")[赛扬处理器家族的一系列桌面处理器](../Page/赛扬.md "wikilink")。

## 状况

自Celeron系列推出以来，不论核心如何随着同期[奔腾处理器而升级换代](../Page/Pentium.md "wikilink")，在名称上从未有任何如[Pentium
II](../Page/Pentium_II.md "wikilink")、[Pentium
III之于](../Page/Pentium_III.md "wikilink")[Pentium的更改](../Page/Pentium.md "wikilink")，全系列桌面处理器一直沿用Celeron标志——直到Celeron
D系列推出，才打破这一惯例。

最早的Celeron
D使用[Prescott核心](../Page/Prescott.md "wikilink")，拥有256KB二级缓存，采用[NetBurst架构](../Page/NetBurst.md "wikilink")，虽然有在Pentium系列上的测试显示，Prescott并不比之前的[Northwood核心有更高的执行效率](../Page/Northwood.md "wikilink")，甚至单位频率性能有所下降，但拜256KB二级缓存所赐，性能较上一代*Northwood*
“赛扬4”有明显提高。在玩家中掀起新一轮的“赛扬热”。

由于Intel此期不成熟的90nm工艺，*Prescott* Celeron D的功耗未能比上一代130nm
Celeron减少，相反，消费者不得不为Celeron
D而好好考虑考虑CPU的散热系统。高功耗的现象直到Intel向Celeron
D引入65nm工艺才有所缓解。

从赛扬D开始，Intel开始向微处理器引入性能指数指标，以一些消费者难以直观了解的数字来区分同族处理器的不同型号。

SIS651 和Intel 845G晶片組不支援

## 不同核心的Celeron D

### Prescott-256

  - 插座 [Socket
    478与](../Page/Socket_478.md "wikilink")[LGA775](../Page/LGA775.md "wikilink")，AGTL+
    133 MHz [FSB](../Page/FSB.md "wikilink") (QDR, FSB 533)
  - 指令集
    [MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，部分产品支持[XD-Bit](../Page/NX-Bit.md "wikilink")，[EM64T](../Page/EM64T.md "wikilink")。
  - 核心电压 (VCore): 1.40 Volt
  - 发布日期：2004-06-24
  - 制程：90 nm
  - 频率：2.13GHz 到 3.33GHz
      - 不带[EM64T支持](../Page/EM64T.md "wikilink")
          - 310: 2.133 GHz
          - 315: 2.266 GHz
          - 320: 2.400 GHz
          - 325: 2.533 GHz
          - 330: 2.667 GHz
          - 335: 2.880 GHz
          - 340: 2.933 GHz
          - 345: 3.066 GHz
          - 350: 3.200 GHz
      - 带有[EM64T支持](../Page/EM64T.md "wikilink")
          - 326: 2.533 GHz
          - 331: 2.667 GHz
          - 336: 2.800 GHz
          - 341: 2.933 GHz
          - 346: 3.067 GHz
          - 351: 3.200 GHz
          - 355: 3.333 GHz

### Cedar Mill-512

  - 插座：[LGA775](../Page/LGA775.md "wikilink")，133 MHz
    [FSB](../Page/FSB.md "wikilink") (QDR, FSB 533)。
  - 指令集
    [MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，支持[XD-Bit](../Page/NX-Bit.md "wikilink")，[EM64T](../Page/EM64T.md "wikilink")。
  - 核心电压 (VCore): 1.3V
  - 制程：65 nm
  - 频率：3.06GHz 到 3.6GHz
      -   - 347: 3.066 GHz
          - 352: 3.200 GHz
          - 356: 3.333 GHz
          - 360: 3.466 GHz
          - 365: 3.600 GHz

## 这个“D”

Intel官方解释说Celeron
D的“D”代表Desktop，即桌面应用。反观移动平台，移动赛扬处理器，Intel也不再仅仅称呼其为“Mobile
Celeron”，而命名为[Celeron M](../Page/Celeron_M.md "wikilink")。
这个D标号与[Pentium D的不同](../Page/Pentium_D.md "wikilink")，Pentium
D为双核心处理器。目前賽揚也推出雙核心，型號為Celeron
E1000/E3000系列，第一顆雙核心賽揚型號為E1200，L2快取為512KB。E3000系列則為45nm、L2為1MB。

## 参见

  - [Celeron D处理器列表](../Page/Celeron_D处理器列表.md "wikilink")
  - [Celeron](../Page/Celeron.md "wikilink")

## 外部链接

[fr:Celeron\#Celeron D](../Page/fr:Celeron#Celeron_D.md "wikilink")

[Category:Intel x86處理器](../Category/Intel_x86處理器.md "wikilink")