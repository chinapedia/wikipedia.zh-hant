**新Monday雜誌**，原名為《Monday雜誌》，現已停刊，是一本獨立創刊的[華文傳媒](../Page/華文傳媒.md "wikilink")，2000年由[楊受成所創辦的](../Page/楊受成.md "wikilink")[英皇集團旗下的](../Page/英皇集團.md "wikilink")[新傳媒集團](../Page/新傳媒集團.md "wikilink")[收購](../Page/收購.md "wikilink")，改名至現在，成為旗下的[娛樂雜誌](../Page/娛樂雜誌.md "wikilink")。《新Monday雜誌》題材與[Face周刊同類](../Page/壹本便利.md "wikilink")，[讀者以](../Page/讀者.md "wikilink")[香港人為主](../Page/香港人.md "wikilink")，主要報導[明星](../Page/明星.md "wikilink")、[電影](../Page/電影.md "wikilink")、[電視劇](../Page/電視.md "wikilink")、[消費](../Page/消費.md "wikilink")[潮流等](../Page/潮流.md "wikilink")[娛樂資訊](../Page/娛樂資訊.md "wikilink")，[創作藝人名人秘史不少](../Page/創作.md "wikilink")[1](http://www.facebook.com/topic.php?uid=12823660517&topic=9602#!/photo.php?pid=30970405&op=1&o=global&view=global&subj=70089410660&id=1024538701)。2016年9月26日，雜誌所屬[新傳媒集團](../Page/新傳媒集團.md "wikilink")10月13日起，逢星期四，《[新假期周刊](../Page/新假期.md "wikilink")》、《[東方新地](../Page/東方新地.md "wikilink")》、《新Monday》、《more》四本雜誌合併一書，消息指集團裁員過百人。\[1\]\[2\]

## 銷量

根據Synovate資料顯示，現時固定的讀者人數已超越250,000人。

## 內容

除《新Monday》本書外，還有女生潮流專書《Honey》，全力發放時裝及美容最新資訊。近年更不定期隨書附送潮流精品。
除書本以外，《新Monday》專屬娛樂網站newmonday.com.hk更有即時新聞、明星wallpapers及獨家專訪等內容。

## 引用資料

## 外部參考

  - [新Monday雜誌](http://www.newmonday.com.hk/index.php?controller=Default&action=Aboutus)

[Category:香港雜誌](../Category/香港雜誌.md "wikilink")
[Category:新傳媒集團](../Category/新傳媒集團.md "wikilink")

1.
2.