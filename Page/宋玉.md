**宋玉**（生卒年不详），[战国后期](../Page/战国.md "wikilink")[楚国辞赋作家](../Page/楚国.md "wikilink")，[鄢城人](../Page/鄢城.md "wikilink")（今[湖北省](../Page/湖北省.md "wikilink")[襄阳](../Page/襄阳.md "wikilink")[宜城市](../Page/宜城市.md "wikilink")）\[1\]。其艺术成就很高，为[屈原之后最杰出的](../Page/屈原.md "wikilink")[楚辞作家](../Page/楚辞.md "wikilink")，后世常将两人合称为“屈宋”。与[潘安](../Page/潘安.md "wikilink")、[兰陵王](../Page/兰陵王.md "wikilink")、[卫玠有中国古代](../Page/卫玠.md "wikilink")[四大美男之称](../Page/四大美男.md "wikilink")。

## 生平事績

关于宋玉的生平事績，据《[史记](../Page/史记.md "wikilink")·屈原贾生列传》载：“屈原既死之后，楚有宋玉、[唐勒](../Page/唐勒.md "wikilink")、[景差之徒者](../Page/景差.md "wikilink")，皆好辞而以赋见称。然皆祖屈原之从容辞令，终莫敢直谏。”《汉书•地理志》：“始楚贤臣屈原被谗放流，作《离骚》诸赋以自伤悼，后有宋玉、唐勒之属慕而述之，皆以显名。”這些零散记述僅知其活动时间在屈原之后，擔任过一些官职，但是对现实的批评不如屈原。

宋玉的作品據《[汉书](../Page/汉书.md "wikilink")·[艺文志](../Page/艺文志.md "wikilink")》记载有十六篇，但篇名不可考。\[2\][王逸](../Page/王逸.md "wikilink")《[楚辞章句](../Page/楚辞章句.md "wikilink")》记载《[九辩](../Page/九辩.md "wikilink")》、《[招魂](../Page/招魂.md "wikilink")》两篇。《[文选](../Page/文选.md "wikilink")》记载有《[风赋](../Page/风赋.md "wikilink")》、《[高唐赋](../Page/高唐赋.md "wikilink")》、《[神女赋](../Page/神女赋.md "wikilink")》、《[登徒子好色赋](../Page/登徒子好色赋.md "wikilink")》、《[对楚王问](../Page/对楚王问.md "wikilink")》五篇。一般认为这七篇的文学价值最高。尽管后来还有题名宋玉赋的作品出现，但是基本屬於伪作且文学价值不高。\[3\]在这七篇中，唯一可以确定《[九辩](../Page/九辩.md "wikilink")》为宋玉作品，《招魂》颇多争议，一般認為是[屈原的作品](../Page/屈原.md "wikilink")，其他五篇文学价值很高，但是真正的作者仍有争论。\[4\]

[宋玉为后人所尊崇](../Page/宋玉.md "wikilink")，在[中国文学史上](../Page/中国文学.md "wikilink")，屈宋并称。\[5\][杜甫於](../Page/杜甫.md "wikilink")《[咏怀古迹五首](../Page/咏怀古迹五首.md "wikilink")--其二》诗中道：*「摇落深知宋玉悲，风流儒雅亦吾师。」*而[鲁迅也於其著作](../Page/鲁迅.md "wikilink")《[汉文学史纲要](../Page/汉文学史纲要.md "wikilink")》中肯定《[九辩](../Page/九辩.md "wikilink")》之文學價值：*「虽驰神逞想不如《[离骚](../Page/离骚.md "wikilink")》，而凄怨之情，实为独绝。」*

## 字號爭議

一些史料記載宋玉字**子淵**，號**鹿溪子**\[6\]。但是由於這些説法來源不明，史學家多以其為後人附會\[7\]。一些現代學者研究認爲宋玉字號可能與其品德及出身有關，不應貿然否定。

## 主要作品

  - 《[九辩](../Page/九辩.md "wikilink")》
  - 《[登徒子好色赋](../Page/登徒子好色赋.md "wikilink")》
  - 《[高唐赋](../Page/高唐赋.md "wikilink")》
  - 《[神女赋](../Page/神女赋.md "wikilink")》

## 参考资料

## 延伸閱讀

  - 谷口洋：〈[试论西汉士人的宋玉情结](http://www.nssd.org/articles/article_read.aspx?id=671220591)〉。

[Category:生年不詳](../Category/生年不詳.md "wikilink")
[Category:卒年不詳](../Category/卒年不詳.md "wikilink")
[Category:春秋战国作家](../Category/春秋战国作家.md "wikilink")
[Category:戰國人](../Category/戰國人.md "wikilink")
[Category:中国诗人](../Category/中国诗人.md "wikilink")
[Category:宜城人](../Category/宜城人.md "wikilink")
[Category:楚國人](../Category/楚國人.md "wikilink")
[Y](../Category/宋姓.md "wikilink")

1.  《[水经注](../Page/水经注.md "wikilink")·沔水篇》记载：“宜城县南有宋玉宅。玉，邑人，隽才辩给，善属文而识音。”《安陆府志》载清李棠馥《重修宋玉井碑记》云：“郢学宫为楚大夫宋玉故第。去泮水数武，有泉冷然，相传为宋玉井云。”民国《钟祥县志·古迹》载：“宋玉宅在兰台之左，相传郢学宫即其遗址。”
2.  《汉书·艺文志·诗赋略》载：“宋玉赋十六篇。”
3.  署名宋玉的作品有十七篇：除了前面《[楚辞章句](../Page/楚辞章句.md "wikilink")》、《[文选](../Page/文选.md "wikilink")》所列的七篇之外，唐人[章樵](../Page/章樵.md "wikilink")《古文苑》錄有《笛赋》、《大言赋》、《小言赋》、《讽赋》、《钓赋》、《舞赋》
    6篇；南宋[陈仁子](../Page/陈仁子.md "wikilink")《文选补遗》录《微咏赋》1篇，明朝[刘节](../Page/刘节.md "wikilink")《广文选》錄有《高唐对》、《微咏赋》、《郢中对》
    3篇。[梅鼎祚](../Page/梅鼎祚.md "wikilink")《文纪》收有《报友人书》1篇。
4.  清人[崔述](../Page/崔述.md "wikilink")《笔乘》卷三对《文选》等所载宋玉赋首先怀疑。[陆侃如先后作](../Page/陆侃如.md "wikilink")《宋玉赋考》和《宋玉评传》認為《九辩》与《招魂》是宋玉作品，其他皆是伪作。[刘大白的](../Page/刘大白.md "wikilink")《宋玉赋辨伪》亦認為只有《九辩》及《招魂》是真正宋玉作品。1986年袁梅的《宋玉辞赋今读》认定除《九辩》之外，其他全部为伪作。
5.  [刘勰](../Page/刘勰.md "wikilink")《[文心雕龙](../Page/文心雕龙.md "wikilink")·辨骚》说：“屈宋逸步，莫之能追……是以枚贾追风以入丽，马扬沿波而得奇，其衣被词人，非一代也。”
6.
7.