**CNET**是[美國](../Page/美國.md "wikilink")[三藩市一間媒體公司](../Page/三藩市.md "wikilink")，集中報導科技新聞，1993年由及創辦。早年曾營辦[電視節目](../Page/電視節目.md "wikilink")，原打算成立[有線電視頻道](../Page/有線電視.md "wikilink")。後來[互聯網普及](../Page/互聯網.md "wikilink")，該公司亦主要在網上運作，曾在納斯達克上市，目前在全球12個國家有辦事處。其中以報道[科技](../Page/科技.md "wikilink")[新聞為主的](../Page/新聞.md "wikilink")、[Cnet.com以及提供](../Page/CNET.md "wikilink")[軟件](../Page/軟件.md "wikilink")[下載的](../Page/下載.md "wikilink")[Download.com較為人所識](../Page/Download.com.md "wikilink")。

2008年，[CBS宣佈以](../Page/哥倫比亞廣播公司.md "wikilink")18亿[美元收購CNET公司](../Page/美元.md "wikilink")。\[1\]

## 參考文獻

## 外部連結

  - [CNET global corporate site](http://www.cnetnetworks.com/)

  - [CNET UK corporate site](http://www.cnetnetworks.co.uk/)

  -
  - [CNET.co.uk](http://www.cnet.co.uk/)

  - [Transcripts of CNET Network's Quarterly Conference
    Calls](http://seekingalpha.com/transcripts/for/cnet)

[CNET](../Category/CNET.md "wikilink")
[Category:美國網際網路公司](../Category/美國網際網路公司.md "wikilink")
[Category:1993年成立的公司](../Category/1993年成立的公司.md "wikilink")
[Category:新聞網站](../Category/新聞網站.md "wikilink")

1.