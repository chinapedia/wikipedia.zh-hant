《**哥德尔、埃舍尔、巴赫：集異璧之大成**》（*Gödel, Escher, Bach: an Eternal Golden
Braid*），是一本贏得[普立茲獎的書](../Page/普立茲獎.md "wikilink")。它是[侯世達的著作](../Page/侯世達.md "wikilink")，由Basic
Books出版社在1979年出版的。這本書的二十周年版本在1999年發行，而且由侯世達加上新的前言。《集異璧之大成》（ISBN
7-100-01323-2）是[商务印书馆在](../Page/商务印书馆.md "wikilink")1996年出版的根据1995年英文版翻译的中文版。本书的英文副标题意译为“一条永恒的金带”，其首字母与哥德尔、埃舍尔、巴赫三人的[英文名字首字母GEB相同](../Page/英文.md "wikilink")，而商务印书馆中文译本的副标题中的“集異璧”则与GEB谐音。
本书一共有两篇，上篇译为“集异璧 GEB”，下篇译为“异集璧 EGB”。
本書主要講述了[邏輯學家](../Page/邏輯學.md "wikilink")[哥德尔](../Page/哥德尔.md "wikilink")，[藝術家](../Page/藝術.md "wikilink")[埃舍尔](../Page/埃舍尔.md "wikilink")，和作曲家[巴赫的创造性的成就怎樣交織在一起](../Page/巴赫.md "wikilink")。正如作者所說：“我認識到，哥德尔、埃舍尔和巴赫只是用不同的方式來表達一样相同的本質。我嘗試重現這种本質而寫出這本書。”

此書在深层次上并非研究這三個人。那只不過是通往該書中心主題的其中一條路——侯世達在序言指出：“到底文字和思想是否依從俱形式的規則？這正是此書的中心問題。”（Do
words and thoughts follow formal rules, or do they not? That problem is
the problem of this book.）

## 參見

  - [中文房间](../Page/中文房间.md "wikilink")
  - [邱奇－图灵论题](../Page/邱奇－图灵论题.md "wikilink")
  - [考拉兹猜想](../Page/考拉兹猜想.md "wikilink")
  - [分形](../Page/分形.md "wikilink")
  - [同构](../Page/同构.md "wikilink")
  - [后设](../Page/后设.md "wikilink")

## 外部链接

  - [麻省理工课](http://ocw.mit.edu/high-school/humanities-and-social-sciences/godel-escher-bach/)

[Category:電腦書籍](../Category/電腦書籍.md "wikilink")
[Category:數學書籍](../Category/數學書籍.md "wikilink")
[Category:哲学书籍](../Category/哲学书籍.md "wikilink")
[Category:普利茲獎獲獎作品](../Category/普利茲獎獲獎作品.md "wikilink")
[Category:1979年书籍](../Category/1979年书籍.md "wikilink")