**泉野線**（）是[日本](../Page/日本.md "wikilink")[相模鐵道的鐵路線](../Page/相模鐵道.md "wikilink")，連結[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")[旭區](../Page/旭區_\(橫濱市\).md "wikilink")[二俁川站與神奈川縣](../Page/二俁川站.md "wikilink")[藤澤市](../Page/藤澤市.md "wikilink")[湘南台站](../Page/湘南台站.md "wikilink")。車站編號使用的路線記號為**SO**。

此外，此路線已經取得延伸至[平塚市的許可](../Page/平塚市.md "wikilink")，然而仍未有延伸前景（詳細參見[後述](../Page/#往平塚方向的延伸計劃.md "wikilink")）。

## 路線資料

  - 路線距離：11.3公里
  - [軌距](../Page/軌距.md "wikilink")：1067毫米
  - 站數：8個（包括起終點站）
  - 複線路段：全線
  - 電氣化路段：全線（直流1500V）
  - 最高速度：100公里/小時（南萬騎原→綠園都市）
  - 保安裝置：[ATS-P](../Page/自動列車停止裝置#ATS-P形（デジタル伝送パターン形）.md "wikilink")
  - 數碼列車無線頻道：5ch

## 概要

泉野線由二俁川站開始即通過丘陵地帶，[隧道很多是其特徵](../Page/隧道.md "wikilink")，南萬騎原站至彌生台站前後皆為隧道。泉野站開始變成南向的高架鐵路，經過夢丘站後與[橫濱市營地下鐵藍線合流](../Page/橫濱市營地下鐵藍線.md "wikilink")，於地下行走進入湘南台站。

## 運行形態

大部份列車於二俁川站通過[相鐵本線直通到達](../Page/相鐵本線.md "wikilink")[橫濱站](../Page/橫濱站.md "wikilink")。列車種別分為[各停](../Page/各站停車.md "wikilink")，[快速和](../Page/快速列車.md "wikilink")[特急](../Page/特別急行列車.md "wikilink")。

## 历史

  - 1976年4月8日 二俁川 - 泉野間開業。
  - 1990年4月4日 泉野 - 泉中央間開業。
  - 1999年3月10日 泉中央 - 湘南台間開業。

## 女性専用車

  - 平日早上7：30～9：30到達橫濱站的全列車第4卡（實施區間為全區間）
  - 平日黃昏18：00以後橫濱站發車的下行全列車第4卡（實施區間為全區間）

{{-}}

## 車站列表

  - 車站編號自2014年2月下旬起依次引入\[1\]。
  - 所有車站均位於[神奈川縣](../Page/神奈川縣.md "wikilink")。
  - 各停、快速於泉野線內停靠所有車站。

<table>
<thead>
<tr class="header">
<th><p>車站編號</p></th>
<th><p>中文站名</p></th>
<th><p>日文站名</p></th>
<th><p>英文站名</p></th>
<th><p>站間距離</p></th>
<th><p>累計距離</p></th>
<th><p>特急</p></th>
<th><p>接續路線</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>二俁川起計</p></td>
<td><p>橫濱起計</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>SO10</p></td>
<td><p><a href="../Page/二俁川站.md" title="wikilink">二俁川</a></p></td>
<td></td>
<td></td>
<td><p>-</p></td>
<td><p>0.0</p></td>
<td><p>10.5</p></td>
<td><p>●</p></td>
<td><p><a href="../Page/相模鐵道.md" title="wikilink">相模鐵道</a>： <strong><a href="../Page/相鐵本線.md" title="wikilink">本線</a>（直運運行至<a href="../Page/橫濱站.md" title="wikilink">橫濱方向</a>）</strong></p></td>
</tr>
<tr class="odd">
<td><p>SO31</p></td>
<td><p><a href="../Page/南萬騎原站.md" title="wikilink">南萬騎原</a></p></td>
<td></td>
<td></td>
<td><p>1.6</p></td>
<td><p>1.6</p></td>
<td><p>12.1</p></td>
<td><p>｜</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>SO32</p></td>
<td><p><a href="../Page/綠園都市站.md" title="wikilink">綠園都市</a></p></td>
<td></td>
<td></td>
<td><p>1.5</p></td>
<td><p>3.1</p></td>
<td><p>13.6</p></td>
<td><p>｜</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>SO33</p></td>
<td><p><a href="../Page/彌生台站.md" title="wikilink">彌生台</a></p></td>
<td></td>
<td></td>
<td><p>1.8</p></td>
<td><p>4.9</p></td>
<td><p>15.4</p></td>
<td><p>｜</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>SO34</p></td>
<td><p><a href="../Page/泉野站.md" title="wikilink">泉野</a></p></td>
<td></td>
<td></td>
<td><p>1.1</p></td>
<td><p>6.0</p></td>
<td><p>16.5</p></td>
<td><p>●</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>SO35</p></td>
<td><p><a href="../Page/泉中央站_(神奈川縣).md" title="wikilink">泉中央</a></p></td>
<td></td>
<td></td>
<td><p>2.2</p></td>
<td><p>8.2</p></td>
<td><p>18.7</p></td>
<td><p>｜</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>SO36</p></td>
<td><p><a href="../Page/夢丘站.md" title="wikilink">夢丘</a></p></td>
<td></td>
<td></td>
<td><p>1.1</p></td>
<td><p>9.3</p></td>
<td><p>19.8</p></td>
<td><p>｜</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>SO37</p></td>
<td><p><a href="../Page/湘南台站.md" title="wikilink">湘南台</a></p></td>
<td></td>
<td></td>
<td><p>2.0</p></td>
<td><p>11.3</p></td>
<td><p>21.8</p></td>
<td><p>●</p></td>
<td><p><a href="../Page/小田急電鐵.md" title="wikilink">小田急電鐵</a>： <a href="../Page/江之島線_(小田急電鐵).md" title="wikilink">江之島線</a>（OE09）<br />
<a href="../Page/橫濱市營地下鐵.md" title="wikilink">橫濱市營地下鐵</a>： <a href="../Page/橫濱市營地下鐵藍線.md" title="wikilink">藍線</a>（B01）</p></td>
</tr>
</tbody>
</table>

## 车辆

  - [相铁10000系电力动车组](../Page/相铁10000系电力动车组.md "wikilink")
  - [相铁11000系电力动车组](../Page/相铁11000系电力动车组.md "wikilink")

## 參考文獻

  -
<!-- end list -->

  -

## 相關條目

  - [日本鐵路線列表](../Page/日本鐵路線列表.md "wikilink")

## 參考資料

[Category:關東地方鐵路路線](../Category/關東地方鐵路路線.md "wikilink")
[線](../Category/相模鐵道.md "wikilink")
[Category:1976年啟用的鐵路線](../Category/1976年啟用的鐵路線.md "wikilink")

1.   - 相模鉄道、2014年2月25日。