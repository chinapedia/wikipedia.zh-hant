**Hello\! Pro研修生**（****）舊稱Hello\! Pro
EGG\[1\]（）。[早安家族的成員](../Page/早安家族.md "wikilink")，為主要成員以外的「研修生」。

目前Hello\! Pro研修生共15人；Hello\! Pro研修生北海道共6人。
在[早安家族冬](../Page/早安家族.md "wikilink")、夏演唱會，或者各團體的演唱會中擔任伴舞。

以「正式出道」為目標，進行一連串歌舞表演的研習及訓練。

## 成員

### 現在活動的成員

  - 以加入的時間先後做為區分，並非官方排定之期數。

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>加入日期</p></th>
<th><p>姓名</p></th>
<th><p>平假名</p></th>
<th><p>出身地</p></th>
<th><p>生日</p></th>
<th><p>年齡</p></th>
<th><p>身高[2]</p></th>
<th><p>血型</p></th>
<th><p>暱稱</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>25期</p></td>
<td></td>
<td><p>小野琴己</p></td>
<td><p>おの ことみ</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td><p>164</p></td>
<td><p>B</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>米村姫良々</p></td>
<td><p>よねむら きらら</p></td>
<td><p>愛知縣</p></td>
<td></td>
<td></td>
<td></td>
<td><p>A</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>26期</p></td>
<td></td>
<td><p>橋迫 鈴</p></td>
<td><p>はしさこ りん</p></td>
<td><p>愛知縣</p></td>
<td></td>
<td></td>
<td><p>147</p></td>
<td><p>O</p></td>
<td></td>
<td><p>2017實力公開診斷個性賞</p></td>
</tr>
<tr class="even">
<td><p>27期</p></td>
<td></td>
<td><p>松永 里愛</p></td>
<td><p>まつなが りあい</p></td>
<td><p>大阪府</p></td>
<td></td>
<td></td>
<td><p>150</p></td>
<td><p>不明</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山田 苺</p></td>
<td><p>やまだ いちご</p></td>
<td><p>静岡縣</p></td>
<td></td>
<td></td>
<td><p>156.3</p></td>
<td><p>O</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中山 夏月姫</p></td>
<td><p>なかやま なつめ</p></td>
<td><p>石川縣</p></td>
<td></td>
<td></td>
<td><p>157</p></td>
<td><p>A</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>28期</p></td>
<td></td>
<td><p>為永 幸音</p></td>
<td><p>ためなが しおん</p></td>
<td><p>長野縣</p></td>
<td></td>
<td></td>
<td></td>
<td><p>不明</p></td>
<td></td>
<td><p>前地方偶像</p></td>
</tr>
<tr class="even">
<td><p>窪田 七海</p></td>
<td><p>くぼた ななみ</p></td>
<td><p>千葉縣</p></td>
<td></td>
<td></td>
<td></td>
<td><p>A</p></td>
<td></td>
<td><p>前地方偶像</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>出頭 杏奈</p></td>
<td><p>しゅっとう あんな</p></td>
<td><p>千葉縣</p></td>
<td></td>
<td></td>
<td></td>
<td><p>O</p></td>
<td></td>
<td><p>前讀者模特兒</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>金光 留々</p></td>
<td><p>かねみつ るる</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td></td>
<td><p>O</p></td>
<td></td>
<td><p>前舞台劇演員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>松原 ユリヤ</p></td>
<td><p>まつばら ゆりや</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td></td>
<td><p>A</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>29期</p></td>
<td></td>
<td><p>斉藤 円香</p></td>
<td><p>さいとう まどか</p></td>
<td><p>埼玉縣</p></td>
<td></td>
<td></td>
<td></td>
<td><p>O</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小野田 華凛</p></td>
<td><p>おのだ かりん</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td></td>
<td><p>不明</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### Hello\!Pro研修生北海道

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>加入日期</p></th>
<th><p>姓名</p></th>
<th><p>平假名</p></th>
<th><p>出身地</p></th>
<th><p>生日</p></th>
<th><p>年齡</p></th>
<th><p>血型</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1期</p></td>
<td><p>2016年7月</p></td>
<td><p>佐藤光</p></td>
<td><p>さとう ひかり</p></td>
<td></td>
<td></td>
<td><p>AB型</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>石栗奏美</p></td>
<td><p>いしぐり かなみ</p></td>
<td></td>
<td></td>
<td><p>A型</p></td>
<td><p>2018實力公開診斷審查員賞</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>河野みのり</p></td>
<td><p>かわの みのり</p></td>
<td></td>
<td></td>
<td><p>A型</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>工藤由愛</p></td>
<td><p>くどう ゆめ</p></td>
<td></td>
<td></td>
<td><p>A型</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山﨑愛生</p></td>
<td><p>やまざき めい</p></td>
<td></td>
<td></td>
<td><p>B型</p></td>
<td><p>2018實力公開診斷歌唱賞</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### BEYOOOOONDS

  - 2018年6月11日宣布兩團的新成員，並於7月14日的H\!P夏控公開亮相。
      - 正式出道日期尚未決定。

<table>
<thead>
<tr class="header">
<th><p>加入日期</p></th>
<th><p>姓名</p></th>
<th><p>平假名</p></th>
<th><p>出身地</p></th>
<th><p>生日</p></th>
<th><p>年齡</p></th>
<th><p>身高</p></th>
<th><p>血型</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2017年5月</p></td>
<td><p>一岡伶奈</p></td>
<td><p>いちおか れいな</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td><p>162</p></td>
<td><p>B</p></td>
<td><p>早安11期最終合宿選拔成員之一，後加入成為17期研修生。 2015年實力公開診斷舞蹈賞。</p>
<ul>
<li>隊長。</li>
</ul></td>
</tr>
<tr class="even">
<td><p>2018年6月11日</p></td>
<td><p>島倉 りか</p></td>
<td><p>しまくら りか</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td><p>153.7</p></td>
<td><p>A</p></td>
<td><p>2018實力公開診斷最佳演出賞。27期研修生。</p></td>
</tr>
<tr class="odd">
<td><p>西田 汐里</p></td>
<td><p>にしだ しおり</p></td>
<td><p>京都府</p></td>
<td></td>
<td></td>
<td><p>149</p></td>
<td><p>B</p></td>
<td><p>2018實力公開診斷個性賞。26期研修生。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>江口 紗耶</p></td>
<td><p>えぐち さや</p></td>
<td><p>兵庫縣</p></td>
<td></td>
<td></td>
<td><p>159</p></td>
<td><p>O</p></td>
<td><p>27期研修生</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>加入日期</p></th>
<th><p>姓名</p></th>
<th><p>平假名</p></th>
<th><p>出身地</p></th>
<th><p>生日</p></th>
<th><p>年齡</p></th>
<th><p>身高</p></th>
<th><p>血型</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2017年5月</p></td>
<td><p>高瀬くるみ</p></td>
<td><p>たかせ くるみ</p></td>
<td><p>栃木縣</p></td>
<td></td>
<td></td>
<td><p>150</p></td>
<td><p>A</p></td>
<td><p>曾為栃木縣地方團體「とちおとめ25」的成員。 2016實力公開診斷歌唱賞。23期研修生。</p></td>
</tr>
<tr class="even">
<td><p>清野桃々姫</p></td>
<td><p>きよの ももひめ</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td></td>
<td><p>A</p></td>
<td><p>2016實力公開診斷最佳演出賞。25期研修生。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年6月11日</p></td>
<td><p>前田こころ</p></td>
<td><p>まえだ こころ</p></td>
<td><p>埼玉縣</p></td>
<td></td>
<td></td>
<td><p>161</p></td>
<td><p>A</p></td>
<td><p>2017、2018實力公開診斷舞蹈賞。24期研修生。</p></td>
</tr>
<tr class="even">
<td><p>山﨑 夢羽</p></td>
<td><p>やまざき ゆはね</p></td>
<td><p>愛知縣</p></td>
<td></td>
<td></td>
<td><p>158</p></td>
<td><p>AB</p></td>
<td><p>2017實力公開診斷歌唱賞。26期研修生。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岡村 美波</p></td>
<td><p>おかむら みなみ</p></td>
<td><p>大阪府</p></td>
<td></td>
<td></td>
<td><p>156</p></td>
<td><p>O</p></td>
<td><p>27期研修生。</p></td>
<td></td>
</tr>
</tbody>
</table>

### 過去的成員(未升格為H\!P正式成員者)

**1期生（2004年）**

  -
  - [川島幸](../Page/川島幸.md "wikilink")：1986年8月26日生。2005年9月14日脫離。

  - [橋田三令](../Page/橋田三令.md "wikilink")：1992年4月28日生、千葉縣。2007年6月10日活動終止；2008年8月重新開始演藝活動。

  - [田中杏里](../Page/田中杏里.md "wikilink")：1991年4月25日生、埼玉縣。2006年7月畢業，2012年參加埼玉當地偶像甄選會合格。

  - [湯德步美](../Page/湯德步美.md "wikilink")：1993年10月19日生、東京都。2007年11月30日發表引退。

  - [武藤水華](../Page/武藤水華.md "wikilink")：1992年1月31日生、埼玉縣。2008年4月30日發表引退。

  - [青木英里奈](../Page/青木英里奈.md "wikilink")：1990年4月16日生、埼玉縣。2009年1月畢業。

  - [澤田由梨](../Page/澤田由梨.md "wikilink")（音樂GATAS）：1991年11月25日生、千葉縣。2009年8月30日畢業。

  - [能登有沙](../Page/能登有沙.md "wikilink")（音樂GATAS）：1988年12月26日生、千葉縣。2009年9月23日畢業，現為聲優、歌手活動中。

  - [西念未彩](../Page/西念未彩.md "wikilink")：1993年6月22日生、東京都。2010年6月畢業。

  - [前田彩里](../Page/前田彩里.md "wikilink")：1997年5月7日生、兵庫縣。2010年10月畢業，2011年加入『Nゼロ』（元AKBN
    0）第五期，2012年退出進行solo活動。

  - [岡井明日菜](../Page/岡井明日菜.md "wikilink")：1996年5月14日生、埼玉縣。是[℃-ute成員](../Page/℃-ute.md "wikilink")[岡井千聖的妹妹](../Page/岡井千聖.md "wikilink")。2010年11月28日畢業。

  - [北原沙彌香](../Page/北原沙彌香.md "wikilink")：1993年11月29日生、埼玉縣。2011年3月9日畢業，現solo活動中。

**2期生（2007年）**

  - [小倉愛實](../Page/小倉愛實.md "wikilink")：1991年7月13日生、東京都。
  - [古峰桃香](../Page/古峰桃香.md "wikilink")：1994年6月27日生、東京都。2010年6月畢業。

**3期生（2007年4月）**

  - [吉川友](../Page/吉川友.md "wikilink")：1992年5月1日生、茨城縣。2010年12月24日畢業，並成為「Hello\!
    Project 2011 WINTER 〜歓迎新鮮まつり〜」的暖場嘉賓。現為個人歌手。

**4期生（2008年3月）**

  - [金子りえ](../Page/金子りえ.md "wikilink")：1997年7月1日生、神奈川縣。身高158cm、A型。在前輩們都畢業後成為最資深者，因而自稱是研修生隊長\[3\]。2013年12月21日畢業，2014年5月表示將準備出國留學。

**6期生（2009年4月）**

  - ：1990年4月16日生。

**7期生（2009年6月）**

  - [平野智美](../Page/平野智美.md "wikilink")：1984年1月27日生。2010年11月28日畢業。

**9期生（2010年2月）**

  - [田辺奈菜美](../Page/田辺奈菜美.md "wikilink")：1999年11月10日生、神奈川縣。身高162cm、A型。「しゅごキャラエッグ！」2期，曾獲得2013年實力公開診斷第一名。2014年11月畢業。

**10期生（2010年3月）**

  - [木沢瑠那](../Page/木沢瑠那.md "wikilink")：1996年9月6日生。2010年11月28日畢業，為團體『スマイル学園』2012新入學生。
  - [長澤和奏](../Page/長澤和奏.md "wikilink")：1996年11月8日生、岩手縣。2011年9月11日畢業，現為團體『さくらガールズ』成員活動中。

**11期（2011年2月）**

  - [吉橋くるみ](../Page/吉橋くるみ.md "wikilink")：1999年9月22日生、千葉縣。身高156cm、A型。曾獲2014年實力公開診斷舞蹈特別賞。2014年11月研修活動結束。

**12期（2011年7月）**

  - [茂木美奈実](../Page/茂木美奈実.md "wikilink")：1999年1月14日生。2013年2月研修活動結束。
  - [大塚愛菜](../Page/大塚愛菜.md "wikilink")：1998年4月3日生。2013年2月宣布組成[Juice=Juice](../Page/Juice=Juice.md "wikilink")；同年7月5日（出道前）官網發表退出聲明，因此未歸入H\!P正式成員。

**13期（2011年9月）**

  - [山賀香菜恵](../Page/山賀香菜恵.md "wikilink")：1995年5月15日生。2012年8月研修活動結束。

**14期（2012年2月）**

  - [小數賀芙由香](../Page/小數賀芙由香.md "wikilink")：1997年11月19日生、神奈川縣。身高162cm、O型。2011年8月14日成為[S/mileage準成員](../Page/S/mileage.md "wikilink")，因病休養而退出，後加入研修生。2014年5月研修活動結束。

**15期（2012年3月）**

  - [岡村里星](../Page/岡村里星.md "wikilink")：1998年7月28日生。2013年6月研修活動結束。

**19期（2013年5月）**

  - [真城佳奈](../Page/真城佳奈.md "wikilink")：1999年8月29日生。2013年10月研修活動結束。
  - [田中可恋](../Page/田中可恋.md "wikilink")：1997年12月24日生。2014年9月研修活動結束。
  - [三瓶海南](../Page/三瓶海南.md "wikilink")：1998年8月28日生。2015年3月研修活動結束。
  - [井上ひかる](../Page/井上ひかる.md "wikilink")：2000年8月31日生、三重縣，2018年5月研修活動結束。

**20期（2013年9月）**

  - [大浦央菜](../Page/大浦央菜.md "wikilink")：2000年9月10日生、東京都。身高144cm（當時）、A型。為早安12期「未來少女」最終合宿選拔成員。2014年9月研修活動結束。
  - [橫川夢衣](../Page/橫川夢衣.md "wikilink")：1999年3月14日生、京都府。身高156cm（當時）、A型。為早安12期「未來少女」最終合宿選拔成員。2016年9月研修活動結束。

**21期（2014年5月）**

  - [齊藤夏奈](../Page/齊藤夏奈.md "wikilink")：2001年8月26日生、埼玉縣，身高148cm（當時）。2015年5月研修活動結束。
  - [竹村未羽](../Page/竹村未羽.md "wikilink"): 2001年4月28日生、岐阜縣、2015年3月研修活動結束。

**22期（2014年11月）**

  - [橋本渚](../Page/橋本渚.md "wikilink")：1997年7月3日生、神奈川縣，身高151cm、AB型。原Nice
    Girl Trainee。2016年9月研修活動結束。
  - [島野萌々子](../Page/島野萌々子.md "wikilink")：1999年5月31日、埼玉縣，身高165cm、O型。原Nice
    Girl Trainee。2016年9月研修活動結束。
  - [堀江葵月](../Page/堀江葵月.md "wikilink")：1998年5月14日生、東京都，身高155cm，曾獲得2015年實力公開診斷個性賞、2017實力公開診斷舞蹈賞。原Nice
    Girl Trainee。2018年9月研修活動結束。

**24期（2015年4月）**

  - [岡本帆乃花](../Page/岡本帆乃花.md "wikilink")：2004年2月28日生、愛知縣，身高147cm（當時）。2015年11月研修活動結束。
  - [仲野りおん](../Page/仲野りおん.md "wikilink"):2001年9月5日生、埼玉縣，身高145cm（當時）。2016年9月研修活動結束。
  - [金津美月](../Page/金津美月.md "wikilink")：2002年10月9日生、東京都，身高150cm、O型。2018年9月研修活動結束。

**25期（2016年1月）**

  - [野口胡桃](../Page/野口胡桃.md "wikilink")：2001年8月29日生、大阪府，2018年11月研修活動結束。
  - [児玉咲子](../Page/児玉咲子.md "wikilink")：2003年3月29日生、三重縣，2019年1月研修活動結束。

**26期（2016年9月）**

  - [吉田眞理恵](../Page/吉田眞理恵.md "wikilink")：2003年4月29日生、東京都，2017年3月研修活動結束。

**27期（2017年3月）**

  - [日比麻里那](../Page/日比麻里那.md "wikilink")：2002年12月17日生、大阪府，2018年11月研修活動結束。
  - [土居麗菜](../Page/土居麗菜.md "wikilink")：2004年9月1日生、大阪府，2018年11月研修活動結束。

**28期（2017年12月）**

  - [後藤咲香](../Page/後藤咲香.md "wikilink")：2003年8月31日生、大分縣，2017年12月研修活動結束。
  - [橋本桃呼](../Page/橋本桃呼.md "wikilink")：2003年6月28日生、山口縣，2018年4月研修活動結束。

**29期（2018年11月）**

  - [楠萌生](../Page/楠萌生.md "wikilink")：2004年11月10日生、大阪府，2019年3月研修活動結束。

**Hello\! Pro研修生北海道 1期**

  - [北川亮](../Page/北川亮.md "wikilink")：2004年6月29日生、北海道，2018年4月研修活動結束。

### 成為Hello\! Project的正式成員

  - [岡田唯](../Page/岡田唯.md "wikilink")，加入[美勇傳](../Page/美勇傳.md "wikilink")，而美勇傳於2008年解散，岡田亦於2009年3月31日從Hello
    Project\!畢業。
  - [有原栞菜](../Page/有原栞菜.md "wikilink")，加入[℃-ute](../Page/℃-ute.md "wikilink")，於2009年7月9日從Hello\!
    Project及℃-ute中脱退。
  - [是永美記](../Page/是永美記.md "wikilink")，加入音樂GATAS。
  - [真野惠里菜](../Page/真野惠里菜.md "wikilink")，以個人名義出演，2013年2月23日自Hello\!
    Project畢業，現為女演員。
  - [早安少女組。](../Page/早安少女組。.md "wikilink")
      - [錢琳](../Page/錢琳.md "wikilink")，8期留學生，2010年12月15日從Hello\!
        Project及早安少女組。畢業。
      - [譜久村聖](../Page/譜久村聖.md "wikilink")，9期，現為早安少女組。第九代隊長。
      - [工藤遥](../Page/工藤遥.md "wikilink")，10期。
      - [小田櫻](../Page/小田櫻.md "wikilink")，11期。
      - [牧野真莉愛](../Page/牧野真莉愛.md "wikilink")，12期。
      - [羽賀朱音](../Page/羽賀朱音.md "wikilink")，12期。
      - [加賀楓](../Page/加賀楓.md "wikilink")，13期。
      - [橫山玲奈](../Page/橫山玲奈.md "wikilink")，13期。
  - S/mileage (現稱：[ANGERME](../Page/ANGERME.md "wikilink"))
      - [和田彩花](../Page/和田彩花.md "wikilink")、[前田憂佳](../Page/前田憂佳.md "wikilink")、[福田花音](../Page/福田花音.md "wikilink")、[小川紗季](../Page/小川紗季.md "wikilink")，1期。
          - 4人於2010年4月3日正式出道。
          - 小川紗季2011年8月27日從Hello\! Project及S/mileage畢業。
          - 前田憂佳2011年12月31日從Hello\! Project及S/mileage畢業。
          - 福田花音2015年11月29日從Hello\! Project及ANGERME畢業。
      - [竹内朱莉](../Page/竹内朱莉.md "wikilink")、[勝田-{里}-奈](../Page/勝田里奈.md "wikilink")，2期。
          - 2人於2011年8月14日加入。
      - [室田瑞希](../Page/室田瑞希.md "wikilink")、[相川茉穂](../Page/相川茉穂.md "wikilink")、[佐佐木莉佳子](../Page/佐佐木莉佳子.md "wikilink")，3期。
          - 3人於2014年10月4日加入。
      - [笠原桃奈](../Page/笠原桃奈.md "wikilink")，5期。
          - 於2016年7月16日加入。
      - [船木結](../Page/船木結.md "wikilink")、[川村文乃](../Page/川村文乃.md "wikilink")，6期。
          - 2人於2017年6月26日加入。
      - [太田遥香](../Page/太田遥香.md "wikilink")，7期。
          - 於2018年11月23日加入。
  - [Juice=Juice](../Page/Juice=Juice.md "wikilink")
      - [宮本佳林](../Page/宮本佳林.md "wikilink")、[高木紗友希](../Page/高木紗友希.md "wikilink")、、[金澤朋子](../Page/金澤朋子.md "wikilink")
          - 4人於2013年9月11日正式出道。
      - [梁川奈奈美](../Page/梁川奈奈美.md "wikilink")、[段原瑠瑠](../Page/段原瑠瑠.md "wikilink")
          - 2人於2017年6月26日加入。
  - [Country Girls](../Page/Country_Girls.md "wikilink")
      - [山木梨沙](../Page/山木梨沙.md "wikilink")、
        [稻場愛香](../Page/稻場愛香.md "wikilink")
          - 2人於2014年11月5日加入。
          - 稻場愛香2016年8月4日從Country Girls畢業。
      - [梁川奈奈美](../Page/梁川奈奈美.md "wikilink")、[船木結](../Page/船木結.md "wikilink")
          - 2人於2015年11月5日加入。
  - [Kobushi Factory](../Page/Kobushi_Factory.md "wikilink")
      - [浜浦彩乃](../Page/浜浦彩乃.md "wikilink")、[田口夏實](../Page/田口夏實.md "wikilink")、[小川麗奈](../Page/小川麗奈.md "wikilink")、[野村みな美](../Page/野村みな美.md "wikilink")、[和田櫻子](../Page/和田櫻子.md "wikilink")、[藤井梨央](../Page/藤井梨央.md "wikilink")、[廣瀨彩海](../Page/廣瀨彩海.md "wikilink")、[井上玲音](../Page/井上玲音.md "wikilink")
          - 2015年1月2日結成，同年9月2日正式出道。
          - 藤井梨央2017年7月6日從Hello\! Project及Kobushi Factory離隊。
          - 小川麗奈2017年9月6日從Hello\! Project及Kobushi Factory畢業。
          - 田口夏實2017年12月6日從Hello\! Project及Kobushi Factory離隊。
  - [Tsubaki Factory](../Page/Tsubaki_Factory.md "wikilink")
      - [山岸理子](../Page/山岸理子.md "wikilink")、[岸本ゆめの](../Page/岸本ゆめの.md "wikilink")、[新沼希空](../Page/新沼希空.md "wikilink")、[淺倉樹々](../Page/淺倉樹々.md "wikilink")、[小片リサ](../Page/小片リサ.md "wikilink")、[谷本安美](../Page/谷本安美.md "wikilink")
          - 2015年4月29日結成
      - [小野田紗栞](../Page/小野田紗栞.md "wikilink")、[小野瑞歩](../Page/小野瑞歩.md "wikilink")、[秋山眞緒](../Page/秋山眞緒.md "wikilink")
          - 3人於2016年8月13日加入

### 組成團體但未成為H\!P的成員

#### アップアップガールズ（仮）

2011年3月結成。

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>姓名</p></th>
<th><p>平假名</p></th>
<th><p>出身地</p></th>
<th><p>生日</p></th>
<th><p>年齡</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1期</p></td>
<td></td>
<td><p>せんごく みなみ</p></td>
<td><p>宮城縣</p></td>
<td></td>
<td></td>
<td><p>2010年12月21日結束研修，從Egg畢業。同時也發表舞台劇演出情報。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>ふるかわ こなつ</p></td>
<td><p>神奈川縣</p></td>
<td></td>
<td></td>
<td><p>2011年1月31日結束研修，從Egg畢業。同時也發表舞台劇演出情報。</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>もり さき</p></td>
<td><p>神奈川縣</p></td>
<td></td>
<td></td>
<td><p>2011年1月17日結束研修，從Egg畢業。同時也發表舞台劇演出情報。</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>さほ あかり</p></td>
<td><p>東京都</p></td>
<td></td>
<td></td>
<td><p>2011年4月18日結束研修，從Egg畢業。5月加入團體。</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>せきね あずさ</p></td>
<td><p>長野縣</p></td>
<td></td>
<td></td>
<td><p>2011年3月9日結束研修，從Egg畢業。</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>あらい まなみ</p></td>
<td><p>群馬縣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6期</p></td>
<td></td>
<td><p>さとう あやの</p></td>
<td><p>神奈川縣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### THE ポッシボー

2006年8月結成。

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>姓名</p></th>
<th><p>平假名</p></th>
<th><p>出身地</p></th>
<th><p>生日</p></th>
<th><p>年齡</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1期</p></td>
<td></td>
<td><p>もろづか かなみ</p></td>
<td><p>埼玉縣</p></td>
<td></td>
<td></td>
<td><p>2007年10月15日從Egg畢業。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>はしもと あいな</p></td>
<td><p>神奈川縣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>あきやま ゆりか</p></td>
<td><p>神奈川縣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>おかだ ロビン しょうこ</p></td>
<td><p>美國</p></td>
<td></td>
<td></td>
<td><p>2007年10月15日從Egg畢業；9月宣布加入。</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>ごとう ゆき</p></td>
<td><p>神奈川縣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 特色

  - 每年3、6、9、12月皆會舉辦研修生公演，而近年來的公演場次與地點也增加許多。
  - 在近期，亦有部分研修生成員會參與演出H\!P中各團體的演唱會或活動。
  - 「ハロプロ研修生 発表会
    2012～12月の生タマゴShow\!～」中首次演唱專屬單曲《彼女になりたいっ！！！》。於10月27日[S/mileage](../Page/S/mileage.md "wikilink")、28日[早安少女組。演唱會公演中販售](../Page/早安少女組。.md "wikilink")\[4\]。
  - 2013年5月5日，「ハロプロ研修生 発表会2013
    ～春の公開実力診断テスト～」首次公開並開放歌迷進場觀看與投票\[5\]。往後每年亦是。
  - 會從研修生中成立新團，如2009年成立[S/mileage](../Page/S/mileage.md "wikilink")、2013年成立[Juice=Juice](../Page/Juice=Juice.md "wikilink")、2015年成立的\[\[Kobushi
    Factory|玉蘭花

`Factory]]及`[`山茶花``
 ``Factory`](../Page/Tsubaki_Factory.md "wikilink")`。或是由研修生中參與`[`早安少女組。與`](../Page/早安少女組。.md "wikilink")[`ANGERME`](../Page/ANGERME.md "wikilink")`(原S/mileage)的徵選，而成為正式成員。`

  - 近年除了開放的甄選外，也會將H\!P團中參加新成員選拔而落選的人員招攬至研修生中。

## 作品

ハロプロ研修生

  - 單曲
      - 彼女になりたいっ\!\!\! （2012年10月27日）\[6\]
      - 天まで登れ\! （2013年6月12日）\[7\]
      - おへその国からこんにちは／天まで登れ！（2013年12月21日）

<!-- end list -->

  - 配信單曲
      - Hello\! まっさらの自分 / ハロプロ研修生（2017年6月2日）
      - いのうえのうた / 井上玲音(こぶしファクトリー)、井上ひかる(ハロプロ研修生)（2017年6月30日）
      - 一尺玉でぶっ放せ！ / ハロプロ研修生（2017年6月30日）
      - 誤爆～We Can't Go Back～ / 一岡伶奈、段原瑠々、川村文乃、高瀬くるみ、清野桃々姫（2017年8月11日）
      - ありがた迷惑物語 / ハロプロ研修生（2017年9月8日）

<!-- end list -->

  - 專輯
      - ① Let's say “Hello\!”（2014年11月29日）
      - Rainbow×2 （2018年7月11日）

ハロプロ研修生北海道

  - 單曲
      - リアル☆リトル☆ガール／彼女になりたいっ\!\!\!(ハロプロ研修生北海道 Ver.)（2017年7月5日）
      - ハンコウキ\!/Ice day Party（ハロプロ研修生北海道 feat.稲場愛香）（2018年7月11日）

## 外部連結

專頁

  - [ハロプロ研修生オフィシャルファンクラブページ頁面](http://www.up-fc.jp/helloproject/member/egg/)

  - [ハロプロ研修生 Official YouTube
    Channel](https://www.youtube.com/helloprokenshusei)

  - [ハロプロ研修生官方頁面](http://www.helloproject.com/helloprokenshusei/)

  -
[ハロプロ研修生北海道官方頁面](http://www.helloproject.com/helloprokenshuseihokkaido/)

## 注釋

[Category:Hello\! Project](../Category/Hello!_Project.md "wikilink")
[Category:日本女子演唱團體](../Category/日本女子演唱團體.md "wikilink")

1.  「」正規的官方英文譯法主要有兩種，分別是「Hello\! pro EGG」和「Hello\! Pro
    EGG」，但前者較常用，可參考演唱會DVD結尾的CAST。
2.  出自歷年月刊BOMB，ハロプロ研修生パーフェクト名鑑。
3.  官方並未指名，但在研修生公演中是負責帶頭喊話的人，亦是當中最資深者。
4.  [ハロプロ研修生 インディーズ
    Single「彼女になりたいっ\!\!\!」販売のお知らせ](http://www.helloproject.com/news/1210261500_egg.html)

5.  [●公演情報 ハロプロ研修生 発表会2013
    ～春の公開実力診断テスト～](http://www.up-fc.jp/helloproject/member/egg/recital_2013spring.php)
6.  從中選出五名成員演唱，分別是宮本佳林、田辺奈菜美、浜浦彩乃、田口夏実、大塚愛菜。
7.  ハロプロ研修生 feat. Juice=Juice，同時亦是Juice=Juice第三張地下單曲。