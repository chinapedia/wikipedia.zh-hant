[唐代对三品以上官](../Page/唐代.md "wikilink")，用“**册授**”；五品以上官，用“**制授**”，六品以下官，用“**敕授**”。唐代文官官阶分为九品，除正一品不设外，二品起每品分正、从，正四品起，又有上、下阶，共有二十九阶。武官官阶亦分九品，除正一品不设外，二品起每品分正、从，正三品起，又有上、下阶，共有三十一阶。文官、武官形式上都不設正一品，實際並非不存在，詳見[天策上將](../Page/天策上將.md "wikilink")。

## 职官

| 品级                                | 職官                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [正一品](../Page/正一品.md "wikilink")  | [太师](../Page/太师.md "wikilink")、[太傅](../Page/太傅.md "wikilink")、[太保](../Page/太保.md "wikilink")、[太尉](../Page/太尉.md "wikilink")、[司徒](../Page/司徒.md "wikilink")、[司空](../Page/司空.md "wikilink")、[天策上将](../Page/天策上将.md "wikilink")\[1\]\[2\]                                                                                                                                                                                                                                                                                                                                                                                                                              |
| [从一品](../Page/从一品.md "wikilink")  | [太子太师](../Page/太子太师.md "wikilink")、[太子太傅](../Page/太子太傅.md "wikilink")、[太子太保](../Page/太子太保.md "wikilink")、[驃騎大將軍](../Page/驃騎大將軍.md "wikilink")\[3\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| [正二品](../Page/正二品.md "wikilink")  | [尚书令](../Page/尚书令.md "wikilink")、[大行台](../Page/大行台.md "wikilink")[尚书令](../Page/尚书令.md "wikilink")\[4\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| [从二品](../Page/从二品.md "wikilink")  | 尚书左右[仆射](../Page/仆射.md "wikilink")、[太子少师](../Page/太子少师.md "wikilink")、[太子少傅](../Page/太子少傅.md "wikilink")、[太子少保](../Page/太子少保.md "wikilink")、京兆/河南/太原府尹、大[都督](../Page/都督.md "wikilink")、大[都护](../Page/都护.md "wikilink")\[5\]                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| [正三品](../Page/正三品.md "wikilink")  | [侍中](../Page/侍中.md "wikilink")、[中书令](../Page/中书令.md "wikilink")\[6\]、[六部](../Page/六部.md "wikilink")[尚书](../Page/尚书.md "wikilink")、[太子宾客](../Page/太子宾客.md "wikilink")、[太常卿](../Page/太常卿.md "wikilink")、[太子詹事](../Page/太子詹事.md "wikilink")、中都督\[7\]、上都护、                                                                                                                                                                                                                                                                                                                                                                                                              |
| [从三品](../Page/从三品.md "wikilink")  | [御史大夫](../Page/御史大夫.md "wikilink")、[秘书监](../Page/秘书监.md "wikilink")、[光禄](../Page/光祿寺.md "wikilink")/[卫尉](../Page/卫尉.md "wikilink")/[宗正](../Page/宗正.md "wikilink")/[太仆](../Page/太仆.md "wikilink")/[大理](../Page/大理.md "wikilink")/[鸿胪](../Page/鸿胪.md "wikilink")/[司农](../Page/司农.md "wikilink")/[太府卿](../Page/太府.md "wikilink")、左右[散骑常侍](../Page/散骑常侍.md "wikilink")、[国子祭酒](../Page/国子祭酒.md "wikilink")、[殿中监](../Page/殿中监.md "wikilink")、[少府监](../Page/少府监.md "wikilink")、[将作大匠](../Page/将作大匠.md "wikilink")、诸卫羽林[千牛将军](../Page/千牛将军.md "wikilink")、下都督、上州[刺史](../Page/刺史.md "wikilink")、大都督府[长史](../Page/长史.md "wikilink")、大都护府[副都护](../Page/副都护.md "wikilink")\[8\] |
| [正四品上](../Page/正四品.md "wikilink") | [黄门侍郎](../Page/黄门侍郎.md "wikilink")、[中书侍郎](../Page/中书侍郎.md "wikilink")、[尚书左丞](../Page/尚书左丞.md "wikilink")、[吏部侍郎](../Page/吏部侍郎.md "wikilink")、[太常少卿](../Page/太常少卿.md "wikilink")、中州刺史、[军器监](../Page/军器监.md "wikilink")、上都护府副都护、上府[折冲都尉](../Page/折冲都尉.md "wikilink")\[9\]                                                                                                                                                                                                                                                                                                                                                                                              |
| 正四品下                              | [尚书右丞](../Page/尚书右丞.md "wikilink")、尚书中司侍郎、左右千牛卫/左右监门卫中郎将、亲勋翊卫羽林中郎将、下州刺史 \[10\]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| [从四品上](../Page/从四品.md "wikilink") | 秘书少监、殿中少监、内侍、大都护府/亲王府长史                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| 从四品下                              | [国子司业](../Page/国子司业.md "wikilink")、[少府少监](../Page/少府少监.md "wikilink")、[将作少匠](../Page/将作少匠.md "wikilink")、京兆/河南/太原府[少尹](../Page/少尹.md "wikilink")、上州[别驾](../Page/别驾.md "wikilink")、大都督府/大都护府/亲王府[司马](../Page/司马.md "wikilink")、中府折冲都尉                                                                                                                                                                                                                                                                                                                                                                                                                                |
| [正五品上](../Page/正五品.md "wikilink") | [谏议大夫](../Page/谏议大夫.md "wikilink")、[御史中丞](../Page/御史中丞.md "wikilink")、[国子博士](../Page/国子博士.md "wikilink")、[给事中](../Page/给事中.md "wikilink")、[中书舍人](../Page/中书舍人.md "wikilink")、[都水使者](../Page/都水使者.md "wikilink")、万年/长安/河南/洛阳/太原/晋阳/奉先[县令](../Page/县令.md "wikilink")、亲勋翊卫羽林郎将、中都督/上都护府长史、亲王府[典军](../Page/典军.md "wikilink")                                                                                                                                                                                                                                                                                                                                            |
| 正五品下                              | [太子中舍人](../Page/太子中舍人.md "wikilink")、[内常侍](../Page/内常侍.md "wikilink")、中都督/上都护府司马、中州别驾、下府折冲都尉、[太子洗馬](../Page/太子洗馬.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| [从五品上](../Page/从五品.md "wikilink") | 尚书左右司诸司郎中、秘书丞、著作郎、太子洗马、殿中丞、亲王府副典军、下都督府/上州长史、下州别驾                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 从五品下                              | 大理正、太常丞、太史令、内给事、上牧监、下都督府/上州司马、[驸马都尉](../Page/驸马都尉.md "wikilink")、奉车都尉、宫苑总监、上府果毅都尉                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| [正六品上](../Page/正六品.md "wikilink") | 太学博士、中州长史、亲勋翊卫校尉、京兆/河南/太原府诸县令、武库中尚署令、诸卫左右司阶、中府果毅都尉                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 正六品下                              | 千牛备身、备身左右、下州长史、中州司马、内谒者监、中牧监、上牧副监、上镇将                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| [从六品上](../Page/从六品.md "wikilink") | 起居郎、起居舍人、尚书诸司员外郎、大理司直、国子助教、城门郎、符宝郎、通事舍人、秘书郎、著作佐郎、侍御医、诸卫羽林长史、两京市令、下州司马、左右监门校尉、亲勋翊卫旅帅、上县令                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| 从六品下                              | 侍御史、少府/将作/国子监丞、司农寺诸园苑监、下牧监、宫苑总监副监、互市监、中牧副监、下府果毅都尉                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| [正七品上](../Page/正七品.md "wikilink") | 四门博士、詹事司直、左右千牛卫长史、军器监丞、中县令、亲勋翊卫队正、亲勋翊卫副隊正、中镇将                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 正七品下                              | 内寺伯、诸仓/诸冶/司竹/温汤监、诸卫左右中候、上府别将/司史、上镇副、下镇将、下牧副监                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| [从七品上](../Page/从七品.md "wikilink") | 殿中侍御史、左右补阙、太常博士、太学助教、门下省录事、尚书都事、中书省主书、左右监门直长、都水监丞、中下县令、京县丞、中府别将/长史、中镇副、勋卫太子亲卫                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 从七品下                              | 太史局丞、御史台/少府/将作/国子监主簿、掖庭/宫闱局令、下县令、太庙诸陵署丞、司农寺诸园苑副监、、宫苑总监丞、公主家令、亲王府旅帅、下府别将/长史、下镇副、诸屯监、诸折冲府校尉                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 视从七品                              | 萨宝府祆正                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| [正八品上](../Page/正八品.md "wikilink") | 监察御史、协律郎、翊卫、太医署医博士、军器监主簿、武库署丞、两京市署丞、上牧监丞、执乘亲事                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 正八品下                              | 奚官/内仆/内府局令、备身、尚药局司医、京兆/河南/太原诸县丞、太公庙丞、诸宫农圃监、互市监丞、司竹副监、司农寺诸园苑监丞、灵台郎、上戍主、诸卫左右司戈                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| [从八品上](../Page/从八品.md "wikilink") | 左右拾遗、太医署针博士、四门助教、左右千牛卫录事参军、上县丞、中牧监丞、京县主簿、诸仓/诸冶/司竹/温汤监丞、保章正、诸折冲府旅帅                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 从八品下                              | 大理评事、律学博士、太医署丞、左右千牛卫诸曹参军、内谒者、都水监主簿、中书/门下/尚书都省/兵部/吏部/考功/礼部主事、中县丞、京县尉、诸屯监丞、上关令、上府兵曹、上挈壶正、中戍主、上戍副、诸率府左右司戈                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| [正九品上](../Page/正九品.md "wikilink") | 校书郎、太祝、典客署掌客、岳渎令、诸津令、下牧监丞、中下县丞、中州博士、武库署监事                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 正九品下                              | 正字、奚官/内仆丞、内府局丞、太史局司辰、典厩署主乘、下县丞、下州博士、京兆/河南/太原府诸县尉、上牧监主簿、诸宫农圃监丞、中关令、亲王国尉、上关丞、诸卫左右执戟、中镇兵曹参军、下戍主、诸折冲队正                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| [从九品上](../Page/从九品.md "wikilink") | 尚书/御史台/秘书省/殿中省主事、奉礼郎、律学助教、弘文馆校书、大史局司历、太医署医助教、京兆/河南/太原府/九寺/少府/将作监录事、都督/都护府/上州录事市令、宫苑总监主簿、上中县尉                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 从九品下                              | 内侍省主事、国子监录事、崇文馆校书、书学博士、算学博士、门下典仪、太医署按摩/祝禁博士、太卜署卜博士、太医署针助教/医正、太卜署卜正、太史局监候、掖庭局宫教博士、太官署监膳、太乐鼓吹署乐正、大理寺狱丞、中下州医博士、中下县尉、下关令、中关丞、诸卫羽林长上、诸津丞、诸折冲府队副、诸率府左右执戟                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 流外一等                              | 诸卫/都水监/羽林军录事、尚书/中书/门下省/御史台令史、太常寺谒者、司仪署诸典书、河渠署河堤谒者、太医署医针师、内侍省寺人                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| 视流外一等                             | 萨宝府祓祝                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| 流外二等                              | 太卜署卜助教、秘书/殿中/内侍省令史、城门/符宝/夕文馆令史、通事令史、尚书/门下/中书省/御史台书令史、太常寺祝史、宫苑总监录事、典客署典客、亲勋翊卫府录事、太史局漏刻博士、御史台殿中令史                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 流外三等                              | 城门/符宝书令史、秘书/殿中/内侍省、御史台书令史、、诸牧园苑监录事、诸仓监/诸关津录事、、诸卫羽林军府/太子詹事府令史、尚食局主食、秘书/殿中/内侍省诸局书令史、内侍省内典引、尚药局太医署按摩祝禁师、太常寺赞引、太医署医工/针工、太卜署卜师诸计史、率更寺漏刻博士                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 流外四等                              | 诸卫羽林军史、门下省主宝/主符、太医主药、门下/中书省传制、太医署按摩祝禁工、御史台监察史                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 视流外四等                             | 萨宝府率                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 流外五等                              | 大理寺司直平事史、诸署农圃监、诸牧园苑监史、诸都护府史、太官署监膳史、良酝署掌酝、掌醢署主醢、诸典事、亲勋翊卫率府史、大理寺狱史                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| 视流外五等                             | 萨宝府史                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| 流外六等                              | 亲勋翊卫府史、诸仓关津府史、太医署药园师、诸亭长                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| 流外七等                              | 门下省主节、诸掌固、大史监历生、天文观生、诸仓关津史、诸仓计史                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 流外八等                              | 守宫署掌设                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| 流外九等                              | 国子学/太公庙干、诸辇者                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |

## 爵

### 男性

1.  正一品：[王(親王、國王)](../Page/親王.md "wikilink")
2.  从一品：[嗣王「承嫡親王者」](../Page/嗣王.md "wikilink")、[郡王](../Page/郡王.md "wikilink")、[国公](../Page/国公.md "wikilink")
3.  正二品：开国[郡公](../Page/郡公.md "wikilink")
4.  从二品：开国[县公](../Page/县公.md "wikilink")
5.  从三品：开国[侯](../Page/侯爵.md "wikilink")
6.  正四品上：开国[伯](../Page/伯爵.md "wikilink")
7.  正五品上：开国[子](../Page/子爵.md "wikilink")
8.  从五品上：开国[男](../Page/男爵.md "wikilink")

### 女性

  - 正一品：[公主](../Page/公主_\(東亞\).md "wikilink")、[妃](../Page/妃.md "wikilink")、[王妃](../Page/王妃.md "wikilink")、[王太妃](../Page/王太妃.md "wikilink")、[國夫人](../Page/國夫人.md "wikilink")
  - 从一品：[郡主](../Page/郡主.md "wikilink")
  - 正二品：[昭儀](../Page/昭儀.md "wikilink")、[昭容](../Page/昭容.md "wikilink")、[昭媛](../Page/昭媛.md "wikilink")、[修儀](../Page/修儀.md "wikilink")、[修容](../Page/修容_\(稱號\).md "wikilink")、[修媛](../Page/修媛.md "wikilink")、[充儀](../Page/充儀.md "wikilink")、[充容](../Page/充容.md "wikilink")、[充媛](../Page/充媛.md "wikilink")、[郡夫人](../Page/郡夫人.md "wikilink")
  - 从二品：[縣主](../Page/縣主.md "wikilink")、[郡夫人](../Page/郡夫人.md "wikilink")
  - 正三品：[婕妤](../Page/婕妤.md "wikilink")、[良娣](../Page/良娣.md "wikilink")
  - 正四品：[美人](../Page/美人_\(位号\).md "wikilink")、[郡君](../Page/郡君.md "wikilink")、[良媛](../Page/良媛.md "wikilink")
  - 从四品：[郡君](../Page/郡君.md "wikilink")
  - 正五品：[才人](../Page/才人.md "wikilink")、[縣君](../Page/縣君.md "wikilink")、[承徽](../Page/承徽.md "wikilink")
  - 从五品：[鄉君](../Page/鄉君.md "wikilink")
  - 正六品：[寶林](../Page/宝林_\(位号\).md "wikilink")
  - 正七品：[御女](../Page/御女.md "wikilink")、[昭訓](../Page/昭訓.md "wikilink")
  - 正八品：[采女](../Page/采女.md "wikilink")、[奉儀](../Page/奉儀.md "wikilink")

## 勋

勳官十二階。北朝《木蘭詩》云：「策勳十二轉，賞賜百千強。」

1.  正二品：[上柱国](../Page/上柱国.md "wikilink")
2.  从二品：[柱国](../Page/柱国.md "wikilink")
3.  正三品：上护军
4.  从三品：护军
5.  正四品：上轻军都尉
6.  从四品：轻车都尉
7.  正五品：上骑都尉
8.  从五品：骑都尉
9.  正六品：骁骑尉
10. 从六品：飞骑尉
11. 正七品：云骑尉
12. 从七品：武骑尉

## 散官

### 文官

文官散階二十九階。

1.  从一品：[开府仪同三司](../Page/开府仪同三司.md "wikilink")
2.  正二品：[特进](../Page/特进.md "wikilink")
3.  从二品：[光禄大夫](../Page/光禄大夫.md "wikilink")
4.  正三品：金紫光禄大夫
5.  从三品：银青光禄大夫
6.  正四品上：[正议大夫](../Page/正议大夫.md "wikilink")
7.  正四品下：[通议大夫](../Page/通议大夫.md "wikilink")
8.  从四品上：[太中大夫](../Page/太中大夫.md "wikilink")
9.  从四品下：[中大夫](../Page/中大夫.md "wikilink")
10. 正五品上：[中散大夫](../Page/中散大夫.md "wikilink")
11. 正五品下：[朝议大夫](../Page/朝议大夫.md "wikilink")
12. 从五品上：[朝请大夫](../Page/朝请大夫.md "wikilink")
13. 从五品下：[朝散大夫](../Page/朝散大夫.md "wikilink")
14. 正六品上：[朝议郎](../Page/朝议郎.md "wikilink")
15. 正六品下：[承议郎](../Page/承议郎.md "wikilink")
16. 从六品上：[奉议郎](../Page/奉议郎.md "wikilink")
17. 从六品下：[通直郎](../Page/通直郎.md "wikilink")
18. 正七品上：[朝请郎](../Page/朝请郎.md "wikilink")
19. 正七品下：[宣德郎](../Page/宣德郎.md "wikilink")
20. 从七品上：[朝散郎](../Page/朝散郎.md "wikilink")
21. 从七品下：[宣议郎](../Page/宣议郎.md "wikilink")
22. 正八品上：[给事郎](../Page/给事郎.md "wikilink")
23. 正八品下：[征事郎](../Page/征事郎.md "wikilink")
24. 从八品上：[承奉郎](../Page/承奉郎.md "wikilink")
25. 从八品下：[承务郎](../Page/承务郎.md "wikilink")
26. 正九品上：[儒林郎](../Page/儒林郎.md "wikilink")
27. 正九品下：[登仕郎](../Page/登仕郎.md "wikilink")
28. 从九品上：[文林郎](../Page/文林郎.md "wikilink")
29. 从九品下：[将仕郎](../Page/将仕郎.md "wikilink")

### 武官

武官散階二十九階，名號中有「懷化」、「歸德」的散階專門賜與蕃官。

1.  从一品：[骠骑大将军](../Page/骠骑大将军.md "wikilink")
2.  正二品：[辅国大将军](../Page/辅国大将军.md "wikilink")
3.  从二品：[镇军大将军](../Page/镇军大将军.md "wikilink")
4.  正三品：[冠军大将军](../Page/冠军大将军.md "wikilink")、[怀化大将军](../Page/怀化大将军.md "wikilink")
5.  从三品：[云麾将军](../Page/云麾将军.md "wikilink")、[归德大将军](../Page/归德大将军.md "wikilink")
6.  正四品上：[忠武将军](../Page/忠武将军.md "wikilink")
7.  正四品下：[壮武将军](../Page/壮武将军.md "wikilink")、[怀化中郎将](../Page/怀化中郎将.md "wikilink")
8.  从四品上：[宣威将军](../Page/宣威将军.md "wikilink")
9.  从四品下：[明威将军](../Page/明威将军.md "wikilink")、[归德中郎将](../Page/归德中郎将.md "wikilink")
10. 正五品上：[定远将军](../Page/定远将军.md "wikilink")
11. 正五品下：[宁远将军](../Page/宁远将军.md "wikilink")、[怀化郎将](../Page/怀化郎将.md "wikilink")
12. 从五品上：[游骑将军](../Page/游骑将军.md "wikilink")
13. 从五品下：[游击将军](../Page/游击将军.md "wikilink")、[归德郎将](../Page/归德郎将.md "wikilink")
14. 正六品上：[昭武校尉](../Page/昭武校尉.md "wikilink")
15. 正六品下：昭武副尉、怀化司阶
16. 从六品上：[振威校尉](../Page/振威校尉.md "wikilink")
17. 从六品下：振威副尉、归德司阶
18. 正七品上：[致果校尉](../Page/致果校尉.md "wikilink")
19. 正七品下：致果副尉、怀化中侯
20. 从七品上：[翊麾校尉](../Page/翊麾校尉.md "wikilink")
21. 从七品下：翊麾副尉、归德中侯
22. 正八品上：[宣节校尉](../Page/宣节校尉.md "wikilink")
23. 正八品下：宣节副尉、怀化司戈
24. 从八品上：[御侮校尉](../Page/御侮校尉.md "wikilink")
25. 从八品下：御侮副尉、归德司戈
26. 正九品上：[仁勇校尉](../Page/仁勇校尉.md "wikilink")
27. 正九品下：仁勇副尉、怀化执戟长上
28. 从九品上：[陪戎校尉](../Page/陪戎校尉.md "wikilink")
29. 从九品下：陪戎副尉、归德执戟长上

## 女官

  - 正五品：[尚宮](../Page/尚宮.md "wikilink")、[尚儀](../Page/尚儀.md "wikilink")、[尚服](../Page/尚服.md "wikilink")、[尚食](../Page/尚食.md "wikilink")、[尚寝](../Page/尚寝.md "wikilink")、[尚功](../Page/尚功.md "wikilink")、[宮正](../Page/宮正.md "wikilink")
  - 正六品：[司記](../Page/司記.md "wikilink")、[司言](../Page/司言.md "wikilink")、[司簿](../Page/司簿.md "wikilink")、[司闈](../Page/司闈.md "wikilink")、[司籍](../Page/司籍.md "wikilink")、[司樂](../Page/司樂.md "wikilink")、[司賓](../Page/司賓.md "wikilink")、[司贊](../Page/司贊.md "wikilink")、[司寶](../Page/司寶.md "wikilink")、[司衣](../Page/司衣.md "wikilink")、[司飾](../Page/司飾.md "wikilink")、[司仗](../Page/司仗.md "wikilink")、[司膳](../Page/司膳.md "wikilink")、[司酝](../Page/司酝.md "wikilink")、[司藥](../Page/司藥.md "wikilink")、[司饎](../Page/司饎.md "wikilink")、[司設](../Page/司設.md "wikilink")、[司輿](../Page/司輿.md "wikilink")、[司苑](../Page/司苑.md "wikilink")、[司燈](../Page/司燈.md "wikilink")、[司制](../Page/司制.md "wikilink")、[司珍](../Page/司珍.md "wikilink")、[司計](../Page/司計.md "wikilink")、[司正](../Page/司正.md "wikilink")
  - 從六品：[司閨](../Page/司閨.md "wikilink")、[司則](../Page/司則.md "wikilink")、[司饌](../Page/司饌.md "wikilink")
  - 正七品：[典記](../Page/典記.md "wikilink")、[典言](../Page/典言.md "wikilink")、[典簿](../Page/典簿.md "wikilink")、[典闈](../Page/典闈.md "wikilink")、[典籍](../Page/典籍.md "wikilink")、[典樂](../Page/典樂.md "wikilink")、[典賓](../Page/典賓.md "wikilink")、[典贊](../Page/典贊.md "wikilink")、[典寶](../Page/典寶.md "wikilink")、[典衣](../Page/典衣.md "wikilink")、[典飾](../Page/典飾.md "wikilink")、[典仗](../Page/典仗.md "wikilink")、[典膳](../Page/典膳.md "wikilink")、[典酝](../Page/典酝.md "wikilink")、[典藥](../Page/典藥.md "wikilink")、[典饎](../Page/典饎.md "wikilink")、[典設](../Page/典設.md "wikilink")、[典輿](../Page/典輿.md "wikilink")、[典苑](../Page/典苑.md "wikilink")、[典燈](../Page/典燈.md "wikilink")、[典制](../Page/典制.md "wikilink")、[典珍](../Page/典珍.md "wikilink")、[典計](../Page/典計.md "wikilink")
  - 正八品：[掌記](../Page/掌記.md "wikilink")、[掌言](../Page/掌言.md "wikilink")、[掌簿](../Page/掌簿.md "wikilink")、[掌闈](../Page/掌闈.md "wikilink")、[掌籍](../Page/掌籍.md "wikilink")、[掌樂](../Page/掌樂.md "wikilink")、[掌賓](../Page/掌賓.md "wikilink")、[掌贊](../Page/掌贊.md "wikilink")、[掌寶](../Page/掌寶.md "wikilink")、[掌衣](../Page/掌衣.md "wikilink")、[掌飾](../Page/掌飾.md "wikilink")、[掌仗](../Page/掌仗.md "wikilink")、[掌膳](../Page/掌膳.md "wikilink")、[掌酝](../Page/掌酝.md "wikilink")、[掌藥](../Page/掌藥.md "wikilink")、[掌饎](../Page/掌饎.md "wikilink")、[掌設](../Page/掌設.md "wikilink")、[掌輿](../Page/掌輿.md "wikilink")、[掌苑](../Page/掌苑.md "wikilink")、[掌燈](../Page/掌燈.md "wikilink")、[掌制](../Page/掌制.md "wikilink")、[掌珍](../Page/掌珍.md "wikilink")、[掌計](../Page/掌計.md "wikilink")
  - 從八品：[掌正](../Page/掌正.md "wikilink")、[掌書](../Page/掌書.md "wikilink")、[掌筵](../Page/掌筵.md "wikilink")、[掌嚴](../Page/掌嚴.md "wikilink")、[掌縫](../Page/掌縫.md "wikilink")、[掌藏](../Page/掌藏.md "wikilink")、[掌食](../Page/掌食.md "wikilink")、[掌醫](../Page/掌醫.md "wikilink")、[掌園](../Page/掌園.md "wikilink")
  - 不入流：[女史](../Page/女史.md "wikilink")、[宮女](../Page/宮女.md "wikilink")

## 注釋

## 参考文献

## 参见

  - [中国古代职官](../Page/中国古代职官.md "wikilink")
      - [明朝官職表](../Page/明朝官職表.md "wikilink")
      - [清朝官職表](../Page/清朝官職表.md "wikilink")

{{-}}

[唐朝官制](../Category/唐朝官制.md "wikilink")
[Category:唐朝列表](../Category/唐朝列表.md "wikilink")

1.  《[舊唐書](../Page/舊唐書.md "wikilink")‧[卷四十二‧志第二十二‧職官一](../Page/s:舊唐書/卷42.md "wikilink")》。

2.  [杜祐](../Page/杜祐.md "wikilink")《[通典](../Page/通典.md "wikilink")‧[卷四十‧職官二十二‧秩品五](../Page/s:通典/卷040.md "wikilink")》。

3.
4.
5.
6.
7.
8.
9.
10.