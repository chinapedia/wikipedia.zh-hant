**密云区**是[中国](../Page/中華人民共和國.md "wikilink")[北京市东北部的一个](../Page/北京市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，西、南与[怀柔区及](../Page/怀柔区.md "wikilink")[顺义区相邻](../Page/顺义区.md "wikilink")，东、北与[河北省相接](../Page/河北省.md "wikilink")。2015年11月，经[中国国务院批准](../Page/中国国务院.md "wikilink")，撤销密云县，设立密云区\[1\]\[2\]。

建有[密云水库](../Page/密云水库.md "wikilink")；[古北口为历史上交通重镇](../Page/古北口.md "wikilink")。

## 地理概况

地处华北平原北端，为华北平原与燕山山脉之交接。全区面积2226.5平方千米，现有人口47.8万（2014年末），大部分从事农业，主要居住在原[燕落盆地周边的](../Page/燕落盆地.md "wikilink")[冲积平原地区](../Page/冲积平原.md "wikilink")。

全区土地基本上处于山区半山区，农业用地很少，有“八山一水一分田”的说法。

密云区林木众多，森林覆盖率超过70%。

由于处于华北、京津的北方门户，密云自古就是华北的战略要冲。绵延的[燕山山脉阻断了北方的交通](../Page/燕山.md "wikilink")，只在[古北口一带留有狭小通路](../Page/古北口.md "wikilink")。

## 历史

最早的人类活动历史可以追溯到[原始社会的](../Page/原始社会.md "wikilink")[旧石器时代](../Page/旧石器时代.md "wikilink")（亦有[新石器时代的看法](../Page/新石器时代.md "wikilink")），距今10万年。

因其良好的[冲积平原位于气候恶劣的北方](../Page/冲积平原.md "wikilink")，古时有“燕国天府”的称呼。

### 史书记载

#### 原始部落时期

山安口村出土的石器、陶器，充分证明早在[新石器时代](../Page/新石器时代.md "wikilink")，密云区不老屯地区就有人类居住。燕落村南[密云水库下的](../Page/密云水库.md "wikilink")[共工城遗址](../Page/共工城.md "wikilink")，距今4100多年，是原始部落时期尧的臣子[共工](../Page/共工.md "wikilink")（掌管手工业的大臣）流放居住的土城，是[北京地区第一座古城](../Page/北京地区.md "wikilink")。《[史记](../Page/史记.md "wikilink")·五帝本纪》载：“舜请流共工于幽陵。”而《[括地志辑校](../Page/括地志辑校.md "wikilink")》则-{云}-：“故共工城在檀州燕落县界”。

#### 战国

[战国时期为](../Page/战国.md "wikilink")[燕地](../Page/燕.md "wikilink")，名[渔阳郡](../Page/渔阳郡.md "wikilink")。《[顺天府志](../Page/顺天府志.md "wikilink")》-{云}-：“[燕王喜十二年置渔阳郡和](../Page/燕王喜.md "wikilink")[渔阳县](../Page/渔阳县.md "wikilink")”。行政机构位于现在的统军庄南。

#### 秦

公元前221年秦[统一中国](../Page/统一中国.md "wikilink")，实行[郡县制](../Page/郡县制.md "wikilink")，[始皇二年设](../Page/秦始皇.md "wikilink")[渔阳郡](../Page/渔阳郡.md "wikilink")，为[三十六郡之一](../Page/三十六郡.md "wikilink")。[兩漢及](../Page/兩漢.md "wikilink")[曹魏沿用](../Page/曹魏.md "wikilink")。

#### 南北朝、隋唐、五代

[北魏时开始使用密云为的称谓](../Page/北魏.md "wikilink")。《魏书》载：“[皇始二年置](../Page/皇始_\(北魏\).md "wikilink")[密云郡](../Page/密云郡.md "wikilink")、[县](../Page/密云县.md "wikilink")，治提携城，领密云、[白檀](../Page/白檀县.md "wikilink")、[要阳三县](../Page/要阳县.md "wikilink")”。

《[隋书](../Page/隋书.md "wikilink")》记载：“密云后魏置密云郡，领白檀、要阳、密云三县。后齐废郡及二县入密云。又有旧[安乐郡](../Page/安乐郡_\(北魏\).md "wikilink")，领[安市](../Page/安市县.md "wikilink")、[土垠二县](../Page/土垠县.md "wikilink")，后[齐废土垠入安市](../Page/北齐.md "wikilink")，后[周废安市入密云县](../Page/北周.md "wikilink")。开皇初郡废。有长城。有桃花山、螺山。有渔水。”

#### 辽史

檀州，武威军，下，刺史。本燕渔阳郡地，汉为白檀县。 密云县。本汉白檀县，后汉以居憕奚。元魏置密云郡，领白檀、要阳、密云三县。

### 战事

由于特殊的地理位置，密云历来成为兵家必争之地，可记述的著名战事如下：

#### 秦开对[东胡](../Page/东胡.md "wikilink")

[燕昭王时](../Page/燕昭王.md "wikilink")，将自己大将[秦开主动送给胡人作为人质](../Page/秦开.md "wikilink")，保证不会进犯，实际是稳定对方。
不过送秦开的计谋还取得的更多收获，秦开在胡期间受到礼遇，且探察了当时胡人的军事、地理。
燕昭王二十六年（公元前286年），命秦开进攻东胡。秦开大败胡人，直到[大兴安岭](../Page/大兴安岭.md "wikilink")，俘虏众多。
后秦开请示燕昭王，仅留下马匹供军用，将妇女、儿童和其他物资送还。
后设渔阳郡，辖：[顺义](../Page/顺义.md "wikilink")、[怀柔](../Page/怀柔.md "wikilink")、密云、[滦平](../Page/滦平.md "wikilink")、[隆化](../Page/隆化.md "wikilink")、[围场](../Page/围场.md "wikilink")、[蓟县等地](../Page/蓟县.md "wikilink")。

#### 安史之乱

公元755年，[安禄山率密云在内的共九郡](../Page/安禄山.md "wikilink")15万，向唐王朝发动军事进攻。[白居易的](../Page/白居易.md "wikilink")《[长恨歌](../Page/长恨歌.md "wikilink")》中名句“渔阳鼙鼓动地来，惊破霓裳羽衣曲”即指此事。八年后，安禄山、[史思明战死](../Page/史思明.md "wikilink")，[安史之乱结束](../Page/安史之乱.md "wikilink")。

#### 民国之后的战事

1933年，日本以[熱河省地方官員表示歸附](../Page/熱河省.md "wikilink")[滿洲國為由](../Page/滿洲國.md "wikilink")，與[滿洲國軍隊進軍熱河](../Page/滿洲國軍.md "wikilink")，省長[湯玉麟不戰而逃](../Page/湯玉麟.md "wikilink")；之後日軍進攻[山海關](../Page/山海關.md "wikilink")、[長城各](../Page/長城.md "wikilink")[隘口與熱河](../Page/隘口.md "wikilink")，[國民政府派遣](../Page/國民政府.md "wikilink")[宋哲元](../Page/宋哲元.md "wikilink")、[關麟徵與](../Page/關麟徵.md "wikilink")[黃傑與](../Page/黃傑.md "wikilink")[日本軍在](../Page/日本軍.md "wikilink")[義院口、冷口、喜峰口、古北口、羅文峪、界嶺口各處鏖戰](../Page/長城戰役.md "wikilink")。

## 风景

  - [番字牌](../Page/番字石刻.md "wikilink")

<!-- end list -->

  -
    番字牌是密云番字牌村附近山石上的石刻文字。这些文字，系北方少数民族的文字。其中有[梵文](../Page/梵文.md "wikilink")，[蒙文和](../Page/蒙文.md "wikilink")[藏文](../Page/藏文.md "wikilink")，文字内容是[佛教的](../Page/佛教.md "wikilink")[六字真言](../Page/六字真言.md "wikilink")，即「唵嘛呢叭咪吽」。石刻上的小字横排两行，三行或四行的是六字真言重复刻写。

<!-- end list -->

  - [密云水库](../Page/密云水库.md "wikilink")
  - [白龙潭](../Page/白龙潭_\(密云\).md "wikilink")
  - [黑龙潭](../Page/黑龙潭_\(密云\).md "wikilink")
  - [长城](../Page/长城.md "wikilink")
      - [蟠龙山（古北口）长城](../Page/蟠龙山长城.md "wikilink")
      - [金山岭长城](../Page/金山岭长城.md "wikilink")
      - [司马台长城](../Page/司马台长城.md "wikilink")
      - [望京楼](../Page/望京楼.md "wikilink")

## 交通

[DF4C_0019_pulling_Y514@MYN_(20150106163632).JPG](https://zh.wikipedia.org/wiki/File:DF4C_0019_pulling_Y514@MYN_\(20150106163632\).JPG "fig:DF4C_0019_pulling_Y514@MYN_(20150106163632).JPG")的Y514次列车进入密云北站\]\]
[京承铁路和](../Page/京承铁路.md "wikilink")[京通铁路均穿过密云区](../Page/京通铁路.md "wikilink")，但仅京承线[密云北站仍办理客运业务](../Page/密云北站.md "wikilink")。从[101国道或](../Page/101国道.md "wikilink")[京承高速公路也可前往密云区](../Page/京承高速公路.md "wikilink")。

## 教育科技

北京[怀柔科学城东区](../Page/怀柔科学城.md "wikilink")\[3\]（密云地块）主要位于[密云区部分地区](../Page/密云区.md "wikilink")，与[北京未来科学城和](../Page/北京未来科学城.md "wikilink")[中关村科学城并称为北京市三大科学城之一](../Page/中关村.md "wikilink")。\[4\]北京市政府明确提出将其打造成“百年科学城”。\[5\][北京大学医学部将落户怀柔科学城东区](../Page/北京大学医学部.md "wikilink")。

## 参考文献

## 参见

  - [檀州](../Page/檀州_\(隋朝\).md "wikilink")

## 外部链接

  - [密云区政府](http://www.bjmy.gov.cn/)
  - [山海永平薊州密云古北口黄花镇等处地方里路图本](http://www.wdl.org/zh/item/11386/)

{{-}}

[Category:中国国家级生态市区县](../Category/中国国家级生态市区县.md "wikilink")
[密云区](../Category/密云区.md "wikilink")
[Category:北京市辖区](../Category/北京市辖区.md "wikilink")

1.
2.
3.
4.
5.