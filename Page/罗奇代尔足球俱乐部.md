<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><a href="../Page/大曼徹斯特.md" title="wikilink">大曼徹斯特</a>（<a href="../Page/英格兰西北.md" title="wikilink">英格蘭西北部</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:EnglandGreaterManchester.svg" title="fig:EnglandGreaterManchester.svg">EnglandGreaterManchester.svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/都市自治市.md" title="wikilink">都市自治市</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><center>
<p><a href="https://zh.wikipedia.org/wiki/File:England_Greater_Manchester_numbered.svg" title="fig:England_Greater_Manchester_numbered.svg">England_Greater_Manchester_numbered.svg</a></p>
</center></td>
</tr>
</tbody>
</table>

**罗奇代尔足球俱乐部**（）是位於[英格蘭](../Page/英格蘭.md "wikilink")[西北部](../Page/英格兰西北.md "wikilink")[大曼徹斯特](../Page/大曼徹斯特.md "wikilink")[罗奇代尔](../Page/罗奇代尔.md "wikilink")（Rochdale）的足球俱乐部，現時在[英格蘭足球甲級聯賽參賽](../Page/英格蘭足球甲級聯賽.md "wikilink")。

罗奇代尔自成立以來僅錄得兩次升班及一次降班，首次於1969年取得丁組季軍升班丙組，直到1974年降班返回丁組，一直逗留在[英格蘭足球聯賽的最低的級別中作賽](../Page/英格蘭足球聯賽.md "wikilink")，雖然曾於1977/78年及1979/80年兩度位居榜末，但獲重選而保留聯賽席位，是留在聯賽最低級別最長的球隊，終於在[2009/10年球季取得英乙第三位直接升班上](../Page/2009年至2010年英格蘭足球乙級聯賽.md "wikilink")[英甲](../Page/英甲.md "wikilink")，再次脫離聯賽最低級別。但於[2011/12年再由](../Page/2011年至2012年英格蘭足球甲級聯賽.md "wikilink")[英甲降班到](../Page/英格蘭足球甲級聯賽.md "wikilink")[英乙作賽](../Page/英格蘭足球乙級聯賽.md "wikilink")。[兩年後再取得英乙第三位直接升班上](../Page/2013年至2014年英格蘭足球乙級聯賽.md "wikilink")[英甲](../Page/英甲.md "wikilink")。

## 歷史

羅奇代爾於1907年成立，同年參加「曼徹斯特聯賽」（Manchester League），翌年轉投「蘭開郡混合聯賽」（Lancashire
Combination）。羅奇代爾於1911年獲得混合聯賽冠軍，但申加入[足球聯盟新增的丙組聯賽不獲接受](../Page/英格兰足球联赛.md "wikilink")。[第一次世界大戰後足球聯盟再次擴大](../Page/第一次世界大戰.md "wikilink")，但羅奇代爾仍然不獲接納。直到1921年羅奇代爾才獲得推荐加入新成立的北區丙組聯賽，首場聯賽於1921年8月27日在主場出戰[-{zh-hans:艾宁顿;zh-hk:阿克寧頓}-](../Page/艾宁顿斯坦利足球俱乐部.md "wikilink")，以6-3取得勝利，惟於球季結束時羅奇代爾位列榜末，需要再次申請才能保留聯賽席位。

1962年羅奇代爾以丁組球隊身份晉級[英格蘭聯賽盃決賽](../Page/英格蘭聯賽盃.md "wikilink")，是唯一最低級別球隊殺入國內主要錦標決賽的球隊，但兩回合累計以0-4負於[-{zh-hans:诺维奇城;zh-hk:諾域治}-](../Page/诺维奇城足球俱乐部.md "wikilink")。

## 大事紀年

| <font color="white"> **年　份**                          | <font color="white"> **事　項**                                                                                                                                           |
| ----------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1907                                                  | 參加曼徹斯特聯賽（Manchester League）                                                                                                                                            |
| 1908-09                                               | 參加蘭開郡混合聯賽（Lancashire Combination）乙組聯賽                                                                                                                                  |
| 1909-10                                               | 升級蘭開郡混合聯賽甲組                                                                                                                                                            |
| 1910-11                                               | **蘭開郡混合聯賽冠軍**                                                                                                                                                          |
| 1911-12                                               | **蘭開郡混合聯賽冠軍**（第二次）                                                                                                                                                     |
| 1921-22                                               | 北區丙組聯賽創始成員                                                                                                                                                             |
| 1922                                                  | 北區丙組聯賽排第廿位，獲重選保留席位                                                                                                                                                     |
| 1923-24                                               | 北區丙組聯賽亞軍                                                                                                                                                               |
| 1926-27                                               | 北區丙組聯賽亞軍                                                                                                                                                               |
| 1958-59                                               | 聯賽重組被置於丙組聯賽                                                                                                                                                            |
| 1959                                                  | 丙組聯賽排第廿四位，降級丁組                                                                                                                                                         |
| 1961-62                                               | 決賽兩回合累計0-4負於[-{zh-hant:諾域治; zh-cn:诺维奇城;}-](../Page/诺维奇城足球俱乐部.md "wikilink")，聯賽盃亞軍                                                                                      |
| 1968-69                                               | 丁組聯賽季軍，升級丙組                                                                                                                                                            |
| 1974                                                  | 丙組聯賽排第廿四位，降級丁組                                                                                                                                                         |
| 1978                                                  | 丁組聯賽排第廿四位，獲重選保留席位                                                                                                                                                      |
| 1980                                                  | 丁組聯賽排第廿四位，獲重選保留席位                                                                                                                                                      |
| 1992-93                                               | 超聯成立，丁組更名丙組《IV》                                                                                                                                                        |
| 2001-02                                               | 丙組《IV》聯賽排第五位，附加賽準決賽兩回合累計3-4負於[-{zh-hant:魯斯頓鑽石; zh-cn:鲁什登钻石;}-](../Page/鲁什登钻石足球俱乐部.md "wikilink")                                                                       |
| 2004-05                                               | 丙組《IV》更名「英乙聯賽」                                                                                                                                                         |
| [2007-08](../Page/2007年至2008年英格蘭足球乙級聯賽.md "wikilink") | 英乙聯賽排第五位，附加賽準決兩回合累計3-3，十二碼5-4擊敗[-{zh-hans:达灵顿; zh-hk:達寧頓;}-](../Page/达灵顿足球俱乐部.md "wikilink")，溫布萊決賽2-3負於[-{zh-hans:斯托克港; zh-hk:史托港;}-](../Page/斯托克港足球俱乐部.md "wikilink") |
| [2008-09](../Page/2008年至2009年英格蘭足球乙級聯賽.md "wikilink") | 英乙聯賽排第六位，附加賽準決兩回合累計1-2負於[-{zh-hans:吉林汉姆;zh-hk:基寧咸}-](../Page/吉林汉姆足球俱乐部.md "wikilink")                                                                                  |
| [2009-10](../Page/2009年至2010年英格蘭足球乙級聯賽.md "wikilink") | 英乙聯賽排第三位，升班英甲聯賽                                                                                                                                                        |
| [2011-12](../Page/2011年至2012年英格蘭足球甲級聯賽.md "wikilink") | 英甲聯賽排第廿四位，降班英乙                                                                                                                                                         |
| [2013-14](../Page/2013年至2014年英格蘭足球乙級聯賽.md "wikilink") | 英乙聯賽排第三位，升班英甲                                                                                                                                                          |

<small>資料來源：[球會歷史資料庫](https://web.archive.org/web/20120308082736/http://www.fchd.btinternet.co.uk/ROCHDALE.HTM)</small>

## 球會近況

*參見[2015年至2016年英格蘭足球甲級聯賽](../Page/2015年至2016年英格蘭足球甲級聯賽.md "wikilink")*

  - 2015年10月2日，中場球員[-{zh-hans:彼得·文森蒂;
    zh-hk:彼得·文森迪;}-](../Page/彼得·文森迪.md "wikilink")（Peter
    Vincenti）榮獲首次頒發的｢聯賽每月無名英雄｣（Sky Bet Football League’s Unsung Hero of
    the Month Award）\[1\]。

### 盃賽成績

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 10%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>圈數</p></th>
<th><p>日期</p></th>
<th><p>主／客</p></th>
<th><p>對賽球隊</p></th>
<th><p>紀錄</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/英格蘭聯賽盃.md" title="wikilink">聯　賽　盃</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第一圈</p></td>
<td><p>2015年8月11日</p></td>
<td><p>主</p></td>
<td><p><a href="../Page/考文垂足球俱乐部.md" title="wikilink">高雲地利</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/33768604">勝1-1</a><br />
（<a href="../Page/加時賽.md" title="wikilink">加時</a>1-1；<a href="../Page/互射十二碼.md" title="wikilink">十二碼</a>5-3）</p></td>
</tr>
<tr class="odd">
<td><p>第二圈</p></td>
<td><p>2015年8月25日</p></td>
<td><p>客</p></td>
<td><p><a href="../Page/赫尔城足球俱乐部.md" title="wikilink">-{zh-hans:赫尔城; zh-hk:侯城;}-</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/33969860">負0-1</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英格蘭聯賽錦標.md" title="wikilink">聯 賽 錦 標</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第一圈（北區東）</p></td>
<td><p>輪空</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第二圈（北區東）</p></td>
<td><p>2015年10月6日</p></td>
<td><p>主</p></td>
<td><p><a href="../Page/切斯特菲尔德足球俱乐部.md" title="wikilink">-{zh-hans:切斯特非尔德; zh-hk:車士打菲特;}-</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/34387141">勝2-1</a></p></td>
</tr>
<tr class="odd">
<td><p>半準決賽（北區）</p></td>
<td><p>2015年11月10日</p></td>
<td><p>主</p></td>
<td><p><a href="../Page/摩尔甘比足球俱乐部.md" title="wikilink">-{zh-hans:摩尔甘比; zh-hk:摩甘比;}-</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/34706689">負0-1</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英格蘭足總盃.md" title="wikilink">足　總　盃</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第一圈</p></td>
<td><p>2015年11月7日</p></td>
<td><p>主</p></td>
<td><p><a href="../Page/斯温登足球俱乐部.md" title="wikilink">-{zh-hans:斯温登; zh-hk:史雲頓;}-</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/34685705">勝3-1</a></p></td>
</tr>
<tr class="even">
<td><p>第二圈</p></td>
<td><p>2015年12月6日</p></td>
<td><p>主</p></td>
<td><p><a href="../Page/貝里足球俱樂部.md" title="wikilink">-{zh-hans:貝里; zh-hk:貝利;}-</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/34956420">負0-1</a></p></td>
</tr>
</tbody>
</table>

## 主場球場

[Spotland1.png](https://zh.wikipedia.org/wiki/File:Spotland1.png "fig:Spotland1.png")
羅奇代爾足球會的主場比賽在位於[英格蘭](../Page/英格蘭.md "wikilink")[西北部](../Page/英格兰西北.md "wikilink")[大曼徹斯特](../Page/大曼徹斯特.md "wikilink")[罗奇代尔的](../Page/罗奇代尔.md "wikilink")**斯波特兰球场**（**Spotland
Stadium**）舉行，球場於1920年為羅奇代爾而興建，可容納10,249名觀眾。但自1988年以後，球場由羅奇代爾、羅奇代爾市議會及羅奇代爾黃蜂欖球隊（Rochdale
Hornets）共同擁有。

球場由四座看台組成：稱為｢Co-Operative看台」（Co-Operative Stand）的主看台，稱為｢Thwaites Beer看台｣
（Thwaites Beer Stand）的仙迪徑看台（Sandy Lane End），稱為｢T.D.S看台｣（T.D.S
Stand）的明珠街看台（Pearl Street end）及稱為｢Westrose
Leisure看台」（Westrose Leisure Stand）的韋畢斯徑看台（Willbutts Lane
Stand）。除位於一方球門後的仙迪徑看台是細小的階梯看台外，其他三座均為全坐席看台。 {{-}}

## 會徽及隊色

羅奇代爾於2007/08年球季球會百周年紀念再次採用球隊首件球衣的款式，相信是受球隊成立時最強的球隊之一的[-{zh-hant:紐卡素;
zh-cn:纽卡斯尔联;}-的影響](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，黑白直間球衣白褲黑襪。在此以前，「代爾」穿著全藍球衣，其款式自1949年以來沒有重大改變，在百周年紀念年後相信將會回復這全藍傳統的款式。其他曾選用的球衣款式包括白衫黑褲、白衫藍褲及白袖藍衫等。作客的黃衫黑褲亦為百周年而推出，但下季仍會採用。為現時的球衣及訓練套裝由[耐克](../Page/耐克.md "wikilink")（Nike）製造。

「代爾」的球衣自1983年開始已獲的贊助，其中包括二手車行「Carcraft」、「All-in-One Garden
Centre」、「Smith Metals」及「Keytech」，現時的贊助商為地產商「MMC Estates」。

羅奇代爾的會徽由舊羅奇代爾鎮的[紋章轉化而成](../Page/紋章.md "wikilink")，內含不同的[工業及紋章的意象](../Page/工業.md "wikilink")，外圍的藍圈內藏羅奇代爾的全名（Rochdale
A.F.C.）及綽號（The
Dale）。羅奇代爾的[格言](../Page/格言.md "wikilink")[拉丁語為](../Page/拉丁語.md "wikilink")「*Crede
Signo*」，意思為「相信神蹟」，同時是羅奇代爾鎮的格言。於百周年紀念年會徽外圍加上幼金圈，加上「百周年紀念」（Centenary
Year）及年份「1907-2007」，以紀念球隊成立一[世紀](../Page/世紀.md "wikilink")。

## 球員名單

<small>更新日期：2015年6月4日 (四) 01:53 (UTC)
**粗體字**為新加盟球員 </small>

### 目前效力（2015/16）

<table style="width:204%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 18%" />
<col style="width: 47%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">前屬球會</p></th>
<th><p><font color="white">身價</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>門　將</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><a href="../Page/喬許·利爾斯.md" title="wikilink">乔希·利利斯</a>（Josh Lillis）</p></td>
<td><p>門將</p></td>
<td><p>1987.06.24</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/18415238">2012</a></p></td>
<td><p><a href="../Page/斯坎索普足球俱乐部.md" title="wikilink">斯肯索普</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><a href="../Page/Jonathan_Diba_Musangu.md" title="wikilink">乔纳森·迪巴·穆桑古</a>（Jonathan Diba Musangu）</p></td>
<td><p>門將</p></td>
<td><p>1997.10.12</p></td>
<td><p>2014</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p><strong>23</strong></p></td>
<td></td>
<td><p><a href="../Page/Steve_Collis.md" title="wikilink">斯蒂芬·科利斯</a>（Stephen "Steve" Collis）</p></td>
<td><p>門將</p></td>
<td><p>1981.03.18</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/21278518">2013</a></p></td>
<td><p><a href="../Page/Buxton_F.C..md" title="wikilink">巴克斯頓</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p>後　衛</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2</strong></p></td>
<td></td>
<td><p><a href="../Page/Joe_Rafferty.md" title="wikilink">乔·拉弗蒂</a>（Joseph "Joe" Rafferty）</p></td>
<td><p>後衛</p></td>
<td><p>1993.10.06</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/18674184">2012</a></p></td>
<td><p><a href="../Page/利物浦足球俱乐部.md" title="wikilink">利物浦青年軍</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>3</strong></p></td>
<td></td>
<td><p><a href="../Page/里斯·本内特.md" title="wikilink">-{zh-hans:里斯·本内特; zh-hk:賓尼特;}-</a>（Rhys Bennett）</p></td>
<td><p>中堅</p></td>
<td><p>1991.09.01</p></td>
<td><p><a href="http://www.football-league.co.uk/league2/news/20120611/dale-snap-up-bennett_2293326_2808475">2012</a></p></td>
<td><p><a href="../Page/博尔顿足球俱乐部.md" title="wikilink">保頓</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>'''4</p></td>
<td></td>
<td><p>'''<a href="../Page/James_McNulty_(footballer).md" title="wikilink">詹姆斯·麦克纳尔特</a>（Jimmy "Jim" McNulty）</p></td>
<td><p>後衛</p></td>
<td><p>1985.02.13</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32974847">2015</a></p></td>
<td><p><a href="../Page/貝里足球俱樂部.md" title="wikilink">貝利</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><a href="../Page/阿什利·伊斯特漢姆.md" title="wikilink">-{zh-hans:阿什利·伊斯特汉; zh-hk:艾殊利·伊斯咸;}-</a>（Ashley Eastham）</p></td>
<td><p>後衛</p></td>
<td><p>1991.03.22</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/22990420">2013</a></p></td>
<td><p><a href="../Page/黑池足球會.md" title="wikilink">黑池</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><a href="../Page/Oliver_Lancashire.md" title="wikilink">奥利维·兰卡西尔</a>（Oliver "Olly" Lancashire）</p></td>
<td><p>後衛</p></td>
<td><p>1988.12.13</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/22909645">2013</a></p></td>
<td><p><a href="../Page/艾迪索特镇足球俱乐部.md" title="wikilink">艾迪索特</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>17</strong></p></td>
<td></td>
<td><p><a href="../Page/斯科特·坦斯爾.md" title="wikilink">斯科特·坦泽</a>（Scott Tanser）</p></td>
<td><p>後衛</p></td>
<td><p>1994.10.23</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/22990420">2013</a></p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p><strong>25</strong></p></td>
<td></td>
<td><p><a href="../Page/邁克·羅斯.md" title="wikilink">迈克尔·罗斯</a>（Michael Rose）</p></td>
<td><p>後衛</p></td>
<td><p>1982.07.28</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/21459127">2013</a></p></td>
<td><p><a href="../Page/科尔切斯特联足球俱乐部.md" title="wikilink">高車士打</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>33</strong></p></td>
<td></td>
<td><p><a href="../Page/湯姆·肯尼迪.md" title="wikilink">汤姆·肯尼迪</a>（Tom Kennedy）</p></td>
<td><p>後衛</p></td>
<td><p>1985.06.24</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/28788706">2014</a></p></td>
<td><p><a href="../Page/巴恩斯利足球俱乐部.md" title="wikilink">班士利</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>中　場</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>7</strong></p></td>
<td></td>
<td><p><a href="../Page/彼得·文森迪.md" title="wikilink">-{zh-hans:彼得·文森蒂; zh-hk:彼得·文森迪;}-</a>（Peter Vincenti）</p></td>
<td><p>中場</p></td>
<td><p>1986.07.07</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/22480031">2013</a></p></td>
<td><p><a href="../Page/艾迪索特镇足球俱乐部.md" title="wikilink">艾迪索特</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p><strong>8</strong></p></td>
<td></td>
<td><p><a href="../Page/馬提·倫德.md" title="wikilink">马修·隆德</a>（Matthew "Matty" Lund）</p></td>
<td><p>中場</p></td>
<td><p>1990.11.21</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/23008418">2013</a></p></td>
<td><p><a href="../Page/斯托克城足球俱樂部.md" title="wikilink">史篤城</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p>'''12</p></td>
<td></td>
<td><p>'''<a href="../Page/Donal_McDermott.md" title="wikilink">-{zh-hans:多纳·麦克德莫特; zh-hk:麥達莫;}-</a>（Donal McDermott）</p></td>
<td><p>翼鋒</p></td>
<td><p>1989.10.19</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32627345">2015</a></p></td>
<td><p><a href="../Page/Ramsbottom_United_F.C..md" title="wikilink">拉姆斯博滕联</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>'''19</p></td>
<td></td>
<td><p>'''<a href="../Page/Nathaniel_Mendez-Laing.md" title="wikilink">纳撒尼尔·门德斯-莱恩</a><small>（Nathaniel Mendez-Laing）</small></p></td>
<td><p>翼鋒</p></td>
<td><p>1992.04.15</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/34072993">2015</a></p></td>
<td><p><a href="../Page/彼得伯勒联足球俱乐部.md" title="wikilink">彼德堡</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><a href="../Page/Brian_Barry-Murphy.md" title="wikilink">白賴仁·巴利-梅菲</a>（Brian Barry-Murphy）</p></td>
<td><p>中場</p></td>
<td><p>1978.07.27</p></td>
<td><p><a href="http://www.skysports.com/story/0,19528,11719_6209950,00.html">2010</a></p></td>
<td><p><a href="../Page/貝里足球俱樂部.md" title="wikilink">貝利</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p><strong>24</strong></p></td>
<td></td>
<td><p><a href="../Page/吉米·阿倫.md" title="wikilink">杰米·艾伦</a>（Jamie Allen）<a href="https://zh.wikipedia.org/wiki/File:Captain_sports.svg" title="fig:Captain_sports.svg">Captain_sports.svg</a></p></td>
<td><p>中場</p></td>
<td><p>1995.01.29</p></td>
<td><p>2012</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p><strong>27</strong></p></td>
<td></td>
<td><p><a href="../Page/安迪·坎農.md" title="wikilink">安迪·坎农</a>（Andy Cannon）</p></td>
<td><p>中場</p></td>
<td><p>1996.03.14</p></td>
<td><p>2014</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p><strong>28</strong></p></td>
<td></td>
<td><p><a href="../Page/加倫·卡普斯.md" title="wikilink">卡勒姆·卡普斯</a>（Callum Camps）</p></td>
<td><p>中場</p></td>
<td><p>1995.11.30</p></td>
<td><p>2013</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>'''--</p></td>
<td></td>
<td><p>'''<a href="../Page/Dave_Syers.md" title="wikilink">戴夫·史耶斯</a>（Dave Syers）</p></td>
<td><p>中場</p></td>
<td><p>1987.11.30</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/35345824">2016</a></p></td>
<td><p><a href="../Page/斯坎索普足球俱乐部.md" title="wikilink">斯肯索普</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>前　鋒</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>9</strong></p></td>
<td></td>
<td><p><a href="../Page/Calvin_Andrew.md" title="wikilink">-{zh-hans:卡尔文·安德鲁; zh-hk:安德鲁;}-</a>（Calvin Andrew）</p></td>
<td><p>前鋒</p></td>
<td><p>1986.12.19</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/28486101">2014</a></p></td>
<td><p><a href="../Page/約克城足球會.md" title="wikilink">約克城</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>'''10</p></td>
<td></td>
<td><p>'''<a href="../Page/路易斯·亞歷山德拉.md" title="wikilink">刘易斯·亚历山大</a>（Lewis Alessandra）</p></td>
<td><p>前鋒</p></td>
<td><p>1989.02.08</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/33048611">2015</a></p></td>
<td><p><a href="../Page/普利茅斯足球俱乐部.md" title="wikilink">普利茅夫</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>11</strong></p></td>
<td></td>
<td><p><a href="../Page/鲁本·诺布尔-拉扎勒斯.md" title="wikilink">-{zh-hans:鲁本·诺布尔-拉扎勒斯; zh-hk:魯本·盧保-拉沙里斯;}-</a>（Reuben Noble-Lazarus）</p></td>
<td><p>前鋒</p></td>
<td><p>1993.08.16</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/30795570">2015</a></p></td>
<td><p><a href="../Page/巴恩斯利足球俱乐部.md" title="wikilink">班士利</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>'''31</p></td>
<td></td>
<td><p><a href="../Page/Nyal_Bell.md" title="wikilink">贝尔</a>（Nyal Bell）</p></td>
<td><p>中場</p></td>
<td><p>1997.01.18</p></td>
<td><p>2014</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p><strong>39</strong></p></td>
<td></td>
<td><p><a href="../Page/喬·巴尼.md" title="wikilink">乔·班尼</a>（Joe Bunney）</p></td>
<td><p>前鋒</p></td>
<td><p>1993.09.26</p></td>
<td><p><a href="http://www.mancunianmatters.co.uk/content/030563470-fantastic-feeling-youngster-joe-bunney-signs-first-rochdale-deal-complete-journey">2013</a></p></td>
<td><p><a href="../Page/諾夫域治維多利亞足球會.md" title="wikilink">諾夫域治維多利亞</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p><strong>40</strong></p></td>
<td></td>
<td><p><a href="../Page/Ian_Henderson_(footballer).md" title="wikilink">-{zh-hans:伊安·亨德森; zh-hk:亨達臣;}-</a>（Ian Henderson）</p></td>
<td><p>前鋒</p></td>
<td><p>1985.01.24</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/21587574">2013</a></p></td>
<td><p><a href="../Page/科尔切斯特联足球俱乐部.md" title="wikilink">高車士打</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p>'''--</p></td>
<td></td>
<td><p>'''<a href="../Page/格兰特·霍尔特.md" title="wikilink">-{zh-hans:格兰特·霍尔特;zh-hk:格蘭·賀特;}-</a>（Grant Holt）</p></td>
<td><p>前鋒</p></td>
<td><p>1981.04.12</p></td>
<td><p><a href="http://www.bbc.com/sport/football/35604642">2016</a></p></td>
<td><p><a href="../Page/威根竞技足球俱乐部.md" title="wikilink">韋根</a></p></td>
<td><p>自由轉會</p></td>
</tr>
</tbody>
</table>

### 外借球員

<table style="width:204%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 18%" />
<col style="width: 47%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">外借球會</p></th>
<th><p><font color="white">外借年期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>26</strong></p></td>
<td></td>
<td><p><a href="../Page/Joel_Logan.md" title="wikilink">乔尔·洛根</a>（Joel Logan）</p></td>
<td><p>前鋒</p></td>
<td><p>1995.01.25</p></td>
<td><p>2012</p></td>
<td><p><a href="../Page/雷克斯汉姆足球俱乐部.md" title="wikilink">域斯咸</a></p></td>
<td><p><a href="http://www.rochdaleafc.co.uk/news/article/joel-logan-heads-out-on-loan-2585749.aspx">15年9月</a></p></td>
</tr>
</tbody>
</table>

### 離隊球員（2015/16）

<table style="width:204%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 18%" />
<col style="width: 47%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">加盟球會</p></th>
<th><p><font color="white">身價</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2015年夏季轉會窗</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>4</strong></p></td>
<td></td>
<td><p><a href="../Page/Sean_McGinty.md" title="wikilink">梳恩·麥堅堤</a>（Sean McGinty）</p></td>
<td><p>後衛</p></td>
<td><p>1993.08.11</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/27484570">2014</a></p></td>
<td><p>--</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32596878">季後放棄</a></p></td>
</tr>
<tr class="odd">
<td><p>'''10</p></td>
<td></td>
<td><p><a href="../Page/菲比恩·布兰迪.md" title="wikilink">-{zh-hans:菲比恩·布兰迪; zh-hk:布蘭迪;}-</a>（Febian Brandy）</p></td>
<td><p>前锋</p></td>
<td><p>1989.02.04</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/30780977">2015</a></p></td>
<td><p>--</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32596878">季後放棄</a></p></td>
</tr>
<tr class="even">
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><a href="../Page/Stephen_Dawson.md" title="wikilink">斯蒂芬·道森</a>（Stephen Dawson）</p></td>
<td><p>中場</p></td>
<td><p>1985.12.04</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/28771207">2014</a></p></td>
<td><p><a href="../Page/斯坎索普足球俱乐部.md" title="wikilink">斯肯索普</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32796955">拒續新約</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><a href="../Page/Bastien_Hery.md" title="wikilink">巴斯提恩·亨利</a>（Bastien Hery）</p></td>
<td><p>中場</p></td>
<td><p>1992.03.23</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/22448542">2013</a></p></td>
<td><p><a href="../Page/卡莱尔联足球俱乐部.md" title="wikilink">卡素爾</a></p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/33263991">自由轉會</a></p></td>
</tr>
<tr class="even">
<td><p><strong>18</strong></p></td>
<td></td>
<td><p><a href="../Page/Jack_Muldoon.md" title="wikilink">杰克·马尔敦</a>（Jack Muldoon）</p></td>
<td><p>前鋒</p></td>
<td><p>1989.05.19</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/27426089">2014</a></p></td>
<td><p>--</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32596878">季後放棄</a> &lt;!--</p></td>
</tr>
<tr class="odd">
<td><p>2016年冬季轉會窗</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>借　用　歸　還 --&gt;</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [羅奇代爾官方網站](http://www.rochdaleafc.co.uk/)

[R](../Category/英格蘭足球俱樂部.md "wikilink")
[Category:1907年建立](../Category/1907年建立.md "wikilink")
[Category:1907年建立的足球俱樂部](../Category/1907年建立的足球俱樂部.md "wikilink")

1.