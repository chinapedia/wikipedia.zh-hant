**大佛口**（）是[香港島近](../Page/香港島.md "wikilink")[灣仔西與](../Page/灣仔.md "wikilink")[金鐘東之間的地方](../Page/金鐘.md "wikilink")，即金鐘道與皇后大道東的岔路口、[先施保險大廈一帶](../Page/先施保險大廈.md "wikilink")。目前莊士敦道旁[衛蘭軒就在此地](../Page/衛蘭軒.md "wikilink")。在[第一次世界大戰前後一間](../Page/第一次世界大戰.md "wikilink")[日本](../Page/日本.md "wikilink")[資本的](../Page/資本.md "wikilink")[出入口](../Page/出入口.md "wikilink")[公司](../Page/公司.md "wikilink")「[大佛洋行](../Page/大佛洋行.md "wikilink")」（）設於[莊士敦道口](../Page/莊士敦道.md "wikilink")，其後[二天堂中藥堂在上址開業](../Page/二天堂.md "wikilink")，並於店外[牆掛上一個](../Page/牆.md "wikilink")[佛像商標](../Page/佛.md "wikilink")，[香港人印像深刻](../Page/香港人.md "wikilink")，街坊們因此習慣稱此地方為大佛口\[1\]\[2\]。1990年代一間於[皇后大道東的](../Page/皇后大道東.md "wikilink")[東美中心旁開設](../Page/東美中心.md "wikilink")，但現已結業的連鎖食肆，亦以**大佛口食坊**為名。

[大佛口亦為](../Page/大佛口_\(選區\).md "wikilink")[灣仔區議會其中一個選區](../Page/灣仔區議會.md "wikilink")，現任議員為親[建制的](../Page/建制.md "wikilink")[民建聯成員李勻頤](../Page/民建聯.md "wikilink")。

## 相關

  - [唐樓](../Page/唐樓.md "wikilink")
  - [二天堂中藥堂](../Page/二天堂.md "wikilink")
  - [大生大廈](../Page/大生大廈.md "wikilink")
  - [日佔時代](../Page/日佔時代.md "wikilink")
  - [明信片](../Page/明信片.md "wikilink")
  - [灣仔天橋畫廊](../Page/灣仔天橋畫廊.md "wikilink")

## 參考

## 外部連結

  - [香港地方: 大佛口](http://www.hk-place.com/db.php?post=d001013)
  - [大佛口雜談](http://www.cuhkacs.org/~benng/Bo-Blog/read.php?1118)

[Category:灣仔](../Category/灣仔.md "wikilink")

1.
2.