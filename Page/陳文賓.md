**陳文賓**（1973年10月5日－），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[統一獅](../Page/統一獅.md "wikilink")、[興農牛及](../Page/興農牛.md "wikilink")[中信鯨](../Page/中信鯨.md "wikilink")，守備位置為[一壘手](../Page/一壘手.md "wikilink")。2002年奪下的[中華職棒全壘打王獎](../Page/中華職棒全壘打王.md "wikilink")，更是[中華職棒第一位輸出到](../Page/中華職棒.md "wikilink")[日本職棒的野手](../Page/日本職棒.md "wikilink")。

## 經歷

  - [屏東縣復興國小少棒隊](../Page/屏東縣.md "wikilink")
  - [台南縣復興國中青少棒隊](../Page/台南縣.md "wikilink")
  - [台中市新民商工青棒隊](../Page/台中市.md "wikilink")（俊國）
  - [俊國建設棒球隊](../Page/俊國建設.md "wikilink")
  - 陸光棒球隊
  - [中華職棒](../Page/中華職棒.md "wikilink")[統一獅隊](../Page/統一獅.md "wikilink")（1996年－1997年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")（1997年－2001年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[和信鯨隊](../Page/中信鯨.md "wikilink")（2001年－2002年）
  - [日本職棒](../Page/日本職棒.md "wikilink")[福岡大榮鷹隊](../Page/福岡軟體銀行鷹.md "wikilink")（2003年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")（2004年－2006年）
  - 大新莊打擊練習場輔大館、中壢館駐場教練
  - [台北市](../Page/台北市.md "wikilink")[陽明高中青少棒隊教練](../Page/陽明高中.md "wikilink")

## 職棒生涯成績

| 年度                               | 球隊                               | 出賽  | 打數  | 安打 | 全壘打    | 打點 | 盜壘 | 四死 | 三振 | 壘打數 | 雙殺打   | 打擊率                                         |
| -------------------------------- | -------------------------------- | --- | --- | -- | ------ | -- | -- | -- | -- | --- | ----- | ------------------------------------------- |
| 1996年                            | [統一獅](../Page/統一獅.md "wikilink") | 55  | 97  | 18 | 3      | 8  | 0  | 6  | 26 | 28  | 4     | 0.186                                       |
| 1997年                            | [統一獅](../Page/統一獅.md "wikilink") | 34  | 96  | 23 | 3      | 17 | 0  | 5  | 27 | 37  | 4     | 0.240                                       |
| [興農牛](../Page/興農牛.md "wikilink") | 39                               | 108 | 30  | 4  | 19     | 0  | 6  | 23 | 47 | 4   | 0.278 |                                             |
| 1998年                            | [興農牛](../Page/興農牛.md "wikilink") | 93  | 288 | 76 | 8      | 15 | 2  | 19 | 68 | 115 | 9     | 0.264                                       |
| 1999年                            | [興農牛](../Page/興農牛.md "wikilink") | 85  | 287 | 77 | 11     | 15 | 11 | 33 | 69 | 125 | 2     | 0.268                                       |
| 2000年                            | [興農牛](../Page/興農牛.md "wikilink") | 84  | 276 | 72 | 11     | 15 | 18 | 24 | 41 | 126 | 7     | 0.261                                       |
| 2001年                            | [中信鯨](../Page/中信鯨.md "wikilink") | 49  | 149 | 28 | 5      | 15 | 4  | 13 | 33 | 53  | 7     | 0.188                                       |
| 2002年                            | [中信鯨](../Page/中信鯨.md "wikilink") | 85  | 283 | 82 | **26** | 61 | 6  | 43 | 65 | 176 | 11    | 0.290                                       |
| 2004年                            | [中信鯨](../Page/中信鯨.md "wikilink") | 64  | 174 | 47 | 7      | 21 | 4  | 13 | 57 | 75  | 1     | 0.270                                       |
| 2005年                            | [中信鯨](../Page/中信鯨.md "wikilink") | 69  | 175 | 40 | 5      | 18 | 1  | 12 | 52 | 61  | 3     | 0.229                                       |
| 2006年                            | [中信鯨](../Page/中信鯨.md "wikilink") | 17  | 47  | 9  | 1      | 6  | 1  | 8  | 13 | 14  | 0     | 0.191 |- span style="background-color:teal" |

## 特殊事蹟

  - 2002年5月11日，[中華職棒](../Page/中華職棒.md "wikilink")[天母棒球場首戰](../Page/天母棒球場.md "wikilink")，[中信鯨對](../Page/中信鯨.md "wikilink")[兄弟象](../Page/兄弟象.md "wikilink")，擊出該球場第一支全壘打。
  - 2002年創下單季26支全壘打的記錄，為當時本土球員最高記錄，但隔年[張泰山即以](../Page/張泰山.md "wikilink")28支全壘打再創本土球員單季全壘新高記錄。

## 外部連結

[C](../Category/1973年出生.md "wikilink")
[C](../Category/在世人物.md "wikilink")
[C](../Category/台灣棒球選手.md "wikilink")
[C](../Category/日本職棒外籍球員.md "wikilink")
[C](../Category/統一獅隊球員.md "wikilink")
[C](../Category/興農牛隊球員.md "wikilink")
[C](../Category/福岡軟體銀行鷹隊球員.md "wikilink")
[C](../Category/中信鯨隊球員.md "wikilink")
[C](../Category/台灣旅日棒球選手.md "wikilink")
[C](../Category/台灣旅外歸國棒球選手.md "wikilink")
[C](../Category/中華職棒全壘打王.md "wikilink")
[C](../Category/屏東人.md "wikilink")
[C](../Category/輔仁大學校友.md "wikilink")
[C](../Category/陳姓.md "wikilink")