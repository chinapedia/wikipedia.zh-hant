**嘉兴市**（[汉语拼音](../Page/汉语拼音.md "wikilink")：jiā xìng，吴语拼音：Kā
shīng，[IPA](../Page/国际音标.md "wikilink")：/gᴀɕiŋ/）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[浙江省下辖的](../Page/浙江省.md "wikilink")[地级市](../Page/地级市.md "wikilink")。位于浙江省北部，[上海](../Page/上海.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[苏州三座江南名城的中间位置](../Page/苏州.md "wikilink")，属于[长三角地区核心地带](../Page/长三角地区.md "wikilink")。是国家园林城市、卫生城市、国家级历史文化名城、全国文明城市、双拥模范城、中国优秀旅游城市。

## 历史

嘉兴的历史长达7000年，这里也是中国南方文明的发源地之一。是环太湖文化区的发源地。可追溯至[新石器时代](../Page/新石器时代.md "wikilink")，为[马家浜文化发祥地](../Page/马家浜文化.md "wikilink")。为中国历朝历代的粮食提供地，被誉为“天下粮仓”。

春秋时，此地名长水，又称槜李，吴越两国在此角逐。战国时，划入楚境。秦置由拳县、海盐县，属[会稽郡](../Page/会稽郡.md "wikilink")。
三国时吴国析由拳县南境、海盐县西境置盐官县。吴黄龙三年（231），“由拳野稻自生”，吴大帝[孙权以为祥瑞](../Page/孙权.md "wikilink")，改由拳为禾兴，[赤乌五年](../Page/赤乌.md "wikilink")（242）改称嘉兴。
[隋朝开凿江南河](../Page/隋朝.md "wikilink")，即杭州经嘉兴到镇江的大运河，给嘉兴带来灌溉舟楫之利。
[缩略图](https://zh.wikipedia.org/wiki/File:Bridge_in_Wuzhen_02.JPG "fig:缩略图")
唐代嘉兴屯田27处，“浙西三屯，嘉禾为大”，嘉兴成为中国东南重要产粮区。
[北宋改秀州为嘉禾郡](../Page/北宋.md "wikilink")，[南宋宁宗庆元元年](../Page/南宋.md "wikilink")（1195）升郡为府，后改嘉兴郡。
明[宣德四年](../Page/宣德.md "wikilink")（1429）析嘉兴县西北境为秀水县，析东北境为嘉善县；析海盐县置平湖县；析崇德县置桐乡县，嘉兴府下辖7县，称一府七县。此后四五百年内[嘉兴府县体制基本未再变动](../Page/嘉兴府.md "wikilink")。嘉兴府因其重要性在[明朝期间曾由](../Page/明朝.md "wikilink")[中书省直接管辖](../Page/中书省.md "wikilink")。
明末清初的清[顺治二年农历闰六月二十六日](../Page/顺治二年.md "wikilink")（1645年8月6日），清军攻破此处，因为嘉兴军民拒绝接受清朝廷的[剃发令](../Page/剃发令.md "wikilink")，后进行了惨绝人寰的[嘉兴屠城](../Page/嘉兴屠城.md "wikilink")，使嘉兴不复当年繁华，通过调查，当年嘉兴被屠杀50万人，嘉兴的社会发展、经济、文化传播等一落千丈。
清朝中期，多次对杭州湾沿岸海塘进行修筑，嘉兴社会经济逐渐好转，市镇恢复繁荣。

民国十一年(1921)年8月2日，[中国共产党第一次全国代表大会在](../Page/中国共产党第一次全国代表大会.md "wikilink")[嘉兴南湖的一艘游船完成了大会的议程](../Page/嘉兴南湖.md "wikilink")。1993年11月，嘉兴城区更名为秀城区；1999年6月，郊区更名为秀洲区，2005年嘉兴秀城区更名为南湖区。

## 地理

**嘉兴市**位于[浙江省东北部](../Page/浙江省.md "wikilink")、[长江三角洲](../Page/长江三角洲.md "wikilink")[杭嘉湖平原腹地](../Page/杭嘉湖平原.md "wikilink")，是[长江三角洲及](../Page/长江三角洲.md "wikilink")[京杭大运河沿线的重要城市之一](../Page/京杭大运河.md "wikilink")，是中国唯一一个[京杭大运河绕城一圈的城市](../Page/京杭大运河.md "wikilink")。市境介于东经120°18’至121°16’与北纬30°21’至3l°2’之间，东临东海，西接[天目苕溪](../Page/天目苕溪.md "wikilink")，[大运河贯穿境内](../Page/大运河.md "wikilink")。嘉兴市境陆域东西长92公里，南北宽76公里，陆地面积3915平方公里，其中平原面积3477平方公里，水域面积328平方公里，丘陵山地40平方公里，市境海域4650平方公里。市境地势低平，平均海拔3.7米([吴淞高程](../Page/吴淞高程.md "wikilink"))。全市有山丘200余个，零星分布于[钱塘江](../Page/钱塘江.md "wikilink")[杭州湾北岸一带](../Page/杭州湾.md "wikilink")，海拔多在200米以下，市内最高点是位于[海盐县与](../Page/海盐县.md "wikilink")[海宁市交界处的](../Page/海宁市.md "wikilink")[高阳山](../Page/高阳山.md "wikilink")，海拔高度为251.6米。嘉兴地势大致呈东南向西北倾斜，市内河道纵横，湖泊众多，河道全长1.38万余公里，骨干河流57条，内河航运发达。

属于[副热带季风气候](../Page/副热带季风气候.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>嘉兴市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党嘉兴市委员会.md" title="wikilink">中国共产党<br />
嘉兴市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/嘉兴市人民代表大会.md" title="wikilink">嘉兴市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/嘉兴市人民政府.md" title="wikilink">嘉兴市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议嘉兴市委员会.md" title="wikilink">中国人民政治协商会议<br />
嘉兴市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/张兵.md" title="wikilink">张兵</a>[1]</p></td>
<td><p><a href="../Page/刘冬生.md" title="wikilink">刘冬生</a>[2]</p></td>
<td><p><a href="../Page/毛宏芳.md" title="wikilink">毛宏芳</a>[3]</p></td>
<td><p><a href="../Page/高玲慧.md" title="wikilink">高玲慧</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/宁阳县.md" title="wikilink">宁阳县</a></p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/靖江市.md" title="wikilink">靖江市</a></p></td>
<td><p><a href="../Page/浙江省.md" title="wikilink">浙江省</a><a href="../Page/宁波市.md" title="wikilink">宁波市</a></p></td>
<td><p>浙江省<a href="../Page/绍兴市.md" title="wikilink">绍兴市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年7月</p></td>
<td><p>2014年1月</p></td>
<td><p>2018年7月</p></td>
<td><p>2014年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管3个[县级市](../Page/县级市.md "wikilink")。2006年废除市级规划，开始一体化市域规划，与下属县市区统一规划。

  - 市辖区：[南湖区](../Page/南湖区.md "wikilink")、[秀洲区](../Page/秀洲区.md "wikilink")
  - 县级市：[海宁市](../Page/海宁市.md "wikilink")、[平湖市](../Page/平湖市.md "wikilink")、[桐乡市](../Page/桐乡市.md "wikilink")
  - 县：[嘉善县](../Page/嘉善县.md "wikilink")、[海盐县](../Page/海盐县.md "wikilink")

正式行政区外设立以下经济功能区：[嘉兴经济技术开发区](../Page/嘉兴经济技术开发区.md "wikilink")、[嘉兴工业园区](../Page/嘉兴工业园区.md "wikilink")、[嘉兴港区](../Page/嘉兴港区.md "wikilink")、[独山港区](../Page/独山港区.md "wikilink")。

其下属的各县市全部进入[中国百强县的前](../Page/中国百强县.md "wikilink")35强，如此满堂红在中国实属罕见。

<table>
<thead>
<tr class="header">
<th><p><strong>嘉兴市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>330400</p></td>
</tr>
<tr class="odd">
<td><p>330402</p></td>
</tr>
<tr class="even">
<td><p>330411</p></td>
</tr>
<tr class="odd">
<td><p>330421</p></td>
</tr>
<tr class="even">
<td><p>330424</p></td>
</tr>
<tr class="odd">
<td><p>330481</p></td>
</tr>
<tr class="even">
<td><p>330482</p></td>
</tr>
<tr class="odd">
<td><p>330483</p></td>
</tr>
<tr class="even">
<td><p>注：南湖区数字包含嘉兴经济技术开发区所辖城南、长水2街道及嘉兴工业园区所辖大桥镇；秀洲区数字包含嘉兴经济技术开发区所辖嘉北、塘汇2街道；海盐县数字包含海盐经济开发区所辖西塘桥街道；海宁市数字包含连杭经济开发区所辖2镇；平湖市数字包含平湖经济开发区所辖钟棣街道，嘉兴港区所辖乍浦镇及独山港区所辖独山港镇。</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>嘉兴市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>嘉兴市</p></td>
<td><p>4501657</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>南湖区</p></td>
<td><p>612663</p></td>
<td><p>13.61</p></td>
</tr>
<tr class="even">
<td><p>秀洲区</p></td>
<td><p>589219</p></td>
<td><p>13.09</p></td>
</tr>
<tr class="odd">
<td><p>嘉善县</p></td>
<td><p>574187</p></td>
<td><p>12.76</p></td>
</tr>
<tr class="even">
<td><p>海盐县</p></td>
<td><p>430940</p></td>
<td><p>9.57</p></td>
</tr>
<tr class="odd">
<td><p>海宁市</p></td>
<td><p>806966</p></td>
<td><p>17.93</p></td>
</tr>
<tr class="even">
<td><p>平湖市</p></td>
<td><p>671834</p></td>
<td><p>14.92</p></td>
</tr>
<tr class="odd">
<td><p>桐乡市</p></td>
<td><p>815848</p></td>
<td><p>18.12</p></td>
</tr>
<tr class="even">
<td><p>注：常住人口数据中，南湖区数字包含嘉兴工业园区57192人；海宁市数字包含连杭经济开发区228615人；平湖市数字包含嘉兴港区83527人及独山港区105643人。单列时，嘉兴经济技术开发区为186829人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

截至到2010年12月，嘉兴市常住人口为450.17万人，户籍人口为343.05万人，市外人口为122.41万人，户口待定人口2.64万人，人口出生率为7.20%。人口自然增长率为0.27‰,为自1995年以来，第二次人口自然增长率为“正”，2010年为0.22‰，2011年为0.27‰。

据嘉兴市公安局统计，截至2008年底，嘉兴市有外来暂住人口221万，其中来源最多的为[湖南省](../Page/湖南省.md "wikilink")（46万）、[重庆市](../Page/重庆市.md "wikilink")（44万）、[江西省](../Page/江西省.md "wikilink")（29万）、[安徽省](../Page/安徽省.md "wikilink")（22万）、[四川省](../Page/四川省.md "wikilink")（18.5万）。

### 全市常住人口

全市常住人口为450.17万人，同第五次全国人口普查2000年11月1日零时的358.30万人相比，十年共增加91.87万人，增长25.64%，年平均增长率为2.31%。

### 家庭户人口

全市常住人口中共有家庭户144.95万户，家庭户人口为417.68万人，平均每个家庭户的人口为2.88人，比2000年第五次全国人口普查的3.38人减少0.50人。

### 性别构成

全市常住人口中，男性人口为226.92万人，占50.41%；女性人口为223.25万人，占49.59%。总人口性别比（以女性为100，男性对女性的比例）由2000年第五次全国人口普查的100.58上升为101.65。

### 年龄构成

全市常住人口中，0-14岁人口为53.69万人，占11.93%；15-59岁人口为328.95万人，占73.07%；60岁及以上人口为67.52万人，占15.00%，其中65岁及以上人口为44.98万人，占9.99%。同2000年第五次全国人口普查相比，0-14岁人口的比重下降5.87个百分点，15-59岁人口的比重上升4.22个百分点，60岁及以上人口的比重上升1.65个百分点，65岁及以上人口的比重上升0.40个百分点。

### 各种受教育程度人口

全市常住人口中，具有大学（指大专以上）程度的人口为34.56万人；具有高中（含中专）程度的人口为53.17万人；具有初中程度的人口为178.79万人；具有小学程度的人口为131.31万人（以上各种受教育程度的人包括各类学校的毕业生、肄业生和在校生）。

同2000年第五次全国人口普查相比，每10万人中具有大学程度的由2229人上升为7676人；具有高中程度的人口由9457人上升为11811人；具有初中程度的人口由32402人上升为39717人；具有小学程度的人口由38938人下降为29169人。

全市常住人口中，文盲人口（15岁及以上不识字的人）为29.36万人，同2000年第五次全国人口普查相比，文盲人口占常住人口的比率由7.93%下降为6.52%，下降1.41个百分点。

### 城乡构成

全市常住人口中，居住在城镇的人口为240.07万人，占53.33%；居住在乡村的人口为210.10万人，占46.67%。与2000年第五次全国人口普查相比，城镇人口增加了103.96万人，乡村人口减少了12.10万人，城镇人口比重上升了15.36个百分点。

## 文化

[缩略图](https://zh.wikipedia.org/wiki/File:Jiaxing_wufangzhai.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Gazetteer_of_Puzhen_WDL4737.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Former_Residence_of_Shen_Junru_02_2013-11.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Juehai_Temple_in_Jiaxing_02_2013-11.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Former_Residence_of_Shen_Cengzhi_10_2013-11.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Gao's_Foreign-style_House_in_Jiaxing_02_2013-11.JPG "fig:缩略图")

### 语言

语言：[吴语](../Page/吴语.md "wikilink")（太湖片）

嘉兴全境通行吴语，历史上是吴越交界处，有吴根越角之称。嘉兴吴语兼具[苏州话的软](../Page/苏州话.md "wikilink")，[上海话的嗲](../Page/上海话.md "wikilink")，[宁波话的硬](../Page/宁波话.md "wikilink")，特色鲜明，与周边的湖州，苏州、绍兴，杭州等地方言极为接近，基本可以互通，是北部吴语中较有典型性的一门方言。

历史上嘉兴属于[苏州](../Page/苏州.md "wikilink")，后分出[秀州](../Page/秀州.md "wikilink")，其后秀州又分出[华亭](../Page/华亭.md "wikilink")，而华亭为今日[上海](../Page/上海.md "wikilink")[松江一带](../Page/松江.md "wikilink")，世人皆言嘉兴吴语和上海吴语在读音、语调、语法等方面最为接近。事实上[上海话最原始的状态差不多等同于今日](../Page/上海话.md "wikilink")[嘉兴话](../Page/嘉兴话.md "wikilink")，故而常有嘉兴人到其他城市被误认为是上海人。吴文化的因素和较近的地理关系也使这两座城市关系特别亲厚，沪嘉两地各方面交流今年愈加频繁，大有实现同城化之势。

随着推普大潮的到来，吴语在嘉兴日渐式微，不少嘉兴人甚至根本不知道自己说的[嘉兴话就是吴语](../Page/嘉兴话.md "wikilink")。由于吴语被驱赶出主流媒体的舞台，很多嘉兴孩子和周边的上海、苏州的孩子一样，从小在电视上和学校里只接触普通话，另一方面由于不少父母都刻意不教孩子吴语，导致很多嘉兴孩子失去了学习吴语的机会和途径。对相当数量的嘉兴孩子来说，吴语已经不是他们的母语，而是一门只有爷爷奶奶才会说的方言。他们多半会听不会讲，连“姆妈”“吾奴”“上昼下昼”“氽油条”“死蟹一只”“做死腔”“汏衣裳”“困晏觉”这样简单的吴语都不会说，甚至连嘉兴人最引以为豪的[五芳斋粽子的](../Page/五芳斋.md "wikilink")“斋”字很多孩子都不会念。在嘉兴常有老人抱怨，孙子孙女不会说土话（嘉兴人对本地吴语的称呼）导致祖孙感情交流出现障碍，更有甚者因此而引发家庭内部矛盾。

### 文化

[吴越文化的概念在新一代嘉兴孩子心中已经消散](../Page/吴越文化.md "wikilink")。吴语的断代问题让不少持不同意见的的热心人士极为忧虑，近年于江南一带，有热心人士发出“保护吴语”“教孩子说吴语”“留住江南文化的根”“尊重[文化多样性](../Page/文化多样性.md "wikilink")”等呼声，上海、苏州、[杭州等地接连有吴语爱好者进行宣传保护吴语的活动](../Page/杭州.md "wikilink")，嘉兴本地也渐渐有一些小规模的保护和研究吴语的团体，[嘉兴日报等官方媒体开始关注吴语的生存危局并时有正面报道](../Page/嘉兴日报.md "wikilink")。

### 风俗

每年农历五月初五的时候，嘉兴的居民也会庆祝端午节。可是他们纪念的不是屈原，而是嘉兴二代城的开城者——[伍子胥](../Page/伍子胥.md "wikilink")，伍子胥在这一天建立嘉兴二代城，而且很巧合的事情是，这一天也是伍子胥尸体被抛入钱塘江的日子。所以嘉兴人对于伍子胥感情很深厚。

### 著名人物

嘉兴自古以来崇尚学习，历来名人辈出。

  - [金庸](../Page/金庸.md "wikilink") － 作家、政论家、社会活动家
  - [沈曾植](../Page/沈曾植.md "wikilink") － 同光体诗人、书法家、晚清名臣
  - [茅盾](../Page/茅盾.md "wikilink") － 现代作家，文学评论家
  - [沈钧儒](../Page/沈钧儒.md "wikilink") － 著名社会活动家，政治活动家，律师
  - [丰子恺](../Page/丰子恺.md "wikilink") － 近代著名散文家、畫家、文學家、美術與音樂教育家
  - [陈省身](../Page/陈省身.md "wikilink") － 数学家，微分几何大师
  - [朱生豪](../Page/朱生豪.md "wikilink") － 翻译家
  - [张乐平](../Page/張樂平.md "wikilink") － 漫画家
  - [朱彝尊](../Page/朱彝尊.md "wikilink") － 清代诗人
  - [吴镇](../Page/吴镇.md "wikilink") － 元代画家
  - [董卿](../Page/董卿.md "wikilink") － 央视著名主持人
  - [余华](../Page/余华.md "wikilink") －
    著名小说家，著有《[活着](../Page/活着.md "wikilink")》等小说
  - [孙道临](../Page/孙道临.md "wikilink") －
    著名表演艺术家，代表作《[雷雨](../Page/雷雨.md "wikilink")》
  - [李叔同](../Page/李叔同.md "wikilink") － 弘一法师
  - [黄菊](../Page/黄菊.md "wikilink") － 中共领导人, 政治家
  - [木心](../Page/木心.md "wikilink") － 画家、作家、詩人
  - [张元济](../Page/張元濟.md "wikilink") - 出版家

### 其他名人

#### 文学名人

嚴忌、嚴助、朱買臣、丘為、劉禹錫、殷堯藩、陸贽、褚無量、文偃、婁機、梵琦、朱淑真、張伯淳、路正、貝琼、程本立、李日華、王翃、俞汝言、魏大中、錢士升、錢棅、屠勛、馮汝弼、過庭訓、吳鵬、李天植、胡震亨、朱妙端、祝萃、陳與郊、朱彝尊、吕留良、張廷濟、胡小石、張宗祥、徐震堮、胡士瑩、項定榮、[王国维](../Page/王国维.md "wikilink")、[丰子恺](../Page/丰子恺.md "wikilink")、[沈德鸿](../Page/沈德鸿.md "wikilink")(茅盾)、[徐志摩](../Page/徐志摩.md "wikilink")、[金庸](../Page/金庸.md "wikilink")

#### 艺术名人

顾况、姚绶、倪瓒、陆晃、许远、徐再思、张成、张德刚、杨茂、盛懋、徐石麒、张涟、魏学洢、黄媛介、朱琰、沙可夫、米谷、许志锋、[丁噹](../Page/丁噹.md "wikilink")、[方逸倫](../Page/方逸倫.md "wikilink")

#### 科技名人

贾祖璋、[李善兰](../Page/李善兰.md "wikilink")、朱琰、沈骊英、陈世骧、陈毓川、[程开甲](../Page/程开甲.md "wikilink")、程庆国、程浴淇、褚应璜、丁舜年、高尚荫、顾功叙、黄昆、姜文汉、倪嘉缵、钱崇澍、钱绍钧、钱正英、钦俊德、沈国舫、沈天慧、沈闻孙、苏元复、谭其骧、陶诗言、屠善澄、屠守锷、汪胡桢、吴澄、徐匡迪、张效祥、张直中、张钟俊、周廷冲、周廷儒、周志、朱夏、邹竞、[陈省身](../Page/陈省身.md "wikilink")

#### 政治名人

卫泾、赵汝愚、钱士晋、金仲华、黄源、沈钧儒、沈泽民、徐永祚、严独鹤、陆费逵、陆宗舆

#### 教育名人

沈鼎三、查良钊、侯家声、虞尔昌、曹云祥、张印通

#### 军事名人

钱旃、陆炳、陆逊、郑晓、祝以豳、蒋百里

### 特产

  - [粽子](../Page/粽子.md "wikilink")
  - [南湖菱](../Page/南湖菱.md "wikilink")
  - [杭白菊](../Page/杭白菊.md "wikilink")
  - [斜桥榨菜](../Page/斜桥榨菜.md "wikilink")
  - [凤桥水蜜桃](../Page/凤桥水蜜桃.md "wikilink")
  - [文虎酱鸭](../Page/文虎酱鸭.md "wikilink")
  - [宏达烧鸡](../Page/宏达烧鸡.md "wikilink")
  - [蓝印花布](../Page/蓝印花布.md "wikilink")

## 教育

### 中学教育

#### 南湖区

  - [嘉兴市第一中学](../Page/嘉兴市第一中学.md "wikilink")
  - [北京师范大学南湖附属学校](../Page/北京师范大学南湖附属学校.md "wikilink")
  - [嘉兴一中实验中学](../Page/嘉兴一中实验中学.md "wikilink")
  - [秀州中学](../Page/秀州中学.md "wikilink")
  - [嘉兴高级中学](../Page/嘉兴高级中学.md "wikilink")
  - [嘉兴市第三中学](../Page/嘉兴市第三中学.md "wikilink")
  - [嘉兴市第四高级中学](../Page/嘉兴市第四高级中学.md "wikilink")
  - [嘉兴市南湖高级中学](../Page/嘉兴市南湖高级中学.md "wikilink")
  - [嘉兴市第五高级中学](../Page/嘉兴市第五高级中学.md "wikilink")

### 高等教育

  - [嘉兴学院](../Page/嘉兴学院.md "wikilink")
  - [嘉兴学院南湖学院](../Page/嘉兴学院南湖学院.md "wikilink")
  - [嘉兴职业技术学院](../Page/嘉兴职业技术学院.md "wikilink")
  - [嘉兴南洋职业技术学院](../Page/嘉兴南洋职业技术学院.md "wikilink")
  - [同济大学浙江学院](../Page/同济大学浙江学院.md "wikilink")
  - [浙江嘉兴教育学院](../Page/浙江嘉兴教育学院.md "wikilink")
  - [浙江大学国际校区](../Page/浙江大学海宁国际校区.md "wikilink")
  - [浙江财经大学东方学院](../Page/浙江财经大学东方学院.md "wikilink")

## 工业园区

### 嘉兴工业园区

### 嘉兴科技城

嘉兴科技城，创建于2003年底，是嘉兴市引进智力资源，提高自主创新能力，以达到调整经济结构、促进经济转型升级目的所进行的一项重要实践。嘉兴市南湖区政府派出管理委员会，下设投资公司，负责嘉兴科技城的运营开发。嘉兴科技城以平台建设为主要任务，通过引进大院名校，打造产业园区等手段，目前已经形成以浙江清华长三角研究院和中国科学院嘉兴应用技术研究与转化中心为核心的应用技术研发和转化平台，以孵化园、软件园、通讯园、芯片园、生物园、材料园为载体的产业孵化和培育平台。嘉兴科技城一期园区3.65平方公里，二期园区计划拓展至10.67平方公里。

### 浙江嘉善经济开发区

浙江嘉善经济开发区\[8\]于1993年经浙江省人民政府批准设立为省级经济开发区，总规划面积65.5平方公里，分设电子信息产业园、精密机械工业园、台湾螺丝城及国际家具园四个专业园区。至今开发区已完成开发面积为20平方公里。嘉善是浙江省外商最密集的区域之一，至今已形成了电子信息、精密机械、木业家俱、纺织服装等专业园区。开发区主要以工业为主，以配套服务为辅，电子信息、精密机械、先进装备制造业及现代服务业是嘉善政府重点鼓励发展的产业方向。全区基础设施已达到“九通一平”的条件。

### 嘉兴德国产业园

嘉兴产业园\[9\]共分三个园区：园区A、园区B以及园区C，一小时内可到达上海、杭州、苏州或宁波。园区的战略发展方向主要集中在以下的几种产业：来自德国、奥地利、瑞士的企业项目；环保装备项目；轻工业；手工制造业；汽车配件及整车制造业；家用电器；电子制造业；IT及半导体；物流；服务性行业。

## 交通

嘉兴市内的交通极其方便，可以使用多种交通工具，并且公路等级都较高，路况较好。嘉兴在[沪杭铁路](../Page/沪杭铁路.md "wikilink")、[沪杭高速公路](../Page/沪杭高速公路.md "wikilink")、[苏嘉杭高速公路的交汇处](../Page/苏嘉杭高速公路.md "wikilink")，与外地交通亦高度便捷。在其一小时到达圈内，可以前往[上海虹桥国际机场](../Page/上海虹桥国际机场.md "wikilink")、[上海浦东国际机场](../Page/上海浦东国际机场.md "wikilink")、[杭州萧山国际机场等空港](../Page/杭州萧山国际机场.md "wikilink")，并可以抵达[嘉兴港](../Page/嘉兴港.md "wikilink")、[杭州港](../Page/杭州港.md "wikilink")、[上海外高桥港](../Page/上海外高桥港.md "wikilink")、[上海芦潮港](../Page/上海芦潮港.md "wikilink")、[舟山港](../Page/舟山港.md "wikilink")、[宁波港](../Page/宁波港.md "wikilink")、[洋山港等大型海港](../Page/洋山港.md "wikilink")。并且正在规划建造[嘉兴市域轨道交通](../Page/嘉兴市域轨道交通.md "wikilink")（[大嘉兴市域轨道交通](../Page/大嘉兴市域轨道交通.md "wikilink")，[嘉兴地方铁道](../Page/嘉兴地方铁道.md "wikilink")）。

在高速公路不发达的年代，嘉兴以水运为主，年发送旅客在2亿人次。铁道年发送旅客750万到800万人次。公路客运年发送旅客是3900万到4200万人次。嘉兴市本级城市公交年发送旅客是5900万到6450万人次。目前嘉兴市本级汽车拥有量是170万辆（私用110万），密度1辆/2.8人。

### 公路

  -
  -
  -
  -
  - （江浙省界至南湖枢纽为部分，苏州称[苏嘉杭高速公路](../Page/苏嘉杭高速公路.md "wikilink")）

  -
  -
  -
  -
  -
  - （嘉兴境内仅有桐乡西一个出口）

### 铁路

  - [沪杭铁路](../Page/沪杭铁路.md "wikilink")（[沪昆铁路](../Page/沪昆铁路.md "wikilink")），境内设有[嘉兴站](../Page/嘉兴站.md "wikilink")
  - [沪杭客运专线](../Page/沪杭客运专线.md "wikilink")，境内设有[嘉兴南站](../Page/嘉兴南站.md "wikilink")

### 水运

  - [京杭大运河](../Page/京杭大运河.md "wikilink")
  - [杭申线](../Page/杭申线.md "wikilink")
  - [六平申线](../Page/六平申线.md "wikilink")
  - [乍嘉苏线](../Page/乍嘉苏线.md "wikilink")
  - [湖嘉申线](../Page/湖嘉申线.md "wikilink")

## 旅游

  - [嘉善縣](../Page/嘉善縣.md "wikilink")[西塘](../Page/西塘镇_\(嘉善县\).md "wikilink")
  - [海鹽縣](../Page/海鹽縣.md "wikilink")[南北湖](../Page/南北湖.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[南湖风景区](../Page/南湖风景区.md "wikilink")[南湖](../Page/嘉兴南湖.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[船文化博物馆](../Page/船文化博物馆.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[月河历史街区](../Page/月河历史街区.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[嘉兴三塔](../Page/嘉兴三塔.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[梅湾街](../Page/梅湾街.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[七星镇](../Page/七星镇.md "wikilink")[湘家荡](../Page/湘家荡.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[双魁巷](../Page/双魁巷.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[子城](../Page/子城.md "wikilink")[嘉兴内城](../Page/嘉兴内城.md "wikilink")
  - [南湖区](../Page/南湖区.md "wikilink")[落帆亭](../Page/落帆亭.md "wikilink")
  - [秀洲区](../Page/秀洲区.md "wikilink")[新塍镇](../Page/新塍镇.md "wikilink")[新塍古镇](../Page/新塍古镇.md "wikilink")
  - [秀洲区](../Page/秀洲区.md "wikilink")[王江泾镇](../Page/王江泾镇.md "wikilink")[油车港镇](../Page/油车港镇.md "wikilink")[莲泗荡](../Page/莲泗荡.md "wikilink")
  - [秀洲区](../Page/秀洲区.md "wikilink")[王店镇](../Page/王店镇.md "wikilink")[朱彝尊](../Page/朱彝尊.md "wikilink")[曝书亭](../Page/曝书亭.md "wikilink")

### 友好城市

#### 国内

  - [湖南省](../Page/湖南省.md "wikilink")[怀化市](../Page/怀化市.md "wikilink")
  - [陕西省](../Page/陕西省.md "wikilink")[延安市](../Page/延安市.md "wikilink")
  - [广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[玉林市](../Page/玉林市.md "wikilink")
  - [广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[来宾市](../Page/来宾市.md "wikilink")
  - [黑龙江省](../Page/黑龙江省.md "wikilink")[牡丹江市](../Page/牡丹江市.md "wikilink")
  - [甘肃省](../Page/甘肃省.md "wikilink")[嘉峪关市](../Page/嘉峪关市.md "wikilink")
  - [四川省](../Page/四川省.md "wikilink")[广元市](../Page/广元市.md "wikilink")
  - [上海市](../Page/上海市.md "wikilink")

#### 国际

  - [静冈县](../Page/静冈县.md "wikilink")[富士市](../Page/富士市.md "wikilink")(1989年1月13日)（[平成元年](../Page/平成元年.md "wikilink")[平成](../Page/平成.md "wikilink")1年）

  - [萨克森-安哈尔特州](../Page/萨克森-安哈尔特州.md "wikilink")[哈雷
    (德国)](../Page/哈雷_\(德国\).md "wikilink") 2009年9月22日

  - [江原道](../Page/江原道.md "wikilink")[江陵市](../Page/江陵市.md "wikilink")(1999年6月11日)

  - [西澳大利亚州](../Page/西澳大利亚州.md "wikilink")[班伯利
    (西澳)](../Page/班伯利_\(西澳\).md "wikilink")(2000年10月13日)

  - [西福尔郡](../Page/西福尔.md "wikilink")[桑德尔福德](../Page/桑德尔福德.md "wikilink")(2001年5月11日)

  - [伊利諾州](../Page/伊利諾州.md "wikilink")[昆西](../Page/昆西.md "wikilink")(2004年11月24日)

  - [印第安纳州](../Page/印第安纳州.md "wikilink")[门罗县](../Page/门罗县.md "wikilink")[布鲁明顿](../Page/布鲁明顿.md "wikilink")　1994年5月20日

  - [佛罗里达州](../Page/佛罗里达州.md "wikilink")[坦帕](../Page/坦帕.md "wikilink")
    2001年6月6日

  - [里约热内卢州](../Page/里约热内卢州.md "wikilink")[尼泰罗伊](../Page/尼泰罗伊.md "wikilink")　2002年5月24日

  - [斯特林市](../Page/斯特林市.md "wikilink") 2004年9月23日

  - [蓬萨科市](../Page/蓬萨科市.md "wikilink") 　2001年9月1日

  - [哈博罗内](../Page/哈博罗内.md "wikilink")[洛巴策](../Page/洛巴策.md "wikilink")
    　2002年5月9日

  - [达蒙维市](../Page/达蒙维市.md "wikilink") 2004年3月19日

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [嘉兴市政府网站](http://www.jiaxing.gov.cn/)

[浙](../Category/国家历史文化名城.md "wikilink")
[Category:嘉兴](../Category/嘉兴.md "wikilink")
[Category:浙江地级市](../Category/浙江地级市.md "wikilink")
[浙](../Category/中国中等城市.md "wikilink")
[Category:长江三角洲城市](../Category/长江三角洲城市.md "wikilink")
[Category:运河城市](../Category/运河城市.md "wikilink")
[浙](../Category/中国共产党革命圣地.md "wikilink")
[3](../Category/全国文明城市.md "wikilink")
[浙](../Category/国家卫生城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  [嘉善经济开发区](http://rightsite.asia/zh-hans/gongyeyuanqu/zhejiangjiashanjingjikaifaqu/)
9.  [嘉兴德国产业园](http://rightsite.asia/zh-hans/gongyeyuanqu/jiaxingdeguochanyeyuan/)