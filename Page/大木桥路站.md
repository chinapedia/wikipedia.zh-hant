**大木桥路站**位于[上海](../Page/上海.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")[大木桥路](../Page/大木桥路.md "wikilink")[零陵路](../Page/零陵路.md "wikilink")，为地下[岛式车站](../Page/岛式站台.md "wikilink")。[轨道交通4号线修复段開通前](../Page/上海轨道交通4号线.md "wikilink")，是顺时针方向上的起点站（仅上客）和逆时针方向上的终点站（仅下客）。在2007年12月29日至2009年9月28日期间，为内圈首班列车的始发站。

[轨道交通12号线的车站于](../Page/上海轨道交通12号线.md "wikilink")2015年12月19日开通，为岛式车站

[日本連鎖](../Page/日本.md "wikilink")[便利商店集團](../Page/便利商店.md "wikilink")[FamilyMart在大木桥路站設有分店](../Page/FamilyMart.md "wikilink")。

## 月台構造

[島式月台](../Page/島式月台.md "wikilink")1面2線地下車站。

  - 月台配置

<table>
<thead>
<tr class="header">
<th><p>外圈</p></th>
<th><p>4号线</p></th>
<th><p><a href="../Page/西藏南路站.md" title="wikilink">西藏南路</a>・<a href="../Page/蓝村路站.md" title="wikilink">蓝村路</a>・<a href="../Page/世纪大道站_(上海).md" title="wikilink">世纪大道</a>・<a href="../Page/宝山路站.md" title="wikilink">宝山路方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>内圈</p></td>
<td><p>4号线</p></td>
<td><p><a href="../Page/东安路站.md" title="wikilink">东安路</a>・<a href="../Page/上海体育馆站.md" title="wikilink">上海体育馆</a>・<a href="../Page/宜山路站.md" title="wikilink">宜山路</a>・<a href="../Page/中山公园站.md" title="wikilink">中山公园方向</a></p></td>
</tr>
</tbody>
</table>

## 公交换乘

41、45、144、327、572、527路区间、734、933、隧道二线、隧道七线、隧道夜宵一线、隧道一线、隧道一线区间、大桥六线、大桥六线区间、万野专线、89、104、128、932、938、隧道八线

## 车站出口

  - 1号口：大木桥路东侧，零陵路北（暂未开通）
  - 2号口：大木桥路东侧，零陵路南
  - 3号口：零陵路北侧，大木桥路西
  - 4号口：零陵路南侧，大木桥路西

<File:Damuqiao> Road Station.jpg|大木桥路站4号线 <File:Damuqiao> Road Station
FamilyMart.jpg|位於大木桥路站4号线站廳的[FamilyMart分店](../Page/FamilyMart.md "wikilink")

## 临近车站

[Category:徐汇区地铁车站](../Category/徐汇区地铁车站.md "wikilink")
[Category:2005年启用的铁路车站](../Category/2005年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")