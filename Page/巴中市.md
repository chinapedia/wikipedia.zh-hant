**巴中市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[四川省下辖的](../Page/四川省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于四川省东北部。东邻[达州市](../Page/达州市.md "wikilink")，南接[南充市](../Page/南充市.md "wikilink")，西界[广元市](../Page/广元市.md "wikilink")，北连[陕西省](../Page/陕西省.md "wikilink")[汉中市](../Page/汉中市.md "wikilink")。地处[四川盆地北部边缘](../Page/四川盆地.md "wikilink")，北面为[米仓山](../Page/米仓山.md "wikilink")、[大巴山区](../Page/大巴山.md "wikilink")，南部则为丘陵区。[南江由北向南流经城区](../Page/南江.md "wikilink")，后段称[巴河](../Page/巴河.md "wikilink")。全市总面积1.23万平方公里，人口332.86万。

## 历史

古时为[巴国地](../Page/巴国.md "wikilink")，[秦国灭巴国后置](../Page/秦.md "wikilink")[巴郡](../Page/巴郡.md "wikilink")。[西汉属巴郡](../Page/西汉.md "wikilink")[宕渠县](../Page/宕渠县.md "wikilink")。[东汉](../Page/东汉.md "wikilink")[永元三年](../Page/永元.md "wikilink")（91年）析宕渠县北置[汉昌县](../Page/汉昌县.md "wikilink")，治今巴州城区。[建安六年](../Page/建安.md "wikilink")（201年），巴郡改称[巴西郡](../Page/巴西郡.md "wikilink")。[蜀汉至](../Page/蜀汉.md "wikilink")[南北朝年间曾屡置](../Page/南北朝.md "wikilink")[宕渠郡](../Page/宕渠郡.md "wikilink")，汉昌县属之。[北魏](../Page/北魏.md "wikilink")[正始元年](../Page/正始.md "wikilink")（504年）于汉昌县置[大谷郡](../Page/大谷郡.md "wikilink")。[延昌三年](../Page/延昌.md "wikilink")（514年）置[巴州](../Page/巴州.md "wikilink")，以古巴国为名。[熙平二年](../Page/熙平.md "wikilink")（517年）移巴州治至汉昌县。[南朝梁](../Page/南朝梁.md "wikilink")[普通六年](../Page/普通_\(梁\).md "wikilink")（525年）于汉昌县置[梁广县](../Page/梁广县.md "wikilink")，为巴州大谷郡治。[北周](../Page/北周.md "wikilink")[大象二年](../Page/大象_\(周\).md "wikilink")（580年）改[化成县](../Page/化成县.md "wikilink")。

[隋](../Page/隋朝.md "wikilink")[大业三年](../Page/大业.md "wikilink")（607年）废巴州，置[清化郡](../Page/清化郡.md "wikilink")，治化成县。[唐](../Page/唐朝.md "wikilink")[武德元年](../Page/武德.md "wikilink")（618年）改清化郡为巴州。[贞观元年](../Page/贞观.md "wikilink")（627年）巴州属[山南道](../Page/山南道.md "wikilink")。[开元二十一年](../Page/开元.md "wikilink")（733年）属[山南西道](../Page/山南西道.md "wikilink")。[宋代巴州属](../Page/宋代.md "wikilink")[利州路](../Page/利州路.md "wikilink")。[元代属](../Page/元代.md "wikilink")[广元路](../Page/广元路.md "wikilink")。[明](../Page/明朝.md "wikilink")[洪武九年](../Page/洪武.md "wikilink")（1376年）省化成县入巴州，并降巴州为[巴县](../Page/巴县.md "wikilink")。[正德九年](../Page/正德.md "wikilink")（1514年）复置巴州，属[保宁府](../Page/保宁府.md "wikilink")。[清代因明制](../Page/清代.md "wikilink")。

[民国二年](../Page/民国.md "wikilink")（1913年）改巴州为[巴中县](../Page/巴中县.md "wikilink")，属[川北道](../Page/川北道.md "wikilink")，次年改属[嘉陵道](../Page/嘉陵道.md "wikilink")。1928年废道制。1933年至1935年间，[中国工农红军](../Page/中国工农红军.md "wikilink")[第四方面军曾在此建立](../Page/第四方面军.md "wikilink")[川陕省巴中县等](../Page/川陕省.md "wikilink")[苏维埃政府](../Page/苏维埃.md "wikilink")。民国二十四年（1935年）巴中县属四川省第十五行政督察区。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，巴中县属[川北行署区](../Page/川北行署区.md "wikilink")[达县专区](../Page/达县专区.md "wikilink")。1952年达县专区由四川省直接领导。1968年达县专区改称[达县地区](../Page/达县地区.md "wikilink")。1993年7月，撤销巴中县，设立县级巴中市；并由达县地区析置[巴中地区](../Page/巴中地区.md "wikilink")，行署驻巴中市，将通江、南江、平昌三县划归巴中地区。2000年6月14日，撤销巴中地区，设立地级巴中市，原县级巴中市改为[巴州区](../Page/巴州区.md "wikilink")。2013年1月18日，由巴州区析置[恩阳区](../Page/恩阳区.md "wikilink")。

## 地理

[巴中市大和乡2013_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:巴中市大和乡2013_-_panoramio.jpg "fig:巴中市大和乡2013_-_panoramio.jpg")
[巴中市大和乡_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:巴中市大和乡_-_panoramio.jpg "fig:巴中市大和乡_-_panoramio.jpg")
巴中位于四川省东北部，[四川盆地边缘地带](../Page/四川盆地.md "wikilink")，[大巴山系](../Page/大巴山.md "wikilink")[米仓山南麓](../Page/米仓山.md "wikilink")，地理范围大致为东经106度21分至107度45分，北纬31度15分至32度45分。平均海拔500米。

经济上以现代旅游观光、生物医药、食品饮料、机械制造和新能源新材料为该市五类重点成长型产业。同时，巴中市是川陕渝绿色食品饮料生产基地，川东北现代生物医药基地，正在建设西部最大石墨新材料产业基地，和四川重要的汽车、摩托车及零部件产业基地。

### 气候

巴中属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，气候温和，四季分明，光照充足，降水充沛，年平均气温16.0—16.9℃，年日照时数1227.3—1574.5小时，年降雨量1120.7—1203.1毫米。

## 资源

境内通江、南江、巴河等主要河流流量大、落差高，水力资源蕴藏量达81.24万千瓦，其中可开发量达41.7万千瓦；淡水资源总面积达14.75万公顷，其中可养殖面积达4.7万公顷；林业，拥有活力木蓄积量2340万立方米，森林面积46.49万公顷，森林覆盖率达47.2%，堪称“绿色宝库”。南江北部山区3000公顷“巴山[水青杠](../Page/水青杠.md "wikilink")”，被专家鉴定为“世界稀有树种”，世界银行已在这里投资开发林牧业。浩翰林海里栖息着[黑熊](../Page/黑熊.md "wikilink")、[金钱豹等](../Page/金钱豹.md "wikilink")20多种国家野生保护动物，被中外专家称为“[四川盆地北缘山地重要的生物基因库](../Page/四川盆地.md "wikilink")”；矿产，境内有可供开采的矿产主要有[煤炭](../Page/煤炭.md "wikilink")、[铁矿](../Page/铁矿.md "wikilink")、[铜矿](../Page/铜矿.md "wikilink")、[金矿](../Page/金矿.md "wikilink")、[大理石](../Page/大理石.md "wikilink")、[花岗石](../Page/花岗石.md "wikilink")、[霞石矿](../Page/霞石矿.md "wikilink")、[石灰矿](../Page/石灰矿.md "wikilink")、[石黑矿](../Page/石黑矿.md "wikilink")、[天然气等](../Page/天然气.md "wikilink")29种，其中煤炭储量6190万吨，[磁铁矿](../Page/磁铁矿.md "wikilink")8355万吨，[天然气](../Page/天然气.md "wikilink")1100亿立方米，[霞石矿](../Page/霞石矿.md "wikilink")2100万吨，[花岗石](../Page/花岗石.md "wikilink")10亿立方米，极具开发潜力。

## 政治

### 现任领导

<table>
<caption>巴中市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党巴中市委员会.md" title="wikilink">中国共产党<br />
巴中市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/巴中市人民代表大会.md" title="wikilink">巴中市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/巴中市人民政府.md" title="wikilink">巴中市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议巴中市委员会.md" title="wikilink">中国人民政治协商会议<br />
巴中市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/罗增斌.md" title="wikilink">罗增斌</a>[1]</p></td>
<td><p><a href="../Page/魏文通_(1958年).md" title="wikilink">魏文通</a>[2]</p></td>
<td><p><a href="../Page/何平_(1964年).md" title="wikilink">何平</a>[3]</p></td>
<td><p><a href="../Page/朱冬.md" title="wikilink">朱冬</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/四川省.md" title="wikilink">四川省</a><a href="../Page/苍溪县.md" title="wikilink">苍溪县</a></p></td>
<td><p>四川省<a href="../Page/南江县.md" title="wikilink">南江县</a></p></td>
<td><p>四川省<a href="../Page/渠县.md" title="wikilink">渠县</a></p></td>
<td><p>四川省<a href="../Page/巴中市.md" title="wikilink">巴中市</a><a href="../Page/恩阳区.md" title="wikilink">恩阳区</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年6月</p></td>
<td><p>2017年1月</p></td>
<td><p>2016年5月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市下辖2个[市辖区](../Page/市辖区.md "wikilink")、3个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[巴州区](../Page/巴州区.md "wikilink")、[恩阳区](../Page/恩阳区.md "wikilink")
  - 县：[通江县](../Page/通江县.md "wikilink")、[南江县](../Page/南江县.md "wikilink")、[平昌县](../Page/平昌县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>巴中市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>511900</p></td>
</tr>
<tr class="odd">
<td><p>511902</p></td>
</tr>
<tr class="even">
<td><p>511903</p></td>
</tr>
<tr class="odd">
<td><p>511921</p></td>
</tr>
<tr class="even">
<td><p>511922</p></td>
</tr>
<tr class="odd">
<td><p>511923</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>巴中市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>巴中市</p></td>
<td><p>3283148</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>巴州区</p></td>
<td><p>1126167</p></td>
<td><p>34.30</p></td>
</tr>
<tr class="even">
<td><p>通江县</p></td>
<td><p>687369</p></td>
<td><p>20.94</p></td>
</tr>
<tr class="odd">
<td><p>南江县</p></td>
<td><p>606992</p></td>
<td><p>18.49</p></td>
</tr>
<tr class="even">
<td><p>平昌县</p></td>
<td><p>862620</p></td>
<td><p>26.27</p></td>
</tr>
<tr class="odd">
<td><p>注：区划调整后，巴州区的常住人口为699860人；恩阳区的常住人口为426307人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")3283771人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加153510人，增长4.9%，平均每年递增0.48%。其中，男性人口为1670250人，占50.86%；女性人口为1613521人，占49.14%。性别比（以女性为100）为103.52。0－14岁人口为698048人，占21.26%；15－64岁人口为2241846人，占68.27%；65岁及以上人口为343877人，占10.47%。城镇人口为962589人，占29.31%，乡村人口为2321182人，占70.69%。

## 交通

[Bazhong_station.jpg](https://zh.wikipedia.org/wiki/File:Bazhong_station.jpg "fig:Bazhong_station.jpg")
于[重庆直辖市](../Page/重庆市.md "wikilink")、[成都市](../Page/成都市.md "wikilink")、[西安市的金三角地带](../Page/西安市.md "wikilink")。市政府所在地南距重庆275公里，西距成都300公里，北距西安550公里，与相邻的[达州市](../Page/达州市.md "wikilink")、[南充市和](../Page/南充市.md "wikilink")[汉中市及](../Page/汉中市.md "wikilink")[宝成铁路](../Page/宝成铁路.md "wikilink")、[襄渝铁路距离都在](../Page/襄渝铁路.md "wikilink")200公里左右。
境内有广巴、巴达铁路，巴达、广巴、成巴、巴广渝等高速公路，巴
（中）陕（陕西汉中）高速公路现已通车至南江北，预计2017年全线贯通。重庆、成都等城市与巴中每日对开普快客运列车，巴中火车站有9个售票点对外售票。另外，巴中恩阳机场预计2017年底通航。[1](https://web.archive.org/web/20141112094933/http://news.bzgd.com/shgc/2014/09/1412037389213894.html)

## 旅游

巴中具有丰富的旅游资源。著名蜀道米仓道贯穿境内，恩阳古镇依山傍水为四川十大最美古镇，自然山水景观有国家级风景区光雾山、诺水河等八个AAAA级景区，以及多个国家级公园风景壮丽秀美。

在红色旅游方面，巴中市曾是全国第二大苏区，拥有川陕革命根据地红军烈士陵园，[红四方面军总指挥部旧址纪念馆等多个红色旅游景点](../Page/红四方面军.md "wikilink")。

山地运动也是巴中市旅游特色之一，环中国国际公路自行车赛、全国拔河锦标赛、全国摩托车越野锦标赛、四川省定向越野锦标赛和巴中国际马拉松赛等赛事定期在巴中市举行。

### 自然风景区

  - [光雾山](../Page/光雾山.md "wikilink")
  - [诺水河](../Page/诺水河.md "wikilink")
  - [三江水乡](../Page/三江水乡.md "wikilink")
  - [阴灵山](../Page/阴灵山.md "wikilink")
  - [米仓山](../Page/米仓山.md "wikilink")
  - [小巫峡](../Page/小巫峡.md "wikilink")
  - [龙耳山](../Page/龙耳山.md "wikilink")

### 文化遗迹

  - [南龛石窟](../Page/南龛石窟.md "wikilink")
  - [恩阳古镇](../Page/恩阳古镇.md "wikilink")

### 国家森林公园

  - [佛头山森林公园](../Page/佛头山森林公园.md "wikilink")
  - [空山国家森林公园](../Page/空山国家森林公园.md "wikilink")
  - [十八月潭](../Page/十八月潭.md "wikilink")
  - [镇龙山国家森林公园](../Page/镇龙山国家森林公园.md "wikilink")
  - [南阳森林公园](../Page/南阳森林公园.md "wikilink")
  - [米仓山国家森林公园](../Page/米仓山国家森林公园.md "wikilink")

### 红色旅游景区

  - [川陕革命根据地红军烈士陵园](../Page/川陕革命根据地红军烈士陵园.md "wikilink")
  - 红四方面军总指挥部旧址纪念馆
  - 通江红军城
  - “[赤化全川](../Page/赤化全川.md "wikilink")”全国最大的红军石刻标语
  - [空山战役遗址](../Page/空山战役.md "wikilink")
  - 毛浴红色古镇

## 教育

### 高等教育

  - [巴中职业技术学院](../Page/巴中职业技术学院.md "wikilink")

## 参考文献

[Category:巴中](../Category/巴中.md "wikilink")
[Category:四川地级市](../Category/四川地级市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.