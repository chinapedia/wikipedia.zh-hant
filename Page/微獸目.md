**微獸目**（Microbiotheria），又名**智魯負鼠目**或**小負鼠目**，是[有袋下綱下的一個](../Page/有袋下綱.md "wikilink")[目](../Page/目.md "wikilink")，其下只有[微獸科](../Page/微獸科.md "wikilink")（Microbiotheriidae）。這個目下的大部份[物種都已經](../Page/物種.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")，只剩下[南猊](../Page/南猊.md "wikilink")。

現時已知最古老的微獸目成員是*Khasia
cordillerensis*，其[化石是在](../Page/化石.md "wikilink")[玻利維亞的](../Page/玻利維亞.md "wikilink")[古新世地層發現](../Page/古新世.md "wikilink")。另外亦有幾個屬是從[南美洲](../Page/南美洲.md "wikilink")[古近紀及](../Page/古近紀.md "wikilink")[新近紀的地層發現](../Page/新近紀.md "wikilink")。在[南極洲西部的](../Page/南極洲.md "wikilink")[西摩島上發現的大量](../Page/西摩島.md "wikilink")[始新世中期化石](../Page/始新世.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，有可能都是屬於微獸目的。亦有報告指在[澳洲西北部發現了屬於始新世早期的微獸目化石](../Page/澳洲.md "wikilink")，但並未描述。若它們真的屬於微獸目，則可以提供重要的[演化及](../Page/演化.md "wikilink")[生物地理學資料](../Page/生物地理學.md "wikilink")。

微獸目最初被認為是屬於[負鼠目](../Page/負鼠目.md "wikilink")，但是[解剖學及](../Page/解剖學.md "wikilink")[遺傳學的研究結果顯示牠們是自成一目的](../Page/遺傳學.md "wikilink")，且與澳洲的[有袋類接近](../Page/有袋類.md "wikilink")。而微獸目與澳洲的有袋類亦組成了[澳洲有袋類](../Page/澳洲有袋類.md "wikilink")。

在南美洲、南極洲及澳洲還是連結在[岡瓦那大陸的時期](../Page/岡瓦那大陸.md "wikilink")，南猊的祖先相信是原本就留在南美洲的，或是跟隨其他[動物群從澳洲進入南美洲的](../Page/動物群.md "wikilink")。

## 外部連結

  - [Mikko's Phylogeny
    Archive](https://web.archive.org/web/20060927212513/http://www.fmnh.helsinki.fi/users/haaramo/Metazoa/Deuterostoma/Chordata/Synapsida/Metatheria/Notometatheria/Microbiotheria.html)
  - [Entry by the Zoological Society of
    London](http://www.edgeofexistence.org/species/species_info.asp?id=42)

[微獸目](../Page/分類:微獸目.md "wikilink")
[分類:澳洲有袋類](../Page/分類:澳洲有袋類.md "wikilink")