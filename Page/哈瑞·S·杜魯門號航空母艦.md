**哈瑞·S·杜魯門號航空母艦**（****），或直接称**杜魯門號航空母艦**，是[美国海军](../Page/美国海军.md "wikilink")[尼米茲級核動力航空母艦的八號艦](../Page/尼米茲級核動力航空母艦.md "wikilink")，1993年11月29日在[紐波特紐斯造船及船塢公司铺设龙骨起造](../Page/紐波特紐斯造船及船塢公司.md "wikilink")，最初命名“[美國號](../Page/美國號.md "wikilink")”（USS
United
States），1995年2月在鋪放[龍骨之前改名杜魯門號](../Page/龍骨.md "wikilink")，1996年9月7日，同年9月13日正式下水。杜魯門號是以美國第33任總統[哈利·杜鲁门命名](../Page/哈利·S·杜鲁门.md "wikilink")。该舰于1998年7月25日编入[美国大西洋舰队服役](../Page/美国大西洋舰队.md "wikilink")，母港[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[諾福克海軍基地](../Page/諾福克海軍基地.md "wikilink")。

## 簡介

杜魯門號是在1993年11月29日時，在紐波特紐斯造船及船塢公司铺设龙骨起造，1995年2月在鋪放龍骨前艦名由原訂的「美國號」改為「杜魯門號」，1996年9月7日擲瓶命名，同年9月13日正式下水。1998年1月船员开始登舰，1998年6月11日通过造船厂的测试，6月25日完成试航。本来造船厂测试和试航计划在1998年5月进行，但是由于一座反应堆在水压试验时产生的-{zh-tw:噪音;zh-cn:噪声;}-使得这些测试-{zh-tw:延期;zh-cn:被推迟;}-。

在1996年9月14日於維吉尼亞州紐波特紐斯造船廠的啟用儀式當時，時任[美国总统的](../Page/美国总统.md "wikilink")[比尔·克林顿擔任了主講人](../Page/比尔·克林顿.md "wikilink")。其他重要参加者和-{zh-tw:演講者;zh-cn:讲话人;}-包括[密蘇里州参议员](../Page/密蘇里州.md "wikilink")（Ike
Skelton，是主要支持以杜魯門命名的人）、密苏里州州长[梅爾·卡納罕](../Page/梅爾·卡納罕.md "wikilink")（Mel
Carnahan）、杜魯門號的首任船长（Thomas
Otterbein）、国防部长[威廉·科恩](../Page/威廉·科恩.md "wikilink")（William
Cohen）等人。

目前杜魯門號的基地是[诺福克](../Page/诺福克_\(弗吉尼亚州\).md "wikilink")。其第一次任务是从2000年11月28日至2001年5月23日期間，参加了[南方守望行动](../Page/南方守望行动.md "wikilink")（）。2002年12月5日該艦再次參與該行动，并参加了[伊拉克战争](../Page/伊拉克战争.md "wikilink")。2003年5月23日它返回母港。2004年10月13日它离开诺福克再赴[波斯湾](../Page/波斯湾.md "wikilink")，2005年3月19日离开。2004年它获得美国大西洋舰队战备最良好的舰只的荣誉，从2003年至2005年它连续三年获得作战效率奖赏。

2005年[颶風卡特里娜在美国东海岸造成巨大损失后](../Page/颶風卡特里娜.md "wikilink")，杜魯門號于9月1日开赴[墨西哥湾海岸](../Page/墨西哥湾.md "wikilink")，成为美国海军营救任务的旗舰。在从事营救任务五星期后它于2005年10月回到母港。

2006年1月杜魯門號按计划进入船坞，它获得多个系统改良、预防性维护以及在制造时就已经出现的小的焊接错误。

2006年12月杜魯門號离开诺福克海军船厂，进入训练阶段。2007年4月训练结束。

2015年[第七艦載飛行聯隊被重新指派至杜魯門號](../Page/第七艦載飛行聯隊.md "wikilink")。\[1\]

2015年12月29日，杜魯門號開始執行對伊斯蘭國（ISIS）的攻擊任務。直到2016年4月中，第七艦載飛行聯隊已投下1,118枚炸彈,打破羅斯福號於2015年的紀錄（1,085枚）。\[2\]

2016年6月3日，F/A-18大黃蜂自東地中海的杜魯門號起飛執行空襲任務，這是美國海軍自2003年對伊拉克戰爭以來，第一次自東地中海執行打擊任務。\[3\]

2016年9月1日，杜魯門號進入諾福克海軍船廠進行維修改裝，預計2017年9月完成。\[4\]

## 一般諸元

[USS_Harry_S._Truman_(CVN-75)_with_the_Italian_aircraft_carrier_ITS_Giuseppe_Garibaldi_(C-551).jpg](https://zh.wikipedia.org/wiki/File:USS_Harry_S._Truman_\(CVN-75\)_with_the_Italian_aircraft_carrier_ITS_Giuseppe_Garibaldi_\(C-551\).jpg "fig:USS_Harry_S._Truman_(CVN-75)_with_the_Italian_aircraft_carrier_ITS_Giuseppe_Garibaldi_(C-551).jpg")外海所舉行的（Majestic
Eagle
2004）多國演習中，杜魯門號與義大利航空母艦[加里波底號並排航行](../Page/加里波底號航空母艦.md "wikilink")\]\]
[US_Navy_070406-N-5345W-339_Commander,_Carrier_Strike_Group_(CSG)_10_Rear_Adm._William_Gortney_fields_a_question_from_Chinese_People's_Liberation_Army_Navy_(PLAN)_Lt._Sun_Jian_after_a_carrier-based_aviation_briefing_in_Strike_Fi.jpg](https://zh.wikipedia.org/wiki/File:US_Navy_070406-N-5345W-339_Commander,_Carrier_Strike_Group_\(CSG\)_10_Rear_Adm._William_Gortney_fields_a_question_from_Chinese_People's_Liberation_Army_Navy_\(PLAN\)_Lt._Sun_Jian_after_a_carrier-based_aviation_briefing_in_Strike_Fi.jpg "fig:US_Navy_070406-N-5345W-339_Commander,_Carrier_Strike_Group_(CSG)_10_Rear_Adm._William_Gortney_fields_a_question_from_Chinese_People's_Liberation_Army_Navy_(PLAN)_Lt._Sun_Jian_after_a_carrier-based_aviation_briefing_in_Strike_Fi.jpg")（右一）在杜魯門號的飛行準備室內替來訪的[中國人民解放軍海軍軍官們進行示範演練後的解說](../Page/中國人民解放軍海軍.md "wikilink")。\]\]

  - 飛行甲板長度：1,092呎
  - 最大飛行甲板寬度：252呎（76.8公尺）
  - 高度（自水線至桅竿）：20層樓高
  - 飛行甲板面積：4.5英畝（約18,000平方公尺）
  - 排水量：标准98500吨，满载104000吨
  - 動力：兩具A4W[核子反應爐](../Page/核子反應爐.md "wikilink")，4台蒸氣轮机
  - 螺旋槳數量：4具（每具五槳，直徑21呎）
  - 螺旋槳重量：每具66,200磅（約30,055公斤）
  - 極速：35节
  - 彈射器數量：4具
  - 艦載機升降器數量：4具
  - 艦載機單位：8個中隊與1個分遣隊，戰機總數超過80架
  - 母港：[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[諾福克港](../Page/諾福克.md "wikilink")（Norfolk,
    VA.）
  - 人員：含飛行大隊人員時超過5,200人，不含時超過3,000人
  - 每日供應餐點份數：18,150份
  - 船艙數：2,700間
  - 船錨數：2具（承襲自[佛瑞斯特號航空母艦（USS Forrestal
    CV-59）](../Page/CV-59.md "wikilink")）
  - 船錨重：每具30噸
  - 船錨鏈長：超過1,000呎（454公尺），以每段365磅（約111公斤）共684段的鏈條串成
  - 電話數量：2,000支

## 搭載機隊

  - [F/A-18黃蜂（Hornet）式戰鬥攻擊機](../Page/F/A-18大黃蜂式戰鬥攻擊機.md "wikilink")
  - [F/A-18 E/F超級大黃蜂式（Super
    Hornet）戰鬥/攻擊機](../Page/F/A-18E/F超級大黃蜂式打擊戰鬥機.md "wikilink")
  - [EA-6B徘徊者（Prowler）式電子作戰機](../Page/EA-6徘徊者式電子作戰機.md "wikilink")
  - [S-3B維京（Viking）式反潛作戰機](../Page/S-3維京式反潛機.md "wikilink")
  - [E-2C鷹眼（Hawkeye）式空中預警管制機](../Page/E-2空中預警機.md "wikilink")
  - [SH-60海鷹（Seahawk）式反潛直升機](../Page/SH-60海鷹直升機.md "wikilink")
  - [C-2A灰狗（Greyhound）式運輸機](../Page/C-2灰狗式運輸機.md "wikilink")

## 轶事

  - [探索頻道的纪录片](../Page/探索頻道.md "wikilink")《海上堡垒》记载了杜魯門號航空母艦的建造、试航和启用。

## 相關話題

  - [美國海軍航空母艦列表](../Page/美國海軍航空母艦列表.md "wikilink")

## 外部連結

  - [美國海軍官方網站](http://www.navy.mil)
  - [杜魯門號官方網站](https://web.archive.org/web/20040811001057/http://www.navy.mil/homepages/cvn75/)

## 资料来源

[Category:核動力航空母艦](../Category/核動力航空母艦.md "wikilink")
[Category:尼米茲級核動力航空母艦](../Category/尼米茲級核動力航空母艦.md "wikilink")
[Category:1998年竣工的船只](../Category/1998年竣工的船只.md "wikilink")

1.
2.
3.
4.