**電力市場**指的是，整體電力在供應、需求、售賣和購買的影響下，對[電力價格產生改變的一個機制](../Page/電力價格.md "wikilink")。
\[1\]

從經濟學的角度來看，[電](../Page/電.md "wikilink")（包括功率與能量）是可供購買、銷售並進行交易的[商品](../Page/日用品.md "wikilink")。電力市場是一個能夠出價購買、開價銷售的系統，通常以財務或憑證交換的方式進行採購與[短期交易](../Page/短期交易.md "wikilink")，依循[供需法則決定價格](../Page/供需法則.md "wikilink")。長期交易合約則類似[購電合約](../Page/購電合約.md "wikilink")，普遍被認為是私人間的[雙邊交易](../Page/雙邊交易.md "wikilink")。電力批發通常由市場運營機構或特定的獨立機構專門負責結算，市場運營機構不進行結算也要具備交易相關知識，以維持發電與負載的平衡。

電力市場通常有兩類商品：[功率與](../Page/功率.md "wikilink")[能量](../Page/能量.md "wikilink")。功率是瞬間的傳輸率，單位為[百萬瓦](../Page/百萬瓦.md "wikilink")（MW）。能量是一段時間的傳輸量，單位為[百萬瓦·時](../Page/千瓦·時.md "wikilink")（MWh）。

能量市場交易的是各種時間長度的淨發電量，經常以5，15與60分鐘為單位遞增。功率市場被視為[輔助服務](../Page/輔助服務.md "wikilink")，涵蓋熱機備轉、非熱機備轉、[操作備轉](../Page/操作備轉.md "wikilink")、響應備轉、頻率控制與[裝置容量等名詞](../Page/裝置容量.md "wikilink")，為確保供電可靠，市場運營機構需要並管理功率市場。

此外，大部分主要的市場運營機構還有線路壅塞以及電力[衍生工具等市場](../Page/衍生工具.md "wikilink")，如交投熱絡的電力[期貨與](../Page/期貨.md "wikilink")[選擇權](../Page/選擇權.md "wikilink")。這些市場的發展是世界各地電力系統重組的結果，過程中經常伴隨著[天然氣市場的重組同時進行](../Page/天然氣.md "wikilink")。

## 歷史

由於[發電設施的建造成本高昂](../Page/發電站.md "wikilink")，牽涉社會的範圍廣泛，初期提供電力的責任，往往落上政府身上。電力市場的私有化最早在1970年代的智利開展，由25名在芝加哥大學受訓的年輕[智利經濟學者推動](../Page/智利.md "wikilink")。及至1990年代。在[戴卓爾夫人領導下的](../Page/戴卓爾夫人.md "wikilink")[英國](../Page/英國.md "wikilink")[政府](../Page/政府.md "wikilink")，把英國電力市場私有化，以[商業形式供電](../Page/商業.md "wikilink")。這個舉動，成為日後很多英聯邦國家，例如[澳洲](../Page/澳洲.md "wikilink")、[新西蘭和地區性市場](../Page/新西蘭.md "wikilink")，如[阿爾伯特省等的放寬電力市場模式](../Page/艾伯塔.md "wikilink")。但有趣的是，這些[英聯邦國家在市場放寬後](../Page/英聯邦.md "wikilink")，卻沒有像英國般徹底的將電力市場[私有化](../Page/私有化.md "wikilink")。\[2\]\[3\]

## 市場的性質

[電難以儲存](../Page/電.md "wikilink")，供給必須及時滿足需求。因此，與其他產品不同的是，在一般情況下不可能建立庫存、實施配給或是請用戶等待。此外，需求與供給還持續不斷地變動。

因此管控機構與[輸電系統運營機構要協調發電機組的調度](../Page/輸電系統運營機構.md "wikilink")，以符合整個[輸電網預期的需求](../Page/輸電系統.md "wikilink")。如果供給與需求不匹配，發電機組加速或減速以提高或降低系統[頻率](../Page/頻率.md "wikilink")（50或60[赫茲](../Page/赫茲.md "wikilink")）。如果頻率超出設定的範圍，系統運營機構就會採取行動，增加或減少發電或負載。

此外，[物理定律決定電在](../Page/物理.md "wikilink")[電網上如何流動](../Page/電路.md "wikilink")。傳輸會損失一部分的電，網路節點壅塞的程度會影響發電機組的[經濟調度](../Page/經濟調度.md "wikilink")
(Economic Dispatch)。

電力市場的範圍由連往批發商、零售商以及各地終端用戶的電網所組成，其範疇可能超越國界。

## 電力批發市場

[Tagesgang_engl.png](https://zh.wikipedia.org/wiki/File:Tagesgang_engl.png "fig:Tagesgang_engl.png")

彼此競爭的[發電商提供電力給](../Page/發電.md "wikilink")[零售商](../Page/零售商.md "wikilink")，即為**電力批發市場**，零售商重新定價並銷售。雖然批發市場曾經是大型零售供應商的專門領域，愈來愈多的市場，如新英格蘭，開始開放給終端用戶。尋求節省能源成本開銷的大型終端用戶開始認識這種採購方式所具有的優勢。消費者直接向發電商買電是相對較新的現象。

購買批發電力並非全然沒有缺點（市場的不確定性、會員費用、設置費用、擔保投資與組織成本，因為基本上需逐日購買）。然而終端用戶電力負載愈大，受益愈大，誘因也就愈大。

電力批發市場要能夠蓬勃發展，必須符合一些準則。[哈佛大學的](../Page/哈佛大學.md "wikilink")[William
Hogan](http://www.hks.harvard.edu/fs/whogan/)教授指出這些準則，其核心是"競價為基礎、安全性限制、以節點價格進行經濟調度"，互相配合的現貨市場。其他學者如[柏克萊加州大學的](../Page/柏克萊加州大學.md "wikilink")[Shmuel
Oren](http://www.ieor.berkeley.edu/~oren/)以及[Pablo
Spiller](https://web.archive.org/web/20120414204039/http://faculty.haas.berkeley.edu/spiller/olderindex.htm)教授亦提出其他準則。美國、澳洲與紐西蘭廣泛採用Hogan教授模式的變化型。

### 競價為基礎、安全性限制、以節點價格進行經濟調度

原則上，前一日市場的價格是藉由撮合各[節點發電機組標售價格與消費者標購價格](../Page/節點.md "wikilink")，發展出典型的[供](../Page/供給和需求.md "wikilink")[需](../Page/供給和需求.md "wikilink")[均衡價格來決定](../Page/經濟均衡.md "wikilink")，通常以小時為間隔，系統運營機構的電力潮流模型中，輸電受限制的各區域分別計算。

理論上，電網每個節點的價格是計算出來的"[影子價格](../Page/影子價格.md "wikilink")"，這是假設節點增加一[度電的需求](../Page/千瓦·時.md "wikilink")，經由機組重新優化虛擬出這一度電的生產成本，所產生虛擬的系統遞增成本。即為所謂的*區域邊際訂價*（LMP）或*節點訂價*，用於某些解除管制的市場，最著名的有美國的[PJM](../Page/PJM_Interconnection.md "wikilink")、[ERCOT](../Page/ERCOT.md "wikilink")、[紐約與](../Page/紐約.md "wikilink")[新英格蘭市場](../Page/新英格蘭.md "wikilink")，以及[紐西蘭](../Page/New_Zealand_Electricity_Market.md "wikilink")。

實務上，供給以發電機組於前一日市場標售的為基礎，需求以負載服務商標購的為基礎，結合安全性限制、最低成本調度計算（見下文），執行上述LMP演算法。

雖然LMP概念理論上可行，且人為操控不明顯，實務上，系統運營機構可以分類機組，進行"非最低成本調度"以排除LMP計算，對LMP的結果有很大的裁量權。提供[無功功率以維持電網的機組在大部分的系統中為](../Page/無功功率.md "wikilink")"非最低成本"（即使通常是區域範圍內相同的機組）
系統運營機構也會要機組上線作為"熱機備轉"以避免突然停電或非預期的需求變化，並宣告他們為"非最低成本"。結果往往是負載增加，價格應該攀升時，結算價格大幅降低。

研究人員已經注意到多種產生"貨幣失蹤"問題的因素，包括設定的價格上限遠低於公認的短缺價值、"非最低成本"調度的影響、使用短缺時期降低電壓一類的技術而無相應的短缺價值信號等等。後果就是市場上支付給供應商的價格遠低於刺激新競爭者進入需要的水準，這樣的市場可以帶來短期系統操作與調度的效率，但是無法達到主要的效益：適時、適地、適當地刺激新投資。

在LMP市場中，存在輸電限制的地方，下游需調度價格更高的電源。從而在輸電限制的兩端分別產生[壅塞訂價與](../Page/壅塞訂價.md "wikilink")[輸電限制租金](../Page/輸電限制租金.md "wikilink")。

輸電限制可能在電網的某分支已達到熱限制或因為電網其他部分的偶發事件（如發電機故障或線路跳脫）而有發生超載的潛在可能時產生。後者稱為安全性限制。即使發生線路跳脫等偶發事故，輸電系統仍然能持續供電，此為所謂有安全性限制的系統。

大部分系統使用的是"直流"演算法，而非"交流"演算法，因此可以發現或預測因為熱限制產生的輸電限制與再調度，然而因為無功功率不足產生的限制就沒有辦法。有些系統會考慮邊際損失。即時市場價格由上述的區域邊際訂價演算法決定，由可用的機組平衡供給。[輸電網各個節點每隔五分鐘](../Page/輸電系統.md "wikilink")、半小時或一小時（依市場而定）進行一次這個程序。決定區域邊際價格的虛擬再調度計算必須遵守安全性限制，並保留足夠的餘裕，以維持系統任何地方出現非預期事故時的穩定度。結果就是競價為基礎、安全性限制、以節點價格進行經濟調度。

自從市場成立以來，紐西蘭歷經2001與2003年的電力短缺，直到2005年電價都維持在高檔，2006年（四月）電價甚至更高，缺電危機更熾。這是因為紐西蘭水力發電占比高，有乾旱問題的風險。

詳見：[紐西蘭電力市場](../Page/紐西蘭電力市場.md "wikilink")

許多成熟市場並不採行節點電價，例如英國、[歐洲電力交易所與](../Page/歐洲電力交易所.md "wikilink")[北歐電力池](../Page/北歐電力池.md "wikilink")（北歐與波羅的海國家）。

### 風險管理

由於市場風險很高，財務風險管理經常是市場參與者優先關心的事項。因為電力批發市場的複雜性，用電尖峰與缺電時價格的[波動性可能會非常大](../Page/波動性.md "wikilink")。此價格風險的特點是它跟市場的實體面高度相關，如發電組合以及用電需求與氣候模式之間的關係。燃料或電廠的變化較為長期，但"天價"難以預測，突顯價格風險。

"數量風險"常用來表示電力市場消費量或生產量的不確定性。例如，零售商無法準確預測某個小時或未來幾天的消費需求，生產者無法準確預測電廠故障或燃料短缺的時間點。極端價格與數量問題間常有複合的相關性。例如電價飆漲經常出現於電廠在用電尖峰時發生故障。大量[間歇性電源如](../Page/間歇性電源.md "wikilink")[風能等有可能影響市場價格](../Page/風能.md "wikilink")。

### 電力批發市場

  - [捷克](../Page/捷克.md "wikilink")/[歐洲](../Page/歐洲.md "wikilink") -
    [捷克電力交易所（OTE）](http://www.ote-cr.cz/)以及[布拉格能源交易所（PXE）](http://www.pxe.cz/)
  - [西澳](../Page/西澳.md "wikilink") -
    [獨立市場調度中心（IMO）](http://www.imowa.com.au/)
  - [澳洲](../Page/澳洲.md "wikilink") -
    [國家電力市場](../Page/National_Electricity_Market.md "wikilink")[澳洲能源市場調度中心（AEMO）](http://www.aemo.com.au/)
  - [奧地利](../Page/奧地利.md "wikilink") -
    參閱[歐洲電力交易所](../Page/EPEX_SPOT.md "wikilink")，[奧地利能源交易所（EXAA）](http://en.exaa.at/)
  - [巴西](../Page/巴西電力部門.md "wikilink") -
    [巴西電力交易所（CCEE）](http://www.ccee.org.br/)
  - [加拿大](../Page/加拿大電力部門.md "wikilink") -
    [安大略電力調度中心（IESO）](http://www.ieso.ca/)以及[亞伯達電力調度中心（AESO）](http://www.aeso.ca/)
  - [智利](../Page/智利電力部門.md "wikilink")
  - [斯堪地納維亞](../Page/斯堪地納維亞.md "wikilink") -
    [北歐電力池](../Page/Node_Pool_Spot.md "wikilink")
  - [法國](../Page/法國電力部門.md "wikilink") -
    [歐洲電力交易所](../Page/EPEX_SPOT.md "wikilink")
  - [德國](../Page/德國電力部門.md "wikilink") -
    [歐洲能源交易所](../Page/European_Energy_Exchange.md "wikilink")[EEX](http://www.eex.com)，[歐洲電力交易所](../Page/RPRX_SPOT.md "wikilink")
  - [大不列顛島](../Page/大不列顛島.md "wikilink") -
    [Elexon](http://www.elexon.co.uk/about/who-we-are/)
  - [匈牙利](../Page/匈牙利.md "wikilink") -
    [匈牙利電力交易所（HUPX）](http://www.hupx.hu/)以及[布拉格能源交易所（PXE）](http://www.pxe.cz/)
  - [印度](../Page/印度.md "wikilink") -
    [印度電力交易有限公司（PXIL）](http://www.powerexindia.com)
  - [印度](../Page/印度.md "wikilink") -
    [印度能源交易所](../Page/Indian_Energy_Exchange.md "wikilink")
  - [愛爾蘭](../Page/愛爾蘭.md "wikilink") -
    [單一電力市場交易所（SEMO）](http://www.sem-o.com)
  - [義大利](../Page/義大利.md "wikilink") -
    [義大利電力交易所（GME）](http://www.mercatoelettrico.org/En/Default.aspx)
  - [日本](../Page/日本電力部門.md "wikilink") - 參閱
    [日本電力交易所（JEPX）](https://web.archive.org/web/20080515081636/http://www.jepx.org/English/index_e.html)
  - [荷蘭](../Page/荷蘭.md "wikilink")，[英國](../Page/英國.md "wikilink")，[比利時](../Page/比利時.md "wikilink")
    - [阿姆斯特丹電力交易所](../Page/APX-ENDEX.md "wikilink")
  - [紐西蘭](../Page/紐西蘭.md "wikilink") -
    參閱[紐西蘭電力市場](../Page/New_Zealand_Electricity_Market.md "wikilink")
  - [菲律賓](../Page/菲律賓.md "wikilink") -
    參閱[菲律賓電力批發市場（WESM）](http://www.wesm.ph/)
  - [葡萄牙](../Page/葡萄牙.md "wikilink") -
    參閱[葡萄牙電力交易所（OMIP）](http://www.omip.pt/)
  - [俄羅斯](../Page/俄羅斯.md "wikilink") - 參閱[Trade System Administrator
    (ATS)](http://www.atsenergo.ru/)
  - [新加坡](../Page/新加坡.md "wikilink") -
    參閱[新加坡能源市場管理局（EMA）](http://www.ema.gov.sg/)以及[能源市場公司（EMC）](http://www.emcsg.com/)
  - [西班牙](../Page/西班牙.md "wikilink") -
    參閱[西班牙電力市場（OMEL）](http://www.omel.com/inicio)
  - [土耳其](../Page/土耳其.md "wikilink") - 參閱[Turkish Electricity
    Market](http://portal.pmum.gov.tr/)
  - [美國](../Page/美國.md "wikilink")（[摘要](http://www.ferc.gov/market-oversight/mkt-electric/overview.asp)）
      - [PJM](../Page/PJM_Interconnection.md "wikilink")
      - [德州電力調度中心（ERCOT）](http://www.ercot.com/),
      - [紐約電力調度中心（NYISO）](http://www.nyiso.com),
      - [中西電力調度中心（MISO）](https://www.midwestiso.org/Pages/Home.aspx),
      - [加州電力調度中心（CaISO）](http://www.caiso.com/),
      - [新英格蘭電力調度中心（NEISO）](http://www.iso-ne.com/)

## 零售電力市場

消費者可以從彼此競爭的[電力零售商中選擇自己的電力供應商](../Page/電力零售商.md "wikilink")，即成為**零售電力市場**；這種消費者選擇權在美國有個名詞叫"用電選擇權"。電力市場的另一個議題是，消費者是否應該直接面對即時電價（隨批發價格變動），或是用其他的訂價方式，如年平均成本。在許多市場中，消費者並不依即時價格付費，因此在（批發）價格高昂的時候沒有降低用電需求量或移轉用電時段的誘因。[需量反應可以用價格機制或技術性方案降低尖峰用電需求量](../Page/需量反應.md "wikilink")。

批購電力的零售商與批售電力的發電商暴露在價格與數量風險下，為保護自己，避免市場波動性的過度影響，彼此會簽訂"[避險合約](../Page/避險.md "wikilink")"。各地的慣例與市場結構不同，合約結構也不一樣。兩個最簡單，也最普遍的形式是價格固定的實物交割[遠期合約以及](../Page/forward_contract.md "wikilink")[差價合約](../Page/差價合約.md "wikilink")，由雙方訂定時段的[履約價](../Page/履約價.md "wikilink")。在[差價合約下](../Page/差價合約.md "wikilink")，任一時段的批發價格指數（合約參考的價格）如果高於履約價，發電商要退還該時段履約價與實際價格之間的價差，同樣地，如果實際價格低於履約價，零售商要還價差給發電商。實際價格指數有時也稱為現貨價格或電力池價格，依市場而定。

高度發展的電力市場有很多避險協議，例如調整合約、[虛擬競標](../Page/Virtual_Bidding.md "wikilink")、金融輸電權、[買權](../Page/call_option.md "wikilink")、[賣權](../Page/put_option.md "wikilink")。一般用於移轉財務風險。

## 參考

  - [電源](../Page/電源.md "wikilink")
  - [電力價格](../Page/電力價格.md "wikilink")

## 註腳

[Category:能源](../Category/能源.md "wikilink")
[Category:市場](../Category/市場.md "wikilink")

1.  [電力市場 - 英文維基](http://en.wikipedia.org/wiki/Electricity_market)
2.  [電力市場歷史-
    英文維基](http://en.wikipedia.org/wiki/Electricity_market#Early_history)
3.  [維省電力價格對紐省的警惕](http://www.smh.com.au/news/national/victorias-electricity-costs-a-flag-for-nsw/2007/12/11/1197135463517.html)
    - 雪梨晨鋒報, 2008年5月11日索取