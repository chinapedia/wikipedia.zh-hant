**乔治·普拉特·舒尔茨**（，），生于[纽约](../Page/纽约_\(纽约州\).md "wikilink")，[美国政治家](../Page/美国.md "wikilink")。他1942年本科毕业于普林斯顿大学，二战期间服役于美国海军陆战队，任[上尉](../Page/上尉.md "wikilink")。1949年获得麻省理工学院（MIT）工业经济学（industrial
economics）博士学位。博士毕业后曾在MIT和芝加哥大学教经济学，之后曾任[美国劳工部长](../Page/美国劳工部长.md "wikilink")、[美国财政部长和](../Page/美国财政部长.md "wikilink")[美国国务卿](../Page/美国国务卿.md "wikilink")\[1\]。

## 參考资料

[Category:美国经济学家](../Category/美国经济学家.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:麻省理工學院教師](../Category/麻省理工學院教師.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:史丹佛大學教師](../Category/史丹佛大學教師.md "wikilink")
[Category:美國海軍陸戰隊軍官](../Category/美國海軍陸戰隊軍官.md "wikilink")
[Category:美国国务卿](../Category/美国国务卿.md "wikilink")
[Category:美国财政部长](../Category/美国财政部长.md "wikilink")
[Category:芝加哥大學教師](../Category/芝加哥大學教師.md "wikilink")
[Category:胡佛研究所人物](../Category/胡佛研究所人物.md "wikilink")
[Category:拉姆福德奖获得者](../Category/拉姆福德奖获得者.md "wikilink")

1.  美国国务院：BIOGRAPHIES OF THE SECRETARIES OF STATE: GEORGE PRATT
    SHULTZ|accessdate=2014-10-01
    |url=<http://history.state.gov/departmenthistory/people/shultz-george-pratt>
    |format=fee, via [美国国务院](../Page/美国国务院.md "wikilink")