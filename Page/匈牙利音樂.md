[Franz_Liszt_photo.jpg](https://zh.wikipedia.org/wiki/File:Franz_Liszt_photo.jpg "fig:Franz_Liszt_photo.jpg")，著名的匈牙利作曲家\]\]
[匈牙利對](../Page/匈牙利.md "wikilink")[民謠音樂的世界有相當多的貢獻](../Page/民謠音樂.md "wikilink")，包含[流行音樂及](../Page/流行音樂.md "wikilink")[古典音樂](../Page/古典音樂.md "wikilink")。匈牙利民謠音樂同時也是匈牙利人自我認同中相當重要的支柱與象徵，也持續在鄰近地區扮演著舉足輕重的角色，像[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[斯洛伐克](../Page/斯洛伐克.md "wikilink")、[波蘭南部](../Page/波蘭.md "wikilink")，尤其是在南[斯洛伐克及](../Page/斯洛伐克.md "wikilink")[川西凡尼亞的羅馬尼亞人所在地](../Page/川西凡尼亞.md "wikilink")，因為這裡也是許多匈牙利人的故鄉。。另外在[索博爾奇-索特馬爾-貝拉格州區域](../Page/索博爾奇-索特馬爾-貝拉格州.md "wikilink")，以及緊鄰[克羅埃西亞的](../Page/克羅埃西亞.md "wikilink")[外多瑙](../Page/外多瑙.md "wikilink")(
Transdanubia)地區西南部，同樣有著相當程度的影響力。每年固定在[莫哈奇市](../Page/莫哈奇市.md "wikilink")（Mohács）舉辦的[Busójárás
Carnival是當今匈牙利民謠界的大事](../Page/Busójárás_Carnival.md "wikilink")，以往是以歷史悠久並享譽國際的[Bogyiszló
orchestra作為活動的代表團體](../Page/Bogyiszló_orchestra.md "wikilink")。。

匈牙利的[古典音樂長期以來就像是](../Page/古典音樂.md "wikilink")「在匈牙利的土地上流傳已久的音樂實驗，並運用其民謠音樂的廣泛流傳，加以開創出屬於自己具有深度、自知的音樂文化。」。儘管匈牙利上層階級長久以來與歐洲其他各國有密切聯繫，使得全歐洲的音樂觀念能湧入匈牙利，但位處鄉村的農民亦能夠保存並延續己身的傳統。正因如此，在19世紀末的匈牙利作曲家們才能從鄉村農民音樂中汲取養分，創作出不同以往的匈牙利特有古典音樂風格
。舉例來說－[巴托克·貝拉與](../Page/巴托克·貝拉.md "wikilink")[高大宜這兩位匈牙利最有名的作曲家](../Page/高大宜.md "wikilink")，即因運用民謠元素在個人作品中而聞名。巴托克採集整個東歐地區（包括羅馬尼亞及斯洛伐克）的民謠歌曲，而高大宜則著重在找尋匈牙利音樂中具特色且不同於其他音樂的部分。

匈牙利在1944年這段[共產黨統治時期](../Page/共產黨.md "wikilink")，政府設有歌曲審查委員會。當時主張意識型態的淨化，並查核其認為有煽動民眾可能性的流行音樂。不過也從此時開始，匈牙利的音樂產業日漸復甦，在各種音樂領域培養出不少成功樂手－[爵士樂方面像是小喇叭手](../Page/爵士樂.md "wikilink")[魯道夫·湯席茲](../Page/魯道夫·湯席茲.md "wikilink")、鋼琴家與作曲家[卡洛里·賓德](../Page/卡洛里·賓德.md "wikilink")，都將匈牙利民謠與現代音樂結合。另外還有[費倫茨·賽布及](../Page/費倫茨·賽布.md "wikilink")[瑪塔.塞巴斯蒂安](../Page/瑪塔.塞巴斯蒂安.md "wikilink")。匈牙利[搖滾樂的三個重要團體是](../Page/搖滾樂.md "wikilink")－[Illés](../Page/Illés.md "wikilink")、[Metró與](../Page/Metró.md "wikilink")[Omega仍受到大眾喜愛](../Page/Omega_\(band\).md "wikilink")，特別是
Omega
在匈牙利本國及德國都很受到歡迎。老牌地下團體如[Sziámi及](../Page/Sziámi.md "wikilink")[Európa
Kiadó到](../Page/Európa_Kiadó.md "wikilink")80年代仍廣受歡迎。。

## 特質

與歐洲他處不同，匈牙利人與[馬扎兒人](../Page/馬扎兒人.md "wikilink")（Magyars）起源自西元5到8世紀，當時[芬蘭-烏戈爾語族與東](../Page/芬蘭-烏戈爾語族.md "wikilink")[土耳其人民在此定居並逐漸交融](../Page/土耳其.md "wikilink")，這正是匈牙利傳統音樂的由來與歐洲絕大部分音樂差異。根據[Simon
Broughton的研究](../Page/Simon_Broughton.md "wikilink")，高大宜定位的這些歌曲與俄羅斯[馬里語族相同](../Page/馬里語.md "wikilink")－「顯然可追溯至兩千五百年前」。[文化人類學音樂研究者](../Page/文化人類學音樂研究者.md "wikilink")[Bruno
Nettl指出傳統匈牙利音樂在形式與元素上和](../Page/Bruno_Nettl.md "wikilink")[蒙古及](../Page/蒙古.md "wikilink")[美國原住民音樂相似度頗高頗高](../Page/美國原住民音樂.md "wikilink")。。[Bence
Szabolcsi研究則指出](../Page/Bence_Szabolcsi.md "wikilink")：「雖無法明確辨認出相關對象或特定民族語系，但芬烏及蒙古-土耳其元素亦在其中。」但即使如此，Szabolcsi仍認為匈牙利傳統音樂和[馬里語](../Page/馬里語.md "wikilink")、[卡爾梅克語](../Page/卡爾梅克語.md "wikilink")、[漢特語](../Page/漢特語.md "wikilink")（Ostyak）、西北部[中國音樂](../Page/中國音樂.md "wikilink")、[韃靼語](../Page/韃靼語.md "wikilink")、[Vogul](../Page/Vogul.md "wikilink")、[土耳其音樂](../Page/土耳其音樂.md "wikilink")、[Bashkirian](../Page/Bashkirian.md "wikilink")、[蒙古音樂及](../Page/蒙古音樂.md "wikilink")[楚瓦什人的音樂有密切聯繫](../Page/楚瓦什人.md "wikilink")。他認為這些都證明了「以往與亞洲的交流與回憶，都沈睡並流動於匈牙利民謠的深處，這些音樂是[西方世界在曲式方面不斷與古老](../Page/西方世界.md "wikilink")[東方聯繫的部分](../Page/東方.md "wikilink")。」。

根據Broughton
的說法，匈牙利傳統音樂如同「匈牙利語」有著高度的特殊性，重音往往位於第一音節，致使音樂的部分產生具強勁口音的[揚抑抑格](../Page/揚抑抑格.md "wikilink")（Dactyl）節奏。。Nettl
辨認出兩項匈牙利民謠的「關鍵特質」：「包含[五聲音階的運用](../Page/五聲音階.md "wikilink")（由[大二度與](../Page/音程.md "wikilink")[小三度](../Page/音程.md "wikilink")，或[間隔音階](../Page/間隔音階.md "wikilink")（gapped
scale）所譜成)），也有部分旋律數度反覆的[變調所創造的歌曲主軸](../Page/變調.md "wikilink")。」這些變調「通常是在[完全五度](../Page/完全五度.md "wikilink")（Perfect
fifth）裡升降。」，產生和諧泛聲的基音音程。有跡象顯示此五聲音階的重要特色，可能是受到[中國音樂的影響](../Page/中國音樂.md "wikilink")。

根據Szabolcsi研究，這些匈牙利變調曲式伴隨的「某些旋律、節奏與裝飾音，可清楚地呈現土耳其人在[歐亞大陸由東至西移動的特色](../Page/歐亞大陸.md "wikilink")。」。明顯受此影響的鄰近國家像[斯洛伐克音樂](../Page/斯洛伐克音樂.md "wikilink")，也會伴隨第二或第三音程間隔（interval）。在[捷克](../Page/捷克.md "wikilink")、匈牙利及其他芬烏語系的音樂，也同樣具有運用「A-B-B-A」二部[音樂形式的特色](../Page/音樂形式.md "wikilink")。匈牙利己身音樂更為人熟知的用法有**A
A' A' A** 的變形，在其中**B**的片段是由五音中的**A**片段轉型而來（升或降）。

## 音樂的歷史

[Oldhungariansheetmusic.png](https://zh.wikipedia.org/wiki/File:Oldhungariansheetmusic.png "fig:Oldhungariansheetmusic.png")

現存匈牙利音樂的最早記錄是在11世紀傳入的[葛利果聖歌](../Page/葛利果聖歌.md "wikilink")(Gregorian
Chant)。記載中的樂器最早可追溯到1222年的[口哨](../Page/口哨.md "wikilink")、1326年的[kobzos](../Page/kobzos.md "wikilink")、1355年的[軍號](../Page/軍號.md "wikilink")、1358年的[小提琴](../Page/小提琴.md "wikilink")、1402年的[風笛](../Page/風笛.md "wikilink")、1427年的[長笛與](../Page/長笛.md "wikilink")1428年的[小號](../Page/小號.md "wikilink")。後來風琴在音樂演奏中扮演了重要角色。*[1508年](../Page/1508.md "wikilink")[Nádor
Codex](../Page/Nádor_Codex.md "wikilink")*則首次以[葛利果聖歌為旋律藍本](../Page/葛利果聖歌.md "wikilink")，添上匈牙利人自己的音樂風格。

16世紀[川西凡尼亞地區興起](../Page/川西凡尼亞.md "wikilink")（在匈牙利東北部從未被土耳其人佔領的地區）成為匈牙利音樂創作中心，[克拉科夫當地首次出版了匈牙利音樂作品](../Page/克拉科夫.md "wikilink")。匈牙利的演奏音樂在當時的歐洲有相當的知名度，舉例來說像是長笛暨作曲家[Bálint
Bakfark即是享有美譽的大師級演奏家](../Page/Bálint_Bakfark.md "wikilink")。他的作曲開拓出一種全新風格，著重在長笛與人聲間的配合。長笛演奏家[Neusiedler兄弟創作了早期許多重要作品](../Page/Neusiedler.md "wikilink")，在建構音樂理論上的作品收錄在*[Epithoma
utriusque muisces](../Page/Epithoma_utriusque_muisces.md "wikilink")*中。

17世紀時匈牙利被分割為三個部分：一部份被土耳其人控制、一部份為[哈布斯堡所統治](../Page/哈布斯堡.md "wikilink")，另外則是川西凡尼亞。自古流傳的歌曲漸漸失去了大眾市場，並被詩歌所取代（宮廷音樂家則取代了吟唱詩人）。他們演奏喇叭、口哨、[欽巴隆](../Page/欽巴隆.md "wikilink")、小提琴或風笛，而許多宮廷或王室都有自己專屬的大型演奏團體。其中的音樂家都來自德國、波蘭、法國或[義大利](../Page/義大利.md "wikilink")。川西凡尼亞王子[Gábor
Bethlen的樂師](../Page/Gábor_Bethlen.md "wikilink")，甚至包括了來自西班牙的吉他手，不過這時期的音樂沒有太多資料流傳下來。。

18世紀中葉，部分[Sárospatak及](../Page/Sárospatak.md "wikilink")[Székelyudvarhely地區的大學生是中下層的貴族](../Page/Székelyudvarhely.md "wikilink")，他們原先來自鄉村區域，帶來了他們所處的地域音樂風格。在這些大學中流傳的合唱形式，採取了更多的複調曲式，學生們的[歌本顯示了許多類似歌曲逐漸廣泛的通俗性](../Page/歌本.md "wikilink")。然而他們仍採用原曲作為記註符號，因此在其中並沒有出現延伸形式的歌曲收錄。直到[Ádám
Pálóczi
Horváth在](../Page/Ádám_Pálóczi_Horváth.md "wikilink")1853年出版了*[Ötödfélszáz
Énekek](../Page/Ötödfélszáz_Énekek.md "wikilink")*，這些歌曲顯示在18世紀中後期，傳統匈牙利歌曲的風格日漸消失，而音樂家開始藉以向西方音樂靠攏來尋找創作靈感。

18世紀時[維邦克斯](../Page/維邦克斯.md "wikilink")（士兵音樂，verbunkos）亦逐漸興起，這種起初為軍隊徵兵所用的樂曲如同當時多數的匈牙利音樂，也就是旋律比歌詞更重要，即使近年來這項特質因
verbunkos 更為流行而有所轉變了。

## 民謠音樂

*另見：[匈牙利民謠音樂](../Page/匈牙利民謠音樂.md "wikilink")*

1895年[Béla
Vikár首次錄製了匈牙利民謠音樂](../Page/Béla_Vikár.md "wikilink")，更開創日後[巴托克·貝拉與](../Page/巴托克·貝拉.md "wikilink")[高大宜等先驅者在原始音樂的採集工作](../Page/高大宜.md "wikilink")。現代匈牙利民謠出現在18世紀由[哈布斯堡統治的帝國時代](../Page/哈布斯堡.md "wikilink")，東歐的音樂元素對其影響十分巨大，包括固定的制式舞曲和[進行曲結構](../Page/進行曲.md "wikilink")，取代了舊時代自由形式的風格。在此時的民謠音樂的形式，已經由演奏弦樂的吉普賽人或[洛馬人取代了鄉村的](../Page/洛馬.md "wikilink")[風笛手](../Page/風笛手.md "wikilink")。」。

在19世紀，[洛馬管弦樂隊在全歐洲逐漸建立起知名度](../Page/洛馬管弦樂隊.md "wikilink")，在當時也常被認為是匈牙利原始音樂的傳統與遺產，像在[李斯特的](../Page/李斯特.md "wikilink")*[匈牙利舞蹈](../Page/匈牙利舞蹈.md "wikilink")*與*[狂想曲](../Page/狂想曲.md "wikilink")*中，運用了匈牙利洛馬音樂來表現匈牙利民謠音樂
。匈牙利的[洛馬音樂常被認為是唯一的洛馬音樂](../Page/洛馬音樂.md "wikilink")，但其他不同的洛馬音樂其實仍遍佈整個歐洲大陸，並且和匈牙利類型不盡相同。在19世紀[匈牙利語的民謠像](../Page/匈牙利語.md "wikilink")[夏得西或](../Page/夏得西.md "wikilink")[verbunkos](../Page/verbunkos.md "wikilink")，往往被歸類為''吉普賽音樂（cigányzene）。。

匈牙利民族主義作曲家如[巴爾托克](../Page/巴爾托克.md "wikilink")，強烈否定匈牙利音樂與洛馬音樂有所關連，因此他投入匈牙利農民歌曲的研究與採集工作。根據音樂史學家[Bruno
Nettl的研究](../Page/Bruno_Nettl.md "wikilink")，巴爾托克所採集到的音樂與洛馬音樂幾乎沒什麼相關聯繫。相同的觀點也有現代作家支持，像是匈牙利的[Bálint
Sárosi](../Page/Bálint_Sárosi.md "wikilink")。然而[Simon
Broughton也宣稱在洛馬音樂的本質中](../Page/Simon_Broughton.md "wikilink")：「匈牙利原始風味與農民音樂其實有許多相似之處，較民俗學家同意認同的部分多出許多。」。另外[Marian
Cotton與](../Page/Marian_Cotton.md "wikilink")[Adelaide
Bradburn則認為匈牙利境內的洛馬音樂](../Page/Adelaide_Bradburn.md "wikilink")「也許在本質上是來自於匈牙利傳統，但洛馬音樂本身有著十分複雜的發展，也讓其風格改變許多。因此我們很難將匈牙利的音樂與所謂真正的洛馬音樂劃分清楚。」。

除了洛馬與匈牙利的民族音樂之外，匈牙利一路傳承的音樂傳統還包括由充滿活力的[Pomáz和](../Page/Pomáz.md "wikilink")[Szentendre族群所保有的](../Page/Szentendre.md "wikilink")[塞爾維亞音樂傳統](../Page/塞爾維亞音樂傳統.md "wikilink")、以及原本居住在[摩爾多瓦](../Page/摩爾多瓦.md "wikilink")[Seret
Valley地區的](../Page/Seret_Valley.md "wikilink")[Csángó族](../Page/Csángó.md "wikilink")，他們如今多數搬遷到[布達佩斯](../Page/布達佩斯.md "wikilink")。他們在樂器演奏與搭配上（像長笛、小提琴、魯特琴等）有著鮮明風格，成為當地民謠運動中的標誌。

### 維爾補恩克斯 (Verbunkos)

[Coloredlithograph.png](https://zh.wikipedia.org/wiki/File:Coloredlithograph.png "fig:Coloredlithograph.png")
depicting a recruitment with 音樂\]\]

在19世紀[維爾補恩克斯是在匈牙利最受歡迎的音樂風格](../Page/維爾補恩克斯.md "wikilink")。這是種具有節奏的舞蹈音樂形式，是由一種由快速舞蹈延伸出的慢舞形式所組成。這種舞蹈音樂有快慢交錯的兩種形式，也曾被視為是「兩個截然不同的音樂類型」。維爾補恩克斯源自於新兵加入軍隊宣誓時，在軍隊典禮中所使用的音樂表演形式。後來有部分曲式成為了[進行曲](../Page/進行曲.md "wikilink")，也形成音樂家[李斯特與](../Page/李斯特.md "wikilink")[埃克托·柏遼茲作品中重要的一部份](../Page/埃克托·柏遼茲.md "wikilink")。當時沒有太多人知道它的起源，推測其來源可能是一些傳統的古老舞曲，像[swine-herd
dance及](../Page/swine-herd_dance.md "wikilink")[Heyduck
dance](../Page/Heyduck_dance.md "wikilink")，還有來自[巴爾幹半島](../Page/巴爾幹半島.md "wikilink")、斯拉夫與[黎凡特](../Page/黎凡特.md "wikilink")（Levantine）的音樂元素影響，還有[義大利與](../Page/義大利.md "wikilink")[維也納的上層文化音樂](../Page/維也納.md "wikilink")，而洛馬音樂家最後匯集了上述的音樂曲風。維爾補恩克斯日漸受到歡迎，但不只是在貧苦農民之間傳唱，也在上層貴族流行了起來，他們認為
verbunkos
是真正代表匈牙利民族的音樂。維爾補恩克斯的演出形式中包括了：[bokázó](../Page/bokázó.md "wikilink")
(*敲擊鞋跟*)、終止式音樂形式（cadence-pattern），並使用二度增音程（augmented
interval）、三連音，有著寬廣的音樂幅度、即興且無歌詞的韻律、快速交替的變化、緩慢節拍與弱拍節奏。在18世紀末期
Verbunkos
被應用在歌劇、[室內樂](../Page/室內樂.md "wikilink")、[鋼琴音樂及歌詞文學當中](../Page/鋼琴.md "wikilink")，並且被視為「古匈牙利舞蹈與音樂的復興，此種音樂的成功象徵了大眾藝術的興起。」。

當時19世紀著名的音樂家有享有盛譽的小提琴家[Panna
Czinka](../Page/Panna_Czinka.md "wikilink")，而樂團領導人[雅諾斯·畢哈里](../Page/雅諾斯·畢哈里.md "wikilink")（János
Bihari）則被譽為「提琴界的[拿破崙](../Page/拿破崙.md "wikilink")」。。畢哈里、[Antal
Csermák與其他作曲家更使得](../Page/Antal_Csermák.md "wikilink")[verbunkos能](../Page/verbunkos.md "wikilink")「表達出最深刻的匈牙利音樂[浪漫主義](../Page/浪漫主義.md "wikilink")。」，並且使其在「民族特有音樂中佔有重要角色。」Bihari
在推廣並創新 verbunkos 上有著特殊巨大的貢獻，被譽為「狂野想像裡音樂魔鬼的化身」。 Bihari
與後繼者創造了[nota這種流傳後世的樂曲](../Page/nota.md "wikilink")，這是種由作曲家如[Lóránt
Fráter](../Page/Lóránt_Fráter.md "wikilink")、[Árpád
Balázs](../Page/Árpád_Balázs.md "wikilink")、[Pista
Dankó](../Page/Pista_Dankó.md "wikilink")、[Béni
Egressy](../Page/Béni_Egressy.md "wikilink")、[Márk
Rózsavölgyi與](../Page/Márk_Rózsavölgyi.md "wikilink")[Imre
Farkas](../Page/Imre_Farkas.md "wikilink")
所譜寫且廣泛流傳的樂曲形式。這幾位現代匈牙利音樂大師皆為演奏
verbunkos 的團體
[Lakatos的家族成員](../Page/Lakatos.md "wikilink")，這團體中也包括了[Sándor
Lakatos和](../Page/Sándor_Lakatos.md "wikilink")[Roby
Lakatos等人](../Page/Roby_Lakatos.md "wikilink")。。

### 洛馬音樂 (洛馬音樂)

雖然洛馬音樂（如
verbunkos）基本上被認為是匈牙利的表演風格，但其仍有自身專屬的民謠音樂形式（特色是多半沒有樂器伴奏）。雖然他們的名聲也在洛馬社群外流傳，但洛馬音樂傾向於呈現周遭人群所使用的特殊音樂。然而因其並未修飾過的「曲折和顫音」變化，反而創造出嶄新、具有特色的洛馬風格。儘管沒有樂器伴奏，音樂家仍用棍棒快速敲擊地面以產生節奏聲響，搭配一種稱為[口發低音](../Page/口發低音.md "wikilink")（oral-bassing）的仿效樂器聲的特殊技巧。一些現代洛馬音樂家，例如[Ando
Drom](../Page/Ando_Drom.md "wikilink")、[Rota與](../Page/Rota.md "wikilink")[Kalyi
Jag在洛馬音樂中加入了像](../Page/Kalyi_Jag.md "wikilink")[吉他之類的現代樂器](../Page/吉他.md "wikilink")。此外[Gyula
Babos主導的](../Page/Gyula_Babos.md "wikilink")[Project
Romani更在其音樂中添加](../Page/Project_Romani.md "wikilink")[前衛爵士的音樂元素](../Page/前衛爵士.md "wikilink")。。

### 匈牙利的音樂在外國的發展

<div style="float: right;">

</div>

從種族上看，匈牙利民族居住在[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")、[塞爾維亞](../Page/塞爾維亞.md "wikilink")、[斯洛伐克](../Page/斯洛伐克.md "wikilink")、[美國](../Page/美國.md "wikilink")
與世界各地。位於羅馬尼亞的匈牙利民族(分佈於在川西凡尼亞及[桑格](../Page/桑格.md "wikilink")（Csángó）區)在音樂的部分反而最多是受到匈牙利的影響，在斯洛伐克的匈牙利人則組成一個稱為[Ghymes的正統樂團](../Page/Ghymes.md "wikilink")，專門演奏傳統的[táncház音樂](../Page/táncház.md "wikilink")。。位於[伏伊伏丁那](../Page/伏伊伏丁那.md "wikilink")[塞爾維亞地區除了大批匈牙利少數民族的所在地之外](../Page/塞爾維亞.md "wikilink")，在其他區域匈牙利音樂則成為[塞爾維亞](../Page/塞爾維亞.md "wikilink")[民族主義者攻擊的對象](../Page/民族主義者.md "wikilink")。。

川西凡尼亞民謠音樂在現代川西凡尼亞人的生活中仍佔有最重要的部分。巴爾托克和高大宜發現川西凡尼亞地區是能夠採集豐富民謠歌曲的區域，更是豐腴的處女地。這類型的民謠團體往往是三人組的弦樂團體，包括了[小提琴](../Page/小提琴.md "wikilink")、[中提琴及](../Page/中提琴.md "wikilink")[低音提琴](../Page/低音提琴.md "wikilink")，有時亦有[欽巴隆的加入](../Page/欽巴隆.md "wikilink")。通常由第一提琴手或*primás*演奏主旋律，搭配著其他伴奏音樂與節奏演出。。
川西凡尼亞同時也是[當齊斯](../Page/當齊斯.md "wikilink")()的發源地，此種音樂類型也在匈牙利廣泛的流傳。

### 當齊斯 (Táncház)

[當齊斯](../Page/當齊斯.md "wikilink")(*舞蹈屋*)是種在1970年代開始流傳的[舞曲形式](../Page/舞曲.md "wikilink")，是作為對抗國家所支持的民謠音樂而出現的產物，也被描述成一種介於[穀倉舞](../Page/穀倉舞.md "wikilink")（類似波爾卡的農村舞蹈）與民謠之間的過渡音樂類型。這種音樂通常是以節拍緩慢的verbunkos
或[Lad's
Dance](../Page/Lad's_Dance.md "wikilink")（一種男子舞蹈音樂）作為起始，緊接於後的是快速的[czárdás](../Page/czardas.md "wikilink")。Czárdás
是種融合許多地區特色且相當受歡迎的匈牙利舞曲，特徵是節拍上的改變。當齊斯是民謠歌曲的綜合體，音樂家[Béla
Halmos與](../Page/Béla_Halmos.md "wikilink")[Ferenc
Sebő與舞曲收集家](../Page/Ferenc_Sebő.md "wikilink")[György
Martin與](../Page/György_Martin.md "wikilink")[Sándor
Timár](../Page/Sándor_Timár.md "wikilink")，蒐集了大量在城市絕跡的民間演奏及舞蹈音樂。特別的是這些歌曲的源頭雖然是來自[羅馬尼亞的](../Page/羅馬尼亞.md "wikilink")[川西凡尼亞](../Page/川西凡尼亞.md "wikilink")，但此地的人口絕大部分卻是由匈牙利人組成。這些團體用於演奏樂器的形式是以[提琴為主](../Page/提琴.md "wikilink")，[小提琴及](../Page/小提琴.md "wikilink")[低音吉他為輔](../Page/低音吉他.md "wikilink")，有時也加入[欽巴隆](../Page/欽巴隆.md "wikilink")，而這種形式是源自於居住在[川西凡尼亞與南](../Page/川西凡尼亞.md "wikilink")[斯洛伐克的匈牙利社區](../Page/斯洛伐克.md "wikilink")，。

許多當代匈牙利音樂家，都是在[當齊斯蓬勃的時期嶄露頭角](../Page/當齊斯.md "wikilink")，像[Muzsikás與](../Page/Muzsikás.md "wikilink")[Márta
Sebestyén](../Page/Márta_Sebestyén.md "wikilink")。其他團體還有[Vujicsics](../Page/Vujicsics.md "wikilink")、[Jánosi](../Page/Jánosi.md "wikilink")、[Téka與](../Page/Téka.md "wikilink")[Kalamajka](../Page/Kalamajka.md "wikilink")。歌手的部分有[Éva
Fábián與](../Page/Éva_Fábián.md "wikilink")[András
Berecz](../Page/András_Berecz.md "wikilink")。著名演奏家包含提琴家[Csaba
Ökrös](../Page/Csaba_Ökrös.md "wikilink")、欽巴隆演奏家[Kálmán
Balogh](../Page/Kálmán_Balogh.md "wikilink")、小提琴家[Félix
Lajkó](../Page/Félix_Lajkó.md "wikilink")(來自於[塞爾維亞的](../Page/塞爾維亞.md "wikilink")[蘇博蒂察](../Page/蘇博蒂察.md "wikilink")），還有演奏家[Mihály
Dresch](../Page/Mihály_Dresch.md "wikilink")。

## 古典音樂

在全世界的[古典音樂中](../Page/古典音樂.md "wikilink")，匈牙利貢獻最顯著的音樂家就是[李斯特](../Page/李斯特.md "wikilink")，這位創作了*[匈牙利狂想曲](../Page/匈牙利狂想曲.md "wikilink")*與
*[前奏曲](../Page/前奏曲.md "wikilink")*的大師，是19世紀備受尊崇的鋼琴家與作曲家。李斯特生存的年代，也是尋找自我本質的現代匈牙利古典音樂成型之際。除了[李斯特之外](../Page/李斯特.md "wikilink")，同時期的[费伦茨·艾凯尔製作的義大利及法國風格的歌劇](../Page/费伦茨·艾凯尔.md "wikilink")，也搭配著匈牙利語的台詞，另外具有德國古典風格的大師[Mihaly
Mosonyi](../Page/Mihaly_Mosonyi.md "wikilink")，他們都替日後的音樂發展鋪下一條大道，其所帶來的影響是後繼者望塵莫及的。這不只是因為他們個人的天份，更因為他們努力將洛馬音樂的精髓融入作品中，以熱情將傳統發揮的淋漓盡致。。

[Szell.jpeg](https://zh.wikipedia.org/wiki/File:Szell.jpeg "fig:Szell.jpeg")

匈牙利也孕育出了許多傑出音樂家，像是創作[Rustic Wedding
Symphony的作曲家](../Page/Rustic_Wedding_Symphony.md "wikilink")[卡尔·戈德马克](../Page/卡尔·戈德马克.md "wikilink")、作曲家兼鋼琴家[埃尔诺·多赫南伊和鋼琴樂作曲家](../Page/埃尔诺·多赫南伊.md "wikilink")[斯蒂芬·海勒](../Page/斯蒂芬·海勒.md "wikilink")。許多來自匈牙利的小提琴家皆享譽國際，尤其是[姚阿幸·約瑟夫](../Page/姚阿幸·約瑟夫.md "wikilink")、[Jenő
Hubay](../Page/Jeno_Hubay.md "wikilink")、[Edward
Remenyi與](../Page/Edward_Remenyi.md "wikilink")[萊奧波德·奧爾](../Page/萊奧波德·奧爾.md "wikilink")。出生於匈牙利的[指揮家有](../Page/指揮家.md "wikilink")[Antal
Doráti](../Page/Antal_Dorati.md "wikilink")、[尤金·奧曼迪](../Page/尤金·奧曼迪.md "wikilink")、[弗里茲·萊納及](../Page/弗里茲·萊納.md "wikilink")[喬治·塞爾](../Page/喬治·塞爾.md "wikilink")
。

### 匈牙利歌劇

匈牙利歌劇的起源為18世紀末自外國引進的歌劇興起，當時許多地區像是[波索尼](../Page/波索尼.md "wikilink")（Pozsony）、[Kismarton](../Page/Kismarton.md "wikilink")、[錫比烏與](../Page/錫比烏.md "wikilink")[布達佩斯都有音樂會舉行](../Page/布達佩斯.md "wikilink")，且多半受到[日耳曼歌劇或](../Page/日耳曼歌劇.md "wikilink")[義大利歌劇影響](../Page/義大利歌劇.md "wikilink")。匈牙利歌劇發展源於戲劇學校與日耳曼歌劇的改編，這些戲劇學校包含了位於[Sátoraljaújhely的](../Page/Sátoraljaújhely.md "wikilink")[Pauline
School](../Page/Pauline_School.md "wikilink")、位於[Csurgó的](../Page/Csurgó.md "wikilink")[Calvinist
School以及設立於](../Page/Calvinist_School.md "wikilink")[布達佩斯的](../Page/布達佩斯.md "wikilink")[Piarist
School](../Page/Piarist_School.md "wikilink")。。

第一部音樂劇的嘗試是1793年晚期由[Gáspár
Pacha與](../Page/Gáspár_Pacha.md "wikilink")[József
Chudy在](../Page/József_Chudy.md "wikilink")[布拉提斯拉瓦所製作的](../Page/布拉提斯拉瓦.md "wikilink")「Prince
Pikkó and Jutka Perzsi」，這也是普遍認同的首部匈牙利歌劇。本劇是翻譯自由[Philipp
Hafner所創作的](../Page/Philipp_Hafner.md "wikilink")「*Prinz Schnudi
und Prinzessin
Evakathel*」。此時期的歌劇仍具有強烈源於[維也納](../Page/維也納.md "wikilink")[Zauberposse形式的喜劇風格](../Page/Zauberposse.md "wikilink")，這種狀態一直持續到19世紀。但即使採取國外歌劇形式，在故事中的「詩詞、歌詞與主角」仍以
verbunkos 為基礎，也在此時成為匈牙利的民族國家象徵。19世紀中葉[Ferenc
Erkel採用了法國與義大利的範本](../Page/Ferenc_Erkel.md "wikilink")，創作了首部以匈牙利語為發音的歌劇。。

### 巴爾托克與高大宜

19世紀末匈牙利音樂的發展受到德國的古典音樂主導，不過這種情況到了1905年則有所轉變。此時[Endre
Ady開始發行詩集](../Page/Endre_Ady.md "wikilink")、作曲家[巴托克·貝拉也首次發表了作品](../Page/巴托克·貝拉.md "wikilink")，同時[高大宜也開始進行收集民謠歌曲的工作](../Page/高大宜.md "wikilink")。巴托克與高大宜都是優秀的作曲家，他們的音樂有著具特色的匈牙利風格。高大宜特別專注於匈牙利民謠音樂，而巴托克則對各式的民謠音樂都有興趣，因此在東歐各國（尤其是匈牙利）收集傳統民謠的資訊，並將其運用在自己的音樂創作中。與先前嘗試探索匈牙利在地風情的作曲家相比，高大宜與巴爾托克並未將洛馬與匈牙利民族音樂併在一起，反而就兩者後期的發展形式做了詳細分類，他們的音樂成就被視為影響「匈牙利人傳統」與「後世本國作曲家」的重要分水嶺。。

### 20世紀

20世紀前期，巴托克與高大宜是最具影響力的代表人物－尤其在高大宜於1947年復興傳統合唱民謠之時。但是這項風潮卻在1950年代被迫中止，此時由國家主張下所興起的[共產主義優於一切](../Page/共產主義.md "wikilink")。當時「信仰與意識型態成為作曲家創作音樂時的衡量基準，那些可恥的形容詞像
'形式主義的' 與
'世界主義的'在一時間蔚為風潮（古典[詠嘆調](../Page/詠嘆調.md "wikilink")、[詠嘆調或](../Page/詠嘆調.md "wikilink")[奏鳴曲成為合乎體統的匈牙利音樂主要定義](../Page/奏鳴曲.md "wikilink")。並消除了高大宜作品中那些會引起偏差觀念的歡樂與樂觀氣氛，此舉致造成了乏味無趣的結果，使其失去大眾的支持。」當時知名的作曲家是[Endre
Szervánszky與](../Page/Endre_Szervánszky.md "wikilink") [Lajos
Bárdos](../Page/Lajos_Bárdos.md "wikilink") 。

大約從1955年開始，受巴爾托克啟發的新一代作曲家逐漸展露頭角，為匈牙利音樂添加新的元素。這個時代的作曲家包括了[Ferenc
Szabó](../Page/Ferenc_Szabó.md "wikilink")、[Endre
Szervánszky](../Page/Endre_Szervánszky.md "wikilink")、[Pál
Kadosa](../Page/Pál_Kadosa.md "wikilink")、[Ferenc
Farkas與](../Page/Ferenc_Farkas.md "wikilink")[György
Ránki](../Page/György_Ránki.md "wikilink")。他們除了都回歸到匈牙利音樂舊時的技巧，同時亦採用當時引進的西方古典音樂中[前衛與現代的元素](../Page/前衛.md "wikilink")。。

## 流行音樂

在20世紀初，匈牙利的流行音樂是由[輕歌劇和多樣化的洛馬音樂所組成](../Page/輕歌劇.md "wikilink")。[Nagymező
utca地區因為擁有為數眾多的夜店與劇場](../Page/Nagymező_utca.md "wikilink")，被稱為「[布達佩斯的](../Page/布達佩斯.md "wikilink")[百老匯](../Page/百老匯.md "wikilink")」，並成為流行音樂的主要發展中心。然而在1945年此地的輝煌時代突然被中止，此時期的流行音樂即是指愛國歌曲，並與蘇俄共產黨劃上等號。雖然仍有為數不多的歌劇仍持續上演著，但任何受西方影響的音樂都會被視為有害且危險的事物。。

然而到了1956年，隨著「3T」而起的自由主義開始風起雲湧（包含了'tűrés*、*tiltás*和*támogatás*，意思是*寬容*、*禁制*與*支持'')，而一段持續頗久的文化掙扎期也自此開始，起使的原因是由於[非裔美國人所屬的](../Page/非裔美國人.md "wikilink")[爵士樂進入此地](../Page/爵士樂.md "wikilink")。。

### 搖滾樂

[搖滾樂最初是源於](../Page/搖滾樂.md "wikilink")[非裔美國人的音樂風格](../Page/非裔美國人.md "wikilink")，後來則成為英美及世界其他地區的白人特色音樂。在1960年代初期，匈牙利青少年們開始群聚欣賞搖滾樂，即使當局仍然對此加以譴責。在1970年初期有三組搖滾樂團佔有重要地位，分別是[Illés](../Page/Illés.md "wikilink")、[Metró與](../Page/Metró_\(band\).md "wikilink")[Omega](../Page/Omega_\(band\).md "wikilink")，這三組樂團也都發行過至少一張以上的專輯。一些零星的樂團也曾錄製過少許單曲，但國營的[音樂製作公司並未宣傳或支持這些樂團](../Page/音樂製作公司.md "wikilink")，也因此他們很快的就消失了。。

到了1968年，[新經濟政策](../Page/新經濟政策.md "wikilink")（New Economic
Mechanism）開始推行，並試圖復甦匈牙利的經濟狀況。此時樂團[Illés贏得知名的](../Page/Illés.md "wikilink")[Táncdal
Fesztivál中幾乎所有的獎項](../Page/Táncdal_Fesztivál.md "wikilink")。然而到了1970年代，[蘇聯對所有在匈牙利的](../Page/蘇聯.md "wikilink")「危險份子」進行了制裁，而搖滾樂手就成為主要的攻擊目標。當[Metró與](../Page/Metró.md "wikilink")[Omega離開此地之時](../Page/Omega.md "wikilink")，[Illés則被禁止繼續表演與灌錄唱片](../Page/Illés.md "wikilink")。這些團體中的部分成員組成了一個超級樂團－[LGT](../Page/LGT.md "wikilink")，並且迅速的走紅。同時來自於「Omega」的其餘團員也在[德國的演藝圈取得空前的成功](../Page/德國.md "wikilink")，並在此時仍相當的受到歡迎。

在1970年代晚期，搖滾團體必須要服從唱片公司的種種要求，並且保證所有歌曲都能通過[歌曲委員會的審查](../Page/歌曲委員會.md "wikilink")（這個單位專門檢查歌曲是否有違理念。）[LGT是支相當受到歡迎的](../Page/LGT.md "wikilink")[傳統搖滾經典樂團](../Page/傳統搖滾.md "wikilink")。當時還有些較迎合歌曲審查委員會標準的樂團，專門製作以搖滾樂為主卻沒有顛覆暗示的[流行音樂](../Page/流行音樂.md "wikilink")，像[The
Sweet與](../Page/The_Sweet.md "wikilink")[Middle of the
Road](../Page/Middle_of_the_Road.md "wikilink")。於此同時由官方批准，屬於[電子樂](../Page/電子樂.md "wikilink")[迪斯科類型的表演者也開始出現](../Page/迪斯科.md "wikilink")，像是[Neoton
Familia](../Page/Neoton_Familia.md "wikilink")、[Beatrice與](../Page/Beatrice_\(band\).md "wikilink")[Szűcs
Judit](../Page/Szűcs_Judit.md "wikilink")。在同一時期也有更具批判特色的[前衛搖滾團體出現](../Page/前衛搖滾.md "wikilink")，像[East](../Page/East_\(band\).md "wikilink")、[V73](../Page/V73.md "wikilink")、[Color和](../Page/Color_\(band\).md "wikilink")[Panta
Rhei](../Page/Panta_Rhei.md "wikilink")。。

1980年代初期，經濟不景氣與低迷的社會氣氛嚴重打擊了匈牙利的文化發展，更導致一股青年人覺醒與採取不合作態度的風潮。使得聆聽搖滾樂並迫切向外尋求[龐克搖滾的人數迅速增長](../Page/龐克搖滾.md "wikilink")，當時主要樂團包括了[Beatrice這個頗受好評的樂團](../Page/Beatrice.md "wikilink")，他們自[迪斯科的風格轉型至](../Page/迪斯科.md "wikilink")[龐克與](../Page/龐克.md "wikilink")[民謠搖滾](../Page/民謠搖滾.md "wikilink")，歌曲未經審查且常有誇張演出。其他還有像[P。
Mobil](../Page/P。_Mobil.md "wikilink")、[Hobo Blues
Band及憂鬱的二重唱](../Page/Hobo_Blues_Band.md "wikilink")[Bizottság與](../Page/Bizottság.md "wikilink")[Edda
művek](../Page/Edda_művek.md "wikilink")。

在1980年代，國營的「音樂製作公司」（Record Production Company）也隨之瓦解。因為匈牙利當局意識到限制
搖滾樂並不能有效的降低其影響力，取而代之的作法是鼓勵年輕音樂人演唱共產主義教條與守則的相關歌曲，想藉此削弱其影響力。在先前的十年間他們見到了由[龐克音樂與](../Page/龐克音樂.md "wikilink")[新浪潮音樂所帶來的全面性衝擊](../Page/新浪潮音樂.md "wikilink")，而政府當局也決定乾脆將此形式併入其控管之下。第一批主要被判入獄的搖滾樂危險份子被釋放，例如曾因政治煽動罪而被判刑入獄兩年的[龐克樂團](../Page/龐克樂團.md "wikilink")[CPG的成員](../Page/CPG.md "wikilink")。。

在1990年代初期，匈牙利內部局勢的困難使得政府無暇監控搖滾樂及其他樂團的表演活動。在共黨政府垮台後，匈牙利的表演風格變的越來越類似於其他歐洲國家的演出形式
。

## 節慶、場所和其他組織

[布達佩斯除了是匈牙利的首都之外](../Page/布達佩斯.md "wikilink")，同時也是音樂重鎮。世界音樂作家[Simon
Broughton稱此地為匈牙利能聽到](../Page/Simon_Broughton.md "wikilink")「真正優秀的民謠音樂」的最佳去處。在此每年都會舉辦稱為[當齊斯találkozó民謠大會的活動](../Page/當齊斯találkozó.md "wikilink")(或稱為*舞廳集會*)，這是當地現代音樂的主要活動之一。[布達佩斯歷史悠久的表演場所包含了](../Page/布達佩斯.md "wikilink")1853年成立的[Philharmonic
Society](../Page/Philharmonic_Society.md "wikilink")、1884年成立的[Opera
House of
Budapest](../Page/Opera_House_of_Budapest.md "wikilink")（屬於當地的音樂學會，在1875年開幕時由[李斯特擔任校長](../Page/李斯特.md "wikilink")，並由[Ferenc
Erkel從事指導工作](../Page/Ferenc_Erkel.md "wikilink")。至今仍是匈牙利重要的音樂教育中心。）

匈牙利[文化部組成了由政府運作的](../Page/文化部.md "wikilink")[國際文化基金會](../Page/國際文化基金會.md "wikilink")（National
Cultural Fund），藉此贊助某些音樂組織團體。在匈牙利的非營利音樂組織包括有匈牙利[Jazz
Alliance與](../Page/Jazz_Alliance.md "wikilink")[匈牙利音樂協會](../Page/匈牙利音樂協會.md "wikilink")
。

## 參考資料

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 延伸閱讀

  -
  -
  -
  -
  -
  -
## 外部連結

[Category:匈牙利文化](../Category/匈牙利文化.md "wikilink")
[Category:各國音樂](../Category/各國音樂.md "wikilink")