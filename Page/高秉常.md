**高秉常**主教（，）是[葡萄牙籍](../Page/葡萄牙人.md "wikilink")[天主教主教](../Page/天主教.md "wikilink")。也是第21任[澳門教區](../Page/天主教澳門教區.md "wikilink")[主教](../Page/天主教澳門教區主教列表.md "wikilink")。

## 生平

高秉常出生於1924年7月8日，[葡萄牙](../Page/葡萄牙.md "wikilink")[亞速爾群島當中](../Page/亞速爾群島.md "wikilink")[皮庫島的](../Page/皮庫島.md "wikilink")[馬達萊娜](../Page/馬達萊娜_\(亞速爾群島\).md "wikilink")。自幼到[葡屬澳門就讀](../Page/葡屬澳門.md "wikilink")[聖若瑟修院](../Page/聖若瑟修院.md "wikilink")。1949年10月6日，在[澳門教區晉鐸](../Page/天主教澳門教區.md "wikilink")。他曾出任修院教授、院長及署理主教之職。1973年被選為[天主教澳門教區代主教](../Page/天主教澳門教區.md "wikilink")，掌管教區教務。

1976年3月25日，被時任[教宗](../Page/教宗.md "wikilink")[保祿六世委任為第](../Page/保祿六世.md "wikilink")21任[澳門教區](../Page/天主教澳門教區.md "wikilink")[主教](../Page/天主教澳門教區主教列表.md "wikilink")。同年3月25日，在[澳門](../Page/澳門.md "wikilink")[聖母聖誕主教座堂由](../Page/澳門主教座堂.md "wikilink")[胡振中主教晉牧就職](../Page/胡振中.md "wikilink")。

1979年，高秉常主教訓令[聖若瑟中學與將](../Page/聖若瑟教區中學第二、三校.md "wikilink")[望德中學](../Page/聖若瑟教區中學第一校.md "wikilink")、[真原小學合併成為](../Page/聖若瑟教區中學第五校.md "wikilink")[聖若瑟教區中學](../Page/聖若瑟教區中學.md "wikilink")，也成為全[澳門唯一的聯網學校](../Page/澳門.md "wikilink")。

高主教在任期間把澳門教區的財務及行政重新納入正軌，同時組織[澳門天主教學校聯會](../Page/澳門天主教學校聯會.md "wikilink")，以聯絡各[修會主辦的](../Page/修會.md "wikilink")[學校](../Page/學校.md "wikilink")。並組織[澳門修女聯會](../Page/澳門修女聯會.md "wikilink")，以聯絡駐澳各女修會。又創辦五所牧民中心，以促進社會傳教和培育工作。高主教於1988年10月6日榮休。於2016年9月12日，在故鄉[葡萄牙](../Page/葡萄牙.md "wikilink")[亞速爾群島的](../Page/亞速爾群島.md "wikilink")[皮庫島逝世](../Page/皮庫島.md "wikilink")，終年92歲。

高主教於1986年獲[澳門東亞大學授予其榮譽哲學博士學位](../Page/澳門大學.md "wikilink")。1984年獲葡萄牙共和國總統授予其功德高級軍官勛章，1988年又授予其功績大十字勳章。他故鄉的亞速爾群島地區立法會，在2012年授予其自治區傑出成就勳章。

## 參考資料

[Category:澳門天主教徒](../Category/澳門天主教徒.md "wikilink")