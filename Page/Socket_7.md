**Socket 7**是一种有着321个针脚的[x86](../Page/x86.md "wikilink")
[CPU插座](../Page/CPU插座.md "wikilink")。

Socket 7是方形多針腳ZIF（Zero Insertion
Force，零插拔力）插座，插座上有一根拉杆，在安裝和更換CPU時只要將拉杆向上拉出，就可以輕易地插進或取出CPU。Socket
7也是CPU進入「[Pentium時代](../Page/Pentium.md "wikilink")」後，最常見的主機板架構，主要特點是──具有66MHz的標準外頻（最高83MHz）、一般提供雙電壓供電機制。这种规格的插座支持[Intel](../Page/Intel.md "wikilink")
[Pentium处理器的部分早期型号](../Page/Pentium.md "wikilink")，同时也兼容其他厂商的CPU，如[Cyrix](../Page/Cyrix.md "wikilink")、[AMD等等](../Page/AMD.md "wikilink")。

## 发布背景

这种规格的插座用于取代较早的[Socket 5](../Page/Socket_5.md "wikilink")。Socket
7是唯一一种支持如此多厂商（Intel、Cyrix、AMD等等）CPU的插座，以致于成为一种事实的工业标准。Intel为了享有独占接口的权利，在之后推出了[Slot
1](../Page/Slot_1.md "wikilink")。Intel在放弃该平台时的最后一款CPU是Pentium MMX
233 MHz。AMD支持该平台直到2000年9月，即[K6-2+与](../Page/K6-2+.md "wikilink")[K6-III](../Page/K6-III.md "wikilink")，在一些芯片组支持下甚至能在600MHz下工作。AMD还扩展了Socket
7而推出了[Super Socket 7](../Page/Super_Socket_7.md "wikilink")。

## 使用Socket 7的处理器

### Intel

  - *P54C* [Pentium](../Page/Pentium.md "wikilink") 75 MHz～200 MHz（FSB
    50 MHz/60 MHz/66 MHz）
  - *P55C* [Pentium MMX](../Page/Pentium_MMX.md "wikilink")
    150 MHz～233 MHz（FSB 60 MHz/66 MHz）

### AMD

  - *SSA-5* [K5](../Page/K5.md "wikilink")／5K86
    P75～PR100（50 MHz～66 MHz）（FSB 50 MHz/60 MHz/66 MHz）
  - *5k86* [K5](../Page/K5.md "wikilink")
    PR120～PR200（90 MHz～133 MHz）（FSB 60 MHz/66 MHz）
  - [K6](../Page/K6.md "wikilink") 166 MHz～300 MHz（FSB 66 MHz）
  - *Chompers* [K6-2](../Page/K6-2.md "wikilink") 233 MHz～350 MHz（FSB
    66 MHz/100 MHz）
  - *CXT* [K6-2](../Page/K6-2.md "wikilink") 266 MHz～550 MHz（FSB
    66 MHz/100 MHz）
  - [K6-2+](../Page/K6-2+.md "wikilink") 450 MHz～540 MHz
  - *Sharptooth* [K6-III](../Page/K6-III.md "wikilink") 400 MHz～500 MHz

### Cyrix

  - [Cyrix 6x86](../Page/Cyrix_6x86.md "wikilink")
    P90～PR166（80 MHz～133 MHz）
  - 6x86L PR150～PR200（120 MHz～166 MHz）
  - 6x86MX PR200～PR300（150 MHz～233 MHz）
  - [MII](../Page/MII.md "wikilink") 200GP～400GP（166 MHz～285 MHz）

### IDT

  - Winchip C6 180 MHz～255 MHz
  - Winchip 200 MHz～300 MHz

### RISE

  - Rise mp6 166 MHz\~300 MHz

## 支持Socket 7的芯片组

  - Intel 430FX
  - Intel 430HX
  - Intel 430VX
  - Intel 430TX
  - VIA VP1
  - VIA VP2
  - VIA VPX
  - VIA VP3
  - VIA MVP3
  - VIA MVP4
  - ALi Aladdin IV
  - ALi Aladdin V
  - ALi Aladdin 7
  - SiS 530/5598

## 参见

  - [Super Socket 7](../Page/Super_Socket_7.md "wikilink")
  - [Slot 1](../Page/Slot_1.md "wikilink")

[Category:CPU插座](../Category/CPU插座.md "wikilink")