**聯合縣**（Union County,
Ohio）是[美國](../Page/美國.md "wikilink")[俄亥俄州中部的一個縣](../Page/俄亥俄州.md "wikilink")。面積1,132平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口10,909。縣治[马里斯维尔](../Page/马里斯维尔_\(俄亥俄州\).md "wikilink")（Marysville）。

成立於1820年1月10日。本縣由[洛根縣](../Page/洛根縣_\(俄亥俄州\).md "wikilink")、[特拉華縣](../Page/特拉華縣_\(俄亥俄州\).md "wikilink")、[富蘭克林縣和](../Page/富兰克林县_\(俄亥俄州\).md "wikilink")[麥迪遜縣的部分土地合成](../Page/麥迪遜縣_\(俄亥俄州\).md "wikilink")，縣名亦由此得來。

## 參考文獻

[U](../Category/俄亥俄州行政區劃.md "wikilink")