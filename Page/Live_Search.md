**Live Search**（前稱**Windows Live Search**及**MSN
Search**）是[微軟開發的網路](../Page/微軟.md "wikilink")[搜尋引擎](../Page/搜尋引擎.md "wikilink")，亦是[MSN
Search後續的搜尋服務](../Page/MSN_Search.md "wikilink")，目的是為了與知名的搜尋引擎[Google和](../Page/Google.md "wikilink")[Yahoo\!競爭](../Page/Yahoo!.md "wikilink")。Live
Search的公開測試版在2006年3月8日發佈，並在同年9月11日發佈正式版本。用戶可透過微軟的[Windows
Live總覽網站](../Page/Windows_Live.md "wikilink")[Live.com和](../Page/Live.com.md "wikilink")[MSN網站使用Live](../Page/MSN.md "wikilink")
Search的服務。Live Search現在已經被[Bing取代](../Page/Bing.md "wikilink")。

這個搜尋引擎提供了一些創新的功能，例如用戶可以透過同一個網頁來瀏覽附加的搜尋結果（不須點擊多重的搜尋結果頁便能瀏覽更多的搜尋結果）以及能設定每個搜尋結果的文字介紹（例如能設定文字介紹的長度或只顯示網頁標題）。它亦允許用戶儲存搜尋結果到[Live.com上以及能自動更新搜尋結果](../Page/Live.com.md "wikilink")。

### 其他服務

[Live Search
Mobile提供行動裝置可用的搜尋服務](../Page/Live_Search_Mobile.md "wikilink")。

[Live Search Product
Upload提供商家上傳商品資訊到](../Page/Live_Search_Product_Upload.md "wikilink")[Live
Search Product](../Page/Live_Search_Product.md "wikilink")。

[微軟亦研發了其他的搜尋工具和服務](../Page/微軟.md "wikilink")，如[Windows Desktop
Search等](../Page/Windows_Desktop_Search.md "wikilink")。

## 參見

  - [Windows Live](../Page/Windows_Live.md "wikilink")

[Category:Windows Live](../Category/Windows_Live.md "wikilink")
[Category:網絡搜尋引擎](../Category/網絡搜尋引擎.md "wikilink")