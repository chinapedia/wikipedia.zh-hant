{{ Infobox Software | name = Gnash | logo = | screenshot =
[Gnash-0.8.0.png](https://zh.wikipedia.org/wiki/File:Gnash-0.8.0.png "fig:Gnash-0.8.0.png")
| caption = Screenshot of Gnash 0.8.0 GTK+ GUI with test SWF file |
maintainer = [Rob Savoye](../Page/Rob_Savoye.md "wikilink") | developer
= [Rob Savoye](../Page/Rob_Savoye.md "wikilink"), Sandro Santilli,
Bastiaan Jacques, Vitaly Alexeev, Tomas Groth, Udo Giacomozzi, Hannes
Mayr, Markus Gothe, Ann Barcomb. | latest_release_version = 0.8.10 |
latest_release_date =  | operating_system =
[跨平台](../Page/跨平台.md "wikilink") | genre =
[直譯器](../Page/直譯器.md "wikilink")，[媒體播放器](../Page/媒體播放器.md "wikilink")
| license = [GNU通用公共许可证](../Page/GNU通用公共许可证.md "wikilink") | website =
<https://web.archive.org/web/20070420072316/http://www.gnashdev.org/> }}

**Gnash**
計畫致力於發展一個播放Flash的[免費](../Page/免費.md "wikilink")[媒體播放器或](../Page/媒體播放器.md "wikilink")[外掛程式](../Page/外掛程式.md "wikilink")，來取代現有的[Adobe
Flash Player](../Page/Adobe_Flash_Player.md "wikilink")。

## 歷史

Gnash計畫是[GNU計畫中的](../Page/GNU.md "wikilink")[高優先自由軟體計劃](../Page/高優先自由軟體計劃.md "wikilink")（High
Priority Free Software Projects），
在Gnash計畫之前，GNU曾經要求社群幫助[GPLFlash計畫](../Page/GPLFlash.md "wikilink")，而現在GPLFlash計畫的主要開發人員都已經移往Gnash計畫，已經存在的GPLFlash計畫未來將會專注於[嵌入式系統](../Page/嵌入式系統.md "wikilink")。

Gnash主要的發行版是基於[GNU
GPL的規範](../Page/GNU_GPL.md "wikilink")，然而因為Gnash是使用[GameSWF計畫的核心程式](../Page/GameSWF.md "wikilink")，Gnash開發的程式能使用在[GameSWF上](../Page/GameSWF.md "wikilink")。

## 技術細節

[Adobe提供了](../Page/Adobe.md "wikilink")[Linux在](../Page/Linux.md "wikilink")[x86上的官方的播放器](../Page/x86.md "wikilink")，以binary形式發佈，但是並沒有支援其他的[處理器架構](../Page/處理器.md "wikilink")，然而Gnash可以被[編譯以及](../Page/編譯.md "wikilink")[執行在許多架構上](../Page/執行.md "wikilink")，包含x86、[AMD64](../Page/AMD64.md "wikilink")、[MIPS/Irix](../Page/MIPS/Irix.md "wikilink")、[PowerPC](../Page/PowerPC.md "wikilink")，也支援了[BSD-base的](../Page/BSD-base.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，早期是移植到並沒有被Adobe支援的[RISC
OS平台](../Page/RISC_OS.md "wikilink")，以及早期移植到[BeOS上](../Page/BeOS.md "wikilink")。

Flash實際上由兩種不同的檔案型態組成，SWF通常被人誤認為就是Flash。

  - [Primary Gnash
    website](https://web.archive.org/web/20070420072316/http://www.gnashdev.org/)
  - [Gnash at GNU Project](http://www.gnu.org/software/gnash/)
  - [Gnash's Savannah Page](http://savannah.gnu.org/projects/gnash/)
  - [FSF/GNU Press Release: FSF announces GNU Gnash - Flash Movie
    Player](http://lwn.net/Articles/166992/)
  - [An interview with Gnash project leader about the future of the
    product](https://web.archive.org/web/20080115182416/http://blogs.zdnet.com/Stewart/index.php?p=177)

[Category:GNU](../Category/GNU.md "wikilink")
[Category:GNU計劃軟體](../Category/GNU計劃軟體.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")