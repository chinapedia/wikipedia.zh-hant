**阿尔坎塔拉**（）[西班牙西部小城](../Page/西班牙.md "wikilink")，位于[卡塞雷斯省境内](../Page/卡塞雷斯省.md "wikilink")[塔霍河南岸的](../Page/塔霍河.md "wikilink")[丘陵上](../Page/丘陵.md "wikilink")，距[卡塞雷斯](../Page/卡塞雷斯_\(西班牙\).md "wikilink")63千米。阿爾坎塔拉之名源於[阿拉伯语](../Page/阿拉伯语.md "wikilink")（），意为“[桥](../Page/桥.md "wikilink")”
。该城為[阿尔坎塔拉区首府](../Page/阿尔坎塔拉区.md "wikilink")。

## 人口

2006年人口统计数据为1694人。

## 历史

阿尔坎塔拉城的历史可追溯到[古罗马时期](../Page/古罗马.md "wikilink")。[中世纪时](../Page/中世纪.md "wikilink")，该城市被[阿拉伯人所征服](../Page/阿拉伯人.md "wikilink")。1167年，[莱昂国王](../Page/莱昂.md "wikilink")[斐迪南二世第一次从](../Page/斐迪南二世_\(莱昂\).md "wikilink")[穆斯林手中夺回阿尔坎塔拉](../Page/穆斯林.md "wikilink")。1176年在这里成立了西班牙最重要的[宗教骑士团](../Page/宗教骑士团.md "wikilink")[阿尔坎塔拉骑士团](../Page/阿尔坎塔拉骑士团.md "wikilink")。此后又经过多次争夺，在1221年，莱昂国王[阿方索十世最終收复阿尔坎塔拉](../Page/阿方索十世.md "wikilink")。

## 景观

在阿尔坎塔拉有一座著名的罗马时代的六孔石桥，该桥修建于104年[图拉真皇帝统治时期](../Page/图拉真.md "wikilink")。在该桥上游的塔霍河上有西班牙著名的水利工程[阿尔坎塔拉水库](../Page/阿尔坎塔拉水库.md "wikilink")。

## 外部連結

  - [官方网站](http://www.alcantara.es)
  - [Portal sobre Alcántara-historia, monumentos, alojamientos,
    etc](http://www.alcantaraweb.com)
  - [阿尔坎塔拉桥](https://web.archive.org/web/20020810232728/http://www.geocities.com/jjescudero/)

[A](../Category/卡塞雷斯省市镇.md "wikilink")