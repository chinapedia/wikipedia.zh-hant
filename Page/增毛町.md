**增毛町**（）是[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[留萌振興局南部的](../Page/留萌振興局.md "wikilink")[町](../Page/町.md "wikilink")，西鄰[日本海](../Page/日本海.md "wikilink")，東側位山區，轄區內有[暑寒別天賣燒尻國定公園的](../Page/暑寒別天賣燒尻國定公園.md "wikilink")[暑寒別岳](../Page/暑寒別岳.md "wikilink")。[牡丹蝦的捕獲量是全日本第一](../Page/牡丹蝦.md "wikilink")，此外也有大量的[紅蝦](../Page/紅蝦.md "wikilink")。又由於具有良好水質，轄區內有日本最北端的大型[酒造](../Page/酒造.md "wikilink")「本間造酒」，其代表品牌為「國稀」。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「maskin-i」或「mas-ke」，意思為「很多的地方」或「有海鷗的地方」。\[1\]

## 歷史

過去曾為增毛支廳（現在的留萌振興局）的支廳辦公室所在地，但現已遷移於[留萌市](../Page/留萌市.md "wikilink")。

  - 1751年：來自松前的商人[村山傳兵衛在此設置據點](../Page/村山傳兵衛.md "wikilink")。\[2\]
  - [江戶時代後期](../Page/江戶時代.md "wikilink")：[俄羅斯勢力開始進入](../Page/俄羅斯.md "wikilink")，因此[秋田藩在此設置北方警備據點](../Page/秋田藩.md "wikilink")。
  - 1897年11月：北海道設置增毛支廳，支廳辦公室並設於此。
  - 1900年7月1日：增毛郡轄下增毛街道、增毛村、別苅村、岩尾村、暑寒澤村、阿分村、舍熊村合併為增毛町，並成為一級町。\[3\]
  - 1914年：增毛支廳辦公室遷移至留萌町（現在的留萌市），並改名為留萌支廳。

## 產業

過去曾經有繁榮的漁業，盛產[鯡魚](../Page/鯡魚.md "wikilink")，並帶動週邊產業，但由於濫捕導致產量銳減，戰後成為近海漁業和水產加工的中心。

## 交通

### 機場

  - [旭川機場](../Page/旭川機場.md "wikilink")（位於[旭川市](../Page/旭川市.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [留萌本線](../Page/留萌本線.md "wikilink")：[阿分車站](../Page/阿分車站.md "wikilink")
        - [信砂車站](../Page/信砂車站.md "wikilink") -
        [舍熊車站](../Page/舍熊車站.md "wikilink") -
        [朱文別車站](../Page/朱文別車站.md "wikilink") -
        [箸別車站](../Page/箸別車站.md "wikilink") -
        [增毛車站](../Page/增毛車站.md "wikilink")

### 道路

<table style="width:70%;">
<colgroup>
<col style="width: 35%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道231號</li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要道道</a></dt>

</dl>
<ul>
<li>北海道道94號增毛稻田線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道301號增毛港線</li>
<li>北海道道546號暑寒別公園線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

[Old_Homma_family_residence.JPG](https://zh.wikipedia.org/wiki/File:Old_Homma_family_residence.JPG "fig:Old_Homma_family_residence.JPG")
[Shokan_Kaigan.jpg](https://zh.wikipedia.org/wiki/File:Shokan_Kaigan.jpg "fig:Shokan_Kaigan.jpg")
[Mashike_Itsukushima_shrine.JPG](https://zh.wikipedia.org/wiki/File:Mashike_Itsukushima_shrine.JPG "fig:Mashike_Itsukushima_shrine.JPG")
[Mashike_lighthouse.JPG](https://zh.wikipedia.org/wiki/File:Mashike_lighthouse.JPG "fig:Mashike_lighthouse.JPG")\]\]

### 觀光

  - [雄冬海岸](../Page/雄冬海岸.md "wikilink")
  - [暑寒別岳](../Page/暑寒別岳.md "wikilink")
  - 鄉土義能 雄冬神樂
  - [暑寒別天賣燒尻國定公園](../Page/暑寒別天賣燒尻國定公園.md "wikilink")
  - 綜合交流促進施設「元陣屋」
  - 歴史建築物群（北海道遺產）
  - [辯天神社](../Page/辯天神社.md "wikilink")
  - [岩尾溫泉](../Page/岩尾溫泉.md "wikilink")

### [文化遺產](../Page/文化遺產.md "wikilink")

  - 國定文化遺產

<!-- end list -->

  - 舊本間家住宅（舊商家丸一本間家）

<!-- end list -->

  - 町定文化遺產

<!-- end list -->

  - 增毛嚴島神社本殿

## 教育

### 高等學校

  - [道立北海道增毛高等學校](https://web.archive.org/web/20071001002329/http://www.mashike.hokkaido-c.ed.jp/)

### 中學校

<table style="width:60%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>增毛町立增毛中學校</li>
</ul></td>
<td><ul>
<li>增毛町立增毛第二中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:60%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>增毛町立增毛小學校</li>
<li>增毛町立別苅小學校</li>
</ul></td>
<td><ul>
<li>增毛町立舍熊小學校</li>
<li>增毛町立阿分小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 本地出身的名人

  - [三國清三](../Page/三國清三.md "wikilink")：法國料理廚師
  - [桂本和夫](../Page/桂本和夫.md "wikilink")：前[國鐵燕子職業棒球選手](../Page/東京養樂多燕子.md "wikilink")、[墨爾本奧林匹克運動會](../Page/1956年夏季奧林匹克運動會.md "wikilink")[摔跤項目第五名](../Page/摔跤.md "wikilink")。
  - [池田三男](../Page/池田三男.md "wikilink")：墨爾本奧林匹克運動會摔跤項目金牌。
  - [琴若](../Page/琴若.md "wikilink")：[相撲力士](../Page/相撲力士.md "wikilink")、前小結。
  - [遠田誠治](../Page/遠田誠治.md "wikilink")：前[中日龍職業棒球選手](../Page/中日龍.md "wikilink")。
  - [本間一夫](../Page/本間一夫.md "wikilink")：[日本點字圖書館創設者](../Page/日本點字圖書館.md "wikilink")。
  - [本間貞雄](../Page/本間貞雄.md "wikilink")：前[札幌信用金庫理事長](../Page/札幌信用金庫.md "wikilink")。
  - [本間歐彦](../Page/本間歐彦.md "wikilink")：本間貞雄之孫、[富士電視台製作人](../Page/富士電視台.md "wikilink")。
  - [石崎喜太郎](../Page/石崎喜太郎.md "wikilink")：前北海道漁連會長、現任町長[石崎大輔之父](../Page/石崎大輔.md "wikilink")。

## 參考資料

## 外部連結

  - [增毛町沿革誌](https://web.archive.org/web/20050710041620/http://ambitious.lib.hokudai.ac.jp/hoppodb/kyuki/doc/0A013840000000.html)

  - [增毛町綜合交流促進施設　原陣屋](https://web.archive.org/web/20070513214757/http://www.pref.hokkaido.jp/kseikatu/ks-bsbsk/bunkashigen/parts/1102.html)（北海道文化資源資料庫）

  - [增毛町商工會](https://web.archive.org/web/20070808012747/http://www11.ocn.ne.jp/~mashikes/index-s.html)

  - [增毛商工會部落格](http://mashike-shoukoukai.blog.ocn.ne.jp/blog/)

  - [增毛町果樹協會青年部](http://blog.livedoor.jp/fruitboys/)

<!-- end list -->

1.

2.
3.