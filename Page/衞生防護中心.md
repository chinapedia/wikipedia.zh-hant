[Chp_HQ.JPG](https://zh.wikipedia.org/wiki/File:Chp_HQ.JPG "fig:Chp_HQ.JPG")
[Public_Health_Laboratory_Centre.JPG](https://zh.wikipedia.org/wiki/File:Public_Health_Laboratory_Centre.JPG "fig:Public_Health_Laboratory_Centre.JPG")

**衞生防護中心**（[英文](../Page/英文.md "wikilink")：**C**entre for **H**ealth
**P**rotection，[縮寫](../Page/縮寫.md "wikilink")：**CHP**）是[香港特別行政區](../Page/香港特別行政區.md "wikilink")[衞生署屬下的流行病防護中心](../Page/衞生署_\(香港\).md "wikilink")，於2004年6月1日正式成立，負責加強[香港市民對香港預防及控制各種流行疾病](../Page/香港市民.md "wikilink")，包括傳染病的認識，並會調查各種流行疾病感染個案，監察疾病通報情況，以及與其他國家交換有關疾病感染個案情況。現任衞生防護中心總監為[黃加慶醫生](../Page/黃加慶.md "wikilink")，顧問醫生為[張竹君醫生](../Page/張竹君.md "wikilink")。

## 標誌

衞生防護中心的標誌以[深藍色](../Page/深藍色.md "wikilink")[英文](../Page/英文.md "wikilink")[縮寫](../Page/縮寫.md "wikilink")「CHP」組合而成，「HP」兩個[羅馬字母被](../Page/羅馬字母.md "wikilink")「C」字母所包圍，意喻健康防護任務放在最中心的位置。採用深藍色意喻「簡潔清晰、專業」，前總監[梁栢賢就任時亦有份參與構思創造標誌](../Page/梁栢賢.md "wikilink")\[1\]。

## 總部

衞生防護中心辦公室選址位於前九龍醫院護士宿舍，毗連[醫院管理局總部](../Page/醫院管理局.md "wikilink")[醫院管理局大樓](../Page/醫院管理局大樓.md "wikilink")，理由為直接及方便即時溝通。

## 歷史

2003年4月1日，[香港爆發](../Page/香港.md "wikilink")[嚴重急性呼吸系統綜合症後](../Page/嚴重急性呼吸系統綜合症.md "wikilink")，時任[香港行政長官](../Page/香港行政長官.md "wikilink")[董建華隨即著手研究在香港設立一個疾病控制及預防中心組織](../Page/董建華.md "wikilink")，以對抗、治療及預防任何[傳染病在](../Page/傳染病.md "wikilink")[香港社會爆發之可能性](../Page/香港社會.md "wikilink")。香港政府專家報告建議成立類似美國的疾病控制中心，以統一防護及控制傳染病。2003年10月，[梁栢賢臨危受命籌備組織衞生防護中心](../Page/梁栢賢.md "wikilink")，初期所面對最大挑戰為與[醫院管理局建立呈報機制](../Page/醫院管理局.md "wikilink")。翌年6月1日，[香港賽馬會撥款](../Page/香港賽馬會.md "wikilink")5億港元，正式成立衞生防護中心\[2\]。

## 歷任總監

  - [梁栢賢醫生](../Page/梁栢賢.md "wikilink")（2004年6月1日－2007年2月28日）
  - [曾浩輝醫生](../Page/曾浩輝.md "wikilink")（2007年3月1日－2012年12月28日）
  - [梁挺雄醫生](../Page/梁挺雄.md "wikilink")（2012年12月29日－2016年11月7日）
  - [黃加慶醫生](../Page/黃加慶.md "wikilink")（2016年11月8日－）

## 架構

1.  監測及流行病學處
2.  感染控制處
3.  項目管理及專業發展處
4.  公共衞生化驗服務處
5.  公共衞生服務處
6.  緊急應變及資訊處

## 總部

衞生防護中心的總部設在[九龍](../Page/九龍.md "wikilink")[亞皆老街](../Page/亞皆老街.md "wikilink")147C號衞生防護中心大樓，由前[九龍醫院護士宿舍改裝而成](../Page/九龍醫院.md "wikilink")。而屬下的公共衞生化驗服務處及公共衞生服務處則設在九龍石硤尾南昌街382號公共衞生檢測中心。

## 參見

  - [衞生署](../Page/衞生署_\(香港\).md "wikilink")
  - 衞生防護中心出版物：
      - [人類豬型流感及季節性流感直擊](../Page/人類豬型流感及季節性流感直擊.md "wikilink")
      - [手足口病及腸病毒71型每日概況](../Page/手足口病及腸病毒71型每日概況.md "wikilink")

## 參考注釋

## 外部連結

  - [香港特別行政區 衞生防護中心](http://www.chp.gov.hk)

[Category:九龍城](../Category/九龍城.md "wikilink")
[Category:衞生防護中心](../Category/衞生防護中心.md "wikilink")

1.  [「CHP」標誌
    設計有段古](http://orientaldaily.on.cc/cnt/news/20130513/00176_029.html)
    《東方日報》 2013年5月13日
2.  [沙士催生衞生防護中心](http://orientaldaily.on.cc/cnt/news/20130513/00176_028.html)
    《東方日報》 2013年5月13日