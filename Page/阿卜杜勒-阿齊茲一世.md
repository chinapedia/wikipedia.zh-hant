**阿卜杜勒-阿齊茲一世**（1830年2月9日—1876年6月4日），1861年—1876年擔任[奧斯曼帝國](../Page/奧斯曼帝國.md "wikilink")[蘇丹](../Page/蘇丹.md "wikilink")。在位前十年，他頗有政績，在內政方面，設立大學，制定法典。外交方面則保持在[英](../Page/英.md "wikilink")[法](../Page/法.md "wikilink")[俄國等距](../Page/俄國.md "wikilink")。不過，因為國內糧食與反對力量聚集，他於1872年起，漸次開始採取較高壓統治。1876年，遭民眾反抗罷黜，不久即鬱悶去世。

## 外交

阿卜杜勒-阿齊茲一世支持[新疆脫離](../Page/新疆.md "wikilink")[清朝統治](../Page/清朝.md "wikilink")。[阿古柏外甥阿吉托拉](../Page/阿古柏.md "wikilink")（亦名[赛义德](../Page/赛义德.md "wikilink")·阿古柏）、喀孜汗土烈在1873年到[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")，表示愿意附属，赠送绸缎、[蒙古马](../Page/蒙古马.md "wikilink")、[哈萨克的好马各九匹](../Page/哈萨克.md "wikilink")，还有各处抢来的童男、童女各九名。阿卜杜勒-阿齐兹一世隆重地接待了阿古柏派去的代表团，并赏给阿古柏“米拉胡尔巴什”（即[埃米尔](../Page/埃米尔.md "wikilink")，国王的意思）的职衔。土耳其给阿古柏派遣了[上校司马衣艾柯克](../Page/上校.md "wikilink")、艾里卡孜木和木拉提伯克等二十多个高级军事人员和政治顾问，并带来了许多武器和礼品。顾问中有七人留在阿古柏身边常任参谋职务。1874年，阿卜杜勒-阿齐兹一世写信命令阿古柏在新疆发行的货币上用自己的名字，并且使用奥斯曼帝国的国旗——红旗白底红星月作为国旗。阿古柏在对土耳其苏丹所使用的国名就是“[东突厥斯坦国](../Page/东突厥斯坦.md "wikilink")”。阿卜杜勒-阿齐兹一世答应阿古柏，在他之后，王位由自己的儿子世袭。特颁[古兰经一卷](../Page/古兰经.md "wikilink")、[奥斯曼旗帜一面及其他赠品多种](../Page/奥斯曼旗帜.md "wikilink")。\[1\]1875年阿古柏从土耳其一次购得新式步枪1.2万支，火炮8门。\[2\]

## 参考文献

[Category:奧斯曼帝國蘇丹](../Category/奧斯曼帝國蘇丹.md "wikilink")
[Category:1830年出生](../Category/1830年出生.md "wikilink")
[Category:1876年逝世](../Category/1876年逝世.md "wikilink")

1.  穆汗买德•阿提夫：《喀什历史》第三八八——三八九页
2.