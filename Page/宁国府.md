**宁国府**，[南宋时设置的](../Page/南宋.md "wikilink")[府](../Page/府_\(行政区划\).md "wikilink")。在今[安徽省境](../Page/安徽省.md "wikilink")。

南宋[乾道二年](../Page/乾道_\(南宋\).md "wikilink")（1166年）升[宣州置](../Page/宣州_\(隋朝\).md "wikilink")，[治所在](../Page/治所.md "wikilink")[宣城县](../Page/宣城县_\(隋朝\).md "wikilink")（今安徽省[宣城市境](../Page/宣城市.md "wikilink")）。属[江南路](../Page/江南路.md "wikilink")。辖境相当今安徽省[宣城](../Page/宣城市.md "wikilink")、[宁国](../Page/宁国县.md "wikilink")、[旌德](../Page/旌德县.md "wikilink")、[泾县](../Page/泾县.md "wikilink")、[南陵](../Page/南陵县.md "wikilink")、[黄山区等市县地](../Page/黄山区.md "wikilink")。[元朝](../Page/元朝.md "wikilink")[至元十四年](../Page/至元_\(忽必烈\).md "wikilink")（1277年），改为[宁国路](../Page/宁国路.md "wikilink")。[至正十七年](../Page/至正.md "wikilink")（1357年）四月，[朱元璋政权复改为宁国府](../Page/朱元璋.md "wikilink")，后又改[宣城府](../Page/宣城府.md "wikilink")、[宣州府](../Page/宣州府.md "wikilink")，至吴王元年（1367年）四月，仍称宁国府。下领六县：宣城县（首县）、[南陵县](../Page/南陵县.md "wikilink")、[泾县](../Page/泾县.md "wikilink")、[宁国县](../Page/宁国县.md "wikilink")、[旌德县](../Page/旌德县.md "wikilink")、[太平县](../Page/太平县_\(天宝\).md "wikilink")\[1\]，属[南直隶](../Page/南直隶.md "wikilink")。

清朝初，属[江南左布政使司](../Page/江南左布政使司.md "wikilink")。[康熙时](../Page/康熙.md "wikilink")，属安徽省。所辖六县不变，宁国府与該省的[徽州府](../Page/徽州府.md "wikilink")、[太平府](../Page/太平府_\(安徽省\).md "wikilink")、[池州府](../Page/池州府.md "wikilink")、[广德直隶州以及江苏省](../Page/广德直隶州.md "wikilink")[江宁府](../Page/江宁府.md "wikilink")、浙江省[杭州府](../Page/杭州府.md "wikilink")、[湖州府相邻](../Page/湖州府.md "wikilink")。考评[繁，难](../Page/衝繁疲難.md "wikilink")\[2\]，[宣統退位](../Page/宣統退位.md "wikilink")，[民國成立之後](../Page/民國.md "wikilink")，隨著全国[罷府而废](../Page/废府州厅改县.md "wikilink")。

## 注释

[Category:南宋的府](../Category/南宋的府.md "wikilink")
[Category:明朝的府](../Category/明朝的府.md "wikilink")
[Category:清朝的府](../Category/清朝的府.md "wikilink")
[Category:安徽的府](../Category/安徽的府.md "wikilink")
[Category:宣城行政区划史](../Category/宣城行政区划史.md "wikilink")
[Category:黄山行政区划史](../Category/黄山行政区划史.md "wikilink")
[Category:芜湖行政区划史](../Category/芜湖行政区划史.md "wikilink")
[Category:1160年代建立的行政区划](../Category/1160年代建立的行政区划.md "wikilink")
[Category:1913年废除的行政区划](../Category/1913年废除的行政区划.md "wikilink")

1.  《[明史](../Page/明史.md "wikilink")·卷四十·志第十六◎地理一》池州府......宁国府，元宁国路，属江浙行省。太祖丁酉年四月曰宁国府。辛丑年四月曰宣城府。丙午年正月曰宣州府。吴元年四月仍曰宁国府。领县六。北距南京三百十里。洪武二十六年编户九万九千七百三十二，口五十三万二千二百五十九。弘治四年，户六万三百六十四，口三十七万一千五百四十三。万历六年，户五万二千一百四十八，口三十八万七千一十九。宣城，倚......
2.  《[清史稿](../Page/清史稿.md "wikilink")·卷五十九·志三十四◎地理六》徽州府......宁国府：繁，难。隶徽宁池太广道。明，宁国府，属江南。顺治初因之，属江南左布政使司。康熙六年，分隶安徽省。西北距省治四百三十里。广二百二十里，袤三百三十五里。北极高二十度二分。京师偏东二度十六分。领县六。宣城繁，疲，难。倚......