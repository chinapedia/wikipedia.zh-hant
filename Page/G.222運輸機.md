Alenia的**G.222**為一種中型[短場起降軍用](../Page/短場起降.md "wikilink")[運輸機](../Page/運輸機.md "wikilink")。原本是為滿足[北約的要求而研發](../Page/北約.md "wikilink")，但最初只有[義大利一個北約成員國採用此機](../Page/義大利.md "wikilink")。但是該機架構在1990年代與[美國廠商合作改良](../Page/美國.md "wikilink")，成為[C-27J斯巴達人運輸機](../Page/C-27J斯巴達人運輸機.md "wikilink")，並得到國際市場的青睞。

G.222設計採用了運輸機的常見構型：高單翼，雙[渦輪旋槳發動機](../Page/渦輪發動機.md "wikilink")，尾部跳板。其裝貨甲板的尺寸是為標準的[463L](../Page/463L.md "wikilink")
貨盤而規劃；機艙底板設有一個供空投使用門；設有供醫療作業用的內置的輸氧系統；並在側門有為傘兵跳傘而設平台。

## 過往及當前用戶

[缩略图](https://zh.wikipedia.org/wiki/File:DF-ST-98-01305.JPEG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:USAF_24th_Wing_C-27A_taxies_with_flags.JPEG "fig:缩略图")

### 各種G.222機型系列與C-27A機型

  - [阿根廷軍隊](../Page/阿根廷軍隊.md "wikilink") (*Comando Aviación Ejército
    Argentino*)[1](https://web.archive.org/web/20070616213350/http://www.avejer.ejercito.mil.ar/GalFot/G-222.jpg)

  - [阿聯酋](../Page/阿聯酋.md "wikilink")

  - [義大利](../Page/義大利.md "wikilink")

  - [利比亞](../Page/利比亞.md "wikilink")

  - [奈及利亞](../Page/奈及利亞.md "wikilink")

  - [索馬里軍隊](../Page/索馬里軍隊.md "wikilink")

  - [泰國](../Page/泰國.md "wikilink")

  - [突尼西亞](../Page/突尼西亞.md "wikilink")

  - [委內瑞拉](../Page/委內瑞拉.md "wikilink")

  - [美國](../Page/美國.md "wikilink")

  -
### C-27J

  - [希臘](../Page/希臘.md "wikilink")

  - [義大利](../Page/義大利.md "wikilink")

  - [保加利亞](../Page/保加利亞.md "wikilink")-5台，將於2011年前陸續運抵

  - [立陶宛](../Page/立陶宛.md "wikilink")-3

[羅馬尼亞購買了七台](../Page/羅馬尼亞.md "wikilink")，打算於2008年2月取代[An-24及](../Page/An-24.md "wikilink")[An-26運輸機](../Page/An-26.md "wikilink")，不過這宗購買在2007年2月取消\<ref
"awst_20061211"\>"Spartan Order." *Aviation Week & Space Technology*
12月11日 2006年</ref> \[1\]

## 性能諸元(G.222)

## 參考資料

## 外部連結

  - [Alenia Canadian website advocating C-27J](http://www.c-27j.ca)
  - [GMAS website promoting C-27J for the U.S. Army and Air Force JCA
    Program](https://web.archive.org/web/20070617224327/http://www.c-27j.com/)
  - [CdnMilitary.ca Article on the C-27J
    Spartan](https://archive.is/20060409185305/http://209.67.210.99/articles/c27jSpartan.htm)

## 相關

[Category:義大利運輸機](../Category/義大利運輸機.md "wikilink")

1.  [2](http://www.hotnews.ro/articol_64689-Licitatia-prin-care-Armata-urma-sa-cumpere-sapte-avioane-Alenia-a-fost-anulata.htm)