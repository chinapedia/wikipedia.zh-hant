[Hastings_Kamuzu_Banda-Denkmal_Lilongwe.jpg](https://zh.wikipedia.org/wiki/File:Hastings_Kamuzu_Banda-Denkmal_Lilongwe.jpg "fig:Hastings_Kamuzu_Banda-Denkmal_Lilongwe.jpg")的海斯廷斯·卡穆祖·班达的铜像，建于2006年\]\]
**海斯廷斯·卡穆祖·班达**（****，），[马拉维政治人物](../Page/马拉维.md "wikilink")，非洲独立运动领导人之一，1966年至1994年间任[马拉维总统](../Page/马拉维总统.md "wikilink")，著名独裁者。

## 早年生活

班达出生于尼亚萨兰[卡松古的一个](../Page/卡松古.md "wikilink")[契瓦族小农家庭](../Page/契瓦族.md "wikilink")，他具体的出生日期已不可考，一般现在认为其出生于1896年或1902年。幼年时依靠当地[基督教教会的资助得到了受教育的机会](../Page/基督教.md "wikilink")。1914年，他独自一人长途跋涉，前往[南非](../Page/南非.md "wikilink")[约翰内斯堡求学](../Page/约翰内斯堡.md "wikilink")。1922年，他受洗为[美以美会教徒](../Page/美以美会.md "wikilink")，在其资助下于1925年前往[美国](../Page/美国.md "wikilink")[田纳西州留学](../Page/田纳西州.md "wikilink")，主攻[医学](../Page/医学.md "wikilink")，1927年获得医学学位。在美期间，他受到著名[美国黑人领袖](../Page/美国黑人.md "wikilink")[杜波依斯的影响](../Page/杜波依斯.md "wikilink")，决定投身于黑人解放运动当中。他的思想在这一时期还受到[富兰克林·罗斯福和](../Page/富兰克林·罗斯福.md "wikilink")[甘地的影响](../Page/甘地.md "wikilink")，这使得他之后主张强势政府和和平主义。

为取得在尼亚萨兰的行医资格，班达于1937年前往[苏格兰](../Page/苏格兰.md "wikilink")[爱丁堡大学攻读学位](../Page/爱丁堡大学.md "wikilink")，1939年，他同争取独立的契瓦族酋长[姆瓦塞会面](../Page/姆瓦塞.md "wikilink")。1941年他获得爱丁堡另一医学学位。1942年，因为尼亚萨兰当地的[白人医护人员拒绝和黑人医师合作](../Page/白人.md "wikilink")，班达只好在[利物浦开业行医](../Page/利物浦.md "wikilink")。不久，[英国政府征召班达入伍参加](../Page/英国.md "wikilink")[二战](../Page/二战.md "wikilink")，但是班达以自己是“和平主义者”为名拒绝，他在利物浦的行医资格因此被取消。

## 政治生涯

1944年，班达被争取独立的[尼亚萨兰非洲人国民大会任命为该组织驻英国代表](../Page/尼亚萨兰非洲人国民大会.md "wikilink")，正式步入政界。在这段时间内，班达同[恩克鲁玛](../Page/恩克鲁玛.md "wikilink")、[肯雅塔等著名](../Page/肯雅塔.md "wikilink")[非洲黑人领袖多有接触](../Page/非洲.md "wikilink")，并建立了良好关系。他还曾加入[英国工党和](../Page/英国工党.md "wikilink")[费边社](../Page/费边社.md "wikilink")。

1953年，英国政府决定将[北罗得西亚](../Page/赞比亚.md "wikilink")、[南罗得西亚和尼亚萨兰合组为](../Page/津巴布韦.md "wikilink")“中非联邦”，班达认为这是英国政府阻止尼亚萨兰等地独立的阴谋，激烈抗议，并离开英国前往[加纳](../Page/加纳.md "wikilink")。这一举动使其俨然成为尼亚萨兰独立运动的象征。1958年，班达应尼亚萨兰非洲人国民大会邀请，回到尼亚萨兰，受到了热烈的欢迎。他以尼亚萨兰非洲人国民大会党主席的身份周游全国，到处发表演说反对联邦制度。殖民政府认为非洲人的憎恨情绪和动乱日益强烈，部分是由于他的缘故，1959宣布紧急状态，他成为独立运动的最高领导人。1959年，班达被英国殖民当局逮捕入狱，尼亚萨兰非洲人国民大会也被迫解散。

1960年，班达被释放，旋即出任[马拉维国民大会党主席](../Page/马拉维国民大会党.md "wikilink")，继续开展抗议活动。数月后接受英国的立宪建议，在立法议会中赋予尼亚萨兰的非洲人以多数席位。1961年，马拉维国民大会党在尼亚萨兰选举中获胜，班达出任自然资源和地方行政部长。1962年，英国同意尼亚萨兰内部自治，班达出任自治政府首脑。1963年，“中非联邦”宣布解散，班达继续担任尼亚萨兰总理。

1964年7月6日，尼亚萨兰在[英联邦内取得独立](../Page/英联邦.md "wikilink")，改名为马拉维，班达仍任总理。1966年7月6日宣布成立共和国，班达任总统。独立后不久班达内阁的有些成员及因抗议他的独断专行和与南非及葡属殖民地的和解而辞职。1965年爆发了由两名前副部长领导的叛乱，但这起叛乱未能控制农村。1966年马拉维成为班达总统严厉的独裁统治下的一党专政共和国。他紧紧地控制着政府的各个方面，将反对者投入监狱或予以处决。班达在掌权后大权独揽，自身一度兼任外交、国防、财政、新闻等所有重要部门的部长，1971年更是宣布自己为“终身总统”，被外国媒体形容为“更像封建国王”。班达集中一切力量建设国家的基础设施和增加农业生产。他主张同实施[种族隔离的南非和罗得西亚保持良好关系](../Page/种族隔离.md "wikilink")，建立起来友好的贸易关系，并推行亲西方的外交政策，马拉维于1967年同南非建交，成为当时唯一一个同南非保持正式外交关系的南部非洲独立国家。为此，南非给与马拉维大量物资援助，并直接援建了其新都[利隆圭](../Page/利隆圭.md "wikilink")。

1980年津巴布韦和[莫桑比克相继独立之后](../Page/莫桑比克.md "wikilink")，班达发觉马拉维已经被敌视南非的诸黑人国家包围，于是又逐渐与南非拉开距离，设法与周边邻国建立良好关系，为此他改善了与津巴布韦总统[穆加贝](../Page/穆加贝.md "wikilink")、[坦桑尼亚总统](../Page/坦桑尼亚.md "wikilink")[尼雷尔和莫桑比克总统](../Page/尼雷尔.md "wikilink")[萨莫拉的关系](../Page/萨莫拉.md "wikilink")。同时，他与美国和英国仍然保持密切联系。

班达在位期间，大力推进马拉维的农业发展，自称为“马拉维第一号农民”，在其任下，马拉维的公共水利设施得到了很大程度的改善。

班达对国内严格控制，实行国民大会党的一党制。他曾经因为有外国记者报道马拉维选举不自由，而于1987年下令驱逐所有外国记者。

班达聚敛了巨大数目的财富，其中包括数个大型[烟草农场](../Page/烟草.md "wikilink")，但他表示自己拥有这些农场是为了“鼓励马拉维农民努力生产”。他喜欢让少女为其歌舞，但是却阻止其国民穿着短裙或[嬉皮装束](../Page/嬉皮.md "wikilink")。他十分羡慕英国的绅士风度，专门模仿[伊顿公学建设了一座学校](../Page/伊顿公学.md "wikilink")，聘请英国教员教授学生[拉丁文和](../Page/拉丁文.md "wikilink")[希腊文](../Page/希腊文.md "wikilink")，并禁止他们使用本部族的语言。

## 对外关系

班达是个[反共主义者](../Page/反共主义.md "wikilink")，上任之初曾颁布法令，严禁马拉维国民同共产主义国家往来，违者重罚重判。1968年[越南战争期间](../Page/越南战争.md "wikilink")，他曾声称“马拉维政府百分之150支持[越南共和国清剿越共](../Page/越南共和国.md "wikilink")。”但1982年，班达放松了其反共外交政策，表示愿同非苏联阵营的欧亚共产主义国家来往；同年，马拉维宣布同[北朝鲜建交](../Page/北朝鲜.md "wikilink")，班达此后甚至出人意料地同[金日成建立了极为良好的私交](../Page/金日成.md "wikilink")。1985年马拉维又与[罗马尼亚](../Page/罗马尼亚.md "wikilink")、[阿尔巴尼亚签署建交公报](../Page/阿尔巴尼亚.md "wikilink")。

冷战时期，出于共同的反共立场，班达治下的马拉维与[中华民国关系极为友好](../Page/中华民国.md "wikilink")、密切，甚至於1967年訪問[台灣](../Page/台灣.md "wikilink")，與[蔣中正互贈勳章](../Page/蔣中正.md "wikilink")。直到1992年，班达才在中间人[坦桑尼亚的撮合下](../Page/坦桑尼亚.md "wikilink")，与[中华人民共和国进行了初步接触](../Page/中华人民共和国.md "wikilink")，马拉维通过向中国大陆出口高级[烟草和](../Page/烟草.md "wikilink")[稀土](../Page/稀土.md "wikilink")，换取中国大陆的日用品，以及引进中国大陆建筑公司参与马拉维政府部门大楼翻修工程；但班达在卸任总统前，两度拒绝了中华人民共和国的建交要求，直至2007年，即班达逝世10年之后，马拉维才断绝了同中华民国的交往，改与中华人民共和国建交。

## 下台后

1994年，班达在大选中被击败，之后流亡[南非](../Page/南非.md "wikilink")。

1997年去世，現葬於[里朗威的班達陵寢](../Page/里朗威.md "wikilink")。

## 参考

## 外部链接

  - [Information on Banda's
    fortune](https://web.archive.org/web/20030508010815/http://www.giles.34sp.com/where.htm)
  - [Official Biography of Kamuzu
    Banda](https://web.archive.org/web/20050728113010/http://www.greatepicbooks.com/banda.html)
  - [Kamuzu Banda: The Control
    Freak](https://web.archive.org/web/20080921110635/http://www.politicalarticles.net/blog/2008/09/15/african-dictators-kamuzu-banda-the-control-freak/)

## 延伸阅读

  - Andrew C. Ross *Colonialism to cabinet crisis: a political history
    of Malawi*,African Books Collective, 2009 ISBN 99908-87-75-6 gives
    extensive biographical detail on Hastings Banda

[Category:马拉维总统](../Category/马拉维总统.md "wikilink")
[Category:馬拉威政治人物](../Category/馬拉威政治人物.md "wikilink")
[Category:馬拉威基督徒](../Category/馬拉威基督徒.md "wikilink")
[Category:非洲紙幣上的人物](../Category/非洲紙幣上的人物.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:愛丁堡大學校友](../Category/愛丁堡大學校友.md "wikilink")