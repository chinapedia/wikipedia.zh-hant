**WTA巡迴賽錦標賽**（*The WTA Tour
Championships*），又被称作**WTA年终总决赛**，是[国际女子网联在每年末定期举办的一项国际顶尖女子网球赛事](../Page/国际女子网联.md "wikilink")。其参赛者都是每年WTA巡回赛中成绩前列的选手。

首届WTA巡迴賽錦標賽在1972年[美国](../Page/美国.md "wikilink")[佛罗里达州的](../Page/佛罗里达州.md "wikilink")[博卡拉顿举行](../Page/博卡拉顿.md "wikilink")。在1972年到1986年期间，该项赛事都在每年三月举行。此后由于WTA决定将每年赛季改为1月至11月，因此总决赛也被改在每年的年末举行。因此，1986年的3月和11月举办了两次WTA总决赛。1979年至2000年期间，这项赛事一直都在美国[纽约的](../Page/纽约.md "wikilink")[麦迪逊广场花园举行](../Page/麦迪逊广场花园.md "wikilink")。此后，赛事的主办地点迁至[德国](../Page/德国.md "wikilink")，最近這項比賽以三年一輪於世界各地比賽，2014-2016在[新加坡](../Page/新加坡.md "wikilink")，2019-2029在[深圳](../Page/深圳.md "wikilink")。

1972年至1994年期间，根据赞助商的要求，这项赛事又被称作**维珍妮烟草杯冠军赛**（Virginia Slims
Championships），在1990年后期也有一段时间被称作**Chase冠军赛**。但是这项赛事最为人熟知的名称依然是WTA年终总决赛。

1984年至1998年期间，该项赛事的决赛一直采用5盤3胜制，这也是女子网球比赛中唯一采用的赛制。但在1999年，主办者将其改为普遍的3盤2胜制。

WTA年终总决赛通常被认为是继[四大网球公开赛之後水平最高的女子网球赛事](../Page/四大网球公开赛.md "wikilink")，在该项比赛中获得名次的选手也会获得仅次于大满贯赛事的積分(冠軍1500分)。因此这项赛事被视为每年网坛的压轴好戏。

## 历届成绩

### 单打

<table>
<thead>
<tr class="header">
<th><p><strong>年份</strong></p></th>
<th><p><strong>冠军</strong></p></th>
<th><p><strong>亚军</strong></p></th>
<th><p><strong>决赛比分</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1972年</p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p><a href="../Page/凱莉·瑞德.md" title="wikilink">凱莉·瑞德</a></p></td>
<td><p>7-5, 6-4</p></td>
</tr>
<tr class="even">
<td><p>1973年</p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p><a href="../Page/南希·瑞奇.md" title="wikilink">南希·瑞奇</a></p></td>
<td><p>6-3, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>1974年</p></td>
<td><p><a href="../Page/伊凡·古拉岡.md" title="wikilink">古拉貢</a></p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p>6-3, 6-4</p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p>6-4, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1976年</p></td>
<td><p><a href="../Page/伊凡·古拉岡.md" title="wikilink">古拉貢</a></p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p>6-3, 5-7, 6-3</p></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p><a href="../Page/蘇·巴克.md" title="wikilink">蘇·巴克</a></p></td>
<td><p>2-6, 6-1, 6-1</p></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/伊凡·古拉岡.md" title="wikilink">古拉貢</a></p></td>
<td><p>7-6, 6-4</p></td>
</tr>
<tr class="even">
<td><p>1979年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/崔西·奧斯丁.md" title="wikilink">崔西·奧斯丁</a></p></td>
<td><p>6-3, 3-6, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td><p><a href="../Page/崔西·奧斯丁.md" title="wikilink">崔西·奧斯丁</a></p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p>6-2, 2-6, 6-2</p></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/安德麗·耶格.md" title="wikilink">安德麗·耶格</a></p></td>
<td><p>6-3, 7-6</p></td>
</tr>
<tr class="odd">
<td><p>1982年</p></td>
<td><p><a href="../Page/西爾維婭·哈尼卡.md" title="wikilink">西爾維婭·哈尼卡</a></p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p>1-6, 6-3, 6-4</p></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p>6-2, 6-0</p></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/克里斯·埃弗特.md" title="wikilink">埃弗特</a></p></td>
<td><p>6-3, 7-5, 6-1</p></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/海蓮娜·蘇科娃.md" title="wikilink">海蓮娜·蘇科娃</a></p></td>
<td><p>6-3, 7-5, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>1986年（1）</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/哈娜·曼德利科娃.md" title="wikilink">曼德利科娃</a></p></td>
<td><p>6-2, 6-0, 3-6, 6-1</p></td>
</tr>
<tr class="even">
<td><p>1986年（2）</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/施特菲·格拉芙.md" title="wikilink">格拉芙</a></p></td>
<td><p>7-6, 6-3, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/施特菲·格拉芙.md" title="wikilink">格拉芙</a></p></td>
<td><p><a href="../Page/加布里埃拉·薩巴蒂尼.md" title="wikilink">薩巴蒂尼</a></p></td>
<td><p>4-6, 6-4, 6-0, 6-4</p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/加布里埃拉·薩巴蒂尼.md" title="wikilink">薩巴蒂尼</a></p></td>
<td><p><a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p>7-5, 6-2, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/施特菲·格拉芙.md" title="wikilink">格拉芙</a></p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p>6-4, 7-5, 2-6, 6-2</p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/莫妮卡·莎莉絲.md" title="wikilink">莎莉絲</a></p></td>
<td><p><a href="../Page/加布里埃拉·薩巴蒂尼.md" title="wikilink">薩巴蒂尼</a></p></td>
<td><p>6-4, 5-7, 3-6, 6-4, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/莫妮卡·莎莉絲.md" title="wikilink">莎莉絲</a></p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p>6-4, 3-6, 7-5, 6-0</p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/莫妮卡·莎莉絲.md" title="wikilink">莎莉絲</a></p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p>7-5, 6-3, 6-1</p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/施特菲·格拉芙.md" title="wikilink">格拉芙</a></p></td>
<td><p><a href="../Page/阿蘭查·桑切斯-維卡裡奧.md" title="wikilink">桑切斯</a></p></td>
<td><p>6-1, 6-4, 3-6, 6-1</p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/加布里埃拉·薩巴蒂尼.md" title="wikilink">薩巴蒂尼</a></p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a></p></td>
<td><p>6-3, 6-2, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/施特菲·格拉芙.md" title="wikilink">格拉芙</a></p></td>
<td><p><a href="../Page/安克·胡貝爾.md" title="wikilink">胡貝爾</a></p></td>
<td><p>6-1, 2-6, 6-1, 4-6, 6-3</p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/施特菲·格拉芙.md" title="wikilink">格拉芙</a></p></td>
<td><p><a href="../Page/瑪蒂娜·辛吉斯.md" title="wikilink">辛吉絲</a></p></td>
<td><p>6-3, 4-6, 6-0, 4-6, 6-0</p></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a></p></td>
<td><p><a href="../Page/玛莉·皮尔斯.md" title="wikilink">皮爾斯</a></p></td>
<td><p>7-6, 6-2, 6-3</p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/瑪蒂娜·辛吉斯.md" title="wikilink">辛吉絲</a></p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a></p></td>
<td><p>7-5, 6-4, 4-6, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a></p></td>
<td><p><a href="../Page/瑪蒂娜·辛吉斯.md" title="wikilink">辛吉絲</a></p></td>
<td><p>6-4, 6-2</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/瑪蒂娜·辛吉斯.md" title="wikilink">辛吉絲</a></p></td>
<td><p><a href="../Page/莫妮卡·莎莉絲.md" title="wikilink">莎莉絲</a></p></td>
<td><p>6-7, 6-4, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a></p></td>
<td><p>不戰而勝（達文波特因傷賽前退出）</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/金·克萊斯特爾斯.md" title="wikilink">克萊斯特爾斯</a></p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p>7-5, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/金·克萊斯特爾斯.md" title="wikilink">克萊斯特爾斯</a></p></td>
<td><p><a href="../Page/阿梅莉·毛瑞斯莫.md" title="wikilink">毛瑞斯莫</a></p></td>
<td><p>6-2, 6-0</p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/莎拉波娃.md" title="wikilink">莎拉波娃</a></p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p>4-6, 6-2, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/阿梅莉·毛瑞斯莫.md" title="wikilink">莫瑞斯莫</a></p></td>
<td><p><a href="../Page/玛莉·皮尔斯.md" title="wikilink">皮爾斯</a></p></td>
<td><p>5-7, 7-6, 6-4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年WTA巡回赛总决赛.md" title="wikilink">2006年</a></p></td>
<td><p><a href="../Page/賈斯汀·海寧.md" title="wikilink">海寧</a></p></td>
<td><p><a href="../Page/阿梅莉·毛瑞斯莫.md" title="wikilink">毛瑞斯莫</a></p></td>
<td><p>6-4, 6-3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年WTA巡回赛总决赛.md" title="wikilink">2007年</a></p></td>
<td><p><a href="../Page/賈斯汀·海寧.md" title="wikilink">海寧</a></p></td>
<td><p><a href="../Page/莎拉波娃.md" title="wikilink">莎拉波娃</a></p></td>
<td><p>5-7, 7-5, 6-3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年WTA巡回赛总决赛.md" title="wikilink">2008年</a></p></td>
<td><p><a href="../Page/大威廉絲.md" title="wikilink">大威廉絲</a></p></td>
<td><p><a href="../Page/薇拉·茲沃納列娃.md" title="wikilink">薇拉·茲沃納列娃</a></p></td>
<td><p>6<sup>5</sup>-7, 6-0 6-2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2009年WTA巡回赛总决赛.md" title="wikilink">2009年</a></p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p><a href="../Page/大威廉絲.md" title="wikilink">大威廉絲</a></p></td>
<td><p>6-2, 7-6<sup>4</sup></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年WTA巡回赛总决赛.md" title="wikilink">2010年</a></p></td>
<td><p><a href="../Page/金·克萊斯特爾斯.md" title="wikilink">克萊斯特爾斯</a></p></td>
<td><p><a href="../Page/卡露蓮·禾絲妮雅琪.md" title="wikilink">禾絲妮雅琪</a></p></td>
<td><p>6-3, 5-7, 6-3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年WTA巡回赛总决赛.md" title="wikilink">2011年</a></p></td>
<td><p><a href="../Page/佩特拉·科维托娃.md" title="wikilink">科薇妥娃</a></p></td>
<td><p><a href="../Page/维多利亚·阿扎伦卡.md" title="wikilink">阿薩伦卡</a></p></td>
<td><p>7-5, 4-6, 6-3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年WTA巡回赛总决赛.md" title="wikilink">2012年</a></p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p><a href="../Page/莎拉波娃.md" title="wikilink">莎拉波娃</a></p></td>
<td><p>6-4, 6-3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年WTA巡回赛总决赛.md" title="wikilink">2013年</a></p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p><a href="../Page/李娜.md" title="wikilink">李娜</a></p></td>
<td><p>2-6, 6-3, 6-0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年WTA巡回赛总决赛.md" title="wikilink">2014年</a></p></td>
<td><p><a href="../Page/小威廉絲.md" title="wikilink">小威廉絲</a></p></td>
<td><p><a href="../Page/西莫娜·哈勒普.md" title="wikilink">哈勒普</a></p></td>
<td><p>6-3, 6-0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年WTA巡回赛总决赛.md" title="wikilink">2015年</a></p></td>
<td><p><a href="../Page/阿格涅什卡·拉德萬斯卡.md" title="wikilink">拉德萬斯卡</a></p></td>
<td><p><a href="../Page/佩特拉·克維托娃.md" title="wikilink">佩特拉·克維托娃</a></p></td>
<td><p>6-2, 4-6, 6-3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年WTA巡回赛总决赛.md" title="wikilink">2016年</a></p></td>
<td><p><a href="../Page/多米尼卡·齐布尔科娃.md" title="wikilink">齊布科娃</a></p></td>
<td><p><a href="../Page/安赫利奎·柯貝.md" title="wikilink">安赫利奎·柯貝</a></p></td>
<td><p>6-3, 6-4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年WTA巡回赛总决赛.md" title="wikilink">2017年</a></p></td>
<td><p><a href="../Page/卡洛琳·瓦芝妮雅琪.md" title="wikilink">卡洛琳·瓦芝妮雅琪</a></p></td>
<td><p><a href="../Page/大威廉絲.md" title="wikilink">大威廉絲</a></p></td>
<td><p>6-4, 6-4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年WTA巡回赛总决赛.md" title="wikilink">2018年</a></p></td>
<td><p><a href="../Page/伊莉娜·斯維托莉娜.md" title="wikilink">伊莉娜·斯維托莉娜</a></p></td>
<td><p><a href="../Page/斯洛恩·史蒂芬斯.md" title="wikilink">斯洛恩·史蒂芬斯</a></p></td>
<td><p>3-6, 6-2, 6-2</p></td>
</tr>
</tbody>
</table>

### 双打冠军

<table>
<thead>
<tr class="header">
<th><p><strong>年份</strong></p></th>
<th><p><strong>冠军</strong></p></th>
<th><p><strong>亞軍</strong></p></th>
<th><p><strong>比數</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1972年</p></td>
<td><p><em>未举办</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1973年</p></td>
<td><p><a href="../Page/羅斯瑪麗·卡薩爾斯.md" title="wikilink">羅斯瑪麗·卡薩爾斯</a><br />
 <a href="../Page/瑪格麗特·考特.md" title="wikilink">瑪格麗特·考特</a></p></td>
<td><p><a href="../Page/弗朗索瓦絲·杜爾.md" title="wikilink">弗朗索瓦絲·杜爾</a><br />
 <a href="../Page/貝蒂·斯托福.md" title="wikilink">貝蒂·斯托福</a></p></td>
<td><p>6-2, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>1974年</p></td>
<td><p><a href="../Page/羅斯瑪麗·卡薩爾斯.md" title="wikilink">羅斯瑪麗·卡薩爾斯</a><br />
 <a href="../Page/比莉·珍·金.md" title="wikilink">比莉·珍·金</a></p></td>
<td><p><a href="../Page/弗朗索瓦絲·杜爾.md" title="wikilink">弗朗索瓦絲·杜爾</a><br />
 <a href="../Page/貝蒂·斯托福.md" title="wikilink">貝蒂·斯托福</a></p></td>
<td><p>6-1, 6-7(2), 7-5</p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p><a href="../Page/瑪格麗特·考特.md" title="wikilink">瑪格麗特·考特</a><br />
 <a href="../Page/維吉尼亞·韋德.md" title="wikilink">維吉尼亞·韋德</a></p></td>
<td><p><a href="../Page/羅斯瑪麗·卡薩爾斯.md" title="wikilink">羅斯瑪麗·卡薩爾斯</a><br />
 <a href="../Page/比莉·珍·金.md" title="wikilink">比莉·珍·金</a></p></td>
<td><p>6-7, 7-6, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1976年</p></td>
<td><p><em>未举办</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/貝蒂·斯托福.md" title="wikilink">貝蒂·斯托福</a></p></td>
<td><p><a href="../Page/弗朗索瓦絲·杜爾.md" title="wikilink">弗朗索瓦絲·杜爾</a><br />
 <a href="../Page/維吉尼亞·韋德.md" title="wikilink">維吉尼亞·韋德</a></p></td>
<td><p>7-5, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td><p><em>未举办</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1979年</p></td>
<td><p><a href="../Page/弗朗索瓦絲·杜爾.md" title="wikilink">弗朗索瓦絲·杜爾</a><br />
 <a href="../Page/貝蒂·斯托福.md" title="wikilink">貝蒂·斯托福</a></p></td>
<td><p><a href="../Page/蘇·巴克.md" title="wikilink">蘇·巴克</a><br />
 <a href="../Page/安·清村.md" title="wikilink">安·清村</a></p></td>
<td><p>7-6, 7-6</p></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td><p><a href="../Page/比莉·珍·金.md" title="wikilink">比莉·珍·金</a><br />
 <a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a></p></td>
<td><p><a href="../Page/羅斯瑪麗·卡薩爾斯.md" title="wikilink">羅斯瑪麗·卡薩爾斯</a><br />
 <a href="../Page/溫蒂·特恩布爾.md" title="wikilink">溫蒂·特恩布爾</a></p></td>
<td><p>6-3, 4-6, 6-3</p></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/芭芭拉·波特.md" title="wikilink">芭芭拉·波特</a><br />
 <a href="../Page/莎朗·華許.md" title="wikilink">莎朗·華許</a></p></td>
<td><p>6-0, 7-6(6)</p></td>
</tr>
<tr class="odd">
<td><p>1982年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/凱西·喬丹.md" title="wikilink">凱西·喬丹</a><br />
 <a href="../Page/安妮·史密斯.md" title="wikilink">安妮·史密斯</a></p></td>
<td><p>6-4, 6-3</p></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/克勞迪婭·科德·基爾施.md" title="wikilink">克勞迪婭·科德·基爾施</a><br />
 <a href="../Page/伊娃·普法夫.md" title="wikilink">伊娃·普法夫</a></p></td>
<td><p>7-5, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/裘·杜利.md" title="wikilink">裘·杜利</a><br />
 <a href="../Page/安·清村.md" title="wikilink">安·清村</a></p></td>
<td><p>6-3, 6-1</p></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/克勞迪婭·科德·基爾施.md" title="wikilink">克勞迪婭·科德·基爾施</a><br />
 <a href="../Page/海蓮娜·蘇科娃.md" title="wikilink">海蓮娜·蘇科娃</a></p></td>
<td><p>6-7(4), 6-4, 7-6(5)</p></td>
</tr>
<tr class="odd">
<td><p>1986年（1）</p></td>
<td><p><a href="../Page/哈娜·曼德利科娃.md" title="wikilink">哈娜·曼德利科娃</a><br />
 <a href="../Page/溫蒂·特恩布爾.md" title="wikilink">溫蒂·特恩布爾</a></p></td>
<td><p><a href="../Page/克勞迪婭·科德·基爾施.md" title="wikilink">克勞迪婭·科德·基爾施</a><br />
 <a href="../Page/海蓮娜·蘇科娃.md" title="wikilink">海蓮娜·蘇科娃</a></p></td>
<td><p>6-4, 6-7(4), 6-3</p></td>
</tr>
<tr class="even">
<td><p>1986年（2）</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/克勞迪婭·科德·基爾施.md" title="wikilink">克勞迪婭·科德·基爾施</a><br />
 <a href="../Page/海蓮娜·蘇科娃.md" title="wikilink">海蓮娜·蘇科娃</a></p></td>
<td><p>1-6, 6-1, 6-1</p></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/克勞迪婭·科德·基爾施.md" title="wikilink">克勞迪婭·科德·基爾施</a><br />
 <a href="../Page/海蓮娜·蘇科娃.md" title="wikilink">海蓮娜·蘇科娃</a></p></td>
<td><p>6-1, 6-1</p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/拉麗莎·沙辰科·尼蘭德.md" title="wikilink">拉麗莎·沙辰科·尼蘭德</a><br />
 <a href="../Page/娜塔莉亞·齊佛瑞娃.md" title="wikilink">娜塔莉亞·齊佛瑞娃</a></p></td>
<td><p>6-3, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/拉麗莎·沙辰科·尼蘭德.md" title="wikilink">拉麗莎·沙辰科·尼蘭德</a><br />
 <a href="../Page/娜塔莉亞·齊佛瑞娃.md" title="wikilink">娜塔莉亞·齊佛瑞娃</a></p></td>
<td><p>6-3, 6-2</p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/凱西·喬丹.md" title="wikilink">凱西·喬丹</a><br />
 <a href="../Page/伊莉莎白·斯麥利.md" title="wikilink">伊莉莎白·斯麥利</a></p></td>
<td><p><a href="../Page/梅塞德斯·帕茲.md" title="wikilink">梅塞德斯·帕茲</a><br />
 <a href="../Page/阿蘭查·桑切斯·維卡裡奧.md" title="wikilink">桑切斯</a></p></td>
<td><p>7-6(4), 6-4</p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/纳芙拉蒂洛娃.md" title="wikilink">纳芙拉蒂洛娃</a><br />
 <a href="../Page/潘·雪莉佛.md" title="wikilink">潘·雪莉佛</a></p></td>
<td><p><a href="../Page/琪琪·費爾南德斯.md" title="wikilink">琪琪·費爾南德斯</a><br />
 <a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a></p></td>
<td><p>4-6, 7-5, 6-4</p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/阿蘭查·桑切斯-維卡裡奧.md" title="wikilink">桑切斯</a><br />
 <a href="../Page/海蓮娜·蘇科娃.md" title="wikilink">海蓮娜·蘇科娃</a></p></td>
<td><p><a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a><br />
 <a href="../Page/拉麗莎·沙辰科·尼蘭德.md" title="wikilink">拉麗莎·沙辰科·尼蘭德</a></p></td>
<td><p>7-6(4), 6-1</p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/琪琪·費爾南德斯.md" title="wikilink">琪琪·費爾南德斯</a><br />
 <a href="../Page/娜塔莉亞·齊佛瑞娃.md" title="wikilink">娜塔莉亞·齊佛瑞娃</a></p></td>
<td><p><a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a><br />
 <a href="../Page/拉麗莎·沙辰科·尼蘭德.md" title="wikilink">拉麗莎·沙辰科·尼蘭德</a></p></td>
<td><p>6-3, 7-5</p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/琪琪·費爾南德斯.md" title="wikilink">琪琪·費爾南德斯</a><br />
 <a href="../Page/娜塔莉亞·齊佛瑞娃.md" title="wikilink">娜塔莉亞·齊佛瑞娃</a></p></td>
<td><p><a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a><br />
 <a href="../Page/阿蘭查·桑切斯·維卡裡奧.md" title="wikilink">桑切斯</a></p></td>
<td><p>6-3, 6-7(4), 6-3</p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a><br />
 <a href="../Page/阿蘭查·桑切斯·維卡裡奧.md" title="wikilink">桑切斯</a></p></td>
<td><p><a href="../Page/琪琪·費爾南德斯.md" title="wikilink">琪琪·費爾南德斯</a><br />
 <a href="../Page/娜塔莉亞·齊佛瑞娃.md" title="wikilink">娜塔莉亞·齊佛瑞娃</a></p></td>
<td><p>6-2, 6-1</p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a><br />
 <a href="../Page/瑪麗·喬·費爾南德斯.md" title="wikilink">瑪麗·喬·費爾南德斯</a></p></td>
<td><p><a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a><br />
 <a href="../Page/阿蘭查·桑切斯·維卡裡奧.md" title="wikilink">桑切斯</a></p></td>
<td><p>6-3, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a><br />
 <a href="../Page/雅娜·諾沃特娜.md" title="wikilink">諾沃特娜</a></p></td>
<td><p><a href="../Page/亞歷山德拉·弗塞.md" title="wikilink">亞歷山德拉·弗塞</a><br />
 <a href="../Page/納塔莉·陶吉雅.md" title="wikilink">納塔莉·陶吉雅</a></p></td>
<td><p>6-7(5), 6-3, 6-2</p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/達文波特.md" title="wikilink">達文波特</a><br />
 <a href="../Page/娜塔莎·齊佛瑞娃.md" title="wikilink">娜塔莎·齊佛瑞娃</a></p></td>
<td><p><a href="../Page/亞歷山德拉·弗塞.md" title="wikilink">亞歷山德拉·弗塞</a><br />
 <a href="../Page/納塔莉·陶吉雅.md" title="wikilink">納塔莉·陶吉雅</a></p></td>
<td><p>6-7(6), 7-5, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/瑪蒂娜·辛吉斯.md" title="wikilink">玛蒂娜·辛吉斯</a><br />
 <a href="../Page/安娜·庫妮可娃.md" title="wikilink">安娜·庫妮可娃</a></p></td>
<td><p><a href="../Page/阿蘭查·桑切斯·維卡裡奧.md" title="wikilink">桑切斯</a><br />
 <a href="../Page/拉麗莎·沙辰科·尼蘭德.md" title="wikilink">拉麗莎·沙辰科·尼蘭德</a></p></td>
<td><p>6-4, 6-4</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/瑪蒂娜·辛吉斯.md" title="wikilink">玛蒂娜·辛吉斯</a><br />
 <a href="../Page/安娜·庫妮可娃.md" title="wikilink">安娜·庫妮可娃</a></p></td>
<td><p><a href="../Page/妮可·阿倫特.md" title="wikilink">妮可·阿倫特</a><br />
 <a href="../Page/瑪儂·伯拉格拉芙.md" title="wikilink">瑪儂·伯拉格拉芙</a></p></td>
<td><p>6-2, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/麗莎·雷蒙.md" title="wikilink">麗莎·雷蒙</a><br />
 <a href="../Page/蓮內·斯塔布斯.md" title="wikilink">蓮內·斯塔布斯</a></p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/艾蓮娜·利霍夫采娃.md" title="wikilink">艾蓮娜·利霍夫采娃</a></p></td>
<td><p>7-5, 3-6, 6-3</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/德門提耶娃.md" title="wikilink">-{zh-hans:德门蒂耶娃;zh-hk:迪文泰娃;zh-tw:德門提耶娃;}-</a><br />
 <a href="../Page/珍妮特·胡莎露娃.md" title="wikilink">珍妮特·胡莎露娃</a></p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/耶蓮娜·利霍夫采娃.md" title="wikilink">耶蓮娜·利霍夫采娃</a></p></td>
<td><p>4-6, 6-4, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/維吉尼亞·勞諾·帕斯奎爾.md" title="wikilink">維吉尼亞·勞諾·帕斯奎爾</a><br />
 <a href="../Page/保拉·蘇亞雷絲.md" title="wikilink">保拉·蘇亞雷絲</a></p></td>
<td><p><a href="../Page/克萊斯特絲.md" title="wikilink">-{zh-hans:克莱斯特尔斯;zh-hk:克莉絲特絲;zh-tw:克萊斯特絲;}-</a><br />
 <a href="../Page/杉山愛.md" title="wikilink">杉山愛</a></p></td>
<td><p>6-4, 3-6, 6-3</p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/娜迪亞·佩特洛娃.md" title="wikilink">-{zh-hans:娜迪亞·佩特洛娃;zh-hk:娜迪亞·柏度娃;zh-tw:佩卓娃;}-</a><br />
 <a href="../Page/梅格海恩·蕭妮西.md" title="wikilink">梅格海恩·蕭妮西</a></p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/蓮內·斯塔布斯.md" title="wikilink">蓮內·斯塔布斯</a></p></td>
<td><p>7-5, 6-2</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/麗莎·雷蒙.md" title="wikilink">麗莎·雷蒙</a><br />
 <a href="../Page/薩曼莎·斯托瑟.md" title="wikilink">薩曼莎·斯托瑟</a></p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/蓮內·斯塔布斯.md" title="wikilink">蓮內·斯塔布斯</a></p></td>
<td><p>6-7(5), 7-5, 6-4</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/麗莎·雷蒙.md" title="wikilink">麗莎·雷蒙</a><br />
 <a href="../Page/薩曼莎·斯托瑟.md" title="wikilink">薩曼莎·斯托瑟</a></p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/蓮內·斯塔布斯.md" title="wikilink">蓮內·斯塔布斯</a></p></td>
<td><p>3-6, 6-3, 6-3</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/莉澤爾·許貝爾.md" title="wikilink">-{zh-hans:莉澤爾·許貝爾;zh-hk:莉澤爾·休伯;zh-tw:莉澤爾·胡珀;}-</a></p></td>
<td><p><a href="../Page/卡特琳娜·斯雷博特尼克.md" title="wikilink">卡特琳娜·斯雷博特尼克</a><br />
 <a href="../Page/杉山愛.md" title="wikilink">杉山愛</a></p></td>
<td><p>5-7, 6-3, 10-8</p></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/莉澤爾·許貝爾.md" title="wikilink">-{zh-hans:莉澤爾·許貝爾;zh-hk:莉澤爾·休伯;zh-tw:莉澤爾·胡珀;}-</a></p></td>
<td><p><a href="../Page/佩斯克.md" title="wikilink">佩斯克</a><br />
 <a href="../Page/蓮內·斯塔布斯.md" title="wikilink">蓮內·斯塔布斯</a></p></td>
<td><p>6-1, 7-5</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/维维斯.md" title="wikilink">维维斯</a><br />
 <a href="../Page/马丁内兹·桑切斯.md" title="wikilink">马丁内兹·桑切斯</a></p></td>
<td><p><a href="../Page/卡拉·布萊克.md" title="wikilink">卡拉·布萊克</a><br />
 <a href="../Page/莉澤爾·許貝爾.md" title="wikilink">-{zh-hans:莉澤爾·許貝爾;zh-hk:莉澤爾·休伯;zh-tw:莉澤爾·胡珀;}-</a></p></td>
<td><p>7–6(0), 5–7, 10–7</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/吉塞拉·杜尔科.md" title="wikilink">吉塞拉·杜尔科</a><br />
 <a href="../Page/弗拉维亚·佩内塔.md" title="wikilink">弗拉维亚·佩内塔</a></p></td>
<td><p><a href="../Page/佩斯克.md" title="wikilink">佩斯克</a><br />
 <a href="../Page/卡特琳娜·斯雷博特尼克.md" title="wikilink">卡特琳娜·斯雷博特尼克</a></p></td>
<td><p>7-5, 6-4</p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/麗莎·雷蒙.md" title="wikilink">麗莎·雷蒙</a><br />
 <a href="../Page/莉澤爾·許貝爾.md" title="wikilink">-{zh-hans:莉澤爾·許貝爾;zh-hk:莉澤爾·休伯;zh-tw:莉澤爾·胡珀;}-</a></p></td>
<td><p><a href="../Page/佩斯克.md" title="wikilink">佩斯克</a><br />
 <a href="../Page/卡特琳娜·斯雷博特尼克.md" title="wikilink">卡特琳娜·斯雷博特尼克</a></p></td>
<td><p>6-4, 6-4</p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/瑪麗亞·姬莉蘭歌.md" title="wikilink">瑪麗亞·姬莉蘭歌</a><br />
 <a href="../Page/娜迪亞·佩特洛娃.md" title="wikilink">娜迪亞·佩特洛娃</a></p></td>
<td><p>Andrea Hlaváčková<br />
 Lucie Hradecká</p></td>
<td><p>6-1, 6-4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年WTA巡回赛总决赛.md" title="wikilink">2013年</a></p></td>
<td><p><a href="../Page/谢淑薇.md" title="wikilink">谢淑薇</a><br />
 <a href="../Page/彭帅.md" title="wikilink">彭帅</a></p></td>
<td><p><a href="../Page/葉卡捷琳娜·馬卡洛娃.md" title="wikilink">葉卡捷琳娜·馬卡洛娃</a><br />
 <a href="../Page/艾蓮娜·費絲蓮娜.md" title="wikilink">艾蓮娜·費絲蓮娜</a></p></td>
<td><p>6-4, 7-5</p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/卡拉·布雷克.md" title="wikilink">卡拉·布雷克</a> <small>(3)</small><br />
 <a href="../Page/薩尼婭·米爾莎.md" title="wikilink">薩尼婭·米爾莎</a></p></td>
<td><p><a href="../Page/谢淑薇.md" title="wikilink">谢淑薇</a><br />
 <a href="../Page/彭帅.md" title="wikilink">彭帅</a></p></td>
<td><p>6–1, 6–0</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/玛蒂娜·辛吉斯.md" title="wikilink">玛蒂娜·辛吉斯</a> <small> (3)</small><br />
 <a href="../Page/薩尼婭·米爾莎.md" title="wikilink">薩尼婭·米爾莎</a> <small> (2) </small></p></td>
<td><p><a href="../Page/加比涅·穆古魯薩.md" title="wikilink">加比涅·穆古魯薩</a><br />
 <a href="../Page/卡拉·蘇亞雷斯·納凡諾.md" title="wikilink">卡拉·蘇亞雷斯·納凡諾</a></p></td>
<td><p>6–0, 6–3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年WTA巡回赛总决赛.md" title="wikilink">2016年</a></p></td>
<td><p><a href="../Page/葉卡捷琳娜·馬卡洛娃.md" title="wikilink">葉卡捷琳娜·馬卡洛娃</a> <small> </small><br />
 <a href="../Page/艾蓮娜·費絲蓮娜.md" title="wikilink">艾蓮娜·費絲蓮娜</a> <small> </small></p></td>
<td><p><a href="../Page/比丹妮·瑪迪克-辛特斯.md" title="wikilink">比丹妮·瑪迪克-辛特斯</a><br />
 <a href="../Page/露丝·萨法洛娃.md" title="wikilink">露丝·萨法洛娃</a></p></td>
<td><p>7-6(5), 6-3</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参见

  - [ATP世界巡回赛总决赛](../Page/ATP世界巡回赛总决赛.md "wikilink")

## 外部链接

  -
[Category:WTA巡迴賽](../Category/WTA巡迴賽.md "wikilink")
[Category:WTA巡迴賽總決賽](../Category/WTA巡迴賽總決賽.md "wikilink")
[Category:1972年建立的週期性體育事件](../Category/1972年建立的週期性體育事件.md "wikilink")