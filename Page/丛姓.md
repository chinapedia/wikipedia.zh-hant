**丛姓**是当今[中国姓氏排行第一百二十七位姓氏](../Page/中国姓氏.md "wikilink")，约占全国[汉族人口的百分之零点一](../Page/汉族.md "wikilink")。[汉武帝时](../Page/汉武帝.md "wikilink")[匈奴](../Page/匈奴.md "wikilink")[休屠王太子](../Page/休屠王.md "wikilink")[日磾归顺汉室](../Page/金日磾.md "wikilink")；由于休屠部曾铸铜人（金人）像祭天，获赐姓“金”，其后代从此姓金。据《池北偶谈》所载，传说[西汉时车骑将军金日磾的后代迁居](../Page/西汉.md "wikilink")[丛家岘](../Page/丛家岘.md "wikilink")（今属[山东省](../Page/山东省.md "wikilink")[文登市](../Page/文登市.md "wikilink")），遂以居地丛为氏。

## 來源

丛姓来源有三：

1.  源于[金姓](../Page/金姓.md "wikilink")。据《池北偶谈》所载，传说[西汉时车骑将军](../Page/西汉.md "wikilink")[金日磾的后代迁居丛家岘](../Page/金日磾.md "wikilink")（今属[山东省](../Page/山东省.md "wikilink")[文登市](../Page/文登市.md "wikilink")），遂以居地丛为氏。

2.
## 名人

  - [丛旭日](../Page/丛旭日.md "wikilink")
  - [叢鐇](../Page/叢鐇.md "wikilink")：[南北朝人](../Page/南北朝.md "wikilink")，官至[刺史](../Page/刺史.md "wikilink")。
  - [叢蘭](../Page/叢蘭.md "wikilink")：[明朝時](../Page/明朝.md "wikilink")[弘治年間進士](../Page/弘治.md "wikilink")，官至南京工部尚書。
  - [叢文蔚](../Page/叢文蔚.md "wikilink")：明朝[隆慶年間進士](../Page/隆慶.md "wikilink")，官至烏程縣令。
  - [叢慧芸](../Page/叢慧芸.md "wikilink")：[台灣壹電視新聞台主播](../Page/台灣.md "wikilink")。

[C](../Category/漢字姓氏.md "wikilink") [\*](../Category/叢姓.md "wikilink")