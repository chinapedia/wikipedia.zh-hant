**藍絲黛爾石**（）也译做**郎士德碳**，又因晶體結構及特性稱作**六方金剛石**（）、**六方碳**。藍絲黛爾石是一種[六方晶系的](../Page/六方晶系.md "wikilink")[金剛石](../Page/金剛石.md "wikilink")，屬於[碳同素異形體的一種構形](../Page/碳同素異形體.md "wikilink")，咸信為[流星上的](../Page/流星.md "wikilink")[石墨在墜入](../Page/石墨.md "wikilink")[地球時所形成](../Page/地球.md "wikilink")。撞擊時的巨大壓力及熱量改變石墨構形形成金剛石，卻又保留了石墨的平行[六邊形晶格](../Page/六邊形.md "wikilink")，並構成了立方的六方[晶格](../Page/晶胞.md "wikilink")。第一次鑑別出藍絲黛爾石是1967年在[美國](../Page/美國.md "wikilink")[亞利桑那州的](../Page/亞利桑那州.md "wikilink")[巴林杰隕石坑](../Page/巴林杰隕石坑.md "wikilink")\[1\]，從位在其中的「[魔谷隕石](../Page/代亞布羅峽谷隕石.md "wikilink")」中所發現，並以20世紀的[晶體學家](../Page/晶體學.md "wikilink")命名。

## 發現

藍絲黛爾石發生在隕石的金剛石上，是一個連結在金剛石上非肉眼可見的顯微晶體。除魔谷隕石外，在美國[新墨西哥州的](../Page/新墨西哥州.md "wikilink")「肯納隕石」（Kenna
meteorite）、[南極洲](../Page/南極洲.md "wikilink")[維多利亞地的](../Page/維多利亞地.md "wikilink")[艾倫丘陵隕石](../Page/艾倫丘陵隕石.md "wikilink")77283（Allan
Hills (ALH)
77283）上亦有發現。此外，1908年6月30日一個[阿波羅星體](../Page/阿波羅星體.md "wikilink")（指[外來星體](../Page/外來星體.md "wikilink")，包括[彗星及](../Page/彗星.md "wikilink")[隕石](../Page/隕石.md "wikilink")）撞擊[俄羅斯](../Page/俄羅斯.md "wikilink")[西伯利亞的](../Page/西伯利亞.md "wikilink")[通古斯加撞擊區也有發現報告](../Page/通古斯加.md "wikilink")。

藍絲黛爾石具有透明棕黃色的外觀，[折射率在](../Page/折射率.md "wikilink")2.40至2.41之間，[比重在](../Page/比重.md "wikilink")3.2至3.3之間，[莫氏硬度在](../Page/莫氏硬度.md "wikilink")7至8之間。而金剛石的莫氏硬度則為10。藍絲黛爾石也可從聚合物——（PHC）在[氬氣的一大氣壓力下從](../Page/氬.md "wikilink")[攝氏](../Page/攝氏.md "wikilink")110度開始到1000度[熱分解人工合成](../Page/熱分解.md "wikilink")。藍絲黛爾石較低的硬度主要原因系天然形成礦石不純且不完美所致。但如果以人工合成則比鑽石硬58%，而抗壓程度也比鑽石高了大約58%。

## 参考

## 外部链接

  - [Mindat.org](http://www.mindat.org/min-2431.html) accessed 3/13/05.
  - [Webmineral](http://webmineral.com/data/Lonsdaleite.shtml) accessed
    3/13/05.
  - Anthony, J.W., et al (1995), *Mineralogy of Arizona*, 3rd.ed.
  - Frondel, C. & U.B. Marvin (1967), Lonsdaleite, a new hexagonal
    polymorph of diamond. Nature: 214: 587-589
  - Frondel, C. & U.B. Marvin (1967), Lonsdaleite, a hexagonal polymorph
    of diamond, Am.Min.: 52
  - Bianconi, P. et al (2004), Diamond and Diamond-like Carbon from a
    Preceramic Polymer. J. Am. Chem. Soc. Vol. 126, No. 10, 3191-3202

[L](../Category/超硬材料.md "wikilink") [L](../Category/隕石.md "wikilink")
[L](../Category/碳的同素异形体.md "wikilink")

1.  <http://www.phys.ncku.edu.tw/~astrolab/mirrors/apod/ap990711.html>