**GPE**（**GPE掌上电脑环境**，
****）是一个计划为运行[Linux操作系统的](../Page/Linux.md "wikilink")[PDA等掌上设备提供](../Page/PDA.md "wikilink")[自由](../Page/自由软件.md "wikilink")[图形用户界面的项目](../Page/图形用户界面.md "wikilink")。GPE并不是一个单一的软件，但可以为运行Linux的掌上设备提供如[个人信息管理系统](../Page/个人信息管理系统.md "wikilink")
(PIM)、音乐播放、电子邮件及网页浏览等功能。

## 支持设备

以下平台上的嵌入式[Linux发行版捆绑了GPE](../Page/Linux发行版.md "wikilink")：

  - [夏普](../Page/夏普.md "wikilink")
    [Zaurus](../Page/Zaurus.md "wikilink")
  - [惠普](../Page/惠普.md "wikilink") [iPAQ](../Page/iPAQ.md "wikilink")
  - 惠普 [Jornada](../Page/Jornada.md "wikilink") 72x
  - [西门子](../Page/西门子.md "wikilink")
    [SIMpad](../Page/SIMpad.md "wikilink") SL4

另外，GPE的维护团队及开源社区还为其它的一些设备进行了开发\[1\]：

  - [GamePark Holdings](../Page/GamePark_Holdings.md "wikilink")
    [GP2x](../Page/GP2x.md "wikilink")
  - [诺基亚770网络终端](../Page/诺基亚770网络终端.md "wikilink")
  - [诺基亚 N800](../Page/诺基亚_N800.md "wikilink")
  - [Palm TX](../Page/Palm_TX.md "wikilink")
  - [Palm](../Page/Palm.md "wikilink") [Treo
    650](../Page/Treo_650.md "wikilink")\[2\]
  - [HTC Universal](../Page/HTC_Universal.md "wikilink")
  - [HTC Typhoon](../Page/HTC_Typhoon.md "wikilink")
  - [HTC Tornado](../Page/HTC_Tornado.md "wikilink")
  - [HTC Wizard](../Page/HTC_Wizard.md "wikilink")

2007年2月5日，GPE项目组宣布了手机版本的开发\[3\]。这个GPE的分支主要用于移动电话。

## 开发

GPE是一个基于[X Window
System的图形用户界面](../Page/X_Window_System.md "wikilink")，采用了[GTK+来做界面及](../Page/GTK+.md "wikilink")[Matchbox进行窗口管理](../Page/Matchbox.md "wikilink")。

这个项目通过提供一些核心软件如[库文件](../Page/库文件.md "wikilink")、数据库模式及使用了一些现有的技术如[SQLite](../Page/SQLite.md "wikilink")，[D-BUS](../Page/D-BUS.md "wikilink")，[GStreamer和各种](../Page/GStreamer.md "wikilink")[freedesktop.org制作的标准](../Page/freedesktop.org.md "wikilink")，给出了一个容易且强大的应用开发平台。

wxGPE是一个跨平台图形用户界面工具库[wxWidgets的接口](../Page/wxWidgets.md "wikilink")，可以使应用这个库的桌面软件轻松地移植到GPE环境\[4\]。

GPE的一个主要目标就是鼓励人们在移动设备上使用[自由软件并体验新技术](../Page/自由软件.md "wikilink")。

目前在GPE上已经开发的应用包括：

  - **GPE-Contacts** - 联系人管理器
  - **GPE-Calendar** - 日历程序
  - **GPE-Edit** - 简单的文本编辑器
  - **GPE-Filemanager** - 带MIME类型和远程访问支持的文件管理器
  - **GPE-Gallery** - 小型易用的图形查看器
  - **GPE-Games** - 小游戏集
  - **GPE-Mini-Browser** -
    兼容[CSS和](../Page/CSS.md "wikilink")[Javascript的小型网页浏览器](../Page/Javascript.md "wikilink")
  - **GPE-Sketchbook** - 创建笔记和草图
  - **GPE-Soundbite** - 录音笔记工具
  - **GPE-ToDo** - 任务列表管理器
  - **GPE-Timesheet** - 追踪任务花费的时间
  - **MkPhone** - 嵌入式的电话套件\[5\]
  - **Starling** - 基于GStreamer的音频播放器
  - **[VLC Media Player](../Page/VLC.md "wikilink")** - 高度移植的多媒体软件\[6\]

GPE的PIM软件（GPE-Contacts、GPE-Calendar及GPE-ToDo）可以通过使用**GPE-Syncd**及[OpenSync与对应的桌面及网络应用](../Page/OpenSync.md "wikilink")（[Novell
Evolution](../Page/Novell_Evolution.md "wikilink")、[Mozilla
Sunbird](../Page/Mozilla_Sunbird.md "wikilink")、[Google
Calendar等](../Page/Google_Calendar.md "wikilink")）进行同步。

GPE也提供了一系列图形化的配置工具，如[无线局域网](../Page/无线局域网.md "wikilink")、[蓝牙](../Page/蓝牙.md "wikilink")、[红外](../Page/红外通讯技术.md "wikilink")、[防火墙](../Page/防火墙.md "wikilink")，[ALSA等](../Page/ALSA.md "wikilink")。

一个基于Tinymail的移动[Push
e-mail客户端也正在开发](../Page/Push_e-mail.md "wikilink")\[7\]。

## Linux发行版中的GPE

GPE在以下嵌入式的Linux发行版中作为主环境使用：

  - [Ångström](../Page/Ångström_distribution.md "wikilink")
  - [Familiar Linux](../Page/Familiar_Linux.md "wikilink")
  - [OpenZaurus](../Page/OpenZaurus.md "wikilink")

而在其它的一些发行版中，GPE也可以通过包管理工具得到较好的支持：

  - [Ubuntu](../Page/Ubuntu_\(Linux_distribution\).md "wikilink")
  - [Debian](../Page/Debian.md "wikilink")
  - [Maemo](../Page/Maemo.md "wikilink")
  - pdaXrom\[8\] / pdaxii13

## 争议

GPE项目在网站服务变更、[IRC频道所有权及商标问题上也存在一些争议](../Page/IRC.md "wikilink")\[9\]\[10\]。

### 网站服务的变更

2002年4月起，GPE就把其网站放在Handhelds.org。后来，在一些GPE开发人员的建议下，网站于2006年10月移到了Linuxtogo.org\[11\]。Handhelds.org后来从GPE在Handhelds.org的网站上删除离开的开发人员用户账号以及所有连接到新站点的链接\[12\]。

### IRC频道所有权

由于双方都声称拥有位于[Freenode.net的](../Page/Freenode.net.md "wikilink")\#gpe
[IRC频道](../Page/IRC.md "wikilink")，Freenode.net锁定了这个频道并希望双方达成一致。后来Linuxtogo.org和Handhelds.org分别在Freenode.net上使用了\#gpe-project和\#handhelds-gpe频道。

### 商标争议

Handhelds.org的一名管理员George
France于2007年3月6日在[美国专利及商标局对GPE加上](../Page/美国专利及商标局.md "wikilink")[OPIE和](../Page/OPIE.md "wikilink")[Ipkg进行了商标注册](../Page/Ipkg.md "wikilink")\[13\]。在2007年6月25日，美国专利及商标局拒绝接受Handhelds.org上GPE网站截图作为Handhelds.org所有权的证据并要求提供更好的证据\[14\]。

Handhelds.org以及[开放源代码促进会的董事会成员](../Page/开放源代码促进会.md "wikilink")[Russ
Nelson声称GPE项目应该交给Handhelds](../Page/Russ_Nelson.md "wikilink").org以便公众参与开发\[15\]。

Linuxtogo.org的GPE开发人员表示他们代表了活跃的GPE项目，而Handhelds.org只是一个主机的提供者\[16\]\[17\]
此外，他们指出GPE项目在存放到Handhelds.org之前就已经存在\[18\]。

## 参考文献

## 参见

  - [OPIE](../Page/OPIE_\(operating_system\).md "wikilink")
  - [Palm OS](../Page/Palm_OS.md "wikilink")
  - [Pocket PC](../Page/Pocket_PC.md "wikilink")
  - [Qtopia](../Page/Qtopia.md "wikilink")
  - [Windows Mobile](../Page/Windows_Mobile.md "wikilink")

## 外部链接

  - [GPE web site at LinuxToGo](http://gpe.linuxtogo.org/)

  - [GPE Project wiki at
    LinuxToGo](https://web.archive.org/web/20080415220956/http://www.linuxtogo.org/gowiki/GpeProject)

  - [GPE web site at
    Handhelds.org](https://web.archive.org/web/20020405193956/http://gpe.handhelds.org/)

  - [GPE Project wiki at
    Handhelds.org](http://www.handhelds.org/moin/moin.cgi/GPEProject)

  - [GPE Phone
    Edition](https://web.archive.org/web/20070208083414/http://gpephone.linuxtogo.org/)

[Category:Linux](../Category/Linux.md "wikilink")
[Category:嵌入式系统](../Category/嵌入式系统.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.