**查理·马特**，意譯為**铁锤查理**（[古法語](../Page/古法語.md "wikilink")、；；；），[法兰克王国](../Page/法兰克王国.md "wikilink")[宫相](../Page/宫相.md "wikilink")，軍事领导人。出生于[埃斯塔勒](../Page/埃斯塔勒.md "wikilink")（位于现代[比利时的](../Page/比利时.md "wikilink")[列日附近](../Page/列日.md "wikilink")），是[宫相兼](../Page/宫相.md "wikilink")[法兰克人的公爵](../Page/法兰克人的公爵.md "wikilink")[丕平二世的私生子](../Page/丕平二世.md "wikilink")，[查理大帝的祖父](../Page/查理大帝.md "wikilink")。718年-741年[法兰克](../Page/法兰克.md "wikilink")[公爵兼攝政](../Page/公爵.md "wikilink")。

查理·馬特是宮相兼法蘭克公爵丕平二世與出生於貴族的的長子，阿爾派達是他父親的第二位妻子，或是妾侍。查理的親弟弟是法蘭克王國的史家。

## 流亡與爭權

赫斯塔的丕平與正妻所生的兩名兒子及[小格里摩爾德分別於](../Page/格里莫二世.md "wikilink")708年及714年逝世。普雷科特魯德為了不讓妾侍阿爾派達所生的兩名兒子查理及奇爾德布蘭\[1\]成為繼承人，極力遊說赫斯塔的丕平立年僅8歲的孫子[特奧德巴爾德](../Page/特奧德巴爾德.md "wikilink")（小格里摩爾德之子）為繼承人，以便普雷科特魯德為攝政。

赫斯塔的丕平於714年12月16日在居皮勒（Jupille）逝世，終年69歲\[2\]。特奧德巴爾德以8歲之齡成為宮相，由普雷科特魯德攝政，普雷科特魯德將查理囚禁於[科隆](../Page/科隆.md "wikilink")。

另一方面，國王[達戈貝爾特三世與](../Page/達戈貝爾特三世.md "wikilink")[紐斯特里亞](../Page/紐斯特里亞.md "wikilink")(Neustria)貴族推舉為紐斯特里亞宮相以推翻特奧德巴爾德的統治\[3\]。查理趁機逃出囚禁，並得到[奧斯特拉西亞](../Page/奧斯特拉西亞.md "wikilink")(Austrasia)貴族支持\[4\]。當普雷科特魯德的軍隊在科隆附近被拉甘弗雷德及國王[希爾佩里克二世](../Page/希爾佩里克二世.md "wikilink")（達戈貝爾特三世的繼承人）、額英夫洛及特奧德巴爾德的外公的聯軍打敗後，查理的軍隊潰退到艾菲爾山。普雷科特魯德只好答應退隱，將赫斯塔的丕平的大量財富交給他們，並讓特奧德巴爾德從宮相之位退下來\[5\]。

## 登上宮相

其後查理其後東山再起，糾集一支軍隊於716年在中打敗紐斯特里亞及弗里斯聯軍。其後於717年，查理又在[康布雷附近進行的](../Page/康布雷.md "wikilink")中徹底將紐斯特里亞人擊敗，從而鞏固了其在奧斯特拉西亞王國的地位。不久，查理便到科隆處理普雷科特魯德的威脅，查理解散她的追隨者，並繼承了奧斯特拉西亞宮相一職，但是查理並沒有加害普雷科特魯德及特奧德巴爾德。查理宣佈擁立[克洛泰爾四世為奧斯特拉西亞國王](../Page/克洛泰爾四世.md "wikilink")，與希爾佩里克二世對抗。當希爾佩里克二世與[阿基坦公爵](../Page/阿基坦公爵.md "wikilink")結盟，但是於718年又再在中被查理打敗。國王和厄德公爵南逃到[盧瓦爾河](../Page/盧瓦爾河.md "wikilink")，額英夫洛逃到[昂熱](../Page/昂熱.md "wikilink")。不久，克洛泰爾四世逝世，厄德公爵放棄與希爾佩里克二世結盟及國王向查理投降，以換對希爾佩里克二世被查理承認為全法蘭克國王。而查理亦正式成為赫斯塔的丕平的繼承人，受封為法蘭克唯一的宮相，成為法蘭克公爵兼王國攝政。

## 奠定加洛林王朝

查理是[欧洲](../Page/欧洲.md "wikilink")[中世纪最重要的人物之一](../Page/中世纪.md "wikilink")，其功绩包括奠定[加洛林王朝的基础](../Page/加洛林王朝.md "wikilink")，确立了[采邑制](../Page/采邑制.md "wikilink")，巩固与发扬当时的[封建社会制度](../Page/封建制度.md "wikilink")。他也是一名名将，最著名的一战便是于732年在[圖爾战役中阻擋了信奉](../Page/圖爾战役.md "wikilink")[伊斯兰教的](../Page/伊斯兰教.md "wikilink")[倭马亚王朝侵袭法兰克王国的军队](../Page/倭马亚王朝.md "wikilink")。此战制止了[穆斯林](../Page/穆斯林.md "wikilink")[勢力对欧洲的入侵](../Page/勢力.md "wikilink")，许多[历史学家认为查理](../Page/历史.md "wikilink")·马特的得胜拯救了欧洲[基督教文明](../Page/基督教.md "wikilink")。在前后长达二十五年的从政生涯中，其领地跨越现今[法国](../Page/法国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[意大利北部等地区](../Page/意大利.md "wikilink")。

737年[法兰克国王死后](../Page/法兰克国王.md "wikilink")，他成为帝国的唯一统治者，但像他父亲一样，他擔任[宮相](../Page/宮相.md "wikilink")，没有国王的称号。按[日耳曼人的传统](../Page/日耳曼人.md "wikilink")[諸子均分制](../Page/諸子均分制.md "wikilink")，马特在死前将帝国分给了他的两个儿子[丕平和](../Page/矮子丕平.md "wikilink")[卡洛曼](../Page/卡洛曼_\(查理·马特的长子\).md "wikilink")。

## 參考

[C](../Category/法兰克人.md "wikilink") [C](../Category/法兰克国王.md "wikilink")
[C](../Category/卡洛林王朝.md "wikilink")

1.

2.  Kurth, Godefroid (1908). "Charles Martel," In *The Catholic
    Encyclopedia,* Vol. 3, New York, NY, USA: Robert Appleton, see
    [1](http://www.newadvent.org/cathen/03629a.htm), accessed August 2,
    2015.

3.  Strauss, Gustave Louis M. (1854) *Moslem and Frank; or, Charles
    Martel and the rescue of Europe,* Oxford, GBR:Oxford University
    Press, see [2](https://books.google.com/books?id=Z4YBAAAAQAAJ),
    accessed 2 August 2015.

4.
5.  Costambeys, Marios; Matthew Innes & MacLean, Simon (2011) *The
    Carolingian World,* p. 43, Cambridge, GBR: Cambridge University
    Press, see [3](https://books.google.com/books?isbn=0521563666),
    accessed 2 August 2015.