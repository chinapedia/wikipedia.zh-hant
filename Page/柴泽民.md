**柴泽民**（），[中华人民共和国](../Page/中华人民共和国.md "wikilink")1949年后首任驻[美利坚合众国](../Page/美利坚合众国.md "wikilink")[特命全权大使](../Page/特命全权大使.md "wikilink")（1979年－1983年）。

柴泽民1916年10月出生在[山西省](../Page/山西省.md "wikilink")[闻喜县一个贫民家庭](../Page/闻喜县.md "wikilink")。青年时代加入[中国共产党](../Page/中国共产党.md "wikilink")。1960年12月进入[外交部工作](../Page/中華人民共和國外交部.md "wikilink")；1961年出任[中国驻匈牙利大使](../Page/中国驻匈牙利大使.md "wikilink")；1964年出任[中国驻几内亚大使](../Page/中国驻几内亚大使.md "wikilink")；1970年出任[中国驻埃及大使](../Page/中国驻埃及大使.md "wikilink")；1975年出任[中国驻泰国大使](../Page/中国驻泰国大使.md "wikilink")。

1978年夏，柴泽民出任[中国驻美国联络处主任](../Page/中国驻美国联络处.md "wikilink")，参与了[中美关系邦交正常化的谈判](../Page/中美关系邦交正常化.md "wikilink")。1978年底，中美两国达成[协议](../Page/协议.md "wikilink")，定于1979年1月1日正式建立[外交关系](../Page/外交关系.md "wikilink")，于3月1日互设[大使馆并互换大使](../Page/大使馆.md "wikilink")。柴泽民由此成为首任驻美大使。

由于对中美关系的建立与发展所做出的贡献，美国[密西根州立大学等三所大学授予柴泽民](../Page/密西根州立大学.md "wikilink")[名誉法学博士学位及](../Page/名誉法学博士.md "wikilink")[名誉教授职位](../Page/名誉教授.md "wikilink")。

卸任驻美大使职位后，柴泽民出任中国人民外交学会副会长。进入九十年代，他辞去外交学会工作后，又先后担任了多家协会、组织的会长、顾问等职。

2010年6月7日凌晨1点58分，在北京医院因病逝世，享年93岁。

## 外部链接

  - [世界华人名人录：柴泽民](http://www.whoswhochinese.com/wsw01/chaizemin.htm)

[Category:中華人民共和國駐美國大使](../Category/中華人民共和國駐美國大使.md "wikilink")
[Category:中華人民共和國駐泰國大使](../Category/中華人民共和國駐泰國大使.md "wikilink")
[Category:中華人民共和國駐埃及大使](../Category/中華人民共和國駐埃及大使.md "wikilink")
[Category:中華人民共和國駐幾內亞大使](../Category/中華人民共和國駐幾內亞大使.md "wikilink")
[Category:中華人民共和國駐匈牙利大使](../Category/中華人民共和國駐匈牙利大使.md "wikilink")
[Category:中華人民共和國外交官](../Category/中華人民共和國外交官.md "wikilink")
[Category:中國共產黨黨員](../Category/中國共產黨黨員.md "wikilink")
[Category:中国世界语者](../Category/中国世界语者.md "wikilink")
[Category:運城人](../Category/運城人.md "wikilink")
[Ze澤民](../Category/柴姓.md "wikilink")