這是可支援開放文件（[OpenDocument](../Page/OpenDocument.md "wikilink")）格式的軟體一覽表，適用於可編輯文書處理軟體的開放性文件的儲存動作。

## 目前支援

目前可支援[开放文档格式的軟體列表如下](../Page/开放文档格式.md "wikilink"):

### 文字文件(Text documents) (.odt)

#### 文字處理

  - [Abiword](../Page/Abiword.md "wikilink") 2.4 (唯讀：2.4, 匯入及匯出：2.4.2)
  - [KWord](../Page/KWord.md "wikilink") 1.4+ (由1.5版本起支援)
  - [LibreOffice Writer](../Page/LibreOffice_Writer.md "wikilink")
    (由3.0本起支援)
  - [OpenOffice.org Writer](../Page/OpenOffice.org_Writer.md "wikilink")
    (由2.0版本起全面支援, 純匯入：1.1.5)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 1.2 Writer
        (OpenOffice.org 1.1.5 derivate)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 2.0 Writer
        (OpenOffice.org 2.0.2 derivate)
      - [NextOffice](../Page/NextOffice.md "wikilink") 9.0 Writer
        (OpenOffice.org 2.0.2 derivate)
      - [StarOffice](../Page/StarOffice.md "wikilink") 8 Writer
        (OpenOffice.org 2.0 derivate)
      - [IBM Workplace](../Page/IBM_Workplace.md "wikilink") Documents
        2.6+ (OpenOffice.org 2.0 derivate)
  - [RedOffice5.0](../Page/RedOffice5.0.md "wikilink")
  - [TextMaker](../Page/TextMaker.md "wikilink") 2006 (純匯入；匯出功能開發中)
  - [Google Docs and
    Spreadsheets](../Page/Google_Docs_and_Spreadsheets.md "wikilink")，線上文書處理及試算表程式，可以讀寫OpenDocument
    word processing (ODT)格式的文件。
  - [Zoho
    Writer](../Page/Zoho_Office_Suite#Zoho_Writer.md "wikilink")，線上的文書處理程式，可以讀寫ODT
    格式的文件。
  - [ajaxWrite](../Page/ajaxWrite.md "wikilink")，線上文書處理程式，可以讀寫ODT 格式的文件。
  - [TextEdit](../Page/TextEdit.md "wikilink"), (In the Leopard
    Developers Preview) 可以讀寫ODT格式的文件。
  - [新代全方位出版系統](../Page/新代全方位出版系統.md "wikilink"),
    中文專業編排系統以ODT為主要格式，也可轉出epub 格式。

#### 其他軟體

##### 資料管理

  - [eLawOffice.it](../Page/eLawOffice.it.md "wikilink") 0.9.6.4, Law
    Firms crossplafrom [Java](../Page/Java.md "wikilink")
    [GPL](../Page/GPL.md "wikilink") 應用
    (客戶端伺服器)。它採用OpenDocument格式為模板生成OpenDocument的文件，並數據合併以便數據庫的應用,如客戶的姓名，地址等
    [1](http://www.elawOffice.it)
  - [LibreOffice Base](../Page/LibreOffice_Base.md "wikilink")

##### 文件管理

  - [eZ publish](../Page/eZ_publish.md "wikilink"), 支援匯入及匯出
  - [TEA text editor](../Page/TEA_text_editor.md "wikilink") (唯讀模式)
  - [Visioo Writer](../Page/Visioo_Writer.md "wikilink") 0.6 — 文件瀏覽器
  - [Mobile Office](../Page/Mobile_Office.md "wikilink") Symbian OS
    平台的OpenDocument套件

##### 翻譯支援

  - [OmegaT](../Page/OmegaT.md "wikilink") —
    OmegaT是一個以[Java編寫的免費翻譯軟件](../Page/Java.md "wikilink")

##### Viewer

  - [TextMaker
    Viewer](../Page/TextMaker_Viewer.md "wikilink")[連結](http://www.officeviewers.com/index_en.htm)

### 試算表文件(Spreadsheet documents) (.ods)

#### 試算表

  - [EditGrid](../Page/EditGrid.md "wikilink") 完整支援。
  - [Gnumeric](../Page/Gnumeric.md "wikilink")
    不完全支援讀寫OpenDocument試算表(ODS)。
  - [KSpread](../Page/KSpread.md "wikilink") (基本支援:1.4.x,本地支援:1.5起)
  - [LibreOffice Calc](../Page/LibreOffice_Calc.md "wikilink") (完整支援)
  - [OpenOffice.org Calc](../Page/OpenOffice.org_Calc.md "wikilink") 2.0
    (完整支援:2.0,只可匯入:1.1.5)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 1.2 Calc
        (OpenOffice 1.1.5的衍生物)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 2.0 Calc
        (OpenOffice 2.0.2的衍生物)
      - [NextOffice](../Page/NextOffice.md "wikilink") 9.0 Calc
        (OpenOffice 2.0.2的衍生物)
      - [StarOffice](../Page/StarOffice.md "wikilink") 8 Calc
        (OpenOffice 2.0的衍生物)
      - [602Office](../Page/602Office.md "wikilink") 2.0 Calc
        (OpenOffice 2.0的衍生物)
      - [IBM Workplace](../Page/IBM_Workplace.md "wikilink") Documents
        2.6+ (OpenOffice 2.0的衍生物)
  - [Tables](http://www.x-tables.eu) 不完全支援閱讀OpenDocument試算表(ODS).

### 簡報製作文件(Presentation documents) (.odp)

  - [KPresenter](../Page/KPresenter.md "wikilink")
    (基本支援:1.4.x,完全支援:1.5起)
  - [LibreOffice Impress](../Page/LibreOffice_Impress.md "wikilink")
    (完全支援)
  - [OpenOffice.org
    Impress](../Page/OpenOffice.org_Impress.md "wikilink") 2.0
    (完全支援:2.0,只可匯入:1.1.5)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 1.2 Impress
        (OpenOffice 1.1.55的衍生物)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 2.0 Impress
        (OpenOffice 2.0.25的衍生物)
      - [NextOffice](../Page/NextOffice.md "wikilink") 9.0 Impress
        (OpenOffice 2.0.25的衍生物)
      - [StarOffice](../Page/StarOffice.md "wikilink") 8 Impress
        (OpenOffice 2.05的衍生物)
      - [602Office](../Page/602Office.md "wikilink") 2.0 Impress
        (OpenOffice 2.05的衍生物)
      - [IBM Workplace](../Page/IBM_Workplace.md "wikilink") Documents
        2.6+ (OpenOffice 2.05的衍生物)

### 繪圖文件(Graphics documents) (.odg)

  - [Karbon14](../Page/Karbon14.md "wikilink") 1.5+ (匯出及匯入)
  - [LibreOffice Draw](../Page/LibreOffice_Draw.md "wikilink") (完全支援)
  - [OpenOffice.org Draw](../Page/OpenOffice.org_Draw.md "wikilink")
    (完全支援:2.0,只可匯入:1.1.5)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 1.2 Draw
        (OpenOffice 1.1.55的衍生物)
      - [NeoOffice](../Page/NeoOffice.md "wikilink") 2.0 Draw
        (OpenOffice 2.0.25的衍生物)
      - [NextOffice](../Page/NextOffice.md "wikilink") 9.0 Draw
        (OpenOffice 2.0.25的衍生物)
      - [StarOffice](../Page/StarOffice.md "wikilink") 8 Draw
        (OpenOffice 2.05的衍生物)
      - [602Office](../Page/602Office.md "wikilink") 2.0 Draw
        (OpenOffice 2.05的衍生物)
  - [Scribus](../Page/Scribus.md "wikilink") 1.2.2+ (只可匯入)

### 公式文件(Formula documents) (.odf)

  - [KFormula](../Page/KFormula.md "wikilink") 1.5+ (完全支援)
  - [LibreOffice Math](../Page/LibreOffice_Math.md "wikilink") (完全支援)
  - [OpenOffice.org Math](../Page/OpenOffice.org_Math.md "wikilink")
    (完全支援:2.0)

### 搜索工具

  - [Beagle](../Page/Beagle.md "wikilink") 0.1.4,
    Gnome項目的桌面搜索引擎。支援編制索引和搜索多種文件格式，包括OpenDocument的檔案。
  - [Google Desktop Search](../Page/Google_Desktop_Search.md "wikilink")
    有一個非官方的OpenDocument的插件提供支援8ODT, OTT, ODG, OTG, ODP, OTP, ODS, OTS,及
    ODF
    OpenDocument的檔案。但該插件沒有正確處理[Unicode字符](../Page/Unicode.md "wikilink")([連結](https://web.archive.org/web/20061119221444/http://desktop.google.com/plugins/i/indextheopenoffice.html))。
  - [Kat Desktop Search
    Environment](../Page/Kat_Desktop_Search_Environment.md "wikilink"),
    [KDE桌面搜索引擎](../Page/KDE.md "wikilink"),支援ODT, ODP, ODS, ODF, 及ODC。
  - [Copernic](http://www.copernic.com),
    桌面搜索引擎的Windows平台，支持OpenOffice.org文件(ODT,
    OTT, ODM, OTH, ODS, OTS, ODP及OTP) (版本: 1.1, 2.0)
  - [Openindexer](../Page/Openindexer.md "wikilink"), Windows
    PC和網絡的搜索引擎。支援編制索引和搜索OD\*文件格式(包括Draw)及PDF。附加功能：備份和恢復。

### 未分類

'請幫助把這些移到以上標題''

  - [Aukyla Document Management
    System](../Page/Aukyla_Document_Management_System.md "wikilink")
    2.0,輕量級及基於Web的文件管理系統。已有OpenDocument
    viewer和索引功能[連結](https://web.archive.org/web/20061021043049/http://www.auton.nl/software/adms_en.html)。
  - [DocMgr](../Page/DocMgr.md "wikilink")
    0.53.3,功能齊全的文件管理系統。包括搜索引擎及索引支援OpenDocument的文件。[連結](http://sourceforge.net/projects/docmgr/)
  - Hancom Office
  - [Knomos](../Page/Knomos.md "wikilink") 1.0 — 律師事務所的管理軟件
    [連結](http://www.knomos.org/)
  - [Lenteja](../Page/Lenteja.md "wikilink") — 支援text 文件存放
    [連結](http://www.ee.usyd.edu.au/~jjga/lenteja/)
  - Artech India's [Akshar Naveen
    連結](https://web.archive.org/web/20060531153039/http://www.efytimes.com/fullnews.asp?edid=10125)

## 無障礙

在[OpenDocument的讨论中提出了一个重要的问题](../Page/OpenDocument.md "wikilink")，即这种格式是否可被那些[身心障碍人士](../Page/身心障碍.md "wikilink")[使用](../Page/无障碍环境.md "wikilink")。有两个问题:
does the specification support accessibility, and are implementations
accessible?

### 成就

值得注意的是，因為 OpenDocument
是一種標準檔案格式，所以並不需要每個人都用相同的程式去讀寫OpenDocument的檔案；每個人都可以自由使用最適合它們的軟體去讀寫這種檔案。

## 參看

  - [OpenDocument software
    comparison](../Page/OpenDocument_software_comparison.md "wikilink")
  - [Network effect](../Page/Network_effect.md "wikilink")
  - [Open format](../Page/Open_format.md "wikilink")

## 參考資料

  - Korn, Peter (November 13, 2005). [Massachusetts, Open Document, and
    Accessibility](https://web.archive.org/web/20060524094115/http://blogs.sun.com/roller/page/korn/20051113)
  - Parris, D.C. (November 16, 2005). [Christians Challenge Microsoft to
    Support OpenDocument for
    Disadvantaged](http://lxer.com/module/newswire/view/47810/)
  - Sutor, Bob (November 10, 2005) [Regarding OpenDocument Format,
    Accessibility, and
    IBM](http://www-03.ibm.com/developerworks/blogs/page/BobSutor?entry=regarding_odf_accessibility_and_ibm)

## 外部連結

  - [Application support for
    ODF](https://web.archive.org/web/20061205085529/http://opendocumentfellowship.org/applications)
    (OpenDocument Fellowship).
  - [Forum
    Debate](https://web.archive.org/web/20070211053410/http://forum.redlers.com/viewtopic.php?t=14)
    a lively and informative ongoing debate over whether or not a word
    processor application should adopt the OpenDocument format.
  - [Open Interoperative Document
    Initiative](https://web.archive.org/web/20161021212152/http://www.oidi.org/)
    An organization dedicated to the promotion of open and
    interoperative formats for all media types. (Wiki, Articles, Forums,
    Links and FAQs).
  - Berlind, David ([October 25](../Page/October_25.md "wikilink")
    2005). [Corel confirms OpenDocument commitment. Novell's "in"
    too.](http://blogs.zdnet.com/BTL/?p=2064)
  - Groklaw's [ODF
    Resources](http://www.groklaw.net/staticpages/index.php?page=20051216153153504).
  - Sutor, Bob. The [ODF
    thread](https://web.archive.org/web/20061118160750/http://www.sutor.com/newsite/blog-open/?cat=93)
    in the [Open Blog](http://www.sutor.com/newsite/blog-open/).
  - Sutor, Bob. Dr. ODF: Examining OpenDocument Format with Python,
    Parts [1](http://www.sutor.com/newsite/blog-open/?p=987),
    [2](http://www.sutor.com/newsite/blog-open/?p=990),
    [3](http://www.sutor.com/newsite/blog-open/?p=995),
    [4](http://www.sutor.com/newsite/blog-open/?p=999),
    [5](http://www.sutor.com/newsite/blog-open/?p=1106), and
    [6](http://www.sutor.com/newsite/blog-open/?p=1117).
  - Updegrove, Andy. [Standards
    Blog](https://web.archive.org/web/20061006124454/http://www.consortiuminfo.org/newsblog/blog.php?ID=1978)
    at consortiuminfo.org, timely and thorough coverage of ODF issues
    especially as relates to adoption by the state of Massachusetts.
  - Vaughan-Nichols, Steven J. ([September
    28](../Page/September_28.md "wikilink") 2005). [WordPerfect Will
    Support OpenDocument -
    Someday](http://www.eweek.com/article2/0,1759,1864843,00.asp).
    *eWeek*.

[Category:OpenDocument](../Category/OpenDocument.md "wikilink")
[OpenDocument](../Category/Technology-related_lists.md "wikilink")
[Category:Computer
accessibility](../Category/Computer_accessibility.md "wikilink")
[Category:Ergonomics](../Category/Ergonomics.md "wikilink")
[Category:Office suites](../Category/Office_suites.md "wikilink")