**吉恩·夏普**（，
）是一位[哲学](../Page/哲学.md "wikilink")、[政治学和](../Page/政治学.md "wikilink")[社会学学者](../Page/社会学.md "wikilink")，以对[权力和](../Page/权力.md "wikilink")[非暴力运动的著述著名](../Page/非暴力.md "wikilink")，廣泛流傳於世界各地的社運界別，被視為全球[茉莉花革命的](../Page/茉莉花革命.md "wikilink")「理論推手」，在[西方國家享有](../Page/西方國家.md "wikilink")「非暴力抵抗的教父」和「非暴力戰爭的[克勞塞維茨](../Page/克勞塞維茨.md "wikilink")」之名\[1\]，又有「非暴力抗爭理論大師」之譽。2018年1月尾愛恩斯坦研究所行政總監
Jamila Raqib
公佈，夏普於上周日在波士頓家中離世，研究所發聲明形容，夏普畢生致力研究[非暴力抗爭](../Page/非暴力抗议.md "wikilink")，啟發了世界各地的學者、行動者，團體，及至政府機構\[2\]。

吉恩·夏普是[阿尔伯特·爱因斯坦研究所的资深学者](../Page/阿尔伯特·爱因斯坦研究所.md "wikilink")，拥有俄亥俄州立大学文学士和文学硕士及牛津大学政治理论哲学博士学位，还被授予[曼哈顿学院法学博士和里维耶学院](../Page/曼哈顿学院.md "wikilink")（Rivier
College）人道服务博士荣誉学位。他是[麻萨诸塞大学达特茅斯分校政治学荣誉教授](../Page/麻萨诸塞大学达特茅斯分校.md "wikilink")，曾任教[奥斯陆大学和麻萨诸塞大学波士顿校区](../Page/奥斯陆大学.md "wikilink")，并且在[哈佛大学国际问题中心从事将近三十年研究](../Page/哈佛大学.md "wikilink")。1983年他创立阿尔伯特·爱因斯坦研究所，目的是研究和宣传用[非暴力手段推进](../Page/非暴力.md "wikilink")[自由主义的](../Page/自由主义.md "wikilink")[民主过程](../Page/民主.md "wikilink")。

夏普首創政治學新名詞「公民防衛」，倡導[非暴力抗爭和](../Page/非暴力抗爭.md "wikilink")[公民不服从](../Page/公民不服从.md "wikilink")。包括過去波蘭、捷克、立陶宛、愛沙尼亞、拉脫維亞等[蘇聯](../Page/蘇聯.md "wikilink")[附庸國爭取獨立](../Page/附庸國.md "wikilink")，到[塞爾維亞人反抗獨裁者](../Page/塞爾維亞.md "wikilink")[米洛塞維奇](../Page/米洛塞維奇.md "wikilink")，以及[颜色革命](../Page/颜色革命.md "wikilink")，乌克兰的[栗子花革命](../Page/栗子花革命.md "wikilink")，吉尔吉斯斯坦的[郁金香革命](../Page/郁金香革命.md "wikilink")，缅甸的[番红花革命](../Page/番红花革命.md "wikilink")，[茉莉花革命](../Page/茉莉花革命.md "wikilink")，到[阿拉伯之春等一連串政治抗議運動](../Page/阿拉伯之春.md "wikilink")，都被視為减少社会动荡与民众痛苦和不流血更换政权的[和平演变与非暴力抗爭的不可防御的创造性的實踐的典范](../Page/和平演变.md "wikilink")。

## 關注台灣民主

夏普曾於1994年到過台灣，對台灣的民主發展情況有所了解。2011年12月，夏普提醒，若[2012年中華民國總統選舉](../Page/2012年中華民國總統選舉.md "wikilink")[民主進步黨總統候選人](../Page/民主進步黨.md "wikilink")[蔡英文勝選](../Page/蔡英文.md "wikilink")，不曉得中國獨裁政權會有什麼反應，這將使台灣長達四個月的政權轉移[空窗期非常危險](../Page/空窗期.md "wikilink")，必須有所防範；如果蔡英文勝選，就算能安度政權轉移空窗期，其第一年任期台灣還是處於面對來自[中國威脅的](../Page/中國威脅.md "wikilink")「高風險」；建議蔡英文，應趕在任期第二年內推動建構台灣「群眾性公民防衛」的相關立法，以對抗強權侵略與維護公民利益。\[3\]

## 著作

  - 《甘地行使道德力量的武器﹕三件個案史》於一九六零年出版，愛因斯坦為這本書撰序.

<!-- end list -->

  - 《作為一個政治戰略家的甘地，附道德及政治論文》一九七九年夏普推出，馬丁．路德．金夫人撰導論。\[4\]

<!-- end list -->

  - 《非暴力抗爭小手冊》，內容彙整包含抗議與說服、拒絕合作等198種非暴力方式，區分「抗議與說服」、「拒絕合作」、「非暴力干預」3大類，在國際廣為流傳。\[5\]\[6\]

<!-- end list -->

  - [《论战略性非暴力冲突：
    关于基本原则的思考》(简体中文版)](https://web.archive.org/web/20110228121309/http://www.aeinstein.org/organizationsOSNC_ChineseSimplified.html)
  - [《从独裁到民主:
    解放运动的概念框架》](https://web.archive.org/web/20120614202444/http://www.aeinstein.org/organizations3c9b.html)
  - [《群眾性防衛：一種超軍事的武器系統》](https://web.archive.org/web/20071016114457/http://www.aeinstein.org/organizationsa0fb.html)

## 参考链接

[Category:美国政治学家](../Category/美国政治学家.md "wikilink")
[Category:革命理論家](../Category/革命理論家.md "wikilink")
[Category:俄亥俄州立大学校友](../Category/俄亥俄州立大学校友.md "wikilink")
[Category:牛津大學聖嘉芙蓮學院校友](../Category/牛津大學聖嘉芙蓮學院校友.md "wikilink")

1.  <http://www.yzzk.com/cfm/Content_Archive.cfm?Channel=ac&Path=3105002231/09aj2a.cfm>
2.
3.  [「茉莉花革命」推手
    肯定小英民主貢獻](http://www.libertytimes.com.tw/2011/new/dec/11/today-fo3.htm)
    ，自由時報，2011-12-11
4.  <http://www.yzzk.com/cfm/Content_Archive.cfm?Channel=ac&Path=3105002231/09aj2a.cfm>
5.  [非暴力抗爭小手冊](http://www.libertytimes.com.tw/2013/new/aug/19/today-fo4.htm?Slots=TPhoto)
    , [自由時報](../Page/自由時報.md "wikilink"), 2013-8-19
6.  [挺大埔　拆政府　２萬人凱道怒吼](http://www.twtimes.com.tw/INDEX.PHP?page=news&nid=351328),
    [臺灣時報](../Page/臺灣時報.md "wikilink"), 2013-08-19