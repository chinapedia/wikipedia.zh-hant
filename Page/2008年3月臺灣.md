## [3月31日](../Page/3月31日.md "wikilink")

## [3月30日](../Page/3月30日.md "wikilink")

## [3月29日](../Page/3月29日.md "wikilink")

## [3月28日](../Page/3月28日.md "wikilink")

## [3月27日](../Page/3月27日.md "wikilink")

  - [第12任正副總統當選人](../Page/2008年中華民國總統選舉#總統候選人.md "wikilink")**[馬英九](../Page/馬英九.md "wikilink")**、**[蕭萬長](../Page/蕭萬長.md "wikilink")**，於今日下午在展開「**請益之旅**」，首站前往拜會前[總統](../Page/中華民國總統.md "wikilink")**[李登輝](../Page/李登輝.md "wikilink")**，針對國政事務進行會談。此次也是李馬兩人八年來的首次會面。[蕃薯藤新聞─中央社](http://news.yam.com/cna/politics/200803/20080327990777.html)[自由時報](http://news.ltn.com.tw/news/focus/paper/199516)

## [3月26日](../Page/3月26日.md "wikilink")

  - [第12任總統候選人兼](../Page/2008年中華民國總統選舉#總統候選人.md "wikilink")[民進黨代理主席](../Page/民主進步黨.md "wikilink")**[謝長廷](../Page/謝長廷.md "wikilink")**，在今日召開的[例行中常會上宣布辭去](../Page/民主進步黨#組織架構.md "wikilink")**黨主席**職務，並針對民進黨的敗選提出數項改革建議。[蕃薯藤新聞─中央社](http://news.yam.com/cna/politics/200803/20080326986726.html)

## [3月25日](../Page/3月25日.md "wikilink")

  - **[美國](../Page/美國.md "wikilink")[國防部](../Page/美國國防部.md "wikilink")**在定期[記者會中表示](../Page/記者會.md "wikilink")，四個裝有[義勇兵洲際彈道飛彈](../Page/洲際彈道飛彈.md "wikilink")[引信的箱子](../Page/引信.md "wikilink")，前年秋天被誤裝為[聯勤訂購的](../Page/聯合後勤司令部.md "wikilink")[直升機](../Page/直升機.md "wikilink")[電池而被運到](../Page/電池.md "wikilink")[台灣](../Page/台灣.md "wikilink")。美國方面在[台灣駐外單位的協助下](../Page/中華民國駐外機構.md "wikilink")，已於日前取回這批引信。[台灣國防部也同步證實此事](../Page/中華民國國防部.md "wikilink")。[自由時報](https://web.archive.org/web/20080329211708/http://www.libertytimes.com.tw/2008/new/mar/26/today-fo1.htm)

## [3月24日](../Page/3月24日.md "wikilink")

  - [總統當選人](../Page/中華民國總統.md "wikilink")[馬英九的](../Page/馬英九.md "wikilink")[妻子](../Page/妻子.md "wikilink")**[周美青](../Page/周美青.md "wikilink")**，在[總統大選後的首個上班日](../Page/2008年中華民國總統選舉.md "wikilink")，如以往一樣搭乘[公車上班](../Page/公車.md "wikilink")。由於周美青已具有準[第一夫人的身分](../Page/第一夫人.md "wikilink")，[國安局維安人員開始進行貼身保護](../Page/國家安全局.md "wikilink")，但其生活作息仍暫時維持正常。[自由時報](https://web.archive.org/web/20080528095435/http://www.libertytimes.com.tw/2008/new/mar/25/today-p1-2.htm)[中國時報](https://web.archive.org/web/20080327202703/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008032500088,00.html)
  - **[台北](../Page/台北.md "wikilink")[匯市](../Page/匯市.md "wikilink")**在[總統大選後的首個交易日](../Page/2008年中華民國總統選舉.md "wikilink")，[新台幣兌換](../Page/新台幣.md "wikilink")[美元](../Page/美元.md "wikilink")[匯率升值](../Page/匯率.md "wikilink")3.21[角](../Page/角.md "wikilink")，以**30.229元**作收，創下十年半以來新高紀錄。[蕃薯藤新聞─中廣](http://news.yam.com/chinatimes/fn/200803/20080325981393.html)
  - **[臺北縣](../Page/新北市.md "wikilink")[淡水鎮](../Page/淡水區.md "wikilink")**發生一件集體**謀財害命案**。在淡水經營[民宿的李憲璋](../Page/民宿.md "wikilink")，涉嫌夥同手下囚禁凌虐**[遊民](../Page/遊民.md "wikilink")**、並帶往[中國大陸謀財害命後](../Page/中國大陸.md "wikilink")，詐領事前投保的鉅額[保險金](../Page/保險金.md "wikilink")。刑事局今日從民宿頂樓，救出五名瘦弱的遊民，並逮捕李嫌及三名共犯。[自由時報](https://web.archive.org/web/20080428022615/http://www.libertytimes.com.tw/2008/new/mar/25/today-t1.htm)[中國時報](https://web.archive.org/web/20080401003511/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110503+112008032500404,00.html)

## [3月23日](../Page/3月23日.md "wikilink")

  - [第12任正副總統當選人](../Page/2008年中華民國總統選舉#總統候選人.md "wikilink")**[馬英九](../Page/馬英九.md "wikilink")**、**[蕭萬長](../Page/蕭萬長.md "wikilink")**於[台北市青少年育樂中心舉行](../Page/台北市青少年育樂中心.md "wikilink")**[國際](../Page/國際.md "wikilink")[記者會](../Page/記者會.md "wikilink")**。關於各界矚目的[兩岸關係](../Page/兩岸關係.md "wikilink")，馬英九呼籲[北京當局](../Page/北京當局.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[人民可選自己的](../Page/台灣人.md "wikilink")[總統](../Page/總統.md "wikilink")、[國會](../Page/國會.md "wikilink")，「希望北京領導人謹記在心」，絕對不要把台灣視同[西藏及](../Page/西藏.md "wikilink")[香港](../Page/香港.md "wikilink")，唯有和平對等，才能化解[兩岸僵局](../Page/台灣問題.md "wikilink")。[中國時報](https://web.archive.org/web/20080327195341/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008032400049,00.html)[自由時報](http://news.ltn.com.tw/news/focus/paper/198312)

## [3月22日](../Page/3月22日.md "wikilink")

  - **[第12任總統、副總統選舉暨](../Page/2008年中華民國總統選舉.md "wikilink")[全國性公民投票第5、6案](../Page/2008年台灣入聯公投.md "wikilink")**舉行投開票，[中國國民黨籍候選人](../Page/中國國民黨.md "wikilink")**[馬英九](../Page/馬英九.md "wikilink")**、**[蕭萬長](../Page/蕭萬長.md "wikilink")**以超過765萬票、58.45%的得票率擊敗[民主進步黨候選人](../Page/民主進步黨.md "wikilink")[謝長廷](../Page/謝長廷.md "wikilink")、[蘇貞昌](../Page/蘇貞昌.md "wikilink")，當選第12任正副總統；同日舉行之[入聯](../Page/以台灣名義加入聯合國全國性公民投票案.md "wikilink")、[返聯公投因投票率僅約](../Page/2008年返聯公投案.md "wikilink")36%，均遭**否決**。[蕃薯藤新聞─中央社](http://news.yam.com/cna/politics/200803/20080322973461.html)

## [3月21日](../Page/3月21日.md "wikilink")

  - [中央選舉委員會對明日舉行的](../Page/中央選舉委員會.md "wikilink")**[第12任總統、副總統選舉暨](../Page/2008年中華民國總統選舉.md "wikilink")[全國性公民投票第5、6案](../Page/2008年台灣入聯公投.md "wikilink")**進行計票總預演，預計明晚9時30分可宣布選舉與公投結果。中央選舉委員會主任委員[張政雄預估](../Page/張政雄.md "wikilink")，這次總統大選的投票率約為75%，可能低於[2004年總統大選的八成投票率](../Page/2004年中華民國總統選舉.md "wikilink")。[中國時報](https://web.archive.org/web/20080426010557/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008032200082,00.html)

## [3月20日](../Page/3月20日.md "wikilink")

  - **[立法院](../Page/立法院.md "wikilink")[民進黨黨團](../Page/民主進步黨.md "wikilink")**上午召開[記者會](../Page/記者會.md "wikilink")，出示黨團所掌握的**[國民黨](../Page/中國國民黨.md "wikilink")**內部文件，內容揭示國民黨將在**[總統大選](../Page/2008年中華民國總統選舉.md "wikilink")**當日實施「**票櫃奇兵**」及「**藍鷹專案**」計畫，以防止選情被[謝長廷陣營逆轉](../Page/謝長廷.md "wikilink")，預計將花費[新台幣](../Page/新台幣.md "wikilink")3億元預算。民進黨團內多位[立委隨後前往](../Page/立法委員.md "wikilink")[台北地檢署](../Page/台北地檢署.md "wikilink")，遞狀控告[馬英九陣營涉嫌](../Page/馬英九.md "wikilink")**[期約賄選](../Page/賄選.md "wikilink")**；國民黨方面則強調兩項固票計劃並不違法，其主要目的是防止選舉出現「奧步」。[蕃薯藤新聞─中央社](http://news.yam.com/cna/politics/200803/20080320925464.html)[中時電子報](https://web.archive.org/web/20080324134241/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,130501+132008032001093,00.html)

## [3月19日](../Page/3月19日.md "wikilink")

  - **[郵政儲金](../Page/臺灣郵政.md "wikilink")**發生掏空爭議。[台灣郵政公司前](../Page/台灣郵政公司.md "wikilink")[董事](../Page/董事.md "wikilink")**[劉政池](../Page/劉政池.md "wikilink")**出面指出，台郵投入資金投資有掏空疑慮的[陽信銀行](../Page/陽信銀行.md "wikilink")，但在[2008年](../Page/2008年.md "wikilink")1月出現改制公司以來最大的[新台幣](../Page/新台幣.md "wikilink")36億元虧損額，而台郵[董事會附設的資金運用監督小組欲進行調查時](../Page/董事會.md "wikilink")，遭到兼代台郵[董事長的](../Page/董事長.md "wikilink")[交通部次長](../Page/中華民國交通部.md "wikilink")**[何煖軒](../Page/何煖軒.md "wikilink")**裁示「緩議」，而監督小組在3月即被裁撤，身為召集人的劉政池也被解除董事一職。而台灣郵政公司在同日坦承金援陽信銀行案屬實。[中國時報１](https://web.archive.org/web/20080327184148/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008032000095,00.html)[２](https://web.archive.org/web/20080324010757/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008032000096,00.html)

## [3月18日](../Page/3月18日.md "wikilink")

  - 資深藝人**[江霞](../Page/江霞.md "wikilink")**在[苗栗縣](../Page/苗栗縣.md "wikilink")[苑裡鎮的](../Page/苑裡鎮.md "wikilink")[民進黨](../Page/民主進步黨.md "wikilink")[正副總統候選人](../Page/2008年中華民國總統選舉#總統候選人.md "wikilink")**[長](../Page/謝長廷.md "wikilink")[昌配](../Page/蘇貞昌.md "wikilink")**的造勢大會中，公開批評海外回國支持[馬英九的藝人](../Page/馬英九.md "wikilink")，並說出「不用把他們當人看」的話語。[媒體大幅報導此消息後](../Page/台灣媒體.md "wikilink")，立即遭到馬英九陣營批評撕裂[族群](../Page/台灣四大族群.md "wikilink")；江霞則表示媒體曲解其談話內容，但她不會對此提出[告訴](../Page/告訴.md "wikilink")。[中國時報](https://web.archive.org/web/20080421234225/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110502+112008031900432,00.html)[PChome新聞─中央社１](http://news.pchome.com.tw/politics/cna/20080319/index-20080319182334180212.html)[２](http://news.pchome.com.tw/politics/cna/20080319/index-20080319195839180257.html)

## [3月17日](../Page/3月17日.md "wikilink")

  - **[中央選舉委員會](../Page/中央選舉委員會.md "wikilink")**對於即將與[總統選舉合併舉行的](../Page/2008年中華民國總統選舉.md "wikilink")**[入聯返聯公投案](../Page/2008年台灣入聯公投.md "wikilink")**作出決議，選民在領取公投票時，將要求選務工作人員以「是不是兩張都領？」的標準用語詢問，以避免影響選民的領投票意象。而選民如果把選舉票和公投票投錯票匭，仍一律算有效票。[中國時報](https://web.archive.org/web/20080322025939/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110502+112008031800467,00.html)

## [3月16日](../Page/3月16日.md "wikilink")

  - [教育部主任秘書](../Page/中華民國教育部.md "wikilink")**[莊國榮](../Page/莊國榮.md "wikilink")**在[台中市參加](../Page/台中市.md "wikilink")[民進黨](../Page/民主進步黨.md "wikilink")「百萬人擊掌逆轉勝」活動時，使用[髒話批評](../Page/髒話.md "wikilink")[馬英九已故父親](../Page/馬英九.md "wikilink")**[馬鶴凌](../Page/馬鶴凌.md "wikilink")**，引發各界批評聲浪。[莊國榮在稍後請辭教育部主任秘書職務](../Page/莊國榮.md "wikilink")，並獲部長[杜正勝批准](../Page/杜正勝.md "wikilink")。[中國時報](https://web.archive.org/web/20080420075719/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008031700067,00.html)

## [3月15日](../Page/3月15日.md "wikilink")

  - 從今日凌晨起，**[中山高速公路](../Page/中山高速公路.md "wikilink")**大安溪橋以南至[楠梓交流道路段](../Page/楠梓交流道.md "wikilink")，[最高速限放寬為時速](../Page/最高速限.md "wikilink")110[公里](../Page/公里.md "wikilink")；[北宜高速公路](../Page/北宜高速公路.md "wikilink")**[雪山隧道](../Page/雪山隧道.md "wikilink")**路段的最高速限，也同時由時速70公里調高至80公里。[自由時報](https://web.archive.org/web/20080525094409/http://www.libertytimes.com.tw/2008/new/mar/16/today-life2.htm)
  - [台灣高鐵公司宣布](../Page/台灣高鐵公司.md "wikilink")**[台灣高鐵](../Page/台灣高速鐵路.md "wikilink")**自通車以來規模最大的**票價優惠方案**。從[3月31日至](../Page/3月31日.md "wikilink")[6月30日止](../Page/6月30日.md "wikilink")，週一至週四（[國定例假日、連續假期前一日及特定公告日除外](../Page/中華民國國定假日.md "wikilink")）搭乘高鐵各班次，除現行的優惠價外再打八折。依此優惠方案，[台北至](../Page/台北車站.md "wikilink")[左營自由座僅](../Page/新左營車站.md "wikilink")1070[元](../Page/新台幣.md "wikilink")，直逼[台鐵](../Page/台鐵.md "wikilink")[自強號台北至](../Page/自強號列車.md "wikilink")[高雄票價](../Page/高雄車站.md "wikilink")。[自由時報１](https://web.archive.org/web/20080520104421/http://www.libertytimes.com.tw/2008/new/mar/16/today-life1.htm)[２](https://web.archive.org/web/20080520104416/http://www.libertytimes.com.tw/2008/new/mar/16/today-life1-3.htm)[中國時報](https://web.archive.org/web/20080418060354/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110503+112008031600163,00.html)[蘋果日報](http://1-apple.com.tw/latest_news/index.cfm?Fuseaction=Article&RArtID=12826&ShowDate=20080316)

## [3月14日](../Page/3月14日.md "wikilink")

  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**第七日（最後一日）賽事，[台灣/中華對](../Page/中華成棒隊.md "wikilink")[南韓之戰於](../Page/韓國棒球代表隊.md "wikilink")[台中洲際棒球場舉行](../Page/台中洲際棒球場.md "wikilink")。中華隊雖從3局上起派出救援投手[張誌家](../Page/張誌家.md "wikilink")，並成功壓制南韓打者火力，終場仍以**3:4**不敵南韓，最終在資格賽以5勝2敗的成績排名第3。[PChome新聞─中央社](http://news.pchome.com.tw/sport/cna/20080315/index-20080315000815180014.html)[中國時報１](https://web.archive.org/web/20080418060155/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008031500082,00.html)[２](https://web.archive.org/web/20080318160006/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008031500083,00.html)[自由時報](https://web.archive.org/web/20080518111952/http://www.libertytimes.com.tw/2008/new/mar/15/today-sp2.htm)
  - [立法委員](../Page/立法委員.md "wikilink")**[費鴻泰](../Page/費鴻泰.md "wikilink")**在[立法院召開](../Page/立法院.md "wikilink")[記者會](../Page/記者會.md "wikilink")，宣佈退出[國民黨](../Page/中國國民黨.md "wikilink")，以對[3月12日爆發的](../Page/3月12日.md "wikilink")**[長昌總部衝突事件](../Page/2008年中華民國總統選舉#台灣維新館事件.md "wikilink")**負責。費鴻泰並宣稱，如果因此事件而造成國民黨[總統候選人](../Page/2008年中華民國總統選舉#總統候選人.md "wikilink")[馬英九落選](../Page/馬英九.md "wikilink")，他不排結束自己的生命。[蕃薯藤新聞─中央社](http://news.yam.com/cna/politics/200803/20080314833830.html)[中國時報](http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008031500094,00.html)[自由時報](https://web.archive.org/web/20080518111240/http://www.libertytimes.com.tw/2008/new/mar/15/today-fo6.htm)

## [3月13日](../Page/3月13日.md "wikilink")

  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**第六日賽事，[台灣/中華以](../Page/中華成棒隊.md "wikilink")**4:0**完封[南非](../Page/南非棒球代表隊.md "wikilink")，取得資格賽第五勝。但中華隊打擊火力減弱，全場僅敲出五支安打。[PChome新聞─中央社](http://news.pchome.com.tw/sport/cna/20080314/index-20080314000808180008.html)

## [3月12日](../Page/3月12日.md "wikilink")

  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**第五日賽事，[台灣/中華在](../Page/中華成棒隊.md "wikilink")[斗六棒球場迎戰](../Page/斗六棒球場.md "wikilink")[澳洲](../Page/澳洲棒球代表隊.md "wikilink")，中華隊投手**[陽建福](../Page/陽建福.md "wikilink")**完投九局，壓制澳洲隊打擊火力，終場以**5:0**完封澳洲。中華隊再勝一場即可取得[北京奧運參賽資格](../Page/2008年夏季奧林匹克運動會棒球比賽.md "wikilink")。[蕃薯藤新聞─中央社](http://sports.yam.com/cna/sports/200803/20080312799920.html)
  - [國民黨籍](../Page/中國國民黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")**[費鴻泰](../Page/費鴻泰.md "wikilink")**、**[陳-{杰}-](../Page/陳杰_\(政治人物\).md "wikilink")**、**[羅明才](../Page/羅明才.md "wikilink")**、**[羅淑蕾](../Page/羅淑蕾.md "wikilink")**等人，下午前往[民進黨](../Page/民主進步黨.md "wikilink")[正副總統候選人](../Page/2008年中華民國總統選舉.md "wikilink")**[謝長廷](../Page/謝長廷.md "wikilink")[蘇貞昌](../Page/蘇貞昌.md "wikilink")[競選總部](../Page/競選總部.md "wikilink")**，質疑其違法使用向[第一金控租用大樓的部分空間](../Page/第一金融控股公司.md "wikilink")，引發謝陣營支持者抗議，兩方人馬爆發衝突。對此事件，國民黨籍總統候選人[馬英九在第一時間表示歉意](../Page/馬英九.md "wikilink")，國民黨中央則表示將在隔日公開向外界致歉。[蕃薯藤新聞─中央社１](http://news.yam.com/cna/politics/200803/20080313803630.html)[２](http://news.yam.com/cna/politics/200803/20080312799984.html)[３](http://news.yam.com/cna/politics/200803/20080313803631.html)

## [3月11日](../Page/3月11日.md "wikilink")

  - **[馬紹爾群島共和國](../Page/馬紹爾群島.md "wikilink")[總統](../Page/總統.md "wikilink")[湯敏彥](../Page/湯敏彥.md "wikilink")**（H.E.
    Litokwa
    Tomeing）今日在**[立法院](../Page/立法院.md "wikilink")**發表演講，重申支持[台灣加入](../Page/台灣.md "wikilink")[聯合國與其他相關](../Page/聯合國.md "wikilink")[國際組織的決心](../Page/國際組織.md "wikilink")。湯敏彥同時也是第三位在立法院演講的現任[外國元首](../Page/國家元首.md "wikilink")。[PChome新聞─中央社](http://news.pchome.com.tw/politics/cna/20080311/index-20080311132941180116.html)

## [3月10日](../Page/3月10日.md "wikilink")

  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**第四日賽事，[台灣/中華對](../Page/中華成棒隊.md "wikilink")[加拿大之戰於](../Page/加拿大棒球代表隊.md "wikilink")[台中洲際棒球場舉行](../Page/台中洲際棒球場.md "wikilink")，雙方纏鬥至第十局延長賽，最後以**5:6**不敵加拿大，吞下比賽首敗。但比賽八局下時，加拿大跑者**[凡歐斯崔德](../Page/吉米·凡歐斯崔德.md "wikilink")**（Jimmy
    VanOstrand）衝回本壘不及，結果衝撞中華隊[捕手](../Page/捕手.md "wikilink")**[葉君璋](../Page/葉君璋.md "wikilink")**，兩隊險爆發衝突。[蕃薯藤新聞─中央社１](http://sports.yam.com/cna/sports/200803/20080311762164.html)[２](https://web.archive.org/web/20080315153701/http://sports.yam.com/cna/sports/200803/20080311762160.html)[中國時報](https://web.archive.org/web/20080324135353/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008031100087,00.html)[自由時報](https://web.archive.org/web/20080516083336/http://www.libertytimes.com.tw/2008/new/mar/11/today-sp2.htm)
  - **[臺灣銀行](../Page/臺灣銀行.md "wikilink")**於[3月8日舉行的](../Page/3月8日.md "wikilink")「[九十七年新進檢券員考試](../Page/民國97年.md "wikilink")」驚傳集體**電子[舞弊案](../Page/作弊.md "wikilink")**，[警政署電信警察隊除逮捕舞弊集團成員](../Page/內政部警政署.md "wikilink")15人外，另在考場逮捕就讀[台大](../Page/國立臺灣大學.md "wikilink")、[成大](../Page/國立成功大學.md "wikilink")[研究所的](../Page/研究所.md "wikilink")4名槍手，以及22名涉嫌舞弊的考生。由於此考試並非[國家考試](../Page/國家考試.md "wikilink")，警方[偵訊後依偽造文書罪嫌將](../Page/偵訊.md "wikilink")41人移送法辦。[自由時報](https://web.archive.org/web/20080516083417/http://www.libertytimes.com.tw/2008/new/mar/11/today-t1.htm)[PChome新聞─中央社](http://news.pchome.com.tw/society/cna/20080310/index-20080310165010180146.html)
  - [台北市](../Page/台北市.md "wikilink")[愛國東路](../Page/臺北市主要道路列表.md "wikilink")[法務部舊宿舍](../Page/中華民國法務部.md "wikilink")「**[華光社區](../Page/華光社區.md "wikilink")**」於今日凌晨發生大火，經過半小時灌救才撲滅火勢，幸未造成人員傷亡。但適逢[台北市政府要求社區內住戶搬遷之時](../Page/台北市政府.md "wikilink")，故此火警遭懷疑為人為縱火。([TVBS(2008/03/10)北市華光社區大火　5戶房陷火海](http://www.tvbs.com.tw/news/news_list.asp?no=ghost20080310075310))

## [3月9日](../Page/3月9日.md "wikilink")

  - [高雄捷運首條營運路線](../Page/高雄捷運.md "wikilink")──**[高雄捷運紅線](../Page/高雄捷運紅線.md "wikilink")**[小港](../Page/小港站.md "wikilink")－[橋頭火車站段正式通車](../Page/橋頭車站.md "wikilink")，即日起至**[4月6日](../Page/4月6日.md "wikilink")**開放免費搭乘。[新浪新聞─中央社１](http://news.sina.com.tw/politics/cna/tw/2008-03-09/15166037103.shtml)[２](http://news.sina.com.tw/politics/cna/tw/2008-03-09/15466037166.shtml)
  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**第三日賽事，[台灣/中華以](../Page/中華成棒隊.md "wikilink")**0:2**擊敗[德國](../Page/德國棒球代表隊.md "wikilink")，取得三連勝。[中國時報](https://web.archive.org/web/20080313155041/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008031000073,00.html)[自由時報](https://web.archive.org/web/20080612144720/http://www.libertytimes.com.tw/2008/new/mar/10/today-sp1.htm)

## [3月8日](../Page/3月8日.md "wikilink")

  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**第二日賽事，[台灣/中華以](../Page/中華成棒隊.md "wikilink")**1:6**擊敗[墨西哥](../Page/墨西哥棒球代表隊.md "wikilink")，拿下二連勝。[中國時報](https://web.archive.org/web/20080314011401/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008030900013,00.html)[蕃薯藤新聞─中央社](http://news.yam.com/cna/sports/200803/20080308713262.html)

## [3月7日](../Page/3月7日.md "wikilink")

  - **[2008年夏季奧運棒球世界區資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")**正式開打，[台灣/中華首戰以](../Page/中華成棒隊.md "wikilink")**13:3**七局提前擊敗[西班牙](../Page/西班牙棒球代表隊.md "wikilink")，獲得首勝。[蕃薯藤新聞─中央社](https://web.archive.org/web/20080313011700/http://sports.yam.com/cna/sports/200803/20080309733996.html)

## [3月6日](../Page/3月6日.md "wikilink")

  - 自開賣後頭彩連續[槓龜](../Page/槓龜.md "wikilink")12期的**[公益彩券威力彩](../Page/台灣彩券.md "wikilink")**開出頭獎，兩名得獎者平分超過**[新台幣10.3億](../Page/新台幣.md "wikilink")**的高額獎金。[中國時報](https://web.archive.org/web/20080409201150/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110503+112008030700437,00.html)

## [3月5日](../Page/3月5日.md "wikilink")

  - [行政院主計處公布](../Page/行政院主計處.md "wikilink")**[2008年](../Page/2008年.md "wikilink")2月[物價指數](../Page/物價.md "wikilink")**，其中以[美元計算的進口物價年增率達到](../Page/美元.md "wikilink")18.99％，創下27年4個月以來的最高紀錄，也是[第二次石油危機以來的新高](../Page/第二次石油危機.md "wikilink")。[中國時報](https://web.archive.org/web/20080410065259/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008030600088,00.html)

## [3月4日](../Page/3月4日.md "wikilink")

  - [空軍](../Page/中華民國空軍.md "wikilink")401聯隊一架**[F-16戰鬥機](../Page/F-16戰隼戰鬥機.md "wikilink")**晚間於[花蓮外海失蹤](../Page/花蓮縣.md "wikilink")，[少校飛官丁世寶下落不明](../Page/少校.md "wikilink")。[蕃薯藤新聞─中廣新聞網](http://news.yam.com/cna/society/200803/20080304638814.html)

## [3月3日](../Page/3月3日.md "wikilink")

  - 關於**[蘇花高速公路](../Page/蘇花高速公路.md "wikilink")**興建案，[環保署環評小組對環境差異評估報告進行審查](../Page/行政院環境保護署.md "wikilink")，並傾向有條件通過。由於蘇花高速公路興建與否尚有爭議，故此消息一出，即造成正反雙方兩極化反應。[蕃薯藤新聞─中央社](http://news.yam.com/cna/life/200803/20080303614702.html)[PChome新聞─中央社](http://news.pchome.com.tw/society/cna/20080304/index-20080304124138180103.html)
  - 假借[小三通名義從](../Page/小三通.md "wikilink")[金門偷渡至](../Page/金門.md "wikilink")[廈門的高中](../Page/廈門.md "wikilink")[高中夜校中輟生](../Page/高中.md "wikilink")**黃少宇**，被[臺北縣](../Page/新北市.md "wikilink")[汐止警方懷疑涉及另一起凶殺案](../Page/汐止區.md "wikilink")，遭警方移送[士林地檢署偵辦](../Page/臺灣士林地方法院檢察署.md "wikilink")。[蕃薯藤新聞─中央社](http://news.pchome.com.tw/society/cna/20080304/index-20080304214408180302.html)[自由時報](https://web.archive.org/web/20080523074217/http://www.libertytimes.com.tw/2008/new/mar/4/today-so4.htm)
  - [中華職棒](../Page/中華職棒.md "wikilink")[La
    New熊隊選手](../Page/La_New熊.md "wikilink")**[陳金鋒](../Page/陳金鋒.md "wikilink")**，由於[腰部舊傷未癒](../Page/腰部.md "wikilink")，經過專科醫師評估後，確定將退出[奧運資格賽](../Page/2008年夏季奧運棒球世界區資格賽.md "wikilink")[台灣代表隊行列](../Page/中華成棒隊.md "wikilink")。[中國時報](http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110501+112008030400104,00.html)[聯合報](http://udn.com/NEWS/SPORTS/SPO5/4242463.shtml)[自由時報](http://www.libertytimes.com.tw/2008/new/mar/4/today-sp5.htm)

## [3月2日](../Page/3月2日.md "wikilink")

  - [羅東](../Page/羅東鎮_\(臺灣\).md "wikilink")[聖母醫院院長](../Page/聖母醫院.md "wikilink")**[呂鴻基](../Page/呂鴻基.md "wikilink")**在[中華民國兒童保健協會舉辦的研討會中](../Page/中華民國兒童保健協會.md "wikilink")，指出[台灣](../Page/台灣.md "wikilink")[少子化情形嚴重](../Page/少子化.md "wikilink")，近六年來未成年人口減少近一成，[生育率只有千分之](../Page/生育率.md "wikilink")1.1，
    為全球最低。[自由時報](https://web.archive.org/web/20080607192830/http://www.libertytimes.com.tw/2008/new/mar/2/today-life9.htm)

## [3月1日](../Page/3月1日.md "wikilink")

  - **國內航線**航班數即日起再度減少，[立榮航空退出](../Page/立榮航空.md "wikilink")[台北](../Page/台北松山機場.md "wikilink")－[高雄線](../Page/高雄國際機場.md "wikilink")、[遠東航空退出台北](../Page/遠東航空.md "wikilink")－[台南及高雄](../Page/台南機場.md "wikilink")－[花蓮線](../Page/花蓮機場.md "wikilink")、[華信航空退出](../Page/華信航空.md "wikilink")[-{台中}-](../Page/台中清泉崗機場.md "wikilink")－[台東線](../Page/台東機場.md "wikilink")，其中**-{台中}-－台東**已無空中交通。[中國時報](https://web.archive.org/web/20080304130222/http://news.chinatimes.com/2007Cti/2007Cti-News/2007Cti-News-Content/0,4521,110503+112008030100400,00.html)[自由時報](https://web.archive.org/web/20080515235736/http://www.libertytimes.com.tw/2008/new/feb/29/today-life6.htm)
  - **[中油](../Page/台灣中油公司.md "wikilink")**表示，依照**[浮動油價機制](../Page/浮動油價機制.md "wikilink")**計算，3月份國內汽、柴油應調漲幅度為12.6%，但自[2007年](../Page/2007年.md "wikilink")9月以來累計漲幅已達26.13%，為配合政府上「累計漲幅達12%時即予凍漲」的政策，因此3月國內汽、柴油價格不予調整。[蕃薯藤新聞─中央社](http://news.yam.com/cna/fn/200803/20080301571688.html)

[台灣新聞動態](../Category/2008年3月.md "wikilink")
[\*03月](../Category/2008年台灣.md "wikilink")