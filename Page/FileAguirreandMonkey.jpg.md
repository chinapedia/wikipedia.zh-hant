## 摘要

## Fair use

It is believed that this image, AguirreandMonkey.jpg, is subject to fair
use in the article *[阿基尔，上帝的愤怒](../Page/阿基尔，上帝的愤怒.md "wikilink")*
because:

1.  This image illustrates the text next to which it appears, which
    describes the film's plot summary and main characters. Plot details
    include specific mention of the film's final sequence, from which
    this shot is taken.
2.  The shot visually illustrates [Klaus
    Kinski](../Page/Klaus_Kinski.md "wikilink") as the title character
    in the article.
3.  It is of much lower resolution than the original (copies made from
    it will be of very inferior quality).
4.  It does not limit the copyright owner's rights to market or sell the
    work in any way.
5.  The image is being used in an informative way and should not detract
    from the original
    work.-[<span style="color: orange">-{冰热海风}-</span>](../Page/User:冰热海风.md "wikilink")([<span style="color: Aqua;">talk</span>](../Page/User_talk:冰热海风.md "wikilink"))
    2008年4月30日 (三) 13:02 (UTC)

## 许可协议