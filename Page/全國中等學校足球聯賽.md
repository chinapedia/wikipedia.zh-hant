{{ infobox football league | name = 全國中等學校足球聯賽
Highschool Football League | another_name = | image = | pixels = |
country =  [中華民國](../Page/中華民國.md "wikilink") | confed = 中華民國高級中等學校體育總會
| founded = 2006年 | first = 2006年 | teams = | relegation = | level = |
champions = 高男組︰[花蓮高農](../Page/花蓮高農.md "wikilink")
高女組︰[醒吾高中](../Page/醒吾高中.md "wikilink")
國男組︰美崙國中
國女組︰[瑞祥中學](../Page/瑞祥中學.md "wikilink") | most_successful_club = |
website = | current = }}
**高中足球聯賽**創立於2006年，由中華民國高級中等學校體育總會主辦，目前共分高中男子組、高中女子組、國中男子組與國中女子組四組。

## 歷史

高中足球聯賽由中華民國足球協會於2006年創立，第一屆高中足球聯賽僅有高中男子組，並限制參賽隊伍數為八隊，其中四個名額由前一年的青年盃前四名隊伍取得，剩餘四個名額由額外的資格賽決定，聯賽採雙循環主客場決勝制，最終由[北門高中奪得第一屆冠軍](../Page/北門高中.md "wikilink")。

第二屆聯賽規定大致與第一屆相同，但在競賽方式改變為兩回合單循環制，第一回合單循環後淘汰四支隊伍，剩餘的四支隊伍在第二回合的比賽中角逐冠軍。

2008年起，聯賽改為由中華民國高級中等學校體育總會主辦，除了原有的高中男子組，聯賽擴大到高中女子組、國中男子組與國中女子組。

## 歷屆成績

### 高中男子組

| 球季                                                    | 冠軍                                     | 亞軍                                     | 季軍                                           | 殿軍                                           | 第五名                                          | 第六名                                    | 第七名                                    | 第八名                                          |
| ----------------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------------- | -------------------------------------------- | -------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------------- |
| 2006年 *[詳情](../Page/2006年高中足球聯賽.md "wikilink")*       | [北門高中](../Page/北門高中.md "wikilink")(台南) | [新北高中](../Page/新北高中.md "wikilink")(新北) | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮)       | [中正高工](../Page/中正高工.md "wikilink")(高雄)       | [新豐高中](../Page/新豐高中.md "wikilink")(台南) | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [泰北高中](../Page/泰北高中.md "wikilink")(台北)       |
| 2007年 *[詳情](../Page/2007年高中足球聯賽.md "wikilink")*       | [北門高中](../Page/北門高中.md "wikilink")(台南) | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭) | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) | [泰北高中](../Page/泰北高中.md "wikilink")(台北)       |                                              |                                        |                                        |                                              |
| 96學年度 *[詳情](../Page/96學年度全國中等學校足球聯賽.md "wikilink")*   | [路竹高中](../Page/路竹高中.md "wikilink")(高雄) | [北門高中](../Page/北門高中.md "wikilink")(台南) | [泰北高中](../Page/泰北高中.md "wikilink")(台北)       | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮)       | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [新豐高中](../Page/新豐高中.md "wikilink")(台南) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮) | [惠文高中](../Page/惠文高中.md "wikilink")(台中)       |
| 97學年度 *[詳情](../Page/97學年度全國中等學校足球聯賽.md "wikilink")*   | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [惠文高中](../Page/惠文高中.md "wikilink")(台中) | [北門高中](../Page/北門高中.md "wikilink")(台南)       | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [路竹高中](../Page/路竹高中.md "wikilink")(高雄)       | [新北高中](../Page/新北高中.md "wikilink")(新北) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮) | [新豐高中](../Page/新豐高中.md "wikilink")(台南)       |
| 98學年度 *[詳情](../Page/98學年度全國中等學校足球聯賽.md "wikilink")*   | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭) | [惠文高中](../Page/惠文高中.md "wikilink")(台中) | [北門高中](../Page/北門高中.md "wikilink")(台南)       | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮)       | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮)       | [路竹高中](../Page/路竹高中.md "wikilink")(高雄) | [中正高工](../Page/中正高工.md "wikilink")(高雄) | [新北高中](../Page/新北高中.md "wikilink")(新北)       |
| 99學年度 *[詳情](../Page/99學年度全國中等學校足球聯賽.md "wikilink")*   | [北門高中](../Page/北門高中.md "wikilink")(台南) | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮)       | [惠文高中](../Page/惠文高中.md "wikilink")(台中)       | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [路竹高中](../Page/路竹高中.md "wikilink")(高雄) | [新北高中](../Page/新北高中.md "wikilink")(新北) | [中山工商](../Page/中山工商.md "wikilink")(高雄)       |
| 100學年度 *[詳情](../Page/100學年度全國中等學校足球聯賽.md "wikilink")* | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭) | [北門高中](../Page/北門高中.md "wikilink")(台南) | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮)       | [路竹高中](../Page/路竹高中.md "wikilink")(高雄)       | [惠文高中](../Page/惠文高中.md "wikilink")(台中)       | [新北高中](../Page/新北高中.md "wikilink")(新北) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮) | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) |
| 101學年度 *[詳情](../Page/101學年度全國中等學校足球聯賽.md "wikilink")* | [惠文高中](../Page/惠文高中.md "wikilink")(台中) | [北門高中](../Page/北門高中.md "wikilink")(台南) | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮)       | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) | [新北高中](../Page/新北高中.md "wikilink")(新北) | [路竹高中](../Page/路竹高中.md "wikilink")(高雄) | [台東專校](../Page/台東專校.md "wikilink")(台東)       |
| 102學年度 *[詳情](../Page/102學年度全國中等學校足球聯賽.md "wikilink")* | [北門高中](../Page/北門高中.md "wikilink")(台南) | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [惠文高中](../Page/惠文高中.md "wikilink")(台中)       | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮)       | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [路竹高中](../Page/路竹高中.md "wikilink")(高雄) | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) |
| 103學年度 *[詳情](../Page/103學年度全國中等學校足球聯賽.md "wikilink")* | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [中正高工](../Page/中正高工.md "wikilink")(高雄) | [北門高中](../Page/北門高中.md "wikilink")(台南)       | [中山工商](../Page/中山工商.md "wikilink")(高雄)       | [惠文高中](../Page/惠文高中.md "wikilink")(台中)       | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭) | [路竹高中](../Page/路竹高中.md "wikilink")(高雄) | [強恕高中](../Page/強恕高中.md "wikilink")(台北)       |
| 104學年度 *[詳情](../Page/104學年度全國中等學校足球聯賽.md "wikilink")* | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [北門高中](../Page/北門高中.md "wikilink")(台南) | [中山工商](../Page/中山工商.md "wikilink")(高雄)       | [惠文高中](../Page/惠文高中.md "wikilink")(台中)       | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮) | [路竹高中](../Page/路竹高中.md "wikilink")(高雄)       |
| 105學年度 *[詳情](../Page/105學年度全國中等學校足球聯賽.md "wikilink")* | [惠文高中](../Page/惠文高中.md "wikilink")(台中) | [北門高中](../Page/北門高中.md "wikilink")(台南) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮)       | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [路竹高中](../Page/路竹高中.md "wikilink")(高雄)       |
| 106學年度 *[詳情](../Page/106學年度全國中等學校足球聯賽.md "wikilink")* | [清水高中](../Page/清水高中.md "wikilink")(新北) | [惠文高中](../Page/惠文高中.md "wikilink")(台中) | [花蓮高中](../Page/花蓮高中.md "wikilink")(花蓮)       | [路竹高中](../Page/路竹高中.md "wikilink")(高雄)       | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       | [花蓮高農](../Page/花蓮高農.md "wikilink")(花蓮) | [中正高中](../Page/中正高中.md "wikilink")(台北) | [宜蘭高中](../Page/宜蘭高中.md "wikilink")(宜蘭)       |

### 高中女子組

| 球季                                                    | 冠軍                                     | 亞軍                                     | 季軍                                     | 殿軍                                     | 第五名                                    | 第六名                                    | 第七名 | 第八名 |
| ----------------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | --- | --- |
| 96學年度 *[詳情](../Page/96學年度全國中等學校足球聯賽.md "wikilink")*   | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [瑞祥高中](../Page/瑞祥高中.md "wikilink")(高雄) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) |                                        |                                        |                                        |     |     |
| 97學年度 *[詳情](../Page/97學年度全國中等學校足球聯賽.md "wikilink")*   | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [瑞祥高中](../Page/瑞祥高中.md "wikilink")(高雄) |                                        |                                        |                                        |     |     |
| 98學年度 *[詳情](../Page/98學年度全國中等學校足球聯賽.md "wikilink")*   | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [瑞祥高中](../Page/瑞祥高中.md "wikilink")(高雄) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [三重商工](../Page/三重商工.md "wikilink")(新北) |                                        |                                        |     |     |
| 99學年度 *[詳情](../Page/99學年度全國中等學校足球聯賽.md "wikilink")*   | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [瑞祥高中](../Page/瑞祥高中.md "wikilink")(高雄) | [三重高中](../Page/三重高中.md "wikilink")(新北) | [弘德工商](../Page/弘德工商.md "wikilink")(嘉義) |     |     |
| 100學年度 *[詳情](../Page/100學年度全國中等學校足球聯賽.md "wikilink")* | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [三重高中](../Page/三重高中.md "wikilink")(新北) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [永慶高中](../Page/永慶高中.md "wikilink")(嘉義) |                                        |     |     |
| 101學年度 *[詳情](../Page/101學年度全國中等學校足球聯賽.md "wikilink")* | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [永慶高中](../Page/永慶高中.md "wikilink")(嘉義) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [楠梓高中](../Page/楠梓高中.md "wikilink")(高雄) |                                        |     |     |
| 102學年度 *[詳情](../Page/102學年度全國中等學校足球聯賽.md "wikilink")* | [永慶高中](../Page/永慶高中.md "wikilink")(嘉義) | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [楠梓高中](../Page/楠梓高中.md "wikilink")(高雄) | [中山工商](../Page/中山工商.md "wikilink")(高雄) |                                        |     |     |
| 103學年度 *[詳情](../Page/103學年度全國中等學校足球聯賽.md "wikilink")* | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [永慶高中](../Page/永慶高中.md "wikilink")(嘉義) |                                        |                                        |     |     |
| 104學年度 *[詳情](../Page/104學年度全國中等學校足球聯賽.md "wikilink")* | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [永慶高中](../Page/永慶高中.md "wikilink")(嘉義) |                                        |                                        |     |     |
| 105學年度 *[詳情](../Page/105學年度全國中等學校足球聯賽.md "wikilink")* | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) |                                        |                                        |                                        |     |     |
| 106學年度 *[詳情](../Page/106學年度全國中等學校足球聯賽.md "wikilink")* | [中山工商](../Page/中山工商.md "wikilink")(高雄) | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [花蓮體中](../Page/花蓮體中.md "wikilink")(花蓮) | [永慶高中](../Page/永慶高中.md "wikilink")(嘉義) | [陸興高中](../Page/陸興高中.md "wikilink")(屏東) |                                        |     |     |

### 國中男子組

| 球季                                                    | 冠軍                                           | 亞軍                                     | 季軍                                     | 殿軍                                           | 第五名                                    | 第六名                                    | 第七名                                    | 第八名                                          |
| ----------------------------------------------------- | -------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------------- |
| 96學年度 *[詳情](../Page/96學年度全國中等學校足球聯賽.md "wikilink")*   | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮)       | [黎明國中](../Page/黎明國中.md "wikilink")(台中) | [佳里國中](../Page/佳里國中.md "wikilink")(台南) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄)       | [安和國中](../Page/安和國中.md "wikilink")(台中) | [順安國中](../Page/順安國中.md "wikilink")(宜蘭) | [文賢國中](../Page/文賢國中.md "wikilink")(台南) | [東港國中](../Page/東港國中.md "wikilink")(屏東)       |
| 97學年度 *[詳情](../Page/97學年度全國中等學校足球聯賽.md "wikilink")*   | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮)       | [文賢國中](../Page/文賢國中.md "wikilink")(台南) | [後勁國中](../Page/後勁國中.md "wikilink")(高雄) | [明志國中](../Page/明志國中.md "wikilink")(新北)       | [黎明國中](../Page/黎明國中.md "wikilink")(台中) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄) | [東港國中](../Page/東港國中.md "wikilink")(屏東) | [安和國中](../Page/安和國中.md "wikilink")(台中)       |
| 98學年度 *[詳情](../Page/98學年度全國中等學校足球聯賽.md "wikilink")*   | [黎明國中](../Page/黎明國中.md "wikilink")(台中)       | [化仁國中](../Page/化仁國中.md "wikilink")(花蓮) | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄)       | [復興國中](../Page/復興國中.md "wikilink")(宜蘭) | [來義國中](../Page/來義國中.md "wikilink")(屏東) | [文賢國中](../Page/文賢國中.md "wikilink")(台南) | [後甲國中](../Page/後甲國中.md "wikilink")(台南)       |
| 99學年度 *[詳情](../Page/99學年度全國中等學校足球聯賽.md "wikilink")*   | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) | [佳里國中](../Page/佳里國中.md "wikilink")(台南) | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮) | [後勁國中](../Page/後勁國中.md "wikilink")(高雄)       | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄) | [東海國中](../Page/東海國中.md "wikilink")(台東) | [順安國中](../Page/順安國中.md "wikilink")(宜蘭) | [新港國中](../Page/新港國中.md "wikilink")(嘉義)       |
| 100學年度 *[詳情](../Page/100學年度全國中等學校足球聯賽.md "wikilink")* | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮)       | [黎明國中](../Page/黎明國中.md "wikilink")(台中) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄) | [鳳西國中](../Page/鳳西國中.md "wikilink")(高雄)       | [順安國中](../Page/順安國中.md "wikilink")(宜蘭) | [佳里國中](../Page/佳里國中.md "wikilink")(台南) | [後勁國中](../Page/後勁國中.md "wikilink")(高雄) | [後甲國中](../Page/後甲國中.md "wikilink")(台南)       |
| 101學年度 *[詳情](../Page/101學年度全國中等學校足球聯賽.md "wikilink")* |                                              |                                        |                                        |                                              |                                        |                                        |                                        |                                              |
| 102學年度 *[詳情](../Page/102學年度全國中等學校足球聯賽.md "wikilink")* | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮)       | [順安國中](../Page/順安國中.md "wikilink")(宜蘭) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄) | [化仁國中](../Page/化仁國中.md "wikilink")(花蓮)       | [黎明國中](../Page/黎明國中.md "wikilink")(台中) | [復興國中](../Page/復興國中.md "wikilink")(宜蘭) | [東海國中](../Page/東海國中.md "wikilink")(台東) | [南澳高中](../Page/南澳高中.md "wikilink")(宜蘭)       |
| 103學年度 *[詳情](../Page/103學年度全國中等學校足球聯賽.md "wikilink")* | [黎明國中](../Page/黎明國中.md "wikilink")(台中)       | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮) | [化仁國中](../Page/化仁國中.md "wikilink")(花蓮) | [右昌高中](../Page/右昌高中.md "wikilink")(高雄)       | [阿蓮高中](../Page/阿蓮高中.md "wikilink")(高雄) | [復興高中](../Page/復興高中.md "wikilink")(宜蘭) | [後甲高中](../Page/後甲高中.md "wikilink")(台南) | [清水高中](../Page/新北市立清水高級中學.md "wikilink")(新北) |
| 104學年度 *[詳情](../Page/104學年度全國中等學校足球聯賽.md "wikilink")* | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮)       | [東海國中](../Page/東海國中.md "wikilink")(台東) | [黎明國中](../Page/黎明國中.md "wikilink")(台中) | [民族國中](../Page/臺北市立民族國民中學.md "wikilink")(台北) | [右昌國中](../Page/右昌國中.md "wikilink")(高雄) | [阿蓮高中](../Page/阿蓮高中.md "wikilink")(高雄) | [鳳西國中](../Page/鳳西國中.md "wikilink")(高雄) | [化仁國中](../Page/化仁國中.md "wikilink")(花蓮)       |
| 105學年度 *[詳情](../Page/105學年度全國中等學校足球聯賽.md "wikilink")* | [黎明國中](../Page/黎明國中.md "wikilink")(台中)       | [民族國中](../Page/民族國中.md "wikilink")(台北) | [右昌國中](../Page/右昌國中.md "wikilink")(高雄) | [鳳西國中](../Page/鳳西國中.md "wikilink")(高雄)       | [後甲國中](../Page/後甲國中.md "wikilink")(台南) | [潭秀國中](../Page/潭秀國中.md "wikilink")(台中) | [明志國中](../Page/明志國中.md "wikilink")(新北) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄)       |
| 106學年度 *[詳情](../Page/106學年度全國中等學校足球聯賽.md "wikilink")* | [民族國中](../Page/民族國中.md "wikilink")(台北)       | [化仁國中](../Page/化仁國中.md "wikilink")(花蓮) | [黎明國中](../Page/黎明國中.md "wikilink")(台中) | [美崙國中](../Page/美崙國中.md "wikilink")(花蓮)       | [右昌國中](../Page/右昌國中.md "wikilink")(高雄) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄) | [前鎮國中](../Page/前鎮國中.md "wikilink")(高雄) | [天母國中](../Page/天母國中.md "wikilink")(台北)       |

### 國中女子組

| 球季                                                    | 冠軍                                     | 亞軍                                     | 季軍                                           | 殿軍                                     | 第五名                                    | 第六名                                    | 第七名                                    | 第八名                                    |
| ----------------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- |
| 96學年度 *[詳情](../Page/96學年度全國中等學校足球聯賽.md "wikilink")*   | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [明志國中](../Page/明志國中.md "wikilink")     | [鳳西國中](../Page/鳳西國中.md "wikilink")           | [來義國中](../Page/來義國中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")     | [東里國中](../Page/東里國中.md "wikilink")     |                                        |                                        |
| 97學年度 *[詳情](../Page/97學年度全國中等學校足球聯賽.md "wikilink")*   | [鳳西國中](../Page/鳳西國中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")           | [明志國中](../Page/明志國中.md "wikilink")     | [新港國中](../Page/新港國中.md "wikilink")     | [來義國中](../Page/來義國中.md "wikilink")     |                                        |                                        |
| 98學年度 *[詳情](../Page/98學年度全國中等學校足球聯賽.md "wikilink")*   | [鳳西國中](../Page/鳳西國中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")           | [東石國中](../Page/東石國中.md "wikilink")     | [右昌國中](../Page/右昌國中.md "wikilink")     | [新港國中](../Page/新港國中.md "wikilink")     | [東里國中](../Page/東里國中.md "wikilink")     | [明志國中](../Page/明志國中.md "wikilink")     |
| 99學年度 *[詳情](../Page/99學年度全國中等學校足球聯賽.md "wikilink")*   | [鳳西國中](../Page/鳳西國中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [東石國中](../Page/東石國中.md "wikilink")           | [南澳國中](../Page/南澳國中.md "wikilink")     | [右昌國中](../Page/右昌國中.md "wikilink")     | [東里國中](../Page/東里國中.md "wikilink")     | [明志國中](../Page/明志國中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")     |
| 100學年度 *[詳情](../Page/100學年度全國中等學校足球聯賽.md "wikilink")* | [右昌國中](../Page/右昌國中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")           | [鳳西國中](../Page/鳳西國中.md "wikilink")     | [後勁國中](../Page/後勁國中.md "wikilink")     | [東石國中](../Page/東石國中.md "wikilink")     | [南澳高中](../Page/南澳高中.md "wikilink")     | [明志國中](../Page/明志國中.md "wikilink")     |
| 101學年度 *[詳情](../Page/101學年度全國中等學校足球聯賽.md "wikilink")* | [鳳西國中](../Page/鳳西國中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [東石國中](../Page/東石國中.md "wikilink")           | [光復國中](../Page/光復國中.md "wikilink")     | [明志國中](../Page/明志國中.md "wikilink")     | [右昌國中](../Page/右昌國中.md "wikilink")     |                                        |                                        |
| 102學年度 *[詳情](../Page/102學年度全國中等學校足球聯賽.md "wikilink")* | [光復國中](../Page/光復國中.md "wikilink")     | [醒吾中學](../Page/醒吾中學.md "wikilink")     | [鳳西國中](../Page/鳳西國中.md "wikilink")           | [東石國中](../Page/東石國中.md "wikilink")     | [臺東體中](../Page/臺東體中.md "wikilink")     | [右昌國中](../Page/右昌國中.md "wikilink")     |                                        |                                        |
| 103學年度 *[詳情](../Page/103學年度全國中等學校足球聯賽.md "wikilink")* | [鳳西國中](../Page/鳳西國中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [臺東體中](../Page/國立臺東大學附屬體育高級中學.md "wikilink") | [瑞祥高中](../Page/瑞祥高中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")     | [東石國中](../Page/東石國中.md "wikilink")     |                                        |                                        |
| 104學年度 *[詳情](../Page/104學年度全國中等學校足球聯賽.md "wikilink")* | [瑞祥高中](../Page/瑞祥高中.md "wikilink")     | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [臺東體中](../Page/臺東體中.md "wikilink")           | [東石國中](../Page/東石國中.md "wikilink")     | [右昌國中](../Page/右昌國中.md "wikilink")     | [五權國中](../Page/五權國中.md "wikilink")     |                                        |                                        |
| 105學年度 *[詳情](../Page/105學年度全國中等學校足球聯賽.md "wikilink")* | [醒吾高中](../Page/醒吾高中.md "wikilink")     | [五權國中](../Page/五權國中.md "wikilink")     | [光復國中](../Page/光復國中.md "wikilink")           | [右昌國中](../Page/右昌國中.md "wikilink")     |                                        |                                        |                                        |                                        |
| 106學年度 *[詳情](../Page/106學年度全國中等學校足球聯賽.md "wikilink")* | [醒吾高中](../Page/醒吾高中.md "wikilink")(新北) | [瑞祥國中](../Page/瑞祥國中.md "wikilink")(高雄) | [五權國中](../Page/五權國中.md "wikilink")(台中)       | [光復國中](../Page/光復國中.md "wikilink")(花蓮) | [秀林國中](../Page/秀林國中.md "wikilink")(花蓮) | [右昌國中](../Page/右昌國中.md "wikilink")(高雄) | [復興國中](../Page/復興國中.md "wikilink")(宜蘭) | [阿蓮國中](../Page/阿蓮國中.md "wikilink")(高雄) |

## 聯賽紀錄

<small>更新日期:2007年5月28日</small>

**奪次數最多球隊**:[北門高中](../Page/北門高中.md "wikilink")（2次）

**積分最多球隊**: 25分—[北門高中](../Page/北門高中.md "wikilink")（2006年）

**進球最多球隊**: [北門高中](../Page/北門高中.md "wikilink")—26球(2006年)

**失球最多球隊**: [中正高工](../Page/中正高工.md "wikilink")—38球(2006年)

**紀律最差球隊**: [花蓮高中](../Page/花蓮高中.md "wikilink")—11張黃牌/3張紅牌 (2006年)

**單場進球最多比賽**:[花蓮高中](../Page/花蓮高中.md "wikilink") 對
[中正高工](../Page/中正高工.md "wikilink") 比分9-1(2006年4月14日)

**最高比分**:[花蓮高中](../Page/花蓮高中.md "wikilink") 對
[中正高工](../Page/中正高工.md "wikilink") 比分9-1(2006年4月14日)

**連勝紀錄**:[三重高中](../Page/三重高中.md "wikilink") 6連勝 從 2006年2月16日 到 4月7日

**連敗紀錄**:[泰北高中](../Page/泰北高中.md "wikilink") 6連敗 從 2006年2月16日 到 4月7日

**連續不敗紀錄**:[三重高中](../Page/三重高中.md "wikilink") 7連不敗 2006年2月16日 到 4月14日

**進球最多球員**: [陳柏良](../Page/陳柏良.md "wikilink")
([中正高工](../Page/中正高工.md "wikilink")) 11球

**單場比賽進球最多球員**:[林京贊](../Page/林京贊.md "wikilink")([花蓮高中](../Page/花蓮高中.md "wikilink"))一場進4球,2006年4月3日在客場
對 [泰北高中](../Page/泰北高中.md "wikilink"),比分1-5

## 外部連結

  - [HFL高中足球聯賽](https://web.archive.org/web/20061216142235/http://www.ltsports.com.tw/HFL/default.asp)
  - [中華民國高級中等學校體育總會](https://web.archive.org/web/20111102143505/http://www.shssf.edu.tw/index.php)

[Category:台灣足球賽事](../Category/台灣足球賽事.md "wikilink")
[Category:臺灣體育教育](../Category/臺灣體育教育.md "wikilink")
[Category:台灣國民教育](../Category/台灣國民教育.md "wikilink")