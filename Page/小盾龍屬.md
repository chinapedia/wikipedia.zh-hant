**小盾龍屬**（屬名：*Scutellosaurus*）意為“有小盾的[蜥蜴](../Page/蜥蜴.md "wikilink")”，是[草食性](../Page/草食性.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，約2億年前到1億9600萬年前。小盾龍被分類於[裝甲亞目](../Page/裝甲亞目.md "wikilink")，是該亞目最早期的物種之一，是目前已知最原始的裝甲類恐龍。與小盾龍最親近的物種應為[腿龍](../Page/腿龍.md "wikilink")，腿龍僅能以四足行走，而小盾龍是可以二足行走或奔跑。
[Scutellosaurus1.jpg](https://zh.wikipedia.org/wiki/File:Scutellosaurus1.jpg "fig:Scutellosaurus1.jpg")
小盾龍身長約1.2公尺，臀部高度為0.5公尺，重達10公斤。牠們擁有小型頭顱骨、長身體、纖細四肢、後肢比前肢長、臀部寬、長尾巴。小盾龍的化石包括兩個發現於[亞利桑那州的部份骨骸](../Page/亞利桑那州.md "wikilink")，但頭顱骨部分只有發現下頷，牠們可用簡單[頰齒來切斷](../Page/頰齒.md "wikilink")、咬碎植物。小盾龍的頸部到背部、體側、尾巴，覆蓋者約300個鱗甲，最大的鱗甲排列在背部中線1、2排。

## 參考資料

  - *The Illustrated Dinosaur Encyclopedia*, D. Dixon, 1998 (ISBN
    978-0-7064-3238-1)

  -
  - Eyewitness Handbook:Dinosaur and Prehistoric Life, David Norman
    (ISBN 978-986-7879-81-3)

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:裝甲亞目](../Category/裝甲亞目.md "wikilink")