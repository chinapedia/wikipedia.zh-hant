**雨豆樹**（學名：），原產地為[熱帶美洲](../Page/熱帶美洲.md "wikilink")，[西印度](../Page/西印度.md "wikilink")。它是[含羞草科](../Page/含羞草科.md "wikilink")、[落葉喬木](../Page/落葉喬木.md "wikilink")，其樹冠呈傘形，為優雅之庭園綠蔭樹、[行道樹](../Page/行道樹.md "wikilink")。

## 特徵

大喬木，株高可達20[公尺](../Page/公尺.md "wikilink")，樹皮有剝落木栓層，灰褐色，冬季時會大量落葉，樹冠很大，可達15公尺，二回羽狀複葉，互生，2-4羽片；小葉2-8對，表面滑澤，背有絨毛，斜形，卵狀長橢圓形或略圓形，長可達4.5[公分](../Page/公分.md "wikilink")。兩性花，花形小；頭狀花序，總梗長10-12.5公分，花冠淡粉紅色，長1.7公分，被絲狀長絨毛，花瓣鑷合狀排列；雄蕊單體，20枚，深紅色，基部合生甚短，花藥2室，直裂，具腺冠，春天開花；子房上位。花藥頂端具一脫落性腺體，稱為「腺冠」。木質莢果扁平或略圓柱形，長15-20公分，寬17-2.5公分，堅硬，不開裂，種子間有隔膜；種子6\~10粒，棕色，不規則形，直徑約1公分。

## 圖集

<File:Bark-> Black-rumped Flameback I IMG
9087.jpg|雨豆樹上的[黑腰啄木鳥](../Page/黑腰啄木鳥.md "wikilink")
<File:Pods> I IMG 3110.jpg|木質莢果 <File:Flower> & flower buds- Samanea
saman I IMG 3407.jpg|雨豆樹的花 <File:Albizia> saman (Raintree) (2).jpg
<File:Albizia> saman (Raintree) (3).jpg <File:Albizia> saman (Raintree)
(4).jpg <File:Albizia> saman (Raintree) (5).jpg <File:Albizia> saman
(Raintree) (6).jpg <File:Albizia> saman (Raintree) (7).jpg
<File:Albizia> saman (Raintree) (8).jpg <File:Albizia> saman (Raintree)
(9).jpg <File:Albizia> saman (Raintree) (10).jpg <File:Albizia> saman
(Raintree) (11).jpg <File:Albizia> saman (Raintree) (12).jpg
<File:Albizia> saman (Raintree) (13).jpg <File:Albizia> saman (Raintree)
(14).jpg <File:Albizia> saman (Raintree) (15).jpg <File:Albizia> saman
(Raintree) (17).jpg <File:Albizia> saman (Raintree) (18).jpg
Image:During_falling_leaf_period_Samanea_saman.jpg Image:Bark-
Black-rumped Flameback I IMG 9087.jpg Image:Samanea saman
hbfjxdh,xnhjkgRain Tree
[Ranchi](../Page/Ranchi.md "wikilink"),
[Jharkhand](../Page/Jharkhand.md "wikilink") Image:Pods I IMG 3110.jpg
Image:AlbiziaSaman.jpg <File:Albizia> saman.jpg

## 外部連結

  - [Hawaiian Ecosystems at Risk Project page for *Samanea
    saman*](http://www.hear.org/Pier/species/samanea_saman.htm)
  - [Reference for Telugu word Nidra
    Ganneru](https://web.archive.org/web/20090815145350/http://forest.ap.nic.in/Silviculture%20of%20Species/Forest%20Seeds/078.htm)

[Category:合欢属](../Category/合欢属.md "wikilink")