**Dotdash.com**（、前稱：**The Mining Company**、**MiningCo.com,
Inc.**、**About,
Inc.**、**About.com**），[美國的](../Page/美國.md "wikilink")[門戶網站](../Page/門戶網站.md "wikilink")，建立於1997年4月21日，擁有[Very
Well](../Page/Very_Well.md "wikilink")、[The
Balance](../Page/The_Balance.md "wikilink")、[Lifewire](../Page/Lifewire.md "wikilink")、[The
Spruce](../Page/The_Spruce.md "wikilink")、[Trip
Savvy](../Page/Trip_Savvy.md "wikilink")、[ThoughtCo](../Page/ThoughtCo.md "wikilink")、[Signal
Labs](../Page/Signal_Labs.md "wikilink")。\[1\]

Dotdash.com為網民提供各種各樣的生活瑣碎問題的解答，包括時尚、美食、家居、健康、興趣、寵物、數碼、旅遊、教育、運動、玩具、汽車、遊戲、財經等方面最新的資訊信息，透過由特選的內容專家提供有償的「指南」，從而形成一個又一個專題的網上社群。為方便管理這些社群頁面，這些頁面又會再組合成為一個一個的「頻道」。在這些網上社群中，用戶得以分享他們的經驗，而這些經驗亦成為了每個網上社群頁面的內容。而另一方面，作為網絡社群領導者的專題專家，亦會從網上搜集其他文章、互動小測驗等，以豐富網上社群網頁的內容。

## 参考文獻

## 外部連結

  - [Dotdash.com](https://www.dotdash.com/)

  - [阿邦網](https://web.archive.org/web/20170724200405/http://abang.com/)

[Category:IAC (公司)](../Category/IAC_\(公司\).md "wikilink")
[Category:美國網站](../Category/美國網站.md "wikilink")
[Category:1997年建立的網站](../Category/1997年建立的網站.md "wikilink")

1.  <http://news.west.cn/18223.html>