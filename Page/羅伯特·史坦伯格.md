**羅伯特·杰弗雷·史坦伯格**（，，又譯**羅伯特·杰弗雷·史登伯格**）美國心理學家，現時[發展心理學的權威](../Page/發展心理學.md "wikilink")。他個人及與其他人合作撰寫的心理學教科書不單是很多學校指定的教科書，他另外有關[創造力及人性發展理論的書籍](../Page/創造力.md "wikilink")，亦成為了現時商界開發人力資源的必讀參考書。他現時是[塔夫茨大學文理學院的院長](../Page/塔夫茨大學.md "wikilink")，之前曾任[美國](../Page/美國.md "wikilink")[耶魯大學心理及教育學系的](../Page/耶魯大學.md "wikilink")[IBM講座教授](../Page/IBM講座教授.md "wikilink")。

## 外部連結

  - [Robert J. Sternberg's
    Homepage](https://web.archive.org/web/20060423072851/http://www.yale.edu/rjsternberg/)
    (Yale University)
  - [Robert J. Sternberg - Dean of the School of Arts and Sciences -
    Tufts
    University](https://web.archive.org/web/20060526204104/http://provost.tufts.edu/academic/deans/sternberg/)
    (Tufts profile)
  - [His Triarchic Theory of Intelligence -
    uwsp.edu](https://web.archive.org/web/20060204205440/http://www.uwsp.edu/education/lwilson/learning/sternb1.htm)

[Category:教育心理學家](../Category/教育心理學家.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")