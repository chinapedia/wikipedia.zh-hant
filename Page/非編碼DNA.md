**非編碼DNA**（，或称“垃圾DNA”），是指不包含製造[蛋白質的](../Page/蛋白質.md "wikilink")[指令](../Page/遺傳密碼.md "wikilink")，或是只能製造出無[轉譯能力](../Page/轉譯.md "wikilink")[RNA的](../Page/RNA.md "wikilink")[DNA序列](../Page/DNA.md "wikilink")。此類DNA在[真核生物的](../Page/真核生物.md "wikilink")[基因組中佔有大多數](../Page/基因組.md "wikilink")。有很长的一段时间科学家们认为这些非编码DNA没有作用，因此，这些重复的DNA片段曾被冠以垃圾DNA的称号。随着时间的推移，科学家们对垃圾DNA的认识逐渐深入，慢慢地发现其实很多垃圾DNA有着其独特的作用，它们在基因剪切等方面起重要的作用。

近年的證據顯示，非編碼DNA可被編碼DNA製造出來的蛋白質利用。一項實驗發現某些非編碼DNA與編碼DNA同樣重要，因為當植物體內某一段非編碼DNA遭受損傷時，會使葉片結構發生顯著改變，原因在於某些與葉片結構有關的蛋白質，需要來自[內含子的訊息](../Page/內含子.md "wikilink")。有些非編碼DNA不含蛋白質製造指令，卻是可以控制基因何時何地表現的基因開關。

此外，有些非編碼DNA，可能是一些[RNA病毒感染後所遺留下來的痕跡](../Page/RNA病毒.md "wikilink")。

## 參考文獻

  -
  -
## 外部链接

  - [Animal Genome Size Database](http://www.genomesize.com/)
  - [Plant DNA C-values
    Database](https://web.archive.org/web/20050901105257/http://www.rbgkew.org.uk/cval/homepage.html)
  - [Fungal Genome Size
    Database](http://www.zbi.ee/fungal-genomesize/index.php)

[Category:DNA](../Category/DNA.md "wikilink")
[Category:遗传学](../Category/遗传学.md "wikilink")
[Category:基因表現](../Category/基因表現.md "wikilink")