**早島町**（）是位于日本[岡山縣南部的一個](../Page/岡山縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")。轄區完全被岡山縣的兩大城市[岡山市和](../Page/岡山市.md "wikilink")[倉敷市所包圍](../Page/倉敷市.md "wikilink")，因此也是兩市的[住宅城市](../Page/住宅城市.md "wikilink")。因此雖然該町是岡山縣面積內最小的行政區劃，但人口密度卻是全縣最高。

## 歷史

在[室町時代以前](../Page/室町時代.md "wikilink")，轄區內北部的丘陵地為位於[穴海內名為](../Page/穴海.md "wikilink")「早島」的島嶼，在[江戶時代經歷大規模](../Page/江戶時代.md "wikilink")[围垦將穴海填平後](../Page/围垦.md "wikilink")，早島變成為[岡山平原的一部份](../Page/岡山平原.md "wikilink")。\[1\]

在江戶時代早島是[旗本早島戶川氏](../Page/旗本.md "wikilink")3千石的領地，並設有[陣屋](../Page/陣屋.md "wikilink")。\[2\]

### 年表

  - 1889年6月1日：實施[町村制](../Page/町村制.md "wikilink")，早島村、矢尾村、前潟村合併為新設置的**早島村**，隸屬[都宇郡](../Page/都宇郡.md "wikilink")。
  - 1896年2月26日：早島村改制為**早島町**。\[3\]
  - 1900年4月1日：都宇郡和[窪屋郡合併為](../Page/窪屋郡.md "wikilink")[都窪郡](../Page/都窪郡.md "wikilink")。

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [宇野線](../Page/宇野線.md "wikilink")（[瀨戶大橋線](../Page/瀨戶大橋線.md "wikilink")）：（[岡山市](../Page/岡山市.md "wikilink")）
        - [早島車站](../Page/早島車站.md "wikilink") -
        [久久原車站](../Page/久久原車站.md "wikilink") -
        （[倉敷市](../Page/倉敷市.md "wikilink")→）

### 道路

  - [高速道路](../Page/高速道路.md "wikilink")

<!-- end list -->

  - [瀨戶中央自動車道](../Page/瀨戶中央自動車道.md "wikilink")：[早島交流道](../Page/早島交流道.md "wikilink")

## 姊妹、友好都市

### 日本

  - [千丁町](../Page/千丁町.md "wikilink")（[熊本縣](../Page/熊本縣.md "wikilink")[八代郡](../Page/八代郡.md "wikilink")）
      -
        兩地於1978年4月13日締結為姊妹都市，但2005年千丁町合併為[八代市後](../Page/八代市.md "wikilink")，兩地即未再繼續締結。

## 參考資料

## 外部連結

  - [早島町觀光中心](http://www.town.hayashima.lg.jp/kankoucenter/kankoutop.html)

  - [早島町民總合會館](http://www.town.hayashima.lg.jp/yurubinoya)

  - [都窪商工會](http://www.tsukubo.net/)

<!-- end list -->

1.
2.
3.