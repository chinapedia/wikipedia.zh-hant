[Methane-2D-stereo.svg](https://zh.wikipedia.org/wiki/File:Methane-2D-stereo.svg "fig:Methane-2D-stereo.svg")是最简单的有机化合物|250px\]\]

**有机化合物**（；英語：**organic compound**、**organic
chemical**），简称**有机物**，是含碳化合物，但是[碳氧化物](../Page/碳氧化物.md "wikilink")（如[一氧化碳](../Page/一氧化碳.md "wikilink")、[二氧化碳](../Page/二氧化碳.md "wikilink")）、[碳酸](../Page/碳酸.md "wikilink")、[碳酸鹽](../Page/碳酸鹽.md "wikilink")、[碳酸氢盐](../Page/碳酸氢盐.md "wikilink")、[氫氰酸](../Page/氫氰酸.md "wikilink")、[氰化物](../Page/氰化物.md "wikilink")、[硫氰化物](../Page/硫氰化物.md "wikilink")、[氰酸](../Page/氰酸.md "wikilink")、[氰酸鹽](../Page/氰酸鹽.md "wikilink")、[氰鹵化物](../Page/氰鹵化物.md "wikilink")（如[氯化氰](../Page/氯化氰.md "wikilink")）、[金屬碳化物](../Page/金屬碳化物.md "wikilink")（如[電石CaC](../Page/電石.md "wikilink")<sub>2</sub>）等除外。有机化合物有时也可被定义为[碳氫化合物及其](../Page/碳氫化合物.md "wikilink")[衍生物的總稱](../Page/衍生物.md "wikilink")。有机物是[生命產生的](../Page/生命.md "wikilink")[物質基礎](../Page/物質.md "wikilink")，例如生命的起源——[胺基酸即為一有機化合物](../Page/胺基酸.md "wikilink")。\[1\]

[L-isoleucine-3D-balls.png](https://zh.wikipedia.org/wiki/File:L-isoleucine-3D-balls.png "fig:L-isoleucine-3D-balls.png")

## 特点

多數有機化合物主要含有[碳](../Page/碳.md "wikilink")、[氫兩種元素](../Page/氫.md "wikilink")，有些含[氧](../Page/氧.md "wikilink")，此外也常含有[氮](../Page/氮.md "wikilink")、[磷](../Page/磷.md "wikilink")、[硫](../Page/硫.md "wikilink")、[鹵素等](../Page/鹵素.md "wikilink")（以[稀有氣體以外的](../Page/稀有氣體.md "wikilink")[非金屬元素為主](../Page/非金屬.md "wikilink")）。在目前发现的有机物中，部分有機物來自[生物圈](../Page/生物圈.md "wikilink")，但絕大多數是以[石油](../Page/石油.md "wikilink")、[天然氣](../Page/天然氣.md "wikilink")、[煤等作為原料](../Page/煤.md "wikilink")，通過人工合成的方法製得。

和[無機物相比](../Page/無機物.md "wikilink")，有機物數目眾多，可達幾百萬種，而[無機物目前只發現數十萬種](../Page/無機物.md "wikilink")，因為有機化合物的[碳](../Page/碳.md "wikilink")[原子的結合能力非常強](../Page/原子.md "wikilink")，可以互相結合成[碳鏈或](../Page/碳鏈.md "wikilink")[碳環](../Page/碳環.md "wikilink")。[碳](../Page/碳.md "wikilink")[原子數量可以是一两個](../Page/原子.md "wikilink")，也可以是幾千、幾萬個，許多有機[高分子化合物](../Page/高分子化合物.md "wikilink")（[聚合物](../Page/聚合物.md "wikilink")）甚至可以有幾十萬個碳原子。此外，有機化合物中[同分異構現象非常普遍](../Page/同分異構.md "wikilink")，這也是有機化合物數目繁多的原因之一。

有機化合物除少數以外，一般都能[燃燒](../Page/燃燒.md "wikilink")。和無機物相比，它們的熱穩定性比較差，[電解質受熱容易分解](../Page/電解質.md "wikilink")。有機物的[熔點較低](../Page/熔點.md "wikilink")，一般不超過400℃。有機物的[極性很弱](../Page/極性.md "wikilink")，因此大多不溶於水。有機物之間的反應，大多是分子間的反應，往往需要一定的[活化能](../Page/活化能.md "wikilink")，因此反應緩慢，往往需要加入[催化劑等方法](../Page/催化劑.md "wikilink")。另外有機物的反應比較複雜，在同樣條件下，一個[化合物可以同時進行幾個不同的反應](../Page/化合物.md "wikilink")，生成不同的產物。

有機化合物一般密度小於2g/cm<sup>3</sup>，而無機化合物正好相反。在溶解性方面，有機化合物是非極性化合物，無機化合物則是[極性化合物](../Page/極性化合物.md "wikilink")。

## 歷史與命名

「有機」這歷史性名詞，可追溯至19世紀，當時[生機論者認為有機化合物只能以](../Page/生機論.md "wikilink")[生物](../Page/生物.md "wikilink")（life-force，vis
vitalis）合成。此理論基於有機物與「無機」的基本分別，有機物是不能被非生命力合成的。後來德國化學家[弗裏德裏希·維勒以](../Page/弗裏德裏希·維勒.md "wikilink")[氰酸與](../Page/氰酸.md "wikilink")[氨水合成](../Page/氨水.md "wikilink")[尿素](../Page/尿素.md "wikilink")（無機物合成有機物）時這一理論才被推翻。一般而言，有機化合物定義為化合物中有[碳氫鍵而無機化合物則沒有](../Page/碳氫鍵.md "wikilink")。因此[碳酸](../Page/碳酸.md "wikilink")（H<sub>2</sub>CO<sub>3</sub>）、[二氧化碳是](../Page/二氧化碳.md "wikilink")[無機化合物](../Page/無機化合物.md "wikilink")，但是[甲酸](../Page/甲酸.md "wikilink")（又名[蟻酸](../Page/蟻酸.md "wikilink")）（HCOOH，第一個[脂肪酸](../Page/脂肪酸.md "wikilink")）則是有機化合物。然而，化合物中没有碳氫鍵也不一定为无机物，如[聚四氟乙烯](../Page/聚四氟乙烯.md "wikilink")。

## 共价键

在有機物中，[碳的](../Page/碳.md "wikilink")[化合價](../Page/化合價.md "wikilink")（原子價）都為4價，但在計算中（尤其是[氧化還原反應](../Page/氧化還原反應.md "wikilink")）[碳的](../Page/碳.md "wikilink")[氧化數被認為是不固定的](../Page/氧化數.md "wikilink")。

有機物中普遍存在[共價鍵](../Page/共價鍵.md "wikilink")。有機物發生反應時，就是一些[共價鍵斷裂](../Page/共價鍵.md "wikilink")，同時一些新的[共價鍵生成](../Page/共價鍵.md "wikilink")。

## 分類

按照[碳鏈結合形式的不同](../Page/碳鏈.md "wikilink")，有機化合物基本可以分為：

  - [脂肪族化合物](../Page/脂肪族化合物.md "wikilink")（開放鏈族[化合物](../Page/化合物.md "wikilink")）：[碳](../Page/碳.md "wikilink")[原子和](../Page/原子.md "wikilink")[碳](../Page/碳.md "wikilink")[原子之間形成一條開放的鏈的化合物](../Page/原子.md "wikilink")，可以是直鏈也可以帶支鏈。
  - [碳環族化合物](../Page/碳.md "wikilink")：[碳](../Page/碳.md "wikilink")[原子連接成環狀的化合物](../Page/原子.md "wikilink")。
      - [脂环族化合物](../Page/脂环族化合物.md "wikilink")：[碳](../Page/碳.md "wikilink")[原子和](../Page/原子.md "wikilink")[碳](../Page/碳.md "wikilink")[原子之間形成一條封閉的環狀鏈的化合物](../Page/原子.md "wikilink")，也可以帶支鏈。
      - [芳香族化合物](../Page/芳香族化合物.md "wikilink")：含有[碳](../Page/碳.md "wikilink")[原子和](../Page/原子.md "wikilink")[碳](../Page/碳.md "wikilink")[原子之間形成一條封閉的環狀鍵的化合物](../Page/原子.md "wikilink")，但苯環上的[碳](../Page/碳.md "wikilink")[原子之間的共价键是介于單鍵与雙鍵之间的一种特殊键](../Page/原子.md "wikilink")(大π键)，鍵數為1.5鍵。
  - [雜環化合物](../Page/雜環化合物.md "wikilink")：[碳](../Page/碳.md "wikilink")[原子和](../Page/原子.md "wikilink")[碳](../Page/碳.md "wikilink")[原子之間形成一條封閉的環狀鏈的化合物](../Page/原子.md "wikilink")，但其中某些[碳](../Page/碳.md "wikilink")[原子被其他](../Page/原子.md "wikilink")[元素的](../Page/元素.md "wikilink")[原子取代](../Page/原子.md "wikilink")。

## 参考文献

## 参见

  - [无机化合物](../Page/无机化合物.md "wikilink")
  - [有機化合物列表](../Page/有機化合物列表.md "wikilink")
  - [有機化學命名法](../Page/有機化學命名法.md "wikilink")

{{-}}

[有机化合物](../Category/有机化合物.md "wikilink")
[Category:有机化学](../Category/有机化学.md "wikilink")

1.  [Spencer L. Seager](../Page/Spencer_L._Seager.md "wikilink"),
    Michael R. Slabaugh. Chemistry for Today: general, organic, and
    [biochemistry](../Page/biochemistry.md "wikilink"). Thomson
    Brooks/Cole, **2004**, p. 342.