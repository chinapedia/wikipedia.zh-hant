**約翰·布肯，第一代特威兹穆尔男爵**，[GCMG](../Page/聖米迦勒及聖喬治勳章.md "wikilink")，[GCVO](../Page/維多利亞皇家勳章.md "wikilink")，[CH](../Page/名譽勳位.md "wikilink")，[PC](../Page/英國樞密院.md "wikilink")（，），[蘇格蘭](../Page/蘇格蘭.md "wikilink")[小說家及政治家](../Page/小說.md "wikilink")，曾任[加拿大總督](../Page/加拿大總督.md "wikilink")。

## 作品

  - 《[三十九級臺階](../Page/三十九級臺階.md "wikilink")》（1915年）

## 參見

  - [特威茲穆爾男爵](../Page/特威茲穆爾男爵.md "wikilink")

[B](../Page/category:苏格兰人.md "wikilink")

[B](../Category/英国小说家.md "wikilink")
[B](../Category/1875年出生.md "wikilink")
[B](../Category/1940年逝世.md "wikilink")
[T](../Category/聯合王國男爵.md "wikilink")
[B](../Category/加拿大總督.md "wikilink")
[B](../Category/名譽勳位成員.md "wikilink")
[Category:牛津大学布雷齐诺斯学院校友](../Category/牛津大学布雷齐诺斯学院校友.md "wikilink")
[Category:格拉斯哥大學校友](../Category/格拉斯哥大學校友.md "wikilink")
[Category:詹姆斯·泰特·布莱克纪念奖获得者](../Category/詹姆斯·泰特·布莱克纪念奖获得者.md "wikilink")