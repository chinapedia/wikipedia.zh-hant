**河西区**是[中国](../Page/中华人民共和国.md "wikilink")[天津市的](../Page/天津市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，面积41.24平方千米，户籍人口76万，因地处[海河西岸而得名](../Page/海河.md "wikilink")。[天津市人民政府](../Page/天津市人民政府.md "wikilink")、[小白楼地区城市主中心](../Page/小白楼地区城市主中心.md "wikilink")、[友谊路金融街](../Page/友谊路_\(天津\).md "wikilink")、[天津市文化中心等都位于该区域内](../Page/天津市文化中心.md "wikilink")。\[1\]

## 区位

河西区东临[海河与](../Page/海河.md "wikilink")[河东区相望](../Page/河东区_\(天津市\).md "wikilink")，西迄卫津南路、卫津河与[南开区](../Page/南开区.md "wikilink")、[西青区交界](../Page/西青区.md "wikilink")，南沿双林农场引水河与[津南区毗邻](../Page/津南区.md "wikilink")，北抵徐州道、[马场道](../Page/马场道.md "wikilink")、津河与[和平区接壤](../Page/和平区_\(天津市\).md "wikilink")。

## 行政区划

[缩略图](https://zh.wikipedia.org/wiki/File:天津公馆.jpg "fig:缩略图")
河西区现下辖13个街道：

  - 街道办事处：大营门街道、下瓦房街道、桃园街道、挂甲寺街道、马场街道、越秀路街道、友谊路街道、天塔街道、尖山街道、陈塘庄街道、柳林街道、东海街道、梅江街道。

## 教育机构

目前，坐落于河西区的院校有：

  - [海河中学](../Page/海河中学.md "wikilink")
  - [新华中学](../Page/新华中学.md "wikilink")
  - [实验中学](../Page/天津市实验中学.md "wikilink")
  - [天津市第四中学](../Page/天津市第四中学.md "wikilink")
  - [天津市第四十二中学](../Page/天津市第四十二中学.md "wikilink")
  - [北京师范大学天津附属中学](../Page/北京师范大学天津附属中学.md "wikilink")
  - [天津市第四十一中学](../Page/天津市第四十一中学.md "wikilink")
  - [天津财经大学](../Page/天津财经大学.md "wikilink")
  - [天津科技大学](../Page/天津科技大学.md "wikilink")
  - [天津外国语大学](../Page/天津外国语大学.md "wikilink")
  - [天津工程师范学院](../Page/天津职业技术师范大学.md "wikilink")
  - [天津体育学院](../Page/天津体育学院.md "wikilink")

## 相关事件

  - [天津市政府爆炸事件](../Page/天津市政府爆炸事件.md "wikilink")

## 内部链接

  - [河西区 (三亚市)](../Page/河西区_\(三亚市\).md "wikilink")

## 相关链接

  - [河西政务网](http://www.tjhexi.gov.cn/)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:天津区县](../Category/天津区县.md "wikilink")
[Category:天津市辖区](../Category/天津市辖区.md "wikilink")
[Category:天津市河西区](../Category/天津市河西区.md "wikilink")
[津](../Category/国家卫生城市.md "wikilink")
[4](../Category/全国文明城市.md "wikilink")

1.  [河西政务网](http://www.tjhexi.gov.cn/)