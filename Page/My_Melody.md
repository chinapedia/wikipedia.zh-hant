**My
Melody**（[台灣譯名為](../Page/台灣.md "wikilink")**美樂蒂**）是[Sanrio第一隻以](../Page/Sanrio.md "wikilink")[兔子為造型的](../Page/兔子.md "wikilink")[卡通角色](../Page/卡通.md "wikilink")，在1975年誕生。近年在[亞洲區很受歡迎](../Page/亞洲.md "wikilink")，因此衍生了不少相關[兒童](../Page/兒童.md "wikilink")[玩具及商品](../Page/玩具.md "wikilink")。

## 歷史

My
Melody的誕生靈感來自[童話故事](../Page/童話故事.md "wikilink")《[小紅帽](../Page/小紅帽.md "wikilink")》，1975年Sanrio以「童話中大野[狼最愛吃的](../Page/狼.md "wikilink")[動物](../Page/動物.md "wikilink")」為題徵求[投票](../Page/投票.md "wikilink")，結果[兔子得到的票數最多](../Page/兔子.md "wikilink")，於是這隻可愛的小白兔便登場了。最初她的名字叫「Little
Red Riding Hood」，次年才改稱為「My Melody」。

1976年My
Melody的好朋友小[老鼠弗蘭多](../Page/老鼠.md "wikilink")（Flat）登場，1983年弟弟Rhythm出現，在80年代末卻沉寂下來，直至1997年My
Melody在[雜誌投票上突然受到](../Page/雜誌.md "wikilink")[日本高中女生的愛戴](../Page/日本.md "wikilink")，成為除了[Hello
Kitty外最受歡迎的角色](../Page/Hello_Kitty.md "wikilink")，從此My
Melody相關的[玩具和精品開始熱賣](../Page/玩具.md "wikilink")。1998年因Hello
Kitty妒忌Melody人氣比她高，在一次野餐中推了Melody落山下落不明，及後受到極大回響，Melody和Hello
Kitty的精品銷售量急速上升，在八月官方決定取消被推落山的設定，聲稱只是一場意外，My
Melody也再次現身。在2005年，以My
Melody為主角的[動畫](../Page/動畫.md "wikilink")《[我愛美樂蒂](../Page/我愛美樂蒂.md "wikilink")》在[日本播放](../Page/日本.md "wikilink")，對手[Kuromi於此時出場](../Page/Kuromi.md "wikilink")，令My
Melody更受歡迎，動畫至今已播放至第四輯，更推出CD和DVD。Sanrio Japan及草莓新聞合辦2010 & 2011 Sanrio
Character Ranking，台灣三麗鷗亦舉辦2010 Sanrio人氣明星網上選舉，My Melody在三個選舉中皆為第一名, 而My
Melody也在2014年的日本人氣明星網上選舉得到第一名。

## 資料

[My_melody_in_the_show.jpg](https://zh.wikipedia.org/wiki/File:My_melody_in_the_show.jpg "fig:My_melody_in_the_show.jpg")

  - **名稱**: My Melody（日語全名：マイメロディ，暱稱マイメロ（My Melo），台灣譯名：美樂蒂）
  - **生日日期**: 1976年1月18日
  - **生肖**: 兔兔
  - **性別**: 女
  - **星座**：摩羯座
  - **出生地**：美国东部的马里兰森林
  - **興趣**：和妈妈一起烹饪食物
  - **性格**：天真无邪，活泼开朗，但同时又有一点急躁。（和任何人都能马上成为好朋友）
  - **最珍藏的物件**：祖母亲手为她缝制的小红帽
  - **最喜歡吃**：杏仁蛋糕
  - **家中成員**：爸爸、媽媽、祖父、祖母、弟弟Rhythm（里泽木）、遠房表弟Littleforestfellow

## 家人

  - 祖父：經常含著煙斗，是個勇敢的冒險家，夢想是環遊世界。
  - 祖母：會說很多好聽的故事，編織很了得，My Melody的小红帽就是她造的。
  - 爸爸：是個待人非常親切的爸爸。體力也很充沛總是充满干劲哦。
  - 媽媽：手工工艺家，喜欢做各式各样好吃的点心。
  - 弟弟Rhythm（旋律）：是個有点调皮的小男孩。
  - 遠房表弟Littleforestfellow：可愛系男孩，暱稱Mello。

## 朋友

  - Flat （老鼠 弗蘭多） 美乐蒂的男朋友 ，有些害羞。
  - Risu （松鼠）老鼠弗蘭多的最好的朋友，有些小小的害羞 喜歡親女孩
  - Piano （绵羊 必爱诺）cute，是个非常亲切有些爱撒娇的女孩子。

## 參看

  - [Hello Kitty](../Page/Hello_Kitty.md "wikilink")
  - [Kuromi](../Page/Kuromi.md "wikilink")
  - [Sanrio](../Page/Sanrio.md "wikilink")
  - [Pom Pom Purin](../Page/Pom_Pom_Purin.md "wikilink")

## 外部網站

  - [台灣三麗鷗官方網站 - 三麗鷗明星家族My
    Melody](https://www.sanrio.com.tw/characters/)
  - [My
    Melody官方網站（日語）](https://web.archive.org/web/20071104112536/http://www.sanrio.co.jp/characters/mymelody/welcome.html)
  - [My
    Melody官方網站（日語簡介）](https://web.archive.org/web/20081013135052/http://www.sanrio.co.jp/characters/detail/mymelody/index.html)
  - [奇幻魔法Melody官方網站 -
    第一輯（日語）](http://www.tv-osaka.co.jp/mymelo/index.html)
  - [奇幻魔法Melody官方網站 -
    第二輯（日語）](http://www.tv-osaka.co.jp/mymelo_kurukuru/)
  - [奇幻魔法Melody官方網站 -
    第三輯（日語）](http://www.tv-osaka.co.jp/animelobby/story_chara_m.html)
  - [奇幻魔法Melody官方網站 -
    第四輯（日語）](http://www.tv-osaka.co.jp/ip4/mymelo_kirara/)

[Category:三麗鷗角色](../Category/三麗鷗角色.md "wikilink")
[Category:虛構兔子](../Category/虛構兔子.md "wikilink")