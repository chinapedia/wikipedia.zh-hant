[Homeless_World_Cup_2008_Melbourne_field_side.jpg](https://zh.wikipedia.org/wiki/File:Homeless_World_Cup_2008_Melbourne_field_side.jpg "fig:Homeless_World_Cup_2008_Melbourne_field_side.jpg")舉行的比賽\]\]
**無家者世界盃**（**The Homeless World
Cup**），又稱為**露宿者世界盃**，是一項由名為“國際街頭報紙網絡”（International
Network of Street
Papers，簡稱INSP）的組織所舉辦的國際性[足球賽事](../Page/足球.md "wikilink")，特點是參賽者全為無家可歸的[露宿者](../Page/露宿者.md "wikilink")。組織希望以足球這項世界知名的[運動作為露宿者與社會交流的橋樑](../Page/運動.md "wikilink")，從而喚起社會對全球窮困以及無家可歸者的關心和注意。賽事由2003年起開始舉辦，以一年一度方式進行。

## INSP

INSP是一個國際知名的組織，專為無家可歸者提供服務和機會，以令他們能重投社會。例如最著名的是在街頭售賣他們的報紙雜誌，而露宿者則可從刊物的售價中得到50%的援助。現時INSP的成員遍佈28個國家。

## 歷史

### 2003年

首屆露宿者世界盃在2003年7月於[奧地利的](../Page/奧地利.md "wikilink")[格拉茨](../Page/格拉茨.md "wikilink")（Graz）舉行。總共有18個國家參與，進行了109場比賽，並共有20,000名觀眾觀看賽事。最後，由東道主奧地利獲得冠軍。

最後賽果排名為（前十名）:

及後，事實證明了這項賽事是頗為成功的，因為在參賽的141名的參賽者中有31名現在都有穩定的工作和收入。

### 2004年

此屆賽事在2004年7月於[瑞典](../Page/瑞典.md "wikilink")[哥登堡](../Page/哥登堡.md "wikilink")（Gothenburg）舉行，參賽隊伍亦由首屆的18隊增加到29隊。上屆冠軍奧地利決賽不敵意大利，衛冕失敗。本屆賽事出現首支[亞洲球隊](../Page/亞洲.md "wikilink")，日本參加此項賽事。

最後賽果排名為（前十名）:

  - 公平競技獎: [日本](../Page/日本.md "wikilink")
  - 最佳守門員: 奇雲．威爾遜 （Kevin Wilson） **英格蘭**
  - 金靴獎: 艾達曼高．耶夫根 （Adamenko Yevgen）
    **[烏克蘭](../Page/烏克蘭.md "wikilink")**，53球

### 2005年

此屆賽事在2005年7月於[蘇格蘭](../Page/蘇格蘭.md "wikilink")[愛丁堡](../Page/愛丁堡.md "wikilink")（Edinburgh）舉行。此屆賽事原本打算由[美國](../Page/美國.md "wikilink")[紐約主辦的](../Page/紐約.md "wikilink")，但由於主辦單位恐防[露宿者在取得](../Page/露宿者.md "wikilink")[美國簽証方面有問題](../Page/美國.md "wikilink")，故此將賽事移師到[蘇格蘭進行](../Page/蘇格蘭.md "wikilink")。本屆賽事有32支隊伍參賽，另再有一支亞洲球隊[香港參賽](../Page/香港.md "wikilink")。意大利於決賽擊敗波蘭，成功衛冕。

最後賽果排名為（前十名）:

  - 公平競技獎: **[美國](../Page/美國.md "wikilink")**（第27名）
  - 最佳新球隊: **[澳大利亞](../Page/澳大利亞.md "wikilink")**

### 2006年

此屆賽事在2006年9月於[南非的](../Page/南非.md "wikilink")[開普敦舉行](../Page/開普敦.md "wikilink")。本屆賽事有48支隊伍參賽。

最後賽果排名為（前四名）:

1.  [俄羅斯](../Page/俄羅斯.md "wikilink")
2.  [哈薩克](../Page/哈薩克.md "wikilink")
3.  [波蘭](../Page/波蘭.md "wikilink")
4.  [墨西哥](../Page/墨西哥.md "wikilink")

### 2007年

此屆賽事在2007年7月於[丹麥的](../Page/丹麥.md "wikilink")[哥本哈根舉行](../Page/哥本哈根.md "wikilink")。本屆賽事有48支隊伍參賽。

最後賽果排名為（前四名）:

1.  [蘇格蘭](../Page/蘇格蘭.md "wikilink")
2.  [波蘭](../Page/波蘭.md "wikilink")
3.  [利比里亞](../Page/利比里亞.md "wikilink")
4.  [丹麥](../Page/丹麥.md "wikilink")

### 2008年

此屆賽事在2008年12月於[澳洲的](../Page/澳洲.md "wikilink")[墨爾本舉行](../Page/墨爾本.md "wikilink")。本屆賽事有56支隊伍參賽。

最後賽果排名為（前四名）:

1.  [阿富汗](../Page/阿富汗.md "wikilink")
2.  [俄羅斯](../Page/俄羅斯.md "wikilink")
3.  [加納](../Page/加納.md "wikilink")
4.  [蘇格蘭](../Page/蘇格蘭.md "wikilink")

### 2009年

此屆賽事在2009年9月6日至13日於[意大利的](../Page/意大利.md "wikilink")[米蘭舉行](../Page/米蘭.md "wikilink")。本屆賽事有48支隊伍參賽。

最後賽果排名為:

1.  [烏克蘭](../Page/烏克蘭.md "wikilink")
2.  [葡萄牙](../Page/葡萄牙.md "wikilink")

## 比賽詳情

### 球員資格

參賽者必須為:

  - 16歲或以上的[男性或](../Page/男性.md "wikilink")[女性](../Page/女性.md "wikilink")
  - 上屆賽事完結之後成為露宿者的人士 **或**
  - 以街頭賣INSP出版的刊物為主要收入的人士 **或**
  - 尋求[政治庇護的人士](../Page/政治庇護.md "wikilink")（包括沒有得到庇護或沒有獲發工作證的人士）

### 參賽人數

球場上每隊最多只能有四名球員在陣:

  - 3名球員及1名[守門員](../Page/守門員.md "wikilink")
  - 4名後備球員（可循環調配）

### 比賽規則

  - 勝方得3分，負方0分。和局需以即時死亡的[點球決勝](../Page/點球.md "wikilink")，如以點球決勝的賽事，勝方3分，負方1分。
  - 賽事全場時間為14分鐘。
  - 球場面積為20米長和14米闊。

## 外部連結

  - [賽事官方網站](http://www.homelessworldcup.org/)
  - [香港代表隊簡介](http://www.soco.org.hk/homeless)
  - [無家者世界盃香港區賽事主頁](https://web.archive.org/web/20160412092957/http://soco-wofoo-homelesssoccer.hk/)

[Category:全球性國家隊足球賽事](../Category/全球性國家隊足球賽事.md "wikilink")