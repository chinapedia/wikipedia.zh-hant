**李未**（），计算机专家，[中国科学院院士](../Page/中国科学院.md "wikilink")，2002年开始任[北京航空航天大学校长](../Page/北京航空航天大学.md "wikilink")。

1966年毕业于[北京大学数学与力学系](../Page/北京大学.md "wikilink")，1983年在[英国](../Page/英国.md "wikilink")[爱丁堡大学计算机科学系获博士学位](../Page/爱丁堡大学.md "wikilink")，曾任英国科学与工程委员会、纽卡瑟大学和爱丁堡大学计算机系高级研究员，[欧洲共同体发展信息战略计划](../Page/欧洲共同体.md "wikilink")（ESPRIT）及德国不莱梅大学教授级研究员，[德国萨尔大学](../Page/德国.md "wikilink")（Zuse）客座教授。1986年任教授，并被批准为博士导师，1997年当选为[中国科学院院士](../Page/中国科学院.md "wikilink")。

他主要从事[计算机软件与科学理论以及](../Page/计算机.md "wikilink")[因特网应用研究](../Page/因特网.md "wikilink")，研究领域包括并发[程序设计语言语义理论](../Page/程序设计语言.md "wikilink")、软件开发方法、[人工智能基础及超大规模](../Page/人工智能.md "wikilink")[集成电路辅助设计技术](../Page/集成电路.md "wikilink")。

1981年提出了用结构操作语义描述软件中并发、同步及通讯现象的方法，系统地解决了诸如Ada、Edison等并发式程序设计语言的操作语义问题，结构操作语义方法已成为[程序设计语言语义学的一种经典方法](../Page/程序设计语言语义学.md "wikilink")。

1992年建立了[开放逻辑理论](../Page/开放逻辑.md "wikilink")，解决了[信息的不完全性](../Page/信息.md "wikilink")、知识的可错性及推理的非单调性的描述问题。

1998年在中国首先倡导[海量信息计算的理论与方法研究](../Page/海量信息计算.md "wikilink")。

2008年，当选第十一届全国政协委员\[1\]，代表科学技术界，分入第三十一组。并担任教科文卫体委员会专委。\[2\]

## 參考文獻

[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:愛丁堡大學校友](../Category/愛丁堡大學校友.md "wikilink")
[Category:北京航空航天大学教授](../Category/北京航空航天大学教授.md "wikilink")
[Category:北京航空航天大学校长](../Category/北京航空航天大学校长.md "wikilink")
[L李](../Category/中国计算机科学家.md "wikilink")
[L李](../Category/北京人.md "wikilink")
[L李](../Category/1943年出生.md "wikilink")
[W](../Category/李姓.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:北京科学家](../Category/北京科学家.md "wikilink")
[Category:中国计算机学会会士](../Category/中国计算机学会会士.md "wikilink")

1.
2.