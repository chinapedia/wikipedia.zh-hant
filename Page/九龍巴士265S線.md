**九龍巴士265S線**是[香港](../Page/香港.md "wikilink")[新界一條來往](../Page/新界.md "wikilink")[天水圍市中心及](../Page/天水圍市中心.md "wikilink")[大埔工業邨的巴士路線](../Page/大埔工業邨.md "wikilink")，取道[元朗公路](../Page/元朗公路.md "wikilink")、[新田公路](../Page/新田公路.md "wikilink")、[粉嶺公路及](../Page/粉嶺公路.md "wikilink")[吐露港公路往來](../Page/吐露港公路.md "wikilink")。

## 歷史

  - 2004年8月31日：投入服務
  - 2014年[6月16日](../Page/6月16日.md "wikilink")：往方向改經[大發街](../Page/大發街.md "wikilink")，不經[大福街及](../Page/大福街.md "wikilink")[大鴻街](../Page/大鴻街.md "wikilink")。\[1\]
  - 2017年7月1日：為配合[九巴專營權延續](../Page/九巴.md "wikilink")，新增由[廣福邨往](../Page/廣福邨.md "wikilink")的[分段收費](../Page/分段收費.md "wikilink")，並推行學生長途路線即日回程半價折扣優惠計劃。

## 服務時間及班次

  - 星期一至六：
      - 天水圍市中心開：06:40、07:15
      - 大埔工業邨開：06:45

<!-- end list -->

  -
    星期日及公眾假期不設服務

## 收費

全程：$15.1

### [八達通轉乘優惠](../Page/八達通轉乘優惠.md "wikilink")

#### 龍運機場巴士及九巴轉乘計劃

### 學生長途路線即日回程半價折扣優惠計劃

此路線設有學生長途路線即日回程半價折扣優惠計劃，12至25歲持有學生個人八達通卡的全日制學生憑該張八達通於同一天內乘搭此線兩個不同方向，或乘搭此線往大埔方向及線往天水圍方向，或乘搭此線往天水圍方向及線往九龍方向，第二程可享有半價折扣優惠，惟兩程之間不可使用該張八達通進行超過九次金錢交易。有關優惠可與巴士本身的八達通轉乘優惠共用，系統會先計算本身的轉乘折扣優惠，再計算回程半價優惠。

## 用車

本線由[265B及](../Page/九龍巴士265B線.md "wikilink")[276A抽調車輛行走](../Page/九龍巴士276A線.md "wikilink")。

## 行車路線

**天水圍市中心開**經：[天恩路](../Page/天恩路.md "wikilink")、[天榮路](../Page/天榮路.md "wikilink")、[天城路](../Page/天城路.md "wikilink")、[天龍路](../Page/天龍路.md "wikilink")、[天葵路](../Page/天葵路.md "wikilink")、[濕地公園路](../Page/濕地公園路.md "wikilink")、[天瑞路](../Page/天瑞路.md "wikilink")、[天湖路](../Page/天湖路.md "wikilink")、[天耀路](../Page/天耀路.md "wikilink")、[天福路](../Page/天福路.md "wikilink")、[朗天路](../Page/朗天路.md "wikilink")、[唐人新村交匯處](../Page/唐人新村交匯處.md "wikilink")、[元朗公路](../Page/元朗公路.md "wikilink")、[新田公路](../Page/新田公路.md "wikilink")、[粉嶺公路](../Page/粉嶺公路.md "wikilink")、[吐露港公路](../Page/吐露港公路.md "wikilink")、[大埔公路](../Page/大埔公路.md "wikilink")—元洲仔段、[廣福道](../Page/廣福道.md "wikilink")、[寶鄉街](../Page/寶鄉街.md "wikilink")、[大埔太和路](../Page/大埔太和路.md "wikilink")、[汀角路](../Page/汀角路.md "wikilink")、[大發街及](../Page/大發街.md "wikilink")[大昌街](../Page/大昌街.md "wikilink")。

**大埔工業邨開**經：[大貴街](../Page/大貴街.md "wikilink")、大昌街、大發街、汀角路、大埔太和路、寶鄉街、廣福道、大埔公路—元洲仔段、吐露港公路、粉嶺公路、新田公路、元朗公路、唐人新村交匯處、朗天路、天福路、天耀路、天湖路、天瑞路、濕地公園路、天葵路、天龍路、天城路、天恩路及[嘉恩街](../Page/嘉恩街.md "wikilink")。

### 沿線車站

[265SRtMap.png](https://zh.wikipedia.org/wiki/File:265SRtMap.png "fig:265SRtMap.png")

| [天水圍市中心開](../Page/天水圍市中心.md "wikilink") | [大埔工業邨開](../Page/大埔工業邨.md "wikilink")      |
| --------------------------------------- | ------------------------------------------ |
| **序號**                                  | **車站名稱**                                   |
| 1                                       | [天水圍市中心巴士總站](../Page/天水圍市中心.md "wikilink") |
| 2                                       | [景湖居](../Page/嘉湖山莊#景湖居（第7期）.md "wikilink") |
| 3                                       | [麗湖居](../Page/嘉湖山莊#麗湖居（第5期）.md "wikilink") |
| 4                                       | [天晴邨晴雲樓](../Page/天晴邨.md "wikilink")        |
| 5                                       | [慧景軒](../Page/慧景軒.md "wikilink")           |
| 6                                       | [香港濕地公園](../Page/香港濕地公園.md "wikilink")     |
| 7                                       | [天逸邨逸潭樓](../Page/天逸邨.md "wikilink")        |
| 8                                       | [天富苑欣富閣](../Page/天富苑.md "wikilink")        |
| 9                                       | [天頌苑](../Page/天頌苑.md "wikilink")           |
| 10                                      | [天瑞邨](../Page/天瑞邨.md "wikilink")           |
| 11                                      | [天水圍公園](../Page/天水圍公園.md "wikilink")       |
| 12                                      | [天耀邨耀盛樓](../Page/天耀邨.md "wikilink")        |
| 13                                      | 天耀邨耀民樓                                     |
| 14                                      | [天祐苑](../Page/天祐苑.md "wikilink")           |
| 15                                      | 廣福邨                                        |
| 16                                      | [王肇枝中學](../Page/王肇枝中學.md "wikilink")       |
| 17                                      | 廣福道                                        |
| 18                                      | [寶鄉橋](../Page/寶鄉橋.md "wikilink")           |
| 19                                      | [平安里](../Page/平安里.md "wikilink")           |
| 20                                      | [榮暉花園](../Page/榮暉花園.md "wikilink")         |
| 21                                      | [太平工業中心](../Page/太平工業中心.md "wikilink")     |
| 22                                      | [富亨邨](../Page/富亨邨.md "wikilink")           |
| 23                                      | 怡雅苑                                        |
| 24                                      | 大昌街                                        |
| 25                                      | 大埔工業邨巴士總站                                  |

## 特點

  - 本線只於平日早上提供服務，方便天水圍居民往大埔上班，也方便往天水圍上學的大埔區學生。本路線是由[276A及](../Page/九龍巴士276A線.md "wikilink")[78K抽調巴士行走](../Page/九龍巴士78K線.md "wikilink")，根據政府憲報路線表令，本線屬於[265B的特別車](../Page/九龍巴士265B線.md "wikilink")，但服務範圍及對象卻與265B線毫無關係。
  - 由於高速公路網絡關係，本線需取道[元朗公路](../Page/元朗公路.md "wikilink")、[新田公路](../Page/新田公路.md "wikilink")、[粉嶺公路及](../Page/粉嶺公路.md "wikilink")[吐露港公路來往](../Page/吐露港公路.md "wikilink")，並於元州仔交匯處進入大埔墟，成為第二條新界西北往來新界東，來回程需取道北區前往，而在北區沒有車站的專利巴士路線。（第一條為[869](../Page/九龍巴士869線.md "wikilink")，但只有單向服務。）

## 參考資料

  - [KMB Air-conditioned Express Route
    九巴空調特快線－265S](http://www.681busterminal.com/265s.html)

## 外部連結

  - [九巴265S線官方網站路線資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=265S)

[265S](../Category/九龍巴士路線.md "wikilink")
[265S](../Category/元朗區巴士路線.md "wikilink")
[265S](../Category/大埔區巴士路線.md "wikilink")

1.  [立法會參考資料摘要：《公共巴士服務條例》(第230章)
    公共專營巴士公司路線表令](http://library.legco.gov.hk/record=b1182773)，2015年10月