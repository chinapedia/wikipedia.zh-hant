**觀爛島龍舟競渡**，是[越南](../Page/越南.md "wikilink")[廣寧省](../Page/廣寧省.md "wikilink")[拜子龍灣的](../Page/拜子龍灣.md "wikilink")[觀爛島](../Page/觀爛島.md "wikilink")（；另亦有[中文媒體譯成](../Page/中文.md "wikilink")「君蘭島」、“關蘭島”）在每年[農曆六月十八日舉行節慶活動](../Page/農曆.md "wikilink")，以紀念[陳朝大將](../Page/越南陳朝.md "wikilink")[陳慶餘抵禦外每](../Page/陳慶餘.md "wikilink")、保家衛國的功績。在活動中，參加競渡的選手會分成兩隊，穿上傳統服飾，由其中一人擔任元帥，競渡奪標，情景與[端午節划](../Page/端午節.md "wikilink")[龍舟相似](../Page/龍舟.md "wikilink")，不過就增添了[越南](../Page/越南.md "wikilink")[觀爛島的民俗特色](../Page/觀爛島.md "wikilink")。該節慶是[拜子龍灣地區較為盛大的節日](../Page/拜子龍灣.md "wikilink")，屆時鄰近地區的人，都會紛紛赴會。

## 起源

[觀爛島的龍舟競渡](../Page/觀爛島.md "wikilink")，是紀念[越南陳朝時抵抗](../Page/越南陳朝.md "wikilink")[元朝入侵的大將](../Page/元朝.md "wikilink")[陳慶餘](../Page/陳慶餘.md "wikilink")（）。1287年至1288年間，[越南遭受第三次](../Page/越南.md "wikilink")[蒙古入侵](../Page/蒙古帝國.md "wikilink")。據《[大越史記全書](../Page/大越史記全書.md "wikilink")‧陳紀‧陳仁宗》所載，陳慶餘在[陳](../Page/越南陳朝.md "wikilink")[重興三年](../Page/重興.md "wikilink")[農曆十二月](../Page/農曆.md "wikilink")，陳慶餘曾在率領水軍，成功截擊[元軍的運糧船](../Page/元.md "wikilink")，「獲[虜軍糧器械](../Page/元.md "wikilink")，不可勝計，俘虜亦甚多」，對[越方獲勝起到了相當重要的作用](../Page/越南.md "wikilink")。\[1\]

陳慶餘的護國有功，令他深受百姓的尊崇。在他死後，觀爛島上的居民，就視他為守護神，供奉在村中的祠堂，並每年舉行[龍舟競渡](../Page/龍舟.md "wikilink")，以作紀念。\[2\]

## 節慶程序及龍舟競渡

### 籌備階段

[龍舟競度在每年](../Page/龍舟.md "wikilink")[農曆六月十八日進行](../Page/農曆.md "wikilink")，而準備工作，則會在此之前數天便開始進行。
從六月十日開始，當地村民若非工作必要，就不會離開該地，以籌措慶典事宜，並歡迎外賓。其後，當地人會相約，一同討論慶典所須經費及籌備情況。然後選人，安排各自在十八日那天參賽、執旗、打鼓、儀仗等工作。繼而整理參賽船隻，預備划[龍舟所須的槳](../Page/龍舟.md "wikilink")、鼓、鑼等等，並修飾船身。並有祭祀活動，以拜祭[陳慶餘](../Page/陳慶餘.md "wikilink")。準備好後，便等待六月十八日的龍舟競渡。\[3\]

### 六月十八日龍舟競渡情況

  - 參加者：在慶典中，[觀爛島上的五處較大的村莊與三個較小的聚落](../Page/觀爛島.md "wikilink")，會按照傳統，派出村中有一到三歲男孩的男子負責花費，並從中選出體魄強健及操槳技巧好的人，擔任[龍舟競渡的選手](../Page/龍舟.md "wikilink")，身穿古代戎裝出賽。
  - 比賽過程：比賽於下午潮漲時進行，兩隊參賽者在拜祭[陳慶餘的公祠前登上龍舟中](../Page/陳慶餘.md "wikilink")，準備就緒，經過舟上的元帥求神庇祐等到一聲令下，到達指定位置便開始，最先到達碼頭上的終點便勝出。其後，得勝者便可以領取比負方多出數陪的獎賞。[龍舟競渡過後](../Page/龍舟.md "wikilink")，於六月二十日，當地人還會舉行慶典，以祝願和平。\[4\]\[5\]

## 注釋

## 參考文獻

<div class="references-small">

  -
  -

</div>

## 參見

  - [下龍灣](../Page/下龍灣.md "wikilink")
  - [龍舟](../Page/龍舟.md "wikilink")
  - [蒙越戰爭](../Page/蒙越戰爭.md "wikilink")

## 外部連結

  -
[Category:越南傳統](../Category/越南傳統.md "wikilink")
[Category:廣寧省](../Category/廣寧省.md "wikilink")
[Category:龍舟競渡](../Category/龍舟競渡.md "wikilink")

1.  吳士連等《大越史記全書‧陳紀‧陳仁宗》，363-364頁。

2.  《經典雜誌》，1999年03月號 第08期，68頁。

3.  [Hotel Vietnam Online - Festival of Quan Lan
    village](http://www.hotelvietnamonline.com/Tours/ecotours/quanlan_festival.htm)

4.  《經典雜誌》，1999年03月號 第08期，68-69頁。

5.