**墨子国际研究中心**位于[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[滕州市](../Page/滕州市.md "wikilink")[东部](../Page/东.md "wikilink")[荆河](../Page/荆河.md "wikilink")[西畔](../Page/西.md "wikilink")，前有[龙泉塔](../Page/龙泉塔.md "wikilink")，后依王学仲艺术馆，南面[府前路](../Page/府前路.md "wikilink")，西临[塔寺街](../Page/塔寺街.md "wikilink")，是一座集办公、[收藏](../Page/收藏.md "wikilink")、[展览](../Page/展览.md "wikilink")、[会议](../Page/会议.md "wikilink")、[旅游于一体的](../Page/旅游.md "wikilink")[墨子研究场所](../Page/墨子.md "wikilink")。

[历史学界对墨子的出生地尚无定论](../Page/历史.md "wikilink")，而滕州市据说是墨子的故里。墨子国际研究中心建于1994年7月，占地[面积](../Page/面积.md "wikilink")1.02万平方米，其中建筑面积4100平方米。
建筑以中国传统的[四合院为设计基础](../Page/四合院.md "wikilink")，由东、西两个相连的四合院组成。东院有汇文堂、图书楼、墨子圣迹堂等建筑，西院有办公楼、迎宾楼、义利堂等。

墨子国际研究中心是[世界上第一个也是唯一的多功能](../Page/世界.md "wikilink")、园林式的研究墨学和纪念墨子的研究中心。

## 主要展室

主要建筑的展室有墨堂、墨子科技堂、墨子圣迹堂、墨子书法展室等。

### 墨堂

墨堂内展有是墨子故里考论、墨子思想体系及墨子研究成果。墨子故里考论方面，内有墨子故里复原沙盘；墨子思想体系方面，有古今中外《墨子》珍藏本的展示，墨子[思想的详细介绍](../Page/思想.md "wikilink")；墨子研究成果方面，有历届墨学国际研讨会的盛况等。

### 墨子科技堂

墨子科技堂展有墨子在[军事](../Page/军事.md "wikilink")、[科技方面的成果](../Page/科技.md "wikilink")。

  - 科技方面包括“[时间与](../Page/时间.md "wikilink")[空间](../Page/空间.md "wikilink")”、“[数学](../Page/数学.md "wikilink")”、“[力学](../Page/力学.md "wikilink")”、“[光学](../Page/光学.md "wikilink")”四个部分。例如光学就有“小孔成像”，“光的传播、折射”等。
  - 军事方面有“军事思想”、“武器装备”、“城防工程”、“防御设施”四个部分。

墨子科技堂由图画、文字、模具、模型实物等组成、参观者可动手操作某些展览品，令参观者对墨子学说有直观的了解。

### 墨子圣迹堂

墨子圣迹堂在墨子研究中心东院北大厅内，“墨子圣迹堂”五个大字由书法家李铎题写。

墨子圣迹堂内有一大型壁画《墨子胜迹图》。

《墨子胜迹图》总面积200余平方米，由62幅画面组成，每幅画面各是一个故事，内容又相互关联，合为一体，每幅画都有简短的文字说明。通过画面与文字，反映墨子的一生。《墨子胜迹图》在制作上吸取[敦煌壁画的传统画技](../Page/敦煌.md "wikilink")，有较高的[艺术欣赏价值](../Page/艺术.md "wikilink")。

### 墨子书法展室

展室内有“王玉玺八体书古今人评墨子”大型书法展览和巨型“墨子城.墨府”模型

## 其他展品

### 墨子全文石刻

墨子国际研究中心的东院内有《墨子》全文[石刻](../Page/石刻.md "wikilink")，据说是中国[历史上第一部石刻](../Page/历史.md "wikilink")《墨子》。
《墨子》石刻上的文字由中国著名[书法家](../Page/书法家.md "wikilink")[马永云先生历时半年](../Page/马永云.md "wikilink")，以[小楷](../Page/小楷.md "wikilink")[书法写成](../Page/书法.md "wikilink")，由嘉祥石[雕刻](../Page/雕刻.md "wikilink")，共83块[石碑](../Page/石碑.md "wikilink")。《墨子》石刻全文镶嵌在东院环廊四周。

### 中外名人书法墨迹碑廊

中外名人书法碑廊刻有[刘华清](../Page/刘华清.md "wikilink")、[迟浩田](../Page/迟浩田.md "wikilink")、[宋健](../Page/宋健.md "wikilink")、[费孝通](../Page/费孝通.md "wikilink")、[楚图南](../Page/楚图南.md "wikilink")、[胡绳](../Page/胡绳.md "wikilink")、[臧克家](../Page/臧克家.md "wikilink")、[姚雪垠](../Page/姚雪垠.md "wikilink")、[蔡尚思](../Page/蔡尚思.md "wikilink")、[任继愈](../Page/任继愈.md "wikilink")、[匡亚明](../Page/匡亚明.md "wikilink")、[张岱年](../Page/张岱年.md "wikilink")、[王学仲](../Page/王学仲.md "wikilink")、[刘艺](../Page/刘艺.md "wikilink")、[李铎等名人的题词](../Page/李铎.md "wikilink")。1998年12月，墨子国际研究中心将这些墨迹整理成《墨子国际研究中心名家题词》由[山东文艺出版社出版发行](../Page/山东文艺出版社.md "wikilink")。

## 相关条目

  - [墨子](../Page/墨子.md "wikilink")
  - [墨家](../Page/墨家.md "wikilink")
  - [滕州市](../Page/滕州市.md "wikilink")
  - [墨学国际研讨会](../Page/墨学国际研讨会.md "wikilink")

## 外部链接

  - [墨子国际研究中心](http://www.moxue.com/zl.asp)
  - [墨子国际研究中心介绍（滕州市官方网页）](https://web.archive.org/web/20071006110645/http://www.tengzhou.gov.cn/xbly/jxtz/t20050407_67280.htm)

[Category:墨家](../Category/墨家.md "wikilink")
[Category:滕州市](../Category/滕州市.md "wikilink")
[Category:非政府间国际组织](../Category/非政府间国际组织.md "wikilink")
[Category:枣庄文化组织](../Category/枣庄文化组织.md "wikilink")
[Category:中国学会](../Category/中国学会.md "wikilink")