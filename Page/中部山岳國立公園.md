[Hotakadake_Takayama01j3872.jpg](https://zh.wikipedia.org/wiki/File:Hotakadake_Takayama01j3872.jpg "fig:Hotakadake_Takayama01j3872.jpg")\]\]
[Mount_Shirouma_from_Mount_Korenge_2000-7-31.jpg](https://zh.wikipedia.org/wiki/File:Mount_Shirouma_from_Mount_Korenge_2000-7-31.jpg "fig:Mount_Shirouma_from_Mount_Korenge_2000-7-31.jpg")和[高山植物](../Page/高山植物.md "wikilink")\]\]
[20_Tateyama_from_Mikurigaike_1998-7-17.jpg](https://zh.wikipedia.org/wiki/File:20_Tateyama_from_Mikurigaike_1998-7-17.jpg "fig:20_Tateyama_from_Mikurigaike_1998-7-17.jpg")\]\]

**中部山岳國立公園**為[日本的](../Page/日本.md "wikilink")[國立公園](../Page/日本国立公园.md "wikilink")，範圍涵蓋[長野縣](../Page/長野縣.md "wikilink")、[岐阜縣](../Page/岐阜縣.md "wikilink")、[富山縣](../Page/富山縣.md "wikilink")、[新潟縣](../Page/新潟縣.md "wikilink")，成立於1934年12月24日。

## 主要景點

  - [立山](../Page/立山_\(日本\).md "wikilink")
  - [黑部水壩](../Page/黑部水壩.md "wikilink")
  - [上高地](../Page/上高地.md "wikilink")
  - [穗高岳](../Page/穗高岳.md "wikilink")
  - [乘鞍岳](../Page/乘鞍岳.md "wikilink")

## 面積

  - 面積 - 174,323ha （陸域部分）
      - 新潟縣 - 8,061ha
      - 富山縣 - 76,431ha
      - 長野縣 - 65,612ha
      - 岐阜縣 - 24,219ha

## 關連市町村

### 新潟縣

  - [糸魚川市](../Page/糸魚川市.md "wikilink")

### 富山縣

  - [富山市](../Page/富山市.md "wikilink")
  - [魚津市](../Page/魚津市.md "wikilink")
  - [黑部市](../Page/黑部市.md "wikilink")
  - [朝日町](../Page/朝日町_\(富山縣\).md "wikilink")
  - [上市町](../Page/上市町.md "wikilink")
  - [立山町](../Page/立山町.md "wikilink")

### 長野縣

  - [松本市](../Page/松本市.md "wikilink")
  - [安曇野市](../Page/安曇野市.md "wikilink")
  - [大町市](../Page/大町市.md "wikilink")
  - [白馬村](../Page/白馬村.md "wikilink")
  - [小谷村](../Page/小谷村.md "wikilink")

### 岐阜縣

  - [高山市](../Page/高山市.md "wikilink")
  - [飛騨市](../Page/飛騨市.md "wikilink")

## 關連項目

  - [日本國立公園](../Page/日本國立公園.md "wikilink")
  - [日本阿爾卑斯](../Page/日本阿爾卑斯.md "wikilink")
      - 北阿爾卑斯 - [飛驒山脈](../Page/飛驒山脈.md "wikilink")
      - 中央阿爾卑斯 - [木曾山脈](../Page/木曾山脈.md "wikilink")
      - 南阿爾卑斯 - [赤石山脈](../Page/赤石山脈.md "wikilink")
  - [南阿爾卑斯國立公園](../Page/南阿爾卑斯國立公園.md "wikilink") - 赤石山脈所在之國立公園
  - [富山地方鐵道](../Page/富山地方鐵道.md "wikilink")

## 参考资料

## 外部連結

  - [中部山岳国立公園](http://www.env.go.jp/park/chubu/index.html)

[Category:日本國立公園](../Category/日本國立公園.md "wikilink")
[Category:新潟縣地理](../Category/新潟縣地理.md "wikilink")
[Category:富山縣地理](../Category/富山縣地理.md "wikilink")
[Category:長野縣地理](../Category/長野縣地理.md "wikilink")
[Category:岐阜縣地理](../Category/岐阜縣地理.md "wikilink")
[Category:糸魚川市](../Category/糸魚川市.md "wikilink")
[Category:富山市](../Category/富山市.md "wikilink")
[Category:魚津市](../Category/魚津市.md "wikilink")
[Category:黑部市](../Category/黑部市.md "wikilink") [Category:朝日町
(富山縣)](../Category/朝日町_\(富山縣\).md "wikilink")
[Category:上市町](../Category/上市町.md "wikilink")
[Category:立山町](../Category/立山町.md "wikilink")
[Category:松本市](../Category/松本市.md "wikilink")
[Category:安曇野市](../Category/安曇野市.md "wikilink")
[Category:大町市](../Category/大町市.md "wikilink")
[Category:白馬村](../Category/白馬村.md "wikilink")
[Category:小谷村](../Category/小谷村.md "wikilink")
[Category:高山市](../Category/高山市.md "wikilink")
[Category:飛驒市](../Category/飛驒市.md "wikilink")
[Category:1934年設立的保護區](../Category/1934年設立的保護區.md "wikilink")
[Category:1934年日本建立](../Category/1934年日本建立.md "wikilink")