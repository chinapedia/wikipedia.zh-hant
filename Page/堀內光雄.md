**堀內光雄**，生于[山梨县](../Page/山梨县.md "wikilink")。是[日本](../Page/日本.md "wikilink")[政治家](../Page/政治家.md "wikilink")。

## 生平簡歷

毕业于[庆应义塾大学经济学部](../Page/庆应义塾大学.md "wikilink")。山梨的铁路公司[富士急行会长](../Page/富士急行.md "wikilink")，曾任[通商產業大臣](../Page/通商產業大臣.md "wikilink")、[自民党政策调查会会长](../Page/自由民主黨政策調查會.md "wikilink")，2000年在[旧桥本派骨干](../Page/旧桥本派.md "wikilink")[野中广务的拉拢下](../Page/野中广务.md "wikilink")，背叛本派会长决定打倒[森喜朗内阁的决定](../Page/森喜朗.md "wikilink")，从[加藤派中分裂出来](../Page/加藤派.md "wikilink")，成立崛内派，自任派系首领，是为党内第四大派系。2001年跟随[旧桥本派成为反对](../Page/旧桥本派.md "wikilink")[小泉純一郎的主要反对派之一](../Page/小泉純一郎.md "wikilink")。2005年因为反对邮政改革被小泉开除出党。[安倍晋三任首相时](../Page/安倍晋三.md "wikilink")，重新加入自民党，任[古贺派名誉会长](../Page/古贺派.md "wikilink")。2009年众议院大选落选。

[Category:慶應義塾大學校友](../Category/慶應義塾大學校友.md "wikilink")
[Category:日本企業家](../Category/日本企業家.md "wikilink")
[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:日本通商產業大臣](../Category/日本通商產業大臣.md "wikilink")
[Category:日本勞動大臣](../Category/日本勞動大臣.md "wikilink")
[Category:宇野內閣閣僚](../Category/宇野內閣閣僚.md "wikilink")
[Category:第二次橋本內閣閣僚](../Category/第二次橋本內閣閣僚.md "wikilink")
[Category:日本自由民主黨總務會長](../Category/日本自由民主黨總務會長.md "wikilink")
[Category:宏池會會長](../Category/宏池會會長.md "wikilink")
[Category:山梨縣出身人物](../Category/山梨縣出身人物.md "wikilink")
[Category:日本眾議院議員
1976–1979](../Category/日本眾議院議員_1976–1979.md "wikilink")
[Category:日本眾議院議員
1979–1980](../Category/日本眾議院議員_1979–1980.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:日本眾議院議員
2009–2012](../Category/日本眾議院議員_2009–2012.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")
[Category:山梨縣選出日本眾議院議員](../Category/山梨縣選出日本眾議院議員.md "wikilink")