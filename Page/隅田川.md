**隅田川**（）是位於[日本](../Page/日本.md "wikilink")[東京的一條](../Page/東京.md "wikilink")[河川](../Page/河川.md "wikilink")，為[荒川支流之一](../Page/荒川_\(關東\).md "wikilink")，全長23.5[公里](../Page/公里.md "wikilink")，屬於[日本政府列管的](../Page/日本政府.md "wikilink")[一級河川](../Page/一級河川.md "wikilink")。從[東京都](../Page/東京都.md "wikilink")[北區](../Page/北區_\(東京\).md "wikilink")開始與荒川分流，往東南向後在[南千住轉西南向](../Page/南千住.md "wikilink")，沿途與、、[神田川等支流河川會合](../Page/神田川.md "wikilink")，最後注入[東京灣](../Page/東京灣.md "wikilink")。古時有「墨田川」、「角田川」等稱呼，其中從以下的河段在[江戶時代被稱為](../Page/江戶時代.md "wikilink")「大川」。隅田川原為荒川主流的最下游河段，至[大正時代開鑿](../Page/大正.md "wikilink")[荒川放水路後被取代](../Page/荒川放水路.md "wikilink")。

## 歷史

隅田川最早為的下游河道，1629年因荒川改道而成為荒川之主流。在1683年（[貞享](../Page/貞享.md "wikilink")3年）前為[下總國與](../Page/下總國.md "wikilink")[武藏國的分界](../Page/武藏國.md "wikilink")，但另有一說是在[寬永年間](../Page/寬永.md "wikilink")（1622年-1643年）以前。

為了防患洪水，日本從[明治末期到](../Page/明治.md "wikilink")[昭和初期闢建](../Page/昭和.md "wikilink")「[荒川放水路](../Page/荒川放水路.md "wikilink")」，以岩淵水門為起點，將荒川的下游進行分流。1965年3月24日，日本政府正式將荒川放水路定為荒川主流，而將岩淵水門以下的荒川河道以俗稱改名為「隅田川」。

## 隅田川流域內的自治體

  - [東京都](../Page/東京都.md "wikilink")
      - [北區](../Page/北區_\(東京\).md "wikilink")
      - [足立區](../Page/足立區.md "wikilink")
      - [荒川區](../Page/荒川區.md "wikilink")
      - [墨田區](../Page/墨田區.md "wikilink")
      - [台東區](../Page/台東區.md "wikilink")
      - [江東區](../Page/江東區_\(東京\).md "wikilink")
      - [中央區](../Page/中央區_\(東京\).md "wikilink")

## 隅田川上的橋樑

[江戶時代由於防備上考量有架橋限制](../Page/江戶時代.md "wikilink")，[明治時期大多使用](../Page/明治.md "wikilink")[隅田川渡船](../Page/隅田川渡船.md "wikilink")。其後隨著交通量的增加而設置不少木橋，但於[關東大地震時大多被破壞](../Page/關東大地震.md "wikilink")，後來多改為鐵橋。

由於隅田川上的橋樑，所採用的橋體種類相當多元，也相當程度的表現了東京的都市變化，使得隅田川有「橋樑博物館」之稱號。

  - （[環七通](../Page/環七通.md "wikilink")）

  -
  - [新豐橋](../Page/新豐橋.md "wikilink")

  - [豐島橋](../Page/豐島橋.md "wikilink")（[東京都道307號王子金町江戶川線](../Page/東京都道307號王子金町江戶川線.md "wikilink")）

  - [首都高速](../Page/首都高速道路.md "wikilink")[中央環狀線橋梁](../Page/首都高速道路中央環狀線.md "wikilink")

  - （小台通 [東京都道458號白山小台線](../Page/東京都道458號白山小台線.md "wikilink")）

  - （尾久橋通 [東京都道・埼玉縣道58號台東鳩谷線](../Page/東京都道・埼玉縣道58號台東鳩谷線.md "wikilink")）

  - 新交通日暮里・舍人線隅田川橋梁（建設中）

  - （）

  - 上水千住水管橋（的上水管）

  - [京成電鐵隅田川橋梁](../Page/京成電鐵隅田川橋梁.md "wikilink")（[京成電鐵](../Page/京成電鐵.md "wikilink")[本線](../Page/京成本線.md "wikilink")）

  - [東京電力送電橋](../Page/東京電力.md "wikilink")

  - 千住水管橋（東京都水道局的工業用水管）

  - （[國道4號](../Page/國道4號.md "wikilink")・[日光街道](../Page/日光街道.md "wikilink")）

  - [常盤線隅田川橋梁](../Page/JR總武線隅田川橋梁.md "wikilink")（[JR東日本](../Page/東日本旅客鐵道.md "wikilink")[常磐線](../Page/常磐線.md "wikilink")）

  - [首都圈新都市鐵道隅田川橋梁](../Page/首都圈新都市鐵道隅田川橋梁.md "wikilink")（[首都圈新都市鐵道筑波快線](../Page/首都圈新都市鐵道筑波快線.md "wikilink")）

  - [日比谷線隅田川橋梁](../Page/東京地下鐵隅田川橋梁.md "wikilink")（[東京地下鐵](../Page/東京地下鐵.md "wikilink")[日比谷線](../Page/日比谷線.md "wikilink")）

  - （[東京都道314號言問大谷田線](../Page/東京都道314號言問大谷田線.md "wikilink")）

  - （[東京都道461號吾妻橋伊興町線支線](../Page/東京都道461號吾妻橋伊興町線.md "wikilink")）

  - （[明治通](../Page/明治通.md "wikilink")）

  - [櫻橋](../Page/櫻橋_\(東京都\).md "wikilink")

  - [東武花川戶鐵道橋](../Page/東武鐵道隅田川橋梁.md "wikilink")（[東武鐵道](../Page/東武鐵道.md "wikilink")[伊勢崎線](../Page/伊勢崎線.md "wikilink")）

  - （[國道6號](../Page/國道6號.md "wikilink")・）

  - （[雷門通](../Page/雷門通.md "wikilink")）

  - （[淺草通](../Page/淺草通.md "wikilink")）

  - （[春日通](../Page/春日通.md "wikilink")）

  - [藏前橋](../Page/藏前橋.md "wikilink")（[藏前橋通](../Page/藏前橋通.md "wikilink")）

  - 藏前專用橋（[NTT電話通信線](../Page/日本電信電話.md "wikilink")）

  - [總武線隅田川橋梁](../Page/JR總武線隅田川橋梁.md "wikilink")（JR東日本[總武線](../Page/總武線.md "wikilink")）

  - [兩國橋](../Page/兩國橋.md "wikilink")（[國道14號](../Page/國道14號.md "wikilink")、[京葉道路](../Page/京葉道路.md "wikilink")）

  - [首都高速](../Page/首都高速道路.md "wikilink")[6號向島線橋梁](../Page/首都高速道路6號向島線.md "wikilink")（[兩國交流道](../Page/兩國JCT.md "wikilink")）

  - （）

  - （）

  - （[水天宮通](../Page/水天宮通.md "wikilink")・[首都高速](../Page/首都高速道路.md "wikilink")[9號深川線下](../Page/首都高速道路9號深川線.md "wikilink")）

  - （）

  - （[清澄通](../Page/清澄通.md "wikilink")）

  - （）

  - （[東京都道473號新富晴海線](../Page/東京都道473號新富晴海線.md "wikilink")）

  - [勝鬨橋](../Page/勝鬨橋.md "wikilink")（[晴海通](../Page/晴海通.md "wikilink")）

## 河川環境・生態系

[冬季有](../Page/冬季.md "wikilink")[紅嘴鷗飛來過冬](../Page/紅嘴鷗.md "wikilink")。

## 關連河川

  - [新河岸川](../Page/新河岸川.md "wikilink")
  - [神田川](../Page/神田川.md "wikilink")
  - [石神井川](../Page/石神井川.md "wikilink")
  - [日本橋川](../Page/日本橋川.md "wikilink")
  - [龜島川](../Page/龜島川.md "wikilink")

## 參見

  - [水上計程車](../Page/水上計程車.md "wikilink")
  - [隅田川花火大會](../Page/隅田川花火大會.md "wikilink")
  - [荒川 (關東)](../Page/荒川_\(關東\).md "wikilink")
  - [隅田川站](../Page/隅田川站.md "wikilink")

[Category:東京都河川](../Category/東京都河川.md "wikilink")