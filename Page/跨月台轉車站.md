[InterchangeStation.png](https://zh.wikipedia.org/wiki/File:InterchangeStation.png "fig:InterchangeStation.png")
[Cross-platform_interchange_on_the_B3_platform_in_Guting_Station.jpg](https://zh.wikipedia.org/wiki/File:Cross-platform_interchange_on_the_B3_platform_in_Guting_Station.jpg "fig:Cross-platform_interchange_on_the_B3_platform_in_Guting_Station.jpg")[古亭站的月台](../Page/古亭站.md "wikilink")。\]\]

**跨月台轉車站**是一種[城市軌道交通系統應用於](../Page/城市軌道交通系統.md "wikilink")[轉車站內的設計](../Page/換乘站.md "wikilink")，其目的是在同一個[島式月台上讓](../Page/島式月台.md "wikilink")[通勤者從一個路線系統的列車的月台下車後](../Page/通勤.md "wikilink")，直接步行到對面另一條路線的月台轉車（在[重鐵的原則上這兩個系統的路軌在通勤時是不交匯的](../Page/重鐵.md "wikilink")，不過可以在興建時刻意遷就）。乘客不需經過[樓梯](../Page/樓梯.md "wikilink")、[電動扶梯或](../Page/電動扶梯.md "wikilink")[電梯走到另一個](../Page/電梯.md "wikilink")-{平台}-轉線，因此能節省車站內的轉乘時間、提高通勤效率。跨月台轉車站還有一個設立原則就是不要求乘客在轉車時經過計算車費的[驗票閘門](../Page/驗票閘門.md "wikilink")，[英國](../Page/英國.md "wikilink")[倫敦的](../Page/倫敦.md "wikilink")[斯特拉特福站就是由兩個不同鐵路經營者](../Page/斯特拉特福站.md "wikilink")（東安格利亞國家特快車及[倫敦地鐵](../Page/倫敦地鐵.md "wikilink")）允許乘客不經過柵閘就進行轉車的典型例子。

跨月台轉車站的優點是對於換線的乘客而言是較快速而簡單的設計（若撇除[直通運行](../Page/直通運行.md "wikilink")），但缺點是為了配合月台設置，各線的行車軌道有時必須透過立體交叉，才能銜接至車站中不同位置的月台。當採用不在同一平面的叠島式設計，建造時比分離式轉車站減少挖泥數量，也相對減少車站建造面積，從而比分離式轉車站節省兩倍或以上的建築及維護成本。然而可能要在地下[隧道的路段遷就跨月台轉車的設計](../Page/隧道.md "wikilink")，為遷就乘客的轉車組合令隧道的施工難度稍為增加。在[臺北捷運](../Page/臺北捷運.md "wikilink")、[杭州地鐵等地的工程中](../Page/杭州地鐵.md "wikilink")，這種因應跨月台轉車設計而生成的立體交叉隧道被暱稱作「[麻花](../Page/麻花.md "wikilink")」\[1\]\[2\]。此外對乘客而言這種設計讓他們轉車來得更直接，不用像分離式轉車站繞幾個大圏子才能轉車，因為同月台兩側是不同的路線，而同一路線另一個方向就是佈置在不同月台上，所以有機會令不熟悉該鐵路系統的乘客產生疑惑而造成搭錯車的可能（如[港鐵](../Page/港鐵.md "wikilink")[金鐘站](../Page/金鐘站.md "wikilink")[南港島綫月台轉乘其他路綫](../Page/南港島綫.md "wikilink")），因此採用此設計的車站必須要有更明確的轉乘指示，如清晰的指示牌；如果有不同顏色的車身來辨認，效果則更佳。

## 月台設置

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>香港鐵路太子至油麻地站路軌月台配置圖，留意<a href="../Page/觀塘綫.md" title="wikilink">觀塘綫及</a><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫在</a><a href="../Page/旺角站.md" title="wikilink">旺角站及</a><a href="../Page/太子站.md" title="wikilink">太子站的雙胞胎跨月台轉車服務</a>。</p></td>
</tr>
</tbody>
</table>

一個擁有跨月台轉車站的通勤列車站，一般會包含最少兩組雙向路線系統，並最少擁有兩座透過行人樓梯、電扶梯等連接的分離的島式月台（可服務4條單線），在客量高的轉車站容納更多的路線可能會導致月台人流飽和及混亂。有時受到地面腹地大小的影響，或為了縮短需於兩座月台間旅客換線的時間、距離，會建造成[島式疊式月台的形式](../Page/島式疊式月台.md "wikilink")，使兩月台間往返僅需上／下樓一次。

世界上大部分通勤鐵路系統如果因為多種原因而只能為兩條路線建設單一個跨月台轉車站，那麼它們在一般情況下都會選擇讓同方向的軌道配對在同一個島式月台上，因為進行同方向轉車的乘客通常要比逆方向的多。然而有時部分車站尚會為了順應乘客量較高一方的動線需求，而被調整為一個逆方向跨月台轉車站，月台兩側的行車方向相反。

而為了同時方便順向及逆向的轉乘，有些系統設計以兩個相鄰車站進行轉乘，在其中一個車站進行順向轉乘，而另一個車站進行逆向，不但方便旅客，也分散了人流。[新加坡地鐵即有一組雙胞胎跨月台轉車站](../Page/新加坡地鐵.md "wikilink")——[东西线](../Page/新加坡地铁东西线.md "wikilink")、[南北线間的](../Page/新加坡地铁南北线.md "wikilink")[政府大廈站及](../Page/政府大廈地鐵站.md "wikilink")[萊佛士坊站](../Page/萊佛士坊地鐵站.md "wikilink")，[港鐵則有兩組](../Page/港鐵.md "wikilink")——第一組是[觀塘綫](../Page/觀塘綫.md "wikilink")、[將軍澳綫的](../Page/將軍澳綫.md "wikilink")[油塘站及](../Page/油塘站.md "wikilink")[調景嶺站](../Page/調景嶺站.md "wikilink")；第二組是[觀塘綫](../Page/觀塘綫.md "wikilink")、[荃灣綫的](../Page/荃灣綫.md "wikilink")[太子站及](../Page/太子站.md "wikilink")[旺角站](../Page/旺角站.md "wikilink")。

## 各地應用

### 亞洲

####  東京

[Rail_Tracks_map_Tokyo_Metro_Akasaka-mitsuke_Station.svg](https://zh.wikipedia.org/wiki/File:Rail_Tracks_map_Tokyo_Metro_Akasaka-mitsuke_Station.svg "fig:Rail_Tracks_map_Tokyo_Metro_Akasaka-mitsuke_Station.svg")

在東京地下鐵的跨月台轉車站中，[赤坂見附站透過兩層島式月台](../Page/赤坂見附站.md "wikilink")，提供[銀座線與](../Page/銀座線.md "wikilink")[丸之內線之間的順向跨月台轉乘](../Page/丸之內線.md "wikilink")，上層月台能前往[新宿](../Page/新宿站.md "wikilink")、[荻窪](../Page/荻窪站.md "wikilink")／[澀谷方向](../Page/澀谷站.md "wikilink")，下層則前往[銀座](../Page/銀座站_\(日本\).md "wikilink")、[上野](../Page/上野站.md "wikilink")、[淺草](../Page/淺草站.md "wikilink")/[池袋方向](../Page/池袋站.md "wikilink")。

[表參道站](../Page/表參道站.md "wikilink")（銀座線及[半藏門線](../Page/半藏門線.md "wikilink")）、[白金高輪站](../Page/白金高輪站.md "wikilink")（[南北線](../Page/南北線_\(東京地下鐵\).md "wikilink")、[三田線](../Page/三田線_\(都營地下鐵\).md "wikilink")）兩站則是設置兩座水平並列的島式月台，方便乘客在此轉乘。

大部分東京地下鐵或都營地下鐵與其他直通運行的近郊鐵路的連接車站，如[中目黑站](../Page/中目黑站.md "wikilink")（地鐵[日比谷線](../Page/日比谷線.md "wikilink")、東急[東橫線](../Page/東橫線.md "wikilink")）、[和光市站](../Page/和光市站.md "wikilink")（地鐵[有樂町線](../Page/有樂町線.md "wikilink")、地鐵[副都心線](../Page/副都心線.md "wikilink")、東武[東上線](../Page/東上線.md "wikilink")）亦採用跨月台轉乘。

東京急行電鐵將若干車站地下化的同時，也一併將那些車站變成跨月台轉車站，例如[大岡山站](../Page/大岡山站.md "wikilink")（[目黑線](../Page/目黑線.md "wikilink")、[大井町線](../Page/大井町線.md "wikilink")）、[田園調布站](../Page/田園調布站.md "wikilink")（[東橫線](../Page/東橫線.md "wikilink")、[目黑線](../Page/目黑線.md "wikilink")）、[日吉站](../Page/日吉站_\(神奈川縣\).md "wikilink")（[東橫線](../Page/東橫線.md "wikilink")、[目黑線](../Page/目黑線.md "wikilink")）等。

####  大阪

[大阪市高速電氣軌道的](../Page/大阪市高速電氣軌道.md "wikilink")[御堂筋線和](../Page/御堂筋線.md "wikilink")[四橋線在](../Page/四橋線.md "wikilink")[大國町站設有跨月台轉乘](../Page/大國町站.md "wikilink")。

[近畿日本鐵道同樣在](../Page/近畿日本鐵道.md "wikilink")[奈良線和](../Page/奈良線_\(近畿日本鐵道\).md "wikilink")[大阪線的](../Page/大阪線.md "wikilink")[鶴橋站設有跨月台轉乘](../Page/鶴橋站.md "wikilink")。

[阪神電氣鐵道在](../Page/阪神電氣鐵道.md "wikilink")[阪神難波線和](../Page/阪神難波線.md "wikilink")[阪神本線間的](../Page/阪神本線.md "wikilink")也設有跨月台轉乘。

####  京都

[京都市營地下鐵](../Page/京都市營地下鐵.md "wikilink")[東西線與](../Page/東西線_\(京都市營地下鐵\).md "wikilink")[京阪電氣鐵道](../Page/京阪電氣鐵道.md "wikilink")[京津線可在](../Page/京津線.md "wikilink")[御陵站進行跨月台轉乘](../Page/御陵站.md "wikilink")。

####  香港

[MTR_multiple_cross_platform_interchange.svg](https://zh.wikipedia.org/wiki/File:MTR_multiple_cross_platform_interchange.svg "fig:MTR_multiple_cross_platform_interchange.svg")
香港鐵路是世界上最繁忙、擁擠的都市鐵道系統之一，因此在跨月台轉車設計的應用上較其他城市更為廣泛，在大量的車站中均得到採取，以節省乘客換車的時間和徒步距離、增加系統運作效率。

在[香港地鐵的路線當中](../Page/港鐵.md "wikilink")，跨月台轉乘首次出現於[荃灣綫通車後的](../Page/荃灣綫.md "wikilink")[太子站和](../Page/太子站.md "wikilink")[旺角站](../Page/旺角站.md "wikilink")。兩站可進行雙胞胎跨月台轉車，太子站進行反向轉乘，而旺角站進行順向轉乘。由於兩個方向都預期出現大量人流，因此當局對此進行了特別設計。

由於這個設計深受乘客歡迎，於是[港島綫通車時](../Page/港島綫.md "wikilink")，[金鐘站也進行了類似的反向轉乘](../Page/金鐘站.md "wikilink")。2002年，[油塘站和](../Page/油塘站.md "wikilink")[調景嶺站亦採用](../Page/調景嶺站.md "wikilink")[太子站和](../Page/太子站.md "wikilink")[旺角站的配置](../Page/旺角站.md "wikilink")。

[馬鞍山綫採用特別的](../Page/馬鞍山綫.md "wikilink")[靠右通行](../Page/道路通行方向.md "wikilink")，以便乘客在[大圍站轉乘](../Page/大圍站.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")。

另一個跨月台轉車站是[荃灣綫和](../Page/荃灣綫.md "wikilink")[東涌綫的](../Page/東涌綫.md "wikilink")[荔景站](../Page/荔景站.md "wikilink")，該站可服務四個方向的跨月台轉乘。而[北角站亦可提供](../Page/北角站.md "wikilink")[將軍澳綫和](../Page/將軍澳綫.md "wikilink")[港島綫之間的轉乘](../Page/港島綫.md "wikilink")，不過兩個月台之間的距離較大。

2007年[兩鐵合併以前](../Page/兩鐵合併.md "wikilink")，[南昌站同時由](../Page/南昌站_\(香港\).md "wikilink")[東涌綫和](../Page/東涌綫.md "wikilink")[九廣西鐵服務](../Page/西鐵綫.md "wikilink")。由於票務系統不一，南行東涌綫和北行西鐵雖然位於同一平面上，但仍受牆壁阻隔。合併後，部分牆壁拆除以便提供跨月台轉乘。不過，這個配置並非主要客流方向。多數的乘客皆為[西鐵綫出發前往西九龍和香港島](../Page/西鐵綫.md "wikilink")，或是[東涌綫前往尖沙咀](../Page/東涌綫.md "wikilink")，大部分乘客仍要途經大堂轉乘。

[九龍南綫通車時](../Page/九龍南綫.md "wikilink")，東鐵綫和西鐵綫都以[紅磡站為總站](../Page/紅磡站.md "wikilink")，並提供跨月台轉乘。沙中綫全線通車後預定改為垂直轉乘。另外，沙中綫當中的[會展站也預定成為](../Page/會展站.md "wikilink")[東鐵綫與](../Page/東鐵綫.md "wikilink")[將軍澳綫的轉乘站](../Page/將軍澳綫.md "wikilink")。

乘客可以在東涌綫[欣澳站跨月台轉乘](../Page/欣澳站.md "wikilink")[迪士尼綫列車](../Page/迪士尼綫.md "wikilink")。不過反之不亦然，由迪士尼回到市區需要跨過天橋乘搭往[香港站的列車](../Page/香港站.md "wikilink")。

香港亦有部分車站曾經計劃為跨月台轉車，但為減少工程經費而放棄，如[香港大學站](../Page/香港大學站.md "wikilink")、[黃竹坑站](../Page/黃竹坑站.md "wikilink")、[鑽石山站等](../Page/鑽石山站.md "wikilink")。

####  臺北

[Cross-platform_transfer_in_Taipei_metro.png](https://zh.wikipedia.org/wiki/File:Cross-platform_transfer_in_Taipei_metro.png "fig:Cross-platform_transfer_in_Taipei_metro.png")

[臺北捷運中的跨月台轉乘設計](../Page/臺北捷運.md "wikilink")，始於1980年代的設計階段，中華民國交通部運輸計劃委員會聘請「英國大眾捷運顧問工程司」（BMTC）與[中華顧問工程司](../Page/中華顧問工程司.md "wikilink")（今臺灣世曦工程顧問股份有限公司）組成計畫小組進行設計。其中BMTC部分人員為曾經參與香港地鐵\[3\]設計的[英國籍](../Page/英國人.md "wikilink")[工程師](../Page/工程師.md "wikilink")，故將香港的同月台轉乘設計加入臺北捷運當中，取代了原先部分交會站必須上下樓層、徒步進行轉乘的動線。

開工之後，共興建了4座跨月台轉車站。有別於2個站構成的雙胞胎轉車組，臺北捷運是由3座兩兩相鄰的車站——[中正紀念堂站](../Page/中正紀念堂站.md "wikilink")、[古亭站及](../Page/古亭站.md "wikilink")[東門站](../Page/東門站_\(台北市\).md "wikilink")，形成「三角轉車組」。[西門站則是除此轉車組外的第](../Page/西門站.md "wikilink")4個轉乘站。

東門站為橘線及紅線之轉車站，BMTC原先規劃橘線及紅線另一轉車站[民權西路站亦為跨月台轉乘設計](../Page/民權西路站.md "wikilink")，後來實際興建時取消，標誌臺北捷運的跨月台轉車服務不會形成雙胞胎轉車組。

####  新加坡

除政府大廈站、萊佛士坊站的雙層島式疊式月台外，[裕廊東站興建了兩座島式月台](../Page/裕廊東地鐵站.md "wikilink")，兩者均為南北線、東西線間的轉乘平台。

[丹那美拉站為純粹的東西線車站](../Page/丹那美拉站.md "wikilink")，但東西線中即有主線列車及[樟宜機場支線列車兩種營運模式行經此處](../Page/新加坡地鐵東西線.md "wikilink")，而該站提供兩種列車間的跨月台轉乘。

[海灣舫站則是](../Page/海灣舫地鐵站.md "wikilink")[环线與](../Page/新加坡地铁环线.md "wikilink")[滨海市区线間的跨月台轉車站](../Page/新加坡地铁滨海市区线.md "wikilink")。

####  北京

[郭公庄站](../Page/郭公庄站.md "wikilink")、[国家图书馆站](../Page/国家图书馆站.md "wikilink")、[朱辛庄站](../Page/朱辛庄站.md "wikilink")、[北京西站](../Page/北京西站.md "wikilink")、[阎村东站为双岛四线同台换乘](../Page/阎村东站.md "wikilink")，[南锣鼓巷站为侧叠式站台](../Page/南锣鼓巷站.md "wikilink")，藉由通道连接。

####  天津

[天津站站](../Page/天津站站.md "wikilink")2号线滨海国际机场方向与9号线同台换乘，[肿瘤医院站](../Page/肿瘤医院站.md "wikilink")、[天津宾馆站](../Page/天津宾馆站.md "wikilink")5号线与6号线双同台换乘。

####  上海

[上海軌道交通系統內位於](../Page/上海軌道交通.md "wikilink")[浦東新區的](../Page/浦東新區.md "wikilink")[東方體育中心站為上海首次採用該種轉乘設計的車站](../Page/東方體育中心站.md "wikilink")。供[上海軌道交通6號線及](../Page/上海軌道交通6號線.md "wikilink")[上海軌道交通11號線的乘客進行跨月台轉乘](../Page/上海軌道交通11號線.md "wikilink")。[虹桥火车站站为](../Page/虹桥火车站站.md "wikilink")[上海轨道交通2号线和](../Page/上海轨道交通2号线.md "wikilink")[上海轨道交通17号线的同台换乘站](../Page/上海轨道交通17号线.md "wikilink")，[虹桥2号航站楼站为](../Page/虹桥2号航站楼站.md "wikilink")[上海轨道交通2号线和](../Page/上海轨道交通2号线.md "wikilink")[上海轨道交通10号线往市区方向的同台换乘站](../Page/上海轨道交通10号线.md "wikilink")，通过中间大站台换乘。

####  杭州

[East_Railway_Station,_Hangzhou_Metro,_2014-02-06_03.JPG](https://zh.wikipedia.org/wiki/File:East_Railway_Station,_Hangzhou_Metro,_2014-02-06_03.JPG "fig:East_Railway_Station,_Hangzhou_Metro,_2014-02-06_03.JPG")同台换乘站台，左侧为4号线，右侧为1号线\]\]
[杭州地铁](../Page/杭州地铁.md "wikilink")[火车东站](../Page/火车东站站_\(杭州地铁\).md "wikilink")、[彭埠站是](../Page/彭埠站.md "wikilink")1号线和4号线的双同台换乘站，[钱江路站是](../Page/钱江路站.md "wikilink")2号线和4号线的同台换乘站，均为双岛四线。

####  廣州

在[广州地鐵系統裡的](../Page/广州地鐵.md "wikilink")[嘉禾望岗站和](../Page/嘉禾望岗站.md "wikilink")[沙园站](../Page/沙园站.md "wikilink")、[广州南站亦為跨月台转车站的設計](../Page/广州南站_\(地铁\).md "wikilink")。這些車站分别為[2号線](../Page/廣州地鐵2號線.md "wikilink")、[3号線和](../Page/廣州地鐵3號線.md "wikilink")[廣州地鐵8號線](../Page/廣州地鐵8號線.md "wikilink")、[广佛线及](../Page/广佛线.md "wikilink")[2号線](../Page/廣州地鐵2號線.md "wikilink")、[7号線的跨月台轉車站](../Page/廣州地鐵7號線.md "wikilink")，未来还将有[彩虹桥站](../Page/彩虹桥站.md "wikilink")、[天河公园站](../Page/天河公园站.md "wikilink")、[植物园站](../Page/植物园站_\(广州\).md "wikilink")、[新塘站](../Page/新塘站_\(广州地铁\).md "wikilink")。

####  深圳

在深圳地鐵二期系統裡的[老街站和](../Page/老街站.md "wikilink")[黃貝嶺站為跨月台转车站的設計](../Page/黃貝嶺站.md "wikilink")，其中[老街站原为](../Page/老街站.md "wikilink")[侧式叠式月台](../Page/侧式叠式月台.md "wikilink")，[深圳地鐵3号綫开通后](../Page/深圳地鐵3号綫.md "wikilink")，原来[1号綫南面的侧式月台被关闭](../Page/深圳地鐵1号綫.md "wikilink")，而在北面增建岛式月台，形成[島式疊式月台](../Page/島式疊式月台.md "wikilink")；[黃貝嶺站是](../Page/黃貝嶺站.md "wikilink")[2号綫和](../Page/深圳地鐵2号綫.md "wikilink")[5号綫的轉乘站](../Page/深圳地鐵5号綫.md "wikilink")。三期系統通車後，新建的[紅樹灣南站和](../Page/紅樹灣南站.md "wikilink")[車公廟站亦是跨月台轉車站](../Page/車公廟站_\(深圳\).md "wikilink")，其中[紅樹灣南站為](../Page/紅樹灣南站.md "wikilink")[9号綫與](../Page/深圳地鐵9号綫.md "wikilink")[11号綫的轉乘站](../Page/深圳地鐵11号綫.md "wikilink")，而[车公庙站中香蜜湖路下的月台則是](../Page/车公庙站_\(深圳\).md "wikilink")[7号綫與](../Page/深圳地鐵7号綫.md "wikilink")[9号綫的轉乘站](../Page/深圳地鐵9号綫.md "wikilink")。

####  成都

[Xipu_Metro_Line.jpg](https://zh.wikipedia.org/wiki/File:Xipu_Metro_Line.jpg "fig:Xipu_Metro_Line.jpg")的同台换乘，位于中间的复线铁路为[成都地铁下属](../Page/成都地铁.md "wikilink")[成都地铁2号线](../Page/成都地铁2号线.md "wikilink")，两侧的复线铁路为[国铁下属](../Page/中华人民共和国铁路运输.md "wikilink")[成灌铁路](../Page/成灌铁路.md "wikilink")。\]\]
[成都地铁运营的](../Page/成都地铁.md "wikilink")[成都地铁2号线和](../Page/成都地铁2号线.md "wikilink")[国铁](../Page/中华人民共和国铁路运输.md "wikilink")[成灌铁路于](../Page/成灌铁路.md "wikilink")[犀浦站同台换乘](../Page/犀浦站.md "wikilink")。这是中国大陸首个[城市轨道交通与](../Page/城市轨道交通.md "wikilink")[国家铁路同台换乘的案例](../Page/中华人民共和国铁路运输.md "wikilink")\[4\]。同时，[成都地铁](../Page/成都地铁.md "wikilink")[2号线与](../Page/成都地铁2号线.md "wikilink")[4号线在](../Page/成都地铁4号线.md "wikilink")[中医大省医院站同台换乘](../Page/中医学院站.md "wikilink")，[太平园站](../Page/太平园站.md "wikilink")10号线与3号线进城方向同台换乘，在未来规划中，[5号线与](../Page/成都地铁5号线.md "wikilink")[6号线也将在](../Page/成都地铁6号线.md "wikilink")[西北桥站站同台换乘](../Page/西北桥站.md "wikilink")。

####  武汉

[English_zhongnan_&_hongguang.svg](https://zh.wikipedia.org/wiki/File:English_zhongnan_&_hongguang.svg "fig:English_zhongnan_&_hongguang.svg")
武汉地铁系统中跨月台转车站较为普遍。得益于设计的前瞻性，系统之中共有五个此类型车站。[武汉地铁2号线与](../Page/武汉地铁2号线.md "wikilink")[4号线的](../Page/武汉地铁4号线.md "wikilink")[洪山广场站与](../Page/洪山广场站.md "wikilink")[中南路站皆为](../Page/中南路站.md "wikilink")“跨月台转车站”，可进行“连续同台换乘”。为了方便去行及回行两个方向两条线路的换乘，这两个车站设计成了以其中一个车站进行去行方向换乘，而另一个车站进行回行方向的换乘。换乘方式类似[港鐵的](../Page/港鐵.md "wikilink")[觀塘綫及](../Page/觀塘綫.md "wikilink")[荃灣綫的](../Page/荃灣綫.md "wikilink")[太子站及](../Page/太子站.md "wikilink")[旺角站](../Page/旺角站.md "wikilink")。\[5\]

除了洪山广场站、中南路站连续同台换乘站之外，[宏图大道站](../Page/宏图大道站.md "wikilink")、[钟家村站和](../Page/钟家村站_\(武汉地铁\).md "wikilink")[香港路站均采用了这种换乘形式](../Page/香港路站.md "wikilink")。

####  南京

[201704_Northbound_Platform_at_Nanjing_South_Railway_Station.jpg](https://zh.wikipedia.org/wiki/File:201704_Northbound_Platform_at_Nanjing_South_Railway_Station.jpg "fig:201704_Northbound_Platform_at_Nanjing_South_Railway_Station.jpg")与[3号线在](../Page/南京地铁3号线.md "wikilink")[南京南站实施同台换乘](../Page/南京南站_\(南京地铁\).md "wikilink")\]\]

[南京地铁1号线与](../Page/南京地铁1号线.md "wikilink")[南京地铁3号线可在](../Page/南京地铁3号线.md "wikilink")[南京南站进行同台换乘](../Page/南京南站_\(南京地铁\).md "wikilink")，[南京地铁S1号线与](../Page/南京地铁S1号线.md "wikilink")[南京地铁S3号线也可在](../Page/南京地铁S3号线.md "wikilink")[南京南站同台换乘](../Page/南京南站_\(南京地铁\).md "wikilink")，[南京地铁S1号线与](../Page/南京地铁S1号线.md "wikilink")[南京地铁S9号线在](../Page/南京地铁S9号线.md "wikilink")[翔宇路南站同台换乘](../Page/翔宇路南站.md "wikilink")\[6\]，[南京地铁S1号线与](../Page/南京地铁S1号线.md "wikilink")[南京地铁S7号线在](../Page/南京地铁S7号线.md "wikilink")[空港新城江宁站同台换乘](../Page/空港新城江宁站.md "wikilink")\[7\]。

####  郑州

[2号线与](../Page/郑州地铁2号线.md "wikilink")[9号线可在十八里河站同台换乘](../Page/郑州地铁9号线.md "wikilink")，这也是郑州地铁第一个同台换乘车站。（目前9号线首通段称为城郊线与2号线贯通运营，暂时只启用2号线站台。）
另外，[5号线与](../Page/郑州地铁5号线.md "wikilink")[8号线在郑州东站](../Page/郑州地铁8号线.md "wikilink")；[4号线与](../Page/郑州地铁4号线.md "wikilink")[7号线在张家村站均可实现同台换乘](../Page/郑州地铁7号线.md "wikilink")。

  - 这三个车站均为双岛式站台。
  - 对于十八里河站，2号线使用内侧的2·3站台，9号线使用外侧的1·4站台。9号线处于规划中。十八里河站为2号线的设计终点站，车站南端设有2号线折返线。（目前2号线终点站为南四环站）
  - 对于郑州东站，5号线使用外侧的3·6站台，8号线使用内侧的4·5站台。（1号线为单独的岛式站台\[1·2站台\]，与3～6站台之间有换乘通道连接）5号线将于2019年4月开通，8号线正在筹划建设。
  - 对于张家村站，4号线使用内侧的2·3站台，7号线使用外侧的1·4站台。4号线将于2020年开通，7号线正在筹划建设。

####  青岛

  - [1号线](../Page/青岛地铁1号线.md "wikilink")（内侧）与[6号线](../Page/青岛地铁6号线.md "wikilink")（外侧）可在[峨眉山路站进行同台换乘](../Page/峨眉山路站.md "wikilink")。
  - [1号线](../Page/青岛地铁1号线.md "wikilink")（内侧）与[7号线](../Page/青岛地铁7号线.md "wikilink")（外侧）可在[兴国路站进行同台换乘](../Page/兴国路站.md "wikilink")。
  - [2号线](../Page/青岛地铁2号线.md "wikilink")（外侧）与[3号线](../Page/青岛地铁3号线.md "wikilink")（内侧）可在[五四广场站进行同台换乘](../Page/五四广场站.md "wikilink")。
  - [3号线](../Page/青岛地铁3号线.md "wikilink")（内侧）与[8号线](../Page/青岛地铁8号线.md "wikilink")（外侧）可在[青岛北站进行同台换乘](../Page/青岛北站.md "wikilink")。
  - [8号线](../Page/青岛地铁8号线.md "wikilink")（外侧）与[9号线](../Page/青岛地铁9号线.md "wikilink")（内侧）可在[科技馆站进行同台换乘](../Page/科技馆站.md "wikilink")。
  - [8号线](../Page/青岛地铁8号线.md "wikilink")（外侧）与[12号线](../Page/青岛地铁12号线.md "wikilink")（内侧）可在[红岛站进行同台换乘](../Page/红岛站.md "wikilink")。
  - [8号线](../Page/青岛地铁8号线.md "wikilink")（内侧）与[14号线](../Page/青岛地铁14号线.md "wikilink")（外侧）可在[胶州北站进行同台换乘](../Page/胶州北站.md "wikilink")。
  - [12号线](../Page/青岛地铁12号线.md "wikilink")（外侧）与[13号线](../Page/青岛地铁13号线.md "wikilink")（内侧）可在[嘉陵江路站进行同台换乘](../Page/嘉陵江路站.md "wikilink")。

####  南宁

[南宁地铁1号线与](../Page/南宁地铁1号线.md "wikilink")[南宁地铁2号线可在](../Page/南宁地铁2号线.md "wikilink")[火车站与](../Page/火车站_\(南宁轨道交通\).md "wikilink")[朝阳广场站双同台换乘](../Page/朝阳广场站.md "wikilink")。

####  昆明

[昆明地铁1号线与](../Page/昆明地铁1号线.md "wikilink")[昆明地铁2号线在](../Page/昆明地铁2号线.md "wikilink")[环城南路站同台换乘](../Page/环城南路站.md "wikilink")（目前两线贯通运营，拆解后将同台换乘）。

####  首爾

[首爾地鐵](../Page/首爾地鐵.md "wikilink")[衿井站提供](../Page/衿井站.md "wikilink")[1號線各停列車和](../Page/首都圈電鐵1號線.md "wikilink")[4號線所有列車的跨月台轉乘](../Page/首都圈電鐵4號線.md "wikilink")。而[金浦機場站亦提供](../Page/金浦機場站.md "wikilink")[首爾地鐵9號線所有列車和](../Page/首爾地鐵9號線.md "wikilink")[仁川國際機場鐵道各停列車的跨月台轉乘](../Page/仁川國際機場鐵道.md "wikilink")。

####  吉隆坡

[Putra_Heights_LRT_Terminal.jpg](https://zh.wikipedia.org/wiki/File:Putra_Heights_LRT_Terminal.jpg "fig:Putra_Heights_LRT_Terminal.jpg")站同台换乘站台，左侧为[格拉那再也线](../Page/格拉那再也线.md "wikilink")、右侧为[大城堡线](../Page/大城堡线.md "wikilink")\]\]
布特拉高原站是[大城堡线](../Page/大城堡线.md "wikilink")（4号线）和[格拉那再也线](../Page/格拉那再也线.md "wikilink")（5号线）之间的同台换乘站，而[陈秀连站则是](../Page/陈秀连站.md "wikilink")[安邦线](../Page/安邦线.md "wikilink")（3号线）和[大城堡线](../Page/大城堡线.md "wikilink")（4号线）之间的同台换乘站

####  曼谷

[曼谷大眾運輸系統的](../Page/曼谷大眾運輸系統.md "wikilink")[暹羅車站是一座高架的跨月台轉車站](../Page/暹羅車站.md "wikilink")，提供[是隆線及](../Page/是隆線.md "wikilink")[素坤逸線的順向跨月台轉乘](../Page/素坤逸線.md "wikilink")。

### 歐洲

####  倫敦

倫敦地鐵的車站中，[貝克盧線上的兩座大站](../Page/貝克盧線.md "wikilink")：[牛津圓環站](../Page/牛津圓環站.md "wikilink")（Oxford
Circus）及[貝克街站](../Page/貝克街站.md "wikilink")（Baker
Street）均設有能讓乘客在同一月台上平行轉乘的機制。前者與[維多利亞線間為同方向的跨月台轉乘](../Page/維多利亞線.md "wikilink")，後者則可與[-{zh-hans:银禧线;
zh-hant:銀禧線;
zh-tw:朱比利線;}-在島式月台上順向平行轉車](../Page/朱比利線.md "wikilink")。此兩站內其他路線的部份則無跨月台平行轉乘的設計。

[芬奇萊路站](../Page/芬奇萊路站.md "wikilink")（Finchley
Road）亦以島式月台平面連接[大都會線及](../Page/大都會線.md "wikilink")[-{zh-hans:银禧线;
zh-hant:銀禧線;
zh-tw:朱比利線;}-的軌道](../Page/朱比利線.md "wikilink")；在[哩尾站](../Page/哩尾站.md "wikilink")（Mile
End），共線運行的[區域線](../Page/區域線.md "wikilink")、[漢默史密斯及城市線透過跨月台與](../Page/漢默史密斯及城市線.md "wikilink")[中央線連接](../Page/中央線_\(倫敦地鐵\).md "wikilink")。

####  巴黎

巴黎地鐵的[拉莫特-皮凱-格勒內勒站](../Page/拉莫特-皮凱-格勒內勒站.md "wikilink")，8號線和10號線可以跨月台轉乘。

####  阿姆斯特丹

[阿姆斯特丹地鐵在](../Page/阿姆斯特丹地鐵.md "wikilink")（Van der
Madeweg）設有50號線和53號線的跨月台轉乘，（Amsterdam
Zuid）亦設50號線和52號線的跨月台轉乘。除此以外，（Amstel）提供地鐵（51、53、54號線）與[鐵路間的跨月台轉乘](../Page/荷蘭鐵路.md "wikilink")。

####  斯德哥爾摩

在斯德哥爾摩地鐵中，可行跨月台轉乘的車站有[地鐵中央站](../Page/地鐵中央站_\(斯德哥爾摩\).md "wikilink")（T-Centralen）、[老城站](../Page/老城站_\(斯德哥尔摩\).md "wikilink")（Gamla
stan）及[斯魯森站](../Page/斯魯森站.md "wikilink")（Slussen）
，三者皆為紅線、綠線間的[換乘站](../Page/換乘站.md "wikilink")，並且彼此相鄰。其中地鐵中心站月台提供逆向轉乘的服務，而後兩者設有順向轉乘月台。

此外，在\[8\]，則設有地鐵綠線和Nockebybanan路面輕軌之間的平面轉乘島式月台，該站因此成為斯德哥爾摩西部重要的郊區轉運站之一。
[Kasxirskaja.png](https://zh.wikipedia.org/wiki/File:Kasxirskaja.png "fig:Kasxirskaja.png")

####  莫斯科

在莫斯科地鐵中，以下車站內有跨月台轉車的設計，而且全部為同方向跨月台轉乘：

  - [中國城站](../Page/中國城站_\(莫斯科地鐵\).md "wikilink")：[6號線與](../Page/莫斯科地鐵6號線.md "wikilink")[7號線](../Page/莫斯科地鐵7號線.md "wikilink")
  - [特列季亞科夫站](../Page/特列季亞科夫站.md "wikilink")：6號線與[8號線](../Page/莫斯科地鐵8號線.md "wikilink")（終點站）
  - [昆采沃站](../Page/昆采沃站.md "wikilink")：[4號線](../Page/莫斯科地鐵4號線.md "wikilink")（終點站）僅可與[3號線的北向列車進行跨月台轉乘](../Page/莫斯科地鐵3號線.md "wikilink")
  - [卡希拉站](../Page/卡希拉站.md "wikilink")：[2號線與](../Page/莫斯科地鐵2號線.md "wikilink")[11號線](../Page/莫斯科地鐵11號線.md "wikilink")（終點站）
  - [勝利公園站](../Page/勝利公園站_\(莫斯科地鐵\).md "wikilink")：3號線和[8號線](../Page/莫斯科地鐵8號線.md "wikilink")
  - [彼得羅夫-拉祖莫夫站](../Page/彼得羅夫-拉祖莫夫站.md "wikilink")：[9號線與](../Page/莫斯科地鐵9號線.md "wikilink")[10號線](../Page/莫斯科地鐵10號線.md "wikilink")

####  聖彼得堡

聖彼得堡地鐵的[技術學院站為同向跨月臺轉乘](../Page/科技學院站.md "wikilink")，而[體育館站](../Page/運動站_\(聖彼得堡地鐵\).md "wikilink")（運動站）為島式疊式月臺。

### 大洋洲

####  奧克蘭

列車在使用中央軌道（同時服務兩個月台）以便與使用外側軌道的進行跨月台轉乘。

### 北美洲

####  紐約

[Queens_Plaza_station_5BBT_jeh.JPG](https://zh.wikipedia.org/wiki/File:Queens_Plaza_station_5BBT_jeh.JPG "fig:Queens_Plaza_station_5BBT_jeh.JPG")外觀\]\]
[Queensboro_Plaza.png](https://zh.wikipedia.org/wiki/File:Queensboro_Plaza.png "fig:Queensboro_Plaza.png")
紐約地鐵的許多路線中都採用了[四線鐵路](../Page/四線鐵路.md "wikilink")（或三線）的設計，同一條路線上即可開行多種營運模式，因此紐約的同月台平行轉乘，有許多是針對同條路線中的快車（直達車）、慢車（各站皆停）間提供的跨月台轉乘服務。通常，外側軌道跑慢車、中央軌道則使快車得以通過其不停靠的普通車站，兩種軌道之間會設置島式月台，方便快慢車間的旅客互換。

除此之外，一些顏色相異的路線之間亦設有跨月台轉車站聯繫彼此。例如[皇后區廣場車站的月台為](../Page/皇后區廣場車站.md "wikilink")[IRT法拉盛線](../Page/紐約地鐵7號線.md "wikilink")（7號線）及[BMT阿斯托利亞線](../Page/BMT阿斯托利亞線.md "wikilink")（[N線](../Page/紐約地鐵N線.md "wikilink")、[Q線](../Page/紐約地鐵Q線.md "wikilink")）之間提供順向的同台轉車服務。位於[布魯克林區的](../Page/布魯克林區.md "wikilink")[霍伊特-歇爾美角街車站則是](../Page/霍伊特-歇爾美角街車站.md "wikilink")[IND跨區線](../Page/紐約地鐵G線.md "wikilink")（G線）及[IND福爾頓街線](../Page/IND福爾頓街線.md "wikilink")（[A線](../Page/紐約地鐵A線.md "wikilink")、[C線](../Page/紐約地鐵C線.md "wikilink")）間的跨月台轉車站。[萊辛頓大道-63街車站在](../Page/萊辛頓大道-63街車站_\(63街線\).md "wikilink")[紐約地鐵第二大道線通車後已成為跨月台轉車站](../Page/紐約地鐵第二大道線.md "wikilink")。

[長島鐵路的](../Page/長島鐵路.md "wikilink")有多個車種停靠。在[繁忙時段](../Page/繁忙時段.md "wikilink")，跨月台轉乘所需的時間甚至更能大為縮短，因為各列車間的水平轉乘動線，可以直接穿越停靠中央軌道、雙側開門的列車。

####  蒙特婁

蒙特婁地鐵擁有兩座跨月台轉車站：[利安納·格魯站](../Page/利安納·格魯站.md "wikilink")（[1號線](../Page/蒙特利爾地鐵1號線.md "wikilink")、[2號線間](../Page/蒙特利爾地鐵2號線.md "wikilink")）及[諾東站](../Page/諾東站.md "wikilink")（2號線、[5號線間](../Page/蒙特利爾地鐵5號線.md "wikilink")）。

## 圖片集

<center>

<File:HKMTRPrinceEdward_20070725.jpg>|[港鐵](../Page/港鐵.md "wikilink")[太子站](../Page/太子站.md "wikilink")4號[荃灣綫月台](../Page/荃灣綫.md "wikilink")（左）及3號[觀塘綫月台](../Page/觀塘綫.md "wikilink")（右），為乘客提供往[中環及](../Page/中環站.md "wikilink")[調景嶺方向跨月台轉車服務](../Page/調景嶺站.md "wikilink")。
<File:Cross-platform> interchange in MRT Guting Station
20120930.jpg|[台北捷運](../Page/台北捷運.md "wikilink")[古亭站](../Page/古亭站.md "wikilink")1號[新店線月台](../Page/新店線.md "wikilink")（左）及2號[中和新蘆線月台](../Page/台北捷運橘線.md "wikilink")（右），為乘客提供往[淡水及](../Page/淡水站.md "wikilink")[迴龍](../Page/迴龍站.md "wikilink")／[蘆洲方向跨月台轉車服務](../Page/蘆洲站.md "wikilink")。
<File:City> Hall MRT
2.JPG|[新加坡地鐵](../Page/新加坡地鐵.md "wikilink")[政府大廈站](../Page/政府大廈地鐵站.md "wikilink")[南北線A月台](../Page/新加坡地铁南北线.md "wikilink")（右）及[東西線B月台](../Page/新加坡地铁东西线.md "wikilink")（左），為乘客提供往[裕廊東及](../Page/裕廊東地鐵站.md "wikilink")[裕群方向跨月台轉車服務](../Page/裕群地鐵站.md "wikilink")。
<File:Stratford> x
pltfm.jpg|在[英國](../Page/英國.md "wikilink")[倫敦的](../Page/倫敦.md "wikilink")[斯特拉特福站](../Page/斯特拉特福站.md "wikilink")，乘客在東安格利亞國家特快車（National
Express East
Anglia）及[地鐵](../Page/倫敦地鐵.md "wikilink")[中央線之間進行轉乘](../Page/倫敦地鐵中央線.md "wikilink")。
[File:Lionel-groulx-montreal-metro.jpg|在](File:Lionel-groulx-montreal-metro.jpg%7C在)[加拿大](../Page/加拿大.md "wikilink")[蒙特利尔的](../Page/蒙特利尔.md "wikilink")[利安納·格魯站](../Page/利安納·格魯站.md "wikilink")，乘客可在[地鐵](../Page/蒙特利爾地鐵.md "wikilink")[2號線及](../Page/蒙特利爾地鐵2號線.md "wikilink")[1號線之間進行轉乘](../Page/蒙特利爾地鐵1號線.md "wikilink")，上層月台為進城轉乘；下層月台為出城轉乘。
<File:Shin-Yatsushiro> Station
Train-Switching.jpg|[日本](../Page/日本.md "wikilink")[九州的](../Page/九州.md "wikilink")[新八代車站曾是](../Page/新八代車站.md "wikilink")[九州新幹線在局部路段通車時期的北端起點站](../Page/九州新幹線.md "wikilink")，在2004年至2011年間乘客可以在特急列車接力燕號及新幹線列車燕號之間進行同月台直接轉乘，但相關措施已在九州新幹線全線通車後取消。
<File:Hanshin> Amagasaki Station platform - panoramio
(3).jpg|特殊的跨月台換乘形式：在[日本](../Page/日本.md "wikilink")[阪神電鐵](../Page/阪神電鐵.md "wikilink")1號月台與3號月台之間換乘的乘客需要穿過圖片中停靠在2號月台的普通列車進行轉乘。

</center>

## 參考資料及注釋

## 參見

  - [城市軌道交通系統](../Page/城市軌道交通系統.md "wikilink")
  - [鐵路車站](../Page/鐵路車站.md "wikilink")
  - [捷運車站](../Page/捷運車站.md "wikilink")
  - [島式月台](../Page/島式月台.md "wikilink")
  - [換乘站](../Page/換乘站.md "wikilink")

[Category:鐵路車站](../Category/鐵路車站.md "wikilink")

1.  [台北捷運東門站麻花隧道宣告成形](http://www2.dorts.gov.tw/news/newsletter/ns254/rp254_03.htm)
    ，台北捷運報導 第254期，[台北市政府捷運工程局](../Page/台北市政府捷運工程局.md "wikilink")
2.  [首条“麻花隧道”缩短换乘距离
    已在杭州地铁建设](http://local.allnet.cn/bj/content!st-mhsd-sdhcjc-yzhzdtjs!ef0cb79a.html)
3.  今港鐵
4.
5.
6.
7.
8.  關於（Alvik）的轉乘情形，可參見部落格：「[衝過頭了！ - 小職員週記 -
    無名小站](http://www.wretch.cc/blog/shcheng/26527884)」