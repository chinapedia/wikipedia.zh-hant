[White-tailed_deer.jpg](https://zh.wikipedia.org/wiki/File:White-tailed_deer.jpg "fig:White-tailed_deer.jpg")是常见的野生动物种类\]\]

**野生动物**泛指各种生活在自然状态下，未经人类[驯化的](../Page/驯化.md "wikilink")[动物](../Page/动物.md "wikilink")。

[自然界裡](../Page/自然界.md "wikilink")，野生动物会用[排泄物來](../Page/排泄物.md "wikilink")[標識其](../Page/標識.md "wikilink")[領土](../Page/領土.md "wikilink")，宣示[主權的習慣](../Page/主權.md "wikilink")。

## 概况

野生动物，故名思意，为野外生长繁殖的动物，一般而言，具有以下特征：

1.  野外独立生存，即不依靠外部因素如人类力量存活
2.  具有[种群及排他性](../Page/种群.md "wikilink")

## 野生动物保护

随着人类的不断发展，野生动物的栖息地受到威胁，很多种类的野生动物已经[灭绝或濒临灭绝](../Page/灭绝.md "wikilink")。多国政府都有保护野生动物的专门法律。在亚洲不少国家，对于野生动物在食用和药用方面的旺盛需求也在威胁着野生动物。一些传说中的“特殊功效”被夸大，导致野生动物大量遭到猎杀。这类动物的制品有：[海马](../Page/海马.md "wikilink")、[鱼翅](../Page/鱼翅.md "wikilink")、虎骨、[燕窝](../Page/燕窝.md "wikilink")、[冬虫夏草等等](../Page/冬虫夏草.md "wikilink")。

2016年，由[世界自然基金会与](../Page/世界自然基金会.md "wikilink")联合撰写的一份报告指出，自1970年以来全世界野生动物数量由于人类活动而减少了近60%。野生动物大量减少的直接原因是人类发展。[世界人口在](../Page/世界人口.md "wikilink")1960年至2016年之間增长超過一倍，導致其他动物的栖息地和食物减少。\[1\]

## 營養價值

[聯合國糧食及農業組織指出](../Page/聯合國糧食及農業組織.md "wikilink")：对[野生动物肉类](../Page/野生动物.md "wikilink")[营养价值的研究表明](../Page/营养价值.md "wikilink")，野生動物的肉即使不比[家畜](../Page/家畜.md "wikilink")[肉好](../Page/肉.md "wikilink")，也是相当的。总的趋势是大多数野生动物品种的肉趋向于低[脂肪](../Page/脂肪.md "wikilink")，而[蛋白质含量等于或优于](../Page/蛋白质.md "wikilink")[牛肉](../Page/牛肉.md "wikilink")、[羊肉](../Page/羊肉.md "wikilink")、[鸡肉或](../Page/鸡肉.md "wikilink")[猪肉](../Page/猪肉.md "wikilink")，并且[维生素含量高得多](../Page/维生素.md "wikilink")\[2\]，更美味\[3\]。

## 中国的野生动物

[中国是野生动物种类最为丰富的国家之一](../Page/中国.md "wikilink")，[脊椎动物种类约占世界种数的](../Page/脊椎动物.md "wikilink")10%以上，其中[哺乳类](../Page/哺乳类.md "wikilink")500种，[鸟类](../Page/鸟类.md "wikilink")1258种，[爬行类](../Page/爬行类.md "wikilink")412种，[两栖类](../Page/两栖类.md "wikilink")295种，[鱼类](../Page/鱼类.md "wikilink")3862种。其中许多为中国特有的珍稀物种，如[大熊猫](../Page/大熊猫.md "wikilink")、[金丝猴](../Page/金丝猴.md "wikilink")、[朱鹮](../Page/朱鹮.md "wikilink")、[褐马鸡](../Page/褐马鸡.md "wikilink")、[扬子鳄等](../Page/扬子鳄.md "wikilink")。\[4\]

## 参考资料

## 外部链接

  - [中国野生动物保护协会网站](https://web.archive.org/web/20081226082017/http://www.cwca.org.cn/index.htm)

[W](../Category/动物.md "wikilink") [\*](../Category/野生生物.md "wikilink")

1.
2.  [Wildlife and food security in
    Africa](http://www.fao.org/docrep/w7540e/w7540e07.htm)
3.  [Farm vs. Wild: Which meat is
    healthier?](https://selfreliancecentral.com/2016/11/25/wild-game/)
4.  [中国国家林业局——中国野生动物资源](http://www.forestry.gov.cn/distribution/2006/09/24/lygk-2006-09-24-2006.html)