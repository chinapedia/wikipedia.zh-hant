**銚期**\[1\]（？—34年），字**次況**，[潁川郡](../Page/潁川.md "wikilink")[郟縣](../Page/郟縣.md "wikilink")（今屬河南）人。

身長八尺二寸，其父[銚猛官桂陽郡太守](../Page/銚猛.md "wikilink")。銚猛死，銚期服喪三年，鄉里稱之。更始元年（23年），刘秀至颍川郡，召为贼曹掾。與[傅寬](../Page/傅寬.md "wikilink")、[呂晏俱隸屬](../Page/呂晏.md "wikilink")[鄧禹部下](../Page/鄧禹.md "wikilink")。鄧禹欣賞他，命为偏将军。銚期攻取真定（今河北正定县）、宋子（今河北栾城县东）、乐阳（今河北石家庄市西北）、稾县（今石家庄市东南）和肥累（今河北晋县西）等地。

光武即位，封铫期为安成侯。又以铫期为魏郡太守。建武五年（29年），担任太中大夫。建武十年（34年），病卒，帝亲临治丧，谥忠侯。[范晔称](../Page/范晔.md "wikilink")：“重于信义，自为将，有所降下，未尝虏掠。乃在朝廷，忧国爱主。其有不得于心，必犯颜谏诤。”有子[銚丹](../Page/銚丹.md "wikilink")、[銚統](../Page/銚統.md "wikilink")。

## 传统剧目

[京剧](../Page/京剧.md "wikilink")、[秦腔](../Page/秦腔.md "wikilink")、[武安平调等传统戏中有剧目](../Page/武安平调.md "wikilink")《铫期》，又名《草桥关》、《打金砖》、《斩铫期》，主要脚色为铫期、刘秀。

## 世系图

<center>

</center>

## 注釋

<div class="references-small">

<references />

</div>

## 參考書目

  - 《[後漢書](../Page/後漢書.md "wikilink")》[卷二十](../Page/:s:後漢書/卷20.md "wikilink")《[銚期傳](../Page/:s:後漢書/卷20#銚期.md "wikilink")》

[Category:34年逝世](../Category/34年逝世.md "wikilink")
[Y](../Category/東漢政治人物.md "wikilink")
[Y](../Category/東漢太守.md "wikilink")
[Y](../Category/郟縣人.md "wikilink")
[Category:銚姓](../Category/銚姓.md "wikilink")

1.  【銚】音yáo，同姚