[Kinki_Sharyou.jpg](https://zh.wikipedia.org/wiki/File:Kinki_Sharyou.jpg "fig:Kinki_Sharyou.jpg")
**近畿車輛株式會社**（，　），是一間生產鐵路車輛、門和報紙銷售機等的近鐵集團企業。總部在[大阪府](../Page/大阪府.md "wikilink")[東大阪市稻田上町](../Page/東大阪市.md "wikilink")2-6-41，車輛製作所也在同樣的地方。總部和製作所最近的站是[德庵站](../Page/德庵站.md "wikilink")。

近畿車輛與[三菱重工業有業務合作](../Page/三菱重工業.md "wikilink")。

## 鐵路車輛

[KTT_train.jpg](https://zh.wikipedia.org/wiki/File:KTT_train.jpg "fig:KTT_train.jpg")
[Kinki_sharyo_logo.jpg](https://zh.wikipedia.org/wiki/File:Kinki_sharyo_logo.jpg "fig:Kinki_sharyo_logo.jpg")

### [日本](../Page/日本.md "wikilink")

  - 日本國鐵、[JR](../Page/JR.md "wikilink")、第3營業者
      - [JR東日本](../Page/東日本旅客鐵道.md "wikilink")（[211系](../Page/日本國鐵211系電力動車組.md "wikilink")、[253系](../Page/JR東日本253系電力動車組.md "wikilink")、[255系](../Page/JR東日本255系電力動車組.md "wikilink")、[E257系](../Page/JR東日本E257系電力動車組.md "wikilink")、[E653系](../Page/JR東日本E653系電力動車組.md "wikilink")、E751系等）
      - [JR東海](../Page/東海旅客鐵道.md "wikilink")（[新幹線100系](../Page/新幹線100系電力動車組.md "wikilink")、[新幹線300系](../Page/新幹線300系電力動車組.md "wikilink")、[新幹線700系](../Page/新幹線700系電聯車.md "wikilink")、[新幹線N700系](../Page/新幹線N700系電聯車.md "wikilink")、[285系](../Page/JR西日本285系電力動車組.md "wikilink")3000番台、[311系](../Page/JR東海311系電力動車組.md "wikilink")、[313系](../Page/JR東海313系電力動車組.md "wikilink")）
      - [JR西日本](../Page/西日本旅客鐵道.md "wikilink")（新幹線100系、[新幹線500系](../Page/新幹線500系電聯車.md "wikilink")、新幹線700系、新幹線N700系、[新幹線W7系](../Page/新幹線E7/W7系電力動車組.md "wikilink")、221系、[207系](../Page/JR西日本207系電力動車組.md "wikilink")、[205系](../Page/日本國鐵205系電力動車組.md "wikilink")1000番台、[223系](../Page/JR西日本223系電力動車組.md "wikilink")、[225系](../Page/JR西日本225系電力動車組.md "wikilink")、[321系](../Page/JR西日本321系電力動車組.md "wikilink")、[281系](../Page/JR西日本281系電力動車組.md "wikilink")、[283系](../Page/JR西日本283系電力動車組.md "wikilink")、681系、[683系](../Page/JR西日本683系電力動車組.md "wikilink")）
      - [JR四國](../Page/四國旅客鐵道.md "wikilink")（7000系（承接全部訂單））
      - [JR九州](../Page/九州旅客鐵道.md "wikilink")（[813系](../Page/JR九州813系電力動車組.md "wikilink")、[303系](../Page/JR九州303系電力動車組.md "wikilink")）
      - [北越急行](../Page/北越急行.md "wikilink")（681系）
  - [大手私鐵](../Page/大手私鐵.md "wikilink")
      - [近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")（全線列車）
      - [南海電氣鐵道](../Page/南海電氣鐵道.md "wikilink")（南海7000系、7100系的一部份）
      - [埼玉高速鐵道](../Page/埼玉高速鐵道.md "wikilink")（2000系）
  - 地下鐵
      - [東京地下鐵](../Page/東京地下鐵.md "wikilink")（[帝都高速度交通營團時代](../Page/帝都高速度交通營團.md "wikilink")：01系、03系、05系、7000系等）
      - [都營地下鐵](../Page/都營地下鐵.md "wikilink")（10-000系、5300型）
      - [大阪市營地下鐵](../Page/大阪市營地下鐵.md "wikilink")（21系、22系）
      - [京都市營地下鐵](../Page/京都市營地下鐵.md "wikilink")
  - 中小型、公營鐵路
      - JTRAM（廣島電鐵5100型、日本國產超低地台LRV）
      - 北九州高速鐵道

### [香港](../Page/香港.md "wikilink")

  - [九廣鐵路公司](../Page/九廣鐵路公司.md "wikilink")（已於2007年與[香港地鐵合併](../Page/香港地鐵.md "wikilink")，成為[港鐵](../Page/港鐵.md "wikilink")）
      - 1974年的客車（后来变成中国铁路32型客车）
      - [港鐵伊藤忠近畿川崎列車](../Page/港鐵伊藤忠近畿川崎列車.md "wikilink")（多數原裝列車及2007年增購車卡）（全數已租予[港鐵公司使用](../Page/港鐵公司.md "wikilink")）
      - [九廣通](../Page/九廣通.md "wikilink")（Ktt）車卡\[1\]
  - [港鐵公司](../Page/港鐵公司.md "wikilink")
      - [港鐵伊藤忠近畿川崎列車](../Page/港鐵伊藤忠近畿川崎列車.md "wikilink")（2016-17年增購車卡及原頭等車卡降格工程）

### [美國](../Page/美國.md "wikilink")

  - [紐澤西州](../Page/紐澤西州.md "wikilink")[哈德遜-博根輕軌鐵路車輛](../Page/哈德遜-博根輕軌鐵路.md "wikilink")
  - [亞利桑那州](../Page/亞利桑那州.md "wikilink")[鳳凰城都會地區](../Page/鳳凰城.md "wikilink")[METRO輕鐵車輛](../Page/METRO輕鐵.md "wikilink")
  - [華盛頓州](../Page/華盛頓州.md "wikilink")[西雅圖海灣輕鐵中央線車輛](../Page/西雅圖海灣輕鐵中央線.md "wikilink")

### [埃及](../Page/埃及.md "wikilink")

  - [開羅](../Page/開羅.md "wikilink")[地鐵](../Page/開羅地鐵.md "wikilink")

## 備考

該公司製造的車輛，也會少有地利用德庵站的側線作試車。

列車出廠付運的方式主要有兩種：1.以有火/無火迴送方式，經由[大阪東線付運至國內用戶](../Page/大阪東線.md "wikilink")。2.出口海外的車輛，則以平板車逐卡運至貨櫃碼頭上船。

## 列車

### 日本國內

<File:JRW> N700-7000series S1.jpg|N700系山陽、九州新幹線 直通用S編組 <File:JR-West>
285-series.jpg|JR西日本、JR東海 285系
[File:WestJapanRailwayCompanyType321-1.jpg|JR西日本](File:WestJapanRailwayCompanyType321-1.jpg%7CJR西日本)
321系

### 日本國外

<File:DART> rail.jpg|DART 高床式LRV <File:Hudson> bergen exchange
place.jpg|NJT Hudson–Bergen 超低床式LRV <File:VTA> LRT 2.JPG|VTA 超低床式LRV
<File:Sound> Transit Link Light Rail Train.jpg|Sound Transit Central
Link 超低床式LRV <File:KCRER> SP1900 20071117.jpg|香港 SP1900 <File:Metro>
Dubai 2.JPG|[杜拜地鐵](../Page/杜拜地鐵.md "wikilink")

## 參考資料

## 外部連結

  - [近畿車輛](http://www.kinkisharyo.co.jp/)

[Category:日本鐵路車輛製造商](../Category/日本鐵路車輛製造商.md "wikilink")
[Category:近鐵集團](../Category/近鐵集團.md "wikilink")
[\*](../Category/近畿製鐵路車輛.md "wikilink")

1.  [九廣通客車資料](http://hkttn.com/ktt/ktt_compartment.htm)