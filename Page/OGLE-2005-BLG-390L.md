**OGLE-2005-BLG-390L**（OGLE-05-390L）是位於[天蝎座的一顆](../Page/天蝎座.md "wikilink")[恆星](../Page/恆星.md "wikilink")，距離[地球](../Page/地球.md "wikilink")21,500
± 3300[光年](../Page/光年.md "wikilink")（6600 ± 1000
[pc](../Page/秒差距.md "wikilink")）。該恆星很有可能是一顆屬於M型[光譜的](../Page/恒星光谱分类.md "wikilink")[紅矮星](../Page/紅矮星.md "wikilink")，其質量約為[太陽的](../Page/太陽.md "wikilink")0.22
± 0.1倍。\[1\]

在[波蘭的](../Page/波蘭.md "wikilink")[OGLE小組對該恆星進行觀測期間](../Page/光學重力透鏡實驗.md "wikilink")，透過[重力透鏡](../Page/重力透鏡.md "wikilink")，發現有一顆行星繞著它公轉，這顆行星的質量約為地球的五倍，距離恆星約為2.6[天文單位](../Page/天文單位.md "wikilink")。該發現於2006年1月26日對外公佈。\[2\]

## 行星

OGLE-2005-BLG-390L目前已知有一顆行星，這顆行星被編為**OGLE-05-390Lb**，是迄今發現體積最小，環繞[主序星旋轉的太陽系外](../Page/主序星.md "wikilink")[類地行星](../Page/類地行星.md "wikilink")。它有可能擁有岩石結構，在2006年1月25日研究顯示，預估該行星質量大約為5倍地球質量，行星軌道半徑大約2.6[AU](../Page/天文單位.md "wikilink")，其餘軌道參數目前人們均一無所知。\[3\]由於行星質量較低，科學家估計表面溫度大約是50[K](../Page/克耳文.md "wikilink")，並推測主要類似冥王星與天王星由冰組成的行星，而不是類似木星由氣體組成的行星。\[4\]

## 參見

  - [OGLE-2005-BLG-169L](../Page/OGLE-2005-BLG-169L.md "wikilink")
  - [光學重力透鏡實驗](../Page/光學重力透鏡實驗.md "wikilink") (OGLE)

## 參考文獻

## 外部連結

  - [**OGLE**: 2005-BLG-390
    Event](http://www.astrouw.edu.pl/~ogle/ogle3/ews/2005/blg-390.html)

  - [Small Rocky Planet Found Orbiting Normal
    Star](http://www.space.com/scienceastronomy/060125_smallest_planet.html)

  -
[Category:天蠍座](../Category/天蠍座.md "wikilink")
[Category:紅矮星](../Category/紅矮星.md "wikilink")
[Category:重力透鏡](../Category/重力透鏡.md "wikilink")
[Category:行星系](../Category/行星系.md "wikilink")

1.

2.
3.
4.