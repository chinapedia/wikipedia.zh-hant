[Harrods_1909.jpg](https://zh.wikipedia.org/wiki/File:Harrods_1909.jpg "fig:Harrods_1909.jpg")
[Nabil_kuzbari.jpg](https://zh.wikipedia.org/wiki/File:Nabil_kuzbari.jpg "fig:Nabil_kuzbari.jpg")的肖像照\]\]
**上層階級**（[英文](../Page/英文.md "wikilink")：**Upper
Class**），又稱**上流社會**、**[權貴](../Page/權貴.md "wikilink")**、**[豪門大戶](../Page/豪門.md "wikilink")**等。是[社會上掌握](../Page/社會.md "wikilink")[政治權力及社會資源及](../Page/政治權力.md "wikilink")[自然資源分配權的已得利益者](../Page/自然資源.md "wikilink")。

在[資本主義](../Page/資本主義.md "wikilink")[社會](../Page/社會.md "wikilink")，上流社會包括掌有最大額[資本的](../Page/資本.md "wikilink")[財團](../Page/財閥.md "wikilink")[企業家](../Page/企業家.md "wikilink")。在[封建社會](../Page/封建社會.md "wikilink")，上流社會則是包括有[貴族](../Page/貴族.md "wikilink")[爵位的知名](../Page/爵位.md "wikilink")[家族成員](../Page/家族.md "wikilink")。在動盪的年代，上流社會的人物不是永遠固定的幾位，例如[法國大革命前後的](../Page/法國大革命.md "wikilink")[路易十六](../Page/路易十六.md "wikilink")，在1790年6月以後，參考以上的定義，他不再是上流社會的代表。在[社會主義國家](../Page/社會主義國家.md "wikilink")，上流社會是指擁有[特權的黨的高官](../Page/特權.md "wikilink")，有「紅色貴族」、[新階級之稱](../Page/新階級.md "wikilink")（英文：New
Class；日文：共産貴族；俄文：партийной номенклатуры\[1\]」）。

雖然社會有階級之分，但在公平的社會，人民可以有適當的途徑，通過奮鬥，努力由低層[社會階級躋身上流社會](../Page/社會階級.md "wikilink")。上流社會是不少[文學](../Page/文學.md "wikilink")[著作的題材](../Page/著作.md "wikilink")，如《[茶花女](../Page/茶花女.md "wikilink")》、《[遠大前程](../Page/遠大前程.md "wikilink")》、《[追憶似水年華](../Page/追憶似水年華.md "wikilink")》和《[大亨小傳](../Page/大亨小傳.md "wikilink")》等皆為代表作。上流社會人士如[戴安娜王妃](../Page/戴安娜王妃.md "wikilink")、[芭黎絲·希爾頓等](../Page/芭黎絲·希爾頓.md "wikilink")，也是時下秘聞[雜誌](../Page/雜誌.md "wikilink")[狗仔隊追訪的對象](../Page/狗仔隊.md "wikilink")。

## 中國

傳統中國**上流**通常指社會上有較高[社會地位的一群人](../Page/社會地位.md "wikilink")，通常有較高學識與修養，有[品味](../Page/品味.md "wikilink")，喜歡[精緻藝術](../Page/精緻藝術.md "wikilink")（至少「附庸風雅」一番）；與**[上層階級](../Page/上層階級.md "wikilink")**不同之處是後者是純粹以財富劃分。

在大多數人看來「上流社會」與「上層階級」相近，不過有些「上流社會」的人會看不起新近富起來的人（「[暴發戶](../Page/暴發戶.md "wikilink")」），想方設法限制其「入流」；某些[俱樂部](../Page/俱樂部.md "wikilink")，例如[香港賽馬會](../Page/香港賽馬會.md "wikilink")，要有多於一名現會員推薦才能加入，不是有錢就成。

[英文或可對應為](../Page/英文.md "wikilink") **High society**。

### 傳統中國上流社會

  - [貴族](../Page/貴族.md "wikilink")
  - [士大夫](../Page/士大夫.md "wikilink")
  - [名流或](../Page/名流.md "wikilink")[名士](../Page/名士.md "wikilink")，如[竹林七賢](../Page/竹林七賢.md "wikilink")
  - [清流官員](../Page/清流黨.md "wikilink")
  - [士紳](../Page/士紳.md "wikilink")
  - [文人](../Page/文人.md "wikilink")

## 晉身上流社會的途徑

  - [婚姻](../Page/婚姻.md "wikilink")
  - 就讀國際知名[學府或傳統](../Page/學府.md "wikilink")[名校](../Page/名校.md "wikilink")
  - 拜師
  - [資本](../Page/資本.md "wikilink")[投資](../Page/投資.md "wikilink")
  - [結拜](../Page/結誼.md "wikilink")
  - [遺產](../Page/遺產.md "wikilink")、爵位繼承
  - [选举](../Page/选举.md "wikilink")
  - [選美活動](../Page/選美活動.md "wikilink")
  - [革命](../Page/革命.md "wikilink")
  - [世襲](../Page/世襲.md "wikilink")

## 參見

  - [門閥](../Page/門閥.md "wikilink")
  - [財閥](../Page/財閥.md "wikilink")
  - [統治階級](../Page/統治階級.md "wikilink")
  - [精英](../Page/精英.md "wikilink")
  - [英國上議院](../Page/英國上議院.md "wikilink")
  - [社會團體](../Page/社會團體.md "wikilink")
  - [百人會](../Page/百人會.md "wikilink")
  - [慈善基金會](../Page/慈善基金會.md "wikilink")
  - [遺產爭奪](../Page/遺產爭奪.md "wikilink")

## 注釋

[fr:Élite](../Page/fr:Élite.md "wikilink")

[Category:社会阶层](../Category/社会阶层.md "wikilink")

1.  \[Джилас,Милован：《нового класса》（新階級－對共產主義制度的分析）