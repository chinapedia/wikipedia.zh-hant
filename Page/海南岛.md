[Hainan_satellite.JPG](https://zh.wikipedia.org/wiki/File:Hainan_satellite.JPG "fig:Hainan_satellite.JPG")
[Haikou_skyline_6_-_2009_09_07.jpg](https://zh.wikipedia.org/wiki/File:Haikou_skyline_6_-_2009_09_07.jpg "fig:Haikou_skyline_6_-_2009_09_07.jpg")城區\]\]
[Sanya_Sun_Photo_by_Dale_Preston.jpg](https://zh.wikipedia.org/wiki/File:Sanya_Sun_Photo_by_Dale_Preston.jpg "fig:Sanya_Sun_Photo_by_Dale_Preston.jpg")海滩\]\]
**海南岛**是位于[南海北部的一座](../Page/南海.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")，又称**琼州**；陸地面积33,210平方公里，海域面積約200萬平方公里，屬於[大陆岛](../Page/大陆岛.md "wikilink")，北邊隔著[琼州海峡与](../Page/琼州海峡.md "wikilink")[雷州半岛隔海相望](../Page/雷州半岛.md "wikilink")。為[海南省主體](../Page/海南省.md "wikilink")。

[中華人民共和國](../Page/中華人民共和國.md "wikilink")1949年成立後，在[該政權實際控制的島嶼裏](../Page/中華人民共和國島嶼列表.md "wikilink")，它屬第一大島，於1950年5月1日發動[海南島戰役後擁有主權](../Page/海南島戰役.md "wikilink")，併入[廣東省](../Page/廣東省.md "wikilink")，1988年獨立建省—[海南省](../Page/海南省.md "wikilink")，同时成立[海南经济特区](../Page/海南经济特区.md "wikilink")\[1\]，它是中国第22个省和面积最大的经济特区，若算上[海南省的領海](../Page/海南省.md "wikilink")，該省為[中華人民共和國最大的](../Page/中華人民共和國.md "wikilink")[省分](../Page/省_\(中華人民共和國\).md "wikilink")。在原本[中華民國的領土規劃裡](../Page/中華民國.md "wikilink")，海南岛屬於[海南特別行政區](../Page/海南特別行政區.md "wikilink")。

海南島現今管轄人口約8,671,518人\[2\]，也是今日[海南省除南海諸島實際控制領域之外的主要组成部份](../Page/海南省.md "wikilink")。

## 相对位置

海南島在[北回归线以南](../Page/北回归线.md "wikilink")，[东经](../Page/东经.md "wikilink")110度左右，该岛也是[中国最南方的大型島嶼](../Page/中华人民共和国.md "wikilink")，北隔[琼州海峡与](../Page/琼州海峡.md "wikilink")[雷州半岛相望](../Page/雷州半岛.md "wikilink")，距离[广东省的雷州半岛最窄处约](../Page/广东省.md "wikilink")18公里\[3\]。西临[中国南海的](../Page/中国南海.md "wikilink")[北部湾](../Page/北部湾.md "wikilink")，东面和南面濒中国南海。

  - 相邻省份：[广东](../Page/广东.md "wikilink")、[广西](../Page/广西.md "wikilink")
  - 相邻国家：[越南](../Page/越南.md "wikilink")

## 气候

海南岛属[海洋性](../Page/海洋性.md "wikilink")[热带季风气候](../Page/热带季风气候.md "wikilink")，地处[热带北缘](../Page/热带.md "wikilink")，占中国[热带土地面积的](../Page/热带.md "wikilink")40%，光照充足，[年日照时数大于](../Page/年日照时数.md "wikilink")2,000小时，[活动积温达](../Page/活动积温.md "wikilink")8,400至9,200[摄氏度](../Page/摄氏度.md "wikilink")；夏热冬暖，[最热月均温在](../Page/最热月.md "wikilink")25至29摄氏度，[最冷月均温大于](../Page/最冷月.md "wikilink")16摄氏度。極端情況下，才於明朝年間降雪\[4\]。

[降水方面](../Page/降水.md "wikilink")，受夏季季风影响，岛上降水从东向西减少；受[地形影响](../Page/地形.md "wikilink")，降水廣泛集中於東部，西南部降水少，因此位於島內西南部有個著名的[莺歌海盐场](../Page/莺歌海盐场.md "wikilink")。

## 地形

海南岛地形为中部高四周低,最高峰为[五指山](../Page/五指山_\(海南\).md "wikilink"),海拔1867米。

## 自然资源

### 生物资源

山地丘陵地区[热带雨林](../Page/热带雨林.md "wikilink")、[热带季雨林发育](../Page/热带季雨林.md "wikilink")，多有[珍稀物种](../Page/珍稀物种.md "wikilink")：
\#[药用植物](../Page/药用植物.md "wikilink")：[见血封喉](../Page/见血封喉.md "wikilink")、[花梨木](../Page/花梨木.md "wikilink")、[砂仁](../Page/砂仁.md "wikilink")、[益智](../Page/益智_\(植物\).md "wikilink")、[槟榔等](../Page/槟榔.md "wikilink")

1.  [用材植物](../Page/用材植物.md "wikilink")：[苦梓](../Page/苦梓.md "wikilink")、[坡垒](../Page/坡垒.md "wikilink")、[青梅](../Page/青梅_\(龍腦香科\).md "wikilink")、[子京](../Page/子京.md "wikilink")、[野荔枝等](../Page/野荔枝.md "wikilink")
2.  [珍稀动物](../Page/珍稀动物.md "wikilink")：[孔雀雉](../Page/孔雀雉.md "wikilink")、[白鹇](../Page/白鹇.md "wikilink")、[长臂猿](../Page/长臂猿.md "wikilink")、[坡鹿](../Page/坡鹿.md "wikilink")、[云豹](../Page/云豹.md "wikilink")、[水獭](../Page/水獭.md "wikilink")、[猴子](../Page/猴子.md "wikilink")、[水鹿](../Page/水鹿.md "wikilink")、[灵猫](../Page/灵猫.md "wikilink")、[穿山甲](../Page/穿山甲.md "wikilink")、[巨松鼠等](../Page/巨松鼠.md "wikilink")

沿海地區則有珊瑚與及珊瑚魚類分布。

### 铁矿

[石碌铁矿](../Page/石碌铁矿.md "wikilink")：中国为数不多的[富铁矿之一](../Page/中国富铁矿形表.md "wikilink")。

## 交通

### [高速公路](../Page/高速公路.md "wikilink")

  - [海南环线高速公路](../Page/海南环线高速公路.md "wikilink")(中華人民共和國國道編號：G98)

### [國際機場](../Page/國際機場.md "wikilink")

  - [海口美蘭國際機場](../Page/海口美蘭國際機場.md "wikilink")
  - [三亞鳳凰國際機場](../Page/三亞鳳凰國際機場.md "wikilink")

### [機場](../Page/機場.md "wikilink")

  - [瓊海博鰵機場](../Page/瓊海博鰵機場.md "wikilink")

## 人文資源

### 海南四大名菜

海南岛四大名菜是文昌鸡、东山羊、加积鸭以及和乐蟹。\[5\]

## 著名人物

**曾被贬谪海南岛的历史名人**

  - [杨炎](../Page/杨炎.md "wikilink")（未至而死）
  - [李德裕](../Page/李德裕.md "wikilink")
  - [韦执谊](../Page/韦执谊.md "wikilink")
  - [韩瑗](../Page/韩瑗.md "wikilink")
  - [卢多逊](../Page/卢多逊.md "wikilink")
  - [丁谓](../Page/丁谓.md "wikilink")
  - [苏轼](../Page/苏轼.md "wikilink")
  - [折彦质](../Page/折彦质.md "wikilink")
  - [李纲](../Page/李纲.md "wikilink")
  - [李光](../Page/李光.md "wikilink")
  - [赵鼎](../Page/赵鼎.md "wikilink")
  - [胡铨](../Page/胡铨.md "wikilink")

### 本地名人

**宋、元**

  - [白玉蟾](../Page/白玉蟾.md "wikilink")

**明**

  - [丘浚](../Page/丘浚.md "wikilink")
  - [海瑞](../Page/海瑞.md "wikilink")
  - [邢宥](../Page/邢宥.md "wikilink")

**清**

  - [张岳崧](../Page/张岳崧.md "wikilink")
  - [吳典](../Page/吳典.md "wikilink")

**中華民國**

  - [宋嘉樹](../Page/宋嘉樹.md "wikilink")
  - [宋靄齡](../Page/宋靄齡.md "wikilink")
  - [宋美龄](../Page/宋美龄.md "wikilink")
  - [宋子文](../Page/宋子文.md "wikilink")

**中華人民共和國**

  - [林文英](../Page/林文英.md "wikilink")
  - [陳得平](../Page/陳得平.md "wikilink")
  - [趙士槐](../Page/趙士槐.md "wikilink")
  - [宋庆龄](../Page/宋庆龄.md "wikilink")
  - [陳俠農](../Page/陳俠農.md "wikilink")
  - [馮平](../Page/馮平.md "wikilink")
  - [鄭介民](../Page/鄭介民.md "wikilink")
  - [葉佩高](../Page/葉佩高.md "wikilink")
  - [陳策](../Page/陳策_\(行政人員\).md "wikilink")

**現代**

  - [張雲逸](../Page/張雲逸.md "wikilink")
  - [冯白驹](../Page/冯白驹.md "wikilink")
  - [周士第](../Page/周士第.md "wikilink")
  - [翁诗杰](../Page/翁诗杰.md "wikilink")
  - [莊田](../Page/莊田.md "wikilink")
  - [李向群](../Page/李向群.md "wikilink")

## 参考文献

## 外部链接

  - [海口](http://www.haikou.gov.cn)
  - [了解海南](http://www.hainan.net)
  - [海南相片](http://www.fotop.net/alannet/HaiNan)
  - [海南岛
    網上電子地圖(中文版)](https://web.archive.org/web/20060617122124/http://www.51ditu.com/maps/static/html/BAA3C4CFCAA1/index.html)

## 参见

  - [海南省](../Page/海南省.md "wikilink")

{{-}}

[Category:海南岛屿](../Category/海南岛屿.md "wikilink")
[海南](../Category/海南.md "wikilink")

1.
2.
3.
4.  <http://www.weather.com.cn/zt/tqzt/452255.shtml>
5.