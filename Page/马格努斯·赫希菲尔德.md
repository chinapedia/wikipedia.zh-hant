**马格努斯·赫希菲尔德**（，），[德国](../Page/德国.md "wikilink")[犹太裔人](../Page/犹太.md "wikilink")。出生于Kolberg（现在位于[波兰](../Page/波兰.md "wikilink")），是[内科医生和性学家](../Page/内科.md "wikilink")，曾经公开承认自己是[同性恋者](../Page/同性恋者.md "wikilink")。他认为[同性恋是第三性](../Page/同性恋.md "wikilink")，即介于男性于女性之间的中性，而不是一种疾病。后人称为“性爱恩斯坦”。

## 生平简介

1897年在柏林创办了世界上第一个“[科学人道主义委员会](../Page/科学人道主义委员会.md "wikilink")”组织，为同性恋者争取权利，使德国在法律上废除了[反同性恋法和对男性同性恋者的监禁](../Page/反同性恋法.md "wikilink")。

1910年出版的《异装症》一书用术语“[异装癖者](../Page/异装癖者.md "wikilink")”（transvestism）首次将“异装癖者”与同性恋者区分出来。

[1933-may-10-berlin-book-burning.JPG](https://zh.wikipedia.org/wiki/File:1933-may-10-berlin-book-burning.JPG "fig:1933-may-10-berlin-book-burning.JPG")分子烧毁马格努斯·赫希菲尔德创办的性学研究所。\]\]
1913年在[柏林与布洛赫](../Page/柏林.md "wikilink")（Iwan Bloch）、摩尔（Albert
Eulenburg）等人创建了第一个“性医学和优生学会”。

1914年出版《同性爱》，被誉为同性恋的[百科全书](../Page/百科全书.md "wikilink")。

1919年在柏林创办了“性学研究所”，其中收藏了大量的对性研究有关的书籍和材料，而且为世界各国医学界所肯定。但后来于1933年5月6日被德国[纳粹分子所烧毁](../Page/纳粹.md "wikilink")，大量有价值的书本、资料、图片被焚毁，因当时**马格努斯·赫希菲尔德**在国外而逃过迫害。后来有人尝试重建该研究所，但未能成功。

1930至32年开始他的环球旅行。曾经到过[美国](../Page/美国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[中国](../Page/中国.md "wikilink")、[印度](../Page/印度.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[巴基斯坦等国家](../Page/巴基斯坦.md "wikilink")，并在各地进行演讲。

1935年5月14日于[法国逝世](../Page/法国.md "wikilink")。

[Category:德国性学家](../Category/德国性学家.md "wikilink")
[Category:德國LGBT人物](../Category/德國LGBT人物.md "wikilink")
[Category:女性主義](../Category/女性主義.md "wikilink")
[Category:男同性戀作家](../Category/男同性戀作家.md "wikilink")
[Category:猶太作家](../Category/猶太作家.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:海德堡大學校友](../Category/海德堡大學校友.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:斯特拉斯堡大學校友](../Category/斯特拉斯堡大學校友.md "wikilink")
[Category:布雷斯勞大學校友](../Category/布雷斯勞大學校友.md "wikilink")
[Category:德國猶太人](../Category/德國猶太人.md "wikilink")
[Category:波美拉尼亞人](../Category/波美拉尼亞人.md "wikilink")
[Category:德國社會主義者](../Category/德國社會主義者.md "wikilink")
[Category:LGBT猶太人](../Category/LGBT猶太人.md "wikilink")
[Category:德國LGBT權利運動家](../Category/德國LGBT權利運動家.md "wikilink")