**徐立功**（），[臺灣著名](../Page/臺灣.md "wikilink")[導演](../Page/導演.md "wikilink")、[編劇和](../Page/編劇.md "wikilink")[電影監製人](../Page/電影監製.md "wikilink")，[金馬國際影展創辦人](../Page/金馬國際影展.md "wikilink")。

## 簡歷

1967年[辅仁大学哲学硕士毕业](../Page/辅仁大学.md "wikilink")。1968年开始编写电视剧。曾任[行政院新聞局廣播電視事業處科长](../Page/行政院新聞局.md "wikilink")、[国家电影资料馆首屆馆长](../Page/国家电影资料馆.md "wikilink")，歷任[中央电影公司副总经理](../Page/中央电影公司.md "wikilink")、总经理和副董事长，以及[纵横国际影视董事长等职](../Page/纵横国际影视.md "wikilink")。

其兄長為前[行政院副院長](../Page/行政院副院長.md "wikilink")[徐立德](../Page/徐立德.md "wikilink")。

35歲那年，他擔任[電影圖書館首任館長](../Page/電影圖書館.md "wikilink")，一手創辦影迷瘋魔的[金馬國際影展](../Page/金馬國際影展.md "wikilink")，培育[黃建業](../Page/黃建業.md "wikilink")、[李幼新等許多優秀影評人](../Page/李幼新.md "wikilink")。46歲他進入中影，先慧眼獨具把窩家六年的[李安拉拔入電影舞台](../Page/李安.md "wikilink")，再一路提攜了[蔡明亮](../Page/蔡明亮.md "wikilink")、[林正盛](../Page/林正盛.md "wikilink")、[陳玉勳](../Page/陳玉勳.md "wikilink")、[陳國富等新銳導演](../Page/陳國富.md "wikilink")，拍攝他們生平第一部劇情長片，甚至獲得第一座國際獎項。53歲他寶刀未老地創辦[縱橫國際影視](../Page/縱橫國際影視.md "wikilink")，拍攝《[臥虎藏龍](../Page/臥虎藏龍_\(電影\).md "wikilink")》、《[人間四月天](../Page/人間四月天.md "wikilink")》等作品，再次締造無數台灣影視界的傳奇及潮流至今\[1\]。並於1994年[第31屆金馬獎獲頒傑出製片人特別獎](../Page/第31屆金馬獎.md "wikilink")，2010年[第47屆金馬獎獲頒終生成就獎](../Page/第47屆金馬獎.md "wikilink")。[台北金馬影展執行委員會表示](../Page/台北金馬影展執行委員會.md "wikilink")，鮮少人能像徐立功一樣，在不同時代、不同位置，為[台灣電影文化提供了舉足輕重的貢獻](../Page/台灣電影.md "wikilink")；時值金馬國際影展30週年，頒發他1座終身成就獎肯定他的電影建樹，實至名歸\[2\]\[3\]。

## 作品

  - 导演

<!-- end list -->

  - [夜奔](../Page/夜奔_\(電影\).md "wikilink")（1999年）

<!-- end list -->

  - 编剧

<!-- end list -->

  - 心恋（2004年）

<!-- end list -->

  - 监制

<!-- end list -->

  - 河流（1997年）
  - 爱情來了（1997年）
  - [卧虎藏龙](../Page/卧虎藏龙.md "wikilink")（2000年）
  - 心恋（2004年）
  - 恋人（2005年）
  - [寶米恰恰](../Page/寶米恰恰.md "wikilink")（2012年）
  - [滿月酒](../Page/滿月酒.md "wikilink")（2015年）
  - [最後的記憶](../Page/最後的記憶.md "wikilink")（2018年）

<!-- end list -->

  - 制片

<!-- end list -->

  - 販母案考（1990年）
  - [推手](../Page/推手_\(電影\).md "wikilink")（1991年）
  - 胭脂（1991年）
  - 娃娃（1991年）
  - [牯嶺街少年殺人事件](../Page/牯嶺街少年殺人事件.md "wikilink")（1991年）
  - [青少年哪吒](../Page/青少年哪吒.md "wikilink")（1992年）
  - [喜宴](../Page/喜宴.md "wikilink")（1993年）
  - 新同居時代（1993年）
  - 青春無悔（1993年）
  - 三個夏天（1993年）
  - [無言的山丘](../Page/無言的山丘.md "wikilink")（1993年）
  - [飲食男女](../Page/飲食男女.md "wikilink")（1994年）
  - [愛情萬歲](../Page/愛情萬歲_\(台灣電影\).md "wikilink")（1994年）
  - [我的美麗與哀愁](../Page/我的美麗與哀愁.md "wikilink")（1995年）
  - 寂寞芳心俱乐部（1995年）
  - [热带鱼](../Page/热带鱼_\(电影\).md "wikilink")（1995年）
  - [少女小漁](../Page/少女小漁.md "wikilink")（1995年）
  - [袋鼠男人](../Page/袋鼠男人.md "wikilink")（1995年）
  - 鹹豆漿（2002年）
  - 心恋（2004年）

<!-- end list -->

  - 出品

<!-- end list -->

  - 在陌生的城市（1995年）
  - 春花夢露（1996年）
  - 飛天（1996年）
  - 河流（1996年）
  - 今天不回家（1996年）
  - [美麗在唱歌](../Page/美麗在唱歌.md "wikilink")（1997年）
  - [徵婚啟事](../Page/徵婚啟事.md "wikilink")（1998年）
  - 候鳥（2000年）
  - [20 30 40](../Page/20_30_40.md "wikilink")（2004年）

## 參考資料

## 參見

  - [金馬獎獎項列表](../Page/金馬獎獎項列表#終身成就獎.md "wikilink")
  - [天主教輔仁大學校友列表](../Page/天主教輔仁大學校友列表#文學藝術界.md "wikilink")

[Category:台灣導演](../Category/台灣導演.md "wikilink")
[Category:台灣編劇](../Category/台灣編劇.md "wikilink")
[Category:台灣電影監製](../Category/台灣電影監製.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Li立功](../Category/徐姓.md "wikilink")
[X徐](../Category/金馬獎終身成就獎得主.md "wikilink")
[Category:中影公司人物](../Category/中影公司人物.md "wikilink")
[Category:國立故宮博物院人物](../Category/國立故宮博物院人物.md "wikilink")

1.
2.
3.