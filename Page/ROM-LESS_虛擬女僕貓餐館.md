《**ROM-LESS
虛擬女僕貓餐館**》（）是[白雪詩音的](../Page/白雪詩音.md "wikilink")[四格漫畫作品](../Page/四格漫畫.md "wikilink")，自2004年6月號在[芳文社的](../Page/芳文社.md "wikilink")《[Manga
Time Kirara](../Page/Manga_Time_Kirara.md "wikilink")》上連載。

## 簡介

## 人物

  -
    河村家的長男，25歲。以卓越的成績從大學畢業。

  -
    河村家的次男。

  -
    河村家的么女。

  -

## 參考

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:Manga Time
Kirara](../Category/Manga_Time_Kirara.md "wikilink")
[Category:四格漫畫](../Category/四格漫畫.md "wikilink")