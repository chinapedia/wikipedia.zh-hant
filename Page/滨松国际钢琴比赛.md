**滨松国际钢琴比赛**（、）是在[日本](../Page/日本.md "wikilink")[静冈县](../Page/静冈县.md "wikilink")[滨松市举办的](../Page/滨松市.md "wikilink")[钢琴比赛](../Page/钢琴.md "wikilink")。

## 概要

此国际性钢琴比赛是滨松市政府1991年为纪念该市成立80周年而创立的，之后每三年一届定期举行。第三届比赛结束后，于1998年加盟总部设在[日内瓦的](../Page/日内瓦.md "wikilink")[国际音乐比赛世界联盟](../Page/国际音乐比赛世界联盟.md "wikilink")（World
Federation of International Music
Competition-简称WFIMC），成为[亚洲唯一加盟该组织的钢琴比赛](../Page/亚洲.md "wikilink")。一等奖获奖者除奖金之外，还获得至少10次在日本及海外举办[演奏会的机会](../Page/演奏会.md "wikilink")。2006年11月举行的第六屆比赛首次免费提供了[视频流](../Page/视频流.md "wikilink")（Video
Streaming）播放服务。

## 沿革

  - 1991年：第一届
  - 1994年：第二届
  - 1997年：第三届
  - 1998年：加盟国际音乐比赛世界联盟(WFIMC)
  - 2000年：第四届
  - 2003年：第五届
  - 2006年：第六届
  - 2009年：第七届
  - 2012年：第八届
  - 2015年：第九届
  - 2018年：第十届

## 历届主要获奖者

第一届：

  - 巴巴扬／Sergei
    BABAYAN（[亚美尼亚](../Page/亚美尼亚.md "wikilink")）第一名。之后获1992年意大利[布索尼国际钢琴比赛第三名](../Page/布索尼国际钢琴比赛.md "wikilink")。
  - 麦克德莫特／Annie Marie McDERMOTT（美国）第二名和日本人作品最优秀演奏奖
  - 许忠／XU Zhong（中国）第三名

第二届：

  - 里亚朵夫／Victor LIADOV（俄罗斯）第一名
  - 潘皮利／Enrico POMPILI（意大利）第二名
  - 蔻儿／Naida
    COLE（[加拿大](../Page/加拿大.md "wikilink")）第三名和日本人作品最优秀演奏奖。之后获1997年美国[范·克莱本国际钢琴比赛最优秀](../Page/范·克莱本国际钢琴比赛.md "wikilink")[室内乐奖](../Page/室内乐.md "wikilink")。
  - 柴田彩子／SHIBATA
    Ayako（日本）第三名。之后获1995年葡萄牙[波尔图国际音乐比赛第一名](../Page/波尔图国际音乐比赛.md "wikilink")。

第三届：

  - 巴克斯／Alessio
    BAX（意大利）第一名。之后获2000年英国[利兹国际钢琴比赛第一名](../Page/利兹国际钢琴比赛.md "wikilink")。
  - 科恩／Oliver KERN（德国）第二名
  - 塔拉索夫／Sergei TARASOV（俄罗斯）第三名和日本人作品最优秀演奏奖

第四届：

  - Alexander
    GAVRYLYUK（[乌克兰](../Page/乌克兰.md "wikilink")）第一名。之后获2005年以色列[阿图尔·鲁宾斯坦国际钢琴比赛第一名](../Page/阿图尔·鲁宾斯坦国际钢琴比赛.md "wikilink")。
  - 林东海／LIM
    Dong-Hyek（韩国）第二名。之后获2001年法国[玛格丽特·隆-雅克·迪博国际钢琴比赛第一名](../Page/玛格丽特·隆-雅克·迪博国际钢琴比赛.md "wikilink")。
  - 上原彩子／UEHARA
    Ayako（日本）第二名和日本人作品最优秀演奏奖。之后获2002年俄罗斯[柴可夫斯基国际钢琴比赛第一名](../Page/柴可夫斯基.md "wikilink")。
  - 科恩／Olga KERN（俄罗斯）第三名。之后获2001年美国范·克莱本国际钢琴比赛第一名。
  - Ferenc VIZI（[罗马尼亚](../Page/罗马尼亚.md "wikilink")）第四名
  - 阿米洛夫／Feodor AMIROV（俄罗斯）第五名

第五届：

  - 布莱哈奇／Rafal
    BLECHACZ（波兰）第二名。之后获2005年波兰[肖邦国际钢琴比赛第一名](../Page/肖邦国际钢琴比赛.md "wikilink")。
  - 科布林／Alexander KOBRIN（俄罗斯）第二名。之后获2005年美国范·克莱本国际钢琴比赛第一名。
  - 沙洛夫／Sergei
    SALOV（乌克兰）第三名。之后获2004年加拿大[蒙特利尔国际音乐比赛第一名](../Page/蒙特利尔.md "wikilink")。
  - 关本昌平／SEKIMOTO Shohei（日本）第四名
  - 须藤梨菜／SUDO Rina（日本）第四名
  - 鈴木弘尚／SUZUKI Hironao日本）第五名
  - 德沙尔姆／Romain DESCHARMES（[法国](../Page/法国.md "wikilink")）日本人作品最优秀演奏奖

第六届：

  - 格尔拉奇／Alexej
    GORLATCH（乌克兰）第一名和日本人作品最优秀演奏奖。之后获2009年英国[利兹国际钢琴比赛第一名](../Page/利兹国际钢琴比赛.md "wikilink")。
  - 库兹涅佐夫／Sergey KUZNETSOV（俄罗斯）第二名
  - 金泰亨／KIM Tae-Hyung（韩国）第三名
  - 北村朋幹／KITAMURA Tomoki（日本）第三名
  - 王淳／WANG Chun（中国）第五名
  - 萨拉托弗斯基／Nikolay SARATOVSKY（俄罗斯）第六名

第七届：

  - 赵成珍／CHO
    Seong-Jin（韩国）第一名和日本人作品最优秀演奏奖。之后获2011年俄罗斯柴可夫斯基国际钢琴比赛第三名及2015年波兰[肖邦国际钢琴比赛第一名](../Page/肖邦国际钢琴比赛.md "wikilink")。
  - 加萨诺夫／Elmar GASANOV（俄罗斯）第二名
  - HUH Jae-Weon（韩国）第三名
  - 弗朗索瓦·杜蒙／François DUMONT（法国）第四名
  - KIM Hyun-Jung（韩国）第五名
  - 安荣洙／ANN Soo-Jung（韩国）第六名
  - Alessandro TAVERNA（意大利）日本人作品最优秀演奏奖

第八届：

  - 拉什科夫斯基／Ilya RASHKOVSKIY（俄罗斯）第一名
  - 中桐望／Nozomi NAKAGIRI（日本）第二名
  - 佐藤卓史／Takashi SATO（日本）第三名
  - 茨布列娃／Anna TCYBULEVA（俄罗斯）第四名。之后获2015年英国利兹国际钢琴比赛第一名。
  - KIM Joon（韩国）第五名
  - 内匠慧／TAKUMI Kei（日本）第六名和日本人作品最优秀演奏奖

第九届

  - 格杰耶夫／Alexander GADJIEV（意大利-斯洛文尼亚）第一名
  - 罗帕廷斯基／Roman LOPATYNSKYI（乌克兰）第二名
  - 丹尼尔-徐／Daniel HSU（美国）第三名
  - 梅尔尼考夫／Alexei MELNIKOV（俄罗斯）第三名
  - 默扎／Alexia MOUZA（希腊-委内瑞拉）第三名
  - Florian MITREA（罗马尼亚）第四名
  - 安德烈耶夫／Igor ANDREEV（俄罗斯）日本人作品最优秀演奏奖

第十届

  - Can CAKMUR（[土耳其](../Page/土耳其.md "wikilink")）第一名
  - 牛田智大／USHIDA Tomoharu（日本）第二名
  - 李赫／LEE Hyuk（韩国）第三名
  - 今田笃／IMADA Atsushi（日本）第四名
  - 务川慧悟／MUKAWA Keigo（日本）第五名
  - 安并贵史／YASUNAMI Takashi（日本）第六名
  - 梅田智也／UMEDA Yomoya（日本）日本人作品最优秀演奏奖

## 外部链接

  - [滨松国际钢琴比赛（日、英文）](http://www.hipic.jp/)
  - [国际音乐比赛世界联盟（英文）](http://www.fmcim.org/www/en/)

[Category:钢琴比赛](../Category/钢琴比赛.md "wikilink")
[Category:日本音乐比赛](../Category/日本音乐比赛.md "wikilink")
[Category:1991年建立](../Category/1991年建立.md "wikilink")