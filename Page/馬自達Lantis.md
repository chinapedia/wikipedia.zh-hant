**馬自達Lantis**（）是1990年代由[日本](../Page/日本.md "wikilink")[馬自達汽車公司生產發售的](../Page/馬自達.md "wikilink")[緊湊型轎車](../Page/緊湊型轎車.md "wikilink")，區分成五門[掀背車與四門](../Page/掀背車.md "wikilink")[轎車等車型](../Page/轎車.md "wikilink")。

## 概要

繼[馬自達323
Astina在](../Page/馬自達323_Astina.md "wikilink")[歐洲地區一砲而紅之後](../Page/歐洲.md "wikilink")，馬自達推出這部後繼車款。[香港版車名維持](../Page/香港.md "wikilink")**馬自達323
Astina**，[歐洲版也繼續叫作](../Page/歐洲.md "wikilink")**馬自達323F**，不過[日本地區則改稱](../Page/日本.md "wikilink")**馬自達Lantis**（マツダ・ランティス）。「Lantis」語源來自[拉丁文的](../Page/拉丁文.md "wikilink")「latens
curtis」，意思是「秘密之城」，代表這款車深不可測。

## 歷史

1993年9月正式在日本發售，共分成五門[掀背車](../Page/掀背車.md "wikilink")（但原廠以奇怪的「4-door
coupé」稱呼）、三門[掀背車和四門](../Page/掀背車.md "wikilink")[轎車三種型式](../Page/轎車.md "wikilink")，[底盤平台為和](../Page/底盤.md "wikilink")[Eunos
500相同的CB平台](../Page/Eunos_500.md "wikilink")。動力來源為1.8L直列四缸[BP型引擎](../Page/馬自達B族引擎#BP型.md "wikilink")、2.0L
V型六缸[KF型引擎兩種](../Page/馬自達K族引擎#KF型.md "wikilink")。2.0L的引擎雖然與[Cronos屬於同一顆](../Page/馬自達Cronos.md "wikilink")，但重新調校過進、排氣行程後，馬力提升10ps達到170ps。當時[日本](../Page/日本.md "wikilink")《Car
Graphic汽車雜誌》替2.0L自然進氣引擎版的Lantis測試0至400公尺加速，只耗費16.0秒，創下該雜誌創刊以來同類引擎的最佳紀錄。

不過由於其獨特的車身外型設計，加上馬自達推行[多品牌策略失敗而導致品牌形象不佳](../Page/多品牌策略.md "wikilink")，在[日本國內只維持至](../Page/日本.md "wikilink")1997年8月就被迫停產，該地區總計出售了43,300部左右。但是其他國家地區仍繼續銷售，譬如[歐洲和](../Page/歐洲.md "wikilink")[香港](../Page/香港.md "wikilink")。在香港推出了1.5L直列四缸[Z5-DE型和](../Page/馬自達Z族引擎#Z5型.md "wikilink")1.8L直列四缸[BP-ZE型兩種引擎版本](../Page/馬自達B族引擎#BP型.md "wikilink")，前者由[福特所供應](../Page/福特汽車.md "wikilink")。

此外，在[拉丁美洲也見得到Lantis的蹤跡](../Page/拉丁美洲.md "wikilink")，比如在[智利叫](../Page/智利.md "wikilink")**馬自達Artis**，在[哥倫比亞叫](../Page/哥倫比亞.md "wikilink")**馬自達Allegro
HB**（hatchback縮寫成「HB」，掀背車之意）。

Image:Mazda 323f front 20071002.jpg|馬自達323F車頭（歐規前期型） Image:Mazda 323f
rear 20071002.jpg|馬自達323F車尾（歐規前期型） Image:Lantis002.jpg|馬自達323F車頭（歐規前期型）
Image:Lantis001.jpg|車頭另一角度 Image:Astina.jpg|馬自達323F車尾（歐規後期型）

{{-}}

## 內部連結

  - [馬自達323](../Page/馬自達323.md "wikilink")
  - [馬自達323 Astina](../Page/馬自達323_Astina.md "wikilink")

## 外部連結

  - [Gazoo.com - 1989年 マツダ ファミリア
    アスティナ](https://web.archive.org/web/20090925071738/http://gazoo.com/meishakan/meisha/shousai.asp?R_ID=3204)

## 參考資料

  - [Gazoo.com - 1989年 マツダ ファミリア
    アスティナ](https://web.archive.org/web/20090925071738/http://gazoo.com/meishakan/meisha/shousai.asp?R_ID=3204)

[Category:1993年面世的汽車](../Category/1993年面世的汽車.md "wikilink")
[Category:馬自達車輛](../Category/馬自達車輛.md "wikilink")
[Category:緊湊型轎車](../Category/緊湊型轎車.md "wikilink")
[Category:掀背車](../Category/掀背車.md "wikilink")
[Category:前置引擎](../Category/前置引擎.md "wikilink")
[Category:前輪驅動](../Category/前輪驅動.md "wikilink")