**鍾楚紅**（英語：，），出生於[香港](../Page/香港.md "wikilink")，[香港知名](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")[女演員](../Page/女演員.md "wikilink")。於1984年及1987年兩度榮獲[亞太影展最佳女主角殊榮](../Page/亞太影展.md "wikilink")。在台灣首支佳麗寶等廣告大受歡迎。

## 事業

曾經在珠寶店當過職員的鍾楚紅於1979年參選[香港小姐出道](../Page/香港小姐.md "wikilink"),當時只得第四名。賽後，她在[無線試鏡失敗](../Page/無線.md "wikilink")，卻得到電影公司垂青，因而踏足影壇。她的第一部大螢幕作品是[杜琪峰的導演處女作](../Page/杜琪峰.md "wikilink")《[碧水寒山奪命金](../Page/碧水寒山奪命金.md "wikilink")》，飾演運金鏢頭的女兒尤佩佩。1984年，她憑《[男與女](../Page/男與女.md "wikilink")》首次獲得香港電影金像獎最佳女主角提名；之後在1987年與[周潤發合演](../Page/周潤發.md "wikilink")《[秋天的童話](../Page/秋天的童話.md "wikilink")》再度獲得提名，可惜仍未能獲獎，但她在片中的演出廣獲好評，同時讓她第二次榮獲[亞太影展影后殊榮](../Page/亞太影展.md "wikilink")。

## 個人生活

1984年，廣告界才子[朱家鼎](../Page/朱家鼎.md "wikilink")（Mike
Chu）邀請鍾楚紅拍攝運動產品廣告，兩人開始交往。1991年12月10日，鍾楚紅與朱家鼎在[美國](../Page/美國.md "wikilink")[羅德島州聖百蘭](../Page/羅德島州.md "wikilink")[天主教](../Page/天主教.md "wikilink")[教堂舉行婚禮](../Page/教堂.md "wikilink")，花費200多萬[港幣](../Page/港幣.md "wikilink")，婚前協議不生育。1994年，鍾楚紅正式息影。2007年8月24日，朱家鼎因[大腸癌在香港](../Page/大腸癌.md "wikilink")[養和醫院病逝](../Page/養和醫院.md "wikilink")。\[1\]2009年，鍾楚紅為圓曾是虔誠[天主教徒的亡夫遺願](../Page/天主教.md "wikilink")，在一年多前開始上[教堂](../Page/教堂.md "wikilink")、報讀「慕道班」，在4月11日正式接受領洗成為[天主教徒](../Page/天主教.md "wikilink")。

鍾楚紅人緣甚佳，圈中好友甚多，尤其是[周潤發](../Page/周潤發.md "wikilink")、[張曼玉](../Page/張曼玉.md "wikilink")、[葉蒨文](../Page/葉蒨文.md "wikilink")、[張國榮](../Page/張國榮.md "wikilink")。

## 電影

| 年份                                                 | 片名                                               | 角色          |
| -------------------------------------------------- | ------------------------------------------------ | ----------- |
| 1980年                                              | [碧水寒山奪命金](../Page/碧水寒山奪命金.md "wikilink")         | 尤佩佩         |
| 1981年                                              | [巡城馬](../Page/巡城馬.md "wikilink")                 | 貴花          |
| [胡越的故事](../Page/胡越的故事.md "wikilink")               | 沈青                                               |             |
| 1982年                                              | [人嚇人](../Page/人嚇人.md "wikilink")                 | 阿雲          |
| [薄荷咖啡](../Page/薄荷咖啡.md "wikilink")                 | 傅敏                                               |             |
| 1983年                                              | [星際鈍胎](../Page/星際鈍胎.md "wikilink")               | 李天珍         |
| [田雞過河](../Page/田雞過河.md "wikilink")                 |                                                  |             |
| [花心大少](../Page/花心大少.md "wikilink")                 | 阿妹／李玉珍                                           |             |
| [奇謀妙計五福星](../Page/奇謀妙計五福星.md "wikilink")           | 小妹                                               |             |
| [日劫](../Page/日劫.md "wikilink")                     | 公主                                               |             |
| [男與女](../Page/男與女.md "wikilink")                   | 孟思晨                                              |             |
| [難兄難弟](../Page/難兄難弟.md "wikilink")                 | 阿珠                                               |             |
| 1984年                                              | [青蛙王子](../Page/青蛙王子_\(電影\).md "wikilink")        | 郁德美         |
| [上天救命](../Page/上天救命.md "wikilink")                 | 石女                                               |             |
| [我愛神仙遮](../Page/我愛神仙遮.md "wikilink")               | 油紙傘妖                                             |             |
| [英倫琵琶](../Page/英倫琵琶.md "wikilink")                 | Amy                                              |             |
| [魔殿屠龍](../Page/魔殿屠龍.md "wikilink")                 | 蒙古公主                                             |             |
| [窺情](../Page/窺情.md "wikilink")                     | 芮塔                                               |             |
| [雪兒](../Page/雪兒_\(電影\).md "wikilink")              | 雪兒                                               |             |
| 1985年                                              | [鬼馬飛人](../Page/鬼馬飛人.md "wikilink")               | Miss Cheung |
| [女人心](../Page/女人心.md "wikilink")                   | 沙妞                                               |             |
| [花心紅杏](../Page/花心紅杏.md "wikilink")                 |                                                  |             |
| 1986年                                              | [Sayonara 再見](../Page/Sayonara_再見.md "wikilink") |             |
| [竹籬笆外的春天](../Page/竹籬笆外的春天.md "wikilink")           | 李琳                                               |             |
| [歡樂叮噹](../Page/歡樂叮噹.md "wikilink")                 | 叮叮                                               |             |
| [刀馬旦](../Page/刀馬旦_\(電影\).md "wikilink")            | 湘紅                                               |             |
| 1987年                                              | [愛情謎語](../Page/愛情謎語.md "wikilink")               | 樂琪          |
| [秋天的童話](../Page/秋天的童話.md "wikilink")               | 李琪                                               |             |
| [八星報喜](../Page/八星報喜.md "wikilink")                 | Beautiful                                        |             |
| [鬼新娘](../Page/鬼新娘.md "wikilink")                   | 魏小蝶                                              |             |
| [意亂情迷](../Page/意亂情迷_\(1987年電影\).md "wikilink")     | Shirley                                          |             |
| [呷醋大丈夫](../Page/呷醋大丈夫.md "wikilink")               | 小嫻                                               |             |
| 1988年                                              | [月亮、星星、太陽](../Page/月亮、星星、太陽.md "wikilink")       | Gigi        |
| [金燕子](../Page/金燕子.md "wikilink")                   | 小雪                                               |             |
| [好女十八嫁](../Page/好女十八嫁.md "wikilink")               | 何芝芝                                              |             |
| [殺之戀](../Page/殺之戀.md "wikilink")                   | 紅                                                |             |
| [流金歲月](../Page/流金歲月.md "wikilink")                 | 朱鎖鎖                                              |             |
| [火舞風雲](../Page/火舞風雲.md "wikilink")                 | 鍾小紅                                              |             |
| [鬼馬保鑣賊美人](../Page/鬼馬保鑣賊美人.md "wikilink")           | 高秀萍                                              |             |
| [三對鴛鴦一張床](../Page/三對鴛鴦一張床.md "wikilink")           | 黃小麗                                              |             |
| [一妻兩夫](../Page/一妻兩夫.md "wikilink")                 | SOGO                                             |             |
| [獵鷹計劃](../Page/獵鷹計劃.md "wikilink")                 | 阿紅                                               |             |
| [婚外情](../Page/婚外情.md "wikilink")                   | 愛麗絲                                              |             |
| [金裝大酒店](../Page/金裝大酒店.md "wikilink")               | 鍾小姐                                              |             |
| 1989年                                              | [小男人周記](../Page/小男人周記.md "wikilink")             | 林太太         |
| [伴我闖天涯](../Page/伴我闖天涯.md "wikilink")               | 李阿雪                                              |             |
| [相見好](../Page/相見好.md "wikilink")                   | 董卓莉/Cherry                                       |             |
| [愛人同志](../Page/愛人同志_\(電影\).md "wikilink")          | 阮紅                                               |             |
| [張國榮音樂電影：日落巴黎](../Page/張國榮音樂電影：日落巴黎.md "wikilink") | cherie                                           |             |
| 1991年                                              | [極道追-{踪}-](../Page/極道追踪.md "wikilink")           | 鐵蘭          |
| [縱橫四海](../Page/縱橫四海_\(電影\).md "wikilink")          | 紅豆                                               |             |
|                                                    |                                                  |             |

## 電視節目（無綫電視）

  - [歡樂今宵](../Page/歡樂今宵.md "wikilink")

## 電視劇（香港電台）

| 時間          | 劇名                                  | 導演                               | 角色          |
| ----------- | ----------------------------------- | -------------------------------- | ----------- |
| 1981年10月17日 | [浮雲](../Page/浮雲.md "wikilink")\[2\] | [羅卓瑤](../Page/羅卓瑤.md "wikilink") | Flora Leung |
|             |                                     |                                  |             |

## 廣告代言

### 香港

  - [卡士蘭](../Page/卡士蘭.md "wikilink")（2013年）

### 台灣

  - [聲寶](../Page/聲寶股份有限公司.md "wikilink")（1985年）
  - [佳麗寶](../Page/佳麗寶.md "wikilink")[化妝品](../Page/化妝品.md "wikilink")（1988－1989年）
  - [裕隆飛羚102](../Page/裕隆飛羚.md "wikilink")（1989年）

## 榮譽獎項

### 亞太影展

<table style="width:163%;">
<colgroup>
<col style="width: 64%" />
<col style="width: 17%" />
<col style="width: 17%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>角色</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1984年</p></td>
<td><p>第29屆<a href="../Page/亞太影展.md" title="wikilink">亞太影展</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/男與女.md" title="wikilink">男與女</a>》</p></td>
<td><p>孟思晨</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td><p>第32屆<a href="../Page/亞太影展.md" title="wikilink">亞太影展</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/秋天的童話.md" title="wikilink">秋天的童話</a>》</p></td>
<td><p>李琪</p></td>
<td></td>
</tr>
</tbody>
</table>

### 台灣電影金馬獎

<table style="width:170%;">
<colgroup>
<col style="width: 64%" />
<col style="width: 17%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>角色</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/第20屆金馬獎.md" title="wikilink">第20屆金馬獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/男與女.md" title="wikilink">男與女</a>》</p></td>
<td><p>孟思晨</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td><p><a href="../Page/第24屆金馬獎.md" title="wikilink">第24屆金馬獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/秋天的童話.md" title="wikilink">秋天的童話</a>》</p></td>
<td><p>李琪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/第26屆金馬獎.md" title="wikilink">第26屆金馬獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/伴我闖天涯.md" title="wikilink">伴我闖天涯</a>》</p></td>
<td><p>阿雪</p></td>
<td></td>
</tr>
</tbody>
</table>

### 香港電影金像獎

<table style="width:170%;">
<colgroup>
<col style="width: 64%" />
<col style="width: 17%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>影片</p></th>
<th><p>角色</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/第2屆香港電影金像獎.md" title="wikilink">第2屆香港電影金像獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/薄荷咖啡.md" title="wikilink">薄荷咖啡</a>》</p></td>
<td><p>傅敏</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/第3屆香港電影金像獎.md" title="wikilink">第3屆香港電影金像獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/男與女.md" title="wikilink">男與女</a>》</p></td>
<td><p>孟思晨</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988年</p></td>
<td><p><a href="../Page/第7屆香港電影金像獎.md" title="wikilink">第7屆香港電影金像獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/秋天的童話.md" title="wikilink">秋天的童話</a>》</p></td>
<td><p>李琪</p></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  -
<!-- end list -->

  - [中文電影資料庫－鍾楚紅](http://www.dianying.com/ft/person/ZhongChuhong)

  -
  -
  -
  -
## 參考文獻

<references/>

[Category:1979年度香港小姐競選參賽者](../Category/1979年度香港小姐競選參賽者.md "wikilink")
[Category:亞太影展獲獎者](../Category/亞太影展獲獎者.md "wikilink")
[Chor](../Category/鍾姓.md "wikilink")
[Category:20世紀演員](../Category/20世紀演員.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:香港環保人士](../Category/香港環保人士.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:香港客家人](../Category/香港客家人.md "wikilink")
[Category:博羅人](../Category/博羅人.md "wikilink")

1.  [朱家鼎頑抗癌魔一年終不治 47歲鍾楚紅喪夫](http://hk.apple.nextmedia.com/entertainment/first/20070828/10077656)2007年8月28日
2.  [『浮雲』(45分鐘 香港電台 RTHK 電視劇集『香港香港』第一集 1981年10月17日播映
    RTHK網頁)](http://app4.rthk.hk/special/rthkmemory/details/tv-drama/453)