**梁俊一**（Anson Leung Chun Yat，原名**梁達祺**，），1999年加入第 13
期[無綫電視藝員訓練班後成為旗下藝員](../Page/無綫電視藝員訓練班.md "wikilink")，曾為[寰宇娛樂旗下藝人](../Page/寰宇娛樂.md "wikilink")。曾拍攝多部得獎大電影和高收視電視劇以及為人熟悉的[麥當勞廣告](../Page/麥當勞.md "wikilink")。而在《[旺角黑夜](../Page/旺角黑夜.md "wikilink")》飾演角色任志彬，演出得到荷里活多份報章讚賞，監制方平和導演尔冬升亦多次表揚其表現。\[1\]

## 演出作品

### 電影

  - 2003年：《[賭俠之人定勝天](../Page/賭俠之人定勝天.md "wikilink")》[郑伟文导演](../Page/郑伟文.md "wikilink")
    飾恆仔
  - 2003年：《[雙雄](../Page/雙雄.md "wikilink")》[陳木勝導演](../Page/陳木勝.md "wikilink")
    飾 阿棠
  - 2003年：《[Miss
    杜十娘](../Page/Miss_杜十娘.md "wikilink")》[高志森導演](../Page/高志森.md "wikilink")
    飾狗仔
  - 2004年：《[旺角黑夜](../Page/旺角黑夜.md "wikilink")》[尔冬升導演](../Page/尔冬升.md "wikilink")
    飾 任志彬 　　
  - 2004年：《[死亡寫真](../Page/死亡寫真.md "wikilink")》[彭順導演](../Page/彭順.md "wikilink")
    飾 家寶 　(男一)　
  - 2005年：《[三岔口](../Page/三岔口.md "wikilink")》陳木勝導演 飾
    [重案組](../Page/重案組.md "wikilink")[警探](../Page/警探.md "wikilink")
  - 2006年：《[半醉人間](../Page/半醉人間.md "wikilink")》晴朗導演 飾 阿一
  - 2006年：《[談談情?說說性](../Page/談談情?說說性.md "wikilink")》陈志舜導演 飾 陶兵 　　
  - 2007年：《[雙龍記](../Page/雙龍記.md "wikilink")》张晓枫 李绍源導演 飾 梁春　　
  - 2007年：《[男兒本色](../Page/男兒本色.md "wikilink")》陳木勝導演 飾 阿俊
  - 2009年：《[同門](../Page/同門.md "wikilink")》[邱礼涛導演](../Page/邱礼涛.md "wikilink")
    飾 文拯 　　
  - 2009年：《[風云II](../Page/風云II.md "wikilink")
    》[彭氏兄弟導演](../Page/彭氏兄弟.md "wikilink") 飾 絕地 　　
  - 2010年：《[奪命金](../Page/奪命金.md "wikilink")》杜琪鋒導演 飾殺手關達文
  - 2010年：《[車手](../Page/車手.md "wikilink")》[郑保瑞導演](../Page/郑保瑞.md "wikilink")
    飾 車手 　　
  - 2010年：《[完美童話](../Page/完美童話.md "wikilink")》[彭發導演](../Page/彭發.md "wikilink")
    飾 驗屍官
  - 2010年：《[最危險人物](../Page/最危險人物.md "wikilink")》梁德森導演 飾 軍警恒仔
  - 2011年: 《[拼命三娘](../Page/拼命三娘.md "wikilink")》阿建導演 飾 張竟 (男一)
  - 2011年: 《[拳王](../Page/拳王.md "wikilink")》楊志堅導演 飾日本軍官山田純一 (反一)
  - 2011年: 《[隨心所愛](../Page/隨心所愛.md "wikilink")》宋奇導演 飾 高峰 (男一)
  - 2012年: 《[戀愛之城之愛在響螺彎](../Page/戀愛之城之愛在響螺彎.md "wikilink")》金琛、藏溪川導演 飾 胡杰
  - 2016年: 《[我的黑道妹妹](../Page/我的黑道妹妹.md "wikilink")》(網絡電影)
  - 2016年: 《詭咒》
  - 2016年: 《我的黑道妹妹2之殺手悲歌》(網絡電影)
  - 2018年: 《香港大营救》

### 電視劇

  - 2000年：《[男親女愛](../Page/男親女愛.md "wikilink")》 飾 裝修佬
  - 2004年：《[功夫足球](../Page/功夫足球.md "wikilink")》 飾 花弄影 (男二)　
  - 2005年：《[阿有正傳](../Page/阿有正傳.md "wikilink")》 飾 阿旺 (男二)
  - 2007年：《[詠春](../Page/咏春_\(电视剧\).md "wikilink")》 飾
    [梁春](../Page/梁春.md "wikilink") (男二)
  - 2008年：《[火蝴蝶](../Page/火蝴蝶.md "wikilink")》 飾 章學禮 (男二)
  - 2010年：[香港電台電視劇](../Page/香港電台.md "wikilink")《[火速救兵](../Page/火速救兵.md "wikilink")》
    飾 劉君池 (男一)
  - 2012年：《[代號九耳犬](../Page/代號九耳犬.md "wikilink")》 飾 童虎 （男一）
  - 2012年：《[女人幫。妞兒](../Page/女人幫。妞兒.md "wikilink")》 飾 阿J (網絡劇)
  - 2014年：《[青年醫生](../Page/青年醫生.md "wikilink")》 飾 金舸
  - 2015年：《[超霸花神](../Page/超霸花神.md "wikilink")》(網絡劇)
  - 2016年：《[醫館笑傳](../Page/醫館笑傳.md "wikilink")2》飾 楊宇軒

### [舞臺劇](../Page/舞臺劇.md "wikilink")

  - 舞臺劇《[方舟東游記](../Page/方舟東游記.md "wikilink")》（2007年）
  - 舞臺劇\<[唯獨祢是王](../Page/唯獨祢是王.md "wikilink")\> (2010年)

### 音樂作品

  - 《火蝴蝶》插曲　 《愛放開》　　
  - 《紅黑皇布道會》主題曲 《活著就是祭》　　
  - 音樂舞台劇《THE ONLY KING 2010》《唯獨你是王》 飾 大衛的爸爸耶西、大祭司亞希米勒和長老

### 廣告

  - 痘痘零調理食品(代信人) 　　
  - 果汁先生(代信人) 　　
  - 百樂牌原子筆(代信人)　　
  - 衛訊電訊(代信人) 　
  - 日本“太八郎”品牌“功夫小子”系列眼鏡(代信人)
  - 雪碧冰薄荷(中國代信人) (合作明星：吳鎮宇)
  - 麥當勞廣告代信三部曲:
  - 2005年麥當勞 “Meet the Parent” (香港) (代信人) 　　
  - 2006年麥當勞 “Magic” (香港) (代信人)
  - 2007年Wedding Abroad (香港) (代信人)

### MV

  - \[江若琳\] - 《撒嬌》

## 外部連結

[chun](../Category/梁姓.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[梁](../Category/香港新教徒.md "wikilink")

1.  [1](http://orientaldaily.on.cc/cnt/entertainment/20091009/00282_056.html)