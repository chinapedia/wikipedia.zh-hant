[Sco-CenMap.png](https://zh.wikipedia.org/wiki/File:Sco-CenMap.png "fig:Sco-CenMap.png")
**天蝎-半人馬星協**（**Sco-Cen**）是最靠近[太陽的](../Page/太陽.md "wikilink")[OB星協](../Page/OB星協.md "wikilink")，這個星協可以分為三個次集團（上天蝎、上半人馬-豺狼和下半人馬-南十字），每一部分的平均距離在380至470[光年](../Page/光年.md "wikilink")\[1\]。天蝎-半人馬星協次集團的年齡大約從500萬至1,500萬年（上半人馬-豺狼和下半人馬-南十字）。許多在[天蝎座](../Page/天蝎座.md "wikilink")、[豺狼座和](../Page/豺狼座.md "wikilink")[半人馬座的藍色亮星](../Page/半人馬座.md "wikilink")，[心宿二](../Page/心宿二.md "wikilink")（上天蝎內質量最重的恆星）以及大多數[南十字座的恆星](../Page/南十字座.md "wikilink")，都是這個[星協的成員](../Page/星協.md "wikilink")。在**天蝎-半人馬星協**有數百顆質量從太陽的15倍（心宿二）到（也就是[褐矮星](../Page/褐矮星.md "wikilink")）的恆星\[2\]，這三個次集團總合起來的總星數在1,000至2,000顆之間\[3\]。

**天蝎-半人馬星協**恆星成員的[自行是](../Page/自行.md "wikilink")0.02至0.04[弧秒](../Page/弧秒.md "wikilink")/[年](../Page/年.md "wikilink")，這表示這些恆星的運動几乎都是平行的，相對於[太陽大約是](../Page/太陽.md "wikilink")20Km/s。在集團內的小群內的速度差異大約是1至2Km/s\[4\]，小組之間似乎並未受到重力的拘束。在過去的1,500萬年有幾顆**天蝎-半人馬星協**的[超新星爆炸](../Page/超新星.md "wikilink")，包括[Loop
I
Bubble](../Page/本星系泡.md "wikilink")\[5\]，在集團的附近殘留了擴張中的氣體網絡。要解釋覆蓋在深海中的鐵錳硬殼內的[放射性](../Page/放射性.md "wikilink")<sup>60</sup>Fe，曾經被假設大約300萬年前在[太陽附近爆炸的](../Page/太陽.md "wikilink")[超新星](../Page/超新星.md "wikilink")，可能就是**天蝎-半人馬星協**的成員\[6\]。

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:星協](../Category/星協.md "wikilink")

1.
2.
3.
4.
5.
6.