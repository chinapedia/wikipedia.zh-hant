**綿矢莉莎**\[1\]（、），本名「山田梨沙」，是一位[日本](../Page/日本.md "wikilink")[小說家](../Page/小說家.md "wikilink")，[早稻田大学教育學部](../Page/早稻田大学.md "wikilink")[國語國文學科畢業](../Page/日本文学.md "wikilink")。

## 人物

  - 筆名由來是依[姓名學參考高中同學姓氏](../Page/姓名學.md "wikilink")「」、本名「」。

<!-- end list -->

  - 出生於[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[左京區](../Page/左京區.md "wikilink")，自幼就喜歡閱讀，包括[江戶川亂步](../Page/江戶川亂步.md "wikilink")、[那須正幹的作品與](../Page/那須正幹.md "wikilink")《[愛麗絲夢遊仙境](../Page/愛麗絲夢遊仙境.md "wikilink")》\[2\]。中學時接觸了[瑪格麗特·米契爾的代表作](../Page/瑪格麗特·米契爾.md "wikilink")《[飄](../Page/飄.md "wikilink")》與[田邊聖子的](../Page/田邊聖子.md "wikilink")《言い寄る》\[3\]。
  - 在17歲[高中就讀時](../Page/高中.md "wikilink")，憑著小說《[Install未成年載入](../Page/Install未成年載入.md "wikilink")》奪得第三十八屆[文藝賞](../Page/文藝賞.md "wikilink")，19歲就讀[大學一年級時](../Page/大學.md "wikilink")，以《[欠踹的背影](../Page/欠踹的背影.md "wikilink")》一書榮獲第一百三十屆[芥川龍之介獎](../Page/芥川龍之介獎.md "wikilink")，成為該兩樣獎項最年輕的得獎作家。

## 作品

  - 《[Install未成年載入](../Page/Install未成年載入.md "wikilink")》（），[河出書房新社](../Page/河出書房新社.md "wikilink")，2001年11月出版，ISBN
    4-309-01437-2 / [尖端出版](../Page/尖端出版.md "wikilink")，2005年3月出版，ISBN
    957-1025771
  - 《[欠踹的背影](../Page/欠踹的背影.md "wikilink")》（），河出書房新社，2003年8月26日出版，ISBN
    4-309-01570-0 / [尖端出版](../Page/尖端出版.md "wikilink")，2004年12月出版，ISBN
    957-8035128
  - 《[給夢的女孩](../Page/給夢的女孩.md "wikilink")》（），河出書房新社，2007年2月8日出版，ISBN
    978-4309018041 / 平裝本出版社，2008年8月25日出版，ISBN 978-9578037076
  - 《[這樣不是太可憐了嗎？](../Page/這樣不是太可憐了嗎？.md "wikilink")》（かわいそうだね？）
  - 《[手寫信](../Page/手寫信.md "wikilink")》（），新潮社，2012年出版，ISBN 978-4103326212
    / 平裝本出版社，2014年8月出版，ISBN 978-9578039209

## 參註

## 参考文献

<div class="references-small">

  -

</div>

## 外部連結

  - [](https://web.archive.org/web/20150401133115/http://www.tokyo-concerts.co.jp/index.cfm?lang=jp&menu=artists&artistid=036)
    -

[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:日本小說家](../Category/日本小說家.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:芥川獎獲獎者](../Category/芥川獎獲獎者.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")

1.  台版作品的譯名。中國大陸版初採「绵矢丽莎」，後與台版統一。
2.  以上
3.  [](http://www.webdoku.jp/rensai/sakka/michi08.html)