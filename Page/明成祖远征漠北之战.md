**明太宗远征漠北之战**指的是明朝[永乐年间](../Page/永乐_\(明朝\).md "wikilink")，明成祖[永樂帝分别于永乐七年](../Page/永樂帝.md "wikilink")（1409年）、十二年（1414年）、二十年（1422年）、二十一年（1423年）、二十二年（1424年）五次亲征对盘据在漠北的北元残余势力[鞑靼](../Page/鞑靼.md "wikilink")、[瓦剌和](../Page/瓦剌.md "wikilink")[兀良哈三个部落的战争](../Page/兀良哈.md "wikilink")，致使[蒙古势力进一步削弱](../Page/蒙古.md "wikilink")，维护了明朝边境的安宁，但無法消滅其殘餘勢力。永樂帝亦在第五次亲-{征}-得胜回朝途中病故。

## 背景

[Anonymous-Ming_Chengzu.jpg](https://zh.wikipedia.org/wiki/File:Anonymous-Ming_Chengzu.jpg "fig:Anonymous-Ming_Chengzu.jpg")坐像
[臺北](../Page/臺北.md "wikilink")[國立故宮博物院藏](../Page/國立故宮博物院.md "wikilink")。\]\]
[明太祖](../Page/明太祖.md "wikilink")[洪武元年](../Page/洪武.md "wikilink")（1368年），朱元璋派[徐达率军攻入](../Page/徐达.md "wikilink")[元大都](../Page/元大都.md "wikilink")（今[北京](../Page/北京.md "wikilink")），[元惠宗逃往漠北继续](../Page/元惠宗.md "wikilink")[元帝国的统治](../Page/元帝国.md "wikilink")，历称“[北元](../Page/北元.md "wikilink")”。[捕鱼儿海之战后](../Page/捕鱼儿海之战.md "wikilink")，北元分裂成[鞑靼](../Page/鞑靼.md "wikilink")、[瓦剌和](../Page/瓦剌.md "wikilink")[兀良哈三部](../Page/兀良哈.md "wikilink")，三个部落经常互相残杀，时常滋扰明朝边境。

[永乐七年](../Page/永乐_\(明朝\).md "wikilink")（1409年），永樂帝派使者[郭骥出使鞑靼](../Page/郭骥.md "wikilink")，结果被杀，永樂帝决心征讨。

## 第一次北伐

[明成祖第一次亲征漠北.png](https://zh.wikipedia.org/wiki/File:明成祖第一次亲征漠北.png "fig:明成祖第一次亲征漠北.png")
明永乐七年（1409年），[本雅失裏殺大明使臣](../Page/本雅失裏.md "wikilink")[郭驥](../Page/郭驥.md "wikilink")，永樂帝派淇国公[丘福率十万大军征讨鞑靼](../Page/丘福.md "wikilink")，由于轻敌，孤军深入，中了敌人的埋伏，全军覆没。为消除边患，永樂帝决心亲征。

明永乐八年（1410年）二月，永樂帝调集五十万大军。\[1\]五月八日，明军行至胪朐河（今[克鲁伦河](../Page/克鲁伦河.md "wikilink")，成祖将之更名为“饮马河”）流域，询得鞑靼可汗[本雅失里率军向西逃往瓦剌部](../Page/本雅失里.md "wikilink")，丞相[阿鲁台则向东逃](../Page/阿鲁台.md "wikilink")。永樂帝亲率将士向西追击本雅失里，五月十三日，明军在[斡难河](../Page/斡难河.md "wikilink")（位于今蒙俄边境）大败本雅失里。\[2\]永樂帝打败本雅失里后，挥师向东攻击阿鲁台，双方在今蒙俄边境之斡难河东北方向交战，明军杀敌无数，阿鲁台坠马逃遁。此时天气炎热，缺水，且粮草不济，成祖下令班师。

鞑靼因此次打击臣服于大明，并向永樂帝进贡马匹，永樂帝亦给予优厚的赏赐，其部臣阿鲁台接受了永樂帝给他“和宁王”的封号。

## 第二次北伐

[明成祖第二次亲征漠北.png](https://zh.wikipedia.org/wiki/File:明成祖第二次亲征漠北.png "fig:明成祖第二次亲征漠北.png")
明军在永乐八年（1410年）出征[鞑靼后](../Page/鞑靼.md "wikilink")，[瓦剌部趁机迅速发展壮大](../Page/瓦剌.md "wikilink")，永乐十一年（1413年），瓦剌军进驻胪朐河（今[克鲁伦河](../Page/克鲁伦河.md "wikilink")），窥视[中国](../Page/中国.md "wikilink")。

永樂帝决心再次亲征，调集兵力，筹集粮饷。[永乐十二年](../Page/永乐.md "wikilink")（1414年）二月，明军从[北京出发](../Page/北京.md "wikilink")，六月初三，明军在三峡口（今[蒙古](../Page/蒙古.md "wikilink")[乌兰巴托东南](../Page/乌兰巴托.md "wikilink")）击败了瓦剌部的一股游兵，杀敌数十騎；初七日，明军行至勿兰忽失温（今[蒙古](../Page/蒙古.md "wikilink")[乌兰巴托东南](../Page/乌兰巴托.md "wikilink")），瓦剌军三万之众，依托山势，分三路阻抗，永樂帝派骑兵冲击，引诱敌兵离开山势，遂命[柳升发炮轰击](../Page/柳升.md "wikilink")，自己亦亲率铁骑杀入敌阵，瓦剌军败退，永樂帝乘势追击，兵分几路夹击瓦剌军的所扑，杀敌数千，瓦剌军纷纷败逃。此役，瓦剌受到重创，此后多年不敢犯边。

## 第三次北伐

[瓦剌被明軍打败](../Page/瓦剌.md "wikilink")，[鞑靼趁此机会经过几年的发展](../Page/鞑靼.md "wikilink")，势力日益强盛起来，从而改变对明朝的依附政策，并侮辱或拘留明朝派去的使节，还时常对明朝边境进行骚扰的劫掠。永乐十九年（1421年）冬初，鞑靼围攻明北方重镇[兴和](../Page/兴和.md "wikilink")，杀死了明军指挥官[王祥](../Page/王祥.md "wikilink")，对此，永樂帝决定第三次亲征漠北。

[永乐二十年](../Page/永乐.md "wikilink")（1422年）三月，永樂帝率軍从[北京出发](../Page/北京.md "wikilink")，出击[鞑靼](../Page/鞑靼.md "wikilink")。其主力部队至宣府（今[河北](../Page/河北.md "wikilink")[宣化](../Page/宣化.md "wikilink")）东南的鸡鸣山时，鞑靼首领[阿鲁台得知明军来袭](../Page/阿鲁台.md "wikilink")，乘夜逃离兴和，避而不战。七月，明军到达煞胡原，俘获鞑靼的部属，得知[阿鲁台已逃走](../Page/阿鲁台.md "wikilink")，成祖下令停止追击。明军在回师途中，击败兀良哈部，九月，回师北京。永樂帝第三次出击漠北，虽对鞑靼部有一定的打击，但成效不大，并没彻底解决盘据漠北的[蒙古三个部落对明朝边境的滋扰](../Page/蒙古.md "wikilink")。

## 第四次北伐

[永乐二十一年](../Page/永乐.md "wikilink")（1423年），[鞑靼首领](../Page/鞑靼.md "wikilink")[阿鲁台再次率部滋扰明朝边境](../Page/阿鲁台.md "wikilink")，成祖闻悉后决定再次亲征。明军八月初出征，九月上旬，明军到达沙城（今[河北](../Page/河北.md "wikilink")[张北以北](../Page/张北.md "wikilink")）时，阿鲁台的部下[阿失贴木儿率部投降明军](../Page/阿失贴木儿.md "wikilink")，并得知阿鲁台被[瓦剌打败](../Page/瓦剌.md "wikilink")，其部已溃散，明军暂时驻扎不前；十月，明军继续北上，在黄河以北击败鞑靼西部的军队，鞑靼王子[也先土干率部众来降明](../Page/也先土干.md "wikilink")，永樂帝随即封也先土干为忠勇王，十一月，明军班师回京。

## 第五次北伐

永樂二十二年（1424年）正月至七月，明軍对蒙古[鞑靼部的作戰](../Page/鞑靼.md "wikilink")。是年正月，鞑靼部首领[阿鲁台率軍進犯明山西大同](../Page/阿鲁台.md "wikilink")、开平（今内蒙古正兰旗东北）等地。成祖遂调集山西、山东、河南、陕西、辽东5都司之兵于京师（今北京）和宣府（今河北宣化）待命。四月三日，以安远侯[柳升](../Page/柳升.md "wikilink")、遂安伯[陈英为中军](../Page/陈英.md "wikilink")；武安侯[郑亨](../Page/郑亨.md "wikilink")、保定侯[孟瑛为左哨](../Page/孟瑛.md "wikilink")，阳武侯[薛禄](../Page/薛禄.md "wikilink")、新宁伯[谭忠为右哨](../Page/谭忠.md "wikilink")；英国公[张辅](../Page/张辅.md "wikilink")、成国公[朱勇为左掖](../Page/朱勇.md "wikilink")，成山侯[王通](../Page/王通_\(明朝\).md "wikilink")、兴安伯[徐亨为右掖](../Page/徐亨.md "wikilink")；宁阳侯[陈懋](../Page/陈懋.md "wikilink")、忠勇王[金忠又名](../Page/金忠.md "wikilink")[也先土干为前锋](../Page/也先土干.md "wikilink")，出兵北征。二十五日，进至[隰宁](../Page/隰宁.md "wikilink")（今河北沽源南），获悉阿鲁台逃往[答兰纳木儿河](../Page/答兰纳木儿河.md "wikilink")（今蒙古境内之哈剌哈河下游），永樂帝令全军急速追击。六月十七日，进至答兰纳木儿河，周围三百余里不见阿鲁台部踪影，遂下令班师。七月十八日，永樂帝在回京途中病死于[榆木川](../Page/榆木川.md "wikilink")（今[内蒙古多伦西北](../Page/内蒙古.md "wikilink")），死時仍處於明蒙邊境。

至此[蒙古势力暂时削弱](../Page/蒙古.md "wikilink")，却未彻底解决边患。在永樂帝病故僅二十五年，便發生[土木之變](../Page/土木之變.md "wikilink")。

## 参考文献

## 外部链接

  - [影响中国的100次战争--明成祖远征漠北之战](https://web.archive.org/web/20041120193758/http://rghqxx.sdedu.net/100zhanzheng/063.htm)
  - [军神在线--明朝军事史](http://www.fs7711.com/01ZGZZ/17MING/)
  - [明成祖远征](http://book.pans.cn/%C6%E4%CB%FB%C0%FA%CA%B7%CA%E9%BC%AE/%D7%A8%CC%E2%C0%E0/%BE%FC%CA%C2/%D3%B0%CF%EC%D6%D0%B9%FA%B5%C4100%B4%CE%D5%BD%D5%F9/063.htm)

## 参见

  - [明朝统一战争](../Page/明朝统一战争.md "wikilink")
      - [徐达北伐](../Page/徐达北伐.md "wikilink")
  - [明太祖北伐](../Page/明太祖北伐.md "wikilink")
  - [土木之变](../Page/土木之变.md "wikilink")

{{-}}

[Category:明成祖](../Category/明成祖.md "wikilink")
[Category:明朝战争](../Category/明朝战争.md "wikilink")
[Category:各蒙古汗国战争](../Category/各蒙古汗国战争.md "wikilink")
[Category:15世纪亚洲战争](../Category/15世纪亚洲战争.md "wikilink")
[Category:1400年代中国](../Category/1400年代中国.md "wikilink")
[Category:1410年代中国](../Category/1410年代中国.md "wikilink")
[Category:1420年代中国](../Category/1420年代中国.md "wikilink")

1.
2.  《明史·鞑靼列传》，中华书局1974年版，卷327