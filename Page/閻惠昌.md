**閻惠昌**（），[指揮家](../Page/指揮家.md "wikilink")、[作曲家](../Page/作曲家.md "wikilink")。出生於[陕西省](../Page/陕西省.md "wikilink")[渭南市](../Page/渭南市.md "wikilink")[合阳县](../Page/合阳县.md "wikilink")。現任[香港中樂團藝術總監兼首席指揮](../Page/香港中樂團.md "wikilink")、[臺灣國樂團藝術總監](../Page/臺灣國樂團.md "wikilink")、[上海音樂學院特聘教授](../Page/上海音樂學院.md "wikilink")、碩士生導師，[四川音樂學院](../Page/四川音樂學院.md "wikilink")、[廣州星海音樂學院等大學客座教授](../Page/廣州星海音樂學院.md "wikilink")。曾任[中央民族樂團首席指揮](../Page/中央民族樂團.md "wikilink")、[中國音樂學院客座指揮教授](../Page/中國音樂學院.md "wikilink")、[台灣](../Page/台灣.md "wikilink")[高雄市實驗國樂團駐團客席指揮](../Page/高雄市實驗國樂團.md "wikilink")。

妻子為[古箏演奏家](../Page/古箏.md "wikilink")[熊岳](../Page/熊岳.md "wikilink")。兩人育有一女，為女歌手[閻奕格](../Page/閻奕格.md "wikilink")。

## 生平

1972年，[18岁考入](../Page/18岁.md "wikilink")[西安音乐学院附中](../Page/西安音乐学院附中.md "wikilink")。畢業后，在[陝西省藝術學院戲曲系擔任樂隊教研室主任兼](../Page/陝西省藝術學院.md "wikilink")[指揮](../Page/指揮.md "wikilink")。

1983年從[上海音樂學院作曲指揮系畢業](../Page/上海音樂學院.md "wikilink")，进入[北京](../Page/北京.md "wikilink")[中央民族乐团擔任首席指挥](../Page/中央民族乐团.md "wikilink")。1987年獲評[中國一級指揮職稱](../Page/中華人民共和國.md "wikilink")。1993年9月受聘擔任[新加坡風格公司古典音樂製作部音樂總監](../Page/新加坡.md "wikilink")。1995年至1997年出任台灣高雄市實驗國樂團駐團客席指揮。1997年6月1日起獲[香港市政局委任為香港中樂團音樂總監](../Page/市政局.md "wikilink")，帶領香港中樂團從[英國管治時期過渡到](../Page/英國.md "wikilink")[香港特別行政區時期](../Page/香港特別行政區.md "wikilink")。2001年完成香港中樂團公司化的轉型改革。2003年10月1日起職稱改為藝術總監兼首席指揮，直到現在。現兼任[中國民族管弦樂學會常任理事等職](../Page/中國民族管弦樂學會.md "wikilink")。

2001年獲新加坡[国家艺术理事会頒發](../Page/国家艺术理事会.md "wikilink")「[文化獎](../Page/文化獎.md "wikilink")」。\[1\]2004年獲[香港特別行政區政府頒授](../Page/香港特別行政區政府.md "wikilink")「[銅紫荊星章](../Page/銅紫荊星章.md "wikilink")」，以表揚他在推廣中樂方面的貢獻。\[2\]
曾多次應邀到[台灣指揮及錄製音樂光盤](../Page/台灣.md "wikilink")，在香港和中國內地也有許多錄音作品。他指揮的大型[佛教音樂作品](../Page/佛教.md "wikilink")《如來世家》與《二胡現代作品集》分別獲得1991年、1992年台灣「[唱片金鼎獎](../Page/金鼎獎.md "wikilink")」。2010年他榮獲[中國文藝協會頒發的](../Page/中國文藝協會.md "wikilink")[中國文藝獎章海外文藝獎](../Page/中國文藝獎章.md "wikilink")。\[3\]\[4\]

2018年以[臺灣國樂團發行的專輯](../Page/臺灣國樂團.md "wikilink")《傾聽
臺灣土地的聲音風景》，榮獲[第29屆金曲獎傳統暨藝術音樂類](../Page/第29屆金曲獎.md "wikilink")「最佳詮釋獎指揮或其他類」。

## 作品

  - 錄音作品

<!-- end list -->

  - 《龜茲古韻》

<!-- end list -->

  -
    指揮

<!-- end list -->

  - 《女媧》

<!-- end list -->

  -
    指揮

<!-- end list -->

  - 《黑土歌》

<!-- end list -->

  -
    指揮，與[馮少先合作](../Page/馮少先.md "wikilink")

<!-- end list -->

  - 《胡琴傳奇》

<!-- end list -->

  -
    指揮，與[丁魯峰合作](../Page/丁魯峰.md "wikilink")

<!-- end list -->

  - 《鄉土頌歌五十年》

<!-- end list -->

  -
    監製，與[郭哲誠分任指揮](../Page/郭哲誠.md "wikilink")

<!-- end list -->

  - 《國語老歌金曲集》

<!-- end list -->

  -
    監製、指揮

<!-- end list -->

  - 《樂韻悠揚台灣情》

<!-- end list -->

  -
    監製

<!-- end list -->

  - 《云》
  - 《月》
  - 《黄河音乐之旅》
  - 《境》
  - 《山鬼》
  - 《将军令》

<!-- end list -->

  - 音樂作品

<!-- end list -->

  - 《水之聲民族交響音畫》

<!-- end list -->

  -
    獲中國第三屆音樂作品評獎2等獎

<!-- end list -->

  - 《儺三弦協奏曲》

<!-- end list -->

  -
    完成於1993年，於[台北首演](../Page/台北.md "wikilink")，作者試圖以三弦獨特粗獷的音色，調動各種音樂表現手法來展現民間儺祭的生動場面。\[5\]

<!-- end list -->

  - 《幻二胡協奏曲》
  - 《瀟湘水雲古箏協奏曲》
  - 《鵲橋仙雙二胡協奏曲》
  - 《眾仙會》

<!-- end list -->

  -
    [歌仔戲曲調](../Page/歌仔戲.md "wikilink")《留傘調》改編

<!-- end list -->

  - 《弦語寄情》
  - 《人天眼目》

<!-- end list -->

  -
    大型佛教音乐作品

## 註釋

[Y閻](../Category/新加坡人.md "wikilink") [Y閻](../Category/合陽人.md "wikilink")
[Y閻](../Category/香港作曲家.md "wikilink")
[Y閻](../Category/香港指揮家.md "wikilink")
[Y閻](../Category/中国指挥家.md "wikilink")
[Y閻](../Category/中国作曲家.md "wikilink")
[Y閻](../Category/上海音乐学院校友.md "wikilink")
[H惠](../Category/閻姓.md "wikilink")

1.
2.
3.
4.
5.  [1](http://www.mousehk.net/agent/db/00017709.txt)