[Map_of_New_York_highlighting_Suffolk_County.svg](https://zh.wikipedia.org/wiki/File:Map_of_New_York_highlighting_Suffolk_County.svg "fig:Map_of_New_York_highlighting_Suffolk_County.svg")
**蘇福克郡**（****）是[美國](../Page/美國.md "wikilink")[紐約州的一個县](../Page/紐約州.md "wikilink")，位於[長島東部](../Page/長島_\(紐約\).md "wikilink")，北向[長島海灣](../Page/長島海灣.md "wikilink")，南面[大西洋](../Page/大西洋.md "wikilink")，西接[納蘇郡](../Page/納蘇郡_\(紐約州\).md "wikilink")，是紐約州與[紐約大都會區最東邊的郡](../Page/紐約大都會區.md "wikilink")。面積6,146平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口1,419,369人。縣治。

此縣成立於1683年，是該州最早的十二個縣之一。縣名紀念[英國](../Page/英國.md "wikilink")-{[沙福克郡](../Page/沙福克郡.md "wikilink")}-。

## 地理

萨福克县位于长岛东部，占该岛约三分之二的面积，其西邻是[拿骚县](../Page/納蘇縣_\(紐約州\).md "wikilink")。长岛最东端的南北两个半岛分别被称为和，两个半岛将[佩科尼克湾](../Page/佩科尼克湾.md "wikilink")（）与[大西洋隔开](../Page/大西洋.md "wikilink")。佩科尼克湾以东，越过[谢尔特岛](../Page/谢尔特岛.md "wikilink")（）的海湾，则称作[加迪纳斯湾](../Page/加迪纳斯湾.md "wikilink")（）。萨福克县的北端，则是[长岛海湾](../Page/长岛海湾.md "wikilink")，其南端面向大西洋。

萨福克县东南端的半岛上的汉普顿地区（）的海滩是美国东北部历史上有名的避暑胜地，也是美国最贵的住宅地所在。

[Category:紐約州行政區劃](../Category/紐約州行政區劃.md "wikilink")
[Category:長島](../Category/長島.md "wikilink")