**加斯科內德县**（**Gasconade County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州中部的一個縣](../Page/密蘇里州.md "wikilink")。面積1,363平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口15,342人。縣治[赫曼](../Page/赫曼_\(密蘇里州\).md "wikilink")（Hermann）。

成立於1820年11月25日，1821年1月1日生效。縣名來自[加斯科內德河](../Page/加斯科內德河.md "wikilink")
（可能來自[法國昔日的行政區](../Page/法國.md "wikilink")[加斯科涅或](../Page/加斯科涅.md "wikilink")[法語](../Page/法語.md "wikilink")「自大的人」的意思，指的是沿河的印地安人）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/密蘇里州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.