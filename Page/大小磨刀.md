[Westbrother.png](https://zh.wikipedia.org/wiki/File:Westbrother.png "fig:Westbrother.png")
[Siu_Mo_To_1.jpg](https://zh.wikipedia.org/wiki/File:Siu_Mo_To_1.jpg "fig:Siu_Mo_To_1.jpg")
[Tai_Mo_To_1.jpg](https://zh.wikipedia.org/wiki/File:Tai_Mo_To_1.jpg "fig:Tai_Mo_To_1.jpg")

**大小磨刀**（，又作**Tai Siu Mo To**或**Da Siu Mo
To**\[1\]），又稱**大小磨刀洲**、**磨刀洲**（\[2\]），是指[大嶼山以北的三個小](../Page/大嶼山.md "wikilink")[島](../Page/島.md "wikilink")\[3\]：**[大磨刀](../Page/大磨刀.md "wikilink")**（，亦作**Tai
Mo To**）、**[小磨刀](../Page/小磨刀.md "wikilink")**（，亦作**Siu Mo
To**）和**[匙羹洲](../Page/匙羹洲.md "wikilink")**（）。大磨刀洲過去是一個[石墨礦場](../Page/石墨.md "wikilink")──「[五福礦場](../Page/五福礦場.md "wikilink")」的所在地。

在1990年代，為了興建[香港](../Page/香港.md "wikilink")[赤鱲角](../Page/赤鱲角.md "wikilink")[新機場](../Page/香港國際機場.md "wikilink")，大小磨刀被削平以避免影響[飛機升降](../Page/飛機.md "wikilink")，以及取得沙石用作[填海](../Page/填海.md "wikilink")。現在該地無人居住，只餘下公共[直升機坪和小型](../Page/直升機.md "wikilink")[氣象站等設施](../Page/氣象站.md "wikilink")。

2007年3月，[食物環境衞生署建議於](../Page/食物環境衞生署.md "wikilink")[塔門以東](../Page/塔門.md "wikilink")、[東龍洲以東](../Page/東龍洲.md "wikilink")、大小磨刀以東及[西博寮海峽以南等四個地點](../Page/西博寮海峽.md "wikilink")，設定指定的[海葬區](../Page/海葬.md "wikilink")。但大小磨刀的建議，受到[屯門區議會以未經諮詢為理由強烈反對](../Page/屯門區議會.md "wikilink")，故此[環境運輸及工務局暫時擱置此地點作為海葬場](../Page/環境運輸及工務局.md "wikilink")，等待重新選擇地點\[4\]。

2016年6月30日，該處定為[海岸公園](../Page/香港海岸公園.md "wikilink")。

## 事故

  - 1908年7月27日，[英京輪遇颱風沉沒事故](../Page/英京輪遇颱風沉沒事故.md "wikilink")（421死42生還\[5\]\[6\]）
  - 1944年12月24日，[嶺南丸被炸沉事件](../Page/嶺南丸被炸沉事件.md "wikilink")（349死23傷\[7\]\[8\]）
  - 2008年3月22日，[龍鼓水道撞船事故](../Page/龍鼓水道撞船事故.md "wikilink")（18死7生還）

## 區議會議席分佈

大小磨刀島上沒有任何居民，故只會劃入陸上[青山灣](../Page/青山灣.md "wikilink")[三聖墟所屬的選區](../Page/三聖墟.md "wikilink")。

| 年度/範圍                                | 2000-2003                               | 2004-2007 | 2008-2011 | 2012-2015 | 2016-2019 | 2020-2023 |
| ------------------------------------ | --------------------------------------- | --------- | --------- | --------- | --------- | --------- |
| [大小磨刀全島](../Page/大小磨刀.md "wikilink") | [三聖選區](../Page/三聖_\(選區\).md "wikilink") |           |           |           |           |           |

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 參見

  - [香港島嶼](../Page/香港島嶼.md "wikilink")
  - [大磨刀](../Page/大磨刀.md "wikilink")
  - [小磨刀](../Page/小磨刀.md "wikilink")
  - [匙羹洲](../Page/匙羹洲.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

{{-}}

[大小磨刀](../Category/大小磨刀.md "wikilink")
[Category:香港礦業](../Category/香港礦業.md "wikilink")

1.  “The Brothers”爲官方英文名稱，也是坊間主流使用的英文名稱，其餘按照粵音拼讀的英文名稱較少見。
2.  此英文名稱僅對應中文名稱「磨刀洲」三字，但意涵與“The Brothers”是相同的。
3.  亦有人認爲大小磨刀及其英語“The Brothers”之名僅指大磨刀和小磨刀，不包括匙羹洲。
4.
5.
6.
7.
8.