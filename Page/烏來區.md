[sakura_in_Wulai.JPG](https://zh.wikipedia.org/wiki/File:sakura_in_Wulai.JPG "fig:sakura_in_Wulai.JPG")
**烏來區**是[中華民國](../Page/中華民國.md "wikilink")[新北市的一個](../Page/新北市.md "wikilink")[市轄區](../Page/市轄區.md "wikilink")，境內多山，是[新北市面積最大的行政區](../Page/新北市.md "wikilink")，也是[新北市最南境的行政區](../Page/新北市.md "wikilink")。依照《地方制度法》，烏來區係[直轄市山地原住民區](../Page/直轄市山地原住民區.md "wikilink")，為具有公法人地位的地方自治團體，與新北市其他市轄區的性質並不相同，享有相當於鄉鎮市的地方自治權，烏來區公所為其自治事項之行政機關，而非新北市政府的派出機關，並設置烏來區民代表會作為其自治事項之立法機關。

烏來區觀光旅遊以[溫泉及山](../Page/溫泉.md "wikilink")[櫻花著稱](../Page/櫻花.md "wikilink")。\[1\]烏來也是台灣分佈最北的原住民山地區，居民以泰雅族為主，「烏來」此一地名即源自於原住民[泰雅族語中的](../Page/泰雅族.md "wikilink")「Ulay」，意為水很燙要小心「Kiluh-ulay」\[2\]。烏來區泰雅族原住民在[清治及](../Page/臺灣清治時期.md "wikilink")[日治時期](../Page/台灣日治時期.md "wikilink")，曾被稱做「屈尺番」。

## 歷史沿革

### 清治時期

烏來泰雅族被稱為「屈尺群」，源於所居地位屈尺之南。依照廖守臣有關遷移史之論述，烏來泰雅族屈尺群組社有三:
李茂岸社、塔拉南社(大羅蘭)及札亞孔社(茶墾)，乃以烏來區福山里為發源點擴展出去。\[3\]

### 日治時期

明治28年(1895年)日本領台後，由陸軍局發行之「台灣蕃地圖」，記載屈尺群有八個社，分別為後坑仔社、納仔社、大舌社、湯裡社、夾精社、內枋山社、外枋山社、林望眼社，通稱內外馬來大八社。明治30年(1897年)為台北通往宜蘭鐵路建設計畫進行調查，派遣探險隊進入烏來，完成後由橫山壯次郎技師呈報「蕃地探險報告書」中，說明烏來有四個蕃社:ウライ社、ラガー社、枋山社以及林望眼社。\[4\][台灣日治時期](../Page/台灣日治時期.md "wikilink")，為[台北州](../Page/臺北州.md "wikilink")[文山郡直轄的原住民](../Page/文山郡.md "wikilink")[蕃地](../Page/蕃地_\(文山郡\).md "wikilink")。

### 戰後時期

[日本投降後](../Page/二戰結束.md "wikilink")，1945年改制為臺北縣烏來鄉（但轄區與文山郡的蕃地範圍，略有變動）。

2010年12月25日，臺北縣改制為[直轄市並改名為新北市](../Page/2010年中華民國縣市改制直轄市.md "wikilink")，臺北縣烏來鄉也配合改制成新北市烏來區，失去原本作為鄉所具有的地方自治權和地方自治團體地位。2014年12月25日，配合《地方制度法》修正，賦予直轄市山地原住民區地方自治權，烏來區再次改制為具有公法人地位的地方自治團體，成為新北市轄域內唯一一個區民可選舉區長與區民代表的市轄區\[5\]。
[鷹乙千株櫻碑.jpg](https://zh.wikipedia.org/wiki/File:鷹乙千株櫻碑.jpg "fig:鷹乙千株櫻碑.jpg")、五百株[梅花給烏來的伊藤鷹乙](../Page/梅花.md "wikilink")\[6\]。\]\]

## 地理

烏來地區為新北市最南端，屬於第三紀亞變質岩區，厚層硬頁岩夾薄層粉砂岩的乾溝層，呈典型幼年期河谷地形，多峽谷斷崖及瀑布。\[7\]東北邊以烘爐山地、羅宏山、大桶山與坪林區、石碇區交界，北邊則以大桶山、四明山與新店區為界，西北方由卡保山、樂佩山、北插天山與三峽區相鄰，西南則是以北插天山、魯培山、拉拉山、塔曼山、巴博庫魯山與桃園市復興區相鄰；東南由雪山山脈之巴博庫魯山、棲蘭山、拳頭母山、阿玉山、烘爐山地與宜蘭縣礁溪鄉、員山鄉、三星鄉及大同鄉交界。

### 水系

烏來區水系屬南勢溪流域，南勢溪源自[雪山山脈主脊的](../Page/雪山山脈.md "wikilink")[棲闌山](../Page/棲闌山.md "wikilink")、[拳頭母山間之](../Page/拳頭母山.md "wikilink")[松羅湖](../Page/松羅湖.md "wikilink")。南勢溪總長45公里，支流包括哈盆溪、扎孔溪、大羅南溪、內洞溪、桶後溪、加九寮溪和烏來溪(雲仙樂園)。

## 行政區與人口結構

烏來佔地雖然廣大，但因人口稀少全境只劃分為五個里級次行政區。境內的地名多為[泰雅語發音的漢譯轉寫](../Page/泰雅語.md "wikilink")：

  - 忠治里：忠治（桶坪Tanpya／Kayo）、加九寮、成功、堰堤
  - 烏來里：烏來街區（Ulai）、野要（Yayao）、西羅岸（Silagan）、啦卡（Laga）
  - 信賢里：哪哮（羅好Lahao）
  - 孝義里：孝義（阿玉Agek）、桶後
  - 福山-{里}-：屯鹿（Tonlok）、哈盆（Habun）、福山（李茂岸Limogan、大羅蘭Talanan、卡拉模基Kalamochi）、馬岸

<table>
<caption>烏來區各里鄰戶口數（2014年底）[8]</caption>
<thead>
<tr class="header">
<th><p>里名</p></th>
<th><p>面積<br />
（km<sup>2</sup>）</p></th>
<th><p>鄰數</p></th>
<th><p>戶數</p></th>
<th><p>人口</p></th>
<th><p>人口密度<br />
（人/km<sup>2</sup>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>忠治里</p></td>
<td><p>25.8142</p></td>
<td><p>7</p></td>
<td><p>510</p></td>
<td><p>1,827</p></td>
<td><p>70.8</p></td>
</tr>
<tr class="even">
<td><p>烏來里</p></td>
<td><p>28.9031</p></td>
<td><p>15</p></td>
<td><p>842</p></td>
<td><p>2,716</p></td>
<td><p>94.0</p></td>
</tr>
<tr class="odd">
<td><p>孝義里</p></td>
<td><p>51.4106</p></td>
<td><p>6</p></td>
<td><p>47</p></td>
<td><p>261</p></td>
<td><p>5.1</p></td>
</tr>
<tr class="even">
<td><p>信賢里</p></td>
<td><p>54.6889</p></td>
<td><p>9</p></td>
<td><p>195</p></td>
<td><p>622</p></td>
<td><p>11.4</p></td>
</tr>
<tr class="odd">
<td><p>福山-{里}-</p></td>
<td><p>160.3138</p></td>
<td><p>6</p></td>
<td><p>198</p></td>
<td><p>739</p></td>
<td><p>4.6</p></td>
</tr>
<tr class="even">
<td><p>總計</p></td>
<td><p>321.1306</p></td>
<td><p>43</p></td>
<td><p>1,792</p></td>
<td><p>6,165</p></td>
<td><p>19.2</p></td>
</tr>
</tbody>
</table>

### 人口結構

2015年底烏來區原住民人口為2,792人，約佔全區人口數之45.13%，其中最多為泰雅族2,525人，佔全區人口數為40.81%，其次為阿美族為128人，僅佔2.06%。\[9\]

## 交通

### 公路

  - [TW_PHW9a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9a.svg "fig:TW_PHW9a.svg")[台9甲線](../Page/台9線#支線.md "wikilink")：新烏公路（忠治－烏來街區）、孝義產業道路（烏來街區－孝義）
  - [TW_THWtp107.png](https://zh.wikipedia.org/wiki/File:TW_THWtp107.png "fig:TW_THWtp107.png")[北107線](../Page/北107線.md "wikilink")：烏來街區－烏來瀑布（溫泉街）－信賢－福山（信福路）
      - [TW_THWtp107-1.png](https://zh.wikipedia.org/wiki/File:TW_THWtp107-1.png "fig:TW_THWtp107-1.png")[北107-1線](../Page/北107線#支線.md "wikilink")：烏來觀光大橋－烏來街區西部－野要－烏來瀑布（環山路）

### 客運

  - [849](../Page/新北市區公車849路線.md "wikilink")
    烏來─台北車站（[新店客運](../Page/新店客運.md "wikilink")）

## 公共設施

  - 警政

<!-- end list -->

  - [新北市政府警察局新店分局](../Page/新北市政府警察局.md "wikilink")
      - 新店分局交通分隊
      - 烏來分駐所
      - 福山派出所
      - 信賢派出所
      - 孝義派出所
      - 忠治派出所

## 教育

  - [國民中學](../Page/國民中學.md "wikilink")

<!-- end list -->

  - [新北市立烏來國民中小學](../Page/新北市立烏來國民中小學.md "wikilink")

<!-- end list -->

  - [國民小學](../Page/國民小學.md "wikilink")

<!-- end list -->

  - [新北市立烏來國民中小學](../Page/新北市立烏來國民中小學.md "wikilink")（國小部）
  - [新北市烏來區福山國民小學](../Page/新北市烏來區福山國民小學.md "wikilink")

<!-- end list -->

  - [幼兒園](../Page/幼兒園.md "wikilink")

<!-- end list -->

  - [新北市立烏來國民中小學附設幼兒園](../Page/新北市立烏來國民中小學附設幼兒園.md "wikilink")

<!-- end list -->

  - [圖書館](../Page/圖書館.md "wikilink")

<!-- end list -->

  - [新北市立圖書館烏來分館](../Page/新北市立圖書館.md "wikilink")：位於新北市烏來區烏來街34號1樓
  - (烏來圖書館固定「休館」日期：每週一、二、每月最後一個週四、國定假日)

## 旅遊

烏來的旅遊資源相當豐富，除了自然環境之外，風土民情也是[觀光客造訪的重點之一](../Page/觀光客.md "wikilink")。台北市巨量觀光客的注入，使烏來溫泉建築物過度開發，既有業者品質良莠不齊，歷史老泉名聲的確保有待更嚴謹制度管理。除了現代化力量造成原住民文化淪為博物館化外，觀光污染更挑戰管理能力不大的部落。人類活動為最大破壞因子，脆弱生態如何不被殺雞取卵，有賴公民智慧與覺醒。

### 泰雅文化（賽考利克群-馬立巴系統）

烏來是[原住民](../Page/原住民.md "wikilink")[泰雅族的聚居地](../Page/泰雅族.md "wikilink")，所以許多建築或景點都少不了[原住民風俗的點綴](../Page/原住民.md "wikilink")。例如道路上就有仿原住民人物的雕塑創作。而[雲仙樂園也會請](../Page/雲仙樂園.md "wikilink")[原住民公開演出](../Page/原住民.md "wikilink")，使整個遊樂區的氣氛更為熱鬧，也為企業本身帶來了人潮與「錢潮」。

### 溫泉

烏來溫泉的發展可追溯自日治初期，1903年深坑長單野英清於南勢溪谷發現後，設置了烏來蕃務史員駐住所浴場；此外軍事後援組織的「愛國婦人會台灣支部」也在日治時期從事溫泉經營\[10\]。

烏來出名的原因自然少不了溫泉。由於溫泉的發現，使當地旅遊業蓬勃發展，在街上隨處可見[溫泉飯店或是提供泡湯服務的商家](../Page/溫泉飯店.md "wikilink")，甚至連部分山產店也順便做起了這個生意。

### 烏來瀑布與雲仙樂園

[烏來瀑布](../Page/烏來瀑布.md "wikilink")，又稱雲仙瀑布，也是觀光客矚目的焦點。除了瀑布本身有名之外，瀑布上面也是有名的景點－[雲仙樂園](../Page/雲仙樂園.md "wikilink")。雲仙樂園是建造於瀑布上方溪谷的森林遊樂區。樂園內除了有[飯店](../Page/飯店.md "wikilink")、林蔭步道之外，還設有[射箭場](../Page/射箭場.md "wikilink")、[漆彈場](../Page/漆彈場.md "wikilink")、[游泳池等現代化設施](../Page/游泳池.md "wikilink")，吸引[觀光客前來造訪](../Page/觀光客.md "wikilink")。

雲仙樂園的另一個特色在於[交通](../Page/交通.md "wikilink")。樂園與對面主要聚落沒有橋樑可供互通，前往雲仙樂園，只有透過[纜車才能穿梭於聚落與樂園之間](../Page/纜車.md "wikilink")。一般觀光客需要購票才能搭乘前往樂園的纜車。烏來溪及其支流縱橫山谷與水瀑之間，成為各種生物遷徙與棲息的生態廊道。潺潺溪水穿越了茂密森林與人工濕地，造就了園區的生物多樣性。雲仙樂園自民國102年即取得環境教育場所，可藉由體驗、瞭解、欣賞自然環境，產生愛護生命與珍惜環境的行動力，憑藉自然環境的體驗和學習
，養成民眾對自然環境的友善行為。

### 仙遊峽

烏來至瀑布間的南勢溪谷，有深谷峭壁之勝，日治時代以來有仙遊峽之美譽。

### 烏來老街

泰雅族為烏來地區的先住民。進行商業活動的烏來老街的店家招牌採用統一風格，使風貌盡可能一致。山產料理手工藝品，讓老街別具一番風味。每到泡湯及賞櫻的旺季，整條街人聲雜沓，瀰漫著溫泉玉米、山豬肉、小米香腸、溫泉蛋、竹筒飯、月桃飯等美食的香氣。

[Little_Trains_in_Wulai.jpg](https://zh.wikipedia.org/wiki/File:Little_Trains_in_Wulai.jpg "fig:Little_Trains_in_Wulai.jpg")
[Waterfall_in_Wawa_Valley.jpg](https://zh.wikipedia.org/wiki/File:Waterfall_in_Wawa_Valley.jpg "fig:Waterfall_in_Wawa_Valley.jpg")
[waterwall_in_Wulai.JPG](https://zh.wikipedia.org/wiki/File:waterwall_in_Wulai.JPG "fig:waterwall_in_Wulai.JPG")\]\]
[Wulai_Hot_Springs_02.JPG](https://zh.wikipedia.org/wiki/File:Wulai_Hot_Springs_02.JPG "fig:Wulai_Hot_Springs_02.JPG")
[烏來生態農場溫室草莓.jpg](https://zh.wikipedia.org/wiki/File:烏來生態農場溫室草莓.jpg "fig:烏來生態農場溫室草莓.jpg")

### 景點

  - [烏來瀑布](../Page/烏來瀑布.md "wikilink")
  - [烏來台車](../Page/烏來台車.md "wikilink")
  - 文山農場
  - 松蘿湖
  - 大納農場
  - 洪荒峽
  - 烏來老街
  - 烏來林業生活館
  - 烏來瀑布公園及商圈
  - 烏來鳳蝶蝶道
  - 信賢吊橋及信賢步道
  - 雲仙樂園\[烏來空中纜車\]
  - 內洞國家森林遊樂區
  - 紅河谷（越嶺古道）
  - 哈盆營地
  - 福巴越嶺古道
  - 烏來風景特定區
  - [烏來種籽親子實小](../Page/種籽學苑.md "wikilink")
  - [烏來泰雅民族博物館](../Page/烏來泰雅民族博物館.md "wikilink")
  - [哈盆自然保留區](../Page/哈盆自然保留區.md "wikilink"):因２０１５年颱風蘇迪勒，而產生土石流，全都不覆見。
  - [新北市立圖書館烏來分館](../Page/新北市立圖書館.md "wikilink")
  - 五重溪瀑布
  - 福山賞魚步道與景觀橋

## 參見

  - [烏來觀光發展協會](../Page/烏來觀光發展協會.md "wikilink")
  - [山地原住民區](../Page/山地原住民區.md "wikilink")
  - [山地原住民鄉](../Page/山地原住民鄉.md "wikilink")

## 註釋

## 參考資料

  - 鄭安睎、許維真《烏來的山與人》玉山社，2009年。
  - 文崇一、蕭新煌，1997（再版）。《烏來鄉志》（[台灣方志網](http://county.nioerar.edu.tw/books.php?pathway=view&borrowno=f0042582)）。臺北縣烏來鄉：北縣烏來鄉公所。
  - 許家華，劉芝芳（編輯），2010。《烏來鄉志》，。，修訂第一版（[台灣方志網](http://county.nioerar.edu.tw/books.php?pathway=view&borrowno=F0044837)）。臺北縣烏來鄉：北縣烏來鄉公所。ISBN
    978-986-02-4504-2。
  - 楊南郡譯註，
    2011。《臺灣原住民族系統所屬之硏究》（臺北帝囯大学土俗.人種學硏究室原著《台灣高砂族系統所屬の研究》）。臺北市：行政院原民會。ISBN
    9789860267877。

## 外部連結

  - [烏來區公所](http://www.wulai.ntpc.gov.tw/)
  - [《2015蘇迪勒颱風新北市烏來區災害彙整》財團法人中興工程顧問社防災科技研究中心， 2015年](http://61.56.4.176:9000/NCDRFile/DoFile/201513/ExpertInterview/DoFile-1-201513E001000120151204103256-001.pdf)
  - [新店烏來深度之旅](http://subtpg.tpg.gov.tw/web-life/taiwan/9612/9612-15.htm)
  - [烏來老街](https://web.archive.org/web/20110718205238/http://tour.tpc.gov.tw/trip_in01-11.html)
  - [行政院原住民族資訊網](http://www.apc.gov.tw)

[Category:新北市行政區劃](../Category/新北市行政區劃.md "wikilink")
[Category:源自台灣原住民語言的台灣地名](../Category/源自台灣原住民語言的台灣地名.md "wikilink")
[Category:直轄市山地原住民區](../Category/直轄市山地原住民區.md "wikilink")
[烏來區](../Category/烏來區.md "wikilink")

1.  烏來鄉公所，2010，烏來鄉志, p.103

2.  烏來鄉公所，2010，烏來鄉志, p.254

3.  鄭安睎、許維真《烏來的山與人》玉山社，2009年

4.
5.  [直轄市原民區
    可選區長、區代表](http://www.libertytimes.com.tw/2014/new/jan/15/today-p12.htm)
    ，自由時報，2014-1-15

6.

7.  [陳盈璇、周稟珊《烏來地質之美》經濟部中央地質調查所](http://twgeoref.moeacgs.gov.tw/GipOpenWeb/imgAction?f=/2012/20122953/0044.pdf)

8.

9.  [《新北市烏來區105年人口結構分析》新北市烏來區公所](http://www.wulai.ntpc.gov.tw/archive/file/105%E5%B9%B4%E4%BA%BA%E5%8F%A3%E7%B5%90%E6%A7%8B%E5%88%86%E6%9E%90\(3\).pdf)

10.