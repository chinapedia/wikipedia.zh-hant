[HK_Sheung_Wan_Kau_U_Fong_Swings_Mother.JPG](https://zh.wikipedia.org/wiki/File:HK_Sheung_Wan_Kau_U_Fong_Swings_Mother.JPG "fig:HK_Sheung_Wan_Kau_U_Fong_Swings_Mother.JPG")上\]\]
**母愛**是[母親對](../Page/母親.md "wikilink")[子女的關心和愛護](../Page/子女.md "wikilink")，例如把[兒子和](../Page/兒子.md "wikilink")[女兒由](../Page/女兒.md "wikilink")[嬰兒期](../Page/嬰兒.md "wikilink")、[兒童](../Page/兒童.md "wikilink")、[青少年](../Page/青少年.md "wikilink")，直至[成年](../Page/成年.md "wikilink")，供書教學，關懷照顧等。

## 對母愛的解讀

母爱是这个世界上最温柔的力量，它为你保驾护航。

### 歌頌母愛

母愛常被描繪成母親對子女的[恩情](../Page/恩情.md "wikilink")，是[無私](../Page/無私.md "wikilink")、偉大，也是[文學](../Page/文學.md "wikilink")[作品中的常見題材](../Page/作品.md "wikilink")。例如[冰心就以歌頌母愛見稱](../Page/冰心.md "wikilink")，古代也有不少詩詞歌頌母愛，例如[唐朝詩人](../Page/唐朝.md "wikilink")[孟郊的](../Page/孟郊.md "wikilink")《[遊子吟](../Page/遊子吟.md "wikilink")》，表現出母親對子女無微不至的[關懷](../Page/關懷.md "wikilink")。

### 質疑母愛

亦有人認為母愛不過是一種出於[生物](../Page/生物.md "wikilink")[本能的行為](../Page/本能.md "wikilink")，亦不認為是值得歌頌的東西，例如[張愛玲就認為母愛只是](../Page/張愛玲.md "wikilink")[人與其他](../Page/人.md "wikilink")[動物都具有的](../Page/動物.md "wikilink")[本性](../Page/本性.md "wikilink")，不能引以[自豪](../Page/自豪.md "wikilink")
\[1\]甚至認為母愛只是被誇大了、[戲劇化了的](../Page/戲劇化.md "wikilink")[感情](../Page/感情.md "wikilink")
\[2\]。

此外，有人認為母愛並非出於偉大的[犧牲](../Page/犧牲.md "wikilink")，而是一種反應，[法國](../Page/法國.md "wikilink")[存在主義作家](../Page/存在主義.md "wikilink")、[女性主義者](../Page/女性主義.md "wikilink")[西蒙·波娃在著作](../Page/西蒙·波娃.md "wikilink")《[第二性](../Page/第二性.md "wikilink")》中提出，母愛並不是出於天生，而是母親對處境的反應\[3\]。

## 相關

  - [父愛](../Page/父愛.md "wikilink")
  - [母性](../Page/母性.md "wikilink")
  - [親子關係](../Page/親子關係.md "wikilink")
  - [棄嬰](../Page/棄嬰.md "wikilink")
  - [孝](../Page/孝.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:母亲](../Category/母亲.md "wikilink")
[Category:愛](../Category/愛.md "wikilink")
[Category:女性主義](../Category/女性主義.md "wikilink")

1.  張愛玲《造人》 ：「自我犧牲的母愛是美德，可是這種美德是我們的獸祖先遺傳下來的，我們的家畜也同樣具有的——我們似乎不能引以自豪。」
2.  張愛玲《談跳舞》：「母愛這大題目，像一切大題目一樣，上面做了太多的濫調文章。普通一般提倡母愛的都是做儿子而不做母親的男人，而女人，如果也標榜母愛的話，那是她自己明白她本身是不足重的，男人只尊敬她這一點，所以不得不加以誇張，渾身是母親了。其實有些感情是，如果時時把它戲劇化，就光剩下戲劇了；母愛尤其是。」
3.  西蒙·波娃《第二性》
    ：「母愛不是直覺的，天生的，在任何情況下，天生這兩個字眼均不適用於人類。母親對小孩的態度，完全決定於母親的處境以及對此處境的反應。」