**法林明高競賽會**，是一間[巴西的體育會](../Page/巴西.md "wikilink")，位於[里約熱內盧](../Page/里約熱內盧.md "wikilink")。法林明高的足球隊在巴西十分有名，在國際足協二十世紀著名球會（FIFA
Clubs of the 20th Century）排名中，法林明高排第九名。

## 歷史

法林明高在1895年11月15日成立，當時是划行會，由多名[划艇好手成立](../Page/划艇.md "wikilink")。他們成立[划艇會的原因](../Page/划艇會.md "wikilink")，是划艇這運動在十九世紀末的[里約熱內盧是精英運動](../Page/里約熱內盧.md "wikilink")，他們也希望這種運動能令划艇普及化，並在中上階層中的年輕女士聞名。不過他們只能負擔一隻二手[賽艇](../Page/賽艇.md "wikilink")，在變成比賽項目前一定要有改革。划艇隊第一次在1895年10月6日亮相，他們在[Caju
Point的](../Page/Caju_Point.md "wikilink")[Maria Angu
beach出發](../Page/Maria_Angu_beach.md "wikilink")，以[法林明高海灘作為目的地](../Page/Flamengo_beach.md "wikilink")。不過，強風使賽艇翻側，他們更差點遇溺，幸好一艘漁船經過該處把他們救起。在維修賽艇的過程中，賽艇卻被人偷去，從此下落不明，他們要重新購買賽艇。在11月15日的晚上，他們的聚會選出了第一屆管理層。

他們最著名的足球部，開始時都是來自[富明尼斯的一班不合資格的球員](../Page/富明尼斯.md "wikilink")，或是和富明尼斯管理層不咬弦而離隊的人。他們改投法林明高，是因為離隊成員其中之一的隊長[Alberto
Borgeth](../Page/Alberto_Borgeth.md "wikilink")，也是法林明高的划艇手。這班球員的申請於1911年11月8日批准，在一輪調整後，足球部於12月24日正式成立。

他們在[Russel
beach訓練](../Page/Russel_beach.md "wikilink")，獲得當地人民的同情，後來他們都有看他們訓練。而足球隊的首場比賽在1912年5月3日進行，他們以15:2大勝[Mangueira](../Page/Mangueira.md "wikilink")。而第一場法林明高和富明尼斯的打比於7月7日進行，當時富明尼斯勝3:2。

2019年2月8日凌晨，弗拉門戈新建成2個月的訓練中心發生火災，造成10名青少年球员死亡\[1\]。根据初步调查结果，这场火灾是住宿区的空调短路引起\[2\]。

## 足球

### 榮譽

#### *國際*

  - **[洲際杯](../Page/洲際杯.md "wikilink")(1)**: 1981
  - **[南美自由杯](../Page/南美自由杯.md "wikilink")(1)**: 1981

#### *國內*

  - **[巴西足球甲級聯賽](../Page/巴西足球甲級聯賽.md "wikilink")(6)**：1980, 1982, 1983,
    1987, 1992, 2009
  - **[巴西杯](../Page/巴西杯.md "wikilink") (3)**：1990, 2006, 2013

#### *州聯賽*

  - **[州聯賽冠軍](../Page/州聯賽冠軍.md "wikilink")(31)**: 1914, 1915 (以不敗姿態奪冠),
    1920（以不敗姿態奪冠）, 1921, 1925, 1927, 1939, 1942-1944, 1953-1955, 1963,
    1965, 1972, 1974, 1978, 1979（以不敗姿態奪冠）, 1979
    <sup>(**[2](../Page/#2.md "wikilink")**)</sup>, 1981, 1986, 1991,
    1996（以不敗姿態奪冠）, 1999-2001, 2004, 2007-2009

<div id="1">

<sup>(**[1](../Page/#1.md "wikilink")**)</sup>和累西腓體育會分享

<div id="2">

<sup>(**[2](../Page/#2.md "wikilink")**)</sup>當年里約熱內盧州和[瓜納巴拉州的州聯賽合併](../Page/瓜納巴拉州.md "wikilink")，所以法林明高同時奪得兩個冠軍。

### 黃金時期

在1978年，法林明高踏入了黃金時期。當年他們贏了州聯賽冠軍，接下來的五年球會獲得不少獎項。[祖利亞](../Page/祖利亞.md "wikilink"),
[Paulo César Carpegiani](../Page/Paulo_César_Carpegiani.md "wikilink"),
[Adílio, Cláudio
Adão和](../Page/Adílio,_Cláudio_Adão.md "wikilink")[Tita等球星由](../Page/Tita.md "wikilink")[薛高帶領下](../Page/薛高.md "wikilink")，令球隊連續三年奪得州聯賽冠軍。而到了1980年，他們也首次贏了巴西甲組聯賽的冠軍，也令他們能參與[南美自由杯](../Page/南美自由杯.md "wikilink")。

1981年是值得紀念的一年，因為法林明高在擊敗[智利球隊](../Page/智利.md "wikilink")[科布雷洛後](../Page/科布雷洛.md "wikilink")，奪得[南美自由杯冠軍](../Page/南美自由杯.md "wikilink")。而他們的下一個目標是[洲際杯](../Page/洲際杯.md "wikilink")，這場在[日本舉行的賽事](../Page/日本.md "wikilink")，對手是[歐洲聯賽冠軍杯盟主](../Page/歐洲聯賽冠軍杯.md "wikilink")，[英格蘭的](../Page/英格蘭.md "wikilink")[利物浦](../Page/利物浦.md "wikilink")。

[Raul
Plassman](../Page/Raul_Plassman.md "wikilink"),[Leandro](../Page/Leandro.md "wikilink"),
[Marinho](../Page/Marinho.md "wikilink"), [Carlos
Mozer](../Page/Carlos_Mozer.md "wikilink"),
[祖利亞](../Page/祖利亞.md "wikilink"), [Jorge Luís Andrade da
Silva](../Page/Jorge_Luís_Andrade_da_Silva.md "wikilink"),
[Adílio](../Page/Adílio.md "wikilink"), [薛高](../Page/薛高.md "wikilink"),
[Tita](../Page/Tita.md "wikilink"),
[紐尼斯和](../Page/紐尼斯.md "wikilink")[Lico是](../Page/Lico.md "wikilink")1981年12月13日，比賽當日賽事的正選球員。Nunes的兩個入球，Adílio的一個入球，還有[薛高的精彩表現](../Page/薛高.md "wikilink")，令他們成為繼[聖保羅後第二支奪冠的巴西球隊](../Page/聖保羅足球會.md "wikilink")。

之後的兩年對法林明高來說也不錯，1981年他們再贏州聯賽冠軍，還在1982年和1983年奪得聯賽冠軍。

### 著名球員

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/阿祖安奴.md" title="wikilink">阿祖安奴</a></p></li>
<li><p><a href="../Page/Aldair.md" title="wikilink">Aldair</a></p></li>
<li><p><a href="../Page/Jorge_Luís_Andrade_da_Silva.md" title="wikilink">Andrade</a></p></li>
<li><p><a href="../Page/白必圖.md" title="wikilink">白必圖</a></p></li>
<li><p><a href="../Page/Jorge_Duílio_Benitez_Candia.md" title="wikilink">Jorge Benitez</a></p></li>
<li><p><a href="../Page/Biguá.md" title="wikilink">Biguá</a></p></li>
<li><p><a href="../Page/Carlinhos.md" title="wikilink">Carlinhos</a></p></li>
<li><p><a href="../Page/Paulo_César_Carpegiani.md" title="wikilink">Carpegiani</a></p></li>
<li><p><a href="../Page/Eusebio_Chamorro.md" title="wikilink">Eusebio Chamorro</a></p></li>
<li><p><a href="../Page/Cláudio_Adão.md" title="wikilink">Cláudio Adão</a></p></li>
<li><p><a href="../Page/Dequinha.md" title="wikilink">Dequinha</a></p></li>
<li><p><a href="../Page/迪達.md" title="wikilink">迪達</a></p></li>
<li><p><a href="../Page/Djalminha.md" title="wikilink">Djalminha</a></p></li>
<li><p><a href="../Page/Domingos_da_Guia.md" title="wikilink">Domingos da Guia</a></p></li>
<li><p><a href="../Page/Narciso_Horacio_Doval.md" title="wikilink">Horacio Doval</a></p></li>
<li><p><a href="../Page/艾迪臣.md" title="wikilink">艾迪臣</a></p></li>
<li><p><a href="../Page/Evaristo_de_Macedo.md" title="wikilink">Evaristo</a></p></li>
<li><p><a href="../Page/Felipe_Loureiro.md" title="wikilink">Felipe</a></p></li>
<li><p><a href="../Page/Ubaldo_Matildo_Fillol.md" title="wikilink">Ubaldo Fillol</a></p></li>
<li><p><a href="../Page/Fio_Maravilha.md" title="wikilink">Fio Maravilha</a></p></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><p><a href="../Page/Arthur_Friedenreich.md" title="wikilink">Friedenreich</a></p></li>
<li><p><a href="../Page/卡洛斯·加馬拉.md" title="wikilink">卡洛斯·加馬拉</a></p></li>
<li><p><a href="../Page/加林查.md" title="wikilink">加林查</a></p></li>
<li><p><a href="../Page/Gaúcho_(footballer).md" title="wikilink">Gaúcho</a></p></li>
<li><p><a href="../Page/蓋爾森.md" title="wikilink">蓋爾森</a></p></li>
<li><p><a href="../Page/Gilmar_Rinaldi.md" title="wikilink">Gilmar</a></p></li>
<li><p><a href="../Page/Henrique_Frade.md" title="wikilink">Henrique</a></p></li>
<li><p><a href="../Page/Aluísio_da_Luz_(Indio).md" title="wikilink">Índio</a></p></li>
<li><p><a href="../Page/Jair_Rosa_Pinto.md" title="wikilink">Jair</a></p></li>
<li><p><a href="../Page/Joel_Antonio_Martins.md" title="wikilink">Joel</a></p></li>
<li><p><a href="../Page/Jordan_da_Costa.md" title="wikilink">Jordan</a></p></li>
<li><p><a href="../Page/Jorginho.md" title="wikilink">Jorginho</a></p></li>
<li><p><a href="../Page/Juan_Silveira_dos_Santos.md" title="wikilink">Juan</a></p></li>
<li><p><a href="../Page/儒利奥·塞萨尔.md" title="wikilink">儒利奥·塞萨尔</a></p></li>
<li><p><a href="../Page/儒尼尼奧.md" title="wikilink">儒尼尼奧</a></p></li>
<li><p><a href="../Page/Júnior.md" title="wikilink">Júnior</a></p></li>
<li><p><a href="../Page/Júnior_Baiano.md" title="wikilink">Júnior Baiano</a></p></li>
<li><p><a href="../Page/Leandro.md" title="wikilink">Leandro</a></p></li>
<li><p><a href="../Page/莱昂纳多·纳西门托·德·阿劳若.md" title="wikilink">李安納度</a></p></li>
<li><p><a href="../Page/Leônidas_da_Silva.md" title="wikilink">Leônidas</a></p></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><p><a href="../Page/Marcelinho_Carioca.md" title="wikilink">馬些連奴</a></p></li>
<li><p><a href="../Page/Carlos_Mozer.md" title="wikilink">Mozer</a></p></li>
<li><p><a href="../Page/Nunes_(footballer).md" title="wikilink">紐尼斯</a></p></li>
<li><p><a href="../Page/Paulo_César_Lima.md" title="wikilink">Paulo César</a></p></li>
<li><p><a href="../Page/Dejan_Petkovic.md" title="wikilink">Dejan Petkovic</a></p></li>
<li><p><a href="../Page/Silvio_Pirilo.md" title="wikilink">Pirilo</a></p></li>
<li><p><a href="../Page/Raul_Plassman.md" title="wikilink">Raul</a></p></li>
<li><p><a href="../Page/連拿度.md" title="wikilink">連拿度</a></p></li>
<li><p><a href="../Page/Francisco_Santiago_Reyes_Villalba.md" title="wikilink">Francisco Reyes</a></p></li>
<li><p><a href="../Page/罗马里奥.md" title="wikilink">罗马里奥</a></p></li>
<li><p><a href="../Page/Sávio.md" title="wikilink">Sávio</a></p></li>
<li><p><a href="../Page/蘇古迪斯.md" title="wikilink">蘇古迪斯</a></p></li>
<li><p><a href="../Page/Tita.md" title="wikilink">Tita</a></p></li>
<li><p><a href="../Page/José_Armando_Ufarte_Ventoso.md" title="wikilink">Armando <em>"Espanhol"</em> Ufarte</a></p></li>
<li><p><a href="../Page/Agustín_Valido.md" title="wikilink">Agustín Valido</a></p></li>
<li><p><a href="../Page/Carlos_Martin_Volante.md" title="wikilink">Carlos Martin Volante</a></p></li>
<li><p><a href="../Page/Mário_Zagallo.md" title="wikilink">薩加奴</a></p></li>
<li><p><a href="../Page/济科.md" title="wikilink">济科</a></p></li>
<li><p><a href="../Page/Zinho.md" title="wikilink">Zinho</a></p></li>
<li><p><a href="../Page/Zizinho.md" title="wikilink">Zizinho</a></p></li>
<li><p><a href="../Page/朗拿甸奴.md" title="wikilink">朗拿甸奴</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

### 紀錄

<div align="left">

<table style="width:35%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 31%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><div

style="float:right;width:30px;">
<p> </p>
</div>
<p><font color="#000000">出場次數*</font></p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:Flamengo_braz_logo.svg" title="fig:Flamengo_braz_logo.svg">Flamengo_braz_logo.svg</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1. <a href="../Page/Júnior.md" title="wikilink">Júnior</a></p></td>
<td><p><strong>857</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2. <a href="../Page/Zico.md" title="wikilink">Zico</a></p></td>
<td><p><strong>731</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>3. <a href="../Page/Adílio.md" title="wikilink">Adílio</a></p></td>
<td><p><strong>611</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>4. <a href="../Page/Jordan_da_Costa.md" title="wikilink">Jordan</a></p></td>
<td><p><strong>589</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>5. <a href="../Page/Jorge_Luís_Andrade_da_Silva.md" title="wikilink">Andrade</a></p></td>
<td><p><strong>556</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>*截至2005年12月31日</small></p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>來源: <a href="http://www.flamengo.com.br">Flamengo Official Website</a></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

<div align="left">

<table style="width:35%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 31%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><div

style="float:right;width:30px;">
<p> </p>
</div>
<p><font color="#000000">入球*</font></p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:Flamengo_braz_logo.svg" title="fig:Flamengo_braz_logo.svg">Flamengo_braz_logo.svg</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1. <a href="../Page/Zico.md" title="wikilink">Zico</a></p></td>
<td><p><strong>508</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2. <a href="../Page/Edvaldo_Santa_Rosa_(Dida).md" title="wikilink">Dida</a></p></td>
<td><p><strong>244</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>3. <a href="../Page/Henrique_Frade.md" title="wikilink">Henrique</a></p></td>
<td><p><strong>214</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>4. <a href="../Page/Romário.md" title="wikilink">Romário</a></p></td>
<td><p><strong>204</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>5. <a href="../Page/Silvio_Pirilo.md" title="wikilink">Pirilo</a></p></td>
<td><p><strong>201</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>*截至2005年12月31日</small></p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><small>來源: <a href="http://www.flamengo.com.br">官方網站</a></small></p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

#### 巴西甲級聯賽紀錄

<center>

|                                     |                                     |                                     |                                     |                                     |                                     |                                     |                                     |
| ----------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------------- |
| <font color="#000000">**年度**</font> | <font color="#000000">**成績**</font> | <font color="#000000">**年度**</font> | <font color="#000000">**成績**</font> | <font color="#000000">**年度**</font> | <font color="#000000">**成績**</font> | <font color="#000000">**年度**</font> | <font color="#000000">**成績**</font> |
| **1971**                            | 第14名                                | **1981**                            | 第6名                                 | **1991**                            | 第9名                                 | **2001**                            | 第24名                                |
| **1972**                            | 第12名                                | **1982**                            | **第1名**                             | **1992**                            | **第1名**                             | **2002**                            | 第18名                                |
| **1973**                            | 第24名                                | **1983**                            | **第1名**                             | **1993**                            | 第7名                                 | **2003**                            | 第8名                                 |
| **1974**                            | 第6名                                 | **1984**                            | 第5名                                 | **1994**                            | 第17名                                | **2004**                            | 第17名                                |
| **1975**                            | 第8名                                 | **1985**                            | 第9名                                 | **1995**                            | 第21名                                | **2005**                            | 第15名                                |
| **1976**                            | 第5名                                 | **1986**                            | 第11名                                | **1996**                            | 第13名                                | **2006**                            |                                     |
| **1977**                            | 第9名                                 | **1987**                            | **第1名**                             | **1997**                            | 第5名                                 | **2007**                            |                                     |
| **1978**                            | 第16名                                | **1988**                            | 第6名                                 | **1998**                            | 第11名                                | **2008**                            |                                     |
| **1979**                            | 第12名                                | **1989**                            | 第9名                                 | **1999**                            | 第12名                                | **2009**                            |                                     |
| **1980**                            | **第1名**                             | **1990**                            | 第11名                                | **2000**                            | 第19名                                | **2010**                            |                                     |

</center>

#### 巴西杯紀錄

<center>

|                                     |                                     |                                     |                                     |
| ----------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------------- |
| <font color="#000000">**年份**</font> | <font color="#000000">**回合**</font> | <font color="#000000">**年份**</font> | <font color="#000000">**回合**</font> |
| **1989**                            | 準決賽                                 | **1999**                            | 半準決賽                                |
| **1990**                            | **冠軍**                              | **2000**                            | 半準決賽                                |
| **1991**                            | \-                                  | **2001**                            | 半準決賽                                |
| **1992**                            | \-                                  | **2002**                            | \-                                  |
| **1993**                            | 準決賽                                 | **2003**                            | 亞軍                                  |
| **1994**                            | \-                                  | **2004**                            | 亞軍                                  |
| **1995**                            | 準決賽                                 | **2005**                            | 十六強                                 |
| **1996**                            | 準決賽                                 | **2006**                            | **冠軍**                              |
| **1997**                            | 亞軍                                  | **2007**                            | \-                                  |
| **1998**                            | 十六強                                 | **2008**                            |                                     |

</center>

### 著名教練

  - [Modesto Bria](../Page/Modesto_Bria.md "wikilink")

  - [Carlinhos](../Page/Carlinhos.md "wikilink")

  - [Cláudio Coutinho](../Page/Cláudio_Coutinho.md "wikilink")

  - [Flávio Costa](../Page/Flávio_Costa.md "wikilink")

  - [Paulo César
    Carpegiani](../Page/Paulo_César_Carpegiani.md "wikilink")

  - [Manuel Fleitas Solich](../Page/Manuel_Fleitas_Solich.md "wikilink")

  - [Joel Santana](../Page/Joel_Santana.md "wikilink")

  - [Telê Santana](../Page/Telê_Santana.md "wikilink")

  - [盧森貝高](../Page/万德雷·卢森博格.md "wikilink")

  - [薩加奴](../Page/马里奥·扎加洛.md "wikilink")

### 球場

法林明高的主場位於[禾達列當達](../Page/禾達列當達.md "wikilink")，在1938年9月4日落成，能容納8,000人。不過，所有的比賽都在[馬拉簡拿進行](../Page/馬拉簡拿.md "wikilink")，那裡能容納103,022人，當中77,720個是座位。

## 趣聞

  - 法林明高的同市宿敵有：[富明尼斯](../Page/富明尼斯.md "wikilink")、[保地花高和](../Page/保地花高.md "wikilink")[華斯高](../Page/華斯高.md "wikilink")。
  - 1984年至2009年，球隊的贊助由[巴西石油股份提供](../Page/巴西石油股份.md "wikilink")，維持了25年，。

## 外部連結

  - [官方網站](http://www.flamengo.com.br/)
  - [Nação Rubro Negra - O site da maior torcida do
    mundo](http://www.nacaorubronegra.com)

[Category:巴西足球俱樂部](../Category/巴西足球俱樂部.md "wikilink")
[Category:1895年建立的足球俱樂部](../Category/1895年建立的足球俱樂部.md "wikilink")

1.  [巴西弗拉门戈足球俱乐部训练中心失火，10人丧生](https://www.jiemian.com/article/2852259.html)
2.  [巴西足球界等对弗拉门戈俱乐部火灾遇难者表示哀悼](http://sports.xinhuanet.com/c/2019-02/09/c_1124094161.htm)