**Snow
Tears**，是[中川翔子的第四张单曲](../Page/中川翔子.md "wikilink")，2008年1月30日发售，商品番号SRCL-6698（CD+DVD盘）。

## 作品概要

  - 中川本人亲自作词并参加了乐曲的制作。
  - 自上一张单曲大受好评之后，本次单曲发行了CD盘、CD+DVD盘、墓场鬼太郎·寝子盘三种不同的版本。
  - 发售首日，获得了[日本公信榜初登场排名](../Page/Oricon.md "wikilink")[日间第](../Page/白天.md "wikilink")1位、总排名第2位的成绩，是个人首张Oricon排名达到第1位的单曲。

## 收录曲

  - CD盘

<!-- end list -->

1.  **snow tears**
      -
        作詞：中川翔子，[yozuca\*](../Page/yozuca*.md "wikilink")
        作曲：[鈴木大輔](../Page/鈴木大輔.md "wikilink")
        編曲：[nishi-ken](../Page/nishi-ken.md "wikilink")
          - 是自第二张单曲以来，本人参与作词的歌曲。
2.  ****
      -
        作詞・作曲：[meg rock](../Page/meg_rock.md "wikilink") 編曲：nishi-ken
3.  **Winter Wish**
      -
        作詞：[mavie](../Page/mavie.md "wikilink") 作曲：鈴木大輔
        編曲：[加藤大祐](../Page/加藤大祐.md "wikilink")
4.  snow tears（[器乐](../Page/器乐.md "wikilink")）

<!-- end list -->

  - CD+DVD盘

<!-- end list -->

  - Disc-1：CD（与上记CD盘相同）
  - Disc-2：DVD

<!-- end list -->

  - 墓场[鬼太郎](../Page/鬼太郎.md "wikilink")·寝子盘

<!-- end list -->

1.  snow tears

2.
3.  Winter Wish

4.  snow tears（动画版TV Size）

## 版本

**CD+DVD盘**

  - 「墓场[鬼太郎](../Page/鬼太郎.md "wikilink")」（封面A）

**CD盘**

  - 「墓场鬼太郎」（封面B）

**墓场鬼太郎·寝子盘**

  - 以「寝子」为主角的封面插画。

## 说明

**snow tears**

  - [电视](../Page/电视.md "wikilink")[动画](../Page/动画.md "wikilink")[墓场鬼太郎片尾曲](../Page/墓场鬼太郎.md "wikilink")（ED）。
  - [music.jp电视广告歌](../Page/music.jp.md "wikilink")。

****

  - 墓场鬼太郎插曲。

## 相关项目

  - [中川翔子](../Page/中川翔子.md "wikilink")
  - [墓场鬼太郎](../Page/墓场鬼太郎.md "wikilink")

[Category:中川翔子歌曲](../Category/中川翔子歌曲.md "wikilink")
[Category:2008年单曲](../Category/2008年单曲.md "wikilink")
[Category:日本配音員單曲](../Category/日本配音員單曲.md "wikilink")
[歌](../Category/鬼太郎.md "wikilink")
[Category:雪題材樂曲](../Category/雪題材樂曲.md "wikilink")
[Category:富士電視台動畫主題曲](../Category/富士電視台動畫主題曲.md "wikilink")
[Category:noitaminA主題曲](../Category/noitaminA主題曲.md "wikilink")