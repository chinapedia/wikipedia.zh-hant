[LhasaBarskor.jpg](https://zh.wikipedia.org/wiki/File:LhasaBarskor.jpg "fig:LhasaBarskor.jpg")
**八廓**（），是[拉萨旧城的商业中心](../Page/拉萨.md "wikilink")、最繁忙的街道。八廓环形路是由八廓东街、八廓南街、八廓西街、八廓北街组成。八廓街的周长约为1000余米，距今已有1300多年历史，较好的保留了拉萨古城的原始风貌。\[1\]

八廓是[拉萨最古老的街道](../Page/拉萨.md "wikilink")，古代[松赞干布和](../Page/松赞干布.md "wikilink")[文成公主率部迁徙到拉萨后](../Page/文成公主.md "wikilink")，首先建造了[大昭寺](../Page/大昭寺.md "wikilink")，[藏传佛教的信徒们就开始围绕大昭寺](../Page/藏传佛教.md "wikilink")[转经](../Page/转经.md "wikilink")，逐渐形成一条道路，成为拉萨三条转经道的**中转经道**（另两条为环绕包括[布达拉宫在内的拉萨老城的外转经道](../Page/布达拉宫.md "wikilink")“”和大昭寺内的内转经道“[囊廓](../Page/囊廓.md "wikilink")”），[藏族人称八廓为](../Page/藏族.md "wikilink")“圣路”，[藏语](../Page/藏语.md "wikilink")“廓”的意思即为“转经道”。八廓是一条[单行道](../Page/单行道.md "wikilink")，在八廓上行走，必须和转经筒的旋转方向一致，即顺时针行走（右绕）。街上也有许多信徒在一步一[长跪地转经](../Page/长跪.md "wikilink")。

## 八廓与八角街

八角街是汉语的音误。

在拉萨的外地人以四川人为多。在四川方言中，“廓”与“角”的发音近似，很多人误将“八廓街”听为“**八角街**”。加之，八廓街是环形道路，更错误认为“八角”是指环形道路是八角形（实际为六角形）。渐渐地，把八廓误读成“八角街”的人越来越多。\[2\]\[3\]

以讹传讹，正确称呼“八廓”反而不如“八角街”为人所知。

## 参考资料

<references/>

</div>

[Category:拉萨](../Category/拉萨.md "wikilink")
[Category:西藏旅游](../Category/西藏旅游.md "wikilink")
[Category:藏传佛教](../Category/藏传佛教.md "wikilink")
[Category:中国历史文化名街](../Category/中国历史文化名街.md "wikilink")

1.  <https://www.greattibettour.com/tibet-attractions/barkhor-street-356>
2.
3.