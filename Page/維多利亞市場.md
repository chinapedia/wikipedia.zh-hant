[Queen_Victoria_Market_201708.jpg](https://zh.wikipedia.org/wiki/File:Queen_Victoria_Market_201708.jpg "fig:Queen_Victoria_Market_201708.jpg")
[Queen_Victoria_Market_View_201708.jpg](https://zh.wikipedia.org/wiki/File:Queen_Victoria_Market_View_201708.jpg "fig:Queen_Victoria_Market_View_201708.jpg")
**維多利亞市場**（**Queen Victoria
Market**），簡稱**QVM**，位于[澳洲的](../Page/澳洲.md "wikilink")[墨爾本市Victoria](../Page/墨爾本市.md "wikilink")
St.及Elizabeth
St.交界，佔地約7公頃，為[南半球最大規模的露天](../Page/南半球.md "wikilink")[市場](../Page/市場.md "wikilink")，全球第三大規模的露天市場，亦是世界上著名[旅遊景點之一](../Page/旅遊.md "wikilink")。\[1\]市場由1878年起開始運作至今，是唯一現存在[墨爾本市中心內的](../Page/墨爾本.md "wikilink")19世紀市場，主要提供[海鮮](../Page/海鮮.md "wikilink")、[肉類](../Page/肉類.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、[生果等新鮮食品](../Page/生果.md "wikilink")，並且設有雜貨區域，販賣各種廉價的[衣服](../Page/衣服.md "wikilink")[鞋](../Page/鞋.md "wikilink")[襪及相關旅遊](../Page/襪.md "wikilink")[手信](../Page/手信.md "wikilink")。市場逢星期一及星期三暫停營業，而在澳洲[夏天期間則會舉辦夜市活動](../Page/夏天.md "wikilink")。2017年6月至9月27日，每個星期三晚上設冬季限定夜市。\[2\]

<File:Queen> Victoria Market General Merchandise 201708.jpg|雜貨區域
<File:Queen> Victoria Market Daily Product Hall 201708.jpg|奶類製品
<File:Queen> Victoria Market Meat & Fish Hall 201708.jpg|海鮮檔
<File:Queen> Victoria Market Food Court 201708.jpg|美食廣場

## 參考資料

[Category:墨爾本](../Category/墨爾本.md "wikilink")
[Category:澳大利亞旅遊](../Category/澳大利亞旅遊.md "wikilink")

1.
2.