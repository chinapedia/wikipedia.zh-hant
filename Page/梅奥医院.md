[MayoClinic\&MedSchool2006-05-07.JPG](https://zh.wikipedia.org/wiki/File:MayoClinic&MedSchool2006-05-07.JPG "fig:MayoClinic&MedSchool2006-05-07.JPG")
**梅奥诊所醫學中心**（），又譯為**梅奥诊所**、**梅約診所**、**-{A|zh-hans:马约诊所;zh-hant:馬約診所}-**，是世界最著名的医疗机构之一，位于[美国](../Page/美国.md "wikilink")[明尼苏达州](../Page/明尼苏达州.md "wikilink")[罗彻斯特](../Page/罗彻斯特_\(明尼苏达州\).md "wikilink")（）。它还有医院设在[佛罗里达州的](../Page/佛罗里达州.md "wikilink")[杰克逊维尔](../Page/杰克逊维尔_\(佛罗里达州\).md "wikilink")（）及[亚利桑那州的](../Page/亚利桑那州.md "wikilink")[斯科茨代尔](../Page/斯科茨代尔_\(美国\).md "wikilink")（）。在明尼苏达州、[爱荷华州](../Page/爱荷华州.md "wikilink")、[威斯康辛州还有一些小的](../Page/威斯康辛州.md "wikilink")[诊所和](../Page/诊所.md "wikilink")[医院](../Page/医院.md "wikilink")。2014年7月15日，《[美国新闻与世界报道](../Page/美国新闻与世界报道.md "wikilink")》（）最新发布了2014年\~15年美国最佳医院排行榜。排名显示，位于罗彻斯特的梅奥诊所首次荣登榜首\[1\]。

梅奥诊所（Mayo
Clinic）2017年綜合財務報告顯示，其2017年營業利潤達7.07億美元，營收120億美元。相比2016年，營業利潤增長了超過2.25億美元，主營業務收入增長了10億美元。營業利潤率也從2016年的4.3％增長至2017年的5.9%

## 梅奥诊所

在梅奥诊所名字里的“诊所”，通常指一个单一的小型门诊设施，梅奥诊所就是这样开始起家的。但现在梅奥诊所是一个全面的医疗保健系统，包括门诊、医院、医学研究及医学教育机构。梅奥诊所是明尼苏达的第二大的非营利性组织，2004年梅奥诊所营业额56亿美元、有51万新病人、总门诊量2百万、13万人次入院、近60万人住院。

梅奥诊所支付医师[酬薪的方式特殊](../Page/酬薪.md "wikilink")。在美国的大多数医疗体系，医生酬薪取决于其所看病人的数量。梅奥诊所的医师酬薪不取决于病人的数量，酬薪是参照可比的大型医疗集团的医师的市场薪酬。

## 历史

[SMHmaindoor.jpg](https://zh.wikipedia.org/wiki/File:SMHmaindoor.jpg "fig:SMHmaindoor.jpg")

  - 1846年，威廉·梅奥从[英国移民到美国](../Page/英国.md "wikilink")。
  - 1850年，威廉·梅奥开始行医。
  - 1883年，威廉·梅奥的儿子，威廉·詹姆斯·梅奥（1861-1939）毕业于美国[密西根大学医学院](../Page/密西根大学.md "wikilink")，加入其父亲的诊所
  - 1888年，威廉·梅奥的儿子，查尔斯·霍勒斯·梅奥（1865-1939）毕业于美国[西北大学的芝加哥医学院](../Page/西北大学.md "wikilink")，加入其父亲的诊所。
  - 1883年，5级龙卷风袭击罗彻斯特，造成大量人员伤亡，威廉梅奥治疗幸存者。
  - 1889年，与圣弗朗西斯修女院合作建立了27张病床的圣玛丽医院。今天，该医院有1157张病床。
  - 1901年，Henry Stanley Plummer加入威廉·梅奥。
  - 后来，梅奥帮助[明尼苏达大学建立了医学院](../Page/明尼苏达大学.md "wikilink")。
  - 1970年代后期梅奥诊所开始有了自己的梅奥医学院。

## 著名病人

[PlummerBldg.JPG](https://zh.wikipedia.org/wiki/File:PlummerBldg.JPG "fig:PlummerBldg.JPG")

  - 美国前总统，[约翰·肯尼迪](../Page/约翰·肯尼迪.md "wikilink")
  - 美国前总统，[杰拉尔德·福特](../Page/杰拉尔德·福特.md "wikilink")
  - 美国前总统，[罗纳德·威尔逊·里根](../Page/罗纳德·威尔逊·里根.md "wikilink")
  - 美国前总统，[乔治·赫伯特·沃克·布什](../Page/乔治·赫伯特·沃克·布什.md "wikilink")
  - [约旦国王](../Page/约旦.md "wikilink")，[侯赛因·宾·塔拉勒](../Page/侯赛因·宾·塔拉勒.md "wikilink")
  - [阿拉伯联合酋长国前总统](../Page/阿拉伯联合酋长国.md "wikilink")，[扎耶德·本·苏尔坦·阿勒纳哈扬](../Page/扎耶德·本·苏尔坦·阿勒纳哈扬.md "wikilink")
  - 美国20世纪[小说家](../Page/小说家.md "wikilink")，[欧内斯特·海明威](../Page/欧内斯特·海明威.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [梅奥诊所中文官方網站](https://mandarin.mayoclinic.org)
  - [梅奥诊所](http://www.mayoclinic.org/)
  - [梅奥诊所医学院](http://www.mayo.edu/)

[Category:美国医院](../Category/美国医院.md "wikilink")
[Category:杰克逊维尔](../Category/杰克逊维尔.md "wikilink")
[Category:1864年建立](../Category/1864年建立.md "wikilink")

1.  <http://www.usnews.com/usnews/health/best-hospitals/honorroll.htm>