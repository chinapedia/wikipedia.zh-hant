[HK_Des_Vouex_Road_REVLON_Tram.jpg](https://zh.wikipedia.org/wiki/File:HK_Des_Vouex_Road_REVLON_Tram.jpg "fig:HK_Des_Vouex_Road_REVLON_Tram.jpg")
**露華濃公司**（[英文](../Page/英文.md "wikilink")：Revlon
Incorporated，簡稱：露華濃，英文：Revlon）
是[美國一間](../Page/美國.md "wikilink")[化妝業公司](../Page/化妝.md "wikilink")，1962年進軍大中華市場。1996年，Revlon进入中国中文名字“露华浓”，出自[李白的](../Page/李白.md "wikilink")[清平調名句](../Page/清平調.md "wikilink")“云想衣裳花想容，春风拂槛露华浓”。

## 具爭議事件

### 染髮劑驗出致癌物

由[中國](../Page/中國.md "wikilink")[廣州市漢邦化妝品公司所生產的](../Page/廣州市.md "wikilink")「露華濃麗然染髮劑」，遭中國[廣東省消費者委員會驗出含有禁用的致癌化學物質](../Page/廣東省.md "wikilink")[間苯二胺](../Page/二氨基苯.md "wikilink")\[1\]。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [露華濃](http://www.revlon.com/)

  -
  -
  -
  -
[Category:1932年成立的公司](../Category/1932年成立的公司.md "wikilink")
[Category:美国化妆品公司](../Category/美国化妆品公司.md "wikilink")

1.