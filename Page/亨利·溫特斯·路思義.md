[Henry_Winters_Luce.jpg](https://zh.wikipedia.org/wiki/File:Henry_Winters_Luce.jpg "fig:Henry_Winters_Luce.jpg")
**路思义**（，），是一位在19世纪末、20世纪初服务于[中國华北](../Page/中國.md "wikilink")[齐鲁大学与](../Page/齐鲁大学.md "wikilink")[燕京大学两校的](../Page/燕京大学.md "wikilink")[美国](../Page/美国.md "wikilink")[传教士](../Page/传教士.md "wikilink")。\[1\]

## 生平

### 早年

路思义生长在美國[麻萨诸塞州的](../Page/麻萨诸塞州.md "wikilink")[斯克兰顿](../Page/斯克兰顿.md "wikilink")（Scranton），父母亲都是敬虔的基督徒，经营一家颇具规模的杂货店。1888年进入[耶鲁大学就读](../Page/耶鲁大学.md "wikilink")，恰逢[学生志愿海外宣道运动](../Page/学生志愿海外宣道运动.md "wikilink")（The
Student Volunteer Movement for Foreign
Missions）的潮流席卷美国各大学校园，八千多名大学毕业生因此远赴海外宣道，而其中最重要的目的地就是中国。

### 傳教

当时毕特金（Horace T.
Pitkin，1900年在中国[保定被杀](../Page/保定.md "wikilink")）、艾迪（Sherwood
Eddy）和路思义就是加入这一行列中著名的「耶鲁三杰」。他们在1892年毕业后，一起进入[纽约协和神学院以及](../Page/纽约协和神学院.md "wikilink")[普林斯敦大學学习](../Page/普林斯敦大學.md "wikilink")。1896年，艾迪和华特金分别动身前往印度与中国，路思义则是在1897年9月才带看新婚三个月的妻子伊丽莎白受[美北长老会的派遣乘船横越](../Page/美北长老会.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")，经[上海前往](../Page/上海.md "wikilink")[山东](../Page/山东.md "wikilink")[登州](../Page/登州.md "wikilink")（蓬莱），在[狄考文](../Page/狄考文.md "wikilink")（Calvin
Mateer）创办的[文会馆中担任物理教师](../Page/文会馆.md "wikilink")。在那里路思义发起了山东省历史上第一场[篮球比赛](../Page/篮球.md "wikilink")。

### 辦學

1900年的[庚子之乱中](../Page/义和团运动.md "wikilink")，山东省的传教士得到[北洋海军的统领](../Page/北洋海军.md "wikilink")[萨镇冰的帮助](../Page/萨镇冰.md "wikilink")，经[烟台转赴](../Page/烟台.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")[汉城避乱](../Page/汉城.md "wikilink")。但路思义的同事们没有失去信心，在废墟上重建教会。这时，年轻的路思义推动山东长老会作出重大决定：将文会馆迁至山东腹地的潍县（今[潍坊市](../Page/潍坊.md "wikilink")），并联合英国[浸礼会](../Page/浸礼会.md "wikilink")（在[青州办有广德书院](../Page/青州.md "wikilink")，Gotch--Robinson
Training
Institute）的力量，提高学校的级别，增加高等课程，最终在省会济南创办一所象样的正规大学。这些建议后来逐步一一得以成为现实。

1904年，文会馆搬到潍县东郊[乐道院的新校园](../Page/乐道院.md "wikilink")，取名[广文大学](../Page/广文大学.md "wikilink")（Shantung
Union College），青州广德书院改为神道学院（Union Theological
College）。1906年，路思义受校长[柏尔根](../Page/柏尔根.md "wikilink")(Paul
D. Bergen)之派回美国募捐。

1909年3月，路思义邀请文会馆校友[丁立美牧师来校主领奋兴聚会](../Page/丁立美.md "wikilink")，有116人决志献身传道，占全校学生的三分之一以上，开始了著名的[中华学生立志布道团](../Page/中华学生立志布道团.md "wikilink")。

1917年，广文与青州神道学院、济南共合医道学堂合并，命名为[齐鲁大学](../Page/齐鲁大学.md "wikilink")，在[济南南关建校](../Page/济南.md "wikilink")。路思义被推选为副校长；为此他又回到美国两年多，四处奔走募集建校经费。

1919年，路思义又担任[燕京大学副校长](../Page/燕京大学.md "wikilink")，受校长[司徒雷登](../Page/司徒雷登.md "wikilink")（John
Leighton
Stuart）之託，赴美募得巨款，终于建成燕京大学（现在是[北京大学的一部分](../Page/北京大学.md "wikilink")）美丽的中国宫殿式校园。校园中未名湖心岛上建有一个八角形的思义亭，以纪念他对燕大的贡献。

### 晚年

[The_Luce_Chapel_2.jpg](https://zh.wikipedia.org/wiki/File:The_Luce_Chapel_2.jpg "fig:The_Luce_Chapel_2.jpg")

1927年秋天，路思义从燕大退休，回到美国，研究中国的历史、宗教与文化，并继续负责为燕京大学、齐鲁大学募集教育经费。

1941年12月7日，[珍珠港事变后的几个小时](../Page/珍珠港事变.md "wikilink")，他在美国去世。

在[台湾](../Page/台湾.md "wikilink")[台中市](../Page/台中市.md "wikilink")[东海大学校园中](../Page/東海大學_\(台灣\).md "wikilink")，建有纪念他的[路思义教堂](../Page/路思义教堂.md "wikilink")。\[2\]

## 家庭

长子[亨利·路思义](../Page/亨利·路思义.md "wikilink")（Henry R.
Luce），1898年4月生于山东蓬莱，后来创办了《[时代周刊](../Page/时代周刊.md "wikilink")》（Time）、《[财富](../Page/财富_\(杂志\).md "wikilink")》（Fortune）与《[生活](../Page/生活_\(杂志\).md "wikilink")》（Life）三大杂志。

## 參考資料

[L](../Category/美国教育家.md "wikilink")
[L](../Category/美北长老会.md "wikilink")
[L](../Category/在华基督教传教士.md "wikilink")
[L](../Category/耶魯大學校友.md "wikilink")
[L](../Category/普林斯頓大學校友.md "wikilink")

1.
2.