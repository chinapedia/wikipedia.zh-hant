**顾长卫**（），中国[电影](../Page/中国电影.md "wikilink")[摄影师及](../Page/摄影师.md "wikilink")[導演](../Page/導演.md "wikilink")。[奥斯卡奖评委之一](../Page/奥斯卡奖.md "wikilink")。

## 生平

祖籍[江苏](../Page/江苏.md "wikilink")[吴江](../Page/吴江.md "wikilink")，出生於[陕西](../Page/陕西.md "wikilink")[西安](../Page/西安.md "wikilink")。1978年考入[北京电影学院摄影系](../Page/北京电影学院.md "wikilink")。曾以[陳凱歌執導的](../Page/陳凱歌.md "wikilink")[霸王別姬入圍](../Page/霸王別姬_\(電影\).md "wikilink")[美国](../Page/美国.md "wikilink")[奥斯卡最佳摄影奖](../Page/奥斯卡最佳摄影奖.md "wikilink")，以及“[世纪百位杰出摄影师](../Page/世纪百位杰出摄影师.md "wikilink")”等荣誉。

2000年帮助[陈冲拍摄](../Page/陳冲_\(演員\).md "wikilink")《纽约的秋天》。[IMDB](http://www.imdb.com/name/nm0345146/)

2004年拍摄了自己执导的首部电影《[孔雀](../Page/孔雀_\(电影\).md "wikilink")》，凭借本片获得了[柏林电影节](../Page/柏林电影节.md "wikilink")“评审团大奖银熊奖”。它描述了20世纪70年代到80年代，一个[河南](../Page/河南省.md "wikilink")[安阳的普通家庭的生活](../Page/安阳.md "wikilink")，获得了观众的强烈反响。

2007年顧長衛執導的第二部影片《[立春](../Page/立春_\(电影\).md "wikilink")》入圍[羅馬電影節競賽單元](../Page/羅馬電影節.md "wikilink")，女主角[蔣雯麗憑藉劇中出色的演出獲得了最佳女演員獎](../Page/蔣雯麗.md "wikilink")。

## 家庭

1993年，與演員[蔣雯麗結婚](../Page/蔣雯麗.md "wikilink")。

## 作品

### 摄影

  - 1987年：《[孩子王](../Page/孩子王_\(電影\).md "wikilink")》
    摄影，（导演：[陈凯歌](../Page/陈凯歌.md "wikilink")）
  - 1987年：《[红高粱](../Page/红高粱.md "wikilink")》
    摄影，（导演：[张艺谋](../Page/张艺谋.md "wikilink")）
  - 1990年：《[边走边唱](../Page/边走边唱.md "wikilink")》 摄影，（导演：陈凯歌）
  - 1992年：《[菊豆](../Page/菊豆.md "wikilink")》 摄影，（导演：张艺谋）
  - 1993年：《[霸王别姬](../Page/霸王别姬.md "wikilink")》 摄影，（导演：陈凯歌）
  - 1994年：《[阳光灿烂的日子](../Page/阳光灿烂的日子.md "wikilink")》
    摄影，（导演：[姜文](../Page/姜文.md "wikilink")）
  - 2000年：《[鬼子来了](../Page/鬼子来了.md "wikilink")》 摄影，（导演：姜文）

### 导演

  - 2004年：《[孔雀](../Page/孔雀_\(电影\).md "wikilink")》，获得第六届[华语电影传媒大奖最佳导演](../Page/华语电影传媒大奖.md "wikilink")
  - 2007年：《[立春](../Page/立春_\(电影\).md "wikilink")》
  - 2011年：《[最爱](../Page/最爱_\(2011年电影\).md "wikilink")》，获得第20届[上海影评人奖最佳导演](../Page/上海影评人奖.md "wikilink")、第19届[北京大学生电影节最佳导演](../Page/北京大学生电影节.md "wikilink")\[1\]
  - 2014年：《[微爱之渐入佳境](../Page/微爱之渐入佳境.md "wikilink")》\[2\]
  - 2018年：《[遇见你真好](../Page/飞火流星_\(电影\).md "wikilink")》\[3\]

## 车震门

2009年初，顾长卫被媒体爆出[车震门丑闻](../Page/车震门.md "wikilink")。當日，记者拍到他先驾车至[北京](../Page/北京.md "wikilink")[昌平区的一个温泉度假村与妻子](../Page/昌平区.md "wikilink")[蒋雯丽碰面](../Page/蒋雯丽.md "wikilink")，随后驾车到蓟门桥的[北京电影学院附近](../Page/北京电影学院.md "wikilink")，与一陌生女子在车内独处。\[4\]

## 奥斯卡评委

截止到2012年，已有九位华人奥斯卡评委，包括：[卢燕](../Page/卢燕.md "wikilink")、[陈冲](../Page/陈冲.md "wikilink")、[邬君梅](../Page/邬君梅.md "wikilink")、[陈凯歌](../Page/陈凯歌.md "wikilink")、[顾长卫](../Page/顾长卫.md "wikilink")、[李安](../Page/李安.md "wikilink")、[吴宇森](../Page/吴宇森.md "wikilink")、[李连杰](../Page/李连杰.md "wikilink")、[章子怡](../Page/章子怡.md "wikilink")。\[5\]

## 参考资料

## 外部链接

  -
  -
  - [Gu Changwei](http://www.cinematographers.nl/PaginasDoPh/gu.htm) at
    the Internet Encyclopedia of Cinematographers

{{-}}

[Category:中国电影摄影师](../Category/中国电影摄影师.md "wikilink")
[Category:陕西电影导演](../Category/陕西电影导演.md "wikilink")
[Category:北京电影学院校友](../Category/北京电影学院校友.md "wikilink")
[Category:西安人](../Category/西安人.md "wikilink")
[Category:吴江人](../Category/吴江人.md "wikilink")
[C长](../Category/顧姓.md "wikilink")
[Category:金馬獎最佳攝影獲得者](../Category/金馬獎最佳攝影獲得者.md "wikilink")
[Category:北京大學生電影節最佳導演得主](../Category/北京大學生電影節最佳導演得主.md "wikilink")
[Category:华语电影传媒大奖最佳导演得主](../Category/华语电影传媒大奖最佳导演得主.md "wikilink")

1.
2.  [顾长卫：曾沉溺于夜店被叫“两会”代表](http://ent.163.com/14/1224/00/AE6K0ROS00034R76.html)
3.
4.
5.