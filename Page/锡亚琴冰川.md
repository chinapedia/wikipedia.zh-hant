[Siachen_Glacier_in_Pakistan_(hatched)_(claims_hatched).svg](https://zh.wikipedia.org/wiki/File:Siachen_Glacier_in_Pakistan_\(hatched\)_\(claims_hatched\).svg "fig:Siachen_Glacier_in_Pakistan_(hatched)_(claims_hatched).svg")
[SiachenGlacier_satellite.jpg](https://zh.wikipedia.org/wiki/File:SiachenGlacier_satellite.jpg "fig:SiachenGlacier_satellite.jpg")
**锡亚琴冰川**（；；），位于[喀喇昆仑山脉南端的](../Page/喀喇昆仑山脉.md "wikilink")[巴控克什米尔](../Page/北部地区_\(巴控克什米尔\).md "wikilink")、[印控克什米尔和](../Page/查谟-克什米尔邦.md "wikilink")[中国](../Page/中国.md "wikilink")[新疆之间的一處](../Page/新疆维吾尔自治区.md "wikilink")[冰川](../Page/冰川.md "wikilink")，高度从印控一侧的海拔5,375米延伸至中国境内的3,620米。

## 地理

这一地区也是[亞歐大陸板塊与](../Page/亞歐大陸板塊.md "wikilink")[印度次大陆北部的](../Page/印度次大陆.md "wikilink")[分水岭](../Page/分水岭.md "wikilink")，在[喀喇昆仑山广泛的冰川部分有时也被称为](../Page/喀喇昆仑山.md "wikilink")“第三极”。[冰川长约](../Page/冰川.md "wikilink")72公里，面积约3000余平方公里，平均气温在摄氏零下30-50度之间\[1\]。地形上呈南北走向，西面为[萨尔托洛山脉](../Page/萨尔托洛山脉.md "wikilink")（Saltoro
Mountains），东面为喀喇昆仑山主脉。萨尔托洛山脉北端延伸至中国境内与喀喇昆仑山[锡亚康戈里峰](../Page/锡亚康戈里峰.md "wikilink")（Sia
Kangri
peak）相连。萨尔托洛山脉的山峰高度处在5,450至7,720米之间，自北向南山口高度5,589米到5,450米；这一地区冬季平均降雪量10.5米，气温最低达零下50摄氏度。

锡亚琴冰川地区为印巴冲突地区，自1947年[印巴克什米尔争端爆发后](../Page/克什米尔战争.md "wikilink")，这里始终是世界上海拔最高的对峙阵地，双方均在这里设有重兵，并多次发生血战。在1984年後，該地區由印度控制。

## 参考文献

{{-}}

[Category:喀喇昆仑山脉](../Category/喀喇昆仑山脉.md "wikilink")
[Category:印度冰川](../Category/印度冰川.md "wikilink")
[Category:克什米尔](../Category/克什米尔.md "wikilink")

1.  [搜狐网](http://news.sohu.com/20050623/n226050113.shtml)，2005年06月23日，《印巴海拔最高战场对峙，大量士兵出师未捷已冻死》