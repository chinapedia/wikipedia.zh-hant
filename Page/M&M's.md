[M\&M's_Plain.jpg](https://zh.wikipedia.org/wiki/File:M&M's_Plain.jpg "fig:M&M's_Plain.jpg")
[M\&M's_Peanuts.JPG](https://zh.wikipedia.org/wiki/File:M&M's_Peanuts.JPG "fig:M&M's_Peanuts.JPG")

**M\&M's**（俗稱：M豆）是美國的牛奶[巧克力品牌](../Page/巧克力.md "wikilink")，由[玛氏食品公司所生產](../Page/玛氏食品.md "wikilink")。

## 顏色

M\&M's原來只有褐色。1960年增加了紅、黃和綠色。由於對人工合成色素[莧菜紅](../Page/莧菜紅.md "wikilink")
(Amaranth, FD\&C Red \#2)的可能致癌的健康顧慮，紅色在1976年被取消
，橘色被增加以取代紅色；儘管M\&M's並沒有使用這種色素，但紅色糖衣的糖果還是被取代，以移除消費者的疑慮。1987年，大眾已忘記對紅色色素的恐慌，因此紅色被恢復。1995年，由消費者投票增加了藍色。

目前M\&M's有6種顏色：紅、橘、黃、綠、褐與藍色（花生核仁的糖果有相同的顏色）。少數其他顏色只在特定節日發行。在[美國](../Page/美國.md "wikilink")，包括：

  - 黑色在[萬聖節販售](../Page/萬聖節.md "wikilink")。
  - 棕色在[感恩節前](../Page/感恩節.md "wikilink")1至2個月販售。
  - 粉紅色在[情人節販售](../Page/情人節.md "wikilink")。
  - 粉色糖果（粉紅色、粉綠色、粉藍色）在[復活節販售](../Page/復活節.md "wikilink")。
  - 白色偶爾在[美國獨立紀念日或特定促銷活動販售](../Page/美國獨立紀念日.md "wikilink")。

## 三聚氰胺污染

2008年，M\&M's的产品中曾經检测出[三聚氰胺](../Page/三聚氰胺.md "wikilink")\[1\]，但其後未有傳媒繼續跟進事件。

## 款式

  - 牛奶朱古力。（棕色包裝）

<!-- end list -->

  -
    1941年自公司開業開始發售。

<!-- end list -->

  - [花生朱古力](../Page/花生.md "wikilink")（黃色包裝）

<!-- end list -->

  -
    牛奶朱古力包著一粒花生，1954年開始發售。

<!-- end list -->

  - [花生醬朱古力](../Page/花生醬.md "wikilink")（橙色包裝）

<!-- end list -->

  -
    開售年份不明

<!-- end list -->

  - 黑朱古力

<!-- end list -->

  -
    2006年開始發售。

<!-- end list -->

  - 花生黑朱古力

<!-- end list -->

  -
    2007年開始發售。

<!-- end list -->

  - [紅莓味朱古力](../Page/紅莓.md "wikilink")

<!-- end list -->

  -
    2007年開始發售。

<!-- end list -->

  - [杏仁味朱古力](../Page/杏仁.md "wikilink")

<!-- end list -->

  -
    1988年開始發售。

<!-- end list -->

  - [杏仁味花生朱古力](../Page/杏仁.md "wikilink")

<!-- end list -->

  -
    1990年開始發售。

<!-- end list -->

  - 迷你M\&M's

<!-- end list -->

  -
    也是牛奶朱古力，但每粒朱古力的體積比正常的小。

<!-- end list -->

  - 脆脆心朱古力（藍色包裝）
  - 薄荷脆心朱古力（綠色包裝）

<File:(M&M's>) six colors.JPG <File:(Red)> M\&M's.JPG <File:(M&M's>),
Blue.JPG <File:(Green)> M\&M's.JPG

## 参考资料

## 外部連結

  -
[Category:巧克力糖](../Category/巧克力糖.md "wikilink")
[Category:1941年面世的產品](../Category/1941年面世的產品.md "wikilink")

1.   M\&M’s牛奶巧克力成都撤柜 四川在线 2008-10-9 09:53