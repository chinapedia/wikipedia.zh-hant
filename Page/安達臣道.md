**安達臣道**（舊譯**晏打臣道**；）是[香港東部的一條道路](../Page/香港.md "wikilink")，座落於[大上托](../Page/大上托.md "wikilink")[新界及](../Page/新界.md "wikilink")[九龍交界線上面](../Page/九龍.md "wikilink")，亦是[觀塘區與](../Page/觀塘區.md "wikilink")[西貢區的分界線](../Page/西貢區.md "wikilink")\[1\]。安達臣道本連接[清水灣道和](../Page/清水灣道.md "wikilink")[寶琳路](../Page/寶琳路.md "wikilink")，惟現功能由安秀道取代。

安達臣道由[茶寮坳對上](../Page/茶寮坳.md "wikilink")、[清水灣道與](../Page/清水灣道.md "wikilink")[新清水灣道交接之處開始](../Page/新清水灣道.md "wikilink")，向東南伸延至蕉欄樹，然後於[秀茂坪對上位置向東走](../Page/秀茂坪.md "wikilink")，最後於[馬游塘連接](../Page/馬游塘.md "wikilink")[寶琳路](../Page/寶琳路.md "wikilink")。整條道路建於[大上托](../Page/大上托.md "wikilink")[安達臣道石礦場西南面山腰](../Page/安達臣道石礦場.md "wikilink")。2014年3月，安達臣道被分成兩段，北端連接[清水灣道及安達臣道](../Page/清水灣道.md "wikilink")9號，南端則由安達臣道9號後面開始，以寶琳路作終點。分成兩段後，北端的安達臣道為一條掘頭路，南端的安達臣道連接新的安秀道，作為來往清水灣及秀茂坪的通道。2016年1月，北端的[掘頭路正式封閉](../Page/掘頭路.md "wikilink")。

自2018年起，隨著位於安達臣道上方的[安達臣道石礦場發展計劃開展](../Page/安達臣道石礦場發展計劃.md "wikilink")，安達臣道由[安泰邨上方至靈實恩光學校一段已經消失](../Page/安泰邨.md "wikilink")，而餘下由安達臣道9號起的路段將於安泰總站上方位置新設迴旋處作掉頭之用。\[2\]
[Anderson_Road_Map.png](https://zh.wikipedia.org/wiki/File:Anderson_Road_Map.png "fig:Anderson_Road_Map.png")

## 歷史

[Anderson_Road_Quarry_Site_panoramio_2010.jpg](https://zh.wikipedia.org/wiki/File:Anderson_Road_Quarry_Site_panoramio_2010.jpg "fig:Anderson_Road_Quarry_Site_panoramio_2010.jpg")\]\]
安達臣道於1900年代由[駐港英軍興建](../Page/駐港英軍.md "wikilink")，以[香港政府物料供應監督安達臣](../Page/香港政府.md "wikilink")（）命名。昔日安達臣道在[馬游塘之後向南伸延](../Page/馬游塘.md "wikilink")，至山下的[三家村一帶為止](../Page/三家村_\(九龍\).md "wikilink")。現時通往[將軍澳華人永遠墳場的行車道也曾經是安達臣道的一部分](../Page/將軍澳華人永遠墳場.md "wikilink")\[3\]。

## 公共交通

自從前往[調景嶺的](../Page/調景嶺.md "wikilink")90號巴士在1996年10月取消，及同年[新界區專線小巴12線改經秀茂坪道及順利邨道後](../Page/新界區專線小巴12線.md "wikilink")，現時已無途經安達臣道的公共交通，但可以乘搭經過[安秀道](../Page/安秀道.md "wikilink")、[寶琳路或](../Page/寶琳路.md "wikilink")[清水灣道的巴士或小巴](../Page/清水灣道.md "wikilink")，在安達臣道路口的分站下車前往。

## 相關參見

  - [安達臣道發展計劃](../Page/安達臣道發展計劃.md "wikilink")
  - [安達臣道石礦場發展計劃](../Page/安達臣道石礦場發展計劃.md "wikilink")

## 外部連結

  - [中原地圖：安達臣道的位置](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=841663&cy=821258&zm=5&mx=841663&my=821258&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

## 資料來源

<references/>

[category:大上托](../Page/category:大上托.md "wikilink")

[Category:觀塘區街道](../Category/觀塘區街道.md "wikilink")
[Category:西貢區街道](../Category/西貢區街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")

1.  [2011年區議會選舉選區分界圖](http://www.eac.gov.hk/pdf/distco/maps/dc2011j.pdf)
2.  [安達臣道石礦場發展工程位置圖](https://www.cedd.gov.hk/eng/projects/major/doc/CDEARQZ0003.pdf)
3.  [［徵相］ 舊調景嶺巴士總站 - 巴士多媒體展示區
    (B3)](http://www.hkitalk.net/HKiTalk2/viewthread.php?tid=293831&extra=page%3D1&page=1)
    HKiTalk.net - 參看第115帖 (第八頁) 的街道圖