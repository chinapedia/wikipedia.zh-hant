**鬥牛**，是指[牛與人](../Page/牛.md "wikilink")、牛與牛、或牛與[狗間在](../Page/狗.md "wikilink")[鬥牛場進行打鬥的一項競技活動](../Page/鬥牛場.md "wikilink")。

## 西班牙鬥牛

[Matador.JPG](https://zh.wikipedia.org/wiki/File:Matador.JPG "fig:Matador.JPG")進行的鬥牛\]\]
鬥牛的[西班牙語為](../Page/西班牙語.md "wikilink")**toreo**，*corrida de
toros*或*tauromaquia*，[葡萄牙語為](../Page/葡萄牙語.md "wikilink")**tourada**，*corrida
de
touros*或*tauromaquia*，是一項人與牛鬥的運動。參與鬥牛的人稱為[鬥牛士](../Page/鬥牛士.md "wikilink")，主要流行於[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙以及](../Page/葡萄牙.md "wikilink")[拉丁美洲](../Page/拉丁美洲.md "wikilink")，更是西班牙的國技。鬥牛的歷史可追溯至史前時代的[牛崇拜以及](../Page/牛崇拜.md "wikilink")[壁畫中](../Page/壁畫.md "wikilink")。

## 葡萄牙鬥牛

鬥牛是葡萄牙的傳統項目，它與西班牙有很大的不同。在葡萄牙鬥牛中，鬥牛士是騎馬的，他們不會當場刺死牛，牛的牛角會被鋸掉，危險系數較西班牙鬥牛低。鬥牛過程中，鬥牛士會使用長矛​​抵擋公牛的進攻。[1](http://entertainment.bowenwang.com.cn/sports-and-athletics-bullfighting1.htm)

## 日本鬥牛

[日本的鬥牛則是一項牛與牛鬥的運動](../Page/日本.md "wikilink")。主要依靠的是兩條牛上的牛角，互相推擠比較力氣。常見於[愛媛縣](../Page/愛媛縣.md "wikilink")、[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")、[島根縣](../Page/島根縣.md "wikilink")、[沖繩縣](../Page/沖繩縣.md "wikilink")、[新潟縣等地](../Page/新潟縣.md "wikilink")。愛媛縣[城下町](../Page/城下町.md "wikilink")[宇和島市可以看到從](../Page/宇和島市.md "wikilink")300多年前沿襲至今的傳統鬥牛表演。現在每年仍要舉行5次。體重達1噸的兩頭牛角對角，一直要頂到一方喪失鬥志爲止。\[1\]

## 韓國

[韓國](../Page/韓國.md "wikilink")[慶尚北道的](../Page/慶尚北道.md "wikilink")[清道郡](../Page/清道郡.md "wikilink")，每年3月都會舉行清道鬥牛節，當中最矚目的，便是由兩頭牛的激烈對壘。為期五日的比賽中，會有超過一百頭經過細心挑選的牛隻參與，當中更分為國內及國外組別，從日本、美國和澳洲而來的牛王會與韓國牛王比賽，比賽長達九小時。欣賞鬥牛的同時，節慶中還有製作鬥牛飾物、坐牛車及品嘗清道牛料理等節目，以及各種的鄉土玩意。

## 中国斗牛

### 斗牛运动

斗牛是中国民间传统的一项习俗。唐代画家[戴嵩就画有](../Page/戴嵩.md "wikilink")《鬥牛圖》。南宋[曾敏行在](../Page/曾敏行.md "wikilink")《獨醒雜-{志}-》记载：“馬正惠公（[馬知節](../Page/馬知節.md "wikilink")）嘗珍其所藏[戴嵩](../Page/戴嵩.md "wikilink")《鬥牛圖》，暇日展曝於廳前，有輸租氓見而竊笑，公疑之，問其故。對曰：『農非知畫，乃識真牛。方其鬥時，夾尾於髀間，雖壯夫膂力不能出之。此圖皆舉其尾，似不類矣。』公為之歎服。”
可见宋代农民常观看斗牛运动。宋代有不少描述斗牛的诗歌，如北宋文学家[文同的](../Page/文同.md "wikilink")《毛老鬥牛》诗曰：“牛牛爾何爭，於此輒鬥怒。長鞭閙兒童，大炬走翁嫗。蒼㺏八九子，駭立各四顧。何時解角歸，茅舍江村暮。”（《丹淵集·卷十九》）在中国的少数民族地区，也存在着斗牛这样的运动，主要存在在牧区。如[云](../Page/云南.md "wikilink")[贵地区的](../Page/贵州.md "wikilink")[苗族和](../Page/苗族.md "wikilink")[侗族均有斗牛习俗](../Page/侗族.md "wikilink")，當中貴州省的開陽縣3月也會舉行鬥牛節，山頭上湧滿圍觀的熱鬧人群。在[浙江](../Page/浙江.md "wikilink")[金华也有斗牛的习俗](../Page/金华.md "wikilink")。

### “斗牛”神兽

中国民间传说中的斗牛是一种虬龙，常出现在中国古代建筑的屋脊之上，为五脊六兽之一。牛身有鱼鳞，一般被传说是一种兴云作雨、镇火防灾的镇水兽吉祥物，如《宸垣识略》载：“西内海子中有斗牛……遇阴雨作云雾，常蜿蜒道路旁及金鳌玉栋坊之上”，古时，曾发水患之地多以牛镇之。牛角龙身，龙爪四指，常出现在明朝赏赐的三品官服上，称斗牛服。形态是小一个等级的囚牛。

## 英國

在英國古代，鬥牛是指用[鬥牛犬等犬類與公牛相鬥取樂](../Page/鬥牛犬.md "wikilink")，在19世紀初被廢止。

## 軼事

2009年1月24日，[墨西哥一名](../Page/墨西哥.md "wikilink")11歲被當地人譽爲「鬥牛神童」鬥牛士米切利托．拉戈拉威爾在[墨西哥](../Page/墨西哥.md "wikilink")[梅里達的一個鬥牛場](../Page/梅里達.md "wikilink")2小時內殺死了六頭一至兩歲公牛，在爭議下申請吉尼斯世界記錄證書。不過，吉尼斯總部以這個紀錄過于血腥和殘忍，表示不會認可，「我們不會接受建立在對動物的殺戮和傷害基礎之上的紀錄。」他將角逐殺死牛最多和年齡最小的鬥牛士兩項世界記錄，包括許多兒童在內的3500人在鬥牛場觀看了拉戈拉威爾的鬥牛表演。拉戈拉威爾在事後稱：「我很高興取得這一偉大的勝利。」他的行為使動物權利保護人士感到憤怒。墨西哥鬥牛協會主席佩德羅·哈瑟斯表示：「米利托的勝利對于墨西哥在推廣鬥牛運動上有著積極的意義。現在墨西哥的鬥牛士越來越少了，米利托的出現，一定會使鬥牛業重新火爆起來。」他在6歲時已在墨西哥已經參加過160場鬥牛比賽、殺死了數十頭牛。\[2\]

## 相關條目

  - [鬥牛場](../Page/鬥牛場.md "wikilink")

## 參考資料

  - [YouTube鬥牛影片](http://www.youtube.com/watch?v=24fzpMbaTPU&eurl=http%3A%2F%2Fwww%2Egerardrodriguez%2Eorg%2Fantitauromaquia%2F)
  - [西班牙14歲鬥牛士在墨西哥表演差點喪命](http://www.epochtimes.com/b5/7/4/17/n1681180.htm)
  - [日本沖繩鬥牛賽　爭輸贏不見血](http://www.ettoday.com/2007/04/16/334-2082414.htm)
  - <http://entertainment.bowenwang.com.cn/sports-and-athletics-bullfighting1.htm>

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[cs:Korida](../Page/cs:Korida.md "wikilink") [gl:Corrida de
touros](../Page/gl:Corrida_de_touros.md "wikilink")

[鬥牛](../Category/鬥牛.md "wikilink")

1.  [日本國家旅遊局－愛媛縣宇和島](http://www.welcome2japan.hk/location/regional/ehime/uwajima.html)
2.  [11歲鬥牛神童2小時殺6公牛
    殘忍血腥吉尼斯不認－中國窗－香港商報](http://www.hkcd.com.hk/content/2009-02/18/content_2247803.htm)