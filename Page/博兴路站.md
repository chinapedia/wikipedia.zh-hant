**博兴路站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[张杨北路](../Page/张杨北路.md "wikilink")[博兴路](../Page/博兴路.md "wikilink")，为[上海轨道交通6号线的地下](../Page/上海轨道交通6号线.md "wikilink")[侧式车站](../Page/侧式站台.md "wikilink")，于2007年12月29日启用。车站以黃色作為佈置的主要色彩。

## 公交换乘

85、716、783、785、791、815、981、大桥四线、1026

## 车站出口

  - 1号口：张杨北路西侧，博兴路南
  - 2号口：张杨北路东侧，博兴路南

## 相邻车站

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")