**大鰻螈**（[學名](../Page/學名.md "wikilink")：**）是一種像[鰻魚的](../Page/鰻魚.md "wikilink")[兩棲類動物](../Page/兩棲類.md "wikilink")。牠被分類為[有尾目被受爭議](../Page/有尾目.md "wikilink")。最大的大鰻螈可以成長至48-97厘米長。體色由深綠色至接近黑色，而下腹則是淺灰色或黃色。年輕的大鰻螈在其兩側有一條淺色的斑紋，隨著年紀漸長而消退。它們有大的[鰓](../Page/鰓.md "wikilink")，沒有後腳。前腳有四趾，細少而可以收藏於鰓內。

大鰻螈一般都是[食肉動物及以](../Page/食肉動物.md "wikilink")[環節動物](../Page/環節動物.md "wikilink")、[昆蟲](../Page/昆蟲.md "wikilink")、[蝸牛及細少的](../Page/蝸牛.md "wikilink")[魚為食物](../Page/魚.md "wikilink")，不過牠們亦曾被發現以[植物為食物](../Page/植物.md "wikilink")。牠們有[體側線的感官](../Page/體側線.md "wikilink")[器官用作探測獵物](../Page/器官.md "wikilink")。

牠們生活在[華盛頓哥倫比亞特區至](../Page/華盛頓哥倫比亞特區.md "wikilink")[佛羅里達州一帶](../Page/佛羅里達州.md "wikilink")。雌性於二月至三月間產卵，可多達500個卵。卵在約兩個月後孵化。而卵的受精方法卻仍未知。

## 參考

  -
  -
[Category:鰻螈科](../Category/鰻螈科.md "wikilink")
[Category:墨西哥动物](../Category/墨西哥动物.md "wikilink")
[Category:美國動物](../Category/美國動物.md "wikilink")