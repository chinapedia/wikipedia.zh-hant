[William_Hogarth_006.jpg](https://zh.wikipedia.org/wiki/File:William_Hogarth_006.jpg "fig:William_Hogarth_006.jpg")
(1697年–1764年)\]\]
**《四個殘酷的舞台》**是一系列的四幅[雕版畫](../Page/雕版.md "wikilink")，由[英國](../Page/英國.md "wikilink")[畫家](../Page/畫家.md "wikilink")[威廉·贺加斯於](../Page/威廉·贺加斯.md "wikilink")1751年所製作的。整套印刷品描述了虛構人物湯姆・尼祿一生中四個不同的舞台。一開始從孩童年代的[戲虐小狗為](../Page/虐待動物.md "wikilink")《第一個殘酷的舞台》，尼祿長大成人後繼而對他的馬作出摔打為《第二個殘酷的舞台》，之後在《終極殘酷》中更牽涉[搶劫](../Page/搶劫.md "wikilink")、[誘姦和](../Page/誘姦.md "wikilink")[謀殺等髮指罪行](../Page/謀殺.md "wikilink")。然而，贺加斯認為由他一開始鋪路作犯科，就要無法避免地承受命運的制裁。結果，尼祿在《殘酷的回報》中以兇殺的罪名被送上[絞架](../Page/絞架.md "wikilink")[問吊](../Page/絞刑.md "wikilink")，而死後他的屍首亦任由一群[外科醫生在](../Page/外科.md "wikilink")[解剖講堂將之切開解剖](../Page/解剖講堂.md "wikilink")。

贺加斯有感於同樣的倫常慘案每天在[倫敦街頭不斷發生](../Page/倫敦.md "wikilink")，因而特意製作雕版畫來達致[教化大眾的效果](../Page/道德.md "wikilink")。為方便當時的[低下階層閱讀](../Page/勞動階層.md "wikilink")，版畫都印刷在廉價的紙張上。整個系列呈現一種[處決帶來的粗暴與殘忍的感覺](../Page/死刑.md "wikilink")，而作者亦沒有顯示一貫幽默的畫風來緩和作品的殘酷氣氛。霍加斯認為畫中的殘忍感覺能夠向既定的觀眾傳遞他意圖表達的訊息。儘管如此，《四個殘酷的舞台》依然可以見到贺加斯作品的色彩，例如帶有豐富的細節與隱約的關聯思想。

## 備註

<div class="references-small">

**a.** A pair of impressions from Bell's original printing were acquired
for [£](../Page/pound_sterling.md "wikilink")1600 by the [University of
Glasgow](../Page/University_of_Glasgow.md "wikilink")'s [Hunterian
Museum and Art
Gallery](../Page/Hunterian_Museum_and_Art_Gallery.md "wikilink") in
2005.\[1\]

**b.** There is some confusion over the date of George "The Barber"
Taylor's career and death. In his earlier work Paulson puts him as a
pupil of Broughton, killed in a fight with him in 1750, and the Tate
Gallery dates Hogarth's sketches to c.1750.\[2\] In *Hogarth's
"Harlot"*, he states that Taylor retired in 1750 but came out of
retirement in 1757 for a final fight in which he was badly beaten, dying
from his injuries several months later. Most records date Taylor's
championship to the middle 1730s.

**c.** The initials on the box are normally read as A. G. for Ann Gill,
but the G resembles a D, suggesting the box too may have been stolen.

**d.** John Ireland identifies the president as "Mr Frieake, the master
of [Nourse](../Page/Edward_Nourse.md "wikilink"), to whom [Mr
Potts](../Page/Percival_Pott.md "wikilink") was a pupil". Since Ireland
identifies him as the master of Nourse, he undoubtedly means John Freke,
an acquaintance of Hogarth's and surgeon at [St Bartholomew's
Hospital](../Page/St_Bartholomew's_Hospital.md "wikilink") from
1729–1755 and a Governor 1736–1756. The dissection could be taking
place at St Bartholomew's Hospital, where all three surgeons were based,
but it also has features of the Cutlerian Theatre of the [Royal College
of Physicians](../Page/Royal_College_of_Physicians.md "wikilink") near
[Newgate](../Page/Newgate.md "wikilink") (particularly the throne, which
bears their arms, and its curved wall resembling a
[cockpit](../Page/cockpit.md "wikilink")) and the niches of the
[Barber-Surgeons'
Hall](../Page/Worshipful_Company_of_Barbers.md "wikilink") (which was
not used for dissection after the Surgeons split away to form the
[Company of Surgeons](../Page/Company_of_Surgeons.md "wikilink") in
1745).

</div>

## 參考資料

<div class="references-small"  style="-moz-column-count:2; column-count:2;">

<references/>

</div>

[Category:版畫](../Category/版畫.md "wikilink")
[Category:威廉·霍加斯](../Category/威廉·霍加斯.md "wikilink")
[Category:1751年作品](../Category/1751年作品.md "wikilink")
[Category:动物权利媒体](../Category/动物权利媒体.md "wikilink")

1.
2.