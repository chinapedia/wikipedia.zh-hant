**NHK WORLD-JAPAN**（），旧称**NHK WORLD**或**NHK WORLD
TV**，台湾方面称之为“**NHK新聞資訊台**”\[1\]，是[日本廣播协会](../Page/日本廣播协会.md "wikilink")（NHK）对日本國外[广播业务](../Page/广播.md "wikilink")（[NHK日本国际传媒](../Page/NHK日本国际传媒.md "wikilink")）所設的[英语](../Page/英语.md "wikilink")[国际新闻频道](../Page/国际新闻频道.md "wikilink")。该频道主要向日本以外的国家播出，但日本国内观众也可以通过[有线电视](../Page/有线电视.md "wikilink")、[IPTV和NHK网站收看该频道](../Page/IPTV.md "wikilink")\[2\]。

## 历史

NHK WORLD的前身是1995年4月3日利用北美[TV
Japan](../Page/TV_Japan.md "wikilink")、歐洲[JSTV頻道時段開播的](../Page/日本語衛星放送.md "wikilink")“NHK國際電視廣播”（NHK
International TV Broadcasting）\[3\]，海外稱「NHK
International」（NHK國際台），因納入「NHK環球廣播網」而改稱“NHK World
TV”。1998年，NHK WORLD开始面向亚太地区播出，2001年开始面向非洲播出，至此NHK WORLD实现了全球覆盖。

2006年，有鉴于[今日俄罗斯](../Page/今日俄罗斯.md "wikilink")、[半岛电视台英语频道和CCTV](../Page/半岛电视台英语频道.md "wikilink")-9（即现在的[中国环球电视网英语新闻频道](../Page/中国环球电視网_\(频道\).md "wikilink")）等新兴英语国际频道的出现，NHK决定增加NHK
WORLD TV英语节目时长。

2008年8月29日，NHK WORLD TV实现全程以英語提供新聞及資訊節目，但仍保留日语副声道。

2009年2月2日，NHK WORLD TV取消所有以日语播出的节目，成为纯英语频道。

2009年12月7日，NHK WORLD
TV開始以[1080i規格全高清播出](../Page/1080i.md "wikilink")，同時[NTSC](../Page/NTSC.md "wikilink")[標清版亦停止傳送](../Page/標清.md "wikilink")；而[PAL標清則保留](../Page/PAL.md "wikilink")，內容跟高清版同步。

2018年4月30日，NHK WORLD TV更名為NHK WORLD-JAPAN。

## 节目

NHK World-Japan节目以新闻资讯为主，每天24小時逢整點播出新聞節目《[NHK
NEWSLINE](../Page/NHK_NEWSLINE.md "wikilink")》，也会提供部分文化、音乐、料理等节目。部分节目会在[NHK
BS1播出](../Page/NHK_BS1.md "wikilink")。另外，如果日本国内发生重大事件，NHK
World-Japan会以英语同声传译的方式与NHK其他频道同步播出（如[2016年熊本地震期间及](../Page/2016年熊本地震.md "wikilink")2016年8月8日[天皇](../Page/天皇.md "wikilink")[明仁发表电视讲话](../Page/明仁.md "wikilink")）。

## 参考文献

## 外部链接

  - [NHK WORLD-JAPAN官方网站](http://www.nhk.or.jp/nhkworld)

  -
[Category:日本电视频道](../Category/日本电视频道.md "wikilink")
[Category:1995年成立的电视台或电视频道](../Category/1995年成立的电视台或电视频道.md "wikilink")
[Category:NHK电视频道](../Category/NHK电视频道.md "wikilink")
[Category:卫星电视频道](../Category/卫星电视频道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")

1.  [國家通訊傳播委員會通過七個頻道申設案](http://www.ncc.gov.tw/Chinese/print.aspx?table_name=news&site_content_sn=8&sn_f=14727).
    國家通訊傳播委員會. 2010-03-31 \[2012-08-27\] .
2.  [How To Watch](https://www3.nhk.or.jp/nhkworld/en/tv/howto/)，NHK
3.  \[<http://www.thefreelibrary.com/TV-JAPAN+to+launch+NHK+International+TV+broadcasting%3b+unscrambles+new>...-a016741872
    TV-JAPAN to launch NHK International TV broadcasting\]. TV Japan.
    1995-03-31 \[2012-08-28\] .