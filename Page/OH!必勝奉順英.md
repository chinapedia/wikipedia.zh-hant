《**OH\!必勝奉順英**》（[韓文](../Page/諺文.md "wikilink")：****，）是[韓國](../Page/大韓民國.md "wikilink")[KBS電視台於](../Page/韓國放送公社.md "wikilink")2004年9月至11月期間，韓國時間逢星期一、二，晚上9時55分播放的[月火迷你連續劇由金鍾學製作公司負責](../Page/KBS月火迷你連續劇.md "wikilink")，[安在旭](../Page/安在旭.md "wikilink")、[蔡琳](../Page/蔡琳.md "wikilink")、[柳鎮](../Page/柳鎮_\(韓國\).md "wikilink")、[朴宣暎等演員演出](../Page/朴宣暎.md "wikilink")。台灣[緯來戲劇台及](../Page/緯來戲劇台.md "wikilink")[華視播映本劇時](../Page/華視.md "wikilink")，改名《**必勝！奉順英**》。

## 劇情介紹

吳必勝自小在與母親在釜山生活，母親更在他讀中學時死去，必勝唯有靠自己的能力生活。有一日，大集團的會長得知她的孫子在交通意外中死去，她唯有派秘書去找失散多年的孫子。吳必勝得知自己是會長的孫子後，便在奶奶的大賣場學習經營，並得到魯有貞的輔助。必勝在那賣場與職員奉順英相遇，並與她由冤家發展成情人；另一方面，會長秘書的兒子，在大賣場當室長的尹在雄亦對奉順英有好感；而有貞在教導必勝的同時，亦漸漸喜歡上必勝，四人由此展開一段複雜的四角關係……

## 演員表

  - [安在旭](../Page/安在旭.md "wikilink") 飾演 **吳必勝**
  - [蔡　琳](../Page/蔡琳.md "wikilink") 飾演 **奉順英**
  - [柳鎮](../Page/柳鎮_\(韓國\).md "wikilink") 飾演 **尹在雄**
  - [朴宣暎](../Page/朴宣暎.md "wikilink") 飾演 **盧有婷**

## 外部連結

  - [KBS《OH\!必勝奉順英》官方網站](http://www.kbs.co.kr/drama/feelyoung/index.html)

  - [緯來戲劇台《必勝！奉順英》官方網站](http://drama.videoland.com.tw/channel/victory/)
  - [華視《必勝！奉順英》官方網站](http://event.cts.com.tw/feelyoung/)

[Category:KBS月火連續劇](../Category/KBS月火連續劇.md "wikilink")
[Category:2004年韓國電視劇集](../Category/2004年韓國電視劇集.md "wikilink")
[Category:亞洲電視外購劇集](../Category/亞洲電視外購劇集.md "wikilink")
[Category:緯來電視外購韓劇](../Category/緯來電視外購韓劇.md "wikilink")
[Category:華視外購電視劇](../Category/華視外購電視劇.md "wikilink")
[Category:韓國愛情劇](../Category/韓國愛情劇.md "wikilink")
[Category:姜銀慶劇本作品](../Category/姜銀慶劇本作品.md "wikilink")
[Category:香港有線電視外購劇集](../Category/香港有線電視外購劇集.md "wikilink")