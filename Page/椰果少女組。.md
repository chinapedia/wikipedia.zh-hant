**椰果少女組。**（[日文](../Page/日文.md "wikilink")：****）是一個來自[Hello\!
Project的](../Page/Hello!_Project.md "wikilink")[日本女子組合](../Page/日本.md "wikilink")，於1999年成立，並於2008年4月30日完全解散。

[Category:Hello\! Project](../Category/Hello!_Project.md "wikilink")
[Category:日本女子演唱團體](../Category/日本女子演唱團體.md "wikilink")
[Category:已解散的女子演唱團體](../Category/已解散的女子演唱團體.md "wikilink")
[Category:1999年成立的音樂團體](../Category/1999年成立的音樂團體.md "wikilink")
[Category:2008年解散的音樂團體](../Category/2008年解散的音樂團體.md "wikilink")