<noinclude></noinclude>
**經紀公司**，即是**經理人公司**（），該些[公司會為簽有合約的旗下](../Page/公司.md "wikilink")[員工](../Page/員工.md "wikilink")（以[藝人為常見](../Page/藝人.md "wikilink")）或[客戶](../Page/客戶.md "wikilink")（如職業[運動員](../Page/運動員.md "wikilink")、[配音員](../Page/配音員.md "wikilink")、[Youtuber](../Page/Youtuber.md "wikilink")）提供[經理人服務](../Page/經理人.md "wikilink")。當該合約生效時，該公司便會自動成為該些員工的經紀公司。簽訂該合約的員工在合約生效期間，身邊會擁有一位由公司派出的[經理人](../Page/經理人.md "wikilink")，負責為其安排基本工作外的工作和其他活動，有額外[薪酬](../Page/薪酬.md "wikilink")。正是這樣，合約生效期間，公司與該員工的關係比較密切，猶如[親屬關係](../Page/親屬關係.md "wikilink")，而合約有效期通常比較長。而[唱片公司亦多為經理人公司](../Page/唱片公司.md "wikilink")。

## 藝人的經紀公司

艺人的经纪人剛開始從围着明星团团转的职业保姆作起，除了要帮自家艺人端茶送水，嘘寒问暖，在更有經驗後，開始要帮他们接通告，接演出，接广告，与媒体唇枪舌剑，为艺人做心理按摩、挡风遮雨、消灾避难。所以经纪人也掌握着艺人的生杀大权，检查艺人是否违约，相當程度支配著藝人的收入所得，以及私生活。

中國的经纪公司的签约年限为五年到十年，一般是八年。一些从小就被大型经纪公司栽培的少年偶像，则要签下长达十多年的合约，例如[谢霆锋](../Page/谢霆锋.md "wikilink")，他和英皇的合约就从四年延长至十年，而[梁洛施](../Page/梁洛施.md "wikilink")12岁时与英皇签下了10年合约，后延长为15年。

一些還沒走紅的藝人，这部分的钱甚至要等艺人将来赚了钱，再來慢慢还清。\[1\]

## 合约

经纪公司与艺人的[合约裡](../Page/合约.md "wikilink")，几乎都对签约艺人提出了一系列约束，却鲜少明确艺人的权利，相當的不平等。为了避免出现艺人一红就走人的状况，合约中会有高昂的违约金。合约期限越长的，违约金额越高。[黄圣依向](../Page/黄圣依.md "wikilink")[周星驰星辉公司单方面提出解约时](../Page/周星驰.md "wikilink")，就遭到对方千万违约金的索赔。如果经纪公司不想带这个艺人了，就可以快速解约，艺人不用付违约金。

[韩国曾爆出艺人不平等合约案](../Page/韩国.md "wikilink")，合约要求艺人不仅要交代学业、去向、交友、家庭以及[性生活](../Page/性生活.md "wikilink")。\[2\]尽管限制[恋爱是不合法的](../Page/恋爱.md "wikilink")，大多数经纪公司都不准艺人[谈恋爱](../Page/拍拖.md "wikilink")，一来怕艺人分神，二来担心影响艺人形象。\[3\]少数公司则不限制恋爱。\[4\]艺人越大牌，经纪公司管得越少。

## 主要的经纪公司

  - 中國大陸

:\* [乐华娱乐](../Page/乐华娱乐.md "wikilink")

:\* [絲芭傳媒](../Page/絲芭傳媒.md "wikilink")

:\* [香蕉娛樂](../Page/香蕉計劃.md "wikilink")

  - 香港

:\* [英皇娛樂](../Page/英皇娛樂.md "wikilink")

  - 台灣

:\* [華研國際](../Page/華研國際音樂.md "wikilink")

:\* [百代唱片](../Page/百代唱片.md "wikilink")

:\* [滾石唱片](../Page/滾石唱片.md "wikilink")

:\* [相信音樂](../Page/相信音樂.md "wikilink")

:\* [伊林模特](../Page/伊林模特兒經紀公司.md "wikilink")

:\* [凱渥模特](../Page/凱渥模特兒經紀公司.md "wikilink")

:\* [鳳凰藝能](../Page/鳳凰藝能.md "wikilink")

  - 日本

:\* [雅慕斯娛樂](../Page/Amuse.md "wikilink")

:\* [傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")

  - 韓國

:\* [SM娛樂](../Page/SM_Entertainment.md "wikilink")

:\* [YG娛樂](../Page/YG_Entertainment.md "wikilink")

:\* [JYP娛樂](../Page/JYP_Entertainment.md "wikilink")

:\* [Cube娛樂](../Page/Cube娛樂.md "wikilink")

:\* [LOEN娛樂](../Page/LOEN娛樂.md "wikilink")

  - 美國

:\* [環球唱片](../Page/環球唱片.md "wikilink")

:\* [華納唱片](../Page/華納唱片.md "wikilink")

:\* [索尼音乐娱乐](../Page/索尼音乐娱乐.md "wikilink")

## 参考资料

## 相關條目

  - [經理人](../Page/經理人.md "wikilink")
  - [電視台](../Page/電視台.md "wikilink")
  - [電影公司](../Page/電影公司.md "wikilink")
  - [唱片公司](../Page/唱片公司.md "wikilink")
  - [酒店經紀](../Page/酒店經紀.md "wikilink")
  - [師傅](../Page/師傅.md "wikilink")

[Category:藝人經紀公司](../Category/藝人經紀公司.md "wikilink")

1.
2.
3.
4.