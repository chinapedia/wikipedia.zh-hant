在[抽象代數中](../Page/抽象代數.md "wikilink")，**交換代數**旨在探討[交換環及其](../Page/環.md "wikilink")[理想](../Page/理想_\(環論\).md "wikilink")，以及交換環上的[模](../Page/模.md "wikilink")。[代數數論與](../Page/代數數論.md "wikilink")[代數幾何皆奠基於交換代數](../Page/代數幾何.md "wikilink")。交換環中最突出的例子包括[多項式環](../Page/多項式.md "wikilink")、[代數整數環與](../Page/代數整數.md "wikilink")[p進數環](../Page/p進數.md "wikilink")，以及它們的各種[商環與](../Page/商環.md "wikilink")[局部化](../Page/局部化.md "wikilink")。

由於[概形無非是](../Page/概形.md "wikilink")[交換環譜的黏合](../Page/交換環譜.md "wikilink")，交換代數遂成為研究[概形](../Page/概形.md "wikilink")[局部性質的主要語言](../Page/局部性質.md "wikilink")。

## 源流

此學科原稱「理想論」，始自[戴德金在](../Page/戴德金.md "wikilink")[理想方面的工作](../Page/理想_\(數學\).md "wikilink")，而其工作又建基於[庫默爾與](../Page/庫默爾.md "wikilink")[克羅內克的早期工作](../Page/利奧波德·克羅內克.md "wikilink")。此後[希爾伯特引入術語](../Page/希爾伯特.md "wikilink")「環」，以推廣先前採用的「數環」。希爾伯特以較抽象的進路取代先前基於[複分析與](../Page/複分析.md "wikilink")[不變量理論的計算導向進路](../Page/不變量理論.md "wikilink")，希爾伯特大大啟發了[埃米·諾特](../Page/埃米·諾特.md "wikilink")，諾特在交換代數中引進了許多公理化的抽象方法。另一位重要角色是希爾伯特的弟子Emanuel
Lasker（也是世界棋王），他引入了[準素理想](../Page/準素分解.md "wikilink")，並證明了Lasker-Noether定理的首個版本。

現代交換代數學置重點於[模](../Page/模.md "wikilink")。一個環\(R\)的理想、商環與\(R\)-代數皆可視為\(R\)-模，是以模論能兼攝理想與環擴張。儘管模論在克羅內克的工作中已開先河，一般仍將此歸功於埃米·諾特。

## 文獻

  - Michael Atiyah & Ian G. MacDonald, *Introduction to Commutative
    Algebra*, Massachusetts : Addison-Wesley Publishing, 1969.
  - David Eisenbud, *Commutative Algebra With a View Toward Algebraic
    Geometry*, New York : Springer-Verlag, 1999.
  - Hideyuki Matsumura, translated by Miles Reid, *Commutative Ring
    Theory (Cambridge Studies in Advanced Mathematics)*,Cambridge, UK :
    Cambridge University Press, 1989.
  - Miles Reid, *Undergraduate Commutative Algebra (London Mathematical
    Society Student Texts)*, Cambridge, UK : Cambridge University Press,
    1996.
  - Jean-Pierre Serre, *Algèbre locale, multiplicités*

## 外部連結

  - [List of Commutative Algebraists](http://www.commalg.org/people)
  - [The Commutative Algebra Community](http://www.commalg.org)
  - [A Course in Commutative
    Algebra](http://www.math.uiuc.edu/~r-ash/ComAlg.html)
    一本入門書籍，可自由下載閱讀。

[J](../Category/抽象代數.md "wikilink") [\*](../Category/交換代數.md "wikilink")