《**演藝界總動員忘我大匯演**》，簡稱《**忘我大匯演**》，是[香港市民為支持](../Page/香港市民.md "wikilink")[1991年華東水災災民而舉辦的籌款活動](../Page/1991年華東水災.md "wikilink")，於1991年7月27日在[跑馬地馬場舉行](../Page/跑馬地馬場.md "wikilink")。此次盛會持續時間長達七小時，[無綫電視翡翠台與](../Page/無綫電視翡翠台.md "wikilink")[亞洲電視本港台聯播](../Page/亞洲電視本港台.md "wikilink")。

## 大會司儀

  - [沈殿霞](../Page/沈殿霞.md "wikilink")、[黃霑](../Page/黃霑.md "wikilink")、[曾志偉](../Page/曾志偉.md "wikilink")、[陳欣健](../Page/陳欣健.md "wikilink")、[岑建勳](../Page/岑建勳.md "wikilink")、[鄭丹瑞](../Page/鄭丹瑞.md "wikilink")

## 出席歌手/樂隊

  - [BEYOND](../Page/BEYOND.md "wikilink")、[軟硬天師](../Page/軟硬天師.md "wikilink")、[梅艷芳](../Page/梅艷芳.md "wikilink")、[草蜢](../Page/草蜢_\(組合\).md "wikilink")、[溫拿](../Page/溫拿樂隊.md "wikilink")、[周潤發](../Page/周潤發.md "wikilink")、[藝進同學會](../Page/藝進同學會.md "wikilink")、[譚詠麟](../Page/譚詠麟.md "wikilink")、[鍾鎮濤](../Page/鍾鎮濤.md "wikilink")、[盧冠廷](../Page/盧冠廷.md "wikilink")、[肥媽](../Page/肥媽.md "wikilink")、[成龍](../Page/成龍.md "wikilink")、[蘇慧倫](../Page/蘇慧倫.md "wikilink")、[甄妮](../Page/甄妮.md "wikilink")、[太極](../Page/太極樂隊.md "wikilink")、[周啟生](../Page/周啟生.md "wikilink")、[黎明](../Page/黎明.md "wikilink")、[劉德華](../Page/劉德華.md "wikilink")、[梁朝偉](../Page/梁朝偉.md "wikilink")、[黃日華](../Page/黃日華.md "wikilink")、[湯鎮業](../Page/湯鎮業.md "wikilink")、[關淑怡](../Page/關淑怡.md "wikilink")、[芳艷芬](../Page/芳艷芬.md "wikilink")、[鍾楚紅](../Page/鍾楚紅.md "wikilink")、[蕭芳芳](../Page/蕭芳芳.md "wikilink")、[周華健](../Page/周華健.md "wikilink")、[林子祥](../Page/林子祥.md "wikilink")、[苗僑偉](../Page/苗僑偉.md "wikilink")、[崔健](../Page/崔健.md "wikilink")、[呂方](../Page/呂方.md "wikilink")、[林青霞](../Page/林青霞.md "wikilink")、[夏韶聲](../Page/夏韶聲.md "wikilink")、[葉蒨文](../Page/葉蒨文.md "wikilink")、[張學友](../Page/張學友.md "wikilink")、[杜德偉](../Page/杜德偉.md "wikilink")、[羅文](../Page/羅文.md "wikilink")、[陳潔靈](../Page/陳潔靈.md "wikilink")、[徐小鳳](../Page/徐小鳳.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[葉麗儀](../Page/葉麗儀.md "wikilink")、[李宗盛](../Page/李宗盛.md "wikilink")、[張艾嘉](../Page/張艾嘉.md "wikilink")、[羅大佑](../Page/羅大佑.md "wikilink")、[蔣志光](../Page/蔣志光.md "wikilink")、[鞏俐](../Page/鞏俐.md "wikilink")、[童安格](../Page/童安格.md "wikilink")、[黃凱芹](../Page/黃凱芹.md "wikilink")、[庾澄慶等](../Page/庾澄慶.md "wikilink")。

## 參見

  - 《[滔滔千里心](../Page/滔滔千里心.md "wikilink")》（粵）－本次大匯演主題曲
  - [豪門夜宴 (1991年電影)](../Page/豪門夜宴_\(1991年電影\).md "wikilink")－香港演藝界忘我大電影

## 參考資料

  - [滔滔千里心歌詞](https://www.kkbox.com/hk/tc/song/OXlSRsg091oPHYbVRYbVR0P4-index.html)

[Category:1991年7月](../Category/1991年7月.md "wikilink")
[Category:香港慈善活動](../Category/香港慈善活動.md "wikilink")
[Category:1991年香港](../Category/1991年香港.md "wikilink")