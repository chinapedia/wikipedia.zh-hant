[Gustave_Moreau_Orphée_1865.jpg](https://zh.wikipedia.org/wiki/File:Gustave_Moreau_Orphée_1865.jpg "fig:Gustave_Moreau_Orphée_1865.jpg")。\]\]
[Jean-Baptiste-Camille_Corot_-_Orphée.jpg](https://zh.wikipedia.org/wiki/File:Jean-Baptiste-Camille_Corot_-_Orphée.jpg "fig:Jean-Baptiste-Camille_Corot_-_Orphée.jpg")离开[地狱](../Page/地狱.md "wikilink")\]\]

**俄耳甫斯**（[希臘文](../Page/希臘文.md "wikilink")：）是希腊神话中的一位音乐家。传说他是[色雷斯人](../Page/色雷斯.md "wikilink")\[1\]，故乡是[奥德里西亚王国的](../Page/奥德里西亚王国.md "wikilink")[比萨尔提亚](../Page/比萨尔提亚.md "wikilink")\[2\]，参加过[阿耳戈英雄远征](../Page/阿耳戈英雄.md "wikilink")，亦以与其妻[欧律狄刻的悲情故事而为人所铭记](../Page/欧律狄刻.md "wikilink")。

根据俄耳甫斯留下的诗篇，[古希腊出现过一个以他為名的](../Page/古希腊.md "wikilink")[秘密宗教](../Page/希臘羅馬密教.md "wikilink")，即[俄耳甫斯教](../Page/俄耳甫斯教.md "wikilink")。

## 神话

[阿波罗与](../Page/阿波罗.md "wikilink")[缪斯女神中的](../Page/缪斯.md "wikilink")[卡利俄珀所生](../Page/卡利俄珀.md "wikilink")，[音乐天资超凡入化](../Page/音乐.md "wikilink")。他的演奏让木石生悲、猛獸驯服。[伊阿宋组织](../Page/伊阿宋.md "wikilink")[阿耳戈英雄远征](../Page/阿耳戈英雄.md "wikilink")，去涛汹地险的黑海王国寻取[金羊毛](../Page/金羊毛.md "wikilink")。俄耳甫斯踊跃参加，在征途中用神乐压倒了[塞壬的艳迷歌声](../Page/塞壬.md "wikilink")，挽救了行将触礁的征船和战友。塞壬们沮丧不堪，纷纷投海自尽。

音乐也使他痛心：[寧芙](../Page/寧芙.md "wikilink")[欧利蒂丝倾醉七弦竖琴](../Page/欧利蒂丝.md "wikilink")（[里拉](../Page/里拉_\(乐器\).md "wikilink")）的恬音美乐，投入英俊少年的怀抱。婚宴中，女仙被毒蛇噬足而亡。痴情的俄耳甫斯冲入地狱，用琴声打动了冥王[黑帝斯](../Page/黑帝斯.md "wikilink")，欧利蒂丝再获生机。但冥王告诫少年，离开地狱前万万不可回首张望。冥途将尽，俄耳甫斯遏不住胸中爱念，转身确定妻子是否跟随在后，却使欧利蒂丝堕回冥界的无底深渊。

悲痛欲绝的少年隐离尘世，山野漂泊中遇到崇奉酒神[戴歐尼修斯及醉里痴狂的一帮](../Page/戴歐尼修斯.md "wikilink")[色雷斯女人](../Page/色雷斯.md "wikilink")，不幸死在她们手中。砍下的头颅虽被抛入河流，口里仍旧呼唤着欧利蒂丝的名字。缪斯女神将他安葬后，七弦琴化成了苍穹间的[天琴座](../Page/天琴座.md "wikilink")。

## 相關條目

  - [地狱中的奥菲欧](../Page/天國與地獄序曲.md "wikilink")

## 参考文献

  - 上海辞书出版社《辞海 上》第637页 1999年版

[Category:文學小作品](../Category/文學小作品.md "wikilink")
[俄耳甫斯](../Category/俄耳甫斯.md "wikilink")

1.  Fritz Graf and Sarah Iles Johnston, *Ritual Texts for the Afterlife:
    Orpheus and the Bacchic Gold Tablets* (Routledge, 2007), p. 167
2.  [John Tzetzes](../Page/John_Tzetzes.md "wikilink").
    *[Chiliades, 1.12
    line 305](http://www.theoi.com/Text/TzetzesChiliades1.html#12)*