**鄭進一**，本名**鄭俊義**，[台灣](../Page/台灣.md "wikilink")[男藝人](../Page/男藝人.md "wikilink")、[歌手](../Page/歌手.md "wikilink")、[綜藝節目](../Page/綜藝節目.md "wikilink")[主持人](../Page/主持人.md "wikilink")、[填詞人](../Page/填詞人.md "wikilink")、[作曲家](../Page/作曲家.md "wikilink")、[製作人](../Page/製作人.md "wikilink")，生於[台南縣](../Page/台南縣.md "wikilink")[新營鎮](../Page/新營鎮.md "wikilink")。因唱腔近似[日本](../Page/日本.md "wikilink")[演歌歌手](../Page/演歌.md "wikilink")[森進一](../Page/森進一.md "wikilink")，因而取藝名為鄭進一。台灣電視綜藝界「三王一-{后}-」\[1\]\[2\]\[3\]中的兩位「綜藝[天王](../Page/天王.md "wikilink")」：「綜藝大哥大」[張菲是鄭進一的](../Page/張菲.md "wikilink")[師父](../Page/師徒.md "wikilink")，「綜藝大哥」[胡瓜則為其師兄弟](../Page/胡瓜.md "wikilink")。

## 演藝生涯

1962年至1965年間，鄭進一參加許多歌唱比賽；其中，在[台南勝利電台](../Page/臺南市.md "wikilink")、[雲林虎尾電台](../Page/雲林縣.md "wikilink")、[彰化國聲電台](../Page/彰化縣.md "wikilink")、[嘉義電聲電台和](../Page/嘉義市.md "wikilink")[南投草屯電台等五場歌唱比賽](../Page/南投縣.md "wikilink")，都得到歌唱比賽第一名。鄭進一在十歲時，灌錄第一首被收錄合輯的單曲《孝子的願望》。

鄭進一成年後，被張菲邀請共同擔任秀場主持人；兩人在台灣各地共同主持的秀場大受歡迎，鄭進一從此聲名大噪。直到1982年，鄭進一為[林慧萍創作單曲](../Page/林慧萍.md "wikilink")《情懷》，是鄭首次發表幫別人創作的歌曲。往後多年，鄭進一持續幫其他歌手創作詞曲。

1985年，鄭進一加盟「金圓唱片」，不久錄製首張個人[國語專輯](../Page/國語.md "wikilink")《情訴》，花費二年完成專輯的製作，主打歌為〈夢寐以求〉。鄭進一加盟金圓唱片時期發行的另一張國語專輯《今夜，我想喝醉》，主打歌則為〈今夜，我想喝醉〉。後來，鄭進一轉而加入[滾石唱片](../Page/滾石唱片.md "wikilink")，期間推出國語專輯《我的壞男人》，主打歌是〈對於妳們，我永遠心疼〉。

1990年代中期，鄭進一加入「金瓜石音樂」。此時期的鄭進一，推出台語專輯《世間》，主要諷刺台灣政治、社會現況等。至1999年，鄭進一再跳槽到「大信唱片」，不久發行創作EP《勇敢是咱的名》，鄭進一找知名製作人何慶清共同擔綱這張EP的製作人。主打歌《勇敢是咱的名》在這張EP中有兩首演唱版本、一首演奏曲和一首伴奏曲等，一是鄭進一和[王識賢合唱](../Page/王識賢.md "wikilink")，二是[民視演藝人員合唱](../Page/民視.md "wikilink")，三是《勇敢是咱的名》的演奏曲，四是《勇敢是咱的名》的伴奏曲。這張EP是為了[921大地震賑災募款](../Page/921大地震.md "wikilink")，以義賣價[新台幣](../Page/新台幣.md "wikilink")200元發行。
[口白歪歌系列II簡簡單單.jpg](https://zh.wikipedia.org/wiki/File:口白歪歌系列II簡簡單單.jpg "fig:口白歪歌系列II簡簡單單.jpg")

後來，鄭進一規劃推出「口白歪歌」系列專輯時，也開始實行他的「黑名單計劃」。2000年2月時，鄭進一找[賀一航合作推出](../Page/賀一航.md "wikilink")《歪歌系列之口白歌
愛國精神病》，這張專輯收錄的多首歪歌為：《愛國精神病》改編自[江蕙的](../Page/江蕙.md "wikilink")《酒後的心聲》；《音樂世家》是改編[新寶島康樂隊的](../Page/新寶島康樂隊.md "wikilink")《鼓聲若響》；《山頂的瘋狗兄》改自[Leslie
Sarony的](../Page/Leslie_Sarony.md "wikilink")《Fine Alpine
Milkman》；《無你，我尚歡喜》改編[吳晉淮的](../Page/吳晉淮.md "wikilink")《暗淡的月》；《阿母的手段》改編自鄭進一自己的《媽媽請你不通痛》；《賊仔的心聲》改編[金門王與](../Page/金門王.md "wikilink")[李炳輝的](../Page/李炳輝.md "wikilink")《流浪到淡水》；《新男性的復仇》則改編[文夏的](../Page/文夏.md "wikilink")《男性的復仇》。同年7月26日，鄭進一與[澎恰恰](../Page/澎恰恰.md "wikilink")、[許效舜合作再發行](../Page/許效舜.md "wikilink")《[口白歪歌系列2
簡簡單單](../Page/口白歪歌系列2_簡簡單單.md "wikilink")》，專輯詞曲花費新台幣180萬元；其中首波主打歌曲《麻將歌》，是鄭進一花費新台幣40萬元向自己的舊東家金圓唱片買下[陳雷原唱的](../Page/陳雷_\(歌手\).md "wikilink")《歡喜就好》[版權](../Page/版權.md "wikilink")，之後套用自己與知名台語唱片界大亨、「吉馬唱片」老闆[陳維祥改編的歌詞](../Page/陳維祥.md "wikilink")。

2001年1月5日，鄭進一與[林安迪](../Page/林安迪.md "wikilink")、[唐從聖續推出](../Page/唐從聖.md "wikilink")《口白歪歌系列Ⅲ
救國神經病》；雖然這張專輯鄭進一沒有用別具風格的自創歌詞改編原唱歌曲，仍用詼諧搞笑的趣味口白搭配歌詞。

2014年6月，鄭進一和[康康主持的](../Page/康康.md "wikilink")[華視綜藝音樂節目](../Page/華視.md "wikilink")《[周五王見王](../Page/周五王見王.md "wikilink")》開播；同年8月，節目調整播出時段並改名為《[綜藝王見王](../Page/綜藝王見王.md "wikilink")》；同年9月，節目宣告停播，10月初播出最後一集\[4\]。

## 個人生活

2006年6月，鄭進一與陳茱莉結婚。不過卻在2007年，不到一年的時間離婚。2007年5月，鄭進一參與錄製[狄鶯主持的](../Page/狄鶯.md "wikilink")[中視](../Page/中視.md "wikilink")[談話性節目](../Page/談話性節目.md "wikilink")《今夜不流淚》時自稱，他和陳茱莉離婚的真正原因是自己「沒辦法與同一個女人睡太久」。

鄭進一有一獨子鄭倫境。鄭倫境24歲時，曾因觸犯《妨害兵役治罪條例》、《[中華民國刑法](../Page/中華民國刑法.md "wikilink")》傷害罪等罪名入獄一年多。鄭倫境29歲時，與鄭進一相認。2008年2月26日，鄭倫境發表第一張專輯《崩潰》；鄭進一在該專輯發表會上為兒子到場宣傳支持時表示，[周-{杰}-倫是](../Page/周杰倫.md "wikilink")「百年難得一見」，鄭倫境是「九十年難得一見」。鄭進一另有位長相貌似的弟弟鄭進二曾是藝人，本名鄭俊明。

2008年9月28日，鄭進一於「喜悅合一[教會](../Page/教會.md "wikilink")」[受洗](../Page/受洗.md "wikilink")，正式成為[基督徒](../Page/基督徒.md "wikilink")。和他一起受洗的還有藝人[姜厚任](../Page/姜厚任.md "wikilink")，兩人受洗日巧遇[颱風](../Page/颱風.md "wikilink")，姜厚任對鄭進一開玩笑表示：「一定是你的罪孽太重，上天怕教會的水不夠，才會派來強颱徹底洗禮你。」聽聞「鄭進一要受洗」，好友都懷疑自己的耳朵，就連他自己也感到不可思議，他在教友的擁抱祝福下完成受洗儀式。鄭進一坦言，「整個人起雞皮疙瘩，感動到很想哭出來。」鄭進一和前妻陳茱莉離婚後，經歷了人生變化期，受洗的三個月前他在童星出身的[紀寶如引薦下接觸教會後](../Page/紀寶如.md "wikilink")，改掉罵髒話的習慣，也沒有發過脾氣。為了表達自己慎重的決心，鄭進一連夜寫下聖歌〈耶穌我愛你〉，並且在這首歌的旋律下受洗，他表示：「以前的我自視甚高，現在找到一個比我還要厲害的[上帝](../Page/基督教的神.md "wikilink")，當然要聽祂的。」鄭開玩笑地長嘆一口氣說：「心淨凡事皆可行，我做好追隨主的準備。」

## 爭議

2011年2月15日，鄭進一因吸食[安非他命被捕](../Page/安非他命.md "wikilink")。鄭起初稱因為自己想寫一首關於吸毒者的歌曲而想親身體驗，後又改口因為壓力大才會吸毒。

## 師承關係

若僅依鄭進一和相關藝人拜師之承啟關係彙整，關係如下：

  - **師父：**
  - [張菲](../Page/張菲.md "wikilink")

<!-- end list -->

  - **師兄弟：**
  - [胡瓜](../Page/胡瓜.md "wikilink")
  - [康康](../Page/康康.md "wikilink")

<!-- end list -->

  - **徒弟輩：**
  - [澎恰恰](../Page/澎恰恰.md "wikilink") (師承鄭進一)
  - [許效舜](../Page/許效舜.md "wikilink") (師承鄭進一)
  - [王識賢](../Page/王識賢.md "wikilink") (師承鄭進一)
  - [歐漢聲](../Page/歐漢聲.md "wikilink") (師承胡瓜)
  - [羅志祥](../Page/羅志祥.md "wikilink") (師承胡瓜)
  - [浩角翔起](../Page/浩角翔起.md "wikilink") (師承胡瓜)
  - [瑪莉亞](../Page/葉欣眉.md "wikilink") (師承胡瓜)

<!-- end list -->

  - **徒孫輩：**
  - [九孔](../Page/九孔_\(藝人\).md "wikilink") (師承許效舜)
  - [NONO](../Page/NONO.md "wikilink") (師承許效舜)
  - [白雲](../Page/白雲_\(台灣藝人\).md "wikilink") (師承許效舜)
  - \-{[洪都拉斯](../Page/洪勝德.md "wikilink")}- (師承許效舜)
  - [黃鐙輝](../Page/黃鐙輝.md "wikilink") (師承許效舜)
  - [馬國畢](../Page/馬國畢.md "wikilink") (師承許效舜)
  - [蜆仔](../Page/劉俊峰.md "wikilink") (師承許效舜)
  - [黃鴻升](../Page/黃鴻升.md "wikilink") (師承羅志祥)

<!-- end list -->

  - **徒曾孫輩：**
  - [曾子余](../Page/曾子余.md "wikilink") (師承黃鐙輝)

## 作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲 目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/情訴.md" title="wikilink">情訴</a></p></td>
<td style="text-align: left;"><p>1986年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/今夜我想喝醉.md" title="wikilink">今夜我想喝醉</a></p></td>
<td style="text-align: left;"><p>1987年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/再說一次愛我.md" title="wikilink">再說一次愛我</a></p></td>
<td style="text-align: left;"><p>1989年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/我的壞男人.md" title="wikilink">我的壞男人</a></p></td>
<td style="text-align: left;"><p>1990年5月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/青春悲喜曲.md" title="wikilink">青春悲喜曲</a></p></td>
<td style="text-align: left;"><p>1990年12月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/孤家寡人.md" title="wikilink">孤家寡人</a></p></td>
<td style="text-align: left;"><p>1993年6月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>世間</p></td>
<td style="text-align: left;"><p>1995年1月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/咱的故鄉.md" title="wikilink">咱的故鄉</a></p></td>
<td style="text-align: left;"><p>1997年11月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>勇敢是咱的名</p></td>
<td style="text-align: left;"><p>1999年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/舊愛新傷.md" title="wikilink">舊愛新傷</a></p></td>
<td style="text-align: left;"><p>1999年12月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/歪歌系列之口白歌_愛國精神病.md" title="wikilink">愛國精神病</a></p></td>
<td style="text-align: left;"><p>2000年2月1日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/口白歪歌系列2_簡簡單單.md" title="wikilink">簡簡單單</a></p></td>
<td style="text-align: left;"><p>2000年7月27日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/口白歪歌系列3_救國神經病.md" title="wikilink">救國神經病</a></p></td>
<td style="text-align: left;"><p>2001年1月5日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/月娘茶.md" title="wikilink">月娘茶</a></p></td>
<td style="text-align: left;"><p>2001年10月13日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/樂透狂想曲.md" title="wikilink">樂透狂想曲</a></p></td>
<td style="text-align: left;"><p>2003年4月9日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/有你置身邊.md" title="wikilink">有你置身邊</a></p></td>
<td style="text-align: left;"><p>2006年6月9日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>感謝妳。陪我彼多年</p></td>
<td style="text-align: left;"><p>2008年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>望你平安</p></td>
<td style="text-align: left;"><p>2010年</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>幸福支票</p></td>
<td style="text-align: left;"><p>2011年</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 詞曲創作

*因鄭進一詞曲創作作品是不勝枚舉，在這列出的是不含鄭進一收納專輯所列詞曲創作作品。*
{| class="wikitable" |- \! 年份 || 歌曲 || 歌手 || 專輯 || 創作 |-
|1982年||情懷||[林慧萍](../Page/林慧萍.md "wikilink")||《往昔》||詞曲 |-
|1987年||想念的季節||[甄秀珍](../Page/甄秀珍.md "wikilink")||《佔據我的心》||詞曲 |-
|1988年||痴心錯付||[劉德華](../Page/劉德華.md "wikilink")||《回到你身邊》||詞曲 |-
|1991年||已經不必要||[李嘉](../Page/李嘉.md "wikilink")||《迺夜市》||詞曲 |-
|1992年||夫妻之歌||[余天](../Page/余天.md "wikilink")&[李亞萍](../Page/李亞萍.md "wikilink")||《為什麼離開我》||詞曲
|- |rowspan="12|1996年||憨愿頭||許效舜||《憨愿頭》||詞曲 |- ||查埔查某||許效舜||《憨愿頭》||詞曲 |-
||本份||許效舜||《憨愿頭》||詞曲 |- ||每一個黃昏||許效舜||《憨愿頭》||詞曲 |-
||孤孤我一個||許效舜||《憨愿頭》||詞曲 |-
||一台腳踏車||許效舜||《憨愿頭》||詞曲 |-
||永遠的愛||許效舜||《憨愿頭》||詞曲 |-
||已經無必要||許效舜||《憨愿頭》||詞曲 |-
||路邊的路燈||許效舜||《憨愿頭》||詞曲 |-
||思思念念||許效舜||《憨愿頭》||詞曲 |-
||愛作夢的查某囡仔||[南台灣小姑娘](../Page/南台灣小姑娘.md "wikilink")||《愛作夢的查某囡仔》||詞曲
|- ||花與蝶||[真妮](../Page/真妮.md "wikilink")||《採花集》||詞曲 |-
|2001年||[家後](../Page/家後.md "wikilink")||江蕙||《江蕙同名專輯》||詞曲 |}

### 電影

| 年份    | 片名                                                                                                       | 角色  |
| ----- | -------------------------------------------------------------------------------------------------------- | --- |
| 1987年 | 《[大頭兵](../Page/大頭兵.md "wikilink")》                                                                       |     |
| 1987年 | 《大頭兵2 [大頭兵出擊](../Page/大頭兵出擊.md "wikilink")》                                                              |     |
| 1987年 | 《大頭兵3 [阿兵哥](../Page/阿兵哥.md "wikilink")》                                                                  |     |
| 1987年 | 《[福星假期](../Page/福星假期.md "wikilink")》：與[胡瓜一同於](../Page/胡瓜.md "wikilink")[港片中客串](../Page/港片.md "wikilink") |     |
| 1987年 | 《[先生騙鬼](../Page/先生騙鬼.md "wikilink")》                                                                     |     |
| 1988年 | 《[大頭仔](../Page/大頭仔.md "wikilink")》                                                                       |     |
| 1988年 | 《[天才小兵](../Page/天才小兵.md "wikilink")》                                                                     | 郵差  |
| 1988年 | 《[報告典獄長](../Page/報告典獄長.md "wikilink")》                                                                   |     |
| 1988年 | 《[人鬼狐新傳](../Page/人鬼狐新傳.md "wikilink")》                                                                   | 金公子 |
| 1989年 | 《[七隻狐狸八條狗](../Page/七隻狐狸八條狗.md "wikilink")》                                                               |     |
| 1989年 | 《[小人物](../Page/小人物.md "wikilink")》                                                                       |     |

#### 導演

| 年份    | 片名                                         |
| ----- | ------------------------------------------ |
| 1988年 | 《[記得當時年紀小](../Page/記得當時年紀小.md "wikilink")》 |
| 1988年 | 《[七隻狐狸八條狗](../Page/七隻狐狸八條狗.md "wikilink")》 |
| 1988年 | 《[英雄無膽](../Page/英雄無膽.md "wikilink")》       |
| 1990年 | 《[鄭進一的鬼故事](../Page/鄭進一的鬼故事.md "wikilink")》 |
| 1990年 | 《[成功嶺2](../Page/成功嶺2.md "wikilink") 全面出擊》  |

#### 電視劇

| 年份   | 頻道 | 劇名       | 角色  |
| ---- | -- | -------- | --- |
| 1987 | 華視 | 《小人物立大功》 | 李貞凡 |

### 主持

| 時間                                    | 頻道                                   | 節目                                         | 備註                                                                                                                                                                                                |
| ------------------------------------- | ------------------------------------ | ------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1984年11月3日至1984年11月24日，每週六13:00至14:00 | [台視](../Page/台視.md "wikilink")       | 《[綜藝龍虎榜](../Page/綜藝龍虎榜.md "wikilink")》     | 與[胡瓜共同主持](../Page/胡瓜.md "wikilink")。                                                                                                                                                              |
| 1984年12月1日至1985年4月13日，每週六12:50至14:00  | 台視                                   | 《[周末1250](../Page/周末1250.md "wikilink")》   | 與胡瓜共同主持。                                                                                                                                                                                          |
| 1987年4月19日—1987年6月28日                 | [華視](../Page/華視.md "wikilink")       | 《[開心果](../Page/開心果.md "wikilink")》         | 與[楊烈](../Page/楊烈.md "wikilink")、[邢峰共同主持](../Page/邢峰.md "wikilink")。                                                                                                                               |
| 1987年1月1日—1988年5月31日                  | [華視](../Page/華視.md "wikilink")       | 《[連環泡](../Page/連環泡.md "wikilink")》—〈今天事〉單元 | 與[胡瓜共同主持](../Page/胡瓜.md "wikilink")。                                                                                                                                                              |
| 1986年11月2日—1990年5月                    | [華視](../Page/華視.md "wikilink")       | 《[鑽石舞台](../Page/鑽石舞台.md "wikilink")》       | 先與[胡瓜擔任助理主持](../Page/胡瓜.md "wikilink")，主持人[高凌風](../Page/高凌風.md "wikilink")、[夏玲玲請辭後](../Page/夏玲玲.md "wikilink")，與[胡瓜共同擔任主持人](../Page/胡瓜.md "wikilink")。鄭進一請辭之後，改由[陽帆接任](../Page/陽帆.md "wikilink")。 |
| 1990年6月16日—1991年2月2日，每週六20：00～22：00   | [中視](../Page/中視.md "wikilink")       | 《[今宵一路發](../Page/今宵一路發.md "wikilink")》     | 與[賀一航共同主持](../Page/賀一航.md "wikilink")。                                                                                                                                                            |
| 2005年11月—2006年                        | 華視                                   | 《[台灣開心秀](../Page/台灣開心秀.md "wikilink")》     | 接替原主持人之一[余天](../Page/余天.md "wikilink")，與[李亞萍](../Page/李亞萍.md "wikilink")、-{[余筱萍](../Page/余筱萍.md "wikilink")}-、-{[余苑琦](../Page/余苑琦.md "wikilink")}-共同主持。                                           |
| 2005年5月—2008年                         | [八大第一台](../Page/八大第一台.md "wikilink") | 《[台灣望春風](../Page/台灣望春風.md "wikilink")》     | 與[蔡幸娟共同主持](../Page/蔡幸娟.md "wikilink")。                                                                                                                                                            |
| 2014年6月20日—2014年8月3日，每週五20：00～22：00   | 華視                                   | 《[周五王見王](../Page/周五王見王.md "wikilink")》     | 與[康康共同主持](../Page/康康.md "wikilink")，後調整播出時段，名稱改為《綜藝王見王》。                                                                                                                                          |
| 2014年8月3日—2014年10月5日，每週日20：00～22：00   | 華視                                   | 《[綜藝王見王](../Page/綜藝王見王.md "wikilink")》     | 與[康康共同主持](../Page/康康.md "wikilink")。                                                                                                                                                              |

### 廣告

  - [鄭杏泰生物科技](../Page/鄭杏泰生物科技.md "wikilink")「鄭杏泰金佳味救肺散」
  - 華視《抱喜報喜》主題曲作詞、作曲兼主唱

### 演唱會

  - 2017年，鄭進一\[台灣一歌\]台北小巨蛋演唱會\[5\]

## 資料來源

  - 胡瓜、許效舜主持，中視綜藝節目《紅白勝利》：〈我的成長：鄭進一〉

## 外部連結

  -
  -
  -
  -
[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[Category:台灣作曲家](../Category/台灣作曲家.md "wikilink")
[Category:台灣綜藝節目主持人](../Category/台灣綜藝節目主持人.md "wikilink")
[Category:臺灣新教徒](../Category/臺灣新教徒.md "wikilink")
[Category:閩南語流行音樂歌手](../Category/閩南語流行音樂歌手.md "wikilink")
[Category:台灣作詞家](../Category/台灣作詞家.md "wikilink")
[Category:新營人](../Category/新營人.md "wikilink")
[Jin進](../Category/鄭姓.md "wikilink")

1.  [胡瓜搶錢王吳宗憲退位](https://tw.appledaily.com/finance/daily/20041219/21458347/)，[蘋果日報
    (台灣)](../Page/蘋果日報_\(台灣\).md "wikilink")，2004-12-19
2.
3.
4.  [康康獲海外演出
    謝高凌風庇蔭](http://www.chinatimes.com/newspapers/20140930000831-260112/)，[中時電子報](../Page/中時電子報.md "wikilink")，2014-09-30
5.  [鄭進一\[台灣一歌\]台北小巨蛋演唱會](https://www.ticket.com.tw/dm.asp?P1=0000017869)