**埃马纽埃尔·珀蒂**（，），法國人，是一名已退役的足球運動員，司職中場。比堤曾代表法國國家隊贏得[1998年世界杯冠軍](../Page/1998年世界杯.md "wikilink")，並在2000年贏得[歐洲國家杯冠軍](../Page/歐洲國家杯.md "wikilink")。

## 生平

比堤出身於法甲聯賽球會[摩納哥](../Page/摩納哥足球會.md "wikilink")，早在18歲時已被著名教練[雲加簽下](../Page/雲加.md "wikilink")，可謂是雲加一手提攜。比堤在摩納哥期間曾隨球會贏得[歐洲杯賽冠軍杯亞軍](../Page/歐洲杯賽冠軍杯.md "wikilink")，當時他司職後衛。

1997年6月比堤正式轉會[英國球會](../Page/英國.md "wikilink")[阿仙奴](../Page/阿仙奴.md "wikilink")，剛好教練正是雲加。雲加把比堤改造成防守中場，夥拍[韋拉扼守中路](../Page/韋拉.md "wikilink")。比堤剛加盟阿仙奴第一個球季，便力壓曼聯贏得[英超冠軍](../Page/英超.md "wikilink")。之後比堤隨法國國家隊出戰[1998年世界杯](../Page/1998年世界杯.md "wikilink")，那年正是法國贏得冠軍的一屆，決賽中比堤也有射入一球。自此比堤一戰成名，旋即受到其他大球會像[曼聯和](../Page/曼聯.md "wikilink")[皇馬的斟介](../Page/皇馬.md "wikilink")。

兩季後比堤轉會至加泰隆尼亞球會[巴塞羅那](../Page/巴塞隆拿足球俱樂部.md "wikilink")，不過該球季他上陣時間不穩定，甚至要打回後衛。結果2001年轉會[車路士](../Page/車路士.md "wikilink")，一直踢至掛靴。

## 榮譽

  - [法國盃冠軍](../Page/法國盃.md "wikilink"): 1991年
  - [法國甲組足球聯賽冠軍](../Page/法國甲組足球聯賽.md "wikilink"): 1997年
  - [英格蘭超級足球聯賽冠軍](../Page/英格蘭超級足球聯賽.md "wikilink"):
    [1998年](../Page/1997年至1998年英格蘭超級聯賽.md "wikilink")
  - [英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink"): 1998年
  - [英格蘭社區盾冠軍](../Page/英格蘭社區盾.md "wikilink"): 1998年、1999年
  - [世界盃冠軍](../Page/世界盃.md "wikilink")：[1998年](../Page/1998年世界盃足球賽.md "wikilink")
  - [歐洲國家盃冠軍](../Page/歐洲國家盃.md "wikilink")：[2000年](../Page/2000年歐洲國家盃.md "wikilink")

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:摩納哥球員](../Category/摩納哥球員.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:巴塞隆拿球員](../Category/巴塞隆拿球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:1992年歐洲國家盃球員](../Category/1992年歐洲國家盃球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")