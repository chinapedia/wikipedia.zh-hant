[新市区](../Page/新市区_\(乌鲁木齐市\).md "wikilink"){{.w}}[水磨沟区](../Page/水磨沟区.md "wikilink"){{.w}}[头屯河区](../Page/头屯河区.md "wikilink"){{.w}}[达坂城区](../Page/达坂城区.md "wikilink"){{.w}}[米东区](../Page/米东区.md "wikilink"){{.w}}[乌鲁木齐县](../Page/乌鲁木齐县.md "wikilink")

` |group2 = `[`克拉玛依市`](../Page/克拉玛依市.md "wikilink")
` |list2 = `[`克拉玛依区`](../Page/克拉玛依区.md "wikilink")`{{.w}}`[`独山子区`](../Page/独山子区.md "wikilink")`{{.w}}`[`白碱滩区`](../Page/白碱滩区.md "wikilink")`{{.w}}`[`乌尔禾区`](../Page/乌尔禾区.md "wikilink")` `

` |group3 = `[`吐鲁番市`](../Page/吐鲁番市.md "wikilink")
` |list3 = `[`高昌区`](../Page/高昌区.md "wikilink")`{{.w}}`[`鄯善县`](../Page/鄯善县.md "wikilink")`{{.w}}`[`托克逊县`](../Page/托克逊县.md "wikilink")

` |group4 = `[`哈密市`](../Page/哈密市.md "wikilink")
` |list4 = `[`伊州区`](../Page/伊州区.md "wikilink")`{{.w}}`[`伊吾县`](../Page/伊吾县.md "wikilink")`{{.w}}`[`巴-{里}-坤哈萨克自治县`](../Page/巴里坤哈萨克自治县.md "wikilink")

}}

|group4style =text-align: center; |group4 =
[地区](../Page/地区行政公署.md "wikilink") |list4 =
[温宿县](../Page/温宿县.md "wikilink"){{.w}}[库车县](../Page/库车县.md "wikilink"){{.w}}[沙雅县](../Page/沙雅县.md "wikilink"){{.w}}[新和县](../Page/新和县.md "wikilink"){{.w}}[拜城县](../Page/拜城县.md "wikilink"){{.w}}[乌什县](../Page/乌什县.md "wikilink"){{.w}}[阿瓦提县](../Page/阿瓦提县.md "wikilink"){{.w}}[柯坪县](../Page/柯坪县.md "wikilink")

` |group2 = `[`喀什地区`](../Page/喀什地区.md "wikilink")
` |list2 = `[`喀什市`](../Page/喀什市.md "wikilink")`{{.w}}`[`疏附县`](../Page/疏附县.md "wikilink")`{{.w}}`[`疏勒县`](../Page/疏勒县.md "wikilink")`{{.w}}`[`英吉沙县`](../Page/英吉沙县.md "wikilink")`{{.w}}`[`泽普县`](../Page/泽普县.md "wikilink")`{{.w}}`[`莎车县`](../Page/莎车县.md "wikilink")`{{.w}}`[`叶城县`](../Page/叶城县.md "wikilink")`{{.w}}`[`麦盖提县`](../Page/麦盖提县.md "wikilink")`{{.w}}`[`岳普湖县`](../Page/岳普湖县.md "wikilink")`{{.w}}`[`伽师县`](../Page/伽师县.md "wikilink")`{{.w}}`[`巴楚县`](../Page/巴楚县.md "wikilink")`{{.w}}`[`塔什库尔干塔吉克自治县`](../Page/塔什库尔干塔吉克自治县.md "wikilink")` `

` |group3 = `[`和田地区`](../Page/和田地区.md "wikilink")
` |list3 = `[`和田市`](../Page/和田市.md "wikilink")`{{.w}}`[`和田县`](../Page/和田县.md "wikilink")`{{.w}}`[`墨玉县`](../Page/墨玉县.md "wikilink")`{{.w}}`[`皮山县`](../Page/皮山县.md "wikilink")`{{.w}}`[`洛浦县`](../Page/洛浦县.md "wikilink")`{{.w}}`[`策勒县`](../Page/策勒县.md "wikilink")`{{.w}}`[`-{于}-田县`](../Page/于田县.md "wikilink")`{{.w}}`[`民丰县`](../Page/民丰县.md "wikilink")

}}

|group5style = text-align: center; |group5 =
[自治州](../Page/自治州.md "wikilink") |list5 =
[阜康市](../Page/阜康市.md "wikilink"){{.w}}[呼图壁县](../Page/呼图壁县.md "wikilink"){{.w}}[玛纳斯县](../Page/玛纳斯县.md "wikilink"){{.w}}[奇台县](../Page/奇台县.md "wikilink"){{.w}}[吉木萨尔县](../Page/吉木萨尔县.md "wikilink"){{.w}}[木垒哈萨克自治县](../Page/木垒哈萨克自治县.md "wikilink")

` |group2 = `[`博尔塔拉蒙古自治州`](../Page/博尔塔拉蒙古自治州.md "wikilink")
` |list2 = `[`博乐市`](../Page/博乐市.md "wikilink")`{{.w}}`[`阿拉山口市`](../Page/阿拉山口市.md "wikilink")`{{.w}}`[`精河县`](../Page/精河县.md "wikilink")`{{.w}}`[`温泉县`](../Page/温泉县.md "wikilink")

` |group3 = `[`巴音郭楞蒙古自治州`](../Page/巴音郭楞蒙古自治州.md "wikilink")
` |list3 = `[`库尔勒市`](../Page/库尔勒市.md "wikilink")`{{.w}}`[`轮台县`](../Page/轮台县.md "wikilink")`{{.w}}`[`尉犁县`](../Page/尉犁县.md "wikilink")`{{.w}}`[`若羌县`](../Page/若羌县.md "wikilink")`{{.w}}`[`且末县`](../Page/且末县.md "wikilink")`{{.w}}`[`和静县`](../Page/和静县.md "wikilink")`{{.w}}`[`和硕县`](../Page/和硕县.md "wikilink")`{{.w}}`[`博湖县`](../Page/博湖县.md "wikilink")`{{.w}}`[`焉耆回族自治县`](../Page/焉耆回族自治县.md "wikilink")

` |group4 = `[`克孜勒苏柯尔克孜自治州`](../Page/克孜勒苏柯尔克孜自治州.md "wikilink")
` |list4 = `[`阿图什市`](../Page/阿图什市.md "wikilink")`{{.w}}`[`阿克陶县`](../Page/阿克陶县.md "wikilink")`{{.w}}`[`阿合奇县`](../Page/阿合奇县.md "wikilink")`{{.w}}`[`乌恰县`](../Page/乌恰县.md "wikilink")

` |group5 = `[`伊犁哈萨克自治州`](../Page/伊犁哈萨克自治州.md "wikilink")
` |list5 = `[`奎屯市`](../Page/奎屯市.md "wikilink")`{{.w}}`[`霍尔果斯市`](../Page/霍尔果斯市.md "wikilink")`{{.w}}`[`伊宁县`](../Page/伊宁县.md "wikilink")`{{.w}}`[`霍城县`](../Page/霍城县.md "wikilink")`{{.w}}`[`巩留县`](../Page/巩留县.md "wikilink")`{{.w}}`[`新源县`](../Page/新源县.md "wikilink")`{{.w}}`[`昭苏县`](../Page/昭苏县.md "wikilink")`{{.w}}`[`特克斯县`](../Page/特克斯县.md "wikilink")`{{.w}}`[`尼勒克县`](../Page/尼勒克县.md "wikilink")`{{.w}}`[`察布查尔锡伯自治县`](../Page/察布查尔锡伯自治县.md "wikilink")

` |group2 = `[`塔城地区`](../Page/塔城地区.md "wikilink")
` |list2 = `[`塔城市`](../Page/塔城市.md "wikilink")`{{.w}}`[`乌苏市`](../Page/乌苏市.md "wikilink")`{{.w}}`[`额敏县`](../Page/额敏县.md "wikilink")`{{.w}}`[`沙湾县`](../Page/沙湾县.md "wikilink")`{{.w}}`[`托里县`](../Page/托里县.md "wikilink")`{{.w}}`[`裕民县`](../Page/裕民县.md "wikilink")`{{.w}}`[`和布克赛尔蒙古自治县`](../Page/和布克赛尔蒙古自治县.md "wikilink")

` |group3 = `[`阿勒泰地区`](../Page/阿勒泰地区.md "wikilink")
` |list3 = `[`阿勒泰市`](../Page/阿勒泰市.md "wikilink")`{{.w}}`[`布尔津县`](../Page/布尔津县.md "wikilink")`{{.w}}`[`富蕴县`](../Page/富蕴县.md "wikilink")`{{.w}}`[`福海县`](../Page/福海县.md "wikilink")`{{.w}}`[`哈巴河县`](../Page/哈巴河县.md "wikilink")`{{.w}}`[`青河县`](../Page/青河县.md "wikilink")`{{.w}}`[`吉木乃县`](../Page/吉木乃县.md "wikilink")

}} }} |group6style = text-align: center; |group6 = [自治区直辖
县级行政区](../Page/省直辖县级行政区.md "wikilink") |list6 =
[阿拉尔市](../Page/阿拉尔市.md "wikilink"){{.w}}[图木舒克市](../Page/图木舒克市.md "wikilink"){{.w}}[五家渠市](../Page/五家渠市.md "wikilink"){{.w}}[北屯市](../Page/北屯市.md "wikilink"){{.w}}[铁门关市](../Page/铁门关市.md "wikilink"){{.w}}[双河市](../Page/双河市.md "wikilink"){{.w}}[可克达拉市](../Page/可克达拉市.md "wikilink"){{.w}}[昆玉市](../Page/昆玉市.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 85%; |below =
注1：伊犁哈萨克自治州为[副省级自治州](../Page/副省级自治州.md "wikilink")。
注2：石河子市、阿拉尔市、图木舒克市、五家渠市、北屯市、铁门关市、双河市、可克达拉市、昆玉市实行“[师市合一](../Page/师市合一.md "wikilink")”制度，由[新疆生产建设兵团直管](../Page/新疆生产建设兵团.md "wikilink")。
注3：与[印度存在](../Page/印度.md "wikilink")[主權争议的](../Page/主權.md "wikilink")[阿克赛钦现由](../Page/阿克赛钦.md "wikilink")[中华人民共和国政府实际统治](../Page/中华人民共和国政府.md "wikilink")，参见[中印边界问题](../Page/中印边界问题.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[新疆维吾尔自治区乡级以上行政区列表](../Page/新疆维吾尔自治区乡级以上行政区列表.md "wikilink")、[新疆生产建设兵团](../Page/新疆生产建设兵团.md "wikilink")。
 }}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[Category:中华人民共和国二级行政区划导航模板](../Category/中华人民共和国二级行政区划导航模板.md "wikilink")
[\*](../Category/新疆行政区划.md "wikilink")
[新疆行政区划模板](../Category/新疆行政区划模板.md "wikilink")