**沅江**（**沅水**）是流经中国[贵州省](../Page/贵州省.md "wikilink")、[湖南省的](../Page/湖南省.md "wikilink")[河流](../Page/河流.md "wikilink")，属于[洞庭湖水系](../Page/洞庭湖.md "wikilink")。支流还流经[重庆市](../Page/重庆市.md "wikilink")、[湖北省](../Page/湖北省.md "wikilink")。沅水是湖南省的第二大河流，干流全长1033公里（湖南568千米），流域面积89163平方千米，其中位于湖南省51066平方千米，多年平均径流量393.3亿立方米。

## 水系

沅水有南北两源：南源[龙头江为主源](../Page/龙头江.md "wikilink")，源自[贵州省](../Page/贵州省.md "wikilink")[都匀市的](../Page/都匀市.md "wikilink")[云雾山](../Page/云雾山.md "wikilink")；北源[重安江](../Page/重安江.md "wikilink")，源于贵州省[麻江县平月间的大山](../Page/麻江县.md "wikilink")。两源在[凯里市岔河汇合后称](../Page/凯里市.md "wikilink")[清水江](../Page/清水江.md "wikilink")，东流至[台江县纳](../Page/台江县.md "wikilink")[小江河](../Page/小江河.md "wikilink")，至[剑河县纳](../Page/剑河县.md "wikilink")[南哨河](../Page/南哨河.md "wikilink")，至[锦屏县](../Page/锦屏县.md "wikilink")[河口乡纳](../Page/河口乡.md "wikilink")[乌下江](../Page/乌下江.md "wikilink")，至锦屏县[三江镇纳](../Page/三江镇.md "wikilink")[洞河](../Page/洞河.md "wikilink")、[亮江](../Page/亮江.md "wikilink")，于[天柱县](../Page/天柱县.md "wikilink")[瓮洞镇出贵州省入湖南省](../Page/瓮洞镇.md "wikilink")[会同县](../Page/会同县.md "wikilink")[漠滨侗族苗族乡金子村](../Page/漠滨侗族苗族乡.md "wikilink")，至[洪江市](../Page/洪江市.md "wikilink")[托口镇纳](../Page/托口镇.md "wikilink")[渠水](../Page/渠江_\(沅水支流\).md "wikilink")，至[黔城镇与](../Page/黔城镇.md "wikilink")[㵲水](../Page/㵲阳河.md "wikilink")（潕水）汇合,始称**沅水（沅江）**（《湖南省志·地理卷》记载清水江至黔城镇始称沅水，另《怀化地区志》记载民间说法至托口镇始称沅水）\[1\]，至[洪江管理区纳](../Page/洪江管理区.md "wikilink")[巫水后北流至](../Page/洪江_\(沅水支流\).md "wikilink")[溆浦县](../Page/溆浦县.md "wikilink")[江口镇纳](../Page/江口镇_\(溆浦县\).md "wikilink")[溆水](../Page/溆水.md "wikilink")，至[辰溪县纳](../Page/辰溪县.md "wikilink")[辰水](../Page/辰水.md "wikilink")，至[泸溪县纳](../Page/泸溪县.md "wikilink")[武水](../Page/武水.md "wikilink")，至[沅陵县纳](../Page/沅陵县.md "wikilink")[酉水](../Page/酉水_\(沅水支流\).md "wikilink")，至[汉寿县注入](../Page/汉寿县.md "wikilink")[大连湖](../Page/大连湖.md "wikilink")（大洞庭湖的一部分），进而由水道在[沅江市通往](../Page/沅江市.md "wikilink")[南洞庭湖](../Page/南洞庭湖.md "wikilink")。\[2\]\[3\]

### 一级支流

  - [白洋河](../Page/白洋河.md "wikilink")（又名黄石河）
  - [辰水](../Page/辰水.md "wikilink")（又名锦水、锦江、麻阳河）
  - [大伏溪](../Page/大伏溪.md "wikilink")
  - [洞庭溪](../Page/洞庭溪.md "wikilink")（又名池蓬溪、郑水江）
  - [公溪河](../Page/公溪河.md "wikilink")（又名寨头溪。古称贡溪河）
  - [渠水](../Page/渠江_\(沅水支流\).md "wikilink")（又名渠江，古称叙水）
  - [深溪](../Page/深溪_\(沅水支流\).md "wikilink")
  - [巫水](../Page/洪江_\(沅水支流\).md "wikilink")（古称雄溪或熊溪，又名洪江，运水、竹舟江）
  - [武水](../Page/武水.md "wikilink")（又名武溪、卢水）
  - [㵲水](../Page/㵲阳河.md "wikilink")（亦名无水、舞水、巫水）
  - [溆水](../Page/溆水.md "wikilink")（也称双龙江，古称序水、溆川）
  - [怡溪](../Page/怡溪.md "wikilink")（夷水）
  - [酉水](../Page/酉水_\(沅水支流\).md "wikilink")（古名酉溪）

### 二级支流

  - 玉泉溪：全長2.6公里。在湖南省[吉首市矮寨鎮德夯村注入武溪](../Page/吉首.md "wikilink")(峒河)，武水於[瀘溪縣武溪鎮注入沅江](../Page/瀘溪縣.md "wikilink")，沅水在[常德德山注入洞庭湖](../Page/常德.md "wikilink")。玉泉溪，途中有椎牛花柱、玉泉門及玉帶瀑布。這裏巖壁陡峭，峽谷幽深，古木蔥鬱，山竹油綠，屬岩溶峽谷地貌。玉泉溪從這關隘般岩門中擠石而出，清澈晶亮。因湘驛女子的一首詩《[題玉泉溪](../Page/#題詠.md "wikilink")》而聞名。

## 水电站

五强溪水电站位于沅水下游湖南省[沅陵县境内](../Page/沅陵.md "wikilink")，是沅水干流上的关键性综合利用枢纽工程，也是湖南省最大的水电站，是一个以发电为主兼有防洪、航运等综合效益的工程。

## 流域[县市](../Page/县级行政区.md "wikilink")

  - 贵州省
      - [黔南布依族苗族自治州](../Page/黔南布依族苗族自治州.md "wikilink")
          - [都匀市](../Page/都匀市.md "wikilink")、[福泉市](../Page/福泉市.md "wikilink")
      - [黔东南苗族侗族自治州](../Page/黔东南苗族侗族自治州.md "wikilink")
          - [麻江县](../Page/麻江县.md "wikilink")、[丹寨县](../Page/丹寨县.md "wikilink")、[凯里市](../Page/凯里市.md "wikilink")、[黄平县](../Page/黄平县.md "wikilink")、[施秉县](../Page/施秉县.md "wikilink")、[镇远县](../Page/镇远县.md "wikilink")、[岑巩县](../Page/岑巩县.md "wikilink")、[剑河县](../Page/剑河县.md "wikilink")、[台江县](../Page/台江县.md "wikilink")、[雷山县](../Page/雷山县.md "wikilink")、[黎平县](../Page/黎平县.md "wikilink")、[锦屏县](../Page/锦屏县.md "wikilink")、[天柱县](../Page/天柱县.md "wikilink")
  - 湖南省
      - [怀化市](../Page/怀化市.md "wikilink")
          - [靖州](../Page/靖州.md "wikilink")、[会同](../Page/会同.md "wikilink")、[洪江](../Page/洪江.md "wikilink")、[中方](../Page/中方.md "wikilink")、[溆浦](../Page/溆浦.md "wikilink")、[辰溪](../Page/辰溪.md "wikilink")、[沅陵](../Page/沅陵.md "wikilink")
      - [湘西土家族苗族自治州](../Page/湘西土家族苗族自治州.md "wikilink")
          - [泸溪](../Page/泸溪.md "wikilink")
      - [常德市](../Page/常德市.md "wikilink")
          - [桃源县和](../Page/桃源县.md "wikilink")[汉寿县](../Page/汉寿县.md "wikilink")
  - 重庆市
  - 湖北省

## 題詠

<poem>

  - 題玉泉溪（唐·湘驛女子）

紅葉醉秋色，碧溪彈夜弦。 佳期不可再，風雨杳如年。 </poem>

## 参考文献

[Category:湖南河流](../Category/湖南河流.md "wikilink")

1.
2.
3.