[Spratly_with_flags.jpg](https://zh.wikipedia.org/wiki/File:Spratly_with_flags.jpg "fig:Spratly_with_flags.jpg")

**费信岛**（,
），位於[南沙群島中的島嶼](../Page/南沙群島.md "wikilink")，目前由[菲律賓控制](../Page/菲律賓.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")、[中華人民共和國及](../Page/中華人民共和國.md "wikilink")[越南社會主義共和國等國亦聲稱對此島擁有主權](../Page/越南社會主義共和國.md "wikilink")。

## 歷史

  - 中國漁民向稱「羅孔仔」。\[1\]
  - 1935年，中華民國水陸地圖審查委員會公佈的名稱為「扁島」。\[2\]
  - 1939年，日本佔領南沙群島，把南沙群島為命名為「[新南群島](../Page/新南群島.md "wikilink")」，並把费信岛命名為「龜甲島」。\[3\]
  - 1947年，中華民國內政部方域司公佈的標準名稱為「费信岛」，以紀念随同[郑和下西洋的随员](../Page/郑和下西洋.md "wikilink")[费信](../Page/费信.md "wikilink")。\[4\]
  - 1970年，菲律賓佔領费信岛。\[5\]

## 地理位置

  - 在[马欢岛以北](../Page/马欢岛.md "wikilink")5[海浬處](../Page/海浬.md "wikilink")。

## 地質地形

  - 島形呈呈長條形。最高點[海拔](../Page/海拔.md "wikilink")12.5米。
  - 全島的長度為230米，總面積約0.04[平方公里](../Page/平方公里.md "wikilink")，由於島嶼面積過小，菲律賓軍方在島上沒有駐軍。

## 島上狀況

### 水源

  - 無地下水源。

### 建筑

  - 岛上有一个高脚屋。

### 人口

  - 目前為無人島，附近馬歡島的[菲律賓駐軍會在該島海域定期巡邏](../Page/菲律賓.md "wikilink")。

## 参考书目

  - 《南海诸岛地名资料汇编》

[Category:亚洲无人岛](../Category/亚洲无人岛.md "wikilink")
[Category:南沙群島](../Category/南沙群島.md "wikilink")
[Category:以人名命名的中国地名](../Category/以人名命名的中国地名.md "wikilink")

1.

2.

3.

4.
5.