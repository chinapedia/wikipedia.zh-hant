**沙耆**（1914年—2005年），当代[中国著名油画家](../Page/中国.md "wikilink")。[浙江](../Page/浙江.md "wikilink")[鄞县人](../Page/鄞县.md "wikilink")。被誉为“中国的[梵高](../Page/梵高.md "wikilink")”。

## 生平

[Former_residence_of_Sha_Qi,_2017-03-25_01.jpg](https://zh.wikipedia.org/wiki/File:Former_residence_of_Sha_Qi,_2017-03-25_01.jpg "fig:Former_residence_of_Sha_Qi,_2017-03-25_01.jpg")
沙耆于1914年生于浙江[宁波鄞县沙村](../Page/宁波.md "wikilink")（今属[宁波市](../Page/宁波市.md "wikilink")[鄞州区](../Page/鄞州区.md "wikilink")），字吉留名引年，后由[书法家](../Page/书法家.md "wikilink")[沙孟海取名耆](../Page/沙孟海.md "wikilink")。1925年，随父亲到[上海](../Page/上海.md "wikilink")，就读于宁波同乡会小学。1929年，考入上海昌明艺术专科学校。1932年，就读于上海美术专科学校。1933年，因政治原因被捕，入狱一年。出狱后入读[杭州西湖艺专](../Page/杭州.md "wikilink")（今[中国美术学院](../Page/中国美术学院.md "wikilink")），并入[中央大学艺术系学习](../Page/中央大学.md "wikilink")，师从著名[画家](../Page/画家.md "wikilink")[徐悲鸿](../Page/徐悲鸿.md "wikilink")。1937年，经徐悲鸿推荐，留学[比利时国立皇家美术学院](../Page/比利时.md "wikilink")，师从比利时画家巴斯天（A.
Bastien）。1938年，获比利时皇家美术学院授予的金质奖章。1939年，以最优异成绩毕业并获“优秀美术金质奖章”。授奖典礼在比利时[布鲁塞尔美术宫举行](../Page/布鲁塞尔.md "wikilink")，由布鲁塞尔市长马可斯亲自授予，引起各方关注。

1940年，与[毕加索等](../Page/毕加索.md "wikilink")[欧洲著名画家一起参加比利时布鲁塞尔阿特利亚蒙](../Page/欧洲.md "wikilink")（Atriome）展览会。1940年6月，又与[匈牙利画家德立克](../Page/匈牙利.md "wikilink")（Trink）等画家在督阿崇道（Toisondor）会场举办展览会，均引起轰动。后多次在毕底格拉地（PetieGalerie）美术馆等地举办个人画展。1942年，在毕底格拉美术馆参展时，其名作《吹笛女》被参观展览的比利时王国皇后购藏。1945年底，旅行欧洲（[英国](../Page/英国.md "wikilink")、[荷兰等国](../Page/荷兰.md "wikilink")）写生。1946年，經确诊为[思覺失調症](../Page/思覺失調症.md "wikilink")，回国治疗养病。徐悲鸿聘其为北平艺术专科学校教授而未成。

1983年，浙江省博物馆、上海油画雕塑创作院和北京首都博物馆先后举办“沙耆画展”，其《自画像》等作品由[中国美术馆收藏](../Page/中国美术馆.md "wikilink")。1983年，被聘为浙江省文史馆馆员。1984年，被聘为上海文史馆馆员。2001年3月，[中国美术馆为沙耆举办](../Page/中国美术馆.md "wikilink")“沙耆七十年作品回顾展”。2005年春，病逝于上海，享年91岁。

## 参考

[回到美术史中的沙耆——水天中谈沙耆](https://web.archive.org/web/20060827095634/http://arts.tom.com/1004/2005719-22291.html)

[沙耆年表](http://www.cnarts.net/cweb/artw/arts/index.asp?mid=314&kind=all&mpage=2)

[S沙](../Category/中国画家.md "wikilink") [S沙](../Category/鄞县人.md "wikilink")
[S](../Category/鄞县沙氏.md "wikilink") [S](../Category/沙姓.md "wikilink")
[S沙](../Category/1914年出生.md "wikilink")
[S沙](../Category/2005年逝世.md "wikilink")