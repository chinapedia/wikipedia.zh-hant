**王志文**（），[中国男](../Page/中国.md "wikilink")[演员](../Page/演员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")、[主持人](../Page/主持人.md "wikilink")，生于[上海](../Page/上海.md "wikilink")，祖籍[浙江](../Page/浙江.md "wikilink")[奉化](../Page/奉化.md "wikilink")。
因在电视剧《[南行记](../Page/南行记.md "wikilink")》中塑造青年艾芜崭露头角，和[江珊主演的](../Page/江珊.md "wikilink")[电视剧](../Page/电视剧.md "wikilink")《[过把瘾](../Page/过把瘾.md "wikilink")》让王志文红遍大江南北，他在[电影](../Page/电影.md "wikilink")、电视剧以及[流行音乐上均有建树](../Page/流行音乐.md "wikilink")。

## 经历

13岁时父亲因车祸去世。高中时就读于上海市甘泉中学。中学时代的王志文喜爱[滑稽戏](../Page/滑稽戏.md "wikilink")，[姚慕双](../Page/姚慕双.md "wikilink")、[周柏春](../Page/周柏春.md "wikilink")、[杨华生等是他的偶像](../Page/杨华生.md "wikilink")，他能说多种方言\[1\]。1984年报考[北京电影学院时被车撞成骨折](../Page/北京电影学院.md "wikilink")，不过最终顺利考入该校表演系，毕业后分配在[中央戏剧学院研究所工作](../Page/中央戏剧学院.md "wikilink")。\[2\]\[3\]1991年，因电视剧《南行记》的青年艾芜而崭露头角。1992年，因电视剧《皇城根儿》的王喜一角而成名。1993年，凭借电视剧《过把瘾》中的方言一角而获第14届中国电视剧飞天奖优秀男主角奖。1994年，推出个人首张专辑《想说爱你不容易》。1996年，在电视剧《像春天一样》中饰演肖亮一角而获得第6届上海电视节最佳男演员奖。1999年，凭借话剧《归来兮》而获第9届白玉兰戏剧表演艺术奖最佳男演员。2003年，凭借电影《和你在一起》而获得第26届大众电影百花奖最佳男配角奖。2010年，因电视剧《手机》而获搜狐娱乐夏季电视剧网络盛典最佳男演员奖。2014年，凭借电视剧《大丈夫》而获得第20届上海电视节白玉兰奖最佳男演员。2015年凭《[黃金時代](../Page/黃金時代_\(2014年電影\).md "wikilink")》里的[鲁迅一角赢得](../Page/鲁迅.md "wikilink")[亚洲电影大奖最佳男配角](../Page/亚洲电影大奖.md "wikilink")。

## 作品

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1985年</p></td>
<td><p>红军留下的儿子</p></td>
<td><p>林秉苏</p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/两宫皇太后.md" title="wikilink">两宫皇太后</a></p></td>
<td><p>同治皇帝</p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/秘密采访.md" title="wikilink">秘密采访</a></p></td>
<td><p>麦克锋</p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/大决战之辽沈战役.md" title="wikilink">大决战之辽沈战役</a><br />
<a href="../Page/大决战之平津战役.md" title="wikilink">大决战之平津战役</a></p></td>
<td><p>黄志勇</p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p>风雨故园</p></td>
<td><p>子凌公公</p></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p>周末情人</p></td>
<td><p>拉拉</p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p>家丑</p></td>
<td><p>朱辉正</p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p>红粉</p></td>
<td><p>老浦</p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p>飞虎队/铁道游击队</p></td>
<td><p>张兰</p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p>紧急救助</p></td>
<td><p>公冶弘</p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/风云之雄霸天下.md" title="wikilink">风云之雄霸天下</a></p></td>
<td><p>龙袖</p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/荆轲刺秦王.md" title="wikilink">荆轲刺秦王</a></p></td>
<td><p>长信侯</p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/半生缘.md" title="wikilink">半生缘</a></p></td>
<td><p>张豫瑾</p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p>超导</p></td>
<td><p>贝小知</p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p>小说/诗意的年代</p></td>
<td><p>赵子轩</p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p>说出你的秘密</p></td>
<td><p>李国强</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>谁说我不在乎</p></td>
<td><p>老曹</p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/和你在一起.md" title="wikilink">和你在一起</a></p></td>
<td><p>江老师</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/芬妮的微笑.md" title="wikilink">芬妮的微笑</a></p></td>
<td><p>马云龙</p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>天黑请闭眼</p></td>
<td><p>马魁</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/枪林恋曲.md" title="wikilink">枪林恋曲</a></p></td>
<td><p>华头</p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/美人依旧.md" title="wikilink">美人依旧</a></p></td>
<td><p>黄先生</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/愛．作戰.md" title="wikilink">愛．作戰</a></p></td>
<td><p>华</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/求求你表扬我.md" title="wikilink">求求你表扬我</a></p></td>
<td><p>古国歌</p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/墨攻_(电影).md" title="wikilink">墨攻</a></p></td>
<td><p>梁王</p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/兄弟_(2007年電影).md" title="wikilink">兄弟之生死同盟</a></p></td>
<td><p>谭信天</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/两个人的房间.md" title="wikilink">两个人的房间</a></p></td>
<td><p>民警</p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/麦田_(电影).md" title="wikilink">麦田</a></p></td>
<td><p>匪首</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/风声_(电影).md" title="wikilink">风声</a></p></td>
<td><p>王田香</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>杀戮之地</p></td>
<td><p>外交官</p></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/黃金時代_(2014年電影).md" title="wikilink">黃金時代</a></p></td>
<td><p><a href="../Page/魯迅.md" title="wikilink">魯迅</a></p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/北京遇上西雅圖之不二情書.md" title="wikilink">北京遇上西雅圖之不二情書</a></p></td>
<td><p>鄧先生</p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/相爱相亲.md" title="wikilink">相爱相亲</a></p></td>
<td><p>民政局辦事員</p></td>
</tr>
<tr class="even">
<td><p>待上映</p></td>
<td><p><a href="../Page/最长一枪.md" title="wikilink">最长一枪</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>待上映</p></td>
<td><p><a href="../Page/英格力士.md" title="wikilink">英格力士</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 电视剧

  - 《[马鲁他](../Page/马鲁他.md "wikilink")》（上世纪80年代后期，饰 上世纪40年代大学生）
  - 《[伦敦启示录](../Page/伦敦启示录.md "wikilink")》（1987年，饰 茕茕）
  - 《[正午阳光](../Page/正午阳光.md "wikilink")》（1989年，饰 程亮平）
  - 《[柳亚子](../Page/柳亚子.md "wikilink")》（1989年，饰 柳亚子
  - 《[南行记](../Page/南行记.md "wikilink")》（1991年，饰 青年艾芜）
  - 《[皇城根儿](../Page/皇城根儿.md "wikilink")》（1992年，饰 王喜）
  - 《[吴敬梓](../Page/吴敬梓.md "wikilink")》（1992年，饰 吴敬梓）
  - 《[过把瘾](../Page/过把瘾.md "wikilink")》（1993年，饰 方言）
  - 《[杀人街的故事](../Page/杀人街的故事.md "wikilink")》（1993年，饰 苏建）
  - 《[东边日出西边雨](../Page/东边日出西边雨.md "wikilink")》 （1994年，饰 陆建平）
  - 《[我爱我家之生活之友](../Page/我爱我家之生活之友.md "wikilink")》（1994年，饰 耶稣同志）
  - 《[无悔追踪](../Page/无悔追踪.md "wikilink")》（1995年，饰 冯静波）
  - 《[大屋的丫环们](../Page/大屋的丫环们.md "wikilink")》（1995年，饰 少爷）
  - 《[像春天一样](../Page/像春天一样.md "wikilink")》（1996年，饰 肖亮）
  - 《[走出凯旋门](../Page/走出凯旋门.md "wikilink")》（1997年，饰 韩涵）
  - 《[闯上海](../Page/闯上海.md "wikilink")》（1997年，饰 托尼）
  - 《[白板](../Page/白板.md "wikilink")》（1998年，饰 建川）
  - 《[找不着北](../Page/找不着北.md "wikilink")》（1998年，饰 舒凡）
  - 《[刑警本色](../Page/刑警本色.md "wikilink")》（1999年，饰 萧文）
  - 《[让爱作主](../Page/让爱作主.md "wikilink")》（2000年，饰 耿林）
  - 《[南国大案之人兽大战](../Page/南国大案之人兽大战.md "wikilink")》（2000年，饰 张成德）
  - 《[黑冰](../Page/黑冰_\(电视剧\).md "wikilink")》（2001年，饰 郭小鹏）
  - 《[DA师](../Page/DA师.md "wikilink")》（2002年，饰 龙凯峰）
  - 《[天之云地之雾](../Page/天之云地之雾.md "wikilink")》（2002年，饰 乔小龙）
  - 《[国家干部](../Page/国家干部.md "wikilink")》（2005年，饰 夏中民）
  - 《[龙虎人生](../Page/龙虎人生.md "wikilink")》（2005年，饰 杨冠雨）
  - 《[天道](../Page/天道.md "wikilink")》（2006年，饰 丁元英）
  - 《[宽恕](../Page/宽恕.md "wikilink")》（2007年，饰 肖一航）
  - 《[幸福还有多远](../Page/幸福还有多远.md "wikilink")》（2008年，饰 吴天亮）
  - 《[手机](../Page/手机_\(电视剧\).md "wikilink")》（2010年，饰 严守一）
  - 《[感動生命](../Page/感動生命_\(2011年电视剧\).md "wikilink")》(2011年，饰 司馬博銘)
  - 《青瓷》（2012年，饰 张仲平）
  - 《[大丈夫](../Page/大丈夫_\(2014年电视剧\).md "wikilink")》(2014年，饰 歐陽劍)
  - 《[待嫁老爸](../Page/待嫁老爸.md "wikilink")》(2015年，饰 周东风)
  - 《[一树桃花开](../Page/一树桃花开.md "wikilink")》(2017年，饰 盛茂林 )
  - 《[归去来](../Page/归去来.md "wikilink")》(2018年，饰 书望)
  - 《[风雨送春归](../Page/风雨送春归.md "wikilink")》(待播，饰 赵达声 )
  - 《[国家行动](../Page/国家行动_\(2017年电视剧\).md "wikilink")》(待播，饰 )

### 话剧

  - 《楼上的玛金》（1994年，饰雨果，编剧张献，导演余莅杭）
  - 《归来兮》（1997年，饰文华生，编剧王俭，导演卢昂）

### 配音

  - 《[福娃](../Page/福娃.md "wikilink")》（动画片，52集，2007年，配战神阿瑞斯，导演刘纯燕）

### 歌曲

  - 《再过把瘾》（1994年，北京京文音像公司）
  - 《想说爱你不容易》（1995年，北京京文音像公司）
  - 《宝贝》（1995年，北京京文音像公司）

## 荣誉

  - 1993年荣获第二届中国四川国际电视节“[金熊猫奖](../Page/金熊猫奖.md "wikilink")”最佳男主角（《南行记》中饰艾芜）
  - 1994年荣获第十一届大众电视“[金鹰奖](../Page/金鹰奖.md "wikilink")”最佳男配角（《皇城根儿》饰王喜）
  - 1994年荣获第十四届全国电视剧“[飞天奖](../Page/飞天奖.md "wikilink")”优秀男主角（《过把瘾》饰方言）
  - 1995年荣获第五届中国电影表演艺术学会奖金凤凰奖（《红粉》饰老浦）
  - 1997年荣获中国十大优秀电视演员（1995-1996年度最佳优秀演员）
  - 1998年荣获上海国际电视节最佳男演员（《像春天一样》饰肖亮）
  - 1998年荣获上海白玉兰奖话剧最佳男演员（《归来兮》饰文华生）
  - 2000年荣获第十八届中国电视金鹰奖观众最喜爱男主角奖（《刑警本色》饰肖文）
  - 2001年中国首届双十佳优秀演员奖
  - 2002年荣获第八届莫斯科“爱之恋电影节”最佳男主角（《芬妮的微笑》饰马云龙）
  - 2002年获第十一届中国金鸡百花电影节"[金鸡奖](../Page/金鸡奖.md "wikilink")"最佳男配角奖（《和你在一起》饰江老师）
  - 2003年荣获第十二届中国金鸡百花电影节"[百花奖](../Page/百花奖.md "wikilink")"最佳男配角奖（《和你在一起》饰江老师）
  - 2003年荣获第九届中国电影表演艺术学会奖金凤凰奖（《和你在一起》饰江老师）
  - 2005年入选[中国电影百年百位优秀演员](../Page/中国电影百年百位优秀演员.md "wikilink")
  - 2014年第二十屇上海电视节人气男演员奖《[大丈夫](../Page/大丈夫.md "wikilink")》

## 爭議事件

  - 曾因电影《芬妮的微笑》而陷入官司。\[4\]
  - 2009年6月12日与友人饮酒后，在驾车离去时对进行拍摄的记者[竖中指](../Page/竖中指.md "wikilink")。\[5\]
  - 2012年10月25日，王志文在上海市长宁区被警方认定酒后驾车\[6\]。

## 参考

<references />

## 外部链接

  - [锐意坦荡王志文俱乐部](http://www.wangzhiwen.net)

[Category:中国电影男演员](../Category/中国电影男演员.md "wikilink")
[Category:中国电视男演员](../Category/中国电视男演员.md "wikilink")
[Category:北京电影学院校友](../Category/北京电影学院校友.md "wikilink")
[Category:奉化人](../Category/奉化人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:寧波裔上海人](../Category/寧波裔上海人.md "wikilink")
[Z志](../Category/王姓.md "wikilink")
[Category:亞洲電影大獎最佳男配角得主](../Category/亞洲電影大獎最佳男配角得主.md "wikilink")
[Category:上海演员](../Category/上海演员.md "wikilink")
[Category:酒醉駕駛](../Category/酒醉駕駛.md "wikilink")

1.  [王志文：烂剧、雷剧不找我，确实不喜欢这些剧](http://news.youth.cn/yl/201406/t20140614_5364931_1.htm).中国青年网.

2.  [王志文：酒香不怕巷子深](http://ent.hunantv.com/t/20081215/139219.html)金鹰网
    2008年12月15日

3.  [揭王志文：十三岁那年父亲去世，高考前被撞骨折](http://www.chinanews.com/yl/2012/11-03/4298997.shtml).中国新闻网.

4.  [一句话打了两年官司
    王志文终赢得<芬妮>官司](http://www.southcn.com/ent/celeb/wangzhiwen/news/200611210129.htm)
    南方网

5.  [王志文竟满身酒气竖中指狠瞪记者](http://news.cctv.com/society/20090614/100330.shtml)
    CCTV

6.