**D小調第三號交響曲**（）乃[古斯塔夫·馬勒的第三首](../Page/古斯塔夫·馬勒.md "wikilink")[交響曲作品](../Page/交響曲.md "wikilink")，於1893年至1896年間寫成。該首涉及龐大陣容的[音樂](../Page/音樂.md "wikilink")，需要較長時間演奏，奏完约需耗时90至100分鐘。

## 結構

此曲最終定作以下之章節：

1.  剛強·果斷()
2.  小步舞曲速度·中板()
3.  自在地·詼諧地·從容不迫()
4.  甚緩板·神秘地·從頭至尾極弱奏()
5.  爽朗的速度及盡情表達()
6.  緩板·祥和地·傷感地()

此段涉及稍為長至30分鐘(有時40分鐘)的首段樂章，形成該交響曲之第一部分。而第二部分歷時60至70分鐘，包含其餘五部分的樂章。

馬勒在首四套交響曲之每章節當中，亦以[標題音樂之主題標示涵意](../Page/標題音樂.md "wikilink")。第三號交響曲所述之標題如下：

1.  「牧神甦醒，夏季昂首闊步地邁進」
2.  「草地上花兒跟我說」
3.  「林中鳥獸跟我說」
4.  「人跟我說」
5.  「天使跟我說」
6.  「愛跟我說」

上述標題在1898年出版交響曲時被予以刪除。

原訂出現之第七標題「小孩跟我說」，卻於其後被刪去，但後來又出現於[第四號交響曲當中並作為最終樂章](../Page/第四號交響曲_\(馬勒\).md "wikilink")。

此交響曲由於樂段極廣及其人物、構造相差異之緣故，跟交響曲極不相似，然而又在馬勒眾作品當中自成一格。以「跟該首交響曲一樣概念奇異」見稱的首段樂章，描繪出原始時期[牧神甦醒為引子](../Page/牧神.md "wikilink")，堪稱19世紀最長奏鳴曲式單一樂章；該處又有一個極美妙的[次中音長號所形成之清晰音節](../Page/長號.md "wikilink")，並於後期形成；由此處開始(包括明顯地出現之長度)，皆每處出現創新之地方。以高座見稱的無數個單獨鼓聲以歷時30秒形成韻律式的段落，與此同時，8個[圓號為開首段重新出現](../Page/圓號.md "wikilink")，並連同於發展部出現的各主題亦在再現部出現，最後在最高漲的氣氛下結束。

第三樂段透過馬勒早期的音樂《夏日換班記》(Ablösung im
Sommer)潛移默化，形成與布穀鳥有關之片段；第四樂章以「午夜之歌」為題，取材自尼采著作《查拉圖斯特拉如是說》，而第五樂章則是根據《少年魔號》改編而成的「天使跟我說」。

最後一個樂章以最慢板開始，五部絃樂一直都佔著非常重要的位置，直至轉拍子時[雙簧管的加入才打破絃樂壟斷的情況](../Page/雙簧管.md "wikilink")，代表上帝的愛的主題旋律在[圓號和其他管樂中不斷重覆](../Page/圓號.md "wikilink")，氣氛漸漸推向高潮，長大的結尾，樂隊一直維持在[D大調的](../Page/D大調.md "wikilink")[主和絃及](../Page/主和絃.md "wikilink")[屬和絃之間徘徊](../Page/屬和絃.md "wikilink")，加上兩部定音鼓的輪流交替以維持高潮，最後引到最高的境界中完結全首交響曲。

## 娜塔麗·鮑雅-里希納與馬勒第三交響曲之關係

馬勒在創作第三交響曲期間，曾與一位名叫[娜塔麗·鮑雅-里希納](../Page/娜塔麗·鮑雅-里希納.md "wikilink")(Natalie
Bauer-Lechner)的[中提琴手成為要好的朋友](../Page/中提琴.md "wikilink")。創作過程當中，馬勒並無透露創作內容予公眾，反而將之透露予鮑雅-里希納。她亦為第三交響曲作私人記錄，由此可見她對馬勒第三交響曲有著極深遠的影響。

## 配器法

第3號交響曲採取下列的配器形式：

**[木管樂器](../Page/木管樂器.md "wikilink")**：4[長笛](../Page/長笛.md "wikilink")(全部兼任短笛)、4[雙簧管](../Page/雙簧管.md "wikilink")(第4雙簧管兼任[英國管](../Page/英國管.md "wikilink"))、2E調[高音單簧管](../Page/高音單簧管.md "wikilink")(第2高音單簧管兼任第4單簧管)、4[單簧管](../Page/單簧管.md "wikilink")、B調[低音單簧管](../Page/低音單簧管.md "wikilink")
(由第3單簧管兼任)、4[巴松管](../Page/巴松管.md "wikilink")、[低音巴松管](../Page/低音巴松管.md "wikilink")(第4巴松管兼任)

**[銅管樂器](../Page/銅管樂器.md "wikilink")**：8[圓號](../Page/圓號.md "wikilink")(F調)、4[小號](../Page/小號.md "wikilink")(F調)、4[長號](../Page/長號.md "wikilink")、[大號](../Page/大號.md "wikilink")

**[敲擊樂器](../Page/敲擊樂器.md "wikilink")**：2[定音鼓](../Page/定音鼓.md "wikilink")、2[鐵片琴](../Page/鐘琴.md "wikilink")、[鈴鼓](../Page/鈴鼓.md "wikilink")、[鑼](../Page/鑼.md "wikilink")、[三角鐵](../Page/三角鐵.md "wikilink")、[鈸](../Page/鈸.md "wikilink")、[小鼓](../Page/小鼓.md "wikilink")、[大鼓](../Page/大鼓.md "wikilink")

**樂隊外樂器**：[小軍鼓](../Page/小鼓.md "wikilink")(於第一樂章再現部前演奏)、B調[郵號](../Page/郵號.md "wikilink")(只於第三樂章出現)、[管鐘](../Page/管鐘.md "wikilink")(只於第五樂章出現，置於合唱團內)

**[合唱部分](../Page/合唱.md "wikilink")**：[女低音](../Page/女低音.md "wikilink")[獨唱](../Page/獨奏.md "wikilink")(第四、五樂章)、女聲及童聲合唱(第五樂章)

**[絃樂器](../Page/絃樂器.md "wikilink")**：第一[小提琴](../Page/小提琴.md "wikilink")、第二小提琴、[中提琴](../Page/中提琴.md "wikilink")、[大提琴](../Page/大提琴.md "wikilink")、[低音提琴](../Page/低音提琴.md "wikilink")、2[豎琴](../Page/豎琴.md "wikilink")

## 歌詞(第四及第五樂章，[德文原詞連](../Page/德文.md "wikilink")[中文翻譯](../Page/中文.md "wikilink"))

### 第四樂章

<table>
<tbody>
<tr class="odd">
<td><p><strong>"Zarathustras Mitternachtslied"</strong><br />
(<em>aus</em> Also sprach Zarathustra <em>von Nietzsche</em>)</p>
<p><strong>ALTSOLO</strong>：</p>
<p>O Mensch! Gib acht!<br />
Was spricht die tiefe Mitternacht?<br />
Ich schlief,ich schlief<br />
Aus tiefem Traum bin ich erwacht!<br />
Die Welt ist tief!<br />
Und tiefer als der Tag gedacht.<br />
Tief ist ihr Weh!<br />
Lust tiefer noch als Herzeleid.<br />
Weh spricht: Vergeh!<br />
Doch alle Lust will Ewigkeit<br />
Will, tiefe, tiefe Ewigkeit!<br />
</p></td>
<td><p><strong>「查拉圖斯特拉午夜之歌」</strong><br />
(取材自<a href="../Page/尼采.md" title="wikilink">尼采著作</a>「<a href="../Page/查拉圖斯特拉如是說.md" title="wikilink">查拉圖斯特拉如是說</a>」)</p>
<p><strong>(<a href="../Page/女中音.md" title="wikilink">女中音獨唱</a>)</strong></p>
<p>人哪，聽著！<br />
深沉的午夜在說甚麼？<br />
我睡了，我睡了—<br />
我從深沉的夢裡醒來；<br />
世界是深沉的，<br />
比白晝所想的還要深沉。<br />
深沉是世界的痛苦；<br />
快樂比起悲痛更深更沉；<br />
痛苦在說：「走吧！」<br />
可惜快樂渴望永恆—<br />
深沉，深沉的永恆。<br />
</p></td>
</tr>
</tbody>
</table>

### 第五樂章

<table>
<tbody>
<tr class="odd">
<td><p><strong>"Es sungen drei Engel"</strong><br />
(<em>aus</em> Des Knaben Wunderhorn)</p>
<p><strong>FRAUEN- UND KNABENCHOR, ALTOSOLO</strong>:</p>
<p>(Bimm bamm!)</p>
<p>Es sungen drei Engel einen süßen Gesang,<br />
Mit Freuden es selig in den Himmel klang;<br />
Sie jauchzten fröhlich auch dabei,<br />
Daß Petrus sei von Sünden frei.<br />
Und als der Herr Jesus zu Tishe saß,<br />
Mit seinen zwölf Jüngern das Abendmahl aß.<br />
Da sprach der Herr Jesus; "Was stehst du denn hier?<br />
Wenn ich dich anseh' so Weinest de mir."<br />
"Ach, sollt' ich nicht weinen, du gütiger Gott;<br />
Ich hab' übertreten die zehn Gebot;<br />
Ich gehe und weine ja biterlich,<br />
Ach komm und erbarme dich über mich!"<br />
"Hast du denn übertreten die zehn Gebot,<br />
So fall auf die Knie und bete zu Gott,<br />
Liebe nur Gott in alle Zeit,<br />
So wirst erlangen die himmlische Freud'!"<br />
Die himmlishe Freud' ist eine selige Stadt;<br />
Die himmlishe Freud', die kein Ende mehr hat.<br />
Die himmlishe Freude war Petro Bereit't<br />
Durch Jesum und allen zur Seligkeit.<br />
</p></td>
<td><p><strong>「三人同唱的天使頌」</strong><br />
(取材自「<a href="../Page/少年魔號.md" title="wikilink">少年魔號</a>」)</p>
<p><strong>女聲及童聲合唱、女中音獨唱</strong><br />
(Bimm bamm！)</p>
<p>三位天使唱著甜美的歌，<br />
聲聲喜樂，響徹天國。<br />
眾天使們齊聲歡呼著說：<br />
「彼得的罪得赦免！」</p>
<p>坐在桌前的主耶穌，<br />
正在跟十二門徒用晚膳。<br />
主耶穌說「你站著所為何事？<br />
我看得見，你為我而哭！」</p>
<p>「仁慈的主！我怎可能不哭？<br />
我犯了十誡！<br />
漫無目的地走著，苦苦痛哭！<br />
主啊，求你憐憫！」</p>
<p>「若犯了十誡的話，<br />
必須跪下禱告，<br />
及發誓以後永遠祇愛神！<br />
因為喜樂由神所賜！」</p>
<p>天賜的喜樂是有福的城，<br />
天賜的喜樂並無終結！<br />
天賜的喜樂由彼得來領受。<br />
耶穌將喜樂賜予彼得，也永遠賜予世人。</p></td>
</tr>
</tbody>
</table>

## 成就

第三號交響曲雖然陣容龐大及演奏時間極長，但近年卻吸引不少頂級樂團嘗試征服這首樂曲，現在，現場演奏機會比起二十世紀前期及中期都為多。另外由於馬勒曾於第二號交響曲的第一樂章結尾處標示
"需休息五分鐘"，因此曾經有一些現場版本同樣於第一樂章後加插短暫的中場休息，不過現在多為一氣呵成地演奏，或只作很短暫的休息。

本交響曲曾多次被主要樂團及指揮作灌錄。其中一個最難忘的錄音，就是一個由名叫[莫禮士·艾伯拉瓦尼爾的美籍指揮領導猶他州交響樂團](../Page/莫禮士·艾伯拉瓦尼爾.md "wikilink")，於[美國](../Page/美國.md "wikilink")[猶他州](../Page/猶他州.md "wikilink")[鹽湖城內以聽覺優良見稱之](../Page/鹽湖城.md "wikilink")[盬湖城大聖堂](../Page/盬湖城大聖堂.md "wikilink")，以[四聲道立體聲作現場錄音](../Page/四聲道立體聲.md "wikilink")。最終之樂章亦曾用作1984年的電視片集「[召喚榮光](../Page/召喚榮光.md "wikilink")」([:en:Call
to
Glory](../Page/:en:Call_to_Glory.md "wikilink"))之主題音樂，亦曾於[BBC紀錄片](../Page/BBC.md "wikilink")「海岸」(Coast)其中一集介紹英國海軍艦艇的環節中出現。

第二樂章曾被[布烈頓於小型樂團當中演奏](../Page/布烈頓.md "wikilink")，此錄音亦在1950年由Boosey &
Hawkes發行。

[盧奇諾·維斯孔蒂](../Page/盧奇諾·維斯孔蒂.md "wikilink")1971年的《[魂斷威尼斯](../Page/魂斷威尼斯_\(電影\).md "wikilink")》亦曾用到第四樂章的一小段。在電影中，此段音樂是電影主角——作曲家[古斯塔夫·馮·奧森巴哈](../Page/古斯塔夫·馮·奧森巴哈.md "wikilink")（[:en:Gustav
von Aschenbach](../Page/:en:Gustav_von_Aschenbach.md "wikilink")）臨死前的作品。

第六樂章曾被用作2004年第28屆雅典奧運會開幕式第二章節的背景音樂，並配合諾貝爾文學獎得主[George
Seferis的詩句朗誦](../Page/George_Seferis.md "wikilink")。

## 樂曲首演

  - 首演(僅限第二、第三及第六樂章)：1897年，由[菲力斯·懷恩加德納作](../Page/菲力斯·懷恩加德納.md "wikilink")[柏林首演](../Page/柏林.md "wikilink")
  - 全套交響曲首演：1902年6月9日，由作曲家本人於[德國](../Page/德國.md "wikilink")[克勒菲德市首演](../Page/克勒菲德.md "wikilink")
  - [美國首演](../Page/美國.md "wikilink")：1914年5月9日於辛辛那堤五月節由[恩斯特·昆瓦德作指揮](../Page/恩斯特·昆瓦德.md "wikilink")。
  - [紐約愛樂樂團首演](../Page/紐約愛樂樂團.md "wikilink")：1922年2月28日於[紐約市由](../Page/紐約市.md "wikilink")[威廉·文告保格作指揮](../Page/威廉·文告保格.md "wikilink")
  - [英國首演](../Page/英國.md "wikilink")(由[BBC播送](../Page/BBC.md "wikilink"))：1947年11月29日由[艾特林·普特領導](../Page/艾特林·普特.md "wikilink")[BBC交響樂團作首演](../Page/BBC交響樂團.md "wikilink")
  - [英國音樂會首演](../Page/英國.md "wikilink")：1961年2月28日由[白賴仁·費弗克斯於](../Page/白賴仁·費弗克斯.md "wikilink")[聖潘克拉斯作演出](../Page/聖潘克拉斯.md "wikilink")

[Category:馬勒交響曲](../Category/馬勒交響曲.md "wikilink")
[Category:1896年樂曲](../Category/1896年樂曲.md "wikilink")