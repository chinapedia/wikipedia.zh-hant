**張恆泰**（），[字](../Page/表字.md "wikilink")**雲亭**，庵號**祥斋**，[清朝末代](../Page/清朝.md "wikilink")[太监总管](../Page/太监.md "wikilink")。[天津](../Page/天津.md "wikilink")[静海县南吕官屯人](../Page/静海县.md "wikilink")，在内宫太监[字辈為](../Page/字辈.md "wikilink")「兰」字，[官章名](../Page/官章名.md "wikilink")**张兰德**，[暱稱為](../Page/暱稱.md "wikilink")**小德张**。[慈禧太后](../Page/慈禧太后.md "wikilink")[赐名](../Page/赐名.md "wikilink")“**张恒泰**”。[其故居在](../Page/张兰德旧居.md "wikilink")[天津](../Page/天津.md "wikilink")。

## 生平

1888年12岁自宫其身，1891年入宫為[內璫](../Page/內璫.md "wikilink")，1892年被派入宫内[南府升平署戏班学](../Page/南府升平署戏班.md "wikilink")[京戲](../Page/京戲.md "wikilink")，為[武生](../Page/武生.md "wikilink")、[小生](../Page/小生.md "wikilink")，武艺精湛，深受[慈禧太后赏识](../Page/慈禧太后.md "wikilink")。

1898年被提升为后宫太监回事。

1900年[庚子事变](../Page/庚子事变.md "wikilink")，[八國聯軍攻來](../Page/八國聯軍.md "wikilink")，随慈禧太后西狩，回京后升任[御膳房掌案](../Page/御膳房.md "wikilink")，三品顶戴。

1909年，按照慈禧的懿旨，小德张升为[长春宫四司八处大总管](../Page/长春宫.md "wikilink")。各[王公](../Page/王公.md "wikilink")、[大臣晋见](../Page/大臣.md "wikilink")[隆裕太后](../Page/隆裕太后.md "wikilink")，必须得到小德张的首肯，权倾一时。

1911年[辛亥革命](../Page/辛亥革命.md "wikilink")，1912年清朝[總理大臣](../Page/總理大臣.md "wikilink")[袁世凱因](../Page/袁世凱.md "wikilink")[與革命黨人和議已成](../Page/1911年南北議和.md "wikilink")，打算成為[民國](../Page/民國.md "wikilink")[大總統](../Page/大總統.md "wikilink")，[行賄小德張](../Page/行賄.md "wikilink")，威嚇[隆裕太后](../Page/隆裕太后.md "wikilink")，如果不同意[宣統退位](../Page/宣統退位.md "wikilink")，皇室一族可能被[革命黨](../Page/革命黨.md "wikilink")[族滅](../Page/族滅.md "wikilink")，而退位可享有《[清室優待條件](../Page/清室優待條件.md "wikilink")》。又加上[段祺瑞](../Page/段祺瑞.md "wikilink")[聯名五十名](../Page/聯名.md "wikilink")[將領發出](../Page/將領.md "wikilink")《**[北洋五十將乞共和電](../Page/北洋五十將乞共和電.md "wikilink")**》與第《**乞共和第二電**》\[1\]隆裕最後同意了，民國建立，袁世凱成為大總統。[遜清小朝廷依條件](../Page/遜清小朝廷.md "wikilink")，居在[紫禁城內](../Page/紫禁城.md "wikilink")，享有小皇宮，並可在「城內保留[年號](../Page/年號.md "wikilink")」。

1913年隆裕去世后，小德张離開[紫禁城](../Page/紫禁城.md "wikilink")，到[天津英租界做](../Page/天津英租界.md "wikilink")[寓公](../Page/寓公.md "wikilink")，深居简出，不问政事，广置田产。1957年4月19日病逝于天津，终年81岁。

## 相關影視作品

  - [太监秘史](../Page/太监秘史.md "wikilink")（1990，主角毓泰的原型为小德张）
  - [蒼穹之昴](../Page/蒼穹之昴.md "wikilink")（2010）
  - [建党伟业](../Page/建党伟业.md "wikilink")（2011）

## 外部链接

  -
[\~](../Category/张姓.md "wikilink") [Z张](../Category/清朝宦官.md "wikilink")
[Z张](../Category/静海人.md "wikilink")

1.