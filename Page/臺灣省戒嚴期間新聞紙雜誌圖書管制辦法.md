[臺灣戒嚴時期各種短命的黨外雜誌_Short-life_pro-democracy_magazines_during_martial-law_period_in_Taiwan.jpg](https://zh.wikipedia.org/wiki/File:臺灣戒嚴時期各種短命的黨外雜誌_Short-life_pro-democracy_magazines_during_martial-law_period_in_Taiwan.jpg "fig:臺灣戒嚴時期各種短命的黨外雜誌_Short-life_pro-democracy_magazines_during_martial-law_period_in_Taiwan.jpg")

《**臺灣省戒嚴期間新聞紙雜誌圖書管制辦法**》公佈於1953年7月，為《[台灣省戒嚴令](../Page/台灣省戒嚴令.md "wikilink")》[子法之一](../Page/子法.md "wikilink")，衍申自[中華民國《戒嚴法》第十一條中的](../Page/:s:戒嚴法/民國37年12月修正.md "wikilink")「*……取締言論、講學、新聞雜誌、圖畫、告白、標語暨其他出版物之認為與軍事有妨害者。……*」，頒布者亦為因為[國共內戰](../Page/國共內戰.md "wikilink")、從[中國大陸撤退至](../Page/中國大陸.md "wikilink")[台灣的](../Page/台灣.md "wikilink")[中華民國政府](../Page/中華民國政府.md "wikilink")。除了該辦法外，相關查禁法源尚有《[出版法](../Page/出版法.md "wikilink")》、《[出版法施行細則](../Page/出版法.md "wikilink")》、《[社會教育法](../Page/社會教育法.md "wikilink")》、《[戒嚴法](../Page/戒嚴法.md "wikilink")》、《**台灣地區戒嚴時期出版物管制辦法**》、《[刑法](../Page/刑法.md "wikilink")》，其目的也均相同。隨著[戒嚴令的解除](../Page/臺灣省戒嚴令.md "wikilink")，此管制辦法亦在1987年7月15日宣告廢除。

## 簡介

實為中華民國政府的[行政院公告此管制辦法](../Page/行政院.md "wikilink")，主要目的在自我審查並限制[台灣新聞](../Page/台灣.md "wikilink")、雜誌、圖書、標語與相關[出版品內容](../Page/出版品.md "wikilink")，亦為箝制[言論自由的重要法律之一](../Page/言論自由.md "wikilink")。該辦法管制內容涵括

  - 臺灣地區與大陸地區兩岸的軍事消息
  - 國防、政治、外交之機密
  - 宣傳[共產主義的圖畫文字](../Page/共產主義.md "wikilink")
  - 詆毀[中華民國元首的圖畫文字](../Page/中華民國元首.md "wikilink")
  - 違背[反共抗俄之基本國策](../Page/反共抗俄.md "wikilink")
  - 足以影響民心士氣或危害台灣社會治安的言論
  - 挑撥政府與人民感情之圖畫文字

## 主管機關

依該管制辦法，台灣所有出版品必須於發行時，檢送[臺灣省保安司令部](../Page/臺灣省保安司令部.md "wikilink")（後改為[警備總部及地方](../Page/警備總部.md "wikilink")[警察局](../Page/警察局.md "wikilink")）並由該單位檢查。於審查後，如該單位認定該出版品違反該處分，即扣押出版品且發文各單位不准陳列。除此，出版負責人並引用他法追究其出版刑責\[1\]。

## 查禁規模

[Taiwan_banned_book_contents.JPG](https://zh.wikipedia.org/wiki/File:Taiwan_banned_book_contents.JPG "fig:Taiwan_banned_book_contents.JPG")

台灣[戒嚴時期引用該等限制言論自由法律的書籍數量十分龐大](../Page/戒嚴.md "wikilink")。1950年代，查禁書刊的第一個10年，[魯迅](../Page/魯迅.md "wikilink")、[巴金](../Page/巴金.md "wikilink")、[茅盾](../Page/茅盾.md "wikilink")、[老舍](../Page/老舍.md "wikilink")、[沈從文等留在中國大陸的](../Page/沈從文.md "wikilink")[五四運動後的知名作家](../Page/五四運動.md "wikilink")，他們的著作、翻譯書籍都被列在查禁的範圍內。[金庸](../Page/金庸.md "wikilink")、[王度廬等武俠小說作家的作品也被查禁](../Page/王度廬.md "wikilink")，還有《女學生的秘密》這類[色情或奇情的書刊](../Page/色情.md "wikilink")。被查禁的書刊也包括了[反共文學](../Page/反共文學.md "wikilink")，如孫陵的《大風雪》等。被查禁的政治類的書刊還有《[司徒雷登回憶錄](../Page/司徒雷登.md "wikilink")》。

1960年代查禁許多「異議知識分子」的出版書刊：例如[雷震的](../Page/雷震.md "wikilink")《[自由中國](../Page/自由中國.md "wikilink")》雜誌，[李敖在](../Page/李敖.md "wikilink")[文星書店出版和](../Page/文星書店.md "wikilink")[柏楊在](../Page/柏楊.md "wikilink")[平原出版社出版的書](../Page/平原出版社.md "wikilink")，在此時期至1970年代被查禁。有些移民至台灣的作家，在中國大陸時期所撰寫的書刊，也陸續被查禁：小說家[郭良蕙的](../Page/郭良蕙.md "wikilink")《[心鎖](../Page/心鎖.md "wikilink")》是在1963年1月，被依違反《出版法》而查禁\[2\]。

時有[文字獄的狀況](../Page/文字獄.md "wikilink")，如[大同公司在](../Page/大同公司.md "wikilink")[台北市中山北路牆面廣告橫寫](../Page/中山北路_\(台北市\).md "wikilink")「世界的國貨」，被檢舉自右往左讀是「禍國的介石」；因董事長[林挺生是](../Page/林挺生.md "wikilink")[中國國民黨中央常務委員](../Page/中國國民黨.md "wikilink")，方逃過一劫\[3\]。

1970年代，因為[中華民國退出聯合國](../Page/中華民國退出聯合國.md "wikilink")，許多探討臺灣未來的雜誌、書籍應運時代或配合[黨外運動而產生](../Page/黨外運動.md "wikilink")，這些雜誌和書刊常是查禁的目標，例如《臺灣政論》、《[美麗島](../Page/美麗島雜誌.md "wikilink")》等雜誌，和[吳濁流的](../Page/吳濁流.md "wikilink")《無花果》、[陳映真的](../Page/陳映真.md "wikilink")《[將軍族](../Page/將軍族.md "wikilink")》等臺灣[鄉土文學作品](../Page/鄉土文學.md "wikilink")。其他因為西洋電影而翻譯的書籍，如《[畢業生](../Page/毕业生_\(电影\).md "wikilink")》、《[教父](../Page/教父_\(小說\).md "wikilink")》等，也遭查禁。

1980年代，許多黨外雜誌蓬勃出現，例如《進步》、《深耕》等雜誌，因為刊載李敖的文章〈為老兵李師科喊話〉和[二二八事件相關文章而被查禁](../Page/二二八事件.md "wikilink")，但仍有《自由滋味》、《前進》、《[蓬萊島](../Page/蓬萊島雜誌案.md "wikilink")》和翻譯書籍《[宋氏王朝](../Page/宋家皇朝.md "wikilink")》接續出現。此時期的警備總部主要查禁的對象為這類書籍，書術類和其性其的書刊被查禁的書量減少。1982年，[陶百川發表文章](../Page/陶百川.md "wikilink")〈禁書有正道，奈何用牛刀〉，反對軍人審查和查禁書籍的作法；警總對應此文，召開「駁斥陶百川先生攻訐警總文化審檢工作座談會」，引起臺灣海內外人士討論。此時期警總查禁書刊的方式，由書店查禁，改到上游的印刷廠、裝訂廠查禁、沒收，此舉在1985年引發抗議活動\[4\]。

## 後續

[Exhibition_of_Publications_banned_during_the_Period_of_Martial_Law,_Tainan_20070925.jpg](https://zh.wikipedia.org/wiki/File:Exhibition_of_Publications_banned_during_the_Period_of_Martial_Law,_Tainan_20070925.jpg "fig:Exhibition_of_Publications_banned_during_the_Period_of_Martial_Law,_Tainan_20070925.jpg")
1982年[臺灣省政府編圖了](../Page/臺灣省政府.md "wikilink")《查禁圖書目錄》，收錄了1950年1月至1982年12月被查禁的書籍。1987年此法廢止，但出版物仍在《出版法》的審查制度之下，出版法於1999年被廢止\[5\]。

2007年7月至10月，[國家圖書館在台灣各地舉辦](../Page/國家圖書館_\(中華民國\).md "wikilink")**戒嚴時期查禁書刊展**，由國家圖書館和台灣各大學圖書館中，選出193種書籍和雜誌展出\[6\]。

2010年，[監察委員](../Page/監察委員.md "wikilink")[黃煌雄等提出調查報告指出](../Page/黃煌雄.md "wikilink")1948至1949年有三次戒嚴令，1949年5月20號臺灣省警備總司令部發佈的[臺灣省戒嚴令是否有依戒嚴法第](../Page/臺灣省戒嚴令.md "wikilink")3條按級呈報代總統[李宗仁](../Page/李宗仁.md "wikilink")、再由總統提交[立法院追認無從考究](../Page/立法院.md "wikilink")，1949年11月22日第三次的戒嚴令如未經總統宣告發布（李宗仁代總統當時並不在中華民國境內，不可能簽字公告，不符憲法第39條規定），形式要件不完整，法定程序有瑕疵，則戒嚴令因欠缺形式法效而失效。但最終相關法律的認定是[司法院大法官會議](../Page/司法院大法官會議.md "wikilink")。\[7\]\[8\]\[9\]\[10\]\[11\]\[12\]

## 參考資料

[Category:審查制度](../Category/審查制度.md "wikilink")
[Category:臺灣已廢止法律](../Category/臺灣已廢止法律.md "wikilink")
[Category:言论自由](../Category/言论自由.md "wikilink")
[Category:臺灣報業史](../Category/臺灣報業史.md "wikilink")
[Category:臺灣人權史](../Category/臺灣人權史.md "wikilink")
[Category:1953年法律](../Category/1953年法律.md "wikilink")
[Category:1987年台灣廢除](../Category/1987年台灣廢除.md "wikilink")

1.  楊秀菁，《臺灣戒嚴時期的新聞管制政策》

2.  戒嚴時期查禁書刊展 展覽目錄 國家圖書館編印

3.

4.
5.
6.
7.  [白色恐怖受難者　控訴大法官失職](http://www.twtimes.com.tw/index.php?page=news&nid=36881),
    [臺灣時報](../Page/臺灣時報.md "wikilink"), 2009-4-6

8.  [監院調查38年戒嚴令有爭議](http://www.cna.com.tw/news/FirstNews/201008110019-1.aspx),
    [中央通訊社](../Page/中央通訊社.md "wikilink"), 2010-8-11

9.  [監院報告:38年戒嚴令
    發布有瑕疵](http://web.pts.org.tw/php/news/pts_news/detail.php?NEENO=156283),
    [公視新聞](../Page/公視新聞.md "wikilink"), 2010-8-11

10. [臺灣發布戒嚴是否符合法定程序
    監察院提調查報告](http://www.cy.gov.tw/sp.asp?xdURL=./di/Message/message_1.asp&ctNode=903&msg_id=3108),
    [監察院](../Page/監察院.md "wikilink"), 2010-8-11

11. [2010年監察院人權工作實錄：第1冊
    公民與政治權利](http://www.cy.gov.tw/dl.asp?fileName=1101817141997.pdf),
    [監察院](../Page/監察院.md "wikilink"), 2010

12. [監院稱38年戒嚴令實施有瑕疵　綠委：慎重研議補救體制](http://www.nownews.com/n/2010/08/12/668124),
    [Nownews](../Page/Nownews.md "wikilink"), 2010-8-12