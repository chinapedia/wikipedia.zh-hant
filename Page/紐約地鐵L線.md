**L線14街-卡納西慢車**（），又稱**紐約地鐵L線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的[地鐵系統](../Page/地鐵.md "wikilink")。由於該線全線使用[BMT卡納西線](../Page/BMT卡納西線.md "wikilink")，因此其路線徽號為灰色\[1\]。

L線在任何時候都營運，來往[曼哈頓](../Page/曼哈頓.md "wikilink")[雀兒喜的](../Page/雀兒喜_\(紐約\).md "wikilink")[第八大道與](../Page/第八大道車站_\(BMT卡納西線\).md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")的[洛克威公園道](../Page/卡納西-洛克威公園道車站_\(BMT卡納西線\).md "wikilink")。此線也在短暫進入[皇后區](../Page/皇后區.md "wikilink")，服務\[2\]。此線是紐約地鐵第一條使用[通訊式列車控制進行](../Page/通訊式列車控制.md "wikilink")的路線。

L線自1928年卡納西線完工後就使用現時的路線和營運模式。布魯克林中心L線軌道曾經有快車軌道營運，在東布魯克林沿運行，但在1956年中止。自此L線完全以慢車行駛。

## 歷史

線前身為LL（1967年至1979年）及BMT 6號線（1924年至1967年）。

1924年[14街-卡納西線部份通車](../Page/BMT卡納西線.md "wikilink")，當時稱為14街-東區線（俗稱14街-東線）、列車編號16。

1928年百老匯聯運站以東的路線完工通車，自此行車路線大致維持至今。

1931年新增第六大道車站至第八大道車站的延伸線，連接[IND第八大道線](../Page/IND第八大道線.md "wikilink")。

1967年11月26日[克莉絲蒂街連接道通車](../Page/克莉絲蒂街連接道.md "wikilink")，BMT 16號線改為LL線。
1986年雙字表慢車取消，LL線改為L線。

</div>

## 路線

### 服務形式

L線列車在任何時候都在同一條路線上使用相同的服務形式。\[3\]

| 路線                                       | 起點                                               | 終點                                                           | 軌道 |
| ---------------------------------------- | ------------------------------------------------ | ------------------------------------------------------------ | -- |
| [BMT卡納西線](../Page/BMT卡納西線.md "wikilink") | [第八大道](../Page/第八大道車站_\(BMT卡納西線\).md "wikilink") | [卡納西-洛克威公園道](../Page/卡納西-洛克威公園道車站_\(BMT卡納西線\).md "wikilink") | 全部 |

### 車站

更詳細的車站列表參見[BMT卡納西線](../Page/BMT卡納西線.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-L.svg" title="fig:NYCS-bull-trans-L.svg">NYCS-bull-trans-L.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/BMT卡納西線.md" title="wikilink">卡納西線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第八大道車站_(BMT卡納西線).md" title="wikilink">第八大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第六大道車站_(BMT卡納西線).md" title="wikilink">第六大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>，<a href="../Page/14街車站_(IRT百老匯-第七大道線).md" title="wikilink">14街</a>）<br />
（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>，<a href="../Page/14街車站_(IND第六大道線).md" title="wikilink">14街</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/14街車站_(PATH).md" title="wikilink">14街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/聯合廣場車站_(BMT卡納西線).md" title="wikilink">聯合廣場</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第三大道車站_(BMT卡納西線).md" title="wikilink">第三大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第一大道車站_(BMT卡納西線).md" title="wikilink">第一大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>北行</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/貝德福德大道車站_(BMT卡納西線).md" title="wikilink">貝德福德大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/羅里默街車站_(BMT卡納西線).md" title="wikilink">羅里默街</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IND跨城線.md" title="wikilink">IND跨城線</a>，<a href="../Page/大都會大道車站_(IND跨城線).md" title="wikilink">大都會大道</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/格拉漢姆大道車站_(BMT卡納西線).md" title="wikilink">格拉漢姆大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/格蘭街車站_(BMT卡納西線).md" title="wikilink">格蘭街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/蒙特羅斯大道車站_(BMT卡納西線).md" title="wikilink">蒙特羅斯大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/摩根大道車站_(BMT卡納西線).md" title="wikilink">摩根大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/傑斐遜街車站_(BMT卡納西線).md" title="wikilink">傑斐遜街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT卡納西線).md" title="wikilink">德卡爾布大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/默特爾-威克奧夫大道車站_(BMT卡納西線).md" title="wikilink">默特爾-威克奧夫大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/BMT默特爾大道線.md" title="wikilink">BMT默特爾大道線</a>）</p></td>
<td><p>早上繁忙時段部分來往第八大道的列車以此站為起終點</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/哈爾西街車站_(BMT卡納西線).md" title="wikilink">哈爾西街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/威爾遜大道車站_(BMT卡納西線).md" title="wikilink">威爾遜大道</a></p></td>
<td></td>
<td><p> ↑</p></td>
<td></td>
<td><p>車站只限北行可<a href="../Page/美國殘疾人法案.md" title="wikilink">無障礙通行</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/布什維克大道-阿伯丁街車站_(BMT卡納西線).md" title="wikilink">布什維克大道-阿伯丁街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/百老匯交匯車站_(BMT卡納西線).md" title="wikilink">百老匯交匯</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IND福爾頓街線.md" title="wikilink">IND福爾頓街線</a>）<br />
（<a href="../Page/BMT牙買加線.md" title="wikilink">BMT牙買加線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/大西洋大道車站_(BMT卡納西線).md" title="wikilink">大西洋大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/蘇特大道車站_(BMT卡納西線).md" title="wikilink">蘇特大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/利沃尼亞大道車站_(BMT卡納西線).md" title="wikilink">利沃尼亞大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/新地段大道車站_(BMT卡納西線).md" title="wikilink">新地段大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>巴士往<a href="../Page/約翰·甘迺迪國際機場.md" title="wikilink">JFK機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/東105街車站_(BMT卡納西線).md" title="wikilink">東105街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>繁忙時段部分北行列車自此站起載</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡納西-洛克威公園道車站_(BMT卡納西線).md" title="wikilink">卡納西-洛克威公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

## 參考文獻

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20060902103336/http://mta.info/nyct/service/lline.htm)
  - [L線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    L線時刻表（PDF）](https://web.archive.org/web/20060921231454/http://www.mta.info/nyct/service/pdf/tlcur.pdf)

[L](../Category/紐約地鐵服務路線.md "wikilink")

1.
2.
3.