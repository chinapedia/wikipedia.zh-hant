[Koningin_Sophie_Württemberg.jpg](https://zh.wikipedia.org/wiki/File:Koningin_Sophie_Württemberg.jpg "fig:Koningin_Sophie_Württemberg.jpg")
**索菲·弗雷德里卡·马蒂尔德·冯·[符腾堡](../Page/符腾堡.md "wikilink")**（**Sophia Frederika
Mathilde von
Württemberg**，），[荷兰国王](../Page/荷兰国王.md "wikilink")[威廉三世的第一任](../Page/威廉三世_\(荷兰国王\).md "wikilink")[王后](../Page/王后.md "wikilink")。

## 生平

她出生于德国[斯图加特](../Page/斯图加特.md "wikilink")(Stuttgart)。她的父母是符腾堡国王[威廉一世和](../Page/威廉一世_\(符腾堡\).md "wikilink")[俄罗斯的凯瑟琳·帕芙洛娃女大公](../Page/俄罗斯的凯瑟琳·帕芙洛娃女大公.md "wikilink")(沙皇[保罗一世长女](../Page/保罗一世_\(俄罗斯\).md "wikilink"))。索菲和威廉是表兄妹(她母亲和威廉的母亲[安娜·帕芙洛娃是姐妹](../Page/安娜·帕夫洛芙娜_\(荷蘭王后\).md "wikilink"))，索菲出生不久后她的母亲就去世了，由她的姑姑[符腾堡的凯瑟琳公主照顾](../Page/凯瑟琳_\(符腾堡\).md "wikilink")。1839年6月18日在斯图加特，索菲和[奥兰治亲王](../Page/奥兰治亲王.md "wikilink")[威廉结为夫妇](../Page/威廉三世_\(荷兰国王\).md "wikilink")。

这对夫妇返回荷兰，并建立自己的努儿登堡宫(Noordeinde Palace)。他们有三个孩子：

  - [*威廉*·尼古拉·亚历山大·弗雷德里克·卡雷尔·亨德里克](../Page/威廉王子_\(荷兰\).md "wikilink")
    (1840-1879)。王位继承人，1849年封奥兰治亲王。
  - [威廉·弗雷德里克·*莫瑞斯*·亚历山大·亨德里克·卡雷尔](../Page/莫瑞斯王子_\(荷兰\).md "wikilink")(1843-1850).
  - [威廉·*亚历山大*·卡雷尔·亨德里克·弗雷德里克](../Page/亚历山大_\(奥兰治亲王\).md "wikilink")
    (1851-1884). 王位继承人，从1879年直至逝世。

在智力方面，索菲远远优于丈夫。她支持一些慈善机构，包括动物保护和建设公共停车场。此外，威廉三世有几个婚外情。她让它广为人知，该婚姻并不成功。为此索菲试图[离婚](../Page/离婚.md "wikilink")，但因她的王室地位和国家重要影响，这被拒绝。从1855年夫妻分开住，直至索菲在海牙附近的豪斯登堡(Huis
Ten Bosch)去世。

[Category:荷蘭王后](../Category/荷蘭王后.md "wikilink")
[Category:符腾堡君主女儿](../Category/符腾堡君主女儿.md "wikilink")
[Category:符腾堡公主](../Category/符腾堡公主.md "wikilink")
[Category:斯圖加特人](../Category/斯圖加特人.md "wikilink")
[Category:玛丽亚·路易莎皇后勋章得主](../Category/玛丽亚·路易莎皇后勋章得主.md "wikilink")
[Category:葬于代尔夫特新教堂王室墓地](../Category/葬于代尔夫特新教堂王室墓地.md "wikilink")