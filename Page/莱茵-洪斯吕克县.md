**莱茵-洪斯吕克县**（）是[德国](../Page/德国.md "wikilink")[莱茵兰-普法尔茨州的一个县](../Page/莱茵兰-普法尔茨州.md "wikilink")，首府[洪斯吕克](../Page/洪斯吕克.md "wikilink")。

## 地理

莱茵-洪斯吕克县北面与[迈恩-科布伦茨县](../Page/迈恩-科布伦茨县.md "wikilink")，东面与[莱茵-拉恩县和](../Page/莱茵-拉恩县.md "wikilink")[美因茨-宾根县](../Page/美因茨-宾根县.md "wikilink")，南面与[巴特克罗伊茨纳赫县和](../Page/巴特克罗伊茨纳赫县.md "wikilink")[比肯费尔德县](../Page/比肯费尔德县.md "wikilink")，西面与[伯恩卡斯特尔-维特利希县和](../Page/伯恩卡斯特尔-维特利希县.md "wikilink")[科赫姆-采尔县相邻](../Page/科赫姆-采尔县.md "wikilink")。

莱茵-洪斯吕克县位于[莱茵河中游的左侧](../Page/莱茵河.md "wikilink")，从[上韦瑟尔到](../Page/上韦瑟尔.md "wikilink")[波帕德](../Page/波帕德.md "wikilink")，此外包括[洪斯吕克山的中部和东部](../Page/洪斯吕克山.md "wikilink")。[松瓦德位于县的南端](../Page/松瓦德.md "wikilink")。最高点海拔653米。

## 历史

在1800年前今天莱茵-洪斯吕克县属于许多不同的贵族领地，其中包括[特里尔选帝侯](../Page/特里尔选帝侯.md "wikilink")、[斯蓬海姆伯爵](../Page/斯蓬海姆伯爵.md "wikilink")、[西默尔伯爵以及当时的帝国城市波帕德和上韦瑟尔](../Page/西默尔伯爵.md "wikilink")。在[拿破崙‧波拿巴时期这里被](../Page/拿破崙‧波拿巴.md "wikilink")[法国占领](../Page/法国.md "wikilink")，成为以[科布伦茨为省会的莱茵](../Page/科布伦茨.md "wikilink")-摩澤爾省的一部分。1815年[维也纳会议后它属于](../Page/维也纳会议.md "wikilink")[普鲁士](../Page/普鲁士.md "wikilink")。1816年普鲁士设立西默尔县和圣哥亚县，这两个县当时属于下莱茵大公国省（从1822年开始属于[莱茵省](../Page/莱茵省.md "wikilink")）1946年它们被划入[科布伦茨地区](../Page/科布伦茨地区.md "wikilink")，属莱茵兰-普法尔茨州。1969年在行政区划改革中西默尔县以及周边的圣哥亚县、科赫姆县和采尔县的部分被合并成莱茵-洪斯吕克县。

## 县徽

说明：县徽的上方是蓝色和金色的方块。右侧是金色的底，上面有一只爪和喙红色的黑鹰，它面向左侧。左侧是黑色的底，上面是一只面向右边的狮子，它的舌头、爪和王冠是供色的。

意义：县徽上的图案体现出过去县内的统治者：徽上方的蓝色和金色的方块是斯蓬海姆伯爵，狮子是普法尔茨选帝侯，鹰是原自由帝国城市上韦瑟尔和波帕德。

## 政治

### 县议会

2004年6月13日县议会选举结果为：

<table>
<tbody>
<tr class="odd">
<td><p>| <strong>党派</strong></p></td>
<td style="text-align: center;"><p><strong>%<br />
2004年</strong></p></td>
<td style="text-align: center;"><p><strong>席位<br />
2004年</strong></p></td>
<td style="text-align: center;"><p><strong>%<br />
1999年</strong></p></td>
<td style="text-align: center;"><p><strong>席位<br />
1999年</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德国社会民主党.md" title="wikilink">德国社会民主党</a></p></td>
<td style="text-align: center;"><p>29.9</p></td>
<td style="text-align: center;"><p>13</p></td>
<td style="text-align: center;"><p>35.2</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德国基督教民主联盟.md" title="wikilink">德国基督教民主联盟</a></p></td>
<td style="text-align: center;"><p>49.3</p></td>
<td style="text-align: center;"><p>21</p></td>
<td style="text-align: center;"><p>48.8</p></td>
<td style="text-align: center;"><p>20</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德国绿党.md" title="wikilink">德国绿党</a></p></td>
<td style="text-align: center;"><p>5.7</p></td>
<td style="text-align: center;"><p>2</p></td>
<td style="text-align: center;"><p>4.4</p></td>
<td style="text-align: center;"><p>2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德国自由民主党.md" title="wikilink">德国自由民主党</a></p></td>
<td style="text-align: center;"><p>9.4</p></td>
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p>7.1</p></td>
<td style="text-align: center;"><p>3</p></td>
</tr>
<tr class="even">
<td><p>自由选民组织</p></td>
<td style="text-align: center;"><p>5.8</p></td>
<td style="text-align: center;"><p>2</p></td>
<td style="text-align: center;"><p>4.4</p></td>
<td style="text-align: center;"><p>2</p></td>
</tr>
<tr class="odd">
<td><p><strong>总</strong></p></td>
<td style="text-align: center;"><p><strong>100.0</strong></p></td>
<td style="text-align: center;"><p><strong>42</strong></p></td>
<td style="text-align: center;"><p><strong>100.0</strong></p></td>
<td style="text-align: center;"><p><strong>42</strong></p></td>
</tr>
<tr class="even">
<td><p>|<strong>参选选民百分比</strong></p></td>
<td style="text-align: center;"><p><strong>65.3</strong></p></td>
<td style="text-align: center;"><p><strong>68.6</strong></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 交通

莱茵-洪斯吕克县境内有一小段莱茵河的左岸，其大多数地区则是洪斯吕克山的高地。早在1859年从[宾根沿莱茵河谷左岸通往科布伦茨的铁路就开通了](../Page/宾根.md "wikilink")。

1889年通往西默尔的铁路开通，1901年和1902年这条铁路继续扩展，1906年向北延展，1908年建成了一条非常陡峭的路段通入莱茵河谷中的波帕德。

到1921年、1922年县内铁路总长度达143千米。在1963年至1984年间许多这些铁路被废弃掉了。今天仅44千米还被用做客运。此外部分山地里的铁路被用来连结[法兰克福-哈恩机场和莱茵](../Page/法兰克福-哈恩机场.md "wikilink")-美因地区。
[Soonwald-Simmern01.jpg](https://zh.wikipedia.org/wiki/File:Soonwald-Simmern01.jpg "fig:Soonwald-Simmern01.jpg")

[德国高速公路](../Page/德国高速公路.md "wikilink")[61号从科布伦茨通往路德维希港](../Page/德国高速公路61号.md "wikilink")，穿过莱茵-洪斯吕克县。县内还有多条[联邦公路和县公路跨越](../Page/德国联邦公路.md "wikilink")，其中包括沿莱茵河左岸行走的9号联邦公路以及50号、421号和327号联邦公路。

法兰克福-哈恩机场位于县的西边。这个机场主要供[廉價航空公司](../Page/廉價航空公司.md "wikilink")、东欧货运航空公司以及军事服务飞行使用。

[R](../Category/莱茵兰-普法尔茨州的县.md "wikilink")