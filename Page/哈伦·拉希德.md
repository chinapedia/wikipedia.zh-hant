[Harun_Al-Rashid_and_the_World_of_the_Thousand_and_One_Nights.jpg](https://zh.wikipedia.org/wiki/File:Harun_Al-Rashid_and_the_World_of_the_Thousand_and_One_Nights.jpg "fig:Harun_Al-Rashid_and_the_World_of_the_Thousand_and_One_Nights.jpg")
**哈倫·拉希德**（；，），伊斯蘭教第二十三代哈里發，[阿拔斯王朝的第五代](../Page/阿拔斯王朝.md "wikilink")[哈里發](../Page/哈里發.md "wikilink")。他是阿拔斯王朝第三任哈里发[阿尔·马赫迪之子](../Page/阿尔·马赫迪.md "wikilink")，在786年继其兄[阿尔·哈迪之位](../Page/阿尔·哈迪.md "wikilink")，任期間為王朝最強盛時代，曾親率軍隊入侵[拜占廷的](../Page/拜占廷.md "wikilink")[小亞細亞](../Page/小亞細亞.md "wikilink")。其[首都](../Page/首都.md "wikilink")[巴格達和](../Page/巴格達.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")[長安為世界第一流的](../Page/長安.md "wikilink")[城市](../Page/城市.md "wikilink")，[人口多達](../Page/人口.md "wikilink")100萬，也是[國際貿易中心](../Page/國際貿易.md "wikilink")。不過哈倫時代也是王朝衰退的開端。西元803年哈倫逮捕[宰相](../Page/宰相.md "wikilink")[叶海亚](../Page/叶海亚·伊本·哈立德.md "wikilink")，[巴爾馬克家族被抄家](../Page/巴爾馬克家族.md "wikilink")，導致[波斯發生叛變](../Page/波斯.md "wikilink")。哈倫也死於波斯的[托斯](../Page/托斯.md "wikilink")，至此阿拔斯王朝步入衰退之路。

## 生平

782年，哈伦率领阿拔斯军队出征[东罗马帝国](../Page/东罗马帝国.md "wikilink")，此次作战远至[博斯普鲁斯海峡](../Page/博斯普鲁斯海峡.md "wikilink")。东罗马摄政皇太后[伊琳娜被迫乞和](../Page/伊琳娜女皇.md "wikilink")，和约规定东罗马帝国向阿拔斯王朝纳贡每年分两期七到九万[第纳尔](../Page/第纳尔.md "wikilink")。哈里发马赫迪为此赐予哈伦荣名「拉希德」（al-Rashid，意为正直者），并立哈伦·拉希德为第二继承人。

### 哈里发

786年，哈伦·拉希德登基。

东罗马新君[尼基弗鲁斯一世扬言废除](../Page/尼基弗鲁斯一世.md "wikilink")[伊琳娜女皇缔结的和约中的一切条款](../Page/伊琳娜女皇.md "wikilink")。作为报复，哈伦·拉希德率军出征东罗马，最终在806年夺取了赫拉克利亚（Hiraqlah）和泰阿那（al-Tuwanah）。并且，除原有贡税外，还加征了一种耻辱性的人丁税（包括皇帝本人和皇室的每个成员）。此次事件被视为阿拔斯王朝势力顶点标志。

### 死亡

809年，哈伦·拉希德死于征讨途中，其子[阿明继任](../Page/阿布·阿卜杜拉·穆罕默德·阿明·本·哈伦.md "wikilink")。

## 流行文化

拉希德也出现在故事集《[一千零一夜](../Page/一千零一夜.md "wikilink")》中。
哈伦·拉希德出现在日本动漫《[哆啦A梦](../Page/哆啦A梦.md "wikilink")》的剧场版《[大雄的天方夜谭](../Page/大雄的天方夜谭.md "wikilink")》中。

[Category:阿拔斯王朝哈里发](../Category/阿拔斯王朝哈里发.md "wikilink")