《**三十风雨路**》（An Ode To
Life），[新加坡](../Page/新加坡.md "wikilink")[新传媒8频道时装剧集](../Page/新传媒8频道.md "wikilink")，于2004年8月10日至2004年10月4日期间首播，共40集，故事人洪汐，監製鄔毓琳。此剧是为[黄碧仁](../Page/黄碧仁.md "wikilink")“量身定做”的，且为2004年“最高收视率”电视剧。

## 演员表

### 主要演员

  - [黄碧仁](../Page/黄碧仁.md "wikilink") 飾 汪素枝
  - [曹国辉](../Page/曹国辉.md "wikilink") 饰 欧启明
  - [徐绮](../Page/徐绮.md "wikilink") 飾 邱美娜
  - [张耀栋](../Page/张耀栋.md "wikilink") 飾 张文禧

### 其他演员

  - [姚玟隆](../Page/姚玟隆.md "wikilink") 飾 張文祿
  - [杨莉冰](../Page/杨莉冰.md "wikilink") 飾 張文思
  - [杨莉娜](../Page/杨莉娜.md "wikilink") 飾 郭愛玲
  - [刘谦益](../Page/刘谦益.md "wikilink") 飾 張耀坤
  - [蔡平開](../Page/蔡平開.md "wikilink") 飾 林鳳珠
  - [洪培興](../Page/洪培興.md "wikilink") 飾 張文福
  - [唐育书](../Page/唐育书.md "wikilink") 飾 张瑞安（少年：陳彥任）
  - [章缜翔](../Page/章缜翔.md "wikilink") 飾 张瑞祥（童年：蔡協晉）
  - [沈炜竣](../Page/沈炜竣.md "wikilink") 飾 張瑞明
  - [潘淑钦](../Page/潘淑钦.md "wikilink") 飾 潘子男（少年：李咸鬟）
  - [戴倩云](../Page/戴倩云.md "wikilink") 飾 潘子鹃（童年：陳欣憶）
  - [秦依凌](../Page/秦依凌.md "wikilink") 飾 翁小云
  - [廖莹莹](../Page/廖莹莹.md "wikilink") 飾 欧宝儿
  - [易　凌](../Page/易凌.md "wikilink") 飾 刘莉莉
  - [曾思佩](../Page/曾思佩.md "wikilink") 飾 老虎嫂
  - [陈天祥](../Page/陈天祥.md "wikilink") 飾 汪金虎

## 奖项

  - 《红星大奖2004》最高收视率电视剧
  - 黄碧仁提名《红星大奖2004》最佳女主角奖
  - 提名《红星大奖2004》最佳剧集

[S三](../Category/新加坡電視劇.md "wikilink")
[S三](../Category/2004年電視劇集.md "wikilink")
[S三](../Category/貧窮題材電視劇.md "wikilink")