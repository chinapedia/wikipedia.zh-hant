[San_Fat_Estate.jpg](https://zh.wikipedia.org/wiki/File:San_Fat_Estate.jpg "fig:San_Fat_Estate.jpg")
**新發邨**（）是[屯門區的](../Page/屯門區.md "wikilink")[公共屋邨](../Page/香港公共屋邨.md "wikilink")，也是屯門區內首個公共屋邨，現已拆卸。原址位於[港鐵](../Page/港鐵.md "wikilink")[屯門站旁的私人屋苑](../Page/屯門站_\(西鐵綫\).md "wikilink")[瓏門](../Page/瓏門.md "wikilink")，鄰近[屯門市中心](../Page/屯門市中心.md "wikilink")、[新墟及](../Page/新墟.md "wikilink")[屯門公園](../Page/屯門公園.md "wikilink")。

## 歷史

新發邨屬於[徙置區](../Page/徙置區.md "wikilink")，於1971年9月10日落成入伙，原稱**青山新區大廈**\[1\]\[2\]，1973年改由[香港房屋委員會負責屋邨管理](../Page/香港房屋委員會.md "wikilink")，易名**青山邨**（英語：Castle
Peak Estate）。

1974年，[香港政府為配合將](../Page/港英政府.md "wikilink")「青山區」更名為「屯門區」，故將**青山邨**同時易名。新發邨中的「新」是指[新界鄉議局前主席](../Page/新界鄉議局.md "wikilink")、時任[屯門鄉事委員會主席](../Page/鄉事委員會.md "wikilink")[陳日新](../Page/陳日新_\(香港\).md "wikilink")；「發」則是指時任屯門鄉事委員會副主席、鄉議局前主席兼前立法會議員[劉皇發](../Page/劉皇發.md "wikilink")。政府為表揚2人之貢獻，故而2人名字為屋邨命名。

及後因為興建九廣西鐵（今港鐵[西鐵綫](../Page/西鐵綫.md "wikilink")）[屯門站](../Page/屯門站_\(西鐵綫\).md "wikilink")，於2002年3月尾至2002年11月連同旁邊的「新墟遊樂場」一起拆卸。除了提供工地，最重要的是提供土地作地產發展，以及興建[公共交通交匯處](../Page/屯門站公共交通交匯處.md "wikilink")。該用地部份土地曾經為臨時[停車場及屯門站臨時公共交通交匯處一部份](../Page/停車場.md "wikilink")，現為新鴻基地產上蓋住宅物業[瓏門及](../Page/瓏門.md "wikilink")[V
City商場](../Page/V_City.md "wikilink")，已於2013年8月完工及入伙。

## 樓宇名稱

| 樓宇座號 | 樓宇類型                             | 落成年份 | 拆卸年份 | 樓宇層數 |
| ---- | -------------------------------- | ---- | ---- | ---- |
| 第1座  | [第六型](../Page/第六型.md "wikilink") | 1971 | 2002 | 16   |
| 第2座  | 8                                |      |      |      |
| 第3座  | 16                               |      |      |      |
| 第4座  | 8                                |      |      |      |
|      |                                  |      |      |      |

## 屋邨設施

新發邨共有4座樓宇及1所小學（[何福堂小學](../Page/何福堂小學.md "wikilink")），屋邨範圍亦設有石地足球場，商舖及休憩公園。何福堂小學現已搬往[龍門居旁](../Page/龍門居.md "wikilink")（現[龍逸邨](../Page/龍逸邨.md "wikilink")）。

## 其他

  - 九廣輕鐵（[兩鐵合併後簡稱](../Page/兩鐵合併.md "wikilink")[輕鐵](../Page/香港輕鐵.md "wikilink")）原來亦有一車站以新發邨命名：[新發站](../Page/新發站.md "wikilink")。該站亦因配合西鐵興建而拆卸重建，即現在的[屯門站](../Page/屯門站_\(輕鐵\).md "wikilink")。

  -
## 外部連結

  - [屯門區相片](https://web.archive.org/web/20100414110624/http://hkclweb.hkpl.gov.hk/doc/internet/cht/18districts/tuenmun/photo.html)

[Category:屯門區公共屋邨](../Category/屯門區公共屋邨.md "wikilink")
[Category:香港已拆卸公共房屋](../Category/香港已拆卸公共房屋.md "wikilink")
[Category:屯門新墟](../Category/屯門新墟.md "wikilink")
[Category:以人名命名的公營房屋](../Category/以人名命名的公營房屋.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.  「[青山](../Page/青山_\(香港\).md "wikilink")」是「屯門」舊稱
2.  〈新城市內安置萬人 青山新區大廈本週入伙〉，《華僑日報》，1971-09-06