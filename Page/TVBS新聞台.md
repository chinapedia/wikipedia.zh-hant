[TVBS_News_microphone_2007.jpg](https://zh.wikipedia.org/wiki/File:TVBS_News_microphone_2007.jpg "fig:TVBS_News_microphone_2007.jpg")

**TVBS新聞台**，是屬於台灣TVBS旗下的電視新聞頻道，於1995年10月2日啟播，TVBS是臺灣第一個24小時本土[新聞頻道](../Page/新聞頻道.md "wikilink")；初名「TVBS
超級新聞網」（TVBS-Newsnet，別名「Super News」），是TVBS四個家族頻道中第三個啟播的頻道。

在台灣，本台被稱為「**TVBS-NEWS**」、「**TVBS-N**」，香港myTV SUPER稱為「**無線衛星新聞台**」\[1\]。

TVBS新聞部位於TVBS「[內湖大樓](../Page/TVBS企業總部.md "wikilink")」九、十樓，而播報整點新聞[攝影棚](../Page/攝影棚.md "wikilink")，位於「[內湖大樓](../Page/TVBS企業總部.md "wikilink")」一樓\[2\]；製播內容包括新聞報導、新聞回顧、娛樂資訊及時事論壇等。

## 沿革

以下時間以當地時間（[台灣時間](../Page/國家標準時間.md "wikilink")）為準

  - 1995年至2003年：頻道ID僅用TVBS-N的[LOGO動畫搭配](../Page/LOGO.md "wikilink")[TVBS的新聞開場音樂](../Page/TVBS.md "wikilink")，並無任何台呼。
  - 2004年：頻道ID使用另外製作的音樂，並加上台呼「您現在收看的是：TVBS-NEWS，新聞台」。
  - 2005年末：再將台呼改為「您所信賴的新聞頻道：TVBS-NEWS，新聞台」；更改台呼後，主播開場時也多會加上「歡迎收看您所信賴的新聞頻道，TVBS-NEWS（或TVBS-N）○○點新聞」。
  - 2007年1月1日：上午6點新聞起，頻道ID的台呼內容不變，但啟用新動畫，內有「體驗新台灣、全球新視野」的字樣。
  - 2006年開始：有時下午兩點至四點間會改成名為「特別報導」的政論談話性節目，由資深主播[何啟聖主持](../Page/何啟聖.md "wikilink")，邀請幾位來賓到攝影棚，並開放觀眾[Call-in](../Page/Call-in.md "wikilink")。此時段的新聞標題字數通常在10字內。後期有了「挑戰大新聞」的節目名稱。2007年，此時段改為2點的「[健康兩點靈](../Page/健康兩點靈.md "wikilink")」與3點的整點新聞，2008年再度改回選舉特別報導，原時段「[健康兩點靈](../Page/健康兩點靈.md "wikilink")」則配合[TVBS台改版計畫而搬移過去](../Page/TVBS_\(頻道\).md "wikilink")。
  - 2008年5月20日起：原「挑戰大新聞」時段改為播出談話性政論節目「九萬兆凱道」，後在2008年7月30日起改名為「NEWS我最大」，主持人為TVBS主播[顧名儀](../Page/顧名儀.md "wikilink")，節目型態維持不變。
  - 2009年4月1日起：「NEWS我最大」停播，原時段恢復播出整點新聞，並不定期播出深度報導節目。
  - 2006年5月21日起：原來在[TVBS首播的](../Page/TVBS_\(頻道\).md "wikilink")「[一步一腳印
    發現新台灣](../Page/一步一腳印_發現新台灣.md "wikilink")」專題性節目，改至TVBS-NEWS每周日22:00-23:00首播。
  - 2009年5月30日起：早上七點播報，搬遷「[內湖大樓](../Page/TVBS企業總部.md "wikilink")」的第一節新聞「早安台灣」，並啟用新攝影棚同步更換新聞報導的開頭[畫面](../Page/電視新聞鏡面.md "wikilink")。
  - 2009年6月9日：更名「**TVBS新聞台**」。
  - 2013年1月1日：TVBS新聞台進行大改革，啟用新動畫和新聞開場音樂。
  - 2013年5月14日：TVBS新聞台的高畫質版本開始播放，但新聞帶仍為普通畫質，並只有部分有線電視系統業者提供。
  - 2014年1月1日：TVBS新聞台的高畫質版本陸續在各大有線電視系統上架，截至2014年8月，已經有99.57%的系統業者提供高畫質版本的TVBS新聞台。
  - 2014年1月中旬：TVBS新聞台的標準畫質（SD）版本的採訪帶逐漸改為高畫質（HD）。
  - 2014年9月29日：TVBS新聞台鏡面設計進行更動，解除4:3的安全框限制，而改用適用於16:9的鏡面。
  - 2015年3月10日，[TVB賣出持有的TVBS全部股權](../Page/TVB.md "wikilink")。
  - 2016年1月11日至2016年1月17日：啟用新虛實合一的攝影棚且有一、二樓。
  - 2016年12月21日14點：更換台標和各節新聞的音樂。\[3\]
  - 2018年11月24日起：啟用新虛實合一的超大攝影棚並擁有雙電視牆及超大主播台。
  - 2018年11月26日起：TVBS新聞YouTube線上直播正式上線。

## 專題節目

### 自製類

  - 詹怡宜《**[一步一腳印 發現新台灣](../Page/一步一腳印_發現新台灣.md "wikilink")**》。
  - 莊開文《**[中國進行式](../Page/中國進行式.md "wikilink")**》。
  - 詹怡宜《**[奮起台灣](../Page/奮起台灣.md "wikilink")**》。

### 國際新聞專題

  - [TVB所採訪新聞](../Page/TVB.md "wikilink")，剪輯而成專題報導。
  - 購買[CNN](../Page/CNN.md "wikilink")、[NHK等外國媒體所採訪新聞](../Page/NHK.md "wikilink")，剪輯而成新聞專題報導。

### 突發事件或重大事件

  - 1999年9月：[九二一大地震特別報導](../Page/九二一大地震.md "wikilink")。
  - 2000年：[斐濟千禧第一道曙光](../Page/斐濟.md "wikilink")、同年的[總統大選](../Page/2000年中華民國總統選舉.md "wikilink")。
  - 2001年：迎接二十一世紀。
  - 2001年9月：[美國](../Page/美國.md "wikilink")[九一一事件特別報導](../Page/九一一.md "wikilink")。
  - 2001年11月：[美阿戰爭](../Page/美阿戰爭.md "wikilink")。
  - 2003年：[美伊戰爭](../Page/美伊戰爭.md "wikilink")。

### 合作專題報導

  - [NHK](../Page/日本放送協會.md "wikilink")：
      - 1999年：海洋台灣系列
      - 2001年：棋聖比賽
      - 2002年：[日本將棋比賽](../Page/日本.md "wikilink")
  - [中國中央電視台](../Page/中國中央電視台.md "wikilink")（[CCTV](../Page/中國中央電視台.md "wikilink")）：
      - 1999年：兩岸暨亞洲經濟展望研討會衛星雙向訪談節目
      - 2000年：兩岸尋親節目。
      - 2013年：世界最大潮－探索亞馬遜。

## 現任TVBS新聞部人員

### 管理層

|                   **管理層**                   |
| :-----------------------------------------: |
| [詹怡宜](../Page/詹怡宜.md "wikilink")（新聞部總監、主持人） |

### 專任主播

|             **專任主播**             |
| :------------------------------: |
| [夏嘉璐](../Page/夏嘉璐.md "wikilink") |
| [吳安琪](../Page/吳安琪.md "wikilink") |
| [張靖玲](../Page/張靖玲.md "wikilink") |
| [蔡孟樺](../Page/蔡孟樺.md "wikilink") |

### 氣象主播

|             **氣象主播**             |
| :------------------------------: |
| [任立渝](../Page/任立渝.md "wikilink") |

### 兼職主播

|             **兼職主播**             |
| :------------------------------: |
| [王德愷](../Page/王德愷.md "wikilink") |
| [蔣志偉](../Page/蔣志偉.md "wikilink") |
| [錢麗如](../Page/錢麗如.md "wikilink") |
|                                  |

### 說新聞記者

於上午及下午離峰時段，負責播報「說新聞」之新聞評述/分析，並與當節主播對談當日重點議題。

|               **新聞主播**                |
| :-----------------------------------: |
|   [葉佳蓉](../Page/葉佳蓉.md "wikilink")    |
|               **新聞記者**                |
|   [曾奕慈](../Page/曾奕慈.md "wikilink")    |
|   [柳采葳](../Page/柳采葳.md "wikilink")    |
|   [謝宜倫](../Page/謝宜倫.md "wikilink")    |
|   [畢倩涵](../Page/畢倩涵.md "wikilink")    |
| \-{[藍于洺](../Page/藍于洺.md "wikilink")}- |
|   [呂蓓君](../Page/呂蓓君.md "wikilink")    |
|                                       |

## 前任TVBS新聞部人員

  - [李四端](../Page/李四端.md "wikilink")（現職[東森新聞台](../Page/東森新聞台.md "wikilink")《李四端的雲端世界》節目主持人）
  - [何啟聖](../Page/何啟聖.md "wikilink")（現職1111人力銀行副總）
  - [王浩](../Page/王浩_\(台灣主播\).md "wikilink")（現任[台北市議員](../Page/台北市議員.md "wikilink")）
  - [姚崑崙](../Page/姚崑崙.md "wikilink")（曾任[中天新聞台主播](../Page/中天新聞台.md "wikilink")，現任LV中山旗艦店經理）
  - [蘇逸洪](../Page/蘇逸洪.md "wikilink")（前台視、華視主播）
  - [郭家群](../Page/郭家群.md "wikilink")（現為Dr. Wells維育牙醫診所副院長兼醫師）
  - [黃志豪](../Page/黃志豪.md "wikilink")（現為[寰宇新聞台主播](../Page/寰宇新聞台.md "wikilink")）
  - [周守訓](../Page/周守訓.md "wikilink")（前[立委](../Page/立委.md "wikilink")）
  - [金汝鑫](../Page/金汝鑫.md "wikilink")（轉戰[中天新聞台記者](../Page/中天新聞台.md "wikilink")）
  - [陳勝鴻](../Page/陳勝鴻.md "wikilink")（現職[愛爾達體育台球評](../Page/愛爾達體育台.md "wikilink")）
  - [沈畦](../Page/沈畦.md "wikilink")（氣象預報擔任）
  - [李金萬](../Page/李金萬.md "wikilink")（氣象預報擔任）
  - [謝維權](../Page/謝維權.md "wikilink")（氣象預報擔任）
  - [李御榮](../Page/李御榮.md "wikilink")（曾任[年代新聞](../Page/年代新聞.md "wikilink")、[中天新聞台主播](../Page/中天新聞台.md "wikilink")，現任職於[中國信託文教基金會](../Page/中國信託.md "wikilink")）
  - [岑永康](../Page/岑永康.md "wikilink")（曾任[壹電視新聞台當家主播](../Page/壹電視新聞台.md "wikilink")，現任[寰宇新聞](../Page/寰宇新聞.md "wikilink")《寰宇全視界》主持人）
  - [蕭子新](../Page/蕭子新.md "wikilink")（現職[年代電視新聞部及](../Page/年代電視.md "wikilink")[壹電視新聞部國際中心主任](../Page/壹電視.md "wikilink")&[壹電視](../Page/壹電視.md "wikilink")[無碼的世界主持人](../Page/無碼的世界.md "wikilink")。)
  - [薩文](../Page/薩文.md "wikilink")（前[鳳凰衛視主播](../Page/鳳凰衛視.md "wikilink")）
  - [李登文](../Page/李登文.md "wikilink")（現職[華視新聞國際中心](../Page/華視新聞.md "wikilink")）
  - [許甫](../Page/許甫.md "wikilink")（現[中天新聞台主播](../Page/中天新聞台.md "wikilink")）
  - 邱顯辰
  - [王博麟](../Page/王博麟.md "wikilink")（前[FOX體育台主播](../Page/FOX體育台.md "wikilink")）
  - 蔡祐吉（現任亞泥公關經理）

<!-- end list -->

  - [張雅琴](../Page/張雅琴.md "wikilink")（現任[1800年代晚報主播](../Page/1800年代晚報-挑戰新聞.md "wikilink")）
  - [陳雅琳](../Page/陳雅琳.md "wikilink")（現任[壹電視新聞台總編輯](../Page/壹電視新聞台.md "wikilink")、10點上新聞主播、新聞深呼吸主持人）
  - [黃寶慧](../Page/黃寶慧.md "wikilink")（現任[ETtoday新聞雲慧眼看天下](../Page/ETtoday新聞雲.md "wikilink")
    節目主持人)
  - [趙薇](../Page/趙薇_\(台灣主播\).md "wikilink")（2001年擔任[年代MUCH台](../Page/年代MUCH台.md "wikilink")《[台灣人在大陸](../Page/台灣人在大陸.md "wikilink")》節目主持人）
  - [鍾佩玲](../Page/鍾佩玲.md "wikilink")（現任台北市議員）
  - [張婯嬅](../Page/張婯嬅.md "wikilink")（現任[壹電視主播](../Page/壹電視.md "wikilink")）
  - [馬度芸](../Page/馬度芸.md "wikilink")
  - [蔣雅淇](../Page/蔣雅淇.md "wikilink")
  - [黃凱聖](../Page/黃凱聖.md "wikilink")
  - [王雅麗](../Page/王雅麗.md "wikilink")
  - [張恆芝](../Page/張恆芝.md "wikilink")
  - [李晶玉](../Page/李晶玉.md "wikilink")（現任[民視新聞台](../Page/民視新聞台.md "wikilink")[政經看民視主持人](../Page/政經看民視.md "wikilink"))
  - [王淑麗](../Page/王淑麗.md "wikilink")（現任[東森新聞台當家氣象主播](../Page/東森新聞台.md "wikilink")）
  - [王薇](../Page/王薇_\(台灣主播\).md "wikilink")（前[華視](../Page/華視.md "wikilink")、[三立](../Page/三立.md "wikilink")、[東森主播](../Page/東森.md "wikilink")）
  - [蔡郁潔](../Page/蔡郁潔.md "wikilink")（前華視、東森主播）
  - [蔡郁璇](../Page/蔡郁璇.md "wikilink")（前華視、東森主播）
  - [廖筱君](../Page/廖筱君.md "wikilink")（現任[三立新聞台節目主持人兼製作人](../Page/三立新聞台.md "wikilink")）
  - [薛楷莉](../Page/薛楷莉.md "wikilink")（曾轉任現職[藝人](../Page/藝人.md "wikilink")）
  - [王怡仁](../Page/王怡仁.md "wikilink")（曾轉任[藝人](../Page/藝人.md "wikilink")）
  - [潘彥妃](../Page/潘彥妃.md "wikilink")（在台北街頭賣咖啡）
  - [丁靜怡](../Page/丁靜怡.md "wikilink")（曾任[鴻海集團公共關係室主任](../Page/鴻海集團.md "wikilink")）
  - [葉宸安](../Page/葉宸安.md "wikilink")（2007年6月29日離職赴英進修後已退出新聞界）
  - [黃逸卿](../Page/黃逸卿.md "wikilink")（現為茶飲品牌總監）
  - [洪麗萍](../Page/洪麗萍.md "wikilink")（現為[東森購物的](../Page/東森購物.md "wikilink")[購物專家](../Page/購物專家.md "wikilink")）
  - [邱沁宜](../Page/邱沁宜.md "wikilink")（現專職東風衛視《單身行不行》主持人)
  - [王之梅](../Page/王之梅.md "wikilink")
  - [洪玟琴](../Page/洪玟琴.md "wikilink")
  - [黃靜雪](../Page/黃靜雪.md "wikilink")
  - [鍾靜如](../Page/鍾靜如.md "wikilink")（現任[85度C公關](../Page/85度C.md "wikilink")）
  - [李娜亞](../Page/李娜亞.md "wikilink")（退出新聞圈，移居美國）
  - [蔣珮棻](../Page/蔣珮棻.md "wikilink")
  - [蔣家宜](../Page/蔣家宜.md "wikilink")
  - [張珮珊](../Page/張珮珊.md "wikilink")（前[三立新聞台](../Page/三立新聞台.md "wikilink")《新聞54珊》主播）
  - [拉娃谷幸](../Page/拉娃谷幸.md "wikilink")（曾任財團法人原住民族文化事業基金會執行長）
  - [顧名儀](../Page/顧名儀.md "wikilink")（前[年代新聞台主播](../Page/年代新聞台.md "wikilink")，[中天新聞](../Page/中天新聞.md "wikilink")《選戰暴風眼》主持人）
  - [李文儀](../Page/李文儀.md "wikilink")（現任[三立新聞台](../Page/三立新聞台.md "wikilink")《[新聞深一度](../Page/新聞深一度.md "wikilink")》主播、《[我們一家人](../Page/我們一家人.md "wikilink")》主持人）
  - [詹慶齡](../Page/詹慶齡.md "wikilink")（現任[ETtoday新聞雲](../Page/ETtoday新聞雲.md "wikilink")《[史瑪特過生活](../Page/史瑪特過生活.md "wikilink")》節目主持人）
  - [廖盈婷](../Page/廖盈婷.md "wikilink")（現任Super Snow Show 網路節目主持人兼製作人)
  - [簡懿佳](../Page/簡懿佳.md "wikilink")
  - [谷庭](../Page/谷庭.md "wikilink")
  - [林秉儀](../Page/林秉儀.md "wikilink")
  - [張愛晶](../Page/張愛晶.md "wikilink")
  - [鍾沛君](../Page/鍾沛君.md "wikilink")（現任台北市議員）
  - [林婉婷](../Page/林婉婷.md "wikilink")
  - [華舜嘉](../Page/華舜嘉.md "wikilink")
  - [廖芳潔](../Page/廖芳潔.md "wikilink")（現任[三立新聞台](../Page/三立新聞台.md "wikilink")《[台灣大頭條](../Page/台灣大頭條.md "wikilink")》主播）

## 爭議

## 參考著作

|               |               |                                                                   |                                        |                    |
| ------------- | ------------- | ----------------------------------------------------------------- | -------------------------------------- | ------------------ |
| 年份            | 書名            | 作者                                                                | 出版社                                    | ISBN               |
| 1999年12月21日初版 | 《921 TVBS總動員》 | [甯育華](../Page/甯育華.md "wikilink")、[甯瑋瑜](../Page/甯瑋瑜.md "wikilink") | [英特發](../Page/英特發股份有限公司.md "wikilink") | ISBN 957-97-6212-0 |
|               |               |                                                                   |                                        |                    |

## 相關條目

  - [台灣媒體爭議](../Page/台灣媒體爭議.md "wikilink")
  - [台灣電視台列表](../Page/台灣電視台列表.md "wikilink")
  - [TVBS](../Page/TVBS.md "wikilink")

## 參考資料

{{ external media | align = right | width = 200px | image1 =
[TVBS新聞台《上午11點新聞》的截圖](http://lh4.googleusercontent.com/-IpBAlRNfS-s/UM2Jv04jLRI/AAAAAAAAAMY/LSLGwHXdvdE/s326/news-1100.jpg)（2009年10月）
| image2 = | image3 = | audio1 = | audio2 = | audio3 = | video1 = |
video2 = | video3 = }}

## 外部連結

  - [TVBS新聞網](http://news.tvbs.com.tw)

  -
  -
[Category:1995年成立的电视台或电视频道](../Category/1995年成立的电视台或电视频道.md "wikilink")
[Category:聯利媒體電視頻道](../Category/聯利媒體電視頻道.md "wikilink")
[Category:電視廣播有限公司新聞電視頻道](../Category/電視廣播有限公司新聞電視頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")

1.  [TVB收費電視《無線衛星新聞台》](http://www.tvbpayvision.com/channel/details/14/).\[2010-02-13\]

2.  許晉榮.[TVBS搞透明新聞棚
    詹慶齡播報獨享閉關](http://www.appledaily.com.tw/appledaily/article/entertainment/20091016/32019644).蘋果日報.2009-10-16\[2010-02-13\]
3.  [TVBS新風貌！　全新logo亮相「新舊媒體融合」](http://news.tvbs.com.tw/news/detail/inter-news/694839).TVBS新聞網.2016-12-21