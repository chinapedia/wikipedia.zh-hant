**氟矽酸銨**或稱**六氟硅酸铵**，是一種白色的結晶體，[分子式為](../Page/分子式.md "wikilink")(NH<sub>4</sub>)<sub>2</sub>\[SiF<sub>6</sub>\]，易溶于水，几乎不溶于[醇和](../Page/醇.md "wikilink")[丙酮](../Page/丙酮.md "wikilink")。主要用於[玻璃雕刻](../Page/玻璃.md "wikilink")，木材防腐剂等，另外它可以製造人造[冰晶石](../Page/冰晶石.md "wikilink")、[氯酸铵等](../Page/氯酸铵.md "wikilink")。

## 危險性

氟矽酸銨有毒，对[眼睛](../Page/眼睛.md "wikilink")、[皮肤和上](../Page/皮肤.md "wikilink")[呼吸道有强烈刺激作用](../Page/呼吸道.md "wikilink")，另外如果不慎食入，短期症狀會造成，[口腔](../Page/口腔.md "wikilink")、[咽喉](../Page/咽喉.md "wikilink")、[鼻烧伤并出血](../Page/鼻.md "wikilink")。而食入2\~5g會致死。\[1\]

## 参考文献

[Category:銨鹽](../Category/銨鹽.md "wikilink")
[Category:氟硅酸盐](../Category/氟硅酸盐.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.