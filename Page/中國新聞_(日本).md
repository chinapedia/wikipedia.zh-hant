[Chugoku_Shimbun_Headquarters_2.jpg](https://zh.wikipedia.org/wiki/File:Chugoku_Shimbun_Headquarters_2.jpg "fig:Chugoku_Shimbun_Headquarters_2.jpg")
**中国新聞社**（報紙為《**中國新聞**》）1892年於[廣島市創立並發行報紙](../Page/廣島市.md "wikilink")。當時其業務為以廣島縣為中心，於[山口縣](../Page/山口縣.md "wikilink")、[島根縣](../Page/島根縣.md "wikilink")、[岡山縣發行及販賣早報及晚報](../Page/岡山縣.md "wikilink")。1970年左右曾把一部份刊物委託[日經在](../Page/日本經濟新聞.md "wikilink")[鳥取縣的零售店銷售](../Page/鳥取縣.md "wikilink")，稱為鳥取版，但因為銷售結果不好而取消。該社為廣島縣的地方報紙代表。

該社也記錄了相當多攸關[廣島市的事件](../Page/廣島市.md "wikilink")，比如[原爆問題或](../Page/原子彈.md "wikilink")[馬自達相關的主題報導與評論](../Page/馬自達.md "wikilink")。對於不以中立姿態報導的新聞，反對原爆問題或[和平等相關議題](../Page/和平.md "wikilink")，也都會給予疑問及意見，因此當時讀者常常有批判性的投稿（因為廣島縣為原子彈受害縣）。該社還做了知名的[暴力團追放Campaign的專題](../Page/暴力團追放Campaign.md "wikilink")。

2005年11月，該社在[廿日市市成立中國新聞廣島製作中心](../Page/廿日市市.md "wikilink")，通稱，擁有24個面的全彩印刷機。

目前該社除了[廣島市總部以外](../Page/廣島市.md "wikilink")，也在[福山市](../Page/福山市.md "wikilink")、[周南市](../Page/周南市.md "wikilink")（舊稱德山市）發行報紙。

## 沿革

  - 1892年　日報「中國」創刊。
  - 1908年　刊刊頭為「中國新聞」。
  - 1931年　創設日本中國地區車站傳記（日本語：[中国駅伝（広島～福山間）](../Page/中国駅伝（広島～福山間）.md "wikilink")）。
  - 1942年　為紀念創刊五十週年，創立[中国文化獎](../Page/中国文化獎.md "wikilink")。
  - 1945年　美軍在廣島投下[原子彈](../Page/原子彈.md "wikilink")，總部受損。轉而請求[朝日新聞代為印製](../Page/朝日新聞.md "wikilink")，該社僅僅休刊2天，從9日再度發行報紙。
  - 1948年　設立[鈴木三重吉獎](../Page/鈴木三重吉獎.md "wikilink")。
  - 1952年　設立[中国体育文化獎](../Page/中国体育文化獎.md "wikilink")。
  - 1959年　連載專題「[瀨戶內海](../Page/瀨戶內海.md "wikilink")」得到新聞協会獎。
  - 2005年　廿日市市大野中的新印刷工場廣島制作中心（）投入運作。

## 中國新聞總社

  - 廣島本社　廣島縣廣島市中区土橋町7-1
  - 備後本社　廣島縣福山市御門町3-2-13
  - 防長本社　山口縣周南市御幸通2-22

## 支社

  -
    [東京](../Page/東京都.md "wikilink")、[大阪](../Page/大阪市.md "wikilink")、[呉](../Page/呉市.md "wikilink")

## 總局

  -
    [岩国](../Page/岩国市.md "wikilink")

## 支局

  -
    西広島、北広島、[三原](../Page/三原市.md "wikilink")、[尾道](../Page/尾道市.md "wikilink")、[山口](../Page/山口市.md "wikilink")、[鳥取](../Page/鳥取市.md "wikilink")、[松江](../Page/松江市.md "wikilink")、[濱田](../Page/濱田市.md "wikilink")、[益田](../Page/益田市.md "wikilink")、[岡山](../Page/岡山市.md "wikilink")、[井原](../Page/井原市.md "wikilink")、[笠岡](../Page/笠岡市.md "wikilink")、[福岡](../Page/福岡市.md "wikilink")、等

## 關係企業・事業

## 放送欄

### 廣島縣版

#### 主要版面（最終項）

  - 通常版

<!-- end list -->

  - [NHK総合](../Page/NHK総合電視台.md "wikilink")（D1）
  - [NHK教育](../Page/NHK教育電視台.md "wikilink")（D2）
  - [RCC](../Page/中国放送.md "wikilink")（D3）
  - [広島電視台](../Page/廣島電視台.md "wikilink")（D4）
  - [HOME](../Page/廣島HOME電視台.md "wikilink")（D5）
  - [TSS](../Page/新廣島電視台.md "wikilink")（D8）

<!-- end list -->

  - 半分版

<!-- end list -->

  - [NHK衛星第1](../Page/NHK衛星第1テレビジョン.md "wikilink")
  - [NHK衛星第2](../Page/NHK衛星第2テレビジョン.md "wikilink")

### 山口縣版

#### 主要版面（最終面）

  - 通常版

<!-- end list -->

  - NHK総合
  - NHK教育
  - [山口放送](../Page/山口放送.md "wikilink")（D4）
  - [山口電視台](../Page/山口電視台.md "wikilink")（D3）
  - [山口朝日](../Page/山口朝日放送.md "wikilink")（D5）
  - [西日本電視台](../Page/西日本電視台.md "wikilink")（D8）

<!-- end list -->

  - 半分版

<!-- end list -->

  - [TVQ](../Page/TVQ九州放送.md "wikilink")（D7）
  - [九州朝日](../Page/九州朝日放送.md "wikilink")（D1）
  - RCC
  - 廣島電視台

<!-- end list -->

  - 小版

<!-- end list -->

  - [RKB每日](../Page/RKB每日放送.md "wikilink")（D4）
  - [大分](../Page/大分放送.md "wikilink")（D3）
  - [大分電視台](../Page/大分電視台.md "wikilink")（D4）
  - [WOWOW](../Page/WOWOW.md "wikilink")

## 外部連結

  - [中国新聞](http://www.chugoku-np.co.jp/)

[Z中](../Category/日本報紙.md "wikilink")
[Category:廣島縣公司](../Category/廣島縣公司.md "wikilink")
[Category:1892年建立](../Category/1892年建立.md "wikilink") [Category:中區
(廣島市)](../Category/中區_\(廣島市\).md "wikilink")