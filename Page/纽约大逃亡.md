《**纽约大逃亡**》（）是一部1981年[美國](../Page/美國.md "wikilink")[反烏托邦](../Page/反烏托邦.md "wikilink")[科幻](../Page/科幻片.md "wikilink")[動作片](../Page/動作片.md "wikilink")，由[約翰·卡本特執導](../Page/約翰·卡本特.md "wikilink")，[寇特·羅素主演](../Page/寇特·羅素.md "wikilink")。

卡本特於1970年代中期開始撰寫本片的劇本，由於《[月光光心慌慌](../Page/月光光心慌慌.md "wikilink")》的成功，使得他得以開始製作電影;成本只約600萬美元，票房卻達到成功的2050萬美元。\[1\]\[2\]。編劇尼克·坎斯托先前曾與卡本特共同在《月光光心慌慌》中合作創作恐怖殺人魔「」（Michael
Myers）。

《纽约大逃亡》於1981年7月10日在美國上映。該片贏得了普遍好評與成功的票房\[3\]。電影提名了四項[土星獎](../Page/土星獎.md "wikilink")，其中包括[最佳科幻電影和](../Page/土星獎最佳科幻電影得獎列表.md "wikilink")。《纽约大逃亡》後來成為了一部[邪典電影](../Page/邪典電影.md "wikilink")，並於1996年推出了續集《[洛杉磯大逃亡](../Page/洛杉磯大逃亡.md "wikilink")》。

## 劇情

1997年，美國犯罪率上升400%，[紐約市淪為重刑犯監獄](../Page/紐約市.md "wikilink")，已變成廢墟；不巧美國總統又因飛行事故墜落於市內遭綁架。使得重刑犯[大蛇·普利斯金](../Page/大蛇·普利斯金.md "wikilink")（[寇特·羅素](../Page/寇特·羅素.md "wikilink")
飾）被要求將功贖罪，潛入紐約於二十四小時內救出總統，否則動脈內的微形炸藥會爆炸。片中充斥許多光怪陸離的劇情發展，對於紐約市[物質文明發達形同美國社會](../Page/物質文明.md "wikilink")[拜金主義縮影](../Page/拜金主義.md "wikilink")，卻也製造出不少畸形社會現象予以深刻批判。

## 演員

  - [寇特·羅素](../Page/寇特·羅素.md "wikilink") 飾
    [大蛇·普利斯金](../Page/大蛇·普利斯金.md "wikilink")（Snake
    Plissken）：退伍軍人，重刑犯。其「大蛇」聲名響亮，被霍克找來要求救出總統。

  - [李·范·克里夫](../Page/李·范·克里夫.md "wikilink") 飾 巴布·霍克（Bob
    Hauk）：前德州響雷部隊隊員，為警隊負責人。

  - [歐尼斯·鮑寧](../Page/歐尼斯·鮑寧.md "wikilink") 飾
    卡畢（Cabbie）：紐約市的罪犯，非常崇拜大蛇，並從吃人幫中救出大蛇。

  - [唐納·普萊森斯](../Page/唐納·普萊森斯.md "wikilink") 飾
    [美國總統](../Page/美國總統.md "wikilink")：帶著關鍵的錄音帶，在乘坐的空軍一號墜毀後，遭到公爵挾持。

  - [艾薩克·海耶斯](../Page/艾薩克·海耶斯.md "wikilink") 飾
    [紐約市公爵](../Page/紐約市.md "wikilink")（The Duke of New
    York City）：紐約市權力最高、最有錢的人，罪犯們的領導；綁架總統目的是威脅政府釋放所有囚犯。

  - 飾 哈羅德·「布萊恩」·瑟曼（Harold "Brain"
    Hellman）：大蛇的前隊友，在堪薩斯城放了大蛇鴿子；擁有逃出曼哈頓的關鍵地雷圖。

  - 飾 瑪姬（Maggie）：瑟曼的妻子。

  - 飾 電腦語音（未掛名）

  - [潔美·李·寇蒂斯](../Page/潔美·李·寇蒂斯.md "wikilink") 飾 前言敘述者（未掛名）

## 反響

《纽约大逃亡》於1981年7月10日在紐約和洛杉磯上映\[4\]，在夏季，美國票房總計2520萬美元\[5\]。

電影獲得的評價以正面居多。[爛番茄基於](../Page/爛番茄.md "wikilink")53篇評論上，新鮮度高達86%，IMDb平均得分6.9／10\[6\]。多數影評人稱讚《纽约大逃亡》採用的骯髒都市的背景和充滿娛樂性的驚險動作是相當古怪、有趣的，且也認為該片有著[B級片風格的優勢](../Page/B級片.md "wikilink")。[寇特·羅素的演出獲得了好評](../Page/寇特·羅素.md "wikilink")，使得羅素為此成名。

## 續集

續集《洛杉磯大逃亡》於1996年推出，約翰·卡本特和寇特·羅素都回歸分別擔任導演和主演。

## 重拍版

2007年3月13日，宣布了[傑拉德·巴特勒即將在未來的](../Page/傑拉德·巴特勒.md "wikilink")《紐約大逃亡》重拍版中擔任全新的大蛇·普利斯金一角，但似乎很多迷不會認同寇特以外的演員\[7\]\[8\]\[9\]。截至2008年6月一直流傳著這個傳言，[佐斯·布連對此事和大蛇的發展也有參與](../Page/佐斯·布連.md "wikilink")，這在「開發地獄」中結束。2010年，根據紐約雜誌，計劃重回正軌，一個新的重寫。負責《[華爾街：金錢萬歲](../Page/華爾街：金錢萬歲.md "wikilink")》的Allan
Loeb表示出了其腳本。據報導，2013年3月24日，英國演員[傑森·史塔森和](../Page/傑森·史塔森.md "wikilink")[湯姆·哈迪是符合大蛇這個角色的影星](../Page/湯姆·哈迪.md "wikilink")。

該項目因後來長期無消息被認為已取消。2015年10月12日，宣布《[路德探長](../Page/路德探長.md "wikilink")》的創作者將為電影撰寫劇本\[10\]。

2016年12月1日，據TheWrap報導，二十世紀福斯將重拍本片，打算將《紐約大逃亡》拍成類似《[人猿星球](../Page/人猿星球.md "wikilink")》風格的電影，但紐約不再是一座高戒備監獄而是座綠意盎然、充滿未來科技的美麗城市，世界其餘的地區則處於危機狀態；此外，大蛇將會有本名和頭銜：「羅柏·大蛇·普利斯金上校」（Colonel
Robert Snake
Plissken）\[11\]。2017年3月，據傳，[勞勃·羅里葛茲有望執導該重拍電影](../Page/勞勃·羅里葛茲.md "wikilink")。

## 刪減片段與幕後趣事

  - 《紐約大逃亡》刪減片段：

片頭仍有一段被刪減的銀行搶劫片段，作為列入考量的電影開場，大蛇與戰友泰勒合作搶劫紐約市中央銀行；後段遭到出賣而被警方圍捕，泰勒因中槍與腿疾無法及時逃跑，先行逃離的大蛇則為了朋友再度回頭，在泰勒勸他離開的狀況下看著泰勒被槍殺，自己則遭到逮捕。

  - 與各大經典電影的淵源

當時電影中的背景採用實體拍攝手法，而製作紐約市高級監獄縮小模型的正是今日的知名電影導演[詹姆斯卡麥隆](../Page/詹姆斯卡麥隆.md "wikilink")，而後該座"紐約市"也經輾轉出現在[雷利史考特導演的經典科幻電影](../Page/雷利史考特.md "wikilink")《[銀翼殺手](../Page/銀翼殺手.md "wikilink")》。

  - 晚輩的致敬

電影海報設計知名的一幕：自由女神像的頭部跌落大街，象徵紐約大逃亡特有的反烏托邦情節，雖然電影中很遺憾無出現該片段；《[科洛弗檔案](../Page/科洛弗檔案.md "wikilink")》其中一幕正以此手法巧妙致敬電影中分崩離析自由衰敗的紐約高級監獄。

  - 主角是真實人物

導演卡本特表示："大蛇·普利斯金"是真有其人，為卡本特中學時期的同學，其真名和主角相同;就連個性、體格、髮型、說話方式（除了獨眼之外）都與本片主角有著可怕的相似。

## 參考文獻

## 外部連結

  -
  -
  -
  - [Escape from New York at Official John Carpenter's
    Website](https://web.archive.org/web/20041010064331/http://www.theofficialjohncarpenter.com/pages/themovies/ny/ny.html)

[Category:1980年电影](../Category/1980年电影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1980年代動作片](../Category/1980年代動作片.md "wikilink")
[Category:1980年代科幻片](../Category/1980年代科幻片.md "wikilink")
[Category:美國科幻動作片](../Category/美國科幻動作片.md "wikilink")
[Category:反烏托邦電影](../Category/反烏托邦電影.md "wikilink")
[Category:發生或曾經發生第三次世界大戰作品](../Category/發生或曾經發生第三次世界大戰作品.md "wikilink")
[Category:文明崩潰後世界題材電影](../Category/文明崩潰後世界題材電影.md "wikilink")
[Category:約翰·卡本特電影](../Category/約翰·卡本特電影.md "wikilink")
[Category:1997年背景電影](../Category/1997年背景電影.md "wikilink")
[Category:紐約市背景電影](../Category/紐約市背景電影.md "wikilink")
[Category:密蘇里州取景電影](../Category/密蘇里州取景電影.md "wikilink")
[Category:紐約市取景電影](../Category/紐約市取景電影.md "wikilink")
[Category:邪典电影](../Category/邪典电影.md "wikilink")

1.
2.

3.
4.
5.
6.

7.  [IGN: Kurt Blasts Escape
    Remake](http://movies.ign.com/articles/775/775013p1.html)

8.  [edmontonsun.com - Showbiz: Movies, TV and Theatre - Snake Plissken
    hissing over Escape From New York
    remake](http://www.edmontonsun.com/Entertainment/Showbiz/2007/04/04/3915117-sun.html)

9.  [NEWS RUSSELL ENRAGED WITH NEW SNAKE PLISSKEN Music, movie &
    Entertainment
    News](http://www.pr-inside.com/russell-enraged-with-new-snake-plissken-r75552.htm)


10.

11.