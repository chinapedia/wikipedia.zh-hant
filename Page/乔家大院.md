**乔家大院**，位于[山西省](../Page/山西省.md "wikilink")[祁县乔家堡村](../Page/祁县.md "wikilink")，是[清代商业金融资本家](../Page/清朝.md "wikilink")[乔致庸的宅第](../Page/乔致庸.md "wikilink")\[1\]。始建于清代[乾隆年间](../Page/乾隆.md "wikilink")，以后曾有两次增修，一次扩建，于[民国初年建成了一座宏伟的建筑群体](../Page/民国.md "wikilink")，体现了中国清代北方民居的典型风格。

1985年，当地政府在古宅的基础上建成了祁县民俗博物馆，1986年11月1日开馆，正式对外开放，2001年，乔家大院被国务院命名为[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")，2001年，被国家旅游局评定为AAAAA级旅游景区。

<File:Jingyi> House.JPG|电影《大红灯笼高高挂》的拍摄场地山西乔家大院之静怡院 <File:Tuisi>
House.jpg|山西乔家大院之退思院 <File:Bilin> compound.JPG|山西乔家大院之碧琳院
[File:乔家大院黄椽木浮雕九龙壁.JPG|乔家大院黄椽木浮雕九龙壁](File:乔家大院黄椽木浮雕九龙壁.JPG%7C乔家大院黄椽木浮雕九龙壁)
[File:乔家大院砖雕.jpg|乔家大院砖雕](File:乔家大院砖雕.jpg%7C乔家大院砖雕)
[File:乔家大院砖雕3.JPG|乔家大院砖雕](File:乔家大院砖雕3.JPG%7C乔家大院砖雕)

## 参考文献

## 外部链接

  -
## 参见

  - [乔家大院 (电视剧)](../Page/乔家大院_\(电视剧\).md "wikilink")

{{-}}

[Category:山西全国重点文物保护单位](../Category/山西全国重点文物保护单位.md "wikilink")
[Q](../Category/中国民居.md "wikilink")
[Category:国家5A级旅游景区](../Category/国家5A级旅游景区.md "wikilink")
[Q](../Category/祁县建筑.md "wikilink")
[Category:山西清代建築](../Category/山西清代建築.md "wikilink")
[Category:中国民俗博物馆](../Category/中国民俗博物馆.md "wikilink")

1.  李艺. 乔家大院——大红灯笼点亮的晋商传奇\[J\]. 旅游时代, 2013, 9: 010.