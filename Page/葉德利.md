**葉德利**（），[印尼](../Page/印尼.md "wikilink")[棉蘭出生](../Page/棉蘭.md "wikilink")，[廣東省](../Page/廣東省.md "wikilink")[梅縣人](../Page/梅縣區.md "wikilink")，[賽車手](../Page/賽車手.md "wikilink")，[德利賽車隊創辦人](../Page/德利賽車隊.md "wikilink")。

## 生平

葉德利的祖先曾移民[印尼從商](../Page/印尼.md "wikilink")，1940年代在[越南](../Page/越南.md "wikilink")[西貢認識](../Page/胡志明市.md "wikilink")[何世光的第六女兒](../Page/何世光.md "wikilink")[何婉婉](../Page/何婉婉.md "wikilink")，並結婚。後葉德利重返香港，在[英資的](../Page/英資.md "wikilink")[國民收銀機公司任職](../Page/國民收銀機公司.md "wikilink")。1950年代與賭王[葉漢在越南西貢開設](../Page/葉漢.md "wikilink")[賭場](../Page/賭場.md "wikilink")，但後來與葉漢反臉，於1970年代與[何鴻燊](../Page/何鴻燊.md "wikilink")(何婉婉之兄)在[澳門合作競投新賭場合約](../Page/澳門.md "wikilink")，並合資興建今日的[葡京酒店及賭場](../Page/葡京酒店.md "wikilink")。

葉德利曾是香港有名的[花花公子](../Page/花花公子.md "wikilink")，熱愛賽車，在澳門有「格蘭披治先生」之稱。1955年首次參與[澳門格蘭披治大賽車](../Page/澳門格蘭披治大賽車.md "wikilink")，雖然他最終由於練習期間的意外而未能完成該屆賽事，但卻使他與賽車結下不解之緣，由當初起每年以車手身份參賽，1970年代初贊助[一級方程式賽車前身](../Page/一級方程式賽車.md "wikilink")—[Formula
5000國際賽](../Page/Formula_5000.md "wikilink")。及至1976年創辦[德利車隊](../Page/德利車隊.md "wikilink")[Theodore
Racing](../Page/Theodore_Racing.md "wikilink")，由參加[澳門格蘭披治大賽車](../Page/澳門格蘭披治大賽車.md "wikilink")、到贊助北美[印第方程式IndyCar500以至參加](../Page/印第方程式IndyCar500.md "wikilink")[一級方程式賽車](../Page/一級方程式賽車.md "wikilink")，使他成為國際賽車事業的殿堂級人物。

由於葉德利與香港及澳門的密切關係，澳門的「東望洋跑道」向被認為是[德利賽車隊的主場](../Page/德利賽車隊.md "wikilink")，而葉德利於澳門松山的別墅亦是「東望洋跑道」傍。但除參與澳門的格蘭披治大賽外，[德利賽車隊其實亦是首支由華人擁有的一級方程式車隊](../Page/德利賽車隊.md "wikilink")，並分別於1978年、1981年、1982年及、1983年參賽，雖然葉德利後來由於接納[Ensign
team的合併計劃](../Page/Ensign_team.md "wikilink")，而未再以[德利賽車隊的名義出戰](../Page/德利賽車隊.md "wikilink")[一級方程式賽車](../Page/一級方程式賽車.md "wikilink")，但他對[一級方程式賽車的貢獻卻未有停止](../Page/一級方程式賽車.md "wikilink")。

1983年在葉德利的積極推動下，澳門的方程式賽事由[太平洋方程式](../Page/太平洋方程式.md "wikilink")—[Formula
Pacific正式改制為](../Page/Formula_Pacific.md "wikilink")[三級方程式賽車](../Page/三級方程式賽車.md "wikilink")，[德利賽車隊邀請了仍屬新人的](../Page/德利賽車隊.md "wikilink")[艾爾頓·冼拿](../Page/艾爾頓·冼拿.md "wikilink")
(Ayrton Senna da
Silva)代表出戰，並取得冠軍，冼拿其後更晉身[一級方程式賽車成為車壇傳奇](../Page/一級方程式賽車.md "wikilink")，包括及後成為[一級方程式賽車冠軍的](../Page/一級方程式賽車.md "wikilink")[米高·夏健倫與及多位車壇名將](../Page/米高·夏健倫.md "wikilink")，亦曾效力[德利車隊並且奠定了澳門賽車作為世界級車手搖籃的地位](../Page/德利車隊.md "wikilink")，而德利車隊在澳門格蘭披治大賽共奪得6次冠軍。

葉德利曾成為全世界單座位賽車最年長車手、澳門[大賽車博物館有專區展出與葉德利有關的展品](../Page/大賽車博物館.md "wikilink")。葉德利於2003年逝世享年90歲，澳門政府為向他對澳門賽車事業所作的貢獻，特於當年的[五十屆澳門格蘭披治大賽車舉行特別儀式向他致意](../Page/五十屆澳門格蘭披治大賽車.md "wikilink")。

葉德利是[澳門旅遊娛樂有限公司股東之一](../Page/澳門旅遊娛樂有限公司.md "wikilink")，曾持有10%股權，1992年出售予[何鴻燊](../Page/何鴻燊.md "wikilink")，套現6億港元。

2003年7月11日，葉德利病逝於香港[瑪麗醫院](../Page/瑪麗醫院.md "wikilink")\[1\]。

## 參考

## 相關條目

  - [何啟東家族](../Page/何啟東家族.md "wikilink")

[Category:梅縣區人](../Category/梅縣區人.md "wikilink")
[Category:棉兰人](../Category/棉兰人.md "wikilink")
[Category:澳门企业家](../Category/澳门企业家.md "wikilink")
[Category:中國賽車手](../Category/中國賽車手.md "wikilink")
[Category:博彩業企業家](../Category/博彩業企業家.md "wikilink")
[Category:印度尼西亚华人](../Category/印度尼西亚华人.md "wikilink")
[D](../Category/叶姓.md "wikilink")

1.