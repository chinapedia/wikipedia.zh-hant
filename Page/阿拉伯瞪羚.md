**阿拉伯瞪羚**（學名*Gazella
arabica*）是種謎樣的[瞪羚](../Page/瞪羚.md "wikilink")，在其[中東的棲地](../Page/中東.md "wikilink")[沙地阿拉伯被獵殺至滅絕](../Page/沙地阿拉伯.md "wikilink")，目前世界僅透過1825年在[紅海的](../Page/紅海.md "wikilink")[發拉森島嶼上的標本了解這種動物](../Page/發拉森島嶼.md "wikilink")。阿拉伯瞪羚於1996年在[世界自然保護聯盟瀕危物種紅色名錄中被列為已滅絕的物種](../Page/世界自然保護聯盟瀕危物種紅色名錄.md "wikilink")。\[1\]

## 參考

<div class="references-small">

<references/>

</div>

  -
## 外部連結

  - [The Extinction Website - Species Info - Arabian
    Gazelle](https://web.archive.org/web/20070311095316/http://www.petermaas.nl/extinct/speciesinfo/arabiangazelle.htm)

[GA](../Category/IUCN絕滅物種.md "wikilink")
[A](../Category/瞪羚属.md "wikilink")
[GA](../Category/已灭绝动物.md "wikilink")

1.