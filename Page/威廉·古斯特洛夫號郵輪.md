**威廉·古斯特洛夫號**（）是一艘[納粹德國](../Page/納粹德國.md "wikilink")[郵輪](../Page/郵輪.md "wikilink")，由[布羅姆與沃斯船塢](../Page/布洛姆－福斯.md "wikilink")（Blohm
+
Voss）所建造，她的名字是來自[納粹黨](../Page/納粹黨.md "wikilink")[瑞士分部的領袖](../Page/瑞士.md "wikilink")，於1937年5月5日下水。在[二戰末期](../Page/二戰.md "wikilink")，此船用來載運遭[蘇聯紅軍圍困在](../Page/蘇聯紅軍.md "wikilink")[東普魯士的](../Page/東普魯士.md "wikilink")[德國人](../Page/德國人.md "wikilink")（包括平民及少數官兵），最後在1945年1月30日於[波羅的海被蘇軍](../Page/波羅的海.md "wikilink")[潛艇發射三枚](../Page/潛艇.md "wikilink")[魚雷擊沉](../Page/魚雷.md "wikilink")。遇難人數估計由5,300至9,931人不等\[1\]\[2\]，超過[泰坦尼克号的傷亡人數六倍](../Page/泰坦尼克號.md "wikilink")，成為歷史上遇難人數最多的[海難](../Page/海難列表.md "wikilink")。湊巧的是，當天正是威廉·古斯特洛夫50歲冥壽，也是[希特勒上台的](../Page/希特勒.md "wikilink")12週年。

## 歷史

這艘船原是作為納粹德國“[力量来自欢乐](../Page/力量来自欢乐.md "wikilink")”组织（，缩写KdF，一个專責為工人提供文娛康樂活動的工會組织）的郵輪，價值約為二千五百萬[帝國馬克](../Page/帝國馬克.md "wikilink")（RM）。直至1939年為止，威廉·古斯特洛夫號是KdF的郵船隊的[旗艦](../Page/旗艦.md "wikilink")。

1939年9月，納粹德國開始對[歐洲各國發動侵略](../Page/歐洲.md "wikilink")，威廉·古斯特洛夫號被軍隊徵用作[醫療船至](../Page/醫療船.md "wikilink")1940年11月。之後這艘船被用作[運兵船](../Page/運兵船.md "wikilink")，負責運載[U-潛艇的見習乘員到東普魯士](../Page/U-潛艇.md "wikilink")[但澤灣](../Page/但澤灣.md "wikilink")（，現[波蘭](../Page/波蘭.md "wikilink")[格但斯克灣](../Page/格但斯克灣.md "wikilink")（Bay
of
Gdańsk））畔的[哥德哈芬港](../Page/哥德哈芬.md "wikilink")（，現波蘭[格丁尼亞](../Page/格丁尼亞.md "wikilink")（））接受訓練。

## 沉沒

### 最後一程

威廉·古斯特洛夫號最後的任務是由格丁尼亞撤離德國平民、傷兵及海員經波羅的海返回德國[基爾](../Page/基爾.md "wikilink")（Kiel）港，船上已登記的海員及乘客總數是6,050人，但這數字並未計算沒有紀錄的數千名[難民](../Page/難民.md "wikilink")。於1980及1990年代對威廉·古斯特洛夫號沉沒事件作出過深入的研究，結論出當時船上搭載著173名船員（海軍武裝輔助隊）、918名第二潛艇訓練師（2.Unterseeboot-Lehrdivision）的士官及士兵、373名女性海軍醫護人員、162名受重傷的士兵及8,956名難民（當中只有4,424名難民有紀錄），合共10,582人。\[3\]這艘船本來的設計只能搭載2,000人以下，但是在充分利用所有船上的公共空間及遊樂設施後，就可以搭載上述人數作短程航行。也就是因為空間有限，船上不能帶太多救生設備，數量只夠少過半數人使用。

1945年1月30日的早上，威廉·古斯特洛夫號離開格丁尼亞港，隨行的船隻有滿載難民的（Hansa）和兩艘[魚雷艇](../Page/魚雷艇.md "wikilink")。漢薩號和其中一艘魚雷艇發生故障後不能繼續航行，只剩下另外一艘魚雷艇獅子號（Löwe）\[4\]負責護送威廉·古斯特洛夫號。船上有四位[船長](../Page/船長.md "wikilink")，其中三位是民航船長，一位是海軍船長，他們在決定航線上意見出現分歧。曾當潛艇乘員的威廉·扎恩海軍中尉（Lieutenant
Commander Wilhelm Zahn）建議把船駛在近岸的淺水中並關掉所有燈火，以避免受蘇軍潛艇伏擊;
經驗較豐富的總船長弗德里希·彼德遜（Friedrich
Petersen）則持不同看法，決定把船駛出深水區。當他從[無線電得知德國](../Page/無線電.md "wikilink")[掃雷艦正迎面駛來時](../Page/掃雷艦.md "wikilink")，彼德遜決定開啟船上紅綠兩色的[導航燈以避免相撞](../Page/導航燈.md "wikilink")。但這個錯誤的決定使威廉·古斯特洛夫號的位置在夜間暴露出來。

由領導的蘇軍[S级潛艇](../Page/S级潛艇.md "wikilink")在德軍忽略下冒險駛到但澤灣找尋建功的好機會，果然讓他在格羅森多夫（、，也就是今日的[弗瓦迪斯瓦沃沃](../Page/弗瓦迪斯瓦沃沃.md "wikilink")）及黎巴（、，今名[韋巴](../Page/韋巴.md "wikilink")）之間離岸30[公里的海面上發現了威廉](../Page/公里.md "wikilink")·古斯特洛夫號和獅子號的行蹤，在當地時間晚上九時（[UTC](../Page/協調世界時.md "wikilink")+1），希特勒在[電台上發表執政](../Page/電台.md "wikilink")12週年的演講完結不久後，馬連尼斯高下令向他以為是軍艦的威廉·古斯特洛夫號發射四枚魚雷，四枚魚雷分別以[俄文寫上](../Page/俄文.md "wikilink")：

  - 「為了祖國」（）
  - 「為了[史太林](../Page/史太林.md "wikilink")」（）
  - 「為了蘇聯人民」（）
  - 「為了[列寧格勒](../Page/列寧格勒.md "wikilink")」（）

除了寫上「為了[史太林](../Page/史太林.md "wikilink")」的一枚魚雷卡在發射管外，其餘三枚全部命中目標。\[5\]第一枚擊中左舷的船頭，第二枚鑽進位於船身中間的游泳池，第三枚擊中尾部的[發動機房](../Page/發動機.md "wikilink")，爆炸點附近的乘客即時喪命，包括擠在游泳池的女性醫護人員，而船上其他乘客都聽到響亮的爆炸聲。隨之而來的，是船上乘客的驚惶失措。有些難民紛紛湧向[救生艇及爭奪](../Page/救生艇.md "wikilink")[救生衣](../Page/救生衣.md "wikilink")，在混亂中好些救生設備掉下海去。有些人被湧入的海水溺死，有些是因為發生恐慌時在樓梯裡或[甲板上遭踩死或壓死](../Page/甲板.md "wikilink")，也有些乘客跳進冰冷的海水裡凍斃。一月份的波羅的海水溫平均是[攝氏](../Page/攝氏.md "wikilink")4度，但當天是特別嚴寒的一天，氣溫只有攝氏-10至-18度，海面上有浮冰。有報告指出一些兒童及嬰孩在混亂中與家人失散，也有一些兒童因穿上不合身的成人救生衣而導致在水中頭下腳上，最後溺斃。

被魚雷擊中50分鐘後，威廉·古斯特洛夫號終於沉入45米深的海底。之後，德軍方面派出艦艇進行搜救，以下是獲艦艇救起的生還者數字：

|                   |          |
| ----------------- | -------- |
| **搜救艦艇**          | **獲救人數** |
| 魚雷艇 T-36          | 564      |
| 魚雷艇獅子號（Löwe）      | 472      |
| 掃雷艦 M387          | 98       |
| 掃雷艦 M375          | 43       |
| 掃雷艦 M341          | 37       |
| 汽船哥丁根號（Gottingen） | 28       |
| 魚雷回收艇 TF19        | 7        |
| 貨輪哥特蘭號（Gotland）   | 2        |
| 巡邏艇 1703          | 1        |
| **總數**            | **1252** |

以上是海恩茨·熊恩研究得出的數字，從而推算出是次海難死亡的人數大約9,330人，成為世界航海史上最嚴重的海難。\[6\]

艾雲·卡比斯（Irwin Kappes）在《The Greatest Marine Disaster in History... and why
you probably never heard of
it》一書中提到威廉·古斯特洛夫號當時載著超過6,000名乘客。事發15分鐘後，魚雷艇獅子號趕到並盡量把生還者救起。本身載著1,500名[難民的郵輪希柏上將號](../Page/難民.md "wikilink")（Admiral
Hipper），船長漢尼希斯特（Henigst）接獲瞭望台報告受[魚雷襲擊後](../Page/魚雷.md "wikilink")，仍然選擇繼續冒險救起海面上的生還者。卡比斯並確實地指出海難中的死亡人數應為5,348，但他並沒有在書中提出有關資料來源。\[7\]

申恩的研究中對死難者數目作出不同方法的估算。[探索頻道](../Page/探索頻道.md "wikilink")（Discovery
Channel）節目《未解的歷史》（Unsolved History）中採用了電腦軟件maritime
EXODUS根據目擊者有關船上載重密度的報告、模擬逃生路線以及沉船後隨時間過去的生還率作出分析，估算出死難人數達9,400人（以原本10,600位乘客計算）。\[8\]\[9\]\[10\]

### 爭議

在[二戰中](../Page/二戰.md "wikilink")，[同盟國和](../Page/同盟國.md "wikilink")[軸心國都有不少搭載平民的船隻被擊沉](../Page/軸心國.md "wikilink")\[11\]，當中以威廉·古斯特洛夫號最為慘烈，成為歷史上死難者最多的海難。1999年[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")[君特·格拉斯](../Page/君特·格拉斯.md "wikilink")（Günter
Grass）於2003年4月8日[紐約時報的訪問中提到](../Page/紐約時報.md "wikilink")，其中一個令他寫作《[蟹行](../Page/蟹行.md "wikilink")》（"Krebsgang"）的原因是他不滿一些德國人的言論，說威廉·古斯特洛夫號事件是一樁戰爭罪行。而他覺得並不如此，他認為這樁事件只是戰爭引致的悲慘結果。\[12\]

## 殘骸

威廉·古斯特洛夫號的沉沒位置為[北緯](../Page/北緯.md "wikilink")55.07度，[東經](../Page/東經.md "wikilink")17.41度的海底，於波蘭弗瓦迪斯瓦沃沃（）以西，韋巴（）以東，離岸30公里。沉船附近之海域被劃為戰爭紀念遺址，於波蘭航海圖上以「第73號障礙物」標示。\[13\]

2006年，船上的一個[鐘被](../Page/鐘.md "wikilink")[打撈起來](../Page/打撈.md "wikilink")，用作波蘭一家海鮮餐廳的裝飾，之後被借到[柏林的](../Page/柏林.md "wikilink")「Forced
Path」展覽中展出。\[14\]

## 小說

《[蟹行](../Page/蟹行.md "wikilink")》（，2002年），作者是1999年諾貝爾文學獎得主[君特·格拉斯](../Page/君特·格拉斯.md "wikilink")，這本書就是根據威廉·古斯特洛夫號事件寫作。

## 參見

  - [海難列表](../Page/海難列表.md "wikilink")
  - [蘇德戰爭](../Page/蘇德戰爭.md "wikilink")
  - [漢尼拔行動](../Page/漢尼拔行動.md "wikilink")

## 參考資料

  - 艾雲·卡比斯（Kappes, Irwin J.）; [The Greatest Marine Disaster in
    History... and why you probably never heard of
    it.（史上最大海難...為何你沒聽聞過）](http://www.militaryhistoryonline.com/wwii/articles/wilhelmgustloff.aspx)
    2003. — 網站
    [militaryhistoryonline.com](http://www.militaryhistoryonline.com)
    被收錄於
    [中央密芝根大學圖書館中的](../Page/中央密芝根大學.md "wikilink")[軍事史網上資料](http://www.lib.cmich.edu/subjectguides/socialsciences/military.htm)
    內
  - 麥可·雷雅（Leja, Michael）; *[Die letzte Fahrt der "Wilhelm
    Gustloff（威廉·古斯特洛夫號的最後一程）"](https://web.archive.org/web/20071206014155/http://www.zdf.de/ZDFde/inhalt/22/0,1872,2116150,00.html)*;（德文）
    這是一間德國電視台網站的一篇文章，2005年8月1日的報告指出估計遇難人數在找到更多資料後由較早時的6,000向上修訂至9,300。
  - 傑森·派普斯（Pipes, Jason）;
    *[紀念威廉·古斯特洛夫號](http://www.feldgrau.com/wilhelmgustloff.html)*
    — 網站 [www.feldgrau.com](http://www.feldgrau.com)
    被收錄為[威爾斯大學](../Page/威爾斯大學.md "wikilink")
    [德文系](http://www.swan.ac.uk/german)
  - 海恩茨·熊恩（Schön, Heinz）; *Die Gustloff Katastrophe（古斯特洛夫浩劫）*（Motorbuch
    Verlag, 斯圖加特, 2002）

[Category:1945年航海事故](../Category/1945年航海事故.md "wikilink")
[Category:德国邮轮](../Category/德国邮轮.md "wikilink")
[Category:蘇聯軍事](../Category/蘇聯軍事.md "wikilink")
[Category:第二次世界大戰](../Category/第二次世界大戰.md "wikilink")
[Category:汉堡建造的船舶](../Category/汉堡建造的船舶.md "wikilink")
[Category:灾难之最](../Category/灾难之最.md "wikilink")

1.  艾雲·卡比斯（Irwin J. Kappes） [參考資料](../Page/#參考資料.md "wikilink")
    提出遇難人數為5,348。他並沒有列出資料來源，但他推介下列兩本書: A. V. Sellwood,
    ''The Damned Don't Drown: The Sinking of the Wilhelm Gustloff ''; 及
    Alfred-Maurice de Zayas, *A Terrible Revenge: The Ethnic Cleansing
    of the East European Germans 1944-1950*.

2.  傑森·派普斯（Jason Pipes）, [參考資料](../Page/#參考資料.md "wikilink")
    引述海恩茨·熊恩[參考資料](../Page/#參考資料.md "wikilink")（無頁數）
    聲稱他估算的遇難人數為9,343

3.
4.  [Löwe Torpedoboot 1940 - 1959 Sleipner
    Class](http://www.german-navy.de/kriegsmarine/captured/torpedoboats/lowe/index.html)

5.
6.
7.
8.  "[採索頻道《未解的歷史 –
    威廉·古斯特洛夫號》 2003](http://shopping.discovery.com/product-36453.html)
    "

9.  [maritime EXODUS](http://fseg.gre.ac.uk/exodus/air.html)

10. 麥可·雷雅（Michael Leja）, [參考資料](../Page/#參考資料.md "wikilink")（德文）

11. George Martin
    [二戰海難](http://members.iinet.net.au/~gduncan/maritime-1.html)


12. [《[蟹行](../Page/蟹行.md "wikilink")》---[君特·格拉斯](../Page/君特·格拉斯.md "wikilink")](http://www.reddotbooks.co.uk/crabwalk-gunter-grass-p-390.html)


13. Irwin J. Kappes [參考資料](../Page/#參考資料.md "wikilink")

14. Mark Landler
    [被柏林展覽激怒的波蘭人](http://www.iht.com/articles/2006/08/30/news/poland.php)
    2006年8月30日刊於[紐約時報](../Page/紐約時報.md "wikilink"),後再刊於[國際先驅論壇報](../Page/國際先驅論壇報.md "wikilink")