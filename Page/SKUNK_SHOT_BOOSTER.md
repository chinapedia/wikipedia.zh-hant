[SSB_in_CLUB_CITTA.JPG](https://zh.wikipedia.org/wiki/File:SSB_in_CLUB_CITTA.JPG "fig:SSB_in_CLUB_CITTA.JPG")CLUB
CITTA'的演唱會\]\] **SKUNK SHOT
BOOSTER**（簡稱**SSB**）是於[日本](../Page/日本.md "wikilink")[佐賀大學民謠研究會](../Page/佐賀大學.md "wikilink")2004年1月組成的[搖滾音樂](../Page/搖滾音樂.md "wikilink")[樂團](../Page/樂團.md "wikilink")。組成後積極展開演唱會和自主[企劃](../Page/企劃.md "wikilink")。2005年於[CHARCOAL
FILTER佐賀公園舉行公開演唱會](../Page/CHARCOAL_FILTER.md "wikilink")，並於佐賀GEILS的「TRIANGLE」與[Maximum
the Hormone](../Page/Maximum_the_Hormone.md "wikilink")、[G-FREAK
FACTORY](../Page/G-FREAK_FACTORY.md "wikilink")、[STOMPIN'
BIRD等共同演出](../Page/STOMPIN'_BIRD.md "wikilink")。同年獲得「」大獎。2007年3月以[Aer-born的樂團身分正式出道](../Page/Aer-born.md "wikilink")。

## 成員

  - ****（本名：德丸英器，1982年8月14日－）主唱。[鹿兒島縣出身](../Page/鹿兒島縣.md "wikilink")。
  - ****（本名：沖野達也，1981年3月5日－）結他。[廣島縣出身](../Page/廣島縣.md "wikilink")。
  - ****（本名：渡瀨浩則，1982年10月20日－）低音結他。[佐賀縣出身](../Page/佐賀縣.md "wikilink")。
  - ****（本名：上野真，1982年5月15日－）鼓。[愛媛縣出身](../Page/愛媛縣.md "wikilink")。

## 作品

### 單曲

1.  **COCOON**（2005年11月24日）
    1.  SSB ALL RIGHT\!\!

    2.
    3.  103
2.  ****（2007年3月7日）
    1.
    2.  103

    3.  Easy GO\!
3.  ****（2007年6月6日）
    1.
    2.  Over The Rainbow

    3.
4.  **BREAK UP/RESTART**（2007年9月21日）
    1.  BREAK UP

    2.  RESTART

    3.
### 專輯

1.  **NOTICE\!\!**
    1.  SSB ALLRIGHT\!\!

    2.  先

    3.
    4.  空

    5.
### 参加作品

1.  ****
      -
        參加歌曲為「SSB ALLRIGHT\!\!」

## 廣播

  - （[FM福岡](../Page/FM福岡.md "wikilink")）

  - （[FM佐賀](../Page/FM佐賀.md "wikilink")）

## 參見

  - [Monthly A Music](../Page/Monthly_A_Music.md "wikilink")

## 外部連結

  - [官方網站](http://ip.tosp.co.jp/i.asp?i=SKUNKSHOT)（自行營運）

  - [Aer-born內官方網站](http://skunkshotbooster.com/index.html)

[Skunk Shot Booster](../Category/日本樂團.md "wikilink")