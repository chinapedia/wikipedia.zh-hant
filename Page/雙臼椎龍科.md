**雙臼椎龍科**（屬名：Polycotylidae）是[蛇頸龍目的其中一個科](../Page/蛇頸龍目.md "wikilink")，是群大型蛇頸龍類，生存於[白堊紀](../Page/白堊紀.md "wikilink")，並且是[薄板龍科的姐妹分類](../Page/薄板龍科.md "wikilink")。

雙臼椎龍科包含相當多的種類。雙臼椎龍科擁有較短的頸部與大且瘦長的頭部，乍看之下牠們類似[上龍類](../Page/上龍類.md "wikilink")；不過一些[系統發生學研究顯示牠們與](../Page/系統發生學.md "wikilink")[蛇頸龍科](../Page/蛇頸龍科.md "wikilink")、[薄板龍科擁有許多共同特徵](../Page/薄板龍科.md "wikilink")。牠們的化石分布在世界各地，包括[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[日本](../Page/日本.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[南美洲](../Page/南美洲.md "wikilink")、以及[前蘇聯等地區](../Page/前蘇聯.md "wikilink")\[1\]。

## 種系發生學

以下[演化樹來自於](../Page/演化樹.md "wikilink")2007年的蛇頸龍類研究\[2\]：

以下演化樹來自於2010年的蛇頸龍類種系發生學研究\[3\]：

以下演化樹來自於2011年的[歐洲蛇頸龍類研究](../Page/歐洲.md "wikilink")\[4\]：

## 參考資料

<div class="references-small">

<references>

</references>

  - [Palæos: The
    Vertebrates](https://web.archive.org/web/20080414142638/http://www.palaeos.com/Vertebrates/Units/220Lepidosauromorpha/220.820.html#Polycotylidae),
    Lepidosauromorpha: Cryptocleidoidea: Tricleidia

</div>

[\*](../Category/蛇頸龍亞目.md "wikilink")

1.
2.  Albright III, L. B., Gillette, D. D., and Titus, A. L., 2007b.
    [Plesiosaurs from the Upper Cretaceous (Cenomanian-Turonian) Tropic
    Shale of southern Utah, part 2:
    polycotylidae](http://www.plesiosaur.com/database/pdf/albrightetal_2007_part2.pdf)
    . Journal of Vertebrate Paleontology, v. 27, n. 1, p. 41-58.
3.
4.