**組織蛋白去乙醯酶抑制劑**(，簡寫**HDI**或**HDAC抑制劑**)是一種透過抑制身體內[組織蛋白去乙醯酶功能的藥物類別](../Page/組織蛋白去乙醯酶.md "wikilink")，亦存在於[胎兒及](../Page/胎兒.md "wikilink")[花椒內](../Page/花椒.md "wikilink")。現時有健康食品生產商建議透過進食[花椒油而達至](../Page/花椒油.md "wikilink")[防癌的功效](../Page/防癌.md "wikilink")。

現時醫學界有研究透過「組織蛋白去乙醯酶抑制劑」來治療[癌症](../Page/癌症.md "wikilink")\[1\]及[神经退行性疾病](../Page/神经退行性疾病.md "wikilink")(neurodegenerative
diseases)。這些抑制劑的具體機制，例如有關的化合物如何達到有關的功效，到現在還未清楚。不過，有關文獻亦有提出可能的[表觀遺傳學途徑](../Page/表觀遺傳學.md "wikilink")\[2\]
Richon et al.\[3\]發現HDAC抑制劑可以透過引導[p21](../Page/p21.md "wikilink")
(WAF1)來調節[p53的](../Page/p53.md "wikilink")[腫瘤抑制功能](../Page/腫瘤壓抑基因.md "wikilink")\[4\]。對於
HDAC 抑制劑如何調控基因表達 ，目前已有比較大的進展。持續的研究 HDAC 抑制劑的調節基因機制，將快速促進新藥設計及生物標記的鑑定
\[5\]。

## 參考

[组蛋白脱乙酰酶抑制剂](../Category/组蛋白脱乙酰酶抑制剂.md "wikilink")
[Category:抗肿瘤药](../Category/抗肿瘤药.md "wikilink")
[Category:情绪稳定剂](../Category/情绪稳定剂.md "wikilink")

1.
2.
3.  Richon VM, Sandhoff TW, Rifkind RA, Marks PA. AUG 29 2000. "Histone
    deacetylase inhibitor selectively induces p21(WAF1) expression and
    gene-associated histone acetylation." *[Proceedings of the National
    Academy of Sciences of the United States of
    America](../Page/Proceedings_of_the_National_Academy_of_Sciences_of_the_United_States_of_America.md "wikilink")*
    97(18):10014-10019.
4.  el-Deiry WS, Tokino T, Velculescu VE, Levy DB, Parsons R, Trent JM,
    Lin D, Mercer WE, Kinzler KW, Vogelstein B. NOV 19 1993. "WAF1, a
    potential mediator of p53 tumor suppression."
    *[Cell](../Page/Cell_\(journal\).md "wikilink")* 75(4):817-25.
5.