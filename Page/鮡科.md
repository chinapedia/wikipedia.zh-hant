**鮡科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯰形目的其中一科](../Page/鯰形目.md "wikilink")。

## 分類

**鮡科**下分17個屬，如下：

## 真鮡亞科(Sisorinae)

### 魾屬(*Bagarius*)

  - [魾](../Page/魾.md "wikilink")(*Bagarius bagarius*)
  - [老挝魾](../Page/老挝魾.md "wikilink")(*Bagarius rutilus*)
  - [暹羅魾](../Page/暹羅魾.md "wikilink")(*Bagarius suchus*)
  - [巨魾](../Page/巨魾.md "wikilink")(*Bagarius yarrelli*)

### 黑鮡屬(*Gagata*)

  - [尾斑黑鮡](../Page/尾斑黑鮡.md "wikilink")(*Gagata cenia*)
  - [長絲黑鮡](../Page/長絲黑鮡.md "wikilink")(*Gagata dolichonema*)
  - [短鬚黑鮡](../Page/短鬚黑鮡.md "wikilink")(*Gagata gagata*)
  - [伊錫基黑鮡](../Page/伊錫基黑鮡.md "wikilink")(*Gagata itchkeea*)
  - [烏鰭黑鮡](../Page/烏鰭黑鮡.md "wikilink")(*Gagata melanopterus*)
  - [巴基斯坦黑鮡](../Page/巴基斯坦黑鮡.md "wikilink")(*Gagata pakistanica*)
  - [絲鰭黑鮡](../Page/絲鰭黑鮡.md "wikilink")(*Gagata sexualis*)
  - [孟加拉黑鮡](../Page/孟加拉黑鮡.md "wikilink")(*Gagata youssoufi*)

### 戈岡鮡屬(*Gogangra*)

  - [光滑戈岡鮡](../Page/光滑戈岡鮡.md "wikilink")(*Gogangra laevis*)
  - [綠戈岡鮡](../Page/綠戈岡鮡.md "wikilink")(*Gogangra viridescens*)

### 南鮡屬(*Nangra*)

  - [阿薩姆南鮡](../Page/阿薩姆南鮡.md "wikilink")(*Nangra assamensis*)
  - [豐頰南鮡](../Page/豐頰南鮡.md "wikilink")(*Nangra bucculenta*)
  - [南鮡](../Page/南鮡.md "wikilink")(*Nangra nangra*)
  - [飾妝南鮡](../Page/飾妝南鮡.md "wikilink")(*Nangra ornata*)
  - [壯體南鮡](../Page/壯體南鮡.md "wikilink")(*Nangra robusta*)

### 真鮡屬(*Sisor*)

  - [巴拉克真鮡](../Page/巴拉克真鮡.md "wikilink")(*Sisor barakensis*)
  - [寬頭真鮡](../Page/寬頭真鮡.md "wikilink")(*Sisor chennuah*)
  - [厚唇真鮡](../Page/厚唇真鮡.md "wikilink")(*Sisor rabdophorus*)
  - [巴基斯坦真鮡](../Page/巴基斯坦真鮡.md "wikilink")(*Sisor rabdophorus*)
  - [河棲真鮡](../Page/河棲真鮡.md "wikilink")(*Sisor rheophilus*)
  - [壯體真鮡](../Page/壯體真鮡.md "wikilink")(*Sisor torosus*)

## 紋胸鮡亞科(Glyptosterninae)

### 離鮡屬(*Creteuchiloglanis*)

  - [短鰭異鮡](../Page/短鰭異鮡.md "wikilink")(*Creteuchiloglanis brachypterus*)
  - [貢山離鮡](../Page/貢山離鮡.md "wikilink")(*Creteuchiloglanis
    gongshanensis*)
  - [扁頭離鮡](../Page/扁頭離鮡.md "wikilink")(*Creteuchiloglanis kamengensis*)
  - [長胸異鮡](../Page/長胸異鮡.md "wikilink")(*Creteuchiloglanis
    longipectoralis*)
  - [大鰭離鮡](../Page/大鰭離鮡.md "wikilink")(*Creteuchiloglanis macropterus*)

### 石爬鮡屬(*Euchiloglanis*)

  - [平背石爬鮡](../Page/平背石爬鮡.md "wikilink")(*Euchiloglanis dorsoarcus*)
  - [黃石爬鮡](../Page/黃石爬鮡.md "wikilink")(*Euchiloglanis kishinouyei*)
  - [長鬚石爬鮡](../Page/長鬚石爬鮡.md "wikilink")(*Euchiloglanis longibarbatus*)
  - [長石爬鮡](../Page/長石爬鮡.md "wikilink")(*Euchiloglanis longus*)
  - [越南石爬鮡](../Page/越南石爬鮡.md "wikilink")(*Euchiloglanis phongthoensis*)

### 鰋屬(*Exostoma*)

  - [巴拉克鰋](../Page/巴拉克鰋.md "wikilink")(*Exostoma barakensis*)
  - [小孔鰋](../Page/小孔鰋.md "wikilink")(*Exostoma berdmorei*)
  - (*Exostoma effrenum*)
  - [藏鰋](../Page/藏鰋.md "wikilink")(*Exostoma labiatum*)
  - [朝聖鰋](../Page/朝聖鰋.md "wikilink")(*Exostoma peregrinator*)
  - [斯氏鰋](../Page/斯氏鰋.md "wikilink")(*Exostoma stuarti*)
  - [長鬚鰋](../Page/長鬚鰋.md "wikilink")(*Exostoma vinciguerrae*)

### 鑿齒鮡屬(*Glaridoglanis*)

  - [安氏鑿齒鮡](../Page/安氏鑿齒鮡.md "wikilink")(*Glaridoglanis andersonii*)

### 原鮡屬(*Glyptosternon*)

  - [阿富汗原鮡](../Page/阿富汗原鮡.md "wikilink")(*Glyptosternon akhtari*)
  - [黑斑原鮡](../Page/黑斑原鮡.md "wikilink")(*Glyptosternon maculatum*)
  - [馬氏原鮡](../Page/馬氏原鮡.md "wikilink")(*Glyptosternon malaisei*)
  - [網紋原鮡](../Page/網紋原鮡.md "wikilink")(*Glyptosternon reticulatum*)

### 紋胸鮡屬(*Glyptothorax*)

  - [阿氏紋胸鮡](../Page/阿氏紋胸鮡.md "wikilink")(*Glyptothorax alaknandi*)
  - [阿那馬拉河紋胸鮡](../Page/阿那馬拉河紋胸鮡.md "wikilink")(*Glyptothorax
    anamalaiensis*)
  - [墨脫紋胸鮡](../Page/墨脫紋胸鮡.md "wikilink")(*Glyptothorax annandalei*)
  - [亞美尼亞紋胸鮡](../Page/亞美尼亞紋胸鮡.md "wikilink")(*Glyptothorax armeniacus*)
  - [褶皺紋胸鮡](../Page/褶皺紋胸鮡.md "wikilink")(*Glyptothorax ater*)
  - [博特斯紋胸鮡](../Page/博特斯紋胸鮡.md "wikilink")(*Glyptothorax botius*)
  - [短鰭紋胸鮡](../Page/短鰭紋胸鮡.md "wikilink")(*Glyptothorax brevipinnis*)
  - [布坎南氏紋胸鮡](../Page/布坎南氏紋胸鮡.md "wikilink")(*Glyptothorax buchanani*)
  - [緬甸紋胸鮡](../Page/緬甸紋胸鮡.md "wikilink")(*Glyptothorax burmanicus*)
  - [麗鰭紋胸鮡](../Page/麗鰭紋胸鮡.md "wikilink")(*Glyptothorax callopterus*)
  - [尾斑紋胸鮡](../Page/尾斑紋胸鮡.md "wikilink")(*Glyptothorax caudimaculatus*)
  - [穴形紋胸鮡](../Page/穴形紋胸鮡.md "wikilink")(*Glyptothorax cavia*)
  - [光刺紋胸鮡](../Page/光刺紋胸鮡.md "wikilink")(*Glyptothorax chimtuipuiensis*)
  - [欽溫紋胸鮡](../Page/欽溫紋胸鮡.md "wikilink")(*Glyptothorax chindwinica*)
  - [錐吻紋胸鮡](../Page/錐吻紋胸鮡.md "wikilink")(*Glyptothorax conirostre*)
  - [大眼紋胸鮡](../Page/大眼紋胸鮡.md "wikilink")(*Glyptothorax coracinus*)
  - [底格里斯河紋胸鮡](../Page/底格里斯河紋胸鮡.md "wikilink")(*Glyptothorax cous*)
  - [戴氏紋胸鮡](../Page/戴氏紋胸鮡.md "wikilink")(*Glyptothorax davissinghi*)
  - [德欽紋胸鮡](../Page/德欽紋胸鮡.md "wikilink")(*Glyptothorax deqinensis*)
  - [迪克朗河紋胸鮡](../Page/迪克朗河紋胸鮡.md "wikilink")(*Glyptothorax
    dikrongensis*)
  - [亮背紋胸鮡](../Page/亮背紋胸鮡.md "wikilink")(*Glyptothorax dorsalis*)
  - [外齒紋胸鮡](../Page/外齒紋胸鮡.md "wikilink")(*Glyptothorax exodon*)
  - [柄帶紋胸鮡](../Page/柄帶紋胸鮡.md "wikilink")(*Glyptothorax filicatus*)
  - [福建紋胸鮡](../Page/福建紋胸鮡.md "wikilink")(*Glyptothorax fokiensis*)
  - [異色紋胸鮡](../Page/異色紋胸鮡.md "wikilink")(*Glyptothorax fucatus*)
  - [棕色紋胸鮡](../Page/棕色紋胸鮡.md "wikilink")(*Glyptothorax fuscus*)
  - [加氏紋胸鮡](../Page/加氏紋胸鮡.md "wikilink")(*Glyptothorax garhwali*)
  - [細體紋胸鮡](../Page/細體紋胸鮡.md "wikilink")(*Glyptothorax gracilis*)
  - [粒線紋胸鮡](../Page/粒線紋胸鮡.md "wikilink")(*Glyptothorax granosus*)
  - [顆粒紋胸鮡](../Page/顆粒紋胸鮡.md "wikilink")(*Glyptothorax granulus*)
  - [海南紋胸鮡](../Page/海南紋胸鮡.md "wikilink")(*Glyptothorax hainanensis*)
  - [蓬萊紋胸鮡](../Page/蓬萊紋胸鮡.md "wikilink")(*Glyptothorax horai*)
  - [豪斯氏紋胸鮡](../Page/豪斯氏紋胸鮡.md "wikilink")(*Glyptothorax housei*)
  - [紅河紋胸鮡](../Page/紅河紋胸鮡.md "wikilink")(*Glyptothorax honghensis*)
  - [火焰紋胸鮡](../Page/火焰紋胸鮡.md "wikilink")(*Glyptothorax igniculus*)
  - [印度紋胸鮡](../Page/印度紋胸鮡.md "wikilink")(*Glyptothorax indicus*)
  - [間棘紋胸鮡](../Page/間棘紋胸鮡.md "wikilink")(*Glyptothorax interspinalus*)
  - [阿富汗紋胸鮡](../Page/阿富汗紋胸鮡.md "wikilink")(*Glyptothorax jalalensis*)
  - [傑亞拉氏紋胸鮡](../Page/傑亞拉氏紋胸鮡.md "wikilink")(''Glyptothorax jayarami '')
  - [喀什米爾紋胸鮡](../Page/喀什米爾紋胸鮡.md "wikilink")(*Glyptothorax
    kashmirensis*)
  - [克坦紋胸鮡](../Page/克坦紋胸鮡.md "wikilink")(*Glyptothorax ketambe*)
  - [庫德雷穆紋胸鮡](../Page/庫德雷穆紋胸鮡.md "wikilink")(*Glyptothorax
    kudremukhensis*)
  - [庫爾德紋胸鮡](../Page/庫爾德紋胸鮡.md "wikilink")(*Glyptothorax kurdistanicus*)
  - [北加里曼丹紋胸鮡](../Page/北加里曼丹紋胸鮡.md "wikilink")(*Glyptothorax laak*)
  - [麗紋胸鮡](../Page/麗紋胸鮡.md "wikilink")(*Glyptothorax lampris*)
  - [矛形紋胸鮡](../Page/矛形紋胸鮡.md "wikilink")(*Glyptothorax lanceatus*)
  - [老撾紋胸鮡](../Page/老撾紋胸鮡.md "wikilink")(*Glyptothorax laosensis*)
  - [路那紋胸鮡](../Page/路那紋胸鮡.md "wikilink")(*Glyptothorax lonah*)
  - [長尾紋胸鮡](../Page/長尾紋胸鮡.md "wikilink")(*Glyptothorax longicauda*)
  - [龍江紋胸鮡](../Page/龍江紋胸鮡.md "wikilink")(*Glyptothorax longjiangensis*)
  - [皺胸紋胸鮡](../Page/皺胸紋胸鮡.md "wikilink")(*Glyptothorax maceriatus*)
  - [大斑紋胸鮡](../Page/大斑紋胸鮡.md "wikilink")(*Glyptothorax macromaculatus*)
  - [馬德拉邦紋胸鮡](../Page/馬德拉邦紋胸鮡.md "wikilink")(*Glyptothorax
    madrspatanum*)
  - [大紋胸鮡](../Page/大紋胸鮡.md "wikilink")(*Glyptothorax major*)
  - [馬拉巴紋胸鮡](../Page/馬拉巴紋胸鮡.md "wikilink")(*Glyptothorax malabarensis*)
  - [曼尼普爾紋胸鮡](../Page/曼尼普爾紋胸鮡.md "wikilink")(*Glyptothorax
    manipurensis*)
  - [細斑紋胸鮡](../Page/細斑紋胸鮡.md "wikilink")(*Glyptothorax minimaculatus*)
  - [內氏紋胸鮡](../Page/內氏紋胸鮡.md "wikilink")(*Glyptothorax naziri*)
  - [納爾遜紋胸鮡](../Page/納爾遜紋胸鮡.md "wikilink")(*Glyptothorax nelsoni*)
  - [雅巴紋胸鮡](../Page/雅巴紋胸鮡.md "wikilink")(*Glyptothorax ngapang*)
  - [尼氏紋胸鮡](../Page/尼氏紋胸鮡.md "wikilink")(*Glyptothorax nieuwenhuisi*)
  - [斜斑紋胸鮡](../Page/斜斑紋胸鮡.md "wikilink")(*Glyptothorax
    obliquimaculatus*)
  - [暗色紋胸鮡](../Page/暗色紋胸鮡.md "wikilink")(*Glyptothorax obscurus*)
  - [白線紋胸鮡](../Page/白線紋胸鮡.md "wikilink")(*Glyptothorax pallozonus*)
  - [波紋紋胸鮡](../Page/波紋紋胸鮡.md "wikilink")(*Glyptothorax panda*)
  - [扇鰭紋胸鮡](../Page/扇鰭紋胸鮡.md "wikilink")(*Glyptothorax pectinopterus*)
  - [扁鬚紋胸鮡](../Page/扁鬚紋胸鮡.md "wikilink")(*Glyptothorax platypogon*)
  - [似扁鬚紋胸鮡](../Page/似扁鬚紋胸鮡.md "wikilink")(*Glyptothorax
    platypogonides*)
  - [絞紋紋胸鮡](../Page/絞紋紋胸鮡.md "wikilink")(*Glyptothorax plectilis*)
  - [浦那紋胸鮡](../Page/浦那紋胸鮡.md "wikilink")(*Glyptothorax poonaensis*)
  - [普氏紋胸鮡](../Page/普氏紋胸鮡.md "wikilink")(*Glyptothorax prashadi*)
  - [旁遮普紋胸鮡](../Page/旁遮普紋胸鮡.md "wikilink")(*Glyptothorax punjabensis*)
  - [四斑紋胸鮡](../Page/四斑紋胸鮡.md "wikilink")(*Glyptothorax quadriocellatus*)
  - [線紋紋胸鮡](../Page/線紋紋胸鮡.md "wikilink")(*Glyptothorax radiolus*)
  - [薩爾溫江紋胸鮡](../Page/薩爾溫江紋胸鮡.md "wikilink")(*Glyptothorax rugimentum*)
  - [塞氏紋胸鮡](../Page/塞氏紋胸鮡.md "wikilink")(*Glyptothorax saisii*)
  - [施密特紋胸鮡](../Page/施密特紋胸鮡.md "wikilink")(*Glyptothorax schmidti*)
  - [暹羅紋胸鮡](../Page/暹羅紋胸鮡.md "wikilink")(*Glyptothorax siamensis*)
  - [西氏紋胸鮡](../Page/西氏紋胸鮡.md "wikilink")(*Glyptothorax silviae*)
  - [中華紋胸鮡](../Page/中華紋胸鮡.md "wikilink")(*Glyptothorax sinense*)
  - [斯氏紋胸鮡](../Page/斯氏紋胸鮡.md "wikilink")(*Glyptothorax steindachneri*)
  - [史氏紋胸鮡](../Page/史氏紋胸鮡.md "wikilink")(*Glyptothorax stocki*)
  - [施氏紋胸鮡](../Page/施氏紋胸鮡.md "wikilink")(*Glyptothorax stolickae*)
  - [史特拉波紋胸鮡](../Page/史特拉波紋胸鮡.md "wikilink")(*Glyptothorax strabonis*)
  - [條紋胸鮡](../Page/條紋胸鮡.md "wikilink")(*Glyptothorax striatus*)
  - [薩氏紋胸鮡](../Page/薩氏紋胸鮡.md "wikilink")(*Glyptothorax sufii*)
  - [辛氏紋胸鮡](../Page/辛氏紋胸鮡.md "wikilink")(*Glyptothorax sykesi*)
  - [特爾紋胸鮡](../Page/特爾紋胸鮡.md "wikilink")(*Glyptothorax telchitta*)
  - [屈氏紋胸鮡](../Page/屈氏紋胸鮡.md "wikilink")(*Glyptothorax trewavasae*)
  - [三線紋胸鮡](../Page/三線紋胸鮡.md "wikilink")(*Glyptothorax trilineatus*)
  - [腹紋紋胸鮡](../Page/腹紋紋胸鮡.md "wikilink")(*Glyptothorax ventrolineatus*)
  - [疣紋胸鮡](../Page/疣紋胸鮡.md "wikilink")(*Glyptothorax verrucosus*)
  - [扎那紋胸鮡](../Page/扎那紋胸鮡.md "wikilink")(*Glyptothorax zanaensis*)
  - [珠江紋胸鮡](../Page/珠江紋胸鮡.md "wikilink")(*Glyptothorax zhujiangensis*)

### 邁氏鮡屬(*Myersglanis*)

  - [尼泊爾邁氏鮡](../Page/尼泊爾邁氏鮡.md "wikilink")(*Myersglanis blythi*)
  - [傑亞邁氏鮡](../Page/傑亞邁氏鮡.md "wikilink")(*Myersglanis jayarami*)

### 異齒鰋屬(*Oreoglanis*)

  - [月尾異齒鰋](../Page/月尾異齒鰋.md "wikilink")(''Oreoglanis colurus '')
  - [細尾異齒鰋](../Page/細尾異齒鰋.md "wikilink")(*Oreoglanis delacouri*)
  - [韁異齒鰋](../Page/韁異齒鰋.md "wikilink")(*Oreoglanis frenatus*)
  - [尖鬚異齒鰋](../Page/尖鬚異齒鰋.md "wikilink")(*Oreoglanis heteropogon*)
  - [高異齒鰋](../Page/高異齒鰋.md "wikilink")(*Oreoglanis hypsiurus*)
  - [無斑異齒鰋](../Page/無斑異齒鰋.md "wikilink")(*Oreoglanis immaculatus*)
  - [帶異齒鰋](../Page/帶異齒鰋.md "wikilink")(*Oreoglanis infulatus*)
  - [伊洛瓦底江異齒鰋](../Page/伊洛瓦底江異齒鰋.md "wikilink")(*Oreoglanis insignis*)
  - [景東異齒鰋](../Page/景東異齒鰋.md "wikilink")(*Oreoglanis jingdongensis*)
  - [凹尾異齒鰋](../Page/凹尾異齒鰋.md "wikilink")(*Oreoglanis laciniosus*)
  - [短尾異齒鰋](../Page/短尾異齒鰋.md "wikilink")(*Oreoglanis lepturus*)
  - [大絲異齒鰋](../Page/大絲異齒鰋.md "wikilink")(*Oreoglanis macronemus*)
  - [大鰭異齒鰋](../Page/大鰭異齒鰋.md "wikilink")(*Oreoglanis macropterus*)
  - [大翅異齒鰋](../Page/大翅異齒鰋.md "wikilink")(*Oreoglanis majusculus*)
  - [納氏異齒鰋](../Page/納氏異齒鰋.md "wikilink")(*Oreoglanis nakasathiani*)
  - [老撾異齒鰋](../Page/老撾異齒鰋.md "wikilink")(*Oreoglanis setiger*)
  - [暹羅異齒鰋](../Page/暹羅異齒鰋.md "wikilink")(*Oreoglanis siamensis*)
  - [薩氏異齒鰋](../Page/薩氏異齒鰋.md "wikilink")(*Oreoglanis sudarai*)
  - [蘇氏異齒鰋](../Page/蘇氏異齒鰋.md "wikilink")(*Oreoglanis suraswadii*)
  - [窄尾異齒鰋](../Page/窄尾異齒鰋.md "wikilink")(*Oreoglanis tenuicauda*)
  - [圓鬚異齒鰋](../Page/圓鬚異齒鰋.md "wikilink")(*Oreoglanis vicinus*)

### 平唇鮡屬(*Parachiloglanis*)

  - [霍氏平唇鮡](../Page/霍氏平唇鮡.md "wikilink")(*Parachiloglanis hodgarti*)

### 鮡屬(*Pareuchiloglanis*)

  - [短體鮡](../Page/短體鮡.md "wikilink")(*Pareuchiloglanis abbreviatus*)
  - [前臀鮡](../Page/前臀鮡.md "wikilink")(*Pareuchiloglanis anteanalis*)
  - [短尾鮡](../Page/短尾鮡.md "wikilink")(*Pareuchiloglanis brevicaudatus*)
  - [短鰭鮡](../Page/短鰭鮡.md "wikilink")(*Pareuchiloglanis feae*)
  - [細尾鮡](../Page/細尾鮡.md "wikilink")(*Pareuchiloglanis gracilicaudata*)
  - [長尾鮡](../Page/長尾鮡.md "wikilink")(*Pareuchiloglanis longicauda*)
  - [大孔鮡](../Page/大孔鮡.md "wikilink")(*Pareuchiloglanis macrotrema*)
  - [蘭坪鮡](../Page/蘭坪鮡.md "wikilink")(*Pareuchiloglanis myzostoma*)
  - [南迪江鮡](../Page/南迪江鮡.md "wikilink")(*Pareuchiloglanis namdeensis*)
  - [煙色鮡](../Page/煙色鮡.md "wikilink")(*Pareuchiloglanis nebulifer*)
  - [越南鮡](../Page/越南鮡.md "wikilink")(*Pareuchiloglanis poilanei*)
  - [長背鮡](../Page/長背鮡.md "wikilink")(*Pareuchiloglanis prolixdorsalis*)
  - [條尾鮡](../Page/條尾鮡.md "wikilink")(*Pareuchiloglanis rhabdurus*)
  - [壯體鮡](../Page/壯體鮡.md "wikilink")(*Pareuchiloglanis robusta*)
  - [四川鮡](../Page/四川鮡.md "wikilink")(*Pareuchiloglanis sichuanensis*)
  - [中華鮡](../Page/中華鮡.md "wikilink")(*Pareuchiloglanis sinensis*)
  - [宋達鮡](../Page/宋達鮡.md "wikilink")(''Pareuchiloglanis songdaensis '')
  - [宋曼鮡](../Page/宋曼鮡.md "wikilink")(*Pareuchiloglanis songmaensis*)
  - [三陽江鮡](../Page/三陽江鮡.md "wikilink")(*Pareuchiloglanis tamduongensis*)
  - [蒂氏鮡](../Page/蒂氏鮡.md "wikilink")(*Pareuchiloglanis tianquanensis*)

### 褶鮡屬(*Pseudecheneis*)

  - [粗尾褶鮡](../Page/粗尾褶鮡.md "wikilink")(*Pseudecheneis brachyurus*)
  - [糙尾褶鮡](../Page/糙尾褶鮡.md "wikilink")(*Pseudecheneis crassicauda*)
  - [埃氏褶鮡](../Page/埃氏褶鮡.md "wikilink")(*Pseudecheneis eddsi*)
  - [纖體褶鮡](../Page/纖體褶鮡.md "wikilink")(*Pseudecheneis gracilis*)
  - [無斑褶鮡](../Page/無斑褶鮡.md "wikilink")(*Pseudecheneis immaculatus*)
  - [科氏褶鮡](../Page/科氏褶鮡.md "wikilink")(*Pseudecheneis koladynae*)
  - [長鰭褶鮡](../Page/長鰭褶鮡.md "wikilink")(*Pseudecheneis longipectoralis*)
  - [暗褶鮡](../Page/暗褶鮡.md "wikilink")(*Pseudecheneis maurus*)
  - [少斑褶鮡](../Page/少斑褶鮡.md "wikilink")(*Pseudecheneis paucipunctata*)
  - [平吻褶鮡](../Page/平吻褶鮡.md "wikilink")(*Pseudecheneis paviei*)
  - [鋸齒褶鮡](../Page/鋸齒褶鮡.md "wikilink")(*Pseudecheneis serracula*)
  - [女神褶鮡](../Page/女神褶鮡.md "wikilink")(*Pseudecheneis sirenica*)
  - [窄尾褶鮡](../Page/窄尾褶鮡.md "wikilink")(*Pseudecheneis stenura*)
  - [黃斑褶鮡](../Page/黃斑褶鮡.md "wikilink")(*Pseudecheneis sulcata*)
  - [似黃斑褶鮡](../Page/似黃斑褶鮡.md "wikilink")(*Pseudecheneis sulcatoides*)
  - [恆河褶鮡](../Page/恆河褶鮡.md "wikilink")(*Pseudecheneis suppaetula*)
  - [盤褶鮡](../Page/盤褶鮡.md "wikilink")(*Pseudecheneis sympelvica*)
  - [張氏褶鮡](../Page/張氏褶鮡.md "wikilink")(*Pseudecheneis tchangi*)
  - [烏克魯爾褶鮡](../Page/烏克魯爾褶鮡.md "wikilink")(*Pseudecheneis ukhrulensis*)

### 擬鰋屬(*Pseudexostoma*)

  - [短身擬鰋](../Page/短身擬鰋.md "wikilink")(*Pseudexostoma brachysoma*)
  - [長鰭擬鰋](../Page/長鰭擬鰋.md "wikilink")(*Pseudexostoma longipterus*)
  - [雲南擬鰋](../Page/雲南擬鰋.md "wikilink")(*Pseudexostoma yunnanensis*)

## 參考

## 扩展阅读

[\*](../Category/鮡科.md "wikilink")