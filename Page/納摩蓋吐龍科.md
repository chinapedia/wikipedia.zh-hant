**納摩蓋吐龍科**（Nemegtosauridae）是一群[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，可能屬於[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")，起初包含兩個生存於晚[白堊紀](../Page/白堊紀.md "wikilink")[蒙古的屬](../Page/蒙古.md "wikilink")，分別為[納摩蓋吐龍](../Page/納摩蓋吐龍.md "wikilink")、[非凡龍](../Page/非凡龍.md "wikilink")。科學家們爭論者這兩個屬與其他蜥腳類恐龍的關係，以及牠們是否構成一個有效的科。最近有其他屬被分類於這個[演化支](../Page/演化支.md "wikilink")，而[親緣分支分類法有助於了解納摩蓋吐龍科的演化關係](../Page/親緣分支分類法.md "wikilink")。

## 分類歷史

在1990年，McIntosh將納摩蓋吐龍與非凡龍分類於[叉龍科的叉龍亞科](../Page/叉龍科.md "wikilink")，因為牠們的[頭顱骨類似](../Page/頭顱骨.md "wikilink")[叉龍](../Page/叉龍.md "wikilink")，但在某些特徵上有差別。當[納摩蓋吐龍的頭顱骨被發現時](../Page/納摩蓋吐龍.md "wikilink")，被認為與缺乏頭部化石的[後凹尾龍有關聯](../Page/後凹尾龍.md "wikilink")，McIntosh反對這個可能性，因為納摩蓋吐龍類似[梁龍](../Page/梁龍.md "wikilink")，而後凹尾龍類似[圓頂龍](../Page/圓頂龍.md "wikilink")。

[保羅·阿普徹奇在](../Page/保羅·阿普徹奇.md "wikilink")1995年將這兩個屬分類於獨立的納摩蓋吐龍科，但沒有提出[親緣分支分類法定義](../Page/親緣分支分類法.md "wikilink")。然而，在[保羅·巴雷特與阿普徹奇稍早的研究中已提到納摩蓋吐龍科](../Page/保羅·巴雷特.md "wikilink")。

Wilson在2002年利用親緣分支分類法，將納摩蓋吐龍科從[梁龍超科移到](../Page/梁龍超科.md "wikilink")[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")。

在2004年，[塞巴斯蒂安·阿派斯特圭](../Page/塞巴斯蒂安·阿派斯特圭.md "wikilink")（Sebastian
Apesteguia）在敘述發現於[巴塔哥尼亞地區的](../Page/巴塔哥尼亞.md "wikilink")[薩爾加多博妮塔龍時](../Page/博妮塔龍.md "wikilink")，首次將納摩蓋吐龍科定義為一個基群演化支，包含：[泰坦巨龍類中](../Page/泰坦巨龍類.md "wikilink")，親近於納摩蓋吐龍，而離薩爾塔龍較遠的所有物種。他提出納摩蓋吐龍、非凡龍、[掠食龍](../Page/掠食龍.md "wikilink")、以及[博妮塔龍等屬之間有親緣關係](../Page/博妮塔龍.md "wikilink")，並參考Wilson在2002的種系發生學研究。

阿普徹奇等人在2004年，將納摩蓋吐龍科重新分類於梁龍超科，並將納摩蓋吐龍科定義為一個基群演化支，包含：梁龍超科之中，親緣關係接近納摩蓋吐龍，而離梁龍較遠的所有物種。而該科僅包含納摩蓋吐龍、非凡龍。

在2005年，Curry
Rogers發表了一個到目前為止最完整的[泰坦巨龍類親緣分支分類法研究](../Page/泰坦巨龍類.md "wikilink")，將納摩蓋吐龍與非凡龍重新分類於泰坦巨龍類，但他認為納摩蓋吐龍科與該演化支都是無效的。非凡龍被分類於[薩爾塔龍亞科](../Page/薩爾塔龍亞科.md "wikilink")，而納摩蓋吐龍與[掠食龍被分類於一個新的未命名演化支](../Page/掠食龍.md "wikilink")；該演化支如果被命名，可能稱為納摩蓋吐龍亞科或納摩蓋吐龍族。而後凹尾龍屬於[後凹尾龍亞科](../Page/後凹尾龍亞科.md "wikilink")。這些演化支都屬於[薩爾塔龍科](../Page/薩爾塔龍科.md "wikilink")（=[泰坦巨龍科](../Page/泰坦巨龍科.md "wikilink")）。

Wilson並沒有注意到阿派斯特圭的研究，而在2005年將納摩蓋吐龍科定義為一個基群演化支，包含：泰坦巨龍類之中，所有親緣關係接近納摩蓋吐龍，而薩爾塔龍較遠的所有物種。他同時提出[後凹尾龍可能是納摩蓋吐龍的](../Page/後凹尾龍.md "wikilink")[次異名](../Page/次異名.md "wikilink")。

## 系統發生學

以下的演化樹是根據Zaher等人2011年的研究\[1\]：

## 參考資料

  - Sebastian Apesteguia., 2004, "*Bonitasaura salgadoi* Gen. et sp.
    nov.: A beaked sauropod from The Late Cretaceous of Patagonia."
    *Naturwissenschaften*, v. 91, n. 10, p. 493-497.
  - Paul M. Barrett & Paul Upchurch, 1995. "Sauropod feeding mechanisms:
    Their bearing on palaeoecology," in Sun A. & Wang Y., eds., *Sixth
    Symposium on Mesozoic Terrestrial Ecosystems and Biota, Short
    Papers*, China Ocean Press, Beijing: 107-110.
  - Kristina Curry Rogers, 2005, "Titanosauria: A Phylogenetic Overview"
    *in* Curry Rogers and Wilson (eds), *The Sauropods: Evolution and
    Paleobiology* pp.50-103
  - Jack McIntosh, 1990, "Sauropoda" in *The Dinosauria*, Edited by
    David B. Weishampel, Peter Dodson, and Halska Osmolska. University
    of California Press, pp. 345-401.
  - Upchurch, P., 1995. "The evolutionary history of sauropod
    dinosaurs," Philosophical Transactions of the Royal Society of
    London B 349: 365-390.
  - Upchurch, P., Barrett, P.M. and Peter Dodson 2004. Sauropoda. In
    *The Dinosauria*, 2nd edition. David B. Weihampel, P. Dodson, and
    Halszka Osmólska (eds.). University of California Press, Berkeley.
    pp. 259-322.
  - Jeffrey A. Wilson (2005). Redescription of the Mongolian sauropod
    *Nemegtosaurus mongoliensis* Nowinski (Dinosauria: Saurischia) and
    comments on Late Cretaceous sauropod diversity. *Journal of
    Systematic Palaeontology* 3: 283-318. See [New Nemegtosaurus
    paper](http://dml.cmnh.org/2005Aug/msg00422.html) for synopsis.

<references/>

## 外部連結

  - [Taxon
    Search](https://web.archive.org/web/20110717121116/http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=207)
  - [納摩蓋吐龍科](http://dml.cmnh.org/1999Apr/msg00075.html)
  - [TaylorNaish2005-diplodocoid-taxonomy.pdf The phylogenetic taxonomy
    of Diplodocoidea (Dinosauria: Sauropoda)
    PDF](http://www.miketaylor.org.uk/dino/pubs/)

[納摩蓋吐龍科](../Category/納摩蓋吐龍科.md "wikilink")

1.