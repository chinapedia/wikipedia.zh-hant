**《卡拉OK.LIVE！‧台北‧我》**是[张雨生發行的第六張國語專輯](../Page/张雨生.md "wikilink")，專輯中所有歌曲的詞曲皆由張雨生自己創作，並以弦樂四重奏樂團現場方式表現。更締造了台灣流行音樂界秒數最長的唱片。然而，由於當時對[香港](../Page/香港.md "wikilink")「[四大天王](../Page/四大天王_\(歌手\).md "wikilink")」的狂熱風潮，此張專輯賣得只能以「慘澹」來形容。

事實上，張雨生這張專輯內每首歌都是耐聽且有深度的歌，取材探討一些社會問題，包括《動物的悲歌》的流浪動物，此外本首歌也被選入華視週六晚間單元劇[納桑麻谷我的家主題曲](../Page/納桑麻谷我的家.md "wikilink")，《永公街的街長》的身心障礙的人。此外，還有屬於個人關懷社會與寫給特定對象的歌曲。張雨生逝世後，[豐華唱片更以現代錄音技術](../Page/豐華唱片.md "wikilink")，讓原來由張雨生獨唱的《我期待》加上[陶晶瑩的聲音](../Page/陶晶瑩.md "wikilink")，成了兩人對唱。

## 曲目

1.  我是多麼想
2.  動物的悲歌(流浪動物)
3.  靈光
4.  永公街的街長(身心障礙的人)（本歌請知名演員[文英擔任前奏間的台語口白](../Page/文英.md "wikilink")）
5.  子夜抒懷
6.  兄弟呀
7.  這一年這一夜
8.  我的心在發燙(与陶晶瑩對唱)
9.  後知後覺
10. 蝴蝶結(紀念妹妹)
11. 跟得上我吧(寫給歌迷)
12. 我期待(1998年利用現代錄音技術，加上陶晶瑩的聲音，成了兩人對唱，對唱版本收錄於[想念雨生合輯](../Page/想念雨生.md "wikilink"))
13. 再見 蘭花草！

## 音樂錄影帶

| 歌名    | 導演  | 首播日期  | 首播頻道 |
| ----- | --- | ----- | ---- |
| 我期待   | 賴偉康 | 1994年 |      |
| 動物的悲歌 |     | 1994年 |      |
|       |     |       |      |

## 外部連結

  - [我是多麼想](http://www.youtube.com/watch?v=Db9E1-ECY9I)
  - [動物的悲歌 MV](http://www.youtube.com/watch?v=OrryhJx0uHM)
  - [靈光](http://www.youtube.com/watch?v=OYyWKcxRIYg)
  - [永公街的街長](http://www.youtube.com/watch?v=qBFUEr0F0HI)
  - [子夜抒懷](http://www.tudou.com/programs/view/-5xVZjof0VE/)
  - [兄弟呀](http://www.youtube.com/watch?v=sw58vvPfG3Y&feature)
  - [這一年這一夜](http://www.youtube.com/watch?v=tXf1IEami7s)
  - [我的心在發燙](http://www.youtube.com/watch?v=QYdvADjfkro)
  - [後知後覺](http://www.youtube.com/watch?v=Z71AyT-j-Ck&feature)
  - [蝴蝶結](http://www.youtube.com/watch?v=HacRaenDOGc)
  - [跟得上我吧](http://www.youtube.com/watch?v=DEAde4fFnqQ)
  - [我期待 MV](http://www.youtube.com/watch?v=oEj25I81cBQ)
  - [再見 蘭花草！](http://www.youtube.com/watch?v=N1Uqo5tq2Ug)

[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:1994年音樂專輯](../Category/1994年音樂專輯.md "wikilink")
[Category:張雨生音樂專輯](../Category/張雨生音樂專輯.md "wikilink")
[Category:飞碟唱片音乐专辑](../Category/飞碟唱片音乐专辑.md "wikilink")