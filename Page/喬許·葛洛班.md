**喬許·溫斯洛·葛洛班**（[英語](../Page/英語.md "wikilink")：****，）是一個曾被提名[葛萊美獎的](../Page/葛萊美獎.md "wikilink")[美國](../Page/美國.md "wikilink")[創作歌手](../Page/創作歌手.md "wikilink")，憑藉他成熟，感情豐富和[男中音聲線而廣為人知](../Page/男中音.md "wikilink")。雖然他的願望是繼續從事[歌劇](../Page/歌劇.md "wikilink")，但近來仍然專注於唱歌及唱片灌錄。

葛洛班目前居住在[洛杉磯](../Page/洛杉磯.md "wikilink")。

## 早期生活

喬許·葛班洛在美國[洛杉磯出生](../Page/洛杉磯.md "wikilink")。他的父親是[猶太人](../Page/猶太人.md "wikilink")\[1\]\[2\]。他的母親有英格蘭、德國、[挪威和](../Page/挪威.md "wikilink")[猶太血統](../Page/猶太.md "wikilink")\[3\]\[4\]。父親在婚後改變信仰，成為[基督徒](../Page/基督徒.md "wikilink")。葛洛班的弟弟．基斯杜化，與他同一天生日，但則較他年輕四年\[5\]。

葛洛班以歌手身分首次登台，是他七年級的時候，但是隨後擱下了數年。他表示：「我很享受藝人的外觀，可是我的水平正在下降。我並不感覺到我有足夠的靈感，所以我去了布里吉斯學院（Bridges
Academy）以提升我的水平至
A。」當葛洛班在布里吉斯學院的時候，他上普通課堂，由上午9時至下午1時，然後出席有關劇場的課堂。他不但在學校演奏一便士哨子，还会演奏[卡祖笛](../Page/卡祖笛.md "wikilink")。

1997及1998年，葛洛班在[密歇根大學參與了因特洛肯藝術營](../Page/密歇根大學.md "wikilink")（Interlochen
Arts
Camp），當他在學校外開始參加[聲樂課堂](../Page/聲樂.md "wikilink")，同時在因特洛肯藝術營又參與了音樂劇院。葛洛班表示：「我一方面開始上音樂課堂，而另一方面，我又很投入於音樂劇院。我擁有一把不錯的男中音聲線，所以我開始在學校參與演戲及唱歌的製作\[6\]。」

1998年後期，時年17歲的葛洛班被介紹到是他的聲樂教練——[葛萊美獎最佳製作人](../Page/葛萊美獎.md "wikilink")／編曲家的得主[大衛·佛斯特](../Page/大衛·佛斯特.md "wikilink")。他以試演歌手的身份，為佛斯特演出一系列的倍受注目的項目，包括1999年的葛萊美獎，當時為安德魯·伯切里（Andrea
Bocelli）作代替人。葛洛班又與[席琳·狄翁合唱佛斯特的歌曲](../Page/席琳·狄翁.md "wikilink")《禱告》（The
Prayer），以及在1999年1月葛雷·戴維斯（Gray
Davis）當選[加利福尼亞州州長的就職典禮](../Page/加利福尼亞州州長.md "wikilink")。

葛洛班在1999年又進入，為劇院重要人物及畢業生。及後在[賓夕法尼亞州](../Page/賓夕法尼亞州.md "wikilink")[匹茲堡的](../Page/匹茲堡.md "wikilink")[卡內基梅隆大學中修讀戲劇](../Page/卡內基梅隆大學.md "wikilink")。

## 早年事業

葛洛班離開卡內基梅隆大學僅僅一年，他便透過科士打的，與[華納兄弟簽署錄音合約](../Page/華納兄弟.md "wikilink")。他一直以歌手身分被注視著，科士打表示：「我十分愛他本身在[流行音樂及](../Page/流行音樂.md "wikilink")[搖滾樂範疇的能力](../Page/搖滾樂.md "wikilink")，但是我更愛他對西洋[古典音樂的感覺](../Page/古典音樂.md "wikilink")，他亦是一個擁有猜想到的真正音樂力量的人。」
\[7\] 因此，受科士打的影響，葛洛班的首張專輯比較集中在西洋古典音樂的歌曲，如《今夜伴我流浪》（義大利語：Gira Con Me
Questa Notte）和《在陽光下》（義大利語：Alla Luce Del Sole），前者是由科士打及葛洛班決定的。

在被科士打提拔不久後，葛洛班在[莎拉·布萊曼的](../Page/莎拉·布萊曼.md "wikilink")2000年-2001年「月球」巡迴演唱會（西班牙語：La
Luna Tour）中合唱《為我守候》（There For
Me），並收錄於「月球」巡迴演唱會[DVD](../Page/DVD.md "wikilink")。他的首次灌錄工作是與[蘿拉·菲比安合唱](../Page/蘿拉·菲比安.md "wikilink")《為了永遠》（For
Always），該歌曲是2001年電影《[人工智慧](../Page/人工智慧_\(電影\).md "wikilink")》的主題曲。葛洛班同時開始參與慈善工作，包括：在「[安德魯·阿格西大滿貫兒童福利活動](../Page/安德魯·阿格西.md "wikilink")」（The
Andre Agassi Grand Slam Event For
Children）中與[艾爾頓·約翰](../Page/艾爾頓·約翰.md "wikilink")、[史提夫·汪達](../Page/史提夫·汪達.md "wikilink")，[唐·亨利及](../Page/唐·亨利.md "wikilink")[羅賓·威廉斯一同演唱](../Page/羅賓·威廉斯.md "wikilink")；「[穆罕默德·阿里拳擊之夜基金會](../Page/穆罕默德·阿里.md "wikilink")」所舉辦的是向[米高·霍士及其他人致敬的活動](../Page/米高·霍士.md "wikilink")；東道主是[克林頓及其妻子](../Page/克林頓.md "wikilink")[希拉蕊·柯林頓和](../Page/希拉蕊·柯林頓.md "wikilink")[大衛·E·凱利及其妻子](../Page/大衛·E·凱利.md "wikilink")[蜜雪兒·菲佛的](../Page/蜜雪兒·菲佛.md "wikilink")2001年「家庭慶典」活動；籌募癌症研究經費的「CapCure」活動。

[Josh_groban_album.jpg](https://zh.wikipedia.org/wiki/File:Josh_groban_album.jpg "fig:Josh_groban_album.jpg")
葛洛班曾於2001年5月，在[電視節目](../Page/電視節目.md "wikilink")《[甜心俏佳人](../Page/甜心俏佳人.md "wikilink")》最後一季中飾演一角，名為馬爾柯姆·維雅特（Malcolm
Wyatt），並在劇中演唱《你還是你》（You're Still
You）。電視劇的創作人大衛·E·凱利對葛洛班在「家庭慶典」活動的演出有很深的印象。基於觀眾對葛洛班歌唱的反應，凱利在最後一季中創造了一個專為他量身訂作的角色。馬爾柯姆·維雅特的角色十分出名，共有8,000個[電郵由支持者寄出](../Page/電郵.md "wikilink")，要求葛洛班要在下一季繼續他的角色，他在該季亦演唱一支歌，名為《與你同在》（To
Where You Are）。

真正令葛洛班成名的是他在2001年11月20日首次露面的專輯。在下一年，該專輯的銷售量由金唱片變為雙白金。

在2002年2月24日，葛洛班與[夏洛蒂·澈奇在](../Page/夏洛蒂·澈奇.md "wikilink")[冬季奧運會的閉幕典禮上](../Page/冬季奧運會.md "wikilink")，合唱一曲《祈願者》（The
Prayer），而在11月，他擁有自身的特別公共廣播節目—《演唱會中的喬許·葛洛班》（Josh Groban In
Concert，2002）。至於在12月，葛洛班跟[西絲兒·凱嘉波於](../Page/西絲兒·凱嘉波.md "wikilink")[挪威首都](../Page/挪威.md "wikilink")[奧斯陸舉行的](../Page/奧斯陸.md "wikilink")合唱《與你同在》和《祈願者》。其後，他與[可兒家族](../Page/可兒家族.md "wikilink")、[羅南·基廷](../Page/羅南·基廷.md "wikilink")、[史汀](../Page/史汀.md "wikilink")、[莱昂纳尔·里奇及其他人在](../Page/莱昂纳尔·里奇.md "wikilink")[意大利首都](../Page/意大利.md "wikilink")[羅馬中的](../Page/羅馬.md "wikilink")[梵蒂岡進行一場](../Page/梵蒂岡.md "wikilink")[聖誕節表演](../Page/聖誕節.md "wikilink")。2003年，葛洛班在科士打創辦的「世界兒童日」演唱會獻唱。他更與[席琳·狄翁一同合唱歌曲](../Page/席琳·狄翁.md "wikilink")《祈願者》以及與眾多歌星，如、[尼克·卡特](../Page/尼克·卡特.md "wikilink")，[安立奎·伊格萊西亞斯和席琳](../Page/安立奎·伊格萊西亞斯.md "wikilink")·狄翁合唱最後一首歌《他們都是我們的孩子》（Aren't
They All Our Children）。

## 近年事業

[Closercover.jpg](https://zh.wikipedia.org/wiki/File:Closercover.jpg "fig:Closercover.jpg")
葛洛班第二張專輯《[靠近](../Page/靠近.md "wikilink")》，同樣是科士打製作的。此專輯在2003年11月11日發行。葛洛班認為他的第二張專輯是一張更能反映自己的專輯，而他的支持者可以透過聆聽這張專輯，更加瞭解他的性格。他又表示：「人們瞭解我是透過我的音樂。這一次，我嘗試打開盡量打開我的心窗，專輯內的音樂是對支持者瞭解我自己邁出了一大步。另外，專輯的音樂是我主要是關於我個人的音樂，故此專輯命名為『靠近』」。

在《靠近》發行兩個月後，它在廣告牌的排行榜由第十一位升至第一位\[8\]。葛洛班因翻唱的歌《[你鼓舞了我](../Page/你鼓舞了我.md "wikilink")》而在[成人當代音樂排行榜變得非常有名](../Page/成人當代音樂.md "wikilink")。他又與為電影《[木馬屠城記](../Page/木馬屠城記.md "wikilink")》演唱配樂，歌曲名為《記憶》（Remember）。至於《相信》（Believe），則是2004年的電影《[北極特快車](../Page/北極特快車.md "wikilink")》的配樂，他更翻唱[聯合公園的歌曲](../Page/聯合公園.md "wikilink")《我的十二月》（My
December）。

2004年[夏季](../Page/夏季.md "wikilink")，葛洛班回到密西根州的因特洛肯，那裏是葛洛班曾經為當地居民及宿營者表演的地方，更是分享他作為一個年輕的藝人的經驗。11月30日，他的第二張現場演唱會DVD——《生活在希臘》（Live
At The
Greek）發行。是次演唱會曾被[公共電視網](../Page/公共電視網.md "wikilink")（PBS）在系列節目中播放。同時在2004年，葛洛班與管絃樂團在[全美音樂獎共同演出一曲](../Page/全美音樂獎.md "wikilink")《記得下雨時》（Remember
When It
Rained）。其時，他在流行音樂的範疇被提名為「最受喜愛的男藝人」。葛洛班與他的錄音在2004年曾經被提名多於十二次，包括全美音樂獎、[世界音樂獎](../Page/世界音樂獎.md "wikilink")、[奧斯卡金像獎以及](../Page/奧斯卡金像獎.md "wikilink")[葛萊美獎](../Page/葛萊美獎.md "wikilink")。

其他的演出包括一系列的[脫口秀](../Page/脫口秀.md "wikilink")，如《[歐普拉·溫芙蕾秀](../Page/歐普拉·溫芙蕾秀.md "wikilink")》、《[艾倫·狄珍妮秀](../Page/艾倫·狄珍妮秀.md "wikilink")》、《[賴瑞金現場](../Page/賴瑞金現場.md "wikilink")》，《[蘿西·歐唐納秀](../Page/蘿西·歐唐納.md "wikilink")》、《》、《[傑·雷諾今夜秀](../Page/傑·雷諾.md "wikilink")》、《[20/20](../Page/20/20_\(節目\).md "wikilink")》、《[今天](../Page/今天_\(NBC\).md "wikilink")》、[梅西感恩節大遊行](../Page/梅西感恩節大遊行.md "wikilink")、[第三十八届超级碗](../Page/第三十八届超级碗.md "wikilink")，以及[洛克菲勒中心聖誕樹點燈儀式](../Page/洛克菲勒中心.md "wikilink")。\[9\]

在2006年9月的第一個星期，葛洛班最新的歌曲命名為《》，這首歌特意在[AOL作第一次的播放](../Page/AOL.md "wikilink")。這一首歌是從他第三張錄音室專輯《》中取出來，它正式發行日期為2006年11月7日。\[10\]
葛洛班不但在他錄音期間演唱《你是被愛的（別放棄）》，而且更於2006年10月26日為在[艾比路錄音室舉行的](../Page/艾比路錄音室.md "wikilink")「」唱出另一兩首從《覺醒》選出來的歌曲。由2007年2月至8月，他的「覺醒」世界巡迴演唱會到訪過71個城市，而且在9月至10月在[澳洲額外多開一場演唱會](../Page/澳洲.md "wikilink")。葛洛班又與[芭芭拉·史翠珊以](../Page/芭芭拉·史翠珊.md "wikilink")[二重唱的方式唱出](../Page/二重唱.md "wikilink")《我唯一所知的愛》（All
I Know of
Love）。另外，他又於同年與[法國歌手](../Page/法國.md "wikilink")以二重奏演唱《飛越彩虹》（Over
the Rainbow）。葛洛班表示即使在[百老匯音樂劇院表演一天都極感興趣](../Page/百老匯音樂劇.md "wikilink")。

2007年7月，葛洛班騰出數星期的時間，在[倫敦與](../Page/倫敦.md "wikilink")[倫敦愛樂樂團及](../Page/倫敦愛樂樂團.md "wikilink")[非洲的兒童合唱團灌錄聖誕節專輯](../Page/非洲.md "wikilink")。這張專輯在2007年10月9日發行，名為《》。\[11\]

根據附在CD/DVD版本的《聖誕》專輯紙條，《覺醒：現場音樂會CD+DVD》將在2008年上半年發行。

## 葛洛班迷

「葛洛班迷」（Grobanites）是一群葛洛班最為激進及忠實的支持者。葛洛班迷來自全世界的人，不論男女及年齡，都可以介定為葛洛班迷。不過，絕大部份的葛洛班迷都是女性。

## 慈善工作

[Josh_Groban_in_a_concert.jpg](https://zh.wikipedia.org/wiki/File:Josh_Groban_in_a_concert.jpg "fig:Josh_Groban_in_a_concert.jpg")的木板大會堂的演唱會|right\]\]
在大衛·科士打的指引下，葛洛班在許多慈善場合作出表演，當中包括[VH1拯救音樂](../Page/VH1.md "wikilink")（2005年）、海嘯援助：希望音樂會（2005年）、第五屆收養雷區組織年度音樂會（2005年）、第二屆年度葛萊美·傑姆（2nd
Annual Grammy
Jam，2005年）、[現場八方](../Page/現場八方.md "wikilink")（2005年）、心臟基金會慈善晚會（2005年）及大卫·福斯特及其朋友慈善晚会（2006年）。葛洛班在2004年的一個[南非旅程中探望](../Page/南非.md "wikilink")[納爾遜·曼德拉時](../Page/納爾遜·曼德拉.md "wikilink")，從中得到啟發。曼德拉確立葛許．葛洛班基金，此項基金乃幫助兒童的教育、健康及藝術發展。\[12\]
曼德拉指定葛洛班為其曼德拉計劃的官方大使，那是一個協助提升全球人士對[非洲](../Page/非洲.md "wikilink")[愛滋病關注活動](../Page/愛滋病.md "wikilink")。2007年4月25日，葛洛班與在《[美國偶像](../Page/美國偶像.md "wikilink")》的〈偶像回馈〉單元演唱其插曲。同年的9月2日，葛洛班曾為籌得150,000美元的善款，以支持音樂教育。\[13\]

## 影響及個人資料

一些葛洛班的音樂是受[電台司令](../Page/電台司令.md "wikilink")、[保羅·西蒙](../Page/保羅·西蒙.md "wikilink")、[史汀](../Page/史汀.md "wikilink")、[彼得·蓋布瑞爾及](../Page/彼得·蓋布瑞爾.md "wikilink")[碧玉影響](../Page/碧玉.md "wikilink")。\[14\]
他說他能由任何人決定他能否衝出音樂的框架及邊界。至於[聲樂](../Page/聲樂.md "wikilink")，他認為「有人以歌聲道出自己的故事」，當中包括、、及[盧奇亞諾·帕華洛帝](../Page/盧奇亞諾·帕華洛帝.md "wikilink")。\[15\]

## 唱片分類

### [唱片](../Page/唱片.md "wikilink")

  - *[Josh Groban](../Page/Josh_Groban_\(album\).md "wikilink")* - 2001
  - *Josh Groban In Concert* CD/DVD - 2002
  - *[Closer](../Page/Closer_\(Josh_Groban_album\).md "wikilink")*,
    available with DVD - 2003
  - *Live at the Greek* CD/DVD - 2004
  - ''[Awake](../Page/Awake_\(Josh_Groban_album\).md "wikilink"),
    available with DVD - 2006
  - *With You* (Hallmark promotional disc) - 2007
  - *[Noel](../Page/Noel_\(Josh_Groban_album\).md "wikilink")*,
    available with DVD - Christmas Album - 2007
  - *[Awake: The Live Concert CD +
    DVD](../Page/Awake:_The_Live_Concert_CD_+_DVD.md "wikilink")*, -
    Early 2008
  - *[ILLUMINATIONS](../Page/ILLUMINATIONS.md "wikilink")*, - 2010
  - *[ALL THAT ECHOES](../Page/ALL_THAT_ECHOES.md "wikilink")*, - 2013

### [演唱會](../Page/演唱會.md "wikilink")

  - [莎拉·布萊曼](../Page/莎拉·布萊曼.md "wikilink"): *La Luna: Live in Concert*
    (DVD) - 2001 - Duet on "There for Me"
  - *[A.I. - Artificial Intelligence](../Page/A.I.人工智慧.md "wikilink")*
    Original Motion Picture Score - 2001 - Duet on "For Always" (with
    [Lara Fabian](../Page/Lara_Fabian.md "wikilink"))
  - *[Enchantment](../Page/Enchantment_\(album\).md "wikilink")* - 2001
    - Duet on "[The Prayer](../Page/The_Prayer.md "wikilink")"
  - Josh Groban In Concert - 2002
  - *[Prelude: The Best of Charlotte
    Church](../Page/Prelude:_The_Best_of_Charlotte_Church.md "wikilink")*
    - 2002 - Duet on "The Prayer"
  - *Enchantment: Charlotte Church* (DVD) - 2002 - Duet on "Somewhere"
    and "The Prayer"
  - *[Duets](../Page/Duets_\(Streisand_album\).md "wikilink")* with
    [Barbra Streisand](../Page/Barbra_Streisand.md "wikilink") - 2002 -
    Duet on "All I Know Of Love"
  - *Concert for World Children's Day* (DVD) - 2003 - Performed "Gira
    Con Me," "To Where You Are," "The Prayer" (duet with
    [席琳·狄翁](../Page/席琳·狄翁.md "wikilink")), and "Aren't They All
    Our Children Anthem" as a cast.
  - [CHESS: Actors' Benefit Fund
    Concert](../Page/Chess_\(musical\)#Actor's_Fund_of_America_Concert_2003.md "wikilink")
    - 2003
  - *[Troy](../Page/Troy_\(film\).md "wikilink")* Soundtrack - 2004 -
    "Remember" with [Tanja
    Tzarovska](../Page/Tanja_Tzarovska.md "wikilink")
  - *[The Polar
    Express](../Page/The_Polar_Express_\(film\).md "wikilink")*
    Soundtrack - 2004 - "Believe"
  - Live At The Greek - 2004
  - Come Together Now - Hurricane Relief Concert - 2005
  - Hurricane Relief - Come Together Now: "Alla Luce Del Sole" & "Tears
    in Heaven" - 2005 - Participated in the fund raising concert and CD
    for victims of the hurricanes of 2005, including
    [Katrina](../Page/Hurricane_Katrina.md "wikilink") and
    [Rita](../Page/Hurricane_Rita.md "wikilink").
  - *[Lady in the Water](../Page/Lady_in_the_Water.md "wikilink")*
    (teaser trailer) - 2006 - "Mi Mancherai (Il Postino)"
  - *[Barbara Cook](../Page/Barbara_Cook.md "wikilink") at the Met* (CD)
    - 2006 - Duet on "Move on" and "Not While I'm Around"
  - [全美音樂獎](../Page/全美音樂獎.md "wikilink") - 2006 - performed "February
    Song"
  - [肯尼迪中心榮譽獎](../Page/肯尼迪中心榮譽獎.md "wikilink") - 2006 - honoring [Andrew
    Lloyd Webber](../Page/Andrew_Lloyd_Webber.md "wikilink") and sang
    "Music of the Night"
  - [Macy's Thanksgiving Day
    Parade](../Page/Macy's_Thanksgiving_Day_Parade.md "wikilink") - 2006
    - performed "February Song"
  - Awake Tour - 2007
  - [美国偶像](../Page/美国偶像.md "wikilink") Idol Gives Back - 2007 -
    performed "You Raise Me Up" with The African Children's Choir
  - [Concert For Diana](../Page/Concert_For_Diana.md "wikilink") - 1st
    July 2007 - performed "All I Ask Of You" of "Phantom of The Opera"
    alongside Sarah Brightman

### [電視劇](../Page/電視劇.md "wikilink")/[電影](../Page/電影.md "wikilink")

  - *[Ally McBeal](../Page/Ally_McBeal.md "wikilink")* Season 4 Box Set
    2 (DVD) - 2002 - In episode "The Wedding" Josh plays Malcolm Wyatt
    and performs "You're Still You"
  - *Ally McBeal* Season 5 Box Set 1 (DVD) - 2003 - In episode "Nine One
    One" Josh plays Malcolm Wyatt and performs "To Where You Are"

[Glee](http://en.wikipedia.org/wiki/Glee_\(TV_series\)) Season 1
"Journey"(1.22) .... Himself /"Acafellas" (1.03) .... Himself

## 參考資料

## 外部連結

  - [官方網站](http://www.joshgroban.com/)

[Category:21世紀美國歌手](../Category/21世紀美國歌手.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")
[Category:美國男中音](../Category/美國男中音.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美国男歌手](../Category/美国男歌手.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:挪威裔美國人](../Category/挪威裔美國人.md "wikilink")
[Category:美国流行音乐歌手](../Category/美国流行音乐歌手.md "wikilink")
[Category:義大利語歌手](../Category/義大利語歌手.md "wikilink")
[Category:洛杉矶市男演员](../Category/洛杉矶市男演员.md "wikilink")
[Category:華納音樂集團旗下歌手](../Category/華納音樂集團旗下歌手.md "wikilink")

1.

2.

3.

4.

5.  [Groban Archives: Dave 'Til Dawn interview
    transcript](http://www.grobanarchives.com/transcripts/tra021213a.html)


6.  [Evening at Pops 2004: Biographies: Josh
    Groban](http://www.pbs.org/wgbh/pops/background/bios/groban.html)

7.  [Official Josh Groban site](http://www.joshgroban.com)

8.  [VH1 Josh Groban
    biography](http://www.vh1.com/artists/az/groban_josh/bio.jhtml)

9.  [Josh Groban
    biography](http://www.8notes.com/biographies/groban.asp)

10. [Josh Groban New CD "Awake" in Stores on
    November 7th](http://www.marketwire.com/mw/release_html_b1?release_id=162536)

11. [Josh Groban- Noel](http://www.joshgroban.com/node/298)

12. [Josh Groban Message
    Forum](http://p071.ezboard.com/fjoshgrobanfrm17.showMessage?topicID=1.topic)


13. [Singer Josh Groban donates $150,000 to
    CMS](http://www.cms.k12.nc.us/includes/gfi.asp?fileHandle=4547.asptopicID=2.topic)


14.
15. [On the Brink of Stardom: Josh Groban live Internet chat on
    ABCNEWS.com](http://www.olografix.org/krees/dfnet/html/readinterviews.php?ID=7)