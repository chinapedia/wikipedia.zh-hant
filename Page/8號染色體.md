人類的**8號染色體**是23對[染色體的其中之一](../Page/染色體.md "wikilink")，正常狀況下每個[細胞擁有兩條](../Page/細胞.md "wikilink")。此染色體含有大約155百萬個[鹼基對](../Page/鹼基對.md "wikilink")，佔細胞內所有[DNA的](../Page/DNA.md "wikilink")4.5%到5%\[1\]。

该染色体有两条臂，分别命名为8p和8q。其中较短的8p臂有大约4,500万个碱基对，有基因484个，[假基因](../Page/假基因.md "wikilink")110个，约占总基因组数量的1.5%。这其中8%的基因和[神经发育及功能有关](../Page/脑部发育.md "wikilink")，16%的基因和[癌症有关](../Page/癌症.md "wikilink")。8p臂有一个很独特的特点，即存在一个约1,500万个碱基对的巨大区域，该区域的突变几率非常高。而在这一片区域，人类基因和[黑猩猩的基因存在巨大的差异](../Page/黑猩猩.md "wikilink")。很可能正是因为该区域的高突变率，对人类脑部的进化做出了贡献\[2\]。

其中約有700到1000個基因，依預測方式而有所不同。

## 基因与假基因

以下是一些位于8号染色体上的基因与假基因：

  -
  - : [成纤维细胞生长因子受体](../Page/成纤维细胞生长因子.md "wikilink")1

  - : 神经节苷脂诱导分化相关蛋白1

  - : [脂蛋白酶](../Page/脂蛋白酶.md "wikilink")

  - : [甲状腺球蛋白](../Page/甲状腺球蛋白.md "wikilink")

  - [TPA](../Page/TPA.md "wikilink"):
    [组织型纤溶酶原激活剂](../Page/组织型纤溶酶原激活剂.md "wikilink")

  - 假基因: 原本用于合成[维生素C的酶](../Page/维生素C.md "wikilink")

## 参考文献

<references />

  -
  -
## 參見

  - [位於8號人類染色體的基因](../Category/位於8號人類染色體的基因.md "wikilink")

{{-}}

[Category:人類染色體](../Category/人類染色體.md "wikilink")

1.

2.