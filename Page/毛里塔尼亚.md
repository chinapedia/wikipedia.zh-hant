**毛里塔尼亚伊斯兰共和国**（；），通稱**毛里塔尼亞**（；），是[西非](../Page/西非.md "wikilink")[阿拉伯国家之一](../Page/阿拉伯国家.md "wikilink")。与[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")、[西撒哈拉](../Page/西撒哈拉.md "wikilink")、[马里](../Page/马里.md "wikilink")、[塞内加尔接壤](../Page/塞内加尔.md "wikilink")。

## 中文名稱

[中华民国將其音譯為](../Page/中华民国.md "wikilink")「**茅利-{}-塔尼亞**」，簡稱「**茅國**」\[1\]；[中华人民共和国將其音譯為](../Page/中华人民共和国.md "wikilink")「**毛里-{}-塔尼亚**」，簡稱「**毛塔**」\[2\]。

## 历史

44年，成為羅馬帝國行省。7世纪[阿拉伯人来到这裡](../Page/阿拉伯人.md "wikilink")，建立了封建王国。15世纪开始，先後被[葡萄牙](../Page/葡萄牙.md "wikilink")、[荷兰和](../Page/荷兰.md "wikilink")[英国殖民](../Page/英国.md "wikilink")。1912年成为法国殖民地；1920年成为[法属西非洲的一部分](../Page/法属西非洲.md "wikilink")；1957年获得半自治共和国地位；1958年成为[法兰西共同体的一个](../Page/法兰西共同体.md "wikilink")[自治共和国](../Page/自治共和国.md "wikilink")；1960年11月28日宣佈独立，名**毛里塔尼亚伊斯兰共和国**，并退出共同体。[1](http://news.xinhuanet.com/ziliao/2002-06/19/content_447748.htm)

## 政治

[Sidi_Mohamed_Ould_Cheikh_Abdallahi.jpg](https://zh.wikipedia.org/wiki/File:Sidi_Mohamed_Ould_Cheikh_Abdallahi.jpg "fig:Sidi_Mohamed_Ould_Cheikh_Abdallahi.jpg")\]\]
1998年總理柯納（）就任，一般認為總統泰亞（）之所以任命柯納為總理，係想藉重其財經專長，負責與國際貨幣基金及世界銀行討論有關減免外債之任務，並爭取外援。

獨立初期曾實行過[多黨制](../Page/多黨制.md "wikilink")。達達赫執政時期，實行[一黨制](../Page/一黨制.md "wikilink")。1978年軍人執政後取締一切政黨。1991年8月，總統泰亞宣佈開放[黨禁](../Page/黨禁.md "wikilink")，頒佈政黨法，開始實行多黨制。

目前政府承認的合法政黨共30個，主要有：

  - 民主社會共和黨（Parti Républicain Démocratique et
    Social－PRDS），1991年8月28日成立。為[執政黨](../Page/執政黨.md "wikilink")，共有黨員45萬。該黨在政治上支援塔亞，主張推行多元化政治和民主制度，維護民族團結；鼓勵私營經濟，改善人民生活；發展睦鄰友好，捍衛國家領土和主權；主張普及教育，號召全民學習知識。《共和周刊》（AlJoumhouria）爲黨的機關刊物。
  - 民主進步聯盟（Union pour la Démocratie et le
    Progrès簡稱UDP），1993年6月11日成立，是總統多數派政黨之一。黨主席娜哈·曼特·穆克納斯（Naha
    Mint Mouknass）。
  - 民主團結聯盟（Rassemblement pour la Démocratie et
    l'Unité－RDU），1991年8月28日成立，是總統多數派政黨之一。黨主席阿赫邁德·烏爾德·西迪·巴巴（Ahmed
    Ould Sidi Baba）。

其他主要政黨有：進步力量聯盟（Union des Forces du Progrès-UFP）、民主力量联盟（Rassemblement des
Forces Démocratiques-RFD）、茅利塔尼亞革新與協調黨（Parti Mauritanien pour le
Renouveau et la Concorde－PMRC）、社會民主人民聯盟（Union Populaire Socialiste et
Démocratique）、民主正義黨(Parti pour la Justice
Démocratique－PJD）、人民進步聯盟（Alliance Populaire
Progressiste－APP）等。

2005年8月3日，总统卫队趁总统塔亚不在国内之际发动军事政变，宣布成立“民主与公正军事委员会”。\[3\]8月4日，民主与公正军事委员会宣布解散国民议会，并将军事委员会宪章添加到宪法中。\[4\]8月6日，民主与公正军事委员会通过了过渡期权力宪章。\[5\]8月10日，民主与公正军事委员会组成了过渡政府，总理为[布巴卡尔](../Page/布巴卡尔.md "wikilink")。\[6\]

2006年6月25日，毛里塔尼亚就修改宪法条款举行全民投票，以确保国家政权顺利实行民主过渡。\[7\]11月19日，毛里塔尼亚举行议会和市政选举。\[8\]2007年3月11日，举行总统选举。\[9\][阿卜杜拉希和](../Page/西迪·乌尔德·谢赫·阿卜杜拉希.md "wikilink")[艾哈迈德·达达赫进入第二轮](../Page/艾哈迈德·达达赫.md "wikilink")。\[10\]3月25日，举行总统选举第二轮投票。\[11\]阿卜杜拉西当选总统。\[12\]4月20日，总统阿卜杜拉希任命[扎因·扎伊丹为总理](../Page/扎因·扎伊丹.md "wikilink")。\[13\]

2008年8月6日，毛里塔尼亚发生军事[政变](../Page/政变.md "wikilink")，总统阿卜杜拉希和总理瓦格夫被扣押\[14\]。其後部份軍人成立了[軍政府国务委员会](../Page/軍政府.md "wikilink")。军政府於8月11日释放了总理瓦格夫和内政部长等4人，但总统阿卜杜拉希仍被软禁在首都努瓦克肖特。国务委员会在8月12日通过法令规定，军队将遵照宪法通过国务委员会集体领导行使总统权力，有权任命政府总理和各部部长。毛里塔尼亚军政权国务委员会8月14日发布命令，任命[穆拉耶·烏爾德·穆罕默德·拉格達夫为新总理](../Page/穆拉耶·烏爾德·穆罕默德·拉格達夫.md "wikilink")。\[15\]2009年7月18日舉行總統選舉，政變領導者[穆罕默德·乌尔德·阿卜杜勒·阿齐兹當選](../Page/穆罕默德·乌尔德·阿卜杜勒·阿齐兹.md "wikilink")。\[16\]\[17\]

## 地理

[Mauritanie_-_Adrar2.jpg](https://zh.wikipedia.org/wiki/File:Mauritanie_-_Adrar2.jpg "fig:Mauritanie_-_Adrar2.jpg")沙漠區\]\]
[Location_Mauritania_AU_Africa.svg](https://zh.wikipedia.org/wiki/File:Location_Mauritania_AU_Africa.svg "fig:Location_Mauritania_AU_Africa.svg")
西瀕[大西洋](../Page/大西洋.md "wikilink")，東與馬利接壤，南與塞內加爾交界，北與摩洛哥、阿爾及利亞相連之茅利塔尼亞原為法國殖民地，面積103萬0700平方公里，1988年宣佈12海哩領海水域和200海哩專屬經濟區水域。全境內有一半國土屬[撒哈拉沙漠](../Page/撒哈拉沙漠.md "wikilink")，境內唯一常年性河流為西南邊境之[塞內加尔河及其支流](../Page/塞內加尔河.md "wikilink")。

[热带沙漠气候](../Page/热带沙漠气候.md "wikilink")；氣候炎熱乾旱，最冷月均溫仍達攝氏20度以上，3月到4月間並有[沙塵暴](../Page/沙塵暴.md "wikilink")，生活條件惡劣。

雨量由南向北遞減，東部和北部地區因雨量稀少幾乎無地表水，年降水量100－50毫米以下，西南边境塞内加尔河一带年降水量可达400毫米以上，草原茂盛。

## 行政

[Mauritania_regions_numbered.png](https://zh.wikipedia.org/wiki/File:Mauritania_regions_numbered.png "fig:Mauritania_regions_numbered.png")
[Central_mosque_in_Nouakchott.jpg](https://zh.wikipedia.org/wiki/File:Central_mosque_in_Nouakchott.jpg "fig:Central_mosque_in_Nouakchott.jpg")\]\]
[Atar_Market_(2).JPG](https://zh.wikipedia.org/wiki/File:Atar_Market_\(2\).JPG "fig:Atar_Market_(2).JPG")
[Bareina,_Mauritania.jpg](https://zh.wikipedia.org/wiki/File:Bareina,_Mauritania.jpg "fig:Bareina,_Mauritania.jpg")
現全國劃爲首都諾克少及12個省（WILAYA）、53個縣（MOUGHATAA），縣下設區（ARRONDISSEMENT）。全國有216個市鎮（COMMUNE）。

1.  [阿德拉爾省](../Page/阿德拉爾省_\(毛里塔尼亞\).md "wikilink")（Adrar）
2.  [阿薩巴省](../Page/阿薩巴省.md "wikilink")（Assaba）
3.  [布拉克納省](../Page/卜拉克納省.md "wikilink")（Brakna）
4.  [達赫萊特·努瓦迪布省](../Page/努瓦迪布灣省.md "wikilink")（Dakhlet Nouadhibou）
5.  [戈戈爾省](../Page/戈爾戈勒省.md "wikilink")（Gorgol）
6.  [吉迪馬卡省](../Page/吉迪馬卡省.md "wikilink")（Guidimaka）
7.  [東霍德省](../Page/東胡德省.md "wikilink")（Hodh Ech Chargui）
8.  [西霍德省](../Page/西胡德省.md "wikilink")（Hodh El Gharbi）
9.  [因奇利省](../Page/因希里省.md "wikilink")（Inchiri）
10. 首都[諾克少](../Page/諾克少.md "wikilink")（Nouakchott）
11. [塔崗省](../Page/塔甘特省.md "wikilink")（Tagant）
12. [提利斯·賽穆爾省](../Page/提里斯-宰穆爾省.md "wikilink")（Tiris Zemmour）
13. [特拉薩省](../Page/特拉扎省.md "wikilink")（Trarza）

## 經濟

世界銀行認定的[重債窮國](../Page/重債窮國.md "wikilink")，由於乾旱，主要的農業活動以[畜牧業為主](../Page/畜牧業.md "wikilink")，耕地僅限於塞內加爾河沿岸及沙漠[綠洲](../Page/綠洲.md "wikilink")，主要農作物為[粟](../Page/粟.md "wikilink")、[椰棗](../Page/椰棗.md "wikilink")、[稻米及](../Page/稻米.md "wikilink")[玉米](../Page/玉米.md "wikilink")。[稻米為茅利塔尼亞人主食](../Page/稻米.md "wikilink")，因耕地有限，糧食無法自給自足需仰賴進口。

以往[鐵礦之開採係茅利塔尼亞之主要外匯來源](../Page/鐵.md "wikilink")，其出口值佔出口總值之一半，惟自1980年後隨著需求減少及價格下滑，鐵礦出口值有逐年遞減現象，反觀漁產品之出口值則逐年遞增，1995年漁產品出口值佔茅利塔尼亞總出口值之57%，已成為茅利塔尼亞最重要之經濟活動。

由於資源有限再加上氣候因素，茅利塔尼亞經濟並不發達，國民平均所得僅約400美元。1980年後隨著外匯收入之減少、乾旱及不當的經濟政策，儘管外資投資金額佔國內生產毛額的36%，但整體之年經濟成長率僅有2%，再加上大量舉債結果，外債金額不斷增加，迄1995年已達25億美元。1996年茅利塔尼亞全國預算收入為3億2900萬美元，支出為2億6500萬美元，[通貨膨脹率](../Page/通貨膨脹率.md "wikilink")4.7%，[失業率達](../Page/失業率.md "wikilink")23%。為解決日益嚴重的經濟危機，目前茅利塔尼亞政府已與[世界銀行](../Page/世界銀行.md "wikilink")、[國際貨幣基金及主要援助國討論並決定採行整合性經濟財政政策](../Page/國際貨幣基金.md "wikilink")，力求總體經濟面之穩定，短期內藉由削減不必要的支出、消除貿易障礙、增進市場透明自由化、加強對資源之管理，以抑制外債金額繼續增加危及物價之穩定。

毛里塔尼亚也有[奴隶制残余](../Page/奴隶制.md "wikilink")，祖先是阿拉伯人的摩尔人是貴族。

## 人口

近336萬（2012年人口普查結果），平均壽命61歲（2011年估计）。

民族組成為40%的白[摩爾人](../Page/摩爾人.md "wikilink")（為阿拉伯－柏柏爾血統），另外黑摩爾人（阿拉伯文化語言傳統的[哈拉廷人](../Page/哈拉廷人.md "wikilink")）和非洲黑人各佔30%。主要[部族是](../Page/部族.md "wikilink")[圖庫勒族](../Page/圖庫勒.md "wikilink")、[頗爾族](../Page/富拉尼人.md "wikilink")、[索寧克族](../Page/索寧克.md "wikilink")、[沃洛夫族和](../Page/沃洛夫族.md "wikilink")[班巴拉族](../Page/班巴拉.md "wikilink")。\[18\]

根據1991年7月20日頒佈的新憲法規定，[伊斯蘭教為國教](../Page/伊斯蘭教.md "wikilink")（99.1%的國民為穆斯林），[阿拉伯語爲官方語言](../Page/阿拉伯語.md "wikilink")，[法語爲通用語言](../Page/法語.md "wikilink")。其他民族語言有哈桑語、布拉爾語、索寧克語和沃洛夫語。[識字率為](../Page/識字率.md "wikilink")37%。其中男性識字人口佔男性總人口之50%，女性僅佔26%。

國民中有高達75%居住於都市地區如首都諾克少、西北部之諾瓦迪布（Nouadhibou）及塞內加爾河沿岸地區。另外25%之民眾則過著游牧生活，因氣候炎熱乾燥，生活條件比較惡劣。

## 文化

[缩略图](https://zh.wikipedia.org/wiki/File:Sahara_oog.jpg "fig:缩略图")空拍\]\]

[摩爾人是茅利塔尼亞的一個主要民族](../Page/摩爾人.md "wikilink")，他們[女兒的婚事都是由](../Page/女兒.md "wikilink")[母親做主](../Page/母親.md "wikilink")，[父親不能干預](../Page/父親.md "wikilink")。小夥子看中哪位[姑娘](../Page/姑娘.md "wikilink")，男方母親便帶禮物到姑娘的母親那提親，姑娘的母親如同意婚事便定下來。婚禮時，新郎身嶄新[白色或](../Page/白色.md "wikilink")[藍色的肥大長袍](../Page/藍色.md "wikilink")，頭上纏一條長達三米多的白頭巾。新娘穿著色澤鮮豔的花[裙子](../Page/裙子.md "wikilink")，頭上纏同裙子一色的布圍巾。婚禮結束後，夫婦共度一星期，便各自回父母家。兩個月後，再共同生活幾天，隨後再分開。這樣持續兩年時間。然後，新郎同親朋好友一道，牽著數頭駱駝來到新娘家，將新娘接回去。從此互不分開。過去，茅利塔尼亞人結婚都很早，女孩十多歲就出嫁。婚後每隔幾天回娘家，將新婚各種事情講述給母親，母親則仔細教導她。兩年後，新娘長大成人，分居兩地生活結束。

摩爾人女人審美觀是腰身粗、[脖子短](../Page/脖子.md "wikilink")、[臀部突出](../Page/臀部.md "wikilink")、[乳房高聳](../Page/乳房.md "wikilink")。[肥胖女人是財富的象徵](../Page/肥胖.md "wikilink")。小夥子娶肥胖超群的妻子，其婚禮必然異常隆重，親人會遠道趕來一睹新娘芳容。母親總是競相研究肥胖之道。富貴人家女子，從七八歲開始，每日喝[羊奶](../Page/羊奶.md "wikilink")，吃富含[脂肪的食物](../Page/脂肪.md "wikilink")，很少參加戶外活動，由女[僕人在身上抹油脂](../Page/僕人.md "wikilink")。普通人家父母要求女兒喝下大量[駱駝的](../Page/駱駝.md "wikilink")[駱駝奶](../Page/駱駝奶.md "wikilink")。此外，女孩幾乎每天還要定時脫去衣服在軟沙上轉動打滾，
將身體上凹凸不平的地方磨平，只見肉不見骨。

<File:Mauritanian> troops stage border drills.jpg|毛里塔尼亚陸軍
<File:Mauritanians> mans.jpg|首都貧民區街頭 <File:Festivals> de lunité
nationale en Mauritanie (6005036267).jpg|草帽編織業
<File:DSC_1387_(4306648627).jpg>|[塔甘特省農具店](../Page/塔甘特省.md "wikilink")
[File:Nkttnovotel.jpg|nouakchott大飯店](File:Nkttnovotel.jpg%7Cnouakchott大飯店)
<File:Chinguetti-Guide.JPG>|[塞內加爾邊界的社區房屋](../Page/塞內加爾.md "wikilink")
[File:Camel_truck.jpg|駱駝養殖業](File:Camel_truck.jpg%7C駱駝養殖業)
<File:Mauritania_children.jpg>|[塔甘特省農村小學](../Page/塔甘特省.md "wikilink")
[File:Nouakchott_camel_market1.jpg|流動市集趕集](File:Nouakchott_camel_market1.jpg%7C流動市集趕集)
<File:KaediRegionalHospital2.JPG>|[東胡德省農村醫院](../Page/東胡德省.md "wikilink")

## 参考文献

## 相关模板

[毛里塔尼亚](../Category/毛里塔尼亚.md "wikilink")
[Category:西非](../Category/西非.md "wikilink")
[Category:非洲国家](../Category/非洲国家.md "wikilink")

1.
2.
3.  [2](http://news.xinhuanet.com/world/2005-08/04/content_3306399.htm)
4.  [3](http://news.xinhuanet.com/world/2005-08/05/content_3311465.htm)
5.  [4](http://news.xinhuanet.com/world/2005-08/07/content_3319919.htm)
6.  [5](http://news.xinhuanet.com/world/2005-08/11/content_3337098.htm)
7.  [6](http://news.xinhuanet.com/newscenter/2006-06/26/content_4747910.htm)
8.  [7](http://news.xinhuanet.com/world/2006-11/19/content_5350616.htm)
9.  张崇防，陈顺，[《毛-{里}-塔尼亚举行总统选举》](http://news.xinhuanet.com/world/2007-03/11/content_5831813.htm)，新华网
10. 陈顺，[《毛-{里}-塔尼亚将举行第二轮总统选举》](http://news.xinhuanet.com/world/2007-03/13/content_5837485.htm)，新华网
11. 陈顺，[《毛-{里}-塔尼亚举行总统选举第二轮投票》](http://news.xinhuanet.com/world/2007-03/25/content_5894593.htm)，新华网
12. 陈顺，[《毛-{里}-塔尼亚选出新总统》](http://news.xinhuanet.com/world/2007-03/27/content_5899858.htm)，新华网
13. 周少平，[《毛-{里}-塔尼亚前央行行长被任命为总理》](http://news.xinhuanet.com/world/2007-04/21/content_6007382.htm)，新华网
14. <http://news.sina.com.cn/w/2008-08-06/183516073172.shtml>
15. [中國新聞網-毛-{里}-塔尼亚军政权任命新总理
    新政府下周初成立](http://www.chinanews.com.cn/gj/lmfz/news/2008/08-15/1348991.shtml)
16. [Q\&A: Mauritania
    elections](http://news.bbc.co.uk/2/hi/africa/8077496.stm)，BBC
    News，2009年7月17日。
17. [General wins Mauritania
    election](http://news.bbc.co.uk/2/hi/africa/8157851.stm)，BBC
    News，2009年7月19日。
18. [中华人民共和国驻毛-{里}-塔尼亚伊斯兰共和国大使馆，毛-{里}-塔尼亚简介：民族](http://mr.china-embassy.org/chn/sbgx/mltnyjj/t215956.htm)