**長與町**（）是[日本](../Page/日本.md "wikilink")[長崎縣轄下的](../Page/長崎縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，位於[長崎市市區東北方約](../Page/長崎市.md "wikilink")10公里，屬[西彼杵郡](../Page/西彼杵郡.md "wikilink")。

## 历史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，9鄉（村）合併為**長與村**。
  - 1969年1月1日：改制為**長與町**。\[1\]
  - 1982年7月23日：町公所所在地測得1小時最大雨量187mm，是日本觀測史上最大值（[長崎大水害](../Page/長崎大水害.md "wikilink")）。

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [長崎本線](../Page/長崎本線.md "wikilink")：[本川內車站](../Page/本川內車站.md "wikilink")
        - [長與車站](../Page/長與車站.md "wikilink") -
        [高田車站](../Page/高田車站_\(長崎縣\).md "wikilink")
        - [道之尾車站](../Page/道之尾車站.md "wikilink")

|                                                                                                              |                                                                                                       |
| ------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------- |
| [Honkawachi_stn_1.jpg](https://zh.wikipedia.org/wiki/File:Honkawachi_stn_1.jpg "fig:Honkawachi_stn_1.jpg") | [Nagayo_Station.jpg](https://zh.wikipedia.org/wiki/File:Nagayo_Station.jpg "fig:Nagayo_Station.jpg") |
| [Kodastation.jpg](https://zh.wikipedia.org/wiki/File:Kodastation.jpg "fig:Kodastation.jpg")                  |                                                                                                       |

## 觀光資源

  - 崎野之鼻
  - 堂崎鼻
  - [中尾城公園](../Page/中尾城.md "wikilink")
  - 長與水庫
  - 琴之尾岳

## 教育

### 大學

  - [長崎縣立大学西博爾德校](../Page/長崎縣立大学西博爾德校.md "wikilink")

### 高等學校

  - [長崎縣立長崎北陽台高等學校](../Page/長崎縣立長崎北陽台高等學校.md "wikilink")
  - [長崎市立長崎商業高等學校](../Page/長崎市立長崎商業高等學校.md "wikilink")（校區位於與長崎市邊界上）

## 參考資料

## 外部連結

  - [長與町商工會](https://web.archive.org/web/20080415213047/http://www1.cncm.ne.jp/%7Enagayo/)

<!-- end list -->

1.