<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><a href="../Page/多塞特郡.md" title="wikilink">多塞特郡</a>（<a href="../Page/西南英格蘭.md" title="wikilink">英格蘭西南部</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Dorse.svg" title="fig:Dorse.svg">Dorse.svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>行　政　區　劃</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Dorset_Ceremonial_Numbered.png" title="fig:Dorset_Ceremonial_Numbered.png">Dorset_Ceremonial_Numbered.png</a></p>
</center>
<p><small>(*) 黃色部份為<a href="../Page/單一管理區.md" title="wikilink">單一管理區</a>。</small></p></td>
</tr>
</tbody>
</table>

**伯恩茅斯足球會**（），[英格蘭](../Page/英格蘭.md "wikilink")[西南部](../Page/西南英格蘭.md "wikilink")[多塞特郡海邊渡假城市](../Page/多塞特郡.md "wikilink")[-{伯恩茅斯}-](../Page/伯恩茅斯.md "wikilink")（Bournemouth）的職業足球會，成立於1899年，[迪恩考特球場](../Page/迪恩考特球場.md "wikilink")（Dean
Court）作為主場，過去曾冠名為「金沙球場」（Goldsands stadium）。

## 歷史

般尼茅夫自創會以來，長期在低組別聯賽角逐，踏入廿一世紀，球會更出現財政危機，最後在2008年宣佈債務重組，並被扣十分，以至當時在英甲角逐的般尼茅夫最後降級到[英乙作賽](../Page/英乙.md "wikilink")。2008/09年度球季開季時，已降至英乙作賽的般尼茅夫因財務問題未解決而再被扣17分，最後幾經辛苦才在該季英乙護級成功，保住職業聯賽席位。經歷一季幾乎破產及降班離開英格蘭足球聯賽後，般尼茅夫於[2009/10年球季獲得英乙聯賽亞軍](../Page/2009年至2010年英格蘭足球乙級聯賽.md "wikilink")，升班返回[英格蘭甲級聯賽](../Page/英格蘭足球甲級聯賽.md "wikilink")。經歷兩季後，於[2012/13年球季取得聯賽亞軍](../Page/2012年至2013年英格蘭足球甲級聯賽.md "wikilink")，歷來第二次升班[次級聯賽](../Page/英格蘭足球冠軍聯賽.md "wikilink")；再[兩季後](../Page/2014年至2015年英格蘭足球冠軍聯賽.md "wikilink")（2015年）贏得英冠冠軍，創會以來首次升班到[頂級聯賽作賽](../Page/英超.md "wikilink")。

般尼茅夫在首個在[頂級聯賽作賽的賽季](../Page/英超.md "wikilink")（2015/16年度）中，取得英超第16名，成功保留在英超的席位。2016/17年度球季，般尼茅夫更突破上季成績，取得英超第9名。2017/18年度球季，般尼茅夫取得英超第12名，保證連續四季在英超作賽。

## 大事紀年

| <font color="white"> **年　份**                          | <font color="white"> **事　項**                                                                                                                                                     |
| ----------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1890                                                  | 球隊前身“博斯坎比聖約翰”（Boscombe St. Johns Institute FC）成立                                                                                                                                 |
| 1899                                                  | 博斯坎比聖約翰解散，部分成員組成“博斯坎比”（Boscombe FC）                                                                                                                                              |
| 1920-21                                               | 參加南部〈英格蘭〉聯賽                                                                                                                                                                      |
| 1922-23                                               | 南部〈英格蘭〉聯賽亞軍                                                                                                                                                                      |
| 1923                                                  | 更名“-{zh-hans:伯恩茅斯; zh-hk:般尼茅夫;}-及博斯坎比”（Bournemouth & Boscombe Athletic FC），加入足球聯盟南區丙組聯賽                                                                                          |
| 1947-48                                               | 南區丙組聯賽亞軍                                                                                                                                                                         |
| 1958-59                                               | 聯賽重組後被置於丙組聯賽                                                                                                                                                                     |
| 1970                                                  | 丙組聯賽排第廿一位，降級丁組                                                                                                                                                                   |
| 1970-71                                               | 丁組聯賽亞軍，升級丙組                                                                                                                                                                      |
| 1971                                                  | 更名-{zh-hans:伯恩茅斯; zh-hk:般尼茅夫;}-（AFC Bournemouth）                                                                                                                                 |
| 1975                                                  | 丙組聯賽排第廿一位，降級丁組                                                                                                                                                                   |
| 1981-82                                               | 丁組聯賽排第四位，升級丙組                                                                                                                                                                    |
| 1983-84                                               | 決賽2-1擊敗[-{zh-hans:赫尔城; zh-hk:侯城;}-](../Page/赫尔城足球俱乐部.md "wikilink")，**聯賽錦標冠軍**                                                                                                   |
| 1986-87                                               | **丙組聯賽冠軍**，升級乙組                                                                                                                                                                  |
| 1990                                                  | 乙組聯賽排第廿二位，降級丙組                                                                                                                                                                   |
| 1992-93                                               | 英超成立，丙組重組為乙組〈III〉                                                                                                                                                                |
| 1997-98                                               | 決賽1-2負於[-{zh-hans:格林斯比镇; zh-hk:甘士比;}-](../Page/格林斯比镇足球俱乐部.md "wikilink")，聯賽錦標亞軍                                                                                                  |
| 1997                                                  | 由球迷成立的「Community Mutual」拯救球隊免於破產                                                                                                                                                 |
| 2002                                                  | 乙組〈III〉聯賽排第廿一位，降級丙組〈IV〉                                                                                                                                                          |
| 2002-03                                               | 丙組〈IV〉聯賽排第四位，附加賽準決賽兩回合累計3-1擊敗[-{zh-hans:伯利; zh-hk:貝利;}-](../Page/伯利足球俱乐部.md "wikilink")，[千禧球場決賽](../Page/千禧球場.md "wikilink")5-2擊敗[林肯城](../Page/林肯城足球俱乐部.md "wikilink")，升級乙組〈III〉 |
| 2004-05                                               | 乙組〈III〉聯賽更名英甲聯賽                                                                                                                                                                  |
| 2005                                                  | 以350萬鎊出售球場減債\[1\]                                                                                                                                                                |
| [2007-08](../Page/2007年至2008年英格蘭足球甲級聯賽.md "wikilink") | 宣佈債務重組，被扣除10分\[2\]。英甲聯賽排第廿一位，降級英乙聯賽 \[3\]                                                                                                                                        |
| [2008-09](../Page/2008年至2009年英格蘭足球乙級聯賽.md "wikilink") | 未能脫離債務重組，開季被扣減17分。成功保級排第廿一位結束球季                                                                                                                                                  |
| [2009-10](../Page/2009年至2010年英格蘭足球乙級聯賽.md "wikilink") | 英乙聯賽亞軍，升班英甲聯賽                                                                                                                                                                    |
| [2010-11](../Page/2010年至2011年英格蘭足球甲級聯賽.md "wikilink") | 英甲聯賽排第六位，附加賽準決賽兩回合累計4-4，[十二碼](../Page/互射十二碼.md "wikilink")2-4負於[哈德斯菲爾德](../Page/哈德斯菲尔德足球俱乐部.md "wikilink")                                                                       |
| [2012-13](../Page/2012年至2013年英格蘭足球甲級聯賽.md "wikilink") | 英甲聯賽亞軍，升班英冠聯賽                                                                                                                                                                    |
| [2014-15](../Page/2014年至2015年英格蘭足球冠軍聯賽.md "wikilink") | **英冠聯賽冠軍**，升班英超聯賽                                                                                                                                                                |

## 球會近況

  - 2017年6月30日，以破球會記錄的轉會費2,000萬鎊，從[車路士收購後衛](../Page/車路士足球會.md "wikilink")[-{zh-hans:內森·阿克;
    zh-hk:尼敦·亞基;}-](../Page/尼敦·亞基.md "wikilink")(Nathan Ake)\[4\]。

### 盃賽成績

| 圈數                                    | 日期          | 主／客 | 對賽球隊                                      | 紀錄                                                             |
| ------------------------------------- | ----------- | --- | ----------------------------------------- | -------------------------------------------------------------- |
| [聯　賽　盃](../Page/英格蘭聯賽盃.md "wikilink") |             |     |                                           |                                                                |
| 第二圈                                   | 2018年8月29日  | 主   | [米爾頓凱恩斯](../Page/米爾頓凱恩斯足球會.md "wikilink") | [勝3 - 0](https://www.bbc.com/sport/football/45254462)          |
| 第三圈                                   | 2018年9月26日  | 主   | [布力般流浪](../Page/布力般流浪足球會.md "wikilink")   | [勝3 - 2](https://www.bbc.com/sport/football/45555897)          |
|                                       |             |     |                                           |                                                                |
| 第四圈                                   | 2018年10月30日 | 主   | [諾域治](../Page/諾域治城足球會.md "wikilink")      | [勝2 - 1](https://www.bbc.com/sport/football/45947948)          |
|                                       |             |     |                                           |                                                                |
| 半準決賽                                  | 2018年12月19日 | 客   | [車路士](../Page/車路士足球會.md "wikilink")       | [負0 - 1](http://global.espn.com/football/report?gameId=528146) |
| [足　總　盃](../Page/英格蘭足總盃.md "wikilink") |             |     |                                           |                                                                |
| 第三圈                                   | 2019年1月5日   | 主   | [白禮頓](../Page/白禮頓足球會.md "wikilink")       | [負1 - 3](http://global.espn.com/football/match?gameId=529108)  |

## 主場球場

[King's_Park,_Fitness_First_Stadium_-_geograph.org.uk_-_644340.jpg](https://zh.wikipedia.org/wiki/File:King's_Park,_Fitness_First_Stadium_-_geograph.org.uk_-_644340.jpg "fig:King's_Park,_Fitness_First_Stadium_-_geograph.org.uk_-_644340.jpg")

**迪恩考特球場**（）是位於[英格蘭](../Page/英格蘭.md "wikilink")[西南部](../Page/西南英格蘭.md "wikilink")[多塞特郡](../Page/多塞特郡.md "wikilink")[度假小鎮](../Page/度假小鎮.md "wikilink")[伯恩茅斯的一座](../Page/伯恩茅斯.md "wikilink")[體育場](../Page/體育場.md "wikilink")，主要用於[足球說賽](../Page/足球.md "wikilink")，是伯恩茅斯足球俱乐部的主場球場。前稱「健身第一球场」（*Fitness
First Stadium*），於2011年將冠名權售予「舒沃汽車集團」（Seward Motor
Group）而更名「舒沃球場」（*Seward
Stadium*）\[5\]。隨著舒沃於2012年2月進行債務重組後，球場最終接受兩年交易命名為「金沙管理球場」（*Goldsands
stadium*）\[6\]。

球場三面看台均於2001年重建時一併建立，基本設計與高度相近，同為單層有蓋結構，兩側設有透明擋風膠板。主看台後方設有一列行政廂房及電子屏幕。主看台對面為「能源諮詢東看台」（Energy
Consulting East Stand），部分可供作客球迷入座。球門後方北看台以球會名宿命名為「史蒂夫·弗萊徹看台」（Steve
Fletcher Stand）。

[2012/13年球季般尼茅夫取得升班](../Page/2012年至2013年英格蘭足球甲級聯賽.md "wikilink")[英冠的資格](../Page/英冠.md "wikilink")，球場於休賽期間在原本空置的南方興建臨時看台，全坐席可容2,400名觀眾，設有頂蓋，但前方有兩條支柱阻礙視野，以另一員名宿命名「特德·麥克杜格爾看台」（Ted
MacDougall Stand）。球場四角設有照明燈塔。

於球隊在2015年升班[英超後](../Page/英超.md "wikilink")，是英超容量最少的球場。

## 球會榮譽

[AFC_Bournemouth.svg](https://zh.wikipedia.org/wiki/File:AFC_Bournemouth.svg "fig:AFC_Bournemouth.svg")

| <font color="white"> **榮譽**                       | <font color="white"> **次數** | <font color="white"> **年度**                              |
| ------------------------------------------------- | --------------------------- | -------------------------------------------------------- |
| **聯　賽　比　賽**                                       |                             |                                                          |
| **[英冠聯賽](../Page/英格蘭足球冠軍聯賽.md "wikilink") 冠軍**    | **1**                       | [2014/15年](../Page/2014年至2015年英格蘭足球冠軍聯賽.md "wikilink") 。 |
| **[丙組聯賽](../Page/英格兰足球联赛.md "wikilink") 冠軍**      | **1**                       | 1986/87年。                                                |
| **[甲級聯賽](../Page/英格蘭足球甲級聯赛.md "wikilink") 亞軍**    | **1**                       | [2012/13年](../Page/2012年至2013年英格蘭足球甲級聯賽.md "wikilink")。  |
| **[南區丙組聯賽](../Page/英格兰足球联赛.md "wikilink") 亞軍**    | **1**                       | 1947/48年。                                                |
| **[丙組〈IV〉附加賽](../Page/英格兰足球联赛.md "wikilink") 冠軍** | **1**                       | 2002/03年。                                                |
| **[乙級聯賽](../Page/英格蘭足球乙級聯賽.md "wikilink") 亞軍**    | **1**                       | [2009/10年](../Page/2009年至2010年英格蘭足球乙級聯賽.md "wikilink")。  |
| **[丁組聯賽](../Page/英格兰足球联赛.md "wikilink") 亞軍**      | **1**                       | 1970/71年。                                                |
| **杯　賽　比　賽**                                       |                             |                                                          |
| **[聯賽錦標](../Page/英格蘭聯賽錦標.md "wikilink") 冠軍**      | **1**                       | 1983/84年。                                                |
| **[聯賽錦標](../Page/英格蘭聯賽錦標.md "wikilink") 亞軍**      | **1**                       | 1997/98年。                                                |

## 球員名單（2018/19）

<small>更新日期：2018年9月16日 (UTC) 07:53
**粗體字**為新加盟球員；[Cruz_Roja.svg](https://zh.wikipedia.org/wiki/File:Cruz_Roja.svg "fig:Cruz_Roja.svg")為長期傷患球員。</small>

### 目前效力

<table style="width:223%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 17%" />
<col style="width: 66%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 11%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">前屬球會</p></th>
<th><p><font color="white">身價</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>門　將</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><a href="../Page/阿尔图尔·博鲁茨.md" title="wikilink">-{zh-hans:阿尔图尔·博鲁茨; zh-hk:保路克;}-</a>（Artur Boruc）</p></td>
<td><p>門將</p></td>
<td><p>1980.02.20</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32884147">2015</a></p></td>
<td><p><a href="../Page/南安普顿足球俱乐部.md" title="wikilink">修咸頓</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>27</strong></p></td>
<td></td>
<td><p><a href="../Page/阿斯米尔·贝戈维奇.md" title="wikilink">-{zh-hans:阿斯米尔·贝戈维奇;zh-hant:阿斯米·比高域}-</a>（Asmir Begovic）</p></td>
<td><p>門將</p></td>
<td><p>1987.06.20</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/football/40098727">2017</a></p></td>
<td><p><a href="../Page/切爾西足球俱乐部.md" title="wikilink">車路士</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="even">
<td><p><strong>42</strong></p></td>
<td></td>
<td><p><a href="../Page/麥克·崔佛.md" title="wikilink">麥克·崔佛</a>（Mark Travers）</p></td>
<td><p>門將</p></td>
<td><p>1999.05.18</p></td>
<td><p>2018</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>後　衛</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2</strong></p></td>
<td></td>
<td><p><a href="../Page/西蒙·弗朗西斯.md" title="wikilink">-{zh-hans:西蒙·弗朗西斯; zh-hk:施蒙·法蘭西斯;}-</a>（Simon Francis） <a href="https://zh.wikipedia.org/wiki/File:Captain_sports.svg" title="fig:Captain_sports.svg">Captain_sports.svg</a></p></td>
<td><p>右閘</p></td>
<td><p>1985.02.16</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/15617242">2012</a></p></td>
<td><p><a href="../Page/查尔顿竞技足球俱乐部.md" title="wikilink">查爾頓</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p><strong>3</strong></p></td>
<td></td>
<td><p>（Steve Cook）</p></td>
<td><p>中堅</p></td>
<td><p>1991.04.19</p></td>
<td><p><a href="http://news.bbc.co.uk/sport2/hi/football/16401727.stm">2012</a></p></td>
<td><p><a href="../Page/布莱顿足球俱乐部.md" title="wikilink">白禮頓</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="even">
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><a href="../Page/尼敦·亞基.md" title="wikilink">-{zh-hans:阿克;zh-hk:尼敦·亞基;zh-tw:阿克;}-</a>（Nathan Aké）'''</p></td>
<td><p>中堅</p></td>
<td><p>1995.02.18</p></td>
<td><p><a href="https://www.afcb.co.uk/news/first-team/nathan-ake-signs-for-club-record-fee">2017</a></p></td>
<td><p><a href="../Page/切爾西足球俱乐部.md" title="wikilink">車路士</a></p></td>
<td><p>2,000萬鎊</p></td>
</tr>
<tr class="odd">
<td><p><strong>11</strong></p></td>
<td></td>
<td><p><a href="../Page/查理·丹尼尔斯.md" title="wikilink">-{zh-hans:查理·丹尼尔斯; zh-hk:查理·丹尼斯;}-</a>（Charlie Daniels）</p></td>
<td><p>左閘</p></td>
<td><p>1986.09.07</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/15869601">2012</a></p></td>
<td><p><a href="../Page/莱顿东方足球俱乐部.md" title="wikilink">奧連特</a></p></td>
<td><p>20萬鎊</p></td>
</tr>
<tr class="even">
<td><p><strong>15</strong></p></td>
<td></td>
<td><p><a href="../Page/亞當·史密斯_(足球運動員).md" title="wikilink">亚当·史密斯</a>（Adam Smith）<a href="https://zh.wikipedia.org/wiki/File:Cruz_Roja.svg" title="fig:Cruz_Roja.svg">Cruz_Roja.svg</a></p></td>
<td><p>右閘</p></td>
<td><p>1991.04.29</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/25927508">2014</a></p></td>
<td><p><a href="../Page/托特纳姆热刺足球俱乐部.md" title="wikilink">热刺</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p>'''（Diego Rico）</p></td>
<td><p>左閘</p></td>
<td><p>1993.02.23</p></td>
<td><p><a href="https://www.bbc.com/sport/football/44931851">2018</a></p></td>
<td><p><a href="../Page/雷加利斯體育會.md" title="wikilink">雷加利斯</a></p></td>
<td><p>無透露<small><a href="../Page/#fn_1.md" title="wikilink"><sup>1</sup></a></small></p></td>
</tr>
<tr class="even">
<td><p>'''25</p></td>
<td></td>
<td><p>（Jack Simpson）</p></td>
<td><p>中堅</p></td>
<td><p>1997.01.08</p></td>
<td><p>2015</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>'''26</p></td>
<td></td>
<td><p><a href="../Page/泰朗·明斯.md" title="wikilink">泰朗·明斯</a>（Tyrone Mings）</p></td>
<td><p>中堅</p></td>
<td><p>1993.03.13</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/33293441">2015</a></p></td>
<td><p><a href="../Page/伊普斯维奇城足球俱乐部.md" title="wikilink">葉士域治</a></p></td>
<td><p>800萬鎊</p></td>
</tr>
<tr class="even">
<td><p>中　場</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>4</strong></p></td>
<td></td>
<td><p><a href="../Page/丹·哥斯林.md" title="wikilink">-{zh-hans:丹·哥斯林; zh-hk:高士寧;}-</a>（Dan Gosling）</p></td>
<td><p>中場</p></td>
<td><p>1990.02.02</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/27438606">2014</a></p></td>
<td><p><a href="../Page/纽卡斯尔联足球俱乐部.md" title="wikilink">-{zh-hans:纽卡斯尔联; zh-hk:紐卡素;}-</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><a href="../Page/安德鲁·苏尔曼.md" title="wikilink">-{zh-hans:安德鲁·苏尔曼; zh-hk:安祖·舒文;}-</a>（Andrew Surman）</p></td>
<td><p>中場</p></td>
<td><p>1986.08.20</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/29021180">2014</a></p></td>
<td><p><a href="../Page/诺维奇城足球俱乐部.md" title="wikilink">諾域治</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p><strong>7</strong></p></td>
<td></td>
<td><p><a href="../Page/马克·皮尤.md" title="wikilink">-{zh-hans:马克·皮尤; zh-hk:麥斯·佩尤;}-</a>（Marc Pugh）</p></td>
<td><p>中場</p></td>
<td><p>1987.04.02</p></td>
<td><p><a href="http://www.skysports.com/story/0,19528,11750_6192840,00.html">2010</a></p></td>
<td><p><a href="../Page/靴尔福特联足球俱乐部.md" title="wikilink">-{zh-hans:靴尔福特联; zh-hk:赫里福特聯;}-</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p><strong>8</strong></p></td>
<td></td>
<td><p>'''<a href="../Page/謝法臣·拿馬.md" title="wikilink">-{zh-hans:杰弗森·勒馬; zh-hk:謝法臣·拿馬; zh-tw:傑弗森·勒馬;}-</a>（Jefferson Lerma）</p></td>
<td><p>中場</p></td>
<td><p>1994.10.25</p></td>
<td><p><a href="https://www.bbc.com/sport/football/45072577">2018</a></p></td>
<td><p><a href="../Page/莱万特体育联盟.md" title="wikilink">利雲特</a></p></td>
<td><p>無透露 <small><a href="../Page/#fn_2.md" title="wikilink"><sup>2</sup></a></small></p></td>
</tr>
<tr class="odd">
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><a href="../Page/佐敦·伊比.md" title="wikilink">佐敦·伊比</a>（Jordon Ibe）</p></td>
<td><p>翼鋒</p></td>
<td><p>1995.12.08</p></td>
<td><p><a href="http://www.bbc.com/sport/football/36800117">2016</a></p></td>
<td><p><a href="../Page/利物浦足球會.md" title="wikilink">利物浦</a></p></td>
<td><p>1,500萬鎊</p></td>
</tr>
<tr class="even">
<td><p><strong>16</strong></p></td>
<td></td>
<td><p><a href="../Page/劉易斯·曲克.md" title="wikilink">-{zh-hans:刘易斯·库克; zh-hk:劉易斯·曲克;}-</a>（Lewis Cook）<a href="https://zh.wikipedia.org/wiki/File:Cruz_Roja.svg" title="fig:Cruz_Roja.svg">Cruz_Roja.svg</a></p></td>
<td><p>中場</p></td>
<td><p>1997.02.03</p></td>
<td><p><a href="http://m.bbc.com/sport/football/36750699">2016</a></p></td>
<td><p><a href="../Page/利茲聯足球俱樂部.md" title="wikilink">列斯聯</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p><strong>19</strong></p></td>
<td></td>
<td><p><a href="../Page/儒尼奥尔·斯塔尼斯拉斯.md" title="wikilink">-{zh-hans:儒尼奥尔·斯塔尼斯拉斯; zh-hk:史坦尼斯拿斯;}-</a>（Junior Stanislas）</p></td>
<td><p>中場</p></td>
<td><p>1989.11.26</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/28037282">2014</a></p></td>
<td><p><a href="../Page/伯恩利足球俱乐部.md" title="wikilink">般尼</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>'''20</p></td>
<td></td>
<td><p>'''（David Brooks）</p></td>
<td><p>中場</p></td>
<td><p>1997.07.08</p></td>
<td><p><a href="https://www.bbc.com/sport/football/44645199">2018</a></p></td>
<td><p><a href="../Page/錫菲聯足球會.md" title="wikilink">錫菲聯</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p><strong>22</strong></p></td>
<td></td>
<td><p><a href="../Page/埃默森·海恩德曼.md" title="wikilink">-{zh-hans:埃默森·海恩德曼; zh-hk:埃默森·海恩德曼;}-</a>（Emerson Hyndman）</p></td>
<td><p>中場</p></td>
<td><p>1996.04.09</p></td>
<td><p><a href="https://www.bbc.com/sport/football/36560218">2016</a></p></td>
<td><p><a href="../Page/富咸足球會.md" title="wikilink">富咸</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="even">
<td><p>'''24</p></td>
<td></td>
<td><p><a href="../Page/瑞安·弗雷澤.md" title="wikilink">-{zh-hans:瑞安·弗雷泽; zh-hk:賴恩·菲沙;}-</a>（Ryan Fraser）</p></td>
<td><p>翼鋒</p></td>
<td><p>1994.02.24</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/21079599">2013</a></p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">鴨巴甸</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p>'''28</p></td>
<td></td>
<td><p><a href="../Page/凱爾·泰勒.md" title="wikilink">凱爾·泰勒</a>（Kyle Taylor）</p></td>
<td><p>中場</p></td>
<td><p>1999.08.28</p></td>
<td><p>2017</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p>'''36*</p></td>
<td></td>
<td><p>（Matt Butcher）</p></td>
<td><p>中場</p></td>
<td><p>1997.07.14</p></td>
<td><p>2015</p></td>
<td><p>青年軍</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>前　鋒</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>'''9</p></td>
<td></td>
<td><p>（Lys Mousset）</p></td>
<td><p>前鋒</p></td>
<td><p>1996.02.08</p></td>
<td><p><a href="http://m.bbc.com/sport/football/36674367">2016</a></p></td>
<td><p><a href="../Page/勒哈費爾體育會.md" title="wikilink">勒哈費爾</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="odd">
<td><p><strong>13</strong></p></td>
<td></td>
<td><p><a href="../Page/卡勒姆·威尔逊.md" title="wikilink">-{zh-hans:卡勒姆·威尔逊; zh-hk:哥林·威爾遜;}-</a>（Callum Wilson）</p></td>
<td><p>前鋒</p></td>
<td><p>1992.02.27</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/28172358">2014</a></p></td>
<td><p><a href="../Page/考文垂足球俱乐部.md" title="wikilink">-{zh-hans:考文垂; zh-hk:高雲地利;}-</a></p></td>
<td><p>無透露</p></td>
</tr>
<tr class="even">
<td><p><strong>17</strong></p></td>
<td></td>
<td><p>（Joshua King）</p></td>
<td><p>前鋒</p></td>
<td><p>1992.01.15</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32918818">2015</a></p></td>
<td><p><a href="../Page/布莱克本流浪者足球俱乐部.md" title="wikilink">布力般流浪</a></p></td>
<td><p>自由轉會</p></td>
</tr>
<tr class="odd">
<td><p><strong>29</strong></p></td>
<td></td>
<td><p>'''<a href="../Page/多米尼克·索蘭克.md" title="wikilink">-{zh-hans:多米尼克·索兰克; zh-hk:多明尼·蘇蘭基;}-</a>（Dominic Solanke）</p></td>
<td><p>前鋒</p></td>
<td><p>1997.09.14</p></td>
<td><p><a href="https://www.bbc.com/sport/football/46763610">2019</a></p></td>
<td><p><a href="../Page/利物浦足球會.md" title="wikilink">利物浦</a></p></td>
<td><p>無透露<small><a href="../Page/#fn_3.md" title="wikilink"><sup>3</sup></a></small></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<small> (\*) 發展隊成員
據報約1,070萬鎊。
據報為2,500萬鎊。
據報為1,900萬鎊。
</small>

### 借用球員

<table style="width:204%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 17%" />
<col style="width: 47%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 11%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">借用球會</p></th>
<th><p><font color="white">借用年期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>23</strong></p></td>
<td></td>
<td><p>'''<a href="../Page/尼達利·佳尼.md" title="wikilink">-{zh-hans:纳塔涅尔·克莱因; zh-hk:尼達利·基尼;}-</a>（Nathaniel Clyne）</p></td>
<td><p>右閘</p></td>
<td><p>1991.04.05</p></td>
<td><p>2019</p></td>
<td><p><a href="../Page/利物浦足球會.md" title="wikilink">利物浦</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/46760201">18/19季末</a></p></td>
</tr>
</tbody>
</table>

### 外借球員

<table style="width:222%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 17%" />
<col style="width: 66%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">外借球會</p></th>
<th><p><font color="white">外借年期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>8</strong></p></td>
<td></td>
<td><p><a href="../Page/哈里·阿特.md" title="wikilink">-{zh-hans:哈里·奥特; zh-hk:哈利·艾達;}-</a>（Harry Arter）</p></td>
<td><p>中場</p></td>
<td><p>1989.12.28</p></td>
<td><p><a href="http://news.bbc.co.uk/sport2/hi/football/teams/b/bournemouth/8708769.stm">2010</a></p></td>
<td><p><a href="../Page/卡迪夫城足球會.md" title="wikilink">卡迪夫城</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/45126885">18/19球季</a></p></td>
</tr>
<tr class="even">
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><a href="../Page/阿隆·拉姆斯代爾.md" title="wikilink">-{zh-hans:阿隆·拉姆斯代尔; zh-hk:艾朗·蘭斯達爾;}-</a>（Aaron Ramsdale）</p></td>
<td><p>門將</p></td>
<td><p>1998.05.14</p></td>
<td><p><a href="https://www.afcb.co.uk/news/first-team/afc-bournemouth-sign-young-keeper-aaron-ramsdale-from-sheffield-united">2017</a></p></td>
<td><p><a href="../Page/AFC溫布頓.md" title="wikilink">AFC溫布頓</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/46761549">18/19季末</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><a href="../Page/布拉德·史密斯.md" title="wikilink">布拉德·史密斯</a>（Bradley Smith）</p></td>
<td><p>左閘</p></td>
<td><p>1994.04.09</p></td>
<td><p><a href="https://www.bbc.com/sport/football/36906382">2016</a></p></td>
<td><p><a href="../Page/西雅圖海灣者足球俱樂部.md" title="wikilink">西雅圖海灣者</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/45120794">18/19球季</a></p></td>
</tr>
<tr class="even">
<td><p><strong>18</strong></p></td>
<td></td>
<td><p><a href="../Page/謝美·迪科爾.md" title="wikilink">-{zh-hans:杰梅恩·迪福; zh-hk:謝美·迪科爾;}-</a>（Jermain Defoe）'''</p></td>
<td><p>前鋒</p></td>
<td><p>1982.10.07</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/football/40051031">2017</a></p></td>
<td><p><a href="../Page/格拉斯哥流浪足球會.md" title="wikilink">格拉斯哥流浪</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/46775600">18個月</a></p></td>
</tr>
<tr class="odd">
<td><p>'''23*</p></td>
<td></td>
<td><p><a href="../Page/康納·馬奥尼.md" title="wikilink">-{zh-hans:康纳·马奥尼; zh-hk:康納·馬奧尼;}-</a>（Connor Mahoney）</p></td>
<td><p>翼鋒</p></td>
<td><p>1997.02.12</p></td>
<td><p><a href="http://www.bbc.com/sport/football/40495709">2017</a></p></td>
<td><p><a href="../Page/伯明翰足球會.md" title="wikilink">伯明翰</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/45098389">18/19球季</a></p></td>
</tr>
<tr class="even">
<td><p><strong>43*</strong></p></td>
<td></td>
<td><p>（Matt Worthington）</p></td>
<td><p>中場</p></td>
<td><p>1997.12.18</p></td>
<td><p>2016</p></td>
<td><p><a href="../Page/格林森林流浪足球會.md" title="wikilink">格連森林</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/45373661">18/19季初</a></p></td>
</tr>
</tbody>
</table>

### 離隊球員

<table style="width:223%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 17%" />
<col style="width: 66%" />
<col style="width: 9%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 11%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">加盟球會</p></th>
<th><p><font color="white">身價</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2018年夏季轉會窗</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>'''9</p></td>
<td></td>
<td><p><a href="../Page/本尼克·阿弗比.md" title="wikilink">-{zh-hans:本尼克·阿弗比; zh-hk:阿科比;}-</a>（Benik Afobe）</p></td>
<td><p>前鋒</p></td>
<td><p>1993.02.12</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/35275889">2016</a></p></td>
<td><p><a href="../Page/狼隊足球會.md" title="wikilink">狼隊</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/44329132">1,000萬鎊</a></p></td>
</tr>
<tr class="odd">
<td><p>'''10</p></td>
<td></td>
<td><p><a href="../Page/麥斯·格拉度.md" title="wikilink">-{zh-hk:阿隆·拉姆斯代尔; zh-hans:麦斯·格拉德尔; zh-hk:麥斯·格拉度;}-</a>（Max Gradel）</p></td>
<td><p>翼鋒</p></td>
<td><p>1987.11.30</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/33762488">2015</a></p></td>
<td><p><a href="../Page/圖盧茲足球會.md" title="wikilink">圖盧茲</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/44606392">無透露</a></p></td>
</tr>
<tr class="even">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><a href="../Page/萊安·阿爾索普.md" title="wikilink">瑞安·奥尔索普</a>（Ryan Allsop）</p></td>
<td><p>門將</p></td>
<td><p>1992.06.17</p></td>
<td><p><a href="http://www.bbc.co.uk/sport/0/football/21079599">2013</a></p></td>
<td><p><a href="../Page/韋康比流浪者足球俱樂部.md" title="wikilink">韋甘比</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/44534338">季後放棄</a></p></td>
</tr>
<tr class="odd">
<td><p>'''28</p></td>
<td></td>
<td><p>（Lewis Grabban）</p></td>
<td><p>前鋒</p></td>
<td><p>1988.01.12</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/35287391">2016</a></p></td>
<td><p><a href="../Page/諾定咸森林足球會.md" title="wikilink">諾定咸森林</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/44745167">無透露</a></p></td>
</tr>
<tr class="even">
<td><p>'''29</p></td>
<td></td>
<td><p><a href="../Page/羅伊斯·威金斯.md" title="wikilink">罗伊斯·威金斯</a>（Rhoys Wiggins）</p></td>
<td><p>左閘</p></td>
<td><p>1987.11.04</p></td>
<td><p><a href="http://www.bbc.com/sport/football/35465127">2016</a></p></td>
<td><p>--</p></td>
<td><p><a href="https://www.bbc.com/sport/football/44252065">退役</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>32</strong></p></td>
<td></td>
<td><p><a href="../Page/亚当·费德里奇.md" title="wikilink">-{zh-hans:亚当·费德里奇;zh-hant:費達歷斯}-</a>（Adam Federici）</p></td>
<td><p>門將</p></td>
<td><p>1985.01.31</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/32900802">2015</a></p></td>
<td><p><a href="../Page/史篤城足球會.md" title="wikilink">史篤城</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/44704487">無透露</a></p></td>
</tr>
<tr class="even">
<td><p><strong>38</strong></p></td>
<td></td>
<td><p><a href="../Page/貝利·卡基爾.md" title="wikilink">-{zh-hans:贝利·卡吉尔; zh-hk:庇利·卡基爾;}-</a>（Baily Cargill）</p></td>
<td><p>中堅</p></td>
<td><p>1995.07.05</p></td>
<td><p>2012</p></td>
<td><p><a href="../Page/米爾頓凱恩斯足球會.md" title="wikilink">米爾頓凱恩斯</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/45037502">季後放棄</a></p></td>
</tr>
<tr class="odd">
<td><p>'''--*</p></td>
<td></td>
<td><p>（Ben Whitfield）</p></td>
<td><p>中場</p></td>
<td><p>1996.02.28</p></td>
<td><p><a href="http://www.bbc.com/sport/0/football/25983604">2014</a></p></td>
<td><p><a href="../Page/維爾港足球會.md" title="wikilink">維爾港</a></p></td>
<td><p><a href="https://www.bbc.com/sport/football/45359000">季後放棄</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 著名球星

  - （Darren Anderton）

  - [-{zh-hant:佐治·貝斯;
    zh-cn:乔治·贝斯特;}-](../Page/佐治·貝斯.md "wikilink")（George
    Best）

  - （Matt Holland）

  - （Gavin Peacock）

  - [-{zh-hans:布雷特·彼特曼;zh-hk:畢列·比特文;}-](../Page/布雷特·彼特曼.md "wikilink")（Brett
    Pitman）

  - [-{zh-hant:列納;
    zh-cn:杰米·雷德克纳普;}-](../Page/杰米·雷德克纳普.md "wikilink")（Jamie
    Redknapp）

  - [-{zh-hant:列納;
    zh-cn:哈里·雷德克納普;}-](../Page/哈里·雷德克納普.md "wikilink")（Harry
    Redknapp）

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [-{zh-hant:般尼茅夫; zh-cn:伯恩茅斯;}-官方網站](http://www.afcb.co.uk)

[Category:英格蘭足球俱樂部](../Category/英格蘭足球俱樂部.md "wikilink")
[伯恩茅斯足球俱乐部](../Category/伯恩茅斯足球俱乐部.md "wikilink")
[Category:1899年建立的足球俱樂部](../Category/1899年建立的足球俱樂部.md "wikilink")
[Category:1899年英格蘭建立](../Category/1899年英格蘭建立.md "wikilink")

1.  [Club completes stadium
    sell-off](http://news.bbc.co.uk/2/hi/uk_news/england/dorset/4525218.stm)
2.  [Bournemouth enter
    administration](http://news.bbc.co.uk/sport2/hi/football/teams/b/bournemouth/7233678.stm)
3.  [Battling Bournemouth
    relegated](http://news.bbc.co.uk/sport2/hi/football/eng_div_2/7368656.stm)
4.
5.
6.