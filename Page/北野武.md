[Takeshi_Kitano_Leone_D'oro.jpg](https://zh.wikipedia.org/wiki/File:Takeshi_Kitano_Leone_D'oro.jpg "fig:Takeshi_Kitano_Leone_D'oro.jpg")

**北野武**（；）是[日本知名](../Page/日本.md "wikilink")[導演](../Page/導演.md "wikilink")，[東京都](../Page/東京都.md "wikilink")[足立區](../Page/足立區.md "wikilink")出身。其身兼[電影導演](../Page/電影導演.md "wikilink")、[演員](../Page/演員.md "wikilink")、[搞笑藝人](../Page/搞笑藝人.md "wikilink")、[电视节目](../Page/电视节目.md "wikilink")[主持人等身分](../Page/主持人.md "wikilink")，除了電影導演的工作外，大多使用艺名**彼得武**（；），亦以搞笑二人組「」一員的名義在其他舞台活動。其與[塔摩利](../Page/塔摩利.md "wikilink")、[明石家秋刀魚並稱為日本搞笑藝人](../Page/明石家秋刀魚.md "wikilink")。

## 生涯

北野武是當油漆師傅的父親北野菊次郎（電影《[菊次郎的夏天](../Page/菊次郎的夏天.md "wikilink")》與其父有關）與母親名北野佐紀的四男（一位兄長幼年夭折，故為實際長大的三男）。長兄北野重一，姐中山安子，次兄曾任[明治大学理工学部應用化学科教授](../Page/明治大学.md "wikilink")。

小學就讀足立区立梅島第一小学校，由於母親對教育非常重視，成績相當優秀，尤其是算数、図画課。小学校畢業後，母親希望他進入好学校，因而越區就讀。中学卒業後，進入。

1965年高校畢業後，接受母親的建議，北野武就读[明治大学工学部](../Page/明治大学.md "wikilink")（現為理工學部）機械工学科。由於熱心參與學生運動及文化活動，並兼職多項工作及參加戲劇表演，雖然已經拿到140個畢業學分當中的106個，仍然因太少到校而中途退学。不过2004年9月，明治大学頒發「特别毕业认定証」及「特別功労賞」給他，以表彰其貢獻。

1983年，北野武在[大島渚作品](../Page/大島渚.md "wikilink")《[戰場上的快樂聖誕](../Page/戰場上的快樂聖誕.md "wikilink")》飾演[二次大戰時](../Page/二次大戰.md "wikilink")[日軍戰俘營的軍官](../Page/日軍.md "wikilink")，獲得了國際知名。

北野武导演的[电影](../Page/电影.md "wikilink")《[花火](../Page/花火_\(日本电影\).md "wikilink")》，於1997年获得第54届[威尼斯影展](../Page/威尼斯影展.md "wikilink")[金狮奖](../Page/金狮奖.md "wikilink")。《[座头市](../Page/座头市.md "wikilink")》2003年获得第60届威尼斯影展银狮奖（导演奖）。作为电影导演在世界范围内得到了较高的评价。特别是在[欧洲](../Page/欧洲.md "wikilink")，有一些忠实的崇拜者亲切地称呼北野武为“Kitanist”。

2005年4月就任[东京艺术大学研究生院](../Page/东京艺术大学.md "wikilink")[教授电影专业主任](../Page/教授.md "wikilink")。其後北野武再度投入電影拍攝工作，並起用全新演員班底，新片《[極惡非道](../Page/極惡非道.md "wikilink")》於2010年[康城影展首映](../Page/康城影展.md "wikilink")\[1\]。

## 导演作品

  - [凶暴的男人](../Page/凶暴的男人.md "wikilink")（1989年）

  - [3-4×10月](../Page/3-4×10月.md "wikilink")（1990年）

  - [那年夏天，宁静的海](../Page/那年夏天，宁静的海.md "wikilink")（1991年）

  - [奏鳴曲](../Page/奏鳴曲_\(電影\).md "wikilink")（1993年）

  - [-{zh-cn:性爱狂想曲; zh-hk:大家都在幹什麼?;
    zh-tw:北野武的性愛狂想曲;}-](../Page/性爱狂想曲.md "wikilink")（1995年）

  - [-{zh-cn:坏孩子的天空; zh-hk:壞孩子的天空;
    }-](../Page/坏孩子的天空.md "wikilink")（1996年）

  - [花火](../Page/花火_\(日本电影\).md "wikilink") （1997年）

  - [菊次郎之夏](../Page/菊次郎之夏.md "wikilink")（1999年）

  - （2000年）

  - [淨琉璃](../Page/淨琉璃_\(電影\).md "wikilink")（2002年）

  - [盲俠座頭市](../Page/盲俠座頭市.md "wikilink")（2003年）

  - [Takeshis'](../Page/Takeshis'.md "wikilink")（2005年）

  - [导演万岁\!](../Page/导演万岁!.md "wikilink") (2007年)

  - [阿基里斯與龜](../Page/阿基里斯與烏龜_\(電影\).md "wikilink")（2008年）

  - [極惡非道](../Page/極惡非道.md "wikilink") （2010年）

  - [極惡非道2](../Page/極惡非道2.md "wikilink")（2012年）

  - （2015年）

  - [極惡非道最終章](../Page/極惡非道最終章.md "wikilink")（2017年）

## 演出作品

下列電影除了《戰場上的快樂聖誕》、《Gonin》、《御法度》、《大逃殺》、《血與骨》、《只為了你》、《紅鳉魚》、《攻殼機動隊》、《破狱》外，都是北野武自己导演與演出的電影（都用**彼得武**這個藝名）

  - [戰場上的快樂聖誕](../Page/戰場上的快樂聖誕.md "wikilink")（*Merry Christmas, Mr.
    Lawrence*）（1983年，導演：[大島渚](../Page/大島渚.md "wikilink")）飾演頗能善待英軍俘虜之日軍原[中士](../Page/中士.md "wikilink")（成名作）

  - [夜叉](../Page/夜叉.md "wikilink")（1985年）

  - [凶暴的男人](../Page/凶暴的男人.md "wikilink")（1989年）

  - [3-4×10月](../Page/3-4×10月.md "wikilink")（1990年）

  - [奏鳴曲](../Page/奏鳴曲_\(電影\).md "wikilink")（1993年）

  - [性爱狂想曲](../Page/性爱狂想曲.md "wikilink")（1995年）

  - Gonin（1995年）

  - [花火](../Page/花火.md "wikilink")（1997年）飾演向黑幫借債之問題刑警

  - [菊次郎之夏](../Page/菊次郎之夏.md "wikilink")（1999年）

  - [御法度](../Page/御法度.md "wikilink")（1999年，導演：[大島渚](../Page/大島渚.md "wikilink")）

  - [大逃杀](../Page/大逃杀.md "wikilink")（2000年，導演：[深作欣二](../Page/深作欣二.md "wikilink")）

  - （2000年）

  - [盲俠座頭市](../Page/盲俠座頭市.md "wikilink")（2003年）

  - [血与骨](../Page/血与骨.md "wikilink")（2004年，導演：[崔洋一](../Page/崔洋一.md "wikilink")）

  - [Takeshis'](../Page/Takeshis'.md "wikilink")（2005年）

  - [导演万岁\!](../Page/导演万岁!.md "wikilink")（2007年）

  - [阿基里斯與龜](../Page/阿基里斯與烏龜_\(電影\).md "wikilink")（2008年）

  - [只為了你](../Page/只為了你.md "wikilink")（2012年）

  - [紅鳉魚](https://ja.wikipedia.org/wiki/%E8%B5%A4%E3%82%81%E3%81%A0%E3%81%8B#.E3.83.86.E3.83.AC.E3.83.93.E3.83.89.E3.83.A9.E3.83.9E)（2015年）TBS電視劇
    飾演 立川談志

  - [人生的約定](../Page/人生的約定.md "wikilink")(人生の約束)(2016年) 飾演 岩濑厚一郎

  - [Doctor-X～外科醫·大門未知子～-SP](../Page/Doctor-X～外科醫·大門未知子～-SP.md "wikilink")（2016年）朝日電視劇
    飾演 黒須貫太郎

  - [女人沉睡時](../Page/女人沉睡時.md "wikilink")（2016年，導演：[王穎](../Page/王穎.md "wikilink")）

  - [人中之龍6](../Page/人中之龍6.md "wikilink")（2016年）SEGA遊戲 飾演 **廣瀨徹**

  - [攻殼機動隊](../Page/攻殼機動隊_\(2017年電影\).md "wikilink")（2017年）

  - [破狱](../Page/破狱.md "wikilink")（2017年）主演 **浦田进**

  - [韋駄天～東京奧運故事～](../Page/韋駄天～東京奧運故事～.md "wikilink")（2019年）[NHK大河劇](../Page/NHK大河劇.md "wikilink")
    飾演 **古今亭志生** (兼任旁白)

## 主持電視節目

  - [日本放送](../Page/日本放送.md "wikilink")《[北野武的深夜日本](../Page/北野武的深夜日本.md "wikilink")》(1981年1月1日～1990年12月27日)
  - [朝日電視台](../Page/朝日電視台.md "wikilink")《[北野武的日本運動大將](../Page/北野武的日本運動大將.md "wikilink")》(1985年4月\~1990年2月)
  - [東京放送](../Page/東京放送.md "wikilink")《[百戰百勝王](../Page/百戰百勝王.md "wikilink")》(1986年\~1989年)
  - 東京放送《[總天然色綜藝
    北野電視台](../Page/總天然色綜藝_北野電視台.md "wikilink")》(1989年4月21日\~1989年9月15日)
  - [富士電視台](../Page/富士電視台.md "wikilink")《[平成教育委員會](../Page/平成教育委員會.md "wikilink")》(1991年\~1997年)
  - 朝日電視台《[北野武的TV擒抱](../Page/北野武的TV擒抱.md "wikilink")》(1991年4月1日\~)
  - 富士電視台《[北野Fan
    Club](../Page/北野Fan_Club.md "wikilink")》(1991年4月12日\~1996年3月22日)
  - [日本電視台](../Page/日本電視台.md "wikilink")《[世界超偉人傳說](../Page/世界超偉人傳說.md "wikilink")》(1992年\~1998年)
  - [朝日放送](../Page/朝日放送.md "wikilink")《[北野武的萬物創世紀](../Page/北野武的萬物創世紀.md "wikilink")》（1995年10月17日\~2001年3月20日）
  - 富士電視台《[北野富士](../Page/北野富士.md "wikilink")》(1996年4月\~1997年9月)
  - [JFN](../Page/JFN.md "wikilink")《北野武的[收音機黃金時代](../Page/收音機黃金時代.md "wikilink")》（1999年4月〜2000年9月）
  - 朝日放送《[最終警告！恐怖的家庭醫學](../Page/最終警告！恐怖的家庭醫學.md "wikilink")》（2004年\~2009年）
  - 日本電視台《[北野武日本教育白皮書](../Page/北野武日本教育白皮書.md "wikilink")》(2005年11月12日\~)
  - [NRN](../Page/NRN.md "wikilink")《北野武的文學夜話》
  - 朝日放送《[全民家庭醫學](../Page/全民家庭醫學.md "wikilink")》（2010年\~）

## 作品特色

  - 一连串快速的暴力画面。
  - 喜欢拍海边的镜头。
  - 喜欢对于人物拍停止式的镜头。

## 軼事

  - 是他在96年邀请当时的模特[安藤政信参加了](../Page/安藤政信.md "wikilink")《[坏孩子的天空](../Page/坏孩子的天空.md "wikilink")》的拍摄，讓初出道的安藤奪得該年度許多電影新人獎。
  - 1994年，[摩托车车祸导致他在医院里住了一个多月](../Page/摩托车.md "wikilink")，由于严重的伤势进行了一次全面的面部手术，但手术後右邊的臉就麻痺了。嚴重受傷的原因是駕駛時沒戴上[安全帽所致](../Page/安全帽.md "wikilink")，事後他說這是一樁「不成功的自殺」。
  - 曾经做过拳擊手和踢踏舞者，前者在《[坏孩子的天空](../Page/坏孩子的天空.md "wikilink")》中有所影射，後者在《[菊次郎之夏](../Page/菊次郎之夏.md "wikilink")》中有所影射。
  - 北野武也主辦專門給[AV女優的獎項](../Page/AV女優.md "wikilink")，並在頒獎典禮上對得獎AV女優襲胸。
  - 北野武曾於1986年因[講談社報導他與女學生偷情](../Page/講談社.md "wikilink")，及後帶同其弟子大鬧該社的編輯部。
  - 在2017年12月29日TBS電視台播放的「ニュースキャスター　超豪華！芸能ニュースランキング２０１７決定版」中，北野武透露了最高月收入曾超過1億日元\[2\]。
  - 北野武是有名的民族主義人士。他堅持釣魚台列島是日本領土的主張。

## 參考

<references/>

## 外部链接

  -
  - [Great Directors: Takeshi
    Kitano](http://www.sensesofcinema.com/2003/great-directors/kitano/)
    at senses of cinema

  - [北野武:官方網站](https://web.archive.org/web/20181007033131/http://www.kitanotakeshi.com/)

  - [Exposure Online Magazine™: Takeshi
    Kitano](http://www.fullspectrumottawa.com/directors/takeshi_kitano.php)

  - [TV Stars: "Beat" Takeshi
    profile](http://www.japan-zone.com/modern/kitano_takeshi.shtml) at
    JapanZone

  - [*A Comedian Star is Born*
    essay](https://web.archive.org/web/20081013063408/http://japattack.com/japattack/film/beattakeshi_1.html)
    at Japattack

  - [MXC
    Castle](https://web.archive.org/web/20110917181158/http://www.mxccastle.com/)
    - Fansite

  - [北野武與王家衛：觀看事件的兩種方法](http://kailinyang.info/image-TraditionalChinese.htm)
    引自 [楊凱麟論文平台](http://kailinyang.info/index.htm)

  - [Office Kitano](http://www.office-kitano.co.jp/) - Kitano's
    Production Company

  - [北野武 とは](http://www.weblio.jp/content/%E5%8C%97%E9%87%8E%E6%AD%A6)

[分類:明治大學校友](../Page/分類:明治大學校友.md "wikilink")

[Category:日本導演](../Category/日本導演.md "wikilink")
[Category:日本男性搞笑藝人](../Category/日本男性搞笑藝人.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:日本电视主持人](../Category/日本电视主持人.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:藍絲帶獎最佳導演得主](../Category/藍絲帶獎最佳導演得主.md "wikilink")
[Category:每日電影獎最佳男主角得主](../Category/每日電影獎最佳男主角得主.md "wikilink")
[Category:每日電影獎最佳男配角得主](../Category/每日電影獎最佳男配角得主.md "wikilink")
[Category:藍絲帶獎最佳男主角得主](../Category/藍絲帶獎最佳男主角得主.md "wikilink")
[Category:藍絲帶獎最佳男配角得主](../Category/藍絲帶獎最佳男配角得主.md "wikilink")
[Category:電影旬報十佳獎最佳男主角得主](../Category/電影旬報十佳獎最佳男主角得主.md "wikilink")
[Category:電影旬報十佳獎最佳男配角得主](../Category/電影旬報十佳獎最佳男配角得主.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:亞洲電影大獎最佳導演得主](../Category/亞洲電影大獎最佳導演得主.md "wikilink")
[Category:報知電影獎最佳導演得主](../Category/報知電影獎最佳導演得主.md "wikilink")
[Category:日刊體育電影大獎最佳導演得主](../Category/日刊體育電影大獎最佳導演得主.md "wikilink")
[Category:日刊體育電影大獎最佳男主角得主](../Category/日刊體育電影大獎最佳男主角得主.md "wikilink")
[Category:日刊體育電影大獎新人獎得主](../Category/日刊體育電影大獎新人獎得主.md "wikilink")

1.  [北野事務所官方消息（日文）](http://www.office-kitano.co.jp/15_takeshi-kitano.html)

2.