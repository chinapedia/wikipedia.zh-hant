**霍震寰**（Fok Chun-wan,
Ian，），籍贯[广东](../Page/广东.md "wikilink")[番禺](../Page/番禺.md "wikilink")[南沙](../Page/南沙區.md "wikilink")，生于[英屬香港](../Page/英屬香港.md "wikilink")，是[霍英东博士之次子](../Page/霍英东.md "wikilink")。已婚，其妻為息影女演員[陳琪琪](../Page/陳琪琪.md "wikilink")，育有1女及2子，早年在香港、[英国及](../Page/英国.md "wikilink")[加拿大接受教育](../Page/加拿大.md "wikilink")。霍震寰主要职务為[港區全國人大代表](../Page/港區全國人大代表.md "wikilink")、[香港中華總商會會長](../Page/香港中華總商會.md "wikilink")、[有荣有限公司董事总经理](../Page/有荣有限公司.md "wikilink")。

## 其它职务

  - [霍英东基金有限公司董事](../Page/霍英东基金有限公司.md "wikilink")
  - [中国人民政治协商会议广东省第](../Page/中国人民政治协商会议.md "wikilink")9届常务委员会委员
  - [中华全国工商业联合会副主席](../Page/中华全国工商业联合会.md "wikilink")
  - [中华海外联谊会副會長](../Page/中华海外联谊会.md "wikilink")
  - [香港中華總商會會長](../Page/香港中華總商會.md "wikilink")
  - [香港培华教育基金会常务委员会主席](../Page/香港培华教育基金会.md "wikilink")
  - [香港中文大学](../Page/香港中文大学.md "wikilink")[联合书院校董会校董](../Page/联合书院.md "wikilink")
  - [香港科技大学大学顾问委员会委员](../Page/香港科技大学.md "wikilink")
  - [香港公开大学校董会委员](../Page/香港公开大学.md "wikilink")
  - [香港管理专业协会理事会会员及执行委员会委员](../Page/香港管理专业协会.md "wikilink")
  - [香港武术联会会长](../Page/香港武术联会.md "wikilink")
  - [国际武术联合会执行委员会委员](../Page/国际武术联合会.md "wikilink")
  - [博鳌亚洲论坛理事会理事](../Page/博鳌亚洲论坛理事会.md "wikilink")
  - [推選委員會委員](../Page/推選委員會.md "wikilink")
  - [選舉委員會委員](../Page/選舉委員會.md "wikilink")

## 教育背景

霍震寰也有少時海外求學的經歷。

他在香港[摩利臣山小學和](../Page/摩利臣山小學.md "wikilink")[聖貞德學校念書](../Page/聖貞德學校.md "wikilink")，後到英國完成中學課程。

在英國念完中學的霍震寰，很順利地考上了加拿大[英屬哥倫比亞大學](../Page/英屬哥倫比亞大學.md "wikilink")。霍回憶道，當時，霍英東覺得兒子已經念完學士了，所學的東西也足夠經商，往後需要的是在實踐中學。因此，希望霍震寰回來幫助打理家族生意。然而，霍震寰自己並不想放棄繼續求學的機會。經過努力，他繼續了學業。

兩年後，他學成回港，成了會計方面的專家。如今，霍震寰已是[霍英東集團董事兼總經理](../Page/霍英東集團.md "wikilink")，被譽為霍氏的“家族財政部長”。

## 武林高手

學業有成的霍震寰並不是一介書生，不為人知的是，他還練有一身好武藝。

在英國讀中學時，就開始學[柔道](../Page/柔道.md "wikilink")、[空手道](../Page/空手道.md "wikilink")，上大學后轉而練[太極拳](../Page/太極拳.md "wikilink")，並開始迷上中國拳術。霍震寰回港后，拜[意拳大師](../Page/意拳.md "wikilink")[韓星垣為師](../Page/韓星垣.md "wikilink")，學習內家拳“意拳”，並向[北京意拳名家](../Page/北京.md "wikilink")[姚宗勛求教](../Page/姚宗勛.md "wikilink")，一練就是20多年，在意拳上達到一定造詣，成為香港意拳學會會長。

出於對武術的熱愛，霍震寰還曾親自率領香港武術隊，赴[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡](../Page/吉隆坡.md "wikilink")，參加有52個國家和地區，600多名武林高手參加的、規模盛大的第二屆[世界武術錦標賽](../Page/世界武術錦標賽.md "wikilink")，並獲得兩面金牌、三面銀牌、四面銅牌。

雖然醉心武學，但霍震寰本身並不是好勇斗狠之人。他說，自己習武主要是為傳播中華武藝。

## 榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（2003年）
  - [銀紫荊星章](../Page/銀紫荊星章.md "wikilink")（2005年）

## 相关条目

  - [霍英東家族](../Page/霍英東家族.md "wikilink")

## 參考資料

李飛: 霍家次子霍震寰：精通武術的“家族財政部長”，《市場報》 ( 2006-11-17 第06版
)，參見：[中國人民網](http://finance.people.com.cn/BIG5/42774/5052588.html)

2010年3月11日 人民代表財產公示：霍震寰----人大代表------香港中华总商会会长---没房，没车。

[Category:番禺人](../Category/番禺人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:不列顛哥倫比亞大學校友](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[Category:霍英東家族](../Category/霍英東家族.md "wikilink")
[Category:第十一屆港區全國人大代表](../Category/第十一屆港區全國人大代表.md "wikilink")
[Category:第十二屆港區全國人大代表](../Category/第十二屆港區全國人大代表.md "wikilink")
[Category:第十三屆港區全國人大代表](../Category/第十三屆港區全國人大代表.md "wikilink")
[C](../Category/霍姓.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")