**大草原城**
（）是[美國](../Page/美國.md "wikilink")[德克薩斯州東北部的一個城市](../Page/德克薩斯州.md "wikilink")，屬[達拉斯縣](../Page/達拉斯縣_\(德克薩斯州\).md "wikilink")、[塔蘭特縣和](../Page/塔蘭特縣.md "wikilink")[埃利斯縣](../Page/埃利斯縣_\(德克薩斯州\).md "wikilink")。面積211.2平方公里，2006年人口為153,812人。\[1\]

1863年開埠，1909年設市。

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/得克萨斯州城市.md "wikilink")

1.  [Grand Prairie, Texas - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&geo_id=16000US4837000&_geoContext=01000US%7C04000US48%7C16000US4837000&_street=&_county=Grand+Prairie+city&_cityTown=Grand+Prairie+city&_state=04000US48&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)