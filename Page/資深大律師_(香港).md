香港的**資深大律師**（[英語](../Page/英語.md "wikilink")：**Senior
Counsel**）是[香港對經確認後的資深](../Page/香港.md "wikilink")[訟務律師稱號](../Page/訟務律師.md "wikilink")。

## 簡介

[大律師累積十年經驗後](../Page/大律師.md "wikilink")，有機會獲[香港終審法院首席法官委任成為](../Page/香港終審法院首席法官.md "wikilink")[資深大律師](../Page/資深大律師.md "wikilink")（Senior
Counsel,
S.C.）。資深大律師在香港的地位，等同[香港主權移交前的](../Page/香港主權移交.md "wikilink")[御用大律師](../Page/御用大律師.md "wikilink")（Queen's
Counsel, Q.C.）。所有在主權移交前已獲認許為御用大律師的香港大律師，在香港主權移交後全部自動獲得香港資深大律師的資格。

過去[英國的御用大律師資格授予制度曾為香港不少法律界人士詬病](../Page/英國.md "wikilink")，包括香港法律界泰斗大律師[胡鴻烈及](../Page/胡鴻烈.md "wikilink")[余叔韶等](../Page/余叔韶.md "wikilink")；因此在設立資深大律師制度時，其資格的認定曾掀起一陣討論。然此項委任絕非理所當然，首席法官會考慮其行內成就、貢獻、經驗等，絕非擁有年資便能順理成章獲得委任。而大多數資深大律師被委任時，年資已超過10年。

至現時為止，[香港大律師公會僅有一名名譽資深大律師](../Page/香港大律師公會.md "wikilink")——前[香港大學法律學院院長](../Page/香港大學法律學院.md "wikilink")、人權學者[陳文敏](../Page/陳文敏.md "wikilink")，他在2003年獲得名譽資深大律師的資格。

截至2018年6月，資深大律師共只有104人（93男、11女）。而資深大律師會根據其執業年份列出排名，排首位的是身為[泛民主派元老的](../Page/泛民主派.md "wikilink")[民主黨創黨主席](../Page/香港民主黨.md "wikilink")[李柱銘](../Page/李柱銘.md "wikilink")。

## 知名的資深大律師

  - [黃仁龍](../Page/黃仁龍.md "wikilink")
  - [唐明治](../Page/唐明治.md "wikilink")
  - [湯家驊](../Page/湯家驊.md "wikilink")
  - [余若薇](../Page/余若薇.md "wikilink")
  - [李柱銘](../Page/李柱銘.md "wikilink")
  - [梁家傑](../Page/梁家傑.md "wikilink")
  - [駱應淦](../Page/駱應淦.md "wikilink")
  - [清洪](../Page/清洪.md "wikilink")
  - [翟紹唐](../Page/翟紹唐.md "wikilink")
  - [袁國強](../Page/袁國強.md "wikilink")
  - [廖長城](../Page/廖長城.md "wikilink")
  - [陳景生](../Page/陳景生.md "wikilink")
  - [余若海](../Page/余若海.md "wikilink")
  - [胡漢清](../Page/胡漢清.md "wikilink")
  - [馮華健](../Page/馮華健.md "wikilink")
  - [李志喜](../Page/李志喜.md "wikilink")
  - [王正宇](../Page/王正宇.md "wikilink")
  - [何沛謙](../Page/何沛謙.md "wikilink")
  - [陳文敏](../Page/陳文敏.md "wikilink")（名譽資深大律師）
  - [石永泰](../Page/石永泰.md "wikilink")
  - [張健利](../Page/張健利.md "wikilink")
  - [梁定邦](../Page/梁定邦_\(大律師\).md "wikilink")
  - [麥高義](../Page/麥高義.md "wikilink")
  - [莫樹聯](../Page/莫樹聯.md "wikilink")
  - [譚允芝](../Page/譚允芝.md "wikilink")
  - [潘熙](../Page/潘熙.md "wikilink")
  - [郭慶偉](../Page/郭慶偉.md "wikilink")
  - [謝華淵](../Page/謝華淵.md "wikilink")
  - [梁冰濂](../Page/梁冰濂.md "wikilink")
  - [林定國](../Page/林定國.md "wikilink")
  - [楊家雄](../Page/楊家雄.md "wikilink")
  - [彭耀鴻](../Page/彭耀鴻.md "wikilink")
  - [戴啟思](../Page/戴啟思.md "wikilink")
  - [鄭若驊](../Page/鄭若驊.md "wikilink")
  - [黃國瑛](../Page/黃國瑛.md "wikilink")
  - [梁卓然](../Page/梁卓然.md "wikilink")
  - [毛樂禮](../Page/毛樂禮.md "wikilink")
  - [陳樂信](../Page/陳樂信.md "wikilink")
  - [蔡維邦](../Page/蔡維邦.md "wikilink")

## 參見

  - [香港大律師公會](../Page/香港大律師公會.md "wikilink")
  - [御用大律師](../Page/御用大律師.md "wikilink")

## 外部連結

  - [香港大律師公會](http://www.hkba.org/)
  - [香港資深大律師列表](http://www.hkba.org/Bar-List/senior-counsel)

[香港資深大律師](../Category/香港資深大律師.md "wikilink")
[Category:香港职业](../Category/香港职业.md "wikilink")