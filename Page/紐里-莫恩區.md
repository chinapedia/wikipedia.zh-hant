**紐里-莫恩區** （Newry and Mourne District
Council；[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：Comhairle
an Iúir agus Mhúrn；[阿爾斯特蘇格蘭語](../Page/阿爾斯特蘇格蘭語.md "wikilink")：Cooncil
o'er tha Newrie an tha Mourn
Kintra），是[北愛爾蘭東南部的一個區](../Page/北愛爾蘭.md "wikilink")。面積902
km²，2006年人口93,400人。區行政中心為[紐里](../Page/紐里.md "wikilink")。

## 參考文獻

[N](../Category/北愛爾蘭行政區劃.md "wikilink")