[HK_WC_Johnston_Road_Cosmos_Books_Ltd.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_Johnston_Road_Cosmos_Books_Ltd.jpg "fig:HK_WC_Johnston_Road_Cosmos_Books_Ltd.jpg")[莊士敦道的門市部](../Page/莊士敦道.md "wikilink")\]\]
[Hk-bookfair2.jpg](https://zh.wikipedia.org/wiki/File:Hk-bookfair2.jpg "fig:Hk-bookfair2.jpg")的天地圖書攤位\]\]

**天地圖書有限公司**（**Cosmos Books
Ltd.**），在1976年成立<small>\[1\]</small>，是[香港](../Page/香港.md "wikilink")[出版社之一](../Page/出版社.md "wikilink")，集出版、發行及零售於一身。陳松齡是天地圖書董事長。天地是中資公司[聯合出版集團成員之一](../Page/聯合出版集團.md "wikilink")\[2\]。

天地圖書以「以兩條腿走路」的方式營運，一方面出版通俗流行讀物，同時亦出版具有水準文藝作品，兼顧不同讀者群的需要。成立之初，天地圖書出版一些時事評論方面的著作，也出版一些翻譯[文學作品](../Page/文學作品.md "wikilink")、[政壇要人的回憶錄及](../Page/政治.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")，著名武俠小說作家[梁羽生的著作都是委託天地圖書出版](../Page/梁羽生.md "wikilink")<small>\[3\]</small>；1980年代初期，隨著[中國內地思想解放](../Page/中國內地.md "wikilink")，出版了一個系列內地作家作品集《天地文叢》。

## 門市部

位於香港[灣仔](../Page/灣仔.md "wikilink")[莊士敦道](../Page/莊士敦道.md "wikilink")30號的總店。

總店佔地面積一萬六千多平方呎，為港島最大規模的書店，除中文部外，尚設有英文部、文具部和雜誌部，此外還有附設的小展覽廳，定期舉辦專題展銷和公開座談會。

### 歷史

天地圖書初時只有在[香港島開設](../Page/香港島.md "wikilink")[書籍](../Page/書籍.md "wikilink")[門市部](../Page/商店.md "wikilink")（總店），自1999年起在[尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道開設分店](../Page/彌敦道.md "wikilink")<small>\[4\]</small>，直至2011年九龍店遷至[旺角](../Page/旺角.md "wikilink")。

1999年天地圖書的灣仔總店因電線漏電而引致火警，大批書刊被浸被焚，連裝修估計損失達3,000萬港元。

## 出版部

天地圖書出版[流行讀物](../Page/時尚.md "wikilink")、[消閒小品](../Page/娛樂.md "wikilink")、嚴肅[文學](../Page/文學.md "wikilink")、[歷史](../Page/歷史.md "wikilink")、[政治](../Page/政治.md "wikilink")、[社會科學類](../Page/社會科學.md "wikilink")[圖書等](../Page/圖書.md "wikilink")。天地圖書的三大皇牌作者是[亦舒](../Page/亦舒.md "wikilink")、[李碧華及](../Page/李碧華.md "wikilink")[蔡瀾](../Page/蔡瀾.md "wikilink")<small>\[5\]</small>。

### 歷史

最初是出一些時事評論方面的著作，部份當代西方思想普及讀物（如[存在主義](../Page/存在主義.md "wikilink")、[結構主義](../Page/結構主義.md "wikilink")、[佛洛依德等](../Page/佛洛依德.md "wikilink")），對介紹新思潮起了一些作用﹔也出版一些翻譯文學作品，如[松本清張的](../Page/松本清張.md "wikilink")[推理小說](../Page/推理小說.md "wikilink")、政壇要人的回憶錄（[戴高樂](../Page/戴高樂.md "wikilink")《[希望回憶錄](../Page/希望回憶錄.md "wikilink")》等）﹔後來有於梨華的留學生文學，有[亦舒](../Page/亦舒.md "wikilink")、[梁羽生的愛情和武俠小說](../Page/梁羽生.md "wikilink")，[巴金的](../Page/巴金.md "wikilink")《[家](../Page/家_\(小说\).md "wikilink")》、《[春](../Page/春_\(小说\).md "wikilink")》、《[秋](../Page/秋_\(小说\).md "wikilink")》（後來又出版了《巴金小說全集》），然後是[李碧華和](../Page/李碧華.md "wikilink")[蔡瀾加入](../Page/蔡瀾.md "wikilink")。

## 天地網

天地網是天地圖書公司的官方網頁，為[讀者提供有關最新](../Page/讀者.md "wikilink")[圖書](../Page/圖書.md "wikilink")、[出版](../Page/出版.md "wikilink")、[文化等資訊](../Page/文化.md "wikilink")。
\[6\]

## 相關

  - [當代散文典藏](../Page/當代散文典藏.md "wikilink")
  - [香港書店列表](../Page/香港書店列表.md "wikilink")

## 參考文獻

## 外部連結

  - [天地圖書官方網站](http://www.cosmosbooks.com.hk/)
  - [香港印藝學會：自有天地－陳松齡](http://www.gaahk.org.hk/articles/214/cbi04801.htm)

[Category:1976年成立的公司](../Category/1976年成立的公司.md "wikilink")
[Category:香港書店](../Category/香港書店.md "wikilink")
[Category:香港出版社](../Category/香港出版社.md "wikilink")

1.
2.
3.
4.
5.
6.  \[[https://www.cosmosbooks.com.hk/\]l](https://www.cosmosbooks.com.hk/%5Dl)
    天地圖書有限公司 官方網 （提供網上訂購服務）