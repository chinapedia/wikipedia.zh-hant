**朱兆祥**（），[浙江](../Page/浙江.md "wikilink")[镇海人](../Page/镇海区_\(宁波市\).md "wikilink")，中国[力学家](../Page/力学.md "wikilink")、教育家和科技事业活动家，[宁波大学创建时的首任校长](../Page/宁波大学.md "wikilink")\[1\]。为中国[爆炸力学研究做出了开拓性贡献](../Page/爆炸力学.md "wikilink")\[2\]。

1949年以前在[浙江大学加入](../Page/浙江大学.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")，从事地下活动，反对[中国国民党](../Page/中国国民党.md "wikilink")。1955年10月，被[中国科学院派往](../Page/中国科学院.md "wikilink")[深圳](../Page/深圳.md "wikilink")，负责接回[钱学森](../Page/钱学森.md "wikilink")，并协助、参与了力学研究所的创立\[3\]。1957年[反右运动发生后](../Page/反右运动.md "wikilink")，因与[钱伟长来往受处罚](../Page/钱伟长.md "wikilink")，被开除[中国共产党党籍](../Page/中国共产党.md "wikilink")\[4\]。其后在[中国科学技术大学任教](../Page/中国科学技术大学.md "wikilink")。1985年秋，宁波大学成立，朱兆祥为首任校长\[5\]。

## 参考文献

[Category:中華人民共和國力學家](../Category/中華人民共和國力學家.md "wikilink")
[Category:中華人民共和國工程師](../Category/中華人民共和國工程師.md "wikilink")
[Category:中華人民共和國物理教育家](../Category/中華人民共和國物理教育家.md "wikilink")
[Category:中国科学技术大学教授](../Category/中国科学技术大学教授.md "wikilink")
[Category:中共地下党员](../Category/中共地下党员.md "wikilink")
[Category:恢复中国共产党党籍者](../Category/恢复中国共产党党籍者.md "wikilink")
[Category:浙江大學校友](../Category/浙江大學校友.md "wikilink")
[Category:宁波人](../Category/宁波人.md "wikilink")
[Category:宁波大学教授](../Category/宁波大学教授.md "wikilink")
[Category:宁波大学校长](../Category/宁波大学校长.md "wikilink")
[Category:镇海县人](../Category/镇海县人.md "wikilink")
[Zhao兆祥](../Category/朱姓.md "wikilink")

1.  [宁波大学首任校长去世
    微博追忆](http://nb.city.ifeng.com/nbxw/detail_2011_12/05/117919_0.shtml)
2.  [沉痛悼念中国力学学会第一届理事朱兆祥先生](http://www.cstam.org.cn/templates/lxxh_1/index.aspx?nodeid=104&page=ContentPage&contentid=170424)

3.  [朱兆祥：50年前我在罗湖桥头接钱学森回国](http://news.qq.com/a/20071207/002745.htm)
4.  [郑哲敏院士忆恩师钱伟长：苍茫远去的光荣与坎坷](http://news.sciencenet.cn/htmlnews/2010/8/235454.shtm)
5.  [朱兆祥：难忘那些艰难困苦的日子](http://www.cas.cn/jzd/jfk/jxzys/200411/t20041116_1726330.shtml)