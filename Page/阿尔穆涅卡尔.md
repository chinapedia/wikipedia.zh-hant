[LocationAlmuñécar.png](https://zh.wikipedia.org/wiki/File:LocationAlmuñécar.png "fig:LocationAlmuñécar.png")
**阿尔穆涅卡尔**（[西班牙语](../Page/西班牙语.md "wikilink")：****）是[西班牙](../Page/西班牙.md "wikilink")[安达卢西亚自治区](../Page/安达卢西亚自治区.md "wikilink")[格拉纳达省的一座城市](../Page/格拉纳达省.md "wikilink")，位于[热带海岸沿线](../Page/热带海岸.md "wikilink")，面积83.4平方公里，人口26,264人（2006年）。

## 友好城市

  - [德国](../Page/德国.md "wikilink")[菲尔斯滕费尔德布鲁克](../Page/菲尔斯滕费尔德布鲁克.md "wikilink")

  - [法国](../Page/法国.md "wikilink")[利夫里-加尔冈](../Page/利夫里-加尔冈.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")[切尔韦泰里](../Page/切尔韦泰里.md "wikilink")

## 外部链接

  - [Almuñécar Official Site](http://www.almunecar.info/index_en.html)
  - [Local Tourist Guide to Almuñécar and La
    Herradura](https://web.archive.org/web/20060205034249/http://almunecar.com/)
  - [Bus
    Timetables](https://web.archive.org/web/20060221123540/http://www.almunecar.com/Visitors_Guide/Making_Plans/Bus_Timetable.html)
  - [Webcam in Playa de
    Tesorillo](https://web.archive.org/web/20060215042817/http://www.alkisol.com/webcam/)
  - [Webcam in Playa de San
    Cristobal](http://www.helios-hotels.com/almunecar/en/webcam-almunecar-costa-tropical-granada.htm)
  - [Photographic Guide to
    Almuñécar](http://www.greatorme.org.uk/Almunecar1.html)

[A](../Category/格拉纳达省市镇.md "wikilink")