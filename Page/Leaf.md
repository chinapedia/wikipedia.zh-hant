**Leaf**（），是[日本公司](../Page/日本.md "wikilink")[AQUAPLUS](../Page/AQUAPLUS.md "wikilink")（アクアプラス）旗下負責開發及發售[成人遊戲的品牌](../Page/日本成人遊戲.md "wikilink")，在[大阪](../Page/大阪.md "wikilink")-{[淀川區](../Page/淀川區.md "wikilink")}-和[东京都有办公室](../Page/东京.md "wikilink")。Leaf亦被稱為「」、「葉」。

## 作品列表

※是由大阪（伊丹）開發室開發

  - 1995年2月24日－ ※
  - 1995年8月3日－[Filsnown -光與刻-](../Page/Filsnown_-光與刻-.md "wikilink") ※
  - 1996年1月26日－[雫](../Page/雫.md "wikilink")　※
  - 1996年7月26日－[痕](../Page/痕.md "wikilink")　※
  - 1997年5月23日－[To Heart](../Page/To_Heart.md "wikilink")　※
  - 1998年5月1日－[白色相簿](../Page/白色相簿.md "wikilink")　※
  - 1999年5月28日－[漫畫派對](../Page/漫畫派對_\(遊戲\).md "wikilink")
  - 2000年4月28日－[Magical☆Antique](../Page/Magical☆Antique.md "wikilink")　※
  - 2001年2月9日－[誰彼](../Page/誰彼.md "wikilink")　※
  - 2002年4月26日－[傳頌之物](../Page/傳頌之物.md "wikilink")
  - 2002年7月26日－痕 重製版　※
  - 2003年2月28日－[Routes](../Page/Routes.md "wikilink")　※
  - 2003年9月26日－[沒有天使的12月](../Page/沒有天使的12月.md "wikilink")
  - 2004年1月23日－雫 重製版　※
  - 2005年4月28日－[Tears to Tiara](../Page/Tears_to_Tiara.md "wikilink")　※
  - 2005年9月22日－　※
  - 2005年12月9日－[ToHeart2
    XRATED](../Page/ToHeart2.md "wikilink")　大阪、東京共同開發
  - 2006年7月28日－[Fullani](../Page/Fullani.md "wikilink")
  - 2008年2月29日 - [ToHeart2
    AnotherDays](../Page/ToHeart2.md "wikilink")　大阪、東京共同開發
  - 2008年12月26日 - [你在米吉多之丘的呼喚](../Page/你在米吉多之丘的呼喚.md "wikilink")
  - 2009年6月26日 -
  - 2010年3月26日 - [白色相簿2](../Page/白色相簿2.md "wikilink") 序章
  - 2011年1月28日 - [星の王子くん](../Page/星の王子くん.md "wikilink")
  - 2011年12月22日 - [白色相簿2](../Page/白色相簿2.md "wikilink") 终章
  - 2018年2月14日 - [白色相簿2](../Page/白色相簿2.md "wikilink") EXTENDED EDITION

<!-- end list -->

  -

<!-- end list -->

  - 1996年11月22日－
  - 1997年11月28日－
  - 2000年1月28日－
  - 2004年4月28日－
  - 2009年12月18日－

## 外部链接

  - [Leaf](http://leaf.aquaplus.co.jp/product/)
  - [AQUAPLUS](http://www.aquaplus.co.jp/)
  - [P/ECE 官方主页](http://www.piece-me.com/)

[Leaf](../Category/Leaf.md "wikilink")
[Category:日本成人遊戲公司](../Category/日本成人遊戲公司.md "wikilink")