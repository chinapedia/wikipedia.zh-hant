**10月31日**是[阳历一年中的第](../Page/阳历.md "wikilink")304天（[闰年第](../Page/闰年.md "wikilink")305天），离全年的结束还有61天。

## 大事记

### 5世紀

  - [475年](../Page/475年.md "wikilink")：[西羅馬帝國皇帝](../Page/羅馬皇帝列表.md "wikilink")[罗慕路斯·奥古斯都即位](../Page/罗慕路斯·奥古斯都.md "wikilink")。

### 16世紀

  - [1517年](../Page/1517年.md "wikilink")：[德國神学家](../Page/德國.md "wikilink")[马丁·路德在](../Page/马丁·路德.md "wikilink")[维滕贝格一间教堂的大门上张贴他的](../Page/维滕贝格.md "wikilink")《[九十五条论纲](../Page/九十五条论纲.md "wikilink")》，标志着[新教的](../Page/新教.md "wikilink")[宗教改革運動开始](../Page/宗教改革運動.md "wikilink")。

### 19世紀

  - [1864年](../Page/1864年.md "wikilink")：[内华达州加入](../Page/内华达州.md "wikilink")[美國](../Page/美國.md "wikilink")，成为其第36个州。
  - [1874年](../Page/1874年.md "wikilink")：[清朝和](../Page/清朝.md "wikilink")[日本订立](../Page/日本.md "wikilink")《[台事北京专约](../Page/北京專約.md "wikilink")》。

### 20世紀

  - [1917年](../Page/1917年.md "wikilink")：[第一次世界大战](../Page/第一次世界大战.md "wikilink")：，号称是世界上最后一次成功的[骑兵冲锋](../Page/骑兵.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：
      - 第一次世界大战，[同盟国](../Page/同盟国.md "wikilink")[土耳其被迫宣布向](../Page/土耳其.md "wikilink")[協約國投降](../Page/協約國.md "wikilink")。
      - [奥地利宣布共和](../Page/奥地利.md "wikilink")。
  - [1922年](../Page/1922年.md "wikilink")：[贝尼托·墨索里尼就任](../Page/贝尼托·墨索里尼.md "wikilink")[意大利第](../Page/意大利.md "wikilink")40任总理。
  - [1940年](../Page/1940年.md "wikilink")：德軍中止對[英國的](../Page/英國.md "wikilink")[空襲](../Page/不列顛空戰.md "wikilink")。
  - [1941年](../Page/1941年.md "wikilink")：[美国](../Page/美国.md "wikilink")[拉什莫尔山高达](../Page/拉什莫尔山.md "wikilink")18米的、四位前总统[华盛顿](../Page/乔治·华盛顿.md "wikilink")、[杰斐逊](../Page/托马斯·杰斐逊.md "wikilink")、[老罗斯福和](../Page/西奥多·罗斯福.md "wikilink")[林肯的巨型头像全部完工](../Page/亚伯拉罕·林肯.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[中國電視公司正式開播](../Page/中國電視公司.md "wikilink")。
  - [1971年](../Page/1971年.md "wikilink")：[中華電視台正式開播](../Page/中華電視公司.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[清潔香港運動全面展開](../Page/清潔香港運動.md "wikilink")。[香港總督](../Page/香港總督.md "wikilink")[麥理浩爵士呼籲全港市民大力支持清潔運動](../Page/麥理浩.md "wikilink")，在[中環舉行火燒](../Page/中環.md "wikilink")「垃圾蟲」儀式，象徵清潔運動正式開始。
  - [1980年](../Page/1980年.md "wikilink")：
      - [中国共产党中央委员会决定开除](../Page/中国共产党中央委员会.md "wikilink")[康生](../Page/康生.md "wikilink")、[谢富治党籍](../Page/谢富治.md "wikilink")。

      - 。
  - [1984年](../Page/1984年.md "wikilink")：
      - [印度总理](../Page/印度总理.md "wikilink")[英迪拉·甘地在總理府被她的兩個](../Page/英迪拉·甘地.md "wikilink")[錫克教保鏢開槍刺殺身亡](../Page/錫克教.md "wikilink")。
      - [严打](../Page/严打.md "wikilink")“第一战役”结束，中共中央开始部署“第二战役”。
  - [1986年](../Page/1986年.md "wikilink")：[台北市立動物園遷至](../Page/台北市立動物園.md "wikilink")[木柵現址](../Page/木柵.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：上海[东风饭店发生重大食物中毒事故](../Page/上海总会大楼.md "wikilink")，百桌酒席700多人中毒。
  - [1988年](../Page/1988年.md "wikilink")：中国内地第一条高速公路[沪嘉高速公路通车](../Page/沪嘉高速公路.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：上海港驳船运输公司的一艘载重量为4359.7吨的「金山」轮在渤海湾突遇气旋风後下落不明而遇难。\[1\]
  - [1992年](../Page/1992年.md "wikilink")：[伽利略蒙冤](../Page/伽利略·伽利莱.md "wikilink")360年后，终于获得[梵蒂冈](../Page/梵蒂冈.md "wikilink")[教宗的平反](../Page/教宗.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[香港有線電視正式啟播](../Page/香港有線電視.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：[埃及航空990号班机在](../Page/埃及航空990號班機空難.md "wikilink")[美国](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")[肯尼迪国际机场起飞后不久](../Page/肯尼迪国际机场.md "wikilink")，墜毁于[大西洋中](../Page/大西洋.md "wikilink")，共造成217人死亡。
  - [2000年](../Page/2000年.md "wikilink")：[新加坡航空006號班機在](../Page/新加坡航空006號班機空難.md "wikilink")[台灣桃園國際機場起飛時失事](../Page/台灣桃園國際機場.md "wikilink")，機上179名乘客和機員有83人死亡，超過70人受傷，死者中包括[香港人](../Page/香港人.md "wikilink")。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[引黄济津开闸放水](../Page/引黄济津.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")：执政长达22年的[马来西亚首相](../Page/马来西亚.md "wikilink")[马哈迪·莫哈末正式宣佈退休并结束任职](../Page/马哈迪·莫哈末.md "wikilink")，由[巫统大会副主席](../Page/马来民族统一机构.md "wikilink")[阿布杜拉·巴达威继任之](../Page/阿布杜拉·巴达威.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：
      - [台灣](../Page/台灣.md "wikilink")[中時晚報發行最後一天](../Page/中時晚報.md "wikilink")，翌日起停刊。

      -
  - [2009年](../Page/2009年.md "wikilink")：[上海长江隧桥通车](../Page/上海长江隧桥.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：根據[聯合國人口基金會的估計](../Page/聯合國人口基金會.md "wikilink")，全球人口突破70億人。\[2\]
  - [2018年](../Page/2018年.md "wikilink")：纪念[印度独立运动领导人](../Page/印度独立运动.md "wikilink")、首任[萨达尔·瓦拉巴伊·帕特尔的](../Page/萨达尔·瓦拉巴伊·帕特尔.md "wikilink")[团结雕像于](../Page/团结雕像.md "wikilink")[古吉拉特邦](../Page/古吉拉特邦.md "wikilink")[那尔马达县正式落成](../Page/那尔马达县.md "wikilink")。雕像高达182米，是目前[全球最高雕像](../Page/最高雕像列表.md "wikilink")。

## 出生

[Chiang_Kai-shek_at_Double_Ten_Day_Parade_19661010.jpg](https://zh.wikipedia.org/wiki/File:Chiang_Kai-shek_at_Double_Ten_Day_Parade_19661010.jpg "fig:Chiang_Kai-shek_at_Double_Ten_Day_Parade_19661010.jpg")[蔣中正](../Page/蔣中正.md "wikilink")\]\]

  - [1345年](../Page/1345年.md "wikilink")：[斐迪南一世](../Page/斐迪南一世_\(葡萄牙\).md "wikilink")，第9位[葡萄牙和](../Page/葡萄牙.md "wikilink")[阿爾加維國王](../Page/阿爾加維.md "wikilink")（逝於[1383年](../Page/1383年.md "wikilink")）
  - [1391年](../Page/1391年.md "wikilink")：[杜阿爾特一世](../Page/杜阿爾特一世_\(葡萄牙\).md "wikilink")，第11位葡萄牙和阿爾加維國王（逝於[1438年](../Page/1438年.md "wikilink")）
  - [1424年](../Page/1424年.md "wikilink")：[瓦迪斯瓦夫三世](../Page/瓦迪斯瓦夫三世.md "wikilink")，[亞蓋隆王朝的](../Page/亞蓋隆王朝.md "wikilink")[波蘭國和](../Page/波蘭.md "wikilink")[匈牙利國王](../Page/匈牙利.md "wikilink")（逝於[1444年](../Page/1444年.md "wikilink")）
  - [1632年](../Page/1632年.md "wikilink")：[扬·弗美尔](../Page/扬·弗美尔.md "wikilink")，[荷蘭黃金時代最傑出的畫家之一](../Page/荷蘭.md "wikilink")（逝於[1675年](../Page/1675年.md "wikilink")）
  - [1705年](../Page/1705年.md "wikilink")：[-{zh-hans:克雷芒十四世;zh-hk:克勉十四世;zh-tw:克勉十四世;}-](../Page/克勉十四世.md "wikilink")，羅馬[教宗](../Page/教宗.md "wikilink")（逝於[1774年](../Page/1774年.md "wikilink")）
  - [1795年](../Page/1795年.md "wikilink")：[約翰·濟慈](../Page/济慈.md "wikilink")，[英国诗人](../Page/英国.md "wikilink")。（逝於[1821年](../Page/1821年.md "wikilink")）
  - [1835年](../Page/1835年.md "wikilink")：[阿道夫·冯·拜尔](../Page/阿道夫·冯·拜尔.md "wikilink")，[德国](../Page/德国.md "wikilink")[化学家](../Page/化学家.md "wikilink")，1905年[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")（逝於[1917年](../Page/1917年.md "wikilink")）
  - [1815年](../Page/1815年.md "wikilink")：[卡尔·魏尔施特拉斯](../Page/卡尔·魏尔施特拉斯.md "wikilink")，德国[数学家](../Page/数学家.md "wikilink")，被譽為「現代分析之父」。（逝於[1897年](../Page/1897年.md "wikilink")）
  - [1838年](../Page/1838年.md "wikilink")：[路易斯一世](../Page/路易斯一世_\(葡萄牙\).md "wikilink")，葡萄牙[萨克森-科堡-哥达王朝第](../Page/萨克森-科堡-哥达王朝.md "wikilink")2位國王，於[里斯本建立了全球第一間水族館](../Page/里斯本.md "wikilink")（逝於[1889年](../Page/1889年.md "wikilink")）
  - [1876年](../Page/1876年.md "wikilink")：[娜塔莉·克利福德·巴尼](../Page/娜塔莉·克利福德·巴尼.md "wikilink")，美國旅法劇作家、詩人和小說家（逝於[1972年](../Page/1972年.md "wikilink")）
  - [1887年](../Page/1887年.md "wikilink")：[蔣中正](../Page/蔣中正.md "wikilink")，[中國國民黨總裁](../Page/中國國民黨.md "wikilink")、[中華民國總統](../Page/中華民國總統.md "wikilink")（逝於[1975年](../Page/1975年.md "wikilink")）
  - [1892年](../Page/1892年.md "wikilink")：[亚历山大·阿廖欣](../Page/亚历山大·阿廖欣.md "wikilink")，俄裔法國西洋棋大師，曾四次獲得西洋棋世界冠軍（逝於[1946年](../Page/1946年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[弗里茨·瓦尔特](../Page/弗里茨·瓦尔特.md "wikilink")，德國足球史上首位傳奇巨星及領袖人物（逝於[2002年](../Page/2002年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[諾羅敦·西哈努克](../Page/諾羅敦·西哈努克.md "wikilink")，[柬埔寨王國国王](../Page/柬埔寨王國.md "wikilink")（逝於[2012年](../Page/2012年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[约翰·波普](../Page/约翰·波普_\(化学家\).md "wikilink")，[英国](../Page/英国.md "wikilink")[化学家](../Page/化学家.md "wikilink")，1998年[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")（逝於[2004年](../Page/2004年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[迈克尔·科林斯](../Page/迈克尔·科林斯.md "wikilink")，[美國](../Page/美國.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：
      - [林宗仁](../Page/林宗仁.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")（逝於[2011年](../Page/2011年.md "wikilink")）
      - [赫尔曼·范龙佩](../Page/赫尔曼·范龙佩.md "wikilink")，[歐盟首任常任主席](../Page/歐盟.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[-{zh-cn:扎哈·哈迪德; zh-tw:札哈·哈蒂;
    zh-hk:薩哈·哈帝;}-](../Page/薩哈·哈帝.md "wikilink")，[英国女建筑师](../Page/英国.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[刘晓庆](../Page/刘晓庆.md "wikilink")，[中国女演員](../Page/中国.md "wikilink")
  - [1956年](../Page/1956年.md "wikilink")：[郭昶](../Page/郭昶.md "wikilink")，中国演員（逝於2006年）
  - [1959年](../Page/1959年.md "wikilink")：[尼尔·斯蒂芬森](../Page/尼尔·斯蒂芬森.md "wikilink")，美國推理小說家
  - [1960年](../Page/1960年.md "wikilink")：[禮薩·巴列維](../Page/禮薩·巴列維.md "wikilink")，伊朗末代皇太子
  - [1961年](../Page/1961年.md "wikilink")：
      - [-{zh-cn:彼得·杰克逊;zh-hk:彼德·積遜;zh-tw:彼得·傑克森;}-](../Page/彼得·杰克逊.md "wikilink")，[新西兰電影導演](../Page/新西兰.md "wikilink")
      - [小賴瑞·慕蘭](../Page/小賴瑞·慕蘭.md "wikilink")，愛爾蘭搖滾團體[U2創始人](../Page/U2.md "wikilink")、鼓手
  - [1963年](../Page/1963年.md "wikilink")：[勞伯·許奈德](../Page/勞伯·許奈德.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - [1964年](../Page/1964年.md "wikilink")：
      - [雲巴士頓](../Page/马尔科·范巴斯滕.md "wikilink")，[荷兰](../Page/荷兰.md "wikilink")[足球員](../Page/足球.md "wikilink")，現為[荷蘭國家隊的領隊](../Page/荷蘭國家足球隊.md "wikilink")
      - [愛德華·科科伊季](../Page/愛德華·科科伊季.md "wikilink")，[摔跤运动员](../Page/摔跤.md "wikilink")、商人、政治家，[格魯吉亞](../Page/格魯吉亞.md "wikilink")[南奥塞梯總統](../Page/南奥塞梯.md "wikilink")
      - [馮翊綱](../Page/馮翊綱.md "wikilink")，台灣劇場[表演藝術家](../Page/表演藝術.md "wikilink")，著名相聲演員
  - [1968年](../Page/1968年.md "wikilink")：[高思博](../Page/高思博.md "wikilink")，台灣[政治人物](../Page/政治人物.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[吳奇隆](../Page/吳奇隆.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[飯島愛](../Page/飯島愛.md "wikilink")，[日本女藝人](../Page/日本.md "wikilink")（逝於[2008年](../Page/2008年.md "wikilink")）
  - [1976年](../Page/1976年.md "wikilink")：[小暮英麻](../Page/小暮英麻.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - 1976年：[張泰山](../Page/張泰山.md "wikilink")，[台灣棒球選手](../Page/台灣.md "wikilink")
  - 1976年：[張家浩](../Page/張家浩.md "wikilink")，[台灣棒球選手](../Page/台灣.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[張天霖](../Page/張天霖.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[艾迪·凯伊·托马斯](../Page/艾迪·凯伊·托马斯.md "wikilink")，美國男演員
  - 1980年：[玉智英](../Page/玉智英.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[任家萱](../Page/任家萱.md "wikilink")，台湾[女子演唱團體](../Page/女子演唱團體.md "wikilink")[S.H.E成員](../Page/S.H.E.md "wikilink")，台灣歌手、演員、主持人。
  - 1981年：[李菁](../Page/李菁_\(聽障教師\).md "wikilink")，香港聾人女狀元。（逝於[2008年](../Page/2008年.md "wikilink")）
  - [1983年](../Page/1983年.md "wikilink")：[是元介](../Page/是元介.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[布兰特·科里根](../Page/布兰特·科里根.md "wikilink")，美國模特、演員和男同性戀色情片演員
  - [1988年](../Page/1988年.md "wikilink")：[塞巴斯蒂安·布米](../Page/塞巴斯蒂安·布米.md "wikilink")，瑞士賽車手
  - [1991年](../Page/1991年.md "wikilink")：[須田亞香里](../Page/須田亞香里.md "wikilink")
    ，日本女子偶像團體[SKE48](../Page/SKE48.md "wikilink") team KII成員
  - [1992年](../Page/1992年.md "wikilink")：[李承協](../Page/李承協.md "wikilink")，韓國男子樂團[N.Flying隊長](../Page/N.Flying.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[余澎杉](../Page/余澎杉.md "wikilink")，新加坡儿童演员、部落客作者、Youtube视频制作者
  - [2005年](../Page/2005年.md "wikilink")：[萊昂諾爾](../Page/萊昂諾爾_\(西班牙公主\).md "wikilink")，[西班牙公主](../Page/西班牙.md "wikilink")

## 逝世

  - [1916年](../Page/1916年.md "wikilink")：[黄兴](../Page/黄兴.md "wikilink")，中國近代[資產階級民主革命家](../Page/資產階級.md "wikilink")，[辛亥革命元勛](../Page/辛亥革命.md "wikilink")（生于1874年）
  - [1925年](../Page/1925年.md "wikilink")：[米哈伊尔·伏龙芝](../Page/米哈伊尔·伏龙芝.md "wikilink")，[苏联统帅](../Page/苏联.md "wikilink")、军事理论家（1885年出生）
  - [1926年](../Page/1926年.md "wikilink")：[哈利·胡迪尼](../Page/哈利·胡迪尼.md "wikilink")，[魔术师](../Page/魔术师.md "wikilink")（生于1874年）
  - [1956年](../Page/1956年.md "wikilink")：[薛覺先](../Page/薛覺先.md "wikilink")，[香港粵劇演員](../Page/香港.md "wikilink")（生于1904年）
  - [1966年](../Page/1966年.md "wikilink")：[马约翰](../Page/马约翰.md "wikilink")，[中华全国体育总会主任](../Page/中华全国体育总会.md "wikilink")、[清华大学教授](../Page/清华大学.md "wikilink")（生于1882年）
  - [1983年](../Page/1983年.md "wikilink")：[陸家羲](../Page/陸家羲.md "wikilink")，中國業餘數學家，身後獲頒[國家自然科學獎一等獎](../Page/國家自然科學獎.md "wikilink")。（生於1935年）
  - [1984年](../Page/1984年.md "wikilink")：[英迪拉·甘地](../Page/英迪拉·甘地.md "wikilink")，[印度总理](../Page/印度总理.md "wikilink")（1917年出生）
  - [2000年](../Page/2000年.md "wikilink")：[華月](../Page/華月.md "wikilink")，[日本樂團](../Page/日本.md "wikilink")[Raphael中的隊長](../Page/Raphael.md "wikilink")，擔任[吉他手](../Page/吉他手.md "wikilink")（1981年出生）
  - [2009年](../Page/2009年.md "wikilink")：[钱学森](../Page/钱学森.md "wikilink")，中国科学家、航天之父（1911年出生）

## 节假日和习俗

  - [萬聖夜](../Page/萬聖夜.md "wikilink")

  - ：[蔣公誕辰](../Page/蔣公誕辰紀念日.md "wikilink")（本節日已於2007年8月29日由[中華民國內政部公告刪除](../Page/中華民國內政部.md "wikilink")）及[榮民節](../Page/榮民節.md "wikilink")

  - [宗教改革日](../Page/宗教改革日.md "wikilink")

  - [玫瑰月最後一天](../Page/玫瑰月.md "wikilink")

  - [世界勤俭日](../Page/世界勤俭日.md "wikilink")

  - [中國電視公司及](../Page/中國電視公司.md "wikilink")[中華電視公司](../Page/中華電視公司.md "wikilink")[台慶](../Page/台慶.md "wikilink")

  - 自[2014年起](../Page/2014年.md "wikilink")，每年的10月31日是[世界城市日](../Page/世界城市日.md "wikilink")

## 參考文獻

1.
2.