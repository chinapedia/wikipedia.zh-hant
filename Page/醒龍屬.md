**醒龍屬**（屬名：*Abrictosaurus*）意為「不眠的蜥蜴」，是[畸齒龍科](../Page/畸齒龍科.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於早[侏儸紀的南部](../Page/侏儸紀.md "wikilink")[非洲](../Page/非洲.md "wikilink")。醒龍是種小型、二足、[草食性或](../Page/草食性.md "wikilink")[雜食性的恐龍](../Page/雜食性.md "wikilink")，身長接近1.2公尺，重量少於45公斤。

醒龍的化石只有兩個個體，發現於[南非](../Page/南非.md "wikilink")[開普省與](../Page/開普省.md "wikilink")[賴索托](../Page/賴索托.md "wikilink")[加查斯內克區的](../Page/加查斯內克區.md "wikilink")[上艾略特組](../Page/上艾略特組.md "wikilink")。上艾略特組的年代被認為是早[侏儸紀的](../Page/侏儸紀.md "wikilink")[赫唐階到](../Page/赫唐階.md "wikilink")[錫內穆階](../Page/錫內穆階.md "wikilink")，接近2億到1億9000萬年前\[1\]。上艾略特組被認為過去是沙丘與季節性的氾濫平原，氣候為半乾旱型，偶爾有豪雨。該地層還發現了以下恐龍：[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[合踝龍](../Page/合踝龍.md "wikilink")、[蜥腳形亞目的](../Page/蜥腳形亞目.md "wikilink")[大椎龍](../Page/大椎龍.md "wikilink")、以及其他的[畸齒龍科如](../Page/畸齒龍科.md "wikilink")[畸齒龍](../Page/畸齒龍.md "wikilink")、[狼鼻龍](../Page/狼鼻龍.md "wikilink")。當地也發現了大量的陸地動物化石，例如[鱷形類](../Page/鱷形類.md "wikilink")、[犬齒獸類](../Page/犬齒獸類.md "wikilink")、以及早期[哺乳類](../Page/哺乳類.md "wikilink")\[2\]。

## 敘述

[Abrictosaurus.jpg](https://zh.wikipedia.org/wiki/File:Abrictosaurus.jpg "fig:Abrictosaurus.jpg")
醒龍屬於畸齒龍科，畸齒龍科是群小型、早期[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")，以牠們的[異型齒齒列為名](../Page/異型齒.md "wikilink")。畸齒龍科最著名的特徵是上下頜各有一對大型、類似[犬齒的長牙](../Page/犬齒.md "wikilink")，通常稱為犬齒型牙齒。牠們的頜部前段沒有牙齒，具有堅硬的嘴鞘，可以切斷[植被](../Page/植被.md "wikilink")。[前上頜骨各有三顆牙齒](../Page/前上頜骨.md "wikilink")，前兩顆是圓錐狀牙齒，而第三顆即為大型的長牙，下頜的長牙是[齒骨的第一顆牙齒](../Page/齒骨.md "wikilink")。上頜牙齒間有個大牙縫，分開前上頜骨牙齒與[上頜骨的頰齒](../Page/上頜骨.md "wikilink")，並可容納下頜長牙。下頜也有類似的牙齒縫隙\[3\]。

醒龍通常被認為是畸齒龍科的基礎物種\[4\]\[5\]。[畸齒龍與](../Page/畸齒龍.md "wikilink")[狼鼻龍都具有高齒冠的頰齒](../Page/狼鼻龍.md "wikilink")，並互相重疊，形成咀嚼用齒列，類似[白堊紀鴨嘴龍科的齒列](../Page/白堊紀.md "wikilink")。醒龍的頰齒間隔較寬，齒冠較矮，較類似早期的鳥臀目恐龍。醒龍過去曾被認為缺乏長牙，屬於較原始的特徵\[6\]。然而，兩個醒龍的標本曾發現了這些犬齒型牙齒。上頜的犬齒型牙齒的長度為10.5公分，而下頜的犬齒型牙齒的長度為17公分。這些犬齒型牙齒僅有前側具有鋸齒狀邊緣，而狼鼻龍與畸齒龍的犬齒型牙齒則是前後都有鋸齒狀邊緣\[7\]\[8\]。與畸齒龍相比，醒龍的前肢較小、較不強壯。醒龍的第四、第五指的[掌骨數量較少](../Page/掌骨.md "wikilink")\[9\]。

## 歷史與命名

醒龍的兩個標本目前都存放在[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")。醒龍的[正模標本](../Page/正模標本.md "wikilink")（編號UCL
B54）發現於[賴索托](../Page/賴索托.md "wikilink")，包含一個部分頭顱骨與骨骸。[古生物學家Richard](../Page/古生物學家.md "wikilink")
Thulborn在1914年首次敘述這個標本，他認為該化石是狼鼻龍的一個新種，並密名為伴侶醒龍（*L.
consors*），*consors*在[拉丁語中意為](../Page/拉丁語.md "wikilink")「同伴」或「配偶」。編號UCL
B54標本缺乏狼鼻龍所擁有的犬齒型牙齒，但Thulborn認為這代表該個體是個雌性\[10\]。在Thulborn的首次醒龍研究中，並沒有完全敘述牠們頭顱骨與骨骸。一顆發現於[瑞士的牙齒](../Page/瑞士.md "wikilink")，年代屬於[三疊紀最晚期](../Page/三疊紀.md "wikilink")，被歸類於醒龍。但這顆牙齒沒有醒龍、畸齒龍科、或鳥臀目的特徵，因此沒有被廣泛的接受\[11\]。

在1975年，[詹姆斯·霍普森](../Page/詹姆斯·霍普森.md "wikilink")（James
Hopson）重新敘述了一個發現於[南非的破碎](../Page/南非.md "wikilink")[畸齒龍科頭顱骨](../Page/畸齒龍科.md "wikilink")（編號UCL
A100），該頭顱骨原本被歸類於狼鼻龍\[12\]。霍普森指出該標本並不屬於狼鼻龍，而更為類似編號UCL
B54標本，霍普森建立了新屬，醒龍（*Abrictosaurus*），以包含這兩個標本。在[古希臘文中](../Page/古希臘文.md "wikilink")，*αβρικτος*/*abriktos*意為「不眠的」，而*σαυρος*/*sauros*意為「蜥蜴」，因為霍普森並不同意Thulborn所提出的畸齒龍科會在熱季或旱季[夏眠的](../Page/夏眠.md "wikilink")[假設](../Page/假設.md "wikilink")\[13\]。儘管霍普森將這兩個標本建立了新屬，Thulborn仍認為狹齒狼鼻龍、塔克畸齒龍、以及伴侶狼鼻龍分別為狼鼻龍的三個種。大部分古生物學家主張這三個種是個別的屬，但牠們之間並沒有精確的[古生物學定義](../Page/古生物學.md "wikilink")\[14\]。

## 兩性異形

長久以來，畸齒龍科的[兩性異形假設主要聚集於醒龍身上](../Page/兩性異形.md "wikilink")。許多現代哺乳類的長牙都為[兩性異形的特徵](../Page/兩性異形.md "wikilink")，包括[麝](../Page/麝.md "wikilink")、[海象](../Page/海象.md "wikilink")、[亞洲象](../Page/亞洲象.md "wikilink")、[豬](../Page/豬.md "wikilink")、這些物種的雄性具有明顯的長牙。編號UCL
B54標本缺乏長牙，因此許多人認為該個體是[雌性](../Page/雌性.md "wikilink")、甚至是其他種的雌性個體\[15\]。編號UCL
A100標本的犬齒型牙齒可能是雄性的特徵，並顯示牠們足以建立個別的種。然而，編號UCL
B54標本的短臉部、未癒合的[薦椎](../Page/薦椎.md "wikilink")，顯示牠有可能是個幼年個體。如果屬實，編號UCL
B54標本缺乏長牙，可能是個[次要性徵](../Page/次要性徵.md "wikilink")（secondary sexual
character），而非[兩性異形特徵](../Page/兩性異形.md "wikilink")\[16\]。

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:非洲恐龍](../Category/非洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:畸齒龍科](../Category/畸齒龍科.md "wikilink")

1.  Norman, D.B., Sues, H.-D., Witmer, L.M., & Coria, R.A. 2004. Basal
    Ornithopoda. In: Weishampel, D.B., Dodson, P., & Osmolska, H.
    (Eds.). *The Dinosauria* (2nd edition). Berkeley: University of
    California Press. Pp. 393-412.

2.  Weishampel, D.B. & Witmer, L.M. 1990. Heterodontosauridae. In:
    Weishampel, D.B., Dodson, P., and Osmolska, H. *The Dinosauria* (1st
    edition). Berkeley: University of California Press. Pp.486-497.

3.
4.
5.
6.  Sereno, P.C. 1986. Phylogeny of the bird-hipped dinosaurs (Order
    Ornithischia). *National Geographic Research* 2: 234-256.

7.  Thulborn, R.A. 1970. The systematic position of the Triassic
    ornithischian dinosaur *Lycorhinus angustidens*. *Zoological Journal
    of the Linnean Society* 49: 235-245.

8.  Hopson, J.A. 1975. On the generic separation of the ornithischian
    dinosaurs *Lycorhinus* and *Heterodontosaurus* from the Stormberg
    Series (Upper Triassic) of South Africa. *South African Journal of
    Science* 71: 302-305.

9.  Thulborn, R.A. 1974. A new heterodontosaurid dinosaur (Reptilia:
    Ornithischia) from the Upper Triassic Red Beds of Lesotho.
    *Zoological Journal of the Linnean Society of London.* 55: 151-175.

10.
11.

12.
13.
14.
15.
16.