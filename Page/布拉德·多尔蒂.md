| position =
中锋{{\#tag:ref|[布拉德·多尔迪](http://stat-nba.com/player/948.html)
- 数据NBA|name=nbastat}} | birth_date =  | birth_place =
[北卡罗来纳州Black](../Page/北卡罗来纳州.md "wikilink") Mountain |
height_ft = 7 | height_in = 0 | weight_lb = 245 | highschool =
Charles D. Owen in Swannanoa, [北卡罗来纳州](../Page/北卡罗来纳州.md "wikilink") |
college = [北卡罗来纳大学](../Page/北卡罗来纳大学.md "wikilink") | draft = |
draft_year = 1986 | draft_round = 1 | draft_pick = 1 | draft_team =
[克里夫兰骑士](../Page/克里夫兰骑士.md "wikilink") | career_start = | career_end =
| teams = | coach = | highlights = | stat1label = | stat1value = |
stat2label = | stat2value = | stat3label = | stat3value = | letter = |
bbr = | profile = | HOF = | HOF_player = | HOF_coach = }}
**布拉德利·李·多尔蒂**（，），[美国退役](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，场上位置[中锋](../Page/中锋.md "wikilink")。

多赫蒂大学效力于[北卡罗来纳大学大学](../Page/北卡罗来纳大学教堂山分校.md "wikilink")，他在[1986年NBA选秀中以第一轮第一顺位身份被](../Page/1986年NBA选秀.md "wikilink")[NBA](../Page/NBA.md "wikilink")[克里夫兰骑士队摘得](../Page/克里夫兰骑士队.md "wikilink")。其后8年他一直效力于该队，直到由于严重背伤而早早退役。在其8年的NBA生涯中，多赫蒂5次入选[NBA全明星赛](../Page/NBA全明星赛.md "wikilink")，并曾率队打入东部联盟决赛。他被认为是骑士队历史上最好的球员之一。

## NBA生涯统计

### 常规赛

|- | align="left" | [1986-87](../Page/1986-87_NBA_season.md "wikilink")
| align="left" |
[Cleveland](../Page/1986-87_Cleveland_Cavaliers_season.md "wikilink") |
**80** || **80** || 33.7 || .538 || - || .696 || 8.1 || 3.8 || 0.6 ||
0.8 || 15.7 |- | align="left" |
[1987-88](../Page/1987-88_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1987-88_Cleveland_Cavaliers_season.md "wikilink") |
79 || 78 || 37.4 || .510 || .000 || .716 || 8.4 || 4.2 || 0.6 || 0.7 ||
18.7 |- | align="left" |
[1988-89](../Page/1988-89_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1988-89_Cleveland_Cavaliers_season.md "wikilink") |
78 || 78 || 36.2 || .538 || .333 || .737 || 9.2 || 3.7 || 0.8 || 0.5 ||
18.9 |- | align="left" |
[1989-90](../Page/1989-90_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1989-90_Cleveland_Cavaliers_season.md "wikilink") |
41 || 40 || 35.1 || .479 || .000 || .704 || 9.1 || 3.2 || 0.7 || 0.5 ||
16.8 |- | align="left" |
[1990-91](../Page/1990-91_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1990-91_Cleveland_Cavaliers_season.md "wikilink") |
76 || 76 || **38.8** || .524 || .000 || .751 || **10.9** || 3.3 ||
**1.0** || 0.6 || **21.6** |- | align="left" |
[1991-92](../Page/1991-92_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1991-92_Cleveland_Cavaliers_season.md "wikilink") |
73 || 73 || 36.2 || .570 || .000 || .777 || 10.4 || 3.6 || 0.9 ||
**1.1** || 21.5 |- | align="left" |
[1992-93](../Page/1992-93_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1992-93_Cleveland_Cavaliers_season.md "wikilink") |
71 || 71 || 37.9 || **.571** || **.500** || **.795** || 10.2 || **4.4**
|| 0.7 || 0.8 || 20.2 |- | align="left" |
[1993-94](../Page/1993-94_NBA_season.md "wikilink") | align="left" |
[Cleveland](../Page/1993-94_Cleveland_Cavaliers_season.md "wikilink") |
50 || 50 || 36.8 || .488 || - || .785 || 10.2 || 3.0 || 0.8 || 0.7 ||
17.0

|- class=sortbottom | style="text-align:center;" colspan=2| Career | 548
|| 546 || 36.5 || .532 || .143 || .747 || 9.5 || 3.7 || 0.8 || 0.7 ||
19.0 |-

### 季后赛

|-
|style="text-align:left;"|[1988](../Page/1988_NBA_Playoffs.md "wikilink")
|style="text-align:left;"|[Cleveland](../Page/1987–88_Cleveland_Cavaliers_season.md "wikilink")
|5||5||**40.8**||.460||-||.677||9.2||3.2||0.4||**1.4**||15.8 |-
|style="text-align:left;"|[1989](../Page/1989_NBA_Playoffs.md "wikilink")
|style="text-align:left;"|[Cleveland](../Page/1988–89_Cleveland_Cavaliers_season.md "wikilink")
|5||5||33.4||.362||.000||.600||9.2||2.4||**1.2**||1.0||11.0 |-
|style="text-align:left;"|[1990](../Page/1990_NBA_Playoffs.md "wikilink")
|style="text-align:left;"|[Cleveland](../Page/1989–90_Cleveland_Cavaliers_season.md "wikilink")
|5||5||37.2||**.586**||-||.696||9.6||**4.0**||0.4||0.8||**22.8** |-
|style="text-align:left;"|[1992](../Page/1992_NBA_Playoffs.md "wikilink")
|style="text-align:left;"|[Cleveland](../Page/1991–92_Cleveland_Cavaliers_season.md "wikilink")
|**17**||**17**||40.4||.528||.000||**.814**||10.2||3.4||0.6||1.0||21.5
|-
|style="text-align:left;"|[1993](../Page/1993_NBA_Playoffs.md "wikilink")
|style="text-align:left;"|[Cleveland](../Page/1992–93_Cleveland_Cavaliers_season.md "wikilink")
|9||9||39.6||.557||-||.800||**11.7**||3.4||0.7||0.8||18.7 |- |-
class="sortbottom" | style="text-align:center;" colspan="2" | Career
|41||41||39.0||.519||.000||.756||10.2||3.3||0.7||1.0||19.1 |}

## 参考资料

## 外部链接

  - [NBA.com资料](http://www.nba.com/cavaliers/history/brad_daugherty.html)
  - [统计数据](http://www.basketball-reference.com/players/d/daughbr01.html)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:克里夫蘭騎士隊球員](../Category/克里夫蘭騎士隊球員.md "wikilink")
[Category:NBA退休背號球员](../Category/NBA退休背號球员.md "wikilink")