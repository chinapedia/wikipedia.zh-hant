**马尾区**（[闽东语](../Page/闽东语.md "wikilink")：*Mā-muōi*）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[福州市所辖的一个区](../Page/福州市.md "wikilink")。面积254平方千米，人口15万。邮政编码350015。区人民政府驻[君竹路](../Page/君竹路.md "wikilink")。

## 行政区划

辖1个街道、3个镇：

  - 街道办事处：[罗星街道](../Page/罗星街道_\(福州市\).md "wikilink")。
  - 镇：[马尾镇](../Page/马尾镇.md "wikilink")、[亭江镇](../Page/亭江镇.md "wikilink")、[琅岐镇](../Page/琅岐镇.md "wikilink")。

## 经济

  - [马尾造船厂](../Page/马尾造船厂.md "wikilink")
  - 清錄鞋業股份有限公司
  - [福州经济技术开发区](../Page/中国（福建）自由贸易试验区.md "wikilink")

## 文化

马尾区的马尾－马祖元宵节俗被列入[国家级非物质文化遗产名录](../Page/国家级非物质文化遗产名录.md "wikilink")。

## 教育

马尾区的教育机构有[阳光学院](../Page/阳光学院.md "wikilink")、[福建船政交通职业学院](../Page/福建船政交通职业学院.md "wikilink")、[福建农林大学东方学院](../Page/福建农林大学东方学院.md "wikilink")、[福建师范大学第二附属中学](../Page/福建师范大学第二附属中学.md "wikilink")、阳光国际学校和亭江中学。

## 景点

  - 罗星塔
  - 昭忠祠
  - 中国船政文化博物馆

## 外部链接

  - [福州市马尾区人民政府门户网站](http://www.mawei.gov.cn/)

  - [马尾区志](http://www.fzdqw.com/ShowBook.asp?BookType=%B8%A3%D6%DD%CA%D0_\(%CF%D8\)%CA%D0%C7%F8%D6%BE&Bookno=1117)

## 参考文献

{{-}}

[马尾区](../Category/马尾区.md "wikilink")
[区](../Category/福州区县市.md "wikilink")
[福州](../Category/福建市辖区.md "wikilink")