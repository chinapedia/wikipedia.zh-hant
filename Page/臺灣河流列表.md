**[台灣](../Page/台灣.md "wikilink")**本島的[分水嶺](../Page/分水嶺.md "wikilink")（[中央山脈與](../Page/中央山脈.md "wikilink")[雪山山脈](../Page/雪山山脈.md "wikilink")）主要偏東，故**[河川](../Page/河流.md "wikilink")**東短西長。又因南部的冬夏季雨量較為不平均，故南部在冬季時經常因降雨量過少造成枯水期，常形成「野溪」，不適合航行。一般而言，台灣的河川具有下列特徵：河身短、坡度大、水流急，最長的[濁水溪長僅](../Page/濁水溪.md "wikilink")186[公里](../Page/公里.md "wikilink")，而[坡度則達四十六分之一](../Page/坡度.md "wikilink")。洪峰流量十分龐大，面積兩三千[平方公里的集水區域](../Page/平方公里.md "wikilink")，經常出現每秒一萬[立方公尺以上的洪水量](../Page/立方公尺.md "wikilink")。河流含沙量大，如濁水溪因而得名。[河階](../Page/河階.md "wikilink")、谷中谷、嵌入[曲流](../Page/曲流.md "wikilink")、隆起[沖積扇等](../Page/沖積扇.md "wikilink")[回春地形顯著](../Page/回春地形.md "wikilink")。

目前台灣河川的主管單位為[中華民國](../Page/中華民國.md "wikilink")[經濟部水利署](../Page/經濟部水利署.md "wikilink")，其列管河川分為[中央管河川](../Page/中央管河川.md "wikilink")、跨省市河川及[縣（市）管河川三級](../Page/縣市管河川.md "wikilink")，包括中央管河川26水系、跨省市河川2水系（[淡水河](../Page/淡水河.md "wikilink")、[磺溪](../Page/磺溪_\(新北市\).md "wikilink")）及縣（市）管河川92水系（其中2水系為其他水系支流），合計列管116獨立水系\[1\]。而地圖上繪出河道且標示名稱但未列管，或是被歸屬至「排水」體系的河川，則略大於此數。至於地圖上未標示名稱或未繪出者，則難以計數。

以下依出海口位置[逆時針順序列出全台灣各行政區獨立水系](../Page/逆時針.md "wikilink")（含較長支流）及其出海口兩側行政區\[2\]。

## 宜蘭縣

  - [和平溪](../Page/和平溪.md "wikilink")（大濁水溪）：[花蓮縣](../Page/花蓮縣.md "wikilink")[秀林鄉](../Page/秀林鄉.md "wikilink")、宜蘭縣[南澳鄉](../Page/南澳鄉.md "wikilink")
  - [南澳溪](../Page/南澳溪.md "wikilink")：蘇澳鎮
      - [南澳南溪](../Page/南澳南溪.md "wikilink")
  - [大灣溪](../Page/大灣溪.md "wikilink")：蘇澳鎮
  - [東澳南溪](../Page/東澳南溪.md "wikilink")：蘇澳鎮
  - [東澳溪](../Page/東澳溪.md "wikilink")（東澳北溪）：蘇澳鎮
  - [蘇澳溪](../Page/蘇澳溪.md "wikilink")：蘇澳鎮
  - [新城溪](../Page/新城溪.md "wikilink")：蘇澳鎮
  - [蘭陽溪](../Page/蘭陽溪.md "wikilink")：[五結鄉](../Page/五結鄉.md "wikilink")、[壯圍鄉](../Page/壯圍鄉.md "wikilink")
      - [宜蘭河](../Page/宜蘭河.md "wikilink")
      - [冬山河](../Page/冬山河.md "wikilink")
      - [清水溪](../Page/清水溪_\(宜蘭縣\).md "wikilink")
      - [羅東溪](../Page/羅東溪.md "wikilink")
  - [得子口溪](../Page/得子口溪.md "wikilink")（竹安溪）：頭城鎮
  - [梗枋溪](../Page/梗枋溪.md "wikilink")：頭城鎮
  - [石壁溪](../Page/石壁溪.md "wikilink")：頭城鎮
  - [大溪川](../Page/大溪川.md "wikilink")：頭城鎮
  - [蕃薯溪](../Page/蕃薯溪.md "wikilink")：頭城鎮
  - [石門溪](../Page/石門溪.md "wikilink")：頭城鎮

## 新北市（東段）

  - [巷內坑溪](../Page/巷內坑溪.md "wikilink")：貢寮區
  - [屠宰坑溪](../Page/屠宰坑溪.md "wikilink")：貢寮區
  - [坑內溪](../Page/坑內溪.md "wikilink")：貢寮區
  - [雙溪](../Page/雙溪_\(新北市\).md "wikilink")：貢寮區
  - [鹽寮溪](../Page/鹽寮溪.md "wikilink")：貢寮區
  - [尖山腳溪](../Page/尖山腳溪.md "wikilink")（石碇溪）：貢寮區
  - [南勢坑溪](../Page/南勢坑溪.md "wikilink")：貢寮區
  - [北勢坑溪](../Page/北勢坑溪.md "wikilink")：貢寮區
  - [半平溪](../Page/半平溪.md "wikilink")：瑞芳區
  - [茶壼溪](../Page/茶壼溪.md "wikilink")：瑞芳區

## 基隆市

  - [碧砂溪](../Page/碧砂溪.md "wikilink")：中正區
  - [田寮河](../Page/田寮河.md "wikilink")（田寮港）：信義區、仁愛區
  - [旭川河](../Page/旭川河.md "wikilink")：仁愛區
  - [牛稠河](../Page/牛稠河.md "wikilink")（牛稠港）：中山區

## 新北市（西段）

  - [瑪鋉溪](../Page/瑪鋉溪.md "wikilink")：萬里區
  - [員潭溪](../Page/員潭溪.md "wikilink")：金山區（近萬里區）
  - [磺溪](../Page/磺溪_\(新北市\).md "wikilink")：金山區
  - [小坑溪](../Page/小坑溪_\(石門區\).md "wikilink")：石門區
  - [乾華溪](../Page/乾華溪.md "wikilink")：石門區
  - [尖子鹿溪](../Page/尖子鹿溪.md "wikilink")：石門區
  - [石門溪](../Page/石門溪.md "wikilink")（石門坑溪）：石門區
  - [老梅溪](../Page/老梅溪.md "wikilink")：石門區
  - [小坑子溪](../Page/小坑子溪.md "wikilink")：石門區
  - [楓林溪](../Page/楓林溪.md "wikilink")：石門區
  - [崁子腳溪](../Page/崁子腳溪.md "wikilink")：石門區
  - [八甲溪](../Page/八甲溪.md "wikilink")：石門區
  - [下員坑溪](../Page/下員坑溪.md "wikilink")：石門區
  - [埔坪溪](../Page/埔坪溪.md "wikilink")：三芝區
  - [八蓮溪](../Page/八連溪_\(三芝區\).md "wikilink")（八連溪）：三芝區
  - [山豬堀下坑溪](../Page/山豬堀下坑溪.md "wikilink")：三芝區
  - [南勢坑溪](../Page/南勢坑溪.md "wikilink")：三芝區
  - [海尾溪](../Page/海尾溪.md "wikilink")：三芝區
  - [北勢溪](../Page/北勢溪_\(三芝區\).md "wikilink")：三芝區
  - [後厝溪](../Page/後厝溪.md "wikilink")：三芝區
  - [大屯溪](../Page/大屯溪.md "wikilink")：淡水區
  - [灰磘子溪](../Page/灰磘子溪.md "wikilink")：淡水區
  - [後洲溪](../Page/後洲溪.md "wikilink")：淡水區
  - [興仁溪](../Page/興仁溪.md "wikilink")：淡水區
  - [下圭柔山溪](../Page/下圭柔山溪.md "wikilink")：淡水區
  - [林子溪](../Page/林子溪.md "wikilink")（公司田溪）：淡水區
  - [淡水河](../Page/淡水河.md "wikilink")：淡水區、八里區
      - [基隆河](../Page/基隆河.md "wikilink")
      - [新店溪](../Page/新店溪.md "wikilink")
          - [景美溪](../Page/景美溪.md "wikilink")
          - [南勢溪](../Page/南勢溪.md "wikilink")
          - [北勢溪](../Page/北勢溪_\(新北市\).md "wikilink")
      - [大漢溪](../Page/大漢溪.md "wikilink")（大嵙崁溪）
          - [三峽河](../Page/三峽河.md "wikilink")
  - [水仙溪](../Page/紅水仙溪.md "wikilink")：八里區
  - [後坑溪](../Page/後坑溪.md "wikilink")：林口區
  - [瑞樹坑溪](../Page/瑞樹坑溪.md "wikilink")：林口區
  - [寶斗溪](../Page/寶斗溪.md "wikilink")：林口區
  - [嘉寶溪](../Page/嘉寶溪.md "wikilink")：林口區
  - [林口溪](../Page/林口溪.md "wikilink")：林口區
  - [出水坑溪](../Page/出水坑溪.md "wikilink")：林口區

## 桃園市

  - [南崁溪](../Page/南崁溪.md "wikilink")：蘆竹區、大園區
  - [埔心溪](../Page/埔心溪.md "wikilink")：大園區
  - [新街溪](../Page/新街溪.md "wikilink")：大園區
  - [老街溪](../Page/老街溪.md "wikilink")：大園區
  - [富林溪](../Page/富林溪.md "wikilink")：觀音區
  - [大堀溪](../Page/大堀溪.md "wikilink")：觀音區
  - [觀音溪](../Page/觀音溪.md "wikilink")：觀音區
  - [小飯壢溪](../Page/小飯壢溪.md "wikilink")：觀音區
  - [新屋溪](../Page/新屋溪.md "wikilink")：觀音區、新屋區
  - [後湖溪](../Page/後湖溪.md "wikilink")：新屋區
  - [社子溪](../Page/社子溪.md "wikilink")（社仔溪）：新屋區
  - [大坡溪](../Page/大坡溪.md "wikilink")：新屋區
  - [羊寮溪](../Page/羊寮溪.md "wikilink")（福興溪）：新屋區、新竹縣新豐鄉

## 新竹縣

  - [羊寮溪](../Page/羊寮溪.md "wikilink")（福興溪）：桃園市新屋區、新竹縣新豐鄉
  - [新豐溪](../Page/新豐溪.md "wikilink")：新豐鄉
  - [鳳山溪](../Page/鳳山溪.md "wikilink")：竹北市
  - [頭前溪](../Page/頭前溪.md "wikilink")：竹北市、新竹市北區
      - [油羅溪](../Page/油羅溪.md "wikilink")
      - [上坪溪](../Page/上坪溪.md "wikilink")

## 新竹市

  - [頭前溪](../Page/頭前溪.md "wikilink")：新竹縣竹北市、新竹市北區
  - [客雅溪](../Page/客雅溪.md "wikilink")：香山區
  - [三姓溪](../Page/三姓溪.md "wikilink")：香山區
  - [汫水港溪](../Page/海水川溪.md "wikilink")：香山區
  - [鹽港溪](../Page/鹽港溪.md "wikilink")：香山區

## 苗栗縣

  - [冷水坑溪](../Page/冷水坑溪.md "wikilink")：竹南鎮
  - [中港溪](../Page/中港溪.md "wikilink")：竹南鎮、後龍鎮
      - [南港溪](../Page/南港溪_\(苗栗縣\).md "wikilink")
      - [峨眉溪](../Page/峨眉溪.md "wikilink")
  - [後龍溪](../Page/後龍溪.md "wikilink")：後龍鎮
      - [老田寮溪](../Page/老田寮溪.md "wikilink")
  - [西湖溪](../Page/西湖溪.md "wikilink")（打八哪溪）：後龍鎮
  - [過溝溪](../Page/過溝溪.md "wikilink")：後龍鎮、通霄鎮
  - [內島溪](../Page/內島溪.md "wikilink")：通霄鎮
  - [通霄溪](../Page/通霄溪.md "wikilink")：通霄鎮
  - [番仔寮溪](../Page/番仔寮溪.md "wikilink")：通霄鎮
  - [苑裡溪](../Page/苑裡溪.md "wikilink")（大埔溪）：通霄鎮、苑裡鎮
  - [房裡溪](../Page/房裡溪.md "wikilink")：苑裡鎮

## 臺中市

  - [四好橋構](../Page/四好橋構.md "wikilink")：大甲區
  - [大安溪](../Page/大安溪.md "wikilink")：大甲區、大安區
  - [溫寮溪](../Page/溫寮溪.md "wikilink")：大安區
  - [龜売溪](../Page/龜売溪.md "wikilink")：大安區
  - [南庄溪](../Page/南庄溪.md "wikilink")：大安區
  - [南埔溪](../Page/南埔溪.md "wikilink")：大安區
  - [大甲溪](../Page/大甲溪.md "wikilink")：大安區、清水區
      - [志樂溪](../Page/志樂溪.md "wikilink")
      - [南湖溪](../Page/南湖溪.md "wikilink")
          - [合歡溪](../Page/合歡溪.md "wikilink")
  - [清水大排水溝](../Page/清水大排水溝.md "wikilink")（米粉寮溪）：清水區
  - [安良港大排](../Page/安良港大排.md "wikilink")（南勢溪）：清水區
  - [烏溪](../Page/烏溪.md "wikilink")（大肚溪）：龍井區、彰化縣伸港鄉
      - [貓羅溪](../Page/貓羅溪.md "wikilink")
      - [南港溪](../Page/南港溪_\(南投縣\).md "wikilink")
          - [眉溪](../Page/眉溪.md "wikilink")

## 彰化縣

  - [烏溪](../Page/烏溪.md "wikilink")（大肚溪）：台中市龍井區、彰化縣伸港鄉
  - [番雅溝](../Page/番雅溝.md "wikilink")：線西鄉、鹿港鎮
  - [洋仔厝溪](../Page/洋仔厝溪.md "wikilink")（洋子溪）：鹿港鎮
  - [員林大排](../Page/員林大排.md "wikilink")：鹿港鎮、福興鄉
  - [東螺溪](../Page/東螺溪.md "wikilink")（舊濁水溪）：福興鄉
  - [漢寶溪](../Page/漢寶溪.md "wikilink")：福興鄉、芳苑鄉
  - [後港溪](../Page/後港溪.md "wikilink")：芳苑鄉
  - [二林溪](../Page/二林溪.md "wikilink")：芳苑鄉
  - [魚寮溪](../Page/魚寮溪.md "wikilink")：大城鄉
  - [濁水溪](../Page/濁水溪.md "wikilink")：大城鄉、雲林縣麥寮鄉
      - [清水溪](../Page/清水溪_\(南投縣\).md "wikilink")
          - [加走寮溪](../Page/加走寮溪.md "wikilink")
      - [水里溪](../Page/水里溪.md "wikilink")
      - [陳有蘭溪](../Page/陳有蘭溪.md "wikilink")
      - [丹大溪](../Page/丹大溪.md "wikilink")
          - [郡大溪](../Page/郡大溪.md "wikilink")
      - [卡社溪](../Page/卡社溪.md "wikilink")
      - [萬大溪](../Page/萬大溪.md "wikilink")

## 雲林縣

  - [濁水溪](../Page/濁水溪.md "wikilink")：彰化縣大城鄉、雲林縣麥寮鄉
  - [新虎尾溪](../Page/新虎尾溪.md "wikilink")：麥寮鄉、臺西鄉
  - [舊虎尾溪](../Page/舊虎尾溪.md "wikilink")：臺西鄉、四湖鄉
  - [牛挑灣溪](../Page/牛挑灣溪.md "wikilink")：口湖鄉
  - [北港溪](../Page/北港溪.md "wikilink")：口湖鄉、嘉義縣東石鄉
      - [大湖口溪](../Page/大湖口溪.md "wikilink")
      - [三疊溪](../Page/三疊溪.md "wikilink")
      - [石牛溪](../Page/石牛溪.md "wikilink")
      - [石龜溪](../Page/石龜溪.md "wikilink")

## 嘉義縣

  - [北港溪](../Page/北港溪_\(雲林縣\).md "wikilink")：雲林縣口湖鄉、嘉義縣東石鄉
  - [朴子溪](../Page/朴子溪.md "wikilink")：東石鄉
      - [牛稠溪](../Page/牛稠溪.md "wikilink")
  - [龍宮溪](../Page/龍宮溪.md "wikilink")：布袋鎮
  - [八掌溪](../Page/八掌溪.md "wikilink")：布袋鎮、臺南市北門區
      - [頭前溪](../Page/頭前溪_\(嘉義縣\).md "wikilink")

## 臺南市

  - [八掌溪](../Page/八掌溪.md "wikilink")：嘉義縣布袋鎮、臺南市北門區
  - [急水溪](../Page/急水溪.md "wikilink")：北門區
      - [龜重溪](../Page/龜重溪.md "wikilink")
      - [白水溪](../Page/白水溪.md "wikilink")
  - [將軍溪](../Page/將軍溪.md "wikilink")：北門區、將軍區
  - [七股溪](../Page/七股溪.md "wikilink")：七股區
  - [曾文溪](../Page/曾文溪.md "wikilink")：七股區、安南區
      - [菜寮溪](../Page/菜寮溪.md "wikilink")
      - [后堀溪](../Page/后堀溪.md "wikilink")
  - [鹿耳門溪](../Page/鹿耳門溪.md "wikilink")：安南區
  - [鹽水溪](../Page/鹽水溪.md "wikilink")：安南區、安平區
  - [二仁溪](../Page/二仁溪.md "wikilink")：南區、高雄市茄萣區

## 高雄市

  - [二仁溪](../Page/二仁溪.md "wikilink")：臺南市南區、高雄市茄萣區
  - [阿公店溪](../Page/阿公店溪.md "wikilink")：永安區、彌陀區
  - [典寶溪](../Page/典寶溪.md "wikilink")：梓官區、楠梓區
  - [後勁溪](../Page/後勁溪.md "wikilink")：楠梓區
  - [愛河](../Page/愛河.md "wikilink")：鼓山區、苓雅區
  - [前鎮河](../Page/鳳山溪_\(高雄市\).md "wikilink")：前鎮區
  - [高屏溪](../Page/高屏溪.md "wikilink")：林園區、屏東縣新園鄉
      - [旗山溪](../Page/旗山溪.md "wikilink")
          - [美濃溪](../Page/美濃溪.md "wikilink")
      - [荖濃溪](../Page/荖濃溪.md "wikilink")
          - [隘寮溪](../Page/隘寮溪.md "wikilink")
              - [社口溪](../Page/社口溪.md "wikilink")
          - [濁口溪](../Page/濁口溪.md "wikilink")
          - [寶來溪](../Page/寶來溪.md "wikilink")
          - [拉庫音溪](../Page/拉庫音溪.md "wikilink")

## 屏東縣

  - [高屏溪](../Page/高屏溪.md "wikilink")：高雄市林園區、屏東縣新園鄉
      - [牛稠溪](../Page/牛稠溪.md "wikilink")
      - [武洛溪](../Page/武洛溪.md "wikilink")
      - [荖濃溪](../Page/荖濃溪.md "wikilink")
  - [東港溪](../Page/東港溪.md "wikilink")：新園鄉、東港鎮
  - [林邊溪](../Page/林邊溪.md "wikilink")：林邊鄉、佳冬鄉
  - [內寮溪](../Page/內寮溪.md "wikilink")：枋寮鄉
  - [率芒溪](../Page/率芒溪.md "wikilink")（士文溪）：枋寮鄉、枋山鄉
  - [南勢湖溪](../Page/南勢湖溪.md "wikilink")：枋山鄉
  - [七里溪](../Page/七里溪.md "wikilink")：枋山鄉
  - [枋山溪](../Page/枋山溪.md "wikilink")：枋山鄉
  - [獅子頭溪](../Page/獅子頭溪.md "wikilink")：枋山鄉
  - [楓港溪](../Page/楓港溪.md "wikilink")：枋山鄉
  - [大石盤溪](../Page/大石盤溪.md "wikilink")（石盤溪）：枋山鄉
  - [竹坑溪](../Page/竹坑溪.md "wikilink")：枋山鄉、車城鄉
  - [社皆坑溪](../Page/社皆坑溪.md "wikilink")：車城鄉
  - [四重溪](../Page/四重溪.md "wikilink")：車城鄉
  - [保力溪](../Page/保力溪.md "wikilink")：車城鄉
  - [石牛溪](../Page/石牛溪.md "wikilink")：恆春鎮
  - [刣牛溪](../Page/刣牛溪.md "wikilink")：恆春鎮
  - [港口溪](../Page/港口溪.md "wikilink")：滿州鄉
  - [溪仔口溪](../Page/溪仔口溪.md "wikilink")：滿州鄉
  - [鹿寮溪](../Page/鹿寮溪_\(屏東縣\).md "wikilink")：滿州鄉
  - [埤日溪](../Page/埤日溪.md "wikilink")：滿州鄉
  - [萬丈深坑溪](../Page/萬丈深坑溪.md "wikilink")：滿州鄉
  - [無水溪](../Page/無水溪.md "wikilink")：滿州鄉
  - [南仁鬱溪](../Page/南仁鬱溪.md "wikilink")：滿州鄉
  - [九棚溪](../Page/九棚溪.md "wikilink")：滿州鄉
  - [港子溪](../Page/港子溪.md "wikilink")：滿州鄉
  - [大流溪](../Page/大流溪.md "wikilink")：牡丹鄉
  - [旭海溪](../Page/旭海溪.md "wikilink")：牡丹鄉
  - [乾溪](../Page/乾溪.md "wikilink")：牡丹鄉
  - [塔瓦溪](../Page/塔瓦溪.md "wikilink")：牡丹鄉、臺東縣達仁鄉

## 臺東縣

  - [塔瓦溪](../Page/塔瓦溪.md "wikilink")：屏東縣牡丹鄉、臺東縣達仁鄉
  - [達仁溪](../Page/達仁溪.md "wikilink")：達仁鄉
  - [安朔溪](../Page/安朔溪.md "wikilink")：達仁鄉、大武鄉
  - [南興溪](../Page/南興溪.md "wikilink")：大武鄉
  - [朝庸溪](../Page/朝庸溪.md "wikilink")：大武鄉
  - [大武溪](../Page/大武溪.md "wikilink")：大武鄉
  - [烏萬溪](../Page/烏萬溪.md "wikilink")（大鳥溪）：大武鄉
  - [津林溪](../Page/津林溪.md "wikilink")（加津林溪）：大武鄉
  - [大竹溪](../Page/大竹溪.md "wikilink")：大武鄉、太麻里鄉
  - [拉布拉溪](../Page/拉布拉溪.md "wikilink")：太麻里鄉
  - [金崙溪](../Page/金崙溪.md "wikilink")：太麻里鄉
  - [太麻里溪](../Page/太麻里溪.md "wikilink")：太麻里鄉
  - [文里溪](../Page/文里溪.md "wikilink")（北太麻里溪）：太麻里鄉
  - [知本溪](../Page/知本溪.md "wikilink")：太麻里鄉、臺東市
  - [利嘉溪](../Page/利嘉溪.md "wikilink")：臺東市
  - [太平溪](../Page/太平溪.md "wikilink")：臺東市
  - [卑南溪](../Page/卑南溪.md "wikilink")：臺東市
      - [鹿野溪](../Page/鹿野溪.md "wikilink")
      - [鹿寮溪](../Page/鹿寮溪_\(臺東縣\).md "wikilink")
      - [新武呂溪](../Page/新武呂溪.md "wikilink")
  - [伽溪](../Page/伽溪.md "wikilink")：卑南鄉
  - [都蘭溪](../Page/都蘭溪.md "wikilink")：東河鄉
  - [八里溪](../Page/八里溪.md "wikilink")（里鄉）：東河鄉
  - [馬武窟溪](../Page/馬武窟溪.md "wikilink")：東河鄉、成功鎮
  - [半屏溪](../Page/半屏溪.md "wikilink")：成功鎮
  - [都歷溪](../Page/都歷溪.md "wikilink")：成功鎮
  - [成功溪](../Page/新港溪.md "wikilink")（新港溪）：成功鎮
  - [富家溪](../Page/富家溪.md "wikilink")：成功鎮
  - [海老溪](../Page/海老溪.md "wikilink")：成功鎮
  - [都威溪](../Page/都威溪.md "wikilink")：成功鎮
  - [沙灣溪](../Page/沙灣溪.md "wikilink")（大濱溪）：成功鎮
  - [堺橋溪](../Page/堺橋溪.md "wikilink")：成功鎮、長濱鄉
  - [胆月曼溪](../Page/胆月曼溪.md "wikilink")：長濱鄉
  - [南石寧埔溪](../Page/南石寧埔溪.md "wikilink")：長濱鄉
  - [寧埔溪](../Page/寧埔溪.md "wikilink")：長濱鄉
  - [僅那鹿角溪](../Page/僅那鹿角溪.md "wikilink")：長濱鄉
  - [彭仔存溪](../Page/彭仔存溪.md "wikilink")：長濱鄉
  - [竹湖溪](../Page/竹湖溪.md "wikilink")（石門溪）：長濱鄉
  - [中賓溪](../Page/中賓溪.md "wikilink")：長濱鄉
  - [南掃別溪](../Page/南掃別溪.md "wikilink")：長濱鄉
  - [掃別溪](../Page/掃別溪.md "wikilink")：長濱鄉
  - [大德溪](../Page/大德溪.md "wikilink")：長濱鄉
  - [石坑溪](../Page/石坑溪.md "wikilink")：長濱鄉
  - [長濱溪](../Page/長濱溪.md "wikilink")：長濱鄉
  - [城埔溪](../Page/城埔溪.md "wikilink")：長濱鄉
  - [馬海溪](../Page/真柄溪.md "wikilink")（真柄溪）：長濱鄉
  - [山間溪](../Page/山間溪.md "wikilink")（三間溪、凸鼻溪）：長濱鄉
  - [水母溪](../Page/水母溪.md "wikilink")（水母丁溪）：長濱鄉
  - [大峰峰溪](../Page/大峰峰溪.md "wikilink")：長濱鄉

## 花蓮縣

  - [三富溪](../Page/三富溪.md "wikilink")：豐濱鄉
  - [秀姑巒溪](../Page/秀姑巒溪.md "wikilink") -
    [樂樂溪](../Page/樂樂溪.md "wikilink") -
    [馬霍拉斯溪](../Page/馬霍拉斯溪.md "wikilink")\[3\]：豐濱鄉
      - [秀姑巒溪](../Page/秀姑巒溪.md "wikilink")
      - [富源溪](../Page/富源溪_\(花蓮縣\).md "wikilink")
      - [豐坪溪](../Page/豐坪溪.md "wikilink")
      - [樂樂溪 (拉庫拉庫溪)](../Page/樂樂溪.md "wikilink")
          - [清水溪](../Page/清水溪_\(花蓮縣卓溪鄉\).md "wikilink")
  - [石梯港溪](../Page/石梯港溪.md "wikilink")：豐濱鄉
  - [拉濫溪](../Page/拉濫溪.md "wikilink")：豐濱鄉
  - [起拉嚕溪](../Page/起拉嚕溪.md "wikilink")：豐濱鄉
  - [豐濱溪](../Page/豐濱溪.md "wikilink")：豐濱鄉
  - [新莊溪](../Page/新莊溪.md "wikilink")：豐濱鄉
  - [加塱溪](../Page/加塱溪.md "wikilink")：豐濱鄉
  - [加蘭溪](../Page/加蘭溪.md "wikilink")（加路蘭溪）：豐濱鄉
  - [蕃薯寮溪](../Page/薯寮溪.md "wikilink")（薯寮溪）：豐濱鄉、壽豐鄉
  - [水連溪](../Page/水連溪.md "wikilink")：壽豐鄉
  - [花蓮](../Page/花蓮溪.md "wikilink") -
    [萬里溪](../Page/萬里溪.md "wikilink")\[4\]：壽豐鄉、吉安鄉
      - [花蓮溪](../Page/花蓮溪.md "wikilink")
      - [木瓜溪](../Page/木瓜溪.md "wikilink")
      - [壽豐溪](../Page/壽豐溪.md "wikilink")
      - [萬里溪](../Page/萬里溪.md "wikilink")
      - [馬鞍溪](../Page/馬鞍溪.md "wikilink")
  - [吉安溪](../Page/吉安溪.md "wikilink")：吉安鄉、花蓮市
  - [美崙溪](../Page/美崙溪.md "wikilink")：花蓮市
  - [三棧溪](../Page/三棧溪.md "wikilink")：新城鄉
  - [立霧溪](../Page/立霧溪.md "wikilink")：新城鄉、秀林鄉
      - [砂卡礑溪](../Page/砂卡礑溪.md "wikilink")
      - [大沙溪](../Page/大沙溪.md "wikilink")
  - [石公溪](../Page/石公溪.md "wikilink")：秀林鄉
  - [大富溪](../Page/大富溪.md "wikilink")：秀林鄉
  - [清水溪](../Page/清水溪_\(花蓮縣秀林鄉\).md "wikilink")：秀林鄉
  - [大清水溪](../Page/良里溪.md "wikilink")（卡那剛溪、大清水溪）：秀林鄉
  - [和平溪](../Page/和平溪.md "wikilink")（大濁水溪）：秀林鄉、宜蘭縣南澳鄉
      - [和平北溪](../Page/和平北溪.md "wikilink")

## 相關條目

  - [台灣河流長度列表](../Page/台灣河流長度列表.md "wikilink")
  - [中央管河川](../Page/中央管河川.md "wikilink")
  - [縣市管河川](../Page/縣市管河川.md "wikilink")

## 註釋

<div class="references-small">

<references />

</div>

## 參考資料

  - 傅金福，[河川之美系列-
    秀姑巒溪之卷(pdf)](https://web.archive.org/web/20061103040001/http://www.wcis.itri.org.tw/Upload/QUART1/000207/22-4.pdf)，漢光文化

<!-- end list -->

  - 宋定莉，[台灣水經--巡禮台灣中部南部東部河川](https://drive.google.com/drive/folders/1ir6ClTQZjzmnyeUF-QB9kQou1coIPFAg)

## 外部連結

  - [e河川](https://web.archive.org/web/20091113072011/http://www.e-river.tw/)
  - [經濟部水利署-讓我們看河去](https://web.archive.org/web/20070629183947/http://www.wra.gov.tw/ct.asp?xItem=14298&CtNode=4347)
  - [2005年台灣年鑑：台灣河流長度及流域面積](https://web.archive.org/web/20051207223712/http://www.gov.tw/EBOOKS/TWANNUAL/show_book.php?path=3_001_006)

{{-}}

[台灣河流列表](../Category/台灣河流列表.md "wikilink")
[Category:各國河流列表](../Category/各國河流列表.md "wikilink")

1.  [《公告河川區分為中央管河川、跨省市河川及縣（市）管河川》，經濟部公告，行政院公報資訊網](http://gazette.nat.gov.tw/EG_FileManager/eguploadpub/eg015068/ch04/type3/gov31/num9/Eg.htm)
2.  《台灣走透透地圖王》，戶外生活，2007年3月
3.  秀姑巒溪本身發源於花蓮與台東兩縣之間的[崙天山南側](../Page/崙天山.md "wikilink")，但整個水系的最遠源流則為其最長支流樂樂溪的支流[馬霍拉斯溪](../Page/馬霍拉斯溪.md "wikilink")，發源於[中央山脈的](../Page/中央山脈.md "wikilink")[馬博拉斯山東側](../Page/馬博拉斯山.md "wikilink")。
4.  花蓮溪本身源流為[光復溪](../Page/光復溪.md "wikilink")，發源於[拔子山](../Page/拔子山.md "wikilink")，但整個水系的最遠源頭則為其最長支流萬里溪的源頭，位於中央山脈[白石山南側的](../Page/白石山.md "wikilink")[萬里池](../Page/萬里池.md "wikilink")。