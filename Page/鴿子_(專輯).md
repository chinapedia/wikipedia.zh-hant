《**鴿子**》是[臺灣歌手](../Page/臺灣.md "wikilink")[楊宗緯個人第一張專輯](../Page/楊宗緯.md "wikilink")。由[周佳佑](../Page/周佳佑.md "wikilink")、[黃韻玲](../Page/黃韻玲.md "wikilink")、[林邁可](../Page/林邁可.md "wikilink")、[五月天的阿信](../Page/五月天.md "wikilink")、[蕭煌奇](../Page/蕭煌奇.md "wikilink")、[蔡健雅](../Page/蔡健雅.md "wikilink")、[李偉菘等知名音樂人](../Page/李偉菘.md "wikilink")，聯手打造多元曲風的全新創作專輯。這張專輯從2007年7月分開始籌劃，多位製作人稱道楊宗緯的唱功了得，錄製工作進行非常順利，僅僅3星期就完成。

首波預購主打《鴿子》，概念源自於[楊宗緯的想法](../Page/楊宗緯.md "wikilink")，他表示：「這一路的過程，我最想感謝的是我的『鴿迷』，無論發生任何事情，他們始終對我不離不棄。」

## 曲目

<table>
<thead>
<tr class="header">
<th><p>曲序</p></th>
<th><p>曲名</p></th>
<th><p>作詞</p></th>
<th><p>作曲</p></th>
<th><p>編曲</p></th>
<th><p>製作</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>《讓》</p></td>
<td><p>姚若龍</p></td>
<td><p><a href="../Page/蕭煌奇.md" title="wikilink">蕭煌奇</a></p></td>
<td><p>Wave G</p></td>
<td><p><a href="../Page/周佳佑.md" title="wikilink">周佳佑</a></p></td>
<td><p>第二主打</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>《幸福的風》</p></td>
<td><p><a href="../Page/陶晶瑩.md" title="wikilink">陶晶瑩</a></p></td>
<td><p>謝布暐</p></td>
<td><p>鍾興民</p></td>
<td><p><a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a></p></td>
<td><p>第三主打</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>《鴿子》</p></td>
<td><p>鄭中庸</p></td>
<td><p><a href="../Page/林邁可.md" title="wikilink">林邁可</a></p></td>
<td><p><a href="../Page/林邁可.md" title="wikilink">林邁可</a></p></td>
<td><p><a href="../Page/林邁可.md" title="wikilink">林邁可</a></p></td>
<td><p>首播主打</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>《洋蔥》</p></td>
<td><p><a href="../Page/陳信宏.md" title="wikilink">阿信</a></p></td>
<td><p><a href="../Page/陳信宏.md" title="wikilink">阿信</a></p></td>
<td><p>周恆毅</p></td>
<td><p><a href="../Page/周佳佑.md" title="wikilink">周佳佑</a><br />
周恆毅</p></td>
<td><p>第四主打、電視劇<a href="../Page/愛的秘笈.md" title="wikilink">愛的秘笈插曲</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>《重來好不好》</p></td>
<td><p>鄭中庸</p></td>
<td><p><a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a></p></td>
<td><p>鍾興民</p></td>
<td><p><a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>《One Day》</p></td>
<td><p>何厚華、<a href="../Page/楊宗緯.md" title="wikilink">楊宗緯</a></p></td>
<td><p><a href="../Page/周傳雄.md" title="wikilink">周傳雄</a></p></td>
<td><p>洪信傑</p></td>
<td><p><a href="../Page/周佳佑.md" title="wikilink">周佳佑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>《誰會改變我》</p></td>
<td><p>吳向飛</p></td>
<td><p><small>Kevin &amp; Sean Kumar</small></p></td>
<td><p><small>Kevin &amp; Sean Kumar</small></p></td>
<td><p><a href="../Page/:en:Jim_Lee_(music_producer).md" title="wikilink">Jim Lee</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>《存愛》</p></td>
<td><p>鄭中庸、<a href="../Page/周佳佑.md" title="wikilink">周佳佑</a></p></td>
<td><p>溫偉傑</p></td>
<td><p>Wave G</p></td>
<td><p><a href="../Page/周佳佑.md" title="wikilink">周佳佑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>《對愛渴望》</p></td>
<td><p>管啟源</p></td>
<td><p><a href="../Page/蔡健雅.md" title="wikilink">蔡健雅</a></p></td>
<td><p>鍾興民</p></td>
<td><p><a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a></p></td>
<td><p>第五主打</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>《回憶沙漠》</p></td>
<td><p><a href="../Page/易家揚.md" title="wikilink">易家揚</a></p></td>
<td><p><a href="../Page/李偉菘.md" title="wikilink">李偉菘</a></p></td>
<td><p>黃銘源</p></td>
<td><p><a href="../Page/李偉菘.md" title="wikilink">李偉菘</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>《你看》</p></td>
<td><p><a href="../Page/楊宗緯.md" title="wikilink">楊宗緯</a></p></td>
<td><p><a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a></p></td>
<td><p>鍾興民</p></td>
<td><p><a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 音樂錄影帶

  - 總共有5首歌曲《鴿子》《讓》《洋蔥》《對愛渴望》與《幸福的風》，拍攝[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")。

## 專輯名稱

楊宗緯在《[超級星光大道](../Page/超級星光大道.md "wikilink")》參賽期間，在他的[部落格上發表了一篇](../Page/部落格.md "wikilink")[網誌](../Page/網誌.md "wikilink")\[1\]，其中有一段內容把自己比擬為「銅像」，支持他的歌迷就好比駐守在銅像上的「鴿子」。

從此[楊宗緯的](../Page/楊宗緯.md "wikilink")[歌迷就自稱為](../Page/歌迷.md "wikilink")「鴿子」，有時也稱為「鴿迷」，家族稱為「鴿窩」，並且把楊宗緯稱為「鴿王」。[楊宗緯感謝他的](../Page/楊宗緯.md "wikilink")「鴿迷」的不離不棄，在預購專輯的文宣物品上，還寫下文案「獻給那些耐心等待為我喝彩的鴿子們～」。

## 實體包裝

[2008_Aska_Yang_Album_Dove_2nd_Cover.jpeg](https://zh.wikipedia.org/wiki/File:2008_Aska_Yang_Album_Dove_2nd_Cover.jpeg "fig:2008_Aska_Yang_Album_Dove_2nd_Cover.jpeg")
2008年1月11日發行初版

  - 音樂 CD（1片裝）
      - 附贈36頁「楊宗緯錄音手札」珍藏楊宗緯錄音心情手稿、照片、插畫
  - 海報 （預購版）

2008年2月29日發行CD+DVD影音升級慶功版

  - 收錄5首主打MV《鴿子》《讓》《洋蔥》《幸福的風》《對愛渴望》

## 電臺播放

  - 2007年12月26日 HitoRadio
    [HitFM聯播網](../Page/HitFM聯播網.md "wikilink")《鴿子》首播
  - 2008年1月10日 [飛碟電台](../Page/飛碟電台.md "wikilink")「楊宗緯日」整天播出新專輯的所有曲目

## 線上點播

  - 2007年12月28日 ezPeer+ 將《鴿子》45秒的副歌版提供給網試聽，從中午開始至下午5時止\[2\]。
  - 從2007年12月28日開始，蟬聯 ezPeer+ 華語專輯排行榜16週冠軍。

## 銷售推廣

  - 預購活動

<!-- end list -->

  - 2007年12月25日開放預購，不到2天預購已超過5千張\[3\]。預購活動在2008年1月10日截止日，新專輯預購量達3萬多張\[4\]。
  - 2007年12月博客來音樂館銷售排行榜冠軍

<!-- end list -->

  - 宣傳活動

[Aska_Yang_Dove_Album_Cover.jpg](https://zh.wikipedia.org/wiki/File:Aska_Yang_Dove_Album_Cover.jpg "fig:Aska_Yang_Dove_Album_Cover.jpg")

  - 2008/01/01 [桃園市](../Page/桃園區.md "wikilink") 多功能藝文區
  - 2008/01/05 [台中市](../Page/台中市.md "wikilink")
    [新光三越百貨](../Page/新光三越百貨.md "wikilink")
  - 2008/01/05 [高雄市](../Page/高雄市.md "wikilink") 大遠百百貨
  - 2008/01/06 [台北市](../Page/台北市.md "wikilink")
    [台北車站南二門廣場](../Page/台北車站.md "wikilink")
  - 2008/01/12 [台南市](../Page/台南市.md "wikilink") 新光三越新天地
  - 2008/01/12 [台中市](../Page/台中市.md "wikilink") 廣三Sogo
  - 2008/01/13 [台北市](../Page/台北市.md "wikilink") 西門町聯合醫院前廣場
  - 2008/01/13 [桃園縣](../Page/桃園市.md "wikilink") 統領百貨
  - 2008/01/19 [中壢市](../Page/中壢區.md "wikilink") 玫瑰唱片
  - 2008/01/19 [新竹市](../Page/新竹市.md "wikilink") 新光三越
  - 2008/01/26 [屏東市](../Page/屏東市.md "wikilink") 太平洋Sogo
  - 2008/01/26 [高雄市](../Page/高雄市.md "wikilink") 漢神百貨

## 創造記錄

  - 預購記錄

<!-- end list -->

  - 首位新人預購登上[7-Eleven全國通路](../Page/7-Eleven.md "wikilink")，一般只有銷售保證的歌手，才能在[7-Eleven全國通路接受預購](../Page/7-Eleven.md "wikilink")
  - 首位凌晨辦[握手會的歌手](../Page/握手會.md "wikilink")，在[跨年倒數後](../Page/跨年.md "wikilink")，緊接在原場地（[桃園縣多功能藝文區](../Page/桃園市.md "wikilink")）辦預購握手會

<!-- end list -->

  - 銷售記錄

<!-- end list -->

  - [G-music](../Page/G-music.md "wikilink") 玫瑰大眾風雲榜
    連續在榜**40週**，**9週第一名**，**年度第4名**（佔[臺灣音樂實體通路](../Page/臺灣.md "wikilink")50%）
  - 五大華語暢銷排行榜 **2週第一名**，**年度第6名**
  - 佳佳唱片 銷售排行榜 **2週**第一名
  - EzPeer+ 專輯排行榜 **16週**第一名，年度專輯第1名
  - EzPeer+ 單曲排行榜
    曾經共有9首入單曲榜前十名，年度單曲**《洋蔥》第1名，《對愛渴望》第4名，《幸福的風》第7名，《鴿子》第10名**
  - 蘋果線上 音樂風雲榜 **6週**第一名
  - Hito Radio FM91.7 Hito中文榜 **5週**第一名
  - Kiss華語排行榜 **7週**第一名
  - UFO飛碟幽浮勁碟排行榜 **7週**第一名
  - MTV台 封神榜 **2週**第一名
  - MTV台 暢銷金榜 **2週**第一名
  - [Channel \[V](../Page/Channel_V.md "wikilink")\] 音樂飆榜 第一名
  - [新加坡Radio](../Page/新加坡.md "wikilink")1003優勢流行榜**2週**第一名
  - [馬來西亞](../Page/馬來西亞.md "wikilink")988排行榜**2週**第一名
  - 博客來華語音樂月排行榜 第一名
  - 大潤發CD暢銷排行榜 第一名
  - 家樂福CD暢銷排行榜
  - 新加坡2008年度 華語專輯銷售榜 第七名

## 得獎

  - UFO飛碟幽浮勁碟排行榜 2008年終榜 【鴿子】專輯 **第一名**
  - Hitfm 2008年度百首單曲 《洋蔥》 **第一名** \[5\]
  - Hitfm 2008年度百首單曲 《鴿子》 第5名
  - Hitfm 2008年度百首單曲 《幸福的風》 第12名
  - 新加坡 Y.E.S 933FM 醉心龍虎榜 **停榜最久金曲** 《洋蔥》
  - 新加坡 Y.E.S 933FM 醉心龍虎榜 **十大最受歡迎金曲** 《洋蔥》
  - 新加坡 Y.E.S 933FM 2008年度頂尖100單曲榜 《鴿子》 **第一名**
  - 香港 TVB主辦的 2008年度勁歌金曲 最受歡迎華語歌曲獎入圍《鴿子》《洋蔥》 \[6\]

## 參考資料

## 相關連結

  - [星光同學會-超級星光大道10強紀念合輯](../Page/星光同學會-超級星光大道10強紀念合輯.md "wikilink").
  - [愛星光精選-昨天今天明天](../Page/愛星光精選-昨天今天明天.md "wikilink")

## 外部連結

  - [華納唱片專輯介紹](https://web.archive.org/web/20071228064115/http://www.warnermusic.com.tw/news/read.asp?id=1431)

[Category:楊宗緯音樂專輯](../Category/楊宗緯音樂專輯.md "wikilink")
[Category:2008年音樂專輯](../Category/2008年音樂專輯.md "wikilink")
[Category:台灣流行音樂專輯](../Category/台灣流行音樂專輯.md "wikilink")

1.  [《昨天電視看完了嗎》](http://www.wretch.cc/blog/nonodarling&article_id=5262885)
2.
3.
4.
5.
6.  [2008年度勁歌金曲頒獎典禮候選名單](http://chinanews.sina.com/ent/2009/0102/04483189549.html)（2009年1月3日引用）