{{ Otheruses|subject=日本年号|other=大长和鄭仁旻的安和年号|始元 (鄭仁旻)}}

**安和**（968年八月十三至970年三月廿五）是[日本的](../Page/日本.md "wikilink")[年號](../Page/年號.md "wikilink")。使用這年號之[日本天皇是](../Page/日本天皇.md "wikilink")[冷泉天皇與](../Page/冷泉天皇.md "wikilink")[圓融天皇](../Page/圓融天皇.md "wikilink")。

## 改元

  - 康保五年八月十三（968年9月8日） ：改元。
  - 安和三年三月廿五（970年5月3日） ：改元[天祿](../Page/天祿_\(圓融天皇\).md "wikilink")。

## 出處

## 大事記

  - 安和二年：[安和之變](../Page/安和之變.md "wikilink")：[左大臣](../Page/左大臣.md "wikilink")[源高明被降職](../Page/源高明.md "wikilink")，以[太政大臣](../Page/太政大臣.md "wikilink")[藤原實賴與其異母弟](../Page/藤原實賴.md "wikilink")[右大臣](../Page/右大臣.md "wikilink")[藤原師尹為首的](../Page/藤原師尹.md "wikilink")[藤原氏完成排斥其他](../Page/藤原氏.md "wikilink")[家族勢力](../Page/家族.md "wikilink")。

## 出生

## 逝世

## 紀年、干支、西曆對照表

|                                |                                |                                |                                |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 安和                             | 元年                             | 二年                             | 三年                             |
| [公元](../Page/公元.md "wikilink") | 968年                           | 969年                           | 970年                           |
| [干支](../Page/干支.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [天會](../Page/天會_\(劉鈞\).md "wikilink")（957年至973年）：[北漢](../Page/北漢.md "wikilink")—劉鈞、[劉繼恩](../Page/劉繼恩.md "wikilink")、[劉繼元之年號](../Page/劉繼元.md "wikilink")
      - [乾德](../Page/乾德_\(趙匡胤\).md "wikilink")（963年十一月至968年十一月）：[北宋](../Page/北宋.md "wikilink")—太祖[趙匡胤之年號](../Page/趙匡胤.md "wikilink")
      - [開寶](../Page/開寶.md "wikilink")（968年十一月至976年）：北宋—太祖趙匡胤之年號
      - [大寶](../Page/大寶_\(劉鋹\).md "wikilink")（958年八月至971年二月）：[南漢](../Page/南漢.md "wikilink")—[劉鋹之年號](../Page/劉鋹.md "wikilink")
      - [順德](../Page/顺德_\(段思聪\).md "wikilink")（968年）：[大理國](../Page/大理國.md "wikilink")—[段思聰之年號](../Page/段思聰.md "wikilink")
      - [明政](../Page/明政_\(段素順\).md "wikilink")（969年至985年）：大理國—[段素順之年號](../Page/段素順.md "wikilink")
      - [太平](../Page/太平_\(丁部領\).md "wikilink")（970年至980年）：[丁朝](../Page/丁朝.md "wikilink")—[丁部領](../Page/丁部領.md "wikilink")、[丁璿之年號](../Page/丁璿.md "wikilink")
      - [應曆](../Page/應曆.md "wikilink")（951年九月至969年二月）：[遼](../Page/遼朝.md "wikilink")—[遼穆宗耶律璟之年號](../Page/遼穆宗.md "wikilink")
      - [保寧](../Page/保寧.md "wikilink")（969年二月至979年十一月）：遼—[遼景宗耶律賢之年號](../Page/遼景宗.md "wikilink")
      - [天尊](../Page/天尊.md "wikilink")（967年至977年）：[于闐](../Page/于闐.md "wikilink")—[尉遲蘇拉之年號](../Page/尉遲蘇拉.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1988年3月，ISBN 4639007116

[Category:10世纪日本年号](../Category/10世纪日本年号.md "wikilink")
[Category:960年代日本](../Category/960年代日本.md "wikilink")
[Category:970年代日本](../Category/970年代日本.md "wikilink")