**岡村明美**是[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")，暨電視節目解說員。[東京都出身](../Page/東京都.md "wikilink")。隸屬日本Mausu
Promotion公司。

代表作品有《[紅豬](../Page/紅豬.md "wikilink")》（菲兒・保可洛）、《[ONE
PIECE](../Page/ONE_PIECE.md "wikilink")》（[娜美](../Page/娜美.md "wikilink")）、〈カサンドラ〜愛と運命の果てに〉（カサンドラ）、《[戀愛情結](../Page/戀愛情結.md "wikilink")》（小泉理沙）等\[1\]。

## 出演作品

### 電視動畫

  - [鬼神童子ZENKI](../Page/鬼神童子ZENKI.md "wikilink")（南雲清）

  - [海月姫](../Page/海月姫.md "wikilink")（瑪雅雅）

  - [幸運女神 小不隆咚便利多多](../Page/我的女神.md "wikilink")（蓓兒丹娣） ※第一至十三集

  -
  - [天才寶貝](../Page/天才寶貝.md "wikilink")（真由美先生、藤井淺子）

  - [明日的娜加](../Page/明日的娜加.md "wikilink")（）

  -
  - [抓狂一族](../Page/抓狂一族.md "wikilink")（大澤木櫻、上田信彥、仁媽媽）

  - [反斗小王子](../Page/反斗小王子.md "wikilink")

  - [CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")（妹之山殘）

  - [快樂小丸日記](../Page/快樂小丸日記.md "wikilink")（小花）

  -
  - [灰姑娘物語](../Page/灰姑娘物語.md "wikilink")（伊莎貝爾）

  -
  - （荒井陽子）

  -
  - [東京喵喵](../Page/東京喵喵.md "wikilink")（萬里子）

  -
  - [勇午～交涉人～](../Page/勇午.md "wikilink")（）

  -
  - [羅密歐的藍天](../Page/羅密歐的藍天.md "wikilink")（碧安卡‧馬丁尼）

  - [大運動會](../Page/大運動會.md "wikilink")（米蘭達·阿卡·巴爾達）

**1995年**

  - [夢幻遊戲](../Page/夢幻遊戲.md "wikilink")（結蓮）

**1996年**

  - [小孩子的玩具](../Page/小孩子的玩具.md "wikilink")（羽山夏美）

**1999年**

  - [ONE
    PIECE](../Page/ONE_PIECE.md "wikilink")（**[娜美](../Page/娜美.md "wikilink")**、絲絲、公貝（蒂姆妮的寵物）、雅菲蘭朵拉、冒牌羅賓（可可）、賓什莫克·約吉士（童年））　※
    岡村明美在《ONE PIECE》中的配音除娜美以外的人物，大多皆是以**粗忽屋武蔵野店**的名義來稱呼
  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")（最上葉月、桐島玲於奈）
  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")（上原杏子）
  - [超時空幻境](../Page/超時空幻境.md "wikilink")（**松谷修造**）

**2000年**

  - [櫻花大戰](../Page/櫻花大戰.md "wikilink")（藤井霞）

**2001年**

  - [大～集合\!\!小魔女Doremi](../Page/小魔女Doremi.md "wikilink")（萬田順治、萬田洋子）

**2002年**

  - [小魔女DoReMi 大合奏\!](../Page/小魔女Doremi.md "wikilink")（萬田順治、萬田洋子）}}

**2003年**

  - （芝村舞）

**2004年**

  - [恋風](../Page/恋風.md "wikilink")（千鳥要）
  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink")（高橋真美子、）

**2005年**

  - [到另一個你的身邊去](../Page/到另一個你的身邊去.md "wikilink")（上乃木明日香）
  - [光之美少女Max Heart](../Page/光之美少女.md "wikilink")（大輝）

**2006年**

  - [NANA](../Page/NANA.md "wikilink")（小松奈緒（小松奈奈的姊姊））
  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")（珠翠、紫劉輝（幼少時期））
  - [地獄少女](../Page/地獄少女.md "wikilink")(2005年)（湊藤江）(第15話)
  - [光之美少女Splash Star](../Page/光之美少女.md "wikilink")（霧生薰、芙芙）
  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（初代「灼眼炎髮的殺手」 瑪蒂達·桑特美爾）
  - [蟲師](../Page/蟲師.md "wikilink")（梶）

**2007年**

  - [戀愛情結](../Page/戀愛情結.md "wikilink")（**小泉理沙**）
  - [風之少女艾蜜莉](../Page/風之少女艾蜜莉.md "wikilink")（佩里·米勒）
  - [搞笑漫畫日和](../Page/搞笑漫畫日和.md "wikilink")（烹飪節目來賓）

**2008年**

  - [夏目友人帳](../Page/夏目友人帳.md "wikilink")（丙）

**2009年**

  - [鋼之鍊金術師 FULLMETAL
    ALCHEMIST](../Page/鋼之鍊金術師_FULLMETAL_ALCHEMIST.md "wikilink")（帕妮孃）
  - [遊戲王5D's](../Page/遊戲王5D's.md "wikilink")（芭芭拉）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")（平良靖枝、北尾留海\#740～741、小柳綠
    \#808-809）
  - [夏目友人帳 續](../Page/夏目友人帳.md "wikilink")（丙）

**2011年**

  - [夏目友人帳 參](../Page/夏目友人帳.md "wikilink")（丙）

**2012年**

  - [夏目友人帳 肆](../Page/夏目友人帳.md "wikilink")（丙）
  - [元氣少女緣結神](../Page/元氣少女緣結神.md "wikilink")（龜姬）

**2013年**

  - [宇宙戰艦大和號2199](../Page/宇宙戰艦大和號2199.md "wikilink")（蜜蕾妮尔·琳凯）※2012年起于电影院先行上映

**2014年**

  - [Happiness Charge
    光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")（霍茜娃）
  - [元氣！抓狂一族](../Page/抓狂一族.md "wikilink")（江戶五郎、山田真夜）
  - [陽炎計畫](../Page/陽炎計畫.md "wikilink")（小櫻紫苑/Shion）
  - [少年好萊塢 -HOLLY STAGE FOR 49-](../Page/少年好萊塢.md "wikilink")（紺野美希）

**2015年**

  - [金田一少年之事件簿R 第2期](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（禅田美瑠久）

**2016年**

  - [只有我不存在的城市](../Page/只有我不存在的城市.md "wikilink")（雛月的母親）
  - [夏目友人帳 伍](../Page/夏目友人帳.md "wikilink")（丙）

**2017年**

  - [夏目友人帳 陸](../Page/夏目友人帳.md "wikilink")（丙）

**2018年**

  - [童話魔法使](../Page/童話魔法使.md "wikilink")（學園長）
  - [書店裡的骷髏店員本田](../Page/書店裡的骷髏店員本田.md "wikilink")（**盔甲**\[2\]、蜜雪兒、母親）

### OVA

  - [AIKa](../Page/AIKa.md "wikilink")（ミナ、エツコ）
  - [動畫執行製作くろみちゃん](../Page/動畫執行製作くろみちゃん.md "wikilink")（深水葵）
  - [亞爾斯蘭戰記Ⅳ](../Page/亞爾斯蘭戰記.md "wikilink")（イリーナ）
  - [低俗靈白日夢](../Page/低俗靈白日夢.md "wikilink")（小愛的姊姊）
  - [烈焰W](../Page/烈焰W.md "wikilink")（千-{里}-）
  - [大運動會](../Page/大運動會.md "wikilink")（米蘭達·阿卡·巴爾達）
  - [非常可怕的格林童話](../Page/非常可怕的格林童話.md "wikilink")（彼得（[糖果屋](../Page/糖果屋.md "wikilink")）、瑪麗安妮（[藍鬍子](../Page/藍鬍子.md "wikilink")）、卡特琳娜（[灰姑娘](../Page/灰姑娘.md "wikilink")））
  - [MEZZO FORTE](../Page/MEZZO_FORTE.md "wikilink")（桃井桃美）

**2015年**

  - [要聽爸爸的話](../Page/要聽爸爸的話.md "wikilink")（莎夏）

**2017年**

  - [夏目友人帳 遊戲之宴](../Page/夏目友人帳.md "wikilink")（丙）

### 劇場版動畫

  - [紅豬](../Page/紅豬.md "wikilink")（菲兒）
  - [6天使之翼](../Page/6天使之翼.md "wikilink")（桃莉絲·尼可拉斯）
  - [貓咪事務所](../Page/貓咪事務所.md "wikilink")（三毛貓）
  - [劇場版ONE PIECE各作品](../Page/ONE_PIECE.md "wikilink")（娜美）
  - [櫻花大戰 活動寫真](../Page/櫻花大戰.md "wikilink")（藤井霞）
  - [數碼寶貝最前線古代數碼精靈復活](../Page/數碼寶貝最前線.md "wikilink")（）

### 遊戲

  - [伊蘇I・II](../Page/伊蘇.md "wikilink")（）

  - [鬼武者](../Page/鬼武者.md "wikilink")（雪姬）

  - [銀河天使](../Page/銀河天使.md "wikilink")（）

  - （芝村舞）

  - [交響曲傳奇](../Page/交響曲傳奇.md "wikilink")（）

  -
  -
  - [櫻花大戰](../Page/櫻花大戰.md "wikilink")（藤井霞）

  - [美少女夢工場5](../Page/美少女夢工場.md "wikilink")（吉普）

  - [海賊無雙](../Page/海賊無雙.md "wikilink")(娜美)

### CD

  - [閃電霹靂車SAGA](../Page/閃電霹靂車.md "wikilink")2（莉莎‧海尼爾）

  - [EREMENTAR GERAD](../Page/武器種族傳說.md "wikilink")

  -
  -
  -
### 劇集

  - [三國演義日語版](../Page/三國演義_\(電視劇\).md "wikilink")（貂蟬）

### 其他

  -
<!-- end list -->

  -

## 外部連結

  - [事務所公開簡歷](http://www.mausu.net/talent/tpdb_view.cgi?UID=4)
  - [岡村明美的資料](https://archive.is/20130501053319/dir.yahoo.co.jp/talent/5/w94-0119.html)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.  [Yahoo\!人物名鑑によるプロフィール](http://talent.yahoo.co.jp/pf/profile/pp940)より
    2010年2月6日閲覧
2.