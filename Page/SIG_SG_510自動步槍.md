**SIG SG 510**/**Sturmgewehr
57**是由[瑞士](../Page/瑞士.md "wikilink")[SIG](../Page/SIG.md "wikilink")（現稱[Swiss
Arms](../Page/Swiss_Arms.md "wikilink")）制造的[自動步槍](../Page/自動步槍.md "wikilink")，採用與[HK
G3及](../Page/HK_G3自動步槍.md "wikilink")[CETME相同的](../Page/CETME.md "wikilink")[滾輪延遲反沖式系統](../Page/反沖作用_\(槍械\)#滾輪延遲反沖.md "wikilink")。SIG
SG
510能發射[22毫米](../Page/22毫米槍榴彈.md "wikilink")[槍榴彈](../Page/步槍用榴彈.md "wikilink")，而且準確度高，在惡劣環境下仍可運作良好。

SIG SG 510曾是瑞士軍隊制式裝備，名為**F ass 57**（[法語](../Page/法語.md "wikilink")：Fusil
d Assaut 57）或**Stgw 57**（[德語](../Page/德語.md "wikilink")：Sturm Gewehr
57）。

## 歷史

Stgw 57/SIG SG
510-1在1957年裝備瑞士軍隊，而[玻利維亞及](../Page/玻利維亞.md "wikilink")[智利軍隊亦有裝備SIG](../Page/智利.md "wikilink")
SG 510-4，當中，[智利的SIG](../Page/智利.md "wikilink") SG
510-4由SIG給[義大利](../Page/義大利.md "wikilink")[貝瑞塔合約特許生產](../Page/貝瑞塔公司.md "wikilink")。雖然SIG
SG 510火力大，準確度高，但體積大及太重（5.56公斤），已被輕型且小口徑的[SIG
550取代](../Page/SIG_550.md "wikilink")，現在只有極小量仍然服役。

SIG SG 510的半自動民用型版本名為**PE-57**，美國民用型名為**SIG AMT**（American Match
Target）。

## 改進型

[Rifle_SIG_Stg_510-4.jpg](https://zh.wikipedia.org/wiki/File:Rifle_SIG_Stg_510-4.jpg "fig:Rifle_SIG_Stg_510-4.jpg")

  - **SG 510-1**—標準型
  - **SG 510-2**—是510-1的輕量化版本
  - **SG
    510-3**—改為發射[7.62×39毫米](../Page/7.62×39.md "wikilink")[彈藥](../Page/彈藥.md "wikilink")
  - **SG
    510-4**—[玻利維亞及](../Page/玻利維亞.md "wikilink")[智利專用版本](../Page/智利.md "wikilink")，改為發射[7.62×51毫米NATO彈藥](../Page/7.62×51mm_NATO.md "wikilink")

## 使用國

  -
  - —只裝備炮兵部隊及訓練用途

  - －大公槍騎兵中隊

  - —仍有極小量採用

  -
  -
  - －曾裝備[德國聯邦國防軍](../Page/德國聯邦國防軍.md "wikilink")，命名為**G2**

## 登場作品

### 電子遊戲

  - 2015年—《[-{zh-hans:战地：硬仗;
    zh-hant:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield
    Hardline）：為SG510-4和Stgw
    57的混合槍，命名為「SG510」，為資料片「犯罪活動」新增的資料片獨有武器，歸類為[戰鬥步槍](../Page/戰鬥步槍.md "wikilink")，24+1發彈匣，被警方執行者（Enforcer）所使用（罪犯解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$78,600。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
    T1、SRS 02、[Comp
    M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
    、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、[電筒](../Page/電筒.md "wikilink")、[戰術燈](../Page/戰術燈.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")、延長彈匣、[穿甲](../Page/穿甲彈.md "wikilink")[曳光彈](../Page/曳光彈.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。

### [輕小說](../Page/輕小說.md "wikilink")

  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：於SJ2期間由一名GGO玩家所持有，並短暫被“不可次郎”繳獲以充當[鈍器使用](../Page/鈍器.md "wikilink")。

## 參考

  - [SIG SG 550](../Page/SIG_SG_550突擊步槍.md "wikilink")
  - [SIG SG 552](../Page/SIG_SG_552卡賓槍.md "wikilink")
  - [SIG MG 710](../Page/SIG_MG_710-3通用機槍.md "wikilink")（MG55）
  - [SIG SG 540](../Page/SIG_SG_540突擊步槍.md "wikilink")

## 外部連結

  - —[world.guns.ru](https://web.archive.org/web/20051120114943/http://world.guns.ru/assault/as45-e.htm)

  - —[www.biggerhammer.net](http://www.biggerhammer.net/sigamt/pe57_amt_general/)

  - —[swissrifles.com](http://www.swissrifles.com/)

  - —[gunco.net](http://www.gunco.net/gallery/showphoto.php/photo/6699)

  - —[exordinanza.net](http://www.exordinanza.net/schede/StGw57.htm)

  - —[euroarms.net](http://www.euroarms.net/Mailing/ele2002/elenco021220.htm)

  - —[枪炮世界—Stgw.57/SIG 510步枪](http://firearmsworld.net/sigsauer/sig510/sig510.htm)

[Category:自动步枪](../Category/自动步枪.md "wikilink")
[Category:戰鬥步槍](../Category/戰鬥步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:瑞士槍械](../Category/瑞士槍械.md "wikilink")