在[密碼學中](../Page/密碼學.md "wikilink")，**Camellia**是一種為許多組織所推崇的[塊密碼](../Page/塊密碼.md "wikilink")（block
cipher），包括[歐盟的](../Page/歐盟.md "wikilink")[NESSIE項目](../Page/NESSIE.md "wikilink")（作為選定算法）和[日本的](../Page/日本.md "wikilink")[CRYPTREC項目](../Page/CRYPTREC.md "wikilink")（作為推薦算法）。該算法由[三菱和](../Page/三菱.md "wikilink")[日本電信電話](../Page/日本電信電話.md "wikilink")（NTT）在2000年共同發明，它和早期的塊算法（[E2及](../Page/E2_\(cipher\).md "wikilink")[MISTY1](../Page/MISTY1.md "wikilink")）有相似的設計思想。

Camellia算法每塊的的長度（block
size）為128[位元](../Page/位元.md "wikilink")，金鑰長度則可以使用128、192或256[位元](../Page/位元.md "wikilink")。具有與[AES同等級的安全強度及運算量](../Page/AES.md "wikilink")\[1\]。

## 設計

在計算方面，Camellia採用18輪（128位元）或者24輪（192或256位元）的[Feistel
cipher](../Page/Feistel_cipher.md "wikilink")。每6輪就會做一個邏輯變換，即所謂的「FL-函數」或者它的反函數。這種算法也使用輸入、輸出[key
whitening](../Page/key_whitening.md "wikilink")。

## 專利狀態

雖然受到[專利保護](../Page/專利.md "wikilink")，但在2001年時NTT宣佈Camellia為Royalty-free
license\[2\]。

## 使用情況

2008年時將Camellia被加入進[Mozilla Firefox
3](../Page/Mozilla_Firefox.md "wikilink")\[3\]。同年稍晚，[FreeBSD也宣佈在](../Page/FreeBSD.md "wikilink")6.4-RELEASE內加入Camellia。[2009年9月](../Page/2009年9月.md "wikilink")，[GnuPG在](../Page/GnuPG.md "wikilink")1.4.10版加入Camellia支援。

## 參見

  - [AES](../Page/高级加密标准.md "wikilink")
  - Kazumaro Aoki, Tetsuya Ichikawa, Masayuki Kanda, Mitsuru Matsui,
    Shiho Moriai, Junko Nakajima, Toshio Tokita. *Camellia: A 128-Bit
    Block Cipher Suitable for Multiple Platforms — Design and Analysis*.
    Selected Areas in Cryptography 2000, pp39–56.

## 参考文献

## 外部連結

  - [Camellia 网站](https://info.isl.ntt.co.jp/crypt/camellia/)
  - [使用Camellia的产品](https://info.isl.ntt.co.jp/crypt/camellia/product.html)
  - [参考代码](http://embeddedsw.net/Cipher_Reference_Home.html)
  - RFC 3657 — Use of the Camellia Encryption Algorithm in Cryptographic
    Message Syntax (CMS)
  - RFC 4312 — The Camellia Cipher Algorithm and Its Use With IPsec

[Category:分组密码](../Category/分组密码.md "wikilink")

1.  [Japan's First 128-bit Block Cipher 'Camellia' Approved as a New
    Standard Encryption Algorithm in the
    Internet](http://www.physorg.com/news5315.html)
2.  [Announcement of Royalty-free Licenses for Essential Patents of NTT
    Encryption and Digital Signature
    Algorithms](http://www.ntt.co.jp/news/news01e/0104/010417.html)
3.  [Camellia cipher added to
    Firefox](http://blog.mozilla.com/gen/2007/07/30/camellia-cipher-added-to-firefox/)