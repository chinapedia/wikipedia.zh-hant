**唇形目**（[學名](../Page/學名.md "wikilink")：），原「**玄参目**」（[學名](../Page/學名.md "wikilink")：）已合併入本目，是一類[真雙子葉植物](../Page/真雙子葉植物.md "wikilink")，有[木本和](../Page/木本.md "wikilink")[草本](../Page/草本.md "wikilink")，其中[玄参科常含有](../Page/玄参科.md "wikilink")[生物碱和](../Page/生物碱.md "wikilink")[糖苷](../Page/糖苷.md "wikilink")，所以很多种都可以作为[药用](../Page/药.md "wikilink")，也有一些观赏植物[花卉](../Page/花.md "wikilink")。

## 特徵

本目植物[乔木有](../Page/乔木.md "wikilink")[白蜡树](../Page/白蜡树.md "wikilink")、[女贞](../Page/女贞.md "wikilink")、[泡桐](../Page/泡桐.md "wikilink")、[水曲柳等](../Page/水曲柳.md "wikilink")；[灌木有](../Page/灌木.md "wikilink")[连翘](../Page/连翘.md "wikilink")、[茉莉](../Page/茉莉.md "wikilink")、[丁香等](../Page/丁香.md "wikilink")；草本如[地黄](../Page/地黄.md "wikilink")、[肉苁蓉](../Page/肉苁蓉.md "wikilink")、[玄参](../Page/玄参.md "wikilink")、[毛地黄等药物](../Page/毛地黄.md "wikilink")，以及观赏花卉如[蒲包花](../Page/蒲包花.md "wikilink")、[金鱼草等](../Page/金鱼草.md "wikilink")。脣形目的花是两性的，两侧对称或辐射对称，[种子为](../Page/种子.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，也有[浆果或](../Page/浆果.md "wikilink")[核果的](../Page/核果.md "wikilink")。

## 科

依[APG IV分类法](../Page/APG_IV分类法.md "wikilink")，唇形目有24科：

  - [爵床科](../Page/爵床科.md "wikilink") Acanthaceae
    [Juss.](../Page/Juss..md "wikilink") (1789),
  - [紫葳科](../Page/紫葳科.md "wikilink") Bignoniaceae
    [Juss.](../Page/Juss..md "wikilink") (1789), nom. cons.
  - [腺毛草科](../Page/腺毛草科.md "wikilink") Byblidaceae
    [Domin](../Page/Domin.md "wikilink") (1922), nom. cons.
  - [荷包花科](../Page/荷包花科.md "wikilink") Calceolariaceae
    [Olmstead](../Page/Olmstead.md "wikilink") (2001)
  - [香茜科](../Page/香茜科.md "wikilink") Carlemanniaceae [Airy
    Shaw](../Page/Airy_Shaw.md "wikilink") (1965)
  - [苦苣苔科](../Page/苦苣苔科.md "wikilink") Gesneriaceae
    [Rich.](../Page/Rich..md "wikilink") &
    [Juss.](../Page/Juss..md "wikilink") (1816), nom. cons.
  - [唇形科](../Page/唇形科.md "wikilink") Lamiaceae
    [Martinov](../Page/Martinov.md "wikilink") (1820), nom. cons.
  - [母草科](../Page/母草科.md "wikilink") Linderniaceae
    [Borsch](../Page/Borsch.md "wikilink"),
    [K.Müll.](../Page/K.Müll..md "wikilink"), &
    [Eb.Fisch.](../Page/Eb.Fisch..md "wikilink") (2005)
  - [狸藻科](../Page/狸藻科.md "wikilink") Lentibulariaceae
    [Rich.](../Page/Rich..md "wikilink") (1808), nom. cons.
  - [角胡麻科](../Page/角胡麻科.md "wikilink") Martyniaceae
    [Horan.](../Page/Horan..md "wikilink") (1847), nom. cons.
  - [通泉草科](../Page/通泉草科.md "wikilink") Mazaceae
    [Reveal](../Page/Reveal.md "wikilink")
  - [木樨科](../Page/木樨科.md "wikilink") Oleaceae
    [Hoffmanns.](../Page/Hoffmanns..md "wikilink") &
    [Link](../Page/Link.md "wikilink") (1809), nom. cons.
  - [列当科](../Page/列当科.md "wikilink") Orobanchaceae
    [Vent.](../Page/Vent..md "wikilink") (1799), nom. cons.
  - [泡桐科](../Page/泡桐科.md "wikilink") Paulowniaceae
    [Nakai](../Page/Nakai.md "wikilink") (1949)
  - [芝麻科](../Page/芝麻科.md "wikilink") Pedaliaceae
    [R.Br.](../Page/R.Br..md "wikilink") (1810), nom. cons.
  - [透骨草科](../Page/透骨草科.md "wikilink") Phrymaceae
    [Schauer](../Page/Schauer.md "wikilink") (1847), nom. cons.
  - [车前科](../Page/车前科.md "wikilink") Plantaginaceae
    [Juss.](../Page/Juss..md "wikilink") (1789), nom. cons.
  - [戴缨木科](../Page/戴缨木科.md "wikilink") Plocospermataceae
    [Hutch.](../Page/Hutch..md "wikilink") (1973)
  - [钟萼桐科](../Page/钟萼桐科.md "wikilink") Schlegeliaceae
    [Reveal](../Page/Reveal.md "wikilink") (1996)
  - [玄参科](../Page/玄参科.md "wikilink") Scrophulariaceae
    [Juss.](../Page/Juss..md "wikilink") (1789), nom. cons.
  - [耀仙木科](../Page/耀仙木科.md "wikilink") Stilbaceae
    [Kunth](../Page/Kunth.md "wikilink") (1831), nom. cons.
  - [四核香科](../Page/四核香科.md "wikilink") Tetrachondraceae
    [Wettst.](../Page/Wettst..md "wikilink") (1924)
  - [猩猩茶科](../Page/猩猩茶科.md "wikilink") Thomandersiaceae
    [Sreem.](../Page/Sreem..md "wikilink") (1977)
  - [马鞭草科](../Page/马鞭草科.md "wikilink") Verbenaceae
    [J.St.-Hil.](../Page/J.St.-Hil..md "wikilink") (1805), nom. cons.

## 参考文献

[\*](../Category/唇形目.md "wikilink")