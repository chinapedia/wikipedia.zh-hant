[Map_of_Sixteen_Kingdoms_1.png](https://zh.wikipedia.org/wiki/File:Map_of_Sixteen_Kingdoms_1.png "fig:Map_of_Sixteen_Kingdoms_1.png")
**段部鮮卑**是[中國](../Page/中國.md "wikilink")[晉朝至](../Page/晉朝.md "wikilink")[五胡十六國時期的](../Page/五胡十六國.md "wikilink")[鮮卑部族的一個支系](../Page/鮮卑.md "wikilink")，其根據地約在今中國[河北省北方](../Page/河北省.md "wikilink")[遼西走廊一帶](../Page/遼西走廊.md "wikilink")，與同時期存在的鮮卑部落－[慕容部](../Page/慕容部.md "wikilink")、[宇文部相同](../Page/宇文部.md "wikilink")，皆屬東部鮮卑的一支，而段部在其中被認為是最強悍的一部，段部鮮卑首領姓[段](../Page/段姓.md "wikilink")。

## 沿革

據《[魏書](../Page/魏書.md "wikilink")》記載，段部第一位首領姓名為[段日陸眷](../Page/段日陸眷.md "wikilink")（其名《[晉書](../Page/晉書.md "wikilink")》作段就陸眷）。

段部鮮卑在第三位首領[段務目塵在位時期](../Page/段務目塵.md "wikilink")，正逢[五胡亂華之始](../Page/五胡亂華.md "wikilink")，而當時的[晉朝幽州將領](../Page/晉朝.md "wikilink")[王浚認為天下將大亂](../Page/王浚.md "wikilink")，欲求向外結援，因此將其中一個女兒嫁給段務目塵，又向朝廷建議封務目塵為遼西公，把遼西郡（今中國[河北省](../Page/河北省.md "wikilink")[秦皇島市](../Page/秦皇島市.md "wikilink")）封予務目塵。這個政權也因此被稱作「遼西公國」，都城在令支（今中國[河北省](../Page/河北省.md "wikilink")[遷安市](../Page/遷安市.md "wikilink")）。史載此時段部鮮卑「據有遼西之地，而臣於晉。其所統三萬餘家，控絃上馬四五萬騎。」成為与[西晋合作在北方與其他民族作戰的力量之一](../Page/西晋.md "wikilink")。当时在王浚西面的[刘琨则和](../Page/刘琨.md "wikilink")[拓跋鲜卑合作](../Page/拓跋.md "wikilink")，和王浚与段部鮮卑的联盟不合，给他们南面的[石勒以可乘之机](../Page/石勒.md "wikilink")。

到了第四位首領[段就六眷在位的時期](../Page/段就六眷.md "wikilink")，因敗於石勒軍，又改附石勒及石勒後來所建的[後趙](../Page/後趙.md "wikilink")。第八位首領[段遼在位後](../Page/段遼.md "wikilink")，因屢侵[前燕及後趙](../Page/前燕.md "wikilink")，被二國夾攻，339年其地為二國所瓜分，遼西公國覆滅。但後來後趙在343年又重新委派被俘的段遼之弟[段蘭](../Page/段蘭.md "wikilink")，率其舊有的鮮卑部眾駐守遼西故都[令支](../Page/令支.md "wikilink")。

段蘭去世後，其子[段龕續統其部眾](../Page/段龕.md "wikilink")，350年後趙[冉閔叛變](../Page/冉閔.md "wikilink")，[中原再度陷入大亂](../Page/中原.md "wikilink")，段龕趁機於廣固（今中國[山東省](../Page/山東省.md "wikilink")[青州市](../Page/青州市.md "wikilink")）稱齊王，雖不久後歸降東晉，被封齊公，但其勢力實質上仍控有今[山東半島一帶](../Page/山東半島.md "wikilink")，頗為強盛。352年，另一位段部鮮卑首領[段末波之子](../Page/段末波.md "wikilink")[段勤亦於繹幕](../Page/段勤.md "wikilink")（今中國[山東省](../Page/山東省.md "wikilink")[平原縣](../Page/平原縣.md "wikilink")）自稱趙帝。這兩股勢力後來分別於356年、352年被前燕所滅，段勤、段龕投降後與其部眾終為前燕所殺，段部鮮卑至此可算完全滅亡。

## 段部鮮卑首領列表

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>在位時间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/段日陸眷.md" title="wikilink">段日陸眷</a>（段就陸眷）</p></td>
<td><p>？</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段乞珍.md" title="wikilink">段乞珍</a></p></td>
<td><p>？</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段務目塵.md" title="wikilink">段務目塵</a>（段務勿塵）</p></td>
<td><p>303年—310年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段就六眷.md" title="wikilink">段就六眷</a>（段疾陸眷）</p></td>
<td><p>310年—318年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段涉復辰.md" title="wikilink">段涉復辰</a></p></td>
<td><p>318年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段末波.md" title="wikilink">段末波</a>（段末杯、段末柸）</p></td>
<td><p>318年—325年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段牙.md" title="wikilink">段牙</a></p></td>
<td><p>325年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段遼.md" title="wikilink">段遼</a>（段護遼）</p></td>
<td><p>325年—338年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段蘭.md" title="wikilink">段蘭</a>（段鬱蘭）</p></td>
<td><p>343年—？</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/段龕.md" title="wikilink">段龕</a><br />
（後稱齊王）</p></td>
<td><p>？—356年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/段勤.md" title="wikilink">段勤</a><br />
（後稱趙帝）</p></td>
<td><p>350年—352年</p></td>
</tr>
</tbody>
</table>

## 參考資料

  - 《[晉書](../Page/晉書.md "wikilink")》卷六十三
  - 《[魏書](../Page/魏書.md "wikilink")》卷一百零三
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")》卷八十五至卷一百

## 參看條目

  - [段部鲜卑世系图](../Page/段部鲜卑世系图.md "wikilink")
  - [鮮卑](../Page/鮮卑.md "wikilink")
  - [段姓](../Page/段姓.md "wikilink")

[Category:五胡十六国](../Category/五胡十六国.md "wikilink")
[Category:鮮卑](../Category/鮮卑.md "wikilink")