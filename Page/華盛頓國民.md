[2012](../Page/2012年國家聯盟分區賽.md "wikilink"){{·}}[2014](../Page/2014年國家聯盟分區賽.md "wikilink"){{·}}[2016](../Page/2016年國家聯盟分區賽.md "wikilink"){{·}}[2017](../Page/2017年國家聯盟分區賽.md "wikilink")
| misc5 = | OTHER DIV CHAMPS = | WC = （0） | Wild Card = | misc6 = |
owner = [Ted Lerner](../Page/Ted_Lerner.md "wikilink") | manager =  | gm
= [Mike Rizzo](../Page/Mike_Rizzo.md "wikilink") }}
[Washington_Nationals.png](https://zh.wikipedia.org/wiki/File:Washington_Nationals.png "fig:Washington_Nationals.png")
**華盛頓國民**（Washington
Nationals）是一支位於[華盛頓特區的](../Page/華盛頓特區.md "wikilink")[美國職棒大聯盟球隊](../Page/美國職棒大聯盟.md "wikilink")，隸屬[國家聯盟東區](../Page/國家聯盟東區.md "wikilink")。

在棒球運動裡面最早在華盛頓的球隊是在華盛頓參議員，不過本球隊和當時的球隊並沒有關係。

國民是在1969年大聯盟擴充時成立的隊伍，球隊當時的位置是在[加拿大](../Page/加拿大.md "wikilink")[魁北克省的](../Page/魁北克.md "wikilink")[蒙特婁城市](../Page/蒙特婁.md "wikilink")，當時球隊名字為蒙特婁博覽會。博覽會第一個使用的場地是賈瑞公園體育場（Jarry
Park Stadium），遷移前最後的主場是[蒙特婁奧林匹克體育場](../Page/蒙特婁奧林匹克體育場.md "wikilink")。

球隊最高的勝率是在1994年（0.649），但當時因為罷工事件而沒有舉行季後賽。在這之後一直到2001年為止，勝率都在五成以下。在2002年和2005年之間，博覽會換新的老闆，並在2005年改名為華盛頓國民，這是1972年華盛頓參議員改為[德克薩斯遊騎兵以後](../Page/德克薩斯遊騎兵_\(棒球\).md "wikilink")，再度回到華盛頓的球隊。

球隊參加過的季後賽是在[1981年國家聯盟冠軍賽以及](../Page/1981年國家聯盟冠軍賽.md "wikilink")[2012年國家聯盟分區賽](../Page/2012年國家聯盟分區賽.md "wikilink")。2008年球隊搬到新建立的國民球場，結束使用三年的RFK體育場（Robert
F. Kennedy Memorial
Stadium），新的球場可以看到[美國國會大廈](../Page/美國國會大廈.md "wikilink")
\[1\]。

## 蒙特婁博覽會的歷史

### 1969年－1990年

蒙特婁博覽會創立於1969年，屬於[國家聯盟](../Page/國家聯盟.md "wikilink")，[聖地牙哥教士也在此時創立](../Page/聖地牙哥教士.md "wikilink")。當時球隊成立Charles
Bronfman幫忙出了許多的力，其最大的股東是西格拉姆（Seagram）企業。會使用博覽會這個名字當球隊名字，是因在1967年時蒙特婁舉辦了[世界博覽會](../Page/世界博覽會.md "wikilink")。博覽會的第一個主場是賈瑞公園體育場，總教練是Gene
Mauch。成立的第一年球季，博覽會輸掉了110場的比賽，接下來博覽會很努力的想要打出好成績，球隊創立初期的十年其間，只有第一個球季和1976年（輸掉107場比賽）的球季，輸掉100場以上的比賽。1977年球季開始之前，博覽會啟用新的主場為[蒙特婁奧林匹克體育場](../Page/蒙特婁奧林匹克體育場.md "wikilink")，這個主場在前一年主辦[1976年夏季奧林匹克運動會](../Page/1976年夏季奧林匹克運動會.md "wikilink")。在1979年，博覽會打出他們最佳的成績，共獲得95場的勝利，接著在1980年球季，球隊的主力球員使用年輕的球員，如捕手Gary
Carter、外野手Tim Raines、Ellis Valentine及Andre Dawson、三壘手Tim Wallach和投手Jeff
Reardon、Steve Rogers和 Bill
Gullickson。球隊獲得的第一個冠軍是在1981年的國家聯盟東區冠軍，也是球隊第一次進入季後賽，但是在[1981年國家聯盟冠軍賽](../Page/1981年國家聯盟冠軍賽.md "wikilink")，博覽會被[洛杉磯道奇以](../Page/洛杉磯道奇.md "wikilink")2勝3敗被淘汰。接下來博覽會的8年球季的成績，皆在分區排名第三名或第四名徘徊。1985年博覽會任命Buck
Rodgers為球隊新的總教練。新的總教練在這6年期間，球隊有5年的勝率皆在五成以上，勝率最高是在1987年獲得91勝的成績，雖然排名分區第三名，但是和排名第一名的[聖路易紅雀只有](../Page/聖路易紅雀.md "wikilink")4場的勝差。

### 1990年－2004年

1991年Charles Bronfman將博覽會所持的股份賣給總經理Claude Brochu\[2\]\[3\]，而Gene
Mauch則擔任球隊新的總經理，並在1992年由Felipe
Alou擔任球隊的總教練，這是大聯盟歷史上第一位在[多明尼加出生的人擔任總教練](../Page/多明尼加.md "wikilink")\[4\]，在Alou的帶領下，球隊在1994年贏得隊史上最高的勝率（0.649），當時著名的球員有Larry
Walker、Moisés Alou、Marquis
Grissom和[佩卓·馬丁尼茲](../Page/佩卓·馬丁尼茲.md "wikilink")，當年因為發生了罷工事件，球季最後的幾場比賽和季後賽被迫取消，而無緣進入隊史第二次的季後賽。在這之後博覽會幾位關鍵球員離開，球迷的鼓勵和支持也隨之減少。1999年Brochu將球隊賣給Jeffrey
Loria
\[5\]\[6\]，但是Loria並沒有認真經營球隊，在2000年時，球隊還失去電視和英文廣播收音機的合約，也因此博覽會在最後的幾年，經營陷入困境當中。2002年11月，大聯盟球隊的老闆們舉行會議，討論縮小聯盟的規模，博覽會和明尼蘇達雙城被討論是否要被縮減\[7\]。最後由大聯盟機關以1億2000萬美金買下博覽會的股份，使球隊可以繼續營運下去。在這之後博覽會開始有遷隊的計畫，選擇的地點包括[奧克拉荷馬市](../Page/奧克拉荷馬市.md "wikilink")、[華盛頓哥倫比亞特區](../Page/華盛頓哥倫比亞特區.md "wikilink")、[聖胡安
(波多黎各)](../Page/聖胡安_\(波多黎各\).md "wikilink")、[蒙特雷](../Page/蒙特雷.md "wikilink")、[波特蘭
(奧勒岡州)](../Page/波特蘭_\(奧勒岡州\).md "wikilink")、[諾福克
(維吉尼亞州)](../Page/諾福克_\(維吉尼亞州\).md "wikilink")、[紐澤西州和](../Page/紐澤西州.md "wikilink")[夏洛特
(北卡羅萊那州)等城市](../Page/夏洛特_\(北卡羅萊那州\).md "wikilink")。最後在2004年9月29日由大聯盟正式宣佈博覽會將在2005年遷往華盛頓哥倫比亞特區\[8\]。博覽會的最後一場比賽是在2004年10月3日，對手為[紐約大都會](../Page/紐約大都會.md "wikilink")，最後博覽會以1：8輸給大都會。

## 華盛頓國民的歷史

### 2005年－2006年

[BushAsAPitcher.jpg](https://zh.wikipedia.org/wiki/File:BushAsAPitcher.jpg "fig:BushAsAPitcher.jpg")。\]\]
2005年4月4日，是博覽會改為華盛頓國民的第一場比賽，在客場面對同分區的[費城費城人](../Page/費城費城人.md "wikilink")，球隊擔任第一位打者的是Brad
Wilkerson，比賽最後的結果是由費城人以8：4擊敗國民，費城人先發投手Jon Lieber主投5.2局拿下勝投，而打擊者Kenny
Lofton擊出3分全壘打，這場比賽中，Terrmel
Sledge擊出國民的第1發全壘打。4月6日，國民獲得今年的第一場勝利，以7：3擊敗費城人，而Brad
Wilkerson在這場比賽有[完全打擊的表現](../Page/完全打擊.md "wikilink")。4月14日是國民在主場舉行的第一場比賽，對手為[亞利桑那響尾蛇](../Page/亞利桑那響尾蛇.md "wikilink")，這場比賽邀請美國總統[小布希擔任開球貴賓](../Page/喬治·沃克·布希.md "wikilink")，這是95年前[威廉·霍華德·塔夫脫擔任開球貴賓以後](../Page/威廉·霍華德·塔夫脫.md "wikilink")，再度有美國總統擔任開球貴賓。這場比賽共有45,596名球員進場觀賞比賽，比賽結果國民以5：3擊敗響尾蛇，拿下主場的第一場勝利。8月4日對洛杉磯道奇的比賽，Brad
Wilkerson擊出[滿壘全壘打](../Page/滿壘全壘打.md "wikilink")，幫助球隊以7：0贏得完封勝。今年國民的最後一場比賽也是對上費城人，在自己的主場舉行，但最後是以5：4輸給[費城人結束今年的球季](../Page/費城費城人.md "wikilink")。

2006年6月18日為美國的父親節，這場比賽共有45,157名的觀眾進場看比賽，對手為[紐約洋基](../Page/紐約洋基.md "wikilink")，比賽最後是以[萊恩·齊默爾曼從洋基的王牌](../Page/萊恩·齊默爾曼.md "wikilink")[投手](../Page/投手.md "wikilink")[王建民手中敲出](../Page/王建民.md "wikilink")2分[再見全壘打](../Page/再見全壘打.md "wikilink")，球隊以3：2獲勝，而王建民吞下完投敗。9月4日，Ramón
Ortiz先發對上[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")，前8局是無安打比賽，在第9局被[亞伯特·普荷斯擊出全壘打破了無安打比賽](../Page/亞伯特·普荷斯.md "wikilink")，不過Ramón
Ortiz還是以4：1拿下勝投。9月16日，[阿方索·索利安諾](../Page/阿方索·索利安諾.md "wikilink")
在這場對[密爾瓦基釀酒人的比賽中](../Page/密爾瓦基釀酒人.md "wikilink")，成為大聯盟第四位40－40俱樂部（40發全壘打和40次盜壘成功）的成員，其他三位球員為1988年的[奧克蘭運動家的Jose](../Page/奧克蘭運動家.md "wikilink")
Canseco、1996年的[舊金山巨人的](../Page/舊金山巨人.md "wikilink")[貝瑞·邦茲和](../Page/貝瑞·邦茲.md "wikilink")1998年[西雅圖水手的](../Page/西雅圖水手.md "wikilink")[艾力士·羅德里奎茲](../Page/艾力士·羅德里奎茲.md "wikilink")。

### 2007年

在2007年球季開始前，國民失去四位先發投手，包括Liván Hernández、Tony Armas、Ramón Ortiz和Pedro
Astacio。為了能夠彌補這一個缺口，國民一共邀請36位投手參與春訓，這是很少見到的\[9\]\[10\]。在開幕戰的時候。國民失去他們的先發游擊手Cristian
Guzmán（[腿後腱受傷](../Page/腿後腱.md "wikilink")）和中外野手Nook
Logan總共五個禮拜的時間。在4月結束的時候，國民的先發投手Jerome
Williams因踝受傷，也跟著進入15天傷兵名單。如此在5月的10天裡面，Shawn Hill、John Patterson和Jason
Bergmann也跟著進入傷兵名單。Jerome
Williams在5月從傷兵名單回到球場先發的第一場比賽，又因為肩膀受傷而再度回到傷兵名單直到球季結束。[華盛頓郵報寫著](../Page/華盛頓郵報.md "wikilink")：幾乎每件不好的狀況都發生在國民身上，在1勝8敗以後，他們又連續輸掉八場比賽變成9勝25敗\[11\]。國民缺乏有實力的先發投手，幾乎都讓新人從救援投手變成先發投手，當時很多人不看好國民的成績，推測國民在2007年的成績將打破[紐約大都會在](../Page/紐約大都會.md "wikilink")1962年的120敗成績\[12\]，並認為國民將成為2003年[底特律老虎在](../Page/底特律老虎.md "wikilink")[美國聯盟的](../Page/美國聯盟.md "wikilink")43勝119敗的最爛成績紀錄，成為[國家聯盟最爛的比賽成績紀錄](../Page/國家聯盟.md "wikilink")，不過在這之後國民打出一波好成績，42場比賽裡面拿下24勝18敗的成績。6月25日Jason
Bergmann脫離傷兵名單並出場比賽，但是他們第一棒的游擊手打擊者Cristian
Guzmán卻因為拇指受傷而只能成為代跑者（受傷前的打擊率為0.329）。5月12日對佛羅里達馬林魚的比賽中，於9局下2出局時，[萊恩·齊默爾曼擊出再見滿貫全壘打](../Page/萊恩·齊默爾曼.md "wikilink")，在這場比賽中，Austin
Kearns也擊出了國民隊史的第一支場內全壘打。8月7日，[貝瑞·邦茲從國民的先發投手Mike](../Page/貝瑞·邦茲.md "wikilink")
Bacsik擊出打破[漢克·阿倫所保持記錄的第](../Page/漢克·阿倫.md "wikilink")756支全壘打\[13\]，不過這場比賽國民還是以8：6贏得比賽勝利。今年球季主場的最後一場比賽，國民以5：3打敗[費城費城人](../Page/費城費城人.md "wikilink")。在2007年球季結束以後，國民拿下73勝89敗的成績，他們在本季對大都會最後的六場比賽裡面贏了五場，也造成大都會崩潰而無法進入季後賽。

### 2008年－2011年

[Washingtonnational2008steam.JPG](https://zh.wikipedia.org/wiki/File:Washingtonnational2008steam.JPG "fig:Washingtonnational2008steam.JPG")
2008年3月30日，為國民新主場國民體育場（Nationals
Park）的開幕戰，要面對的對手為[亞特蘭大勇士](../Page/亞特蘭大勇士.md "wikilink")。這場比賽的開球貴賓為美國總統小布希，接球的球員為總教練Manny
Acta。這場比賽最後的結果是在9局下的時候，由[萊恩·齊默爾曼從勇士的投手](../Page/萊恩·齊默爾曼.md "wikilink")[彼得·莫伊蘭擊出再見全壘打](../Page/彼得·莫伊蘭.md "wikilink")，幫助國民以3：2獲得勝利，也拿下主場開幕戰勝利。在國民新主場擊出第1支安打和獲得打點的是Cristian
Guzmán，獲得第1個得分的是[尼克·詹森](../Page/尼克·詹森.md "wikilink")，擊出第1發全壘打的是勇士的[奇伯·瓊斯](../Page/奇伯·瓊斯.md "wikilink")，第一場比賽的先發投手是Odalis
Pérez和勇士的Tim Hudson。

2009年7月27日，Josh
Willingham在客場面對[密爾瓦基釀酒人的比賽中](../Page/密爾瓦基釀酒人.md "wikilink")，單場擊出2發滿貫全壘打，第1發是對[傑夫·蘇潘](../Page/傑夫·蘇潘.md "wikilink")，第2發是對Mark
DiFelice所擊出來的，最後國民就以14：6大勝，Willingham是大聯盟歷史上第13位有此表現的球員。2009年10月4日，是國民該年球季的最後一場比賽，一直比賽到第15局的時候，國民才以2：1獲得比賽勝利。

### 一輪遊魔咒

#### 2012:暌違79年的季後賽

9月20日，國民以4：1擊敗[洛杉磯道奇後](../Page/洛杉磯道奇.md "wikilink")，確定晉級季後賽，而這也是華盛頓地區暌違79年以後，再度有棒球隊晉級季後賽\[14\]。

10月1日，由於[亞特蘭大勇士輸球的關係](../Page/亞特蘭大勇士.md "wikilink")，國民確定拿下國聯東區的冠軍\[15\]。

10月3日，球季例行賽最後一戰，國民以5：1擊敗[費城費城人](../Page/費城費城人.md "wikilink")，當年的戰績為98勝64敗，確定為國家聯盟的第一種子，同時也是今年大聯盟所有球隊裡面戰績最佳的\[16\]。

國民在[國家聯盟分區賽的對手為](../Page/2012年國家聯盟分區賽.md "wikilink")[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")，在前3場比賽國民以1勝2敗處於落後的局面，在第4場比賽的時候，靠著[傑森·沃斯在第](../Page/傑森·沃斯.md "wikilink")9局下的再見全壘打，將比賽扳平。第5場關鍵的比賽，國民在前3局取的6：0的領先，但之後投手壓不住憤怒鳥的攻勢，比數一路被追近，在第9局還差一個出局數就可以晉級的國民，結果卻被紅雀連得4分，最後以7：9被逆轉淘汰，無緣晉級國家聯盟冠軍賽\[17\]。

#### 2014年

2014年9月17日，國民客場以3：0完封勇士，3年內第2度拿下國聯東區冠軍。

2014年10月4日(NLDS第2戰)，國民隊在主場迎戰舊金山巨人，纏鬥到18局，總共打了6小時又23分鐘，打破大聯盟史上打最久的季後賽紀錄，不過最終以1:2輸球。

2014年10月7日，國民隊在客場以2:3輸給巨人，在NLDS以1勝3敗遭到淘汰。

#### 2015年

季前成功用7年2.1億美元的合約招來強投[麥斯·薛澤](../Page/麥斯·薛澤.md "wikilink")，國民原本也被看好能在國聯東區完成連霸，但最後事與願違，反倒讓大都會搭上季後賽列車。

毫無疑問，國民在這個球季的表現令人失望，也因此國民教練團在球季結束後全部下台，由前紅人總教練Dusty Baker擔任2016賽季的總教練。

台灣時間的聖誕節，國民為了填補防線，用一紙3年3750萬美元合約簽下阻止小熊回到未來的「山羊先生」Daniel Murphy。

#### 2016年

先發輪值發威，打線靠著山羊先生帶頭衝，國民很快便站上分區龍頭的位置，即使大都會與馬林魚都進行補強，依然望塵莫及。

季中交易，國民從海盜換來終結者Mark Melancon。

2016年9月25日，國民客場以8：1擊敗海盜，同一時間大都會以8：10輸給費城人，5年內第3度拿下國聯東區冠軍。

首戰對手為洛杉磯道奇，國民在先聽牌的情況下遭到翻盤，以2:3落敗，5年內第3度以國聯東區冠軍之姿在NLDS落敗。

#### 2017年

華盛頓國民隊主戰先發投手史特拉斯堡(Stephen
Strasburg)在今天主場對費城人之戰，投8局只被敲2支安打，投出1次四壞和10次三振，帶領球隊以3比2驚險獲勝，大約1個半小時後，勇士在延長11局下靠著亞當斯(Lane
Adams)的再見2分彈，以10比8力克馬林魚，國民因此提早登上國聯東區王座。也是首次完成分區冠軍二連霸。而國民隊也將會在國聯分區系列賽出戰衛冕軍芝加哥小熊隊。美國職棒大聯盟今(7)日進行小熊對戰國民系列賽的首戰，衛冕軍小熊此役靠著先發投手Kyle
Hendricks主投7局無失分的好表現，加上當家三壘手Kris
Bryant於6局敲出重要安打、打回致勝分，終場就以3：0完封對手，客場首戰就開紅盤。

國聯分區系列賽在主場和小熊進行第2戰，國民靠著8局下Bryce Harper和Ryan
Zimmerman的全壘打，一口氣逆轉比賽，最後以6:3擊敗小熊，也將系列賽扳平，避免被小熊隊聽牌。

Max
Scherzer似乎已經不受傷勢影響，為華盛頓國民繳出6.1局好投。但芝加哥小熊在他退場後展開反擊，於7、8局各打下1分逆轉，以2比1在主場獲勝，搶下2勝1敗系列賽聽牌優勢。

華盛頓國民跟芝加哥小熊的比賽延賽一天以後開打，國民一比零領先僵持到第八局敲出了滿貫全壘打，五比零完封衛冕隊小熊。小熊全場只有三支安打卻發生了兩次失誤，前四場系列戰一共累計六次失誤。

系列賽最終戰的國民隊一度取得領先，在5局上換上王牌Max Scherzer上來中繼，但沒想到卻成為本場比賽的轉捩點，小熊靠著Addison
Russell的關鍵二壘安打，單局灌進4分逆轉戰局，也再度重演國民在2012年慘遭紅雀翻盤的噩夢，最終小熊以9:8一分之差險勝，國民最終淘汰。最後貝克遭到撤換，國民總經理Mike
Rizzo表示「我們期待更多，贏很多場例行賽或分區系列賽是不夠的，我們的目標是世界大賽冠軍。」。貝克也對此表示，他感到意外且失望，想不通為什麼球隊會做出此決定。

## 棒球名人堂球員

<center>

<table>
<thead>
<tr class="header">
<th><p><strong>華盛頓國民名人堂成员</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>隶属于<a href="../Page/棒球名人堂.md" title="wikilink"><font color="white">棒球名人堂</font></a></strong></p></td>
</tr>
<tr class="even">
<td><center>
<table>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/Gary_Carter.md" title="wikilink">Gary Carter</a></strong><br />
<strong><a href="../Page/Tony_Perez.md" title="wikilink">Tony Perez</a></strong><br />
</p></td>
<td></td>
<td><p><a href="../Page/Andre_Dawson.md" title="wikilink">Andre Dawson</a><br />
<a href="../Page/法蘭克·羅賓森.md" title="wikilink">法蘭克·羅賓森</a></p></td>
<td></td>
<td><p><a href="../Page/Dick_Williams.md" title="wikilink">Dick Williams</a></p></td>
</tr>
</tbody>
</table>
</center>
<dl>
<dt></dt>
<dd><strong>粗体</strong> 表示以國民或博覽會队球员身份进入名人堂
</dd>
</dl></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

</center>

## 退休號碼

  - 42 [傑基·羅賓森](../Page/傑基·羅賓森.md "wikilink")（所有大聯盟球隊均將此號碼退休）

註：在博覽會時代，有4個號碼被退休（包括傑基·羅賓森的42號），其餘3個已經被國民隊球員所使用。但在2011年賽季，球隊宣布重新承認該3個退休號碼。

## 登錄名單

## 蒙特婁博覽會的標誌


\==附屬小聯盟球隊==

  - **3A：** [雪城酋長](../Page/雪城酋長.md "wikilink") (Syracuse
    Chiefs)，[國際聯盟](../Page/國際聯盟.md "wikilink") (International
    League)
  - **2A：** [哈里斯堡參議員](../Page/哈里斯堡參議員.md "wikilink") (Harrisburg
    Senators), [東方聯盟](../Page/東方聯盟.md "wikilink") (Eastern League)
  - **高階1A：** [波多馬克國民](../Page/波多馬克國民.md "wikilink") (Potomac
    Nationals)，[卡羅萊那聯盟](../Page/卡羅萊那聯盟.md "wikilink") (Carolina
    League)
  - **1A：** [沙瓦那沙蟲](../Page/沙瓦那沙蟲.md "wikilink") (Savannah Sand Gnats),
    [南大西洋聯盟](../Page/南大西洋聯盟.md "wikilink") (South Atlantic League)
  - **短期1A：** [佛蒙特博覽會](../Page/佛蒙特博覽會.md "wikilink") (Vermont Lake
    Monsters), [紐約─賓州聯盟](../Page/紐約─賓州聯盟.md "wikilink") (New York-Penn
    League)
  - **新人聯盟：** [灣岸國民](../Page/灣岸國民.md "wikilink") (Gulf Coast Nationals),
    [灣岸聯盟](../Page/灣岸聯盟.md "wikilink") (Gulf Coast League)
  - **新人聯盟：** [VSL國民](../Page/VSL國民.md "wikilink") (VSL
    Nationals)，[委內瑞拉夏季聯盟](../Page/委內瑞拉夏季聯盟.md "wikilink")
    (Venezuelan Summer League)

## 參考資料

## 外部連結

  - [華盛頓國民隊官方網站](http://washington.nationals.mlb.com/NASApp/mlb/index.jsp?c_id=was)
  - [ESPN.com-Washington
    Nationals-Roster](http://sports.espn.go.com/mlb/teams/roster?team=was)
  - [Expos New Name: Nationals
    (TSN)](http://www.tsn.ca/mlb/news_story.asp?ID=105200&hubName=mlb)

[W](../Category/美國職棒大聯盟球隊.md "wikilink")
[W](../Category/華盛頓哥倫比亞特區體育.md "wikilink")
[Category:1969年建立](../Category/1969年建立.md "wikilink")
[\*](../Category/華盛頓國民.md "wikilink")

1.  [- Nationals victorious in stadium
    debut](http://mlb.mlb.com/news/gameday_recap.jsp?ymd=20080329&content_id=2465865&vkey=recap&fext=.jsp&c_id=mlb)

2.

3.

4.
5.

6.

7.

8.

9.

10.

11.

12.

13.

14. <http://www.washingtonpost.com/sports/nationals/washington-nationals-clinch-mlb-playoff-spot-with-4-1-win-over-los-angeles-dodgers/2012/09/20/847bf182-038b-11e2-91e7-2962c74e7738_story.html?hpid=z2>

15. <http://scores.espn.go.com/mlb/recap?gameId=321001120>

16. <http://scores.espn.go.com/mlb/recap?gameId=321003120>

17.