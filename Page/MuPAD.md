**MuPAD**（**Mu**lti **P**rocessing **A**lgebra **D**ata
Tool）是一个商用[计算机代数系统](../Page/计算机代数系统.md "wikilink")。最初由[德国](../Page/德国.md "wikilink")[帕德博恩大学](../Page/帕德博恩大学.md "wikilink")（Universität
Paderborn）Benno Fuchsteiner教授的MuPAD研究小组开发而成，1997年以来，其开发由该研究小组与德国SciFace
Software有限公司共同承担。2008年9月，SciFace公司被[The
MathWorks公司收购](../Page/The_MathWorks.md "wikilink")，从此MuPAD作为符号数学工具箱（Symbolic
Math Toolbox）被包含在[MATLAB当中](../Page/MATLAB.md "wikilink")。

[Enneper.gif](https://zh.wikipedia.org/wiki/File:Enneper.gif "fig:Enneper.gif")

## 功能

  - [数学](../Page/数学.md "wikilink")[表达式的](../Page/表达式.md "wikilink")[符号运算](../Page/符号运算.md "wikilink")，
  - 有[线性代数](../Page/线性代数.md "wikilink")，[微分方程](../Page/微分方程.md "wikilink")，[数论](../Page/数论.md "wikilink")，[统计等多达数十种的程序包](../Page/统计.md "wikilink")，
  - 互动的[图像界面](../Page/图像界面.md "wikilink")，
  - 任意精确度的[数值分析](../Page/数值分析.md "wikilink")，
  - 亦可以连接[Java程序](../Page/Java.md "wikilink")。

## MuPAD代码举例

[矩阵](../Page/矩阵.md "wikilink")[运算](../Page/运算.md "wikilink")

` `<small>`A := matrix( [[1,2],[3,4|1,2],[3,4]] )`</small>
` `<small>`A+A, -A, 2*A, A*A, A^-1, exp( A ), A.A, A^0, 0*A`</small>

## 参见

  - [Maple](../Page/Maple.md "wikilink")
  - [Mathematica](../Page/Mathematica.md "wikilink")
  - [Maxima](../Page/Maxima.md "wikilink")
  - [MATLAB](../Page/MATLAB.md "wikilink")
  - [GNU Octave](../Page/GNU_Octave.md "wikilink")
  - [Scilab](../Page/Scilab.md "wikilink")

## 外部連結

  - [MuPAD Website](http://www.mupad.de/)
  - [SciFace Software GmbH](http://www.sciface.com/)
  - [„MuPAD in Schule und Studium“Website](http://schule.mupad.de/)
  - [Artikel im Linux-Magazin über
    MuPAD](https://web.archive.org/web/20060101173031/http://www.linux-magazin.de/Artikel/ausgabe/2002/06/mupad/mupad.html)

[Category:2008年廢除](../Category/2008年廢除.md "wikilink")
[Category:Linux计算机代数系统软件](../Category/Linux计算机代数系统软件.md "wikilink")
[Category:计算机代数系统](../Category/计算机代数系统.md "wikilink")
[Category:已停止開發的軟體](../Category/已停止開發的軟體.md "wikilink")
[Category:1997年面世的產品](../Category/1997年面世的產品.md "wikilink")
[Category:Linux专有商业软件](../Category/Linux专有商业软件.md "wikilink")