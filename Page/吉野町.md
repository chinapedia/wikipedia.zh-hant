**吉野町**（）是[奈良縣中部的一](../Page/奈良縣.md "wikilink")[町](../Page/町.md "wikilink")，在[南北朝時代時為南朝的朝廷](../Page/南北朝時代_\(日本\).md "wikilink")（首都）所在。

## 參見

  - [兩統迭立](../Page/兩統迭立.md "wikilink")
  - [南北朝時代 (日本)](../Page/南北朝時代_\(日本\).md "wikilink")

[分類:日本舊都](../Page/分類:日本舊都.md "wikilink")