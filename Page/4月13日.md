**4月13日**是公历年中的第103天（[闰年的第](../Page/闰年.md "wikilink")104天），离全年结束还有262天。

## 大事记

### 7世紀

  - [675年](../Page/675年.md "wikilink")：[唐高宗诏令](../Page/唐高宗.md "wikilink")[武则天摄国政](../Page/武则天.md "wikilink")。

### 13世纪

  - [1204年](../Page/1204年.md "wikilink")：[十字军攻陷](../Page/十字军.md "wikilink")[君士坦丁堡并进行屠杀](../Page/君士坦丁堡.md "wikilink")。

### 16世紀

  - [1598年](../Page/1598年.md "wikilink")：[法国国王](../Page/法国国王.md "wikilink")[亨利四世颁布](../Page/亨利四世_\(法兰西\).md "wikilink")[南特敕令](../Page/南特敕令.md "wikilink")，允许[雨格诺派教徒](../Page/雨格诺派.md "wikilink")[信仰自由](../Page/信仰自由.md "wikilink")。

### 18世紀

  - [1742年](../Page/1742年.md "wikilink")：[德国作曲家](../Page/德国.md "wikilink")[亨德尔创作的](../Page/格奥尔格·弗里德里希·亨德尔.md "wikilink")[清唱剧](../Page/神剧.md "wikilink")《[弥赛亚](../Page/弥赛亚_\(亨德尔\).md "wikilink")》在[爱尔兰](../Page/爱尔兰.md "wikilink")[都柏林首演](../Page/都柏林.md "wikilink")。

### 20世紀

  - [1919年](../Page/1919年.md "wikilink")：[印度发生](../Page/印度.md "wikilink")[阿姆利則慘案](../Page/阿姆利則慘案.md "wikilink")。
  - [1919年](../Page/1919年.md "wikilink")：[巴伐利亚苏维埃共和国建立](../Page/巴伐利亚苏维埃共和国.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[冼星海創作](../Page/冼星海.md "wikilink")《[黄河大合唱](../Page/黄河大合唱.md "wikilink")》在[陝西省](../Page/陝西省.md "wikilink")[延安首演](../Page/延安.md "wikilink")。
  - [1943年](../Page/1943年.md "wikilink")：[納粹德國宣布在](../Page/納粹德國.md "wikilink")[蘇聯卡廷森林发现](../Page/蘇聯.md "wikilink")[苏军埋葬](../Page/苏联红军.md "wikilink")[波兰](../Page/波兰.md "wikilink")[战俘的](../Page/战俘.md "wikilink")[万人坑](../Page/万人坑.md "wikilink")，[卡廷大屠殺被揭露](../Page/卡廷大屠殺.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：[蘇聯收复](../Page/蘇聯.md "wikilink")[刻赤半岛](../Page/刻赤半岛.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[黄河](../Page/黄河.md "wikilink")[三门峡水利枢纽工程正式开工](../Page/三门峡水利枢纽.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[萬國郵政聯盟决议接纳](../Page/萬國郵政聯盟.md "wikilink")[中华人民共和国为正式成员国](../Page/中华人民共和国.md "wikilink")，取消对[中华民国的认同](../Page/中华民国.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[香港](../Page/香港.md "wikilink")[文憑教師發動](../Page/文憑教師.md "wikilink")[罷課](../Page/文憑教師薪酬運動.md "wikilink")，三位宗教領袖出面調停<ref>[1](http://hk.apple.nextmedia.com/news/art/20120731/16561964)，歷史抗爭

教協30年來三次罷課。</ref>。

  - [1975年](../Page/1975年.md "wikilink")：[乍得发生](../Page/乍得.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[唐代高僧](../Page/唐代.md "wikilink")[鉴真坐像从](../Page/鉴真.md "wikilink")[日本运往](../Page/日本.md "wikilink")[扬州](../Page/扬州.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[印度军队抢先占领](../Page/印度.md "wikilink")[克什米尔的](../Page/克什米尔.md "wikilink")[锡亚琴冰川](../Page/锡亚琴冰川.md "wikilink")，随后与[巴基斯坦军队展开争夺该地区的战斗](../Page/巴基斯坦.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[中国與](../Page/中華人民共和國.md "wikilink")[葡萄牙政府在](../Page/葡萄牙.md "wikilink")[北京簽署](../Page/北京.md "wikilink")《[中葡聯合聲明](../Page/中葡聯合聲明.md "wikilink")》，協議將[澳門治權於](../Page/澳門.md "wikilink")[1999年](../Page/1999年.md "wikilink")[12月20日交還中國](../Page/12月20日.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[第七屆全國人民代表大會第一次會議通過關於設立](../Page/第七屆全國人民代表大會.md "wikilink")[海南省的決定及建立](../Page/海南省.md "wikilink")[海南經濟特區的決議](../Page/海南經濟特區.md "wikilink")。

### 21世紀

  - [2012年](../Page/2012年.md "wikilink")：[朝鲜最高人民会议推举](../Page/朝鲜最高人民会议.md "wikilink")[金正恩为](../Page/金正恩.md "wikilink")[朝鲜国防委员会第一委员长](../Page/朝鲜国防委员会第一委员长.md "wikilink")。此前[4月11日](../Page/4月11日.md "wikilink")，[朝鲜劳动党第四次代表会议推举其为](../Page/朝鲜劳动党第四次代表会议.md "wikilink")[朝鲜劳动党中央委员会第一书记](../Page/朝鲜劳动党中央委员会第一书记.md "wikilink")。
  - 2012年：朝鲜[光明星3号通讯卫星在平安北道](../Page/光明星3号.md "wikilink")[铁山郡由](../Page/铁山郡.md "wikilink")[银河3号运载火箭搭载升空](../Page/银河3号运载火箭.md "wikilink")，其后由于运载火箭发生爆炸而坠毁。
  - [2015年](../Page/2015年.md "wikilink")：[私人國家](../Page/私人國家.md "wikilink")[利伯自由共和國在](../Page/利伯自由共和國.md "wikilink")[克羅地亞和](../Page/克羅地亞.md "wikilink")[塞爾維亞邊界的西加成立](../Page/塞爾維亞.md "wikilink")。

## 出生

  - [1519年](../Page/1519年.md "wikilink")：[凯瑟琳·德·美第奇](../Page/凯瑟琳·德·美第奇.md "wikilink")，法国王后。（[1589年逝世](../Page/1589年.md "wikilink")）
  - [1559年](../Page/1559年.md "wikilink")：[松平信康](../Page/松平信康.md "wikilink")，[日本](../Page/日本.md "wikilink")[戰國時代武將](../Page/戰國時代_\(日本\).md "wikilink")。（[1579年逝世](../Page/1579年.md "wikilink")）
  - [1570年](../Page/1570年.md "wikilink")：[盖伊·福克斯](../Page/盖伊·福克斯.md "wikilink")，[天主教阴谋组织成员](../Page/天主教.md "wikilink")。（[1606年逝世](../Page/1606年.md "wikilink")）
  - [1732年](../Page/1732年.md "wikilink")：[諾斯勳爵](../Page/諾斯勳爵.md "wikilink")，[英國首相](../Page/英國首相.md "wikilink")。（[1792年逝世](../Page/1792年.md "wikilink")）
  - [1743年](../Page/1743年.md "wikilink")：[托马斯·杰斐逊](../Page/托马斯·杰斐逊.md "wikilink")，[美国资产阶级革命家](../Page/美国.md "wikilink")、第三任[美国总统](../Page/美国总统.md "wikilink")。（[1826年逝世](../Page/1826年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[赵世炎](../Page/赵世炎.md "wikilink")，[中国共产党早期革命活动家](../Page/中国共产党.md "wikilink")、工人運動領袖。（[1927年逝世](../Page/1927年.md "wikilink")）
  - [1934年](../Page/1934年.md "wikilink")：[劉興欽](../Page/劉興欽.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")、[發明家](../Page/發明家.md "wikilink")
  - [1939年](../Page/1939年.md "wikilink")：[谢默斯·希尼](../Page/谢默斯·希尼.md "wikilink")，爱尔兰作家、诗人。（[2013年逝世](../Page/2013年.md "wikilink")）
  - [1940年](../Page/1940年.md "wikilink")：[劉家昌](../Page/劉家昌.md "wikilink")，台灣[音樂人](../Page/音乐人.md "wikilink")
  - [1942年](../Page/1942年.md "wikilink")：[黃星華](../Page/黃星華.md "wikilink")，[香港政府官员](../Page/香港.md "wikilink")（[2012年逝世](../Page/2012年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[朗·柏曼](../Page/朗·柏曼.md "wikilink")，[美國男演員](../Page/美國.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[威廉·托馬斯·桑德勒](../Page/威廉·托馬斯·桑德勒.md "wikilink")，[美國男演員](../Page/美國.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[加里·卡斯帕罗夫](../Page/加里·卡斯帕罗夫.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")[国际象棋世界冠军](../Page/国际象棋.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[邵傳勇](../Page/邵傳勇.md "wikilink")，香港演員
  - [1972年](../Page/1972年.md "wikilink")：[陳寶玲](../Page/貴花田_\(電台主持\).md "wikilink")，外號「貴花田」，香港電台節目主持
  - [1975年](../Page/1975年.md "wikilink")：[詹雅菁](../Page/詹雅菁.md "wikilink")，台灣女性[配音員](../Page/配音員.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[卡莱斯·普约尔](../Page/卡莱斯·普约尔.md "wikilink")，[西班牙足球运动员](../Page/西班牙.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[姚予晴](../Page/姚予晴.md "wikilink")，香港[模特兒](../Page/模特兒.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[衛蘭](../Page/衛蘭.md "wikilink")、[衛詩](../Page/衛詩.md "wikilink")，攣生姐妹，香港女歌手
  - 1982年：[林子善](../Page/林子善.md "wikilink")，香港演員
  - [1984年](../Page/1984年.md "wikilink")：[安達斯·連迪加特](../Page/安達斯·連迪加特.md "wikilink")，[丹麥](../Page/丹麥.md "wikilink")[守門員](../Page/守門員_\(足球\).md "wikilink")
  - 1984年：[水嶋宏](../Page/水嶋宏.md "wikilink")，[日本男演員](../Page/日本.md "wikilink")、作家
  - [1985年](../Page/1985年.md "wikilink")：[謝俊奇](../Page/謝俊奇.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[呂政儒](../Page/呂政儒.md "wikilink")，台灣男子籃球運動員
  - [1987年](../Page/1987年.md "wikilink")：[楊伊湄](../Page/楊伊湄.md "wikilink")，台灣主播
  - [1988年](../Page/1988年.md "wikilink")：[小蝦](../Page/陳道賢.md "wikilink")，台灣[諧星](../Page/搞笑藝人.md "wikilink")、演員
  - [1991年](../Page/1991年.md "wikilink")：[布蘭克莎·米赫露域](../Page/布蘭克莎·米赫露域.md "wikilink")，[塞爾維亞女子排球運動員](../Page/塞爾維亞.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[安吉洛·亨里克兹](../Page/安吉洛·亨里克兹.md "wikilink")，[智利足球运动员](../Page/智利.md "wikilink")
  - [2000年](../Page/2000年.md "wikilink")：[克洛伊·金](../Page/克洛伊·金.md "wikilink")，美国[单板滑雪运动员](../Page/单板滑雪.md "wikilink")

## 逝世

  - [814年](../Page/814年.md "wikilink")：[克鲁姆](../Page/克鲁姆.md "wikilink")，[保加利亚大公](../Page/保加利亚.md "wikilink")
  - [1695年](../Page/1695年.md "wikilink")：[让·德·拉封丹](../Page/让·德·拉封丹.md "wikilink")，[法国诗人](../Page/法国.md "wikilink")。（[1621年出生](../Page/1621年.md "wikilink")）
  - [1941年](../Page/1941年.md "wikilink")：[安妮·坎农](../Page/安妮·坎农.md "wikilink")，[美國女天文学家](../Page/美國.md "wikilink")，恒星光谱分类的开拓者。（[1863年出生](../Page/1863年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[莫壽增](../Page/莫壽增.md "wikilink")，[香港教區主教](../Page/香港.md "wikilink")。（[1866年出生](../Page/1866年.md "wikilink")）
  - [1956年](../Page/1956年.md "wikilink")：[俞成华](../Page/俞成华.md "wikilink")，[上海教会长老](../Page/上海.md "wikilink")，在政治風暴中因信仰殉道。（[1901年出生](../Page/1901年.md "wikilink")）
  - [1995年](../Page/1995年.md "wikilink")：[郎静山](../Page/郎静山.md "wikilink")，摄影大师（[1892年出生](../Page/1892年.md "wikilink")）
  - [1998年](../Page/1998年.md "wikilink")：[帅孟奇](../Page/帅孟奇.md "wikilink")，中国妇女运动先驱。（[1897年出生](../Page/1897年.md "wikilink")）
  - [2002年](../Page/2002年.md "wikilink")：[劉其偉](../Page/劉其偉.md "wikilink")，[臺灣畫家](../Page/臺灣.md "wikilink")、人類學家。（[1912年出生](../Page/1912年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[約翰·阿奇博爾德·惠勒](../Page/約翰·惠勒.md "wikilink")，美國[物理學家](../Page/物理學家.md "wikilink")，[黑洞一詞創造者](../Page/黑洞.md "wikilink")。（[1911年出生](../Page/1911年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[梅欣](../Page/梅欣.md "wikilink")，[香港女演员](../Page/香港.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[林洋港](../Page/林洋港.md "wikilink")，臺灣政治家（[1927年出生](../Page/1927年.md "wikilink")）\[1\]。
  - [2014年](../Page/2014年.md "wikilink")：[厄尼斯特·拉克勞](../Page/厄尼斯特·拉克勞.md "wikilink")，[阿根廷政治哲學家](../Page/阿根廷.md "wikilink")。（[1935年出生](../Page/1935年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[君特·格拉斯](../Page/君特·格拉斯.md "wikilink")，[德國](../Page/德國.md "wikilink")[作家](../Page/作家.md "wikilink")，[1999年](../Page/1999年.md "wikilink")[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")（[1927年出生](../Page/1927年.md "wikilink")）\[2\]。
  - 2015年：[愛德華多·加萊亞諾](../Page/愛德華多·加萊亞諾.md "wikilink")，[烏拉圭](../Page/烏拉圭.md "wikilink")[記者](../Page/記者.md "wikilink")、[小說家](../Page/小說家.md "wikilink")（[1940年出生](../Page/1940年.md "wikilink")）\[3\]

## 節假日與習俗

  - ：[宋干节](../Page/宋干节.md "wikilink")，即[泰曆](../Page/泰曆.md "wikilink")[新年](../Page/新年.md "wikilink")。

  - [泼水节](../Page/泼水节.md "wikilink")，是[傣族](../Page/傣族.md "wikilink")、[德昂族最盛大的传统节日](../Page/德昂族.md "wikilink")，相当于[中国的](../Page/中国.md "wikilink")[新年](../Page/新年.md "wikilink")。

## 參考資料

1.  [林洋港病逝 享壽87歲 | 重點新聞 | 中央社即時新聞 CNA
    NEWS](http://www.cna.com.tw/News/FirstNews/201304140001-1.aspx)
2.
3.