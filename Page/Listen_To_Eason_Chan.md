[2007ListenToEasonChanWhite.jpg](https://zh.wikipedia.org/wiki/File:2007ListenToEasonChanWhite.jpg "fig:2007ListenToEasonChanWhite.jpg")

《**Listen To Eason
Chan**》為[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[陳奕迅的](../Page/陳奕迅.md "wikilink")[粵語](../Page/粵語.md "wikilink")[音樂專輯](../Page/音樂.md "wikilink")，於2007年10月18日正式發行，內地及台灣版本專輯名為《**聽陳奕迅吧**》。

## 專輯簡介

陳奕迅近期在音樂上的新嘗試，就是推出跳舞歌曲專輯。甚少推出跳舞組曲的他，選擇嘗試推出一張令人熱舞的專輯。本專輯內收錄的歌曲，絕大部分都是較快版的[電子音樂](../Page/電子音樂.md "wikilink")，例如《瑪利奧派對》、《熱島小夜曲》及配合07－09年以跳舞為主題的個人巡迴演唱會「Eason's
Moving On Stage World
Tour」的主題曲《演唱會》等。另外，為配合跳舞的主題，陳奕迅邀請了[英國數個著名的](../Page/英國.md "wikilink")[混音團體](../Page/混音.md "wikilink")（包括Soul
Seekerz、Wideboys Club、Digital Dog等），將全部歌曲（bonus
track《兄弟》除外）重新混音，製成另一張Remix大碟一併推出。

### 包裝設計

  - 沒有附送歌詞集\[1\]
  - 採用[T恤形狀剪裁](../Page/T恤.md "wikilink")
  - 共有厚膠版／布袋版／平裝版紙套三種包裝
  - 提供八種顏色選擇（紅、橙、黃、綠、藍、紫、黑、白）

## 曲目

### Disc 1

### Disc 2 (Remix Album)

## 音樂錄像

| 曲目次序 | 歌名                         | 導演                               | 首播日期       | 首播平台                                     | 連結                                                         |
| ---- | -------------------------- | -------------------------------- | ---------- | ---------------------------------------- | ---------------------------------------------------------- |
| 04   | 演唱會                        | \-                               | 2008年9月21日 | [Youtube](../Page/Youtube.md "wikilink") | [官方Youtube連結](http://www.youtube.com/watch?v=bvZ-qeZdrr0)  |
| 06   | Crying In The Party(官方版本)  | [莊少榮](../Page/莊少榮.md "wikilink") | 2007年      | \-                                       | [非官方Youtube連結](http://www.youtube.com/watch?v=su989Hduu7g) |
| 06   | Crying In The Party(TVB版本) | \-                               | 2007年      | 無線電視                                     | [非官方Youtube連結](http://www.youtube.com/watch?v=IOqsdHq1MiU) |
| 09   | 時代巨輪(官方版本)                 | 莊少榮                              | 2008年9月21日 | [Youtube](../Page/Youtube.md "wikilink") | [官方Youtube連結](http://www.youtube.com/watch?v=_lu07UTH86A)  |
| 09   | 時代巨輪(TVB版本)                | \-                               | 2008年      | 無線電視                                     | [非官方Youtube連結](http://www.youtube.com/watch?v=iOMEjLRaoWU) |

## 派台歌曲及四台成績

此唱片共有6首派台歌曲，《Crying In The Party》、《兄弟》、《演唱會》、《時代巨輪》、《熱島小夜曲》及《乜嘢啫》。

| 歌曲                      | 903 專業推介 | RTHK 中文歌曲龍虎榜 | 997 勁爆流行榜 | TVB 勁歌金榜 |
| ----------------------- | -------- | ------------ | --------- | -------- |
| **Crying In The Party** | **1**    | **1**        | **1**     | **1**    |
| 兄弟                      | 19       | 18           | (無派台)     | (無派台)    |
| **演唱會**                 | **1**    | **1**        | **1**     | (無派台)    |
| **時代巨輪**                | **1**    | 8            | 2         | 2        |
| 熱島小夜曲                   | 16       | (無派台)        | (無派台)     | (無派台)    |
| 乜嘢啫                     | 18       | (無派台)        | (無派台)     | (無派台)    |

**粗體**表示冠軍歌

## 相關獎項或提名

### 專輯《Listen To Eason Chan》

  - [2007年度叱吒樂壇流行榜頒獎典禮](../Page/2007年度叱吒樂壇流行榜頒獎典禮得獎名單.md "wikilink") -
    至尊唱片大獎
  - [IFPI香港唱片銷量大獎](../Page/IFPI.md "wikilink")2007 - 十大銷量廣東唱片\[2\]
  - [2007年度新城勁爆頒獎禮](../Page/2007年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆年度大碟

### 歌曲《Crying in the Party》

  - [2007年度勁歌金曲優秀選](../Page/勁歌金曲優秀選.md "wikilink") - 第三回金曲
  - 2007年度叱吒樂壇流行榜頒獎典禮 - 專業推介 叱吒十大（第二位）
  - 2007年度新城勁爆頒獎禮 - 新城勁爆歌曲
  - [2007年度SINA
    Music樂壇民意指數頒獎禮](../Page/2007年度SINA_Music樂壇民意指數頒獎禮得獎名單.md "wikilink")
    - 最高收聽率歌曲（第七位）

### 歌曲《時代巨輪》

  - [2008年度勁歌金曲優秀選](../Page/勁歌金曲優秀選.md "wikilink") - 第一回金曲
  - [2008CASH金帆音樂獎](../Page/2008_CASH金帆音樂獎.md "wikilink") - 最佳男歌手演繹獎
  - [TVB8金曲榜頒獎典禮2008](../Page/2008年度TVB8金曲榜頒獎典禮得獎名單.md "wikilink") -
    全球熱愛粵語歌曲
  - [2008年度新城勁爆頒獎禮](../Page/2008年度新城勁爆頒獎禮得獎名單.md "wikilink") - 新城勁爆歌曲

### 歌曲《兄弟》

  - [2008年香港電影金像獎](../Page/2008年香港電影金像獎.md "wikilink")－最佳原創電影歌曲（提名）

## 參考文獻

<references />

[Category:陳奕迅音樂專輯](../Category/陳奕迅音樂專輯.md "wikilink")
[Category:陳奕迅粵語專輯](../Category/陳奕迅粵語專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")

1.  [專輯簡介及歌詞下載(按黃T恤)](http://www.umg.com.hk/minisite/eason_listentoeasonchan/)
2.  [2007年度《香港唱片銷量大獎》](http://www.ifpihk.org/zh/hong-kong-top-sales-music-award-presented-01-11/hong-kong-top-sales-music-award-presented/2007)