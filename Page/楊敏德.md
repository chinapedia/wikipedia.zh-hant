**楊敏德**（Yang Mun-tak,
Marjorie，），香港商人，[溢達集團董事長](../Page/溢達集團.md "wikilink")，前[香港理工大學校董會主席](../Page/香港理工大學.md "wikilink")，曾任[香港特別行政區行政會議成員](../Page/香港特別行政區行政會議.md "wikilink")。她是[上海](../Page/上海.md "wikilink")[紡織商人](../Page/紡織.md "wikilink")[楊元龍的長女](../Page/楊元龍.md "wikilink")，商人[潘迪生的第一任妻子](../Page/潘迪生.md "wikilink")，在業界有「[配額女王](../Page/配額.md "wikilink")」之稱。

楊敏德於[香港出生](../Page/香港.md "wikilink")，並在[美國](../Page/美國.md "wikilink")[麻省理工學院](../Page/麻省理工學院.md "wikilink")[數學系畢業](../Page/數學.md "wikilink")，[哈佛大學](../Page/哈佛大學.md "wikilink")[工商管理碩士畢業](../Page/工商管理碩士.md "wikilink")。其後返回香港，協助打理家族的紡織及製衣業務，1978年成為溢達集團的掌舵人，帶領集團發展成跨國企業，現擔任集團董事長。1996年在[新疆設立紡織品公司](../Page/新疆.md "wikilink")，供應[棉花紡織品予多間跨國時裝](../Page/棉花.md "wikilink")[品牌](../Page/品牌.md "wikilink")，包括
[Ralph Lauren](../Page/Ralph_Lauren.md "wikilink")、[Hugo
Boss](../Page/Hugo_Boss.md "wikilink")、[Tommy
Hilfiger等](../Page/Tommy_Hilfiger.md "wikilink")，亦成為全球[恤衫最大生產及出口商之一](../Page/恤衫.md "wikilink")。

她亦擔任[美國](../Page/美國.md "wikilink")[吉列公司](../Page/吉列.md "wikilink")、香港[太古股份有限公司及](../Page/太古股份有限公司.md "wikilink")[匯豐銀行董事局成員](../Page/匯豐銀行.md "wikilink")，美國[紐約證券交易所國際顧問委員會會員](../Page/紐約證券交易所.md "wikilink")。

楊敏德曾經成為《[福布斯](../Page/福布斯.md "wikilink")》雜誌的[封面人物](../Page/封面人物.md "wikilink")，是繼小甜甜[龔如心外第二位成為封面人物的香港女企業家](../Page/龔如心.md "wikilink")。2006年，楊敏德在《[財富](../Page/财富_\(杂志\).md "wikilink")》[雜誌世界商界女強人排行榜中排名第](../Page/雜誌.md "wikilink")44位。

2009年1月至2012年6月任行政會議成員，是第十屆、十一屆、十二屆全國政協委員。

## 資料來源

  - [楊敏德：做工業有幸福感,414|2011.9
    信報財經月刊](http://www.hkej.com/template/magazine/jsp/cate_search.jsp?cat=2)
  - [香港行會成員](https://web.archive.org/web/20090820094303/http://www.ceo.gov.hk/exco/chi/majorie_yang.html)
  - [《財富》最新商界女強人榜出爐
    中國搶佔](http://finance.people.com.cn/BIG5/8215/71100/71106/4822507.html)

[category:前香港行政會議成員](../Page/category:前香港行政會議成員.md "wikilink")

[Category:上海人](../Category/上海人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:全国政协香港委员](../Category/全国政协香港委员.md "wikilink")
[Category:汇丰人物](../Category/汇丰人物.md "wikilink")
[Category:第十届全国政协委员](../Category/第十届全国政协委员.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十二届全国政协委员](../Category/第十二届全国政协委员.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:哈佛商學院校友](../Category/哈佛商學院校友.md "wikilink")
[M](../Category/楊姓.md "wikilink")