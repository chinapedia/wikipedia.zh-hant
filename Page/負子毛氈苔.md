**負子毛氈苔**\[1\]（[學名](../Page/學名.md "wikilink")：**）為[茅膏菜科](../Page/茅膏菜科.md "wikilink")[茅膏菜屬的](../Page/茅膏菜屬.md "wikilink")[食肉植物](../Page/食肉植物.md "wikilink")，[澳洲](../Page/澳洲.md "wikilink")[昆士蘭](../Page/昆士蘭.md "wikilink")[特有種](../Page/特有種.md "wikilink")。生長在潮濕涼爽的環境。本種花梗會蔓生，容易產生不定芽。與[阿迪露毛氈苔和](../Page/阿迪露毛氈苔.md "wikilink")[叉蕊毛氈苔有近緣關係](../Page/叉蕊毛氈苔.md "wikilink")。
[Drosera_prolifera.jpg](https://zh.wikipedia.org/wiki/File:Drosera_prolifera.jpg "fig:Drosera_prolifera.jpg")
[DroseraProlifera.jpg](https://zh.wikipedia.org/wiki/File:DroseraProlifera.jpg "fig:DroseraProlifera.jpg")

## 注釋

## 外部連結

  - [D.
    prolifera炎熱考驗的開始](http://www.tbg.org.tw/~tbgweb/cgi-bin/topic.cgi?forum=19&topic=8556&show=200)

  - [Drosera
    prolifera](http://www.exoticaplants.com/database/cps/drosera/page/prolifera.htm)
    負子毛氈苔的植株與介紹

  - [D.
    prolifera-昆士蘭閃亮三姊妹](http://forums.plant-seeds.idv.tw/showthread.php?t=20013)

  - [D.
    prolifera](http://web.guestbook.com.tw/b7/viewtopic.php?p=18&mforum=exoticaplants&sid=8210a5b25d2e56a05a3e53c401c6e8fa)
    負子毛氈苔的植株和花

  - [D.
    prolifera](http://flowers.hunternet.com.tw/showthread.php?t=15125)
    負子毛氈苔

  - [Drosera
    prolifera](http://www.humboldt.edu/~rrz7001/zphotos/D_prolifera.html)
    多張負子毛氈苔的圖片

[prolifera](../Category/茅膏菜屬.md "wikilink")
[Category:澳大利亞食蟲植物](../Category/澳大利亞食蟲植物.md "wikilink")

1.  第157到158頁；第275頁