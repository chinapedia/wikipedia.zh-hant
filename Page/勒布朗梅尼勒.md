[Le_Blanc-Mesnil_map.svg](https://zh.wikipedia.org/wiki/File:Le_Blanc-Mesnil_map.svg "fig:Le_Blanc-Mesnil_map.svg")
**勒布朗梅尼勒**（[法语](../Page/法语.md "wikilink")：****）是[法国](../Page/法国.md "wikilink")[法兰西岛大区](../Page/法兰西岛大区.md "wikilink")[塞纳-圣但尼省的一个镇](../Page/塞纳-圣但尼省.md "wikilink")，位于[巴黎郊区东北部](../Page/巴黎.md "wikilink")，面积8.05平方公里，该市镇年时的人口为人。

[勒布朗梅尼勒站是](../Page/勒布朗梅尼勒站.md "wikilink")[法兰西岛大区快铁B线上的一个车站](../Page/法兰西岛大区快铁B线.md "wikilink")。

## 参考文献

## 外部链接

  - [Official website](http://www.blancmesnil.fr/)
  - [Forum
    culturel](https://web.archive.org/web/20071008215318/http://www.forumculturel.asso.fr/)
  - [Blog du Forum
    culturel](https://web.archive.org/web/20071012002141/http://blog.forumculturel.asso.fr/)

[B](../Category/塞纳-圣但尼省市镇.md "wikilink")