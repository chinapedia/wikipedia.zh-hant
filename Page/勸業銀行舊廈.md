[Historical_Museum_of_the_Land_Bank_of_Taiwan_Interior_exhibit_2016.JPG](https://zh.wikipedia.org/wiki/File:Historical_Museum_of_the_Land_Bank_of_Taiwan_Interior_exhibit_2016.JPG "fig:Historical_Museum_of_the_Land_Bank_of_Taiwan_Interior_exhibit_2016.JPG")
[Historical_Museum_of_the_Land_Bank_of_Taiwan_Building_Gallery_2016.jpg](https://zh.wikipedia.org/wiki/File:Historical_Museum_of_the_Land_Bank_of_Taiwan_Building_Gallery_2016.jpg "fig:Historical_Museum_of_the_Land_Bank_of_Taiwan_Building_Gallery_2016.jpg")
[Story_of_the_Land_Bank_of_Taiwan_2016.JPG](https://zh.wikipedia.org/wiki/File:Story_of_the_Land_Bank_of_Taiwan_2016.JPG "fig:Story_of_the_Land_Bank_of_Taiwan_2016.JPG")

**原日本勸業銀行台北支店**位於[台北市](../Page/台北市.md "wikilink")[中正區襄陽路](../Page/中正區_\(臺北市\).md "wikilink")（舊址為台北市[表町](../Page/表町_\(臺北市\).md "wikilink")2丁目11番地），為建於1933年的直轄市定古蹟，列案名稱為**勸業銀行舊廈**。該類似[馬雅文明建築的建物為](../Page/馬雅文明.md "wikilink")[日治時代的](../Page/台灣日治時期.md "wikilink")[日本勸業銀行](../Page/日本勸業銀行.md "wikilink")（現[瑞穗銀行前身之一](../Page/瑞穗銀行.md "wikilink")）台北支店（分行），與同時期完成的[台南支店外形幾乎相同](../Page/原日本勸業銀行台南支店.md "wikilink")。

## 沿革

勸業銀行的「勸業」於[日本語為](../Page/日本語.md "wikilink")「提倡實業」意思，其業務也與生產事業息息相關。1920年代之前，勸業銀行在台業務均由[台灣銀行代辦](../Page/台灣銀行.md "wikilink")，以產業貸款為主，金額並不高。1922年，勸業銀行正式來台經營土地開發、造林及埤圳水利等事業，並於臺北、臺南兩市各設立支店（分行）。1930年代，為因應日漸龐大的業務來往，勸業銀行特別於台北與台南分別興蓋兩座大型銀行廳舍。

勸業銀行於臺南市[末廣町與臺北市](../Page/末廣町_\(台南市\).md "wikilink")[表町興建的這兩座營業廳舍](../Page/表町_\(臺北市\).md "wikilink")，與1930年代流行於台灣的[希臘](../Page/希臘.md "wikilink")、[羅馬風格的大型建物極不相同](../Page/羅馬.md "wikilink")。兩座勸業銀行舊廈都充滿古代[中美洲馬雅建築風味](../Page/中美洲.md "wikilink")，是典型的[折衷現代主義建築](../Page/折衷現代主義.md "wikilink")。其中，建於1933年的台北勸業銀行舊廈外觀約是正面50m、側面40m的凹字矩形建物，構造則為鐵骨及[鋼筋混凝土](../Page/鋼筋混凝土.md "wikilink")（RC）的三層樓建築。

戰後，則由[台灣土地銀行](../Page/台灣土地銀行.md "wikilink")（土銀）接收為總行使用，並另外新建大樓連結。在土銀停用該建築後，[台北市政府和](../Page/台北市政府.md "wikilink")[文建會計劃將其與附近的](../Page/文建會.md "wikilink")[三井物產株式會社舊廈](../Page/三井物產株式會社舊廈.md "wikilink")、[台灣博物館與](../Page/國立臺灣博物館.md "wikilink")[二二八和平紀念公園等地點以](../Page/二二八和平紀念公園.md "wikilink")[地下道連接](../Page/地下道.md "wikilink")，結合成古蹟專區（首都核心區博物館）。2007年，勸業銀行舊廈開始進行大規模整修，後由[國立台灣博物館使用](../Page/國立台灣博物館.md "wikilink")（但[產權仍歸土銀](../Page/產權.md "wikilink")），定名為「**國立臺灣博物館土銀展示館**」，於2010年2月21日開館。

## 簡介

該銀行位居轉角處，大廳設有[騎樓](../Page/騎樓.md "wikilink")，騎樓最外方立有兩至三人環抱的八大柱子及四端巨大石柱。四角落石柱除了貼有仿石材的橫帶裝飾，也密集貼有人造石片。

該建築物最特殊的是在八石柱與四端柱頭[山牆都有以假石構成獸](../Page/山牆.md "wikilink")（獅）面雕飾及捲曲植物紋樣作為裝飾。整體除了給人穩重與安定感之外，也帶有日治時期建築少見的[中南美洲神秘感覺](../Page/中南美洲.md "wikilink")。

<File:Historical> Museum of LBOT entry 20131016.JPG|臺博館土銀展示館大門
<File:Historical> Museum of the Land Bank of Taiwan Reception
2016.JPG|接待處 <File:Historical> Museum of the Land Bank of Taiwan
Bank Vault Entrance 2016.JPG|金庫入口 <File:LBOT> Bank Vault view
2016.JPG|金庫 <File:Historical> Museum of the Land Bank of Taiwan Bank
Outside corridor 2016.JPG|展示館外的[騎樓](../Page/騎樓.md "wikilink")

## 參考資料

  - [黃昭堂著](../Page/黃昭堂.md "wikilink")，《台灣總督府》，2003年。臺北市：鴻儒堂出版社。
  - [莊永明著](../Page/莊永明.md "wikilink")，《台北老街》，1991年。臺北市：[時報出版](../Page/時報出版.md "wikilink")。
  - 又吉盛清著，《台灣今昔之旅》，1997年，台北市：[前衛出版社](../Page/前衛出版社.md "wikilink")。
  - 武內貞義著，《台灣》，1929，台灣刊行會。

## 外部連結

  - [勸業銀行舊廈—文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/19910524000001)
  - [府城和風之台南土地銀行](http://naturallybread.yam.org.tw/2007-oldhouse-1/oldhouse-tnd/oldhouse-tnd-index.htm)

[Category:台北市古蹟](../Category/台北市古蹟.md "wikilink")
[Category:國立臺灣博物館](../Category/國立臺灣博物館.md "wikilink")
[Category:1933年建筑](../Category/1933年建筑.md "wikilink")
[Category:台灣日治時期產業建築](../Category/台灣日治時期產業建築.md "wikilink")