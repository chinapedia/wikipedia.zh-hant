[Aftnn_King_Stephen,_who_we_reckon_was_responsible_for_Christianity_in_eastern_Europe.jpg](https://zh.wikipedia.org/wiki/File:Aftnn_King_Stephen,_who_we_reckon_was_responsible_for_Christianity_in_eastern_Europe.jpg "fig:Aftnn_King_Stephen,_who_we_reckon_was_responsible_for_Christianity_in_eastern_Europe.jpg")
**圣伊什特万一世**（[匈牙利语](../Page/匈牙利语.md "wikilink")：**I. (Szent)
István**，约970年至975年之间—1038年8月15日）[匈牙利](../Page/匈牙利.md "wikilink")大公（997年—1001年）和第一位国王（1001年起）。[盖萨大公之唯一的兒子](../Page/盖萨大公.md "wikilink")。

伊什特万原名沃伊克（Vajk）\[1\]，其後在受洗後改名為伊什特萬，以紀念第一位殉道者－[聖斯德望](../Page/聖斯德望.md "wikilink")（伊什特萬是斯德望的匈牙利釋音）。997年，伊什特萬一世繼承其父蓋薩大公，盤據西北方的[潘諾尼亞平原](../Page/潘諾尼亞平原.md "wikilink")，匈牙利其他地方仍在其他酋長手中。\[2\]伊什特萬靠著外來的武士及一些地方貴族打敗了受異教戰士支持的叔父，成為擁有實權的匈牙利大公。1000年12月25日，在教皇[西尔维斯特二世主持下伊什特萬加冕称王](../Page/西尔维斯特二世.md "wikilink")，成為匈牙利第一任國王。

在伊什特万一世统治时期，[马扎尔人完成了从游牧部落向封建国家的转变](../Page/马扎尔人.md "wikilink")。伊什特万一世废除了旧有的按氏族划分人民的办法，实行行政区制度（设立国王省）。他在马扎尔民族中强制推行[天主教信仰](../Page/天主教.md "wikilink")。1001年，[教皇为伊什特万一世加冕](../Page/教皇.md "wikilink")，从此匈牙利升格为王国。

1027年，伊什特万一世从软弱无能的[波兰国王](../Page/波兰.md "wikilink")[梅什科二世手中夺取了隶属波兰的](../Page/梅什科二世.md "wikilink")[斯洛伐克](../Page/斯洛伐克.md "wikilink")。1030年，伊什特万一世击退了[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[康拉德二世的进攻](../Page/康拉德二世_\(神圣罗马帝国\).md "wikilink")。

1031年，伊什特萬一世唯一在世的兒子[伊姆雷在一次狩獵中受傷早逝](../Page/伊姆雷.md "wikilink")。伊什特萬一世的健康每況愈下。他的堂弟本為最有資格的繼承人，因為他涉嫌偏向異教，伊什特萬一世不容許他繼位，所以立了他的外甥[威尼斯人](../Page/威尼斯人.md "wikilink")[奧爾塞奧羅·彼得為繼承人](../Page/彼得一世_\(匈牙利\).md "wikilink")。瓦祖爾被擒下並弄盲，三名兒子[利雲特](../Page/利雲特.md "wikilink")、[安德烈](../Page/安德烈一世.md "wikilink")
及[貝拉被逐出匈牙利](../Page/貝拉一世.md "wikilink")。伊什特萬一世於1038年8月15日逝世，葬於[塞克什白堡](../Page/塞克什白堡.md "wikilink")。伊什特萬一世死後，因為內戰、異教徒暴亂及外族入侵，匈牙利長時間不穩定，直至瓦祖爾的孫兒[拉斯洛一世於](../Page/拉斯洛一世.md "wikilink")1077年登上王位才終結。

伊什特万一世在1083年即已被宣布为圣人。他的大氅和剑，以及加冕时的誓词，直到[哈布斯堡王朝的最后一位君主都一直是匈牙利国王加冕式上必不可少的内容](../Page/哈布斯堡王朝.md "wikilink")。

[Szent-Jobb-.jpg](https://zh.wikipedia.org/wiki/File:Szent-Jobb-.jpg "fig:Szent-Jobb-.jpg")：[王之聖右手](../Page/王之聖右手.md "wikilink")\]\]

伊什特万死后，人们将他的手保存在大教堂中，人称圣右手。

伊什特万一世雄才大略，开创了一个时代，成为了匈牙利的民族英雄，他的名字也成为人们起名的首选。

## 家庭

伊什特萬一世娶了[巴伐利亞公爵亨利二世的女兒季賽拉](../Page/巴伐利亞統治者列表.md "wikilink")。他們有兩名兒子，長子奧托死於幼時，次子伊姆雷於1031年逝世，比伊什特萬一世早逝，與伊什特萬一世同於1083年被宣佈成為聖人。

## 参考来源

|-

[Category:匈牙利国王](../Category/匈牙利国王.md "wikilink")
[Category:歐洲紙幣上的人物](../Category/歐洲紙幣上的人物.md "wikilink")
[I](../Category/1038年逝世.md "wikilink")

1.
2.