**巧克力**（，\[1\]）原产自中美洲，巧克力来自“xocolatl”，是纳瓦特尔语单词意為“苦水”，是以[可可做为主料的混合型](../Page/可可.md "wikilink")[食品](../Page/食品.md "wikilink")。主要原料[可可豆](../Page/可可豆.md "wikilink")（Cacao），产于[赤道南北](../Page/赤道.md "wikilink")[緯線](../Page/緯線.md "wikilink")18度以内狭长地带。

巧克力含有豐富的[鐵](../Page/鐵.md "wikilink")、[鈣](../Page/鈣.md "wikilink")、[鎂](../Page/鎂.md "wikilink")、[鉀](../Page/鉀.md "wikilink")、[維生素A](../Page/維他命A.md "wikilink")、[維生素C](../Page/維他命C.md "wikilink")
和[可可鹼](../Page/可可鹼.md "wikilink")，由于多添加糖分，因此具有高能量值。由天然成分制作的巧克力對人類之外的許多動物有毒（例如:
狗），但對[人類無毒](../Page/人類.md "wikilink")、且其中微量的[可可鹼是健康的反鎮靜成分](../Page/可可鹼.md "wikilink")。故食用有助提升精神，增強興奮等功效。可可含有[苯乙胺](../Page/苯乙胺.md "wikilink")，坊间流传能使人有[戀愛感覺的流言](../Page/戀愛.md "wikilink")\[2\]。

巧克力由可可豆加工而成，主要有效成分是高[脂肪的](../Page/脂肪.md "wikilink")[可可脂与低脂肪的](../Page/可可脂.md "wikilink")[可可块](../Page/可可粉.md "wikilink")。可可碱主要存在于可可块中。可可脂有六种结晶形式，依各种结晶形式比例不同，熔點在37攝氏度左右至18摄氏度左右不等。

巧克力按组成的不同而被分为不同的产品，如[黑巧克力](../Page/黑巧克力.md "wikilink")、[牛奶巧克力](../Page/牛奶巧克力.md "wikilink")、果料巧克力、坚果巧克力、酒芯巧克力、香料巧克力、[松露巧克力等等](../Page/松露巧克力.md "wikilink")。

在欧美的眾多國家中，有许多巧克力的百年老店、博物馆和巧克力公园，李俊涛的中文书籍《我的巧克力地盘》中提到，世界上有20家巧克力博物馆，介绍巧克力的制作、技术、人物和知名的巧克力品牌等。与巧克力有关的节日有许多，如[情人节](../Page/情人节.md "wikilink")、[圣诞节](../Page/圣诞节.md "wikilink")、[感恩节](../Page/感恩节.md "wikilink")、[复活节等](../Page/复活节.md "wikilink")。

## 词源

[Kakaw_(Mayan_word).png](https://zh.wikipedia.org/wiki/File:Kakaw_\(Mayan_word\).png "fig:Kakaw_(Mayan_word).png")。\]\]
“巧克力”音译自英文单词“chocolate”\[3\]，该单词于大约1600年从西班牙语中引入到英语\[4\]。此单词如何来到西班牙语尚不确定，
有几种相互竞争的解释。或许被引用最多的解释是“chocolate”来自[纳瓦特尔语](../Page/纳瓦特尔语.md "wikilink")，阿兹台克人的语言，中的
“chocolātl”，很多来源说此单词衍生自“xocolātl”，结合
“xococ”，酸或苦，与“ātl”水或饮\[5\]。单词“chocolatl”在墨西哥中部殖民地来源中不存在，因此这个由来不太可能\[6\]。另一起源来自[尤卡坦玛雅语单词](../Page/尤卡坦玛雅语.md "wikilink")
“chokol”意为热，与纳瓦特尔语“atl”意为水\[7\]。纳瓦特尔词语“chicolatl”意为“被打的水”，可能衍生自起泡的棍的单词“chicoli”\[8\]。词语“chocolate
chip”意为巧克力碎片，首次用于1940年\[9\]。词语“chocolatier”意为做巧克力甜点的人，已证明1888年起出现\[10\]。

## 历史

最早拿來飲用的是馬雅人，而最初是由當地的[墨西哥人所製作的](../Page/墨西哥人.md "wikilink")，16世纪初期的[西班牙](../Page/西班牙.md "wikilink")[探险家](../Page/探险家.md "wikilink")[哥倫布在](../Page/哥倫布.md "wikilink")[墨西哥发现当地的](../Page/墨西哥.md "wikilink")[阿兹特克国王饮用一种可可豆加](../Page/阿兹特克.md "wikilink")[水和](../Page/水.md "wikilink")[香料制成的](../Page/香料.md "wikilink")[饮料](../Page/饮料.md "wikilink")，科尔特斯品尝后在1528年带回西班牙，并在[西非一个小岛上种植了](../Page/西非.md "wikilink")[可可树](../Page/可可树.md "wikilink")。西班牙人将可可豆磨成了粉，从中加入了水和[糖](../Page/糖.md "wikilink")，在加热后被制成的饮料称为“巧克力”，深受大众的欢迎。不久其制作方法被[意大利人学会](../Page/意大利人.md "wikilink")，并且很快传遍整個[欧洲](../Page/欧洲.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Pietro_Longhi_La_cioccolata_del_mattino_The_Morning_Chocolate_1775-1780.jpg "fig:缩略图")

  - 1706年，康熙皇帝成為中國記載第一位接触巧克力的人。\[11\]
  - 1765年，巧克力进入[美国](../Page/美国.md "wikilink")，被第三任總統[托马斯·杰斐逊赞为](../Page/托马斯·杰斐逊.md "wikilink")“具有健康和营养的优点”。
  - 1847年，巧克力饮料中被加入[可可脂](../Page/可可脂.md "wikilink")，制成如今人们熟知的可咀嚼巧克力块。
  - 1875年，[瑞士发明了制造](../Page/瑞士.md "wikilink")[牛奶巧克力的方法](../Page/牛奶.md "wikilink")，从而有了现在所看到的巧克力。
  - 1914年，[第一次世界大战刺激了巧克力的生产](../Page/第一次世界大战.md "wikilink")，巧克力被运到战场分发给[士兵](../Page/士兵.md "wikilink")。

## 制作

[Cocoa_Pods.JPG](https://zh.wikipedia.org/wiki/File:Cocoa_Pods.JPG "fig:Cocoa_Pods.JPG")
全世界大约三分之二的可可是由[西非生产的](../Page/西非.md "wikilink")，其中有43%是来自[-{A](../Page/科特迪瓦.md "wikilink")。\[12\]在这里童工的使用在可可生产中非常普遍。\[13\]\[14\]\[15\]根据世界可可基金会统计，全世界大约有五千万人依靠可可为生。\[16\]在英国，大多数巧克力生产者都是购买可可，然后自己融化，注模并用自己的设计包装。\[17\]或利用可可為主材料，搭配打發雞蛋、糖等，製作各式各樣的巧克力經典甜點。

[可可豆经過](../Page/可可豆.md "wikilink")[发酵](../Page/发酵.md "wikilink")、[干燥](../Page/干燥.md "wikilink")、[烘焙](../Page/烘焙.md "wikilink")、[研磨](../Page/研磨.md "wikilink")，制成可可汁。可可汁可制成[可可脂与](../Page/可可脂.md "wikilink")[可可粉](../Page/可可粉.md "wikilink")，或选高品质的粗制可可汁经进一步加工制成巧克力。**可可液块**（Cocoa
liquor）是可可块与可可脂液体混合物凝固后的物质，两种成分的比例大致相等。

## 种类

### 依成份分类

[Chocolate.jpg](https://zh.wikipedia.org/wiki/File:Chocolate.jpg "fig:Chocolate.jpg")

  - **无味巧克力**：质地很硬，作为半成品，制作[巧克力馅等](../Page/甘納許.md "wikilink")。

<!-- end list -->

  -
    含量：可可脂含量高于50%。
    组成：可可脂。

<!-- end list -->

  - **黑巧克力**（Dark chocolate）：硬度较大，微苦。

<!-- end list -->

  -
    含量：一般指可可固形物含量介于70%到99%之间，或乳质含量少于12％的巧克力。價格最高的巧克力。
    组成：可可粉、可可脂、少量糖。含豐富鐵質, 是缺鐵性貧血患者、素食者、補充鐵質的良好食物來源。

<!-- end list -->

  - **紅寶石巧克力**（Ruby chocolate）：淡淡的莓果香氣，微酸甜感，自然的淡紅色

<!-- end list -->

  -
    含量：紅寶石可可豆（產自巴西、厄瓜多爾與象牙海岸的可可豆提煉而成），紅寶石可可豆磨成的紅色可可粉是這種巧克力的粉紅色澤來源
    组成：

<!-- end list -->

  - **牛奶巧克力**（Milk chocolate）：

<!-- end list -->

  -
    含量：至少含10％的可可浆，以及12％的乳质。是市面上數量最多最常見的巧克力。
    组成：由可可制品（可可液塊、可可粉、可可脂）、乳制品、糖粉、[香料和表面活性剂等材料组成](../Page/香料.md "wikilink")。

<!-- end list -->

  - **白巧克力**（White chocolate）：

<!-- end list -->

  -
    含量：含可可脂，不含可可粉。
    组成：与牛奶巧克力大致相同，乳制品和糖粉的含量相对较大，甜度高。

<!-- end list -->

  - **非調溫巧克力**（Compound chocolate）：

<!-- end list -->

  -
    含量：含可可粉，不含可可脂。
    组成：使用較便宜的植物油、椰油和棕櫚油代替昂貴的可可脂作為其脂肪來源。

### 依添加物分类

  - **实心巧克力**（Solid chocolate）：

<!-- end list -->

  -
    是指没有混合其它果仁、饼干等成分的巧克力，以片状及块状居多。\[18\]

<!-- end list -->

  - **混有其他成分的实心巧克力**（Solid chocolate with inclusions）：

<!-- end list -->

  -
    是指巧克力中混有细碎的果仁、葡萄乾、软胶糖、饼干等成分的巧克力。

<!-- end list -->

  - **果仁巧克力**（Enrobed or moulded product with candy, fruit, or nut
    center）：

<!-- end list -->

  -
    一种包有果仁类内容的巧克力。

<!-- end list -->

  - **酒心巧克力**（Beer chocolate）：

<!-- end list -->

  -
    是指在巧克力的中间加入了烈酒酒漿。

<!-- end list -->

  - **饼干巧克力**（Enrobed or moulded products with bakery centers）：

<!-- end list -->

  -
    饼干的酥脆和巧克力的细滑形成对比。

<!-- end list -->

  - **生巧克力**（Nama chocolate）：

<!-- end list -->

  -
    來自日文漢字「生」直接使用，意指新鮮的意思；使用牛奶、鮮奶油、奶油與酒類等製成，質地偏軟，口感滑順，保存期限較短。

<!-- end list -->

  - **松露巧克力**（Truffle chocolate ）：

<!-- end list -->

  -
    形狀與價格昂貴的蕈類食材[松露相近](../Page/松露.md "wikilink")，但並不含有任何松露成分。外層為巧克力或包裹[可可粉](../Page/可可粉.md "wikilink")，內層為巧克力溶於新鮮奶油里並搓成球裹巧克力粉、[水果或](../Page/水果.md "wikilink")[堅果等](../Page/堅果.md "wikilink")。

## 成分

巧克力含有多種物質，其中一些具有影響身體化學的功效。這些物質包括：

  - [大麻素](../Page/大麻素.md "wikilink")
  - [精氨酸](../Page/精氨酸.md "wikilink")
  - [咖啡因](../Page/咖啡因.md "wikilink")，目前在較小數額
  - [多巴胺](../Page/多巴胺.md "wikilink")
  - [黃嘌呤](../Page/黃嘌呤.md "wikilink")(能讓人興奮)等（[可可鹼](../Page/可可鹼.md "wikilink")，[咖啡因和](../Page/咖啡因.md "wikilink")[茶鹼](../Page/茶鹼.md "wikilink")）
  - [單胺氧化酶](../Page/單胺氧化酶.md "wikilink")
  - [草酸](../Page/草酸.md "wikilink")
  - [苯乙胺](../Page/苯乙胺.md "wikilink")，內源性生物鹼有時稱為'愛化學品，它是通過快速代謝單胺氧化酶-
    B和未達到大量腦
  - [苯丙氨酸](../Page/苯丙氨酸.md "wikilink")
  - [羥色胺](../Page/血清素.md "wikilink")
  - [糖](../Page/糖.md "wikilink")
  - [可可鹼](../Page/可可鹼.md "wikilink")，主要生物鹼固體可可和巧克力和部分負責巧克力的騰雲駕霧效果
  - [茶鹼](../Page/茶鹼.md "wikilink")
  - [色氨酸](../Page/色氨酸.md "wikilink")，一種必需氨基酸和前體的複合胺

營養成份每100[克中含有](../Page/克.md "wikilink")：

  - [碳水化合物約](../Page/碳水化合物.md "wikilink")50克
  - [脂肪約](../Page/脂肪.md "wikilink")30克
  - [蛋白質約](../Page/蛋白質.md "wikilink")15克
  - 含有較多的[鋅](../Page/鋅.md "wikilink")
  - [維生素B2](../Page/維生素B2.md "wikilink")
  - [鐵和](../Page/鐵.md "wikilink")[鈣等](../Page/鈣.md "wikilink")
  - [熱量約](../Page/熱量.md "wikilink")600[千卡](../Page/千卡.md "wikilink")

## 健康功效

大多數人吃巧克力是為了消遣，但巧克力本身也存在一些保健功效；舉例說可可粉或者黑巧克力對循環系统有益處，同時也具有抗癌、刺激大腦活動、预防咳嗽和止瀉等功能。\[19\]只是市售的巧克力常添加過高的精制糖、飽和脂肪及加工過程會抵銷其保健功效。

### 循環系統

經證實後，未加工[可可粉與](../Page/可可粉.md "wikilink")[黑巧克力對人體有益](../Page/黑巧克力.md "wikilink")，能保護『低密度脂蛋白』不被氧化。

《美國臨床營養學雜誌》報導，黑巧克力可降低[血壓](../Page/血壓.md "wikilink")，因為黑巧克力中的豐富[黃酮](../Page/黃酮.md "wikilink")（或可稱[兒茶素](../Page/兒茶素.md "wikilink")）具血管張力作用，可有效改善血壓。據統計，黑巧克力可平均降低10%血壓。一些研究還發現，每天適度食用黑巧克力可以降低血壓並有利血管擴張，但是食用牛奶巧克力、[白巧克力](../Page/白巧克力.md "wikilink")、或喝含巧克力的[牛奶](../Page/牛奶.md "wikilink")，則似乎沒有健康效益。加工可可粉（所謂的荷蘭巧克力），加工可可粉與“原始”可可粉相比，它的抗氧化能力則是大大减弱了。因為可可粉加工後會破壞大多數黃酮類化合物。

巧克力中[脂肪](../Page/脂肪.md "wikilink")1/3形態為硬脂酸甘油脂（[飽和脂肪酸](../Page/飽和脂肪酸.md "wikilink")）和油酸甘油脂（[不飽和脂肪酸](../Page/不飽和脂肪酸.md "wikilink")）。而且，與其他飽和脂肪酸相比，硬脂酸並不會造成血液中低密度脂蛋白-膽固醇（LDL-膽固醇）濃度上升。攝取相對大量的黑巧克力和可可似乎不會提高血清LDL-膽固醇濃度；有些研究甚至發現，它可以降低其濃度。事實上，定期食用少量黑巧克力可以降低[心臟病發作機率](../Page/心臟病.md "wikilink")。
一項由[斯德哥爾摩Karolinska研究所進行的研究發現](../Page/斯德哥爾摩.md "wikilink")（刊載於2009年9月的內科[雜誌](../Page/雜誌.md "wikilink")），每個星期至少吃兩三次巧克力的心臟病患者死亡機率與沒有吃巧克力的心臟病患者相比减小了2/3。

### 抗氧化

可可比起其他含有多酚抗氧化劑的食物和飲料還擁有更強的抗氧化作用，瑞士蘇黎世大學醫院最新報告指出，每日少量黑巧克力可顯著增加吸取抗氧化劑，研究並指出，黑巧克力含有的抗氧化劑高於紅酒、茶及莓類水果。可可多酚的高抗氧化作用能夠抑制成為老化原因的活性氧的活動。對於修復受到紫外線損傷的肌膚，促進肌膚的代謝是很有效果的，還可預防痘痘、痘疤、黑斑及皺紋等。

### 抗憂鬱

巧克力所含的可可碱，的確能使人情緒興奮，有抗抑鬱效果。

### 降低血壓

保護心血管　美國心臟學會最新研究，黑巧克力能降低心臟病的危險，因為巧克力多酚可改善血液循環，防止心血管疾病；並且多酚能減少壞膽固醇量，有效避免氧化及動脈硬化，降低心血管疾病及中風機率。
降低膽固醇　可可豆中的可可脂是天然脂肪，雖然含很高的飽和脂肪酸，但對血液中膽固醇水準沒有影響，而單不飽和脂肪酸中的油酸可以降低體內膽固醇濃度。

### 肌肉恢復

一項研究由詹姆斯麥迪遜大學提交給美國運動醫學學院年會上顯示出，運動後飲用低脂巧克力牛奶提供平等的或可能優於肌肉恢復藥物，高碳水化合物飲料亦恢復同樣數量的熱量。
運動員飲用巧克力牛奶明顯較低濃度的肌酸激酶指標的肌肉損傷，而飲酒者的碳水化合物飲料。出汗造成的損失也很重要液體和礦物質，包括鈣，鉀和鎂。

### 預防與緩和腹瀉

黑巧克力成分中的化學物質，是最自然的止瀉劑。美國奧克蘭大學的一項新研究顯示，黑巧克力中的可可粉含黃酮，可用於營養補充品，並能幫助預防與緩和腹瀉。

### 延遲大腦功能下降

研究表明了一種特殊類型的可可能夠延遲大腦功能下降。

### 提升好心情

巧克力成分中的色胺酸，可幫助人體合成血清素，當人體內的血清素充足，便能穩定情緒於和諧中，通常女性的血清素比男性少，因此女性吃巧克力後，更容易讓心情轉好。

### 粉刺

各種研究指出此問題不在巧克力，而是其他的高血糖食物。

### 鉛

最近的研究表明，可可豆本身雖然很少吸收[鉛](../Page/鉛.md "wikilink")，它往往綁定到可可殼和污染可能會出現在生產過程中。最近同行評審的出版物說在巧克力中發現大量的鉛。美國農業部2004年的研究指出，每克的巧克力平均含鉛量的介於0.0010至0.0965微克之間，但一个瑞士研究小組在2002年的另一項研究中發現，一些巧克力含鉛量達到0.769微克/每克，接近國際標準限制1微克每克。
2006年，美國FDA将在糖果的允许量降低至国际标准的五分之一。

### 動物毒性

對於如[狗](../Page/狗.md "wikilink")、[馬](../Page/馬.md "wikilink")、[鸚鵡](../Page/鸚鵡.md "wikilink")、小老鼠和[貓等動物](../Page/貓.md "wikilink")，因為牠們體內無法進行有效的化學代謝來代謝可可鹼。如果牠們誤食巧克力，可可鹼將留在血液中長達20個小時，這些動物可能會出現[眩晕](../Page/眩暈_\(醫學\).md "wikilink")、[呕吐](../Page/呕吐.md "wikilink")、[腹泻](../Page/腹泻.md "wikilink")、多尿等中毒症狀，导致[癲癇](../Page/癲癇.md "wikilink")、[心臟病](../Page/心臟病.md "wikilink")、[內出血](../Page/內出血.md "wikilink")，最終死亡。獸醫會進行催吐，並在兩小時以苯二氮或[巴比妥對癲癇解毒](../Page/巴比妥.md "wikilink")，並投與抗心律失常藥治療[心律不整](../Page/心律不整.md "wikilink")。

巧克力中的[可可鹼对动物的神经系统有影响](../Page/可可鹼.md "wikilink")。人類对可可鹼的[新陈代谢能力比较强](../Page/新陈代谢.md "wikilink")，對於人類，巧克力是一種溫和的興奮劑，主要是由於可可鹼的存在。白巧克力只含有微量的咖啡因和可可鹼，因為這些化學物質包含在可可，而不是可可脂。但有些动物，比如[犬](../Page/犬.md "wikilink")、[猫等](../Page/猫.md "wikilink")，对其新陈代谢速度要慢得多，可以說是有毒，一隻一般大的狗摄入400克（将近半公斤）巧克力后可能会出现包括从食欲不振到死亡等各种反应，小[狗相应的导致](../Page/狗.md "wikilink")[中毒巧克力劑量也要小](../Page/中毒.md "wikilink")。\[20\]

一条標準的20公斤（44磅）的狗食用不到240克（8.5盎司）的黑巧克力以后，通常會发生肠道疾病。但也未必出现心跳過緩或心跳過速，除非它吃至少0.5公斤（1.1英鎊）的牛奶巧克力。黑巧克力有2至5倍的可可鹼，因此對於犬類更加危險。據所说，大約每千克体重的狗食用1.3克Baker's巧克力（0.02盎司/磅），是足以造成中毒症狀。例如，一個標準的25克（0.88盎司）Baker's巧克力就足以使20公斤（44磅）的狗產生症狀。虽然大型狗的安全食用量更高，但是仍然远小于人类的食用量（1/4），所以要避免狗誤食巧克力。

## 業界标准

### 国际

[国际食品法典委员会](../Page/国际食品法典委员会.md "wikilink")（CAC）标准规定[可可脂占的比例不低于](../Page/可可脂.md "wikilink")18％，[可可粉不低于](../Page/可可粉.md "wikilink")14％。\[21\]

### 中國大陸

2003年，[中华人民共和国国家质量监督检验检疫总局下轄](../Page/中华人民共和国国家质量监督检验检疫总局.md "wikilink")[中国国家标准化管理委员会發布的GB](../Page/中国国家标准化管理委员会.md "wikilink")
9678.2-2003《巧克力卫生标准》；GB/T 19343-2003《巧克力及巧克力制品》；SB/T
10024-1992《巧克力及巧克力制品》，當中規定黑巧克力的可可脂要達到18％以上、白巧克力達到20％以上。

### 日本

1971年3月《》第10條第1項，經由相關產業統一規範。

### 臺灣

2016年1月，[衛生福利部食品藥物管理署制定](../Page/衛生福利部食品藥物管理署.md "wikilink")《巧克力標示規定》草案，總可可固形物含量高於35%、可可脂高於18%才能標黑巧克力，以及若使用植物油替代可可脂則須標示，2017年7月正式施行。\[22\]\[23\]\[24\]

## 知名巧克力品牌

  - [Amedei](../Page/Amedei.md "wikilink")
  - [Dove](../Page/德芙.md "wikilink")
  - [Ferrero Rocher](../Page/费列罗.md "wikilink")
  - [Godiva](../Page/歌帝梵巧克力.md "wikilink")
  - [Guylian](../Page/吉力蓮.md "wikilink")
  - [Hershey's](../Page/Hershey's.md "wikilink")（[好時](../Page/好時.md "wikilink")）
  - [Michel Cluizel](../Page/Michel_Cluizel.md "wikilink")
  - [Milka](../Page/妙卡.md "wikilink")
  - [M\&M's](../Page/M&M's.md "wikilink")（[瑪氏食品](../Page/瑪氏食品.md "wikilink")）
  - [Neuhaus](../Page/Neuhaus.md "wikilink")
  - [La Maison du Chocolat](../Page/La_Maison_du_Chocolat.md "wikilink")
  - [Leonidas](../Page/Leonidas.md "wikilink")
  - [Lindt](../Page/Lindt.md "wikilink")
  - [Roshen](../Page/Roshen.md "wikilink")（[如勝](../Page/如勝.md "wikilink")）
  - [Snickers](../Page/士力架.md "wikilink")
  - [Toblerone](../Page/瑞士三角巧克力.md "wikilink")
  - [Valrhona](../Page/法芙娜.md "wikilink")
  - [Zotter](../Page/Zotter.md "wikilink")

## 相關景點

  - [台灣苗栗巧克力雲莊](../Page/台灣.md "wikilink")、桃園巧克力共和國、淡水世界巧克力夢公園、埔里妮娜夢想城堡
  - [香港巧克力博物館](../Page/香港.md "wikilink")
  - [德國科隆巧克力博物館](../Page/德國.md "wikilink")

## 參見

  - [可可](../Page/可可.md "wikilink")
  - [热巧克力](../Page/热巧克力.md "wikilink")
  - [金币巧克力](../Page/金币巧克力.md "wikilink")
  - [巧克力癮](../Page/巧克力癮.md "wikilink")

## 參考來源

  - 文獻

<!-- end list -->

  -
  - 《我的巧克力地盘》，中国测绘出版社出版，作者李俊涛，ISBN 978-7-5030-2769-7

<!-- end list -->

  - 引用

## 延伸閱讀

  - *The True History of Chocolate*, by Sophie D. Coe & Michael D. Coe,
    Thames & Hudson, 1996.
  - *Naked Chocolate*, by David Wolfe and Shazzie, Rawcreation, 2005.
  - *The Great Book of Chocolate*, by David Lebovitz, Ten Speed Press,
    2004.

## 外部链接

  - [Detailed drug information](http://www.chocolate.org)

  - [Chocolate Culture](http://www.chokladkultur.se/english.htm)

  - [Exploring
    Chocolate](http://www.exploratorium.edu/exploring/exploring_chocolate/)

  - [Cornell News on
    Cocoa](http://www.news.cornell.edu/releases/Nov03/HotCocoa-Lee.bpf.html)

  - [A Pet Owner's Guide to Poisons:
    Chocolate](https://web.archive.org/web/20090705142823/http://www.avma.org/careforanimals/animatedjourneys/livingwithpets/poisoninfo.asp#Misc3)

  - [巧克力的科學](http://www.ntsec.gov.tw/user/Article.aspx?a=409&theme=1)-演講摘要
    - 國立臺灣科學教育館

[巧克力](../Category/巧克力.md "wikilink")
[Category:加工食品](../Category/加工食品.md "wikilink")
[Category:飲料](../Category/飲料.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.

9.

10.
11.

12.

13.

14.

15. McKenzie, David and Swails, Brent (19 January 2012) [Slavery in
    Cocoa fields: a horrible
    "normal"](http://thecnnfreedomproject.blogs.cnn.com/2012/01/19/slavery-in-cocoa-fields-a-horrible-normal/).
    CNN.

16.

17.

18. 魔法朱古力 ISBN 978-962-17-9153-5

19. [〈哪種巧克力才健康？〉](http://cht.naturalnews.com/?p=2609)，《Natural News》

20. [巧克力——人类的美食，狗狗的毒药](http://songshuhui.net/archives/19825.html)

21.

22.

23. （[video](http://video.udn.com/news/434536)）

24.