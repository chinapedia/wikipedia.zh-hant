《**阿拉伯的劳伦斯**》（），[1962年電影](../Page/1962年電影.md "wikilink")，[英國](../Page/英國.md "wikilink")[導演](../Page/導演.md "wikilink")[大卫·利恩執导](../Page/大卫·利恩.md "wikilink")，並获得多項[奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")。影片松散地依据[托马斯·爱德华·劳伦斯的自傳](../Page/托马斯·爱德华·劳伦斯.md "wikilink")《[智慧的七柱](../Page/智慧的七柱.md "wikilink")》改编，[彼得·奥图饰演主角](../Page/彼得·奥图.md "wikilink")，制片人为[山姆·史匹格](../Page/山姆·史匹格.md "wikilink")（1957年[大卫·利恩和史匹格刚完成受称赞的影片](../Page/大卫·利恩.md "wikilink")《[桂河大桥](../Page/桂河大桥.md "wikilink")》）。[莫里斯·賈爾的配樂和Freddie](../Page/莫里斯·賈爾.md "wikilink")
Young的[电影摄影特别受称赞](../Page/电影摄影.md "wikilink")。

## 概述

電影描述[一战时](../Page/一战.md "wikilink")，英國[軍官](../Page/軍官.md "wikilink")[T·E·勞倫斯在](../Page/托馬斯·愛德華·勞倫斯.md "wikilink")[開羅情報部及](../Page/開羅.md "wikilink")[英國外交部](../Page/英國外交部.md "wikilink")[阿拉伯局任職](../Page/阿拉伯局.md "wikilink")，期間拉攏阿拉伯部族，協助[阿拉伯起義對抗](../Page/阿拉伯起義.md "wikilink")[鄂圖曼帝國的事跡](../Page/鄂圖曼帝國.md "wikilink")。电影的主题包括劳伦斯与战争中暴力（特别是阿拉伯部落之间和与土耳其军队作战时）的情感斗争，对个人身份的认同（“你是谁？”是影片中反复出现的台词），以及在他祖国的英军与他新结交的阿拉伯游牧部落之间如何作出选择。

## 主要人物／演員

  - [T·E·勞倫斯](../Page/托马斯·爱德华·劳伦斯.md "wikilink")：[彼得·奧圖](../Page/彼得·奧圖.md "wikilink")
  - [謝里夫·阿里](../Page/謝里夫·阿里.md "wikilink")：[奧瑪·雪瑞夫](../Page/奧瑪·雪瑞夫.md "wikilink")
  - [費薩爾王子](../Page/费萨尔一世_\(伊拉克\).md "wikilink")：[亞歷·堅尼斯](../Page/亞歷·堅尼斯.md "wikilink")
  - [Auda ibu
    Tayi](../Page/Auda_ibu_Tayi.md "wikilink")：[安東尼·奎恩](../Page/安東尼·奎恩.md "wikilink")

## 製作

有別於大部份電影所用的35毫米胶片，本片採用了70毫米胶片及相關技術（）攝製，提供更高的[解像度](../Page/解像度.md "wikilink")、更細緻的畫面，但需要特定器材放映。

影片於1961年5月15日至1962年10月20日期間拍攝，沙漠部分[鏡頭取自](../Page/鏡頭.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")、[約旦](../Page/約旦.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")。

## 評價

  - 它在[美国电影学院评出的有史以来的最佳影片中位列第五位](../Page/美国电影学院.md "wikilink")。这部电影被[美国国会图书馆认为具有](../Page/美国国会图书馆.md "wikilink")“文化上的重要性”，被[美國國家電影保護局保存](../Page/美國國家電影保護局.md "wikilink")。

<!-- end list -->

  - 該片在1999年的[英國電影學會](../Page/英國電影學會.md "wikilink")100部最佳英國電影投票中位列第三，雜誌《Total
    Film》於2004年稱該片為有史以来最佳英國影片的第八位。

<!-- end list -->

  - 導演[史蒂芬·史匹柏曾表示他在拍片遇到瓶頸時](../Page/史蒂芬·史匹柏.md "wikilink")，本片是為了能讓自己回歸初衷必看的四部電影之一；另外三部為[法蘭克·卡普拉的](../Page/法蘭克·卡普拉.md "wikilink")《[風雲人物](../Page/風雲人物.md "wikilink")》、[約翰·福特的](../Page/約翰·福特.md "wikilink")《[搜索者](../Page/搜索者.md "wikilink")》、[黑澤明的](../Page/黑澤明.md "wikilink")《[七武士](../Page/七武士.md "wikilink")》。

## 獎項

  - **1963年[奥斯卡奖](../Page/奥斯卡奖.md "wikilink")**
      - [最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")
      - [最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink") －
        [大卫·利恩](../Page/大卫·利恩.md "wikilink")
      - [最佳艺术指导/美工/布景装置奖](../Page/奥斯卡最佳艺术指导奖.md "wikilink")
      - [最佳摄影奖](../Page/奥斯卡最佳摄影奖.md "wikilink")
      - [最佳剪辑奖](../Page/奥斯卡最佳剪辑奖.md "wikilink")
      - [最佳原创音乐奖](../Page/奥斯卡最佳原创音乐奖.md "wikilink") －
        [莫里斯·賈爾](../Page/莫里斯·賈爾.md "wikilink")（Maurice
        Jarre）
      - [最佳音响/混音奖](../Page/奥斯卡最佳音响奖.md "wikilink")
  - 奥斯卡奖提名
      - [最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink") －
        [彼得·奧圖](../Page/彼得·奧圖.md "wikilink")
      - [最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink") －
        [奧瑪·雪瑞夫](../Page/奧瑪·雪瑞夫.md "wikilink")
      - [最佳改编剧本奖](../Page/奥斯卡最佳改编剧本奖.md "wikilink")

<!-- end list -->

  - **[英國電影電視協會獎](../Page/英國電影和電視藝術學院.md "wikilink")**
      - 最佳影片 －
        [大衛·連](../Page/大衛·連.md "wikilink")、[山姆·史匹格](../Page/山姆·史匹格.md "wikilink")
      - 最佳英國電影 －
        [大衛·連](../Page/大衛·連.md "wikilink")、[山姆·史匹格](../Page/山姆·史匹格.md "wikilink")
      - 最佳英國男演員 － [彼得·奧圖](../Page/彼得·奧圖.md "wikilink")
      - 最佳英國劇本 — *Robert Bolt*、*Michael Wilson*

<!-- end list -->

  - **[金球獎](../Page/金球獎.md "wikilink")**
      - [最佳戲劇類影片](../Page/金球獎最佳戲劇類影片.md "wikilink") －
        [大衛·連](../Page/大衛·連.md "wikilink")、[山姆·史匹格](../Page/山姆·史匹格.md "wikilink")
      - 最佳導演 － [大衛·連](../Page/大衛·連.md "wikilink")
      - 最佳男配角 － [奧瑪·雪瑞夫](../Page/奧瑪·雪瑞夫.md "wikilink")
      - 最佳新晉演員 － [奧瑪·雪瑞夫](../Page/奧瑪·雪瑞夫.md "wikilink")
      - 最佳攝影（彩色） — *Freddie Young*

## 外部链接

  -
  - [在 filmsite.org 的影評](http://www.filmsite.org/lawr.html)

[Category:1962年電影](../Category/1962年電影.md "wikilink")
[Category:英國劇情片](../Category/英國劇情片.md "wikilink")
[Category:英國傳記片](../Category/英國傳記片.md "wikilink")
[Category:第一次世界大戰電影](../Category/第一次世界大戰電影.md "wikilink")
[Category:英國戰爭片](../Category/英國戰爭片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[史詩電影](../Category/史詩戰爭片.md "wikilink")
[Category:非洲真人真事改編電影](../Category/非洲真人真事改編電影.md "wikilink")
[Category:亞洲真人真事改編電影](../Category/亞洲真人真事改編電影.md "wikilink")
[Category:奧斯卡最佳導演獲獎電影](../Category/奧斯卡最佳導演獲獎電影.md "wikilink")
[Category:奧斯卡最佳影片](../Category/奧斯卡最佳影片.md "wikilink")
[Category:英國電影學院獎最佳影片](../Category/英國電影學院獎最佳影片.md "wikilink")
[Category:英国电影学院奖最佳英国电影](../Category/英国电影学院奖最佳英国电影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:三小時以上電影](../Category/三小時以上電影.md "wikilink")
[Category:哥倫比亞影業電影](../Category/哥倫比亞影業電影.md "wikilink")
[Category:大英帝國背景電影](../Category/大英帝國背景電影.md "wikilink")
[Category:西班牙取景电影](../Category/西班牙取景电影.md "wikilink")
[Category:约旦取景电影](../Category/约旦取景电影.md "wikilink")
[Category:摩洛哥取景电影](../Category/摩洛哥取景电影.md "wikilink")
[Category:大卫·利恩电影](../Category/大卫·利恩电影.md "wikilink")
[Category:金球獎最佳劇情片](../Category/金球獎最佳劇情片.md "wikilink")
[Category:奧斯卡最佳攝影獲獎電影](../Category/奧斯卡最佳攝影獲獎電影.md "wikilink")
[Category:奧斯卡最佳美術指導獲獎電影](../Category/奧斯卡最佳美術指導獲獎電影.md "wikilink")
[Category:奥斯卡最佳剪辑获奖电影](../Category/奥斯卡最佳剪辑获奖电影.md "wikilink")
[Category:奧斯卡最佳原創配樂獲獎電影](../Category/奧斯卡最佳原創配樂獲獎電影.md "wikilink")
[Category:金球獎最佳男配角獲獎電影](../Category/金球獎最佳男配角獲獎電影.md "wikilink")
[Category:奥斯卡最佳音响效果获奖电影](../Category/奥斯卡最佳音响效果获奖电影.md "wikilink")
[Category:電影旬報十佳獎最佳外語片](../Category/電影旬報十佳獎最佳外語片.md "wikilink")