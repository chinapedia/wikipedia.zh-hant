[KWV2012.jpg](https://zh.wikipedia.org/wiki/File:KWV2012.jpg "fig:KWV2012.jpg")

**天水圍公園**（）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[天水圍新市鎮中央](../Page/天水圍新市鎮.md "wikilink")，佔地14.8[公頃](../Page/公頃.md "wikilink")，是[元朗區的大型公園](../Page/元朗區.md "wikilink")，公園的四周均由建築物所包圍，包括有[嘉湖山莊](../Page/嘉湖山莊.md "wikilink")、[置富嘉湖](../Page/置富嘉湖.md "wikilink")、[天瑞邨及](../Page/天瑞邨.md "wikilink")[天耀邨等](../Page/天耀邨.md "wikilink")，[天水圍體育館](../Page/天水圍體育館.md "wikilink")、[天水圍運動場和](../Page/天水圍運動場.md "wikilink")[天水圍游泳池座落天水圍公園旁](../Page/天水圍游泳池.md "wikilink")。

## 歷史

天水圍公園前稱**天水圍市鎮公園**，於早期規劃稱為「天水圍第22區分區公園」\[1\]\[2\]，為天水圍新市鎮的主公園。整個公園原本屬於[嘉湖山莊的發展項目之一](../Page/嘉湖山莊.md "wikilink")，並且由[長江實業附屬公司](../Page/長江實業.md "wikilink")「巍城有限公司」斥資1億1000多萬元興建\[3\]。1991年時，公園一期的棕櫚園外觀已成形，同時鄰近的[嘉湖銀座](../Page/嘉湖銀座.md "wikilink")（現時的置富嘉湖）也正進行前期工程\[4\]。天水圍公園分為兩期落成，第一期在1993年3月26日開放，第二期在1997年1月16日全面開放予居民使用。公園落成初期曾預留大量[草坪供居民在該處休憩](../Page/草坪.md "wikilink")。不過到了1998年，原有的草坪種滿了樹木，草坪不對外開放。此外，公園的[人工湖在落成初期大受歡迎](../Page/人工湖.md "wikilink")，准許進行划船的水上單車租賃服務，並設有小食部\[5\]，不過後期因不明原因，人工湖於2003年開始不准划船\[6\]。

## 設施

園內有水景設施、噴水池及人工湖營造了「天」與「水」的效果，以配合「天水圍」這名稱，還包括以下其他設施：

  - 太極園
  - 觀景亭
  - 模型船區（前身為進行水上單車活動的人工湖）
  - 模型車場
  - 香味花園
  - 門球場及練習場
  - 網球場
  - 露天劇場
  - 滑板場
  - 迷宮
  - 人造地面籃球暨排球場
  - 健身站
  - 兒童遊樂場

<File:Tin> Shui Wai Park Plase1 Enterance.jpg|天水圍公園入口 <File:HK> Tin Shui
Wai Park Avenue.jpg|天水圍公園噴泉廣場 <File:HK> Tin Shui Wai Park Plase1
Playground.jpg|兒童遊樂場

## 禁煙安排

此場地設有吸煙區，場內其他地方禁止[吸煙](../Page/吸煙.md "wikilink")\[7\]。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[天水圍站](../Page/天水圍站_\(西鐵綫\).md "wikilink")
  - [輕鐵](../Page/輕鐵.md "wikilink")[705](../Page/香港輕鐵705綫.md "wikilink")、[706](../Page/香港輕鐵706綫.md "wikilink")、[751](../Page/香港輕鐵751綫.md "wikilink")、[761P](../Page/香港輕鐵761P綫.md "wikilink")[天瑞站](../Page/天瑞站.md "wikilink")、[翠湖站](../Page/翠湖站.md "wikilink")、[天榮站](../Page/天榮站.md "wikilink")、[銀座站](../Page/銀座站.md "wikilink")、[天湖站](../Page/天湖站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 外部連結

  - [天水圍公園](http://www.lcsd.gov.hk/tc/parks/tswp/index.html)
  - [天水圍公園在2009年3月的影像](http://www.youtube.com/watch?v=32thxhLlAFY)

## 資料來源

<references/>

[分類:香港極限運動場地](../Page/分類:香港極限運動場地.md "wikilink")

[Category:元朗區公園](../Category/元朗區公園.md "wikilink")
[Category:天水圍](../Category/天水圍.md "wikilink")
[Category:香港遙控車賽車場](../Category/香港遙控車賽車場.md "wikilink")

1.  《217LS ─ 天水圍第22區分區公園第一期工程—提升為第三類工程》(區域市政局委員會備忘錄，RSD 6/HQ 726/86(9)
    III，1991年8月5日)
2.  《217LS ─ 天水圍第22區分區公園第一期工程—提升為第二類工程》(區域市政局委員會備忘錄，S/F RSD 6/HQ
    726/86(9)，1992年12月28日)
3.  《257LS — 天水圍第22區地區公園第二期工程政府與巍城有限公司合作發展的工程》(區域市政局委員會備忘錄，RSD 5/HQ
    726/89(9)，1993年12月24日)
4.
5.
6.  [康樂及文化事務署 - 天水圍公園 -
    背景](http://www.lcsd.gov.hk/parks/tswp/b5/index.html)
7.  [康樂及文化事務署 ── 公眾遊樂場的禁煙安排 ──
    元朗區](http://www.lcsd.gov.hk/specials/anti-smoking/b5/index.php?dist=18)