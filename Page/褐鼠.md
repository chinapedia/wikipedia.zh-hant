**褐家鼠**（[学名](../Page/学名.md "wikilink")：），又名**褐鼠**、**大鼠**、**挪威鼠**、**大家鼠**、**白尾吊**、**耗子**、**粪鼠**、**溝鼠**，为[鼠科](../Page/鼠科.md "wikilink")[家鼠属的动物](../Page/家鼠属.md "wikilink")，是有名及常見的[老鼠之一](../Page/老鼠.md "wikilink")，也是之中最大的物種，一般生活於田野、家舍广泛栖息。该物种的模式产地在[英国](../Page/英国.md "wikilink")\[1\]。

褐家鼠最早源自中國北方，且目前已經散佈到[南極以外所有大陸上](../Page/南極.md "wikilink")，在[歐洲成為優勢鼠類物種](../Page/歐洲.md "wikilink")，於[北美洲有也不少數量](../Page/北美洲.md "wikilink")。生活於人類居住地，尤其是都會區。溝鼠也被選出用來作為實驗用鼠類，是重要的[模式生物](../Page/模式生物.md "wikilink")，此外也是一種寵物。

人們不知為何将其命名為挪威鼠，實際上牠們並不是來自[挪威](../Page/挪威.md "wikilink")，1769年的書《Outlines
of the Natural History of Great
Britain》作者[約翰·貝克恩霍特](../Page/約翰·貝克恩霍特.md "wikilink")（John
Berkenhout）被認為可能是這誤稱的來源。貝克恩霍特給此物種的學名為*Rattus
norvegicus*，因為他相信此物種是在1728年經由[挪威船隻遷移到](../Page/挪威.md "wikilink")[英國](../Page/英國.md "wikilink")，不過事實上此物種是來自[丹麥](../Page/丹麥.md "wikilink")。

## 特徵

褐家鼠的皮毛粗糙，一般呈棕色或深灰色，而身體下半部分則呈現較淺的棕色或灰色。其身長可達25公分，而其尾巴自己就已長達25公分（相等於其身長）。雄成體平均重350克，而雌成體則為250克，但一隻褐家鼠其實最多可重達500克。甚少有過一公斤者，在故事中所述大得像[貓的褐家鼠或](../Page/貓.md "wikilink")[麝鼠純屬誇大](../Page/麝鼠.md "wikilink")。褐家鼠[聽覺敏銳](../Page/聽覺.md "wikilink")，對[超聲波極為敏感](../Page/超聲波.md "wikilink")，其[嗅覺亦極為發達](../Page/嗅覺.md "wikilink")。其平均[心率約為每分鐘](../Page/心跳.md "wikilink")300至400次，每分鐘的呼吸次數約為100次。其[視覺極差](../Page/視覺.md "wikilink")，不能分辨顏色，且接收不到長波的光線。

## 食物、棲息地及行為

褐家鼠是雜食性動物，牠飢不擇食，差不多所有可吃的均為其食物，但還是以[穀類為主](../Page/穀類.md "wikilink")。[動物行為學會](../Page/動物行為學會.md "wikilink")（Animal
Behavior Society）的創辦人[馬丁·雪恩](../Page/馬丁·雪恩.md "wikilink")（Martin
Schein）曾於1964年對褐家鼠的進食習性作了相關的研究，並最後撰了《對褐家鼠進食習性的初步分析──以垃圾為食》（"*A
Preliminary Analysis of Garbage as Food for the Norway
Rat*"）一文作研究總結，表示褐家鼠最愛吃的食物是（已循其先後次序排列）炒蛋、[通心粉](../Page/通心粉.md "wikilink")、[乾酪和烹熟了的穀粒仁](../Page/乾酪.md "wikilink")。牠們最不喜歡的食物是生[甜菜](../Page/甜菜.md "wikilink")、[桃子和生](../Page/桃子.md "wikilink")[芹菜](../Page/芹菜.md "wikilink")。牠們通常都是在夜間才活躍起來的，且都是[游泳好手](../Page/游泳.md "wikilink")，不論是在水面或水底游泳都極為擅長，但不善攀爬。牠們擅於挖洞，常會挖出大規模而複雜的地洞系統作藏身之所。一個在2007年作的研究指出，褐家鼠擁有[後設認知](../Page/後設認知.md "wikilink")。在這次研究之前，人們一直認為只有[人類及部分](../Page/人類.md "wikilink")[靈長類動物才擁有此一認知能力](../Page/靈長類.md "wikilink")。\[2\]
另外，從實驗室對實驗用的白老鼠的研究顯示：褐家鼠亦會作夢。實驗把他們的腦直接連接到電腦，以監察他們在學習走[迷宮時腦內電波的變化](../Page/迷宮.md "wikilink")。實驗人員發現，在他們在當天學習完結後，當他們[睡覺時](../Page/睡覺.md "wikilink")，腦內會把學習時腦內電波的變化走向以加快的速度倒轉重播一次，之後白老鼠就會學會怎樣走[迷宮](../Page/迷宮.md "wikilink")。\[3\]

## 繁殖

若條件許可的話，褐家鼠全年任何時間均可進行繁殖的行為，而一隻雌褐家鼠每年最多可生產5次。[妊娠期只長約](../Page/妊娠.md "wikilink")1至2個月，而每次生產的小褐家鼠數量最多可達14隻，但一般只有7隻。其最高壽限是3歲，但一般褐家鼠的壽命僅有1歲。

褐家鼠居住在人類所居住的地方。

## 疾病

褐家鼠會帶有一些疾病，包括[鈎端螺旋體病](../Page/鈎端螺旋體病.md "wikilink")、[隱孢子蟲症](../Page/隱孢子蟲症.md "wikilink")、[病毒出血熱](../Page/病毒出血熱.md "wikilink")（VHF）、[Q熱和](../Page/Q熱.md "wikilink")[漢塔病毒侵肺型徵候群](../Page/漢塔病毒侵肺型徵候群.md "wikilink")。不像[黑鼠那般](../Page/黑鼠.md "wikilink")，褐家鼠鮮少帶有[腺鼠疫](../Page/腺鼠疫.md "wikilink")。

## 寵物飼料

在自然界，鼠的天敵極多，當中包括很多猛禽如[鷹](../Page/鷹.md "wikilink")、[隼](../Page/隼.md "wikilink")、[貓頭鷹](../Page/貓頭鷹.md "wikilink")、[伯勞](../Page/伯勞.md "wikilink")、大部份[蛇類](../Page/蛇.md "wikilink")、大型[蛙類](../Page/蛙類.md "wikilink")(如[牛蛙](../Page/牛蛙.md "wikilink"))、[家貓](../Page/家貓.md "wikilink")、[狐狸](../Page/狐狸.md "wikilink")、[豬](../Page/豬.md "wikilink")。當中以蛇類對鼠類威脅最大，因蛇能夠進入鼠的巢穴裡，而且部份蛇擁有熱能感測的特殊器官，讓鼠類難以逃離蛇的追捕。

因為鼠的繁殖很快，因此牠們亦成為其他動物活生生的食物，通常是大型的[爬蟲類如](../Page/爬蟲類.md "wikilink")[蛇等](../Page/蛇.md "wikilink")。

另外，實驗用的白老鼠Wistar大鼠亦是這種鼠的[白化品系](../Page/白化.md "wikilink")。

## 亚种

  - **褐家鼠东北亚种**（[学名](../Page/学名.md "wikilink")：），Pallas于1779年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[黑龙江](../Page/黑龙江.md "wikilink")、[内蒙](../Page/内蒙.md "wikilink")、[吉林等地](../Page/吉林.md "wikilink")。该物种的模式产地在东[西伯利亚](../Page/西伯利亚.md "wikilink")。\[4\]
  - **褐家鼠华北亚种**（[学名](../Page/学名.md "wikilink")：），Milne-Edwards于1868年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[辽宁](../Page/辽宁.md "wikilink")、[江苏](../Page/江苏.md "wikilink")（北部）、[河北](../Page/河北.md "wikilink")、[山东等地](../Page/山东.md "wikilink")。该物种的模式产地在[北京附近](../Page/北京.md "wikilink")。\[5\]
  - **褐家鼠指名亚种**（[学名](../Page/学名.md "wikilink")：），Berkenhout于1769年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[海南](../Page/海南.md "wikilink")、[广东](../Page/广东.md "wikilink")、[福建南部](../Page/福建.md "wikilink")）等地。该物种的模式产地在[英国](../Page/英国.md "wikilink")。\[6\]
  - **褐家鼠甘肃亚种**（[学名](../Page/学名.md "wikilink")：），Miller于1914年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[江西](../Page/江西.md "wikilink")、[广西](../Page/广西.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[青海](../Page/青海.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[内蒙古](../Page/内蒙古.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、江苏（南部）、[宁夏](../Page/宁夏.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[山西](../Page/山西.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[四川](../Page/四川.md "wikilink")、[福建等地](../Page/福建.md "wikilink")。该物种的模式产地在[甘肃](../Page/甘肃.md "wikilink")[临潭](../Page/临潭.md "wikilink")。\[7\]

## 參考資料

<div class="references-small">

  -
  -

</div>

## 外部連結

  - [Rat Behaviour and Biology](http://www.ratbehavior.org/rats.html)
  - [Nature: Rat Genome](http://www.nature.com/nature/focus/ratgenome/)
  - [Rat Genome Database](http://rgd.mcw.edu/)
  - [Waarneming.nl](https://web.archive.org/web/20081030035051/http://waarneming.nl/soort.php?id=1509&wno_datum_van=&wno_datum_tm=&tab=info)
    Pictures, sightings and distribution maps of brown rats in the
    Netherlands.
  - [Rats, by Robert
    Sullivan](http://www.amazon.com/gp/product/1582343853/002-2567717-7601668?v=glance&n=283155)
  - [The Holy temple of Rats in
    Rajasthan](http://willypuchner.com/en/diereise/rajasthan/rajasthan_index.htm)
    - photographs of [维利卜](../Page/维利卜.md "wikilink")

[H](../Category/中國哺乳動物.md "wikilink")
[norvegicus](../Category/大家鼠屬.md "wikilink")

1.
2.

3.

4.

5.

6.

7.