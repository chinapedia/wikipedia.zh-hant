**KID** 是「**K**indle **I**magine
**D**evelop」的縮寫，乃是[日本](../Page/日本.md "wikilink")[CyberFront公司經營的](../Page/CyberFront.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")[品牌](../Page/品牌.md "wikilink")。此品牌由同名之**KID**公司於1988年創立，並經營至2006年11月底止，該公司已在同年12月1日向[東京地方法院申請自行](../Page/東京地方法院.md "wikilink")[破產](../Page/破產.md "wikilink")\[1\]。[CyberFront公司則是在](../Page/CyberFront.md "wikilink")2007年2月透過破產管理[律師接手該公司所有遊戲軟體的一切](../Page/律師.md "wikilink")[權利及業務](../Page/權利.md "wikilink")\[2\]，而開發遊戲的主要工作人員大部份轉到[5pb.工作](../Page/5pb..md "wikilink")。

KID公司經營初期曾代工一些[射擊遊戲和](../Page/射擊遊戲.md "wikilink")[動作遊戲](../Page/動作遊戲.md "wikilink")，1996年起開始制作[戀愛題材的](../Page/戀愛.md "wikilink")[冒險遊戲和](../Page/戀愛冒險遊戲.md "wikilink")[美少女遊戲](../Page/美少女遊戲.md "wikilink")。主要作品包括《[秋之回憶](../Page/秋之回憶系列.md "wikilink")》（Memories
Off）系列、《[无限轮回](../Page/无限轮回.md "wikilink")》（Infinity）系列等。部份作品已經正式翻譯成[中文](../Page/中文.md "wikilink")、[英文等](../Page/英文.md "wikilink")[語言](../Page/語言.md "wikilink")。

## 主要作品

不含廉價版。**粗體**為原[十八禁遊戲移植](../Page/十八禁遊戲.md "wikilink")，括號內為原製作公司，引號是原題。

### 1996年

  - 「」**（[Cocktail Soft](../Page/Cocktail_Soft.md "wikilink")）**
      -
        KID唯一的[十八禁遊戲](../Page/十八禁遊戲.md "wikilink")。當時[SEGA
        Saturn准許售賣十八禁遊戲](../Page/SEGA_Saturn.md "wikilink")。
  - 「」**（Cocktail Soft）**

### 1997年

  - 「[Pocket Love](../Page/Pocket_Love.md "wikilink")」
  - 「きゃんきゃんバニー エクストラ」**（Cocktail Soft）**
  - 「[坂本龍馬・維新開國](../Page/坂本龍馬・維新開國.md "wikilink")」

### 1998年

  - 「」**（[LIBIDO](../Page/LIBIDO.md "wikilink")）**
  - 「」**（Cocktail Soft）**
  - 「High School Terra Story」**（URAN）**
  - 「」**（「[Virtuacall3](../Page/Virtuacall.md "wikilink")」）**
  - 「[瑠璃色の雪](../Page/瑠璃色の雪.md "wikilink")」**（[AIL](../Page/AIL.md "wikilink")）**
  - 「」

### 1999年

  - 「[百事超人](../Page/百事超人.md "wikilink")」
  - 「前往燦爛季節」**（[Tactics](../Page/Tactics.md "wikilink")「[ONE～光辉的季节～](../Page/ONE～光辉的季节～.md "wikilink")」）**
  - 「」
  - 「[ja](../Page/{{lang.md "wikilink")」
  - 「[秋之回憶](../Page/秋之回憶.md "wikilink")」

### 2000年

  - 「」
  - 「[無限輪迴](../Page/無限輪迴.md "wikilink")」
  - 「」**（「」）**
  - 「Screen」**（「Campus」）**
  - 「」
  - 「[Never7 -the end of infinity-](../Page/無限輪迴.md "wikilink")」
  - 「Go Go Island」**（「」）**

### 2001年

  - 「[見習天使](../Page/見習天使.md "wikilink")」
  - 「[Close to ～祈願之丘～](../Page/Close_to_～祈願之丘～.md "wikilink")」
  - 「」**（「」）**
  - 「[KID MIX Session](../Page/KID_MIX_Session.md "wikilink")」
  - 「[秋之回憶2](../Page/秋之回憶2.md "wikilink")」
  - 「[Prism Heart](../Page/Prism_Heart.md "wikilink")」**（[Pajamas
    Soft](../Page/Pajamas_Soft.md "wikilink")）**

### 2002年

  - 「」**（TOPCAT「」）**
  - 「[Milky Season](../Page/Milky_Season.md "wikilink")」
  - 「[王子さまLv1](../Page/王子さまLv1.md "wikilink")」**（[Alice
    Blue](../Page/Alice_Blue.md "wikilink")）**
  - 「」
  - 「[My Merry May](../Page/My_Merry_May.md "wikilink")」
  - 「[Ever17 -the out of
    infinity-](../Page/Ever17_-the_out_of_infinity-.md "wikilink")」
  - 「」**（[light](../Page/light.md "wikilink")）**
  - 「[想君：秋之回憶](../Page/想君：秋之回憶.md "wikilink")」
  - 「」

### 2003年

  - 「[王子さまLv1.5](../Page/王子さまLv1.md "wikilink")」**（Alice Blue）**
  - 「[鳶尾花物語](../Page/鳶尾花物語.md "wikilink")」
  - 「[青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")」
  - 「[Memories Off Duet](../Page/Memories_Off_Duet.md "wikilink")」
  - 「[My Merry Maybe](../Page/My_Merry_Maybe.md "wikilink")」
  - 「」**（TAKUYO）**
  - 「[夏夢夜話](../Page/夏夢夜話.md "wikilink")」
  - 「[Memories Off MIX](../Page/Memories_Off_MIX.md "wikilink")」

### 2004年

  - 「[魔法護士小麥](../Page/魔法護士小麥.md "wikilink")」
  - 「[見習天使2wins](../Page/見習天使.md "wikilink")」
  - 「Angel's Feather」**（Blue Impact）**
  - 「CROSS†CHANNEL ～To all
    people～」**（[FlyingShine](../Page/FlyingShine.md "wikilink")「[CROSS†CHANNEL](../Page/CROSS†CHANNEL.md "wikilink")」）**
  - 「[Remember11 -the age of
    infinity-](../Page/Remember11_-the_age_of_infinity-.md "wikilink")」
  - 「Colorful BOX to Love」**（SoundTail「[Colorful
    BOX](../Page/Colorful_BOX.md "wikilink")」）**
  - 「」**（Pajamas Soft「」）**
  - 「[秋之回憶：從今以後](../Page/秋之回憶：從今以後.md "wikilink")」
  - 「」
  - 「[Monochrome](../Page/Monochrome.md "wikilink")」
  - 「水月 -迷心-」**（[F\&C
    FC01](../Page/F&C.md "wikilink")「[水月](../Page/水月.md "wikilink")」）**
  - 「」**（[Studio e.go\!](../Page/Studio_e.go!.md "wikilink")「」）**

### 2005年

  - 「[Memories Off After
    Rain](../Page/Memories_Off_After_Rain.md "wikilink")」（Vol.1 折鶴／Vol.2
    想演／Vol.3 卒業）
  - 「」**（「」）**
  - 「」
  - 「[My Merry May with be](../Page/My_Merry_May_with_be.md "wikilink")」
  - 「[White Princess the
    second](../Page/White_Princess.md "wikilink")」**（[feng](../Page/feng.md "wikilink")「」）**
  - 「」**（etude「」）**
  - 「[水之旋律](../Page/水之旋律.md "wikilink")」
  - 「[秋之回憶5：中斷的影片](../Page/秋之回憶5：中斷的影片.md "wikilink")」
  - 「」**（[Innocent
    Grey](../Page/Innocent_Grey.md "wikilink")「[恋狱月狂病](../Page/恋狱月狂病.md "wikilink")」）**

### 2006年

  - 「[Separate Hearts](../Page/Separate_Hearts.md "wikilink")」
  - 「[水之旋律 ～協奏曲～](../Page/水之旋律.md "wikilink")」
  - 「[秋之回忆：从今以后again](../Page/秋之回忆：从今以后again.md "wikilink")」
  - 「[水之旋律2 ～緋之記憶～](../Page/水之旋律.md "wikilink")」
  - 「FESTA\!\! -HYPER GIRLS PARTY-」**（Lass「[FESTA\!\! -HYPER GIRLS
    POP-](../Page/FESTA!!_-HYPER_GIRLS_POP-.md "wikilink")」）**
  - 「[We Are\*](../Page/We_Are*.md "wikilink")」
  - 「[龍刻 RYU-KOKU](../Page/龍刻_RYU-KOKU.md "wikilink")」
  - 「」**（[F\&C FC02](../Page/F&C.md "wikilink")「」）**

## 资料来源

## 外部連結

  - [Kid Official
    HP](https://web.archive.org/web/20070105182250/http://www.kid-game.co.jp/index_1.html)，存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

  - [KID game Area](http://www.mowypan.com/KID)（玩家自行整理）

[KID](../Category/KID.md "wikilink")
[Category:日本已結業電子遊戲公司](../Category/日本已結業電子遊戲公司.md "wikilink")
[Category:1988年開業電子遊戲公司](../Category/1988年開業電子遊戲公司.md "wikilink")
[Category:2006年結業公司](../Category/2006年結業公司.md "wikilink")

1.
2.