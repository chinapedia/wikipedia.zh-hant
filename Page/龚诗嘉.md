**龔詩嘉**（英文名：，），[新加坡女歌手](../Page/新加坡.md "wikilink")，出生于[新加坡](../Page/新加坡.md "wikilink")，籍貫[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")。2007年12月25日
與[華研國際音樂合約結束](../Page/華研國際音樂.md "wikilink")。

## 出道經歷

透過[經紀人](../Page/經紀人.md "wikilink")[吳慶隆的介紹](../Page/吳慶隆.md "wikilink")，還未出片時，龔詩嘉就與知名歌手，同時也身兼[製作人的陶喆見面](../Page/製作人.md "wikilink")，聊天，一起討論歌曲的概念、走向等等。

令龔詩嘉驚訝的是，陶喆雖然已經是華人音樂圈第一把交椅的製作人，但他絲毫不擺[架子](../Page/架子.md "wikilink")，也不因龔詩嘉只是一個新人就草率了事，從一開始便積極主動約龔詩嘉見面，陶喆認為一定要先認識、了解將要詮釋這首歌的歌手是誰，才能精準地捕捉到歌曲要傳達的感覺。

龔詩嘉表示:一開始見到[陶喆時她很緊張](../Page/陶喆.md "wikilink")，但是陶喆不斷的讓她感到輕鬆、自然，陶喆拿著一把吉他，隨性地哼哼唱唱，很free、很真，自然而然地也讓龔詩嘉感受到這樣的氣氛、卸下原來的陌生、僵硬，打開心防與陶喆分享自己對音樂的看法，於是才有了專輯中「遠遠在一起」這首歌的誕生。另外最讓大家印象深刻的當然就是在首支[MV](../Page/MV.md "wikilink")「再一次擁有」中，同門師姐[S.H.E.的](../Page/S.H.E..md "wikilink")[Ella跨刀協助拍攝](../Page/Ella.md "wikilink")。龔詩嘉表示，由於自己絲毫沒有戲劇經驗，因此得知MV中必須有戲劇演出時十分緊張，在拍攝現場時，也幸虧有Ella的鼓勵，以及不斷傳授她拍攝音樂[錄影帶的一些小技巧](../Page/錄影帶.md "wikilink")，才使得她漸漸適應這樣的方式。

## 初次來台

對於從[南洋來到](../Page/南洋.md "wikilink")[亞熱帶的](../Page/亞熱帶.md "wikilink")[新加坡女孩龔詩嘉來說](../Page/新加坡.md "wikilink")，台灣給他的第一個印象是「四季分明」的氣候。在新加坡一年到頭都身著清涼裝扮的龔詩嘉，第一件任務就要是學會適應台灣[冬天的寒冷](../Page/冬天.md "wikilink")，第一次出國的她認為這也是極為新鮮的體驗。另外龔詩嘉訝異台灣的一點就是，店家總是不打烊，例如[誠品書店](../Page/誠品書店.md "wikilink")、[便利商店等等](../Page/便利商店.md "wikilink")，都可以維持二十四小時營業，讓人不論身處何處都十分便利。除此之外，名聞遐邇的台灣小吃也總是讓龔詩嘉食指大動，她特別喜歡[甜不辣](../Page/甜不辣.md "wikilink")、麵線，也喜歡和台灣友人一起逛台灣的各大[夜市](../Page/夜市.md "wikilink")，大快朵頤一番。

比起法規謹嚴的新加坡，台灣的交通也同樣讓龔詩嘉大開眼界。尤其是大街小巷無所不在的[機車流竄景象更讓來自新加坡的她大呼驚奇](../Page/機車.md "wikilink")，龔詩嘉更向我們透漏，由於新加坡的機車不多，所以她第一次看到「[待轉區](../Page/待轉區.md "wikilink")」這個東西時，便十分好奇它的用處是什麼，為什麼要劃一個四四方方的小格子在馬路上，後來經過朋友的解釋她才了解待轉區的功用何在。對於龔詩嘉來說，台灣是個處處充滿驚奇的地方。

## 作品

### 专辑

  - 2005年11月4日《好‧詩嘉》（首張個人專輯）

<!-- end list -->

1.  再一次擁有
2.  想不想自由
3.  遠遠在一起
4.  歷險記
5.  十字路口
6.  白鴿
7.  如果你真愛我
8.  一天天
9.  起初只是朋友
10. 思念

### 其他

  - 2005年《[真命天女原聲帶](../Page/真命天女.md "wikilink")》

<!-- end list -->

  -
    收錄「再一次擁有」

<!-- end list -->

  - 2006年《[東方茱麗葉原聲帶](../Page/東方茱麗葉.md "wikilink")》

<!-- end list -->

  -
    收錄「放不下」

<!-- end list -->

  - 2006年《[白色巨塔原聲帶](../Page/白色巨塔_\(2006年電視劇\).md "wikilink")》

<!-- end list -->

  -
    收錄「你要離開一些時候」

## 參考資料

  - [I'm Vlog-龔詩嘉-遠遠在一起](http://www.im.tv/vlog/personal/412023/3656266)
  - [I'm Vlog-龔詩嘉-思念](http://www.im.tv/vlog/personal/412023/3656301)
  - [好女聲、好詩嘉！(上)
    －龔詩嘉](http://www.youthwant.com.tw/column/index.php?d=0512194)
  - [好女聲、好詩嘉！(下)
    －龔詩嘉](http://www.youthwant.com.tw/column/index.php?d=0512264)
  - [龔詩嘉所有曲目](https://web.archive.org/web/20090212145627/http://web.ezpeer.com/song/s9.html)
  - [KKBOX-龔詩嘉
    【好.詩嘉】專輯介紹](https://web.archive.org/web/20090108234133/http://www.kkbox.com.tw/funky/web_info/A6zvkmUFXHw5MgD00Djd008l.html)
  - [ezPeer+ – 龔詩嘉
    歷年專輯](https://web.archive.org/web/20081218083155/http://web.ezpeer.com/singer/s9.html)
  - [徵求龔詩嘉心目中的白瑞德活動](http://friends.yam.com/event/2005/minnie/index.html)
  - [新浪網-音樂-龔詩嘉的專輯](https://web.archive.org/web/20080923120123/http://music.sina.com.hk/cgi-bin/mu/star/main.cgi?id=12595)
  - [思念-龔詩嘉](http://podcast.blog.webs-tv.net/m/807998)

## 外部連結

  - [KKBOX龔詩嘉討論](http://www.kkbox.com.tw/forum_web/topic_index.php?topic_id=7251)
  - [蕃薯藤-追星頻道-明星檔案](http://stars.yam.com/file/star.php?si=000630)
  - [und聯合追星網-明星-龔詩嘉專區](http://stars.udn.com/udnstars/StarBasic/StarBasic2.do?starID=AS05125)
  - [龔詩嘉KWONG
    SHIJIA](https://web.archive.org/web/20141015014042/http://ent.sina.com.tw/music/starinfo/1729)

[K](../Category/新加坡女歌手.md "wikilink")
[Category:龔姓](../Category/龔姓.md "wikilink")
[K](../Category/新加坡國立大學校友.md "wikilink")