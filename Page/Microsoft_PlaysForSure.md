[Playsforsure.gif](https://zh.wikipedia.org/wiki/File:Playsforsure.gif "fig:Playsforsure.gif")

**Microsoft
PlaysForSure**是一種認證標誌，由[微軟發給能與](../Page/微軟.md "wikilink")[Microsoft
Windows相容](../Page/Microsoft_Windows.md "wikilink")，並支援微軟[數位著作權管理](../Page/數位著作權管理.md "wikilink")（DRM）的[媒體播放器](../Page/媒體播放器.md "wikilink")。用意為提示消費者該[数字音频播放器或](../Page/數字音頻播放器.md "wikilink")[視訊播放器能播放的音频和視频格式類型](../Page/視訊播放器.md "wikilink")。PlaysForSure標誌使用在數位音樂播放器與便携视频播放器的包裝上，以及與PlaysForSure相容的[線上音樂商店](../Page/線上音樂商店.md "wikilink")。

## 概要

此標誌證明該裝置可以播放使用[Windows Media
Player第](../Page/Windows_Media_Player.md "wikilink")10版的[数字版权管理方案](../Page/数字版权管理.md "wikilink")[Windows
Media DRM錄製的](../Page/Windows_Media_DRM.md "wikilink")[Windows Media
Audio](../Page/Windows_Media_Audio.md "wikilink")（WMA）或[Windows Media
Video](../Page/Windows_Media_Video.md "wikilink")（WMV）檔案。這代表著必須支援「[Janus](../Page/Janus_\(DRM\).md "wikilink")」（WMDRM-PD）。

許多-{zh-hans:在线; zh-hant:線上; :zh-hk:網路;}-音樂商店都銷售此種格式的數位媒體，而廣受歡迎的[iTunes
Store](../Page/iTunes_Store.md "wikilink")（iTunes商店）則是使用另一種與之競爭的格式（[AAC](../Page/AAC.md "wikilink")+[FairPlay](../Page/FairPlay.md "wikilink")
DRM）。iTunes
Store中的檔案皆使用「[FairPlay](../Page/FairPlay.md "wikilink")」DRM技術加密，而非PlayForSure產品使用的Windows
Media DRM。由於格式的不同，從iTunes
Store購買的檔案無法在PlaysForSure的裝置上播放。同樣的，[蘋果電腦](../Page/蘋果電腦.md "wikilink")（Apple）廣受歡迎的個人音樂播放器[iPod以及](../Page/iPod.md "wikilink")[iTunes皆不支援Windows](../Page/iTunes.md "wikilink")
Media的檔案。

通過PlaysForSure認證，使用[媒體傳輸協定](../Page/媒體傳輸協定.md "wikilink")（MTP）或[USB大量存取裝置規格](../Page/USB大量存取裝置規格.md "wikilink")（MSC）的裝置可以播放帶有[DRM的Windows](../Page/數字版權管理.md "wikilink")
Media Audio檔案。

## 支援PlaysForSure的硬體廠商

  - [Archos](../Page/Archos.md "wikilink")
  - [創新科技](../Page/創新科技.md "wikilink")（Creative Labs）
  - [iRiver](../Page/iRiver.md "wikilink")
  - [RCA](../Page/RCA.md "wikilink")
  - [三星](../Page/三星電子.md "wikilink")（Samsung）
  - [SanDisk](../Page/SanDisk.md "wikilink")
  - [東芝](../Page/東芝.md "wikilink")（Toshiba）

## 批評

PlayForSure的裝置在與[Windows Media
Player連接時](../Page/Windows_Media_Player.md "wikilink")，僅會顯示出Windows
Media
Player能夠播放的音樂（或影片）檔案，而非裝置能夠播放的檔案。因此使用者可能會認為是他們的裝置不支援非微軟的音訊格式（例如：[Ogg
Vorbis格式](../Page/Ogg_Vorbis.md "wikilink")），但實際上卻是Windows Media
Player不支援那些格式。而Windows Media Player也不會將那些歌曲傳送到可攜式裝置中。

在最近的一個訴訟事件中，對於微軟在「可攜式裝置授權協議」中使用的文字提出了嚴厲批評\[1\]。該協議內容規範與Windows Media
Player相容的可攜式裝置製造商，禁止他們使用非微軟的音訊編碼格式。而微軟表明這份用詞不當的的協議，是因為一位年輕的微軟員工疏忽出錯所造成。並在法官的指示下，迅速修正他們用詞失當的許可協議內容。

由於引進PlaysForSure和[媒體傳輸協定](../Page/媒體傳輸協定.md "wikilink")（Media Transfer
Protocol），可攜式裝置製造商能獲得額外的媒體傳輸和音樂管理[軟體](../Page/軟體.md "wikilink")。如果有夠多的製造商加入，開發費用和裝置之間的差異性都會降低。開發費用的降低能讓消費者受益，但也會減少產品的選擇性，甚至可能造成[壟斷](../Page/壟斷.md "wikilink")。

過去在連接裝置時，電腦中會顯示裝置為「[USB大量存取裝置](../Page/USB大量存取裝置.md "wikilink")」。而新的[媒體傳輸協定](../Page/媒體傳輸協定.md "wikilink")（Media
Transfer
Protocol；MTP），則有一些-{zh-hans:用户;zh-hk:使用者;zh-tw:使用者;}-認為較以前的方式不便許多。以前大部分的系統不需額外的軟體就能支援這類裝置，但是不論所連接的裝置實際功能，卻都以類似硬碟的「USB大量儲存裝置」（MSC）形態出現。而新的MTP則允許裝置在作業系統中以媒體播放器、相機、電話等形態出現。此外也有使用者擔心MTP也無法讓使用者如同MSC利用裝置進行檔案的共享，但實際上並非如此。[Windows
Vista特別針對MTP進行了加強](../Page/Windows_Vista.md "wikilink")，讓傳輸檔案的方式與MSC相同。

微軟的[Zune發表時](../Page/Zune.md "wikilink")，因為證實了「[Zune不兼容來自PlaysForSure零售商的媒體](http://www.engadget.com/2006/09/14/the-engadget-interview-j-allard-microsoft-corporate-vice-presi/)」而造成嘩然。

## 移除

有某些工具程式能解除Windows
Media的DRM限制。其中一個例子是「[FairUse4WM](../Page/FairUse4WM.md "wikilink")」。\[2\]

## 注釋

<div style="font-size: small">

<references/>

</div>

## 外部連結

  - [PlaysForSure官方網站](http://www.playsforsure.com)

[Category:微軟](../Category/微軟.md "wikilink")

1.  《[法官嚴厲批評微軟企圖壟斷音樂裝置](http://www.theregister.co.uk/2005/10/27/accidental_music_monopoly_bid/)》
2.