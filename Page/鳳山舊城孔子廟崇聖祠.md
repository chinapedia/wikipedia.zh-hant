**鳳山舊城孔子廟崇聖祠**位於[台灣](../Page/台灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[左營區蓮潭路上的舊城國小內](../Page/左營區.md "wikilink")，係[康熙二十五年](../Page/康熙.md "wikilink")（1686年）[鳳山首任知縣](../Page/鳳山縣_\(台灣\).md "wikilink")[楊芳聲所創建](../Page/楊芳聲.md "wikilink")，現為三級古蹟。前有[蓮池潭為天然](../Page/蓮池潭.md "wikilink")[泮池](../Page/泮池.md "wikilink")，遠處有[打鼓](../Page/打鼓山.md "wikilink")、[半屏兩山左右拱衛](../Page/半屏山.md "wikilink")，近則有龜山、蛇山相互環抱，地勢鍾靈毓秀，廟置其間，形成人文勝地。後因遭風雨損壞，於[乾隆及](../Page/乾隆.md "wikilink")[光緒年間皆有修建紀錄](../Page/光緒.md "wikilink")。經兩次重修、重建，整個學宮的周長有122丈餘，可謂體制皆備，規模宏大（僅次於[臺南孔廟](../Page/臺南孔廟.md "wikilink")）。至1895年[甲午戰爭戰敗](../Page/甲午戰爭.md "wikilink")，[清廷割讓](../Page/清.md "wikilink")[臺灣與](../Page/臺灣.md "wikilink")[日本後缺乏整修](../Page/日本.md "wikilink")，歲久傾毀。而後日人又於廟內設「舊城公學校」，使原有廟貌再被破壞。[二戰後](../Page/臺灣戰後時期.md "wikilink")，僅剩一座崇聖祠\[1\]，原有九開間，現只剩中央三間的享堂了。崇聖祠是清初康熙年間的建築風格，彌足珍貴。為保留這僅存的建築物，近年曾整修完成一部份規模，並將散置附近的清代古碑收集起來，重新立於崇聖祠後方，包括「奉旨文武官員軍民人等至此下馬」碑等碑林十方，極具文獻價值。

## 註解

<div class="references-small">

<references />

</div>

## 參考資料

  - 曾光正主編，《左營鳳山縣舊城建城180週年導覽手冊》，2006年，高雄市政府文化局。
  - 高雄市政府建設局編印，《左營蓮池潭觀光導覽手冊》，高雄市政府建設局。

## 外部連結

  - [文化部文化資產局——鳳山舊城孔子廟崇聖祠](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/19850819000084)
  - [高雄市政府文化局——鳳山舊城孔子廟崇聖祠](http://heritage.khcc.gov.tw/Heritage.aspx?KeyID=a9c01648-c06f-4afe-823d-a38e866fdb3f)

[Category:台灣孔廟](../Category/台灣孔廟.md "wikilink")
[F](../Category/高雄市廟宇.md "wikilink")
[Category:高雄市古蹟](../Category/高雄市古蹟.md "wikilink")
[Category:高雄市歷史](../Category/高雄市歷史.md "wikilink")
[Category:左營區](../Category/左營區.md "wikilink")
[Category:高雄市旅遊景點](../Category/高雄市旅遊景點.md "wikilink")

1.  現有專家研究指出現存建築並非「**崇聖祠**」而是「**文昌祠**」，但普遍的說法仍是崇聖祠，有待查證。