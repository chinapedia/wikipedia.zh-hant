**亨利·维克托·勒尼奥**（，），[法国化学家](../Page/法国.md "wikilink")、物理学家，因精确测量气体热力性质而闻名，是早期热力学家之一。

## 生平

1810年7月21日出生于[德国](../Page/德国.md "wikilink")[亚琛](../Page/亚琛.md "wikilink")。

[R](../Category/1810年出生.md "wikilink")
[R](../Category/1878年逝世.md "wikilink")
[R](../Category/法国化学家.md "wikilink")
[R](../Category/法国物理学家.md "wikilink")
[R](../Category/巴黎綜合理工學院校友.md "wikilink")
[Category:马泰乌奇奖章获得者](../Category/马泰乌奇奖章获得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:拉姆福德奖章获得者](../Category/拉姆福德奖章获得者.md "wikilink")