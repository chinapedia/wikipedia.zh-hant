[Penghu_201506.jpg](https://zh.wikipedia.org/wiki/File:Penghu_201506.jpg "fig:Penghu_201506.jpg")

**澎湖群島**，亦稱**澎湖列島**，是位於[臺灣海峽上的一組](../Page/臺灣海峽.md "wikilink")[群島](../Page/群島.md "wikilink")，東距[臺灣本島約](../Page/臺灣本島.md "wikilink")50[公里](../Page/公里.md "wikilink")，西離[歐亞大陸約](../Page/歐亞大陸.md "wikilink")140公里，由90個大小[島嶼組成](../Page/島嶼.md "wikilink")，總面積約為128平方公里，全境均隸屬[中華民國](../Page/中華民國.md "wikilink")[臺灣省](../Page/臺灣省.md "wikilink")[澎湖縣管轄](../Page/澎湖縣.md "wikilink")，為中華民國[實際管轄區域第一大](../Page/臺灣地區.md "wikilink")[離島群](../Page/群島.md "wikilink")。16世紀時[葡萄牙殖民者來到](../Page/葡萄牙.md "wikilink")[東方](../Page/東方世界.md "wikilink")，發現澎湖海域魚產豐富，在島上住著許多[漁民](../Page/漁民.md "wikilink")，因此稱呼澎湖為**漁翁島**（）。澎湖群島多漁港，夜間萬點漁火流動，忽明忽滅，與水中映射之星-{斗}-互相煇映，1953年[臺灣省政府將](../Page/臺灣省政府.md "wikilink")「澎湖漁火」選定作為[臺灣八景之一](../Page/臺灣八景.md "wikilink")。\[1\][觀光遊憩是現今澎湖群島的重要產業之一](../Page/觀光.md "wikilink")，中華民國[交通部觀光局也在](../Page/交通部觀光局.md "wikilink")1995年於澎湖群島設立「[澎湖國家風景區](../Page/澎湖國家風景區.md "wikilink")」。

## 歷史

### 史前

澎湖古名「島夷」、「方壺」、「西瀛」、「亶州」、「平湖」\[2\]。最早在四千五百年前即有人居，當時可能是前往澎湖一帶從事漁撈活動的短期居民。

九到十世紀時（約在[唐朝中期以後](../Page/唐.md "wikilink")），[漢族開始長期定居澎湖](../Page/漢族.md "wikilink")\[3\]。

### 宋代

這個時期，根據文獻記載，澎湖列島又稱彭湖、平湖嶼。

[中國文獻對澎湖最早的記載出現於](../Page/中國.md "wikilink")[南宋](../Page/南宋.md "wikilink")，[樓鑰](../Page/樓鑰.md "wikilink")《攻媿集》卷八十八泉州知府〈[汪大猷行狀](../Page/汪大猷.md "wikilink")〉：[乾道七年](../Page/乾道_\(南宋\).md "wikilink")（1171年）四月，起知泉州，到郡……郡實臨海，中有沙洲數萬畝，號平湖……」文中並描述[汪大猷為保護在平湖的漢人不被](../Page/汪大猷.md "wikilink")[毗舍邪國人](../Page/毗舍邪國.md "wikilink")（傳說是今[菲律賓](../Page/菲律賓.md "wikilink")[米沙鄢群島上的原住民](../Page/米沙鄢群島.md "wikilink")）所劫掠，乃在平湖造屋二百間遣將駐守。平湖與澎湖在[閩南語中發音相近](../Page/閩南語.md "wikilink")，臺灣學界便普遍同意平湖即澎湖。由此可知中國最晚在南宋時已派兵駐守，將澎湖收為版圖\[4\]。

一直到1278年，元朝把泉州升格為[泉州路](../Page/泉州路.md "wikilink")，歷史走進新的時代。

### 元代

1278年，[大元政府入主泉州後](../Page/元朝.md "wikilink")。到了1281年，始置[澎湖寨巡檢司](../Page/澎湖寨巡檢司.md "wikilink")。

根據相關史書記載，駐於[澎湖](../Page/澎湖縣.md "wikilink")[馬公的巡檢官職](../Page/馬公市.md "wikilink")，是元朝最基層的官員，而在1366年在任的澎湖寨巡檢司主官[陳信惠是有文獻記載的首位漢人官員](../Page/陳信惠.md "wikilink")。

### 明代

[MAO_KUN_MAP-17.jpg](https://zh.wikipedia.org/wiki/File:MAO_KUN_MAP-17.jpg "fig:MAO_KUN_MAP-17.jpg")
[Dutch-pescadores.png](https://zh.wikipedia.org/wiki/File:Dutch-pescadores.png "fig:Dutch-pescadores.png")

1370年（洪武三年），明政府“罢太仓黄渡市舶司”；1374年（洪武七年），明政府再下令撤销自唐以来即存在的、负责海外贸易的福建泉州、浙江明州、广东广州三市舶司；1381年（洪武十四年），朱元璋以倭寇仍不稍敛足迹，又下令禁濒海民私通海外诸国”，此后每隔一两年即将该海禁政策再次昭示天下\[5\]。1384年（洪武十七年），澎湖寨巡檢司亦廢。

15世纪[永乐](../Page/永樂_\(明朝\).md "wikilink")[宣德年间郑和下西洋时留下的](../Page/宣德.md "wikilink")[郑和航海图中的平湖屿就是澎湖群島](../Page/郑和航海图.md "wikilink")。

16世紀時[葡萄牙人來到東方](../Page/葡萄牙.md "wikilink")，發現澎湖海域魚產豐富，島上住著許多漁民，因此稱呼澎湖為漁翁島（Pescadores）。

1563年，考量沿海安全因素，復置澎湖寨巡檢司。

1567年（隆慶元年），史稱[隆慶開關](../Page/隆慶開關.md "wikilink")。

1604年（萬曆三十二年），荷蘭東印度公司司令官[韋麻郎](../Page/韋麻郎.md "wikilink")（Wijbrant Van
Waerwijck）率兩艘船抵達澎湖，並派員往[福建請求貿易](../Page/福建.md "wikilink")。福建當局立即嚴禁人民出海接濟，並派都司[沈有容率兵船五十艘前往澎湖](../Page/沈有容.md "wikilink")，要求荷蘭人撤退。韋麻郎見求通商無望，又缺乏補給，於是在當年底離開澎湖。明朝政府於島上立有[沈有容諭退紅毛番碑](../Page/沈有容諭退紅毛番碑.md "wikilink")。

1622年（天啓二年），由於與葡萄牙人爭奪[澳門的](../Page/澳門.md "wikilink")[軍事攻擊行動失敗](../Page/葡荷澳門戰役.md "wikilink")，[荷蘭東印度公司的武裝艦隊](../Page/荷蘭.md "wikilink")（包括七艘軍艦、戰士九百人）最後占領了澎湖並且在[馬公風櫃尾蛇頭山頂設立了基地](../Page/馬公市.md "wikilink")，荷蘭東印度公司的武裝軍隊強迫當地的居民建設碉堡，並使得奴役工作的1500個工人的其中1300人死亡\[6\]\[7\]。又封鎖了漳州出海口，明朝海軍無法出動。

1623年（天啓三年）11月，[南居益故意邀請荷蘭人前往廈門談判](../Page/南居益.md "wikilink")，在宴會上囚禁荷蘭代表團，並乘機襲擊燒毀了入侵明朝沿海的荷蘭戰艦。

天啟四年（1624年）2月，福建巡撫南居益親自乘船到金門，下令明軍（總兵力達1萬人、兵船2百艘）渡海出擊收復澎湖。福建總兵[俞咨皋](../Page/俞咨皋.md "wikilink")、守備[王夢熊](../Page/王夢熊.md "wikilink")，率領兵船至澎湖，登陸白沙島，與荷軍接戰。但荷蘭軍隊依仗堅固的工事與戰艦頑抗，澎湖久攻不下。『澎湖廳志』記載道：紅木埕城要塞「砲樓堅緻如鐵，巡撫南宮益，遣兵攻之，賊首高文律拒守不下，官軍以藥轟之，樓傾下海。」

天啟四年（1624年）7月，南居益又派出火銃部隊支援，明軍發動總攻，一直打到風櫃仔的紅毛城下，然後雙方又形成僵持的局面。

天啟四年（1624年）8月，明軍再次兵分三路，直逼夷城，荷蘭人勢窮力孤，不得不撤離占領了兩年的澎湖。[明朝政府警告駐紮的荷蘭艦隊澎湖為其領土](../Page/明朝.md "wikilink")，並建議他們移往並非[明朝領土的](../Page/明朝.md "wikilink")[台灣設立基地](../Page/台灣.md "wikilink")。

天啟四年（1624年）9月，福建巡撫南居益、總兵俞咨皁統率三軍與荷軍苦戰七個月，炸毀紅毛城，終於收復澎湖，荷將高文律（Kobenloet）等十二人被補。此一戰役，明軍共動用兵船二百隻，兵力一萬人，交戰七月，耗軍費達十七萬七千餘兩，明軍生擒荷軍守將高文律，其餘荷蘭殘兵敗將倉皇逃往臺灣南部，建立[熱蘭遮城](../Page/熱蘭遮城.md "wikilink")，開啟了台灣史上近四十年的荷蘭統治時期。荷將高文律等人被被解送北京，[明熹宗並](../Page/明熹宗.md "wikilink")『祭告郊廟，御門受俘，刑高文律等於西市，傳首各邊，以昭示天下』。

然而此戰之後，明朝雖再次取得澎湖，卻未復設澎湖巡檢司之職。

### 明鄭

1661年郑成功自鹿耳門登陸臺灣之後，在澎湖设置[安抚司](../Page/安抚司.md "wikilink")。

### 清代

1682年，[康熙排除朝廷中反对意见](../Page/康熙.md "wikilink")，决定攻台，命福建总督[姚启圣](../Page/姚启圣.md "wikilink")“统辖福建全省兵马，同提督施琅，进取[澎湖](../Page/澎湖.md "wikilink")、臺湾”，授[萬正色為步兵提督領軍](../Page/萬正色.md "wikilink")12萬進駐福建，接應水師提督[大將軍施琅](../Page/大將軍.md "wikilink")，俱受[姚啟聖節制](../Page/姚啟聖.md "wikilink")。

1683年六月，施琅指揮清軍水師先行在[澎湖海戰對明鄭水師獲得大勝](../Page/澎湖海戰.md "wikilink")。

1727年（雍正5年），[雍正應閩浙總督高其倬之請](../Page/雍正.md "wikilink")，原附屬於台灣縣之澎湖設立澎湖廳，隸屬福建省台灣府管轄。

1885年3月14日，法國政府停止增援雞籠戰事，並命[孤拔攻佔澎湖](../Page/孤拔.md "wikilink")。3月29日，孤拔率領遠東艦隊8艘戰艦進攻媽宮城（今澎湖縣馬公市），清軍將領梁景夫死守四角仔要塞，與以砲轟還擊，但法軍火力強大，至3月31日時幾乎夷平整座要塞，鎮守清軍死傷慘重、完全瓦解，法軍得以順利佔領澎湖\[8\]。

1885年4月3日，[孤拔將雞籠法軍撤至澎湖](../Page/孤拔.md "wikilink")，準備調往越南增援。4月14日雙方開始談和，令孤拔解除對台封鎖。4月15日，法軍正式解除封鎖。但於和約訂下為止都仍占領澎湖。

1885年5月至6月間，澎湖發生瘟疫，法軍病死若干，甚至司令孤拔也罹患赤痢，並快速惡化成貧血。

1885年6月9日，在英國的調停下，李鴻章和法國公使巴德諾（Patenotre）在天津簽訂了《中法天津條約》。6月11日，[孤拔病歿於澎湖馬公](../Page/孤拔.md "wikilink")。

1885年6月13日，法國遠東艦隊接獲和談通知，6月16日由李士卑斯代理艦隊司令一職，前往雞籠交換俘虜，並於6月21日撤離雞籠，8月4日完全撤離澎湖。

1885年（光緒11年），[台灣建省](../Page/台灣建省.md "wikilink")，澎湖改隸於臺灣省。

### 日本

1895年3月，在[日清戰爭交戰國日本與清國仍在談判之時](../Page/日清戰爭.md "wikilink")，日軍已先行發動[澎湖之役](../Page/澎湖之役_\(1895年\).md "wikilink")，攻下澎湖群島。

1895年4月，依據《[馬關條約](../Page/馬關條約.md "wikilink")》第二条明文规定“澎湖群岛”與台灣島及所有附属各岛屿一同被割讓給日本，设澎湖岛厅，1897年改称[澎湖厅](../Page/澎湖厅.md "wikilink")。

1900年4月，澎湖島開始建造名為大山堡壘的要塞。1901年3月，第一期的要塞工事完工。

1902年5月，日軍在澎湖島設置要塞司令部。

1913年，島上的電氣會社開始供應電力。

1920年，整備郵遞制度，政策促進日本企業的投資。

1926年，澎湖水産會成立。

1927年，馬公巴士公司整備中。

1928年（昭和3年）11月8日，[澎湖神社啟用](../Page/澎湖神社.md "wikilink")，二次大戰後被拆毀，改為忠烈祠。

### 中華民國

1945年，[二次大戰](../Page/二次大戰.md "wikilink")[終戰](../Page/終戰.md "wikilink")，中華民國國民政府[接管臺灣](../Page/臺灣戰後時期.md "wikilink")，就原辖地區設立了[澎湖縣](../Page/澎湖縣.md "wikilink")，隸臺灣省管轄。

1991年，[中華民國](../Page/中華民國.md "wikilink")[交通部觀光局設立](../Page/交通部觀光局.md "wikilink")「澎湖風景特定區籌備處」。

1995年，正式成立[澎湖國家風景區](../Page/澎湖國家風景區.md "wikilink")，目標和宗旨為建設澎湖成為「國際級海上觀光休閒渡假中心」。

## 地理

### 位置範圍

澎湖群島南海的虎井嶼與望安島之間海域有[北迴歸線通過](../Page/北迴歸線.md "wikilink")，絕對位置由北緯23°12至23°47，經度與[百慕達相差約一百八十度](../Page/百慕達.md "wikilink")，由東經119°19至119°43間，因此澎湖群島又被稱為**東方的百慕達**。\[9\]

### 島嶼構成

澎湖群島由90個大小島嶼所組成，以下列出主要有人居住的島嶼

  - **[澎湖本島](../Page/澎湖本島.md "wikilink")(大山嶼)**（[馬公市](../Page/馬公市.md "wikilink")、[湖西鄉](../Page/湖西鄉.md "wikilink")）
  - **[白沙島](../Page/白沙島_\(澎湖\).md "wikilink")**（[白沙鄉](../Page/白沙鄉_\(台灣\).md "wikilink")）
  - **[西嶼](../Page/西嶼.md "wikilink")(漁翁島)**（[西嶼鄉](../Page/西嶼鄉.md "wikilink")）
  - **[望安島](../Page/望安島.md "wikilink")**（[望安鄉](../Page/望安鄉.md "wikilink")）
  - **[七美嶼](../Page/七美嶼.md "wikilink")(大嶼)**（[七美鄉](../Page/七美鄉_\(台灣\).md "wikilink")）
  - **[吉貝嶼](../Page/吉貝嶼.md "wikilink")**（白沙鄉）
  - **[鳥嶼](../Page/鳥嶼.md "wikilink")**（白沙鄉）
  - **[中屯嶼](../Page/中屯嶼.md "wikilink")**（白沙鄉）
  - **[大倉嶼](../Page/大倉嶼.md "wikilink")**（白沙鄉）
  - **[員貝嶼](../Page/員貝嶼.md "wikilink")**（白沙鄉）
  - **[目斗嶼](../Page/目斗嶼.md "wikilink")**（白沙鄉）
  - **[小門嶼](../Page/小門嶼.md "wikilink")**（西嶼鄉）
  - **[虎井嶼](../Page/虎井嶼.md "wikilink")**（馬公市）
  - **[桶盤嶼](../Page/桶盤嶼.md "wikilink")**（馬公市）
  - **[花嶼](../Page/花嶼.md "wikilink")**（望安鄉）
  - **[將軍澳嶼](../Page/將軍澳嶼.md "wikilink")**（望安鄉）
  - **[東吉嶼](../Page/東吉嶼.md "wikilink")**（望安鄉）
  - **[西吉嶼](../Page/西吉嶼.md "wikilink")**（望安鄉）
  - **[東嶼坪嶼](../Page/東嶼坪嶼.md "wikilink")**（望安鄉）
  - **[西嶼坪嶼](../Page/西嶼坪嶼.md "wikilink")**（望安鄉）

### 地質地貌

90個大小不等的島嶼裡面，除了最西邊的花嶼是由[安山岩組成之外](../Page/安山岩.md "wikilink")，其他都是由[玄武岩組成](../Page/玄武岩.md "wikilink")。群島的熔岩是於約
1740 萬年前至 800
萬年前，由許多小的裂隙孔洞噴出。各島嶼的地貌和夾層[沉積岩皆十分平緩](../Page/沉積岩.md "wikilink")。所以澎湖的平坦面是真正的[熔岩流台地面](../Page/熔岩.md "wikilink")，而不是差異侵蝕造成的侵蝕面。

面向太平洋的亞洲大陸邊緣地帶在大地構造上是一連串的[斷層](../Page/斷層.md "wikilink")[盆地](../Page/盆地.md "wikilink")，斷層盆地是因為地殼受到張力拉開造成的，會形成一系列的[地塹和](../Page/地塹.md "wikilink")[地壘](../Page/地壘.md "wikilink")，而澎湖正好位於南海盆地的北端，因此也有類似的地形構造。

群島地表平坦，缺少地形的高低變化。

### 植物與氣候

植物只有矮草和[灌木](../Page/灌木.md "wikilink")，跟台灣本島的樹木蒼鬱有很大的差別，主因是氣候的關係。台灣年雨量約兩千五百公厘，但澎湖只有約一千公厘左右，降雨季也分布不均，強勁的[東北季風是最主要的氣候特徵](../Page/東北季風.md "wikilink")，當通過台灣與大陸之間的管狀地形風力便會加速，讓澎湖籠罩在冷冽的季風下，每年約有三分之一時間處於大風下，尤其是十月到三月之間，平均風速約每秒八公尺；最大風速每秒可達每秒二十公尺以上。季風不止影響農林植物的生長，也對土壤有很大的影響。因此，本地的樹木高不過附近的建築物，是因為風切壓住它的高度。另因島嶼位於海上，土壤鹽份偏高，不利於植物的生長。

### 潮汐

海水每天都有兩次規律性的漲退潮稱為[潮汐](../Page/潮汐.md "wikilink")，外海潮差約一公尺；但內海因受地形影響，潮差可達三公尺左右，因時因地潮差也不一樣。潮汐侵蝕地形能力並不大，可是卻可以日積月累搬運漂砂。因此潮汐對於[海埔新生地](../Page/海埔新生地.md "wikilink")、[沙洲](../Page/沙洲.md "wikilink")、[海灘跟](../Page/海灘.md "wikilink")[沙嘴等海岸地形有很大的影響力](../Page/沙嘴.md "wikilink")。

## 澎湖寺廟

澎湖因為是個海島，居民多半從事[漁業生活](../Page/漁業.md "wikilink")，面對著變化無常的海域及澎湖自然環境影響，只好依靠神明寄託心靈，於是澎湖民間宗教開始蓬勃發展。
\=== 統計分析 === 下表是根據民政局2007年澎湖寺廟教堂數統計
{| border="1" |- | 宗教|| [佛教](../Page/佛教.md "wikilink") ||
[道教](../Page/道教.md "wikilink") ||
[天主教](../Page/天主教.md "wikilink") ||
[基督新教](../Page/基督新教.md "wikilink")||[一貫道](../Page/一貫道.md "wikilink")||合計
|- | 寺廟教堂數(座) || 38 || 140 || 5 || 13 || 2 || 198 |- |}

縣政府統計[馬公市面積為](../Page/馬公市.md "wikilink")33.99平方公里、共計34個里、人口54951人、有65座廟宇，平均每公里約有2座寺廟、平均每平方公里約有27座。跟台北市相比，台北市面積271.8平方公里、有208座廟，平均每平方公里約有1座廟。由此可看出澎湖的廟宇密度是臺北市廟宇密度的20幾倍，更可說澎湖廟宇密度是全臺之冠。

下表是澎湖[民間信仰主神統計](../Page/臺灣民間信仰.md "wikilink")，由於民間信仰主神種類繁多，故以廟數前十名作為代表。
{| border="1" |- | 神明
名稱|| [王爺](../Page/王爺千歲信仰.md "wikilink") ||
[觀音菩薩](../Page/觀音菩薩.md "wikilink") ||
[文衡聖帝](../Page/文衡聖帝.md "wikilink") ||
[福德正神](../Page/福德正神.md "wikilink")||[關聖帝君](../Page/關聖帝君.md "wikilink")||[玄天上帝](../Page/玄天上帝.md "wikilink")||[保生大帝](../Page/保生大帝.md "wikilink")||[天上聖母](../Page/天上聖母.md "wikilink")||
[中壇元帥](../Page/中壇元帥.md "wikilink") || 合計 |- | 小計 || 32 || 13 || 11 || 10
|| 6 || 6 || 6 || 7 || 4 || 95 |- |}

澎湖將近200間寺廟，其中奉祀[王爺神共](../Page/王爺神.md "wikilink")32座，其次是[觀音菩薩](../Page/觀音菩薩.md "wikilink")、[文衡聖帝](../Page/文衡聖帝.md "wikilink")（[關聖帝君的](../Page/關聖帝君.md "wikilink")[儒家稱謂](../Page/儒家.md "wikilink")）等；[福德正神](../Page/福德正神.md "wikilink")（[土地公](../Page/土地公.md "wikilink")）、[玄天上帝](../Page/玄天上帝.md "wikilink")、[保生大帝](../Page/保生大帝.md "wikilink")、[天上聖母](../Page/天上聖母.md "wikilink")（媽祖）、[中壇元帥各具信眾](../Page/中壇元帥.md "wikilink")。若與其它主神相比下，王爺廟數約為其它主神的2到3倍之多，比[海神](../Page/海神.md "wikilink")[媽祖的廟宇多](../Page/媽祖.md "wikilink")10幾倍，從中可知澎湖王爺神是本地的重要信仰之一。主神為王爺的廟宇每年都會有「請王」、「送王」的祭典，不僅是代臺灣具有特色的王船祭，也成為澎湖獨特民間宗教特色之一。

### 建築特色

澎湖寺廟不但是村民信仰中心，更具有社會、經濟和文化的功能。因此澎湖居民對寺廟的建築相當重視，甚至不惜巨額興建，要求美侖美奐，以代表對神明的尊敬。

  - 建築材料：寺廟建築材料大致上可分為二種，一種是木料，用來作建築的主體，但由於澎湖本身並無森林，所以木材主要是進口而來。第二種是石材，主要是以花崗石為主，目的是防止石雕[風化](../Page/風化.md "wikilink")、侵蝕，也有部分寺廟用於牆身，對抗強烈的澎湖季風。另有當地石材－[硓{{僻字](../Page/硓𥑮石.md "wikilink")（澎湖的[珊瑚礁所形成的石材](../Page/珊瑚礁.md "wikilink")，一說應為硓石\[10\]）。
  - 空間隔局：澎湖[寺廟是以](../Page/寺廟.md "wikilink")[合院建築為主](../Page/合院.md "wikilink")，廟宇屋脊背較大，是以防風來考量。
  - 泥塑：泥塑是將三合土摻入棉花，再塑出各種形象的藝術，也會加入[貝殼](../Page/貝殼.md "wikilink")，再嵌上瓷片稱之為剪花或剪黏。
  - 花窗：寺廟正面花窗，一般寺廟多用花鳥或螭虎圖案等吉祥圖案為主，不過澎湖常以[洗石子技法為之](../Page/洗石子.md "wikilink")，或加色灰泥塑而成，有地域的特色。
  - 彩繪：澎湖寺廟深受[陰陽五行的影響](../Page/陰陽五行.md "wikilink")，特別重視彩繪。不少寺廟樑柱和柱子都有使用「擂金畫」。「擂金畫」主要是將金箔磨成粉末狀，趁底漆未乾時用筆沾金箔粉敷上之畫法，主要用在木雕上，顏色豐富。
  - 柱子：功能是用來支撐整座廟，有圓柱、方柱、六角柱和八角柱，大多都有雕龍在柱身，身體盤繞柱子一圈，並用爪子捉珠，有的柱身更有豊富的蝦兵蟹將、和封神榜或八仙神話人物配置。

## 自然生態

澎湖群島中設有[澎湖南方四島國家公園及](../Page/澎湖南方四島國家公園.md "wikilink")[澎湖玄武岩自然保留區](../Page/澎湖玄武岩自然保留區.md "wikilink")；另外還設有[貓嶼海鳥保護區與](../Page/貓嶼海鳥保護區.md "wikilink")[望安島綠蠵龜產卵棲地保護區等兩個野生動物保護區](../Page/望安島綠蠵龜產卵棲地保護區.md "wikilink")。另外，[交通部也設置](../Page/中華民國交通部.md "wikilink")**澎湖國家風景區**，範圍涵蓋澎湖群島陸地和向海洋延伸20公尺等深線的海域。最北的島嶼為[目斗嶼](../Page/目斗嶼.md "wikilink")，最南為[七美嶼](../Page/七美嶼.md "wikilink")，極東為[查母嶼](../Page/查母嶼.md "wikilink")，最西為[花嶼](../Page/花嶼.md "wikilink")。

## 旅遊

[寂寞星球出版社推薦](../Page/寂寞星球.md "wikilink")2011年全球十大最佳世外桃源島嶼時將澎湖群島列名其中\[11\]\[12\]。

### 自然景觀

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>玄武岩</p>
<ul>
<li>蒔裡海水浴場（蒔裡沙灘）</li>
<li>太武山</li>
<li>龍門鼓浪</li>
<li>青螺沙嘴濕地</li>
<li>虎頭山</li>
<li>菓葉日出</li>
<li>山水沙灘</li>
<li><a href="../Page/風櫃洞.md" title="wikilink">風櫃洞</a></li>
<li><a href="../Page/通樑古榕.md" title="wikilink">通樑古榕</a></li>
<li><a href="../Page/桶盤玄武岩石柱.md" title="wikilink">桶盤玄武岩石柱</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/西嶼牛心山.md" title="wikilink">西嶼牛心山</a></li>
<li><a href="../Page/天台山_(澎湖).md" title="wikilink">天台山</a></li>
<li>蛇頭山</li>
<li><a href="../Page/七美望夫石.md" title="wikilink">七美望夫石</a></li>
<li>奎璧山遊憩區</li>
<li><a href="../Page/小門鯨魚洞.md" title="wikilink">小門鯨魚洞</a></li>
<li><a href="../Page/牛母坪.md" title="wikilink">牛母坪</a></li>
<li><a href="../Page/小臺灣.md" title="wikilink">小臺灣</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 古蹟廟宇

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/澎湖天后宮.md" title="wikilink">澎湖天后宮</a></li>
<li>萬軍井</li>
<li>媽宮古城</li>
<li><a href="../Page/順承門.md" title="wikilink">順承門</a></li>
<li>施公祠</li>
<li><a href="../Page/中央街.md" title="wikilink">中央街</a></li>
<li><a href="../Page/澎湖觀音亭.md" title="wikilink">澎湖觀音亭</a></li>
<li><a href="../Page/二崁陳宅.md" title="wikilink">二崁陳宅</a></li>
<li>中社古厝</li>
<li><a href="../Page/七美人塚.md" title="wikilink">七美人塚</a></li>
<li><a href="../Page/西嶼西臺.md" title="wikilink">西嶼西臺</a></li>
<li><a href="../Page/西嶼東臺.md" title="wikilink">西嶼東臺</a></li>
<li>龍門裡正角日軍登陸紀念碑</li>
</ul></td>
<td><ul>
<li>馬公金龜頭砲臺</li>
<li><a href="../Page/四眼井.md" title="wikilink">四眼井</a></li>
<li><a href="../Page/文澳城隍廟.md" title="wikilink">文澳城隍廟</a></li>
<li><a href="../Page/媽宮城隍廟.md" title="wikilink">媽宮城隍廟</a></li>
<li>西嶼塔公、塔婆</li>
<li>鎖港南北石塔</li>
<li>第一賓館</li>
<li>風櫃尾荷蘭城堡</li>
<li>湖西拱北砲臺</li>
<li><a href="../Page/漁翁島燈塔.md" title="wikilink">漁翁島燈塔</a></li>
<li><a href="../Page/文石書院.md" title="wikilink">文石書院</a>（後改建為孔廟）</li>
</ul></td>
</tr>
</tbody>
</table>

### 公園

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>山水星空公園</li>
<li>觀音亭濱海公園</li>
<li>青灣仙人掌公園</li>
<li><a href="../Page/桶盤地質公園.md" title="wikilink">桶盤地質公園</a></li>
<li><a href="../Page/東衛石雕公園.md" title="wikilink">東衛石雕公園</a></li>
<li><a href="../Page/林投公園.md" title="wikilink">林投公園</a></li>
</ul></td>
<td><ul>
<li>奎壁山地質公園</li>
<li>中屯綠能源公園</li>
<li>岐頭親水公園</li>
<li>赤崁親水公園</li>
<li>望安花宅公園</li>
<li>火燒坪公園</li>
</ul></td>
</tr>
</tbody>
</table>

### 公共設施

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/澎湖開拓館.md" title="wikilink">澎湖開拓館</a></li>
<li><a href="../Page/澎湖跨海大橋.md" title="wikilink">澎湖跨海大橋</a></li>
<li>七美九孔展覽館</li>
<li>生活博物館</li>
<li><a href="../Page/澎湖水族館.md" title="wikilink">澎湖水族館</a></li>
<li>濱海公路</li>
<li>小門地質館</li>
<li>竹灣螃蟹博物館</li>
</ul></td>
<td><ul>
<li>澎湖酒廠</li>
<li>澎湖遊客中心</li>
<li>菊苑</li>
<li>西嶼燈塔</li>
<li><a href="../Page/七美嶼燈塔.md" title="wikilink">七美嶼燈塔</a></li>
<li><a href="../Page/望安綠蠵龜觀光保育中心.md" title="wikilink">望安綠蠵龜觀光保育中心</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 地景景觀

  - [澎湖石滬](../Page/澎湖石滬.md "wikilink")
  - [澎湖菜宅](../Page/澎湖菜宅.md "wikilink")

### 海灘

<table style="width:90%;">
<colgroup>
<col style="width: 28%" />
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>山水沙灘</li>
<li><a href="../Page/蒔裡海水浴場.md" title="wikilink">蒔裡海水浴場</a></li>
<li>青灣情人海沙灘</li>
<li>風櫃無名港沙灘</li>
<li><a href="../Page/隘門沙灘.md" title="wikilink">隘門沙灘</a></li>
<li>林投沙灘</li>
<li>尖山海灘</li>
<li>烏泥沙灘</li>
<li>裡正角沙灘</li>
<li>龍門後灣沙灘</li>
</ul></td>
<td><ul>
<li>菓葉沙灘</li>
<li>奎壁山遊憩區</li>
<li>青螺海灘</li>
<li>城前沙灘</li>
<li>講美沙灘</li>
<li>鎮海沙灘</li>
<li><a href="../Page/吉貝沙尾.md" title="wikilink">吉貝沙尾</a></li>
<li>後寮沙灘</li>
<li>后螺海灘</li>
<li>橫礁沙灘</li>
</ul></td>
<td><ul>
<li>後灣沙灘</li>
<li><a href="../Page/網垵海灘.md" title="wikilink">網垵海灘</a>(大池角海灘)</li>
<li>池西沙灘</li>
<li>赤馬沙灘</li>
<li>內垵遊憩區</li>
<li>戶頭角沙灘</li>
<li>網垵口沙灘</li>
<li>大瀨仔沙灘</li>
<li>長瀨仔沙灘</li>
<li>西安水庫沙灘</li>
<li>天台山沙灘</li>
</ul></td>
</tr>
</tbody>
</table>

## 澎湖特產

<table style="width:90%;">
<colgroup>
<col style="width: 36%" />
<col style="width: 27%" />
<col style="width: 27%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="植物">植物</h3>
<ul>
<li><a href="../Page/稜角絲瓜.md" title="wikilink">稜角絲瓜</a></li>
<li><a href="../Page/嘉寶瓜.md" title="wikilink">嘉寶瓜</a></li>
<li><a href="../Page/仙人掌.md" title="wikilink">仙人掌</a></li>
<li><a href="../Page/天人菊.md" title="wikilink">天人菊</a></li>
<li><a href="../Page/馬鞍藤.md" title="wikilink">馬鞍藤</a></li>
<li><a href="../Page/風茹草.md" title="wikilink">風茹草</a></li>
</ul></td>
<td><h3 id="海產">海產</h3>
<ul>
<li><a href="../Page/海鱺.md" title="wikilink">海鱺</a></li>
<li><a href="../Page/玳瑁石斑.md" title="wikilink">玳瑁石斑</a></li>
<li><a href="../Page/丁香魚.md" title="wikilink">丁香魚</a></li>
<li><a href="../Page/鞍帶石斑.md" title="wikilink">鞍帶石斑</a></li>
<li><a href="../Page/马鲛鱼.md" title="wikilink">{{僻字</a></li>
</ul></td>
<td><h3 id="特色美食">特色美食</h3>
<dl>
<dt>甜點類</dt>

</dl>
<ul>
<li><a href="../Page/黑糖糕.md" title="wikilink">黑糖糕</a></li>
<li><a href="../Page/冬瓜糕.md" title="wikilink">冬瓜糕</a></li>
<li><a href="../Page/冰心糕.md" title="wikilink">冰心糕</a></li>
<li><a href="../Page/海苔酥.md" title="wikilink">海苔酥</a></li>
<li><a href="../Page/花生酥.md" title="wikilink">花生酥</a></li>
<li>仙人掌冰</li>
</ul>
<dl>
<dt>海產類</dt>

</dl>
<ul>
<li><a href="../Page/小管槍烏賊.md" title="wikilink">小管</a></li>
<li><a href="../Page/南瓜.md" title="wikilink">金瓜</a><a href="../Page/米粉.md" title="wikilink">米粉</a></li>
<li><a href="../Page/蛤蜊.md" title="wikilink">蛤蜊炒</a><a href="../Page/十角絲瓜.md" title="wikilink">絲瓜</a></li>
<li><a href="../Page/海菜.md" title="wikilink">海菜</a><a href="../Page/魚丸.md" title="wikilink">魚丸湯</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 交通

### 澎湖群島往來台灣的交通情形

澎湖群島與[台灣島由於被](../Page/台灣島.md "wikilink")[台灣海峽所相隔](../Page/台灣海峽.md "wikilink")，因此往來台灣都必須依賴航空、海運，其中以航空運輸最為頻繁。航空運輸一天最多可達到108班次（以來回算）；海上運輸一天最多可達到25班次（以來回算）。

#### 航空交通

台灣地區往澎湖群島的航空站有[台北](../Page/台北.md "wikilink")[松山機場](../Page/松山機場.md "wikilink")、[台中清泉崗機場](../Page/台中清泉崗機場.md "wikilink")、[台南機場](../Page/台南機場.md "wikilink")、[嘉義機場](../Page/嘉義機場.md "wikilink")、[高雄國際機場](../Page/高雄國際機場.md "wikilink")、[金門機場](../Page/金門機場.md "wikilink")；而澎湖群島往其他地方的航空站有[澎湖機場](../Page/澎湖機場.md "wikilink")、[七美機場](../Page/七美機場.md "wikilink")、[望安機場](../Page/望安機場.md "wikilink")。有航行台灣往來澎湖群島航線的航空公司有遠東、立榮、華信、德安。
下表總和了目前航空的情形

|       |          |      |      |
| ----- | -------- | ---- | ---- |
| 航線    | 航空公司     | 航行時間 | 往返班次 |
| 澎湖至台北 | 立榮、遠東、華信 | 50分鐘 | 44班  |
| 澎湖至高雄 | 立榮、遠東、華信 | 30分鐘 | 34班  |
| 澎湖至台中 | 立榮、華信、遠東 | 40分鐘 | 24班  |
| 澎湖至嘉義 | 立榮       | 30分鐘 | 2班   |
| 澎湖至台南 | 立榮       | 30分鐘 | 8班   |
| 七美至高雄 | 德安       | 35分鐘 | 4班   |
| 望安至高雄 | 德安       | 40分鐘 | 2班   |
|       |          |      |      |

#### 海運交通

澎湖群島往來台灣的船隻有[台華輪](../Page/台華輪.md "wikilink")、遠翔公主號、南海之星、今一之星、滿天星、凱旋3號、今日之星、金星3號，其航線有馬公至高雄、七美至高雄、馬公至嘉義（布袋）、馬公至台南（安平）。
下表總和了目前海運的情形
{| border="1" |- | 航線 || 輪船 || 航行時間 || 往返班次 || 承載人數 |- | 馬公至高雄 || 台華輪 ||
日航：4小時30分
夜航：6小時30分 || 3班 || 1150人 |- | 七美至高雄 || 南海之星 || 2小時40分 || 1班 || 197人 |- |
馬公至嘉義 || 今一之星 || 1小時20分 || 2班 || 317人 |- | 馬公至嘉義 || 滿天星 || 1小時20分 || 4班
|| 355人 |- | 馬公至台南 || 凱旋3號 || 2小時 || 3班 || 346人 |- | 馬公至台南 || 今日之星 ||
2小時 || 2班 || 699人 |- | 馬公至台南 || 金星3號 || 2小時 || 6班 || 302人 |}

### 澎湖本島往來澎湖離島的交通情形

前往澎湖離島主要分為：
\* 東海離島（[員貝嶼](../Page/員貝嶼.md "wikilink")、[鳥嶼](../Page/鳥嶼.md "wikilink")）

  - 南海離島（[桶盤嶼](../Page/桶盤嶼.md "wikilink")、[虎井嶼](../Page/虎井嶼.md "wikilink")、[望安島](../Page/望安島.md "wikilink")、[七美嶼](../Page/七美嶼.md "wikilink")、[將軍澳嶼](../Page/將軍澳嶼.md "wikilink")）
  - 北海離島（[吉貝嶼](../Page/吉貝嶼.md "wikilink")、目斗嶼）
  - 內海離島（[大倉嶼](../Page/大倉嶼.md "wikilink")）

（有些離島是[生態保護區或無人島](../Page/生態保護區.md "wikilink")，因此無法前往。）
除了馬公本島至望安島和馬公本島至[七美嶼有德安航空往返外](../Page/七美嶼.md "wikilink")，其他島嶼都必須依賴輪船才能往返，而去東海離島必須前往岐頭港口搭船；去南海離島必須前往[南海遊客中心搭船](../Page/南海遊客中心.md "wikilink")；去北海離島必須前往[赤崁遊客中心搭船或後寮遊客中心搭船](../Page/赤崁遊客中心.md "wikilink")；去內海離島必須前往重光港口搭船。前往搭船處均有定期的交通船及不定期的民間快艇可搭乘前往。

### 澎湖群島島上的交通情形

|                             |                                                                                                                                     |
| --------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| 島嶼                          | 島上主要的交通工具                                                                                                                           |
| 澎湖本島（澎湖島、白沙島、漁翁島）、望安島、七美嶼   | [公車](../Page/公車.md "wikilink")、汽[機車](../Page/機車.md "wikilink")、[計程車](../Page/計程車.md "wikilink")、[腳踏車](../Page/腳踏車.md "wikilink")、徒步 |
| 大倉嶼、員貝嶼、鳥嶼、吉貝嶼、桶盤嶼、虎井嶼、將軍澳嶼 | 機車、腳踏車、徒步                                                                                                                           |

## 相關條目

  - [交通部觀光局](../Page/交通部觀光局.md "wikilink")
  - [東北角暨宜蘭海岸國家風景區](../Page/東北角暨宜蘭海岸國家風景區.md "wikilink")
  - [東部海岸國家風景區](../Page/東部海岸國家風景區.md "wikilink")
  - [大鵬灣國家風景區](../Page/大鵬灣國家風景區.md "wikilink")
  - [花東縱谷國家風景區](../Page/花東縱谷國家風景區.md "wikilink")
  - [馬祖國家風景區](../Page/馬祖國家風景區.md "wikilink")
  - [日月潭國家風景區](../Page/日月潭國家風景區.md "wikilink")
  - [參山國家風景區](../Page/參山國家風景區.md "wikilink")
  - [阿里山國家風景區](../Page/阿里山國家風景區.md "wikilink")
  - [茂林國家風景區](../Page/茂林國家風景區.md "wikilink")
  - [北海岸及觀音山國家風景區](../Page/北海岸及觀音山國家風景區.md "wikilink")
  - [雲嘉南濱海國家風景區](../Page/雲嘉南濱海國家風景區.md "wikilink")
  - [西拉雅國家風景區](../Page/西拉雅國家風景區.md "wikilink")

## 參考資料

## 外部連結

  - [澎湖縣政府](http://www.penghu.gov.tw/)

{{-}}

[澎湖群島](../Category/澎湖群島.md "wikilink")
[Category:交通部觀光局](../Category/交通部觀光局.md "wikilink")

1.  《重修臺灣省通志》，卷二土地志勝蹟篇，第十五至十六頁
2.  國家文化總會，2009，《走讀臺灣：澎湖縣》，頁10，台北市。網路版：[走讀臺灣-澎湖縣](http://tw.taiwanebooks.com/files/ebook/1907261694537f12db4c534/_SWF_Window.html)，2016年2月8日檢索
3.  [臧振華](../Page/臧振華.md "wikilink")，1989，〈地下出土的澎湖古史〉，《澎湖開拓史：西台古堡建堡暨媽宮城建城一百週年學術研討會實錄》，頁63-102，澎湖：澎湖縣立文化中心。
4.  方豪，1994，《台灣早期史綱》，頁33-34，台北：學生書局。ISBN：957-15-0631-1
5.  《[明史](../Page/明史.md "wikilink") 朱纨传》：初，明祖定制，片板不许入海
6.  Blussé, Leonard (1994). "Retribution and Remorse: The Interaction
    between the Administration and the Protestant Mission in Early
    Colonial Formosa". In Prakash, Gyan. After Colonialism: Imperial
    Histories and Postcolonial Displacement. Princeton University Press.
    ISBN 978-0691037424.
7.  村上直次郎著，許賢瑤譯，2001，〈澎湖島上的荷蘭人〉，《荷蘭時代台灣史論文集》，頁14。宜蘭縣宜蘭市：佛光人文社會學院。
8.  Garnot, L'expédition française de Formose, 1884–1885 (Paris, 1894)
    P193
9.  [夏日追蹤！東方的百慕達](http://www.nownews.com/2010/05/27/11580-2607948.htm)
10.  余樹楨教授寫了一篇《 從硓𥑮石到𥑮硓石》，文中提出珊瑚礁應該稱為𥑮硓石。他認為𥑮硓石的語源可能來自於英語 coral
11. [全球10大秘密島嶼
    澎湖入選第7](http://www.libertytimes.com.tw/2010/new/dec/30/today-t3.htm)
12. [The world’s best secret
    islands](http://www.lonelyplanet.com/australia/travel-tips-and-articles/76176),
    寂寞星球新聞稿