**AZUKI七**（）是日本的[作詞家](../Page/作詞家.md "wikilink")、[鍵盤手](../Page/鍵盤手.md "wikilink")、詩人。女性。血型是AB型。1998年至2013年間，以[GARNET
CROW成員身份為主](../Page/GARNET_CROW.md "wikilink")。

AZUKI七是她的藝名，原名菅谷（）。至於匿稱，就是AZUKI的漢字寫法“小豆”。為[GIZA
studio公司旗下的眾多歌手提供詞作品](../Page/GIZA_studio.md "wikilink")。從1999年開始加入GARNET
CROW，任樂隊鍵盤手，並擔任全作詞。最初是由於要為新節目的試作課題作詞，以此為契機開始了作為作詞家的活動。

## 個人情報

她從4、5歲起直到18、19歲一直在進行鋼琴的練習。到大學時因課題的契機而成為作詞人的活動開始，作品發表沒有時期的持續著。而以AZUKI七的名字第一次出現，是在1999年2月14日發售的[大黒摩季单曲](../Page/大黒摩季.md "wikilink")「」時。曾為[WANDS](../Page/WANDS.md "wikilink")、Field
of View和同為GARNET
CROW成員的[岡本仁志提供作品](../Page/岡本仁志.md "wikilink")，她還發表了個人詩集《80,0》，收錄了80首詩和一些攝影作品。在2003年作為伴奏嘉賓單獨參加了WAG的LIVE。

當自己的身體處於“入睡前狀態”時，AZUKI七開始了她的詞創作。從詞作的傾向說，眾多諸如無常觀、寂寥感、死亡主體的抽象專用語被廣泛地使用，由此構成了一種流轉自如的獨特境界。AZUKI七在一次採訪中說過，她填詞時會故意加入和‘生死’有關的內容來滿足自己。正如她經常戴象徵十字架的飾物，難怪隨意舉出這些專用語中的一個例子，都能發現是引用自[基督教](../Page/基督教.md "wikilink")、[佛教](../Page/佛教.md "wikilink")、以及其他眾多宗教或神話。代表性的單詞有比如[猶太教](../Page/猶太教.md "wikilink")、基督教中的“[耶穌](../Page/耶穌.md "wikilink")”、[舊約聖經的](../Page/舊約聖經.md "wikilink")[民數記](../Page/民數記.md "wikilink")35章中的“[逃城](../Page/逃城.md "wikilink")”、佛教概念中的“[輪回](../Page/輪回.md "wikilink")”、“[三途川](../Page/三途川.md "wikilink")”等等。除此之外，諸如“[諾瓦利斯之門](../Page/諾瓦利斯.md "wikilink")”、“to
be or not to
be”（[哈姆雷特](../Page/哈姆雷特.md "wikilink")……）這種受到文學作品影響的部分在她的歌詞中也很多見。AZUKI七曾說“給別人作詞就好比出門前整理衣服、梳洗打扮，但給GARNET
CROW 寫詞就像在自己家裡，想做什麼就做什麼，非常自在”。

在[GARNET
CROW方面](../Page/GARNET_CROW.md "wikilink")，和成員[中村由利](../Page/中村由利.md "wikilink")（主唱）的交情很好，還替中村取了綽號「ゆりっぺ」。

她個人受《[飄](../Page/飄.md "wikilink")》的女主角[郝思嘉影響很深](../Page/郝思嘉.md "wikilink")。喜歡的音樂家有Peter
Gabriel、Iggy Pop、JohnZorn、The Cure、Jesus Jones和Kula Shaker。

在興趣方面，AZUKI七每星期都要閱讀3－4本書。除此之外，她還有著[滑雪](../Page/滑雪.md "wikilink")、[潛水](../Page/潛水.md "wikilink")、[電視遊戲](../Page/電視遊戲.md "wikilink")、[cosplay](../Page/cosplay.md "wikilink")、[陶藝](../Page/陶藝.md "wikilink")、[黏土](../Page/黏土.md "wikilink")（能夠表現出自身風格的東西）、[釣魚](../Page/釣魚.md "wikilink")、做[意大利菜](../Page/意大利菜.md "wikilink")、[登山](../Page/登山.md "wikilink")、[射箭等等廣泛的興趣](../Page/射箭_\(體育項目\).md "wikilink")。此外，在訪談裡會旁若無人地使用[關西腔說話](../Page/關西腔.md "wikilink")。除此之外，AZUKI七在攝影方面也有著很深的造詣，她的作品廣泛收錄於GARNET
CROW的CD內封和各種書籍之中，被更多的人們所欣賞著。喜歡吃的東西是蔬果類食物，討厭牛奶類食物。

節目「[芸恋リアル](../Page/芸恋リアル.md "wikilink")」全國調查結果公開（2007年6月），有關一般人的評價中，18歲至29歲的1萬人女性裡，AZUKI七在「憧憬的女性完全名次」中排行第202名，18歲至30歲的1萬名男性裡，「想做情人的女性完全名次」中排行第303名。落這2個名次的評價，與歌手[相川七瀨相同](../Page/相川七瀨.md "wikilink")。

## 提供歌词

  - [岡本仁志](../Page/岡本仁志.md "wikilink") 「First fine
    day」「joyride」「Res-no」「Sweet×2 Summer Rain」「Happy
    Trash」「手遅れな再会」「Stray Beast」「翼」「LOST CHILD」「Autumn
    Sky」「静寂の間に」「It's only one」「sphere」

  - [岩田さゆり](../Page/岩田さゆり.md "wikilink")「[空色の猫](../Page/空色の猫_\(岩田さゆり\).md "wikilink")」

  - [上原あずみ](../Page/上原あずみ.md "wikilink")「[青い青いこの地球に](../Page/青い青いこの地球に.md "wikilink")」（共作）「[Special
    Holynight](../Page/Special_Holynight.md "wikilink")」（共作）

  - [大黒摩季](../Page/大黒摩季.md "wikilink")「[太陽の国へ行こうよ
    すぐに〜空飛ぶ夢に乗って〜](../Page/太陽の国へ行こうよ_すぐに〜空飛ぶ夢に乗って〜.md "wikilink")」（共作）

  - [岸本早未](../Page/岸本早未.md "wikilink")「[迷Q\!?-迷宮-MAKE★YOU-](../Page/迷Q!?-迷宮-MAKE★YOU-.md "wikilink")」「OPEN
    YOUR
    HEART」「[愛する君が傍にいれば](../Page/愛する君が傍にいれば.md "wikilink")」「会いたくて」「カマワナイデ」「同じ世界で」「SODA
    POP」「reigning star」「Yes or
    No?」「[みえないストーリー](../Page/みえないストーリー.md "wikilink")」「It's
    so
    easy...」「[風に向かい歩くように](../Page/風に向かい歩くように.md "wikilink")」「素敵な夢見ようね」「[Dessert
    Days](../Page/Dessert_Days.md "wikilink")」「NEVER
    CHANGE」「NON-FICTION・GAME」

  - [Soul Crusaders](../Page/Soul_Crusaders.md "wikilink") 「SAFETY
    LOVE」「感傷」「Free my mind」「Lonesome Tonight
    〜君だけ見つめてる〜」「Resolution」「DO YOU FEEL LIKE
    I LIKE?」「Holiday」

  - [高岡亜衣](../Page/高岡亜衣.md "wikilink")「[To Beat The
    Blues](../Page/fiction.md "wikilink")」「Summer flooding」「Never to
    Return」「夏のある日に」

  - [竹井詩織里](../Page/竹井詩織里.md "wikilink")「[静かなるメロディー](../Page/静かなるメロディー.md "wikilink")」「close
    line」「[君に恋してる](../Page/君に恋してる.md "wikilink")」「夕凪」「[桜色](../Page/桜色_\(竹井詩織里\).md "wikilink")」「[きっともう恋にはならない](../Page/きっともう恋にはならない.md "wikilink")」「[Like
    a little Love](../Page/Like_a_little_Love.md "wikilink")」

  - [DEEN](../Page/DEEN.md "wikilink")「」

  - [FIELD OF
    VIEW](../Page/FIELD_OF_VIEW.md "wikilink")「[CRASH](../Page/CRASH_\(FIELD_OF_VIEW\).md "wikilink")」「」

  - [WAG](../Page/WAG.md "wikilink")「[Free
    Magic](../Page/Free_Magic.md "wikilink")」「」（共作）

  - [WANDS](../Page/WANDS.md "wikilink")「」

  - 「」（此曲由[桃色幸運草第二首單曲](../Page/桃色幸運草.md "wikilink")「」所收錄）

[Category:日本作詞家](../Category/日本作詞家.md "wikilink") [Category:GARNET
CROW](../Category/GARNET_CROW.md "wikilink")
[Category:日本鍵盤手](../Category/日本鍵盤手.md "wikilink")
[Category:Being旗下藝人](../Category/Being旗下藝人.md "wikilink")