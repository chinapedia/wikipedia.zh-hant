[Hieronymus_Bosch_094.jpg](https://zh.wikipedia.org/wiki/File:Hieronymus_Bosch_094.jpg "fig:Hieronymus_Bosch_094.jpg")
**中世紀飲食文化**泛指[欧洲](../Page/欧洲.md "wikilink")[中世纪](../Page/中世纪.md "wikilink")（從5世纪到16世纪）的[飲食習慣](../Page/飲食習慣.md "wikilink")、[烹調方法和](../Page/烹調.md "wikilink")[餐桌禮儀](../Page/餐桌禮儀.md "wikilink")，這套飲食文化跨越近千年，涵蓋嚴寒的[北歐和酷熱的](../Page/北歐.md "wikilink")[地中海](../Page/地中海.md "wikilink")，屢經轉變和更替，成為今日歐洲菜系的基礎。隨著歐洲文明的擴張，其影響見諸世界各地。

從古時起，[麵包已是歐洲的](../Page/麵包.md "wikilink")[主食](../Page/主食.md "wikilink")，[麵條](../Page/面条.md "wikilink")、[麥片亦常見於民間](../Page/麥片.md "wikilink")；常用[调味料有](../Page/调味料.md "wikilink")[濃酸果汁](../Page/酸葡萄汁.md "wikilink")、[酒和](../Page/酒.md "wikilink")[醋](../Page/醋.md "wikilink")，伴以[白糖和](../Page/食糖.md "wikilink")[蜜糖](../Page/蜂蜜.md "wikilink")，令當時的菜式偏向酸甜。一如不少古文明，[肉類同樣是富人之食](../Page/肉类食物.md "wikilink")，而以[豬肉和](../Page/豬肉.md "wikilink")[雞肉最常見](../Page/雞肉.md "wikilink")，[牛因為有農業價值而較少入饌](../Page/牛.md "wikilink")。當時的歐洲人亦喜好[扁桃漿](../Page/扁桃漿.md "wikilink")，在[四旬節更會用來代替動物奶](../Page/大齋期.md "wikilink")。

雖然外界往往視歐洲為一個文化單元，但它幅員廣闊，而且古時交通不便和[保鮮方法不周](../Page/食物保存法.md "wikilink")，各地會因應不同就近的特產而發展出不同的飲品習慣。

當時雖然有「[進口](../Page/進口.md "wikilink")」食品，但價格高昂，只限於[貴族享用](../Page/貴族.md "wikilink")。這種以食物區分貴賤的傳統，在古歐洲十分常見。當時只有貴族有資格「浪費」食物，享用不同[香料](../Page/香辛料.md "wikilink")，而未進身貴族的[富商卻要受制於](../Page/富豪.md "wikilink")[禁奢令](../Page/禁奢令.md "wikilink")，不得濫用食物。社會上亦普遍認為食物質素與進食者有一種神聖和自然的聯繫，下層人民應當吃粗糙、廉價食品，而富人要享用精緻菜餚。以[階級決定進食食物的優劣](../Page/階級.md "wikilink")，這方面与儒家的[名教思想在中國封建時代的用餐](../Page/名教.md "wikilink")（乃至社會和生活的方面）都有著相同的影響。

## 進餐

### 餐數

中世紀鼓吹一天吃兩餐。第一餐的進食在[拉丁文中稱為](../Page/拉丁语.md "wikilink")**，解作「打破[齋戒](../Page/齋戒.md "wikilink")」，一般在[中午舉行](../Page/中午.md "wikilink")，是為一天的主餐。另一餐在晚上進食，餐量較少，在[法文中叫作](../Page/法语.md "wikilink")**，亦即英語**的本字，[汉语如今一般譯成](../Page/汉语.md "wikilink")[晚餐或](../Page/晚餐.md "wikilink")[夜宵](../Page/夜宵.md "wikilink")，但當時主要是指晚餐。

當時歐洲教會主張深宵齋戒，認為起床後不應過早吃飯，以免過早打破深宵禁食的規定，不少神職人員和有教養的貴族都會遵從這規定，把起床後第一餐叫成**。後來[古法語採用原拉丁語的動詞形態](../Page/古法語.md "wikilink")**來造語，轉成了**，。

[英語世界認為](../Page/英語世界.md "wikilink")**是一天最重要的一餐，因為由它衍生的**一字應解作晚餐；英國人再按照「打破[齋戒](../Page/齋戒.md "wikilink")」（）的意思，併湊出早餐**一字，解作早餐。

[法語世界則認為](../Page/法语国家和地区列表.md "wikilink")**是在中午進食，由它衍生的**一詞自然解作「午餐」，而早餐就叫成**，直譯是「打破齋戒前的小餐」。

不過，雖然中世紀教會傳揚[貪食這樣的觀念](../Page/貪食.md "wikilink")，認為吃得過多應感羞恥，但勞動階層出於體力需要，仍保持吃早餐的習慣，而婦孺、長者和病人亦可以吃早餐。當時也有夜宵，稱作
**（[奥克语](../Page/奥克语.md "wikilink")：），但若一邊喝酒一邊吃夜宵，同樣是件可恥的事。縱然如此，當時勞動階層會從僱主身上拿到[零用錢](../Page/零用錢.md "wikilink")，在休息時購買[零食](../Page/零食.md "wikilink")。

### 禮儀

[Banquet_de_Charles_V_le_Sage.jpg](https://zh.wikipedia.org/wiki/File:Banquet_de_Charles_V_le_Sage.jpg "fig:Banquet_de_Charles_V_le_Sage.jpg")
中世紀歐洲人認為獨自用餐是有失禮儀、[自我中心](../Page/自我中心主義.md "wikilink")，並把吃飯看成一種[群體活動](../Page/社交.md "wikilink")。不論是弟兄姊妹和工人，都應該一同進食。13世紀時，[英国](../Page/英国.md "wikilink")[林肯郡](../Page/林肯郡.md "wikilink")[主教](../Page/主教.md "wikilink")[罗伯特·格罗塞特斯特就曾建議](../Page/罗伯特·格罗塞特斯特.md "wikilink")[英王頒令](../Page/英国君主.md "wikilink")，禁止人離開會堂進食、亦不得在私人房間中吃飯，否則是對主人和女士不敬云云。

雖然中世紀時歐洲對某些節日要有一定的[餐桌禮儀](../Page/餐桌禮儀.md "wikilink")，但在[普羅大眾的生活中](../Page/普羅大眾.md "wikilink")，餐桌禮儀的記錄仍乏善可陳，後人只注意沒有前菜、主菜這類起菜規定，也不會用香水洗手。\[1\]

不過，富人用膳卻嚴謹得多。吃每道菜前，他們會向客人遞上淺水盤和[毛巾](../Page/毛巾.md "wikilink")，讓人擦手。盛宴幾乎是男士專享，只有最尊貴的賓客可以攜眷出席，而主人家的太太為了保持端莊、優雅，往往要躲在一旁與隨從吃餐。

當時社會[階級分明](../Page/階級.md "wikilink")、[父權至上](../Page/父權.md "wikilink")，也影響了餐桌禮儀。後輩應侍奉長輩、青年要服侍老人，男人也得照顧女人，以免菜餚弄污女士，男士進餐時更要避免[女性化的舉動](../Page/女性化.md "wikilink")。

### 餐具

[Spätgotische_Keramik_Burg_Vianden.JPG](https://zh.wikipedia.org/wiki/File:Spätgotische_Keramik_Burg_Vianden.JPG "fig:Spätgotische_Keramik_Burg_Vianden.JPG")
當時歐洲人採用與中國人近似的分食習慣，菜餚先放在燉鍋或盤子，置於枱上，各人再以[勺子或徒手取出自己的份量](../Page/匙.md "wikilink")，放在碟上進食，這些碟可以是發硬的[麵包](../Page/麵包.md "wikilink")、[木頭](../Page/木頭.md "wikilink")、又或是[白臘製造](../Page/白臘.md "wikilink")。

不過，低下階層會摒棄[餐碟](../Page/餐碟.md "wikilink")，用[刀割下食物後](../Page/刀.md "wikilink")，直接放進口裡。通常一張刀要數人共用。如果用膳者中有人地位較高、又或與主人家熟稔，才會獨自用一張刀。

今天歐洲菜中常用的[餐叉](../Page/餐叉.md "wikilink")，約在中世紀末期至近代歐洲才開始流行。早年歐洲人對餐叉顯得頗為不安，這在11世紀的拜占庭公主狄奥多拉身上可見一斑。她下嫁給威尼斯總督[多梅尼科·塞尔沃後](../Page/多梅尼科·塞尔沃.md "wikilink")，吃飯時堅持由一位[宦官為自己切食物](../Page/宦官.md "wikilink")，然後以餐叉刺起食物細嚼，令一眾食客感到驚訝。[奧斯提亞主教批評她過於驕矜和優雅](../Page/奧斯提亞.md "wikilink")，舉止陳腐。\[2\]義大利要到14世紀，各階層才開始使用餐叉。

### 宗教

[Beaver_fish_tail.jpg](https://zh.wikipedia.org/wiki/File:Beaver_fish_tail.jpg "fig:Beaver_fish_tail.jpg")，這幅1480年的畫作反映當時一些人認為海狸尾是「魚類」，可以在齋戒日進食。\]\]
[羅馬天主教和](../Page/天主教.md "wikilink")[正教會的曆法對中世紀飲食有著舉足輕重的影響力](../Page/正教會.md "wikilink")。在頗長時間內，各地一年中約三分一時間會實施齋戒，以求令抑制慾念、令靈魂昇華、提醒眾生感懷[基督釘](../Page/基督.md "wikilink")[十字架的苦難](../Page/十字架.md "wikilink")。

齋戒時間一般是每逢週三、週五，一些重要日子如[降臨節](../Page/降臨節.md "wikilink")、[四旬節和](../Page/大齋期.md "wikilink")[领圣体前亦會齋戒](../Page/聖餐禮.md "wikilink")，期間不得進食[肉類](../Page/肉类食物.md "wikilink")、[牛奶](../Page/牛奶.md "wikilink")、[乾酪和](../Page/乾酪.md "wikilink")[蛋](../Page/蛋.md "wikilink")，只可以吃[鱼和](../Page/鱼.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")。\[3\]這並非說魚類特別聖潔，只是想讓人不吃酒肉而達到禁慾的效果，但兒童、老人、朝聖者、工人、無家可居的窮人和乞丐可以豁免規定。而一般人如果違反規定，就只好[苦修贖罪了](../Page/苦修贖罪.md "wikilink")。

所謂上有政策，下有對策，為了避免太多人違禁，有些地方如果魚菜不足，教會會把一些[水禽及](../Page/水禽.md "wikilink")[海洋哺乳類動物當成](../Page/海洋哺乳類動物.md "wikilink")「魚類」，容許當地人食用，例如挪威人就會在齋戒時吃[海雀](../Page/海雀.md "wikilink")，[海貍](../Page/海貍.md "wikilink")、[北極鵝](../Page/北極鵝.md "wikilink")、[鯨](../Page/鯨.md "wikilink")。

[Pietro_Lorenzetti_001.jpg](https://zh.wikipedia.org/wiki/File:Pietro_Lorenzetti_001.jpg "fig:Pietro_Lorenzetti_001.jpg")
不過，即使宗教氣氛濃厚的中世紀，對齋戒也有流露不滿。[修道運動不少成員會借助重新詮譯聖經](../Page/修道運動.md "wikilink")，避過禁食規定，他們指病人可以豁免齋戒，反映這種禁令只適用於主要飯廳；從王室、貴族、學生到普羅大眾，也有抱怨教會利用贖罪刑罪，剝奪他們吃肉的權利。在四旬節時，養[家畜的人也會被訓示小心](../Page/家畜.md "wikilink")[狗隻](../Page/犬.md "wikilink")，皆因牠們被四週的魚骨弄得發瘋，可能會偷吃農民的牲畜。\[4\]不少人也看中分齋戒只是禁止吃肉，沒有明文禁止吃大餐，一大堆「假肉」也應運而生，例如把魚煮得像鹿肉，也可以把[魚籽塞入雞蛋殼](../Page/魚籽.md "wikilink")，當成假蛋，但[拜占庭教會當時是不鼓勵這種仿肉食物](../Page/拜占庭教會.md "wikilink")。\[5\]

### 醫學

中世紀醫學認為每種食物都有一定特性，並按照[古希腊醫學家](../Page/古希腊.md "wikilink")[盖伦的理論](../Page/盖伦.md "wikilink")，把食物大致分為熱寒和濕-{zh-hans:干;zh-hant:乾;}-。這種學說從古代流行，直至17世纪才其他新理論所取代。

當時學者認為，[消化是煮食過程的延續](../Page/消化作用.md "wikilink")，為了把食物在[胃裡](../Page/胃.md "wikilink")「煮好」，更有效吸收營養，進食如煮菜一樣，有一定步驟，例如要先吃容易消化的食物，然後按食物難消化的程度，順次序食用。違反次序時，重質食物會沉入胃的底部，阻塞消化通道。進食不同特性的食物時，也要配上不同食材，加以中和。\[6\]

進餐前，宜先用開胃酒把胃「打開」，在法文中[開胃酒](../Page/開胃酒.md "wikilink")**一詞正是拉丁文「打開」之意，這些酒最好是帶有-{zh-hans:干;zh-hant:乾;}-和熱的特性，而[蜜饯](../Page/蜜饯.md "wikilink")、甜[薑](../Page/薑.md "wikilink")、[苹果等也是開胃小吃](../Page/苹果.md "wikilink")。之後會再吃[生菜](../Page/生菜.md "wikilink")、[捲心菜](../Page/捲心菜.md "wikilink")、[雞肉或](../Page/雞肉.md "wikilink")[肉湯這類較輕質的食品](../Page/肉湯.md "wikilink")，然後才到[豬肉](../Page/豬肉.md "wikilink")、[牛肉等肉重質食物](../Page/牛肉.md "wikilink")。

有「開胃」，自然也有「收胃」。他們飯後多數會吃一種叫**的餐後[甜品](../Page/甜品.md "wikilink")，做法大致是用糖衣包著一些果實，此外飯後也會吃陳年[芝士](../Page/芝士.md "wikilink")，認為有助消化。\[7\]

## 備菜

### 烹調

[烤](../Page/烤.md "wikilink")、[炖](../Page/炖.md "wikilink")、[焗](../Page/焗烤.md "wikilink")，是中世紀時最常見的煮食方法。當時燉鍋相當流行，因為它既能省柴火，也可避免肉汁流失，所以不少中世紀菜式也是肉味濃湯和燉菜。\[8\]這些菜的脂肪遠高於今天的歐洲菜，但當時的人認為白皙膚色和豐腴身形代表高貴和美麗，只有[苦行僧](../Page/苦行僧.md "wikilink")、病人和窮人才會瘦骨嶙峋、肌肉突出，加上當時氣候的寒冷以及人類平均壽命的歲數通常不至於活到累積出太多的[慢性病的年紀](../Page/慢性病.md "wikilink")，所以高熱量飲食並不是問題。\[9\]

[烤爐的建造費龐大](../Page/烤爐.md "wikilink")，除了大戶人家有自己的烤爐外，不少[中世紀公社都會有公共烤爐](../Page/中世紀公社.md "wikilink")，以製造麵包。當時也有一種輕便烤爐，只要放入食物，再用[煤燒](../Page/煤.md "wikilink")，就可以烤製食物，這類工具也會用在小販車上，用來製作烤[餡餅出售](../Page/餡餅.md "wikilink")。此外，不少肉食會直接用火烹調，一直至18世紀，歐洲才出現[爐灶](../Page/爐灶.md "wikilink")。

不過，食物並非可任意烹調，中世紀的人相信每種食物都有固定特質，需要特定煮法配合，例如魚肉又濕又冷，所以最好煮得又熱又乾，這解釋了為何炸魚和烤魚在歐洲如此盛行。而牛肉被認為是-{zh-hans:干;
zh-hant:乾;}-和熱，所以最好用水煲熟；豬肉則熱中帶濕，宜用烤煎方法處理\[10\]。他們亦相信菜式配料應該要近似主菜，例如帶有蘋果味的水果[榅桲會與捲心菜一同入饌](../Page/榅桲.md "wikilink")，[蕪菁與梨子則屬同類](../Page/芜菁.md "wikilink")。\[11\]

### 廚房

[Decameron_1432-cooking_on_spit.jpg](https://zh.wikipedia.org/wiki/File:Decameron_1432-cooking_on_spit.jpg "fig:Decameron_1432-cooking_on_spit.jpg")
當時部分家庭主客廳的中心位置，置有一個開放式爐膛，這除了方便煮食外，也可以令煮食的熱力令居室暖和，即使大富之家也習慣把廚房放到飯廳內。直到中世紀後期，為了防止油煙、氣味流入客廳、降低火灾風險，爐火開始移向大廳一旁，慢慢搬入獨立房間，形成獨立廚房。

煮食工具有[平底鑊](../Page/平底锅.md "wikilink")、湯煲、水煲、以至[對開式鐵心](../Page/對開式鐵心.md "wikilink")，但對貧苦大眾來說這些工具相當昂貴。當時也會用一些鐵枝，叉起不同型狀的食物，還有一些吊鉤，方便把湯煲和大鍋爐提起。

廚師也會用調羹、各式切削刀具、湯勺和磨削，富裕家庭還會用上打磨工具，以調製更為精緻的菜餚。當時的醫學認為，只要食物越精緻，食物配合越好，會有助吸收。其中一種富人食法是把動物起皮、磨肉、混以香料和食材，把碎肉放回原來的動物皮內包好，又或製成形態完全不同的動物。\[12\]

皇室貴族的廚房的分工猶為仔細，廚房工人可以上百人，有司膳總管、麵包師傅、聖餅製作師、調味汁廚師、貯儲食物師、[屠夫](../Page/屠宰.md "wikilink")、切肉員、搾奶工人、男[管家和無數雜役](../Page/管家.md "wikilink")。一些大型廚房每天更要為過百人提供每日兩餐。15世紀時，[薩伏依公爵一名總廚師曾撰寫](../Page/萨伏依王室.md "wikilink")**（法文：“料理真諦”）一書，介紹兩日宴會中要最少處理一千卡柴火和大煤炭，反映中世紀豪門夜宴的規模。\[13\]

### 保存

[Medieval_wine_conservation.jpg](https://zh.wikipedia.org/wiki/File:Medieval_wine_conservation.jpg "fig:Medieval_wine_conservation.jpg")
在19世紀時發明[罐頭前](../Page/罐裝食品.md "wikilink")，食物保存方法在千多年大致不變，最常見的方法是利用熱和風，移走食物中的濕氣，令[微生物因缺水而不能滋生](../Page/微生物.md "wikilink")，可應用在穀類食物和肉類之上。在氣候和暖的地區，食物會放在太陽下曬乾，但北歐的人多會把肉類任由強風吹掃，不少魚乾也是以此法製做。也有人會利用焗爐微溫、酒窖、閣樓等地方把食物弄乾。

[煙醺](../Page/熏.md "wikilink")、[鹽醃](../Page/醃.md "wikilink")、[鹽水等方法亦非常普遍](../Page/鹽水.md "wikilink")，鹽可以脫水製造微生物不易滋長的環境，這除了能快速保存食物，亦會為原來食物加入新味道。當時的歐洲人也會為了減少家畜耗用太多糧食，在冬季時會把它們屠宰，才以煙醺和鹽醃加以保存。而牛油亦會加入5至10%的鹽份，防止腐壞。蔬菜、蛋和魚會放在密封瓶內，加入鹽水和[果汁](../Page/果汁.md "wikilink")、[醋等酸汁液體](../Page/醋.md "wikilink")。他們有時也會把食物以[糖](../Page/糖.md "wikilink")、[蜜糖和](../Page/蜂蜜.md "wikilink")[油脂煮和](../Page/脂類.md "wikilink")，令食物被重重包圍，從現代角度看，這方法可減少[微生物入侵](../Page/微生物.md "wikilink")，避免食物腐壞。另一常見方法是改良[細菌](../Page/细菌.md "wikilink")，以做成[發酵](../Page/发酵.md "wikilink")。例如[穀物](../Page/粮食.md "wikilink")、[葡萄可以](../Page/葡萄.md "wikilink")[釀酒](../Page/釀酒.md "wikilink")，而[奶品可以轉化成](../Page/奶品.md "wikilink")[乳類](../Page/奶制品.md "wikilink")。\[14\]

## 食材

[Baker_punishment.jpg](https://zh.wikipedia.org/wiki/File:Baker_punishment.jpg "fig:Baker_punishment.jpg")

### 穀類

穀類是歐洲人最重要的主食，穀物多數會製成麵包，一些估計指當時歐洲人一天會吃1至1.5公斤的麵包，但穀類有時也會煮成稀粥、乳粥（西班牙文及英文：）和麵條，其中[裸麥](../Page/裸麥.md "wikilink")、[大麥](../Page/大麦.md "wikilink")、[蕎麥](../Page/蕎麥.md "wikilink")、[粟麥](../Page/粟麥.md "wikilink")、[燕麥最為盛行](../Page/燕麦.md "wikilink")。當時意大利北部有種植[大米](../Page/稻.md "wikilink")，但中世紀時期大米價格仍偏高。鬧饑荒時，他們會用[栗子](../Page/栗子.md "wikilink")、乾[豆莢](../Page/荚果.md "wikilink")、[橡樹果和](../Page/橡子.md "wikilink")[蕨類植物代替穀物](../Page/蕨类植物.md "wikilink")。

在芸芸縠物中，中世紀人認為小麥是眾麥之首，營養最高，售價亦最貴。貴族吃的麵包是用精製麵粉製作，而下等人只能吃一些粗糙、乾黑和滿布麵糠的麵包。不分貧賤和大小場合，他們經常會把麵包放到酒、湯和醬汁之內，混和食用，這種濕吃麵包在英文中稱為**，而在西班牙文中稱為**。
[Bakermiddleages.jpg](https://zh.wikipedia.org/wiki/File:Bakermiddleages.jpg "fig:Bakermiddleages.jpg")

而乳粥的做法是把麥片壓碎、煮滾，加入牛奶、蛋或肉湯烹調，亦可放入[扁桃](../Page/扁桃.md "wikilink")、[葡萄乾](../Page/葡萄乾.md "wikilink")、糖、橙味水等調味，這做法在今天的歐洲仍頗為流行。\[15\]這類乳粥當時也會供給病人食用。

焗批是另一種廣受歡迎的穀類產品，早期焗批的底皮是不會食用，直至14世紀才出現現代焗批的樣式，常混以肉、蛋、菜、以至水果。而一些油炸麵糰如[多拿滋](../Page/甜甜圈.md "wikilink")（，甜甜圈，冬甩）、炸粉糰（）亦相當普遍。中世紀後期，[餅乾亦開始出現](../Page/餅乾.md "wikilink")。

麵包在社會上舉足輕重的地位，也連帶周邊行業出現附帶規管。其中，中世紀的行會最初就是由麵包師傅組成，而各地亦有連串法令，穩定麵包價格。1266年，英國就曾訂立《麵包和麥酒法令》（**）指定每便士必須購得多少麵包、其重量和小麥與麵包的關係\[16\]。麵包師傅的利潤同樣有保證，當時英國麵包行會為了提高價值限制，於是展開遊說，成功把燈油火蠟、妻子家用、住屋和家犬的開銷，都列入麵包的成本之內，直到19世紀這類法令才在[倫敦廢止](../Page/伦敦.md "wikilink")。

若有人借麵包行騙，在當時更屬嚴重罪行。麵包師傅要是騙秤，又或用次級材料製作上等麵包，會被重重罰款。業者為求自保，每當有人買一打12個的麵包，他們寧可給予一打13個，稱之為「麵包師傅的一打」（Baker's
dozen）。\[17\]

麵包的用途不止在食用，他們用餐時由於共用一張刀，當把刀具傳給另一人使用前，往往會把麵包當作抹布，把刀具清潔乾淨，有時更會把麵包當成隔熱布，而以舊麵包當作餐碟的做法亦相當普遍。這些另類麵包用途，今天仍見諸於歐洲餐桌上。\[18\]

### 海鮮

[Tacuinum_Sanitatis-fishing_lamprey.jpg](https://zh.wikipedia.org/wiki/File:Tacuinum_Sanitatis-fishing_lamprey.jpg "fig:Tacuinum_Sanitatis-fishing_lamprey.jpg")
中世紀的[齋戒時](../Page/齋戒.md "wikilink")，家家戶戶都不得吃紅肉，[海鮮由此成為最受常見的替代品](../Page/海鮮.md "wikilink")，而在沿海地區，海鮮更是每日必備，但由於運費昂貴，魚類在[中歐等內陸地帶亦相當昂貴](../Page/中欧.md "wikilink")。

[鯡魚和](../Page/鯡魚.md "wikilink")[鱈魚是最主要的魚獲](../Page/鳕鱼.md "wikilink")，常見於[大西洋和](../Page/大西洋.md "wikilink")[波羅的海](../Page/波罗的海.md "wikilink")，其中鯡魚更曾對[北歐經濟扮演舉足輕重的角色](../Page/北歐.md "wikilink")，亦是德國北部行會[漢薩同盟中極為重要的商品](../Page/汉萨同盟.md "wikilink")，一些醃鯡魚更會由北海運到遙遠的[君士坦丁堡出售](../Page/君士坦丁堡.md "wikilink")。這些魚獲除了新鮮烹調外，也會被鹽醃、風乾，有時也會煙醺。其他常見魚類有[梭子魚](../Page/梭子魚.md "wikilink")、[鯉魚](../Page/鯉.md "wikilink")、[鱸魚](../Page/鱸魚.md "wikilink")、[七鰓鰻和](../Page/七鰓鰻.md "wikilink")[鮭魚等](../Page/鮭魚.md "wikilink")。\[19\]而當時魚類泛指所有海上生物，包括鯨魚、海豚、海貍，有些地方還會把[白額黑雁也包括進來](../Page/白颊黑雁.md "wikilink")。

不少[軟體動物也頗為常見](../Page/软体动物.md "wikilink")，其中[生蠔](../Page/牡蠣.md "wikilink")、[青口](../Page/青口.md "wikilink")、[帶子是沿河或沿海居民經常的普遍食糧](../Page/扇贝.md "wikilink")，[小龍蝦則是禁食節日中深受喜歡的食物](../Page/小龍蝦.md "wikilink")。

### 肉類

[Medieval_pig_slaughter.jpg](https://zh.wikipedia.org/wiki/File:Medieval_pig_slaughter.jpg "fig:Medieval_pig_slaughter.jpg")
今天歐洲人對動物[內臟較為抗拒](../Page/內臟.md "wikilink")，但中世紀人幾乎會吃盡動物每一部分。[耳](../Page/耳.md "wikilink")、[舌](../Page/舌.md "wikilink")、[尾](../Page/尾.md "wikilink")、[嘴巴以至](../Page/口腔.md "wikilink")[子宮](../Page/子宫.md "wikilink")，全部會入饌；而[腸](../Page/腸.md "wikilink")、[膀胱](../Page/膀胱.md "wikilink")、[胃會用來做腸衣](../Page/胃.md "wikilink")，又或[復活節時的人造巨蛋](../Page/復活節.md "wikilink")。

芸芸肉類中，豬肉最為流行，因為飼料相宜，而且養豬毋須太多打理。當時家豬會放到戶外四處走動，即使城鎮的道路上亦常有家豬的踪迹，牠們吃的是飯菜渣，[乳豬更是非常受歡迎的美食](../Page/燒乳豬.md "wikilink")。

羊肉是另一種常見肉類，在織毛業盛行的城鎮，羊肉尤為普遍。但牛隻能夠提供[牛乳](../Page/牛奶.md "wikilink")、而且飼養牛隻需要牧場和大量食物，當時只有一些年老或不事生產的牛才會被宰殺食用。除了這些現代人常見的肉類外，[刺猬](../Page/刺猬.md "wikilink")（Hedgehog）和[箭豬](../Page/豪豬.md "wikilink")（porcupine），在中世紀末期的食譜中亦偶有提及。這兩種動物的英文名字都帶有「豬」的意思，反映當時人對牠們的看法。\[20\]

在禽鳥方面，中世紀人幾乎會吃所有狩獵而得的雀鳥，當中有[天鵝](../Page/天鹅.md "wikilink")、[孔雀](../Page/孔雀.md "wikilink")、[鵪鶉](../Page/鹌鹑.md "wikilink")、[山鶁](../Page/山鶁.md "wikilink")、[鸛鳥](../Page/鹳科.md "wikilink")、[丹頂鶴](../Page/丹顶鹤.md "wikilink")、[百靈鳥](../Page/百靈鳥.md "wikilink")。其中，天鵝和孔雀會被馴養，供上層社會所食，吃禽鳥也不一定是為了肉質，有時只為了看牠們漂亮的外貌，並用牠們做成伴菜。[鵝和](../Page/鹅.md "wikilink")[鴨也被當時的人飼養](../Page/鸭.md "wikilink")，但雞隻才是所有家禽中最多人食用，其角色猶如家畜中的豬肉。　

一如不少古代國家，肉類遠遠比植物昂貴，有些研究認為肉類大致較麵包貴4倍，而魚類更可以貴上16倍。直至14世紀歐洲爆發大規模的[黑死病](../Page/黑死病.md "wikilink")，估計約總人口三分之一左右的歐洲人死亡，令大量農地荒廢，可以種更多飼料供家畜食用，以及人工薪資不斷上揚，也推高了購買力，結果肉類、蛋和奶也開始在民間流行起來，不論貧富都可以享用。\[21\]

### 蔬果

[Tacuinum_Sanitatis-cabbage_harvest.jpg](https://zh.wikipedia.org/wiki/File:Tacuinum_Sanitatis-cabbage_harvest.jpg "fig:Tacuinum_Sanitatis-cabbage_harvest.jpg")
中世紀視蔬菜為低下食材，雖然不少人以此為生，甚至是人類生命中不可或缺，但食譜卻很少記載純蔬菜製作的菜式，多數只用來當作伴菜。較例外是法國北部興起的菜肉濃湯（法文：），這種湯以蔬菜和肉類餚成，質感相當厚，但濃湯在當時也不能算作主菜。

常見蔬菜有[捲心菜](../Page/捲心菜.md "wikilink")、[甜菜](../Page/甜菜.md "wikilink")、[洋蔥](../Page/洋蔥.md "wikilink")、[蒜](../Page/蒜.md "wikilink")、[胡蘿蔔](../Page/胡蘿蔔.md "wikilink")。以胡蘿蔔為例，一種是紅紫色、一種是較次等的黃綠色品種。至於今人常吃的橙色胡蘿蔔要到17世紀才出現。不少人亦會吃[豆莢吸取](../Page/荚果.md "wikilink")[蛋白質](../Page/蛋白质.md "wikilink")，[德國酸菜也頗為流行](../Page/德國酸菜.md "wikilink")，當時的農民一天可以吃上三至四次。\[22\]

水果是另一種廣為採用的食材，中世紀人秉承[古羅馬和](../Page/古罗马.md "wikilink")[古希臘的](../Page/古希腊.md "wikilink")[體液學說](../Page/體液學說.md "wikilink")，認為水果有濕氣，不宜生吃，但生吃、風乾和醃製同樣普遍。由於白糖和蜜糖售價不菲，生果也會用作調味，點綴肉類菜式，其中南歐較多用[檸檬](../Page/柠檬.md "wikilink")、[柚子](../Page/柚子.md "wikilink")、[苦枳](../Page/苦橙.md "wikilink")（）、[石榴](../Page/石榴.md "wikilink")、[葡萄](../Page/葡萄.md "wikilink")，北歐多會用蘋果、[梨子](../Page/梨.md "wikilink")、[杨梅和](../Page/杨梅.md "wikilink")[草莓](../Page/草莓.md "wikilink")。[棗椰樹果實和](../Page/海枣.md "wikilink")[無花果在歐洲各地流行](../Page/無花果.md "wikilink")，但在北歐因運費緣故，售價會較高。

不過，不少現在歐洲菜常見的蔬菜，中世紀時並不存在。其中[马铃薯](../Page/马铃薯.md "wikilink")、[紅豆](../Page/紅豆.md "wikilink")、[可可](../Page/可可.md "wikilink")、[香莢蘭](../Page/香莢蘭.md "wikilink")、[番茄](../Page/番茄.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")、[玉米都是在](../Page/玉米.md "wikilink")15世纪[發現新大陸後](../Page/地理大发现.md "wikilink")，才開始從[美洲引入歐洲](../Page/美洲.md "wikilink")，並且經歷一段漫長時間和爭議，才慢慢融入歐洲菜系中。\[23\]

### 香料

[Le_livre_des_merveilles_de_Marco_Polo-pepper.jpg](https://zh.wikipedia.org/wiki/File:Le_livre_des_merveilles_de_Marco_Polo-pepper.jpg "fig:Le_livre_des_merveilles_de_Marco_Polo-pepper.jpg")遊記曾加入一幅插圖，描述收割黑椒的情況。\]\]
歐洲原生的香料，包括[藥用鼠尾草](../Page/藥用鼠尾草.md "wikilink")、[芥末](../Page/芥末.md "wikilink")、[香芹](../Page/香芹.md "wikilink")、[香菜](../Page/香菜.md "wikilink")、[薄荷](../Page/薄荷.md "wikilink")、[蒔蘿和](../Page/蒔蘿.md "wikilink")[茴香](../Page/小茴香.md "wikilink")。這些原生香料味道始終不夠豐足，多數的效果較接近[色素](../Page/色素.md "wikilink")。

到了大航海時代，進口[香料一直是奢昂的材料](../Page/香料.md "wikilink")，比方[黑椒](../Page/黑胡椒.md "wikilink")、[藏紅花](../Page/番紅花.md "wikilink")、[肉桂](../Page/桂皮.md "wikilink")、[桂皮](../Page/桂皮.md "wikilink")、[孜然](../Page/孜然.md "wikilink")、[肉豆蔻](../Page/肉豆蔻.md "wikilink")、[薑和](../Page/薑.md "wikilink")[丁香都要從外地進口](../Page/丁香.md "wikilink")，令它們價格居高不下。有估計指出該時期，西歐每年進口多達1000噸黑椒和1000噸不同香料，其貨值足以為150萬人購買日常穀物，其中又以黑椒和藏紅花最為矜貴。

當時也會用一種叫[非洲豆蔻](../Page/天堂椒.md "wikilink")（）的香料代替黑椒，在法國北部尤其常見，此外，[長椒](../Page/長椒.md "wikilink")（）、肉豆蔻種子中的[核仁](../Page/核仁.md "wikilink")、[甘松香](../Page/甘松香.md "wikilink")、[高良姜](../Page/高良姜.md "wikilink")、[蓽澄茄也常會入饌](../Page/荜澄茄.md "wikilink")。當時因為白糖也被視為香料之一，而且售價高昂。\[24\]

## 飲料

[Monk_sneaking_a_drink.jpg](https://zh.wikipedia.org/wiki/File:Monk_sneaking_a_drink.jpg "fig:Monk_sneaking_a_drink.jpg")
中世紀人認為[酒精比清水更有營養](../Page/乙醇.md "wikilink")，亦有助消化，而且酒精較難腐壞，而潔淨食水在古歐洲亦非必然可得，這令酒精在當時大為流行，其中[地中海北部和大部分法國等盛產葡萄的地方](../Page/地中海北部.md "wikilink")，更視酒精為每日必備飲料。而在[北歐以](../Page/北歐.md "wikilink")[麥酒和](../Page/麥酒.md "wikilink")[啤酒較常見](../Page/啤酒.md "wikilink")。

當時歐洲也有[蜂蜜酒和用](../Page/蜂蜜酒.md "wikilink")[駱駝奶或](../Page/駱駝奶.md "wikilink")[馬乳製成的](../Page/馬乳.md "wikilink")[酸奶酒精飲料](../Page/酸奶.md "wikilink")（稱為*[马奶酒](../Page/马奶酒.md "wikilink")*），但各地飲用習慣有所不同。有些地方視它們為藥水，由古時醫生處方\[25\]。但[斯拉夫人卻視蜂蜜酒為精品](../Page/斯拉夫人.md "wikilink")，在簽定一些重要條約時，還會互贈蜜酒，以示友好。在[波蘭等地](../Page/波兰.md "wikilink")，蜜酒與外來進口的香料和葡酒同樣珍貴。\[26\]

### 葡萄酒

[29-autunno,Taccuino_Sanitatis,_Casanatense_4182..jpg](https://zh.wikipedia.org/wiki/File:29-autunno,Taccuino_Sanitatis,_Casanatense_4182..jpg "fig:29-autunno,Taccuino_Sanitatis,_Casanatense_4182..jpg")
[葡萄酒在中世紀廣為接納](../Page/葡萄酒.md "wikilink")，既是高級飲料，也被視為有營養價值。古希臘名醫[伽林認為葡萄酒本性乾](../Page/蓋侖.md "wikilink")、熱，但酒被喝飲後，其性會轉趨溫和，並斷定葡萄酒有別於濕寒的啤酒和清水，對腸胃更為有益、可幫助製造良好血液、改善心情。

當時的人認為葡萄酒的品質除了因應葡萄品種和釀造年期外，更重要是葡萄被擠壓的次數。所謂擠壓是指把果皮裡的汁放在釀酒桶內，加上壓力，令果皮和果汁都失去糖份，釋放果中的[丹寧酸](../Page/单宁酸.md "wikilink")。

頭一次擠壓出來的酒是為上品，專供[上流社會享用](../Page/上流社會.md "wikilink")；第二及第三次擠壓的酒，質素已大為下降，會留給[農民和](../Page/鄉民.md "wikilink")[工人飲用](../Page/工人.md "wikilink")。社會中最窮的人，又或最虔誠的神職人員，甚至會用稀和了的[黑醋代替酒類](../Page/黑醋.md "wikilink")。

上等紅酒還需要經年發酵，這又需要更多昂貴設備。不少中世紀文獻中都廣泛教人如何避免釀酒時令酒質變壞，其中14世紀的食譜*[Le
Viandier](../Page/Le_Viandier.md "wikilink")*就教人酒桶要時刻向上放，又或把乾白葡萄種子混入酒糟灰，再倒入酒桶內，在現代醫學中，這方法有如天然的殺菌劑，減慢酒精發酵過程。

中世紀人也認為，溫熱的酒對身體最有益。這些熱酒常會加入香料飲用，幾乎歐洲各地都有相關詞彙形容此一酒類，其中英文會叫成**、德文叫**、法文稱為**、波蘭語稱為**，俄羅斯叫作**（）、瑞典則稱為，其製法大同小異，部分歐洲國家今天仍視為禦寒飲品。

此外，葡萄酒中也可以加入薑、小豆蔻、丁香或糖等，變成香料酒。14世紀時，這些調酒香料包會在香料商店出售。\[27\]

### 啤酒

[The_Brewer_designed_and_engraved_in_the_Sixteenth._Century_by_J_Amman.png](https://zh.wikipedia.org/wiki/File:The_Brewer_designed_and_engraved_in_the_Sixteenth._Century_by_J_Amman.png "fig:The_Brewer_designed_and_engraved_in_the_Sixteenth._Century_by_J_Amman.png")

雖然大部分歐洲地區都飲用葡萄酒，但歐洲北部沒有葡萄出產，即使貴族也會飲用啤酒和麥酒，這在今日英國、荷蘭、德國、波蘭和北歐一帶最為常見。當地人不論階層，幾乎每天都會飲啤酒，但受到阿拉伯和地中海的醫學影響，這種酒往往被視為有害身體。

相比南部出產的葡萄酒，啤酒是次級的酒類替代品，而且附帶連串負面評價。1256年，意大利[锡耶納醫生](../Page/锡耶納.md "wikilink")[阿杜班羅甸奴這樣形容啤酒](../Page/Aldobrandino.md "wikilink")：「不論是用燕麥、大麥或小麥釀製，這些酒有損頭腦和胃部，令人[口臭](../Page/口臭.md "wikilink")、[蛀牙](../Page/龋齿.md "wikilink")、令胃部充斥臭味……」但他亦指啤酒有[利尿作用](../Page/利尿.md "wikilink")，而且會令人的皮膚看來更白更滑。當時的人亦相信，喝啤酒會宿醉得更長時間。

雖然啤酒的負面評價不少，但法國北部和意大利中部也有喝啤酒的習慣，估計是[诺曼人入侵英國後](../Page/諾曼人.md "wikilink")，把當地的啤酒帶回歐洲，加上英法兩地互相通婚，也令啤酒得以在法國流行。14世紀法國食譜《**》就曾指一種叫**的酒，估計正是英語**（好麥酒）一字。

中世紀初期，啤酒會在修道院和一些家庭中釀造，但到了中期，一些中世紀公社開始取代發展出私人釀酒工業，每間商店往往聘請8至10人協助釀酒，而為了應付競爭，他們會購入新式設備，發明不同秘方，加入特別調味，令酒味更為出眾，並以自家品牌出售啤酒。14世紀時，這些釀酒業傳入荷蘭、[佛蘭德和](../Page/佛蘭德.md "wikilink")[布拉班特](../Page/布拉班特.md "wikilink")，15世紀時輾轉傳入英國。

在今日英國和荷蘭等地，每人一年就可飲用275至300公升的啤酒，幾乎每餐都會飲用，其中早上會飲酒精較少的啤酒，晚上酒精濃度也相應提升。\[28\]

### 蒸餾

雖然古希臘和羅馬也懂得這種方法，但直到12世紀，[阿拉伯人發明](../Page/阿拉伯人.md "wikilink")[琉璃水冷蒸餾器後](../Page/琉璃水冷蒸餾器.md "wikilink")，歐洲才開始大規模以此法製造飲料。中世紀的學者相信蒸餾法能把液體精華提鍊出來，一律把蒸餾液體稱為「」（生命之水）。

早期的蒸餾液體會當作藥品或調味料。例如蒸餾而成的葡萄糖漿混合砂糖和香料後，會拿來治療不同疾病；[玫瑰香味水可以當成](../Page/玫瑰.md "wikilink")[香水](../Page/香水.md "wikilink")、調味料又或典雅的洗手水。蒸餾酒更是中世紀醫學備受推崇的藥品。1309年，西班牙[鍊金術師](../Page/煉金術.md "wikilink")[路化就指常飲可延年益壽](../Page/Arnaldus_de_Villa_Nova.md "wikilink")、鼓舞人心、常保青春。

從13世紀起，一種叫**（德文：家中燒製之意）的[蒸餾酒開始流行](../Page/蒸餾酒.md "wikilink")，成為日後[白蘭地的雛型](../Page/白蘭地.md "wikilink")，但蒸餾酒的酒精濃度相當高，到了15世紀時，各地政府開始規管其銷售和生產。1496年德國紐倫堡就禁止在假日和週日出售烈性白蘭地。\[29\]

在馬鈴薯傳入歐洲後，有了大量的澱粉來源釀造更多的蒸餾酒，使得蒸餾酒變得更為普遍。

### 乳類

[Milk_glass.jpg](https://zh.wikipedia.org/wiki/File:Milk_glass.jpg "fig:Milk_glass.jpg")
19世紀前，鮮奶消毒技術仍未發明，直接擠出的奶常常被細菌污染，而在炎夏中，乳類可能會在數小時內腐壞。受種種限制，中世紀雖然已知奶品的營養價值，但成年人只會在生病或太貧窮時，才會飲用，這類飲料多留給小孩和老年人，當中又以奶酪或乳清較為普遍。\[30\]

當時中國人認為食酪漿是蠻族的特徵，而歐洲富人也對乳類敬而遠之。歐洲一些地方更鬧過傳說，認為牛奶與[生蠔](../Page/牡蠣.md "wikilink")、[菠菜](../Page/菠菜.md "wikilink")、[番茄或](../Page/番茄.md "wikilink")[黃瓜混在一起](../Page/黄瓜.md "wikilink")，會使它帶有劇毒。這可能源於乳類變壞或乳蛋白過敏，但亦反映時人對乳類的態度。\[31\]

直到中世紀末期，西方航海家出海時，為了解決營養問題，開始把奶牛帶上船。1493年，哥倫布第二次探索美洲時，就開始帶有[奶牛](../Page/乳牛.md "wikilink")。而[新教徒大舉移居美洲時](../Page/基督新教.md "wikilink")，英國更規定每5名乘客，必須備有一頭奶牛。

## 各國飲食特色

雖然歐洲文化相近，但各地的氣候、政治和宗教的差異，令地方菜系發展出不同特式，其中南方和暖，盛產[葡萄和](../Page/葡萄.md "wikilink")[橄欖](../Page/橄榄.md "wikilink")，但北方氣候太冷，[麥酒](../Page/麥酒.md "wikilink")、[無花果明顯較多](../Page/無花果.md "wikilink")\[32\]，這也令歐洲菜出現南北迥异的局面。以油來說，[橄欖油是地中海廣為採用的材料](../Page/橄欖油.md "wikilink")，但送到北歐後，卻變成貴價進口貨，當地尋常百姓多會用[榛子油](../Page/榛子油.md "wikilink")、[罌粟油烹食](../Page/罌粟油.md "wikilink")。

不過，歐洲民族的遷移活動、多民族雜居、彼此間的互相討伐，令歐洲菜各類菜系也互為影響。下文的分類是以現代或古國家作為分類總覽，但即使在一國之內，南北東西的人也可以存在不同飲食習慣，難以一概以論。

### 法國

[Les_Très_Riches_Heures_du_duc_de_Berry_Janvier.jpg](https://zh.wikipedia.org/wiki/File:Les_Très_Riches_Heures_du_duc_de_Berry_Janvier.jpg "fig:Les_Très_Riches_Heures_du_duc_de_Berry_Janvier.jpg")
雖然法國北部的菜系近似英倫的諾曼第王朝，但當地猶為喜歡肉湯，善於烹調各式肉類。當時歐洲有一種食物叫**，外型怪誔，色顏斑豔，食用價值一般不高，但在喜慶場合卻成為耍樂之物，法國北區煮這類菜尤其出色。他們會把食物染成兩種顏色，1420年《**》一書就曾記載一個烤野豬頭，一邊染成綠色，另一邊染成金黃，以增加喜慶氣氛。\[33\]

不過，一岸之隔的英國，雖然已開始流行以粉糰製作食物，但法國北部只用粉糰做些[餡餅](../Page/餡餅.md "wikilink")。而當時中歐一帶流行用以包餡或製成[麵條](../Page/麵條.md "wikilink")，在法國亦很少有記錄。

南部菜式與意大利和西班牙加泰羅尼亞一帶則頗為相似，食物中加不少[石榴](../Page/石榴.md "wikilink")、[檸檬等熱帶食材](../Page/柠檬.md "wikilink")，在[奧克語地區](../Page/奥克语.md "wikilink")，石榴汁也會用來調味。對照德國地區，南部法國人不常用牛油和豬油。

當地以乾烤、炸和焗的烹調方法最常見，炸的菜式有咸肉，而焗菜時當地人備有一種叫*trapa*的便攜焗爐，只要塞入食物，將焗爐埋在熱灰之內，就可使用。今日法國有一種[醋味油炸魚](../Page/醋.md "wikilink")**和[蒜泥](../Page/蒜.md "wikilink")[蛋黃醬](../Page/蛋黃醬.md "wikilink")**也都是中世紀流傳下來的食品。一些西班牙穆斯林的菜式，也曾流入法國南部，但南部人會轉用豬肉而非羊肉製作。而當時法國南部大城市[蒙彼利埃更以一種叫](../Page/蒙彼利埃.md "wikilink")**的酒聞名，做法是用細火煮熱[紅酒或](../Page/葡萄酒.md "wikilink")[白酒](../Page/白酒.md "wikilink")，加入[黑糖](../Page/紅糖.md "wikilink")、[水果糖浆](../Page/水果糖浆.md "wikilink")、[肉桂](../Page/桂皮.md "wikilink")、[薑](../Page/薑.md "wikilink")、[肉豆蔻](../Page/肉豆蔻.md "wikilink")、[黑椒等材料](../Page/黑胡椒.md "wikilink")，之後置放一星期，經過濾後入樽，一個月後才可以飲用，這種需要大量香料的酒，在當時被視為上等精品。\[34\]\[35\]

不過，中世紀南部菜譜大多已散失，目前除了一本以[拉丁語夾雜](../Page/拉丁语.md "wikilink")[奧克語的小食譜流傳下來外](../Page/奥克语.md "wikilink")，大部分資料都是從梵蒂冈1305年至1378年的檔案作推敲。當時教廷為了向窮人提供救濟，會紀錄一些地區窮人會吃甚麼，但接濟品多數是低質素的麵包、豆類、酒、芝士、魚、橄欖油和肉類，這並不能反映當時各階層的人在吃些甚麼。

### 英國

[Bayeux_Tapestry_scene43_banquet.jpg](https://zh.wikipedia.org/wiki/File:Bayeux_Tapestry_scene43_banquet.jpg "fig:Bayeux_Tapestry_scene43_banquet.jpg")舉行的宴會場面。\]\]
在[诺曼底公爵](../Page/诺曼底公爵.md "wikilink")[威廉征服](../Page/威廉一世_\(英格兰\).md "wikilink")[英格兰前](../Page/英格兰.md "wikilink")，當時[盎格魯-撒克遜人吃些甚麼](../Page/盎格魯-撒克遜人.md "wikilink")，紀錄乏善足陳。當時英國的菜式相當粗糙，菜式有燉肉和簡單的肉湯，平民和貴族都只喝麥酒，但諾曼第人的出現，改寫了英國的食餚，對貴族影響猶為明顯。

這些菜不止受法國人影響，也有著獨特的英國特色，這除了是食材有別外，最重要是當時諾曼第人攻陷[西西里](../Page/西西里岛.md "wikilink")，該處9世紀時曾受阿拉伯人入侵，食物充滿不少阿拉伯風情，這些特式也從西西里傳入英國。隨著諾曼第人加入[十字軍之列](../Page/十字軍東征.md "wikilink")，拜占庭和中東更多特式也流入英國。

今天英國菜常給人次人一等的感覺，但中世紀英國菜卻精緻得多。當地廚師會做一種叫驚喜食品，專門在各菜式傳上之間呈上，其中一名叫「金肉球」（法文：**）的菜式，以羊肉丸或雞丸製作，混入藏紅花色和塗一層蛋黃漿，做法後色彩斑麗，因此被叫作鍍金的蘋果。\[36\]

### 德國

在今日德國一帶，菜系以油膩見稱，當地人平日可採用大量的[牛油和](../Page/黄油.md "wikilink")[豬油煮食](../Page/豬油.md "wikilink")，但在齋戒時，食物問題卻嚴重得多。當地氣候嚴寒，沒有橄欖油出產，平日可以用牛油和豬油，但齋戒只得採用各類[堅果油](../Page/堅果油.md "wikilink")；而齋戒時教徒會吃魚類，偏偏魚獲在當地亦相當昂貴。

在調味上，當時德國人已酷愛[芥末](../Page/芥末.md "wikilink")，而且用量亦比其他歐洲人多。14世紀法國詩人[德尚就曾在遊記中指德國幾乎在每碟肉都放芥末](../Page/德尚.md "wikilink")，令人受不了。\[37\]

至於饑荒時的食譜，一向很少有紀錄，但德國現存一份古代食譜，教人打仗時可以把所有綠色東西和蔬菜隨便塞入動物的胃和腸衣內，這種近似[香腸的食品在煮食工具匱乏時猶為方便](../Page/香腸.md "wikilink")。\[38\]

### 波蘭

波蘭雖然有出產大麥和燕麥，但多用作飼料和釀酒，最常見的穀物仍是[粟和小麥](../Page/粟米.md "wikilink")，其中粟會用來做一種叫**的麥片粥。蔬菜則有[捲心菜](../Page/捲心菜.md "wikilink")、[豌豆](../Page/豌豆.md "wikilink")、[蠶豆](../Page/蚕豆.md "wikilink")、[洋蔥](../Page/洋蔥.md "wikilink")、[蒔蘿](../Page/蒔蘿.md "wikilink")、[芥末](../Page/芥末.md "wikilink")、[歐芹](../Page/香芹.md "wikilink")；其中歐芹還會用作食用染料，點綴上流社會的菜式。最常見的肉類有牛、豬、雞，而間中亦會有羔羊和綿羊肉。魚肉則主要是齋戒時才會食用。　

當時波蘭人不論階層，已偏好喝由不同穀物釀製的啤酒，小麥酒猶為普遍，他們還會加入有輕微毒素的[拉布拉多茶](../Page/拉布拉多茶.md "wikilink")，混和蛇麻草，令人喝後更有醉意。雖然斯拉夫民族喜好蜂蜜酒，但它售價較貴，波蘭人只會在洗禮、婚宴和重要場合上飲用。至於在西歐流行的葡萄酒，在當地多為進口貨，售價高昂令平民百姓卻步。　

早在中世紀時，不少德國麵包師傅已在當地謀生，也影響了當地麵包的發展。當地有一種叫**的炸麵圈，內裡填滿芝士，炸好後以[草莓伴吃](../Page/草莓.md "wikilink")，這正是由德國師傅所引進。他們還有一款叫**的餅派類食物，是一款扁平的榚點，上面加入蘋果等食材。\[39\]

### 意大利

[6-alimenti,_pasta,Taccuino_Sanitatis,_Casanatense_4182..jpg](https://zh.wikipedia.org/wiki/File:6-alimenti,_pasta,Taccuino_Sanitatis,_Casanatense_4182..jpg "fig:6-alimenti,_pasta,Taccuino_Sanitatis,_Casanatense_4182..jpg")已相當普遍。\]\]
中世紀時意大利是歐洲、亞洲和非洲的貿易樞杻，各種香料都會途經[熱那亞](../Page/热那亚.md "wikilink")、[威尼斯和](../Page/威尼斯.md "wikilink")[佛羅倫斯這些重要城邦](../Page/佛罗伦萨.md "wikilink")。經貿活動昌盛，令這一帶地區的菜餚不斷吸收外國文化，而中產階層也對飲食有相當要求。

約13世紀，[面条成為當地家家戶戶的主食](../Page/面条.md "wikilink")，但當時的麵多以大米而非小麥製造。當時還有一種意式千層蛋榚，表層加上奶油凍、一種叫[*marzapane*的甜點](../Page/杏仁膏.md "wikilink")、雞肉以至[大麻](../Page/大麻.md "wikilink")。對意大利人來說，[粥和](../Page/粥.md "wikilink")[飯同樣普遍](../Page/飯.md "wikilink")。他們會用法國扁豆或大麥做一種叫[*polenta*的粥](../Page/波倫塔.md "wikilink")，也有一種叫[*risotto*的燉飯](../Page/義大利燉飯.md "wikilink")。

芝士和香腸也十分常見，當時芝士的分類已相當仔細，托斯卡納主要出產新鮮芝士、米蘭則出品陳年芝士，芝士戶以樹皮包好產品後，再運送到意大利各地方。他們也愛用雞蛋下菜，常見菜式就有菜肉餡煎蛋餅和不同掩烈。地中海的氣候也令當地出產不少橄欖和葡萄，尋常家庭也可以用來調味煮食，這些食材在歐洲北部連貴族也未必負擔得起。\[40\]

不過，不少意大利菜都是在中世紀末才開始出現。正如今天不少人眼中，意大利菜會有[香莢蘭](../Page/香莢蘭.md "wikilink")、[粟米](../Page/粟米.md "wikilink")、[腰豆](../Page/腰豆.md "wikilink")、[番茄](../Page/番茄.md "wikilink")，這些食材都是[發現新世界後才得以引進](../Page/地理大发现.md "wikilink")。\[41\]

### 西班牙

今日西班牙身處的[伊比利亞半島](../Page/伊比利亞半島.md "wikilink")，有一座[比利牛斯山脈把它與歐洲其他地方分隔](../Page/比利牛斯山.md "wikilink")，而且半島上地勢差異廣闊，造就當地多元文化的诞生。

在古時，地中海東岸古國[腓尼基為當地引入橄欖](../Page/腓尼基.md "wikilink")；古希臘帶來一種叫**的白葡萄，為當地中世紀的釀酒業殿定基礎。它亦曾是羅馬帝國最西的領土，羅馬遺風對當地飲食影響深遠。當羅馬人倒台後，4世紀時西哥特人在法國和西班牙建立條頓族王國後，雖然繼承不少羅馬風俗，但半島隨後又陷落在北非的穆斯林手中，把不少阿拉伯菜式引入當地。

這些阿拉伯統治者用玻璃製造[聖杯](../Page/聖杯.md "wikilink")；以[肉桂](../Page/肉桂.md "wikilink")、[乳香](../Page/乳香.md "wikilink")、[香菜](../Page/香菜.md "wikilink")（）、[芝麻](../Page/芝麻.md "wikilink")、[薄荷](../Page/薄荷.md "wikilink")、生果煮肉；食餚偏好加入濃烈的濃酸汁和羅望子調味；還會用大米或扁桃粉，把菜式煮得濃稠。今次西班牙文食譜中仍有不少阿拉伯外來語的痕跡，還流入歐洲其他地區，例如糖是**；[朝鮮薊是](../Page/菜薊.md "wikilink")**；[藏紅花叫](../Page/藏紅花.md "wikilink")**；菠菜叫**。有些研究更認為今天歐洲習慣先喝湯、再吃肉、最後吃甜點，正是由阿拉伯人開始；而歐洲流行的油炸醋魚（）也可能是阿拉伯和波斯的菜式。

在中世紀，當地有一本非拉丁文寫成的食譜叫**，以西班牙的[加泰罗尼亚语教人以苦枳](../Page/加泰罗尼亚语.md "wikilink")、玫瑰水、蘋果酒（），製作多款氣味濃烈的菜式，還羅列不少煮魚方法。這本書雖然在1520年出版，但估計早在1490年前已撰寫。從食譜可見，當地人常吃麵包、葡萄酒、大蒜、洋蔥、橄欖油、雞蛋、羔羊、山羊和煙肉。\[42\]

### 拜占庭

[拜占庭又稱](../Page/拜占庭.md "wikilink")[東羅馬帝國](../Page/東羅馬帝國.md "wikilink")，當地人像古希臘人一樣愛好[橄欖](../Page/橄欖.md "wikilink")、小麥包、[海芋植物](../Page/海芋.md "wikilink")、鹹[魚露](../Page/魚露.md "wikilink")，其中鹹魚露甚至代替了[食盐的角色](../Page/食盐.md "wikilink")；他們也從阿拉伯進口了[橙和](../Page/橙.md "wikilink")[茄子](../Page/茄.md "wikilink")。[吞拿魚](../Page/鮪魚.md "wikilink")、[龍蝦](../Page/龍蝦.md "wikilink")、[青口](../Page/青口.md "wikilink")、[生蠔和](../Page/生蠔.md "wikilink")[鯉魚也是桌上佳餚](../Page/鯉魚.md "wikilink")，而自11世紀開始，從[黑海進口的](../Page/黑海.md "wikilink")[鱼子酱也成為美饌](../Page/鱼子酱.md "wikilink")。

當地的芝士以[羊乳酪最常見](../Page/羊乳酪.md "wikilink")；葡萄酒中以**和**兩類較普及；水果以無花果、葡萄、石榴、蘋果為主；肉類有羔羊肉、[羚羊肉](../Page/羚羊.md "wikilink")、野驢和其他還在吃奶的小動物；肉多數是煙薰、風乾和鹽煮。拜占庭的甜品猶為著名，當時有米布丁、餅乾、果醬等，他們還會蜜糖調味。

不過，低下層的食物較為簡單。他們會用醋混和水飲用，代替葡萄酒；食物多數是橄欖、水果和洋蔥，有時也可以吃一片芝士，又或用咸豬肉與捲心菜餚製一味燉菜。

雖然東西羅馬的教會處於分裂，但東羅馬人仍會實施齋戒，在周三、周五和特別節日前亦需禁食[紅肉](../Page/紅肉.md "wikilink")、牛奶、雞蛋等。\[43\]

## 參考文獻

### 引用

### 书籍

<div class="references-small">

  - Adamson, Melitta Weiss (2004), *Food in Medieval Times*. ISBN
    0-313-32147-7.
  - Dembinska, Maria (1999), *Food and drink in medieval Poland:
    rediscovering a cuisine of the past*，原作為波蘭文，英文版由 Magdalena
    Thomas翻譯。ISBN 0-8122-3224-0.
  - *The Fontana Economic History of Europe: The Middle Ages* (1972);
    J.C Russel *Population in Europe 500-1500*. ISBN 0-00-632841-5.
  - Henisch, Bridget Ann (1976), *Fast and Feast: Food in Medieval
    Society*. ISBN 0-271-01230-7.
  - *Medieval science, technology, and medicine : an encyclopedia*
    (2005), Thomas Glick, Steven J. Livesey, Faith Wallis, editors. ISBN
    0-415-96930-1.
  - *Regional Cuisines of Medieval Europe: A Book of Essays*
    (2002)，編輯：Melitta Weiss Adamson. ISBN 0-415-92994-6.
  - Scully, Terence (1995), *The Art of Cookery in the Middle Ages*.
    ISBN 0-85115-611-8.

</div>

## 外部連結

  - [*Le Viandier de
    Taillevent*](http://www.telusplanet.net/public/prescotj/data/viandier/viandier1.html)－14世紀法國著名食譜

  - [英國圖書館中世紀飲食叢書](http://www.bl.uk/learning/langlit/booksforcooks/med/medievalfood.html)

  - [如何用現代材料煮中世紀食物](http://www.godecookery.com/how2cook/howto08a.htm)

  - [MedievalCookery.com](http://www.medievalcookery.com/) - 中世紀飲食食譜及資料

## 参见

  - [古埃及饮食文化](../Page/古埃及饮食文化.md "wikilink")
  - [古希腊饮食文化](../Page/古希腊饮食文化.md "wikilink")
  - [古罗马饮食文化](../Page/古罗马饮食文化.md "wikilink")
  - [西餐](../Page/西餐.md "wikilink")

{{-}}

[中世纪饮食文化](../Category/中世纪饮食文化.md "wikilink")
[Category:歐洲飲食](../Category/歐洲飲食.md "wikilink")

1.  Henisch 2: *Mealtimes*

2.  Henisch, pp. 185–186.

3.

4.  Henisch, p. 40.

5.  Henisch, p. 43.

6.  Scully, pp. 135-136.

7.  Scully, pp. 126-135.

8.  Adamson, 2: *Food Preparation*

9.  Dembinska, p. 143.

10. Scully. pp. 44-46.

11. Scully, p. 70.

12.
13. Scully, p. 96.

14. *Medieval science...*; *Food storage and preservation*

15. Black, William. (2005). The Land that Thyme Forgot. Bantam. ISBN
    0593 053621. p. 346

16. Wood, Diana. *Medieval Economic Thought*. Cambridge University
    Press. ISBN 0-521-45893-5. 2002. p 97-98.

17. Scully, pp. 35–38.

18. Adamson, pp. 161–164.

19.
20. *Regional Cuisines* p. 89.

21. Adamson, p. 164.

22. 德國中世紀時的學者[博克曾在食品庫紀錄當地人常吃捲心菜等食譜](../Page/Hieronymus_Bock.md "wikilink")，參見
    *Regional Cuisines* 第163頁.

23. Adamson 1: *Foodstuffs*

24. Scully, pp. 84-86.

25. Scully, pp. 154-157.

26. Dembinska, p. 80.

27. Scully, pp. 138–146.

28. *Medieval science...*; *Brewing*

29. Scully, pp. 157-165.

30. Adamson, pp. 48–51.

31. [新華網:牛奶的故事](http://news.xinhuanet.com/banyt/2005-12/05/content_3878242.htm)
    2005年

32. Scully pg. 218

33. *Regional Cuisines* Chapter 4: *Medieval France* A. The North

34. *Regional Cuisines* Chapter 4: *Medieval France* B. The South

35. [Hypocras.com：酒成份介紹](http://www.hypocras.com/composition.html)

36. *Regional Cuisines* Chapter 4: *Medieval Britain*

37. Scully p. 233

38. *Regional Cuisines...* pg. 159

39. Dembinska, Chapter 4

40. *Regional Cuisines* Chapter 4: *Medieval and Renaissance Italy* A.
    The Peninsula

41. *Regional Cuisines* pg. 96

42. *Regional Cuisines* Chapter 5: *Medieval Spain*

43. Regional Cuisines Chapter 1: The Greco-Roman World