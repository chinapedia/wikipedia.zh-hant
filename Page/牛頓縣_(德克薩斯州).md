**牛頓縣**（**Newton County,
Texas**）是[美國](../Page/美國.md "wikilink")[德克薩斯州最東部的一個縣](../Page/德克薩斯州.md "wikilink")，東鄰[路易斯安那州](../Page/路易斯安那州.md "wikilink")。面積2,433平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口15,072人。縣治[牛頓](../Page/牛頓_\(德克薩斯州\).md "wikilink")（Newton）。

成立於1846年4月22日。縣名紀念紀念[法蘭西斯·馬里昂將軍手下的](../Page/法蘭西斯·馬里昂.md "wikilink")[約翰·牛頓](../Page/約翰·牛頓_\(美國軍人\).md "wikilink")（John
Newton）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[N](../Category/得克萨斯州行政区划.md "wikilink")

1.  [德克薩斯百科全書](http://www.tshaonline.org/handbook/online/articles/hcn03)