**莫吉廖夫**（[白俄罗斯语](../Page/白俄罗斯语.md "wikilink")：**Магілёў**，[俄罗斯语](../Page/俄罗斯语.md "wikilink")：**Могилёв**）位于[白俄罗斯东部](../Page/白俄罗斯.md "wikilink")[第聂伯河畔](../Page/第聂伯河.md "wikilink")，是该国第三大城市和[莫吉廖夫州的首府](../Page/莫吉廖夫州.md "wikilink")，人口约374,644（2015年），莫吉廖夫市面积118.50平方公里，分为列宁区和十月区。

## 名人

  - [伊赛·舒尔](../Page/伊赛·舒尔.md "wikilink")：数学家
  - [奥托·施密特](../Page/奥托·施密特.md "wikilink")：数学家、天文学家、地理学家
  - [列夫·波卢加耶夫斯基](../Page/列夫·波卢加耶夫斯基.md "wikilink")：国际象棋大师
  - [欧文·伯林](../Page/欧文·伯林.md "wikilink")：作曲家

## 友好城市

<div style="-moz-column-count: 3; column-count: 3;">

  - [Flag_of_Serbia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Serbia.svg "fig:Flag_of_Serbia.svg")
    [塞尔维亚](../Page/塞尔维亚.md "wikilink")[克拉古耶瓦茨](../Page/克拉古耶瓦茨.md "wikilink")

  - [Flag_of_Bulgaria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bulgaria.svg "fig:Flag_of_Bulgaria.svg")
    [保加利亚](../Page/保加利亚.md "wikilink")[加布罗沃](../Page/加布罗沃.md "wikilink")

  - [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")
    [法国](../Page/法国.md "wikilink")[维勒班](../Page/维勒班.md "wikilink")

  - [郑州市](../Page/郑州市.md "wikilink")

  - [蘇姆蓋特](../Page/蘇姆蓋特.md "wikilink")

  - [艾森納赫](../Page/艾森納赫.md "wikilink")

  - [大不里士](../Page/大不里士.md "wikilink")

  - [奇姆肯特](../Page/奇姆肯特.md "wikilink")

  - [克萊佩達](../Page/克萊佩達.md "wikilink")

  - [弗沃茨瓦韦克](../Page/弗沃茨瓦韦克.md "wikilink")

  - [奔薩](../Page/奔薩.md "wikilink")

  - [圖拉](../Page/圖拉_\(圖拉州\).md "wikilink")

  - [托博爾斯克](../Page/托博爾斯克.md "wikilink")

  - [茲韋尼哥羅德](../Page/茲韋尼哥羅德.md "wikilink")

  - /[克赤](../Page/克赤.md "wikilink")

  - [尼古拉耶夫](../Page/尼古拉耶夫_\(烏克蘭\).md "wikilink")

  - [巴爾代約夫](../Page/巴爾代約夫.md "wikilink")

## 外部链接

  - [Weather Mogilev - mogilev.the.by](http://mogilev.the.by)

[M](../Category/莫吉廖夫州城市.md "wikilink")