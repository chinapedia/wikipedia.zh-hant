**安汶岛**（Ambon
Island），是[印度尼西亚](../Page/印度尼西亚.md "wikilink")[马鲁古群岛的主要](../Page/马鲁古群岛.md "wikilink")[岛屿之一和政治中心](../Page/岛屿.md "wikilink")，位于[班达海北岸](../Page/班达海.md "wikilink")，面积约775平方公里。该岛属[马鲁古省管辖](../Page/马鲁古省.md "wikilink")，岛上最大城市是[安汶市](../Page/安汶市.md "wikilink")，该市是马鲁古省首府。

## 外部链接

  - [安汶岛信息](http://www.malukuprov.go.id/subpage.asp?id=40)

[A](../Category/印尼岛屿.md "wikilink")