**鄭志強**（），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")，其胞兄為[義大犀牛隊左外野手](../Page/義大犀牛.md "wikilink")[鄭達鴻](../Page/鄭達鴻.md "wikilink")。

## 經歷

  - [台東縣南王國小少棒隊](../Page/台東縣.md "wikilink")
  - [台東縣卑南國中青少棒隊](../Page/台東縣.md "wikilink")
  - [高雄縣高苑工商青棒隊](../Page/高雄縣.md "wikilink")
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽 | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死 | 三振 | 責失 | 投球局數 | 防禦率  |
| ----- | -------------------------------- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | ---- | ---- |
| 2006年 | [中信鯨](../Page/中信鯨.md "wikilink") | 9  | 0  | 0  | 0  | 0  | 0  | 0  | 7  | 11 | 3  | 14.2 | 1.84 |
| 2007年 | [中信鯨](../Page/中信鯨.md "wikilink") | 2  | 0  | 0  | 0  | 0  | 0  | 0  | 4  | 0  | 1  | 5.0  | 1.80 |
| 合計    | 2年                               | 11 | 0  | 0  | 0  | 0  | 0  | 0  | 11 | 11 | 4  | 19.2 | 1.83 |

## 特殊事蹟

## 外部連結

[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:中信鯨隊球員](../Category/中信鯨隊球員.md "wikilink")
[Zhi志強](../Category/鄭姓.md "wikilink")