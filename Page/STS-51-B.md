****是历史上第十七次航天飞机任务，也是[挑战者号航天飞机的第六次太空飞行](../Page/挑戰者號太空梭.md "wikilink")。

## 任务成员

  - **[罗伯特·欧沃米尔](../Page/罗伯特·欧沃米尔.md "wikilink")**（，曾执行以及任务），指令长
  - **[弗雷德里克·格里高利](../Page/弗雷德里克·格里高利.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[唐·林德](../Page/唐·林德.md "wikilink")**（，曾执行任务），任务专家
  - **[诺曼·萨加德](../Page/诺曼·萨加德.md "wikilink")**（，曾执行、、、、以及任务），任务专家
  - **[威廉·桑顿](../Page/威廉·桑顿.md "wikilink")**（，曾执行以及任务），任务专家
  - **[路德维克·范·登·伯格](../Page/路德维克·范·登·伯格.md "wikilink")**（，曾执行任务），有效载荷专家
  - **[王赣骏](../Page/王赣骏.md "wikilink")**（，曾执行任务），有效载荷专家

[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:1985年科学](../Category/1985年科学.md "wikilink")
[Category:挑战者号航天飞机任务](../Category/挑战者号航天飞机任务.md "wikilink")
[Category:1985年4月](../Category/1985年4月.md "wikilink")
[Category:1985年5月](../Category/1985年5月.md "wikilink")