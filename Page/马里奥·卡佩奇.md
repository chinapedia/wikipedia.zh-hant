**马里奥·卡佩奇**（，），生于意大利的美国分子遗传学家\[1\]，也是2007年[诺贝尔生理学或医学奖得主之一](../Page/诺贝尔生理学或医学奖.md "wikilink")\[2\]。马里奥·卡佩奇目前是美国[犹他大学医学院](../Page/犹他大学.md "wikilink")[人类](../Page/人类.md "wikilink")[遗传学与](../Page/遗传学.md "wikilink")[生物学杰出教授](../Page/生物学.md "wikilink")（Distinguished
Professor）。

## 生平

马里奥·卡佩奇于1937年出生于[意大利北部城市](../Page/意大利.md "wikilink")[维罗纳](../Page/维罗纳.md "wikilink")\[3\]，并于1961年获得美国[俄亥俄州](../Page/俄亥俄州.md "wikilink")[安提阿大学](../Page/安提阿大学.md "wikilink")（Antioch
College）[化学及](../Page/化学.md "wikilink")[物理学学士学位](../Page/物理学.md "wikilink")。1967年，马里奥·卡佩奇在[詹姆斯·沃森](../Page/詹姆斯·沃森.md "wikilink")（1962年诺贝尔生理学或医学奖获得者之一）的指导下，于[哈佛大学取得](../Page/哈佛大学.md "wikilink")[生物物理学](../Page/生物物理学.md "wikilink")（Biophysics）[博士学位](../Page/博士.md "wikilink")。

1967年至1969年，卡佩奇在哈佛大学[学者学会](../Page/学者学会.md "wikilink")（Society of
Fellows）担任初级研究员（Junior
Fellow）。1969年，卡佩奇成为哈佛大学医学院[生物化学系助理教授](../Page/生物化学.md "wikilink")，并于1971年晋升为副教授。1973年，卡佩奇进入[犹他大学任教](../Page/犹他大学.md "wikilink")。自1988年起，卡佩奇还担任了[霍华德·休斯医学研究所的研究员](../Page/霍华德·休斯医学研究所.md "wikilink")（Investigator
of the Howard Hughes Medical
Institute），另外他还担任美国[国家科学院院士](../Page/美国国家科学院.md "wikilink")。

## 所获荣誉

  - 1992 - [Bristol-Myers](../Page/Bristol-Myers.md "wikilink") Squibb
    Award for Distinguished Achievement in Neuroscience Research
  - 1993 - [盖尔德纳国际奖](../Page/盖尔德纳国际奖.md "wikilink")
  - 2001 - [阿尔伯特·拉斯克基础医学研究奖](../Page/阿尔伯特·拉斯克基础医学研究奖.md "wikilink")\[4\]
  - 1996 - [京都賞](../Page/京都賞.md "wikilink")（基礎科學）
  - 2001 - 西班牙Jiménez-Diáz獎
  - 2007 -
    诺贝尔生理学或医学奖，与[马丁·埃文斯和](../Page/马丁·埃文斯.md "wikilink")[奥利弗·史密斯一同获得](../Page/奥利弗·史密斯.md "wikilink")\[5\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:美国遗传学家](../Category/美国遗传学家.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")
[Category:沃爾夫醫學獎得主](../Category/沃爾夫醫學獎得主.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")

1.

2.

3.
4.

5.