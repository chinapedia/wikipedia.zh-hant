**福建师范大学附属中学**（英文：**The Affiliated High School Of Fujian Normal
University**，常缩写为**FJSDFZ**或者**SDFZ**，简称：**福建师大附中**）是一所三年制政府全资建立的公立中学，为[福建师范大学的附属中学](../Page/福建师范大学.md "wikilink")。

## 简介

[The_gate_of_the_Affiliated_Middle_School_of_Fujian_Normal_University.jpg](https://zh.wikipedia.org/wiki/File:The_gate_of_the_Affiliated_Middle_School_of_Fujian_Normal_University.jpg "fig:The_gate_of_the_Affiliated_Middle_School_of_Fujian_Normal_University.jpg")
[Fjsdfz_qs.JPG](https://zh.wikipedia.org/wiki/File:Fjsdfz_qs.JPG "fig:Fjsdfz_qs.JPG")
[Fjsdfz_de.JPG](https://zh.wikipedia.org/wiki/File:Fjsdfz_de.JPG "fig:Fjsdfz_de.JPG")
[Fjsdfz_jr.JPG](https://zh.wikipedia.org/wiki/File:Fjsdfz_jr.JPG "fig:Fjsdfz_jr.JPG")
[The_Observatory_of_the_Affiliated_Middle_School_of_Fujian_Normal_University.jpg](https://zh.wikipedia.org/wiki/File:The_Observatory_of_the_Affiliated_Middle_School_of_Fujian_Normal_University.jpg "fig:The_Observatory_of_the_Affiliated_Middle_School_of_Fujian_Normal_University.jpg")

福建师大附中位于[福州市](../Page/福州市.md "wikilink")[仓山区对湖路](../Page/仓山区.md "wikilink")，是福建省首批办好的重点中学之一，1994年被省教委确认为省一级达标学校，2003年9月被省教育厅确认为首批省级示范性普通高级中学。

学校现有校园面积62623平方米（约95亩），建筑面积51795平方米。教学设施完善，师资力量雄厚。现有教职工167人，其中高级教师79人。先后有27人评为特级教师，18位教师具有硕士学位。目前有38个高中教学班（含2个空军飞行学员预备班），在校学生1900多人。

作为福建师大附中前身的英华中学创办于1881年。该校在130多年的历史中，涌现了[侯德榜](../Page/侯德榜.md "wikilink")、[陈景润](../Page/陈景润.md "wikilink")、[沈元](../Page/沈元_\(空气动力学家\).md "wikilink")、[郭孔辉等](../Page/郭孔辉.md "wikilink")12位[中国科学院和](../Page/中国科学院.md "wikilink")[工程院院士和其他著名专家学者](../Page/中国工程院.md "wikilink")；以及[辛亥革命元老](../Page/辛亥革命.md "wikilink")、前[国民政府主席](../Page/国民政府.md "wikilink")[林森](../Page/林森.md "wikilink")，著名[华侨](../Page/华侨.md "wikilink")[黄乃裳等大批民主革命志士](../Page/黄乃裳.md "wikilink")。

## 历史

前身是三所私立教会学校（[私立英华中学](../Page/私立英华中学.md "wikilink")、[私立华南女子高级中学](../Page/私立华南女子高级中学.md "wikilink")、[私立陶淑女子中学](../Page/私立陶淑女子中学.md "wikilink")）。1951年7月31日[福建省人民政府接办福州](../Page/福建省.md "wikilink")[私立英华中学](../Page/私立英华中学.md "wikilink")、[私立华南女子高级中学](../Page/私立华南女子高级中学.md "wikilink")，合并成立福州第二中学。同年8月11日又将[私立陶淑女子中学并入福州第二中学](../Page/私立陶淑女子中学.md "wikilink")。1952年改名福州大学附属中学，1953年更名福建师范学院附属中学，1973年定为现名。\[1\]

## 著名校友

### 革命志士

[林森](../Page/林森.md "wikilink") 前国民政府主席；

[黄乃裳](../Page/黄乃裳.md "wikilink")
著名华侨，前[孙中山大元帅府顾问](../Page/孙中山.md "wikilink")；

[池步洲](../Page/池步洲.md "wikilink")
[中统特工](../Page/中统.md "wikilink")，成功破译二战日军偷袭[珍珠港的电报密码](../Page/珍珠港.md "wikilink")。

### 两院院士

[侯德榜](../Page/侯德榜.md "wikilink") 1903至1906年就读于原福州英华中学，
[中国科学院院士](../Page/中国科学院.md "wikilink")，化工专家；

[沈元](../Page/沈元.md "wikilink") 1935年毕业于原英华高中，中国科学院院士，空气动力学家；

[高由禧](../Page/高由禧.md "wikilink") 1938年毕业于原英华高中，中国科学院院士，气象学家；

[王仁](../Page/王仁.md "wikilink") 1930年至l934年就读于原福州英华中学，中国科学院院士，力学专家；

[陈彪](../Page/陈彪.md "wikilink") 1938至1940年就读于原福州英华中学，中国科学院院士，天体物理学家；

[曾融生](../Page/曾融生.md "wikilink") 1942年毕业于原福州英华中学，中国科学院院士，地球物理学家；

[阙端麟](../Page/阙端麟.md "wikilink") 1947年毕业于原福州英华高中，中国科学院院士，半导体材料学家；

[卢耀如](../Page/卢耀如.md "wikilink")
1950年毕业于原福州英华高中，[中国工程院院士](../Page/中国工程院.md "wikilink")，地质学家；

[陈景润](../Page/陈景润.md "wikilink")
1948至1950年就读于原福州英华高中，l950年夏于高三上提前考入厦门大学数理系，中国科学院院士，数学家；

[张　钹](../Page/张_钹.md "wikilink")
1953年于毕业于该校高中部，中国科学院院士，俄罗斯自然科学院外籍院士，计算机专家；

[郭孔辉](../Page/郭孔辉.md "wikilink") 1952年毕业于该校高中部，中国工程院院士，汽车工业专家；

[萨支唐](../Page/萨支唐.md "wikilink") 1949年毕业于该校，中国科学院外籍院士，地、微电子学和半导体器件专家。

[黎念之](../Page/黎念之.md "wikilink")
1945至1946年就读于原英华中学。中国科学院外籍院士、美国国家工程院士、[台湾中央研究院院士](../Page/台湾中央研究院.md "wikilink")，化工专家，膜科学与技术的奠基人之一。

## 学校标志

**校标**：制作于1991年，图案上方是八道光芒的[北极星](../Page/北极星.md "wikilink")，代表坚定正确的政治方向，又代表[八闽大地](../Page/八闽.md "wikilink")。下方U字（代表大学）构成的书本，嵌上[原子结构中](../Page/原子.md "wikilink")[电子运动的轨道](../Page/电子.md "wikilink")（代表附属中学），构成“中”字，象征勤奋学习，热爱科学；也象征福建师范大学附属中学之义。

**校训**：以天下为己任

**校风**：文明、勤奋、求实、创新

**校歌**：[《福建师大附中之歌》](http://www.fjsdfz.org/introduction/xg.asp)

## 办学特色

福建师大附中在1985年以来，已先后在国际中学生学科竞赛中获得15面奖牌（其中[国际科学奥林匹克竞赛获](../Page/国际科学奥林匹克竞赛.md "wikilink")8金5银1铜计14面奖牌，占全省奖牌数一半）。在全国及省、市各类竞赛中每年均有150人次以上获奖。多年来，该校高、中考升学率、进入重点大学人数均稳居省、市前列。

## 参考文献

<references/>

## 内部链接

  - [福建师范大学](../Page/福建师范大学.md "wikilink")
  - [福州时代中学](../Page/福州时代中学.md "wikilink")

## 外部链接

  - [师大附中网站](http://www.fjsdfz.org)

[附](../Category/福州中等学校.md "wikilink")
[Category:福建師範大學](../Category/福建師範大學.md "wikilink")
[Category:1951年創建的教育機構](../Category/1951年創建的教育機構.md "wikilink")

1.