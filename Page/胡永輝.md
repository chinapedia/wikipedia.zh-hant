**胡永輝**<span style="font-size:smaller;">，[MBE](../Page/MBE.md "wikilink")</span>（**Vincent
Woo**），祖籍[順德市](../Page/順德.md "wikilink")[桂洲](../Page/桂洲.md "wikilink")，是香港永常(集團)有限公司董事經理。
1997年成立[胡永輝慈善基金](../Page/胡永輝慈善基金.md "wikilink")，在中港兩地推動[慈善及](../Page/慈善.md "wikilink")[教育活動方面相當積極](../Page/教育.md "wikilink")。

## 經歷

胡永輝於1951年先後在[廣州市法商學院](../Page/廣州市.md "wikilink")、[嶺南大學](../Page/岭南大学_\(广州\).md "wikilink")、[中山大學就讀](../Page/中山大學.md "wikilink")，1955年从[武漢](../Page/武漢.md "wikilink")[中南財經大學畢業](../Page/中南財經大學.md "wikilink")，获[經濟學學士学位](../Page/經濟學.md "wikilink")。
胡永輝后在[中國水利部](../Page/中國水利部.md "wikilink")[黃河](../Page/黃河.md "wikilink")[水利委員會及計委工作](../Page/水利.md "wikilink")。1962年秋移居[香港](../Page/香港.md "wikilink")，助兄（金王）[胡漢輝發展地產](../Page/胡漢輝.md "wikilink")。

## 公職

  - 香港順德聯誼總會 - 榮譽會長。
  - 1976年任香港[保良局](../Page/保良局.md "wikilink") - 總理
  - 1982年任保良局 - [主席](../Page/主席.md "wikilink")
  - 保良局顧問局 - 顧問
  - [陳南昌學校](../Page/陳南昌學校.md "wikilink") - 校監
  - [嶺南學院](../Page/嶺南大學_\(香港\).md "wikilink")[獎學金](../Page/獎學金.md "wikilink")
    - 贊助人
  - [香港公開進修大學](../Page/香港公開大學.md "wikilink") - 籌款委員會委員
  - [香港中華科學杜會協進會](../Page/香港中華科學杜會協進會.md "wikilink") - 董事
  - [香港順德聯誼總會梁潔華小學](../Page/香港順德聯誼總會梁潔華小學.md "wikilink") - 校董
  - [鄭裕彤中學](../Page/鄭裕彤中學.md "wikilink") - 顧問
  - [中國岳陽大學](../Page/中國岳陽大學.md "wikilink") - 名譽教授
  - [順德桂洲社會福利教育基金會](../Page/順德桂洲社會福利教育基金會.md "wikilink") - 委員
  - 1993年 [香港工業獎](../Page/香港工業獎.md "wikilink") - 贊助人
  - 1996年
    [公益金](../Page/公益金.md "wikilink")[港島區](../Page/港島區.md "wikilink")[百萬行](../Page/百萬行.md "wikilink")
    - 主席
  - [香港青年獎勵計划](../Page/香港青年獎勵計划.md "wikilink")[委員會](../Page/委員會.md "wikilink")
    - 委員
  - [香港順德聯誼總會](../Page/香港順德聯誼總會.md "wikilink") - 榮譽會長
  - [香港西區婦女會](../Page/香港西區婦女會.md "wikilink") - 名譽會長
  - [香港中區街坊福利委員會](../Page/香港中區街坊福利委員會.md "wikilink") - 名譽會長
  - [香港童軍地域總會](../Page/香港童軍.md "wikilink") - 名譽會長
  - 偉思幼稚園 - 校董

## 勛銜

  - 1984年 [MBE](../Page/MBE.md "wikilink")

## 支持桂洲家鄉建設

  - 捐助[桂鎮小學](../Page/桂鎮小學.md "wikilink")、[桂洲新中學](../Page/桂洲新中學.md "wikilink")、[桂洲職業中學](../Page/桂洲職業中學.md "wikilink")、[桂洲馮派普頤老院](../Page/桂洲馮派普頤老院.md "wikilink")、[桂洲社會福利教育基金會](../Page/桂洲社會福利教育基金會.md "wikilink")、[順德體育中心](../Page/順德體育中心.md "wikilink")、[順德市慈善基金等](../Page/順德市慈善基金.md "wikilink")。

## 名下馬匹

胡永輝是[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")，曾經與胡經華、胡經聰合夥育有馬匹振翅高飛、飛越關山。

## 外部連線

  - [胡永輝教授](https://web.archive.org/web/20060823023800/http://202.104.28.98/dfwx/sdyxl/part2a/hyh/hyh.htm)
  - [胡永輝注資救](http://www.singtao.com.hk/yesterday/fin/1108do06.html)
    [鴻運證券](../Page/鴻運證券.md "wikilink")

[Category:香港順德人](../Category/香港順德人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港慈善家](../Category/香港慈善家.md "wikilink")
[W](../Category/MBE勳銜.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[W](../Category/胡姓.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")