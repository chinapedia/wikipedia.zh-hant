**柴田昌弘**。日本[漫畫家](../Page/漫畫家.md "wikilink")。[三重縣](../Page/三重縣.md "wikilink")[松阪市出身](../Page/松阪市.md "wikilink")。其妻子為是日本[漫畫家](../Page/漫畫家.md "wikilink")。代表作是《[齋女傳說](../Page/齋女傳說.md "wikilink")》。

## 作品

  - [PANDORA](../Page/PANDORA.md "wikilink")（潘朵拉）、全1冊、原名《》\[1\]
  - [THE APEMAN 猴人](../Page/THE_APEMAN_猴人.md "wikilink")、全1冊、原名《》\[2\]

<!-- end list -->

  -
    收錄短篇：《契約之樹》、《和幽靈約會的方法》

<!-- end list -->

  - [齋女傳說](../Page/齋女傳說.md "wikilink")、全18冊、原名《》<ref>

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9573436426

:\# ISBN 9573436434

:\# ISBN 9573436442

:\# ISBN 9573436450

:\# ISBN 9573436469

:\# ISBN 957343816X

:\# ISBN 9573439166

:\# ISBN 9573439174

:\# ISBN 9573441349

:\# ISBN 957344500X

:\# ISBN 9573449099

:\# ISBN 9573449102

:\# ISBN 9573457709

:\# ISBN 9573457717

:\# ISBN 9573457725

:\# ISBN 9573457733

:\# ISBN 9573487039

:\# ISBN 9573487047

</div>

</ref>

  - [奇謀女衛士](../Page/奇謀女衛士.md "wikilink")、全19冊、原名《》<ref>

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9573487799

:\# ISBN 9573487802

:\# ISBN 9576997178

:\# ISBN 9576997186

:\# ISBN 9861103481

:\# ISBN 9861112529

:\# ISBN 986111338X

:\# ISBN 9861132082

:\# ISBN 9861134492

:\# ISBN 9861148353

:\# ISBN 9861153934

:\# ISBN 9861155228

:\# ISBN 9861171665

:\# ISBN 9861175121

:\# ISBN 9861179631

:\# ISBN 9789861189208

:\# ISBN 9789861199344

</div>

</ref>

  - [求愛探險隊](../Page/求愛探險隊.md "wikilink")、8冊待續、原名《》（日文普通版全9冊，文庫版全5冊）<ref>

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9573422301

:\# ISBN 957342231X

:\# ISBN 9573422328

:\# ISBN 9573422336

:\# ISBN 9573422344

:\# ISBN 9573443783

:\# ISBN 9573443791

:\# ISBN 9573452316

</div>

</ref>

  - [疾風快遞](../Page/疾風快遞.md "wikilink")、全2冊、原名《》<ref>

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9576315174

:\# ISBN 9576315182

</div>

</ref>

  - [冥界人形](../Page/冥界人形.md "wikilink")、全1冊、原名《》<ref>

:\# ISBN 9861106774 </ref>

  - [龍之城](../Page/龍之城.md "wikilink")、全2冊、原名《》<ref>

<div class="references-small" style="-moz-column-count:3; column-count:3;">

:\# ISBN 9573486342

:\# ISBN 9573486350

</div>

</ref>

  - [沉默之街](../Page/沉默之街.md "wikilink")、全1冊\[3\]

<!-- end list -->

  -
    收錄短篇：《黑暗之穴》、《哈利亞（親愛的守護獸人）》、《雪橇（快遞騎士之路）》、《第2次的祭典》

<!-- end list -->

  - [宗三郎・男子漢](../Page/宗三郎・男子漢.md "wikilink")、全1冊\[4\]

<!-- end list -->

  -
    收錄短篇：《水裡的貓》、《惡靈山谷》

<!-- end list -->

  - [死亡競賽](../Page/死亡競賽.md "wikilink")、原名《》
  - [魔睡宮](../Page/魔睡宮.md "wikilink")、全1冊

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [恐慌舎](http://www.linkclub.or.jp/~shakan/)（官方網站）2008年5月6日閉鎖

  - [](http://talent.yahoo.co.jp/pf/detail/pp205183)

  -
[Category:成蹊大學校友](../Category/成蹊大學校友.md "wikilink")
[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:SF漫畫家](../Category/SF漫畫家.md "wikilink")
[Category:三重縣出身人物](../Category/三重縣出身人物.md "wikilink")

1.  ISBN 9861169016
2.  ISBN 9861122710
3.  ISBN 9573456958
4.  ISBN 9573456966