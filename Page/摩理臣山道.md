**摩理臣山道**是[香港的一條道路](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[灣仔東部](../Page/灣仔.md "wikilink")，為區內的主要道路。摩理臣山道南接[黃泥涌道及](../Page/黃泥涌道.md "wikilink")[皇后大道東交界](../Page/皇后大道東.md "wikilink")，北接[天樂里及](../Page/天樂里.md "wikilink")[灣仔道交界](../Page/灣仔道.md "wikilink")。與此交匯的道路包括[崇德街](../Page/崇德街.md "wikilink")、[堅拿道](../Page/堅拿道.md "wikilink")、[體育道及](../Page/體育道.md "wikilink")[禮頓道等](../Page/禮頓道.md "wikilink")。摩理臣山道以西為[摩理臣山](../Page/摩理臣山.md "wikilink")，現已夷平。

此路鄰近[香港仔隧道](../Page/香港仔隧道.md "wikilink")[跑馬地出口](../Page/跑馬地.md "wikilink")（北面出口），是[香港島](../Page/香港島.md "wikilink")[南區經該隧道前往](../Page/南區_\(香港\).md "wikilink")[銅鑼灣的主要道路](../Page/銅鑼灣.md "wikilink")。

摩理臣山道以[燈飾店林立而著名](../Page/燈飾.md "wikilink")，是香港主要燈飾店集中地。

## 街名由來

街道以[摩理臣山名命](../Page/摩理臣山.md "wikilink")，而摩理臣山則以[傳教士摩理臣](../Page/傳教士.md "wikilink")（Morrison）父子——[馬禮遜和](../Page/馬禮遜.md "wikilink")[馬儒翰命名](../Page/馬儒翰.md "wikilink")。除了摩理臣山道外，[摩理臣街亦以其命名](../Page/摩理臣街.md "wikilink")。

## 途經著名地點

  - [南洋酒店](../Page/南洋酒店.md "wikilink")
  - [跑馬地馬場](../Page/跑馬地馬場.md "wikilink")
  - [香港銅鑼灣太平洋帆船酒店](../Page/香港銅鑼灣太平洋帆船酒店.md "wikilink")
  - [伊利沙伯體育館](../Page/伊利沙伯體育館.md "wikilink")

[File:HK_Junction_MorrisonHillRoad_TinLokLane.JPG|摩理臣山道與禮頓道之交匯點](File:HK_Junction_MorrisonHillRoad_TinLokLane.JPG%7C摩理臣山道與禮頓道之交匯點)
<File:Morrison> hill road.JPG|摩理臣山道與與崇德街交界 <File:Coiled> Dragon brings
luck (facade).jpg|摩理臣山道的「蟠龍匯瑞」雕像正面 <File:HK> Wan Chai Morrison Hill Road
n Tin Lok Lane.JPG|摩理臣山道的「蟠龍匯瑞」雕像側面

## 參見

  - [黃泥涌峽天橋](../Page/黃泥涌峽天橋.md "wikilink")

## 外部連線

  - [摩理臣山道地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=836612&cy=815165&zm=5&mx=836612&my=815165&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[en:Morrison Hill Road](../Page/en:Morrison_Hill_Road.md "wikilink")

[Category:灣仔街道](../Category/灣仔街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")