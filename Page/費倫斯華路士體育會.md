**費倫斯華路士體育會**（**Ferencvárosi Torna
Club**；**FTC**）一般称之为**费伦茨瓦罗斯**，是匈牙利最著名的运动俱乐部之一，创建于1899年，俱乐部体育场位于[布达佩斯第九区的费伦茨瓦罗斯](../Page/布达佩斯.md "wikilink")。球队队色为绿色和白色，吉祥物是老鹰。

足球队是俱乐部中最大的组成部分，球迷称呼球队为绿色的老鹰（[匈牙利语](../Page/匈牙利语.md "wikilink"):**Zöld
Sasok**）。

该队在匈牙利人中非常知名。球队在国内的主要对手是[乌伊佩斯特](../Page/乌伊佩斯特足球俱乐部.md "wikilink")。其他对手包括[德布雷辛尼VSC](../Page/德布雷辛尼VSC.md "wikilink")，[MTK匈牙利人](../Page/MTK匈牙利人足球俱乐部.md "wikilink")，[基斯佩斯特洪韦德和](../Page/布达佩斯洪韦德足球俱乐部.md "wikilink")[华萨斯SC](../Page/华萨斯SC.md "wikilink")。

球队的官办杂志叫做**100% Fradi**。费伦茨瓦罗斯TC还拥有一支女子手球俱乐部，男子冰球俱乐部，男子水球俱乐部和男子自行车俱乐部。

## 足球

费伦茨瓦罗斯TC参加了从1901年匈牙利足球联赛创建到2006/07赛季的全部赛季的甲级联赛，但是球队由于财政问题在2007赛季前被足协勒令降入第一个级别的乙级联赛中。

费伦茨瓦罗斯是现今为止匈牙利唯一的一支闯入[欧洲冠军联赛小组赛阶段比赛的俱乐部](../Page/欧洲冠军联赛.md "wikilink")，在1995年球队在资格赛中击败了[安德莱赫特](../Page/安德莱赫特足球俱乐部.md "wikilink")，并且击败了[苏黎世草蜢](../Page/苏黎世草蜢足球俱乐部.md "wikilink")，最终球队被[阿贾克斯所淘汰](../Page/阿贾克斯.md "wikilink")。

自从1990年代以来，俱乐部一直存在着[足球流氓并时常伴随着](../Page/足球流氓.md "wikilink")[民族主义](../Page/民族主义.md "wikilink")，[种族歧视和](../Page/种族歧视.md "wikilink")[反犹太主义的不良风气](../Page/反犹太主义.md "wikilink")。

2006年7月，俱乐部由于持续的财政危机被罚以剥夺参加甲级联赛资格。[1](http://sports.yahoo.com/sow/news;_ylt=AkAzfCnXTpZvqKOLv374MfMmw7YF?slug=reu-hungaryferencvaros&prov=reuters&type=lgns)
这也是俱乐部第一次从顶级联赛降级。

2006-07年， 费伦茨瓦罗斯青年队赢得了Foyle杯锦标。

## 球隊榮譽

  - **[匈牙利足球甲級聯賽](../Page/匈牙利足球甲級聯賽.md "wikilink")**
      - '''冠军（29次）¹: ''' 1903, 1905, 1907, 1909, 1910, 1911, 1912, 1913,
        1926, 1927, 1928, 1932, 1934, 1938, 1940, 1941, 1949, 1963,
        1964, 1967, 1968, 1976, 1981, 1992, 1995, 1996, 2001, 2004, 2016

<!-- end list -->

  - **[匈牙利盃](../Page/匈牙利盃.md "wikilink")**
      - '''冠军（23次）¹: ''' 1913, 1922, 1927, 1928, 1933, 1935, 1942, 1943,
        1944, 1958, 1972, 1974, 1976, 1978, 1991, 1993, 1994, 1995,
        2003, 2004, 2015, 2016, 2017

<!-- end list -->

  - **[Mitropa Cup](../Page/Mitropa_Cup.md "wikilink")**
      - 1928, 1937

<!-- end list -->

  - **[國際城市博覽會盃](../Page/國際城市博覽會盃.md "wikilink")**
      - 1965年 1-0 vs. [祖雲達斯](../Page/尤文圖斯足球俱樂部.md "wikilink")

¹ 夺得次数最多的俱乐部

## 著名球员

  - [弗洛里安·阿尔伯特](../Page/弗洛里安·阿尔伯特.md "wikilink")

  - [László Budai](../Page/László_Budai.md "wikilink")

  - [Márton Bukovi](../Page/Márton_Bukovi.md "wikilink")

  - [Zoltán Czibor](../Page/Zoltán_Czibor.md "wikilink")

  - [Jenő Dalnoki](../Page/Jenő_Dalnoki.md "wikilink")

  - [Ferenc Deák](../Page/Ferenc_Deák_\(footballer\).md "wikilink")

  - [Zoltán Ebedli](../Page/Zoltán_Ebedli.md "wikilink")

  - [桑多尔·柯奇什](../Page/桑多尔·柯奇什.md "wikilink")

  -   [László Kubala](../Page/László_Kubala.md "wikilink")

  -  [Rudolph
    Nickolsburger](../Page/Rudolph_Nickolsburger.md "wikilink")

  - [Dezső Novák](../Page/Dezső_Novák.md "wikilink")

  - [Tibor Nyilasi](../Page/Tibor_Nyilasi.md "wikilink")

  - [György Sárosi](../Page/György_Sárosi.md "wikilink")

  - [Imre Schlosser](../Page/Imre_Schlosser.md "wikilink")

  - [Zoltán Varga](../Page/Zoltán_Varga_\(footballer\).md "wikilink")

  - [Zoltan Gera](../Page/Zoltan_Gera.md "wikilink")

  - [西蒙·蒂波尔](../Page/西蒙·蒂波尔.md "wikilink")

  - [彼得·里普塞](../Page/彼得·里普塞.md "wikilink")

  - [克里斯蒂安·利斯泰什](../Page/克里斯蒂安·利斯泰什.md "wikilink")

## 退役球衣

  - 2  [西蒙·蒂波尔](../Page/西蒙·蒂波尔.md "wikilink"), 后卫, 1985-1999

## 外部链接

  - [Official website](http://www.ftc.hu)

  - [Ferencvárosi Torna Club Forum (In
    English)](https://web.archive.org/web/20070311013921/http://www.xtratime.com/forums/showthread.php?t=141969)

  - [Fansite](http://www.fradi.de.vu/) Hungarian, German

  - [Fans of FTC -
    Videoblog](http://www.fanatical.hu/category/ferencvaros/)

  - [Ultra Group
    Site](https://web.archive.org/web/20040414014000/http://www.monsters.hu/)

[Category:匈牙利足球俱乐部](../Category/匈牙利足球俱乐部.md "wikilink")