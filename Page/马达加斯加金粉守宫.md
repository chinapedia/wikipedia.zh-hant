**马达加斯加金粉守宫**（学名：*Phelsuma laticauda
laticauda*）是一种日间活动的[壁虎](../Page/壁虎.md "wikilink")，产于[馬達加斯加北部和](../Page/馬達加斯加.md "wikilink")[科摩羅](../Page/科摩羅.md "wikilink")，棲息在樹木和房屋，以[昆蟲和](../Page/昆蟲.md "wikilink")[花蜜為食](../Page/花蜜.md "wikilink")。

## 描述

这种壁虎属于小型[日行守宫属](../Page/日行守宫属.md "wikilink")，身体总长可达约13厘米。颜色为鲜绿色或黄绿色，甚至很少数为蓝色。其颈部和上背部有黄色的斑点。鼻口部和头部有三条锈色的横纹，上眼皮为蓝色。其下背部有三条稍长的红色纹，尾巴稍平，腹部为灰白色。

马达加斯加金粉守宫栖息在[马达加斯加北部](../Page/马达加斯加.md "wikilink")、[贝岛](../Page/贝岛.md "wikilink")、[科摩罗和](../Page/科摩罗.md "wikilink")[留尼汪岛](../Page/留尼汪.md "wikilink")。根据麦基翁（McKeown）的研究，这个物种还被引入到[塞舌尔](../Page/塞舌尔.md "wikilink")，还有[夏威夷群岛中的](../Page/夏威夷群岛.md "wikilink")[欧胡岛](../Page/欧胡岛.md "wikilink")、[科纳区和](../Page/科纳区.md "wikilink")[茂伊岛](../Page/茂伊岛.md "wikilink")。人们还在[考艾岛的西侧和](../Page/考艾岛.md "wikilink")[夏威夷群岛东端发现了它](../Page/夏威夷群岛.md "wikilink")。

## 饮食

[Gold_dust_day_gecko_at_flower-edit1.jpg](https://zh.wikipedia.org/wiki/File:Gold_dust_day_gecko_at_flower-edit1.jpg "fig:Gold_dust_day_gecko_at_flower-edit1.jpg")
马达加斯加金粉守宫吃各种昆虫和无脊椎动物，也吃其他个体较小的蜥蜴。它们还吃软甜的水果、花粉和花蜜，通常在一棵植物上聚集了很多个马达加斯加金粉守宫。

## 行为

这个物种的雄性相当具有攻击性且好斗。它们不接受其他雄性作为邻居。在人工饲养的情况下，雌性无法逃避，雄性也会严重伤害雌性。在这种情况下，雄性和雌性必须分开。

## 繁殖

雌性最多产5对蛋。在28度的温度时，孵化的时间约为40-45天。未成年的个体长度约为55-60毫米，虽然还没有长大但是已经很好斗，因此它们必须被分开。生长到10-12个月时性成熟。

## 人工饲养

这种动物人工饲养时需单独或成对饲养，并需要一个较大的，种满植物的玻璃容器。白天温度保持在28度，夜间约为20度，湿度保持在65-75%之间。可以喂食[蟋蟀](../Page/蟋蟀.md "wikilink")、蜡蠕虫（蜡蛾）、[果蝇](../Page/果蝇.md "wikilink")、[蛆](../Page/蛆.md "wikilink")、膳食蠕虫和家蝇、果泥。

## 参考资料

  -
  -
  -
[Category:壁虎科](../Category/壁虎科.md "wikilink")