**PlayStation
Portable**（官方[縮寫](../Page/縮寫.md "wikilink")：**PSP**）是[索尼電腦娛樂於](../Page/索尼.md "wikilink")2004年12月12日發售的[掌上型遊戲機](../Page/掌上型遊戲機.md "wikilink")。2007年9月，輕量、薄型化的[PSP-2000系列發售](../Page/PlayStation_Portable_Slim_&_Lite.md "wikilink")。2008年10月，提升液晶螢幕鮮豔及對比度並內建麥克風的PSP-3000系列發售。2009年，[PSPgo發售](../Page/PSPgo.md "wikilink")。2014年PSP停產，在十年的全壽命周期中，售出8200萬部。PSP停產之後，由2011年開始發售的[PlayStation
Vita取代](../Page/PlayStation_Vita.md "wikilink")。

## 簡介

採用最大擁有1.8GB容量的[UMD](../Page/通用媒體光碟.md "wikilink")、4.3英寸的液晶螢幕和理論值達32GB容量（目前已达32GB，可以利用TF卡加"卡套"进行更大扩充）的[Memory
Stick
Duo介面](../Page/Memory_Stick.md "wikilink")，可播放影片與音樂。經由[WLAN機能以](../Page/WLAN.md "wikilink")[IEEE
802.11b](../Page/IEEE_802.11b.md "wikilink")（11Mbit/s工作在2.4 GHz）連結上[瀏覽器與](../Page/網頁瀏覽器.md "wikilink")[LocationFree](../Page/LocationFree.md "wikilink")、[PS3以及](../Page/PlayStation_3.md "wikilink")[USB](../Page/通用序列匯流排.md "wikilink")
port等擴張性，透過[更新系統軟體能追加各式的機能為其特徵](../Page/PlayStation_Portable系統軟體.md "wikilink")。

精巧並有著雙層加工過的透明感與富含光澤的外觀設計機體上搭載著由[SHARP開發的大型ASV液晶螢幕](../Page/夏普.md "wikilink")。在PSP發售以前家用機和攜帶型遊戲機間有一定的性能差異，特別是於3D圖形的表現上幾乎是家用機獨佔優勢，而如今卻可讓擁有不比家用機遜色繪圖性能的遊戲也能夠於攜帶型遊戲機上遊玩。但有著高性能的同時也被指出採用對於隨機存取較弱的[CLV式](../Page/CLV.md "wikilink")[光碟媒體所需的讀取時間](../Page/光碟.md "wikilink")、電池持續時間短、機體表面的脆弱性等等問題（關於讀取部份因軟體製作公司的技術也存在著而可於短時間內處理完的軟體）。詳細性能參見[主要規格](../Page/#主要規格.md "wikilink")。

雖然有冠上「[PlayStation](../Page/PlayStation.md "wikilink")」（PS）的名號卻與PS、PS2等不同，並無互換性（但是，關於PS部分[系統軟體版本](../Page/PlayStation_Portable系統軟體.md "wikilink")3.00後加入了可執行[PLAYSTATION
Store下載方式所販售PlayStation軟體的](../Page/PlayStation_Network.md "wikilink")[模擬器](../Page/家用機模擬器.md "wikilink")）。

从PSP的发明到现在为止，很少有顶级和着名的游戏。许多人投票选择了合金装备：Peace
Walker和Lumines作为PSP的最佳游戏。\[1\]

### 发行历史

[PlayStation_Portable_booth,_Tokyo_Game_Show_20040926.jpg](https://zh.wikipedia.org/wiki/File:PlayStation_Portable_booth,_Tokyo_Game_Show_20040926.jpg "fig:PlayStation_Portable_booth,_Tokyo_Game_Show_20040926.jpg")上初次參展的PSP\]\]

  - 2007年9月20日，輕量化、薄型化的「[PSP-2000](../Page/PSP-2000.md "wikilink")」（PSP
    Slim／薄型PSP）於日本上市。新型號的主要優點包括：比原來的型號輕33%，亮33%，薄19%；更快的遊戲載入；視頻信號輸出端子；記憶體升至64MB；更多的機身顏色。[索尼電腦娛樂宣佈現有的PSP](../Page/索尼電腦娛樂.md "wikilink")（PSP-1000）將會停止生產。之後又出（PSP-3000）

<!-- end list -->

  - 2008年1月7日，索尼於本年度「[國際消費電子展](../Page/國際消費電子展.md "wikilink")」（CES）宣佈，新版PSP娛樂系統（PSP-2000
    series）中將加入「[Skype](../Page/Skype.md "wikilink")」功能，預計將有超過246百萬戶登記用戶有機會使用此項軟件。新型PSP用家可隨時隨地使用經濟又方便的Skype免費任打網上電話，進行PSP與PSP或PSP與PC用戶的溝通。同日，[日本索尼電腦娛樂宣佈](../Page/日本索尼電腦娛樂.md "wikilink")，將於日本地區首先推出PSP專用USB外接麥克風作單獨販售，支援Skype網路語音通話。

<!-- end list -->

  - 2008年1月8日，索尼於CES第二日展期中宣佈，將會推出利用[PlayStation
    3拷貝藍光光碟影片並傳輸給PSP的功能](../Page/PlayStation_3.md "wikilink")，使用者通過這種方式就可以享受可擕式的高清電影。

<!-- end list -->

  - 2008年1月9日，索尼發表將於2月28日在日本地區「花系列（Blume
    Series）」中推出新配色，款式為新型PSP主機「薄荷綠（Mint
    Green）」。

<!-- end list -->

  - 2008年4月16日，SCET發表將於4月24日在[台灣](../Page/台灣.md "wikilink")「花系列（Blume
    Series）」中推出新配色，款式為新型PSP主機「消光銅」。

<!-- end list -->

  - 2008年8月20日，新一代改進設計的3000型PSP在Leipzig的媒體大會上被宣佈。[PSP-3000的設計基於PSP](../Page/PSP-3000.md "wikilink")-2000，其包括有增強的防反射光線技術的LCD顯示屏、一個內置的麥克風以及兼容隔行掃瞄的視頻輸出。該款型號的PSP預定於2008年10月在日本、亞洲地區、北美以及歐洲開始發行。

<!-- end list -->

  - 2008年10月14日，索尼宣佈PSP-3000正式發售。[索尼電腦娛樂宣佈現有的PSP](../Page/索尼電腦娛樂.md "wikilink")（PSP-2000）將會停止生產。

<!-- end list -->

  - 2011年1月27日，索尼在PlayStation®Meeting 2011上宣布下一代代号为“NGP”的便携式娱乐系统。\[2\]
  - 2011年8月17日，索尼在[Gamescom上宣布将发售一款不带wifi](../Page/Gamescom.md "wikilink")，重新设计的PSP，型号为PSP
    E-1000，只在欧洲的PAL制式地区销售，售价99欧元。
  - 2014年6月4日，索尼正式宣布PSP停產。

## 功能

[Pursuit_Force_UMD_(front).jpg](https://zh.wikipedia.org/wiki/File:Pursuit_Force_UMD_\(front\).jpg "fig:Pursuit_Force_UMD_(front).jpg")（正面）\]\]
[Pursuit_Force_UMD_(back).jpg](https://zh.wikipedia.org/wiki/File:Pursuit_Force_UMD_\(back\).jpg "fig:Pursuit_Force_UMD_(back).jpg")（背面）\]\]
[PSP_med_UMD_luckan_öppen.JPG](https://zh.wikipedia.org/wiki/File:PSP_med_UMD_luckan_öppen.JPG "fig:PSP_med_UMD_luckan_öppen.JPG")插口\]\]
[Universal_Media_Disc_logo.png](https://zh.wikipedia.org/wiki/File:Universal_Media_Disc_logo.png "fig:Universal_Media_Disc_logo.png")標誌\]\]

### 遊戲

  - PSP使用UMD作為遊戲儲存媒介，放入UMD並在遊戲選項中啟動UMD，即可以進行遊戲。PSP的游戏是不锁区的。
      - 除了實體UMD光碟之外，於PlayStation
        Store也有販售PSP專用下載版的遊戲軟體（試玩版免費，完整版要付費），以及提供[PlayStation的經典遊戲付費下載](../Page/PlayStation.md "wikilink")。
      - 下載版遊戲需儲存於另售的Memory Stick記憶卡，可直接透過MS卡啟動遊戲。
          - 也另外可以改機直接下載遊戲

### 外接畫面輸出

  - PSP-2000型（含）後的PSP，可將主機畫面輸出至電視（無法同步顯示）。
  - 有AV、S端子、色差與HDMI四種介面。

### 媒體播放

能播放及閱覽存放在[UMD-VIDEO及](../Page/通用媒體光碟.md "wikilink")[Memory
Stick裡的影片檔](../Page/Memory_Stick.md "wikilink")、音樂與圖片。

#### 影片

  - [MPEG4](../Page/MPEG4.md "wikilink")
      - [MPEG4](../Page/MPEG4.md "wikilink")、[H.264/MPEG-4
        AVC等編碼格式能在官方](../Page/H.264/MPEG-4_AVC.md "wikilink")[XMB裡播放](../Page/Cross_Media_Bar.md "wikilink")
      - 320x240、352x240、480x272、720x480、720x576等[解像度都能播放](../Page/解像度.md "wikilink")
  - （5.00固件增加640×480分辨率支持）
  - （5.50固件增加720x576分辨率支持30fps以下）
      - 30fps、Level3、\>2000kbps（從6.20版本開始支援Main@L4.0/Main@L5.0畫質格式）
  - [AVI](../Page/AVI.md "wikilink")
      - [MJPEG編碼格式能在官方XMB裡播放](../Page/MJPEG.md "wikilink")
      - 參數同上

#### 音樂

  - [AAC](../Page/AAC.md "wikilink")、以[AAC編碼的其他檔案如](../Page/AAC.md "wikilink")[3GP](../Page/3GP.md "wikilink")
  - [MP3](../Page/MP3.md "wikilink")
  - [WAV](../Page/WAV.md "wikilink")、[PCM](../Page/PCM.md "wikilink")
  - [ATRAC](../Page/ATRAC.md "wikilink")、[ATRAC3
    Plus](../Page/ATRAC.md "wikilink")
  - [WMA](../Page/WMA.md "wikilink")
      - 不支援[WMA9
        Professional](../Page/WMA9_Professional.md "wikilink")、[WMA9
        Lossless與對應著作權保護機能的WMA檔案](../Page/WMA9_Lossless.md "wikilink")
      - 需先接駁WiFi網絡以啟動[WMA播放功能](../Page/WMA.md "wikilink")

#### 圖片

  - [JPEG](../Page/JPEG.md "wikilink")
  - [TIFF](../Page/TIFF.md "wikilink")
  - [BMP](../Page/BMP.md "wikilink")
  - [PNG](../Page/PNG.md "wikilink")
  - [GIF](../Page/GIF.md "wikilink")（需在Internet Browser內開啟）

#### 動畫

  - [Adobe Flash](../Page/Adobe_Flash.md "wikilink") 6.0
      - PSP-1000需先接駁WiFi網絡以啟動[Adobe Flash
        Lite](../Page/Adobe_Flash_Lite.md "wikilink")
      - 僅可於Internet Browser內開啟
      - 流量和體積有一定限制
      - 功能不完整
  - [Java](../Page/Java.md "wikilink")
      - 支援Java及JavaScript
      - 流量和體積有一定限制

### 無線網路連線

PSP能透過[WiFi和他台PSP與個人電腦的無線接取器等連上](../Page/WiFi.md "wikilink")。有[Ad
Hoc模式和infrastructure模式](../Page/Ad_Hoc.md "wikilink")。[PS3-PSP_Net.jpg](https://zh.wikipedia.org/wiki/File:PS3-PSP_Net.jpg "fig:PS3-PSP_Net.jpg")

#### 網路瀏覽

系統軟體版本2.00開始可連結上[網際網路閱覽網頁](../Page/網際網路.md "wikilink")，瀏覽器是採用[愛可信的](../Page/愛可信.md "wikilink")[NetFront](../Page/NetFront.md "wikilink")。

  - 保存書籤
  - 變更文字大小
  - 變更顯示的文字編碼
  - 智能頁面分割
  - 顯示履歷
  - 支援[JavaScript](../Page/JavaScript.md "wikilink")
  - [代理伺服器的使用](../Page/代理伺服器.md "wikilink")
  - 檔案的上傳和下載
  - 顯示[Flash內容](../Page/Adobe_Flash.md "wikilink")
      - 系統軟體版本2.70開始支援
      - 必須要由\[主機設定\]-\[啟動FlashPlayer\]接受認證
          - PSP-2000系列以後機種不需認證
      - 不支援[YouTube等](../Page/YouTube.md "wikilink")[FLV類型的影片網站](../Page/Flash_Video.md "wikilink")
  - 分頁瀏覽功能

有著紅、藍、橙色3個分頁，可同時開啟3個網頁。以按住□鈕再按L鈕及R鈕來切換分頁。

#### 網上電台

系統軟體版本3.80開始可收聽[網上電台](../Page/網路電台.md "wikilink")。想要收聽必須事先將廣播電台或個人所提供的PSP專用網上電台播放器登錄至Memory
Stick。

### UMD影片

能觀看提供給[UMD-VIDEO的影像軟體](../Page/通用媒體光碟.md "wikilink")。由日本的『[最终幻想VII：降临之子](../Page/最终幻想VII：降临之子.md "wikilink")』開始，世界不少廠商及電影公司陸續推出，價格與DVD相仿。有包括電影、電視劇、音樂影片等多種類型。大部分推出的影片都能提供多語言選擇，以及內容、字幕選擇和特別花絮。（日本版還提供更電子化的包含UMD/DVD兩種介質的套裝\[3\]，另外在日本還有大量UMD格式的成人影片）。

第一張UMD介質的電影在2005年4月發行。最早一批在北美發售的目錄包括[索尼電影公司的](../Page/Sony_Pictures.md "wikilink")《[十面埋伏](../Page/十面埋伏.md "wikilink")》、《[極限特工](../Page/XXX_\(movie\).md "wikilink")》、《[地獄男孩](../Page/Hellboy.md "wikilink")》、《[墨西哥往事](../Page/Once_Upon_a_Time_in_Mexico.md "wikilink")》、《[蜘蛛人2](../Page/蜘蛛人2.md "wikilink")》和《[-{zh-hans:生化危机2：启示录;zh-hant:惡靈古堡2：啟示錄}-](../Page/惡靈古堡.md "wikilink")》，還有[米拉麥克斯公司的](../Page/米拉麥克斯.md "wikilink")《[追殺比爾](../Page/追殺比爾.md "wikilink")》以及迪士尼公司的《[加勒比海盜](../Page/加勒比海盜.md "wikilink")》，以及[周杰倫](../Page/周杰倫.md "wikilink")、[黃秋生](../Page/黃秋生.md "wikilink")、[陳冠希](../Page/陳冠希.md "wikilink")、[鈴木杏主演的](../Page/鈴木杏.md "wikilink")《頭文字D》。

2005年6月22日，：-{zh-hans:索尼;zh-hant:索尼}-宣布《十面埋伏》和《生化危機2》各售出超過100萬部。

2006年3月，Universal和Image Entertainment宣佈停止對UMD格式的支援。

2006年4月12日，一篇在The Digital Bits網站的文章顯示Warner Home
Video取消預計發行的UMD格式電影。而Paramount也停止對UMD格式的支援。

## 特性

### 控制

PSP比以前Play Station系列新增影片和音樂播放能力，PSP的操控設計繼續以遊戲為基礎。基本上按鍵均以Play
Station的手把一體的方式而設計：

  - 兩個上部按鈕（L、R）
  - 四个PlayStation式指令键（○、×、□、△）
  - 一個十字型八向按鍵（↑、↓、←、→）
  - 一個模擬輸入控制板，也被稱為“類比墊”，其設計並不是傳統的模擬[搖桿](../Page/搖桿.md "wikilink")，而是一個可滑動的扁平控制板。在遊戲中它能提供與傳統模擬搖桿相似的控制，在網絡瀏覽器裡，更可以作模擬滑鼠游標用。

屏幕下方有一排控制鍵，包括：

  - 可調節音量（VOLUME +/-）
  - 音樂設置鍵（音樂符號），可在遊戲中長按控制音效開／關；非遊戲時選擇不同的預設平衡器
  - 螢幕亮度（方型），可在聽音樂時壓此鍵維持2秒左右，使螢幕變黑屏以省電。
  - PlayStation式的開始和選擇鍵（START、SELECT）
  - 主選單鍵（Home），能離開遊戲回到主選單（HOME）。（3000型將Home按鈕改成PlayStation圖示按鈕）

### 睡眠功能

電源按鈕短暫的往上扳一下，可以使遊戲以現在的狀態暫時中斷，並將資料以低電壓供電儲存在記憶體中。再次啟動只需約1～2秒開啟後仍保持前一次的關機前的畫面。睡眠時耗電量極少。也可設定過一定時間未使用即自動睡眠。另外，充電量少於1%時會自動進入睡眠來節省電池，但如果電池電力完全用完則會切斷電源回到主畫面。

## 型号

## 主要規格

### PSP-1000

  - [MIPS](../Page/MIPS.md "wikilink") 32bit內核R4000×2)，1～333 MHz
  - [FLOPS](../Page/FLOPS.md "wikilink")：2.6Gflops（333MHz運作時）
  - [Cache](../Page/Cache.md "wikilink")：L1 32KB
  - 170 x 33 x 74公厘（長x高x寬）
  - 主[記憶體](../Page/記憶體.md "wikilink")：32MB
  - 自帶[DRAM](../Page/DRAM.md "wikilink")：4MB
  - 4.3英寸[SHARP製寬螢幕](../Page/夏普.md "wikilink")[ASV](../Page/Advanced_Super_View.md "wikilink")
    [LCD](../Page/LCD.md "wikilink")
  - 480×272 [像素](../Page/像素.md "wikilink")（16：9）、1,677萬色
  - 最大亮度約400[cd](../Page/燭光.md "wikilink")／m<sup>2</sup>；官方韌體鎖定為200/180/130/80
    [cd](../Page/燭光.md "wikilink")／m<sup>2</sup>四個亮度
  - 內建[立體聲揚聲器](../Page/立體聲.md "wikilink")
  - [IEEE
    802.11b標準](../Page/IEEE_802.11b.md "wikilink")（[Wi-Fi](../Page/Wi-Fi.md "wikilink")）
  - [USB 2.0](../Page/USB_2.0.md "wikilink") High Speed mini-B Type
  - [Memory Stick PRO
    Duo](../Page/Memory_Stick.md "wikilink")（[MagicGate](../Page/MagicGate.md "wikilink")・高速傳送）
  - [紅外線通訊](../Page/紅外線.md "wikilink")（[IrDA](../Page/紅外通訊技術.md "wikilink")）
  - 外部電源供給端子
  - 本體電源輸入端子（[鋰離子電池](../Page/鋰離子電池.md "wikilink")）
  - 耳機／麥克風端子
  - 播放專用[UMD光碟機](../Page/UMD.md "wikilink")
  - 內含[鋰離子電池](../Page/鋰離子電池.md "wikilink")（3.6V/1,800mAh。也有另售2,200mAh）
  - PSP專用外接電源（5V DC，使用AC 100～240V轉DC 5V）
  - 約280g（含電池）

### PSP-2000

[PSP2000_UMDPlacement.jpg](https://zh.wikipedia.org/wiki/File:PSP2000_UMDPlacement.jpg "fig:PSP2000_UMDPlacement.jpg")的PSP-2000\]\]

  - CPU同上
  - 170 x 18.6 x 74公厘（長x高x寬）
  - 主[記憶體](../Page/記憶體.md "wikilink")：64MB（較PSP-1000多出32MB，能縮短UMD選取次數（配合UMD
    CACHE功能）、)
  - 4.3英寸寬螢幕[TFT液晶](../Page/薄膜電晶體.md "wikilink")
  - 480×272 [像素](../Page/像素.md "wikilink")（16：9）、1,677萬色
  - 最大亮度約400[cd](../Page/燭光.md "wikilink")／m<sup>2</sup>；官方韌體鎖定為200/180/130/80
    [cd](../Page/燭光.md "wikilink")／m<sup>2</sup>四個亮度
  - 內建[立體聲揚聲器](../Page/立體聲.md "wikilink")
  - [IEEE
    802.11b標準](../Page/IEEE_802.11b.md "wikilink")（[Wi-Fi](../Page/Wi-Fi.md "wikilink")）
  - [USB 2.0](../Page/USB_2.0.md "wikilink") High Speed mini-B Type
  - [Memory Stick PRO
    Duo](../Page/Memory_Stick.md "wikilink")（[MagicGate](../Page/MagicGate.md "wikilink")・高速傳送）
  - 類比影像輸出（進行遊戲必需使用色差或D端子）
  - 外部電源供給端子
  - 本體電源輸入端子（[鋰離子電池](../Page/鋰離子電池.md "wikilink")）
  - 耳機／麥克風端子
  - 播放專用[UMD光碟機](../Page/UMD.md "wikilink")
  - 內含[鋰離子電池](../Page/鋰離子電池.md "wikilink")（3.6V/1,200mAh）
  - PSP專用外接電源（100～240V）
  - 約189g（含電池）

### PSP-3000

PSP-3000，是[索尼电脑娱乐](../Page/索尼电脑娱乐.md "wikilink")（SCEI）[PlayStation
Portable Slim &
Lite的升级版](../Page/PlayStation_Portable_Slim_&_Lite.md "wikilink")，[2008年10月](../Page/2008年10月.md "wikilink")14至16日在全球不同地区分别上市。\[4\]
[PSP-3007_VB_face.jpg](https://zh.wikipedia.org/wiki/File:PSP-3007_VB_face.jpg "fig:PSP-3007_VB_face.jpg")

  - MIPS 32bit内核R4000×2)，1～333 MHz
  - FLOPS：2.6Gflops（333MHz运转时）
  - Cache：L1 32k
  - 170 x 18.6 x 74公釐（長x高x寬）
  - 主记忆体：64MB
  - 4.3英寸宽屏幕TFT液晶
  - 480×272像素（16：9）、1,677万色
  - 内置立体声扬声器
  - IEEE 802.11b标准（Wi-Fi）
  - USB2.0 High Speed mini-B Type
  - Memory Stick PRO Duo（MagicGate・高速传送）
  - 模拟图像输出
  - 外部电源供给端子
  - 本体电源输入端子（锂离子电池）
  - 图像／耳机／麦克风端子
  - 麦克风
  - 播放专用UMD光驱
  - 内含锂离子电池（3.6V/1,200mAh）
  - PSP专用外接电源（100～240V）
  - 约189g（含电池）\[5\]

## 軟體

  - [XMB](../Page/Xross_Media_Bar.md "wikilink")（Cross Media
    Bar）可以直覺性的操作
      - 音樂
      - 影片
      - 圖片
  - 安全技術
      - 128位AES加密
  - 訪問控制
      - 區域碼
      - 年齡限制
  - 無線通信功能
      - Infrastructure mode（通過無線基台進行多部PSP的連接）
      - Ad hoc mode（最大16部PSP之間直接互相連接）
  - 搭載[愛可信製網頁瀏覽器](../Page/愛可信.md "wikilink")[NetFront](../Page/NetFront.md "wikilink")。
  - 搭載[Adobe Systems製](../Page/Adobe_Systems.md "wikilink")[Macromedia
    Flash Player](../Page/Macromedia_Flash_Player.md "wikilink") 6.0。
  - 搭載安全性軟體RSA Security製RSA BSAFE。
  - 可從瀏覽器中對記憶卡上的“.txt”，“.htm”及“.html”類型文件進行瀏覽（輸入方式為“file:/\*.txt”）。
  - 搭載[R∞M功能](../Page/R∞M.md "wikilink")（PlayStation
    Portable上的社區空間，和PlayStation Home功能相似）

## 周邊附屬配件

[PSPs.jpg](https://zh.wikipedia.org/wiki/File:PSPs.jpg "fig:PSPs.jpg")
[PSP-Accessories-MSPRODuo-32MB&1GB.JPG](https://zh.wikipedia.org/wiki/File:PSP-Accessories-MSPRODuo-32MB&1GB.JPG "fig:PSP-Accessories-MSPRODuo-32MB&1GB.JPG")

### SCEI製

#### PSP-1000用

  - 電池組
      - PSP-110 DC 3.6V 1800mAh（與PSP-1000主機附屬的相同）
      - PSP-280G DC 3.6V 2200mAh
  - 電池充電器PSP-190
  - 線控裝置與耳機（浮雲白PSP-140G）

#### PSP-2000、PSP-3000两用

  - 電池組
      - PSP-S110G DC 3.6V 1200mAh（與PSP-2000主機附屬的相同）
  - 線控裝置與耳機PSP-S140G
  - 類比輸出用接線
      - 影像色差連接線（遊戲的映像輸出必須要有支援循序掃描的電視）
          - PSP-S180 [色差連接線](../Page/色差端子.md "wikilink")、約2.5m

#### PSP-1000、PSP-2000、PSP-3000通用

  - 交流電轉換器（與PSP主機附屬的相同）
  - 車用轉接器PSP-180G
  - 吊帶

<!-- end list -->

  - [Memory Stick Duo](../Page/Memory_Stick_Duo.md "wikilink")
      - Memory Stick PRO Duo 1GB\\2GB\\4GB\\8GB\\16GB\\32G（更新3.8版本後可用）
      - Memory Stick Duo 32MB
  - PSP專用充電器（PSP-192T，台灣專用型)
  - 附屬收藏包
  - 擦拭布

### 地区版本

  - PSP-X000 [日本](../Page/日本.md "wikilink")
  - PSP-X001 [北美](../Page/北美.md "wikilink")
  - PSP-X002
    [澳大利亚](../Page/澳大利亚.md "wikilink")/[新西兰](../Page/新西兰.md "wikilink")
  - PSP-X003 [英国](../Page/英国.md "wikilink")
  - PSP-X004 [欧洲](../Page/欧洲.md "wikilink")
  - PSP-X005 [韩国](../Page/韩国.md "wikilink")
  - PSP-X006
    [香港](../Page/香港.md "wikilink")/[马来西亚](../Page/马来西亚.md "wikilink")/[新加坡](../Page/新加坡.md "wikilink")
  - PSP-X007 [臺灣](../Page/臺灣.md "wikilink")
  - PSP-X008 [俄罗斯](../Page/俄罗斯.md "wikilink")
  - PSP-X009 [中國大陸](../Page/中國大陸.md "wikilink")
  - PSP-X010 [墨西哥](../Page/墨西哥.md "wikilink")
  - PSP-X011
  - PSP-X012

### SONY製

  - [LocationFree](../Page/LocationFree.md "wikilink") Base Station
    (LF-PK1)
      - 連結上Base Station可以使PSP以LocationFree Player的方式來活用。

### 原廠以外的主要商品

  - 液晶保護膜
    由各廠商發售的液晶保護用膜，如[HORI](../Page/HORI.md "wikilink")。於電玩店等販售。因PSP的畫面易沾上指紋及灰塵加上販賣PSP主機利潤低的關係（這是遊戲機整體的狀況，不限於專指PSP）有標註上「請使用保護膜」的電玩店也不少。依商品的不同而有透明度、影像清晰度、視角、易貼度等等差異，挑選時有必要注意且PSP不適應高溫多濕而有會使機械的壽命縮短，最糟的情況下有破損的可能。
  - Memory Stick
    由[SanDisk](../Page/SanDisk.md "wikilink")、[Lexar等販售](../Page/Lexar.md "wikilink")。

除此之外還有販售堅固的EVA製的收納包、大容量電池和UMD收納盒等的產品。

## 缺點

  - 由於PSP的摇杆較為脆弱，不少玩家也有摇杆斷裂的情況。
  - PSP螢幕有入塵問題，而其UMD外表上的金属環也容易被刮傷。
  - PSP1000型的部分機器存在飛盤問題。

## 参见

  - [PlayStation
    Portable系統軟體](../Page/PlayStation_Portable系統軟體.md "wikilink")
  - [UMD](../Page/Universal_Media_Disc.md "wikilink")
  - [PlayStation Vita](../Page/PlayStation_Vita.md "wikilink")
  - [攜帶型遊戲機](../Page/攜帶型遊戲機.md "wikilink")

## 參考文獻

## 外部連結

  - [PlayStation Asia](http://asia.playstation.com/) **（亞洲語言）**

[Category:PlayStation
Portable](../Category/PlayStation_Portable.md "wikilink")
[Category:索尼遊戲機](../Category/索尼遊戲機.md "wikilink")
[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink")
[Category:数字音频播放器](../Category/数字音频播放器.md "wikilink")
[Category:第七世代遊戲機](../Category/第七世代遊戲機.md "wikilink")
[Category:2004年面世的產品](../Category/2004年面世的產品.md "wikilink")
[Category:好設計獎](../Category/好設計獎.md "wikilink")

1.
2.
3.
4.  [1](http://psp.tgbus.com/pspgame/psp3000/)
5.  [2](http://psp.tgbus.com/news/200808/20080821102420.shtml)