**Last.fm**是一个以[英国为总部的](../Page/英国.md "wikilink")[網路電台和音乐社区](../Page/網路電台.md "wikilink")。有遍布232个国家超过1500万的活跃听众。2007年被[CBS互動以](../Page/CBS互動.md "wikilink")2.8亿美元价格收购，是目前歐洲Web
2.0網站中成交價最高的紀錄。

Last.fm使用的音樂推薦方式稱為「Last.fm
Scrobbler」，該系統提供安裝在使用者的電腦播放程式（支持[iTunes](../Page/iTunes.md "wikilink")、[Winamp](../Page/Winamp.md "wikilink")、[Windows
Media
Player](../Page/Windows_Media_Player.md "wikilink")、[Foobar2000等](../Page/Foobar2000.md "wikilink")）或隨身聽裝置的[外掛程式](../Page/外掛程式.md "wikilink")，記錄（scrobbling）使用者收聽的每一首歌（線上串流電台或本地音樂檔案）的資訊，傳送回Last.fm資料庫，並以其建立該使用者的個人音樂品味，顯示在該站提供予使用者的個人網頁上。該站亦提供多種[社交網路服務](../Page/社交網路服務.md "wikilink")，可讓使用者推薦或收聽合其喜好的音樂。

使用者可在Last.fm的音樂資料庫中自訂音樂電台與播放清單，但不能自由選擇收聽的樂曲，也不能在未經音樂版權所有人事先同意的前提下下載音樂。使用者需要在Last.fm網站註冊以獲得可使用的個人檔案，但瀏覽該站或收聽電台並不需要任何帳號。目前，Last.fm网络音乐电台已宣布将实施收费制度，但德国、英国和美国用户除外\[1\]。

2013年1月15日起，Last.fm仅在美国、英国、德国、加拿大、爱尔兰、澳大利亚、新西兰、巴西提供网络音乐电台服务，其它地区仍然可以使用除网络音乐电台之外的其它功能。\[2\]

Last.fm在中国大陆能够正常访问，但所有的视频都无法播放。

## 歷史

現在的Last.fm網站是由Audioscrobbler與Last.fm兩個獨立來源，在2005年合併而成的。
[缩略图](https://zh.wikipedia.org/wiki/File:Martin_Stiksel.jpg "fig:缩略图")

Audioscrobbler原本是英國[南安普頓大學學生理查德](../Page/南安普頓大學.md "wikilink")·琼斯（Richard
Jones）的[電腦科學專題](../Page/電腦科學.md "wikilink")。琼斯開發了第一版的外掛程式，並在夠多的播放程式能跨[平台獲得支援後釋出一套](../Page/作業系統.md "wikilink")[API予社群使用](../Page/API.md "wikilink")。此時Audioscrobbler的紀錄限於已註冊、受允使用榜單與協作篩檢功能的電腦上。

[缩略图](https://zh.wikipedia.org/wiki/File:Felix_Miller.jpg "fig:缩略图")
Last.fm是由Felix Miller、Martin Stiksel、Michael Breidenbruecker與Thomas
Willomitzer在2002年建立的，該網站提供網路電台與音樂社群功能，利用相似的音樂檔案產生動態播放清單。使用者可利用其所提供的「Love」（喜愛）與「Ban」（封禁）按鈕自訂他們的個人檔案。Last.fm贏得2002年[Europrix多媒體獎](../Page/Europrix.md "wikilink")，並被提名為2003年[電子藝術大獎](../Page/電子藝術大獎.md "wikilink")。

Audioscrobbler與Last.fm的工作團隊逐漸合作，並共同搬至位在[倫敦](../Page/倫敦.md "wikilink")[白教堂的同一辦公室](../Page/白教堂.md "wikilink")。2003年Last.fm完成與Audioscrobbler的音樂資料整合：資料可以利用Audioscrobbler的外掛套件或Last.fm的網路電台輸入。兩站也分享許多大同小異的社群論壇。

2005年8月9日，Audioscrobbler的[舊站域名](http://audioscrobbler.com)結合至Last.fm網站；9月5日另外建立了開發取向的[新站](http://cn.last.fm/api)。[缩略图](https://zh.wikipedia.org/wiki/File:Last.fm-software.png "fig:缩略图")

2006年7月14日該站更新，其中包括推出一套應用程式，可播放Last.fm電台與記錄其他播放程式的播放紀錄。其他的更新包括：交友系統更新為雙向互動式；推出Last.fm控制台（Dashboard），使用者可在其中看到與自己專頁相關的資訊；增強線上購買音樂的功能；網站佈景翻新（另外新增黑色版面）等。[缩略图](https://zh.wikipedia.org/wiki/File:LastFMScrobbler.png "fig:缩略图")

社群對新的佈景褒貶不一，對新增功能的看法也趨向兩極化：部份人樂見社交功能提昇，其他人則認為應該先處理技術問題（例如與[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")
7不相容與其他程式臭蟲等等）。該站每日需接受1千萬筆曲目的記錄，尖峰時間時資料庫負荷過重，造成個人榜單等資訊更新不夠快。

目前該站語言版本包括[英](../Page/英語.md "wikilink")、[德](../Page/德語.md "wikilink")、[西](../Page/西班牙語.md "wikilink")、[法](../Page/法語.md "wikilink")、-{zh-hans:[意](../Page/意大利语.md "wikilink");zh-hant:[意](../Page/意大利語.md "wikilink");zh-tw:[義](../Page/義大利語.md "wikilink");}-、[日](../Page/日語.md "wikilink")、[波](../Page/波蘭語.md "wikilink")、[葡](../Page/葡萄牙語.md "wikilink")、[瑞](../Page/瑞典語.md "wikilink")、[土](../Page/土耳其語.md "wikilink")、[俄](../Page/俄語.md "wikilink")、[簡中等多種語言](../Page/簡體中文.md "wikilink")。

2006年12月，Last.fm與[EMI合作推出Tuneglue](../Page/EMI.md "wikilink")-Audiomap。

2006年10月，該站獲得BT數位音樂獎之最佳音樂社群網站，並於2007年1月獲提名為[NME獎最佳網站](../Page/新音樂快遞.md "wikilink")。

2007年4月，謠言指出[Viacom有意以](../Page/Viacom.md "wikilink")4.5億[美元收購Last](../Page/美元.md "wikilink").fm。\[3\]

2007年5月，[Channel
4電台宣佈將推出一個叫](../Page/Channel_4電台.md "wikilink")「世界排行榜」（Worldwide
Chart）的節目，該節目內容是反映全球Last.fm使用者的收聽趨勢。另外，也有計畫推出能讓使用者自訂個人化影片頻道的影片區。

2007年5月30日，新聞發佈Last.fm已被[CBS以](../Page/CBS.md "wikilink")1.4億[歐元收購](../Page/歐元.md "wikilink")，管理團隊不做變更。\[4\]

2009年3月25日，Last.fm网络音乐电台宣布将实施收费制度，居住在德国、英国和美国地區之外的用户必須在試聽過後每個月繳交3美元才能繼續收聽\[5\]。

2013年1月15日，Last.fm在全球除了美国、英国、德国、加拿大、爱尔兰、澳大利亚、新西兰、巴西以外的地区取消网络音乐电台服务。美国、英国、德国地区的非付费用户只能通过网页服务收听免费电台，桌面客户端以及移动客户端的网络音乐电台服务必须付费订阅后才可使用。\[6\]

## 参考文献

## 外部链接

  - Last.fm首页
      - [](http://www.last.fm/)
      - [](http://cn.last.fm/)

[Category:虛擬社群](../Category/虛擬社群.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:網絡電台](../Category/網絡電台.md "wikilink")
[Category:2002年英國建立](../Category/2002年英國建立.md "wikilink")

1.

2.  <http://www.last.fm/announcements/radio2013> Radio changes to
    Last.fm from Tuesday 15 January 2013

3.

4.

5.
6.  <http://www.last.fm/announcements/radio2013> Radio changes to
    Last.fm from Tuesday 15 January 2013