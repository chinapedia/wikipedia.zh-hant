[Cristatusaurus_claw.jpg](https://zh.wikipedia.org/wiki/File:Cristatusaurus_claw.jpg "fig:Cristatusaurus_claw.jpg")
**脊飾龍屬**（[學名](../Page/學名.md "wikilink")：*Cristatusaurus*）是[棘龍科](../Page/棘龍科.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生活於[下白堊紀時期的](../Page/下白堊紀.md "wikilink")[非洲](../Page/非洲.md "wikilink")。

牠的[化石是於](../Page/化石.md "wikilink")1973年在[非洲](../Page/非洲.md "wikilink")[尼日被發現](../Page/尼日.md "wikilink")。這些化石與[重爪龍及](../Page/重爪龍.md "wikilink")[似鱷龍的很相似](../Page/似鱷龍.md "wikilink")。[模式種是](../Page/模式種.md "wikilink")**拉伯氏脊飾龍**体长约12米，体重9吨（*C.
lapparenti*），由P. Taquet及D.A. Russell在1998年正式描述、命名\[1\]。

脊飾龍的身份一直有些質疑，有些科學家認為牠其實是似鱷龍，而其他的則認為牠是[疑名](../Page/疑名.md "wikilink")\[2\]。

## 參考資料

## 外部連結

  - [恐龍博物館——脊飾龍](https://web.archive.org/web/20070612144514/http://www.dinosaur.net.cn/Museum/Cristatusaurus.htm)
  - [*Cristatusaurus*](http://web.me.com/dinoruss/de_4/5caa7b9.htm) in
    the Dinosaur Encyclopaedia

[Category:棘龍科](../Category/棘龍科.md "wikilink")
[Category:非洲恐龍](../Category/非洲恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  Taquet, P. and Russell, D.A. (1998). "New data on spinosaurid
    dinosaurs from the Early Cretaceous of the Sahara". *Comptes Rendus
    de l'Académie des Sciences à Paris, Sciences de la Terre et des
    Planètes* 327: 347-353
2.  Rauhut, O.W.M. (2003). "The interrelationships and evolution of
    basal theropod dinosaurs". *Special Papers in Palaeontology* 69:
    1-213.