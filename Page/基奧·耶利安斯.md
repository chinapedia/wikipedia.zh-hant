**基奧·耶利安斯**（**Kew
Jaliens**，）出生於[荷蘭](../Page/荷蘭.md "wikilink")[鹿特丹](../Page/鹿特丹.md "wikilink")，[蘇利南裔](../Page/蘇利南.md "wikilink")[荷蘭職業](../Page/荷蘭.md "wikilink")[足球運動員](../Page/足球.md "wikilink")，擔任後衛，現效力[波蘭甲組足球聯賽球隊](../Page/波蘭甲組足球聯賽.md "wikilink")[克拉科夫](../Page/维斯瓦克拉科夫足球俱乐部.md "wikilink")。

## 生涯

在[阿爾克馬爾](../Page/AZ阿爾克馬爾.md "wikilink")，耶利安斯可按球隊需要而擔任[右閘或](../Page/右閘.md "wikilink")[中堅](../Page/中堅.md "wikilink")，由於主教練[雲高爾較喜愛以](../Page/雲高爾.md "wikilink")[史汀遜擔任中堅](../Page/格里塔·斯滕森.md "wikilink")，因此經常把耶利安斯伙拍[當克](../Page/賴恩·當克.md "wikilink")（Ryan
Donk）擔任[中堅](../Page/中堅.md "wikilink")。耶利安斯與球會簽訂新合約至2012年，而球會總監[馬素·白蘭斯](../Page/馬素·白蘭斯.md "wikilink")（Marcel
Brands）更認為耶利安斯是球隊的基石\[1\]\[2\]。雖然耶利安斯在荷甲有不俗表現，被認為是全荷甲最穩定的後衛，但他並未在國家隊得到固定的地位。

[2006年世界盃決賽周](../Page/2006年世界盃.md "wikilink")，耶利安斯入選了[荷蘭隊的](../Page/荷蘭國家足球隊.md "wikilink")23人名單，但他卻未能入選[2008年歐洲國家盃荷蘭參賽名單](../Page/2008年歐洲國家盃.md "wikilink")。

耶利安斯是[南美洲國家](../Page/南美洲.md "wikilink")[蘇利南的後裔](../Page/蘇利南.md "wikilink")，他的叔叔（Kenneth
Jaliens）現時是[蘇利南國家足球隊的主教練](../Page/蘇利南國家足球隊.md "wikilink")\[3\]。

## 職業統計

<table>
<thead>
<tr class="header">
<th><p>賽季</p></th>
<th><p>球隊</p></th>
<th><p>賽事</p></th>
<th><p>出賽</p></th>
<th><p>入球</p>
<tr>
<td>
<p>1996/97</p>
<td>
<p><a href="../Page/鹿特丹斯巴達.md" title="wikilink">鹿特丹斯巴達</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>2</p>
<td>
<p>0</p>
<tr>
<td>
<p>1997/98</p>
<td>
<p><a href="../Page/鹿特丹斯巴達.md" title="wikilink">鹿特丹斯巴達</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>32</p>
<td>
<p>3</p>
<tr>
<td>
<p>1998/99</p>
<td>
<p><a href="../Page/鹿特丹斯巴達.md" title="wikilink">鹿特丹斯巴達</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>32</p>
<td>
<p>0</p>
<tr>
<td>
<p>1999/00</p>
<td>
<p><a href="../Page/鹿特丹斯巴達.md" title="wikilink">鹿特丹斯巴達</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>2</p>
<td>
<p>1</p>
<tr>
<td>
<td>
<p><a href="../Page/威廉二世足球俱樂部.md" title="wikilink">威廉二世</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>22</p>
<td>
<p>0</p>
<tr>
<td>
<p>2000/01</p>
<td>
<p><a href="../Page/威廉二世足球俱樂部.md" title="wikilink">威廉二世</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>31</p>
<td>
<p>2</p>
<tr>
<td>
<p>2001/02</p>
<td>
<p><a href="../Page/威廉二世足球俱樂部.md" title="wikilink">威廉二世</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>29</p>
<td>
<p>0</p>
<tr>
<td>
<p>2002/03</p>
<td>
<p><a href="../Page/威廉二世足球俱樂部.md" title="wikilink">威廉二世</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>32</p>
<td>
<p>1</p>
<tr>
<td>
<p>2003/04</p>
<td>
<p><a href="../Page/威廉二世足球俱樂部.md" title="wikilink">威廉二世</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>33</p>
<td>
<p>1</p>
<tr>
<td>
<p>2004/05</p>
<td>
<p><a href="../Page/AZ阿爾克馬爾.md" title="wikilink">阿爾克馬爾</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>22</p>
<td>
<p>1</p>
<tr>
<td>
<p>2005/06</p>
<td>
<p><a href="../Page/AZ阿爾克馬爾.md" title="wikilink">阿爾克馬爾</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>30</p>
<td>
<p>0</p>
<tr>
<td>
<p>2006/07</p>
<td>
<p><a href="../Page/AZ阿爾克馬爾.md" title="wikilink">阿爾克馬爾</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>28</p>
<td>
<p>1</p>
<tr>
<td>
<p>2007/08</p>
<td>
<p><a href="../Page/AZ阿爾克馬爾.md" title="wikilink">阿爾克馬爾</a></p>
<td>
<p><a href="../Page/荷甲.md" title="wikilink">荷甲</a></p>
<td>
<p>7</p>
<td>
<p>0</p>
<tr>
<td>
<td>
<td>
<p><strong>合計</strong></p>
<td>
<p><strong>302</strong></p>
<td>
<p><strong>10</strong></p></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - [耶利安斯
    職業數據](http://www.wereldvanoranje.nl/profielen/profiel.php?id=450)

[Category:荷蘭足球運動員](../Category/荷蘭足球運動員.md "wikilink")
[Category:阿爾克馬爾球員](../Category/阿爾克馬爾球員.md "wikilink")
[Category:威廉二世球員](../Category/威廉二世球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年夏季奧林匹克運動會足球運動員](../Category/2008年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:蘇里南裔荷蘭人](../Category/蘇里南裔荷蘭人.md "wikilink")

1.  [Jaliens signs contract extension with
    AZ](http://az.nl/index.php?module=news&articleID=21146)
2.  [Kew Jaliens jaar langer bij
    AZ](http://az.nl/index.php?module=news&articleID=21144)
3.