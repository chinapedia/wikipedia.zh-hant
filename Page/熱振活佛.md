[Bundesarchiv_Bild_135-S-12-40-25,_Tibetexpedition,_Regent_von_Tibet.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_135-S-12-40-25,_Tibetexpedition,_Regent_von_Tibet.jpg "fig:Bundesarchiv_Bild_135-S-12-40-25,_Tibetexpedition,_Regent_von_Tibet.jpg")
[Bundesarchiv_Bild_135-S-12-20-36,_Tibetexpedition,_Regent_von_Tibet.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_135-S-12-20-36,_Tibetexpedition,_Regent_von_Tibet.jpg "fig:Bundesarchiv_Bild_135-S-12-20-36,_Tibetexpedition,_Regent_von_Tibet.jpg")

**熱振活佛**（）[清朝先封为](../Page/清朝.md "wikilink")[诺门罕](../Page/諾門罕_\(稱號\).md "wikilink")，称**广衍黄法阿齐图诺门罕**，后封为[呼图克图](../Page/呼图克图.md "wikilink")，称**哷徵阿齐图呼圖克圖**（简称“**哷徵呼圖克圖**”），此后又曾一度被剥夺[呼图克图职衔](../Page/呼图克图.md "wikilink")，为[清朝有資格任](../Page/清朝.md "wikilink")[攝政的活佛之一](../Page/西藏摄政.md "wikilink")；[中华民國时期续封](../Page/中华民國.md "wikilink")[呼图克图](../Page/呼图克图.md "wikilink")，稱**热振呼圖克圖**；[中华人民共和国时期称为](../Page/中华人民共和国.md "wikilink")**熱振活佛**；駐錫[拉萨](../Page/拉萨.md "wikilink")[喜德寺及](../Page/喜德寺.md "wikilink")[林周县](../Page/林周县.md "wikilink")[热振寺](../Page/热振寺.md "wikilink")。

## 沿革

[热振寺是](../Page/热振寺.md "wikilink")[藏傳佛教](../Page/藏傳佛教.md "wikilink")[噶当派的祖寺](../Page/噶当派.md "wikilink")。位于[拉薩](../Page/拉薩.md "wikilink")[林周縣北部](../Page/林周縣.md "wikilink")，建于1057年（藏历阴火鸡年）。至今已有近千年的历史，比拉薩三大寺[甘丹寺](../Page/甘丹寺.md "wikilink")、[哲蚌寺](../Page/哲蚌寺.md "wikilink")、[色拉寺早](../Page/色拉寺.md "wikilink")350年以上。\[1\]\[2\]

热振寺的创建者是噶当派始祖[仲敦巴](../Page/仲敦巴.md "wikilink")。[藏历](../Page/藏历.md "wikilink")“[火空海](../Page/火空海.md "wikilink")”纪元381年即公元1004年，仲敦巴生于[吐蕃堆龙地方](../Page/吐蕃.md "wikilink")（今[堆龙德庆县羊八井一带](../Page/堆龙德庆县.md "wikilink")），先后师从[赛尊](../Page/赛尊.md "wikilink")（）、[班智达](../Page/班智达.md "wikilink")[弥底学习佛经和梵文](../Page/弥底.md "wikilink")、[因明学](../Page/因明.md "wikilink")。1045年，他拜北上西藏的[孟加拉高僧](../Page/孟加拉.md "wikilink")、当时[印度最著名的佛教学者](../Page/印度.md "wikilink")[阿底峡为师](../Page/阿底峡.md "wikilink")。阿底峡圆寂后，仲敦巴率徒众修建了热振寺和供奉阿底峡法体的银塔。自此，仲敦巴继承和弘传阿底峡的经教，逐渐形成了[噶当派](../Page/噶当派.md "wikilink")，即為[格魯派](../Page/格魯派.md "wikilink")（黃教）的前身。\[3\]

1751年，[阿旺曲丹请求](../Page/阿旺曲丹.md "wikilink")[七世达赖将](../Page/七世达赖.md "wikilink")[热振寺赐给自己](../Page/热振寺.md "wikilink")，获得达赖批准。[阿旺曲丹是生于](../Page/阿旺曲丹.md "wikilink")[青海的](../Page/青海.md "wikilink")[藏传佛教](../Page/藏传佛教.md "wikilink")[格鲁派高僧](../Page/格鲁派.md "wikilink")，曾任[七世达赖的经师](../Page/七世达赖.md "wikilink")，并且曾任[甘丹赤巴](../Page/甘丹赤巴.md "wikilink")。1734年（雍正十二年），获[雍正帝赐](../Page/雍正帝.md "wikilink")“**广衍黄法阿齐图[诺门罕](../Page/诺门罕.md "wikilink")**”衔名，并赐印信。\[4\]\[5\]自[阿旺曲丹开始转世](../Page/阿旺曲丹.md "wikilink")，形成了热振活佛系统。

后来，下一世的[洛桑益西丹巴绕杰也于](../Page/热振·洛桑益西丹巴绕杰.md "wikilink")1770年（乾隆三十五年），获[清朝](../Page/清朝.md "wikilink")[乾隆帝赐予](../Page/乾隆帝.md "wikilink")“[诺门罕](../Page/诺门罕.md "wikilink")”职衔，并给印信。\[6\]其后一世的[阿旺益西楚臣坚赞在拉萨扩建寺院](../Page/热振·阿旺益西楚臣坚赞.md "wikilink")，并请得[道光帝御赐寺名为](../Page/道光帝.md "wikilink")“凝禧寺”（该寺即[喜德寺](../Page/喜德寺.md "wikilink")）。他曾两次出任[西藏摄政](../Page/西藏摄政.md "wikilink")，于1853年获[咸丰帝谕旨](../Page/咸丰帝.md "wikilink")：“著加恩销去诺门罕，作为**哷徵阿齐图呼图克图**，并著准其转世。”这就将热振活佛的衔职地位从[诺门罕提高到了](../Page/诺门罕.md "wikilink")[呼图克图](../Page/呼图克图.md "wikilink")。1856年，因为“辨理察木多夷案出力”，[咸丰帝还赏加其](../Page/咸丰帝.md "wikilink")“慧灵”名号，并赐用黄缰。\[7\]

但是，1862年，由于[阿旺益西楚臣坚赞为革退](../Page/热振·阿旺益西楚臣坚赞.md "wikilink")[哲蚌寺](../Page/哲蚌寺.md "wikilink")[堪布一事而同](../Page/堪布.md "wikilink")[哲蚌寺发生严重冲突](../Page/哲蚌寺.md "wikilink")，哲蚌寺僧众联络[甘丹寺僧众攻打其摄政府](../Page/甘丹寺.md "wikilink")，使[喜德寺遭到严重破坏](../Page/喜德寺.md "wikilink")。他携带摄政印信赴[北京向清廷呈诉](../Page/北京.md "wikilink")，但未果。\[8\][驻藏大臣](../Page/驻藏大臣.md "wikilink")[满庆向](../Page/满庆.md "wikilink")[同治帝上奏](../Page/同治帝.md "wikilink")，请求革除阿旺益西楚臣坚赞的“呼图克图”衔名，注销其敕印，由[夏扎·旺曲杰布取代其出任](../Page/夏扎·旺曲杰布.md "wikilink")[西藏摄政](../Page/西藏摄政.md "wikilink")。[满庆的上奏获](../Page/满庆.md "wikilink")[同治帝批准](../Page/同治帝.md "wikilink")。此后，阿旺益西楚臣坚赞的“呼图克图”衔名被革除。\[9\]\[10\]

1877年（光绪三年）
，[光绪帝谕旨](../Page/光绪帝.md "wikilink")：“兹据[松溎奏](../Page/松溎.md "wikilink")，现在众喇嘛等，禀诉已故哷徵呼图克图从前劳绩及被屈情形，据情代奏等语。著照所请，准其查访已故哷徵呼图克图转世之幼子，仍掌该寺事务，并将名号赏还。”\[11\]自此，热振活佛系统又恢复了“[呼图克图](../Page/呼图克图.md "wikilink")”地位。

1933年，[十三世达赖圆寂后](../Page/十三世达赖.md "wikilink")，根据其遗训，[热振·图旦绛白益西丹巴坚赞主持了](../Page/热振·图旦绛白益西丹巴坚赞.md "wikilink")[十四世达赖](../Page/十四世达赖.md "wikilink")[转世灵童寻访工作](../Page/转世灵童.md "wikilink")。\[12\]1933年，[国民政府册封](../Page/国民政府.md "wikilink")[热振·图旦绛白益西丹巴坚赞为](../Page/热振·图旦绛白益西丹巴坚赞.md "wikilink")“辅国普化禅师”。国民政府令称：“热振呼图克图阐扬道化，世著令名，自达赖圆寂，综摄全藏政教，翊赞中央，扶绥地方，丕绩懋昭，深感嘉尚，着给予辅国普化禅师名号，用示优隆。此令。”\[13\]

此后，[热振·图旦绛白益西丹巴坚赞自](../Page/热振·图旦绛白益西丹巴坚赞.md "wikilink")1934年至1941年担任[西藏摄政](../Page/西藏摄政.md "wikilink")。1941年他辞职，由[达扎·阿旺松饶担任摄政](../Page/达扎·阿旺松饶.md "wikilink")，约定达扎·阿旺松饶只任摄政两三年。但其后达扎·阿旺松饶拒绝交出权力，并于1947年逮捕[热振·图旦绛白益西丹巴坚赞](../Page/热振·图旦绛白益西丹巴坚赞.md "wikilink")，热振于同年5月死在[布达拉宫的监狱中](../Page/布达拉宫.md "wikilink")。\[14\]\[15\]达扎还革除了热振活佛的“[呼图克图](../Page/呼图克图.md "wikilink")”名号，仅仅准其以普通活佛转世。1947年6月7日，热振派到中国内地的代表[邓珠朗杰上书提醒国民政府应当对](../Page/邓珠朗杰.md "wikilink")[热振事件采取强硬立场](../Page/热振事件.md "wikilink")，并拟出了善后处理办法三项，其中有“一、查热振呼图克图乃中央册封，此次事变后被达扎政府革去呼图克图名号，仅准以普通活佛转世，系藐视中央，玩忽典令。今后热振呼图克图转世，应请中央明令使其保有原有地位及封号。”\[16\]同年6月17日，[国民政府](../Page/国民政府.md "wikilink")[蒙藏委员会回复称](../Page/蒙藏委员会.md "wikilink")，第一项“自可令饬依照旧例办理。”\[17\]

达扎革去热振活佛的“[呼图克图](../Page/呼图克图.md "wikilink")”名号后，热振活佛的转世工作一直未能开展。直到达扎于1950年被迫辞去摄政职务，由[达赖亲政后](../Page/14世达赖.md "wikilink")，热振活佛的转世灵童才于1951年被认定，并于1955年[坐床](../Page/坐床.md "wikilink")，正式继任热振活佛，接受西藏方面所给“热振阿齐图诺门罕”称号（注意其并未获得[呼图克图名号](../Page/呼图克图.md "wikilink")），这就是[热振·单增晋美土多旺秋](../Page/热振·单增晋美土多旺秋.md "wikilink")。\[18\]但是，热振活佛的转世工作并非全无争议。一位目前住在印度，自称“六世热振呼圖克圖”（The
Sixth Reting
Hutukthu）者，在给[达赖的公开信里](../Page/14世达赖.md "wikilink")，指责西藏[噶厦在五世热振活佛圆寂后继续压制热振系](../Page/噶厦.md "wikilink")，不合法地确认单增晋美为六世热振活佛。\[19\]

1997年，[热振·单增晋美土多旺秋活佛圆寂后](../Page/热振·单增晋美土多旺秋.md "wikilink")，[西藏自治区人民政府及](../Page/西藏自治区人民政府.md "wikilink")[拉萨市人民政府组成了](../Page/拉萨市人民政府.md "wikilink")[转世灵童寻访领导班子以及有高僧参加的寻访顾问班子](../Page/转世灵童.md "wikilink")，开展了热振活佛转世灵童的寻访工作。经过一年多的寻访，确认索朗平措为[转世灵童](../Page/转世灵童.md "wikilink")。此后，通过报请[国家宗教事务局同意](../Page/国家宗教事务局.md "wikilink")，经[西藏自治区人民政府批准](../Page/西藏自治区人民政府.md "wikilink")，索朗平措继任为第七世热振活佛，于2000年1月16日（藏历土兔年十二月十日）在[拉萨](../Page/拉萨.md "wikilink")[大昭寺](../Page/大昭寺.md "wikilink")[释迦牟尼像前举行了传承](../Page/释迦牟尼.md "wikilink")、剃度仪式，于2000年7月14日在[热振寺举行](../Page/热振寺.md "wikilink")[坐床典礼](../Page/坐床.md "wikilink")。\[20\]索朗平措成为了现任热振活佛[热振·洛追嘉措赤烈伦珠](../Page/热振·洛追嘉措赤烈伦珠.md "wikilink")。在[印度的](../Page/印度.md "wikilink")[达赖的发言人则表示](../Page/14世达赖.md "wikilink")，由于未经达赖批准，第七世热振活佛不具有任何合法性。当时达赖和[中华人民共和国政府方面正因为](../Page/中华人民共和国.md "wikilink")[噶玛巴出走而相互指责](../Page/噶瑪巴·伍金赤列多傑.md "wikilink")，关系陷于低潮。\[21\]\[22\]

## 历世热振活佛

热振活佛的世数在[汉文及](../Page/汉文.md "wikilink")[藏文文献中有许多不同种说法](../Page/藏文.md "wikilink")，其中主要有两种说法。一说到[热振·单增晋美土多旺秋为六世](../Page/热振·单增晋美土多旺秋.md "wikilink")，一说到[热振·单增晋美土多旺秋为二十二世](../Page/热振·单增晋美土多旺秋.md "wikilink")。其中，六世说在[藏族人民中获得较为普遍的承认](../Page/藏族.md "wikilink")。\[23\]以下暂按六世说列出历世热振活佛：

| 世数  | 生年    | 坐床年份  | 卒年    | 汉文姓名                                                 | 藏文姓名                                    |
| --- | ----- | ----- | ----- | ---------------------------------------------------- | --------------------------------------- |
| 第一世 | 1677年 | ——    | 1751年 | [阿旺曲丹](../Page/阿旺曲丹.md "wikilink")                   | ཀཅན་ཚ་ངག་དབང་མཆོག་ལྡན                   |
| 第二世 | 1759年 | 1761年 | 1815年 | [热振·洛桑益西丹巴绕杰](../Page/热振·洛桑益西丹巴绕杰.md "wikilink")     | བློ་བཟང་ཡེ་ཤེས་བསྟན་པ་རབ་རྒྱས           |
| 第三世 | 1816年 | （不详）  | 1863年 | [热振·阿旺益西楚臣坚赞](../Page/热振·阿旺益西楚臣坚赞.md "wikilink")     | ངག་དབང་ཡེ་ཤེས་ཚུལ་ཁྲིམས་རྒྱལ་མཚན        |
| 第四世 | （不详）  | （不详）  | （不详）  | [热振·阿旺洛桑益西丹巴坚赞](../Page/热振·阿旺洛桑益西丹巴坚赞.md "wikilink") | ངཀ་དབང་བློ་བཟང་ཡེ་ཤེས་བསྟན་པའི་རྒྱལ་མཚན |
| 第五世 | 1912年 | 1930年 | 1947年 | [热振·图旦绛白益西丹巴坚赞](../Page/热振·图旦绛白益西丹巴坚赞.md "wikilink") | ཐུབ་བསྟན་འཇམ་དཔལ་ཡེ་ཤེས་རྒྱལ་མཚན་       |
| 第六世 | 1948年 | 1955年 | 1997年 | [热振·单增晋美土多旺秋](../Page/热振·单增晋美土多旺秋.md "wikilink")     | བསྟན་འཛིན་འཇིགས་མེད་ཐུབ་བསྟན་དབང་ཕྱུག་  |
| 第七世 | 1997年 | 2000年 | 在世    | [热振·洛追嘉措赤烈伦珠](../Page/热振·洛追嘉措赤烈伦珠.md "wikilink")     | བློ་གྲོས་རྒྱ་མཚོ་འཕྲིན་ལས་ལྷུན་གྲུབ་    |

## 参考文献

## 外部链接

  - [<http://www.reting.org>](http://www.reting.org/)

{{-}}

[热振活佛](../Category/热振活佛.md "wikilink")
[Category:呼图克图](../Category/呼图克图.md "wikilink")

1.

2.

3.  [仲敦巴·甲瓦迥乃，中国藏族教育网，2011-11-07](http://www.teducn.com/chinese/renwu/dashihuicui/2011-11-07/167.html)

4.  历史的见证，北京：五洲传播出版社，2001年，第188页

5.  章嘉·若必多吉，热振大座主传

6.
7.  [锡德林——哷徵阿齐图呼图克图的住锡寺，载
    杨辉麟，西藏佛教寺庙，成都：四川人民出版社，2003年](http://info.tibet.cn/newzt/rsxzzt/xzsm/t20050705_40363.htm)


8.
9.
10. 蘇發祥，歷輩逹賴喇嘛，西寧：青海人民出版社，2009年，第191頁

11. 元以来西藏地方与中央政府关系档案史料汇编: 淸代，北京：中国藏学出版社，1994年，第1044页

12. 刘国铭等编，中国国民党百年人物全书（下），北京：團结出版社，2005年

13. [西藏·历史，侨报网，2008年4月30日](http://epaper.usqiaobao.com:81/qiaobao/html/2008-04/30/content_36684.htm)


14. 曹自強、毛翔、喜饶尼玛，西藏的寺庙和僧侣，北京：中国藏学出版社，1995年，第138页

15. 陈谦平，“热振事件”与战后国民政府的西藏政策，民国档案2006年1期

16. 邓珠朗杰等为热振被害圆寂有关善后请求三项致蒙藏委员会呈（1947年6月7日），载
    元以来西藏地方与中央政府关系档案史料汇编（第七册），第2880页

17. 蒙藏委员会为热振圆寂善后事项复邓珠朗杰代电（1947年6月17日），载 元以来西藏地方与中央政府关系档案史料汇编（第七册），第2881页

18. [热振寺，人民网西藏频道，于2012-7-29查阅](http://xz.people.com.cn/GB/138902/139221/139276/8394048.html)


19. Open Letter to the 14th Dalai Lama, by the 6th Reting Hutuku,
    Reting.org.[1](http://www.reting.org/Reting-OpenLetterToDalaiLama.pdf)

20. [第六世热振活佛转世灵童批准继位、剃度、取法名仪式在拉萨举行，法音2000年第2期（总第186期），第33页](http://www.chinabuddhism.com.cn/a/fayin/dharma/2k02/g2k02f12.htm)

21. [达赖不承认第七世热振活佛，BBC中文网，2000年1月17日](http://news.bbc.co.uk/chinese/simp/hi/newsid_600000/newsid_606500/606512.stm)

22. World Tibet Network News (January 11, 2000) [*Beijing Discovers
    Another "Living Buddha"
    (AFP)*](http://www.tibet.ca/en/newsroom/wtn/archive/old?y=2000&m=1&p=11-2_5)

23. 藏学研究论丛8，拉萨：西藏人民出版社，1996年，第69页