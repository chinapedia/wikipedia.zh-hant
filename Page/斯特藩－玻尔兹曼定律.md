**斯特藩-玻尔兹曼定律**（），又称**斯特藩定律**，是[热力学中的一个著名定律](../Page/热力学.md "wikilink")，其内容为：
一个[黑体表面单位面积在单位](../Page/绝对黑体.md "wikilink")[时间内](../Page/时间.md "wikilink")[辐射出的总](../Page/辐射.md "wikilink")[能量](../Page/能量.md "wikilink")（称为物体的[辐射度或](../Page/辐射度.md "wikilink")[能量通量密度](../Page/能量通量密度.md "wikilink")）*j*<sup>\*</sup>与黑体本身的[热力学温度](../Page/热力学温度.md "wikilink")*T*（又称[绝对温度](../Page/绝对温度.md "wikilink")）的<U>四次方</U>成正比，即：

\[j^{\star} = \epsilon\sigma T^{4}\]

其中辐射度*j*<sup>\*</sup>具有[功率密度的](../Page/功率密度.md "wikilink")[量纲](../Page/量纲.md "wikilink")（能量/（时间·距离<sup>2</sup>）），[国际单位制标准单位为](../Page/国际单位制.md "wikilink")[焦耳](../Page/焦耳.md "wikilink")/（秒·平方米），即[瓦特](../Page/瓦特.md "wikilink")/平方米。绝对温度*T*的标准单位是[开尔文](../Page/热力学温标.md "wikilink")，*\(\epsilon\)*为黑体的[辐射系数](../Page/辐射系数.md "wikilink")；若为绝对黑体，则\(\epsilon=1\).

[比例系数σ称为](../Page/比例系数.md "wikilink")[斯特藩-玻尔兹曼常数或](../Page/斯特藩-玻尔兹曼常数.md "wikilink")**斯特藩常量**。它可由[自然界其他已知的](../Page/自然界.md "wikilink")[基本物理常数算得](../Page/基本物理常数.md "wikilink")，因此它不是一个基本物理常数。该常数的值为：

\[\sigma=\frac{2\pi^5 k^4}{15c^2h^3}= 5.670 400(40) \times 10^{-8} \textrm{J}\textrm{s}^{-1}\textrm{m}^{-2}\textrm{K}^{-4}.\]

所以温度为100 K的绝对黑体表面辐射的能量通量密度为5.67 W/m<sup>2</sup>，1000 K的黑体为56.7
kW/m<sup>2</sup>，等等。

斯特藩-玻尔兹曼定律是一个典型的[幂次定律](../Page/幂次定律.md "wikilink")。

本定律由[斯洛文尼亚](../Page/斯洛文尼亚.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")[约瑟夫·斯特藩](../Page/约瑟夫·斯特藩.md "wikilink")（Jožef
Stefan）和[奥地利物理学家](../Page/奥地利.md "wikilink")[路德维希·玻尔兹曼分别于](../Page/路德维希·玻尔兹曼.md "wikilink")1879年和1884年各自独立提出。提出过程中斯特藩通过的是对实验数据的归纳总结，玻尔兹曼则是从[热力学理论出发](../Page/热力学.md "wikilink")，通过假设用[光](../Page/光.md "wikilink")（[电磁波辐射](../Page/电磁波.md "wikilink")）代替[气体作为](../Page/气体.md "wikilink")[热机的工作](../Page/热机.md "wikilink")[介质](../Page/介质.md "wikilink")，最终推导出与斯特藩的归纳结果相同的结论。本定律最早由斯特藩于1879年3月20日以*Über
die Beziehung zwischen der Wärmestrahlung und der
Temperatur*（《论热辐射与温度的关系》）为论文题目发表在[维也纳科学院的大会报告上](../Page/维也纳.md "wikilink")，这是唯一一个以斯洛文尼亚人的名字命名的物理学定律。

本定律只适用于黑体这类理想辐射源。

## 斯特藩-玻尔兹曼定律的推导

斯特藩-玻尔兹曼定律能够方便地通过对黑体表面各点的辐射[谱强度应用](../Page/电磁波谱.md "wikilink")[普朗克黑体辐射定律](../Page/普朗克黑体辐射定律.md "wikilink")，再将结果在辐射进入的半球形空间表面以及所有可能辐射频率进行[积分得到](../Page/积分.md "wikilink")。

\[j^{\star}=\int_0^\infty \!d\nu \int_{\Omega_0} d\Omega~I(\nu,T)\cos(\theta)\]

式中Ω<sub>0</sub>黑体表面一点的辐射进入的半球形空间表面（以辐射点为球心），\(I(\nu,T)\)为在温度*T*时黑体表面的单位面积在单位时间、单位[立体角上辐射出的频率为](../Page/立体角.md "wikilink")\(\nu\)的电磁波能量。式中包括了一个余弦因子，因为黑体辐射几何上严格符合[朗伯余弦定律](../Page/朗伯余弦定律.md "wikilink")（Lambert's
cosine law）。将几何微元关系dΩ=sin(θ)dθdφ代入上式并积分得：

\[j^{\star}=\int_0^\infty \!d\nu \int_0^{2\pi} \!d\phi \int_0^{\pi/2}\!d\theta
~I(\nu,T)\cos(\theta)\sin(\theta)=\frac{2\pi^5 k^4}{15c^2h^3}\,T^4\]

（对频率的玻色积分项的计算方法参见条目[多重對數函數](../Page/多重對數函數.md "wikilink")）

## 日面温度

提出本定律后斯特藩利用它估算了[太阳的表面温度](../Page/太阳.md "wikilink")。当时[法国人](../Page/法国.md "wikilink")[查理·索里特](../Page/查理·索里特.md "wikilink")（Charles
Soret，1854年–1904年）用实验测得地球上接收到的太阳发出的能量通量密度约为一块加热金属板表面辐射的能量通量密度的29倍。将适当大小的圆形金属版放置在测量仪器前方适当的距离，则可以认为测量仪器接收到的金属板发出辐射的角度与太阳光照射的角度基本相同。索里特测得金属板的表面温度为1900[°C到](../Page/摄氏度.md "wikilink")2000 °C之间。斯特藩猜测太阳照射到地球的能量有1/3被[地球大气层吸收](../Page/地球大气层.md "wikilink")（当时尚未有关于大气层对电磁辐射的[吸收的公认测量数据](../Page/吸收_\(电磁辐射\).md "wikilink")），所以算得实际接收到的太阳辐射强度应为金属板辐射强度的29×3/2
= 43.5倍。金属板的表面温度斯特藩取索里特猜测的中间值1950 °C，即2200 K。由于43.5 =
2.57<sup>4</sup>，所以根据上面的定律，太阳表面的绝对温度应为金属板表面绝对温度的2.57倍，即5430 °C或5700
K（现代精确测量结果为5780
K）。这是历史上对日面温度的第一个较精确的测量结果。在此之前人们对日面温度的数值曾经众说纷纭，测量结果从1800 °C到13,000,000 °C都有。通过其他方法测量的日面温度与该结果的吻合验证了本定律的正确性。

## 参考文献

  - Stefan, J.: *Über die Beziehung zwischen der Wärmestrahlung und der
    Temperatur*, in: *Sitzungsberichte der
    mathematisch-naturwissenschaftlichen Classe der kaiserlichen
    Akademie der Wissenschaften*, Bd. 79 (Wien 1879), S. 391-428.
  - Boltzmann, L.: *Ableitung des Stefan'schen Gesetzes, betreffend die
    Abhängigkeit der Wärmestrahlung von der Temperatur aus der
    electromagnetischen Lichttheorie*, in: *Annalen der Physik und
    Chemie*, Bd. 22 (1884), S. 291-294
  - 汪志诚编，《热力学·统计物理》（第三版），高等教育出版社，北京，1993，第87页\~第91页，ISBN 7-04-004360-2
  - 吴强、郭光灿编，《光学》，中国科学技术大学出版社，合肥，1996，第380页\~第381页，ISBN 7-312-00762-7

## 参见

  - [维恩位移定律](../Page/维恩位移定律.md "wikilink")
  - [瑞利-金斯定律](../Page/瑞利-金斯定律.md "wikilink")

[Category:热力学](../Category/热力学.md "wikilink")
[Category:物理定律](../Category/物理定律.md "wikilink")