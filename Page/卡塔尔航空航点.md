**卡塔尔航空通航城市**，包含[卡塔尔航空在全球范围内开通航线](../Page/卡塔尔航空.md "wikilink")156個目的地(結至2018年10月)之詳細的国家、城市和机场名称，清单如下。

未公佈開航日期航點有：

  - [喀麦隆](../Page/喀麦隆.md "wikilink")[杜阿拉](../Page/杜阿拉.md "wikilink")、[加蓬](../Page/加蓬.md "wikilink")[利伯维尔](../Page/利伯维尔.md "wikilink")、[印尼](../Page/印尼.md "wikilink")[棉蘭](../Page/棉蘭.md "wikilink")\[1\]
  - [美國](../Page/美國.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")、[赞比亚](../Page/赞比亚.md "wikilink")[卢萨卡](../Page/卢萨卡.md "wikilink")\[2\]、[马来西亚](../Page/马来西亚.md "wikilink")[浮罗交怡](../Page/浮罗交怡.md "wikilink")

## [非洲](../Page/非洲.md "wikilink")

### [北非](../Page/北非.md "wikilink")

  - [阿尔及利亚](../Page/阿尔及利亚.md "wikilink")

      - [阿尔及尔](../Page/阿尔及尔.md "wikilink")
        （[阿爾及爾胡阿里·布邁丁機場](../Page/阿爾及爾胡阿里·布邁丁機場.md "wikilink")）

  - [摩洛哥](../Page/摩洛哥.md "wikilink")

      - [卡萨布兰卡](../Page/卡萨布兰卡.md "wikilink")
        （[穆罕默德五世国际机场](../Page/穆罕默德五世国际机场.md "wikilink")）
      - [馬拉喀什](../Page/馬拉喀什.md "wikilink")
        （[馬拉喀什－邁納拉國際機場](../Page/馬拉喀什－邁納拉國際機場.md "wikilink")）

  - [突尼斯](../Page/突尼斯.md "wikilink")

      - [突尼斯](../Page/突尼斯.md "wikilink")
        （[突尼斯-迦太基国际机场](../Page/突尼斯-迦太基国际机场.md "wikilink")）

  - [苏丹](../Page/苏丹共和国.md "wikilink")

      - [喀土穆](../Page/喀土穆.md "wikilink")（[喀土穆國際機場](../Page/喀土穆國際機場.md "wikilink")）

### [東非](../Page/東非.md "wikilink")

  - [肯尼亚](../Page/肯尼亚.md "wikilink")

      - [内罗毕](../Page/内罗毕.md "wikilink")
        （[乔莫·肯雅塔国际机场](../Page/乔莫·肯雅塔国际机场.md "wikilink")）

  - [烏干達](../Page/烏干達.md "wikilink")

      - [恩德培](../Page/恩德培.md "wikilink")
        （[恩德培國際機場](../Page/恩德培國際機場.md "wikilink")）

  - [盧旺達](../Page/盧旺達.md "wikilink")

      - [基加利](../Page/基加利.md "wikilink")
        （[基加利國際機場](../Page/基加利國際機場.md "wikilink")）

  - [坦桑尼亞](../Page/坦桑尼亞.md "wikilink")

      - [達累斯薩拉姆](../Page/達累斯薩拉姆.md "wikilink")
        （[朱利叶斯·尼雷尔国际机场](../Page/朱利叶斯·尼雷尔国际机场.md "wikilink")）
      - [乞力马扎罗](../Page/乞力马扎罗山.md "wikilink")
        （[乞力马扎罗國際機場](../Page/:en:Kilimanjaro_International_Airport.md "wikilink")）

  - [衣索比亞](../Page/衣索比亞.md "wikilink")

      - [阿迪斯阿貝巴](../Page/阿迪斯阿貝巴.md "wikilink")
        （[博萊國際機場](../Page/博萊國際機場.md "wikilink")）

### [南非](../Page/南部非洲.md "wikilink")

  - [南非](../Page/南非.md "wikilink")

      - [约翰内斯堡](../Page/约翰内斯堡.md "wikilink")
        （[约翰内斯堡国际机场](../Page/约翰内斯堡国际机场.md "wikilink")）
      - [开普敦](../Page/开普敦.md "wikilink")
        （[开普敦国际机场](../Page/开普敦国际机场.md "wikilink")）

  - [莫三比克共和國](../Page/莫三比克共和國.md "wikilink")

      - [馬普托](../Page/馬普托.md "wikilink")
        （[馬普托國際機場](../Page/馬普托國際機場.md "wikilink")）

### [西非](../Page/西非.md "wikilink")

  - [奈及利亞](../Page/奈及利亞.md "wikilink")

      - [拉哥斯](../Page/拉哥斯.md "wikilink")
        （[穆爾塔拉·穆罕默德國際機場](../Page/穆爾塔拉·穆罕默德國際機場.md "wikilink")）

## [亚洲](../Page/亚洲.md "wikilink")

### [东亚](../Page/东亚.md "wikilink")

  - [中国](../Page/中国.md "wikilink")

      - [北京](../Page/北京.md "wikilink")
        （[北京首都国际机场](../Page/北京首都国际机场.md "wikilink")）
      - [上海](../Page/上海.md "wikilink")
        （[上海浦东国际机场](../Page/上海浦东国际机场.md "wikilink")）
      - [广州](../Page/广州.md "wikilink")
        （[广州白云国际机场](../Page/广州白云国际机场.md "wikilink")）
      - [重庆](../Page/重庆.md "wikilink")
        （[重庆江北国际机场](../Page/重庆江北国际机场.md "wikilink")）
      - [成都](../Page/成都.md "wikilink")
        （[成都双流国际机场](../Page/成都双流国际机场.md "wikilink")）
      - [杭州](../Page/杭州.md "wikilink")
        （[杭州萧山国际机场](../Page/杭州萧山国际机场.md "wikilink")）

  - [澳門](../Page/澳門.md "wikilink")

      - [澳門](../Page/澳門.md "wikilink")
        （[澳門国际机场](../Page/澳門国际机场.md "wikilink")）

  - [香港](../Page/香港.md "wikilink")

      - [香港](../Page/香港.md "wikilink")
        （[香港国际机场](../Page/香港国际机场.md "wikilink"))

  - [日本](../Page/日本.md "wikilink")

      - [東京](../Page/東京.md "wikilink")
        （[東京成田国际机场](../Page/東京成田国际机场.md "wikilink")）
      - [東京](../Page/東京.md "wikilink")
        （[東京羽田国际机场](../Page/東京羽田国际机场.md "wikilink")）

  - [韩国](../Page/韩国.md "wikilink")

      - [首尔](../Page/首尔.md "wikilink")
        （[仁川国际机场](../Page/仁川国际机场.md "wikilink")）

### [西亚](../Page/西亚.md "wikilink")

  - [卡塔尔](../Page/卡塔尔.md "wikilink")

      - [杜哈](../Page/杜哈.md "wikilink")
        （[哈馬德國際機場](../Page/哈馬德國際機場.md "wikilink")）
        **基地**

  - [伊朗](../Page/伊朗.md "wikilink")

      - [德黑兰](../Page/德黑兰.md "wikilink")
        （[伊玛目霍梅尼国际机场](../Page/伊玛目霍梅尼国际机场.md "wikilink")）
      - [马什哈德](../Page/马什哈德.md "wikilink")

  - [约旦](../Page/约旦.md "wikilink")

      - [安曼](../Page/安曼.md "wikilink")
        （[阿勒娅王后国际机场](../Page/阿勒娅王后国际机场.md "wikilink")）

  - [科威特国](../Page/科威特.md "wikilink")

      - [科威特](../Page/科威特.md "wikilink")
        （[科威特国际机场](../Page/科威特国际机场.md "wikilink")）

  - [黎巴嫩](../Page/黎巴嫩.md "wikilink")

      - [贝鲁特](../Page/贝鲁特.md "wikilink")
        （[贝鲁特-拉菲克·哈里里国际机场](../Page/贝鲁特-拉菲克·哈里里国际机场.md "wikilink")）

  - [阿曼](../Page/阿曼.md "wikilink")

      - [马斯喀特](../Page/马斯喀特.md "wikilink")
        （[马斯喀特国际机场](../Page/马斯喀特国际机场.md "wikilink")）
      - [塞拉萊](../Page/塞拉萊.md "wikilink")
        （[塞拉萊機場](../Page/塞拉萊機場.md "wikilink")）

  - [伊拉克](../Page/伊拉克.md "wikilink")

      - [巴格達](../Page/巴格達.md "wikilink")
        （[巴格達國際機場](../Page/巴格達國際機場.md "wikilink")）
      - [巴士拉](../Page/巴士拉.md "wikilink")
        （[巴士拉國際機場](../Page/巴士拉國際機場.md "wikilink")）

### [南亚](../Page/南亚.md "wikilink")

  - [孟加拉国](../Page/孟加拉国.md "wikilink")

      - [达卡](../Page/达卡.md "wikilink")
        （[齐亚国际机场](../Page/齐亚国际机场.md "wikilink")）

  - [印度](../Page/印度.md "wikilink")

      - [海得拉巴](../Page/海得拉巴_\(印度\).md "wikilink")
      - [柯枝](../Page/柯枝.md "wikilink")
        （[柯枝国际机场](../Page/柯枝国际机场.md "wikilink")）
      - [孟买](../Page/孟买.md "wikilink")
        （[贾特拉帕蒂·希瓦吉国际机场](../Page/贾特拉帕蒂·希瓦吉国际机场.md "wikilink")）
      - [特里凡得琅](../Page/特里凡得琅.md "wikilink")
        （[特里凡得琅国际机场](../Page/特里凡得琅国际机场.md "wikilink")）
      - [新德里](../Page/新德里.md "wikilink")
        （[英迪拉·甘地国际机场](../Page/英迪拉·甘地国际机场.md "wikilink")）
      - [果阿](../Page/果阿.md "wikilink")
        （[果阿国际机场](../Page/果阿国际机场.md "wikilink")）
      - [加爾各答](../Page/加爾各答.md "wikilink")
        （[內塔吉·蘇巴斯·錢德拉·鮑斯國際機場](../Page/內塔吉·蘇巴斯·錢德拉·鮑斯國際機場.md "wikilink")）
      - [阿姆利則](../Page/阿姆利則.md "wikilink")
        （[拉加‧杉錫國際機場](../Page/:en:Sri_Guru_Ram_Dass_Jee_International_Airport.md "wikilink")）

  - [马尔代夫](../Page/马尔代夫.md "wikilink")

      - [马累](../Page/马累.md "wikilink")
        （[马累国际机场](../Page/马累国际机场.md "wikilink")）

  - [尼泊尔](../Page/尼泊尔.md "wikilink")

      - [加德满都](../Page/加德满都.md "wikilink")
        （[特里布万国际机场](../Page/特里布万国际机场.md "wikilink")）

  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")

      - [伊斯兰堡](../Page/伊斯兰堡.md "wikilink")
        （[贝娜齐尔·布托国际机场](../Page/贝娜齐尔·布托国际机场.md "wikilink")）
      - [卡拉奇](../Page/卡拉奇.md "wikilink")
        （[真纳国际机场](../Page/真纳国际机场.md "wikilink")）
      - [拉合尔](../Page/拉合尔.md "wikilink")
        （[阿拉马·伊克巴勒国际机场](../Page/阿拉马·伊克巴勒国际机场.md "wikilink")）
      - [白沙瓦](../Page/白沙瓦.md "wikilink")
        （[白沙瓦国际机场](../Page/白沙瓦国际机场.md "wikilink")）

  - [斯里兰卡](../Page/斯里兰卡.md "wikilink")

      - [科伦坡](../Page/科伦坡.md "wikilink")
        （[班达拉奈克国际机场](../Page/班达拉奈克国际机场.md "wikilink")）

### [东南亚](../Page/东南亚.md "wikilink")

  - [柬埔寨](../Page/柬埔寨.md "wikilink")

      - [金邊](../Page/金邊.md "wikilink")
        （[金邊國際機場](../Page/金邊國際機場.md "wikilink")）\[3\]

  - [印度尼西亚](../Page/印度尼西亚.md "wikilink")

      - [雅加达](../Page/雅加达.md "wikilink")
        （[苏加诺-哈达国际机场](../Page/苏加诺-哈达国际机场.md "wikilink")）
      - [峇里島](../Page/峇里島.md "wikilink")
        （[伍拉·賴國際機場](../Page/伍拉·賴國際機場.md "wikilink")）

  - [马来西亚](../Page/马来西亚.md "wikilink")

      - [吉隆坡](../Page/吉隆坡.md "wikilink")
        （[吉隆坡国际机场](../Page/吉隆坡国际机场.md "wikilink")）
      - [檳城](../Page/檳城.md "wikilink")
        （[檳城国际机场](../Page/檳城国际机场.md "wikilink")）\[4\]

  - [缅甸](../Page/缅甸.md "wikilink")

      - [仰光](../Page/仰光.md "wikilink")
        （[仰光国际机场](../Page/仰光国际机场.md "wikilink")）

  - [菲律宾](../Page/菲律宾.md "wikilink")

      - [马尼拉](../Page/马尼拉.md "wikilink")
        （[尼诺伊·阿基诺国际机场](../Page/尼诺伊·阿基诺国际机场.md "wikilink")）
      - [克拉克](../Page/克拉克自由港区.md "wikilink")
        （[迪奧斯達多·馬卡帕加爾國際機場](../Page/迪奧斯達多·馬卡帕加爾國際機場.md "wikilink")）

  - [新加坡](../Page/新加坡.md "wikilink")

      - [樟宜](../Page/樟宜.md "wikilink")
        （[新加坡樟宜机场](../Page/新加坡樟宜机场.md "wikilink")）

  - [泰国](../Page/泰国.md "wikilink")

      - [曼谷](../Page/曼谷.md "wikilink")
        （[素萬那普國際機場](../Page/素萬那普國際機場.md "wikilink")）
      - [布吉](../Page/泰國.md "wikilink")
        （[布吉國際機場](../Page/布吉國際機場.md "wikilink")）\[5\]
      - [清迈](../Page/清迈.md "wikilink")
        （[清迈国际机场](../Page/清迈国际机场.md "wikilink")）\[6\]
      - [甲米](../Page/甲米.md "wikilink")
        （[甲米機場](../Page/甲米機場.md "wikilink")）\[7\]
      - [芭堤雅](../Page/芭堤雅.md "wikilink")
        （[乌塔保国际机场](../Page/乌塔保国际机场.md "wikilink")）\[8\]

  - [越南](../Page/越南.md "wikilink")

      - [胡志明](../Page/胡志明.md "wikilink")
        （[新山一國際機場](../Page/新山一國際機場.md "wikilink")）
      - [河內](../Page/河內.md "wikilink")
        （[內排國際機場](../Page/內排國際機場.md "wikilink")）

## [欧洲](../Page/欧洲.md "wikilink")

### [北歐](../Page/北歐.md "wikilink")

  - [挪威](../Page/挪威.md "wikilink")

      - [奧斯陸](../Page/奧斯陸.md "wikilink")
        （[奧斯陸加勒穆恩機場](../Page/奧斯陸加勒穆恩機場.md "wikilink")）

  - [丹麥](../Page/丹麥.md "wikilink")

      - [哥本哈根](../Page/哥本哈根.md "wikilink")
        （[哥本哈根凱斯楚普機場](../Page/哥本哈根凱斯楚普機場.md "wikilink")）

  - [瑞典](../Page/瑞典.md "wikilink")

      - [斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")
        （[斯德哥爾摩-阿蘭達機場](../Page/斯德哥爾摩-阿蘭達機場.md "wikilink")）

  - [芬蘭](../Page/芬蘭.md "wikilink")

      - [赫爾辛基](../Page/赫爾辛基.md "wikilink")
        （[赫爾辛基萬塔機場](../Page/赫爾辛基萬塔機場.md "wikilink")）

### [南歐](../Page/南歐.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")

      - [羅馬](../Page/羅馬.md "wikilink")
        （[列奥纳多·达芬奇机场](../Page/列奥纳多·达芬奇机场.md "wikilink")）
      - [米兰](../Page/米兰.md "wikilink")
        （[米兰-马尔彭萨机场](../Page/米兰-马尔彭萨机场.md "wikilink")）
      - [威尼斯](../Page/威尼斯.md "wikilink")
        （[威尼斯-泰塞拉机场](../Page/威尼斯-泰塞拉机场.md "wikilink")）
      - [比薩](../Page/比薩.md "wikilink")
        （[比薩-聖朱斯托機場](../Page/比薩-聖朱斯托機場.md "wikilink")）

  - [西班牙](../Page/西班牙.md "wikilink")

      - [马德里](../Page/马德里.md "wikilink")
        （[阿道弗·蘇亞雷斯馬德里-巴拉哈斯機場](../Page/阿道弗·蘇亞雷斯馬德里-巴拉哈斯機場.md "wikilink")）
      - [巴塞隆納](../Page/巴塞隆納.md "wikilink")
        （[巴塞羅那國際機場](../Page/巴塞羅那國際機場.md "wikilink")）

  - [希腊](../Page/希腊.md "wikilink")

      - [雅典](../Page/雅典.md "wikilink")
        （[雅典国际机场](../Page/雅典埃莱夫塞里奥斯·韦尼泽洛斯国际机场.md "wikilink")）

  - [馬其頓共和國](../Page/馬其頓共和國.md "wikilink")

      - [史高比耶](../Page/史高比耶.md "wikilink")
        （[史高比耶亞歷山大大帝機場](../Page/史高比耶亞歷山大大帝機場.md "wikilink")）

### [西歐](../Page/西歐.md "wikilink")

  - [法国](../Page/法国.md "wikilink")

      - [巴黎](../Page/巴黎.md "wikilink")
        （[巴黎－夏尔·戴高乐机场](../Page/巴黎－夏尔·戴高乐机场.md "wikilink")）
      - [尼斯](../Page/尼斯.md "wikilink")
        （[尼斯蔚藍海岸机场](../Page/尼斯蔚藍海岸机场.md "wikilink"))

  - [德国](../Page/德国.md "wikilink")

      - [法兰克福](../Page/法兰克福.md "wikilink")
        （[法兰克福国际机场](../Page/法兰克福国际机场.md "wikilink")）
      - [慕尼黑](../Page/慕尼黑.md "wikilink")
        （[慕尼黑国际机场](../Page/慕尼黑国际机场.md "wikilink")）
      - [柏林](../Page/柏林.md "wikilink")
        （[泰格尔国际机场](../Page/泰格尔国际机场.md "wikilink")）

  - [比利時](../Page/比利時.md "wikilink")

      - [布魯塞爾](../Page/布魯塞爾.md "wikilink")
        （[布魯塞爾機場](../Page/布魯塞爾機場.md "wikilink")）

  - [荷兰](../Page/荷兰.md "wikilink")

      - [阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")
        （[史基浦机场](../Page/史基浦机场.md "wikilink"))

  - [瑞士](../Page/瑞士.md "wikilink")

      - [苏黎世](../Page/苏黎世.md "wikilink")
        （[苏黎世国际机场](../Page/苏黎世国际机场.md "wikilink")）
      - [日内瓦](../Page/日内瓦.md "wikilink")
        （[日內瓦國際機場](../Page/日內瓦國際機場.md "wikilink")）

  - [英国](../Page/英国.md "wikilink")

      - [伦敦](../Page/伦敦.md "wikilink")（[伦敦希思罗机场](../Page/伦敦希思罗机场.md "wikilink")）
      - [曼彻斯特](../Page/曼彻斯特.md "wikilink")
        （[曼彻斯特国际机场](../Page/曼彻斯特国际机场.md "wikilink")）
      - [愛丁堡](../Page/愛丁堡.md "wikilink")
        （[愛丁堡機場](../Page/愛丁堡機場.md "wikilink")）
      - [伯明翰](../Page/伯明翰.md "wikilink")
        （[伯明翰機場](../Page/伯明翰機場.md "wikilink")）
      - [加的夫](../Page/加的夫.md "wikilink")
        （[卡迪夫机场](../Page/卡迪夫机场.md "wikilink")）\[9\]

  - [愛爾蘭](../Page/愛爾蘭.md "wikilink")

      - [都柏林](../Page/都柏林.md "wikilink")
        （[都柏林機場](../Page/都柏林機場.md "wikilink")）

### [中歐](../Page/中歐.md "wikilink")

  - [奧地利](../Page/奧地利.md "wikilink")

      - [維也納](../Page/維也納.md "wikilink")
        （[維也納國際機場](../Page/維也納國際機場.md "wikilink")）

  - [捷克](../Page/捷克.md "wikilink")

      - [布拉格](../Page/布拉格.md "wikilink")
        （[布拉格瓦茨拉夫·哈維爾國際機場](../Page/布拉格瓦茨拉夫·哈維爾國際機場.md "wikilink")）

### [東歐](../Page/東歐.md "wikilink")

  - [波蘭](../Page/波蘭.md "wikilink")

      - [華沙](../Page/華沙.md "wikilink")（[華沙肖邦機場](../Page/華沙肖邦機場.md "wikilink")）

  - [塞爾維亞](../Page/塞爾維亞.md "wikilink")

      - [貝爾格勒](../Page/貝爾格勒.md "wikilink")（[貝爾格勒國際機場](../Page/貝爾格勒國際機場.md "wikilink")）

  - [保加利亞](../Page/保加利亞.md "wikilink")

      - [索菲亞](../Page/索菲亞.md "wikilink")（[索菲亞機場](../Page/索菲亞機場.md "wikilink")）

  - [羅馬尼亞](../Page/羅馬尼亞.md "wikilink")

      - [布加勒斯特](../Page/布加勒斯特.md "wikilink")（[布加勒斯特奧托佩尼－亨利·科安德國際機場](../Page/布加勒斯特奧托佩尼－亨利·科安德國際機場.md "wikilink")）

  - [匈牙利](../Page/匈牙利.md "wikilink")

      - [布達佩斯](../Page/布達佩斯.md "wikilink")（[布達佩斯李斯特·費倫茨國際機場](../Page/布達佩斯李斯特·費倫茨國際機場.md "wikilink")）

  - [克羅埃西亞](../Page/克羅埃西亞.md "wikilink")

      - [薩格勒布](../Page/薩格勒布.md "wikilink")（[薩格勒布機場](../Page/薩格勒布機場.md "wikilink")）

  - [喬治亞共和國](../Page/喬治亞共和國.md "wikilink")

      - [第比利斯](../Page/第比利斯.md "wikilink")（[第比利斯國際機場](../Page/第比利斯國際機場.md "wikilink")）

  - [亞塞拜然](../Page/亞塞拜然.md "wikilink")

      - [阿塞拜疆](../Page/阿塞拜疆.md "wikilink")（[蓋達爾·阿利耶夫國際機場](../Page/蓋達爾·阿利耶夫國際機場.md "wikilink")）

  - [波斯尼亚和黑塞哥维那](../Page/波斯尼亚和黑塞哥维那.md "wikilink")

      - [萨拉热窝](../Page/萨拉热窝.md "wikilink")（[萨拉热窝国际机场](../Page/萨拉热窝国际机场.md "wikilink")）

### 兼跨歐亞

  - [土耳其](../Page/土耳其.md "wikilink")

      - [伊斯坦布尔](../Page/伊斯坦布尔.md "wikilink")
          - [伊斯坦布尔阿塔图尔克机场](../Page/伊斯坦布尔阿塔图尔克机场.md "wikilink")
          - [萨比哈·格克琴国际机场](../Page/萨比哈·格克琴国际机场.md "wikilink")
      - [安卡拉](../Page/安卡拉.md "wikilink")
        （[埃森博阿國際機場](../Page/埃森博阿國際機場.md "wikilink")）
      - 阿达纳（[阿达纳國際機場](../Page/:en:Adana_Şakirpaşa_Airport.md "wikilink")）

  - [塞普勒斯](../Page/塞普勒斯.md "wikilink")

      - [拉納卡](../Page/拉納卡.md "wikilink")
        （[拉納卡國際機場](../Page/拉納卡國際機場.md "wikilink")）

  - [俄罗斯](../Page/俄罗斯.md "wikilink")

      - [莫斯科](../Page/莫斯科.md "wikilink")
        （[多莫傑多沃国际机场](../Page/多莫傑多沃国际机场.md "wikilink")）
      - [圣彼得堡](../Page/圣彼得堡.md "wikilink")
        （[普尔科沃机场](../Page/普尔科沃机场.md "wikilink")）

## [美洲](../Page/美洲.md "wikilink")

### [北美洲](../Page/北美洲.md "wikilink")

  - [美国](../Page/美国.md "wikilink")

      - [紐約](../Page/紐約.md "wikilink")
        （[约翰·菲茨杰拉德·甘迺迪國際機場](../Page/约翰·菲茨杰拉德·甘迺迪國際機場.md "wikilink")）
      - [華盛頓](../Page/美國政府.md "wikilink")
        （[華盛頓杜勒斯國際機場](../Page/華盛頓杜勒斯國際機場.md "wikilink")）
      - [休士頓](../Page/休士頓.md "wikilink")
        （[喬治·布什洲際機場](../Page/喬治·布什洲際機場.md "wikilink")）
      - [芝加哥](../Page/芝加哥.md "wikilink")
        （[奥黑尔國際機場](../Page/奥黑尔國際機場.md "wikilink")）
      - [費城](../Page/費城.md "wikilink")
        （[費城國際機場](../Page/費城國際機場.md "wikilink")）
      - [达拉斯](../Page/达拉斯.md "wikilink")
        （[达拉斯-沃思堡国际机场](../Page/达拉斯-沃思堡国际机场.md "wikilink")）
      - [阿特蘭大](../Page/阿特蘭大.md "wikilink")
        （[傑克遜亞特蘭大國際機場](../Page/哈茨菲尔德-杰克逊亚特兰大国际机场.md "wikilink")）
      - [拉斯维加斯](../Page/拉斯维加斯.md "wikilink")
        （[麦卡伦国际机场](../Page/麦卡伦国际机场.md "wikilink")）\[10\]

<!-- end list -->

  - [加拿大](../Page/加拿大.md "wikilink")

      - [蒙特利爾](../Page/蒙特利爾.md "wikilink")
        （[蒙特利爾皮埃爾·埃利奧特·特魯多國際機場](../Page/蒙特利爾皮埃爾·埃利奧特·特魯多國際機場.md "wikilink")）

### [南美洲](../Page/南美洲.md "wikilink")

  - [阿根廷](../Page/阿根廷.md "wikilink")

      - [布宜諾斯艾利斯](../Page/布宜諾斯艾利斯.md "wikilink")
        （[埃塞薩皮斯塔里尼部長國際機場](../Page/埃塞薩皮斯塔里尼部長國際機場.md "wikilink")）

  - [巴西](../Page/巴西.md "wikilink")

      - [聖保羅](../Page/聖保羅.md "wikilink")
        （[聖保羅/瓜魯柳斯-安德烈·弗朗哥·蒙托羅州長國際機場](../Page/聖保羅/瓜魯柳斯-安德烈·弗朗哥·蒙托羅州長國際機場.md "wikilink")）
      - [里约热内卢](../Page/里约热内卢.md "wikilink")
        （[里约热内卢/加利昂国际机场](../Page/里约热内卢/加利昂-安东尼奥·卡洛斯·若比姆国际机场.md "wikilink")）\[11\]

  - [智利](../Page/智利.md "wikilink")

      - [圣地亚哥](../Page/圣地亚哥_\(智利\).md "wikilink")
        （[圣地亚哥-普达韦尔机场](../Page/阿图罗·梅里诺·贝尼特斯准将国际机场.md "wikilink")）\[12\]

## [大洋洲](../Page/大洋洲.md "wikilink")

  - [澳大利亚](../Page/澳大利亚.md "wikilink")

      - [墨尔本](../Page/墨尔本.md "wikilink")
        （[墨尔本机场](../Page/墨尔本机场.md "wikilink")）

      - [悉尼](../Page/悉尼.md "wikilink")
        （[悉尼國際機場](../Page/悉尼國際機場.md "wikilink")）

      - [伯斯](../Page/伯斯.md "wikilink")
        （[伯斯机场](../Page/伯斯机场.md "wikilink")）

      - [阿德萊德](../Page/阿德萊德.md "wikilink")
        （[阿德萊德機場](../Page/阿德萊德機場.md "wikilink")）

      - [堪培拉](../Page/堪培拉.md "wikilink")
        （[堪培拉机场](../Page/堪培拉机场.md "wikilink")）\[13\]

  - [紐西蘭](../Page/紐西蘭.md "wikilink")

      - [奧克蘭](../Page/奧克蘭_\(紐西蘭\).md "wikilink")（[奧克蘭國際機場](../Page/奧克蘭國際機場.md "wikilink")）

## 参考来源

## 外部链接

  - [图示卡塔尔航空通航城市（英语）](http://www.qatarairways.com/routemap/index.php)

[id:Bandara-bandara Tujuan Qatar
Airways](../Page/id:Bandara-bandara_Tujuan_Qatar_Airways.md "wikilink")

[Category:卡塔尔航空](../Category/卡塔尔航空.md "wikilink")
[Category:航空公司航点](../Category/航空公司航点.md "wikilink")

1.

2.

3.  [Qatar Airways’ First Flight to Phnom Penh
    Touches Down](https://www.cambodiadaily.com/archives/qatar-airways-first-flight-to-phnom-penh-touches-down-11271/)Cambodia
    Daily，2013年2月22日

4.  [Qatar Airways To Launch Service to Exotic Malaysian Holiday
    Destination
    Penang](https://www.qatarairways.com/en/press-releases/2017/Oct/qatar-airways-to-launch-service-to-exotic-malaysian-holiday-dest.html?activeTag=Press-releases)Qatar
    Airways，2017年10月17日

5.  [Qatar Airways launch direct daily flights from
    Doha](https://thethaiger.com/thai-life/Qatar-Airways-launch-direct-daily-flights-Doha?amp)Phuket
    Gazette，2014年

6.

7.

8.  [Qatar Airways launches Pattaya
    flights](http://www.nationmultimedia.com/detail/Travel_log/30330939)The
    Nation，2017年11月7日

9.

10.

11.

12.
13.