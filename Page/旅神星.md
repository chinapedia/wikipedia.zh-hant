****（Vibilia）是第144颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1875年6月3日发现。的[直径为](../Page/直径.md "wikilink")141.8千米，[质量为](../Page/质量.md "wikilink")3.0×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1579.562天。

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1875年发现的小行星](../Category/1875年发现的小行星.md "wikilink")