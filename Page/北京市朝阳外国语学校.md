<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>北京市朝阳外国语学校</strong><br />
Beijing Chaoyang Foreign Language School</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>建立时间</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>学校类型</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>校长</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>教师</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>学生</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>地点</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>校园</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>校园颜色</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>网站</p></td>
</tr>
</tbody>
</table>

**北京市朝阳外国语学校**([英语](../Page/英语.md "wikilink")：Beijing Chaoyang Foreign
Language
School)，位于[中国](../Page/中国.md "wikilink")[北京市的](../Page/北京市.md "wikilink")[朝阳区](../Page/朝阳区_\(北京市\).md "wikilink")，是一所朝阳区示范中学。

20世纪90年代，时任[中国国务院副总理的](../Page/中国国务院.md "wikilink")[李岚清提出](../Page/李岚清.md "wikilink")：“普及外语和培训外语人才，改革外语教学方法，提高外语教学水平，已经不是一般的教学问题。”而1997年，[中国教育部又作出](../Page/中国教育部.md "wikilink")“进一步办好外国语学校”的指示。
而其后的1998年，北京市朝阳外国语学校经[北京市教育局和朝阳区教委的许可](../Page/北京市教育局.md "wikilink")，在当年成立。2001年，该校的高中部随着第一届高中生的入学也宣告成立。而其后几年，该校也逐步建立了自己的小学部，同时完善了培训部门，使该校成为了一个由小学到高中并结合课外培训的全方位初等、中等教育服务机构。

## 现状

该校位于[北京](../Page/北京.md "wikilink")[北四环](../Page/北京四环路.md "wikilink")[奥运公园核心区内](../Page/奥运公园.md "wikilink")。拥有四处常设校址且相隔不远。每个教学区都配备了空调、机房、教室、图书馆等基本教学设施，初中部和高中部还另配有缩小尺寸的体育场，初中部所有教室都接入了[闭路电视系统](../Page/闭路电视.md "wikilink")。

2008年前，虽然学校具备了一个教育机构应该具有的基本硬件条件，但是在这些设施的利用保养以及后勤维护上，学校仍然不能让人满意。学校的足球场在铺设草皮之后疏于管理，很快就成为了荒草场，直到最后几乎没有了草。而且学校的伙食和住宿条件也多年遭到在校生的诟病。

2008年，由于[北京奥运会的举行](../Page/北京奥运会.md "wikilink")，学校周边环境得到进一步改善。学校内进行了装修，硬件设施得到了极大地改善，新设了塑胶篮球场以及人工草皮足球场。曾经遭受诟病的餐饮及住宿条件也得到了一定改善。

2008年12月21日，学校在北京国际会议中心举行了十周年校庆庆典\[1\]，成为学校发展史上的一个里程碑。

2010年1月7日，[北京市教育委员会正式作出批复](../Page/北京市教育委员会.md "wikilink")，要求该校停止体制改革试点，加入公立校行列\[2\]。

2012年，该校在北京[朝阳区北苑开设新校区](../Page/朝阳区.md "wikilink")\[3\]。

## 历年高考成绩

以下是最近四年和2013年该校高考成绩：\[4\]

  - 2005年 理工类学生有16人成绩超过600分，文科最高分为590分。理工总分平均分为548，文科总分平均分为526分。
  - 2006年 文史类最高分637，8人过600分，平均557分；理工类最高分693，51人过600分，平均588分。
  - 2007年 文史类最高分635，8人过600分，平均578分；理工类最高分690，52人过600分，平均588分。
  - 2008年 文史类1人过630分，平均572分；理工类24人过650分，平均627分。
  - 2010年 文史类3人过650分，平均580分；理工类平均618分。
  - 2013年 文史类1人过670分，平均分全市第七名，理工类平均分全市第四名。
  - 2014年 所有学生全部超过600分。

## 现任校长

[郝又明](../Page/郝又明.md "wikilink")，该校创始人，现任校长，自1998年创办学校并担任校长至今，获得“朝阳教育家”的称号。2018年朝外20周年校庆卸任。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [北京市朝阳外国语学校官方网站](http://www.cyfls.net)

[Category:北京中等学校](../Category/北京中等学校.md "wikilink")
[Category:北京小学](../Category/北京小学.md "wikilink")

1.  北京市朝阳外国语学校十周年校庆主题网站[1](http://www.cyfls.net/news/tenyears/index.html)
2.
3.
4.  北京市朝阳外国语学校官方网站[2](http://www.cyfls.net)