**米蘇拉** （Missoula,
Montana）是[美國](../Page/美國.md "wikilink")[蒙大拿州西部的一個城市](../Page/蒙大拿州.md "wikilink")、[米蘇拉縣縣治](../Page/米蘇拉縣.md "wikilink")。面積61.9平方公里，2006年人口64,081
人，是該州第二大城市。米苏拉县（Missoula County, Montana）是美国蒙大拿州西部的一个县，西南
邻爱达荷州。面积6,781平方公里。根据美国2000年人口普查，共有人口95,802。县治米苏拉
(Missoula)。 　　成立于1865年2月2日，是该州第一批成立的县之一。县名来自扁头族
(Flathead，操萨利什语的印第安部落)的“m-i-sul-e-etiku”，意思是“令人恐惧或者伏击的地方”，指的是地狱门峡谷—他们在那里有时会被黑脚族
(Blackfeet)袭击。

## 教育機構

[蒙大拿大學位於本市](../Page/蒙大拿大學.md "wikilink")。

  - [Confucius Institute](../Page/Confucius_Institute.md "wikilink")

## 姐妹城市

  - [內卡格明德](../Page/內卡格明德.md "wikilink")

  - [北帕莫斯頓](../Page/北帕莫斯頓.md "wikilink")

## 註釋

　　

[M](../Category/蒙大拿州城市.md "wikilink")