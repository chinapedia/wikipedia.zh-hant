**田剛**（），江苏南京人，中国数学家。[中國科學院院士](../Page/中國科學院.md "wikilink")。现任[北京大学副校长](../Page/北京大学.md "wikilink")。

## 生平

1958年出生在江苏[南京](../Page/南京.md "wikilink")，从小在[南京大学校园长大](../Page/南京大学.md "wikilink")，母亲是[南京大学数学系的教师](../Page/南京大学数学系.md "wikilink")，父亲则是农学专家。作为往届生，经历1977年落第之后于第二次高考中考上。为1978级，1982年夏季本科毕业于[南京大学数学系](../Page/南京大学.md "wikilink")，1984年获[北京大学硕士学位并赴](../Page/北京大学.md "wikilink")[聖地牙哥加利福尼亞大學攻读博士学位](../Page/聖地牙哥加利福尼亞大學.md "wikilink")，师从[數學家](../Page/數學家.md "wikilink")[丘成桐](../Page/丘成桐.md "wikilink")。后跟随[丘成桐转学至](../Page/丘成桐.md "wikilink")[哈佛大学](../Page/哈佛大学.md "wikilink")，并于1988年获美国[哈佛大学数学系博士学位](../Page/哈佛大学.md "wikilink")。自1998年起，受聘为教育部“[长江学者奖励计划](../Page/长江学者奖励计划.md "wikilink")”在北京大学的特聘教授。2003年至2018年,
田刚在[美國](../Page/美國.md "wikilink")[普林斯頓大學數學系任Higgins讲座教授](../Page/普林斯頓大學.md "wikilink")。現在，田刚是美国普林斯顿大学荣誉退休教授、[中國](../Page/中國.md "wikilink")[北京大學數學研究所及中国](../Page/北京大學.md "wikilink")[南京大学现代数学研究所所长](../Page/南京大学现代数学研究所.md "wikilink")、[北京大学北京国际数学研究中心主任](../Page/北京大学北京国际数学研究中心.md "wikilink")。2017年2月，任北京大学副校长。\[1\]

## 荣誉

2012年出任国际数学界奖金最高（100万美元）的权威大奖[阿贝尔奖仅有的](../Page/阿贝尔奖.md "wikilink")5位评委之一，成为中国籍数学家出任该奖评委第一人。

曾获美国国家基金委1994年度[沃特曼奖](../Page/沃特曼奖.md "wikilink")；1996年，他获[美国数学会的](../Page/美国数学会.md "wikilink")[奥斯瓦尔德·维布伦奖](../Page/奥斯瓦尔德·维布伦奖.md "wikilink")。2002年，成为第24届国际数学家大会的“1小时大会报告”特邀报告人。2001年，当选为[中国科学院院士](../Page/中国科学院.md "wikilink")。2004年当选为[美国文理科学院院士](../Page/美国文理科学院.md "wikilink")。

## 社会职位

是[第十一届](../Page/第十一届全国政协.md "wikilink")、[第十二届全国政协常委](../Page/第十二届全国政协.md "wikilink")。2010年2月加入[中国民主同盟](../Page/中国民主同盟.md "wikilink")，2012年当选中国民主同盟中央委员会副主席。

## 参见

  - [田刚丘成桐事件](../Page/田刚丘成桐事件.md "wikilink")

## 参考来源

## 外部連結

  - [M.I.T. home page for Gang Tian](http://www-math.mit.edu/~tian/)
  - [田剛盗作文書について](http://www1.bbsland.com/education/messages/245256.html)
  - [田剛盗作Todorov文書](http://www.xys.org/forum/db/194/230.html)

{{-}}

[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:江苏科学家](../Category/江苏科学家.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十二届全国政协委员](../Category/第十二届全国政协委员.md "wikilink")
[Category:中華人民共和國數學家](../Category/中華人民共和國數學家.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:长江学者讲座教授](../Category/长江学者讲座教授.md "wikilink")
[Category:北京大学副校长](../Category/北京大学副校长.md "wikilink")
[Category:中国民主同盟副主席](../Category/中国民主同盟副主席.md "wikilink")
[G刚](../Category/田姓.md "wikilink")
[Category:南京人](../Category/南京人.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")
[Category:奥斯瓦尔德·维布伦几何学奖获得者](../Category/奥斯瓦尔德·维布伦几何学奖获得者.md "wikilink")

1.