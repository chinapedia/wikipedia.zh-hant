，是根據[中華民國作家](../Page/中華民國.md "wikilink")[瓊瑤的小說](../Page/瓊瑤.md "wikilink")《[一簾幽夢](../Page/一簾幽夢.md "wikilink")》改編的[電視連續劇](../Page/電視連續劇.md "wikilink")，剧情围绕汪家两姐妹汪绿萍和汪紫菱展开，二人同时爱上了楚家长子楚濂，此时费云帆又闖進了三角關係。

曾有 1975 年電影版 (
[甄珍](../Page/甄珍.md "wikilink")、[謝賢](../Page/謝賢.md "wikilink")、[汪萍](../Page/汪萍.md "wikilink")、[秦祥林主演](../Page/秦祥林.md "wikilink")
) ，和 1996 年電視劇版 (
[陳德容](../Page/陳德容.md "wikilink")、[劉德凱](../Page/劉德凱.md "wikilink")、[蕭薔](../Page/蕭薔.md "wikilink")、[林瑞陽主演](../Page/林瑞陽.md "wikilink")
)。

## 創作經歷

[湖南衛視爲了這部劇](../Page/湖南衛視.md "wikilink")，曾舉辦了《尋找紫菱》選秀節目來選出劇中女主角“紫菱”的扮演者張嘉倪。

《又見一簾幽夢》於 2007 年 6 月 30
日在[湖南衛視首播](../Page/湖南衛視.md "wikilink")，成為第一部在中華人民共和國首映的瓊瑤劇
( 中華民國於同年 7 月 11 日在[中華電視公司上映](../Page/中華電視公司.md "wikilink") )，共46集。

劇組遠赴[法國](../Page/法國.md "wikilink")[巴黎和素有](../Page/巴黎.md "wikilink")[花都美譽的](../Page/花都.md "wikilink")[普羅旺斯取景](../Page/普羅旺斯.md "wikilink")，萬紫千紅的花海，以及[凱旋門](../Page/凱旋門.md "wikilink")、[艾菲爾鐵塔](../Page/艾菲爾鐵塔.md "wikilink")、[羅浮宮](../Page/羅浮宮.md "wikilink")、[聖母院](../Page/聖母院.md "wikilink")、[塞納河等名勝盡收眼底](../Page/塞納河.md "wikilink")。

本劇故事主要背景由中華民國[台北市轉移中華人民共和國](../Page/台北市.md "wikilink")[上海市](../Page/上海市.md "wikilink")，加入上一代的愛情糾葛情節，並配合時代大量加入了使用[手機](../Page/手機.md "wikilink")、[MSN](../Page/MSN.md "wikilink")、[視訊聊天](../Page/視訊.md "wikilink")、視訊會議等植入式廣告情節；故事雖然和原著小說相差甚大，但情節更加緊湊而浩大，相較於
1975 年電影版和 1996 年電視劇版，實有不同之感。

## 演員表

### 主要演員

|                                  |        |                                  |              |
| -------------------------------- | ------ | -------------------------------- | ------------ |
| **演員**                           | **角色** | **《一帘幽梦》对应角色**                   | **《一帘幽梦》演员** |
| [保劍鋒](../Page/保劍鋒.md "wikilink") | 楚濂     | [林瑞陽](../Page/林瑞陽.md "wikilink") |              |
| [張嘉倪](../Page/張嘉倪.md "wikilink") | 汪紫菱    | [陳德容](../Page/陳德容.md "wikilink") |              |
| [方中信](../Page/方中信.md "wikilink") | 费云帆    | [刘德凯](../Page/刘德凯.md "wikilink") |              |
| [秦岚](../Page/秦岚.md "wikilink")   | 汪绿萍    | [萧蔷](../Page/萧蔷.md "wikilink")   |              |
|                                  |        |                                  |              |

### 其他演員

|                                  |          |                                  |                                  |
| -------------------------------- | -------- | -------------------------------- | -------------------------------- |
| **演員**                           | **角色**   | **《一帘幽梦》对应角色**                   | **《一帘幽梦》演员**                     |
| [曾之喬](../Page/曾之喬.md "wikilink") | 劉雨珊\[1\] | 戴晓妍\[2\]                         | [陈仙梅](../Page/陈仙梅.md "wikilink") |
| [孫佺](../Page/孫佺.md "wikilink")   | 楚沛       | [钟本伟](../Page/钟本伟.md "wikilink") |                                  |
| [黃勐](../Page/黃勐.md "wikilink")   | 陶劍波      | [陈威安](../Page/陈威安.md "wikilink") |                                  |
| [吳冕](../Page/吳冕.md "wikilink")   | 舜絹       | [李麗鳳](../Page/李麗鳳.md "wikilink") |                                  |
| [曹穎](../Page/曹穎.md "wikilink")   | 蓉兒       | [王秀峰](../Page/王秀峰.md "wikilink") |                                  |
| [安翅](../Page/安翅.md "wikilink")   | 費雲舟      | [康殿宏](../Page/康殿宏.md "wikilink") |                                  |
| [博弘](../Page/博弘.md "wikilink")   | 雅芙       | [安慧敏](../Page/安慧敏.md "wikilink") |                                  |
| [張晨光](../Page/張晨光.md "wikilink") | 汪展鵬      | [勾峰](../Page/勾峰.md "wikilink")   |                                  |
| [劉威葳](../Page/劉威葳.md "wikilink") | 沈隨心      | 秦雨秋                              | [刘玉婷](../Page/刘玉婷.md "wikilink") |
| [劉忠虎](../Page/劉忠虎.md "wikilink") | 楚尚德      | [寇恒禄](../Page/寇恒禄.md "wikilink") |                                  |
| [鄭衛莉](../Page/鄭衛莉.md "wikilink") | 心怡       | [江霞](../Page/江霞.md "wikilink")   |                                  |
| [廖景萱](../Page/廖景萱.md "wikilink") | 小錦       |                                  |                                  |
| [呂紅旭](../Page/呂紅旭.md "wikilink") | 阿秀       |                                  |                                  |
| [朱曉雪](../Page/朱曉雪.md "wikilink") | 小雪       |                                  |                                  |
| [陳思璇](../Page/陳思璇.md "wikilink") | 思璇       |                                  |                                  |
| [傅詩芮](../Page/傅詩芮.md "wikilink") | 方小姐      |                                  |                                  |

## 主題曲

註明：以**台灣版**的為主

|         |                         |                                      |                                                                       |                                  |                                    |
| ------- | ----------------------- | ------------------------------------ | --------------------------------------------------------------------- | -------------------------------- | ---------------------------------- |
| **曲別**  | **歌名**                  | **作詞**                               | **作曲**                                                                | **編曲**                           | **演唱**                             |
| **片頭曲** | 《一簾幽夢》                  | [瓊瑤](../Page/瓊瑤.md "wikilink")       | [劉家昌](../Page/劉家昌.md "wikilink")                                      |                                  | [同恩](../Page/同恩.md "wikilink")     |
| **片尾曲** | 《一簾幽夢 之 這種感覺就是愛》(前期)    | 瓊瑤                                   | [陳志遠](../Page/陳志遠.md "wikilink")                                      | 陳志遠                              | [曾之喬](../Page/曾之喬.md "wikilink")   |
| **片尾曲** | 《一簾幽夢 之 相遇的魔咒（浪漫版）》(後期) | 瓊瑤                                   | 陳志遠                                                                   | 陳志遠                              | [張嘉倪](../Page/張嘉倪.md "wikilink")   |
| **插曲**  | 《E貓兒》                   | [沈芷凝](../Page/沈芷凝.md "wikilink")     | [磐子](../Page/磐子.md "wikilink")、[Brandy](../Page/Brandy.md "wikilink") |                                  | 曾之喬                                |
| **插曲**  | 《錯》                     | 瓊瑤                                   | [李榮浩](../Page/李榮浩.md "wikilink")                                      | [王繼康](../Page/王繼康.md "wikilink") | [范逸臣](../Page/范逸臣.md "wikilink")   |
| **插曲**  | 《Every breath you take》 | [Sting](../Page/Sting.md "wikilink") | Sting                                                                 |                                  | [藤田惠美](../Page/藤田惠美.md "wikilink") |
| **插曲**  | 《愛情來了》                  | [彭靖惠](../Page/彭靖惠.md "wikilink")     | 彭靖惠                                                                   |                                  | 彭靖惠                                |
| **插曲**  | 《一簾幽夢 之 相遇的魔咒（奔放版）》     | 瓊瑤                                   | 陳志遠                                                                   | [黃忠岳](../Page/黃忠岳.md "wikilink") | 范逸臣                                |
| **插曲**  | 《夢女孩》                   | 瓊瑤                                   | [曹俊鴻](../Page/曹俊鴻.md "wikilink")                                      |                                  | [馬毓芬](../Page/馬毓芬.md "wikilink")   |
| **插曲**  | 《錯錯錯》                   | 瓊瑤                                   | 曹俊鴻                                                                   |                                  | 馬毓芬                                |
| **插曲**  | 《崩潰》（配樂曲）               |                                      | 陳志遠                                                                   | 陳志遠                              |                                    |
| **插曲**  | 《火鳥》（配樂曲）               |                                      | [Ricky Ho](../Page/Ricky_Ho.md "wikilink")                            | Ricky Ho                         |                                    |

註：本劇所有歌曲，收錄於《又見一簾幽夢》電視原聲帶（2007年7月13日發行）。

## 上海屏播事件

2007年7月27日中國湖南衛視播出《又見一簾幽夢》大結局。中國[上海地區出現電視信號中斷](../Page/上海.md "wikilink")。使得上海的電視觀眾無法在第一時間收看到大結局。此事，是繼湖南衛視「超級女聲」之後，第二次發生上海地區湖南衛視信號卡斷遭屏弊的事件。

## 收視率

### 重播收視率

<table>
<thead>
<tr class="header">
<th><p>播映日期</p></th>
<th><p>週數</p></th>
<th><p>平均收視率</p></th>
<th><p>排名</p></th>
<th><p>集數</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2011年9月26日－09月30日</p></td>
<td><p>1</p></td>
<td><p>0.44</p></td>
<td><p>-</p></td>
<td><p>01-05</p></td>
<td><p>9月26、27日播出時間為21：30-22：00<br />
9月28日-9月30日播出時間為21：00-22：00</p></td>
</tr>
<tr class="even">
<td><p>2011年10月3日－10月7日</p></td>
<td><p>2</p></td>
<td><p><font color=blue><strong>0.30</strong></font></p></td>
<td><p>5</p></td>
<td><p>06-10</p></td>
<td><p>10月3日播出時間為21:00-22:00<br />
10月4日播出時間為21:20-22:00<br />
10月5日起播出120分鐘</p></td>
</tr>
<tr class="odd">
<td><p>2011年10月10日－10月14日</p></td>
<td><p>3</p></td>
<td><p>0.42</p></td>
<td><p>5</p></td>
<td><p>11-15</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年10月17日－10月21日</p></td>
<td><p>4</p></td>
<td><p>0.39</p></td>
<td><p>6</p></td>
<td><p>16-20</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年10月24日－10月28日</p></td>
<td><p>5</p></td>
<td><p><font color=red><strong>0.53</strong></font></p></td>
<td><p>6</p></td>
<td><p>21-25</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>平均收視</p></td>
<td><p>0.42</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - <small>**同時段節目：**
      - [三立台灣台](../Page/三立台灣台.md "wikilink")《[家和萬事興](../Page/家和萬事興_\(2010年電視劇\).md "wikilink")》
      - [民視](../Page/民視無線台.md "wikilink")《[父與子](../Page/父與子_\(民視電視劇\).md "wikilink")》
      - [台視](../Page/台視主頻.md "wikilink")：
          - 週一～四《[田-{庄}-英雄](../Page/田庄英雄.md "wikilink")》
          - 週五《[百萬小學堂](../Page/百萬小學堂.md "wikilink")》
      - [中視](../Page/中視數位台.md "wikilink")《[巾幗梟雄](../Page/巾幗梟雄.md "wikilink")／[巾幗梟雄之義海豪情](../Page/巾幗梟雄之義海豪情.md "wikilink")》
      - [大愛](../Page/大愛衛星電視.md "wikilink")《[愛的界線](../Page/愛的界線.md "wikilink")／[畫人生](../Page/畫人生.md "wikilink")》
  - **由[AGB尼爾森調查](../Page/AGB尼爾森.md "wikilink")，調查範圍是四歲以上收看電視之觀眾。**
  - **資料來源：[中時娛樂](https://web.archive.org/web/20130126141713/http://showbiz.chinatimes.com/2009Cti/Channel/Showbiz/showbiz-rank/0%2C5137%2Ctv%2C00.html)**</small>

## 參考來源

## 外部連結

  - [《又見一簾幽夢》華視专题](http://event.cts.com.tw/newromance/index.html)
  - [《又見一簾幽夢》新浪微博](http://v.blog.sina.com.cn/h/linx/yilian.html)
  - [《又見一簾幽夢》湖南衛視专题](http://www.hunantv.com/lanmu/yjylym/)

[Category:2007年中國電視劇集](../Category/2007年中國電視劇集.md "wikilink")
[Category:華視外購電視劇](../Category/華視外購電視劇.md "wikilink")
[Category:瓊瑤小說改編電視劇](../Category/瓊瑤小說改編電視劇.md "wikilink")
[Category:臺灣海峽兩岸合拍電視劇](../Category/臺灣海峽兩岸合拍電視劇.md "wikilink")

1.  《又见一帘幽梦》中为沈隨心的女儿，楚沛女朋友
2.  《一帘幽梦》中为秦雨秋的外甥女，楚沛女朋友