## 大事记

### 按照地区分类

#### 罗马帝国

  - [罗马派遣军队征服](../Page/罗马.md "wikilink")[易北河与](../Page/易北河.md "wikilink")[多瑙河流域](../Page/多瑙河.md "wikilink")[日耳曼人](../Page/日耳曼.md "wikilink")。

  - [奧古斯都召唤](../Page/奧古斯都.md "wikilink")[提比里烏斯来罗马](../Page/提比里烏斯.md "wikilink")，并指定其为继承人。同时，[玛尔库斯·维普撒尼乌斯·阿格里帕的小儿子](../Page/玛尔库斯·维普撒尼乌斯·阿格里帕.md "wikilink")也被指定为继承人。

  - 提比里乌斯也指定[日耳曼尼库斯为继承人](../Page/日耳曼尼库斯.md "wikilink")。

  - 为[奴隶制度制定了](../Page/奴隶制度.md "wikilink")[奴隶解放的规章条例](../Page/奴隶解放.md "wikilink")。

  - 提比里乌斯代表[罗马帝国与由其王塞吉莫尔代表的日耳曼部落](../Page/罗马帝国.md "wikilink")签订互不侵犯条约，并建立友好关系。塞吉莫尔的儿子，[阿米尼乌斯和弗拉乌斯被带进罗马军队当附属部队的领导者](../Page/阿米尼乌斯.md "wikilink")。

  - [大茱莉亚流亡后带着耻辱回到](../Page/大茱莉亚.md "wikilink")[雷焦卡拉布里亚](../Page/雷焦卡拉布里亚.md "wikilink")。

  - 奥古斯都赦免，因为他涉嫌[马尔库斯·埃米利乌斯·雷必达的孙女](../Page/马尔库斯·埃米利乌斯·雷必达.md "wikilink")对皇帝的阴谋的案件。

  - 被任命为亚洲地区总督。

  - [奧古斯都当年停閏](../Page/奧古斯都.md "wikilink")，2月僅有28日，無2月29日。\[1\]

  - 波利亚努斯·马拉多纽斯成为[雅典执政官](../Page/雅典执政官列表.md "wikilink")。

#### 朝鲜半岛

  - [新罗王](../Page/新罗.md "wikilink")[赫居世居世干卒](../Page/赫居世居世干.md "wikilink")，其子[南解次次雄继位](../Page/南解次次雄.md "wikilink")。

#### 中东

  - [安息国王](../Page/帕提亚.md "wikilink")[弗拉特斯五世和女王](../Page/弗拉特斯五世.md "wikilink")被推翻并杀害，王位被提议由继承——空位期开始。

#### 中国

  - [汉平帝娶](../Page/汉平帝.md "wikilink")[王莽之女](../Page/王莽.md "wikilink")[王嬿为妻](../Page/孝平王皇后.md "wikilink")，巩固他的统治。
  - [王莽加號](../Page/王莽.md "wikilink")[宰衡](../Page/宰衡.md "wikilink")，位在諸侯王公之上。大力宣揚禮樂教化，得到[儒生的擁戴](../Page/儒生.md "wikilink")，被加[九錫](../Page/九錫.md "wikilink")。
  - [王莽奏起](../Page/王莽.md "wikilink")[明堂](../Page/明堂.md "wikilink")、[辟雍](../Page/辟雍.md "wikilink")、[靈臺](../Page/靈臺.md "wikilink")，為學者築舍萬區。他還奏立[樂經](../Page/樂經.md "wikilink")，並增加[博士員額](../Page/博士.md "wikilink")，每經各設博士五人。徵天下通[六藝之一教授十一人以上](../Page/六藝.md "wikilink")；有通曉《[逸禮](../Page/逸禮.md "wikilink")》、《[古書](../Page/古書.md "wikilink")》、《[毛詩](../Page/毛詩.md "wikilink")》、《[周官](../Page/周官.md "wikilink")》、《[爾雅](../Page/爾雅.md "wikilink")》、[天文](../Page/天文.md "wikilink")、[圖讖](../Page/圖讖.md "wikilink")、[鍾律](../Page/鍾律.md "wikilink")、[月令](../Page/月令.md "wikilink")、[兵法](../Page/兵法.md "wikilink")、《[史籀篇](../Page/史籀篇.md "wikilink")》[大篆文字之意者](../Page/大篆.md "wikilink")，皆以[公家馬車載送](../Page/公車_\(漢朝\).md "wikilink")。一時間，“天下異能之士”上千人獲得徵召。

### 按照主题分类

#### 科学和艺术

  - 编写了十五卷**《世界历史》**。

## 出生

  - [南解次次雄](../Page/南解次次雄.md "wikilink")，新羅第2代君主。（死于[24年](../Page/24年.md "wikilink"),20岁）
  - [高无恤](../Page/大武神王.md "wikilink")，高句丽王。（死于[48年](../Page/48年.md "wikilink"),44岁）
  - [科鲁迈拉](../Page/科鲁迈拉.md "wikilink")，罗马作家。（死于[80年](../Page/80年.md "wikilink"),76岁）

## 逝世

  - [赫居世居世干](../Page/赫居世居世干.md "wikilink")，新罗第一任王。（生于[前69年](../Page/前69年.md "wikilink"),73岁）

  - ，[玛尔库斯·维普撒尼乌斯·阿格里帕和](../Page/玛尔库斯·维普撒尼乌斯·阿格里帕.md "wikilink")[大茱莉亚的儿子](../Page/大茱莉亚.md "wikilink")。（生于[前20年](../Page/前20年.md "wikilink"),24岁）

  - ，罗马演说家、[诗人和](../Page/诗人.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")。（生于[前65年](../Page/前65年.md "wikilink"),61岁）

  - ，[西塞罗的第一任妻子](../Page/西塞罗.md "wikilink")。（生于[前98年](../Page/前98年.md "wikilink"),102岁）

## 注释

<references />

## 参见

  - [4年国家领导人列表](../Page/4年国家领导人列表.md "wikilink")

[\*](../Category/4年.md "wikilink") [4年](../Category/0年代.md "wikilink")
[0](../Category/1世纪各年.md "wikilink") [0](../Category/甲子年.md "wikilink")

1.  [「兩千年中西曆轉換」資料庫介紹](http://www.sinica.edu.tw/~tdbproj/sinocal/lusodoc.html)