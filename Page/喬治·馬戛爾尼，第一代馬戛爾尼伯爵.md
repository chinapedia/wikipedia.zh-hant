**乔治·馬戛爾尼，第一代馬戛爾尼伯爵**，（；），[英国](../Page/英国.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")、[外交官](../Page/外交官.md "wikilink")。

## 早年生平

马戛尔尼出身于一个[苏格兰贵族家庭](../Page/苏格兰.md "wikilink")，在[爱尔兰出生](../Page/爱尔兰.md "wikilink")。1759年，他毕业于[都柏林](../Page/都柏林.md "wikilink")[三一学院](../Page/三一学院.md "wikilink")，之后进入[伦敦](../Page/伦敦.md "wikilink")[坦普尔大学进修](../Page/坦普尔大学.md "wikilink")，师从荷兰伯爵[亨利·福克斯](../Page/亨利·福克斯.md "wikilink")。

1764年，他被任命为全权特使，赴[俄国与](../Page/俄罗斯帝国.md "wikilink")[叶卡捷琳娜二世商谈结盟事宜](../Page/叶卡捷琳娜二世.md "wikilink")。之后他进入[英国议会](../Page/英国议会.md "wikilink")。1769年，他返回爱尔兰出任[爱尔兰议会议员](../Page/爱尔兰议会.md "wikilink")，并出任[爱尔兰事务大臣](../Page/爱尔兰事务大臣.md "wikilink")。1772年，在他辞职之后，被封为[骑士](../Page/骑士.md "wikilink")。

1775年，他出任[加勒比群岛总督](../Page/加勒比群岛.md "wikilink")，1776年被封为“马戛尔尼男爵”，属爱尔兰贵族序列。1780年，出任[印度](../Page/印度.md "wikilink")[马德拉斯总督](../Page/马德拉斯.md "wikilink")，驻今[昌奈](../Page/昌奈.md "wikilink")。1786年，他拒绝出任[印度总督](../Page/印度总督.md "wikilink")，返回英国。1792年，他被加封为“马戛尔尼伯爵”。

## 出使中国

1793年，英王[喬治三世以祝壽為由](../Page/喬治三世.md "wikilink")，派遣馬加爾尼為正使，率領使團到訪[清朝覲見](../Page/清.md "wikilink")[乾隆皇帝](../Page/乾隆皇帝.md "wikilink")，實則要求中國允許英國派駐公使。\[1\]但乾隆帝以「不合體制」為由，所請事項一律不准。

## 晚年经历

1795年，马戛尔尼作为密使出访[撒丁王国](../Page/撒丁王国.md "wikilink")，商谈[反法同盟事宜](../Page/反法同盟.md "wikilink")，之后他再次被封为“马戛尔尼男爵”，不过这次是作为[大不列顛贵族受封](../Page/大不列顛.md "wikilink")。

1796年底，他被任命为[好望角总督](../Page/好望角.md "wikilink")，统治新获得的[开普殖民地](../Page/开普殖民地.md "wikilink")。1798年11月，他因健康原因辞职。1806年逝世。

## 著作

  - 《[一七九三乾隆英使觐见记](../Page/一七九三乾隆英使觐见记.md "wikilink")》，有[刘半农的中文译本](../Page/刘半农.md "wikilink")。

## 其他

其后裔[简·马戛尔尼](../Page/简·马戛尔尼.md "wikilink")（Jane
Macartney，中文名马珍）於2008年5月開始為《[泰晤士報](../Page/泰晤士報.md "wikilink")》駐[北京特派](../Page/北京.md "wikilink")[記者](../Page/記者.md "wikilink")，[汶川大地震之際做了許多相關報道](../Page/汶川大地震.md "wikilink")。

## 参考文献

## 研究書目

  - Cranmer-Byng, J. L. "Lord Macartney’s Embassy to Peking in 1793."
    Journal of Oriental Studies. Vol. 4, Nos. 1,2 (1957-58): 117-187.
  - Hevia, James Louis. (1995). Cherishing Men from Afar: Qing Guest
    Ritual and the Macartney Embassy of 1793. Durham: Duke University
    Press.
      - 中文版：

## 外部連結

  - [黃一農](../Page/黃一農.md "wikilink")：〈[印象與真相──清朝中英兩國的覲禮之爭](https://web.archive.org/web/20160304193857/http://hss.nthu.edu.tw/~ylh/uploadfiles/paper202_0.pdf)〉。
  - 黃一農：〈[龍與獅對望的世界：以馬戛爾尼使團訪華後的出版物為例](https://web.archive.org/web/20140827001822/http://hss.nthu.edu.tw/~ylh/uploadfiles/paper169_0.pdf)〉。
  - 王宏志：〈[馬戛爾尼使華的翻譯問題](http://www.mh.sinica.edu.tw/MHDocument/PublicationDetail/PublicationDetail_74.pdf)〉。
  - 葉曉青：〈[《四海昇平》──乾隆為瑪噶爾尼而編的朝貢戲](http://www.cuhk.edu.hk/ics/21c/media/articles/c105-200711023.pdf)〉。

## 参见

  - [阿美施德](../Page/阿美施德.md "wikilink")
  - [朝贡体系](../Page/朝贡体系.md "wikilink")
  - [蒂進](../Page/蒂進.md "wikilink")：荷蘭兼[荷屬東印度公司大使](../Page/荷屬東印度公司.md "wikilink")，率使團賀乾隆大壽

{{-}}

[Category:英国外交官](../Category/英国外交官.md "wikilink")
[Category:愛爾蘭伯爵](../Category/愛爾蘭伯爵.md "wikilink")
[Category:巴斯騎士](../Category/巴斯騎士.md "wikilink")
[Category:不列顛東印度公司相關人物](../Category/不列顛東印度公司相關人物.md "wikilink")
[Category:都柏林三一学院校友](../Category/都柏林三一学院校友.md "wikilink")

1.