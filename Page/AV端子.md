[Composite.jpg](https://zh.wikipedia.org/wiki/File:Composite.jpg "fig:Composite.jpg")
[Composite_video_connector.jpg](https://zh.wikipedia.org/wiki/File:Composite_video_connector.jpg "fig:Composite_video_connector.jpg")
**AV端子**（Composite video
connector，又稱**复合端子**），是家用[影音](../Page/影音.md "wikilink")[電器用來傳送](../Page/電器.md "wikilink")[視訊](../Page/視訊.md "wikilink")[模拟信号](../Page/模拟信号.md "wikilink")（如[NTSC](../Page/NTSC.md "wikilink")、[PAL](../Page/PAL.md "wikilink")、[SECAM](../Page/SECAM.md "wikilink")）的常見[端子](../Page/端子.md "wikilink")。AV端子通常采用黃色的[RCA端子传送视频信号](../Page/RCA端子.md "wikilink")，另外配合兩條紅色與白色的RCA端子傳送音訊，亦合稱為**三色線/紅白黃線**。[歐洲的](../Page/歐洲.md "wikilink")[電視機通常以](../Page/電視機.md "wikilink")取代RCA端子，不過SCART的設計上可以載送畫質比[YUV更好的](../Page/YUV.md "wikilink")[RGB訊號](../Page/RGB.md "wikilink")，故也被用來連接[顯示器](../Page/顯示器.md "wikilink")、[電視遊樂器或](../Page/電視遊樂器.md "wikilink")[DVD播放機](../Page/DVD播放機.md "wikilink")。在專業應用當中，也有使用[BNC端子以求獲得更佳訊號品質](../Page/BNC連接器.md "wikilink")。

在AV端子中傳送的是類比電視訊號的三個來源要素：Y、U、V，以及作為[同步化基準的](../Page/同步化.md "wikilink")[脈衝信號](../Page/脈衝.md "wikilink")。Y代表影像的*亮度（luminance，又稱brightness）*，並且包含了同步脈衝，只要有Y信號存在就可以看到黑白的電視影像（事實上，這是彩色電視與早期黑白電視相容的方法）。U信號與V信號之間承載了顏色的資料，U和V先被混合成一個信號中的兩組[正交相位](../Page/正交.md "wikilink")（此混合後的信號稱為*彩度(chrominance)*），再與Y信號作加總。因為Y是基頻信號而UV是與載波混合在一起，所以這個加總的動作等同於[分頻多工](../Page/FDM.md "wikilink")。

[Composite_Video.svg](https://zh.wikipedia.org/wiki/File:Composite_Video.svg "fig:Composite_Video.svg")
AV端子所傳送的複合視訊可以藉由簡單地[調變其](../Page/調變.md "wikilink")[載波來將其導引至任何一個電視機的頻道](../Page/載波.md "wikilink")。早期大多數的家用視訊裝置都是使用複合視訊。例如[鐳射影碟就是完整的將複合視訊數位化](../Page/鐳射影碟.md "wikilink")，[VHS錄影帶則是記錄稍微修改過的複合視訊](../Page/VHS.md "wikilink")。這些播放裝置大多數可以選擇是直接輸出其記錄的訊號，或者調變至特定的電視頻道以供沒有AV端子專屬頻道的早期電視機收看。在1980年代早期，當時的個人電腦與電視遊樂器通常也輸出複合視訊，使用者必須使用一台[RF調變器來將其載波導向至電視的特定頻道](../Page/RF調變器.md "wikilink")，在[北美常為第](../Page/北美.md "wikilink")3或第4頻道（66\~72Mhz），歐洲為36頻道，[日本日規第](../Page/日本.md "wikilink")1或第2頻道（90\~102MHz）；[台灣則因當時僅開放](../Page/台灣.md "wikilink")1\~7CH（即美規[VHF](../Page/VHF.md "wikilink")
7\~13CH，但7\~12CH被[臺視](../Page/臺視.md "wikilink")、[中視](../Page/中視.md "wikilink")、[華視使用中](../Page/華視.md "wikilink")），故僅剩第13頻道（210\~216MHz）可用。RF調變器通常為外接盒形式，以免其電波影響電視機的運作。不過，將複合訊號進行調變再讓電視解調的結果，是引入更多的雜訊，使得畫質失真。所以1980年代後期，電視機開始提供直接的AV輸入端口，並且將其與天線或有線電視所傳送的RF信號分開處理，使得RF調變器慢慢消失。

雖然RF調變所造成的失真已經不再普遍，複合信號本身將YUV信號混合在一起的設計本身就造成了畫質上的減損；因為，加總之後的信號在數學上即無法完全分離回原來的樣子，造成本來應該是亮度的信號被解釋為彩度，反之亦然，既无法做到完全的亮色分离。表現在畫面上就是物件邊緣滲色、彩虹化，或亮度不穩定。電視機製造廠商一方面改善影像處理電路（如**三次元Y-C分離回路**）來極力降低此等影響，另一方面也提出了[S端子與](../Page/S端子.md "wikilink")[色差端子來根本解決以上問題](../Page/色差端子.md "wikilink")。

[Category:類比顯示接口](../Category/類比顯示接口.md "wikilink")