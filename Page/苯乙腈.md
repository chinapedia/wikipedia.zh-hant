**苯乙腈**（Benzyl
cyanide或phenylacetonitrile或α-tolunitrile，[化學式](../Page/化學式.md "wikilink")：[C](../Page/碳.md "wikilink")<sub>8</sub>[H](../Page/氫.md "wikilink")<sub>7</sub>[N](../Page/氮.md "wikilink")，俗稱**苄腈**、**苄基氰**、**氰化苄**、**α-苄基腈**）是一種[有機化合物](../Page/有機化合物.md "wikilink")。它是無色油狀液體，有芬芳氣味，並且有毒性。苯乙腈會造成皮膚與眼睛的燒傷，如果吸入或吞入有可能會致命。它在燃燒時也會產生[氰化氫](../Page/氰化氫.md "wikilink")。

## 用途

苯乙腈是產生[苯巴比妥](../Page/苯巴比妥.md "wikilink")（Phenobarbital）、[派醋甲酯](../Page/派醋甲酯.md "wikilink")（Methylphenidate）與許多[苯丙胺化合物的](../Page/苯丙胺.md "wikilink")[中間體](../Page/中間體.md "wikilink")，並列為[DEA
list of chemicals之中](../Page/DEA_list_of_chemicals.md "wikilink")。
该化合物可作为制造甲基苯丙胺中间体苯基丙酮的起始原料 。参与碳负离子反应。

## 外部連結

  - [苯乙腈的EPA化學檔案](https://web.archive.org/web/20041112074019/http://yosemite.epa.gov/oswer/CeppoEHS.nsf/Profiles/140-29-4?OpenDocument)

[Category:腈](../Category/腈.md "wikilink")
[Category:芳香化合物](../Category/芳香化合物.md "wikilink")