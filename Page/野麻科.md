**野麻科**又名[羽叶麻科](../Page/羽叶麻科.md "wikilink")，只有1[属](../Page/属.md "wikilink")2[种](../Page/种.md "wikilink")，分布在暖[温带地区](../Page/温带.md "wikilink")。其中[剃刀草](../Page/剃刀草.md "wikilink")（*Datisca
cannabina*）分布在[中亚和](../Page/中亚.md "wikilink")[西亚的](../Page/西亚.md "wikilink")[小亚细亚半岛周围](../Page/小亚细亚半岛.md "wikilink")；[北美假大麻](../Page/北美假大麻.md "wikilink")（*Datisca
glomerata*）分布在[北美洲西海岸](../Page/北美洲.md "wikilink")。

本可4[植物为高大](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，可以高达2[米](../Page/米.md "wikilink")，复[叶互生](../Page/叶.md "wikilink")；[花小单性](../Page/花.md "wikilink")，生长在花轴上，雄花[花瓣](../Page/花瓣.md "wikilink")3–9，雌花3–8。

1981年的[克朗奎斯特分类法将本](../Page/克朗奎斯特分类法.md "wikilink")[属和](../Page/属.md "wikilink")[四数木科合并在一起](../Page/四数木科.md "wikilink")，属于[堇菜目](../Page/堇菜目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其单独列为一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，和[四数木科一起放在](../Page/四数木科.md "wikilink")[葫芦目中](../Page/葫芦目.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[野麻科](http://delta-intkey.com/angio/www/datiscac.htm)
  - {[APG
    网站中的野麻科](http://www.mobot.org/MOBOT/Research/APWeb/orders/cucurbitalesweb.htm#Datiscaceae)
  - [NCBI中的野麻科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=25931)

[\*](../Category/野麻科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")