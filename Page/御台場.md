[RainbowBridge_over_TokyoBay.jpg](https://zh.wikipedia.org/wiki/File:RainbowBridge_over_TokyoBay.jpg "fig:RainbowBridge_over_TokyoBay.jpg")，執台場進出樞紐的[彩虹大橋](../Page/彩虹大橋.md "wikilink")。\]\]
[Ferris_Wheeel_of_Odaiba.jpg](https://zh.wikipedia.org/wiki/File:Ferris_Wheeel_of_Odaiba.jpg "fig:Ferris_Wheeel_of_Odaiba.jpg")是台場地區最受歡迎的地標之一。\]\]
[AQUACiTY_and_FujiTV_HQ.jpg](https://zh.wikipedia.org/wiki/File:AQUACiTY_and_FujiTV_HQ.jpg "fig:AQUACiTY_and_FujiTV_HQ.jpg")與其後方的[富士電視台本社大樓](../Page/富士電視台本社大樓.md "wikilink")。\]\]
[OdaibaView.jpg](https://zh.wikipedia.org/wiki/File:OdaibaView.jpg "fig:OdaibaView.jpg")
[Interoir_of_Toyota_MegaWeb.jpg](https://zh.wikipedia.org/wiki/File:Interoir_of_Toyota_MegaWeb.jpg "fig:Interoir_of_Toyota_MegaWeb.jpg")設立在台場的大型展示館，也是世界上最巨大的汽車展示間之一，內有多種可以讓來賓親自試乘各式豐田汽車的設施。\]\]
[Badehaus_in_Odaiba_in_Tokio_006.jpg](https://zh.wikipedia.org/wiki/File:Badehaus_in_Odaiba_in_Tokio_006.jpg "fig:Badehaus_in_Odaiba_in_Tokio_006.jpg")
**御台場**（），或直接簡稱為**台場**，是[日本](../Page/日本.md "wikilink")[東京灣內一座以](../Page/東京灣.md "wikilink")[填海造陸方式製造出來的巨大](../Page/填海.md "wikilink")[人工島](../Page/人工島.md "wikilink")。1980年代起，此地成為[東京臨海副都心的核心地帶而快速開發](../Page/東京臨海副都心.md "wikilink")，作為東京未來的都市規劃的要角，由於區域內陸續遷入許多大型[企業的總部](../Page/企業.md "wikilink")，並且修築了許多採用現代規劃造景的[商場與娛樂設施](../Page/商場.md "wikilink")，近年來已成為[東京都內最受國內外遊客歡迎的重要景點](../Page/東京都內.md "wikilink")。

## 歷史

御台場的名稱源自於「」，原本是指[江戶時代末期](../Page/江戶時代.md "wikilink")，[幕府方面為了抵禦外人入侵](../Page/幕府.md "wikilink")，而在日本全國各地海濱所設置的[砲台](../Page/砲台.md "wikilink")。在這許多的砲台之中，位於[品川的](../Page/品川.md "wikilink")[東京灣水域所修築的一系列砲台是特別重要有名的](../Page/東京灣.md "wikilink")，被稱為「品川台場」。當時江戶幕府原本打算在幕府中樞重地[江戶外圍的觀音崎](../Page/江戶.md "wikilink")（位於[神奈川縣](../Page/神奈川縣.md "wikilink")[橫須賀市](../Page/橫須賀市.md "wikilink")）與富津（位於[千葉縣](../Page/千葉縣.md "wikilink")[富津市](../Page/富津市.md "wikilink")）之間建立防衛線，卻不料在1853年時被[美國海軍准將](../Page/美國海軍.md "wikilink")[馬修·佩里](../Page/馬休·佩里.md "wikilink")（Matthew
C.
Perry）所率領的四艘軍艦輕易突破而在[浦賀](../Page/浦賀.md "wikilink")（位於神奈川縣橫須賀市）登陸，打破日本長年來的[鎖國政策](../Page/鎖國.md "wikilink")，也就是著名的「[黑船來航](../Page/黑船來航.md "wikilink")」事件。因為此事件的發生，激發了幕府方面的危機意識，因此特別委託了[伊豆國](../Page/伊豆國.md "wikilink")[田方郡](../Page/田方郡.md "wikilink")[韮山](../Page/韮山.md "wikilink")（今[靜岡縣](../Page/靜岡縣.md "wikilink")[伊豆之國市](../Page/伊豆之國市.md "wikilink")[韮山町](../Page/韮山町.md "wikilink")）[代官](../Page/代官.md "wikilink")[江川英龍](../Page/江川英龍.md "wikilink")（江川[太郎左衛門](../Page/太郎左衛門.md "wikilink")），於同年起在東京灣內開始修築砲台。

江川當時投入了約75萬兩的金額，利用伊豆等地運來的石頭，與將品川鑿挖拆崩後所獲得的土砂，陸續完成多座以方形或五角形石牆圍繞住的西式砲台，稱為「台場」。其中，由於第四與第七台場並未真的完成，因此實際上建好的台場共有第一、二、三、五、六等5座（原本規劃是要建造11座），做為江戶的海上防線。

1940年（[昭和](../Page/昭和.md "wikilink")15年）[東京港開港](../Page/東京港.md "wikilink")。[戰後為了應付大規模工業發展所產生的大量](../Page/第二次世界大戰.md "wikilink")[垃圾](../Page/垃圾.md "wikilink")，當局開始在灣內覓地建立[掩埋场](../Page/垃圾堆填區.md "wikilink")（埋立地），其中，第13號掩埋場的北緣就是江戶時代所修築的台場，通稱「御台場」（，「」這字在[日文裡面是用作指特定事物的](../Page/日文.md "wikilink")[冠詞](../Page/冠詞.md "wikilink")，因此相當於「那個台場」的意味），第14號掩埋場就是著名的[夢之島](../Page/夢之島.md "wikilink")（），而第15號掩埋場則稱為[新夢之島](../Page/新夢之島.md "wikilink")。

昭和30年代（1955年－1965年），東京灣進行了大規模的整頓，在這段期間第一、第五台場因為[品川碼頭](../Page/品川碼頭.md "wikilink")（）的修築而被拆除，第二台場也因會阻礙到主航道而遭拆除。第四台場的遺跡，位於今日的[天王洲島](../Page/天王洲島.md "wikilink")（）。剩餘的第三與第六台場則被日本政府指定為國家史蹟而殘留下來，其中第三台場變成與[台場海濱公園](../Page/台場海濱公園.md "wikilink")（）連在一起的[台場公園](../Page/台場公園.md "wikilink")，第六台場則因為保存的理由禁止閒人進入，孤立在[彩虹大橋底下變成](../Page/彩虹大橋.md "wikilink")[鳥類棲息的樂園](../Page/鳥類.md "wikilink")。

1990年代初期，在前[東京都知事](../Page/東京都知事.md "wikilink")[鈴木俊一的積極推動下](../Page/鈴木俊一.md "wikilink")，發起「[東京臨海副都心](../Page/東京臨海副都心.md "wikilink")」的開發計畫，並在1995年（[平成](../Page/平成.md "wikilink")7年）定案成為東京都第7個副都心計畫，隸屬港區的台場（台場1、2丁目）也包含在計畫用地範圍內。由於鈴木的奔走鼓吹，再加上對計畫於1996年3月開幕的有所期待，許多大企業紛紛投下重金，在此區域大興土木。不料鈴木在博覽會開幕不久前（1995年4月）的東京都知事選舉中落敗，反對繼續此計劃的[青島幸男上任後立刻中止了博覽會的籌備](../Page/青島幸男.md "wikilink")，再加上[泡沫經濟的崩潰](../Page/泡沫經濟.md "wikilink")，使得廠商紛紛抽手，開發到一半的台場地區頓時變成荒煙漫草的無人城。

1997年3月10日，[富士電視台將總部從](../Page/富士電視台.md "wikilink")[新宿區](../Page/新宿區.md "wikilink")[河田町搬遷到目前位於台場的](../Page/河田町.md "wikilink")[富士電視台本社大樓](../Page/富士電視台本社大樓.md "wikilink")，這棟由日本知名建築師[丹下健三所設計](../Page/丹下健三.md "wikilink")、像是一架[電視機般造型的大樓除了成為遊客絡繹不絕的觀光景點外](../Page/電視機.md "wikilink")，也帶動了更多廠商進駐台場的趨勢。由於台場地區經常被拿來作為[日本連續劇的拍攝場地](../Page/日本電視劇.md "wikilink")（尤其是彩虹大橋、[調色板城大摩天輪等場景](../Page/調色板城大摩天輪.md "wikilink")），透過日劇的播放，台場的美麗景緻不只廣為日本各地的電視觀眾所熟悉，甚至在像是[台灣等經常播放日劇的地區](../Page/台灣.md "wikilink")，也同樣讓觀眾留下深刻印象。

最初於1996年局部通車，2002年延伸至[JR](../Page/JR.md "wikilink")[山手線](../Page/JR山手線.md "wikilink")[大崎車站並全線通車的](../Page/大崎車站.md "wikilink")[東京臨海高速鐵道臨海線](../Page/臨海線_\(東京臨海高速鐵道\).md "wikilink")（），與1995年通車，以新橋站為端點站的[東京臨海新交通臨海線](../Page/東京臨海新交通臨海線.md "wikilink")（暱稱「百合鷗號」；），將台場與整個東京的主要[鐵路與](../Page/鐵路.md "wikilink")[地下鐵系統串連在一起](../Page/地下鐵.md "wikilink")；加上早已興建、成為[首都圈高速公路網一環的兩條高速公路](../Page/首都高速道路.md "wikilink")，便利的交通刺激了更多的廠商與居民進駐。時任東京都知事的石原慎太郎曾提出將台場建設為[賭城的構想](../Page/賭城.md "wikilink")，但由於日本目前的法律禁止賭博行為，因此在修法通過之前，這個提案還是只能停留在構思與討論的階段。

2017年，品川台場被選入[續日本100名城](../Page/續日本100名城.md "wikilink")。

## 範圍

[Tokio20040612_2.jpg](https://zh.wikipedia.org/wiki/File:Tokio20040612_2.jpg "fig:Tokio20040612_2.jpg")
雖然嚴格來說，所謂的御台場原本僅專指隸屬於東京都[港區之下的台場](../Page/港區_\(東京\).md "wikilink")1、2丁目範圍。但一般而言，對於不了解東京行政區劃分方式的外來遊客來說，所謂的台場地區，大致上涵蓋了東京都轄下，包括港區台場、[品川區](../Page/品川區.md "wikilink")[東八潮](../Page/東八潮.md "wikilink")、[江東區](../Page/江東區_\(東京\).md "wikilink")[青海與](../Page/青海_\(東京都\).md "wikilink")[有明在內](../Page/有明.md "wikilink")（過去稱為第13號掩埋場；）的人工造陸區域（也有許多人沿用「[臨海副都心](../Page/東京臨海副都心.md "wikilink")」這個通稱）。

## 著名地標與設施

  - 港區（台場）
      - [彩虹大橋](../Page/彩虹大橋.md "wikilink")（橫跨於港區[芝浦與台場間](../Page/芝浦.md "wikilink")）

      - [台場海濱公園](../Page/台場海濱公園.md "wikilink")

      - [台場公園與第三台場](../Page/台場公園.md "wikilink")

      - [富士電視台本社大樓](../Page/富士電視台本社大樓.md "wikilink")（）

      - （Hotel Grand Pacific Le Daiba）：旗下的觀光飯店，在1998年開幕時原名「美麗殿太平洋酒店」（Le
        Meridien Grand Pacific Hotel）

      - （；Hotel Nikko Tokyo）

      - [自由女神像](../Page/世界各地的自由女神像.md "wikilink")

      - （；DECKS Tokyo Beach）：內有台場小香港，台場一丁目商店街與世嘉歡樂城（SEGA Joypolis）等遊憩設施。

      - [台場海洋城](../Page/台場海洋城.md "wikilink")（；AQUACiTY Odaiba）
  - 江東區（有明，青海）
      - （；Palette Town）

          - [調色板城大摩天輪](../Page/調色板城大摩天輪.md "wikilink")

          - （，Venus
            Fort）：主要鎖定女性消費者客層的大型[購物中心](../Page/購物中心.md "wikilink")。

          - Zepp Tokyo：連鎖[展演館經營業者](../Page/展演館.md "wikilink")旗下的展演館場地之一。

          - ：[豐田汽車的大型形象展覽館](../Page/豐田汽車.md "wikilink")。館內包括了豐田都市陳列館（Toyota
            City Showcase）、汽車史冊的寶庫（History Garage）與豐田通用設計陳列館（Toyota
            Universal Design Showcase）等三大主要展館與週邊設施。

      - （ダイバーシティ東京；DiverCity Tokyo）：由所經營的大型複合商業設施。

          - 台場高達

      -
      - （）

      - [日本科學未來館](../Page/日本科學未來館.md "wikilink")

      - [東京灣岸警察署](../Page/東京灣岸警察署.md "wikilink")

      - 資訊港橋（；Teleport Bridge）

      - 夢之大橋（）

      - [東京國際展示場](../Page/東京國際展示場.md "wikilink")

      - [電訊中心](../Page/電訊中心.md "wikilink")（；Telecom Center）

      - [林原自然科學博物館](../Page/林原自然科學博物館.md "wikilink")

      - [有明網球之森公園](../Page/有明網球之森公園.md "wikilink")（）與有明競技場（）

      - [东京国际交流馆](../Page/东京国际交流馆.md "wikilink")：由[獨立行政法人](../Page/行政法人.md "wikilink")[日本學生支援機構經營](../Page/日本學生支援機構.md "wikilink")，整合了會議中心與留學生、學者宿舍的複合建築。又常被稱為「平成廣場」（Plaza平成）

      - （）
  - 品川區
      - （）

      - [潮風公園](../Page/潮風公園.md "wikilink")

## 交通

要進出位於海灣中人工島上的台場，共有道路交通、軌道交通、水上交通等幾種基本型式，分列如下：

  - 軌道交通
      - [東京臨海高速鐵道臨海線](../Page/東京臨海高速鐵道.md "wikilink")：服務於[新木場至](../Page/新木場.md "wikilink")[大崎間](../Page/大崎_\(品川區\).md "wikilink")，路線中段橫貫台場地區；自大崎站起與[JR](../Page/JR.md "wikilink")[埼京線相互直通運轉](../Page/埼京線.md "wikilink")（可以直接連結[澀谷](../Page/澀谷.md "wikilink")、[新宿](../Page/新宿.md "wikilink")、[池袋三個大型轉乘站](../Page/池袋.md "wikilink")）。
      - [東京臨海新交通臨海線](../Page/新交通百合鷗號.md "wikilink")：「新交通」在日文中指的是類似[輕軌系統的高架膠輪捷運系統](../Page/輕軌.md "wikilink")。相較於路線的正式名稱，暱稱「百合鷗號」（）比較為大多數人所熟悉。營運路線是從[新橋經由彩虹大橋到島上的](../Page/新橋_\(東京\).md "wikilink")[有明](../Page/有明.md "wikilink")，之後持續延伸至[豐洲](../Page/豐洲.md "wikilink")。
      - 臨海高速鐵道國際展示場站與百合鷗號有明站間，可以進行近距離站外轉乘。

<!-- end list -->

  - 巴士
      - 東京都交通局[都營巴士](../Page/都營巴士.md "wikilink")
          - [海01](../Page/都營巴士深川營業所.md "wikilink")：[門前仲町](../Page/門前仲町.md "wikilink")
            - [東京電訊港站](../Page/東京電訊港站.md "wikilink")
          - [都05](../Page/都營巴士深川營業所.md "wikilink")：[東京站丸之内南口](../Page/東京站.md "wikilink")
            - [東京Big Sight](../Page/東京Big_Sight.md "wikilink")
          - [門19](../Page/都營巴士深川營業所.md "wikilink")：門前仲町 - 東京Big Sight
          - [東16](../Page/都營巴士深川營業所.md "wikilink")：東京站八重洲北口 - 東京Big
            Sight
          - [海01](../Page/都營巴士深川營業所.md "wikilink")：門前仲町 - 東京電訊港站
          - [急行05](../Page/都營巴士江東營業所.md "wikilink")：[錦糸町站](../Page/錦糸町站.md "wikilink")
            - [日本未来科學館](../Page/日本未来科學館.md "wikilink")
          - [急行06](../Page/都營巴士臨海支所.md "wikilink")：[森下站](../Page/森下站_\(東京都\).md "wikilink")
            - 日本未来科學館
          - [波01](../Page/都營巴士品川營業所.md "wikilink")：中央防波堤 - 東京電訊港站
          - 無編號：[品川站港南口](../Page/品川站.md "wikilink") - 東京電訊港站
      - [京濱急行巴士](../Page/京濱急行巴士.md "wikilink")
          - [井30](../Page/京濱急行巴士大森營業所.md "wikilink")：[大井町站](../Page/大井町站.md "wikilink")
            - 東京國際郵輪碼頭站
          - [井32](../Page/京濱急行巴士大森營業所.md "wikilink")：大井町站 - 台場站
          - [森30](../Page/京濱急行巴士大森營業所.md "wikilink")：[大森站](../Page/大森站_\(東京都\).md "wikilink")
            - 東京國際郵輪碼頭站
          - [森40](../Page/京濱急行巴士大森營業所.md "wikilink")：大森站 - 東京國際郵輪碼頭站
      - [KM觀光](../Page/KM觀光.md "wikilink")
          - [台場彩虹巴士](../Page/KM觀光.md "wikilink")：[品川站東口](../Page/品川站.md "wikilink")
            - 富士電視台前 - 日航東京酒店 - 台場學園 - 品川站東口
          - [台場彩虹巴士](../Page/KM觀光.md "wikilink")：[田町站東口](../Page/田町站_\(東京都\).md "wikilink")
            - 富士電視台前 - 日航東京酒店 - 台場學園 - 田町站東口
          - [km花巴士](../Page/KM觀光.md "wikilink")：[濱松町站巴士總站](../Page/濱松町站.md "wikilink")
            - 東京Big
            Sight/[國際展示場站前](../Page/國際展示場站.md "wikilink")（2013年3月31日前由都營巴士[虹01運行](../Page/都營巴士港南支所.md "wikilink")）
      - [日之丸自動車興業](../Page/日之丸自動車集團.md "wikilink")
          - [東京灣穿梭巴士](../Page/東京灣穿梭巴士.md "wikilink")：日本未來科學館 - 東京電訊港站 -
            日航東京酒店 - 日本未來科學館（循環）

<!-- end list -->

  - 水上交通

[Himiko_Cruise_Ship.jpg](https://zh.wikipedia.org/wiki/File:Himiko_Cruise_Ship.jpg "fig:Himiko_Cruise_Ship.jpg")\]\]

  -   - [東京都觀光汽船](../Page/東京都觀光汽船.md "wikilink")（[淺草](../Page/淺草.md "wikilink")
        -
        [台場海濱公園](../Page/台場海濱公園.md "wikilink")、[日之出棧橋](../Page/日之出棧橋.md "wikilink")
        - 台場海濱公園、其他）
      - 觀光汽船興業
          - [URBAN
            LAUNCH](../Page/URBAN_LAUNCH.md "wikilink")（[都市船塢Lalaport豐洲](../Page/都市船塢Lalaport豐洲.md "wikilink")
            - [台場海濱公園](../Page/台場海濱公園.md "wikilink") -
            [芝浦島](../Page/芝浦島.md "wikilink")）
      - [東京都公園協會](../Page/東京都公園協會.md "wikilink")
          - [東京水邊線](../Page/東京水邊線.md "wikilink")（[兩國](../Page/兩國_\(墨田區\).md "wikilink")
            - 台場海濱公園、其他）

<!-- end list -->

  - 高速道路
      - 首都圈高速道路灣岸線
      - 首都圈高速道路（11號）台場線

此外，台場雖然不是必經道路，但[羽田機場的航線在近鄰的](../Page/羽田機場.md "wikilink")[江東區](../Page/江東區.md "wikilink")[青海上空](../Page/青海_\(江東區\).md "wikilink")，加上區内東西向[首都高速灣岸線以及](../Page/首都高速灣岸線.md "wikilink")[東京港旅客](../Page/東京港.md "wikilink")、貨物船舶等形成龐大的交通量，在戶外時能感受到交通噪音。

## 以台場為舞台的作品

  - 電影
      - 《[棄寶之島：遙與魔法鏡](../Page/棄寶之島：遙與魔法鏡.md "wikilink")》（）
      - 《[哥吉拉vs戴斯特洛伊亞](../Page/哥吉拉vs戴斯特洛伊亞.md "wikilink")》（）
      - 《[大搜查線THE MOVIE：灣岸署史上最惡之三日](../Page/大搜查線.md "wikilink")》（）
      - 《[蠟筆小新：黑暗珠珠大追擊](../Page/蠟筆小新.md "wikilink")》（）
      - 《[哥吉拉×美加基拉斯 G消滅作戰](../Page/哥吉拉×美加基拉斯_G消滅作戰.md "wikilink")》（）
      - 《[大搜查線THE MOVIE 2：封鎖彩虹橋](../Page/大搜查線.md "wikilink")》（）
      - 《[超人力霸王高斯VS超人力霸王正義 THE FINAL
        BATTLE](../Page/超人力霸王高斯.md "wikilink")》（）
      - 《[烏龍派出所 UFO大逆襲！龍捲風大作戰！！](../Page/烏龍派出所.md "wikilink")》（）
  - 電視劇
      - 《》（）
      - 《[大搜查線](../Page/大搜查線.md "wikilink")》
      - 《[超人力霸王蓋亞](../Page/超人力霸王蓋亞.md "wikilink")》（）
      - 《[超人力霸王馬克斯](../Page/超人力霸王馬克斯.md "wikilink")》（）
  - 動畫
      - 《[機動警察](../Page/機動警察.md "wikilink")》（）
      - 《[数码兽系列](../Page/数码兽系列.md "wikilink")》
      - 《[東京地震8.0](../Page/東京地震8.0.md "wikilink")》（）
      - 《[核爆末世錄](../Page/核爆末世錄.md "wikilink")》
  - 其他
      - 《》（漫畫，2000年）
      - 《[東京灣景](../Page/東京灣景.md "wikilink")》（小說，2002年）
      - 《》（，小說，2003年）
      - 《》（小說，2005年）
      - 《[守護她的51種方法](../Page/守護她的51種方法.md "wikilink")》（，漫畫，2005年）
      - 《[緋彈的亞莉亞](../Page/緋彈的亞莉亞.md "wikilink")》（，輕小說，2008年）
  - 遊戲
      - 《捉猴啦 百萬猴軍勁爆大出擊》（PlayStation 2，2006年）

此外，在掩埋場還沒開發為重劃區之前，包括《》、《[假面騎士](../Page/假面騎士.md "wikilink")》（）等電視劇都曾使用此地作為特技場景的外景拍攝地。

## 參見

  - [東京灣](../Page/東京灣.md "wikilink")
  - [東京臨海副都心](../Page/東京臨海副都心.md "wikilink")
  - [人工島](../Page/人工島.md "wikilink")
  - [富士電視台](../Page/富士電視台.md "wikilink")

## 外部連結

  - 台场指南（东京临海股份公司提供）：[日文](http://www.tokyo-odaiba.net/)、[简体中文](http://www.tokyo-odaiba.net/zh/)、[繁体中文](http://www.tokyo-odaiba.net/zh-tw/)
  - [臨海副都心造街協會](https://web.archive.org/web/20051125142528/http://www.seaside-tokyo.gr.jp/index.html)（）

[御台場](../Category/御台場.md "wikilink")
[Category:東京都區部地區](../Category/東京都區部地區.md "wikilink")
[Category:日本人工島](../Category/日本人工島.md "wikilink")