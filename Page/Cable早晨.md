**Cable早晨**（）是[香港有線新聞有限公司製作的兩小時](../Page/香港有線新聞有限公司.md "wikilink")[直播](../Page/直播.md "wikilink")[早晨](../Page/早晨.md "wikilink")[新聞節目](../Page/新聞節目.md "wikilink")，以輕鬆並帶著朝氣活力的方式，為[早上剛醒來的](../Page/早上.md "wikilink")[觀眾提供各類型](../Page/觀眾.md "wikilink")[新聞](../Page/新聞.md "wikilink")[資訊](../Page/資訊.md "wikilink")。

節目賣點為其獨有的環節，即互動形式的[天氣報告和今日新聞焦點](../Page/天氣報告.md "wikilink")。在頭一節先由主播在直播室的電視牆前闡釋[香港天文台的即日及未來數天](../Page/香港天文台.md "wikilink")[天氣預測](../Page/天氣.md "wikilink")，透過直播鏡頭描述戶外即時天色狀況，觀測雲層、陽光與能見度變化。在每半小時的節目尾聲，會預告當日的數個新聞焦點，預視觀眾時事動向。此外，亦有今日社評和報章摘要環節，提供當日報章的觀點。

而為了令節目生動、讓觀眾有親切感，主播、編輯和導播早於節目開始前三小時——即凌晨四時——便要開始準備，訂出主打新聞、構思開場白，以及互動形式的天氣環節。節目歷年來經過最少數次改革。

節目每半小時開始一遍，每次分三部份播出，以[廣告劃分](../Page/廣告.md "wikilink")。節目最初只於[有線新聞台](../Page/有線新聞台.md "wikilink")（第9頻道）播映。其後，[有線第1台](../Page/有線第1台.md "wikilink")（第4頻道）亦開始同步播映；而[有線財經資訊台](../Page/有線財經資訊台.md "wikilink")（第8頻道）曾於星期一至五早上非[港股交易日亦會聯播](../Page/港股.md "wikilink")，直至2018年7月30日因《[創富早餐](../Page/創富早餐.md "wikilink")》停播，港股交易日起亦會同步播映。有線新聞踏入高清時代後，節目同時於[有線高清新聞台](../Page/有線新聞台.md "wikilink")（第209頻道）播出。免費電視頻道[香港開電視](../Page/香港開電視.md "wikilink")（第1/3台）由2018年元旦起同步播出。

當有突發事件發生時，會暫停各環節，由[主播和記者透過直播畫面或最新片段](../Page/香港有線電視新聞#主播、主持及記者.md "wikilink")，作即時的旁述和報道。香港其他電視台的早晨新聞節目有[無綫電視的](../Page/無綫電視.md "wikilink")《[香港早晨](../Page/香港早晨.md "wikilink")》、[now寬頻電視的](../Page/now寬頻電視.md "wikilink")《[Now早晨](../Page/Now早晨.md "wikilink")》和[港台電視的](../Page/港台電視.md "wikilink")《[早辰。早晨](../Page/早辰。早晨.md "wikilink")》。本節目為於2018年1月1日之前，唯一沒有在香港免費電視台播出的早晨新聞節目\[1\]。

節目曾以每半小時分三部份播出，首節主播先以數分鐘時間把重點新聞包括本港、國際、財經和體育消息，按重要和觀眾關心程度概述一遍，但近年已取消此安排，並恢復每半小時兩節播出。

## 歷史

  - 2001年1月29日，節目正式播映，當時節目時間為逢星期一至五早上6時至9時。節目播放初期曾派遣記者擔當交通報導員，但由於成本高昂，有關安排在不足一年的時間內取消。另外，本節目會邀請嘉賓在節目中進行訪問，不過有關安排在2002年取消。

<!-- end list -->

  - 2006年10月31日，[荃灣](../Page/荃灣.md "wikilink")[有線電視大廈發生一級火警](../Page/有線電視.md "wikilink")；正在直播的《Cable早晨》暫停，新聞報道須改於中環直播室進行\[2\]。

<!-- end list -->

  - 2009年3月上旬起，取消閱讀報章頭版消息的安排，改為閱讀報章摘要。

<!-- end list -->

  - 2009年8月31日起，節目以全新面貌登場。節目時間改為早上7時至9時播映，並取消報導交通消息的環節。節目開始時主播站立在虛擬電視幕牆旁描述當天的天氣狀況，接着用數分鐘簡述重點新聞，下一節才會詳細報導新聞內容。節目中的背景及版面亦更換為新版本。

<!-- end list -->

  - 2013年12月9日，節目以全新面貌登場。

<!-- end list -->

  - 2014年9月中旬起，由於佔領中環行動，節目改為實境直播室進行直播。主播台後面的平面電視熒幕則顯示「上午要聞」。

<!-- end list -->

  - 2015年4月，節目重新使用虛擬場景進行直播。

<!-- end list -->

  - 2016年1月1日，節目採用高清拍攝，改於全新直播室進行直播。描述當天的天氣狀況時亦會直播香港各地的畫面，例如[維港](../Page/維港.md "wikilink")、荃灣、[黃大仙](../Page/黃大仙.md "wikilink")、[沙田](../Page/沙田.md "wikilink")[城門河](../Page/城門河.md "wikilink")、[紅磡海旁](../Page/紅磡.md "wikilink")、[觀塘海旁等](../Page/觀塘.md "wikilink")。但由於成本高昂，有關安排在不足半年已取消。

<!-- end list -->

  - 2016年11月9日，由於播放美國大選特備節目，本節目暫停播映。

<!-- end list -->

  - 2018年1月1日，奇妙電視中文台（現稱「[香港開電視](../Page/香港開電視.md "wikilink")」）開始播放本節目；節目同時在[有線新聞台](../Page/有線新聞台.md "wikilink")、[有線第1台和奇妙電視中文台聯播](../Page/有線第1台.md "wikilink")。

<!-- end list -->

  - 2018年7月30日，因《[創富早餐](../Page/創富早餐.md "wikilink")》停播，[有線財經資訊台逢港股交易日開始播放本節目](../Page/有線財經資訊台.md "wikilink")；節目同時在有線新聞台、有線第1台、有線財經資訊台和奇妙電視中文台聯播。

<!-- end list -->

  - 2018年11月5日，節目更換片頭及螢幕佈景。

## 場景

從2001年至2014年，節目於[荃灣](../Page/荃灣.md "wikilink")[有線新聞中心的虛擬場景中進行](../Page/香港有線新聞有限公司#有線電視新聞中心.md "wikilink")。[虛擬場景由電腦設計](../Page/虛擬演播室.md "wikilink")，從2001年啟播至2014年，虛擬場景轉變過九次，如2002年的兩次、2003年的一次、2008年的一次、2009年8月的一次。此外，場景中亦有一些真實的擺設，如桌子、供[主播使用的手提電腦等](../Page/主播.md "wikilink")。片頭則於2002年、2003年、2004年1月5日、2009年8月31日及2013年12月9日轉變設計。

由2014年9月中至2015年4月，節目一度改用實景直播室進行直播。到2015年6月中，節目再次用虛擬場景進行直播。2016年1月1日起，節目採用高清拍攝，於全新實景直播室進行直播。

## 節目內容和環節

  - 天氣消息（節目的第一部份）
  - 重點新聞（節目的第一部份）
  - 本港及國際新聞（節目的第一及第二部份）
  - 早晨財經（節目的第三部份）
  - 早晨體育（節目的第三部份）
  - 今日新聞焦點（節目的第三部份結尾）
  - 今日社評（7:00、8:00第三部份）
  - 報章摘要（7:30、8:30第三部份）
  - 文娛快訊（星期五：8:00第三部份）
  - 新聞通識（7:00、7:30最後一部份）
  - 足球（歐聯、歐霸）戰報（比賽日翌日：8:00第三部份）
  - 小事大意義（星期四：隨意編排）
  - 新聞刺針（隨意編排）

## 主播安排

Cable早晨由兩位[有線電視主播主持](../Page/香港有線新聞有限公司#主播、主持及記者.md "wikilink")，通常為一男一女年輕主播，或者兩位女年輕主播，以配合節目的活力、輕鬆形象。

節目當年剛開始播映時由[張宏艷和](../Page/張宏艷.md "wikilink")[莊安宜負責](../Page/莊安宜.md "wikilink")，初時還有一位財經主播一同在虛擬場景中主持。亦曾由較資深的[王靜茵和](../Page/王靜茵.md "wikilink")[關善茹配上新加盟的主播以作培訓](../Page/關善茹.md "wikilink")。

2009年8月31日起，全新面貌的Cable早晨，初期由[盧頌萱和](../Page/盧頌萱.md "wikilink")[王春媚負責](../Page/王春媚.md "wikilink")。至11月中，由[王沛然取代王春媚主持](../Page/王沛然.md "wikilink")；2010年3月下旬起，[張曉雯取代](../Page/張曉雯.md "wikilink")[王沛然主持](../Page/王沛然.md "wikilink")。而[關善茹亦會間中主持](../Page/關善茹.md "wikilink")。

2010年5月10日起，由[王春媚及](../Page/王春媚.md "wikilink")[關善茹主持](../Page/關善茹.md "wikilink")；5月中旬起，由[游安怡取代關善茹主持](../Page/游安怡.md "wikilink")。8-9月期間，[黃凱欣一度主持Cable早晨](../Page/黃凱欣.md "wikilink")。同年9月13日起，由[關善茹及](../Page/關善茹.md "wikilink")[羅慧沁主持](../Page/羅慧沁.md "wikilink")，但在翌日起，便由[游安怡取代關善茹主持](../Page/游安怡.md "wikilink")。在9月27日起，恢復由[關善茹及](../Page/關善茹.md "wikilink")[羅慧沁主持](../Page/羅慧沁.md "wikilink")。

2013年12月9日起，經改革後的Cable早晨由[張靜文及](../Page/張靜文.md "wikilink")[張惠萍主持](../Page/張惠萍.md "wikilink")，其後另外數名主播麥恬昕、胡凱文相繼加入。

## 參見

  - [香港有線電視](../Page/香港有線電視.md "wikilink")
  - [有線電視主播和記者](../Page/香港有線新聞有限公司#主播、主持及記者.md "wikilink")
  - 2003年宣傳片段：[2003年Cable早晨](http://www.youtube.com/watch?v=FrqVmOfq8Ks)
  - 2009年宣傳片段：[Cable早晨
    朝朝醒晨](http://www.youtube.com/watch?v=qCabZqoPOVs&e)

## 節目調動（[香港開電視](../Page/香港開電視.md "wikilink")）

### 常規調動

  - 2018年8月20日至9月2日，由於07:00-08:00播出《[亞運最精彩](../Page/2018年亞洲運動會.md "wikilink")》，延至08:00才聯播**Cable早晨**。

### 特別調動

## 參考文獻附註

## 相關節目

  - [有線新聞](../Page/有線新聞_\(電視節目\).md "wikilink")
  - [新聞最前線](../Page/新聞最前線_\(香港\).md "wikilink")
  - [國際最前線](../Page/國際最前線.md "wikilink")
  - [十一點最前線](../Page/十一點最前線.md "wikilink")
  - [十一點半最前線](../Page/十一點半最前線.md "wikilink")
  - [午夜最前線](../Page/午夜最前線.md "wikilink")

[Category:香港有線電視節目](../Category/香港有線電視節目.md "wikilink")
[Category:香港電視新聞節目](../Category/香港電視新聞節目.md "wikilink")
[Category:早間電視節目](../Category/早間電視節目.md "wikilink")
[Category:香港電視小作品](../Category/香港電視小作品.md "wikilink")
[Category:奇妙電視節目](../Category/奇妙電視節目.md "wikilink")

1.  《[香港早晨](../Page/香港早晨.md "wikilink")》在[翡翠台和](../Page/翡翠台.md "wikilink")[無綫新聞台播出](../Page/無綫新聞台.md "wikilink")，《[Now早晨](../Page/Now早晨.md "wikilink")》在[ViuTV播出](../Page/ViuTV.md "wikilink")，《[早辰。早晨](../Page/早辰。早晨.md "wikilink")》在[港台電視31、31A播出](../Page/港台電視31、31A.md "wikilink")
2.  [有線電視大廈發生火警](http://www.mpinews.com/htm/INews/20061031/gb20836t.htm)
    （明報）