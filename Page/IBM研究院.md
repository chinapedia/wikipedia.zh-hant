**IBM研究院**是[IBM公司的一個](../Page/IBM.md "wikilink")（研究）部門，是一個研究與先進發展的組織，該部門目前分佈在全球八個地方，並正進行著數百個研究專案，這個部門最早可追溯到1945年成立於[哥倫比亞大學的華生科學運算實驗室](../Page/哥倫比亞大學.md "wikilink")（Watson
Scientific Computing Laboratory）。\[1\]

IBM研究院的主要研究活動包括創新材料與結構的發明、高效能微處理器及電腦、分析方法與工具、演算法、軟體架構、管理方法、從資料進行搜尋並探知意向。

過去許多知名的科技發展也都出自此部門，例如[DES](../Page/DES.md "wikilink")（Data Encryption
Standard）加密演算、經典的電腦程式語言：[FORTRAN](../Page/FORTRAN.md "wikilink")（Formula
Translation System）、[本華·曼德博](../Page/本華·曼德博.md "wikilink")（Benoît B.
Mandelbrot）的報告書中發表了[分形](../Page/碎形.md "wikilink")（Fractal）、磁性碟片儲存（[硬碟](../Page/硬碟.md "wikilink")）、用單一個[電晶體即可記憶一個比特的動態RAM](../Page/電晶體.md "wikilink")（Dynamic
Random Access
Memory，[DRAM](../Page/DRAM.md "wikilink")）、精簡指令集電腦（[RISC](../Page/RISC.md "wikilink")）架構、以及[關聯式資料庫等](../Page/關聯式資料庫.md "wikilink")。IBM研究院在物理科學上也有所貢獻，包括[掃描隧道顯微鏡](../Page/掃描隧道顯微鏡.md "wikilink")（簡稱：STM）以及高溫超導等，此兩項成就都獲得了[諾貝爾獎](../Page/諾貝爾獎.md "wikilink")。

IBM研究院在全球拥有十二个實驗室，其中三處在[美國本土](../Page/美國.md "wikilink")，分别是：

1.  （Thomas J. Watson Research
    Center）共分佈在三個位置，分別是[紐約州約克鎮](../Page/紐約州.md "wikilink")（Yorktown,
    New York）、[霍桑](../Page/霍桑.md "wikilink")（Hawthorne, New
    York）、以及[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[剑桥](../Page/剑桥_\(马萨诸塞州\).md "wikilink")（Cambridge,
    Massachusetts）。

2.  [愛曼登研究中心](../Page/愛曼登研究中心.md "wikilink")（），位于[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[聖荷西](../Page/聖荷西_\(加利福尼亞州\).md "wikilink")（[矽谷](../Page/矽谷.md "wikilink")）。

3.  [奧斯汀研究實驗室](../Page/奧斯汀研究實驗室.md "wikilink")（），位于[德克萨斯州](../Page/德克萨斯州.md "wikilink")[奧斯汀](../Page/奧斯汀.md "wikilink")。

而在美國本土之外的九處為：

1.  [瑞士的](../Page/瑞士.md "wikilink")[蘇黎世](../Page/蘇黎世.md "wikilink")
2.  [以色列的](../Page/以色列.md "wikilink")[海法](../Page/海法.md "wikilink")
3.  [日本的](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")
4.  [中國的](../Page/中國.md "wikilink")[北京](../Page/北京.md "wikilink")
5.  [印度的](../Page/印度.md "wikilink")[德里和](../Page/德里.md "wikilink")[班加罗尔](../Page/班加罗尔.md "wikilink")
6.  [肯尼亚的](../Page/肯尼亚.md "wikilink")[内罗毕和](../Page/内罗毕.md "wikilink")[南非的](../Page/南非.md "wikilink")[约翰内斯堡](../Page/约翰内斯堡.md "wikilink")
7.  [巴西的](../Page/巴西.md "wikilink")[里约热内卢和](../Page/里约热内卢.md "wikilink")[圣保罗](../Page/圣保罗.md "wikilink")
8.  [爱尔兰的](../Page/爱尔兰.md "wikilink")[都柏林](../Page/都柏林.md "wikilink")
9.  [澳大利亚的](../Page/澳大利亚.md "wikilink")[墨尔本](../Page/墨尔本.md "wikilink")

## IBM中国研究院

IBM中国研究院成立于1995年9月，是[IBM在全球设立的](../Page/IBM.md "wikilink") 12
个研究[实验室之一](../Page/实验室.md "wikilink")。[IBM](../Page/IBM.md "wikilink")
CRL位于[北京](../Page/北京.md "wikilink")[上地信息产业基地的西北角](../Page/上地信息产业基地.md "wikilink")，坐落在[中关村软件园内](../Page/中关村.md "wikilink")，是IBM在[发展中国家设立的第一个研究中心](../Page/发展中国家.md "wikilink")。目前CRL共有研究人员近200名，其中绝大多数拥有中国甚至世界一流大学的[博士和](../Page/博士.md "wikilink")[硕士学位](../Page/硕士.md "wikilink")。目前IBM中国研究院的主要研究方向有[系统和](../Page/系统.md "wikilink")[网络](../Page/网络.md "wikilink")、[信息管理和](../Page/信息管理.md "wikilink")[协作](../Page/协作.md "wikilink")、[分布式计算和](../Page/分布式计算.md "wikilink")[系统管理](../Page/系统管理.md "wikilink")、[下一代服务](../Page/下一代服务.md "wikilink")。

## 參考資料

## 外部連結

  - [IBM Research官方網站](http://www.research.ibm.com)
  - [研究的專案](https://web.archive.org/web/20051210065715/http://domino.research.ibm.com/Comm/wwwr_projects.nsf/projectlist.html?ReadForm&count=500&alpha=a)
  - [出色研究的歷史](http://www.research.ibm.com/about/top_innovations_history.shtml)（最佳創新）
  - [IBM Research的編年史](http://www.research.ibm.com/about/history.shtml)
  - [IBM Research位在蘇黎世的實驗室](http://www.zurich.ibm.com/)
  - [位在哥倫比亞大學的IBM華生研究實驗室之歷史](http://www.columbia.edu/acis/history/watsonlab.html)

[Category:IBM](../Category/IBM.md "wikilink")

1.  [IBM Watson Laboratory at Columbia
    University](http://www.columbia.edu/acis/history/watsonlab.html)