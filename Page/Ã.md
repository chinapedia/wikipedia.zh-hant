（带[腭化符的](../Page/波浪號.md "wikilink")[a](../Page/a.md "wikilink")）是[卡舒比语字母第](../Page/卡舒比语.md "wikilink")3个和[瓜拉尼语字母第](../Page/瓜拉尼语字母.md "wikilink")2个，表示[鼻化元音](../Page/鼻化元音.md "wikilink")。卡舒比语用作代表
 和
。此字母也在[葡萄牙语](../Page/葡萄牙语.md "wikilink")、[越南语中作](../Page/越南语.md "wikilink")[变音字母使用](../Page/变音符号.md "wikilink")。此字母亦是[越南语](../Page/越南语.md "wikilink")
a 的[跌声](../Page/越南語音系.md "wikilink")。

[巴西的](../Page/巴西.md "wikilink")[圣保罗州](../Page/圣保罗.md "wikilink")／市 (*São
Paulo*) 即使用了这个字母。

## 字符编码

<table>
<thead>
<tr class="header">
<th><p>字符编码</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></th>
<th><p><a href="../Page/ISO/IEC_8859.md" title="wikilink">ISO 8859</a>-<a href="../Page/ISO/IEC_8859-1.md" title="wikilink">1</a>，<a href="../Page/ISO/IEC_8859-4.md" title="wikilink">4</a>，<a href="../Page/ISO/IEC_8859-9.md" title="wikilink">9</a>，<br />
<a href="../Page/ISO/IEC_8859-10.md" title="wikilink">10</a>，<a href="../Page/ISO/IEC_8859-14.md" title="wikilink">14</a>，<a href="../Page/ISO/IEC_8859-15.md" title="wikilink">15</a></p></th>
<th><p><a href="../Page/VISCII.md" title="wikilink">VISCII</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大寫字母.md" title="wikilink">大写</a> </p></td>
<td><p>U+00C3</p></td>
<td><p>C3</p></td>
<td><p>C3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小寫字母.md" title="wikilink">小写</a> </p></td>
<td><p>U+00E3</p></td>
<td><p>E3</p></td>
<td><p>E3</p></td>
</tr>
</tbody>
</table>

## 參見

  - [拉丁字母列表](../Page/拉丁字母列表.md "wikilink")
  - [波浪號](../Page/波浪號.md "wikilink")

## 外部链接

  -
  -
  - [Letter Ã](http://graphemica.com/Ã) on the website graphemica.com

  - [Letter ã](http://graphemica.com/ã) on the website graphemica.com

{{-}}

[AÃ](../Category/衍生拉丁字母.md "wikilink")