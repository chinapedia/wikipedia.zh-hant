**琵琶湖**（）位於[日本](../Page/日本.md "wikilink")[滋賀縣](../Page/滋賀縣.md "wikilink")，為日本最大的[湖泊](../Page/湖泊.md "wikilink")；日本指定湖泊，也列入[濕地公約](../Page/濕地公約.md "wikilink")[國際重要濕地名錄中](../Page/國際重要濕地名錄.md "wikilink")。

## 地理

[Mt_ibuki02s3200.jpg](https://zh.wikipedia.org/wiki/File:Mt_ibuki02s3200.jpg "fig:Mt_ibuki02s3200.jpg")\]\]
琵琶湖佔有滋賀縣六分之一的面積，總面積670.33平方公里，湖岸長241公里，最深103.58米，平均水深41.2米 。

自琵琶湖流出的河流依上下游的不同依序稱為[瀨田川](../Page/瀨田川.md "wikilink")、[宇治川](../Page/宇治川.md "wikilink")、[淀川](../Page/淀川.md "wikilink")、最後流入[大阪灣](../Page/大阪灣.md "wikilink")。此外為了供應[京都市之](../Page/京都市.md "wikilink")[自來水以及灌溉](../Page/自來水.md "wikilink")、發電等種種目的，因而開闢有[琵琶湖疎水這條人工水道](../Page/琵琶湖疎水.md "wikilink")。

在湖頸段的最狹處建有[琵琶湖大橋](../Page/琵琶湖大橋.md "wikilink")，以北的部分稱為**北湖**，以南的部分稱為**南湖**，北湖面積約為南湖的11倍\[1\]，最深處位於北湖。

湖水的來源主要為周圍的山地，湖水供應[京阪神地方的居民用水](../Page/京阪神地方.md "wikilink")，也因為經濟的高度成長，導致水質污濁與[富養化](../Page/富養化.md "wikilink")，因此設有琵琶湖條例保護湖水的潔淨。

自古以來為重要的水上交通要道，在鐵路開通以前為日本東部與[北陸地方運輸要道](../Page/北陸地方.md "wikilink")。

## 歷史

琵琶湖大約成形於400萬年前（也有一說為600萬年前），因為[地殼的變動在現今的](../Page/地殼.md "wikilink")[三重縣上野地方形成](../Page/三重縣.md "wikilink")。之後逐漸向北移動至現今的位置。在世界的湖泊中僅次於[貝加爾湖](../Page/貝加爾湖.md "wikilink")、[坦干依喀湖是第三古老的湖泊](../Page/坦干依喀湖.md "wikilink")。

  - 1890年供應京都市用水的琵琶湖疎水開通。
  - 1950年7月24日成立[琵琶湖國定公園](../Page/琵琶湖國定公園.md "wikilink")。
  - 1964年9月琵琶湖大橋開通。
  - 1980年制訂琵琶湖條例。
  - 1993年列入[濕地公約國際重要濕地名錄中](../Page/濕地公約.md "wikilink")。
  - 2003年琵琶湖觀光利用條例施行。

## 生物

[Fish_from_Lake_Biwa_for_sale_at_a_fish_store_in_Otsu,_Shiga,_Japan.jpg](https://zh.wikipedia.org/wiki/File:Fish_from_Lake_Biwa_for_sale_at_a_fish_store_in_Otsu,_Shiga,_Japan.jpg "fig:Fish_from_Lake_Biwa_for_sale_at_a_fish_store_in_Otsu,_Shiga,_Japan.jpg")
琵琶湖的[生態體系相當的豐富化](../Page/生態體系.md "wikilink")，有超過1000種動植物生長其中。魚類約有46種，貝類約40種，水草約70種，因而被稱日本淡水魚的寶庫。琵琶湖的淡水珍珠養殖也相當有名。

## 流入的主要河川

  - [野州川](../Page/野州川.md "wikilink")
  - [日野川](../Page/日野川.md "wikilink")
  - [愛知川](../Page/愛知川.md "wikilink")
  - [安曇川](../Page/安曇川.md "wikilink")
  - [宇曾川](../Page/宇曾川.md "wikilink")
  - [犬上川](../Page/犬上川.md "wikilink")
  - [芹川](../Page/芹川.md "wikilink")
  - [姉川](../Page/姉川.md "wikilink")
  - [余吳川](../Page/余吳川.md "wikilink")

## 島嶼

[竹生島](../Page/竹生島.md "wikilink")、、 。

## 琵琶湖八景

1945年6月經由公開徵求選出：

  - 煙雨－[比叡的樹林](../Page/比叡山.md "wikilink")
  - 夕陽－[瀨田](../Page/淀川.md "wikilink")[石山的清流](../Page/石山寺.md "wikilink")
  - 涼風－[雄松崎的白汀](../Page/雄松崎.md "wikilink")
  - 曉霧－[海津大崎的岩礁](../Page/海津大崎.md "wikilink")
  - 新雪－[賤岳的大觀](../Page/賤岳.md "wikilink")
  - 明月－[彥根的古城](../Page/彥根城.md "wikilink")
  - 深綠－[竹生島的倒影](../Page/竹生島.md "wikilink")
  - 春色－[安土](../Page/安土町.md "wikilink")[八幡的水鄉](../Page/近江八幡市.md "wikilink")

## 活動

  - 自1977年7月2日起，每年的七、八月間舉辦[日本國際鳥人錦標賽](../Page/日本國際鳥人錦標賽.md "wikilink")。
  - 2006年的日本電影《[大奧](../Page/大奧_\(2006年電影\).md "wikilink")》的拍攝場景。
  - [日本競艇的](../Page/日本競艇.md "wikilink")24個競艇場之一的所在地。

## 参考文献

## 外部連結

  - [琵琶湖與環境](https://web.archive.org/web/20100818190822/http://www.pref.shiga.jp/d/biwako/)
  - [琵琶湖河川事務所](https://web.archive.org/web/20070928060044/http://www.biwakokasen.go.jp/)
  - [B-SKY／琵琶湖河川情報](https://web.archive.org/web/20060407024619/http://www.webgis.biwakokasen.go.jp/)
  - [滋賀県立琵琶湖博物館](http://www.lbm.go.jp/)

[Category:滋賀縣地理](../Category/滋賀縣地理.md "wikilink")
[Category:日本湖泊](../Category/日本湖泊.md "wikilink")
[Category:近江國](../Category/近江國.md "wikilink")
[Category:平成百景](../Category/平成百景.md "wikilink")
[Category:拉姆薩公約登錄地](../Category/拉姆薩公約登錄地.md "wikilink")
[Category:長濱市](../Category/長濱市.md "wikilink")
[Category:米原市](../Category/米原市.md "wikilink")
[Category:彥根市](../Category/彥根市.md "wikilink")
[Category:東近江市](../Category/東近江市.md "wikilink")
[Category:近江八幡市](../Category/近江八幡市.md "wikilink")
[Category:野洲市](../Category/野洲市.md "wikilink")
[Category:守山市](../Category/守山市.md "wikilink")
[Category:草津市](../Category/草津市.md "wikilink")
[Category:大津市](../Category/大津市.md "wikilink")
[Category:高島市](../Category/高島市.md "wikilink")
[Category:日本地理之最](../Category/日本地理之最.md "wikilink")

1.  [《陸水学雑誌》〈琵琶湖南湖の面積について〉，芳賀裕樹，2006年](http://www.jstage.jst.go.jp/article/rikusui/67/2/67_123/_article/-char/ja)