**祖語**（）又称**原始语**、**基础语**或**母语**\[1\]，[歷史語言學的重要概念](../Page/歷史語言學.md "wikilink")，指同一[系屬中若干相關語言的共同祖先](../Page/语言系属分类.md "wikilink")——一種古代語言，或者是處於[語言起源的某一階段的尚不能稱作](../Page/語言起源.md "wikilink")「語言」的一個溝通體系。

## 從現存語言推演出來的古語

這是「祖語」的最初定義，也是普遍認可的定義。即認為祖語是某個[語言家族](../Page/語言.md "wikilink")（如[語系](../Page/語系.md "wikilink")、[語族](../Page/語族.md "wikilink")、[語支](../Page/語支.md "wikilink")）的始祖[語言](../Page/語言.md "wikilink")——或稱**原語**。

通常情況下，祖語不能被直接認知，而是要運用一種被稱作[比照法的技法通過對同系屬的現存語言進行比較而](../Page/比照法.md "wikilink")[構擬出來](../Page/構擬.md "wikilink")。比照法並不能構擬出祖語的全部細節，所求的祖語越古老，所能構擬的部分就越爲有限。已（部分）構擬出的無文獻記載的語言如[原始印歐語](../Page/原始印歐語.md "wikilink")、[原始烏拉爾語](../Page/原始烏拉爾語.md "wikilink")、[原始班圖語即分別爲](../Page/原始班圖語.md "wikilink")[印歐諸語](../Page/印歐語系.md "wikilink")、[烏拉爾諸語和](../Page/烏拉爾語系.md "wikilink")[班圖諸語的祖先](../Page/班圖語系.md "wikilink")。

當然，祖語也不盡然都是史前語言（無文字）。有的古語有相當完整的文獻材料，有的則通過碑刻等文物保留了部分資料。如[羅曼語族現代語言](../Page/羅曼語族.md "wikilink")[法語](../Page/法語.md "wikilink")、[意大利語](../Page/意大利語.md "wikilink")、[西班牙語](../Page/西班牙語.md "wikilink")、[羅馬尼亞語的祖語](../Page/羅馬尼亞語.md "wikilink")[世俗拉丁語的大部分特徵都以古代拉丁文文獻的形式保存了下來](../Page/通俗拉丁語.md "wikilink")\[2\]。又如，現代漢語的絕大多數方言都是[先秦漢語的直系後代](../Page/上古漢語.md "wikilink")，而先秦漢語的歷史文存也非常的豐富。雖然漢字不直接表音，對上古漢語語音體系和形態特徵的構擬相對困難，但是詞彙和語法等語言元素都被相當完好地記錄了下來。再如[斯堪的納維亞和](../Page/斯堪的納維亞.md "wikilink")[不列顛島上發現的](../Page/不列顛島.md "wikilink")[如尼銘文也爲](../Page/如尼字母.md "wikilink")[原始挪威語的構擬和驗證提供了寶貴的第一手資料](../Page/原始挪威語.md "wikilink")。

## 非完整語言的溝通體系

[Derek
Bickerton絕對意義上的](../Page/Derek_Bickerton.md "wikilink")「祖語」下了定義，認為這種溝通體系的特徵是：

  - 缺乏充分發展的[語法](../Page/語法.md "wikilink")
  - 缺乏[時](../Page/时态.md "wikilink")、[體](../Page/体_\(语法\).md "wikilink")、[式](../Page/语气.md "wikilink")、[態和](../Page/语态.md "wikilink")[助動詞等形態](../Page/助動詞.md "wikilink")
  - 詞彙（非語彙）相對封閉

該定義實質上是對祖語——Proto-language——的逐字拆解，取「Proto-」「最初、原始」的意思，也就是尚未成熟的準語言，或許將其譯作「前語言」較為妥當。歷史語言學家中很少有人將語言的起源作為研究課題，而且從研究方法上看「前語言」和通常意義上的「祖語」其推演方式也是迥然不同的。

Bickerton認為先民以「前語言」進行溝通可能與[皮欽語的會話或](../Page/皮欽語.md "wikilink")[狼孩所講的話有某些相似之處](../Page/野孩子.md "wikilink")。如在[洋涇浜英語中](../Page/洋涇浜英語.md "wikilink")：

  - 甲說：**王之**（wanchee=want）**琶海**（buy）——義爲「請付錢」。
  - 乙說：**配**（pay）**由**（you）**陪陪**（by \*and\* by）——義爲「過兩天再給你」。\[3\]

語法和詞彙都含混不清，僅能有限地交流。

Bickerton以爲，真正的語言就是在這種「前語言」的基礎上經歷了某種類似[宇宙大爆發的劇烈變化而產生](../Page/宇宙大爆發.md "wikilink")。而[Terrence
Deacon則在其著作](../Page/Terrence_Deacon.md "wikilink")*[The Symbolic
Species](../Page/The_Symbolic_Species.md "wikilink")*中闡述了與此截然相反的觀點。

## 參見

  - [比照法](../Page/比照法.md "wikilink")
  - [語言起源](../Page/語言起源.md "wikilink")

## 注釋

## 參考文獻

  -
  -
[祖語](../Category/祖語.md "wikilink")
[Category:歷史語言學](../Category/歷史語言學.md "wikilink")

1.
2.  [古典拉丁文與世俗拉丁語之間存在著不少差異](../Page/古典拉丁文.md "wikilink")，但兩者的傳承關係則是明確無疑的。
3.  《别琴竹枝词》例22[1](http://shc2000.sjtu.edu.cn/031207/bieqin.htm)