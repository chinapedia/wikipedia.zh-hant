[HK_Causeway_Bay_St_Pauls_Convent_School.jpg](https://zh.wikipedia.org/wiki/File:HK_Causeway_Bay_St_Pauls_Convent_School.jpg "fig:HK_Causeway_Bay_St_Pauls_Convent_School.jpg")[禮頓道](../Page/禮頓道.md "wikilink")140號\]\]
[HK_CWB_聖保祿學校_St_Paul's_Convert_School_facade_March_2016_DSC_曦巒_Park_Haven.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_聖保祿學校_St_Paul's_Convert_School_facade_March_2016_DSC_曦巒_Park_Haven.JPG "fig:HK_CWB_聖保祿學校_St_Paul's_Convert_School_facade_March_2016_DSC_曦巒_Park_Haven.JPG")\]\]

**聖保祿學校**（），由[天主教開辦](../Page/天主教.md "wikilink")，為[香港補助學校議會](../Page/香港補助學校議會.md "wikilink")22所補助學校之一，是[香港一家女子學校](../Page/香港.md "wikilink")。

聖保祿學校創立於1854年，原名法國傳道會學校，1955年易名聖保祿學校。

聖保祿學校位於香港[銅鑼灣](../Page/銅鑼灣.md "wikilink")[禮頓道](../Page/禮頓道.md "wikilink")，建築物包括[修院和教堂](../Page/聖保祿修院.md "wikilink")，在庭院中建有[基督君王小堂](../Page/基督君王小堂.md "wikilink")。修院建於1914年。現在由[司徒惠建築師事務所設計的校舍於](../Page/司徒惠.md "wikilink")1981年落成，當時因應某地產商意圖洽購有關地段興建[酒店](../Page/酒店.md "wikilink")，但為學校的校友反對。當時有人捐了一大筆錢，用以永久購入學校所在的地段，餘下的錢則用作重建校舍，使校舍從原來的歐陸式建築變成今日的近現代建築。新校舍無論在環境和配套，在往後20年的香港來講都算非常吸引，特別是校舍中央的[游泳池](../Page/游泳池.md "wikilink")。即使在今日，學校在銅鑼灣鬧市中也顯得十分搶眼。

聖保祿學校的夏季校服為白色背扣短袖襯衣、格仔裙配黑白雙色鞋（熊貓鞋），幼稚園和小學生結[蝴蝶領帶](../Page/蝴蝶領帶.md "wikilink")、中學生結普通領帶，而冬季校服為白色長袖襯衣、寶藍色裙配黑白雙色鞋（熊貓鞋）。聖保祿學校的校服在香港中學校服中甚為突出。

聖保祿學校多年來培育出不少名人名媛，2004年9月轉為[直接資助計劃學校](../Page/直接資助計劃.md "wikilink")。

## 辦學宗旨

以基督之價值觀，提供卓越的全人教育，使學生具有仁愛、良知、自信、勇毅、創新、能幹及負責任之美德，提升生活素質，對家庭、職業及社會作出貢獻。

## 校歌

  -
    Hark\! Daughters of the great St. Paul,
    Come listen to his call:
    "O children of this loved school
    The loving nurse of all.
    Rejoice in God, do work and pray
    Be true from day to day."

<!-- end list -->

  -
    Beloved school of mine,
    My pains and joys are thine.
    My childhood's early dreams,
    Are closely linked to thee.
    The hope that Heaven brings,
    The dost unfold to me,
    The dost unfolds to me.

<!-- end list -->

  -
    Sweet are the days of girlhood,
    When friends we love and care.
    Those golden links of childhood,
    Whose sympathy we share.
    Do stay and while the hours away,
    With us in work and pray.

<!-- end list -->

  -
    And when we leave our dear old school,
    These mem'ries we'll recall\!
    These mem'ries we'll recall\!

## 學社

  - **CAVELL HOUSE**

<!-- end list -->

  -
    藍色

<!-- end list -->

  - **KENNY HOUSE**

<!-- end list -->

  -
    綠色

<!-- end list -->

  - **CURIE HOUSE**

<!-- end list -->

  -
    黃色

<!-- end list -->

  - '''KELLER HOUSE

<!-- end list -->

  -
    紅色

<!-- end list -->

  - **MASON HOUSE**

<!-- end list -->

  -
    橙色

<!-- end list -->

  - **MORRIS HOUSE**

<!-- end list -->

  -
    紫色

## 香港傑出學生選舉

直至2018年(第33屆)，聖保祿學校共出產18名[香港傑出學生](../Page/香港傑出學生選舉.md "wikilink")\[1\]，在香港所有中學中排名第4。

## 著名校友

  - [鄭碧影](../Page/鄭碧影.md "wikilink") - 香港已故粵劇花旦、電影演員
  - [郭美超](../Page/郭美超.md "wikilink") -
    [香港](../Page/香港.md "wikilink")[高等法院上訴法庭](../Page/高等法院.md "wikilink")[法官](../Page/法官.md "wikilink")
  - [露雲娜](../Page/露雲娜.md "wikilink") - 歌手
  - [陳美玲](../Page/陳美玲_\(香港藝人\).md "wikilink") - 歌手
  - [黎芷珊](../Page/黎芷珊.md "wikilink") - 電視節目主持
  - [關淑怡](../Page/關淑怡.md "wikilink") - 歌手
  - [向海嵐](../Page/向海嵐.md "wikilink") - 藝員
  - [車婉婉](../Page/車婉婉.md "wikilink") - 歌手
  - [陸恭蕙](../Page/陸恭蕙.md "wikilink") -
    前[環境局](../Page/環境局.md "wikilink")[副局長](../Page/香港政治委任制度.md "wikilink")，前[香港立法局議員](../Page/香港立法局.md "wikilink")
  - [陳逸璇](../Page/陳逸璇.md "wikilink") - 歌手
  - [張燊悅](../Page/張燊悅.md "wikilink") -
    藝員、前[教育局](../Page/教育局.md "wikilink")[政治助理](../Page/香港政治委任制度.md "wikilink")[楊哲安妻子](../Page/楊哲安.md "wikilink")
  - [任葆琳](../Page/任葆琳.md "wikilink") -
    [南區區議會議員](../Page/南區區議會.md "wikilink")（[香港仔選區](../Page/香港仔.md "wikilink")）
  - [鄧蓮如](../Page/鄧蓮如.md "wikilink")<small>勳爵</small> -
    前[英國上議院](../Page/英國上議院.md "wikilink")[議員](../Page/議員.md "wikilink")、1990年獲[英女皇冊封](../Page/英女皇.md "wikilink")[終身貴族成為](../Page/終身貴族.md "wikilink")[女男爵](../Page/女男爵.md "wikilink")。前[行政局](../Page/香港特別行政區行政會議.md "wikilink")、[立法局首席議員](../Page/香港立法會.md "wikilink")。前[貿易發展局主席](../Page/貿易發展局.md "wikilink")
  - [許鞍華](../Page/許鞍華.md "wikilink") -
    [香港女性](../Page/香港.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")、[監製](../Page/監製.md "wikilink")、[編劇](../Page/編劇.md "wikilink")
  - [趙碩之](../Page/趙碩之.md "wikilink") - 藝員
  - [吳靄儀](../Page/吳靄儀.md "wikilink") -
    前[立法會議員（法律界）](../Page/第四屆香港立法會.md "wikilink")、[公民黨黨員](../Page/公民黨_\(香港\).md "wikilink")、執業[大律師](../Page/大律師.md "wikilink")
  - [陳祖泳](../Page/陳祖泳.md "wikilink") -
    前法國五月藝術節總監,香港電台[法識生活主持人](../Page/法識生活.md "wikilink")
  - [何超瓊](../Page/何超瓊.md "wikilink") -
    [信德集團](../Page/信德集團.md "wikilink")[董事總經理](../Page/董事總經理.md "wikilink")
  - [陳小芝](../Page/陳小芝.md "wikilink") - DJ、電台創作總監、現SmarTone 娛樂台節目總監
  - [陳英凝](../Page/陳英凝.md "wikilink") -
    [香港中文大學醫學院賽馬會公共衛生及基層醫療學院教授及副院長](../Page/香港中文大學.md "wikilink")（外務及協作）、2005年世界十大傑出青年、2007年香港人道年獎
  - [顏　嘉](../Page/顏嘉.md "wikilink") - 前瑞士寶盛銀行經理，大中華控股席蕭炎坤博士之子蕭文豪的妻子
  - [劉明軒](../Page/劉明軒.md "wikilink") - 香港有線電視娛樂台及娛樂新聞台女主播
  - [陳樂敏](../Page/陳樂敏.md "wikilink") - 前香港女歌手
  - [黄德如](../Page/黄德如.md "wikilink") -
    香港註冊[中醫師](../Page/中醫師.md "wikilink")、[作家](../Page/作家.md "wikilink")、[記者](../Page/記者.md "wikilink")、節目主持人以及主播，前[無綫電視新聞記者及主播](../Page/無綫電視.md "wikilink")
  - [盧凱彤](../Page/盧凱彤.md "wikilink") -
    香港已故歌唱組合[at17成員](../Page/at17.md "wikilink")、唱作歌手
  - [吳芷寧](../Page/吳芷寧.md "wikilink") -
    [香港電台記者](../Page/香港電台.md "wikilink")、前[香港新聞從業員](../Page/香港.md "wikilink")、主播及[記者](../Page/記者.md "wikilink")
  - [江海迦](../Page/江海迦.md "wikilink") -
    香港[創作歌手](../Page/創作歌手.md "wikilink")
  - [謝文欣](../Page/謝文欣.md "wikilink") - 香港藝人
  - [陳曦靈](../Page/陳曦靈.md "wikilink") - 會計師
  - [陳偉琪](../Page/陳偉琪.md "wikilink") - 藝員
  - [黎紀君](../Page/黎紀君.md "wikilink") - 模特兒
  - [羅霖](../Page/羅霖.md "wikilink") - 藝員\[2\]
  - [可宜](../Page/可宜.md "wikilink") - 節目主持
  - [葛珮帆](../Page/葛珮帆.md "wikilink") - 立法會議員（小四至小六）\[3\]
  - 司徒淑嫻 - [循道中學前校長](../Page/循道中學.md "wikilink")（1953年畢業）

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")：

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[銅鑼灣站F出口](../Page/銅鑼灣站.md "wikilink")

</div>

</div>

## 鄰近建築

### 私人屋苑

  - [光明臺](../Page/光明臺.md "wikilink")（[大坑道](../Page/大坑道.md "wikilink")5-7號）-
    [明星](../Page/明星.md "wikilink")[郭富城家人及](../Page/郭富城.md "wikilink")[關智斌居住於此](../Page/關智斌.md "wikilink")
  - \-{R|[帝后臺](../Page/帝后臺.md "wikilink")（[大坑道](../Page/大坑道.md "wikilink")26號）}-
  - [麗星樓](../Page/麗星樓.md "wikilink")（現重建為[豪宅](../Page/豪宅.md "wikilink")「上林」）（[大坑道](../Page/大坑道.md "wikilink")15號）
  - [大寶閣](../Page/大寶閣.md "wikilink")（[大坑道](../Page/大坑道.md "wikilink")70號）-
    [明星](../Page/明星.md "wikilink")[鄭秀文及家人居住於此](../Page/鄭秀文.md "wikilink")
  - [豪園](../Page/豪園.md "wikilink")（嘉寧徑／[光明臺斜對面](../Page/光明臺.md "wikilink")）-
    [明星](../Page/明星.md "wikilink")[陳寶珠及家人居住於此](../Page/陳寶珠.md "wikilink")
  - [龍園](../Page/龍園.md "wikilink")（[大坑道春暉台](../Page/大坑道.md "wikilink")1號）
  - [渣甸山名門](../Page/名門_\(香港\).md "wikilink")（[大坑徑](../Page/大坑徑.md "wikilink")23號）
  - [禮頓山](../Page/禮頓山_\(住宅\).md "wikilink")（[樂活道](../Page/樂活道.md "wikilink")2B號）-
    [明星](../Page/明星.md "wikilink")[陳偉霆](../Page/陳偉霆.md "wikilink")、[鄭少秋](../Page/鄭少秋.md "wikilink")、前政務司司長[許仕仁居住於此](../Page/許仕仁.md "wikilink")
  - [比華利山](../Page/比華利山_\(香港\).md "wikilink")（[樂活道](../Page/樂活道.md "wikilink")6號）

### 其他

  - [聖公會聖馬利亞堂](../Page/聖公會聖馬利亞堂.md "wikilink")
  - [聖保祿醫院](../Page/聖保祿醫院.md "wikilink")
  - [皇仁書院](../Page/皇仁書院.md "wikilink")
  - [香港真光中學](../Page/香港真光中學.md "wikilink")
  - [中華基督教會公理高中書院](../Page/中華基督教會公理高中書院.md "wikilink")
  - [虎豹別墅](../Page/虎豹別墅_\(香港\).md "wikilink")
  - 中華遊樂會 -
    [富豪](../Page/富豪.md "wikilink")（例如[何鴻燊或](../Page/何鴻燊.md "wikilink")[劉鑾雄](../Page/劉鑾雄.md "wikilink")）打球之地
  - 摩頓台天橋

## 外部連結

  - [聖保祿學校網頁](http://www.spcs.edu.hk/)
  - [聖保祿學校創意媒體中心](http://spcscm.hkmediaed.net/)

## 參考來源

[Category:銅鑼灣](../Category/銅鑼灣.md "wikilink")
[Category:香港直資學校](../Category/香港直資學校.md "wikilink")
[Category:香港天主教學校](../Category/香港天主教學校.md "wikilink")
[Category:香港補助學校議會](../Category/香港補助學校議會.md "wikilink")
[S](../Category/灣仔區中學.md "wikilink")
[Category:灣仔區小學](../Category/灣仔區小學.md "wikilink")
[Category:1854年創建的教育機構](../Category/1854年創建的教育機構.md "wikilink")
[Category:香港女子中學](../Category/香港女子中學.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.  [Past Winners of the Hong Kong Outstanding Students
    Awards](http://youtharch.org/osa_history.html)
2.
3.