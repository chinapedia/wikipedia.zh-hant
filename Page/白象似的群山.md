《**白象似的群山**》（**Hills Like White
Elephants**）是[歐內斯特·海明威的](../Page/歐內斯特·海明威.md "wikilink")[短篇故事](../Page/短篇故事.md "wikilink")（[Short
story](../Page/:en:Short_story.md "wikilink")），是關於一個[美國人與其](../Page/美國人.md "wikilink")[懷孕的](../Page/懷孕.md "wikilink")[女友的故事](../Page/女友.md "wikilink")。

這篇故事首次發表在1927年出版的小說集《[沒有女人的男人们](../Page/沒有女人的男人们.md "wikilink")》（[Men
Without
Women](../Page/:en:Men_Without_Women.md "wikilink")）之中。故事用記敘手法來描寫男女間的關係和[墮胎的問題](../Page/墮胎.md "wikilink")。男人希望女人去墮胎，卻假意尊重女子的看法。而女子根本不願意墮胎，卻又假裝願意去作。兩人從頭到尾並未提到「墮胎」的字眼，不過對話內容，無字無句不在糾結於「墮胎」的話題上。

《白象似的群山》通常在一般[美國文學課程中都會當作教材](../Page/美國文學.md "wikilink")，原因是簡潔且易得，它包括了獨創巧妙的[象徵](../Page/象徵主義.md "wikilink")，有效且有力的對話，以及將廣泛性議題應用到爭議話題（[墮胎](../Page/墮胎.md "wikilink")），這個議題在過去不曾被如此明確的描述，而這篇故事是首次的探索。這些元素構成這個故事易於引導進海明威最低限度的故事風格，此即他所陳述的[冰山理論](../Page/冰山理論.md "wikilink")（[Iceberg
Theory](../Page/:en:Iceberg_Theory.md "wikilink")）。它也勾勒了角色設定可以對小說的意義提供多大的貢獻程度。\[1\]

## 參考文獻

<references/>

## 外部連結

  - [故事全文](http://www.millionbook.net/wg/h/haimingwei/dpxs/039.htm)

  - [故事全文](https://web.archive.org/web/20081001205701/http://www.moonstar.com/~acpjr/Blackboard/Common/Stories/WhiteElephants.html)

  - [《白象似的群山》專業文學分析](http://www.gummyprint.com/blog/hills-like-white-elephants-literary-analysis/)

[Category:美國小說](../Category/美國小說.md "wikilink")

1.  Fletcher, M. "Hemingway's Hills Like White Elephants." *Explicator*,
    Summer 1980. Vol. 38 No. 4. p. 16