**阮潢**（，）是[越南的](../Page/越南.md "wikilink")[廣南阮主第一代領袖](../Page/阮主.md "wikilink")，他是[阮淦的次子](../Page/阮淦.md "wikilink")，為阮氏所生。

## 生平

1527年，阮潢之父阮淦因不滿於[莫登庸的篡位](../Page/莫登庸.md "wikilink")，率[後黎朝遺臣逃往](../Page/後黎朝.md "wikilink")[哀牢](../Page/哀牢.md "wikilink")（今[寮國](../Page/寮國.md "wikilink")）以圖東山再起。阮潢被託付給太傅[阮於己照顧](../Page/阮於己.md "wikilink")。後來阮淦擁立[黎莊宗反抗](../Page/黎莊宗.md "wikilink")[莫朝](../Page/莫朝.md "wikilink")，阮潢也從軍，在[玉山縣斬莫朝將領](../Page/玉山縣.md "wikilink")[鄭誌](../Page/鄭誌.md "wikilink")，獲得了黎莊宗的嘉獎。後因軍功，封**端郡公**（）。

元和十三年（1545年），[阮淦死后](../Page/阮淦.md "wikilink")，由長子[阮汪任左丞相](../Page/阮汪.md "wikilink")、女婿[郑检任右丞相](../Page/郑检.md "wikilink")，同时荫封阮潢为夏溪侯。不過鄭檢掌握了兵权，最終殺死了阮汪，獨攬朝政。阮潢非常不安，與[阮於己一起自稱得病](../Page/阮於己.md "wikilink")，再不上朝。阮潢為求自保，派人問[阮秉謙該怎麼辦](../Page/阮秉謙.md "wikilink")。阮秉謙告訴阮潢：「[橫山一帶](../Page/橫山_\(越南\).md "wikilink")，萬代容身」，建議阮潢在南方建立根據地。阮潢深以為然，通過姐姐[阮氏玉寶的關係](../Page/阮氏玉寶.md "wikilink")，要求姐夫鄭檢讓他鎮守順化。[正治元年](../Page/正治.md "wikilink")（1558年），鄭檢同意了這個請求，阮潢便借守[顺化之机在南方重新奠定起阮氏基业](../Page/顺化.md "wikilink")，當地人稱之為**僊主**（）。

雖然阮潢身處順化，但他仍舊向後黎朝朝廷效忠，並派兵協助鄭檢攻打莫朝，並兩次率兵入朝，覲見黎帝。正治十二年（1569年）九月，阮潢入覲黎帝，正治十三年（1570年）正月返回順化，黎帝召還廣南總兵[阮伯駉](../Page/阮伯駉.md "wikilink")，以阮潢兼領順化廣南二處。嘉泰元年（1573年），黎帝晉封阮潢為太傅，令其向西都繳納貢賦。光興十六年（1593年）四月，黎帝還都東都升龍；五月，阮潢入覲。黎帝晉封中軍都督府左都督掌府事太尉**端國公**。[慎德元年五月](../Page/慎德.md "wikilink")，阮潢回到順化，自此阮主不再前往升龍覲見黎帝。弘定五年（1604年），阮潢重劃順化廣南二處行政區劃，分為六府二十二縣。弘定十二年（1611年），阮潢初置富安府，設立同春、綏和二縣。

弘定十四年六月初三日（1613年7月20日），阮潢去世，壽八十八歲，黎帝追赠**謹義公**（）。阮福源諡為**謹義達理顯應昭祐耀靈嘉裕王**（）。景興五年四月十二日（1744年5月23日），[阮世宗正式稱王](../Page/阮世宗.md "wikilink")，改諡其為**肇基垂統欽明恭懿謹義達理顯應昭祐耀靈嘉裕太王**（），[廟號](../Page/廟號.md "wikilink")**烈祖**（）。

嘉隆五年六月初八日（1806年7月23日），阮世祖[阮福映追尊其廟號為](../Page/阮福映.md "wikilink")**太祖**（），諡號為**肇基垂統欽明恭懿謹義達理顯應昭祐耀靈嘉裕皇帝**（），陵為[長基陵](../Page/長基陵.md "wikilink")（）。

阮潢普遍被認為是[阮主的奠基者](../Page/阮主.md "wikilink")，他開啟了廣南和日本的緊密關係。他曾收日本商人[船本顯定](../Page/船本顯定.md "wikilink")（彌七郎）為義子。

## 家庭

<center>

</center>

### 妻室

  - [嘉裕皇后阮氏](../Page/嘉裕皇后.md "wikilink")

| 封號    | 姓名                               | 生卒年 | 生平  |
| ----- | -------------------------------- | --- | --- |
| 嘉裕皇后  | [阮氏](../Page/嘉裕皇后.md "wikilink") |     | 生1子 |
| 端國太夫人 |                                  |     | 生1子 |
| 明德王太妃 |                                  |     | 生1子 |

### 子女

阮潢有10子2女。

<table>
<thead>
<tr class="header">
<th><p>排行</p></th>
<th><p>封號</p></th>
<th><p>諡號</p></th>
<th><p>姓名</p></th>
<th><p>生卒年</p></th>
<th><p>生母</p></th>
<th><p>生平</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>皇子</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>和郡公</p></td>
<td></td>
<td><p><a href="../Page/阮河.md" title="wikilink">阮河</a></p></td>
<td><p>？年－1576年4月19日</p></td>
<td><p>端國太夫人</p></td>
<td><p>有6子</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>莅郡公<br />
莅仁公</p></td>
<td></td>
<td><p><a href="../Page/阮漢.md" title="wikilink">阮漢</a></p></td>
<td><p>？年－1593年10月19日</p></td>
<td><p>不詳</p></td>
<td><p>有2子</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/阮成.md" title="wikilink">阮成</a></p></td>
<td></td>
<td><p>不詳</p></td>
<td><p>十七歲去世</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>豪郡公</p></td>
<td><p>義烈</p></td>
<td><p><a href="../Page/阮演.md" title="wikilink">阮演</a></p></td>
<td><p>？年－1598年</p></td>
<td><p>不詳</p></td>
<td><p>有4子</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>錦郡公</p></td>
<td><p>雄俊</p></td>
<td><p><a href="../Page/阮海.md" title="wikilink">阮海</a></p></td>
<td><p>？年－1616年12月24日</p></td>
<td><p>不詳</p></td>
<td><p>有4子</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>熙宗</p></td>
<td></td>
<td><p><a href="../Page/阮福源.md" title="wikilink">阮福源</a></p></td>
<td><p>1563年8月16日－1635年11月19日</p></td>
<td><p>嘉裕皇后</p></td>
<td><p>有12子4女1養女</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>文郡公</p></td>
<td></td>
<td><p><a href="../Page/阮福洽.md" title="wikilink">阮福洽</a></p></td>
<td></td>
<td><p>不詳</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>右郡公</p></td>
<td></td>
<td><p><a href="../Page/阮福澤.md" title="wikilink">阮福澤</a></p></td>
<td></td>
<td><p>不詳</p></td>
<td><p>無嗣</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>義郡公</p></td>
<td></td>
<td><p><a href="../Page/阮福洋.md" title="wikilink">阮福洋</a></p></td>
<td></td>
<td><p>不詳</p></td>
<td><p>無嗣</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>祥郡公<br />
義興郡王</p></td>
<td><p>忠誼<br />
忠直</p></td>
<td><p><a href="../Page/阮福溪.md" title="wikilink">阮福溪</a></p></td>
<td><p>1589年－1646年</p></td>
<td><p>明德王太妃</p></td>
<td><p>有13子</p></td>
</tr>
<tr class="even">
<td><p><strong>皇女</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/阮氏玉僊.md" title="wikilink">阮氏玉僊</a></p></td>
<td></td>
<td><p>不詳</p></td>
<td><p>嫁嚴郡公</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>正妃</p></td>
<td><p>慈順</p></td>
<td><p><a href="../Page/阮氏玉秀.md" title="wikilink">阮氏玉秀</a></p></td>
<td><p>？年－1631年4月24日</p></td>
<td><p>不詳</p></td>
<td><p>嫁鄭主<a href="../Page/郑梉.md" title="wikilink">郑梉</a></p></td>
</tr>
</tbody>
</table>

## 參考資料

  - [《大南實錄前編卷一·太祖嘉裕皇帝》](http://lib.nomfoundation.org/collection/1/volume/179/page/36)

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**阮潢**

[Category:廣南國君主](../Category/廣南國君主.md "wikilink")
[Category:阮肇祖公子](../Category/阮肇祖公子.md "wikilink")
[Category:阮朝追尊皇帝](../Category/阮朝追尊皇帝.md "wikilink")
[Category:後黎朝國公](../Category/後黎朝國公.md "wikilink")
[Category:太尉](../Category/太尉.md "wikilink")
[Category:後黎朝三師三少三公](../Category/後黎朝三師三少三公.md "wikilink")
[Category:後黎朝侯爵](../Category/後黎朝侯爵.md "wikilink")