**辛烷**的[化學式](../Page/化學式.md "wikilink")[C](../Page/碳.md "wikilink")<sub>8</sub>[H](../Page/氫.md "wikilink")<sub>18</sub>，[烷烴中的第八個成員](../Page/烷烴.md "wikilink")。
有18種[同分異構體](../Page/同分異構體.md "wikilink")（若包括[立体异构则有](../Page/立体异构.md "wikilink")22种）。

其中的異構物[2,2,4-三甲基戊烷作為汽油引擎燃料使用時](../Page/2,2,4-三甲基戊烷.md "wikilink")，震爆現象很低，因此被定為[辛烷值](../Page/辛烷值.md "wikilink")100的標準。

18種同分異構物如下：

1.  **正辛烷**
2.  [2-甲基庚烷](../Page/2-甲基庚烷.md "wikilink")
3.  [3-甲基庚烷](../Page/3-甲基庚烷.md "wikilink")
4.  [4-甲基庚烷](../Page/4-甲基庚烷.md "wikilink")
5.  [2,2-二甲基己烷](../Page/2,2-二甲基己烷.md "wikilink")
6.  [3,3-二甲基己烷](../Page/3,3-二甲基己烷.md "wikilink")
7.  [3-乙基己烷](../Page/3-乙基己烷.md "wikilink")
8.  [2,3-二甲基己烷](../Page/2,3-二甲基己烷.md "wikilink")
9.  [2,4-二甲基己烷](../Page/2,4-二甲基己烷.md "wikilink")
10. [2,5-二甲基己烷](../Page/2,5-二甲基己烷.md "wikilink")
11. [3,4-二甲基己烷](../Page/3,4-二甲基己烷.md "wikilink")
12. [2,3,4-三甲基戊烷](../Page/2,3,4-三甲基戊烷.md "wikilink")
13. [2,2,3-三甲基戊烷](../Page/2,2,3-三甲基戊烷.md "wikilink")
14. [2,2,4-三甲基戊烷](../Page/2,2,4-三甲基戊烷.md "wikilink")
15. [2,3,3-三甲基戊烷](../Page/2,3,3-三甲基戊烷.md "wikilink")
16. [2-甲基-3-乙基戊烷](../Page/2-甲基-3-乙基戊烷.md "wikilink")
17. [3-甲基-3-乙基戊烷](../Page/3-甲基-3-乙基戊烷.md "wikilink")
18. [2,2,3,3-四甲基丁烷](../Page/2,2,3,3-四甲基丁烷.md "wikilink")

[08](../Category/烷烃.md "wikilink")