**5channel**（，一般簡稱**5ch**），2017年10月1日更为現名，舊名**2channel**（，簡稱**2ch**），是一個大型的[日本](../Page/日本.md "wikilink")[網路論壇](../Page/網路論壇.md "wikilink")，擁有超過1170萬用戶（2009年統計）\[1\]。2ch基本上是一個非常巨大的[留言板集合體](../Page/留言板.md "wikilink")。2ch也是網域名稱的正確拼法（2ch.net）。2007年統計顯示，2ch的用戶每天平均創建共約250萬個帖子\[2\]\[3\]。

该网站由[西村博之創立於](../Page/西村博之.md "wikilink")1999年，在日本社會有著一定的影響力，其影響力与广播、電視及报刊等的傳統媒體不相上下\[4\]\[5\]\[6\]。截至2008年，2ch每年的收入達到1億[日圓](../Page/日圓.md "wikilink")\[7\]。它在Packet
Monster公司的管理下合法經營，而Packet
Monster公司則是一個位於[新加坡](../Page/新加坡.md "wikilink")[直落亚逸的公司](../Page/直落亚逸.md "wikilink")\[8\]\[9\]\[10\]。

## 簡介

2ch是在1999年時由西村博之在美國[阿肯色州](../Page/阿肯色州.md "wikilink")[康威市的](../Page/康威_\(阿肯色州\).md "wikilink")[阿肯色中央大學](../Page/阿肯色中央大學.md "wikilink")（）求學時所居住大學公寓中初創，\[11\]\[12\]2009年1月2日移交给PACKET
MONSTER公司\[13\]其伺服器架設於[美國](../Page/美國.md "wikilink")。而今日該站上大部份的更新，則都是由2ch用戶自願參與的。2ch是巨大的[留言版群](../Page/留言版.md "wikilink")，細分成多個[分類](../Page/分類.md "wikilink")（category），每個分類內有多個[留言板](../Page/留言板.md "wikilink")，留言板內可自由開立[討論串](../Page/討論串.md "wikilink")（（sure），取自英語thread），討論串則由回應（（resu），取自英語response）構成\[14\]。2ch的架構非常簡單，甚至較[telnet環境的](../Page/telnet.md "wikilink")[BBS系統都要簡單不少](../Page/BBS.md "wikilink")。

2ch網站本身的運作是非營利性的，在過去該站的主要收入來源是刊登包含[成人網站在內的](../Page/成人網站.md "wikilink")[交友網站廣告](../Page/交友網站.md "wikilink")，之後又出現[網路賭場的廣告刊登](../Page/網路賭場.md "wikilink")。除此之外，被稱為「●」（，「點」之意）的過去紀錄收費查閱系統，也帶給網站相當程度的資金捐注。\[15\]

### 影子公司

2012年，东京[警視廳在调查一宗利用该网站贩毒的案件时发现收购该网站的新加坡母公司PACKET](../Page/警視廳.md "wikilink")
MONSTER公司是一家[影子公司](../Page/空殼公司.md "wikilink")\[16\]，而该网站的真正管理者却未能找到\[17\]。

## 討論串

[Akushukai_repo.png](https://zh.wikipedia.org/wiki/File:Akushukai_repo.png "fig:Akushukai_repo.png")的2ch討論串\]\]

2ch限制每個討論串只有1000篇發言，總容量不超過500KB\[18\]。所以，熱門討論串在接近1000篇時，就會有人主動開立接續討論串，並指示正在看討論串的人準備移動。滿1000篇文章時，系統會自動貼出第1001篇表示停止，此時本討論串不得繼續發言。隔日，該討論串資料會從[伺服器上刪除](../Page/伺服器.md "wikilink")（稱為「」），數個月後才會以[紀錄](../Page/紀錄.md "wikilink")（log）的形式重新公開。由於近年實際上進行[HTML化的頻率越來越低](../Page/HTML.md "wikilink")，熱門的討論串，會有使用者在討論串被刪除前自己[備份到其他伺服器](../Page/備份.md "wikilink")。\[19\]

當討論串回應文章增加速度太快、閱覽人數過多等情況發生時，會發生所謂「」（人太多）現象，一般的[網頁瀏覽器會無法閱讀這種過於熱門的頁面](../Page/網頁瀏覽器.md "wikilink")，而只能使用專用閱覽軟體開啟。\[20\]

一般來說，2ch大部分版面禁止「實況」（根據現有的事件密集做即時留言、感想）。為了電視節目、廣播、偶像、運動等容易實況的話題，2ch下開立了「實況ch」分類，以獨立伺服器運作，所有實況文章應該在此發表。一般瀏覽器也無法直接閱覽實況ch。

## 匿名制度

2ch的特點是用戶可以使用「無名氏」的匿名身份留言，無需申請帳戶\[21\]，可以更自由地發言。但從另一方面來看，這也令不良份子可以從中生事，例如[洗板](../Page/洗板.md "wikilink")、引起[罵戰](../Page/罵戰.md "wikilink")、[誹謗](../Page/誹謗.md "wikilink")、恶意[剧透](../Page/剧透.md "wikilink")、散布[谣言或放置](../Page/谣言.md "wikilink")[電腦病毒](../Page/電腦病毒.md "wikilink")、（）等許多不負責任的網路行為。\[22\]

由於2ch匿名發言經常釀成[對簿公堂的事件](../Page/訴訟.md "wikilink")，受到[法院壓力](../Page/法院.md "wikilink")，現在除了自由輸入的暱稱（通常會是無名氏）之外，並加上了ID紀錄，但部分板除外。ID並不是使用者申請的帳號，而是[IP位址及日期經運算後的文字列](../Page/IP位址.md "wikilink")。\[23\]

## 2ch的用戶

2ch的用戶稱為「」（2channelor），簡稱「」或「」。\[24\]

2ch的用戶男性居多，目前使用率最高的是「」。
從2002年以來，使用率最高的版面一直是「」（關於[早安少女組的看板](../Page/早安少女組.md "wikilink")），多次2ch人數過多導致當機停擺事件，多與此板有關。主要的使用者為早安迷，故板內充斥各種[御宅用語](../Page/御宅.md "wikilink")。但因為其熱鬧性，也有大量非早安迷常駐。為了流量控制等原因，經過多次的版面分割，現在在2ch的使用率仍保持在第二高。部分深信2ch會對社會造成強大影響的忠實使用者，被稱為「」。

排斥2ch的人（主要是來自2ch的前身「」的使用者），愛稱2ch用戶為「」（清国奴）。是二戰前日本對中國人的蔑稱，由於亦與的同音，諧音做2ch使用者的蔑稱使用。\[25\]

### 用戶的年齡組別

根據NetRatings於2009年的調查顯示，2ch擁有超過1170萬用戶\[26\]。而[ITmedia的調查則顯示](../Page/ITmedia.md "wikilink")，年齡為10至19歲的用戶佔所有用戶的20.0%、20至29歲的用戶佔15.0%、30至39歲的用戶佔30.7%、40至49歲的用戶佔21.9%、而50歲以上的用戶佔12.5%\[27\]。此數據顯示，2ch用戶的主要年齡組別為30至49歲，佔了所有用戶的52.6%。

## 關於2ch的問題

### 誹謗和法律問題

2ch上唯一一種不被允許的帖子為惡意破壞帖子（如[垃圾帖子](../Page/垃圾郵件.md "wikilink")）以及觸犯[日本法律中誹謗條文的帖子](../Page/日本法律.md "wikilink")。另外，在[西鐵巴士劫持事件發生後](../Page/西鐵巴士劫持事件.md "wikilink")，2ch便決定把犯罪聲明的帖子交由警方處理。\[28\]

### 仇恨言論

因其龐大的規模和大量匿名發帖，所以難免充斥著對公眾人物、機構、少數族裔和特定族群\[29\]的誹謗和[仇恨言論](../Page/仇恨言論.md "wikilink")\[30\]。雖然2ch有關於刪除觸犯日本法律的帖子的規則，但是大量的匿名發帖使刪除工作難以執行。2ch的其中一個分類主要圍繞[韓國和](../Page/韓國.md "wikilink")[朝鮮](../Page/朝鮮.md "wikilink")，其討論內容上至韓國料理，下至韓國女性的性道德。其中一個典型的討論串名為「你更討厭誰，[中國人或](../Page/中國人.md "wikilink")[韓國人](../Page/韓國人.md "wikilink")？」，此討論串截至2003年3月共有365個回應\[31\]。其中一些回應表示希望殺死中國人和韓國人，而那些抨擊此討論串的用戶則被冠上「[日本朝鲜族](../Page/日本朝鲜族.md "wikilink")」和「愚蠢的韓國人」（バカチョン）的稱號\[32\]。在某些情況下，2ch會被指控沒有除去誹謗或仇恨言論帖子。\[33\]

### 保守主義

一些日本的網絡新聞媒體和來源指出，2ch成為了[右派中](../Page/右派.md "wikilink")[民族主義和](../Page/民族主義.md "wikilink")[愛國主義者的立足之地](../Page/愛國主義.md "wikilink")\[34\]。與2ch有很大關聯的[NICONICO動畫上的一個調查顯示](../Page/NICONICO動畫.md "wikilink")，支持[自由民主党的](../Page/自由民主党_\(日本\).md "wikilink")2ch用戶佔的比率明顯比其他地區的比率高\[35\]\[36\]。

### 過度色情

2ch的帖子和文字角色亦被指內容過度色情。另外，很多2ch的廣告都會經轉介系統\[37\]。把用戶傳送至成人網站\[38\]。

### 犯罪溫床

有些犯罪分子利用2ch徵集犯罪方法，犯罪同夥。

## 轉介系統

2ch使用一個「轉介系統」來鏈接所有帖子上的外部網站。當用戶點擊該鏈接時，便會被傳送到在ime.nu域名上的一個充滿廣告的頁面。用戶需要在該頁面中再次點擊鏈接才能到達網站。2ch亦會讓其他網站刊登廣告，而用戶點擊廣告後，會被傳送到相應的網站，且不能回到2ch。\[39\]因此，不少用戶都會把帖子上的[URL刪去首個](../Page/URL.md "wikilink")「h」字，例如把「http://www.wikipedia.org/」寫成「ttp://-{}-www.-{}-wikipedia.-{}-org/」。

## 2ch次文化

[2ch_AA_Characters.gif](https://zh.wikipedia.org/wiki/File:2ch_AA_Characters.gif "fig:2ch_AA_Characters.gif")

### 2ch用語

2ch有大量特殊用語，例如因為2ch使用者愛用無名氏的習慣，使得回應發言時不知道如何稱呼對方，故直接以對方發言編號回覆。如回覆第20篇發言，直接寫「\>\>20」。該數字也往往直接代替這個人，如[Winny的作者](../Page/Winny.md "wikilink")[金子勇在發表他想開發Winny的概念時](../Page/金子勇.md "wikilink")，文章編號是47，自此被稱為「47氏」。\[40\]\[41\]

其他常見的2ch用語如下：\[42\]

  - ：去死

  - ：辛苦了（「」的簡稱）

  - ：搞不清楚場面的人、拎不清、[白目](../Page/白目.md "wikilink")（，原意為要求對方「搞清楚狀況」的命令句）

  - ：直譯為「來了」，表示大事發生、有事發生了等情況的感嘆辭。

  - ：强烈同意，完全正确（（hageshikudōi））。

  - [DQN](../Page/DQN.md "wikilink")：低Q脑，脑子有问题的人。

  - 、、（蟑螂）：中国人的[蔑稱](../Page/蔑稱.md "wikilink")。（因为2ch住民认为福建人喜欢躲在轮船货柜中偷渡，所以将中国人形容为蟑螂）。

  - （amekō）：對[美國人的](../Page/美國人.md "wikilink")[蔑稱](../Page/蔑稱.md "wikilink")。

  - （ro），（rosuke），（恐怖的俄国佬，おそろしい在日语中是恐怖）：對[俄罗斯人的蔑稱](../Page/俄罗斯人.md "wikilink")。

  - ，：朝鲜人，韩国人。因为朝鲜族人喜欢吃（泡菜）。

  - （zainichi）：侨居日本的朝鲜人和朝鲜裔日本人。

  - ：楼主，thread发起者。

  - GJ：Good Job。干得好。

  - w，www，：笑。

  - （chū），（chūbō）：中学生，什么也不懂的家伙。

  - ，（chuuni），意指[青春期](../Page/青春期.md "wikilink")（叛逆期）什么也不懂且自大的人，名稱由來為進入青春期（叛逆期）的時期的平均值為中學二年級時（14～15歲）

  - （fuyuchū）：冬歇期（[寒假](../Page/寒假.md "wikilink")）来的傻子\[43\]

  - （natsuchū）：[暑假来的傻子](../Page/暑假.md "wikilink")

  - ：呆在家里没有被雇用的人。（「Not in Education, Employment or
    Training」的略寫,即是又沒有被雇用又不是在讀書進修的人，常被誤解為靠父母養的人，但其實也可以指自由業者）

  - （kichigai）：神经病，脑子不正常（）。

  - ，：网络右翼的缩写。

  - ：OK。

  - ry：以下省略（ryaku）

  - （hitogeto）、（nikana）：[沙发](../Page/沙發_（網路用語）.md "wikilink")！

  - （urayamashikei）：直譯為拖到后山執行[死刑](../Page/死刑.md "wikilink")。其實是羡慕（（urayamashii））他執行死刑吧。通常在[奸淫](../Page/奸淫.md "wikilink")[幼女被捕的帖子中使用](../Page/幼女.md "wikilink")。

其他還有些故意代用的習慣，如「」「」等。\[44\]

### 文字人物

### 衍生的社會現象

2ch在日本社會的影響力不小，也發生過幾次廣大社會注目的事件，其中影響好壞不一。

  - 產生了2ch獨有的文字人物。
  - 匿名制度被外國的網站所仿效，在中國、南韓、北韓方面批判不少。\[45\]
  - [電車男的愛情故事](../Page/電車男.md "wikilink")。\[46\]
  - 為[隱蔽青年和](../Page/隱蔽青年.md "wikilink")[御宅族帶來了交流的空間](../Page/御宅族.md "wikilink")。
  - 有一些人在2ch「預告」自己的犯罪行為，或者做出以他人狂言為本的犯罪行為。這些事件直接同時衝擊2ch留言版的匿名制度，動漫文化的和[御宅族的潛在問題](../Page/御宅族.md "wikilink")，令整個日本社會對以上事情頓時帶來了極壞的不良印象。\[47\]
  - 2ch充斥大量[排韓與](../Page/反韓.md "wikilink")[排華的言論](../Page/排華.md "wikilink")。\[48\]

## 参考文献

## 外部連結

  - [2ch網站](http://www.2ch.net)

      - [2典](https://web.archive.org/web/20060702103032/http://www.media-k.co.jp/jiten/)（2ch的線上用語辭典）

      - [Amazon.jp書單](http://www.amazon.co.jp/exec/obidos/ASIN/4835440331/250-6639415-0827444)（書本版2ch用語詞典）

      - [4-ch.net](http://4-ch.net/2chportal/)（2ch各板的英文名單和導覽）

      - [stats.2ch.net](http://stats.2ch.net/suzume.cgi?yes)（2ch每日的投稿數量）

  - [Jane Style](http://janesoft.net/janestyle/)

## 参见

  - [2chan](../Page/2chan.md "wikilink")
  - [Komica](../Page/Komica.md "wikilink")
  - [4chan](../Page/4chan.md "wikilink")
  - [Tripcode](../Page/Tripcode.md "wikilink")
  - [电车男](../Page/电车男.md "wikilink")
  - [宅男](../Page/宅男.md "wikilink")
  - [御宅族](../Page/御宅族.md "wikilink")
  - [腐女](../Page/腐女.md "wikilink")

[2ch](../Category/2ch.md "wikilink")
[Category:社群網站](../Category/社群網站.md "wikilink")
[Category:日本网站](../Category/日本网站.md "wikilink")
[Category:網絡討論區](../Category/網絡討論區.md "wikilink")
[Category:1999年建立的网站](../Category/1999年建立的网站.md "wikilink")
[Category:1999年日本建立](../Category/1999年日本建立.md "wikilink")

1.  [](http://www.netratings.co.jp/New_news/News12242008.htm)　2008年12月24日

2.

3.  2ch的官方統計數字：[stats.2ch.net](http://stats.2ch.net/suzume.cgi?yes)

4.
5.

6.

7.
8.

9.

10.

11.
12. Matsutani, Minoru, "[2channel's success rests on
    anonymity](http://search.japantimes.co.jp/cgi-bin/nn20100406i1.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed:%20japantimes%20%28The%20Japan%20Times:%20All%20Stories%29)",
    *[Japan Times](../Page/Japan_Times.md "wikilink")*, p. 3.

13. 西村博之
    「[2ch譲渡](http://www.asks.jp/users/hiro/54104.html)」『ひろゆき＠オープンSNS』
    2009年1月2日

14. [2ちゃん用語 Ver 1.0](http://pmakino.jp/channel5/misc/2chbible.html)

15.
16. [2ちゃんねる管理会社、実体なし…日本で運営か](https://web.archive.org/web/20120603012541/http://www.yomiuri.co.jp/net/news/20120328-OYT8T00220.htm)
    読売新聞2012年3月27日

17.

18. [２ちゃんねるって？](http://info.2ch.net/guide/faq.html)

19. [不滅　
    一人で暴言　はにゃ～ん　スレッド](http://kitanet.2ch.net/kitanettr/kako/970/970284472.html)

20. [人大杉 ひとおおすぎ](http://d.hatena.ne.jp/keyword/%BF%CD%C2%E7%BF%F9)

21.
22.
23.
24. [2ちゃんねらー
    にちゃんねらー](http://d.hatena.ne.jp/keyword/2%A4%C1%A4%E3%A4%F3%A4%CD%A4%E9%A1%BC)

25. [『チャンコロ（ちゃんころ）』の意味](http://zokugo-dict.com/17ti/chankoro.htm)

26.
27. [ブログ訪問者は1年で2倍の2000万超に　2chは990万人](http://www.itmedia.co.jp/news/articles/0511/29/news004.html)　[ITmediaニュース](../Page/ITmedia.md "wikilink")、2005年11月29日

28. [2channel founder says don't blame him for criminals'
    posts](http://www.japantoday.com/category/shukan-post/view/2channel-founder-says-dont-blame-him-for-criminals-posts#tool_button)


29.

30.

31.

32.
33. [a case made by Debito
    Arudou](http://www.debito.org/2channelsojou.html#english)

34.

35.

36. [survey, June, 2009, nicovideo,
    Japanese](http://www.nicovideo.jp/static/enquete/o/20090618.html)

37.

38. [2ch的轉介頁面](http://ime.nu/sankei.jp.msn.com/politics/election/091126/elc0911261939000-n1.htm)

39.

40.
41. [秋月高太郎](../Page/秋月高太郎.md "wikilink") 『日本語ヴィジュアル系 ―あたらしいにほんごのかきかた』
    [角川グループパブリッシング](../Page/角川グループパブリッシング.md "wikilink")、2009年、37頁。ISBN
    978-4047101586。

42.
43. [2channel portal: Vocabulary](http://services.4-ch.net/2chportal/)

44. 『「電車男」は誰なのか―“ネタ化”するコミュニケーション』18-26頁。

45. ["Ayashii
    World"](http://www.bienvenidoainternet.org/wiki/Ayashii_World)

46.

47.
48.