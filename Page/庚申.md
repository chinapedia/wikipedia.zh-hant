**庚申**为[干支之一](../Page/干支.md "wikilink")，顺序为第57个。前一位是[己未](../Page/己未.md "wikilink")，后一位是[辛酉](../Page/辛酉.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之庚屬陽之金](../Page/天干.md "wikilink")，[地支之申屬陽之金](../Page/地支.md "wikilink")，是比例和好。

## 庚申年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")57年称“**庚申年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘0，或年份數減3，除以10的餘數是7，除以12的餘數是9，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“庚申年”：

<table>
<caption><strong>庚申年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li>60年</li>
<li>120年</li>
<li>180年</li>
<li>240年</li>
<li>300年</li>
<li>360年</li>
<li>420年</li>
<li>480年</li>
<li>540年</li>
<li>600年</li>
<li>660年</li>
<li>720年</li>
<li>780年</li>
<li>840年</li>
<li>900年</li>
<li>960年</li>
</ul></td>
<td><ul>
<li>1020年</li>
<li>1080年</li>
<li>1140年</li>
<li>1200年</li>
<li>1260年</li>
<li>1320年</li>
<li>1380年</li>
<li>1440年</li>
<li>1500年</li>
<li>1560年</li>
<li>1620年</li>
<li>1680年</li>
<li>1740年</li>
<li>1800年</li>
<li>1860年</li>
<li>1920年</li>
<li>1980年</li>
</ul></td>
<td><ul>
<li>2040年</li>
<li>2100年</li>
<li>2160年</li>
<li>2220年</li>
<li>2280年</li>
<li>2340年</li>
<li>2400年</li>
<li>2460年</li>
<li>2520年</li>
<li>2580年</li>
<li>2640年</li>
<li>2700年</li>
<li>2760年</li>
<li>2820年</li>
<li>2880年</li>
<li>2940年</li>
<li>3000年</li>
</ul></td>
</tr>
</tbody>
</table>

## 庚申月

天干戊年和癸年，[立秋到](../Page/立秋.md "wikilink")[白露的時間段](../Page/白露.md "wikilink")，就是**庚申月**：

  - ……
  - 1978年8月立秋到9月白露
  - 1983年8月立秋到9月白露
  - 1988年8月立秋到9月白露
  - 1993年8月立秋到9月白露
  - 1998年8月立秋到9月白露
  - 2003年8月立秋到9月白露
  - 2008年8月立秋到9月白露
  - ……

## 庚申日

## 庚申時

天干戊日和癸日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）15時到17時，就是**庚申時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/庚申年.md "wikilink")