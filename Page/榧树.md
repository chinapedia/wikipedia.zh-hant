**香榧**（[学名](../Page/学名.md "wikilink")：**，又常稱為**中國榧**）属[三尖杉科](../Page/三尖杉科.md "wikilink")[榧属](../Page/榧属.md "wikilink")，为[常绿乔木](../Page/常绿乔木.md "wikilink")。主要生长在[中國南方较为湿润的地区](../Page/中國.md "wikilink")，目前香榧较多见的地方为[浙江](../Page/浙江省.md "wikilink")[东阳](../Page/东阳.md "wikilink")、[诸暨](../Page/诸暨.md "wikilink")、[富阳](../Page/富阳.md "wikilink")、[安徽](../Page/安徽省.md "wikilink")[黟县等地](../Page/黟县.md "wikilink")。香榧生长成熟期为三年：第一年开花，第二年结果，第三年成熟。香榧的果实为坚果，橄榄形，果壳较硬，内有黑色果衣包裹淡黄色果肉，可食用，营养丰富。在东亚国家榧木是被用来制作[棋盘的高级木料](../Page/棋盘.md "wikilink")。

<File:Torreya> grandis 20090130.jpg|上海植物园内的香榧 <File:Torreya> grandis
Merrillii1.jpg <File:Torreya> grandis Merrillii6.jpg <File:Torreya>
grandis Nusseibe.jpg

## 參考文献

## 外部链接

  -
[TG](../Category/IUCN無危物種.md "wikilink")
[grandis](../Category/榧属.md "wikilink")