**赵尔宓**（），[满族](../Page/满族.md "wikilink")，[伊尔根觉罗氏](../Page/伊尔根觉罗氏.md "wikilink")\[1\]，[中国](../Page/中国.md "wikilink")[两栖动物和](../Page/两栖动物.md "wikilink")[爬行动物学家](../Page/爬行动物.md "wikilink")，[中国科学院成都生物研究所研究员](../Page/中国科学院.md "wikilink")、[院士](../Page/中国科学院院士.md "wikilink")，[四川](../Page/四川.md "wikilink")[成都人](../Page/成都.md "wikilink")。\[2\]\[3\]

## 生平

赵中学就读于[树德中学](../Page/树德中学.md "wikilink")，1947年考入[華西協和大學生物系](../Page/華西協和大學.md "wikilink")，师从[刘承钊教授](../Page/刘承钊.md "wikilink")。1951年赵毕业后被分配到[哈尔滨医科大学担任助教](../Page/哈尔滨医科大学.md "wikilink")，1954年调回[四川医学院](../Page/四川医学院.md "wikilink")，协助[徐福均教授进行研究](../Page/徐福均.md "wikilink")。1962年起跟随刘承钊教授进行研究，很快取得一系列成果。

赵曾于1993年同[美国学者Kraig](../Page/美国.md "wikilink")
Adler合作出版《中国两栖爬行动物》（Herpetology of
China）一书。

## 注释

[Category:成都人](../Category/成都人.md "wikilink")
[Category:满族人](../Category/满族人.md "wikilink")
[Category:伊尔根觉罗氏](../Category/伊尔根觉罗氏.md "wikilink")
[E尔](../Category/赵姓.md "wikilink")
[Category:沈阳市满族联谊会会员](../Category/沈阳市满族联谊会会员.md "wikilink")
[Category:树德中学校友](../Category/树德中学校友.md "wikilink")
[Category:华西协和大学校友](../Category/华西协和大学校友.md "wikilink")

1.
2.  [著名两栖爬行动物学家赵尔宓成都病逝
    享年87岁](http://www.toutiao.com/a6368237315810722049/)
3.  [川大赵尔宓院士因病去世 享年87岁](http://news.chengdu.cn/2016/1226/1841855.shtml)