**费利佩·冈萨雷斯·马克斯**（[西班牙語](../Page/西班牙語.md "wikilink")：，）西班牙政治家，前任[西班牙首相](../Page/西班牙首相列表.md "wikilink")。

## 生平

冈萨雷斯曾就读于法律学校和比利时卢万天主教大学。1964年加入工人社会党。

1974年当选[西班牙工人社会党第一书记并于](../Page/西班牙工人社会党.md "wikilink")1979年9月再次当选。期间協助推动[西班牙民主转型](../Page/西班牙民主转型.md "wikilink")。

1982年領導工人社會黨取得勝利，出任西班牙首相，實現首次和平的[政黨輪替](../Page/政黨輪替.md "wikilink")，組建西班牙46年來首個左翼政府，並於1986年、1989年及1993年成功連任。

1996年大選失利下台，把政權移交予右翼的[西班牙人民黨](../Page/西班牙人民黨.md "wikilink")，實現第二次和平的政黨輪替。1997年辭去[西班牙工人社会党第一书记一職](../Page/西班牙工人社会党.md "wikilink")。

[Category:西班牙首相](../Category/西班牙首相.md "wikilink")
[Category:塞維利亞人](../Category/塞維利亞人.md "wikilink")
[Category:西班牙天主教徒](../Category/西班牙天主教徒.md "wikilink")
[Category:查理曼獎得主](../Category/查理曼獎得主.md "wikilink")
[Category:塞維利亞大學校友](../Category/塞維利亞大學校友.md "wikilink")
[Category:歐洲理事會主席](../Category/歐洲理事會主席.md "wikilink")
[Category:西班牙工人社会党党员](../Category/西班牙工人社会党党员.md "wikilink")