**陳昇琳**，原名**陳森霖**（），[台灣戲曲演員](../Page/台灣.md "wikilink")、[歌仔戲導演](../Page/歌仔戲.md "wikilink")、音樂家，[苗栗縣](../Page/苗栗縣.md "wikilink")[銅鑼鄉](../Page/銅鑼鄉_\(臺灣\).md "wikilink")[客家人](../Page/客家人.md "wikilink")，善演[老生](../Page/老生.md "wikilink")。

## 生平

陳父[陳錦海家境富有](../Page/陳錦海.md "wikilink")，學戲後於[客家戲團](../Page/客家戲.md "wikilink")[明興社演出](../Page/明興社.md "wikilink")；陳母[李秀英則於同團擔綱](../Page/李秀英.md "wikilink")[苦旦腳色](../Page/苦旦.md "wikilink")。陳昇琳青少年時開始學習[京劇藝術](../Page/京劇.md "wikilink")，拜[關漢樓與](../Page/關漢樓.md "wikilink")[陳拯文為師](../Page/陳拯文.md "wikilink")，並曾參加兼演歌仔戲的台灣京劇戲班「[金山樂社](../Page/金山樂社.md "wikilink")」、客家採茶戲班，有時參加樂隊工作，演奏[小喇叭](../Page/小喇叭.md "wikilink")、[薩克斯風等西樂器](../Page/薩克斯風.md "wikilink")。歌仔戲胡琴（殼子弦、大廣弦、喇叭弦、[六角弦](../Page/六角弦.md "wikilink")）、[二胡](../Page/二胡.md "wikilink")、[三弦](../Page/三弦.md "wikilink")、[月琴](../Page/月琴.md "wikilink")、[管](../Page/管.md "wikilink")、[笛](../Page/笛.md "wikilink")、[簫](../Page/簫.md "wikilink")、[嗩吶等傳統民樂器也很拿手](../Page/嗩吶.md "wikilink")。

早期演出作品以《呂布與貂蟬》（《鳳儀亭》）、《[斬經堂](../Page/斬經堂.md "wikilink")》（《吳漢殺妻》）、《秦香蓮》等京劇較有代表性。亦做過電影的武打演員，參與《[龍門客棧](../Page/龍門客棧.md "wikilink")》（[胡金銓作品](../Page/胡金銓.md "wikilink")）等片演出，主要合作的導演是[郭南宏](../Page/郭南宏.md "wikilink")。

他是歌仔戲演員[葉青的表哥](../Page/葉青.md "wikilink")，葉青領銜的[華視](../Page/華視.md "wikilink")[神仙歌劇團成立後](../Page/神仙歌劇團.md "wikilink")，應邀出任武術指導和身段指導，負責設計武術動作和身段，同時協助劇團導演[石文戶](../Page/石文戶.md "wikilink")（曜齊）、[余漢祥導戲](../Page/余漢祥.md "wikilink")。

他在神仙歌劇團學生班擔任指導教師，教身段教唱腔，並負責伴奏，學生有[石惠君](../Page/石惠君.md "wikilink")、陳秀嫦、狄鶯等人。曾為神仙歌劇團執導《金鵰玉芙蓉》、《蝶戀花》2檔電視歌仔戲，並兼武術指導、[身段指導](../Page/身段.md "wikilink")。\[1\]

他還曾為[中視黃香蓮歌仔戲團執導](../Page/中視.md "wikilink")《羅通掃北》，為[台視](../Page/台視.md "wikilink")[楊麗花歌仔戲團執導](../Page/楊麗花.md "wikilink")《新狄青》。

1990年，[明聲歌仔戲團](../Page/明聲歌仔戲團.md "wikilink")《琴劍恨》於[台北市](../Page/台北市.md "wikilink")[國立國父紀念館首演](../Page/國立國父紀念館.md "wikilink")，由陳昇琳擔任武術指導\[2\]

1991年起，他開始與「[河洛歌仔戲團](../Page/河洛歌仔戲團.md "wikilink")」合作，在《曲判記》、《天鵝宴》、《浮沉紗帽》、《欽差大臣》、《秋風辭》等戲中擔綱要角。

國立復興劇藝實驗學校歌仔戲科（[國立台灣戲曲學院歌仔戲學系前身](../Page/國立台灣戲曲學院.md "wikilink")）成立後，擔任專任教師。

2004年因中風臥病。2011年元月四日辭世。

## 電視歌仔戲導演作品

  - 《金鵰玉芙蓉》（[許再添音樂指導](../Page/許再添.md "wikilink")、文場領班、主弦，[蕭誠一武場領班](../Page/蕭誠一.md "wikilink")，神仙歌劇團，華視）
  - 《蝶戀花》（許再添音樂指導、文場領班、主弦，[王清松武場領班](../Page/王清松.md "wikilink")，神仙歌劇團，華視）
  - 《羅通掃北》（[簡永福音樂指導](../Page/簡永福.md "wikilink")、文場領班，[洪堯進主弦](../Page/洪堯進.md "wikilink")，王清松武場領班，黃香蓮歌仔戲團，中視）
  - 《新狄青》（[簡永福音樂指導](../Page/簡永福.md "wikilink")、文場領班，[洪堯進主弦](../Page/洪堯進.md "wikilink")，王清松武場領班，楊麗花歌仔戲團，台視）
  - 《楊宗保與穆桂英》（開元藝坊，力霸友聯電視台）
  - 《大劈棺》（開元藝坊，力霸友聯電視台）
  - 《孟麗君》（開元藝坊，力霸友聯電視台）
  - 《喬太守亂點鴛鴦譜》（開元藝坊，力霸友聯電視台）
  - 《王老虎搶親》（開元藝坊，力霸友聯電視台）
  - 《棒打薄情郎》（開元藝坊，力霸友聯電視台）

## 舞台歌仔戲導演作品

  - 《金枝玉葉》（[劉文亮音樂設計](../Page/劉文亮.md "wikilink")、主弦，陳美雲歌劇團）
  - 《秦香蓮》（劉文亮音樂設計、主弦，陳美雲歌劇團）
  - 《貍貓換太子》（《丹心救主》，劉文亮音樂設計、主弦，陳美雲歌劇團）
  - 《洛水之秋》（劉文亮音樂設計、主弦，[王清松打鼓](../Page/王清松.md "wikilink")，尚和歌仔戲劇團）
  - 《刺桐花開》（與[李小平合導](../Page/李小平.md "wikilink")，[莊步超打鼓](../Page/莊步超.md "wikilink")，陳美雲歌劇團）
  - 《河邊春夢》（時裝歌仔戲，合導，[呂冠儀音樂設計](../Page/呂冠儀.md "wikilink")，[周以謙主弦](../Page/周以謙.md "wikilink")，[莊步超](../Page/莊步超.md "wikilink")、[陳申南打鼓](../Page/陳申南.md "wikilink")，陳美雲歌劇團）等。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:台灣歌仔戲導演](../Category/台灣歌仔戲導演.md "wikilink")
[Category:京劇演員](../Category/京劇演員.md "wikilink")
[Category:台灣戲曲樂師](../Category/台灣戲曲樂師.md "wikilink")

1.  《蝶戀花》是神仙歌劇團最後1部戲，神仙歌劇團每1部戲的音樂指導都是[許再添](../Page/許再添.md "wikilink")，武術指導；身段指導都是陳昇琳。
2.  該戲由[王振義編劇](../Page/王振義.md "wikilink")、並擔任音樂指導；文場領班是[簡永福和許再添](../Page/簡永福.md "wikilink")，主弦是許再添，武場領班（鼓師）是王清松，[石文戶導演](../Page/石文戶.md "wikilink")。