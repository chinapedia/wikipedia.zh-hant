**高姓**为[中文姓氏之一](../Page/中文姓氏.md "wikilink")，在[排行第18位](../Page/百家姓#当代百家姓.md "wikilink")；《[百家姓](../Page/百家姓.md "wikilink")》中排名第153位。

## 起源

### 古時的夷族

### 姜姓

[周朝時](../Page/周朝.md "wikilink")，[周武王](../Page/周武王.md "wikilink")[牧野之戰平定](../Page/牧野之戰.md "wikilink")[天下](../Page/天下.md "wikilink")，后封[姜太公為](../Page/姜太公.md "wikilink")[齊國](../Page/齊國.md "wikilink")[君主](../Page/君主.md "wikilink")，太公六世孫被封於高地，人稱[公子高](../Page/公子高.md "wikilink")。公子高之孫[高傒助](../Page/高傒.md "wikilink")[齊桓公奪位](../Page/齊桓公.md "wikilink")[稱霸](../Page/稱霸.md "wikilink")。其後裔是高姓中最重要的一支，成為當代[渤海高氏的始祖](../Page/渤海.md "wikilink")。　

#### 以王父字为氏

[齐惠公的儿子](../Page/齐惠公.md "wikilink")[公子祁](../Page/公子祁.md "wikilink")，字子高，其后裔世为高氏。
　　

### 他族或他姓改高

  - 出自他族或他姓改姓。如魏时，鲜卑族有楼氏，后改高氏。
  - 十六国时，后燕皇帝慕容云自称为高阳氏后裔，遂改姓高，称高云，其后裔有改复姓为单姓，称高氏。
  - [新莽時有](../Page/新莽.md "wikilink")[巨姓人士因任](../Page/巨姓.md "wikilink")[王莽之將領](../Page/王莽.md "wikilink")，族人在漢[光武中興後避禍](../Page/光武中興.md "wikilink")，以高、巨同義，改高氏。
  - 高丽羽真氏，后有改高氏的。
  - 南北朝时，先后有元氏和徐氏改姓为高氏。北齐文宣帝姓高名洋，当时有元景安、元文遥本鲜卑族，随汉姓元，因有功于北齐，高洋赐他们"高"姓；又北齐时重臣，高隆之，本姓徐，因其父与高欢交厚，遂改为高氏。
  - 以"高"字开头的两个字的复姓，后有改单姓"高"为氏。譬如：高车氏、高堂氏、高阳氏、高陵氏等。
  - [高句丽王室也是高姓](../Page/高句丽.md "wikilink")。

## 迁徙分布

### 中國大陸

高氏的发源地虽说在今河南省境内，但自春秋以后的高姓却大都出自齐鲁之地。战国或秦时，高氏已自山东迁入今河北、辽宁省境。东汉时，有渤海王太守高洪定居渤海蓨（今河北省景县）。南北朝时，有渤海郡人高欢，曾在北魏执魏政达16年之久，死后，其儿子高洋便代东魏称帝，为北齐。京兆高氏又有与北齐同祖，初居文安（今属河北），后迁徙到京兆。吴丹阳太守高瑞，初居广陵，其后迁至秣陵（今属江苏）。高氏大批南迁始于西晋末年"永嘉之乱"时。唐初，有高氏随陈政、陈元光父子入闽开辟漳州。唐僖宗时，有高钢（河南高氏）因避"黄巢之乱"南迁入闽，为高姓入闽后的始祖。在汉末时，又有一支高氏由陇西（今属甘肃）南迁至滇中（今属云南），后成为白蛮大姓。宋绍圣元年间，高升泰夺取大理段氏政权，自立为王，称大中国。传子高太明，于圣绍三年，又将王位还给段氏，仍封为中国公，世为大理宰相，控制大理国政权，被称为高国主。其后裔历经元、明、清数代均为云南土司。中国历史上，高姓称帝王者14人，曾建立北齐、燕、荆南等政权。

### 台灣

台湾的第三十个大姓，最早發源于中國山东。近代播迁到台湾的时间，是在康熙二十二年郑克爽归降清廷以后。依照台湾省文献会的调查统计，高姓的台湾省籍人士，大多聚居在台北一带。另外在台灣，「高」與異體字「-{髙}-」於登記時被視為不同姓氏。\[1\]

## 字輩

### （渤海原谱序）

良立朝宗、永季有本、叔公子必、汝彦士克、世继以伯、孟甫景

### （龙门谱序）

仲维哲德、允守孝敬、道在纯诚、体仁居信、尊孔宪文、用中崇正、善则孚之、民皆顺应、天其显思、锡尔福庆

### （民国十八年新增谱序）

国步昌明、家声振起、逢时翼运、俊杰济美、兴学毓才、贻谋恒久、培元定基、存心笃厚、如日东升、同登尚寿、传万年代、育英贤

### 明末迄今

另一支已排出60世，其中前20世已用過，排字如下：
鵬育岐安恆、天庭震九宗、清典宇體治、百世品仙成、聰明睿知臨、寬裕溫柔容、發強剛毅執、齊莊中正敬、忠孝傳家寶、內聖基業宏、仁義布德宗、外王至善功。

### 安平高氏(泉州譜序)

鋼囦木美(號土)。鎰惠桂善佖。稺溥章鳴賢。閭山廣卿吕。保佛積弘子。 文欽淳植炅。培鍾派標烶。墀銘泉樹炳。堂錦洙機煊。增鏇源本煥。

### 高氏鲁西南地方分支

  - 宝腾观潭壮　扶荷巩以良 亭晓锡正典 环海佩凌骧。 新议二十字：仁显家声远 义存衍继长 道广文章盛 德厚恒世昌

## 台灣 高姓宗族

  - 明末高姓先民從[萬華地區沿](../Page/萬華地區.md "wikilink")[淡水河流域](../Page/淡水河流域.md "wikilink")，經
    台北市[公館](../Page/公館_\(臺北市\).md "wikilink")、[景美](../Page/景美.md "wikilink")、[新店](../Page/新店區.md "wikilink")、[木柵](../Page/木柵.md "wikilink")、[深坑](../Page/深坑區.md "wikilink")、[石碇開墾而上](../Page/石碇區.md "wikilink")。因此大部分台灣高姓宗族散居於這些地區。
  - [台灣原住民因戰後](../Page/台灣原住民.md "wikilink")[國民黨政府主政初期](../Page/國民黨.md "wikilink")，因名字填寫麻煩，加以居住在高山地區，於是改姓高。
  - [學海書院高姓在台灣的](../Page/學海書院.md "wikilink")[祠堂](../Page/祠堂.md "wikilink")。
  - [草山春暉述敘高銘宗兄弟在台灣及](../Page/草山春暉.md "wikilink")[西雅圖奮鬥的故事](../Page/西雅圖.md "wikilink")。

## 堂号

  - [厚余堂](../Page/厚余堂.md "wikilink")：孔子弟子[高柴](../Page/高柴.md "wikilink")，做费城宰（今之县长）。孔子评他："柴也愚"。[朱熹注](../Page/朱熹.md "wikilink")"愚是知不足而后知有余"。《词海》：愚，纯朴也。
  - [渤海堂](../Page/渤海堂.md "wikilink")：唐朝时[高固](../Page/高固.md "wikilink")、[高崇文都被封为渤海郡王](../Page/高崇文.md "wikilink")；北齐高欢被封为渤海王。
  - [漁陽堂](../Page/漁陽堂.md "wikilink")：
  - [遼東堂](../Page/遼東堂.md "wikilink")：
  - [廣陵堂](../Page/廣陵堂.md "wikilink")：
  - [河南堂](../Page/河南堂.md "wikilink")：
  - [有繼堂](../Page/有繼堂.md "wikilink")：相傳大儒[朱熹曾贈聯日](../Page/朱熹.md "wikilink")：“後周忠節第，有宋尚書門。”為題堂名日“有繼堂”。\[2\]
  - [供侯堂](../Page/供侯堂.md "wikilink")：

## 歷史名人

### [中國](../Page/中國.md "wikilink")

#### [史前時期](../Page/史前時期.md "wikilink")

  - [高元](../Page/高元.md "wikilink")，發明房屋的始祖

#### [春秋時期](../Page/春秋時期.md "wikilink")

  - [高傒](../Page/高傒.md "wikilink")，[公子高之孙](../Page/公子高.md "wikilink")，协助[齐桓公即位](../Page/齐桓公.md "wikilink")，[高氏
    (二守)](../Page/高氏_\(二守\).md "wikilink")
  - [高彊 (齊國)](../Page/高彊_\(齊國\).md "wikilink")，[高氏
    (二惠)](../Page/高氏_\(二惠\).md "wikilink")
  - [高柴](../Page/高柴.md "wikilink")，[孔子](../Page/孔子.md "wikilink")72弟子之一

#### [戰國時期](../Page/戰國時期.md "wikilink")

  - [高渐离](../Page/高渐离.md "wikilink")，战国末年时燕人，擅长击筑，燕太子丹派荆轲谋刺秦王政（即秦始皇），到易水送行，他击筑，荆轲和歌。后因在筑内暗藏铅块扑击秦始皇，不中被杀。

#### [漢朝](../Page/漢朝.md "wikilink")

  - [高览](../Page/高览.md "wikilink")，
    [东汉末年](../Page/东汉.md "wikilink")[袁绍部将](../Page/袁绍.md "wikilink")，后与[张郃一同投降](../Page/张郃.md "wikilink")[曹操](../Page/曹操.md "wikilink")，官至偏将军、莱侯。
  - [高順](../Page/高順.md "wikilink")，[三國時期](../Page/三國.md "wikilink")[呂布重要將領](../Page/呂布.md "wikilink")。

#### [魏晉南北朝](../Page/魏晉南北朝.md "wikilink")

  - [高柔](../Page/高柔.md "wikilink")，曹魏官員
  - [高照容](../Page/高照容.md "wikilink")，[北魏](../Page/北魏.md "wikilink")[孝文帝的貴人](../Page/北魏孝文帝.md "wikilink")
  - [高英](../Page/高英.md "wikilink")，[北魏](../Page/北魏.md "wikilink")[宣武帝皇后](../Page/北魏宣武帝.md "wikilink")
  - [高歡](../Page/高歡.md "wikilink")，[北魏](../Page/北魏.md "wikilink")、[東魏權臣](../Page/東魏.md "wikilink")，追尊為[北齊高祖神武帝](../Page/北齊.md "wikilink")
  - [高澄](../Page/高澄.md "wikilink")，高歡子、[東魏權臣](../Page/東魏.md "wikilink")，追尊為[北齊文襄帝](../Page/北齊.md "wikilink")
  - [高洋](../Page/高洋.md "wikilink")，高歡子、[北齊文宣帝](../Page/北齊.md "wikilink")、北齊開國皇帝
  - [高長恭](../Page/高長恭.md "wikilink")，高歡孫，[北齊](../Page/北齊.md "wikilink")[蘭陵王](../Page/蘭陵王.md "wikilink")，武將

#### [隋朝](../Page/隋朝.md "wikilink")

  - [高熲](../Page/高熲.md "wikilink")，[隋朝名臣](../Page/隋朝.md "wikilink")

#### [唐朝](../Page/唐朝.md "wikilink")

  - [高適](../Page/高適.md "wikilink")，詩人
  - [高士廉](../Page/高士廉.md "wikilink")，受封申国公
  - [高仙芝](../Page/高仙芝.md "wikilink")，[高句麗族的名將](../Page/高句麗.md "wikilink")

#### [五代十國](../Page/五代十國.md "wikilink")

  - [高季興](../Page/高季興.md "wikilink")、[高從誨](../Page/高從誨.md "wikilink")、[高保融](../Page/高保融.md "wikilink")、[高保勖](../Page/高保勖.md "wikilink")、[高繼沖](../Page/高繼沖.md "wikilink")：五代十國[荊南君主](../Page/荊南.md "wikilink")

#### [宋朝](../Page/宋朝.md "wikilink")

  - [高懷德](../Page/高懷德.md "wikilink")，開國將領
  - [高应松](../Page/高应松.md "wikilink")，忠臣，不肯草降表与[蒙哥](../Page/蒙哥.md "wikilink")，绝食而卒
  - [高俅](../Page/高俅.md "wikilink")，[宋徽宗的宠臣](../Page/宋徽宗.md "wikilink")
  - [高寵](../Page/高寵.md "wikilink")，忠臣

#### [明朝](../Page/明朝.md "wikilink")

  - [高拱](../Page/高拱.md "wikilink")，[明穆宗時](../Page/明穆宗.md "wikilink")[首輔](../Page/首輔.md "wikilink")
  - [高第](../Page/高第.md "wikilink")，兵部[尚書](../Page/尚書.md "wikilink")，經略[遼東](../Page/遼東.md "wikilink")

#### [清朝](../Page/清朝.md "wikilink")

  - [高鶚](../Page/高鶚.md "wikilink")，文學家-[紅樓夢後](../Page/紅樓夢.md "wikilink")40回作者
  - [高士奇](../Page/高士奇.md "wikilink")，杭州余姚人，清朝作家，中国历史学家
  - [高斌](../Page/高斌.md "wikilink")，清朝官員，官至大學士、河道總督，清朝的治河名臣。
  - [慧賢皇貴妃](../Page/慧賢皇貴妃.md "wikilink")，高斌長女，乾隆皇帝側福晉，後進封皇貴妃。

### [朝鮮半島](../Page/朝鮮半島.md "wikilink")

#### [高句丽](../Page/高句丽.md "wikilink")

  - [高朱蒙](../Page/高朱蒙.md "wikilink")，高句麗的開國君主
  - [高無恤](../Page/高無恤.md "wikilink")，高句麗第三代國王，即大武神王
  - [高谈德](../Page/高谈德.md "wikilink")，[高句丽](../Page/高句丽.md "wikilink")[好太王](../Page/好太王.md "wikilink")
  - [高巨连](../Page/高巨连.md "wikilink")，[高句丽](../Page/高句丽.md "wikilink")[长寿王](../Page/长寿王.md "wikilink")

## 現代名人

### 政界

  - [高承元](../Page/高承元.md "wikilink")，1926年曾任[國民政府](../Page/國民政府.md "wikilink")[外交部部長](../Page/外交部.md "wikilink")。
  - [高玉樹](../Page/高玉樹.md "wikilink")，前[臺北市市長](../Page/臺北市市長.md "wikilink")、[交通部部長及](../Page/中華民國交通部.md "wikilink")[總統府資政](../Page/總統府資政.md "wikilink")。
  - [高孔廉](../Page/高孔廉.md "wikilink")，[海峽交流基金會副董事長兼秘書長](../Page/海峽交流基金會.md "wikilink")。
  - [高志鵬](../Page/高志鵬.md "wikilink")，[民主進步黨籍](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。
  - [高建智](../Page/高建智.md "wikilink")，民主進步黨籍立法委員。
  - [高思博](../Page/高思博.md "wikilink")，中華民國[行政院政務委員](../Page/行政院.md "wikilink")。
  - [高基讚](../Page/高基讚.md "wikilink")，[臺中縣議會](../Page/臺中縣議會.md "wikilink")[台聯黨籍縣議員](../Page/台聯黨.md "wikilink")。
  - [高金素梅](../Page/高金素梅.md "wikilink")，[無黨團結聯盟籍](../Page/無黨團結聯盟.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

### 商界

  - [高清愿](../Page/高清愿.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[統一企業董事長](../Page/統一企業.md "wikilink")。

### 學界

  - [高錕](../Page/高錕.md "wikilink")，[英](../Page/英國.md "wikilink")、[美籍華裔物理學家](../Page/美國.md "wikilink")、教育家，[光纖通訊理論主要奠基者](../Page/光纖通訊.md "wikilink")，[香港中文大學第三任校長](../Page/香港中文大學.md "wikilink")，2009年[諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")。
  - [高行健](../Page/高行健.md "wikilink")，首位獲得[諾貝爾文學獎的中文作家](../Page/諾貝爾文學獎.md "wikilink")。
  - [高士其](../Page/高士其.md "wikilink")，科学家
  - [辛灝年](../Page/辛灝年.md "wikilink")（本名高爾品），著名歷史學者

### 體育界

  - [高英傑](../Page/高英傑.md "wikilink")，中華民國棒球代表隊前總教練、現為[臺北市立大學教練](../Page/臺北市立大學.md "wikilink")。
  - [高志綱](../Page/高志綱.md "wikilink")，台灣[中華職棒大聯盟棒球選手](../Page/中華職棒大聯盟.md "wikilink")，目前效力於[統一7-ELEVEn獅隊](../Page/統一7-ELEVEn獅.md "wikilink")。
  - [高國慶](../Page/高國慶.md "wikilink")，台灣中華職棒大聯盟棒球選手，目前效力於統一7-ELEVEn獅隊。

### 軍人

  - [高志航](../Page/高志航.md "wikilink")，[中華民國空軍將領](../Page/中華民國空軍.md "wikilink")，抗日英雄。
  - [高魁元](../Page/高魁元.md "wikilink")，[中華民國前](../Page/中華民國.md "wikilink")[國防部部長](../Page/中華民國國防部.md "wikilink")，現為[中華民國總統府戰略顧問](../Page/中華民國總統府戰略顧問.md "wikilink")。
  - [高華柱](../Page/高華柱.md "wikilink")，中華民國前[行政院國軍退除役官兵輔導委員會主任委員](../Page/行政院國軍退除役官兵輔導委員會.md "wikilink")、國防部部長。

### 演藝界

  - [高慧君](../Page/高慧君.md "wikilink")，台灣著名原住民女歌手。
  - [高蕾雅](../Page/高蕾雅.md "wikilink")，台灣著名原住民女歌手。
  - [高志超](../Page/高志超.md "wikilink")，[澳門聞名旅遊家兼食家](../Page/澳門.md "wikilink")，人稱膠超。
  - [高可慧](../Page/高可慧.md "wikilink")，[香港藝員及配音員](../Page/香港.md "wikilink")。
  - [高皓正](../Page/高皓正.md "wikilink")，香港藝人。
  - [高松傑](../Page/高松傑.md "wikilink")，香港著名藝術教育學校總監。
  - [高鈞賢](../Page/高鈞賢.md "wikilink")，香港演員，知名於第一屆[香港先生選舉冠軍](../Page/香港先生.md "wikilink")。
  - [高志森](../Page/高志森.md "wikilink")，香港舞台劇資深導演及演員。
  - [高以翔](../Page/高以翔.md "wikilink")，台灣演藝人。
  - [高以愛](../Page/高以愛.md "wikilink")，著名華語流行音樂女歌手，具台灣和美國血統。
  - [高宇蓁](../Page/高宇蓁.md "wikilink")，台灣知名本土劇女演員。
  - [高亚麟](../Page/高亚麟.md "wikilink")，中国电视演员。
  - [高晓松](../Page/高晓松.md "wikilink")，中国著名音乐制作人，电影导演，脱口秀节目主讲人，现任阿里巴巴集团旗下阿里音乐董事长。
  - [高志溶](../Page/高志溶.md "wikilink")，韓國偶像團體[水晶男孩之成員](../Page/水晶男孩.md "wikilink")，目前為企業家。
  - [高甫潔](../Page/高甫潔.md "wikilink")，韓國女演員。

### 醫藥界

  - [高永文](../Page/高永文.md "wikilink")，[香港](../Page/香港.md "wikilink")[醫生](../Page/醫生.md "wikilink")，曾任香港政府[食物及衛生局局長](../Page/食物及衛生局局長.md "wikilink")。

### 媒體界

  - [高芳婷](../Page/高芳婷.md "wikilink")，香港[無線新聞附屬新聞台主播](../Page/無線新聞.md "wikilink")。

## 地名

## 参考文献

<references/>

## 外部連結

  - [高氏家谱网](http://www.gsjp.net/)

{{-}}

[ko:고 (성씨)](../Page/ko:고_\(성씨\).md "wikilink")

[G高](../Category/漢字姓氏.md "wikilink") [高姓](../Category/高姓.md "wikilink")

1.  [1](https://news.housefun.com.tw/news/article/181885191319.html)
2.  [安海高氏宗祠](http://kaostaiwan.pixnet.net/blog/post/32077022-%E5%AE%89%E6%B5%B7%E9%AB%98%E6%B0%8F%E5%AE%97%E7%A5%A0)