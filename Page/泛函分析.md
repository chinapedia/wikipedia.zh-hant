[Drum_vibration_mode12.gif](https://zh.wikipedia.org/wiki/File:Drum_vibration_mode12.gif "fig:Drum_vibration_mode12.gif")

**泛函分析**（）是现代[数学分析的一个分支](../Page/数学分析.md "wikilink")，隶属于[分析学](../Page/分析学.md "wikilink")，其研究的主要对象是[函数构成的](../Page/函数.md "wikilink")[函数空间](../Page/函数空间.md "wikilink")。泛函分析历史根源是由对函数空间的研究和对函数的变换（如[傅立叶变换等](../Page/傅立叶变换.md "wikilink")）的性质的研究。这种观点被证明是对[微分方程和](../Page/微分方程.md "wikilink")[积分方程的研究中特别有用](../Page/积分方程.md "wikilink")。

使用[泛函这个词作为表述源自](../Page/泛函.md "wikilink")[变分法](../Page/变分法.md "wikilink")，代表作用于函数的函数，这意味着，一个函数的参数是函数。这个名词首次被[雅克·阿达马在](../Page/雅克·阿达马.md "wikilink")1910年使用于这个课题的书中。是泛函分析理论的主要奠基人之一。然而，泛函的一般概念以前曾在1887年是由意大利数学家和物理学家[維多·沃爾泰拉](../Page/維多·沃爾泰拉.md "wikilink")（Vito
Volterra）介绍。非线性泛函理论是由[雅克·阿达马的学生继续研究](../Page/雅克·阿达马.md "wikilink")，特别是[莫里斯·弗雷歇](../Page/莫里斯·弗雷歇.md "wikilink")（Maurice
Fréchet）可和列维（Levy）。[雅克·阿达马还创立线性泛函分析的现代流派](../Page/雅克·阿达马.md "wikilink")，并由[弗里杰什·里斯和一批围绕着](../Page/弗里杰什·里斯.md "wikilink")[斯特凡·巴拿赫](../Page/斯特凡·巴拿赫.md "wikilink")（Stefan
Banach）的[波兰数学家](../Page/波兰.md "wikilink")进一步发展。

## 赋范线性空间

从现代观点来看，泛函分析研究的主要是[实数域或](../Page/实数域.md "wikilink")[复数域上的](../Page/复数域.md "wikilink")[完备](../Page/完备.md "wikilink")[赋范线性空间](../Page/赋范线性空间.md "wikilink")。这类空间被称为[巴拿赫空间](../Page/巴拿赫空间.md "wikilink")，巴拿赫空间中最重要的特例被称为[希尔伯特空间](../Page/希尔伯特空间.md "wikilink")，其上的[范数由一个](../Page/范数.md "wikilink")[内积导出](../Page/内积.md "wikilink")。这类空间是[量子力学数学描述的基础](../Page/量子力学.md "wikilink")。更一般的泛函分析也研究[Fréchet空间和](../Page/Fréchet空间.md "wikilink")[拓扑向量空间等没有定义范数的空间](../Page/拓扑向量空间.md "wikilink")。

泛函分析所研究的一个重要对象是巴拿赫空间和希尔伯特空间上的[连续](../Page/连续.md "wikilink")[线性算子](../Page/线性算子.md "wikilink")。这类算子可以导出[C\*-代数和其他](../Page/C*-代数.md "wikilink")[算子代数的基本概念](../Page/算子代数.md "wikilink")。

### 希尔伯特空间

希尔伯特空间（*Hilbert*）可以利用以下结论完全分类，即对于任意两个希尔伯特空间，若其[基的](../Page/基.md "wikilink")[基数相等](../Page/基数.md "wikilink")，则它们必彼此[同构](../Page/同构.md "wikilink")。对于[有限维希尔伯特空间而言](../Page/有限维.md "wikilink")，其上的[连续](../Page/连续.md "wikilink")[线性算子即是](../Page/线性算子.md "wikilink")[线性代数中所研究的](../Page/线性代数.md "wikilink")[线性变换](../Page/线性变换.md "wikilink")。对于[无穷维希尔伯特空间而言](../Page/无穷维.md "wikilink")，其上的任何[态射均可以分解为](../Page/态射.md "wikilink")[可数](../Page/可数.md "wikilink")[维度](../Page/维度.md "wikilink")（基的基数为\(\aleph_0\)）上的[态射](../Page/态射.md "wikilink")，所以泛函分析主要研究[可数维度上的希尔伯特空间及其态射](../Page/可数.md "wikilink")。希尔伯特空间中的一个尚未完全解决的问题是，是否对于每个希尔伯特空间上的[算子](../Page/算子.md "wikilink")，都存在一个真[不变子空间](../Page/不变子空间.md "wikilink")。该问题在某些特定情况下的答案是肯定的。

### 巴拿赫空间

一般的巴拿赫空间（*Banach*）比较复杂，例如没有通用的办法构造其上的一组基。

对于每个实数\(p\)，如果\(p \ge 1\)，一个巴拿赫空间的例子是“所有[绝对值的](../Page/绝对值.md "wikilink")\(p\)次方的[积分收敛的](../Page/积分.md "wikilink")[勒贝格可测函数](../Page/勒贝格可测.md "wikilink")”所构成的空间。（参看[Lp空间](../Page/Lp空间.md "wikilink")）

在巴拿赫空间中，相当部分的研究涉及到[对偶空间的概念](../Page/对偶空间.md "wikilink")，即巴拿赫空间上所有[连续](../Page/连续.md "wikilink")[线性泛函所构成的空间](../Page/线性泛函.md "wikilink")。[对偶空间的](../Page/对偶空间.md "wikilink")[对偶空间可能与原空间并不](../Page/对偶空间.md "wikilink")[同构](../Page/同构.md "wikilink")，但总可以构造一个从巴拿赫空间到其[对偶空间的](../Page/对偶空间.md "wikilink")[对偶空间的一个](../Page/对偶空间.md "wikilink")[单同态](../Page/单同态.md "wikilink")。

[微分的概念可以在巴拿赫空间中得到推广](../Page/微分.md "wikilink")，[微分](../Page/微分.md "wikilink")[算子作用于其上的所有函数](../Page/算子.md "wikilink")，一个函数在给定点的微分是一个[连续](../Page/连续.md "wikilink")[线性映射](../Page/线性映射.md "wikilink")。

## 主要结果和定理

泛函分析的主要定理包括：

  - [一致有界定理](../Page/一致有界定理.md "wikilink")（亦称[共鸣定理](../Page/共鸣定理.md "wikilink")），该定理描述一族[有界算子的性质](../Page/有界算子.md "wikilink")。
  - [谱定理包括一系列结果](../Page/谱定理.md "wikilink")，其中最常用的结果给出了希尔伯特空间上[正规算子的一个积分表达](../Page/正规算子.md "wikilink")，该结果在[量子力学的数学描述中起到了核心作用](../Page/量子力学.md "wikilink")。
  - [哈恩-巴拿赫定理](../Page/哈恩-巴拿赫定理.md "wikilink")（Hahn-Banach
    Theorem）研究了如何将一个[算子保](../Page/算子.md "wikilink")[范数地从一个子空间](../Page/范数.md "wikilink")[延拓到整个空间](../Page/延拓.md "wikilink")。另一个相关结果是[对偶空间的非平凡性](../Page/对偶空间.md "wikilink")。
  - [开映射定理和](../Page/开映射定理.md "wikilink")[闭图像定理](../Page/闭图像定理.md "wikilink")。

## 泛函分析与选择公理

泛函分析所研究的大部分[空间都是](../Page/空间.md "wikilink")[无穷维的](../Page/无穷维.md "wikilink")。为了证明[无穷维](../Page/无穷维.md "wikilink")[向量空间存在一组基](../Page/向量空间.md "wikilink")，必须要使用[佐恩引理](../Page/佐恩引理.md "wikilink")（Zorn's
Lemma）。此外，泛函分析中大部分重要定理都构建于[哈恩-巴拿赫定理的基础之上](../Page/哈恩-巴拿赫定理.md "wikilink")，而该定理本身就是[选择公理](../Page/选择公理.md "wikilink")（Axiom
of Choice）弱于[布尔素理想定理](../Page/布尔素理想定理.md "wikilink")（Boolean prime ideal
theorem）的一个形式。

## 泛函分析的研究现状

泛函分析目前包括以下分支：

  - [软分析](../Page/软分析.md "wikilink")（soft
    analysis），其目标是将数学分析用[拓扑群](../Page/拓扑群.md "wikilink")、[拓扑环和](../Page/拓扑环.md "wikilink")[拓扑向量空间的语言表述](../Page/拓扑向量空间.md "wikilink")。
  - [巴拿赫空间的](../Page/巴拿赫空间.md "wikilink")[几何结构](../Page/几何结构.md "wikilink")，以Jean
    Bourgain的一系列工作为代表。
  - [非交换几何](../Page/非交换几何.md "wikilink")，此方向的主要贡献者包括Alain
    Connes，其部分工作是以George
    Mackey的[遍历论中的结果为基础的](../Page/遍历论.md "wikilink")。
  - 与[量子力学相关的理论](../Page/量子力学.md "wikilink")，狭义上被称为[数学物理](../Page/数学物理.md "wikilink")，从更广义的角度来看，如按照Israel
    Gelfand所述，其包含[表示论的大部分类型的问题](../Page/表示论.md "wikilink")。

## 相關主題

  - [调和分析](../Page/调和分析.md "wikilink")
  - [逼近理論](../Page/逼近理論.md "wikilink")
  - [分析](../Page/分析.md "wikilink")
  - [微分幾何及](../Page/微分幾何.md "wikilink")[拓撲](../Page/拓撲.md "wikilink")
  - [代數拓撲](../Page/代數拓撲.md "wikilink")
  - [代數幾何](../Page/代數幾何.md "wikilink")
  - [抽象代數](../Page/抽象代數.md "wikilink")

[\*](../Category/泛函分析.md "wikilink") [\*](../Category/函数.md "wikilink")