**申健**，原名**申振民**，[中国共产党情報](../Page/中国共产党.md "wikilink")「後三傑」。[河北](../Page/河北.md "wikilink")[大城人](../Page/大城縣.md "wikilink")。
[Sen_Jian.jpg](https://zh.wikipedia.org/wiki/File:Sen_Jian.jpg "fig:Sen_Jian.jpg")

## 生平

1937年10月在[北京师范大学](../Page/北京师范大学.md "wikilink")（抗战开始后合并为[西北联大](../Page/西北联大.md "wikilink")）参加[中华民族解放先锋队](../Page/中华民族解放先锋队.md "wikilink")，1938年加入[中国共产党](../Page/中国共产党.md "wikilink")。潛伏於[胡宗南屬下](../Page/胡宗南.md "wikilink")，使胡宗南屢戰屢敗。1947年入美国[西保大学研究院](../Page/西保大学.md "wikilink")。1949年6月回国，曾任駐[印度大使館參贊](../Page/印度.md "wikilink")、外交部美澳司司長兼中國人民外交學會副會長、對外友協常務理事、中國拉丁美洲友好協會副會長、第一任[駐古巴大使](../Page/中国驻古巴大使列表.md "wikilink")\[1\]、[中共中央對外聯絡部副秘書長](../Page/中共中央對外聯絡部.md "wikilink")、副部長、中國古巴友好協會會長、[駐印度大使](../Page/中国驻印度大使列表.md "wikilink")、[中國現代國際關係研究所高級研究員兼外交學院教授](../Page/中國現代國際關係研究所.md "wikilink")、第五屆[全國政協常委](../Page/全國政協.md "wikilink")、第六屆全國政協委員。1992年逝世。

## 參見

  - [熊向晖](../Page/熊向晖.md "wikilink")
  - [陳忠經](../Page/陳忠經.md "wikilink")
  - [潛伏於中華民國國軍中的中共間諜列表](../Page/潛伏於中華民國國軍中的中共間諜列表.md "wikilink")

## 参考

{{-}}

[Category:第六屆全國政協委員](../Category/第六屆全國政協委員.md "wikilink")
[Category:第五屆全國政協委員](../Category/第五屆全國政協委員.md "wikilink")
[Category:中華人民共和國外交官](../Category/中華人民共和國外交官.md "wikilink")
[Category:中華人民共和國駐古巴大使](../Category/中華人民共和國駐古巴大使.md "wikilink")
[Category:中華人民共和國駐印度大使](../Category/中華人民共和國駐印度大使.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:潛伏於中華民國國軍中的中共間諜](../Category/潛伏於中華民國國軍中的中共間諜.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")
[Category:北京師範大學校友](../Category/北京師範大學校友.md "wikilink")
[Category:大城人](../Category/大城人.md "wikilink")
[Jian健](../Category/申姓.md "wikilink")
[Category:中华人民共和国驻古巴大使](../Category/中华人民共和国驻古巴大使.md "wikilink")
[Category:凯斯西储大学校友](../Category/凯斯西储大学校友.md "wikilink")

1.