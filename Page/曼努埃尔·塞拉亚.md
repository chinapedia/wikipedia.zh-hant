**何塞·曼努埃爾·塞拉亞·羅薩萊斯**（；），[洪都拉斯政治人物](../Page/洪都拉斯.md "wikilink")，自2006年1月27日起出任[洪都拉斯總統](../Page/洪都拉斯總統.md "wikilink")。2009年6月28日洪都拉斯軍方發動[政變](../Page/2009年宏都拉斯軍事政變.md "wikilink")，塞拉亞被軍方押送出境，最高法院隨後罷免其總統職位。

## 出任總統

2005年11月27日，塞拉亞代表[洪都拉斯自由黨在](../Page/洪都拉斯自由黨.md "wikilink")[大选中擊敗代表](../Page/2005年宏都拉斯大選.md "wikilink")[洪都拉斯國民黨的](../Page/洪都拉斯國民黨.md "wikilink")[波菲裏奧·佩佩·洛沃](../Page/波菲裏奧·佩佩·洛沃.md "wikilink")（）當選總統。

## 軍事政變

塞拉亞計劃在2009年6月28日舉行修憲公決投票，建議容許總統連任，但遭到政治派系反對，認為他藉修憲爭取連任，最高法庭裁定全民公決違法。2009年6月28日，公投前一個小時，軍方將塞拉亞帶離總統府，押送至首都近郊一座空軍基地\[1\]
，又將塞拉亞驅逐出境。[哥斯達黎加安排專機予塞拉亞](../Page/哥斯達黎加.md "wikilink")，平安抵達哥國，但並未向哥國尋求[政治庇護](../Page/政治庇護.md "wikilink")。他在哥國發表講話，表示自己被部分軍人綁架，呼籲民眾抵抗發動政變的人。

國會隨後召開緊急會議，表決接納塞拉亞辭職。最高法院亦宣布，罷免塞拉亞，並指軍方拘押及驅逐塞拉亞獲得法院授權，屬於合法行為。

2009年11月29日改選總統前，總統職務暫由國會議長[羅伯特·米契列第](../Page/羅伯特·米契列第.md "wikilink")（Roberto
Micheletti）代理\[2\]。

## 參考資料

[Category:洪都拉斯总统](../Category/洪都拉斯总统.md "wikilink")
[Category:被政變推翻的領導人](../Category/被政變推翻的領導人.md "wikilink")

1.  [洪都拉斯總統被軍方逮捕
    BBC](http://news.bbc.co.uk/chinese/trad/hi/newsid_8120000/newsid_8123100/8123186.stm)
2.