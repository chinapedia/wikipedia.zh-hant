**松三糖**（Melezitose）為一種非還原[三糖](../Page/三糖.md "wikilink")，可從數種樹的汁液中被萃取出來，如[落葉松或是](../Page/落葉松.md "wikilink")[黃杉](../Page/黃杉.md "wikilink")。松三糖可以部份被[水解成](../Page/水解.md "wikilink")[葡萄糖和松二糖](../Page/葡萄糖.md "wikilink")。（松二糖為[蔗糖的](../Page/蔗糖.md "wikilink")[同分異構體](../Page/同分異構體.md "wikilink")）

配合其他的生化檢驗方式，藉由是否利用松三糖，可辨認Klebsiella和Raoultella兩種不同屬的細菌。

## 參考文獻

  - [Alves MS, Dias RC, de Castro AC, Riley LW, Moreira BM.
    Identification of clinical isolates of indole-positive and
    indole-negative klebsiella spp. Journal of clinical
    microbiology 2006;44:3640-3646.](http://jcm.asm.org/cgi/content/full/44/10/3640?view=long&pmid=16928968)

[Category:三糖](../Category/三糖.md "wikilink")