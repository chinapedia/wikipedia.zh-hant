**.tp**為[東帝汶](../Page/东帝汶.md "wikilink")[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")（ccTLD）的舊[域名](../Page/域名.md "wikilink")，現正逐步改用**[.tl](../Page/.tl.md "wikilink")**。

## 外部連結

  - [Timor-Leste (.TL) & East Timor (.TP) ccTLD NIC](http://www.nic.tl)
  - [Legacy www.nic.tp
    website](https://web.archive.org/web/20050207154532/http://old.nic.tp/)
  - [IANA .tp whois information](http://www.iana.org/root-whois/tp.htm)
  - [IANA .tl whois information](http://www.iana.org/root-whois/tl.htm)

[sv:Toppdomän\#T](../Page/sv:Toppdomän#T.md "wikilink")

[tp](../Category/國家及地區頂級域.md "wikilink")
[Category:東帝汶](../Category/東帝汶.md "wikilink")