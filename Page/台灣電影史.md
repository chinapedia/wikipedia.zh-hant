**台灣電影史**介绍了1898年以来，[台湾电影的发展历史](../Page/台湾电影.md "wikilink")。

## 日治時期

[莎勇之鐘.jpg](https://zh.wikipedia.org/wiki/File:莎勇之鐘.jpg "fig:莎勇之鐘.jpg")》，[李香蘭主演](../Page/李香蘭.md "wikilink")\]\]
1898年，日本佛教真宗少年教會在台北北門外舉辦「少年教育映畫幻燈會」，是首次有電影在台灣播放。\[1\]

第一部在台灣拍攝的電影是1907年2月17日由日本人[高松豐次郎率領日本攝影師等一行人在全台灣北](../Page/高松豐次郎.md "wikilink")、中、南一百多處地點取鏡的《[台灣實況介紹](../Page/台灣實況介紹.md "wikilink")》。電影內容涵蓋城市建設、電力、農業、工業、礦業、鐵路、教育、風景、民俗、征討原住民等題材\[2\]。

第一部台灣人製作的劇情片《[誰之過](../Page/誰之過.md "wikilink")》於1925年由[劉喜陽](../Page/劉喜陽.md "wikilink")、[李松峰等人組成的](../Page/李松峰.md "wikilink")[台灣映畫研究會製作](../Page/台灣映畫研究會.md "wikilink")，[鄭連捷等人演出](../Page/鄭連捷.md "wikilink")\[3\]。不過即使劇情片，仍有相當多的影片具有政治宣傳的作用\[4\]。例如台日合拍的《[義人吳鳳](../Page/義人吳鳳.md "wikilink")》等宣導片。純日本的電影則以《[鞍馬天狗](../Page/鞍馬天狗.md "wikilink")》、《[忠臣藏](../Page/忠臣藏.md "wikilink")》、《[宮本武藏](../Page/宮本武藏.md "wikilink")》等武道片較受台灣人歡迎。此外，當時台灣人也喜愛中國片，當時全台灣的電影院約有四十家。

1908年，[高松豐次郎於台灣定居](../Page/高松豐次郎.md "wikilink")，開始在台灣北、中、南七大都會建戲院放映電影，並與日本及歐美的電影公司簽約，建立制度化的[電影發行](../Page/電影發行.md "wikilink")[放映制度](../Page/放映制度.md "wikilink")。1924年後，台北的放映業者由日本請來一流的[辯士](../Page/電影辯士.md "wikilink")（電影說明者），[電影放映業愈加蓬勃起來](../Page/電影放映業.md "wikilink")。

1935年10月，日本領台40年舉行[台灣博覽會](../Page/始政四十週年紀念臺灣博覽會.md "wikilink")，以及隔年台北與福岡間開闢航空通運，這兩件事推化造就了日治時期台灣電影放映業的鼎盛時期\[5\]

1938年，第一映畫製作所[吳錫洋完成的](../Page/吳錫洋.md "wikilink")《[望春風](../Page/望春風.md "wikilink")》，口碑賣座俱佳，算是殖民地時代最具代表性的台灣電影了。\[6\]

## 戰後

[國民政府來台初期](../Page/國民政府.md "wikilink")，大部分電影都是由政府主導的的政治、政令宣傳片。例如[公賣局投資拍抓私菸私酒的](../Page/臺灣菸酒公司.md "wikilink")《[良心與罪惡](../Page/良心與罪惡.md "wikilink")》、[農教公司推出的](../Page/農業教育電影公司.md "wikilink")《[美麗寶島](../Page/美麗寶島.md "wikilink")》等。

1949年，由[張徹主導的](../Page/張徹.md "wikilink")《[阿里山風雲](../Page/阿里山風雲.md "wikilink")》是是台灣脫離日本人的統治之後，第一部由中國人拍攝的電影。這是大導演[張徹在台灣所拍攝的第一支電影](../Page/張徹.md "wikilink")，當時張徹才26歲。當年，國民黨政府剛敗退到台灣，對於電影拍攝工作，並未十分熱衷。《阿里山風雲》是一部爲政治服務的宣傳電影，[藝術效果不高](../Page/藝術.md "wikilink")。但是，這部電影的插曲「[高山青](../Page/高山青.md "wikilink")」，卻廣為人知。

1954年[農教公司與](../Page/農業教育電影公司.md "wikilink")[台影合併成立中影](../Page/台影.md "wikilink")，利用[美援買了較好的拍片設備](../Page/美援.md "wikilink")，同時，許多電影人也自[香港來台](../Page/香港.md "wikilink")，[台灣國語電影事業](../Page/台灣.md "wikilink")，開始有了基礎。《[梅崗春回](../Page/梅崗春回.md "wikilink")》是[中影成立後所拍攝的第一部電影](../Page/中影股份有限公司.md "wikilink")，是部反共片，而後出品的十餘短片，也是以[反共國策與](../Page/反共.md "wikilink")[農村經濟成功為主題](../Page/農村.md "wikilink")。\[7\]。

## 臺語電影興起

1955年，[麥寮](../Page/麥寮鄉.md "wikilink")[拱樂社](../Page/拱樂社.md "wikilink")[歌仔戲團團主](../Page/歌仔戲.md "wikilink")[陳澄三與](../Page/陳澄三.md "wikilink")[何基明導演合作](../Page/何基明.md "wikilink")，拍攝該團的拿手戲《[薛平貴與王寶釧](../Page/薛平貴與王寶釧（1956年）.md "wikilink")》，成為[二戰後第一齣](../Page/二戰.md "wikilink")[臺語片](../Page/台語電影.md "wikilink")，一直到1981年最後一齣臺語片《[陳三五娘](../Page/陳三五娘.md "wikilink")》為止，歷經了二十多年的[台語電影時代](../Page/台語電影.md "wikilink")。根據統計，臺語片的總產量將近兩千部，產量最多時一年高達120齣作品問世。《[薛平貴與王寶釧](../Page/薛平貴與王寶釧（1956年）.md "wikilink")》在當時臺灣社會引起大轟動，皆創下當時票房紀錄，\[8\]，引來一窩蜂的[臺語](../Page/臺語.md "wikilink")[歌仔戲跟拍風](../Page/歌仔戲.md "wikilink")。

## [中央電影公司興起](../Page/中央電影公司.md "wikilink") (1960年代)

  - 1960年代，[越戰與](../Page/越戰.md "wikilink")[冷戰期間](../Page/冷戰.md "wikilink")，[中央電影公司提出健康寫實的製作路線](../Page/中央電影公司.md "wikilink")，兼顧歐美[寫實主義電影下](../Page/寫實主義.md "wikilink")，聘請[李行拍攝](../Page/李行.md "wikilink")《[蚵女](../Page/蚵女.md "wikilink")》與《[養鴨人家](../Page/養鴨人家.md "wikilink")》兩片。白景瑞拍攝《[家在台北](../Page/家在台北.md "wikilink")》，均頗叫好叫座。\[9\]。
  - 60年代末期，香港知名導演[李翰祥來台發展](../Page/李翰祥.md "wikilink")，並香港電影技術帶進台灣。此階段所拍電影類型除了初期的[黃梅調電影](../Page/黃梅調電影.md "wikilink")，尚有鄉土寫實片[冬暖](../Page/冬暖.md "wikilink")，歷史[宮闈片如](../Page/宮闈片.md "wikilink")《[西施](../Page/西施.md "wikilink")》及文藝片，其中以[瓊瑤原著為藍圖者如](../Page/瓊瑤.md "wikilink")[煙雨濛濛](../Page/煙雨濛濛.md "wikilink")、[女蘿草](../Page/女蘿草.md "wikilink")、[菟絲花等佔了](../Page/菟絲花.md "wikilink")8部。

## 新武俠功夫片 (1970年代)

  - 1960年代，香港[邵氏公司開始製作新派的武俠片](../Page/邵氏公司.md "wikilink")，其中的兩位主要導演[胡金銓與](../Page/胡金銓.md "wikilink")[張徹後來都來台灣發展](../Page/張徹.md "wikilink")。胡金銓於1967年為聯邦電影公司編導武俠片《[龍門客棧](../Page/龍門客棧.md "wikilink")》締造了絕佳的票房紀錄\[10\]，從此[武俠](../Page/武侠文化.md "wikilink")、[功夫](../Page/功夫.md "wikilink")、[武打類型的電影成為台灣電影的主流之一](../Page/武打.md "wikilink")，直到80年代才沒落。胡金銓的電影發揮中國[京劇的特色](../Page/京劇.md "wikilink")，結合[彈簧床與吊](../Page/彈簧床.md "wikilink")[鋼絲的特技](../Page/鋼絲.md "wikilink")，運用[蒙太奇電影手法](../Page/蒙太奇.md "wikilink")，使得武打動作快速俐落，動靜收放形成視覺韻律。
  - 功夫片興起：1971年，香港[嘉禾公司推出](../Page/橙天嘉禾.md "wikilink")[李小龍主演的功夫片](../Page/李小龍.md "wikilink")《[唐山大兄](../Page/唐山大兄.md "wikilink")》及次年的《[精武門](../Page/精武門_\(電影\).md "wikilink")》均在台掀起一股功夫熱潮，而台灣武俠主流自此就轉化為功夫片、武打片繼續在台灣流行。接著[張徹在](../Page/張徹.md "wikilink")[卲氏公司幕後支持下](../Page/卲氏.md "wikilink")，於1974年率其子弟兵[姜大衛](../Page/姜大衛.md "wikilink")、[狄龍等來台成立的](../Page/狄龍.md "wikilink")[長弓公司](../Page/長弓.md "wikilink")，也讓功夫武打片，成為當時台灣電影的要角。

## 愛國政宣電影

  - 愛國政宣電影：1970年代中期之後，因中華民國退出聯合國等種種不利因素，開始了一段台灣電影的愛國政宣片時期，以穩定民心，如《[英烈千秋](../Page/英烈千秋.md "wikilink")》、《[大摩天嶺](../Page/大摩天嶺.md "wikilink")》、《[戰地英豪](../Page/戰地英豪.md "wikilink")》、《[女兵日記](../Page/女兵日記.md "wikilink")》、《[八百壯士](../Page/八百壯士.md "wikilink")》、《[梅花](../Page/梅花_\(電影\).md "wikilink")》、《[筧橋英烈傳](../Page/筧橋英烈傳.md "wikilink")》、《[望春風](../Page/望春風.md "wikilink")》、《[香火](../Page/香火_\(電影\).md "wikilink")》、《[黃埔軍魂](../Page/黃埔軍魂.md "wikilink")》、《[成功嶺上](../Page/成功嶺上_\(1979年電影\).md "wikilink")》、《[源](../Page/源.md "wikilink")》、《[皇天后土](../Page/皇天后土.md "wikilink")》、《[假如我是真的](../Page/假如我是真的_\(電影\).md "wikilink")》、《[辛亥雙十](../Page/辛亥雙十.md "wikilink")》、《[血戰大二膽](../Page/血戰大二膽.md "wikilink")》等。近年較新的愛國政宣片為2011年由中華民國國防部與三立電視合作拍攝的紀念中華民國建國百年軍事偶像劇《[勇士們](../Page/勇士們_\(電視劇\).md "wikilink")》。除此，較特殊的片種則有如《[汪洋中的一條船](../Page/汪洋中的一條船_\(電影\).md "wikilink")》的自我勉勵寫實片及如《[一個女工的故事](../Page/一個女工的故事.md "wikilink")》的文藝片。

## 兒少電影類型

  - 兒童電影：在新浪潮電影前台灣在[國際影展最成功的一個區塊](../Page/國際影展.md "wikilink")，[中影在農教時代](../Page/中影股份有限公司.md "wikilink")[進口](../Page/進口.md "wikilink")600部美國[兒童教育短片](../Page/兒童.md "wikilink")，每週日在國際戲院放映，以極低價招待學童，可見其台灣電影傳統。從早期的[張英導演到新電影時期的](../Page/張英_\(導演\).md "wikilink")[楊立國](../Page/楊立國.md "wikilink")、[柯一正導演屢屢在區域及國際影展獲獎](../Page/柯一正.md "wikilink")，如[亞洲影展以](../Page/亞洲影展.md "wikilink")[張小燕主演的](../Page/張小燕_\(臺灣\).md "wikilink")《[苦女尋親記](../Page/苦女尋親記.md "wikilink")》，得最佳童星特別獎，[李嘉導演的](../Page/李嘉_\(台灣導演\).md "wikilink")《[我女若蘭](../Page/我女若蘭.md "wikilink")》，在[伊朗](../Page/伊朗.md "wikilink")[第一屆國際兒童電影節](../Page/第一屆國際兒童電影節.md "wikilink")，第十五屆亞洲影展及[金馬獎得了一大堆獎](../Page/金馬獎.md "wikilink")。[李行導演](../Page/李行.md "wikilink")《[婉君表妹](../Page/婉君表妹.md "wikilink")》、《[汪洋中一條船](../Page/汪洋中一條船.md "wikilink")》也在亞展得獎，到了1979年[哥倫比亞舉辦](../Page/哥倫比亞.md "wikilink")「第一屆國際兒童影展」，在21國28部片的競爭下，台灣兒童片《[鄉野奇談](../Page/鄉野奇談.md "wikilink")》獲最佳影片首獎，男女主角黃一龍、[劉煥幗獲最佳演員獎等](../Page/劉煥幗.md "wikilink")；早期的[台語電影也有很多佳作](../Page/台語電影.md "wikilink")，如木偶電影《[豬八戒招親](../Page/豬八戒招親.md "wikilink")》甚至在五零年代就成功賣出[美國](../Page/美國.md "wikilink")[版權](../Page/版權.md "wikilink")，另外還有《[八歲小偵探](../Page/八歲小偵探.md "wikilink")》、《[罪海孤兒淚](../Page/罪海孤兒淚.md "wikilink")》、《[玻璃眼球](../Page/玻璃眼球.md "wikilink")》、《[紅葉師生情](../Page/紅葉師生情.md "wikilink")》等等。《[五子哭墓](../Page/五子哭墓.md "wikilink")》、《[淚的小花](../Page/淚的小花.md "wikilink")》、《[桃太郎](../Page/桃太郎.md "wikilink")》等台語兒童電影也曾經翻拍成[國語電影](../Page/國語.md "wikilink")。所以兒童電影是台灣電影史不得不說的一個重要分類。
  - 學生電影：之後，[李行](../Page/李行.md "wikilink")、[宋存壽](../Page/宋存壽.md "wikilink")、[屠忠訓等導演拍了一些如](../Page/屠忠訓.md "wikilink")《[小城故事](../Page/小城故事.md "wikilink")》、《[早安台北](../Page/早安台北.md "wikilink")》、《[候鳥之愛](../Page/候鳥之愛.md "wikilink")》、《[歡顏](../Page/歡顏.md "wikilink")》等清新的小品。之後，導演[林清介則在](../Page/林清介.md "wikilink")《[一個問題學生](../Page/一個問題學生.md "wikilink")》賣座後，連續拍攝相同類型，以學生生活為主題的[學生電影](../Page/學生電影.md "wikilink")，這些類型電影包括《[學生之愛](../Page/學生之愛.md "wikilink")》、《[同班同學](../Page/同班同學_\(電影\).md "wikilink")》、《[男女合班](../Page/男女合班.md "wikilink")》、《[台北甜心](../Page/台北甜心.md "wikilink")》、《[畢業班](../Page/畢業班.md "wikilink")》等。而這波電影尚有《[拒絕聯考的小子](../Page/拒絕聯考的小子.md "wikilink")》、《[年輕人的心聲](../Page/年輕人的心聲.md "wikilink")》、《[不妥協的一代](../Page/不妥協的一代.md "wikilink")》。此階段學生電影成為[台灣新浪潮電影出現以前的重要台灣電影類型](../Page/台灣新浪潮電影.md "wikilink")。這些低成本獨立製作方式，也讓新導演有了拍片的機會。事實上，台灣知名導演[侯孝賢也在此階段接拍了](../Page/侯孝賢.md "wikilink")《[就是溜溜的她](../Page/就是溜溜的她.md "wikilink")》、《[風兒踢踏踩](../Page/風兒踢踏踩.md "wikilink")》等兩部片子。

## 台灣新浪潮電影時期 (1980年代初期)

1980年代，新生代電影工作者及電影導演發起了電影改革運動。電影主要呈現寫實風格，其題材貼近現實社會，回顧民眾的真實生活，由於形式新穎、風格獨特，促成了台灣電影的新風貌。
1982年，中央電影公司在[楊德昌](../Page/楊德昌.md "wikilink")、[柯一正和](../Page/柯一正.md "wikilink")[張毅等三位導演參與](../Page/張毅_\(導演\).md "wikilink")，共同合作構想小成本電影的拍攝，再經由[明驥及小野](../Page/明驥.md "wikilink")，拍成四段式集錦電影《[光陰的故事](../Page/光陰的故事\(台灣電影\).md "wikilink")》。本片解析社會真實現象，並關懷大眾現實生活和共同記憶，普遍被認為是台灣新浪潮電影的首部作品\[11\]。該片的創作者均成為後來新電影的重要成員，影片的自然寫實風格與文學表現特質，象徵了「新電影」與「舊電影」之間的差別（盧非易，1998）。
[中影公司大膽啟用新人拍攝](../Page/中影股份有限公司.md "wikilink")[鄉土文學作品](../Page/鄉土文學.md "wikilink")，也是確立台灣電影新浪潮的主因。中影啟用楊德昌、[柯一正](../Page/柯一正.md "wikilink")、[張毅](../Page/張毅_\(導演\).md "wikilink")、[陶德辰拍攝了](../Page/陶德辰.md "wikilink")《[光陰的故事](../Page/光陰的故事\(台灣電影\).md "wikilink")》，《[在那河畔青草青](../Page/在那河畔青草青.md "wikilink")》與《[小畢的故事](../Page/小畢的故事_\(電影\).md "wikilink")》。

另一部具有「現代主義」的原著改編電影——《[兒子的大玩偶](../Page/兒子的大玩偶_\(電影\).md "wikilink")》，突破當時台灣政治與電影保守勢力的抵制，於輿論、口碑與市場的支持下，為往後台灣電影的創作自由創造一片天空。其後，包括侯孝賢、楊德昌、張毅、[萬仁](../Page/萬仁.md "wikilink")、柯一正、[陳坤厚](../Page/陳坤厚.md "wikilink")、曾壯祥、李祐寧、[王童](../Page/王童.md "wikilink")、[虞戡平等也確定了以導演為主](../Page/虞戡平.md "wikilink")；形式新穎、風格獨特、意識前進的台灣新電影。這些電影也促成了台灣電影的新風貌，更因票房賣座，讓台灣新電影成為主流\[12\]。
一般來說，此波新浪潮電影中較重要的作品計有《[搭錯車](../Page/搭錯車_\(電影\).md "wikilink")》、《[風櫃來的人](../Page/風櫃來的人.md "wikilink")》、《[海灘的一天](../Page/海灘的一天.md "wikilink")》、《[看海的日子](../Page/看海的日子_\(電影\).md "wikilink")》、《[老莫的第二個春天](../Page/老莫的第二個春天.md "wikilink")》、《[玉卿嫂](../Page/玉卿嫂_\(電影\).md "wikilink")》、《[油麻菜籽](../Page/油麻菜籽_\(電影\).md "wikilink")》、《[童年往事](../Page/童年往事.md "wikilink")》、《[我這樣過了一生](../Page/我這樣過了一生.md "wikilink")》、《[青梅竹馬](../Page/青梅竹馬_\(電影\).md "wikilink")》、《[殺夫](../Page/殺夫_\(電影\).md "wikilink")》、《[恐怖份子](../Page/恐怖份子_\(電影\).md "wikilink")》、《[戀戀風塵](../Page/戀戀風塵.md "wikilink")》、《[桂花巷](../Page/桂花巷_\(電影\).md "wikilink")》。
該新電影絕大多數是由台灣政府所屬的中影所投資拍攝，主要的推動者為中影主事者[明驥與中高階層的](../Page/明驥.md "wikilink")[小野和](../Page/小野.md "wikilink")[吳念真](../Page/吳念真.md "wikilink")。此三人，可說是促成台灣新電影的主要功臣，也對當時低迷的台灣電影產生重大影響\[13\]。
在此影響下，台灣業內原本拍攝商業電影的導演見狀，也將此潮流引進商業電影境地，此種以[鄉土小說為類型的商業電影](../Page/鄉土小說.md "wikilink")，計有《[金大班的最後一夜](../Page/金大班的最後一夜_\(電影\).md "wikilink")》、《[在室男](../Page/在室男_\(電影\).md "wikilink")》、《[嫁妝一牛車](../Page/嫁妝一牛車_\(電影\).md "wikilink")》、《[孤戀花](../Page/孤戀花_\(電影\).md "wikilink")》、《[孽子](../Page/孽子_\(電影\).md "wikilink")》等。不過因為產量畢竟有限，1980年代台灣電影仍以如[許不了主角的喜劇](../Page/許不了.md "wikilink")，與其他如賭博片、犯罪片的商業電影。

另一方面，因為[香港電影的成功](../Page/香港電影.md "wikilink")，讓台灣輿論上也開始出現對台灣新電影的批判聲音。以藝術電影為主軸的台灣新電影的支持者與反對者逐漸壁壘分明，此因素，讓集體的台灣新潮流電影方朝終於1980年代末期結束\[14\]。不但如此，在[票房也大為失利](../Page/票房.md "wikilink")。不過，相對的，此藝術電影，卻同時間大受[國際影展與各國](../Page/國際影展.md "wikilink")[藝術電影市場上的歡迎](../Page/藝術電影.md "wikilink")。

## 解嚴時期 (1980年代中期)

  - 台灣在1980年代中葉起，隨著社會經濟泡沫及言論自由的達成，讓導演為主導的台灣電影，將電影觸角伸向過去禁忌題材，並回顧與探討台灣近現代社會、歷史與個人記憶。這階段代表作有《[刀瘟](../Page/刀瘟_\(電影\).md "wikilink")》、《[香蕉天堂](../Page/香蕉天堂.md "wikilink")》、《[童黨萬歲](../Page/童黨萬歲.md "wikilink")》、《[牯嶺街少年殺人事件](../Page/牯嶺街少年殺人事件.md "wikilink")》等。其中最震撼台灣的莫過於侯孝賢的《[悲情城市](../Page/悲情城市.md "wikilink")》。這部片以一[九份世家各成員經歷](../Page/九份.md "wikilink")[二戰結束](../Page/二戰結束.md "wikilink")、[二二八事件](../Page/二二八事件.md "wikilink")、及[戰後初期的歷程](../Page/臺灣戰後時期.md "wikilink")，具體而微地反映[台灣人的歷史經驗](../Page/台灣人.md "wikilink")，不但票房叫座，也在[威尼斯影展榮獲](../Page/威尼斯影展.md "wikilink")[金獅獎](../Page/金獅獎.md "wikilink")，使侯孝賢成為國際矚目的知名導演。之後幾年，台灣電影於世界各地大小影展獲得不少得獎紀錄，這使得主管電影業務的新聞局自1990年開始，透過[電影輔導金來支持藝術電影的製作](../Page/電影輔導金.md "wikilink")。
  - 1980年代起，[中華民國政府新成立](../Page/中華民國政府.md "wikilink")[電影發展基金會](../Page/電影發展基金會.md "wikilink")、[電影圖書館](../Page/電影圖書館.md "wikilink")（今[國家電影中心](../Page/國家電影中心.md "wikilink")），其功能乃為保存電影資產及推廣電影欣賞。同一時間，促成設置了[金穗獎](../Page/金穗獎.md "wikilink")。以鼓勵[動畫](../Page/動畫.md "wikilink")、[紀錄片](../Page/紀錄片.md "wikilink")、[實驗電影和](../Page/實驗電影.md "wikilink")[劇情短片製作為講目的的獎項](../Page/劇情短片.md "wikilink")，也讓台灣電影藉由[劇情片](../Page/劇情片.md "wikilink")、[紀錄片](../Page/紀錄片.md "wikilink")、[動畫](../Page/動畫.md "wikilink")、與實驗電影提拔出相當多的製作人才。

## 衰退與崩盤 (1980年代晚期-2000年)

台灣電影的衰退有幾項原因

1.  政府採行的電影政策失誤，电影辅导金额倾向支持为参加[国际影展而拍摄之](../Page/国际影展.md "wikilink")**艺术电影**，造成台湾电影在市场上的竞争力不足。
2.  台湾的电影经营业者將是否得到"获得辅导金"作为他们是否开拍该部影片的指标，无视本土电影业的永续发展。
3.  部分导演只为参加影展拍片而忽视观众，造成了观众对台湾电影失去信心。
4.  在全球化的浪潮下，台灣本土市場被[好莱坞电影佔據](../Page/好莱坞.md "wikilink")\[15\]。

80年代晚期，由于[录影带](../Page/录影带.md "wikilink")、[CD](../Page/CD.md "wikilink")、[有线电视等新兴视听媒体陆续出现](../Page/有线电视.md "wikilink")，以及[MTV](../Page/MTV.md "wikilink")、[KTV等娱乐场所竞争之下](../Page/卡拉OK.md "wikilink")，电影產业失去对观众的独一无二吸引力，电影院的数目已剧减至1990年的567家，台灣產量與發行量較多的如[學甫](../Page/學甫.md "wikilink")、龍族、[麗城](../Page/麗城.md "wikilink")、[中影](../Page/中影股份有限公司.md "wikilink")、上上、[倍倫](../Page/倍倫.md "wikilink")、新船、[金壂](../Page/金壂.md "wikilink")、學者、[龍祥等各公司只能萎縮產量](../Page/龍祥.md "wikilink")，讓製片量一路下滑，把產業重心移往如[有線電視的](../Page/有線電視.md "wikilink")[影視產業](../Page/影視產業.md "wikilink")。

1989年，台灣政府为因应加入[WTO的新环境](../Page/WTO.md "wikilink")，决定对外国电影采取开放措施。

1996年起，國片每年產出量一直維持在15至20部，票房市占率僅有全台總票房1～2％間

1998年，台灣電影生產不到20部\[16\]，

2000年初正式進入WTO後，**幾次的開放政策成為壓死駱駝的最後一根稻草**，這悲慘的景況到2003年達到谷底\[17\]，這年僅推出15部國片，總票房約NT1,500萬元，不到總票房的1％，這樣的票房相當於《[鐵達尼號](../Page/鐵達尼號_\(1997年電影\).md "wikilink")》的40％，國片當年愁雲慘霧，台灣成為世界少數沒有電影工業的非第三世界國家，台灣電影可謂是政府放棄的產業\[18\]。在產業無法支持下，惟剩台灣政府於[文化維持的壓力下](../Page/文化.md "wikilink")，勉為其難出資維持。
2006年，台灣電影於台灣市佔率僅佔1.62％。

就[票房方面](../Page/票房.md "wikilink")，以台北市首輪票房為例子，台灣電影的票房收入僅有163萬元，佔全部台北電影票房收入不到1%，比起美國[好萊塢所佔](../Page/好萊塢.md "wikilink")[台北票房的](../Page/台北票房.md "wikilink")95%以上相差甚遠。因為台灣電影產量極低，票房慘澹，實已不足以支撐[電影工業](../Page/電影工業.md "wikilink")。因此有媒體業者認為「再多的影展獎項都無法遮掩台灣電影全面崩盤的事實\[19\]。」

2000年後，台灣的國產電影偶有佳作，如2000年的《[聖石傳說](../Page/聖石傳說.md "wikilink")》、2002年的《[雙瞳](../Page/雙瞳_\(電影\).md "wikilink")》、2005年的《[刺青](../Page/刺青_\(電影\).md "wikilink")》等；但整體上，台灣的電影市場仍由好萊塢電影主宰，國片持續處於弱勢。

## 國片復興期(2007年-2010年)

[Taipei_movie_1000824.JPG](https://zh.wikipedia.org/wiki/File:Taipei_movie_1000824.JPG "fig:Taipei_movie_1000824.JPG")》的消費者\]\]
2007年起的[全球金融危機](../Page/2000年代後期環球金融危機.md "wikilink")，在台灣居於優勢地位的美商影視企業受到影響，使得台灣電影有了喘息的機遇。藝人[周杰倫在](../Page/周杰倫.md "wikilink")2007年用僅僅3個月的時間，自編自導自演電影《[不能說的秘密](../Page/不能說的秘密.md "wikilink")》。由於其名氣與號召力所影響，使該片讓台灣以及各地華人民眾再度接受台灣電影。此片一舉奪得三項金馬獎及兩項金曲獎，該片也是華人民眾公認周杰倫的經典電影之一，2015年也將在韓國重映。這部片成為國片低谷末期到國片熱潮期間的關鍵轉折。

2008年，[魏德聖執導的電影](../Page/魏德聖.md "wikilink")《[海角七號](../Page/海角七號.md "wikilink")》以大規模國際化商業路線（包括延攬日本歌星[中孝介演出](../Page/中孝介.md "wikilink")）並在內容上揉合本土特色，在票房方面成為[戰後以來最賣座的華語片](../Page/二戰.md "wikilink")、及台灣影史最賣座影片的第三名（不計算[通膨](../Page/通膨.md "wikilink")），同時也獲得不少獎項，叫好又叫座的結果讓台灣觀眾開始重新關注台灣自製電影\[20\]。

隨著《[海角七號](../Page/海角七號.md "wikilink")》熱賣，[片商與觀眾均對台灣電影重拾信心](../Page/片商.md "wikilink")，加上[好萊塢片廠籌措資金不易市場低迷因素減少大](../Page/好萊塢.md "wikilink")[卡司的投入](../Page/卡司.md "wikilink")，多部電影亦趕上這波替補[美國電影遺留下來的台灣市場爭奪](../Page/美國電影.md "wikilink")，因而取得較以往為佳的[票房成績](../Page/票房.md "wikilink")，如《[囧男孩](../Page/囧男孩.md "wikilink")》、《[九降風](../Page/九降風_\(電影\).md "wikilink")》、《[花吃了那女孩](../Page/花吃了那女孩.md "wikilink")》等等；而2010年的《[艋舺](../Page/艋舺_\(電影\).md "wikilink")》，2011年的《[雞排英雄](../Page/雞排英雄.md "wikilink")》、《[那些年，我們一起追的女孩](../Page/那些年，我們一起追的女孩.md "wikilink")》、《[賽德克·巴萊](../Page/賽德克·巴萊.md "wikilink")》更被視為台灣電影票房的指標。

另一方面，台灣電影也有新的發展。[鴻海集團董事長](../Page/鴻海集團.md "wikilink")[郭台銘曾發下豪願要投入電影業](../Page/郭台銘.md "wikilink")，並以電子業代工為概念，於2008年退休後拍100部以上的電影或電視劇\[21\]。有人認為，該做法是傚法[Sony進軍](../Page/Sony.md "wikilink")[好萊塢模式](../Page/好萊塢.md "wikilink")，在擁有極強大硬體整合力量之後積極轉進電影業，並謀取好萊塢600億美元電影產業的代工數位商機\[22\]。其中初試啼聲的作品（同時也是唯一的一部）即為電影《[白銀帝國](../Page/白銀帝國.md "wikilink")》。由於有郭台銘所屬鴻海集團1000萬美元的的投資，《[白銀帝國](../Page/白銀帝國.md "wikilink")》成為中台兩地少見的高製作成本的[商業電影](../Page/商業.md "wikilink")。在開拍之時，有人認為郭台銘的加入，不但能於[資本上挹注台灣電影](../Page/資本.md "wikilink")，更能為台灣電影帶來新想法與新視野\[23\]。郭台銘以此為契機跨足[娛樂圈](../Page/娛樂圈.md "wikilink")，與眾女星大玩桃色新聞；在娶得新夫人後改為低調退出。而後郭台銘的[精緯電影公司解散](../Page/精緯電影公司.md "wikilink")，但其長子[郭守正的](../Page/郭守正.md "wikilink")[山水國際娛樂股份有限公司仍繼續經營](../Page/山水國際娛樂股份有限公司.md "wikilink")\[24\]。

2008年台灣電影總產量（指製作及完成者）為36部，總票房約1,085萬元，市占率12.09%，較2007年之7.38%成長約0.6倍；2009年國產電影片總產量為48部，較2008年之36部成長約33%，總票房約1億2,499萬元，市占率2.3%，衰退八成以上；2010年國產電影片總產量為50部，總票房約4億5,117萬元，市占率7.31%，較2009年成長約2倍，以產量而言台灣電影已逐漸復甦之趨勢\[25\]。

## 2010年代

  - 2011年《[賽德克·巴萊](../Page/賽德克·巴萊.md "wikilink")》、《[雞排英雄](../Page/雞排英雄.md "wikilink")》、《[那些年，我們一起追的女孩](../Page/那些年，我們一起追的女孩.md "wikilink")》\[26\]，是當年台灣票房破億的電影，其中那些年更在[兩岸四地](../Page/兩岸四地.md "wikilink")、馬星取得不錯的成績，是台灣電影成功的海外輸出例子。
  - 2012年《[陣頭](../Page/陣頭_\(電影\).md "wikilink")》、《[愛](../Page/愛_\(電影\).md "wikilink")》、《[犀利人妻最終回：幸福男·不難](../Page/犀利人妻最終回：幸福男·不難.md "wikilink")》、《[痞子英雄首部曲:全面開戰](../Page/痞子英雄首部曲:全面開戰.md "wikilink")》
    ，都是台灣破億影片；口碑與票房都頗受肯定\[27\]。
  - 2013年《[大尾鱸鰻](../Page/大尾鱸鰻.md "wikilink")》、《[總舖師](../Page/總舖師_\(電影\).md "wikilink")》，也是台灣破億影片。和以往國片低迷期時藝術電影掛帥的狀況不同，以一般大眾為市場目標的商業電影重新成為台灣電影的主流，以戲院上映為主的商業[紀錄片也開始取得一定的發展](../Page/紀錄片.md "wikilink")（如《[翻滾吧！男孩](../Page/翻滾吧！男孩.md "wikilink")》、《[看見台灣](../Page/看見台灣.md "wikilink")》等），其中**[看見台灣](../Page/看見台灣.md "wikilink")**更是台灣史上最高票房的[紀錄片](../Page/紀錄片.md "wikilink")；而好票房衍伸出商機，讓片商有資本繼續製作吸引觀眾的電影，也讓台灣電影市場逐漸邁向資金提供、影片內容、觀眾票房三者均贏的[正向循環](../Page/正向循環.md "wikilink")\[28\]。
  - 2014年《[KANO](../Page/KANO.md "wikilink")》、《[等一個人咖啡](../Page/等一個人咖啡.md "wikilink")》、《[大稻埕](../Page/大稻埕_\(電影\).md "wikilink")》票房破億，然而此年只有[九把刀的](../Page/九把刀.md "wikilink")《等一個人咖啡》能在台灣票房回本，其他都因為成本太高回不了本，由此可知，在台灣拍電影等於把錢丟到水裡的艱困狀況仍然存在\[29\]。
  - 2015年《[大囍臨門](../Page/大囍臨門.md "wikilink")》、《[我的少女時代](../Page/我的少女時代.md "wikilink")》是此年破億票房的國片，其中**我的少女時代**在全球24億票房，是繼**那些年**之後，另一個成功的海外輸出國片\[30\]。
  - 2016年《[大尾鱸鰻2](../Page/大尾鱸鰻2.md "wikilink")》、《[樓下的房客](../Page/樓下的房客.md "wikilink")》是此年破億票房的國片，但其中《[大尾鱸鰻2](../Page/大尾鱸鰻2.md "wikilink")》引發原住民議題造成票房比前部明顯下滑。隨著中國大陸電影市場日趨興起，以及陸方豐厚的資金與優渥條件，臺灣電影人開始展開兩岸合拍，或是轉向大陸市場。而大陸電影公司也因市場條件，紛紛與好萊塢電影展開合作與併購，類似於2016年底上映的《[長城](../Page/長城.md "wikilink")》或《[金剛：骷髏島](../Page/金剛：骷髏島.md "wikilink")》等合拍性質電影紛紛崛起，同時挾帶好萊塢影星與華人影星的共同名氣，在華人與國際市場取得雙贏。在同時，過多臺灣電影繼續走青春校園路線，或是過於雷同的劇情、帶有許多髒話、低俗笑料的賀歲電影(例如《[大尾鱸鰻](../Page/大尾鱸鰻.md "wikilink")》、《[大宅們](../Page/大宅們.md "wikilink")》)，造成臺灣觀眾對於國片再度感到疲乏。多數觀眾興致再度轉移好萊塢甚至中美合拍電影。臺灣電影商業，資金的確不如大陸與好萊塢，但臺灣的電影人缺的其實不是資金，而是構思出有深度與意義的劇情的智慧。電影人不能總是將資金和場面畫上等號，一旦自認為無法達成而卻步，臺灣電影仍會回到從前的低谷期。
  - 2017年《[紅衣小女孩2](../Page/紅衣小女孩2.md "wikilink")》此年破億票房國片，從2015年起台灣恐怖驚悚類型國片逐漸串起帶動國片風潮。2017年賀歲片票房失利，各片皆僅有兩千多萬票房成績\[31\]。台灣首次選派紀錄片《[日常對話](../Page/日常對話.md "wikilink")》作為代表，角逐[第90屆奧斯卡金像獎](../Page/第90屆奧斯卡金像獎.md "wikilink")[最佳外語片獎](../Page/奥斯卡最佳外语片奖.md "wikilink")\[32\]。

## 参考文献

## 外部連結

  - [台灣電影網](http://www.taiwancinema.com/mp.asp?mp=1/)
  - [台灣電影資料庫](https://web.archive.org/web/20080517154332/http://cinema.nccu.edu.tw/)

[臺灣電影史](../Category/臺灣電影史.md "wikilink")
[Category:台灣文化史](../Category/台灣文化史.md "wikilink")

1.

2.  [暮然回首----台灣電影一百年](http://techart.tnua.edu.tw/~dmlee/article1.html)

3.

4.  [李道明，《日本統治時期電影與政治的關係》](http://techart.tnua.edu.tw/~dmlee/article5.html)


5.
6.  [台灣電影史](http://taojianbao.myweb.hinet.net/city/mvphoto/mvphoto.htm)

7.  [首頁 \> 電影大事記 \> 1950 \~ 1959
    年](http://www.ctfa.org.tw/history/index.php?id=1096)

8.  [陳澄三](http://movie.cca.gov.tw/PEOPLE/people_inside.asp?rowid=142&id=5)


9.
10. 林文淇，《大醉俠與龍門客棧的電影敘事比較》

11.

12.
13.
14.
15. [九十年代台湾电影的政策](http://scholar.fju.edu.tw/%E8%AA%B2%E7%A8%8B%E5%A4%A7%E7%B6%B1/upload/030827/handout/961/G-0950-15804-.doc)

16. 盧非易，《從數字看台灣電影五十年》

17. [美麗寶島的光影之路——台灣電影紀實](http://store.gvm.com.tw/article_content_19393.html)

18. [國片元年總體檢
    如何延續國片熱？](http://mag.cnyes.com/Content/20111124/47ac5a0ee303437c9b37d3f172b2793a_2.shtml)

19. 何自力、范麗青，《台灣電影遭遇寒冬》

20. [觀眾盼拍海角七號前傳，魏導：故事的美好是留在現在](http://www.nownews.com/2008/09/24/38-2339738.htm)

21. 中時電子報，《郭台銘的電影夢》

22. 科技產業研究室，《鴻海郭台銘與數位內容》

23.
24. [郭台銘 電影大夢半年玩完！ 投資百億拍百部國片淪為空話 -
    自由時報 2008.07.18](http://www.libertytimes.com.tw/2008/new/jul/18/today-show1.htm)


25.

26. [2013年台灣小品電影
    賺到票房](http://www.chinatimes.com/newspapers/20140130000525-260308)

27. [TVBS：投資國片眼光準　郭台強砸4億笑豐收](http://tw.news.yahoo.com/article/url/d/a/110823/8/2xejn.html)

28. [社論《掌握契機馳騁大華人區文創市場》 -
    工商時報 2011.12.08](http://money.chinatimes.com/news/news-content.aspx?id=20111208001096&cid=1206)

29. [叫好不叫座，叫座不叫好：由2014國片票房榜看台灣電影的難題](http://punchline.asia/archives/8025)

30. [票房王《少女時代》全球狂撈24億
    在台卻輸給...](http://news.cts.com.tw/cts/entertain/201512/201512231697393.html#.VoAKjvl97IU)

31.

32.