**让-克洛德·杜瓦利埃**（**Jean-Claude Duvalier**，绰号**Baby
Doc**（娃娃醫生），\[1\]），[海地](../Page/海地.md "wikilink")[总统](../Page/海地总统.md "wikilink")、[獨裁者](../Page/獨裁者.md "wikilink")，迄今為止任職時間最長的[海地總統](../Page/海地總統.md "wikilink")，統治[海地長達](../Page/海地.md "wikilink")15年。由於他與父亲同樣擔任過[海地总统](../Page/海地总统.md "wikilink")，因此又常被稱為**小杜瓦利埃**以區別，而他的父亲則被稱為[老杜瓦利埃](../Page/老杜瓦利埃.md "wikilink")。

## 早期生活

1951年7月3日，小杜瓦利埃出生在[太子港](../Page/太子港.md "wikilink")，他曾就读于太子港的和。1971年4月22日，年僅19岁的他在父亲[弗朗索瓦·杜瓦利埃去世後](../Page/弗朗索瓦·杜瓦利埃.md "wikilink")，經公投以「100％贊成票」，同意他繼任其父成為終身職的[海地总统](../Page/海地总统.md "wikilink")，成为[世界上最年轻的](../Page/世界.md "wikilink")[总统](../Page/总统.md "wikilink")（該項記錄至今還未有被任何一个[國家的](../Page/國家.md "wikilink")[領導人打破](../Page/領導人.md "wikilink")）。

## 海地总统 (1971-1986)

在小杜瓦利埃执政期间，海地是全[美洲](../Page/美洲.md "wikilink")（包括[北美洲及](../Page/北美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")）最[黑暗的](../Page/獨裁.md "wikilink")[国家和最](../Page/国家.md "wikilink")[贫穷的](../Page/贫穷.md "wikilink")[国家](../Page/国家.md "wikilink")，同時，[杜瓦利埃家族竟然擁有全国](../Page/杜瓦利埃家族.md "wikilink")90%的財富，更为[拉美地区的首富](../Page/拉美.md "wikilink")。杜瓦利埃政府的[海地軍隊](../Page/海地軍隊.md "wikilink")、防暴力反游击特种部队、海地[宪兵](../Page/宪兵.md "wikilink")（[國家安全志願軍](../Page/國家安全志願軍.md "wikilink")）、[伏都教民兵和总统卫队](../Page/伏都教.md "wikilink")，每年的维持费用佔国家预算的一半。而且，在[老杜瓦利埃和小杜瓦利埃统治的](../Page/老杜瓦利埃.md "wikilink")29年內竟然有10多万人被[枪杀](../Page/枪杀.md "wikilink")、[拷打](../Page/拷打.md "wikilink")、[拘押或](../Page/拘押.md "wikilink")[流放](../Page/流放.md "wikilink")。

1985年7月22日，海地以“99.98%的公投赞成票”獲得通過憲改提案，在小杜瓦利埃仍为[终身总统](../Page/终身总统.md "wikilink")，各黨派對其效忠的前提下，開放多黨政治，而其仍握有單獨任命總理以及選擇總統繼承人的權利。1986年1月29日，[海地角](../Page/海地角.md "wikilink")3万多人举行大型[示威及](../Page/示威.md "wikilink")[游行](../Page/游行.md "wikilink")，2月3日，[太子港爆发大型](../Page/太子港.md "wikilink")[罢工](../Page/罢工.md "wikilink")，400多家[商店和](../Page/商店.md "wikilink")50多家[工厂关门](../Page/工厂.md "wikilink")，100多所[学校](../Page/学校.md "wikilink")[罢课](../Page/罢课.md "wikilink")。2月7日，小杜瓦利埃带着十多名家眷乘坐[美国提供的](../Page/美国.md "wikilink")[运输机逃出](../Page/运输机.md "wikilink")[海地](../Page/海地.md "wikilink")（先后受到[美国和](../Page/美国.md "wikilink")[法国的](../Page/法国.md "wikilink")[庇护](../Page/庇护.md "wikilink")）。

## 流亡及渴望返国

2004年2月，正在[美国](../Page/美国.md "wikilink")[流亡中的小杜瓦利埃表示](../Page/流亡.md "wikilink")，他希望能夠回到[海地居住](../Page/海地.md "wikilink")，而且，小杜瓦利埃還表示：“没有谋求再次出任[总统的计划](../Page/总统.md "wikilink")”。但是，海地[舆论卻开始关注小杜瓦利埃在目前](../Page/舆论.md "wikilink")（2004年）海地局势十分混乱的情况下表示希望回国有何所图。

## 回到海地及去世

2011年，在[海地政府推動和解的政策後](../Page/海地政府.md "wikilink")，他在流亡25年後獲准回到海地居住。2014年10月4日，他於[太子港自己的住宅因](../Page/太子港.md "wikilink")[心臟病而](../Page/心臟病.md "wikilink")[逝世](../Page/逝世.md "wikilink")\[2\]\[3\]。

## 參考資料

{{-}}

[Category:海地总统](../Category/海地总统.md "wikilink")
[Category:杜瓦利埃家族](../Category/杜瓦利埃家族.md "wikilink")
[Category:考迪羅](../Category/考迪羅.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:反共主義者](../Category/反共主義者.md "wikilink")
[Category:死于心脏衰竭的人](../Category/死于心脏衰竭的人.md "wikilink")

1.

2.
3.  [海地前總統小杜瓦利埃逝世，联合國强调有必要繼續寻求正义](http://www.un.org/chinese/News/story.asp?NewsID=22713/)