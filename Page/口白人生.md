《**奇幻人生**》（***Stranger than
Fiction***）是一部[2006年的](../Page/2006年電影.md "wikilink")[美國](../Page/美國.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")[電影](../Page/電影.md "wikilink")，由[馬克·福斯特執導](../Page/馬克·福斯特.md "wikilink")，劇本由[柴克·海姆](../Page/柴克·海姆.md "wikilink")（Zach
Helm）編寫，演員包括[威爾·法洛](../Page/威爾·法洛.md "wikilink")、[瑪姬·賈倫荷](../Page/瑪姬·賈倫荷.md "wikilink")、[-{zh-hans:达斯汀·霍夫曼;zh-hk:德斯汀·荷夫曼;zh-tw:達斯汀·霍夫曼;}-](../Page/德斯汀·荷夫曼.md "wikilink")、[皇后·拉蒂法](../Page/皇后·拉蒂法.md "wikilink")、[艾瑪·湯普遜和](../Page/艾瑪·湯普遜.md "wikilink")[琳達·杭特](../Page/琳達·杭特.md "wikilink")。電影在美國和[台灣等地區是由](../Page/台灣.md "wikilink")[哥倫比亞影業發行](../Page/哥倫比亞影業.md "wikilink")。本片大部分的場景是在美國[伊利諾州](../Page/伊利諾州.md "wikilink")[芝加哥拍攝](../Page/芝加哥.md "wikilink")。

## 劇情

哈洛·克里（Harold
Crick，[威爾·法洛飾](../Page/威爾·法洛.md "wikilink")）是一名在[美國國稅局工作的查稅員](../Page/美國國稅局.md "wikilink")，他的日常生活非常平凡規律。某一天早晨，哈洛被自己的[天美時](../Page/天美時.md "wikilink")（Timex）Ironman
T56371型[手錶叫醒](../Page/手錶.md "wikilink")，在刷牙時突然發現自己能夠聽到一個女人的聲音，這個聲音精準的描述了他生活中每一件事情、動作，和他每一個想法，而且非常精準，用詞優美，但哈洛深受這個聲音的困擾。

在上班時，哈洛被分配到前往[糕點烘焙師安娜](../Page/點心.md "wikilink")·貝斯卡（Ana
Pascal，[瑪姬·賈倫荷飾](../Page/瑪姬·賈倫荷.md "wikilink")）的店內查稅，哈洛一見到安娜就開始喜歡上她，而那個女人的聲音也在當時完全說中哈洛的想法。當哈洛離開糕點店時，他的手錶突然故障，於是他向一旁行人詢問現在的時間，就在這個時候，哈洛聽到那個聲音宣告他即將死亡。

哈洛陷入極度的緊張和焦慮，因此他去向[心理醫生求診](../Page/心理醫生.md "wikilink")，但心理醫生最後卻推薦他去找一位[文學專家](../Page/文學.md "wikilink")—朱利斯·希柏特（Jules
Hilbert，[達斯汀·霍夫曼飾](../Page/達斯汀·霍夫曼.md "wikilink")）。希柏特提供了哈洛許多建議，但都無法真正的幫助他。某一天，哈洛在希柏特辦公室內的電視上，發現一直在他腦海中旁白的聲音是來自正在參加訪談節目的作家凱倫·艾菲爾（Karen
Eiffel，[艾瑪·湯普遜飾](../Page/艾瑪·湯普遜.md "wikilink")），而凱倫正在節目中暢談她的正在撰寫中的新書—《死亡與賦稅》（*Death
and Taxes*），哈洛認為這本書就是在講述自己的故事，因此他立刻動身，希望尋找到凱倫，阻止她將小說中的主角（也就是哈洛自己）殺死……

## 角色

| 角色                     | 演員                                                        | 角色背景                                                                                                            |
| ---------------------- | --------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| 哈洛·克里（Harold Crick）    | [威爾·法洛](../Page/威爾·法洛.md "wikilink")                      | 美國國稅局的查稅員，某天開始發現自己的人生出現旁白的聲音，而且那個聲音宣告他即將邁向死亡，而這樣的狀況改變了他的整個人生。                                                   |
| 凱倫·艾菲爾（Karen Eiffel）   | [艾瑪·湯普遜](../Page/艾瑪·湯普遜.md "wikilink")                    | 知名[作家](../Page/作家.md "wikilink")，也是哈洛人生旁白的聲音來源。                                                                 |
| 朱利斯·希柏特（Jules Hilbert） | [德斯汀·荷夫曼](../Page/德斯汀·荷夫曼.md "wikilink")                  | [文學](../Page/文學.md "wikilink")[教授](../Page/教授.md "wikilink")，他試著藉由分析哈洛的故事是悲劇或是喜劇，希望能幫助哈洛找出旁白聲音的來源。他也是凱倫·艾菲爾的書迷。 |
| 潘妮·艾斯可（Penny Escher）   | [皇后·拉蒂法](../Page/皇后·拉蒂法.md "wikilink")                    | 艾菲爾的助理，是由出版社所聘請，希望她能幫助艾菲爾準時完成小說。                                                                                |
| 安娜·貝斯卡（Ana Pascal）     | [瑪姬·賈倫荷](../Page/瑪姬·賈倫荷.md "wikilink")（Maggie Gyllenhaal） | 糕點烘培師，曾就讀過[哈佛大學法律系](../Page/哈佛大學.md "wikilink")，卻對於[烘培有著極大的興趣和熱忱](../Page/烘培.md "wikilink")，希望利用烘培來讓人們變得更快樂。    |
| 大衛（Dave）               | [東尼·海爾](../Page/東尼·海爾.md "wikilink")（Tony Hale）           | 哈洛在國稅局的朋友，也是他唯一的朋友。在哈洛的家意外被誤拆後，大衛提供了公寓讓他借住。                                                                     |

## 獲獎與提名

  - 2007年[金球獎](../Page/金球獎.md "wikilink") -
    音樂或喜劇類最佳男主角**提名**：[威爾·法洛](../Page/威爾·法洛.md "wikilink")
  - [国家评论协会最佳原创剧本奖](../Page/国家评论协会.md "wikilink")
  - [卫星奖最佳编剧](../Page/卫星奖.md "wikilink")、最佳男主角、最佳女主角（Maggie
    Gyllenhaal）、最佳女配角（Emma Thompson）四项提名

## 外部連結

  - [《口白人生》官方網站](http://www.sonypictures.com/movies/strangerthanfiction/)

  -
  -
  - [《口白人生》原聲帶相關資訊](http://www.soundtrackinfo.com/ost.asp?soundtrack=5859)

  - [《口白人生》達斯汀·霍夫曼的專訪](https://web.archive.org/web/20071013222604/http://www.stv.tv/content/out/dontmiss/display.html?id=opencms%3A%2Fout%2Fdontmiss%2Fdustin_hoffman)

  - [Roger Ebert
    撰寫的《口白人生》影評](http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20061109/REVIEWS/611090301/1023)

  - [在電影中出現的 Timex T56371 手錶](http://www.lotsofwatches.com/t56371.html)

[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美国喜劇片](../Category/美国喜劇片.md "wikilink")
[Category:美國奇幻電影](../Category/美國奇幻電影.md "wikilink")
[Category:2000年代喜剧片](../Category/2000年代喜剧片.md "wikilink")
[Category:2000年代奇幻片](../Category/2000年代奇幻片.md "wikilink")
[Category:後設電影](../Category/後設電影.md "wikilink")
[Category:作家题材电影](../Category/作家题材电影.md "wikilink")
[Category:芝加哥取景電影](../Category/芝加哥取景電影.md "wikilink")
[Category:马克·福斯特电影](../Category/马克·福斯特电影.md "wikilink")
[Category:哥倫比亞影業電影](../Category/哥倫比亞影業電影.md "wikilink")