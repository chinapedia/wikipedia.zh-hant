**肯尼士·A·阿諾德**（[英语](../Page/英语.md "wikilink")：****，）是一位[美國](../Page/美國.md "wikilink")[企業家與](../Page/企業家.md "wikilink")[飛機](../Page/飛機.md "wikilink")[駕駛員](../Page/駕駛員.md "wikilink")，出生於[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")。

## 生平

## 目擊事件

[K-Arnold(reconstitution).png](https://zh.wikipedia.org/wiki/File:K-Arnold\(reconstitution\).png "fig:K-Arnold(reconstitution).png")
[Arnold_AAF_drawing.jpg](https://zh.wikipedia.org/wiki/File:Arnold_AAF_drawing.jpg "fig:Arnold_AAF_drawing.jpg")描述UFO的信件\]\]

1947年6月24日，肯尼士·阿諾德為了尋找失事的飛機，從[華盛頓州的](../Page/華盛頓州.md "wikilink")[敘阿利斯](../Page/奇黑利斯_\(华盛顿州\).md "wikilink")（Chehalis）駕駛CallAir
A-2型飛機前往[雅基馬](../Page/雅基馬.md "wikilink")\[1\]\[2\]。在途中，阿諾德目擊到一系列的不明飛行物體從他所駕駛的飛機左前方快速的消失在右測。在落地之後，阿諾德很快的告知朋友與機場經理這件驚人的目擊事件，並將這些未知的飛行物體形容為「飛舞的碟子」。在這之後，飛碟這個名詞於是成為[UFO的](../Page/UFO.md "wikilink")[代名詞之一](../Page/代名詞.md "wikilink")。該起事件亦引發了全球對UFO現象的關注\[3\]，世界各國陸續出現不少類似的飛碟目擊報告\[4\]\[5\]。

## 參考資料

<references />

## 外部連結

  - [Out of This World: 60 Years of Flying Saucers by Nigel
    Watson](http://www.wired.com/culture/lifestyle/news/2007/06/flyingsaucer_anniversary?currentPage=1)
  - [The most complete Arnold sighting description and analysis on the
    net](https://web.archive.org/web/20100107012904/http://brumac.8k.com/KARNOLD/KARNOLD.html)
  - [Resolving Arnold
    part 1](http://www.reall.org/newsletter/v05/n06/resolving-arnold-part-1.html)
  - [Resolving Arnold
    part 2](http://www.reall.org/newsletter/v05/n07/resolving-arnold-part-2.html)
  - [Transcript of telephone conversation with
    Arnold](http://www.project1947.com/fig/kamurrow.htm) by [Edward R.
    Murrow](../Page/Edward_R._Murrow.md "wikilink")
  - [Some early newspaper articles on the Arnold
    sighting](http://www.project1947.com/fig/1947b.htm)

[Category:美國飛行員](../Category/美國飛行員.md "wikilink")
[Category:美国企业家](../Category/美国企业家.md "wikilink")
[Category:蒙大拿州人](../Category/蒙大拿州人.md "wikilink")
[Category:鹰级童军](../Category/鹰级童军.md "wikilink")
[Category:明尼蘇達大學校友](../Category/明尼蘇達大學校友.md "wikilink")

1.

2.

3.
4.

5.