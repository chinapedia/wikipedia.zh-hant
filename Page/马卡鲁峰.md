[Himalaya_annotated.jpg](https://zh.wikipedia.org/wiki/File:Himalaya_annotated.jpg "fig:Himalaya_annotated.jpg")

**马卡鲁峰**（[英語](../Page/英語.md "wikilink")：Makalu）位于[中国与](../Page/中国.md "wikilink")[尼泊尔边境](../Page/尼泊尔.md "wikilink")、[珠穆朗玛峰东南约](../Page/珠穆朗玛峰.md "wikilink")24-{zh-hans:千米;zh-hant:公里}-，海拔8,485-{zh-hans:米;zh-hant:公尺}-，是世界第五高峰。

## 参见

  - [山峰列表](../Page/山峰列表.md "wikilink")

[Category:八千米高山](../Category/八千米高山.md "wikilink")
[Category:西藏山峰](../Category/西藏山峰.md "wikilink")
[Category:尼泊爾山峰](../Category/尼泊爾山峰.md "wikilink")
[Category:喜馬拉雅山脈](../Category/喜馬拉雅山脈.md "wikilink")
[Category:中尼边界](../Category/中尼边界.md "wikilink")