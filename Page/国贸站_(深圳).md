**國貿站**是[深圳地鐵](../Page/深圳地鐵.md "wikilink")[1号綫的一个车站](../Page/深圳地鐵1号綫.md "wikilink")，位於[深圳市商業區](../Page/深圳市.md "wikilink")[人民南路與](../Page/人民南路_\(深圳\).md "wikilink")[嘉賓路交叉路口](../Page/嘉賓路.md "wikilink")，临近[国贸大厦](../Page/深圳國際貿易中心大廈.md "wikilink")，於2004年12月28日啟用。车站設計以白色及淺藍色為主。

## 車站結構

國貿站为地下三层8.5米车站，地面为人民南路與嘉賓路路口，地下一层为站厅，地下二层、三层为两个[侧式站台](../Page/侧式站台.md "wikilink")。车站建筑总面积12410平方米。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面(L1)</strong></p></td>
<td></td>
<td><p>出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一層(B1)</strong></p></td>
<td><p>車站大堂</p></td>
<td><p>車站大堂、售票機</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二層(B2)</strong></p></td>
<td><center>
<p><small><a href="../Page/側式月台.md" title="wikilink">側式月台</a>，右邊車門將會開啟</small></p>
</center></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong></strong> <strong>往机场东</strong><small>（<a href="../Page/老街站.md" title="wikilink">老街</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地下三層(B3)</strong></p></td>
<td><center>
<p><small>側式月台，左邊車門將會開啟</small></p>
</center></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong></strong> <strong>往罗湖</strong> <small>（總站）</small></p></td>
<td></td>
</tr>
</tbody>
</table>

### 站厅

站厅采用敞开式设计，东侧为收费区，站内设有[便利店](../Page/便利店.md "wikilink")、[ATM](../Page/ATM.md "wikilink")、[洗手间](../Page/洗手间.md "wikilink")。从站厅出发的两部扶手电梯可前往地下二层，前往地下三层的乘客需在1号站台再乘扶手电梯前往地下三层。

### 站台

国贸站采用[侧式叠式站台](../Page/侧式叠式站台.md "wikilink")，地下二层1号站台可前往[机场东站](../Page/机场东站.md "wikilink")，地下三层2号站台可前往[罗湖站](../Page/罗湖站.md "wikilink")。

### 出口

國貿站設有五個出口。A出口临近[金光华广场](../Page/金光华广场.md "wikilink")，B、C出口分别位于[国贸大厦南端](../Page/国贸大厦.md "wikilink")、西端，D、E出口分别位于嘉宾路北面、南面。本站**不设**无障碍设施。

<table>
<thead>
<tr class="header">
<th><p>width:70px | 出口编号</p></th>
<th><p>建议前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>通往嘉賓路南側、羅湖醫院、南湖小學、<a href="../Page/金光華廣場.md" title="wikilink">金光華廣場</a>、嘉里中心、佳寧娜廣場、友誼城等地。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>通往人民南路東側、羅湖大酒店、彭年酒店、<a href="../Page/國貿大廈.md" title="wikilink">國貿大廈</a>、富苑酒店等地。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>通往人民南路東側、羅湖大酒店、天安國際大酒店、東洋國際時裝批發廣場等地。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>通往人民南路西側、深圳市眼科醫院、海燕大廈、安華大廈等地。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>通往嘉賓路南、羅湖小學、國際商場、友誼商場等地。</p></td>
</tr>
</tbody>
</table>

## 运营时刻表

<table>
<thead>
<tr class="header">
<th><p>行驶方向</p></th>
<th><p>首班车</p></th>
<th><p>末班车</p></th>
<th><p>所属线路</p></th>
<th><p>高峰间隔</p></th>
<th><p>平峰间隔</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>罗湖</p></td>
<td><p>06:55（工作日）<br />
06:59（节假日及休息日）</p></td>
<td><p>00:06（工作日）<br />
00:05（休息日）<br />
01:05（节假日）</p></td>
<td></td>
<td><p>3分钟</p></td>
<td><p>6分钟</p></td>
</tr>
<tr class="even">
<td><p>机场东</p></td>
<td><p>06:32（工作日）<br />
06:31（节假日及休息日）</p></td>
<td><p>23:02（工作日）<br />
23:01（休息日）<br />
00:01（节假日）</p></td>
<td></td>
<td><p>3分钟</p></td>
<td><p>6分钟</p></td>
</tr>
</tbody>
</table>

## 利用情况

由于国贸站临近人民南商圈，处于[罗湖区的繁华地带](../Page/罗湖区.md "wikilink")，且周围有不少购物商场（如[金光华广场](../Page/金光华广场.md "wikilink")）、[写字楼](../Page/写字楼.md "wikilink")，以及数个住宅小区，该站无论何时客流量都较高。

## 接驳交通

## 历史

  - 2001年3月：国贸站开工建设。
  - 2004年12月28日：車站随[深圳地铁1号线开通而啟用](../Page/深圳地铁1号线.md "wikilink")。
  - 2010年12月14日：发生扶梯事故。

## 扶手电梯事故

2010年12月14日上午8:50左右，一条月台往大堂的扶手电梯在运行中突然倒转运行，乘客因失去平衡摔倒，造成25人受伤，包括一名孕妇及一名台灣老太太。

事故發生後，多名傷者無法行走，坐在站台扶梯口處。事故發生後約20分鐘，國貿站地鐵工作人員及保安才叫來附近羅湖醫院及深圳市人民醫院留醫部的[救護車將傷者送至醫院](../Page/救護車.md "wikilink")。有傷者據此抱怨地鐵方面反應過於遲鈍。羅湖醫院為送至該院的乘客均做了X光透視。大部分乘客均為腿腳處受傷，有三名受傷乘客傷情較重，經X光射線檢查為骨折。

此次事故發生時，正值上班高峰期。不少傷者為國貿站附近大廈的上班族。部分傷者姓名：豐超、江仕洪、李權珍（女）、吳廣、黃留意、向曉華、李旭（女）等。

地鐵公司承擔了全部傷者的治療及住院費用，並報銷此後因往來醫院的打的費用。

地鐵公司委託[白石洲站的兩名工作人員與傷者進行談判](../Page/白石洲站.md "wikilink")。大部分傷者均只賠償了誤工費。其中傷情較重的豐超先生獲賠人民幣9000餘元，其他傷者均在數千元不等。

媒體報道與地鐵公司回應：深圳地鐵1號線副總經理於當天下午回應了眾多記者對此次事故的提問。事故發生第二天，地鐵公司聲稱事故發生的原因是由於在緊急情況下用於固定扶梯的一枚螺絲鬆動造成的。肇事電梯由[奥的斯电梯公司](../Page/奥的斯电梯公司.md "wikilink")（OTIS）製造。深圳本地的媒體，如[深圳電視台都市頻道](../Page/深圳電視台都市頻道.md "wikilink")、公共頻道、[深圳衛視以及深圳新聞網](../Page/深圳衛視.md "wikilink")、[鳳凰衛視駐深圳記者](../Page/鳳凰衛視.md "wikilink")、[深圳人民廣播電台均作了相應報道](../Page/深圳人民廣播電台.md "wikilink")。\[1\]

## 未来发展

2007年，深圳市规划局编制了《深圳市城市轨道交通近期建设规划（2010－2020年）》，其中规划的[深圳地铁8号线起点在国贸](../Page/深圳地铁8号线.md "wikilink")，与[深圳地铁1号线换乘](../Page/深圳地铁1号线.md "wikilink")，采用轻轨建设\[2\]。然而在2009年，深圳市政府又决定采用中低速磁悬浮技术，若以该技术建设8号线，该线将不经国贸站。由于当时在采用何种方案上尚存争议，8号线是否经国贸站尚不清楚。

2015年7月，新的建设方案推出，8号线将成為2號線東延路段一部分（實即路線編號將會懸空），亦不以国贸站为起点。\[3\]

## 鄰接車站

## 参考文献及外部連結

  - [深圳地铁官网车站资料](http://www.szmc.net/page/service/service.html?code=2103&site=0102)

[Category:罗湖区](../Category/罗湖区.md "wikilink")
[Category:2004年啟用的鐵路車站](../Category/2004年啟用的鐵路車站.md "wikilink")

1.  [深圳地铁国贸站扶梯突然逆行24人受伤](http://news.sznews.com/content/2010-12/14/content_5170880.htm)
    深圳新闻网,2010-12-14
2.  [市人大代表视察东部交通建设](http://news.sina.com.cn/c/2007-03-02/073011320636s.shtml)
    [新浪网](../Page/新浪网.md "wikilink"),2010-12-14
3.  [地铁8号线起点改为盐田路
    与2号线换乘](http://sz.southcn.com/content/2015-07/07/content_127823152.htm)
    [南方日报](../Page/南方日报.md "wikilink"),2015-7-7