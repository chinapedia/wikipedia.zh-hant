**叉齒鱚科**（[学名](../Page/学名.md "wikilink")：），為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目的其中一個](../Page/鱸形目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分類

**叉齒鱚科**其下分4個屬，如下：

### 叉齒龍鰧屬(*Chiasmodon*)

  -   - (*Chiasmodon asper*)
      - [布氏叉齒龍鰧](../Page/布氏叉齒龍鰧.md "wikilink")(*Chiasmodon braueri*)
      - (*Chiasmodon harteli*)
      - (*Chiasmodon microcephalus*)
      - [黑叉齒龍鰧](../Page/黑叉齒龍鰧.md "wikilink")(*Chiasmodon niger*)：又稱叉齒鱚。
      - (*Chiasmodon pluriradiatus*)
      - [暗灰叉齒龍鰧](../Page/暗灰叉齒龍鰧.md "wikilink")(*Chiasmodon subniger*)

### 線棘細齒鰧屬(*Dysalotus*)

  -   - [線棘細齒鰧](../Page/線棘細齒鰧.md "wikilink")(*Dysalotus
        alcocki*)：又稱線棘細齒鱚。
      - [犁齒線棘四齒鰧](../Page/犁齒線棘四齒鰧.md "wikilink")(*Dysalotus
        oligoscolus*)

### 蛇牙龍鰧屬(*Kali*)

  -   - (*Kali colubrina*)
      - (*Kali falx*)
      - [印度蛇牙龍鰧](../Page/印度蛇牙龍鰧.md "wikilink")(*Kali indica*)
      - (*Kali kerberti*)
      - [大口蛇牙龍鰧](../Page/大口蛇牙龍鰧.md "wikilink")(*Kali macrodon*)
      - [大尾蛇牙龍鰧](../Page/大尾蛇牙龍鰧.md "wikilink")(*Kali macrura*)
      - [帕氏蛇牙龍鰧](../Page/帕氏蛇牙龍鰧.md "wikilink")(*Kali parri*)

### 黑線岩鱸屬(*Pseudoscopelus*)

  -   - [高鰭黑線岩鱸](../Page/高鰭黑線岩鱸.md "wikilink")(*Pseudoscopelus
        altipinnis*)
      - (*Pseudoscopelus aphos*)
      - (*Pseudoscopelus astronesthidens*)
      - (*Pseudoscopelus australis*)
      - (*Pseudoscopelus bothrorrhinos*)
      - (*Pseudoscopelus cephalus*)
      - (*Pseudoscopelus cordilluminatus*)
      - (*Pseudoscopelus lavenbergi*)
      - (*Pseudoscopelus obtusifrons*)
      - (*Pseudoscopelus odontoglossum*)
      - (*Pseudoscopelus parini*)
      - (*Pseudoscopelus paxtoni*)
      - (*Pseudoscopelus pierbartus*)
      - [黑線岩鱸](../Page/黑線岩鱸.md "wikilink")(*Pseudoscopelus sagamianus*)
      - [巴哈馬黑線岩鱸](../Page/巴哈馬黑線岩鱸.md "wikilink")(*Pseudoscopelus
        scriptus*)
      - [截吻黑線岩鱸](../Page/截吻黑線岩鱸.md "wikilink")(*Pseudoscopelus
        scutatus*)

[\*](../Category/叉齒鱚科.md "wikilink")