**D
Album**是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")4張[專輯](../Page/音樂專輯.md "wikilink")。於2000年12月13日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

在本專輯推出前，同年已推出一張單曲精選專輯『[KinKi Single
Selection](../Page/KinKi_Single_Selection.md "wikilink")』，而距離上一張原創專輯『[C
album](../Page/C_album.md "wikilink")』則已經相隔了1年4個月之久。可以說近畿小子每隔一年才推出原創專輯的定律已經成形。

在本專輯發售出的單曲「[夏之王者／除了你誰都不愛](../Page/夏之王者/除了你誰都不愛.md "wikilink")」，在兩位成員主演的電視連續劇帶動下再次在[Oricon銷量榜累積銷量裡超越了](../Page/Oricon.md "wikilink")80萬張。而單曲精選專輯亦同樣達成了百萬銷量的好成績。但與上兩張專輯『[B
album](../Page/B_album.md "wikilink")』及『C
album』均創下80萬張銷量相比之下，本專輯的推出被安排於同年的大熱單曲及單曲精選專輯之下則顯得遜色不少，累積銷量一次過回落到50萬張左右。其中一個理由，是因為本專輯只收錄了「夏之王者／除了你誰都不愛」一張單曲而已。在此之前的原創專輯，通常都會收錄差不多3張單曲的主打歌曲。如果只收錄一張單曲的話，對於歌迷以外的消費者來說只會是一張滿佈了全不熟識的新曲專輯而已，缺乏了購買的誘因。雖然收錄了在發售前已經在電視廣告上播放的廣告歌「KinKi
Kids forever」，但亦不可否認話題性欠奉。至於一年後下一張的原創專輯『[E
album](../Page/E_album.md "wikilink")』，其累積銷量亦只僅僅上升了兩萬張而已。

自在本年度推出的單曲「夏之王樣／除了你誰都不愛」裡親自擔當[製作人後](../Page/製作人.md "wikilink")，KinKi
Kids二人從本專輯開始就負起製作人一職。本專輯亦繼續分別收錄了剛和光一各自作曲作詞的獨唱作品。提到親自監製，這張專輯光一的參與比較多。他積極地參與了編曲及混音部份，反之剛則較少參與製作方面的工作。所以名義上是KinKi
Kids作監製，不過實際上卻由光一作主導，而剛則從下張專輯開始多點參與監製。

專輯名稱的“[D](../Page/D.md "wikilink")”與之前的三張作品有所關連，解作『*A*lbum *B*est
*C*hoise
**D**antotsu』（專輯、最佳、選擇、大幅拋離第二位），亦包含了二人的[姓氏](../Page/姓氏.md "wikilink")「堂本」（Domoto）裡的『D』字。

可能由於二人親自監製的關係，本專輯一次過拋棄了過往偶像曲風的流行歌曲，整體上以創作歌曲為主。另外，他們各自的歌唱風格亦有所轉變，可見他們親自監製的獨特之處。

## 收錄歌曲

1.  **Burning Love**
      - 作曲：[Ooyagi Hiroo](../Page/Ooyagi_Hiroo.md "wikilink")
      - 作詞：Ooyagi Hiroo
      - 編曲：[宗像仁志](../Page/宗像仁志.md "wikilink")
2.  **Back Fire**
      - 作曲：[石塚知生](../Page/石塚知生.md "wikilink")
      - 作詞：[戶澤暢美](../Page/戶澤暢美.md "wikilink")
      - 編曲：石塚知生
      - 和音編排：[岩田雅之](../Page/岩田雅之.md "wikilink")
3.  **夏之王者**（****）
      - ※堂本剛參與演出的[東京放送](../Page/東京放送.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『Summer
        Snow』主題曲。第10張單曲。
      - 作曲：[羽田一郎](../Page/羽田一郎.md "wikilink")
      - 作詞：[康　珍化](../Page/康珍化.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
4.  **Misty**
      - 作曲：[堂島孝平](../Page/堂島孝平.md "wikilink")
      - 作詞：堂島孝平
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
5.  **Angel**（****）
      - 作曲：[谷本　新](../Page/谷本新.md "wikilink")
      - 作詞：[淺田信一](../Page/淺田信一.md "wikilink")
      - 編曲：[白井良明](../Page/白井良明.md "wikilink")
      - 和音編排：[松下　誠](../Page/松下誠.md "wikilink")
6.  **十二月**
      - ※堂本剛自己作曲作詞的獨唱作品。
      - 作曲：[堂本　剛](../Page/堂本剛.md "wikilink")
      - 作詞：堂本　剛
      - 編曲：[新川　博](../Page/新川博.md "wikilink")
7.  **想個辦法吧**（****）
      - 作曲：[Face 2 fAKE](../Page/Face_2_fAKE.md "wikilink")
      - 作詞：[三井　拓](../Page/三井拓.md "wikilink")
      - 編曲：Face 2 fAKE
      - 和音編排：[佐佐木久美](../Page/佐佐木久美.md "wikilink")
8.  **答案永遠在心中**（****）
      - 作曲：堂島孝平
      - 作詞：堂島孝平
      - 編曲：堂島孝平
9.  **永遠的每一天**（****）
      - ※堂本光一自己作曲作詞的獨唱作品。
      - 作曲：[堂本光一](../Page/堂本光一.md "wikilink")
      - 作詞：堂本光一
      - 編曲：石塚知生、[鶴田海生](../Page/鶴田海生.md "wikilink")
10. **慾望的Rain**（****）
      - 作曲：[宮崎　步](../Page/宮崎步.md "wikilink")
      - 作詞：戶澤暢美
      - 編曲：[鈴木雅也](../Page/鈴木雅也.md "wikilink")
11. **KinKi Kids Forever**
      - ※KinKi
        Kids參與演出的日本[森永製果廣告](../Page/森永製果.md "wikilink")『Dars』巧克力廣告曲。
      - 作曲：[Jonas Saeed](../Page/Jonas_Saeed.md "wikilink")
      - 作詞：Jonas Saeed
      - 日語歌詞：[尾上一平](../Page/尾上一平.md "wikilink")
12. **除了你誰都不愛**（****）
      - ※堂本光一參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『天使消失的城市』主題曲。第10張單曲。
      - 作曲：[周　　水](../Page/周水.md "wikilink")
      - 作詞：[周　　水](../Page/周水.md "wikilink")
      - 編曲：[重實 徹](../Page/重實徹.md "wikilink")
13. **Hey\!和**
      - 作曲：[永井真人](../Page/永井真人.md "wikilink")
      - 作詞：永井真人
      - 編曲：CHOKKAKU

[Category:近畿小子專輯](../Category/近畿小子專輯.md "wikilink")
[Category:2000年音樂專輯](../Category/2000年音樂專輯.md "wikilink")
[Category:2000年Oricon專輯週榜冠軍作品](../Category/2000年Oricon專輯週榜冠軍作品.md "wikilink")