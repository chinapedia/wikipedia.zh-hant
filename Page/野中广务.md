**野中广务**，[日本政治家](../Page/日本.md "wikilink")。

## 生平

出生於[京都府](../Page/京都府.md "wikilink")[船井郡](../Page/船井郡.md "wikilink")[園部村的](../Page/園部村.md "wikilink")[部落民](../Page/部落民.md "wikilink")（現稱[南丹市](../Page/南丹市.md "wikilink")），前[日本众议院议员](../Page/日本众议院.md "wikilink")，[日本自由民主党成员](../Page/日本自由民主党.md "wikilink")。曾任自民党干事长、冲绳开发厅长官、自治大臣、国家公安委员会委员长和小渊惠三内阁的[内阁官房长官](../Page/内阁官房长官.md "wikilink")。自民党内实力派人物，[小渊惠三病倒后](../Page/小渊惠三.md "wikilink")，一手策划[森喜朗上台继任首相](../Page/森喜朗.md "wikilink")，并在森喜朗任内粉碎自民党内[加藤派](../Page/加藤派.md "wikilink")、[山崎派的造反行动](../Page/山崎派.md "wikilink")。在[小泉纯一郎任首相期间](../Page/小泉纯一郎.md "wikilink")，因在自民党总裁选举中，推举候选人反对[小泉纯一郎未果而退出政坛](../Page/小泉纯一郎.md "wikilink")。

2013年6月3日，野中表示，日中邦交正常化时两国领导人曾就搁置[钓鱼岛问题达成共识](../Page/钓鱼岛.md "wikilink")\[1\]。

[2018年](../Page/2018年.md "wikilink")1月26日下午，於京都府内的醫院去世，享壽92歲\[2\]。

## 参考文献

<references>

\[3\]

</references>

[Category:日本自由民主党干事长](../Category/日本自由民主党干事长.md "wikilink")
[Category:日本内阁官房长官](../Category/日本内阁官房长官.md "wikilink")
[Category:日本冲绳开发厅长官](../Category/日本冲绳开发厅长官.md "wikilink")
[Category:日本自治大臣](../Category/日本自治大臣.md "wikilink")
[Category:日本国家公安委员会委员长](../Category/日本国家公安委员会委员长.md "wikilink")
[Category:小淵內閣閣僚](../Category/小淵內閣閣僚.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")
[Category:获称“中国人民的老朋友”的日本人](../Category/获称“中国人民的老朋友”的日本人.md "wikilink")
[Category:日本市町村長](../Category/日本市町村長.md "wikilink")
[Category:京都府選出日本眾議院議員](../Category/京都府選出日本眾議院議員.md "wikilink")

1.
2.
3.