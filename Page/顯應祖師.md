**顯應祖師**（），[宋朝](../Page/宋朝.md "wikilink")[河南](../Page/河南.md "wikilink")[僧人](../Page/僧人.md "wikilink")，俗姓**黃**，名**惠勝**，精通[佛法](../Page/佛法.md "wikilink")，在[福建](../Page/福建.md "wikilink")[永春](../Page/永春.md "wikilink")、[安溪兩地弘法](../Page/安溪.md "wikilink")，深得民心，[圓寂後被該兩地的百姓奉祀](../Page/圓寂.md "wikilink")，頗享[香火](../Page/香火.md "wikilink")。

## 简述

[清](../Page/清.md "wikilink")[乾隆時的](../Page/乾隆.md "wikilink")《永春州志》記載：
顯應祖師「幼不茹葷，預知禍福」，且還能為民祈雨：「歲大旱，語人曰：吾能致雨。禱經罷，忽曰：眾可急歸。未及半途，大雨滂沱」，求雨甚驗，故人稱「黃水車」。

顯應祖師至[福建省](../Page/福建省.md "wikilink")[永春結庵](../Page/永春.md "wikilink")，後離永春，改往[安溪縣泰山](../Page/安溪縣.md "wikilink")（大尖山）修道。[宋高宗](../Page/宋高宗.md "wikilink")[紹興四年](../Page/紹興.md "wikilink")（1134年）在安溪[湖頭建](../Page/湖頭.md "wikilink")「**泰山巖**」，修道，直至[圓寂](../Page/圓寂.md "wikilink")。

《[閩書](../Page/閩書.md "wikilink")》上說：昭應祖師（[清水祖師](../Page/清水祖師.md "wikilink")）建造[清水巖](../Page/清水巖.md "wikilink")，白米自動出現；[顯應祖師建造](../Page/顯應祖師.md "wikilink")[泰山巖](../Page/泰山巖.md "wikilink")，磚瓦自動出現；[惠應祖師建造泰湖巖](../Page/惠應祖師.md "wikilink")，木材自動出現。被視為三大[神蹟](../Page/神蹟.md "wikilink")。

顯應祖師生時即靈異，有德於民。[坐化後](../Page/坐化.md "wikilink")，許多百姓紛往瞻仰，且集資增修廟宇，尊為神明。後受詔封為「**顯應普濟大師**」，俗稱其「**顯應祖師**」。亦有人稱「**泰山祖師**」、「**泰山顯應大師**」者。由於顯應祖師在永春弘法、安溪修道至於仙逝，故永春、安溪人也頗為崇信祭拜，如清[康熙時](../Page/康熙.md "wikilink")[大學士](../Page/大學士.md "wikilink")[李光地家族即是](../Page/李光地.md "wikilink")。

顯應祖師在[臺灣](../Page/臺灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[泰山區有二廟](../Page/泰山區_\(新北市\).md "wikilink")，曰[頂泰山巖](../Page/頂泰山巖.md "wikilink")、[下泰山巖](../Page/下泰山巖.md "wikilink")。「泰山區」之名，亦由「泰山巖」而來。
另五股區龍鳳巖，林口區頂福巖亦有名气。

顯應祖師與[泉州安溪最負盛名的佛教高僧](../Page/泉州.md "wikilink")**[清水祖師](../Page/清水祖師.md "wikilink")**常被誤會為同一人，但考其出身、事蹟、行誼皆不同，實為兩人，不可混淆。

## 組合

「[法主公](../Page/法主公.md "wikilink")」意為[法術高強的](../Page/法術.md "wikilink")[神明](../Page/神明.md "wikilink")，各地信奉的法主公組合均不同，大多有「張法主」與「蕭法主」，在[永春](../Page/永春.md "wikilink")、[安溪一帶](../Page/安溪.md "wikilink")，顯應祖師亦被視為法主公之一。

在[德化的](../Page/德化.md "wikilink")「法主公組合」是[張慈觀](../Page/張慈觀.md "wikilink")、輔天真君[蕭朗瑞](../Page/蕭朗瑞.md "wikilink")、弼天真君[章朗慶](../Page/章朗慶.md "wikilink")（一說姓洪）等[結義三兄弟](../Page/結義.md "wikilink")，是為「法主三公」。在[永春](../Page/永春.md "wikilink")、[安溪一帶亦是](../Page/安溪.md "wikilink")「張、蕭、章」；亦有人奉祀「張、蕭、黃」，加入了[黃法師](../Page/顯應祖師.md "wikilink")（顯應祖師）。[漳平則是](../Page/漳平.md "wikilink")「張、蕭、吳」，張、蕭二法主之外，加入[吳真人](../Page/保生大帝.md "wikilink")（保生大帝），或是奉祀「張、蕭、吳、黃」。[福州是](../Page/福州.md "wikilink")「張、蕭、連」，即[張慈觀](../Page/張慈觀.md "wikilink")、[蕭朗瑞](../Page/蕭朗瑞.md "wikilink")、[連光陽三位真人](../Page/連光陽.md "wikilink")。[潮州的](../Page/潮州.md "wikilink")[客家人則是](../Page/客家人.md "wikilink")「張、蕭、劉、連」四聖，即[張慈觀](../Page/張慈觀.md "wikilink")、[蕭朗瑞](../Page/蕭朗瑞.md "wikilink")、[連光陽](../Page/連光陽.md "wikilink")、[劉志達四聖](../Page/劉志達.md "wikilink")，四聖也就是[五營神將中的東南西北四營的主帥](../Page/五營神將.md "wikilink")。

## 參考資料

  - [永春小天下
    閩台祖師源](http://www.qzwb.com/gb/content/2004-02/14/content_1140648.htm)

## 參看

  - [安溪諸聖圖](../Page/安溪諸聖圖.md "wikilink")
  - [應惠靈天](../Page/應惠靈天.md "wikilink")
  - [高僧信仰](../Page/高僧信仰.md "wikilink")
  - [清水祖師](../Page/清水祖師.md "wikilink")
  - [三平祖師](../Page/三平祖師.md "wikilink")
  - [三代祖師](../Page/三代祖師.md "wikilink")
  - [普庵祖師](../Page/普庵祖師.md "wikilink")
  - [扣冰祖師](../Page/扣冰祖師.md "wikilink")
  - [定光祖師](../Page/定光祖師.md "wikilink")
  - [泰湖祖師](../Page/泰湖祖師.md "wikilink")
  - [慚愧祖師](../Page/慚愧祖師.md "wikilink")

[X显](../Category/宋朝僧人.md "wikilink") [X显](../Category/禪僧.md "wikilink")
[X显](../Category/河南人.md "wikilink")
[X显](../Category/中國民間信仰.md "wikilink")
[Category:臨濟宗僧人](../Category/臨濟宗僧人.md "wikilink")