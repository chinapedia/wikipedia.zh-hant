**Graphisoft**
公司原由匈牙利一羣建築師與數學家共同開發而來，慢慢擴展至今的規模，現已有二十多萬的使用者，可以說是BIM（[建築資訊模型](../Page/建築資訊模型.md "wikilink")）的始祖之一。

## 旗艦產品

[虛擬建築](../Page/虛擬建築.md "wikilink")<sup>TM</sup> solutions

  - [ArchiCAD](../Page/ArchiCAD.md "wikilink")
  - [MaxonForm](../Page/MaxonForm.md "wikilink")

[虛擬營造](../Page/虛擬營造.md "wikilink")<sup>TM</sup> solutions

  - [Graphisoft
    Constructor](../Page/Graphisoft_Constructor.md "wikilink")
  - [Graphisoft Estimator](../Page/Graphisoft_Estimator.md "wikilink")
  - [Graphisoft Control](../Page/Graphisoft_Control.md "wikilink")
  - [Graphisoft Change
    Manager](../Page/Graphisoft_Change_Manager.md "wikilink")

## 官方網站

  - Graphisoft R\&D Zrt.
    [布達佩斯](../Page/布達佩斯.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")（總部）
  - [德國Graphisoft公司](http://www.graphisoft.de/)，[慕尼黑](../Page/慕尼黑.md "wikilink")，[德國](../Page/德國.md "wikilink")
  - [美國Graphisoft公司](http://www.graphisoftus.com/)，[波士頓](../Page/波士頓.md "wikilink")，[美國](../Page/美國.md "wikilink")
  - [日本Graphisoft公司](http://www.graphisoft.co.jp/)，[東京](../Page/東京.md "wikilink")，[日本](../Page/日本.md "wikilink")
  - Graphisoft UK Ltd.,
    [Woking](../Page/Woking.md "wikilink")，[英國](../Page/英國.md "wikilink")
  - [Archicad España,
    S.A.](http://www.archicad.es/)，[馬德里](../Page/馬德里.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")
  - Graphisoft Finland Oy,
    [赫爾辛基](../Page/赫爾辛基.md "wikilink")，[芬蘭](../Page/芬蘭.md "wikilink")
  - [澳洲Graphisoft公司](http://www.graphisoft.com.au/)，[澳洲](../Page/澳洲.md "wikilink")

## 地方經銷商

  - 台灣官方網站：[龍庭資訊首頁](http://www.academicd.com/index.htm)
  - 加拿大官方網站：[GSCNE首頁](https://web.archive.org/web/20080312054511/http://www.gscne.com/)

[Category:軟體公司](../Category/軟體公司.md "wikilink")
[Category:歐洲股份公司](../Category/歐洲股份公司.md "wikilink")