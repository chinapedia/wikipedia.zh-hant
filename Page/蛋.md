[Anatomy_of_an_egg.svg](https://zh.wikipedia.org/wiki/File:Anatomy_of_an_egg.svg "fig:Anatomy_of_an_egg.svg")剖面圖。从上到下依次是：
1\.[蛋壳](../Page/#蛋壳.md "wikilink")
2\.[外壳膜](../Page/#卵壳膜与气室.md "wikilink")
3\.[内壳膜](../Page/#卵壳膜与气室.md "wikilink")
4\.[上繫帶](../Page/#系带.md "wikilink")
5\.[稀蛋白](../Page/#蛋白.md "wikilink")
6\.[浓蛋白](../Page/#蛋白.md "wikilink")
7\.[卵黄膜](../Page/#卵黄膜.md "wikilink")
8\.[核筋膜](../Page/#蛋黄.md "wikilink")
9\.[胚盘](../Page/#胚盘.md "wikilink")
10\.[深色蛋黃](../Page/#蛋黃.md "wikilink")
11\.[淺色蛋黃](../Page/#蛋黃.md "wikilink")
12\.[內部蛋白](../Page/#蛋白.md "wikilink")
13\.[下繫帶](../Page/#系带.md "wikilink")
14\.[气室](../Page/#卵壳膜与气室.md "wikilink")
15\.[蛋殼緣](../Page/#蛋壳.md "wikilink")\]\]

**蛋**，是[鳥類](../Page/鳥類.md "wikilink")、[爬蟲類和](../Page/爬蟲類.md "wikilink")[兩棲動物所生](../Page/兩棲動物.md "wikilink")、帶有硬殼的[卵](../Page/卵.md "wikilink")，受精之後可孵出小動物，為人類食用已有幾千年歷史。蛋由[蛋殼保護](../Page/蛋殼.md "wikilink")，而當中的[蛋白和](../Page/蛋白.md "wikilink")[蛋黃被各種薄膜包裹](../Page/蛋黃.md "wikilink")。

蛋黃和全蛋存儲大量的[蛋白質](../Page/蛋白質.md "wikilink")、[膽鹼和其他](../Page/膽鹼.md "wikilink")[營養素](../Page/營養素.md "wikilink")。故此，[美國農業部將蛋在](../Page/美國農業部.md "wikilink")[飲食金字塔中界定為](../Page/飲食金字塔.md "wikilink")[肉類](../Page/肉類.md "wikilink")。

最常為[人類食用的蛋是](../Page/人類.md "wikilink")[雞蛋](../Page/雞蛋.md "wikilink")\[1\]，其他較常作食用的蛋有[鴨蛋](../Page/鴨蛋.md "wikilink")、[鵪鶉蛋](../Page/鵪鶉蛋.md "wikilink")、[鵝蛋等](../Page/鵝蛋.md "wikilink")。

## 结构

蛋與其他的卵最大的差異是它蛋有殼保護著，蛋殼有「硬殼」及「革殼」兩種。鳥類的蛋屬於硬殼的，容易破裂，目前最大的蛋是[鴕鳥蛋](../Page/鴕鳥.md "wikilink")，最小的蛋是[蜂鳥蛋](../Page/蜂鳥.md "wikilink")。然[爬蟲類的蛋如](../Page/爬蟲類.md "wikilink")[蜥蜴或蛇等](../Page/蜥蜴.md "wikilink")，蛋殼像皮革有彈性，稍為凹下去也沒關係。

在適當的[溫度下](../Page/溫度.md "wikilink")，受精的蛋會在一定時候孵化，幼體用[口部上方的](../Page/口.md "wikilink")[角質物鑿開蛋殼破殼而出](../Page/角質.md "wikilink")。鳥類的蛋大多呈梨型的，也就是說一頭較尖，另一頭較大，這是為了讓親鳥孵蛋的時候，在翻蛋使整個蛋受熱平均，不至於讓蛋滾出鳥巢而演化出來的形狀，蛋會朝向尖的那一端滾動。

蛋在大頭的那端有個[氣室](../Page/氣室.md "wikilink")，功能是[氧氣交換以利於](../Page/氧氣.md "wikilink")[呼吸作用](../Page/呼吸作用.md "wikilink")。所以蛋的表面也有數不清的氣孔，若經過水洗就無法孵化了。[蛋白像羊水一樣有保護及穩定的功效](../Page/蛋白.md "wikilink")，[蛋黃則是供及養分給胚胎成長](../Page/蛋黃.md "wikilink")。

### 蛋殼

[蛋壳又叫卵壳](../Page/蛋壳.md "wikilink")，完整的蛋殼呈鸡蛋圆，一头小，約佔全蛋[體積的](../Page/體積.md "wikilink")11%\~11.5%。蛋殼又可分為殼上膜、殼下皮、氣室。

蛋殼的主要成分是[碳酸鈣](../Page/碳酸鈣.md "wikilink")，約佔整個蛋殼重量的91％～95％。其含鈣的成分與[珍珠](../Page/珍珠.md "wikilink")、[牡蠣](../Page/牡蠣.md "wikilink")、[牛骨](../Page/牛骨.md "wikilink")、[小魚乾相同](../Page/小魚乾.md "wikilink")，是[鈣質的良好來源](../Page/鈣質.md "wikilink")。此外，蛋殼中尚含約佔有5％的[碳酸鎂](../Page/碳酸鎂.md "wikilink")，以及2％的[磷酸鈣和](../Page/磷酸鈣.md "wikilink")[膠質](../Page/膠質.md "wikilink")。

### 卵壳膜与气室

卵壳膜分为外壳膜和内壳膜，中间夹着气室。

殼上膜（外壳膜），即在蛋殼外面，一層不透明、無結構的膜；作用是防止蛋的水份蒸發。殼下皮（内壳膜）是在蛋殼裡面的薄膜，共二層，[空氣能自由通過此膜](../Page/空氣.md "wikilink")。

[气室是二層殼下皮之間的空隙](../Page/气室.md "wikilink")，储存[氧气等空气](../Page/氧气.md "wikilink")。若蛋內[氣體遺失](../Page/氣體.md "wikilink")，氣室會不斷地增大。

### 蛋白

[蛋白又叫蛋清](../Page/蛋白.md "wikilink")、卵白，是殼下皮內半流動的膠狀物質，體積約佔全蛋的57%～58.5%。蛋白中約含[蛋白質](../Page/蛋白質.md "wikilink")12％，主要是蛋白。蛋白中還含有一定量的[核黃素](../Page/核黃素.md "wikilink")、[尼克酸](../Page/尼克酸.md "wikilink")、[生物素和](../Page/生物素.md "wikilink")[鈣](../Page/鈣.md "wikilink")、[磷](../Page/磷.md "wikilink")、[鐵等](../Page/鐵.md "wikilink")[物質](../Page/物質.md "wikilink")。蛋白凝結的溫度大約在80℃\[2\]。

蛋白又分濃蛋白和稀蛋白。濃蛋白是靠近蛋黃的部分蛋白，濃度較高。稀蛋白是靠近蛋殼的部分蛋白，濃度較稀。

### 蛋黃

[蛋黃又叫卵黄](../Page/蛋黃.md "wikilink")，多居於蛋白的中央，由[繫帶懸於兩極](../Page/繫帶.md "wikilink")，相当于[卵细胞的](../Page/卵细胞.md "wikilink")[细胞质](../Page/细胞质.md "wikilink")。蛋黃體積約全蛋的30％～32％，主要組成物質為[卵黃磷蛋白](../Page/卵黃磷蛋白.md "wikilink")，另外[脂肪含量為](../Page/脂肪.md "wikilink")28.2％，脂肪多屬於[磷脂類中一的](../Page/磷脂.md "wikilink")[卵磷脂](../Page/卵磷脂.md "wikilink")。對人類的營養方面，蛋黃含有豐富的[維生素A和](../Page/維生素A.md "wikilink")[維生素D](../Page/維生素D.md "wikilink")，且含有較高的鐵、[磷](../Page/磷.md "wikilink")、[硫和鈣等礦物質](../Page/硫.md "wikilink")。蛋黃內有[胚盘](../Page/胚盘.md "wikilink")。若雞蛋有[受精](../Page/受精.md "wikilink")，約經過21天會孵出小雞。蛋黃凝結的溫度大約在70℃\[3\]。

胚盘== 卵黄膜 ===
卵黄膜包在蛋黄的周围，有保护蛋黄的作用。敲开一个生蛋，会发现里面的蛋黄成凝固状，将卵黄膜戳破，即可发现卵黄从戳破的位置流出。

### 胚盘

胚盘是雏鸡发育的部位，含有遗传物质，是[卵细胞的](../Page/卵细胞.md "wikilink")[细胞核](../Page/细胞核.md "wikilink")。

### 繫带

繫带有固定[卵黃的作用](../Page/卵黃.md "wikilink")。

## 食用相關事項

[Chicken_egg02_binovular.jpg](https://zh.wikipedia.org/wiki/File:Chicken_egg02_binovular.jpg "fig:Chicken_egg02_binovular.jpg")
[Chicken_Egg_without_Eggshell_5859.jpg](https://zh.wikipedia.org/wiki/File:Chicken_Egg_without_Eggshell_5859.jpg "fig:Chicken_Egg_without_Eggshell_5859.jpg")
蛋黃含有較高的[膽固醇](../Page/膽固醇.md "wikilink")，但也含有較高的卵磷脂、[類胡羅蔔素及](../Page/類胡羅蔔素.md "wikilink")[葉黃素](../Page/葉黃素.md "wikilink")（都可以防止[動脈硬化](../Page/動脈硬化.md "wikilink")），蛋黃的[飽和脂肪酸佔總脂肪的比例也只有](../Page/飽和脂肪酸.md "wikilink")27%；因此對[心血管疾病的影響有所爭論](../Page/心血管疾病.md "wikilink")，有些研究會認為一天一顆雞蛋（蛋黃）對預防心血管疾病無害甚至有利、甚至少數研究顯示一天兩顆對心血管疾病患者也是沒問題的、有醫師認為健康的蛋奶素者甚至可以一天吃三顆雞蛋蛋黃；主要理由在於人體內的膽固醇主要是肝臟製造的，而當吃了多量的膽固醇，肝臟就會自動減少膽固醇的製造，因此不必太擔心食物內的膽固醇，而飽和脂肪的過量攝取才會刺激肝臟製造出超量的膽固醇。

除了高膽固醇的疑慮外，雞蛋有良好且均衡的營養成分。另含有膽鹼、維生素A、維生素D、[維生素E](../Page/維生素E.md "wikilink")、[維生素B群等營養成分](../Page/維生素B群.md "wikilink")。而近來在各國禁止使用[肉骨粉飼養雞隻後](../Page/肉骨粉.md "wikilink")，雞蛋的膽固醇含量开始下降。如果使用高[ω-3脂肪酸飼料餵食母雞](../Page/ω-3脂肪酸.md "wikilink")，雞蛋裡面會有較高量的ω-3脂肪酸，ω-3脂肪酸是現代飲食容易缺乏的脂肪酸，也被認為是可以減少[心血管疾病](../Page/心血管疾病.md "wikilink")、[糖尿病](../Page/糖尿病.md "wikilink")、[癌症](../Page/癌症.md "wikilink")、[憂鬱症及](../Page/憂鬱症.md "wikilink")[不孕風險的脂肪酸](../Page/不孕.md "wikilink")（魚油膠囊就是為了補充ω-3脂肪酸）。

雞蛋另一個健康顧慮是很容易造成[慢性食物過敏](../Page/慢性食物過敏.md "wikilink")，慢性食物過敏是一種症狀不明顯、難以察覺的食物過敏。[蛋奶素素食主義者同樣可能食用蛋與](../Page/蛋奶素素食主義者.md "wikilink")[奶類](../Page/奶.md "wikilink")；而[纯素食者除非额外补充](../Page/纯素食主义.md "wikilink")，否则很容易缺乏维生素B<sub>12</sub>（主要来自于动物食品）\[4\]。而一些研究認為，蛋奶素比純素更健康，因為[纯素會造成膽固醇過低](../Page/纯素食主义.md "wikilink")，增加出血性中風風險。

荷蘭萊登大學一項研究顯示含色氨酸（雞蛋、朱古力及紅肉等食物）可增互信。該研究讓參加者先喝下含色氨酸的橙汁，然後他們二人一組玩信任遊戲，將金錢交給其中一人，讓他決定交多少錢給同伴，然後同伴再將部分金錢交回第一人，並重覆數次。\[5\]

## 參見

  - [假雞蛋](../Page/假雞蛋.md "wikilink")
  - [雞](../Page/雞.md "wikilink")
  - [卵](../Page/卵.md "wikilink")
  - [人造蛋](../Page/人造蛋.md "wikilink")
  - [鸟蛋](../Page/鸟蛋.md "wikilink")
  - [鸡蛋](../Page/鸡蛋.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[蛋](../Category/蛋.md "wikilink")

1.

2.

3.
4.

5.  [研究指食蛋等色氨酸食品可增互信](http://news.on.cc/cnt/world/20131027/bkn-20131027143139073-1027_00902_001.html)