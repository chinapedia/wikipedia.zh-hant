***N*,*N*-二甲基苯胺**（*N*,*N*-dimethylaniline），有时简称**二甲基苯胺**，由[苯胺的两个](../Page/苯胺.md "wikilink")[氨基氢被两个](../Page/氨基.md "wikilink")[甲基取代后形成](../Page/甲基.md "wikilink")。它是油状液体，分子式为C<sub>8</sub>H<sub>11</sub>N，用作[溶剂](../Page/溶剂.md "wikilink")，也用于[特屈儿](../Page/特屈儿.md "wikilink")[炸药的合成](../Page/炸药.md "wikilink")。

## 参见

  - [二甲基苯胺](../Page/二甲基苯胺.md "wikilink")—消歧义页

## 外部链接

  - <https://web.archive.org/web/20070829095213/http://www.compositesaustralia.com.au/dma.htm>

[Category:芳香胺](../Category/芳香胺.md "wikilink")