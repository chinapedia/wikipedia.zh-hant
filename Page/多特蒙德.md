**多特蒙德**（），[德國西部](../Page/德國.md "wikilink")[北萊茵-威斯特法倫州的重要城市](../Page/北萊茵-威斯特法倫.md "wikilink")，位于[魯爾區東部](../Page/魯爾區.md "wikilink")。截至2005年6月20日，多特蒙德人口有587,830人，是德国第七大城市。

[魯爾河從市內南面流過](../Page/魯爾河.md "wikilink")，小河埃姆斯查河流過多特蒙德市區。[多蒙特-埃姆斯河運河同樣流入](../Page/多蒙特-埃姆斯河運河.md "wikilink")[多蒙特港](../Page/多蒙特港.md "wikilink")，而[多蒙特港是德國最大的運河海港](../Page/多蒙特港.md "wikilink")，連接多蒙特與[北海](../Page/北海_\(大西洋\).md "wikilink")。

多特蒙德是[北萊茵-威斯特法倫州的](../Page/北萊茵-威斯特法倫.md "wikilink")"綠色都市"。市內接近有一半地區是佈滿水道、林地、耕地和綠色地帶和寬敞的公園例如威斯法倫公園和龍白克公園。根據歷史所見，一百年前在市內範圍大規模的採煤、煉炭、煉鋼之後，現在的市容跟以前比較是一個頗大的對比。

多特蒙德是主場位於[西格納伊度納公園威斯法倫體育場作賽](../Page/西格納伊度納公園.md "wikilink")，[北萊茵-威斯特法倫的](../Page/北萊茵-威斯特法倫.md "wikilink")[波魯西亞多特蒙德足球俱樂部的根據地](../Page/多特蒙德足球俱樂部.md "wikilink")。[西格納伊度納公園於](../Page/西格納伊度納公園.md "wikilink")1974年開幕，容量包含座席及立席為81,264人，是全德國最大的球場。球場過往已舉辦過[1974年世界盃足球賽和](../Page/1974年世界盃足球賽.md "wikilink")[2006年世界盃足球賽一些賽事](../Page/2006年世界盃足球賽.md "wikilink")。

## 交通

[Dortmund_Panorama.jpg](https://zh.wikipedia.org/wiki/File:Dortmund_Panorama.jpg "fig:Dortmund_Panorama.jpg")

[多特蒙德機場是一個中轉站](../Page/多特蒙德機場.md "wikilink")，但在多特蒙德市中心以東十三公里處在[霍茨威肯德](../Page/霍茨威肯德.md "wikilink")(Holzwickede)市的範圍內正有一個快速發展的機場。

中央車站是德國第三大的交通匯合處。

多特蒙德港是德國最大的運河海港，是全歐洲最大的;甚至大於擁有全球最大的內陸港[杜伊斯堡的運河海港](../Page/杜伊斯堡.md "wikilink")。

多特蒙德亦是歐洲及德國高速公路的交通匯合處。40號高速公路跟隨古時漢薩同盟交易路線去將多特蒙德與[魯爾區的其他大都市連接](../Page/魯爾區.md "wikilink")。德國國內更偏僻的地方是由1號高速公路和2號高速公路連接，並在多特蒙德的東面和北面經過，在四葉形道路交叉點交匯向東北方轉往多特蒙德。公共交通方面，多特蒙德擁有大規模的[輕鐵](../Page/輕軌運輸.md "wikilink")、有軌電車和巴士系統。另一條額外的輕鐵路線現正於市中心內建造中。

[多特蒙德大學的](../Page/多特蒙德大學.md "wikilink")[懸掛鐵路](../Page/懸掛鐵路.md "wikilink")(H-Bahn)是一條特別建造的獨特懸掛單軌鐵路來回運送欲橫越大學校園的乘客，並受到研究實驗室和其他高科技公司採用。

## 历史

[View_of_the_Town_of_Dortmund_in_the_Sixteenth_Century_From_an_Engraving_on_Copper_in_P_Bertius_Theatrum_Geographicum.png](https://zh.wikipedia.org/wiki/File:View_of_the_Town_of_Dortmund_in_the_Sixteenth_Century_From_an_Engraving_on_Copper_in_P_Bertius_Theatrum_Geographicum.png "fig:View_of_the_Town_of_Dortmund_in_the_Sixteenth_Century_From_an_Engraving_on_Copper_in_P_Bertius_Theatrum_Geographicum.png")

多特蒙德歷史起源於公元880年，在官方文件中首次以*Throtmanni*\[1\]之名稱提及。當時此地是小村一條。1152年神聖羅馬帝國皇帝[巴巴羅薩來到此地](../Page/腓特烈一世_\(神聖羅馬帝國\).md "wikilink")。兩年內短時間，多特蒙德是[巴巴羅薩的居住地方](../Page/腓特烈一世_\(神聖羅馬帝國\).md "wikilink")，之後多特蒙德成為帝國最強大的城市之一。在十三世紀多特蒙德加入了[漢薩同盟](../Page/漢薩同盟.md "wikilink")。1220年，多特蒙德得到了[帝國自由城市的名銜](../Page/帝國自由城市.md "wikilink")，換言之多特蒙德直轄於神聖羅馬帝國皇帝。1320年之後，這個富裕的貿易城市開始以*Dorpmunde*的名稱出現。市名的詞形變化是不確定的。

多特蒙德於1803年失去了[帝国自由城市的名銜](../Page/帝国自由城市.md "wikilink")。多特蒙德在[拿破崙戰爭之後併入](../Page/拿破崙戰爭.md "wikilink")[普魯士的](../Page/普魯士.md "wikilink")[西發里亞省之中](../Page/西發里亞省.md "wikilink")，並成為煤炭、鋼鐵和[啤酒的中心](../Page/多蒙特啤酒.md "wikilink")。.

當[第三帝國統治的時候](../Page/第三帝國.md "wikilink")，多特蒙德是[Aplerbeck醫院的所在地](../Page/Aplerbeck醫院.md "wikilink")，目的是"照料"精神病患者及傷殘人士。這醫院位於[魯爾區的中心](../Page/魯爾區.md "wikilink")，多特蒙德與其他鄰近城市一樣是盟軍轟炸的目標。在[第二次世界大戰時市內三分之二的民居被毀](../Page/第二次世界大戰.md "wikilink")。

現時的多特蒙德是高科技產業的中心。多特蒙德也是[北萊茵-威斯特法倫其中一個綠色都市](../Page/北萊茵-威斯特法倫.md "wikilink")，二次大戰後市內所重建的地方興建了廣闊的公園和花園。另外，世界上其中一個強勁的棋藝聯賽，[多蒙特棋藝聯賽](../Page/多蒙特棋藝聯賽.md "wikilink")，每年都在多特蒙德舉行。

## 建築物

  - [多蒙特電視塔](../Page/多蒙特電視塔.md "wikilink") -
    德國第一座的電視塔，並建有全球第一間[旋轉餐廳](../Page/旋轉餐廳.md "wikilink")。電視塔可讓人有機會在140米高的地方玩高空彈跳，但當2003年初發生致命悲劇後活動已經停止。
  - [西格納伊度納公園](../Page/西格納伊度納公園.md "wikilink") -
    [多蒙特足球會的主場](../Page/多蒙特足球會.md "wikilink")，即為以前的威斯法倫體育場。鄰近它的是室內體育競技場(Westfalenhalle)，是一個大型會議中心，在1952年起曾舉辦大型會議、展銷會、溜冰比賽和其他大型活動。
  - [圣玛琳教堂](../Page/圣玛琳教堂.md "wikilink") -
    圣玛琳教堂坐落在多特蒙德的主城，于12世纪建成，其建筑形式结合了罗马式和哥特式的建筑元素。教堂内珍藏有很多中世纪的艺术宝藏，如[康拉德·冯·索斯特的](../Page/康拉德·冯·索斯特.md "wikilink")[玛丽亚祭坛](../Page/玛丽亚祭坛.md "wikilink")。

## 友好城市

  - [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")[法国](../Page/法国.md "wikilink")[亚眠](../Page/亚眠.md "wikilink")
  - [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg")[英国](../Page/英国.md "wikilink")[利兹](../Page/利兹.md "wikilink")
  - [Flag_of_Russia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Russia.svg "fig:Flag_of_Russia.svg")[俄罗斯](../Page/俄罗斯.md "wikilink")[顿河畔罗斯托夫](../Page/顿河畔罗斯托夫.md "wikilink")
  - [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")[美国](../Page/美国.md "wikilink")[布法罗
    (纽约州)](../Page/布法罗_\(纽约州\).md "wikilink")
  - [Flag_of_Israel.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Israel.svg "fig:Flag_of_Israel.svg")[以色列](../Page/以色列.md "wikilink")[内坦亚](../Page/内坦亚.md "wikilink")
  - [Flag_of_Serbia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Serbia.svg "fig:Flag_of_Serbia.svg")[塞尔维亚](../Page/塞尔维亚.md "wikilink")[诺维萨德](../Page/诺维萨德.md "wikilink")
  - [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg")[中国](../Page/中国.md "wikilink")[西安](../Page/西安.md "wikilink")

## 外部链接

  - [多特蒙德市德文官方網站](https://web.archive.org/web/20061007142330/http://www1.dortmund.de/home/template0.jsp?ecode=home&eid=0&elimit=8&ncode=home&nid=0&nlimit=8&smi=1.0&tncode=topnews.home&tnid=0)
  - [多特蒙德市英文官方网站](https://web.archive.org/web/20060705235223/http://city.dortmund.de/city/project/assets/template1.jsp?ncode=englisch&nid=0&ntitle=News+from+Dortmund&smi=1.0&tid=51668)

## 参考

[D](../Category/北莱茵-威斯特法伦州市镇.md "wikilink")
[D](../Category/汉萨同盟.md "wikilink")
[Category:帝國自由城市](../Category/帝國自由城市.md "wikilink")
[Category:882年歐洲建立](../Category/882年歐洲建立.md "wikilink")

1.  Rudolf Kötzschke (Hrsg.): Die Urbare der Abtei Werden a. d. Ruhr (=
    Publikationen der Gesellschaft für rheinische Geschichtskunde XX:
    Rheinische Urbare). Bd. 2: A. Die Urbare vom 9.-13. Jahrhundert.
    Hrsg. von Rudolf Kötzschke, Bonn 1908, Nachdruck Düsseldorf 1978,
    Bd. 3: B. Lagerbücher, Hebe- und Zinsregister vom 14. bis ins 17.
    Jahrhundert, Bonn 1908, Nachdruck Düsseldorf 1978, Bd. 4,I:
    Einleitung und Register. I. Namenregister. Hrsg. von Fritz Körholz,
    Düsseldorf 1978, Bd. 4,II: Einleitung, Kapitel IV: Die
    Wirtschaftsverfassung und Verwaltung der Großgrundherrschaft Werden.
    Sachregister. Hrsg. von Rudolf Kötzschke, Bonn 1958