**油尖區議會**於1982年依照[地方行政計劃設立](../Page/地方行政計劃.md "wikilink")。當時稱**油麻地區議會**，到了1988年才改稱油尖區。[油尖區範圍包括](../Page/油尖區.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")、[官涌](../Page/官涌.md "wikilink")、[佐敦](../Page/佐敦.md "wikilink")、[油麻地](../Page/油麻地.md "wikilink")、[京士柏和新填海區](../Page/京士柏.md "wikilink")。由於油尖區面積細小，加上[香港政府為了節省行政資源](../Page/香港殖民地時期#香港政府.md "wikilink")，於1994年9月30日將油尖區議會和[旺角區議會合併成新的](../Page/旺角區議會.md "wikilink")[油尖旺區議會](../Page/油尖旺區議會.md "wikilink")。

## 區徽

與現在的[油尖旺區議會區徽類似](../Page/油尖旺區議會.md "wikilink")。

## 議員列表（1989年-1991年）

| 選區                                | 姓名                                 | 所屬政黨                             | 備註                               |
| --------------------------------- | ---------------------------------- | -------------------------------- | -------------------------------- |
| [尖沙咀](../Page/尖沙咀.md "wikilink")  |                                    | [李詠娜](../Page/李詠娜.md "wikilink") | [革新會](../Page/革新會.md "wikilink") |
|                                   | [加利](../Page/加利.md "wikilink")     | 獨立                               |                                  |
| [油麻地東](../Page/油麻地.md "wikilink") |                                    | [何德華](../Page/何德華.md "wikilink") | 獨立                               |
| [佐敦](../Page/佐敦.md "wikilink")    |                                    | [伍建新](../Page/伍建新.md "wikilink") | [自民聯](../Page/自民聯.md "wikilink") |
|                                   | [李景華](../Page/李景華.md "wikilink")   | [革新會](../Page/革新會.md "wikilink") |                                  |
| [油麻地北](../Page/油麻地.md "wikilink") |                                    | [黃國桐](../Page/黃國桐.md "wikilink") | 獨立                               |
|                                   | [黎榮德](../Page/黎榮德.md "wikilink")   | 獨立                               |                                  |
| [渡船角](../Page/文華新村.md "wikilink") |                                    | [葉國忠](../Page/葉國忠.md "wikilink") | [工聯會](../Page/工聯會.md "wikilink") |
| 市政局當然議員                           |                                    | 取消                               |                                  |
|                                   | 取消                                 |                                  | \[1\]                            |
| 委任議員                              |                                    | [蘇淑賢](../Page/蘇淑賢.md "wikilink") | 獨立                               |
|                                   | [葉華](../Page/葉華.md "wikilink")（主席） | 獨立                               |                                  |
|                                   | [吳萬強](../Page/吳萬強.md "wikilink")   | 獨立                               |                                  |
|                                   | [趙汝熙](../Page/趙汝熙.md "wikilink")   | 獨立                               |                                  |

## 議員列表（1986年-1988年）

| 選區                                | 姓名                                 | 所屬政黨                               | 備註                               |
| --------------------------------- | ---------------------------------- | ---------------------------------- | -------------------------------- |
| [尖沙咀](../Page/尖沙咀.md "wikilink")  |                                    | [陳偉宏](../Page/陳偉宏.md "wikilink")   | 獨立                               |
|                                   | [蘇仲平](../Page/蘇仲平.md "wikilink")   | [公民協會](../Page/公民協會.md "wikilink") |                                  |
| [油麻地東](../Page/油麻地.md "wikilink") |                                    | [陸順甜](../Page/陸順甜.md "wikilink")   | [匯點](../Page/匯點.md "wikilink")   |
| [佐敦](../Page/佐敦.md "wikilink")    |                                    | [伍建新](../Page/伍建新.md "wikilink")   | [勵進會](../Page/勵進會.md "wikilink") |
|                                   | [韋建邦](../Page/韋建邦.md "wikilink")   | [革新會](../Page/革新會.md "wikilink")   |                                  |
| [油麻地北](../Page/油麻地.md "wikilink") |                                    | [關妙美](../Page/關妙美.md "wikilink")   | [革新會](../Page/革新會.md "wikilink") |
|                                   | [黎榮德](../Page/黎榮德.md "wikilink")   | 獨立                                 |                                  |
| [渡船角](../Page/文華新村.md "wikilink") |                                    | [葉國忠](../Page/葉國忠.md "wikilink")   | [工聯會](../Page/工聯會.md "wikilink") |
| 市政局當然議員                           |                                    | [關廉豪](../Page/關廉豪.md "wikilink")   | [革新會](../Page/革新會.md "wikilink") |
|                                   | [郭志權](../Page/郭志權.md "wikilink")   | [勵進會](../Page/勵進會.md "wikilink")   |                                  |
| 委任議員                              |                                    | [陳袁燕紅](../Page/陳袁燕紅.md "wikilink") | 獨立                               |
|                                   | [葉華](../Page/葉華.md "wikilink")（主席） | 獨立                                 |                                  |
|                                   | [關銘煊](../Page/關銘煊.md "wikilink")   | 獨立                                 |                                  |
|                                   | [趙汝熙](../Page/趙汝熙.md "wikilink")   | 獨立                                 |                                  |

## 議員列表（1982年-1985年）

| 選區                                | 姓名                               | 所屬政黨                               | 備註                                 |
| --------------------------------- | -------------------------------- | ---------------------------------- | ---------------------------------- |
| [尖沙咀](../Page/尖沙咀.md "wikilink")  |                                  | [陳偉宏](../Page/陳偉宏.md "wikilink")   | 獨立                                 |
| [油麻地東](../Page/油麻地.md "wikilink") |                                  | [冼灼芬](../Page/冼灼芬.md "wikilink")   | 獨立                                 |
| [油麻地南](../Page/油麻地.md "wikilink") |                                  | [李烈文](../Page/李烈文.md "wikilink")   | [教協](../Page/教協.md "wikilink")     |
| [油麻地北](../Page/油麻地.md "wikilink") |                                  | [陳蔭堂](../Page/陳蔭堂.md "wikilink")   | [公民協會](../Page/公民協會.md "wikilink") |
| 市政局當然議員                           |                                  | [黃夢花](../Page/黃夢花.md "wikilink")   | 獨立                                 |
|                                   | [郭志權](../Page/郭志權.md "wikilink") | 獨立                                 |                                    |
| 委任議員                              |                                  | [陳袁燕紅](../Page/陳袁燕紅.md "wikilink") | 獨立                                 |
|                                   | [葉華](../Page/葉華.md "wikilink")   | 獨立                                 |                                    |
|                                   | [黎時煖](../Page/黎時煖.md "wikilink") | 獨立                                 |                                    |
|                                   | [趙汝熙](../Page/趙汝熙.md "wikilink") | 獨立                                 |                                    |

  - 在此屆的所有區議會中，均設有官守議員，而各區的官守議員大致是市政總署助理署長、地政總署地政專員、社署福利專員、運輸署運輸主任、地政工務科總工程主任、警務處警司或指揮官和康樂文化署康體主任，如果議程涉及其他部門，該部門亦會派代表出席。

## 注釋

<references/>

## 參考

  - [1984
    年香港年鑑(第三十七回)](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_actual_q=%28%20%28%20allTermsMandatory%3A%28true%29%20OR+all_dc.title%3A%28%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984%29%20OR+all_dc.creator%3A%28%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984%29%20OR+all_dc.contributor%3A%28%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984%29%20OR+all_dc.subject%3A%28%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984%29%20OR+fulltext%3A%28%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984%29%20OR+all_dc.description%3A%28%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984%29%29%29&_coverpage_WAR_mmisportalportlet_sort_field=score&p_r_p_-1078056564_c=QF757YsWv5%2BakvA8rFW5EkPCy%2F3QRlcT&_coverpage_WAR_mmisportalportlet_sort_order=desc&_coverpage_WAR_mmisportalportlet_o=0&_coverpage_WAR_mmisportalportlet_hsf=%E9%A6%99%E6%B8%AF%E5%B9%B4%E9%91%911984)

[Category:油尖旺區](../Category/油尖旺區.md "wikilink")
[Category:香港殖民地政府前部門機構](../Category/香港殖民地政府前部門機構.md "wikilink")
[Category:香港區議會](../Category/香港區議會.md "wikilink")

1.