## 頒獎禮詳情

## 得獎歌曲

| 總排名  | 票數   | 曲名    | 歌者                               | 作曲                                     | 填詞                                                                | 編曲                                     | 大碟            | 公司                                      | 註                                                                                        |
| ---- | ---- | ----- | -------------------------------- | -------------------------------------- | ----------------------------------------------------------------- | -------------------------------------- | ------------- | --------------------------------------- | ---------------------------------------------------------------------------------------- |
| 第01位 | 6451 | 小李飛刀  | [羅文](../Page/羅文.md "wikilink")   | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | [盧國沾](../Page/盧國沾.md "wikilink")                                  |                                        | 小李飛刀          | [娛樂](../Page/娛樂唱片.md "wikilink")        | [無綫電視劇](../Page/TVB.md "wikilink")《[小李飛刀](../Page/小李飛刀_\(1978年電視劇\).md "wikilink")》主題曲   |
| 第02位 | 3502 | 小村之戀  | [鄧麗君](../Page/鄧麗君.md "wikilink") | [薄井須志程](../Page/薄井須志程.md "wikilink")   | [莊奴](../Page/莊奴.md "wikilink")                                    | [竜崎孝路](../Page/:ja:竜崎孝路.md "wikilink") | 島國之情歌第四集 香港之戀 | [寶麗金](../Page/寶麗金.md "wikilink")        |                                                                                          |
| 第03位 | 6009 | 每當變幻時 | [薰妮](../Page/薰妮.md "wikilink")   | [古賀正男](../Page/古賀政男.md "wikilink")     | [盧國沾](../Page/盧國沾.md "wikilink")                                  |                                        | 每當變幻時．飄零夢     | [永恆](http://www.winghangmusic.com/)     |                                                                                          |
| 第04位 | 6629 | 明日話今天 | [甄妮](../Page/甄妮.md "wikilink")   | [中村泰士](../Page/:ja:中村泰士.md "wikilink") | [盧國沾](../Page/盧國沾.md "wikilink")                                  |                                        | 奮鬥            | [新興全音](../Page/新興全音.md "wikilink")      |                                                                                          |
| 第05位 | 6446 | 風雨同路  | [徐小鳳](../Page/徐小鳳.md "wikilink") | [筒美京平](../Page/筒美京平.md "wikilink")     | [鄭國江](../Page/鄭國江.md "wikilink")                                  | [吳智強](../Page/吳智強.md "wikilink")       | 風雨同路          | [新力](../Page/新力音樂_\(香港\).md "wikilink") |                                                                                          |
| 第06位 | 6524 | 倚天屠龍記 | [鄭少秋](../Page/鄭少秋.md "wikilink") | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | [黃霑](../Page/黃霑.md "wikilink")                                    | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | 倚天屠龍記         | [娛樂](../Page/娛樂唱片.md "wikilink")        | [無綫電視劇](../Page/TVB.md "wikilink")《[倚天屠龍記](../Page/倚天屠龍記_\(1978年電視劇\).md "wikilink")》主題曲 |
| 第07位 | 3406 | 誓要入刀山 | [鄭少秋](../Page/鄭少秋.md "wikilink") | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | [黃霑](../Page/黃霑.md "wikilink")                                    | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | 倚天屠龍記         | [娛樂](../Page/娛樂唱片.md "wikilink")        | [無綫電視劇](../Page/TVB.md "wikilink")《[陸小鳳之武當之戰](../Page/陸小鳳之武當之戰.md "wikilink")》主題曲        |
| 第08位 | 5371 | 賣身契   | [許冠傑](../Page/許冠傑.md "wikilink") | [許冠傑](../Page/許冠傑.md "wikilink")       | [許冠傑](../Page/許冠傑.md "wikilink")／[黎彼得](../Page/黎彼得.md "wikilink") |                                        | 賣身契           | [寶麗金](../Page/寶麗金.md "wikilink")        | 電影《[賣身契](../Page/賣身契_\(1978年電影\).md "wikilink")》主題曲                                      |
| 第09位 | 2747 | 願君心記取 | [張德蘭](../Page/張德蘭.md "wikilink") | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | [盧國沾](../Page/盧國沾.md "wikilink")                                  |                                        | 陸小鳳           | [娛樂](../Page/娛樂唱片.md "wikilink")        | [無綫電視劇](../Page/TVB.md "wikilink")《[陸小鳳之決戰前後](../Page/陸小鳳之決戰前後.md "wikilink")》插曲         |
| 第10位 | 2565 | 鱷魚淚   | [袁麗嫦](../Page/袁麗嫦.md "wikilink") | [黃霑](../Page/黃霑.md "wikilink")         | [黃霑](../Page/黃霑.md "wikilink")                                    |                                        | 鱷魚淚           | [百代](../Page/百代唱片_\(香港\).md "wikilink") | [麗的電視劇](../Page/麗的電視.md "wikilink")《[鱷魚淚](../Page/鱷魚淚.md "wikilink")》主題曲                 |

十大中文金曲

  - 「**總排名**」是根據聽眾、唱片商、以及唱片騎師三方投選得出的最後結果。\[1\]\[2\]
  - 「**票數**」是由聽眾歌迷透過郵寄方式投票得出的結果，合共6700票，佔總評選機制的七成。\[3\]\[4\]\[5\]

## 落選歌曲

| 得票排名 | 票數   | 曲名     | 歌者                                                              | 作曲                                                                                                                      | 填詞                                 | 編曲                                     | 大碟    | 註                                                                                       |
| ---- | ---- | ------ | --------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ---------------------------------- | -------------------------------------- | ----- | --------------------------------------------------------------------------------------- |
| 第11位 | 2344 | 決戰前夕   | [鄭少秋](../Page/鄭少秋.md "wikilink")                                | [顧嘉煇](../Page/顧嘉煇.md "wikilink")                                                                                        | [盧國沾](../Page/盧國沾.md "wikilink")   | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | 陸小鳳   | [無綫電視劇](../Page/TVB.md "wikilink")《[陸小鳳之決戰前後](../Page/陸小鳳之決戰前後.md "wikilink")》插曲        |
| 第12位 | 2226 | 巨星     | [李龍基](../Page/李龍基.md "wikilink")                                | [黎小田](../Page/黎小田.md "wikilink")                                                                                        | [司徒立光](../Page/司徒立光.md "wikilink") |                                        | 巨星    | [麗的電視劇](../Page/麗的電視.md "wikilink")《[巨星](../Page/巨星（電視劇）.md "wikilink")》主題曲             |
| 第13位 | 2179 | 春殘夢斷   | [汪明荃](../Page/汪明荃.md "wikilink")                                |                                                                                                                         | [葉紹德](../Page/葉紹德.md "wikilink")   |                                        | 春殘夢斷  |                                                                                         |
| 第14位 | 2147 | 山歌情誼長  | [汪明荃](../Page/汪明荃.md "wikilink")                                |                                                                                                                         | [葉紹德](../Page/葉紹德.md "wikilink")   |                                        | 山歌情誼長 |                                                                                         |
| 第15位 | 2066 | 浪子一招   | [羅文](../Page/羅文.md "wikilink")                                  | [顧嘉煇](../Page/顧嘉煇.md "wikilink")                                                                                        | [江羽](../Page/江羽.md "wikilink")     |                                        | 強人    | 電影《[浪子一招](../Page/浪子一招.md "wikilink")》主題曲                                               |
| 第16位 | 2012 | 六月天    | [陳秋霞](../Page/陳秋霞.md "wikilink")                                | [筒美京平](../Page/筒美京平.md "wikilink")                                                                                      | [鄭國江](../Page/鄭國江.md "wikilink")   |                                        | 秋霞之歌  |                                                                                         |
| 第17位 | 1790 | 你裝作不知道 | [鄧麗君](../Page/鄧麗君.md "wikilink")                                | [猪俣公章](../Page/:ja:猪俣公章.md "wikilink")                                                                                  | [莊奴](../Page/莊奴.md "wikilink")     | [竜崎孝路](../Page/:ja:竜崎孝路.md "wikilink") | 愛情更美麗 |                                                                                         |
| 第18位 | 1443 | 追趕跑跳碰  | [溫拿](../Page/溫拿.md "wikilink")                                  | [鍾鎮濤](../Page/鍾鎮濤.md "wikilink")                                                                                        | [鄭國江](../Page/鄭國江.md "wikilink")   |                                        | 追趕跑跳碰 | 電影《[追趕跑跳碰](../Page/追趕跑跳碰.md "wikilink")》主題曲                                             |
| 第19位 | 1033 | 熊熊聖火   | [鄭少秋](../Page/鄭少秋.md "wikilink")                                | [顧嘉煇](../Page/顧嘉煇.md "wikilink")                                                                                        | [黃霑](../Page/黃霑.md "wikilink")     | [顧嘉煇](../Page/顧嘉煇.md "wikilink")       | 倚天屠龍記 | [無綫電視劇](../Page/TVB.md "wikilink")《[倚天屠龍記](../Page/倚天屠龍記_\(1978年電視劇\).md "wikilink")》插曲 |
| 第20位 | 671  | 不可以逃避  | [溫拿](../Page/溫拿.md "wikilink")                                  | [Poul Dehnhardt](../Page/:en:Poul_Dehnhardt.md "wikilink")／[Torben Lendager](../Page/:en:Torben_Lendager.md "wikilink") | [黃霑](../Page/黃霑.md "wikilink")     |                                        | 追趕跑跳碰 | 電影《[追趕跑跳碰](../Page/追趕跑跳碰.md "wikilink")》插曲                                              |
| 第21位 | 469  | 莫忘我    | [羅文](../Page/羅文.md "wikilink")                                  |                                                                                                                         | [江羽](../Page/江羽.md "wikilink")     |                                        | 死亡遊戲  |                                                                                         |
| 第22位 | 432  | 青春曲    | [薰妮](../Page/薰妮.md "wikilink")／[馮偉棠](../Page/馮偉棠.md "wikilink") | [いずみたく](../Page/:ja:いずみたく.md "wikilink")                                                                                | [黎彼得](../Page/黎彼得.md "wikilink")   |                                        | 雪山飛狐  | [無綫外購電視劇](../Page/TVB.md "wikilink")《[青春曲](../Page/:ja:われら青春!.md "wikilink")》港版主題曲      |
| 第23位 | 366  | 大亨     | [徐小鳳](../Page/徐小鳳.md "wikilink")                                | [顧嘉煇](../Page/顧嘉煇.md "wikilink")                                                                                        | [黃霑](../Page/黃霑.md "wikilink")     |                                        | 大亨    | [無綫電視劇](../Page/TVB.md "wikilink")《[大亨](../Page/大亨_\(電視劇\).md "wikilink")》主題曲           |
| 第24位 | 280  | 有水冇閉翳  | [許冠英](../Page/許冠英.md "wikilink")                                | [John Gummoe](../Page/:en:John_Gummoe.md "wikilink")                                                                    | [鄭國江](../Page/鄭國江.md "wikilink")   |                                        | 夏之戀   |                                                                                         |
| 第25位 | 79   | 愛的夢最美麗 | [黃鳳鳳](../Page/黃鳳鳳.md "wikilink")                                |                                                                                                                         |                                    |                                        |       |                                                                                         |

十大中文金曲落選歌曲

## 資料

### 歌曲補充資料

  - 《**明日話今天**》原曲：心のこり｜原唱：細川たかし
  - 《**風雨同路**》原曲：しあわせの一番星｜原唱：淺田美代子
  - 《**小村之戀**》原曲：ふるさとはどこですか｜原詞：中山大三郎
  - 《**你裝作不知道**》唱片：愛情更美麗（島國之情歌第五集）
  - 《**青春曲**》為中村雅俊首齣電視劇｜[無綫於](../Page/TVB.md "wikilink")1978年首播
  - 《**[每當變幻時](../Page/每當變幻時.md "wikilink")**》原曲：「サヨンの鐘」（[莎韻之鐘](../Page/莎韻之鐘.md "wikilink")），原唱為1940年代日本女歌手[渡邊はま子的流行歌曲](../Page/渡邊はま子.md "wikilink")《莎韻之鐘》，曾被翻唱為國語歌《[月光小夜曲](../Page/月光小夜曲.md "wikilink")》和粵語歌《每當變幻時》。原作者古賀正男曾先後使用「古賀正男」及「古賀政男」為名字。

### 參考資料

<references/>

### 其他音樂頒獎禮

[第01](../Page/category:十大中文金曲.md "wikilink")

1.  [華僑日報](../Page/華僑日報.md "wikilink")，一九七九年二月廿一日，第三張第四頁
2.  [華僑日報](../Page/華僑日報.md "wikilink")，一九七九年二月廿三日，第八張第四頁
3.  [華僑日報](../Page/華僑日報.md "wikilink")，一九七九年一月五日，第七張第四頁
4.  [港台第一屆十大金曲候選歌之謎](http://blogold.chinaunix.net/u/14418/showart_420041.html)
    （[黃志華](http://blogold.chinaunix.net/u/14418/)
    [音樂研究日誌](http://blogold.chinaunix.net/u/14418/article_37534.html)
    ）
5.  [樂壇趣事--歌曲《小李飛刀》奪魁記](http://blog.sina.com.cn/s/blog_66c677e30100iwb4.html)（[李明暉的博客](http://blog.sina.com.cn/u/1724282851)）