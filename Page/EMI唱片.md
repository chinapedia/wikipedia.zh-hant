**EMI唱片**（）是一家英国的[唱片公司](../Page/唱片公司.md "wikilink")，由[EMI公司作为其旗下的旗舰品牌于](../Page/EMI.md "wikilink")1972年创办，1973年1月作为、[Parlophone唱片公司的继任品牌推出](../Page/Parlophone.md "wikilink")。随后，公司在全世界范围内推广该品牌。\[1\]

## 参考资料

[Category:英国唱片公司](../Category/英国唱片公司.md "wikilink")
[Category:EMI](../Category/EMI.md "wikilink")

1.