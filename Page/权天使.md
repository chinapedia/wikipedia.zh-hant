[Principalities,_St_Michael_and_All_Angels',_Somerton.jpg](https://zh.wikipedia.org/wiki/File:Principalities,_St_Michael_and_All_Angels',_Somerton.jpg "fig:Principalities,_St_Michael_and_All_Angels',_Somerton.jpg")
**權天使**（[希臘語](../Page/希臘語.md "wikilink")[轉寫](../Page/羅馬化.md "wikilink")：，[拉丁語](../Page/拉丁語.md "wikilink")：，[英語](../Page/英語.md "wikilink")：）見於[哥羅森書一章](../Page/哥羅森書.md "wikilink")16節及[厄弗所書一章](../Page/厄弗所書.md "wikilink")21節，[天主教譯作](../Page/天主教.md "wikilink")**率領者**，[基督新教譯作](../Page/基督新教.md "wikilink")**執政的**，第三級第一等天使，其工作為掌管保衛國家，並決定人世間的統治者，[政治](../Page/政治.md "wikilink")、[軍事](../Page/軍事.md "wikilink")、[經濟等都是其自掌管的範圍](../Page/經濟.md "wikilink")，在《[失樂園](../Page/失樂園.md "wikilink")》中有敘述到權天使直接領導天使軍團和惡魔軍團作戰。

此外[主天使](../Page/主天使.md "wikilink")（）也有被譯作權天使的情况。

在[現代中文譯本](../Page/現代中文譯本.md "wikilink")（修訂版）[聖經](../Page/聖經.md "wikilink")[但以理書](../Page/但以理書.md "wikilink")10:13經文中寫到：於是他說：「[但以理呀](../Page/但以理.md "wikilink")，不要怕。自從第一天你謙卑地在上帝面前祈求明白這異象的意思，祂就垂聽你的禱告。我就是來為你解釋的。但是[波斯的](../Page/波斯.md "wikilink")*[護國天使](../Page/護國天使.md "wikilink")（原文意为：王子，统治者。下同）*攔阻我，使我耽擱了二十一天。」和10:20-21中的經文寫到：他說：「你知道我為什麼到這裡來嗎？我來是要把真理書上所寫的啟示給你。現在我必須回去，跟[波斯的](../Page/波斯.md "wikilink")*[護國天使](../Page/護國天使.md "wikilink")*作戰。以後，[希臘的](../Page/希臘.md "wikilink")*[護國天使](../Page/護國天使.md "wikilink")*要出現。除了*守護[以色列的天使](../Page/以色列.md "wikilink")[米迦勒](../Page/米迦勒.md "wikilink")*，沒人能幫助我。」皆曾提到*[護國天使](../Page/護國天使.md "wikilink")*一詞，可能指的就是權天使。

## 參見

  - [天使等級](../Page/天使等級.md "wikilink")

[A](../Category/基督教中的天使.md "wikilink")
[A](../Category/天使的級別.md "wikilink")
[A](../Category/天使.md "wikilink")