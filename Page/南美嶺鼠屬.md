**南美嶺鼠屬**（***Thomasomys***），[哺乳綱](../Page/哺乳綱.md "wikilink")、[囓齒目](../Page/囓齒目.md "wikilink")、[倉鼠科的一屬](../Page/倉鼠科.md "wikilink")，而與南美嶺鼠屬（金嶺鼠）同科的動物尚有[美洲攀鼠屬](../Page/美洲攀鼠屬.md "wikilink")（稽攀鼠）、紅鼻鼠屬（紅鼻鼠）、暖鼠屬（暖鼠）、稻水鼠屬（哈氏稻水鼠）等之數種[哺乳動物](../Page/哺乳動物.md "wikilink")。

## 參考文獻

  - 中國科學院，《中國科學院動物研究所的世界動物名稱資料庫》，[1](https://web.archive.org/web/20050718232917/http://vzd.brim.ac.cn/division/fauna/index.asp)

[Category:南美嶺鼠屬](../Category/南美嶺鼠屬.md "wikilink")