**孙鹏**（），[中国](../Page/中国.md "wikilink")[网球运动員](../Page/网球.md "wikilink")。9歲開始接觸網球，師承多位中國網球名將，現在教練為[Hai-Long
Jin](../Page/Hai-Long_Jin.md "wikilink")。2005年[十運會](../Page/十運會.md "wikilink")，與[王鈺被譽為振興](../Page/王鈺_\(網球運動員\).md "wikilink")[天津市在國內網球的霸主地位的希望](../Page/天津市.md "wikilink")，但是在團體賽和雙打中接連失利；而后在男單決賽中擊敗[曾少眩而奪冠](../Page/曾少眩.md "wikilink")。

他曾代表中國參加[戴维斯杯](../Page/戴维斯杯.md "wikilink")、[奥運會等大型賽事](../Page/奥運會.md "wikilink")，2006年更與[彭帅聯手代表中國參加](../Page/彭帅.md "wikilink")[霍普曼杯](../Page/霍普曼杯.md "wikilink")。單打世界排名最高為290位，雙打則為670位。台維斯盃戰績為3勝6負。

## 國內比賽

  - 2005年[中华人民共和国第十届全运会网球比赛男子單打比賽金牌](../Page/中华人民共和国第十届全运会网球比赛.md "wikilink")、男子團體比賽銀牌、男子雙打比賽銅牌（代表[天津](../Page/天津.md "wikilink")）
  - 2009年[中華人民共和國第十一屆全運會網球男子團體比賽銅牌](../Page/中華人民共和國第十一屆全運會網球男子團體比賽.md "wikilink")（代表[天津](../Page/天津.md "wikilink")）

## 外部連結

  -
  -
  -
  - [彭帅、孙鹏代表中国首次出征霍普曼杯赛](https://web.archive.org/web/20070212135937/http://www.tj.xinhuanet.com/misc/2005-12/28/content_5929725.htm)

  - [中国网球“男一号”孙鹏获北京奥运会外卡](http://news.xinhuanet.com/sports/2008-07/01/content_8470432.htm)

[Category:鞍山人](../Category/鞍山人.md "wikilink")
[Category:鞍山籍运动员](../Category/鞍山籍运动员.md "wikilink")
[Category:中国男子网球运动员](../Category/中国男子网球运动员.md "wikilink")
[Category:中国奥运网球运动员](../Category/中国奥运网球运动员.md "wikilink")
[Category:2008年夏季奧林匹克運動會網球運動員](../Category/2008年夏季奧林匹克運動會網球運動員.md "wikilink")
[Category:2006年亞洲運動會網球運動員](../Category/2006年亞洲運動會網球運動員.md "wikilink")
[P鹏](../Category/孫姓.md "wikilink")