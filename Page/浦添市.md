**浦添市**（，）位於[沖繩島南部地區與中部地區的分界](../Page/沖繩島.md "wikilink")，緊鄰[那霸市](../Page/那霸市.md "wikilink")，西側面海，北側為[宜野灣市](../Page/宜野灣市.md "wikilink")，東側為[西原町](../Page/西原町.md "wikilink")。僅次於那霸市、沖繩市、[宇流麻市為](../Page/宇流麻市.md "wikilink")[沖繩縣第四大城市](../Page/沖繩縣.md "wikilink")。人口密度在縣內僅次於那霸市，比政令指定城市的[埼玉市還高](../Page/埼玉市.md "wikilink")。產業以工商業為主。12世紀至14世紀間，浦添城為[中山王國的首都](../Page/中山王國.md "wikilink")。

在[沖繩島戰役中](../Page/沖繩島戰役.md "wikilink")，[日軍為了保護設置於](../Page/日軍.md "wikilink")[首里的中央司令部](../Page/首里.md "wikilink")，在浦添市以及宜野灣市的區域內與[美軍展開了城市戰](../Page/美軍.md "wikilink")。戰後市內西海岸地區也成為美軍的基地「牧港補給基地」。

## 地理

### 轄區

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 34%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>安波茶</li>
<li>伊祖</li>
<li>伊奈武瀨</li>
<li>西洲</li>
<li>內間</li>
<li>大平</li>
<li>經塚</li>
</ul></td>
<td><ul>
<li>城間</li>
<li>小灣</li>
<li>勢理客</li>
<li>澤岻</li>
<li>當山</li>
<li>仲西</li>
<li>仲間</li>
</ul></td>
<td><ul>
<li>西原</li>
<li>前田</li>
<li>牧港</li>
<li>港川</li>
<li>宮城</li>
<li>屋富祖</li>
</ul></td>
</tr>
</tbody>
</table>

## 歷史

13世紀時，[琉球王國的](../Page/琉球王國.md "wikilink")[英祖王朝開始以](../Page/英祖王朝.md "wikilink")[浦添為首都](../Page/浦添城.md "wikilink")，直到[中山王國被](../Page/中山王國.md "wikilink")[尚巴志滅亡為止](../Page/尚巴志.md "wikilink")。

戰後由於那霸市人口成長快速，使得緊鄰那霸市的浦添人口也跟著成長，於1970年從浦添村改制為浦添市，1998年1月人口突破10萬人。

  - 1887年：浦添小學成立。
  - 1896年：實施[郡區制](../Page/郡區制.md "wikilink")，浦添間切編入[中頭郡](../Page/中頭郡.md "wikilink")。
  - 1908年：實施[島嶼町村制](../Page/島嶼町村制.md "wikilink")，改設**浦添村**。
  - 1922年：沖繩縣營鐵道通車，設置內間站、城間站。
  - 1935年：建立伊祖神社。
  - 1945年：約半數（44.6%）的居民在沖繩島戰役中犧牲。
  - 1946年：多數居民成為美軍的俘虜被送往收容所。
  - 1947年：收納戰爭死者遺體的浦和塔完成。
  - 1950年：開始建設美軍基地「牧港補給基地」。
  - 1970年：浦添村改制為**浦添市**。
  - 1990年：浦添市美術館開館。
  - 2006年4月29日：大型購物中心「浦添購物中心」開幕。

## 產業

### 商業

市內的主要的商圈，包括汽車銷售公司、購物中心和快餐店，分布於國道58號沿線。1991年西海岸填海造地後形成了沖繩縣批發商業區，區內有63公司，是沖繩縣內主要物流據點。而第二次填海造地後，預計將以運輸等物流站為設置目標。

在浦添市設置總公司的企業包括沖繩電力、獵戶座啤酒、沖繩明治乳業、沖繩可口可樂。

### 農業

1998年12月末時，市內種植面積為4,053公頃，佔全市區面積的2％，多數集中在東部。甘蔗佔種植面積的51.2％、蔬菜類23％。農戶222戶，其中13戶為專業農戶，209戶為兼營農家。

### 漁業

1998年12月末時有87戶漁民，64艘漁船，64戶（73.5％）為職業漁民，年捕魚量為120噸，營收約1億2,996萬[日圓](../Page/日圓.md "wikilink")。

近幾年安排江捕漁業轉換為養殖漁業，於1986年開始蝦的養殖，每年均順利的成長，1998年12月末時已年產38，營收約1億9,800萬日圓。

### 工業

1997年末時，市內有167個事業所，為沖繩縣第四高，工作人員1,988人，為沖繩縣第二；製造品出貨營業額495億日圓，為沖繩縣第三。

金屬產品製造業有31個事業所，員工188人，食品製造業有37個事業所，員工813人，出版印刷相關產業有32個事業所，員工228人。

## 交通

### 鐵路

  - 二次大戰前原本有沖繩縣營鐵道嘉手納線，但已於二次大戰期間遭到破壞，當時在浦添市內設有「牧港站」、「城間站」、「內間站」。
  - 目前有將那霸市內[沖繩都市單軌鐵路延伸至浦添市的計劃](../Page/沖繩都市單軌鐵路.md "wikilink")。

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/沖繩自動車道.md" title="wikilink">沖繩自動車道</a>：<a href="../Page/西原交流道.md" title="wikilink">西原交流道</a></li>
</ul>
<dl>
<dt>國道</dt>

</dl>
<ul>
<li>國道58號</li>
<li>國道330號</li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>沖繩縣道38號浦添西原線</li>
</ul></td>
<td><dl>
<dt>一般<a href="../Page/都道府縣道.md" title="wikilink">縣道</a></dt>

</dl>
<ul>
<li>沖繩縣道153號線</li>
<li>沖繩縣道241號宜野灣南風原線</li>
<li>沖繩縣道251號那霸宜野灣線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

  - [浦添城遺址](../Page/浦添城.md "wikilink")
  - [浦添ようどれ](../Page/浦添ようどれ.md "wikilink")
  - [伊波普猷陵園](../Page/伊波普猷.md "wikilink")
  - 浦添貝塚
  - [伊祖城遺址](../Page/伊祖城.md "wikilink")
  - 經塚碑

## 教育

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.urasoe-h.open.ed.jp/">沖繩縣立浦添高等學校</a></li>
<li><a href="http://www.urasoe-ch.open.ed.jp/">沖繩縣立浦添商業高等學校</a></li>
<li><a href="http://www.urasoe-th.open.ed.jp/">沖繩縣立浦添工業高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.naha-th.open.ed.jp/">沖繩縣立那霸工業高等學校</a></li>
<li><a href="http://www.yomei-h.open.ed.jp/">沖繩縣立陽明高等學校</a></li>
<li><a href="https://web.archive.org/web/20061109214833/http://www.showayakka-jh.ed.jp/index1.html">私立昭和藥科大學附屬高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20060806080917/http://www.urasoe.ed.jp/urasoe-j/">浦添市立浦添中學校</a></li>
<li><a href="https://web.archive.org/web/20051229215307/http://www.urasoe.ed.jp/minatogawa-j/">浦添市立港川中學校</a></li>
<li><a href="https://web.archive.org/web/20060806103541/http://www.urasoe.ed.jp/kamimori-j/">浦添市立神森中學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20060808190222/http://www.city.urasoe.okinawa.jp/nakanisi-j/">浦添市立仲西中學校</a></li>
<li><a href="https://web.archive.org/web/20060806103626/http://www.urasoe.ed.jp/uranishi-j/">浦添市立浦西中學校</a></li>
<li><a href="https://web.archive.org/web/20061109214833/http://www.showayakka-jh.ed.jp/index1.html">私立昭和藥科大學附屬中學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20070318165611/http://www.urasoe.ed.jp/urasoe-e/">浦添市立浦添小學校</a></li>
<li><a href="https://web.archive.org/web/20061123140513/http://www.city.urasoe.okinawa.jp/nakanisi-e/">浦添市立仲西小學校</a></li>
<li><a href="https://web.archive.org/web/20060806081203/http://www.urasoe.ed.jp/kamimori-e/index.htm">浦添市立神森小學校</a></li>
<li><a href="https://web.archive.org/web/20060806104450/http://www.urasoe.ed.jp/urashiro-e/">浦添市立浦城小學校</a></li>
<li><a href="https://web.archive.org/web/20060806081139/http://www.urasoe.ed.jp/makiminato-e/">浦添市立牧港小學校</a></li>
<li><a href="https://web.archive.org/web/20061126103710/http://www.urasoe.ed.jp/touyama-e/">浦添市立當山小學校</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20061126104210/http://www.urasoe.ed.jp/uchima-e/uchima-e/">浦添市立內間小學校</a></li>
<li><a href="https://web.archive.org/web/20061126104005/http://www.urasoe.ed.jp/minatogawa-e/">浦添市立港川小學校</a></li>
<li><a href="https://web.archive.org/web/20060806104058/http://www.urasoe.ed.jp/miyagi-e/">浦添市立宮城小學校</a></li>
<li><a href="https://web.archive.org/web/20060806103858/http://www.urasoe.ed.jp/takushi-e/">浦添市立澤岻小學校</a></li>
<li><a href="https://web.archive.org/web/20060806080838/http://www.urasoe.ed.jp/maeda-e/">浦添市立前田小學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 市立幼稚園

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20070611083415/http://www.city.urasoe.okinawa.jp/cgi/yotien/urasoe/indexkanri.cgi">浦添市立浦添幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070927013206/http://www.city.urasoe.okinawa.jp/cgi/yotien/nakanisi/indexkanri.cgi">浦添市立仲西幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070927013331/http://www.city.urasoe.okinawa.jp/cgi/yotien/kamimori/indexkanri.cgi">浦添市立神森幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070901155056/http://www.city.urasoe.okinawa.jp/cgi/yotien/urasiro/indexkanri.cgi">浦添市立浦城幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070902023704/http://www.city.urasoe.okinawa.jp/cgi/yotien/makiminato/indexkanri.cgi">浦添市立牧港幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070902023657/http://www.city.urasoe.okinawa.jp/cgi/yotien/touyama/indexkanri.cgi">浦添市立当山幼稚園</a></li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20070927013213/http://www.city.urasoe.okinawa.jp/cgi/yotien/utima/indexkanri.cgi">浦添市立内間幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070902023520/http://www.city.urasoe.okinawa.jp/cgi/yotien/minatogawa/indexkanri.cgi">浦添市立港川幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070901212742/http://www.city.urasoe.okinawa.jp/cgi/yotien/miyagi/indexkanri.cgi">浦添市立宮城幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070901212941/http://www.city.urasoe.okinawa.jp/cgi/yotien/takusi/indexkanri.cgi">浦添市立沢岻幼稚園</a></li>
<li><a href="https://web.archive.org/web/20070902043154/http://www.city.urasoe.okinawa.jp/cgi/yotien/maeda/indexkanri.cgi">浦添市立前田幼稚園</a></li>
</ul></td>
</tr>
</tbody>
</table>

### [特殊學校](../Page/特殊學校.md "wikilink")

  - [沖繩縣立大平養護學校](https://web.archive.org/web/20060302063932/http://www.sif.oki.ed.jp/~oohirayog-s/)
  - [沖繩縣立鏡丘養護學校](http://www.kagamigaoka-sh.open.ed.jp/)<small>（原名：）</small>
      - [沖繩縣立鏡丘養護學校浦添分校](http://www.urasoebunko-sh.open.ed.jp/index.html)<small>（原名：）</small>

### 專修學校

  - [沖繩縣立浦添看護學校](http://www.urasoe-ns.ac.jp/)
  - [沖繩齒科衛生士學校](https://web.archive.org/web/20060918003033/http://okisi.org/galtuko/eiseisi.htm)
  - [美野里學園
    琉球調理師專修學校](http://minori-gakuen.ac.jp/chouri/chouri-top.htm)<small>（原名：）</small>
  - 國際設計學會

## 姊妹市

### 日本

  - [蒲郡市](../Page/蒲郡市.md "wikilink")（[愛知縣](../Page/愛知縣.md "wikilink")）

### 海外

  - [泉州市](../Page/泉州市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")
    [福建省](../Page/福建省.md "wikilink")）

## 本地出身之名人

  - [仲間由紀惠](../Page/仲間由紀惠.md "wikilink")：[演員](../Page/演員.md "wikilink")
  - [丹尼友利](../Page/丹尼友利.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")
  - KEN：歌手、[DA PUMP成員](../Page/DA_PUMP.md "wikilink")
  - MAKI （中村麻紀）：歌手、前[HIGH and MIGHTY
    COLOR主唱](../Page/HIGH_and_MIGHTY_COLOR.md "wikilink")
  - [宫平保](../Page/宫平保.md "wikilink")：[中国武术](../Page/中国武术.md "wikilink")

## 外部連結

  -
[U](../Category/東海沿海城市.md "wikilink")