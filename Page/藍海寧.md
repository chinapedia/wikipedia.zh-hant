藍海寧，[香港作家](../Page/香港.md "wikilink")，其正職為[空中小姐](../Page/空中小姐.md "wikilink")。

藍海寧原本是中學教師，任教[英文及](../Page/英文.md "wikilink")[歷史科](../Page/歷史.md "wikilink")。她因對工作感到厭倦而考慮轉工，因而參加了一間外國[航空公司在香港舉行的](../Page/航空公司.md "wikilink")[空中服務員招聘面試](../Page/空中服務員.md "wikilink")，並獲得取錄。

藍海寧以業餘作家的身份，在[香港經濟日報撰寫專欄](../Page/香港經濟日報.md "wikilink")，並推出過多本與空中服務員題材有關的書籍。

## 作品

  - [《三萬呎高空上的悸動─空姐事件簿》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9626782242)ISBN 9626782242
  - [《再邂逅在三萬呎高空上─空姐事件簿
    II》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9626782455)ISBN 9626782455
  - [《送你一朵三萬呎高空上的白雲─空姐事件簿
    III》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9626783095)ISBN 9626783095
  - [《尋找三萬呎高空上的美麗─空姐事件簿
    IV》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9626783737)ISBN 9626783737
  - [《我在雲上等你說愛我》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626784693)ISBN 9789626784693
  - [《三萬呎高空上的喝彩聲─空姐事件簿
    V》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626785003)ISBN 9789626785003
  - [《三萬呎高空上的日與夜─空姐事件簿
    VI》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626785812)ISBN 9789626785812
  - [《三萬呎高空上的玻璃球─空姐事件簿
    VII》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626786260)ISBN 9789626786260
  - [《三萬呎高空上的阿四任務－空姐事件簿
    VIII》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626786710)ISBN︰9789626786710
  - [《專心走好每一步─登上心靈航班》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626786277)ISBN 9789626786277
  - [《三萬呎高空上的「飛」常夢──空姐事件簿IX》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626787168)ISBN 9789626787168

## 資料來源

  - [飛人生活：藍海寧](https://web.archive.org/web/20070310221853/http://www.hkedcity.net/article/future_focus/flyinglife/airhostess.phtml)

[Category:香港女性作家](../Category/香港女性作家.md "wikilink")
[Category:香港專欄作家](../Category/香港專欄作家.md "wikilink")
[藍](../Category/香港新教徒.md "wikilink")