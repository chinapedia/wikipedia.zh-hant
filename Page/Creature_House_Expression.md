**Creature House Expression**
是一個[矢量繪圖軟件](../Page/矢量繪圖.md "wikilink")，原本是由一間名為
Creature House 的[香港公司開發](../Page/香港.md "wikilink")，後來這間公司輾轉易手，賣給 Ray
Dream，Ray Dream 後來跟 Fractal Design 合併，及後 Fractal Design 又再跟 MetaTools
合併而成為 MetaCreations。接下來 MetaCreations 將其下的軟件部門分拆出售，Expression
軟件的版權又重回它的創辦人 徐迢之(Alex Hsu) 及 Creature House 公司手中。

此後 Creature House 分別推出 Expression 2.0 及 3.0 版。到了 2003 年 9 月，消息傳出
[Microsoft](../Page/Microsoft.md "wikilink") 收購了 Creature House，並把旗下的
Expression 矢量繪圖與 Living Cels 動畫技術收歸未來的
[Sparkle](../Page/Sparkle.md "wikilink") 架構之中。

到了 2004 年 6 月左右，[Microsoft](../Page/Microsoft.md "wikilink") 把更新的
Expression 3.3 版釋放出來給用戶免費下載
[1](https://web.archive.org/web/20060424232957/http://www.microsoft.com/products/expression/en/graphic_designer/previous/expression3_home.aspx)（需註冊
Hotmail Passport 帳號），而且同時提供了 [Mac OS X](../Page/Mac_OS_X.md "wikilink")
版及 [Windows](../Page/Windows.md "wikilink") 版。另外，Expression 3.3
的介面已經全面中文化，以利廣大中文用戶。

於 2005 年 6 月，Microsoft Expression 推出了新的測試版本，代號名為
[Acrylic](http://www.cgan.com/news/2724.htm)：這個新版本正處於測試階段。Acrylic
的一些新功能介紹可參考
[這個英文網頁](https://web.archive.org/web/20050617033639/http://www.studioe3.com/Acrylic/new.asp)。06年12月，Creature
House Expression已融入[Microsoft Expression
Studio中的](../Page/Microsoft_Expression_Studio.md "wikilink")[Microsoft
Expression Design](../Page/Microsoft_Expression_Design.md "wikilink")。

## 支持文件格式

可以打開[PSD](../Page/PSD.md "wikilink")、[AI](../Page/Adobe_Illustrator.md "wikilink")、[WMF等格式](../Page/WMF.md "wikilink")，導入[BMP](../Page/BMP.md "wikilink")、[GIF](../Page/GIF.md "wikilink")、[ICO](../Page/ICO.md "wikilink")、[JPEG](../Page/JPEG.md "wikilink")、[PNG](../Page/PNG.md "wikilink")、[TIFF等格式](../Page/TIFF.md "wikilink")，導出PNG、JPEG、GIF、TIFF、BMP、PSD、[PDF等格式](../Page/PDF.md "wikilink")。

## 相關文章及資源

  - [下載 Creature House
    Expression 3.3](https://web.archive.org/web/20050222200121/http://download.microsoft.com/download/a/5/d/a5d625a5-2e3d-4e9c-8608-6de48d7b569f/CreatureHouseExpression3_3.exe)（免费，需註冊，並無使用日期限制）
  - [下載 Microsoft® Expression® Graphic Designer September 2006 Community
    Technology
    Preview](http://www.microsoft.com/downloads/details.aspx?FamilyId=565CAFC7-343F-4927-ABD1-8BBF6E7E01DE&displaylang=en+/)（需註冊，使用限期為2006年12月31日）[2](http://www.microsoft.com/products/expression/en/graphic_designer/gd_faq.mspx#q1)
  - zonble's prompt blog:
      - [從 Expression 3
        說起](https://archive.is/20040520185025/http://zonble.twbbs.org/archives/2004_04/478.php)
      - [Expression
        的描圖紙功能](https://archive.is/20040520185052/http://zonble.twbbs.org/archives/2004_04/484.php)（暨最簡單的向量繪圖教學）
  - [The GIMP 及 2D
    向量繪圖軟體介紹](https://web.archive.org/web/20050312231407/http://cclien.net/phpBB2/viewtopic.php?t=229)，當中有介紹
    Expression


\*（日文）[Expression Users
Link](https://web.archive.org/web/20011010185522/http://www.seri.sakura.ne.jp/~iori/natsu/)

  - （英文）[Yahoo\! expression3
    討論群](http://groups.yahoo.com/group/expression3/)
  - （英文）[在 Linux 上使用 WINE 安裝及執行
    Expression 3.3](https://web.archive.org/web/20050211193644/http://frankscorner.org/index.php?p=expression3)

## 美術作品樣本

  - Microsoft 官方網站：[Expression
    矢量美工展示](http://www.microsoft.com/products/expression/en/graphic_designer/previous/expression3_gallery.aspx)（提供了
    \*.XPR 樣本下載）

[Category:向量圖形編輯器](../Category/向量圖形編輯器.md "wikilink")