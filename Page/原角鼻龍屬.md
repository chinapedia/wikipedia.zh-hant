**原角鼻龍屬**（[屬名](../Page/屬.md "wikilink")：）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於中[侏儸紀](../Page/侏儸紀.md "wikilink")[巴統階的](../Page/巴統階.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")。原角鼻龍是種體型小的[肉食性恐龍](../Page/肉食性.md "wikilink")，身長略小於3公尺\[1\]。

原角鼻龍起初被認為是[角鼻龍的祖先](../Page/角鼻龍.md "wikilink")，因為牠們口鼻部上的類似小型突起物\[2\]。但是，原角鼻龍目前被認為是種已知最早的[虛骨龍類之一](../Page/虛骨龍類.md "wikilink")\[3\]，屬於原始的[暴龍超科](../Page/暴龍超科.md "wikilink")
\[4\]。

[模式標本是在](../Page/模式標本.md "wikilink")1910年發現於[英國的](../Page/英國.md "wikilink")[敏辛漢頓](../Page/敏辛漢頓.md "wikilink")，當時正在進行一項水利工程。該標本目前存放於[倫敦自然歷史博物館](../Page/倫敦自然歷史博物館.md "wikilink")\[5\]。

## 分類

[Proceratosaurus_skull.png](https://zh.wikipedia.org/wiki/File:Proceratosaurus_skull.png "fig:Proceratosaurus_skull.png")
[Proceratosaurus_estimated_size.png](https://zh.wikipedia.org/wiki/File:Proceratosaurus_estimated_size.png "fig:Proceratosaurus_estimated_size.png")的體型比較圖\]\]
在1910年，[亞瑟·史密斯·伍德沃德](../Page/亞瑟·史密斯·伍德沃德.md "wikilink")（Arthur Smith
Woodward）最初命名這種恐龍時，將牠們歸類於[斑龍的一種](../Page/斑龍.md "wikilink")，布氏斑龍（*Megalosaurus
bradleyi*），並根據類似的[鼻骨突起](../Page/鼻骨.md "wikilink")，而認為牠們是[角鼻龍的祖先](../Page/角鼻龍.md "wikilink")\[6\]。在1926年，[休尼](../Page/休尼.md "wikilink")（Friedrich
von
Huene）博士將牠們建立為獨立的原角鼻龍屬，並支持角鼻龍祖先的理論。在1932年，休尼提出原角鼻龍、角鼻龍都屬於[虛骨龍類](../Page/虛骨龍類.md "wikilink")\[7\]。

直到1980年代晚期，角鼻龍被發現是種較原始的獸腳類恐龍，並不屬於虛骨龍類，而原角鼻龍的分類位置出現爭議。[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
S.
Paul）提出原角鼻龍是[嗜鳥龍的近親](../Page/嗜鳥龍.md "wikilink")，因為當時認為嗜鳥龍的鼻端也有類似的鼻角（這理論後來被推翻）。保羅認為這兩個相似的物種，可能都屬於原角鼻龍類、虛骨龍類、或是原始[異特龍超科](../Page/異特龍超科.md "wikilink")。此外，保羅更提出體型較大的[皮爾逖龍與原角鼻龍是同種動物](../Page/皮爾逖龍.md "wikilink")，所以皮爾逖龍是原角鼻龍的一個[次異名](../Page/次異名.md "wikilink")\[8\]。但根據2010年的最新研究，皮爾逖龍與原角鼻龍的化石沒有相似之處，兩者是個別獨立的屬\[9\]。

21世紀初期的數個[種系發生學研究](../Page/種系發生學.md "wikilink")，發現原角鼻龍、嗜鳥龍都屬於虛骨龍類，與角鼻龍類、異特龍類的關係較遠；只有一個2000年的研究仍主張原角鼻龍屬於角鼻龍類。在2004年，[湯瑪斯·霍爾茲](../Page/湯瑪斯·霍爾茲.md "wikilink")（Thomas
R. Holtz）也發現原角鼻龍屬於虛骨龍類，而且可能是嗜鳥龍的近親\[10\]。

在2010年，[奧利佛·勞赫](../Page/奧利佛·勞赫.md "wikilink")（Oliver
Rauhut）等人發表原角鼻龍的最新演化關係研究，並先在2009年發表於網路上。勞赫等人發現原角鼻龍屬於虛骨龍類的[暴龍超科](../Page/暴龍超科.md "wikilink")，牠們的最近親是[中國的](../Page/中國.md "wikilink")[冠龍](../Page/五彩冠龍.md "wikilink")。勞赫等人將原角鼻龍、冠龍歸類於[原角鼻龍科](../Page/原角鼻龍科.md "wikilink")，此科的範圍是：獸腳亞目之中，所以親緣關係較接近於原角鼻龍，而離[暴龍](../Page/暴龍.md "wikilink")、[異特龍](../Page/異特龍.md "wikilink")、[美頜龍](../Page/美頜龍.md "wikilink")、[虛骨龍](../Page/虛骨龍.md "wikilink")、[似鳥龍](../Page/似鳥龍.md "wikilink")、或[恐爪龍較遠的所有物種](../Page/恐爪龍.md "wikilink")\[11\]。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [BBC page on
    *Proceratosaurus*](http://news.bbc.co.uk/1/hi/sci/tech/8340922.stm)
  - [原角鼻龍基本數據](http://web.me.com/dinoruss/de_4/5a6661b.htm) -
    DinoRuss.com網站

[Category:暴龍超科](../Category/暴龍超科.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.  Holtz, Thomas R. Jr. (2008) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages*
    [Supplementary
    Information](http://www.geol.umd.edu/~tholtz/dinoappendix/DinoappendixSummer2008.pdf)

2.

3.

4.

5.
6.  Woodward, A.S. (1910). "On a skull of *Megalosaurus* from the Great
    Oolite of Minchinhampton (Gloucestershire)." *Quarterly Journal of
    the Geological Society of London*, **66**: 111–115.

7.  von Huene, F. (1932). "Die fossile Reptil-Ordnung Saurischia, ihre
    Entwicklung und Geschichte." *Monographien zur Geologie und
    Palaeontologie (Serie 1)*, **4**: 1–361.

8.

9.
10. Rauhut, O.W.M., Milner, A.C. and Moore-Fay, S. (2010). "Cranial
    osteology and phylogenetic position of the theropod dinosaur
    *Proceratosaurus bradleyi* (Woodward, 1910) from the Middle Jurassic
    of England". *Zoological Journal of the Linnean Society*, published
    online before print November 2009.

11.