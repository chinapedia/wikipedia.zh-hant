[群](../Page/群.md "wikilink"){{.w}}[环](../Page/环_\(代数\).md "wikilink"){{.w}}[-{zh-cn:域;
zh-tw:體;}-](../Page/域_\(數學\).md "wikilink"){{.w}}[有限-{zh-cn:域;
zh-tw:體;}-](../Page/有限域.md "wikilink"){{.w}}[本原元](../Page/本原元.md "wikilink"){{.w}}[格](../Page/格_\(數學\).md "wikilink"){{.w}}[-{zh-cn:逆元;
zh-tw:反元素;}-](../Page/逆元素.md "wikilink"){{.w}}[等价关系](../Page/等价关系.md "wikilink"){{.w}}[代數中心](../Page/中心_\(代數\).md "wikilink"){{.w}}[同态](../Page/同态.md "wikilink"){{.w}}[同构](../Page/同构.md "wikilink"){{.w}}[商结构](../Page/商结构.md "wikilink")<small>（商系统）</small>{{.w}}[同构基本定理](../Page/同构基本定理.md "wikilink"){{.w}}[合成列](../Page/合成列.md "wikilink"){{.w}}[自由對象](../Page/自由對象.md "wikilink")

|group1 = [群论](../Page/群论.md "wikilink") | list1 =
[半群](../Page/半群.md "wikilink"){{.w}}[阿贝尔群](../Page/阿贝尔群.md "wikilink"){{.w}}[非阿贝尔群](../Page/非阿贝尔群.md "wikilink"){{.w}}[循環群](../Page/循環群.md "wikilink"){{.w}}[有限群](../Page/有限群.md "wikilink"){{.w}}[-{zh-cn:单群;
zh-tw:單純群;}-](../Page/单群.md "wikilink"){{.w}}[半-{zh-cn:单群;
zh-tw:單純群;}-](../Page/半单群.md "wikilink"){{.w}}[-{zh-cn:典型群;
zh-tw:古典群;}-](../Page/典型群.md "wikilink"){{.w}}[自由群](../Page/自由群.md "wikilink"){{.w}}[交换子群](../Page/交换子群.md "wikilink")<small>（[交換子](../Page/交換子.md "wikilink")）</small>{{.w}}[幂零群](../Page/幂零群.md "wikilink"){{.w}}[可解群](../Page/可解群.md "wikilink"){{.w}}[p-群](../Page/p-群.md "wikilink"){{.w}}[对称群](../Page/对称群_\(n次对称群\).md "wikilink"){{.w}}[李群](../Page/李群.md "wikilink"){{.w}}[伽罗瓦群](../Page/伽罗瓦群.md "wikilink")

` |group2 = `[`子群`](../Page/子群.md "wikilink")
` | list2 = `[`陪集`](../Page/陪集.md "wikilink")`{{.w}}`[`双陪集`](../Page/双陪集.md "wikilink")`{{.w}}`[`商群`](../Page/商群.md "wikilink")`{{.w}}`[`共轭类`](../Page/共轭类.md "wikilink")`{{.w}}`[`拉格朗日定理`](../Page/拉格朗日定理_\(群论\).md "wikilink")`{{.w}}`[`西羅定理`](../Page/西羅定理.md "wikilink")`{{.w}}`[`正规子群`](../Page/正规子群.md "wikilink")`{{.w}}`[`群中心`](../Page/中心_\(群论\).md "wikilink")`{{.w}}`[`中心化子和正规化子`](../Page/中心化子和正规化子.md "wikilink")`{{.w}}`[`稳定子群`](../Page/稳定子群.md "wikilink")`{{.w}}`[`置换群`](../Page/置换群.md "wikilink")
` |group3 = 其他`
` | list3 = `[`阶`](../Page/阶_\(群论\).md "wikilink")`{{.w}}`[`群擴張`](../Page/群擴張.md "wikilink")`{{.w}}`[`群同態`](../Page/群同態.md "wikilink")`{{.w}}`[`群同構`](../Page/群同構.md "wikilink")`{{.w}}`[`群表示`](../Page/群表示論.md "wikilink")`{{.w}}`[`群作用`](../Page/群作用.md "wikilink")`{{.w}}`[`波利亞計數定理`](../Page/波利亞計數定理.md "wikilink")

}} |group2 = [環論](../Page/環_\(代數\).md "wikilink") | list2 =
[整环](../Page/整环.md "wikilink"){{.w}}[除环](../Page/除环.md "wikilink"){{.w}}[多项式环](../Page/多项式环.md "wikilink"){{.w}}[-{zh-cn:素环;
zh-tw:質環;}-](../Page/素环.md "wikilink"){{.w}}[商环](../Page/商环.md "wikilink"){{.w}}[諾特環](../Page/諾特環.md "wikilink"){{.w}}[局部環](../Page/局部環.md "wikilink"){{.w}}[賦值環](../Page/賦值環.md "wikilink"){{.w}}[環代數](../Page/代數_\(環論\).md "wikilink"){{.w}}[理想](../Page/理想_\(环论\).md "wikilink"){{.w}}[主理想环](../Page/主理想环.md "wikilink"){{.w}}[唯一分解整環](../Page/唯一分解整環.md "wikilink")

` |group2 = `[`模`](../Page/模.md "wikilink")
` | list2 = `[`深度`](../Page/深度_\(模論\).md "wikilink")`{{.w}}`[`單模`](../Page/單模.md "wikilink")`{{.w}}`[`自由模`](../Page/自由模.md "wikilink")`{{.w}}`[`平坦模`](../Page/平坦模.md "wikilink")`{{.w}}`[`阿廷模`](../Page/阿廷模.md "wikilink")`{{.w}}`[`諾特模`](../Page/諾特模.md "wikilink")
` |group3 = 其他`
` | list3 = `[`幂-{zh-cn:零元;``
 ``zh-tw:零元素;}-`](../Page/幂零元.md "wikilink")`{{.w}}`[`特征`](../Page/特征_\(代数\).md "wikilink")`{{.w}}`[`完備化`](../Page/完備化_\(環論\).md "wikilink")`{{.w}}`[`環的局部化`](../Page/環的局部化.md "wikilink")

}} |group3 = [-{zh-cn:域; zh-tw:體;}-论](../Page/域论.md "wikilink") | list3
= [原根](../Page/原根.md "wikilink"){{.w}}[代数闭-{zh-cn:域;
zh-tw:體;}-](../Page/代數閉域.md "wikilink"){{.w}}[局部-{zh-cn:域;
zh-tw:體;}-](../Page/局部域.md "wikilink"){{.w}}[分裂-{zh-cn:域;
zh-tw:體;}-](../Page/分裂域.md "wikilink"){{.w}}[分式環](../Page/分式環.md "wikilink")

` |group2 = `[`-{zh-cn:域;``
 ``zh-tw:體;}-扩张`](../Page/域扩张.md "wikilink")
` | list2 = `[`单扩张`](../Page/单扩张.md "wikilink")`{{.w}}`[`有限扩张`](../Page/有限扩张.md "wikilink")`{{.w}}`[`超越扩张`](../Page/超越扩张.md "wikilink")`{{.w}}`[`代数扩张`](../Page/代数扩张.md "wikilink")`{{.w}}`[`正规扩张`](../Page/正规扩张.md "wikilink")`{{.w}}`[`可分扩张`](../Page/可分扩张.md "wikilink")`{{.w}}`[`伽罗瓦扩张`](../Page/伽罗瓦扩张.md "wikilink")`{{.w}}`[`阿贝尔扩张`](../Page/阿贝尔扩张.md "wikilink")`{{.w}}`[`伽罗瓦理论基本定理`](../Page/伽罗瓦理论基本定理.md "wikilink")

}} }}<noinclude></noinclude>