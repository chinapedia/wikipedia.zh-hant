**周密**（），[宋末](../Page/南宋.md "wikilink")[元初人](../Page/元朝.md "wikilink")，字**公谨**，号**草窗**，又号**四水潜夫**、**弁阳老人**、**弁阳啸翁**。著有《[齐东野语](../Page/齐东野语.md "wikilink")》等書。

## 生平

祖籍[济南](../Page/济南.md "wikilink")（今属[山东](../Page/山东.md "wikilink")），後來曾祖[周秘南渡](../Page/周秘.md "wikilink")[吴兴](../Page/吴兴.md "wikilink")。早年以門蔭监建康府都钱库。景定二年（1261年）擔任浙西安抚司幕僚\[1\]。咸淳初为两浙运司掾属\[2\]。十年（1274年）监丰储仓\[3\]，端宗景炎闸知义乌县\[4\]。[元朝灭](../Page/元朝.md "wikilink")[南宋统一全国後](../Page/南宋.md "wikilink")，周密不願意做[元朝的官](../Page/元朝.md "wikilink")，於是在臨安隱居，並著書記錄舊朝南宋的各種事物，成書《[武林舊事](../Page/武林舊事.md "wikilink")》、《[癸辛雜識](../Page/癸辛雜識.md "wikilink")》。大德二年卒，年六十七。

周密为南宋末年[雅词词派领袖](../Page/雅词词派.md "wikilink")，有词集《萍洲渔笛谱》，词选《[绝妙好词](../Page/绝妙好词.md "wikilink")》流传于世。周密曾作有《三姝媚》送[王沂孙](../Page/王沂孙.md "wikilink")，王沂孙也赋词相和。周密、[张炎](../Page/张炎.md "wikilink")，和王沂孙、[蒋捷并称宋末四大词家](../Page/蒋捷.md "wikilink")。他虽出身望族，却无意仕进\[5\]，一生中大部分时间为[平民](../Page/平民.md "wikilink")，可谓一个「职业江湖雅人」，从其自号“草窗”便可见端倪，其词风格在[姜夔](../Page/姜夔.md "wikilink")、[吴文英之间](../Page/吴文英.md "wikilink")，与吴文英并称“二窗”。从周密留下来作品中更可以窥见他充满雅趣的生活和经历。他在词、[诗](../Page/诗.md "wikilink")、[书](../Page/书.md "wikilink")、[杂文](../Page/杂文.md "wikilink")、[画方面都有极高的造诣](../Page/画.md "wikilink")。比如他曾作《[志雅堂杂钞](../Page/志雅堂杂钞.md "wikilink")》录有关图画碑帖、诸玩、[宝器](../Page/宝器.md "wikilink")、[医药](../Page/医药.md "wikilink")、[阴阳](../Page/阴阳.md "wikilink")[算术](../Page/算术.md "wikilink")、[仙](../Page/神仙.md "wikilink")[佛](../Page/佛.md "wikilink")、[书](../Page/书.md "wikilink")[史等方面的知识](../Page/史.md "wikilink")。又撰《[云烟过眼录](../Page/云烟过眼录.md "wikilink")》记载当时各家所藏奇珍古玩（如玉器、古琴之类）及评论书画。其《[澄怀录](../Page/澄怀录.md "wikilink")》则是前人片断散文的辑录，其中多是古人写自然风光或田园生活的，表达他对林泉高致的向往。

## 注釋

## 參考書目

  - 《宋史翼》卷三四
  - 夏承焘《草窗著述考》

[Z](../Category/宋朝作家.md "wikilink") [宋](../Category/元朝作家.md "wikilink")
[Z](../Category/宋朝詞人.md "wikilink") [宋](../Category/元朝詞人.md "wikilink")
[Z](../Category/宋朝詩人.md "wikilink") [宋](../Category/元朝詩人.md "wikilink")
[Z](../Category/宋朝畫家.md "wikilink") [宋](../Category/元朝畫家.md "wikilink")
[M](../Category/周姓.md "wikilink")

1.  《癸辛杂识》后集
2.  《清容居士集》卷三三《师友渊源录》
3.  《癸辛杂识》前集、续集卷上
4.  清嘉庆《义乌县志》卷八
5.  [王行](../Page/王行.md "wikilink")《题周草窗画像》载周密“以无所责守而志节不屈著称”