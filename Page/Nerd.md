{{ mergefrom |书呆子}}
**Nerd**通常被翻譯成**怪胎**、**怪咖**，是[英語](../Page/英語.md "wikilink")[俚语中一个稍含贬义的用语](../Page/俚语.md "wikilink")，一般指偏爱钻研书本知识，将大量闲暇用于脑力工作，对流行文化不感兴趣，而不愿或不善于体育活动或其他社会活动的人。在美国中学和大学文化中则通指那些专心一致刻苦学习，不会和人交往又行为举止尴尬的学生。在華人圈內類似的概念是“[书呆子](../Page/书呆子.md "wikilink")”或“[书虫](../Page/书虫.md "wikilink")”，古稱**兩腳書櫥**。在美国大部分学校，這樣的人依然顯著地被排擠，他们不会和人交往，经常做出尴尬的事情。

Nerd在[美式英语中相对的词语是](../Page/美式英语.md "wikilink")**Jock**，指擅长体育、四肢发达、自信且熱愛泡妞的人。

注意，nerd不是说学习好的就被称为nerd，而是学习好的但是没有社会技能才被称为nerd。

## 辞源

英文“nerd”一词最早出现在[苏斯博士](../Page/苏斯博士.md "wikilink")1950年出版的《If I Ran the
Zoo》一书中，但是现今的俗语含义则是因为《新闻周刊》杂志在1951年报道了此词在[密歇根州](../Page/密歇根州.md "wikilink")[底特律市经常被用来与](../Page/底特律.md "wikilink")“drip”（指身材瘦小、性格软弱、不受人关注）和“square”（指为人老实、重义气、守规矩）等词同义。在60年代初期其使用传遍美国全国甚至远至[苏格兰](../Page/苏格兰.md "wikilink")，随后一段时间其词义逐渐与“两耳不闻窗外事，一心只读圣贤书”之类的形容挂钩，这个词义在70年代因为著名[情境喜剧](../Page/情境喜剧.md "wikilink")《青春年華》（Happy
Days）开始变为主流。

## 特征

根据影视作品的描述和大众心目中的印象，Nerd往往有以下特征：

  - 身体柔弱、单薄或臃肿，（男子）行为欠缺阳刚，嗓音柔弱尖细
  - 学习成绩优异，[智商较高](../Page/智商.md "wikilink")
  - 古板的发型，毫无时尚的着装，通常带有大号[眼镜](../Page/眼镜.md "wikilink")、[牙套](../Page/牙套.md "wikilink")、高腰长裤，目光渙散，表情膽怯，面容不开朗
  - 缺乏自信，不善与人交谈，恐於主動認識異性
  - 喜欢循规蹈矩并且在细节上较真，有[强迫症和](../Page/强迫症.md "wikilink")[阿斯伯格综合征的特征](../Page/阿斯伯格综合征.md "wikilink")
  - 喜欢钻研一般人無法理解或远远超出同龄人成熟度的学科，如[古生物学](../Page/古生物学.md "wikilink")、[历史学](../Page/历史学.md "wikilink")、[高等数学](../Page/高等数学.md "wikilink")、[物理](../Page/物理.md "wikilink")、[电脑科学和](../Page/电脑科学.md "wikilink")[美术](../Page/美术.md "wikilink")，或者兴趣集中于[桌上游戏](../Page/桌上游戏.md "wikilink")、[闪卡](../Page/闪卡_\(嗜好\).md "wikilink")、[电子游戏](../Page/电子游戏.md "wikilink")、[漫画](../Page/漫画.md "wikilink")、[科幻](../Page/科幻.md "wikilink")/[奇幻小说等大多数他人不感兴趣的方面](../Page/奇幻小说.md "wikilink")。
  - 对[流行文化和](../Page/流行文化.md "wikilink")[音乐以及体育新闻等方面没有兴趣](../Page/流行音乐.md "wikilink")，或者極度缺乏相關的概念
  - [情商较低](../Page/情商.md "wikilink")（愛哭或常鬧彆扭），不喜欢且不善于人际交往，经常成为同学欺负、排挤、嫉妒的对象
  - 缺少男性的玩伴，也極不受男同學歡迎；但可能有幾個比較乖的女孩會主動示好
  - 跟[母親的關係比](../Page/母親.md "wikilink")[父親好得多](../Page/父親.md "wikilink")，情感上較依賴母親，跟家庭中其他男性成員關係都較疏離

根据美国2010年的一份研究表明，在美国[亚裔最有可能被形容为Nerd](../Page/亚裔美国人.md "wikilink")，其次是[白人](../Page/美国白人.md "wikilink")，反之[拉丁裔和](../Page/拉丁裔美国人.md "wikilink")[黑人被看作是Nerd的比例最小](../Page/非裔美国人.md "wikilink")。

## Nerd的自我認同

而自20世纪80年代后，科学技术尤其是信息技术产业的发达使得许多具有Nerd性质的人（比如比尔·盖茨）变得非常富有，在社会上处于很大的优势地位，使得许多追求脑力的人都以自己是“Nerd”为豪。1984年的喜剧电影《Revenge
of the Nerds》系列中主人公一行的正直形象使得“nerd
pride”在90年代开始觉醒。[麻省理工学院教授](../Page/麻省理工学院.md "wikilink")[杰拉德·杰伊·萨斯曼就曾说](../Page/杰拉德·杰伊·萨斯曼.md "wikilink")：“我的想法是让孩子们知道有智慧是好事，不要去理会[反智主义的](../Page/反智主义.md "wikilink")[朋辈压力](../Page/朋辈压力.md "wikilink")。我希望每个孩子都成为Nerd——也就是指用学习去竞争社会优势的人，虽然有时也可能不幸会被排斥。”美国作家查尔斯·赛克斯的名言“对Nerd们好点——以后你可能会给他打工”在网络时代则大举流行。而美国动画片《Freakazoid》中的一段独白则更是表明了Nerd的价值：“他们用脑力弥补体能上的不足。畅销书都是谁写的？是Nerd们。好莱坞爆场大片是谁导演的？是Nerd们。只有内行才懂的高端技术是谁创造的？是Nerd们……此外进入总统府高层办公室的都有谁？非Nerd们莫属！”

在西班牙，自2006年以来每年的5月25日被民间人士定为“Nerd自豪日”。

## 相关作品

  - 《Revenge of the Nerds》(1984年)
  - 《[生活大爆炸](../Page/生活大爆炸.md "wikilink")》 （，2007年－）
  - 《[The Angry Video Game
    Nerd](../Page/The_Angry_Video_Game_Nerd.md "wikilink")》
  - 《[哈利波特](../Page/哈利波特.md "wikilink")》
  - 《[事發的19分鐘](../Page/事發的19分鐘.md "wikilink")》（內容為一名有Nerd特徵的男孩遭到霸凌的過程）

## 參見

  - [宅男](../Page/宅男.md "wikilink")/宅女
  - [御宅族](../Page/御宅族.md "wikilink")
  - [Geek](../Page/Geek.md "wikilink")
  - Weirdo

## 延伸閱讀

  -
  -
  - [*Genuine Nerd* (2006)](http://www.imdb.com/title/tt0820887/) -
    Feature-length documentary on Toby Radloff.

  -
  -
  -
  -
  - Newitz, A. & Anders, C. (Eds) *She's Such a Geek: Women Write About
    Science, Technology, and Other Nerdy Stuff*. Seal Press, 2006.

  - Okada, Toshio. *Otaku Gaku Nyumon* (Translated: 'Introduction to
    [Otakuology](../Page/Otaku.md "wikilink")'). Ohta Verlag. Tokyo,
    1996.

## 外部链接

  - ["Why Nerds are Unpopular"](http://www.paulgraham.com/nerds.html) An
    essay by Paul Graham about the conformist society in American
    highschools.
  - [Wikihow.com](http://www.wikihow.com/Tell-the-Difference-between-Nerds-and-Geeks)
    A short description of the differences between geeks and nerds.
  - ["The Nerds have
    won"](http://www.americanscientist.org/issues/pub/the-nerds-have-won)，an
    article in the [American
    Scientist](../Page/American_Scientist.md "wikilink")

[Category:成见](../Category/成见.md "wikilink")
[仁](../Category/俗语.md "wikilink")
[Category:反智主义](../Category/反智主义.md "wikilink")
[Category:定型角色](../Category/定型角色.md "wikilink")
[Category:學校文化](../Category/學校文化.md "wikilink")
[Category:電腦文化](../Category/電腦文化.md "wikilink")
[Category:网络文化](../Category/网络文化.md "wikilink")
[Category:贬义词](../Category/贬义词.md "wikilink")
[Category:次文化](../Category/次文化.md "wikilink")
[Category:特定人群稱謂](../Category/特定人群稱謂.md "wikilink")