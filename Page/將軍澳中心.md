[Park_Central.jpg](https://zh.wikipedia.org/wiki/File:Park_Central.jpg "fig:Park_Central.jpg")
[Top_treatment_of_Parl_Central_Tower_13.jpg](https://zh.wikipedia.org/wiki/File:Top_treatment_of_Parl_Central_Tower_13.jpg "fig:Top_treatment_of_Parl_Central_Tower_13.jpg")
[Top_treatment_of_Parl_Central_Phase_2.jpg](https://zh.wikipedia.org/wiki/File:Top_treatment_of_Parl_Central_Phase_2.jpg "fig:Top_treatment_of_Parl_Central_Phase_2.jpg")
**將軍澳中心**（**Park
Central**），當地居民簡稱**將中**，位於[港鐵](../Page/港鐵.md "wikilink")[將軍澳站旁](../Page/將軍澳站.md "wikilink")，是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[將軍澳南的一項大型物業連商場](../Page/將軍澳南.md "wikilink")。全屋苑有12座(1\~12座及將軍澳豪庭，
當中沒有第4座)，平均樓高約50層，合共提供4,542個單位\[1\]\[2\]。物業由[新鴻基地產](../Page/新鴻基地產.md "wikilink")、[恆基兆業](../Page/恆基兆業.md "wikilink")、[華懋集團](../Page/華懋集團.md "wikilink")、[地鐵公司](../Page/地鐵公司.md "wikilink")（今[港鐵公司](../Page/香港鐵路有限公司.md "wikilink")）（港鐵公司只運用將軍澳線地役權，參與第3期之發展計劃）及[南豐發展聯合發展](../Page/南豐集團.md "wikilink")，1998年起始興建，於2001年至2005年公開發售。整個商住物業發展均由[新鴻基地產旗下康業服務有限公司及將軍澳中心管理有限公司負責管理](../Page/新鴻基地產.md "wikilink")。

## 背景資料

將軍澳中心位於[將軍澳市中心唐德街](../Page/將軍澳市中心.md "wikilink")9號，[港鐵](../Page/港鐵.md "wikilink")[將軍澳站旁](../Page/將軍澳站.md "wikilink")，附近為[將軍澳站公共交通交匯處](../Page/將軍澳站公共交通交匯處.md "wikilink")。整個發展共分3期，第1期共有5座，1,872個單位，於2002年尾入伙；第2期全6座提供2,280個單位，2003年秋季入伙。單位面積介符500至900餘平方呎。第3期[將軍澳豪庭為將軍澳中心第](../Page/將軍澳豪庭.md "wikilink")13座，提供390個單位，於2005年2月開售，亦已入伙。

## 會所設施

將軍澳中心設有逾25萬平方呎的會所和園藝花園，提供文娛、康樂等不同設施，包括水力按摩池、[按摩室](../Page/按摩室.md "wikilink")，以及當年全區罕有的石卵足底按摩徑，亦引入多項家庭親子康樂設施，例如家庭式浸浴/蒸氣浴及桑拿中心、模型車賽道和玩具圖書館。會所內還設有螢光[保齡球場](../Page/保齡球場.md "wikilink")、[桌球室](../Page/桌球室.md "wikilink")、室內多用途運動場、燒烤樂園等設施。室內恆溫泳池則設有熱泵式系統，回收室內剩餘熱量循環至泳池溫水系統，增加能源效益。

平台花園遍植四季花木，並設有寬闊草坪及多個特色水池。物業備有天幕扶手電梯，由屋苑直達物業基座的大型商場。

## 商場

[Park_Central_Void_201802.jpg](https://zh.wikipedia.org/wiki/File:Park_Central_Void_201802.jpg "fig:Park_Central_Void_201802.jpg")
[Park_Central_G-F_Shops_201802.jpg](https://zh.wikipedia.org/wiki/File:Park_Central_G-F_Shops_201802.jpg "fig:Park_Central_G-F_Shops_201802.jpg")
[Park_Central_Mal_Park_Campus_201405.jpg](https://zh.wikipedia.org/wiki/File:Park_Central_Mal_Park_Campus_201405.jpg "fig:Park_Central_Mal_Park_Campus_201405.jpg")
[Park_Central_Entrance_void_view_201802.jpg](https://zh.wikipedia.org/wiki/File:Park_Central_Entrance_void_view_201802.jpg "fig:Park_Central_Entrance_void_view_201802.jpg")
將軍澳中心的商場第一期和第二期於2003年年底開幕，共有129間商舖\[3\]，佔地逾350,000平方呎，共分3層。商場的定位及市場，主要以小型家庭、親子，年輕化為主導。商場主要商戶為食肆、時裝、親子店等，當中食肆佔兩成半。商場第三期50,000平方呎樓面在2005年7月已取得入伙紙，提供25個商舖，商舖面積主要為300至600方呎的小型商舖，整個商場項目均由康業服務有限公司管理。

商場的樓層命名方式和一般的商場不同，唐德街入口的一層稱為地庫（B1），如超市Jasons Ichiba
(前惠康超級廣場)的地址為將軍澳唐德街九號將軍澳中心地庫第一層。地庫層再上為地下（G），再上為一樓，這和一般商場以街道入口的一層為地下層的慣例明顯不同。

2005年12月，一樓佔地12,000平方呎美食廣場Deli
Park開業，共有8個舖位和262個座位。商場業主[新鴻基地產於](../Page/新鴻基地產.md "wikilink")2006年打算在二期商場地下，興建面積達6,000方呎的大型滾軸溜冰場，溜冰場於2007年開業。\[4\]。

2008年7月，商場業主[新鴻基地產指區內發展成熟](../Page/新鴻基地產.md "wikilink")，中產家庭漸多，有條件將商場租客重組，引入更多親子教育等新元素租客。例如補習社租用商場8,000平方呎樓面及另一個幼兒教育機構正洽購14,000平方呎樓面。\[5\]。商場亦有透過邀請歌星及舉辦特色活動增加人流，如《四大名錦·回歸十年》等\[6\]。

2013年8月，商場業主將位於2樓20,000平方呎區域打造Park
Campus教育專區，召集八大幼兒學府，為小朋友提供多方面輔助教育。\[7\]

<File:Park> Central Mall Void 2011.jpg|翻新前的中庭（2011年） <File:Park> Central
Food Court 2016.jpg|商場1樓曾設美食廣場，於2017年5月關閉 <File:Park> Central Phase 2
Mall
201112.jpg|將軍澳中心商場第三期主要租戶曾為兒童補習社，唯自2012年往[PopCorn商場通道開通後](../Page/PopCorn.md "wikilink")，已改為零售商店
<File:Park> Central Shopping Centre (Hong Kong).jpg|將軍澳中心1樓，右方商店曾為Dr.
Kong 健康鞋專門店 <File:Park> Central Level 2 view 201802.jpg|翻新後的將軍澳中心1樓

2015年1月，商場業主計劃斥資3.15億元翻新商場，預計2018年底完成。翻新後店舖將由現時141間，增加至200間，中庭加設一至兩間複式舖位，佔地1.8萬平方呎，期望租金收入倍增。商場內現有大型超市獲保留，50間新租客於2017年下旬陸續進駐，第一期優化項目在同年11月26日正式完成。\[8\]新租戶包括全新餐廳「Say
Cheese」、「Cherry Tokyo Café」及「漁釧」，「美中鴨子」、「The
Point」等首次進駐將軍澳區。同時開設多間珠寶和美容化妝店。第二期優化工程預計2019年中完成。

## 各層商舖及食肆

### B1層

主要租戶包括[J.Co Donuts &
Coffee](../Page/J.Co_Donuts_&_Coffee.md "wikilink")、[薄餅博士](../Page/薄餅博士.md "wikilink")、[玉子造屋](../Page/玉子造屋.md "wikilink")、[PizzaExpress](../Page/PizzaExpress.md "wikilink")、[Jasons
Ichiba](../Page/Jasons_Ichiba.md "wikilink")、[OK便利店](../Page/OK便利店.md "wikilink")、[翠河餐廳](../Page/翠河餐廳.md "wikilink")、[香港置業](../Page/香港置業.md "wikilink")、[中原地產](../Page/中原地產.md "wikilink")、[美聯物業](../Page/美聯物業.md "wikilink")、[利嘉閣](../Page/利嘉閣.md "wikilink")

### G/F層

主要租戶包括 [2/3 DOLCI](../Page/2/3_DOLCI.md "wikilink")、[The
Point](../Page/The_Point.md "wikilink")、[Pizza +
Pasta](../Page/Pizza_+_Pasta.md "wikilink")、[美中．鴨子](../Page/美中．鴨子.md "wikilink")、[Green
Common](../Page/Green_Common.md "wikilink")、[漁釧日本居酒屋](../Page/漁釧日本居酒屋.md "wikilink")、[囍越](../Page/囍越.md "wikilink")、[CHERRY
TOKYO
CAFE](../Page/CHERRY_TOKYO_CAFE.md "wikilink")、[Mukia日式吐司](../Page/Mukia日式吐司.md "wikilink")、[Bless](../Page/Bless.md "wikilink")、[余仁生](../Page/余仁生.md "wikilink")、[農本方中醫診所](../Page/農本方中醫診所.md "wikilink")、[華潤堂](../Page/華潤堂.md "wikilink")、[和順堂](../Page/和順堂.md "wikilink")、[榮華藥房](../Page/榮華藥房.md "wikilink")、[英皇鐘錶珠寶](../Page/英皇鐘錶珠寶.md "wikilink")、[六福珠寶](../Page/六福珠寶.md "wikilink")、[MaBelle](../Page/MaBelle.md "wikilink")、[鯉魚門紹香園](../Page/鯉魚門紹香園.md "wikilink")、[綠盈坊有機店](../Page/綠盈坊有機店.md "wikilink")、[7-11](../Page/7-11.md "wikilink")、[Jurlique](../Page/Jurlique.md "wikilink")、[Bioscreen
Organic
Beauty](../Page/Bioscreen_Organic_Beauty.md "wikilink")、[CANVAS](../Page/CANVAS.md "wikilink")、[Benefit](../Page/Benefit.md "wikilink")、[Salon
Anca](../Page/Salon_Anca.md "wikilink")

### L1層

主要租戶包括[HallmarkBabies](../Page/HallmarkBabies.md "wikilink")、[3
(電訊)](../Page/3_\(電訊\).md "wikilink")、[廣匯通訊](../Page/廣匯通訊.md "wikilink")、[Maxims
Cake Lab](../Page/Maxims_Cake_Lab.md "wikilink")、[Say
Cheese](../Page/Say_Cheese.md "wikilink")、[6IXTY8IGHT](../Page/6IXTY8IGHT.md "wikilink")、[冬蔭公公](../Page/冬蔭公公.md "wikilink")、[蠔站
O S
Plus](../Page/蠔站_O_S_Plus.md "wikilink")、[糧友麵包工房](../Page/糧友麵包工房.md "wikilink")、[滿記甜品](../Page/滿記甜品.md "wikilink")、[偉成洋酒](../Page/偉成洋酒.md "wikilink")、[膳魔師](../Page/膳魔師.md "wikilink")、[滙豐理財易中心](../Page/滙豐.md "wikilink")、[Handscript](../Page/Handscript.md "wikilink")、[GET
FRESH](../Page/GET_FRESH.md "wikilink")、[普嵐緹](../Page/普嵐緹.md "wikilink")

### L2層

主要租戶包括[潮庭](../Page/潮庭.md "wikilink")、和造地20,000平方呎的Park Campus教育專區

其他新商戶將稍後公佈

## 重大事件

2014年4月18日晚上6時36分，商場內一個冷氣機房突然發生火警，冒出滾滾濃煙，刺鼻黑煙迅速攻入店舖及竄上二樓，並沿通道沖出大廈外，冒升至半空達五十層樓高，黑煙籠罩整個物業及樓上第五及六座住戶，部分濃煙更乘風吹入附近大廈單位，猶如恐怖襲擊。

商場職員隨即報警及落閘封鎖其中一半起火範圍，以免濃煙及火勢蔓延，同時發出廣播通知顧客逃生，逾百名顧客及商戶職員緊急疏散。消防員出動8輛消防車、1輛救護車及42名消防員，開動一喉射水灌救，花約一小時將火救熄，事件中無人受傷。經調查後，相信火警因機房漏電或過熱引起。受火警影響，商店被迫停業，其中一間上海菜食肆負責人表示，事發時店內仍有顧客，顧客聞悉火警後都有付款才離開，估計損失約五萬元營業額。\[9\]

## 途經的公共交通服務

<div class="NavFrame collapsed" style="background-color:yellow; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color:yellow; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color:yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{將軍澳綫色彩}}">█</font>[將軍澳綫](../Page/將軍澳綫.md "wikilink")：[將軍澳站B出口](../Page/將軍澳站.md "wikilink")
  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")、<font color="{{將軍澳綫色彩}}">█</font>[將軍澳綫](../Page/將軍澳綫.md "wikilink")：
    [調景嶺站A出口](../Page/調景嶺站.md "wikilink")

<!-- end list -->

  - [尚德巴士總站](../Page/尚德巴士總站.md "wikilink")

<!-- end list -->

  - [將軍澳站公共運輸交匯處](../Page/將軍澳站公共運輸交匯處.md "wikilink")

<!-- end list -->

  - [寶邑路](../Page/寶邑路.md "wikilink")

<!-- end list -->

  - [唐明街](../Page/唐明街.md "wikilink")

<!-- end list -->

  - [唐賢街](../Page/唐賢街.md "wikilink")

<!-- end list -->

  - [唐俊街](../Page/唐俊街.md "wikilink")

<!-- end list -->

  - [唐德街](../Page/唐德街.md "wikilink")

</div>

</div>

## 連結

  - [將軍澳中心](http://www.park-central.com.hk/)
  - [將中之家-將軍澳中心住户討論區](http://www.park-central.org/)
  - [將軍澳中心簡介](http://www.shkp.com/zh-hk/scripts/property/property_mall_parkcentral.php)
    - [新鴻基地產](../Page/新鴻基地產.md "wikilink")

## 全景圖片

## 注釋

[Category:西貢區商場](../Category/西貢區商場.md "wikilink")
[Category:西貢區私人屋苑](../Category/西貢區私人屋苑.md "wikilink")
[Category:南豐發展物業](../Category/南豐發展物業.md "wikilink")
[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:將軍澳](../Category/將軍澳.md "wikilink")
[Category:港鐵公司物業](../Category/港鐵公司物業.md "wikilink")
[Category:恒基兆業物業](../Category/恒基兆業物業.md "wikilink")
[Category:華懋集團物業](../Category/華懋集團物業.md "wikilink")
[Category:香港建築之最](../Category/香港建築之最.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  \[<http://hk.apple.nextmedia.com/supplement/family/art/20130823/18390306>|
    title = 兩萬呎教育城吸港童 《蘋果日報》2013年8月23日 \]
8.  PopWalk最快年底開幕 《新報》2015年1月21日
9.  [商場失火濃煙沖天似恐襲，太陽報
    (2014年4月19日)](http://the-sun.on.cc/cnt/news/20140419/00407_045.html)