**白朗**（）[字](../Page/表字.md "wikilink")**明心**，[幼名](../Page/幼名.md "wikilink")**六儿**，是[中华民国初年](../Page/中华民国.md "wikilink")[河南](../Page/河南.md "wikilink")[馬賊](../Page/馬賊.md "wikilink")、[农民军领袖](../Page/农民军.md "wikilink")，河南[宝丰人](../Page/宝丰.md "wikilink")。白朗清末時是低级军官，因响应局势而反清，靠[劫富济贫获得穷人支持](../Page/劫富济贫.md "wikilink")，并收编众多[绿林匪盗](../Page/绿林.md "wikilink")，后又成为积极反抗[袁世凯之地方](../Page/袁世凯.md "wikilink")[民变领袖](../Page/民变.md "wikilink")，最终戰死沙場。。

## 生平

白朗[清末时曾在](../Page/清.md "wikilink")[陆军第六镇](../Page/陆军第六镇.md "wikilink")[统制](../Page/统制.md "wikilink")[吴禄贞手下充当](../Page/吴禄贞.md "wikilink")[参谋](../Page/参谋.md "wikilink")[軍官](../Page/軍官.md "wikilink")。1911年[辛亥革命爆发](../Page/辛亥革命.md "wikilink")，吴被[暗杀](../Page/暗杀.md "wikilink")，白朗在河南[宝丰起兵](../Page/宝丰.md "wikilink")，起初仅有二十余[士兵和一支](../Page/士兵.md "wikilink")[步枪](../Page/步枪.md "wikilink")，后很快[招募周围](../Page/募兵.md "wikilink")[團練](../Page/團練.md "wikilink")，至1912年，已有500余人，在河南西部一带[游击](../Page/游击.md "wikilink")，每到一处，白朗军劫掠[官家及](../Page/官家.md "wikilink")[绅士](../Page/绅士.md "wikilink")、[富豪财物](../Page/富豪.md "wikilink")，布施穷人，並鼓励穷人造反\[1\]，自称为「中原扶汉军大[都督](../Page/都督.md "wikilink")」。

1913年5月31日，白郎发动[农民起义攻占](../Page/农民起义.md "wikilink")[唐县](../Page/唐县.md "wikilink")，反对[袁世凯](../Page/袁世凯.md "wikilink")[政权](../Page/政权.md "wikilink")，威胁[京汉铁路的安全](../Page/京汉铁路.md "wikilink")。因民国初年，河南连年饥荒，加上[北洋政府的河南都督](../Page/北洋政府.md "wikilink")[张镇芳横征暴敛](../Page/张镇芳.md "wikilink")，常有被裁士兵、贫民、饥民铤而走险，成為[馬賊](../Page/馬賊.md "wikilink")\[2\]。这些盗匪中有许多人响应白朗的起义。为保证这条交通干线的畅通，袁世凯调集[湖北](../Page/湖北.md "wikilink")、河南和[陕西三省联军对白朗进行围剿](../Page/陕西.md "wikilink")。不料联军中的陕西[王生歧部阵前投敵](../Page/王生歧.md "wikilink")，参加白朗军，使得白朗军的军力和装备都得到了很大的改善。白朗遂自称“中华民国扶汉讨袁司令大都督”。

1914年初，白朗率众突破袁军的包围圈，进入[安徽](../Page/安徽.md "wikilink")。2月13日，袁世凯任命[段祺瑞为总司令](../Page/段祺瑞.md "wikilink")，率领[北洋精锐追击白朗](../Page/北洋军.md "wikilink")，并派出了4架[飞机对白朗军进行侦察](../Page/飞机.md "wikilink")，。白朗迫于压力，离[皖入](../Page/皖.md "wikilink")[鄂](../Page/鄂.md "wikilink")。3月8日，白朗军占领[老河口](../Page/老河口.md "wikilink")，全军发展到2万余人。在老河口，白朗军打死[挪威](../Page/挪威.md "wikilink")[基督教传教士](../Page/基督教.md "wikilink")[费兰德医生](../Page/费兰德.md "wikilink")，打伤[沙麻牧师](../Page/沙麻.md "wikilink")，酿成“[老河口案](../Page/老河口案.md "wikilink")”\[3\]，引起西方国家对袁世凯的强烈抗议。

此后，白朗决意西进，3月17日入[秦](../Page/秦.md "wikilink")，后多次击败当地驻军，经[天水](../Page/天水.md "wikilink")、[岷县等地进入](../Page/岷县.md "wikilink")[甘肃南部](../Page/甘肃.md "wikilink")[藏族聚居地区](../Page/藏族.md "wikilink")。由于无法在当地立足，[糧食與](../Page/糧食.md "wikilink")[彈藥也无处补充](../Page/彈藥.md "wikilink")，白朗只得率军返回河南。白朗军虽然成功突破了袁军[劉鎮華的三道防线](../Page/劉鎮華.md "wikilink")，于6月返回河南，但是途中损失惨重，只得分散进行[游击战](../Page/游击战.md "wikilink")。不久，白朗军被袁军各个击破。白朗也于8月3日的[寶豐戰鬥](../Page/寶豐戰鬥.md "wikilink")[阵亡](../Page/阵亡.md "wikilink")。白朗死後被劉鎮華斬下首級，[函首](../Page/斬首.md "wikilink")[燕京](../Page/燕京.md "wikilink")。

## 影响

白朗在河南地区所领导的反袁战争在河南当地引起轰动，被视为英雄人物传颂一时，尤其是对当时年纪尚小的[马尚德触动很大](../Page/马尚德.md "wikilink")。\[4\]\[5\]马尚德以白朗为偶像，立志长大以后也像白朗一样通过革命斗争的道路改变社会的旧面貌。\[6\]\[7\]马尚德年轻时领导过河南当地的红军部队，后又化名“[杨靖宇](../Page/杨靖宇.md "wikilink")”领导了中国[东北抗日联军](../Page/东北抗日联军.md "wikilink")，并成为抗战时期中国东北地区最著名的抗日将领。

## 另见

  - [鲁山起义](../Page/鲁山起义.md "wikilink")

## 参考资料

## 扩展阅读

  - 《中国近代历史大事详解---浩劫风暴》，2006年，吉林文史出版社 ISBN 7807022140

[Category:中華民國大陸時期軍事人物](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:中國革命家](../Category/中國革命家.md "wikilink")
[Category:中華民國戰爭身亡者](../Category/中華民國戰爭身亡者.md "wikilink")
[Category:宝丰人](../Category/宝丰人.md "wikilink")
[Lang](../Category/白姓.md "wikilink")

1.  [中国近代史大事详解-白朗起义](http://www.yuncheng.com/reader/#30427/6648256/16)

2.
3.  秦学贞《老河口地区的基督教传入与发展》。

4.

5.

6.
7.