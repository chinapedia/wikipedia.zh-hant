**岳州**，[中国古代的](../Page/中国.md "wikilink")[州](../Page/州.md "wikilink")。

[隋朝](../Page/隋朝.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年）改[巴州置州](../Page/巴州_\(南梁\).md "wikilink")。[治所在](../Page/治所.md "wikilink")[巴陵县](../Page/巴陵县.md "wikilink")（今[岳阳市](../Page/岳阳市.md "wikilink")）。[唐朝时](../Page/唐朝.md "wikilink")，辖今[湖南](../Page/湖南.md "wikilink")[洞庭湖东](../Page/洞庭湖.md "wikilink")、南、北沿岸各县地，后略小。[南宋](../Page/南宋.md "wikilink")[绍兴二十五年](../Page/紹興_\(南宋\).md "wikilink")（1155年），因名同[岳飞姓](../Page/岳飞.md "wikilink")，改为[纯州](../Page/纯州.md "wikilink")\[1\]，旋恢复旧称。[元朝改为](../Page/元朝.md "wikilink")[岳州路](../Page/岳州路.md "wikilink")，[明朝改为](../Page/明朝.md "wikilink")[岳州府](../Page/岳州府.md "wikilink")。1913年废。

## 行政区划

  - [巴陵縣](../Page/巴陵縣.md "wikilink")
  - [華容縣](../Page/華容縣.md "wikilink")（本[安南縣](../Page/安南縣.md "wikilink")，[開皇十八年更名華容縣](../Page/開皇.md "wikilink")，[垂拱二年更名](../Page/垂拱.md "wikilink")[容城縣](../Page/容城縣.md "wikilink")，[神龍元年復名華容縣](../Page/神龍.md "wikilink")）
  - [橋江縣](../Page/橋江縣.md "wikilink")（本[藥山縣](../Page/藥山縣.md "wikilink")，開皇九年更名[安樂縣](../Page/安樂縣.md "wikilink")，十八年更名[沅江縣](../Page/沅江縣.md "wikilink")，[乾寧年間更名](../Page/乾寧.md "wikilink")[橋江縣](../Page/橋江縣.md "wikilink")，宋室南渡后改隸[常德府](../Page/常德府.md "wikilink")）
  - [湘陰縣](../Page/湘陰縣.md "wikilink")（本[岳陽縣](../Page/岳阳县_\(南北朝县份\).md "wikilink")，開皇十一年更名湘陰縣，併[玉山县](../Page/玉山县.md "wikilink")，[武德八年併羅縣](../Page/武德.md "wikilink")，[淳化四年改隸](../Page/淳化.md "wikilink")[潭州](../Page/潭州.md "wikilink")）
  - [平江縣](../Page/平江縣.md "wikilink")（神龍三年析湘陰縣置[昌江縣](../Page/昌江縣.md "wikilink")，[同光元年改平江縣](../Page/同光.md "wikilink")）
  - [羅　縣](../Page/羅縣.md "wikilink")（開皇九年併[吴昌](../Page/吴昌縣.md "wikilink")、[湘滨二縣](../Page/湘滨縣.md "wikilink")，武德八年省入湘陰縣）
  - [臨湘縣](../Page/臨湘縣.md "wikilink")（淳化元年升王朝場為[王朝縣](../Page/王朝縣.md "wikilink")，改臨湘縣）

## 行政长官

### 唐代

  - 岳州刺史
      - [王武宣](../Page/王武宣.md "wikilink")
        [貞觀年間](../Page/貞觀.md "wikilink")
      - [楊思禮](../Page/楊思禮.md "wikilink") 貞觀年間

<!-- end list -->

  - 巴陵太守
      - [高元彧](../Page/高元彧.md "wikilink") 貞觀年間

<!-- end list -->

  - 岳州刺史
      - [薛仁方](../Page/薛仁方.md "wikilink")
        [高宗年間](../Page/唐高宗.md "wikilink")
      - [楊　諲](../Page/楊諲.md "wikilink") 高宗年間
      - [李素節](../Page/李素節.md "wikilink")
        [永隆元年](../Page/永隆.md "wikilink")（680年）
      - [郭待舉](../Page/郭待舉.md "wikilink")
        [光宅元年](../Page/光宅.md "wikilink")（684年）
      - [崔言道](../Page/崔言道.md "wikilink")
        [武后年間](../Page/武后.md "wikilink")
      - [鄭子況](../Page/鄭子況.md "wikilink") 武后年間
      - [張　說](../Page/張說.md "wikilink")
        [開元四年](../Page/開元.md "wikilink")-開元五年（716年-717年）
      - [牛　肅](../Page/牛肅.md "wikilink") 開元年間

<!-- end list -->

  - 巴陵太守
      - [盧幼臨](../Page/盧幼臨.md "wikilink")
        [天寶五載](../Page/天寶.md "wikilink")（746年）
      - [韋　斌](../Page/韋斌.md "wikilink") 天寶五載（746年）
      - [劉　縉](../Page/劉縉.md "wikilink") 天寶年間

<!-- end list -->

  - 岳州刺史
      - [夏侯宋客](../Page/夏侯宋客.md "wikilink")
        [上元元年](../Page/上元.md "wikilink")-廣德元年（760年-763年）
      - [夏　深](../Page/夏深.md "wikilink") [大曆年間](../Page/大曆.md "wikilink")
      - [許子端](../Page/許子端.md "wikilink") 大曆年間
      - [元　持](../Page/元持.md "wikilink") 大曆年間
      - [楊　諫](../Page/楊諫.md "wikilink") 大曆年間
      - [張建封](../Page/張建封.md "wikilink")
        [建中二年](../Page/建中.md "wikilink")-建中四年（781年-783年）
      - [韋　繫](../Page/韋繫.md "wikilink") [貞元年間](../Page/貞元.md "wikilink")
      - [李　萼](../Page/李萼.md "wikilink") 貞元年間
      - [路　怤](../Page/路怤.md "wikilink") 貞元年間
      - [李　俊](../Page/李俊.md "wikilink") 貞元年間
      - [程　異](../Page/程異.md "wikilink") 貞元二十一年（805年）
      - [竇　庠](../Page/竇庠.md "wikilink")
        [永貞元年](../Page/永貞.md "wikilink")（805年）
      - [李　位](../Page/李位.md "wikilink") [元和年間](../Page/元和.md "wikilink")
      - [李象古](../Page/李象古.md "wikilink") 元和年間
      - [盧士瑛](../Page/盧士瑛.md "wikilink") 元和年間
      - [王　堪](../Page/王堪.md "wikilink") 元和十五年（820年）
      - [張　愉](../Page/張愉.md "wikilink")
        [長慶元年](../Page/長慶.md "wikilink")（821年）
      - [韓　常](../Page/韓常.md "wikilink") [寶歷年間](../Page/寶歷.md "wikilink")
      - [徐希仁](../Page/徐希仁.md "wikilink")
        [大和二年](../Page/大和.md "wikilink")（828年）
      - [杜師仁](../Page/杜師仁.md "wikilink") 大和三年（829年）
      - [馮　芫](../Page/馮芫.md "wikilink") 大和五年（831年）
      - [李　虞](../Page/李虞.md "wikilink")
        [開成元年](../Page/開成.md "wikilink")（836年）
      - [李　遠](../Page/李遠.md "wikilink") [大中年間](../Page/大中.md "wikilink")
      - [李　肱](../Page/李肱.md "wikilink") 大中年間
      - 濮陽公 [咸通四年](../Page/咸通.md "wikilink")（863年）
      - 嚴　某 咸通年間
      - [于　琮](../Page/于琮.md "wikilink") 咸通十四年（873年）
      - [薛　位](../Page/薛位.md "wikilink")
        [中和三年](../Page/中和_\(唐朝\).md "wikilink")（883年）
      - [韓師德](../Page/韓師德.md "wikilink") 中和四年（884年）
      - [杜　洪](../Page/杜洪.md "wikilink") 中和四年-光啟二年（884年-886年）
      - [鄧進思](../Page/鄧進思.md "wikilink")
        [光啟二年](../Page/光啟.md "wikilink")-天復二年（886年-902年）
      - [鄧進忠](../Page/鄧進忠.md "wikilink")
        [天復二年](../Page/天復.md "wikilink")-天復三年（902年-903年）
      - [許德勛](../Page/許德勛.md "wikilink") 天復三年-天祐三年（903年-906年）
      - [陳知新](../Page/陳知新.md "wikilink")
        [天祐三年](../Page/天祐.md "wikilink")-天祐四年（906年-907年）
      - [陳處政](../Page/陳處政.md "wikilink")
      - [元利濟](../Page/元利濟.md "wikilink")
      - 弘農公

### 五代

  - 岳州刺史
      - [許德勛](../Page/許德勛.md "wikilink")
        [梁太祖](../Page/梁太祖.md "wikilink")[開平年間](../Page/開平.md "wikilink")
      - [苑　玫](../Page/苑玫.md "wikilink")
        梁太祖[乾化年間](../Page/乾化.md "wikilink")
      - [李廷規](../Page/李廷規.md "wikilink")
        [唐明宗](../Page/唐明宗.md "wikilink")[天成年間](../Page/天成.md "wikilink")
      - [王　贇](../Page/王贇.md "wikilink")
        [漢高祖](../Page/后漢高祖.md "wikilink")[乾祐年間](../Page/乾祐.md "wikilink")
      - [宋德權](../Page/宋德權.md "wikilink")
        [周太祖年間](../Page/后周太祖.md "wikilink")

### 宋代

  - 岳州防禦使
      - [馬仁瑀](../Page/馬仁瑀.md "wikilink")
        [乾德元年](../Page/乾德.md "wikilink")（963年）
  - 知岳州
      - [閻　象](../Page/閻象.md "wikilink")
        [太平興國元年](../Page/太平興國.md "wikilink")-太平興國三年（976年-978年）
      - [董　儼](../Page/董儼.md "wikilink")
        [至道二年](../Page/至道.md "wikilink")-咸平二年（996年-999年）
  - 判岳州
      - [李繼捧](../Page/李繼捧.md "wikilink")
        [咸平二年](../Page/咸平.md "wikilink")-咸平四年（999年-1001年）
  - 知岳州
      - [姚　鉉](../Page/姚鉉.md "wikilink")
        [大中祥符五年](../Page/大中祥符.md "wikilink")-大中祥符六年（1012年-1013年）
      - [宋至和](../Page/宋至和.md "wikilink") 大中祥符六年-大中祥符九年（1013年-1016年）
      - [歐陽潁](../Page/歐陽潁.md "wikilink") 大中祥符九年-天禧二年（1016年-1018年）
      - [朱顯之](../Page/朱顯之.md "wikilink")
        [天禧二年](../Page/天禧.md "wikilink")-天禧五年（1018年-1021年）
      - [陳孝威](../Page/陳孝威.md "wikilink") 天禧五年-天聖元年（1021年-1023年）
      - [張　仿](../Page/張仿.md "wikilink")
        [天聖元年](../Page/天聖.md "wikilink")-天聖三年（1023年-1025年）
      - [張　稹](../Page/張稹.md "wikilink") 天聖三年-天聖六年（1025年-1028年）
      - [韓　瀆](../Page/韓瀆.md "wikilink")
        [明道二年](../Page/明道.md "wikilink")-景祐三年（1033年-1036年）
      - [句希仲](../Page/句希仲.md "wikilink")
        [寶元元年](../Page/寶元.md "wikilink")-康定元年（1038年-1040年）
      - [張中行](../Page/張中行.md "wikilink")
        [康定元年](../Page/康定.md "wikilink")-慶曆二年（1040年-1042年）
      - [楊　畋](../Page/楊畋.md "wikilink")
        [慶曆二年](../Page/慶曆.md "wikilink")-慶曆四年（1042年-1044年）
      - [滕宗諒](../Page/滕宗諒.md "wikilink") 慶曆四年-慶曆六年（1044年-1046年）
      - [張　肅](../Page/張肅.md "wikilink") 慶曆七年-皇祐二年（1047年-1050年）
      - [張　式](../Page/張式.md "wikilink")
        [皇祐二年](../Page/皇祐.md "wikilink")（1050年）
      - [孫　琳](../Page/孫琳.md "wikilink") 皇祐三年-皇祐五年（1051年-1053年）
      - [朱顯之](../Page/朱顯之.md "wikilink")
        [至和元年](../Page/至和.md "wikilink")-嘉祐元年（1054年-1056年）
      - [朱壽昌](../Page/朱壽昌.md "wikilink")
        [嘉祐元年](../Page/嘉祐.md "wikilink")-嘉祐四年（1056年-1059年）
      - [趙尚之](../Page/趙尚之.md "wikilink")
        [治平二年](../Page/治平.md "wikilink")-熙寧元年（1065年-1068年）
      - [鄭民瞻](../Page/鄭民瞻.md "wikilink")
        [元豐元年](../Page/元豐.md "wikilink")-元豐三年（1078年-1080年）
      - [黃　沐](../Page/黃沐.md "wikilink") 元豐三年-元豐四年（1080年-1081年）
      - [李　觀](../Page/李觀.md "wikilink") 元豐四年-元豐七年（1081年-1084年）
      - [鄭　憺](../Page/鄭憺.md "wikilink") 元豐七年-元祐元年（1084年-1086年）
      - [吳安肅](../Page/吳安肅.md "wikilink")
        [元祐元年](../Page/元祐.md "wikilink")-元祐三年（1086年-1088年）
      - [王　綸](../Page/王綸.md "wikilink") 元祐五年-元祐七年（1090年-1092年）
      - [王　震](../Page/王震.md "wikilink")
        [紹聖二年](../Page/紹聖.md "wikilink")-紹聖四年（1095年-1097年）
      - [楊器之](../Page/楊器之.md "wikilink")
        [元符三年](../Page/元符.md "wikilink")-崇寧元年（1100年-1102年）
      - [曾　肇](../Page/曾肇.md "wikilink")
        [崇寧二年](../Page/崇寧.md "wikilink")（1103年）
      - [孫　勰](../Page/孫勰.md "wikilink")
        [政和三年](../Page/政和.md "wikilink")-政和六年（1113年-1116年）
      - [傅惟肖](../Page/傅惟肖.md "wikilink") 政和七年-重和元年（1117年-1118年）
      - [王俊義](../Page/王俊義.md "wikilink")
        [宣和三年](../Page/宣和.md "wikilink")-宣和四年（1121年-1122年）
      - [張汝明](../Page/張汝明.md "wikilink") 宣和四年-宣和五年（1122年-1123年）
      - [蕭　淵](../Page/蕭淵.md "wikilink") 宣和五年-宣和六年（1123年-1124年）
      - [邢　倞](../Page/邢倞.md "wikilink") 宣和七年-靖康元年（1125年-1126年）
      - [余應求](../Page/余應求.md "wikilink")
        [建炎二年](../Page/建炎.md "wikilink")-建炎三年（1128年-1129年）
      - [袁　植](../Page/袁植.md "wikilink") 建炎三年-建炎四年（1129年-1130年）
      - [吳　錫](../Page/吳錫.md "wikilink") 建炎四年-紹興元年（1130年-1131年）
      - [趙士諒](../Page/趙士諒.md "wikilink")
        [紹興元年](../Page/紹興.md "wikilink")（1131年）
      - [王　聲](../Page/王聲.md "wikilink") 紹興元年（1131年）
      - [范寅敷](../Page/范寅敷.md "wikilink") 紹興元年-紹興三年（1131年-1133年）
      - [劉　愿](../Page/劉愿.md "wikilink") 紹興三年-紹興四年（1133年-1134年）
      - [程千秋](../Page/程千秋.md "wikilink") 紹興四年（1134年）
      - [張　觷](../Page/張觷.md "wikilink") 紹興四年-紹興五年（1134年-1135年）
      - [程千秋](../Page/程千秋.md "wikilink") 紹興五年-紹興七年（1135年-1137年）
      - [趙尚之](../Page/趙尚之.md "wikilink") 紹興七年-紹興十年（1137年-1140年）
      - [鄧純赤](../Page/鄧純赤.md "wikilink") 紹興十年-紹興十二年（1140年-1142年）
      - [范　淙](../Page/范淙.md "wikilink") 紹興十四年-紹興十六年（1144年-1146年）
      - [李遇龍](../Page/李遇龍.md "wikilink") 紹興十七年-紹興二十年（1146年-1150年）
      - [呂思宣](../Page/呂思宣.md "wikilink") 紹興二十三年-紹興二十五年（1153年-1155年）
      - [王　趯](../Page/王趯.md "wikilink") 紹興二十六年-紹興二十九年（1156年-1159年）
      - [蘭師稷](../Page/蘭師稷.md "wikilink") 紹興二十九年-紹興三十一年（1159年-1161年）
      - [方　擴](../Page/方擴.md "wikilink") 紹興三十一年-隆興二年（1161年-1164年）
      - [錢　建](../Page/錢建.md "wikilink")
        [隆興二年](../Page/隆興.md "wikilink")-乾道三年（1164年-1167年）
      - [劉穀𥪡](../Page/劉穀𥪡.md "wikilink")
        [乾道三年](../Page/乾道.md "wikilink")-乾道六年（1167年-1170年）
      - [王伯時](../Page/王伯時.md "wikilink") 乾道六年-乾道七年（1170年-1171年）
      - [王　習](../Page/王習.md "wikilink") 乾道七年-淳熙元年（1171年-1174年）
      - [莫若沖](../Page/莫若沖.md "wikilink")
        [淳熙元年](../Page/淳熙.md "wikilink")-淳熙四年（1174年-1177年）
      - [李光邦](../Page/李光邦.md "wikilink") 淳熙五年-淳熙七年（1178年-1180年）
      - [趙善特](../Page/趙善特.md "wikilink") 淳熙七年-淳熙九年（1180年-1182年）
      - [張　溥](../Page/張溥.md "wikilink") 淳熙九年-淳熙十二年（1182年-1185年）
      - [董袞臣](../Page/董袞臣.md "wikilink") 淳熙十二年-淳熙十五年（1185年-1188年）
      - [劉　俁](../Page/劉俁.md "wikilink")
        [紹熙元年](../Page/紹熙.md "wikilink")-紹熙四年（1190年-1193年）
      - [沈　合](../Page/沈合.md "wikilink") 紹熙五年-慶元二年（1194年-1196年）
      - [葉　挺](../Page/葉挺.md "wikilink")
        [慶元三年](../Page/慶元.md "wikilink")-慶元六年（1197年-1200年）
      - [馬子嚴](../Page/馬子嚴.md "wikilink") 慶元六年-嘉泰二年（1200年-1202年）
      - [趙善稼](../Page/趙善稼.md "wikilink")
        [嘉泰二年](../Page/嘉泰.md "wikilink")-嘉泰四年（1202年-1204年）
      - [黃　何](../Page/黃何.md "wikilink") 嘉泰四年-開禧元年（1204年-1205年）
      - [胡朝潁](../Page/胡朝潁.md "wikilink") 開禧元年-開禧三年（1205年-1207年）
      - [曹　格](../Page/曹格.md "wikilink")
        [開禧三年](../Page/開禧.md "wikilink")-嘉定二年（1207年-1209年）
      - [張惠卿](../Page/張惠卿.md "wikilink")
        [嘉定二年](../Page/嘉定.md "wikilink")（1209年）
      - [曹　格](../Page/曹格.md "wikilink") 嘉定二年（1209年）
      - [陳　邕](../Page/陳邕.md "wikilink") 嘉定二年-嘉定三年（1209年-1210年）
      - [黃　渙](../Page/黃渙.md "wikilink") 嘉定三年-嘉定四年（1210年-1211年）
      - [曾　寧](../Page/曾寧.md "wikilink") 嘉定四年-嘉定六年（1211年-1213年）
      - [孟　植](../Page/孟植.md "wikilink") 嘉定六年-嘉定八年（1213年-1215年）
      - [王其賢](../Page/王其賢.md "wikilink") 嘉定九年-嘉定十年（1216年-1217年）
      - [張聲道](../Page/張聲道.md "wikilink") 嘉定十一年-嘉定十三年（1218年-1220年）
      - [虞旒孫](../Page/虞旒孫.md "wikilink") 嘉定十四年-嘉定十六年（1221年-1223年）
      - [孟之經](../Page/孟之經.md "wikilink") 嘉定十六年-寶慶元年（1223年-1225年）
      - [章　塤](../Page/章塤.md "wikilink")
        [寶慶元年](../Page/寶慶.md "wikilink")-寶慶三年（1225年-1227年）
      - [方世京](../Page/方世京.md "wikilink") 寶慶三年-紹定元年（1227年-1228年）
      - [蔡　開](../Page/蔡開.md "wikilink")
        [紹定元年](../Page/紹定.md "wikilink")-紹定二年（1228年-1229年）
      - [陳士表](../Page/陳士表.md "wikilink") 紹定二年-紹定四年（1229年-1231年）
      - [留　碩](../Page/留碩.md "wikilink") 紹定四年-端平元年（1231年-1234年）
      - [趙善瀚](../Page/趙善瀚.md "wikilink")
        [端平元年](../Page/端平.md "wikilink")-嘉熙元年（1234年-1237年）
      - [李曾伯](../Page/李曾伯.md "wikilink")
        [嘉熙元年](../Page/嘉熙.md "wikilink")-嘉熙二年（1237年-1238年）
      - [孟　珙](../Page/孟珙.md "wikilink") 嘉熙二年-嘉熙三年（1238年-1239年）
      - [汪有開](../Page/汪有開.md "wikilink")
        [淳祐四年](../Page/淳祐.md "wikilink")-淳祐六年（1244年-1246年）
      - [謝　焱](../Page/謝焱.md "wikilink") 淳祐十一年（1251年）
      - [趙汝巋](../Page/趙汝巋.md "wikilink") 淳祐十一年-寶祐二年（1251年-1254年）
      - [孟　英](../Page/孟英.md "wikilink")
        [景定元年](../Page/景定.md "wikilink")-景定四年（1260年-1263年）
      - [張　璹](../Page/張璹.md "wikilink") 景定四年-咸淳元年（1263年-1265年）
      - [李應春](../Page/李應春.md "wikilink")
        [咸淳元年](../Page/咸淳.md "wikilink")-咸淳四年（1265年-1268年）
      - [張天驥](../Page/張天驥.md "wikilink") 咸淳五年-咸淳七年（1268年-1271年）
      - [高世傑](../Page/高世傑.md "wikilink")
        咸淳十年-[德祐元年](../Page/德祐.md "wikilink")（1274年-1275年）

## 参考文献

  - 《隆庆岳州府志》
  - 《唐刺史考》
  - 《宋两湖大郡守臣易替考》

[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:五代十国的州](../Category/五代十国的州.md "wikilink")
[Category:宋朝的州](../Category/宋朝的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:湖南的州](../Category/湖南的州.md "wikilink")
[Category:岳阳已撤销的行政区划](../Category/岳阳已撤销的行政区划.md "wikilink")
[Category:589年建立的行政区划](../Category/589年建立的行政区划.md "wikilink")
[Category:1276年废除的行政区划](../Category/1276年废除的行政区划.md "wikilink")

1.  《宋史·本紀·第三十一·高宗八》：「二十五年六月癸卯，以言者追谮岳飞，改岳州为纯州，岳阳军为华容军。」