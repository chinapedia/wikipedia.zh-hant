**中華穿山甲**（[學名](../Page/學名.md "wikilink")：）又稱鯪鲤，是八種[穿山甲中的一個種](../Page/穿山甲.md "wikilink")，属[穿山甲科](../Page/穿山甲科.md "wikilink")[穿山甲属](../Page/穿山甲属.md "wikilink")，主要分佈於[印度东北部](../Page/印度.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[東南亞北部地區以至](../Page/東南亞.md "wikilink")[台灣](../Page/台灣.md "wikilink")、[華南及](../Page/華南.md "wikilink")[海南島等在內](../Page/海南島.md "wikilink")，為一種瀕臨絕種的[動物](../Page/動物.md "wikilink")。

中華穿山甲就像[食蟻獸般靠吃](../Page/食蟻獸.md "wikilink")[蟻為生](../Page/蟻.md "wikilink")。牠們適合生活於濕度高的[森林內](../Page/森林.md "wikilink")。牠們頭身共長60厘米，尾巴長18厘米。

分佈於台灣的穿山甲是中華穿山甲的亞種[台灣穿山甲](../Page/台灣穿山甲.md "wikilink")（台灣鯪鯉）。

## 藥用價值

在傳統[中藥的角度中](../Page/中藥.md "wikilink")，穿山甲的鱗片有藥用之效，其肉亦有進補的作用，因此穿山甲常被視作野味而被獵殺。但其藥效與[犀角一樣只屬於傳統](../Page/犀角.md "wikilink")[迷信](../Page/迷信.md "wikilink")，其主要成份與人類指甲成份相同，同為[角質](../Page/角質.md "wikilink")。\[1\]
而且在非法捕獵的過程中為方便運送等理由，會為穿山甲注射各類型的[鎮定劑](../Page/鎮定劑.md "wikilink")、[興奮劑及](../Page/興奮劑.md "wikilink")[重金屬等](../Page/重金屬.md "wikilink")，令進食人士的肝腎功能受損。\[2\]

## 威脅及保育

穿山甲在[非洲及](../Page/非洲.md "wikilink")[亞洲均被廣泛獵殺](../Page/亞洲.md "wikilink")，以作為食物及傳統[藥物使用](../Page/藥物.md "wikilink")。多個物種在其原生棲地均大幅減少，包括[大穿山甲](../Page/大穿山甲.md "wikilink")、中華穿山甲及[馬來穿山甲等](../Page/馬來穿山甲.md "wikilink")。中華穿山甲自1990年起列入[瀕危野生動植物種國際貿易公約附錄二中](../Page/瀕危野生動植物種國際貿易公約.md "wikilink")，表示任何從野外捕獵的商業用途均被禁止；更於2016年改列為CITES附錄一的物種（受滅絕威脅）。在多個不同國家及地區均禁止出口及貿易，包括[孟加拉](../Page/孟加拉.md "wikilink")、[中國](../Page/中國.md "wikilink")、[印度](../Page/印度.md "wikilink")、[老撾](../Page/老撾.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[泰國及](../Page/泰國.md "wikilink")[越南等](../Page/越南.md "wikilink")。在中國屬於[國家二級保護動物](../Page/中國國家二級保護動物.md "wikilink")，禁止捕殺和食用，非法捕殺、走私或販賣，可被判監5年以上有期徒刑，案情嚴重最高可判處無期徒刑。\[3\]

## 參考文献

[pentadactyla](../Category/穿山甲屬.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國哺乳動物](../Category/中國哺乳動物.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[中华穿山甲](../Category/中华穿山甲.md "wikilink")

1.
2.
3.