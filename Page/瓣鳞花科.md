**瓣鳞花科**共有4[属](../Page/属.md "wikilink")，约90[种](../Page/种.md "wikilink")，分布于[热带和](../Page/热带.md "wikilink")[温带地区的沿海或盐碱地区](../Page/温带.md "wikilink")。[中国有](../Page/中国.md "wikilink")1属—[瓣鳞花属](../Page/瓣鳞花属.md "wikilink")（*Frankenia*），1种—[瓣鳞花](../Page/瓣鳞花.md "wikilink")（*Frankenia
pulverulenta* Linn.
），分布于[新疆](../Page/新疆.md "wikilink")、[甘肃和](../Page/甘肃.md "wikilink")[内蒙西部](../Page/内蒙.md "wikilink")。本[科植物皆为](../Page/科.md "wikilink")[草本或小](../Page/草本.md "wikilink")[灌木](../Page/灌木.md "wikilink")；[茎如铁丝](../Page/茎.md "wikilink")，通常铺散；叶小，[叶对生](../Page/叶.md "wikilink")，无托叶，坚硬，常有腺点；小[花辐射对称](../Page/花.md "wikilink")，两性，，单生或排成聚伞花序，花萼常有腺毛。

本科[植物和](../Page/植物.md "wikilink")[柽柳科的亲缘很近](../Page/柽柳科.md "wikilink")，生长的条件也相似。1981年的[克朗奎斯特分类法将这两科列入](../Page/克朗奎斯特分类法.md "wikilink")[堇菜目](../Page/堇菜目.md "wikilink")，1998年根据[基因亲缘分类的](../Page/基因.md "wikilink")[APG
分类法将这两科都列入](../Page/APG_分类法.md "wikilink")[石竹目](../Page/石竹目.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[瓣鳞花科](http://delta-intkey.com/angio/www/frankeni.htm)
  - [植物网](http://www.efloras.org/browse.aspx?flora_id=1&name_str=Frankeniaceae&btnSearch=Search&chkAllFloras=on)''
  - 在澳大利亚国家植物园中的[瓣鳞花科图片](http://www.anbg.gov.au/images/photo_cd/frankeniaceae/)
  - [NCBI分类法中的瓣鳞花科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=63076&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/瓣鳞花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")