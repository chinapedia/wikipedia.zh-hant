**伍献文**（）\[1\]\[2\]，字显闻，[浙江](../Page/浙江.md "wikilink")[温州](../Page/温州.md "wikilink")[瑞安人](../Page/瑞安.md "wikilink")。著名动物学家，是中国[鱼类学和线虫学的主要奠基人之一](../Page/鱼类.md "wikilink")。

## 生平

1921年毕业于[南京高等师范学校](../Page/南京高等师范学校.md "wikilink")（後更名[國立東南大學](../Page/國立東南大學.md "wikilink")\[3\]、[國立中央大學](../Page/國立中央大學.md "wikilink")、[南京大學](../Page/南京大學.md "wikilink")）农业专修科（今[南京农业大学](../Page/南京农业大学.md "wikilink")），在校时师从[秉志等人](../Page/秉志.md "wikilink")。毕业后任教[福建](../Page/福建.md "wikilink")[厦门](../Page/厦门.md "wikilink")[集美学校](../Page/集美学校.md "wikilink")。1922年厦门大学正式成立，担任[厦门大学动物学系助教](../Page/厦门大学.md "wikilink")，跟随S.F.Light指导学生动物学实验。1925年秉志来到厦门大学，伍献文遂注册为动物学系学生，一面给秉志当助教，一面学习，1927年获[厦门大学理学士学位](../Page/厦门大学.md "wikilink")。同年到[國立中央大學生物学系任动物学助教](../Page/國立中央大學.md "wikilink")。1929年到[法国留学](../Page/法国.md "wikilink")，1932年获[巴黎大学科学博士学位](../Page/巴黎大学.md "wikilink")。1948年当选为[中央研究院院士](../Page/中央研究院.md "wikilink")，1955年选为[中国科学院院士](../Page/中国科学院.md "wikilink")。

1966年，[文革爆發](../Page/文化大革命.md "wikilink")，伍獻文遭到批判、幽禁和迫害，多次被強迫勞動，所有的研究心血差點毀於文革期間。\[4\]

1983年当选为[英国林奈学会外籍会员](../Page/英国.md "wikilink")。历任中央研究院自然博物馆[动物学部主任](../Page/动物.md "wikilink")、[中国科学院水生生物研究所所长](../Page/中国科学院水生生物研究所.md "wikilink")、[中国科学院武汉分院院长](../Page/中国科学院武汉分院.md "wikilink")。

## 著作

其专著《鲤亚目鱼类分科的系统及其科间系统发育的相互关系》论点被引用于权威性著作《[世界鱼类](../Page/世界鱼类.md "wikilink")》第二版。

## 参考来源

[Category:中央研究院生命科學組院士](../Category/中央研究院生命科學組院士.md "wikilink")
[Category:國立中央大學教授](../Category/國立中央大學教授.md "wikilink")
[Category:國立中央大学校友](../Category/國立中央大学校友.md "wikilink")
[Category:中央大学校友](../Category/中央大学校友.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:南京农业大学校友](../Category/南京农业大学校友.md "wikilink")
[Category:温州人](../Category/温州人.md "wikilink")
[Category:中国动物学家](../Category/中国动物学家.md "wikilink")
[Category:中华人民共和国鱼类学家](../Category/中华人民共和国鱼类学家.md "wikilink")
[Category:厦门大学校友](../Category/厦门大学校友.md "wikilink")
[Category:九三学社社员](../Category/九三学社社员.md "wikilink")
[X](../Category/伍姓.md "wikilink")
[Category:文革被迫害学者](../Category/文革被迫害学者.md "wikilink")
[Category:中国科学院武汉分院院长](../Category/中国科学院武汉分院院长.md "wikilink")

1.  [伍献文](http://www.93.gov.cn/html/93gov/syfc/lyys/zgkxyys/yq/130111159967485759.html)九三学社中央宣传部
    2016-7-20
2.  [水生所师生深切缅怀伍献文院士](http://www.ihb.cas.cn/xwzx/wtyd/201504/t20150407_4333088.html)中国科学院水生生物研究所
    2015-4-7
3.  [中国科学院水生生物研究所先后共有十位中国科学院院士](http://www.ihb.cas.cn/gkjj/zjys/)中国科学院水生生物研究所
    2013年
4.