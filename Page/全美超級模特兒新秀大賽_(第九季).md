**《全美超級模特兒新秀大賽》第九季**，是[美國真人秀](../Page/美國.md "wikilink")《[全美超級模特兒新秀大賽](../Page/全美超級模特兒新秀大賽.md "wikilink")》的第九季。在2007年9月19日於The
CW首播。在2007年10月28日於[明珠台播出](../Page/明珠台.md "wikilink")。本季冠軍由來自洛杉機的[絲麗莎](../Page/絲麗莎.md "wikilink")(Saleisha
Stowers)奪得。獲勝者將由Elite模特公司作為經紀，還會獲得與化妝品巨頭Covergirl的一份十萬美元的合同。最後，獲勝者還將出現在Seventeen雜誌的封面上，並且還有6頁的份額。全美超模新秀大賽是由10家企業與Bankable公司聯合主辦的，Tyra
Banks為創辦者，並與Ken Mok(「Making the Band」）及Daniel Soiseth（地獄廚房）共同作為執行製作人。

## 參賽者

### 按淘汰次序排名

  - 按淘汰序
  - 香港譯名為明珠台的翻譯，台灣譯名為Channel\[V\]的翻譯(翻譯已公佈)

| 參賽者                                                    | 香港譯名 | 台灣譯名 | 參賽歲數 | 工作   | 來自         |
| ------------------------------------------------------ | ---- | ---- | ---- | ---- | ---------- |
| Lyudmila "Mila" Bouzinova                              | 美蕾   | 蜜拉   | 20歲  | 學生   | 麻州波士頓      |
| Kimberly Leemans                                       | 金芭麗  | 金柏莉  | 20歲  | 學生   | 佛羅里達州奧卡拉   |
| Victoria Marshman                                      | 維多莉亞 | 維多利亞 | 20歲  | 學生   | 康乃狄格州紐哈芬市  |
| Janet Mills                                            | 珍奈   | 珍妮特  | 22歲  | 美容師  | 喬治亞州班布里基   |
| [Ebony Morgan](../Page/艾班妮·摩根.md "wikilink")           | 艾班妮  | 艾寶妮  | 20歲  | 護校學生 | 伊利諾州芝加哥    |
| Sarah Hartshorne                                       | 莎萊   | 莎拉   | 20歲  | 學生   | 麻州希斯       |
| Ambreal Williams                                       | 安芭奧  | 安碧歐  | 19歲  | 學生   | 德州達拉斯      |
| [Lisa Jackson](../Page/莉莎·杰森.md "wikilink")            | 莉莎   | 麗莎   | 20歲  | 舞蹈家  | 新澤西州澤西市    |
| [Heather Kuzmich](../Page/海瑟·庫茲米契.md "wikilink")       | 海瑟   | 海瑟   | 21歲  | 學生   | 印第安納州威爾帕芮索 |
| Bianca Golden                                          | 碧安卡  | 碧昂卡  | 18歲  | 學生   | 紐約皇后區      |
| [Jenah Doucette](../Page/珍納·戴辛.md "wikilink")          | 珍納   | 貞娜   | 18歲  | 學生   | 康乃迪克州法明頓   |
| [Heather "Chantal" Jones](../Page/珊圖·鍾斯.md "wikilink") | 珊圖   | 香緹   | 19歲  | 學生   | 德州奧斯丁      |
| [Saleisha Stowers](../Page/絲麗莎·史都娃斯.md "wikilink")     | 絲麗莎  | 薩麗莎  | 21歲  | 接待員  | 加州洛杉磯      |

## 拍攝主題

  - 賽前：沙灘照
  - 第一週：吸煙後遺症
  - 第二週：時尚攀岩照
  - 第三週：花的姿態
  - 第四週：時裝怪獸
  - 第五週：資源回收
  - 第七週：Enrique Iglesias MV 拍攝
  - 第八週：沙漠火燒車事件
  - 第九週：CoverGirl廣告
  - 第十週：舞龍舞獅照
  - 第十一週：長城侍衛照
  - 第十二週：Cover Girl

## 詳細資料

### 入圍次序

| 名次 | 集數       |
| -- | -------- |
| 1  | 2        |
| 1  | Mila     |
| 2  | Bianca   |
| 3  | Jenah    |
| 4  | Chantal  |
| 5  | Ambreal  |
| 6  | Victoria |
| 7  | Sarah    |
| 8  | Saleisha |
| 9  | Kimberly |
| 10 | Ebony    |
| 11 | Janet    |
| 12 | Heather  |
| 13 | Lisa     |
|    |          |

  -

    該參賽者退出比賽

    該參賽者為本周封面女郎，並勝出小挑戰

    該參賽者為本周封面女郎

    該參賽者勝出了比賽

    該參賽者勝出小挑戰

    該參賽者為本周封面女郎，但被淘汰

    該參賽者勝出小挑戰，但同時於本周被淘汰，卻又被挽留

    該參賽者被淘汰

### 大變身

  - 安芭奧：剪超短髮
  - 碧安卡：光頭（原為染啡金駁髮）
  - 珊圖：駁髮及剪留海
  - 艾班妮：[娜奧美·金寶式長髮](../Page/娜奧美·金寶.md "wikilink")
  - 海瑟：剪層次及局部染栗色
  - 珍奈：染黑
  - 珍納：駁髮及染金
  - 莉莎：剪短
  - 絲麗莎：Louis Brook髮型
  - 莎萊：剪短及染金
  - 維多莉亞：染亞麻金色

### 比賽數據

  - 參賽者總數：13
  - 年紀最大的參賽者：珍奈（22歲）
  - 年紀最小的參賽者：碧安卡及珍納（都是18歲）
  - 最高的參賽者：莉莎（6呎1.25吋／186厘米）
  - 最矮的參賽者：珍奈（5呎8吋／173厘米）
  - 最重的參賽者：莎萊（150磅／68公斤）
  - 連續贏得最多小挑戰的參賽者：海瑟和絲麗莎（都是2次）
  - 贏得最多小挑戰的參賽者：海瑟和絲麗莎（都是3次）
  - 贏得最多個人小挑戰的參賽者：絲麗莎（3次）
  - 頭三名名次的平均數：（不包括半準決賽和最後一次評審））：絲麗莎 4.0 珊圖 3.91 珍納 3.27
  - 頭三名名次的中位數：（不包括半準決賽和最後一次評審）： 絲麗莎 3 珊圖 4 珍納 3
  - 連續被首先叫名最多的參賽者：珍納及珊圖（2次）
  - 被首先叫名最多的參賽者：珊圖（3次）
  - 連續出現在包尾最多的參賽者：珍納（3次）
  - 出現在包尾最多的參賽者：安芭奧和珍納（3次）
  - 贏得最多「本周封面女郎」的參賽者：海瑟（9次）

## 注釋

<div class="references-small">

<references />

</div>

## 站外連結

  - [Fans of Reality TV
    劇透區](http://www.fansofrealitytv.com/forums/americas-next-top-model/64098-cycle-9-spoiler-thread-spoilers-only.html)
  - [第九季偷拍影片](http://www.youtube.com/watch?v=PqwONpBiTIg)
  - [CW官方網站](https://web.archive.org/web/20080203202341/http://cwtv.com/shows/americas-next-top-model09/)

[Category:全美超級模特兒新秀大賽](../Category/全美超級模特兒新秀大賽.md "wikilink")