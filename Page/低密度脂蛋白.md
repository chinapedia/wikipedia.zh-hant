**低密度脂蛋白**（，[缩写为](../Page/缩写.md "wikilink")****）指一類及範圍的[脂蛋白粒子](../Page/脂蛋白.md "wikilink")，有著約18-25納米直徑的大小，負責在[血液內運載](../Page/血液.md "wikilink")[脂肪酸分子至全身供](../Page/脂肪酸.md "wikilink")[細胞使用](../Page/細胞.md "wikilink")。它是由[肝臟所產生的](../Page/肝臟.md "wikilink")[極低密度脂蛋白的最終階段](../Page/極低密度脂蛋白.md "wikilink")。它只包含有[載體蛋白B](../Page/載體蛋白.md "wikilink")-100（即一種有4536種[氨基酸的](../Page/氨基酸.md "wikilink")[蛋白質](../Page/蛋白質.md "wikilink")）\[1\]。它亦包含[維他命](../Page/維他命.md "wikilink")[抗氧化劑](../Page/抗氧化劑.md "wikilink")（[維生素E或](../Page/維生素E.md "wikilink")[類胡蘿蔔素](../Page/類胡蘿蔔素.md "wikilink")）。低密度脂蛋白携带的[胆固醇標準與](../Page/胆固醇.md "wikilink")[心血管疾病的发生存在紧密的](../Page/心血管疾病.md "wikilink")[正相关](../Page/正相关.md "wikilink")，因此低密度脂蛋白胆固醇一度被普遍稱為「壞膽固醇」。但值得留意的是，低密度脂蛋白並非膽固醇。

## 功能

一般來說，低密度脂蛋白由[肝臟及](../Page/肝臟.md "wikilink")[小腸運送](../Page/小腸.md "wikilink")[膽固醇及](../Page/膽固醇.md "wikilink")[三酸甘油脂至有需要的](../Page/三酸甘油脂.md "wikilink")[細胞及](../Page/細胞.md "wikilink")[組織](../Page/组织_\(生物学\).md "wikilink")。因為[脂肪](../Page/脂肪.md "wikilink")、膽固醇等脂質為[疏水性物質](../Page/疏水性.md "wikilink")，不會和血液結合，故此需要由小蛋白質覆蓋其表面，形成[親水性脂蛋白](../Page/親水性.md "wikilink")，方能在血液中流動\[2\]。

## 與疾病的關係

由於低密度脂蛋白將[膽固醇運送至](../Page/膽固醇.md "wikilink")[動脈](../Page/動脈.md "wikilink")，過量的低密度脂蛋白會造成[動脈硬化](../Page/動脈硬化.md "wikilink")、[心肌梗塞](../Page/心肌梗塞.md "wikilink")、[中風及](../Page/中風.md "wikilink")[周圍動脈疾病](../Page/周圍動脈疾病.md "wikilink")。所以低密度脂蛋白內的膽固醇會被稱為「**壞膽固醇**」。當然，並非它本身必然是壞的膽固醇，而是要看它被送往甚麼地方，與長年累積的數量。

證據顯示低密度脂蛋白的濃度及大小，相比在它之內的膽固醇濃度，才是最與動脈硬化有所關連。最健康的是有少量的大型低密度脂蛋白及沒有細小粒子，但卻是非常罕見。高濃度的細小低密度脂蛋白是最常見而又不健康的情況。雖然高濃度的細小低密度脂蛋白所攜帶的膽固醇與低濃度的大型低密度脂蛋白相當，但它與[粥樣斑塊的快速增生](../Page/粥樣斑塊.md "wikilink")、動脈硬化及過早出現嚴重的[心血管疾病](../Page/心血管疾病.md "wikilink")，甚至於[死亡相關](../Page/死亡.md "wikilink")。

低密度脂蛋白是由[極低密度脂蛋白](../Page/極低密度脂蛋白.md "wikilink")，透過[脂蛋白分解酶拆去](../Page/脂蛋白分解酶.md "wikilink")[甘油三脂而形成](../Page/甘油三脂.md "wikilink")，從而變得細少及較高[密度包含較多的膽固醇](../Page/密度.md "wikilink")。

[家族性高膽固醇血症是一種遺傳的高低密度脂蛋白症狀](../Page/家族性高膽固醇血症.md "wikilink")。而遞增的低密度脂蛋白稱為[高脂蛋白血症二型](../Page/高脂蛋白血症二型.md "wikilink")。

當低密度脂蛋白侵襲[內皮細胞](../Page/內皮細胞.md "wikilink")，並令之[氧化](../Page/氧化.md "wikilink")，造成內皮細胞障礙，巨噬細胞從而進入血管壁。巨噬細胞然後會把膽固醇包圍起來形成泡沫細胞化，它們是含有低密度脂蛋白的泡狀細胞。當泡沫細胞崩壤時其會釋放膽固醇，並成為動脈壁內動脈粥樣化瘤，再造成血管平滑肌增殖、纖維性硬化與鈣化，血管壁因而增厚\[3\]，最終會提高[心血管疾病的風險](../Page/心血管疾病.md "wikilink")。

一個複雜的[生物化學反應可以調節低密度脂蛋白的氧化](../Page/生物化學.md "wikilink")，並主要是由內皮細胞內的[自由基來引發](../Page/自由基.md "wikilink")。[一氧化氮在](../Page/一氧化氮.md "wikilink")[精氨酸的催化下可以減低這個氧化過程](../Page/精氨酸.md "wikilink")。而當有高標準的[非對稱性二甲基精氨酸](../Page/非對稱性二甲基精氨酸.md "wikilink")，一氧化氮的製造會被阻礙，並且低密度脂蛋白氧化會出現。

## 建議攝取標準

[美國心臟學會](../Page/美國心臟學會.md "wikilink")、[美國國立衞生研究院圖像及](../Page/美國國立衞生研究院圖像.md "wikilink")[美國國家膽固醇教育計劃提供了一系列的建議來降低低密度脂蛋白](../Page/美國國家膽固醇教育計劃.md "wikilink")[膽固醇水平](../Page/膽固醇.md "wikilink")，也提供了濃度與[心臟病風險之關係](../Page/心臟病.md "wikilink")。於2003年，建議是：\[4\]

| colspan = 2 | 標準 | rowspan = 2 | 解釋 |
| ---------------- | ---------------- |
| (mg/dL)          | (mmol/L)         |
| \<100            | \<2.6            |
| 100 - 129        | 2.6 - 3.3        |
| 130 - 159        | 3.3 - 4.1        |
| 160 - 189        | 4.1 - 4.9        |
| \>190            | \>4.9            |

這套指引是假設[心血管疾病的目標死亡率每年下降](../Page/心血管疾病.md "wikilink")2-3%，或是每十年下降20-30%所釐定。\[5\]另外，最佳的水平並非100mg/dL，而是低於100mg/dL，但多少卻沒有指明。

隨著更多的臨床研究，這些建議的水平亦不斷下調。\[6\]這是由於低密度脂蛋白的下降，包括不正常的低水平，在大型[雙盲的隨機臨床測試中能最有效減低心血管疾病死亡率](../Page/雙盲.md "wikilink")，遠比起冠心血管成形術或搭橋手術為佳。

舉例來說，對於已患有[動脈硬化的病人](../Page/動脈硬化.md "wikilink")，就2004年建議的健康水平為低於70mg/dL。有估計指低密度脂蛋白的水平要下降至50mg/dL可以將心血管疾病出現率下降至接近零。\[7\]而另有一項量度在低密度脂蛋白內[脂肪](../Page/脂肪.md "wikilink")／[膽固醇濃度的縱向人口分析](../Page/膽固醇.md "wikilink")，於患有動脈硬化的病人從兒童至成人間的相關活動中，發現在形成[脂肪斑紋前正常的低密度脂蛋白水平為](../Page/脂肪斑紋.md "wikilink")35mg/dL。

## 量度方法

現時臨床量度[脂肪濃度的方法是化學措施](../Page/脂肪.md "wikilink")，雖然這種方法不是與個別結果有最好的關聯，但它卻是便宜及容易使用。可是，有證據顯示一個更準確的量度是有必要的。低密度脂蛋白粒子數與[動脈硬化及](../Page/動脈硬化.md "wikilink")[心血管疾病有更緊密的關聯](../Page/心血管疾病.md "wikilink")。\[8\]低密度脂蛋白[膽固醇濃度可以是低的](../Page/膽固醇.md "wikilink")，但它的粒子數量卻可以很高，造成高心血管發病率。相反，低密度脂蛋白膽固醇可以相同地高，而它的粒子數量低時，心血管發病率亦會降低。如果追蹤低密度脂蛋白粒子濃度與發病率時，其他在[統計學上與心血管疾病有關聯的因素](../Page/統計學.md "wikilink")，如[糖尿病](../Page/糖尿病.md "wikilink")、[肥胖症及](../Page/肥胖症.md "wikilink")[吸煙](../Page/吸煙.md "wikilink")，都會失去了它們的附加預測性。

## 低密度脂蛋白子類模式

低密度脂蛋白粒子實際在大小及密度上都有分別，研究指出較多細小而高[密度的低密度脂蛋白](../Page/密度.md "wikilink")（稱為「B模式」），相對大型而低密度的「A模式」會有較高風險造成[冠心病](../Page/冠心病.md "wikilink")。這是因為細小的粒子能更容易穿透[內皮細胞](../Page/內皮細胞.md "wikilink")。「I模式」即中級，是指大部份低密度脂蛋白粒子非常接近正常內皮細胞的空隙（約26納米）。

B模式（較低密度脂蛋白粒子）與冠心病的關係更密切。用作量度這些粒子類的測試費用昂貴及不廣泛使用，所以一般的[脂肪測試仍然普遍](../Page/脂肪.md "wikilink")。脂肪測試並不直接量度低密度脂蛋白水平，但可以透過Freidewald公式及其他膽固醇（如[高密度脂蛋白](../Page/高密度脂蛋白.md "wikilink")）水平來估計：

\[\mbox{LDL-C} \approx \mbox{TC} - \mbox{HDL-C} - \frac{\mbox{TG}}{5}\]

其中：

  - \(\mbox{LDL-C}  \,\!\)是低密度脂蛋白膽固醇；
  - \(\mbox{TC(Total Cholesterol)} \,\!\)是總膽固醇；
  - \(\mbox{HDL-C} \,\!\)是高密度脂蛋白膽固醇；及
  - \(\mbox{TG(Triglycerides)} \,\!\)是總[三酸甘油脂](../Page/三酸甘油脂.md "wikilink")。

這個公式可以提供對大部份人較準確的估計，假設是[血液是在禁食](../Page/血液.md "wikilink")14小時後被抽取。

較高三酸甘油脂水平亦與高水平的細小而密的低密度脂蛋白有所關聯，相反低的三酸甘油脂水平則與高水平的大型而疏的低密度脂蛋白有關連。\[9\]

就成本的下降、更大的採用率、廣泛的接受及使用其他[脂蛋白子類分析方法](../Page/脂蛋白.md "wikilink")，尤其是[核磁共振頻譜學](../Page/核磁共振頻譜學.md "wikilink")，證明了[人類](../Page/人類.md "wikilink")[心血管疾病與粒子數量濃度有最強的關聯](../Page/心血管疾病.md "wikilink")，比起粒子大小及膽固醇／脂肪粒子成份更強。

## 減少低密度脂蛋白的方法

[HMG-CoA还原酶抑制劑是一類能抑制](../Page/HMG-CoA还原酶.md "wikilink")、阻斷[肝臟內生成](../Page/肝臟.md "wikilink")[膽固醇的藥物](../Page/膽固醇.md "wikilink")，從而降低血液中膽固醇和低密度脂蛋白的含量。該類化學物於1980年代開發，1990年代曾於[歐洲與](../Page/歐洲.md "wikilink")[美國等地進行大規模的臨床試驗](../Page/美國.md "wikilink")，結果顯示使用HMG-CoA还原酶抑制劑能降低體內膽固醇，進而減少[動脈硬化發生率](../Page/動脈硬化.md "wikilink")\[10\]。HMG-CoA还原酶抑制劑代表藥物為[辛伐他汀](../Page/辛伐他汀.md "wikilink")。

## 参考文献

## 外部連結

  - [美國國家膽固醇教育計劃第三版成人治療流程報告](https://web.archive.org/web/20050524010904/http://www.nhlbi.nih.gov/guidelines/cholesterol/atp3_rpt.htm)
  - [美國國家膽固醇教育計劃2004年更新第三版成人治療流程報告](https://web.archive.org/web/20140202053506/http://www.nhlbi.nih.gov/guidelines/cholesterol/atp3upd04.htm)

## 参见

  - [高密度脂蛋白](../Page/高密度脂蛋白.md "wikilink")、[極低密度脂蛋白](../Page/極低密度脂蛋白.md "wikilink")
  - [膽固醇](../Page/膽固醇.md "wikilink")
  - [三酸甘油脂](../Page/三酸甘油脂.md "wikilink")
  - [低密度脂蛋白受體](../Page/低密度脂蛋白受體.md "wikilink")
  - [脂蛋白X](../Page/脂蛋白X.md "wikilink")
  - [褪黑激素](../Page/褪黑激素.md "wikilink")
  - [飽和脂肪](../Page/飽和脂肪.md "wikilink")

{{-}}

[Category:脂蛋白](../Category/脂蛋白.md "wikilink")
[Category:血脂紊亂](../Category/血脂紊亂.md "wikilink")

1.

2.
3.

4.

5.
6.

7.

8.

9.

10.