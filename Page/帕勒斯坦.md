**帕勒斯坦**（**Palestine**）位于[美国](../Page/美国.md "wikilink")[得克萨斯州东部](../Page/得克萨斯州.md "wikilink")，是[安德森县的县治所在](../Page/安德森县_\(得克萨斯州\).md "wikilink")，面积46.3平方公里。根据[美国2000年人口普查](../Page/美国2000年人口普查.md "wikilink")，共有17,598人，其中[白人占](../Page/白人.md "wikilink")64.6%、[非裔美国人占](../Page/非裔美国人.md "wikilink")24.77%。

## 参考资料

## 外部链接

  - [City of Palestine](http://www.cityofpalestinetx.org/)
  - [Columbia Scientific Balloon Facility](http://www.csbf.nasa.gov/)
  - [Historic Photos of Palestine
    Texas](http://texashistory.unt.edu/search/?q=%22United+States+-+Texas+-+Anderson+County+-+Palestine%22&t=dc.coverage)
    hosted by the [Portal to Texas
    History](http://texashistory.unt.edu/)
  - [Palestine Independent School
    District](http://www.palestineschools.org/)
  - [Westwood Independent School District](http://www.westwoodisd.net/)
  - [University of Texas at Tyler - Palestine
    Campus](http://www.uttyler.edu/palestine/)
  - [Trinity Valley Community College - Palestine
    Campus](https://web.archive.org/web/20080117204522/http://www.tvcc.edu/campus/palestine/default.aspx)

[P](../Category/得克萨斯州城市.md "wikilink")