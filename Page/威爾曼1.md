{{ Galaxy | | image = | name = 威爾曼1 | epoch =
[J2000](../Page/J2000.md "wikilink") | type = 極端矮橢球星系或
異常球狀星團\[1\] | ra = \[2\] | dec = \[3\] | dist_ly =120,000 ±
20,000[光年](../Page/光年.md "wikilink") (38,000 ±
7,000[秒差距](../Page/秒差距.md "wikilink"))\[4\] | z = |
appmag_v = 15.4 ± 0.4 | size_v = | constellation name =
[大熊座](../Page/大熊座.md "wikilink") | notes = | names = SDSS
J1049+5103\[5\] }}

**威爾曼1**或**SDSS
J1049+5103**\[6\]是[紐約大學的](../Page/紐約大學.md "wikilink")[貝絲·威爾曼領導的一個小組](../Page/貝絲·威爾曼.md "wikilink")，使用[史隆數位巡天資料發現的一個異常的](../Page/史隆數位巡天.md "wikilink")[球狀星團或超低質量的](../Page/球狀星團.md "wikilink")[矮星系](../Page/矮星系.md "wikilink")
\[7\]。在2006年，它是繼[牧夫座矮橢球星系和](../Page/牧夫座矮星系.md "wikilink")[大熊座矮橢球星系之後第三暗的星系](../Page/大熊座矮星系.md "wikilink")，而比後繼的還按200倍。這個星系是[銀河系的衛星星系](../Page/銀河系.md "wikilink")，距離大約12,000[光年遠](../Page/光年.md "wikilink")，絕對亮度為-2.5等\[8\]。亮度函數由中心向外的變化，顯示[質量分離的情況與](../Page/質量分離.md "wikilink")[帕羅馬
5中所發現的類似](../Page/帕羅馬_5.md "wikilink")\[9\]。

## 註解

<div class="references-small">

1.  15.4 ± 0.4 apparent magnitude - 5 \* (log<sub>10</sub>(38 ± 7\[10\]
    kpc distance) - 1) = -2.5\[11\] absolute magnitude

<references/>

</div>

## 參考資料

<div class="references-small">

  -
  -
  -

</div>

[Category:矮橢球星系](../Category/矮橢球星系.md "wikilink")
[Category:球狀星團](../Category/球狀星團.md "wikilink")
[Category:銀河系次集團](../Category/銀河系次集團.md "wikilink")

1.

2.
3.
4.

5.
6.
7.

8.
9.
10.
11.