**高宇蓁**，台灣一線女星，戲劇一姊，[明新科技大學畢業](../Page/明新科技大學.md "wikilink")，曾是[新竹科學園區工程師](../Page/新竹科學園區.md "wikilink")。
2002年出道，參與多部[民視膾炙人口之戲劇作品](../Page/民視.md "wikilink")，2008年演出民視八點檔《[娘家](../Page/娘家_\(電視劇\).md "wikilink")》劇中的「[賴惠君](../Page/娘家_\(電視劇\)#賴家.md "wikilink")」（三八強）一炮而紅，而後演出民視八點檔《[夜市人生](../Page/夜市人生_\(2009年電視劇\).md "wikilink")》劇中的「[楊愛眉](../Page/夜市人生_\(2009年電視劇\)#楊家.md "wikilink")」也廣受好評，奠定一線女星位置。2012年加入伊林娛樂，擔綱多部戲劇要角，接下三立都會台首部華劇《[我們發財了](../Page/我們發財了.md "wikilink")》女主角，殺青後，順勢接下三立台灣台《[天下女人心](../Page/天下女人心.md "wikilink")》女主角，近期作品為三立電視《[金家好媳婦](../Page/金家好媳婦.md "wikilink")》一劇女主角。2015年開創「大創紅」娛樂公司擔任執行長。2018年更名為大創紅國際有限公司，開創自有品牌J.KAO，擁有「台劇女神」封號。

## 個人经历

  - 2002年正式出道，參與多部[民視無線台戲劇作品](../Page/民視無線台.md "wikilink")。
  - 2008年演出[民視無線台八點檔](../Page/民視無線台.md "wikilink")「娘家」賴惠君（三八強）一角一舉成名，且主演[民視無線台八點檔](../Page/民視無線台.md "wikilink")「夜市人生」－楊愛眉。
  - 2012年轉投[三立電視台](../Page/三立電視台.md "wikilink")，於[三立都會台演出華劇](../Page/三立都會台.md "wikilink")《[我們發財了](../Page/我們發財了.md "wikilink")》蘇錦華一角，並接下[三立台灣台八點檔](../Page/三立台灣台.md "wikilink")《[天下女人心](../Page/天下女人心.md "wikilink")》女主角演出，隔年加入[伊林娛樂且拍攝多部雜誌](../Page/伊林娛樂.md "wikilink")，參與各類時尚活動，且演出[三立台灣台八點檔](../Page/三立台灣台.md "wikilink")《[世間情](../Page/世間情.md "wikilink")》－沈曼娟。
  - 2015年拍攝台灣動畫電影《[OPEN！
    OPEN！](../Page/OPEN！_OPEN！.md "wikilink")》飾演阿嬌姐，並成立大創紅娛樂公司，2016年於工作空檔之餘演出[三立台灣台八點檔](../Page/三立台灣台.md "wikilink")[甘味人生](../Page/甘味人生.md "wikilink")－林海茵。
  - 2017年，拍攝大陸劇《[大宋北斗司](../Page/大宋北斗司.md "wikilink")》，演飾皇后。
  - 2017年，拍攝大陸劇《[萌妻食神](../Page/萌妻食神.md "wikilink")》，飾演榮夫人。
  - 2017年12月5日在三立台灣臺定裝，準備接演八點檔大戲《[金家好媳婦](../Page/金家好媳婦.md "wikilink")》。21日開鏡。
  - 2018年1月9日《[金家好媳婦](../Page/金家好媳婦.md "wikilink")》開播。
  - 2018年發表記者會，宣布發展面膜品牌《J.KAO》副業\[1\]。

## 作品

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>年 份</strong></p></td>
<td><p><strong>頻 道</strong></p></td>
<td><p><strong>劇 名</strong></p></td>
<td><p><strong>角 色</strong></p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/民視.md" title="wikilink">民視</a></p></td>
<td><p><a href="../Page/不了情.md" title="wikilink">不了情</a></p></td>
<td><p>貴子</p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/日正當中_(電視劇).md" title="wikilink">日正當中</a></p></td>
<td><p>管曉薇</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/女人40一枝花.md" title="wikilink">女人40一枝花</a></p></td>
<td><p>廖曉蘋</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/慾望人生.md" title="wikilink">慾望人生</a></p></td>
<td><p>凱莉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/意難忘.md" title="wikilink">意難忘</a></p></td>
<td><p>方麗君</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/再生緣_(2006年台灣電視劇).md" title="wikilink">再生緣</a></p></td>
<td><p>劉桂香</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夫妻天註定.md" title="wikilink">夫妻天註定</a></p></td>
<td><p>賴秀蕙</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p><a href="../Page/公共電視文化事業基金會#人生劇展.md" title="wikilink">公視人生劇展</a>──<a href="../Page/我的阿嬤是鬼.md" title="wikilink">我的阿嬤是鬼</a></p></td>
<td><p>林淑滿</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/民視.md" title="wikilink">民視</a></p></td>
<td><p><a href="../Page/愛_(民視電視劇).md" title="wikilink">愛</a></p></td>
<td><p>小真</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/濟公_(2007年電視劇).md" title="wikilink">濟公</a></p></td>
<td><p>劉玉蓮<br />
楊天嬌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/娘家_(民視電視劇).md" title="wikilink">娘家</a></p></td>
<td><p>賴惠君</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/夜市人生_(2009年電視劇).md" title="wikilink">夜市人生</a></p></td>
<td><p>楊爱眉<br />
薇多麗</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/三立都會台.md" title="wikilink">三立都會台</a></p></td>
<td><p><a href="../Page/我們發財了.md" title="wikilink">我們發財了</a></p></td>
<td><p>蘇錦華</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p><a href="../Page/天下女人心.md" title="wikilink">天下女人心</a></p></td>
<td><p>孫寶兒（第一代）<br />
杜小小（靈魂交換）<br />
陳小小（靈魂交換）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a></p></td>
<td><p><a href="../Page/愛情急整室.md" title="wikilink">愛情急整室</a></p></td>
<td><p>白珊娜</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台視.md" title="wikilink">台視</a></p></td>
<td><p><a href="../Page/親愛的，我愛上別人了.md" title="wikilink">親愛的，我愛上別人了</a></p></td>
<td><p>紀雲芳</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p><a href="../Page/世間情.md" title="wikilink">世間情</a></p></td>
<td><p>孫曼娟<br />
沈曼娟</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大愛.md" title="wikilink">大愛</a></p></td>
<td><p><a href="../Page/情牽萬里.md" title="wikilink">情牽萬里</a></p></td>
<td><p>翁珍珍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p><a href="../Page/甘味人生.md" title="wikilink">甘味人生</a></p></td>
<td><p>林海茵<br />
</p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/腾讯视频.md" title="wikilink">腾讯视频</a></p></td>
<td><p><a href="../Page/萌妻食神.md" title="wikilink">萌妻食神</a></p></td>
<td><p><a href="../Page/榮夫人.md" title="wikilink">榮夫人</a></p></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p><a href="../Page/金家好媳婦.md" title="wikilink">金家好媳婦</a></p></td>
<td><p>金欣蓉</p></td>
</tr>
<tr class="odd">
<td><p>2019年</p></td>
<td><p><a href="../Page/騰訊視頻.md" title="wikilink">騰訊視頻</a>、<a href="../Page/愛奇藝.md" title="wikilink">愛奇藝</a></p></td>
<td><p><a href="../Page/大宋北斗司.md" title="wikilink">大宋北斗司</a></p></td>
<td><p>皇后 刘茹</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 主持

  - 民視《[我的第一次](../Page/我的第一次.md "wikilink")》
  - 民視《[馬祖勞軍](../Page/馬祖勞軍.md "wikilink")》

### 廣告

  - Huawei‬ ‪G7 Plus‬ 女神機

### 代言

  - 台糖詩丹雅蘭All In One頂級修護霜
  - 台糖詩丹雅蘭3D無瑕美肌粉凝霜
  - 奇士美全系列保養品
  - Huawei G7 Plus手機
  - 全新系列行動麥克風2019代言人
  - J.KAO亞太區代言人
  - 山本富也·維納斯系列-咖啡奶茶2019代言人
  - 明洞洗髮精2019代言人
  - 粉刺戰鬥機2019代言人

### 節目通告

  - [中天娛樂台](../Page/中天娛樂台.md "wikilink")－[天天樂財神](../Page/天天樂財神.md "wikilink")

## 參考來源

## 外部連結

  -
  -
  -
  -
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Y](../Category/高姓.md "wikilink")
[Category:明新科技大學校友](../Category/明新科技大學校友.md "wikilink")

1.  [高宇蓁保養快狠準！聖誕自曝美麗祕方](https://www.msn.com/zh-tw/entertainment/news/%e9%ab%98%e5%ae%87%e8%93%81%e4%bf%9d%e9%a4%8a%e5%bf%ab%e7%8b%a0%e6%ba%96%ef%bc%81%e8%81%96%e8%aa%95%e8%87%aa%e6%9b%9d%e7%be%8e%e9%ba%97%e7%a5%95%e6%96%b9/ar-BBQTkaN)，《[三立新聞網](../Page/三立新聞網.md "wikilink")》，2018-12-13。