*德干暗色岩*（Deccan
Traps）又譯**德干玄武岩**，是一個[大火成岩省](../Page/大火成岩省.md "wikilink")，位於[印度南部的](../Page/印度.md "wikilink")[德干高原](../Page/德干高原.md "wikilink")，是地表上最大型的[火山地形之一](../Page/火山.md "wikilink")。德干玄武岩由多層[洪流玄武岩所構成](../Page/洪流玄武岩.md "wikilink")，厚度超過2,000公尺，面積為50萬平方里。

## 歷史

德干暗色岩是在6,800萬年前到6,000萬年前所形成的\[1\]\[2\]，時間是[白堊紀末期](../Page/白堊紀.md "wikilink")。在6,600萬年前，[西高止山脈發生了大量的火山爆發](../Page/西高止山脈.md "wikilink")。這一連串的火山爆發持續了近三萬年之久。火山爆發所噴出的氣體可能是[白堊紀-第三紀滅絕事件的原因之一](../Page/白堊紀-第三紀滅絕事件.md "wikilink")，該次滅絕事件滅絕了包含[恐龍在內的大部分生物](../Page/恐龍.md "wikilink")。

德干暗色岩火山爆發所形成的[熔岩地形](../Page/熔岩.md "wikilink")，被估計最大面積為150萬平方公里，相當於現在印度的一半面積，而後經由[侵蝕作用與](../Page/侵蝕作用.md "wikilink")[大陸漂移](../Page/大陸漂移.md "wikilink")，形成現在的大小。目前所能直接觀測的熔岩面積約為51.2萬平方公里

這次火山爆發所噴出的氣體，似乎與[全球暖化有關](../Page/全球暖化.md "wikilink")。有證據指出在[希克蘇魯伯撞擊事件之前的](../Page/希克蘇魯伯隕石坑.md "wikilink")50萬年內，大氣溫度曾上升8°C\[3\]。

## 化學成分

德干暗色岩的岩石有至少95%屬於[拉斑玄武岩](../Page/拉斑玄武岩.md "wikilink")，其他包含：[鹼性玄武岩](../Page/鹼性玄武岩.md "wikilink")、[霞石岩](../Page/霞石岩.md "wikilink")、[煌斑岩](../Page/煌斑岩.md "wikilink")、[碳酸岩](../Page/碳酸岩.md "wikilink")。印度西部的[古吉拉特邦](../Page/古吉拉特邦.md "wikilink")[喀奇縣其他地方](../Page/喀奇縣.md "wikilink")，則發現了[地函](../Page/地函.md "wikilink")[捕虜岩](../Page/捕虜岩.md "wikilink")。

## 形成理論

德干暗色岩的成因被認為與地函中的[熱柱有關](../Page/熱柱.md "wikilink")。現今位於[留尼旺群島下的](../Page/留尼旺群島.md "wikilink")[留尼旺熱點](../Page/留尼旺熱點.md "wikilink")，造成了一連串的火山爆發，造成了德干暗色岩，並使[塞席爾高原與印度分離開來](../Page/塞席爾.md "wikilink")。隨後，[印度板塊與](../Page/印度板塊.md "wikilink")[非洲板塊之間的](../Page/非洲板塊.md "wikilink")[海底擴張](../Page/海底擴張.md "wikilink")，將印度推離留尼旺熱柱北方。但是，目前有科學家對這個熱柱理論提出異議\[4\]。

### 濕婆隕石坑

印度西岸外海的海底，有個可能是大型[撞擊事件所留下的隕石坑](../Page/撞擊事件.md "wikilink")，名為[濕婆隕石坑](../Page/濕婆隕石坑.md "wikilink")，被認為發生於6,500萬年前，與[K-T界線時間相符](../Page/K-T界線.md "wikilink")。有研究人員認為這次撞擊事件可能間階造成德干暗色岩火山爆發，並使印度板塊與非洲板塊在[第三紀早期加速遠離](../Page/第三紀.md "wikilink")\[5\]。但是，有地理學家提出異議，認為這個大型海底地形，並不是撞擊事件所造成的\[6\]。

## 對生物的影響

德干暗色岩火山爆發所噴出的氣體，對當時的全球氣候造成激烈的變化，也影響了當時的生物。[倫敦](../Page/倫敦.md "wikilink")[自然歷史博物館的Norman](../Page/自然歷史博物館.md "wikilink")
Macleod博士指出，因為這時期的恐龍[化石紀錄難以判斷生物群的變化](../Page/化石紀錄.md "wikilink")，只能用其他型態的生物群來判斷其變化。在K-T界線的600萬年前，海洋中約有20種[菊石](../Page/菊石.md "wikilink")，K-T界線的三百萬年後，海洋中只有15種菊石，但是在K-T界線的100萬年前，海洋中只有10種菊石，所以滅絕事件可能已持續了數百萬年之久。[魚類](../Page/魚類.md "wikilink")、陸棲[爬行動物](../Page/爬行動物.md "wikilink")、[哺乳類的化石紀錄也顯示出相同的模式](../Page/哺乳類.md "wikilink")。恐龍可能也與這些生物群一樣，受到滅絕事件影響達數百萬年之久\[7\]。許多科學家認為這個長時間的滅絕事件，與火山爆發造成的全球性氣候暖化有關，這次全球性氣候事件也對恐龍造成影響，但是希克蘇魯伯的撞擊事件所造成的灰塵，使白堊紀-第三紀滅絕事件推向高峰\[8\]\[9\]。

## 參考資料

[Category:火山学](../Category/火山学.md "wikilink")
[Category:地质灾害](../Category/地质灾害.md "wikilink")
[Category:超级火山](../Category/超级火山.md "wikilink")
[Category:大火成岩省](../Category/大火成岩省.md "wikilink")
[Category:印度地理](../Category/印度地理.md "wikilink")
[Category:火山](../Category/火山.md "wikilink")
[Category:白堊紀](../Category/白堊紀.md "wikilink")

1.  [德干玄武岩的成因理論](http://www.mantleplumes.org/Deccan.html)

2.  [Geochronological Study of the Deccan
    Volcanism](http://ksgeo.kj.yamagata-u.ac.jp/~iwata/personal/Deccan-e.htm)


3.

4.
5.  [2003
    濕婆隕石坑與德干暗色岩火山爆發、印度與賽席爾的分離、恐龍滅絕的關連](http://gsa.confex.com/gsa/2003AM/finalprogram/abstract_58126.htm)

6.  [濕婆隕石坑，另一個撞擊事件？](http://www.spacedaily.com/news/deepimpact-04r.html)

7.
8.
9.  [雙重災難：隕石撞擊與火山爆發](http://www.livescience.com/animals/071112-dino-volcanoes.html)