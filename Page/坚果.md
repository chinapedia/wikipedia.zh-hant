[Hazelnuts.jpg](https://zh.wikipedia.org/wiki/File:Hazelnuts.jpg "fig:Hazelnuts.jpg")
**坚果**是植物的一类[果实](../Page/果实.md "wikilink")，通常用来泛指[果皮坚硬的](../Page/果皮.md "wikilink")[干果类](../Page/干果.md "wikilink")，依[植物学的定义](../Page/植物学.md "wikilink")，坚果是指由坚硬的果皮和種子組成的果實，且在果實成熟时果皮不开裂（[閉果](../Page/閉果.md "wikilink")）。但在日常的定義下，只要有堅硬外殼、油性的果仁就會稱為坚果。

大部份果實中的種子可以自然的和殼分開，但像[榛子](../Page/榛子.md "wikilink")、[栗子及](../Page/栗.md "wikilink")[橡子等堅果](../Page/橡子.md "wikilink")，其硬殼包住種子，不易將硬殼和種子分開。日常定義的坚果範圍較寬，像[开心果及](../Page/开心果.md "wikilink")[巴西堅果不是植物學中所定義的堅果](../Page/巴西堅果.md "wikilink")，但在日常生活中視為坚果\[1\]。日常定義的坚果一般是指硬殼、果仁可食用的乾果\[2\]。

## 植物学定义

### 定義

[植物学上的坚果是](../Page/植物学.md "wikilink")[果实的一种](../Page/果实.md "wikilink")。為[乾果](../Page/乾果.md "wikilink")，[果皮坚硬且成熟时不开裂](../Page/果皮.md "wikilink")（[閉果](../Page/閉果.md "wikilink")），果實內通常只有一枚[种子](../Page/种子.md "wikilink")（少数有2枚），且[种皮与](../Page/种子#.E7.A7.8D.E7.9A.AE.md "wikilink")[果皮分離](../Page/果皮.md "wikilink")。

### 常見的例子

常见的堅果多屬於[殼斗目植物](../Page/殼斗目.md "wikilink")：

  - [殼斗科植物](../Page/殼斗科.md "wikilink")：[板栗](../Page/板栗.md "wikilink")
  - [胡桃科植物](../Page/胡桃科.md "wikilink")：[胡桃](../Page/胡桃.md "wikilink")、[碧根果](../Page/碧根果.md "wikilink")
  - [樺木科植物](../Page/樺木科.md "wikilink")：[榛子](../Page/榛子.md "wikilink")

### 生態

對[野生生物而言](../Page/野生动物.md "wikilink")，堅果是個重要的[營養來源](../Page/營養.md "wikilink")，特別是[溫帶地區的動物](../Page/溫帶地區.md "wikilink")，如[松鴉](../Page/松鴉.md "wikilink")、[松鼠會於秋天時](../Page/松鼠.md "wikilink")，收集儲存[橡實](../Page/橡.md "wikilink")（acorn）與其他的堅果，以備秋末至初春免於飢餓。

## 日常食用

### 口語上的堅果

一般作為食用的果實，雖不符植物學上堅果的定義，但緣於英語的習慣用法，常會稱呼任何具堅硬外殼、油性的果仁、作為食品者為堅果。

### 成分及利用

[美國](../Page/美國.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[凱斯西儲大學醫學部的丹妮](../Page/凱斯西儲大學.md "wikilink")‧馬諾爾博士研究發現，堅果富含[維生素E](../Page/維生素E.md "wikilink")，能有效防治[脂肪肝](../Page/脂肪肝.md "wikilink")。若長期、適量食用，對預防心血管疾病也有正面成效\[3\]。另外亦含有[鋅](../Page/鋅.md "wikilink")、[鐵](../Page/鐵.md "wikilink")、[鎂](../Page/鎂.md "wikilink")、[鈣等成分](../Page/鈣.md "wikilink")。需要注意的是，堅果[脂肪](../Page/脂肪.md "wikilink")（[不飽和脂肪酸](../Page/不飽和脂肪酸.md "wikilink")）含量高，每次最好不要食用過量\[4\]否則有可能提高肥胖機率。\[5\]

由於堅果經常內含高[油脂成分](../Page/油脂.md "wikilink")，它們被認為是極佳的[食物及](../Page/食物.md "wikilink")[能量來源](../Page/能量.md "wikilink")。有極多種類的可食種子，被人們利用作為食品，處理方式除了直接食用，還可經由[烹飪](../Page/烹飪.md "wikilink")、發芽、烘培（烤食）後作為[零食](../Page/零食.md "wikilink")；亦可榨油提供[食品](../Page/食品.md "wikilink")、[化妝品使用](../Page/化妝品.md "wikilink")。

坚果是非常健康的，因为它们富含营养和抗氧化剂。它可以预防心脏病和糖尿病，而且可以帮助减肥。\[6\]

### 常見的過敏源

這些被統稱為堅果的食物，亦是常見的[食物過敏來源](../Page/食物過敏.md "wikilink")。\[7\]

### 例子

下列為**日常生活中常被稱為堅果**，但實際上的利用之部位並不符合堅果定義者。

  - **扁桃**：屬於[薔薇科的](../Page/薔薇科.md "wikilink")[扁桃](../Page/扁桃.md "wikilink")，由於其[外果皮為革質](../Page/外果皮.md "wikilink")，而非其他同為[李屬果類的肉質果肉](../Page/李屬.md "wikilink")，在外果皮更內一層為木質化的[內果皮](../Page/內果皮.md "wikilink")，內果皮之內則為供作食用的[種子](../Page/種子.md "wikilink")。
  - **巴西果**：[玉蕊科的](../Page/玉蕊科.md "wikilink")[巴西果果實為](../Page/巴西果.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，被稱為**堅果**的食用部位其實是其種子。
  - **石栗**：[大戟科的](../Page/大戟科.md "wikilink")[石栗種子含油量高](../Page/石栗.md "wikilink")，通常作為[油漆](../Page/油漆.md "wikilink")、[肥皂](../Page/肥皂.md "wikilink")、[蠟燭等工業的原料](../Page/蠟燭.md "wikilink")，但[印尼人會將處理過的堅果拿來煮](../Page/印尼.md "wikilink")[咖哩](../Page/咖哩.md "wikilink")。
  - **腰果**：[漆樹科的](../Page/漆樹科.md "wikilink")[腰果是種子](../Page/腰果.md "wikilink")。
  - **澳大利亞堅果**：[澳大利亞堅果](../Page/澳大利亞堅果.md "wikilink")（或稱**夏威夷果**）原產[澳大利亞的](../Page/澳大利亞.md "wikilink")[山龍科植物](../Page/山龍科.md "wikilink")。
  - **馬拉巴栗**：[錦葵科的](../Page/錦葵科.md "wikilink")[馬拉巴栗種子可煮食](../Page/馬拉巴栗.md "wikilink")。其果實為[蒴果](../Page/蒴果.md "wikilink")。
  - **花生**：[豆科的](../Page/豆科.md "wikilink")[花生](../Page/花生.md "wikilink")，常見帶[莢的是整個](../Page/豆莢.md "wikilink")[莢果](../Page/莢果.md "wikilink")，[花生米則是已去掉豆莢的種子](../Page/花生米.md "wikilink")。
  - **松子**：數種[松類植物的種子](../Page/松科.md "wikilink")。
  - **開心果**：[漆樹科的](../Page/漆樹科.md "wikilink")[阿月渾子樹果實為](../Page/阿月渾子.md "wikilink")[核果](../Page/核果.md "wikilink")，可食用部分是種子。\[8\]

<!-- end list -->

  - 其他：
      - [山龍眼科的](../Page/山龍眼科.md "wikilink")

      - Horse chestnut
        [無患子科](../Page/無患子科.md "wikilink")[七葉樹屬的植物](../Page/七葉樹屬.md "wikilink")，蒴果及種子，不可直接食用。

      - 深焙或烤熟的玉米粒作為零食之用，英文稱為Corn nut。

      - 大戟科。

      - 大戟科。

## 相關條目

  - [核桃](../Page/核桃.md "wikilink")
  - [食用果仁列表](../Page/食用果仁列表.md "wikilink")

## 參考資料

[Category:果实](../Category/果实.md "wikilink")

1.

2.

3.

4.  <http://news.xinhuanet.com/fashion/2013-06/09/c_124834455.htm>

5.
6.

7.  [what nuts are considered "tree
    nuts?](http://www.fda.gov/Food/GuidanceRegulation/GuidanceDocumentsRegulatoryInformation/Allergens/ucm059116.htm)Questions
    and Answers Regarding Food Allergens, including the Food Allergen
    Labeling and Consumer Protection Act of 2004

8.