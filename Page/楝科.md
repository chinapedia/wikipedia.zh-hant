**楝科**（學名：）属于[被子植物門](../Page/被子植物門.md "wikilink")（Angiospermae）[雙子葉植物綱](../Page/雙子葉植物綱.md "wikilink")（Magnoliopsida）[無患子目](../Page/無患子目.md "wikilink")（Sapindales）[植物的一](../Page/植物.md "wikilink")[科](../Page/科.md "wikilink")。

大部分是常绿植物，也有部分是落叶的。《[中國種子植物科屬詞典](../Page/中國種子植物科屬詞典.md "wikilink")》引用[侯宽昭和](../Page/侯宽昭.md "wikilink")[陈德昭於](../Page/陈德昭.md "wikilink")1955年編著的《中国楝科志》指：本科有「约50属，1400种」\[1\]\[2\]；而按Christenhusz-
& Byng (2016)，本科現時包括約53個屬、600個物種\[3\]。

## 分佈

本科物種有一個：基本生长在[热带地区](../Page/热带.md "wikilink")，但最北有可生長到[中国的溫帶氣候地區的](../Page/中国.md "wikilink")[楝属](../Page/楝属.md "wikilink")（*[Melia](../Page/Melia.md "wikilink")*），而最南有可在[澳大利亚东南部的](../Page/澳大利亚.md "wikilink")[温带地区生長的](../Page/温带.md "wikilink")[香椿属](../Page/香椿属.md "wikilink")（*[Toona](../Page/Toona.md "wikilink")*）。[中國國內有](../Page/中國.md "wikilink")18個屬，62種\[4\]。

## 形态

  - 大部分都是[乔木](../Page/乔木.md "wikilink")，少数为[灌木](../Page/灌木.md "wikilink")；小枝常有皮孔。
  - 叶片互生、稀有对生，一般为1～3回羽状复叶；没有托叶。
  - 花朵小至中等大小，辐射对称，通常为圆锥花序；通常5基数；浅杯状或短管状[花萼](../Page/花萼.md "wikilink")，全缘，4～5齿裂或由4～5萼片组成；花瓣一般为4～5枚，发芽时呈镊合状或覆瓦状排列，分离或下部与雄蕊管合生；[雄蕊花丝通常合生成雄蕊管](../Page/雄蕊.md "wikilink")，花药为花瓣的2倍，或与花瓣等数，直立，内向，着生于雄蕊管内面裂齿的顶端或每2裂齿之间；子房小，上位，分离或与花盘合生，一般4～5室，每室有[胚珠](../Page/胚珠.md "wikilink")1～2颗或很多。
  - 果皮一般为革质、木质；种子常有假种皮，有时具有膜质翅。

## 利用

藥用10屬23種\[5\]，有的品种可以提炼植物油，用于制造肥皂或杀虫剂；桃花芯木是名贵的[木材](../Page/木材.md "wikilink")；[香椿的嫩芽可以用做](../Page/香椿.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")。

本科中一些著名的品种：

  - [印度楝](../Page/印度楝.md "wikilink")（*Azadirachta indica*）
  - [西班牙柏木](../Page/西班牙柏木.md "wikilink")（*Cedrela
    odorata*）（南[美洲](../Page/美洲.md "wikilink")）
  - [科特迪瓦桃花心木](../Page/科特迪瓦桃花心木.md "wikilink")（*Khaya
    ivorensis*）（热带[非洲](../Page/非洲.md "wikilink")）
  - [桃花心木](../Page/桃花心木.md "wikilink")（*Khaya
    senegalensis*）（热带[非洲](../Page/非洲.md "wikilink")）
  - [苦楝](../Page/苦楝.md "wikilink")（*Melia
    azedarach*）（[中国](../Page/中国.md "wikilink")，[印度和](../Page/印度.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")）
  - [香椿](../Page/香椿.md "wikilink")（*Toona
    siensis*）（[中国](../Page/中国.md "wikilink")）

## 属

  - [米仔兰属](../Page/米仔兰属.md "wikilink") *Aglaia*
    [Aglaiaodorata1web.jpg](https://zh.wikipedia.org/wiki/File:Aglaiaodorata1web.jpg "fig:Aglaiaodorata1web.jpg")
  - *[Anthocarapa](../Page/Anthocarapa.md "wikilink")*
  - [山楝属](../Page/山楝属.md "wikilink") *Aphanamixis*
  - *Astrotrichilia*
  - [蒜楝属](../Page/蒜楝属.md "wikilink") *Azadirachta*
  - [南美楝属](../Page/南美楝属.md "wikilink") *Cabralea*
  - *Calodecarya*
  - *Capuronianthus*
  - [酸渣树属](../Page/酸渣树属.md "wikilink") *Carapa*
  - [洋椿属](../Page/洋椿属.md "wikilink") *Cedrela*
  - [溪桫属](../Page/溪桫属.md "wikilink") *Chisocheton*
  - [麻楝属](../Page/麻楝属.md "wikilink") *Chukrasia*
  - [浆果楝属](../Page/浆果楝属.md "wikilink") *Cipadessa*
  - [樫木属](../Page/樫木属.md "wikilink") *Dysoxylum*
  - [假槟榔属](../Page/假槟榔属.md "wikilink") *Ekebergia*
  - [非洲楝属](../Page/非洲楝属.md "wikilink") *Entandrophragma*
  - [驼峰楝属](../Page/驼峰楝属.md "wikilink") *Guarea*
  - *Heckeldora*
  - *Humbertioturraea*
  - [非洲桃花芯木属](../Page/非洲桃花芯木属.md "wikilink") *Khaya*
  - [兰橵属](../Page/兰橵属.md "wikilink") *Lansium*
  - *Lepidotrichilia*
  - *Lovoa*
  - *Malleastrum*
  - [楝属](../Page/楝属.md "wikilink") *Melia*
  - [地黄连属](../Page/地黄连属.md "wikilink") *Munronia*
  - [印度吐根属](../Page/印度吐根属.md "wikilink") *Naregamia*
  - *Neobeguea*
  - *[Owenia](../Page/Owenia.md "wikilink")*
  - *Pseudobersama*
  - *Pseudocarapa*
  - *Pseudocedrela*
  - *Pterorhachis*
  - [雷楝属](../Page/雷楝属.md "wikilink") *Reinwardtiodendron*
  - *Ruagea*
  - [山道楝属](../Page/山道楝属.md "wikilink") *Sandoricum*
  - *Schmardaea*
  - *Soymida*
  - *Sphaerosacme*
  - [桃花心木属](../Page/桃花心木属.md "wikilink") *Swietenia*
  - *[Synoum](../Page/Synoum.md "wikilink")*
  - [香椿属](../Page/香椿属.md "wikilink") *Toona*
  - [鹧鸪花属](../Page/鹧鸪花属.md "wikilink") *Trichilia*
  - [杜楝属](../Page/杜楝属.md "wikilink") *Turraea*
  - *Turraeanthus*
  - *Vavaea*
  - [割舌树属](../Page/割舌树属.md "wikilink") *Walsura*
  - [木果楝属](../Page/木果楝属.md "wikilink") *Xylocarpus*

## 参考文献

  -
## 註解

## 外部链接

  - 在[L. Watson和M.J.
    Dallwitz（1992年）《有花植物分科》](http://delta-intkey.com/angio/)中的[楝科](http://delta-intkey.com/angio/www/meliacea.htm)
  - [Project
    楝科](https://archive.is/20070929163235/http://www.mahoganyforthefuture.org/projectmeliaceae/)

[\*](../Category/楝科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.

2.

3.

4.

5.