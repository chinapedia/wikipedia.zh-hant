**建武**（304年七月-十一月）是[西晋皇帝](../Page/西晋.md "wikilink")[晋惠帝司马衷的第八个](../Page/晋惠帝.md "wikilink")[年号](../Page/年号.md "wikilink")。共计5个月。

[永安元年七月改元建武元年](../Page/永安_\(西晋\).md "wikilink")，同年十一月复称永安。

## 纪年

<table>
<thead>
<tr class="header">
<th><p>建武</p></th>
<th><p>元年</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/公元纪年.md" title="wikilink">公元</a></p></td>
<td><p>304年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/干支纪年.md" title="wikilink">干支</a></p></td>
<td><p><a href="../Page/甲子.md" title="wikilink">甲子</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/成汉.md" title="wikilink">成汉</a></p></td>
<td><p><a href="../Page/建初_(李特).md" title="wikilink">建初二年</a><br />
<a href="../Page/建兴_(李雄).md" title="wikilink">建兴元年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/前赵.md" title="wikilink">前赵</a></p></td>
<td><p><a href="../Page/元熙_(刘渊).md" title="wikilink">元熙元年</a></p></td>
</tr>
</tbody>
</table>

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[建武年号](../Page/建武.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:西晋年号](../Category/西晋年号.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:300年代中国政治](../Category/300年代中国政治.md "wikilink")
[Category:304年](../Category/304年.md "wikilink")