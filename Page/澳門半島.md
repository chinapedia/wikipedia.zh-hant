**澳門半島**（）是組成[澳門的四大部份](../Page/澳門.md "wikilink")（區域）之一，是澳門居民的主要聚居地，也是澳門最早開發的地區，有超過四百年的歷史。位於澳門半島的[澳門歷史城區於](../Page/澳門歷史城區.md "wikilink")2005年正式被列入[世界文化遺產名錄內](../Page/世界文化遺產.md "wikilink")。

## 地理

[Macau_oldmap.jpg](https://zh.wikipedia.org/wiki/File:Macau_oldmap.jpg "fig:Macau_oldmap.jpg")
[Mapa_Peninsula_Macau_1889.gif](https://zh.wikipedia.org/wiki/File:Mapa_Peninsula_Macau_1889.gif "fig:Mapa_Peninsula_Macau_1889.gif")
[Macau_Colina_da_Guia.jpg](https://zh.wikipedia.org/wiki/File:Macau_Colina_da_Guia.jpg "fig:Macau_Colina_da_Guia.jpg")，其上面是[東望洋燈塔](../Page/東望洋燈塔.md "wikilink")\]\]
澳門半島在數千年前為一海上小島（澳門島），為[萬山群島的一部份](../Page/萬山群島.md "wikilink")，其後因珠江口西側的泥沙而令珠海和澳門之間形成一[連島沙洲](../Page/連島沙洲.md "wikilink")（現址於今日的[關閘](../Page/關閘.md "wikilink")），澳門島才開始與[中國大陸相連](../Page/中國大陸.md "wikilink")，加上多年的[填海才形成今日的澳門半島](../Page/澳門填海.md "wikilink")。

澳門半島的平地大部分由填海取得。據[統計暨普查局](../Page/統計暨普查局.md "wikilink")（2011年）資料，澳門半島的面積約為9.3平方公里，人口約52.2萬人\[1\]。

澳門半島的北面關閘地區和[廣東省的](../Page/廣東省.md "wikilink")[珠海市](../Page/珠海市.md "wikilink")[拱北連接](../Page/拱北.md "wikilink")，西面與同屬珠海市的[灣仔對望](../Page/湾仔街道.md "wikilink")，南面與[氹仔隔海相望](../Page/氹仔.md "wikilink")，東北面則與[香港的](../Page/香港.md "wikilink")[大嶼山隔](../Page/大嶼山.md "wikilink")[珠江入海口相望](../Page/珠江.md "wikilink")。

### 山峰

澳門半島的山峰皆為小山，平均海拔高度在100米以下，而且分佈零散。澳門半島的最高點為[東望洋山](../Page/東望洋山.md "wikilink")（松山）北峰，高度為海拔93[米](../Page/米_\(计量单位\).md "wikilink")。

| 名稱                                      | 高度（米） | 行政區劃                                   |
| --------------------------------------- | ----- | -------------------------------------- |
| [東望洋山](../Page/東望洋山.md "wikilink")（松山）  | 93    | [望德堂區](../Page/望德堂區.md "wikilink")     |
| [媽閣山](../Page/媽閣山.md "wikilink")        | 71.6  | [風順堂區](../Page/風順堂區.md "wikilink")     |
| [西望洋山](../Page/西望洋山.md "wikilink")（主教山） | 62.7  | [風順堂區](../Page/風順堂區.md "wikilink")     |
| [望廈山](../Page/望廈山.md "wikilink")（黑鬼山）   | 60.7  | [花地瑪堂區](../Page/花地瑪堂區.md "wikilink")   |
| [大炮台山](../Page/大炮台山.md "wikilink")      | 57.3  | [聖安多尼堂區](../Page/聖安多尼堂區.md "wikilink") |
| [青洲山](../Page/青洲山.md "wikilink")        | 54.5  | [花地瑪堂區](../Page/花地瑪堂區.md "wikilink")   |
| [馬交石山](../Page/馬交石山.md "wikilink")      | 48.1  | [花地瑪堂區](../Page/花地瑪堂區.md "wikilink")   |
|                                         |       |                                        |

## 人口分佈

澳門半島自古以來都是[澳門居民的主要居住地](../Page/澳門居民.md "wikilink")，根據[統計暨普查局於](../Page/統計暨普查局.md "wikilink")2011年進行的[人口普查顯示](../Page/人口普查.md "wikilink")\[2\]，澳門半島的居住人口為469,009人，佔全[澳門總人口中的](../Page/澳門.md "wikilink")84.9%，而有一半的人口都居住在[花地瑪堂區](../Page/花地瑪堂區.md "wikilink")（北區）。澳門半島的人口密度為每平方公里50,431人，高於整體密度（每平方公里18,478人），為世界人口密度最高的地區之一。

## 區域劃分

### 官方行政區劃

[Administrative_Division_of_Macau_SAR.png](https://zh.wikipedia.org/wiki/File:Administrative_Division_of_Macau_SAR.png "fig:Administrative_Division_of_Macau_SAR.png")
澳門半島由5個[堂區組成](../Page/堂區.md "wikilink")，分別是：

  - [花地瑪堂區](../Page/花地瑪堂區.md "wikilink")（[青洲區](../Page/青洲_\(澳門\).md "wikilink")、[台山區](../Page/台山_\(澳門\).md "wikilink")、[黑沙環及](../Page/黑沙環.md "wikilink")[祐漢區](../Page/祐漢.md "wikilink")、[黑沙環新填海區](../Page/黑沙環.md "wikilink")、[望廈及](../Page/望廈.md "wikilink")[水塘區](../Page/水塘.md "wikilink")、[筷子基區](../Page/筷子基.md "wikilink")）
  - [聖安多尼堂區](../Page/聖安多尼堂區.md "wikilink")（林茂塘區、[高士德及雅廉訪區](../Page/高士德大馬路.md "wikilink")、[新橋區](../Page/新橋_\(澳門\).md "wikilink")、[沙梨頭及](../Page/沙梨頭.md "wikilink")[大三巴區](../Page/大三巴.md "wikilink")）
  - [大堂區](../Page/大堂區.md "wikilink")（[新口岸區](../Page/新口岸.md "wikilink")、[外港及南灣湖新填海區](../Page/新口岸新填海區.md "wikilink")、[中區](../Page/中區_\(澳門\).md "wikilink")）
  - [望德堂區](../Page/望德堂區.md "wikilink")（[荷蘭園區](../Page/荷蘭園.md "wikilink")、[東望洋區](../Page/塔石.md "wikilink")）
  - [風順堂區](../Page/風順堂區.md "wikilink")（下環區、[南](../Page/南灣_\(澳門\).md "wikilink")[西灣及](../Page/西灣_\(澳門\).md "wikilink")[主教山區](../Page/主教山.md "wikilink")）

堂區是[天主教澳門教區的一種劃分澳門地區的方法](../Page/天主教澳門教區.md "wikilink")，雖然政府部門廣泛使用，但它不是[澳門的行政分區](../Page/澳門.md "wikilink")，也沒有被多數[澳門市民所接納或使用](../Page/澳門市民.md "wikilink")。

### 澳門市民常用的行政區劃

[macaucolourfulname.png](https://zh.wikipedia.org/wiki/File:macaucolourfulname.png "fig:macaucolourfulname.png")
澳門人日常生活更為傾向以該區的建築物、建築特色、土地用途、人口分佈和實際的地理位置來對澳門作出分區。而[氹仔](../Page/氹仔.md "wikilink")、[路氹和](../Page/路氹.md "wikilink")[路環因甚少人居住而沒有分區](../Page/路環.md "wikilink")。

## 交通

[Ponte_de_Sai_Van_e_Torre_de_Macau.JPG](https://zh.wikipedia.org/wiki/File:Ponte_de_Sai_Van_e_Torre_de_Macau.JPG "fig:Ponte_de_Sai_Van_e_Torre_de_Macau.JPG")\]\]
[MacauOuterHarbourFerryTerminal1.jpg](https://zh.wikipedia.org/wiki/File:MacauOuterHarbourFerryTerminal1.jpg "fig:MacauOuterHarbourFerryTerminal1.jpg")，[澳門主要的海上客運碼頭](../Page/澳門.md "wikilink")\]\]

### 陸上交通

澳門開埠初期，人們主要以步行、馬車、人力車這三種方式出行。到了20世紀初期，開始有街坊車（澳門早期的[巴士](../Page/澳門巴士.md "wikilink")）出現在澳門半島。現至今日，[巴士成為澳門半島以及連接離島](../Page/巴士.md "wikilink")（[氹仔和](../Page/氹仔.md "wikilink")[路環](../Page/路環.md "wikilink")）的主要公共交通工具，也是[澳門居民最常用的公共交通工具](../Page/澳門居民.md "wikilink")。[的士是澳門半島另一種常見公共交通工具](../Page/澳門的士.md "wikilink")，主要服務本地居民和遊客。此外，還有主要服務遊客的[人力車及](../Page/人力車.md "wikilink")[發財車行走於澳門半島](../Page/澳門巴士.md "wikilink")。

[大眾捷運系統方面](../Page/大眾捷運系統.md "wikilink")，[澳門輕軌第一期澳門半島段已經進行規劃中](../Page/澳門輕軌.md "wikilink")，走行於[關閘](../Page/關閘.md "wikilink")、[黑沙環](../Page/黑沙環.md "wikilink")、[外港碼頭](../Page/外港碼頭.md "wikilink")、[新口岸新填海區](../Page/新口岸新填海區.md "wikilink")、[南灣](../Page/南灣_\(澳門\).md "wikilink")、[媽閣然後連接至](../Page/媽閣.md "wikilink")[氹仔](../Page/氹仔.md "wikilink")，還有第二期，計劃行走[筷子基](../Page/筷子基.md "wikilink")、[內港一帶](../Page/內港.md "wikilink")，最後與第一期系統交會於[媽閣站](../Page/媽閣站.md "wikilink")，並且形成一條澳門半島的環線。

### 跨海交通

在[嘉樂庇總督大橋建成通車之前](../Page/嘉樂庇總督大橋.md "wikilink")，往來澳門半島與離島（[氹仔和](../Page/氹仔.md "wikilink")[路環](../Page/路環.md "wikilink")）的途徑是乘坐渡輪，由當時的澳門海島市小輪船有限公司（即後來的[澳巴](../Page/澳巴.md "wikilink")）經營。1974年10月5日，連接澳門半島和[氹仔島的第一條跨海大橋](../Page/氹仔島.md "wikilink")（[嘉樂庇總督大橋](../Page/嘉樂庇總督大橋.md "wikilink")）正式通車，標誌著澳門三島正式以陸路的方式連結起來。同時，輪渡式微，[巴士在此開始成為澳門半島往來離島的主要交通工具](../Page/巴士.md "wikilink")。

現時，澳門半島和[氹仔島之間以三條跨海大橋連接在一起](../Page/氹仔島.md "wikilink")。分別為[嘉樂庇總督大橋](../Page/嘉樂庇總督大橋.md "wikilink")（1974年通車）、[友誼大橋](../Page/澳門友誼大橋.md "wikilink")（1994年通車）和[西灣大橋](../Page/西灣大橋.md "wikilink")（2005年1月通車），三條大橋都會在懸掛[八號風球或以上颱風訊號時會封閉](../Page/八號風球.md "wikilink")。[嘉樂庇總督大橋為公交專道](../Page/嘉樂庇總督大橋.md "wikilink")，僅供[巴士](../Page/巴士.md "wikilink")、[的士及](../Page/的士.md "wikilink")[緊急車輛使用](../Page/救護車.md "wikilink")，值得一提，它是唯一一條供行人行走的跨海大海；[西灣大橋為澳門第一條雙層式大橋](../Page/西灣大橋.md "wikilink")，上層提供4線普通行車線，兩線為[電單車專道](../Page/電單車.md "wikilink")，下層設有[澳門輕軌的軌道及供颱風時三條大橋封閉時供](../Page/澳門輕軌.md "wikilink")[輕型汽車行駛的行車道](../Page/輕型汽車.md "wikilink")。第四條澳氹跨海大橋和[澳門輕軌](../Page/澳門輕軌.md "wikilink")[澳氹線目前正詳細規劃中](../Page/澳氹線.md "wikilink")。

### 跨境交通

澳門半島上有兩個港口，分別為西面的[內港和東面的](../Page/澳門內港.md "wikilink")[外港客運碼頭](../Page/外港客運碼頭.md "wikilink")。外港的[外港碼頭是現時來往](../Page/外港碼頭.md "wikilink")[香港與澳門的主要途徑](../Page/香港.md "wikilink")。此外，[港珠澳大橋設有澳門口岸](../Page/港珠澳大橋.md "wikilink")，位於澳門半島東北人工島上。

### 東望洋跑道

由澳門半島街道組成的[東望洋跑道是世界著名的街場跑道](../Page/東望洋跑道.md "wikilink")，每年均會舉辦[澳門格蘭披治大賽車](../Page/澳門格蘭披治大賽車.md "wikilink")。

## 参考文献

## 外部連結

  - [Google Maps
    的澳門半島衛星圖片](http://maps.google.com/maps?ll=22.197962,113.547306&spn=0.033646,0.055532&t=k&hl=en)

## 參見

  - [青洲 (澳門)](../Page/青洲_\(澳門\).md "wikilink")
  - [氹仔](../Page/氹仔.md "wikilink")
  - [路氹城](../Page/路氹城.md "wikilink")
  - [路環](../Page/路環.md "wikilink")
  - [澳門填海造地](../Page/澳門填海造地.md "wikilink")

{{-}}

[Category:澳門地方](../Category/澳門地方.md "wikilink")
[Category:中国半岛](../Category/中国半岛.md "wikilink")

1.  <http://www.dsec.gov.mo/getAttachment/564633df-27ea-4680-826c-37d1ef120017/C_CEN_PUB_2011_Y.aspx>
2.  <http://www.dsec.gov.mo/getAttachment/564633df-27ea-4680-826c-37d1ef120017/C_CEN_PUB_2011_Y.aspx>