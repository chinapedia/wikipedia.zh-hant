**环己烯**是六个碳的[环烯烃](../Page/环烯烃.md "wikilink")，分子式为C<sub>6</sub>H<sub>10</sub>，室温下为无色液体。

环己烯可通过[环己醇酸催化脱水制得](../Page/环己醇.md "wikilink")。\[1\]

[Cyclohexensynthese1.svg](https://zh.wikipedia.org/wiki/File:Cyclohexensynthese1.svg "fig:Cyclohexensynthese1.svg")

[Cyclohexensynthese2.svg](https://zh.wikipedia.org/wiki/File:Cyclohexensynthese2.svg "fig:Cyclohexensynthese2.svg")

与链状[烯烃类似](../Page/烯烃.md "wikilink")，环己烯能被[高锰酸钾氧化](../Page/高锰酸钾.md "wikilink")，并与[溴水发生](../Page/溴.md "wikilink")[加成反应](../Page/加成反应.md "wikilink")\[2\]

*3*' C<sub>6</sub>H<sub>10</sub>+ **2** KMnO<sub>4</sub>+**4**
H<sub>2</sub>O**--\>3**
C<sub>6</sub>H<sub>10</sub>(OH)<sub>2</sub>+**2** MnO<sub>2</sub>+**2**
KOH

## 参考资料

[Category:环烯烃](../Category/环烯烃.md "wikilink")
[Category:烃类溶剂](../Category/烃类溶剂.md "wikilink")
[Category:六元环](../Category/六元环.md "wikilink")
[環己烯](../Category/環己烯.md "wikilink")

1.  [环己烯的合成](http://www.orgsyn.org/orgsyn/prep.asp?prep=cv1p0183)
2.  [环己烯与溴和高锰酸钾的反应](http://www.uni-regensburg.de/Fakultaeten/nat_Fak_IV/Organische_Chemie/Didaktik/Keusch/D-Addit_ar-e.htm)