**株洲中车时代电氣股份有限公司**（），簡稱**中车时代电氣**，是位于[中国](../Page/中国.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[株洲市的电气设备制造厂商](../Page/株洲市.md "wikilink")，由[中国南车集团](../Page/中国南车集团.md "wikilink")[株洲电力机车](../Page/株洲电力机车.md "wikilink")、[株洲电力机车研究所](../Page/株洲电力机车研究所.md "wikilink")、[戚墅堰机车车辆厂等五方组建而成](../Page/戚墅堰机车车辆厂.md "wikilink")。产品包括供[铁路车辆使用的牽引](../Page/铁路车辆.md "wikilink")[逆变器](../Page/逆变器.md "wikilink")、辅助逆变器、控制系统、大功率半导体元件、传感器等。

前身為「株洲南车时代电氣股份有限公司」。

公司于2006年12月20日起在[香港上市](../Page/香港.md "wikilink")。

南车时代于2008年8月底与[英国](../Page/英国.md "wikilink")[丹尼克斯半导体公司](../Page/丹尼克斯半导体.md "wikilink")（Dynex）签订收购协议，包括以1,672万[加元收购于](../Page/加拿大元.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[多伦多证券交易所发行的](../Page/多伦多证券交易所.md "wikilink")75%普通股，收购已于2008年10月31日完成\[1\]，时代电气得以从英国获得大电压IGBT芯片货源，吸收有关技朮，制造交流电传动列车变流装置，及应付国内其他需求。

2011年5月，位于株洲的首条8英寸[IGBT晶圆生产线正式开建](../Page/絕緣柵雙極晶體管.md "wikilink")，届时其产能可达每年12万片\[2\]。该芯片厂已于2014年6月竣工并正式投产\[3\]\[4\]\[5\]。

近年該公司亦開始生產電動公交車， 長度有8米12米和18米三種選項

## 列车产品

### 牵引变流器

用于[HXD1](../Page/HXD1.md "wikilink")7200kW大功率电力机车、[HXN5内燃机车及](../Page/HXN5.md "wikilink")[CRH2动车组](../Page/CRH2.md "wikilink")。

### 牵引逆变器

  - TEP28WG04（GTO，用于[KZ4A型电力机车上](../Page/KZ4A型電力機車.md "wikilink")）
  - TAG-9（IGBT，用於[HXD1C及](../Page/HXD1C.md "wikilink")[HXD1D電力機車上](../Page/HXD1D.md "wikilink")）

### 辅助逆变器

  - TGF27（IGBT
    130kV，用于[港铁东铁线都城嘉慕列车上](../Page/港鐵東鐵綫都城嘉慕電動列車.md "wikilink")）

## 電動公交車產品

  - C12R

## 参考资料

## 外部链接

  - [tec.crrczic.cc](http://www.tec.crrczic.cc)
  - [1](https://web.archive.org/web/20101228143106/http://www.sanfull.com/pdf/2006_IPO/3898.pdf)

[Category:中国中车](../Category/中国中车.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:株洲公司](../Category/株洲公司.md "wikilink")
[Category:湖南上市公司](../Category/湖南上市公司.md "wikilink")
[Category:中國製造公司](../Category/中國製造公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:中国半导体公司](../Category/中国半导体公司.md "wikilink")
[Category:石峰区](../Category/石峰区.md "wikilink")
[Category:2005年成立的公司](../Category/2005年成立的公司.md "wikilink")

1.  [Dynex Completes Plan of
    Arrangement](http://www.dynexpower.com/news/60)
2.  [大功率半导体器件IGBT产业化基地奠基](http://www.timeselectric.cn/s/1089-3788-391.html)
3.  [公司8英寸IGBT芯片生产线即将投产](http://www.timeselectric.cn/s/1089-3788-131.html)
4.  [我国成功实现8英寸IGBT芯片产业化](http://www.most.gov.cn/dfkj/hun/zxdt/201406/t20140627_113977.htm)
5.  [CSR Times Electric open an 8-inch IGBT wafer and module production
    base in China](http://www.dynexpower.com/news/108)