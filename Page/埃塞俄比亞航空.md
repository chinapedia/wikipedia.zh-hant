**埃塞俄比亞航空**（，，簡寫為，[IATA編號](../Page/IATA.md "wikilink")：**ET**，[ICAO編號](../Page/ICAO.md "wikilink")：**ETH**）是[埃塞俄比亞的國營航空公司](../Page/埃塞俄比亞.md "wikilink")，總部位於[亚的斯亚贝巴的](../Page/亚的斯亚贝巴.md "wikilink")[亚的斯亚贝巴國際機場](../Page/博樂國際機場.md "wikilink")。公司經營多條定期客運和貨運航班，前往埃塞俄比亚和[非洲](../Page/非洲.md "wikilink")、[亚洲](../Page/亚洲.md "wikilink")、[欧洲](../Page/欧洲.md "wikilink")、[北美及](../Page/北美.md "wikilink")[南美共](../Page/南美.md "wikilink")113個目的地。另外，公司也提供客運和貨運的包機服務。2011年，埃塞俄比亚航空加入[星空联盟](../Page/星空联盟.md "wikilink")。埃塞俄比亚航空比全球任何一间航空公司在非洲都拥有更多航点，亦是非洲大陆上增长最快，拥有最大和最年轻机队的航空公司。

## 歷史

[Ethiopian_Airlines_Boeing_767-200ER_ET-AIF_FRA_2001-5-11.png](https://zh.wikipedia.org/wiki/File:Ethiopian_Airlines_Boeing_767-200ER_ET-AIF_FRA_2001-5-11.png "fig:Ethiopian_Airlines_Boeing_767-200ER_ET-AIF_FRA_2001-5-11.png")(舊版塗裝)\]\]
埃塞俄比亞航空於1945年12月30日在[環球航空](../Page/環球航空.md "wikilink")（TWA）的協助下成立，在1946年4月8日開始營運。首條航線為每週一班來往[亚的斯亚贝巴和](../Page/亚的斯亚贝巴.md "wikilink")[開羅的航線](../Page/開羅.md "wikilink")。剛開始營運時公司使用5架[道格拉斯DC-3型客機](../Page/DC-3.md "wikilink")，而當時的設計是乘客在機身的兩邊乘坐能摺疊的布椅。

雖然，公司開始時倚賴一批美國的機師和技工，但自從在1971年公司成立25年開始，公司由管理層以至前線員工都是埃塞俄比亞人。而Paul B.
Henze曾經形容公司為「第三世界中最可靠和最賺錢的航空公司」（one of the most reliable and profitable
airlines in the Third
World）。而財經雜誌《[經濟學人](../Page/經濟學人.md "wikilink")》也曾報導公司優良的狀況。在1998年，公司開始營運跨越[大西洋的路線](../Page/大西洋.md "wikilink")。

在2002年，公司的年載客量為1,054,687人。而截至2005年1月，公司聘請了4,539名員工。

雖然埃塞俄比亞航空的管理優良，可是由於當地政局不穩，該公司經常遭尋求政治庇護的劫機者劫機。

埃塞俄比亞航空已经於2011年12月13日正式加入[星空聯盟](../Page/星空聯盟.md "wikilink")，成為聯盟中第三家來自非洲的會員航空公司。

## 國內航線

[Ethiopian_Airlines_Boeing_777-200LR_Zhao-1.jpg](https://zh.wikipedia.org/wiki/File:Ethiopian_Airlines_Boeing_777-200LR_Zhao-1.jpg "fig:Ethiopian_Airlines_Boeing_777-200LR_Zhao-1.jpg")；埃塞俄比亞航空於1973年開通中國航線\[1\]\]\]
埃塞俄比亞航空現在經營以下航線：

  - 國內定期航班：**[亚的斯亚贝巴](../Page/亚的斯亚贝巴博莱国际机场.md "wikilink")**、Arba
    Minch、[阿索萨](../Page/阿索萨.md "wikilink")、[阿瓦萨](../Page/阿瓦萨.md "wikilink")、[阿克蘇姆](../Page/阿克蘇姆.md "wikilink")、[巴赫达尔](../Page/巴赫达尔.md "wikilink")、Dembidolo、Dessie、Dire
    Dawa、[甘贝拉](../Page/甘贝拉.md "wikilink")、Goba、Gode、[贡德尔](../Page/贡德尔.md "wikilink")、Humera、[吉吉加](../Page/吉吉加.md "wikilink")、Jimma、Kabri
    Dahar、[拉利贝拉](../Page/拉利贝拉.md "wikilink")、[默克莱](../Page/默克莱.md "wikilink")、[塞梅拉](../Page/塞梅拉.md "wikilink")、Shire。

## 國際航線

### 亞洲

  -

<!-- end list -->

  - [北京](../Page/北京.md "wikilink") -
    [北京首都國際機場](../Page/北京首都國際機場.md "wikilink")
  - [上海](../Page/上海.md "wikilink") -
    [上海浦東國際機場](../Page/上海浦東國際機場.md "wikilink")
  - [廣州](../Page/廣州.md "wikilink") -
    [廣州白雲國際機場](../Page/廣州白雲國際機場.md "wikilink")
  - [成都](../Page/成都.md "wikilink") -
    [成都雙流國際機場](../Page/成都雙流國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [香港國際機場](../Page/香港國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [德里](../Page/德里.md "wikilink") -
    [英迪拉·甘地國際機場](../Page/英迪拉·甘地國際機場.md "wikilink")
  - [孟買](../Page/孟買.md "wikilink") -
    [賈特拉帕蒂·希瓦吉國際機場](../Page/賈特拉帕蒂·希瓦吉國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [首爾](../Page/首爾.md "wikilink") -
    [仁川國際機場](../Page/仁川國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [東京](../Page/東京.md "wikilink") -
    [成田國際機場](../Page/成田國際機場.md "wikilink")
  - [大阪](../Page/大阪.md "wikilink") -
    [關西國際機場](../Page/關西國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [馬尼拉](../Page/馬尼拉.md "wikilink") -
    [尼諾伊·阿基諾國際機場](../Page/尼諾伊·阿基諾國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [新加坡樟宜機場](../Page/新加坡樟宜機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [曼谷](../Page/曼谷.md "wikilink") -
    [蘇凡納布國際機場](../Page/蘇凡納布國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [吉隆坡](../Page/吉隆坡.md "wikilink") -
    [吉隆坡國際機場](../Page/吉隆坡國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [雅加達](../Page/雅加達.md "wikilink") -
    [蘇加諾-哈達國際機場](../Page/蘇加諾-哈達國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [杜拜](../Page/杜拜.md "wikilink") -
    [杜拜國際機場](../Page/杜拜國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [科威特](../Page/科威特.md "wikilink") -
    [科威特國際機場](../Page/科威特國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [吉達](../Page/吉達.md "wikilink") -
    [阿卜杜勒-阿齐兹国王国际机场](../Page/阿卜杜勒-阿齐兹国王国际机场.md "wikilink")
  - [利雅德](../Page/利雅德.md "wikilink") -
    [哈立德国王国际机场](../Page/哈立德国王国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [特拉維夫](../Page/特拉維夫.md "wikilink") -
    [本-古里安国际机场](../Page/本-古里安国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [貝魯特](../Page/貝魯特.md "wikilink") -
    [贝鲁特-拉菲克·哈里里国际机场](../Page/贝鲁特-拉菲克·哈里里国际机场.md "wikilink")

### 美洲

  -

<!-- end list -->

  - [多倫多](../Page/多倫多.md "wikilink") -
    [多伦多皮爾遜國際機場](../Page/多伦多皮爾遜國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [洛杉磯](../Page/洛杉磯.md "wikilink") -
    [洛杉磯國際機場](../Page/洛杉磯國際機場.md "wikilink")
  - [紐華克](../Page/紐華克.md "wikilink") -
    [紐華克自由國際機場](../Page/紐華克自由國際機場.md "wikilink")
  - [華盛頓](../Page/華盛頓.md "wikilink") -
    [杜勒斯國際機場](../Page/杜勒斯國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [聖保羅](../Page/聖保羅.md "wikilink") -
    [聖保羅國際機場](../Page/聖保羅國際機場.md "wikilink")

### 歐洲

  -

<!-- end list -->

  - [維也納](../Page/維也納.md "wikilink") -
    [維也納國際機場](../Page/維也納國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [布魯塞爾](../Page/布魯塞爾.md "wikilink") -
    [布魯塞爾機場](../Page/布魯塞爾機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [巴黎](../Page/巴黎.md "wikilink") -
    [巴黎夏爾·戴高樂機場](../Page/巴黎夏爾·戴高樂機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [法蘭克福](../Page/法蘭克福.md "wikilink") -
    [法蘭克福機場](../Page/法蘭克福機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [都柏林](../Page/都柏林.md "wikilink") -
    [都柏林機場](../Page/都柏林機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [米蘭](../Page/米蘭.md "wikilink") -
    [馬爾彭薩機場](../Page/馬爾彭薩機場.md "wikilink")
  - [羅馬](../Page/羅馬.md "wikilink") -
    [菲烏米奇諾機場](../Page/菲烏米奇諾機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [奧斯陸](../Page/奧斯陸.md "wikilink") -
    [奧斯陸加勒穆恩機場](../Page/奧斯陸加勒穆恩機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [馬德里](../Page/馬德里.md "wikilink") -
    [馬德里—巴拉哈斯機場](../Page/馬德里—巴拉哈斯機場.md "wikilink")

<!-- end list -->

  -

[斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink") -
[斯德哥爾摩—阿蘭達機場](../Page/斯德哥爾摩—阿蘭達機場.md "wikilink")

  -

<!-- end list -->

  - [基輔](../Page/基輔.md "wikilink") -
    [鮑里斯波爾國際機場](../Page/鮑里斯波爾國際機場.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [倫敦](../Page/倫敦.md "wikilink") -
    [倫敦希思羅機場](../Page/倫敦希思羅機場.md "wikilink")

### 非洲

  -

<!-- end list -->

  - [開羅](../Page/開羅.md "wikilink") -
    [開羅国际机场](../Page/開羅国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [巴馬科](../Page/巴馬科.md "wikilink") -
    [巴马科国际机场](../Page/巴马科国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [阿必尚](../Page/阿必尚.md "wikilink") -
    [阿必尚国际机场](../Page/阿必尚国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [阿布賈](../Page/阿布賈.md "wikilink") -
    [纳姆迪·阿齐基韦国际机场](../Page/纳姆迪·阿齐基韦国际机场.md "wikilink")
  - [拉哥斯](../Page/拉哥斯.md "wikilink") -
    [穆尔塔拉·穆罕默德国际机场](../Page/穆尔塔拉·穆罕默德国际机场.md "wikilink")
  - [卡诺](../Page/卡诺.md "wikilink") -
    [卡諾国际机场](../Page/卡諾国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [阿克拉](../Page/阿克拉.md "wikilink") -
    [科托卡国际机场](../Page/科托卡国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [塔那那利佛](../Page/塔那那利佛.md "wikilink") -
    [塔那那利佛/伊瓦图国际机场](../Page/塔那那利佛/伊瓦图国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [布蘭泰爾](../Page/布蘭泰爾.md "wikilink") -
    [奇勒卡國際機場](../Page/奇勒卡國際機場.md "wikilink")
  - [里郎威](../Page/里郎威.md "wikilink") -
    [里郎威國際機場](../Page/里郎威國際機場.md "wikilink")、[利隆圭](../Page/利隆圭国际机场.md "wikilink")、

<!-- end list -->

  -

<!-- end list -->

  - [馬斯喀特](../Page/馬斯喀特.md "wikilink") -
    [马斯喀特国际机场](../Page/马斯喀特国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [內羅畢](../Page/內羅畢.md "wikilink") -
    [乔莫·肯雅塔国际机场](../Page/乔莫·肯雅塔国际机场.md "wikilink")

[蒙巴萨](../Page/莫伊国际机场.md "wikilink")、

  -

<!-- end list -->

  - [尼阿美](../Page/尼阿美.md "wikilink") -
    [迪奥里·哈马尼国际机场](../Page/迪奥里·哈马尼国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [恩將那](../Page/恩將那.md "wikilink") -
    [恩贾梅纳国际机场](../Page/恩贾梅纳国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [布拉柴维尔](../Page/马亚-马亚国际机场.md "wikilink")、[黑角](../Page/黑角.md "wikilink")、

<!-- end list -->

  -

<!-- end list -->

  - [布琼布拉](../Page/布琼布拉国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [科纳克里](../Page/科纳克里国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [科托努](../Page/柯多努国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [達卡](../Page/达喀尔-约夫-利奥波德·塞达尔·桑戈尔国际机场.md "wikilink")、

<!-- end list -->

  -

<!-- end list -->

  - [达累斯萨拉姆](../Page/朱利叶斯·尼雷尔国际机场.md "wikilink")
  - [桑给巴尔](../Page/桑给巴尔.md "wikilink")
  - [乞力马扎罗山](../Page/乞力马扎罗山.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [吉布提](../Page/吉布提-安布利国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [杜阿拉](../Page/杜阿拉国际机场.md "wikilink")
  - [雅温得](../Page/雅温得-恩西马兰国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [坎帕拉─恩德培](../Page/恩德培国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [哈博罗内](../Page/塞雷茨·卡马爵士国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [哈拉雷](../Page/哈拉雷国际机场.md "wikilink")

[维多利亚瀑布](../Page/维多利亚瀑布.md "wikilink")

  -

<!-- end list -->

  - [哈尔格萨](../Page/埃加勒国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink") -
    [約翰尼斯堡國際機場](../Page/約翰尼斯堡國際機場.md "wikilink")
  - [開普敦](../Page/開普敦.md "wikilink") -
    [开普敦国际机场](../Page/开普敦国际机场.md "wikilink")
  - [德班](../Page/德班.md "wikilink") -
    [沙卡国王国际机场](../Page/沙卡国王国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [喀土穆](../Page/喀土穆.md "wikilink") -
    [喀土穆国际机场](../Page/喀土穆国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [朱巴](../Page/朱巴.md "wikilink") -
    [朱巴国际机场](../Page/朱巴国际机场.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [吉佳利](../Page/吉佳利.md "wikilink") -
    [基加利国际机场](../Page/基加利国际机场.md "wikilink")

<!-- end list -->

  -

[金沙萨](../Page/恩吉利国际机场.md "wikilink")
[卢本巴希](../Page/卢本巴希国际机场.md "wikilink")

  -

[利伯维尔](../Page/利伯维尔国际机场.md "wikilink")

  -

[洛美](../Page/纳辛贝·埃亚德马国际机场.md "wikilink")

  -

[羅安達](../Page/二月四日国际机场.md "wikilink")

  -

[卢萨卡](../Page/肯尼思·卡翁达国际机场.md "wikilink")
[恩多拉](../Page/恩多拉.md "wikilink")

  -

[塞舌尔](../Page/塞舌尔国际机场.md "wikilink")

  -

[马拉博](../Page/马拉博机场.md "wikilink")

  -

[马普托](../Page/马普托国际机场.md "wikilink")

  -

[莫罗尼](../Page/赛义德·易卜拉欣王子国际机场.md "wikilink")

  -

[瓦加杜古](../Page/瓦加杜古国际机场.md "wikilink")、

  -

[溫德和克](../Page/溫德和克.md "wikilink") -
[霍齐亚·库塔科国际机场](../Page/霍齐亚·库塔科国际机场.md "wikilink")。

## 事故

自1970年起，公司的航班發生了四次致命的航空事故及一次起火事故。

  - 在1988年9月15日，[埃塞俄比亞航空604號班機](../Page/埃塞俄比亞航空604號班機空難.md "wikilink")，一架[波音737客機](../Page/波音737.md "wikilink")，在巴赫達爾起飛後，航機的兩個引擎都吸入了大量的[白鴿](../Page/鴿.md "wikilink")。其中一個引擎立即失去動力，而第二個引擎則在緊急折返的途中失去動力。結果在着陸時墜毀，導致機上105名乘客當中，有31人死亡。

<!-- end list -->

  - 在1996年11月23日，三名劫機者騎劫[埃塞俄比亞航空961號班機](../Page/埃塞俄比亞航空961號班機.md "wikilink")。航班原定由[亚的斯亚贝巴前往](../Page/亚的斯亚贝巴.md "wikilink")[内罗毕](../Page/内罗毕.md "wikilink")，再前往[布拉柴維爾](../Page/布拉柴維爾.md "wikilink")，再前往[拉各斯](../Page/拉各斯.md "wikilink")，最後到達[阿比讓](../Page/阿比讓.md "wikilink")。而當時該三名劫機者則要求客機前往[澳洲](../Page/澳洲.md "wikilink")。客機於是沿海岸線向南走，然後途中耗盡備用燃油，導致一個引擎停止運作。最後客機在[科摩羅的](../Page/科摩羅.md "wikilink")[莫洛尼完全耗盡燃油](../Page/莫洛尼.md "wikilink")，飛機在離岸邊500米的海中墜毀。事件導致客機上175名乘客和機員中，有125人死亡，包括3名劫機者。\[2\]

<!-- end list -->

  - 在2010年1月25日，[埃塞俄比亞航空409號班機](../Page/埃塞俄比亞航空409號班機空難.md "wikilink")，载有92人的该航空公司[波音737-800客机从](../Page/波音737.md "wikilink")[黎巴嫩](../Page/黎巴嫩.md "wikilink")[贝鲁特起飛后坠入](../Page/贝鲁特.md "wikilink")[地中海](../Page/地中海.md "wikilink")。

<!-- end list -->

  - 2013年7月13日，[波音787客機在英國倫敦希思羅機場停機坪上起火](../Page/波音787.md "wikilink")。

<!-- end list -->

  - 2017年12月20日，[埃塞俄比亚航空409号班机的一架](../Page/埃塞俄比亚航空606号班机突发事件.md "wikilink")[波音777-300ER在执飞亚的斯亚贝巴](../Page/波音777.md "wikilink")-广州航线时，一名乘客突发心脏病，飞机紧急降落[昆明长水国际机场](../Page/昆明长水国际机场.md "wikilink")，最终该乘客抢救无效死亡。

<!-- end list -->

  - 2019年3月10日，[埃塞俄比亞航空302號班機的一架載有](../Page/埃塞俄比亚航空302号班机空难.md "wikilink")157人的[波音737MAX-8客機墜毀](../Page/波音737MAX.md "wikilink")，客機從埃塞俄比亞首都[亚的斯亚贝巴出發](../Page/亚的斯亚贝巴.md "wikilink")，前往肯尼亞首都[內羅畢](../Page/奈洛比.md "wikilink")，但機起飛後不久隨即失事，於8時44分墜毀，机上人员全部遇难。\[3\]

## 機隊

[ET-AOT@HKG_(20181006142313).jpg](https://zh.wikipedia.org/wiki/File:ET-AOT@HKG_\(20181006142313\).jpg "fig:ET-AOT@HKG_(20181006142313).jpg")客機在[香港國際機場降落](../Page/香港國際機場.md "wikilink")。\]\]
[ethiopian_b767-300er_et-all_arp.jpg](https://zh.wikipedia.org/wiki/File:ethiopian_b767-300er_et-all_arp.jpg "fig:ethiopian_b767-300er_et-all_arp.jpg")客機在倫敦希斯路機場降落。\]\]
[Ethiopian_Airlines_Fokker.jpg](https://zh.wikipedia.org/wiki/File:Ethiopian_Airlines_Fokker.jpg "fig:Ethiopian_Airlines_Fokker.jpg")飛機。\]\]

截至2019年3月，埃塞俄比亞航空機隊如下：

<center>

| <font style="color:white;">客機                       |
| --------------------------------------------------- |
| <font color=white>機型</font>                         |
| <abbr title="商務艙"><font color=white>P</font></abbr> |
| [空中巴士A350-900](../Page/空中客车A350.md "wikilink")      |
| [波音737-700](../Page/波音737.md "wikilink")            |
| [波音737-800](../Page/波音737.md "wikilink")            |
| [波音737 MAX 8](../Page/波音737.md "wikilink")          |
| [波音767-360ER](../Page/波音767.md "wikilink")          |
| 24                                                  |
| 24                                                  |
| 24                                                  |
| [波音777-200LR](../Page/波音777.md "wikilink")          |
| [波音777-300ER](../Page/波音777.md "wikilink")          |
| [波音787-8](../Page/波音787.md "wikilink")              |
| [波音787-9](../Page/波音787.md "wikilink")              |
| [龐巴迪Dash 8 Q400](../Page/Dash_8.md "wikilink")      |
| 7                                                   |
| 7                                                   |
| <font style="color:white;">貨機                       |
| [波音737-800SF](../Page/波音737.md "wikilink")          |
| [波音777F](../Page/波音777.md "wikilink")               |
| **總數**                                              |

**埃塞俄比亞航空機隊**\[4\] \[5\]

</center>

## 參考資料

## 外部連結

  - [官方網站](https://www.ethiopianairlines.com)
  - [機隊年齡](http://www.airfleets.net/ageflotte/Ethiopian%20Airlines.htm)
  - [波音客機機隊資料](http://www.planespotters.net/Airline/Ethiopian_Airlines?show=all)
  - [乘客意見](http://www.airlinequality.com/Forum/ethiopian.htm)

[埃塞俄比亚航空](../Category/埃塞俄比亚航空.md "wikilink")
[Category:1945年成立的航空公司](../Category/1945年成立的航空公司.md "wikilink")
[Category:埃塞俄比亞航空公司](../Category/埃塞俄比亞航空公司.md "wikilink")

1.
2.  [Special Report: Ethiopian Airlines
    Flight 961](https://www.webcitation.org/6AtdKPxWo?url=http://www.airdisaster.com/special/ethiopian961.shtml)
3.  [埃航客机失事无人生还
    波音发布声明称深感悲痛](https://www.chinanews.com/wap/detail/zwsp/gj/2019/03-10/8776498.shtml)，中国新闻网
4.
5.  <https://www.planespotters.net/airline/Ethiopian-Airlines>