**X诗社**，也被称为**X集团**、**X小组**，是指1963年，几位[北京的热爱文学的青年](../Page/北京.md "wikilink")（包括一些[高干子弟](../Page/高干子弟.md "wikilink")）组织的一个文学社团，互相交流作品、感想，X表示很多含义，包括[未知数](../Page/未知数.md "wikilink")、[十字架](../Page/十字架.md "wikilink")、[十字街头](../Page/十字街头.md "wikilink")，乃至“探索[哲学领域中诸多难题](../Page/哲学.md "wikilink")”。但这个文学社团，后被当时的[中国共产党政府](../Page/中国共产党.md "wikilink")，当作[反革命集团来处理](../Page/反革命集团.md "wikilink")，甚至[周恩来都亲自过问此事](../Page/周恩来.md "wikilink")。

## 成员

  - [张鹤慈](../Page/张鹤慈.md "wikilink")：1943年8月27日出生。[哲学家](../Page/哲学家.md "wikilink")[张东荪之孙](../Page/张东荪.md "wikilink")、[北大](../Page/北大.md "wikilink")[生物学教授](../Page/生物学.md "wikilink")[张宗炳之子](../Page/张宗炳.md "wikilink")；
  - [孙经武](../Page/孙经武.md "wikilink")：父亲是[总后勤部副部长](../Page/总后勤部.md "wikilink")、少将军衔的长征干部；
  - [郭世英](../Page/郭世英.md "wikilink")：[郭沫若之子](../Page/郭沫若.md "wikilink")，[北大哲学系学生](../Page/北大.md "wikilink")；
  - [周国平](../Page/周国平.md "wikilink")：[北大哲学系学生](../Page/北大.md "wikilink")；
  - [曹天予](../Page/曹天予.md "wikilink")：[北大哲学系学生](../Page/北大.md "wikilink")；
  - [叶蓉青](../Page/叶蓉青.md "wikilink")、[金蝶](../Page/金蝶.md "wikilink")、[牟敦白](../Page/牟敦白.md "wikilink")。
  - 张鹤慈在新浪微博指出：成员只包括：张鹤慈，孙经武、郭世英、叶蓉青，牟敦白不但不是成员，而且他当时根本不知道这件事

## 过程

1963年2月，[郭世英与](../Page/郭世英.md "wikilink")[张鹤慈](../Page/张鹤慈.md "wikilink")、孙经武等人结成X诗社，这个社团不成立有形组织、不出版有形刊物、不制定成文纲领、订立保密规则。
1963年5月上旬，有人向有关部门告发了X诗社离经叛道的情况，公安人员很快立案，随后X诗社成员被迫交代自己的“反动”思想。

## 结果

  - [张鹤慈](../Page/张鹤慈.md "wikilink")：被判[劳动教养二年](../Page/劳动教养.md "wikilink")。
  - [孙经武](../Page/孙经武.md "wikilink")：被判劳动教养二年。
  - [郭世英](../Page/郭世英.md "wikilink")：受到宽大处理，到[河南一家农场劳动一年](../Page/河南.md "wikilink")，期满后，在他自己要求下，又延长了一年。但1968年4月，被[北京农业大学的](../Page/北京农业大学.md "wikilink")[红卫兵抓去](../Page/红卫兵.md "wikilink")，刑讯逼供，追究“谁包庇了反动学生郭世英”，4月22日，倍受折磨的郭世英从四楼坠下而死。
  - 金蝶：事先已经移居[香港](../Page/香港.md "wikilink")。

## 参考资料

1.  X诗社与郭世英之死，<http://www.lingshidao.com/shilun/xinshi/46.htm>
2.  郭民英与郭世英，<http://www.china.com.cn/chinese/RS/617164.htm>
3.  谈谈二弟张鹤慈、郭世英与“X集团”案，<https://web.archive.org/web/20060113200653/http://www.edubridge.com/erxiantang/library/zhangheci.htm>
4.  周国平回忆郭世英——"重要的是，他在思考"，<http://www.nanfangdaily.com.cn/zm/20040805/wh/wxydhc/200408050022.asp>
5.  新浪微博：http://weibo.com/1971861621/z6yvCuaVH

[Category:文学组织](../Category/文学组织.md "wikilink")
[Category:文化大革命](../Category/文化大革命.md "wikilink")