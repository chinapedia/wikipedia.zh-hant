**鲍登學院**（**Bowdoin College**
）是位于美国[缅因州的一所私立](../Page/缅因州.md "wikilink")[文理學院](../Page/美國文理學院.md "wikilink")。

## 历史

[Bowdoin-chapel-winter.jpg](https://zh.wikipedia.org/wiki/File:Bowdoin-chapel-winter.jpg "fig:Bowdoin-chapel-winter.jpg")
1794年成立。它以[詹姆斯·鲍登的名字命名](../Page/詹姆斯·鲍登.md "wikilink")，以纪念这位[马萨诸塞州前州长](../Page/马萨诸塞州.md "wikilink")。该大学在美国文理学院中排名第五名以内，在美国东北海岸享有盛名。该校注重通才教育，意即注重学生的全面素质以及修养。学生入校不需要决定专业，学校要求学生学习不同领域的课程。学生一般在大二第二个学期决定专业。

前[美国总统](../Page/美国总统.md "wikilink")[福兰克林·皮尔斯](../Page/福兰克林·皮尔斯.md "wikilink")，著名美国文学家[霍桑](../Page/霍桑.md "wikilink")，诗人[朗费罗](../Page/朗费罗.md "wikilink")，诗人[梭罗皆为該校毕业生](../Page/梭罗.md "wikilink")。

## 學術

它是美國最好的[文理學院之一](../Page/美國文理學院.md "wikilink")。按學生SAT成績，它被《[商业内幕](../Page/商业内幕.md "wikilink")》評為美國聰明人最多的學院之一，全國排名第五\[1\]。

## 参考文献

## 外部链接

  - [官方网站](http://www.bowdoin.edu/)

[鲍登学院](../Category/鲍登学院.md "wikilink")
[Category:美国文理学院](../Category/美国文理学院.md "wikilink")
[Category:1790年代創建的教育機構](../Category/1790年代創建的教育機構.md "wikilink")
[Category:緬因州大學](../Category/緬因州大學.md "wikilink")

1.