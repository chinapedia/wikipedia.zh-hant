**大坑東邨**（英語：**Tai Hang Tung
Estate**）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[深水埗區東面近](../Page/深水埗區.md "wikilink")[九龍塘](../Page/九龍塘.md "wikilink")，於1955年開始入伙，於1979年開始重建，新建樓宇於分別於1983年、1986年及2002年入伙，現由[領先管理有限公司負責屋邨管理](../Page/領先管理有限公司.md "wikilink")。

大坑東邨位於[南山邨的南面](../Page/南山邨.md "wikilink")，以及[大坑西邨的東面](../Page/大坑西邨.md "wikilink")，大坑東道是其主要的街道。

## 歷史

第二次世界大戰後，大量內地難民移港定居，由於當時[香港政府沒有一個房屋政策來為低下階層而設的](../Page/香港殖民地時期#香港政府.md "wikilink")，所以很多難民在1949年至1953年間在山邊興建木屋來居住，但這些木屋設備簡陋，衛生環境惡劣，加上木屋的密度高，因而經常發生火警。

**已結束的學校**

  - 榮基學校(1座)
  - 深培學校(2座)
  - 陶庵學校(3座)
  - 信義宗信光學校(4座)
  - 基督教靈磐學校(5座)
  - 信義學校(6座)
  - 神召會嘉貞學校(7座)
  - 香港信義會信德學校(11座)
  - 救世軍光耀學校(13座)
  - 救世軍兒童團契學校(14座)
  - 聖伯多祿學校(14座)

### 安置災民

[Tai_Hang_Tung_Estate_Overview_201504.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_Overview_201504.jpg "fig:Tai_Hang_Tung_Estate_Overview_201504.jpg")
[Tai_Hang_Tung_Estate_cantonese_restaurant.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_cantonese_restaurant.jpg "fig:Tai_Hang_Tung_Estate_cantonese_restaurant.jpg")
[Tai_Hang_Tung_Estate_Badminton_Court.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_Badminton_Court.jpg "fig:Tai_Hang_Tung_Estate_Badminton_Court.jpg")
[Tai_Hang_Tung_Estate_Playground.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_Playground.jpg "fig:Tai_Hang_Tung_Estate_Playground.jpg")
[Tai_Hang_Tung_Estate_Gym_Zone.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_Gym_Zone.jpg "fig:Tai_Hang_Tung_Estate_Gym_Zone.jpg")
[Tai_Hang_Tung_Estate_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_Pebble_Walking_Trail.jpg "fig:Tai_Hang_Tung_Estate_Pebble_Walking_Trail.jpg")
[Tai_Hang_Tung_Estate_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Estate_Covered_Walkway.jpg "fig:Tai_Hang_Tung_Estate_Covered_Walkway.jpg")
[Tai_Hang_Tung_Bus_Terminus.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Tung_Bus_Terminus.jpg "fig:Tai_Hang_Tung_Bus_Terminus.jpg")
1953年的聖誕夜，[石硤尾木屋區發生了史無前例的](../Page/石硤尾.md "wikilink")[大火](../Page/石硤尾大火.md "wikilink")，令5萬多人無家可歸，於是政府在石硤尾一帶興建[徙置大廈](../Page/徙置大廈.md "wikilink")。到了1954年7月22日，另一場大火在大坑東木屋區發生，令2萬多人無家可歸，政府便仿照石硤尾的做法興建大坑東邨以容納災民。除了大坑東外，還在[大窩口和](../Page/大窩口.md "wikilink")[李鄭屋興建徙置大廈](../Page/李鄭屋.md "wikilink")，時至今天，大坑東、李鄭屋和大窩口的徙置大廈已重建完成，而石硤尾的徙置大廈已全部拆卸。

### 重建

1970年代，大坑東邨開始進行重建工程，與[石硤尾邨一樣](../Page/石硤尾邨.md "wikilink")，[香港房屋委員會不是把所有徙置大廈重建](../Page/香港房屋委員會.md "wikilink")，而是把部分徙置大廈改建，其餘拆卸重建，被改建的有第1、2及14座，當中第1、2座兩翼中間的廁樓被拆去，單位加入獨立廚廁，而第14座則將兩個前後單位打通，住戶仍需使用中間的共用廁所。改建工程於1980年完成，其餘的新樓宇於1983-1986年入伙，而本邨的東海樓、東輝樓、東成樓、東裕樓、東滿樓及東旺樓是首批落成及層數最少的[相連長型大廈](../Page/相連長型大廈.md "wikilink")，只有11層。原因是本邨興建時位於飛機航道之下，樓宇高度受限制。由於相連長型的外觀較長而窄，加上設計較靈活，所以這個樓宇設計其後被使用於面積較細及受高度限制的徙置大廈重建工程地盤，所以相連長型的樓宇大多數於市區找到。

東新樓（第14座）於2001年拆卸，受影響的居民大部分都安置到[幸福邨](../Page/幸福邨.md "wikilink")。餘下的四座徙置大廈（東榮樓、東富樓、東運樓及東和樓）於2003年全面拆卸，受影響的居民原被安排搬遷至[富昌邨](../Page/富昌邨.md "wikilink")，但因前[民協區議員](../Page/民協.md "wikilink")[王桂雲及](../Page/王桂雲.md "wikilink")[梁錦滔與一班街坊爭取原區安置](../Page/梁錦滔.md "wikilink")，最後利用邨內原有硬地足球場位置興建了東健樓及東怡樓，大部分都安置到新建的邨內兩座樓宇，而將後期拆卸的四座徙置大廈原址以地換地方式重建回球場。

## 屋邨資料

### 現有樓宇

| 樓宇名稱 | 樓宇類型                                    | 落成年份 |
| ---- | --------------------------------------- | ---- |
| 東龍樓  | 舊長型                                     | 1983 |
| 東成樓  | [相連長型第一款](../Page/相連長型大廈.md "wikilink") | 1986 |
| 東輝樓  |                                         |      |
| 東海樓  |                                         |      |
| 東裕樓  |                                         |      |
| 東滿樓  |                                         |      |
| 東旺樓  |                                         |      |
| 東健樓  | 小型單位大廈                                  | 2002 |
| 東怡樓  |                                         |      |
|      |                                         |      |

Tai Hang Tung Road.jpg|大坑東邨東輝樓及東健樓、東怡樓，右方是南山邨 Tai Hang Tung Estate Chess
Zone and Playground (2).jpg|棋藝區及兒童遊樂場（2） Tai Hang Tung Estate Gym Zone
(2) and Pavilion.jpg|健體區（2）及涼亭 Tai Hang Tung Estate Drying Clothes
Rack.jpg|邨內設有多個曬衣架

### 歷代樓宇

<div class="mw-collapsible mw-collapsed" style="padding:7px; border-radius:7px; background: rgb(209, 240, 255); width:50%;">

<div class="mw-collapsible-toggle" style="float:none; font-size:1.2em; ">

樓宇歷史及變遷

</div>

<div class="mw-collapsible-content">

| 名稱/座號 | 類型       | 落成年份 | 改建年份     | 拆卸年份    | 原座號 |
| ----- | -------- | ---- | -------- | ------- | --- |
| 第3座   | 第1型      | 1955 | 不適用      | 1983    | M座  |
| 第4座   | L座       |      |          |         |     |
| 第5座   | K座       |      |          |         |     |
| 第6座   | J座       |      |          |         |     |
| 第7座   | G座       |      |          |         |     |
| 第8座   | H座       |      |          |         |     |
| 第9座   | F座       |      |          |         |     |
| 第10座  | E座       |      |          |         |     |
| 第11座  | D座       |      |          |         |     |
| 第12座  | C座       |      |          |         |     |
| 第13座  | B座       |      |          |         |     |
| 東新樓   | 半改建第1型   | 1980 | 2001     | 第14座(A) |     |
| 東榮樓   | 改建第1型    | 2003 | 第1座(N)北翼 |         |     |
| 東富樓   | 第1座(N)南翼 |      |          |         |     |
| 東運樓   | 第2座(O)北翼 |      |          |         |     |
| 東和樓   | 第2座(O)南翼 |      |          |         |     |

</div>

</div>

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/石硤尾站.md" title="wikilink">石硤尾站</a></li>
<li><font color="{{東鐵綫色彩}}">█</font><a href="../Page/東鐵綫.md" title="wikilink">東鐵綫</a>、<font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/九龍塘站_(香港).md" title="wikilink">九龍塘站</a></li>
</ul>
<dl>
<dt><a href="../Page/大坑東道.md" title="wikilink">大坑東道</a>/<a href="../Page/桃源街.md" title="wikilink">桃源街</a></dt>

</dl>
<dl>
<dt><a href="../Page/公共小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/黃大仙.md" title="wikilink">黃大仙</a>/<a href="../Page/九龍城.md" title="wikilink">九龍城至</a><a href="../Page/青山道.md" title="wikilink">青山道線</a>[1]</li>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至青山道線</a>[2]</li>
<li><a href="../Page/油塘.md" title="wikilink">油塘</a>/<a href="../Page/藍田.md" title="wikilink">藍田至青山道線</a>[3]</li>
<li>觀塘/黃大仙至青山道線 (通宵服務)[4]</li>
</ul>
<dl>
<dt><a href="../Page/窩仔街.md" title="wikilink">窩仔街</a></dt>

</dl>
<dl>
<dt><a href="../Page/公共小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/黃大仙.md" title="wikilink">黃大仙</a>/<a href="../Page/九龍城.md" title="wikilink">九龍城至</a><a href="../Page/青山道.md" title="wikilink">青山道線</a></li>
<li><a href="../Page/觀塘.md" title="wikilink">觀塘至青山道線</a></li>
<li><a href="../Page/油塘.md" title="wikilink">油塘</a>/<a href="../Page/藍田.md" title="wikilink">藍田至青山道線</a></li>
<li>觀塘/黃大仙至青山道線 (通宵服務)</li>
</ul></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部連結

  - [房屋署大坑東邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=3191)

[Category:石硤尾](../Category/石硤尾.md "wikilink")
[Category:2002年完工建築物](../Category/2002年完工建築物.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [黃大仙及九龍城—青山道](http://www.16seats.net/chi/rmb/r_k62.html)
2.  [觀塘協和街—青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)
3.  [青山道香港紗廠—藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)
4.  [觀塘及黃大仙—青山道](http://www.16seats.net/chi/rmb/r_k22.html)