**王俊棠**（**Ricky Wong Chun
Tong**，），人稱**卡啦之星**、**棠哥**，是一位香港[演員](../Page/演員.md "wikilink")，於1980年代中期活躍至今。現為[無綫電視基本藝人合約藝員](../Page/無綫電視.md "wikilink")。

## 簡介

王俊棠曾就讀[香港航海學校](../Page/香港航海學校.md "wikilink")，他於1979年出道，當時的[藝名是](../Page/藝名.md "wikilink")「伊凡威」，其後一直在1997至1998年間使用。王俊棠於1970年代，曾任職警察。

王俊棠洋名Ricky，曾有傳聞以前為[譚詠麟](../Page/譚詠麟.md "wikilink")[保鑣](../Page/保鑣.md "wikilink")，但王俊棠本人在2014年12月19日，已於[香港電台第二台節目](../Page/香港電台.md "wikilink")《守下留情》訪問中澄清這實屬誤傳，他並未任職過保鑣。

曾是[李修賢旗下](../Page/李修賢.md "wikilink")「[萬能影業公司](../Page/萬能影業公司.md "wikilink")」[演員](../Page/演員.md "wikilink")。

由鄺業生監製找他開始入TVB拍電視劇。

王俊棠擅長演出一些打打殺殺的大塊頭角色、囂張[武夫](../Page/武夫.md "wikilink")、[粗人](../Page/粗人.md "wikilink")、[打手](../Page/打手.md "wikilink")、[惡霸](../Page/惡霸.md "wikilink")、[社團人士](../Page/社團人士.md "wikilink")、[大隻佬等角色](../Page/大隻佬.md "wikilink")，有時候也會演出喜感十足的[搞笑](../Page/搞笑.md "wikilink")[角色](../Page/角色.md "wikilink")。其演技也深受觀眾認同。但現實生活中的**王俊棠**愛好唱歌
,成功參加數屆[歡樂滿東華表演](../Page/歡樂滿東華.md "wikilink")，爲[東華醫院籌善款](../Page/東華醫院.md "wikilink")，後成為[慈善演唱](../Page/慈善演唱.md "wikilink")[全台藝員卡拉之星大唱遊的常客](../Page/全台藝員卡拉之星大唱遊.md "wikilink")，並引進他在多個佛教及社團演出
,他在電影演出中，還有數次與影壇大哥大[周星馳合作](../Page/周星馳.md "wikilink")，如風雨同路、霹靂先鋒等，對於甘草演員來說確是一個不錯的演出經歷。

[Kung_Fu_Dim_Sum_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Kung_Fu_Dim_Sum_\(Hong_Kong\).jpg "fig:Kung_Fu_Dim_Sum_(Hong_Kong).jpg")
現時除了拍電影、唱歌外，他還是功夫點心店的東主。\[1\]

2017年11月，接受訪問時表示擔心孻仔自己沒有能力照顧自己，所以沒有想過他會養我，反而我想他回內地發展，因為內地機會較多。而自己都會用自己的人脈，幫他在內地找工作，讓外人教他，始終他在溫室長大，要時間讓他成長，他可以做得好一點，但常常三分鐘熱度，以為自己聰明，我估大多數年輕人都是這樣，他仍處於方向未明的階段\[2\]。

## 家庭

王俊棠有2個兒子王子銘,是攝影師,，是前香港[無綫電視合約男藝員](../Page/無綫電視.md "wikilink")。

## 作品

### 電視劇（[無線電視](../Page/無線電視.md "wikilink")）

|                                                    |                                               |                                  |
| -------------------------------------------------- | --------------------------------------------- | -------------------------------- |
| **年份**                                             | **电视剧**                                       | **角色**                           |
| 1998年                                              | [鹿鼎記](../Page/鹿鼎記_\(1998年電視劇\).md "wikilink") | [鰲拜](../Page/鰲拜.md "wikilink")   |
| [掃黃先鋒](../Page/掃黃先鋒.md "wikilink")                 | 大飛                                            |                                  |
| [陀槍師姐](../Page/陀槍師姐.md "wikilink")                 | 聶世官                                           |                                  |
| 1999年                                              | [鑑證實錄II](../Page/鑑證實錄II.md "wikilink")        | 葉冠榮                              |
| [烈火雄心](../Page/烈火雄心.md "wikilink")                 | 消防員                                           |                                  |
| [寵物情緣](../Page/寵物情緣.md "wikilink")                 | 史泰龍                                           |                                  |
| [雪山飛狐](../Page/雪山飛狐_\(1999年電視劇\).md "wikilink")    | 商劍鳴                                           |                                  |
| [創世紀](../Page/創世紀_\(電視劇\).md "wikilink")           | 監獄長                                           |                                  |
| 2000年                                              | [碧血劍](../Page/碧血劍_\(2000年電視劇\).md "wikilink") | 溫家堡溫氏老大                          |
| [男親女愛](../Page/男親女愛.md "wikilink")                 | 何英俊                                           |                                  |
| [陀槍師姐II](../Page/陀槍師姐II.md "wikilink")             | 張子貴                                           |                                  |
| 2001年                                              | [封神榜](../Page/封神榜_\(2001年電視劇\).md "wikilink") | 哼將                               |
| [倚天屠龍記](../Page/倚天屠龍記_\(2001年電視劇\).md "wikilink")  | 周顛                                            |                                  |
| [尋秦記](../Page/尋秦記_\(電視劇\).md "wikilink")           | 囂魏牟                                           |                                  |
| 2002年                                              | [皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink")  | 惡霸森／獨臂刀／王大人                      |
| [談判專家](../Page/談判專家_\(電視劇\).md "wikilink")         | 高大威                                           |                                  |
| [紅衣手記](../Page/紅衣手記.md "wikilink")                 | 劉柴                                            |                                  |
| [雲海玉弓緣](../Page/雲海玉弓緣_\(無綫電視劇集\).md "wikilink")    | 厲昐歸                                           |                                  |
| [烈火雄心II](../Page/烈火雄心II.md "wikilink")             | 張偉波（波Sir）                                     |                                  |
| [蕭十一郎](../Page/蕭十一郎_\(無綫電視劇\).md "wikilink")       | 軒轅                                            |                                  |
| 2003年                                              | [俗世情真](../Page/俗世情真.md "wikilink")            | 化骨龍                              |
| [西關大少](../Page/西關大少.md "wikilink")                 | 廣州市財政廳長                                       |                                  |
| [帝女花](../Page/帝女花_\(無綫電視劇\).md "wikilink")         | [多爾袞](../Page/多爾袞.md "wikilink")              |                                  |
| [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink")       | 趙公子、廟街大隻B、崔生、馮華                               |                                  |
| 2004年                                              | [無名天使3D](../Page/無名天使3D.md "wikilink")        | 老大                               |
| [陀槍師姐IV](../Page/陀槍師姐IV.md "wikilink")             | 麥耀東                                           |                                  |
| [棟篤神探](../Page/棟篤神探.md "wikilink")                 | 龍慶松                                           |                                  |
| [大唐雙龍傳](../Page/大唐雙龍傳_\(電視劇\).md "wikilink")       | [隋炀帝](../Page/隋炀帝.md "wikilink")              |                                  |
| [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink")       | 大隻佬、紋身男子、南美廠家                                 |                                  |
| [楚漢驕雄](../Page/楚漢驕雄.md "wikilink")                 | [英布](../Page/英布.md "wikilink")                |                                  |
| 2005年                                              | [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink")  | 蛇匪                               |
| [學警雄心](../Page/學警雄心.md "wikilink")                 | 傻佬                                            |                                  |
| [御用閒人](../Page/御用閒人.md "wikilink")                 | 宗邦                                            |                                  |
| [秀才遇著兵](../Page/秀才遇著兵.md "wikilink")               | 唐森                                            |                                  |
| [酒店風雲](../Page/酒店風雲.md "wikilink")                 | 豹哥                                            |                                  |
| [佛山贊師父](../Page/佛山贊師父.md "wikilink")               | 歐陽豹                                           |                                  |
| [我的野蠻奶奶](../Page/我的野蠻奶奶.md "wikilink")             | 虎哥                                            |                                  |
| [翻新大少](../Page/翻新大少.md "wikilink")                 | 茶餐廳老闆                                         |                                  |
| [人間蒸發](../Page/人間蒸發_\(電視劇\).md "wikilink")         | 何大威                                           |                                  |
| 2006年                                              | [愛情全保](../Page/愛情全保.md "wikilink")            | 七叔                               |
| [高朋滿座](../Page/高朋滿座.md "wikilink")                 | 蔡榮                                            |                                  |
| [鐵血保鏢](../Page/鐵血保鏢.md "wikilink")                 | 大將                                            |                                  |
| [天幕下的戀人](../Page/天幕下的戀人.md "wikilink")             | 權單佑                                           |                                  |
| [肥田囍事](../Page/肥田囍事.md "wikilink")                 | 大漢                                            |                                  |
| 2007年                                              | [凶城計中計](../Page/凶城計中計.md "wikilink")          | 趙霸山                              |
| [強劍](../Page/強劍.md "wikilink")                     | 華山派掌門                                         |                                  |
| [奸人堅](../Page/奸人堅.md "wikilink")                   | 趙猛丁                                           |                                  |
| [通天幹探](../Page/通天幹探.md "wikilink")                 | 洪坤                                            |                                  |
| [同事三分親](../Page/同事三分親.md "wikilink")               | 保安主任                                          |                                  |
| 2008年                                              | [最美麗的第七天](../Page/最美麗的第七天.md "wikilink")      | 鄧來喜                              |
| [原來愛上賊](../Page/原來愛上賊.md "wikilink")               | 大口勇                                           |                                  |
| [師奶股神](../Page/師奶股神.md "wikilink")                 | 彭可暢手下                                         |                                  |
| [與敵同行](../Page/與敵同行.md "wikilink")                 | 李秋瑞                                           |                                  |
| 2009年                                              | [幕后大老爷](../Page/幕后大老爷.md "wikilink")          | 商人                               |
| [ID精英](../Page/ID精英.md "wikilink")                 | 地盤黑工判頭                                        |                                  |
| [老婆大人II](../Page/老婆大人II.md "wikilink")             | 陳樹根                                           |                                  |
| [碧血鹽梟](../Page/碧血鹽梟.md "wikilink")                 | 小沅父親                                          |                                  |
| [烈火雄心3](../Page/烈火雄心3.md "wikilink")               | 葉一堅（堅叔）                                       |                                  |
| [有營煮婦](../Page/有營煮婦.md "wikilink")                 | 武有友                                           |                                  |
| [蔡鍔與小鳳仙](../Page/蔡鍔與小鳳仙.md "wikilink")             | [冯国璋](../Page/冯国璋.md "wikilink")              |                                  |
| 2010年                                              | [秋香怒點唐伯虎](../Page/秋香怒點唐伯虎.md "wikilink")      | 張濟川                              |
| [鐵馬尋橋](../Page/鐵馬尋橋.md "wikilink")                 | 羅東                                            |                                  |
| [飛女正傳](../Page/飛女正傳.md "wikilink")                 | 鄭寶榮                                           |                                  |
| [談情說案](../Page/談情說案.md "wikilink")                 | 謝天威                                           |                                  |
| [摘星之旅](../Page/摘星之旅.md "wikilink")                 | 富豪                                            |                                  |
| [刑警](../Page/刑警_\(無綫電視劇\).md "wikilink")           | 雷堅                                            |                                  |
| 2011年                                              | [依家有喜](../Page/依家有喜.md "wikilink")            | 石地棠                              |
| [Only You 只有您](../Page/Only_You_只有您.md "wikilink") | 夏江                                            |                                  |
| [女拳](../Page/女拳.md "wikilink")                     | 爛命昆                                           |                                  |
| [誰家灶頭無煙火](../Page/誰家灶頭無煙火.md "wikilink")           | 棠                                             |                                  |
| [洪武三十二](../Page/洪武三十二.md "wikilink")               | 侯順懷                                           |                                  |
| [怒火街頭](../Page/怒火街頭.md "wikilink")                 | 收數佬                                           |                                  |
| [真相](../Page/真相_\(無綫電視劇集\).md "wikilink")          | 林就（就哥）                                        |                                  |
| [潛行狙擊](../Page/潛行狙擊.md "wikilink")                 | 羅勝                                            |                                  |
| [九江十二坊](../Page/九江十二坊.md "wikilink")               | 人販子                                           |                                  |
| [萬凰之王](../Page/萬凰之王.md "wikilink")                 | 倪公公                                           |                                  |
| 2012年                                              | [缺宅男女](../Page/缺宅男女.md "wikilink")            | 黑社會大佬                            |
| [愛·回家 (第一輯)](../Page/愛·回家_\(第一輯\).md "wikilink")   | 雷武                                            |                                  |
| [心戰](../Page/心戰_\(電視劇\).md "wikilink")             | 章建國                                           |                                  |
| [護花危情](../Page/護花危情.md "wikilink")                 | 高佬輝                                           |                                  |
| [回到三國](../Page/回到三國.md "wikilink")                 | 公孫武                                           |                                  |
| [怒火街頭2](../Page/怒火街頭2.md "wikilink")               | 范達晉                                           |                                  |
| [巴不得媽媽...](../Page/巴不得媽媽....md "wikilink")         | 黃生                                            |                                  |
| [造王者](../Page/造王者_\(電視劇\).md "wikilink")           | 柱父                                            |                                  |
| [天梯](../Page/天梯_\(電視劇\).md "wikilink")             | 胡先生                                           |                                  |
| [雷霆掃毒](../Page/雷霆掃毒.md "wikilink")                 | 九紋龍                                           |                                  |
| [我外母唔係人](../Page/我外母唔係人.md "wikilink")             | 四大天王                                          |                                  |
| [法網狙擊](../Page/法網狙擊.md "wikilink")                 | 兒子                                            |                                  |
| 2013年                                              | [情越海岸線](../Page/情越海岸線.md "wikilink")          | 蝦叔                               |
| [好心作怪](../Page/好心作怪.md "wikilink")                 | 水哥                                            |                                  |
| [衝上雲霄II](../Page/衝上雲霄II.md "wikilink")             | 安以泰之父                                         |                                  |
| [神鎗狙擊](../Page/神鎗狙擊.md "wikilink")                 | 莊家                                            |                                  |
| [巨輪](../Page/巨輪.md "wikilink")                     | 郭丙榮                                           |                                  |
| [My盛Lady](../Page/My盛Lady.md "wikilink")           | 夏添                                            |                                  |
| 2014年                                              | [食為奴](../Page/食為奴.md "wikilink")              | [索額圖](../Page/索額圖.md "wikilink") |
| [守業者](../Page/守業者.md "wikilink")                   | 阿超                                            |                                  |
| [寒山潛龍](../Page/寒山潛龍.md "wikilink")                 | 宋國朝臣                                          |                                  |
| [再戰明天](../Page/再戰明天.md "wikilink")                 | 潘大成                                           |                                  |
| [飛虎II](../Page/飛虎II.md "wikilink")                 | 高兆倫                                           |                                  |
| [醋娘子](../Page/醋娘子.md "wikilink")                   | 冷兵兵                                           |                                  |
| 2015年                                              | [華麗轉身](../Page/華麗轉身.md "wikilink")            | 高官                               |
| [潮拜武當](../Page/潮拜武當.md "wikilink")                 | 喪波                                            |                                  |
| [鬼同你OT](../Page/鬼同你OT.md "wikilink")               | Bubble哥                                       |                                  |
| [樓奴](../Page/樓奴_\(電視劇\).md "wikilink")             | 高大威                                           |                                  |
| [收規華](../Page/收規華.md "wikilink")                   | 權                                             |                                  |
| [無雙譜](../Page/無雙譜_\(2015年電視劇\).md "wikilink")      | 閻王                                            |                                  |
| 2016年                                              | [警犬巴打](../Page/警犬巴打.md "wikilink")            | 少女父親                             |
| [EU超時任務](../Page/EU超時任務.md "wikilink")             | 辣                                             |                                  |
| [末代御醫](../Page/末代御醫.md "wikilink")                 | [榮祿](../Page/榮祿.md "wikilink")                |                                  |
| [純熟意外](../Page/純熟意外.md "wikilink")                 | 錢凱豐                                           |                                  |
| [完美叛侶](../Page/完美叛侶.md "wikilink")                 | 鄭武                                            |                                  |
| [城寨英雄](../Page/城寨英雄.md "wikilink")                 | 進興幫大佬                                         |                                  |
| [幕後玩家](../Page/幕後玩家.md "wikilink")                 | 榮駒                                            |                                  |
| [巾帼枭雄之谍血长天](../Page/巾帼枭雄之谍血长天.md "wikilink")       | 三井司令                                          |                                  |
| 2017年                                              | [財神駕到](../Page/財神駕到.md "wikilink")            | 武德輝                              |
| [愛·回家之開心速遞](../Page/愛·回家之開心速遞.md "wikilink")       | 池富                                            |                                  |
| [同盟](../Page/同盟.md "wikilink")                     | 戴麒忠                                           |                                  |
| [燦爛的外母](../Page/燦爛的外母.md "wikilink")               | 周文豹                                           |                                  |
| [降魔的](../Page/降魔的.md "wikilink")                   | 佳爺                                            |                                  |
| 2018年                                              | [翻生武林](../Page/翻生武林.md "wikilink")            | 魯公公                              |
| [逆緣](../Page/逆緣.md "wikilink")                     | 福                                             |                                  |
| [宮心計2深宮計](../Page/宮心計2深宮計.md "wikilink")           | 盧子奇                                           |                                  |
| [天命](../Page/天命_\(無綫電視劇\).md "wikilink")           | 富察氏明亮                                         |                                  |
| 未播映                                                | [解決師](../Page/解決師.md "wikilink")              |                                  |

### 電視節目

  - 2017年 [最緊要好玩](../Page/最緊要好玩.md "wikilink")

## 電影

  - 2017年：[追龍](../Page/追龍_\(電影\).md "wikilink")
  - 2016年：[超能太監2之黃金右手](../Page/超能太監2之黃金右手.md "wikilink")
  - 2014年：[鴨王](../Page/鴨王.md "wikilink")
  - 2014年：[天師鬥殭屍](../Page/天師鬥殭屍.md "wikilink")
  - 2014年：[澳门風雲](../Page/賭城風雲_\(香港電影\).md "wikilink")
  - 2009年：[家有喜事2009](../Page/家有喜事2009.md "wikilink")
  - 2003年：[清理門戶](../Page/清理門戶.md "wikilink")
  - 2003年：[陰陽路十五之客似魂來](../Page/陰陽路十五之客似魂來.md "wikilink")
  - 2001年：[飛哥傳奇](../Page/飛哥傳奇.md "wikilink")
  - 2000年：[鐵男本色](../Page/鐵男本色.md "wikilink")
  - 2000年：[愛殺2000](../Page/愛殺2000.md "wikilink")
  - 2000年：[野獸童黨](../Page/野獸童黨.md "wikilink")
  - 1999年：[冷暖心聲冷暖情](../Page/冷暖心聲冷暖情.md "wikilink")
  - 1999年：[上帝之手](../Page/上帝之手.md "wikilink")
  - 1999年：[四人幫之錢唔夠洗](../Page/四人幫之錢唔夠洗.md "wikilink")
  - 1998年：[新古惑仔之少年激鬥篇](../Page/新古惑仔之少年激鬥篇.md "wikilink")
  - 1998年：[邪教檔案之末日風暴](../Page/邪教檔案之末日風暴.md "wikilink")
  - 1998年：[玉蒲團III官人我要](../Page/玉蒲團III官人我要.md "wikilink")
  - 1997年：[至激殺人犯](../Page/至激殺人犯.md "wikilink")
  - 1997年：[精裝難兄難弟](../Page/精裝難兄難弟.md "wikilink")
  - 1997年：[愛上100%英雄](../Page/愛上100%英雄.md "wikilink")
  - 1996年：[運財五福星](../Page/運財五福星.md "wikilink")
  - 1996年：[三個受傷的警察](../Page/三個受傷的警察.md "wikilink")
  - 1996年：[孟波](../Page/孟波.md "wikilink")
  - 1995年：[城市炸彈](../Page/城市炸彈.md "wikilink")
  - 1995年：[特警急先鋒](../Page/特警急先鋒.md "wikilink")
  - 1995年：[重案實錄污點証人](../Page/重案實錄污點証人.md "wikilink")
  - 1995年：[月黑風高](../Page/月黑風高.md "wikilink")
  - 1994年：[重案實錄Ｏ記](../Page/重案實錄Ｏ記.md "wikilink")
  - 1994年：[屯門色魔](../Page/屯門色魔.md "wikilink")
  - 1994年：[初生之犢](../Page/初生之犢.md "wikilink")
  - 1992年：[伴我縱橫](../Page/伴我縱橫.md "wikilink")
  - 1992年：[現代應召女郎](../Page/現代應召女郎.md "wikilink")
  - 1992年：[積奇瑪莉](../Page/積奇瑪莉.md "wikilink")
  - 1991年：[烈火危情](../Page/烈火危情.md "wikilink")
  - 1991年：[曝光人物](../Page/曝光人物.md "wikilink")
  - 1991年：[龍的傳人](../Page/龍的傳人.md "wikilink")
  - 1991年：[五億探長雷洛傳](../Page/五億探長雷洛傳.md "wikilink")
  - 1990年：[棄卒](../Page/棄卒.md "wikilink")
  - 1990年：[最佳賊拍檔](../Page/最佳賊拍檔.md "wikilink")
  - 1990年：[唯我獨尊](../Page/唯我獨尊.md "wikilink")
  - 1990年：[都市煞星](../Page/都市煞星.md "wikilink")
  - 1990年：[鐵血騎警II朋黨](../Page/鐵血騎警II朋黨.md "wikilink")
  - 1990年：[風雨同路](../Page/風雨同路.md "wikilink")
  - 1990年：[摩登襯家](../Page/摩登襯家.md "wikilink")
  - 1990年：[血在風上](../Page/血在風上.md "wikilink")
  - 1989年：[賭神](../Page/賭神.md "wikilink")
  - 1989年：[義膽群英](../Page/義膽群英.md "wikilink")
  - 1989年：[我要富貴](../Page/我要富貴.md "wikilink")
  - 1989年：[專釣大鱷](../Page/專釣大鱷.md "wikilink")
  - 1989年：[沖天小子](../Page/沖天小子.md "wikilink")
  - 1989年：[喋血雙雄](../Page/喋血雙雄.md "wikilink")
  - 1989年：[花心夢裡人](../Page/花心夢裡人.md "wikilink")
  - 1989年：[打工狂想曲](../Page/打工狂想曲.md "wikilink")
  - 1988年：[猛鬼佛跳牆](../Page/猛鬼佛跳牆.md "wikilink")
  - 1988年：[霹靂先鋒](../Page/霹靂先鋒.md "wikilink")
  - 1988年：[義膽紅唇](../Page/義膽紅唇.md "wikilink")
  - 1986年：[皇家飯](../Page/皇家飯.md "wikilink")
  - 1986年：[堂口故事](../Page/堂口故事.md "wikilink")
  - 1985年：[流氓公僕](../Page/流氓公僕.md "wikilink")

## 參考

## 外部連結

  - [綠葉演員一覽](https://web.archive.org/web/20070929204717/http://www.taohua-dao.com/dispbbs.asp?boardID=53&ID=47613&page=7)
  - [男藝人](http://hi.baidu.com/220681198/blog/item/2cb8830f073de8ecab6457ba.html)
  - [【55个同一演员】2000倚天屠龙记和2004楚汉骄雄](https://web.archive.org/web/20070626155434/http://shenzhifeng.blog.china.com/200703/224333.html)
  - [facebook
    專頁](https://www.facebook.com/pages/王俊棠/48680825052)／[新浪微博](http://www.weibo.com/u/2063763790)

[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:香港飲食界人士](../Category/香港飲食界人士.md "wikilink")
[chun](../Category/王姓.md "wikilink")
[Category:香港男性模特兒](../Category/香港男性模特兒.md "wikilink")
[Category:香港航海學校校友](../Category/香港航海學校校友.md "wikilink")

1.  [名人戰商界——王俊棠7位數賣點心 慶幸搵到好拍檔
    頭條日報 2011年10月28日](http://news.hkheadline.com/dailynews/headline_news_detail_columnist.asp?id=166548&section_name=wtt&kw=107)
2.  [【獨家專訪】王俊棠兒子入行無壓力　頂嘴當溝通又愛又恨](http://bka.mpweekly.com/interview/%E5%85%A9%E4%BB%A3%E4%B9%8B%E9%96%93/20171125-91925)