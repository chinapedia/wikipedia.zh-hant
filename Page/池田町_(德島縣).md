三好市 | 都道府縣=德島縣 | 支廳= | 郡=三好郡 | 郡連結= | 編號=36483-5 | 面積=167.8 | 邊界未定= |
人口=15,960 | 統計時間=2005年10月1日 |
自治體=[三好町](../Page/三好町.md "wikilink")、、、[西祖谷山村](../Page/西祖谷山村.md "wikilink")
**[愛媛縣](../Page/愛媛縣.md "wikilink")**：[四國中央市](../Page/四國中央市.md "wikilink")
**[香川縣](../Page/香川縣.md "wikilink")**：[觀音寺市](../Page/觀音寺市.md "wikilink")、[三豐市](../Page/三豐市.md "wikilink")
| 樹=[櫻](../Page/櫻.md "wikilink") | 花=[鷺草](../Page/鷺草.md "wikilink") |
鳥=[鶯](../Page/鶯.md "wikilink") | 其他象徵物= | 其他象徵= | 郵遞區號=778-8501 |
所在地=三好郡池田町シンマチ1500-2 | 電話號碼=883-72-7641 |
外部連結=<http://www.awaikeda.jp/> | 經度= | 緯度= | 地圖= }}
**池田町**（）是過去位于[德島縣西北部的一個](../Page/德島縣.md "wikilink")[町](../Page/町.md "wikilink")，已於2006年3月1日與[三好郡内](../Page/三好郡.md "wikilink")5町村[合併為](../Page/市町村合併.md "wikilink")[三好市](../Page/三好市_\(德島縣\).md "wikilink")。

## 歷史

在[江戶時代由於周邊山區生產大量](../Page/江戶時代.md "wikilink")[菸草](../Page/菸草.md "wikilink")，因此位於河谷的此地變成為菸草的集散地，並成為主要的商業發展依據，也因此此地成為[三好郡的政治經濟中心](../Page/三好郡.md "wikilink")，並且駐有[德島藩的武士負責德島藩與鄰近其他地區的邊界警衛](../Page/德島藩.md "wikilink")。

### 年表

  - 1889年10月1日：實施[町村制](../Page/町村制.md "wikilink")，池田町最後的轄區在當時分屬：[三好郡池田村](../Page/三好郡.md "wikilink")、箸藏村、佐馬地村、三繩村。\[1\]
  - 1905年10月1日：池田村改制為**池田町**。
  - 1956年9月30日：箸藏村被[併入池田町](../Page/市町村合併.md "wikilink")。
  - 1959年3月31日：池田町、三繩村、佐馬地村合併為新設置的池田町。
  - 2006年3月1日：池田町與[三野町](../Page/三野町_\(德島縣\).md "wikilink")、[山城町](../Page/山城町_\(德島縣\).md "wikilink")、[井川町](../Page/井川町_\(德島縣\).md "wikilink")、[東祖谷山村](../Page/東祖谷山村.md "wikilink")、[西祖谷山村合併為](../Page/西祖谷山村.md "wikilink")**三好市**。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年10月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>三野村</p></td>
<td><p>1924年1月26日<br />
三野町</p></td>
<td><p>2006年3月1日<br />
合併為三好市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>箸藏村</p></td>
<td><p>1956年9月30日<br />
併入池田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>池田村</p></td>
<td><p>1905年10月1日<br />
池田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佐馬地村</p></td>
<td><p>1959年3月31日<br />
併入池田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三繩村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三名村</p></td>
<td><p>1956年9月30日<br />
併入山城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山城谷村</p></td>
<td><p>1956年9月30日<br />
山城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>井川村</p></td>
<td><p>1907年11月1日<br />
辻町</p></td>
<td><p>1959年4月1日<br />
合併為井川町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>井內谷村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東祖谷山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西祖谷山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [土讃線](../Page/土讃線.md "wikilink")：（[香川縣](../Page/香川縣.md "wikilink")[三豐市](../Page/三豐市.md "wikilink")）
        - [坪尻車站](../Page/坪尻車站.md "wikilink") -
        [箸藏車站](../Page/箸藏車站.md "wikilink") -
        （[三好町](../Page/三好町_\(德島縣\).md "wikilink")） -
        （[井川町](../Page/井川町_\(德島縣\).md "wikilink")） -
        [阿波池田車站](../Page/阿波池田車站.md "wikilink") -
        [三繩車站](../Page/三繩車站.md "wikilink") -
        （[山城町](../Page/山城町_\(德島縣\).md "wikilink")）

|                                                                                                                                                |                                                                                                        |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| [Tsuboriji-Station20100525-02.jpg](https://zh.wikipedia.org/wiki/File:Tsuboriji-Station20100525-02.jpg "fig:Tsuboriji-Station20100525-02.jpg") | [JRS_minawa_sta.jpg](https://zh.wikipedia.org/wiki/File:JRS_minawa_sta.jpg "fig:JRS_minawa_sta.jpg") |

### 道路

  - 高速道路

<!-- end list -->

  - [德島自動車道](../Page/德島自動車道.md "wikilink")：[井川池田交流道](../Page/井川池田交流道.md "wikilink")（位於與井川町的邊界）
    - [池田休息區](../Page/池田休息區.md "wikilink") -
    （[愛媛縣](../Page/愛媛縣.md "wikilink")[四國中央市](../Page/四國中央市.md "wikilink")）

## 觀光資源

[Iya_Valley_03.jpg](https://zh.wikipedia.org/wiki/File:Iya_Valley_03.jpg "fig:Iya_Valley_03.jpg")

  - [祖谷溪](../Page/祖谷溪.md "wikilink")：被列入[日本清流百選](../Page/日本清流百選.md "wikilink")、德島88景。景。
  - [祖谷溫泉](../Page/祖谷溫泉.md "wikilink")
  - [黑澤濕原](../Page/黑澤濕原.md "wikilink")。
  - [池田城遺址](../Page/池田城_\(阿波國\).md "wikilink")
  - [白地城遺址](../Page/白地城.md "wikilink")

## 教育

  - 高等學校

<!-- end list -->

  - [德島縣立池田高等學校](../Page/德島縣立池田高等學校.md "wikilink")
  - [德島縣立三好高等學校](../Page/德島縣立三好高等學校.md "wikilink")

<!-- end list -->

  - 特殊學校

<!-- end list -->

  - [德島縣立國府支援學校池田分校](../Page/德島縣立國府支援學校.md "wikilink")

## 姊妹城市

### 日本

  - [池田町](../Page/池田町_\(北海道\).md "wikilink")（[北海道](../Page/北海道.md "wikilink")[中川郡](../Page/中川郡_\(十勝綜合振興局\).md "wikilink")）
  - [池田町](../Page/池田町_\(福井縣\).md "wikilink")（[福井縣](../Page/福井縣.md "wikilink")[今立郡](../Page/今立郡.md "wikilink")）

## 本地出身之名人

  - [三好長慶](../Page/三好長慶.md "wikilink")：[戰國時代](../Page/戰國時代_\(日本\).md "wikilink")[大名](../Page/大名.md "wikilink")。
  - [竹葉多重子](../Page/竹葉多重子.md "wikilink")：飛靶射擊選手。
  - [今井ゆうぞう](../Page/今井ゆうぞう.md "wikilink")：歌手。
  - [山口俊一](../Page/山口俊一.md "wikilink")：眾議院議員。

## 參考資料

## 相關條目

  - [池田町](../Page/池田町.md "wikilink")

[Category:三好市 (德島縣)](../Category/三好市_\(德島縣\).md "wikilink")
[Category:三好郡](../Category/三好郡.md "wikilink")

1.
2.