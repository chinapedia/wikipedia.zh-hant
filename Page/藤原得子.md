**藤原得子**（永久五年（1117年）-永历元年（1160年）11月），為中納言[藤原長實之女](../Page/藤原長實.md "wikilink")，第74代[天皇](../Page/天皇.md "wikilink")[鳥羽天皇](../Page/鳥羽天皇.md "wikilink")[皇后](../Page/皇后.md "wikilink")，藤原得子姿色美豔，以美貌获得了[鸟羽天皇的宠信](../Page/鸟羽天皇.md "wikilink")，生下一子三女[近衛天皇](../Page/近衛天皇.md "wikilink")、[叡子內親王](../Page/叡子內親王.md "wikilink")、[暲子內親王](../Page/暲子內親王.md "wikilink")、[姝子內親王](../Page/姝子內親王.md "wikilink")。

## 入內

[鳥羽天皇於](../Page/鳥羽天皇.md "wikilink")[保安四年出家成為](../Page/保安.md "wikilink")[法皇](../Page/法皇.md "wikilink")，稱**鳥羽法皇**，以第一皇子顯仁親王繼位（[崇德天皇](../Page/崇德天皇.md "wikilink")），當時[鳥羽天皇祖父](../Page/鳥羽天皇.md "wikilink")—[白河法皇尚在](../Page/白河天皇.md "wikilink")，且朝中仍以[白河法皇實行](../Page/白河天皇.md "wikilink")[院政](../Page/院政.md "wikilink")，為了有所區別，將[白河法皇稱為](../Page/白河天皇.md "wikilink")**本院**，而[鳥羽法皇稱](../Page/鳥羽天皇.md "wikilink")**新院**。[保延二年](../Page/保延.md "wikilink")，[鳥羽法皇將得子納為妾侍](../Page/鳥羽天皇.md "wikilink")，並敘從三位，之後藤原得子生下[鳥羽法皇出家成為](../Page/鳥羽天皇.md "wikilink")[上院後的首位](../Page/上院.md "wikilink")[皇女](../Page/皇女.md "wikilink")—[叡子內親王](../Page/叡子內親王.md "wikilink")，並於同年宣下為[內親王](../Page/內親王.md "wikilink")，隔年[保延三年](../Page/保延.md "wikilink")，又將[叡子內親王](../Page/叡子內親王.md "wikilink")**準三宮宣下**，還加封額外的1000封戶賞賜。

[保延五年](../Page/保延.md "wikilink")，得子宣下為[女御](../Page/女御.md "wikilink")，[永治元年](../Page/永治.md "wikilink")**準三宮宣下**，此時得子逐漸取代在她之前兩位受寵的[璋子中宮和](../Page/藤原璋子.md "wikilink")[泰子皇后](../Page/藤原泰子.md "wikilink")，不久，得子和[鳥羽法皇唯一的皇子](../Page/鳥羽天皇.md "wikilink")—**體仁親王**即位為第76代[天皇](../Page/天皇.md "wikilink")[近衛天皇後](../Page/近衛天皇.md "wikilink")，得子也被[鳥羽法皇](../Page/鳥羽天皇.md "wikilink")**皇后宣下**，成為[鳥羽法皇第二位皇后](../Page/鳥羽天皇.md "wikilink")。[近衛天皇朝](../Page/近衛天皇.md "wikilink")[久安五年](../Page/久安.md "wikilink")，以[國母的身份得到](../Page/國母.md "wikilink")[女院宣下](../Page/女院.md "wikilink")，號**美福門院**。

## 美福門院

藤原得子和[鳥羽法皇的獨子](../Page/鳥羽天皇.md "wikilink")[近衛天皇](../Page/近衛天皇.md "wikilink")，以2歲之沖齡即位後曾有段時間相當虛弱。美福門院得子以為是[崇德天皇和其支持者](../Page/崇德天皇.md "wikilink")—[藤原賴長詛咒的結果](../Page/藤原賴長.md "wikilink")，因此在心中對兩人大感不滿。近衛天皇不久即以19歲的英齡崩御，而[近衛天皇生前沒有留下子嗣](../Page/近衛天皇.md "wikilink")，當時所有近支皇嗣中，除去得子和[鳥羽法皇的親生女兒](../Page/鳥羽天皇.md "wikilink")—[暲子內親王](../Page/暲子內親王.md "wikilink")\[1\]有被考慮過繼位的可能性之外，另有兩位皇子：[崇德上皇之子](../Page/崇德天皇.md "wikilink")**[重仁親王](../Page/重仁親王.md "wikilink")**\[2\]，和[崇德上皇同母弟的](../Page/崇德天皇.md "wikilink")[雅仁親王之子](../Page/後白河天皇.md "wikilink")—也就是[崇德上皇的姪子](../Page/崇德天皇.md "wikilink")**[守仁親王](../Page/二条天皇.md "wikilink")**，是最有資格的皇位候選人。而[守仁親王自幼喪母](../Page/二条天皇.md "wikilink")，就是由美福門院撫養長大的，理當是最佳人選，但由於[守仁親王之父](../Page/二条天皇.md "wikilink")[雅仁親王尚在](../Page/後白河天皇.md "wikilink")，若以[守仁親王繼位](../Page/二条天皇.md "wikilink")，那[雅仁親王的地位會變的極為尷尬](../Page/後白河天皇.md "wikilink")，因此最終仍以有**遊藝皇子**之稱的[雅仁親王繼位為第](../Page/後白河天皇.md "wikilink")77代[天皇](../Page/天皇.md "wikilink")—[後白河天皇](../Page/後白河天皇.md "wikilink")。也因為[後白河天皇的繼位](../Page/後白河天皇.md "wikilink")，是美福門院在背後一守促成的，因此[後白河天皇對美福門院以義母之禮相待之](../Page/後白河天皇.md "wikilink")。

[後白河天皇朝](../Page/後白河天皇.md "wikilink")[保元元年](../Page/保元.md "wikilink")，[鳥羽法皇病重](../Page/鳥羽天皇.md "wikilink")，美福門院便剃髮出家，法名**真性定**，以替[鳥羽法皇祈福](../Page/鳥羽天皇.md "wikilink")。雖是如此，但[鳥羽法皇的病情依然嚴重](../Page/鳥羽天皇.md "wikilink")，自知生命將盡的[鳥羽法皇](../Page/鳥羽天皇.md "wikilink")，將朝政和最寵愛的親生女兒—[暲子內親王一同托付於美福門院後](../Page/暲子內親王.md "wikilink")，不久即崩逝。

[後白河天皇即位三年後](../Page/後白河天皇.md "wikilink")，匆匆讓位於兒子**[守仁親王](../Page/二条天皇.md "wikilink")**即位成為第78代[天皇](../Page/天皇.md "wikilink")—[二条天皇](../Page/二条天皇.md "wikilink")。[二条天皇朝](../Page/二条天皇.md "wikilink")[永曆元年](../Page/永曆.md "wikilink")[十一月](../Page/十一月.md "wikilink")，美福門院藤原得子在**押小路殿**以四十四歲之齡崩逝。她被後世歷史學家認為是[玉藻前的原型](../Page/玉藻前.md "wikilink")。

## 參看

  - [暲子內親王](../Page/暲子內親王.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

[Category:平安時代皇后](../Category/平安時代皇后.md "wikilink")
[Category:末茂流](../Category/末茂流.md "wikilink")
[Category:女院](../Category/女院.md "wikilink")
[Category:1117年出生](../Category/1117年出生.md "wikilink")
[Category:1160年逝世](../Category/1160年逝世.md "wikilink")

1.  由於[暲子內親王是女兒身](../Page/暲子內親王.md "wikilink")，不由近支皇子繼位而以內親王繼位，世間難以認同，因此最後決定放棄由暲子內親王繼位的打算。
2.  但由於美福門院的怨恨，便勸[鳥羽法皇改立](../Page/鳥羽天皇.md "wikilink")[崇德上皇同母弟的](../Page/崇德天皇.md "wikilink")[雅仁親王之子](../Page/後白河天皇.md "wikilink")—也就是[崇德上皇的姪子](../Page/崇德天皇.md "wikilink")**[守仁親王](../Page/二条天皇.md "wikilink")**為帝。