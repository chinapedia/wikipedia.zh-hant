[Chen_Shaobai.jpg](https://zh.wikipedia.org/wiki/File:Chen_Shaobai.jpg "fig:Chen_Shaobai.jpg")

**陈少白**（）原名**闻绍**，后更名**白**，字**少白**，[以字行](../Page/以字行.md "wikilink")，在[日本期间曾用名](../Page/日本.md "wikilink")**陈璧**、**服部次郎**，[廣東](../Page/廣東.md "wikilink")[新會](../Page/新會.md "wikilink")[外海南華里人](../Page/外海.md "wikilink")，清末革命家，與[孫中山](../Page/孫中山.md "wikilink")、[尢列和](../Page/尢列.md "wikilink")[楊鶴齡同被稱为](../Page/楊鶴齡.md "wikilink")「[四大寇](../Page/四大寇.md "wikilink")」。

## 生平

[Si_Da_Kou.jpg](https://zh.wikipedia.org/wiki/File:Si_Da_Kou.jpg "fig:Si_Da_Kou.jpg")和[關景良](../Page/關景良.md "wikilink")（1890年攝于[香港](../Page/香港.md "wikilink")）。前排左起：[楊鶴齡](../Page/楊鶴齡.md "wikilink")、[孫中山](../Page/孫中山.md "wikilink")、[陳少白](../Page/陳少白.md "wikilink")、[尢列](../Page/尢列.md "wikilink")。後立者為[關景良並不是四大寇之一](../Page/關景良.md "wikilink")。\]\]

1869年7月20日，陈少白生于一个[基督教](../Page/基督教.md "wikilink")[牧师家庭](../Page/牧师.md "wikilink")。6岁时，他入[私塾](../Page/私塾.md "wikilink")。[1888年](../Page/1888年.md "wikilink")，他入讀[广州](../Page/广州.md "wikilink")[格致书院](../Page/格致书院.md "wikilink")（即[岭南大学的前身](../Page/岭南大学_\(广州\).md "wikilink")），在读书期间，其三叔[陈麦南常带给他许多西文著作的译本](../Page/陈麦南.md "wikilink")，后来他曾对人说“革命思想，多得于季父。”1889年，他认识了[孙中山](../Page/孙中山.md "wikilink")。1890年他21岁时入讀[香港西医书院](../Page/香港西医书院.md "wikilink")，和[孙中山](../Page/孙中山.md "wikilink")[结拜兄弟](../Page/结拜兄弟.md "wikilink")。在学习期间，他与[孙中山](../Page/孙中山.md "wikilink")、[尢列和](../Page/尢列.md "wikilink")[杨鹤龄经常在](../Page/杨鹤龄.md "wikilink")[杨耀记谈论反满清革命](../Page/杨耀记.md "wikilink")，被稱为“[四大寇](../Page/四大寇.md "wikilink")”。光绪十八年（1892年）他辍学，此后帮孙中山在[澳门](../Page/澳门.md "wikilink")、[广州设医局](../Page/广州.md "wikilink")，并参加了革命活动。\[1\]

[1895年](../Page/1895年.md "wikilink")，陳少白参与组织香港[兴中会](../Page/兴中会.md "wikilink")，并筹划广州起义。起义事败后，他和[孙中山](../Page/孙中山.md "wikilink")、[郑士良一起逃到日本](../Page/郑士良.md "wikilink")，成立了[兴中会横滨分会](../Page/兴中会横滨分会.md "wikilink")。[1897年他赴](../Page/1897年.md "wikilink")[台湾设立](../Page/台湾.md "wikilink")[兴中会台北分会](../Page/兴中会台北分会.md "wikilink")。[1900年](../Page/1900年.md "wikilink")，他奉孙中山之命回香港办《[中國日報](../Page/中國日報_\(香港\).md "wikilink")》，宣传革命。在香港办报期间，他还在[毕永年的协助下加入](../Page/毕永年.md "wikilink")[哥老会](../Page/哥老会.md "wikilink")，被推举为“龙头之龙头”。其间，他合并了[三合会](../Page/三合会.md "wikilink")、[哥老会](../Page/哥老会.md "wikilink")、[兴中会三派势力](../Page/兴中会.md "wikilink")，成立了[忠和堂兴汉会](../Page/忠和堂兴汉会.md "wikilink")，推[孙中山任总会长](../Page/孙中山.md "wikilink")，以[兴中会纲领为总会纲领](../Page/兴中会.md "wikilink")。\[2\]為了宣傳革命，他還成立了“采南歌”、“振天声”、“振天声白话剧”等剧社。\[3\]1900年，陈少白在[香港策应了](../Page/香港.md "wikilink")[惠州起义](../Page/惠州起义.md "wikilink")。\[4\]

1905年[中国同盟会成立后](../Page/中国同盟会.md "wikilink")，他任[中国同盟会香港分会会长](../Page/中国同盟会香港分会.md "wikilink")。1906年，他支持香港商人[陈席儒](../Page/陈席儒.md "wikilink")、[杨西岩等人反对清政府收回](../Page/杨西岩.md "wikilink")[粤汉铁路实行官办](../Page/粤汉铁路.md "wikilink")，并任[粤路股东维持路权会顾问](../Page/粤路股东维持路权会.md "wikilink")。1910年，他任[香港工商局顾问及](../Page/香港工商局.md "wikilink")[四邑轮船公司经理](../Page/四邑轮船公司.md "wikilink")。1911年广东光复后，他任[广东军政府外交司司长](../Page/广东军政府.md "wikilink")，不久辞职，创立了[粤航公司并任总司理](../Page/粤航公司.md "wikilink")。1915年，他和[李煜堂创立了](../Page/李煜堂.md "wikilink")[上海保险公司](../Page/上海保险公司.md "wikilink")，他任主席。1921年，[孙中山任](../Page/孙中山.md "wikilink")[非常大总统后](../Page/非常大总统.md "wikilink")，陈少白任总统府顾问、大本营参议。1922年1月，他任[国立中华国民银行监督](../Page/国立中华国民银行.md "wikilink")。[六一六事变后](../Page/六一六事变.md "wikilink")，他辞职回家乡。\[5\]

1934年12月23日，他在[北平病逝](../Page/北平.md "wikilink")。

## 著作

  - 《兴中会革命史要》
  - 《兴中会革命史要别录》

## 参考文献

<div class="references-small">

<references />

</div>

[C陈](../Page/分類:中華民國大陸時期政治人物.md "wikilink")
[C陈](../Page/分類:中國革命家.md "wikilink")
[陳少白](../Page/分類:新會人.md "wikilink")
[S少](../Page/分類:陳姓.md "wikilink")

1.  [陈少白，人民网，2011年09月20日](http://politics.people.com.cn/GB/8198/203099/203103/15709430.html)

2.
3.  [张运华，五邑名人故事：陈少白创立革命剧社，江门新闻网，2006-03-22](http://www.jmnews.com.cn/c/2006/03/22/09/c_846580.shtml)

4.
5.