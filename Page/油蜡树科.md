**油蜡树科**也叫[旱霸王科](../Page/旱霸王科.md "wikilink")、[旱黄杨科](../Page/旱黄杨科.md "wikilink")、[荷荷巴科](../Page/荷荷巴科.md "wikilink")（由[墨西哥的西班牙语](../Page/墨西哥.md "wikilink")\]\]名称jojoba[音译而来的](../Page/音译.md "wikilink")）和直接由拉丁名音译的[希蒙得木科](../Page/希蒙得木科.md "wikilink")，只有1[属](../Page/属.md "wikilink")—[油蜡树属](../Page/油蜡树属.md "wikilink")（*Simmondsia*）1[种](../Page/种.md "wikilink")—[油蜡树](../Page/油蜡树.md "wikilink")（*Simmondsia
chinensis* (Link) C. K. Schneid.
），原生于[北美洲干旱地带](../Page/北美洲.md "wikilink")，种加名chinensis是命名者错误地将缩写“Calif”当成“China”造成的，实际在[中国并没有原生种](../Page/中国.md "wikilink")。目前在[云南有引进种](../Page/云南.md "wikilink")。

油蜡树是一种常绿[灌木](../Page/灌木.md "wikilink")，1-2[米高](../Page/米.md "wikilink")，单[叶](../Page/叶.md "wikilink")，对生，全缘，叶长2-4厘米，宽1.5-3
厘米，厚革质，无托叶；[花两性](../Page/花.md "wikilink")，雌雄异株，雄花小，聚成头状，雌花单生，均无[花瓣](../Page/花瓣.md "wikilink")，[花萼](../Page/花萼.md "wikilink")4-6；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，1-2厘米长，[种子可提取透明而呈浅黄色的液体蜡](../Page/种子.md "wikilink")，含量约达54％，可提炼出高级精密机械润滑油。

1981年的[克朗奎斯特分类法将其列入](../Page/克朗奎斯特分类法.md "wikilink")[大戟目](../Page/大戟目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其放在](../Page/APG_分类法.md "wikilink")[石竹目中](../Page/石竹目.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[油蜡树科](http://delta-intkey.com/angio/www/simmonds.htm)
  - [NCBI中的油蜡树科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=3997&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/油蜡树科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")