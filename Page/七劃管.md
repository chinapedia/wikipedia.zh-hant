[Seven_segment_02_Pengo.jpg](https://zh.wikipedia.org/wiki/File:Seven_segment_02_Pengo.jpg "fig:Seven_segment_02_Pengo.jpg")作顯示組件的七劃管，由本體下的十支接腳控制發光的部份。\]\]
**七劃管**（）為常用顯示數字的[電子元件](../Page/電子元件.md "wikilink")。因為藉由七個[發光二極體以不同組合來顯示數字](../Page/發光二極體.md "wikilink")，所以稱為「-{七}-劃管」、「-{七}-段数码管」、「-{七}-段顯示器」，由於所有燈管全亮時所表示的是「8」，所以又稱「8字管」、「8字顯示器」。

多数七劃管還會在右下角附加一個表示小數點的燈管，因此也称八段管。

## 構造

[7_segment_display_labeled.svg](https://zh.wikipedia.org/wiki/File:7_segment_display_labeled.svg "fig:7_segment_display_labeled.svg")
[7-segment.svg](https://zh.wikipedia.org/wiki/File:7-segment.svg "fig:7-segment.svg")
[7-segments_Indicator.gif](https://zh.wikipedia.org/wiki/File:7-segments_Indicator.gif "fig:7-segments_Indicator.gif")
一般的七劃管擁有八個[發光二極體](../Page/發光二極體.md "wikilink")（三横四纵）用以顯示[十進位](../Page/十進位.md "wikilink")[0至](../Page/0.md "wikilink")[9的](../Page/9.md "wikilink")[數字外加](../Page/數字.md "wikilink")[小数点](../Page/小数点.md "wikilink")，也可以顯示[英文字母](../Page/英文字母.md "wikilink")，包括[十六進位中的英文](../Page/十六進位.md "wikilink")
[A](../Page/A.md "wikilink") 至 [F](../Page/F.md "wikilink")（b、d
為小寫，其他為大寫）。現時大部份的七劃管會以斜體顯示。

除七劃管外，還有十四及十六劃等添加额外斜向笔划的顯示器；但由于[點陣顯示器](../Page/點陣顯示.md "wikilink")（）价格的下跌，这些“多划管”已基本上被后者取代。

## 控制

七劃管分為**共陽極**及**共陰極**，共陽極的七劃管的[正極](../Page/正極.md "wikilink")（或陽極）為八個[發光二極體的共有正極](../Page/發光二極體.md "wikilink")，其他接點為獨立發光二極體的[負極](../Page/負極.md "wikilink")（或陰極），使用者只需把正極接電，不同的負極接地就能控制七劃管顯示不同的數字。共陰極的七劃管與共陽極的只是接駁方法相反而已。

七劃管已可以特定的[集成電路控制](../Page/集成電路.md "wikilink")，-{只}-要向[集成電路輸入](../Page/集成電路.md "wikilink")4-bit的二進位數字訊號就能控制七劃管顯示；市面上更有
8421-BCD 代码直接转为七划管控制电平的 IC，方便配合[单片机使用](../Page/单片机.md "wikilink")。

## 相關條目

  - [九劃管](../Page/九劃管.md "wikilink")
  - [十四劃管](../Page/十四劃管.md "wikilink")
  - [點陣顯示](../Page/點陣顯示.md "wikilink")

[Category:電子元件](../Category/電子元件.md "wikilink")
[Category:顯示科技](../Category/顯示科技.md "wikilink")