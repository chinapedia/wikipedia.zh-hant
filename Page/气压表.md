**气压表**或稱**气压計**（英语、德语:
Barometer）是用来测量[气压的仪器](../Page/气压.md "wikilink")，在[气象学中被广泛使用](../Page/气象学.md "wikilink")。气压表有多种造型和原理。因此它是[压力表的一类](../Page/压力表.md "wikilink")。[气压记是由气压表发展出来的仪器](../Page/气压记.md "wikilink")，气压记可以用图表或电子方式记录一个地区的气压的时间性变化。眾多測量氣壓方法配合[天氣圖用於幫助查找地面](../Page/天氣圖.md "wikilink")[低壓槽](../Page/低壓槽.md "wikilink")、高壓系統和額葉界限（frontal
boundaries）。

Mercury Barometer1.jpg|標準型水銀柱氣壓計
MercuryBarometer.svg|一個簡單的[水銀氣壓表垂直水銀柱的示意圖](../Page/汞.md "wikilink")
Old-barometers.jpg|法國[巴黎](../Page/巴黎.md "wikilink")[工藝美術博物館典藏的老晴雨表](../Page/工藝美術博物館.md "wikilink")
Barometer Goethe 01.jpg|[歌德水压計](../Page/约翰·沃尔夫冈·冯·歌德.md "wikilink")
1890s Barometer.JPG|1890年代的晴雨表

## 词源

欧洲大多数语言中气压表一词被称为Barometer，这个名字是爱尔兰学者[罗伯特·波义耳引入的](../Page/罗伯特·波义耳.md "wikilink")，它来自[希腊语的báros](../Page/希腊语.md "wikilink")（重）和métron（度量衡），意思是测量空气的重量的仪器。

## 历史

### 从伽利略开始

[Galileo.arp.300pix.jpg](https://zh.wikipedia.org/wiki/File:Galileo.arp.300pix.jpg "fig:Galileo.arp.300pix.jpg")
约1635年[佛罗伦萨的工程师和钻井人被授命建造宫廷花园里巨大的灌溉装置](../Page/佛罗伦萨.md "wikilink")。他们吃惊地发现无论如何他们的抽水机无法将水提升约10米的高度。伽利略被授命来研究这个问题。伽利略在他的《关于两门新学科的谈话及数学证明》（Discorsi
e dimostrazioni matematiche）中描写了这个问题，但他逝世于1642年，未能来得及提供这个问题的解决办法。

早在伽利略1614年的笔记中就已经看得出，他当时研究过空气的重量，并确定其值为水的重量的660分之一，但他并未从中得出其它结论。当时的教条与今天的见识正好相反，当时无法设想到不是抽水机将水抽向上，而是气压将水挤向上来。当时的人认为抽水机可以抽水是因为大自然“[憎恶真空](../Page/憎恶真空.md "wikilink")”（拉丁语：horror
vacui）。

### 托里拆利发明水银气压表

[伊万奇里斯特·托里拆利继伽利略成为](../Page/伊万奇里斯特·托里拆利.md "wikilink")[托斯卡纳伯爵的宫廷物理学家](../Page/托斯卡纳.md "wikilink")，他继续伽利略的研究，并做试验来证明水是由于空气压力上升的。为了不必使用10[米高的](../Page/米.md "wikilink")[水柱](../Page/水.md "wikilink")，他使用比水的比重高13.6倍的[汞](../Page/汞.md "wikilink")（俗稱*水銀*）。他将汞灌入一个很长的玻璃管，用手指堵住一端，将玻璃管倒过来插入一个灌满汞的盆。他发现玻璃管里的水银不完全流出，而留下来的水银柱的高度总是一样的，不管他将玻璃管插入水银盆里多深这个高度始终约为76[厘米](../Page/厘米.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Baro_1.png" title="fig:Baro_1.png">Baro_1.png</a></p>
<p><a href="https://zh.wikipedia.org/wiki/File:Baro_4.png" title="fig:Baro_4.png"><code>Baro_4.png</code></a><code>管式的气压表]]</code></p></td>
</tr>
</tbody>
</table>

他由此得出结论说空气向盆的表面施加压力来抵消水银柱的重力。而且这个压力是使得抽水机可以将水抽高约10米，但无法继续抽高的原因。他还发现水银柱的高度变化，而且在坏天气到来之前它会降低。这样一来托里拆利于1643年发明了气压表。

由于开口的盆非常不易于运输气压表，因此有人设计了各种不同的气压表，比如有人将水银封入一个连在玻璃管上的通气的、装有少量水银的皮袋里。

[罗伯特·波义耳爵士将气压表的玻璃管向上弯](../Page/罗伯特·波义耳.md "wikilink")，制成了今天依然被使用的[虹吸管式的气压表](../Page/虹吸管.md "wikilink")。

法国物理学家[勒奈·笛卡尔改善了托里拆利的装置](../Page/勒奈·笛卡尔.md "wikilink")，他在玻璃管边上添加了一个纸的标记表。笛卡尔也是第一位提出高处的气压比低处的气压低的人。

### 布莱士·帕斯卡和气压

气压使得水银柱达到约76厘米的高度，但它不足以使得水银柱以上的空间也被水银占据。1640年左右在科学界关于空气是否有重量的问题是讨论最多的问题。

[布莱士·帕斯卡重复了托里拆利的试验](../Page/布莱士·帕斯卡.md "wikilink")，他与笛卡尔一样相信假如空气有重量的话水银柱在高处上升的高度会比较低。在[巴黎的一个](../Page/巴黎.md "wikilink")52米高的塔的顶上他证实了这个猜测，不过当时的试验还相当不精确。在他的一个住在[多姆山省的亲家的帮助下他于](../Page/多姆山省.md "wikilink")1648年9月19日重复了这个试验，在不同的高处他确定水银柱随着高度的确不断下降。

后来在[国际单位制中压力的单位以他命名为](../Page/国际单位制.md "wikilink")[帕斯卡](../Page/帕斯卡.md "wikilink")，一帕斯卡相当于一[牛頓每](../Page/牛頓_\(單位\).md "wikilink")[平方米](../Page/平方米.md "wikilink")。

### 马格德堡半球

1663年[奥托·冯·盖利克使用](../Page/奥托·冯·盖利克.md "wikilink")[马格德堡半球证明气压的存在](../Page/马格德堡半球.md "wikilink")。他使用两个半球，将半球内抽空后马无法将半球分开。今天的[低压舱就是按这个原理工作的](../Page/低压舱.md "wikilink")。

### 后来的发展

[Mercury_Barometer2.jpg](https://zh.wikipedia.org/wiki/File:Mercury_Barometer2.jpg "fig:Mercury_Barometer2.jpg")
直到19世纪中仪表工、眼鏡匠和钟表匠才开始工业化生产气压表，一开始主要作为科学仪器，后来也作为家用。
1675年在一次夜间运输水银气压表的过程中有人偶然发现气压表受碰撞颠簸时其玻璃管会发出蓝色的光。波义耳的一个学生研究过这个现象但未能提出令人满足的解释。但与此同时人们开始研究真空放电的现象。今天我们知道这个光是水银原子与玻璃摩擦导致的。

## 种类

### 液体气压表

液体气压表有一个含有[液体的垂直的管](../Page/液体.md "wikilink")，管的上端密封，下端浸入一个含有同样液体的容器。液体受其重量影响流入容器，在管的上端造成一个[低压区](../Page/低压.md "wikilink")。气压防止液体下流，使得它在管内保持一定的高度。

#### 水银气压表

[Barometer_MK1890.jpg](https://zh.wikipedia.org/wiki/File:Barometer_MK1890.jpg "fig:Barometer_MK1890.jpg")
最常见的液体气压表使用水银，因此被称为水银气压表。在[标准状况下其水银柱高](../Page/标准状况.md "wikilink")760毫米。由于水银和玻璃管受[温度影响而变化](../Page/温度.md "wikilink")，因此其读出的值要转化计算：
\(p = p_a \cdot (1 - 0,000182 \cdot T)\)

  - \(p\) - 实际气压
  - \(p_a\) - 指示气压
  - \(T\) - 温度（[°C](../Page/摄氏温标.md "wikilink")）
  - 水银的[热膨胀系数为](../Page/热膨胀系数.md "wikilink")0.000182
  - 更精确的测量还要顾及[纬度和高度](../Page/纬度.md "wikilink")

使用水银的原因是因为它的[比重比较高](../Page/比重.md "wikilink")，因此使用的管子不必非常长。假如使用水的话其管子要达10米长。此外水银[蒸发很小](../Page/蒸发.md "wikilink")，即使在管子上部的[真空裡水银的蒸发依然非常小](../Page/真空.md "wikilink")。

第一架水银气压表是1643年伊万奇里斯特·托里拆利发明的。他也认识到气压的变化。气压的[单位](../Page/单位.md "wikilink")[托是以他命名的](../Page/托.md "wikilink")。1托=1[毫米汞柱](../Page/毫米汞柱.md "wikilink")，相当于与133.32帕斯卡。

#### 水性氣壓計

（1805-1866）提出利用低氣壓來預測暴風雨天氣，是首次氣候預測儀器“暴風玻璃”或“歌德氣壓表”的原理。

歌德气压表也是一种液体气压表，它往往被用来作为装饰品。它由一个密封的、外形优美的主容器组成，在主容器的下方有一个小的，向上开口的管子连出来。低压时管里的液体会升高，高压时管里的液体会下降。歌德氣壓表命名來自
[約翰·沃爾夫岡·馮·歌德利用](../Page/约翰·沃尔夫冈·冯·歌德.md "wikilink")[埃萬傑利斯塔·托里拆利](../Page/埃萬傑利斯塔·托里拆利.md "wikilink")
理論自製一簡單但有效的氣壓球。許多英語系國家亦稱之於法文 *le baromètre
Liègeois*，主因是早期天氣預測玻璃裝置來自於比利時的[列日](../Page/列日.md "wikilink")。

### 盒式气压表

[Dosen-barometer.jpg](https://zh.wikipedia.org/wiki/File:Dosen-barometer.jpg "fig:Dosen-barometer.jpg")
[Barometer-schatz.jpg](https://zh.wikipedia.org/wiki/File:Barometer-schatz.jpg "fig:Barometer-schatz.jpg")
盒式气压表由一个由薄金属片组成的盒子组成，其发明人是。好的盒式气压表由到八层这样的盒子组成来提高其灵敏度。高压时盒子受挤压，低压时盒子向外膨胀，这个体积变化可以由机械部分传递到指针上。

盒式气压表的一个毛病是其受温度的影响。为了防止盒内的空气受高温影响膨胀盒内被抽真空，但盒子本身也受温度影响热胀冷缩，因此盒子要由特别的合金组成，不同的组成部分受温度影响时互相之间抵消其变形来降低温度的影响。虽然如此盒式气压表受温度影响造成测量误差。

### 风镜气压表

风镜气压表是[罗伯特·菲茨罗伊发明的](../Page/罗伯特·菲茨罗伊.md "wikilink")。它含有[樟脑在](../Page/樟脑.md "wikilink")[乙醇中的](../Page/乙醇.md "wikilink")[溶液](../Page/溶液.md "wikilink")。随气压和气温的不同它可以[结晶](../Page/结晶.md "wikilink")。高压下晶体溶解，低压下结晶，溶液浑浊。

## 用途

气压表最主要是用在气象学中，它是每个[气象站必备的仪器](../Page/气象站.md "wikilink")。由于气压随高度降低，它也可以用作[飞机里的](../Page/飞机.md "wikilink")[高度计](../Page/高度计.md "wikilink")。测量人为的高压或低压的仪器不被称为气压表，而被称为[压力表](../Page/压力表.md "wikilink")。使用气压变化来测量高度变化的仪器被称为[升降速率表](../Page/升降速率表.md "wikilink")。

在中纬度地区气压表往往被简化用来作为“天气表”，高压被视为是好天气的预兆，低压被视为坏天气的预兆。但这个用法是相当不精确的，有时坏天气到来前也会气压升高，因此这个用法是非常粗略的。

## 参见

[Category:度量儀器](../Category/度量儀器.md "wikilink")
[Category:氣象儀器和設備](../Category/氣象儀器和設備.md "wikilink")