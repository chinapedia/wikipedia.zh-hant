**王國強**(Peter Wong Kwok Keung,
)，祖籍[廣東](../Page/廣東.md "wikilink")[東莞](../Page/東莞.md "wikilink")[石排鎮](../Page/石排鎮.md "wikilink")，[香港政治人物兼註冊專業工程師](../Page/香港.md "wikilink")、[金城營造集團主席兼行政總裁](../Page/金城營造集團.md "wikilink")、[香港廣東社團總會主席](../Page/香港廣東社團總會.md "wikilink")。亦為[香港工程師學會資深會員](../Page/香港工程師學會.md "wikilink")、第十屆、十一屆[全國政協委員](../Page/全國政協.md "wikilink")、[九龍樂善堂常務總理](../Page/九龍樂善堂.md "wikilink")、[選舉委員會委員](../Page/選舉委員會.md "wikilink")、[策略發展委員會社會發展及生活質素委員會委員](../Page/策略發展委員會.md "wikilink")、[九龍城工商業聯會會長](../Page/九龍城工商業聯會.md "wikilink")。2004年至2011年獲委任為[九龍城](../Page/九龍城.md "wikilink")[區議員](../Page/區議員.md "wikilink")。[第十二屆全國政協委員](../Page/中國人民政治協商會議全國委員會香港地區委員.md "wikilink")。已婚，有兩子一女，長子王紹基，次子王紹恆，幼女王沛芝。

2013年8月，王國強與傳媒人[周融](../Page/周融.md "wikilink")、中大政治系前主任[鄭赤琰](../Page/鄭赤琰.md "wikilink")、[嶺大公共政策研究中心主任](../Page/嶺南大學.md "wikilink")[何濼生](../Page/何濼生.md "wikilink")、創新科技協會創會會長[李榮貴](../Page/李榮貴.md "wikilink")、資深金融界人士[馮家彬等人組成政治組織](../Page/馮家彬.md "wikilink")「[幫港出聲](../Page/幫港出聲.md "wikilink")」（
Silent Majority
），反對[戴耀廷的](../Page/戴耀廷.md "wikilink")[佔領中環爭取普選運動](../Page/讓愛與和平佔領中環.md "wikilink")。

## 名下馬匹

他是[香港賽馬會的會員](../Page/香港賽馬會.md "wikilink")，名下馬匹包括個人名義的金滿城、金滿載、團體名義的龍城動力、龍城快勝（[香港九龍城工商業聯會團體名下](../Page/香港九龍城工商業聯會.md "wikilink")）、金志成城、金在手（金城之友團體名下）。

## 榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink") (2005年)
  - [銀紫荊星章](../Page/銀紫荊星章.md "wikilink") (2008年)

## 外部連結

  - [金城營造集團 kumshing.com.hk](http://www.kumshing.com.hk/)

### 訪談/演講錄

  - [「蛻變:
    苦難輝煌半世紀」](http://lib-nt2.hkbu.edu.hk/hkbutube/vod.asp?bibno=b2458658)主講：王國強，主辦：[香港浸會大學發展事務處](../Page/香港浸會大學.md "wikilink")
    2010/11/12

[Category:香港東莞人](../Category/香港東莞人.md "wikilink")
[Category:東莞人](../Category/東莞人.md "wikilink")
[Category:前香港區議員](../Category/前香港區議員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:香港工程師](../Category/香港工程師.md "wikilink")
[K](../Category/王姓.md "wikilink")
[Category:太平紳士](../Category/太平紳士.md "wikilink")
[Category:獲頒授香港銀紫荊星章者](../Category/獲頒授香港銀紫荊星章者.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十二屆全國政協委員](../Category/第十二屆全國政協委員.md "wikilink")
[Category:黃大仙官立小學校友](../Category/黃大仙官立小學校友.md "wikilink")
[Category:香港浸會大學榮譽博士](../Category/香港浸會大學榮譽博士.md "wikilink")
[Category:工程師出身的商人](../Category/工程師出身的商人.md "wikilink")