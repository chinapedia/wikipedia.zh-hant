**藏緬語族**是分布於[中國西南部](../Page/中華人民共和國.md "wikilink")、[印度東北部](../Page/印度.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南等地的一組語言](../Page/越南.md "wikilink")。根據[民族语网站](../Page/民族语.md "wikilink")2009年的資料，藏缅语族共包含有435种語言\[1\]，其中主要的语言有[緬甸語](../Page/緬甸語.md "wikilink")、[藏語](../Page/藏語.md "wikilink")、[彝語](../Page/彝語.md "wikilink")、[曼尼普爾語等](../Page/曼尼普爾語.md "wikilink")。藏緬語族被歸類在漢藏語系之下，與長期被歸入[漢藏語系的另兩個語族](../Page/漢藏語系.md "wikilink")（[苗瑤語族](../Page/苗瑤語族.md "wikilink")、[壯侗語族](../Page/壯侗語族.md "wikilink")）之間的關係，一直是語言學界爭論的焦點。

藏緬語族语言的总使用人口约有6000万。[緬甸語是藏緬語族中使用人口最多的一個語言](../Page/緬甸語.md "wikilink")，大概有3200萬人使用。此外，有700萬左右的[藏人使用](../Page/藏人.md "wikilink")[藏語](../Page/藏語.md "wikilink")。

## 簡介

藏緬語族不但分布廣，而且語種多、差別大，這意味著藏緬語族源遠流長，其歷史縱深可能足與[印歐語系等量齊觀](../Page/印歐語系.md "wikilink")。藏緬語族之下到底有多少分支，由於現行分類體系不完整，對現代語言的調查又不夠全面，目前尚無定論。如果採取比較保守的分類，加上系屬不明之「[孤立語言](../Page/孤立語言.md "wikilink")」（language
isolate），如[土家語](../Page/土家語.md "wikilink")、[蘇龍語](../Page/蘇龍語.md "wikilink")、[格曼語](../Page/格曼語.md "wikilink")、[貢都語](../Page/貢都語.md "wikilink")、[達迈語](../Page/鲁索语支.md "wikilink")、[舍爾都克奔語等](../Page/舍朱奔语支.md "wikilink")，未來承認之獨立語支可能將超過二十餘個\[2\]。

藏緬語族的語言多半都沒有[文字](../Page/文字.md "wikilink")，有較長期文獻資料者僅有[藏語](../Page/藏語.md "wikilink")、[緬甸語](../Page/緬甸語.md "wikilink")、[西夏語](../Page/西夏語.md "wikilink")、戎（雷普查）語以及[彝語五種](../Page/彝語.md "wikilink")。其中藏語從7世紀起就有以[印度系統字母書寫的](../Page/印度.md "wikilink")[藏文](../Page/藏文字母.md "wikilink")，為整個漢藏語系中最古老、最精確的標音文獻記錄，對了解漢藏語系具有關鍵性的作用\[3\]。

## 藏緬語族概念簡史

大概在1850年左右，相關學者從「藏文」(起源於7世紀)和「缅文」(起源於12世紀)的相關資料中，發現到這兩種語言似乎有某種程度的關聯性。在這之後，有一些[英國學者以及英國派駐在印度和緬甸的殖民地官員](../Page/英國.md "wikilink")，也開始採用比較有系統的方式，試著對該地區比較不為人知的一些「[部落](../Page/部落.md "wikilink")」(tribal)語言進行實地的田野調查和紀錄，而發現到這些語言和藏語以及緬語這兩個具有文字傳統的語言，似乎也有某種程度的親和關係。在這些相關研究中，George
Abraham Grierson的《印度語言調查(*Linguistic Survey of
India*)》(1903-1928，其中有三卷和藏緬語系的語言有關)，是這個階段對於藏緬語言最重要的研究成果(STEDT\[4\])。

接下來，雖然有人試著要在藏語和漢語之間找尋其中的親緣關係，但是，由於相關實證資料的不足，學者並無法對原始藏緬語(Proto-Tibeto-Burman)進行擬構的工作，也因此無法產生什麼明確的結論。1930年左右，美國語言學者Robert
Shafer在[白保羅](../Page/白保羅.md "wikilink")(Paul K.
Benedict)的協助下，以在該地區工作的殖民地官員和[傳教士所編寫的一些](../Page/傳教士.md "wikilink")[字典和語言研究為基礎](../Page/字典.md "wikilink")，首次對後來被歸類為藏緬語族的這些語言進行比較有系統的研究工作，也初步將這些語言的系譜關係作了一定程度的釐清。這次研究的成果，是被稱之為《漢藏語言學(*Sino-Tibetan
Linguistics*)》(1939-1941)的三卷未出版手稿(STEDT\[5\])。

1966年，Shafer第一次正式將他的研究心得加以出版，這就是《漢藏語言介紹(*Introduction to
Sino-Tibetan*)》(見Shafer
1966)這本書。在這本書中，他不但將[泰語列入漢藏語系當中](../Page/泰語.md "wikilink")，同時也對藏緬語族的各種語言，作了相當詳盡的分類。雖然這個分類系統乍看之下十分地合理，但是，由於某些語言的原始資料並不齊備，他的某些分類其實是很有問題的(STEDT\[6\])。

同樣以這些資料為基礎，白保羅卻獲得了和Shafer不太相同的結論。在其1972年所出版的《漢藏語概論(*Sino-Tibetan: A
Conspectus*)》中(這本書的初稿完成於1941年左右)，白保羅一方面將泰語排除在漢藏語系之外，另一方面，他則將緬甸北部的[克欽語](../Page/景頗語.md "wikilink")(Kachin)視為是其他藏緬語族語言的「輻射中心」，而將[克倫語](../Page/克倫語.md "wikilink")(Karen)排除在這個中心之外。雖然白保羅的這本書還留下不少無法解決的難題，但是，目前多數的語言學者都認為，《漢藏語概論》的出版代表了漢藏語系研究的一個新紀元，也在某種程度上對藏緬語族的分類，提供了比較可信的假設(STEDT\[7\])。

## 分類

### 马蒂索夫的分类

流传最广的可能是[马蒂索夫的分类](../Page/马蒂索夫.md "wikilink")：

  - [卡默鲁珀](http://en.wikipedia.org/wiki/Kamarupa)（地理划分）
      - [库基-钦-那加](../Page/库基-钦-那加语言.md "wikilink")（地理划分）
      - [阿博尔-米里-达夫拉](../Page/塔尼語支.md "wikilink")
      - [博多-加罗](../Page/博多-科奇语支.md "wikilink")
  - 喜马拉雅（Himalayish，地理划分）
      - 大[基兰特](../Page/基兰特语支.md "wikilink")
      - [藏-金瑙尔](../Page/藏-金瑙尔语支.md "wikilink")（Tibeto-Kanauri）
  - [羌](../Page/羌语支.md "wikilink")
  - 景颇-侬-卢伊
      - [景颇](../Page/景颇语.md "wikilink")
      - [侬](../Page/侬语支.md "wikilink")
      - 卢伊
  - [倮倮-缅](../Page/缅彝语群.md "wikilink")-[纳西](../Page/纳西语.md "wikilink")
    (Lolo-Burmese)
  - [克伦](../Page/克伦语支.md "wikilink")
  - [白](../Page/白语.md "wikilink")
  - [土家语](../Page/土家语.md "wikilink")（未分类）

### 杜冠明的分类

杜冠明（Thurgood 2003）的分类如下：

  - [倮倮-缅](../Page/缅彝语群.md "wikilink")
  - [藏](../Page/藏-喜马拉雅语群.md "wikilink")（Bodic）
      - A组
          - [藏语方言](../Page/藏语方言.md "wikilink")
          - [达芒-古隆-塔卡利-玛囊巴](../Page/达芒语支.md "wikilink")
          - [Takpa](../Page/北门巴语支.md "wikilink")
      - B组
          - [雷布查语](../Page/雷布查语.md "wikilink")、[尼瓦尔语](../Page/尼瓦尔语.md "wikilink")
          - [仓洛语](../Page/仓洛语.md "wikilink")（暂定）
  - [萨尔](../Page/萨尔语群.md "wikilink")（可能包含卢伊）
  - [库基-钦-那加](../Page/库基-钦-那加语言.md "wikilink")（暂定，包含[曼尼普尔语](../Page/曼尼普尔语.md "wikilink")）
  - 戎
      - [嘉绒](../Page/嘉绒语支.md "wikilink")
      - [独龙语和相关语言](../Page/独龙语.md "wikilink")（[侬语支](../Page/侬语支.md "wikilink")）
      - [基兰特](../Page/基兰特语支.md "wikilink")
      - [西喜马拉雅](../Page/西喜马拉雅语支.md "wikilink")
      - [卡姆语](../Page/卡姆语.md "wikilink")（Kham）、[马嘉尔语](../Page/马嘉尔语.md "wikilink")、Chepang（不构成一个[子类](../Page/马嘉尔语支.md "wikilink")）
      - [羌](../Page/羌语支.md "wikilink")
  - [克伦](../Page/克伦语支.md "wikilink")
  - 其他小分支：[塔尼](../Page/塔尼語支.md "wikilink")、[舍朱奔-布贡-苏龙-利西巴](../Page/舍朱奔语支.md "wikilink")、[鲁索语支](../Page/鲁索语支.md "wikilink")、[义都-Digaru](../Page/义都-达让语支.md "wikilink")、[米教/格曼](../Page/格曼语支.md "wikilink")
  - 未分类语言：[骠语](../Page/骠语.md "wikilink")、[纳西语](../Page/纳西语.md "wikilink")、[土家语](../Page/土家语.md "wikilink")、[白语](../Page/白语.md "wikilink")

杜冠明认为[白语也可能属于](../Page/白语.md "wikilink")[汉语族](../Page/汉语族.md "wikilink")。

## 藏緬語族語言舉隅

  - [藏語](../Page/藏語.md "wikilink")、[不丹語](../Page/不丹語.md "wikilink")
  - [緬甸語](../Page/緬甸語.md "wikilink")、[彝語](../Page/彝語.md "wikilink")、[傈僳語](../Page/傈僳語.md "wikilink")、[拉祜語](../Page/拉祜語.md "wikilink")
  - [羌語](../Page/羌語.md "wikilink")
  - [嘉絨語](../Page/嘉絨語.md "wikilink")
  - [景頗語](../Page/景頗語.md "wikilink")（克欽語）
  - [白語](../Page/白語.md "wikilink")
  - [土家語](../Page/土家語.md "wikilink")
  - [曼尼普爾語](../Page/曼尼普爾語.md "wikilink")
  - [西夏語](../Page/西夏語.md "wikilink")

完整列表見[漢藏語列表\#藏緬語族](../Page/漢藏語列表#藏緬語族.md "wikilink")。

參見[中国语言列表\#藏緬語族](../Page/中国语言列表#藏緬語族.md "wikilink")。

## 注释

## 參考文献

  - Benedict, Paul K.（白保罗）*Sino-Tibetan: A Conspectus*. New York:
    Cambridge University Press, 1972.
  - Grierson, Sir George Abraham. 1903-1928. *Linguistic Survey of
    India*. Calcutta : Office of the Superintendent of Government
    Printing, India.
  - Shafer, Robert. 1966. *Introduction to Sino-Tibetan*. Wiesbaden:
    Otto Harrassowitz.
  - Graham Thurgood（杜冠明）、Randy J. LaPolla（罗仁地）编，*The Sino-Tibetan
    Languages*, Routledge, 2003.

## 外部連結

  - [中央研究院語言學研究所：藏緬語族簡介](https://web.archive.org/web/20060725233906/http://www.ling.sinica.edu.tw/field/a_tibeto.html)

[藏緬語族](../Category/藏緬語族.md "wikilink")
[Category:漢藏語系](../Category/漢藏語系.md "wikilink")

1.  [民族语](../Page/民族语.md "wikilink")：世界的语言，第16版。[语系树：汉藏，藏缅](http://www.ethnologue.com/show_family.asp?subid=232-16)。[达拉斯](../Page/达拉斯.md "wikilink")，2009年10月引用。

2.  孫天心，[藏緬語族簡介](http://www.ling.sinica.edu.tw/field/a_tibeto.html)
    。台北：中央研究院語言學研究所。无日期，引用於2005年2月2日。

3.
4.  STEDT,
    [藏缅语及其分类](http://stedt.berkeley.edu/html/STfamily.html#TBlg)。[伯克利](../Page/伯克利.md "wikilink")：[加利福尼亚大学语言学系](../Page/加利福尼亚大学.md "wikilink")，无日期，2004年11月24日引用。

5.
6.
7.