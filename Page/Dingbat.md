**Dingbats**，俗称**杂锦[字型](../Page/字型.md "wikilink")**
，本来是印刷品之中使用的装饰及图形符号。在[计算机被用来制作](../Page/计算机.md "wikilink")[印刷刊物后](../Page/印刷.md "wikilink")，印刷业界便制造了各种杂锦字型，最著名的是[Adobe的](../Page/Adobe.md "wikilink")[Zapf
Dingbats字型](../Page/Zapf_Dingbats.md "wikilink")。

[微软于](../Page/微软.md "wikilink")[Windows
3.1](../Page/Windows_3.x.md "wikilink")[操作系统之中](../Page/操作系统.md "wikilink")，提供了[Wingdings杂锦符号字型](../Page/Wingdings.md "wikilink")。而微软于1997年之后发行的操作系统，再加入了[Webdings杂锦符号字型](../Page/Webdings.md "wikilink")。

| **Zapf Dingbats 字符集** |
| --------------------- |
|                       |
| 0x                    |
| 1x                    |
| 2x                    |
| 3x                    |
| 4x                    |
| 5x                    |
| 6x                    |
| 7x                    |
| 8x                    |
| 9x                    |
| Ax                    |
| Bx                    |
| Cx                    |
| Dx                    |
| Ex                    |
| Fx                    |

[Unicode的U](../Page/Unicode.md "wikilink")+2700至U+27BF[1](http://www.unicode.org/charts/PDF/U2700.pdf)，亦收录了一些杂锦字型。

<table>
<thead>
<tr class="header">
<th><p><small>Dingbats</small><br />
<small><a href="http://www.unicode.org/charts/PDF/U2700.pdf">Unicode.org chart</a> (PDF)</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>U+</p></td>
</tr>
<tr class="even">
<td><p>2700</p></td>
</tr>
<tr class="odd">
<td><p>2710</p></td>
</tr>
<tr class="even">
<td><p>2720</p></td>
</tr>
<tr class="odd">
<td><p>2730</p></td>
</tr>
<tr class="even">
<td><p>2740</p></td>
</tr>
<tr class="odd">
<td><p>2750</p></td>
</tr>
<tr class="even">
<td><p>2760</p></td>
</tr>
<tr class="odd">
<td><p>2770</p></td>
</tr>
<tr class="even">
<td><p>2780</p></td>
</tr>
<tr class="odd">
<td><p>2790</p></td>
</tr>
<tr class="even">
<td><p>27A0</p></td>
</tr>
<tr class="odd">
<td><p>27B0</p></td>
</tr>
</tbody>
</table>

<center>

Image:ZapfDingbats_charset.png|[ZapfDingbats
BT](../Page/ZapfDingbats_BT.md "wikilink")
Image:Webdings-big.png|[Webdings](../Page/Webdings.md "wikilink")
Image:Wingdings sample.png|[Wingdings](../Page/Wingdings.md "wikilink")
Image:Wingdings 2 sample.png|[Wingdings
2](../Page/Wingdings_2.md "wikilink") Image:Wingdings 3
sample.png|[Wingdings 3](../Page/Wingdings_3.md "wikilink")

</center>

[Category:字符集](../Category/字符集.md "wikilink")