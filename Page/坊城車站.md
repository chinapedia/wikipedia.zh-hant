**坊城車站**（）是一個位於[日本](../Page/日本.md "wikilink")[奈良縣](../Page/奈良縣.md "wikilink")[橿原市東坊城町](../Page/橿原市.md "wikilink")281-2號，由[近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")（近鐵）所經營的[鐵路車站](../Page/鐵路車站.md "wikilink")。坊城車站是近鐵[南大阪線沿線的車站](../Page/南大阪線.md "wikilink")，[區間急行以下等級的列車](../Page/急行.md "wikilink")，會在本站停靠。

## 車站結構

對向式月台2面2線的車站，閘口、大堂位於地底，月台位於地面。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>南大阪線</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/橿原神宮前站.md" title="wikilink">橿原神宮前</a>、<a href="../Page/吉野站_(奈良縣).md" title="wikilink">吉野方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>上行</p></td>
<td><p>、<a href="../Page/大阪阿部野橋站.md" title="wikilink">大阪阿部野橋</a>、、<a href="../Page/河內長野站.md" title="wikilink">河內長野方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - 近畿日本鐵道

    南大阪線

      -

        急行

          -
            **通過**

        區間急行、準急、普通

          -

            （F25）－**坊城（F26）**－（F27）

[Ujou](../Category/日本鐵路車站_Bo.md "wikilink")
[Category:奈良縣鐵路車站](../Category/奈良縣鐵路車站.md "wikilink")
[Category:南大阪線車站](../Category/南大阪線車站.md "wikilink")
[Category:1929年启用的铁路车站](../Category/1929年启用的铁路车站.md "wikilink")
[Category:橿原市](../Category/橿原市.md "wikilink")