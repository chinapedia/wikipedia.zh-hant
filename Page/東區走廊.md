[Island_Eastern_Corridor_near_Sai_Wan_Ho_201803.jpg](https://zh.wikipedia.org/wiki/File:Island_Eastern_Corridor_near_Sai_Wan_Ho_201803.jpg "fig:Island_Eastern_Corridor_near_Sai_Wan_Ho_201803.jpg")
[Island_Eastern_Corridor.jpg](https://zh.wikipedia.org/wiki/File:Island_Eastern_Corridor.jpg "fig:Island_Eastern_Corridor.jpg")一帶\]\]
[Island_Eastern_Corridor_at_night_(revised).jpg](https://zh.wikipedia.org/wiki/File:Island_Eastern_Corridor_at_night_\(revised\).jpg "fig:Island_Eastern_Corridor_at_night_(revised).jpg")
**東區走廊**（；簡稱**東廊**）是[香港4號幹線的](../Page/香港4號幹線.md "wikilink")[快速公路路段](../Page/快速公路.md "wikilink")（[永泰道以北](../Page/永泰道.md "wikilink")，佔約7.9公里，其中約1公里不屬於4號幹線的範圍\[1\]），同時亦為[香港島](../Page/香港島.md "wikilink")4號幹線的起始路段，沿香港島[銅鑼灣至](../Page/銅鑼灣.md "wikilink")[杏花邨海岸興建](../Page/杏花邨.md "wikilink")，並繞過[柴灣公園西面](../Page/柴灣公園.md "wikilink")，連接[銅鑼灣及](../Page/銅鑼灣.md "wikilink")[柴灣](../Page/柴灣.md "wikilink")。全線為2至4線雙程分隔[公路](../Page/公路.md "wikilink")，主要為[高架道路](../Page/高架道路.md "wikilink")，當中由[東區海底隧道至銅鑼灣](../Page/東區海底隧道.md "wikilink")[天后的一段](../Page/天后_\(香港\).md "wikilink")4線雙程高架道路是香港首條設有8線的高架車路（另外兩條分別位於[青葵公路及](../Page/青葵公路.md "wikilink")[吐露港公路](../Page/吐露港公路.md "wikilink")）。雖然是香港島快速公路，但是車速限制為每小時70公里，比同屬於4號幹線、[干諾道西天橋](../Page/干諾道西天橋.md "wikilink")、[林士街天橋及](../Page/林士街天橋.md "wikilink")[中環灣仔繞道](../Page/中環灣仔繞道.md "wikilink")（每小時80公里）嚴格。東區走廊全長8.6公里，其中從[柴灣道](../Page/柴灣道.md "wikilink")[迴旋處至](../Page/迴旋處.md "wikilink")[中環灣仔繞道合共](../Page/中環灣仔繞道.md "wikilink")7.56公里的路段屬於4號幹線的範圍。

## 歷史

[HK_Shau_Kei_Wan_2_Taikoo_Shing_stone_marked_25-July-1985.JPG](https://zh.wikipedia.org/wiki/File:HK_Shau_Kei_Wan_2_Taikoo_Shing_stone_marked_25-July-1985.JPG "fig:HK_Shau_Kei_Wan_2_Taikoo_Shing_stone_marked_25-July-1985.JPG")
東區走廊的興建，主要是取代車流飽和的[英皇道](../Page/英皇道.md "wikilink")、[筲箕灣道及](../Page/筲箕灣道.md "wikilink")[柴灣道](../Page/柴灣道.md "wikilink")，成為連接[東區及](../Page/東區_\(香港\).md "wikilink")[香港島中心地帶的主要通道](../Page/香港島.md "wikilink")。這條公路於1968年倡議興建，1976年展開詳細設計工作\[2\]，並在1978年至1981年期間開始[填海和進行相關工程](../Page/填海.md "wikilink")，並分四階段於1981年4月、1982年5月和1986年8月興建。首期[銅鑼灣至](../Page/銅鑼灣.md "wikilink")[太古城段全長](../Page/太古城.md "wikilink")3.7公里，於1984年6月7日由[港督](../Page/港督.md "wikilink")[尤德主持開幕儀式](../Page/尤德.md "wikilink")，6月8日下午2時半通車\[3\]；二期太古城至筲箕灣段於1985年7月26日通車；筲箕灣至柴灣的柴灣段於1989年10月12日全線通車。

## 路線

[東區車道柴灣道入口.jpeg](https://zh.wikipedia.org/wiki/File:東區車道柴灣道入口.jpeg "fig:東區車道柴灣道入口.jpeg")
東區走廊全長8.6公里，由[香港海底隧道以東的](../Page/香港海底隧道.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[維園道開始](../Page/維園道.md "wikilink")，經過約1公里與[中環灣仔繞道連接後](../Page/中環灣仔繞道.md "wikilink")，便以2.1公里長的[架空道路跨過](../Page/架空道路.md "wikilink")[銅鑼灣避風塘東南角一隅](../Page/銅鑼灣避風塘.md "wikilink")，繞過並經過[炮台山及](../Page/炮台山.md "wikilink")[北角的沿岸外圍](../Page/北角.md "wikilink")，到達[鰂魚涌](../Page/鰂魚涌.md "wikilink")[東匯中心後落回地面](../Page/東匯中心.md "wikilink")，到達[太古城](../Page/太古城.md "wikilink")[交匯處與](../Page/交匯處.md "wikilink")[2號幹線起點](../Page/2號幹線.md "wikilink")／終點的[東區海底隧道連接](../Page/東區海底隧道.md "wikilink")，與[太古城及](../Page/太古城.md "wikilink")[康山交匯](../Page/康山.md "wikilink")。從[鯉景灣及](../Page/鯉景灣.md "wikilink")[太古灣道](../Page/太古灣道.md "wikilink")[上斜路段之間開始](../Page/坡道.md "wikilink")，接著的3.5公里路段再為架空道路，穿越[愛秩序灣和](../Page/愛秩序灣.md "wikilink")[筲箕灣之間](../Page/筲箕灣.md "wikilink")，並在[柴灣道架設匝道直達](../Page/柴灣道.md "wikilink")[大潭](../Page/大潭.md "wikilink")、[石澳](../Page/石澳.md "wikilink")、[鯉魚門公園及渡假村及](../Page/鯉魚門公園及渡假村.md "wikilink")[赤柱](../Page/赤柱.md "wikilink")，再經[阿公岩直至](../Page/阿公岩.md "wikilink")[香港海防博物館](../Page/香港海防博物館.md "wikilink")，然後再度回落地面到達[柴灣](../Page/柴灣.md "wikilink")。在[香港專業教育學院柴灣分校有](../Page/香港專業教育學院柴灣分校.md "wikilink")[支路連接](../Page/支路.md "wikilink")[永泰道通往](../Page/永泰道.md "wikilink")[杏花邨](../Page/杏花邨.md "wikilink")、柴灣工業區及[小西灣](../Page/小西灣.md "wikilink")。直到[港鐵](../Page/港鐵.md "wikilink")[柴灣站旁的](../Page/柴灣站.md "wikilink")[柴灣道](../Page/柴灣道.md "wikilink")[迴旋處](../Page/迴旋處.md "wikilink")，以交匯[柴灣道及](../Page/柴灣道.md "wikilink")[環翠道結束](../Page/環翠道.md "wikilink")。

紅色小巴只能行走[東區海底隧道至西灣河太康街及柴灣道連接路](../Page/東區海底隧道.md "wikilink")。

## 出口

\! colspan="4" style="border-bottom: 5px solid \#B58B25;" |
[HK_Route4.svg](https://zh.wikipedia.org/wiki/File:HK_Route4.svg "fig:HK_Route4.svg")
4號幹線 |- \! colspan="2" align="center" |
往[堅尼地城方向](../Page/堅尼地城.md "wikilink") \!
colspan="2" align="center" | 往[柴灣方向](../Page/柴灣.md "wikilink") |- |
width="30" align="center" | 編號 | width="170" align="center" | 前往地區 |
width="170" align="center" | 前往地區 | width="30" align="center" | 編號 |- \!
colspan="4" align="center" | 連接[柴灣道迴旋處](../Page/柴灣道.md "wikilink") |- |
1A | [利眾街](../Page/利眾街.md "wikilink") | | |- | 2 |
[東區醫院](../Page/東區醫院.md "wikilink") |
[杏花邨](../Page/杏花邨.md "wikilink")、[小西灣](../Page/小西灣.md "wikilink")
| 2 |- | 2A |
[愛秩序灣](../Page/愛秩序灣.md "wikilink")、[阿公岩](../Page/阿公岩.md "wikilink")
| | |- | 2B | [筲箕灣](../Page/筲箕灣.md "wikilink") | | |- | | |
[石澳及赤柱](../Page/石澳.md "wikilink") | 3A |- | | |
[筲箕灣](../Page/筲箕灣.md "wikilink")、[愛秩序灣](../Page/愛秩序灣.md "wikilink")
| 3B |- | rowspan="2" | | rowspan="2" | | [康山](../Page/康山.md "wikilink")
| rowspan="2" | 3C |- |
[西灣河](../Page/西灣河.md "wikilink")、[太古城](../Page/太古城.md "wikilink")
|- | 4 | 太古城 | 太古城 | 4 |- | 5 |
[九龍（東）](../Page/東九龍.md "wikilink")（經[東區海底隧道](../Page/東區海底隧道.md "wikilink")）
 | 九龍（東）（經[東區海底隧道](../Page/東區海底隧道.md "wikilink")）  | 5 |- | 6 |
[北角](../Page/北角.md "wikilink") | [鰂魚涌](../Page/鰂魚涌.md "wikilink") |
6 |- | | | 北角 | 6A |- | 6B |
[銅鑼灣](../Page/銅鑼灣.md "wikilink")、[跑馬地](../Page/跑馬地.md "wikilink")、[九龍](../Page/九龍.md "wikilink")（經[紅磡海底隧道](../Page/紅磡海底隧道.md "wikilink")）
| | |- \! colspan="4" align="center" |
連接[中環灣仔繞道路段](../Page/中環灣仔繞道.md "wikilink")
|-

## 途經的公共交通服務

<div class="NavFrame collapsed" style="background-color: yellow;clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: yellow;margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [公共小巴](../Page/香港小巴.md "wikilink")

觀塘至柴灣線

</div>

</div>

## 改善工程

2000年5月政府在鰂魚涌及太古城一段東區走廊進行改善工程，舒緩往來東區海底隧道的交通。工程包括在海旁加建一條兩線高架橋作為往柴灣的東行線，原本的三線東行則改為兩線通往東區海底隧道，讓出一線改為西行。太古城交匯處亦進行改建，加建了多條[行車線](../Page/行車線.md "wikilink")，令鰂魚涌至西灣河一段東區走廊成為四線雙程。工程於2003年7月完成。\[4\]此外為防止東區海底隧道往北角、銅鑼灣及[香港仔隧道方向擠塞](../Page/香港仔隧道.md "wikilink")，政府同意於東隧架設電腦警告牌；同時[路政署同意於東區走廊柴灣段進行太古及東九龍方向防音路面工程](../Page/路政署.md "wikilink")。

## 批評

東區走廊常被批評為佔用海旁空間，使市民無法親近海濱\[5\]。部分路段相當靠近民居，居民亦受交通噪音所影響\[6\]。有人希望當局在該些路段增設[隔音屏障](../Page/隔音屏障.md "wikilink")。有人亦曾提議把屈臣道至[電照街一段改以隧道代替](../Page/電照街.md "wikilink")，惟因工程相當複雜，故建議從未被考慮。

## 未來發展

### 海旁改善

2012年3月21日，根據[規劃署](../Page/規劃署.md "wikilink")[顧問推出的港島東](../Page/顧問.md "wikilink")[研究](../Page/研究.md "wikilink")[報告](../Page/報告.md "wikilink")，東區走廊[灣仔至](../Page/灣仔.md "wikilink")[鰂魚涌路段下方將會興建長兩公里](../Page/鰂魚涌.md "wikilink")、闊3.5至8米的行人板道，由灣仔即將興建的海濱公園伸延至[海裕街](../Page/海裕街.md "wikilink")，沿途設4個出入口\[7\]。

2012年10月，[土木工程拓展署完成東區走廊下興建行人板道及單車徑的可行性研究](../Page/土木工程拓展署.md "wikilink")，確認東區走廊的橋躉承托力足以負荷加建的行人板道及其他設施，但是必須將行人板道提升高度4米至主水平基準9米或者13米，以方便[警輪及](../Page/警輪.md "wikilink")[滅火輪等船隻通過](../Page/滅火輪.md "wikilink")。然而由於行人板道臨近糖水道一段的東區走廊高度不足，板道必須伸延出海面，可能受到《[保護海港條例](../Page/保護海港條例.md "wikilink")》限制，有關[香港政府部門正在研究是否有所牴觸](../Page/香港政府部門.md "wikilink")\[8\]。

## 參考來源

## 外部連結

  -
[Category:香港海上高架道路](../Category/香港海上高架道路.md "wikilink")
[Category:香港高速公路](../Category/香港高速公路.md "wikilink")
[Category:香港主要幹道](../Category/香港主要幹道.md "wikilink")
[Category:香港行車天橋](../Category/香港行車天橋.md "wikilink")
[Category:東區 (香港)](../Category/東區_\(香港\).md "wikilink")
[Category:維多利亞港](../Category/維多利亞港.md "wikilink")

1.  [1](https://www.google.com.hk/maps/dir/22.2636461,114.2374258/22.2907437,114.1924845/@22.2789119,114.2159536,12z/data=!4m2!4m1!3e0?hl=zh-TW)
    圖中的起點為東區走廊起點，終點為東區走廊與[中環灣仔繞道的交匯點](../Page/中環灣仔繞道.md "wikilink")

2.

3.

4.  [路政署 -
    東區走廊改善工程北角交匯處至西灣河段](http://www.hyd.gov.hk/tc/road_and_railway/road_projects/B642th/index.html)

5.
6.
7.  \[<http://hk.news.yahoo.com/筲箕灣建高架天梯-設觀景台看鯉魚門-211423739.html>;
    筲箕灣建高架天梯 設觀景台看鯉魚門\] 《明報》 2012年3月22日

8.  [東廊確認可建行人板道](http://orientaldaily.on.cc/cnt/news/20121030/00176_026.html)
    《東方日報》 2012年10月30日