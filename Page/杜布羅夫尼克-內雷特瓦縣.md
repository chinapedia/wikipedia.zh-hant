**杜布羅夫尼克-內雷特瓦縣**（）是[克羅地亞](../Page/克羅地亞.md "wikilink")[達爾馬提亞地區](../Page/達爾馬提亞.md "wikilink")，也是該國最南的一個縣。東面的[波黑把本縣分成兩片僅靠領海相連的陸地](../Page/波黑.md "wikilink")，西傍[亞德里亞海](../Page/亞德里亞海.md "wikilink")。面積1,781平方公里\[1\]，人口122,870（2001年）。首府[杜布羅夫尼克](../Page/杜布羅夫尼克.md "wikilink")。与[波黑](../Page/波黑.md "wikilink")、[黑山共和国接壤](../Page/黑山共和国.md "wikilink")。

下分五市、十七鎮。縣議會有41席。

## 行政区划

  - [察夫塔特](../Page/察夫塔特.md "wikilink")
  - [杜布羅夫尼克](../Page/杜布羅夫尼克.md "wikilink")（首府）
  - [科爾丘拉島](../Page/科爾丘拉島.md "wikilink")
  - [梅特科維奇](../Page/梅特科維奇.md "wikilink")
  - [奧普曾](../Page/奧普曾.md "wikilink")
  - [普洛切](../Page/普洛切.md "wikilink")

## 图集

<center>

Dubrovnic_Croatia_June2007.jpg|[杜布羅夫尼克](../Page/杜布羅夫尼克.md "wikilink")
Dubrovnik_east_port.jpg|杜布羅夫尼克老城区 Korčula_overview.jpg|科爾丘拉島
Ston-SK.jpg|[斯通与](../Page/斯通.md "wikilink")[盐田](../Page/盐田.md "wikilink")

</center>

## 参考资料

[Category:克羅地亞行政區劃](../Category/克羅地亞行政區劃.md "wikilink")
[Category:外飛地](../Category/外飛地.md "wikilink")

1.