**范馨香**（），字**欣薌**，[湖北省應城縣](../Page/湖北省.md "wikilink")（今[應城市](../Page/應城市.md "wikilink")）人，[法律學者](../Page/法律.md "wikilink")，曾任[中華民國](../Page/中華民國.md "wikilink")[司法院大法官](../Page/司法院大法官.md "wikilink")。其父[范韻珩曾任地方法院院長](../Page/范韻珩.md "wikilink")、最高法院檢察署檢察官，母熊雲鸞為明末名臣[熊廷弼之後裔](../Page/熊廷弼.md "wikilink")，范馨香即為二人之長女。其夫為中華民國前[監察院院長](../Page/監察院.md "wikilink")[王作榮](../Page/王作榮.md "wikilink")。

## 生平

范馨香於[湖北省立第一女子中學畢業後](../Page/湖北省立第一女子中學.md "wikilink")，1938年以第一志願進入位於重慶的[国立中央大学法律系就讀](../Page/国立中央大学_\(南京\).md "wikilink")，其在校時學業、體育俱佳，中學時曾代表學校出席湖北省運動會的她，也是一名[短跑好手](../Page/短跑.md "wikilink")。大學時代亦曾代表學校參加大學校際運動會。

1943年大學畢業後，同年即[司法官考試及格](../Page/司法官.md "wikilink")。1944年4月接受[公務人員高等考試及格人員訓練後旋奉派為](../Page/公務人員高等考試.md "wikilink")[四川省長壽縣](../Page/四川省.md "wikilink")（今[重慶市](../Page/重慶市.md "wikilink")[長壽區](../Page/長壽區.md "wikilink")）地方法院[推事](../Page/推事.md "wikilink")（[法官](../Page/法官.md "wikilink")），1944年末與[王作榮結婚](../Page/王作榮.md "wikilink")，其後在一度任[江蘇](../Page/江蘇.md "wikilink")[鎮江地方法院推事](../Page/鎮江.md "wikilink")，其後雖亦曾轉任[新疆](../Page/新疆.md "wikilink")[喀什等地之地方法院推事](../Page/喀什.md "wikilink")，惟皆留部辦事。

1949年，隨[中华民国政府來台](../Page/中华民国政府.md "wikilink")，擔任[台灣高等法院推事](../Page/台灣高等法院.md "wikilink")，1955年升台灣高等法院簡任[庭長](../Page/庭長.md "wikilink")，是中華民國第一位女性簡任庭長；次年又擔任最高法院推事；1970年升任[最高法院庭長](../Page/中華民國最高法院.md "wikilink")，這兩次范馨香又是第一位女性到達此一職位。1972年以「曾任最高法院推事
10
年以上而成績卓著」的資格，出任司法院的大法官，是次於[張金蘭女士的第二位女性大法官](../Page/張金蘭.md "wikilink")，後來並於1976年、1985年二次連任該職，至第五屆時已是最年長的大法官。至目前為止，范馨香除了是任職時間最久的女性大法官外，也是唯一連任大法官的女性，甚至就歷屆大法官而言，其任職之久亦僅次於[翁岳生](../Page/翁岳生.md "wikilink")、[胡伯岳與](../Page/胡伯岳.md "wikilink")[林紀東三人](../Page/林紀東.md "wikilink")；而上述有關大法官的諸多紀錄在目前[中華民國憲法增修條文第](../Page/中華民國憲法增修條文.md "wikilink")5條第2項規定大法官不得連任後，除非再次修憲，否則將不會有人超越。

在教學方面，1956年起范馨香曾兼任[東吳大學及](../Page/東吳大學_\(台灣\).md "wikilink")[政治大學教授](../Page/政治大學.md "wikilink")，教授[商事法](../Page/商事法.md "wikilink")、[物權法等課程](../Page/物權法.md "wikilink")。在社會活動方面，曾任[國際崇她社副會長](../Page/國際崇她社.md "wikilink")、[國際職業婦女協會會長](../Page/國際職業婦女協會.md "wikilink")，並曾多次赴[日本](../Page/日本.md "wikilink")、[德國及非洲各國考察司法業務及參加國際婦女法學會議](../Page/德國.md "wikilink")。

1987年，范馨香在大法官任內因[肝病於](../Page/肝病.md "wikilink")[臺北去世](../Page/臺北.md "wikilink")，終年66歲。2001年，被司法院表揚為「高風亮節司法典範楷模」。現有以其名成立之[范馨香法學基金會](../Page/范馨香法學基金會.md "wikilink")。

## 參考文献

  - 《司法院史實紀要》，[司法院編](../Page/司法院.md "wikilink")，1985年。
  - 《大法官釋憲史料》，司法院編，1998年。

{{-}}

[Category:中華民國法學家](../Category/中華民國法學家.md "wikilink")
[Category:中華民國法官](../Category/中華民國法官.md "wikilink")
[Category:女性法官](../Category/女性法官.md "wikilink")
[Category:東吳大學教授](../Category/東吳大學教授.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[法](../Category/中央大学校友.md "wikilink")
[Category:南京大学校友](../Category/南京大学校友.md "wikilink")
[Category:湖北裔台灣人](../Category/湖北裔台灣人.md "wikilink")
[Category:应城人](../Category/应城人.md "wikilink")
[H](../Category/范姓.md "wikilink")
[Category:女性人物](../Category/女性人物.md "wikilink")