**巩献田**（）今[山东省](../Page/山东省.md "wikilink")[淄博市](../Page/淄博市.md "wikilink")[桓台县人](../Page/桓台县.md "wikilink")，[北京大学法学院](../Page/北京大学法学院.md "wikilink")[法理学教授](../Page/法理学.md "wikilink")。

## 经历

巩献田1944年生于今[山东省](../Page/山东省.md "wikilink")[淄博市](../Page/淄博市.md "wikilink")[桓台县](../Page/桓台县.md "wikilink")，1967年[北京政法学院毕业](../Page/北京政法学院.md "wikilink")，获法学学士学位；1981年7月于[北京大学法律系毕业](../Page/北京大学.md "wikilink")，获法学硕士学位；1987年4月于[南斯拉夫](../Page/南斯拉夫.md "wikilink")[萨拉热窝大学毕业](../Page/萨拉热窝大学.md "wikilink")，获法学博士学位。\[1\]

2005年8月12日发表公开信，认为《[中華人民共和國物權法](../Page/中華人民共和國物權法.md "wikilink")》草案违反《[中华人民共和国宪法](../Page/中华人民共和国宪法.md "wikilink")》，背离[社会主义](../Page/社会主义.md "wikilink")[公有制原则](../Page/公有制.md "wikilink")，有人认为这是《物权法》推迟交付[全国人大表决的因素之一](../Page/全国人大.md "wikilink")。\[2\]

## 参考文献

## 外部链接

  - [巩献田在乌有之乡的学者主页](https://web.archive.org/web/20071118071852/http://www.wyzxsx.com/xuezhe/gongxiantian/)

[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:中华人民共和国法学家](../Category/中华人民共和国法学家.md "wikilink")
[X](../Category/巩姓.md "wikilink")
[Category:桓台人](../Category/桓台人.md "wikilink")
[Category:中國政法大學校友](../Category/中國政法大學校友.md "wikilink")
[Category:北京大學校友](../Category/北京大學校友.md "wikilink")
[Category:薩拉熱窩大學校友](../Category/薩拉熱窩大學校友.md "wikilink")

1.  [巩献田在北大法学院的主页](http://www.law.pku.edu.cn/teacher/teacherView.asp?id=13&menuid=20038206442528&menuname=本院教师)

2.  [巩献田公开信，质疑《物权法》草案合宪性](http://www.chinaelections.org/NewsInfo.asp?NewsID=45986)