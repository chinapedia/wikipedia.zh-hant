**精氨基琥珀酸裂解酶**（，簡稱ASL），或稱**精氨酸琥珀酸裂解酶**是一種將[精氨基琥珀酸分裂成](../Page/精氨基琥珀酸.md "wikilink")[精氨酸及](../Page/精氨酸.md "wikilink")[延胡索酸的](../Page/延胡索酸.md "wikilink")[酶](../Page/酶.md "wikilink")（[EC](../Page/EC編號.md "wikilink")4.3.2.1）。它負責[尿素循環中的第](../Page/尿素循環.md "wikilink")4個步驟。

|          |                                                            |                                                |                                                      |                                        |                      |                                                |                             |                                                    |                                              |                                                         |                                      |                                                      |                                              |                                            |                                     |                              |                  |                                                              |                              |                            |                             |                                  |
| -------- | ---------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------------- | -------------------------------------- | -------------------- | ---------------------------------------------- | --------------------------- | -------------------------------------------------- | -------------------------------------------- | ------------------------------------------------------- | ------------------------------------ | ---------------------------------------------------- | -------------------------------------------- | ------------------------------------------ | ----------------------------------- | ---------------------------- | ---------------- | ------------------------------------------------------------ | ---------------------------- | -------------------------- | --------------------------- | -------------------------------- |
| {{复杂酶促反应 | major_substrate_1=[精氨基琥珀酸](../Page/精氨基琥珀酸.md "wikilink") | major_substrate_1_stoichiometric_constant= | major_substrate_1_image=Argininosuccinic acid.svg | major_substrate_1_image_size=200px | major_substrate_2= | major_substrate_2_stoichiometric_constant= | major_substrate_2_image= | major_product_1=[精氨酸](../Page/精氨酸.md "wikilink") | major_product_1_stoichiometric_constant= | major_product_1_image=L-arginine-skeletal-(tall).png | major_product_1_image_size=100px | major_product_2=[延胡索酸](../Page/延胡索酸.md "wikilink") | major_product_2_stoichiometric_constant= | major_product_2_image=Fumarate wpmp.png | major_product_2_image_size=80px | foward_enzyme=**精氨基琥珀酸裂解酶** | reverse_enzyme= | reaction_direction_(forward/reversible/reverse)=reversible | minor_foward_substrate(s)= | minor_foward_product(s)= | minor_reverse_product(s)= | minor_reverse_substrate(s)= }} |

## 基因及病理

為精氨基琥珀酸裂解酶編碼的[基因](../Page/基因.md "wikilink")，稱為**精氨基琥珀酸裂解基因**，是位於在[着絲粒與長臂](../Page/着絲粒.md "wikilink")(q)的11.2位置之間的第7號的[染色體](../Page/染色體.md "wikilink")，由鹼基對64,984,963至65,002,090。

精氨基琥珀酸裂解基因的[突變](../Page/突變.md "wikilink")，部份會導致精氨基琥珀酸裂解酶比起正常較短或畸形，影響其攜帶物質引發[化學反應的能力](../Page/化學反應.md "wikilink")，造成[精氨基琥珀酸尿症](../Page/精氨基琥珀酸尿症.md "wikilink")。[尿素循環的第](../Page/尿素循環.md "wikilink")4個步驟亦會被破壞，導致身體內積聚過多的[氮及有毒的](../Page/氮.md "wikilink")[氨](../Page/氨.md "wikilink")。

## 參考

  - PMID 9256435

  - PMID 11092456

  - PMID 11747433

[Category:动物基因](../Category/动物基因.md "wikilink")
[Category:裂合酶](../Category/裂合酶.md "wikilink")
[Category:尿素循环](../Category/尿素循环.md "wikilink")