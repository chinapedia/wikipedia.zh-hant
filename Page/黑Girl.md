**黑Girl** （英語：**Hey
Girl**，前稱：**黑澀會美眉**），[台灣女子演唱組合](../Page/台灣.md "wikilink")，成立於2006年5月，初期成員都是由《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》節目參加者中所選出。起初由九位成員組成，所以又叫「九妞妞」，曾是台灣演藝史上最多人的女子團體。出道時成員都只有15至22歲。第一代的成員包括：**「古典美眉」**[Apple](../Page/黃暐婷.md "wikilink")、**「性感美眉」**[大牙](../Page/周宜霈.md "wikilink")、**「日系美眉」**[MeiMei](../Page/郭婕祈.md "wikilink")、**「動感美眉」**[彤彤](../Page/蔡玓彤.md "wikilink")、**「嬌滴美眉」**[丫頭](../Page/詹子晴.md "wikilink")、**「氣質美眉」**[小薰](../Page/黃瀞怡.md "wikilink")、**「搞怪美眉」**[鬼鬼](../Page/鬼鬼.md "wikilink")、**「俏麗美眉」**[小蠻和](../Page/王承嫣.md "wikilink")**「搞笑美眉」**[小婕](../Page/張甯兒.md "wikilink")。此時期亦為**黑Girl**的黃金時期，跨足唱片、戲劇、節目主持及廣告代言各領域。

經歷多次成員變動後，於2010年重組第二代的**黑Girl**，包括初期成員丫頭、小薰，以及新成員括**「強勢美眉」**[勇兔和](../Page/林筳諭.md "wikilink")**「熱舞美眉」**[糖果](../Page/陳斯亞.md "wikilink")。允菲離開後，再加入**「活力美眉」**[子庭](../Page/張子庭.md "wikilink")（唯一不是《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》節目參加者出身）。後來[糖果也退出了](../Page/陳斯亞.md "wikilink")，在2013年再以[Twinko成員出道](../Page/Twinko.md "wikilink")。

然而，重組後的**黑Girl**自2012年完結EP宣傳後已鮮少合體演出，子庭大概在2013年11月也退出團體，3人亦分散在不同經理人公司，組合幾乎名存實亡，如今各成員在演藝圈不同領域各自發展。

## 改名原因

由於其他參加《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》節目的錄影者皆被統稱為「[黑澀會美眉](../Page/黑澀會美眉.md "wikilink")」，與**八妞妞**容易造成稱呼上的混淆，而某些節目成員的負面新聞，因為同一稱呼的關係，亦會直接影響「黑澀會美眉」聲譽；且「黑澀會」跟「黑社會」的[國語發音相近](../Page/國語.md "wikilink")，亦導致中國大陆不批准「黑澀會美眉」的唱片，使唱片无法与[台灣](../Page/台灣.md "wikilink")、[香港同步發行](../Page/香港.md "wikilink")。因此，從2008年8月8日起，「**黑澀會美眉-八妞妞**」正式更名為「**黑Girl**」。改爲**黑Girl**的原因有兩個，第一是不要忘本，不要忘了她們是從[我愛黑澀會出生的](../Page/我愛黑澀會.md "wikilink")，第二是因爲**黑**字跟英文的**Hey**同音，
所以**黑Girl**的英文名是**Hey Girl**。[Channel
V亦開始播放](../Page/Channel_V.md "wikilink")「是誰？取代黑澀會美眉」的宣傳廣告來配合。

## 成員列表

  -
    <small>各成員的詳細資料請參閱各成員之頁面。</small>

| 過往成員列表                              |
| ----------------------------------- |
| 藝名\[1\]                             |
| 第一代成員                               |
| [大牙](../Page/周宜霈.md "wikilink")     |
| [Apple](../Page/黃暐婷.md "wikilink")  |
| [MeiMei](../Page/郭婕祈.md "wikilink") |
| [貝童彤](../Page/蔡玓彤.md "wikilink")    |
| [丫頭](../Page/詹子晴.md "wikilink")     |
| [小薰](../Page/黃瀞怡.md "wikilink")     |
| [鬼鬼](../Page/吳映潔.md "wikilink")     |
| [小蠻](../Page/王承嫣.md "wikilink")     |
| [小婕](../Page/張甯兒.md "wikilink")     |
| 第二代成員                               |
| [丫頭](../Page/詹子晴.md "wikilink")     |
| [小薰](../Page/黃瀞怡.md "wikilink")     |
| [勇兔](../Page/林筳諭.md "wikilink")     |
| [糖果](../Page/陳斯亞.md "wikilink")     |
| [子庭](../Page/張子庭.md "wikilink")     |

### 成員變化

|                                     |      |       |      |      |      |      |           |
| :---------------------------------: | :--: | :---: | :--: | :--: | :--: | :--: | :-------: |
|        <small>成員＼年份</small>         | 2006 | 2007  | 2008 | 2009 | 2010 | 2011 | 2012-2013 |
|                5-12                 | 1-9  | 10-12 |  1   | 2-4  | 5-12 | 1-3  |    4-8    |
|   [大牙](../Page/周宜霈.md "wikilink")   |      |       |      |      |      |      |           |
| [MeiMei](../Page/郭婕祈.md "wikilink") |      |       |      |      |      |      |           |
| [Apple](../Page/黃暐婷.md "wikilink")  |      |       |      |      |      |      |           |
|   [小薰](../Page/黃瀞怡.md "wikilink")   |      |       |      |      |      |      |           |
|   [小婕](../Page/張甯兒.md "wikilink")   |      |       |      |      |      |      |           |
|   [小蠻](../Page/王承嫣.md "wikilink")   |      | 活動暫停  |      |      |      |      |           |
|   [鬼鬼](../Page/吳映潔.md "wikilink")   |      |       |      |      |      |      |           |
|   [丫頭](../Page/詹子晴.md "wikilink")   |      |       |      |      |      |      |           |
|   [彤彤](../Page/蔡玓彤.md "wikilink")   |      |       |      |      |      |      |           |
|   [勇兔](../Page/林筳諭.md "wikilink")   |      |       |      |      |      |      |           |
|   [糖果](../Page/陳斯亞.md "wikilink")   |      |       |      |      |      |      |           |
|   [子庭](../Page/張子庭.md "wikilink")   |      |       |      |      |      |      |           |

## 概述

### 黃金時期（2006年～2007年）

2006年5月，[星空傳媒在](../Page/星空家族.md "wikilink")《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》節目參加者中選出9名成員（被稱為「**九妞妞**」），組成**黑澀會美眉**，由[大牙擔任隊長](../Page/周宜霈.md "wikilink")。**九妞妞**憑著高人氣，即使第一張EP《[我愛黑澀會美眉](../Page/九妞妞的迷你專輯列表#我愛黑澀會美眉.md "wikilink")》當時成為台灣「最貴的一張僅單曲專輯」，但EP銷售量超過3萬張，台北首場簽唱會擠爆3千人，[星空傳媒](../Page/星空傳媒.md "wikilink")（經紀公司）於同年12月15日又推出第二張EP《[美眉私密的一天](../Page/九妞妞的迷你專輯列表#美眉私密的一天.md "wikilink")》。九位美眉分為[甜心轟炸機](../Page/九妞妞的迷你專輯列表#甜心轟炸機.md "wikilink")、[粉紅高壓電兩組](../Page/九妞妞的迷你專輯列表#粉紅高壓電.md "wikilink")，發行不同版本的EP。

**九妞妞**初出道人氣高企，健康活力和青春的形象吸引不少廣告代言的邀請，2007年6月13日與le
tea飲料合作發行第三張EP《[美眉私密Party：幸福的泡泡](../Page/九妞妞的迷你專輯列表#美眉私密Party:幸福的泡泡.md "wikilink")》連100頁美眉寫真書，九位美眉再度合併，次團體模式告一段落。這張EP是成員[貝童彤在團的最後一張唱片](../Page/蔡玓彤.md "wikilink")。同年**九妞妞**與[Lollipop棒棒堂共同拍攝了出道後第一部](../Page/Lollipop棒棒堂.md "wikilink")「[黑糖系列](../Page/template:黑糖系列.md "wikilink")」的青春偶像劇《[黑糖瑪奇朵](../Page/黑糖瑪奇朵.md "wikilink")》。**黑澀會美眉**在當時主持、戲劇、唱片、廣告等方面都取得了不錯的成績，風靡中港台三地甚至在日本拥有后援会，成為當時的熱門話題。

然而2007年10月5日，前成員[貝童彤因為個人因素離隊單飛](../Page/蔡玓彤.md "wikilink")\[2\]\[3\]。由於成員人數減至8人，所以改稱**八妞妞**。

### 重新出發（2008年）

2008年4月，**黑澀會美眉**與[福茂唱片的合約期滿](../Page/福茂唱片.md "wikilink")，轉到[華納音樂旗下](../Page/華納音樂_\(台灣\).md "wikilink")，並改名為**黑Girl**。同年8月29日，[華納音樂發行](../Page/華納音樂_\(台灣\).md "wikilink")**黑Girl**第一張專輯《[黑Girl首張同名專輯](../Page/黑Girl首張同名專輯.md "wikilink")》，這張專輯是成員[鬼鬼](../Page/吳映潔.md "wikilink")、[大牙](../Page/周宜霈.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[Apple和](../Page/黃暐婷.md "wikilink")[甯兒在團的最後一張唱片](../Page/張甯兒.md "wikilink")。**八妞妞**風格漸趨成熟，強調從美眉轉變成「姊姊」，並開始打入中國大陸市場。而成員亦慢慢開始個人活動，先後拍戲和主持節目。

### 停滯期（2009年～2011年年中）

2009年起成員開始大幅改動，相繼離團。2月，[小蠻被傳媒拍到與](../Page/王承嫣.md "wikilink")[Lollipop棒棒堂成員](../Page/Lollipop棒棒堂.md "wikilink")[小煜約會和公然吸煙](../Page/楊奇煜.md "wikilink")，被公司雪藏\[4\]，暫停參加**黑Girl**的活動和錄映節目[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")。同年4月，[鬼鬼經紀約到期](../Page/吳映潔.md "wikilink")，因個人計畫決定單飛，宣布離開[我愛黑澀會和](../Page/我愛黑澀會.md "wikilink")**黑Girl**，但仍會完成單飛前的廣告代言活動，現時成為CJ
E\&M旗下藝人。同時[小蠻回歸節目](../Page/王承嫣.md "wikilink")，但仍然暫停參加**黑Girl**的活動。

其間，明報、生活文化出版發行**黑Girl**的自傳《[Hey Girl
Friend](../Page/Hey_Girl_Friend.md "wikilink")》。經歷[小蠻暫停在](../Page/王承嫣.md "wikilink")**黑Girl**的活動和[鬼鬼離隊](../Page/吳映潔.md "wikilink")，**黑Girl**以6人姿態活動（**六妞妞**）。2009年下旬，**黑Girl**與[華納唱片的合約期滿](../Page/華納唱片_\(台灣\).md "wikilink")。

2010年，[大牙](../Page/周宜霈.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[Apple和](../Page/黃暐婷.md "wikilink")[甯兒相繼約滿加盟由經理人](../Page/張甯兒.md "wikilink")[Andy的新經紀公司](../Page/張世明.md "wikilink")[傳奇星娛樂](../Page/傳奇星娛樂.md "wikilink")，脫離**黑Girl**。[小蠻於](../Page/王承嫣.md "wikilink")3月17日最後一次為《[我愛黑澀棒棒堂](../Page/我愛黑澀棒棒堂.md "wikilink")》節目演出，並宣佈已經加盟[伊林娛樂](../Page/伊林模特兒經紀公司.md "wikilink")，正式脫離**黑Girl**，作個人單飛發展，未來將朝向全方位的模特兒工作努力，站上伸展台。\[5\]原有成員因合約屆滿時產生不少問題，組合的事業在2010年完全停頓。及後，本身為節目《[我愛黑澀棒棒堂](../Page/我愛黑澀棒棒堂.md "wikilink")》的固定班底的[勇兔及](../Page/林筳諭.md "wikilink")[糖果代替離團的成員加入](../Page/陳斯亞.md "wikilink")**黑Girl**。**黑Girl**因此變成4人組合（簡稱**四妞妞**），舊成員只剩下[丫頭和](../Page/詹子晴.md "wikilink")[小薰](../Page/黃瀞怡.md "wikilink")。然而同年9月16日的[南昌活動中](../Page/南昌.md "wikilink")，[勇兔並沒出現表演](../Page/林筳諭.md "wikilink")，而換上了新成員[子庭](../Page/張子庭.md "wikilink")。後來，[丫頭證實](../Page/詹子晴.md "wikilink")[勇兔已退出](../Page/林筳諭.md "wikilink")**黑Girl**，[子庭為](../Page/張子庭.md "wikilink")**黑Girl**的新成員。\[6\][勇兔在團只有短短](../Page/林筳諭.md "wikilink")4個月就離開。

2011年4月10日，[糖果在微博的簡介更新為](../Page/陳斯亞.md "wikilink")「無經紀約」，她也沒有出席**黑Girl**在4月15日前往大陸的活動，表示她已退出**黑Girl**，離團初時以錄影綜藝節目為主，後於2013年跟[星潼](../Page/林星潼.md "wikilink")、[可青](../Page/曾可青.md "wikilink")、[凱希](../Page/徐凱希.md "wikilink")、[篠崎組成](../Page/篠崎泫.md "wikilink")[女子團體](../Page/女子團體.md "wikilink")[Twinko](../Page/Twinko.md "wikilink")。

### 重新出發（2011年年中至2012年）

2011年，**黑Girl**與[群石國際](../Page/群石國際.md "wikilink")、[藝房紫傳播簽約成為旗下藝人](../Page/藝房紫傳播.md "wikilink")。相隔三年，經歷多次成員變動後，**黑Girl**以3人（**[丫頭](../Page/詹子晴.md "wikilink")**、**[小薰](../Page/黃瀞怡.md "wikilink")**、**[子庭](../Page/張子庭.md "wikilink")**）團體姿態重新出發，並由[丫頭擔任新](../Page/詹子晴.md "wikilink")**黑Girl**隊長。同年10月18日，發行首張同名EP（總計第四張EP）——**[Hey
Girl](../Page/Hey_Girl_\(EP\).md "wikilink")**。然而銷量成績不如以往，**黑Girl**人氣大不如前，及後並沒有再發行唱片。

### 各自發展（2012年至今）

其後的**黑Girl**成員多分開工作，較少合體出現螢幕前，[丫頭轉型為通告藝人](../Page/詹子晴.md "wikilink")，[小薰主要往戲劇發展](../Page/黃瀞怡.md "wikilink")，而[子庭則學習室內設計](../Page/張子庭.md "wikilink")，盼接手父親的公司。2013年起，三小妞相繼約滿，除[小薰外](../Page/黃瀞怡.md "wikilink")，[丫頭和](../Page/詹子晴.md "wikilink")[子庭都分別加入新的經紀公司](../Page/張子庭.md "wikilink")。**黑Girl**所有成員均各散東西，各自在演藝圈發展。雖沒有公開宣佈解散組合，但**黑Girl**已名存實亡。

## 參與演出及作品

### 節目主持

<table>
<tbody>
<tr class="odd">
<td><p>日期</p></td>
<td><p>播出頻道</p></td>
<td><p>節目名稱</p></td>
<td><p>性質</p></td>
<td><p>參與成員</p></td>
</tr>
<tr class="even">
<td><p>2005年至2006年</p></td>
<td><p><a href="../Page/TVBS歡樂台.md" title="wikilink">TVBS歡樂台</a></p></td>
<td><p>《娛樂新聞─美眉ㄅㄠˋㄅㄠˋ》</p></td>
<td><p>主持</p></td>
<td><p>九妞妞</p></td>
</tr>
<tr class="odd">
<td><p>2005年至2009年</p></td>
<td><p><a href="../Page/Channel_V.md" title="wikilink">Channel V</a></p></td>
<td><p>《<a href="../Page/我愛黑澀會.md" title="wikilink">我愛黑澀會</a>》</p></td>
<td><p>美眉/助教</p></td>
<td><p>九妞妞、勇兔、斯亞</p></td>
</tr>
<tr class="even">
<td><p>2006年至2008年</p></td>
<td><p>《<a href="../Page/模范棒棒堂.md" title="wikilink">模范棒棒堂</a>》</p></td>
<td><p>助教</p></td>
<td><p>九妞妞</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年至2009年</p></td>
<td><p>《<a href="../Page/流行_in_house.md" title="wikilink">流行 in house</a>》</p></td>
<td><p>助理主持</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/美眉普普風.md" title="wikilink">美眉普普風</a>》</p></td>
<td><p>VJ</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年至2009年<br />
(每年除夕及春節)</p></td>
<td><p>《<a href="../Page/我愛黑澀棒棒堂_(賀年節目).md" title="wikilink">我愛黑澀棒棒堂 (賀年節目)</a>》</p></td>
<td><p>美眉</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年至2007年</p></td>
<td><p>《[V]好機會》</p></td>
<td><p>VJ主持</p></td>
<td><p>大牙</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年9月7日至<br />
2008年11月30日</p></td>
<td><p>《<a href="../Page/哪裡5打抗.md" title="wikilink">哪裡5打抗</a>》</p></td>
<td><p>主持</p></td>
<td><p>Apple</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年7月3日至<br />
2009年11月27日</p></td>
<td><p>《<a href="../Page/Love_Love_Love_(綜藝節目).md" title="wikilink">Love Love Love</a>》</p></td>
<td><p>MeiMei</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年10月12日至<br />
2011年2月3日</p></td>
<td><p>《<a href="../Page/我愛黑澀棒棒堂.md" title="wikilink">我愛黑澀棒棒堂</a>》</p></td>
<td><p>美眉</p></td>
<td><p>六妞妞、小蠻、勇兔、斯亞</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年12月27日至<br />
2010年3月28日</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>《<a href="../Page/旅行應援團.md" title="wikilink">旅行應援團</a>》</p></td>
<td><p>外景助理主持</p></td>
<td><p>小薰、勇兔</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/東南衛視.md" title="wikilink">東南衛視</a></p></td>
<td><p>《<a href="../Page/越秀越開心.md" title="wikilink">越秀越開心</a>》</p></td>
<td><p>主持</p></td>
<td><p>丫頭</p></td>
</tr>
<tr class="even">
<td><p>2010年3月</p></td>
<td><p><a href="../Page/星空衛視.md" title="wikilink">星空衛視</a></p></td>
<td><p>《<a href="../Page/Lady呱呱.md" title="wikilink">Lady呱呱</a>》</p></td>
<td><p>小薰、丫頭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/八大綜合台.md" title="wikilink">八大綜合台</a></p></td>
<td><p>《<a href="../Page/美眉向前行.md" title="wikilink">美眉向前行</a>》</p></td>
<td><p>三小妞</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 代言

#### 廣告代言

|                        |                                                                                                                                                                        |                                                                                                                                        |
| ---------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| 代言年份                   | 代言產品                                                                                                                                                                   | 參與成員                                                                                                                                   |
| 2006年                  | 新蜀山劍俠Online                                                                                                                                                            | 九妞妞                                                                                                                                    |
| Edwin牛仔褲               |                                                                                                                                                                        |                                                                                                                                        |
| 2007年                  | 爭鮮回転壽司                                                                                                                                                                 |                                                                                                                                        |
| Le tea cherry微發泡蘇打櫻桃口味 |                                                                                                                                                                        |                                                                                                                                        |
| 美眉星光保濕凝膠               |                                                                                                                                                                        |                                                                                                                                        |
| KnightsBridge 服裝品牌     |                                                                                                                                                                        |                                                                                                                                        |
| Yuskin護手霜              | [大牙](../Page/周宜霈.md "wikilink")、[丫頭](../Page/詹子晴.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[小薰](../Page/黃瀞怡.md "wikilink")、[甯兒](../Page/張甯兒.md "wikilink")    |                                                                                                                                        |
| 台灣森永製菓Hi-Chew「嗨啾」軟糖    | [GEMMA](../Page/吳映潔.md "wikilink")、[丫頭](../Page/詹子晴.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[小薰](../Page/黃瀞怡.md "wikilink")                                 |                                                                                                                                        |
| 潘朵拉的甜蜜衣櫥               | [GEMMA](../Page/吳映潔.md "wikilink")、[大牙](../Page/周宜霈.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[小薰](../Page/黃瀞怡.md "wikilink")                                 |                                                                                                                                        |
| 卡打車身體補給水               | [GEMMA](../Page/吳映潔.md "wikilink")                                                                                                                                     |                                                                                                                                        |
| 愛之味覆盆子飲料               |                                                                                                                                                                        |                                                                                                                                        |
| Epson 相片列表機            |                                                                                                                                                                        |                                                                                                                                        |
| 2008年                  | 潘朵拉的甜蜜衣櫥                                                                                                                                                               | [GEMMA](../Page/吳映潔.md "wikilink")、[大牙](../Page/周宜霈.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[小薰](../Page/黃瀞怡.md "wikilink") |
| 爭鮮回転壽司                 | 八妞妞                                                                                                                                                                    |                                                                                                                                        |
| 美眉星光美白面膜               |                                                                                                                                                                        |                                                                                                                                        |
| KnightsBridge 服裝品牌     |                                                                                                                                                                        |                                                                                                                                        |
| 奪寶王Online              |                                                                                                                                                                        |                                                                                                                                        |
| 康師傅鮮之每日C               | [GEMMA](../Page/吳映潔.md "wikilink")、[丫頭](../Page/詹子晴.md "wikilink")、[MeiMei](../Page/郭婕祈.md "wikilink")、[小薰](../Page/黃瀞怡.md "wikilink")、[甯兒](../Page/張甯兒.md "wikilink") |                                                                                                                                        |
| 2009年                  |                                                                                                                                                                        |                                                                                                                                        |
| KnightsBridge 服裝品牌     | 六妞妞                                                                                                                                                                    |                                                                                                                                        |
| FIFA Online 足球寶貝       |                                                                                                                                                                        |                                                                                                                                        |
| 芬達凍感10下                |                                                                                                                                                                        |                                                                                                                                        |
| SKIN79化妝品              | [小薰](../Page/黃瀞怡.md "wikilink")                                                                                                                                        |                                                                                                                                        |
| 2010年                  | KnightsBridge 服裝品牌                                                                                                                                                     | 四妞妞、四小妞                                                                                                                                |
| 2011年                  | 黑Girl（四小妞、三小妞）                                                                                                                                                         |                                                                                                                                        |
| 2012年                  | 三小妞                                                                                                                                                                    |                                                                                                                                        |
| 聖境傳說                   |                                                                                                                                                                        |                                                                                                                                        |

#### 活動代言

|                                                                          |                                            |      |
| ------------------------------------------------------------------------ | ------------------------------------------ | ---- |
| 代言年份                                                                     | 代言產品                                       | 參與成員 |
| 2008年                                                                    | [香港動漫電玩節大使](../Page/香港動漫電玩節.md "wikilink") | 八妞妞  |
| [中華民國](../Page/中華民國.md "wikilink")[紅十字會公益大使](../Page/紅十字會.md "wikilink") |                                            |      |
| 2011年                                                                    | KnightsBridge 一日店長活動                       | 三小妞  |
|                                                                          |                                            |      |

### 音樂

#### 專輯

<table>
<thead>
<tr class="header">
<th><p>唱片 #</p></th>
<th><p>資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>《<a href="../Page/黑Girl首張同名專輯.md" title="wikilink">黑Girl首張同名專輯</a>》</p>
<ul>
<li>發行日期：2008年8月29日</li>
<li>唱片公司：<a href="../Page/華納唱片_(台灣).md" title="wikilink">華納唱片 (台灣)</a></li>
<li>參與成員：黑Girl－八妞妞</li>
<li><a href="../Page/蔡玓彤.md" title="wikilink">貝童彤退出</a>，以八人姿態回歸，並改團名</li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

#### EP

<table>
<thead>
<tr class="header">
<th><p>唱片 #</p></th>
<th><p>資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1st</p></td>
<td><p>《<a href="../Page/九妞妞的迷你專輯列表#我愛黑澀會美眉.md" title="wikilink">我愛黑澀會美眉</a>》</p>
<ul>
<li>發行日期：2006年7月14日</li>
<li>唱片公司：福茂唱片</li>
<li>參與成員：黑澀會美眉－九妞妞</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>《<a href="../Page/九妞妞的迷你專輯列表#粉紅高壓電.md" title="wikilink">美眉私密的一天 - 粉紅高壓電</a>》</p>
<ul>
<li>發行日期：2006年12月15日</li>
<li>唱片公司：福茂唱片</li>
<li>參與成員：黑澀會美眉－粉紅高壓電<br />
（大牙、Apple、丫頭、GEMMA、小蠻）</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/九妞妞的迷你專輯列表#甜心轟炸機.md" title="wikilink">美眉私密的一天 - 甜心轟炸機</a>》</p>
<ul>
<li>發行日期：2006年12月15日</li>
<li>唱片公司：福茂唱片</li>
<li>參與成員：黑澀會美眉－甜心轟炸機<br />
（MeiMei、貝童彤、小薰、甯兒）</li>
</ul></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3rd</p></td>
<td><p>《<a href="../Page/九妞妞的迷你專輯列表#美眉私密Party:幸福的泡泡.md" title="wikilink">美眉私密Party:幸福的泡泡</a>》</p>
<ul>
<li>發行日期：2007年6月7日</li>
<li>唱片公司：福茂唱片</li>
<li>參與成員：黑澀會美眉－九妞妞</li>
<li>這張EP為成員<a href="../Page/蔡玓彤.md" title="wikilink">貝童彤在團時出最後一張唱片</a><br />
（<a href="../Page/黑糖瑪奇朵#黑糖瑪奇朵偶像劇原聲帶.md" title="wikilink">黑糖瑪奇朵偶像劇原聲帶不計算</a>）</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>《<a href="../Page/Hey_Girl_(EP).md" title="wikilink">Hey Girl</a>》</p>
<ul>
<li>發行日期：2011年10月18日</li>
<li>唱片公司：<a href="../Page/群石國際.md" title="wikilink">群石國際</a></li>
<li>參與成員：黑Girl－三小妞</li>
<li>經歷多次成員變動，新成員<a href="../Page/張子庭.md" title="wikilink">子庭加入</a></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

#### 原聲帶

<table>
<thead>
<tr class="header">
<th><p>唱片 #</p></th>
<th><p>資料</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1st</p></td>
<td><p><a href="../Page/18禁不禁.md" title="wikilink">18禁不禁影音全紀錄CD</a>+DVD</p>
<ul>
<li>發行日期：2007年5月15日</li>
<li>唱片公司：<a href="../Page/福茂唱片.md" title="wikilink">福茂唱片</a></li>
<li>參與成員：<a href="../Page/黃瀞怡.md" title="wikilink">小薰</a></li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p><a href="../Page/黑糖瑪奇朵#黑糖瑪奇朵偶像劇原聲帶.md" title="wikilink">黑糖瑪奇朵偶像劇原聲帶</a> (CD+DVD)</p>
<ul>
<li>發行日期：2007年8月31日</li>
<li>唱片公司：<a href="../Page/科藝百代.md" title="wikilink">科藝百代</a> （<a href="../Page/金牌大風.md" title="wikilink">金牌大風前身</a>）</li>
<li>參與成員：黑澀會美眉－九妞妞</li>
<li>與<a href="../Page/Lollipop@F.md" title="wikilink">Lollipop棒棒堂合作</a></li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3rd</p></td>
<td><p><a href="../Page/黑糖群俠傳#黑糖群俠傳電視原聲帶.md" title="wikilink">黑糖群俠傳電視原聲帶</a> (CD)</p>
<ul>
<li>發行日期：2008年10月3日</li>
<li>唱片公司：<a href="../Page/金牌大風.md" title="wikilink">金牌大風</a></li>
<li>參與成員：<a href="../Page/詹子晴.md" title="wikilink">丫頭</a>、<a href="../Page/黃瀞怡.md" title="wikilink">小薰</a>、<a href="../Page/王承嫣.md" title="wikilink">小蠻</a></li>
<li>與<a href="../Page/Lollipop-F.md" title="wikilink">Lollipop棒棒堂</a>-<a href="../Page/楊奇煜.md" title="wikilink">小煜</a>、<a href="../Page/劉峻緯.md" title="wikilink">阿緯</a>、<a href="../Page/邱勝翊.md" title="wikilink">王子合作</a></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

#### 其他歌曲

  - Ben 10（卡通《[Ben
    10](../Page/Ben_10.md "wikilink")》台灣主題曲）－（大牙、MeiMei、丫頭、小薰、小蠻）
  - 四川賑災歌曲－相親相愛
    （合唱：八妞妞、[薛凱琪](../Page/薛凱琪.md "wikilink")、[F.I.R.飛兒樂團](../Page/F.I.R.飛兒樂團.md "wikilink")、[方大同](../Page/方大同.md "wikilink")、[辛曉琪](../Page/辛曉琪.md "wikilink")、[蕭敬騰](../Page/蕭敬騰.md "wikilink")、[郭采潔](../Page/郭采潔.md "wikilink")、[183Club](../Page/183Club.md "wikilink")、[七朵花](../Page/七朵花.md "wikilink")、[金海心](../Page/金海心.md "wikilink")、[老狼](../Page/老狼.md "wikilink")、[徐若瑄](../Page/徐若瑄.md "wikilink")、[張菲](../Page/張菲.md "wikilink")、[郭美美](../Page/郭美美.md "wikilink")、[蔡淳佳](../Page/蔡淳佳.md "wikilink")、[陳偉聯](../Page/陳偉聯.md "wikilink")）
  - 哪個（電影《寶島雙雄》主題曲）－（三小妞 & [房祖名合唱](../Page/房祖名.md "wikilink")）

### 團體獎項

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong>2010</strong></p></td>
<td style="text-align: left;"><ul>
<li>環球紅歌盛典 - CCTV風雲音樂亞洲最受關注組合獎</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2012</strong></p></td>
<td style="text-align: left;"><ul>
<li>第16屆全球华语榜中榜 - 最佳新晋组合</li>
</ul></td>
</tr>
</tbody>
</table>

### 書籍

  - 2006年7月14日，《我愛黑澀會美眉》單曲CD+美眉秘密日記DVD+寫真書
  - 2007年6月7日，美眉私密Party寫真集（九妞妞）（[福茂唱片發行](../Page/福茂唱片.md "wikilink")）
  - 2007年8月，[18禁不禁戀愛講義](../Page/18禁不禁.md "wikilink")（小薰）
  - 2007年9月，黑糖瑪奇朵歡樂Party （九妞妞）
  - 2007年9月，黑糖瑪奇朵電視小說（九妞妞）
  - 2008年5月12日，[翻滾吧！蛋炒飯幕後紀實](../Page/翻滾吧！蛋炒飯#週邊商品.md "wikilink")（小薰）（台視文化出版發行）
  - 2008年5月23日，翻滾吧！蛋炒飯電視小說（小薰）（台視文化出版發行）
  - 2008年7月2日，翻滾吧！蛋炒飯【雙廚講堂】 我們要為你做做飯（小薰）（台視文化出版發行）
  - 2008年10月，[黑糖群俠傳電視小說](../Page/黑糖群俠傳.md "wikilink")（小薰、丫頭、小蠻、MeiMei）
  - 2008年10月，黑糖群俠傳寫真秘笈（小薰、丫頭、小蠻、MeiMei）
  - 2008年11月21日，[霹靂MIT電視小說](../Page/霹靂MIT#週邊商品.md "wikilink") （GEMMA）
  - 2008年12月30日，霹靂MIT霹靂寫真紀實（GEMMA）
  - 2009年7月22日 ，《Hey Girl Friend》（内地版名称：黑Girl的秘密）（六妞妞）（明報、生活文化出版發行）
  - 2011年，[美樂。加油電視寫真書](../Page/美樂。加油#週邊商品.md "wikilink")（小薰）

### 戲劇

<table>
<tbody>
<tr class="odd">
<td><p>首播日期</p></td>
<td><p>播出頻道</p></td>
<td><p>劇名</p></td>
<td><p>角色</p></td>
<td><p>合作演員</p></td>
<td><p>性質</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/緯來電視台.md" title="wikilink">緯來電視台</a></p></td>
<td><p>《<a href="../Page/Q狼特勤組.md" title="wikilink">Q狼特勤組</a>》</p></td>
<td><p>子庭—</p></td>
<td><p><a href="../Page/張天霖.md" title="wikilink">張天霖</a>、<a href="../Page/梁又琳.md" title="wikilink">梁又琳</a>、<a href="../Page/唐治平.md" title="wikilink">唐治平</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p>2006年5月1日</p></td>
<td><p><a href="../Page/三立.md" title="wikilink">三立</a></p></td>
<td><p>《<a href="../Page/住左邊住右邊.md" title="wikilink">住左邊住右邊</a>》<br />
第4季全民拼幸福</p></td>
<td><p>丫頭─小玉</p></td>
<td><p><a href="../Page/郭子乾.md" title="wikilink">郭子乾</a>、<a href="../Page/卜學亮.md" title="wikilink">卜學亮</a>、<a href="../Page/楊可涵.md" title="wikilink">楊可涵</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p>2006年6月26日</p></td>
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/危險心靈_(電視劇).md" title="wikilink">危險心靈</a>》</p></td>
<td><p>甯兒─張筱婕</p></td>
<td><p><a href="../Page/溫昇豪.md" title="wikilink">溫昇豪</a>、<a href="../Page/張書豪.md" title="wikilink">張書豪</a></p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2006年11月12日</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>《<a href="../Page/驚異傳奇.md" title="wikilink">驚異傳奇</a>》之鬼計</p></td>
<td><p>小薰─佩如</p></td>
<td><p><a href="../Page/張惠春.md" title="wikilink">張惠春</a>、<a href="../Page/楊龍澤.md" title="wikilink">楊龍澤</a>、<a href="../Page/張皓明.md" title="wikilink">張皓明</a></p></td>
<td><p>單元主角</p></td>
</tr>
<tr class="even">
<td><p>2006年12月4日</p></td>
<td><p>《<a href="../Page/天使情人.md" title="wikilink">天使情人</a>》</p></td>
<td><p>大牙─小君<br />
小薰─林芳華 (年輕)</p></td>
<td><p><a href="../Page/明道_(藝人).md" title="wikilink">明道</a>、<a href="../Page/白歆惠.md" title="wikilink">白歆惠</a>、<a href="../Page/蔣怡.md" title="wikilink">蔣怡</a>、<a href="../Page/杜德偉.md" title="wikilink">杜德偉</a></p></td>
<td><p>女配角<br />
客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年4月29日</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a>、<a href="../Page/八大電視.md" title="wikilink">八大</a></p></td>
<td><p>《<a href="../Page/18禁不禁.md" title="wikilink">18禁不禁</a>》</p></td>
<td><p>小薰─夏念喬</p></td>
<td><p><a href="../Page/翁瑞迪.md" title="wikilink">阿本</a>、<a href="../Page/董偉倫.md" title="wikilink">小草</a>、<a href="../Page/地球_(藝人).md" title="wikilink">地球</a>、<a href="../Page/黃楷婷.md" title="wikilink">Party</a>、<a href="../Page/張家寧.md" title="wikilink">張家寧</a>、<a href="../Page/周曉涵.md" title="wikilink">周曉涵</a></p></td>
<td><p>女主角</p></td>
</tr>
<tr class="even">
<td><p>2007年12月15日</p></td>
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/我在墾丁*天氣晴.md" title="wikilink">我在墾丁*天氣晴</a>》</p></td>
<td><p>大牙─小雁</p></td>
<td><p><a href="../Page/阮經天.md" title="wikilink">阮經天</a>、<a href="../Page/彭于晏.md" title="wikilink">彭于晏</a>、<a href="../Page/李康宜.md" title="wikilink">李康宜</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p>2007年7月15日</p></td>
<td><p><a href="../Page/民視.md" title="wikilink">民視</a>、<a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>《<a href="../Page/黑糖瑪奇朵.md" title="wikilink">黑糖瑪奇朵</a>》</p></td>
<td><p>九妞妞 (飾演同名角色)</p></td>
<td><p><a href="../Page/Lollipop棒棒堂.md" title="wikilink">Lollipop棒棒堂</a>、<a href="../Page/林郁智.md" title="wikilink">納豆</a>、<a href="../Page/劉容嘉.md" title="wikilink">容嘉</a></p></td>
<td><p>女主角</p></td>
</tr>
<tr class="even">
<td><p>2007年10月6日</p></td>
<td><p>woo.com網路劇</p></td>
<td><p>《<a href="../Page/黑糖來了.md" title="wikilink">黑糖來了</a>》</p></td>
<td><p>大牙─大牙<br />
MeiMei─MeiMei<br />
Apple─Apple<br />
小蠻─小蠻</p></td>
<td><p><a href="../Page/簡翔棋.md" title="wikilink">小馬</a>、<a href="../Page/李銓.md" title="wikilink">李銓</a>、<a href="../Page/江莛鈞.md" title="wikilink">Terry</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年5月4日</p></td>
<td><p><a href="../Page/中視.md" title="wikilink">中視</a>、<a href="../Page/八大.md" title="wikilink">八大</a></p></td>
<td><p>《<a href="../Page/翻滾吧！蛋炒飯.md" title="wikilink">翻滾吧！蛋炒飯</a>》</p></td>
<td><p>小薰─哇莎米</p></td>
<td><p><a href="../Page/汪東城.md" title="wikilink">汪東城</a>、<a href="../Page/唐禹哲.md" title="wikilink">唐禹哲</a>、<a href="../Page/卓文萱.md" title="wikilink">卓文萱</a></p></td>
<td><p>第二女主角</p></td>
</tr>
<tr class="even">
<td><p>2008年7月26日</p></td>
<td><p><a href="../Page/衛視中文台.md" title="wikilink">衛視中文台</a></p></td>
<td><p>《<a href="../Page/黑糖群俠傳.md" title="wikilink">黑糖群俠傳</a>》</p></td>
<td><p>小薰─任瑩瑩<br />
丫頭─陸見寧<br />
小蠻─小聾女<br />
MeiMei─聖女護法<br />
甯兒─邪女護法<br />
GEMMA、Apple、大牙─邪教護法<br />
勇兔─霜兒</p></td>
<td><p><a href="../Page/Lollipop棒棒堂.md" title="wikilink">Lollipop棒棒堂</a>、<a href="../Page/簡翔棋.md" title="wikilink">小馬</a>、<a href="../Page/翁瑞迪.md" title="wikilink">阿本</a></p></td>
<td><p>女主角：小薰、丫頭、小蠻<br />
女配角：MeiMei<br />
客串：GEMMA、小婕、大牙、Apple、勇兔</p></td>
</tr>
<tr class="odd">
<td><p>2008年11月7日</p></td>
<td><p><a href="../Page/民視.md" title="wikilink">民視</a>、<a href="../Page/八大.md" title="wikilink">八大</a></p></td>
<td><p>《<a href="../Page/霹靂MIT.md" title="wikilink">霹靂MIT</a>》</p></td>
<td><p>GEMMA─李曉星<br />
(ID:天魔星)</p></td>
<td><p><a href="../Page/炎亞綸.md" title="wikilink">炎亞綸</a>、<a href="../Page/范瑋琪.md" title="wikilink">范瑋琪</a>、<a href="../Page/黃鴻升.md" title="wikilink">小鬼</a>、<a href="../Page/陸廷威.md" title="wikilink">陸廷威</a>、<a href="../Page/田麗.md" title="wikilink">田麗</a>、<a href="../Page/張善傑.md" title="wikilink">張善傑</a></p></td>
<td><p>女主角</p></td>
</tr>
<tr class="even">
<td><p>2009年9月12日</p></td>
<td><p><a href="../Page/Channel_V.md" title="wikilink">Channel V</a></p></td>
<td><p>《<a href="../Page/廖問之越V風雲.md" title="wikilink">廖問之越V風雲</a>》</p></td>
<td><p>丫頭─小祿的老婆<br />
MeiMei─千面殺手</p></td>
<td><p><a href="../Page/廖亦崟.md" title="wikilink">威廉</a>、<a href="../Page/廖允傑.md" title="wikilink">小-{杰}-</a>、<a href="../Page/簡翔棋.md" title="wikilink">小馬</a>、<a href="../Page/劉祿存.md" title="wikilink">小祿</a>、<a href="../Page/李銓.md" title="wikilink">李銓</a></p></td>
<td><p>女主角</p></td>
</tr>
<tr class="odd">
<td><p>2011年3月6日</p></td>
<td><p><a href="../Page/超級電視.md" title="wikilink">超視</a></p></td>
<td><p>《<strong><a href="../Page/33故事館.md" title="wikilink">33故事館</a></strong>之【妹妹情人夢】》</p></td>
<td><p>丫頭─宜蓁<br />
小薰─嘉榆</p></td>
<td><p><a href="../Page/陳奕_(台灣).md" title="wikilink">陳奕</a></p></td>
<td><p>單元女主角</p></td>
</tr>
<tr class="even">
<td><p>2011年3月13日</p></td>
<td><p>《<strong>33故事館</strong>之【相戀無罪】》</p></td>
<td><p>斯亞─糖糖</p></td>
<td><p><a href="../Page/丁春誠.md" title="wikilink">丁春誠</a>、<a href="../Page/沈建宏.md" title="wikilink">沈建宏</a>、<a href="../Page/Zero+.md" title="wikilink">愛瑪Emma</a>、<a href="../Page/小炳.md" title="wikilink">小炳</a></p></td>
<td><p>單元女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年4月17日</p></td>
<td><p><a href="../Page/台視.md" title="wikilink">台視</a>、<a href="../Page/三立.md" title="wikilink">三立</a></p></td>
<td><p>《<a href="../Page/醉後決定愛上你.md" title="wikilink">醉後決定愛上你</a>》</p></td>
<td><p>小薰─《綜藝麻辣鍋》主持人</p></td>
<td><p><a href="../Page/楊丞琳.md" title="wikilink">楊丞琳</a>、<a href="../Page/張孝全.md" title="wikilink">張孝全</a>、<a href="../Page/許瑋甯.md" title="wikilink">許瑋甯</a></p></td>
<td><p>客串第1,7集</p></td>
</tr>
<tr class="even">
<td><p>2011年4月22日</p></td>
<td><p><a href="../Page/民視.md" title="wikilink">民視</a></p></td>
<td><p>《<a href="../Page/新兵日記之特戰英雄.md" title="wikilink">新兵日記之特戰英雄</a>》</p></td>
<td><p>小薰─吳君鳳</p></td>
<td><p><a href="../Page/江俊翰.md" title="wikilink">江俊翰</a>、<a href="../Page/胡宇威.md" title="wikilink">胡宇威</a>、<a href="../Page/葉瑋庭.md" title="wikilink">葉瑋庭</a></p></td>
<td><p>第2-15章女配角</p></td>
</tr>
<tr class="odd">
<td><p>2011年6月5日</p></td>
<td><p><a href="../Page/八大.md" title="wikilink">八大</a>、<a href="../Page/中視.md" title="wikilink">中視</a></p></td>
<td><p>《<a href="../Page/美樂。加油.md" title="wikilink">美樂。加油</a>》</p></td>
<td><p>小薰─韓以霏</p></td>
<td><p><a href="../Page/王心凌.md" title="wikilink">王心凌</a>、<a href="../Page/賀軍翔.md" title="wikilink">賀軍翔</a>、<a href="../Page/施易男.md" title="wikilink">施易男</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p>2011年8月11日</p></td>
<td><p><a href="../Page/華視.md" title="wikilink">華視</a></p></td>
<td><p>《<a href="../Page/莒光園地.md" title="wikilink">莒光園地</a>》之軍法教育單元劇「迷惘」</p></td>
<td><p>丫頭─胡茵茵<br />
子庭─周筱丹</p></td>
<td><p><a href="../Page/李易.md" title="wikilink">李易</a>、<a href="../Page/汪建民.md" title="wikilink">汪建民</a></p></td>
<td><p>單元女主角</p></td>
</tr>
<tr class="odd">
<td><p>2012年7月20日</p></td>
<td><p><a href="../Page/台視.md" title="wikilink">台視</a></p></td>
<td><p>《<a href="../Page/花是愛.md" title="wikilink">花是愛</a>》</p></td>
<td><p>小薰─王雪芙</p></td>
<td><p><a href="../Page/吳慷仁.md" title="wikilink">吳慷仁</a>、<a href="../Page/周幼婷.md" title="wikilink">周幼婷</a>、<a href="../Page/黎登勤.md" title="wikilink">Duncan</a>、<a href="../Page/夏于喬.md" title="wikilink">夏于喬</a>、<a href="../Page/金勤.md" title="wikilink">金勤</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p>2012年11月8日</p></td>
<td><p><a href="../Page/台視.md" title="wikilink">台視</a>、<a href="../Page/三立.md" title="wikilink">三立</a>、<a href="../Page/東森綜合台.md" title="wikilink">東森綜合</a></p></td>
<td><p>《<a href="../Page/我租了一個情人.md" title="wikilink">我租了一個情人</a>》</p></td>
<td><p>小薰─趙珊珊</p></td>
<td><p><a href="../Page/宥勝.md" title="wikilink">宥勝</a>、<a href="../Page/許瑋甯.md" title="wikilink">許瑋甯</a>、<a href="../Page/李康宜.md" title="wikilink">李康宜</a>、<a href="../Page/柯有倫.md" title="wikilink">柯有倫</a></p></td>
<td><p>女二角</p></td>
</tr>
<tr class="odd">
<td><p>2013年6月6日</p></td>
<td><p><a href="../Page/華視.md" title="wikilink">華視</a></p></td>
<td><p>《<a href="../Page/莒光園地.md" title="wikilink">莒光園地</a> 法制單元劇「心念」》</p></td>
<td><p><a href="../Page/小薰.md" title="wikilink">小薰</a>─佳佳<br />
Amy</p></td>
<td></td>
<td><p>單元女主角</p></td>
</tr>
</tbody>
</table>

### 微電影

<table>
<tbody>
<tr class="odd">
<td><p>播出日期</p></td>
<td><p>戲名</p></td>
<td><p>飾演</p></td>
<td><p>性質</p></td>
</tr>
<tr class="even">
<td><p>2012年3月22日</p></td>
<td><p><a href="../Page/聖境傳說.md" title="wikilink">聖境傳說</a>（代言人）</p></td>
<td><p>小薰─賽車手<br />
丫頭─Party女王<br />
子庭─女特工</p></td>
<td><p>主角</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

<table>
<tbody>
<tr class="odd">
<td><p>上映日期</p></td>
<td><p>戲名</p></td>
<td><p>飾演</p></td>
<td><p>合作演員</p></td>
<td><p>性質</p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/魔鬼女教頭.md" title="wikilink">魔鬼女教頭</a></p></td>
<td><p>貝童彤—同學<br />
子庭—</p></td>
<td><p><a href="../Page/吳辰君.md" title="wikilink">吳辰君</a>、<a href="../Page/許孟哲.md" title="wikilink">許孟哲</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/人在江湖_(香港電影).md" title="wikilink">人在江湖</a></p></td>
<td><p>大牙—大哥的女人</p></td>
<td><p><a href="../Page/張智霖.md" title="wikilink">張智霖</a>、<a href="../Page/張達明.md" title="wikilink">張達明</a>、<a href="../Page/尹馨.md" title="wikilink">尹馨</a></p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p>2011年7月</p></td>
<td><p><a href="../Page/布袋甩尾.md" title="wikilink">布袋甩尾</a>（手機電影）</p></td>
<td><p>丫頭─嫚君</p></td>
<td><p><a href="../Page/黃鐙輝.md" title="wikilink">黃鐙輝</a>、<a href="../Page/焦糖哥哥.md" title="wikilink">焦糖哥哥</a>、<a href="../Page/李千娜.md" title="wikilink">李千娜</a>、<a href="../Page/蔡旻佑.md" title="wikilink">蔡旻佑</a></p></td>
<td><p>女主角</p></td>
</tr>
<tr class="odd">
<td><p>2012年5月11日</p></td>
<td><p><a href="../Page/白天的星星.md" title="wikilink">白天的星星</a></p></td>
<td><p>小薰─莉慕伊</p></td>
<td><p><a href="../Page/林美秀.md" title="wikilink">林美秀</a>、<a href="../Page/紀亞文.md" title="wikilink">紀亞文</a>、<a href="../Page/陳慕義.md" title="wikilink">陳慕義</a></p></td>
<td><p>女配角</p></td>
</tr>
<tr class="even">
<td><p>2012年6月5日</p></td>
<td><p><a href="../Page/寶島雙雄.md" title="wikilink">寶島雙雄</a></p></td>
<td><p>小薰─女保安<br />
丫頭─纪念品售货员<br />
子庭─槟榔西施</p></td>
<td><p><a href="../Page/房祖名.md" title="wikilink">房祖名</a>、<a href="../Page/夏雨.md" title="wikilink">夏雨</a>、<a href="../Page/陳漢典.md" title="wikilink">陳漢典</a>、<a href="../Page/錦榮.md" title="wikilink">錦榮</a></p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2012年12月7日</p></td>
<td><p><a href="../Page/命運狗不理.md" title="wikilink">命運狗不理</a></p></td>
<td><p>丫頭─護士大喬</p></td>
<td><p><a href="../Page/王柏杰.md" title="wikilink">王柏杰</a>、<a href="../Page/林若亚.md" title="wikilink">林若亚</a>、<a href="../Page/林盈臻.md" title="wikilink">大元</a>、<a href="../Page/袁艾菲.md" title="wikilink">袁艾菲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年9月18日</p></td>
<td><p><a href="../Page/一座城池.md" title="wikilink">一座城池</a></p></td>
<td><p>小薰─永久妹妹</p></td>
<td><p><a href="../Page/房祖名.md" title="wikilink">房祖名</a>、<a href="../Page/王太利.md" title="wikilink">王太利</a></p></td>
<td><p>女主角</p></td>
</tr>
</tbody>
</table>

### MV演出

<table>
<tbody>
<tr class="odd">
<td><p>發行日期</p></td>
<td><p>歌名</p></td>
<td><p>演出</p></td>
<td><p>收錄唱片</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p>2006年7月14日</p></td>
<td><center>
<p>我愛黑澀會</p></td>
<td><center>
<p>九妞妞</p></td>
<td><p>黑澀會美眉《<a href="../Page/九妞妞的迷你專輯列表#美眉私密日記：我愛黑澀會美眉.md" title="wikilink">美眉私密日記：我愛黑澀會美眉</a>》</p></td>
<td><p>Rap－<a href="../Page/陳建州.md" title="wikilink">黑人</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年12月15日</p></td>
<td><center>
<p>Shake It Baby</p></td>
<td><center>
<p>九妞妞</p></td>
<td><p>黑澀會美眉《<a href="../Page/九妞妞的迷你專輯列表#美眉私密的一天.md" title="wikilink">美眉私密的一天</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>晴天娃娃</p></td>
<td><center>
<p><a href="../Page/甜心轟炸機.md" title="wikilink">甜心轟炸機</a></p></td>
<td><p>《<a href="../Page/葡萄園之戀.md" title="wikilink">葡萄園之戀</a>》片尾曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>Shining Kiss</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>123木頭人</p></td>
<td><center>
<p><a href="../Page/粉紅高壓電.md" title="wikilink">粉紅高壓電</a></p></td>
<td><p>《<a href="../Page/葡萄園之戀.md" title="wikilink">葡萄園之戀</a>》片尾曲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>我要愛的好</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年5月15日</p></td>
<td><center>
<p>甜甜圈</p></td>
<td><center>
<p><a href="../Page/黃瀞怡.md" title="wikilink">小薰</a></p></td>
<td><p>《<a href="../Page/18禁不禁.md" title="wikilink">18禁不禁電視原聲帶</a>》</p></td>
<td><p>與<a href="../Page/翁瑞迪.md" title="wikilink">阿本合唱</a><br />
《<a href="../Page/18禁不禁.md" title="wikilink">18禁不禁</a>》片尾曲</p></td>
</tr>
<tr class="odd">
<td><p>2007年6月7日</p></td>
<td><center>
<p>幸福的泡泡</p></td>
<td><center>
<p>九妞妞</p></td>
<td><p>黑澀會美眉《<a href="../Page/九妞妞的迷你專輯列表#美眉私密Party.md" title="wikilink">美眉私密Party</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年8月31日</p></td>
<td><center>
<p>黑糖秀</p></td>
<td><center>
<p>九妞妞</p></td>
<td><p>黑澀會美眉、Lollipop棒棒堂<br />
《<a href="../Page/黑糖瑪奇朵#電視原聲帶.md" title="wikilink">黑糖瑪奇朵電視原聲帶</a>》</p></td>
<td><p>與<a href="../Page/Lollipop棒棒堂.md" title="wikilink">Lollipop棒棒堂合唱</a><br />
《<a href="../Page/黑糖瑪奇朵.md" title="wikilink">黑糖瑪奇朵</a>》片頭曲(《<a href="../Page/黑糖瑪奇朵.md" title="wikilink">黑糖瑪奇朵</a>》片段)</p></td>
</tr>
<tr class="odd">
<td><p>2008年8月8日</p></td>
<td><center>
<p>叫姊姊</p></td>
<td><center>
<p>八妞妞</p></td>
<td><p>黑Girl《<a href="../Page/黑Girl首張同名專輯.md" title="wikilink">黑Girl首張同名專輯</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>女生</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年8月18日</p></td>
<td><center>
<p>OOXX</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年8月25日</p></td>
<td><center>
<p>哈庫吶瑪塔塔</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年10月18日</p></td>
<td><center>
<p>Hey Girl</p></td>
<td><center>
<p>三小妞</p></td>
<td><p>《<a href="../Page/Hey_Girl.md" title="wikilink">Hey Girl</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>是情人也是朋友</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 參演其他歌手MV

<table>
<tbody>
<tr class="odd">
<td><p>發行日期</p></td>
<td><p>歌名</p></td>
<td><p>演出</p></td>
<td><p>收錄唱片</p></td>
</tr>
<tr class="even">
<td><p>1999年8月</p></td>
<td><center>
<p>遊戲愛情<br />
(<a href="../Page/歐漢聲.md" title="wikilink">歐漢聲獨唱歌曲</a>)</p></td>
<td><center>
<p><a href="../Page/黃暐婷.md" title="wikilink">Apple</a></p></td>
<td><p><a href="../Page/羅密歐_(團體).md" title="wikilink">羅密歐</a>《動起來》</p></td>
</tr>
<tr class="odd">
<td><p>2004年9月6日</p></td>
<td><center>
<p>Do That to Me One More Time</p></td>
<td><center>
<p><a href="../Page/周宜霈.md" title="wikilink">大牙</a></p></td>
<td><p><a href="../Page/潘瑋柏.md" title="wikilink">潘瑋柏</a>《<a href="../Page/Wu_Ha.md" title="wikilink">Wu Ha</a>》</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><center>
<p>光</p></td>
<td><center>
<p><a href="../Page/黃暐婷.md" title="wikilink">Apple</a></p></td>
<td><p><a href="../Page/劉若英.md" title="wikilink">劉若英</a>《一整夜》</p></td>
</tr>
<tr class="odd">
<td><p>2005年6月3日</p></td>
<td><center>
<p>專屬密碼</p></td>
<td><center>
<p><a href="../Page/張甯兒.md" title="wikilink">甯兒</a></p></td>
<td><p><a href="../Page/黃義達.md" title="wikilink">黃義達</a>《專屬密碼》</p></td>
</tr>
<tr class="even">
<td><p>2005年7月8日</p></td>
<td><center>
<p>一指神功</p></td>
<td><center>
<p><a href="../Page/黃暐婷.md" title="wikilink">Apple</a></p></td>
<td><p><a href="../Page/潘瑋柏.md" title="wikilink">潘瑋柏</a>《高手》</p></td>
</tr>
<tr class="odd">
<td><p>2005年12月16日</p></td>
<td><center>
<p>宇宙</p></td>
<td><center>
<p><a href="../Page/周宜霈.md" title="wikilink">大牙</a></p></td>
<td><p><a href="../Page/展翼樂團.md" title="wikilink">展翼樂團</a>《愛的坦克》</p></td>
</tr>
<tr class="even">
<td><p>2006年3月17日</p></td>
<td><center>
<p>慶祝</p></td>
<td><center>
<p><a href="../Page/張甯兒.md" title="wikilink">甯兒</a>、<a href="../Page/王婧喬.md" title="wikilink">小蠻</a></p></td>
<td><p><a href="../Page/楊丞琳.md" title="wikilink">楊丞琳</a>《遇上愛》</p></td>
</tr>
<tr class="odd">
<td><p>2006年10月13日</p></td>
<td><center>
<p>男傭</p></td>
<td><center>
<p><a href="../Page/吳映潔.md" title="wikilink">GEMMA</a>、<a href="../Page/郭婕祈.md" title="wikilink">MeiMei</a>、<a href="../Page/詹子晴.md" title="wikilink">丫頭</a>、<a href="../Page/黃暐婷.md" title="wikilink">Apple</a>、<a href="../Page/蔡玓彤.md" title="wikilink">貝童彤</a></p></td>
<td><p><a href="../Page/吳克群.md" title="wikilink">吳克群</a>《將軍令》</p></td>
</tr>
<tr class="even">
<td><p>2008年11月14日</p></td>
<td><center>
<p>慢半拍</p></td>
<td><center>
<p><a href="../Page/黃瀞怡.md" title="wikilink">小薰</a></p></td>
<td><p><a href="../Page/黃靖倫.md" title="wikilink">黃靖倫</a>《倫語錄WISH YOUR LOVE》</p></td>
</tr>
<tr class="odd">
<td><p>2009年9月11日</p></td>
<td><center>
<p>說說</p></td>
<td><center>
<p><a href="../Page/張甯兒.md" title="wikilink">甯兒</a></p></td>
<td><p><a href="../Page/Lollipop棒棒堂.md" title="wikilink">Lollipop棒棒堂</a>《我是傳奇》</p></td>
</tr>
<tr class="even">
<td><p>2012年10月30日</p></td>
<td><center>
<p>操場上的夏天</p></td>
<td><center>
<p><a href="../Page/黃瀞怡.md" title="wikilink">小薰</a></p></td>
<td><p><a href="../Page/薛嘯秋.md" title="wikilink">薛嘯秋</a>《獨奏者的秘密》</p></td>
</tr>
<tr class="odd">
<td><center>
<p>眼淚</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 商演活動

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>活動</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td><ul>
<li>范瑋琪-我們的紀念日演唱會（擔任演唱會特別來賓）</li>
<li>[V]Power音樂風暴演唱會</li>
<li>臺北最HIGH新年城跨年晚會</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>2007</strong></p></td>
<td><ul>
<li>高雄La Tea樂派對 　　</li>
<li>MTV樂翻天演唱會</li>
<li>第13屆上海華語音樂榜中榜（表演嘉賓）　　</li>
<li>台客搖滾嘉年華（表演嘉賓） 　　</li>
<li>金鐘獎星光大道 （特別來賓）　</li>
<li>香港觀塘APM商場“黑澀會美眉聖誕Encore Show” 　　</li>
<li>香港觀塘APM商場“平安夜倒數音樂會”（特別嘉賓） 　　</li>
<li>香港大埔超級城“聖誕星躍馬戲團”</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>2008</strong></p></td>
<td><ul>
<li>棒棒堂哪里怕臺北小巨蛋演唱會 （擔任演唱會特別來賓） 　　</li>
<li>香港西九龍中天地「演藝界512關愛行動」大匯演 　</li>
<li>臺灣 捷運·愛·關懷銀髮族音樂晚會 （表演嘉賓） 　　</li>
<li>臺北第四十三屆金鐘獎表演嘉賓（與模範七棒） 　　</li>
<li>范瑋琪“我們是朋友范瑋琪2008巡迴演唱會”（擔任演唱會嘉賓）　</li>
<li>[V]Power愛音樂演唱會</li>
<li>香港屯門市廣場「璀璨閃耀平安夜倒數派對」</li>
<li>「就是愛台中．就是要跨年！2009台中市跨年晚會」（表演嘉賓）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>2009</strong></p></td>
<td><ul>
<li>長沙一呼百應見面會</li>
<li>江門紅歌KTV簽售會</li>
<li>上海足球寶貝見面會</li>
<li>Outerspace開幕禮</li>
<li>上海鮮之每日C舞蹈大賽</li>
<li>廣州鮮之每日C舞蹈大賽</li>
<li>芬達搖搖汽水活動</li>
<li>奧海城X黑Girl簽書會</li>
<li>黑Girl Mini Live音樂會記者會</li>
<li>星空影展閉幕禮</li>
<li>黑澀會美眉無限FunFun派對</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><ul>
<li>香港城市大學歌唱比賽</li>
<li>香港奧海城親親海洋照相會</li>
<li>第十三屆華語榜中榜 (擔任星光大道主持人)</li>
<li>環球紅歌盛典</li>
<li>香港藍灣廣場 &lt;小型人·夢想起行&gt;照相會</li>
<li>樂巢酒吧南昌店新張派對</li>
<li>星空年度客户答谢會</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td><ul>
<li>香港上水廣場「至冧」媽咪五星級甜蜜盛宴</li>
<li>陝西把安康帶回家演唱會</li>
<li>MTV步步高音樂手機全球音樂之旅</li>
<li>第十五屆全球華語音樂榜中榜 ( 擔任星光大道主持人)</li>
<li>師鵬愛之經典北京演唱會</li>
<li>北京BlingBling奇緣派對</li>
<li>「杭州濱文店聖誕歌友會」新聞發布會</li>
<li>中國美女盛典擔任表演嘉賓及頒獎人</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong>2012</strong></p></td>
<td><ul>
<li>昆明南亞之門都市印象大型群星演 （與<a href="../Page/Hit-5.md" title="wikilink">HIT-5合唱戀愛達人</a>）</li>
<li>香港國際影視展</li>
<li>澳門威尼斯第16屆全球華語榜中榜（擔任嘉賓及主持人）</li>
<li>第十一屆中國·桐鄉菊花節開幕式時尚盛典(擔任表演嘉賓)</li>
<li>蘇州中韓群星演唱會</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>2013</strong></p></td>
<td><ul>
<li>澳門「潮拜MUSIC演唱會」</li>
<li>四川宜賓演唱會</li>
<li>孝昌「金上海时代天街」明星演唱會</li>
</ul></td>
</tr>
</tbody>
</table>

## 外訪事件

### 香港

2007年11月18日，《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》**八位美眉**抵達[香港為商場主持聖誕亮燈儀式](../Page/香港.md "wikilink")。有近300名Fans到機場接機，當**黑澀會美眉**一眾成員步出閘口時，眾多Fans情緒失控，一湧而上，以致有Fans跌倒及甩鞋底，情況十分混亂。\[7\]

### 廣州

為慶祝《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》節目官網落戶[網易娛樂](../Page/網易.md "wikilink")，網易娛樂特邀《**我愛黑澀會**》**八妞妞**前往[廣州](../Page/廣州.md "wikilink")，展開為期五天的中國大陆行。**八妞妞**抵達[廣州機場時](../Page/廣州白雲國際機場.md "wikilink")，還引來數十名粉絲接機，有人穿上自製《**我愛黑澀會**》制服以示支持，也有人送上禮物和公仔給妞妞。

## 參考資料

## 內部連結

  - [Channel V](../Page/Channel_V.md "wikilink")
  - [星空傳媒 (經紀部)](../Page/星空家族.md "wikilink")
  - [模范棒棒堂](../Page/模范棒棒堂.md "wikilink")
  - [我愛黑澀會](../Page/我愛黑澀會.md "wikilink")
  - [黑澀會美眉](../Page/黑澀會美眉.md "wikilink")
  - [黑澀會二軍](../Page/黑澀會二軍.md "wikilink")

## 外部連結

  - [群石國際有限公司](http://www.mstones.com.tw/artists.php?cate=11&classid=12&kindid=22)
  - [藝房紫(北京)文化傳播有限公司](https://web.archive.org/web/20140110222220/http://onehouse.cn/)

[H黒](../Category/臺灣女子演唱團體.md "wikilink")
[H黑](../Category/我愛黑澀會.md "wikilink")
[\*](../Category/黑Girl.md "wikilink")

1.  如有改用本名活動，仍會標記黑Girl時期的藝名。
2.
3.
4.
5.
6.  [丫頭微博 "每一件事情我們都是聽公司的安排！勇兔還是有錄黑澀會！她是很認真努力的好女孩！希望大家還是繼續支持勇兔唷！新來的女生叫庭庭
    (子庭)！很平易近人可愛的女孩！大家多多給他鼓勵和支持唷！謝謝大家"(9月17日 19:56)](http://t.sina.com.cn/1746089664/wr0nRpEvGg#a_comment)
7.