**武义**（或作**颁义**；919年四月-921年正月）是[南吳高祖](../Page/南吳.md "wikilink")[楊隆演的](../Page/楊隆演.md "wikilink")[年號](../Page/年號.md "wikilink")，也是南吴政权自己的第一个年号。之前南吴年号一直沿用[唐朝的年号](../Page/唐朝.md "wikilink")。共计3年。

武义二年五月南吳睿帝[楊溥即位沿用](../Page/楊溥.md "wikilink")\[1\]。

## 紀年

| 武义                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 919年                           | 920年                           | 921年                           |
| [干支](../Page/干支纪年.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 同期存在的其他政权年号
      - [貞明](../Page/貞明_\(朱友貞\).md "wikilink")（915年十一月至921年四月）：後梁—朱友貞之年號
      - [天佑](../Page/天祐_\(李存勗\).md "wikilink")（909年至923年）：晉—[李存勗之年號](../Page/李存勗.md "wikilink")
      - [天佑](../Page/天祐_\(李茂貞\).md "wikilink")（907年四月至924年）：[岐](../Page/岐.md "wikilink")—[李茂貞之年號](../Page/李茂貞.md "wikilink")
      - [乾亨](../Page/乾亨_\(劉龑\).md "wikilink")（917年七月至925年十一月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [乾德](../Page/乾德_\(王衍\).md "wikilink")（919年正月至924年十二月）：前蜀—[王衍之年號](../Page/王衍_\(前蜀\).md "wikilink")
      - [神冊](../Page/神冊.md "wikilink")（916年十二月至922年正月）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機之年號](../Page/耶律阿保機.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗太祖](../Page/高麗.md "wikilink")[王建之年號](../Page/高麗太祖.md "wikilink")
      - [延喜](../Page/延喜.md "wikilink")（901年-922年）：[平安時代](../Page/平安時代.md "wikilink")[醍醐天皇之年號](../Page/醍醐天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:南吴年号](../Category/南吴年号.md "wikilink")
[Category:910年代中国政治](../Category/910年代中国政治.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月，144 ISBN 7101025129