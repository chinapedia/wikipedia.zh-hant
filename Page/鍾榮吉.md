**鍾榮吉**（），臺灣[高雄縣](../Page/高雄縣.md "wikilink")[美濃鎮](../Page/美濃區.md "wikilink")（今[高雄市美濃區](../Page/高雄市.md "wikilink")）[客家人](../Page/客家.md "wikilink")，前任[中華民國](../Page/中華民國.md "wikilink")[立法院副院長](../Page/立法院副院長.md "wikilink")。

## 簡歷

鍾榮吉原為國民黨倚重的人物，但在[蕭萬長任行政院長期內不獲重用](../Page/蕭萬長.md "wikilink")，僅被委任為行政院[政務委員](../Page/政務委員_\(中華民國\).md "wikilink")，憤而在[2000年中華民國總統大選中支持脫離](../Page/2000年中華民國總統大選.md "wikilink")[國民黨參選的](../Page/國民黨.md "wikilink")[宋楚瑜](../Page/宋楚瑜.md "wikilink")。鍾榮吉是在當次選舉中，表態支持[宋楚瑜的最高職級的政務官](../Page/宋楚瑜.md "wikilink")。

鍾榮吉在親民黨成立後被宋楚瑜委予重任，成為[親民黨秘書長](../Page/親民黨.md "wikilink")，地位比起一路跟隨宋楚瑜的[吳容明](../Page/吳容明.md "wikilink")（曾任[中華民國考試院副院長](../Page/中華民國考試院.md "wikilink")）、[夏龍及](../Page/夏龍.md "wikilink")[馬傑明等更高](../Page/馬傑明.md "wikilink")。2002年，因[高雄市議會議長賄選案中](../Page/高雄市.md "wikilink")，親民黨人受賄而支持[朱安雄當選議長而辭職](../Page/朱安雄.md "wikilink")。2005年，因為國親兩黨合作氣氛迷糊，在親民黨鐵定與國民黨合作後，被親民黨立法院黨團推舉為副院長人選，獲國民黨支持，結果當選。

## 得獎

  - 臺灣省政府新聞評論獎

## 家庭成員

鍾榮吉的胞侄[鍾紹和曾是](../Page/鍾紹和.md "wikilink")[國親聯盟](../Page/國親聯盟.md "wikilink")[高雄縣第一選區的立法委員](../Page/高雄縣.md "wikilink")，叔侄曾一度同為高雄客家的立法院代表。

## 外部連結

|- |colspan="3"
style="text-align:center;"|**[Seal_of_the_Legislative_Yuan.svg](https://zh.wikipedia.org/wiki/File:Seal_of_the_Legislative_Yuan.svg "fig:Seal_of_the_Legislative_Yuan.svg")[立法院](../Page/立法院.md "wikilink")**
|-           [R榮](../Page/category:鍾姓.md "wikilink")

[Category:立法院副院長](../Category/立法院副院長.md "wikilink")
[Category:行政院政務委員](../Category/行政院政務委員.md "wikilink")
[Category:第一屆監察委員](../Category/第一屆監察委員.md "wikilink")
[Category:親民黨秘書長](../Category/親民黨秘書長.md "wikilink")
[Category:親民黨黨員](../Category/親民黨黨員.md "wikilink")
[Category:中國國民黨副秘書長](../Category/中國國民黨副秘書長.md "wikilink")
[Category:中國國民黨臺灣省黨部主任委員](../Category/中國國民黨臺灣省黨部主任委員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:中華民國編輯](../Category/中華民國編輯.md "wikilink")
[Category:聯合報記者](../Category/聯合報記者.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:美濃人](../Category/美濃人.md "wikilink")
[C](../Category/客家裔臺灣人.md "wikilink")