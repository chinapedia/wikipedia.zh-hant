**科技預測**是指[人類對](../Page/人類.md "wikilink")[未來](../Page/未來.md "wikilink")[科技發展的估計](../Page/科技.md "wikilink")。這些估計通常都是按當時的科技發展作出推算。然而，由於科技的發展進度難以作出準確的估計，因此很多時候都很難完全準確，有時更會出現偏差。

## 現代的科技預測

2006年11月，科學期刊《[新科學家](../Page/新科學家.md "wikilink")》以慶祝創辦50年，邀請全球超過40名頂尖[科學家](../Page/科學家.md "wikilink")，預測2056年的科技發展。當中的預測包括：

  - [人類可與](../Page/人類.md "wikilink")[猴子及](../Page/猴子.md "wikilink")[金魚](../Page/金魚.md "wikilink")[說話](../Page/說話.md "wikilink")
  - 發現[外星生物屍體](../Page/外星生物.md "wikilink")
  - 把[火星作](../Page/火星.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")
  - 人類[器官由](../Page/器官.md "wikilink")[農場製造](../Page/農場.md "wikilink")
  - 發明令身體復原的[藥物](../Page/藥物.md "wikilink")
  - 破解[宇宙起源及](../Page/宇宙.md "wikilink")[大爆炸之謎](../Page/大爆炸.md "wikilink")

## 歷史上的科技預測

在歷史上，不少科學家甚至重要人物均作出科技預測，但甚少準確，不少為低估了科技的發展，也有對科技發展作出過於樂觀的估計。較著名的過去錯誤的科技預測計有：

  - 1878年，[美國](../Page/美國.md "wikilink")[電報公司](../Page/電報.md "wikilink")[西聯電報預測](../Page/西聯電報.md "wikilink")[電話缺點太多](../Page/電話.md "wikilink")，不能夠作為正式[通訊工具](../Page/通訊.md "wikilink")
  - 1901年，[天文學家](../Page/天文學.md "wikilink")[紐康認為](../Page/紐康.md "wikilink")[機械不可能在空中飛行](../Page/機械.md "wikilink")，因為機械重於[空氣](../Page/空氣.md "wikilink")
  - 1932年，著名科學家[愛因斯坦指出](../Page/愛因斯坦.md "wikilink")，沒有證據顯示[核能能夠被獲得](../Page/核能.md "wikilink")，因為需要將[原子分解](../Page/原子.md "wikilink")
  - 1936年，《[紐約時報](../Page/紐約時報.md "wikilink")》估計[火箭無法離開](../Page/火箭.md "wikilink")[地球](../Page/地球.md "wikilink")[大氣層](../Page/大氣層.md "wikilink")
  - 1939年，[英國首相](../Page/英國首相.md "wikilink")[邱吉爾推斷](../Page/邱吉爾.md "wikilink")，[核能不能製造大殺傷力的](../Page/核能.md "wikilink")[武器](../Page/武器.md "wikilink")
  - 1946年，[電影製作人](../Page/電影.md "wikilink")[柴納克認為](../Page/柴納克.md "wikilink")[電視很快被淘汰](../Page/電視.md "wikilink")，因為觀眾每晚盯著電視會很疲倦
  - 1949年，科學期刊《[大眾機械](../Page/大眾機械.md "wikilink")》預測，未來[電腦重](../Page/電腦.md "wikilink")1.5[噸](../Page/噸.md "wikilink")，有1,000條[真空管](../Page/真空管.md "wikilink")
  - 1954年，[美國國家癌病學院認為](../Page/美國國家癌病學院.md "wikilink")，[吸煙在導致](../Page/吸煙.md "wikilink")[肺癌上只有無足輕重的角色](../Page/肺癌.md "wikilink")
  - 1957年，[天文學家](../Page/天文學.md "wikilink")[瓊斯稱人類登陸](../Page/瓊斯.md "wikilink")[月球遙遙無期](../Page/月球.md "wikilink")，並指[太空旅行是癡心妄想](../Page/太空旅行.md "wikilink")
  - 1965年，[人工智能創始人](../Page/人工智能.md "wikilink")[西蒙預測](../Page/西蒙.md "wikilink")，機器於1985年便可代替人類做任何事
  - 1981年，[微軟創辦人](../Page/微軟.md "wikilink")[蓋茨估計](../Page/蓋茨.md "wikilink")，沒有人需要使用超過637[KB的](../Page/KB.md "wikilink")[個人電腦](../Page/個人電腦.md "wikilink")

## 參考資料

[分类:新兴技术](../Page/分类:新兴技术.md "wikilink")

[Category:技術總論](../Category/技術總論.md "wikilink")
[Category:未來](../Category/未來.md "wikilink")