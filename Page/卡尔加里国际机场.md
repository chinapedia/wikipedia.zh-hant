**卡尔加里国际机场**（[英语](../Page/英语.md "wikilink")：****；[IATA](../Page/IATA.md "wikilink")：YYC，[ICAO](../Page/ICAO.md "wikilink")：CYYC）是一座服务于[加拿大](../Page/加拿大.md "wikilink")[阿尔伯塔省](../Page/阿尔伯塔.md "wikilink")[卡尔加里市及周边地区的国际机场](../Page/卡尔加里.md "wikilink")。它坐落于卡尔加里市东北部，距市中心17公里。按乘客排名，它是加拿大最繁忙的机场之一，在2017年共服务了1626万乘客。同时，按飞机起降排名，它则是加拿大第四繁忙的机场，共242,658班次。卡尔加里国际机场提供定期直达航班飞至[欧洲](../Page/欧洲.md "wikilink")、[美国](../Page/美国.md "wikilink")、[中美洲与加拿大的主要城市](../Page/中美洲.md "wikilink")；另有货运与包机飞往[亚洲](../Page/亚洲.md "wikilink")。卡尔加里国际机场是加拿大西部的枢纽机场，[西捷航空的总部便在这里](../Page/西捷航空.md "wikilink")。同样，它也是[加拿大航空的一个枢纽](../Page/加拿大航空.md "wikilink")。卡尔加里是全加拿大八座设有[美国入境预检设施的机场之一](../Page/美國境外入境審查.md "wikilink")。

## 航空公司與航点

## 外部链接

  - [卡尔加里国际机场网页](http://www.calgaryairport.com/)

[分類:美國境外入境審查機場](../Page/分類:美國境外入境審查機場.md "wikilink")

[C](../Category/加拿大機場.md "wikilink")
[Category:1966年启用的机场](../Category/1966年启用的机场.md "wikilink")