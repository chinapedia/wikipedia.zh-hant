{{ infobox football league | name = 卡塔爾星級足球聯賽 | another_name = Qatar
Stars League（QSL）
Q-League | image = | pixels = | country =  | confed =
[AFC](../Page/亞洲足球聯合總會.md "wikilink")（[亞洲](../Page/亞洲.md "wikilink")）
| founded = 1963年 | first = | teams = 12 隊 | relegation =
[卡塔爾足球乙級聯賽](../Page/卡塔爾足球乙級聯賽.md "wikilink") | level =
第 1 級 | domestic_cup = [卡塔爾酋長盃](../Page/卡塔爾酋長盃.md "wikilink")
[卡塔爾盃](../Page/卡塔爾盃.md "wikilink")
[謝赫賈西姆盃](../Page/卡塔爾謝赫賈西姆盃.md "wikilink") | international_cup =
[亞洲聯賽冠軍盃](../Page/亞洲聯賽冠軍盃.md "wikilink") | season =
2018–19 | champions = [艾薩德](../Page/薩德體育俱樂部.md "wikilink") |
most_successful_club = [艾薩德](../Page/薩德體育俱樂部.md "wikilink") |
no_of_titles = 14 次 | website =
[qsl.com.qa](https://web.archive.org/web/20130102013432/http://www.qsl.com.qa/default.html)
| current = }}

**卡塔爾星級足球聯賽**（；），是[卡塔爾的足球聯賽中最高級別](../Page/卡塔爾.md "wikilink")。聯賽成立於1963年，目前有
12 隊參與角逐。

## 聯賽制度

卡塔爾星級足球聯賽目前有 12 隊角逐。每季星級足球聯賽前 4
名球隊，均可參與[卡塔爾盃](../Page/卡塔爾盃.md "wikilink")。

## 參賽球隊

2019–20年卡塔爾星級足球聯賽參賽隊伍共有 12 支。

| 中文名稱                                           | 英文名稱                    | 所在城市                             | 上季成績              |
| ---------------------------------------------- | ----------------------- | -------------------------------- | ----------------- |
| [艾薩德](../Page/薩德體育俱樂部.md "wikilink")           | Al Sadd SC              | [多哈](../Page/多哈.md "wikilink")   | 第 1 位             |
| [艾杜哈尼](../Page/艾杜哈尼體育會.md "wikilink")          | Al-Duhail SC            | [多哈](../Page/多哈.md "wikilink")   | 第 2 位             |
| [艾塞利亞](../Page/艾塞利亞體育會.md "wikilink")          | Al-Sailiya SC           | [多哈](../Page/多哈.md "wikilink")   | 第 3 位             |
| [艾雷恩](../Page/賴揚體育俱樂部.md "wikilink")           | Al-Rayan SC             | [賴揚](../Page/賴揚.md "wikilink")   | 第 4 位             |
| [多哈艾阿里](../Page/阿赫利體育俱樂部_\(多哈\).md "wikilink") | Al Ahli SC              | [多哈](../Page/多哈.md "wikilink")   | 第 5 位             |
| [多哈艾阿拉比](../Page/多哈艾阿拉比體育俱樂部.md "wikilink")    | Al-Arabi SC             | [多哈](../Page/多哈.md "wikilink")   | 第 6 位             |
| [艾沙哈尼亞](../Page/艾沙哈尼亞體育會.md "wikilink")        | Al-Shahania SC          | [多哈](../Page/多哈.md "wikilink")   | 第 7 位             |
| [艾加拉法](../Page/加拉法體育俱樂部.md "wikilink")         | Al-Gharafa Sports Club  | [多哈](../Page/多哈.md "wikilink")   | 第 8 位             |
| [烏姆沙拉爾](../Page/烏姆沙拉爾體育俱樂部.md "wikilink")      | Umm Salal SC            | [多哈](../Page/多哈.md "wikilink")   | 第 9 位             |
| [艾哥爾](../Page/艾哥爾體育會.md "wikilink")            | Al-Khor SC              | [艾豪爾](../Page/艾豪爾.md "wikilink") | 第 10 位            |
| [卡塔爾SC](../Page/卡塔爾體育會.md "wikilink")          | Qatar SC                | [多哈](../Page/多哈.md "wikilink")   | 第 11 位            |
|                                                |                         |                                  | 甲組，第 1 位 \<\!--|- |
| [艾馬希亞](../Page/艾馬希亞體育會.md "wikilink")          | Al-Markhiya Sports Club | [多哈](../Page/多哈.md "wikilink")   | 甲組，第 2 位--\>      |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

  - 備註：

<!-- end list -->

  - 「艾馬菲」於 1966–67 球季解散

  - 「艾奧魯巴」與「卡塔爾足球會」於 1971–72
    球季合併為「艾斯迪拿」，並於1981年更名為[卡塔爾SC](../Page/卡塔爾體育會.md "wikilink")

  - 「伊迪哈德」於2004年更名為[艾加拉法](../Page/艾加拉法體育會.md "wikilink")

  - 「歷基韋亞」於2017年更名為[艾杜哈尼](../Page/艾杜哈尼體育會.md "wikilink")

## 奪冠次數

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/薩德體育俱樂部.md" title="wikilink">艾薩德</a></p></td>
<td><center>
<p>14</p></td>
<td><p>1971–72, 1973–74, 1978–79, 1979–80, 1980–81, 1986–87, 1987–88, 1989–89, 1999–2000, 2003–04,<br />
2005–06, 2006–07, 2012–13, 2018–19</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡塔爾體育會.md" title="wikilink">卡塔爾SC</a></p></td>
<td><center>
<p>8</p></td>
<td><p>1966–67, 1967–68, 1968–69, 1969–70, 1970–71, 1972–73, 1976–77, 2002–03</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賴揚體育俱樂部.md" title="wikilink">艾雷恩</a></p></td>
<td><center>
<p>8</p></td>
<td><p>1975–76, 1977–78, 1981–82, 1983–84, 1985–86, 1989–90, 1994–95, 2015–16</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/多哈艾阿拉比體育俱樂部.md" title="wikilink">多哈艾阿拉比</a></p></td>
<td><center>
<p>7</p></td>
<td><p>1982–83, 1984–85, 1990–91, 1992–93, 1993–94, 1995–96, 1996–97</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾加拉法體育會.md" title="wikilink">艾加拉法</a></p></td>
<td><center>
<p>7</p></td>
<td><p>1991–92, 1997–98, 2001–02, 2004–05, 2007–08, 2008–09, 2009–10</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾杜哈尼體育會.md" title="wikilink">艾杜哈尼</a></p></td>
<td><center>
<p>6</p></td>
<td><p>2010–11, 2011–12, 2013–14, 2014–15, 2016–17, 2017–18</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾馬菲體育俱樂部.md" title="wikilink">艾馬菲</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1963–64, 1964–65, 1965–66</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾華卡拉體育會.md" title="wikilink">艾華卡拉</a></p></td>
<td><center>
<p>2</p></td>
<td><p>1998–99, 2000–01</p></td>
</tr>
</tbody>
</table>

## 參考

## 外部連結

  - [Qatar League - goalzz.com](http://www.goalzz.com/main.aspx?c=2443)

[Qatar](../Category/亞洲各國足球聯賽.md "wikilink")
[亚](../Category/國家頂級足球聯賽.md "wikilink")
[Category:亞洲頂級足球聯賽](../Category/亞洲頂級足球聯賽.md "wikilink")
[Category:卡達足球賽事](../Category/卡達足球賽事.md "wikilink")

1.