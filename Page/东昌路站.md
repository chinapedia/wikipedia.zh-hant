**东昌路站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[世纪大道](../Page/世纪大道.md "wikilink")[东昌路](../Page/东昌路_\(上海\).md "wikilink")，为[上海轨道交通2号线的地下](../Page/上海轨道交通2号线.md "wikilink")[岛式車站](../Page/岛式站台.md "wikilink")。

## 公交换乘

82、119、181、314、338、339、454、583、584、607、630、783、787、791、792、798、818、870、961、977、981、985、隧道三线、沪南线、浦东21路

## 车站出口

  - 1号口：世纪大道北侧，浦东南路东
    [<File:Lift.svg>](https://zh.wikipedia.org/wiki/File:Lift.svg "fig:File:Lift.svg")
  - 2号口：世纪大道北侧，南泉北路（原名崂山西路）西
  - 3号口：世纪大道南侧，南泉北路（原名崂山西路）西
  - 4号口：世纪大道南侧，浦东南路东

注：[<File:Lift.svg>](https://zh.wikipedia.org/wiki/File:Lift.svg "fig:File:Lift.svg")
设有无障碍电梯

## 邻近车站

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:1999年啟用的鐵路車站](../Category/1999年啟用的鐵路車站.md "wikilink")
[Category:以街道命名的上海軌道交通車站](../Category/以街道命名的上海軌道交通車站.md "wikilink")