**S1**（），是日本的[成人视频制造商](../Page/成人视频制造商.md "wikilink")。全名是**S1 No. 1
Style**（）。

隸屬於[北都集團](../Page/北都集團.md "wikilink")，旗下[女優以人氣女優與美形女優著稱](../Page/AV女優.md "wikilink")\[1\]。2004年11月11日，S1加入AV業界，地點在[石川縣](../Page/石川縣.md "wikilink")[加賀市美岬町](../Page/加賀市.md "wikilink")1-1
AVC活動中心，最早的女優有[蒼井空](../Page/蒼井空.md "wikilink")、[小倉愛莉絲](../Page/小倉愛莉絲.md "wikilink")、[小川流果](../Page/小川流果.md "wikilink")。由於S1公司財力雄厚，旗下擁有大量優質女優。

## 主要系列

### S1

  - 交わる体液、濃密セックス
  - 新人NO.1STYLE
  - 犯された●●シリーズ
  - 風俗嬢シリーズ
  - イカセシリーズ
  - 乱交シリーズ
  - ピストンシリーズ
  - 潮吹きシリーズ
  - おっぱいシリーズ
  - 女捜査官シリーズ
  - 巨根シリーズ
  - 超高級風俗嬢
  - 完全緊縛された●●
  - 完全固定シリーズ

### True Real Love (暫停)

  - ドキドキ初体験セックス
  - ○○アイドル
  - 制服ロマンス
  - 萌え萌えコスプレ7
  - ○○のヒミツの性感スイッチ

### S1 DASH

2012年6月推出的數位配送專門品牌。[DMM.R18優先壟斷配送](../Page/DMM.com.md "wikilink")，不久過後也釋出了完整版。總結有以下AV女優演出。

[春菜華](../Page/春菜華.md "wikilink")、[瑠川里菜](../Page/瑠川里菜.md "wikilink")、[倉多真央](../Page/倉多真央.md "wikilink")、[堀咲莉娅](../Page/堀咲莉娅.md "wikilink")、[香西咲](../Page/香西咲.md "wikilink")、[上原保奈美](../Page/上原保奈美.md "wikilink")、[吉澤明步](../Page/吉澤明步.md "wikilink")、[君野歩美](../Page/君野歩美.md "wikilink")

## 專屬女優

**符號意義**

|   |                       |
| - | --------------------- |
| ☆ | AV出道                  |
| ★ | 銷售出道                  |
| ◆ | 從移籍                   |
| ◎ | 「True Real Love」出道及發表 |

### 現在專屬女優

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/吉澤明步.md" title="wikilink">吉澤明步</a></p></td>
<td></td>
<td><p>2007年1月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/夢乃愛華.md" title="wikilink">夢乃愛華</a></p></td>
<td></td>
<td><p>2013年5月7日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/星野娜美.md" title="wikilink">星野娜美</a></p></td>
<td></td>
<td><p>2013年8月7日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/奥田咲.md" title="wikilink">奥田咲</a></p></td>
<td></td>
<td><p>2013年8月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/天使萌.md" title="wikilink">天使萌</a></p></td>
<td></td>
<td><p>2014年7月7日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/小島南.md" title="wikilink">小島南</a></p></td>
<td></td>
<td><p>2014年3月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/葵_(AV女優).md" title="wikilink">葵</a></p></td>
<td></td>
<td><p>2014年10月8日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/葵司.md" title="wikilink">葵司</a></p></td>
<td></td>
<td><p>2015年6月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/橋本有菜.md" title="wikilink">橋本有菜</a></p></td>
<td></td>
<td><p>2016年3月19日</p></td>
</tr>
<tr class="even">
<td><p>◆</p></td>
<td><p><a href="../Page/羽咲美晴.md" title="wikilink">羽咲美晴</a></p></td>
<td></td>
<td><p>2016年6月7日</p></td>
</tr>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/三上悠亜.md" title="wikilink">三上悠亜</a></p></td>
<td></td>
<td><p>2016年11月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/吉高寧寧.md" title="wikilink">吉高寧寧</a></p></td>
<td></td>
<td><p>2017年9月1日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/架乃由羅.md" title="wikilink">架乃由羅</a></p></td>
<td></td>
<td><p>2017年11月19日</p></td>
</tr>
<tr class="even">
<td><p>|<a href="../Page/彩美旬果.md" title="wikilink">彩美旬果</a></p></td>
<td></td>
<td><p>2018年5月7日</p></td>
<td><p>從<a href="../Page/PRESTIGE.md" title="wikilink">PRESTIGE移籍</a>。</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/坂道美琉.md" title="wikilink">坂道美琉</a></p></td>
<td><p>出生年月日不明</p></td>
<td><p>2018年8月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/夕美紫苑.md" title="wikilink">夕美紫苑</a></p></td>
<td></td>
<td><p>2018年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/星宮一花.md" title="wikilink">星宮一花</a></p></td>
<td></td>
<td><p>2018年10月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

### 過去專屬女優

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2004年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/蒼井空.md" title="wikilink">蒼井そら</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/Maika.md" title="wikilink">MEW</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/小倉ありす.md" title="wikilink">小倉ありす</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/小川流果.md" title="wikilink">小川流果</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/原千尋.md" title="wikilink">原千尋</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/かわい果南.md" title="wikilink">かわい果南</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/白石みさと.md" title="wikilink">白石みさと</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/早咲まみ.md" title="wikilink">早咲まみ</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/日野鈴.md" title="wikilink">日野鈴</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/しじみ_(女優).md" title="wikilink">持田茜</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/米倉夏弥.md" title="wikilink">米倉夏弥</a></p></td>
<td></td>
<td><p>2004年11月11日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2005年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/来栖りお.md" title="wikilink">来栖りお</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/藤沢マリ.md" title="wikilink">藤沢マリ</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/小春_(AV女優).md" title="wikilink">小春</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/小森美樹.md" title="wikilink">小森美樹</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/桃子美優.md" title="wikilink">桃子美優</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/平山千里.md" title="wikilink">平山千里</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/涼果りん.md" title="wikilink">涼果りん</a></p></td>
<td></td>
<td><p>2005年3月11日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/下村愛.md" title="wikilink">穂花</a></p></td>
<td></td>
<td><p>2005年3月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/宮崎あいか.md" title="wikilink">宮崎あいか</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/半沢あい.md" title="wikilink">半沢あい</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p>|<a href="../Page/美咲沙耶.md" title="wikilink">美咲沙耶</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/心愛_(AV女優).md" title="wikilink">心愛</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/水野ひかり.md" title="wikilink">水野ひかり</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/宝乃ありか.md" title="wikilink">宝乃ありか</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/あおりんご.md" title="wikilink">あおりんご</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/瞳ケイ.md" title="wikilink">瞳ケイ</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/ANNA_(AV女優).md" title="wikilink">ANNA</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/安部ちなつ.md" title="wikilink">安部ちなつ</a></p></td>
<td></td>
<td><p>2005年4月11日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/愛田由.md" title="wikilink">愛田由</a></p></td>
<td></td>
<td><p>2005年5月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/松坂みるく.md" title="wikilink">松坂みるく</a></p></td>
<td></td>
<td><p>2005年5月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/日向ゆず葉.md" title="wikilink">日向ゆず葉</a></p></td>
<td></td>
<td><p>2005年5月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/姫野りむ.md" title="wikilink">姫野りむ</a></p></td>
<td></td>
<td><p>2005年5月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/水原みなみ.md" title="wikilink">水原みなみ</a></p></td>
<td></td>
<td><p>2005年5月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/椎名ゆめ.md" title="wikilink">椎名ゆめ</a></p></td>
<td></td>
<td><p>2005年5月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/北島優.md" title="wikilink">北島優</a></p></td>
<td></td>
<td><p>2005年6月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/加瀬あゆむ.md" title="wikilink">加瀬あゆむ</a></p></td>
<td></td>
<td><p>2005年6月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/原田裕希.md" title="wikilink">原田裕希</a></p></td>
<td></td>
<td><p>2005年6月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/中川瞳.md" title="wikilink">中川瞳</a></p></td>
<td></td>
<td><p>2005年6月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/藤崎夕凪.md" title="wikilink">藤崎夕凪</a></p></td>
<td></td>
<td><p>2005年6月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/小泉优香.md" title="wikilink">小泉优香</a></p></td>
<td></td>
<td><p>2005年7月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/当真ゆき.md" title="wikilink">当真ゆき</a></p></td>
<td></td>
<td><p>2005年7月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/酒菜いるか.md" title="wikilink">酒菜いるか</a></p></td>
<td></td>
<td><p>2005年7月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/雪乃まひる.md" title="wikilink">雪乃まひる</a></p></td>
<td></td>
<td><p>2005年8月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/黑木麻衣.md" title="wikilink">花野真衣</a></p></td>
<td></td>
<td><p>2005年8月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/ほしのまき.md" title="wikilink">詩織</a></p></td>
<td></td>
<td><p>2005年9月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/なぎさ_(AV女優).md" title="wikilink">なぎさ</a></p></td>
<td></td>
<td><p>2005年9月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/葉月奈穗.md" title="wikilink">葉月奈穗</a></p></td>
<td></td>
<td><p>2005年9月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/相川奈々.md" title="wikilink">相川奈々</a></p></td>
<td></td>
<td><p>2005年9月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/小澤瑪麗亞.md" title="wikilink">小澤瑪麗亞</a></p></td>
<td></td>
<td><p>2005年10月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/麻美由真.md" title="wikilink">麻美由真</a></p></td>
<td></td>
<td><p>2005年11月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/樱纱希.md" title="wikilink">さくら紗希</a></p></td>
<td></td>
<td><p>2005年12月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/藍ゆうき.md" title="wikilink">藍ゆうき</a></p></td>
<td></td>
<td><p>2005年12月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/一色あずさ.md" title="wikilink">一色あずさ</a></p></td>
<td></td>
<td><p>2005年12月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2006年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/青木鈴.md" title="wikilink">青木鈴</a></p></td>
<td></td>
<td><p>2006年5月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/安達真実.md" title="wikilink">安達真実</a></p></td>
<td></td>
<td><p>2006年11月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/天宫真奈美.md" title="wikilink">天宫真奈美</a></p></td>
<td></td>
<td><p>2006年2月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/しいないおり.md" title="wikilink">しいないおり</a></p></td>
<td></td>
<td><p>2006年4月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/綾瀬はるな.md" title="wikilink">綾瀬はるな</a></p></td>
<td></td>
<td><p>2006年9月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/杏珠.md" title="wikilink">杏珠</a></p></td>
<td></td>
<td><p>2006年2月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/純名もも.md" title="wikilink">純名もも</a></p></td>
<td></td>
<td><p>2006年4月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/井上詩織.md" title="wikilink">井上詩織</a></p></td>
<td></td>
<td><p>2006年11月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/伊澤千夏.md" title="wikilink">伊澤千夏</a></p></td>
<td></td>
<td><p>2006年11月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/石川えみ.md" title="wikilink">石川えみ</a></p></td>
<td></td>
<td><p>2006年2月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/蛯原まい.md" title="wikilink">蛯原まい</a></p></td>
<td></td>
<td><p>2006年8月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/園原りか.md" title="wikilink">園原りか</a></p></td>
<td></td>
<td><p>2006年7月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/小川阿佐美.md" title="wikilink">小川阿佐美</a></p></td>
<td></td>
<td><p>2006年11月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/乙葉ゆい.md" title="wikilink">乙葉ゆい</a></p></td>
<td></td>
<td><p>2006年9月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/晶愛麗.md" title="wikilink">大沢佑香</a></p></td>
<td></td>
<td><p>2006年4月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/北島玲.md" title="wikilink">北島玲</a></p></td>
<td></td>
<td><p>2006年10月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/木村那美.md" title="wikilink">木村那美</a></p></td>
<td></td>
<td><p>2006年4月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/倉田ゆい.md" title="wikilink">倉田ゆい</a></p></td>
<td></td>
<td><p>2006年4月7日|</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/片瀬まこ.md" title="wikilink">片瀬まこ</a></p></td>
<td></td>
<td><p>2006年12月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/加藤ツバキ.md" title="wikilink">加藤ツバキ</a></p></td>
<td></td>
<td><p>2006年4月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/かわい愛.md" title="wikilink">かわい愛</a></p></td>
<td></td>
<td><p>2006年4月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/川村カンナ.md" title="wikilink">川村カンナ</a></p></td>
<td></td>
<td><p>2006年2月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/神咲アンナ.md" title="wikilink">神咲アンナ</a></p></td>
<td></td>
<td><p>2006年4月19日|</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/滝沢優季.md" title="wikilink">滝沢優季</a></p></td>
<td></td>
<td><p>2006年10月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/千秋のどか.md" title="wikilink">千秋のどか</a></p></td>
<td></td>
<td><p>2006年12月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/千春_(AV女優).md" title="wikilink">千春</a></p></td>
<td></td>
<td><p>2006年4月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/蕾_(AV女優).md" title="wikilink">蕾</a></p></td>
<td></td>
<td><p>2006年4月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/仲村りお.md" title="wikilink">仲村りお</a></p></td>
<td></td>
<td><p>2006年9月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/鳴海すず.md" title="wikilink">鳴海すず</a></p></td>
<td></td>
<td><p>2006年4月7日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/西野翔.md" title="wikilink">西野翔</a></p></td>
<td></td>
<td><p>2006年6月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/果梨.md" title="wikilink">果梨</a></p></td>
<td></td>
<td><p>2006年9月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/はるる.md" title="wikilink">はるる</a></p></td>
<td></td>
<td><p>2006年1月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/遙惠美.md" title="wikilink">遙惠美</a></p></td>
<td></td>
<td><p>2006年8月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/深津亜季.md" title="wikilink">深津亜季</a></p></td>
<td></td>
<td><p>2006年2月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/福沢あや.md" title="wikilink">福沢あや</a></p></td>
<td></td>
<td><p>2006年10月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/藤咲あいな.md" title="wikilink">藤咲あいな</a></p></td>
<td></td>
<td><p>2006年10月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/星野みく_(AV女優).md" title="wikilink">星野みく</a></p></td>
<td></td>
<td><p>2006年4月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/松嶋れいな.md" title="wikilink">松嶋れいな</a></p></td>
<td></td>
<td><p>2006年6月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/真中かおり.md" title="wikilink">真中かおり</a></p></td>
<td></td>
<td><p>2006年9月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/美月れいな.md" title="wikilink">美月れいな</a></p></td>
<td></td>
<td><p>2006年10月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/みなみ優羽.md" title="wikilink">みなみ優羽</a></p></td>
<td></td>
<td><p>2006年12月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/宮澤ケイト.md" title="wikilink">宮澤ケイト</a></p></td>
<td></td>
<td><p>2006年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/宮路ナオミ.md" title="wikilink">宮路ナオミ</a></p></td>
<td></td>
<td><p>2006年12月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/MO☆MO.md" title="wikilink">MO☆MO</a></p></td>
<td></td>
<td><p>2006年12月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/安田みらい.md" title="wikilink">安田みらい</a></p></td>
<td></td>
<td><p>2006年1月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/山本美姫.md" title="wikilink">山本美姫</a></p></td>
<td></td>
<td><p>2006年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/吉乃ひとみ_(AV女優).md" title="wikilink">吉乃ひとみ</a></p></td>
<td></td>
<td><p>2006年4月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2007年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/愛玲.md" title="wikilink">愛玲</a></p></td>
<td></td>
<td><p>2007年2月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/赤西涼.md" title="wikilink">赤西涼</a></p></td>
<td></td>
<td><p>2007年3月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/秋月まりん.md" title="wikilink">秋月まりん</a></p></td>
<td></td>
<td><p>2007年1月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/綾瀬ひめ.md" title="wikilink">綾瀬ひめ</a></p></td>
<td></td>
<td><p>2007年2月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/安西あき.md" title="wikilink">安西あき</a></p></td>
<td></td>
<td><p>2007年2月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/岩佐めい.md" title="wikilink">岩佐めい</a></p></td>
<td></td>
<td><p>2007年4月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/来未エリカ.md" title="wikilink">来未エリカ</a></p></td>
<td></td>
<td><p>2007年11月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/黒木なほ.md" title="wikilink">黒木なほ</a></p></td>
<td></td>
<td><p>2007年4月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/霞理沙.md" title="wikilink">霞理沙</a></p></td>
<td></td>
<td><p>2007年6月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/夏川亜咲.md" title="wikilink">夏川亜咲</a></p></td>
<td></td>
<td><p>2007年7月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/七海.md" title="wikilink">七海</a></p></td>
<td></td>
<td><p>2007年7月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/麻倉ジェシカ.md" title="wikilink">麻倉ジェシカ</a></p></td>
<td></td>
<td><p>2007年4月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/花井美沙.md" title="wikilink">花井美沙</a></p></td>
<td></td>
<td><p>2007年11月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/初音實.md" title="wikilink">初音實</a></p></td>
<td></td>
<td><p>2007年11月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/浜名優衣.md" title="wikilink">浜名優衣</a></p></td>
<td></td>
<td><p>2007年10月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/藤倉れいみ.md" title="wikilink">藤倉れいみ</a></p></td>
<td></td>
<td><p>2007年8月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/前乃さとみ.md" title="wikilink">前乃さとみ</a></p></td>
<td></td>
<td><p>2007年7月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/岬里沙.md" title="wikilink">岬リサ</a></p></td>
<td></td>
<td><p>2007年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/水城奈緒.md" title="wikilink">水城奈緒</a></p></td>
<td></td>
<td><p>2007年7月19日</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p><a href="../Page/朝美穗香.md" title="wikilink">朝美穗香</a></p></td>
<td></td>
<td><p>2007年8月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/美優千奈.md" title="wikilink">美優千奈</a></p></td>
<td></td>
<td><p>2007年2月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/モカ_(AV女優).md" title="wikilink">モカ</a></p></td>
<td></td>
<td><p>2007年9月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/桃瀬えみる.md" title="wikilink">桃瀬えみる</a></p></td>
<td></td>
<td><p>2007年9月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/夢見ほのか.md" title="wikilink">夢見ほのか</a></p></td>
<td></td>
<td><p>2007年8月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/吉野さくら.md" title="wikilink">吉野さくら</a></p></td>
<td></td>
<td><p>2007年12月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/若宮莉那.md" title="wikilink">若宮莉那</a></p></td>
<td></td>
<td><p>2007年2月7日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2008年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/相内リカ.md" title="wikilink">相内リカ</a></p></td>
<td></td>
<td><p>2008年4月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/あのあるる.md" title="wikilink">あのあるる</a></p></td>
<td></td>
<td><p>2008年9月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/鮎川あゆみ.md" title="wikilink">鮎川あゆみ</a></p></td>
<td></td>
<td><p>2008年7月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/青木純奈.md" title="wikilink">青木純奈</a></p></td>
<td></td>
<td><p>2008年12月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/杏樹紗奈.md" title="wikilink">杏樹紗奈</a></p></td>
<td></td>
<td><p>2008年3月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/KEI_(AV女優).md" title="wikilink">KEI</a></p></td>
<td></td>
<td><p>2008年9月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/雨宮琴音.md" title="wikilink">雨宮琴音</a></p></td>
<td></td>
<td><p>2008年10月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/倉咲ゆう.md" title="wikilink">倉咲ゆう</a></p></td>
<td></td>
<td><p>2008年11月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/AMI.md" title="wikilink">Ami</a></p></td>
<td></td>
<td><p>2008年4月7日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/佐山愛.md" title="wikilink">佐山愛</a></p></td>
<td></td>
<td><p>2008年7月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/きこうでんみさ.md" title="wikilink">きこうでんみさ</a></p></td>
<td></td>
<td><p>2008年6月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/渋谷梨果.md" title="wikilink">渋谷梨果</a></p></td>
<td></td>
<td><p>2008年3月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/竹内結.md" title="wikilink">竹内結</a></p></td>
<td></td>
<td><p>2008年8月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/长泽梓.md" title="wikilink">长泽梓</a></p></td>
<td></td>
<td><p>2008年12月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/長月ラム.md" title="wikilink">長月ラム</a></p></td>
<td></td>
<td><p>2008年8月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/仲村知夏_(AV女優).md" title="wikilink">仲村知夏</a></p></td>
<td></td>
<td><p>2008年1月7日</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p><a href="../Page/南波杏.md" title="wikilink">南波杏</a></p></td>
<td></td>
<td><p>2008年2月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/蓮井志帆.md" title="wikilink">蓮井志帆</a></p></td>
<td></td>
<td><p>2008年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/春風えみ.md" title="wikilink">春風えみ</a></p></td>
<td></td>
<td><p>2008年9月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/日向ひかる.md" title="wikilink">日向ひかる</a></p></td>
<td></td>
<td><p>2008年9月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/平子エミリ.md" title="wikilink">平子エミリ</a></p></td>
<td></td>
<td><p>2008年2月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/松島楓.md" title="wikilink">松島楓</a></p></td>
<td></td>
<td><p>2008年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/女池さゆり.md" title="wikilink">女池さゆり</a></p></td>
<td></td>
<td><p>2008年11月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/百華リオン.md" title="wikilink">百華リオン</a></p></td>
<td></td>
<td><p>2008年6月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/Rio_(AV女優).md" title="wikilink">Rio</a></p></td>
<td></td>
<td><p>2008年2月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/吉原ミィナ.md" title="wikilink">吉原ミィナ</a></p></td>
<td></td>
<td><p>2008年7月18日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2009年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/P醬.md" title="wikilink">風子</a></p></td>
<td></td>
<td><p>2009年5月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/有沢実紗.md" title="wikilink">有沢実紗</a></p></td>
<td></td>
<td><p>2009年1月7日</p></td>
</tr>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/藤浦惠.md" title="wikilink">藤浦惠</a></p></td>
<td></td>
<td><p>2009年3月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/卯月麻衣.md" title="wikilink">卯月麻衣</a></p></td>
<td></td>
<td><p>2009年6月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/大空花音.md" title="wikilink">大空花音</a></p></td>
<td></td>
<td><p>2009年10月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/桐原绘里香.md" title="wikilink">桐原绘里香</a></p></td>
<td></td>
<td><p>2009年5月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/佐伯さき.md" title="wikilink">佐伯さき</a></p></td>
<td></td>
<td><p>2009年12月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/藤本莉娜.md" title="wikilink">藤本莉娜</a></p></td>
<td></td>
<td><p>2009年12月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/咲美りんね.md" title="wikilink">咲美りんね</a></p></td>
<td></td>
<td><p>2009年1月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/櫻心美.md" title="wikilink">櫻心美</a></p></td>
<td></td>
<td><p>2009年4月19日</p></td>
</tr>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/月見栞.md" title="wikilink">月見栞</a></p></td>
<td></td>
<td><p>2009年6月7日</p></td>
</tr>
<tr class="even">
<td><p>◆</p></td>
<td><p><a href="../Page/戸田アイラ.md" title="wikilink">戸田アイラ</a></p></td>
<td></td>
<td><p>2009年11月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/音羽レオン.md" title="wikilink">音羽レオン</a></p></td>
<td></td>
<td><p>2009年7月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/惠佳.md" title="wikilink">恵けい</a></p></td>
<td></td>
<td><p>2009年2月7日</p></td>
</tr>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/佳山三花.md" title="wikilink">佳山三花</a></p></td>
<td></td>
<td><p>2009年8月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2010年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/真木こころ.md" title="wikilink">真木こころ</a></p></td>
<td></td>
<td><p>2010年12月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/南ともか.md" title="wikilink">南ともか</a></p></td>
<td></td>
<td><p>2010年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>◎</p></td>
<td><p><a href="../Page/瑠川里菜.md" title="wikilink">瑠川里菜</a></p></td>
<td></td>
<td><p>2010年5月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/柚本紗希.md" title="wikilink">柚本紗希</a></p></td>
<td></td>
<td><p>2010年3月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/青山ローラ.md" title="wikilink">青山ローラ</a></p></td>
<td></td>
<td><p>2010年11月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/梓ユイ.md" title="wikilink">梓ユイ</a></p></td>
<td></td>
<td><p>2010年9月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/あやせめる.md" title="wikilink">あやせめる</a></p></td>
<td></td>
<td><p>2010年7月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/小沢真理奈.md" title="wikilink">小沢真理奈</a></p></td>
<td></td>
<td><p>2010年2月7日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/黑川綺羅.md" title="wikilink">黑川綺羅</a></p></td>
<td></td>
<td><p>2010年10月19日</p></td>
</tr>
<tr class="even">
<td><p>★</p></td>
<td><p><a href="../Page/希志愛野.md" title="wikilink">希志愛野</a></p></td>
<td></td>
<td><p>2010年2月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/北川ゆり.md" title="wikilink">北川ゆり</a></p></td>
<td></td>
<td><p>2010年8月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/西条琉璃.md" title="wikilink">西条琉璃</a></p></td>
<td></td>
<td><p>2010年9月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/白瀬エリナ.md" title="wikilink">白瀬エリナ</a></p></td>
<td></td>
<td><p>2010年4月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/とこな由羽.md" title="wikilink">とこな由羽</a></p></td>
<td></td>
<td><p>2010年6月19日</p></td>
</tr>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/七海なな.md" title="wikilink">七海なな</a></p></td>
<td></td>
<td><p>2010年7月7日</p></td>
</tr>
<tr class="even">
<td><p>◆</p></td>
<td><p><a href="../Page/春菜華.md" title="wikilink">春菜華</a></p></td>
<td></td>
<td><p>2010年12月7日</p></td>
</tr>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/藤間ゆかり.md" title="wikilink">藤間ゆかり</a></p></td>
<td></td>
<td><p>2010年3月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/立花紗耶.md" title="wikilink">立花紗耶</a></p></td>
<td></td>
<td><p>2010年6月7日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2011年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/二階堂ソフィア.md" title="wikilink">二階堂ソフィア</a></p></td>
<td></td>
<td><p>2011年1月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/沖田杏梨.md" title="wikilink">沖田杏梨</a></p></td>
<td></td>
<td><p>2011年2月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/菜月アンナ.md" title="wikilink">菜月アンナ</a></p></td>
<td></td>
<td><p>2011年3月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/上原保奈美.md" title="wikilink">上原保奈美</a></p></td>
<td></td>
<td><p>2011年4月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/森川真羽.md" title="wikilink">森川真羽</a></p></td>
<td></td>
<td><p>2011年4月19日</p></td>
</tr>
<tr class="even">
<td><p>◎</p></td>
<td><p><a href="../Page/市川まほ.md" title="wikilink">市川まほ</a></p></td>
<td></td>
<td><p>2011年5月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/水川菜々子.md" title="wikilink">水川菜々子</a></p></td>
<td></td>
<td><p>2011年6月19日</p></td>
</tr>
<tr class="even">
<td><p>◎</p></td>
<td><p><a href="../Page/渚ことみ.md" title="wikilink">渚ことみ</a></p></td>
<td></td>
<td><p>2011年8月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/梨々衣.md" title="wikilink">梨々衣</a></p></td>
<td></td>
<td><p>2011年9月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/源みいな.md" title="wikilink">源みいな</a></p></td>
<td></td>
<td><p>2011年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/堀咲りあ.md" title="wikilink">堀咲りあ</a></p></td>
<td></td>
<td><p>2011年11月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/御厨あおい.md" title="wikilink">御厨あおい</a></p></td>
<td></td>
<td><p>2011年11月19日</p></td>
</tr>
<tr class="odd">
<td><p>◎</p></td>
<td><p><a href="../Page/星野飛鳥.md" title="wikilink">星野飛鳥</a></p></td>
<td></td>
<td><p>2011年12月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/昂热.md" title="wikilink">アンジェ</a></p></td>
<td></td>
<td><p>2011年12月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2012年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>★</p></td>
<td><p><a href="../Page/篠原杏.md" title="wikilink">篠原杏</a></p></td>
<td></td>
<td><p>2012年1月7日</p></td>
</tr>
<tr class="even">
<td><p>◆</p></td>
<td><p><a href="../Page/香西咲.md" title="wikilink">香西咲</a></p></td>
<td></td>
<td><p>2012年1月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/鶴田かな.md" title="wikilink">鶴田かな</a></p></td>
<td></td>
<td><p>2012年2月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p>絵里奈モア</p></td>
<td></td>
<td><p>2012年2月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/芦名尤莉亚.md" title="wikilink">芦名尤莉亚</a></p></td>
<td></td>
<td><p>2012年3月7日</p></td>
</tr>
<tr class="even">
<td><p>◎</p></td>
<td><p><a href="../Page/夏風まりん.md" title="wikilink">夏風まりん</a></p></td>
<td></td>
<td><p>2012年3月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/仲里紗羽.md" title="wikilink">仲里紗羽</a></p></td>
<td></td>
<td><p>2012年4月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/姫野ゆうり.md" title="wikilink">姫野ゆうり</a></p></td>
<td></td>
<td><p>2012年4月19日</p></td>
</tr>
<tr class="odd">
<td><p>◎</p></td>
<td><p><a href="../Page/中野えりか.md" title="wikilink">中野えりか</a></p></td>
<td></td>
<td><p>2012年7月7日</p></td>
</tr>
<tr class="even">
<td><p>◆</p></td>
<td><p><a href="../Page/长谷川理穗.md" title="wikilink">长谷川理穗</a></p></td>
<td></td>
<td><p>2012年7月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/倉多まお.md" title="wikilink">倉多まお</a></p></td>
<td></td>
<td><p>2012年8月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/乃ノ原みき.md" title="wikilink">乃ノ原みき</a></p></td>
<td></td>
<td><p>2012年9月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/本田岬.md" title="wikilink">本田岬</a></p></td>
<td></td>
<td><p>2012年10月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/きみの歩美.md" title="wikilink">きみの歩美</a></p></td>
<td></td>
<td><p>2012年11月8日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/伊藤りな.md" title="wikilink">伊藤りな</a></p></td>
<td></td>
<td><p>2012年12月7日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2013年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/緒川里緒.md" title="wikilink">緒川里緒</a></p></td>
<td></td>
<td><p>2013年2月7日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/蒂亞_(AV女優).md" title="wikilink">蒂亞</a></p></td>
<td></td>
<td><p>2013年5月20日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/明日花綺羅.md" title="wikilink">明日花綺羅</a></p></td>
<td></td>
<td><p>2013年7月19日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/RION.md" title="wikilink">RION</a></p></td>
<td></td>
<td><p>2015年10月7日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/坂咲美穗.md" title="wikilink">坂口美穗乃</a></p></td>
<td></td>
<td><p>2013年10月7日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/秋山祥子.md" title="wikilink">秋山祥子</a></p></td>
<td></td>
<td><p>2013年11月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2014年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/庵野杏.md" title="wikilink">庵野杏</a></p></td>
<td></td>
<td><p>2014年4月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/香椎梨亞.md" title="wikilink">香椎梨亞</a></p></td>
<td></td>
<td><p>2014年11月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/森春流.md" title="wikilink">森春流</a></p></td>
<td></td>
<td><p>2014年12月19日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2015年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/美里有紗.md" title="wikilink">美里有紗</a></p></td>
<td></td>
<td><p>2015年1月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/吉川愛美.md" title="wikilink">吉川愛美</a></p></td>
<td></td>
<td><p>2015年2月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/菜菜葉.md" title="wikilink">菜菜葉</a></p></td>
<td></td>
<td><p>2015年6月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/園田美櫻.md" title="wikilink">園田美櫻</a></p></td>
<td></td>
<td><p>2015年8月1日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/小野寺梨紗.md" title="wikilink">小野寺梨紗</a></p></td>
<td></td>
<td><p>2015年11月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/相澤友梨奈.md" title="wikilink">相澤友梨奈</a></p></td>
<td></td>
<td><p>2015年12月19日</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2016年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/北川柚子.md" title="wikilink">北川柚子</a></p></td>
<td></td>
<td><p>2016年2月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/妃月留衣.md" title="wikilink">妃月留衣</a></p></td>
<td></td>
<td><p>2016年5月19日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/美竹鈴.md" title="wikilink">美竹鈴</a></p></td>
<td></td>
<td><p>2016年7月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/翼_(AV女優).md" title="wikilink">翼</a></p></td>
<td></td>
<td><p>2016年9月1日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/希崎潔西卡.md" title="wikilink">希崎潔西卡</a></p></td>
<td></td>
<td><p>2016年9月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/悠月安夏.md" title="wikilink">悠月安夏</a></p></td>
<td></td>
<td><p>2016年10月7日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/湊莉久.md" title="wikilink">湊莉久</a></p></td>
<td></td>
<td><p>2016年10月7日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/桃園未來.md" title="wikilink">桃園未來</a></p></td>
<td></td>
<td><p>2016年10月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/雛形久留美.md" title="wikilink">雛形久留美</a></p></td>
<td></td>
<td><p>2016年11月13日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/辻本杏.md" title="wikilink">辻本杏</a></p></td>
<td></td>
<td><p>2016年11月13日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/夏川明.md" title="wikilink">夏川明</a></p></td>
<td></td>
<td><p>2016年12月13日</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2017年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>◆</p></td>
<td><p><a href="../Page/松本菜奈實.md" title="wikilink">松本菜奈實</a></p></td>
<td></td>
<td><p>2017年6月7日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/日菜菜彩音.md" title="wikilink">日菜菜彩音</a></p></td>
<td></td>
<td><p>2017年6月19日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/深田奈奈.md" title="wikilink">深田奈奈</a></p></td>
<td></td>
<td><p>2017年9月25日</p></td>
</tr>
<tr class="even">
<td><p>◆</p></td>
<td><p><a href="../Page/松田美子.md" title="wikilink">松田美子</a></p></td>
<td></td>
<td><p>2017年10月7日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/風間莉娜.md" title="wikilink">風間莉娜</a></p></td>
<td></td>
<td><p>2017年10月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/YURI_(AV女優).md" title="wikilink">YURI</a></p></td>
<td></td>
<td><p>2017年12月7日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/鈴木心春.md" title="wikilink">鈴木心春</a></p></td>
<td></td>
<td><p>2017年12月13日</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

</div>

<div class="NavFrame" style="border:0;text-align:left;font-size:small">

<div class="NavHead">

2018年

</div>

<div class="NavContent">

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>加入S1時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/凜音桃花.md" title="wikilink">凜音桃花</a></p></td>
<td></td>
<td><p>2018年2月19日</p></td>
</tr>
<tr class="even">
<td><p>☆</p></td>
<td><p><a href="../Page/河北彩花.md" title="wikilink">河北彩花</a></p></td>
<td></td>
<td><p>2018年4月19日</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p><a href="../Page/音梓.md" title="wikilink">音梓</a></p></td>
<td><p>出生年月日不明</p></td>
<td><p>2018年7月7日</p></td>
</tr>
</tbody>
</table>

</div>

</div>

## 相關條目

  - [AV女優列表](../Page/AV女優列表.md "wikilink")
  - [成人視訊制造商](../Page/成人视频制造商.md "wikilink")

## 參考資料

## 外部連結

  -
  -
  -
  -
  - [S1
    DASH](http://www.dmm.co.jp/digital/videoa/s1dash/index_html/=/ch_navi=/)

  -
  - [現場KORIN TV【S1公關】](http://com.nicovideo.jp/community/co1169845) -
    niconico媒體

[Category:2004年成立的公司](../Category/2004年成立的公司.md "wikilink")
[Category:日本色情片公司](../Category/日本色情片公司.md "wikilink")
[Category:S1女優](../Category/S1女優.md "wikilink")

1.