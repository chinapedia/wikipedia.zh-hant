**白石市**（）是[宮城縣最南端的一市](../Page/宮城縣.md "wikilink")。面積286.47平方公里，總人口39121人。

## 地理

位于宮城縣內最南端，[白石川由西南向東北流過該市](../Page/白石川.md "wikilink")。

  - 山：[不忘山](../Page/不忘山.md "wikilink")（[藏王連峰南端的山峰](../Page/藏王連.md "wikilink")）、[鉢森山](../Page/鉢森山.md "wikilink")
  - 河川：白石川及其支流

## 历史

  - 1954年4月1日：[白石町](../Page/白石町_\(宮城縣\).md "wikilink")・[大平村](../Page/大平村_\(宮城縣\).md "wikilink")、[斎川村](../Page/斎川村.md "wikilink")、[越河村](../Page/越河村.md "wikilink")、[大鷹澤村](../Page/大鷹澤村.md "wikilink")、[白川村](../Page/白川村_\(宮城縣\).md "wikilink")、[福岡村合併](../Page/福岡村_\(宮城縣\).md "wikilink")，宮城縣第六個市誕生。
  - 1957年3月31日：[小原村編入](../Page/小原村_\(宮城縣\).md "wikilink")。

## 行政

  - 市長：[風間康静](../Page/風間康静.md "wikilink")（）2004年11月～

## 姊妹都市

### 日本國內

  - [札幌市](../Page/札幌市.md "wikilink")[白石區](../Page/白石區.md "wikilink")
  - [登別市](../Page/登別市.md "wikilink")（[北海道](../Page/北海道.md "wikilink")）
  - [海老名市](../Page/海老名市.md "wikilink")（[神奈川縣](../Page/神奈川縣.md "wikilink")）