TeamSpeak是一套[專有的](../Page/专有软件.md "wikilink")[VoIP軟體](../Page/IP电话.md "wikilink")，使用者可以經由它與其他使用者進行語音對話，很像[電話會議](../Page/電話會議.md "wikilink")。使用者可以戴上[耳機與](../Page/耳機.md "wikilink")[麥克風進行通話](../Page/麥克風.md "wikilink")。使用者可以經由客戶端軟體連線到指定的伺服器，在伺服器內的頻道進行通話。

通常TeamSpeak的使用者大多為多人連線遊戲的玩家，與同隊伍的玩家進行通訊。在遊戲的對戰方面，語音對話通訊具有競爭優勢。\[1\]

## 伺服器

TeamSpeak 3伺服器目前支援[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[Mac OS
X](../Page/Mac_OS_X.md "wikilink")、[Linux和](../Page/Linux.md "wikilink")[FreeBSD](../Page/FreeBSD.md "wikilink")，並使用基於[Web或](../Page/Web.md "wikilink")[Telnet的工具來控制伺服器的管理及設定](../Page/Telnet.md "wikilink")。伺服器是單獨運行在一台主機上，與用戶端分開。TeamSpeak
3支援虛擬伺服器個體化，這允許最多10個虛擬伺服器運行在同一個處理程序上。

## 第三方工具

可經由官方網站下載第三方應用程式。這些應用程式的特性在於，[客戶端的遊戲支援](../Page/客戶端.md "wikilink")，以及[伺服器的進階管理](../Page/伺服器.md "wikilink")。

## TeamSpeak 3

新版的TeamSpeak
\[2\]早在2004年起開始研發。重新設計許多新的功能與特性，有部分更新於2006年在研發人員的部落格內發佈。首次公開發佈TeamSpeak
3 SDK\[3\]的日期是2008年6月5日，其包含的元件還有MMO遊戲[Vendetta
Online的整合元件](../Page/Vendetta_Online.md "wikilink")。\[4\]測試版的TeamSpeak
3於2009年12月19日發佈。

## 相關條目

  - [Mumble](../Page/Mumble.md "wikilink")

  -
  -
  - [VoiceChatter](../Page/VoiceChatter.md "wikilink")

  - [VoIP軟體的比較](../Page/VoIP軟體的比較.md "wikilink")

## 參考資料

## 外部連結

  - TeamSpeak – [teamspeak.com](http://www.teamspeak.com/)
  - TeamSpeex –
    [savvy.nl/blog](https://archive.is/20051023002753/http://www.savvy.nl/blog/)

[Category:专有软件](../Category/专有软件.md "wikilink")
[Category:VoIP軟體](../Category/VoIP軟體.md "wikilink")
[Category:網際網路會議](../Category/網際網路會議.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")

1.
2.
3.
4.