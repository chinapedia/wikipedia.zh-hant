**Raphael**是[日本](../Page/日本.md "wikilink")[視覺系](../Page/視覺系.md "wikilink")[樂隊](../Page/樂隊.md "wikilink")，1997年成立，由[主唱](../Page/主唱.md "wikilink")[Yuki](../Page/Yuki.md "wikilink")、[貝斯手](../Page/貝斯手.md "wikilink")[Yukito](../Page/Yukito.md "wikilink")、[鼓手](../Page/鼓手.md "wikilink")[Hiro](../Page/Hiro.md "wikilink")、和[吉他手](../Page/吉他手.md "wikilink")[華月四人組成](../Page/華月.md "wikilink")。

2000年10月31日[團長華月因壓力過大誤服過量](../Page/團長.md "wikilink")[鎮定劑去世](../Page/鎮定劑.md "wikilink")，Raphael最終於2001年宣佈活動休止。

其後[貝斯手](../Page/貝斯手.md "wikilink")[Yukito便在名為](../Page/Yukito.md "wikilink")「[BLACK
LOVE](../Page/BLACK_LOVE.md "wikilink")」的樂團開始個人發展，而[Yuki與](../Page/Yuki.md "wikilink")[Hiro在Raphael活動休止前已經企劃結成的樂團](../Page/Hiro.md "wikilink")[Rice在Raphael活動休止後便也以](../Page/Rice.md "wikilink")「為了華月而唱」的名義發展至今。

2012年4月7日，Raphael宣布復活，並於10月31日及11月1日舉行兩場復活演出。

時隔3年半，Raphael在2016年再度展開活動，於4月7日亦即已故結他手華月的生忌舉行了紀念公演「蒼の邂逅」，並於5月23日至7月24日期間展開全國巡演「癒し小屋」\[1\]\[2\]。同年10月31日至11月1日，Raphael舉行了最後的兩場公演「悠久の檜舞台」，並於11月1日正式解散。
\[3\]

## 成員

  -
    **[Yuki](../Page/Yuki.md "wikilink")（[主唱](../Page/主唱.md "wikilink")）**
      -
        本名：櫻井 有紀
        生日：1981年7月11日
        出生地︰神奈川縣
    **[華月](../Page/華月.md "wikilink")（[吉他手](../Page/吉他手.md "wikilink")、[團長](../Page/團長.md "wikilink")）**
      -
        本名：渡邊 和樹
        生日：1981年4月7日
        出生地：東京都
    **[Yukito](../Page/Yukito.md "wikilink")（[貝斯手](../Page/貝斯手.md "wikilink")）**
      -
        本名：本田 之人
        生日：1981年5月23日
    **[Hiro](../Page/Hiro.md "wikilink")（[鼓手](../Page/鼓手.md "wikilink")）**
      -
        本名：村田 一弘
        生日：1981年11月17日
        出生地：神奈川縣

## 專輯

### 1998年

  - 4月7日發行首張自主盤Album《[Lilac](../Page/Lilac.md "wikilink")》
  - 8月1日發行自主盤Video《[Lilac-Vision Of
    Extremes](../Page/Lilac-Vision_Of_Extremes.md "wikilink")》
  - 11月1日發行自主盤Single 《[White Love
    Story](../Page/White_Love_Story.md "wikilink")》

### 1999年

  - 2月20日發行自主盤Single 《[Sick \~ xxx
    患者的病例卡](../Page/Sick_~_xxx_患者的病例卡.md "wikilink")》
  - 4月29日同日發售兩張自主盤Single《[比夢更美好](../Page/比夢更美好.md "wikilink")》和《[Sweet
    Romance](../Page/Sweet_Romance.md "wikilink")》
  - 7月23日發行首張正式Single《[只要有花開的生命](../Page/只要有花開的生命.md "wikilink")》
  - 10月1日發行第二張單曲《[Eternal
    wish\~無法傳達的妳](../Page/Eternal_wish~無法傳達的妳.md "wikilink")》
  - 11月20日發行第三張單曲《[Promise](../Page/Promise.md "wikilink")》
  - 12月1日發行首張正式Album《[Mind soap](../Page/Mind_soap.md "wikilink")》

### 2000年

  - 2月2日發行第四張Single《[Lost
    graduation](../Page/Lost_graduation.md "wikilink")》
  - 3月23日發行Mini Album《[卒業](../Page/卒業.md "wikilink")》
  - 5月24日發行Video《[Pictorial Poem](../Page/Pictorial_Poem.md "wikilink")》
  - 8月23日發行第五張Single《[Evergreen](../Page/Evergreen.md "wikilink")》、《[Raphael
    Special Live
    graduation\~2000.3.4日本武道館\~](../Page/Raphael_Special_Live_graduation~2000.3.4日本武道館~.md "wikilink")》
  - 11月1日 發行第六張Single《[秋風的狂詩曲](../Page/秋風的狂詩曲.md "wikilink")》

## 參考資料

## 外部連結

  - [raphael官網(2012年復活後架設)](http://www.raphael.jp)
  - [raphael官網(舊)](http://www.forlife.co.jp/raphael/fchp/flame.html)
  - [rice官網](https://web.archive.org/web/20070321083906/http://www.riceinfo.info/)
  - [2007年4月1日新開放的rice官網](http://rice.secret.jp/)

[Category:1997年成立的音樂團體](../Category/1997年成立的音樂團體.md "wikilink")
[Category:2001年解散的音樂團體](../Category/2001年解散的音樂團體.md "wikilink")
[Category:2012年復出的音樂團體](../Category/2012年復出的音樂團體.md "wikilink")
[Category:視覺系樂團](../Category/視覺系樂團.md "wikilink")
[Category:日本硬式搖滾樂團](../Category/日本硬式搖滾樂團.md "wikilink")
[Category:日本重金屬樂團](../Category/日本重金屬樂團.md "wikilink")
[Category:日本前衛搖滾樂團](../Category/日本前衛搖滾樂團.md "wikilink")

1.  [VROCKHK - 華月誕生公演
    Raphael宣佈迎向終幕](http://vrockhk.com/2016/04/19/%E8%8F%AF%E6%9C%88%E8%AA%95%E7%94%9F%E5%85%AC%E6%BC%94%E3%80%80raphael%E5%AE%A3%E4%BD%88%E8%BF%8E%E5%90%91%E7%B5%82%E5%B9%95/)
2.  [VROCKHK - Raphael 11月1日解散　Zepp
    Tokyo最後舞台](http://vrockhk.com/2016/05/24/raphael-11%E6%9C%881%E6%97%A5%E8%A7%A3%E6%95%A3%E3%80%80zepp-tokyo%E6%9C%80%E5%BE%8C%E8%88%9E%E5%8F%B0/)
3.  [VROCKHK - Raphael Live 2016「悠久の檜舞台」第弍夜黒中夢2016/11/1 @ Zepp
    Tokyo](http://vrockhk.com/2016/11/22/raphael/)