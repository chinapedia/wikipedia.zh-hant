**第十州**(波斯尼亚语：Kanton 10, Livanjski kanton；克罗地亚语：Hercegbosanska
županija)，又称**西波斯尼亞州**、**利夫诺州**，位於[波士尼亞與赫塞哥維納的西部](../Page/波士尼亞與赫塞哥維納.md "wikilink")，與該州所屬[波黑聯邦的](../Page/波黑聯邦.md "wikilink")[烏納-薩納州](../Page/烏納-薩納州.md "wikilink")、[西黑塞哥維納州及](../Page/西黑塞哥維納州.md "wikilink")[班雅盧卡區相鄰](../Page/班雅盧卡區.md "wikilink")。該州並與波斯尼亞另一政治實體[塞族共和國相鄰](../Page/塞族共和國.md "wikilink")，以及鄰國的[克羅地亞相鄰](../Page/克羅地亞.md "wikilink")。第十州总面积為4,934.1平方公里，而总人口為90,727（2013年）。

## 行政区划

第十州分为6个市镇。

| 市镇                                                         | 人口 \[1\] | 面积 (km<sup>2</sup>)\[2\] |
| ---------------------------------------------------------- | -------- | ------------------------ |
| [Bosansko Grahovo](../Page/Bosansko_Grahovo.md "wikilink") | 3,091    | 780.0                    |
| [德瓦尔](../Page/德瓦尔.md "wikilink")                           | 7,506    | 1030.0                   |
| [Glamoč](../Page/Glamoč.md "wikilink")                     | 4,038    | 1033.6                   |
| [库普雷斯](../Page/库普雷斯.md "wikilink")                         | 5,573    | 569.8                    |
| [利夫诺](../Page/利夫诺.md "wikilink")                           | 37,487   | 994.0                    |
| [多米斯拉夫格勒](../Page/多米斯拉夫格勒.md "wikilink")                   | 33,032   | 967.4                    |

## 参考文献

1.

2.