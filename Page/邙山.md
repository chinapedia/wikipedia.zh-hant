**邙山**，山名，又名**氓山**，古名**郏山**，又称**北邙**，俗称**邙岭**。位於[河南西部](../Page/河南.md "wikilink")，是秦嶺山脈的北支，距離[太行山四五百公里](../Page/太行山.md "wikilink")，靠近[黃河邊上](../Page/黃河.md "wikilink")，與[崤山互為犄角](../Page/崤山.md "wikilink")，古有天險之稱。

## 自然地理

邙山位于[河南省](../Page/河南.md "wikilink")[洛阳市北](../Page/洛阳.md "wikilink")，[黄河南岸](../Page/黄河.md "wikilink")，是[秦岭山脉的餘脉](../Page/秦岭.md "wikilink")，[崤山支脉](../Page/崤山.md "wikilink")。广义的邙山起自洛阳市北，沿黄河南岸绵延至[郑州市北的](../Page/郑州.md "wikilink")[广武山](../Page/广武山.md "wikilink")，长度100多公里。狭义的邙山仅指洛阳市以北的黄河与其支流[洛河的](../Page/洛河_\(黄河\).md "wikilink")[分水岭](../Page/分水岭.md "wikilink")。

## 人文古蹟

邙山海拔300米左右。邙山为黄土丘陵地，是洛阳北面的一道天然屏障，也是军事上的战略要地。最高峰为翠云峰，在今市区正北，上有唐玄元皇帝庙。

由于邙山山川绚丽，风光宜人，山虽不高，但土厚水低，宜于殡葬，所以邙山上多古代帝王陵墓，比较有名的有东周皇陵、东汉皇陵，曹魏、西晋和北魏时期的皇陵。不誇張的說，邙山自东汉以来就是洛阳人的墓地。《[后汉书](../Page/后汉书.md "wikilink")》卷十下《桓帝邓皇后纪》:
“诏废后，送暴室，以忧死。立七年，葬于北邙。”《西京杂记》載:
“何武葬北邙山薄龙坂，王嘉冢东北一里。”貞觀元年，[李勣愛女卒](../Page/李勣.md "wikilink")，葬北邙。故有謂：“生在蘇杭，葬在北邙”。\[1\]历代更無數詩咏邙山。晋代诗人[张载](../Page/張載_\(西晉\).md "wikilink")《七哀诗》-{云}-：“北邙何累累，高陵有四五。借问谁家坟，皆-{云}-汉世主。”[陶渊明](../Page/陶渊明.md "wikilink")《拟古诗》-{云}-:
“一旦百岁后，相与还北邙。”[白居易詩感慨道](../Page/白居易.md "wikilink"):“何事不隨東洛水，誰家又葬北邙山？”[沈佺期](../Page/沈佺期.md "wikilink")《邙山》-{云}-:
“北邙山上列坟茔，万古千秋对洛城。”[張籍有詩](../Page/張籍.md "wikilink")-{云}-:“洛陽北門北邙道，喪車轔轔入秋箪。”[王建](../Page/王建_\(唐朝\).md "wikilink")《北邙行》-{云}-:“北邙山頭少閑土，盡是洛陽人舊墓。舊墓人家歸葬多，堆著黄金無買處。”

清末建造[隴海鐵路](../Page/隴海鐵路.md "wikilink")，在[洛阳邙山毁损部分的唐代墓葬](../Page/洛阳.md "wikilink")，發現大量的[唐三彩](../Page/唐三彩.md "wikilink")[陶器](../Page/陶器.md "wikilink")。古董商将其运至北京，引起学者[王国维](../Page/王国维.md "wikilink")、[罗振玉等的高度重视](../Page/罗振玉.md "wikilink")。\[2\]现存有秦相[吕不韦](../Page/吕不韦.md "wikilink")、南朝[陈后主](../Page/陈后主.md "wikilink")、南唐[李后主](../Page/李后主.md "wikilink")、西晋司马氏、[汉光武帝刘秀的原陵](../Page/汉光武帝.md "wikilink")、唐朝诗人[杜甫](../Page/杜甫.md "wikilink")、大书法家[颜真卿等历代名人之墓](../Page/颜真卿.md "wikilink")。现在建有中国第一座古墓博物馆－[洛阳古墓博物馆](../Page/洛阳古墓博物馆.md "wikilink")。

### 歷史傳說

相传[老子曾在邙山炼丹](../Page/老子.md "wikilink")，山上建有[上清观以奉祀老子](../Page/上清观.md "wikilink")。\[3\][张道陵亦在北邙山隐炼修道](../Page/张道陵.md "wikilink")，所以此山被视为[道教的发源地之一](../Page/道教.md "wikilink")。\[4\]附近还有道教寺观[吕祖庵](../Page/吕祖庵.md "wikilink")、[武则天避暑行宫](../Page/武则天避暑行宫.md "wikilink")、[中清宫](../Page/中清宫.md "wikilink")、[下清宫等古建筑](../Page/下清宫.md "wikilink")。[唐朝诗人](../Page/唐朝.md "wikilink")[张籍诗](../Page/张籍.md "wikilink")-{云}-：“人居朝市未解愁，请君暂向北邙游。”“[邙山晚眺](../Page/邙山晚眺.md "wikilink")”被誉为“[洛阳八景](../Page/洛阳八景.md "wikilink")”之一。唐、宋以降，每逢[重阳佳节](../Page/重阳.md "wikilink")，上邙山游览者络绎不绝。\[5\]

邙山的西端有[仰韶文化遗址](../Page/仰韶文化.md "wikilink")，这是新石器时期黄河中游地区人类文明的一个标志。

## 武俠小說中的邙山

邙山流傳最廣的傳說，當數現代武俠小說中的邙山派，其中又以[梁羽生小說為佼佼者](../Page/梁羽生.md "wikilink")，位於河南邙山玄女觀。

## 注釋

## 外部連結

  - [典故
    邙山](http://sou-yun.com/Query.aspx?type=allusion&id=620&key=北邙山&lang=t)
  - [洛陽邙山陵墓羣](http://big5.china.com.cn/culture/txt/2007-08/14/content_8681160.htm)

[Category:崤山](../Category/崤山.md "wikilink")
[Category:河南山峰](../Category/河南山峰.md "wikilink")
[Category:洛阳地理](../Category/洛阳地理.md "wikilink")

1.  朱介凡：《中華諺語志》卷9
2.  《孟津縣誌》：“在1920年前后南石山村有7位高姓民间艺人，以修复损坏的出土唐三彩为生，后来陶塑艺人高良田在自己多年修复古唐三彩的技艺上，首次复制唐三彩获得成功，成为高姓的家传秘技，当时用小窑烧制唐三彩也仅流传于高姓家族之间。”
3.  [康骈](../Page/康骈.md "wikilink")《剧谈录》记载:
    “东都北邙山有玄元观，南有老君庙，台殿高敞，下瞰伊洛。神仙泥塑之像，皆开元中杨惠之所制，奇巧精严，见者增敬。壁有吴道子画五圣真容及老子庙胡经事。丹青绝妙，古今无比。”
4.  《汉天师世家》卷二谓：永平二年，张道陵“以直言极谏科中之，拜江州令，谢官归洛阳北邙山，修炼三年，有白虎衔符座隅。”
5.  崇祯二年，重阳节，[王铎與](../Page/王铎.md "wikilink")“同乡里友人登览邙山，并游鄏山之阴龙洞”，有《南山登高同友苗家村夜归漫兴》之作。（张升：《王铎年谱》）