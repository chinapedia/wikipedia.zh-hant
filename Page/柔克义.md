[William_Woodville_Rockhill.jpg](https://zh.wikipedia.org/wiki/File:William_Woodville_Rockhill.jpg "fig:William_Woodville_Rockhill.jpg")
[The_Land_of_the_Lamas.jpg](https://zh.wikipedia.org/wiki/File:The_Land_of_the_Lamas.jpg "fig:The_Land_of_the_Lamas.jpg")
[Life_of_The_Budda.JPG](https://zh.wikipedia.org/wiki/File:Life_of_The_Budda.JPG "fig:Life_of_The_Budda.JPG")
**柔克义**（**William Woodville
Rockhill**，），美国[外交官](../Page/外交官.md "wikilink")、[汉学家](../Page/汉学家.md "wikilink")、[藏学家](../Page/藏学家.md "wikilink")。1854年生于美国[费城](../Page/费城.md "wikilink")，1914年因染风寒在[檀香山逝世](../Page/檀香山.md "wikilink")。

## 简历

  - 1884年－1888年柔克义任美国驻[大清使馆一秘](../Page/大清.md "wikilink")。
  - 1886年－1887年柔克义任美国驻[朝鲜参赞](../Page/朝鲜.md "wikilink")。
  - 1897年美国驻[希腊大使](../Page/希腊.md "wikilink")
  - 1901年9月7日大美國欽差特辦議和事宜全權大臣柔克義代表美国签署《[辛丑条约](../Page/辛丑条约.md "wikilink")》，後向[美國國務院提交了](../Page/美國國務院.md "wikilink")《減免部分[庚子拳亂賠款以資助清國留學美國](../Page/庚子拳亂賠款.md "wikilink")》的建議書。
  - 1905年至1909年[美国驻大清公使](../Page/美國駐華大使列表.md "wikilink")。
  - 1909年－1911年[美国驻俄罗斯大使](../Page/美国驻俄罗斯大使.md "wikilink")。
  - 1911年－1913年[美国驻土耳其大使](../Page/美国驻土耳其大使.md "wikilink")。

## 汉学

柔克义深受法国学派的影响，对藏文和汉文皆有较深造诣；他对中国古代和[南洋](../Page/南洋.md "wikilink")、[西洋的](../Page/西洋.md "wikilink")[交通史](../Page/交通.md "wikilink")，有较深的研究。1911年柔克义和[德国](../Page/德国.md "wikilink")[汉学家](../Page/汉学.md "wikilink")[夏德合作将](../Page/夏德.md "wikilink")[宋代](../Page/宋代.md "wikilink")[赵汝适所著](../Page/赵汝适.md "wikilink")《[诸蕃志](../Page/诸蕃志.md "wikilink")》翻译成[英文](../Page/英文.md "wikilink")。1914年柔克义又将[元代航海家](../Page/元代.md "wikilink")[汪大渊著](../Page/汪大渊.md "wikilink")《[岛夷志略](../Page/岛夷志略.md "wikilink")》一书，部分翻译成英文。柔克义对上述二书中的古代南洋和西洋地名多有考证。

他是第一位學習[藏語的美國人](../Page/藏語.md "wikilink")，他在北京拜来自拉萨和西宁的喇嘛为师，学藏语四年\[1\]，被譽為美國現代[西藏學之父](../Page/西藏學.md "wikilink")。

## 西藏之旅

柔克义在学生时代就对西藏感到兴趣。1888年冬，他辞去美国驻[大清使馆一秘之职务](../Page/大清.md "wikilink")，筹备由华北陆路到西藏的旅程。他雇了二辆骡车，每车配两匹骡子，前往甘肃省会[兰州](../Page/兰州.md "wikilink")。12月17日，他带了一名仆人出发，取道保定府、12月23日抵达长城的北天门口；26日抵达太原府。次日向潼关进发，经[平遥县](../Page/平遥县.md "wikilink")、[灵石县](../Page/灵石县.md "wikilink")，1月5日抵达黄河，渡河到潼关，经[临潼县](../Page/临潼县.md "wikilink")，抵达西安府。1月10日抵达[咸阳](../Page/咸阳.md "wikilink")。16日抵达[平凉府](../Page/平凉府.md "wikilink")。

## 名字

柔克义是他自己认可并广泛使用的汉文名字；后世有的译者率译其姓为罗克希尔。藏文对其称呼，或音译其姓为རོག་ཧིལ་，曾一度使用于写给柔克义的藏文通信中。

## 著作

### 英文著作

  - 《佛陀传》1884
  - 《喇嘛之境》1891
  - 《辛卯壬辰年蒙藏旅行记》1894
  - 《西藏民俗学》1895
  - 《我国外交事务中亟需克服之问题》1897
  - 《美国外交：中国事务》1901
  - 《中韩关系论》1889
  - 《十五世记至十九世记末的中韩关系》1905
  - 《拉萨的达赖喇嘛和满清皇帝的关系》1910
  - 《中国1910年人口调查》1912

### 汉译英

  - F. Hirth and W. W. Rockhill: Chau Ju-kua, His work on the Chinese
    and Arab trade in the twelfth and thirteenth centuries, entitled
    Chu-fan-chî, St Petersburg,1911.
  - W. W. Rockhill tr.《Description of the Barbarians of the Isles》
    Tʼoung Pao， 1913

## 参考文献

<references/>

[R](../Page/category:美国驻华大使.md "wikilink")

[R](../Category/美国外交官.md "wikilink")
[R](../Category/美國漢學家.md "wikilink")
[Category:美國藏學家](../Category/美國藏學家.md "wikilink")
[Category:美国驻俄罗斯大使](../Category/美国驻俄罗斯大使.md "wikilink")
[Category:法国外籍兵团军官](../Category/法国外籍兵团军官.md "wikilink")
[Category:美国军官](../Category/美国军官.md "wikilink")
[Category:美国助理国务卿](../Category/美国助理国务卿.md "wikilink")

1.  柔克义 《喇嘛之乡》 第5页