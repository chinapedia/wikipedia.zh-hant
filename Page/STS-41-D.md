****是历史上第十二次航天飞机任务，也是[发现号的首次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[亨利·哈特斯菲尔德](../Page/亨利·哈特斯菲尔德.md "wikilink")**（，曾执行、以及任务），指令长
  - **[迈克尔·科茨](../Page/迈克尔·科茨.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[朱迪斯·蕾斯尼克](../Page/朱迪斯·蕾斯尼克.md "wikilink")**（，曾执行以及任务），任务专家
  - **[斯蒂芬·霍利](../Page/斯蒂芬·霍利.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[理查德·穆莱恩](../Page/理查德·穆莱恩.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[查尔斯·沃克](../Page/查尔斯·沃克.md "wikilink")**（，曾执行、以及任务），有效载荷专家

[Category:1984年美国](../Category/1984年美国.md "wikilink")
[Category:1984年科學](../Category/1984年科學.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1984年8月](../Category/1984年8月.md "wikilink")
[Category:1984年9月](../Category/1984年9月.md "wikilink")