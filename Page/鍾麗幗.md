**鍾麗幗**（），畢業於[嘉諾撒聖心書院](../Page/嘉諾撒聖心書院.md "wikilink")，1969年加入[香港政府任](../Page/香港政府.md "wikilink")[政務官](../Page/政務官.md "wikilink")，1974年任[房屋署助理處長](../Page/香港房屋署.md "wikilink")，1979年任首席助理[房屋司](../Page/房屋司.md "wikilink")，1984年任香港房屋署副署長；亦曾任[市政總署署長](../Page/市政總署.md "wikilink")。

1997年任職市政總署署長時，與時任[市政局主席](../Page/市政局.md "wikilink")[梁定邦就](../Page/梁定邦_\(醫生\).md "wikilink")[香港中央圖書館外觀設計爆發罵戰](../Page/香港中央圖書館.md "wikilink")，後被指擅自更改中央圖書館的外觀設計而被降職。

退休後加入香港地產商[恒基地產旗下](../Page/恒基地產.md "wikilink")[香港小輪公司](../Page/香港小輪公司.md "wikilink")，曾經參與[西九文化區投標事宜](../Page/西九文化區.md "wikilink")，被指違反[公眾利益](../Page/公眾利益.md "wikilink")。

## 相關

  - [香港政府公告:
    副署長鍾麗幗告別公務員生涯](http://www.housingauthority.gov.hk/b5/aboutus/resources/archivepublications/housingdimension/0,,3-0-8071-8006_0,00.html)
  - 鍾麗幗[父親](../Page/父親.md "wikilink")[鍾榮光](../Page/鍾榮光.md "wikilink")[是香港體育界名人](http://www.epochtimes.com/b5/1/4/20/n78800.htm)
  - [明報政情周記](http://www.cheungmankwong.org.hk/newspaper/np20000512.html)
  - [民主黨房屋政策發言人李永達對鍾麗幗使用高官身份有意見](http://www.dphk.org/database/opress/000505.htm)
  - [前香港房屋署副署長鍾麗幗離職半年後進入私營房地產業](http://www.rfa.org/cantonese/xinwen/2004/12/22/hongkong_government_officials/?simple=1)

[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[L](../Category/鍾姓.md "wikilink")
[Category:獲頒授香港銅紫荊星章者](../Category/獲頒授香港銅紫荊星章者.md "wikilink")