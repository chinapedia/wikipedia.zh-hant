<noinclude></noinclude><includeonly>__NOINDEX__</includeonly>

<div class="messagebox plainlinks" style="padding: 10px; margin: 20px auto; width: 96%; font-size: 10pt;">

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Sandbox.png" title="Sandbox.png" alt="Sandbox.png" width="75" /><figcaption>Sandbox.png</figcaption>
</figure></td>
<td><p><big>欢迎来到<strong><a href="../Page/WP:关于沙盒.md" title="wikilink">维基百科沙盒</a></strong>！</big> }?action=edit 这-{zh-tw:裡;zh-hk:裏;zh-cn:里;}-]'''开始測試{{#if:</p></td>
<td><p>或-{zh-cn:点击;zh-hk:點擊;zh-tw:點選;}-上方的<strong>編輯</strong>}}（如果您使用<a href="../Page/WP:移動存取.md" title="wikilink">-{zh:手機;zh-tw:行動;zh-hk:流動;zh-cn:移动;}-版</a>，可以-{zh-tw:點選;zh-hk:點擊;zh-cn:点击;}-上面的<strong>鉛筆-{zh-tw:圖示;zh-cn:图标;}-</strong>）。若要使用<a href="../Page/WP:可视化编辑器.md" title="wikilink">可视化编辑器测试</a>，请移步至<strong>[?veaction=edit 这-{zh-tw:裡;zh-hk:裏;zh-cn:里;}-]</strong>。}}}</p>
<p>沙盒是用于练习页面编辑技巧的測試页面。您可在此随意地进行编辑的练习。完成编辑后，可-{zh-cn:点击;zh-hk:點擊;zh-tw:點選;}-“<strong></strong>”按钮预览内容，也可-{zh-cn:点击;zh-hk:點擊;zh-tw:點選;}-“<strong></strong>”按钮-{zh-tw:儲存;zh-cn:保存;}-内容。就像在沙灘上所寫的文字涨潮时就會消失，同样沙盒中所寫的內容，隨時可能被清空，不會有任何預先通知（但曾-{zh-tw:儲存;zh-cn:保存;}-的内容仍会永久存留在页面的历史记录中）。</p></td>
<td><p>{{{editnotice|{{#switch: </p></td>
<td><p>Wikipedia:沙盒 = </p></td>
<td><p>Template:沙盒 = </p></td>
<td><p>Template:X1 = </p></td>
<td><p>Template:X2 = </p></td>
<td><p>Category:沙盒 = </p></td>
<td><p>模块:沙盒 = </p></td>
<td><p>Draft:沙盒 =  }} }}}</p></td>
</tr>
</tbody>
</table>

</div>

<div class="abusefilter-sandbox-header">

</div>

<includeonly> {{\#switch:

`| Category:沙盒 = `

| \#default = {{\#ifeq:|User||{{\#ifeq:|User talk||{{\#if:||}}}}}}
}}</includeonly> <noinclude>  </noinclude>

[Category:維基百科站務](../Category/維基百科站務.md "wikilink")
[Category:沙盒](../Category/沙盒.md "wikilink")