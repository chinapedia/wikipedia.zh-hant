**文山堡**，舊名**拳山堡**，是[台灣北部自](../Page/台灣.md "wikilink")[清治時期至](../Page/清治時期.md "wikilink")[日治初期的一個行政區劃](../Page/台灣日治時期.md "wikilink")。相較於現行的[文山區](../Page/文山區.md "wikilink")，文山堡範圍極為遼闊，今臺北市公館以南，包括文山、新店、深坑、石碇、坪林、雙溪等區多屬之。

拳山原為[臺北盆地東南丘陵的一個小概略性區域](../Page/臺北盆地.md "wikilink")，大約是指[蟾蜍山](../Page/蟾蜍山.md "wikilink")（高128公尺）至[寶藏巖](../Page/寶藏巖.md "wikilink")（海拔35公尺）一帶。該地區開發甚早，1721年即有[客家人於此開墾](../Page/客家人.md "wikilink")，1729年，客家人簡岳率眾至拳山開墾，與當地[凱達格蘭族發生糾紛](../Page/凱達格蘭族.md "wikilink")，數百[漢人被殺](../Page/漢人.md "wikilink")，隨後[清治時期](../Page/清治時期.md "wikilink")[淡水廳並於此地設拳山堡](../Page/淡水廳.md "wikilink")，管轄範圍遍及臨近地區。[清朝](../Page/清朝.md "wikilink")[光緒二十年](../Page/光緒.md "wikilink")（1894年）地方士紳嫌名稱不雅，請求改為「文山秀氣」的[文山堡](../Page/大文山區.md "wikilink")。

1920年10月1日實施州郡制，當地屬於[臺北州](../Page/臺北州.md "wikilink")[文山郡](../Page/文山郡.md "wikilink")，郡役所設於[新店](../Page/新店區.md "wikilink")，管轄[新店街](../Page/新店街.md "wikilink")、[深坑庄](../Page/深坑庄.md "wikilink")、[石碇庄](../Page/石碇庄.md "wikilink")、[坪林庄及不設街庄的](../Page/坪林庄.md "wikilink")[蕃地](../Page/蕃地_\(文山郡\).md "wikilink")，轄域即今[新北市](../Page/新北市.md "wikilink")[新店區](../Page/新店區.md "wikilink")、[深坑區](../Page/深坑區.md "wikilink")、[石碇區](../Page/石碇區.md "wikilink")、[坪林區](../Page/坪林區.md "wikilink")、[烏來區](../Page/烏來區.md "wikilink")、[臺北市](../Page/臺北市.md "wikilink")[文山區等地](../Page/文山區.md "wikilink")。戰後一度將文山郡改為區，但隨即因實施鄉鎮制而撤消。一直到1990年代[臺北市](../Page/臺北市.md "wikilink")[木柵及](../Page/木柵.md "wikilink")[景美兩區合併](../Page/景美.md "wikilink")，文山一名才又重現於行政區，但區內不包括昔日郡役所在地[新店及其他地區](../Page/新店區.md "wikilink")（[深坑](../Page/深坑區.md "wikilink")、[石碇](../Page/石碇區.md "wikilink")、[坪林](../Page/坪林區.md "wikilink")、[烏來](../Page/烏來區.md "wikilink")）。

## 管轄街庄

文山堡共轄45個街庄\[1\]：

  - 今新店區境內：[安坑庄](../Page/安坑庄.md "wikilink")、[大坪林庄](../Page/大坪林_\(新北市\).md "wikilink")、[青潭庄](../Page/青潭庄.md "wikilink")、[直潭庄](../Page/直潭庄.md "wikilink")
  - 今深坑區境內：[深坑仔庄](../Page/深坑子_\(新北市\).md "wikilink")、[土庫庄](../Page/土庫_\(新北市\).md "wikilink")、[升高坑庄](../Page/升高坑庄.md "wikilink")、[烏月庄](../Page/烏月庄.md "wikilink")、[萬順藔庄](../Page/萬順藔庄.md "wikilink")、[阿柔坑庄](../Page/阿柔坑庄.md "wikilink")
  - 今石碇區境內：[小格頭庄](../Page/小格頭庄.md "wikilink")、[蓬菜藔庄](../Page/蓬菜藔庄.md "wikilink")、[烏塗窟庄](../Page/烏塗窟_\(新北市\).md "wikilink")、[雙溪庄](../Page/雙溪_\(新北市石碇區\).md "wikilink")、[楓仔林庄](../Page/楓仔林庄.md "wikilink")、[大溪墘庄](../Page/大溪墘庄.md "wikilink")、[松柏崎庄](../Page/松柏崎庄.md "wikilink")、[排藔庄](../Page/排藔庄.md "wikilink")、[新興坑庄](../Page/新興坑庄.md "wikilink")、[鹿窟庄](../Page/鹿窟庄.md "wikilink")、[石碇街](../Page/石碇街.md "wikilink")、[玉桂嶺庄](../Page/玉桂嶺庄.md "wikilink")、[員潭仔坑庄](../Page/員潭仔坑庄.md "wikilink")、[崩山-{庄}-](../Page/崩山_\(新北市\).md "wikilink")
  - 今坪林區境內：[坪林尾庄](../Page/坪林尾庄.md "wikilink")、[灣潭庄](../Page/灣潭庄.md "wikilink")、[坑仔口庄](../Page/坑子口_\(新北市\).md "wikilink")、[{{僻字](../Page/𩻸魚堀庄.md "wikilink")、[水聳淒坑庄](../Page/水聳淒坑庄.md "wikilink")、[九芎林庄](../Page/九芎林_\(新北市\).md "wikilink")、[厚德崗坑庄](../Page/厚德崗坑庄.md "wikilink")、[大粗坑庄](../Page/大粗坑庄.md "wikilink")、[鷺鶿岫庄](../Page/鷺鶿岫庄.md "wikilink")、[楣仔藔庄](../Page/楣仔藔庄.md "wikilink")、[大舌湖庄](../Page/大舌湖庄.md "wikilink")、[柑腳坑庄](../Page/柑腳坑庄.md "wikilink")、[濶瀨庄](../Page/濶瀨庄.md "wikilink")
  - 今雙溪區境內：[大平庄](../Page/大平_\(新北市\).md "wikilink")、[烏山庄](../Page/烏山庄.md "wikilink")、[溪尾藔庄](../Page/溪尾藔庄.md "wikilink")、[料角坑庄](../Page/料角坑庄.md "wikilink")
  - 今文山區境內：[萬盛庄](../Page/萬盛_\(大字\).md "wikilink")、[興福庄](../Page/興福庄.md "wikilink")、[內湖庄](../Page/內湖_\(臺北市文山區\).md "wikilink")、[陂內坑庄](../Page/坡內坑_\(臺北市\).md "wikilink")

## 參考文獻

[Category:新北市堡里](../Category/新北市堡里.md "wikilink")
[Category:深坑廳](../Category/深坑廳.md "wikilink")
[Category:基隆廳](../Category/基隆廳.md "wikilink")
[Category:台北市堡里](../Category/台北市堡里.md "wikilink")
[Category:宜蘭縣堡里](../Category/宜蘭縣堡里.md "wikilink")
[Category:文山區](../Category/文山區.md "wikilink")
[Category:深坑區](../Category/深坑區.md "wikilink")
[Category:新店區](../Category/新店區.md "wikilink")
[文山堡](../Category/文山堡.md "wikilink")

1.  《新舊對照管轄便覽》，臺灣總督府