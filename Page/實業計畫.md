[孫中山先生建國方略圖.jpg](https://zh.wikipedia.org/wiki/File:孫中山先生建國方略圖.jpg "fig:孫中山先生建國方略圖.jpg")

**實業計畫**，為《[建國方略](../Page/建國方略.md "wikilink")》之物質建設。1920年《實業計畫》完稿，原以英文發表，英文本名為*The
International Development of
China*(國際共同開發中國實業計劃書)，是為「物質建設」。1921年10月10日由[林雲陔](../Page/林雲陔.md "wikilink")、[馬君武等人譯出中文本](../Page/馬君武.md "wikilink")，[孫文在廣州發表中文自序](../Page/孫文.md "wikilink")，《實業計畫》中提出六項計劃，分港口建設、鐵路、道路建設、採礦業、治河道等八個範疇。而對於此計畫書，孫中山在其自序中有提到，希望借助外國資本與技術，與中國政府簽約一同開發中國，使得中國基礎建設與輕重工業進行發展，中外共創雙贏。

## 著述過程

曾經參與此《**實業計畫**》校改的近代中國[教育家](../Page/教育家.md "wikilink")[蔣夢麟](../Page/蔣夢麟.md "wikilink")，在其[回憶錄](../Page/回憶錄.md "wikilink")《[西潮](../Page/西潮.md "wikilink")》一書中，描述[孫中山如何以現代](../Page/孫中山.md "wikilink")[科學方法寫下此書](../Page/科學.md "wikilink")，為[中國](../Page/中國.md "wikilink")[工業化譜下](../Page/工業化.md "wikilink")[藍圖](../Page/藍圖.md "wikilink")，節錄如下：

## 参考文献

## 外部連結

  - [實業計畫摘要](https://web.archive.org/web/20160306093600/http://club.jledu.gov.cn/?uid-4476-action-viewspace-itemid-40044)
  - [實業計劃全文](http://www.gzmg.gov.cn/Article/szs/szswx/200811/32.html)
  - [建國方略--實業計畫(含圖)](http://greatroc.org/bbs/viewthread.php?tid=4879&page=1&extra=#pid15615)

{{-}}

[Category:孙中山著作](../Category/孙中山著作.md "wikilink")
[Category:經濟書籍](../Category/經濟書籍.md "wikilink")
[Category:1920年書籍](../Category/1920年書籍.md "wikilink")