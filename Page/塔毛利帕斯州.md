[Tamaulipas_in_Mexico_(location_map_scheme).svg](https://zh.wikipedia.org/wiki/File:Tamaulipas_in_Mexico_\(location_map_scheme\).svg "fig:Tamaulipas_in_Mexico_(location_map_scheme).svg")

**塔毛利帕斯州**
（）是[墨西哥東北部的一個](../Page/墨西哥.md "wikilink")[州](../Page/墨西哥行政區劃.md "wikilink")。面积79,384平方公里，人口3,024,238（2005.10.17）。

## 外部連結

  - [Music and Dance of the State of
    Tamaulipas](https://web.archive.org/web/20070702142330/http://www.mexfoldanco.org/tamaulipas.shtml)

  - [Tamaulipas state government](http://www.tamaulipas.gob.mx/).

  - [University of
    Tamaulipas](https://web.archive.org/web/20070607030017/http://www.uat.mx/)

  - [Colonization Laws of the State of
    Tamaulipas, 1826](http://texashistory.unt.edu/widgets/pager.php?object_id=meta-pth-5872&recno=462&path=/data/UNT/GLT/meta-pth-5872.tkl)
    from [Gammel's Laws of Texas, Vol.
    I.](http://texashistory.unt.edu/permalink/meta-pth-5872) hosted by
    the [Portal to Texas History](http://texashistory.unt.edu/).

[Category:墨西哥行政區劃](../Category/墨西哥行政區劃.md "wikilink")
[Category:1824年建立的一級行政區](../Category/1824年建立的一級行政區.md "wikilink")