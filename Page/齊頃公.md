**齊頃公**，[姜姓](../Page/姜姓.md "wikilink")，名**無野**，[齊惠公之子](../Page/齊惠公.md "wikilink")。在位期間執政為[高固](../Page/高固.md "wikilink")、[國佐](../Page/國佐.md "wikilink")。姬妾[聲孟子](../Page/聲孟子.md "wikilink")，生有继承人[齐灵公](../Page/齐灵公.md "wikilink")。

## 簡介

[前592年春](../Page/前592年.md "wikilink")，晋景公派遣[郤克到](../Page/郤克.md "wikilink")[齐国参加](../Page/齐国.md "wikilink")[会盟](../Page/会盟.md "wikilink")。[齐顷公让母亲](../Page/齐顷公.md "wikilink")[萧同叔子躲在帷幕中](../Page/萧同叔子.md "wikilink")，觀看郤克跛腳的樣子，萧同叔子卻邊看邊恥笑郤克的跛腳，郤克生气，出来发誓说：“不报复这次耻辱，就不渡过黄河！”《[春秋谷梁传](../Page/春秋谷梁传.md "wikilink")》说：“齐之患，必自此始矣！”

前589年齊頃公率軍南下攻魯國龍邑（山東泰安東南），寵臣[盧蒲就癸被殺](../Page/盧蒲就癸.md "wikilink")，頃公怒而攻至巢丘（今山東泰安境内）。前589年頃公在[鞍之戰大敗](../Page/鞍之戰.md "wikilink")，齊頃公被晉軍追逼，“三周華不注”，差點被俘，幸得大臣[逢丑父相救](../Page/逢丑父.md "wikilink")，二人互換衣服，佯命齊頃公到山腳華泉取水，得以逃走。宋代[曾鞏](../Page/曾鞏.md "wikilink")《登華山》詩：“丑父遺忠無處問，空餘一掬野泉甘。”

後來齊國國勢趨衰。齊頃公變得低調內歛，周濟窮人，照顧[鰥寡](../Page/鰥寡.md "wikilink")，頗得民心。

## 參見

  -
[Category:姜齐君主](../Category/姜齐君主.md "wikilink")
[W](../Category/姜姓.md "wikilink")
[Category:前582年逝世](../Category/前582年逝世.md "wikilink")
[Category:齊國君主](../Category/齊國君主.md "wikilink")
[Category:春秋時代](../Category/春秋時代.md "wikilink")