**定边县**是[中国](../Page/中国.md "wikilink")[陕西省](../Page/陕西省.md "wikilink")[榆林市所辖的一个](../Page/榆林市.md "wikilink")[县](../Page/县.md "wikilink")，位于陕西西北部，[洛河上游](../Page/洛河.md "wikilink")。面积为6920平方公里，2002年人口为30万。

## 历史沿革

[宋置定边军](../Page/宋.md "wikilink")，[明](../Page/明.md "wikilink")[成化六年](../Page/成化.md "wikilink")（1470年）置定边营，[清](../Page/清.md "wikilink")[雍正九年](../Page/雍正.md "wikilink")（1731年）设定边县。

## 行政区划

下辖1个[街道办事处](../Page/街道办事处.md "wikilink")、14个[镇](../Page/镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

[太中银铁路在定边县设](../Page/太中银铁路.md "wikilink")[定边站](../Page/定边站.md "wikilink")，属[兰州铁路局管辖](../Page/兰州铁路局.md "wikilink")。

## 外部链接

  - [榆林市定边县简介](http://www.dingbian.gov.cn/Html/Intro/96770.html)

[定边县](../Category/定边县.md "wikilink") [县](../Category/榆林区县.md "wikilink")
[榆林](../Category/陕西省县份.md "wikilink")
[陕西](../Category/国家级贫困县.md "wikilink")