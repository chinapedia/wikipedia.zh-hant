**元載**（），字**公輔**，[凤翔府](../Page/凤翔府.md "wikilink")[岐山县](../Page/岐山县.md "wikilink")（今[陕西省](../Page/陕西省.md "wikilink")[鳳翔縣](../Page/鳳翔縣.md "wikilink")）人，[唐朝中期](../Page/唐朝.md "wikilink")[官员](../Page/官员.md "wikilink")。

## 簡介

元载生父不详，母亲携元载[改嫁](../Page/改嫁.md "wikilink")[景昇](../Page/景昇.md "wikilink")。继父景昇为[唐太宗子曹王](../Page/唐太宗.md "wikilink")[李明王妃](../Page/李明_\(唐朝\).md "wikilink")[元氏收田租得力](../Page/元妃_\(李明\).md "wikilink")，景昇向元妃请求后[賜姓元](../Page/賜姓.md "wikilink")，改称元昇。因元载[冒姓元氏](../Page/冒姓.md "wikilink")，《新唐书·宰相世系表》未将他列入元氏表。

《[旧唐书](../Page/旧唐书.md "wikilink")》称他“性惠敏，博览子史，尤学道书”，[天寶初](../Page/天宝_\(唐朝\).md "wikilink")，因熟讀[莊子](../Page/莊子.md "wikilink")、[老子](../Page/老子.md "wikilink")、[列子](../Page/列子.md "wikilink")、[文子之學](../Page/文子.md "wikilink")，以[道家學說考上](../Page/道家.md "wikilink")[進士](../Page/進士.md "wikilink")，後任[新平](../Page/新平县.md "wikilink")[尉](../Page/县尉.md "wikilink")。[至德初年](../Page/至德_\(唐朝\).md "wikilink")，江都采访使[李希言上書](../Page/李希言.md "wikilink")，請求讓元載擔任自己的[副官](../Page/副官.md "wikilink")，擢[祠部](../Page/祠部.md "wikilink")[员外郎](../Page/员外郎.md "wikilink")、[洪州](../Page/洪州_\(隋朝\).md "wikilink")[刺史](../Page/刺史.md "wikilink")。入为[度支郎中](../Page/度支郎中.md "wikilink")，[唐肃宗驚異於他對答敏捷](../Page/唐肃宗.md "wikilink")；之後累迁[户部侍郎](../Page/户部侍郎.md "wikilink")，充度支、江淮转运等使。在肅宗朝，因與掌權宦官[李輔國之妻元氏同族而受到重用](../Page/李輔國.md "wikilink")，管理[漕運](../Page/漕運.md "wikilink")。

[代宗時](../Page/唐代宗.md "wikilink")，為[中書侍郎](../Page/中書侍郎.md "wikilink")[同平章事](../Page/同平章事.md "wikilink")（即[宰相](../Page/宰相.md "wikilink")），封**许昌子**，後又授與[天下元帥](../Page/天下元帥.md "wikilink")[行軍司馬](../Page/行軍司馬.md "wikilink")。因先後助代宗殺了[李輔國以及後來的](../Page/李輔國.md "wikilink")[魚朝恩兩個掌權宦官而更加受到皇帝信任](../Page/魚朝恩.md "wikilink")，“志气骄溢，每众中大言，自谓有文武才略，古今莫及，弄权舞智，政以贿成，僭侈无度。”\[1\]此後營專其私產，大興土木，排除異己，最後因為貪賄被殺、[抄家](../Page/抄家.md "wikilink")，有赃物[胡椒八百石](../Page/胡椒.md "wikilink")，[钟乳五百两](../Page/钟乳.md "wikilink")，史載：“下诏赐载自尽，妻王（王韫秀，[王忠嗣女](../Page/王忠嗣.md "wikilink")，或误作[王缙女](../Page/王缙.md "wikilink")\[2\]，唐代名媛、詩人）及子扬州兵曹参军[伯和](../Page/元伯和.md "wikilink")、祠部员外郎[仲武](../Page/元仲武.md "wikilink")、校书郎[季能并](../Page/元季能.md "wikilink")[赐死](../Page/赐死.md "wikilink")，发其祖、父冢，断棺弃尸，毁私庙主及大宁、安仁里二第，以赐百官署舍，披东都第助治禁苑”，女儿**真一**，是资敬寺[女尼](../Page/女尼.md "wikilink")，被没入[掖庭](../Page/掖庭宫.md "wikilink")。\[3\]野史载其有爱妾[薛瑶英](../Page/薛瑶英.md "wikilink")，在其死后改嫁。“及死，行路无嗟惜者”\[4\]。宋代[羅大經在](../Page/羅大經.md "wikilink")《[鶴林玉露](../Page/鶴林玉露.md "wikilink")》一書中感嘆：“臭襪終須來塞口，枉收八百斛胡椒”。其著作有全集十卷，《[全唐诗](../Page/全唐诗.md "wikilink")》收有一詩《别妻[王韞秀](../Page/王韞秀.md "wikilink")》。

元载死前官爵为银青光禄大夫、中书侍郎、平章事、**颍川郡公**。银青光禄大夫行兵部侍郎李纾为元载作《唐故中书侍郎平章事颍川郡公元府君墓志铭并序》，记载元载继父景昇祖上：景昇祖父景扬名，魏王文学；父景敬同，赠兵部尚书；景昇本人赠户部尚书。

唐德宗继位后，追念元载帮助自己成为皇[太子](../Page/太子.md "wikilink")，于784年追复元载官爵，许以改葬。元载生前的下属[許初](../Page/許初.md "wikilink")、[楊皎](../Page/楊皎.md "wikilink")、[紀慆出资改葬元载](../Page/紀慆.md "wikilink")。元载初谥**荒**，后改为贬义稍弱的**成纵**。[唐文宗](../Page/唐文宗.md "wikilink")[开成初年](../Page/开成.md "wikilink")，[严厚本以元载有翊戴德宗之功](../Page/严厚本.md "wikilink")，主张改其谥号为**忠**并得到通过。

## 参考资料

  - 《[旧唐书](../Page/旧唐书.md "wikilink")·列传第六十八》
  - 《[新唐书](../Page/新唐书.md "wikilink")·列传第七十》

## 注釋

[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")
[Category:唐朝進士](../Category/唐朝進士.md "wikilink")
[Category:唐朝县尉](../Category/唐朝县尉.md "wikilink")
[Category:唐朝礼部员外郎](../Category/唐朝礼部员外郎.md "wikilink")
[Category:唐朝洪州刺史](../Category/唐朝洪州刺史.md "wikilink")
[Category:唐朝尚书省郎中](../Category/唐朝尚书省郎中.md "wikilink")
[Category:唐朝户部侍郎](../Category/唐朝户部侍郎.md "wikilink")
[Category:唐朝中书侍郎](../Category/唐朝中书侍郎.md "wikilink")
[Category:唐朝县子](../Category/唐朝县子.md "wikilink")
[Category:唐朝郡公](../Category/唐朝郡公.md "wikilink")
[Category:唐朝银青光禄大夫](../Category/唐朝银青光禄大夫.md "wikilink")
[Category:唐朝行军司马](../Category/唐朝行军司马.md "wikilink")
[Category:唐朝被赐死人物](../Category/唐朝被赐死人物.md "wikilink")
[Category:唐朝詩人](../Category/唐朝詩人.md "wikilink")
[Category:鳳翔人](../Category/鳳翔人.md "wikilink")
[Z](../Category/元姓.md "wikilink")
[Category:谥荒](../Category/谥荒.md "wikilink")
[Category:諡成縱](../Category/諡成縱.md "wikilink")
[Category:谥忠](../Category/谥忠.md "wikilink")

1.  《[資治通鑑](../Page/資治通鑑.md "wikilink")》卷二百二十四
2.  《[太平广记](../Page/太平广记.md "wikilink")》卷二百三十七·奢侈二：载之妻王氏字韫秀，缙之女也。
3.  《[新唐書](../Page/新唐書.md "wikilink")·列傳第七十·元王黎楊嚴竇》
4.  [李国文](../Page/李国文.md "wikilink")：《元载之死》