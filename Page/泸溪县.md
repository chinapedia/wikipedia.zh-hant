**泸溪县**位于[湖南省](../Page/湖南省.md "wikilink")[湘西土家族苗族自治州东南](../Page/湘西土家族苗族自治州.md "wikilink")，县[治所位于](../Page/治所.md "wikilink")：武溪镇\[1\]，[面积](../Page/面积.md "wikilink")1569[平方公里](../Page/平方公里.md "wikilink")，[国产值总量为](../Page/GDP.md "wikilink")77001万[元](../Page/元.md "wikilink")（西元2003年），人口31.4万（西元2015年[第六次人口普查](../Page/人口普查.md "wikilink")，含外来人口，不包括外出人口），是一个[少数民族多数](../Page/少数民族.md "wikilink")[县](../Page/县.md "wikilink")，少数民族16万人占50.95%。

## 历史

“泸溪”作为区划最早可以追溯到[南北朝](../Page/南北朝.md "wikilink")[梁](../Page/梁.md "wikilink")[萧衍十年](../Page/萧衍.md "wikilink")（西元511年）所置[卢州](../Page/卢州.md "wikilink")，梁萧铣鸣凤三年（西元619年）始置[卢溪县](../Page/卢溪县.md "wikilink")。清代属湖南[辰沅永靖道](../Page/辰沅永靖道.md "wikilink")[辰州府](../Page/辰州府.md "wikilink")，顺治六年，‘卢’改‘泸’，民国初年属[辰沅道](../Page/辰沅道.md "wikilink")，民国29年（西元1940年）属[湖南省第九行政督察区](../Page/湖南省第九行政督察区.md "wikilink")，西元[1949年](../Page/1949年.md "wikilink")11月属[湘西行署沅陵专区](../Page/湘西行署沅陵专区.md "wikilink")，西元1952年属湘西州至今。

## 地理

泸溪东邻[沅陵](../Page/沅陵.md "wikilink")、南接[辰溪县](../Page/辰溪县.md "wikilink")、[麻阳苗族自治县](../Page/麻阳苗族自治县.md "wikilink")，西界[吉首市](../Page/吉首市.md "wikilink")、[凤凰县](../Page/凤凰县.md "wikilink")，北连[古丈县](../Page/古丈县.md "wikilink")；地图坐标为[东经](../Page/东经.md "wikilink")109°40′～110°14′，[北纬](../Page/北纬.md "wikilink")27°54′～28°28′；地处[武陵山脉向](../Page/武陵山脉.md "wikilink")[雪峰山脉过渡地带](../Page/雪峰山脉.md "wikilink")，为“八山半水一分田、半分道路和村庄”[山区县](../Page/山区.md "wikilink")；[地貌自东向西南排成](../Page/地貌.md "wikilink")“川”字形状，[海拔](../Page/海拔.md "wikilink")300至500米，最高处县西南[八面山峰海拔](../Page/八面山峰.md "wikilink")884.3米；属中[亚热带](../Page/亚热带.md "wikilink")[季风](../Page/季风.md "wikilink")[湿润气候](../Page/湿润气候.md "wikilink")，年平均气温17．5℃，无[霜期](../Page/霜.md "wikilink")285天，[日照](../Page/日照.md "wikilink")1500小时，[降雨量](../Page/降雨量.md "wikilink")1440毫米。

## 行政区划

泸溪县辖有 7个镇，4个乡：

  - 镇：合水镇、达岚镇、兴隆场镇、[武溪镇](../Page/武溪镇_\(泸溪\).md "wikilink")、洗溪镇、浦市镇、潭溪镇。
  - 乡：小章乡、白羊溪乡、石榴坪乡、解放岩乡。
      - 村级行政区：221个村委会、19个居委会，1167个村民小组、73个居民小组。

## 官方网站

  - [湖南省泸溪县政府门户网站](http://www.lxx.gov.cn/)

[泸溪县](../Category/泸溪县.md "wikilink") [县](../Category/湘西县市.md "wikilink")
[湘西](../Category/湖南省县份.md "wikilink")
[湘](../Category/国家级贫困县.md "wikilink")

1.