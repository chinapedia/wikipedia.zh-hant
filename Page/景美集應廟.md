[Jingmei_Jiying_Temple.JPG](https://zh.wikipedia.org/wiki/File:Jingmei_Jiying_Temple.JPG "fig:Jingmei_Jiying_Temple.JPG")
**景美集應廟**，又稱**[雙忠廟](../Page/雙忠廟.md "wikilink")**、**尪公廟**等。主祀[雙忠](../Page/雙忠.md "wikilink")（即**尪公**，[保儀尊王與](../Page/保儀尊王.md "wikilink")[保儀大夫](../Page/保儀大夫.md "wikilink")），位於[臺灣](../Page/臺灣.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[文山區景美街](../Page/文山區.md "wikilink")37號，現是[景美區居民的信仰中心](../Page/景美區.md "wikilink")，三級古蹟，[分香自](../Page/分香.md "wikilink")[福建](../Page/福建.md "wikilink")[安溪大坪集應廟](../Page/安溪.md "wikilink")。

## 祀神

主祀[雙忠](../Page/雙忠.md "wikilink")，即[唐朝](../Page/唐朝.md "wikilink")[安史之亂時死守](../Page/安史之亂.md "wikilink")[睢陽而殉難的](../Page/睢陽.md "wikilink")[張巡](../Page/張巡.md "wikilink")、[許遠兩位忠臣](../Page/許遠.md "wikilink")。因其忠烈可風，幾乎全[中國都有](../Page/中國.md "wikilink")[香火奉祀](../Page/香火.md "wikilink")，在[安溪稱為尪公](../Page/安溪.md "wikilink")。本廟大殿供奉主神保儀尊王。殉國後玉帝封之為神，專司驅逐禾苗害蟲之責，在黃巢之亂河南光州一帶時，高張林三姓遷往福建安溪，均經指引而保平安，宋時尊張巡為「保儀尊王」，許遠為「保儀大夫」。另有「尊號互易」之說，許遠為睢陽太守，官位較高，是為「保儀尊王」，張巡為「保儀大夫」。據說安溪的雙忠信仰，是由[河南](../Page/河南.md "wikilink")[光州供奉尪公的高](../Page/光州.md "wikilink")、張、林三姓族人傳入，而後他們亦把此信仰帶入[臺灣](../Page/臺灣.md "wikilink")，以[大臺北一帶最盛](../Page/大臺北.md "wikilink")\[1\]

## 簡史

[清](../Page/清.md "wikilink")[康熙年間](../Page/康熙.md "wikilink")，[福建省](../Page/福建省.md "wikilink")[安溪縣大坪有高](../Page/安溪縣.md "wikilink")、林、張三姓移民，前往[臺灣](../Page/臺灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")，由大坪集應廟奉請尪公（保儀尊王）聖像、夫人媽（保儀尊王的配偶，封「申國夫人」，又稱尪媽或尪娘）聖像與香爐，隨行護佑，議定三姓輪流奉祀，而後三姓人士各自開墾，分別遷居[北投](../Page/北投區.md "wikilink")、[淡水](../Page/淡水區.md "wikilink")、[景美](../Page/景美.md "wikilink")、[木柵](../Page/木柵.md "wikilink")、[大安](../Page/大安區_\(臺北市\).md "wikilink")、[新店](../Page/新店區.md "wikilink")、[深坑](../Page/深坑區.md "wikilink")、[石碇各地](../Page/石碇區.md "wikilink")。

[清文宗](../Page/清文宗.md "wikilink")[咸豐三年](../Page/咸豐.md "wikilink")（1853年）發生[頂下郊拚事件](../Page/頂下郊拚.md "wikilink")，臺北情勢嚴峻，三姓人士決議分家避禍，取出三聖物：尪公聖像、夫人媽聖像、香爐，[拈鬮](../Page/拈鬮.md "wikilink")（[抽籤](../Page/抽籤.md "wikilink")）分配，高姓人士抽得尪公聖像（今日景美集應廟），林姓人士抽得夫人媽聖像（今日[萬隆集應廟](../Page/萬隆集應廟.md "wikilink")），張姓人士抽得香爐（今日[木柵集應廟](../Page/木柵集應廟.md "wikilink")）。三姓人士各自請藝師雕塑其餘缺乏之神像，以建廟奉祀。

[咸豐十年](../Page/咸豐.md "wikilink")（1860年）高姓人士在[景美](../Page/景美.md "wikilink")[竹圍](../Page/竹圍.md "wikilink")（今[景美國小](../Page/景美國小.md "wikilink")[操場旁](../Page/操場.md "wikilink")）建廟，[清穆宗](../Page/清穆宗.md "wikilink")[同治六年](../Page/同治.md "wikilink")（1867年）遷於現址。

## 相關條目

  - [尪公](../Page/尪公.md "wikilink")
  - [學海書院](../Page/學海書院.md "wikilink")：現為高氏族人的宗祠

## 資料來源

## 外部連結

  - [景美集應廟—文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/19850819000024)
  - [文山區公所
    景美集應廟](http://www.wsdo.taipei.gov.tw/ct.asp?xItem=37993&CtNode=4771&mp=124121)

[Category:文山區廟宇](../Category/文山區廟宇.md "wikilink")
[Category:雙忠廟](../Category/雙忠廟.md "wikilink")
[Category:1860年台灣建立](../Category/1860年台灣建立.md "wikilink")

1.  [協和工商資料處理科-集順廟](http://www.mis.hhvs.tp.edu.tw/html/user/game/97pro/a102/aoyou1_04.html)