**[九龍巴士](../Page/九龍巴士.md "wikilink")292P線**是[香港一條由](../Page/香港.md "wikilink")[西貢單](../Page/西貢.md "wikilink")-{向}-往[觀塘的巴士路線](../Page/觀塘.md "wikilink")，途經[菠蘿輋](../Page/菠蘿輋.md "wikilink")、[白沙灣](../Page/白沙灣.md "wikilink")、[南圍](../Page/南圍.md "wikilink")、[壁屋](../Page/壁屋.md "wikilink")、[井欄樹](../Page/井欄樹.md "wikilink")、[牛池灣及](../Page/牛池灣.md "wikilink")[牛頭角等地方](../Page/牛頭角.md "wikilink")。

用車到達[觀塘道](../Page/觀塘道.md "wikilink")[創紀之城東行分站後](../Page/創紀之城.md "wikilink")，會不載客駛回西貢行走94線。

本路線是唯一由西貢郊區開出的早晨特別路線。

## 歷史

  - 1994年10月16日：投入服務，初時於平日07:35開出一班。
  - 1998年8月24日：增加班次至兩班車，開車時間改為平日07:30及07:45兩班。
  - 2002年10月28日：港鐵將軍澳線通車後，部份居民改乘[新巴792M線或](../Page/新巴792M線.md "wikilink")[新界區專線小巴101M線接駁港鐵](../Page/新界區專線小巴101M線.md "wikilink")，加上往返西貢及觀塘的紅色公共小巴投入服務，而井欄樹一帶亦已經有快捷的專線小巴[104線來往觀塘市中心及港鐵牛頭角站](../Page/新界區專線小巴104線.md "wikilink")，收費比起本線便宜，本線需求下降，因此取消07:45的班次。
  - 2016年7月9日：取消星期六服務。
  - 2017年8月28日：更改行車路線，改經九龍灣商貿區\[1\]：
      - 依原有路線駛至觀塘道後，改經[福淘街](../Page/福淘街.md "wikilink")、[牛頭角道](../Page/牛頭角道.md "wikilink")、[啟祥道](../Page/啟祥道.md "wikilink")、[宏照道](../Page/宏照道.md "wikilink")、[常悅道](../Page/常悅道.md "wikilink")、[宏光道](../Page/宏光道.md "wikilink")、啟福道及啟福道天橋，然後返回觀塘道「創紀之城」終點站；
      - 取消觀塘道「[牛頭角下邨](../Page/牛頭角下邨.md "wikilink")」及「[觀塘道休憩處](../Page/觀塘道休憩處.md "wikilink")」站，並新增牛頭角道「[淘大花園](../Page/淘大花園.md "wikilink")」、宏照道「[臨華街](../Page/臨華街.md "wikilink")」、常悅道「常悅道」及啟福道「宏通街」站。

## 服務時間及班次

  - 星期一至五

<!-- end list -->

  - 西貢開：07:30

<!-- end list -->

  -
    星期六、日及公眾假期不設服務

## 收費

全程：$7.8

## 用車

本線由[九龍灣車廠管理的](../Page/九巴九龍灣車廠.md "wikilink")[92線行走當天字軌S](../Page/九龍巴士92線.md "wikilink")01的用車擔任。該用車行走完畢會返回西貢調走94線。

由於行走路段的路面限制，所有車長11米或以下的巴士均有機會行走本線。目前92線所用車型以[丹尼士三叉戟](../Page/丹尼士三叉戟.md "wikilink")10.6米（ATS）為主，間中出現[富豪超級奧林比安](../Page/富豪超級奧林比安.md "wikilink")10.6米（ASV）或[亞歷山大丹尼士Enviro
400](../Page/亞歷山大丹尼士Enviro_400.md "wikilink")（ATSE）用車。

## 行車路線

[KMB292P_RtMap.png](https://zh.wikipedia.org/wiki/File:KMB292P_RtMap.png "fig:KMB292P_RtMap.png")
**經**：[惠民路](../Page/惠民路.md "wikilink")、[福民路](../Page/福民路.md "wikilink")、[普通道](../Page/普通道.md "wikilink")、[西貢公路](../Page/西貢公路.md "wikilink")、[清水灣道](../Page/清水灣道.md "wikilink")、[觀塘道](../Page/觀塘道.md "wikilink")、[福淘街](../Page/福淘街.md "wikilink")、[牛頭角道](../Page/牛頭角道.md "wikilink")、[啟祥道](../Page/啟祥道.md "wikilink")、[宏照道](../Page/宏照道.md "wikilink")、[常悅道](../Page/常悅道.md "wikilink")、[宏光道](../Page/宏光道.md "wikilink")、[啟福道](../Page/啟福道.md "wikilink")、啟福道天橋及觀塘道。

### 沿線車站

| [西貢開](../Page/西貢.md "wikilink") |
| ------------------------------- |
| **序號**                          |
| 1                               |
| 2                               |
| 3                               |
| 4                               |
| 5                               |
| 6                               |
| 7                               |
| 8                               |
| 9                               |
| 10                              |
| 11                              |
| 12                              |
| 13                              |
| 14                              |
| 15                              |
| 16                              |
| 17                              |
| 18                              |
| 19                              |
| 20                              |
| 21                              |
| 22                              |
| 23                              |
| 24                              |
| 25                              |
| 26                              |
| 27                              |
| 28                              |
| 29                              |
| 30                              |
| 31                              |
| 32                              |

## 參考資料

  - 《二十世紀新界（東）區巴士路線發展史》，ISBN 9789628414642，144頁
  - [九巴292P線—i-busnet.com](http://www.i-busnet.com/busroute/kmb/kmbr292p.php)
  - [九巴292P線—681巴士總站](http://www.681busterminal.com/292p.html)

## 外部連結

  - [九巴292P線—九龍巴士](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=292P)

[292P](../Category/九龍巴士路線.md "wikilink")
[292P](../Category/西貢區巴士路線.md "wikilink")
[292P](../Category/觀塘區巴士路線.md "wikilink")

1.  [九巴乘客通告](http://www.hkitalk.net/HKiTalk2/data/attachment/forum/201708/24/235026jx29v9xb9xlyl1xn.png.thumb.jpg)