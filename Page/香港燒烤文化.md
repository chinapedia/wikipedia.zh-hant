**香港燒烤文化**是指[香港人的自助](../Page/香港人.md "wikilink")[燒烤形式](../Page/燒烤.md "wikilink")，香港人稱之為「燒嘢食」（「嘢食」是[粵語所指的](../Page/粵語.md "wikilink")「食物」），由於流通於香港社會的[香港粵語](../Page/香港粵語.md "wikilink")，常會夾雜[英語](../Page/英語.md "wikilink")，因此香港人也經常直接以英語barbecue或BBQ稱之。香港的燒烤活動，通常是參與者圍着一個燒烤爐，各自手持燒烤叉，烤製各自選擇的食物。這種獨特的燒烤文化，一般相信是源於[1960年代的](../Page/香港1960年代.md "wikilink")[工廠勞工的集體旅行](../Page/工廠.md "wikilink")\[1\]。當時的集體旅行活動，一般都是到[新界地區郊遊](../Page/新界.md "wikilink")，除了攜帶食物外，還會帶備鍋具，以便到達後處理食物。

燒烤是香港很普遍的集體消閒活動，通常會和家人或朋友一起進行，燒烤也常見於香港學校的郊遊活動，也有商業機構把燒烤作為安排給僱員的娛樂活動。

## 公共燒烤場

[香港政府在](../Page/香港政府.md "wikilink")[香港郊野公園或](../Page/香港郊野公園.md "wikilink")[香港海灘設置了不少免費的公共燒烤場地](../Page/香港海灘.md "wikilink")\[2\]。[香港島的](../Page/香港島.md "wikilink")[石澳](../Page/石澳.md "wikilink")、[赤柱](../Page/赤柱.md "wikilink")，以及[新界](../Page/新界.md "wikilink")[西貢一帶的郊野公園](../Page/西貢.md "wikilink")，均是燒烤熱點。[長洲或](../Page/長洲_\(香港\).md "wikilink")[南丫島等離島亦設有燒烤場](../Page/南丫島.md "wikilink")。公共的燒烤爐通常是用水泥、石塊或磚塊建造而成，爐身的外觀呈方柱狀或圓柱狀，中央空心部分設有鐵製的欄柵用於承載木炭，燒烤爐其中一則欄柵以下位置會有一個固定的開孔，以利空氣進入助燃。燒烤爐旁會圍着擺放數張石凳，在附近通常還有一張供擺放食物的木枱。使用這些公共燒烤爐無需收費，也不設預約，由燒烤人士以先到先得方式自行分配使用。在[星期六](../Page/星期六.md "wikilink")、[星期日及](../Page/星期日.md "wikilink")[公眾假期](../Page/公眾假期.md "wikilink")，尤其是天氣較涼快的[秋季](../Page/秋季.md "wikilink")，燒烤爐通常供不應求。這些公共燒烤設施最早始建於1971年的[城門郊野公園](../Page/城門郊野公園.md "wikilink")\[3\]，主要目的是避免市民在不適合燒烤的地方自行搭建火爐燒烤而導致[山火](../Page/山火.md "wikilink")，並為當時的年輕人提供有益身心的康樂設施\[4\]。其後因為大受歡迎，所以便推廣到其他郊野公園。

因為木炭燃燒時會造成空氣污染，亦為遏止燒炭自殺的風氣，所以香港政府曾在部分郊野公園的公共燒烤場，試驗使用電熱式燒烤爐\[5\]\[6\]。

在香港的郊野公園，燒烤活動必須在指定的燒烤區內進行，任何人士皆不得在燒烤區範圍以外的地方隨意生火，否則可被檢控。此外，使用公共燒烤爐的燒烤人士應保持環境清潔，離開燒烤場前必須弄熄火種，把攜來的用品帶走，垃圾則應放入指定的廢物箱內\[7\]。

## 過程

[Hkstylebbq.jpg](https://zh.wikipedia.org/wiki/File:Hkstylebbq.jpg "fig:Hkstylebbq.jpg")
燒烤前必須令[木炭燃燒](../Page/木炭.md "wikilink")。燒烤人士須在燒烤爐放置承載木炭的鐵絲網，再於上面堆放木炭，並使用一些稱為「炭精」的助燃物品幫助引燃木炭，炭精可用[打火機或](../Page/打火機.md "wikilink")[火柴點燃](../Page/火柴.md "wikilink")。生火過程中，需要以扇子或報紙煽風，直至木炭能夠自行燃燒。之後即使燒烤爐的木炭不足，只要把新的木炭直接放入仍在燃燒的木炭上，再稍為煽風即可維持木炭燃燒。

港式燒烤與其他地方的燒烤文化最大的不同之處，是較少把食物直接放在燒烤網上燒烤，而是採用類似[自助餐的模式](../Page/自助餐.md "wikilink")，各自把自己想吃的食物插在燒烤叉上，然後一起圍著燒烤爐烤製自己選擇的食物。

燒烤叉是一枝長約60厘米的鐵叉，其中一端是呈「U」字型的叉狀，尖銳的末端能夠刺入和固定食物，另一端則裝有供握持用的木製手柄。在開始燒烤前，一般會先利用爐火把燒烤叉稍為烘烤，作為清潔殺菌的程序，之後便可以把未熟的食物插在燒烤叉上，然後把燒烤叉上的食物，放在木炭以上約10至15厘米的位置進行烘烤。傳統的港式燒烤，通常會在食物接近完全燒熟前，便會在食物塗上[蜜糖](../Page/蜜糖.md "wikilink")，然後繼續把食物燒烤直至烤製完成。除了蜜糖外，也可以塗抹燒烤醬或其他醬料代替。當食物烤熟後，一般會使用紙碟或紙碗盛載。

在燒烤結束後，必須用水把燒烤爐的火種弄熄，以免引發[山火](../Page/山火.md "wikilink")\[8\]。

## 食物

[BBQ_Facility_in_Aberdeen_Country_Park.jpg](https://zh.wikipedia.org/wiki/File:BBQ_Facility_in_Aberdeen_Country_Park.jpg "fig:BBQ_Facility_in_Aberdeen_Country_Park.jpg")

港式燒烤的食物以[肉類為主](../Page/肉類.md "wikilink")，如[雞翼](../Page/雞翅.md "wikilink")、[牛排](../Page/牛排.md "wikilink")、[雞排](../Page/雞排.md "wikilink")、[豬排及](../Page/豬排.md "wikilink")[羊肉](../Page/羊肉.md "wikilink")，這些肉類通常先以[蒜茸](../Page/蒜茸.md "wikilink")、[黑椒](../Page/黑椒.md "wikilink")、[香茅](../Page/香茅.md "wikilink")、[醬油](../Page/醬油.md "wikilink")、[沙茶或](../Page/沙茶.md "wikilink")[沙爹等](../Page/沙爹.md "wikilink")[調味料](../Page/調味料.md "wikilink")[醃制調味](../Page/醃.md "wikilink")。常見的燒烤食物包括[香腸](../Page/香腸.md "wikilink")，[紅腸和芝士腸](../Page/紅腸.md "wikilink")，還有[牛丸](../Page/牛丸.md "wikilink")、[貢丸](../Page/貢丸.md "wikilink")、[魚丸和](../Page/魚丸.md "wikilink")[墨魚丸等肉丸](../Page/墨魚丸.md "wikilink")，有時也會出現[海鮮](../Page/海鮮.md "wikilink")，例如[多春魚](../Page/多春魚.md "wikilink")、[魷魚和](../Page/魷魚.md "wikilink")[扇貝等](../Page/扇貝.md "wikilink")。較有特色的燒烤食物，包括有鴨腎、鲜菇、[吐司及](../Page/吐司.md "wikilink")[棉花糖等](../Page/棉花糖.md "wikilink")。

除了把食物直接放在爐火上燒烤外，也可以在燒烤爐的爐火旁，放置用[錫紙](../Page/鋁箔紙.md "wikilink")（即[鋁箔紙](../Page/鋁.md "wikilink")）包裹的食物，以“煨”的方法煮熟。這種燒烤方法較易保存食物的汁液和水分，但需要較長的時間把食物煮熟。[金針菇](../Page/金針菇.md "wikilink")、[茄子](../Page/茄子.md "wikilink")、[番薯及](../Page/番薯.md "wikilink")[玉米](../Page/玉米.md "wikilink")，較適合以[錫紙包裹煨熟](../Page/鋁箔紙.md "wikilink")。

因為燒烤時經常要處理生和熟的食物，所以生熟食物須要使用不同的用具和器皿，避免交叉污染。由於肉類[脂肪在燒烤過程中所產生的](../Page/脂肪.md "wikilink")[多環芳香族碳氫化合物是](../Page/多環芳香族碳氫化合物.md "wikilink")[致癌物質](../Page/致癌物質.md "wikilink")，所以[食物安全中心建議市民不應過量進食燒烤食物](../Page/食物安全中心.md "wikilink")\[9\]。

## 商業活動

[HK_Gold_Coast_Mall_Roof_07_BBQ.JPG](https://zh.wikipedia.org/wiki/File:HK_Gold_Coast_Mall_Roof_07_BBQ.JPG "fig:HK_Gold_Coast_Mall_Roof_07_BBQ.JPG")

### 燒烤包

以往香港家庭的燒烤活動，都會早一天在家中準備將要使用的食材。不過，由於在家中醃製燒烤食材需要多個器皿，又要準備包裝食材的用品，所以便有預先把肉類醃製和包裝的燒烤包出現。燒烤包可在凍肉店或[超級市場購買](../Page/超級市場.md "wikilink")，因為燒烤包內的肉類都已預先醃製，所以只要把包裝食物的[保鮮膜拆去即可使用](../Page/保鮮膜.md "wikilink")。

### 私營燒烤場

雖然香港的郊野公園有不少燒烤爐，但交通方便或鄰近停車場的公共燒烤爐在假日總是供不應求，一些交通較方便的地點便出現一些私人經營的燒烤場，在場內提供可出租使用的燒烤爐。最早出現的，是位於[大圍](../Page/大圍.md "wikilink")[歡樂城舊址的燒烤樂園](../Page/歡樂城.md "wikilink")（今原址為[港鐵](../Page/港鐵.md "wikilink")[大圍站](../Page/大圍站.md "wikilink")），之後在[淺水灣及其他旅遊熱點亦開始出現這種私營的](../Page/淺水灣.md "wikilink")「自助燒烤場」。這些自助燒烤場，通常都會因為供求問題，而在假日大幅提高收費，部分更限制使用時間。此外，也有一些大廈在[天台架設燒烤爐讓住客使用](../Page/天台.md "wikilink")。

私營燒烤場的燒烤爐類似“美式燒烤”的鐵架燒烤爐，同樣使用木炭作為燃料。私營燒烤場一般都會為顧客租用的燒烤爐提供點火服務，為提高點火的效率，職員一般會用罐裝[石油氣配合火焰器直接噴射木炭使之自燃](../Page/石油氣.md "wikilink")。

隨着都市發展，這些私營燒烤場亦慢慢遠離市區。現時香港的私營燒烤場，大多數都位於遠離市區的[新界的市郊地區](../Page/新界.md "wikilink")，如[小欖](../Page/小欖_\(香港\).md "wikilink")、[大欖](../Page/大欖.md "wikilink")、[龍鼓灘](../Page/龍鼓灘.md "wikilink")、[元朗及](../Page/元朗.md "wikilink")[大尾篤等地](../Page/大尾篤.md "wikilink")。這些私營燒烤場大多採用劃一收費，部分設有[停車場](../Page/停車場.md "wikilink")，方便顧客駕車前往。顧客無需準備燒烤用品和食物，只要購買入場券便能進場。大部分私營燒烤場以自助模式運作，讓顧客自行取用燒烤食物，並憑入場門票取用指定數量的飲品，部分燒烤場更提供額外的[娛樂設施](../Page/娛樂.md "wikilink")，如播放[電視節目](../Page/電視節目.md "wikilink")、[足球直播或](../Page/足球.md "wikilink")[麻將等](../Page/麻將.md "wikilink")。

## 衍生的問題

  - 由於傳統燒烤使用木炭，在燃燒時常會造成[空氣污染](../Page/空氣污染.md "wikilink")。香港市區大廈林立，在大廈天台燒烤容易污染周圍環境，並對鄰近居民構成滋擾\[10\]。
  - 進行燒烤活動時，切勿使用易燃液體點燃木炭或用於助燃。香港過往已發生多次有人把[火酒倒在火種引發搶火導致燒傷的意外](../Page/火酒.md "wikilink")\[11\]。
  - 由於燒烤是香港居民很普遍的消閒活動，所以燒烤用的木炭很容易在香港的[超級市場和](../Page/超級市場.md "wikilink")[便利店購得](../Page/便利店.md "wikilink")，也導致有自殺傾向的人士，同樣很容易取得木炭進行[燒炭自殺](../Page/燒炭自殺.md "wikilink")，因此很多燒烤木炭的包裝袋上，都會加上協助有關人士求助的標示。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《香港燒烤露營指南》，萬里機構 ISBN 9621423473

## 參見

  - [燒烤](../Page/燒烤.md "wikilink")
  - [燒炭自殺](../Page/燒炭自殺.md "wikilink")

[en:Regional variations of
barbecue\#Hong_Kong](../Page/en:Regional_variations_of_barbecue#Hong_Kong.md "wikilink")

[Category:燒烤](../Category/燒烤.md "wikilink")
[Category:香港飲食](../Category/香港飲食.md "wikilink")

1.  [香江漫話：上山下海探尋香港燒烤文化](http://www.hkcd.com.hk/content/2011-12/04/content_2857467.htm)
    ，香港商報，2011-12-4
2.  [燒烤地點/地區](http://www.afcd.gov.hk/tc_chi/country/cou_vis/cou_vis_rec/cou_vis_bar.html)，漁農自然護理署，2014-2-10
3.  [荒山野嶺變綠蔭處處](http://www.rocks.org.hk/images/PolicyReview/2013-09-18_appledaily0220131009224719.pdf)，蘋果日報，2013-9-18
4.  [郊野公園先鋒在城門](http://www.mingpaohealth.com/cfm/health3.cfm?File=20110205/leisure/wta5.txt)
    ，明報健康網，2011-2-5
5.  [電燒烤爐試用計劃](http://www.afcd.gov.hk/tc_chi/country/cou_vis/cou_vis_rec/cou_vis_rec_ele/cou_vis_rec_ele_bud.html)
    ，漁農自然護理署，二零零六年三月
6.  [BBQ新體驗
    郊野公園鐵板燒](http://hd.stheadline.com/living/living_content.asp?contid=2087&srctype=g)，頭條日報
7.  [保持沙灘及泳池清潔注意事項](http://www.lcsd.gov.hk/b5/ls_do_swim.php)
    ，康樂及文化事務署，2014年7月8日
8.  [郊野公園遠足安全指引](https://www.afcd.gov.hk/tc_chi/country/cou_wha/cou_wha_whe_sat.html)，漁農自然護理署，二零一四年八月六日
9.  [食物安全小貼士–燒烤](http://www.cfs.gov.hk/tc_chi/programme/programme_rafs/programme_rafs_fc_01_06_pub.html)，香港食物安全中心，2009-10-11
10. [天台無牌燒烤場鬧市炸彈](http://orientaldaily.on.cc/cnt/news/20100206/00196_001.html)，東方日報，2010-2-6
11. [火堆淋酒精露營漢變燒豬](http://the-sun.on.cc/cnt/news/20110514/00407_050.html)，東方日報，2011-5-14