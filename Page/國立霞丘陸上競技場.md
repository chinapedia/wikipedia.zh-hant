[Japan_national_stadium02.jpg](https://zh.wikipedia.org/wiki/File:Japan_national_stadium02.jpg "fig:Japan_national_stadium02.jpg")

**國立霞丘陸上競技場**（；又譯**國立霞丘田徑場**）是[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[霞丘一座已拆除的](../Page/霞丘町.md "wikilink")[運動場](../Page/運動場.md "wikilink")，曾是[東京最主要的大型運動場地](../Page/東京.md "wikilink")，1958年啟用，提供48,000位座席；在當地[口語的使用上](../Page/口語.md "wikilink")，常被簡稱為「**[國立競技場](../Page/國立競技場.md "wikilink")**」或「**[國立](../Page/國立.md "wikilink")**」。其為[1964年東京奧運的](../Page/1964年夏季奧林匹克運動會.md "wikilink")[主場館](../Page/奥林匹克体育场.md "wikilink")，也是[日本國家足球隊的主場地](../Page/日本國家足球隊.md "wikilink")，每年[元旦舉行的](../Page/元旦.md "wikilink")[天皇盃決賽](../Page/天皇盃.md "wikilink")、1月舉行的[全國高等學校足球選手權大會](../Page/全國高等學校足球選手權大會.md "wikilink")、以及11月舉行的[日本聯賽盃皆在此舉行](../Page/日本聯賽盃.md "wikilink")，被譽為日本體育活動的[聖地](../Page/聖地.md "wikilink")。因應[2020年東京奧運舉行的需求](../Page/2020年夏季奧林匹克運動會.md "wikilink")，2014年7月起拆除改建為[新國立競技場](../Page/新國立競技場.md "wikilink")。

## 歷史

國立霞丘田徑場於1958年落成，首項舉行的主要活動是[1958年東京亞運](../Page/1958年亞洲運動會.md "wikilink")，此後先後舉辦過多項主要的運動會。

為了滿足東京舉辦[2020年夏季奧運及](../Page/2020年夏季奧林匹克運動會.md "wikilink")[帕運的需要](../Page/2020年夏季帕拉林匹克運動會.md "wikilink")，相關單位決定將使用多年的國立霞丘田徑場予以重建，做為這兩個運動會的主場館（[新國立競技場](../Page/新國立競技場.md "wikilink")）。國立霞丘田徑場在2014年5月31日關場，並於同年7月動工拆除、2015年10月拆除完畢。

## 交通

  - 千駄谷門（主看台北側）
      - [都營地下鐵](../Page/都營地下鐵.md "wikilink")[大江戶線](../Page/都營大江戶線.md "wikilink")－[國立競技場站](../Page/國立競技場站.md "wikilink")
      - [JR](../Page/JR東日本.md "wikilink")[中央總武線](../Page/中央、總武緩行線.md "wikilink")－[千駄谷站](../Page/千駄谷站.md "wikilink")
  - 青山門（後看台）
      - JR中央總武線－[信濃町站](../Page/信濃町站.md "wikilink")
  - 代代木門（主看台南側）
      - JR中央總武線－千駄谷站
      - [東京地下鐵](../Page/東京地下鐵.md "wikilink")[銀座線](../Page/東京地下鐵銀座線.md "wikilink")－[外苑前站](../Page/外苑前站.md "wikilink")

## 演唱會

由於有噪音限制，所以國立霞丘陸上競技場從2005年開始，每年限定只有一組藝人開唱，加上容納數能達到6萬人，並不是一般歌手就能開唱，所以國立霞丘陸上競技場被認為是頂尖的夢幻舞台（甚至有「聖地」的美名），能登上这一舞台的艺人屈指可数；但在2012年是第一次破例讓[嵐及](../Page/嵐.md "wikilink")[彩虹樂團同年開唱](../Page/彩虹樂團.md "wikilink")。

隨著國立霞丘陸上競技場於2014年拆除重建，在工程開始前破例增加了艺人组数。競技場重建前，已在本场馆开唱的艺人共有7组。

  - 2005年：[SMAP](../Page/SMAP.md "wikilink")
  - 2006年：[SMAP](../Page/SMAP.md "wikilink")
  - 2007年：[DREAMS COME TRUE](../Page/DREAMS_COME_TRUE.md "wikilink")
  - 2008年：[嵐](../Page/嵐.md "wikilink")
  - 2009年：[嵐](../Page/嵐.md "wikilink")
  - 2010年：[嵐](../Page/嵐.md "wikilink")
  - 2011年：[嵐](../Page/嵐.md "wikilink")
  - 2012年：[嵐](../Page/嵐.md "wikilink")、[彩虹樂團](../Page/彩虹樂團.md "wikilink")
  - 2013年：[嵐](../Page/嵐.md "wikilink")
  - 2014年：[桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")、[彩虹樂團](../Page/彩虹樂團.md "wikilink")、[AKB48](../Page/AKB48.md "wikilink")（第二場因天雨關係取消）、<s>[保羅·麥卡尼](../Page/保羅·麥卡尼.md "wikilink")</s>（因身體不適而延期並改至其它地點開演\[1\]）、特別企劃「Japan
    Night、再見國立」（[SEKAI NO
    OWARI](../Page/SEKAI_NO_OWARI.md "wikilink")、[Perfume](../Page/Perfume.md "wikilink")、[生物股長等多組藝人](../Page/生物股長.md "wikilink")）\[2\]

## 參考注釋

## 外部連結

  - [國立競技場](http://www.jpnsport.go.jp/kokuritu/)

[Category:東京體育場地](../Category/東京體育場地.md "wikilink")
[Category:新宿區](../Category/新宿區.md "wikilink")
[Category:日本足球場](../Category/日本足球場.md "wikilink")
[Category:日本田徑場](../Category/日本田徑場.md "wikilink")
[Category:橄欖球場](../Category/橄欖球場.md "wikilink")
[Category:奧林匹克體育場](../Category/奧林匹克體育場.md "wikilink")
[Category:國家體育場](../Category/國家體育場.md "wikilink")
[Category:1964年夏季奧林匹克運動會](../Category/1964年夏季奧林匹克運動會.md "wikilink")
[Category:1958年完工體育場館](../Category/1958年完工體育場館.md "wikilink")
[Category:2014年日本廢除](../Category/2014年日本廢除.md "wikilink")
[Category:奧運足球場館](../Category/奧運足球場館.md "wikilink")
[O](../Category/日本音樂場地.md "wikilink")
[Category:東京都已不存在的建築物](../Category/東京都已不存在的建築物.md "wikilink")

1.
2.