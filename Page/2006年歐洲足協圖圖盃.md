**2006年歐洲足協圖圖盃**（**2006 UEFA Intertoto
Cup**）是圖圖盃改制後的首屆比賽，改制後會只有三輪比賽，而出線[歐洲足協盃的隊伍會增至十一隊](../Page/歐洲足協盃.md "wikilink")。

## 第一輪

首回合於2006年6月17日至6月18日舉行，而次回合於6月24日至6月25日舉行。               |}

## 第二輪

首回合於2006年7月1日至7月2日舉行，而次回合於7月8日至7月9日舉行。                |}

<sup>1</sup>
<small>以[塞爾維亞和黑山球隊名義參賽](../Page/塞爾維亞和黑山.md "wikilink")，現在屬於[黑山球隊](../Page/黑山.md "wikilink")。</small>
<sup>2</sup>
<small>此場比賽於[貝爾格萊德舉行](../Page/貝爾格萊德.md "wikilink")，因為[薛達的主場館未能達到](../Page/澤塔足球俱樂部.md "wikilink")[歐洲足協要求](../Page/歐洲足協.md "wikilink")。</small>
<sup>3</sup>
<small>此場比賽於[Herzliya舉行](../Page/Herzliya.md "wikilink")，因為[彼達迪華馬卡比的主場館正在翻新](../Page/彼達迪華馬卡比足球會.md "wikilink")。</small>

## 第三輪

首回合於2006年7月15日至7月16日舉行，而次回合於7月22日舉行。             |}

\* 馬賽和奧丹斯憑作客入球優惠晉級

<sup>1</sup>
<small>在[國際足協諮詢下](../Page/國際足協.md "wikilink")，因為[2006年義大利甲組足球聯賽醜聞正在調查](../Page/2006年義大利甲組足球聯賽醜聞.md "wikilink")，聯賽排名未確定，[意大利代表](../Page/意大利.md "wikilink")[巴勒莫退出圖圖盃](../Page/巴勒莫足球俱樂部.md "wikilink")。根據規例，由[歐塞爾補上](../Page/歐塞爾青年會.md "wikilink")。</small>

## 總結

2006年圖圖盃第三圈獲勝球隊中：

  - 除川迪、列特及馬里博爾外所有球隊都可躋身歐洲足協盃第一圈。
  - 紐卡素、歐塞爾、草蜢及奧丹斯（奧丹斯擊敗另一隊圖圖盃第三圈獲勝球隊哈化柏林以獲得晉級資格）可晉身歐洲足協盃分組賽。
  - 紐卡素晉身歐洲足協盃 32 強，並成為在該圈中唯一一隊從圖圖盃晉身歐洲足協盃的球隊，因而獲得圖圖盃總冠軍。

## 參考

  - [2006–07年歐洲冠軍聯賽](../Page/2006–07年歐洲冠軍聯賽.md "wikilink")
  - [2006–07年歐洲足協盃](../Page/2006–07年歐洲足協盃.md "wikilink")

## 外部連結

  - [歐洲足協官方網站](https://web.archive.org/web/20090302065945/http://www.uefa.com/Competitions/IntertotoCup/index.html)

[圖](../Category/2006年足球.md "wikilink")
[Category:歐洲足協圖圖盃](../Category/歐洲足協圖圖盃.md "wikilink")