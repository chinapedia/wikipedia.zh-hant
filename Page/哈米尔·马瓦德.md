**豪尔赫·哈米尔·马瓦德·维特**（**Jorge Jamil Mahuad
Witt**，）是前[厄瓜多爾總統](../Page/厄瓜多爾總統.md "wikilink")，1987－1988年及1991－1993年两次任人民民主党主席，1983至84年曾任劳动部长。1992年当选首都[基多市市长](../Page/基多.md "wikilink")，1998年当选總統，2000年1月他宣佈以[美元取代厄瓜多爾貨幣](../Page/美元.md "wikilink")[蘇克雷](../Page/蘇克雷.md "wikilink")，觸發全國性的抗議活動，同月他被政变推翻，流亡[美国](../Page/美国.md "wikilink")。副总统[古斯塔沃·诺沃亚接替总统职务](../Page/古斯塔沃·诺沃亚.md "wikilink")。

他是[黎巴嫩和](../Page/黎巴嫩.md "wikilink")[德国后裔](../Page/德国.md "wikilink")。

## 外部連結

  - [Extended biography (in Spanish) by CIDOB
    Foundation](http://www.cidob.org/es/documentacion/biografias_lideres_politicos/america_del_sur/ecuador/jamil_mahuad_witt)
  - [Text of the Rio
    Protocol](https://web.archive.org/web/20060112025400/http://www.usip.org/library/pa/ep/ep_rio01291942.html)
  - [Mahuad and
    Fujimori](https://web.archive.org/web/20050420055327/http://www.oas.org/OASNews/1999/English/March-April99/article1.htm)
  - [厄國前總統馬瓦德背景資料](http://news.bbc.co.uk/chinese/simp/hi/newsid_610000/newsid_614700/614710.stm)

[M](../Category/厄瓜多尔总统.md "wikilink")