**意蒂絲·内斯比特**（**Edith Nesbit**，婚後姓名為**Edith
Bland**，1858年8月15日－1924年5月4日）英國小說作家和詩人，在兒童文學的出版上使用**E.
Nesbit**為名。其最著名的文學作品為曾經改編成電影《[沙仙活地魔](../Page/沙仙活地魔.md "wikilink")》（Five
Children and It，2004年）的奇幻魔法故事系列：《五個孩子和一個怪物》、《五個孩子和鳳凰與魔毯》、《五個孩子和一個護身符》三部曲
\[1\]，和《鐵路邊的孩子們》（[The Railway
Children](http://en.wikipedia.org/wiki/The_Railway_Children_%28film%29)，1970年）。

## 作品列表

  - 《尋寶奇謀》 The Story of the Treasure Seekers (1899年)
  - 《淘氣鬼行善記》The Wouldbegoods (1899年)
  - 《五個孩子和一個怪物》Five Children and It (1902年)
  - 《五個孩子和鳳凰與魔毯》The Phoenix and the Carpet (1904年)
  - 《五個孩子和一個護身符》The Story of the Amulet (1905年)
  - 《鐵路邊的孩子們》The Railway Children (1906年)
  - 《魔法城堡》The Enchanted Castle (1907年)
  - 《亞頓城的魔法》The House of Arden (1908年)
  - 《迪奇的時空旅行》Harding's Luck (1909年)

<!-- end list -->

  - Grim Tales (stories) (1893年)
  - The Pilot (1893年)
  - The Seven Dragons (1899年)
  - The New Treasure Seekers (1904年)
  - The Three Mothers (1908年)
  - These Little Ones (1909年)
  - The Magic City (1910年)
  - Dormant (1911年)
  - Wet Magic (1913年)
  - To the Adventurous (stories) (1923年)

## 參考注釋

<references/>

## 外部連結

  - Vidal, Gore. The Writing of E. Nesbit. The New York Review of Books.
    DECEMBER 3, 1964 ISSUE

<http://www.nybooks.com/articles/1964/12/03/the-writing-of-e-nesbit/>
\* The Literature Network
<http://www.online-literature.com/edith-nesbit/>

[N](../Category/1858年出生.md "wikilink")
[N](../Category/1924年逝世.md "wikilink")
[N](../Category/英國作家.md "wikilink")
[N](../Category/奇幻小說家.md "wikilink")
[N](../Category/兒童文學作家.md "wikilink")
[Category:费边社成员](../Category/费边社成员.md "wikilink")

1.  中文譯本因出版社不同而有不同譯名版本，目前以出版年代最新的為名。