[Parcentrance.jpg](https://zh.wikipedia.org/wiki/File:Parcentrance.jpg "fig:Parcentrance.jpg")

**帕羅奧多研究中心公司**（，縮寫為PARC），前身為**施乐帕羅奧多研究中心**（Xerox
PARC），曾是[施乐公司所成立的最重要的研究机构](../Page/施乐公司.md "wikilink")，它坐落于[美国](../Page/美国.md "wikilink")[加利福尼亚州的](../Page/加利福尼亚州.md "wikilink")[帕羅奧多](../Page/帕羅奧多.md "wikilink")，成立于1970年。在2002年1月4日起成為獨立子公司。

帕羅奧多研究中心是许多现代计算机技术的诞生地，他们的创造性的研发成果包括：[个人电脑](../Page/个人电脑.md "wikilink")[Xerox
Alto](../Page/Xerox_Alto.md "wikilink")、[激光打印机](../Page/激光打印机.md "wikilink")、[鼠标](../Page/鼠标.md "wikilink")、[以太网](../Page/以太网.md "wikilink")；[图形用户界面](../Page/图形用户界面.md "wikilink")、[Smalltalk](../Page/Smalltalk.md "wikilink")、页面描述语言[Interpress](../Page/Interpress.md "wikilink")（[PostScript的先驱](../Page/PostScript.md "wikilink")）、[-{zh-cn:图标;
zh-tw:圖示;
zh-hk:圖標}-](../Page/图标.md "wikilink")、[下拉式選單](../Page/下拉式選單.md "wikilink")、[所见即所得文本编辑器](../Page/所见即所得.md "wikilink")、语音压缩技术等。

## 外部連結

  - [PARC](http://www.parc.com/)
  - [Xerox PARC 的创新](http://www.xerox.com/innovation/parc.html)
  - [Xerox Star
    历史文档](http://www.digibarn.com/friends/curbow/star/index.html)

[P](../Category/帕羅奧圖公司.md "wikilink")
[Category:美國研究機構](../Category/美國研究機構.md "wikilink")
[Category:全錄公司](../Category/全錄公司.md "wikilink")
[Category:1970年成立的公司](../Category/1970年成立的公司.md "wikilink")