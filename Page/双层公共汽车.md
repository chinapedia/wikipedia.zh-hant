[Routemaster_RM871.jpg](https://zh.wikipedia.org/wiki/File:Routemaster_RM871.jpg "fig:Routemaster_RM871.jpg")地標之一：紅色雙層巴士（[AEC
Routemaster型](../Page/AEC_Routemaster.md "wikilink")）\]\]
[London_Bus_route_452.jpg](https://zh.wikipedia.org/wiki/File:London_Bus_route_452.jpg "fig:London_Bus_route_452.jpg")
[KMB_VB5699_40X.jpg](https://zh.wikipedia.org/wiki/File:KMB_VB5699_40X.jpg "fig:KMB_VB5699_40X.jpg")的雙層巴士\]\]
[B9TLWright-B3-SBST.jpg](https://zh.wikipedia.org/wiki/File:B9TLWright-B3-SBST.jpg "fig:B9TLWright-B3-SBST.jpg")的雙層巴士\]\]

[San_Chung_Bus_KKA-1156_20170501.jpg](https://zh.wikipedia.org/wiki/File:San_Chung_Bus_KKA-1156_20170501.jpg "fig:San_Chung_Bus_KKA-1156_20170501.jpg")
[Ikarus-bodied_Scania_K124EB_double_decker_in_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Ikarus-bodied_Scania_K124EB_double_decker_in_Hong_Kong.jpg "fig:Ikarus-bodied_Scania_K124EB_double_decker_in_Hong_Kong.jpg")
**雙層巴士**，或稱作**-{zh-hans:双层巴士; zh-hant:雙層巴士;
zh-hk:雙層公車;zh-sg:双层公交车}-**、**-{zh-hans:双层公交;
zh-hant:雙層公交;
zh-hk:雙層公交;zh-sg:双层公车}-**，是指載客車廂由上下兩層組成的[公共汽车](../Page/公共汽车.md "wikilink")。

## 构造

雙層巴士由兩层构成，两层之间有[楼梯相连接](../Page/楼梯.md "wikilink")，楼梯在车厢的中部或后部。大多的巴士有兩扇车[门](../Page/门.md "wikilink")，分别设在车的前部和中部。虽然汽车的长度和宽度与一般巴士并无差别，但由于多了一层，車輛高度比其他的高，这使得此种车型的运載量比其它车要大，比[單層巴士能接載多一倍的乘客](../Page/單層巴士.md "wikilink")。雖然雙層巴士較容易受到建築物的高度限制，但相比起要達到相同載客量的掛接式巴士，雙層巴士在轉向及上落斜坡方面會較掛接式巴士靈活。

雙層巴士起源於[英國](../Page/英國.md "wikilink")，其中行走[倫敦的紅色雙層巴士](../Page/倫敦.md "wikilink")，更是英國的「國寶」。在一些（前）[英聯邦國家和地區](../Page/英聯邦.md "wikilink")，諸如[印度](../Page/印度.md "wikilink")、[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[南非等地](../Page/南非.md "wikilink")，雙層巴士是主要的[通勤交通工具之一](../Page/通勤.md "wikilink")。另外[德国](../Page/德国.md "wikilink")，特别是北部德国城市也大量使用。由於車體龐大，重心較高，驾驶员和乘客都要特别注意乘车安全，雙層巴士的設計要特別考慮穩定性。因此，双层巴士在設計上需更慎重考慮其車體和物理特性對機械設計和運行的影響。而為免更進一步將車輛[重心升高](../Page/重心.md "wikilink")，乘客一般也不能站在上层，以免巴士翻側。在一些地方例如[香港](../Page/香港.md "wikilink")，新型號巴士在獲准載客前必須通過[傾斜測試](../Page/傾斜測試.md "wikilink")。

## 利用情况

不少地方例如[中國大陸](../Page/中國大陸.md "wikilink")、[台灣和](../Page/台灣.md "wikilink")[澳門](../Page/澳門.md "wikilink")，都曾引進過雙層巴士，但由於行車安全、經營成本，甚至是[人行天橋的高度限制等因素](../Page/人行天橋.md "wikilink")，最後還是以失敗告終。在目前一些[歐洲](../Page/歐洲.md "wikilink")、北美城市及中國大陸、[馬來西亞](../Page/馬來西亞.md "wikilink")、[日本一些城市](../Page/日本.md "wikilink")，雖也有雙層巴士，惟主要還是作为观光巴士以及長途客運。这些车通常有兩種，分別為車身低矮，設有後置樓梯及行李艙的雙層遊覽車，以及上层是露天或半露天的雙層观光巴士。英國、香港及新加坡因為很早已普及使用雙層巴士，並透過建築物及道路的相關法規，新建的建築物須要保留雙層巴士能夠通過的高度，確保雙層巴士可以通行無阻。

現時，中國城市[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[南寧](../Page/南寧.md "wikilink")、[西安](../Page/西安.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[天津](../Page/天津.md "wikilink")、[杭州等都有雙層公車行走](../Page/杭州.md "wikilink")。长途汽车的上層甚至設置铺位。

[上海](../Page/上海.md "wikilink")[好運道書局發行的一張](../Page/好運道書局.md "wikilink")[明信片](../Page/明信片.md "wikilink")，黑白照片顯示1920年代中國[南京路已經出現雙層巴士](../Page/南京路.md "wikilink")。\[1\]

[臺灣](../Page/臺灣.md "wikilink")[臺北市早在](../Page/臺北市.md "wikilink")1978年有過引進雙層巴士的計畫，當時[臺北市政府建設局屬意引入](../Page/臺北市政府建設局.md "wikilink")12米長、4米高的德國製雙層巴士，總載客量達126人，但由於超過法令的高度限制問題（臺灣法令規定大客車車高不能超過3.8米），因此需要修改法令才可引入，引進雙層巴士計畫因而不了了之\[2\]。到了1990年臺北市引入了雙層巴士試行投入路線營運，為兩輛在香港裝嵌車身的左-{zh-hant:軚;zh-hans:舵}-[利蘭奧林比安](../Page/利蘭奧林比安.md "wikilink")11米雙層巴士（台灣以Volvo作登記）\[3\]\[4\]，在1990年3月1日早上6時首航[74號路線](../Page/台北聯營公車74路線.md "wikilink")，該計畫預定試行3個月。新的雙層巴士按香港標準設計，上層車廂只有在樓梯扶手設下車鐘，下層配用長條形膠帶按鐘\[5\]。巴士上路後引來許多臺北市民好奇目光，更有不少民眾因新鮮感而試搭，在尖峰時段、上學時間等全車爆滿，惟部分人對搭乘這種雙層巴士的方法並不熟悉，如不知危險性地於上層站立，也有反映樓梯過於狹窄且陡峭不易上落\[6\]，更有人因深怕來不及按下車鐘而不敢在上層坐，引致下層客滿，上層仍有座位的情況出現\[7\]。後來[臺北捷運工程導致](../Page/臺北捷運.md "wikilink")74號需要改道而行，路段因此不再適合雙層巴士行駛，之後巴士試行了504、[505線](../Page/台北聯營公車505路線.md "wikilink")，另也曾建議過為巴士安裝空調，但代理商稱技術問題而未能成事。試驗結果認為在提升運載能力及減低日常營運成本方面雙層巴士有優勢，惟最後因購車成本預算問題、巴士繞路避開過矮之陸橋（香港稱天橋）徒添行車時間等，計畫最終無疾而終\[8\]\[9\]，車輛則運回香港，輾轉至2000年代才拆毀\[10\]。

马来西亚的双层巴士则可分为公共和旅游两种，其中公共双层巴士可以在[巴生谷和](../Page/巴生谷.md "wikilink")[槟城找到](../Page/槟城.md "wikilink")。其中[槟城快捷通目前共使用](../Page/槟城快捷通.md "wikilink")3辆双层巴士川行，并将在不久后投入30辆双层巴士以容纳更多乘客\[11\]。吉隆坡的双层巴士则有100余辆，每辆可容纳108名乘客\[12\]。作为旅游用途的双层巴士服务则有槟城的[随乘随下巴士和吉隆坡的随乘随下巴士](../Page/随乘随下.md "wikilink")，然而收费较高，主要于旅游景点川行。

## 雙層巴士應用地區

[Guangzhou_Sunshine_Bus_Line_226_For_JS6111SHCP.jpg](https://zh.wikipedia.org/wiki/File:Guangzhou_Sunshine_Bus_Line_226_For_JS6111SHCP.jpg "fig:Guangzhou_Sunshine_Bus_Line_226_For_JS6111SHCP.jpg")曾于2003年全數退役，但2009年再次引入雙層巴士運行\]\]
[360791_at_Beijing_West_Railway_Station_(20150212161455).JPG](https://zh.wikipedia.org/wiki/File:360791_at_Beijing_West_Railway_Station_\(20150212161455\).JPG "fig:360791_at_Beijing_West_Railway_Station_(20150212161455).JPG")

### 通勤用途

  -
  -
  -
  - 部分城市（如[北京](../Page/北京.md "wikilink")、[广州](../Page/广州.md "wikilink")）

  -
  -
  - 部分城市（如[吉隆坡](../Page/吉隆坡.md "wikilink")、[檳城](../Page/檳城.md "wikilink")）

  -
  - 部分城市（[華盛頓哥倫比亞特區](../Page/華盛頓哥倫比亞特區.md "wikilink")\[13\]）

  - 部分城市（如[維多利亞](../Page/維多利亞_\(不列顛哥倫比亞\).md "wikilink")）

  -
  -
  -
### 旅遊觀光巴士專用

  - [巴黎](../Page/巴黎.md "wikilink")

  -   - 臺北：「[臺北觀光巴士](../Page/台北市雙層觀光巴士.md "wikilink")」是由[三重客運](../Page/三重客運.md "wikilink")、台灣租車旅遊集團以及三普旅行社共同合作成立三晉整合營銷公司負責營運，運用APP系統配有中、英、日、韓四種語音導覽功能。
      - 高雄：「高雄觀光巴士」是由[高雄客運負責營運](../Page/高雄客運.md "wikilink")。
      - 臺南：「臺南觀光巴士」是由[府城客運負責營運](../Page/府城客運.md "wikilink")。

  -
  -
### 旅遊觀光巴士或長途客運專用

  -
  - 部分城市（如[上海](../Page/上海.md "wikilink")）

  - 部分城市（如[紐約](../Page/紐約.md "wikilink")、[芝加哥以及Megabus運營的長途巴士線路](../Page/芝加哥.md "wikilink")）

  - [首爾](../Page/首爾.md "wikilink")

## 雙層巴士製造商

  - [佳牌](../Page/佳牌.md "wikilink")
  - [利蘭](../Page/利蘭.md "wikilink")
  - [丹拿](../Page/丹拿_\(英國\).md "wikilink")
  - [丹尼士](../Page/丹尼士車廠.md "wikilink")
  - [亞歷山大丹尼士](../Page/亞歷山大丹尼士.md "wikilink")
  - [富豪車廠](../Page/富豪車廠.md "wikilink")
  - [都城嘉慕威曼](../Page/都城嘉慕威曼.md "wikilink")
  - [猛獅](../Page/猛獅.md "wikilink")
  - [Neoplan](../Page/Neoplan.md "wikilink")
  - [Scania](../Page/斯堪尼亚汽车.md "wikilink")
  - [DAF](../Page/DAF.md "wikilink")/[VDL](../Page/VDL_巴士.md "wikilink")
  - [北汽福田](../Page/北汽福田.md "wikilink")
  - [扬州亚星](../Page/扬州亚星.md "wikilink")
  - [安凯汽车](../Page/安凯汽车.md "wikilink")
  - [南京金陵](../Page/南京金陵.md "wikilink")
  - [厦门金龙](../Page/金龙联合汽车.md "wikilink")
  - [郑州宇通](../Page/宇通客车.md "wikilink")
  - [青年汽车](../Page/青年汽车.md "wikilink")
  - [海格客车](../Page/海格客车.md "wikilink")
  - [厦门金旅](../Page/厦门金旅.md "wikilink")
  - [蜀都客车](../Page/蜀都客车.md "wikilink")
  - [广通汽车](../Page/广通汽车.md "wikilink")
  - [广通客车](../Page/广通客车.md "wikilink")
  - [比亞迪](../Page/比亞迪.md "wikilink")
  - [大吉汽車](../Page/大吉汽車.md "wikilink")

## 註釋

## 參見

  - [公共汽車](../Page/公共汽車.md "wikilink")
  - [掛接巴士](../Page/鉸接客車.md "wikilink")

[雙層巴士](../Category/雙層巴士.md "wikilink")
[Category:巴士](../Category/巴士.md "wikilink")

1.

2.
3.

4.

5.
6.  facebook.com/groups/193180350726223/permalink/1695459933831583/
    台灣巴士文化協會facebook專頁

7.
8.

9.

10.

11.

12.

13.