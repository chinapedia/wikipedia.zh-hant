《**You're the Only
One**》（意思：你是唯一）是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王靖雯的第三張大碟](../Page/王靖雯.md "wikilink")，[粵語專輯](../Page/粵語.md "wikilink")，於1990年12月发行。專輯10首歌曲，7首為翻唱。這張唱片發佈之後，她便暫時去了[美國進修](../Page/美國.md "wikilink")、休息。自這張專輯開始，王靖雯的音樂開始有強烈的[R\&B風格](../Page/R&B.md "wikilink")，一直延續到後來1993年的《[執迷不悔](../Page/執迷不悔.md "wikilink")》。

## 唱片版本

  - 首版：[美國壓盤](../Page/美國.md "wikilink")。
  - 再版：
      - 《Recall》版：《Recall》是將王菲前3張大碟捆綁發售。
      - 《精彩全記錄》版（首版、再版）：《精彩全記錄》是將王菲前8張大碟捆綁發售。
      - 《從頭認識》版：《從頭認識》是將王菲在新藝寶一共9張粵語大碟捆綁發售。
      - 環球復黑版：2004年8月27日。
      - [黑膠唱片版](../Page/黑膠唱片.md "wikilink")
      - 蜚聲環球系列：2017年4月25日。
      - [卡帶](../Page/卡帶.md "wikilink") [錄音帶版](../Page/錄音帶.md "wikilink")

## 曲目

## 獎項

香港：

  - [商業電台的](../Page/商業電台.md "wikilink")[叱咤樂壇流行榜](../Page/叱咤樂壇流行榜.md "wikilink")：
      - 1990年度：「叱咤女歌手銅獎」\[1\]

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:1990年音樂專輯](../Category/1990年音樂專輯.md "wikilink")
[Category:新艺宝唱片音乐专辑](../Category/新艺宝唱片音乐专辑.md "wikilink")

1.  [商業電台官方網站](http://www1.881903.com/framework/pccs.gateway?url=jsp/lsc/index.jsp&menuID=36&pageURL=/fwcontent/lsc/html/lsc_20021206award1990.htm)