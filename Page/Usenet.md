[Usenet_servers_and_clients.svg](https://zh.wikipedia.org/wiki/File:Usenet_servers_and_clients.svg "fig:Usenet_servers_and_clients.svg")

**Usenet**是一種分佈式的[互聯網交流系統](../Page/互聯網.md "wikilink")，源自通用用途的[UUCP網絡](../Page/UUCP.md "wikilink")（名字亦類似），它的發明是在1979年由[杜克大學的研究生](../Page/杜克大學.md "wikilink")[Tom
Truscott與](../Page/Tom_Truscott.md "wikilink")[Jim
Ellis所設想出來的](../Page/Jim_Ellis.md "wikilink")，Usenet包含眾多**[新聞組](../Page/新聞組.md "wikilink")**，它是新聞組（異於傳統，新聞指交流、信息）及其消息的[網絡集合](../Page/網絡.md "wikilink")。

## 簡介

Usenet裡的消息（帖子）根據所分Usenet層級（新聞組）存儲在伺服器，多數伺服器不斷轉發其消息給其他伺服器，最終新聞組消息被分佈式存儲於全球大量的計算機中，但它不是[万维网](../Page/万维网.md "wikilink")。

通過軟件用戶能夠選擇訂閱感興趣的新聞組、消息進行閱讀、索引、刪除過期消息等。Usenet的最初構想是借助網絡進行技術信息交流，後來被廣泛推廣到大眾領域，如社會新聞、業餘愛好、個人興趣等主題。現在默認情況下，它使用[NNTP協議](../Page/NNTP.md "wikilink")。Usenet最主要的特色是統一分組、全球轉信（轉發消息）。一些人認為Usenet一詞來自於User
Network。

**新聞組**歸入層級的八大組：

  - **comp.\***：關於[計算機的相關話題](../Page/計算機.md "wikilink")。
  - **misc.\***：其他無法歸入現有層級的討論。
  - **news.\***：關於Usenet本身的討論。
  - **rec.\***：關於娛樂活動的討論（遊戲、愛好等等）。
  - **sci.\***：關於科學方面的討論。
  - **soc.\***：關於社會話題、社會文化的討論。
  - **talk.\***：關於社會及宗教熱點話題的討論。
  - **humanities.\***：關於[人文學科](../Page/人文學科.md "wikilink")（[文學](../Page/文學.md "wikilink")、[哲學等等](../Page/哲學.md "wikilink")）的討論。

加上：

  - **alt.\***：無法歸於其他類別或不願歸入其他類別的話題。
  - **biz.\***：商業話題

**中文Usenet**：

  - **cn.\***：2001年由news.cn99.com發起成立了cn.\*顶级組（The Top CN Hierachy）。
  - **tw.\***：1994年成立，目前大多數的文章由BBS產生，並集中於tw.bbs.\*。
  - **hk.\***：二十世紀九十年代初成立，由[香港中文大學資訊科技服務處架設](../Page/香港中文大學.md "wikilink")。

[Google公司的](../Page/Google公司.md "wikilink")[Google
Groups存檔了幾乎全部的Usenet文章](../Page/Google_Groups.md "wikilink")，主要是從[Deja
News以及其他來源取得更早之前的文章](../Page/Deja_News.md "wikilink")。

## 中國的常用Usenet服务器

  - [新帆](news://news.newsfan.net)
  - [yaako](news://news.yaako.com)
  - [cn99](news://news.cn99.com)
  - [cnusenet](news://news.cnusenet.org)

## 参考文献

## 外部链接

  - [Google新闻组20年存档之重要事件列表](http://www.google.com/googlegroups/archive_announce_20.html)

  - [Google Groups Search](http://groups.google.com)

  - [微软新闻组](news://msnews.microsoft.com)

## 参见

  - [新闻组](../Page/新闻组.md "wikilink")
  - [Google公司](../Page/Google公司.md "wikilink")

{{-}}

[Usenet](../Category/Usenet.md "wikilink")
[Category:1980年面世](../Category/1980年面世.md "wikilink")