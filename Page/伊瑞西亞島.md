在[托爾金](../Page/托爾金.md "wikilink")（J. R. R.
Tolkien）小說裡的[中土大陸](../Page/中土大陸.md "wikilink")，**伊瑞西亞島**（Tol
Eressëa）是一個大島，特有的[梅苓樹](../Page/梅苓樹.md "wikilink")（Mallorn）生長在伊瑞西亞島\[1\]\[2\]。伊瑞西亞島是[精靈語](../Page/精靈語.md "wikilink")，意思是「孤獨島」。伊瑞西亞島原本位於[貝烈蓋爾海](../Page/貝烈蓋爾海.md "wikilink")（Belegaer）中部，距離陸地甚遠。

[烏歐牟](../Page/烏歐牟.md "wikilink")（Ulmo）曾將伊瑞西亞島兩度前後移動，將[精靈轉移至](../Page/精靈_\(中土大陸\).md "wikilink")[阿門洲](../Page/阿門洲.md "wikilink")（Aman），此後不再移動。伊瑞西亞島位於[艾爾達瑪灣](../Page/艾爾達瑪灣.md "wikilink")（Bay
of
Eldamar），[帖勒瑞族](../Page/帖勒瑞族.md "wikilink")（Teleri）住在伊瑞西亞島，直至他們前往[澳闊隆迪](../Page/澳闊隆迪.md "wikilink")（Alqualondë）。

[第一紀元末](../Page/第一紀元.md "wikilink")，許多中土大陸的精靈都前往阿門洲，他們住在伊瑞西亞島。伊瑞西亞島的都城是[艾佛隆尼](../Page/艾佛隆尼.md "wikilink")（Avallónë）。

在托爾金早期的作品裡，[盎格魯-撒克遜旅人到訪伊瑞西亞島](../Page/盎格鲁-撒克逊人.md "wikilink")，為《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》埋下伏線。

## 參考資料

<references />

  - Verlyn Flieger, "Do the Atlantis story and abandon Eriol-Saga"
    Tolkien Studies - Volume 1, 2004, pp. 43–68

[Category:中土大陸的地理](../Category/中土大陸的地理.md "wikilink")

1.
2.