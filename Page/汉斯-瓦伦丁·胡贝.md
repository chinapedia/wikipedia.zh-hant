**汉斯-瓦伦丁·胡贝**（[德语](../Page/德语.md "wikilink")：****，1890年10月29日[瑙姆堡](../Page/瑙姆堡.md "wikilink")
-
1944年4月21日上[萨尔茨堡](../Page/萨尔茨堡.md "wikilink")），[德国将军](../Page/德国.md "wikilink")，[第一次世界大战和](../Page/第一次世界大战.md "wikilink")[第二次世界大战期间服役于](../Page/第二次世界大战.md "wikilink")[德国陆军](../Page/德国陆军.md "wikilink")。

1944年，胡贝因飞机失事而遇难。

## 外部連結

  - [Hans-Valentin
    Hube](http://www.lexikon-der-wehrmacht.de/Personenregister/HubeHV.htm)
    Lexikon-der-Wehrmacht German Language site.

[H](../Category/1890年出生.md "wikilink")
[H](../Category/1944年逝世.md "wikilink")
[H](../Category/德國空難身亡者.md "wikilink")
[H](../Category/德國大將.md "wikilink")