**朱塞佩·法瓦利**（**Giuseppe
Favalli**，），是一名已退役的[意大利](../Page/意大利.md "wikilink")[足球运动员](../Page/足球.md "wikilink")，司职边后卫，也可客串中卫，曾效力于[意甲班霸](../Page/意甲.md "wikilink")[AC米兰](../Page/米兰足球俱乐部.md "wikilink")。

法瓦利早年是[拉素的主力](../Page/拉素體育會.md "wikilink")，曾為球队贏得1999年[欧洲优胜者杯](../Page/欧洲优胜者杯.md "wikilink")，和2000年[意甲聯賽冠軍](../Page/意大利足球甲级联赛.md "wikilink")，在左后卫位置上表现出色。及後由於球會財政困難，於2004年轉投[國際米蘭](../Page/国际米兰足球俱乐部.md "wikilink")。2006年他自由转会至同城对手[AC米兰](../Page/米兰足球俱乐部.md "wikilink")。

2010年夏季，38歲的法瓦利選擇退役，從而結束其長達22年的職業生涯。

## 榮譽

[拉素](../Page/拉素.md "wikilink")

  - [意甲聯賽冠軍](../Page/意甲.md "wikilink"):1999-2000
  - [意大利盃](../Page/意大利盃.md "wikilink"):1998, 2000, 2004
  - [意大利超級盃](../Page/意大利超級盃.md "wikilink"):1998, 2000
  - [歐洲盃賽冠軍盃](../Page/歐洲盃賽冠軍盃.md "wikilink"):1999
  - [歐洲超級盃](../Page/歐洲超級盃.md "wikilink"):1999

[國際米蘭](../Page/國際米蘭.md "wikilink")

  - [意甲聯賽冠軍](../Page/意甲.md "wikilink"):2005-2006
  - [意大利盃](../Page/意大利盃.md "wikilink"):2005,2006
  - [意大利超級盃](../Page/意大利超級盃.md "wikilink"):2005

[AC米蘭](../Page/AC米蘭.md "wikilink")

  - [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink"):2007
  - [歐洲超級盃](../Page/歐洲超級盃.md "wikilink"):2007
  - [國際足協世界冠軍球會盃](../Page/國際足協世界冠軍球會盃.md "wikilink"):2007

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")