**艾倫縣**（**Allen Parish,
Louisiana**）是[美國](../Page/美國.md "wikilink")[印地安納州西南部的一個縣](../Page/印地安納州.md "wikilink")。面積1,983平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口25,440人。縣治[奧柏林](../Page/奧柏林_\(路易斯安那州\).md "wikilink")
(Oberlin)。

成立於1912年6月12日。縣名紀念[州長](../Page/路易斯安那州州長.md "wikilink")、[南北戰爭時期將領](../Page/南北戰爭.md "wikilink")[亨利·沃特金斯·艾倫](../Page/亨利·沃特金斯·艾倫.md "wikilink")
(Henry Watkins Allen)。

[A](../Category/路易斯安那州行政區劃.md "wikilink")