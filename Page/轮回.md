[The_wheel_of_life,_Trongsa_dzong.jpg](https://zh.wikipedia.org/wiki/File:The_wheel_of_life,_Trongsa_dzong.jpg "fig:The_wheel_of_life,_Trongsa_dzong.jpg")[唐卡中的](../Page/唐卡.md "wikilink")[六道輪廻](../Page/六道.md "wikilink")，此唐卡又稱為[有輪](../Page/有輪.md "wikilink")\]\]
**轮回**（），是一种思想理論，認為生命會以不同的面貌和形式，不斷經歷出生、死亡。一般认为这些思想来自东方，但在歐洲亦有轮回观念，即[希臘的輪迴哲學](../Page/輪迴_\(古希臘\).md "wikilink")，例如[畢達哥拉斯及](../Page/畢達哥拉斯.md "wikilink")[柏拉圖等](../Page/柏拉圖.md "wikilink")，和[德魯伊教](../Page/德魯伊.md "wikilink")；作为一种宗教体验，则被认为是世界的另一种真实（感官所认识世界的延伸）。

## 各文化中的概念

### 印度教

輪迴起於《[梵書](../Page/梵書.md "wikilink")》時代，成熟於《[奧義書](../Page/奧義書.md "wikilink")》，是流轉之意。奥义书认为，轮回包摄三道，即天道、祖道、兽道。輪迴是[婆罗门教主要教义之一](../Page/婆罗门教.md "wikilink")，婆罗门教至於[印度教认为四大](../Page/印度教.md "wikilink")[种姓及](../Page/种姓.md "wikilink")[贱民于轮回中生生世世永袭不变](../Page/贱民.md "wikilink")。佛教再將三道扩展为六道四圣，佛教认为，轮回是一个过程，人死去以后，「[識](../Page/識.md "wikilink")」会离开人体，经过一些过程以后进入另一个刚刚出生的新生命体内，该新生命体可以是人类，也可以是[动物](../Page/动物.md "wikilink")、[鬼](../Page/鬼.md "wikilink")、[神](../Page/神.md "wikilink")。到達[涅槃的境界就可擺脫轮回](../Page/涅槃.md "wikilink")。《[法华经](../Page/法华经.md "wikilink")·方便品》：“以诸欲因缘，坠堕三恶道，轮迴六趣中，备受诸苦毒。”这一过程中，一個人當下所存在的狀態称为今生，前一个轮回的生命体称为前世，下一个称为来世或来生。

### 佛教

以[佛學而论](../Page/佛學.md "wikilink")，众生从[无始以来](../Page/无始.md "wikilink")，因為對世間[無常的真相無所了知](../Page/無常.md "wikilink")、或因為對生命的實相不明瞭的[無明](../Page/無明.md "wikilink")，而產生種種導致不斷生死的煩惱未能斷盡，便輾轉生死于[三界](../Page/三界.md "wikilink")[五趣之中](../Page/五趣.md "wikilink")，或稱在[六道中如车轮一样地旋转](../Page/六道.md "wikilink")，即“[六道輪迴](../Page/六道輪迴.md "wikilink")”，至少要修成[阿羅漢](../Page/阿羅漢.md "wikilink")，乃至成[佛](../Page/佛.md "wikilink")，否则无有脱出之期。

由於佛教的修行內涵[三乘菩提中共通的法即是](../Page/三乘菩提.md "wikilink")[解脫道的智慧與修證道理來看](../Page/二乘解脫道.md "wikilink")，一世又一世不斷出生以及老死的[有情眾生](../Page/有情.md "wikilink")，一旦死去的身心，便已壞滅而不復存在於世間，一定有一個能夠串連三世輪迴的生生滅滅的不滅的真實的法。所以在佛教的輪迴觀中，并不涉及[靈魂之說](../Page/靈魂.md "wikilink")，因為佛教認為一般人所謂的[靈魂仍然是](../Page/靈魂.md "wikilink")[有情的](../Page/有情.md "wikilink")[五蘊身](../Page/五蘊.md "wikilink")，仍是會有壽命期限的、終究會壞滅死去的、是無常的。佛學形象地將其比喻為用蠟燭的火點燃另一根蠟燭，蠟燭的相續正如前世、今世、來世，而在其中輪迴的祇有火，每根蠟燭上的火有關聯性，但並非是同一團，故説并非固定的「靈魂」，即所謂「萬般帶不走，唯有[業隨身](../Page/業_\(佛教\).md "wikilink")」。

表現輪迴運作方式的[繪畫稱之為](../Page/繪畫.md "wikilink")[有輪](../Page/有輪.md "wikilink")，又可稱為生命之輪、生死輪、六道輪迴圖等。

### 道教

[道教的传统教义讲求](../Page/道教.md "wikilink")“长生不老”，自[全真道大量吸收佛教教义以来](../Page/全真道.md "wikilink")，也产生了例如佛教的轮回和[转世观念](../Page/转世.md "wikilink")，并对[中国民间信仰影响至今](../Page/中国民间信仰.md "wikilink")。

例如古代善书《[文昌帝君阴骘文](../Page/文昌帝君阴骘文.md "wikilink")》，记载了[文昌帝神通过](../Page/文昌帝.md "wikilink")[扶乩讲述自己从](../Page/扶乩.md "wikilink")[秦朝到](../Page/秦朝.md "wikilink")[宋朝时所轮回转生为何人](../Page/宋朝.md "wikilink")，有何作为等。在道教还有[亡人落道的说法](../Page/亡人落道.md "wikilink")，是用术数的方式推断亡者下一世会落到哪一轮回道。

### 古希腊

[俄耳甫斯教](../Page/俄耳甫斯教.md "wikilink")，這一脈古希臘教派即擁有輪迴轉世觀。

### 其他

[犹太教](../Page/犹太教.md "wikilink")、[基督教](../Page/基督教.md "wikilink")[诺斯替派和](../Page/诺斯替主义.md "wikilink")[伊斯兰教的部分神秘主义教派支持转世说](../Page/伊斯兰教.md "wikilink")，但对转世的观念没有形成类似于印度宗教的轮回观。

## 轮回实例探秘

  - 美国医学博士，[弗吉尼亚大学医学院精神病学教授史蒂文森](../Page/弗吉尼亚大学.md "wikilink")（Ian
    Stevenson）调查了世界各地2500多个声称记得前世的孩子，出版了四卷本研究轮回案例的专著及《轮回与生物学》（Reincarnation
    and
    Biology）等著作。他研究的结论是：轮回确实存在，人在2-5岁左右能够回忆起前世，在5岁以后就会逐渐忘记；前世身体所受的伤害可能是[胎记或先天缺陷的病因](../Page/胎记.md "wikilink")，人的喜好、行为特征以及恐惧症等都可能来源于前世。

<!-- end list -->

  - 曾任[耶鲁大学精神科主治医师的](../Page/耶鲁大学.md "wikilink")[布莱恩·魏斯](../Page/布莱恩·魏斯.md "wikilink")（BRIAN
    L. WEISS, M.D.）博士所著《前世今生》（英文版书名《Many Lives, Many Masters》）、《Same
    Soul, Many Bodies》、《Through Time Into Healing》等著作，记录了他执业生涯中的大量轮回案例。

<!-- end list -->

  - 著名的[探索頻道](../Page/探索頻道.md "wikilink")（[Discovery
    Channel](../Page/Discovery_Channel.md "wikilink")），摄制了关于探索轮回的[纪录片](../Page/纪录片.md "wikilink")《前生往世》（PAST
    LIVES），[Youtube网站上有该纪录片的中文字幕版](../Page/Youtube.md "wikilink")\[1\]。

<!-- end list -->

  - [英国广播公司](../Page/英国广播公司.md "wikilink")**BBC**的“学习”节目（Learning），也摄制了关于探索轮回的纪录片《往世》（Previous
    Lives）。Youtube网站有该片的英语版\[2\]。

<!-- end list -->

  - [怀化新闻网记者对](../Page/怀化.md "wikilink")[通道侗族自治县](../Page/通道侗族自治县.md "wikilink")[坪阳乡的](../Page/坪阳乡.md "wikilink")100多例“再生人”现象进行了实地探秘。\[3\]

## 参见

  - [瀕死經驗](../Page/瀕死經驗.md "wikilink")
  - [六道輪迴](../Page/六道.md "wikilink")
  - [十法界](../Page/十法界.md "wikilink"):[四聖](../Page/四聖.md "wikilink")[六凡](../Page/六凡.md "wikilink")
  - [轉世](../Page/轉世.md "wikilink")
  - [涅槃](../Page/涅槃.md "wikilink")
  - [灵童转世](../Page/灵童转世.md "wikilink")
  - [復活](../Page/復活.md "wikilink")
  - [火影忍者](../Page/火影忍者.md "wikilink")
  - [前世回溯](../Page/前世回溯.md "wikilink")
  - [有輪](../Page/有輪.md "wikilink")

## 参考资料

[Category:印度教](../Category/印度教.md "wikilink")
[Category:佛教術語](../Category/佛教術語.md "wikilink")
[Category:藏傳佛教](../Category/藏傳佛教.md "wikilink")
[Category:死後生命](../Category/死後生命.md "wikilink")
[Category:宗教學](../Category/宗教學.md "wikilink")

1.  [**探索頻道Discovery Channel**中文字幕版纪录片: PAST
    LIVES](http://www.youtube.com/watch?v=dz-fOKoehX8)
2.  [英国广播公司BBC英语版纪录片《往世》: Previous
    Lives](http://www.youtube.com/watch?v=iq64XvwqzyQ)
3.  [怀化坪阳乡有100多个“再生人”](http://news.zhengjian.org/node/13363)