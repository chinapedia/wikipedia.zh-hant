**尤尔根·哈贝马斯**（，），[德国当代最重要的](../Page/德国.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[社會學家之一](../Page/社會學家.md "wikilink")。历任[海德堡大学教授](../Page/海德堡大学.md "wikilink")、[法兰克福大学教授](../Page/法兰克福大学.md "wikilink")、法兰克福大学社会研究所所长以及[马克斯普朗克学会生活世界研究所所长](../Page/马克斯普朗克学会.md "wikilink")。1994年荣休。他同时也是西方马克思主义[法兰克福学派第二代的中坚人物](../Page/法兰克福学派.md "wikilink")。他继承和发展了[康德哲学](../Page/伊曼努尔·康德.md "wikilink")，致力於重建“启蒙”傳統，視[现代性为](../Page/现代性.md "wikilink")“尚未完成之工程”，提出了著名的[溝通理性](../Page/溝通理性.md "wikilink")（communicative
rationality）的理论，对[后现代主义思潮进行了深刻的對話及有力的批判](../Page/后现代主义.md "wikilink")。

## 生平

哈伯马斯1929年6月18日生于[威瑪共和國的](../Page/威瑪共和國.md "wikilink")[杜塞道夫一個中產家庭](../Page/杜塞道夫.md "wikilink")。因為天生患有[唇顎裂](../Page/唇顎裂.md "wikilink")(俗稱兔唇)，童年時曾兩度接受矯形手術；亦因為這個先天缺陷的關係，使他無法清楚咬字，並嚴重影響到他的社交生活。哈伯瑪斯認為，他的語言障礙使得他以不同方式去思考溝通的重要性，並且喜歡上以書寫口語作為溝通媒介。

## 知識旨趣與社會實踐

哈伯马斯在知识论上的主張是：任何一个认识都起源于旨趣(knowledge interest)。他提出人类旨趣的三种类型\[1\]：

1.  「经验─分析的科学研究」包含技术的认知旨趣
2.  「历史－解释学的科学研究」包含实践的认知旨趣　
3.  具有批判倾向的科学（critically oriented）的研究，包含解放的认知旨趣

這些旨趣的整理分梳試圖解決啓蒙運動以來工具理性（instrumental
rationality）之流弊,為左派批判思想找尋有別於後現代運動對理性棄如敝屣的態度。

## 公共領域的結構转型

## 溝通行為理論

  - 真實性（Truth）
  - 理解性（comprehensibility）
  - 真誠（Truthfulness）
  - 公正（rightness）

## 學術與社會參與

1999年德国决定出兵科索沃，哈伯马斯发表长篇文章，从政治伦理角度支持[约施卡·菲舍尔和这个违背绿党政纲的出兵决定](../Page/约施卡·菲舍尔.md "wikilink")。
2003年4～5月间，哈伯马斯两次发表声明，反对伊拉克战争并支持加速欧洲一体化。

哈伯马斯于2001年4月访华，在[北京和](../Page/北京.md "wikilink")[上海两地做了题为](../Page/上海.md "wikilink")《全球化压力下的欧洲民族国家》等一系列的演讲，在中国学术界反响巨大。

## 参考文献

## 研究書目

  - 阮新邦：《批判詮釋與知識重建：哈伯瑪斯視野下的社會研究》（北京：社會科學文獻出版社，1999年）.
  - 陳曉林：《學術巨人與理性困境——韋伯、巴柏、哈伯瑪斯》（臺北：時報出版公司，1987年）.

## 外部链接

  - [現代理性困境與救贖
    ,哈伯馬斯對西方理性主義的重建](http://life.fhl.net/Philosophy/bookclub/society/06.htm)
  - [哈貝馬斯：公共空間與政治公共領域](https://web.archive.org/web/20110402083033/http://www.taiwanpost.com/online/2011/03/333.html)
  - 陳弘毅：[〈从哈貝馬斯的哲學看現代性與現代法治〉](http://www.aisixiang.com/data/67209.html)（2013）
  - 劉擎：[〈現代性的哲學話語：哈貝馬斯的思想史視野〉](http://www.aisixiang.com/data/11653.html)（2006）
  - 張汝倫：[〈評哈貝馬斯對全球化政治的思考〉](http://www.aisixiang.com/data/32409.html)（2010）
  - [Habermas Forum](http://www.habermasforum.dk)

[Category:20世纪哲学家](../Category/20世纪哲学家.md "wikilink")
[Category:德國社會學家](../Category/德國社會學家.md "wikilink")
[Category:德国哲学家](../Category/德国哲学家.md "wikilink")
[Category:語言哲學家](../Category/語言哲學家.md "wikilink")
[Category:政治哲學家](../Category/政治哲學家.md "wikilink")
[Category:社會哲學](../Category/社會哲學.md "wikilink")
[Category:馬克思主義理論家](../Category/馬克思主義理論家.md "wikilink")
[Category:法蘭克福學派](../Category/法蘭克福學派.md "wikilink")
[Category:法蘭克福大學教師](../Category/法蘭克福大學教師.md "wikilink")
[Category:海德堡大學教師](../Category/海德堡大學教師.md "wikilink")
[Category:馬爾堡大學教師](../Category/馬爾堡大學教師.md "wikilink")
[Category:馬爾堡大學校友](../Category/馬爾堡大學校友.md "wikilink")
[Category:波恩大學校友](../Category/波恩大學校友.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:伊拉斯谟奖得主](../Category/伊拉斯谟奖得主.md "wikilink")
[Category:郝尔拜奖获得者](../Category/郝尔拜奖获得者.md "wikilink")
[Category:克鲁格奖获得者](../Category/克鲁格奖获得者.md "wikilink")
[Category:京都奖获得者](../Category/京都奖获得者.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")
[Category:杜塞爾多夫人](../Category/杜塞爾多夫人.md "wikilink")

1.  Habermas, Jürgen. *Knowledge and Human Interests*. Trans. Jeremy
    Shapiro. Boston:Beacon Press, 1971.