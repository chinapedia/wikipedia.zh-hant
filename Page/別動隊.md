[Execution_of_Poles_by_German_Einsatzkommando_Oktober_1939.jpg](https://zh.wikipedia.org/wiki/File:Execution_of_Poles_by_German_Einsatzkommando_Oktober_1939.jpg "fig:Execution_of_Poles_by_German_Einsatzkommando_Oktober_1939.jpg")

**別動隊**（，Einsatz是投入或出動兵力的意思，gruppen解释为分队），全稱**特別行動隊**，又名**突击队**、**行刑队**。是[纳粹德国占领区](../Page/纳粹德国.md "wikilink")[党卫队中的一等兵组成的部队](../Page/党卫队.md "wikilink")。一等兵也叫「突击队员」，所以别动队也叫突击队。他们的任务是大规模执行抓捕、屠杀、搜索的部队。与[骷髅总队一样](../Page/骷髅总队.md "wikilink")，别动队也是泛称，因为每个地区的普通[親衛隊或](../Page/親衛隊.md "wikilink")[武裝親衛隊都有一等兵](../Page/武裝親衛隊.md "wikilink")。早期，別動隊的任務為在[納粹歐洲抓捕並殺害](../Page/納粹歐洲.md "wikilink")[猶太人](../Page/猶太人.md "wikilink")，尤其在[蘇聯境內留下許多大量屠殺的影像](../Page/蘇聯.md "wikilink")。

别动队不是战斗部队。他们只对付[手无寸铁的](../Page/非戰鬥人員.md "wikilink")[平民](../Page/平民.md "wikilink")，并且公开大规模行动，这是与[盖世太保有区别的地方](../Page/盖世太保.md "wikilink")。\[1\]\[2\]\[3\]

## 參考資料

[Category:纳粹党卫队](../Category/纳粹党卫队.md "wikilink")
[Category:德语词汇或短语](../Category/德语词汇或短语.md "wikilink")

1.
2.
3.