**何塞‧路易斯‧奇拉维特**（**José Luis Félix Chilavert
González**，），[巴拉圭人](../Page/巴拉圭.md "wikilink")，世界知名的足球守門員，現已退役。

奇拉维特早年在[阿根廷聯賽成名](../Page/阿根廷.md "wikilink")，曾效力過西班牙球會[薩拉戈薩](../Page/皇家薩拉戈薩.md "wikilink")，1991年返回阿根廷效力[沙士菲](../Page/薩斯菲爾德足球俱樂部.md "wikilink")。很多人對芝拉華特除了是撲球能力外，還有是他的罰球絕技。

職業生涯中一共射入67球，全部都是定位球，有比其他守門員極佳的腳下功夫，曾經在一場比賽中踢進距離球門長達60米的驚天吊射，也曾在自家球門前用帶球技巧戲耍對手，他的長傳也堪稱一絕，時常幫助球隊發起進攻。

他曾代表[巴拉圭國家隊出戰](../Page/巴拉圭國家足球隊.md "wikilink")[1998年和](../Page/1998年世界盃足球賽.md "wikilink")[2002年世界盃](../Page/2002年世界盃足球賽.md "wikilink")，憑他的坐鎮令巴拉圭擁有出色的防守。而他在國際賽亦有8個入球。

芝拉華特除了其出色的球技外，其場內場外的火爆脾气眾所周知。事實上他曾有多次與球员、球證爭執的紀錄，常都被罰停賽。故一直以來他主要都是效力阿根廷聯賽球會，曾有利物浦，曼联等欧洲球会求购，但最终都在价钱上未能达成一致。2003年他宣佈退出國家隊，2004年宣佈退役。

## 榮譽

### [瓜拉尼足球俱樂部](../Page/瓜拉尼足球俱樂部.md "wikilink")

  - [巴拉圭甲級聯賽](../Page/巴拉圭甲級聯賽.md "wikilink")：1984年

### [沙士菲足球俱樂部](../Page/沙士菲足球俱樂部.md "wikilink")

  - [南美春季聯賽](../Page/南美春季聯賽.md "wikilink")：1995年
  - [阿根廷足球甲級聯賽](../Page/阿根廷足球甲級聯賽.md "wikilink")：1993年，1996年，1998年
  - [南美解放者盃](../Page/南美解放者盃.md "wikilink")：1994年
  - [洲際盃](../Page/洲際盃.md "wikilink")：1994年
  - [南美超級球會盃](../Page/南美超級球會盃.md "wikilink")：1996年
  - [南美優勝者盃](../Page/南美優勝者盃.md "wikilink")：1997年

### [斯特拉斯堡競賽會](../Page/斯特拉斯堡競賽會.md "wikilink")

  - [法國盃](../Page/法國盃.md "wikilink")：2001年

### [彭拿路足球俱樂部](../Page/彭拿路足球俱樂部.md "wikilink")

  - [烏拉圭甲級聯賽](../Page/烏拉圭甲級聯賽.md "wikilink")：2003年

## 個人

1995、1997、1998年世界最佳门将（注：当时国际足联授权IFFHS评选）

1996年阿根廷足球先生、南美足球先生

1998年世界杯最佳阵容

1994到1999年一直入选南美最佳阵容

1999年阿根廷联赛萨斯菲尔德对阵费罗卡里尔时完成门将“帽子戏法”

20世纪最佳门将第6名（国际足球历史和统计联合会）

20世纪最伟大的100名足球运动员（世界足球评选）

20世纪南美最佳门将（国际足联评选）

[Category:巴拉圭足球運動員](../Category/巴拉圭足球運動員.md "wikilink")
[Category:聖羅倫素球員](../Category/聖羅倫素球員.md "wikilink")
[Category:薩拉戈薩球員](../Category/薩拉戈薩球員.md "wikilink")
[Category:沙士菲球員](../Category/沙士菲球員.md "wikilink")
[Category:斯特拉斯堡球員](../Category/斯特拉斯堡球員.md "wikilink")
[Category:彭拿路球員](../Category/彭拿路球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:1991年美洲盃球員](../Category/1991年美洲盃球員.md "wikilink")
[Category:1993年美洲盃球員](../Category/1993年美洲盃球員.md "wikilink")
[Category:1997年美洲盃球員](../Category/1997年美洲盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:Burning
Production系列所屬藝人](../Category/Burning_Production系列所屬藝人.md "wikilink")