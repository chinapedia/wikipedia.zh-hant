**胡静**（，），[中国大陆](../Page/中国大陆.md "wikilink")[女演员](../Page/女演员.md "wikilink")，出生于[云南](../Page/云南.md "wikilink")[昆明](../Page/昆明.md "wikilink")，2000年毕业于[中央戏剧学院](../Page/中央戏剧学院.md "wikilink")，获[学士学位](../Page/学士.md "wikilink")。于2008年9月27日嫁给[大马富商朱兆祥](../Page/马来西亚.md "wikilink")\[1\]\[2\]。

2010年10月16日，朱兆祥在[吉隆坡苏丹皇宫受封为](../Page/吉隆坡.md "wikilink")[拿督](../Page/拿督.md "wikilink")，作为其妻的胡静也因而受封为[拿汀](../Page/拿汀.md "wikilink")，成为第一位中国拿汀。\[3\]

## 学历

  - 1990-1996年：[中央民族大学舞蹈系中专班民族舞专业](../Page/中央民族大学.md "wikilink")。
  - 1996-2000年：[中央戏剧学院表演系本科](../Page/中央戏剧学院.md "wikilink")。特长民族舞蹈、话剧及音乐。

## 影视作品

### 电视剧

|        |                                                   |                                    |                                                                                                                                                                                                                                                                 |
| ------ | ------------------------------------------------- | ---------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **年份** | **剧名**                                            | **角色**                             | **合作演員**                                                                                                                                                                                                                                                        |
| 2000年  | 《[大明宫词](../Page/大明宫词.md "wikilink")》              | 少年[韦氏](../Page/韦后.md "wikilink")   |                                                                                                                                                                                                                                                                 |
| 2000年  | 《[聊斋先生](../Page/聊斋先生.md "wikilink")》              | [婴宁](../Page/婴宁.md "wikilink")     |                                                                                                                                                                                                                                                                 |
| 2000年  | 《[败家子](../Page/败家子.md "wikilink")》                | 多情                                 |                                                                                                                                                                                                                                                                 |
| 2001年  | 《[少年張三丰](../Page/少年張三丰.md "wikilink")》            | 凌雪雁                                |                                                                                                                                                                                                                                                                 |
| 2001年  | 《[大龙邮票](../Page/大龙邮票.md "wikilink")》              | 荣格格                                |                                                                                                                                                                                                                                                                 |
| 2002年  | 《[齐天大圣孙悟空](../Page/齐天大圣孙悟空.md "wikilink")》        | [女儿国国王](../Page/女儿国.md "wikilink") |                                                                                                                                                                                                                                                                 |
| 2002年  | 《[孝庄秘史](../Page/孝庄秘史.md "wikilink")》              | [苏茉儿](../Page/苏麻喇姑.md "wikilink")  |                                                                                                                                                                                                                                                                 |
| 2002年  | 《[六女当铺](../Page/六女当铺.md "wikilink")》              | 黄梅                                 |                                                                                                                                                                                                                                                                 |
| 2002年  | 《[十八罗汉](../Page/十八罗汉.md "wikilink")》              | 侠女红萧                               |                                                                                                                                                                                                                                                                 |
| 2003年  | 《[皇太子秘史](../Page/皇太子秘史.md "wikilink")》            | 紫瑛                                 |                                                                                                                                                                                                                                                                 |
| 2003年  | 《[杨门虎将](../Page/杨门虎将.md "wikilink")》              | 明姬公主                               |                                                                                                                                                                                                                                                                 |
| 2004年  | 《[魔界](../Page/魔界.md "wikilink")》                  | 天香                                 |                                                                                                                                                                                                                                                                 |
| 2005年  | 《[别让眼泪流过夜](../Page/别让眼泪流过夜.md "wikilink")》        | 杨佩玲                                |                                                                                                                                                                                                                                                                 |
| 2005年  | 《[欢颜](../Page/欢颜.md "wikilink")》                  | 许柯茹                                |                                                                                                                                                                                                                                                                 |
| 2005年  | 《[大清后宫](../Page/大清后宫.md "wikilink")》              | 西林覺羅·春兒                            |                                                                                                                                                                                                                                                                 |
| 2005年  | 《[权利背后](../Page/权利背后.md "wikilink")》              |                                    | [刘恺威](../Page/刘恺威.md "wikilink")、[任程伟](../Page/任程伟.md "wikilink")、[潘虹](../Page/潘虹.md "wikilink")、[陈刚](../Page/陈刚.md "wikilink")、[冯淬帆](../Page/冯淬帆.md "wikilink")、[姚鲁](../Page/姚鲁.md "wikilink")、[吴雨桥](../Page/吴雨桥.md "wikilink")、[米艾](../Page/米艾.md "wikilink") |
| 2006年  | 《[康熙秘史](../Page/康熙秘史.md "wikilink")》              | 青格儿                                |                                                                                                                                                                                                                                                                 |
| 2006年  | 《[楚留香傳奇](../Page/楚留香傳奇.md "wikilink")》            | [蘇蓉蓉](../Page/蘇蓉蓉.md "wikilink")   |                                                                                                                                                                                                                                                                 |
| 2008年  | 《女人不悔》（又名《[女人的战争](../Page/女人的战争.md "wikilink")》）  | 朱杏梅                                | 潘虹                                                                                                                                                                                                                                                              |
| 2009年  | 《[兵圣](../Page/兵圣.md "wikilink")》                  | 紫苏                                 |                                                                                                                                                                                                                                                                 |
| 2012年  | 《[笑紅顏](../Page/笑紅顏.md "wikilink")》(又名:《鬥紅顏》)      |                                    |                                                                                                                                                                                                                                                                 |
| 2013年  | 《[泪洒女人花](../Page/泪洒女人花.md "wikilink")》            | 杨素云                                |                                                                                                                                                                                                                                                                 |
| 2016年  | 《[老爸的梦想](../Page/老爸的梦想.md "wikilink")》            | 牛尔婵                                |                                                                                                                                                                                                                                                                 |
| 2017年  | 《[人民的名义](../Page/人民的名义.md "wikilink")》            | 高小琴/高小凤                            |                                                                                                                                                                                                                                                                 |
| 2017年  | 《[小情人](../Page/小情人.md "wikilink")》                | 青红                                 |                                                                                                                                                                                                                                                                 |
| 2018年  | 《[橙红年代](../Page/橙红年代.md "wikilink")》              | 丽莎                                 | 客串                                                                                                                                                                                                                                                              |
| 2019年  | 《[天衣无缝](../Page/天衣无缝_\(电视剧\).md "wikilink")》      | 陈萱玉                                |                                                                                                                                                                                                                                                                 |
| 2019年  | 《[封神演义](../Page/封神演义_\(2019年电视剧\).md "wikilink")》 | [姜皇后](../Page/姜皇后.md "wikilink")   |                                                                                                                                                                                                                                                                 |
| 待播中    | 《[抓紧时间爱](../Page/抓紧时间爱.md "wikilink")》            | 林嘉薇                                |                                                                                                                                                                                                                                                                 |
| 待播中    | 《[爱情去哪儿了](../Page/爱情去哪儿了.md "wikilink")》          |                                    |                                                                                                                                                                                                                                                                 |
| 待播中    | 《向警予》                                             | [向警予](../Page/向警予.md "wikilink")   |                                                                                                                                                                                                                                                                 |
| 待播中    | 《[决胜法庭](../Page/决胜法庭.md "wikilink")》              |                                    |                                                                                                                                                                                                                                                                 |
| 待播中    | 《新世界》                                             |                                    |                                                                                                                                                                                                                                                                 |
|        |                                                   |                                    |                                                                                                                                                                                                                                                                 |

### 電影

|        |                                                  |        |                                                                                                  |
| ------ | ------------------------------------------------ | ------ | ------------------------------------------------------------------------------------------------ |
| **年份** | **片名**                                           | **角色** | **合作演員**                                                                                         |
| 1999年  | 《[網路時代的愛情](../Page/網路時代的愛情.md "wikilink")》       |        |                                                                                                  |
| 2005年  | 《[千杯不醉](../Page/千杯不醉.md "wikilink")》             |        | [吳彥祖](../Page/吳彥祖.md "wikilink")、[楊千嬅](../Page/楊千嬅.md "wikilink")                                |
| 2005年  | 《[左麟右李之我愛醫家人](../Page/左麟右李之我愛醫家人.md "wikilink")》 |        | [譚詠麟](../Page/譚詠麟.md "wikilink")、[李克勤](../Page/李克勤.md "wikilink")                                |
| 2006年  | 《[天行者](../Page/天行者.md "wikilink")》               |        | [鄭伊健](../Page/鄭伊健.md "wikilink")、[方中信](../Page/方中信.md "wikilink")、[狄龙](../Page/狄龙.md "wikilink") |
| 2010年  | 《[爱情维修站](../Page/爱情维修站.md "wikilink")》           |        |                                                                                                  |
| 2011年  | 《[我知女人心](../Page/我知女人心.md "wikilink")》           |        | [刘德华](../Page/刘德华.md "wikilink")、[巩俐](../Page/巩俐.md "wikilink")                                  |
| 2012年  | 《[跑出一片天](../Page/跑出一片天.md "wikilink")》           |        |                                                                                                  |
| 2014年  | 《[催眠大師](../Page/催眠大師.md "wikilink")》             |        |                                                                                                  |
| 2015年  | 《[巴黎假期](../Page/巴黎假期.md "wikilink")》             |        |                                                                                                  |
| 2016年  | 《[大話西遊3](../Page/大话西游3_\(电影\).md "wikilink")》    | 觀音菩薩   |                                                                                                  |
|        |                                                  |        |                                                                                                  |

### 音樂

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>2007年</p></td>
<td style="text-align: left;"><p>《<a href="../Page/天蠍座_(專輯).md" title="wikilink">天蠍座</a>》</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 话剧

  - [梁祝](../Page/梁祝.md "wikilink")
  - [费加罗的婚礼](../Page/费加罗的婚礼.md "wikilink")
  - 2001年：[孔乙己正传](../Page/孔乙己正传.md "wikilink")

### 监制

  - 2010年 网络时装情景剧《[两个女孩的那些事](../Page/两个女孩的那些事.md "wikilink")》
    主演:[巩新亮](../Page/巩新亮.md "wikilink")、[熊乃瑾](../Page/熊乃瑾.md "wikilink")

## 曾获奖项

|            |                                                                                        |
| ---------- | -------------------------------------------------------------------------------------- |
| **年份**     | **奖项**                                                                                 |
| 1994年      | [北京市舞蹈比赛二等奖](../Page/北京.md "wikilink")                                                 |
| 1996-1998年 | 连续两年获得[中央戏剧学院专业单项奖](../Page/中央戏剧学院.md "wikilink")                                      |
| 1998-1999年 | 获得[中央戏剧学院三等](../Page/中央戏剧学院.md "wikilink")[奖学金](../Page/奖学金.md "wikilink")，专业单项奖       |
| 1999-2000年 | 获得[中央戏剧学院](../Page/中央戏剧学院.md "wikilink")“[三好学生](../Page/三好学生.md "wikilink")”称号，获得一等奖学金 |

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  -
  -
  - [胡静](http://hujing1.blog.sohu.com/)的[搜狐博客](../Page/搜狐.md "wikilink")

  -
  -
  -
  -
  -
[J静](../Page/category:胡姓.md "wikilink")
[category:云南人](../Page/category:云南人.md "wikilink")
[category:昆明人](../Page/category:昆明人.md "wikilink")

[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:云南演员](../Category/云南演员.md "wikilink")
[Category:中央民族大学校友](../Category/中央民族大学校友.md "wikilink")
[Category:中央戏剧学院校友](../Category/中央戏剧学院校友.md "wikilink")

1.  [胡靜
    嫁大馬富商](http://udn.com/NEWS/ENTERTAINMENT/ENT2/4538780.shtml)《聯合報》聯合追星網，2008年9月30日
2.  [世紀婚禮 胡靜今嫁大馬首富 老公朱兆祥身家50億
    能嫁豪門因身家清白](http://www.worldjournal.com/wj-en-news.php?nt_seq_id=1780533)
    《世界日報》2008年9月27日
3.  [胡静成史上第一位中国籍拿汀 老公大马受封拿督](http://ent.qq.com/a/20101018/000024.htm)