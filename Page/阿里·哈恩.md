**阿里·汉**（，）出生于[荷兰费斯达罗德](../Page/荷兰.md "wikilink")（），是一名退役[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，退役后担任足球教练。

## 球員生涯

阿里汉是1970年代两届世界杯亚军的[荷兰队成员之一](../Page/荷兰国家足球队.md "wikilink")，共上阵35场及射入6球。在俱乐部层面上更为成功，出身[-{zh-hans:阿贾克斯;zh-hant:阿积士}-](../Page/阿贾克斯.md "wikilink")，是三夺[欧冠杯的功臣之一](../Page/欧联.md "wikilink")，其它效力的球队包括[-{zh-hans:安德莱赫特;zh-hant:安德烈治}-](../Page/安德莱赫特足球俱乐部.md "wikilink")、[标准列日及](../Page/标准列日.md "wikilink")[-{zh-hans:埃因霍温;zh-hant:PSV燕豪芬}-等大球会](../Page/PSV燕豪芬.md "wikilink")。

阿里汉最著名的入球是[1978年阿根廷世界杯在次轮分组赛最后一场对](../Page/1978年世界杯足球赛.md "wikilink")[意大利](../Page/意大利国家足球队.md "wikilink")，下半场75分钟海恩一记40米长距离远射击破意大利大门，以2-1获胜，小组力压意大利及[西德两支强队晋级决赛对主办国](../Page/德国国家足球队.md "wikilink")[阿根廷](../Page/阿根廷国家足球队.md "wikilink")。

1984年，阿里汉赴[香港效力](../Page/香港.md "wikilink")[精工](../Page/精工足球隊.md "wikilink")\[1\]，為球隊取得聯賽及盃賽三料冠軍，被譽為香港聯賽歷來最佳外援之一，他於1985年宣佈退休。

## 教练生涯

阿里汉退役后曾到过不同的地方如[比利时](../Page/比利时.md "wikilink")、[德国](../Page/德国.md "wikilink")、[希腊及出生地](../Page/希腊.md "wikilink")[荷兰担任球队教练](../Page/荷兰.md "wikilink")，亦曾短暂逗留过[奥地利及](../Page/奥地利.md "wikilink")[伊朗任教](../Page/伊朗.md "wikilink")。于2002年执教[中国国家足球队两年](../Page/中国国家足球队.md "wikilink")，率队在2004年中国本土举办的[亚洲杯上获得亚军](../Page/2004年亚洲杯足球赛.md "wikilink")，其执教中国队的国际A级比赛成绩为30战17胜7平6负。2006年出任[喀麦隆国家足球队主教练](../Page/喀麦隆国家足球队.md "wikilink")，但2007年2月即因为和喀麦隆足协负责人的矛盾而辞职，至2008年，才转任阿尔巴尼亚足球队教练一职，因战绩不佳于2009年4月被解雇。

2009年5月已经与[重庆力帆草签工作合同](../Page/重庆力帆.md "wikilink")，重返中国，6月正式执教[重庆力帆](../Page/重庆力帆.md "wikilink")。2009年11月30日，他和泰达俱乐部达成协议，成为[天津泰达队的主帅](../Page/天津泰达.md "wikilink")\[2\]。

2010年阿里汉带领天津泰达在[中超联赛获得亚军](../Page/中超联赛.md "wikilink")，2011年天津泰达获得[中国足协杯冠军](../Page/中国足协杯.md "wikilink")，并成为唯一一支晋级[亚冠联赛淘汰赛的中国球队](../Page/亚冠联赛.md "wikilink")。2011年赛季结束后，阿里汉不与天津泰达续约。

2011年12月，[中甲球队](../Page/中甲.md "wikilink")[沈阳沈北宣布阿里汉担任球队主教练兼青训总监](../Page/沈阳沈北.md "wikilink")，合同为期三年。2012年5月1日，在联赛前7轮仅收获3平4负的沈阳沈北队宣布正式接受阿里汉的辞职，\[3\]阿里汉是次执教仅维持4个月。

2014年1月12日，[天津泰达宣布阿里汉再次担任球队主教练](../Page/天津泰达.md "wikilink")。\[4\]

## 參考資料

## 外部連結

  - [-{zh-hans:阿里·哈恩;zh-hant:海恩}-簡歷](http://www.beijen.net/frank/cvs/haan-a.htm)
  - [荷兰重炮：阿里汉](http://www.southcn.com/sports/2006wc/celebrity/200604100328.htm)

[Category:1978年世界盃足球賽球員](../Category/1978年世界盃足球賽球員.md "wikilink")
[Category:1974年世界盃足球賽球員](../Category/1974年世界盃足球賽球員.md "wikilink")
[Category:1980年歐洲國家盃球員](../Category/1980年歐洲國家盃球員.md "wikilink")
[Category:阿爾巴尼亞國家足球隊主教練](../Category/阿爾巴尼亞國家足球隊主教練.md "wikilink")
[Category:喀麥隆國家足球隊主教練](../Category/喀麥隆國家足球隊主教練.md "wikilink")
[Category:中國國家足球隊主教練](../Category/中國國家足球隊主教練.md "wikilink")
[Category:中国外籍教练](../Category/中国外籍教练.md "wikilink")
[Category:荷蘭足球主教練](../Category/荷蘭足球主教練.md "wikilink")
[Category:史特加主教練](../Category/史特加主教練.md "wikilink")
[Category:紐倫堡主教練](../Category/紐倫堡主教練.md "wikilink")
[Category:飛燕諾主教練](../Category/飛燕諾主教練.md "wikilink")
[Category:荷兰足球运动员](../Category/荷兰足球运动员.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:安德列治球員](../Category/安德列治球員.md "wikilink")
[Category:標準列治球員](../Category/標準列治球員.md "wikilink")
[Category:PSV燕豪芬球員](../Category/PSV燕豪芬球員.md "wikilink")
[Category:精工球員](../Category/精工球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:比甲球員](../Category/比甲球員.md "wikilink")
[Category:安特衛普主教練](../Category/安特衛普主教練.md "wikilink")
[Category:安德列治主教練](../Category/安德列治主教練.md "wikilink")
[Category:標準列治主教練](../Category/標準列治主教練.md "wikilink")
[Category:PAOK主教練](../Category/PAOK主教練.md "wikilink")
[Category:奧莫尼亞主教練](../Category/奧莫尼亞主教練.md "wikilink")
[Category:奧地利維也納主教練](../Category/奧地利維也納主教練.md "wikilink")
[Category:柏斯波利斯主教練](../Category/柏斯波利斯主教練.md "wikilink")
[Category:天津泰达教练](../Category/天津泰达教练.md "wikilink")
[Category:德甲主教練](../Category/德甲主教練.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[H](../Category/格羅寧根省人.md "wikilink")
[Category:香港外籍足球運動員](../Category/香港外籍足球運動員.md "wikilink")

1.  [精工歷屆的超強陣容](http://albertchh.sinaman.com/seiko_main.htm)
2.  [阿里汉40万美金入主泰达
    主动承诺"带队进前四"](http://sports.163.com/09/1201/11/5PEOPO9M00051C89.html)
3.  [沈北官方宣布阿里汉下课
    李铮暂接任大羽进教练组](http://sports.sina.com.cn/b/2012-05-01/16516045111.shtml)
4.