（）出生于[日本](../Page/日本.md "wikilink")[群马县](../Page/群马县.md "wikilink")[中之条町](../Page/中之條町.md "wikilink")，日本[政治人物](../Page/政治人物.md "wikilink")。毕业于[早稻田大学](../Page/早稻田大学.md "wikilink")[文学部](../Page/文學.md "wikilink")，第84任[日本內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")，属[自由民主党](../Page/自由民主党_\(日本\).md "wikilink")。

他在日本有個外號叫作“平成大叔”，這是因為他在担任[竹下登内阁的](../Page/竹下登.md "wikilink")[官房长官时](../Page/内阁官房长官.md "wikilink")，[昭和天皇駕崩的當天下午的記者會上](../Page/昭和天皇.md "wikilink")，由他公佈了新的年號“[平成](../Page/平成.md "wikilink")”，並恭敬地展示了寫有新年號的紙板。鮮為人知的是，他也是個擁有[呼號](../Page/無線電台呼號.md "wikilink")“JI1KIT”的[業餘無線電玩家](../Page/業餘無線電.md "wikilink")，曾任國會業餘無線電俱樂部的會長。

## 經歷

  - 1937年：出生
  - 1963年：眾議院選舉初當選
  - 1979年：就任第二次大平内閣的總理府總務長官、[沖繩開發廳長官](../Page/沖繩.md "wikilink")
  - 1986年：就任眾議院預算委員長
  - 1987年：就任[竹下内閣的官房長官](../Page/竹下內閣.md "wikilink")
  - 1991年：就任自由民主黨幹事長（黨總裁[海部俊樹](../Page/海部俊樹.md "wikilink")）
  - 1994年：就任自民党副總裁
  - 1997年：就任第二次橋本改造内閣的外務大臣。
  - 1998年：當選自民黨第18任總裁及第84任内閣總理大臣。
  - 2000年：病逝

## 從政

### 政次與入閣

小渊惠三其父原为[群馬縣選出的](../Page/群馬縣.md "wikilink")[眾議院議員](../Page/眾議院.md "wikilink")，故他在群馬縣有一些地方勢力。小淵光平多次連任眾議院议员，但在最後一次連任後的三個月，即1958年8月26日在國會回家的路上突然[中風](../Page/中風_\(中醫\).md "wikilink")，送醫後逝世。

去世後，小淵惠三在1960年中還沒有被選舉權。當時前參議院議員（曾任群馬縣知事與[長野縣知事](../Page/長野縣.md "wikilink")）参加竞选，但是最後落選。小淵惠三為了奪回伊能芳雄未能拿到的議院议员，而進入政壇。

1963年11月，小渊惠三就讀[大學時](../Page/大學.md "wikilink")，参加第30回眾議院議員總選舉。他受到自民黨推薦参加3區竞选，获得4萬7350票而首次當選（4個議席中排名第三位），時年26歲。同期当选的還有[橋本龍太郎](../Page/橋本龍太郎.md "wikilink")、[中川一郎](../Page/中川一郎.md "wikilink")、、、[伊東正義與](../Page/伊東正義.md "wikilink")等人。1967年，小渊惠三與[大野千鶴子結婚](../Page/大野千鶴子.md "wikilink")，媒人為。

### 1970年代-1990年代

小渊惠三在自民黨内，歷任佐藤派→田中派→竹下派→小淵派的保守主流，成為自民黨内的實力派。小渊惠三與、[小澤一郎和橋本龍太郎等七人被稱為](../Page/小泽一郎.md "wikilink")「竹下派七奉行」，一直作為未來首相的熱門人選。拜師於[竹下登](../Page/竹下登.md "wikilink")，為竹下直系。

1970年1月20日，就任第三次佐藤內閣的郵政政務次官。就任時以「郵政省的政務次官必須理解基層職務」為理由，混進郵政外務職員中自行派信。當時的郵政省職員與[郵局局員大為震驚](../Page/郵局.md "wikilink")，因此得到支持。亦因他這樣突出的舉動成為熱門話題（[竹下登曾說](../Page/竹下登.md "wikilink")：「眾議院議員並非成為郵差人員，但似乎郵差人員就成為眾議院議員了。」）。

1972年自民黨總裁選舉中，[田中角榮與](../Page/田中角荣.md "wikilink")[福田赳夫大戰](../Page/福田赳夫.md "wikilink")，世稱為[角福戰爭](../Page/角福戰爭.md "wikilink")。小淵投票給佐藤派之長[佐藤榮作屬意的候選人田中](../Page/佐藤荣作.md "wikilink")，而不是投票給同鄉的[福田赳夫](../Page/福田赳夫.md "wikilink")，因此招來期望福田赳夫成為首相的群馬縣縣民的憤怒。在第33回總選舉中大苦戰，結果以全國最低得票當選。

1972年7月12日，小渊惠三就任[第1次田中角榮內閣的](../Page/第1次田中角榮內閣.md "wikilink")[建設政務次官](../Page/建設政務次官.md "wikilink")。1973年11月25日就任[第二次田中改造內閣的](../Page/第二次田中改造內閣.md "wikilink")[總理府總務副長官](../Page/總理府總務副長官.md "wikilink")。小淵合共就任三次，比起[自民黨黨內只就任兩次政務次官的慣例來說](../Page/自由民主黨_\(日本\).md "wikilink")，是非常例外的例子。

1979年11月9日，小渊惠三初入閣為[第二次大平內閣](../Page/第二次大平內閣.md "wikilink")兼。比同期的議員入閣時間也最遲。他為竹下登成為首相而奔走，結果在1987年11月6日成立的竹下內閣就任[內閣官房長官](../Page/內閣官房長官.md "wikilink")。小渊惠三亦曾臨時代理[内閣總理大臣](../Page/内閣總理大臣.md "wikilink")。

### 平成新機遇

他任[官房長官時](../Page/内阁官房长官.md "wikilink")，剛巧遇上[昭和天皇](../Page/昭和天皇.md "wikilink")[駕崩且變更](../Page/昭和天皇之死.md "wikilink")[年號](../Page/年號.md "wikilink")。他在1989年1月7日下午的記者會宣布新年號為「平成」。由於發表新元號、招來國民注意，小淵亦因「平成大叔」稱號成為超人氣政治家。小淵恭敬地展示寫上「平成」的紙板，多利用作平成時代的象徵。

[昭和天皇駕崩後](../Page/昭和天皇.md "wikilink")，官房長官需要處理大喪之禮等一連串問題。他發揮了領袖的性質，當時與官房事務副長官同鄉，故一同協力面對。

令人深刻的是，當初就任官房長官發接內閣閣僚名單時，一時忘記稱呼[環境廳](../Page/环境省.md "wikilink")[長官](../Page/環境大臣.md "wikilink")的名字，多次在發言作修正，被揶揄為「訂正長官」。

### 1990年代之後

1991年4月，當時任[自民黨幹事長的](../Page/自民黨幹事長.md "wikilink")[小澤一郎在](../Page/小泽一郎.md "wikilink")[東京都知事選舉時](../Page/東京都知事.md "wikilink")，強行推出[NHK評論員](../Page/日本廣播協會.md "wikilink")，引來自民黨[東京都支部反對](../Page/東京都.md "wikilink")。自民黨與[民主社會黨的東京都支部推出現任知事](../Page/民主社會黨.md "wikilink")[鈴木俊一](../Page/鈴木俊一.md "wikilink")，形成自民黨的分裂選舉，結果鈴木在五人角逐下成功第三次連任。小澤引咎辭職，故小淵接任自由民主黨幹事長。

1992年10月，[經世會](../Page/經世會.md "wikilink")（後來[平成研究会](../Page/平成研究會.md "wikilink")，屬[竹下派](../Page/平成研究會.md "wikilink")）會長[金丸信因](../Page/金丸信.md "wikilink")辭去議員，為繼承金丸的會長職位使[小澤一郎與反小澤派的對立被激化](../Page/小泽一郎.md "wikilink")。小澤派推出[羽田孜](../Page/羽田孜.md "wikilink")，反小澤派推出小淵參選後繼會長。小淵成為後繼派系領袖。而小澤與羽田則自立門戶，成立改革論壇21（羽田・小澤派）與經世會（小淵派）分裂。1993年羽田離開自民黨成立[新生黨](../Page/新生黨.md "wikilink")。

1994年自民黨重新執政後，他擔任自民黨副總裁，專注於黨務、與重要閣僚無緣。1995年，自民黨群馬縣支部聯合會會長選舉之際，對小淵選拔衆院選小選舉區的候選人人才不滿的[中曾根康弘](../Page/中曾根康弘.md "wikilink")，對小淵連任縣聯會長提出異議，而[福田康夫亦有異議](../Page/福田康夫.md "wikilink")。結果小淵不再連任，在群馬縣被認為「小淵的政治生命就此結束」。1996年1月，[村山富市辭職](../Page/村山富市.md "wikilink")。小淵派的[橋本龍太郎就任](../Page/橋本龍太郎.md "wikilink")[内閣總理大臣](../Page/内閣總理大臣.md "wikilink")。身為小淵派會長的小淵表示願意參與政權，在[野中廣務勸說下](../Page/野中广务.md "wikilink")，支援橋本政府。為與[河野洋平對抗的](../Page/河野洋平.md "wikilink")[加藤紘一就任黨幹事長進行工作](../Page/加藤紘一.md "wikilink")。同年10月[第二次橋本内閣成立](../Page/第二次橋本内閣.md "wikilink")，一道傳出要小淵就任衆議院議長。小淵曾表樂意接受，但因就任議長與未來就任首相無緣招至地方支持者的反對。手下側近的[額賀福志郎](../Page/額賀福志郎.md "wikilink")、[青木幹雄](../Page/青木幹雄.md "wikilink")、[綿貫民輔以及秘書](../Page/綿貫民輔.md "wikilink")反對故力推，最後由親近竹下的[伊藤宗一郎就任眾議院議長](../Page/伊藤宗一郎.md "wikilink")。1997年9月，就任[第二次橋本内閣改造内閣的](../Page/第二次橋本内閣改造内閣.md "wikilink")[外務大臣](../Page/外務大臣.md "wikilink")，重歸政治舞台。對外務省的事業，招來政敵[土井多賀子以及](../Page/土井多賀子.md "wikilink")[菅直人的高評價](../Page/菅直人.md "wikilink")。

1998年7月30日，橋本首相為敗北的責任請辭。小淵當選自民黨總裁及[總理大臣](../Page/日本內閣總理大臣.md "wikilink")。由於橋本與小淵同派系，初時招來批判。2000年4月2日，因中風入院。日前曾有預兆，在回答記者詢問一時無言，花十秒以上時間才能回答。自民黨其後指名[内閣官房長官](../Page/内阁官房长官.md "wikilink")[青木幹雄臨時代理首相職權](../Page/青木幹雄.md "wikilink")。對於在野黨質疑，在小淵指定青木為首相臨時代理之際，小淵到底是否已失去意識。青木在記者會上否認小淵腦死的傳言。但2000年5月14日下午4時7分，小淵在順天堂醫科大學醫學部附屬順天堂醫院逝世。令人感到天意弄人的是他父親亦因此病逝世。小淵死後，獲追頒[大勳位菊花大綬章](../Page/大勳位菊花大綬章.md "wikilink")。

### 過世之後

衆議院對曾任總理大臣的[議員朗讀弔辭](../Page/民意代表.md "wikilink")，依慣例是由最大[在野黨](../Page/在野黨.md "wikilink")[黨魁朗讀的](../Page/黨魁.md "wikilink")。本來由民主黨代表[鳩山由紀夫朗讀弔辭](../Page/鳩山友紀夫.md "wikilink")。但小淵家屬拒絕，故破例由同曾當上總理大臣的在野黨社會民主黨眾議院議員、前首相[村山富市在眾議院朗讀弔辭](../Page/村山富市.md "wikilink")。當時鳩山正強烈攻擊小淵長兄小淵光平與秘書的不當利益，被視為令小渊惠三加重壓力引致突然死亡的原因。野中亦因此曾指責鳩山，亦有指當時自由黨突然脫離執政聯盟，使自民黨舉步為艱才是原因。

2000年6月8日，[日本武道館舉行](../Page/日本武道館.md "wikilink")「内閣－自民黨聯合葬禮」，再次舉行弔問外交。兩個月後的衆議院總選舉，次女[小淵優子出戰群馬](../Page/小淵優子.md "wikilink")5區，與第二位的（前[日本社會黨書記長](../Page/日本社會黨.md "wikilink")・前總務廳長官）相差13萬票的差距當選，之後當選連任。

2006年5月、因七回忌而舉辦的「小淵前首相追思會」[森喜朗](../Page/森喜朗.md "wikilink")、[橋本龍太郎](../Page/橋本龍太郎.md "wikilink")、[青木幹雄與](../Page/青木幹雄.md "wikilink")等多位重要人物出席。小淵惠三死後六年，會場有近2000人參加，顯示小淵惠三人氣仍然很高。

## 参考文献

## 外部链接

  - [總理府網頁](http://www.kantei.go.jp/jp/obutisouri/index.html)

{{-}}

[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:群馬縣出身人物](../Category/群馬縣出身人物.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")
[Category:日本內閣總理大臣](../Category/日本內閣總理大臣.md "wikilink")
[Category:日本自由民主党总裁](../Category/日本自由民主党总裁.md "wikilink")
[Category:日本外务大臣](../Category/日本外务大臣.md "wikilink")
[Category:平成研究会会长](../Category/平成研究会会长.md "wikilink")
[Category:日本自由民主党干事长](../Category/日本自由民主党干事长.md "wikilink")
[Category:日本内阁官房长官](../Category/日本内阁官房长官.md "wikilink")
[Category:日本冲绳开发厅长官](../Category/日本冲绳开发厅长官.md "wikilink")
[Category:日本总理府总务长官](../Category/日本总理府总务长官.md "wikilink")
[Category:第二次大平內閣閣僚](../Category/第二次大平內閣閣僚.md "wikilink")
[Category:竹下內閣閣僚](../Category/竹下內閣閣僚.md "wikilink")
[Category:第二次橋本內閣閣僚](../Category/第二次橋本內閣閣僚.md "wikilink")
[Category:小淵內閣閣僚](../Category/小淵內閣閣僚.md "wikilink") [Category:日本眾議院議員
1963–1966](../Category/日本眾議院議員_1963–1966.md "wikilink")
[Category:日本眾議院議員
1967–1969](../Category/日本眾議院議員_1967–1969.md "wikilink")
[Category:日本眾議院議員
1969–1972](../Category/日本眾議院議員_1969–1972.md "wikilink")
[Category:日本眾議院議員
1972–1976](../Category/日本眾議院議員_1972–1976.md "wikilink")
[Category:日本眾議院議員
1976–1979](../Category/日本眾議院議員_1976–1979.md "wikilink")
[Category:日本眾議院議員
1979–1980](../Category/日本眾議院議員_1979–1980.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:获称“中国人民的老朋友”的日本首相](../Category/获称“中国人民的老朋友”的日本首相.md "wikilink")
[Category:群馬縣選出日本眾議院議員](../Category/群馬縣選出日本眾議院議員.md "wikilink")