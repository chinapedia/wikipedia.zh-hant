**雲紋蛇鱔**，又名**星帶蝮鯙**、**雲紋海鱔**，俗名[薯鰻](../Page/薯鰻.md "wikilink")、[錢鰻](../Page/錢鰻.md "wikilink")，为[鯙科](../Page/鯙科.md "wikilink")[蛇鱔屬下的一種海洋鱼类](../Page/蛇鱔屬.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[紅海](../Page/紅海.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[葛摩](../Page/葛摩.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[留尼旺](../Page/留尼旺.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[聖誕島](../Page/聖誕島.md "wikilink")、[印度](../Page/印度.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[日本](../Page/日本.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[庫克群島](../Page/庫克群島.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[關島](../Page/關島.md "wikilink")、[萬那杜](../Page/萬那杜.md "wikilink")、[薩摩亞群島](../Page/薩摩亞群島.md "wikilink")、[法屬波里尼西亞](../Page/法屬波里尼西亞.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[哥斯大黎加等海域](../Page/哥斯大黎加.md "wikilink")。

## 深度

水深0至20公尺。

## 特徵

本魚生体细长，體色有變異，通常為白色或黃色；體側有兩列，約25至30粒棕黑色斑點，每一斑點中心有2至3個黃色花形小點。齒成錐形，隨年紀越大，有越鈍之傾向，表皮厚且光滑無鱗片，能分泌黏液，無胸鰭，背鰭、臀鰭與尾鰭相連呈皮膜狀，尾部側扁。體長可達70公分。

## 生態

本魚常見於潮池，晝間棲息珊瑚礁或岩礁之孔隙、洞穴中，夜間出來捕食[魚類](../Page/魚類.md "wikilink")、[蝦](../Page/蝦.md "wikilink")、[蟹為食](../Page/蟹.md "wikilink")，嗅覺靈敏但視覺不佳。性情兇猛，有地域性。

## 經濟利用

本魚可養在水族箱作觀賞魚，另亦可食用。

## 参考文献

  -
## 扩展阅读

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  -
  - {{ cite book | title = 觀賞魚圖鑑 | publisher = 貓頭鷹出版社 | date = 1996年6月
    }}

[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[nebulosa](../Category/蛇鱔屬.md "wikilink")