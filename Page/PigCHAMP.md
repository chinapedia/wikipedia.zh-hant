**PigCHAMP**是[北美著名的](../Page/北美.md "wikilink")[猪场管理软件](../Page/猪场管理软件.md "wikilink")。PigCHAMP最初于20世纪80年代早期由[明尼苏达州立大学兽医学院开发](../Page/明尼苏达州立大学.md "wikilink")，最初的开发目的是用于采集科研数据\[1\]。

PigCHAMP的“CHAMP”是“计算机保健及管理程序”（Computerized Health and Management
Program）的[缩写](../Page/缩写.md "wikilink")\[2\]。

PigCHAMP最初由明尼苏达州立大学兽医学院进行推广。1989年，PigCHAMP
1.1发布\[3\]。1999年，明尼苏达州立大学将PigCHAMP的所有权移交给PigCHAMP的雇员及外部投资者。2001年11月，Farms.com有限公司[收购了PigCHAMP](../Page/收购.md "wikilink")\[4\]。

曾有几家公司在[中国推广PigCHAMP](../Page/中国.md "wikilink")，但由于收费很高没有推广成功\[5\]。PigCHAMP
Care 3000按母猪头数定价，每头母猪的平均许可价格随母猪头数增加而减少。最低价格为200头母猪的群体每年500美元\[6\]。

## 外部链接

  - [PigCHAMP官方首页](http://www.pigchamp.com/)

  - [Farms.com](http://www.farms.com/)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:猪场管理软件](../Category/猪场管理软件.md "wikilink")

1.

2.
3.

4.
5.

6.