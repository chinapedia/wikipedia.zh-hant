[RareEarthOreUSGOV.jpg](https://zh.wikipedia.org/wiki/File:RareEarthOreUSGOV.jpg "fig:RareEarthOreUSGOV.jpg")
**稀土金属**，或称**稀土元素**，是[元素週期表第](../Page/元素週期表.md "wikilink")Ⅲ族副族元素[钪](../Page/钪.md "wikilink")、[钇和](../Page/钇.md "wikilink")[镧系元素共](../Page/镧系元素.md "wikilink")17种[化学元素的合称](../Page/化学元素.md "wikilink")。[钪和](../Page/钪.md "wikilink")[钇因为经常与](../Page/钇.md "wikilink")[镧系元素在矿床中共生](../Page/镧系元素.md "wikilink")，且具有相似的化学性质，故被认为是稀土元素。

与其名称暗示的不同，稀土元素（[钷除外](../Page/钷.md "wikilink")）在地壳中的豐度相当高，其中[铈在地壳元素豐度排名第](../Page/铈.md "wikilink")25，占0.0068%（与[铜接近](../Page/铜.md "wikilink")）。稀土元素並不稀有，但其傾向於兩兩一起生成合金，且難以將稀土元素單獨分離。另外，稀土元素在地殼中的分佈相當分散，很少有稀土元素集中到容許商業开采的礦床。人类第一种发现的稀土矿物是从[瑞典](../Page/瑞典.md "wikilink")[伊特比村的矿山中提取出的](../Page/伊特比村.md "wikilink")，许多稀土元素的名称正源自于此地。

## 性质

[Rareearthoxides.jpg](https://zh.wikipedia.org/wiki/File:Rareearthoxides.jpg "fig:Rareearthoxides.jpg")
多数呈银灰色，有光泽，[晶体结构多为](../Page/晶体结构.md "wikilink")[六方最密堆積](../Page/六方最密堆積.md "wikilink")（HCP）或[面心立方](../Page/面心立方.md "wikilink")（FCC）。性质较软，在潮湿空气中不易保存，易溶于稀酸。

原子价主要是+3价（[铈正四价较稳定](../Page/铈.md "wikilink")，[镨和](../Page/镨.md "wikilink")[铽也有极个别的四价](../Page/铽.md "wikilink")[氧化物](../Page/氧化物.md "wikilink")，[钐](../Page/钐.md "wikilink")、[铕](../Page/铕.md "wikilink")、[镱有二价化合物](../Page/镱.md "wikilink")），能形成稳定的[配合物及微溶于水的](../Page/配合物.md "wikilink")[草酸盐](../Page/草酸盐.md "wikilink")、[氟化物](../Page/氟化物.md "wikilink")、[碳酸盐](../Page/碳酸盐.md "wikilink")、[磷酸盐及](../Page/磷酸盐.md "wikilink")[氢氧化物等](../Page/氢氧化物.md "wikilink")。

在三价稀土氧化物中，[氧化镧的吸水性和碱性与](../Page/氧化镧.md "wikilink")[氧化钙相似](../Page/氧化钙.md "wikilink")，其余则依次转弱。三价稀土的化学性质除[钪的差异较显著外](../Page/钪.md "wikilink")，其余都很相似，所以分离较难。

## 稀土金属所含元素

具体的稀土金属包括：[镧](../Page/镧.md "wikilink")（La）、[铈](../Page/铈.md "wikilink")（Ce）、[镨](../Page/镨.md "wikilink")（Pr）、[钕](../Page/钕.md "wikilink")（Nd）、[钷](../Page/钷.md "wikilink")（Pm）、[钐](../Page/钐.md "wikilink")（Sm）、[铕](../Page/铕.md "wikilink")（Eu）、[钆](../Page/钆.md "wikilink")（Gd）、[铽](../Page/铽.md "wikilink")（Tb）、[镝](../Page/镝.md "wikilink")（Dy）、[钬](../Page/钬.md "wikilink")（Ho）、[铒](../Page/铒.md "wikilink")（Er）、[铥](../Page/铥.md "wikilink")（Tm）、[镱](../Page/镱.md "wikilink")（Yb）、[镥](../Page/镥.md "wikilink")（Lu）以及与[镧系的](../Page/镧系.md "wikilink")15个元素密切相关的两个元素：[钪](../Page/钪.md "wikilink")（Sc）和[钇](../Page/钇.md "wikilink")（Y），称为稀土元素（Rare
Earth），简称稀土（RE）。

### 分类

根据稀土元素[原子](../Page/原子.md "wikilink")[电子层结构和物理化学性质](../Page/电子层.md "wikilink")，以及它们在矿物中共生情况和不同的[离子半径可产生不同性质的特征](../Page/离子.md "wikilink")，十七种稀土元素通常分为二组：

**轻稀土**包括：[镧](../Page/镧.md "wikilink")、[铈](../Page/铈.md "wikilink")、[镨](../Page/镨.md "wikilink")、[钕](../Page/钕.md "wikilink")、[钷](../Page/钷.md "wikilink")、[钐](../Page/钐.md "wikilink")、[铕](../Page/铕.md "wikilink")、[钆](../Page/钆.md "wikilink")。

**重稀土**包括：[铽](../Page/铽.md "wikilink")、[镝](../Page/镝.md "wikilink")、[钬](../Page/钬.md "wikilink")、[铒](../Page/铒.md "wikilink")、[铥](../Page/铥.md "wikilink")、[镱](../Page/镱.md "wikilink")、[镥](../Page/镥.md "wikilink")、[钪](../Page/钪.md "wikilink")、[钇](../Page/钇.md "wikilink")。

也可分为**铈组**（镧、铈、镨、钕、钷、钐）和**钇组**（铕、钆、铽、镝、钬、铒、铥、镱、钇）。

### 稀土分類列表

| [Z](../Page/原子序数.md "wikilink") | 化學符號 | 名稱                           | 英文名          | 詞源                                                                                                | 應用範圍                                                                                                                                                                                      |
| ------------------------------- | ---- | ---------------------------- | ------------ | ------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 21                              | Sc   | [钪](../Page/钪.md "wikilink") | Scandium     | 源自第一個稀土矿石被發現的半岛[斯堪的纳维亚之](../Page/斯堪的纳维亚.md "wikilink")[拉丁语名](../Page/拉丁语.md "wikilink")"Scandia"。 | [铝](../Page/铝.md "wikilink")[钪合金](../Page/钪.md "wikilink")（用于制造航天器械）、[水银灯配件](../Page/水银灯.md "wikilink")                                                                                   |
| 39                              | Y    | [钇](../Page/钇.md "wikilink") | Yttrium      | 源自第一个稀土矿石被发现的地方，[瑞典](../Page/瑞典.md "wikilink")[伊特比村之名](../Page/伊特比.md "wikilink")（Ytterby）。       | 钇鋁[石榴石](../Page/石榴石.md "wikilink")（YAG）、[YBCO](../Page/钇钡铜氧.md "wikilink")[高溫超導体](../Page/高溫超導体.md "wikilink")、钇铁石榴石（YIG）                                                                 |
| 57                              | La   | [镧](../Page/镧.md "wikilink") | Lanthanum    | 源自[希臘語](../Page/希臘語.md "wikilink")"lanthanon"，意為*隐藏*。                                             | 高[折射率玻璃](../Page/折射率.md "wikilink")、[燧石](../Page/燧石.md "wikilink")、氫氣儲藏裝置、[電池電極](../Page/電池.md "wikilink")、[相機鏡片](../Page/相機.md "wikilink")、石油提煉液體催化過程（FCC）催化劑                            |
| 58                              | Ce   | [铈](../Page/铈.md "wikilink") | Cerium       | 源自矮行星[穀神星](../Page/穀神星.md "wikilink")（Ceres）之名。                                                   | [氧化剂](../Page/氧化剂.md "wikilink")、拋光粉、玻璃和瓷器的黃色染料、石油提煉液體催化過程（FCC）催化劑                                                                                                                        |
| 59                              | Pr   | [镨](../Page/镨.md "wikilink") | Praseodymium | 源自希臘語"prasios"，意為*韭菜綠*，以及"didymos"，意為*双胞胎*。                                                       | [稀土磁鐵](../Page/稀土磁鐵.md "wikilink")、[激光](../Page/激光.md "wikilink")、玻璃和[珐琅制品染料](../Page/珐琅.md "wikilink")、[燧石](../Page/燧石.md "wikilink")                                                    |
| 60                              | Nd   | [钕](../Page/钕.md "wikilink") | Neodymium    | 源自希臘語"neo"，意為*新的*，以及"didymos"，意為*双胞胎*。                                                            | [稀土磁鐵](../Page/稀土磁鐵.md "wikilink")、[激光](../Page/激光.md "wikilink")、玻璃和瓷器的紫色染料、陶瓷[電容器](../Page/電容器.md "wikilink")                                                                           |
| 61                              | Pm   | [钷](../Page/钷.md "wikilink") | Promethium   | 源自[希腊神话中盗火者](../Page/希腊神话.md "wikilink")[普罗米修斯](../Page/普罗米修斯.md "wikilink")（Prometheus）之名。       | [核電池](../Page/核電池.md "wikilink")                                                                                                                                                          |
| 62                              | Sm   | [钐](../Page/钐.md "wikilink") | Samarium     | 源自[俄罗斯矿业工程师](../Page/俄罗斯.md "wikilink")之名。稀土矿石就是以他的名字命名的。                                         | [稀土磁鐵](../Page/稀土磁鐵.md "wikilink")、[激光](../Page/激光.md "wikilink")、中子捕獲裝置、[激微波](../Page/激微波.md "wikilink")                                                                                 |
| 63                              | Eu   | [铕](../Page/铕.md "wikilink") | Europium     | 源自[歐洲](../Page/歐洲.md "wikilink")（Europe）一词。                                                       | 紅色和藍色的[荧光粉](../Page/荧光粉.md "wikilink")、[激光](../Page/激光.md "wikilink")、[水銀燈部件](../Page/水銀燈.md "wikilink")                                                                                  |
| 64                              | Gd   | [钆](../Page/钆.md "wikilink") | Gadolinium   | 源自之名，以紀念他對稀土的研究。                                                                                  | [稀土磁鐵](../Page/稀土磁鐵.md "wikilink")、高折射指數玻璃、[石榴石](../Page/石榴石.md "wikilink")、[激光](../Page/激光.md "wikilink")、[X射線管](../Page/X射線管.md "wikilink")、[電腦記憶體](../Page/電腦記憶體.md "wikilink")、中子捕獲裝置 |
| 65                              | Tb   | [铽](../Page/铽.md "wikilink") | Terbium      | 源自瑞典[伊特比村之名](../Page/伊特比.md "wikilink")。                                                          | 綠色荧光粉、[激光](../Page/激光.md "wikilink")、[荧光灯](../Page/荧光灯.md "wikilink")                                                                                                                     |
| 66                              | Dy   | [镝](../Page/镝.md "wikilink") | Dysprosium   | 源自希臘語"dysprositos"，意為*难以获得*.                                                                      | [稀土磁鐵](../Page/稀土磁鐵.md "wikilink")、[激光](../Page/激光.md "wikilink")                                                                                                                         |
| 67                              | Ho   | [钬](../Page/钬.md "wikilink") | Holmium      | 源自其发现者的故乡[斯德哥尔摩之拉丁语名](../Page/斯德哥尔摩.md "wikilink")"Holmia"。                                       | [激光](../Page/激光.md "wikilink")                                                                                                                                                            |
| 68                              | Er   | [铒](../Page/铒.md "wikilink") | Erbium       | 源自瑞典[伊特比村之名](../Page/伊特比.md "wikilink")。                                                          | [激光](../Page/激光.md "wikilink")、[钒鋼](../Page/钒.md "wikilink")                                                                                                                              |
| 69                              | Tm   | [铥](../Page/铥.md "wikilink") | Thulium      | 源自[希腊神话中的北方神秘之地](../Page/希腊神话.md "wikilink")[图勒](../Page/图勒.md "wikilink")（）。                     | 便携式[X射线机](../Page/X射线机.md "wikilink")                                                                                                                                                     |
| 70                              | Yb   | [镱](../Page/镱.md "wikilink") | Ytterbium    | 源自瑞典[伊特比村之名](../Page/伊特比.md "wikilink")。                                                          | 紅外線[激光](../Page/激光.md "wikilink")、[还原剂](../Page/还原剂.md "wikilink")                                                                                                                        |
| 71                              | Lu   | [镥](../Page/镥.md "wikilink") | Lutetium     | 源自法國村鎮Lutetia之名（现为[巴黎](../Page/巴黎.md "wikilink")）。                                                | 高折射率玻璃                                                                                                                                                                                    |

## 相關英文缩写

  - RE = 稀土（Rare Earth）
  - REM = 稀土金屬（Rare Earth Metals）
  - REE = 稀土元素（Rare Earth Elements）
  - REO = 稀土氧化物（Rare Earth Oxides）
  - LREE = 輕稀土元素（Light Rare Earth Elements，La-Sm)
  - HREE = 重稀土元素（Heavy Rare Earth Elements，Eu-Lu)

## 分布

[元素豐度.svg](https://zh.wikipedia.org/wiki/File:元素豐度.svg "fig:元素豐度.svg")
[Rareearth_production.svg](https://zh.wikipedia.org/wiki/File:Rareearth_production.svg "fig:Rareearth_production.svg")
[Lutetium_sublimed_dendritic_and_1cm3_cube.jpg](https://zh.wikipedia.org/wiki/File:Lutetium_sublimed_dendritic_and_1cm3_cube.jpg "fig:Lutetium_sublimed_dendritic_and_1cm3_cube.jpg")的加工形態\]\]

各种稀土元素常以差别很大的含量存在于同一矿石中，如[独居石及氟碳铈矿中以轻稀土为主](../Page/独居石.md "wikilink")，钪钇矿石以钪和钇为主，黑稀金矿以钇及重稀土为主。

全世界已知有约9261万吨稀土矿，其中有约占36%储藏在[中国](../Page/中华人民共和国.md "wikilink")。[内蒙古](../Page/内蒙古.md "wikilink")[包头市的](../Page/包头市.md "wikilink")[白云鄂博](../Page/白云鄂博.md "wikilink")、[江西](../Page/江西.md "wikilink")、[广东](../Page/广东.md "wikilink")、[四川等地混合矿储量巨大](../Page/四川.md "wikilink")。中国是目前世界第一大稀土矿生产国，也是稀土矿第一大出口国，约占世界出口额总量的90%。2009年中国开始管控和限制稀土矿石出口，此举有利于保护资源环境和遏制过度开发，但同时也引起美日欧等国家的不满。2012年3月美国表示将就中国限制稀土矿出口问题向[世贸组织](../Page/世贸组织.md "wikilink")(WTO)提起诉讼，欧盟和日本将会支持并参与其中\[1\]。中国报道称美国和其它国家虽有大量稀土矿资源，但因其国内工资远高于中国，开采经济效益不高，故长期向中国廉价进口。各国普遍认为中国限制稀土出口与中日领土争端有关，而中国则指责美国和其它国家为战略储备和保护珍贵资源的目的长期向中国廉价进口，中国以此为理由限制稀土出口规模\[2\]。

其它拥有稀土资源的国家和地区有[独联体国家](../Page/独联体.md "wikilink")（19%）、[美国](../Page/美国.md "wikilink")（13%）、[澳大利亚](../Page/澳大利亚.md "wikilink")（5%）、[印度](../Page/印度.md "wikilink")（3.14%）、[加拿大](../Page/加拿大.md "wikilink")、[南非](../Page/南非.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[马来西亚和](../Page/马来西亚.md "wikilink")[越南等](../Page/越南.md "wikilink")，而[欧盟和](../Page/欧盟.md "wikilink")[日本基本没有稀土资源](../Page/日本.md "wikilink")。它们的稀土来源主要从[中国进口](../Page/中国.md "wikilink")\[3\]。

日本科學家於2011年7月4日聲稱，在[太平洋海底發現巨大的稀土蘊藏](../Page/太平洋.md "wikilink")，可開採量約是全球陸地上已證實藏量的1000倍。\[4\]又2012年6月28日[東京大學研究團隊在日本最東端的](../Page/東京大學.md "wikilink")[南鳥島周邊海域](../Page/南鳥島.md "wikilink")，發現含大量稀土的泥礦床，其蘊藏量相當於日本約230年的消費量。\[5\]但目前并无任何国家到太平洋海底开采出稀土矿资源，而从经济角度来讲，相较于目前稀土矿价格，自海底开采并不符合经济效益。

### 稀土金屬礦之開採

由週期表特性可知稀土元素不易形成大規模礦區，必以大量挖掘後再予以過濾。副作用是損失大量土方及飛塵污染，容易引發居民抗議，因此多被苛徵巨額環保稅。

扣除上列因素，原則上能挖掘的古大陸地形都能開採稀土，早期伴隨煤鐵石油鑽石礦區一起挖掘，例如：近[北極圈各國與](../Page/北極圈.md "wikilink")[中歐](../Page/中歐.md "wikilink")；機械工具發達後便不受限制，多半由運輸成本及出口稅率而定，例如[前蘇聯](../Page/前蘇聯.md "wikilink")、中國與澳洲等。

迄2010年底為止主要稀土出口地為中國（約佔97%），以鉛、鋅、高嶺土等初級品為主；可精鍊治金品目前由大型礦業或鋼鐵公司掌握專利，例如：[必和必拓](../Page/必和必拓.md "wikilink")（BHP
Billiton）、[力拓](../Page/力拓集團.md "wikilink")（Rio
Tinto）、[淡水河谷](../Page/淡水河谷公司.md "wikilink")（Vale）等等。此外為增加工作人口，澳洲、中國、巴西等地都有意提高原料出口價格，以吸引礦商與金屬業者到地主國投資先進[冶金技術](../Page/冶金.md "wikilink")。

## 应用

稀土已被广泛应用于[國防工業](../Page/國防工業.md "wikilink")、[冶金](../Page/冶金.md "wikilink")、[机械](../Page/机械.md "wikilink")、[石油](../Page/石油.md "wikilink")、[化工](../Page/化工.md "wikilink")、[玻璃](../Page/玻璃.md "wikilink")、[陶瓷](../Page/陶瓷.md "wikilink")、[纺织](../Page/纺织.md "wikilink")、[皮革](../Page/皮革.md "wikilink")、[农牧养殖等各传统方面领域](../Page/农牧养殖.md "wikilink")，在社会生活中几乎随处可见。

作为改性添加元素在[钢铁和](../Page/钢铁.md "wikilink")[有色金属中加入极少量稀土就能明显改善金属材料性能](../Page/有色金属.md "wikilink")，提高钢材的强度及[耐磨性和](../Page/耐磨性.md "wikilink")[抗腐蚀性能力](../Page/抗腐蚀性.md "wikilink")。\[6\]

## “稀土金属”一词存在的争议

“稀土”中的“土”字实际上指的是[氧化物](../Page/氧化物.md "wikilink")。这些元素被发现时人们以为它们在地球上分布非常稀少。实际上它们在[地壳内的含量并不算太低](../Page/地壳.md "wikilink")，最高的[铈是地壳中第](../Page/铈.md "wikilink")25丰富的元素，比[铅还要高](../Page/铅.md "wikilink")，而“稀土金属”中稳定元素里丰度最低的[镥在地壳中的含量比](../Page/镥.md "wikilink")[金甚至还要高出](../Page/金.md "wikilink")200倍。

## 參見

  - [克里普礦物](../Page/克里普礦物.md "wikilink")：一種只在月球發現含有多種稀土金屬的礦物。
  - [伊特比](../Page/伊特比.md "wikilink")：發現17種稀土金属的瑞典小村落。
  - [廢電腦回收技術](../Page/廢電腦回收技術.md "wikilink")

## 参考文献

## 外部链接

  - [中国稀土学会](http://www.cs-re.org.cn/)

  - [中国稀土网](http://www.cre.net/)

  - [西藏矿业咨询网《稀土的性质及应用》](http://www.xzkyzxw.com/read.asp?ID=1530)

  -
  -
  -
[Category:金属元素](../Category/金属元素.md "wikilink")
[Category:稀土金属](../Category/稀土金属.md "wikilink")

1.  [美国将就中国稀土出口问题向世贸组织提出诉讼](http://finance.china.com/html/c/20123/15/1333958_1_418.html)

2.  [人民网评：应对稀土贸易战，中国理直气壮](http://opinion.people.com.cn/GB/223228/17401411.html)
3.  [网易特别策划_ 遭遇美日欧围剿 中国打响“稀土之战”](http://money.163.com/special/view140/)
4.  [日發現太平洋稀土礦
    藏量高陸地千倍](http://hk.news.yahoo.com/%E6%97%A5%E7%99%BC%E7%8F%BE%E5%A4%AA%E5%B9%B3%E6%B4%8B%E7%A8%80%E5%9C%9F%E7%A4%A6-%E8%97%8F%E9%87%8F%E9%AB%98%E9%99%B8%E5%9C%B0%E5%8D%83%E5%80%8D-220825236.html)
5.  [日本宣佈發現大規模海底稀土礦床](http://www.bbc.co.uk/zhongwen/trad/world/2012/06/120628_japan_rare_earth.shtml)
6.  2009-2012年中国稀土市场投资分析及前景预测报告（09年5月）