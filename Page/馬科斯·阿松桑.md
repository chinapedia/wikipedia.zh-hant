**馬高斯·阿辛卡奧**（**Marcos
Assunção**，），[巴西足球運動員](../Page/巴西.md "wikilink")，司職[防守中場](../Page/防守中場.md "wikilink")。

這名球員早年曾效力巴西國內一些大球會，包括[山度士和](../Page/山度士足球會.md "wikilink")[法林明高](../Page/法林明高.md "wikilink")。他於2002年加盟西甲球會貝迪斯。他最強的是罰球,曾經試過在同一場比場中射3次罰球中橫楣。

[Category:巴西足球運動員](../Category/巴西足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:山度士球員](../Category/山度士球員.md "wikilink")
[Category:法林明高球員](../Category/法林明高球員.md "wikilink")
[Category:羅馬球員](../Category/羅馬球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:皇家貝迪斯球員](../Category/皇家貝迪斯球員.md "wikilink")
[Category:巴西旅外足球運動員](../Category/巴西旅外足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:阿聯酋外籍足球運動員](../Category/阿聯酋外籍足球運動員.md "wikilink")
[Category:1998年美洲金盃球員](../Category/1998年美洲金盃球員.md "wikilink")