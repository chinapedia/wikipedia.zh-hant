[AlicePaul_1901.jpg](https://zh.wikipedia.org/wiki/File:AlicePaul_1901.jpg "fig:AlicePaul_1901.jpg")
**艾麗斯·保爾**\[1\]（，），[美國](../Page/美國.md "wikilink")[女权运动领袖](../Page/女性主義.md "wikilink")。她和她的朋友[露西·伯恩和其他朋友](../Page/露西·伯恩.md "wikilink")，成立了[全國婦女黨](../Page/全國婦女黨.md "wikilink")(National
Women's
Party)，她們每天有三名女性在[白宮前高舉標語](../Page/白宮.md "wikilink")，經過7年的努力，於1920年促使美國聯邦政府通過[美國憲法第十九修正案關於女性投票權的法案](../Page/美國憲法第十九修正案.md "wikilink")。

## 家庭背景

艾麗斯‧保爾，1885年1月11日出生在美國新澤西州莫爾斯城的一個[貴格會家庭中](../Page/貴格會.md "wikilink")，並有一位弟弟William
Jr.與一位妹妹Parry，其父親William Mickle
Paul是一名銀行家與商人，於當地柏靈頓信託投資公司擔任總裁職務。而身為Hicksite[貴格會的保爾一家](../Page/貴格會.md "wikilink")，堅信兩性平等、婦女教育與改善社會的重要性，母親Tacie
Parry Paul時常帶著年幼的艾麗斯參與當時的婦女爭取投票權會議，也為將艾麗斯推上女權奮鬥革命舞台。

## 教育背景

  - 艾麗斯‧保爾首先前往[貴格會的學校Friends](../Page/貴格會.md "wikilink")
    School就讀，並在班上以優異成績畢業。
  - 1905年，16歲的艾麗斯‧保爾自[斯沃斯莫尔学院畢業](../Page/斯沃斯莫尔学院.md "wikilink")，並成功取得生物科學學士學位。
  - 1907年，於美國[賓夕法尼亞大學順利同時獲得社會文學碩士學位](../Page/賓夕法尼亞大學.md "wikilink")，並且通過赴英國[伯明翰大学留學獎學金](../Page/伯明翰大学.md "wikilink")，此間，也同時在[倫敦政治經濟學院攻讀社會工作](../Page/倫敦政治經濟學院.md "wikilink")。
  - 回到美國後，艾麗斯更於1912年取得美國[賓夕法尼亞大學社會學博士學位](../Page/賓夕法尼亞大學.md "wikilink")。
  - 1922年，並從美國[華盛頓大學法學院取得法學學士學位](../Page/華盛頓大學.md "wikilink")。
  - 1927年與1928年分別在美國[美利坚大学取得法學碩士與民法博士學位](../Page/美利坚大学.md "wikilink")。

## 參與爭取女權契機

1908年前往英國[伯明翰大学唸書的艾麗斯](../Page/伯明翰大学.md "wikilink")‧保爾，聽到一場由[Christabel
Pankhurst](http://en.wikipedia.org/wiki/Christabel_Pankhurst)主講的演講，而得到啟發。隨後的艾麗斯也投身於許多爭取女權的活動與抗爭，並加入[婦女社會政治同盟Women](../Page/婦女社會政治同盟.md "wikilink")'s
Social and Political Union (WSPU)
。而在一次女權抗爭集會遊行活動中，遭到英國警局逮捕的艾麗斯，遇到了往後一同為女權抗爭奮鬥的朋友[露西·伯恩](../Page/露西·伯恩.md "wikilink")。

## 女權爭取歷史

  - 1913年，艾麗斯‧保爾與[露西·伯恩加入](../Page/露西·伯恩.md "wikilink")[Congressional
    Union for Women
    Suffrage](../Page/Congressional_Union_for_Women_Suffrage.md "wikilink")（CUWS），其間試圖採取在[婦女社會政治同盟](../Page/婦女社會政治同盟.md "wikilink")（Women's
    Social and Political Union,
    WSPU）的抗爭方法，包含組織大型示威活動與每日於白宮的抗爭，並於此年成立[全國婦女黨](../Page/全國婦女黨.md "wikilink")。
  - 1917年，[美國宣布參加](../Page/美國.md "wikilink")[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")，其間，艾麗斯‧保爾與朋友們仍持續為爭取女權進行抗爭，這樣的舉動在國家於存亡遊蕩之際，引起諸多撻伐聲浪，同年的10月，國家以「阻礙交通」為罪名逮捕艾麗斯等數名朋友，監禁於美國[維吉尼亞州Occoquan](../Page/維吉尼亞州.md "wikilink")
    Workhouse與哥倫比亞監獄，獄中的她們，也飽受許多不人道的對待。以絕食進行抗議的艾麗斯‧保爾，在七個月後與朋友一同受到釋放。
  - 直至1920年，美國聯邦政府才通過[美國憲法第十九修正案關於女性投票權的法案](../Page/美國憲法第十九修正案.md "wikilink")。
  - 1938年，艾麗斯‧保爾成立World Party for Equal Rights for Women（World Women's
    Party）。
  - 1964年，艾麗斯‧保爾成功引用[聯合國憲章序言進行遊說](../Page/聯合國憲章.md "wikilink")，成功促成民權法案性別平等。

## 相關參考資料

1.  <https://web.archive.org/web/20130403215146/http://www.spartacus.schoolnet.co.uk/USApaul.htm>
2.  <https://web.archive.org/web/20060619192543/http://www.lkwdpl.org/wihohio/paul-ali.htm>

## 註釋

[Category:女性主義者](../Category/女性主義者.md "wikilink")
[Category:美國社會運動者](../Category/美國社會運動者.md "wikilink")
[Category:新澤西州人](../Category/新澤西州人.md "wikilink")
[Category:斯沃斯莫爾學院校友](../Category/斯沃斯莫爾學院校友.md "wikilink")
[Category:賓夕法尼亞大學校友](../Category/賓夕法尼亞大學校友.md "wikilink")
[Category:美利堅大學校友](../Category/美利堅大學校友.md "wikilink")
[Category:伯明翰大學校友](../Category/伯明翰大學校友.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")
[Category:美国进步时代](../Category/美国进步时代.md "wikilink")

1.  根據《世界人名翻譯大辭典》p.2127「Paul, Alice」條，「Paul」一般譯「保羅」，唯此人物譯「保爾」。