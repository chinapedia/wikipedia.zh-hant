**明治製菓股份有限公司**（，1949年5月16日〜2009年3月25日）是一家生產[糖果點心與](../Page/糖果點心.md "wikilink")[葯物的](../Page/葯物.md "wikilink")[日本公司](../Page/日本.md "wikilink")，產品包括[Hello
Panda與](../Page/Hello_Panda.md "wikilink")[欣欣杯等](../Page/欣欣杯.md "wikilink")，對手包括[江崎固力果](../Page/江崎固力果.md "wikilink")、[河馬屋](../Page/河馬屋.md "wikilink")、[不二家](../Page/不二家.md "wikilink")、[樂天與](../Page/樂天.md "wikilink")[森永製菓等公司](../Page/森永製菓.md "wikilink")。

## 大事紀年

  - 1916年（大正5年）10月9日 東京菓子成立，為明治製菓前身
  - 1916年（大正5年）12月 [明治製糖的製菓部門獨立為](../Page/明治製糖.md "wikilink")「大正製菓」
  - 1917年（大正6年）3月 東京菓子與大正製菓合併，是明治製糖的子公司
  - 1924年（大正13年）改名「明治製菓」
  - 1925年（大正14年）[川崎工廠完工](../Page/川崎市.md "wikilink")（1989年關廠）
  - 1946年（昭和21年）正式生產[青霉素](../Page/青霉素.md "wikilink")，藥品事業開始
  - 1955年（昭和30年）[大阪工廠成立](../Page/大阪.md "wikilink")
  - 1969年（昭和44年）東海工廠成立
  - 1971年（昭和46年）[岐阜工廠成立](../Page/岐阜.md "wikilink")
  - 1974年（昭和49年）在[新加坡與](../Page/新加坡.md "wikilink")[印尼成立合資公司](../Page/印尼.md "wikilink")
  - 1979年（昭和54年）在[泰國成立合資公司](../Page/泰國.md "wikilink")
  - 1985年（昭和60年）在[美國成立合資公司](../Page/美國.md "wikilink")
  - 1986年（昭和61年）在[台灣成立合資公司](../Page/台灣.md "wikilink")
  - 1989年（平成1年）在[法國成立合資公司](../Page/法國.md "wikilink")
  - 1990年（平成2年）在中國成立汕頭明治
  - 1993年（平成5年）成立[廣州明治](../Page/廣州.md "wikilink")
  - 2004年（平成16年）成立[上海明治](../Page/上海.md "wikilink")
  - 2011年（平成23年）4月1日 改名「Meiji Seika
    Pharma」，保留醫療用藥品部門，菓子、食品、一般用藥品部門則移交「株式会社明治」（原[明治乳業](../Page/明治乳業.md "wikilink")）。

## 生產地點

### 工廠

  - 東海工場：位於[靜岡縣](../Page/靜岡縣.md "wikilink")[藤枝市](../Page/藤枝市.md "wikilink")
  - 大阪工場：位於[大阪府](../Page/大阪府.md "wikilink")[高槻市](../Page/高槻市.md "wikilink")
  - 岐阜工場：位於[岐阜縣](../Page/岐阜縣.md "wikilink")[本巢郡北方町](../Page/本巢郡.md "wikilink")，專門生產藥品

### 研究所

  - 橫濱研究所：位於神奈川縣[橫濱市港北區](../Page/橫濱市.md "wikilink")。

### 已關閉

  - 川崎工場：位於[神奈川縣](../Page/神奈川縣.md "wikilink")[川崎市](../Page/川崎市.md "wikilink")，於1989年關閉。

[Meiji_seika.jpg](https://zh.wikipedia.org/wiki/File:Meiji_seika.jpg "fig:Meiji_seika.jpg")
[Meiji_Seika_Kaisha_logo.svg](https://zh.wikipedia.org/wiki/File:Meiji_Seika_Kaisha_logo.svg "fig:Meiji_Seika_Kaisha_logo.svg")
{{-}}

## 外部連結

  - [明治製菓官方網站](http://www.meiji.co.jp/)

[Category:日本食品公司](../Category/日本食品公司.md "wikilink")
[Category:日本製藥公司](../Category/日本製藥公司.md "wikilink")
[Category:日本零食](../Category/日本零食.md "wikilink")
[Category:1916年日本建立](../Category/1916年日本建立.md "wikilink")
[Category:1916年成立的公司](../Category/1916年成立的公司.md "wikilink")