**愛荷華州**（）是位於[美國中西部大平原的一個](../Page/美國中西部.md "wikilink")[州份](../Page/美国州份.md "wikilink")；20世纪中后期，愛荷華州由农业经济為主过渡到多元化经济，如高级制造业，金融保险，生物技术和绿色能源产品等；愛荷華州被列为美国治安最好的州之一，也是基础教育最好的州之一。[首府為](../Page/首府.md "wikilink")[德梅因](../Page/德梅因.md "wikilink")。

## 歷史

艾奥瓦名稱源自印第安部落（Ioway）。

文件記載歐洲人首次到達艾奥瓦州是在1673年 Jacques Marquette和Louis Jolliet 旅行到密西西比河。

美國南北戰後，艾奥瓦的人口和農業快速發展，而在大蕭條和二戰時期逐漸發展大型農業與都市化。

## 人口

2003年人口普查顯示，艾奥瓦共有2,944,062人。種族比例分為：

  - 92.6%是無拉美血統的白人；
  - 2.1%是非裔美國人；
  - 2.8%是[拉美裔人](../Page/拉美裔人.md "wikilink")；
  - 1.3%是亞裔美國人；
  - 0.3%是[印地安原住民](../Page/印第安人.md "wikilink")；
  - 1.1%是混血。

艾奥瓦的前五大居民祖先為：

1.  [德國人](../Page/德國.md "wikilink")（35.7%）
2.  [愛爾蘭人](../Page/愛爾蘭.md "wikilink")（13.5%）
3.  [英國人](../Page/英國.md "wikilink")（9.5%）
4.  本土化血統者（6.6%）
5.  [挪威人](../Page/挪威.md "wikilink")（8.4%）。

艾奥瓦州有6.4%的居民在5歲以下，25.1%的居民在18歲以下，14.9%的居民在65歲以上，女性比男性多，約有50.9%。

### 宗教

在2001年時一個由[紐約市立大學所主導的調查之結果中](../Page/紐約市立大學.md "wikilink")，顯示約有52%的艾奥瓦居民為[新教徒或其衍生教派](../Page/新教徒.md "wikilink")，23%為[羅馬天主教信徒](../Page/羅馬天主教.md "wikilink")，6%為其他信仰，與13%無宗教信仰者\[1\]。

## 地理

80,392
平方英里。行政區位上，艾奥瓦州北接[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")，西邊與[內布拉斯加州與](../Page/內布拉斯加州.md "wikilink")[南達科他州相鄰](../Page/南達科他州.md "wikilink")，南接[密蘇里州](../Page/密蘇里州.md "wikilink")，東鄰[威斯康辛州與](../Page/威斯康辛州.md "wikilink")[伊利諾州](../Page/伊利諾州.md "wikilink")。

本州共轄99個郡，可

## 重要市鎮

[Iowa_overview.jpg](https://zh.wikipedia.org/wiki/File:Iowa_overview.jpg "fig:Iowa_overview.jpg")

  - [德梅因](../Page/德梅因.md "wikilink")（Des Moines）
  - [達芬波特](../Page/達芬波特.md "wikilink")（Davenport）
  - [锡达拉皮兹](../Page/锡达拉皮兹.md "wikilink")(Cedar Rapids)
  - [迪比克](../Page/迪比克.md "wikilink")（Dubuque）
  - [蘇城](../Page/蘇城.md "wikilink")（Sioux City）
  - [滑鐵盧](../Page/滑鐵盧_\(艾奥瓦州\).md "wikilink")（Waterloo）
  - [艾姆斯](../Page/艾姆斯.md "wikilink")（Ames），是[艾奥瓦州立大學](../Page/艾奥瓦州立大學.md "wikilink")（Iowa
    State University）所在地
  - [艾奥瓦城](../Page/艾奥瓦城_\(艾奥瓦州\).md "wikilink")（Iowa
    City），是[艾奥瓦大學](../Page/艾奥瓦大學.md "wikilink")（University
    of Iowa）所在地

## 姐妹州(省、县或地区)

  - [山梨縣](../Page/山梨縣.md "wikilink")（1960年）

  - [猶加敦](../Page/猶加敦.md "wikilink")（1964年）

  - [河北省](../Page/河北省.md "wikilink")（1983）

  - [登嘉樓](../Page/登嘉樓.md "wikilink")（1987）

  - [台灣](../Page/臺灣_\(中華民國\).md "wikilink")（1989）

  - [斯塔夫羅波爾邊疆區](../Page/斯塔夫羅波爾邊疆區.md "wikilink")（1989）

  - [切爾卡瑟](../Page/切爾卡瑟.md "wikilink")（1996）

  - [威尼托大區](../Page/威尼托大區.md "wikilink")（1997）

  - [普里什蒂納州](../Page/普里什蒂納州.md "wikilink") （2013）

## 教育

[Iowa_harvest_2009.jpg](https://zh.wikipedia.org/wiki/File:Iowa_harvest_2009.jpg "fig:Iowa_harvest_2009.jpg")[玉米田](../Page/玉米.md "wikilink")。\]\]
[June_23,_2006_657_2.JPG](https://zh.wikipedia.org/wiki/File:June_23,_2006_657_2.JPG "fig:June_23,_2006_657_2.JPG")（Clinton
County）舉辦的[熱氣球比賽](../Page/熱氣球.md "wikilink")。\]\]
[Davenport_skyline_2009.jpg](https://zh.wikipedia.org/wiki/File:Davenport_skyline_2009.jpg "fig:Davenport_skyline_2009.jpg")的天際線。\]\]
[Preston's_Station_Belle_Plaine.jpg](https://zh.wikipedia.org/wiki/File:Preston's_Station_Belle_Plaine.jpg "fig:Preston's_Station_Belle_Plaine.jpg")（Belle
Plaine）、[30號公路旁的普萊斯頓服務站](../Page/美國30號公路.md "wikilink")（Preston's
Service
Station）設立於1923年，目前已經改裝成[林肯公路](../Page/林肯公路.md "wikilink")（Lincoln
Highway）的紀念博物館。\]\]
[Ethanol_butler_co_iowa.jpg](https://zh.wikipedia.org/wiki/File:Ethanol_butler_co_iowa.jpg "fig:Ethanol_butler_co_iowa.jpg")，正在興建中的[乙醇工廠](../Page/乙醇.md "wikilink")\]\]
艾奥瓦注重教育並歷史上佔有重要的角色，由國家考試可見一斑。2003艾奥瓦的[SAT考試平均成績為全美第二高分](../Page/SAT.md "wikilink")，並在超過20%的畢業生參加測試的SAT平均成績亦為全美第二高分。[ACT試務中心位於艾奥瓦市](../Page/ACT.md "wikilink")，[ITBS與](../Page/ITBS.md "wikilink")[ITED試務系統由艾奥瓦大學建構](../Page/ITED.md "wikilink")。

愛荷華有3所州立大學，62所公立和私立大学和28所社區大學。

  - 主要大学
      - [艾奥瓦大學](../Page/艾奥瓦大學.md "wikilink")（University of
        Iowa），位於[艾奥瓦城](../Page/艾奥瓦城_\(艾奥瓦州\).md "wikilink")（Iowa
        city）
      - [艾奥瓦州立大學](../Page/艾奥瓦州立大學.md "wikilink")（Iowa State
        University），位於[艾姆斯](../Page/艾姆斯.md "wikilink")（Ames）
      - [北艾奥瓦大學](../Page/北艾奥瓦大學.md "wikilink")（University of Northern
        Iowa，位於[滑鐵盧](../Page/滑鐵盧_\(艾奥瓦州\).md "wikilink")（Waterloo）
  - 獨立學院與大學
      - [阿什福德大学](../Page/阿什福德大学.md "wikilink") (Ashford University)
      - [布莱尔克利夫大学](../Page/布莱尔克利夫大学.md "wikilink") (Briar Cliff
        University)
      - [比尤纳维斯特大学](../Page/比尤纳维斯特大学.md "wikilink") (Buena Vista
        University)
      - [中央学院](../Page/中央学院.md "wikilink") (Central College)
      - [克拉克学院](../Page/克拉克学院.md "wikilink") (Clarke College)
      - [寇伊学院](../Page/寇伊学院.md "wikilink") (Coe College)
      - [康奈尔学院](../Page/康奈尔学院.md "wikilink") (Cornell College)
      - [圣言学院](../Page/圣言学院.md "wikilink")(Divine Word College)
      - [多尔特学院](../Page/多尔特学院.md "wikilink") (Dordt College)
      - [德雷克大学](../Page/德雷克大学.md "wikilink") (Drake University)
      - [以马忤斯圣经学院](../Page/以马忤斯圣经学院.md "wikilink")（Emmaus Bible College）
      - [信仰浸礼会圣经学院与圣学院](../Page/信仰浸礼会圣经学院与圣学院.md "wikilink")（Faith
        Baptist Bible College and Theological Seminary）
      - [格里斯兰大学](../Page/格里斯兰大学.md "wikilink")（Graceland University）
      - [格兰德弗学院](../Page/格兰德弗学院.md "wikilink")（Grand View College）
      - [格林内尔学院](../Page/格林内尔学院.md "wikilink") (Grinnell College)
      - [衣阿华卫斯理学院](../Page/衣阿华卫斯理学院.md "wikilink")（Iowa Wesleyan
        College）
      - [洛拉斯学院](../Page/洛拉斯学院.md "wikilink")（Loras College）
      - [路德学院](../Page/路德学院.md "wikilink")（Luther College）
      - [玛赫西管理大学](../Page/玛赫西管理大学.md "wikilink")（Maharishi University of
        Management）
      - [莫宁塞德学院](../Page/莫宁塞德学院.md "wikilink")（Morningside College）
      - [慈悲山学院](../Page/慈悲山学院.md "wikilink")（Mount Mercy College）
      - [西北学院](../Page/西北学院.md "wikilink")（Northwestern College）
      - [辛普森学院](../Page/辛普森学院.md "wikilink")（Simpson College）
      - [圣安布罗斯大学](../Page/圣安布罗斯大学.md "wikilink")（Saint Ambrose
        University）
      - [杜比克大学](../Page/杜比克大学.md "wikilink")（University of Dubuque）
      - [艾奥瓦上层大学](../Page/艾奥瓦上层大学.md "wikilink")（Upper Iowa University）
      - [文那德大学](../Page/文那德大学.md "wikilink")（Vennard College）
      - [沃尔多夫学院](../Page/沃尔多夫学院.md "wikilink")（Waldorf College）
      - [瓦尔特堡大学](../Page/瓦尔特堡大学.md "wikilink")（Wartburg College）
      - [威廉宾州大学](../Page/威廉宾州大学.md "wikilink")（William Penn University）
  - 社區大學
      - [克林顿社区学院](../Page/克林顿社区学院.md "wikilink")（Clinton Community
        College）
      - [得梅因地区社区学院](../Page/得梅因地区社区学院.md "wikilink")（Des Moines Area
        Community College）
      - [埃尔斯沃斯社区学院](../Page/埃尔斯沃斯社区学院.md "wikilink")（Ellsworth Community
        College）
      - [鹰眼社区学院](../Page/鹰眼社区学院.md "wikilink")（Hawkeye Community
        College）
      - [印第安小山社区学院](../Page/印第安小山社区学院.md "wikilink")（Indian Hills
        Community College）
      - [艾奥瓦中央社区学院](../Page/艾奥瓦中央社区学院.md "wikilink")（Iowa Central
        Community College）
      - [艾奥瓦湖社区学院](../Page/艾奥瓦湖社区学院.md "wikilink")（Iowa Lakes Community
        College）
      - [艾奥瓦西部社区学院](../Page/艾奥瓦西部社区学院.md "wikilink")（Iowa Western
        Community College）
      - [柯克伍德社区学院](../Page/柯克伍德社区学院.md "wikilink")（Kirkwood Community
        College）
      - [马歇尔敦社区学院](../Page/马歇尔敦社区学院.md "wikilink")（Marshalltown
        Community College）
      - [马斯卡廷社区学院](../Page/马斯卡廷社区学院.md "wikilink")（Muscatine Community
        College）
      - [北艾奥瓦社区学院](../Page/北艾奥瓦社区学院.md "wikilink")（North Iowa Area
        Community College）
      - [东北艾奥瓦社区学院](../Page/东北艾奥瓦社区学院.md "wikilink")（Northeast Iowa
        Community College）
      - [艾奥瓦西北社区学院](../Page/艾奥瓦西北社区学院.md "wikilink")（Northwest Iowa
        Community College）
      - [斯科特社区学院](../Page/斯科特社区学院.md "wikilink")（Scott Community
        College）
      - [东南社区学院](../Page/东南社区学院.md "wikilink")（Southeastern Community
        College）
      - [西南社区学院](../Page/西南社区学院.md "wikilink")（Southwestern Community
        College）
      - [西艾奥瓦社区学院](../Page/西艾奥瓦社区学院.md "wikilink")（Western Iowa
        Community College）
  - 專業技術學院與大學
      - [美国烘烤技术研究所商学院](../Page/美国烘烤技术研究所商学院.md "wikilink")（AIB College
        of Business）
      - [艾伦护理大学](../Page/艾伦护理大学.md "wikilink")（Allen College of Nursing）
      - [得梅因大学](../Page/得梅因大学.md "wikilink")（Des Moines University）
      - [汉弥尔顿林肯学院](../Page/汉弥尔顿林肯学院.md "wikilink")（Hamilton College）
      - [卡普兰学院](../Page/卡普兰学院.md "wikilink")（Kaplan College）
      - [怜悯健康科学学院](../Page/怜悯健康科学学院.md "wikilink")（Mercy College of
        Health Sciences）
      - [帕尔马脊骨神经医学院](../Page/帕尔马脊骨神经医学院.md "wikilink")（Palmer College of
        Chiropractic）
      - [圣路加护理及健康科学学院](../Page/圣路加护理及健康科学学院.md "wikilink")（St. Luke's
        College of Nursing and Health Sciences）
      - [翡特罗特学院](../Page/翡特罗特学院.md "wikilink")（Vatterott College）

## 交通

### 機場

得梅因国际机场

### 重要高速公路

  - [29號州際公路](../Page/29號州際公路.md "wikilink") I-29
  - [35號州際公路](../Page/35號州際公路.md "wikilink") I-35
  - [80號州際公路](../Page/80號州際公路.md "wikilink") I-80

## 政治

### 美國總統大選黨團會議

英文稱之為「**Iowa presidential
caucus**」。愛荷華州每逢4年一度的[美國總統大選期間](../Page/美國總統大選.md "wikilink")，一定會成為全國各地矚目的焦點。美國總統大選通常在該年的1月份開始進行初選的投票，初選的類型可分為「[黨團會議](../Page/黨團會議.md "wikilink")（caucus）」和「[初選](../Page/初選.md "wikilink")（primary）」兩種。最早舉行黨團會議的州就是**愛荷華州**，而最早舉行初選的州則是**[新罕布什尔州](../Page/新罕布什尔州.md "wikilink")**。新罕布什尔州的初選投票會比愛荷華州的黨團會議投票還要晚一個禮拜的時間。由於領先全國其他州開出初選結果，所以讓這兩州的初選投票有「美國總統大選前哨戰」之稱。

## 重要職業運動

### 美式足球

  - [NCAA](../Page/NCAA.md "wikilink")
      - University of Iowa
      - Iowa State University

### 篮球

  - [NCAA](../Page/NCAA.md "wikilink")
      - University of Iowa
      - Iowa State University

### 棒球

  - [小聯盟](../Page/小聯盟.md "wikilink")
      - 艾奥瓦小熊（Iowa
        Cubs，3A級太平洋岸聯盟，母隊：[芝加哥小熊](../Page/芝加哥小熊.md "wikilink")）
      - 布靈頓蜜蜂（Burlington
        Bees，1A級中西部聯盟，母隊：[堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")）
      - 洋杉激流市麥粒（Cedar Rapids
        Kernals，1A級中西部聯盟，母隊：[洛杉磯安那罕天使](../Page/洛杉磯安那罕天使.md "wikilink")）
      - 四聯市搖擺（Swing of Quad
        Cities，1A級中西部聯盟，母隊：[聖路易紅雀](../Page/聖路易紅雀.md "wikilink")）
      - 柯林頓伐木王（Clinton Lumber
        Kings，1A級中西部聯盟，母隊：[德克薩斯遊騎兵](../Page/德克薩斯遊騎兵_\(棒球\).md "wikilink")）

## 註釋

## 參考文獻

## 外部链接

  - [州政府主页](http://www.iowa.gov)

[Category:美国州份](../Category/美国州份.md "wikilink")
[\*](../Category/艾奥瓦州.md "wikilink")
[Category:美国中西部](../Category/美国中西部.md "wikilink")

1.  [American Religious Identification
    Survey](http://www.gc.cuny.edu/faculty/research_studies/aris.pdf)