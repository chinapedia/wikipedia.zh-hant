**縣道189號
屏東機場－林邊**，是位於[臺灣](../Page/臺灣.md "wikilink")[屏東縣的一條縣道級公路](../Page/屏東縣.md "wikilink")。北起[屏東縣](../Page/屏東縣.md "wikilink")[屏東市屏東機場](../Page/屏東市.md "wikilink")，南至屏東縣[林邊鄉](../Page/林邊鄉.md "wikilink")，全長共計39.936公里（公路總局資料）。有一條支線聯絡[市道188號通往](../Page/市道188號.md "wikilink")[台88線](../Page/台88線.md "wikilink")。

## 行經行政區域

## 支線

### 甲線

[TW_CHW189a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW189a.svg "fig:TW_CHW189a.svg")
**縣道189甲線
大洲－中-{庄}-**，全線均位於[屏東縣境內](../Page/屏東縣.md "wikilink")，2014年5月1日公告加工區聯外道路納編為縣道189甲線，全長共計4.8公里。

  - 行經行政區域

## 沿線風景

## 註釋

## 參考資料

## 外部連結

  -
  -
  -
  -
[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:屏東縣道路](../Category/屏東縣道路.md "wikilink")