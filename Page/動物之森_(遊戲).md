，是由[任天堂情報開發本部所開發製造的](../Page/任天堂情報開發本部.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")，於2001年在日本發行後，接著在2002年9月15日於[美國發行的版本有新增的要素](../Page/美國.md "wikilink")，稍後則是發行了[Player's
Choice的版本](../Page/廉價版.md "wikilink")，這個遊戲使用了[GameCube的內建時鐘去創造一個](../Page/任天堂GameCube.md "wikilink")[持續發展的世界](../Page/持續發展的世界.md "wikilink")。

因為它的複雜性，遊戲佔用了記憶卡的57格，而記憶卡已經被附加在遊戲本體上，且裡面內含了佔用一格記憶體的禮物檔案，如果玩家在遊戲中還擁有紅白機(FC)的遊戲，整個記憶卡的容量將被用完。

在日本，[任天堂64版的](../Page/任天堂64.md "wikilink")《動物之森》在2001年4月14日發行，而它的新版本《動物之森
+》，則在同年的12月於[GameCube上發行](../Page/Nintendo_GameCube.md "wikilink")，此外，另一個新版本《動物之森
e+》則在2003年6月27日發行。只有日本有發行任天堂64的版本。

此遊戲的[Nintendo
DS版本](../Page/Nintendo_DS.md "wikilink")[歡迎光臨\!動物之森於](../Page/歡迎光臨!動物之森.md "wikilink")2005年11月23日在日本上市，並且加入了網路連線功能，讓玩家們可以到彼此的村莊交流互動；銷售成績獲佳評。

而在2008年11月16日（美版）11月20日（日版）系列最新作《[动物之森
城市人](../Page/动物之森_城市人.md "wikilink")》（[Animal
Crossing City
Folk](../Page/Animal_Crossing_City_Folk.md "wikilink")）终于登陆家用主机[Wii上](../Page/Wii.md "wikilink")，随之配套发售的还有[Wii的新配件](../Page/Wii.md "wikilink")：[Wii
Speak](../Page/Wii_Speak.md "wikilink")。

## 遊玩

**[任天堂](../Page/任天堂.md "wikilink")**視動物之森為"[溝通遊戲](../Page/溝通遊戲.md "wikilink")"，這是個有[固定劇情的開放遊戲](../Page/固定劇情.md "wikilink")，玩家可以在裡面獨自生活，不受預設的劇情、任務限制，許多人(小孩子或成人)享受這樣與有趣動物角色"對話"的遊玩樂趣。透過使用Gamecube的內部時鐘，這個遊戲可與真實時間相對映，因此有著實際的事件發生，這包含了[美國獨立紀念日](../Page/美國獨立日.md "wikilink")、[聖誕節](../Page/聖誕節.md "wikilink")、[萬聖節](../Page/萬聖節.md "wikilink")，或其他規律性的活動，如做早操、釣魚大賽，一些玩家也會故意向前或向後調整時間來做時光旅行。

### 房屋擴建

這個遊戲比較明顯的目標是擴建玩家的房屋，房屋是拿來放置遊戲中購買、取得的家具或道具。

Tom Nook，一隻[狸](../Page/狸.md "wikilink")，他開設商店，給玩家貸款(需19,800
Bells)第一棟房子，這棟房子很小，只能放置壁紙、地毯、收音機，在付清第一筆貸款後，玩家可以向Tom
Nook提出擴建房屋的要求(需148,000
Bells)，在隔天，房屋的空間會立即變大，再次付清貸款後，玩家可以選擇繼續擴張房屋空間(需398,000
Bells)或是蓋[地下室](../Page/地下室.md "wikilink")(需498,000 Bells)，再一次付清貸款後，Tom
Nook可讓玩家最後再擴建一次，那就是蓋二樓(需798,000 Bells)。

### 風水

家具擺設會影響風水，把特定顏色的物品放在特定方位，可以增加找到 Bells或稀有物品的機率。風水也是Happy Room
Academy給分的依據。至於獎盃、特殊節日收到的物品，不受顏色或方位的限制，擺了就可以增加賺取 Bells或物品的機率。

好風水的定義是: 北邊放橘色傢飾，更容易找到Bells和物品； 東邊放紅色傢飾，更容易找到物品； 西邊放黃色傢飾，更容易找到Bells；
南邊放綠色傢飾，更容易找到Bells和物品。

### 博物館

玩家可以參觀本地的[博物館](../Page/博物館.md "wikilink")，裡面有房間可以放置魚、昆蟲、畫、化石，但是一開始博物館裡面並沒有任何東西，這些物品有賴於玩家的捐贈。魚和昆蟲可以用標本的方式捐贈、畫則是可以在其他的家具中發現、化石則是必須被發掘(但是需去驗證)。
等化石收集完後，可以一次收集一周的量拿去驗證，再賣給商店，就可以有高額收入

魚和昆蟲玩家都可以在遊戲中捕捉到，根據季節的不同會出現不一樣的種類；化石一如上一段提到的，在村莊的土地上可以看到裂痕，每天系統會自動更新，可以挖掘到三個化石和一個陷阱，土偶則在下雨或下雪過後就會出現；畫作可以向商人購買，但是買到畫之後需要先讓博物館長鑑定，確認是真畫才會收到博物館中。而且真畫很難買到

博物館內的各個項目收集完全後，玩家會收到博物館模型作為謝禮。

## 外部連結

  - [どうぶつの森](http://www.nintendo.co.jp/n01/n64/software/nus_p_nafj/index.html)
  - [动物森林](http://www.ique.com/games/21041.htm)
  - [Animal Crossing](http://www.animal-crossing.com/)

[Category:2001年电子游戏](../Category/2001年电子游戏.md "wikilink")
[1](../Category/动物之森系列电子游戏.md "wikilink")
[Category:神游科技游戏](../Category/神游科技游戏.md "wikilink")
[Category:任天堂64遊戲](../Category/任天堂64遊戲.md "wikilink")
[Category:任天堂GameCube游戏](../Category/任天堂GameCube游戏.md "wikilink")
[Category:官方简体中文化游戏](../Category/官方简体中文化游戏.md "wikilink")