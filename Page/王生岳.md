**王生岳**（），[清末](../Page/清.md "wikilink")[民国](../Page/民国.md "wikilink")[企业家](../Page/企业家.md "wikilink")，制造了[中国近代第一台万能铣床](../Page/中国.md "wikilink")，并有“铣床大王”之称。[字](../Page/字.md "wikilink")**庭豪**，[浙江省](../Page/浙江省.md "wikilink")[鄞县人](../Page/鄞县.md "wikilink")。

## 生平简历

早年就读于家乡[私塾](../Page/私塾.md "wikilink")，家境非常穷困，[父亲曾在](../Page/父亲.md "wikilink")[轮船上做厨工](../Page/轮船.md "wikilink")。1885年，赴[上海做木工学徒](../Page/上海.md "wikilink")，学刻雕花。王生岳曾在[上海元昌机器厂做学徒](../Page/上海.md "wikilink")，为其首次接触中国近代最早之机械行业。

[清朝](../Page/清朝.md "wikilink")[光绪十五年](../Page/光绪.md "wikilink")（1889年），王生岳学徒学期期限结束，就职于英资耶松船厂，担任车工。[光绪二十七年](../Page/光绪.md "wikilink")（1901年），因工作业绩突出，王生岳被升为车间的领班。[光绪三十二年](../Page/光绪.md "wikilink")（1906年），上海瑞镕船厂（今上海船厂西厂区）兼并了耶松船厂，王生岳遂入瑞镕船厂，担任领班。

[民国二年](../Page/民国.md "wikilink")（1913年），王生岳辞去瑞镕船厂职务，并自主创业。王生岳遂筹资[白银](../Page/银两.md "wikilink")2000两，创建了王岳记机器厂（今上海减速机械厂），专门从事[齿轮加工业务](../Page/齿轮.md "wikilink")，是为[上海最早的齿轮加工厂](../Page/上海.md "wikilink")，亦为[中国最早的华人齿轮加工工厂](../Page/中国.md "wikilink")。

[民国四年](../Page/民国.md "wikilink")（1915年），实业家[史鹤鸣创办了史鹤记机器厂](../Page/史鹤鸣.md "wikilink")（今上海冲剪机床厂），并需要一台万能[铣床](../Page/铣床.md "wikilink")，史鹤鸣开造价[白银](../Page/银两.md "wikilink")1200两。王生岳获得这一订单，并制造成功，是为[中国首台国产的铣床](../Page/中国.md "wikilink")。之后王生岳大力扩展企业业务和规模，并引进[德国](../Page/德国.md "wikilink")、[美国的生产技术和设备](../Page/美国.md "wikilink")，产品在国内畅销，并有力地和进口产品竟争，成为闻名一时的“铣牙大王”。

[民国二十五年](../Page/民国.md "wikilink")（1936年），[中国首座公铁路混合两用大桥](../Page/中国.md "wikilink")——[钱塘江大桥开工兴建](../Page/钱塘江大桥.md "wikilink")，主要设计者桥梁工程专家[茅以升特向王生岳订购制造大桥所需的关键重型设备和构建](../Page/茅以升.md "wikilink")，并由王生岳主持建造成功。

[民国二十六年](../Page/民国.md "wikilink")（1937年），[日本侵华战争爆发](../Page/日本侵华战争.md "wikilink")，[日軍不久即集大军大举进攻](../Page/日軍.md "wikilink")[上海](../Page/上海.md "wikilink")，王生岳的工厂不幸毁于战火。1938年，王生岳主持重建机器厂，并由其次子[王承恩接掌](../Page/王承恩.md "wikilink")。[民国三十四年](../Page/民国.md "wikilink")（1945年）10月，王生岳因病逝世。

## 参考

  - [上海机电工业志 \>\> 王生岳
    简介](http://www.shtong.gov.cn/newsite/node2/node2245/node4456/node59421/node59423/node59439/userobject1ai47922.html)

[W王](../Category/中国工程师.md "wikilink")
[W王](../Category/中国企业家.md "wikilink")
[W王](../Category/鄞县人.md "wikilink")
[S](../Category/王姓.md "wikilink")