[The_Tibetan_Exiles_March_to_Tibet_\~_Rise_Up,_Resist,_Return_流亡圖博人行進西藏_-_圖博_\~_奮起,_對抗,_返鄉.jpg](https://zh.wikipedia.org/wiki/File:The_Tibetan_Exiles_March_to_Tibet_~_Rise_Up,_Resist,_Return_流亡圖博人行進西藏_-_圖博_~_奮起,_對抗,_返鄉.jpg "fig:The_Tibetan_Exiles_March_to_Tibet_~_Rise_Up,_Resist,_Return_流亡圖博人行進西藏_-_圖博_~_奮起,_對抗,_返鄉.jpg")
[The_March_2008_Uprising_in_Tibet_for_Freedom_二零零八年三月西藏_-_圖博起義爭自由.jpg](https://zh.wikipedia.org/wiki/File:The_March_2008_Uprising_in_Tibet_for_Freedom_二零零八年三月西藏_-_圖博起義爭自由.jpg "fig:The_March_2008_Uprising_in_Tibet_for_Freedom_二零零八年三月西藏_-_圖博起義爭自由.jpg")\]\]
[Protests_Rage_Across_Tibet_as_China_Responds_with_Brute_Force_\~_Tibetan_Clash_with_Chinese_Troops_in_Lhasa_as_Unprecedented_Unrest_throughout_Tibet_抗議怒火延燒西藏_-_圖博_而中國則以殘暴武力對付西藏_-_圖博人民.jpg](https://zh.wikipedia.org/wiki/File:Protests_Rage_Across_Tibet_as_China_Responds_with_Brute_Force_~_Tibetan_Clash_with_Chinese_Troops_in_Lhasa_as_Unprecedented_Unrest_throughout_Tibet_抗議怒火延燒西藏_-_圖博_而中國則以殘暴武力對付西藏_-_圖博人民.jpg "fig:Protests_Rage_Across_Tibet_as_China_Responds_with_Brute_Force_~_Tibetan_Clash_with_Chinese_Troops_in_Lhasa_as_Unprecedented_Unrest_throughout_Tibet_抗議怒火延燒西藏_-_圖博_而中國則以殘暴武力對付西藏_-_圖博人民.jpg")
**2008年西藏人民起義運動**（**Tibetan People's Uprising
Movement**，西方媒体称作**徒步返乡**\[1\]），是由五個藏人民間團體共同發起的一連串促進西藏獨立的活動。

由於[2008年夏季奧林匹克運動會將於](../Page/2008年夏季奧林匹克運動會.md "wikilink")[中國](../Page/中國.md "wikilink")[北京舉行](../Page/北京.md "wikilink")，幾個月後的2009年3月10日又適逢[西藏人民起義日四十九週年](../Page/西藏人民起義日.md "wikilink")，為爭取世人的對西藏人權問題的關注，活動項目之一為號召100位參與者，於2008年3月10日自[西藏流亡政府所在地](../Page/西藏流亡政府.md "wikilink")[達蘭薩拉出發](../Page/達蘭薩拉.md "wikilink")，以步行方式回到故鄉[拉薩](../Page/拉薩.md "wikilink")\[2\]。

在被中國政府逮捕的在拉萨进行暴力示威的藏人中，有人供称[2008年藏人抗議事件屬於這個活動的一部分](../Page/2008年藏人抗議事件.md "wikilink")。中国政府单方面声称[第十四世達賴喇嘛是整個活動的主導人物](../Page/第十四世達賴喇嘛.md "wikilink")，但达赖喇嘛自称他个人完全支持北京奥运，但无能力控制海外流亡藏人抵制奥运。\[3\]。

## 發起團體

1.  [西藏青年大會](../Page/西藏青年大會.md "wikilink")
2.  [西藏婦女協會](../Page/西藏婦女協會.md "wikilink")（Tibetan Women's Association）
3.  [西藏九·十·三運動](../Page/西藏九·十·三運動.md "wikilink")（Gu-Chu-Sum Movement of
    Tibet）
4.  [西藏全國民主黨](../Page/西藏全國民主黨.md "wikilink")（National Democratic Party
    of Tibet）
5.  「[自由西藏學生運動](../Page/自由西藏學生運動.md "wikilink")」印度分會

## 宣言與要求

2008年1月4日，主辦單位在[印度](../Page/印度.md "wikilink")[新德里記者會上發表了宣言與要求](../Page/新德里.md "wikilink")，包括\[4\]：

1.  “排除所有阻礙，無條件地讓達賴喇嘛回西藏並擔任西藏人民的合法領袖”。
2.  “開始撤除對西藏的殖民佔據。中國的殖民統治和遷徙漢人到西藏的做法正逐漸將西藏人民排除在社會主流之外，並令我們在自己的土地上成為少數民族”。
3.  “釋放所有以任何形式拘禁的藏人政治犯，並恢復西藏人民的人權”。
4.  要求[國際奧林匹克委員會取消举行](../Page/國際奧林匹克委員會.md "wikilink")[2008年北京奧運](../Page/2008年北京奧運.md "wikilink")。

## 參考資料

## 外部連結

[Category:2008年西藏](../Category/2008年西藏.md "wikilink")
[Category:西藏獨立運動](../Category/西藏獨立運動.md "wikilink")

1.  [西藏流亡人士徒步游行抗议北京奥运](http://news.bbc.co.uk/chinese/simp/hi/newsid_7170000/newsid_7171600/7171692.stm)、[印度流亡藏人“返乡”受阻
    系列活动吁中共停止镇压](http://www.rfa.org/mandarin/shenrubaodao/2008/03/13/zang/)

2.  [Background](http://tibetanuprising.org/category/background/)「西藏人民大起義運動」活動
    官方網站
3.  [达赖集团操纵“西藏人民大起义运动”内幕](http://news.xinhuanet.com/newscenter/2008-04/01/content_7900919.htm)
    2008年04月1日 新华网
4.  [Exiled Tibetans plan protest March to Tibet ahead of Beijing
    Olympics](http://www.phayul.com/News/article.aspx?article=Exiled+Tibetans+plan+protest+March+to+Tibet+ahead+of+Beijing+Olympics&id=18982&c=1&t=1)