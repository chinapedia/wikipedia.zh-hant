**無國界醫生**（，发音，，简称：**MSF**），是一个獨立的、从事[人道救援的](../Page/人道救援.md "wikilink")[非政府组织](../Page/非政府组织.md "wikilink")，以在飽受戰爭摧殘的地區和貧窮國家致力協助抵抗[地方疾病的計畫聞名](../Page/地方性流行.md "wikilink")。\[1\]

## 組織和機構

[Médecins_Sans_Frontières_-_Missions.svg](https://zh.wikipedia.org/wiki/File:Médecins_Sans_Frontières_-_Missions.svg "fig:Médecins_Sans_Frontières_-_Missions.svg")
1968年至1970年，參加[法國](../Page/法國.md "wikilink")[紅十字會在](../Page/紅十字會.md "wikilink")[非洲](../Page/非洲.md "wikilink")[比夫拉救援工作的一个法國醫生](../Page/比夫拉.md "wikilink")[貝爾納·庫希內](../Page/貝爾納·庫希內.md "wikilink")，看到當地一個又一個骷髏似的瘦削孩子，深為未能有效提供救援感到無奈和激憤。1971年，法國醫生們在[尼日利亞內戰後](../Page/尼日利亞內戰.md "wikilink")，成立了無國界醫生組織。醫生們深信，無論人們的種族、宗教信仰與政治立場為何，任何人都有獲得醫療保健的權利，而且人們的這些需求是超越國界的。加拿大籍華裔醫生[廖滿嫦](../Page/廖滿嫦.md "wikilink")（Joanne
Liu）自2013年起擔任組織國際主席（président international）。

無國界醫生是全球最大的獨立醫療救援組織，目前總部設於[瑞士的](../Page/瑞士.md "wikilink")[日內瓦](../Page/日內瓦.md "wikilink")，有五個主要的行動中心位於[歐洲](../Page/歐洲.md "wikilink")，分別是[巴黎](../Page/巴黎.md "wikilink")、[布魯塞爾](../Page/布魯塞爾.md "wikilink")、[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")、[巴塞隆納和](../Page/巴塞隆納.md "wikilink")[日內瓦](../Page/日內瓦.md "wikilink")。此組織的目標是｢不分種族、國家與宗教背景、義務的協助戰火和自然災害中受傷的人類得到醫治｣。無國界醫生組織經常深入戰亂地區，生命和義務工作等也常受到威脅。他們經常會代表受害的地區向聯合國提交抗議，例如對[車臣和](../Page/車臣.md "wikilink")[科索沃戰亂的譴責](../Page/科索沃.md "wikilink")。

無國界醫生組織目前針對下列四大項狀況進行醫療協助：

  - 針對戰爭和內亂地區的民眾進行緊急醫療幫助
  - 針對難民和流亡的群眾進行醫療安置和協助
  - 天然或人為災難的緊急醫療支持
  - 長期對偏遠地區做醫療協助

無國界醫生組織的總部設在[瑞士](../Page/瑞士.md "wikilink")[日內瓦並有](../Page/日內瓦.md "wikilink")20個部門。該組織每年招募大約3000名醫生，護士，助產士，和後勤人員執行各個計劃，另外還有1000名長期工作人員負責招募志願者和處理財務和媒體關係。該組織80%的資金來自醫生與個人捐款，其餘的來自於政府和企業的捐助。這些使得无国界医生的年預算大約有4億[美元](../Page/美元.md "wikilink")\[2\]

無國界醫生組織積極地為70余個國家人民提供衛生保健和醫療培訓，並且一貫堅持在衝突地區比如[車臣和](../Page/車臣.md "wikilink")[科索沃的政治責任](../Page/科索沃.md "wikilink")。在它的歷史上多次進行抗議活動，
包括在[1994年盧旺達種族屠殺中](../Page/盧旺達大屠殺.md "wikilink")，該組織呼吁軍事干涉，以及對柬埔寨赤棉的屠殺等。

無國界醫生於1999年獲頒[諾貝爾和平獎](../Page/諾貝爾和平獎.md "wikilink")，以肯定他們不斷努力在緊急危機事故發生時提供醫療服務，並引起國際對可能發生的人道危機事件的關注。\[3\]

以下列出無國界醫生的創始人名單:

  - 馬塞爾·德爾古醫生
    <span style="font-size:smaller;">Dr Marcel Delcourt</span>

<!-- end list -->

  - 馬克斯·雷卡米耶醫生
    <span style="font-size:smaller;">Dr Max Recamier</span>

<div style="background:#eeeeee;">

  - 熱拉爾·皮容醫生
    <span style="font-size:smaller;">Dr Gérard Pigeon</span>

</div>

  - [貝爾納·庫什內醫生](../Page/貝爾納·庫什內.md "wikilink")
    <span style="font-size:smaller;">Dr Bernard Kouchner</span>

<div style="background:#eeeeee;">

  - 雷蒙·博雷爾醫生
    <span style="font-size:smaller;">Dr Raymond Borel</span>

</div>

  - 讓·卡布羅爾醫生
    <span style="font-size:smaller;">Dr Jean Cabrol</span>

<div style="background:#eeeeee;">

  - 弗拉丹·拉多曼醫生
    <span style="font-size:smaller;">Dr Vladan Radoman</span>

</div>

  - 讓-米歇爾·維爾德醫生
    <span style="font-size:smaller;">Dr Jean-Michel Wild</span>

<div style="background:#eeeeee;">

  - 帕斯卡爾·格勒萊蒂-博斯維埃爾醫生
    <span style="font-size:smaller;">Dr Pascal Greletty-Bosviel</span>

</div>

<div style="background:#eeeeee;">

  - 雅克·貝雷斯醫生
    <span style="font-size:smaller;">Dr Jacques Bérés</span>

</div>

  - 熱拉爾·伊利烏茲醫生
    <span style="font-size:smaller;">Dr Gérard Illiouz</span>

<div style="background:#eeeeee;">

  - 菲利普·貝尼耶
    <span style="font-size:smaller;">Philippe Bernier</span>

</div>

  - 格扎維埃·埃馬努埃利醫生
    <span style="font-size:smaller;">Dr Xavier Emmanuelli</span>

## 无国界医生憲章

无国界医生的成员大多是医生和医务人员，但也得到许多其它职业人士的帮助。所有成员均遵循以下的原则：

  - 無國界醫生不分種族、宗教、信仰和政治立場，為身處困境的人們以及天災人禍和武裝衝突的受害者提供援助。
  - 無國界醫生遵循國際醫療守則，堅持人道援助的權利，恪守中立不偏的立場，並要求在其行動中不受任何阻撓。
  - 全體成員嚴格遵循其職業規範，並且完全獨立於任何政治、經濟和宗教勢力之外。
  - 作為志願者，全體成員深諳執行組織的使命所面臨的風險和困難，並且不會要求組織向其本人或受益人作出超乎該組織所能提供的賠償。

## 工作的危险性

除了要面对在战地和瘟疫区的死伤威胁，无国界医生志愿者有时还会因为政治原因被攻击或绑架。在一些进行内战的国家中，对其中单方的人道主义援助会被认为是对敌方的帮助，并因而被袭击，例如：

  - 无国界医生北[高加索代表团团长](../Page/高加索.md "wikilink")[Arjan
    Erkel在](../Page/Arjan_Erkel.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[達吉斯坦共和国被绑架](../Page/達吉斯坦共和国.md "wikilink")，从2002年8月12日到2004年4月11日一直被扣留作[人质](../Page/人质.md "wikilink")。
  - 2004年6月2日，5名无国界医生工作者在[阿富汗Badghis省](../Page/阿富汗.md "wikilink")[Khair
    Khana附近遭埋伏牺牲](../Page/Khair_Khana.md "wikilink")，他们分别是：阿富汗人[Fasil
    Ahmad和](../Page/Fasil_Ahmad.md "wikilink")[Besmillah](../Page/Besmillah.md "wikilink")、[比利时人](../Page/比利时.md "wikilink")[Helene
    de
    Beir](../Page/Helene_de_Beir.md "wikilink")、[挪威人](../Page/挪威.md "wikilink")[Egil
    Tynaes](../Page/Egil_Tynaes.md "wikilink")、以及[荷兰人](../Page/荷兰.md "wikilink")[Willem
    Kwint](../Page/Willem_Kwint.md "wikilink")。阿富汗政府没有逮捕凶手。[塔利班发言人](../Page/塔利班.md "wikilink")[Abdul
    Hakim
    Latifi发表言论对这次袭击负责](../Page/Abdul_Hakim_Latifi.md "wikilink")。自2003年开始，共计30多位无国界医生的成员在阿富汗陆续遇害身亡。7月28日，无国界医生因为因成员没有充分的安全保障，所以决定退出已驻守有24年的阿富汗地区、将医疗工作交接给阿富汗政府当局。无国界医生在他们的刊物中发表文章斥责塔利班和美国军方。他们谴责塔利班以救援工作者为袭击目标：“这一恐怖袭击意味着塔利班拒绝接受中立的人道主义行动。”对美国军方方面，他们谴责：“这次恐怖袭击矛头直接指向人道救援工作者，因为恐怖分子认为美国暗地里一直寻求与人道救援组织合作以达到军事和政治的野心。无国界医生公开谴责这种合作会被军方利用作赢取民心。如果这样，所提供的救援就不再被视为中立的行为，这样会危及人道主义志愿者的生命安全并使需要帮助的人得不到帮助。2004年5月12日，无国界医生公开谴责阿富汗南部的联合军散发传单称如果塔利班和[阿爾蓋達組織想继续得到救援就必须提供他们的訊息](../Page/阿爾蓋達組織.md "wikilink")。”

## 缘起

早在无国界医生创立（1971年）之前，向有需要的人群提供紧急食品和药物援助的组织（例如[樂施會](../Page/樂施會.md "wikilink")）就已经存在了。创立于1863年的[国际红十字会](../Page/国际红十字会.md "wikilink")（CICR/ICRC），是受[战争及](../Page/战争.md "wikilink")[自然灾害影响的人民接受紧急食品及药物援助的主要来源](../Page/自然灾害.md "wikilink")。[第二次世界大战之后](../Page/World_War_II.md "wikilink")，国际红十字会因对[屠杀保持沉默而开始受到批评](../Page/屠杀.md "wikilink")。一些人（例如无国界医生的创始人之一[貝爾納·庫希內](../Page/貝爾納·庫希內.md "wikilink")（Bernard
Kouchner））则认为，在危机的状况下，该组织的模稜兩可的中立就是共犯。

### 比夫拉

[Starved_girl.jpg](https://zh.wikipedia.org/wiki/File:Starved_girl.jpg "fig:Starved_girl.jpg")内战中罹患[恶性营养不良的孩子](../Page/恶性营养不良.md "wikilink")\]\]
在1967-1970年的[尼日利亚内战中](../Page/尼日利亚内战.md "wikilink")，尼日利亚军队包围並封锁了新独立的东南地区，[比夫拉](../Page/比夫拉.md "wikilink")。当时，法国是唯一一个支持比夫拉人的大国（[英国](../Page/英国.md "wikilink")、[前苏联和](../Page/前苏联.md "wikilink")[美国皆站在尼日利亚政府一边](../Page/美国.md "wikilink")），而且外界对封锁区内的情况一无所知。[贝尔纳·库什内当时是通过法国红十字会志愿前往被包围的比夫拉的医院和哺育中心的法国医生中的一员](../Page/贝尔纳·库什内.md "wikilink")。红十字会要求志愿者们签署一份被部分人（像庫希內和他的支持者们）认为是禁口令的协议，来保持该组织无条件的中立。庫希內和其他法国医生签署了这份协议。

进入尼日利亚之后，这些志愿者，以及比夫拉的医疗工作者和医院受到了尼日利亚军队的攻击，并且目睹了平民被杀害和在封锁中忍饥挨饿。庫希內也目睹了这些场面，特别是大量饥饿的儿童。当他回到法国之后，他公开批评尼日利亚政府和红十字会看似同谋一样的行为。在其他法国医生的帮助下，庫希內将比夫拉置于媒体的聚光灯下，并呼吁国际社会的回应。庫希內领导下的这些医生，决定成立一个新的，不理会政治或信仰界限而以受害者利益为先的救助组织。\[4\]

## 無國界醫生的發展

在1982年，Malhuret和Rony
Brauman（在1982年成為該組織的會長），藉由郵件介紹資金募集活動增加捐款收入，促使該組織財務獨立。1980年代，法國無國界醫生（1971）促使多國成立無國界醫生行動中心（管理救援項目的總部）：比利時在1980年成立、瑞士在1981年、荷蘭於1984年、西班牙則成立於1986年。盧森堡是第一個分部（負責籌款、招募海外工作人員、推廣及技術救援），成立於1986年。在1990年代，多數的國家都成立分部：希臘、美國無國界醫生成立於1990年，加拿大成立於1991年、日本在1992年成立、[-{zh-hans:英联邦;zh-hk:英聯邦;zh-tw:大英國協;}-及義大利於](../Page/英聯邦.md "wikilink")1993年成立、1994年澳大利亞也成立分部；還有德國、奧地利、丹麥、瑞典、挪威、香港（阿拉伯則稍晚成立）。2007年香港办事处在中国广州成立代表处，为无国界医生于中国大陆的救援项目提供支持。

在Kouchner離開後，Malhuret和Rony
Brauman帶給無國界醫生組織極大的改變。1979年12月，蘇聯入侵阿富汗後，無國界醫生立即展開實地救援的任務，提供醫藥援助給伊斯蘭游擊隊戰士。1980年二月，無國界醫生公開譴責赤棉。1984年到1985年衣索比亞的飢荒，無國界醫生在1984年於該國成立營養計畫，但在1985年因譴責衣索比亞濫用國際援助及武裝移民遭到驅逐。無國界醫生並在1986年侵襲薩爾瓦多的大地震後，提供潔淨的水源給該國首都聖撒爾瓦多。

### 統計數據

為了向全世界明確報告人道救援的緊急狀況，無國界醫生組織會在每一次任務中都會[統計相關數據](../Page/統計.md "wikilink")，兒童營養不良的比例可間接用來衡量整體族群營養不良率，並因此得知每個供給中心所需的糧食量\[5\]。

## 參見

  - [红十字会与红新月会国际联合会](../Page/红十字会与红新月会国际联合会.md "wikilink")

## 參考文獻

## 延伸閱讀

<small>

  - Bortolotti, D (2004). [*Hope in Hell: Inside the World of Doctors
    Without Borders*](http://www.无国界医生.ca/hopeinhell/enter.html),
    Firefly Books.

  - Greenberg KS (2002). , The Concord Review.

</small> <small>

  - McCall M, Salama P (1999). [*Selection, training, and support of
    relief workers: an occupational health
    issue*](http://bmj.bmjjournals.com/cgi/content/full/bmj;318/7176/113),
    British Medical Journal 318: 113-116.

  - Zwi AB (2004). [*How should the health community respond to violent
    political
    conflict?*](http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=523835),
    PLoS Medicine (online) 1 (1).

  - Katz IT, Wright AA (2004). [*Collateral Damage — Médecins sans
    Frontières Leaves Afghanistan and
    Iraq*](http://content.nejm.org/cgi/content/full/351/25/2571), The
    New England Journal of Medicine 351 (25): 2571-2573.

</small>

## 外部連結

<small>

  -   - [无国界医生香港办事处](http://www.msf.org.hk/)
      - [无国界医生广州代表处](http://www.msf.org.cn/)

  - [諾貝爾和平獎官方網站－無國界醫生](http://www.nobel.se/peace/laureates/1999/index.html)

  - [Essentialdrugs.org](https://web.archive.org/web/20060104130535/http://www.essentialdrugs.org/)

  - [Observatoire de l'action
    humanitaire](https://web.archive.org/web/20050509091516/http://www.observatoire-humanitaire.org/fusion.php?l=FR&id=23)

  - [Drugs for Neglected Diseases Initiative](http://www.dndi.org/)

</small>

{{-}}

[\*](../Category/无国界医生.md "wikilink")
[Category:國際醫療與健康組織](../Category/國際醫療與健康組織.md "wikilink")
[Category:非政府间国际组织](../Category/非政府间国际组织.md "wikilink")
[Category:國際慈善组织](../Category/國際慈善组织.md "wikilink")
[Category:获得诺贝尔和平奖的组织](../Category/获得诺贝尔和平奖的组织.md "wikilink")
[Category:总部在日内瓦的国际组织](../Category/总部在日内瓦的国际组织.md "wikilink")
[Category:1971年建立的組織](../Category/1971年建立的組織.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")
[Category:人道援助组织](../Category/人道援助组织.md "wikilink")

1.  [我在南苏丹：一个无国界医生的奇幻旅程](http://www.apdnews.com/news/30052.html)
    ，亚太日报，2013年8月15日
2.  Forsythe, David P. (2005) *The Humanitarians: The International
    Committee of the Red Cross*, Cambridge University Press. ISBN
    0-521-61281-0.
3.  [无国界医生-USA: Special Report: The 10 Most Underreported Humanitarian
    Crises
    of 2005](http://www.doctorswithoutborders.org/publications/reports/2006/top10_2005.html)
4.  Bortolotti, Dan (2004). *Hope in Hell: Inside the World of Doctors
    Without Borders*, Firefly Books. ISBN 1-55297-865-6.
5.  无国界医生 Article (2002) [Malnutrition: rates and
    measures](http://www.无国界医生.org/无国界医生international/invoke.cfm?component=article&objectid=09972681-0B23-4AF6-B488A0B4A177C6CF&method=full_html)
    **无国界医生**. Retrieved Dec. 28, 2005.