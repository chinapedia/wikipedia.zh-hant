**弗米利恩縣**（**Vermillion Parish,
Louisiana**）是[美國](../Page/美國.md "wikilink")[路易斯安那州西南部的一個縣](../Page/路易斯安那州.md "wikilink")，南傍[墨西哥灣](../Page/墨西哥灣.md "wikilink")。面積3,984平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口53,807人。縣治[阿比維爾](../Page/阿比維爾_\(路易斯安那州\).md "wikilink")
(Abbevile)。

成立於1844年3月25日，其名來自[弗米利恩河和](../Page/弗米利恩河_\(路易斯安那\).md "wikilink")[弗米利恩灣](../Page/弗米利恩灣.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[V](../Category/路易斯安那州行政區劃.md "wikilink")

1.  [Vermilion
    Parish](http://www.dhh.louisiana.gov/OPH/PHP%202005/PDF/Vermillion/Parish%20Vermilion.pdf)