**吳京**，[满族](../Page/满族.md "wikilink")，老姓[乌雅氏](../Page/乌雅氏.md "wikilink")\[1\]，北京人，中国大陸男演员、导演。2017年自導自演電影《[戰狼2](../Page/戰狼2.md "wikilink")》票房突破50億，并憑此片奪得[百花獎最佳男主角](../Page/百花獎.md "wikilink")。妻子是光線女主持[謝楠](../Page/謝楠.md "wikilink")，共同生育了兩個兒子。圈中好友是[鄧超](../Page/鄧超.md "wikilink")，二人互相投資合作彼此的作品。

## 簡歷

吳京原為[武術](../Page/武術.md "wikilink")[運動員](../Page/運動員.md "wikilink")，六歲加入北京武術隊，年約十二歲成為全國武術少年組比賽拳、槍、刀冠軍\[2\]。之後獲香港著名導演[袁和平賞識力邀接拍武打](../Page/袁和平.md "wikilink")[電影](../Page/電影.md "wikilink")\[3\]，開始進軍演藝圈，於[香港及](../Page/香港.md "wikilink")[中國大陸接拍](../Page/中國大陸.md "wikilink")[電影及](../Page/電影.md "wikilink")[電視連續劇](../Page/電視連續劇.md "wikilink")，主要題材為武打。

2014年，因拍摄自导自演的动作片《[战狼](../Page/战狼.md "wikilink")》致腿部受伤，进行了[手术](../Page/手术.md "wikilink")\[4\]。

2017年，吳京自導自演電影《[戰狼2](../Page/戰狼2.md "wikilink")》成為首部打入全球TOP
100票房影片榜的中国國產片\[5\]。翌年，憑此片榮获[第34屆大眾電影百花獎](../Page/第34屆大眾電影百花獎.md "wikilink")「最佳男主角」獎，首度封帝。\[6\]

2019年，吴京带资进组，投资并拍摄《流浪地球》，并零片酬饰演刘培强一角，流浪地球票房一路高涨成为2019年春节档票房最高的电影。

## 作品

### 电视剧

|                                               |                                                 |                                |                  |
| --------------------------------------------- | ----------------------------------------------- | ------------------------------ | ---------------- |
| 首播年份                                          | 剧名                                              | 角色                             | 备注               |
| 1998                                          | [太极宗师](../Page/太极宗师.md "wikilink")              | 杨昱乾                            |                  |
| [新水浒后传](../Page/新水浒后传.md "wikilink")          | 西门金哥                                            | 又名「拭血问剑」                       |                  |
| 1999                                          | [小李飛刀](../Page/小李飛刀_\(1999年電視劇\).md "wikilink") | [阿飛](../Page/阿飛.md "wikilink") |                  |
| 2000                                          | [新少林寺](../Page/新少林寺.md "wikilink")              | [觉远](../Page/觉远.md "wikilink") |                  |
| [乱世桃花](../Page/乱世桃花.md "wikilink")            | [裴元慶](../Page/裴元慶.md "wikilink")                |                                |                  |
| [大人物](../Page/大人物_\(2000年電視劇\).md "wikilink") | 杨凡                                              | 又名「凡人楊大頭」/「絕命鴛鴦」               |                  |
| 2001                                          | [策马啸西风](../Page/策马啸西风.md "wikilink")            | 孟星魂                            |                  |
| [大钦差之皇城神鹰](../Page/大钦差之皇城神鹰.md "wikilink")    | 齊雲傲                                             |                                |                  |
| 2002                                          | [金蚕丝雨](../Page/金蚕丝雨.md "wikilink")              | 云飞扬                            |                  |
| [江山儿女几多情](../Page/江山儿女几多情.md "wikilink")      | [乾隆帝](../Page/乾隆帝.md "wikilink")                |                                |                  |
| [江山为重](../Page/江山为重.md "wikilink")            | 乾隆，陳邦國                                          |                                |                  |
| 2003                                          | [少林武王](../Page/少林武王.md "wikilink")              | 曇宗                             |                  |
| 南少林                                           | 方世玉                                             |                                |                  |
| [倩女幽魂](../Page/倩女幽魂_\(電視劇\).md "wikilink")    | 諸葛流雲                                            |                                |                  |
| 2004                                          | [南少林三十六房](../Page/南少林三十六房.md "wikilink")        | 方世玉                            |                  |
| [谁为我心动](../Page/谁为我心动.md "wikilink")          | 白翔飛                                             |                                |                  |
| 2006                                          | [武当II](../Page/武当II.md "wikilink")              | 張無極                            |                  |
| [鼓上蚤时迁](../Page/鼓上蚤时迁.md "wikilink")          | 时迁                                              |                                |                  |
| 2012                                          | [我是特种兵之利刃出鞘](../Page/我是特种兵之利刃出鞘.md "wikilink")  | 何衛東，何晨光                        | 何衛東是何晨光父親，第一集裡戰死 |
| 2014                                          | [极品女士第三季第四集](../Page/极品女士.md "wikilink")        | 客串                             | 網絡劇              |
|                                               |                                                 |                                |                  |

### 電影

<table>
<tbody>
<tr class="odd">
<td><p>上映年份</p></td>
<td><p>電影名稱</p></td>
<td><p>飾演</p></td>
<td><p>备注</p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/功夫小子闖情關.md" title="wikilink">功夫小子闖情關</a>[7][8]</p></td>
<td><p>楊學文</p></td>
<td><p>香港譯名為《太極拳》</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/蜀山傳.md" title="wikilink">蜀山傳</a></p></td>
<td><p>廉刑</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/醉猴.md" title="wikilink">醉猴</a></p></td>
<td><p>陳秋德（陳永業）</p></td>
<td><p>香港譯名為《醉馬騮》</p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/殺破狼.md" title="wikilink">殺破狼</a></p></td>
<td><p>殺手Jack</p></td>
<td><p>特別演出</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/天上掉馅饼.md" title="wikilink">天上掉馅饼</a></p></td>
<td><p>榴溜</p></td>
<td><p>只在中國內地上映</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑拳.md" title="wikilink">黑拳</a></p></td>
<td><p>高崗</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/雙子神偷.md" title="wikilink">雙子神偷</a></p></td>
<td><p>劉曦、劉晨</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/男兒本色_(電影).md" title="wikilink">男兒本色</a></p></td>
<td><p>天養生</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/奪帥.md" title="wikilink">奪帥</a></p></td>
<td><p>駱天虹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我的最愛_(電影).md" title="wikilink">我的最愛</a></p></td>
<td><p>Michael</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/盜墓迷城3.md" title="wikilink">盜墓迷城3</a></p></td>
<td><p>刺客</p></td>
<td><p>客串，首部好莱坞作品</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狼牙.md" title="wikilink">狼牙</a></p></td>
<td><p>布同林</p></td>
<td><p>首部自導自演作品</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/機器俠.md" title="wikilink">機器俠</a></p></td>
<td><p>K88機器人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/愛情36計.md" title="wikilink">愛情36計</a></p></td>
<td><p>吳杰倫</p></td>
<td><p>客串<br />
只在中國內地上映</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/越光寶盒.md" title="wikilink">越光寶盒</a></p></td>
<td><p>騎兵組長</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/全城戒備.md" title="wikilink">全城戒備</a></p></td>
<td><p>孫皓</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西風烈_(2010年電影).md" title="wikilink">西風烈</a></p></td>
<td><p>楊曉明（羊倌兒）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/新少林寺_(电影).md" title="wikilink">新少林寺</a></p></td>
<td><p>少僧淨能</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開心魔法.md" title="wikilink">開心魔法</a></p></td>
<td><p>畢野武</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/不二神探.md" title="wikilink">不二神探</a></p></td>
<td><p>保險員</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/分手大师.md" title="wikilink">分手大师</a></p></td>
<td><p>客户</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/戰狼.md" title="wikilink">戰狼</a></p></td>
<td><p>冷鋒</p></td>
<td><p>首部自編作品；第二部自導自演作品</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/殺破狼2.md" title="wikilink">殺破狼2</a></p></td>
<td><p>志傑</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><a href="../Page/危城.md" title="wikilink">危城</a></p></td>
<td><p>張亦</p></td>
<td><p>特別演出</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大话西游3_(电影).md" title="wikilink">大话西游3</a></p></td>
<td><p><a href="../Page/唐僧.md" title="wikilink">唐僧</a></p></td>
<td><p>特別演出</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/战狼2.md" title="wikilink">战狼2</a></p></td>
<td><p>冷锋</p></td>
<td><p>第二部自編作品；第三部自導自演作品</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/功守道.md" title="wikilink">功守道</a></p></td>
<td><p>高手京</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/祖宗十九代.md" title="wikilink">祖宗十九代</a></p></td>
<td><p>梅辦法</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p><a href="../Page/流浪地球_(电影).md" title="wikilink">流浪地球</a></p></td>
<td><p>刘培强</p></td>
<td><p>兼任"出品人"</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/老师·好.md" title="wikilink">老师·好</a></p></td>
<td><p>体育老师</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/攀登者.md" title="wikilink">攀登者</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 音樂錄影帶

|       |                                  |                  |
| ----- | -------------------------------- | ---------------- |
| 年份    | 歌手                               | 歌名               |
| 2008年 | [蕭亞軒](../Page/蕭亞軒.md "wikilink") | 《More More More》 |
|       |                                  |                  |

## 真人秀節目

  - 2015年《[出发吧爱情](../Page/出发吧爱情.md "wikilink")》
  - 2015年《[今晚睇李](../Page/今晚睇李.md "wikilink")》
  - 2017年《[真星話大冒險](../Page/真星話大冒險.md "wikilink")》第12期

## 獎項

  - 1984年：北京武術比賽拳術冠軍。
  - 1986年：全國武術比賽拳、槍、刀冠軍。
  - 1987年：全國武術比賽拳、槍、刀冠軍。
  - 1989年：全國武術比賽拳、槍、刀、對練冠軍。
  - 1991年：全國武術比賽槍術、對練冠軍。
  - 1994年：全國武術比賽精英賽槍術、對練冠軍。
  - 2018年：第34屆大眾電影百花獎 「最佳男主角」。
  - 2018年：第17屆中国電影[华表獎](../Page/华表獎.md "wikilink") 「优秀男演员」。

## 事件

2006年12月13日凌晨，吴京在北京市[朝阳区](../Page/朝阳区.md "wikilink")[工人体育馆附近因](../Page/工人体育馆.md "wikilink")[醉酒驾车被交警查处](../Page/醉酒驾车.md "wikilink")，后被行政拘留10天、暂扣驾驶证5个月、罚款1800元，扣12分\[9\]。

## 參考資料

## 外部連結

  -
  -
[J](../Page/category:吳姓.md "wikilink")
[category:北京人](../Page/category:北京人.md "wikilink")
[category:滿族演員](../Page/category:滿族演員.md "wikilink")
[分類:中國武術家](../Page/分類:中國武術家.md "wikilink")
[category:動作片演員](../Page/category:動作片演員.md "wikilink")

[Category:北京男演员](../Category/北京男演员.md "wikilink")
[Category:北京电影导演](../Category/北京电影导演.md "wikilink")
[Category:中国电影男演员](../Category/中国电影男演员.md "wikilink")
[Category:中国电视男演员](../Category/中国电视男演员.md "wikilink")
[Category:中国电影导演](../Category/中国电影导演.md "wikilink")
[Category:電影演員兼導演](../Category/電影演員兼導演.md "wikilink")
[Category:金牌大風藝人](../Category/金牌大風藝人.md "wikilink")
[Category:烏雅氏](../Category/烏雅氏.md "wikilink")

1.  [功夫明星吴京：习武也曾当逃课大王
    不想过把瘾就死](http://sports.sina.com.cn/o/2010-08-26/06375166938.shtml)
2.
3.
4.
5.  [《戰狼2》累收46億人幣
    首部國產片打入全球百強](https://news.mingpao.com/pns/dailynews/web_tc/article/20170815/s00016/1502735693060)
6.  [吳京《戰狼2》奪百花影帝](https://hk.on.cc/hk/bkn/cnt/entertainment/20181110/mobile/bkn-20181110210009755-1110_00862_001.html)
7.
8.
9.  [名人酒驾那些事儿](http://www.legalweekly.cn/article_show.jsp?f_article_id=17844)