**小鼠屬**（[学名](../Page/学名.md "wikilink")：**）也稱**鼠屬**、**鼷鼠属**，是[啮齿目](../Page/啮齿目.md "wikilink")[鼠科的一属](../Page/鼠科.md "wikilink")，当中最常見的是[小家鼠](../Page/小家鼠.md "wikilink")（*Mus
musculus*）。牠幾乎在所有的國家都能找到，例如在[生物學研究中作為](../Page/生物學.md "wikilink")[模式生物](../Page/模式生物.md "wikilink")(Model
organism)的實驗鼠。也是受人們喜愛的[寵物之一](../Page/寵物.md "wikilink")。

除本属外，一些小型鼠类如[仓鼠科的](../Page/仓鼠科.md "wikilink")[美國白足鼠](../Page/美國白足鼠.md "wikilink")（*Peromyscus
leucopus*) 和[鹿鼠](../Page/鹿鼠.md "wikilink")（*Peromyscus
maniculatus*）有時會棲息於房屋中，這些鼠通常和人們一起生活。

[PCWmice1.jpg](https://zh.wikipedia.org/wiki/File:PCWmice1.jpg "fig:PCWmice1.jpg")\]\]

## 物種

小鼠屬共有30種，列表如下\[1\]：

  - *Coelomys*

      - *Mus crociduroides*：[苏门答腊西部](../Page/苏门答腊.md "wikilink")

      - *Mus mayori*：[斯里兰卡](../Page/斯里兰卡.md "wikilink")

      - [锡金小鼠](../Page/锡金小鼠.md "wikilink") *Mus
        pahari*：[印度东北部至](../Page/印度.md "wikilink")[柬埔寨西南部及](../Page/柬埔寨.md "wikilink")[越南北部](../Page/越南.md "wikilink")

      - *Mus vulcani*：[爪哇西部](../Page/爪哇.md "wikilink")

  - *Mus*

      - *Mus
        booduga*：[巴基斯坦](../Page/巴基斯坦.md "wikilink")、印度、斯里兰卡、[孟加拉](../Page/孟加拉.md "wikilink")、[尼泊尔南部](../Page/尼泊尔.md "wikilink")、[缅甸中部](../Page/缅甸.md "wikilink")

      - [田鼷鼠](../Page/田鼷鼠.md "wikilink") *Mus
        caroli*：[琉球群岛](../Page/琉球群岛.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[华南至](../Page/华南.md "wikilink")[泰国](../Page/泰国.md "wikilink")，被引入至[马来西亚与](../Page/马来西亚.md "wikilink")[印尼西部](../Page/印尼.md "wikilink")

      - *Mus cervicolor*：印度北部至越南，被引入至苏门答腊及爪哇

      - *Mus cookii*：印度东北部及南部、尼泊尔至越南

      - *Mus cypriacus*：[塞浦路斯](../Page/塞浦路斯.md "wikilink")

      - *Mus famulus*：印度西南部

      - *Mus fragilicauda*：泰国及[寮国](../Page/寮国.md "wikilink")

      - *Mus
        macedonicus*：[巴尔干半岛至](../Page/巴尔干半岛.md "wikilink")[以色列及](../Page/以色列.md "wikilink")[伊朗](../Page/伊朗.md "wikilink")

      - [小家鼠](../Page/小家鼠.md "wikilink")（小鼠、家鼷鼠） *Mus musculus*：引入至世界各地

      - *Mus nitidulus*：缅甸中部

      - *Mus
        spicilegus*：[奥地利至](../Page/奥地利.md "wikilink")[乌克兰南部及](../Page/乌克兰.md "wikilink")[希腊](../Page/希腊.md "wikilink")

      - *Mus
        spretus*：[法国南部](../Page/法国.md "wikilink")，[伊比利亚半岛](../Page/伊比利亚半岛.md "wikilink")，[巴利阿里群岛](../Page/巴利阿里群岛.md "wikilink")，[摩洛哥至](../Page/摩洛哥.md "wikilink")[突尼斯](../Page/突尼斯.md "wikilink")

      - *Mus terricolor*：印度、尼泊尔、孟加拉、巴基斯坦；被引入至苏门答腊

  - *Nannomys*

      - *Mus
        baoulei*：[象牙海岸和](../Page/象牙海岸.md "wikilink")[几内亚](../Page/几内亚.md "wikilink")

      - *Mus
        bufo*：[乌干达](../Page/乌干达.md "wikilink")、[卢旺达](../Page/卢旺达.md "wikilink")、[布隆迪及](../Page/布隆迪.md "wikilink")[刚果民主共和国边境山区](../Page/刚果民主共和国.md "wikilink")

      - *Mus callewaerti*：[安哥拉及刚果民主共和国](../Page/安哥拉.md "wikilink")

      - *Mus goundae*：[中非共和国](../Page/中非共和国.md "wikilink")

      - *Mus
        haussa*：[塞内加尔至](../Page/塞内加尔.md "wikilink")[尼日利亚北部](../Page/尼日利亚.md "wikilink")

      - *Mus
        indutus*：安哥拉西南部至[津巴布韦和](../Page/津巴布韦.md "wikilink")[南非北部](../Page/南非.md "wikilink")

      - *Mus
        mahomet*：[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")、乌干达西南部和[肯尼亚西南部](../Page/肯尼亚.md "wikilink")

      - *Mus mattheyi*：[加纳](../Page/加纳.md "wikilink")

      - [南非小鼠](../Page/南非小鼠.md "wikilink") *Mus
        minutoides*：津巴布韦、[莫桑比克南部](../Page/莫桑比克.md "wikilink")、[南非](../Page/南非.md "wikilink")

      - *Mus
        musculoides*：[撒哈拉以南非洲](../Page/撒哈拉以南非洲.md "wikilink")，常见异名*M.
        minutoides*

      - *Mus neavei*：刚果民主共和国东部至南非东北部

      - *Mus orangiae*：南非

      - *Mus oubanguii*：中非共和国

      - *Mus setulosus*：塞内加尔至埃塞俄比亚和肯尼亚西部

      - *Mus
        setzeri*：[纳米比亚东北部](../Page/纳米比亚.md "wikilink")、[博茨瓦纳和](../Page/博茨瓦纳.md "wikilink")[赞比亚西部](../Page/赞比亚.md "wikilink")

      - *Mus
        sorella*：[喀麦隆东部到](../Page/喀麦隆.md "wikilink")[坦桑尼亚北部](../Page/坦桑尼亚.md "wikilink")

      - *Mus
        tenellus*：[苏丹至](../Page/苏丹.md "wikilink")[索马里南部和](../Page/索马里.md "wikilink")[坦桑尼亚中部](../Page/坦桑尼亚.md "wikilink")

      - *Mus triton* 埃塞俄比亚南部至安哥拉中部和[马拉维](../Page/马拉维.md "wikilink")

  - *Pyromys*

      - *Mus fernandoni*：斯里兰卡

      - [菲氏小鼠](../Page/菲氏小鼠.md "wikilink") *Mus phillipsi*：印度西南部

      - *Mus platythrix*：印度

      - *Mus saxicola*：巴基斯坦南部、尼泊尔南部及印度

      - *Mus shortridgei*：缅甸至柬埔寨西南部及越南西北部

## 参考文献

## 外部链接

[oc:Garri](../Page/oc:Garri.md "wikilink")

[\*](../Category/小鼠屬.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [中國生物多樣性信息中心動物學分部](http://vzd.brim.ac.cn/Mnamesrch.asp)