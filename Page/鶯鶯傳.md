[ChenLaolian.jpg](https://zh.wikipedia.org/wiki/File:ChenLaolian.jpg "fig:ChenLaolian.jpg")》\]\]
《**鶯鶯傳**》，又稱《**崔鶯鶯傳**》、《**会真记**》，[唐朝著名詩人](../Page/唐朝.md "wikilink")[元稹著](../Page/元稹.md "wikilink")，是《[唐人傳奇](../Page/唐人傳奇.md "wikilink")》中最著名的篇章之一。

## 本事

劇情在於張生对崔鶯鶯一見鍾情，鶯鶯礙於[禮教](../Page/禮教.md "wikilink")，拒絕了張生的求愛，而侍女[紅娘在其身边劝解](../Page/紅娘.md "wikilink")，兩人成功相戀后，如胶似漆，後來張生赴京赶考，張生说自己不能压住她的妖，怕像周幽王一般引致祸端，便与其断绝联系。鶯鶯只能自怨自艾。
各自婚配后，张生回京，对崔莺莺情愫又起，以兄长的身份让崔莺莺丈夫催她相见，崔莺莺劝其“怜取眼前人。”

鶯鶯傳是[唐人傳奇中最著名的一篇](../Page/唐人傳奇.md "wikilink")，故事广泛流传，北宋以降，士大夫“无不举此以为美谈，至于倡优女子，皆能调说大略”\[1\]。元代[王實甫據此改編成雜劇](../Page/王實甫.md "wikilink")《[西廂記](../Page/西廂記.md "wikilink")》。

## 禁书

[王实甫根据](../Page/王实甫.md "wikilink")《会真记》所写的《[西厢记](../Page/西厢记.md "wikilink")》曾被明清朝廷列为禁书。小说《[红楼梦](../Page/红楼梦.md "wikilink")》中有[贾宝玉与](../Page/贾宝玉.md "wikilink")[林黛玉在](../Page/林黛玉.md "wikilink")[大观园偷看禁书](../Page/大观园.md "wikilink")《会真》，即是《西厢记》。

## 故事主角的原型考證

[宋代](../Page/宋代.md "wikilink")[蘇軾認為張生是元稹的好友](../Page/蘇軾.md "wikilink")[張籍](../Page/張籍.md "wikilink")。[王銍在](../Page/王銍.md "wikilink")《〈傳奇〉辯證》考證張生為元稹本人，並認為鶯鶯為元稹姨母鄭氏與永年縣尉崔鵬之女崔氏，即元稹的表妹。

[鲁迅在](../Page/鲁迅.md "wikilink")《[中国小说史略](../Page/中国小说史略.md "wikilink")》中说：“《莺莺传》者，……元稹以张生自寓，述其亲历之境。”

近人[陳寅恪從元稹詩集一首](../Page/陳寅恪.md "wikilink")《曹十九舞綠鈿》，假定“曹十九”是“曹九九”的讹误，又說“九九”二字古音与莺鸟鸣声相近，認為崔鶯鶯應是名叫“曹九九”的“酒家胡”\[2\]，“此女姓曹名九九，殆亦出於中亞種族”\[3\]，因“中亚胡人善于酿酒”，得出曹九九是“酒家胡”的结论。但亦有持不同觀點，[卞孝萱在](../Page/卞孝萱.md "wikilink")《元稹年谱》中指出，“‘工于投机取巧之才人’元稹，怎么忽然蠢起来，把一个社会地位低下的‘酒家胡’，说成自己的姨妹？”

## 注釋

## 扩展阅读

英文:

  - Luo, Manling ([圣路易斯华盛顿大学](../Page/圣路易斯华盛顿大学.md "wikilink")). "[THE
    SEDUCTION OF AUTHENTICITY: “THE STORY OF
    YINGYING”](http://474miranairresearchpaper.wmwikis.net/file/view/yingyingseductionofauthenticity.pdf)."
    ([Archive](http://www.webcitation.org/6PmwH5VHq)). *[Nan
    Nü](../Page/Nan_Nü.md "wikilink")* 7.1.
    [Brill](../Page/Brill.md "wikilink"), Leiden, 2005. p. 40-70.
  - Yu, Pauline ([余宝琳](../Page/余宝琳.md "wikilink")). "[The Story of
    Yingying](http://isites.harvard.edu/fs/docs/icb.topic1220028.files/Paulin%20Yu_%20Comments_The%20Story%20of%20Yingying.pdf)"
    ([Archive](http://www.webcitation.org/6Pmx3QsLa)). In: Yu, Pauline,
    Peter Bol, Stephen Owen, and Willard Peterson (editors). *Ways with
    Words: Writing about Reading Texts from Early China* (Volume 24 of
    Studies on China). [University of California
    Press](../Page/University_of_California_Press.md "wikilink") (),
    2000. ISBN 0520224663, 9780520224667. p. 182-185.

## 參見

  - [唐人傳奇](../Page/唐人傳奇.md "wikilink")
  - [王實甫的](../Page/王實甫.md "wikilink")《[西廂記](../Page/西廂記.md "wikilink")》
  - [元稹](../Page/元稹.md "wikilink")

[Category:唐人傳奇](../Category/唐人傳奇.md "wikilink")

1.  宋代[赵令畤](../Page/赵令畤.md "wikilink")《〈蝶恋花〉鼓子词序》
2.  陳寅恪：《读莺莺传》
3.  陳寅恪：《元白诗笺证稿》