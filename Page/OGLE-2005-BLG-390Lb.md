**OGLE-2005-BLG-390Lb**是一顆[太陽系外行星](../Page/太陽系外行星.md "wikilink")，繞著[恆星](../Page/恆星.md "wikilink")[OGLE-2005-BLG-390L公轉](../Page/OGLE-2005-BLG-390L.md "wikilink")。它位於[天蠍座](../Page/天蠍座.md "wikilink")，距離[地球](../Page/地球.md "wikilink")21,500
±
3,300[光年](../Page/光年.md "wikilink")，其位置接近[銀河系的中心](../Page/銀河系.md "wikilink")。截至2006年1月，這顆行星是眾多系外行星當中，與地球最為相似的一個。

這顆行星是由三個小組共同發現的，分別為PLANET/RoboNet、[OGLE和MOA](../Page/光學重力透鏡實驗.md "wikilink")，該發現於2006年1月25日公佈。科學家發現OGLE-2005-BLG-390Lb並未擁有[行星適居性](../Page/行星適居性.md "wikilink")\[1\]。

## 物理特性

這顆系外行星的日距介乎2.0到4.1[天文單位之間](../Page/天文單位.md "wikilink")，如果把它放到[太陽系內](../Page/太陽系.md "wikilink")，它的軌道會在[火星及](../Page/火星.md "wikilink")[木星的軌道之間](../Page/木星.md "wikilink")。由於人們尚未得到它的軌道數據，因此其日距的改變並不一定代表它真正的離心率，而會是量度及計算時所出現的誤差。在這顆行星被發現以前，沒有一顆已知的小型系外行星，其日距會超過0.15
AU。經初步計算，它的公轉週期大約為10個地球年\[2\]。

根據[J2000所記載的天體座標](../Page/曆元.md "wikilink")，恆星OGLE-2005-BLG-390L距離地球
6,600 ± 1,000
[pc](../Page/秒差距.md "wikilink")，[赤經](../Page/赤經.md "wikilink")17:54:19.2，[赤緯](../Page/赤緯.md "wikilink")−30°22′38″。

人們相信，該行星的恆星有很大可能會是一顆[紅矮星](../Page/紅矮星.md "wikilink")（95%），也有可能是[白矮星](../Page/白矮星.md "wikilink")（4%），而[中子星或](../Page/中子星.md "wikilink")[黑洞的機會則極微](../Page/黑洞.md "wikilink")，可能性加起來還不到1%。不過無論如何，該恆星釋出的能量一定會比[太陽的少](../Page/太陽.md "wikilink")。

據估計，這顆行星的質量約為地球的五倍（5.5 +5.5/-2.7
*[M<sub>E</sub>](../Page/地球.md "wikilink")*），一些天文學家推斷，它擁有岩石核心，與地球相似，並擁有一層稀薄的[大氣](../Page/大氣.md "wikilink")。又由於恆星的溫度比太陽相對較低，因此行星的表面溫度約為53
[K](../Page/热力学温标.md "wikilink")（[攝氏零下](../Page/攝氏.md "wikilink")220度、[華氏零下](../Page/華氏.md "wikilink")364度）左右。如果它是由岩石構成的話，在這樣的溫度環境下，其表面將佈滿已凝結的揮發性物質。在地球上諸如[水](../Page/水.md "wikilink")、[氨](../Page/氨.md "wikilink")、[甲烷](../Page/甲烷.md "wikilink")、[氮氣等液體和氣體](../Page/氮.md "wikilink")，只要搬到這顆行星上，它們將會被凝固。又如果它是一顆氣體行星，它將會與[天王星相似](../Page/天王星.md "wikilink")，但其大小則會比天王星小得多，類似一顆「迷你天王星」\[3\]。

這顆行星的引人注目的地方不只是它的大小及其物質組成，還有它較遠的公轉軌道。雖然以往也曾發現小型的系外行星，但這些行星的日距相對較近。以
[格利泽876d](../Page/格利泽876.md "wikilink")
為例，它繞日公轉一周只需三個地球天。因此，OGLE-2005-BLG-390Lb這顆行星無論在大小及軌道上均切合太陽系的規律，也是系外行星中前所未見的。

## 行星的發現

[Gravitational_micro_rev.jpg](https://zh.wikipedia.org/wiki/File:Gravitational_micro_rev.jpg "fig:Gravitational_micro_rev.jpg")
這顆系外行星最先於2005年8月10日，由位於[智利的](../Page/智利.md "wikilink")[歐洲南方天文台](../Page/歐洲南方天文台.md "wikilink")，成員國[丹麥以](../Page/丹麥.md "wikilink")1.54m天文望遠鏡偵測得到，這副望遠鏡也是PLANET/RoboNet小組以[重力透鏡方式尋找行星計劃的一部分](../Page/重力透鏡.md "wikilink")。而該行星隨後的觀測數據則由澳洲西部的珀斯天文台，以一台0.6m天文望遠鏡進行。

每當遙遠恆星的光線經過其他天體的重力場時，其光線的方向會被重力所扭曲，同時也把光線聚焦，地球上的觀測者將看到恆星的樣子給重力放大，儼如一副透鏡放在中間，稱為[重力透鏡效應](../Page/重力透鏡.md "wikilink")。而當行星在恆星的表面「凌日」時，也會發生「[重力透鏡](../Page/重力透鏡.md "wikilink")」現像，行星外圍的光度會稍為增大一點。

在一般的凌日觀測中，當行星凌日發生時，恆星整體的光度會略微出現下降，從而得知行星的存在。但這種尋找行星方法並不能應用在距離數萬光年以上，或體積與地球類似的行星上，因此這些行星便需使用重力透鏡來觀測，當透鏡現象發生時，來自恆星不同位置的光度會隨時間而變化。

在發現行星的天文研究小組之中，PLANET/Robonet負責深入調查及分析[OGLE和MOA小組所報告的恆星微透鏡現象](../Page/光學重力透鏡實驗.md "wikilink")，使這顆隱藏的行星得以被發現。

PLANET小組花了連續兩星期，去深入觀測OGLE-2005-BLG-390L恆星的微透鏡現象，當微透鏡出現時，其「行星」外圈的光度會增大15%，每次透鏡持續時間近12小時，透過光度變化及持續時間，天文學家得以估計行星的質量、日距等數據。

## 請參閱

  - [重力透鏡](../Page/引力透镜效应.md "wikilink")
  - [OGLE小組](../Page/光學重力透鏡實驗.md "wikilink")
  - [OGLE-2007-BLG-349(AB)b](../Page/OGLE-2007-BLG-349\(AB\)b.md "wikilink")

## 參考資料

## 相關網站

  - [新發現目前距離最遙遠的日外行星](https://web.archive.org/web/20060822044904/http://www.asiaa.sinica.edu.tw/outreach/news/old/2003/03020606.htm)
  - [太陽系外發現最小類地行星
    是地球質量5倍多（人民網）](https://web.archive.org/web/20071115234756/http://world.people.com.cn/BIG5/4067091.html)
  - [法國科學家探測到太陽系外品質最小的類地行星（新華網）](http://news.xinhuanet.com/st/2006-01/26/content_4101422.htm)
  - [最小類地行星被發現
    距地球2.8萬光年（大紀元）](http://www.epochtimes.com/b5/6/1/26/n1203541.htm)
  - [Space.com的有關文章](http://www.space.com/scienceastronomy/060125_smallest_planet.html)
  - [PLANET - Probing Lensing Anomalies NETwork](http://planet.iap.fr)
  - [RoboNet](http://www.astro.livjm.ac.uk)
  - [OGLE Collaboration](http://ogle.astrouw.edu.pl)
  - [MOA Collaboration](http://www.phys.canterbury.ac.nz/moa)
  - [ESO新聞稿](http://www.eso.org/outreach/press-rel/pr-2006/pr-03-06.html)
  - [BBC新聞](http://news.bbc.co.uk/2/hi/science/nature/4647142.stm)
  - [Notes for Planet OGLE-05-390L b (Extrasolar Planets
    Encyclopaedia)](https://web.archive.org/web/20061117154916/http://vo.obspm.fr/exoplanetes/encyclo/planet.php?p1=OGLE-05-390L&p2=b)

[Category:重力微透鏡法發現的系外行星](../Category/重力微透鏡法發現的系外行星.md "wikilink")
[Category:2006年發現的系外行星](../Category/2006年發現的系外行星.md "wikilink")
[Category:重力透鏡](../Category/重力透鏡.md "wikilink")

1.
2.
3.  .