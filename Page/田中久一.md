[Tanaka_Hisakazu.jpg](https://zh.wikipedia.org/wiki/File:Tanaka_Hisakazu.jpg "fig:Tanaka_Hisakazu.jpg")
[Hisakazu_Tanaka_2.jpg](https://zh.wikipedia.org/wiki/File:Hisakazu_Tanaka_2.jpg "fig:Hisakazu_Tanaka_2.jpg")

**田中久一**，本姓**小金井**，為[大日本帝國陸軍的軍人](../Page/大日本帝國陸軍.md "wikilink")，官至[中將](../Page/中將.md "wikilink")，曾任日本軍[第23軍司令官](../Page/第23軍_\(日本陸軍\).md "wikilink")，兼任[香港佔領地總督](../Page/香港總督.md "wikilink")。在[太平洋戰爭期間負責指揮日本軍](../Page/太平洋戰爭.md "wikilink")[第21師團進攻](../Page/第21師團.md "wikilink")[法屬印度支那](../Page/法屬印度支那.md "wikilink")（史稱[佛印作戰](../Page/日軍入侵法屬印度支那.md "wikilink")）。

1945年[日本投降後](../Page/日本投降.md "wikilink")，被指控為[乙級戰犯](../Page/乙級戰犯.md "wikilink")。1946年3月以「[法外處決盟軍](../Page/就地正法.md "wikilink")[戰俘](../Page/戰俘.md "wikilink")」罪名，遭[美軍](../Page/美軍.md "wikilink")[上海](../Page/上海.md "wikilink")[軍事法庭](../Page/軍事法庭.md "wikilink")\[1\]判[繯首死刑](../Page/繯首.md "wikilink")，後被轉交給[中國重新審判](../Page/中國.md "wikilink")\[2\]，判處[死刑](../Page/死刑.md "wikilink")，1947年被[槍決](../Page/槍決.md "wikilink")。

## 生平

田中久一是[兵庫縣人](../Page/兵庫縣.md "wikilink")，本姓小金井，後來過繼成為[田中八作的養子](../Page/田中八作.md "wikilink")。1910年5月，畢業於[日本陸軍士官學校第](../Page/日本陸軍士官學校.md "wikilink")22期，同年12月加入陸軍成為士官，官階少尉，任[步兵第37聯隊幹部](../Page/步兵第37聯隊.md "wikilink")。之後就讀[陸軍戶山學校](../Page/陸軍戶山學校.md "wikilink")，並就任學校教官。任[教育總監部幹部](../Page/教育總監.md "wikilink")。1918年11月，從[日本陸軍大學校](../Page/日本陸軍大學校.md "wikilink")（30期）畢業。1933年至1935年任官中佐，擔任[陸軍步兵學校教官](../Page/陸軍步兵學校.md "wikilink")。1934年進級大佐。1937年12月升任陸軍[少將](../Page/少將.md "wikilink")。

1938年2月被任為任[台灣軍](../Page/台灣軍.md "wikilink")[參謀長](../Page/參謀長.md "wikilink")；同年9月，任新建之[第21軍參謀長](../Page/第21軍_\(日本陸軍\).md "wikilink")，戶山學校校長。10月參與指揮[第21軍在](../Page/第21軍_\(日本陸軍\).md "wikilink")[廣東](../Page/廣東.md "wikilink")[大亞灣登陸](../Page/大亞灣.md "wikilink")，然後攻佔[廣州](../Page/廣州.md "wikilink")。1939年8月，日本駐華南軍隊改編為[南支那方面軍](../Page/南支那方面軍.md "wikilink")，田中升任中將參謀。1940年年中為配合進攻香港，南支那方面軍撤消，改成[第23軍](../Page/第23軍_\(日本陸軍\).md "wikilink")。[第23軍於](../Page/第23軍_\(日本陸軍\).md "wikilink")1941年攻佔香港。1943年3月，田中任[第23軍司令](../Page/第23軍_\(日本陸軍\).md "wikilink")，主管華南軍事，駐地為廣州。1944年參與指揮進攻廣西、湖南的[湘桂作戰](../Page/豫湘桂會戰.md "wikilink")。同年12月，日軍改組香港佔領地政府，[磯谷廉介調任台灣](../Page/磯谷廉介.md "wikilink")，香港總督一職由1945年2月起改由駐廣州之田中兼任，至1945年8月日軍投降為止。

1945年8月，[裕仁天皇宣佈](../Page/裕仁天皇.md "wikilink")[日本無條件投降](../Page/日本.md "wikilink")。田中久一先後在[廣州向](../Page/廣州.md "wikilink")[張發奎的](../Page/張發奎.md "wikilink")[第2方面軍](../Page/第2方面軍_\(日本陸軍\).md "wikilink")，和在[汕頭向](../Page/汕頭.md "wikilink")[余漢謀的第](../Page/余漢謀.md "wikilink")7戰區簽署降書。

## 軍事審判

投降時田中被當做戰俘，被認定為[乙級戰犯](../Page/乙級戰犯.md "wikilink")，1945年12月送到美軍的[上海](../Page/上海.md "wikilink")[軍事法庭](../Page/軍事法庭.md "wikilink")被當成戰犯審理。1946年3月田中被追究在擔任[香港佔領地總督期間涉嫌法外處決盟軍戰俘的責任](../Page/香港總督.md "wikilink")\[3\]，被判處死刑，並要執行[絞刑](../Page/絞刑.md "wikilink")，但是後來卻被轉交給中國[南京](../Page/南京.md "wikilink")[軍事法庭審判](../Page/軍事法庭.md "wikilink")，同年5月18日廣州行營[軍事法庭開始審理](../Page/軍事法庭.md "wikilink")，同年10月9日判處死刑，1947年3月27日於[廣州市](../Page/廣州市.md "wikilink")[流花橋刑場遭由中華民國憲兵石光中執行](../Page/流花橋.md "wikilink")[槍決死亡](../Page/槍決.md "wikilink")，當時石光中朝著背向田中開了兩槍，再將倒地的田中翻身朝著正中心補了一槍。\[4\]

田中久一被枪决后，遗体被广州当地三元里村的4个埋尸工拖到行刑附近的一处低凹洼地（天热时水牛浸水纳凉的小水塘）处，随手铲土覆盖掩埋。1972年田中久一的遗骸运回日本。

## 簡表

  - 1907年（[明治](../Page/明治.md "wikilink")40年）12月 -
    [陸軍士官學校候補生](../Page/日本陸軍士官學校.md "wikilink")。
  - 1910年（明治43年）5月28日 - 陸軍士官學校畢業（22期）。
      - 12月26日 - 晉升少尉。[步兵第37聯隊付](../Page/步兵第37聯隊.md "wikilink")。
  - 1913年（[大正](../Page/大正.md "wikilink")2年）12月 - 晉升中尉。
  - 1914年（大正3年）1月 - 陸軍戶山學校教官。
      - 7月 - 教育總監部副。
  - 1915年（大正4年）11月29日 - [陸軍大學校入學](../Page/日本陸軍大學校.md "wikilink")。
  - 1918年（大正7年）11月29日 - 陸軍大學校畢業（30期）。
  - 1919年（大正8年）4月 - 參謀本部副勤務。
  - 1920年（大正9年）4月 - 晉升大尉。參謀本部部員。
  - 1923年（大正12年）9月 - 出差[美國](../Page/美國.md "wikilink")。
  - 1924年（大正13年）12月 - 參謀本部部員。
  - 1925年（大正14年）12月 - 晉升少佐。[步兵第1聯隊付](../Page/步兵第1聯隊.md "wikilink")。
  - 1927年（[昭和](../Page/昭和.md "wikilink")2年）12月 - 陸軍大學校教官。
  - 1930年（昭和5年）8月 - 晉升中佐。
  - 1932年（昭和7年）6月9日 - [陸軍省軍務局課員](../Page/陸軍省.md "wikilink")。
  - 1933年（昭和8年）8月1日 - 陸軍步兵學校教官。
  - 1934年（昭和9年）3月5日 - 晉升大佐。
  - 1935年（昭和10年）3月15日 - [近衛步兵第4聯隊長](../Page/近衛步兵第4聯隊.md "wikilink")。
  - 1937年（昭和12年）3月1日 - 陸軍大學校教官。
      - 12月28日 - 晉升少將。
  - 1938年（昭和13年）2月19日 - [台灣軍參謀長](../Page/台灣軍.md "wikilink")。
      - 9月8日 - [第21軍參謀長](../Page/第21軍_\(日本陸軍\).md "wikilink")。
  - 1939年（昭和14年）8月1日 - 陸軍戶山學校校長。
  - 1940年（昭和15年）8月1日 - 晉升中將。
      - 9月28日 - 第21師團長。
  - 1943年（昭和18年）3月1日 - [第23軍司令官](../Page/第23軍_\(日本陸軍\).md "wikilink")。
  - 1944年（昭和19年）12月16日 - 兼[香港佔領時期總督](../Page/香港總督.md "wikilink")。
  - 1945年（昭和20年）12月 - 美軍[上海軍事法庭審理其戰爭犯行](../Page/上海.md "wikilink")。
  - 1946年（昭和21年）3月 -
    美軍的[上海軍事法庭判決](../Page/上海.md "wikilink")[死刑](../Page/死刑.md "wikilink")，並要執行[絞刑](../Page/絞刑.md "wikilink")。
      - 5月18日 - 轉送到中國[廣州行營軍事法庭審理其戰爭犯行](../Page/廣州.md "wikilink")。
      - 10月9日 -
        中國廣州行營軍事法庭判決[死刑](../Page/死刑.md "wikilink")，並要執行[槍決](../Page/槍決.md "wikilink")。
  - 1947年（昭和22年）3月27日 - 執行死刑，遭槍決死亡。

## 辭世遺言

  -
    *永年の　終りの歩み　さわやかに*（再一次步上永恆的終結）
    *恵みの途を　辿りゆく吾*（於恩典的旅途中、隨著我想要哭出來）

## 親族

  - 長男 田中義信（[陸軍少佐](../Page/少佐.md "wikilink")，遭殺害）

## 參見

  -
  -
  - [南方作戰](../Page/南方作戰.md "wikilink")

  - [香港日佔時期](../Page/香港日佔時期.md "wikilink")

## 參考資料

## 參考文獻

  - [秦郁彦編](../Page/秦郁彦.md "wikilink")『日本陸海軍総合事典』第2版、[東京大学出版会](../Page/東京大学出版会.md "wikilink")、2005年。
  - 福川秀樹『日本陸軍将官辞典』芙蓉書房出版、2001年。
  - 外山操編『陸海軍将官人事総覧 陸軍篇』芙蓉書房出版、1981年。

## 外部連結

  - [田中久一經歷](http://www24049u.sakura.ne.jp/db/%E7%94%B0%E4%B8%AD%E4%B9%85%E4%B8%80)

|width=25% align=center|**前任：**
[磯谷廉介](../Page/磯谷廉介.md "wikilink")
|width=50% align=center|**[佔領地總督](../Page/香港總督.md "wikilink")**
1945年2月1日－1945年8月15日 |width=25% align=center|**繼任：**
[詹遜](../Page/詹遜.md "wikilink")（署任）
<small>[總督](../Page/香港總督.md "wikilink")

[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")
[Category:日本陸軍中將](../Category/日本陸軍中將.md "wikilink")
[Category:被處決的日本人](../Category/被處決的日本人.md "wikilink")
[Category:被中華民國處決者](../Category/被中華民國處決者.md "wikilink")
[Category:台灣軍參謀長](../Category/台灣軍參謀長.md "wikilink")
[Category:香港日佔時期總督](../Category/香港日佔時期總督.md "wikilink")
[Category:乙級丙級戰犯](../Category/乙級丙級戰犯.md "wikilink")

1.
2.
3.
4.  <http://udn.com/news/story/8101/944689-老兵悲歌／石光中槍決日中將-終身難忘>