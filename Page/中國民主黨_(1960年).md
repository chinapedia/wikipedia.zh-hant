**中國民主黨**是[中華民國史上一個胎死腹中的](../Page/中華民國歷史.md "wikilink")[反對黨](../Page/反對黨.md "wikilink")，於1960年5月4日成立至1960年9月4日解散。

## 過程

1949年，[雷震與](../Page/雷震.md "wikilink")[胡適](../Page/胡適.md "wikilink")、[王世傑](../Page/王世傑.md "wikilink")、[杭立武等籌備在](../Page/杭立武.md "wikilink")[上海建立一份名為](../Page/上海.md "wikilink")《[自由中國](../Page/自由中國.md "wikilink")》的雜誌。該雜誌早期與[蔣中正和](../Page/蔣中正.md "wikilink")[中國國民黨關係密切](../Page/中國國民黨.md "wikilink")，但後期刊登大量批評政府的言論，碰到[蔣中正的政治禁忌](../Page/蔣中正.md "wikilink")。

## 籌組政黨

1960年5月4日，雷震發表了《我們為什麼迫切需要一個強有力的反對黨》，鼓吹成立反對黨參與選舉以制衡執政黨。5月18日，非國民黨籍人士舉行選舉改進檢討會，主張成立新黨，要求公正選舉，實現真正的民主。決議即日起組織「地方選舉改進座談會」，隨即籌備組織反對黨。雷震擔任地方選舉改進座談會召集委員，與李萬居、高玉樹共同擔任發言人。7至8月間舉行四次分區座談會，確定黨名為「中國民主黨」。參與新黨籌組的人員包括了當年省議會「五龍一鳳」的[李萬居](../Page/李萬居.md "wikilink")、[郭雨新](../Page/郭雨新.md "wikilink")、[吳三連](../Page/吳三連.md "wikilink")、[郭國基](../Page/郭國基.md "wikilink")、[李源棧](../Page/李源棧.md "wikilink")、[許世賢](../Page/許世賢.md "wikilink")，以及[高玉樹](../Page/高玉樹.md "wikilink")、[楊金虎](../Page/楊金虎.md "wikilink")、[齊世英](../Page/齊世英.md "wikilink")、[夏濤聲等](../Page/夏濤聲.md "wikilink")。

## 結果

1960年9月4日，雷震、[劉子英](../Page/劉子英.md "wikilink")、[馬之驌](../Page/馬之驌.md "wikilink")、[傅正被逮捕](../Page/傅正.md "wikilink")，並被軍事法庭以「包庇匪諜、煽動叛亂」的罪名判處十年徒刑。直到1970年，雷震才得以出獄，但仍被特務密切監視。到1990年代，才得以平反。

[Category:台灣已解散政黨](../Category/台灣已解散政黨.md "wikilink")
[Category:台灣民主運動](../Category/台灣民主運動.md "wikilink")
[Category:自由主義政黨](../Category/自由主義政黨.md "wikilink")
[Category:1960年建立的組織](../Category/1960年建立的組織.md "wikilink")