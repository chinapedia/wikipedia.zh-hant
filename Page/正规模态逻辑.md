在[逻辑中](../Page/逻辑.md "wikilink")，**正规[模态逻辑](../Page/模态逻辑.md "wikilink")**是模态公式的集合
\(L\)，\(L\) 包含

  - 所有命题[重言式](../Page/重言式.md "wikilink")，
  - 所有满足 Kripke 模式的实例: \(\Box(A\to B)\to(\Box A\to\Box B)\)，

并且 \(L\) 闭合于

  - 分拆规则（[肯定前件](../Page/肯定前件.md "wikilink")）:
    \(A\rightarrow B, A\vdash B\)，
  - 必然性规则: 从 \(\vdash A\) 推出 \(\vdash\Box A\)。

最小化的满足上述条件的逻辑叫做 **K**。大多数如今常用的模态逻辑（指有哲学动机的）如C. I.
刘易斯的S4与[S5皆为在](../Page/S5_\(模态逻辑\).md "wikilink")**K**基础之上的扩展。然而也有一部分如[道义逻辑与](../Page/道义逻辑.md "wikilink")[认识逻辑是非正规的](../Page/认识逻辑.md "wikilink")，因为它们舍弃了Kripke模式。

## 常见的模态逻辑

<onlyinclude> 下表给出了一些常见的模态逻辑系统。</onlyinclude>表中的标记可参见 [Kripke 语义 §
常见模态公理模式](../Page/关系语义#模态逻辑的语义.md "wikilink")。
<onlyinclude>某些系统的框架条件要求被简化了，它们在给定的框架类中完备，但是可能对应一个更大的框架类。

| 名称                                      | 公理               | 框架条件                                                                            |
| --------------------------------------- | ---------------- | ------------------------------------------------------------------------------- |
| K                                       | —                | 所有框架                                                                            |
| T                                       | T                | 自反                                                                              |
| K4                                      | 4                | 传递                                                                              |
| S4                                      | T, 4             | [预序](../Page/预序.md "wikilink")                                                  |
| [S5](../Page/S5_\(模态逻辑\).md "wikilink") | T, 5 或 D, B, 4   | [等价关系](../Page/等价关系.md "wikilink")                                              |
| S4.3                                    | T, 4, H          | 完全预序                                                                            |
| S4.1                                    | T, 4, M          | 预序, \(\forall w\,\exists u\,(w\,R\,u\land\forall v\,(u\,R\,v\Rightarrow u=v))\) |
| S4.2                                    | T, 4, G          | [有向预序](../Page/有向集合.md "wikilink")                                              |
| GL                                      | GL or 4, GL      | 有穷[严格偏序](../Page/偏序关系#非严格偏序，自反偏序.md "wikilink")                                 |
| Grz, S4Grz                              | Grz or T, 4, Grz | 有穷[偏序](../Page/偏序.md "wikilink")                                                |
| D                                       | D                | serial                                                                          |
| D45                                     | D, 4, 5          | 传递，全序且欧拉                                                                        |

</onlyinclude>

## 参见

  - Alexander Chagrov and Michael Zakharyaschev, *Modal Logic*, vol. 35
    of Oxford Logic Guides, Oxford University Press, 1997.

[Category:模态逻辑](../Category/模态逻辑.md "wikilink")