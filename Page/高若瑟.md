**高若瑟**樞機（，）是[葡萄牙籍的](../Page/葡萄牙.md "wikilink")[天主教](../Page/天主教.md "wikilink")[司鐸級樞機](../Page/樞機.md "wikilink")。更在1972年8月14日至1976年11月29日是為在世最年長樞機，並曾擔任[西印度宗主教及](../Page/西印度宗主教列表.md "wikilink")[果亞暨達曼總教區總主教](../Page/天主教果亞暨達曼總教區.md "wikilink")。

## 生平

### 早年生活

高若瑟出生於1880年3月15日，[葡萄牙王國](../Page/葡萄牙王國.md "wikilink")[亞速群島的城鎮](../Page/亞速群島.md "wikilink")[馬達萊娜](../Page/馬達萊娜_\(亞速爾群島\).md "wikilink")。年小時隨[鮑理諾主教到葡萄牙](../Page/鮑理諾.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")[澳門](../Page/澳門.md "wikilink")，後就讀於[聖若瑟修院](../Page/聖若瑟修院.md "wikilink")。於1903年6月4日晉鐸後，出任鮑理諾主教秘書。1906年至1913年被任命為澳門暨[東帝汶](../Page/東帝汶.md "wikilink")[副主教](../Page/副主教.md "wikilink")，於是他開始東帝汶傳教工作。

### 晉牧

1920年12月16日晉牧，並繼任成為第十七任[澳門教區](../Page/天主教澳門教區.md "wikilink")[主教](../Page/天主教澳門教區主教列表.md "wikilink")。他任內重建[聖羅撒女子中學及](../Page/聖羅撒女子中學.md "wikilink")[聖嘉辣小堂](../Page/聖嘉辣小堂.md "wikilink")。又興建主教寓所及小堂於[西望洋山](../Page/西望洋山.md "wikilink")，並聘請[耶穌會會士重掌修院教職](../Page/耶穌會.md "wikilink")，創建[培貞學校](../Page/培貞學校.md "wikilink")，[聖若瑟中學](../Page/聖若瑟教區中學.md "wikilink")、[公教學校](../Page/公教學校.md "wikilink")、[望德學校](../Page/聖若瑟教區中學第一校.md "wikilink")，和重修[澳門主教公署及](../Page/澳門主教公署.md "wikilink")[聖母聖誕主教座堂](../Page/澳門主教座堂.md "wikilink")。

1940年12月11日，出任[果亞暨達曼總教區總主教和](../Page/天主教果亞暨達曼總教區.md "wikilink")[西印度宗主教](../Page/西印度宗主教列表.md "wikilink")。1953年12月16日，他被任命為[羅馬聖教會副總司庫和領銜奧德修斯總教區總主教](../Page/羅馬聖教會總司庫.md "wikilink")（Titular
Archbishop of Odessus），但同時可以保留“宗主教”的個人稱號。

### 冊封樞機

[José_da_Costa_Nunes_visitou_Timor_1937.jpg](https://zh.wikipedia.org/wiki/File:José_da_Costa_Nunes_visitou_Timor_1937.jpg "fig:José_da_Costa_Nunes_visitou_Timor_1937.jpg")
1962年3月19日，被時任[教宗](../Page/教宗.md "wikilink")[若望二十三世擢昇為](../Page/若望二十三世.md "wikilink")[司鐸級樞機](../Page/樞機.md "wikilink")。高若瑟還參加了1962年至1965年的[梵蒂岡第二次會議](../Page/梵蒂岡第二次會議.md "wikilink")，和及後的[1963年教宗選舉秘密會議](../Page/1963年教宗選舉秘密會議.md "wikilink")，堂時選出的教宗是[保祿六世](../Page/保祿六世.md "wikilink")。於1965年11月10日，他以教宗使節代表身份出席天主教傳入澳門的四百周年慶典。

他於1976年，在[意大利](../Page/意大利.md "wikilink")[羅馬逝世](../Page/羅馬.md "wikilink")，享年96歲。他的遺體後來被轉移到[戰神廣場里斯本聖安多尼堂安葬](../Page/戰神廣場里斯本聖安多尼堂.md "wikilink")。

## 牧徽

<File:Coat> of arms of José da Costa Nunes.svg|高若瑟樞機牧徽

## 参考文献

<references/>

[Category:澳門天主教徒](../Category/澳門天主教徒.md "wikilink")
[Category:西印度宗主教](../Category/西印度宗主教.md "wikilink")
[Category:葡萄牙枢机](../Category/葡萄牙枢机.md "wikilink")