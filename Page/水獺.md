**水獺**是一类水棲、[肉食性的](../Page/肉食性.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")，在動物[分類學中屬於](../Page/分類學.md "wikilink")[亞科級別](../Page/亞科.md "wikilink")，稱為**水獺亞科**（*Lutrinae*），現存七個[屬及十三個](../Page/屬_\(生物\).md "wikilink")[物種](../Page/物種.md "wikilink")。

## 分布

水獺亞科動物分佈於全球各地。[紅樹林的水獺生長在](../Page/紅樹林.md "wikilink")[歐亞大陸](../Page/歐亞大陸.md "wikilink")，[美洲獺屬的水獺則生長在](../Page/美洲獺屬.md "wikilink")[美洲大陸](../Page/美洲大陸.md "wikilink")，而[海獺則常見於](../Page/海獺.md "wikilink")[北太平洋的海岸](../Page/北太平洋.md "wikilink")。

在[中國](../Page/中國.md "wikilink")，水獺分佈在黑龍江、吉林、遼寧、內蒙古東北部、河南、山西、陝西等省。

## 特徵

水獺身长70－75厘米，尾巴細長；头扁耳小脚短，趾间有蹼；有十層非常短而密（密度達到1000根/平方毫米）的細軟絨毛，以保持身體的乾燥和溫暖；背部深褐色有光泽，腹部颜色较淡。

所有的水獺都具有細長、流綫型的身體結構，身體優美靈活，四肢較短。大多數都具有鋒利的爪。

## 生活習性

大多數水獺都以魚類為食，此外也吃[蛙類](../Page/蛙.md "wikilink")、淡水[蝦和](../Page/蝦.md "wikilink")[蟹類](../Page/蟹.md "wikilink")。

性格凶猛，在遭到猎犬圍捕时，敢於向身形較大的進攻者發起反抗，常有咬死猎犬的记载。\[1\]
2014年，曾有多隻水獺在美國國家公園被拍攝到攻擊鱷魚的畫面。

## 保育狀況

各種水獺的保護現狀不一。在2006年的[世界自然保護聯盟瀕危物種紅色名錄中](../Page/世界自然保護聯盟瀕危物種紅色名錄.md "wikilink")，[北美水獺](../Page/北美水獺.md "wikilink")\[2\]屬[無危級別](../Page/無危.md "wikilink")，而[歐亞水獺](../Page/歐亞水獺.md "wikilink")\[3\]和[亞洲小爪水獺](../Page/亞洲小爪水獺.md "wikilink")\[4\]則屬[近危](../Page/近危.md "wikilink")，但如[海獺](../Page/海獺.md "wikilink")\[5\]、[巨獺](../Page/巨獺.md "wikilink")\[6\]等物種則已處於[瀕危級別](../Page/瀕危.md "wikilink")。

水獺受到[瀕臨絕種野生動植物國際貿易公約的保護](../Page/瀕臨絕種野生動植物國際貿易公約.md "wikilink")。持有、[出口及](../Page/出口.md "wikilink")[轉口水獺均必須向有關部门申請許可證](../Page/轉口.md "wikilink")。\[7\]

歐亞水獺是中国國家一級保護動物。\[8\]

## 物種

  - [小爪水獺屬](../Page/小爪水獺屬.md "wikilink") (*Aonyx*)
      - [非洲小爪水獺](../Page/非洲小爪水獺.md "wikilink") (*Aonyx capensis*)
      - [剛果小爪水獺](../Page/剛果小爪水獺.md "wikilink") (*Aonyx congicus*)
      - [亞洲小爪水獺](../Page/亞洲小爪水獺.md "wikilink") (*Aonyx cinereus*)
  - [海獺屬](../Page/海獺屬.md "wikilink") (*Enhydra*)
      - [海獺](../Page/海獺.md "wikilink") (*Enhydra lutris*)
  - [美洲獺屬](../Page/美洲獺屬.md "wikilink") (*Lontra*)
      - [北美水獺](../Page/北美水獺.md "wikilink") (*Lontra canadensis*)
      - [智利水獺](../Page/智利水獺.md "wikilink") (*Lontra provocax*)
      - [長尾水獺](../Page/長尾水獺.md "wikilink") (*Lontra longicaudis*)
      - [貓獺](../Page/貓獺.md "wikilink") (*Lontra felina*)
  - [水獺屬](../Page/水獺屬.md "wikilink") (*Lutra*)
      - [歐亞水獺](../Page/歐亞水獺.md "wikilink") (*Lutra lutra*)
          - [日本水獺](../Page/日本水獺.md "wikilink") *Lutra lutra nippon*
      - [長鼻水獺](../Page/長鼻水獺.md "wikilink") (*Lutra sumatrana*)
      - [斑頸水獺](../Page/斑頸水獺.md "wikilink") (*Lutra maculicollis*)
  - [江獺屬](../Page/江獺屬.md "wikilink") (*Lutrogale*)
      - [江獺](../Page/江獺.md "wikilink") (*Lutrogale perspicillata*)
  - [巨獺屬](../Page/巨獺屬.md "wikilink") (*Pteronura*)
      - [巨獺](../Page/巨獺.md "wikilink") (*Pteronura brasiliensis*)

## 民間的水獭

### 中國大陸

中国古代的[志怪小说中有水獭成精的故事](../Page/志怪小说.md "wikilink")。[晋代](../Page/晋代.md "wikilink")[戴祚的](../Page/戴祚.md "wikilink")《[甄异记](../Page/甄异记.md "wikilink")》和[南朝宋时](../Page/南朝宋.md "wikilink")[刘义庆的](../Page/刘义庆.md "wikilink")《[幽明录](../Page/幽明录.md "wikilink")》中分别有一则和三则有关水獭成精的故事。其中的水獭会化成美丽女子，魅惑路过水边的人\[9\]\[10\]。晋代[干宝的](../Page/干宝.md "wikilink")《[搜神记](../Page/搜神记.md "wikilink")》中也有水獭精的记载，叙述水獭化为撑伞的女子，魅惑河边行人\[11\]。

### 臺灣

歐亞水獺早期遍布台灣河川，有民間偏方相信水獺的手可以治療感冒，導致許多人延誤就診死亡。

### 日本

[SekienKawauso.jpg](https://zh.wikipedia.org/wiki/File:SekienKawauso.jpg "fig:SekienKawauso.jpg")《[画图百鬼夜行](../Page/画图百鬼夜行.md "wikilink")》中水獭妖怪的形象。\]\]
在日本水獭又名川獺。在民间传说中，有很多水獭幻化为人报恩的故事。在[鸟山石燕的](../Page/鸟山石燕.md "wikilink")《[画图百鬼夜行](../Page/画图百鬼夜行.md "wikilink")》里也有对川獭的描写。由于身形类似黄鼠狼，水獭也被认为是聪明有灵性的动物。与中国神话类似，水獭会化成女性，穿上和服，用伞蒙住脸。如果有年轻人向她打招呼的话，就会拿下伞，露出它原本的脸孔，趁着年轻人吃惊的间隙袭击并咬死他\[12\]。在[水木茂的作品](../Page/水木茂.md "wikilink")[鬼太郎里也有登场](../Page/鬼太郎.md "wikilink")。有说法是川獺是[河童的变形](../Page/河童.md "wikilink")。

## 與人類的關係

從千年前開始，世界各地就有漁民會馴養水獺來協助捕魚\[13\]\[14\]，不過此一傳統目前已瀕臨失傳\[15\]\[16\]\[17\]。

## 參考資料

[Category:河童](../Category/河童.md "wikilink")
[Category:鼬科](../Category/鼬科.md "wikilink")
[Category:水獺亞科](../Category/水獺亞科.md "wikilink")
[Category:石川縣的妖怪](../Category/石川縣的妖怪.md "wikilink")

1.  见《中国动物志 兽纲 食肉目》
2.  世界自然保護聯盟瀕危物種紅色名錄
    (北美水獺)，2006年，<http://www.iucnredlist.org/search/details.php/12302/all>
3.  世界自然保護聯盟瀕危物種紅色名錄
    (歐亞水獺)，2006年，<http://www.iucnredlist.org/search/details.php/12419/all>
4.  世界自然保護聯盟瀕危物種紅色名錄
    (亞洲小爪水獺)，2006年，<http://www.iucnredlist.org/search/details.php/44166/all>
5.  世界自然保護聯盟瀕危物種紅色名錄
    (海獺)，2006年，<http://www.iucnredlist.org/search/details.php/7750/all>
6.  世界自然保護聯盟瀕危物種紅色名錄
    (巨獺)，2006年，<http://www.iucnredlist.org/search/details.php/18711/all>
7.  [世界自然基金會](../Page/世界自然基金會.md "wikilink")（香港分會）有關瀕臨絕種野生動植物國際貿易公約的介紹，<http://www.wwf.org.hk/chi/conservation/wl_trade/cites/how.php>
8.  科技之光網站，<http://www.losn.com.cn/hjbh/cnanimals/3-57.htm>
9.  （南朝宋）刘义庆，《幽明錄》，文化艺术出版社.
10. 李昉等，《太平廣記》卷四百六十八，水族五
11. 干宝，《搜神记》卷十八。
12. [川獺（カワウソ）](http://www58.tok2.com/home/hermitage/monster_touyou/kawauso.htm)

13.
14.
15.
16.
17.