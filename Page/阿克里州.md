[Trevo_BR-364.2.jpg](https://zh.wikipedia.org/wiki/File:Trevo_BR-364.2.jpg "fig:Trevo_BR-364.2.jpg")

**阿克里**（[葡萄牙语](../Page/葡萄牙语.md "wikilink")：**Acre**，[国际音标](../Page/国际音标.md "wikilink")：//\[1\]）是[巴西的一个](../Page/巴西.md "wikilink")[州](../Page/巴西行政区划.md "wikilink")，位于该国的西北部，首府[里约布兰科](../Page/里约布兰科.md "wikilink")。阿克里州以北是[亚马孙州](../Page/亚马孙州_\(巴西\).md "wikilink")，以东是[朗多尼亚州](../Page/朗多尼亚州.md "wikilink")；以南接壤[玻利维亚](../Page/玻利维亚.md "wikilink")，以西则与[秘鲁](../Page/秘鲁.md "wikilink")[乌卡亚利区域相邻](../Page/乌卡亚利区域.md "wikilink")。

## 地理

阿克里州的土地大部分被[亚马孙雨林覆盖](../Page/亚马孙雨林.md "wikilink")。该州主要生产和出口[橡胶](../Page/橡胶.md "wikilink")。[阿克里河位于阿克里州南部](../Page/阿克里河.md "wikilink")，是为巴西与玻利维亚的边界。

## 注释

<div class="references-small">

<references />

</div>

## 外部链接

  - [阿克里州位置](https://web.archive.org/web/20070311025951/http://www.professores.uff.br/hjbortol/arquivo/2006.1/applets/acre_en.html)（Java）
  - [阿克里州政府官方网站](http://www.ac.gov.br)

[Category:巴西行政區劃](../Category/巴西行政區劃.md "wikilink")

1.  这个音标是根据[巴西葡萄牙语](../Page/巴西葡萄牙语.md "wikilink")；[欧洲葡萄牙语发音则是](../Page/欧洲葡萄牙语.md "wikilink")
    //。