**奈及利亞國家足球隊**，別稱「**非洲雄鹰**」或者「**超霸鷹**」，是[奈及利亞男子](../Page/奈及利亞.md "wikilink")[足球國家代表隊](../Page/足球.md "wikilink")，由[奈及利亞足球協會管理](../Page/奈及利亞足球協會.md "wikilink")。

奈及利亞的球衣贊助商是[Nike](../Page/Nike.md "wikilink")。

## 歷史

尼日利亞首次晉身[世界盃決賽週是在](../Page/世界盃足球賽.md "wikilink")[1994年](../Page/1994年世界盃足球賽.md "wikilink")，及後兩屆賽事均可以出線決賽週，其中1994年及1998年都曾經晉身十六強階段，由於隊中每位球員獨當一面，整體踢法極具侵略性，是1990年代一支成績崛起的國家足球隊。

尼日利亞於[1980年](../Page/1980年非洲國家盃.md "wikilink")、[1994年及](../Page/1994年非洲國家盃.md "wikilink")[2013年三度贏得](../Page/2013年非洲國家盃.md "wikilink")[非洲國家盃冠軍](../Page/非洲國家盃.md "wikilink")，亦於1996年獲得[奧林匹克運動會金牌](../Page/1996年夏季奧林匹克運動會.md "wikilink")。

2008年非洲國家盃，尼日利亞於八強被迦納打敗，而福士也提早請辭收場。

## 世界盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>總排名</p></th>
<th><p>場數</p></th>
<th><p>勝</p></th>
<th><p>和*</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
<td><p><em>沒有參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
<td><p><em>退出</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
<td><p>十六強</p></td>
<td><p>9/24</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
<td><p>十六強</p></td>
<td><p>12/32</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
<td><p>小組賽</p></td>
<td><p>27/32</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
<td><p>小組賽</p></td>
<td><p>27/32</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
<td><p>十六強</p></td>
<td><p>16/32</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
<td><p>小組賽</p></td>
<td><p>21/32</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2022年世界盃足球賽.md" title="wikilink">2022年</a></p></td>
<td><p>待定</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p>6/21</p></td>
<td></td>
<td><p>21</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>12</p></td>
<td><p>23</p></td>
<td><p>30</p></td>
</tr>
</tbody>
</table>

## 洲際國家盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>場數</p></th>
<th><p>勝</p></th>
<th><p>和*</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1992年洲際國家盃.md" title="wikilink">1992年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1995年洲際國家盃.md" title="wikilink">1995年</a></p></td>
<td><p>第四名</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997年洲際國家盃.md" title="wikilink">1997年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1999年洲際國家盃.md" title="wikilink">1999年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2001年洲際國家盃.md" title="wikilink">2001年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2003年洲際國家盃.md" title="wikilink">2003年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005年洲際國家盃.md" title="wikilink">2005年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年洲際國家盃.md" title="wikilink">2009年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年洲際國家盃.md" title="wikilink">2013年</a></p></td>
<td><p>第一圈</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年洲際國家盃.md" title="wikilink">2017年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p>2/10</p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>11</p></td>
<td><p>7</p></td>
</tr>
</tbody>
</table>

## 非洲國家盃成績

<table>
<colgroup>
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th></th>
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th></th>
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1957年非洲國家盃.md" title="wikilink">1957年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1976年非洲國家盃.md" title="wikilink">1976年</a></p></td>
<td><p>季軍</p></td>
<td><p><a href="../Page/1994年非洲國家盃.md" title="wikilink">1994年</a></p></td>
<td><p><strong>冠軍</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1959年非洲國家盃.md" title="wikilink">1959年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1978年非洲國家盃.md" title="wikilink">1978年</a></p></td>
<td><p>季軍</p></td>
<td><p><a href="../Page/1996年非洲國家盃.md" title="wikilink">1996年</a></p></td>
<td><p><em>賽前退出</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1962年非洲國家盃.md" title="wikilink">1962年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1980年非洲國家盃.md" title="wikilink">1980年</a></p></td>
<td><p><strong>冠軍</strong></p></td>
<td><p><a href="../Page/1998年非洲國家盃.md" title="wikilink">1998年</a></p></td>
<td><p><em>被取消參賽資格</em> <a href="../Page/#fn_1.md" title="wikilink"><sup>1</sup></a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1963年非洲國家盃.md" title="wikilink">1963年</a></p></td>
<td><p>第一圈</p></td>
<td><p><a href="../Page/1982年非洲國家盃.md" title="wikilink">1982年</a></p></td>
<td><p>第一圈</p></td>
<td><p><a href="../Page/2000年非洲國家盃.md" title="wikilink">2000年</a></p></td>
<td><p>亞軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1965年非洲國家盃.md" title="wikilink">1965年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1984年非洲國家盃.md" title="wikilink">1984年</a></p></td>
<td><p>亞軍</p></td>
<td><p><a href="../Page/2002年非洲國家盃.md" title="wikilink">2002年</a></p></td>
<td><p>季軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年非洲國家盃.md" title="wikilink">1968年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1986年非洲國家盃.md" title="wikilink">1986年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/2004年非洲國家盃.md" title="wikilink">2004年</a></p></td>
<td><p>季軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1970年非洲國家盃.md" title="wikilink">1970年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1988年非洲國家盃.md" title="wikilink">1988年</a></p></td>
<td><p>亞軍</p></td>
<td><p><a href="../Page/2006年非洲國家盃.md" title="wikilink">2006年</a></p></td>
<td><p>季軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1972年非洲國家盃.md" title="wikilink">1972年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1990年非洲國家盃.md" title="wikilink">1990年</a></p></td>
<td><p>亞軍</p></td>
<td><p><a href="../Page/2008年非洲國家盃.md" title="wikilink">2008年</a></p></td>
<td><p>八強</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年非洲國家盃.md" title="wikilink">1974年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p><a href="../Page/1992年非洲國家盃.md" title="wikilink">1992年</a></p></td>
<td><p>季軍</p></td>
<td><p><a href="../Page/2010年非洲國家盃.md" title="wikilink">2010年</a></p></td>
<td><p>季軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/2013年非洲國家盃.md" title="wikilink">2013年</a></p></td>
<td><p><strong>冠軍</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015年非洲國家盃.md" title="wikilink">2015年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年非洲國家盃.md" title="wikilink">2017年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2019年非洲國家盃.md" title="wikilink">2019年</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<small> 由於上屆賽事（[1996年](../Page/1996年非洲國家盃.md "wikilink")）賽前退出而被罰禁止參賽一屆。
</small>

<small>從2013年賽事開始，賽事從偶數年改至奇數年舉辦，不會和世界盃足球賽衝突。 </small>

## 近賽成績

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽第二輪F組](../Page/2014年世界盃外圍賽非洲區第二輪#F組.md "wikilink")

## 球員名單

### [2018年國際足總世界盃](../Page/2018年國際足總世界盃.md "wikilink")

最新一期球員名單更新至[2018年國際足總世界盃](../Page/2018年國際足總世界盃.md "wikilink")23人名單。\[1\]
    |----- \! colspan="9" bgcolor="\#B0D3FB" align="left" | |-----
bgcolor="\#DFEDFD"         |----- \! colspan="9" bgcolor="\#B0D3FB"
align="left" | |----- bgcolor="\#DFEDFD"        |----- \! colspan="9"
bgcolor="\#B0D3FB" align="left" | |----- bgcolor="\#DFEDFD"

## 著名球員

  - [-{zh-hant:奧利殊; zh-cn:桑迪·奥利塞赫;}-](../Page/桑迪·奥利塞赫.md "wikilink")
  - [-{zh-hant:簡奴; zh-cn:恩万科·卡努;}-](../Page/恩萬科沃·卡努.md "wikilink")
  - [-{zh-hant:奧高查; zh-cn:杰伊·杰伊·奥科查;}-](../Page/杰伊·杰伊·奥科查.md "wikilink")
  - [-{zh-hant:韋斯特; zh-cn:特里柏·韦斯特;}-](../Page/特里柏·韦斯特.md "wikilink")
  - [-{zh-hant:巴巴耶路;
    zh-cn:塞莱斯汀·巴巴亚罗;}-](../Page/塞莱斯汀·巴巴亚罗.md "wikilink")
  - [耶古保](../Page/耶古保.md "wikilink")
  - [-{zh-hant:馬田斯; zh-cn:奥巴费米·马丁斯;}-](../Page/奥巴费米·马丁斯.md "wikilink")
  - [-{zh-hant:艾哈賀華;
    zh-cn:朱利尤斯·阿加霍瓦;}-](../Page/朱利尤斯·阿加霍瓦.md "wikilink")

## 參考資料

## 外部連結

  - [尼日利亞足協官方網站](http://www.nigeriafa.com/)
  - [超霸鷹](http://www.supereagles.com/)
  - [RSSSF檔案室-1955年後所有賽事賽果](http://www.rsssf.com/tablesn/nig-intres.html)

[Category:非洲足球代表隊](../Category/非洲足球代表隊.md "wikilink")
[Category:尼日利亞國家足球隊](../Category/尼日利亞國家足球隊.md "wikilink")

1.