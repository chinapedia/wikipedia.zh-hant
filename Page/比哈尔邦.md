**比哈尔邦**（[印地语](../Page/印地语.md "wikilink")：****，[乌尔都语](../Page/乌尔都语.md "wikilink")：****，[拉丁字母转写](../Page/拉丁字母.md "wikilink")：）是[印度东北的一个邦](../Page/印度.md "wikilink")，传说是[佛教的起源地](../Page/佛教.md "wikilink")。除[印地语](../Page/印地语.md "wikilink")、[乌尔都语外](../Page/乌尔都语.md "wikilink")，[比哈尔语也是该邦的官方语言](../Page/比哈尔语.md "wikilink")，此語言包含的方言有[昂加语](../Page/昂加语.md "wikilink")、[博杰普尔语](../Page/博杰普尔语.md "wikilink")、[摩揭陀語以及](../Page/摩揭陀語.md "wikilink")[迈蒂利语](../Page/迈蒂利语.md "wikilink")。[華氏城與](../Page/華氏城.md "wikilink")[王舍城](../Page/王舍城.md "wikilink")，[菩提伽耶是印度佛教的重要地方](../Page/菩提伽耶.md "wikilink")。值得一提的是，這裡是全印度[婆羅門與](../Page/婆羅門.md "wikilink")[賤民互鬥最嚴重的地方](../Page/賤民.md "wikilink")。[恆河橫貫比哈爾邦邦境](../Page/恆河.md "wikilink")。南部為[拉杰默哈爾丘陵](../Page/拉杰默哈爾丘陵.md "wikilink")。邦議會為兩院制。

全邦下分[38縣](../Page/比哈尔邦行政区划.md "wikilink")。

比哈爾邦是主要礦產地，產[雲母](../Page/雲母.md "wikilink")、[煤](../Page/煤.md "wikilink")、[銅](../Page/銅.md "wikilink")。農業產品有[稻米](../Page/稻米.md "wikilink")、[甘蔗](../Page/甘蔗.md "wikilink")、[油籽](../Page/油籽.md "wikilink")、[黃麻](../Page/黃麻.md "wikilink")、[玉蜀黍及](../Page/玉蜀黍.md "wikilink")[小麥](../Page/小麥.md "wikilink")。工業產品有[鋼鐵](../Page/鋼鐵.md "wikilink")、[機床工具](../Page/機床工具.md "wikilink")、[水泥](../Page/水泥.md "wikilink")、[電機](../Page/電機.md "wikilink")、[化學肥料及](../Page/化學肥料.md "wikilink")[紙張](../Page/紙張.md "wikilink")。

## 經濟

| 印度全國                                             | [巴特那](../Page/巴特那.md "wikilink") | [班加羅爾](../Page/班加羅爾.md "wikilink")                   | [孟買](../Page/孟買.md "wikilink")    | [德里](../Page/德里.md "wikilink") | [加爾各答](../Page/加爾各答.md "wikilink")                                                               | [海德拉巴](../Page/海德拉巴.md "wikilink") |
| ------------------------------------------------ | -------------------------------- | ---------------------------------------------------- | --------------------------------- | ------------------------------ | ------------------------------------------------------------------------------------------------ | ---------------------------------- |
| Rs 22,946<ref name="patna_prosperity">{{cite web | first = Aditi |last = Nigam      | title = For Bihar, P stands for Patna and prosperity | publisher = The Financial Express | year = 2008                    | url = <http://www.financialexpress.com/news/For-Bihar-P-stands-for-Patna-and-prosperity/293289/> | accessdate = 2008-04-07 }}</ref>   |

  - 單位為[印度盧比](../Page/印度盧比.md "wikilink")

### 主要經濟走向

下表是表示比哈爾邦邦內生產總值\[1\]由印度統計和計畫部門所公佈的資料，單位為百萬[印度盧比](../Page/印度盧比.md "wikilink")。

| 年份    | 比哈爾邦邦內生產總值 |
| ----- | ---------- |
| 1980年 | 73,530     |
| 1985年 | 142,950    |
| 1990年 | 264,290    |
| 1995年 | 244,830    |
| 2000年 | 469,430    |
| 2008年 | 568,450    |

包括[賈坎德邦](../Page/賈坎德邦.md "wikilink")，為2000年11月15日脫離比哈爾邦，成為印度第28邦。

## 中國驻印军与[滇西远征军的训练](../Page/滇西远征军.md "wikilink")

1942年7月15日，[新38师由](../Page/新38师.md "wikilink")[因帕爾开往比哈尔邦](../Page/因帕爾.md "wikilink")[蓝姆伽](../Page/蓝姆伽.md "wikilink")，8月初，从[缅甸北方](../Page/缅甸.md "wikilink")[野人山脱险入印的](../Page/野人山.md "wikilink")[第5军](../Page/国民革命军第五军.md "wikilink")[新22师和军直属部队也来到了蓝姆伽](../Page/新22师.md "wikilink")。根据中美协议，[远征军第一路司令长官部撤销](../Page/远征军第一路.md "wikilink")，改称为[中国驻印军总指挥部](../Page/中国驻印军.md "wikilink")。[史迪威为总指挥](../Page/史迪威.md "wikilink")，[罗卓英为副总指挥](../Page/罗卓英.md "wikilink")。同时，国民政府利用[驼峰航線的飞机回航机会](../Page/驼峰航線.md "wikilink")，每天空运几百名士兵到印度，以补充兵源。

1942年底，由于[史迪威与罗卓英矛盾不可调和](../Page/史迪威.md "wikilink")，[蒋中正被迫将罗卓英调回国内](../Page/蒋中正.md "wikilink")，经过反复考虑，决定派第8军军长[郑洞国中将接替罗卓英的职务](../Page/郑洞国.md "wikilink")。同时决定在驻印军指挥部下设新编第一军建制，下辖新38师、新22师。郑洞国任军长，[孙立人为副军长兼新](../Page/孙立人.md "wikilink")38师师长，[廖耀湘为新](../Page/廖耀湘.md "wikilink")22师师长。1943年3月中旬，郑洞国率军部人员来到蓝姆伽，正式成立[新一军](../Page/新一军.md "wikilink")。\[2\]

## 参考文献

## 外部連結

  - [比哈尔邦官方网页](http://www.bihar.com)
  - [Database of Bhojpuri Speakers](http://www.bhojpuri.org/)
  - [Bhojpuri eGroup](http://groups.yahoo.com/group/bhojpuri/)

{{-}}

[比哈尔邦](../Category/比哈尔邦.md "wikilink")
[邦](../Category/印度的邦和中央直辖区.md "wikilink")

1.  [估計](http://mospi.nic.in/mospi_nad_main.htm)
2.  《中缅印战场抗日战争史》，徐康明著，解放军出版社，2007年。