**圣犹达**（，）是[耶稣的](../Page/耶稣.md "wikilink")[十二门徒之一](../Page/十二门徒.md "wikilink")，他亦被稱為**聖达陡**（，、）。因此他也被稱為**犹达·达陡**（Jude
Thaddaeus，或Judas Thaddaeus）或**犹达·利貝烏**（Jude Lebbeus、Judas
Lebbeus）。有时，他被认作[“耶稣的兄弟”犹达](../Page/犹大_\(耶稣的兄弟\).md "wikilink")。应当注意的是，他不是出賣耶穌的[犹大](../Page/加略人猶大.md "wikilink")\[1\]。

[亞美尼亞使徒教會將他與](../Page/亞美尼亞使徒教會.md "wikilink")[巴尔多禄茂](../Page/巴多羅買.md "wikilink")，列為其[主保聖人](../Page/主保聖人.md "wikilink")。在[罗马天主教中](../Page/罗马天主教.md "wikilink")，他是衰败的事业和处于绝境的人的[主保圣人](../Page/主保圣人.md "wikilink")。在[俄羅斯正教会傳統中被稱為Saint](../Page/俄羅斯正教会.md "wikilink")
Faddej（, in Russian, along with Saint Jude）。

圣犹达的[象征是棍棒](../Page/圣人符号学.md "wikilink")。在[圣像中](../Page/圣像.md "wikilink")，他的头顶常围绕着[火焰](../Page/火焰.md "wikilink")。犹达的另一个象征是他握着[曼德兰基督圣像](../Page/:en:Image_of_Edessa.md "wikilink")。有些场合中，犹达会拿着一支卷轴、书本，表明他是[公函](../Page/大公书信.md "wikilink")[犹达书的作者](../Page/犹达书.md "wikilink")。还有就是拿着[木匠的尺](../Page/木匠.md "wikilink")。

## 身份推测

《[新约](../Page/新约.md "wikilink")》中关于犹达的记载很少，只知他又称为雅各伯的兒子（或者是兄弟）犹达（不是出卖耶稣的[犹达斯](../Page/加略人猶大.md "wikilink")），可能是[耶穌的兄弟犹达](../Page/猶大_\(耶穌的兄弟\).md "wikilink")。一般被认为是[新约的](../Page/新约.md "wikilink")[猶达書作者](../Page/猶达書.md "wikilink")。

《玛窦福音》稱他為达陡（；）\[2\]。在某些[拉丁文抄本中](../Page/拉丁文.md "wikilink")，他被稱為热诚者猶达。

《路加福音》與《[宗徒大事录](../Page/宗徒大事录.md "wikilink")》稱他為，雅各伯的猶达（）\[3\]\[4\]。某些[新教圣经版本在翻譯時](../Page/新教.md "wikilink")，這句話可以被翻為「雅各伯的兒子猶达」（如[新国际版圣经](../Page/新国际版圣经.md "wikilink")）、或「雅各伯的兄弟猶达」（如[詹姆士王譯本](../Page/詹姆士王譯本.md "wikilink")）。在《[猶达書](../Page/猶达書.md "wikilink")》中，他自稱為雅各伯的兄弟猶达\[5\]。如果此處的雅各伯是指耶穌的兄弟，公義者雅各伯，那麼宗徒猶达應該就是[耶穌的兄弟猶达](../Page/猶大_\(耶穌的兄弟\).md "wikilink")。

## 福音书中

唯一有关犹达对于[信仰所发出的问题](../Page/信仰.md "wikilink")，是他问[耶穌说](../Page/耶穌.md "wikilink")：“主，究竟为了甚麼你要将你自己显示给我们，而不显示给世界呢？”\[6\]，而耶穌回答说，只有愿意谦卑接受真理和真心爱天主、遵守他圣意的人，才能与天主同在。\[7\]

## 教会传统

[Thaddeus_San_Giovanni_in_Laterano_2006-09-07.jpg](https://zh.wikipedia.org/wiki/File:Thaddeus_San_Giovanni_in_Laterano_2006-09-07.jpg "fig:Thaddeus_San_Giovanni_in_Laterano_2006-09-07.jpg")所作。\]\]

教会传统上叙述犹达往[犹太](../Page/犹太.md "wikilink")、[撒玛利亚](../Page/撒玛利亚.md "wikilink")、[厄东](../Page/以东.md "wikilink")、[叙利亚](../Page/叙利亚.md "wikilink")、[美索不达米亚](../Page/美索不达米亚.md "wikilink")、[利比亚传教](../Page/利比亚.md "wikilink")。据说他还也去了[贝鲁特和](../Page/贝鲁特.md "wikilink")[埃德萨](../Page/:en:Edessa.md "wikilink")。

犹达能说希腊语和阿拉美语，就像这个地区其他同代的人一样，是一位农民。根据传说，犹达是[克罗帕与](../Page/革罗罢.md "wikilink")[玛利亚之子](../Page/马利亚_\(革罗罢的妻子\).md "wikilink")。传统上认为，犹达的父亲克罗帕因为他直率地为复活的[基督作证而殉道](../Page/基督.md "wikilink")。\[8\]

尽管[圣启蒙者额我略被认作是促使](../Page/格列高利_\(启蒙者\).md "wikilink")[亚美尼亚皈依基督教的人](../Page/亚美尼亚.md "wikilink")，犹达与[巴尔多禄茂在传统上被视为是最早将基督教传入亚美尼亚的人物](../Page/巴尔多禄茂.md "wikilink")，因此被[亚美尼亚使徒教会奉为亚美尼亚的](../Page/亚美尼亚使徒教会.md "wikilink")[主保圣人](../Page/主保圣人.md "wikilink")。人们建立了圣达徒修道院（现位于[伊朗北部](../Page/伊朗.md "wikilink")）与圣巴尔多禄茂修道院（现位于[土耳其东南部](../Page/土耳其.md "wikilink")）纪念他们。

## 殉道

[Thaddeus_mosaic.jpg](https://zh.wikipedia.org/wiki/File:Thaddeus_mosaic.jpg "fig:Thaddeus_mosaic.jpg")的象征\]\]

根据教会传统，圣犹达于公元65年，与[热诚者西满一起在](../Page/热诚者西满.md "wikilink")[罗马帝国](../Page/罗马帝国.md "wikilink")[叙利亚行省的](../Page/叙利亚_\(罗马行省\).md "wikilink")[贝鲁特殉道](../Page/贝鲁特.md "wikilink")。犹达被利斧击毙，这也是在艺术形象中他常持利斧的原因。\[9\]这一事件被记录于Abdias
of Babylon与他的学生Tropaeus Africanus的作品*[Golden
Legend](../Page/:en:Golden_Legend.md "wikilink")*中。\[10\]

犹达死后，他的[圣髑被从](../Page/圣髑.md "wikilink")[贝鲁特迁往](../Page/贝鲁特.md "wikilink")[罗马的](../Page/罗马.md "wikilink")[圣伯多禄大殿的地下室内](../Page/圣伯多禄大殿.md "wikilink")，与[圣西满的圣髑一起安葬](../Page/热诚者西满.md "wikilink")。另一说是犹达的圣髑被保存在[吉尔吉斯斯坦境内的](../Page/吉尔吉斯斯坦.md "wikilink")[伊塞克湖上的一座小岛的](../Page/伊塞克湖.md "wikilink")[修道院内](../Page/修道院.md "wikilink")，一直到15世纪，之后被移往[帕米尔高原的一个荒废的堡垒中](../Page/帕米尔高原.md "wikilink")。

## 作品

新约全集中，[犹达书被认为是圣犹达宗徒所写](../Page/犹达书.md "wikilink")。犹达写此信的动机，是因为他听到这些信友已处于在假学士和[异端邪说的威胁下](../Page/异端.md "wikilink")，所以写下此信，为保卫信友的[信德](../Page/信德.md "wikilink")，指明这些假学士及其害人的异端邪说。\[11\]

本书文笔简单，但生动有力，近似[旧约中的](../Page/旧约.md "wikilink")[先知文体](../Page/先知.md "wikilink")。他喜用[比喻](../Page/比喻.md "wikilink")，富于想象。作者为了容易表明[真理](../Page/真理.md "wikilink")，连犹太民间所流行的载于[次经](../Page/次经.md "wikilink")《》\[12\]\[13\]\[14\]\[15\]以及《[以诺书](../Page/以诺书.md "wikilink")》\[16\]\[17\]\[18\]上的事迹言论也加以引用\[19\]，正如[保禄也引用过外教作家和诗人的诗句一样](../Page/保禄.md "wikilink")。

本书的写作时间，大约是在64与65年间，即在[次雅各伯死后](../Page/次雅各伯.md "wikilink")（62年），[伯多禄后书写作之前](../Page/伯多禄后书.md "wikilink")。本书的写作地点，大概是在[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")，或者就是在[耶路撒冷](../Page/耶路撒冷.md "wikilink")。

## 参见

  - [猶大 (耶穌的兄弟)](../Page/猶大_\(耶穌的兄弟\).md "wikilink")
  - [加略人猶大](../Page/加略人猶大.md "wikilink")
  - [雅各](../Page/雅各.md "wikilink")
  - [犹大支派](../Page/犹大支派.md "wikilink")
  - [犹大王国](../Page/犹大王国.md "wikilink")
  - [十二门徒](../Page/十二门徒.md "wikilink")

## 参考文献

## 外部链接

  - Catherine Fournier, [Saint Simon and Saint
    Jude](http://www.domestic-church.com/CONTENT.DCC/19980901/SAINTS/STJUDE.HTM)

{{-}}

[Category:耶稣的十二使徒](../Category/耶稣的十二使徒.md "wikilink")
[Category:新约圣经中的人](../Category/新约圣经中的人.md "wikilink")
[Category:1世纪基督教聖人](../Category/1世纪基督教聖人.md "wikilink")
[Category:犹太人](../Category/犹太人.md "wikilink")

1.  参看、

2.  路6:14-16：「即西满，耶穌又給他起名叫[伯多禄](../Page/伯多禄.md "wikilink")、和他的兄弟[安得肋](../Page/安德烈.md "wikilink")、[雅各伯](../Page/雅各伯.md "wikilink")、[若望](../Page/若望.md "wikilink")、[斐理伯和](../Page/斐理伯.md "wikilink")[巴尔多禄茂](../Page/巴尔多禄茂.md "wikilink")、[玛窦](../Page/玛窦.md "wikilink")、[多默](../Page/多默.md "wikilink")、[阿斐尔的兒子雅各伯](../Page/阿斐尔的兒子雅各伯.md "wikilink")、号称“热诚者”的[西满](../Page/热诚者西满.md "wikilink")、雅各伯的兄弟犹达和[犹达斯依斯加略](../Page/犹达斯依斯加略.md "wikilink")，他成了负卖者。」

3.
4.  宗徒大事录1:13：「他们进了城，就上了那座他们所居住的樓房．在那裏有伯多禄、若望、雅各伯、安得肋、斐理伯、多默、巴尔多茂禄、玛窦、阿斐尔的兒子雅各伯、热诚者西满及雅各伯的兄弟猶达。」

5.  《猶达書》1:1：「耶穌基督的僕人，雅各伯的兄弟猶达，致书給在天主父内蒙爱，為耶穌基督而保存的蒙召者。」

6.  若14:22

7.  若14:23

8.

9.  ["St. Jude the Apostle", St. Jude Shrine,
    Koothattukulam](http://www.stjudekoothattukulam.org/st_judeapostle.html)


10.

11. 参见犹大书第4、8、10、12、16、23节。

12. [Origen](http://www.ccel.org/ccel/schaff/anf04.vi.v.iv.iv.html)

13. James Charlesworth *Old Testament Pseudepigrapha*, p. 76, [Google
    books
    link](http://books.google.com/books?id=eRQ9AAAAIAAJ&lpg=PP1&dq=James%20Charlesworth&pg=PA76#v=onepage&q=Jude%20Moses&f=false)

14.
15. The Assumption of Moses: a critical edition with commentary By
    Johannes Tromp. P270

16. Charles R. *Enoch* OUP, p. 119

17. Nickelsburg G. *1 Enoch* Fortress

18. Cox S. [*Slandering celestial
    beings*](http://www.christadelphia.org/pamphlet/p_sinned.htm)

19. 参见犹大书第9、14节