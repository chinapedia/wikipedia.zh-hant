**徐凱希**（，），原名**徐宛鈴**、**徐亦晴**、藝名**凱希**，舊藝名**鴨子**\[1\]\[2\]\[3\]，台灣女歌手，台灣女子組合[Twinko成員](../Page/Twinko.md "wikilink")。國中時與[夏宇童是同學](../Page/馬詩婷.md "wikilink")。畢業於[達人女中](../Page/達人女中.md "wikilink")、[世新大學口語傳播學系](../Page/世新大學.md "wikilink")。曾是台灣綜藝節目[我愛黑澀會的美眉之一](../Page/我愛黑澀會.md "wikilink")，曾參與林育賢導演《[六號出口](../Page/六號出口.md "wikilink")》的演出，亦獲邀參加台灣《[超級星光大道](../Page/超級星光大道.md "wikilink")》首季的歌唱比賽，獲得第11名\[4\]。現為華視《[天才衝衝衝](../Page/天才衝衝衝.md "wikilink")》節目固定助理主持。

## 出道過程

凱希小時候是合唱團團員，所以在 Twinko
成為唯一一位[和音歌手](../Page/和聲歌手.md "wikilink")。她就讀[達人女中二年級時擔任](../Page/達人女中.md "wikilink")[啦啦隊隊長](../Page/啦啦隊.md "wikilink")，由於樣子甜美可愛，因而被邀至綜藝節目《[我猜我猜我猜猜猜](../Page/我猜我猜我猜猜猜.md "wikilink")》的「我猜啦啦美少女」單元中表演[競技啦啦隊](../Page/競技啦啦隊.md "wikilink")。節目播出後，適逢當時[Channel
\[V](../Page/Channel_V.md "wikilink")\]的新節目《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》需要一批少女擔任節目中的美眉，凱希以其甜美可愛的外表及出色的競技啦啦隊表演，獲得機會成為節目中的美眉。

除了出色的競技啦啦隊表演之外，凱希甜美的歌聲亦是她廣受《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》觀眾支持的另一個原因。凱希可算是節目中公認的美聲美眉，因此每逢節目中的歌唱環節，她幾乎都能獲得表演機會。其後由於學業的關係，凱希停止了《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》的錄影，準備考試升讀大學。

2006年成功考進[世新大學後](../Page/世新大學.md "wikilink")，凱希重返《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》節目，由於在《[六號出口](../Page/六號出口.md "wikilink")》電影試鏡會中表現出色，得到導演林育賢的賞識，邀請她擔任電影《[六號出口](../Page/六號出口.md "wikilink")》的女配角。其後凱希憑著甜美的歌聲以及出色的競技啦啦隊，更與其他三位美眉（[萬太](../Page/陳螢怡.md "wikilink")、[米恩](../Page/蔣美恩.md "wikilink")、[妖嬌](../Page/陳美燕.md "wikilink")）獲選成為生力軍。

因在《我愛黑澀會》表現出色，凱希亦獲邀參加《[超級星光大道](../Page/超級星光大道.md "wikilink")》歌唱比賽，並用那時候的本名「徐宛鈴」參賽，雖然無緣進入10強，但仍得到[華研國際音樂的賞識](../Page/華研國際音樂.md "wikilink")，簽為旗下藝人。

## 超級星光大道參賽歷程

### 第一屆比賽成績

| 集數 | 演唱歌曲/表演項目                                                               | 分數 | 註                                                  |
| -- | ----------------------------------------------------------------------- | -- | -------------------------------------------------- |
| 02 | 遺失的美好／[張韶涵](../Page/張韶涵.md "wikilink")                                  | 15 | 百萬巨星殊死戰(上)，過關                                      |
| 03 | 替身／[周蕙](../Page/周蕙.md "wikilink")                                       | 11 | 音樂故事指定賽(下)，失敗（保障名額救回）                              |
| 09 | 你的甜蜜／[-{范}-曉萱](../Page/范曉萱.md "wikilink")                               | 15 | 一對一PK賽(下)，敗[謝震廷](../Page/謝震廷.md "wikilink")（20分）   |
| 10 | 哭砂／[黃鶯鶯](../Page/黃鶯鶯.md "wikilink")                                     | 13 | 老歌指定賽，失敗                                           |
| 12 | 梁山伯與茱麗葉／[卓文萱](../Page/卓文萱.md "wikilink")&[曹格](../Page/曹格.md "wikilink") | 19 | 16強合唱指定賽（feat.[林宥嘉](../Page/林宥嘉.md "wikilink")），過關 |
| 13 | 遇上愛／[楊丞琳](../Page/楊丞琳.md "wikilink")                                    | 12 | 15強淘汰賽，過關                                          |
| 14 | 一個像夏天一個像秋天／[-{范}-瑋琪](../Page/范瑋琪.md "wikilink")                         | 16 | 大震撼！臨場狀況考驗淘汰賽，過關                                   |
| 15 | 話題／[周蕙](../Page/周蕙.md "wikilink")                                       | 16 | 1對1pk淘汰賽，勝[林宥嘉](../Page/林宥嘉.md "wikilink")（14分）    |
| 16 | 求婚／[趙詠華](../Page/趙詠華.md "wikilink")                                     | 12 | 藝人合唱指定賽（feat.[趙詠華](../Page/趙詠華.md "wikilink")），淘汰  |
| 23 | 心動心痛／[劉畊宏](../Page/劉畊宏.md "wikilink")、[許慧欣](../Page/許慧欣.md "wikilink")  | 19 | 默契合唱考驗賽（feat.[楊宗緯](../Page/楊宗緯.md "wikilink")）     |

### 星光傳奇賽成績

| 集數 | 演唱歌曲/表演項目                              | 分數 | 註       |
| -- | -------------------------------------- | -- | ------- |
| 01 | 遺失的美好／[張韶涵](../Page/張韶涵.md "wikilink") | 15 | 重返榮耀，淘汰 |

### 其他

| 集數                                 | 表演項目             | 分數 | 註                                                                                      |
| ---------------------------------- | ---------------- | -- | -------------------------------------------------------------------------------------- |
| 特別節目                               | 啦啦隊表演            | 24 | 除夕特別節目－星光閃耀賀新春，勝[劉軒蓁](../Page/劉軒蓁.md "wikilink")&[丁衣凡](../Page/丁衣凡.md "wikilink")（20分） |
| [舞林大道](../Page/舞林大道.md "wikilink") | Cheerleading－黑白棋 | 42 | 舞林積分賽 Part I（與Monster合作）                                                               |

## 影視作品

### 電影

  - 2007年《[六號出口](../Page/六號出口.md "wikilink")》（飾 女配角-萱萱）
  - 2008年《[送暖](../Page/送暖.md "wikilink")》（飾 女主角-高中女生）
  - 2011年《[搶救老爸](../Page/搶救老爸.md "wikilink")》（飾 美麗娟）
  - 2012年《[花漾](../Page/花漾.md "wikilink")》 （飾 小杜鵑）
  - 2014年《[宅男女神殺人狂](../Page/宅男女神殺人狂.md "wikilink")》（飾 凱希）

### 戲劇

| 首播日期        | 播出頻道                                                                | 劇名                                           | 飾演    | 性質  |
| ----------- | ------------------------------------------------------------------- | -------------------------------------------- | ----- | --- |
| 2008年12月14日 | [華視](../Page/華視.md "wikilink")、[東森綜合台](../Page/東森綜合台.md "wikilink") | 《[王子看見二公主](../Page/王子看見二公主.md "wikilink")》   | 趙幸安   | 女配角 |
| 2009年10月18日 | [中視](../Page/中視.md "wikilink")、[八大綜合台](../Page/八大綜合台.md "wikilink") | 《[桃花小妹](../Page/桃花小妹.md "wikilink")》         | 鴨鴨護士  | 客串  |
| 2010年11月20日 | [公視](../Page/公視.md "wikilink")                                      | 《[死神少女](../Page/死神少女_\(電視劇\).md "wikilink")》 | 米雪    | 女配角 |
| 2011年9月18日  | [民視](../Page/民視.md "wikilink")                                      | 《[我可能不會愛你](../Page/我可能不會愛你.md "wikilink")》   | 張小姐   | 客串  |
| 2012年9月6日   | [公視](../Page/公視.md "wikilink")                                      | 《[你是春風我是雨](../Page/你是春風我是雨.md "wikilink")》   | MISS張 | 客串  |
| 2014年11月22日 | [八大綜合台](../Page/八大綜合台.md "wikilink")                                | 《[終極惡女](../Page/終極惡女.md "wikilink")》         | 荷潔    | 客串  |
| 2019年(待播)   |                                                                     | 《[電競高校](../Page/電競高校.md "wikilink")》         |       |     |

## 舞台劇

  - [如果兒童劇團](../Page/如果兒童劇團.md "wikilink")《[流浪狗之歌](../Page/流浪狗之歌.md "wikilink")》（飾
    女配角-Baby）
  - [如果兒童劇團](../Page/如果兒童劇團.md "wikilink")《[舅舅的閣樓](../Page/舅舅的閣樓.md "wikilink")》（飾
    女主角-小潔）
  - [KISS
    ME親一下劇團](../Page/KISS_ME親一下劇團.md "wikilink")《[姨婆的時光寶盒](../Page/姨婆的時光寶盒.md "wikilink")》（飾
    女主角-拉拉）

## 主持

  - [年代綜合台](../Page/年代綜合台.md "wikilink")
      - 《[動物福利社](../Page/動物福利社.md "wikilink")》(外景主持)
  - [中視](../Page/中國電視公司.md "wikilink")
      - 《[我猜我猜我猜猜猜](../Page/我猜我猜我猜猜猜.md "wikilink")》(主持)(2007/11/10-2008/09/27)
      - 《[Mr.Q特攻隊in Hakka
        2](../Page/Mr.Q特攻隊in_Hakka_2.md "wikilink")》(主持)(2013)
  - [Channel V](../Page/Channel_V.md "wikilink")
      - 《[美少女時代](../Page/美少女時代.md "wikilink")》(代班主持)(2010/04/14)
  - [MOMO親子台](../Page/MOMO親子台.md "wikilink")
      - 《[Mr.Q特攻隊in
        Hakka](../Page/Mr.Q特攻隊in_Hakka.md "wikilink")》(主持)(2012/04/08)
  - [公視 HI HD](../Page/公視_HI_HD.md "wikilink")
      - 《[Mr.Q特攻隊](../Page/Mr.Q特攻隊.md "wikilink")》(主持)(2012/08/14)
  - [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")
      - 《[全球中文音樂榜上榜](../Page/全球中文音樂榜上榜_\(臺灣版\).md "wikilink")》(助理主持)(2015/05/09，2016/03/19)
  - [遊戲犬網路平台](../Page/遊戲犬網路平台.md "wikilink")
      - 《[遊戲骨灰罈](../Page/遊戲骨灰罈.md "wikilink")》(主持)(2015/12/11)
  - [TVBS網路直播](../Page/TVBS網路直播.md "wikilink")
      - 《[下午茶新聞之下午茶綜口味](../Page/下午茶新聞之下午茶綜口味.md "wikilink")》(主持)(2017/07/26)
  - [華視](../Page/華視.md "wikilink")
      - 《[天才衝衝衝](../Page/天才衝衝衝.md "wikilink")》(固定助理主持)(2018/10/13－至今)

## 音樂作品

所屬團體之共同作品，請參閱[Twinko音樂作品](../Page/Twinko.md "wikilink")

### 單曲

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p><strong>唱片公司</strong></p></td>
<td><p><strong>發行日期</strong></p></td>
<td><p><strong>專輯名稱</strong></p></td>
<td><p><strong>曲目</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>1.</strong></p></td>
<td><p>滾石移動(發行)</p></td>
<td><p>2019年4月19日(數位發行)</p></td>
<td><p>《<strong>平行時空我愛你</strong>》</p></td>
<td><p>1.平行時空我愛你<br />
(<a href="../Page/愛奇藝.md" title="wikilink">愛奇藝</a><a href="../Page/如果愛，重來.md" title="wikilink">如果愛，重來片尾曲</a>)</p></td>
</tr>
</tbody>
</table>

### 其他歌曲

  - 《[總在我身旁](http://www.youtube.com/watch?v=3A6oBu-BsHE)》([星光幫合唱](../Page/星光幫.md "wikilink")，收錄於星光幫《[愛星光精選—昨天今天明天](../Page/愛星光精選—昨天今天明天.md "wikilink")》)
  - 《[比較美好的世界](http://www.youtube.com/watch?v=4JnC0L3IfY8)》
    ([華研群星](../Page/華研國際音樂.md "wikilink")，四川大地震公益單曲，與[動力火車](../Page/動力火車.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")、[Tank](../Page/呂建中.md "wikilink")、[飛輪海](../Page/飛輪海.md "wikilink")、[林宥嘉](../Page/林宥嘉.md "wikilink")、[周定緯](../Page/周定緯.md "wikilink")、[潘裕文](../Page/潘裕文.md "wikilink")、[許仁-{杰}-](../Page/許仁杰.md "wikilink")、[安伯政](../Page/安伯政.md "wikilink")、[劉力揚合唱](../Page/劉力揚.md "wikilink"))
  - 《管不住自己》
    (偶像劇《[愛就宅一起](../Page/愛就宅一起.md "wikilink")》插曲，收錄於《愛就宅一起電視原聲帶》)
  - 《[幸福的時光](http://www.youtube.com/watch?v=DiOYetru0kE)》
    (偶像劇《[那一年的幸福時光](../Page/那一年的幸福時光.md "wikilink")》片尾曲，與[潘裕文合唱](../Page/潘裕文.md "wikilink")，收錄於潘裕文《[夢·想·家](../Page/夢·想·家.md "wikilink")》
  - 《Praan》 (日劇《[東京Little
    Love](../Page/東京Little_Love.md "wikilink")》主題曲)
  - 《我的快樂世界》、《娘惹舞曲》、《星洲之夜》、《你的眼淚》、《四季歌》
    (新加坡新傳媒8頻道電視劇《[星洲之夜](../Page/星洲之夜.md "wikilink")》插曲，收錄於《[星洲之夜電視原聲帶](../Page/星洲之夜電視原聲帶.md "wikilink")》)
  - 《[暖流](http://www.youtube.com/watch?v=DZrOD2Me4sE)》
    ([廖曉彤主唱](../Page/廖曉彤.md "wikilink")，[Twinko全員配唱](../Page/Twinko.md "wikilink"))
    (暖流計劃一週年主題曲)

### MV

| 歌手                                                                      | MV名稱                        | 專輯名称                                                  | 日期          |
| ----------------------------------------------------------------------- | --------------------------- | ----------------------------------------------------- | ----------- |
| [星光幫](../Page/星光幫.md "wikilink")                                        | 總在我身旁                       | 星光幫[愛星光精選—昨天今天明天](../Page/愛星光精選—昨天今天明天.md "wikilink") | 2007年       |
| [華研群星](../Page/華研國際音樂.md "wikilink")                                    | 比較美好的世界                     | 比較美好的世界                                               | 2008年       |
| [潘裕文](../Page/潘裕文.md "wikilink") feat. **徐宛鈴**                          | 幸福的時光                       | 潘裕文[夢·想·家](../Page/夢·想·家.md "wikilink")               | 2009年       |
| [Twinko](../Page/Twinko.md "wikilink")                                  | 黑幫 Crime Craft              | Twinko X Crime Craft                                  | 2013年       |
| [廖曉彤](../Page/廖曉彤.md "wikilink")+[Twinko](../Page/Twinko.md "wikilink") | 暖流 I Am Always By Your Side |                                                       | 2013年       |
| **Twinko**                                                              | Falling in Love             | [Twinko同名迷你專輯](../Page/Twinko同名迷你專輯.md "wikilink")    | 2014年11月10日 |
| Bling Bling                                                             | 2014年11月13日                 |                                                       |             |
| 空位                                                                      | 2014年11月25日                 |                                                       |             |
| 說我愛你                                                                    | 2015年5月5日                   |                                                       |             |
| **Twinko**+[Dal★Shabet](../Page/Dal★Shabet.md "wikilink")               | AVENUE 創意方城市                |                                                       | 2015年5月9日   |
| **Twinko**                                                              | 夢遊症                         | 戀愛限定                                                  | 2017年4月28日  |

### 表演活動

|             |                 |                                              |            |
| ----------- | --------------- | -------------------------------------------- | ---------- |
| **日期**      | **活動名稱**        | **舉行地點**                                     | **參與成員**   |
| 2013年12月26日 | 第36屆時報華文廣告金像獎   | [台北市NeoStudio展演館](../Page/台北市.md "wikilink") | **Twinko** |
| 2014年10月25日 | Candy Land糖果時尚秀 | [高雄市夢時代廣場](../Page/高雄市.md "wikilink")        |            |
|             |                 |                                              |            |

## 書籍

<table>
<thead>
<tr class="header">
<th><p>書名</p></th>
<th><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></th>
<th><p>頁數</p></th>
<th><p>出版社</p></th>
<th><p>發行日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>王子看見二公主：<br />
幕後紀實看這本就夠了</p></td>
<td><p>ISBN 9789866763618</p></td>
<td><p>192</p></td>
<td><p>捷徑文化</p></td>
<td><p>2008年12月30日</p></td>
</tr>
</tbody>
</table>

## 代言

### 廣告代言

  - 2007年
      - [7-ELEVEN
        迪士尼星光大道](http://www.7-11.com.tw/event/07disney/index.asp)
      - [封魔獵人Bright
        Shadow](https://tw.event.gamania.com/BSEvent/BSNet/20070913_flash/index.htm)
  - 2013年
      - [Latteya Coffee](../Page/Latteya_Coffee.md "wikilink")
        （與[星潼](../Page/林星潼.md "wikilink")、[可青](../Page/曾可青.md "wikilink")、[斯亞和](../Page/陳斯亞.md "wikilink")[泫](../Page/篠崎泫.md "wikilink")，也正式宣布唱跳團體[Twinko成軍](../Page/TWINKO.md "wikilink")。）
      - [黑幫Online](../Page/黑幫Online.md "wikilink")
        （與[星潼](../Page/林星潼.md "wikilink")、[可青](../Page/曾可青.md "wikilink")、[斯亞和](../Page/陳斯亞.md "wikilink")[泫](../Page/篠崎泫.md "wikilink")）

### 活動代言

  - [苗栗黃金台六觀光節](../Page/苗栗縣.md "wikilink")(2007/09/02)
  - [遊戲橘子封魔獵人線上遊戲](../Page/遊戲橘子.md "wikilink")(2007)

## 獎項

  - [第一屆《超級星光大道》第十一名](../Page/超級星光大道_\(第一屆\).md "wikilink")
  - [第一屆《超級星光大道》最佳曖昧獎](../Page/超級星光大道_\(第一屆\).md "wikilink")（與[林宥嘉](../Page/林宥嘉.md "wikilink")）

## 參考資料

## 外部連結

  -
  -
[S](../Category/台灣女歌手.md "wikilink")
[S](../Category/台灣电视女演員.md "wikilink")
[S徐](../Category/台灣電影女演員.md "wikilink")
[S徐](../Category/台灣華語流行音樂歌手.md "wikilink")
[S徐](../Category/台灣綜藝節目主持人.md "wikilink")
[S徐](../Category/我愛黑澀會.md "wikilink")
[S徐](../Category/星光幫.md "wikilink")
[S徐](../Category/世新大學校友.md "wikilink")
[Category:達人女子高級中學校友](../Category/達人女子高級中學校友.md "wikilink")
[S徐](../Category/內湖人.md "wikilink") [K凱](../Category/徐姓.md "wikilink")
[S徐](../Category/Twinko.md "wikilink")

1.  <http://ent.ltn.com.tw/m/news/paper/488206> 徐亦晴23歲生日禮 後照鏡毀壞,
    2011-04-29,陳慧玲, 自由時報
2.  <https://star.ettoday.net/news/906705> 西門電視牆10分鐘播1次！鐵粉砸錢慶《黑澀會》鴨子30歲,
    2017年4月18日, 關韶文, ettoday
    <https://star.ettoday.net/amp/amp_news.php?news_id=906705>
3.  <https://star.ettoday.net/news/918920> 專訪／從黑澀會到星光..徐凱希低潮「我放棄唱歌了！」,
    2017年5月6日, 關韶文, ettoday
4.  [NOWnews【娛樂新聞】：閃亮兩顆星
    星光幫捧紅林宥嘉、徐凱希(2007/05/31)](http://www.nownews.com/2007/05/31/37-2105125.htm)