《**周易叄同契**》又名《**叄同契**》，是一本講[炼丹术著作](../Page/炼丹术.md "wikilink")，被称为“万古丹经王”。作者是[东汉的](../Page/东汉.md "wikilink")[魏伯阳](../Page/魏伯阳.md "wikilink")。《周易叄同契》的书名中“叄”为“三(sān)”《周易三同契》，指[周易](../Page/周易.md "wikilink")、[黄老](../Page/黄老.md "wikilink")、[炉火三事](../Page/炼丹.md "wikilink")。

## 出書時期

《周易参同契》最早出现在[东汉末期](../Page/东汉.md "wikilink")。作者魏伯阳的事迹在正史中并没有记载，大致生活在[汉桓帝](../Page/汉桓帝.md "wikilink")（公元147年
- 167年）时期。

《周易参同契》托名[阴长生注本序](../Page/阴长生.md "wikilink")-{云}-∶“盖闻《参同契》者，昔是古《龙虎上经》，本徐真人。徐真人，[青州从事](../Page/青州.md "wikilink")，北海（今山东[昌乐](../Page/昌乐.md "wikilink")）人也。后因越[上虞人](../Page/上虞.md "wikilink")[魏伯阳](../Page/魏伯阳.md "wikilink")，造《五相类》以解前篇，遂改为《参同契》。更有淳于叔通,
补续其类……叔通亲事徐君，习此经。”

## 內容佈局

全书分为上、中、下三篇，以及《周易参同契鼎器歌》一首，共约6000字，基本是用四字一句、五字一句的[韵文体及少数长短不齐的](../Page/韵文.md "wikilink")[散文体和](../Page/散文.md "wikilink")[离骚体写成的](../Page/离骚.md "wikilink")。全书用周易爻象来论述[炼丹成仙的方法](../Page/炼丹.md "wikilink")。被道教的[外丹和](../Page/外丹.md "wikilink")[内丹派都视为重要的著作](../Page/内丹.md "wikilink")。书中用[炼丹](../Page/炼丹.md "wikilink")[术语来介绍修炼方法](../Page/术语.md "wikilink")，使得本书非常晦涩难懂，而书的文风也对后世的丹书有很大影响。

### 理論濫觴

《周易参同契》指出，物质变化是自然界的普遍规律，[炼丹过程正如以檗染黄](../Page/炼丹.md "wikilink")，煮皮革为胶，用[曲蘖作](../Page/曲蘖.md "wikilink")[酒等等一样](../Page/酒.md "wikilink")，是“自然之所为”，“非有邪伪道”。他还将[阴阳](../Page/阴阳.md "wikilink")[五行学说用于解释炼丹术现象](../Page/五行.md "wikilink")，认为万物的产生和变化都是“五行错王，相据以生”，是阴阳相须，彼此交媾，使精气得以舒发的结果。魏伯阳认为修丹与天地造化是同一个道理，易道与丹道是相通的，所以能用《周易》的道理来解释炼丹的道理，体现了炼丹家的哲学思维。

### 修煉之路

在道教的領域中，《周易叁同契》的丹道修煉是以乾坤為鼎爐，以坎離為藥物，和《[黃庭內景經](../Page/黃庭內景經.md "wikilink")》以黃庭為鼎爐不同。修煉過程中，學人要保持和具備大易中庸和平、慈祥寬恕、體貼瞭解、周流無滯的性情與智慧。穿透種種的比喻，經過「乾坤萬年依舊在，六爻游盡一場空」的神會，終於領悟到天人同一之真理，親見「太陽流珠」的無限金華，達到宗教真理追求的目標。\[1\]

## 注解

### 外丹

  - (唐) 無名氏，《陰真君周易參同契》
  - (唐) 無名氏，《周易參同契注》
  - (宋) 盧天驥，《參同契五相類秘要》

### 內丹

  - (後蜀) [彭曉](../Page/彭曉.md "wikilink")，《周易參同契分章通真義》（流通最廣）
  - (宋) 無名氏，《周易參同契注》
  - (宋) 儲華谷，《周易參同契注》
  - (宋) 陳顯微，《周易參同契解》
  - (宋) [朱熹](../Page/朱熹.md "wikilink")，《周易參同契考異》
  - (元) 俞琰，《周易參同契發揮》
  - (元) [陳致虛](../Page/陳致虛.md "wikilink")，《周易參同契分章注》
  - (明) [陸西星](../Page/陸西星.md "wikilink")，《周易參同契測疏》，《周易參同契口義》
  - (明) 董德寧，《周易參同契正義》
  - (明) 蔣一彪，《古文周易參同契集解》（採「石函古本」，實即杜一誠改編本）
  - (清) [仇兆鰲](../Page/仇兆鰲.md "wikilink")，《古本周易参同契集註》（採「石函古本」，實即杜一誠改編本）
  - (清) 朱元育，《參同契闡幽》
  - (清) [劉一明](../Page/劉一明.md "wikilink")，《參同契經文直指》，《參同契直指箋注》，《參同契直指三相類》

## 參見

  - [太乙金華宗旨](../Page/太乙金華宗旨.md "wikilink")
  - [化書](../Page/化書.md "wikilink")

## 参考文献

### 引用

### 来源

  - Торчинов Е.А. \<Даосизм.(道家思想)\> СПб., 1998, с. 105 – 110;
  - Wu Lu-Chiang and Tenney L. Davis. (1932). "[An Ancient Chinese
    Treatise on Alchemy Entitled Ts'an T'ung
    Ch'i](http://www.jstor.org/stable/224531)", *Isis* 18.2:210-289;
  - [南懷瑾](../Page/南懷瑾.md "wikilink")：《我說〈參同契〉》"(上中下3冊),東方出版社,[北京](../Page/北京.md "wikilink"),2010年4月.
    ISBN 978-7-5060-3499-9
  - Pregadio, Fabrizio. 2011. [*The Seal of the Unity of the Three: A
    Study and Translation of the Cantong qi, the Source of the Taoist
    Way of the Golden
    Elixir*](http://www.goldenelixir.com/press/trl_02_ctq.html).
    Mountain View: Golden Elixir Press.

## 研究書目

  - 欽偉剛：《朱熹與《參同契》文本》（成都：巴蜀書社，2004）。
  - 钦伟刚：〈[南宋初期《参同契》文献实态的考察（上）](http://www.nssd.org/articles/article_read.aspx?id=9141908)〉。
  - 钦伟刚：〈[南宋初期《参同契》文献实态的考察（下）](http://www.nssd.org/articles/article_read.aspx?id=9820541)〉。
  - 钦伟刚：〈[朱熹删改《参同契》经文考](http://www.nssd.org/articles/article_read.aspx?id=15825682)〉。

## 外部链接

  - [《周易参同契》全文](http://www.mypcera.com/book/gu/yu/027.htm)

[Category:子部道家類](../Category/子部道家類.md "wikilink")
[Category:道经](../Category/道经.md "wikilink")
[Category:气功](../Category/气功.md "wikilink")

1.  董德寧：《周易參同契正義》，自由出版社.