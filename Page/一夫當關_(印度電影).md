**一夫當關**（**''Main Hoon Na**''，[印地語](../Page/印地語.md "wikilink")：**मैं हूँ
ना**, [烏爾都語](../Page/烏爾都語.md "wikilink")：**میں ہوں نہ**
）是2004年的[印度電影](../Page/印度.md "wikilink")，是一部傳統的音樂舞蹈片。由[花拉·罕導演](../Page/花拉·罕.md "wikilink")、[葛莉·罕](../Page/葛莉·罕.md "wikilink")（沙·茹克·罕之妻）監製，印度著名演員[沙·茹克·罕領銜主演](../Page/沙·茹克·罕.md "wikilink")，另外[苏丝米塔·森也參加演出](../Page/苏丝米塔·森.md "wikilink")，該影片在當年獲得成功。

## 故事梗概

印度軍方打算進行「友誼行動」，即通過釋放以往誤越國境的巴基斯坦邊民以便取得對方信任而改善兩國關係。

前印軍上尉拉格萬（*Raghavan*）堅決反對此計劃，因為他在[印巴戰爭中曾失去兒子和父親](../Page/印巴戰爭.md "wikilink")。他試圖通過[恐怖手段綁架或殺死巴克什將軍](../Page/恐怖.md "wikilink")（*General
Bakshi*）的家人以達到阻止「友誼行動」的目的。

蘭姆·沙馬少將（*Major Ram Prasad
Sharma*）接受了保護將軍女兒珊佳娜的任務。他假扮作學生，留在珊佳娜的班裡面，在臥底的過程中，他又查到了同班同學拉克什曼就是他失散多年的同父異母的弟弟，而且，一場學年晚會之后，他還同化學老師相愛了。

最後，恐怖分子出擊，蘭姆說一聲「有我在此」就孤身一人進入教學大樓同他們進行生死決斗，影片在手榴彈大爆炸掀起的高潮氣氛中結束。。。

## 得獎

  - *Main Hoon
    Na*獲得《[印度電影觀眾獎](../Page/印度電影觀眾獎.md "wikilink")》最佳影片獎、最佳演員獎等多項提名。然而最終僅由[Anu
    Malik獲得](../Page/Anu_Malik.md "wikilink")[最佳音樂導演獎](../Page/印度電影觀眾－最佳音樂導演獎.md "wikilink")。

<!-- end list -->

  - 2005年獲得[全球印度電影節獎](../Page/全球印度電影節獎.md "wikilink") -（Global Indian
    Film Awards - GIFA）最佳影片獎 - 得主為：[Main Hoon
    Naa的電影片廠](../Page/Main_Hoon_Naa.md "wikilink")[紅辣椒娛樂公司](../Page/紅辣椒娛樂公司.md "wikilink")（[Red
    Chillies
    Entertainment](../Page/Red_Chillies_Entertainment.md "wikilink")）。

## 卡士表

  - [沙·茹克·罕](../Page/沙·茹克·罕.md "wikilink") ... [Shahrukh
    Khan](../Page/Shahrukh_Khan.md "wikilink") ... 扮演蘭姆·沙馬少將（Major Ram
    Prasad Sharma）
  - [蘇絲美黛·森](../Page/蘇絲美黛·森.md "wikilink") ... [Sushmita
    Sen](../Page/Sushmita_Sen.md "wikilink") ... 扮演化學教師蟬荻妮（Miss Chandni）
  - [扎伊德·罕](../Page/扎伊德·罕.md "wikilink") ... [Zayed
    Khan](../Page/Zayed_Khan.md "wikilink") ... 扮演拉克什曼·沙馬（Lakshman
    Prasad Sharma a.k.a Lucky）
  - [阿米黛·勞](../Page/阿米黛·勞.md "wikilink") ... [Amrita
    Rao](../Page/Amrita_Rao.md "wikilink") ... 扮演將軍女兒珊佳娜·巴科什（Sanjana
    Bakshi）
  - [Sunil Shetty](../Page/Sunil_Shetty.md "wikilink") ...
    扮演叛軍首領拉嘎萬（Raghavan）
  - [Kabir Bedi](../Page/Kabir_Bedi.md "wikilink") ... 扮演巴科什將軍（General
    Bakshi）
  - [Naseeruddin Shah](../Page/Naseeruddin_Shah.md "wikilink") ...
    扮演沙馬旅長（Brigadier Shekhar Sharma）
  - [Kiron Kher](../Page/Kiron_Kher.md "wikilink") ... 扮演沙馬太太（Mrs.
    Sharma）
  - [Bindu](../Page/Bindu_\(actress\).md "wikilink") ... Mrs. Kakkad
  - [Boman Irani](../Page/Boman_Irani.md "wikilink") ... The Principal
  - [Satish Shah](../Page/Satish_Shah.md "wikilink") ... Professor Rasai
  - [Rakhi Sawant](../Page/Rakhi_Sawant.md "wikilink") ....
    扮演學院女生迷你（Mini）

## 電影歌曲

影片共有六首歌曲，全部由：[Javed
Akhtar作詞](../Page/Javed_Akhtar.md "wikilink")，並由[花拉·罕](../Page/花拉·罕.md "wikilink")（[Farah
Khan](../Page/花拉·罕.md "wikilink")）導演親自編排舞蹈及音樂導演。

  - 主題曲：''Main Hoon Na ''
  - *Tumse Milke Dil Ka Jo Haal*
  - *Tumhe Jo Maine Dekha*
  - *Gori Gori* - [Shreya
    Ghosal](../Page/Shreya_Ghosal.md "wikilink")，[Sunidhi
    Chauhan](../Page/Sunidhi_Chauhan.md "wikilink")
  - *Chale Jaise Hawaien*- [Kay
    Kay](../Page/Kay_Kay.md "wikilink")，[Vasundhara
    Das](../Page/Vasundhara_Das.md "wikilink")
  - *Ye Fizaein* - ''[Alka
    Yagnik](../Page/Alka_Yagnik.md "wikilink")，[Kay
    Kay](../Page/Kay_Kay.md "wikilink")

[該電影的歌詞－Traduction des
chansons](https://web.archive.org/web/20071209194101/http://www.fantastikasia.net/bollywood-lyrics.php3?id_article=752&id_rubrique=26)

## 外部連結

  -
[Category:印度電影作品](../Category/印度電影作品.md "wikilink")
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:恐怖活動電影](../Category/恐怖活動電影.md "wikilink")