**王偉樑**是[香港人](../Page/香港人.md "wikilink")，是[香港前](../Page/香港.md "wikilink")[無線電視的](../Page/無線電視.md "wikilink")[藝人](../Page/藝人.md "wikilink")，在1990年代初期活躍於無綫連續劇，主要參演配角角色。一般[時裝片他都是飾演一些](../Page/時裝片.md "wikilink")「變態佬」、[精神病](../Page/精神病.md "wikilink")[患者](../Page/患者.md "wikilink")、[癮君子或其他小角色](../Page/癮君子.md "wikilink")，但是古裝片卻又常演出[武術高手](../Page/武術高手.md "wikilink")、善用[謀略的智者角色](../Page/謀略.md "wikilink")、醫術高明的[中醫等](../Page/中醫.md "wikilink")。由於幾乎每部連續劇都有他的份兒，所以在無綫算是演出無數。

王偉樑在1995年電視劇《[情濃大地](../Page/情濃大地.md "wikilink")》飾大包米頗受好評，其後演出很多甘草角色，後來一直到1997年出現代表作是：劇集《[醉打金枝](../Page/醉打金枝.md "wikilink")》中[郭曖](../Page/郭曖.md "wikilink")([歐陽震華飾](../Page/歐陽震華.md "wikilink"))長樂坊的好兄弟華十八（長樂坊五虎之一，[華陀傳人的醫術高手](../Page/華陀.md "wikilink")，結局被冊封為[大唐](../Page/大唐.md "wikilink")[御醫](../Page/御醫.md "wikilink")），也是從影多年來，戲份最多的一部。其後還演出[狀王宋世傑II](../Page/狀王宋世傑II.md "wikilink")、[寵物情緣](../Page/寵物情緣.md "wikilink")、[倚天屠龍記等戲劇](../Page/倚天屠龍記.md "wikilink")，到了2001年因為之前的[醉打金枝的長樂坊五虎的角色大受歡迎](../Page/醉打金枝.md "wikilink")，所以在[金裝四大才子中](../Page/金裝四大才子.md "wikilink")，又再次出現。

他曾於1993年獲周星馳邀請參演電影《[唐伯虎點秋香](../Page/唐伯虎點秋香.md "wikilink")》中「四大淫俠」之一。

王偉樑在香港[無線電視服務了](../Page/無線電視.md "wikilink")20餘年後離開娛樂圈。

## 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

### 1989年

  - [他來自江湖](../Page/他來自江湖.md "wikilink") 飾 汽車經紀（第2集）／文忠信手下（第28-30集）

### 1990年

  - [零點出擊](../Page/零點出擊.md "wikilink") 飾 警察學堂學員
  - [人在邊緣](../Page/人在邊緣.md "wikilink") 飾 囚犯

### 1991年

  - [卡拉屋企](../Page/卡拉屋企.md "wikilink") 飾 朱仔
  - [今生無悔](../Page/今生無悔.md "wikilink") 飾 阿　忠

### 1992年

  - [火玫瑰](../Page/火玫瑰.md "wikilink") 飾 歐陽海鋒
  - [壹號皇庭](../Page/壹號皇庭.md "wikilink") 飾 周漢
  - [大時代](../Page/大時代.md "wikilink") 飾 志（第39集）
  - [重案傳真](../Page/重案傳真.md "wikilink") 飾 油脂

### 1993年

  - [壹號皇庭II](../Page/壹號皇庭II.md "wikilink") 飾 化驗師
  - [龍兄鼠弟](../Page/龍兄鼠弟_\(電視劇\).md "wikilink") 飾
    [耀](../Page/耀.md "wikilink")

### 1994年

  - [射鵰英雄傳之南帝北丐](../Page/射鵰英雄傳之南帝北丐.md "wikilink") 飾 擄劉瑛者 （於1993年海外發行）
  - [獨臂刀客](../Page/獨臂刀客.md "wikilink") 飾 右使
  - [新重案傳真](../Page/新重案傳真.md "wikilink") 飾 阿昌
  - [金毛獅王](../Page/金毛獅王_\(電視劇\).md "wikilink") 飾 明教弟子/探子 （於1993年海外發行）

### 1995年

  - [刑事偵緝檔案](../Page/刑事偵緝檔案.md "wikilink") 飾 傳知雜誌社員工
  - [情濃大地](../Page/情濃大地.md "wikilink") 飾 關大米（大包米） （於1994年海外發行）
  - [O記實錄](../Page/O記實錄.md "wikilink") 飾 大昏迷
  - [真情](../Page/真情.md "wikilink") 飾 桂州人

### 1996年

  - [聊齋](../Page/聊齋_\(無線電視劇\).md "wikilink")
    [俠女田郎](../Page/俠女田郎.md "wikilink") 飾 皇帝
  - [聊齋](../Page/聊齋_\(無線電視劇\).md "wikilink")
    [秋月還陽](../Page/秋月還陽.md "wikilink") 飾 鬼差
  - [孽吻](../Page/孽吻.md "wikilink") 飾 周　威
  - [O記實錄II](../Page/O記實錄II.md "wikilink") 飾 馬志豪
  - [笑傲江湖](../Page/笑傲江湖.md "wikilink") 飾 一字電劍 丁堅

### 1997年

  - [鑑證實錄](../Page/鑑證實錄.md "wikilink") 飾 陳廣文（小提琴教師）
  - [狀王宋世傑](../Page/狀王宋世傑.md "wikilink") 飾
    陸[狀師](../Page/狀師.md "wikilink")
  - [皇家反千組](../Page/皇家反千組.md "wikilink") 飾 伍宗強 （於1996年海外發行）
  - [醉打金枝](../Page/醉打金枝.md "wikilink") 飾
    [華十八](../Page/華十八.md "wikilink")（[郭曖](../Page/郭曖.md "wikilink")（[歐陽震華飾](../Page/歐陽震華.md "wikilink")）長樂坊的好兄弟）
  - [天龍八部](../Page/天龍八部.md "wikilink") 飾 朱丹臣
  - [美味天王](../Page/美味天王.md "wikilink") 飾 司　儀

### 1998年

  - [陀槍師姐](../Page/陀槍師姐.md "wikilink") 飾 阿　郎
  - [烈火雄心](../Page/烈火雄心_\(電視劇\).md "wikilink") 飾
    [陳主任](../Page/陳主任.md "wikilink")

### 1999年

  - [雙面伊人](../Page/雙面伊人.md "wikilink") 飾 標叔叔
  - [寵物情緣](../Page/寵物情緣.md "wikilink") 飾 偷渡客
  - [狀王宋世傑II](../Page/狀王宋世傑II.md "wikilink") 飾 小安子
  - [人龍傳說](../Page/人龍傳說.md "wikilink") 飾 初一
  - [楊貴妃](../Page/楊貴妃_\(電視劇\).md "wikilink") 飾 李白
  - [洗冤錄](../Page/洗冤錄.md "wikilink") 飾 程富（第9集）
  - [茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink") 飾 郭少爺(第26集)
  - [創世紀](../Page/創世紀_\(電視劇\).md "wikilink") 飾 酒店經理

### 2000年

  - [陀槍師姐II](../Page/陀槍師姐II.md "wikilink") 飾 曾家寶

<!-- end list -->

  - [金裝四大才子](../Page/金裝四大才子.md "wikilink") 飾 周遠材（周文賓的爺爺）

### 2001年

  - [FM701](../Page/FM701.md "wikilink") 飾 神父
  - [倚天屠龍記](../Page/倚天屠龍記_\(2001年電視劇\).md "wikilink") 飾 絕世高手+六大派高手白垣
    （於2000年海外發行）
  - [封神榜](../Page/封神榜_\(2001年電視劇\).md "wikilink") 飾 鄧九公
  - [皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink") 飾 張三
  - [尋秦記](../Page/尋秦記_\(電視劇\).md "wikilink") 飾
    [趙高](../Page/趙高.md "wikilink")
  - [勇往直前](../Page/勇往直前_\(電視劇\).md "wikilink") 飾
    [林家強](../Page/林家強.md "wikilink")

### 2002年

  - [洛神](../Page/洛神_\(2002年劇集\).md "wikilink") 飾 甄 儼
  - [Loving You我愛你II](../Page/Loving_You我愛你II.md "wikilink") 飾
  - [烈火雄心II](../Page/烈火雄心II.md "wikilink") 飾 王先生（第16集）
  - [皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink") 飾 賣家、傻佬、廖公公
  - [蕭十一郎](../Page/蕭十一郎_\(無綫電視劇\).md "wikilink") 飾
    和尚（於2007年1月25日至2月7日期間在[翡翠台首播](../Page/翡翠台.md "wikilink")）
  - [情牽百子櫃](../Page/情牽百子櫃.md "wikilink") 飾 忠仔 （於2001年海外發行）
  - [戇夫成龍](../Page/戇夫成龍.md "wikilink") 飾 芬芳茶館客人(第14集)
  - [談判專家 (電視劇)](../Page/談判專家_\(電視劇\).md "wikilink") 飾 江永健

### 2003年

  - [洗冤錄II](../Page/洗冤錄II.md "wikilink") 飾 袁日川（第10-11、13集）
  - [帝女花](../Page/帝女花.md "wikilink") 飾 [史可法](../Page/史可法.md "wikilink")
    （於2002年海外發行）
  - [十萬噸情緣](../Page/十萬噸情緣.md "wikilink") 飾 員工 （於2002年海外發行）
  - [智勇新警界](../Page/智勇新警界.md "wikilink") 飾 顛狗華
  - [衝上雲霄](../Page/衝上雲霄.md "wikilink") 飾 阿達（第14-16集）
  - [西關大少](../Page/西關大少.md "wikilink") 飾
    唐展鵬（廣運行[祕書兼周明軒之私人助手](../Page/公司秘書.md "wikilink")）
  - [衛斯理](../Page/衛斯理.md "wikilink") 飾 小偷（屍變單元）
  - [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink") 飾 導演（第120集）

### 2004年

  - [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink") 飾 傻強（第238集）
  - [大唐雙龍傳](../Page/大唐雙龍傳_\(電視劇\).md "wikilink") 飾 虛行之
  - [楚漢驕雄](../Page/楚漢驕雄.md "wikilink") 飾 武
  - [棟篤神探](../Page/棟篤神探.md "wikilink") 飾 陳偉強/道友強（吸毒者）

### 2006年

  - [舞動全城](../Page/舞動全城.md "wikilink") 飾 金　輝

### 2007年

  - [紅衣手記](../Page/紅衣手記.md "wikilink") 飾 徐健康
  - [凶城計中計](../Page/凶城計中計.md "wikilink") 飾 霍天命的同謀（雷標）
  - [天機算](../Page/天機算.md "wikilink") 飾 鼠　漢
  - [寫意人生](../Page/寫意人生.md "wikilink") 飾 羅威（高大威（麥包）的「好友」）
  - [同事三分親](../Page/同事三分親.md "wikilink") 飾
    風之子（第5集）、王　從（王唐子）（第61集）、雷　彪（雷歡父親）（第309集）

### 2008年

  - [少年四大名捕](../Page/少年四大名捕.md "wikilink") 飾 韋驚濤

### 2009年

  - [ID精英](../Page/ID精英.md "wikilink") 飾 李有根
  - [老婆大人II](../Page/老婆大人II.md "wikilink") 飾 鍾大發（第11集：鍾黃美玉之夫）
  - [老友狗狗](../Page/老友狗狗.md "wikilink") 飾 鍾　成（第3集）
  - [巾幗梟雄](../Page/巾幗梟雄.md "wikilink") 飾 康梓榮

### 2010年

  - [施公奇案II](../Page/施公奇案II.md "wikilink") 飾 胡公公（第14集）

### 2011年

  - [七號差館](../Page/七號差館_\(電視劇\).md "wikilink") 飾 李成威

## 電影

  - 1993年：[唐伯虎點秋香](../Page/唐伯虎點秋香.md "wikilink")
  - 2007年：[戲王之王](../Page/戲王之王_\(2007年電影\).md "wikilink")

## 連結

  - [【55个同一演员】2000倚天屠龙记和2004楚汉骄雄](https://web.archive.org/web/20070626155434/http://shenzhifeng.blog.china.com/200703/224333.html)
  - [熟悉的陌生人](http://www.am730.com.hk/article-184805)

[wai](../Category/王姓.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")