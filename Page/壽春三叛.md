**壽春三叛**，又稱**淮南三叛**。事情發生於[曹魏後期](../Page/曹魏.md "wikilink")，由於司馬氏奪權專政，使得掌握軍事重鎮[壽春的統帥先後發生三次反抗司馬氏的兵變](../Page/壽春.md "wikilink")。這三次分別為[王凌之叛](../Page/王凌.md "wikilink")（251年四月）、[毌丘儉](../Page/毌丘儉.md "wikilink")[文欽之叛](../Page/文欽.md "wikilink")（255年正月）及[諸葛誕之叛](../Page/諸葛誕.md "wikilink")（257年五月—258年二月）。三次叛亂皆為司馬氏所平定。

## 經過

### 王凌之叛

249年司馬懿發動了[高平陵之變](../Page/高平陵之变.md "wikilink")，[曹爽被滅族](../Page/曹爽.md "wikilink")，司馬懿於是掌握魏國大權，升任鎮守[壽春的王凌為](../Page/壽春.md "wikilink")[太尉](../Page/太尉.md "wikilink")。[王凌與外甥](../Page/王凌.md "wikilink")[兗州刺史](../Page/兗州.md "wikilink")[令狐愚見魏帝](../Page/令狐愚.md "wikilink")[曹芳年幼無力掌政](../Page/曹芳.md "wikilink")，致令君權旁落在司馬懿之手，於是意圖廢曹芳而改立楚王[曹彪](../Page/曹彪.md "wikilink")，令狐愚更派部將[張式聯絡曹彪](../Page/張式_\(三國\).md "wikilink")，但未及举事，令狐愚就病死了。

251年，王凌見[東吳在涂水有軍事行動](../Page/孫吳.md "wikilink")，於是上書請朝廷准許討伐東吳，打算乘此而起兵反抗司馬懿，此為王凌之叛。但是請求並沒有得到回應，王凌於是派部將[楊弘將廢立計劃告訴新任兗州刺史](../Page/杨弘_\(曹魏\).md "wikilink")[黃華](../Page/黃華_\(三國\).md "wikilink")，希望得到支持，但楊弘和黃華卻向司馬懿告發王凌的圖謀，[司馬懿於是率軍討伐](../Page/司馬懿.md "wikilink")。王凌自知不敵，司馬懿又在發軍時先赦免他的罪，於是投降，司馬懿納降。但後來王凌自知必死无疑，於是在被押解到洛陽的途中自殺。王凌、令狐愚等人被誅滅三族，曹彪亦被賜死。

**參戰人物**

<table>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li>王凌軍
<ul>
<li><a href="../Page/王凌.md" title="wikilink">王凌</a>
<ul>
<li><a href="../Page/令狐愚.md" title="wikilink">令狐愚</a></li>
<li><a href="../Page/張式_(三國).md" title="wikilink">張式</a></li>
</ul></li>
</ul></li>
</ul></td>
<td><ul>
<li>曹魏軍
<ul>
<li><a href="../Page/司馬懿.md" title="wikilink">司馬懿</a>
<ul>
<li><a href="../Page/杨弘_(曹魏).md" title="wikilink">楊弘</a>（向司馬懿揭發王凌）</li>
<li><a href="../Page/黃華_(三國).md" title="wikilink">黃華</a></li>
</ul></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### 毌丘儉文欽之叛

司馬懿死後，兒子[司馬師繼之掌權](../Page/司馬師.md "wikilink")。254年，[李豐與](../Page/李豐.md "wikilink")[夏侯玄及](../Page/夏侯玄.md "wikilink")[張緝等人意圖推翻司馬師](../Page/張緝.md "wikilink")，但計劃敗露，李豐、夏侯玄和張緝等皆被殺。曹芳對李豐等人被殺後深感不平，引起司馬師不滿，於是在數月後強行廢了曹芳而改立[曹髦為帝](../Page/曹髦.md "wikilink")。李豐等人被誅殺和曹芳被廢，令到當時駐守壽春的鎮東將軍[毌丘儉和](../Page/毌丘儉.md "wikilink")[揚州刺史](../Page/揚州.md "wikilink")[文欽非常不安](../Page/文欽.md "wikilink")，害怕會牽連到自己；而毌丘儉兒子[毌丘甸亦勸父親要舉兵保衛曹魏](../Page/毌丘甸.md "wikilink")，二人於是決心要反抗司馬師。

次年正月乙丑日（2月5日），二人在壽春舉兵討伐[司馬師](../Page/司馬師.md "wikilink")，並進兵[項城](../Page/項城.md "wikilink")，此為毌丘儉文欽之叛。東吳知道毌丘儉叛亂後亦由丞相[孫峻率領](../Page/孫峻.md "wikilink")[呂據和](../Page/呂據.md "wikilink")[留贊領兵到壽春支援毌丘儉](../Page/留贊.md "wikilink")。司馬師自己親自率軍討伐，並派遣[荊州刺史](../Page/荊州.md "wikilink")[王基率兵與叛軍對抗](../Page/王基_\(三國\).md "wikilink")，搶先佔領[南頓](../Page/南頓.md "wikilink")。及後諸葛誕、[胡遵和](../Page/胡遵.md "wikilink")[鄧艾都領軍與司馬師會合](../Page/鄧艾.md "wikilink")。司馬師命令諸軍不能進攻，毌丘儉和文欽不能進攻，又怕撤退時壽春被襲，軍中的淮南將士因家屬都在北方，軍心於是潰散，只有新歸附的農民仍然效命。此時鄧艾駐屯[樂嘉](../Page/樂嘉.md "wikilink")，毌丘儉見鄧艾兵弱，於是派文欽攻擊，但文欽到後卻發現司馬師率領大軍到來，於是撤退。司馬師派左[長史](../Page/長史.md "wikilink")[司馬班追擊](../Page/司馬班.md "wikilink")，文欽軍敗退，因其子[文鴦奮戰才得以全身而退](../Page/文鴦.md "wikilink")；此時殿中人[尹大目追出試圖勸降文欽](../Page/尹大目.md "wikilink")，但遭文欽拒絕。毌丘儉知道文欽敗退後乘夜逃走，餘眾於是崩潰，毌丘儉逃到慎縣被平民[張屬射殺](../Page/張屬.md "wikilink")，梟首被送到[洛陽](../Page/洛陽.md "wikilink")。\[1\]文欽回到項縣時大軍已潰散，壽春又被諸葛誕佔領，於是逃亡到東吳。孫峻到[東興時知道諸葛誕已佔領壽春](../Page/東興.md "wikilink")，於是退兵；諸葛誕派部將[蔣班追擊](../Page/蔣班.md "wikilink")，斬殺留贊。

**參戰人物**

<table>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li>毌丘儉軍
<ul>
<li><a href="../Page/毌丘儉.md" title="wikilink">毌丘儉</a>（被殺）
<ul>
<li><a href="../Page/毌丘秀.md" title="wikilink">毌丘秀</a>（投奔東吳）</li>
<li><a href="../Page/毌丘重.md" title="wikilink">毌丘重</a>（投奔東吳）</li>
</ul></li>
<li><a href="../Page/文欽.md" title="wikilink">文欽</a>（投奔東吳）
<ul>
<li><a href="../Page/文鴦.md" title="wikilink">文鴦</a>（投奔東吳）</li>
</ul></li>
<li><a href="../Page/鄭翼.md" title="wikilink">鄭翼</a>（不詳）</li>
<li><a href="../Page/呂宣.md" title="wikilink">呂宣</a>（不詳）</li>
<li><a href="../Page/張休.md" title="wikilink">張休</a>（不詳）</li>
<li><a href="../Page/丁尊.md" title="wikilink">丁尊</a>（不詳）</li>
<li><a href="../Page/王休.md" title="wikilink">王休</a>（不詳）</li>
</ul></li>
<li>東吳軍
<ul>
<li><a href="../Page/孫峻.md" title="wikilink">孫峻</a>
<ul>
<li><a href="../Page/呂據.md" title="wikilink">呂據</a></li>
<li><a href="../Page/留贊.md" title="wikilink">留贊</a>（戰死）</li>
</ul></li>
</ul></li>
</ul></td>
<td><ul>
<li>曹魏軍
<ul>
<li><a href="../Page/司馬師.md" title="wikilink">司馬師</a>
<ul>
<li><a href="../Page/司馬班.md" title="wikilink">司馬班</a></li>
<li><a href="../Page/尹大目.md" title="wikilink">尹大目</a></li>
<li><a href="../Page/鄧艾.md" title="wikilink">鄧艾</a></li>
<li><a href="../Page/王基.md" title="wikilink">王基</a></li>
<li><a href="../Page/諸葛誕.md" title="wikilink">諸葛誕</a>
<ul>
<li><a href="../Page/蔣班.md" title="wikilink">蔣班</a></li>
</ul></li>
<li><a href="../Page/胡遵.md" title="wikilink">胡遵</a></li>
</ul></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### 諸葛誕之叛

毌丘儉文欽之叛被平定後不久，司馬師病逝，由弟弟司馬昭接手掌權。征東大將軍諸葛誕見好友夏侯玄、早前在壽春叛變的王凌和毌丘儉皆相繼被誅殺，十分不安，於是一方面在淮南籠絡人心，一方面又蓄養死士以作自保。[司馬昭為了鏟除支持曹魏的勢力](../Page/司馬昭.md "wikilink")，聽從[賈充之言逼反](../Page/賈充.md "wikilink")[諸葛誕](../Page/諸葛誕.md "wikilink")，徵召他入朝為[司空](../Page/司空.md "wikilink")。諸葛誕接到詔令後十分恐懼，害怕一到[朝廷就被](../Page/朝廷.md "wikilink")[斬殺](../Page/斬殺.md "wikilink")，於是殺揚州刺史[樂綝](../Page/樂綝.md "wikilink")，據守壽春，起兵反抗司馬昭，又派長史[吳綱帶兒子](../Page/吳綱_\(曹魏\).md "wikilink")[諸葛靚和牙門子弟到東吳當人質](../Page/諸葛靚.md "wikilink")，請求援兵；此為諸葛誕之叛。

司馬昭率二十六萬兵討伐諸葛誕，駐屯[丘頭](../Page/丘頭.md "wikilink")，並派鎮東將軍王基及安東將軍[陳騫包圍壽春](../Page/陳騫.md "wikilink")，派[石苞](../Page/石苞.md "wikilink")、[胡質及](../Page/胡質.md "wikilink")[州泰領兵抵抗吳兵](../Page/州泰.md "wikilink")。東吳援軍[文欽](../Page/文欽.md "wikilink")、[唐咨和](../Page/唐咨.md "wikilink")[全懌等趁包圍圈未成突入壽春城](../Page/全懌.md "wikilink")，但其後王基建立起堅固的包圍圈，文欽等數次突圍也不能攻破。同時吳將[朱異率軍屯壽春西南的](../Page/朱異.md "wikilink")[安豐作外援](../Page/安豐.md "wikilink")，被兗州刺史[州泰擊破](../Page/州泰.md "wikilink")。[孫綝率軍屯](../Page/孫綝.md "wikilink")[鑊里](../Page/鑊里.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")[巢湖市境](../Page/巢湖市.md "wikilink")），派朱異率領[丁奉和](../Page/丁奉.md "wikilink")[黎斐等再前往壽春解圍](../Page/黎斐.md "wikilink")，但被石苞和州泰擊敗，[太山太守](../Page/泰山郡.md "wikilink")[胡烈更出奇兵盡焚吳軍糧秣](../Page/胡烈.md "wikilink")。朱異因糧秣已失，拒絕再進攻。孫綝大怒，斬殺朱異，但因無力再戰，被迫退還[建業](../Page/南京市.md "wikilink")。被圍困的壽春久久沒有等到援兵，諸葛誕部將[蔣班和](../Page/蔣班.md "wikilink")[焦彝勸諸葛誕率主力專攻一方以求突圍](../Page/焦彝.md "wikilink")，不宜困守壽春；但文欽認為東吳救兵必至，勸諸葛誕固守。諸葛誕不聽蔣班的建議，更意圖殺掉二人，二人於是逃出城投降曹軍。後來，司馬昭接納鍾會的計謀，偽造剛剛投降曹魏的[全輝和](../Page/全輝.md "wikilink")[全儀的誘降書信](../Page/全儀.md "wikilink")，又派二人的親信將書信交給吳將[全禕和](../Page/全禕.md "wikilink")[全端等人](../Page/全端.md "wikilink")。全褘等收到書信後，果然率眾向曹魏投降，此舉令壽春城民大驚。

次年正月壬寅日（257年3月3日），[諸葛誕和文欽及唐咨等突圍但失敗](../Page/諸葛誕.md "wikilink")，死傷枕藉，唯有撤回城內。而城內糧食已經接近枯竭，已有數萬人出降，文欽亦意圖盡釋城中的北方人，僅以吳兵據守以減省糧食消耗，諸葛誕不聽，更加因忌恨文欽而將他殺害，文欽子[文鴦和](../Page/文鴦.md "wikilink")[文虎知道文欽被殺](../Page/文虎.md "wikilink")，於是投降司馬昭。文鴦二人獲封賞更令壽春的兵民喪失戰意，最終司馬昭在二月攻克壽春，諸葛誕兵敗出城逃亡，被[胡奮部下士兵擊殺](../Page/胡奮.md "wikilink")；吳將[于诠亦力戰而死](../Page/于诠.md "wikilink")；唐咨和[王祚則投降](../Page/王祚.md "wikilink")。

**參戰人物**

<table>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li>諸葛誕軍
<ul>
<li><a href="../Page/諸葛誕.md" title="wikilink">諸葛誕</a> （戰死）
<ul>
<li><a href="../Page/蔣班.md" title="wikilink">蔣班</a>（投降）</li>
<li><a href="../Page/焦彝.md" title="wikilink">焦彝</a>（投降）</li>
</ul></li>
</ul></li>
<li>東吳軍
<ul>
<li><a href="../Page/孫綝.md" title="wikilink">孫綝</a>
<ul>
<li><a href="../Page/朱異.md" title="wikilink">朱異</a>（被孫綝所殺）</li>
<li><a href="../Page/丁奉.md" title="wikilink">丁奉</a></li>
<li><a href="../Page/黎斐.md" title="wikilink">黎斐</a></li>
<li><a href="../Page/文欽.md" title="wikilink">文欽</a>（被諸葛誕所殺）
<ul>
<li><a href="../Page/文鴦.md" title="wikilink">文鴦</a>（投降）</li>
<li><a href="../Page/文虎_(三國).md" title="wikilink">文虎</a>（投降）</li>
</ul></li>
<li><a href="../Page/全懌.md" title="wikilink">全懌</a>（投降）</li>
<li><a href="../Page/全端.md" title="wikilink">全端</a>（投降）</li>
<li><a href="../Page/唐咨.md" title="wikilink">唐咨</a>（投降）</li>
<li><a href="../Page/王祚.md" title="wikilink">王祚</a>（投降）</li>
<li><a href="../Page/于诠.md" title="wikilink">于诠</a>（戰死）</li>
</ul></li>
</ul></li>
</ul></td>
<td><ul>
<li>曹魏軍
<ul>
<li><a href="../Page/司馬昭.md" title="wikilink">司馬昭</a>
<ul>
<li><a href="../Page/鍾會.md" title="wikilink">鍾會</a></li>
<li><a href="../Page/王基_(三國).md" title="wikilink">王基</a></li>
<li><a href="../Page/陳鶱.md" title="wikilink">陳鶱</a></li>
<li><a href="../Page/石苞.md" title="wikilink">石苞</a></li>
<li><a href="../Page/胡質.md" title="wikilink">胡質</a></li>
<li><a href="../Page/州泰.md" title="wikilink">州泰</a></li>
<li><a href="../Page/胡烈.md" title="wikilink">胡烈</a></li>
<li><a href="../Page/胡奮.md" title="wikilink">胡奮</a></li>
</ul></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 影響

這三次兵變的失敗，使司馬氏成功鏟除擁護魏帝的勢力。此後朝廷上很少有實力派支持魏帝，士大夫紛紛擁護司馬氏，[司馬昭成功走向篡位大道](../Page/司馬昭.md "wikilink")。260年發生[甘露之變](../Page/甘露之變_\(三國\).md "wikilink")，[曹髦不甘被控制](../Page/曹髦.md "wikilink")\[2\]，率臣反抗[司馬昭](../Page/司馬昭.md "wikilink")，最後被司馬昭、[賈充及](../Page/賈充.md "wikilink")[成濟所殺](../Page/成济.md "wikilink")。265年，司馬昭死後不久，其子[司馬炎即篡魏稱帝](../Page/司馬炎.md "wikilink")，建立[西晉](../Page/西晉.md "wikilink")，[曹魏滅亡](../Page/曹魏.md "wikilink")。

## 參見

  - [三国](../Page/三国.md "wikilink")
  - [高平陵之变](../Page/高平陵之变.md "wikilink")

## 附註

<references />

## 參考資料

  - 《[三國志](../Page/三國志.md "wikilink")·魏書·三少帝紀》
  - 《三國志·魏書·王毌丘諸葛鄧鍾傳》
  - 《[資治通鑑](../Page/資治通鑑.md "wikilink")》

[Category:曹魏战役](../Category/曹魏战役.md "wikilink")
[Category:250年代中国军事](../Category/250年代中国军事.md "wikilink")
[Category:安徽历次战争与战役](../Category/安徽历次战争与战役.md "wikilink")
[Category:中國叛亂](../Category/中國叛亂.md "wikilink")
[SCSP](../Category/勤王.md "wikilink")
[Category:251年](../Category/251年.md "wikilink")
[Category:255年](../Category/255年.md "wikilink")
[Category:257年](../Category/257年.md "wikilink")
[Category:258年](../Category/258年.md "wikilink")

1.  《三國志·魏書·高貴鄉公紀》：（閏月）甲辰（3月16日），（安風淮津）〔安風津〕都尉斬儉，傳首京都。
2.  曹髦見王沈、王經、王業等人，憤慨說道：「司馬昭之心，路人皆知也！吾不能坐受廢辱，今日當與卿等自出討之。」