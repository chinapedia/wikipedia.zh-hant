**大衛·休伯爾**（，），加拿大-美籍[神經科學家](../Page/神經科學.md "wikilink")，生前任[哈佛大学](../Page/哈佛大学.md "wikilink")[神经生物学教授](../Page/神经生物学.md "wikilink")，與合作者[托斯坦·威泽尔](../Page/托斯坦·威泽尔.md "wikilink")（Torsten
N.
Wiesel）由於對[視覺系統中](../Page/視覺系統.md "wikilink")[视觉信息处理的研究的贡献](../Page/视觉.md "wikilink")，而與另一團隊的科學家[羅傑·斯佩里](../Page/羅傑·斯佩里.md "wikilink")（Roger
W. Sperry）共同獲得1981年[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")。
1978年，休伯尔获得[哥伦比亚大学授予的](../Page/哥伦比亚.md "wikilink")[路易莎·格罗斯·霍维茨奖](../Page/路易莎·格罗斯·霍维茨奖.md "wikilink")。

## 生平

大衛·休伯爾1926年生於[安大略省](../Page/安大略省.md "wikilink")[溫莎市](../Page/溫莎_\(安大略省\).md "wikilink")，父母親是從美國移居加拿大，祖父輩則是從德國[巴伐利亞](../Page/巴伐利亞.md "wikilink")[訥德林根來的移民](../Page/訥德林根.md "wikilink")。1929年時，休伯尔全家搬到[蒙特婁居住](../Page/蒙特婁.md "wikilink")，並在當地渡過童年。休伯尔于1953年和Ruth
Izzard结婚，他们一共育有三个儿子，四个孙辈\[1\]。Ruth
Izzard在2013年2月去世\[2\]，休伯尔则于2013年9月22日在美国[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[林肯镇由于肾衰竭去世](../Page/林肯镇.md "wikilink")，享年87岁\[3\]。

## 科研生涯

休伯尔在年轻时就讀[麥吉爾大學時主修](../Page/麥吉爾大學.md "wikilink")[數學與](../Page/數學.md "wikilink")[物理](../Page/物理.md "wikilink")，随后修[医学](../Page/医学.md "wikilink")，獲得學位後前往美國發展。1954年他進入[約翰·霍普金斯大學医学院工作](../Page/約翰·霍普金斯大學.md "wikilink")，职位是[神经内科](../Page/神经内科.md "wikilink")[住院医生](../Page/医生.md "wikilink")，随后应召入伍，进入[美国海军的](../Page/美国海军.md "wikilink")[沃尔特·里德医院](../Page/沃尔特·里德医院.md "wikilink")。1958年回到約翰·霍普金斯大學，开始了与[威泽尔长达](../Page/托斯坦·威泽尔.md "wikilink")25年的合作\[4\]\[5\]\[6\]。1959年時轉入[哈佛大學執教](../Page/哈佛大學.md "wikilink")。

## 科学贡献

休伯尔和威泽尔所做的实验大幅扩展了科学界关于感觉信号处理的知识。在1959年的一次实验中，他们将玻璃包被的钨丝[微电极插入麻醉猫的](../Page/微电极.md "wikilink")[初级视皮层](../Page/初级视皮层.md "wikilink")，然后在置于猫前方的幕布上投射出一条光带，改变光带的[空间方位角度](../Page/空间方位.md "wikilink")，用微电极记录[神经元的](../Page/神经元.md "wikilink")[发放](../Page/发放.md "wikilink")。他们发现当光带处于某个空间方位角度时，发放最为强烈。而且不同的神经元对不同空间方位的偏好不尽相同。还有些神经元对亮光带和暗光带的反应模式也不相同。休伯尔和威泽尔将这种神经元稱为[简单细胞](../Page/简单细胞.md "wikilink")。\[7\]初级视皮层里另外的那些神经元，叫做[复杂细胞](../Page/复杂细胞.md "wikilink")，这些细胞对于其[感受野中的边界信息比较敏感](../Page/感受野.md "wikilink")，还可以检测其感受野中的运动信息。\[8\]
这些研究给人们展示了[视觉系统是怎样将简单的](../Page/视觉系统.md "wikilink")[视觉特征在](../Page/视觉特征.md "wikilink")[视皮层呈现出来的](../Page/视皮层.md "wikilink")\[9\]。

休伯尔和威泽尔被授予诺贝尔生理学和医学奖主要是基于他们下面的两项成就：第一，他们在二十世纪六七十年代在视觉系统发育方面的研究，发现了[眼优势柱](../Page/眼优势柱.md "wikilink")。第二，他们的工作奠定了视觉[神经生理学的基础](../Page/神经生理学.md "wikilink")。他们的工作给人们呈现了视觉系统是如何将来自外界的视觉信号传递到视皮层，并通过一系列处理过程（包括[边界检测](../Page/边界检测.md "wikilink")、[运动检测](../Page/运动知觉（视觉）.md "wikilink")、[立体深度检测和](../Page/立体视觉.md "wikilink")[颜色检测](../Page/颜色知觉.md "wikilink")），最后在大脑中构建出一幅视觉图像的。他们通过对[小猫的一只眼睛](../Page/猫.md "wikilink")（A）进行[关键期](../Page/关键期.md "wikilink")[视觉剥夺](../Page/视觉剥夺.md "wikilink")（缝上眼皮，使其不能接收视觉输入），发现原本应当分配给这只眼睛（A）的[视皮层代表区域全部被分配给了另外一只眼睛](../Page/视皮层代表区域.md "wikilink")（B）。这个发现使得我们能够理解[弱视现象](../Page/弱视.md "wikilink")：在视觉系统发育的关键期视觉剥夺不均衡导致的一种视觉缺陷。在关键期进行视觉剥夺以后，这些小猫就没有发育出能够接受双眼视觉信息输入的[双眼细胞](../Page/双眼细胞.md "wikilink")，因此丧失[双眼视觉能力](../Page/双眼视觉.md "wikilink")。休伯尔和维厄泽尔的实验结果表明，眼优势柱的发育过程在动物幼年时代一旦完成，便已定型。这项研究为我们了解和治疗幼儿[白内障和](../Page/白内障.md "wikilink")[斜视打开了大门](../Page/斜视.md "wikilink")。对于[大脑皮层的](../Page/大脑皮层.md "wikilink")[神经元可塑性的研究也非常重要](../Page/神经可塑性.md "wikilink")\[10\]。另外我们对[感觉系统](../Page/感觉系统.md "wikilink")[信号处理过程的认识启发了人们对于](../Page/数字信号处理.md "wikilink")[计算机视觉中](../Page/计算机视觉.md "wikilink")[物体特征识别和](../Page/特征识别.md "wikilink")[宽基线匹配中](../Page/宽基线匹配.md "wikilink")[尺度不变特征转换](../Page/尺度不变特征转换.md "wikilink")（SIFT）特性的理解。

## 个人生活

## 著名论文

  - [Hubel, D. H. & T. N. Wiesel, Receptive Fields Of Single Neurones In
    The Cat's Striate Cortex, Journal of Physiology,
    (1959) 148, 574-591.](http://jp.physoc.org/content/148/3/574.full.pdf)

<!-- end list -->

  - [Hubel, D. H. & T. N. Wiesel. Receptive Fields, Binocular
    Interaction And Functional Architecture In The Cat's Visual Cortex,
    Journal of Physiology,
    (1962), 160, 106–154](http://jp.physoc.org/content/160/1/106.full.pdf)

## 另外参见

  - [单细胞电生理记录](../Page/单细胞电生理记录.md "wikilink")
  - [斯蒂芬·库夫勒](../Page/斯蒂芬·库夫勒.md "wikilink")
  - [托斯坦·威泽尔](../Page/托斯坦·威泽尔.md "wikilink")
  - [莉迪亞·維拉-科馬羅夫](../Page/莉迪亞·維拉-科馬羅夫.md "wikilink")

## 参考文献

<references/>

## 外部链接

  - [Nobel Prize
    Biography](http://www.nobel.se/medicine/laureates/1981/hubel-autobio.html)
  - [Eye, Brain, and Vision - online
    book](https://web.archive.org/web/20070314135325/http://neuro.med.harvard.edu/site/dh/index.html)
  - [The Official Site of Louisa Gross Horwitz
    Prize](http://www.cumc.columbia.edu/horwitz/)

[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:加拿大神经科学家](../Category/加拿大神经科学家.md "wikilink")
[Category:美国神经科学家](../Category/美国神经科学家.md "wikilink")
[Category:神经生物学](../Category/神经生物学.md "wikilink")
[Category:加拿大生物學家](../Category/加拿大生物學家.md "wikilink")
[Category:加拿大生理學家](../Category/加拿大生理學家.md "wikilink")
[Category:哈佛大學教授](../Category/哈佛大學教授.md "wikilink")
[Category:約翰霍普金斯大學教師](../Category/約翰霍普金斯大學教師.md "wikilink")
[Category:麥吉爾大學校友](../Category/麥吉爾大學校友.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:諾貝爾生理學或醫學獎獲得者](../Category/諾貝爾生理學或醫學獎獲得者.md "wikilink")
[Category:在美国的加拿大人](../Category/在美国的加拿大人.md "wikilink")
[Category:霍维茨奖获得者](../Category/霍维茨奖获得者.md "wikilink")
[Category:蒙特婁人](../Category/蒙特婁人.md "wikilink")
[Category:安大略省人](../Category/安大略省人.md "wikilink")

1.  Denise Gellene (24 September 2013): [David Hubel, Nobel-Winning
    Scientist, Dies
    at 87](http://www.nytimes.com/2013/09/25/science/david-hubel-nobel-winning-scientist-dies-at-87.html?pagewanted=1&_r=0&hpw)
    *[New York Times](../Page/New_York_Times.md "wikilink")*. Retrieved
    25 September 2013

2.  Boston Globe: [Shirley Ruth (Izzard)
    Hubel](http://www.legacy.com/obituaries/bostonglobe/obituary.aspx?pid=163217003#fbLoggedOut)
    Legacy.com. Retrieved 25 September 2013

3.

4.  Livingstone, M. (2013). "David Hubel: In Memoriam." Neuron 80(2):
    267-269.

5.  Shatz, C. J. (2013). "David Hunter Hubel (1926-2013)." Nature
    502(7473): 625.

6.  Wurtz, R. H. (2013). "Retrospective. David H. Hubel (1926-2013)."
    Science 342(6158): 572.

7.

8.  , Chapter 4, pg 16

9.

10.