**小林章**（）是[日本一位著名的](../Page/日本.md "wikilink")[字体设计师](../Page/字体.md "wikilink")。最初在日本[照相排版業一大公司](../Page/照相排版.md "wikilink")「写研」工作，之后设计了[Hiragino](../Page/Hiragino.md "wikilink")（）[明朝](../Page/宋体.md "wikilink")、[AXIS字体的西文部分](../Page/AXIS字体.md "wikilink")，在日本被誉为西文[字体第一人](../Page/字体.md "wikilink")。现任[Linotype公司的字体总监](../Page/Linotype.md "wikilink")，进行字体设计的指挥策划，进行了[Optima等著名字体的改刻](../Page/Optima.md "wikilink")。常被聘为国际各种字体设计大赛的评委。

## 生平

1960年出生于日本[新潟县](../Page/新潟县.md "wikilink")[新潟市](../Page/新潟市.md "wikilink")，毕业于日本[武藏野美术大学视觉传达设计学科](../Page/武藏野美术大学.md "wikilink")，1983年开始在写研公司设计照排字模。之后在1989年赴[英国留学一年半学习西文字体](../Page/英国.md "wikilink")。1990年回到日本之后即开始[Hiragino](../Page/Hiragino.md "wikilink")（）明朝字体的工作，之后作为独立设计师在欧美的大型字体设计比赛频频获奖。与[铃木功共同完成AXIS字体以后](../Page/铃木功.md "wikilink")，应邀2001年加入[德国](../Page/德国.md "wikilink")[LinoType公司](../Page/LinoType.md "wikilink")。

小林章现在暂时停止新字体的创作，而是与[赫尔曼·察普夫](../Page/赫尔曼·察普夫.md "wikilink")（Hermann
Zapf）、[阿德里安·弗鲁提格等字体设计大师共事](../Page/阿德里安·弗鲁提格.md "wikilink")，进行字体名作的改刻工作。改刻工作要保留字体原来具有的字形和魅力，从[活字印刷时代的金属铅字技术的制约中解放出来](../Page/活字印刷.md "wikilink")，在数码环境中进行再现。工作的成果就是[LinoType公司新发布的一些改刻字体系列产品](../Page/LinoType.md "wikilink")。

## 主要作品

  - SKID ROW

  - 明朝体的西文部分

  - 字体的西文部分

  - AXIS字体的西文部分

  - Clifford

  - Woodland

  - Conrad

  - Scarborough

  - Japanese Garden

  - Seven Treasures

  - Luna

  - Silvermoon

  - Acanthus

  - Magnifico

  - Vineyard

  - Calcite Pro

  - TTTGB-Medium 腾讯字体参与

## 外部链接

  - \[<http://www.kazuipress.com/blog/>? 小林章的德国日記\]
  - [小林章的德国日記（博客版：从2006年4月1日开始）](http://doitunikki.exblog.jp/)

[Category:日本設計師](../Category/日本設計師.md "wikilink")
[Category:字体设计师](../Category/字体设计师.md "wikilink")
[Category:新潟縣出身人物](../Category/新潟縣出身人物.md "wikilink")