**七美人塚**是位於[台灣](../Page/台灣.md "wikilink")[澎湖縣](../Page/澎湖縣.md "wikilink")[七美鄉南港村的旅遊景點](../Page/七美鄉_\(臺灣\).md "wikilink")\[1\]，紀念七名因逃避[倭寇追趕而投井](../Page/倭寇.md "wikilink")[自盡的女子](../Page/自盡.md "wikilink")。
[Seven_Beauties_Tomb_title_20080525.jpg](https://zh.wikipedia.org/wiki/File:Seven_Beauties_Tomb_title_20080525.jpg "fig:Seven_Beauties_Tomb_title_20080525.jpg")
[Seven_Beauties_Tomb_intro_by_Lu_Zhao-lin_20080525.jpg](https://zh.wikipedia.org/wiki/File:Seven_Beauties_Tomb_intro_by_Lu_Zhao-lin_20080525.jpg "fig:Seven_Beauties_Tomb_intro_by_Lu_Zhao-lin_20080525.jpg")

## 典故

相傳在[明朝](../Page/明朝.md "wikilink")[嘉靖年間](../Page/嘉靖.md "wikilink")（1522年－1566年），一批[倭寇登陸大嶼燒殺擄掠](../Page/倭寇.md "wikilink")。有七名女子在倭寇的追趕下，自知難以逃過，又不甘[受辱](../Page/性侵犯.md "wikilink")，為保全自身[貞節](../Page/貞節.md "wikilink")，因而集體投井自盡；事後有人發現此井居然長出七棵樹，因此人們便傳說這些樹就是那七名烈女子的化身。此樹終年長青，會在春秋二季會開出小白花，飄有清香，當地人又稱它為“香花樹”。為感念七名烈女子的故事，後人填井立塚，在此立了塊“七美人碑”，稱為七美人塚；原名大嶼的七美，也在1949年改名為現在的名稱。現今七美人塚上有兩個石碑，左邊的石碑是[鄭碾立的](../Page/鄭碾.md "wikilink")，石邊的碑文是[何志浩將軍作歌詞和敘文](../Page/何志浩.md "wikilink")、[張默君女士寫書法](../Page/張默君.md "wikilink")，表示對七名女子的追悼\[2\]。1989年1月31日，七美鄉鄉長呂昭麟題〈七美人塚事蹟簡介〉並刻石，以紀念七名烈女子的故事。

## 參考文獻

[七](../Category/台灣民間故事.md "wikilink")
[七](../Category/臺灣女性.md "wikilink")
[Category:澎湖縣旅遊景點](../Category/澎湖縣旅遊景點.md "wikilink")
[Category:文建會歷史建築百景](../Category/文建會歷史建築百景.md "wikilink")
[Category:台灣墓葬](../Category/台灣墓葬.md "wikilink") [Category:七美鄉
(臺灣)](../Category/七美鄉_\(臺灣\).md "wikilink")

1.  [澎湖縣政府旅遊處](http://tour.penghu.gov.tw/tw/Attractionscontent.aspx?id=4995)

2.  [台灣大百科全書](http://nrch.culture.tw/twpedia.aspx?id=22853)