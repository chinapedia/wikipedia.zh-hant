在[NBA聯盟中](../Page/NBA.md "wikilink")，聯盟為了嚇阻各球團毫無限制地灑錢來網羅明星球員，所以定訂了球隊的[薪資上限](../Page/NBA薪資上限.md "wikilink")，任何球隊只要球員總薪資超過此上限，就不可以再簽下任何自由球員。但是只要球員符合某些特殊條款的需求，則球隊可以以無視薪金上限的方式來簽下任何自由球員，這類特殊條款就是屬於**NBA特殊條款**。

## 條款起源

## NBA所有特殊條款

### 三鳥條款

  - [拉里·伯德條款](../Page/拉里·伯德條款.md "wikilink")（Larry Bird exception）
  - [早鳥條款](../Page/早鳥條款.md "wikilink")（Early Bird exception）
  - [非鳥條款](../Page/非鳥條款.md "wikilink")（Non-Bird exception）

### 其他條款

  - [中產階級條款](../Page/中產階級條款.md "wikilink")（Mid-Level Salary exception）
  - [一百萬特例](../Page/一百萬特例.md "wikilink")（One-Million
    exception）（Bi-Annual exception）
  - [底薪特例](../Page/底薪特例.md "wikilink")（Minimum Salary Exception）
  - [新秀條款](../Page/新秀條款.md "wikilink")（Rookie Exception）
  - [傷兵條款](../Page/傷兵條款.md "wikilink")（Disabled Player Exception）

## 參見

  - [NBA](../Page/NBA.md "wikilink")
  - [NBA薪金上限](../Page/NBA工资帽.md "wikilink")

## 外部連結

  - [NBA Salary
    Cap 101](http://www.nba.com/blazers/news/Salary_Cap_101-147720-41.html)。
  - [NBA Players' Association CBA
    page](https://web.archive.org/web/20100206125325/http://www.nbpa.com/cba.php)。
  - [NBA salary cap
    FAQ](https://web.archive.org/web/20100722080241/http://members.cox.net/lmcoon/salarycap.htm)。

[\*](../Category/NBA.md "wikilink")