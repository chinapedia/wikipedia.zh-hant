**大師**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")，只在[大局將棋出現](../Page/大局將棋.md "wikilink")。

大師不得升級。

<table>
<tbody>
<tr class="odd">
<td><p>前身棋</p></td>
<td><p>步法</p></td>
</tr>
<tr class="even">
<td><p>大師</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>＼</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>／</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>＼</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>／</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p>☆</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>☆</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>☆</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>＼</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>／</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>＼</p></td>
<td><p>│</p></td>
<td><p>／</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p><strong>師</strong></p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p>│</p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>○</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p>○</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")