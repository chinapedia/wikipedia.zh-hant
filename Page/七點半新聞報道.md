《**七點半新聞報道**》（[英文](../Page/英语.md "wikilink")：），[香港](../Page/香港.md "wikilink")[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")[新聞及資訊部製作的新聞節目](../Page/無綫新聞.md "wikilink")，每日傍晚19:30於[明珠台播出](../Page/明珠台.md "wikilink")。報道內容與[翡翠台](../Page/翡翠台.md "wikilink")《[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")》相似，但也有不同的新聞內容，包括更多國際新聞、外國趣聞及更長的體育環節。

新聞播放完畢後，廣告後有一節《[天氣報告](../Page/無綫電視天氣報告.md "wikilink")》，然後轉播《[瞬間看地球](../Page/瞬間看地球.md "wikilink")》。

香港其他免費電視台的傍晚英語新聞節目有[ViuTVsix的](../Page/ViuTVsix.md "wikilink")[ViuTV
News和](../Page/ViuTV_News.md "wikilink")[奇妙電視](../Page/奇妙電視.md "wikilink")[香港國際財經台的](../Page/香港國際財經台.md "wikilink")[Main
News](../Page/Main_News_\(香港國際財經台\).md "wikilink")。

## 歷史

自1967年11月19日開始啟播，[明珠台的新聞報道已於](../Page/明珠台.md "wikilink")19:00-19:30開始播出，及後更改新聞的播出時間，詳情如下：

  - 1975年開始，當時的新聞分2節播放，播出時間為18:40-18:45及20:45-21:10，18:40-18:45的時段播放5分鐘，而20:45-21:10的時段播放25分鐘。
  - 1978年4月3日起，取消18:40-18:45的播放時段，而20:45-21:10的新聞時段更改為19:00-19:30。
  - 1987年3月7日起，改為19:30-20:00播出。
  - 1988年12月1日起，星期一至五的新聞時段為19:15-19:55，而星期六、日分2節播放，播出時間為18:00-18:15及21:00-21:30，18:00-18:15的時段播放15分鐘，而21:00-21:30的時段播放30分鐘。
  - 1990年10月6日起，取消星期六、日18:00-18:15的播放時段，而21:00-21:30的新聞時段更改為19:15-19:40，播放25分鐘。
  - 1992年8月10日起，改為19:10-19:35播出。
  - 1993年5月31日起，改為20:00-20:30播出。
  - 1995年9月18日起，改為19:30-20:00播出，並維持至今。
  - 2008年起，同時在[無綫電視網站](http://news.tvb.com)提供節目重溫。

## 主播

《七點半新聞報道》的主播組合安排上與[翡翠台的](../Page/翡翠台.md "wikilink")《[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")》大同小異，同樣由具經驗的主播才可負責報道。主播通常為一男一女主播，而男主播同時負責報道體育新聞，現時擔任的男主播有[Chris
Lincoln](../Page/林國基.md "wikilink")、[James
Atiken](../Page/James_Atiken.md "wikilink")、[Jameson
Wong及](../Page/Jameson_Wong.md "wikilink")[Tony
Sabine](../Page/Tony_Sabine.md "wikilink")。而女主播有[Sonya
Artero](../Page/Sonya_Artero.md "wikilink")、[Anne-Marie
Sim及](../Page/Anne-Marie_Sim.md "wikilink")[Elmy
Lung](../Page/龍惠榆.md "wikilink")。

近年的主播如[Tesa Arcilla](../Page/Tesa_Arcilla.md "wikilink")、[Regina De
Luna](../Page/Regina_De_Luna.md "wikilink")、[Shelley
Fines](../Page/Shelley_Fines.md "wikilink")、[Eric
Finney](../Page/Eric_Finney.md "wikilink")、[Patrick
Fok](../Page/霍炳宗.md "wikilink")、[Chris
Gelken](../Page/葛爾勤.md "wikilink")、[Emma
Jones](../Page/Emma_Jones.md "wikilink")、[Deborah
Kan](../Page/Deborah_Kan.md "wikilink")、[Eve
Lam](../Page/Eve_Lam.md "wikilink")、[Jenny
Lam](../Page/林楚芹.md "wikilink")、[Etienne
Lamy-Smith](../Page/Etienne_Lamy-Smith.md "wikilink")、[Diana
Lin](../Page/Diana_Lin.md "wikilink")、[Betty
Liu](../Page/Betty_Liu.md "wikilink")、[Peter
Maize](../Page/Peter_Maize.md "wikilink")、[Priscilla
Ng](../Page/吳芷寧.md "wikilink")、[Jimmy
So](../Page/Jimmy_So.md "wikilink")、[Diane
To](../Page/Diane_To.md "wikilink")、[Nicole
Tsang](../Page/曾碧慧.md "wikilink")、[Evelina
Leung及](../Page/Evelina_Leung.md "wikilink")[Robert
Trott已離職](../Page/Robert_Trott.md "wikilink")，而[翡翠台的主播](../Page/翡翠台.md "wikilink")[周潔儀近年都曾經在這節新聞擔任主播](../Page/周潔儀.md "wikilink")。

## 名稱

「七點半新聞報導」的名稱其實早在1967年無綫電視啟播時已出現，當時為中文版本，其後在1976年因為《[翡翠劇場](../Page/翡翠劇場.md "wikilink")》首播而改為18:30播放，並改名為《[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")》至今。而當時的[明珠台的新聞名稱曾多次更改](../Page/明珠台.md "wikilink")，在1978年4月3日至1987年3月6日的名稱為《[七點新聞報道](../Page/七點新聞報道.md "wikilink")》（《NEWS
AT 7:00》），1987年3月7日至1988年11月30日改為《七點半新聞報道》（《NEWS AT
7:30》），1988年12月1日至1990年9月30日改為《[九點新聞報道](../Page/九點新聞報道.md "wikilink")》（《NEWS
AT
9:00》），1993年5月31日至1995年9月17日改為《[八點新聞報道](../Page/八點新聞報道.md "wikilink")》（《NEWS
AT 8:00》），1995年9月18日起再次改回《七點半新聞報道》並維持至今。

## 外部連結

  - [無綫電視官方網頁 - 七點半新聞報道](http://programme.tvb.com/news/newsat730)
  - [myTV《七點半新聞報道》重溫](http://mytv.tvb.com/news/newsat730/)
  - [無綫新聞網站-《七點半新聞報道》重溫](http://news.tvb.com/programmes/newsat730)

[Category:無綫新聞節目](../Category/無綫新聞節目.md "wikilink")
[Category:香港電視新聞節目](../Category/香港電視新聞節目.md "wikilink")