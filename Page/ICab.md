**iCab**是一個[Mac
OS作業系統上的](../Page/Mac_OS.md "wikilink")[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，採用獨立製作的[排版引擎](../Page/排版引擎.md "wikilink")，iCab
4改用[WebKit](../Page/WebKit.md "wikilink")\[1\]。有著相當久的歷史，就算在相當早期的[MC68000上也可以運行](../Page/MC68000.md "wikilink")。2009年4月21日，iCab进入[App
Store](../Page/App_Store.md "wikilink")，收费1.99美元，成为首款入驻原版iOS的主流第三方浏览器\[2\]。

## 參考文獻

## 相關條目

  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [網頁瀏覽器比较](../Page/網頁瀏覽器比较.md "wikilink")

## 外部連結

  -
[Category:1999年軟體](../Category/1999年軟體.md "wikilink")
[Category:Webkit衍生軟體](../Category/Webkit衍生軟體.md "wikilink")

1.
2.