[Etchedwafer.jpg](https://zh.wikipedia.org/wiki/File:Etchedwafer.jpg "fig:Etchedwafer.jpg")（etch）製程處理後的晶圓\]\]
**晶圆**（）是指制作矽(硅)[半导体](../Page/半导体.md "wikilink")[集成电路所用的矽](../Page/集成电路.md "wikilink")(硅)晶片，由于其形状为圆形，故称为晶圆。晶圆是生产[集成电路所用的载体](../Page/集成电路.md "wikilink")，一般晶圆產量多為[单晶硅圆片](../Page/单晶硅.md "wikilink")\[1\]。

晶圆是最常用的[半导体材料](../Page/半导体.md "wikilink")，按其直径分为4[英寸](../Page/英寸.md "wikilink")、5英寸、6英寸、8英寸等规格，近来发展出12英寸甚至研發更大规格（14英吋、15英吋、16英吋、20英吋以上等）。晶圆越大，同一圆片上可生产的[集成电路](../Page/集成电路.md "wikilink")（integrated
circuit,
**IC**）就越多，可降低成本；但对材料技术和生产技术的要求更高，例如均勻度等等的問題。一般認為矽(硅)晶圆的直径越大，代表著这座晶圆厂有更好的技術，在生产晶圆的过程当中，[良品率是很重要的条件](../Page/設備效率評價#良率\(Quality\).md "wikilink")\[2\]。

## 製造過程

[Czochralski_Process.svg](https://zh.wikipedia.org/wiki/File:Czochralski_Process.svg "fig:Czochralski_Process.svg")

很簡單的說，首先由普通矽(硅)砂拉製[提煉](../Page/提煉.md "wikilink")，經過[溶解](../Page/溶解.md "wikilink")、[提純](../Page/提純.md "wikilink")、[蒸餾一系列措施製成單晶硅棒](../Page/蒸餾.md "wikilink")，單晶硅棒經過[切片](../Page/切片.md "wikilink")、[拋光之後](../Page/拋光.md "wikilink")，就得到了單晶矽(硅)圓片，也即晶圓。

1.  将[二氧化硅礦石](../Page/二氧化硅.md "wikilink")（[石英砂](../Page/石英砂.md "wikilink")）与焦炭混合后，經由電弧爐加热还原，即生成粗矽（纯度98%，冶金级）。SiO<sub>2</sub>
    + C = Si + CO<sub>2</sub> ↑ \[3\]
2.  [鹽酸氯化並經蒸餾後](../Page/鹽酸.md "wikilink")，製成了高純度的[多晶硅](../Page/多晶硅.md "wikilink")(半导体级[純度](../Page/純度.md "wikilink")11个9，[太阳能级](../Page/太阳能.md "wikilink")7个9），因在精密電子元件當中，矽晶圓需要有相當的純度（99.999999999%），不然會產生缺陷。
3.  晶圓製造廠再以[柴可拉斯基法將此多晶矽](../Page/柴可拉斯基法.md "wikilink")(硅)熔解，再於溶液內摻入一小粒的矽(硅)晶體晶種，然後將其慢慢拉出，以形成圓柱狀的單晶矽(硅)晶棒，由於矽(硅)晶棒是由一顆小晶粒在融熔態的矽(硅)原料中逐漸生成，此過程稱為「長晶」。這根晶棒的直徑，就是晶圓的直径。
4.  矽(硅)晶棒再經過切片、研磨、拋光後，即成為[積體電路工廠的基本原料](../Page/積體電路.md "wikilink")——矽(硅)晶圓片，這就是「晶圓」（wafer）。
5.  晶圓經多次光罩處理，其中每一次的步驟包括感光劑塗佈、曝光、顯影、腐蝕、滲透、植入、[蝕刻或蒸著等等](../Page/蝕刻.md "wikilink")，將其光罩上的電路複製到層層晶圓上，製成具有多層線路與元件的[IC晶圓](../Page/集成电路.md "wikilink")，再交由後段的測試、切割、封裝廠，以製成實體的[積體電路成品](../Page/積體電路.md "wikilink")。一般來說，大晶圓(12吋)多是用來製作記憶體等技術層次較低的IC，而小晶圓(6吋)則多用來製作類比IC等技術層次較高之IC，8吋的話則都有。

從晶圓要加工成為產品需要專業精細的分工。

## 著名晶圓廠商

[Wafer_2_Zoll_bis_8_Zoll_2.jpg](https://zh.wikipedia.org/wiki/File:Wafer_2_Zoll_bis_8_Zoll_2.jpg "fig:Wafer_2_Zoll_bis_8_Zoll_2.jpg")
[英特尔](../Page/英特尔.md "wikilink")（Intel）等公司則自行設計並製造自己的IC晶圓直至完成並行銷其產品。[三星電子等則兼有晶圓代工及自製業務](../Page/三星電子.md "wikilink")。

### [晶圓代工](../Page/晶圓代工.md "wikilink")

著名晶圓代工廠有[台積電](../Page/台積電.md "wikilink")、[聯華電子](../Page/聯華電子.md "wikilink")、[格羅方德](../Page/格羅方德.md "wikilink")（Global
Foundries）及[中芯國際等](../Page/中芯國際.md "wikilink")。

### 只製造晶圓基片

例如[合晶](../Page/合晶.md "wikilink")（台灣股票代號：6182）、[中美晶](../Page/中美晶.md "wikilink")（台灣股票代號：5483）、日本的[信越化學](../Page/信越化學.md "wikilink")、[Sumco等](../Page/Sumco.md "wikilink")。

### 封装测试

[日月光半導體](../Page/日月光半導體.md "wikilink")、[艾克爾國際科技等則為世界前二大晶圓產業後段的封裝](../Page/艾克爾國際科技.md "wikilink")、測試廠商。

[Siligon_ingot_at_Intel_Museum.JPG](https://zh.wikipedia.org/wiki/File:Siligon_ingot_at_Intel_Museum.JPG "fig:Siligon_ingot_at_Intel_Museum.JPG")所展示的矽晶棒\]\]

## 其他相关

### 多晶矽(硅)生产

主要制造多晶矽(硅)的大厂有： Hemlock（美）、MEMC（美）、Wacker（德）、REC（挪）、OCI（韩）、Tokuyama（日）和
GCL（中）。

### IC设计

仅从事IC設計的公司有美国的[高通](../Page/高通.md "wikilink")、台湾的[聯發科技等](../Page/聯發科技.md "wikilink")。

### [記憶體](../Page/記憶體.md "wikilink")

[南亞科技](../Page/南亞科技.md "wikilink")、[瑞晶科技](../Page/瑞晶科技.md "wikilink")（現已併入美光科技，更名台灣美光記憶體）、[Hynix](../Page/Hynix.md "wikilink")、[美光科技](../Page/美光科技.md "wikilink")（Micron）等則專於[記憶體產品](../Page/記憶體.md "wikilink")。

## 引用和注释

[Category:集成電路](../Category/集成電路.md "wikilink")
[Category:电子工程](../Category/电子工程.md "wikilink")

1.
2.  [半導體產業的根基：矽晶圓是什麼？](http://technews.tw/2015/07/18/what-is-wafer/)

3.  痞客帮 -
    [半導體產業上游---矽晶圓產業](http://zhe09.pixnet.net/blog/post/50868359-半導體產業上游---矽晶圓產業)