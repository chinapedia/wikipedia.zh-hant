**歐陽克**是[金庸](../Page/金庸.md "wikilink")[武俠小说](../Page/武俠小说.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》中的反派人物，人稱「小毒物」。

歐陽克身穿白衣、容貌英俊，出入都有大量[白駝山莊女弟子陪同](../Page/白駝山莊.md "wikilink")。舉止優雅、學識淵博、武功高強但貪淫好色，看到美女就想侵犯，最後因為淫念而死。

其武功高強，擅長用毒和蛇陣，除五絕高手之外少有敵手。

登場回數:(射)7-13,18-25

## 生平

是「西毒」[歐陽鋒和嫂子私通生下的](../Page/歐陽鋒.md "wikilink")[私生子](../Page/私生子.md "wikilink")，為免人側目以叔侄相稱。歐陽克生性好色，到處捻花惹草，姦污許多女孩家的清白。

歐陽克曾加入[大金六王爺](../Page/金朝.md "wikilink")[完顏洪烈旗下](../Page/完顏洪烈.md "wikilink")，協助[大金消滅](../Page/金朝.md "wikilink")[南宋及](../Page/南宋.md "wikilink")[蒙古](../Page/蒙古帝國.md "wikilink")，並找尋《[武穆遺書](../Page/武穆遺書.md "wikilink")》。後來看中[黃蓉美色](../Page/黃蓉.md "wikilink")，處心積慮欲一親芳澤，後來在明霞島慘被黃蓉用計以千鈞巨岩壓斷單腿。

曾多次對黃蓉、[穆念慈以及](../Page/穆念慈.md "wikilink")[全真教](../Page/全真教.md "wikilink")[孫不二的徒弟](../Page/孫不二.md "wikilink")[程瑤迦起色心](../Page/程瑤迦.md "wikilink")，後來因想學歐陽鋒只傳一人的武學加上他對穆念慈有非份之想而激怒[楊康](../Page/楊康.md "wikilink")，最後在牛家村曲三酒館被楊康偷襲刺死。

## 武功

  - 瞬息千里

白駝山莊家傳上乘輕功，斗然間已欺到了對手身旁。

  - 神駝雪山掌

白駝山莊家傳武功，身形飄忽，出掌進攻。

  - 靈蛇拳

歐陽鋒从毒蛇身上領悟的拳法。

手臂犹似忽然没了骨头，如变了一根软鞭，打出后能在空中任意拐弯。

## 影视形象

### 電視劇

  - 1976[刘江](../Page/劉江_\(香港\).md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(1976年電視劇\).md "wikilink")
  - 1983[黃允材](../Page/黃允材.md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(1983年電視劇\).md "wikilink")
  - 1988[戈伟家](../Page/戈伟家.md "wikilink")：大漠英雄傳華山論劍
  - 1994[林偉](../Page/林偉_\(演員\).md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(1994年電視劇\).md "wikilink")
  - 2003[修庆](../Page/修庆.md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(2003年電視劇\).md "wikilink")
  - 2008[李解](../Page/李解.md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(2008年電視劇\).md "wikilink")
  - 2017[劉智揚](../Page/劉智揚.md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(2017年電視劇\).md "wikilink")

### 電影

  - 1977[李修贤](../Page/李修贤.md "wikilink")：[射鵰英雄傳](../Page/射鵰英雄傳_\(1977年電影\).md "wikilink")
  - 1978[李修贤](../Page/李修贤.md "wikilink")：[射鵰英雄傳續集](../Page/射鵰英雄傳續集.md "wikilink")
  - 1982[李修贤](../Page/李修贤.md "wikilink")：[神雕侠侣](../Page/神雕侠侣.md "wikilink")

[vi:Nhân vật trong Anh hùng xạ điêu\#Âu Dương
Khắc](../Page/vi:Nhân_vật_trong_Anh_hùng_xạ_điêu#Âu_Dương_Khắc.md "wikilink")

[O歐陽](../Category/射鵰英雄傳角色.md "wikilink")