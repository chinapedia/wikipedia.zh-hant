**烏蘭泰**（），字**远芳**，满洲正红旗人。由[火器营鸟枪护军从征](../Page/火器营.md "wikilink")[回疆有功](../Page/回疆.md "wikilink")，升蓝翎长，累擢护军参领，道光二十七年，擢广东副都统。善训练，讲求火器。

## 生平

  - 咸丰元年，广西太平軍炽，诏[乌兰泰帮辨军务](../Page/乌兰泰.md "wikilink")，选带適用器械及得力[章京兵丁赴军](../Page/章京.md "wikilink")，以广东绿营精兵五百人隶之。
  - 四月，偕[向荣](../Page/向荣.md "wikilink")、[秦定三等围太平軍於](../Page/秦定三.md "wikilink")[武宣](../Page/武宣.md "wikilink")，太平軍窜[象州](../Page/象州.md "wikilink")，自请治罪。诏以其初至，免议，命偕[向荣节制镇将](../Page/向荣.md "wikilink")。
  - 时军中将帅不和，文宗忧之，密谕[乌兰泰实陈勿隐](../Page/乌兰泰.md "wikilink")，又言：“向荣初剿太平軍屡捷，未免轻贼。及其子招嫌，楚兵藉口，遂多诿卸。然在军镇将无及荣者。更易其兵，仍可立功。”上下其疏，命赛尚阿覈奏，**[赛尚阿请不咎既往](../Page/赛尚阿.md "wikilink")，令乌兰泰与[向荣分任军事](../Page/向荣.md "wikilink")，以专责成**。
  - 太平軍踞[象州中坪](../Page/象州.md "wikilink")，乌兰泰督贵州三镇兵，殪太平軍千馀。是年秋，太平軍窜[桂平新墟](../Page/桂平.md "wikilink")，乌兰泰分四路进攻，破伏太平軍於[莫村](../Page/莫村.md "wikilink")，一日七战皆捷，斩级数千，赐花翎。
  - 太平軍屯[紫荆山](../Page/紫荆山_\(廣西\).md "wikilink")，新墟为山前门户，太平軍遂陷[永安州](../Page/永安州.md "wikilink")。[乌兰泰追至](../Page/乌兰泰.md "wikilink")，战於[水窦](../Page/水窦.md "wikilink")、[圞岭](../Page/圞岭.md "wikilink")，皆大捷，赐黄马褂。[永安地险](../Page/永安州.md "wikilink")，太平軍皆死党固结，仅乌兰泰一军久战已疲，故不能制之。
  - 向荣自平南败后被谴，讬病至冬始抵[永安](../Page/永安州.md "wikilink")，攻北路，乌兰泰攻南路，毁水窦太平軍軍營。[向荣亦进夺槓岭要隘](../Page/向荣.md "wikilink")，合击迭挫太平軍。[赛尚阿亲莅督战](../Page/赛尚阿.md "wikilink")，期在必克。[江忠源号知兵](../Page/江忠源.md "wikilink")，隶乌兰泰军，倚其赞助；每言**太平軍凶悍，久蔓将不可制，必聚而歼之。[乌兰泰主锁围困太平軍](../Page/乌兰泰.md "wikilink")，[向荣谓围城缺一面](../Page/向荣.md "wikilink")，乃古法，宜纵太平軍贼出击，两人意不合**。
  - 会荣克城西砲台，二年元旦，同诣[赛尚阿贺岁](../Page/赛尚阿.md "wikilink")。**赛尚阿遇荣特优，乌兰泰愤甚，忠源解之**，然益不相能。忠源以母忧，辞归。

## 戰歿

  - 时严诏促战，春雨连旬，士卒疲困。二月，太平軍弃城冒雨夜走，北犯[桂林](../Page/桂林.md "wikilink")。[乌兰泰率兵急追至](../Page/乌兰泰.md "wikilink")[昭平山中](../Page/昭平.md "wikilink")，路险雨滑，为太平軍所乘，败绩，总兵[长瑞](../Page/长瑞.md "wikilink")、[長壽](../Page/長壽_\(清朝人物\).md "wikilink")、[董光甲](../Page/董光甲.md "wikilink")、[邵鹤龄死之](../Page/邵鹤龄.md "wikilink")。
  - [向荣径收州城](../Page/向荣.md "wikilink")，由间道趋桂林，先太平軍至。[乌兰泰踵太平軍后](../Page/乌兰泰.md "wikilink")，战於南门外，争[将军桥](../Page/将军桥.md "wikilink")，砲中右腿，创甚，退屯[阳朔](../Page/阳朔.md "wikilink")，越二十日卒於军。
  - **[乌兰泰忠勇为诸将冠](../Page/乌兰泰.md "wikilink")，文宗深惜之**，赐银一千两治丧，予[轻车都尉世职](../Page/轻车都尉.md "wikilink")，谥武壮。

[Category:清朝軍事人物](../Category/清朝軍事人物.md "wikilink")
[Category:清朝戰爭身亡者](../Category/清朝戰爭身亡者.md "wikilink")
[Category:满洲正红旗人](../Category/满洲正红旗人.md "wikilink")
[Category:諡武壯](../Category/諡武壯.md "wikilink")