[天母街景New_-_panoramio_-_Tianmu_peter_(27).jpg](https://zh.wikipedia.org/wiki/File:天母街景New_-_panoramio_-_Tianmu_peter_\(27\).jpg "fig:天母街景New_-_panoramio_-_Tianmu_peter_(27).jpg")
[National_Taipei_University_of_Nursing_and_Health_Science_20161210.jpg](https://zh.wikipedia.org/wiki/File:National_Taipei_University_of_Nursing_and_Health_Science_20161210.jpg "fig:National_Taipei_University_of_Nursing_and_Health_Science_20161210.jpg")
[NTUNHS_Campus1.jpg](https://zh.wikipedia.org/wiki/File:NTUNHS_Campus1.jpg "fig:NTUNHS_Campus1.jpg")
[NTUNHS_Downtown_Campus_20170624.jpg](https://zh.wikipedia.org/wiki/File:NTUNHS_Downtown_Campus_20170624.jpg "fig:NTUNHS_Downtown_Campus_20170624.jpg")
{{ Expand |time=2010-03-17}}
**國立臺北護理健康大學**，簡稱**北護大**、**國北護**、**北護**，是一所位於[臺灣](../Page/臺灣.md "wikilink")[臺北市的國立](../Page/臺北市.md "wikilink")[科技大學](../Page/科技大學_\(學制\).md "wikilink")，以教授[健康科學課程為主](../Page/健康科學.md "wikilink")。內江街校址為城區部，僅語言治療與聽力學系暨碩士班、健康事業管理系暨研究所、長期照護研究所等三所一系仍設置在城區部。
除了中西醫護理研究所、醫護教育研究所等特色系所，臺北護理健康大學也是目前唯一設有[助產學](../Page/助產學.md "wikilink")、[語言治療](../Page/語言治療.md "wikilink")、[聽力學及](../Page/聽力學.md "wikilink")[生死學的公立醫療大學](../Page/生死學.md "wikilink")。

## 沿革

### 高級職業學校時期

  - 1947年：**臺灣省立臺北高級醫事職業學校**成立，時為[醫院的附屬學校](../Page/醫院.md "wikilink")，並以助產特科（二年制）為主。第一屆招生有34人，在1947年5月4日正式開學。設校初期因校舍無著，暫以[臺大醫院撥借房屋兩間作為臨時校舍](../Page/國立臺灣大學醫學院附設醫院.md "wikilink")。1947年9月，政府擇定[日治時期](../Page/台灣日治時期.md "wikilink")1915年\~1937年期間的臺北州立臺北第三高等女學校（今[臺北市立中山女子高級中學](../Page/臺北市立中山女子高級中學.md "wikilink")）與1937年\~1945年期間原址新設的臺北州立臺北第四高等女學校（戰後與剩餘的州立臺北第一高女、第二高女學生合併成臺北市立第一女子高級中學校區**萬華區內江街89號**為校舍。
  - 1953年：改為三年制的護士職業學校，名稱改為**臺灣省立臺北高級護理助產職業學校**。
  - 1954年：正式成立**臺灣省立護理專科學校**，此時也開設了公共衛生護士訓練班、並有五年制的護理專科和夜間部課程。
  - 1955年：三月，始正式遷入**中正區漳州街（已經併入[中華路二段](../Page/中華路_\(台北市\).md "wikilink")）3巷2號**原臺灣省立臺北女子師範學校（今[臺北市立大學校本部](../Page/臺北市立大學校本部.md "wikilink")）學生宿舍。
  - 1958年：時任校長徐藹諸女士因感漳州街校址環境簡陋、場地狹窄，積極尋覓新校舍。當時[臺灣省政府財源並不寬裕](../Page/臺灣省政府.md "wikilink")，徐校長乃多方奔走籌措經費。屢經艱難，終於徵收北投區石牌路二段210號土地七甲餘為新校址。在石牌新校舍興建完成前，自1963年暑期，暫時遷回**萬華區內江街89號**臺北高級護理助產職業學校上課，並將漳州街校舍讓與[臺北市政府設置臺北市立古亭女子初級中學](../Page/臺北市政府.md "wikilink")（今[臺北市立古亭國民中學](../Page/臺北市立古亭國民中學.md "wikilink")）。

### 專科學校時期

  - 1963年：改制為五專，並與**臺灣省立護理專科學校**合併。
  - 1981年：省制修改之後，改稱**國立臺北護理專科學校**，並引進許多現代化的醫療設備與國際交流。
  - 1986年：2月，北投區新校舍興建完成，全校遷入**北投區明德路365號**新址，新校區佔地七甲餘，環境清幽，是為校本部

### 護理學院時期

  - 1994年：改制為**國立臺北護理學院**，設置護理系、醫護管理系。原有的三專與二專制停止招生。
  - 1996年：增設嬰幼兒保育系。
  - 1998年：增設護理研究所碩士班、醫護管理研究所碩士班。
  - 2000年：成立聽語障礙科學研究所碩士班，為華語區最早成立的語言治療及聽力學研究所，參考美國聽力語言學會的架構，培育碩士級別的語言治療師及聽力師。增設長期照護研究所碩士班、護理助產研究所碩士班、醫護教育研究所碩士班。
  - 2001年：增設嬰幼兒保育研究所碩士班、旅遊健康研究所碩士班、生死教育與輔導教育研究所碩士班、中西醫結合護理研究所碩士班。
  - 2002年：增設運動保健系、資訊管理系。
  - 2004年：增設資訊管理研究所。
  - 2005年：增設護理研究所博士班。
  - 2007年：增設國際護理碩士班、運動保健研究所。
  - 2008年：醫護管理系改名健康事業管理系。
  - 2009年：聽語障礙科學研究所成立聽語中心，提供語言、言語、嗓音、吞嚥、聽力及聽覺復健之服務。

### 護理健康大學時期

  - 2010年：奉教育部核准，改名**國立臺北護理健康大學**。
  - 2014年：生死教育與輔導研究所併入生死與健康心理諮商系為生死與健康心理諮商系碩士班。健康事業管理學院改名健康科技學院。
  - 2015年：增設長期照護系並與長期照護研究所系所合一，亦增設學士後長期照護學位學程，增設休閒產業與健康促進系並與旅遊健康研究所系所合一，增設語言治療與聽力系並與聽語障礙研究所系合一。
  - 2016年：增設嬰幼兒保育系國際蒙特梭利碩士專班、高齡健康照護系、增設二技日間部助產及婦女健康照護系。護理助產研究所與助產及婦女健康照護系整併為助產及婦女健康照護系暨護理助產碩士班。
  - 2017年：增設醫護教育暨數位學習系，醫護教育研究所併入新設之醫護教育暨數位學習系為碩士班。

## 歷任校長

| 任別  | 姓名                               | 起始日期     | 卸任日期     |
| --- | -------------------------------- | -------- | -------- |
| 首任  | [譚素蓉](../Page/譚素蓉.md "wikilink") | 1947年1月  | 1948年3月  |
| 第二任 | [夏德真](../Page/夏德真.md "wikilink") | 1948年3月  | 1958年7月  |
| 第三任 | [徐藹諸](../Page/徐藹諸.md "wikilink") | 1958年8月  | 1978年7月  |
| 第四任 | [朱寶金](../Page/朱寶金.md "wikilink") | 1978年2月  | 1981年1月  |
| 第五任 | [沈 蓉](../Page/沈_蓉.md "wikilink") | 1981年2月  | 1990年7月  |
| 第六任 | [林壽惠](../Page/林壽惠.md "wikilink") | 1990年8月  | 2001年5月  |
| 第七任 | [鐘聿琳](../Page/鐘聿琳.md "wikilink") | 2001年3月  | 2009年10月 |
| 第八任 | [黃秀梨](../Page/黃秀梨.md "wikilink") | 2009年11月 | 2013年8月  |
| 第九任 | [謝楠楨](../Page/謝楠楨.md "wikilink") | 2013年8月  | 至今       |

## 教學單位

| <span style="font-size:12pt;"> **護理學院** | 護理系（含二技日間班、二技進修班、四技、碩士班、碩士在職專班、博士班、學士後） | 護理助產與婦女健康系（含二技、護理助產碩士班） |
| --------------------------------------- | --------------------------------------- | ----------------------- |
| 中西醫結合護理研究所（碩士班）                         | 醫護教育暨數位學習系（含碩士班）                        |                         |
| 護理學院國際碩士班                               | 高齡健康照護系                                 |                         |

| <span style="font-size:12pt;"> **健康科技學院** | 健康事業管理系（含二技日間班、二技進修班、四技、碩士班、碩士在職專班、國際班） | 資訊管理系（含四技、碩士班） |
| ----------------------------------------- | --------------------------------------- | -------------- |
| 休閒產業與健康促進系（含四技、碩士班）                       | 長期照護系（含二技、碩士班、學士後長期照護學位學程）              |                |
| 語言治療與聽力學系（含四技、碩士班）                        |                                         |                |

| <span style="font-size:12pt;"> **人類發展與健康學院** | 嬰幼兒保育系（含二技、四技、碩士班） | 運動保健系（含四技、碩士班、碩士在職專班） |
| -------------------------------------------- | ------------------ | --------------------- |
| 生死與健康心理諮商系（含四技、碩士班）                          |                    |                       |

**北區技專校院教學資源中心**

  - 以「北區技專校院教學資源中心」統合各校教學資源，以整合[技職教育與追求卓越教學績效](../Page/技職教育.md "wikilink")，現結合北區共39所辦學績優之[技專院校](../Page/技專院校.md "wikilink")，期許在尊重各校特色發展與自主性的原則下，以建立資源共享平台，營造優質教學環境，提升北區技職教育教學品質。透過全面性的[輔導規劃](../Page/輔導.md "wikilink")，群策群力共同開創技職教育的新風貌。

**城區部聽語中心**

  - 語言治療與聽力學系附設之聽語中心，位於內江街的城區部，提供[語言障礙](../Page/語言障礙.md "wikilink")、[言語障礙](../Page/言語障礙.md "wikilink")、[嗓音異常](../Page/嗓音異常.md "wikilink")、[吞嚥障礙](../Page/吞嚥障礙.md "wikilink")、[聽力及](../Page/聽力.md "wikilink")[聽覺復健之服務](../Page/聽覺復健.md "wikilink")。

## 合作醫院

|                                                          |                                                                                      |
| -------------------------------------------------------- | ------------------------------------------------------------------------------------ |
| [臺北市立聯合醫院](../Page/臺北市立聯合醫院.md "wikilink")               |                                                                                      |
| [臺北榮民總醫院](../Page/臺北榮民總醫院.md "wikilink")                 | 位於[臺北市](../Page/臺北市.md "wikilink")[北投區](../Page/北投區.md "wikilink")，於國立臺北護理健康大學校本部隔壁。 |
| [國立臺灣大學醫學院附設醫院](../Page/國立臺灣大學醫學院附設醫院.md "wikilink")     |                                                                                      |
| [國立臺灣大學醫學院附設醫院北護分院](../Page/國立臺灣大學醫學院附設醫院.md "wikilink") | 位於[臺北市](../Page/臺北市.md "wikilink")[萬華區](../Page/萬華區.md "wikilink")，於國立臺北護理健康大學城區部隔壁。 |
| [天主教耕莘醫院](../Page/天主教耕莘醫院.md "wikilink")                 | 位於[新北市](../Page/新北市.md "wikilink")[新店區](../Page/新店區.md "wikilink")。                  |

## 姊妹校

國立臺北護理健康大學目前與共46所國外大學院校締結為姊妹校

<table>
<thead>
<tr class="header">
<th><p>非洲地區</p></th>
<th><ul>
<li><strong></strong>：<a href="../Page/國立健康大學.md" title="wikilink">國立健康大學</a></li>
</ul></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北美洲地區</p></td>
<td><ul>
<li><strong></strong><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a>：<a href="../Page/聖地牙哥州立大學.md" title="wikilink">聖地牙哥州立大學</a></li>
<li><strong></strong><a href="../Page/馬里蘭州.md" title="wikilink">馬里蘭州</a>：<a href="../Page/馬里蘭大學.md" title="wikilink">馬里蘭大學</a></li>
<li><strong></strong><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a>：<a href="../Page/聖道大學.md" title="wikilink">聖道大學</a></li>
<li><strong></strong><a href="../Page/華盛頓州.md" title="wikilink">華盛頓州</a>：<a href="../Page/華盛頓大學.md" title="wikilink">華盛頓大學</a></li>
<li><strong></strong><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a>：<a href="../Page/南加州大學.md" title="wikilink">南加州大學</a></li>
<li><strong></strong><a href="../Page/俄亥俄州.md" title="wikilink">俄亥俄州</a>：<a href="../Page/邁阿密大學.md" title="wikilink">邁阿密大學</a></li>
<li><strong></strong><a href="../Page/密西根州.md" title="wikilink">密西根州</a>：<a href="../Page/密西根大學.md" title="wikilink">密西根大學</a></li>
<li><strong></strong><a href="../Page/奧克拉荷馬州.md" title="wikilink">奧克拉荷馬州</a>：<a href="../Page/奧克拉荷馬城市大學.md" title="wikilink">奧克拉荷馬城市大學</a></li>
<li><strong></strong><a href="../Page/密西根州.md" title="wikilink">密西根州</a>：<a href="../Page/東密歇根大學.md" title="wikilink">東密歇根大學</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>歐洲地區</p></td>
<td><ul>
<li><strong></strong><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a>：<a href="../Page/阿爾斯特大學.md" title="wikilink">阿爾斯特大學</a></li>
<li><strong></strong><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a>：<a href="../Page/貝爾法斯特女王大學.md" title="wikilink">貝爾法斯特女王大學</a></li>
<li><strong></strong><a href="../Page/英格蘭.md" title="wikilink">英格蘭</a>：<a href="../Page/貝德福德大學.md" title="wikilink">貝德福德大學</a></li>
<li><strong></strong><a href="../Page/摩拉維亞-西利西亞州.md" title="wikilink">摩拉維亞-西利西亞州</a>：<a href="../Page/奧斯特拉瓦大學.md" title="wikilink">奧斯特拉瓦大學</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>大洋洲地區</p></td>
<td><ul>
<li><strong></strong><a href="../Page/坎培拉.md" title="wikilink">坎培拉</a>：<a href="../Page/坎培拉大學.md" title="wikilink">坎培拉大學</a></li>
<li><strong></strong><a href="../Page/昆士蘭省.md" title="wikilink">昆士蘭省</a>：<a href="../Page/昆士蘭科技大學.md" title="wikilink">昆士蘭科技大學</a></li>
<li><strong></strong><a href="../Page/昆士蘭省.md" title="wikilink">昆士蘭省</a>：<a href="../Page/格瑞弗大學.md" title="wikilink">格瑞弗大學</a></li>
<li><strong></strong><a href="../Page/維多利亞省.md" title="wikilink">維多利亞省</a>：<a href="../Page/迪肯大學.md" title="wikilink">迪肯大學</a></li>
<li><strong></strong><a href="../Page/奧克蘭都會區.md" title="wikilink">奧克蘭都會區</a>：<a href="../Page/瑪努考技術健康大學.md" title="wikilink">瑪努考技術健康大學</a></li>
<li><strong></strong><a href="../Page/奧克蘭都會區.md" title="wikilink">奧克蘭都會區</a>：<a href="../Page/奧克蘭大學.md" title="wikilink">奧克蘭大學</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>亞洲地區</p></td>
<td><ul>
<li><strong></strong><a href="../Page/北京.md" title="wikilink">北京</a>：<a href="../Page/北京中醫藥大學.md" title="wikilink">北京中醫藥大學</a></li>
<li><strong></strong>：<a href="../Page/新加坡國立大學.md" title="wikilink">新加坡國立大學</a></li>
<li><strong></strong><a href="../Page/東京.md" title="wikilink">東京</a>：<a href="../Page/首都大學東京.md" title="wikilink">首都大學東京</a></li>
<li><strong></strong><a href="../Page/首爾.md" title="wikilink">首爾</a>：<a href="../Page/首爾女子護理大學.md" title="wikilink">首爾女子護理大學</a></li>
<li><strong></strong>：<a href="../Page/環球領袖大學.md" title="wikilink">環球領袖大學</a></li>
<li><strong></strong><a href="../Page/戈壁阿爾泰省.md" title="wikilink">戈壁阿爾泰省</a>：<a href="../Page/戈壁阿爾泰健康科學大學.md" title="wikilink">戈壁阿爾泰健康科學大學</a></li>
<li><strong></strong>：<a href="../Page/歐圖曼羅巴大學.md" title="wikilink">歐圖曼羅巴大學</a></li>
<li><strong></strong><a href="../Page/孔敬.md" title="wikilink">孔敬</a>：<a href="../Page/亞洲學者健康大學.md" title="wikilink">亞洲學者健康大學</a></li>
<li><strong></strong><a href="../Page/清邁.md" title="wikilink">清邁</a>：<a href="../Page/清邁大學.md" title="wikilink">清邁大學</a></li>
<li><strong></strong><a href="../Page/順化市.md" title="wikilink">順化市</a>：<a href="../Page/順化醫藥大學.md" title="wikilink">順化醫藥大學</a></li>
<li><strong></strong><a href="../Page/茶榮市.md" title="wikilink">茶榮市</a>：<a href="../Page/茶榮大學.md" title="wikilink">茶榮大學</a></li>
<li><strong></strong><a href="../Page/日惹.md" title="wikilink">日惹</a>：<a href="../Page/亞沙葉健康科技大學.md" title="wikilink">亞沙葉健康科技大學</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學梭羅分校.md" title="wikilink">穆罕默德大學梭羅分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學助產學院井裡汶分校.md" title="wikilink">穆罕默德大學助產學院井裡汶分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學普禾加多分校.md" title="wikilink">穆罕默德大學普禾加多分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學雅加達分校.md" title="wikilink">穆罕默德大學雅加達分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學三寶瓏分校.md" title="wikilink">穆罕默德大學三寶瓏分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學西蘇門答臘分校.md" title="wikilink">穆罕默德大學西蘇門答臘分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德健康學院昂望分校.md" title="wikilink">穆罕默德健康學院昂望分校</a></li>
<li><strong></strong>：<a href="../Page/穆罕默德大學坦格朗分校.md" title="wikilink">穆罕默德大學坦格朗分校</a></li>
<li><strong></strong>：<a href="../Page/艾西亞助產學院萬丹分校.md" title="wikilink">艾西亞助產學院萬丹分校</a></li>
<li><strong></strong>：<a href="../Page/艾西亞健康學院梭羅分校.md" title="wikilink">艾西亞健康學院梭羅分校</a></li>
<li><strong></strong>：<a href="../Page/艾西亞助產學院坤甸分校.md" title="wikilink">艾西亞助產學院坤甸分校</a></li>
<li><strong></strong>：<a href="../Page/奴薩塔拉第一健康學院武吉丁宜分校.md" title="wikilink">奴薩塔拉第一健康學院武吉丁宜分校</a></li>
<li><strong></strong>：<a href="../Page/德翰生健康學院蘇門答臘明古魯分校.md" title="wikilink">德翰生健康學院蘇門答臘明古魯分校</a></li>
<li><strong></strong>：<a href="../Page/健康學院蘇門答臘巴東分校.md" title="wikilink">健康學院蘇門答臘巴東分校</a></li>
<li><strong></strong><a href="../Page/穆哈拉格.md" title="wikilink">穆哈拉格</a>：<a href="../Page/愛爾蘭皇家醫學院巴林分校.md" title="wikilink">愛爾蘭皇家醫學院巴林分校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 海外研修

國立臺北護理健康大學與多所國外大學院校簽署協定，每年定期送學生赴海外研修

|                                                 |                     |
| ----------------------------------------------- | ------------------- |
| ****[新加坡國立大學](../Page/新加坡國立大學.md "wikilink")    | 本校護理系學生             |
| ****[首爾女子護理大學](../Page/首爾女子護理大學.md "wikilink")  | 本校護理系、幼保系及健管系學生     |
| ****[艾西亞健康學院](../Page/艾西亞健康學院.md "wikilink")    | 本校護理系學生             |
| ****[福建醫科大學](../Page/福建醫科大學.md "wikilink")      | 本校大學部學生             |
| ****[南方醫科大學](../Page/南方醫科大學.md "wikilink")      | 本校學生（含研究所）          |
| ****[歐洲聯盟語言治療暑期學校](../Page/歐洲聯盟.md "wikilink")  | 本校語言治療與聽力學系學生（含研究所） |
| ****[南加州大學醫學院附設醫院](../Page/南加州大學.md "wikilink") | 本校語言治療與聽力學系碩士班學生    |

## 雙聯學位

國立臺北護理健康大學目前與共3所國外大學院校簽署雙聯學位協定

<table>
<thead>
<tr class="header">
<th><p>護理系</p></th>
<th><ul>
<li><strong></strong>：<a href="../Page/貝德福德大學.md" title="wikilink">貝德福德大學</a></li>
<li><strong></strong>：<a href="../Page/昆士蘭科技大學.md" title="wikilink">昆士蘭科技大學</a></li>
</ul></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>健康事業管理系</p></td>
<td><ul>
<li><strong></strong>：<a href="../Page/迪肯大學.md" title="wikilink">迪肯大學</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 知名校友

  - [雨玄](../Page/雨玄.md "wikilink")（模特兒）\[1\]
  - [張海倫](../Page/張海倫.md "wikilink")
  - [阿爆](../Page/阿爆.md "wikilink")

## 相關條目

  - [國立臺灣大學醫學院附設醫院](../Page/國立臺灣大學醫學院附設醫院.md "wikilink")
  - [國立臺北護理健康大學城區部文教大樓](../Page/國立臺北護理健康大學城區部文教大樓.md "wikilink")

## 註釋

## 外部連結

  - [國立臺北護理健康大學](http://www.ntunhs.edu.tw)

[\*](../Category/國立臺北護理健康大學.md "wikilink")
[Category:台北市大專院校](../Category/台北市大專院校.md "wikilink")
[Category:臺灣醫學院校](../Category/臺灣醫學院校.md "wikilink")
[Category:北投區學校](../Category/北投區學校.md "wikilink")
[Category:石牌](../Category/石牌.md "wikilink")
[Category:萬華區學校](../Category/萬華區學校.md "wikilink")
[Category:1947年創建的教育機構](../Category/1947年創建的教育機構.md "wikilink")
[Category:护理教育](../Category/护理教育.md "wikilink")
[Category:1947年台灣建立](../Category/1947年台灣建立.md "wikilink")

1.  [臺北護理健康大學竟出了此等隱藏版S級女神！](https://tw.news.yahoo.com/%E3%80%90%E5%A8%9B%E6%A8%82%E6%98%9F%E6%AD%A3%E5%A6%B9%E3%80%91%E8%87%BA%E5%8C%97%E8%AD%B7%E7%90%86%E5%81%A5%E5%BA%B7%E5%A4%A7%E5%AD%B8%E7%AB%9F%E5%87%BA%E4%BA%86%E6%AD%A4%E7%AD%89%E9%9A%B1%E8%97%8F%E7%89%88s%E7%B4%9A%E5%A5%B3%E7%A5%9E%EF%BC%81%E3%80%8A%E5%9C%8B%E5%85%89%E5%B9%AB%E5%B9%AB%E5%BF%99%E3%80%8B%E6%AD%A3%E5%A6%B9%E4%BE%86%E8%B3%93%EF%BC%8C%E7%BE%8E%E8%89%B7%E5%8B%95%E4%BA%BA%E9%9F%93%E7%B3%BB%E8%AD%B7%E7%90%86%E6%AD%A3%E5%A6%B9-%E9%9B%A8%E7%8E%84-113320423.html)【娛樂星正妹】《國光幫幫忙》正妹來賓，美艷動人韓系護理正妹
    雨玄