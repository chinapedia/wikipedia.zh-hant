**Oricon**（）是以提供等娛樂資訊服務為主要業務的[日本企業](../Page/日本.md "wikilink")，其名稱來自[英文](../Page/英文.md "wikilink")「」（絕對的信賴）的[縮寫](../Page/縮寫.md "wikilink")，也因此該公司的主要業務——[影音產品銷售排行榜在](../Page/影音.md "wikilink")[中文社群被稱為](../Page/中文.md "wikilink")「**公信榜**」。現任代表取締役社長（[總經理](../Page/總經理.md "wikilink")）為[小池恒](../Page/小池恒.md "wikilink")，是公司創辦者[小池聰行的長子](../Page/小池聰行.md "wikilink")。

該公司公佈的[Oricon公信榜](../Page/Oricon公信榜.md "wikilink")，是日本現時最具知名度及公信力的[音樂排行榜](../Page/音樂.md "wikilink")。銷量榜共提供每天、每星期、每月等不同計算方法的銷量榜，也提供諸如[流行曲](../Page/流行曲.md "wikilink")、[演歌](../Page/演歌.md "wikilink")、[西洋音樂等不同的音樂類型的排行榜](../Page/西洋音樂.md "wikilink")，甚至更會公佈[DVD及](../Page/DVD.md "wikilink")[電視遊戲等的銷量榜](../Page/電視遊戲.md "wikilink")，以及由觀眾的[電視廣告好感度等](../Page/電視廣告.md "wikilink")。除此之外其透過[子公司Oricon](../Page/子公司.md "wikilink")
ME出版[雜誌](../Page/雜誌.md "wikilink")。

## 沿革

  - 1999年10月 -
    Oricon株式會社（現Oricon娛樂株式會社）中的音樂[資料庫](../Page/資料庫.md "wikilink")・[手機鈴聲部門從母公司分離](../Page/手機鈴聲.md "wikilink")，以**Oricon直營數碼株式會社**（）名字成立新公司。
  - 2000年11月 - 在[大阪證券交易所Nasdaq](../Page/大阪證券交易所.md "wikilink")
    Japan市場（現[hercules市場](../Page/hercules.md "wikilink")）上市。
  - 2001年6月 - 公司名稱更改為**Oricon全球娛樂株式會社**（），Oricon株式會社改屬旗下。
  - 2002年4月 - 手機鈴聲業務再次交與Oricon株式會社，同時從Oricon株式會社接收排行榜業務。
  - 2002年7月 - 公司更改為現時名稱。
  - 2005年10月 - 把音樂資料庫業務以Oricon市場推廣株式會社名字分拆，使Oricon株式會社成為持股公司。
  - 2015年10月 - 將子公司併入。

## 刑事訴訟案件

由[Infobahn發行的](../Page/Infobahn.md "wikilink")[月刊](../Page/月刊.md "wikilink")《》，在2006年的4月號中，揭載了一段由音樂記者烏賀陽弘道從《才藏》編輯部撥打到Oricon來進行的電話採訪，質疑Oricon銷量榜的公信力的發言。Oricon以該段發言毫無根據使其名譽受損，在2006年11月17日向東京地方法院提出民事訴訟，但並非向出版社、執筆者及編輯，而只是向當時進行電話採訪的烏賀陽個人要求5000萬日圓的[損害賠償](../Page/損害賠償.md "wikilink")。對於這次訴訟，Oricon普遍受到坊間指責，指他們如之前[武富士一樣](../Page/武富士.md "wikilink")，封殺言論自由。而Oricon也承認他們當初決定要提出高額索償的訴訟，是為了要發揮自己的影響力。案件原定於2007年1月9日早上10時開始，在東京地方法院第12部第709號法庭作第一次口頭辯論，但基於烏賀陽方面的要求而押後至2月13日的下午1時10分，烏賀陽並申請作大約5分鐘左右的口頭陳述。

## 參考文獻

## 外部連結

  - [Oricon公司網站](https://www.oricon.jp/)
  - [ORICON NEWS](http://www.oricon.co.jp/) - Oricon旗下娛樂資訊入口網站
  - [ORICON BiZ online](http://biz.oricon.co.jp/) - Oricon旗下娛樂營銷服務網站

[Oricon](../Category/Oricon.md "wikilink")
[Category:日本音樂](../Category/日本音樂.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:音樂網站](../Category/音樂網站.md "wikilink")
[Category:日本新聞網站](../Category/日本新聞網站.md "wikilink")