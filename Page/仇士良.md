**仇士良**（），字**匡美**。循州興寧（今[廣東](../Page/廣東.md "wikilink")[興寧北](../Page/兴宁市.md "wikilink")）人。[唐文宗時](../Page/唐文宗.md "wikilink")[宦官](../Page/宦官.md "wikilink")。

## 生平

[唐順宗時進宮](../Page/唐順宗.md "wikilink")，侍[太子東宮](../Page/太子東宮.md "wikilink")。[唐憲宗時](../Page/唐憲宗.md "wikilink")，任內給事，出為[平盧](../Page/平盧.md "wikilink")、[鳳翔](../Page/鳳翔.md "wikilink")[監軍](../Page/監軍.md "wikilink")，復入為五坊使。文宗大和九年（835年）為神策左軍[中尉](../Page/中尉.md "wikilink")。

### 甘露之變

文宗近臣宰相[李訓](../Page/李訓.md "wikilink")、[凤翔节度使](../Page/凤翔节度使.md "wikilink")[鄭注等謀誅宦官](../Page/鄭注.md "wikilink")，仇士良發現，立刻劫持[天子而走](../Page/天子.md "wikilink")，發兵殺害了李訓、鄭注等。仇士良與右軍中尉[魚弘志等](../Page/魚弘志.md "wikilink")，以[神策軍等誅戮朝臣](../Page/神策軍.md "wikilink")，殺四宰相，史称[甘露之变](../Page/甘露之变.md "wikilink")。從此唐朝宦官更為專橫。宰相[李石為士良所忌](../Page/李石_\(唐朝\).md "wikilink")，遣刺客刺之。李石幸而不死，懼而辭職。

### 挾持天子

文宗更受到家奴歧視，一次問當值學士[周墀](../Page/周墀.md "wikilink")：“朕可方前代何主？”墀答：“陛下[堯](../Page/堯.md "wikilink")、[舜之主也](../Page/舜.md "wikilink")。”文宗嘆道，“朕豈敢比堯、舜，何如[周赧](../Page/周赧王.md "wikilink")，[漢獻耳](../Page/漢獻帝.md "wikilink")！”墀曰：“彼亡國之主，豈可比圣德？”文宗曰：“赧、獻受制于強諸侯，今朕受制于家奴，以此言之，朕殆不如！”開成五年（840年），文宗鬱鬱而死。文宗生前因亲子全部去世而立兄敬宗子陈王[李成美为太子](../Page/李成美.md "wikilink")，而[杨贤妃和宰相](../Page/楊賢妃_\(唐文宗\).md "wikilink")[杨嗣复又曾倡立皇弟安王](../Page/杨嗣复.md "wikilink")[李溶](../Page/李溶.md "wikilink")。仇士良为图拥立之功，矫诏仍废太子为陈王，改立安王弟颍王[李瀍为](../Page/李瀍.md "wikilink")[皇太弟](../Page/皇太弟.md "wikilink")，说服其赐死陈王、安王、杨贤妃，拥立其为[唐武宗](../Page/唐武宗.md "wikilink")。故史称仇士良于拥立武宗之际，杀二王一妃。

### 被迫離朝

唐武宗時，[李德裕為](../Page/李德裕.md "wikilink")[宰相](../Page/宰相.md "wikilink")，士良有擁立之功，表面上示以尊寵，實抑其權。[会昌二年](../Page/會昌_\(唐朝\).md "wikilink")（842年），仇士良藉故試圖發動[神策軍殺李德裕](../Page/神策軍.md "wikilink")，李德裕得知之後逃到武宗處，武宗出面以[滅族恐嚇](../Page/滅族.md "wikilink")，士兵於是不敢造次，士良知大勢去矣。

會昌三年（843年）五月，迫士良以內侍監致仕。離朝時，士良叮囑送行的宦官說，須誘使皇帝縱樂，使其無暇讀書和接見朝臣，以鞏固宦官擅政的局面\[1\]。

### 死後抄家

六月，士良卒。有《内侍省监楚国公仇士良神道碑》，载其曾被赐[上柱国](../Page/上柱国.md "wikilink")、进封**南安县开国男**，食邑三百户。次年因被檢舉家藏武器，下詔削官爵，籍沒其家。

长子仇从广，次子仇亢宗，可能是养子。

## 注釋

<div class="references-small">

<references />

</div>

[Category:唐朝国公](../Category/唐朝国公.md "wikilink")
[Category:唐朝宦官](../Category/唐朝宦官.md "wikilink")
[Category:唐朝武官](../Category/唐朝武官.md "wikilink")
[Category:興寧人](../Category/興寧人.md "wikilink")
[S](../Category/仇姓.md "wikilink")
[Category:唐朝上柱国](../Category/唐朝上柱国.md "wikilink")

1.  士良曰：「天子不可令閑暇，暇必觀書，見儒臣，則又納諫，智深慮遠，減玩好，省游幸，吾屬恩且薄而權輕矣。為諸君計，莫若殖財貨，盛鷹馬，日以毬獵聲色蠱其心，極侈靡，使悅不知息，則必斥經術，闇外事，萬機在我，恩澤權力欲焉往哉？」眾再拜。（《新唐書·仇士良列傳》）