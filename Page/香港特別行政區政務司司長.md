**政務司司長**（，[缩写为](../Page/缩写.md "wikilink")）是[香港特別行政區政府中](../Page/香港特別行政區政府.md "wikilink")，地位僅次於[行政長官的排名第一的](../Page/香港特別行政區行政長官.md "wikilink")[主要官員](../Page/香港特別行政區主要官員.md "wikilink")，其支援部門為政務司司長辦公室。司長由[行政長官提名](../Page/香港特別行政區行政長官.md "wikilink")，報[國務院](../Page/中華人民共和國國務院.md "wikilink")（中央人民政府）任命。

政務司司長現屬[行政會議官守成員](../Page/香港行政會議.md "wikilink")，也是可以[署理行政長官職務的三名](../Page/署理.md "wikilink")[司長中最高级的一位](../Page/香港政府行政部門#司長.md "wikilink")\[1\]。政務司司長休假時，會由一眾由政務司司長領導的局長中年資最久的一位署任，為香港政府中人事變動最多的官守成員，回歸至今已先後有7位不同人士出任。

## 歷史

政務司司長的職權源自[香港殖民地時代的](../Page/英屬香港.md "wikilink")**布政司**（早期稱為**輔政司**，英文為Colonial
Secretary，1976年8月27日改稱Chief
Secretary\[2\]），為[香港政府](../Page/香港政府.md "wikilink")[首長](../Page/行政首長.md "wikilink")、[香港首席公務員](../Page/香港公務員.md "wikilink")、[行政局及](../Page/行政局.md "wikilink")[立法局當然議員](../Page/香港立法局.md "wikilink")，統領[布政司署](../Page/布政司署.md "wikilink")（早期稱為輔政司署）和全體政府[公務員](../Page/香港公務員.md "wikilink")，執行[香港總督會同行政局的決定](../Page/香港總督會同行政局.md "wikilink")。

1985年初設立**副布政司**，負責協調涉及兩個科或以上的事務，和處理有關[政制](../Page/香港政治.md "wikilink")、[選舉等事務](../Page/香港選舉制度.md "wikilink")。**副布政司**的工作單位稱為**副布政司辦公室**。1989年，**憲制事務科**成立，**副布政司**改稱為**憲制事務司**，即現時[政制及內地事務局的前身](../Page/政制及內地事務局.md "wikilink")。

1997年7月1日[香港主權移交後](../Page/香港主權移交.md "wikilink")，改名“政務司司長”，英文改稱“Chief
Secretary for
Administration”\[3\]；大致沿襲布政司的職務，但政府首長之位由行政長官取代，亦不再是[行政會議當然成員](../Page/香港特別行政區行政會議.md "wikilink")；布政司署則改稱[政府總部](../Page/香港特別行政區政府總部.md "wikilink")。雖然根據《[香港基本法](../Page/香港基本法.md "wikilink")》第60條，「……香港特別行政區政府設政務司、財政司、律政司和各局、處、署。」，但香港特區政府從無設「政務司」的部門或職位，而政務司司長的支援部門則名為「政務司司長辦公室」而非「政務司」，有別於[律政司的設立](../Page/律政司.md "wikilink")。

2002年7月1日以前，政務司司長屬於政府公務員，為公務員中最高職位，各局局長亦為其下屬，政治及行政地位在各局局長之上。但前行政長官[董建華](../Page/董建華.md "wikilink")（1997年至2005年任香港特首）推行[高官問責制後](../Page/高官問責制.md "wikilink")，政務司司長成為了政治任命官員，各局長改為直接向行政長官負責。有人認為，政務司司長職責减少，只是負責行政長官所指定的工作，沒有固定範疇，沒有行政長官的命令亦不可干涉各政策局，只是排名上和薪金比各局長高一些，曾被一些人戲稱為沒有實權的「無兵司令」。

2005年6月，曾任政務司司長的新任行政長官[曾蔭權上任](../Page/曾蔭權.md "wikilink")，表明政務司司長將重新在政府擔任較重要的政治角色和職權。

## 歷任

### 輔政司（1843－1976）

1.  [麻恭](../Page/麻恭.md "wikilink")（1843年6月27日－1844年5月9日）
      - [馬儒翰](../Page/馬儒翰.md "wikilink")（署理）（1843年8月21日－1843年8月29日）（任內逝世）
2.  [卜魯斯](../Page/卜魯斯.md "wikilink")（1844年5月10日－1846年）
3.  [威廉·堅](../Page/威廉·堅.md "wikilink")（1846年－1854年4月12日）
4.  [孖沙](../Page/孖沙.md "wikilink")（1854年4月13日－1867年5月14日）
      - 巴爾（[署理](../Page/署理.md "wikilink")）（Henry John
        Ball）（1867年5月14日－1867年7月13日）
      - [史密士](../Page/史密士.md "wikilink")（署理）（1867年7月13日－1868年5月7日）
5.  [柯士甸](../Page/柯士甸_\(香港輔政司\).md "wikilink")（1868年5月7日－1878年4月4日）
      - [裴樂士](../Page/裴樂士.md "wikilink")（署理）（J. M.
        Price）（1878年4月4日－1878年9月16日）
      - [查理士·梅理](../Page/查理士·梅理.md "wikilink")（署理）（1878年9月16日－1879年1月3日）
6.  [馬師](../Page/馬師.md "wikilink")（1879年1月3日－1885年6月10日)
      - [史釗域](../Page/史釗域_\(香港輔政司\).md "wikilink")（署理）（1885年6月10日 -
        1887年10月5日）
7.  史釗域（1887年10月5日－1889年10月6日）（任內逝世）
      - [利斯特](../Page/利斯特.md "wikilink")（署理）（Alfred
        Lister）（1889年10月7日－1890年1月17日）
8.  [菲林明](../Page/菲林明.md "wikilink")（1890年1月17日－1891年2月26日）
      - [田尼](../Page/田尼.md "wikilink")（署理）（1891年2月26日－1891年5月30日）
      - [古德曼](../Page/古德曼.md "wikilink")（署理）（William Meigh
        Goodman）（1891年5月30日－1892年3月11日）
9.  [柯布連](../Page/柯布連.md "wikilink")（1892年3月11日－1894年4月30日）
      - [駱克](../Page/駱克.md "wikilink")（署理）（1894年4月30日－1895年3月26日）
10. 駱克（1895年3月26日 - 1902年4月23日）
      - [譚臣](../Page/譚臣.md "wikilink")（署理）（1902年4月23日－1902年5月14日）
11. [梅含理](../Page/梅含理.md "wikilink")（1902年5月14日－1910年4月30日）
      - [譚臣](../Page/譚臣.md "wikilink")（署理）（1910年4月30日－1911年2月22日）
      - [金文泰](../Page/金文泰.md "wikilink")（署理）（1911年2月22日－1911年6月7日)
12. [班士](../Page/班士.md "wikilink")（Warren Delabere
    Barnes）（1911年6月7日－1911年10月28日）（任內逝世）
      - [蒲魯賢](../Page/蒲魯賢.md "wikilink")（署理）（1911年10月30日－1911年11月28日）
      - [金文泰](../Page/金文泰.md "wikilink")（署理）（1911年11月28日－1912年2月2日）
13. [施勳](../Page/施勳.md "wikilink")（1912年2月2日－1925年11月14日）
      - [符烈槎](../Page/符烈槎.md "wikilink")（署理）（1925年11月14日－1926年1月9日）
      - [夏理德](../Page/夏理德.md "wikilink")（署理）（Edwin Richard
        Hallifax）（1926年1月9日－1926年5月1日）
14. [修頓](../Page/修頓.md "wikilink")（1926年5月1日－1936年3月23日）
      - [那魯麟](../Page/那魯麟.md "wikilink")（署理）Ronald Arthur Charles
        North）（1936年3月23日－1936年8月25日）
      - [富勵士](../Page/富勵士.md "wikilink")（署理）（1936年8月25日 - 1936年9月8日）
      - [那魯麟](../Page/那魯麟.md "wikilink")（署理）（1936年9月8日 - 1936年11月26日）
15. [史美](../Page/史美.md "wikilink")（1936年11月26日 - 1941年12月8日）
16. [詹遜](../Page/詹遜.md "wikilink")（1941年12月8日 - 1941年12月25日）
    *[香港日治時期](../Page/香港日治時期.md "wikilink")（1941年12月25日－1945年8月30日）*
17. [麥道高](../Page/麥道高.md "wikilink")（1946年5月1日 - 1949年5月）
      - [杜德](../Page/杜德.md "wikilink")（署理）（1946年6月－1947年1月）
18. [列誥](../Page/列誥.md "wikilink")（1949年5月16日－1952年2月）
      - [杜德](../Page/杜德.md "wikilink")（署理）（1952年2月）
19. [柏立基](../Page/柏立基.md "wikilink")（1952年2月13日－1955年3月）
      - [侯士](../Page/侯士.md "wikilink")（署理）（Richard John Clyde
        Howes）(1955年4月－1955年4月）
20. [戴維德](../Page/戴維德.md "wikilink")（1955年4月14日－1957年12月）
      - [白嘉時](../Page/白嘉時.md "wikilink")（署理）（1957年12月－1958年1月）
21. [白嘉時](../Page/白嘉時.md "wikilink")（1958年1月24日－1963年3月10日 ）
22. [戴斯德](../Page/戴斯德.md "wikilink")（1963年3月11日－1965年3月28日）
      - [韓美洵](../Page/韓美洵.md "wikilink")（署理）（Geoffrey Cadzow
        Hamilton）（1965年3月29日－1965年9月1日）
23. [祈濟時](../Page/祈濟時.md "wikilink")（1965年9月2日 －1969年1月22日）
      - [韓美洵](../Page/韓美洵.md "wikilink")（署理）（Geoffrey Cadzow
        Hamilton）（1969年1月23日－1969年3月27日）
24. [羅樂民](../Page/羅樂民.md "wikilink")（1969年3月28日－1973年9月29日）
25. [羅弼時](../Page/羅弼時.md "wikilink")（1973年9月30日－1976年8月26日\[4\]）

### 布政司（1976－1997）

<table>
<thead>
<tr class="header">
<th><p>任</p></th>
<th><p>肖像</p></th>
<th><p>姓名</p></th>
<th><p>任期</p></th>
<th><p>香港總督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sir_Denys_Roberts.jpg" title="fig:Sir_Denys_Roberts.jpg">Sir_Denys_Roberts.jpg</a></p></td>
<td><p><a href="../Page/羅弼時.md" title="wikilink">羅弼時</a></p></td>
<td><p>1976年8月27日－1978年10月2日</p></td>
<td><p><a href="../Page/麥理浩.md" title="wikilink">麥理浩</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sir_Jack_Cater.jpg" title="fig:Sir_Jack_Cater.jpg">Sir_Jack_Cater.jpg</a></p></td>
<td><p><a href="../Page/姬達.md" title="wikilink">姬達</a></p></td>
<td><p>1978年10月3日－1981年11月19日</p></td>
<td><p><a href="../Page/麥理浩.md" title="wikilink">麥理浩</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Charles_Philip_Haddon-Cave.jpg" title="fig:Charles_Philip_Haddon-Cave.jpg">Charles_Philip_Haddon-Cave.jpg</a></p></td>
<td><p><a href="../Page/夏鼎基.md" title="wikilink">夏鼎基</a></p></td>
<td><p>1981年11月20日－1985年6月9日</p></td>
<td><p><a href="../Page/麥理浩.md" title="wikilink">麥理浩</a><br />
<a href="../Page/尤德.md" title="wikilink">尤德</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:SirDavidJones.jpg" title="fig:SirDavidJones.jpg">SirDavidJones.jpg</a></p></td>
<td><p><a href="../Page/鍾逸傑.md" title="wikilink">鍾逸傑</a></p></td>
<td><p>1985年6月10日－1987年2月11日</p></td>
<td><p><a href="../Page/尤德.md" title="wikilink">尤德</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:David_Robert_Ford.jpg" title="fig:David_Robert_Ford.jpg">David_Robert_Ford.jpg</a></p></td>
<td><p><a href="../Page/霍德.md" title="wikilink">霍德</a></p></td>
<td><p>1987年2月12日－1993年11月28日</p></td>
<td><p><a href="../Page/衛奕信.md" title="wikilink">衛奕信</a><br />
<a href="../Page/彭定康.md" title="wikilink">彭定康</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Anson_Chan_larged_20051219.jpg" title="fig:Anson_Chan_larged_20051219.jpg">Anson_Chan_larged_20051219.jpg</a></p></td>
<td><p><a href="../Page/陳方安生.md" title="wikilink">陳方安生</a></p></td>
<td><p>1993年11月29日－1997年6月30日</p></td>
<td><p><a href="../Page/彭定康.md" title="wikilink">彭定康</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 政務司司長 （1997－）

<table>
<thead>
<tr class="header">
<th><p>任</p></th>
<th><p>肖像</p></th>
<th><p>姓名</p></th>
<th><p>任期</p></th>
<th><p>行政長官</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Anson_Chan_larged_20051219.jpg" title="fig:Anson_Chan_larged_20051219.jpg">Anson_Chan_larged_20051219.jpg</a></p></td>
<td><p><a href="../Page/陳方安生.md" title="wikilink">陳方安生</a></p></td>
<td><p>1997年7月1日－2001年4月30日[5]</p></td>
<td><p><a href="../Page/董建華.md" title="wikilink">董建華</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sir_Donald_Tsang.jpg" title="fig:Sir_Donald_Tsang.jpg">Sir_Donald_Tsang.jpg</a></p></td>
<td><p><a href="../Page/曾蔭權.md" title="wikilink">曾蔭權</a></p></td>
<td><p>2001年5月1日－2005年5月25日[6]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>署任</em></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Michael_Suen_Ming_Yeung.JPG" title="fig:Michael_Suen_Ming_Yeung.JPG">Michael_Suen_Ming_Yeung.JPG</a></p></td>
<td><p><em><a href="../Page/孫明揚.md" title="wikilink">孫明揚</a></em></p></td>
<td><p>2005年5月25日－2005年6月30日</p></td>
<td><p><a href="../Page/唐英年.md" title="wikilink">唐英年</a>（署任）</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Rafael_Hui_Si-yan.JPG" title="fig:Rafael_Hui_Si-yan.JPG">Rafael_Hui_Si-yan.JPG</a></p></td>
<td><p><a href="../Page/許仕仁.md" title="wikilink">許仕仁</a></p></td>
<td><p>2005年7月1日－2007年6月30日</p></td>
<td><p><a href="../Page/曾蔭權.md" title="wikilink">曾蔭權</a></p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Henry_Tang_Ying_Yen_2008.JPG" title="fig:Henry_Tang_Ying_Yen_2008.JPG">Henry_Tang_Ying_Yen_2008.JPG</a></p></td>
<td><p><a href="../Page/唐英年.md" title="wikilink">唐英年</a></p></td>
<td><p>2007年7月1日－2011年9月30日[7]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><em>署任</em></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Michael_Suen_Ming_Yeung.JPG" title="fig:Michael_Suen_Ming_Yeung.JPG">Michael_Suen_Ming_Yeung.JPG</a></p></td>
<td><p><em><a href="../Page/孫明揚.md" title="wikilink">孫明揚</a></em></p></td>
<td><p>2011年9月28日－2011年9月30日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stephen_Lam_Sui-lung_2010.jpg" title="fig:Stephen_Lam_Sui-lung_2010.jpg">Stephen_Lam_Sui-lung_2010.jpg</a></p></td>
<td><p><a href="../Page/林瑞麟.md" title="wikilink">林瑞麟</a></p></td>
<td><p>2011年9月30日－2012年6月30日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Carrie_Lam_2016.jpg" title="fig:Carrie_Lam_2016.jpg">Carrie_Lam_2016.jpg</a></p></td>
<td><p><a href="../Page/林鄭月娥.md" title="wikilink">林鄭月娥</a></p></td>
<td><p>2012年7月1日－2017年1月16日[8]</p></td>
<td><p><a href="../Page/梁振英.md" title="wikilink">梁振英</a></p></td>
</tr>
<tr class="odd">
<td><p><em>署任</em></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Matthew_Cheung_Kin-chung.JPG" title="fig:Matthew_Cheung_Kin-chung.JPG">Matthew_Cheung_Kin-chung.JPG</a></p></td>
<td><p><a href="../Page/張建宗.md" title="wikilink">張建宗</a></p></td>
<td><p>2017年1月13日－2017年1月16日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2017年1月16日－</p></td>
<td><p><a href="../Page/梁振英.md" title="wikilink">梁振英</a><br />
<a href="../Page/林鄭月娥.md" title="wikilink">林鄭月娥</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歷任政務司司長辦公室官員

**政治助理**

  - [張秀麗](../Page/張秀麗.md "wikilink")（2012年11月1日—2013年10月31日）
  - [蕭嘉怡](../Page/蕭嘉怡.md "wikilink")（2017年9月1日—）

**常任秘書長（專責事務）**

  - [黎高穎怡](../Page/黎高穎怡.md "wikilink")（2006年6月—2007年8月）協調香港主權移交十周年大型慶祝活動之前期工作

**政務司司長私人辦公室專員（特別職務）**

  - [聶德權](../Page/聶德權.md "wikilink")（2013年6月—12月）專責人口政策

## 福利及待遇

[Victoria_House_01.JPG](https://zh.wikipedia.org/wiki/File:Victoria_House_01.JPG "fig:Victoria_House_01.JPG")15號[政務司司長官邸](../Page/政務司司長官邸.md "wikilink")\]\]

  - 會獲編配[政務司司長官邸](../Page/政務司司長官邸.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[白加道](../Page/白加道.md "wikilink")15號。
  - 坐駕[專用車牌是](../Page/香港車輛號牌#雙字母.md "wikilink")**CS**，是英語**Chief
    Secretary**的簡寫。
  - 月薪為[港幣](../Page/港幣.md "wikilink")37萬元\[9\]

## 注释

## 参考文献

## 外部链接

  - [香港特別行政區政府 政務司司長](http://www.gov.hk/tc/about/govdirectory/po/cs.htm)
  - [香港特別行政區政府 政務司司長辦公室](http://www.cso.gov.hk)

## 參見

  - [布政司署](../Page/布政司署.md "wikilink")
  - [三司十三局](../Page/三司十三局.md "wikilink")
  - [香港特別行政區主要官員](../Page/香港特別行政區主要官員.md "wikilink")
  - [澳門特別行政區行政法務司司長](../Page/澳門特別行政區行政法務司.md "wikilink")

[香港特別行政區政務司司長](../Category/香港特別行政區政務司司長.md "wikilink")
[Category:香港特别行政区主要官员列表](../Category/香港特别行政区主要官员列表.md "wikilink")

1.  《[香港基本法](../Page/香港基本法.md "wikilink")》第五十三條
2.
3.
4.
5.  提早退休
6.  辭職並準備參選行政長官，2005年5月25日提出請辭及開始休假。
7.  辭職並準備參選行政長官，2011年9月28日提出請辭及開始休假。
8.  辭職並準備參選行政長官，2017年1月12日提出請辭，1月13日開始休假。
9.  [林鄭當選：年薪近500萬元　享優厚福利](http://hk.on.cc/hk/bkn/cnt/news/20170326/bkn-20170326124008398-0326_00822_001.html)