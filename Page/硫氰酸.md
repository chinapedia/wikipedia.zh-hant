**硫氰酸**（[化学式](../Page/化学式.md "wikilink")：HSCN）是[硫氰的氢](../Page/硫氰.md "wikilink")[酸](../Page/酸.md "wikilink")，与[异硫氰酸](../Page/异硫氰酸.md "wikilink")（HNCS）互为异构体。硫氰酸可由[硫氰酸钡和](../Page/硫氰酸钡.md "wikilink")[硫酸反应制得](../Page/硫酸.md "wikilink")：

  -
    Ba(SCN)<sub>2</sub> + H<sub>2</sub>SO<sub>4</sub> →
    BaSO<sub>4</sub>↓ + 2 HSCN

硫氰酸是无色的、极易挥发的[液体](../Page/液体.md "wikilink")，在常温下迅速分解。它在-90°C左右或5%的稀溶液中是稳定的。它易溶于水，水溶液为强酸。它的盐（[硫氰酸盐](../Page/硫氰酸盐.md "wikilink")）很容易制得，也很稳定。

## 参见

  - [硫氰](../Page/硫氰.md "wikilink")
  - [氰酸](../Page/氰酸.md "wikilink")

## 参考文献

  - 《无机化学丛书》编委会. 《[无机化学丛书](../Page/无机化学丛书.md "wikilink")》第五卷. 北京：科学出版社.

[Category:无机酸](../Category/无机酸.md "wikilink")
[\*](../Category/硫氰酸盐.md "wikilink")
[Category:硫化合物](../Category/硫化合物.md "wikilink")
[Category:无机氮化合物](../Category/无机氮化合物.md "wikilink")
[Category:无机碳化合物](../Category/无机碳化合物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")