**保華建業集團有限公司**，簡稱**保華建業集團**，以及**保華建業**（****，港交所除牌前0577），於1946年由[車炳榮創立](../Page/車炳榮.md "wikilink")，屬於一家[香港的國際](../Page/香港.md "wikilink")[工程服務公司](../Page/工程.md "wikilink")，母公司為[德祥企業](../Page/德祥企業.md "wikilink")，業務包括項目管理、[建築管理及產業管理](../Page/建築.md "wikilink")，集團有多個部門，包括樓宇建造、土木工程、地基與專項工程及建築材料。在[香港](../Page/香港.md "wikilink")、[中國及](../Page/中國.md "wikilink")[東南亞地區皆有業務](../Page/東南亞.md "wikilink")。保華建業現時為[保華集團有限公司](../Page/保華集團有限公司.md "wikilink")，以及[路易十三集團有限公司股東](../Page/路易十三集團有限公司.md "wikilink")，持有49%和51%股權。保華建業前主席為[詹伯樂和](../Page/詹伯樂.md "wikilink")[趙雅各](../Page/趙雅各.md "wikilink")[工程師](../Page/工程師.md "wikilink")，前副主席為[劉高原](../Page/劉高原.md "wikilink")。

保華建業承建的主要工程主要是[長江實業發展的物業](../Page/長江實業.md "wikilink")，包括[港鐵](../Page/港鐵.md "wikilink")[調景嶺站](../Page/調景嶺站.md "wikilink")(B區)([城中駅](../Page/城中駅.md "wikilink"))、[亞洲電視舊址重建計劃](../Page/亞洲電視.md "wikilink")([尚御](../Page/尚御.md "wikilink"))、[雍雅軒](../Page/雍雅軒.md "wikilink")、[映灣園](../Page/映灣園.md "wikilink")、[藍澄灣](../Page/藍澄灣.md "wikilink")、[港景峰](../Page/港景峰.md "wikilink")、[半山壹號](../Page/半山壹號.md "wikilink")、[海名軒](../Page/海名軒.md "wikilink")、[鹿茵山莊](../Page/鹿茵山莊.md "wikilink")、[海怡半島](../Page/海怡半島.md "wikilink")、[嵐山等](../Page/嵐山_\(香港\).md "wikilink")\[1\]，其他項目包括[油塘的住宅項目](../Page/油塘.md "wikilink")、[啟德](../Page/啟德.md "wikilink")[龍譽](../Page/龍譽.md "wikilink")、[香港麗嘉酒店舊址重建計劃](../Page/香港麗嘉酒店.md "wikilink")([中國建設銀行大廈](../Page/中國建設銀行大廈.md "wikilink"))、[渠務署廠房保養合約](../Page/香港渠務署.md "wikilink")、[數-{}-碼港](../Page/數碼港.md "wikilink")、[信興廣場](../Page/信興廣場.md "wikilink")、[匯豐中心](../Page/匯豐中心.md "wikilink")、[西九文化區](../Page/西九文化區.md "wikilink")、[廣深港高速鐵路](../Page/廣深港高速鐵路.md "wikilink")[西九龍總站的地盤平整工程等](../Page/西九龍總站.md "wikilink")。

於2011年會計年度，營業額為4.3億港元，[除稅及利息前盈利](../Page/除稅及利息前盈利.md "wikilink")﹝[EBIT](../Page/:en:EBIT.md "wikilink")﹞為5,000萬港元\[2\]。

2011年8月，保華建業宣布與[传奇电影公司及](../Page/传奇电影公司.md "wikilink")[華誼兄弟合作](../Page/華誼兄弟.md "wikilink")，進軍電影市場，保華建業打算以17.2億元購入傳奇娛樂旗下傳奇東方50%股權。保華建業計劃以每股作價0.65港元配售最多31.8億股新股，集資20.7億元。\[3\]同年12月，保華建業終止配股計劃。\[4\]

2012年11月，保華建業宣布以總代價20億元向[洪永時的兒子](../Page/洪永時.md "wikilink")[洪澤禮收購Falloncroft](../Page/洪澤禮.md "wikilink")。Falloncroft於[澳門](../Page/澳門.md "wikilink")[路環持有一幅地皮](../Page/路環.md "wikilink")。\[5\]

保華建業2013年4月4日召開特別股東大會，通過更改公司名稱為[路易十三集團有限公司](../Page/路易十三集團有限公司.md "wikilink")。

## 外部連結

  - [保華建業集團有限公司](http://www.pyengineering.com/)

## 參考

<references/>

[Category:香港建築公司](../Category/香港建築公司.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:1946年成立的公司](../Category/1946年成立的公司.md "wikilink")

1.
2.  <http://www.pyengineering.com/pyeCms/documentLibrary/2011%20AR%20Financial%20Highlights.pdf>
3.  [夥拍傳奇娛樂及華誼攻國際市場
    保華建業轉型拍電影](http://hk.apple.nextmedia.com/financeestate/art/20110822/15544321)
    蘋果日報，2011年8月22日
4.  [保華終止配股
    飆22%](http://hk.apple.nextmedia.com/financeestate/art/20111231/15940772)
    蘋果日報，2011年12月31日
5.  [殼王玩財技 借保華吸金 變身濠賭股
    抽水64億](http://hk.apple.nextmedia.com/financeestate/art/20121121/18074135)
    蘋果日報，2012年11月21日