**吳少陽**（），[滄州](../Page/滄州.md "wikilink")[清池人](../Page/清池.md "wikilink")。[唐代](../Page/唐代.md "wikilink")[申蔡節度使](../Page/申蔡節度使.md "wikilink")。

[申蔡節度使](../Page/申蔡節度使.md "wikilink")[吳少誠之父](../Page/吳少誠.md "wikilink")[吳翔在](../Page/吳翔.md "wikilink")[魏博軍軍中](../Page/魏博軍.md "wikilink")，與吳少陽相識，後吳少誠寵任吳少陽，收為義弟，出入如至親，累遷[申州](../Page/申州.md "wikilink")（今[河南](../Page/河南.md "wikilink")[信陽](../Page/信陽.md "wikilink")）[刺史](../Page/刺史.md "wikilink")。

吳少誠有疾，家僮**鮮於熊兒**詐以少誠命，召少陽攝副使、知軍州事。吳少陽為了鞏固權位，殺少誠之子[吳元慶](../Page/吳元慶_\(唐朝\).md "wikilink")，軟禁少誠。

[元和四年](../Page/元和_\(唐朝\).md "wikilink")（809年）秋十一月吳少誠忧病致死，吳少陽自稱留後，朝廷不能節制，又怕少陽造反，遂同意任命。时吴少阳为彰义军马军先锋兵马使、正议大夫、检校右散骑常侍、使持节申州诸军事、申州刺史、兼[御史大夫](../Page/御史大夫.md "wikilink")、**会稽郡王**，朝廷任其为[银青光禄大夫](../Page/银青光禄大夫.md "wikilink")、检校左散骑常侍、依前兼御史大夫、使持节蔡州诸军事、权知蔡州刺史、充彰义军节度管内支度营田、申光蔡等州观察处置等使留后，仍赐[上柱国](../Page/上柱国.md "wikilink")，封如故。\[1\]吴少阳从不[朝觐](../Page/朝觐.md "wikilink")，又给朝廷进贡马匹，朝廷遂予善待。元和九年（814年）九月吳少陽卒，其子[吳元濟擁兵自立](../Page/吳元濟.md "wikilink")，秘不發喪。

## 參考書目

  - 《[舊唐書](../Page/舊唐書.md "wikilink")》列傳第九十五
  - 《[資治通鑒](../Page/資治通鑒.md "wikilink")》卷第二百三十八

[category:滄州人](../Page/category:滄州人.md "wikilink")
[S](../Page/category:吴姓.md "wikilink")

[Category:淮西節度使](../Category/淮西節度使.md "wikilink")
[Category:唐朝异姓郡王](../Category/唐朝异姓郡王.md "wikilink")

1.  [白居易《授吴少阳淮西节度留後制》](../Page/:s:授吴少阳淮西节度留後制.md "wikilink")