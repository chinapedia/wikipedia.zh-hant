**中国金鹰电视艺术节**（简称**金鹰节**），由[中国文学艺术界联合会和](../Page/中国文学艺术界联合会.md "wikilink")[中国电视艺术家协会](../Page/中国电视艺术家协会.md "wikilink")、[湖南省人民政府和](../Page/湖南.md "wikilink")[湖南省广播电视局联合主办](../Page/湖南省广播电视局.md "wikilink")，湖南电广传媒（即[湖南电广传媒股份有限公司](../Page/湖南电广传媒股份有限公司.md "wikilink")）承办的[中国大陆电视艺术节庆活动](../Page/中国大陆电视.md "wikilink")。主要内容为评选[中国电视金鹰奖](../Page/中国电视金鹰奖.md "wikilink")，每两年举行一次，[长沙为永久举办地](../Page/长沙.md "wikilink")，首届金鹰电视艺术节于2000年10月19日－10月21日举行。

## 来历

“金鹰节”源自2000年的第18届中国电视金鹰奖，湖南电广传媒在该年耗资1000万元人民币买断“金鹰奖”永久承办权，至此原本由各大城市每年轮流举行一场的金鹰奖颁奖晚会转变为为期三天的“艺术节”，主场地为[湖南国际影视会展中心](../Page/湖南国际影视会展中心.md "wikilink")。

## 特点

金鹰电视艺术节是中国大陆第一个以国产电视作品作为评奖和交流对象的艺术活动庆典节日。

## 金鹰女神

“金鹰女神”源自[中国电视金鹰奖的奖杯标志](../Page/中国电视金鹰奖.md "wikilink")，其主体为一个双手托起高飞金鹰的女神形象。从2006年第6届中国金鹰电视艺术节开始，主办方将“金鹰女神”的形象具体化，挑选出一位在过去两年有优秀电视剧代表作品的新生代女星担任“金鹰女神”，作为该届金鹰节的形象代言人，首位担任“金鹰女神”的女星为[刘亦菲](../Page/刘亦菲.md "wikilink")\[1\]。

在历届金鹰节首日，担任“金鹰女神”的女星会身穿通体金色的礼服在开幕式当天的晚会上进行舞蹈表演。最初三届“金鹰女神”均由主办方直接选定，从2012年第9届金鹰节开始，“金鹰女神”改为由大众通过网络投票产生，最终[刘诗诗以](../Page/刘诗诗.md "wikilink")278万票成功当选为第四位“金鹰女神”\[2\]。第10屆后改回主办方直接选定。

| 年份（届数）      | 名字                                 | 年龄                             | 代表作                                                                                                          |
| ----------- | ---------------------------------- | ------------------------------ | ------------------------------------------------------------------------------------------------------------ |
| 2006年（第6届）  | [刘亦菲](../Page/刘亦菲.md "wikilink")   | [19](../Page/19.md "wikilink") | 2006年《[神雕侠侣](../Page/神雕侠侣_\(2006年电视剧\).md "wikilink")》                                                       |
| 2008年（第7届）  | [李小璐](../Page/李小璐.md "wikilink")   | [26](../Page/26.md "wikilink") | 2007年《[奋斗](../Page/奋斗_\(2007年电视剧\).md "wikilink")》                                                           |
| 2010年（第8届）  | [王珞丹](../Page/王珞丹.md "wikilink")   | [26](../Page/26.md "wikilink") | 2010年《[杜拉拉升职记](../Page/杜拉拉升职记_\(电视剧\).md "wikilink")》                                                        |
| 2012年（第9届）  | [刘诗诗](../Page/刘诗诗.md "wikilink")   | [25](../Page/25.md "wikilink") | 2011年《[步步惊心](../Page/步步惊心_\(电视剧\).md "wikilink")》                                                            |
| 2014年（第10届） | [赵丽颖](../Page/赵丽颖.md "wikilink")   | [27](../Page/27.md "wikilink") | 2013年《[陆贞传奇](../Page/陆贞传奇.md "wikilink")》2014年《[杉杉來了](../Page/杉杉來了.md "wikilink")》                           |
| 2016年（第11届） | [唐嫣](../Page/唐嫣.md "wikilink")     | [33](../Page/33.md "wikilink") | 2015年《[何以笙箫默](../Page/何以笙箫默_\(电视剧\).md "wikilink")》、《[活色生香](../Page/活色生香_\(电视剧\).md "wikilink")》             |
| 2018年（第12届） | [迪麗熱巴](../Page/迪麗熱巴.md "wikilink") | [26](../Page/26.md "wikilink") | 2017年《[三生三世十里桃花](../Page/三生三世十里桃花_\(電視劇\).md "wikilink")》2018年《[一千零一夜](../Page/一千零一夜_\(電視劇\).md "wikilink")》 |
|             |                                    |                                |                                                                                                              |

历届中国金鹰电视艺术节“金鹰女神”

## 参见

  - [中国大陆电视](../Page/中国大陆电视.md "wikilink")
  - [中国电视金鹰奖](../Page/中国电视金鹰奖.md "wikilink")
  - [中国电视剧飞天奖](../Page/中国电视剧飞天奖.md "wikilink")
  - [上海电视节](../Page/上海电视节.md "wikilink")
  - [音乐风云榜](../Page/音乐风云榜.md "wikilink")
  - [华语音乐传媒大奖](../Page/华语音乐传媒大奖.md "wikilink")
  - [全国公益文化之星歌手大赛](../Page/全国公益文化之星歌手大赛.md "wikilink")

## 参考文献

## 外部链接

  - 历届金鹰节门户网站专题：
  - [第1届中国金鹰电视艺术节](http://ent.sina.com.cn/jiny.html)
  - [第2届中国金鹰电视艺术节](http://ent.sina.com.cn/jiny2.html)
  - [第3届中国金鹰电视艺术节](http://ent.sina.com.cn/f/jiny20/)
  - [第4届中国金鹰电视艺术节](http://ent.sina.com.cn/f/jiny21/)
  - [第5届中国金鹰电视艺术节](http://ent.sina.com.cn/f/jiny22/)
  - [第6届中国金鹰电视艺术节](http://ent.sina.com.cn/f/v/jinying6th/)
  - [第7届中国金鹰电视艺术节](http://ent.hunantv.com/v/tv/7jyj/)
  - [第8届中国金鹰电视艺术节](http://ent.hunantv.com/v/8jyj/)
  - [第9届中国金鹰电视艺术节](http://www.hunantv.com/v/2012/9jyj/)
  - [第10届中国金鹰电视艺术节](http://www.hunantv.com/v/1/57007/)
  - [第11届中国金鹰电视艺术节](https://web.archive.org/web/20161012234624/http://www.mgtv.com/v/1/301053/)
  - [第12届中国金鹰电视艺术节](https://www.mgtv.com/h/325960.html?fpa=se)

[category:中国年度事件](../Page/category:中国年度事件.md "wikilink")

[Category:长沙媒体](../Category/长沙媒体.md "wikilink")
[Category:中国电视](../Category/中国电视.md "wikilink")

1.
2.