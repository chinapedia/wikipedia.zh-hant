**豐臣氏**是[安土桃山時代時授與掌握政權的](../Page/安土桃山時代.md "wikilink")[關白](../Page/關白.md "wikilink")[羽柴秀吉的](../Page/豐臣秀吉.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")。

當時[公卿的姓有](../Page/公卿.md "wikilink")**源平藤橘**（[源氏](../Page/源氏.md "wikilink")、[平氏](../Page/平氏.md "wikilink")、[藤原氏](../Page/藤原氏.md "wikilink")、[橘氏](../Page/橘氏.md "wikilink")）以及可以與之相匹敵的新姓[菅原氏](../Page/菅原氏.md "wikilink")、[賀茂氏](../Page/賀茂氏.md "wikilink")、[大江氏等](../Page/大江氏.md "wikilink")，而朝廷則在1586年時下賜給秀吉**豐臣[朝臣](../Page/朝臣.md "wikilink")**的氏姓（[本姓](../Page/本姓.md "wikilink")）。

對照明智光秀等人當初賜九州名族的[苗字](../Page/苗字.md "wikilink")，豐臣此朝臣姓，也與九州島有關，[大宰府所在地](../Page/大宰府.md "wikilink")[豐後國](../Page/豐後國.md "wikilink")，在律令制前即為[豐國](../Page/豐國.md "wikilink")，豐臣秀吉死後其主祀神社，亦為[豐國神社](../Page/豐國神社_\(京都市\).md "wikilink")。

有說法「豐臣」（）姓是取自於[聖德太子的本名](../Page/聖德太子.md "wikilink")「豐聰耳」（）。

以一介平民出身而沒有一門眾家臣團的秀吉，賜給有力家臣與[大名豐臣朝臣的氏姓和羽柴的苗字](../Page/大名.md "wikilink")，使大名們要自稱為**豐臣朝臣羽柴何某**，達到類似自己一族的效果（[江戶幕府時代](../Page/江戶幕府.md "wikilink")，許多大名是德川氏與松平氏出身，[豐臣政權可以與之做對照](../Page/豐臣政權.md "wikilink")）。

後來[德川氏穩定霸權的過程中](../Page/德川氏.md "wikilink")，允許當初被賜豐臣朝臣、[羽柴氏的大名們](../Page/羽柴氏.md "wikilink")，可以捨棄這個姓。[江戶時代還以豐臣朝臣為本姓的大名家](../Page/江戶時代.md "wikilink")，只有秀吉夫人[北政所](../Page/高台院.md "wikilink")（寧寧）娘家[木下氏](../Page/木下氏.md "wikilink")。木下氏在[明治維新後因為本姓與苗字合一的法令](../Page/明治維新.md "wikilink")，必需只能使用其中一個姓，至此豐臣氏不復存在，現今豐臣姓的日本人基本上是崇拜秀吉才改的。

## 氏族豐臣氏的歷史

氏族的豐臣氏，即指秀吉的兄弟姊妹與他們子孫所結合的一族，也就是以下列出的人們：

  - [大政所](../Page/大政所.md "wikilink")（母）
  - [木下彌右衛門](../Page/木下彌右衛門.md "wikilink")（瑞龍院、秀吉之父）
  - [竹阿彌](../Page/竹阿彌.md "wikilink")（秀長、旭姬之父）

<!-- end list -->

  - [瑞龍院](../Page/瑞龍院.md "wikilink")（長女）
      - [豐臣秀次](../Page/豐臣秀次.md "wikilink")（秀吉的養子）
      - [豐臣秀勝](../Page/豐臣秀勝.md "wikilink")（秀吉的養子）
      - [豐臣秀保](../Page/豐臣秀保.md "wikilink")（秀長的養子）
  - [豐臣秀吉](../Page/豐臣秀吉.md "wikilink")（長男）
      - [豐臣鶴松](../Page/豐臣鶴松.md "wikilink")（夭折）
      - [豐臣秀賴](../Page/豐臣秀賴.md "wikilink")
          - [豐臣國松](../Page/豐臣國松.md "wikilink")（處死）
          - [天秀尼](../Page/天秀尼.md "wikilink")
  - [豐臣秀長](../Page/豐臣秀長.md "wikilink")（次男）
  - [旭姬](../Page/旭姬.md "wikilink")（次女）

秀吉只有以上少數的親人，弟弟**[秀長](../Page/豐臣秀長.md "wikilink")**是[大和國郡山城主](../Page/大和國.md "wikilink")100萬石，外甥**[秀次](../Page/豐臣秀次.md "wikilink")**是[近江國八幡城主](../Page/近江國.md "wikilink")43萬石、養子**[秀勝](../Page/豐臣秀勝.md "wikilink")**是[丹波國龜山城主](../Page/丹波國.md "wikilink")，親生兒子**[鶴松](../Page/豐臣鶴松.md "wikilink")**則是原本預定為繼承人。但在1590年至1595年天下統一之間，弟弟、養子、親生子以及繼承[秀長的](../Page/豐臣秀長.md "wikilink")**[秀保](../Page/豐臣秀保.md "wikilink")**竟然都相繼死去了。

老來喪子的秀吉心灰意冷決定以外甥[秀次當繼承人](../Page/豐臣秀次.md "wikilink")，為了確保繼承地位並於1591年將關白及豐臣家督之位讓給他。秀吉讓出權位後卻又自稱更高位階的官位**[太閤](../Page/太閤.md "wikilink")**，依然掌握豐臣家的直轄領與軍權。1593年時，秀吉再次意外得子**[秀賴](../Page/豐臣秀賴.md "wikilink")**，也因繼承權問題秀吉與[秀次產生對立矛盾](../Page/豐臣秀次.md "wikilink")。1595年，秀吉先將[秀次流放到](../Page/豐臣秀次.md "wikilink")[高野山](../Page/高野山.md "wikilink")，再命秀次切腹自殺，又將他的妻妾子女共三十九人在京都三條河原處死，連未過門的媳婦([駒姬](../Page/駒姬.md "wikilink"))都被下令處決。從此二重政權消除，豐臣政權再次歸於秀吉，而且為了政權的正統性，關白一職便從豐臣家完全消失了。豐臣家督之位因[秀賴年幼](../Page/豐臣秀賴.md "wikilink")，先暫由太閣秀吉掌管，因此在秀次被處死到秀賴繼承家督之位，總共有四年的時間，豐臣政權是沒有家督的情形。

1598年秀吉過世前，他將豐臣直屬政權託付給**[五奉行](../Page/五奉行.md "wikilink")**，國家政權託付給**[五大老](../Page/五大老.md "wikilink")**，其中以[德川家康實力最大](../Page/德川家康.md "wikilink")，在1600年的[關原之戰時擊倒](../Page/關原之戰.md "wikilink")[石田三成等豐臣政權擁護派的諸大名](../Page/石田三成.md "wikilink")，建立霸權。家康在1603年時就任[征夷大將軍](../Page/征夷大將軍.md "wikilink")，創立**[江戶幕府](../Page/江戶幕府.md "wikilink")**，並在1605年時將將軍之位傳給其子[德川秀忠](../Page/德川秀忠.md "wikilink")，以確立德川政權的正統性，而豐臣氏的政權從此消失。

德川政權確立初期，德川家康依然以**[五大老](../Page/五大老.md "wikilink")**之長的身分向[豐臣秀賴稱臣](../Page/豐臣秀賴.md "wikilink")。1614年冬[德川家康發起](../Page/德川家康.md "wikilink")[大坂冬之陣](../Page/大坂之戰.md "wikilink")，並且在第二年的[大坂夏之陣時攻下](../Page/大坂之戰.md "wikilink")[大坂城](../Page/大坂城.md "wikilink")，[秀賴與其母](../Page/豐臣秀賴.md "wikilink")[淀殿被迫自殺](../Page/淀殿.md "wikilink")。諷刺的是[秀賴之子](../Page/豐臣秀賴.md "wikilink")**[國松](../Page/豐臣國松.md "wikilink")**在[慶長二十年五月二十三日](../Page/慶長.md "wikilink")（西元1615年6月19日）在京都三條河原被處死，於是由秀吉一手興起的豐臣氏正式宣告滅亡。

## 外部連結

  - [豐臣氏](http://www2.harimaya.com/sengoku/html/toyo_k.html)

[Category:戰國大名](../Category/戰國大名.md "wikilink")
[Category:日本氏族](../Category/日本氏族.md "wikilink")
[木下氏](../Category/木下氏.md "wikilink")
[Category:賜名姓氏](../Category/賜名姓氏.md "wikilink")