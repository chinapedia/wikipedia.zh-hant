**飛車載卿情**（***Ta Ra Rum
Pum***）是2007年的印度電影。影片導演是[西德哈斯·阿南德](../Page/西德哈斯·阿南德.md "wikilink")（[Siddharth
Anand](../Page/:en:Siddharth_Anand.md "wikilink")），攝制幾乎全部在[美國](../Page/美國.md "wikilink")[紐約完成](../Page/紐約.md "wikilink")，賽車道攝制在[北卡羅來納州的羅京漢車場](../Page/北卡羅來納州.md "wikilink")，影片公演後在印度造成熱烈氣氛。\[1\]

## 故事梗概

**RV**（*Rajveer*的簡稱）是一個瘋狂追求自己夢想的年輕的士司機兼賽車手，有一次他在開的士當中不小心弄壞了富家小姐拉荻卡（Radhika）的[I-Pod](../Page/I-Pod.md "wikilink")，他答應如果禮拜五賽車得獎就還她錢。
結果他真的如願取勝了，不但還了錢，還得到拉荻卡的芳心。

拉荻卡剛開始和*RV*談婚嫁論，此時他們遇到了第一個障礙，她的富豪爸爸說她有[哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")（[Columbia
University](../Page/:en:Columbia_University.md "wikilink")）的學歷，而RV卻沒有上過學，可能僅僅會一個外語字：[法拉利](../Page/法拉利.md "wikilink")，
總之她們倆根本就不般配，但是拉荻卡卻堅決地結了婚，兩年后兩個孩子*Princess*和*Champ*相繼來到世上，

此時*RV*在一次賽車中出了事故受傷，一年后傷是好了，但是他卻不能放開當時翻車時的心理陰影，重返賽場后比賽成績直線下降，很快，他們的車隊僱了新車手，*RV*被辭退了。沒有了收入來源，全家只好搬到[紐約的](../Page/紐約.md "wikilink")[布朗科斯](../Page/布朗科斯.md "wikilink")（[Bronx](../Page/Bronx.md "wikilink")）貧民區去，全家開始體驗到了以前僅在電視上看到過的生活艱辛，*RV*被迫重新開的士掙錢養家，但是拉荻卡依然愛他，全家對生活充滿了樂觀，她也絕口不對富豪父親提及現今生活的雋困。

眼看這種生活將不斷持續下去，又發生了一次意外，兒子*Champ*無意吞食了一塊玻璃昏迷、生命垂危，而醫院要預收六萬美金的手術費用才進行手術，*RV*終於認識到只有他自己振作起來才能拯救全家于困境，他讓妻子在醫院守護兒子，自己重返賽車場，女兒*Princess*代媽媽為爸爸打氣，他不但取得了最後的冠軍，還把強敵摔出了賽車道。

當他再次出現在車場上時，全場觀眾頓時歡聲雷動，慶祝冠軍**RV**終於又回來了。。。

## 卡士

  - [賽伊夫·阿里·罕](../Page/賽伊夫·阿里·罕.md "wikilink") ... [Saif Ali
    Khan](../Page/Saif_Ali_Khan.md "wikilink") ... 飾演拉吉維爾·辛格（Rajveer
    Singh a.k.a RV）
  - [拉妮·穆科吉](../Page/拉妮·穆科吉.md "wikilink") ... [Rani
    Mukerji](../Page/Rani_Mukerji.md "wikilink") ... 飾演拉荻卡（Radhika
    Shekar Roy Banerjee Singh a.k.a. Shona）
  - [安吉利娜·伊德娜妮](../Page/安吉利娜·伊德娜妮.md "wikilink") ... [Angelina
    Idnani](../Page/Angelina_Idnani.md "wikilink") ... 飾演小女孩普麗婭（Priya
    Singh a.k.a Princess）
  - [阿里·哈吉](../Page/阿里·哈吉.md "wikilink") ... [Ali
    Haji](../Page/Ali_Haji.md "wikilink") ... 飾演小男孩蘭維爾（Ranveer Singh
    a.k.a Champ）
  - [賈韋德·傑弗里](../Page/賈韋德·傑弗里.md "wikilink") ... [Jaaved
    Jaffrey](../Page/Jaaved_Jaffrey.md "wikilink") ... 飾演經濟人哈里（Harry
    Patel）
  - [舒魯蒂·謝斯](../Page/舒魯蒂·謝斯.md "wikilink") ... [Shruti
    Seth](../Page/Shruti_Seth.md "wikilink") ... 飾演莎夏（Sasha）
  - [維克多·巴內吉](../Page/維克多·巴內吉.md "wikilink") ... [Victor
    Banerjee](../Page/Victor_Banerjee.md "wikilink") ...
    飾演拉荻卡的父親蘇波（Subho Shekar Roy Banerjee）

## 影片歌曲

本片共有六首歌曲，由[Vishal-Shekhar作曲](../Page/Vishal-Shekhar.md "wikilink")，由[Javed
Akhtar作詞](../Page/Javed_Akhtar.md "wikilink")。

| 歌曲名                          | 歌手名                                                                                                                                                                                                       | 演唱時間 | 畫面人物                                                                                                                                                                                                            |
| ---------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| *Ta Ra Rum Pum*              | [Shaan](../Page/Shaan.md "wikilink"), [Mahalaxmi Iyer](../Page/Mahalaxmi_Iyer.md "wikilink"), [Sneha Suresh](../Page/Sneha_Suresh.md "wikilink") & [Shravan Suresh](../Page/Shravan_Suresh.md "wikilink") |      | [Saif Ali Khan](../Page/Saif_Ali_Khan.md "wikilink"), [Rani Mukerji](../Page/Rani_Mukerji.md "wikilink"), [Angelina Idnani](../Page/Angelina_Idnani.md "wikilink") & [Ali Haji](../Page/Ali_Haji.md "wikilink") |
| *Hey Shona*                  | [Sunidhi Chauhan](../Page/Sunidhi_Chauhan.md "wikilink") & [Shaan](../Page/Shaan.md "wikilink")                                                                                                           |      | [Saif Ali Khan](../Page/Saif_Ali_Khan.md "wikilink") & [Rani Mukerji](../Page/Rani_Mukerji.md "wikilink")                                                                                                       |
| *Nachle Ve*                  | [Sonu Nigam](../Page/Sonu_Nigam.md "wikilink") & [Somya Raoh](../Page/Somya_Raoh.md "wikilink")                                                                                                           |      | [Saif Ali Khan](../Page/Saif_Ali_Khan.md "wikilink"), [Rani Mukerji](../Page/Rani_Mukerji.md "wikilink"), [Angelina Idnani](../Page/Angelina_Idnani.md "wikilink") & [Ali Haji](../Page/Ali_Haji.md "wikilink") |
| *Ta Ra Ra Ra Rum Tararumpum* | [Shreya Ghoshal](../Page/舒蕾亞·戈沙爾.md "wikilink")                                                                                                                                                           |      | [Saif Ali Khan](../Page/Saif_Ali_Khan.md "wikilink"), [Rani Mukerji](../Page/Rani_Mukerji.md "wikilink"), [Angelina Idnani](../Page/Angelina_Idnani.md "wikilink") & [Ali Haji](../Page/Ali_Haji.md "wikilink") |
| *Ab To Forever*              | [KK](../Page/KK.md "wikilink"), [Shreya Ghoshal](../Page/舒蕾亞·戈沙爾.md "wikilink") and [Vishal Dadlani](../Page/Vishal_Dadlani.md "wikilink")                                                                |      | [Saif Ali Khan](../Page/Saif_Ali_Khan.md "wikilink"), [Rani Mukerji](../Page/Rani_Mukerji.md "wikilink") and [Javed Jafferi](../Page/Javed_Jafferi.md "wikilink")                                               |
| *Saiyaan*                    | [Vishal Dadlani](../Page/Vishal_Dadlani.md "wikilink")                                                                                                                                                    |      | [Saif Ali Khan](../Page/Saif_Ali_Khan.md "wikilink"), [Rani Mukerji](../Page/Rani_Mukerji.md "wikilink"), [Angelina Idnani](../Page/Angelina_Idnani.md "wikilink") & [Ali Haji](../Page/Ali_Haji.md "wikilink") |
|                              |                                                                                                                                                                                                           |      |                                                                                                                                                                                                                 |

## 票房收入

  - [英國](../Page/英國.md "wikilink") - $1,527,252
  - [美國](../Page/美國.md "wikilink") - $425,102
  - [澳大利亞](../Page/澳大利亞.md "wikilink") - $87,082
  - [印度全國](../Page/印度.md "wikilink") - 暫無統計數字

\[2\] \[3\] \[4\]

## 片中瑣事

  - 在影片插曲：《Nachle
    Ve》當中，RV到[C-Town商場尋工作](../Page/C-Town.md "wikilink")，他把一些商品從貨架上弄下地，老闆就把他辭退了，這個鏡頭一晃就過去，很容易被人忽略。
  - 影片中一個女人提出付六百美金給RV，要他送她[肯尼迪國境機場趕飛機](../Page/肯尼迪國境機場.md "wikilink")，實際上道具車開上了到[紐瓦克自由國境機場的道路](../Page/紐瓦克自由國境機場.md "wikilink")，最後又停在該機場的場站大廳門外。
  - 影片中一段事故翻車的鏡頭很像2004年的一次[NASCAR Subway
    400真實事故鏡頭重放](../Page/NASCAR_Subway_400.md "wikilink")，一些車迷看出了這一巧合，然而這的確純屬巧合。

## 參照

## 外部連結

  - [電影《飛車載卿情》的公式網址](https://web.archive.org/web/20080401085357/http://www.tararumpum.org/)

  - [《Watch Movie》雜誌的評論](http://bollywood.mindrelish.com/)

  -
  - [Yash Raj片場的資料庫](http://www.watchindia.tv)

[T](../Category/印度電影作品.md "wikilink")
[T](../Category/2007年電影.md "wikilink")

1.
2.  Ta Ra Rum Pum box below
    expectations（HTML）．glamsham.com．於2007年1月1日造訪．
3.
4.