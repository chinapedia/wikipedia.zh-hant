[Abercrombie_&_Fitch_Ginza_Store_2018.jpg](https://zh.wikipedia.org/wiki/File:Abercrombie_&_Fitch_Ginza_Store_2018.jpg "fig:Abercrombie_&_Fitch_Ginza_Store_2018.jpg")[銀座的旗艦店](../Page/銀座.md "wikilink")\]\]
[Abercrombie_&_Fitch_Hong_Hong_store_1.jpg](https://zh.wikipedia.org/wiki/File:Abercrombie_&_Fitch_Hong_Hong_store_1.jpg "fig:Abercrombie_&_Fitch_Hong_Hong_store_1.jpg")的旗艦店（2012年-2017年）\]\]
[Abercrombie_&_Fitch_in_Harbour_City_201802.jpg](https://zh.wikipedia.org/wiki/File:Abercrombie_&_Fitch_in_Harbour_City_201802.jpg "fig:Abercrombie_&_Fitch_in_Harbour_City_201802.jpg")[海港城的旗艦店](../Page/海港城.md "wikilink")（2018年）\]\]
**爱芙趣**（，[縮寫](../Page/縮寫.md "wikilink")：**A\&F**）是一間[美國的](../Page/美國.md "wikilink")[零售商](../Page/零售.md "wikilink")，公司前任的主席與執行長為[麥克·傑佛瑞斯](../Page/麥克·傑佛瑞斯.md "wikilink")(Mike
Jeffries)。A\&F販售針對18歲至22歲的顧客所設計的休閒服裝和配件\[1\]。A\&F在美國有超過300間店鋪，也持續在其他國家設立新據點\[2\]。A\&F旗下有三個附屬品牌，分別是[Abercrombie
kids](../Page/Abercrombie_kids.md "wikilink")（童裝）、[Hollister
Co.和](../Page/Hollister_Co..md "wikilink")[Gilly
Hicks](../Page/Gilly_Hicks.md "wikilink")\[3\]。

A\&F在1892年由[大衛·亞伯克朗比](../Page/大衛·亞伯克朗比.md "wikilink")(David T.
Abercrombie)於[曼哈頓創立](../Page/曼哈頓.md "wikilink")，而後律師埃茲拉·費區加入經營。主要販賣高級運動和旅行服飾及用品。之後在1960年代公司經歷了財務危機，直到1988年被公司購併並重新規劃品牌定位後後才得以解決，之後轉由麥可·傑佛瑞斯掌管，目前品牌訴求為「休閒奢華」品牌\[4\]

在過去販售旅行用品的期間，A\&F有許多知名的顧客，包括[西奧多·羅斯福](../Page/西奧多·羅斯福.md "wikilink")\[5\]、[愛蜜莉亞·厄爾哈特](../Page/愛蜜莉亞·厄爾哈特.md "wikilink")\[6\]\[7\]、[葛麗泰·嘉寶](../Page/葛麗泰·嘉寶.md "wikilink")\[8\]、[凯瑟琳·赫本](../Page/凯瑟琳·赫本.md "wikilink")\[9\]、[克拉克·盖博](../Page/克拉克·盖博.md "wikilink")\[10\][約翰·史坦貝克](../Page/約翰·史坦貝克.md "wikilink")\[11\]\[12\]、[約翰·甘迺迪](../Page/約翰·甘迺迪.md "wikilink")\[13\]、[歐內斯特·沙克爾頓](../Page/歐內斯特·沙克爾頓.md "wikilink")\[14\]、[德懷特·艾森豪](../Page/德懷特·艾森豪.md "wikilink")\[15\]與[海明威](../Page/欧内斯特·海明威.md "wikilink")\[16\]。

自1997年以來，A\&F的廣告宣傳方式讓品牌在社會上維持著高調的形象，其中包括了正面與負面的觀感，此外公司也涉入包括品牌經營、服飾設計和[員工待遇不平等方面的法律糾紛](../Page/剝削.md "wikilink")，尤其是[種族歧視等問題](../Page/種族歧視.md "wikilink")。在美國[公平就业机会委员会](../Page/公平就业机会委员会.md "wikilink")(EEOC)的要求下，A\&F同意支付4000萬美元以上的款項，給曾因[膚色而受到不平等待遇的](../Page/膚色.md "wikilink")[拉丁美洲](../Page/拉丁美洲.md "wikilink")、[穆斯林](../Page/穆斯林.md "wikilink")、[非裔美國人和](../Page/非裔美國人.md "wikilink")[亞洲人的求職者](../Page/亞洲人.md "wikilink")。這些求職者表示A\&F要求他們僅能在商店的後端區域工作。\[17\]

## 簡史

Abercrombie &
Fitch的歷史橫跨19、20和21三個世紀。[大衛·亞伯克朗比在](../Page/大衛·亞伯克朗比.md "wikilink")1892年成立了一間專賣高級運動用品的「亞伯克朗比公司」（Abercrombie
Co.），隨後與[埃茲拉·費區](../Page/埃茲拉·費區.md "wikilink")（Ezra
Fitch）結盟，在20世紀繼續擴張公司版圖，改稱「亞伯克朗比及費區公司」（Abercrombie
& Fitch Co.），簡稱「A＆F公司」（A\&F
Co.）。亞伯克朗比離開公司後，費區成為唯一的經營者，公司也持續在經營上獲得成功。費區退休後，公司在其他人的管理下也維持不錯的營運成績，直到1977年因為面臨財務危機而關閉。Limited
Brands在1988年買下這個狀況不佳的品牌，並交由[麥克·傑佛瑞斯掌管](../Page/麥克·傑佛瑞斯.md "wikilink")，他將此品牌轉型，改賣高級的年輕流行服飾。

### 創辦

公司創辦初期的名稱為「亞伯克朗比公司」，由大衛·亞伯克朗比在1892年6月4日成立，當時是一間位於[紐約](../Page/紐約.md "wikilink")[曼哈頓](../Page/曼哈頓.md "wikilink")[下城的小商店](../Page/下城_\(曼哈頓\).md "wikilink")。而當時在[紐約擔任](../Page/紐約.md "wikilink")[律師的埃茲拉](../Page/律師.md "wikilink")·費區成為這間商店的常客。在1900年，費區離開了他任職的法律事務所，成為亞伯克朗比公司的主要股東之一，隨後被列名為共同創辦人。之後亞伯克朗比公司搬遷至[百老滙上較大的店面](../Page/百老滙.md "wikilink")，費區也將自己實驗性的想法運用在店面的重新改裝上。在1904年，費區的姓氏（Fitch）被加入到商店名稱的後方，公司名稱成為「亞伯克朗比及費區公司」。

然而，亞伯克朗比和費區之間的合作關係並沒有完美的結局。兩人對於A\&F的未來方向有著不同的想法，且經常為此而爭執，雖然公司營業額同時也持續穩定的成長。費區希望擴展客源，讓[普羅大眾接納](../Page/普羅大眾.md "wikilink")，而亞伯克朗比則堅持主要客層應該是「專業的戶外活動人士」\[18\]。最後亞伯克朗比在1907年將公司賣給費區，並轉行生產運動用品\[19\]。費區則和新的生意夥伴繼續經營A\&F公司。

### 分店

[HK_Causeway_Bay_Kai_Chiu_Road_3_male_model_outdoor_photographer_Haysan_Place_fans_Aug-2012.JPG](https://zh.wikipedia.org/wiki/File:HK_Causeway_Bay_Kai_Chiu_Road_3_male_model_outdoor_photographer_Haysan_Place_fans_Aug-2012.JPG "fig:HK_Causeway_Bay_Kai_Chiu_Road_3_male_model_outdoor_photographer_Haysan_Place_fans_Aug-2012.JPG")
A\&F在全世界設置分店的速度相當積極\[20\]

  - ：301間分店

  - ：4間分店

  - ：3間分店

  - ：3間分店

  - ：2間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

  - ：1間分店

## 法律訴訟

自Abercrombie & Fitch開始受到歡迎以來，這個品牌已持續涉入多起法律訴訟案件中。

### 控告American Eagle Outfitters

2002年，Abercrombie & Fitch控告服飾品牌[American Eagle
Outfitters](../Page/American_Eagle_Outfitters.md "wikilink")，宣稱該公司抄襲A\&F的服裝設計。在訴訟中，A\&F認為American
Eagle Outfitters在服飾產品的視覺外觀和包裝方面與A\&F非常相像。A\&F更進一步指控American Eagle
Outfitters拷貝了特定款式的服裝、店鋪內的陳列設計、廣告宣傳，甚至包括A\&F的產品目錄設計。最後法院雖然認為American
Eagle
Outfitters的確在素材、設計、店內陳列、符號圖像、配色和花紋方面和A\&F十分相似，但還沒有到達會使消費者混淆兩個品牌的程度，因此最後做出對被告American
Eagle Outfitters有利的判決\[21\]。

### 員工待遇

2004年，Abercrombie &
Fitch被指控歧視[少數族群員工](../Page/少數民族.md "wikilink")，優先將高階職缺提供給[美國白人](../Page/美國白人.md "wikilink")\[22\]。最後A\&F同意[庭外和解](../Page/庭外和解.md "wikilink")，其中一部份條件包括賠償4,500萬美元給升遷遭拒的申請者和受影響的員工、提高廣告宣傳中少數族群的比率、增設「多元化副總裁」職位、聘請25位專門負責招募少數族群的員工，以及終止在白人[兄弟姊妹會](../Page/兄弟會.md "wikilink")(fraternities
and sororities)中招募員工的政策\[23\]。

2009年6月，英國一名曾任職於A\&F[倫敦旗艦店的法律系學生瑞恩](../Page/倫敦.md "wikilink")·汀(Riam
Dean)，在勞動法庭上控告A\&F公司。汀在出生時就沒有左前臂，她表示主管告知她的外表違反了公司的「外貌政策」，因此給她特別許可，允許她穿著可蓋住[義肢的服裝](../Page/義肢.md "wikilink")，但隨後卻將她調往遠離顧客的倉庫工作。汀控告A\&F公司歧視[身障人士](../Page/身心障礙.md "wikilink")，並要求20,000[英鎊的賠償](../Page/英鎊.md "wikilink")\[24\]。2009年8月13日，汀贏得了訴訟，法院判決認為這名22歲的少女受到不公平的解雇以及不合法的對待。汀獲得了6,800英鎊的精神損失賠償、1,077英鎊的薪資損失賠償，以及136英鎊的不公平解雇賠償（共計8,013英鎊）。但法院駁回了她對A\&F公司歧視身障人士的控訴，認為這起事件未達到歧視的程度\[25\]\[26\]。

2009年9月16日，時值17歲的莎曼珊·爾萊夫(Samantha
Elauf)在美國[圖爾薩地區法庭的](../Page/圖爾薩.md "wikilink")「公平就業機會委員會」中，宣稱她在2009年6月前往A\&F公司旗下的童裝店應徵職位，當時她因宗教信仰而戴著[希賈布](../Page/希賈布.md "wikilink")（面紗），而商店經理告訴她他的面紗違反了公司的「外貌政策」。

2010年，一名在美國[加州](../Page/加州.md "wikilink")[聖馬刁](../Page/聖馬刁.md "wikilink")[霍利斯特](../Page/霍利斯特.md "wikilink")（A\&F旗下品牌之一）店內工作的[穆斯林女性無預警的遭到開除](../Page/穆斯林.md "wikilink")。在被解雇之前，這名女性拒絕接受A\&F人力資源代表的要求拿下她的[希賈布面紗](../Page/希賈布.md "wikilink")。A\&F的人力資源代表標示她因宗教信仰而穿戴的面紗違反了公司的「外貌政策」。公民自由團體「美國與伊斯蘭關係諮詢會」(Council
on American-Islamic
Relations)認為這次的解雇違反了[反歧視法](../Page/反歧視法.md "wikilink")，並向公平就業機會委員會提出檢舉\[27\]。

### 其他問題

## 爭議與批評

自1988年重新經營以來，亞伯克朗比及費區在員工待遇、商品方面受到許多批評，而廣告行銷更被指責內容過度情色與種族歧視\[28\]。

### 歧視自閉症患者

2009年，亞伯克朗比及費區遭美國[明尼蘇達人權局罰款超過](../Page/明尼蘇達州.md "wikilink")115,000美元，因為該公司拒絕讓一名少女在更衣室中協助她患有[自閉症的姊姊試穿衣服](../Page/自閉症.md "wikilink")\[29\]。人權局表示高額的罰款是由於A\&F公司對於少女母親的抱怨冷淡回應，並且指控少女假裝自己有自閉症。

### 侵犯隱私權

一名16歲的少女發現自己在更衣室遭到A\&F店內員工偷拍錄影後控告該公司，涉案的員工否認犯案，但店內的同事在數天後發現了他的攝影機，其中還存有影片\[30\]。

### 控告碧昂絲

2009年9月27日，亞伯克朗比及費區公司控告歌手[碧昂絲](../Page/碧昂絲·諾利斯.md "wikilink")\[31\]。A\&F提出訴訟的理由是因為碧昂絲即將推出名為「Sasha
Fierce」的香水和服裝系列，而這個名稱與該公司自2003年起推出的熱賣香水產品「Fierce」十分類似\[32\]。A\&F控告畢昂絲侵犯專利商標、不公平競爭和侵犯著作權\[33\]。

最後，碧昂絲香水商品的發行公司Coty, Inc.表示，「Fierce」、「Sasha Fierce」不會用在預計推出的香水上\[34\]。

### 勞工權益

在2009年11月，A\&F被勞工團體「國際勞工權利論壇」（International Labor Rights
Forum）列在「2010年血汗工廠可恥名單」（2010 Sweatshop Hall of
Shame）上，同樣被列入清單的包括[Gymboree](../Page/Gymboree.md "wikilink")、[漢斯](../Page/漢斯.md "wikilink")（Hanes）、[宜家](../Page/宜家.md "wikilink")、[柯爾百貨](../Page/柯爾百貨.md "wikilink")（Kohl's）、[L.L.Bean](../Page/L.L.Bean.md "wikilink")、[Propper
International](../Page/Propper_International.md "wikilink")、[Pier
1和](../Page/Pier_1.md "wikilink")[沃尔玛](../Page/沃尔玛.md "wikilink")\[35\]。

### A\&F季刊

部份保守和宗教團體呼籲抵制美國版的《A\&F季刊》（*A\&F
Quarterly*，從1997年至2003年間出版），這些團體認為刊物包含顯著的性相關內容\[36\]。這份雜誌中刊載了由[布魯斯·瑋柏](../Page/布魯斯·瑋柏.md "wikilink")（Bruce
Weber）拍攝的[裸體照片](../Page/裸體.md "wikilink")、關於[性愛的文章](../Page/性愛.md "wikilink")，以及[酒精飲料的](../Page/酒精飲料.md "wikilink")[食譜](../Page/食譜.md "wikilink")。這份刊物同時也是A\&F的目錄，其中包含產品的資訊以及價格。《A\&F季刊》的廣告刊登在《[訪問](../Page/訪問_\(雜誌\).md "wikilink")》、《[Out](../Page/Out_\(雜誌\).md "wikilink")》、《[滾石](../Page/滾石_\(雜誌\).md "wikilink")》和《[浮華世界](../Page/浮華世界_\(雜誌\).md "wikilink")》雜誌中\[37\]。

雖然A\&F的政策是不將《A\&F季刊》販售給未成年者，但批評者指控實際上這份刊物被廣泛的售予未成年者。在2003年，一群宗教團體、女權運動團體，以及亞裔美國人團體組織了針對這份刊物的抵制行動，最後《A\&F季刊》的「聖誕版本」從商店內下架\[38\]。雖然執行長傑佛瑞斯標示停刊的原因是「對於這份刊物感到厭煩無趣」，但在2010年6月17日，A\&F宣布將要重新推出《A\&F季刊》，並邀請電子訊息訂閱者預購售價10美元的季刊，並在2010年7月17日於各店鋪開始販售。

### 產品爭議

此公司經常推出受外界批評的產品。

#### 歧視亞洲人或亞裔

2002年一系列标榜亚洲和其它民族的衣服备受爭議。其中一款印有主题标语「Wong Brothers Laundry Service-Two
Wongs Can Make It White」
及一個代表1900年代[華裔](../Page/華裔.md "wikilink")[移民的戴圆锥型草帽微笑小人圖案](../Page/移民.md "wikilink")。此标语借用40年代支持[白澳政策的澳大利亚移民部长](../Page/白澳政策.md "wikilink")[亚瑟·卡尔韦尔的名言](../Page/亚瑟·卡尔韦尔.md "wikilink")：“两个‘黄’不抵一个‘白’”（Two
Wongs do not make a
White），因此被指侮辱华裔及亚裔族群。在[亚裔学生團體的聯合抵制下](../Page/亚裔.md "wikilink")，A\&F停止了該系列產品和公开道歉。

#### 其它

2002年，它的兒童服装部在家长團體發動全美店前示威後除去了一系列的女童[丁字裤](../Page/丁字裤.md "wikilink")。此系列[内裤前面印有](../Page/内裤.md "wikilink")“Eye
Candy”和“Wink Wink”之类的标语。

2004年再爆发两宗爭議T-shirt事件。第一宗牵连以標語「It's All Relative in West Virginia」
为主题的T-shirt。此標語取笑美國南部鄉下的近親关系。西弗吉尼亞州州长Bob
Wise公开批評A\&F對「西州無根据的負面形象」描述。但A\&F沒有因此除去此系列的T-Shirt。\[39\]

第二宗事件围绕另一印有「L is for
loser」標語和在吊環上作出L形動作的男[体操](../Page/体操.md "wikilink")[運動員圖案的T](../Page/運動員.md "wikilink")-shirt。在美國體操會長Bob
Colarossi宣布抵制A\&F產品後，A\&F在2004年10月停止售卖受爭議的T-shirt.

2005年11月，由于A\&F售賣標題如「Who needs brains when you have these?」
涉及金发女性胸部大而没头脑的刻板印象的衣服，宾夕法尼亚西南妇女基金會（Women and
Girls Foundation of Southwest
Pennsylvania）发动对A\&F的「女童[杯葛](../Page/杯葛.md "wikilink")」（girl-cott）。此抵制上NBC的Today
Show節目後擴散至全美。A\&F在2005年11月5日停售相关衣物。\[40\]

## 競爭品牌

  - [Zara](../Page/Zara.md "wikilink")
  - [Uniqlo](../Page/Uniqlo.md "wikilink")
  - [GAP](../Page/蓋璞.md "wikilink")
  - [H\&M](../Page/H&M.md "wikilink")
  - [Forever 21](../Page/Forever_21.md "wikilink")
  - [Jack Wills](../Page/Jack_Wills.md "wikilink")
  - [American Eagle](../Page/American_Eagle.md "wikilink")

## 參考資料

## 外部連結

  - [Abercrombie & Fitch](http://www.abercrombie.com)台灣官方網站

  -
  - [Abercrombie & Fitch SEC
    Filings](http://www.sec.gov/cgi-bin/browse-edgar?CIK=ANF&action=getcompany)

  -
  -
  -
  -
[Category:1892年紐約州建立](../Category/1892年紐約州建立.md "wikilink")
[Category:俄亥俄州公司](../Category/俄亥俄州公司.md "wikilink")
[Category:美國服裝品牌](../Category/美國服裝品牌.md "wikilink")
[Category:內衣品牌](../Category/內衣品牌.md "wikilink")
[Category:美國服裝零售商](../Category/美國服裝零售商.md "wikilink")

1.

2.  [View Competitors for Abercrombie & Fitch Company
    (ANF)](http://www.nasdaq.com/screening/viewcompetitors.asp?symbol=ANF&selected=ANF)

3.
4.

5.

6.
7.  Purdue University. [Earhart suede
    jacket, 1932](http://e-archives.lib.purdue.edu/cdm4/item_viewer.php?CISOROOT=/earhart&CISOPTR=27&CISOBOX=1&REC=1)

8.
9.
10.
11.

12.

13.
14.

15.
16. Meyers, Jeffrey (1985). "Suicide and Aftermath". Hemingway: A
    Biography. London: Macmillan. pp. 550-560. ISBN 0333421264.

17. Ariana Bianchi，Abercrombie & Fitch - A Model of Employment
    Discrimination?

18. [The History of Abercrombie and
    Fitch](http://www.saintmarys.edu/~berdayes/vincehome/courses/comm303/student%20projects/finchness/history.html)


19.

20. <http://www.abercrombie.com/webapp/wcs/stores/servlet/StoreLocator?storeId=19658&catalogId=11556&langId=-1>

21. <http://www.audiocasefiles.com/acf_cases/9996-abercrombie-fitch-stores-inc-v-american-eagle-outfitters>

22. [$40 Million Paid to Class Members in December 2005 in Abercrombie &
    Fitch Discrimination Lawsuit
    Settlement](http://www.afjustice.com/index.htm)

23.

24. [BBC News: Disabled woman sues clothes
    store](http://news.bbc.co.uk/1/hi/england/london/8116231.stm)

25. [Sky News : Abercrombie And Fitch Lose Wrongful Dismissal Case
    Against Law Student With Prosthetic
    Arm](http://news.sky.com/skynews/Home/UK-News/Abercrombie-And-Fitch-Lose-Wrongful-Dismissal-Case-Against-Law-Student-With-False-Arm/Article/200908215360619?lpos=UK_News_Carousel_Region_3&lid=ARTICLE_15360619_Abercrombie_And_Fitch_Lose_Wrongful_Dismissal_Case_Against_Law_Student_With_False_Arm_)

26. <http://news.bbc.co.uk/1/hi/england/london/8200140.stm>

27.

28. [Poseurs Paradise\! What's it really like to work at the new
    Abercrombie & Fitch
    store?](http://www.dailymail.co.uk/pages/live/femail/article.html?in_article_id=447183&in_page_id=1879)

29. Shiffer, James Eli 和 Jane Friedman，[Girl: I Was Treated Like a
    'Misfit' at Abercrombie & Fitch; Magrath Agrees to
    Action](http://www.startribune.com/local/south/57918152.html)
    ，*Minneapolis Star Tribune*，2009年9月9日

30. <http://consumerist.com/5356685/16+year+old-unwittingly-stars-in-homemade-abercrombie--fitch-dressing-room-video>


31.

32.
33.

34.
35. [The 2010 Sweatshop Hall of
    Shame](http://www.laborrights.org/sites/default/files/publications-and-resources/sweatshop_hall_shame_2010.pdf)

36.

37.

38. Gross, Daniel. ["Abercrombie & Fitch's Blue
    Christmas"](http://www.slate.com/id/2092175). *Slate*, December 8,
    2003.

39. Dao, James. "T-Shirt Slight Has West Virginia in Arms." *The New
    York Times*, 22 March 2004.

40. "Abercrombie & Fitch to pull tees after "girl-cott." Reuters, 4
    November 2005.