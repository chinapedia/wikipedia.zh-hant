**買辦**是指在[清代](../Page/清代.md "wikilink")[五口通商地区被西方公民](../Page/五口通商.md "wikilink")[雇佣并且参与其主要经营活动的中国人](../Page/雇佣.md "wikilink")，原對應之西班牙文Comprar意譯為「購買」的意思，英文則作Comprador。清代買辦分為洋行買辦、外商銀行買辦、輪船公司買辦，多以廣東和江浙出身。這類由[外商僱用之](../Page/外商.md "wikilink")[商人通常](../Page/商人.md "wikilink")[外語能力強](../Page/外語.md "wikilink")，一方面可作為歐美商人與中國商人的[翻譯](../Page/翻譯.md "wikilink")，也可處理歐美國家商界與[中國政府之雙向溝通](../Page/中國政府.md "wikilink")。除此，這類型商人還可自營商舖，因此致富者頗眾。

買辦的另一個溫床是[國家資本主義壟斷下](../Page/國家資本主義.md "wikilink")，誰擁有[特權](../Page/特權.md "wikilink")，誰就是某個行業的買辦\[1\]。

而從[馬英九政府執政開始](../Page/馬英九政府.md "wikilink")，買辦已被認為形成一種「[潛規則式](../Page/潛規則.md "wikilink")」的買辦文化。在兩岸交流美名背後暗自運作，「[中介者](../Page/中介者.md "wikilink")」即[買辦文化縮影](../Page/買辦文化.md "wikilink")；馬政府執政期間，兩岸各種大小形式的往來交流，中介者從一開始扮演[北京當局和](../Page/北京當局.md "wikilink")[馬英九政府之間的訊息傳遞者](../Page/馬英九政府.md "wikilink")，最終成為穿梭兩岸，從北京當局與馬政府兩方之間的利益輸運中伺機獲取利益的「中間商」\[2\]，這類中間商的集體化就被稱為「買辦集團」。\[3\]

## 著名買辦

  - [吴健彰](../Page/吴健彰.md "wikilink")
  - [杨坊早起任职于](../Page/杨坊.md "wikilink")[怡和洋行](../Page/怡和洋行.md "wikilink")，英语极为流利，后成为上海最早的大买办，帮助华尔[洋枪队筹集资金抵抗](../Page/洋枪队.md "wikilink")[太平天国进攻上海](../Page/太平天国.md "wikilink")。
  - [陳廉伯](../Page/陳廉伯.md "wikilink")
  - [何甘棠](../Page/何甘棠.md "wikilink")
  - [盧亞貴](../Page/盧亞貴.md "wikilink")
  - [馮明珊](../Page/馮明珊.md "wikilink")：[有利銀行](../Page/有利銀行.md "wikilink")。
  - [郭甘章](../Page/郭甘章.md "wikilink")：[廣東](../Page/廣東.md "wikilink")[香山縣買辦](../Page/香山縣.md "wikilink")，1870年代初，名下有甘章船廠。[1](https://web.archive.org/web/20070429080403/http://economy.guoxue.com/article.php/5901)
  - [唐廷樞](../Page/唐廷樞.md "wikilink")
  - [張嘉璈](../Page/張嘉璈.md "wikilink")
  - [莫仕揚](../Page/莫仕揚.md "wikilink")：[莫仕揚家族](../Page/莫仕揚家族.md "wikilink")。[2](http://www.cnr.cn/zhfw/csyx/zhrw/200612/t20061204_504340826.html)
  - [吳炳垣](../Page/吳炳垣.md "wikilink") (阿偉)
  - [何東](../Page/何東.md "wikilink")

從移民社會轉型為定居社會以後，因為臺灣1860年代開港通商，在傳統的地主與郊商之外，買辦成為臺灣新興的社會領導階層：

  - [李春生](../Page/李春生.md "wikilink")：[台灣](../Page/台灣.md "wikilink")19世紀中期的富商。其買辦身分，延續到[台灣日治時期](../Page/台灣日治時期.md "wikilink")。
  - [陳福謙](../Page/陳福謙.md "wikilink")：[台灣打狗](../Page/台灣.md "wikilink")（今[高雄](../Page/高雄.md "wikilink")）。
  - [陳中和](../Page/陳中和.md "wikilink")

## 研究書目

  - 郝延平著，李榮昌譯：《十九世紀的中國買辦：東西間橋樑》（上海：上海社會科學出版社，1988）。

## 参见

  - [大班](../Page/大班.md "wikilink")
  - [幫辦](../Page/幫辦.md "wikilink")
  - [代理](../Page/代理.md "wikilink")
  - [洋行](../Page/洋行.md "wikilink")
  - [尋租](../Page/尋租.md "wikilink")
  - [套利](../Page/套利.md "wikilink")
  - [國家資本主義](../Page/國家資本主義.md "wikilink")

[Category:清朝商业](../Category/清朝商业.md "wikilink")

1.
2.
3.