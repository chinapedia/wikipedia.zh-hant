《**反三国演义**》又名《反三国志》，为[中华民国文人](../Page/中華民國.md "wikilink")[周大荒所撰写的](../Page/周大荒.md "wikilink")[白话文](../Page/白话文.md "wikilink")[章回小說](../Page/章回小說.md "wikilink")。据信该书始作于1919年，1924年起在《[民德报](../Page/民德报.md "wikilink")》上连载，1930年结集，由[上海](../Page/上海.md "wikilink")[卿云书局出版](../Page/卿云书局.md "wikilink")。

该书从[徐庶进曹营一节开始改写了](../Page/徐庶.md "wikilink")《[三国演义](../Page/三国演义.md "wikilink")》的剧情，褒[劉備](../Page/劉備.md "wikilink")，抑[孫權](../Page/孫權.md "wikilink")，貶[曹操](../Page/曹操.md "wikilink")。并且加大了[赵云与](../Page/赵云.md "wikilink")[马超的戏分](../Page/马超.md "wikilink")，让此二人成为[刘备的顶梁柱](../Page/刘备.md "wikilink")，并最终攻陷[许昌](../Page/许昌.md "wikilink")，助[蜀汉统一全国](../Page/蜀汉.md "wikilink")。文中的[刘备手下](../Page/刘备.md "wikilink")[谋士众多](../Page/谋士.md "wikilink")：[诸葛亮同样足智多谋](../Page/诸葛亮.md "wikilink")；而[徐庶则识破](../Page/徐庶.md "wikilink")[曹操诓徐母手书的把戏](../Page/曹操.md "wikilink")，在[赵云的协助下将母亲救出](../Page/赵云.md "wikilink")[许昌](../Page/许昌.md "wikilink")，自己亦留在[蜀汉](../Page/蜀汉.md "wikilink")。[庞统逃出](../Page/庞统.md "wikilink")[落凤坡之难](../Page/落凤坡.md "wikilink")，得以继续辅佐刘备。[周瑜则脱离心胸狭隘的形象](../Page/周瑜.md "wikilink")，变得很[正義](../Page/正義.md "wikilink")。[孙尚香就是由他力主嫁给](../Page/孙尚香.md "wikilink")[刘备的](../Page/刘备.md "wikilink")。[诸葛亮为](../Page/诸葛亮.md "wikilink")[赵云做](../Page/赵云.md "wikilink")[媒人](../Page/媒人.md "wikilink")，迎娶了[马超智勇双全的妹妹](../Page/马超.md "wikilink")[马云騄](../Page/马云騄.md "wikilink")，令其双双建功立业。

日本漫画作家[池上辽一的作品](../Page/池上辽一.md "wikilink")[超三国志也有参考此内容](../Page/超三国志.md "wikilink")。

## 外部參考

[Category:中华民国大陆时期小说](../Category/中华民国大陆时期小说.md "wikilink")
[Category:章回小說](../Category/章回小說.md "wikilink")
[Category:三國題材小說](../Category/三國題材小說.md "wikilink")