**伽利略的星空**（Eppur Si
Muove）是[德國](../Page/德國.md "wikilink")[交響金屬樂團](../Page/交響金屬.md "wikilink")[曠野鷹豪樂團](../Page/曠野鷹豪樂團.md "wikilink")（Haggard）第三張專輯。於2004年4月26日發行。

專輯內容是描述[義大利](../Page/義大利.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")[伽利略](../Page/伽利略.md "wikilink")（1564
–1642）的一生。專輯標題是根據傳說，伽利略在宗教法庭前被迫放棄[日心說的主張之後](../Page/日心說.md "wikilink")，低聲說出「Eppur
si muove」（And yet it moves）。

## 專輯曲目

1.  《All'inizio è La Morte》 – 6:50
2.  《Menuetto In Fa-Minore》 – 1:16
3.  《Per Aspera Ad Astra》 – 6:40
4.  《Of a Might Divine》 – 8:20
5.  《Gavotta In Si-Minore》 – 0:58
6.  《Herr Mannelig》 – 4:50
7.  《The Observer》 – 4:40
8.  《Eppur Si Muove》 – 8:19
9.  《Larghetto / Epilogo Adagio》 – 2:13
10. 《Herr Mannelig (short version)》 – 6:10

### 限量版

限量版增加以下兩首：

1.  《De La Morte Noir》 – 7:59
2.  《Robin's Song》 – 4:36

同時附加一張DVD，包含1998年於「Wacken Open Air」的5段影片以及一部附贈影片。

1.  《Requiem》
2.  《In A Pale Moon's Shadow》
3.  《Cantus Firmus》
4.  《De La Morte Noir》
5.  《Lost (Robin)》
6.  《In a Pale Moon's Shadow》(bonus video clip)

[Category:2004年音樂專輯](../Category/2004年音樂專輯.md "wikilink")
[Category:曠野鷹豪樂團專輯](../Category/曠野鷹豪樂團專輯.md "wikilink")