**軟體授權條款**是一種具有[法律性质的合同或指导](../Page/法律.md "wikilink")，目的在規範受[著作權保護的](../Page/著作權.md "wikilink")[軟體的使用或散佈行為](../Page/軟體.md "wikilink")。通常的授權方式會允許用戶來使用單一或多份該軟體的複製，因為若無授權而徑予使用該軟體，將違反著作權法給予該軟體開發者的專屬保護。效用上來說，軟體授權是軟體開發者與其用戶之間的一份[合約](../Page/合約.md "wikilink")，用來保證在符合授權範圍的情況下，用戶將不會受到控告。

## 授權方式

軟體的授權方式大致可分為[專屬軟體與](../Page/專屬軟體.md "wikilink")[自由开源软件](../Page/自由开源软件.md "wikilink")。其主要區別在授予使用者的[權利有所不同](../Page/權利.md "wikilink")。

## 软件许可证和版权法

大多数分布式软件可以根据其许可类型进行分类(见下表)。

<table>
<caption>Software licenses and rights granted in context of the copyright according to <a href="../Page/Mark_Webbink.md" title="wikilink">Mark Webbink</a>.[1] Expanded by freeware and sublicensing.</caption>
<thead>
<tr class="header">
<th><p><a href="../Page/授權_(法律).md" title="wikilink">授取权利</a></p></th>
<th><p><a href="../Page/Public_domain_software.md" title="wikilink">Public domain</a></p></th>
<th><p><a href="../Page/Permissive_license.md" title="wikilink">Permissive</a> <a href="../Page/Free_and_open-source_software.md" title="wikilink">FOSS</a><br />
license (e.g. <a href="../Page/BSD_license.md" title="wikilink">BSD license</a>)</p></th>
<th><p><a href="../Page/Copyleft_license.md" title="wikilink">Copyleft</a> FOSS<br />
license (e.g. <a href="../Page/GPL.md" title="wikilink">GPL</a>)</p></th>
<th><p><a href="../Page/Freeware.md" title="wikilink">Freeware</a>/<a href="../Page/Shareware.md" title="wikilink">Shareware</a>/<br />
<a href="../Page/Freemium.md" title="wikilink">Freemium</a></p></th>
<th><p><a href="../Page/Proprietary_software.md" title="wikilink">Proprietary license</a></p></th>
<th><p><a href="../Page/Trade_secret.md" title="wikilink">Trade secret</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Copyright.md" title="wikilink">Copyright</a> retained</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Performing_rights.md" title="wikilink">Right to perform</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Right to display</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Right to copy</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Derivative_works.md" title="wikilink">Right to modify</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Right to distribute</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Right to sublicense</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Example software</p></td>
<td><p><a href="../Page/SQLite.md" title="wikilink">SQLite</a>, <a href="../Page/ImageJ.md" title="wikilink">ImageJ</a></p></td>
<td><p><a href="../Page/Apache_web_server.md" title="wikilink">Apache web server</a>, <a href="../Page/ToyBox.md" title="wikilink">ToyBox</a></p></td>
<td><p><a href="../Page/Linux_kernel.md" title="wikilink">Linux kernel</a>, <a href="../Page/GIMP.md" title="wikilink">GIMP</a>, <a href="../Page/Open_Broadcaster_Software.md" title="wikilink">OBS</a></p></td>
<td><p><a href="../Page/Irfanview.md" title="wikilink">Irfanview</a>, <a href="../Page/Winamp.md" title="wikilink">Winamp</a>, <em><a href="../Page/League_of_Legends.md" title="wikilink">League of Legends</a></em></p></td>
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a>, the majority of commercial video games and their <a href="../Page/Digital_rights_management.md" title="wikilink">DRMs</a>, <a href="../Page/Spotify.md" title="wikilink">Spotify</a>, <a href="../Page/xSplit.md" title="wikilink">xSplit</a>, <a href="../Page/Tidal_(service).md" title="wikilink">TIDAL</a></p></td>
<td><p>Server-side<br />
<a href="../Page/Cloud_computing.md" title="wikilink">Cloud computing</a><br />
Games by <a href="../Page/Blizzard_Entertainment.md" title="wikilink">Blizzard Entertainment</a>, <a href="../Page/Rockstar_Games.md" title="wikilink">Rockstar</a>, <a href="../Page/Activision.md" title="wikilink">Activision</a>, etc.<br />
<a href="../Page/PlayStation_Network.md" title="wikilink">PlayStation Network</a> and <a href="../Page/Xbox_Live.md" title="wikilink">Xbox Live</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 专有软件许可证

## 自由开源软件许可证

  -
  - [開源授權](../Page/開源授權.md "wikilink")

## 参见

  - [著作權](../Page/著作權.md "wikilink")
  - [專屬軟體](../Page/專屬軟體.md "wikilink")
  - [自由軟體](../Page/自由軟體.md "wikilink")
  - [共享軟體](../Page/共享軟體.md "wikilink")
  - [软件许可证列表](../Page/软件许可证列表.md "wikilink")

## 外部链接

[Category:軟體授權](../Category/軟體授權.md "wikilink")

1.