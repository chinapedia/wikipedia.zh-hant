**吴祖泽**()，生于[浙江省](../Page/浙江省.md "wikilink")[宁波市](../Page/宁波市.md "wikilink")[镇海县](../Page/镇海区_\(宁波市\).md "wikilink")，[中国血液学家](../Page/中国.md "wikilink")，[中国科学院](../Page/中国科学院.md "wikilink")[院士](../Page/院士.md "wikilink")。

## 生平

1957年9月，毕业于[山东大学](../Page/山东大学.md "wikilink")。\[1\]1994年2月，任[军事医学科学院院长](../Page/军事医学科学院.md "wikilink")（[少将军衔](../Page/少将.md "wikilink")）。1993年12月，当选为[中国科学院院士](../Page/中国科学院院士.md "wikilink")（[生物学与](../Page/生物学.md "wikilink")[医学学部](../Page/医学.md "wikilink")）。他是[国际辐射研究协会的第一位中国理事](../Page/国际辐射研究协会.md "wikilink")。

## 參考

[Category:中国医学家](../Category/中国医学家.md "wikilink")
[Category:国立山东大学校友](../Category/国立山东大学校友.md "wikilink")
[Category:山东大学校友](../Category/山东大学校友.md "wikilink")
[Category:镇海县人](../Category/镇海县人.md "wikilink")
[Z](../Category/吴姓.md "wikilink")

1.