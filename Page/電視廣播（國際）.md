**電視廣播（國際）有限公司**（（又稱「**無綫電視（國際）**」），英文又稱和簡稱分別為和）是由[香港](../Page/香港.md "wikilink")[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")（又稱「**無綫電視**」）於1976年成立，專責營運無綫的[全球業務](../Page/全球.md "wikilink")。TVBI為全球各地提供[電視節目製作](../Page/電視節目.md "wikilink")、收費及[衛星電視頻道](../Page/衛星電視.md "wikilink")、[有線電視頻道和](../Page/有線電視.md "wikilink")[錄影帶及](../Page/錄影帶.md "wikilink")[光碟租賃等服務](../Page/光碟.md "wikilink")。在[台灣](../Page/台灣.md "wikilink")、[美國](../Page/美國.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡等地均有TVBI的電視頻道](../Page/新加坡.md "wikilink")，供三千多萬[華人收看](../Page/華人.md "wikilink")。

TVBI並會為節目[配音](../Page/配音.md "wikilink")，除了[普通話之外](../Page/普通話.md "wikilink")，還有[泰語](../Page/泰語.md "wikilink")、[越南語及](../Page/越南語.md "wikilink")[柬埔寨語和](../Page/柬埔寨語.md "wikilink")[閩南語](../Page/閩南語.md "wikilink")，發行到超過30多個[國家](../Page/國家.md "wikilink")，售予當地[電視台或](../Page/電視台.md "wikilink")[出租節目光碟和錄影帶](../Page/出租.md "wikilink")。另外，現時無綫大部份[中國大陸業務都經由TVBI經營和中介](../Page/中國大陸.md "wikilink")，包括節目[版權](../Page/版權.md "wikilink")、衛星頻道、節目製作等。

## 無綫在世界各地經營的業務

### 國際業務

[TVBUSA.png](https://zh.wikipedia.org/wiki/File:TVBUSA.png "fig:TVBUSA.png")
早於1984年，TVBI已在美國[洛杉磯發展有線電視](../Page/洛杉磯.md "wikilink")。1991年，TVBI有線電視業務擴展到[三藩市](../Page/三藩市.md "wikilink")。1998年12月7日，TVBI成立華語綜合頻道[TVB8及全球首條華語劇集頻道](../Page/TVB8.md "wikilink")[星河頻道](../Page/TVB星河頻道.md "wikilink")，由衛星轉送至[亞洲](../Page/亞洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[澳洲及部份](../Page/澳洲.md "wikilink")[東歐地區](../Page/東歐.md "wikilink")，並於[印尼](../Page/印尼.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡等收費電視台落地](../Page/新加坡.md "wikilink")，TVB8部份節目亦會在TVBI其他平台內播出。而另一條頻道[翡翠衛星台](../Page/翡翠衛星台.md "wikilink")（TVBJ），播放地包括[澳洲](../Page/澳洲.md "wikilink")、新加坡、[越南等](../Page/越南.md "wikilink")。

至2000年，TVBI在美國和澳洲成立收費衛星電視平台「[JadeWorld](../Page/JadeWorld.md "wikilink")」（[翡翠互動電視](../Page/翡翠互動電視.md "wikilink")），提供多頻道[數碼廣播](../Page/數碼廣播.md "wikilink")，為海外觀眾提供緊貼[翡翠台播放進度的電視節目](../Page/翡翠台.md "wikilink")，現時訂戶共超逾25萬。2006年，由於美國操[普通話和其他](../Page/普通話.md "wikilink")[方言的](../Page/方言.md "wikilink")[人口增加](../Page/人口.md "wikilink")，TVBI在JadeWorld推出[TVB
Vietnam](../Page/TVB_Vietnam.md "wikilink")（TVBV）頻道提供[越南語配音的TVB節目](../Page/越南語.md "wikilink")，並計劃新增普通話頻道。2009年，TVB正式落地[紐約有線電視](../Page/紐約.md "wikilink")。而澳洲的JadeWorld在2006年新增[無線韓劇台](../Page/無線韓劇台.md "wikilink")，吸引喜歡收看[韓國電視劇的觀眾](../Page/韓國電視劇.md "wikilink")。

1995年，TVBI收購在歐洲經營的「The Chinese
Channel」（TCC）60%[股權](../Page/股權.md "wikilink")，接手管理旗下電視頻道「時視」。1997年，時視改名為「無線衛星台」；及後於2003年11月，TVBI全面收購TCC。2006年，TVB表示，為配合歐洲普通話人口增長，TCC正計劃推出普通話綜合娛樂頻道，以提高現有粵語頻道的吸引力。2006年10月10日，TVBI獨家授權[JumpTV在歐洲以至](../Page/JumpTV.md "wikilink")[中東地區](../Page/中東.md "wikilink")，透過互聯網播放無線衛星台及[星河頻道](../Page/TVB星河頻道.md "wikilink")。

### 馬來西亞業務

由於[馬來西亞政府不允許外資經營電視台](../Page/馬來西亞.md "wikilink")，TVBI與馬來西亞[Astro合作開辦](../Page/Astro.md "wikilink")「[Astro華麗台](../Page/Astro華麗台.md "wikilink")」提供顧問及[廣告代理服務](../Page/廣告.md "wikilink")，而該台以播放TVB節目為主。[Astro
On
Demand剧集首映是TVBI與Astro合力開辦的第二个播放](../Page/Astro_On_Demand.md "wikilink")[港劇的電視頻道](../Page/港劇.md "wikilink")，播放與香港同步首播最新的港劇，同時也會播出TVB海外首發劇集。而Astro平台播放的TVB8節目《[殘酷一叮](../Page/殘酷一叮.md "wikilink")》、《TVB8金曲榜季選》等皆為當地高收視節目，華人觀眾佔有率逾30%。另外，TVBI亦與[泰國第3電視台合組](../Page/泰國第3電視台.md "wikilink")「TVB
3 Network
Company」，在[泰國製作](../Page/泰國.md "wikilink")[泰語綜藝節目](../Page/泰語.md "wikilink")。

2004年末，無綫電視對[報章表示](../Page/報章.md "wikilink")，計劃與其他亞洲國家的電視台合作拍攝劇集，並安排在當地播放。2005年1月4日，[新加坡](../Page/新加坡.md "wikilink")[新傳媒電視與TVBI宣佈首次合製電視劇](../Page/新傳媒電視.md "wikilink")《[Yummy
Yummy](../Page/Yummy_Yummy.md "wikilink")》\[1\]。其後，無綫再與泰國第3電視台合製電視劇《[爭分奪秒](../Page/爭分奪秒_\(電視劇\).md "wikilink")》。

### 中國大陸業務

早於1998年，無綫衛星電視頻道「TVB8」及「星河頻道」獲[中國廣電總局批准在中國大陸有限落地](../Page/中華人民共和國國家廣播電影電視總局.md "wikilink")，主要為三星或以上級別[酒店和涉外單位](../Page/酒店.md "wikilink")（[大使館等](../Page/大使館.md "wikilink")）提供服務。

2004年10月27日，香港與中國內地簽訂《[更緊密經貿關係安排](../Page/更緊密經貿關係安排.md "wikilink")》（CEPA）
的補充協議，進一步開放大陸電視市場，允許[香港電視製作單位以](../Page/香港電視廣播.md "wikilink")「合拍」電視劇的形式進入大陸市場。有別於過往的「協拍」模式，「合拍」劇集均可視為「國產劇集」在大陸發行及播放，毋須將劇集版權售予大陸。無綫在2004年表示，每年增加製作120小時「合拍」電視劇，題材以中國古代[武俠故事為主](../Page/武俠.md "wikilink")。第1套與大陸製作公司合作拍攝的電視劇為《[游劍江湖](../Page/游劍江湖_\(電視劇\).md "wikilink")》，由作家[梁羽生的](../Page/梁羽生.md "wikilink")[小說改編](../Page/小說.md "wikilink")。

無綫亦與[上海文廣新聞傳媒集團成員](../Page/上海文廣新聞傳媒集團.md "wikilink")「上海天視文化傳播有限公司」合資成立公司，於2003年開始合拍大陸「國產劇集」和資訊節目，如《[恭親王](../Page/恭親王.md "wikilink")》、《歷史的天空》、《娛樂星天地》。2005年中，開拍劇集《[一生為奴](../Page/一生為奴.md "wikilink")》。惟無綫於2006年6月24日入稟[法庭控告上海天視違反兩項協議](../Page/法庭.md "wikilink")——拒絕向無綫支付版權費和分擔虧損（共約1500萬[人民幣](../Page/人民幣.md "wikilink")）\[2\]。另外，報道指無綫在大陸推出劇集光碟，但[盜版問題嚴重影響銷量](../Page/盜版.md "wikilink")，無綫亦需要更換代理商。

同期，無綫與[中國中央電視台](../Page/中國中央電視台.md "wikilink")（央視）簽訂戰略合作框架協議，合作範圍擴展到合拍電視劇和其他計劃，例如在2007年初開拍劇集《[歲月風雲](../Page/歲月風雲.md "wikilink")》。央視的競爭對手[湖南衛視亦有意與無綫合作](../Page/湖南衛視.md "wikilink")，湖南衛視總編室主任[李浩說](../Page/李浩.md "wikilink")：「我們也在考慮與TVB拍合拍片，一些偶像劇正在策劃。不僅是電視劇，我們也希望在娛樂節目方面與TVB進行合作。目前一些相關專案正在落實過程中。」\[3\]另外，[中國中央電視台综合頻道首次引進多套無綫製作的劇集](../Page/中國中央電視台综合頻道.md "wikilink")，無綫指成績令人鼓舞。\[4\]2009年，[上海广播电视台也与無綫电视合作了剧集](../Page/上海广播电视台.md "wikilink")《[摘星之旅](../Page/摘星之旅.md "wikilink")》，剧集于2010年播出。

2006年6月，由於無綫劇集《[金枝慾孽](../Page/金枝慾孽.md "wikilink")》2005年在大陸收視率甚高，引起電視台爭奪同類劇集《[火舞黃沙](../Page/火舞黃沙.md "wikilink")》。湖南衛視與央視互出高價競投《火舞黃沙》的播放權。報導指央視出價2.5萬[美金一集](../Page/美金.md "wikilink")，比一般的1.2萬至1.8萬美金高。\[5\]同时，内地其他电视台也引进了无线近年来高口碑或者高收视的剧集，例如[东方卫视的](../Page/东方卫视.md "wikilink")《[溏心风暴](../Page/溏心风暴.md "wikilink")》、《[溏心风暴之家好月圆](../Page/溏心风暴之家好月圆.md "wikilink")》。

2008年8月8日，無綫電視、[華人文化產業股權投資（上海）中心（有限合夥）與](../Page/华人文化产业投资基金.md "wikilink")[上海東方傳媒集團有限公司於上海正式組成](../Page/上海東方傳媒集團有限公司.md "wikilink")[翡翠東方傳媒有限公司](../Page/翡翠東方.md "wikilink")（TVBC），藉此全力展推無綫電視在中國的業務；當中包括發行無綫電視節目予內地電視台和互聯網站、在廣東省管理和分銷無綫電視頻道、代理無綫電視香港頻道在內地的廣告銷售，及投資和分銷TVBC所製作的華語劇集。在未來的日子，憑藉TVBC在內地的強大業務網絡，必可為無綫電視於中國大陸市場帶來獨特優勢，發展更全面的媒體業務及藝人演出活動，帶領無綫電視邁向一個全新的時代。

### 台灣業務

1982年，無綫劇集《[楚留香](../Page/楚留香_\(電視劇\).md "wikilink")》由[中國電視公司購入台灣播映權](../Page/中國電視公司.md "wikilink")，掀起1980年代台灣港劇熱潮；在此熱潮下，TVBI授權[年代影視在台灣發行無綫劇集錄影帶](../Page/年代影視.md "wikilink")，標示當時TVBI使用的「HK-TVB
International」商標，提供[Betamax與](../Page/Betamax.md "wikilink")[VHS格式](../Page/VHS.md "wikilink")，遍布台灣各地錄影帶出租店\[6\]。

無綫與年代影視更在1993年成立[TVBS](../Page/TVBS.md "wikilink")，從事電視節目製作、雜誌、書籍出版等業務，其頻道在香港、美國、澳洲、日本等地均有落地。2005年，由於年代集團將TVBS股權全數售予無綫，加上TVBS《[2100全民開講](../Page/2100全民開講.md "wikilink")》等部份節目揭發[陳水扁政府醜聞](../Page/陳水扁政府.md "wikilink")（[高雄捷運外勞弊案](../Page/高雄捷運外勞弊案.md "wikilink")、[國務機要費案等](../Page/國務機要費案.md "wikilink")），被疑以「大陸資本」操作，最終被裁定指控不成立。不過到2016年末，TVB最終將所持TVBS股權全部轉賣，結束台灣業務；而TVBS也改用新的企業識别，新聞報道也不再使用與[無綫新聞一樣的開場旋律](../Page/無綫新聞.md "wikilink")。

1990年代末期，TVBI授權[年代集團旗下的](../Page/年代集團.md "wikilink")[勇士物流在台灣發行無綫劇集光碟](../Page/勇士物流.md "wikilink")，提供[VCD與](../Page/VCD.md "wikilink")[DVD-Video格式](../Page/DVD-Video.md "wikilink")。2001年勇士物流涉嫌[惡性倒閉](../Page/惡性倒閉.md "wikilink")\[7\]以後，TVBI改授權[弘音多媒體在台灣發行無綫劇集光碟至](../Page/弘音多媒體.md "wikilink")2008年。2008年，TVBI授權[群體國際在台灣發行無綫劇集光碟](../Page/群體國際.md "wikilink")。

### 日本業務

2002年1月，TVBI与日本[株式会社大富合作](../Page/株式会社大富.md "wikilink")，将無綫主力频道以及TVBS的代表性娱乐节目整合為單一頻道[TVB大富](../Page/TVB大富.md "wikilink")，在日本落地播出，播出语种为普通话和粤语。2011年9月30日，TVBI与大富合约终止，TVB大富停播。

### 互聯網業務

2006年5月24日，無綫電視副董事總經理[陳禎祥透露](../Page/陳禎祥.md "wikilink")，無綫2006年初開始與內地多個合作伙伴開展互聯網[自選影像服務](../Page/自選影像.md "wikilink")，內地網民可於網站點播無綫節目；合作模式包括按點播率拆帳，或直接出售版權。現時的合作伙伴包括[上海優度寬頻科技](https://web.archive.org/web/20080705143741/http://www.viewtoo.com/)、[PPS影音等](../Page/PPS影音.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

## 參見

  - [電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")

## 外部連結

  - [電視廣播有限公司](http://www.tvb.com/)

<!-- end list -->

  - [電視廣播(美國)有限公司](http://www.tvbusa.com/)
  - [電視廣播(澳洲)有限公司](http://www.tvb.com.au/)
  - [無線衛星台有限公司](https://web.archive.org/web/20160304121352/http://www.chinese-channel.co.uk/)

[電視廣播（國際）有限公司](../Category/電視廣播（國際）有限公司.md "wikilink")
[Category:1976年成立的公司](../Category/1976年成立的公司.md "wikilink")

1.  [MediaCorp 新聞發佈 - MediaCorp 與 TVBI
    的首次合作](http://www.corporate.mediacorp.sg/press_release/pr_1104823314.htm)
     (英文)
2.  控內地媒體違約 無綫索償1500萬，2006-06-25，港聞 A11，成報
3.  [電視臺暗戰港劇](http://www.crftv.com.cn/showNews.asp?newsID=2191)
4.  [TVB 2006
    全年業績公佈](http://www.tvb.com/affairs/faq/announce/2006/pdf/yrend2006_c.pdf)
5.  湖南衞視與央視較勁爭奪《火舞黃沙》“身價”暴漲港產電視劇迎來第二春，2006-06-24，A17，大河報，劉嘉怡
6.  [HK-TVB
    International商標中文動畫結尾](http://logos.wikia.com/wiki/File:HK-TVB_International_Limited_logo.png)
7.