**1月2日**是[公历年的第](../Page/公历.md "wikilink")2天，离一年的结束还有363天（[闰年是](../Page/闰年.md "wikilink")364天）。

## 大事记

### 4世紀

  - [366年](../Page/366年.md "wikilink")：[日耳曼一个部落同盟](../Page/日耳曼尼亚.md "wikilink")[阿拉曼人越过冰封的](../Page/阿拉曼人.md "wikilink")[莱茵河](../Page/莱茵河.md "wikilink")，入侵[罗马帝国](../Page/罗马帝国.md "wikilink")。

### 6世紀

  - [533年](../Page/533年.md "wikilink")：[教宗](../Page/教宗.md "wikilink")[若望二世即位](../Page/若望二世.md "wikilink")，是第一位使用新名號即位的教宗。

### 15世紀

  - [1492年](../Page/1492年.md "wikilink")：[西班牙军队攻下了](../Page/西班牙.md "wikilink")[摩尔人在](../Page/摩尔人.md "wikilink")[伊比利亚半岛的最后一个重镇](../Page/伊比利亚半岛.md "wikilink")[格拉纳达](../Page/格拉纳达.md "wikilink")，[收復失地運動结束](../Page/收復失地運動.md "wikilink")。

### 18世紀

  - [1777年](../Page/1777年.md "wikilink")：[喬治·華盛頓領導的](../Page/喬治·華盛頓.md "wikilink")[大陸軍在](../Page/大陸軍.md "wikilink")[紐澤西州](../Page/紐澤西州.md "wikilink")[翠登附近發生的](../Page/翠登.md "wikilink")[阿孫平克溪戰役中擊退](../Page/阿孫平克溪戰役.md "wikilink")[英國軍隊](../Page/英國軍隊.md "wikilink")。

### 20世紀

  - [1905年](../Page/1905年.md "wikilink")：[日俄战争](../Page/日俄战争.md "wikilink")：駐紮在[中國](../Page/中國.md "wikilink")[旅順口區](../Page/旅順口區.md "wikilink")[海軍基地的](../Page/海軍基地.md "wikilink")[俄羅斯部隊向](../Page/俄羅斯部隊.md "wikilink")[日本軍隊投降](../Page/日本軍隊.md "wikilink")，[旅順會戰宣告結束](../Page/旅順會戰.md "wikilink")。
  - [1909年](../Page/1909年.md "wikilink")：[清政府将](../Page/清朝.md "wikilink")[袁世凯撤职](../Page/袁世凯.md "wikilink")。
  - [1920年](../Page/1920年.md "wikilink")：[美国](../Page/美国.md "wikilink")[禁酒令生效](../Page/禁酒令.md "wikilink")。
  - [1921年](../Page/1921年.md "wikilink")：[卢森堡](../Page/卢森堡.md "wikilink")[共产党成立](../Page/卢森堡共产党.md "wikilink")。
  - [1923年](../Page/1923年.md "wikilink")：中国国民党党纲公布。
  - [1932年](../Page/1932年.md "wikilink")：日军攻占[锦州](../Page/锦州.md "wikilink")，全部占领[中国东北](../Page/中国东北.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：平津学生组成南下抗日宣传团在[河北固安县进行抗日宣传](../Page/河北.md "wikilink")。
  - [1941年](../Page/1941年.md "wikilink"):
    [納粹德國空軍針對](../Page/納粹德國.md "wikilink")[英國](../Page/英國.md "wikilink")[威爾斯](../Page/威爾斯.md "wikilink")[卡地夫展開大規模轟炸](../Page/卡地夫.md "wikilink")，造成[蘭達夫座堂嚴重受損](../Page/蘭達夫座堂.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：中国军队在[第三次长沙战役击败日军](../Page/第三次长沙战役.md "wikilink")。
  - 1942年：日军占领[菲律宾首都](../Page/菲律宾.md "wikilink")[马尼拉](../Page/马尼拉.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[罗盛教牺牲](../Page/罗盛教.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[南斯拉夫与](../Page/南斯拉夫.md "wikilink")[中国建交](../Page/中国.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：[苏联在](../Page/苏联.md "wikilink")[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")[拜科努尔发射场发射了世界上第一个星际](../Page/拜科努尔发射场.md "wikilink")[探测器](../Page/空间探测器.md "wikilink")[月球1号](../Page/月球1号.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：[捷·古華拉率革命軍成功佔領](../Page/捷·古華拉.md "wikilink")[古巴首都](../Page/古巴.md "wikilink")[哈瓦那](../Page/哈瓦那.md "wikilink")，[巴蒂斯塔出逃](../Page/巴蒂斯塔.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：
  - [1967年](../Page/1967年.md "wikilink")：前[美國電影演員](../Page/美國.md "wikilink")[隆納·雷根宣誓就任第](../Page/隆納·雷根.md "wikilink")33任[加利福尼亞州州長](../Page/加利福尼亞州州長.md "wikilink")，開始投入政治工作。
  - [1971年](../Page/1971年.md "wikilink")：[苏甲](../Page/苏甲.md "wikilink")[格拉斯哥流浪者队在主场](../Page/流浪者足球俱樂部.md "wikilink")[埃布羅克斯球場对阵](../Page/埃布羅克斯球場.md "wikilink")[凱尔特人队的比赛中发生重大踩踏事故](../Page/凱爾特人足球俱樂部.md "wikilink")，66人死亡，145人受伤。
  - [1977年](../Page/1977年.md "wikilink")：[洛阳黄河公路大桥建成通车](../Page/洛阳.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[中国与](../Page/中国.md "wikilink")[厄瓜多尔建交](../Page/厄瓜多尔.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[音樂劇](../Page/音樂劇.md "wikilink")《》在表演了2377場後，在[紐約市](../Page/紐約市.md "wikilink")[百老匯的Alvin](../Page/百老匯.md "wikilink")
    Theatre進行最後一次演出。
  - [1988年](../Page/1988年.md "wikilink")：[香港廉政公署拘捕](../Page/香港廉政公署.md "wikilink")[香港聯合交易所高層人員](../Page/香港聯合交易所.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[金門與](../Page/金門.md "wikilink")[廈門之間開通](../Page/廈門.md "wikilink")「[小三通](../Page/小三通.md "wikilink")」。
  - [2004年](../Page/2004年.md "wikilink")：[美国](../Page/美国.md "wikilink")[星尘号](../Page/星尘号.md "wikilink")[太空探测器飞过](../Page/太空探测器.md "wikilink")[维尔特二号](../Page/维尔特二号.md "wikilink")[彗星](../Page/彗星.md "wikilink")，采集彗星[尘埃及](../Page/尘埃.md "wikilink")[气体样本](../Page/气体.md "wikilink")。
  - [2015年](../Page/2015年.md "wikilink")：[哈爾濱市道外區「1·2」火災事故](../Page/哈爾濱市道外區「1·2」火災事故.md "wikilink")。
  - [2019年](../Page/2019年.md "wikilink")：[中共中央總書記](../Page/中國共產黨中央委員會總書記.md "wikilink")[習近平在](../Page/習近平.md "wikilink")《[告台灣同胞書](../Page/告台灣同胞書.md "wikilink")》發表40周年紀念會上發表「[重要講話](../Page/習五條.md "wikilink")」重申[兩岸統一](../Page/中國統一.md "wikilink")。

## 出生

  - [1642年](../Page/1642年.md "wikilink")：[穆罕默德四世](../Page/穆罕默德四世_\(奥斯曼帝国\).md "wikilink")，[奥斯曼土耳其帝国蘇丹](../Page/奥斯曼土耳其帝国.md "wikilink")（1693年去世）
  - [1699年](../Page/1699年.md "wikilink")：[奥斯曼三世](../Page/奥斯曼三世.md "wikilink")，奧斯曼帝國第25任蘇丹兼任哈里發（1757年去世）
  - [1732年](../Page/1732年.md "wikilink")：[弗朗蒂舍克·布里克西](../Page/弗朗蒂舍克·布里克西.md "wikilink")，[波西米亞作曲家](../Page/波西米亞.md "wikilink")（1771年去世）
  - [1822年](../Page/1822年.md "wikilink")：[魯道夫·克勞修斯](../Page/魯道夫·克勞修斯.md "wikilink")，[德國](../Page/德國.md "wikilink")[物理學家和](../Page/物理學家.md "wikilink")[數學家](../Page/數學家.md "wikilink")，[熱力學主要奠基人](../Page/熱力學.md "wikilink")（1888年去世）
  - [1837年](../Page/1837年.md "wikilink")：[米利·阿列克谢耶维奇·巴拉基列夫](../Page/米利·阿列克谢耶维奇·巴拉基列夫.md "wikilink")，[俄羅斯鋼琴演奏家](../Page/俄羅斯.md "wikilink")、指揮家和作曲家（1910年去世）
  - [1875年](../Page/1875年.md "wikilink")：[沈钧儒](../Page/沈钧儒.md "wikilink")，[中華人民共和國最高人民法院院長](../Page/中華人民共和國最高人民法院.md "wikilink")（1963年去世）
  - [1900年](../Page/1900年.md "wikilink")：[黄子卿](../Page/黄子卿.md "wikilink")，[中国](../Page/中国.md "wikilink")[化学家](../Page/化学家.md "wikilink")（1982年去世）
  - [1901年](../Page/1901年.md "wikilink")：[黃海岱](../Page/黃海岱.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[布袋戲國寶級大師](../Page/布袋戲.md "wikilink")（2007年去世）
  - [1905年](../Page/1905年.md "wikilink")：[迈克尔·蒂皮特](../Page/迈克尔·蒂皮特.md "wikilink")，[英国作曲家](../Page/英国.md "wikilink")（1998年去世）
  - [1909年](../Page/1909年.md "wikilink")：[貝利·高華德](../Page/貝利·高華德.md "wikilink")，[美國](../Page/美國.md "wikilink")[亞利桑那州參議員](../Page/亞利桑那州.md "wikilink")（1998年去世）
  - [1920年](../Page/1920年.md "wikilink")：[-{zh-hans:艾萨克·阿西莫夫;
    zh-hant:以撒·艾西莫夫;}-](../Page/艾萨克·阿西莫夫.md "wikilink")，美國[科幻小说](../Page/科幻小说.md "wikilink")[作家](../Page/作家.md "wikilink")（1992年去世）
  - [1928年](../Page/1928年.md "wikilink")：[池田大作](../Page/池田大作.md "wikilink")，[日本作家](../Page/日本.md "wikilink")、攝影師
  - [1931年](../Page/1931年.md "wikilink")：[海部俊樹](../Page/海部俊樹.md "wikilink")，日本第76、77任[內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")
  - [1933年](../Page/1933年.md "wikilink")：[森村诚一](../Page/森村诚一.md "wikilink")，日本[推理小说作家](../Page/推理小说.md "wikilink")
  - [1938年](../Page/1938年.md "wikilink")：[高建](../Page/高建_\(韓國\).md "wikilink")，前[韓國漢城市長](../Page/韓國.md "wikilink")、國務總理、暫代總統
  - [1939年](../Page/1939年.md "wikilink")：[夏佳理](../Page/夏佳理.md "wikilink")，[香港政府官员](../Page/香港.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[S·R·S·瓦拉德汉](../Page/S·R·S·瓦拉德汉.md "wikilink")，[印度裔美國數學家](../Page/印度.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[拉那烈亲王](../Page/拉那烈亲王.md "wikilink")，[柬埔寨前政府第一首相](../Page/柬埔寨.md "wikilink")
  - [1948年](../Page/1948年.md "wikilink")：[朱迪思·米勒](../Page/朱迪思·米勒.md "wikilink")，[紐約時報記者](../Page/紐約時報.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[吳孟達](../Page/吳孟達.md "wikilink")，香港男演員
  - [1960年](../Page/1960年.md "wikilink")：[浦澤直樹](../Page/浦澤直樹.md "wikilink")，日本漫画家
  - [1962年](../Page/1962年.md "wikilink")：[速水健太郎](../Page/jp:速水けんたろう.md "wikilink")，日本男歌手
  - [1964年](../Page/1964年.md "wikilink")：[鄧景輝](../Page/鄧景輝.md "wikilink")，[香港前新聞主播](../Page/香港.md "wikilink")（2015年去世）
  - [1964年](../Page/1964年.md "wikilink")：[诺丽雅卡斯农](../Page/诺丽雅卡斯农.md "wikilink")，马来西亚内阁副部长（[2016年去世](../Page/2016年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[小库珀·古丁](../Page/小库珀·古丁.md "wikilink")，美國演員
  - [1968年](../Page/1968年.md "wikilink")：[須田剛一](../Page/須田剛一.md "wikilink")，日本遊戲設計師、遊戲製作人、編劇
  - [1969年](../Page/1969年.md "wikilink")：[克莉絲蒂·杜靈頓](../Page/克莉絲蒂·杜靈頓.md "wikilink")，美國[超級名模](../Page/超級名模.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[竹野內豐](../Page/竹野內豐.md "wikilink")，日本男演員
  - [1975年](../Page/1975年.md "wikilink")：[陳曉明](../Page/陳曉明.md "wikilink")，香港足球教練
  - [1975年](../Page/1975年.md "wikilink")：[傑夫·蘇潘](../Page/傑夫·蘇潘.md "wikilink")，美國職棒大聯盟投手
  - [1977年](../Page/1977年.md "wikilink")：[斯特凡·庫貝克](../Page/斯特凡·庫貝克.md "wikilink")，[奧地利職業網球運動員](../Page/奧地利.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[豐口惠](../Page/豐口惠.md "wikilink")，日本女性聲優
  - [1981年](../Page/1981年.md "wikilink")：[-{zh-hans:柯克·辛里奇;zh-hant:柯克·韓瑞克}-](../Page/柯克·辛里奇.md "wikilink")，美国职业[篮球](../Page/篮球.md "wikilink")[运动员](../Page/运动员.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[-{zh-hans:马克西·罗德里格斯;zh-hk:麥斯·洛迪古斯;zh-tw:馬克西·羅德里格茲;}-](../Page/马克西米利亚诺·罗德里格斯.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")[足球运动员](../Page/足球.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[姬蒂·寶絲禾芙](../Page/凱特·伯斯沃斯.md "wikilink")，美国女演员
  - [1986年](../Page/1986年.md "wikilink")：[夏語心](../Page/夏語心.md "wikilink")，台灣女藝人
  - [1988年](../Page/1988年.md "wikilink")：[明坂聰美](../Page/明坂聰美.md "wikilink")，日本女性聲優
  - [1988年](../Page/1988年.md "wikilink")：[-{zh-hans:乔尼·埃文斯;zh-hk:莊尼·伊雲斯;zh-tw:喬尼·埃文斯;}-](../Page/乔尼·埃文斯.md "wikilink")，[北愛爾蘭足球員](../Page/北愛爾蘭.md "wikilink")，現效力[曼聯](../Page/曼聯.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[达维德·桑顿](../Page/达维德·桑顿.md "wikilink")，[義大利足球運動員](../Page/義大利.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[有智](../Page/有智.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[BESTie前成員](../Page/BESTie.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[郭曉婷](../Page/郭曉婷.md "wikilink")，[中國女演員](../Page/中國.md "wikilink")

## 逝世

  - [1952年](../Page/1952年.md "wikilink")：[罗盛教](../Page/罗盛教.md "wikilink")，[中国人民志愿军战士](../Page/中国人民志愿军.md "wikilink")（1931年出生）
  - [1999年](../Page/1999年.md "wikilink")：[賽巴斯提安·哈夫納](../Page/賽巴斯提安·哈夫納.md "wikilink")，[德國](../Page/德國.md "wikilink")[記者](../Page/記者.md "wikilink")、[作家](../Page/作家.md "wikilink")（1907年出生）
  - [2006年](../Page/2006年.md "wikilink")：[比爾·史凱特](../Page/比爾·史凱特.md "wikilink")，[巴布亞紐幾內亞前總理](../Page/巴布亚新几内亚总理.md "wikilink")（1952年出生）
  - [2006年](../Page/2006年.md "wikilink")：[楊子江](../Page/楊子江_\(香港\).md "wikilink")，[香港主持人](../Page/香港.md "wikilink")（1958年出生）
  - [2007年](../Page/2007年.md "wikilink")：[白南淳](../Page/白南淳.md "wikilink")，[朝鮮](../Page/朝鮮.md "wikilink")[外务相](../Page/外交官.md "wikilink")（1929年出生）
  - [2011年](../Page/2011年.md "wikilink")：[司徒華](../Page/司徒華.md "wikilink")，[香港的](../Page/香港.md "wikilink")[香港民主黨黨鞭](../Page/香港民主黨.md "wikilink")、[支聯會主席](../Page/支聯會.md "wikilink")、香港民主派元老（1931年出生）
  - [2011年](../Page/2011年.md "wikilink")：[理查德·溫特斯](../Page/理查德·溫特斯.md "wikilink")，[美國陸軍](../Page/美國陸軍.md "wikilink")[第101空降師](../Page/第101空降師.md "wikilink")506傘兵團第2營E連退役軍官（1918年出生）
  - [2014年](../Page/2014年.md "wikilink")：[李泰祥](../Page/李泰祥.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[音樂家](../Page/音樂家.md "wikilink")\[1\]（1941年出生）
  - [2015年](../Page/2015年.md "wikilink")：[林保全](../Page/林保全.md "wikilink")，[香港資深](../Page/香港.md "wikilink")[男](../Page/男.md "wikilink")[配音員](../Page/配音員.md "wikilink")（1951年出生）

## 节假日和习俗

  - ：[婦女掌權日](../Page/婦女掌權日.md "wikilink")。

## 參考資料

1.