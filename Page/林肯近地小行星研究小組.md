[Neo-chart.png](https://zh.wikipedia.org/wiki/File:Neo-chart.png "fig:Neo-chart.png")

**林肯近地小行星研究小組**（）
計畫是由[美國空軍](../Page/美國空軍.md "wikilink")、[美國太空總署及](../Page/美國太空總署.md "wikilink")[麻省理工大學的](../Page/麻省理工大學.md "wikilink")[林肯實驗室所組成](../Page/林肯實驗室.md "wikilink")，而其簡稱多譯為「**麗妮兒**或**林尼爾**」。該小組成立於1998年，其目的是尋找及記錄對地球存在威脅的[近地小行星](../Page/近地小行星.md "wikilink")。從1998年起，很負責的檢測出大部份的小行星，直到被[卡特林那巡天系統超越](../Page/卡特林那巡天系統.md "wikilink")。迄2007年12月31日，LINEAR已經檢測到226,193顆新天體，其中包括2019顆[近地小行星和](../Page/近地小行星.md "wikilink")236顆[彗星](../Page/彗星.md "wikilink")\[1\]。LINEAR所有的發現都是使用[機器人望遠鏡](../Page/機器人望遠鏡.md "wikilink")。

最初的測試場所可以回溯到1972年，而在1980年代初期，原型建設完成，林肯實驗室的實驗測試系統：ETS（新墨西哥州，MPC天文台代碼704）。1996年，LINEAR計畫開始運作一個[近地天體](../Page/近地天體.md "wikilink")（NEO）
的發現裝置，使用1米口徑的[地基光電深空監控](../Page/地基光電深空監控.md "wikilink")（）[望遠鏡](../Page/望遠鏡.md "wikilink")。這種廣角的光學望遠鏡是空軍設計來觀察地球軌道上的太空船。LINEAR計畫使用的GEODSS是林肯實驗室實驗測試網站位於[新墨西哥州](../Page/新墨西哥州.md "wikilink")[索柯洛](../Page/索柯洛.md "wikilink")[白砂導彈靶場的儀器](../Page/白砂導彈靶場.md "wikilink")，然後資料送至位於[麻塞諸塞州](../Page/麻塞諸塞州.md "wikilink")[列星頓](../Page/列星頓.md "wikilink")[漢斯科姆空軍基地的林肯實驗室](../Page/漢斯科姆空軍基地.md "wikilink")。

在1997年3月至7月，一個1024 × 1024
[像素的](../Page/像素.md "wikilink")[電荷耦合元件](../Page/電荷耦合元件.md "wikilink")（CCD）檢測器進行視野測試，而這個探測器的[視野僅約望遠鏡視野的五分之一](../Page/視野.md "wikilink")，就發現了4顆近地天體。在1997年10月，一個由1960
X
2560像素構成的CCD，完整的涵蓋了望遠鏡2[平方度的視野](../Page/平方度.md "wikilink")，在使用中共成功的發現9顆新的近地天體。從1997年11月至1998年1月的，在這兩個大型和小型的CCD檢測器的使用期間，又增加了5顆近地天體\[2\]。

從1999年10月開始，第2架1米望遠鏡也加入搜尋的工作\[3\]。在2002年，第3架[口徑](../Page/口徑.md "wikilink")0.5米的望遠鏡被加入線上以提供這兩架1米望遠鏡發現天體的後續追蹤。目前，LINEAR望遠鏡每天晚上沿著[黃道觀察預測中最可能有近地天體進入的區域五次](../Page/黃道.md "wikilink")，以搜尋這些區域內的近地天體。CCD的靈敏度，和相對快速的資料輸出，使LINEAR每個夜晚的檢測都可以覆蓋大部份的天空。目前，LINEAR計畫仍然負責近地天體的主要發現。

這個計畫的首席研究員是[格蘭特·斯托克](../Page/格蘭特·斯托克.md "wikilink")，共同研究員包括[珍妮佛·埃文斯和](../Page/珍妮佛·埃文斯.md "wikilink")[埃里克·皮爾斯](../Page/埃里克·皮爾斯_\(天文學家\).md "wikilink")。

除了發現數以萬計的小行星（迄2007年12月31日為225,957顆小行星）\[4\]，LINEAR也發現、共同發現或再發現一些[週期彗星](../Page/週期彗星.md "wikilink")，包括：[11P/坦普爾-斯威夫特-林尼爾彗星](../Page/坦普爾-斯威夫特-林尼爾彗星.md "wikilink")、[146P/Shoemaker-LINEAR](../Page/146P/Shoemaker-LINEAR.md "wikilink")、[148P/Anderson-LINEAR](../Page/148P/Anderson-LINEAR.md "wikilink")、[156P/Russell-LINEAR](../Page/156P/Russell-LINEAR.md "wikilink")、[158P/Kowal-LINEAR](../Page/158P/Kowal-LINEAR.md "wikilink")、[160P/林尼爾](../Page/LINEAR彗星_\(160P\).md "wikilink")（LINEAR
43）、[165P/林尼爾](../Page/LINEAR彗星_\(165P\).md "wikilink")（LINEAR
10）、和[176P/LINEAR](../Page/176P/LINEAR.md "wikilink")（LINEAR
52，118401 LINEAR：在分類上暨是彗星也是小行星的5顆天體之一）。

## 相關條目

  -
  -
  - [2004 FH](../Page/2004_FH.md "wikilink")

  - [以LINEAR為名的彗星列表](../Page/Comet_LINEAR.md "wikilink")

  - [Planetary Data
    System](../Page/Planetary_Data_System.md "wikilink")（PDS）

  - [小行星中心](../Page/小行星中心.md "wikilink")

  - [近地小行星追蹤](../Page/近地小行星追蹤.md "wikilink")（NEAT）

  - [羅威爾天文台近地天體搜尋](../Page/羅威爾天文台近地小行星搜尋計畫.md "wikilink")（LONEOS）

  - [泛星計畫](../Page/泛星計畫.md "wikilink")

  - [太空警衛](../Page/太空警衛.md "wikilink")

  - [太空觀測](../Page/太空觀測.md "wikilink")

## 參考資料

  - [Taff, L. G.](../Page/Laurence_G._Taff.md "wikilink");
    *\[<http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?1981PASP>...93..658T
    A new asteroid observation and search technique\]*, Publications of
    the Astronomical Society of the Pacific (PASP), Vol. 93 (Oct.-Nov.
    1981), pp. 658-660

## 外部連結

  - [MIT Lincoln Laboratory:
    LINEAR](https://web.archive.org/web/20080517053406/http://www.ll.mit.edu/linear)
  - [NEO discovery statistics](http://neo.jpl.nasa.gov/stats/) from JPL.
    Shows the number of asteroids of various types (potentially
    hazardous, size \>1 km, etc.) that different programs have
    discovered, by year.

[Category:美國公設研究機構](../Category/美國公設研究機構.md "wikilink")
[Category:天文组织和机构](../Category/天文组织和机构.md "wikilink")
[Category:彗星發現者](../Category/彗星發現者.md "wikilink")
[Category:巡天項目](../Category/巡天項目.md "wikilink")
[Category:小行星巡天](../Category/小行星巡天.md "wikilink")
[Category:近地天體追蹤](../Category/近地天體追蹤.md "wikilink")

1.  [LINEAR Observations, Detections, and New
    Discoveries](http://www.ll.mit.edu/mission/space/linear/)

2.  <http://adsabs.harvard.edu/abs/1978STIN>...7826044R

3.  [LINCOLN NEAR-EARTH ASTEROID RESEARCH
    (LINEAR)](http://neo.jpl.nasa.gov/missions/linear.html)

4.