[Amiga1k.jpg](https://zh.wikipedia.org/wiki/File:Amiga1k.jpg "fig:Amiga1k.jpg")（1985年）\]\]
[Amiga500_system.jpg](https://zh.wikipedia.org/wiki/File:Amiga500_system.jpg "fig:Amiga500_system.jpg")是當時最受歡迎的Amiga電腦型號。\]\]
[Amiga4000_Ramsey.jpg](https://zh.wikipedia.org/wiki/File:Amiga4000_Ramsey.jpg "fig:Amiga4000_Ramsey.jpg")

**Amiga**（非正式譯名為**阿米加**）是[Amiga公司開發的](../Page/Amiga公司.md "wikilink")[個人電腦產品系列](../Page/個人電腦.md "wikilink")。在1982年，主要的硬件設計師[杰·邁納開始了Amiga的開發工作](../Page/杰·邁納.md "wikilink")。[Commodore
International收購了](../Page/Commodore_International.md "wikilink")[Amiga公司後](../Page/Amiga公司.md "wikilink")，在1985年將Amiga引入到市場裡。

「Amiga」這個名詞在[西班牙語裡意為女性](../Page/西班牙語.md "wikilink")[朋友](../Page/朋友.md "wikilink")，其意為"User
Friendy"(對用戶友好的)\[1\]

Amiga系列電腦一共售出了約485萬部，多數售往歐洲國家，在英國和德國銷量最高。\[2\]美國銷量則有70萬部左右，*多數售給了商業用戶。*

## 產品時間線

| Amiga 產品時間線 |
| ----------- |
|             |
| 3           |
| **品牌擁有方**   |
|             |
| **晶片組**     |
|             |
|             |
|             |
| **桌面級產品**   |
|             |
| **低階產品**    |
|             |
| **電子遊戲系統**  |

\!--

## 參見

  - [AmigaOS](../Page/AmigaOS.md "wikilink")
  - [Amiga公司](../Page/Amiga公司.md "wikilink")

## 參考資料

## 外部連結

  - [Amiga, Inc.](http://www.amiga.com/)

[Category:个人电脑](../Category/个人电脑.md "wikilink")

1.
2.