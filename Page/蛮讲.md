**蛮讲**，又称**蛮话**或**蛮讲话**，是[中国](../Page/中国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[溫州市南部和东南部一带的一种方言](../Page/溫州市.md "wikilink")，分为两支：一支为泰顺蛮讲，使用者居住在[泰顺县](../Page/泰顺县.md "wikilink")；另一支为苍南蛮话，使用者居住在[苍南县东北部沿海一带](../Page/苍南县.md "wikilink")。

## 概要

泰顺蛮讲是[泰顺縣的原住土语](../Page/泰顺縣.md "wikilink")，百越语与[汉语的融合体](../Page/汉语.md "wikilink")，语式多样，辞彩丰富。泰顺隶属于[浙江省](../Page/浙江省.md "wikilink")[温州市](../Page/温州市.md "wikilink")，上古时期居住着古东越人，明景泰三年分疆立县，除西北部分地区与东端一小片以外，大部分地区通用蛮讲语，以蛮讲为[母语的人口约有二十余万人](../Page/母语.md "wikilink")。蛮讲先民信奉[闾山教](../Page/闾山教.md "wikilink")，族群神祇为「佛姨妮」（[顺天圣母陈十四娘娘](../Page/陳靖姑.md "wikilink")），蛮讲人居住的地方称为东山越。蛮讲为“**闽讲**”的谐音。作为境内的代表方言，蛮讲不断地影响着当地其它方言，同时也不断地受周围方言的影响，逐渐形成自己的方言特色。

苍南蛮话主要分布在[苍南县的东部沿海地区](../Page/苍南县.md "wikilink")，通行蛮话的乡镇有：钱库、夏口、陈东、芦浦、仙居、项桥、新城、炎亭、白沙、海城等。蛮讲分布在[泰顺县的中部](../Page/泰顺县.md "wikilink")、南部和西部地区，主要乡镇有：泗溪、凤垟、三魁、下洪、仕阳、东溪、雅阳、柳峰、筱村、岭北、南山等。

## 苍南蛮话与泰顺蛮讲的差异

蛮话和蛮讲的主要区别在于蛮话受瓯语（[温州话](../Page/温州话.md "wikilink")）影响较大，特别是语音系统基本上向瓯语靠拢，如：“东、通、同”三个字[声母不同](../Page/声母.md "wikilink")：“东”的声母不带音不送气；“通”的声母不带音送气，“同”的声母带音不送气，具有一般瓯语的特点，因此人误以为蛮话应同瓯语一样。至於蛮讲則直接和[福建](../Page/福建.md "wikilink")[寿宁话连成一片](../Page/寿宁话.md "wikilink")，属于[闽东话系统](../Page/闽东话.md "wikilink")，保留着古闽越语的许多特点。至于蛮话究竟属于闽语还是吴语尚无定论。蛮话和蛮讲虽有相同之处，如：“有”的主要元音都是u，“没有”都说“无”，“吃”都说“食”，“打”都说“拍”，“猪、桌、抽、丑、虫、迟、除、场、沉、潮、郑”等字的声母都与闽语相同或相近，但相互难以通话。男孩，蛮话和蛮讲都说“丈夫囝”；女孩，蛮话说“作母囝”，蛮讲说“做母囝”，与闽语相同或相似。据统计，泰顺讲蛮講的有20万，苍南讲蛮话的有25.4万。

## 归属的争议

蛮讲与[闽语各话为同源关系](../Page/闽语.md "wikilink")，但却与[闽东语的](../Page/闽东语.md "wikilink")[福宁片](../Page/福宁片.md "wikilink")（北片）沟通困难；另外一方面，蛮讲又受到了[吴语](../Page/吴语.md "wikilink")[温州话很深的影响](../Page/温州话.md "wikilink")。因此，语言学界对于蛮讲的归属存在着很大的争议。中国的语言学家或认为归属于闽东语，或认为归属于吴语，没有定论。《[中国语言地图集](../Page/中国语言地图集.md "wikilink")》将蛮话（蛮讲）归属于闽东语，和[侯官片](../Page/侯官片.md "wikilink")、福宁片为并列关系的片区；[ISO
639-6则将蛮话](../Page/ISO_639-6.md "wikilink")（蛮讲）当作闽语之下的一个分支，与闽东语是并列关系的语言。

## 蠻話和其他語言比較

<table>
<tbody>
<tr class="odd">
<td></td>
<td><center>
<p><a href="../Page/普通话.md" title="wikilink">普通话</a></p>
</center></td>
<td><center>
<p><a href="../Page/甌語.md" title="wikilink">甌語</a></p>
</center></td>
<td><center>
<p><a href="../Page/蠻話.md" title="wikilink">蠻話</a></p>
</center></td>
<td><center>
<p><a href="../Page/閩南語.md" title="wikilink">閩南語</a></p>
</center></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/儿媳.md" title="wikilink">儿媳</a></p>
</center></td>
<td><center>
<p>er xi</p>
</center></td>
<td><center>
<p>san ngu(吴拼:sang1 nyu2)</p>
</center></td>
<td><center>
<p>xin bu</p>
</center></td>
<td><center>
<p>sim bu</p>
</center></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/姐姐.md" title="wikilink">姐姐</a></p>
</center></td>
<td><center>
<p>jie jie</p>
</center></td>
<td><center>
<p>do/zi/za(吴拼:doo3;ci2;ca1)</p>
</center></td>
<td><center>
<p>zi/ji</p>
</center></td>
<td><center>
<p>zi/ze</p>
</center></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/飯.md" title="wikilink">飯</a></p>
</center></td>
<td><center>
<p>fan</p>
</center></td>
<td><center>
<p>vo(吴拼:voo3)</p>
</center></td>
<td><center>
<p>me/bo</p>
</center></td>
<td><center>
<p>mai/bng</p>
</center></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/打架.md" title="wikilink">打架</a></p>
</center></td>
<td><center>
<p>da jia</p>
</center></td>
<td><center>
<p>xie dia(吴拼:shie1 ta2)</p>
</center></td>
<td><center>
<p>xio pa</p>
</center></td>
<td><center>
<p>sio pha /uan ke</p>
</center></td>
</tr>
</tbody>
</table>

## 外部链接

  - [东山越蛮讲网](https://web.archive.org/web/20041016004145/http://www.mangomo.com/)
  - [蛮话方言网](https://web.archive.org/web/20141219050954/http://cnmanhua.net/index.html)

[Category:浙江語言](../Category/浙江語言.md "wikilink")
[Category:温州方言](../Category/温州方言.md "wikilink")
[Category:閩東語](../Category/閩東語.md "wikilink")