**鄒族**（[鄒語](../Page/鄒語.md "wikilink")：，）舊稱**曹族**，**朱欧**，傳統語言為[鄒語](../Page/鄒語.md "wikilink")，在邵族被承認以前曾是台灣山區原住民人口僅次於[賽夏族第二少的族群](../Page/賽夏族.md "wikilink")，唯以往歸於「南鄒」的[卡那卡那富族和](../Page/卡那卡那富族.md "wikilink")[拉阿魯哇族成為獨立族群後](../Page/拉阿魯哇族.md "wikilink")，鄒族人口已於2019年1月被[賽夏族人口超過](../Page/賽夏族.md "wikilink")。為[台灣](../Page/台灣.md "wikilink")[原住民族的一支](../Page/台灣原住民.md "wikilink")，主要居住於[嘉義縣](../Page/嘉義縣.md "wikilink")[阿里山鄉](../Page/阿里山鄉.md "wikilink")[樂野村](../Page/樂野村.md "wikilink")、[特富野村](../Page/特富野村.md "wikilink")、[達邦村](../Page/達邦村.md "wikilink")、[來吉](../Page/來吉.md "wikilink")、[山美等地區](../Page/山美.md "wikilink")，亦分布於[南投縣](../Page/南投縣.md "wikilink")[信義鄉](../Page/信義鄉_\(台灣\).md "wikilink")。

## 分布

[Tsou_youth_of_Taiwan_(pre-1945).jpg](https://zh.wikipedia.org/wiki/File:Tsou_youth_of_Taiwan_\(pre-1945\).jpg "fig:Tsou_youth_of_Taiwan_(pre-1945).jpg")
**鄒族**又稱**曹族**，[鳥居龍藏之分類稱其為](../Page/鳥居龍藏.md "wikilink")**新高族**。鄒族有「特富野」、「達邦」、「伊姆諸」與「魯富都」社，而四社中的伊姆諸社與魯富都社都於廿世紀初因惡疾、部落首長絕嗣而廢社。目前有八個村落，[來吉](../Page/來吉村_\(台灣\).md "wikilink")、樂野附屬於**特富野社**。[新美](../Page/新美.md "wikilink")、[茶山](../Page/茶山.md "wikilink")、[山美](../Page/山美.md "wikilink")、[里佳附屬於](../Page/里佳.md "wikilink")**達邦社**。

主要居住在[高雄市的](../Page/高雄市.md "wikilink")[卡那卡那富族和](../Page/卡那卡那富族.md "wikilink")[拉阿魯哇族過去被視為鄒族的分支](../Page/拉阿魯哇族.md "wikilink")，合稱「南鄒」，2014年成為法定的獨立族群。

## 社會組織

鄒族人的觀念裡擁有會所、能夠舉行全部落性祭儀的社群才有資格稱為**大社**，也才是一個完整的政治實體。其社會組織可以分為以下幾個部份：

1.  大社（hosa）：由幾個氏族聯合組成。旗下包含數個小社（denohiyu）。
2.  氏族（aemana）：由幾個聯合家族組成。可能有血緣關係，也可能沒有血緣關係，同一氏族之間禁止通婚。
3.  聯合家族（ongo-no-emo）：由幾個單一姓氏的家族組成，共有耕地、共有河流漁區、共有小米祭祀小屋（又稱粟祭屋）。　

鄒族並沒有階級制度，卻有幾個特殊地位的人物：

  - 頭目：由其一家族固定承襲。
  - 征帥：爭戰、獵首的指揮官，有時可能是頭目。如果戰事頻繁，會有好幾個征帥。
  - 勇士：在戰場上有特殊功勳的族人。

鄒族的集會場所稱為「Kuba（庫巴）」，但僅該部落之男性可以進入，非該部落者及女性皆不得進入。
拉阿魯哇族的男子聚會所稱為「Tapulailia」。

## 祭儀

鄒族部落中最重要的祭儀為「播種祭」、「homeyaya
（小米收穫祭）」與「[mayasvi](../Page/mayasvi.md "wikilink")
（瑪雅士比、戰祭）」三大祭儀。鄒族一年一度的mayasvi
（瑪雅士比），目前分由達邦及特富野分別舉行祭典儀式，mayasvi又譯為戰祭。\[1\]

## 鄒族名人

參見「[台灣原住民人物列表](../Page/台灣原住民人物列表.md "wikilink")」

[臺灣原住民鄒族領袖高一生(吾雍．雅達烏猶卡那)與湯守仁(湯川一丸)_Indigenous_Taiwanese_Leaders_Kao_Yi-shen_and_Tang_Shou-jen_who_Killed_by_Brutal_Regime_from_China_in_1954.jpg](https://zh.wikipedia.org/wiki/File:臺灣原住民鄒族領袖高一生\(吾雍．雅達烏猶卡那\)與湯守仁\(湯川一丸\)_Indigenous_Taiwanese_Leaders_Kao_Yi-shen_and_Tang_Shou-jen_who_Killed_by_Brutal_Regime_from_China_in_1954.jpg "fig:臺灣原住民鄒族領袖高一生(吾雍．雅達烏猶卡那)與湯守仁(湯川一丸)_Indigenous_Taiwanese_Leaders_Kao_Yi-shen_and_Tang_Shou-jen_who_Killed_by_Brutal_Regime_from_China_in_1954.jpg")（右）與湯守仁（左），均於1954年白色恐怖遭政府槍決\]\]

  - 政治
      - [高一生](../Page/高一生.md "wikilink")（吾雍．雅達烏猶卡那、矢多一生）(**Uyongu
        Yata'uyungana**)：日治時期警察、[蕃童教育所教師](../Page/蕃童教育所.md "wikilink")、首任[吳鳳鄉](../Page/阿里山鄉.md "wikilink")（今阿里山鄉）鄉長、音樂家，為阿里山鄒族經濟發展、教育、傳統文化保存與現代化重要推手，領導參與[二二八事件武裝抵抗](../Page/二二八事件.md "wikilink")，1954年遭政府槍決。歌手[高慧君之祖父](../Page/高慧君.md "wikilink")。
      - [湯守仁](../Page/湯守仁.md "wikilink")（湯川一丸）(**Yapasuyongʉ
        Yulunana／雅巴斯勇．優路拿納**)：[關東軍少尉](../Page/關東軍.md "wikilink")，中華民國少校，領導參與[二二八事件武裝抵抗](../Page/二二八事件.md "wikilink")，於1954年遭槍決，[嘉義縣議員](../Page/嘉義縣.md "wikilink")[湯進賢之父](../Page/湯進賢.md "wikilink")。
      - [杜孝生](../Page/杜孝生.md "wikilink")(Voyu Tosku 博尤
        特士庫)：[台北帝國大學](../Page/台北帝國大學.md "wikilink")[附屬醫學專門部](../Page/附屬醫學專門部.md "wikilink")([台灣總督府醫學校](../Page/台灣總督府醫學校.md "wikilink"))專班畢業，為鄒族第一位西醫，[嘉義縣第一屆議員](../Page/嘉義縣.md "wikilink")，台灣[白色恐怖受難者之一](../Page/白色恐怖.md "wikilink")，歌手[杜銘哲之父](../Page/杜銘哲.md "wikilink")。
      - [浦忠成](../Page/浦忠成.md "wikilink")（Pasuya -{’}-e Poiconʉ,
        巴蘇亞·博伊哲努）：現任考試院第十一屆考試委員（2008年7月迄今）。曾任行政院原住民族委員會副主任委員、台灣史前文化博物館館長。
      - 汪明輝 (Tibusungʉ -{’}-e Vayayana)：鄒族文教基金會董事長，台師大地理系副教授,
        行政院原住民族委員會副主任委員。
  - 運動
      - [石志偉](../Page/石志偉.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[職棒](../Page/中華職棒.md "wikilink")[La
        New熊隊三壘手](../Page/La_New熊隊.md "wikilink")，著名[棒球國手](../Page/棒球.md "wikilink")。
      - [汪竣泰](../Page/汪竣泰.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[職棒](../Page/中華職棒.md "wikilink")[兄弟象隊捕手](../Page/兄弟象隊.md "wikilink")。
      - [藍少文](../Page/藍少文.md "wikilink")：[台灣中華職棒選手](../Page/台灣.md "wikilink")。
      - [藍少白](../Page/藍少白.md "wikilink")：中華職棒選手，鄒族、[布農族混血](../Page/布農族.md "wikilink")
      - [方克偉](../Page/方克偉.md "wikilink")：中華職棒[富邦悍將隊捕手](../Page/富邦悍將.md "wikilink")。
      - [鄭思嘉](../Page/鄭思嘉.md "wikilink")：[台灣棒球選手](../Page/台灣.md "wikilink")。
      - [鄭衛青](../Page/鄭衛青.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[台灣大聯盟嘉南勇士隊](../Page/台灣大聯盟嘉南勇士隊.md "wikilink")（1997年～2000年）。
      - [朱麥可](../Page/朱麥可.md "wikilink")：[第一屆IBAF世界少棒錦標賽中華少棒代表隊國手](../Page/第一屆IBAF世界少棒錦標賽.md "wikilink")。
      - [甘林依靜](../Page/甘林依靜.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[中華女子拔河隊](../Page/中華女子拔河隊.md "wikilink")。
      - [杜凱文](../Page/杜凱文.md "wikilink")：2012年入選為倫敦奧運柔道男子66公斤級的中華代表隊，也是台灣柔道史上第一位參加奧運的原住民籍選手。
      - [汪忠義](../Page/汪忠義.md "wikilink")：1984年參加台中縣東勢國中軟網隊赴日參加世界盃軟式網球錦標賽榮獲團體冠軍、個人銅牌，台灣史上第一位參加世界盃軟式網球賽的的原住民籍選手。

<!-- end list -->

  - 演藝娛樂
      - [湯蘭花](../Page/湯蘭花.md "wikilink")：（**Yurunana
        Daniiv／優路那那．丹妮芙**）：知名[藝人](../Page/藝人.md "wikilink")。
      - [杜銘哲](../Page/杜銘哲.md "wikilink")
      - [田麗](../Page/田麗.md "wikilink")：知名藝人。
      - [高慧君](../Page/高慧君.md "wikilink")：（**Paicʉ
        Yatauyungana／白芷．雅達烏尤安娜**）：知名藝人。
      - [高蕾雅](../Page/高蕾雅.md "wikilink")：（**Yinguyu
        Yatauyungana**）：知名藝人，高慧君之妹。
      - [安歆澐](../Page/安欽雲.md "wikilink")：凱渥模特兒
      - [鄭為之](../Page/鄭為之.md "wikilink")
      - 獵人與百合：[莊立德](../Page/莊立德.md "wikilink")、[鄭薇婷](../Page/鄭薇婷.md "wikilink")。
      - 原始林樂團：4人組
      - 高山阿嬤：2人組
      - [莎莎](../Page/吳茹伶.md "wikilink") :
        前黑澀會美眉、其母親是[湯蘭花之表妹](../Page/湯蘭花.md "wikilink")。
      - Abu'u（溫雅云）：[lamigirls](../Page/lamigirls.md "wikilink")
        成員之一，[方克偉](../Page/方克偉.md "wikilink") 表妹；安歆澐姪女。

<!-- end list -->

  - 藝術家
      - [白紫·迪雅奇安娜](../Page/白紫·迪雅奇安娜.md "wikilink")
      - [白茲‧牟固那那](../Page/白茲‧牟固那那.md "wikilink")：漢名是劉武香梅，作家。

<!-- end list -->

  - 其他
      - [湯英伸](../Page/湯英伸.md "wikilink")：台灣最年輕死刑犯(19歲)。

## 統計

| 縣市  | 鄒族人口  | 總人口        | 比率    |
| --- | ----- | ---------- | ----- |
| 嘉義縣 | 3,542 | 552,749    | 0.64% |
| 高雄縣 | 775   | 1,262,969  | 0.06% |
| 嘉義市 | 397   | 266,126    | 0.15% |
| 南投縣 | 236   | 487,398    | 0.05% |
| 臺北縣 | 218   | 3,722,082  | 0.01% |
| 桃園縣 | 179   | 1,808,833  | 0.01% |
| 臺北市 | 101   | 2,624,257  | 0.00% |
| 高雄市 | 96    | 1,493,806  | 0.01% |
| 屏東縣 | 87    | 872,902    | 0.01% |
| 臺中縣 | 82    | 1,510,480  | 0.01% |
| 臺南縣 | 74    | 1,120,394  | 0.01% |
| 花蓮縣 | 66    | 327,064    | 0.02% |
| 臺中市 | 60    | 989,047    | 0.01% |
| 彰化縣 | 53    | 1,255,332  | 0.00% |
| 宜蘭縣 | 39    | 444,950    | 0.01% |
| 臺東縣 | 30    | 204,919    | 0.01% |
| 基隆市 | 27    | 387,504    | 0.01% |
| 新竹縣 | 24    | 451,316    | 0.01% |
| 臺南市 | 23    | 725,985    | 0.00% |
| 苗栗縣 | 15    | 534,366    | 0.00% |
| 雲林縣 | 14    | 705,440    | 0.00% |
| 新竹市 | 13    | 395,746    | 0.00% |
| 澎湖縣 | 8     | 83,214     | 0.01% |
| 金門縣 | 7     | 56,275     | 0.01% |
| 連江縣 | 3     | 17,775     | 0.02% |
| 總計  | 6,169 | 22,300,929 | 0.03% |
|     |       |            |       |

縣市別鄒族人口 (2000年)\[2\]

| 鄉鎮市區    | 鄒族人口  | 總人口   | 比率     |
| ------- | ----- | ----- | ------ |
| 嘉義縣阿里山鄉 | 3,206 | 5,729 | 55.96% |
| 高雄市那瑪夏區 | 445   | 2,998 | 14.84% |
|         |       |       |        |

鄉鎮市區別鄒族人口 (2000年)\[3\]

## 其他

## 註釋

## 參考資料

  - [阿里山國家風景區管理處](../Page/阿里山國家風景區.md "wikilink")

## 參見

  - [斯瓦迪士核心詞列表](../Page/斯瓦迪士核心詞列表.md "wikilink")
  - [原住民族語能力認證](../Page/原住民族語能力認證.md "wikilink")
  - [原住民族語言書寫系統 (中華民國)](../Page/原住民族語言書寫系統_\(中華民國\).md "wikilink")
  - [族語新聞](../Page/族語新聞.md "wikilink")
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")
  - [原住民部落社區大學](../Page/原住民部落社區大學.md "wikilink")
  - [臺灣原住民命名文化](../Page/臺灣原住民命名文化.md "wikilink")
  - [原始人類語言](../Page/原始人類語言.md "wikilink")
  - [小行星175586](../Page/小行星175586.md "wikilink")
  - [吳鳳](../Page/吳鳳.md "wikilink")
  - [湯英伸事件](../Page/湯英伸事件.md "wikilink")

## 外部連結

  - [中央研究院南島語數位典藏](https://web.archive.org/web/20070115071850/http://formosan.sinica.edu.tw/)
  - [中央研究院語言學研究所](https://web.archive.org/web/20070205024307/http://www.ling.sinica.edu.tw/index-2.html)
  - [鄒族原住民文化網](http://www.ims.org.tw/template/aboriginal_c28.php)
  - [鄒族氏族關係](https://www.evernote.com/shard/s6/sh/64645394-c6b0-4a9d-9215-0cefc68bcb2e/fc43798113adee1487a81523732af6fd)

[Tsou](../Category/臺灣原住民族.md "wikilink")
[鄒族](../Category/鄒族.md "wikilink")

1.

2.

3.