**MRTG**（Multi Router Traffic
Grapher）是一套可用來繪出網路流量圖的[軟體](../Page/軟體.md "wikilink")，由[瑞士](../Page/瑞士.md "wikilink")[奧爾滕的Tobias](../Page/奧爾滕.md "wikilink")
Oetiker與Dave Rand所開發，此軟體以[GPL授權](../Page/GPL.md "wikilink")。

## 簡介

MRTG最早的版本是在1995年[春天所推出](../Page/春天.md "wikilink")，以[Perl所寫成](../Page/Perl.md "wikilink")，因此可以跨平台使用，它利用了[SNMP送出帶有物件識別碼](../Page/SNMP.md "wikilink")（OIDs）的請求給要查詢的網路設備，因此設備本身需支援SNMP。MRTG再以所收集到的資料產生[HTML檔案並以](../Page/HTML.md "wikilink")[GIF或](../Page/GIF.md "wikilink")[PNG格式繪製出圖形](../Page/PNG.md "wikilink")，並可以日、週、月等單位分別繪出。它也可產生出最大值最小值的資料供統計用。

原本MRTG只能繪出網路設備的流量圖，後來發展出了各種[plug-in](../Page/plug-in.md "wikilink")，因此網路以外的設備也可由MRTG監控，例如[伺服器的](../Page/伺服器.md "wikilink")[硬碟使用量](../Page/硬碟.md "wikilink")、[CPU的負載等](../Page/CPU.md "wikilink")。

## 類似程式

  - [RRDtool](../Page/RRDtool.md "wikilink")：MRTG原作者開發的流量記錄監控軟體，較MRTG強大
  - [PRTG](../Page/PRTG.md "wikilink")：[Windows下的流量監控軟體](../Page/Windows.md "wikilink")

## 參考資料

  - <https://web.archive.org/web/20071024193630/http://www.hmes.kh.edu.tw/~jang/mrtg-install.htm>
  - <http://oss.oetiker.ch/mrtg/doc/mrtg.en.html>

## 外部連結

  - [MRTG Home page](http://oss.oetiker.ch/mrtg/) 軟體官方網站

[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:网络软件](../Category/网络软件.md "wikilink")