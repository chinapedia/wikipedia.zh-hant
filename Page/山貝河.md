[Shan_Pui_River_Yuen_Long_Town_Centre_Section.jpg](https://zh.wikipedia.org/wiki/File:Shan_Pui_River_Yuen_Long_Town_Centre_Section.jpg "fig:Shan_Pui_River_Yuen_Long_Town_Centre_Section.jpg")
[Shan_Pui_River_Yuen_Long_Industrial_Estate_Section_2006.jpg](https://zh.wikipedia.org/wiki/File:Shan_Pui_River_Yuen_Long_Industrial_Estate_Section_2006.jpg "fig:Shan_Pui_River_Yuen_Long_Industrial_Estate_Section_2006.jpg")
**山貝河**（），又名**山背河**、**元朗河**或**元朗瀝**，是[香港的一條](../Page/香港.md "wikilink")[河流](../Page/河流.md "wikilink")，位於[新界的](../Page/新界.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")。河流的源頭由[大棠的](../Page/大棠.md "wikilink")[九逕山山頭開始](../Page/九逕山.md "wikilink")，海拔約380米，向北流往[元朗市中心](../Page/元朗市中心.md "wikilink")、[元朗工業園](../Page/元朗工業園.md "wikilink")、[米埔](../Page/米埔.md "wikilink")、[橫洲及](../Page/橫洲_\(元朗\).md "wikilink")[甩洲等地](../Page/甩洲.md "wikilink")，與[錦田河匯合流進](../Page/錦田河.md "wikilink")[后海灣](../Page/后海灣.md "wikilink")。山貝河幹流水平長度約14.3公里，流域面積約為29.2平方公里。在元朗市中心一段的人工河道，也俗稱為**元朗大坑渠**。

## 特色

山貝河是香港少有的天然寬闊河流，同時亦是香港少數能形成[泛濫平原的河流](../Page/泛濫平原.md "wikilink")。數百年來不少[新界原居民在山貝河沿岸進行耕作](../Page/新界原居民.md "wikilink")，包括[稻米](../Page/稻米.md "wikilink")、[蔬菜等](../Page/蔬菜.md "wikilink")，並飼養像[豬](../Page/豬.md "wikilink")、[牛和一些家禽](../Page/牛.md "wikilink")。在現時元朗市中心位置一帶亦發展成市集，乃昔日鄉民前來把農作物進行交易的地方。

山貝河流經[米埔附近](../Page/米埔.md "wikilink")，是香港境內一片具價值的[沼澤](../Page/沼澤.md "wikilink")、[濕地地區](../Page/濕地.md "wikilink")，生長着茂密的[蘆葦叢及](../Page/蘆葦.md "wikilink")[紅樹林](../Page/紅樹林.md "wikilink")，吸引了不少[候鳥在此逗留](../Page/候鳥.md "wikilink")。所以，山貝河流域近[南生圍一帶亦成為](../Page/南生圍.md "wikilink")[攝影愛好者和觀鳥人士的一個熱門地點](../Page/攝影.md "wikilink")。此外，該處亦是玩[遙控飛機和航拍機的熱點之一](../Page/遙控飛機.md "wikilink")。而山貝河的[橫水渡](../Page/橫水渡.md "wikilink")，是香港僅存的橫水渡，吸引不少遊客慕名而來。

然而，由於山貝河也流經[元朗工業邨](../Page/元朗工業邨.md "wikilink")，所以也受到[工業廢料的](../Page/工業.md "wikilink")[污染](../Page/污染.md "wikilink")，

山貝河於2003年11月曾發現一條雌性[灣鱷](../Page/鱷魚.md "wikilink")，吸引了不少市民慕名而來觀看，傳媒也對此進行廣泛報導，至2004年6月才擒獲該鱷魚。及後，[漁農自然護理署和](../Page/漁農自然護理署.md "wikilink")[香港電台合辦小鱷魚命名比賽](../Page/香港電台.md "wikilink")，8月12日定名為「[貝貝](../Page/鱷魚貝貝.md "wikilink")」，飼養於[香港濕地公園](../Page/香港濕地公園.md "wikilink")。

## 資料來源

  - 《我叫貝貝小灣鱷》，載香港《明報》，2004年8月13日

  -
[Category:元朗區](../Category/元朗區.md "wikilink")
[Category:元朗市中心](../Category/元朗市中心.md "wikilink")
[Category:十八鄉](../Category/十八鄉.md "wikilink")
[Category:香港河流](../Category/香港河流.md "wikilink")