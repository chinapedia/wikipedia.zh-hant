下表是**微軟視窗作業系統應用程式設計介面函式**（APIs）列表：

## 應用程式設計介

### 現代

  - [.NET Framework](../Page/.NET_Framework.md "wikilink"),
    當中包括[Remoting](../Page/.NET_Remoting.md "wikilink"),
    [Assemblies](../Page/.NET_assembly.md "wikilink"),
    [Metadata](../Page/.NET_metadata.md "wikilink"), [Common Language
    Runtime](../Page/Common_Language_Runtime.md "wikilink"), [Common
    Type System](../Page/Common_Type_System.md "wikilink"), [Global
    Assembly Cache](../Page/Global_Assembly_Cache.md "wikilink"),
    [Microsoft Intermediate
    Language](../Page/Microsoft_Intermediate_Language.md "wikilink"),
    [Windows Forms](../Page/Windows_Forms.md "wikilink"), [Windows
    Communication
    Foundation](../Page/Windows_Communication_Foundation.md "wikilink"),
    [Windows Presentation
    Foundation](../Page/Windows_Presentation_Foundation.md "wikilink"),
    [Windows CardSpace](../Page/Windows_CardSpace.md "wikilink"),
    [Windows Workflow
    Foundation](../Page/Windows_Workflow_Foundation.md "wikilink"),
    [Windows PowerShell](../Page/Windows_PowerShell.md "wikilink")
  - [Active Scripting](../Page/Active_Scripting.md "wikilink")
  - [Collaboration Data
    Objects](../Page/Collaboration_Data_Objects.md "wikilink") (CDO)
  - [Component Object
    Model](../Page/Component_Object_Model.md "wikilink") (COM) and
    [Distributed Component Object
    Model](../Page/Distributed_Component_Object_Model.md "wikilink")
    (DCOM)
  - [Cryptographic Application Programming
    Interface](../Page/Cryptographic_Application_Programming_Interface.md "wikilink")
  - [DirectShow](../Page/DirectShow.md "wikilink")
  - [DirectX](../Page/DirectX.md "wikilink"),
    當中包括[Direct3D](../Page/Direct3D.md "wikilink"),
    [DirectDraw](../Page/DirectDraw.md "wikilink"),
    [DirectInput](../Page/DirectInput.md "wikilink"),
    [DirectMusic](../Page/DirectMusic.md "wikilink"),
    [DirectPlay](../Page/DirectPlay.md "wikilink"),
    [DirectSetup](../Page/DirectSetup.md "wikilink"), and
    [DirectSound](../Page/DirectSound.md "wikilink")
  - [Graphics Device
    Interface](../Page/Graphics_Device_Interface.md "wikilink") (GDI)
    and GDI+
  - [Microsoft Data Access
    Components](../Page/Microsoft_Data_Access_Components.md "wikilink")
    (MDAC), including: [ActiveX Data
    Objects](../Page/ActiveX_Data_Objects.md "wikilink") (ADO) and [OLE
    DB](../Page/OLE_DB.md "wikilink").
  - [Messaging Application Programming
    Interface](../Page/Messaging_Application_Programming_Interface.md "wikilink")
    (MAPI)
  - [Object linking and
    embedding](../Page/Object_linking_and_embedding.md "wikilink")
    (OLE), [OLE Automation](../Page/OLE_Automation.md "wikilink"),
    [ActiveX](../Page/ActiveX.md "wikilink")
  - [Remote Application Programming
    Interface](../Page/Remote_Application_Programming_Interface.md "wikilink")
    (RAPI)
  - [Speech Application Programming
    Interface](../Page/Speech_Application_Programming_Interface.md "wikilink")
    (SAPI)
  - [Telephony Application Programming
    Interface](../Page/Telephony_Application_Programming_Interface.md "wikilink")
    (TAPI)
  - [Uniscribe](../Page/Uniscribe.md "wikilink")
  - [Windows Image
    Acquisition](../Page/Windows_Image_Acquisition.md "wikilink") (WIA)
  - [Windows Management
    Instrumentation](../Page/Windows_Management_Instrumentation.md "wikilink")
    (WMI)
  - [Winsock](../Page/Winsock.md "wikilink")

### 拒絕

  - [Collaboration Data Objects for Windows NT
    Server](../Page/Collaboration_Data_Objects_for_Windows_NT_Server.md "wikilink")
  - [Dynamic Data Exchange](../Page/Dynamic_Data_Exchange.md "wikilink")
  - Older data access technologies: [Microsoft Jet Database
    Engine](../Page/Microsoft_Jet_Database_Engine.md "wikilink"), [Data
    Access Objects](../Page/Data_Access_Objects.md "wikilink") (DAO),
    [Remote Data Objects](../Page/Remote_Data_Objects.md "wikilink")
    (RDO), [Remote Data
    Services](../Page/Remote_Data_Services.md "wikilink") (RDS)
  - Setup API

## 应用程序接口 (APIs)

  - [Win16](../Page/Win16.md "wikilink")
  - [Win32s](../Page/Win32s.md "wikilink")
  - [Win32控制台](../Page/Win32控制台.md "wikilink")
  - [Win32](../Page/Win32.md "wikilink")
  - [Windows API](../Page/Windows_API.md "wikilink")

## 參見

  - [微軟議題列表](../Page/微軟議題列表.md "wikilink")

[APIs](../Category/Microsoft_lists.md "wikilink") [List of Microsoft
APIs](../Category/微軟API.md "wikilink")