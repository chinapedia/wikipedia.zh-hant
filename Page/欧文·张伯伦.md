**欧文·张伯伦**（**Owen Chamberlain**，1920年7月10日 -
2006年2月28日），是一名出生在[旧金山的](../Page/旧金山.md "wikilink")[美國](../Page/美國.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，因与[埃米利奥·塞格雷发现](../Page/埃米利奥·塞格雷.md "wikilink")[反质子而共同获得](../Page/反质子.md "wikilink")1959年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

## 生平

於1920年生於舊金山，1937年畢業於[費城的Germantown](../Page/費城.md "wikilink") Friends
School。之後他在[達特茅斯學院研讀物理](../Page/達特茅斯學院.md "wikilink")。

[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")，張伯倫在知名物理學家[恩里科·费米的指導下於](../Page/恩里科·费米.md "wikilink")[芝加哥大學繼續他的博士研究](../Page/芝加哥大學.md "wikilink")。\[1\]

張伯倫在1985年被診斷出罹患[帕金森氏症](../Page/帕金森氏症.md "wikilink")，之後於1989年離開教職。張伯倫最後因帕金森氏症所產生的併發症而於2006年2月28日死亡，享壽85歲。

## 参考资料

## 外部連結

  - [诺贝尔官方网站关于欧文·张伯伦简介](http://nobelprize.org/nobel_prizes/physics/laureates/1959/chamberlain-bio.html)

[C](../Category/1920年出生.md "wikilink")
[C](../Category/2006年逝世.md "wikilink")
[C](../Category/美国物理学家.md "wikilink")
[C](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:柏克萊加州大學教師](../Category/柏克萊加州大學教師.md "wikilink")
[C](../Category/達特茅斯學院校友.md "wikilink")
[C](../Category/加州大學柏克萊分校校友.md "wikilink")
[C](../Category/芝加哥大學校友.md "wikilink")

1.  Sanders, Robert. [Owen Chamberlain, Physics Nobelist, UC Berkeley
    professor, LBNL researcher and co-discoverer of the anti-proton, has
    died
    at 85](http://berkeley.edu/news/media/releases/2006/03/01_chamberlain.shtml).
    www.berkeley.edu (2006 March 1)