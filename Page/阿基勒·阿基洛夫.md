**阿基勒·阿基洛夫**（[塔吉克语](../Page/塔吉克语.md "wikilink")：****，[波斯語](../Page/波斯語.md "wikilink")：****，**Oqil
Oqilov**，全名****，**Okil Ghaybulloyevich
Okilov**，），[塔吉克斯坦政治人物](../Page/塔吉克斯坦.md "wikilink")，在1999年至2013年間担任[塔吉克斯坦总理](../Page/塔吉克斯坦总理.md "wikilink")（[Prime
Minister of
Tajikistan](../Page/:en:Prime_Minister_of_Tajikistan.md "wikilink")）。

他是[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")[人民民主黨](../Page/塔吉克人民民主黨.md "wikilink")（[People's
Democratic Party of
Tajikistan](../Page/:en:People's_Democratic_Party_of_Tajikistan.md "wikilink")）的成員。

## 外部參照

  - [AKILOV, Akil
    Gaibullayevich](http://www.worldwhoswho.com/views/entry.html?id=aki-ed3-070301-1021)
    International Who's Who, accessed 2006年9月3日

[Category:塔吉克斯坦总理](../Category/塔吉克斯坦总理.md "wikilink")