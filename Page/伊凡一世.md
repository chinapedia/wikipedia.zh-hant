[Ivan_Kalita.jpg](https://zh.wikipedia.org/wiki/File:Ivan_Kalita.jpg "fig:Ivan_Kalita.jpg")
**伊凡一世·丹尼洛维奇（钱袋）**（，），是[莫斯科大公](../Page/莫斯科大公.md "wikilink")（约1325年－1340年3月31日在位），[亚历山大·涅夫斯基幼子](../Page/亚历山大·涅夫斯基.md "wikilink")[丹尼尔·亚历山德罗维奇之子](../Page/丹尼尔·亚历山德罗维奇.md "wikilink")。

伊凡一世富于谋略，为达到自己的目的不择手段，狡猾而残忍。[金帳汗國在俄羅斯的](../Page/金帳汗國.md "wikilink")[達魯花赤人數太少](../Page/達魯花赤.md "wikilink")，無法有效統治。因此只有委託當地大公統治。利用以往积累的财力贿赂金帐汗国统治阶层，又站在对清算封建分裂势力有利的教会一方，抑制以[特维尔王公为首的莫斯科邻近各公国](../Page/特维尔.md "wikilink")。

## 成為弗拉基米爾大公

1327年[特维尔市民杀害负责收集税收的](../Page/特维尔.md "wikilink")[蒙古官员](../Page/蒙古.md "wikilink")，甚至杀了[欽察汗](../Page/欽察.md "wikilink")[月即别的一位堂兄弟](../Page/月即别.md "wikilink")，因此，月即别派5万人给莫斯科的伊凡大公，命令他对特维尔进行镇压。正是作为欽察汗意志的执行者，莫斯科大公们朝着远大前程迈出了第一步。1328年，伊凡一世取得[弗拉基米尔大公之位](../Page/弗拉基米尔大公.md "wikilink")，并掌握了从俄罗斯各地向金帐汗国缴收贡赋的征集权。他常常把贡赋的一部分据为己有，作为自己的活动资金。從他開始，弗拉基米爾大公之位沒有再派給其他公國，人們咸信汗國已委託他管理羅斯，不對他妄加干預。他將稅收統一化，再把各公國統一。蒙古人每到一處，也實施人口普查，但蒙古人人數太少，只有委託俄羅斯公國負責。莫斯科正合蒙古要求。

伊凡一世在莫斯科建造了最早用石头建造的乌斯平斯基大教堂，把作为[俄罗斯统一象征的](../Page/俄罗斯.md "wikilink")[弗拉基米尔主教也迁至莫斯科](../Page/弗拉基米尔.md "wikilink")（1328年）。由于他利用了宗教的权威，莫斯科事实上成为俄罗斯的政治、宗教中心。

伊凡一世一生橫徵暴斂，斂財無數，卻又一毛不拔，因此获得“卡利塔”（钱包）的外号。

|-   |-

## 資料來源

俄國史

[Category:莫斯科大公](../Category/莫斯科大公.md "wikilink")
[Category:弗拉基米尔大公](../Category/弗拉基米尔大公.md "wikilink")
[Category:诺夫哥罗德王公](../Category/诺夫哥罗德王公.md "wikilink")