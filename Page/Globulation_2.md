***Globulation
2***是一個[即時戰略遊戲](../Page/即時戰略遊戲.md "wikilink")，使用[GPLv3授权的](../Page/GPLv3.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")。程序自動分配工作給單位，將游戏微操作降到最低程度。\[1\]\[2\]现在正在[beta状态](../Page/beta.md "wikilink")，准备好后将发布稳定版本。

## 遊戲

玩家可以選擇單位的數量去做指派的各種工作，單位會在能力範圍內盡量去達完成工作，這讓玩家可以更專注在戰略上而不是管理單位工作而已。共有三種單位：工人、戰士和探險者。\[3\]
雖然許多建築或是單位的能力需要[技術](../Page/科技樹.md "wikilink")：工人要建造學校，就會需要[海藻](../Page/海藻.md "wikilink")，要取得海藻必須會游泳，這時就必須蓋個游泳池來訓練工人。

指令系統無法對單位進行直接控制，而是在地圖上布置標記。

## 遊戲模式

玩家可以進行單人遊戲，也可以藉由[區域網路](../Page/區域網路.md "wikilink")（LAN）或者透過[網際網路上的Ysagoon線上遊戲](../Page/網際網路.md "wikilink")（YOG）伺服器進行多人遊戲。也可以和[AI一起遊戲](../Page/人工智能.md "wikilink")。另外還有一個[地圖編輯器和](../Page/地圖編輯器.md "wikilink")[腳本語言程式編輯地圖並設計劇情](../Page/腳本語言.md "wikilink")。

*Globulation 2*是跨平台（Linux發行版、BSD Unix、GNU Hurd、蘋果Mac
OS、微軟Windows，等）自由軟體（[GNU通用公共許可證第3版授權](../Page/GNU通用公共許可證.md "wikilink")），包括了許多主要的開源作業系統版本例如[Debian](../Page/Debian.md "wikilink")\[4\]和[Ubuntu](../Page/Ubuntu.md "wikilink")\[5\]並且被The
Linux Game Tome網站的玩家評為5星級。\[6\]

## 參考資料

## 外部連結

  - [*Globulation 2*官方網站](http://www.globulation2.org/)

[Category:即時戰略遊戲](../Category/即時戰略遊戲.md "wikilink")
[Category:MacOS遊戲](../Category/MacOS遊戲.md "wikilink")
[Category:Linux遊戲](../Category/Linux遊戲.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:免費遊戲](../Category/免費遊戲.md "wikilink")
[Category:網路遊戲](../Category/網路遊戲.md "wikilink")
[Category:开源游戏](../Category/开源游戏.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")

1.  [*Globulation 2*
    review](http://linux.about.com/od/softgame/fr/fr_Globulation2.htm)
    linux.about.com
2.  [*Globulation 2* review](http://www.linuxjournal.com/article/7725)
    at linuxjournal.com
3.  [*Globulation 2* User
    Manual](http://www.globulation2.org/wiki/User_Manual)
4.  [Debian -
    glob2细节─lenny](http://packages.debian.org/testing/games/glob2)
5.  [Ubuntu - glob2](http://packages.ubuntu.com/feisty/games/glob2)
6.  [The Linux Game Tome:
    Globulation 2](http://www.happypenguin.org/show?Globulation%202)