**七美機場**（），全名為**交通部民用航空局七美航空站**，屬[交通部民用航空局管理](../Page/交通部民用航空局.md "wikilink")，位於[台灣](../Page/台灣.md "wikilink")[澎湖縣](../Page/澎湖縣.md "wikilink")[七美鄉平和村](../Page/七美鄉_\(臺灣\).md "wikilink")，由[德安航空負責班機的營運](../Page/德安航空.md "wikilink")。

## 簡介

七美機場原由[澎湖縣政府經營](../Page/澎湖縣.md "wikilink")，1991年5月起移由[交通部民用航空局接管](../Page/交通部民用航空局.md "wikilink")，成立[七美航空站](../Page/七美航空站.md "wikilink")，由[馬公航空站負責督導與管理](../Page/馬公機場.md "wikilink")。早期有[永興航空飛航高雄及馬公](../Page/永興航空.md "wikilink")，改為[國華航空後仍繼續營運](../Page/國華航空.md "wikilink")，在[華信航空併購](../Page/華信航空.md "wikilink")[國華航空之後](../Page/國華航空.md "wikilink")，[華信航空因不堪虧損而退出此航線](../Page/華信航空.md "wikilink")，目前由[德安航空接手經營](../Page/德安航空.md "wikilink")。

## 航空公司與航點

## 參考文獻

  -
## 外部連結

  - [七美航空站](http://www.mkport.gov.tw/Qimei/main/index.aspx)
  - [電子式飛航指南，CIVIL AERONAUTICS ADMINISTRATION MOTC
    R.O.C.](https://web.archive.org/web/20080704115523/http://eaip.caa.gov.tw/eaip/user_docs.aspx)

[C](../Category/澎湖縣交通.md "wikilink") [C](../Category/台灣機場.md "wikilink")
[Category:1991年啟用的機場](../Category/1991年啟用的機場.md "wikilink")
[Category:七美鄉 (臺灣)](../Category/七美鄉_\(臺灣\).md "wikilink")