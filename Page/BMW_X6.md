**BMW
X6**是[寶馬自](../Page/寶馬.md "wikilink")2008年開始銷售的豪華[跨界休旅車](../Page/跨界休旅車.md "wikilink")，雖然外形接近[运动型多用途车](../Page/运动型多用途车.md "wikilink")，寶馬稱其設計概念為**Sports
Activity
Coupe**，主要對手為[荒原路華及](../Page/荒原路華.md "wikilink")[保時捷卡宴](../Page/保時捷卡宴.md "wikilink")、[梅賽德斯
- 奔馳M級](../Page/梅賽德斯_-_奔馳M級.md "wikilink")，即全新類型的汽車。

## ActiveHybrid X6 (2009-)

[BMW_X6_hybrid_--_2011_DC.jpg](https://zh.wikipedia.org/wiki/File:BMW_X6_hybrid_--_2011_DC.jpg "fig:BMW_X6_hybrid_--_2011_DC.jpg")
2009年底全球油電混合車大會上[BMW發表了混合車版本X](../Page/BMW.md "wikilink")6，目前已知有兩種款式。\[1\]本款車名為BMW
ActiveHybrid
X6，也是當時世界上馬力最大的油電混合車，但是本車不會在[英國上市](../Page/英國.md "wikilink")。\[2\]同時這台車的公開問世，也透露了將來用於7系列的油電車系統（2009年[法蘭克福車展上就已經公佈該消息](../Page/法蘭克福車展.md "wikilink")）。ActiveHybrid
X6在2009年12月於[美國市場開賣](../Page/美國.md "wikilink")，售價約89,765[美元](../Page/美元.md "wikilink")。\[3\]

BMW ActiveHybrid X6的動力有300 kW（407 hp），V8引擎加掛BMW TwinPower渦輪和兩具67 kW（91
hp）電動[馬達](../Page/馬達.md "wikilink")，各有63 kW（86 hp）實質功率，最大系統輸出357 kW（485
hp）、峰值馬力780 N·m（575 lb-ft）。

BMW ActiveHybrid科技有三種驅動模式:
純電力、純引擎和最大加速力下的兩者混合，此時可達485匹[馬力](../Page/馬力.md "wikilink")；然而就算在不排放[二氧化碳的電力模式還是有](../Page/二氧化碳.md "wikilink")時速。本系統還融合stop-start科技與諸多節能專利技術，縱然車子本身重量不低，但是汽油消耗量平均不高，未來的[柴油電力版X](../Page/柴油.md "wikilink")6還能再壓低油耗。

## 賽車

BMW X6 M
做為2009年[MotoGP的](../Page/MotoGP.md "wikilink")[安全車](../Page/安全車.md "wikilink")。

## 參考文獻

## 外部連結

  - [2008 BMW X6: First Test
    Drive](http://www.motortrend.com/roadtests/suvs/112_0806_2008_bmw_X6_first_drive)
  - [Video and Spy Photos: 2009 BMW
    X6](https://web.archive.org/web/20090204171944/http://www.edmunds.com/insideline/do/GeneralFuture/articleId%3D119193#4)

[Category:2008年面世的汽車](../Category/2008年面世的汽車.md "wikilink")
[Category:BMW車輛](../Category/BMW車輛.md "wikilink")
[Category:跨界休旅車](../Category/跨界休旅車.md "wikilink")

1.  "[BMW, Daimler, GM hybrid alliance nears
    end](http://www.autonews.com/apps/pbcs.dll/article?AID=/20090714/COPY02/307149993&AssignSessionID=373348067472168)",
    Matthias Krust, [Automotive
    News](../Page/Automotive_News.md "wikilink"), July 14, 2009
2.
3.