[HK_Belcher_s_Street_Kennedy_Town.jpg](https://zh.wikipedia.org/wiki/File:HK_Belcher_s_Street_Kennedy_Town.jpg "fig:HK_Belcher_s_Street_Kennedy_Town.jpg")。\]\]
**陳特楚**是一名香港政治人物，前[中西區區議會](../Page/中西區區議會.md "wikilink")[議員](../Page/議員.md "wikilink")，2004至2011年任[中西區區議會](../Page/中西區區議會.md "wikilink")[主席](../Page/主席.md "wikilink")，曾為[自由黨員和自由黨選委會主席](../Page/自由黨_\(香港\).md "wikilink")，已退黨。\[1\]

在[2007年香港區議會選舉中](../Page/2007年香港區議會選舉.md "wikilink")，陳特楚在無對手之下[自動當選](../Page/自動當選.md "wikilink")。\[2\]

陳特楚的議員辦事處在[香港](../Page/香港.md "wikilink")[西環](../Page/西環.md "wikilink")[卑路乍街](../Page/卑路乍街.md "wikilink")56號。
陳特楚也是[新中華飯店](../Page/新中華飯店.md "wikilink")[公司](../Page/公司.md "wikilink")[董事](../Page/董事.md "wikilink")。

陳特楚主要關心的地區事務包括[文化](../Page/文化.md "wikilink")、[康樂](../Page/康樂.md "wikilink")、[食物](../Page/食物.md "wikilink")、[環境](../Page/環境.md "wikilink")、[衛生](../Page/衛生.md "wikilink")、[交通](../Page/交通.md "wikilink")、[運輸](../Page/運輸.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:陳姓](../Category/陳姓.md "wikilink")
[Category:前香港區議員](../Category/前香港區議員.md "wikilink")

1.  [為撐葉國謙　陳特楚退黨](http://www1.hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20080728&sec_id=4104&subsec_id=11867&art_id=11400012)
2.  [中西區區議會:
    陳特楚](http://www.districtcouncils.gov.hk/central_d/chinese/member_detail.htm)