**百合目**\[1\]（[学名](../Page/学名.md "wikilink")：）是很多科[单子叶植物的总称](../Page/单子叶植物.md "wikilink")。

  - [六出花科](../Page/六出花科.md "wikilink") (Alstroemeriaceae)
  - [金梅草科](../Page/金梅草科.md "wikilink") (Campynemataceae)
  - [秋水仙科](../Page/秋水仙科.md "wikilink") (Colchicaceae)
  - [美麗腐草科](../Page/美麗腐草科.md "wikilink") (Corsiaceae)
  - [百合科](../Page/百合科.md "wikilink") (Liliaceae)
  - [菝葜木科](../Page/菝葜木科.md "wikilink") (Luzuriagaceae)
  - [黑藥花科](../Page/黑藥花科.md "wikilink") (Melanthiaceae)
  - [刺藤科](../Page/刺藤科.md "wikilink") (Petermanniaceae)
  - [垂花科](../Page/垂花科.md "wikilink") (Philesiaceae)
  - [菝葜藤科](../Page/菝葜藤科.md "wikilink") (Ripogonaceae)
  - [菝葜科](../Page/菝葜科.md "wikilink") (Smilacaceae)

## 參考資料

<references />

[\*](../Category/百合目.md "wikilink")

1.