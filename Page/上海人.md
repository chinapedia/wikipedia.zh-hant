从[上海简短的](../Page/上海.md "wikilink")[城市历史来看](../Page/上海历史.md "wikilink")，现代**上海人**的概念存在仅百年左右。上海人通常讲[上海话](../Page/上海话.md "wikilink")，上海话属于[江南](../Page/江南.md "wikilink")[吴语的一种](../Page/吴语.md "wikilink")。由于现代上海地区的三分之二是最近的两千年才逐渐成陆，而其余早先成陆的地区也饱受海侵与洪水，几度荒无人烟，所以如今上海的居民追根搠源，基本上都是不同时期的[移民](../Page/移民.md "wikilink")。其中大部分是来自于邻近的[江苏](../Page/江苏.md "wikilink")、[浙江地区](../Page/浙江.md "wikilink")。

## 来源与形成

长久以来，上海地区的上古居民被认为是[吴越人的一支](../Page/百越.md "wikilink")。但最近的[遗传学研究發現](../Page/遗传学.md "wikilink")，现代上海市区人约45%的遗传成分以及上海郊区人约30%的遗传成分是来自于当时北方黃河流域居民\[1\]，而曾经的原住民——吴越人則成為漢族的一部分。中国现存的吴越遗传成分以上海南郊和[浙江居多](../Page/浙江.md "wikilink")，而上海市区、[嘉定区](../Page/嘉定区.md "wikilink")、[宝山区](../Page/宝山区.md "wikilink")、[浦东新区](../Page/浦东新区.md "wikilink")、[崇明县等较晚成陆的北部地区吴越人所占的人口比例則有所降低](../Page/崇明县.md "wikilink")。吴越人是[周朝以来分布在](../Page/周朝.md "wikilink")[长江以南](../Page/长江.md "wikilink")[吴国](../Page/吴国.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")、[江西](../Page/江西.md "wikilink")）和[越国](../Page/越国.md "wikilink")（今[浙江](../Page/浙江.md "wikilink")、[福建](../Page/福建.md "wikilink")、[广东](../Page/广东.md "wikilink")）等地的一个部落，其源头可追溯至[马家浜文化甚至是浙江的](../Page/马家浜文化.md "wikilink")[河姆渡文化时期的先民](../Page/河姆渡文化.md "wikilink")。[西周时期](../Page/西周.md "wikilink")[太伯奔吳建立](../Page/太伯.md "wikilink")[吴国](../Page/吴国.md "wikilink")，后又先后被[越国](../Page/越国.md "wikilink")、[楚国征服](../Page/楚国.md "wikilink")，楚国时为[春申君的封地](../Page/春申君.md "wikilink")，因而上海又简称“申”。在[晋朝的](../Page/晋朝.md "wikilink")[衣冠南渡](../Page/衣冠南渡.md "wikilink")\[2\]、[两宋之间](../Page/两宋.md "wikilink")\[3\]等时期，都曾有中原人大规模移民[江南](../Page/江南.md "wikilink")。在[南宋时](../Page/南宋.md "wikilink")，设立了上海镇\[4\]，[元朝时设立了](../Page/元朝.md "wikilink")[上海县](../Page/上海县.md "wikilink")\[5\]，近代以来逐渐成为一个大都市。1950年代，上海市周边郊县划入上海市，之后其中的大多数的郊县改为区或新区\[6\]。

## 上海名人

[上海开埠后](../Page/上海开埠.md "wikilink")，各地的精英移民来到上海。上海有“冒险家乐园”之称，很多近代的上海名人并不出生于上海，如[鲁迅](../Page/鲁迅.md "wikilink")（绍兴）、[周璇](../Page/周璇.md "wikilink")（常州），但上海成功吸引这些移民和上海本地居民，並共同成就了上海的都市辉煌。在体育方面，上海人也取得了突出的成绩，如女子[足球运动员](../Page/足球.md "wikilink")[孙雯](../Page/孙雯.md "wikilink")、男子足球运动员[范志毅](../Page/范志毅.md "wikilink")、[田径运动员](../Page/田径.md "wikilink")[刘翔](../Page/刘翔.md "wikilink")、[NBA球员](../Page/NBA.md "wikilink")[姚明等](../Page/姚明.md "wikilink")。

### 文化名人

在近代、现代与当代上海文化名人中，除了本来就是上海人，也包括曾经在上海寓居过的文化名人。除了大家耳熟能详的名人，亦包括由于种种历史的原因而被埋没与封存了许久的前辈。

[文学界](../Page/中国文学.md "wikilink")：[李伯元](../Page/李伯元.md "wikilink")（武進籍）、[曾朴](../Page/曾朴.md "wikilink")、[陈独秀](../Page/陈独秀.md "wikilink")、[鲁迅](../Page/鲁迅.md "wikilink")、[瞿秋白](../Page/瞿秋白.md "wikilink")、[储安平](../Page/储安平.md "wikilink")、[茅盾](../Page/茅盾.md "wikilink")、[巴金](../Page/巴金.md "wikilink")、[史量才](../Page/史量才.md "wikilink")、[王元化](../Page/王元化.md "wikilink")、[王造时](../Page/王造时.md "wikilink")、芦芒、[吴强](../Page/吴强.md "wikilink")、[王安忆](../Page/王安忆.md "wikilink")、叶辛、[陆星儿](../Page/陆星儿.md "wikilink")、[叶永烈](../Page/叶永烈.md "wikilink")、[程乃珊](../Page/程乃珊.md "wikilink")、[陈伯吹](../Page/陈伯吹.md "wikilink")、[施蛰存](../Page/施蛰存.md "wikilink")、[赵超构](../Page/赵超构.md "wikilink")、[陆谷孙](../Page/陆谷孙.md "wikilink")、[曹禺](../Page/曹禺.md "wikilink")、[胡适](../Page/胡适.md "wikilink")、[蔡元培](../Page/蔡元培.md "wikilink")、[柔石](../Page/柔石.md "wikilink")、[邹韬奋](../Page/邹韬奋.md "wikilink")、[陶行知](../Page/陶行知.md "wikilink")、[黄炎培](../Page/黄炎培.md "wikilink")、[郭沫若](../Page/郭沫若.md "wikilink")、[卞之琳](../Page/卞之琳.md "wikilink")、周子亚、[宗白华](../Page/宗白华.md "wikilink")、[吴晗](../Page/吴晗.md "wikilink")、[季羡林](../Page/季羡林.md "wikilink")、[柳无忌](../Page/柳无忌.md "wikilink")、[马寅初](../Page/马寅初.md "wikilink")、[梁实秋](../Page/梁实秋.md "wikilink")、[冯友兰](../Page/冯友兰.md "wikilink")、[傅雷](../Page/傅雷.md "wikilink")、[费孝通](../Page/费孝通.md "wikilink")、[朱自清](../Page/朱自清.md "wikilink")、[钱锺书](../Page/钱锺书.md "wikilink")、[叶圣陶等](../Page/叶圣陶.md "wikilink")。
　　

[戏剧界](../Page/戏剧.md "wikilink")：[汪笑侬](../Page/汪笑侬.md "wikilink")、[梅兰芳](../Page/梅兰芳.md "wikilink")、[周信芳](../Page/周信芳.md "wikilink")、[尚长荣](../Page/尚长荣.md "wikilink")、蔡正仁、岳美缇、梁谷音、[李炳淑](../Page/李炳淑.md "wikilink")、评弹三杰、[马莉莉](../Page/马莉莉.md "wikilink")、[焦晃](../Page/焦晃.md "wikilink")、[王汝刚](../Page/王汝刚.md "wikilink")、[茅善玉](../Page/茅善玉.md "wikilink")、[袁雪芬](../Page/袁雪芬.md "wikilink")、[傅全香等](../Page/傅全香.md "wikilink")。

影视界：[王家卫](../Page/王家卫.md "wikilink")、[赵丹](../Page/赵丹.md "wikilink")、[谢晋](../Page/谢晋.md "wikilink")、[孙道临](../Page/孙道临.md "wikilink")、[陈冲](../Page/陈冲.md "wikilink")、[宁静](../Page/宁静.md "wikilink")、[潘虹](../Page/潘虹.md "wikilink")、[张瑞芳](../Page/张瑞芳.md "wikilink")、[秦怡](../Page/秦怡.md "wikilink")、[奚美娟](../Page/奚美娟.md "wikilink")、蓝苹（[江青](../Page/江青.md "wikilink")）、[于伶](../Page/于伶.md "wikilink")、[白杨](../Page/白杨.md "wikilink")、[黄佐临](../Page/黄佐临.md "wikilink")、[周璇](../Page/周璇.md "wikilink")、[阮玲玉](../Page/阮玲玉.md "wikilink")、[严顺开](../Page/严顺开.md "wikilink")、[章金莱等](../Page/章金莱.md "wikilink")。
　　

歌舞界：[陈燮阳](../Page/陈燮阳.md "wikilink")、[黄豆豆](../Page/黄豆豆.md "wikilink")、[廖昌永](../Page/廖昌永.md "wikilink")、陈传熙、[舒巧](../Page/舒巧.md "wikilink")、辛丽丽、杜聪、周洁、[毛阿敏等](../Page/毛阿敏.md "wikilink")。
　　

[音乐界](../Page/中国音乐.md "wikilink")：[聂耳](../Page/聂耳.md "wikilink")、[贺绿汀](../Page/贺绿汀.md "wikilink")、[周小燕](../Page/周小燕.md "wikilink")、[朱践耳](../Page/朱践耳.md "wikilink")、[薛之谦等](../Page/薛之谦.md "wikilink")。

上海，上溯[秦](../Page/秦.md "wikilink")[汉时期](../Page/汉.md "wikilink")，下至当今，诸代都有名人见诸史册。[三国时期](../Page/三国时期.md "wikilink")，华亭人[陆逊便受拜](../Page/陆逊.md "wikilink")[东吴](../Page/东吴.md "wikilink")[丞相](../Page/丞相.md "wikilink")，封华亭侯。[两晋时期](../Page/两晋.md "wikilink")，[江东名士](../Page/江东.md "wikilink")[陆机](../Page/陆机.md "wikilink")、[陆云以书法文章名动一时](../Page/陆云.md "wikilink")，被时人誉为“云间两陆”。此后，因[江南经济迅速发展](../Page/江南.md "wikilink")，名人辈出。近代开埠以后，由于是中国最大都会和对外交流的中心城市，上海云集来自全国各地的名人。许多名人均由上海发迹走向全国。1949年以后，由于在文艺、体育、科教等领域的优势，上海也培养了一大批名闻全国的著名人物。

### 政界

[Soong_sisters.jpg](https://zh.wikipedia.org/wiki/File:Soong_sisters.jpg "fig:Soong_sisters.jpg")
[明代有](../Page/明代.md "wikilink")[内阁首辅](../Page/内阁首辅.md "wikilink")[徐阶](../Page/徐阶.md "wikilink")，内阁次辅[徐光启及其学生](../Page/徐光启.md "wikilink")、[炮兵专家](../Page/炮兵.md "wikilink")[孙元化](../Page/孙元化.md "wikilink")，少年[诗人](../Page/诗人.md "wikilink")、抗[清英烈](../Page/清.md "wikilink")[夏完淳](../Page/夏完淳.md "wikilink")。近代，[孙中山长期居住在上海](../Page/孙中山.md "wikilink")。[国民政府时期的](../Page/国民政府.md "wikilink")[四大家族核心人物](../Page/四大家族_\(蒋宋孔陈\).md "wikilink")[宋霭龄](../Page/宋霭龄.md "wikilink")、[宋庆龄](../Page/宋庆龄.md "wikilink")、[宋子文](../Page/宋子文.md "wikilink")、[宋美龄](../Page/宋美龄.md "wikilink")、[陈立夫](../Page/陈立夫.md "wikilink")、[陈果夫或生于上海或成长于上海](../Page/陈果夫.md "wikilink")。[中国共产党早期曾长期将](../Page/中国共产党.md "wikilink")[中央机关设在上海](../Page/中国共产党中央局.md "wikilink")，成为大批[共产党员活跃的地区](../Page/共产党员.md "wikilink")。民国著名的政界人物，后转任[台湾省主席的](../Page/台湾省主席.md "wikilink")[吴国桢曾于光复后担任](../Page/吴国桢.md "wikilink")[上海市市长直至](../Page/上海市市长.md "wikilink")[解放军进入上海前夕](../Page/解放军.md "wikilink")。1949年以后，[中共经济战线领导人](../Page/中共.md "wikilink")[陈云是上海](../Page/陈云.md "wikilink")[青浦人](../Page/青浦.md "wikilink")。上海曾是“[四人帮](../Page/四人帮.md "wikilink")”的主要基地。[陈毅](../Page/陈毅.md "wikilink")、[汪道涵](../Page/汪道涵.md "wikilink")、[江泽民](../Page/江泽民.md "wikilink")、[习近平](../Page/习近平.md "wikilink")、[朱镕基](../Page/朱镕基.md "wikilink")、[吴邦国](../Page/吴邦国.md "wikilink")、[黄菊](../Page/黄菊.md "wikilink")、[陈良宇](../Page/陈良宇.md "wikilink")、[韩正](../Page/韩正.md "wikilink")、[徐匡迪](../Page/徐匡迪.md "wikilink")、[孟建柱](../Page/孟建柱.md "wikilink")、[陈至立等](../Page/陈至立.md "wikilink")[党和国家领导人](../Page/党和国家领导人.md "wikilink")，都曾担任[上海市市长](../Page/上海市市长.md "wikilink")、[市委书记或其副职](../Page/市委书记.md "wikilink")。[香港回归后的首任](../Page/香港回归.md "wikilink")[立法会主席](../Page/立法会.md "wikilink")[范徐丽泰亦为上海人](../Page/范徐丽泰.md "wikilink")。

### 科学界

上海由于经济发达，科教文化亦位居中国前列。[元末](../Page/元.md "wikilink")[明初的](../Page/明.md "wikilink")[纺织改革家](../Page/纺织.md "wikilink")[黄道婆](../Page/黄道婆.md "wikilink")、[明末](../Page/明末.md "wikilink")[西学和](../Page/西学.md "wikilink")[农学学者](../Page/农学.md "wikilink")[徐光启即为上海人](../Page/徐光启.md "wikilink")。此外还有[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")[李政道](../Page/李政道.md "wikilink")、[高锟](../Page/高锟.md "wikilink")，[遗传学家](../Page/遗传学家.md "wikilink")[谈家桢](../Page/谈家桢.md "wikilink")，显微外科专家[陈中伟](../Page/陈中伟.md "wikilink")、[张涤生](../Page/张涤生.md "wikilink")，肝胆外科专家[吴孟超](../Page/吴孟超.md "wikilink")，[血液学专家](../Page/血液学.md "wikilink")[王振义](../Page/王振义.md "wikilink")、[陈竺等](../Page/陈竺.md "wikilink")。

### 文化艺术界

上海自古代到现代文化艺术十分繁荣，目前上海、[台湾](../Page/台湾.md "wikilink")、[香港等地著名文化艺术界人士有大批都为上海人](../Page/香港.md "wikilink")。著名报业家[史量才即长期活动在上海](../Page/史量才.md "wikilink")；著名作家[茅盾](../Page/茅盾.md "wikilink")、[张爱玲](../Page/张爱玲.md "wikilink")、[巴金都长期居住在上海](../Page/巴金.md "wikilink")；音乐表演的上海人还有[贺绿汀](../Page/贺绿汀.md "wikilink")、[周璇](../Page/周璇.md "wikilink")、[周小燕](../Page/周小燕.md "wikilink")、[金莎](../Page/金莎.md "wikilink")、[谢晋](../Page/谢晋.md "wikilink")、[孙道临](../Page/孙道临.md "wikilink")、[周信芳](../Page/周信芳.md "wikilink")、[姚慕双](../Page/姚慕双.md "wikilink")、[周柏春等人](../Page/周柏春.md "wikilink")；[香港著名艺人](../Page/香港.md "wikilink")[鄧紫棋](../Page/鄧紫棋.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[沈殿霞](../Page/沈殿霞.md "wikilink")、[张曼玉](../Page/张曼玉.md "wikilink")、[陈慧琳](../Page/陈慧琳.md "wikilink")、[张卫健](../Page/张卫健.md "wikilink")、[方大同](../Page/方大同.md "wikilink")、[angelababy也都是上海籍貫](../Page/angelababy.md "wikilink")。

### 美术界

上海有许多美术大家：[任伯年](../Page/任伯年.md "wikilink")、[钱慧安](../Page/钱慧安.md "wikilink")、[吴昌硕](../Page/吴昌硕.md "wikilink")、[刘海粟](../Page/刘海粟.md "wikilink")、[张大千](../Page/张大千.md "wikilink")、[林风眠](../Page/林风眠.md "wikilink")、[王一亭](../Page/王一亭.md "wikilink")(王震)、[朱屺瞻](../Page/朱屺瞻.md "wikilink")、杨可扬、[谢稚柳](../Page/谢稚柳.md "wikilink")、[张乐平](../Page/张乐平.md "wikilink")、朱宣咸、[胡一川](../Page/胡一川.md "wikilink")、[程十发](../Page/程十发.md "wikilink")、庞薰琴、郑野夫、陈铁耕、陈烟桥、沃渣、马达、力群、[江丰](../Page/江丰.md "wikilink")、黄新波、李桦、王琦、余白墅、王麦秆、[黄永玉](../Page/黄永玉.md "wikilink")、邵克萍、汪刃锋、[陈逸飞](../Page/陈逸飞.md "wikilink")、[陈丹青](../Page/陈丹青.md "wikilink")、赵延年、荒烟、沈柔坚、沈鸿根、[沈尹默](../Page/沈尹默.md "wikilink")、[陆俨少](../Page/陆俨少.md "wikilink")、黄丕谟、[施大畏](../Page/施大畏.md "wikilink")、[李磊](../Page/李磊.md "wikilink")、许德民、韩天衡等。

### 体育界

上海体育资源较为丰富，故成功培养了一大批体育[运动员与教练员](../Page/运动员.md "wikilink")。[田径界的有亚洲第一位男子田径](../Page/田径.md "wikilink")[世界纪录创造者](../Page/世界纪录.md "wikilink")、[跳高运动员](../Page/跳高.md "wikilink")[朱建华](../Page/朱建华.md "wikilink")；110米栏原世界纪录保持者[刘翔及其](../Page/刘翔.md "wikilink")[教练](../Page/教练.md "wikilink")[孙海平](../Page/孙海平.md "wikilink")；[游泳](../Page/游泳.md "wikilink")[冠军](../Page/冠军.md "wikilink")[庄泳](../Page/庄泳.md "wikilink")、[杨文意](../Page/杨文意.md "wikilink")、[乐靖宜](../Page/乐靖宜.md "wikilink")、[沈坚强](../Page/沈坚强.md "wikilink")、[刘子歌等](../Page/刘子歌.md "wikilink")；著名[篮球运动员](../Page/篮球运动员.md "wikilink")[姚明](../Page/姚明.md "wikilink")；[排球运动员沈富麟](../Page/排球.md "wikilink")、[汪嘉伟](../Page/汪嘉伟.md "wikilink")、[沈琼](../Page/沈琼.md "wikilink")、[汤淼等](../Page/汤淼.md "wikilink")；[乒乓球运动员后任](../Page/乒乓球.md "wikilink")[国家体育总局](../Page/国家体育总局.md "wikilink")[副局长的](../Page/副局长.md "wikilink")[李富荣](../Page/李富荣.md "wikilink")、[国际乒联主席](../Page/国际乒联.md "wikilink")[徐寅生](../Page/徐寅生.md "wikilink")、[王励勤等](../Page/王励勤.md "wikilink")；[足球运动员和教练员](../Page/足球运动员.md "wikilink")[徐根宝](../Page/徐根宝.md "wikilink")、[范志毅](../Page/范志毅.md "wikilink")、[孙雯](../Page/孙雯.md "wikilink")、[谢晖](../Page/谢晖.md "wikilink")、[孙祥等](../Page/孙祥.md "wikilink")；[跳水运动员](../Page/跳水.md "wikilink")[吴敏霞](../Page/吴敏霞.md "wikilink")、[火亮](../Page/火亮.md "wikilink")；[中国象棋数届全国](../Page/中国象棋.md "wikilink")[冠军](../Page/冠军.md "wikilink")[胡荣华](../Page/胡荣华.md "wikilink")，[围棋冠军](../Page/围棋.md "wikilink")[常昊等](../Page/常昊.md "wikilink")。

## 上海人的类型

广义上，在上海市的行政区域内定居或出生并取得户口的人都称为上海人。而上海人亦可細分为包括本地人、郊区人、老移民在内的“老上海人”以及或被称作“新上海人”的新移民。

### 本地人

上海人中所说的“本地人”，是指在1843年[上海开埠以前](../Page/上海开埠.md "wikilink")，就已经定居上海市区的原住居民。

### 郊区人

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

上海因[第一次鸦片战争](../Page/第一次鸦片战争.md "wikilink")[开埠形成重要的国际性都市](../Page/上海開埠.md "wikilink")，继而从[江苏省分离出来](../Page/江苏省.md "wikilink")，最后设立[一级行政区](../Page/一级行政区.md "wikilink")（[直辖市](../Page/直辖市.md "wikilink")）的这段历史有一百多年，但今天上海市除九个市中心区以外的各个新区、郊区、县都是在[中华人民共和国成立后的](../Page/中华人民共和国.md "wikilink")1950年代开始才逐渐从江苏省划归上海市的，此前它们都是江苏省的县。于是这些上海人习惯上被称作“郊区人”。相对于市区上海人主要是由过去的移民形成，人口成分变动大，郊区的上海人则基本是原本隶属于江苏省松江地区、苏州地区和南通地区人，人口变动较小。在上海郊区，通常称上海市区为上海。因此，有时他们认同自己“上海市人”的身份，但不认同自己的“上海人”身份。这一情况在远郊的金山、奉贤、南汇、崇明等地尤为明显。
各个郊县划归上海市的历史过程为：

1.  江苏省松江专区的嘉定、宝山、上海3县划归上海市（国务院1958年1月14日批准）\[7\]
2.  撤销江苏省松江专区，下辖松江、川沙、青浦、南汇、奉贤、金山6县并入苏州专区（国务院1958年3月26日批准）\[8\]
3.  江苏省[川沙县的](../Page/川沙县.md "wikilink")[横沙乡](../Page/横沙乡.md "wikilink")，划归上海市[宝山县](../Page/宝山县.md "wikilink")\[9\]
4.  江苏省苏州专区的松江、川沙、青浦、南汇、奉贤、金山6个县，南通专区的[崇明县](../Page/崇明县.md "wikilink")，划归上海市（1958年11月21日第82次[国务院全体会议决定](../Page/国务院全体会议.md "wikilink")）。

### 老移民

上海开埠后，移民的数量远远超过世居于此的本地人。他们来自于[中国各地](../Page/中国.md "wikilink")，但是其中来自[江苏](../Page/江苏.md "wikilink")、[浙江两省者占到](../Page/浙江.md "wikilink")80%以上，占据绝对优势。这些移民大致上又可以分为两大部分：一部分来自[浙江省和](../Page/浙江省.md "wikilink")[江苏南部的](../Page/江苏.md "wikilink")[吴语区](../Page/吴语区.md "wikilink")，其中一般又按照[清代府属细分为](../Page/清代.md "wikilink")[宁波人](../Page/宁波.md "wikilink")、[绍兴人](../Page/绍兴.md "wikilink")、[苏州人等](../Page/苏州.md "wikilink")。他们构成了[上海市中心城区居民的主体](../Page/上海市.md "wikilink")，其中尤以宁波籍为最多；另一部分来自[江苏的](../Page/江苏.md "wikilink")[江淮官話区](../Page/江淮官話.md "wikilink")，被前者称为[江北人](../Page/江北人.md "wikilink")，传统上大多聚居于[苏州河以北西北部的](../Page/苏州河.md "wikilink")[闸北](../Page/闸北.md "wikilink")、[普陀](../Page/普陀.md "wikilink")；也有不少人住在城市东北部的[杨浦](../Page/杨浦.md "wikilink")、[虹口](../Page/虹口.md "wikilink")，与来自[浙江](../Page/浙江.md "wikilink")[宁波等地的移民杂居](../Page/宁波.md "wikilink")。[老城厢南市等区域也有部分](../Page/上海老城厢.md "wikilink")[苏北移民迁入](../Page/苏北.md "wikilink")。上海现有16区，其中[黄浦](../Page/黄浦.md "wikilink")、[徐汇](../Page/徐汇.md "wikilink")、[长宁](../Page/长宁.md "wikilink")、[静安](../Page/静安.md "wikilink")、[普陀](../Page/普陀.md "wikilink")、[虹口](../Page/虹口.md "wikilink")、[杨浦为传统中心城区](../Page/杨浦.md "wikilink")。据来自以上各地区的移民数量均为数甚多，各自约在数十万至百万人以上不等。总体而言，来自[吴语区移民经济社会地位高于](../Page/吴语区.md "wikilink")[江淮官话区移民](../Page/江淮官话.md "wikilink")，传统上优越感较强，后者常感受到歧视。

来自其他各省的移民数量相对较少，其中较为重要者系来自[广东省的移民](../Page/广东省.md "wikilink")，到1949年底，上海有[广东人](../Page/广东人.md "wikilink")119,178人，其中[香山人估计为](../Page/香山.md "wikilink")3.5万。他们相对集中的区域，为老上海的[虹口](../Page/虹口.md "wikilink")、新成、[静安寺](../Page/静安寺.md "wikilink")、[嵩山等区](../Page/嵩山.md "wikilink")，在今[虹口](../Page/虹口.md "wikilink")、[黄浦](../Page/黄浦.md "wikilink")、[卢湾](../Page/卢湾.md "wikilink")、[静安等区域内](../Page/静安.md "wikilink")，都是上海的黄金地段\[10\]。

今日上海市中心城区居住的大都是这些移民，是“上海人”的核心主体。

上海大规模的移民最初源于[太平天国战争时期](../Page/太平天国.md "wikilink")。1860年代初[太平军大规模进攻](../Page/太平军.md "wikilink")[苏](../Page/苏.md "wikilink")[杭地区](../Page/杭.md "wikilink")，造成大批难民躲入有外国势力保护的[租界](../Page/租界.md "wikilink")，直接造就了租界的繁荣，并为日后源源不断地吸引移民奠定了基础。近代上海行政格局为一市三治，政府对社会控制能力相对薄弱，各地移民的同乡组织在社会生活中起重要作用，移民的地域特征相当显著，[广东人](../Page/广东人.md "wikilink")、[宁波人](../Page/宁波.md "wikilink")、[苏北人是最为重要的三大群体](../Page/苏北.md "wikilink")，而广东人中又以[香山人最为突出](../Page/香山縣.md "wikilink")\[11\]。

[中华人民共和国成立以后](../Page/中华人民共和国.md "wikilink")，上海市的人口迁出量甚大，但流入量则大为减少，现代上海人的群体逐渐稳定下来。这一情况直到1990年前后才被改变。

### 新移民

自[中华人民共和国进行](../Page/中华人民共和国.md "wikilink")[改革开放](../Page/改革开放.md "wikilink")，特别是1990年代上海开发[浦东以来](../Page/浦东.md "wikilink")，[中国乃至](../Page/中国.md "wikilink")[世界各地的新移民纷纷来到上海工作](../Page/世界.md "wikilink")、生活。他（她）们中定居下来的自诩为“新上海人”。新移民和外来人员的区别在于是否拥有上海[户口](../Page/户口.md "wikilink")。

新老移民的冲突，基本存在于民间，在网络较为激烈\[12\]。这种冲突一般被官方媒体刻意忽视淡化。

比较著名的冲突地点，如上海本地论坛“[寬-{}-帶山](../Page/宽带山.md "wikilink")”中，以“硬-{}-盘”或“yp”作为外地人的贬称\[13\]（因关于外地人的讨论被论坛屏蔽，而“[西部数据](../Page/西部数据.md "wikilink")”这个[硬盘厂商的缩写和外地人拼音首字母相同](../Page/硬盘.md "wikilink")）。在矛盾激化后，该论坛时常有网民鼓吹各类老上海人联合起来抵抗“新移民”和外地来沪人员。

## 移居外地的上海人

1937年[中國抗日戰爭爆发后](../Page/中國抗日戰爭.md "wikilink")，有不少上海人随[國民政府西迁](../Page/國民政府.md "wikilink")[重庆等地](../Page/重庆.md "wikilink")，或移居[香港](../Page/香港.md "wikilink")\[14\]。抗战初期，从上海遷出的民間工厂达146家\[15\]。

1949年，有众多的上海人离开[大陆](../Page/大陆.md "wikilink")，移居[香港](../Page/香港.md "wikilink")、[台湾或](../Page/台湾.md "wikilink")[美国等地](../Page/美国.md "wikilink")。而上海人最初移居[香港则以](../Page/香港.md "wikilink")[北角较集中](../Page/北角.md "wikilink")，色情行業林立，故称“小上海”。\[16\]

1949年以后，也有大批上海人陆续随工厂内迁。

## 上海人的特点

### 对国家贡献卓著

由于上海从1843年开埠至今已经有约170年而中国大陆[对外开放仅](../Page/改革开放.md "wikilink")30多年，因此可能造成上海人和相当部分[外地来沪人员在文化](../Page/外地来沪人员.md "wikilink")、[价值取向和](../Page/价值观.md "wikilink")[意识形态上有较大差异](../Page/意识形态.md "wikilink")。这种差异可能造成了目前上海人和许多中国其他地区的民众的一些矛盾\[17\]。在1950年代至1990年代，上海人占中国人口的百分之一，但负担着全中国10%的工业产值、16%以上的财政收入以及大量技术含量相对较高的产品，上海制造几乎涵盖了所有的轻工业产品，“上海牌”商品在[中国人心目中几乎就是](../Page/中国人.md "wikilink")“高档货”的代名词，而这也连带体现上海人认真负责和聪明能干的性格特点\[18\]\[19\]。

### 整体综合素质高

在科学素养方面，依据《2008中国两院院士调查报告》所发布的统计数据显示，，在按照城市出生地划分的[中国科学院和](../Page/中国科学院.md "wikilink")[中国工程院院士中](../Page/中国工程院.md "wikilink")，上海人比例最高（占总数12%强），人数亦最多（229人）\[20\]。另依据2009年[经济合作与发展组织](../Page/经济合作与发展组织.md "wikilink")（OECD）正式公布的“第四次国际学生评估项目”（PISA）之调查结果，上海学生综合素质位居全球第一，不仅如此，在PISA的全部三项分项指标：阅读素养、数学素养和科学素养的考评中，上海学生均排在第一位\[21\]。在身体素质方面，依据中国国家体育总局在2015年11月发布的《2014年国民体质监测公报》的相关统计数据显示，上海人体质达标率达到97.1%，体质综合指数高达107.91，两项指标均位居全国第一，上海人的身体素质是全中国最好的\[22\]。

### 遭受诽谤和诋毁

但是，由于以《[春晚](../Page/春晚.md "wikilink")》为代表的北方影视作品的严重污蔑和诋毁\[23\]，导致[中国内地的部分民众对上海人印象不佳](../Page/中国内地.md "wikilink")，他们片面认为上海人精明小气\[24\]；上海女人妖娆妩媚；上海男人缺乏血性等等。这种认识是一种偏见和误读，因为上海不但是中国现代慈善事业的发源地，而且在建国后的灾难捐款中，人均捐款数也是名列前茅\[25\]。虽然被北方媒体的批判，并未用什么过激的手段反击，但上海人依据体现了自己顽强的生命力，他们依旧过着自己市井小民的日子\[26\]。上海人的身上，不断融合[现代商业社会的精明](../Page/西方文明.md "wikilink")、理性，却又保留着[中华文化的优雅](../Page/中华文化.md "wikilink")、仁义\[27\]。上海的男人与女人从骨子里散发出的从容与优雅，捍卫着传统文化的自尊。要了解上海人，有三个路径，亦即：城市的历史、家族的历史以及市民的普遍价值\[28\]。有人批评上海人的政治冷漠，已故上海记者曹小磊说：上海人不是政治冷漠，而是他们实在见识得太多了\[29\]。1990年代伴随着[浦东开发开放](../Page/浦东开发开放.md "wikilink")，上海人逐步重拾自信，开始变得更阳光、直率、幽默起来。比如[姚明](../Page/姚明.md "wikilink")、[刘翔和](../Page/刘翔.md "wikilink")[韩寒](../Page/韩寒.md "wikilink")，他们有教养、有个性，目标明确，温和而坚定\[30\]。

## 注释

## 参考文献

## 参见

  - [上海文化](../Page/上海文化.md "wikilink")、[上海话](../Page/上海话.md "wikilink")、[上海城市精神](../Page/上海城市精神.md "wikilink")
  - [上海历史](../Page/上海历史.md "wikilink")、[上海人口](../Page/上海人口.md "wikilink")、[海上宁波人](../Page/海上宁波人.md "wikilink")
  - [上海外来人口问题](../Page/上海外来人口问题.md "wikilink")、[乡下人](../Page/乡下人.md "wikilink")

[上海人](../Category/上海人.md "wikilink")
[Category:上海人口](../Category/上海人口.md "wikilink")

1.  《上海原住民的Y染色体遗传分析》,杨俊、李辉、金建中、金力、卢大儒 - 中央民族大学学报

2.  [中國古典園林史 - 第 107
    頁](https://books.google.com.hk/books?id=WYRiP994rxoC&pg=PA107&lpg=PA107&dq=%E8%A1%A3%E5%86%A0%E5%8D%97%E6%B8%A1+%E7%A7%BB%E6%B0%91%E6%B1%9F%E5%8D%97&source=bl&ots=09gFXz9-Bx&sig=Nhh_-zeryo526aekUAFoVl5AbDc&hl=zh-TW&sa=X&ei=kQpQVZmjOMyF8gWbjYGoAw&redir_esc=y#v=onepage&q=%E8%A1%A3%E5%86%A0%E5%8D%97%E6%B8%A1%20%E7%A7%BB%E6%B0%91%E6%B1%9F%E5%8D%97&f=false)

3.  [南宋江南市场的移民因素 -
    山东大学移民研究所](http://www.ims.sdu.edu.cn/cms/attachment/101103062825.pdf)

4.  [上海镇、上海县、上海县城考录 -
    何惠明](http://www.shtong.gov.cn/newsite/node2/node70393/node70403/node83902/node83904/userobject1ai121430.html)

5.  [《上海民族志》\>\>大事记](http://www.shtong.gov.cn/newsite/node2/node2245/node65931/node65936/index.html)

6.
7.  [《上海地名志》\>\>第二篇 政区地名\>\>第一章 市、区、县\>\>第一节
    市](http://www.shtong.gov.cn/newsite/node2/node2245/node70862/node70868/node70907/node70910/userobject1ai73644.html)


8.
9.  [第一节行政区划 -
    上海市地方志办公室](http://www.shtong.gov.cn/newsite/node2/node2245/node72907/node72912/node72927/node72947/userobject1ai85637.html)

10.
11. [上海香山人与香山文化](http://www.zsnews.cn/News/2006/10/11/597972.shtml)，中山日报
    2006-10-11

12.

13. [被误读的"硬盘"和"YP"——我们到底有没有歧视农民工 -
    东方网](http://ld.eastday.com/l/20110106/u1a843119.html)

14. [论战时民营工厂内迁中的国民政府与企业主 -《抗日战争研究》 2010年第 2期 -
    江满情](http://jds.cass.cn/UploadFiles/zyqk/2010/12/201012021501214710.pdf)


15.

16. [北角又稱「小福建」「小上海」 -
    成報](http://www.singpao.com/xw/yw/201308/t20130804_450514.html)

17.

18.
19.

20.

21.

22.

23.

24. [任评声：“葛朗台贪官”无疑是个伪命题](http://www.kaixian.tv/gd/2015/0410/229299.html)

25.

26.
27.
28.
29.
30.