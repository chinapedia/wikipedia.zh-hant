[St_Georges_Building.jpg](https://zh.wikipedia.org/wiki/File:St_Georges_Building.jpg "fig:St_Georges_Building.jpg")眺望聖佐治大廈\]\]
[St._George's_Building_Office_Lobby_2016.jpg](https://zh.wikipedia.org/wiki/File:St._George's_Building_Office_Lobby_2016.jpg "fig:St._George's_Building_Office_Lobby_2016.jpg")
[HK_Chater_Road_St_George_Building.JPG](https://zh.wikipedia.org/wiki/File:HK_Chater_Road_St_George_Building.JPG "fig:HK_Chater_Road_St_George_Building.JPG")
 **聖佐治大廈**（英文：**St George's
Building**），是[香港甲級商業大廈](../Page/香港.md "wikilink")，位於[中環](../Page/中環.md "wikilink")[雪廠街](../Page/雪廠街.md "wikilink")2號，於1969年落成，樓高25層，擁有中環市景及[維多利亞港海景](../Page/維多利亞港.md "wikilink")。

聖佐治大廈內有數家[嘉道理家族轄下的公司](../Page/嘉道理家族.md "wikilink")，包括[中電控股](../Page/中電控股.md "wikilink")（19樓）、嘉道理父子公司（22樓）、[香港上海大酒店有限公司的寫字樓](../Page/香港上海大酒店有限公司.md "wikilink")、也有嘉道理置業、[半島酒店](../Page/半島酒店.md "wikilink")、[中華航空公司](../Page/中華航空公司.md "wikilink")（已遷至鰂魚涌）、基金公司等的辦事處、投資者賠償有限公司（已遷出），及其他律師事務所。

## 鄰近主要建築

  - [香港文華東方酒店](../Page/香港文華東方酒店.md "wikilink")
  - [香港匯豐銀行總行大廈](../Page/香港匯豐銀行總行大廈.md "wikilink")
  - [皇后像廣場](../Page/皇后像廣場.md "wikilink")
  - [歷山大廈](../Page/歷山大廈.md "wikilink")
  - [遮打大廈](../Page/遮打大廈.md "wikilink")

## 鄰近街道

  - [遮打道](../Page/遮打道.md "wikilink")
  - [雪廠街](../Page/雪廠街.md "wikilink")
  - [干諾道中](../Page/干諾道中.md "wikilink")
  - [德輔道中](../Page/德輔道中.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[中環站](../Page/中環站.md "wikilink")

<!-- end list -->

  - [電車](../Page/香港電車.md "wikilink")

## 站外鏈結

  - 同名建築物1：在[九龍](../Page/九龍.md "wikilink")[窩打老道](../Page/窩打老道.md "wikilink")81號，有一座物業簡稱[聖佐治大廈](../Page/聖佐治大廈_\(九龍\).md "wikilink")（St.
    George Apartments），全名是「加多利山聖佐治大廈」
    [1](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=836374&cy=820504&zm=2&mx=836374&my=820504&ms=2&sx=836374&sy=820504&ss=0&sl=&vm=&lb=&ly=)
  - 同名建築物2：在[九龍](../Page/九龍.md "wikilink")[何文田](../Page/何文田.md "wikilink")[嘉道理道也有一項住宅物業名為](../Page/嘉道理道.md "wikilink")「聖佐治大廈」（St.
    George's
    Court）[2](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=836143&cy=820271&zm=3&mx=836143&my=820271&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)[住宅：聖佐治大廈](http://hk.realestate.yahoo.com/estate.html?c=3474)

[Category:中西區寫字樓 (香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港中環寫字樓](../Category/香港中環寫字樓.md "wikilink")