[IBM_PC_5150.jpg](https://zh.wikipedia.org/wiki/File:IBM_PC_5150.jpg "fig:IBM_PC_5150.jpg")
[Amiga_1000_system_with_sidecar.jpg](https://zh.wikipedia.org/wiki/File:Amiga_1000_system_with_sidecar.jpg "fig:Amiga_1000_system_with_sidecar.jpg")推出的1000型電腦可用外掛右方機箱的方式成為IBM
PC兼容机\]\] **IBM
PC兼容机**（），是指与IBM的PC机兼容的[个人电脑](../Page/个人电脑.md "wikilink")。早期的IBM兼容机主要是基于[x86系列](../Page/x86.md "wikilink")[CPU](../Page/CPU.md "wikilink")，使用[ISA总线](../Page/ISA总线.md "wikilink")，能够运行[PC-DOS](../Page/PC-DOS.md "wikilink")／[MS-DOS系统](../Page/MS-DOS.md "wikilink")。

“IBM
PC兼容机”一般用于指[80486及之前的PC机](../Page/80486.md "wikilink")，之后渐渐代之以“[标准PC](../Page/标准PC.md "wikilink")”的说法。

## 发展历史

1980年代初期，市场上存在大量不同标准的个人电脑，例如[Apple机](../Page/Apple机.md "wikilink")、[TRS-80机](../Page/TRS-80机.md "wikilink")、[日本的](../Page/日本.md "wikilink")[PC-9801机等](../Page/PC-9801.md "wikilink")。1981年8月，IBM推出了[IBM
PC](../Page/IBM_PC.md "wikilink")。
1982年，[IBM公开了](../Page/IBM.md "wikilink")[IBM
PC上除](../Page/IBM_PC.md "wikilink")[BIOS之外的全部技术资料](../Page/BIOS.md "wikilink")，从而形成了PC机的“开放标准”，使不同厂商的标准部件可以互换。开放标准聚拢了大量板卡生产商和整机生产商，大大促进了PC机的产业化发展速度。到1990年代初，个人电脑市场上仅剩下IBM
PC兼容机和[麦金塔电脑](../Page/麦金塔电脑.md "wikilink")（Macintosh）两个主要系列，并且IBM兼容机数量占据了绝对主导地位。随着IBM兼容机的发展，计算能力的大大提高，甚至蚕食了[小型机的市场份额](../Page/小型机.md "wikilink")。

在IBM
PC兼容机逐步成为事实上的PC标准过程中，为[微软](../Page/微软.md "wikilink")、[Intel](../Page/英特尔.md "wikilink")，以及大量兼容机部件商、兼容机厂商提供了市场机会，甚至IBM自己在PC市场上的份额都不是第一位。

随着技术的发展，IBM兼容机经历了XT/AT（[8086](../Page/8086.md "wikilink")）、[80286](../Page/80286.md "wikilink")、[80386](../Page/80386.md "wikilink")、[80486](../Page/80486.md "wikilink")、[奔腾](../Page/奔腾.md "wikilink")（Pentium）等阶段，很多新的内容加入进来，计算机技术人员更倾向称之为[Wintel标准架构](../Page/Wintel.md "wikilink")。

在[奔腾电脑之后](../Page/奔腾.md "wikilink")，由于大量新的PC的技术标准应用，PC的技术标准开始由[IEEE等组织而不是某个厂家来确定](../Page/IEEE.md "wikilink")，“IBM
PC兼容机”的说法逐渐被“[标准PC](../Page/标准PC.md "wikilink")”所取代。

## 外部链接

  - <http://www.allgame.com/cg/agg.dll?p=agg&sql=5:2>

<!-- end list -->

  - <http://oldcomputers.net/compaqi.html>

[lt:PC](../Page/lt:PC.md "wikilink")

[Category:IBM PC兼容机](../Category/IBM_PC兼容机.md "wikilink")
[Category:计算平台](../Category/计算平台.md "wikilink")
[Category:個人電腦](../Category/個人電腦.md "wikilink")