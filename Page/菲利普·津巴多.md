**菲利普·津巴多**（，）是一位[美国](../Page/美国.md "wikilink")[心理学家](../Page/心理学家.md "wikilink")、[斯坦福大学退休教授](../Page/斯坦福大学.md "wikilink")，以[斯坦福监狱实验和编写大学心理学教材而著称](../Page/斯坦福监狱实验.md "wikilink")。

## 早年

1933年3月23日，菲利普·津巴多出生于[美国](../Page/美国.md "wikilink")[纽约市](../Page/纽约市.md "wikilink")。1954年，获得[布鲁克林学院的心理学](../Page/布鲁克林学院.md "wikilink")、社会学和人类学的文学士学位，并且获得最优等[拉丁文学位荣誉](../Page/拉丁文学位荣誉.md "wikilink")（*Summa
Cum
Laude*）。1955年，津巴多获得[耶鲁大学心理学硕士学位](../Page/耶鲁大学.md "wikilink")，1959年又获得该校心理学哲学博士学位，并在耶鲁大学任教。次年，他被[纽约大学聘为心理学教授](../Page/纽约大学.md "wikilink")。1967年，津巴多任教于[哥伦比亚大学](../Page/哥伦比亚大学.md "wikilink")，次年，他转往[斯坦福大学任教](../Page/斯坦福大学.md "wikilink")。

## 斯坦福监狱实验

1971年，津巴多受聘担任斯坦福大学心理学教授。他在那里进行了著名的
[斯坦福监狱实验](../Page/斯坦福监狱实验.md "wikilink")，随意指派24名师范学院学生在位于斯坦福大学心理学系大楼的地下室内的模拟监狱内充当“囚犯”和“看守”（其中6名大学生分别作为囚犯和看守的候补，没有参加实验）。

这些学生很快就进入所扮演的角色，“看守”显示出虐待狂病态人格，而“囚犯”显示出极端[被动和沮丧](../Page/被动.md "wikilink")。囚犯和看守很快适应了自己的角色，一步步地超过了预设的界限，通向危险和造成心理伤害的情形。三分之一的看守被评价为显示出“真正的”虐待狂倾向，而许多囚犯在情感上受到创伤，有2人不得不提前退出实验。为了防止伤害扩大，原计划进行两周的实验进行了一周就不得不提前终止。

斯坦福监狱实验经常被拿来与[米尔格拉姆实验进行比较](../Page/米尔格拉姆实验.md "wikilink")，米尔格拉姆实验是由津巴多中学时代的好友[斯坦利·米尔格拉姆于](../Page/斯坦利·米尔格拉姆.md "wikilink")1961年在[耶鲁大学进行的](../Page/耶鲁大学.md "wikilink")。该实验发现在穿制服的权威人员命令下，路人可以将电击受试人员的电压逐渐提高到非常危险的程度而不提出异议。实验主要目的是研究人们对于权威的服从。

## 其他贡献

斯坦福监狱实验之后，津巴多决定寻找利用心理学帮助人们的途径，于是他在[门洛帕克
(加利福尼亚州)设立了](../Page/门洛帕克_\(加利福尼亚州\).md "wikilink")[害羞诊所](../Page/害羞诊所.md "wikilink")（The
Shyness
Clinic），治疗成人和儿童的害羞。津巴多将其关于害羞的研究成果写成了几本畅销书出售。他研究的其他课题还有[精神控制和](../Page/精神控制.md "wikilink")
[邪教](../Page/邪教.md "wikilink") 行为。\[1\]

津巴多和[石溪大学的](../Page/石溪大学.md "wikilink")[认知心理学教授](../Page/认知心理学.md "wikilink")[理查德·格里格合作编写了心理学教材](../Page/理查德·格里格.md "wikilink")《[心理学与生活](../Page/心理学与生活.md "wikilink")》，这本教材被美国许多大学的心理学课程所采用。他还主持美国[公共电视网](../Page/公共电视网.md "wikilink")
的“探索心理学”（*Discovering
Psychology*），并被许多[大学电视课程](../Page/遥距教育.md "wikilink")\[2\]所采用。

2002年，津巴多当选为[美国心理学会会长](../Page/美国心理学会.md "wikilink")。在他的指导下，这个组织开发了网站
[PsychologyMatters.org](http://www.psychologymatters.org/)，是一个已经应用于日常生活的心理学研究成果的概略。同年，他在一个英国[真人实境秀](../Page/真人实境秀.md "wikilink")
“人类动物园”（*The Human Zoo*）中出现。参与者进入一个受控装置中接受观察，而津巴多和一位英国心理学家负责分析他们的行为。

2004年，津巴多出席美国国防部[军事法庭](../Page/军事法庭.md "wikilink")，为阿布格莱布监狱的一名看守伊万·弗里德里克（Ivan
Frederick）中士作证，他认为鉴于弗里德里克的情况，判刑应该从轻，他解释说很少有人能够抵御监狱强大的情境压力，尤其是在没有适当的训练和监督的情况下。显然法官没有采信津巴多的证词，判处弗里德里克最高刑期：8年徒刑。津巴多运用他在弗里德里克案件中获得的知识写了一本关于阿布格莱布监狱与监狱实验之间的联系的新书《[路西法效应](../Page/路西法效应.md "wikilink")》。\[3\]\[4\]

2006年秋天，津巴多受聘担任[太平洋心理学研究所心理学教授](../Page/太平洋心理学研究所.md "wikilink")，为[临床心理学专业的博士生教授](../Page/临床心理学.md "wikilink")[社会心理学](../Page/社会心理学.md "wikilink")。

津巴多的著作发表在由[伯克利加州大学的Greater](../Page/伯克利加州大学.md "wikilink")
Good科学中心出版的*Greater Good*
杂志上。津巴多的贡献包括解释同情心、利他主义与和平的人类关系的根源的科学研究。他在Greater
Good 杂志上最近发表的文章题为“英雄主义的平凡”（"The Banality of
Heroism"），探讨平凡人如何能成为日常生活中的英雄。

2007年3月7日，已经在2003年正式退休的津巴多,
又在[斯坦福大学校园发表其最后的](../Page/斯坦福大学.md "wikilink")“探究人性”（"Exploring
Human Nature"）演讲，宣告他50年教学生涯的结束。
[斯坦福大学医学院的](../Page/斯坦福大学医学院.md "wikilink")[精神病学教授大卫](../Page/精神病学.md "wikilink")
Spiegel称津巴多为“一位传奇的教师”，说“他改变了我们对于社会影响的思考方式”\[5\]

[2008年2月](../Page/2008年2月.md "wikilink")，津巴多受邀参加[柯伯特报告](../Page/柯伯特报告.md "wikilink")（Colbert
Report）。\[6\]

## 著作

  - *Influencing Attitudes and Changing Behavior*.（影响态度与改变行为） 马萨诸塞州
    Reading：Addison Wesley Publishing Co., 1969年，ISBN 0-07-554809-7
  - *The Cognitive Control of Motivation*.（动机的认知控制） Glenview, IL: Scott,
    Foresman, 1969
  - *Stanford prison experiment: A simulation study of the psychology of
    imprisonment*（斯坦福监狱实验：监狱心理学仿真研究），1972年
  - *The psychology of imprisonment: privation, power and
    pathology*，斯坦福大学，1972年
  - *Canvassing for Peace: A Manual for Volunteers*.
    密歇根州安娜堡：社会问题心理学研究学会，1970年
  - *Cults go to high school: A theoretical and empirical analysis of
    the initial stage in the recruitment process*，美国家庭基金会, 1985
  - *Shyness: What It Is, What to Do About It*（害羞） ，Addison Wesley,
    1990年，ISBN 0-201-55018-0
  - *The Psychology of Attitude Change and Social
    Influence*（态度改变与社会影响的心理学）. 纽约：McGraw-Hill,
    1991年，ISBN 0-87722-852-3
  - *Psychology* (心理学), 第3版，马萨诸塞州 Reading：Addison Wesley Publishing Co.,
    1999年，ISBN 0-321-03432-5
  - *The Shy Child : Overcoming and Preventing Shyness from Infancy to
    Adulthood*, Malor Books, 1999年，ISBN 1-883536-21-9
  - *Violence Workers: Police Torturers and Murderers Reconstruct
    Brazilian Atrocities*. 加州伯克利：伯克利加州大学出版社，2002年，ISBN 0-520-23447-2
  - *Psychology - Core Concepts*, （心理学核心概念）第5版, Allyn & Bacon
    Publishing, 2005年，ISBN 0-205-47445-4
  - *Psychology And Life*（心理学与生活），第17版, Allyn & Bacon Publishing,
    2005年，ISBN 0-205-41799-X
  - *The Lucifer Effect: Understanding How Good People Turn
    Evil*（[路西法效应：好人是如何变成恶魔的](../Page/路西法效应.md "wikilink")），纽约：Random
    House，2007年，ISBN 1-400-06411-2

## 参见

  - [心理学家列表](../Page/心理学家列表.md "wikilink")
  - [米尔格拉姆实验](../Page/米尔格拉姆实验.md "wikilink")

## 参考文献

## 外部連結

  - [Zimbardo's official website](http://www.zimbardo.com)

  - [The Heroic Imagination Project](http://heroicimagination.org/)

  - [Philip G. Zimbardo Papers (Stanford University
    Archives)](http://www.oac.cdlib.org/findaid/ark:/13030/kt7f59s371)

  -
  -
  -
  - [Philip Zimbardo on the Lucifer Effect, in two
    parts](https://archive.is/20121212131836/http://isites.harvard.edu/icb/icb.do?keyword=k13943&pageid=icb.page205767)

[Category:美国心理学家](../Category/美国心理学家.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:西西里裔美國人](../Category/西西里裔美國人.md "wikilink")
[Category:布魯克林學院校友](../Category/布魯克林學院校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:史丹佛大學教師](../Category/史丹佛大學教師.md "wikilink")
[Category:哥倫比亞大學教師](../Category/哥倫比亞大學教師.md "wikilink")
[Category:紐約大學教師](../Category/紐約大學教師.md "wikilink")
[Category:群体心理学家](../Category/群体心理学家.md "wikilink")

1.  [What messages are behind today's
    cults?](http://www.csj.org/studyindex/studycult/study_zimbar.htm),
    APA Monitor, May 1997
2.  见[1](http://www.learner.org/resources/series138.html/learner.org)
3.  [魔鬼效应官方网站](http://www.LuciferEffect.com)
4.  [](http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/books/book_reviews/article1576761.ece)
5.  [Palo Alto News
    profile](http://www.paloaltodailynews.com/article/2007-3-8-03-08-07-pa-zimbardo)
6.  [津巴多在柯伯特报告会上](http://thesituationist.wordpress.com/2008/02/12/video-colbert-report-philip-zimbardo/)