本表列出**[科威特](../Page/科威特.md "wikilink")[埃米尔](../Page/埃米尔.md "wikilink")**。

## 科威特君主列表 (1718–至今)

### 科威特谢赫 (1718–1961)

<table>
<thead>
<tr class="header">
<th><p>頭銜</p></th>
<th><p>姓名</p></th>
<th><p>在位年份</p></th>
<th><p>备忘</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>*谢赫</p></td>
<td><p>萨巴赫一世·宾·贾比尔<br />
(Sabah I bin Jaber)</p></td>
<td><p>1718-1764</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>*谢赫</p></td>
<td><p>阿卜杜拉一世·萨巴赫<br />
(Abdullah I Al-Sabah)</p></td>
<td><p>1764-1814</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>*谢赫</p></td>
<td><p>贾比尔一世·萨巴赫<br />
(Jaber I Al-Sabah)</p></td>
<td><p>1814-1859</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>*谢赫</p></td>
<td><p>萨巴赫二世·萨巴赫<br />
(Sabah II Al-Sabah)</p></td>
<td><p>1859-1866</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>*谢赫</p></td>
<td><p>阿卜杜拉二世·萨巴赫<br />
(Abdullah II Al-Sabah)</p></td>
<td><p>1866-1892</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>*谢赫</p></td>
<td><p>穆罕默德一世·萨巴赫<br />
(Muhammad I Al-Sabah)</p></td>
<td><p>1892-1896</p></td>
<td><p>遇刺身亡</p></td>
</tr>
<tr class="odd">
<td><p>*谢赫</p></td>
<td><p>穆巴拉克·萨巴赫<br />
(Mubarak Al-Sabah)</p></td>
<td><p>1896-1915</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>*谢赫</p></td>
<td><p>贾比尔二世·萨巴赫<br />
(Jaber II Al-Sabah)</p></td>
<td><p>1915-1917</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>*谢赫</p></td>
<td><p>萨利姆一世·穆巴拉克·萨巴赫<br />
(Salim I Al-Mubarak Al-Sabah)</p></td>
<td><p>1917-1921</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>*谢赫</p></td>
<td><p>阿赫马德一世·贾比尔·萨巴赫<br />
(Ahmad I Al-Jaber Al-Sabah)</p></td>
<td><p>1921-1950</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>*谢赫</p></td>
<td><p>阿卜杜拉三世·萨利姆·萨巴赫<br />
(Abdullah III Al-Salim Al-Sabah)</p></td>
<td><p>1950-1965</p></td>
<td><p>1961年称埃米尔</p></td>
</tr>
</tbody>
</table>

### 科威特埃米尔 (1961–至今)

<table>
<thead>
<tr class="header">
<th><p><strong>姓名</strong></p></th>
<th><p><strong>图像</strong></p></th>
<th><p><strong>在位</strong></p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>开始</strong></p></td>
<td><p><strong>结束</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>埃米尔 <a href="../Page/阿卜杜拉三世·萨利姆·萨巴赫.md" title="wikilink">阿卜杜拉三世·萨利姆·萨巴赫</a><br />
(Abdullah III Al-Salim Al-Sabah)</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Emblem_of_Kuwait.svg" title="fig:Emblem_of_Kuwait.svg">Emblem_of_Kuwait.svg</a></p></td>
<td><p>1961年6月19日</p></td>
<td><p>1965年11月24日</p></td>
</tr>
<tr class="odd">
<td><p>埃米尔 <a href="../Page/萨巴赫三世·萨利姆·萨巴赫.md" title="wikilink">萨巴赫三世·萨利姆·萨巴赫</a><br />
(Sabah III Al-Salim Al-Sabah)</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Emblem_of_Kuwait.svg" title="fig:Emblem_of_Kuwait.svg">Emblem_of_Kuwait.svg</a></p></td>
<td><p>1965年11月24日</p></td>
<td><p>1977年12月31日</p></td>
</tr>
<tr class="even">
<td><p>埃米尔 <a href="../Page/贾比尔三世·艾哈迈德·贾比尔·萨巴赫.md" title="wikilink">贾比尔三世·艾哈迈德·贾比尔·萨巴赫</a><br />
(Jaber III Al-Ahmad Al-Jaber Al-Sabah)</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Jabir_al-Ahmad_al-Jabir_Al_Sabah_1998.jpg" title="fig:Jabir_al-Ahmad_al-Jabir_Al_Sabah_1998.jpg">Jabir_al-Ahmad_al-Jabir_Al_Sabah_1998.jpg</a></p></td>
<td><p>1977年12月31日</p></td>
<td><p>2006年1月15日</p></td>
</tr>
<tr class="odd">
<td><p>埃米尔 <a href="../Page/萨阿德·阿卜杜拉·萨利姆·萨巴赫.md" title="wikilink">萨阿德一世·阿卜杜拉·萨利姆·萨巴赫</a><br />
(Saad I Al-Abdullah Al-Salim Al-Sabah)</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Emblem_of_Kuwait.svg" title="fig:Emblem_of_Kuwait.svg">Emblem_of_Kuwait.svg</a></p></td>
<td><p>2006年1月15日</p></td>
<td><p>2006年1月24日</p></td>
</tr>
<tr class="even">
<td><p>埃米尔 <a href="../Page/萨巴赫·艾哈迈德·贾比尔·萨巴赫.md" title="wikilink">萨巴赫四世·艾哈迈德·贾比尔·萨巴赫</a><br />
(Sabah IV Al-Ahmad Al-Jaber Al-Sabah)</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sheikh_Sabah_IV.jpg" title="fig:Sheikh_Sabah_IV.jpg">Sheikh_Sabah_IV.jpg</a></p></td>
<td><p>2006年1月24日</p></td>
<td><p>至今</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参见

  - [萨巴赫王朝](../Page/萨巴赫王朝.md "wikilink")

## 外部链接

  - [List of Kuwait Rulers & Amirs at Amiri Diwan of Kuwait
    website](http://www.da.gov.kw/eng/picsandevents/rulers.php)

[Category:科威特埃米尔](../Category/科威特埃米尔.md "wikilink")
[Category:領袖列表](../Category/領袖列表.md "wikilink")
[Category:科威特列表](../Category/科威特列表.md "wikilink")