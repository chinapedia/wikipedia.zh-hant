**上海市控江中学**（，簡稱：**控江中学**、**控江**），位于[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")[杨浦区](../Page/杨浦区.md "wikilink")[双阳路](../Page/双阳路.md "wikilink")388号\[1\]
，创建于1953年，是上海市首批市级[重点中学](../Page/重点中学.md "wikilink")，亦是首批[上海市实验性、示范性高中](../Page/上海市实验性、示范性高中.md "wikilink")。\[2\]

## 参考资料

<references />

[S](../Category/上海中等教育.md "wikilink")
[Category:杨浦区教育](../Category/杨浦区教育.md "wikilink")
[Category:1953年創建的教育機構](../Category/1953年創建的教育機構.md "wikilink")

1.
2.