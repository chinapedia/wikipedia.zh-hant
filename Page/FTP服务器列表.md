以下是[FTP服务器的列表](../Page/文件传输协议.md "wikilink")。

## 图形界面

  - [Bullet Proof FTP
    Server](../Page/Bullet_Proof_FTP_Server.md "wikilink")
    [1](http://www.bpftpserver.com/)

  - [CesarFTP](../Page/CesarFTP.md "wikilink")
    [2](http://webarchive.loc.gov/all/20020813115226/http%3A//www.aclogic.com/)

  - [3](http://www.cerberusftp.com/)

  - [Gene6 FTP Server](../Page/Gene6_FTP_Server.md "wikilink")
    [4](http://www.g6ftpserver.com/)

  - [GuildFTPd](../Page/GuildFTPd.md "wikilink")
    [5](http://www.guildftpd.com/)

  - [RaidenFTPD](../Page/RaidenFTPD.md "wikilink")
    [6](https://web.archive.org/web/20050318060531/http://raidenftpd.com/)

  - [Serv-U FTP Server](../Page/Serv-U_FTP_Server.md "wikilink")
    [7](http://www.serv-u.com/)

  - [8](https://web.archive.org/web/20060411110306/http://www.warftp.org/)

  - [WS_FTP Server](../Page/WS_FTP_Server.md "wikilink")
    [9](https://www.ipswitch.com/ftp-server/)

## 文字界面

  - [AnomicFTPD](../Page/AnomicFTPD.md "wikilink")
    [10](https://web.archive.org/web/20050306214851/http://www.anomic.de/AnomicFTPServer/)

  - [BSD ftpd](../Page/BSD_ftpd.md "wikilink")

  - [11](http://www.glftpd.com/)

  - [ProFTPd](../Page/ProFTPd.md "wikilink")

  - [12](http://www.pureftpd.org/)

  - [13](http://vsftpd.beasts.org/)

  - [14](https://web.archive.org/web/19991129022519/http://www.wu-ftpd.org/)

## 参见

  - [FTP服务器比较](../Page/FTP服务器比较.md "wikilink")

[FTP服务器](../Category/FTP服务器.md "wikilink")
[Category:软件列表](../Category/软件列表.md "wikilink")