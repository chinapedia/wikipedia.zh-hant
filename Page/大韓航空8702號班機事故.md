**大韓航空8702號班機事故**發生於1998年8月5日，肇事飛機為一架[波音747-4B5](../Page/波音747.md "wikilink")，編號HL7496，原定由[東京](../Page/東京.md "wikilink")[成田機場飛往](../Page/成田機場.md "wikilink")[首爾](../Page/首爾.md "wikilink")[金浦國際機場](../Page/金浦国际機場.md "wikilink")。

## 事件經過

（以下時間全部以日本和韓國時間為準，即GMT+0900）

1998年8月5日，肇事客機於16時50分從[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[成田機場起飛](../Page/成田機場.md "wikilink")，原定於19時20分抵達[南韓](../Page/南韓.md "wikilink")[首爾](../Page/首爾.md "wikilink")[金浦國際機場](../Page/金浦国际机场.md "wikilink")。但當天首爾市的天氣相當惡劣，有大雨及狂風雷暴，迫使肇事客機改飛[濟州](../Page/濟州.md "wikilink")。

航機於21時07分在濟州起飛繼續行程，約一小時後在[金浦國際機場降落](../Page/金浦国际机场.md "wikilink")。肇事客機在降落時懷疑因跑道面濕滑，全機衝離跑道，在跑道盡頭100米外在一處草地上才停下。由於衝力強大，肇事客機其中一邊機翼折斷，幸好並沒有發生爆炸。機上379名乘客及16名機員中，有25人受傷。

## 其他

肇事的[747客機在](../Page/波音747.md "wikilink")1996年6月投入服務，即是事發時機齡只有2年。由於肇事客機機身嚴重受損，加上一隻機翼折斷，無法修復，故即時退役。HL7496成為了全球第2架報銷的波音747-400客機。（第1架是[中華航空的](../Page/中華航空.md "wikilink")[B-165](../Page/中華航空605號班機.md "wikilink")，1993年退役。）

## 類似事故

  - [中華航空605號班機事故](../Page/中華航空605號班機事故.md "wikilink")
  - [法國航空358號班機事故](../Page/法國航空358號班機事故.md "wikilink")
  - [巴西天馬航空3054號班機空難](../Page/巴西天馬航空3054號班機空難.md "wikilink")
  - [澳洲航空1號班機事故](../Page/澳洲航空1號班機事故.md "wikilink")

## 外部連結

  - [Aviation-Safety網站之相關資料](http://aviation-safety.net/database/record.php?id=19980805-0)

[Category:1998年航空事故](../Category/1998年航空事故.md "wikilink")
[Category:韓國航空事故](../Category/韓國航空事故.md "wikilink")
[Category:天氣因素觸發的航空事故](../Category/天氣因素觸發的航空事故.md "wikilink")
[Category:1998年韩国](../Category/1998年韩国.md "wikilink")
[8702](../Category/大韓航空公司航空事故.md "wikilink")
[Category:1998年8月](../Category/1998年8月.md "wikilink")
[Category:波音747航空事故](../Category/波音747航空事故.md "wikilink")