[Novodevichy_cemetery.jpg](https://zh.wikipedia.org/wiki/File:Novodevichy_cemetery.jpg "fig:Novodevichy_cemetery.jpg")
[Grave_of_Anton_Chekhov.jpg](https://zh.wikipedia.org/wiki/File:Grave_of_Anton_Chekhov.jpg "fig:Grave_of_Anton_Chekhov.jpg")墓\]\]
**新圣女公墓**（）是[俄罗斯首都](../Page/俄罗斯.md "wikilink")[莫斯科最为著名的公墓](../Page/莫斯科.md "wikilink")，主要安葬俄罗斯作家、剧作家、诗人、著名演员、政治领导人和科学家等，目前共有27,000多人。

## 知名人士

  - [谢尔盖·阿克萨科夫](../Page/谢尔盖·阿克萨科夫.md "wikilink")（1791-1859）：作家
  - [娜杰日达·阿利卢耶娃-斯大林娜](../Page/娜杰日达·阿利卢耶娃-斯大林娜.md "wikilink")（1901-1932）：[约瑟夫·斯大林妻子](../Page/约瑟夫·斯大林.md "wikilink")
  - [丹尼尔·安德烈耶夫](../Page/丹尼尔·安德烈耶夫.md "wikilink")（1906-1959）：作家
  - [安德烈·别利](../Page/安德烈·别利.md "wikilink")（1880-1934）：作家
  - [帕维尔·别利亚耶夫](../Page/帕维尔·别利亚耶夫.md "wikilink")（1925-1970）：宇航员
  - [格奥尔基·别列戈沃伊](../Page/格奥尔基·别列戈沃伊.md "wikilink")（1921-1995）：宇航员
  - [谢尔盖·邦达尔丘克](../Page/谢尔盖·邦达尔丘克.md "wikilink")（1920-1994）：演员、导演
  - [瓦列里·勃留索夫](../Page/瓦列里·勃留索夫.md "wikilink")（1873-1924）：作家
  - [米哈伊尔·布尔加科夫](../Page/米哈伊尔·布尔加科夫.md "wikilink")（1881-1940）：剧作家
  - [尼古拉·布尔加宁](../Page/尼古拉·布尔加宁.md "wikilink")（1895-1975）：苏联部长会议主席
  - [尼古拉·布尔坚科](../Page/尼古拉·布尔坚科.md "wikilink")（1876-1946）：俄罗斯神经外科奠基人
  - [帕维尔·切连科夫](../Page/帕维尔·切连科夫.md "wikilink")（1904-1990）：诺贝尔物理学家获得者
  - [伊万·切尔尼亚霍夫斯基](../Page/伊万·切尔尼亚霍夫斯基.md "wikilink")（1906-1945）：苏联红军大将
  - [费奥多尔·夏里亚宾](../Page/费奥多尔·夏里亚宾.md "wikilink")（1873-1938）：歌剧演唱家
  - [格奥尔基·奇切林](../Page/格奥尔基·奇切林.md "wikilink")（1872-1936）：苏联外交部长
  - [伊萨克·杜纳耶夫斯基](../Page/伊萨克·杜纳耶夫斯基.md "wikilink")（1900-1955）：作曲家、指挥家
  - [伊利亚·爱伦堡](../Page/伊利亚·爱伦堡.md "wikilink")（1891-1967）：作家
  - [谢尔盖·艾森斯坦](../Page/谢尔盖·艾森斯坦.md "wikilink")（1898年-1948年）：电影导演
  - [亚历山大·法捷耶夫](../Page/亚历山大·法捷耶夫.md "wikilink")（1901-1956）：作家
  - [赖因霍尔德·格里爱尔](../Page/赖因霍尔德·格里爱尔.md "wikilink")（1875-1956）：作曲家
  - [瓦伦丁·格卢什科](../Page/瓦伦丁·格卢什科.md "wikilink")（1908-1989）：苏联三大宇宙飞船和火箭设计师之一
  - [尼古拉·果戈里](../Page/尼古拉·果戈里.md "wikilink")（1809-1852）：作家
  - [赖莎·戈尔巴乔娃](../Page/赖莎·戈尔巴乔娃.md "wikilink")（1932-1999）：[米哈伊尔·戈尔巴乔夫妻子](../Page/米哈伊尔·戈尔巴乔夫.md "wikilink")
  - [安德烈·葛罗米柯](../Page/安德烈·葛罗米柯.md "wikilink")（1909-1989）：苏联外交部长
  - [谢尔盖·伊留申](../Page/谢尔盖·伊留申.md "wikilink")（1894-1977）：飞机设计师
  - [德米特里·卡巴列夫斯基](../Page/德米特里·卡巴列夫斯基.md "wikilink")（1904-1987）：作曲家
  - [拉扎尔·卡冈诺维奇](../Page/拉扎尔·卡冈诺维奇.md "wikilink")（1892-1991）：政治家
  - [列昂尼德·坎托罗维奇](../Page/列昂尼德·坎托罗维奇.md "wikilink")（1912-1986）：诺贝尔经济学奖获得者
  - [韦利米尔·赫列布尼科夫](../Page/韦利米尔·赫列布尼科夫.md "wikilink")（1885-1922）：诗人
  - [尼基塔·赫鲁晓夫](../Page/尼基塔·赫鲁晓夫.md "wikilink")（1894-1971）：苏联共产党中央委员会第一书记，部长会议主席
  - [亚历山德拉·柯伦泰](../Page/亚历山德拉·柯伦泰.md "wikilink")（1872-1952）：政治家、世界第一位女性外交官
  - [伊万·阔日杜布](../Page/伊万·阔日杜布.md "wikilink")（1920-1991）：飞行员
  - [彼得·克鲁泡特金](../Page/彼得·克鲁泡特金.md "wikilink")（1842-1921）：地理学家、无政府主义者
  - [列夫·朗道](../Page/列夫·朗道.md "wikilink")（1908-1968）：诺贝尔物理学奖获得者
  - [亚历山大·列别德](../Page/亚历山大·列别德.md "wikilink")（1950-2002）：政治家
  - [伊萨克·列维坦](../Page/伊萨克·列维坦.md "wikilink")（1860-1900）：画家
  - [马克西姆·李维诺夫](../Page/马克西姆·李维诺夫.md "wikilink")（1876-1951）：政治家
  - [弗拉基米尔·马雅可夫斯基](../Page/弗拉基米尔·马雅可夫斯基.md "wikilink")（1893-1930）：诗人
  - [阿纳斯塔斯·米高扬](../Page/阿纳斯塔斯·米高扬.md "wikilink")（1895-1978）：政治家
  - [维亚切斯拉夫·莫洛托夫](../Page/维亚切斯拉夫·莫洛托夫.md "wikilink")（1890-1986）：政治家
  - [尤里·尼库林](../Page/尤里·尼库林.md "wikilink")（1921-1997）：演员
  - [达维德·奥伊斯特拉赫](../Page/达维德·奥伊斯特拉赫.md "wikilink")（1908-1974）：小提琴家
  - [尼古拉·奥斯特洛夫斯基](../Page/尼古拉·奥斯特洛夫斯基.md "wikilink")（1904-1936）：作家
  - [亚历山大·奥巴林](../Page/亚历山大·奥巴林.md "wikilink")（1894-1980）：科学家
  - [尼古拉·波德戈尔内](../Page/尼古拉·波德戈尔内.md "wikilink")（1903-1983）：政治家
  - [亚历山大·伊万诺维奇·波克雷什金](../Page/亚历山大·伊万诺维奇·波克雷什金.md "wikilink")（1913-1985）：空军元帅
  - [谢尔盖·普罗科菲耶夫](../Page/谢尔盖·普罗科菲耶夫.md "wikilink")（1891-1953）：作曲家
  - [斯维亚托斯拉夫·里希特](../Page/斯维亚托斯拉夫·里希特.md "wikilink")（1915-1997）：钢琴演奏家
  - [阿尔弗雷德·施尼特凯](../Page/阿尔弗雷德·施尼特凯.md "wikilink")（1934-1998）：作曲家
  - [亚历山大·斯克里亚宾](../Page/亚历山大·斯克里亚宾.md "wikilink")（1872-1915）：作曲家
  - [瓦伦丁·谢罗夫](../Page/瓦伦丁·谢罗夫.md "wikilink")（1865-1911）：作家、艺术家
  - [德米特里·肖斯塔科维奇](../Page/德米特里·肖斯塔科维奇.md "wikilink")（1906-1975）：作曲家
  - [弗拉基米尔·舒霍夫](../Page/弗拉基米尔·舒霍夫.md "wikilink")（1853-1939）：工程师
  - [弗拉基米尔·索洛维约夫](../Page/弗拉基米尔·索洛维约夫.md "wikilink")（1853-1900）：哲学家
  - [康斯坦丁·斯坦尼斯拉夫斯基](../Page/康斯坦丁·斯坦尼斯拉夫斯基.md "wikilink")（1863-1938）：剧作家
  - [谢尔盖·塔涅耶夫](../Page/谢尔盖·塔涅耶夫.md "wikilink")（1856-1915）：作曲家
  - [弗拉基米尔·塔特林](../Page/弗拉基米尔·塔特林.md "wikilink")（1885-1953）：画家
  - [尼古拉·吉洪诺夫](../Page/尼古拉·吉洪诺夫.md "wikilink")（1905-1997）：政治家
  - [卓娅·科斯莫杰米扬斯卡娅](../Page/卓娅·科斯莫杰米扬斯卡娅.md "wikilink")（1923－1941）：苏联英雄，烈士。
  - [盖尔曼·蒂托夫](../Page/盖尔曼·蒂托夫.md "wikilink")（1935-2000）：宇航员
  - [安德烈·图波列夫](../Page/安德烈·图波列夫.md "wikilink")（1888-1972）：飞机设计师
  - [加林娜·乌兰诺娃](../Page/加林娜·乌兰诺娃.md "wikilink")（1909-1998）：芭蕾舞演员
  - [弗拉基米尔·韦尔纳茨基](../Page/弗拉基米尔·韦尔纳茨基.md "wikilink")（1863-1945）：地理学家
  - [济加·韦尔托夫](../Page/济加·韦尔托夫.md "wikilink")（1896-1954）：电影制片人
  - [鲍里斯·叶利钦](../Page/鲍里斯·叶利钦.md "wikilink")（1931-2007）：俄罗斯联邦第一任总统
  - [维克托·切尔诺梅尔金](../Page/维克托·切尔诺梅尔金.md "wikilink")（1938-2010）：俄罗斯联邦第一任总理
  - [王明](../Page/王明.md "wikilink")（1904－1974）：中國共產黨前期主要領導人
  - [孟慶樹](../Page/孟慶樹.md "wikilink")（1911－1983）：中國共產黨前期主要領導人，[王明妻子](../Page/王明.md "wikilink")
  - [謝苗·拉沃契金](../Page/謝苗·拉沃契金.md "wikilink")（1900－1960）：飛機設計師

## 外部連結

  - [Unofficial site. Hi-resolution photos](http://novodevichye.com/)
  - [Photographs](http://northstargallery.com/pages/NovaGal1.htm)
  - [More photographs](http://www.requiem.ru/photos/novodev/)
  - [Famous and picturesque (24) memorials photographed
    June 2005](http://www.kingdouglas.com/Novodevichy/)
  - [Novodevichy Cemetery](http://www.oval.ru/enc/48107.html) –article
    from the [Great Soviet Encyclopedia](../Page/苏联大百科全书.md "wikilink")

[Category:莫斯科墓葬](../Category/莫斯科墓葬.md "wikilink")