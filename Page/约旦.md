**约旦哈希姆王国**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：）
通稱**約旦**，是位於[西亞的](../Page/西亞.md "wikilink")[中东的国家](../Page/中东.md "wikilink")，它北临[叙利亚](../Page/叙利亚.md "wikilink")，东临[伊拉克](../Page/伊拉克.md "wikilink")，东南临[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")，西临[以色列和](../Page/以色列.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")。

## 历史

[Petra_Jordan_BW_21.JPG](https://zh.wikipedia.org/wiki/File:Petra_Jordan_BW_21.JPG "fig:Petra_Jordan_BW_21.JPG")。\]\]

### 古约旦

约旦作为独立国家的根源可以追溯到[納巴特王國](../Page/納巴特王國.md "wikilink")，这是由[纳巴泰人所建立的](../Page/纳巴泰人.md "wikilink")。他们宽松的贸易管理网络以一連串的绿洲为中心点，但中心点没有足够范围作集体农业，而绿洲间的通路在荒漠周遭毫无防线。

在今天约旦的国土上，人类在数千年前就已经建立了古文明。约旦原是巴勒斯坦的一部分。先后為[亚述](../Page/亚述.md "wikilink")、[巴比伦](../Page/巴比伦.md "wikilink")、[波斯和](../Page/波斯.md "wikilink")[马其顿所统治](../Page/马其顿.md "wikilink")；7世纪属[阿拉伯帝国版图](../Page/阿拉伯帝国.md "wikilink")；16世纪归属[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")。

### 现代约旦

[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，约旦由[英国代国际联盟管理](../Page/英国.md "wikilink")。1921年，英国以[约旦河为边界](../Page/约旦河.md "wikilink")，把巴勒斯坦分为东西两部分，西部仍称巴勒斯坦，东部称外约旦。英国於東部设立了一个半自主的苏旦国，立[汉志国王](../Page/汉志.md "wikilink")[侯赛因次子](../Page/侯赛因_\(汉志\).md "wikilink")[阿卜杜拉一世为](../Page/阿卜杜拉一世_\(约旦\).md "wikilink")[外约旦](../Page/外约旦.md "wikilink")[酋长国酋长](../Page/酋长国.md "wikilink")。1946年5月25日，约旦获得独立，阿卜杜拉登基为王（[埃米尔](../Page/埃米尔.md "wikilink")，即国家元首），国名为外约旦哈希姆王国；1950年4月，约旦河西岸和东岸合并称为约旦哈希姆王国。

约旦支持[巴勒斯坦人](../Page/巴勒斯坦人.md "wikilink")，反对[以色列建国](../Page/以色列.md "wikilink")。它参加[阿拉伯国家联合对以色列的战争](../Page/中東戰爭.md "wikilink")。在阿拉伯国家战败后，约旦获得了对[西岸的控制](../Page/西岸.md "wikilink")。1968年约旦又与[埃及](../Page/埃及.md "wikilink")、叙利亚和伊拉克一起对以作战。以色列占领了西岸与整个[耶路撒冷](../Page/耶路撒冷.md "wikilink")。在两次战争后，都有大量巴勒斯坦难民逃至约旦，使巴勒斯坦人在约旦的势力大为增加。此势力的增加，使约旦王室非常不安。1970年，在[巴勒斯坦解放组织](../Page/巴勒斯坦解放组织.md "wikilink")[多次劫机后](../Page/道森機場劫機事件.md "wikilink")，约旦开始对他们开火，这影响到全阿拉伯世界。叙利亚在约旦的北部边境上，集中坦克部队以对约旦施加压力。1970年9月22日，在开罗的阿拉伯国家外交部长会议的调停下，双方熄火，但此后仍有小战斗持續发生。一直到1971年7月，约旦将[巴勒斯坦解放组织逐出其国境](../Page/巴勒斯坦解放组织.md "wikilink")。

在1973年的[贖罪日戰爭中](../Page/贖罪日戰爭.md "wikilink")，约以边境上没有战火。但约旦派出一个旅的兵力进入叙利亚，抵禦那里的以色列军队。约旦没有介入1991年的[海湾战争](../Page/海湾战争.md "wikilink")。

## 地理

約旦面積89342平方公里，北鄰敘利亞、東毗伊拉克，南接沙烏地阿拉伯，西與以色列 (約人稱之為巴勒斯坦)
為鄰。\[1\]大部分地區是[高原](../Page/高原.md "wikilink")，海拔650－1000米。西部有约旦河谷，東和東南部是沙漠，被[亞巴琳山脈貫穿](../Page/亞巴琳.md "wikilink")。全國可耕地仅占全国面积的4%。在[亚喀巴湾](../Page/亚喀巴湾.md "wikilink")，约旦有一小段海岸线。[死海位于约旦的西部](../Page/死海.md "wikilink")，是其与以色列的边境的一部分。最高峰達1854公尺，死海最低處低於海平面408公尺，為地球上最低窪的地方\[2\]。

### 气候

大部分地区属[亚热带沙漠气候](../Page/亚热带沙漠气候.md "wikilink")，西部山地属[地中海气候](../Page/地中海气候.md "wikilink")；年降水量100－700毫米。

## 軍事

[JORDANIAN_WM-120.jpg](https://zh.wikipedia.org/wiki/File:JORDANIAN_WM-120.jpg "fig:JORDANIAN_WM-120.jpg")\]\]
[Two_F-16_of_the_Royal_Jordanian_Air_Force.jpg](https://zh.wikipedia.org/wiki/File:Two_F-16_of_the_Royal_Jordanian_Air_Force.jpg "fig:Two_F-16_of_the_Royal_Jordanian_Air_Force.jpg")\]\]
約旦軍屬於中東的一支中型[遜尼派武裝力量約](../Page/遜尼派.md "wikilink")10萬人，敏感的地理位置和特殊的宗教國體歷史，使其軍隊雖然不具有戰略攻擊型實力，卻是各國都有所顧慮的力量。

在複雜的中東局勢中，约旦採取微妙的國防姿態，與西方和以色列交好，但同時與遜尼派的[沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")、[什葉派的](../Page/什葉派.md "wikilink")[伊朗也維持相對較好關係](../Page/伊朗.md "wikilink")，21世紀後和崛起但遙遠的的中國也意圖加強戰略夥伴關係，其此種政策的好處在於能得到多國的武器軍購並降低敵意，難處在於多方平衡的手腕，但是敏感的地理位置是其最大資產，讓它能做最大發揮。\[3\]

在[伊斯蘭國崛起後](../Page/伊斯蘭國.md "wikilink")，該組織訴求的[哈里發問題中](../Page/哈里發.md "wikilink")，從族譜世系表來看目前的约旦王族最有資格自稱哈里發後裔和繼承者，正當性高過沙烏地阿拉伯王室，因此在打擊伊斯蘭國上使约旦又占據了正當性，成為西方和遜尼諸國意圖交好的的對象。\[4\]\[5\]

[Pétra_24.jpg](https://zh.wikipedia.org/wiki/File:Pétra_24.jpg "fig:Pétra_24.jpg")

## 政治

約旦為[君主立憲國家](../Page/君主立憲.md "wikilink")，[國王擁有實權](../Page/約旦國王.md "wikilink")，總攬軍、政及外交大權，擁有任命總理，召集、解散國會之權力，上議院議員由國王任命，下議院議員民選。约旦独立后的大多数时间裏，[约旦國王是](../Page/约旦國王.md "wikilink")[哈希姆家族的](../Page/哈希姆家族.md "wikilink")[侯赛因国王](../Page/侯赛因·宾·塔拉勒.md "wikilink")，為[伊斯蘭教創始人](../Page/伊斯蘭教.md "wikilink")[穆罕默德的後裔](../Page/穆罕默德.md "wikilink")。現任國王[阿卜杜拉二世為前國王](../Page/阿卜杜拉二世.md "wikilink")[侯塞因的長子](../Page/侯賽因·賓·塔拉勒.md "wikilink")，於1999年繼位。\[6\]

约旦地处一个政治上非常不稳定的地方，许多政治力量在这裏逐鹿：[美国](../Page/美国.md "wikilink")、[苏联](../Page/苏联.md "wikilink")、[英国](../Page/英国.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、各个阿拉伯国家以及約旦國内数量眾多的巴勒斯坦[移民](../Page/移民.md "wikilink")。在眾多政治力量和兴趣中，侯塞因试图寻找一条可行的路，他溫和的外交政策保障了约旦的主权和统一。虽然[中东諸國在这些时间里](../Page/中东.md "wikilink")，经历了多次战争和动荡，约旦社会都基本上保持稳定。侯赛因本人是多次[刺杀的对象](../Page/刺杀.md "wikilink")。1989年他恢复了约旦的[国会选举](../Page/国会.md "wikilink")；1994年，他签署了与[以色列的和平条约](../Page/以色列.md "wikilink")。

据约旦现行法律，国会议员虽通过选举产生，但是倾向于支持政府，成员包括效忠于国王的部落人员。

### 憲政改革

在[茉莉花革命的影響下](../Page/茉莉花革命.md "wikilink")，加上當時約旦約四分之一人口處於貧窮線下，失業率達百分之十四。[穆斯林兄弟會號召約旦民眾站起來](../Page/穆斯林兄弟會.md "wikilink")，力爭政治和經濟改革，期望由選舉產生新政府。2011年1月29日，在安曼街頭約五千名示威群眾遊行，並手持約旦國旗；約二千名民眾響應了穆斯林兄弟會的號召，在其他城市抗議。2011年2月，約旦國王，解散由首相[萨米尔·里法伊領導的政府](../Page/萨米尔·里法伊.md "wikilink")，並要求前首相[马鲁夫·巴希特組建新的政府內閣](../Page/马鲁夫·巴希特.md "wikilink")，立即開始「真正」的政治改革。
\[7\]

2011年6月，國王阿卜杜拉在电视演讲中表示支持改革选举法，即未来的内阁将由经选举出来的议会多数派组建，而不是由国王直接委任。他说：“我希望更新后的选举法令，将确保国会代表全约旦人民。”\[8\]2016年2月23日，約旦下議院經過6場馬拉松式會期，通過2015年國會選舉法，完成約旦3主要政治改革法（包括政黨法、地方自治法暨市政局法），並將國會眾議院席次自150席減至130席。選舉法中重要者為選制變革，將目前所行「兩票制」（一票投選區候選人，另一票投全國選區政團名單）改為「單一選區」及「比例代表制」之混合制（選區推出政團候選人名單，選民對支持政團候選人投下一票）\[9\]。

### 对外政策

[Abdullah_II.jpg](https://zh.wikipedia.org/wiki/File:Abdullah_II.jpg "fig:Abdullah_II.jpg")\]\]
约旦向來奉行著溫和的外交政策，基本上與[西方國家有著密切的关系](../Page/西方國家.md "wikilink")，尤其與[英国和](../Page/英国.md "wikilink")[美国](../Page/美国.md "wikilink")。[海湾战争时](../Page/海湾战争.md "wikilink")，由於约旦继续支持[伊拉克](../Page/伊拉克.md "wikilink")，使它和西方的关系稍微弱化，但是至今为止，美国是提供给约旦援助最大的国家。1994年，约旦和以色列达成了[和平协议](../Page/和约.md "wikilink")；在[阿拉伯国家中](../Page/阿拉伯国家.md "wikilink")，只有它和[埃及對](../Page/埃及.md "wikilink")[以色列分別締結了和平协议](../Page/以色列.md "wikilink")。[伊拉克战争时](../Page/伊拉克战争.md "wikilink")，[萨达姆政权崩溃以后](../Page/萨达姆政权.md "wikilink")，约旦積極扮演著关键的溝通角色，对伊拉克的稳定和安全貢獻甚多。

## 行政划分

[Jo-map.png](https://zh.wikipedia.org/wiki/File:Jo-map.png "fig:Jo-map.png")

约旦分一十二个省，省长由国王任命。

| 省                                    | 人口（2008统计）\[10\] | 首府                                 | 人口（城市，2008统计）\[11\] |
| ------------------------------------ | ---------------- | ---------------------------------- | ------------------- |
| [阿傑隆省](../Page/阿傑隆省.md "wikilink")   | 118,496          | [阿傑隆](../Page/阿傑隆.md "wikilink")   | 8,161               |
| [安曼省](../Page/安曼省.md "wikilink")     | 1,939,405        | [安曼](../Page/安曼.md "wikilink")     | 1,135,733           |
| [亚喀巴省](../Page/亚喀巴省.md "wikilink")   | 107,115          | [亚喀巴](../Page/亚喀巴.md "wikilink")   | 95,408              |
| [拜勒加省](../Page/拜勒加省.md "wikilink")   | 349,580          | [萨勒特](../Page/萨勒特.md "wikilink")   | 87,778              |
| [伊爾比德省](../Page/伊爾比德省.md "wikilink") | 950,700          | [伊爾比德](../Page/伊爾比德.md "wikilink") | 650,000             |
| [杰拉什省](../Page/杰拉什省.md "wikilink")   | 156,680          | [杰拉什](../Page/杰拉什.md "wikilink")   | 39,540              |
| [卡拉克省](../Page/卡拉克省.md "wikilink")   | 214,225          | [卡拉克](../Page/卡拉克.md "wikilink")   | 22,580              |
| [马安省](../Page/马安省.md "wikilink")     | 103,920          | [马安](../Page/马安.md "wikilink")     | 30,050              |
| [马代巴省](../Page/马代巴省.md "wikilink")   | 135,890          | [马代巴](../Page/马代巴.md "wikilink")   | 83,180              |
| [马弗拉克省](../Page/马弗拉克省.md "wikilink") | 245,670          | [马弗拉克](../Page/马弗拉克.md "wikilink") | 56,340              |
| [塔菲拉省](../Page/塔菲拉省.md "wikilink")   | 81,000           | [塔菲拉](../Page/塔菲拉.md "wikilink")   |                     |
| [扎尔卡省](../Page/扎尔卡省.md "wikilink")   | 838,250          | [扎尔卡](../Page/扎尔卡.md "wikilink")   | 447,880             |

每省被分成52街道（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：ناحية，""）。

## 经济

[Baqa.jpg](https://zh.wikipedia.org/wiki/File:Baqa.jpg "fig:Baqa.jpg")
[Jabal_al_ganobe-_Russeifa4.jpg](https://zh.wikipedia.org/wiki/File:Jabal_al_ganobe-_Russeifa4.jpg "fig:Jabal_al_ganobe-_Russeifa4.jpg")

约旦全国缺水，同時缺乏[石油之类的自然矿产品](../Page/石油.md "wikilink")，因此關稅為政府重要的財源。安曼為其商業中心，阿卡巴港為唯一進出口港，主要商品仍賴進口，對奢侈品、菸酒、電氣用品電及汽車課徵重稅，以收寓禁於徵之效。約旦每年進口金額約為出口金額的3倍。為照顧低收入民眾，約旦政府對米、麵、糖及汽(重)油等基本民生必需品採補貼政策。\[12\]

1991年的[第一次海湾战争给约旦经济带来了极大困难](../Page/第一次海湾战争.md "wikilink")。约旦被迫暫停偿还债务。约旦国内的大批巴勒斯坦难民、其它海湾国家对约旦的资助、去他国工作的民工，都给约旦的经济带来了极大的混乱，造成全国经济非常不稳定而复杂。

阿卜杜拉二世国王试图对国家经济进行有限的改革，其中包括将一些国营企业私有化，改善投资环境，积极寻求外援，扭转了约经济长期负增长或零增长的局面。1999年约加入世界贸易组织。2004–2008年间经济增长率超过8%。债务、贫困和[失业依然是约旦最大的经济困难](../Page/失业.md "wikilink")。\[13\]

## 人口

约旦人口中约93%是[阿拉伯人](../Page/阿拉伯人.md "wikilink")，其他民族包括[高加索人](../Page/高加索人.md "wikilink")、[希腊人和](../Page/希腊人.md "wikilink")[亚述人](../Page/亚述人.md "wikilink")（含古[阿拉米人后裔](../Page/阿拉米人.md "wikilink")）等。

约旦是以伊斯兰教为主的国家。目前全国约90%人口为[逊尼派穆斯林](../Page/逊尼派.md "wikilink")，8%为[基督徒](../Page/基督徒.md "wikilink")。其余为[什叶派穆斯林](../Page/什叶派.md "wikilink")，包括一些[德鲁兹派](../Page/德鲁兹派.md "wikilink")（什叶派的一个独特分支）信徒。

36.6%的人口小于14岁，3.4%老于64岁。

人口增长率是2.89%。婴儿死亡率是1.961%。期望寿命为77.71岁。

## 宗教

約旦絕大多數信奉[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")，遜尼派佔92%，6%為羅馬天主教及希臘東正教信徒，其餘2%為其他教派。東正教及天主教信徒集中於[安曼和](../Page/安曼.md "wikilink")[米底巴市](../Page/米斯巴.md "wikilink")。約旦雖屬伊斯蘭教國家，卻不似沙烏地阿拉伯、阿曼、葉門等伊斯蘭教國家具有濃厚宗教氛圍和禁忌，宗教較自由，兩教信徒相處和諧，並不相互排斥。\[14\]
也因约旦人多數為溫和的遜尼派，在較少宗教衝突下，使約旦經濟能夠發展。\[15\]

## 教育

約旦重视教育事业，实行10年义务基础教育，公立學校免收學費，高中教育为非义务性专业学习，学制两年。第12年級學期末，所有學生必須參加會考（Tawjihi），評成績申請就讀大學。\[16\]\[17\]

全国共有10所公立大学和17所私立大学，較著名的大學如下：[約旦大學](../Page/約旦大學.md "wikilink")（University
of Jordan）、[雅爾木克大學](../Page/雅爾木克大學.md "wikilink")（Yarmouk
University）、[約旦科技大學](../Page/約旦科技大學.md "wikilink")（Jordan
University of Science and
Technology）、[穆塔大學](../Page/穆塔大學.md "wikilink")（Mo'tah
University）、[艾勒·貝塔大学](../Page/艾勒·貝塔大学.md "wikilink")、[侯赛因大学](../Page/侯赛因大学.md "wikilink")、[拜勒加應用大學](../Page/拜勒加應用大學.md "wikilink")。另有51所中专院校。约学龄儿童入学率95％，全国文盲率7％。\[18\]\[19\]

## 参考

## 外部連結

[约旦](../Category/约旦.md "wikilink")
[Category:前英國殖民地](../Category/前英國殖民地.md "wikilink")
[Category:王國](../Category/王國.md "wikilink")
[Category:君主立憲國](../Category/君主立憲國.md "wikilink")

1.  外交部領事事務局，[約旦](http://www.boca.gov.tw/ct.asp?xItem=456&ctNode=753&mp=1)

2.
3.  al-Hussein, Abdullah II bin. *Our Last Best Chance: The Pursuit of
    Peace in a Time of Peril*, New York City: Viking Adult, 2011. ISBN
    978-0-670-02171-0 pg 241

4.  [環球視線-约旦：营救被俘飞行员不惜代价](http://news.cntv.cn/2014/12/26/VIDE1419607199010507.shtml)，《央視網》，2014年12月26日

5.  [Jordanian Military Helps Its
    Neighbors](http://www.defense.gov/news/newsarticle.aspx?id=14978)

6.
7.  [「茉莉花革命」燃燒
    約旦政府被解散](http://www.epochtimes.com/b5/11/2/2/n3160351.htm)，《大紀元》，2011年2月2日。

8.  [担心街头示威酿成骚乱 约旦国王承诺民主改革](http://www.zaobao.com/gj/gj110614_014.shtml)

9.  [約旦通過2015年國會選舉法，僅作小幅修改](http://www.roc-taiwan.org/jo/post/711.html)，中華民國（台灣）駐約旦代表處，2016年2月24日。

10. [دائرة الإحصاءات العامة -
    الأردن](http://www.dos.gov.jo/dos_home_a/main/index.htm)

11.

12.
13. [約旦國家概況](http://www.fmprc.gov.cn/ce/cejo/chn/zjyd/gjgk/t142982.htm)，中華人民共和國駐約旦哈希姆王國大使館。

14. 中華民國外交部，[約旦](http://www.mofa.gov.tw/CountryInfo.aspx?CASN=7F220D7E656BE749&n=A985E71D2A3FA4B6&sms=26470E539B6FA395&s=E13057BB37942D3F)

15.
16.
17.
18.
19.