[SINOPEC_HQ_20170425_091147.jpg](https://zh.wikipedia.org/wiki/File:SINOPEC_HQ_20170425_091147.jpg "fig:SINOPEC_HQ_20170425_091147.jpg")
[SinopecHQChaoyang.JPG](https://zh.wikipedia.org/wiki/File:SinopecHQChaoyang.JPG "fig:SinopecHQChaoyang.JPG")
[13-08-11-hongkong-50mm-38.jpg](https://zh.wikipedia.org/wiki/File:13-08-11-hongkong-50mm-38.jpg "fig:13-08-11-hongkong-50mm-38.jpg")
[HK_TaiPoRoad_SinopecOilStation.JPG](https://zh.wikipedia.org/wiki/File:HK_TaiPoRoad_SinopecOilStation.JPG "fig:HK_TaiPoRoad_SinopecOilStation.JPG")[大埔](../Page/大埔_\(香港\).md "wikilink")[大埔公路大埔滘段的加油站](../Page/大埔公路.md "wikilink")\]\]

**中国石油化工股份有限公司**，簡稱**中石化**，（Sinopec, the China Petroleum and Chemical
Corporation）（、、、）是一家巨型能源化工企业。它集[石油的勘探](../Page/石油.md "wikilink")、开采、炼制、运输、销售和化工产品的生产于一体，涵盖石油生产的上中下游全流程。公司是由[中国石油化工集团公司于](../Page/中国石油化工集团公司.md "wikilink")2000年2月25日独家发起的股份制企业，在[香港](../Page/香港.md "wikilink")、[纽约](../Page/纽约.md "wikilink")、[伦敦和](../Page/伦敦.md "wikilink")[上海的](../Page/上海.md "wikilink")[证券交易所分别上市](../Page/证券交易所.md "wikilink")。

## 概述

中国的化工行业历来生产厂家众多、生产效率低下、国内竞争激烈、国际竞争力非常薄弱。为改变这一状况，中国政府对石油化工业实施了全面重组，按生产的上下游和地域关系，将原松散型的中国石化总公司与原石油工业部重组成两大集团，即[中国石油天然气集团公司和](../Page/中国石油天然气集团公司.md "wikilink")[中国石油化工集团公司](../Page/中国石油化工集团公司.md "wikilink")。中国石油化工股份有限公司就是中国石油化工集团公司的控股子公司。

中国石化的业务主要是石油的开采、销售以及相关化工产品的生产，它与[中国石油天然气股份有限公司](../Page/中国石油天然气股份有限公司.md "wikilink")、[中国海洋石油有限公司共同垄断了中国大陆的石油市场](../Page/中国海洋石油有限公司.md "wikilink")。与另两家公司相比，中国石化更注重炼油与化工业务，它是中国最大的石油制品和化工产品生产商，原油生产量位居第二。公司的产品主要有原油、[天然气](../Page/天然气.md "wikilink")、[化纤](../Page/化纤.md "wikilink")、[化肥](../Page/化肥.md "wikilink")、[橡胶](../Page/橡胶.md "wikilink")、成品油等。

2012年10月31日其子公司中國石化國際入股俄羅斯氣體加工和石化公司西布尔（）旗下克拉斯諾亞爾斯克市合成塑膠廠（Krasnoyarsk
Synthetic Rubbers Plant）25%股份加1股

2013年2月25日中國石化斥10.2億美元(約79.5億港元)購Chesapeake Energy
Corporation(CEC)所持美國密西西比油氣田項目50%股權。該項目於2012年底止平均日產3.4萬桶油。

2018年12月，中国石化于[新加坡的第一间](../Page/新加坡.md "wikilink")[加油站正式投入运作](../Page/加油站.md "wikilink")。\[1\]

## 合资加油站

中石化与数家国外企业合资成立成品油销售公司，其合资加油站遍布华东、华南。主要有位于[武汉](../Page/武汉.md "wikilink")、[杭州的中石化](../Page/杭州.md "wikilink")[道达尔](../Page/道达尔.md "wikilink")，位于[浙江](../Page/浙江.md "wikilink")[绍兴](../Page/绍兴.md "wikilink")、[宁波](../Page/宁波.md "wikilink")、[萧山的中石化](../Page/萧山.md "wikilink")[BP](../Page/BP.md "wikilink")，位于[苏州](../Page/苏州.md "wikilink")、[成都](../Page/成都.md "wikilink")、[广东的中石化](../Page/广东.md "wikilink")[壳牌](../Page/壳牌.md "wikilink")。

## 事件

### 中石化97号油争论事件

由于中国早期催化裂化催化剂与[催化](../Page/催化.md "wikilink")[裂化脱硫助剂技术一直处于薄弱状态](../Page/裂化.md "wikilink")，其汽油[硫含量偏高](../Page/硫.md "wikilink")，而[辛烷值不足](../Page/辛烷值.md "wikilink")。为补充辛烷值，中石化不得不在其90号以上的汽油中添加[MTBE或MMT](../Page/甲基叔丁基醚.md "wikilink")(甲基环戊二烯三羰基锰)，造成汽油标号越高，这些抗爆震剂的含量越高，对[发动机腐蚀性损害越大](../Page/发动机.md "wikilink")，因此出现了中国国内90号以上的汽油都靠勾兑制备的说法，进而引起了中国产97号油不如93号油的争论和中国各种[汽油添加剂产业的兴起](../Page/汽油添加剂.md "wikilink")。中石化在2008年后逐渐引进BP的专利多次裂解技术生产出了直接由多次裂解制法制备的97号或更高标号的汽油，但这种“真正的”97号汽油只在极少数中石化加油站出售。

### 中石化“天价酒”事件

### 中石化输油管线爆燃事故

## 参考文献

## 外部链接

  - [中国石油化工股份有限公司](http://www.sinopec.com/)

## 参见

  - [中国石油化工集团](../Page/中国石油化工集团.md "wikilink")

{{-}}

[中国石化](../Category/中国石化.md "wikilink")

1.