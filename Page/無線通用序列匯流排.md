[USB_Wireless_certified_Logo.svg](https://zh.wikipedia.org/wiki/File:USB_Wireless_certified_Logo.svg "fig:USB_Wireless_certified_Logo.svg")

**無線通用序列匯流排**（**Wireless Universal Serial Bus**，正式縮寫**Wireless
USB**，通常縮寫*WUSB*或*無線USB*）是一種的高[頻寬的短距離無線傳輸技術](../Page/頻寬.md "wikilink")，将[通用序列匯流排](../Page/通用序列匯流排.md "wikilink")（USB）2.0的技術的速度和易用性和无线技術的便捷性结合起来。

無線通用序列匯流排的無線傳輸技術是基於[WiMedia聯盟](../Page/WiMedia聯盟.md "wikilink")（）的[超寬頻](../Page/超寬頻.md "wikilink")（Ultra-WideBand）的無線廣播技術。它能在3米範圍內以480Mb/s作傳輸，並能在10米範圍內以110Mb/s作傳輸。比外，它的運载[頻率十分廣](../Page/頻率.md "wikilink")，可在3.1G[Hz至](../Page/赫兹.md "wikilink")10.6GHz频率范围内使用。因此，在不同地區內使用基于無線通用序列匯流排相关技术的设备，能依照當地的法律而調整至合適、合法的頻率，這樣可使無線通用序列匯流排及其裝置能在世界各地廣泛应用。\[1\]

## 應用

無線通用序列匯流排與[通用序列匯流排的應用相若](../Page/通用序列匯流排.md "wikilink")，也能應用在[遊戲手掣](../Page/遊戲手掣.md "wikilink")、[列印機](../Page/列印機.md "wikilink")、[影像掃描器](../Page/影像掃描器.md "wikilink")、[數碼相機](../Page/數碼相機.md "wikilink")、[MP3播放器](../Page/MP3播放器.md "wikilink")、[硬盘和](../Page/硬盘.md "wikilink")[-{zh-hans:闪存盘;
zh-hant:隨身碟;}-等](../Page/隨身碟.md "wikilink")。

此外無線通用序列匯流排的技術也十分適合传送[并行视频](../Page/並行計算.md "wikilink")-{zh-hans:流;zh-hk:串流;zh-tw:串流;}-。

## 開發歷史

  - 2004年2月17日\[2\]

<!-- end list -->

  -
    [無線通用序列匯流排促進組織](../Page/無線通用序列匯流排促進組織.md "wikilink")（Wireless USB
    Promoter
    Group，或稱無線通用序列匯流排推廣小組）成立，並初步制定了無線通用序列匯流排的技术标准。當時小组成員包括[傑爾](../Page/傑爾系統.md "wikilink")、[惠普](../Page/惠普公司.md "wikilink")、[英特爾](../Page/英特爾.md "wikilink")、[微軟](../Page/微軟.md "wikilink")、[日本電氣](../Page/日本電氣股份有限公司.md "wikilink")、[飛利浦及](../Page/飛利浦公司.md "wikilink")[三星](../Page/三星集團.md "wikilink")。

<!-- end list -->

  - 2005年5月

<!-- end list -->

  -
    無線通用序列匯流排促進組織公布了無線通用序列匯流排完整的技术标准。而[Ellisys](../Page/Ellisys.md "wikilink")、[LeCroy等公司也能提供開發協助工具](../Page/LeCroy.md "wikilink")（相容性測試工具）以協助無線通用序列匯流排的技術開發。

<!-- end list -->

  - 2006年6月

<!-- end list -->

  -
    USB開發者論壇（USB Implementers
    Forum，简称USB-IF）首次以實物示範了五個不同廠牌的裝置（包括[軟件及](../Page/軟件.md "wikilink")[硬件](../Page/硬件.md "wikilink")）同時使用。一台[手提電腦使用以](../Page/手提電腦.md "wikilink")[Alereon的](../Page/Alereon.md "wikilink")[晶片運作的](../Page/晶片.md "wikilink")[英特爾的主机适配器](../Page/英特爾.md "wikilink")，連接至以[瑞昱](../Page/瑞昱.md "wikilink")（Realtek）的晶片運作的[飛利浦](../Page/飛利浦.md "wikilink")[高清影像播放器](../Page/高清.md "wikilink")。而所有無線通用序列匯流排设备的[驅動程式是由](../Page/驅動程式.md "wikilink")[微軟提供](../Page/微軟.md "wikilink")。\[3\]

<!-- end list -->

  - 2006年10月

<!-- end list -->

  -
    [美國聯邦通信委員會正式認可了首個主机有线适配器](../Page/美國聯邦通信委員會.md "wikilink")（HWA）和设备有线适配器（DWA）的無線通用序列匯流排完全实现方案。该方案可以同时应用于室内和室外。

<!-- end list -->

  - 2007年11月

<!-- end list -->

  -
    多個電腦[周邊裝置廠牌開始推出無線通用序列匯流排的集線器](../Page/周邊裝置.md "wikilink")，[D-Link的DUB](../Page/D-Link.md "wikilink")-9240\[4\]。
    而電腦主機廠牌[Lenovo及](../Page/Lenovo.md "wikilink")[DELL](../Page/DELL.md "wikilink")\[5\]，各有一個型號的手提電腦（分別為Lenovo
    61及DellInspiron 1720）成了首批獲得USB-IF認證的無線通用序列匯流排的主控制器。

## 主控制器有線轉接器、裝置有線轉接器，及多用途裝置

無線通用序列匯流排能容許127個裝置同時連接至主控制器。因為它是以無線傳輸，並沒有傳輸線及插頭，所以在連接時不用[集線器](../Page/集線器.md "wikilink")（Hub）。

不過，一些沒有無線主控制器的電腦使用無線裝置時，則要裝上特別的轉接器。其頻寬為2.4
GHz，一般也只能用在[人機介面裝置](../Page/人機介面裝置.md "wikilink")（Human
Interface
Device、HID、人工介面裝置），即[滑鼠](../Page/滑鼠.md "wikilink")、[鍵盤等](../Page/鍵盤.md "wikilink")。

裝置有線轉接器及無線裝置一般也需要額外[電源運作](../Page/電源.md "wikilink")，而主控制器有線轉接器則可由通用序列匯流排取得電源。

## 無線通用序列匯流排與藍牙

無線通用序列匯流排和[藍牙是兩種基於不同](../Page/藍牙.md "wikilink")[協議下的無線傳輸技術](../Page/協議.md "wikilink")，雙方也希望能成功佔有大部份的使用率。

無線通用序列匯流排是一種高[頻寬下運作的無線協議](../Page/頻寬.md "wikilink")，而可用範圍及頻寬也比[WiFi低](../Page/WiFi.md "wikilink")。但資料傳輸率比藍牙高。

## 參考

<div class="references-small">

  - [Wireless USB Specification
    Revision 1.0（.zip檔案）](http://www.usb.org/developers/wusb/wusb_2007_0214.zip)
  - PC 硬體介面徹底研究 ISBN 957-442-275-5
  - USB 2.0、Wireless USB、USB OTG 技術徹底研究 3rd edition ISBN 957-442-300-X
      - USB Complete Third edition ISBN 1-931448-02-7 （USB 2.0、Wireless
        USB、USB OTG 技術徹底研究 3rd edition 原版）

<references />

</div>

## 參見

  - [藍牙](../Page/藍牙.md "wikilink")
  - [ZigBee](../Page/ZigBee.md "wikilink")
  - [通用序列匯流排](../Page/通用序列匯流排.md "wikilink")

## 外部連結

  - [USB開發者論壇（USB-IF）USB.org首頁](http://www.usb.org)
  - [無線通用序列匯流排促進組織首頁](http://www.usb.org/wusb/home)
  - [無線通用序列匯流排的規格](http://www.usb.org/developers/wusb/)
  - [Wimedia/MBOA
    聯盟](https://web.archive.org/web/20020426095418/http://www.wimedia.org/)
  - [USB Central](http://www.lvr.com/usb.htm)
  - [Wireless USB News
    (english)](https://web.archive.org/web/20100412055325/http://www.wireless-usb.eu/wusb/)

[Category:USB](../Category/USB.md "wikilink")
[Category:無線網路](../Category/無線網路.md "wikilink")

1.  [超寬頻技術簡介](http://www.usb.org/developers/wusb/docs/Ultra-Wideband.pdf)
    。

2.  [無線通用序列匯流排的成立及展望簡介](http://www.usb.org/developers/wusb/docs/wirelessUSB.pdf)
    。

3.  [示範五個不同廠牌的裝置同時使用的新聞稿](http://www.usb.org/press/WUSB_press/2006_06_21_usbif.pdf)
    。

4.  [連接各種週邊，從此免線！
    《PChome十一月號》](http://tech.chinatimes.com/2007Cti/2007Cti-News/Inc/2007cti-news-Tech-inc/Tech-Content/0,4703,17170103+172007111601305,00.html)


5.