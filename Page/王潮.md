**王潮**（[閩東語](../Page/閩東語.md "wikilink")[平話字](../Page/平話字.md "wikilink")：；[閩南語](../Page/閩南語.md "wikilink")[白話字](../Page/白話字.md "wikilink")：；），[譜名](../Page/譜名.md "wikilink")**审潮**，[表字](../Page/表字.md "wikilink")**信臣**，，[光州固始](../Page/光州.md "wikilink")（今[河南](../Page/河南.md "wikilink")[固始](../Page/固始.md "wikilink")）人，[唐末任](../Page/唐.md "wikilink")[固始縣](../Page/固始縣.md "wikilink")[長史](../Page/長史.md "wikilink")。後南下征戰，為[福建](../Page/福建.md "wikilink")[觀察使](../Page/觀察使.md "wikilink")、[威武軍節度使](../Page/威武軍節度使.md "wikilink")。與弟[王审邽](../Page/王审邽.md "wikilink")、[王審知率兵自中原南下入閩](../Page/王審知.md "wikilink")，是[五代](../Page/五代.md "wikilink")[閩國的濫觴](../Page/閩國.md "wikilink")。

## 生平

王潮生于[唐武宗](../Page/唐武宗.md "wikilink")[会昌六年](../Page/会昌.md "wikilink")（846年），[琅琊王氏末裔](../Page/琅琊王氏.md "wikilink")，[王恁之子](../Page/王恁.md "wikilink")，母徐氏。本為固始縣長史。

[僖宗](../Page/唐僖宗.md "wikilink")[中和元年](../Page/中和_\(唐僖宗\).md "wikilink")（881年），[壽州](../Page/壽州.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")[壽縣](../Page/壽縣.md "wikilink")）[農民軍首領](../Page/流寇.md "wikilink")[王緒呼應](../Page/王緒.md "wikilink")[黃巢之亂](../Page/黃巢之亂.md "wikilink")，攻破[光州](../Page/光州.md "wikilink")，俘獲王潮、[王審邽](../Page/王審邽.md "wikilink")、[王審知三兄弟](../Page/王審知.md "wikilink")，王緒命王潮為軍正。

中和五年（885年）隨王緒轉戰[福建](../Page/福建.md "wikilink")，時為[參軍](../Page/參軍.md "wikilink")，因王緒要求他拋棄老母，節省軍糧，王潮非常憤怒，遂與前軍將軍合作，發動[兵變](../Page/兵變.md "wikilink")[禁錮王緒](../Page/禁錮.md "wikilink")，王緒憂憤自殺。

[光启二年](../Page/光启.md "wikilink")（886年），王潮应[泉州張延魯等人所请](../Page/泉州.md "wikilink")，克泉州，杀刺史[廖彦若](../Page/廖彦若.md "wikilink")，[福建觀察使](../Page/福建觀察使.md "wikilink")[陈岩任命其為泉州](../Page/陈岩.md "wikilink")[刺史](../Page/刺史.md "wikilink")。陳巖病后曾召王潮，欲授以[军政](../Page/军政.md "wikilink")。王潮还没有到达，陳巖已经去世，陳之妻弟[護軍](../Page/護軍.md "wikilink")[范晖](../Page/范晖.md "wikilink")，自称[留后](../Page/留后.md "wikilink")，发兵拒潮\[1\]。王潮命其弟王審知、堂弟[王彥復攻福州](../Page/王彥復.md "wikilink")（今福建[福州](../Page/福州.md "wikilink")），[昭宗](../Page/唐昭宗.md "wikilink")[景福二年](../Page/景福.md "wikilink")（893年）攻克\[2\]，素服葬陈岩，并将女儿嫁给陈岩子陈延晦。其後漸佔福建全境，王潮受封為[福建觀察使](../Page/福建觀察使.md "wikilink")，又升為[威武軍](../Page/威武軍.md "wikilink")[節度使](../Page/節度使.md "wikilink")\[3\]。

[乾寧四年十二月初六](../Page/乾寧.md "wikilink")（898年1月2日）王潮去世，被唐[朝廷追贈為](../Page/朝廷.md "wikilink")[司空](../Page/司空.md "wikilink")、。尽管王潮有四子[王延興](../Page/王延興.md "wikilink")、[王延虹](../Page/王延虹.md "wikilink")、[王延豐](../Page/王延豐.md "wikilink")、[王延休](../Page/王延休.md "wikilink")，二弟王审邽也还在世，他选择由三弟王審知繼位，王审知後封王，開創了[閩國](../Page/閩國.md "wikilink")。

王潮兄弟自[中原南下](../Page/中原.md "wikilink")，帶來大批族人，所以至今仍被不少福建、[臺灣](../Page/臺灣.md "wikilink")[王姓家族奉為始祖](../Page/王姓.md "wikilink")。

## 墓葬

王潮死后葬于[中国](../Page/中华人民共和国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[泉州市](../Page/泉州市.md "wikilink")[惠安县](../Page/惠安县.md "wikilink")[螺阳镇盘龙村的凤旗山](../Page/螺阳镇.md "wikilink")。王潮墓为大型的唐末、五代古墓建筑，是保存较完整的一个古代石构墓。1996年，福建省人民政府公布为第四批省级文物保护单位。

王潮死後被鄉民奉為「威武尊王」（）或「廣武尊王」（），在福州等地有廟宇，稱**水西大王廟**。或配祀在[開閩聖王廟](../Page/開閩聖王.md "wikilink")。

[福建人尊稱](../Page/福建人.md "wikilink")[審知為](../Page/王審知.md "wikilink")「開閩尊王」、[審邽為](../Page/王審邽.md "wikilink")「泉安尊王」，視為鄉土神明供奉，與王潮合稱「[開閩三王](../Page/開閩三王.md "wikilink")」，時常合奉於開閩尊王廟。[泉州承天寺西側至今仍有](../Page/泉州承天寺.md "wikilink")「開閩三王祠」，奉祀開閩三王。

## 註釋

## 參考資料

  - 《[資治通鑒](../Page/資治通鑒.md "wikilink")》
  - 《[淳熙三山志](../Page/淳熙三山志.md "wikilink")》

[W王](../Category/唐朝政治人物.md "wikilink")
[W王](../Category/唐朝軍事人物.md "wikilink")
[W王](../Category/五代十国人物.md "wikilink")
[C](../Category/王姓.md "wikilink")
[Category:閩國皇族](../Category/閩國皇族.md "wikilink")
[W王](../Category/固始人.md "wikilink")
[Category:福建民間信仰](../Category/福建民間信仰.md "wikilink")
[Category:道教神祇](../Category/道教神祇.md "wikilink")
[Category:人物神](../Category/人物神.md "wikilink")

1.  《[资治通鉴/卷258](../Page/s:资治通鉴/卷258.md "wikilink")》“遣使以书召泉州刺史王潮，欲授以军政，未至而巖卒。巖妻弟都将范晖……发兵拒潮。“
2.  《[淳熙三山志/卷01](../Page/s:淳熙三山志/卷01.md "wikilink")》
3.  《[讀史方輿紀要/卷九十六](../Page/s:讀史方輿紀要/卷九十六.md "wikilink")》『乾宁三年，升为威武节度使，以授王潮。』