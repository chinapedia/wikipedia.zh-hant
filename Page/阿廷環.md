**阿廷環**是[抽象代數中一類滿足](../Page/抽象代數.md "wikilink")[降鏈條件的](../Page/降鏈條件.md "wikilink")[環](../Page/環.md "wikilink")，以其開創者[埃米爾·阿廷命名](../Page/埃米爾·阿廷.md "wikilink")。

## 定義

一個環\(A\)稱作阿廷環，若且唯若對每個由\(A\)的[理想構成的降鏈](../Page/理想.md "wikilink")\(\mathfrak{a}_1 \supset \mathfrak{a}_2 \supset \ldots, \supset\mathfrak{a}_n \supset\ldots\)，必存在\(N \subset \mathbb{N}\)，使得對所有的\(n,m \geq N\)都有\(\mathfrak{a}_n = \mathfrak{a}_m\)（換言之，此降鏈將會固定）。

將上述定義中的理想代換為左理想或右理想，可以類似地定義左阿廷環與右阿廷環，A是左（右）阿廷環若且唯若A在自己的左（右）乘法下形成一個左（右）[阿廷模](../Page/阿廷模.md "wikilink")；對於交換環則無須分別左右。

## 例子

  - 設\(k\)為一個[域](../Page/域.md "wikilink")，若環\(A\)是佈於\(k\)上的有限維代數，則\(A\)是阿廷環。

## 基本性質

若一個環\(A\)是交換阿廷環，則滿足下列性質：

  - \(A\)是[諾特環](../Page/諾特環.md "wikilink")。
  - 每個[素理想皆是](../Page/理想_\(環論\).md "wikilink")[極大理想](../Page/理想_\(環論\).md "wikilink")。
  - \(A\)僅有有限個素理想。
  - 對每個素理想的[局部化誘導出同構](../Page/局部化.md "wikilink")
    \(A \stackrel{\sim}{\rightarrow} \prod_\mathfrak{p} A_\mathfrak{p}\)。

就[代數幾何的觀點](../Page/代數幾何.md "wikilink")，阿廷環的[譜在拓樸上只是有限多個點](../Page/交換環譜.md "wikilink")，但其結構層可能帶有冪零的元素，這就使得局部阿廷環成為描述無窮小變化量的代數語言。

## 文獻

  - Charles Hopkins. *Rings with minimal condition for left ideals*.
    Ann. of Math. (2) 40, (1939). 712--730.
  - Serge Lang, *Algebra* (2002), Graduate Texts in Mathematics 211,
    Springer. ISBN 0-387-95385-X

[A](../Category/交換代數.md "wikilink") [A](../Category/抽象代數.md "wikilink")
[A](../Category/環論.md "wikilink")