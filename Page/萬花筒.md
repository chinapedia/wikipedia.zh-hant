[800px-Kaleidoscope-tube-modified.jpg](https://zh.wikipedia.org/wiki/File:800px-Kaleidoscope-tube-modified.jpg "fig:800px-Kaleidoscope-tube-modified.jpg")
**萬花筒**，一种[光学](../Page/光学.md "wikilink")[玩具](../Page/玩具.md "wikilink")，将有鲜艳[颜色的实物放於圆筒的一端](../Page/颜色.md "wikilink")，圆筒中间放置[三棱镜](../Page/三棱镜.md "wikilink")，另一端用開孔的[玻璃密封](../Page/玻璃.md "wikilink")，由孔中看去即可观测到[对称的美丽图像](../Page/对称.md "wikilink")。1817年[蘇格蘭科学家和發明家](../Page/蘇格蘭.md "wikilink")[大卫·布儒斯特爵士發明萬花筒](../Page/大卫·布儒斯特.md "wikilink")\[1\]。

## 結構

[Kaleidoscopes.jpg](https://zh.wikipedia.org/wiki/File:Kaleidoscopes.jpg "fig:Kaleidoscopes.jpg")

萬花筒是一個內放置[三棱镜](../Page/三棱镜.md "wikilink")，兩端密封，其中一端放有颜色的物體（如[玻璃碎片](../Page/玻璃.md "wikilink")），另一端開孔的[圓柱](../Page/圓柱.md "wikilink")。

## 歷史

1815年[大衛·布儒斯特爵士的工作主要從事光學和光譜研究](../Page/大衛·布儒斯特.md "wikilink")\[2\]，這導致萬花筒的發明，他正實驗光的偏振，但一開始它沒有申請專利。1817年布魯斯特以「Kaleidoscope」之名申請專利，開始有萬花筒專利。Kaleidoscope字源是希臘文来源于kalos,是美麗的意思\[3\]，而eidos
是form 的意思，即觀看美麗的形狀。由當時著名的光學公司Philip Carpenter
承包萬花筒的製造。三個月内，[伦敦和](../Page/伦敦.md "wikilink")[巴黎一共售出](../Page/巴黎.md "wikilink")
20
万个万花筒，但大部份的萬花筒都是盜版品，布魯斯特未获得应有的金钱回报。布魯斯特寫信給妻子說：“數以千計的窮人都靠製作和銷售萬花筒為生”。十九世纪，万花筒傳入中国，作为大清王朝达官贵人的珍藏。

現代萬花筒是由黃銅管、彩色玻璃、木材、鋼材、葫蘆或幾乎任何材質都可以使用。日本設計師山見浩司深入鑽研萬花筒，連續15年入選有「萬花筒奧斯卡」之稱的Brewster
conventions，並兩度獲得美國[全美民選獎](../Page/全美民選獎.md "wikilink")。

## 參考文獻

[Category:光学仪器](../Category/光学仪器.md "wikilink")
[Category:玩具](../Category/玩具.md "wikilink")

1.

2.
3.  [καλός](http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A1999.04.0057%3Aentry%3Dkalo%2Fs2),
    Henry George Liddell, Robert Scott, *A Greek-English Lexicon*, on
    Perseus