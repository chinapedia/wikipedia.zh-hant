[Paramagnetism_of_liquid_oxygen.jpeg](https://zh.wikipedia.org/wiki/File:Paramagnetism_of_liquid_oxygen.jpeg "fig:Paramagnetism_of_liquid_oxygen.jpeg")
**順磁性**（Paramagnetism）指的是一種材料的磁性狀態。有些材料可以受到外部磁场的影响，产生跟外部磁場同樣方向的[磁化向量的特性](../Page/磁化向量.md "wikilink")。这样的物质具有正的[磁化率](../Page/磁化率.md "wikilink")。与順磁性相反的现象被称为[抗磁性](../Page/抗磁性.md "wikilink")。

## 原理

[Paramagnetic_probe_without_magnetic_field.svg](https://zh.wikipedia.org/wiki/File:Paramagnetic_probe_without_magnetic_field.svg "fig:Paramagnetic_probe_without_magnetic_field.svg")
[Paramagnetic_probe_with_weak_magnetic_field.svg](https://zh.wikipedia.org/wiki/File:Paramagnetic_probe_with_weak_magnetic_field.svg "fig:Paramagnetic_probe_with_weak_magnetic_field.svg")
[Paramagnetic_probe_with_strong_magnetic_field.svg](https://zh.wikipedia.org/wiki/File:Paramagnetic_probe_with_strong_magnetic_field.svg "fig:Paramagnetic_probe_with_strong_magnetic_field.svg")
順磁性物質可以被看作是由許多微小的[磁棒組成的](../Page/磁棒.md "wikilink")，这些磁棒可以旋轉，但是無法移動。這樣的物質受到外部磁場的影響後其磁棒主要順[磁力線方向排列](../Page/磁力線.md "wikilink")，但是這些磁棒互相之间不影響。熱振動不斷地使得磁棒的方向重新排列，因此磁棒指向不排列比排列的可能性高。因此磁力線的强度越强順磁性物質内磁棒的排列性就越强。

以上模型當然只是一個簡化的模型。實際上順磁性物質内部并没有小磁棒，而是微觀的[磁矩](../Page/磁矩.md "wikilink")。在順磁性物質中這些磁矩互相之間不影影響。然而與[鐵磁性不同的是在順磁性物质中外部磁場消失後物質内的磁場立刻就由于熱運動消失了](../Page/鐵磁性.md "wikilink")。[磁化向量](../Page/磁化向量.md "wikilink")\(M\)與[磁場强度](../Page/磁場强度.md "wikilink")\(H\)成正比

\[\left.M=\chi H\right.\], \(\left.\chi>0\right.\)。
物質的磁化率\(\chi\)越高，它就越容易被磁化。因此磁化率是衡量順磁性的强度的量。由於磁化率和相對[磁導率之間有以下簡單關係](../Page/磁導率.md "wikilink")\(\mu_r=\chi+1\)磁導率往往也被看作是衡量順磁性的量。

假如磁矩之間有[耦合的話](../Page/耦合.md "wikilink")，物質内就會產生[磁性有序狀態](../Page/磁性有序狀態.md "wikilink")，在這種情況下磁化率會非常複雜，因此这這樣的物質不再是順磁性的。總的來說這樣的物質的磁性有序狀態在一個阈温度以上會被破壞，由於物質中依然有磁矩，因此在這個溫度以上这這樣的物質呈順磁性。

[鐵磁性物質均擁有極大的磁化率](../Page/鐵磁性.md "wikilink")，但是大的[磁化率不一定就說明一个物質是](../Page/磁化率.md "wikilink")[鐵磁性的](../Page/鐵磁性.md "wikilink")。

## 分类

从经典物理学出发物质的順磁性无法完全被解释，只有从[量子力学出发这个特征才能被完全理解](../Page/量子力学.md "wikilink")。对于磁学最重要的认识是一个[原子状态的总](../Page/原子.md "wikilink")[角动量](../Page/角动量.md "wikilink")\(\vec J\)总是与其磁矩\(\vec \mu\)相连的：

\[\vec \mu=-g\mu_\mathrm{B}\vec J\]
在这里\(g\)是[電子自旋g因數](../Page/電子自旋g因數.md "wikilink")，\(\mu_\mathrm{B}\)是[玻尔磁子](../Page/玻尔磁子.md "wikilink")。原子的总角动量由以下三部分组成：

1.  [自旋](../Page/自旋.md "wikilink")
2.  [电子的](../Page/电子.md "wikilink")[角量子数](../Page/角量子数.md "wikilink")
3.  [核子的](../Page/核子.md "wikilink")[核自旋](../Page/核自旋.md "wikilink")。

核自旋导致的磁矩非常弱，对磁化率基本上没有多少作用，因此这个量一般不被顾及，不过这个量还是可以被测量得到的，[医学中使用的](../Page/医学.md "wikilink")[核磁共振成像就是测量这个量获得的](../Page/核磁共振成像.md "wikilink")。

以下各个因素均对磁化率有重要的作用。在物质内也总是有[抗磁性的因素存在](../Page/抗磁性.md "wikilink")，因此一个物质最终是否是順磁性要看所有因素的总和。但是原子在基态下的磁矩所提供的順磁性一般来说是最大的。假如一个物质的原子在基态下其磁矩呈順磁性，则一般这个物质是順磁性的。

### 原子基态下的磁矩

一个原子在[基态下的总角动量可以通过](../Page/基态.md "wikilink")[洪德定则来理论确定](../Page/洪德定则.md "wikilink")。最重要的规则是一个完全饱和和除一个电子外半饱和的电子层的总角动量为零。在其它情况下原子拥有一个磁矩。

[居里定律描写这个量与温度之间的关系](../Page/居里定律.md "wikilink")

\[\chi_\mathrm{Langevin}=\frac{C}{T}\]
\(C\)是[居里常数](../Page/居里常数.md "wikilink")。

### 自由电子的磁矩

在[金属中电子实际上可以自由运动](../Page/金属.md "wikilink")。理论上所有的电子都有其自己的磁矩，但是出于[泡利不相容原理只有可以自由运动的电子才能够在将它们的磁矩指向磁场方向](../Page/泡利不相容原理.md "wikilink")，这样的电子的数量与\(T/T_\mathrm{F}\)成正比，其中\(T_\mathrm{F}\)是[费米温度](../Page/费米能.md "wikilink")：

\[\chi_\mathrm{Pauli}\sim\frac{C}{T}\cdot\frac{T}{T_\mathrm{F}}=\frac{C}{T_\mathrm{F}}\]
精确的计算显示它还随外部磁场强度而变化。

### 原子受激态的磁矩

在[受激态下即使在基态总角动量为零的原子也可能获得角动量](../Page/受激态.md "wikilink")。由于在绝对零度以上物质中总是有基态以上的原子，因此在所有物质中均有一定的原子角动量。但是只有在分子晶体中这个量才会达到一定的大小。在有些物质中它甚至会强于基态的磁矩。要计算[分子中的受激态的磁矩很困难](../Page/分子.md "wikilink")。

## 超顺磁性

有晶粒的铁磁性的固体的磁性与其[晶粒度有关](../Page/晶粒度.md "wikilink")。晶粒度越小，每个晶粒内的[外磁畴的数量就越少](../Page/外磁畴.md "wikilink")。晶粒度小于一个[阈值后每个晶粒只有一个外磁畴](../Page/阈值.md "wikilink")，因为这样晶体内的能量最低。在这种情况一个晶粒内所有的原子的磁矩均指向同一方向。假如晶粒继续缩小到一个阈值以下的话在绝对零度以上热运动会破坏铁磁性，一个晶粒中的所有原子无法将其总磁矩指向同一方向。整个固体呈顺磁性，但是这个顺磁性有一个特殊点：指向外部磁场的磁矩的数量与外部磁场的强度不呈正比，其数量块状增加。这个不寻常的顺磁性被称为超顺磁性。

## 例子

### 碱金属

[碱金属的电子层由惰性气体电子层加上一个](../Page/碱金属.md "wikilink")电子组成。按照洪德定则它们在基态下有磁矩，这个磁矩提供很强的磁化率，因此碱金属是顺磁性的。

### 碱土金属

[碱土金属有两个](../Page/碱土金属.md "wikilink")电子，因此其电子层饱和，但是它们属于金属，因此拥有自由电子。除[铍外其自由电子导致的顺磁性强于抗磁性](../Page/铍.md "wikilink")，因此它们均是弱顺磁性物质。

### 稀土金属

[稀土金属是制造](../Page/稀土金属.md "wikilink")[磁铁时最重要的合金物质](../Page/磁铁.md "wikilink")，原因是稀土金属不饱和的电子层不是最外部的电子层，而是内部的电子层（层），因此它们对于原子的化学性能没有影响。几乎所有的稀土金属是顺磁性的，但是其磁化率不同。通过它们合金可以成为非常强的磁铁。

### 分子

分子一般拥有饱和的电子层，而且不是金属，因此它们一般只有很弱的、由于受激态导致的顺磁性。一些顺磁性的材料包括：

  - [二氧化氮](../Page/二氧化氮.md "wikilink")
  - [氧气](../Page/氧气.md "wikilink")

### 磁铁矿

[磁铁矿](../Page/磁铁矿.md "wikilink")（）一般呈铁磁性，但是颗粒小于20至30纳米的磁铁矿在室温下呈超顺磁性。在外部磁场的影响下其所有磁矩均顺磁场方向排列。但是在外部磁场消失后热运动足以使其粒子的磁化消失为零。

## 參見

  - [磁化曲線](../Page/磁化曲線.md "wikilink")

## 参考文献

  - [paramagnetism](http://global.britannica.com/EBchecked/topic/442927/paramagnetism).
    Encyclopædia Britannica.

## 外部链接

  - [中国科普博览，磁学馆](http://www.kepu.net.cn/gb/basic/magnetism/magnetabc/200306110017.html)

[S](../Category/磁学.md "wikilink")