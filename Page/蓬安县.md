**蓬安县**是[中国](../Page/中国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[南充市所辖的一个](../Page/南充市.md "wikilink")[县](../Page/县.md "wikilink")，面积约为1334平方公里，2012年的户籍人口约为713227人，共辖39个乡镇，县城建成区的面积为9平方公里，县城的[常住人口为](../Page/常住人口.md "wikilink")11万人。　　

## 历史沿革

蓬安县原为[蓬州](../Page/蓬州.md "wikilink")，公元前七世纪起，县境属古[巴国](../Page/巴国.md "wikilink")，[两汉时属](../Page/两汉.md "wikilink")[安汉县](../Page/安汉县.md "wikilink")，[南朝梁](../Page/南朝梁.md "wikilink")[天监六年](../Page/天监.md "wikilink")（507年）从安汉县中置相如县，时北部为[大寅县](../Page/大寅县.md "wikilink")（后改名[蓬池县](../Page/蓬池县.md "wikilink")），南部为相如县，[北周](../Page/北周.md "wikilink")[天和四年](../Page/天和.md "wikilink")（569年）在今蓬安一代设立[蓬州](../Page/蓬州.md "wikilink")，治所在今[营山县](../Page/营山县.md "wikilink")[安固乡](../Page/安固乡.md "wikilink")。[唐玄宗开元二十九年](../Page/唐玄宗.md "wikilink")（741年），蓬州治所由安固迁至大寅（今[仪陇县](../Page/仪陇县.md "wikilink")[大寅镇](../Page/大寅镇.md "wikilink")），唐朝[广德元年](../Page/广德.md "wikilink")（763年），大寅县更名为蓬池县，治所在今天蓬安县[茶亭乡的蓬池坝](../Page/茶亭乡.md "wikilink")。[南宋](../Page/南宋.md "wikilink")[淳祜年间](../Page/淳祜.md "wikilink")，蓬州治所再由大寅迁至[河舒镇的燕山寨](../Page/河舒镇.md "wikilink")，[元朝](../Page/元朝.md "wikilink")[至元十五年](../Page/至元.md "wikilink")（1278年），治所再徒至[相如镇](../Page/相如镇.md "wikilink")，属于[四川行省](../Page/四川行省.md "wikilink")[顺庆路](../Page/顺庆路.md "wikilink")，至正二十年（1283年），废除[蓬池县](../Page/蓬池县.md "wikilink")。[明太祖](../Page/明太祖.md "wikilink")[洪武年间](../Page/洪武.md "wikilink")（1368年—1398年），废除[相如县](../Page/相如县.md "wikilink")，直属蓬州，[清世祖](../Page/清世祖.md "wikilink")[顺治七年](../Page/顺治.md "wikilink")（1650年）蓬州改为不领县的县级散州，[民国二年](../Page/民国.md "wikilink")（1913年），废蓬州，取[蓬州](../Page/蓬州.md "wikilink")、[安汉县二县的首字改名为蓬安县](../Page/安汉县.md "wikilink")，1949年由[川北行署](../Page/川北行署.md "wikilink")[南充专区管辖](../Page/南充专区.md "wikilink")，县城为[锦屏镇](../Page/锦屏镇.md "wikilink")，1952年属四川省南充专区管辖，并将[二道](../Page/二道镇_\(仪陇县\).md "wikilink")、[复兴乡](../Page/复兴镇_\(仪陇县\).md "wikilink")、双河乡（今[双胜乡](../Page/双胜镇_\(仪陇县\).md "wikilink")）、[歧山乡](../Page/歧山乡.md "wikilink")、[赛金乡](../Page/赛金镇.md "wikilink")、陇城乡（今[新城乡](../Page/新城乡.md "wikilink")）等划入仪陇，并将三兴场（今[三兴镇](../Page/三兴镇_\(营山县\).md "wikilink"))划入营山县。1968年为[南充地区管辖](../Page/南充地区.md "wikilink")（县城所在地为[周口镇](../Page/相如镇.md "wikilink")），1993年起为南充市辖县。

## 地理概况

本县位于四川省东北部，属[川北向](../Page/川北.md "wikilink")[川南的](../Page/川南.md "wikilink")[过渡地带](../Page/过渡.md "wikilink")，介于[北纬](../Page/北纬.md "wikilink")30°44'～31°17'，[东经](../Page/东经.md "wikilink")106°10'～106°10'之间，，西接[高坪](../Page/高坪.md "wikilink")、[南部](../Page/南部.md "wikilink")、北倚[朱德](../Page/朱德.md "wikilink")[元帅故里](../Page/元帅.md "wikilink")[仪陇县](../Page/仪陇县.md "wikilink")、东连[营山](../Page/营山.md "wikilink")、[渠县](../Page/渠县.md "wikilink")、南邻[岳池县](../Page/岳池县.md "wikilink")、[广安区](../Page/广安区.md "wikilink")，东西宽度约为22.1公里，南北长度约为61公里。面积约为1334平方公里，[耕地面积](../Page/耕地面积.md "wikilink")54.9万亩。[嘉陵江从境内通过有](../Page/嘉陵江.md "wikilink")90公里，县域为[丘陵地带](../Page/丘陵.md "wikilink")，是农业大县，[柑桔](../Page/柑桔.md "wikilink")、[蚕茧等产量丰富](../Page/蚕茧.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Penganxian.jpg "fig:缩略图")

## 河流

蓬安县境内主要的河流有[嘉陵江](../Page/嘉陵江.md "wikilink")、[清溪河](../Page/清溪河.md "wikilink")、[河舒河](../Page/河舒河.md "wikilink")\[1\]，均属于长江流域。

## 交通

蓬安县交通发达，[达成铁路穿过县境南部](../Page/达成铁路.md "wikilink")，并在城南设有[火车站](../Page/火车站.md "wikilink")\[2\]，[G5515](../Page/G5515.md "wikilink")
张南高速、
[Kokudou_318(China).svg](https://zh.wikipedia.org/wiki/File:Kokudou_318\(China\).svg "fig:Kokudou_318(China).svg")
[318国道穿过该县的南部](../Page/318国道.md "wikilink")，[203省道和](../Page/203省道.md "wikilink")[204省道也穿过该县](../Page/204省道.md "wikilink")。

## 方言

全县的主要方言是南充话，属于西南官话下的四川话成渝片，与[成都话和](../Page/成都话.md "wikilink")[重庆话很接近](../Page/重庆话.md "wikilink")，蓬安县的大多数乡镇都说这种话，约占全县总人口的70%左右。第二大的方言为[湘语](../Page/湘语.md "wikilink")[永州话](../Page/永州话.md "wikilink")，该[方言于](../Page/方言.md "wikilink")[老湘语](../Page/老湘语.md "wikilink")，福德镇、南燕乡、银汉镇、诸家乡、茶亭乡、石孔乡、海田乡，天成乡、兴旺镇、凤石乡、罗家镇、杨家镇、开元乡、徐家镇、鲜店乡等部分村子的方言都是[永州方言](../Page/永州.md "wikilink")。湘语[沅江话是全县第三大的方言](../Page/沅江话.md "wikilink")，主要分布在今天的龙蚕镇、天成乡、兴旺镇、凤石乡的部分乡村，说[沅江话的仅人口只占全县人口的](../Page/沅江.md "wikilink")2%左右。其他的还有[客家话方言岛分布](../Page/客家话.md "wikilink")。

## 姓氏

蓬安县的姓氏和四川其它地区的姓氏差不多，主要有[唐](../Page/唐姓.md "wikilink")，[罗](../Page/罗姓.md "wikilink")，[胡](../Page/胡姓.md "wikilink")，[吴](../Page/吴姓.md "wikilink")，[杨](../Page/杨姓.md "wikilink")，[周等](../Page/周姓.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")，另外[百家姓人口前十位的](../Page/百家姓.md "wikilink")[姓氏在本县人口也有很多](../Page/姓氏.md "wikilink")。除此之外，蓬安县还有罕见的[母姓](../Page/母姓.md "wikilink")，[诸姓](../Page/诸姓.md "wikilink")，[祝姓](../Page/祝姓.md "wikilink")，[伍姓](../Page/伍姓.md "wikilink")，[庹姓](../Page/庹姓.md "wikilink")，[雷姓等姓氏](../Page/雷姓.md "wikilink")。

## 名人

蓬安是[西汉初年著名的大辞赋家](../Page/西汉.md "wikilink")，[文学家](../Page/文学家.md "wikilink")，[诗人](../Page/诗人.md "wikilink")，[政治家](../Page/政治家.md "wikilink")[司马相如的故乡](../Page/司马相如.md "wikilink")（一说司马相如为四川成都人），直到现在，蓬安本地还有司马相如的故宅、洗墨池、弹琴台、练剑台、相如里、文君里等十多处遗址遗迹。2010年，有九十多名专家学者签名呼吁恢复相如县的名字，以纪念司马相如\[3\]。2012年，纪念司马相如的纪录片《寻找司马相如》亦在蓬安开机\[4\]。除此之外，蓬安还是[农学家](../Page/农学家.md "wikilink")[兰梦九](../Page/兰梦九.md "wikilink")、哲学家[伍非百](../Page/伍非百.md "wikilink")、[藏学家](../Page/藏学家.md "wikilink")[张怡荪](../Page/张怡荪.md "wikilink")、[数学](../Page/数学.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")[魏时珍](../Page/魏时珍.md "wikilink")、革命先烈[王白与](../Page/王白与.md "wikilink")，医学家[沈琼](../Page/沈琼.md "wikilink")，[中华民国国防部次长](../Page/中华民国国防部.md "wikilink")，[国民革命军二级上将](../Page/国民革命军.md "wikilink")[萧毅肃等人的故乡](../Page/萧毅肃.md "wikilink")。

## 行政区划

  - 蓬安县全县15个镇：相如、锦屏、金溪、徐家、巨龙、河舒、兴旺、罗家、利溪、正源、龙云、龙蚕、杨家、福德、银汉。
  - 24个乡：睦坝、鲜店、石孔、金甲、茶亭、诸家、平头、高庙、济渡、骑龙、群乐、三坝、新园、开元、碧溪、凤石、南燕、天成、新河、海田、石梁、两路、长梁、柳滩。

<!-- end list -->

  - 蓬安县政府驻地[相如镇](../Page/相如镇.md "wikilink")。

## 小吃

  - 河舒豆腐
  - [川北凉粉](../Page/川北凉粉.md "wikilink")
  - [锅盔](../Page/锅盔.md "wikilink")
  - 姚[麻花](../Page/麻花.md "wikilink")
  - 唐氏米[凉粉](../Page/凉粉.md "wikilink")
  - 吴馓子
  - [樟茶鸭](../Page/樟茶鸭.md "wikilink")

## 旅游景点

  - [嘉陵第一桑梓](../Page/嘉陵第一桑梓.md "wikilink")
  - 小乐山摩崖石刻
  - 量金斗
  - 桃花村
  - 太阳岛
  - 月亮岛
  - 周子古镇
  - 龙角山
  - 白云山
  - 大深南海
  - 铜鼓寨
  - 白云寨
  - 南充白云避暑山庄
  - 南充白云山
  - 利溪镇大夫第
  - 锦屏古镇
  - 蓬安凤凰谷（凤凰山）

## 图片集

## 参考资料

[蓬安县](../Category/蓬安县.md "wikilink")
[县](../Category/南充区县市.md "wikilink")
[南充](../Category/四川省县份.md "wikilink")

1.
2.
3.
4.