[RIGP320.jpg](https://zh.wikipedia.org/wiki/File:RIGP320.jpg "fig:RIGP320.jpg")
**Radeon
IGP**是一款[ATi晶片組](../Page/ATi.md "wikilink")，於2002年首次發佈。而ATI的第一款晶片組則是**S1-370TL**，支援[Pentium
III處理器](../Page/Pentium_III.md "wikilink")，支援雙通道記憶體，並集成了ArtX的顯示核心\[1\]。這個晶片組系列，明顯是衝着[nVidia的](../Page/nVidia.md "wikilink")**[nForce](../Page/nForce.md "wikilink")**
420/220而來。這是一款整合形晶元組，意即北桥整合了[顯示核心](../Page/顯示核心.md "wikilink")。初次發佈時公佈了兩款[北桥](../Page/北桥.md "wikilink")，针对主流桌面市场的**RADEON
IGP 320**，它支持[AMD](../Page/AMD.md "wikilink")
[Athlon和](../Page/Athlon.md "wikilink")[Duron处理器](../Page/Duron.md "wikilink")，其後再推出應用於[Intel处理器平臺的](../Page/Intel.md "wikilink")**IGP340**晶元組，功能於IGP320相似。另外一款是针对主流[笔记本市场的](../Page/笔记本.md "wikilink")**RADEON
IGP 320M**，它支持移动形[AMD](../Page/AMD.md "wikilink") [Athlon
4和移动形](../Page/Athlon_4.md "wikilink")[Duron处理器](../Page/Duron.md "wikilink")。同時公佈了兩款[南桥](../Page/南桥.md "wikilink")（ATi將南桥稱為**IXP**（通信处理芯片）)，**IXP
200**和**IXP 250**，两者都可以被笔记本或桌面电脑所利用。兩者分別在於IXP
250是针对商业市场而提供較高级的管理功能，例如遥控启动代理，桌面管理界面和局域网唤醒。

整合式图形芯片的結構，相當於[Radeon
VE的級別](../Page/Radeon_R100.md "wikilink")。它不支援硬件T\&L，只有一條像素流水線。縱使效能一般，但已是最當時笔记本平台，最強的整合式顯示核心。在桌面平台方面，[NVIDIA的](../Page/NVIDIA.md "wikilink")[nForce比較強](../Page/nForce.md "wikilink")，但不能應用在笔记本中。

當時，有一定數量的笔记本电脑廠商選用了該產品，作為一款新推出的晶片組，已算是了不起。背後原因，是由於整合式晶片組已集成顯示核心，省卻了獨立的顯示卡，笔记本的電池壽命就可以提升，而當時市場並沒有同類型的產品。而ATI在顯示核心的領域已具有一定的知名度，理應可以提供不錯的多媒體效果，所以廠商才會嘗試採用新產品。[惠普在](../Page/惠普.md "wikilink")2002年11月宣佈會採用ATI的晶片組。Radeon
IGP晶片組的南北桥是以32Bit/33MHz的接口連接，並與[PCI接口相容](../Page/PCI.md "wikilink")，意味著廠商可以使用第三方南桥。由於ATI的南桥晶片並不穩定，所以有笔记本电脑廠商利用[ALi的南桥取代之](../Page/ALi.md "wikilink")。而主機版廠商Sapphire，就利用[VIA的VT](../Page/VIA.md "wikilink")686B南桥去配合IGP
340北桥。

驅動程式方面，催化劑 4.8已開始支援Radeon IGP的整合式顯示核心。縱使Radeon
IGP並沒有在市場上盛行，但為ATI累積了不少寶貴經驗。

## Radeon IGP规格

  - 支援DDR 266單通道記亿體，並可分享為整合式图形芯片的显存
  - ATI POWERPLAY电源管理技术
  - 支援[AGP](../Page/AGP.md "wikilink") 4X接口
  - 內建整合式图形芯片
      - 支援Z轴快速清空
      - 支援视频输出，和LVDS接口
      - 支持HYDRAVISION双显示輸出
      - PIXEL TAPESTRY和VIDEO IMMERSION引擎
  - [3Com以太网](../Page/3Com.md "wikilink")
  - 6个[USB](../Page/USB.md "wikilink") 2.0端口
  - 5.1声道数字环绕声卡
  - [PCI](../Page/PCI.md "wikilink") 2.3扩展插槽

## [Intel处理器平臺](../Page/Intel.md "wikilink")

  - RADEON IGP340 - 支援[Pentium
    4](../Page/Pentium_4.md "wikilink")，533/400MHzFSB
  - RADEON IGP330 - 支援[Pentium
    4](../Page/Pentium_4.md "wikilink")，400MHzFSB
  - RADEON IGP340M - 支援[Pentium
    4M](../Page/Pentium_4M.md "wikilink")，400MHzFSB

## [AMD处理器平臺](../Page/AMD.md "wikilink")

  - RADEON IGP320 - 支援[AMD](../Page/AMD.md "wikilink")
    [Athlon和](../Page/Athlon.md "wikilink")[Duron](../Page/Duron.md "wikilink")，266/200MHzFSB
  - RADEON IGP320M - 支援移动形[AMD](../Page/AMD.md "wikilink") [Athlon
    4和移动形](../Page/Athlon_4.md "wikilink")[Duron](../Page/Duron.md "wikilink")，266/200MHzFSB

## 參考文獻

[Category:主板](../Category/主板.md "wikilink")
[Category:冶天科技](../Category/冶天科技.md "wikilink")
[Category:AMD晶片組](../Category/AMD晶片組.md "wikilink")

1.  \[ ATI Radeon IGP芯片组预览\]