<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")<small>《相對濕度》也是[香港歌手](../Category/香港歌手.md "wikilink")[鄭希怡的一首作品](../Page/鄭希怡.md "wikilink")。</small>

</div>

[Umidaderelativa.jpg](https://zh.wikipedia.org/wiki/File:Umidaderelativa.jpg "fig:Umidaderelativa.jpg")正在紀錄相對濕度\]\]

**相對濕度**（[英文](../Page/英文.md "wikilink")：relative
humidity）是指單位體積[空氣中](../Page/空氣.md "wikilink")，實際[水蒸氣的分压與相同溫度和體積下水](../Page/水.md "wikilink")[饱和蒸气压的百分比](../Page/饱和蒸气压.md "wikilink")。也就是[绝对湿度与最高湿度之间的比](../Page/绝对湿度.md "wikilink")，它的值显示[水蒸气的饱和度有多高](../Page/水蒸气.md "wikilink")。在當前的[氣溫之下](../Page/氣溫.md "wikilink")，空氣裏的水分含量達至飽和，相對濕度就是100%。空气中相对湿度超过[100%時](../Page/100%.md "wikilink")，水蒸气一般會凝结成水出来。随着温度的增高空气中可以含的水就越多，也就是说，在同样多的水蒸气的情况下温度降低相对湿度就会升高。因此在提供相对湿度的同时也必须提供[温度的数据](../Page/温度.md "wikilink")。通过最高湿度和温度也可以计算出[露点](../Page/露点.md "wikilink")。

## 計算公式

以下是计算相对湿度的公式：

\(\varphi := \frac {\rho_w}{\rho_{w, max}} \cdot 100\ \% = \frac {e}{E} \cdot 100\ \% = \frac {s}{S} \cdot 100\ \%\)

其中的符号分别是：

\[\rho_w\] –
[绝对湿度](../Page/绝对湿度.md "wikilink")，单位是[克](../Page/克.md "wikilink")/[立方米](../Page/立方米.md "wikilink")

\[\rho_{w, max}\] – [最高湿度](../Page/最高湿度.md "wikilink")，单位是克/立方米

  -
    *e* –
    [蒸汽压](../Page/蒸汽压.md "wikilink")，单位是[帕斯卡](../Page/帕斯卡.md "wikilink")
    *E* – [飽和蒸氣壓](../Page/飽和蒸氣壓.md "wikilink")，单位是帕斯卡
    *s* – [比湿](../Page/比湿.md "wikilink")，单位是克/千克
    *S* – [最高比湿](../Page/最高比湿.md "wikilink")，单位是克/千克

通常气温的改变比水汽压的改变要快，所以温度往往起主导作用；当水汽压一定时，温度降低则相对湿度增大，温度升高则相对湿度减小；雾和霜在夜间或清晨产生就是这个道理。

## 應用

相對濕度常用於[氣象預測之中](../Page/氣象.md "wikilink")。它反映了[降雨](../Page/雨.md "wikilink")、[有霧的可能性](../Page/霧.md "wikilink")。在炎熱的天氣之下，高的相對濕度會讓人類（和其他動物）感到更熱，因為這妨礙了[汗水的揮發](../Page/汗.md "wikilink")。人類可以從而制訂出[酷熱指數](../Page/酷熱指數.md "wikilink")。[香港天文台會因應溫度和相對濕度](../Page/香港天文台.md "wikilink")，而發出「[酷熱天氣警告](../Page/酷熱天氣警告.md "wikilink")」。另外，秋冬兩季和初春，香港受到東北季候風帶來的乾燥大陸性氣流影響，濕度通常較低，風速通常較高，任何物品，不論室內或戶外，都會變得乾燥，火警更容易發生。遇上陽光普照的天氣，情況便會更為嚴重，即使是星星之火，轉瞬間便會釀成火災。天文台在決定是否發出[火災危險警告時會根據有利於火警發生及擴散的天氣因素](../Page/香港火災危險警告.md "wikilink")，如低濕度及高風速，以及由漁農自然護理署提供有關草木乾燥情況的資料。

## 參見

  - [湿度](../Page/湿度.md "wikilink")
  - [绝对湿度](../Page/绝对湿度.md "wikilink")

[Category:测湿法](../Category/测湿法.md "wikilink")
[Category:大气热力学](../Category/大气热力学.md "wikilink")
[Category:物理量](../Category/物理量.md "wikilink")