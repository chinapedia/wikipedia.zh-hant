[Galvanometer_1890_drawing.png](https://zh.wikipedia.org/wiki/File:Galvanometer_1890_drawing.png "fig:Galvanometer_1890_drawing.png")可以用來測量電流。\]\]

**安培**，简称**安**，是[国际单位制中](../Page/国际单位制.md "wikilink")[电流强度的](../Page/电流强度.md "wikilink")[单位](../Page/单位_\(度量衡\).md "wikilink")，符号是**A**\[1\]\[2\]\[3\]。同时它也是国际单位制中七个基本单位之一\[4\]。安培是以法国数学家和物理学家[安德烈-马里·安培命名的](../Page/安德烈-马里·安培.md "wikilink")，为了纪念他在[经典电磁学方面的贡献](../Page/经典电磁学.md "wikilink")。

实际情况中，安培是对单位时间内通过[导体横截面的](../Page/导体.md "wikilink")[电荷量的度量](../Page/电荷量.md "wikilink")。1[秒内通过横截面的](../Page/秒.md "wikilink")[电量为](../Page/电量.md "wikilink")1[库仑](../Page/库仑.md "wikilink")（个电子的电量）时，电流大小為1安培。\[5\]

比安培小的電流可以用[毫安](../Page/毫安.md "wikilink")、[微安等單位表示](../Page/微安.md "wikilink")。

  - 1安（A）= 1000毫安（mA）
  - 1毫安（mA）= 1000微安（μA）

## 定义

[Ampere-def-en.svg](https://zh.wikipedia.org/wiki/File:Ampere-def-en.svg "fig:Ampere-def-en.svg")
[安培力定律证明在两根通电的平行直导线之间存在吸引力或排斥力](../Page/安培力定律.md "wikilink")\[6\]\[7\]。这个力被用于安培的正式定义。

1948年第九届国际计量大会决定定义安培为：

[电荷量的](../Page/电荷量.md "wikilink")[国际单位制单位](../Page/国际单位制.md "wikilink")1[库仑表示](../Page/库仑.md "wikilink")1安培的电流1秒内通过横截面的电量\[8\]。同样，1安培就是1库仑的电荷在一秒内通过横截面时的电流强度。

## 历史

安培最初是被定义为[厘米-克-秒制中电流单位](../Page/厘米-克-秒制.md "wikilink")[绝对安培的十分之一](../Page/绝对安培.md "wikilink")\[9\]。如此确定它的大小是为了保证从[国际单位制中的其他单位推导安培得到的值比较合适](../Page/国际单位制.md "wikilink")。

“国际安培”是一个较早的电流单位，定义为使[硝酸银溶液中每秒析出](../Page/硝酸银.md "wikilink")克[银的电流](../Page/银.md "wikilink")\[10\]。之后，更精确地测量发现，这一电流是0.99985 A。

1948年第九届国际计量大会决定定义安培为：

2005年，[国际计量委员会同意研究将元电荷电荷量用于安培定义的可能](../Page/国际计量委员会.md "wikilink")。新的定义在2014年的第25屆[国际度量衡委员会上被討論](../Page/国际度量衡委员会.md "wikilink")，将于2019年5月20日生效。\[11\]\[12\]

2018年第二十六届国际计量大会通过给予[元电荷确定的电荷量](../Page/元电荷.md "wikilink")，确定了安培的新定义。自2019年5月20日起，元电荷的电荷量被确定为
\(1.602 176 634\times 10^{-19} C\) ，而 \(C = A \cdot s\)。由此，1
安培所代表的电流强度大小由元电荷电荷量和秒确定。\[13\]

## 应用

安培这一单位与和[欧姆定律中的单位](../Page/欧姆定律.md "wikilink")[伏特](../Page/伏特.md "wikilink")、[欧姆相联系](../Page/欧姆.md "wikilink")。而在[约瑟夫森效应和](../Page/约瑟夫森效应.md "wikilink")[量子霍尔效应中也都各自有所应用](../Page/量子霍尔效应.md "wikilink")。
\[14\]

现在，建立一安培的技术已经能够达到10<sup>7</sup>的[逼近误差](../Page/逼近误差.md "wikilink")。
\[15\]

## 例子

  - 计算器工作电流1×10<sup>-4</sup>A\[16\]
  - \-{zh-cn:手机;zh-tw:行動電話}-充电电流0.8-2A
  - 闪电约1×10<sup>4</sup>A\[17\]
  - 住宅[配電箱約](../Page/配電箱.md "wikilink")60A
  - 住宅[斷路器](../Page/斷路器.md "wikilink")，常見的有10/20A
  - 塑殼斷路器(MCCB)，常見200-800A
  - 空氣斷路器(ACB)，用於低壓電（380V），常見1600/2500/4000A
  - 用於配電的1650k[VA](../Page/VA.md "wikilink")[三相](../Page/三相.md "wikilink")[變壓器](../Page/變壓器.md "wikilink")，低壓線圈（380V）最大輸出2500A

## 参见

  - [安培计](../Page/安培计.md "wikilink")
  - [电流](../Page/电流.md "wikilink")
  - [欧姆定律](../Page/欧姆定律.md "wikilink")
  - [库仑](../Page/库仑.md "wikilink")
  - [毕奥-萨伐尔定律](../Page/毕奥-萨伐尔定律.md "wikilink")

## 注释

## 参考资料

## 外部链接

  - [The NIST Reference on Constants, Units, and
    Uncertainty](http://physics.nist.gov/cuu/)
  - [NIST *Definition of ampere and
    μ<sub>0</sub>*](http://physics.nist.gov/cuu/Units/ampere.html)

[A](../Category/电学单位.md "wikilink")
[A](../Category/国际单位制基本单位.md "wikilink")
[Category:电磁学](../Category/电磁学.md "wikilink")

1.

2.

3.  [Base unit definitions:
    Ampere](http://physics.nist.gov/cuu/Units/ampere.html).
    Physics.nist.gov. Retrieved on 2010-09-28.

4.  另外六个是[米](../Page/米_\(单位\).md "wikilink")、[开尔文](../Page/开尔文.md "wikilink")、[秒](../Page/秒.md "wikilink")、[摩尔](../Page/摩尔_\(单位\).md "wikilink")、[坎德拉和](../Page/坎德拉.md "wikilink")[千克](../Page/千克.md "wikilink")

5.

6.

7.  .

8.  .

9.  .

10.

11.

12.

13. [Resolutions of the CGPM - 26th meeting of the CGPM: 13-16
    November 2018](https://www.bipm.org/utils/common/pdf/CGPM-2018/26th-CGPM-Resolutions.pdf)

14. .

15.
16. 以CASIO fx-82ES 1.5V 0.0002W计

17. 物理.高中二年级上册.上海科技出版社