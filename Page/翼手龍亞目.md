**翼手龍亞目**（學名：Pterodactyloidea），[希臘文意思為](../Page/希臘文.md "wikilink")“有翼的手指”，是[翼龍目兩個亞目之一](../Page/翼龍目.md "wikilink")，包括翼龍類的較先進物種。牠們在中[侏罗紀出現](../Page/侏罗紀.md "wikilink")，與[喙嘴翼龍亞目的差別在於牠們的短尾巴](../Page/喙嘴翼龍亞目.md "wikilink")，與翼上的長[指骨](../Page/指骨.md "wikilink")。最先進的物種缺乏牙齒。許多物種頭顱上有發展良好的冠飾，這是一種展示物，[夜翼龍與](../Page/夜翼龍.md "wikilink")[古神翼龍擁有巨大的冠飾](../Page/古神翼龍.md "wikilink")。翼手龍類，尤其是[神龍翼龍科](../Page/神龍翼龍科.md "wikilink")，是[白堊紀末最後存活的翼龍目動物](../Page/白堊紀.md "wikilink")。[白堊紀-第三紀滅絕事件滅絕了翼龍類](../Page/白堊紀-第三紀滅絕事件.md "wikilink")、[恐龍](../Page/恐龍.md "wikilink")、與大多數[海生爬行動物](../Page/海生爬行動物.md "wikilink")。

目前已知最早的翼手龍類化石，發現於[英國的Taynton石灰岩組地層](../Page/英國.md "wikilink")，地質年代約1億6600萬年前，相當於[侏儸紀中期的](../Page/侏儸紀.md "wikilink")[巴通階](../Page/巴通階.md "wikilink")。這些標本極可能屬於[梳頜翼龍超科](../Page/梳頜翼龍超科.md "wikilink")\[1\]。

在一般大眾的用語裡，翼手龍（*Pterodactyl*）通常用來稱呼翼手龍亞目動物，也可用來稱呼翼龍目動物，或是特別稱呼[翼手龍屬](../Page/翼手龍屬.md "wikilink")。翼手龍類著名的屬有：翼手龍、[準噶爾翼龍](../Page/準噶爾翼龍.md "wikilink")、[無齒翼龍](../Page/無齒翼龍.md "wikilink")、[風神翼龍](../Page/風神翼龍.md "wikilink")。

## 分類

### 種系發生學

以下演化樹是根據大衛·安文在2003年的研究\[2\]：

不確定位置：[阿拉利坡龍](../Page/阿拉利坡龍.md "wikilink")（*Araripedactylus*）、[懷俄明翼龍](../Page/懷俄明翼龍.md "wikilink")（*Wyomingopteryx*）、[班尼特翼龍](../Page/班尼特翼龍.md "wikilink")（*Bennettazhia*）、[皮翼龍](../Page/皮翼龍.md "wikilink")（*Dermodactylus*）、[郝氏翼龍](../Page/郝氏翼龍.md "wikilink")（*Haopterus*）、[買薩翼龍](../Page/買薩翼龍.md "wikilink")（*Mesadactylus*）、[槌喙龍](../Page/槌喙龍.md "wikilink")（*Criorhynchus*）。

## 參考資料

[翼手龍亞目](../Category/翼手龍亞目.md "wikilink")

1.  Buffetaut, E. and Jeffrey, P. (2012). "A ctenochasmatid pterosaur
    from the Stonesfield Slate (Bathonian, Middle Jurassic) of
    Oxfordshire, England." *Geological Magazine*, (advance online
    publication)
2.  Unwin, D. M., (2003). "On the phylogeny and evolutionary history of
    pterosaurs." Pp. 139-190. in Buffetaut, E. & Mazin, J.-M., (eds.)
    (2003). *Evolution and Palaeobiology of Pterosaurs*. Geological
    Society of London, Special Publications 217, London, 1-347.