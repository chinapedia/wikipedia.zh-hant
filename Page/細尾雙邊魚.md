**細尾雙邊魚**，又稱**尾紋雙邊魚**，俗名玻璃魚、大面側仔為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目](../Page/鱸亞目.md "wikilink")[雙邊魚科的其中一個](../Page/雙邊魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[東非](../Page/東非.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[留尼旺](../Page/留尼旺.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[加羅林群島](../Page/加羅林群島.md "wikilink")、[越南](../Page/越南.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[所羅門群島等海域](../Page/所羅門群島.md "wikilink")。

## 特徵

本
魚體側扁，體較細長，呈長橢圓形，口大。魚體略透明，無條紋與斑點。頜骨、鋤骨和腭骨均具呈絨毛狀齒。體被圓鱗，易脫落，頰部鱗片只有1列，易與其他種類
分別。尾鰭叉形。背鰭硬棘8枚、背鰭軟條9至10枚、臀鰭硬棘3枚、臀鰭軟條9至10枚。眶前骨及前鰓蓋骨均雙重緣，具細齒或小棘。體長可達14公分。

## 习性

本魚喜棲息於水流不強的[潟湖或河口淺水區](../Page/潟湖.md "wikilink")0至10公尺水深区域，對鹽度耐受性強，但離水便馬上死亡。常背對著[紅樹林的樹根或人工構造物](../Page/紅樹林.md "wikilink")，頂流群游以吃食順流而來的浮游動物，屬肉食性。產浮游性卵。

## 价值

非食用性魚類，無經濟價值。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[urotaenia](../Category/雙邊魚屬.md "wikilink")