**曼弗雷德·艾根**（，\[1\]），[德国](../Page/德国.md "wikilink")[化学家及](../Page/化学家.md "wikilink")[生物物理学家](../Page/生物物理学.md "wikilink")，曾任位于[哥廷根的](../Page/哥廷根.md "wikilink")[马克斯·普朗克生物物理化学所主任](../Page/马克斯·普朗克学会.md "wikilink")。

## 生平

1927年艾根出生于德国[波鸿](../Page/波鸿.md "wikilink")。完成了在高中的物理学和化学学习后，1944年他来到[哥廷根](../Page/哥廷根.md "wikilink")，进入[哥廷根大学学习](../Page/哥廷根大学.md "wikilink")。1951年他获得物理学[博士学位之后](../Page/博士.md "wikilink")，在该校的物理化学研究所从事科学研究工作。1953年进入卡尔·弗雷德里希·彭霍费尔研究所从事[物理化学研究工作](../Page/物理化学.md "wikilink")。1957年他成为研究所主任。1958年他当选为[马克斯·普朗克学会的会员](../Page/马克斯·普朗克学会.md "wikilink")，1964年成为马普所首席科学家。1967年获得[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")，同年任马普学会会长至1970年。1971年彭霍费尔研究所扩建并更名为马克斯·普朗克生物物理化学研究所。

艾根由于对极快[化学反应研究的突出成就](../Page/化学反应.md "wikilink")，与[英国的](../Page/英国.md "wikilink")[罗纳德·乔治·雷福德·诺里奇和](../Page/罗纳德·乔治·雷福德·诺里奇.md "wikilink")[乔治·波特共获](../Page/乔治·波特.md "wikilink")1967年的[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。艾根的主要贡献是他和合作者发展了研究[溶液中](../Page/溶液.md "wikilink")[半寿期在](../Page/半寿期.md "wikilink")[毫秒以下的极快反应](../Page/毫秒.md "wikilink")[动力学的弛豫技术](../Page/动力学.md "wikilink")。这种方法可以对在10秒内完成的极快反应进行观测和研究。此后，艾根将研究兴趣转入自组织系统和[生物进化研究](../Page/生物进化.md "wikilink")。1979年他和[彼得·舒斯特阐述了](../Page/彼得·舒斯特.md "wikilink")理论。

## 参考资料

[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:物理化學](../Category/物理化學.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:马克斯·普朗克学会人物](../Category/马克斯·普朗克学会人物.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")

1.