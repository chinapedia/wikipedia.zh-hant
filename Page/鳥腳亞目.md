**鳥腳亞目**（Ornithopoda）是[鳥臀目](../Page/鳥臀目.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")，牠們起初是小型、二足、快速奔跑的[草食性動物](../Page/草食性.md "wikilink")，後來種類與體型都逐漸變大、變多，直到牠們成為[白堊紀時代裡最成功的](../Page/白堊紀.md "wikilink")[草食性恐龍之一](../Page/草食性.md "wikilink")，並成為[北美洲的優勢物種](../Page/北美洲.md "wikilink")。鳥腳類恐龍在演化上最主要的優點是咀嚼方式的進展，是[爬行動物裡最複雜的咀嚼方式](../Page/爬行動物.md "wikilink")，可與現代[哺乳類相比](../Page/哺乳類.md "wikilink")，例如[牛](../Page/牛.md "wikilink")。牠們在[鴨嘴龍科時達到頂峰](../Page/鴨嘴龍科.md "wikilink")，而與其他恐龍一起在[白堊紀-第三紀滅絕事件中滅絕](../Page/白堊紀-第三紀滅絕事件.md "wikilink")。所有大陸都發現了牠們的蹤影，雖然在[南極洲發現的化石還未命名](../Page/南極洲.md "wikilink")，而牠們在[南半球通常是罕見的](../Page/南半球.md "wikilink")。

## 敘述

[Iguanodon_feet.JPG](https://zh.wikipedia.org/wiki/File:Iguanodon_feet.JPG "fig:Iguanodon_feet.JPG")的後腳掌，具有三趾\]\]
鳥腳亞目意為“鳥的腳部”，在[希臘文裡](../Page/希臘文.md "wikilink")*ornis*意為鳥，*pous*意為腳；這名稱指的是牠們腳掌的三個腳趾，類似[鳥類](../Page/鳥類.md "wikilink")，但許多早期物種有四個腳趾。牠們的其他特徵是沒有護甲、發展出角質喙狀嘴、修長的[恥骨最後延展超過](../Page/恥骨.md "wikilink")[腸骨](../Page/腸骨.md "wikilink")、下頜沒有洞孔。許多鳥腳類恐龍的[肋骨兩側](../Page/肋骨.md "wikilink")，有多塊薄的[軟骨骨板](../Page/軟骨.md "wikilink")，其功能未明；有些骨板在化石化的過程中保存下來。目前已在[稜齒龍](../Page/稜齒龍.md "wikilink")、[奧斯尼爾洛龍](../Page/奧斯尼爾洛龍.md "wikilink")、[帕克氏龍](../Page/帕克氏龍.md "wikilink")、[小頭龍](../Page/小頭龍.md "wikilink")、[奇異龍](../Page/奇異龍.md "wikilink")、[巨謎龍的化石發現這些骨板](../Page/巨謎龍.md "wikilink")\[1\]\[2\]。

早期鳥腳類恐龍的身長只有約1公尺，移動速度可能很快。牠們有僵直的尾巴，如同[獸腳類恐龍](../Page/獸腳類.md "wikilink")，在牠們奔跑時可協助維持平衡。較晚期鳥腳類演化成以四足方式吃低矮[植被](../Page/植被.md "wikilink")；牠們的脊柱成彎曲狀，類似現代地面草食性[動物的脊柱](../Page/動物.md "wikilink")，例如[野牛](../Page/野牛.md "wikilink")。當牠們為了低矮植被而演化得更為彎曲時，牠們變成半四足動物；奔跑時仍以二足方式，能輕易接觸到樹的頂端；但當行走或吃低矮植被時的大部分時間是採四足方式。[稜齒龍科的分類有很大爭議](../Page/稜齒龍科.md "wikilink")，稜齒龍科原本包含非[禽龍類的二足](../Page/禽龍類.md "wikilink")[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")，但近年[系統發生學研究認為稜齒龍科是個](../Page/系統發生學.md "wikilink")[並系群](../Page/並系群.md "wikilink")，目前唯一的可確認屬只有稜齒龍\[3\]。

更晚鳥腳類的體型更為大型，但並未達到長頸、長尾[蜥腳類恐龍般的巨大體型](../Page/蜥腳類.md "wikilink")，部分蜥腳類恐龍的[生態位被鳥腳類取代](../Page/生態位.md "wikilink")。最大型的鳥腳類，例如：[埃德蒙頓龍與](../Page/埃德蒙頓龍.md "wikilink")[山東龍](../Page/山東龍.md "wikilink")，身長並未達到15公尺。

## 分類

[Hadrosauroids.jpg](https://zh.wikipedia.org/wiki/File:Hadrosauroids.jpg "fig:Hadrosauroids.jpg")
在過去分類歷史上，大多數未確定的二足[鳥臀目恐龍被劃分到鳥腳亞目](../Page/鳥臀目.md "wikilink")。其中大多數以重新分類為基礎[頭飾龍類](../Page/頭飾龍類.md "wikilink")；有些則建立起牠們的分類，如[腫頭龍下目](../Page/腫頭龍下目.md "wikilink")。

### 分類學

[Largestornithopods_scale.png](https://zh.wikipedia.org/wiki/File:Largestornithopods_scale.png "fig:Largestornithopods_scale.png")、[巨保羅龍](../Page/巨保羅龍.md "wikilink")、[鴨龍](../Page/鴨龍.md "wikilink")、[埃德蒙頓龍](../Page/埃德蒙頓龍.md "wikilink")。\]\]
鸟脚类通常归类为亚目等级，在鸟臀目之下。该分类等级已被大部分恐龙学家弃用，但仍然有学者继续使用该分类法，虽然资料来源有所不同。Benton于2004年将鸟脚类分类在下目等级，角足亚目（角足类）之下（原为未排序演化支），而其他人如Ibiricu等人于2010年将其保留为原本的亚目等级\[4\]

### 系统发生学

以下[演化树复原自Zheng与同事](../Page/演化树.md "wikilink")2009年的研究。\[5\]

以下演化树由Butler等人于2011年提出。\[6\]

## 參考資料

## 外部連結

  - [鳥腳下目](https://web.archive.org/web/20130927064547/http://www.thescelosaurus.com/ornithopoda.htm),
    by Justin Tweet, from *Thescelosaurus*\!
  - [GEOL 104 dinosaurs: a Natural history, ornithopoda: beaks, bills &
    crests](http://www.geol.umd.edu/~tholtz/G104/10419orni.htm), by
    Thomas R. Holtz, Jr.

[鳥腳下目](../Category/鳥腳下目.md "wikilink")

1.
2.
3.
4.  Ibiricu, L. M.; Martínez, R. D.; Lamanna, M. C.; Casal, G. A.; Luna,
    M.; Harris, J. D. and Lacovara, K. J. (2010). "A Medium-Sized
    Ornithopod (Dinosauria: Ornithischia) from the Upper Cretaceous Bajo
    Barreal Formation of Lago Colhué Huapi, Southern Chubut Province,
    Argentina." *Annals of Carnegie Museum*, **79**(1): 39-50.
5.
6.