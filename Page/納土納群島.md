**納土納群島**（[印尼語](../Page/印尼語.md "wikilink")：****）是[印度尼西亞的一個](../Page/印度尼西亞.md "wikilink")[群島](../Page/群島.md "wikilink")，位於[馬來半島和](../Page/馬來半島.md "wikilink")[婆羅洲島之間的](../Page/婆羅洲.md "wikilink")[南海内](../Page/南海.md "wikilink")，由272個島嶼組成，面积2,110平方公里，人口約9萬，屬[廖内群岛省管轄](../Page/廖内群岛省.md "wikilink")。納土納群島以西為[阿南巴斯群島](../Page/阿南巴斯群島.md "wikilink")。

1980年代末人口约1.5万，当时[華人占八成以上](../Page/華人.md "wikilink")。印尼政府近年来大量向纳土纳岛移民，其结果使得仅该主岛人数就增至85000。

印尼国家石油公司正与美国[埃克森美孚合作](../Page/埃克森美孚.md "wikilink")，在岛上开采[石油与](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")。

各國官方宣稱此群島上並不存在主權爭議但當地居民對此有所顧慮\[1\]。印尼和[馬來西亞曾對纳土纳群岛有主權爭議](../Page/馬來西亞.md "wikilink")，經仲裁後判予印尼。另外印尼以此為基點的專屬經濟海域與中國的[南海九段線重曡](../Page/南海九段線.md "wikilink")\[2\]。2015年11月12日中国外交部的例行記者會明确表明纳土纳群岛主权属于印尼。\[3\]\[4\]

## 参考文献

## 外部链接

  -
{{-}}

[Category:南海諸島](../Category/南海諸島.md "wikilink")
[Category:印尼群島](../Category/印尼群島.md "wikilink")
[Category:廖内群岛省地理](../Category/廖内群岛省地理.md "wikilink")

1.
2.  [南海主权争端的现状](http://niis.cass.cn/upload/2012/12/d20121209020249877.pdf)

3.  [2015年11月12日
    外交部发言人洪磊主持例行记者会](http://www.fmprc.gov.cn/web/fyrbt_673021/t1314278.shtml)
4.