****（）是第40颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1856年3月31日发现被[赫尔曼·迈尔·萨洛蒙·戈尔德施密特](../Page/赫尔曼·迈尔·萨洛蒙·戈尔德施密特.md "wikilink")。它的名字是以希腊神话中[阿瑞斯和](../Page/阿瑞斯.md "wikilink")[阿佛羅狄忒的女儿和谐女神](../Page/阿佛羅狄忒.md "wikilink")[哈耳摩尼亚命名的](../Page/哈耳摩尼亚.md "wikilink")。

谐神星轨道的[近日点离太阳](../Page/近日点.md "wikilink")2.1660[天文单位](../Page/天文单位.md "wikilink")，[遠日點离太阳](../Page/遠日點.md "wikilink")2.3740天文单位，[公转周期为](../Page/公转周期.md "wikilink")1247.514天。轨道与[黄道的偏角为](../Page/黄道.md "wikilink")4.2555°，[離心率为](../Page/離心率.md "wikilink")0.0471。

的[直径为](../Page/直径.md "wikilink")107.6千米，[质量为](../Page/质量.md "wikilink")1.3×10<sup>18</sup>千克，自转周期为8小时55分钟。它的表面为[矽酸鹽](../Page/矽酸鹽.md "wikilink")，比较明亮，其[反照率为](../Page/反照率.md "wikilink")0.242。

## 参考资料

<references />

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1856年发现的小行星](../Category/1856年发现的小行星.md "wikilink")