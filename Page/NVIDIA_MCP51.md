**NVidia
C51芯片组**是[nVidia的](../Page/nVidia.md "wikilink")[K8](../Page/K8.md "wikilink")
IGP[晶片組](../Page/晶片組.md "wikilink")，於2005年推出。它是nVidia第一款為K8平台而設的整合型晶片組。它建了Geforce
6100顯示核心，完整支援[DirectX](../Page/DirectX.md "wikilink")
9.0和[HDR](../Page/HDR.md "wikilink")，是當時繒圖效能最佳的整合型晶片組。南橋的硬碟支援部分有小[BUG](../Page/BUG.md "wikilink")（有時不能啟动[NCQ](../Page/NCQ.md "wikilink")）。

nVidia眼見ATi在Intel和AMD平台晶片組市場漸有成績，逐在AMD平台晶片組下工夫，推出支援AMD
64-bit处理器的整合形晶片組。本來計劃推出Intel平台整合形晶片組（代号C60），但Intel推出內建ATI整合形晶片組的主機板，NVIDIA為免尷尬，決定終止C60計劃。

## 型號列表

  - C51G ， Geforce 6100， 不支援[Pure
    Video](../Page/Pure_Video.md "wikilink")
  - C51PV， Geforce 6150， 支援[Pure
    Video](../Page/Pure_Video.md "wikilink")， HD Video， TV Encoder， Dual
    Head/TMDS

## 顯示核心（北橋）技術

  - 90奈米制程
      - 2個Pixel Shader流水線
      - 1個Vertex Shader流水線
  - 支援Direct X 9.0c
  - 支援Shader Model 3.0
  - 顯示核心時脈
      - Geforce 6100 -- 425MHz
      - Geforce 6150 -- 475MHz
  - PCI-E Lanes
      - C51G ， Geforce 6100 -- 1條PCI-E x16 + 1條PCI-E x1 = 17
      - C51PV， Geforce 6150 -- 1條PCI-E x16 + 2條PCI-E x1 = 18

## 南橋技術

  - nForce 410
      - 10/100Mbps Ethernet
      - 2組SATA II接口
      - RAID模式支援：RAID 0、1
      - 7.1聲道音效（Azalia， HD Audio）

<!-- end list -->

  - nForce 430
      - Gigabit Ethernet + Active Armor Firewall
      - 4組SATA II接口
      - RAID模式支援：RAID 0、1、0+1及5
      - 7.1聲道音效（Azalia， [HD Audio](../Page/HD_Audio.md "wikilink")）

## 南北橋配搭

  - C51G ，GeForce 6100 配 nForce410/430
  - C51PV，GeForce 6150 配 nForce430

對手：[ATi](../Page/ATi.md "wikilink") [Xpress
200](../Page/Xpress_200.md "wikilink")

[Category:主板](../Category/主板.md "wikilink")
[Category:英伟达](../Category/英伟达.md "wikilink")