《**聖母瑪利亞**》（[意大利語](../Page/意大利語.md "wikilink")：*Madonna*）是一系列描繪[耶穌基督的母親](../Page/耶穌基督.md "wikilink")[瑪利亞的繪畫作品的統稱](../Page/瑪利亞.md "wikilink")（時常與嬰孩時期的耶穌在一起）。在西方[基督教美術界的稱呼可直譯為](../Page/基督教.md "wikilink")「我們的貴夫人」。

對聖母的描繪在[拜占庭時代開始興盛](../Page/拜占庭.md "wikilink")。早期的聖母像的意念[希臘語稱為](../Page/希臘語.md "wikilink")，意思是虔誠、懺悔和愛—對聖子耶穌和人類的無盡的愛。

[中世紀結束後對聖像出現新的想法](../Page/中世紀.md "wikilink")，繪畫技巧和理論的創新亦為畫壇帶來了不少鉅作，而聖母像的傳統亦為[文藝復興帶來巨大的衝擊](../Page/文藝復興.md "wikilink")。

## 著名的聖母畫像

  - 《[琴斯托霍瓦的黑色聖母](../Page/琴斯托霍瓦的黑色聖母.md "wikilink")》
  - 《[聖母子](../Page/聖母子_\(達文西\).md "wikilink")》，[達文西](../Page/達文西.md "wikilink")
  - 《[聖母瑪利亞](../Page/聖母瑪利亞_\(蒙克\).md "wikilink")》，[愛德華·蒙克](../Page/愛德華·蒙克.md "wikilink")
  - [聖母瑪利亞 (繪畫)維維日城堡](../Page/維維日城堡.md "wikilink")

### 聖母畫像

Image:Duccio The-Madonna-and-Child-with-Angels-1.jpg|*Madonna and
Angels*, [Duccio](../Page/Duccio.md "wikilink"), 1282 Image:Gaddi Agnolo
Coronation.jpg|[Agnolo Gaddi](../Page/Agnolo_Gaddi.md "wikilink"),
[Coronation of the
Virgin](../Page/Coronation_of_the_Virgin.md "wikilink"), c. 1380
Image:Jan van Eyck 070.jpg|[Madonna of Chancellor
Rolin](../Page/Madonna_of_Chancellor_Rolin.md "wikilink"),
[揚·范·艾克](../Page/揚·范·艾克.md "wikilink"),
[Burgundy](../Page/Burgundy_\(region\).md "wikilink"), c. 1435
Image:Enguerrand Charonton 001.jpg|An unusual French *Coronation* by
[Enguerrand Quarton](../Page/Enguerrand_Quarton.md "wikilink"), 1454
<File:Madonna> FiveAngels.jpg|*Madonna and five angels*,
[桑德罗·波提切利](../Page/桑德罗·波提切利.md "wikilink"), c. 1485-1490
Image:Madona del gran duque, por Rafael.jpg|[Madonna del
Granduca](../Page/Madonna_del_Granduca.md "wikilink"),
[拉斐尔·圣齐奥](../Page/拉斐尔·圣齐奥.md "wikilink"), 1505
Image:Dolorosa.jpg|[Dolorosa](../Page/Dolorosa.md "wikilink"),
[Murillo](../Page/巴托洛梅·埃斯特萬·牟利羅.md "wikilink"), 1665 Image:Sassoferrato
- Jungfrun i bön.jpg|Virgin Mary by
[Sassoferrato](../Page/Giovanni_Battista_Salvi_da_Sassoferrato.md "wikilink"),
17th century <File:Torelli> Virgin c.jpg| *Virgin and Child with Angels
and Saints*, [Felice Torelli](../Page/Felice_Torelli.md "wikilink"),
17th century <File:La_Vierge_au_lys.jpg>|*Virgin of the Lilies*,
[威廉·阿道夫·布格罗](../Page/威廉·阿道夫·布格罗.md "wikilink"), 1899

## 参考文献

## 外部链接

## 参见

  - [圣母子](../Page/圣母子.md "wikilink")

[圣母玛利亚相关绘画作品](../Category/圣母玛利亚相关绘画作品.md "wikilink")
[Category:東正教聖像](../Category/東正教聖像.md "wikilink")