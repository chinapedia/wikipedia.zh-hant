**汤姆·斯托帕德**爵士，[OM](../Page/功績勳章_\(英國\).md "wikilink")，[CBE](../Page/CBE.md "wikilink")（，），原名**托馬斯·施特勞斯勒**（），[英國劇作家](../Page/英國.md "wikilink")，曾獲得[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")。史塔伯德著名的劇本包括《The
Coast of Utopia》、《The Real Thing》、《Rosencrantz & Guildenstern Are
Dead》，他也有寫電影《Brazil》與《[莎翁情史](../Page/莎翁情史.md "wikilink")》的劇本。

## 早年生活

史塔佩於1937年出生於[捷克](../Page/捷克.md "wikilink")，1939年3月15日，史塔佩一家為了躲避[納粹的侵入](../Page/納粹.md "wikilink")，跟其他的猶太人搬到了[新加坡](../Page/新加坡.md "wikilink")，然而，1941年，日軍侵入[新加坡](../Page/新加坡.md "wikilink")，史塔佩一家又再度撤退到印度去，只是史塔佩與他的母親以及兄弟平安到達，父親Eugene
Straussler則是被日軍於日軍監獄營中處死。
1946年，史塔佩母親Martha選擇改嫁給英國上校Kenneth
Stoppard，[二次世界大戰結束後](../Page/二次世界大戰.md "wikilink")，全家移民到英國。
史塔佩在17歲之後就放棄求學，選擇走出社會。

## 戲院劇本作品

  - *Rosencrantz & Guildenstern Are Dead* (1966年)
  - *Enter a Free Man* (1968年)
  - *The Real Inspector Hound* (1968年)
  - *After Magritte* (1970年)
  - *Jumpers* (1972年)
  - *Travesties* (1974年)
  - *Dirty Linen and New-Found-Land* (1976年)
  - *Every Good Boy Deserves Favour* (1977年)
  - *Night and Day* (1978年)
  - ''Dogg's Hamlet, Cahoot's Macbeth '' (1979年)
  - *15-Minute Hamlet* (1979年)
  - *Undiscovered Country* (1979年)
  - *On the Razzle* (1981年)
  - *The Real Thing* (1982年)
  - *Rough Crossing* (1984年)
  - *Dalliance* (1986年)
  - *Hapgood* (1988年)
  - *Arcadia* (1993年)
  - *Indian Ink* (1993年)
  - *The Invention of Love* (1995年)
  - *The Coast of Utopia* (2002年)
  - *Rock 'n' Roll* (2006年)

## 廣播、電影、電視作品

### 廣播

史塔佩最初是在[英國廣播公司工作](../Page/英國廣播公司.md "wikilink")，他的廣播作品包括：

  - *The Dissolution of Dominic Boot* (1964年)
  - *‘M’ is for Moon amongst Other Things* (1964年)
  - *If you’re Glad I’ll be Frank* (1966年)
  - *Albert’s Bridge* (1967年)
  - *Where are They Now?* (1968年)
  - *Artist Descending a Staircase* (1972年)
  - *The Dog it was that Died* (1982年)
  - *In the Native State* (1991年)
  - *Darkside* (2013年)

### 電影、電視

  - *Three Men in a Boat* (1975年)([TV](../Page/TV.md "wikilink"))
  - *The Boundary* (1975年)([TV](../Page/TV.md "wikilink"))
  - *Professional Foul* (1977年)([TV](../Page/TV.md "wikilink"))
  - *Brazil* (1985年)
  - *[太陽帝國](../Page/太陽帝國.md "wikilink")* (1987年)
  - *[俄羅斯大廈](../Page/俄羅斯大廈.md "wikilink")* (1990年)
  - *[莎翁情史](../Page/莎翁情史.md "wikilink")* (1998年)
  - *[攔截密碼戰](../Page/攔截密碼戰.md "wikilink")* (2001年)
  - *[黃金羅盤](../Page/黃金羅盤.md "wikilink")* (2005年)
  - *[神鬼認證：最後通牒](../Page/神鬼認證：最後通牒.md "wikilink")* (2007年)

## 獎項

  - [Evening Standard
    Award最有潛質編劇獎](../Page/:en:Evening_Standard_Award.md "wikilink")
    (1967年)
  - Rosencrantz & Guildenstern Are
    Dead－[東妮獎最佳劇本](../Page/東妮獎.md "wikilink")、New
    York Critics Award年度最佳劇本、Prix Italia、Plays and Players Award最佳劇本
    (1968年)
  - Jumpers－[Evening Standard
    Award最佳劇本](../Page/:en:Evening_Standard_Award.md "wikilink")、Plays
    and Players Award最佳劇本 (1972年)
  - Travesties－[Evening Standard
    Award年度最佳喜劇](../Page/:en:Evening_Standard_Award.md "wikilink")
  - Travesties－[東妮獎最佳劇本](../Page/東妮獎.md "wikilink")、New York Critics
    Award最佳劇本
  - Night and Day－[Evening Standard
    Award最佳劇本](../Page/:en:Evening_Standard_Award.md "wikilink")
  - The Real Thing－[Evening Standard
    Award最佳劇本](../Page/:en:Evening_Standard_Award.md "wikilink")
  - The Real Thing－[東妮獎最佳劇本](../Page/東妮獎.md "wikilink")、New York Critics
    Award最佳外國劇本
  - Arcadia－Critics' Circle Theatre Awards最佳劇本、[Evening Standard
    Award年度最佳劇本](../Page/:en:Evening_Standard_Award.md "wikilink")
  - Arcadia－Laurence Olivier/BBC Award最佳劇本
  - The Invention of Love－[Evening Standard
    Award最佳劇本](../Page/:en:Evening_Standard_Award.md "wikilink")
  - 莎翁情史－[奧斯卡](../Page/奧斯卡.md "wikilink")[最佳原創劇本獎](../Page/最佳原創劇本獎.md "wikilink")
    (1998年)
  - The Coast of Utopia－[東妮獎最佳劇本獎](../Page/東妮獎.md "wikilink") (2007年)

## 小說

史塔佩的小說作品只有一本，Lord Malquist and Mr Moon，1966年出版。

## 生活

史塔佩有過兩次婚姻，第一次是娶了護士Josie Ingle，時間為1965年至1972年；第二次是娶了Miriam
Stoppard，時間為1972年到1992年。史塔佩在這兩次婚姻中生了兩個兒子，威爾·史塔佩和艾德·史塔佩。

## 參考文獻

## 外部链接

  -
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:英国剧作家](../Category/英国剧作家.md "wikilink")
[Category:英國作家](../Category/英國作家.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:男性编剧](../Category/男性编剧.md "wikilink")
[Category:高松宫殿下纪念世界文化奖获得者](../Category/高松宫殿下纪念世界文化奖获得者.md "wikilink")
[Category:戴维·科恩奖获得者](../Category/戴维·科恩奖获得者.md "wikilink")