《**德古拉**》（*Dracula*）是[爱尔兰作家](../Page/爱尔兰.md "wikilink")[布拉姆·斯托克](../Page/布拉姆·斯托克.md "wikilink")（Bram
Stoker）于1897年出版的以[吸血鬼为题材的](../Page/吸血鬼.md "wikilink")[哥特式](../Page/哥特小说.md "wikilink")[恐怖小说](../Page/恐怖小说.md "wikilink")。

这部小说以15世纪时[瓦拉几亚](../Page/瓦拉奇公國.md "wikilink")（[罗马尼亚南部一公国](../Page/罗马尼亚.md "wikilink")）的领主[弗拉德三世为原型创作的](../Page/弗拉德三世.md "wikilink")。小说中的主人公[德古拉有别于以往古代神话和传说中的吸血鬼丑陋](../Page/德古拉.md "wikilink")、没有智力的动物形象，作者将[吸血鬼描绘成文质彬彬](../Page/吸血鬼.md "wikilink")、聪明、具有吸引异性魅力，能够控制受害人的思想的绅士。这本小说的成功和流行使得德古拉成为吸血鬼的代名词。这本书和另一本关于吸血鬼的小说《[卡蜜拉](../Page/卡蜜拉_\(小说\).md "wikilink")》一起成为吸血鬼文化的经典小说。

## 评价

## 改編

此書曾多次被改編成[舞台劇](../Page/舞台劇.md "wikilink")、[電影及](../Page/電影.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")。

第一部改編自此小說的電影為[無聲](../Page/無聲電影.md "wikilink")[黑白電影](../Page/黑白.md "wikilink")《[吸血鬼](../Page/吸血鬼_\(電影\).md "wikilink")》（*Nosferatu*），在電影業發展初期的[1922年發表](../Page/1922年電影.md "wikilink")，導演為[F·W·穆瑙](../Page/F·W·穆瑙.md "wikilink")。穆瑙因未能獲得授權而須為電影及片中角色名字改名（如主角德古拉則成為Orlok伯爵），但最後仍被斯托克的遺孀起訴他侵犯版權。穆瑙敗訴後，法官命穆瑙毀滅此片所有的拷貝，但卻因盜版的存在而讓此片得以流傳後世。

第一部獲正式授權的電影改編則是[1931年的](../Page/1931年電影.md "wikilink")《[德古拉](../Page/德古拉_\(電影\).md "wikilink")》（*Dracula*），其實是再改編自同名舞台劇，是第一套有聲的吸血鬼電影。德古拉由[貝拉·盧古喜](../Page/貝拉·盧古喜.md "wikilink")（Béla
Lugosi）扮演；電影中，他的口音則令到一句相當普通的「我想吸你的血。」（I want to suck your blood.）變作「I
vant to suck your
blood.」而成為名句。此片大受歡迎，不但拍了不少續集，如《[德古拉的女兒](../Page/德古拉的女兒.md "wikilink")》（*Dracula's
Daughter*）及《[德古拉的兒子](../Page/德古拉的兒子.md "wikilink")》（*Son of
Dracula*），亦帶起了一陣[恐怖片熱](../Page/恐怖片.md "wikilink")。

[1958年](../Page/1958年電影.md "wikilink")，[英國的](../Page/英國電影.md "wikilink")[咸馬電影製作公司](../Page/咸馬電影製作公司.md "wikilink")（Hammer
Film
Productions）不甘人後，發行了《》（Dracula），這一次德拉庫拉則由[克里斯多福·李扮演](../Page/克里斯多福·李.md "wikilink")。此片相當受歡迎，續集亦愈拍愈多，最後共有8套續集，即《[德古拉的新娘](../Page/德古拉的新娘.md "wikilink")》（*Brides
of Dracula*）、《[德古拉：黑暗王子](../Page/德古拉：黑暗王子.md "wikilink")》（*Dracula:
Prince of Darkness*）、*Dracula Has Risen from the Grave*、*Taste the Blood
of Dracula*、*Scars of Dracula*、*Dracula AD 1972*、*The Satanic Rites of
Dracula*及《》（The Legend of the 7 Golden
Vampires），而最後的一套更是與[邵氏電影合作](../Page/邵氏兄弟（香港）.md "wikilink")。

最近一次改編自此書的大型製作則為[1992年的](../Page/1992年電影.md "wikilink")《[吸血鬼：真愛不死](../Page/吸血鬼：真愛不死.md "wikilink")》（*Bram
Stoker's
Dracula*，又譯《吸血殭屍：驚情四百年》），由[弗朗西斯·科波拉執導](../Page/弗朗西斯·科波拉.md "wikilink")，[蓋瑞·歐德曼飾德古拉](../Page/蓋瑞·歐德曼.md "wikilink")，其餘主要演員有[基努·李維](../Page/基努·李維.md "wikilink")、[安東尼·鶴健士及](../Page/安東尼·鶴健士.md "wikilink")[薇諾娜·瑞德等](../Page/薇諾娜·瑞德.md "wikilink")。

[category:爱尔兰小说](../Page/category:爱尔兰小说.md "wikilink")

[Category:吸血鬼題材小說](../Category/吸血鬼題材小說.md "wikilink")
[Category:1897年長篇小說](../Category/1897年長篇小說.md "wikilink")
[德古拉](../Category/德古拉.md "wikilink")