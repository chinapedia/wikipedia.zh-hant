**田中敦子**（本名：佐藤
敦子\[1\]，）為[日本的](../Page/日本.md "wikilink")[女性配音員](../Page/配音員.md "wikilink")、[旁白](../Page/旁白.md "wikilink")。出身於[群馬縣](../Page/群馬縣.md "wikilink")[前橋市](../Page/前橋市.md "wikilink")\[2\]。隸屬於[Mausu
Promotion](../Page/Mausu_Promotion.md "wikilink")。畢業於、[菲利斯女學院大學國文科](../Page/菲利斯女學院大學.md "wikilink")（日文科）\[3\]。

大部分多演出如[職業婦女](../Page/職業婦女.md "wikilink")、具知性的惡女角色。角色代表作是動畫[攻殼機動隊系列的](../Page/攻殼機動隊.md "wikilink")[草薙素子與在日本地區放映的](../Page/公安九課#草薙素子.md "wikilink")[六人行影集中的](../Page/六人行.md "wikilink")[菲比·布菲](../Page/菲比·布菲.md "wikilink")。

## 略歷

就讀大學時和朋友因為興趣而從事舞台劇活動，畢業之後在電腦產業相關的公司當了五年的上班族。在上班的這段時期，同時也去學習舞蹈，所隸屬的舞蹈團最少每月會有一次的舞台活動。原本並沒有當[聲優的打算](../Page/聲優.md "wikilink")，透過舞蹈團中從事聲優工作者的介紹，去了東京播報員學園（Tokyo
Announce Academy）上課，之後開始以當聲優為目標。

一開始從事企業用錄影帶與電視劇演出，直到擔任美國影集『ENG』的配音時，才開始成為聲優正式出道。感受到聲優工作的魅力後，開始專心致志於聲優演出。（目前在電視劇上很少登場，要看到她本人演出的演技，現在只有在其經紀公司MAUSU
Production舞台上，才有可能看到）。曾說過，若比較動畫與外國電影，她比較喜歡追求自然演技的外國電影。

尊敬的人是同樣在聲優業活躍的[高島雅羅](../Page/高島雅羅.md "wikilink")。在新人時代與高島雅羅共演時，受到她穩重個性、現場精彩演出的演技所吸引；據本人所說，在此之後在聲優業裡就以她為目標。

和經常在洋片中一起配音的[小山力也有著會互相叫暱稱的好交情](../Page/小山力也.md "wikilink")。（田中敦子會叫小山「小力」（），而小山力也會叫田中「小敦」（））。

此外在動畫『YAT安心\!宇宙旅行』中和扮演女兒（田中敦子是演母親）的[興梠里美是同年同月同日生](../Page/興梠里美.md "wikilink")。另外，和經常共同演出的[古澤徹是幼稚園的同學](../Page/古澤徹.md "wikilink")。

将自己的手机铃定为草薙素子的台词。

在出演《无罪》时铃木敏夫曾想将主角的声优定为山口智子，在TV版企画阶段神山健治也曾想换声优，都因押井的反对作罢。

有一个妹妹和一个儿子。

## 逸事

在『東京角色秀RADIO』（Tokyo Character Show
RADIO）中，原先預定由同事務所的[大塚明夫出場](../Page/大塚明夫.md "wikilink")，因大塚明夫生了急病而代替他擔任來賓上場時，為了對[田中理惠曾經在](../Page/田中理惠_\(聲優\).md "wikilink")『[攻殻機動隊
S.A.C. 2nd
GIG](../Page/攻殻機動隊_S.A.C._2nd_GIG.md "wikilink")』的影像特典中，模仿草薙素子聲音演出的事報一箭之仇，田中敦子就模仿起田中理惠在[CHOBITS中所演的小唧](../Page/CHOBITS.md "wikilink")。（之後大塚明夫仍上這節目擔任來賓。）另外，田中敦子和田中理惠之後則在[工作狂中共同演出](../Page/工作狂.md "wikilink")。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 1993年

<!-- end list -->

  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")（ユカ・マイラス）
  - [魯邦三世：魯邦暗殺指令](../Page/魯邦三世.md "wikilink")（カレン･コーリスキー教授）

<!-- end list -->

  - 1994年

<!-- end list -->

  - [咕嚕咕嚕魔法陣](../Page/咕嚕咕嚕魔法陣.md "wikilink")（ビケイン）
  - [碧奇魂](../Page/碧奇魂.md "wikilink")（禮子的母亲）

<!-- end list -->

  - 1995年

<!-- end list -->

  - [VR快打](../Page/VR快打.md "wikilink")（エヴァ・デュリックス）
  - [夢幻游戏](../Page/夢幻游戏.md "wikilink")（房宿）
  - [忍空](../Page/忍空.md "wikilink")（朱美、典子）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [超者萊汀](../Page/超者萊汀.md "wikilink")（天賀井玲子）
  - [YAT安心\!宇宙旅行](../Page/YAT安心!宇宙旅行.md "wikilink")（アン・マリーゴールド）
  - [名侦探柯南](../Page/名侦探柯南.md "wikilink")（木下明子）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")（笹本美華）
  - [烙印勇士](../Page/烙印勇士.md "wikilink")（スラン）
  - [馬赫GOGOGO](../Page/馬赫GOGOGO.md "wikilink")（1997年）（賽西兒·葉月）
  - マスターモスキートン'99（ウルフレディ）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [AWOL -Absent Without
    Leave-](../Page/AWOL_-Absent_Without_Leave-.md "wikilink")（ダナ・マクラレン）
  - [星際牛仔](../Page/星際牛仔.md "wikilink")（コフィ）
  - [腦力者](../Page/腦力者.md "wikilink")（シラー・グラス）
  - [夢幻拉拉](../Page/夢幻拉拉.md "wikilink")（羽根石弘美）

<!-- end list -->

  - 1999年

<!-- end list -->

  - [亞歷山大戰記](../Page/亞歷山大戰記.md "wikilink")（卡珊德拉）
  - [星方天使](../Page/星方天使.md "wikilink")（薇樂莉亞）
  - ぶぶチャチャ（ママ）
  - [人形草紙傀儡师左近](../Page/人形草紙傀儡师左近.md "wikilink")（秋月二葉）
  - [火魅子传](../Page/火魅子传.md "wikilink")（藤那）

<!-- end list -->

  - 2000年

<!-- end list -->

  - [萬有引力](../Page/萬有引力_\(漫畫\).md "wikilink")（薰子）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [神奇寶貝超世代](../Page/神奇寶貝動畫版.md "wikilink")（アザミ）
  - [Weiß kreuz Gluhen](../Page/白色十字架.md "wikilink")（辻井真由美）
  - [攻壳机动队 STAND ALONE
    COMPLEX](../Page/攻壳机动队_STAND_ALONE_COMPLEX.md "wikilink")（[草薙素子](../Page/公安九課#草薙素子.md "wikilink")）
  - [翼神世音](../Page/翼神世音.md "wikilink")（七森小夜子）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [銀河鐵道物語](../Page/銀河鐵道物語.md "wikilink")（カタリナ・シェリル）
  - [灌籃少年](../Page/灌籃少年.md "wikilink")（冰室恭子）
  - バトルプログラマーシラセ（天野琴恵）
  - [狼雨](../Page/狼雨.md "wikilink")（ジャガラ卿）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [阿嘉莎‧克莉絲蒂：名偵探白羅與瑪波](../Page/阿嘉莎‧克莉絲蒂：名偵探白羅與瑪波.md "wikilink")（雷蒙小姐）
  - [神無月巫女](../Page/神無月巫女.md "wikilink")（アメノムラクモ）
  - [攻壳机动队 S.A.C. 2nd
    GIG](../Page/攻壳机动队_S.A.C._2nd_GIG.md "wikilink")（草薙素子）
  - 超生命体トランスフォーマー ビーストウォーズリターンズ（ボタニカ）
  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")（黑皇后）
  - 魔法少女隊アルス（アテリア）
  - [MONSTER](../Page/MONSTER.md "wikilink")（マルゴット・ランガー）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [Gallery Fake](../Page/Gallery_Fake.md "wikilink")（翡翠）
  - [到另一個你的身邊去](../Page/到另一個你的身邊去.md "wikilink")（後藤美有樹）
  - [BLACK CAT](../Page/黑貓.md "wikilink")（艾基多娜‧帕拉斯）
  - [魯邦三世：天使の策略 ～夢のカケラは殺しの香り～](../Page/魯邦三世.md "wikilink")（辻斬りカオル）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [傳頌之物](../Page/傳頌之物.md "wikilink")（卡露拉）
  - [Ergo Proxy](../Page/死亡代理人.md "wikilink")（ラカン）
  - [攻殼機動隊 S.A.C. Solid State
    Society](../Page/攻殼機動隊_S.A.C._Solid_State_Society.md "wikilink")（草薙素子）
  - [史上最強弟子兼一](../Page/史上最強弟子兼一.md "wikilink")（芙雷雅/久賀館要）
  - [雙面騎士](../Page/雙面騎士.md "wikilink")（伊麗莎維塔）
  - [少年陰陽師](../Page/少年陰陽師.md "wikilink")（高龗神）
  - [超級機械人大戰OG -Divine Wars-](../Page/超級機器人大戰OG.md "wikilink")（薇蕾塔·巴迪姆）
  - 奏光のストレイン（メロドック）
  - [工作狂](../Page/工作狂.md "wikilink")（梶舞子）
  - [Fate/Stay night](../Page/Fate/Stay_night.md "wikilink")（Caster）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（江本彩）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [京四郎與永遠的天空](../Page/京四郎與永遠的天空.md "wikilink") （綾小路三華）
  - [MOONLIGHT MILE](../Page/MOONLIGHT_MILE.md "wikilink")（）
  - [悲慘世界 少女珂賽特](../Page/悲慘世界_少女珂賽特.md "wikilink")（瑟芬）
  - [惡魔獵人](../Page/惡魔獵人系列.md "wikilink")（翠絲）
  - [獸神演武](../Page/獸神演武.md "wikilink")（紅英玖儒）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [死後文](../Page/死後文.md "wikilink")（立石尚子）
  - [野良耳](../Page/野良耳.md "wikilink")（トリミ）
  - [破天荒遊戲](../Page/破天荒遊戲.md "wikilink")（イリリア・ローズ）
  - [二十面相少女](../Page/二十面相少女.md "wikilink")（白髮鬼）
  - [S.A特優生](../Page/S.A特優生.md "wikilink")（狩野堇）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [貓願三角戀](../Page/貓願三角戀.md "wikilink")（**喵姆薩斯**）
  - [青澀文學系列](../Page/青澀文學系列.md "wikilink")「人間失格」（夫人）
  - 川之光（**ブルー**）
  - [源氏物語千年纪 Genji](../Page/源氏物語千年纪_Genji.md "wikilink")（王命妇）
  - [戰場女武神](../Page/戰場女武神.md "wikilink")（艾蕾諾亞·巴羅多）
  - [女王之刃](../Page/女王之刃.md "wikilink")（クローデット）
  - [蒼天航路](../Page/蒼天航路.md "wikilink")（卞玲瓏）
  - [海猫泣鳴之時](../Page/海猫悲鳴時.md "wikilink")（右代宮霧江、須磨寺霞）
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")（[小南](../Page/小南.md "wikilink")）
  - [Princess Lover\!](../Page/Princess_Lover!.md "wikilink")（約瑟芬·喬斯塔）
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（木山春生）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [一騎當千 XTREME XECUTOR](../Page/一騎當千.md "wikilink")（孟獲）
  - [只有神知道的世界](../Page/只有神知道的世界.md "wikilink")（二階堂老師）
  - [马达加斯加企鹅第一季](../Page/馬達加斯加的企鵝.md "wikilink")（キッカ）
  - [超級機械人大戰OG -The
    Inspector-](../Page/超級機械人大戰OG_-The_Inspector-.md "wikilink")（薇蕾塔·巴迪姆）
  - [信蜂 Reverse](../Page/信蜂.md "wikilink")（ボニー）
  - [變形金剛進化版](../Page/變形金剛進化版.md "wikilink")（航空兵スリップストリーム）
  - [寵物小精靈BestWishes](../Page/寵物小精靈BestWishes.md "wikilink")（蘆薈）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [只有神知道的世界II](../Page/只有神知道的世界.md "wikilink")（二階堂老師）
  - [銀魂'](../Page/銀魂'.md "wikilink")（不三子）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [Campione 弒神者！](../Page/Campione_弒神者！.md "wikilink")（璐克蕾琪雅·索拉）
  - [心連·情結](../Page/心連·情結.md "wikilink")（永瀨玲佳〈伊織的母親〉）
  - [TARI TARI](../Page/TARI_TARI.md "wikilink")（教導主任）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [心跳！光之美少女](../Page/心跳！光之美少女.md "wikilink")（瑪莫）
  - [Little Busters\!](../Page/Little_Busters!.md "wikilink")（主播）
  - [只有神知道的世界 女神篇](../Page/只有神知道的世界.md "wikilink")（二階堂老師）
  - [銀狐](../Page/銀狐_\(漫畫\).md "wikilink")（豐倉江津子）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [魔法戰爭](../Page/魔法戰爭.md "wikilink")（瓦爾蕾特·諾斯）
  - [銀河騎士傳](../Page/銀河騎士傳.md "wikilink")（莎瑪莉）
  - [寄生獸 生命的準則](../Page/寄生獸.md "wikilink")（**田宮良子**）
  - [Fate/stay
    night（ufotable版）](../Page/Fate/stay_night.md "wikilink")（Caster）
  - [四月是你的謊言](../Page/四月是你的謊言.md "wikilink")（落合由里子）
  - [-{zh-cn:临时女友;zh-tw:女友伴身邊}-](../Page/臨時女友.md "wikilink")（藤堂靜子）
  - [鋼彈 Reconguista in
    G](../Page/GUNDAM_G之复国运动.md "wikilink")（[威爾米特・傑納姆](../Page/GUNDAM_G之复国运动.md "wikilink")）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（[領域外的妹妹](../Page/名偵探柯南角色列表#主要人物的關係者.md "wikilink")）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [The Rolling Girls](../Page/The_Rolling_Girls.md "wikilink")（藤原春）
  - [亞爾斯蘭戰記](../Page/亞爾斯蘭戰記.md "wikilink")（泰巴美娜）
  - [銀河騎士傳 第九行星戰役](../Page/銀河騎士傳.md "wikilink")（莎瑪莉）
  - [偶像大師 灰姑娘女孩](../Page/偶像大師_灰姑娘女孩.md "wikilink")（美城常務）
  - [傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")（卡露拉）
  - [機動戰士GUNDAM 鐵血的孤兒](../Page/機動戰士GUNDAM_鐵血的孤兒.md "wikilink")（亞米達·艾卡）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [紅殼的潘朵拉](../Page/紅殼的潘朵拉.md "wikilink")（**烏薩爾·大利拉**）
  - [SUPER LOVERS](../Page/SUPER_LOVERS.md "wikilink")（春子·D·狄克曼）
  - [亞爾斯蘭戰記](../Page/亞爾斯蘭戰記.md "wikilink") 風塵亂舞（泰巴美娜）
  - [齊木楠雄的災難](../Page/齊木楠雄的災難.md "wikilink")（性感女性）
  - 機動戰士GUNDAM 鐵血的孤兒 第二期（亞米達·艾卡）
  - [超心動！文藝復興](../Page/超心動！文藝復興.md "wikilink")（瑠衣的母親）
  - [3年D班玻璃假面](../Page/玻璃假面.md "wikilink")（**月影千草**）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [ACCA13區監察課](../Page/ACCA13區監察課.md "wikilink")（莫芙）
  - [SUPER LOVERS 2](../Page/SUPER_LOVERS_2.md "wikilink")（春子·D·狄克曼）
  - [騎士&魔法](../Page/騎士&魔法.md "wikilink")（馬蒂娜·歐魯特·克沙佩加）
  - [牙狼〈GARO〉-VANISHING
    LINE-](../Page/牙狼〈GARO〉-VANISHING_LINE-.md "wikilink")（修女）
  - [動畫同好會](../Page/動畫同好會.md "wikilink")（女僕世場須）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink") 第貳期 其之貳（茶吉尼）
  - [甜心戰士 Universe](../Page/甜心戰士_Universe.md "wikilink")（**基爾**\[4\]）

### OVA

  - [海底嬌娃藍華](../Page/海底嬌娃藍華.md "wikilink")（）
  - [轉校生](../Page/轉校生.md "wikilink")（真一的母亲）
  - [不思议游戏](../Page/不思议游戏.md "wikilink")（多喜子）
  - 不思议游戏 女誠国物語（房宿）
  - 不思议游戏 第二部（房宿）
  - [超級機器人大戰ORIGINAL GENERATION THE
    ANIMATION](../Page/超級機器人大戰ORIGINAL_GENERATION_THE_ANIMATION.md "wikilink")（薇蕾塔·巴迪姆）
  - [戰鬥妖精雪風](../Page/戰鬥妖精雪風.md "wikilink")（）
  - [Carnival
    Phantasm](../Page/Carnival_Phantasm.md "wikilink")（Caster、）

**2017年**

  - [SUPER LOVERS](../Page/SUPER_LOVERS.md "wikilink") OAD2（春子·D·狄克曼）

### 劇場版動畫

  - 《[金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")2》：藍沢由理恵
  - 《[攻殼機動隊](../Page/攻殼機動隊_\(電影\).md "wikilink")》：草薙素子
  - 《[攻壳机动队2 INNOCENCE](../Page/攻壳机动队2_INNOCENCE.md "wikilink")》：草薙素子
  - 《[機動警察三](../Page/機動警察.md "wikilink")》（岬冴子）
  - 《[Brave Story](../Page/Brave_Story.md "wikilink")》（陽光神將）
  - 《[貓的報恩](../Page/貓的報恩.md "wikilink")》：布魯（原名：ブルー）
  - 《[琴之森](../Page/琴之森.md "wikilink")》（雨宮奈美惠）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [剧场版 黑執事 Book of the
    Atlantic](../Page/黑執事.md "wikilink")（**法蘭西絲·米多福特**）
  - [Fate/stay night Heaven's
    Feel](../Page/Fate/stay_night_Heaven's_Feel.md "wikilink")（Caster）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [我想吃掉你的胰臟](../Page/我想吃掉你的胰臟.md "wikilink")（「我」的媽媽\[5\]）
  - [忍者蝙蝠俠](../Page/忍者蝙蝠俠.md "wikilink")（毒藤女）

### 遊戲

  - [Fate/tiger
    colosseum](../Page/Fate/tiger_colosseum.md "wikilink")（Caster）
  - [機戰傭兵前線方程式](../Page/機戰傭兵前線方程式.md "wikilink")（マニュアル）
  - アイドル雀士を作っちゃおう♥ （早乙女ななみ）
  - [安潔莉可](../Page/安潔莉可.md "wikilink")（第255代女王補佐官ディア）
  - [永遠的伊蘇I・II](../Page/伊蘇.md "wikilink")（サラ）
  - [夜行偵探PLUS](../Page/夜行偵探PLUS.md "wikilink")（アクア・ロイド）
  - いつか、重なりあう未来へ（ルージュ・ヴァンスタージュ）
  - [OZ](../Page/OZ.md "wikilink")（アルミラ）
  - [鬼武者2](../Page/鬼武者.md "wikilink")（高女）
  - [CAPCOM FIGHTING
    Jam](../Page/CAPCOM_FIGHTING_Jam.md "wikilink")（[春麗](../Page/春麗.md "wikilink")、[Rose](../Page/Rose_（快打旋風）.md "wikilink")）
  - [女忍者](../Page/女忍者.md "wikilink")（緋花）
  - [攻壳机动队 STAND ALONE
    COMPLEX](../Page/攻壳机动队_STAND_ALONE_COMPLEX.md "wikilink")（草薙素子）
  - [高機動幻想GPM](../Page/高機動幻想GPM.md "wikilink")（本田節子）
  - [編碼時代指導者～繼承者與被繼承者～](../Page/編碼時代指導者～繼承者與被繼承者～.md "wikilink")（フィオナ）
  - [櫻花大戰3～巴黎在燃燒嗎～](../Page/櫻花大戰3.md "wikilink")（ピトン）
  - [式神之城3](../Page/式神之城.md "wikilink")（ふみこ・オゼット・ヴァンシュタイン）
      - 式神之城 七夜月幻想曲（ふみこ・オゼット・ヴァンシュタイン）
  - [闇影之心2](../Page/闇影之心2.md "wikilink")（犬神咲）
  - [英雄傳說3～白髮魔女](../Page/英雄傳說.md "wikilink")（Sega Saturn版）（ゲルド）
  - [超級機器人大戰系列](../Page/超級機器人大戰系列.md "wikilink")（薇蕾塔·巴迪姆、ユカ・マイラス、シラー・グラス）
  - [快打旋風III 3rd Strike](../Page/街頭快打系列.md "wikilink")（春麗、ポイズン）
  - ソリッドフォース［PC-Engine\]（サラ・メディシナ）
  - [地獄三頭犬的輓歌：FFVII](../Page/地獄三頭犬的輓歌：FFVII.md "wikilink")（ロッソ）
  - [DESIRE](../Page/DESIRE.md "wikilink")（クリスティ・シェパード）
  - [天下人](../Page/天下人.md "wikilink")（吉岡林）
  - [Twelve～戦国封神伝～](../Page/Twelve～戦国封神伝～.md "wikilink")（帝、ナカエ、セリーナ）
  - [縱橫諜海3：混沌理論](../Page/縱橫諜海.md "wikilink")（アンナ・グリムスドッティア）
  - [NAMCO x CAPCOM](../Page/NAMCO_x_CAPCOM.md "wikilink")（春麗、レジーナ）
  - [鋼之鍊金術師3～繼承神的少女](../Page/鋼之鍊金術師.md "wikilink")（ヴィーナス・ローズマリア）
  - [女神侧身像2：希爾梅莉亞](../Page/女神侧身像2：希爾梅莉亞.md "wikilink")（レオーネ）（アーリィ）
  - フィロソマ（ニコラ・ミショー大尉）
  - [Fate/stay night \[Realta
    Nua](../Page/Fate/stay_night.md "wikilink")\]（Caster）
  - [閃電霹靂車系列](../Page/閃電霹靂車.md "wikilink")（AKF-0/1B ネメシス）
  - [烙印勇士：千年帝國之鷹篇聖魔戰記之章](../Page/烙印勇士.md "wikilink")（スラン）
  - [我的暑假](../Page/我的暑假.md "wikilink")（沙織）
  - [我的暑假2](../Page/我的暑假2.md "wikilink")（芳花）
  - [義經紀](../Page/義經紀.md "wikilink")（源義經）
  - [立体忍者活劇 天誅弐](../Page/天誅系列.md "wikilink")（香我美）
  - [立体忍者活劇 天誅 忍凱旋](../Page/天誅系列.md "wikilink")（明智田鶴）
  - [Refrain Love](../Page/Refrain_Love.md "wikilink")（高宮祥子）
  - [R.O.H.A.N](../Page/ROHAN.md "wikilink")（ダン女性、デカン女性）
  - [摔角天使](../Page/摔角天使.md "wikilink")（中森あずみ、霧島レイラ）
  - [探偵神宮寺三郎 Innocent
    Black](../Page/探偵神宮寺三郎_Innocent_Black.md "wikilink")（氷室透子）
  - [毀滅全人類](../Page/毀滅全人類.md "wikilink")（少佐）
  - [蘿洛娜工坊～阿蘭德的鍊金術士～](../Page/蘿洛娜工坊～阿蘭德的鍊金術士～.md "wikilink")（阿思崔德·澤克瑟斯）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [古墓奇兵：暗黑天使](../Page/古墓丽影：黑暗天使.md "wikilink")（蘿拉卡芙特 ララ・クロフト）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [BALDR FORCE](../Page/BALDR_FORCE.md "wikilink")（橘玲佳）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [傳頌之物](../Page/傳頌之物.md "wikilink")（卡露拉）

<!-- end list -->

  - 2007年

<!-- end list -->

  - ジェットインパルス（ナオミ・モリファー中尉，PSP）
  - [Fate/unlimited
    codes](../Page/Fate/unlimited_codes.md "wikilink")（Caster）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [戰場女武神](../Page/戰場女武神.md "wikilink")（艾蕾諾亞・巴羅多）
  - [最終幻想：紛爭](../Page/最終幻想：紛爭.md "wikilink")（阿尔迪米西娅）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [龍之谷](../Page/龍之谷.md "wikilink")（亞勒詹達）
  - [噬神者](../Page/噬神者.md "wikilink")（雨宮椿）
  - [尼爾 人工生命](../Page/尼爾_\(遊戲\).md "wikilink")（**凱寧**）
  - [Marvel vs Capcom 3](../Page/Marvel_vs_Capcom_3.md "wikilink")（翠丝）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [紛爭 012：最終幻想](../Page/紛爭_012：最終幻想.md "wikilink")（阿尔迪米西娅）
  - [最終幻想 零式](../Page/最終幻想_零式.md "wikilink")（アレシア．アルラシア）
  - [Ultimate Marvel vs Capcom
    3](../Page/Ultimate_Marvel_vs_Capcom_3.md "wikilink")（翠丝）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [魔龍寶冠](../Page/魔龍寶冠.md "wikilink")（亞馬遜 Amazon、旁白）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [猎天使魔女2](../Page/猎天使魔女2.md "wikilink")（Bayonetta）
  - [零～濡鸦之巫女～](../Page/零～濡鸦之巫女～.md "wikilink")（黑泽密花）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [Fate/Grand
    Order](../Page/Fate/Grand_Order.md "wikilink")（[美狄亞](../Page/美狄亞.md "wikilink")、[荊軻](../Page/荊軻.md "wikilink")、[卡蜜拉](../Page/卡蜜拉_\(小說\).md "wikilink")、迷你Nobu〔Caster〕）
  - [傳頌之物 虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")（卡露拉）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [傳頌之物 兩人的白皇](../Page/傳頌之物_兩人的白皇.md "wikilink")（卡露拉）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [碧藍航線](../Page/碧藍航線.md "wikilink")（[提爾比茨](../Page/鐵必制號戰艦.md "wikilink")、[胡德](../Page/胡德號戰鬥巡洋艦.md "wikilink")）
  - [仁王](../Page/仁王_\(遊戲\).md "wikilink")（[茶茶](../Page/茶茶.md "wikilink")）
  - [最终幻想 纷争（街机版）](../Page/最终幻想_纷争_\(2015年游戏\).md "wikilink")（阿尔迪米西娅）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [最终幻想 纷争 Opera
    Omnia](../Page/最终幻想_纷争_Opera_Omnia.md "wikilink")（阿尔迪米西娅）
  - [JUMP FORCE](../Page/JUMP_FORCE.md "wikilink")（嘉雷娜）
  - [惡魔獵人5](../Page/惡魔獵人5.md "wikilink")（翠丝、伊娃）

### 日語配音

  - 24小時(第4-5季) （オードリー・レインズ）

  - 哥吉拉系列 （オードリー・ティモンズ）

  - [六人行](../Page/六人行.md "wikilink") （菲比·布菲 兼阿斯拉(雙胞胎姊姊)）

  - 犯罪捜査官 ネイビーファイル （サラ・“マック”マッケンジー）

  - ER緊急救命室 （アンナ・デル・アミコ）

  - ハムナプトラ （エヴリン）

  - [決戰異世界](../Page/決戰異世界.md "wikilink") （西琳）

  - 傲慢與偏見（珍・班奈特）

  - リベリオン（メアリー・オブライエン）

  - [康斯坦汀：驅魔神探](../Page/康斯坦汀：驅魔神探.md "wikilink")（加百列（[蒂達·史雲頓](../Page/蒂達·史雲頓.md "wikilink")））

  - コールドケース 迷宮事件簿（リリー・ラッシュ）

  - ビバリーヒルズ高校白書（トレイシー・ゲーリアン/トニー）

  - ハイ・フィデリティ（リズ）

  - [鬼水怪談西洋篇](../Page/鬼水怪談.md "wikilink")（達莉雅（[珍妮佛·康納莉](../Page/珍妮佛·康納莉.md "wikilink")））

  - ターミナル・ベロシティ（ナスターシャ・キンスキー（クリス役））

  - ソルジャー・オブ・フォーチュン（マーゴ）

  - ギフト（ケイト・ブランシェット（アニー・ウィルソン役））

  - インビジブル（リンダ･マッケイ）

  - ファム・ファタール（レベッカ・ローミン=ステイモス）

  - スターリングラード（ターニャ）

  - [愛情限時簽](../Page/愛情限時簽.md "wikilink")（瑪格麗特・泰特（[珊卓·布拉克](../Page/珊卓·布拉克.md "wikilink")））

  - （A Walk on the Moon）（普爾·坎特羅威茨（[黛安·蓮恩](../Page/黛安·蓮恩.md "wikilink")））

  - [妖夜尋狼之魔間叛徒](../Page/妖夜尋狼之魔間叛徒.md "wikilink")（Underworld：Evolution）（莎倫娜（[凱特·貝琴薩](../Page/凱特·貝琴薩.md "wikilink")））

### 特攝

  - 魔法戰隊魔法連者（冥府神ゴーゴン）
  - 獸拳戰隊（米雪兒・企鵝）

### 電視演出

  - [老天幫幫忙](../Page/老天幫幫忙.md "wikilink")（2005年11月9日於日本地區放映：敦子演出持續3年只給孩子吃泡麵的壞母親･･･令工作室裡其他演出者感到戰慄的的優秀演技，而與平常此節目喜劇色彩強烈的印象畫出一道明顯的界線。2007年3月13日開始於[台灣地區的](../Page/台灣.md "wikilink")[JET日本台亦有放映](../Page/JET日本台.md "wikilink")。）

### CD

  - アカイイト 廣播劇CD「京洛降魔」（鈴鹿）
  - 傳頌之物 廣播劇～トゥスクルの皇后～（カルラ）
  - 傳頌之物 廣播劇～トゥスクルの内乱～（カルラ）
  - 傳頌之物 廣播劇～トゥスクルの財宝～（カルラ）
  - 少年陰陽師　廣播劇CD
  - CD Theater Dragon Quest VI（シェーラ）
  - コンプレックスハート（岩永哲哉企畫的廣播劇、歌曲ＣＤ）
  - 翼を持つ者（抄華）
  - Vassalord 廣播劇CD（レイフェル）
  - 廣播CD 傳頌之物廣播 CD+CD-ROM Vol.1 傳頌之物廣播特別版

### 布袋戲

**2016年**

  - [Thunderbolt Fantasy
    東離劍遊紀](../Page/Thunderbolt_Fantasy_東離劍遊紀.md "wikilink")（旁白、妖荼黎）

### 其他

  - トップランナー（旁白）
  - 未來創造堂（49回中的配音）

## 腳註

## 外部連結

  - [田中敦子のCafe＊Central
    Park/田中敦子的公式個人部落格](http://cafe-centralpark.at.webry.info/)

  - [事務所公開簡歷](http://www.mausu.net/talent/tpdb_view.cgi?UID=45)

  -
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:群馬縣出身人物](../Category/群馬縣出身人物.md "wikilink")
[Category:旁白](../Category/旁白.md "wikilink")

1.  出自於《日本藝人名鑑2008》。
2.  [真夏的太陽・・・ 田中敦子的Cafe＊Central
    Park/田中敦子的公視個人部落格](http://cafe-centralpark.at.webry.info/200907/article_30.html)
3.  《》，市原光敏著 ISBN 978-4921023508
4.
5.