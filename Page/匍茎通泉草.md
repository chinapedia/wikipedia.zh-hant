**匍莖通泉草**（[学名](../Page/学名.md "wikilink")：**）是[玄參科](../Page/玄參科.md "wikilink")[通泉草屬植物](../Page/通泉草屬.md "wikilink")，和近親[佛氏通泉草](../Page/佛氏通泉草.md "wikilink")(*Mazus
faurei* Bonati)有些小差異，常致混淆。

## 形态

多年生[草本植物](../Page/草本.md "wikilink")；具有匍匐莖，會往四周伸出。葉倒卵形或卵圓形，呈現[簇生或](../Page/簇生.md "wikilink")[對生](../Page/對生.md "wikilink")。開花屬於[頂生](../Page/頂生.md "wikilink")[總狀花序](../Page/總狀花序.md "wikilink")，每枝花莖約2-5朵花，花冠為[淡紫色或](../Page/淡紫色.md "wikilink")[紫紅色](../Page/紫紅色.md "wikilink")。[種子為](../Page/種子.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")。因生長成片，在草地上有如繁星點點。

## 分布

常生長在較為潮濕的草地；從低海拔的荒地、溝渠旁，都很容易發現它的身影。潮溼之地常常意味著泉水水源在附近，因此可能是「通泉」命名的由來。

在[台灣為校園常見植物](../Page/台灣.md "wikilink")。

## 外部連結

  -
  -
  - [《東森新聞報》漁光中的春天／有趣的唇形花！通泉草學兔寶寶裝可愛](http://www.nownews.com/2007/05/26/11426-1920211.htm)

[MM](../Category/IUCN無危物種.md "wikilink")
[M](../Category/通泉草屬.md "wikilink")