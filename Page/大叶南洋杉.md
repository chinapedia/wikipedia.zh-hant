**大叶南洋杉**（学名：**）又稱**廣葉南洋杉**，是[南洋杉属下的一种](../Page/南洋杉属.md "wikilink")[常青树](../Page/常青树.md "wikilink")，最高可达30-40米，主要分布在[澳大利亚](../Page/澳大利亚.md "wikilink")。

## 别名

洋刺杉（福州），澳洲南洋杉（经济植物手册），披针叶南洋杉（华北经济植物志要）

## 参考文献

<div class="references-small">

  -

</div>

[大叶南洋杉](../Category/南洋杉属.md "wikilink")
[Category:澳洲植物](../Category/澳洲植物.md "wikilink")