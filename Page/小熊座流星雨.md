**小熊座流星雨**（Ursids，URS）在每年的12月17日開始活躍，大約可以持續一星期左右，在12月25日或26日結束。
這個[流星雨因為](../Page/流星雨.md "wikilink")[輻射點接近](../Page/輻射點.md "wikilink")[小熊座的](../Page/小熊座.md "wikilink")[北極二](../Page/北極二.md "wikilink")（小熊座β星，帝）而得名。

## 歷史

小熊座流星雨可能是發現的\[1\]。直到在20世紀的中葉，1945年，畢卡威博士（Dr. A.
Bečvář）觀測到一次每小時169顆爆發，才整合了各地流星雨觀測者零星的觀測報告研究\[2\]。在1970年代持續的觀測，進而確認與週期[彗星](../Page/彗星.md "wikilink")[8P/塔特爾彗星有所關聯](../Page/8P/塔特爾彗星.md "wikilink")\[3\]。在2007年，[8P/塔特爾彗星回歸的年份](../Page/8P/塔特爾彗星.md "wikilink")，小熊座流星雨顯得特別活躍，強烈暗示與此彗星有所關聯
[1](http://ursid.seti.org/WGNUrsids.pdf)。

和Esko Lyytinen
發現，爆發可能是因為該彗星通過遠日點時，被困在與[木星有](../Page/木星.md "wikilink")7/6的[軌道共振所導致](../Page/軌道共振.md "wikilink")。

## 專業資訊

早期的觀測者敘述[輻射點的平均位置是赤經](../Page/輻射點.md "wikilink") = 217度，赤緯 =
+76度，最大期出現在太陽黃經270.66度（大約是12月22日），估計的出沒期間是12月17日至24日。

小熊座流星雨的峰值時間特別狹窄，佛羅里達的諾曼底W麥克勞三世是一位訓練有素的流星觀測者，評論小熊座流星雨"必定是像[象限儀座流星雨一樣](../Page/象限儀座流星雨.md "wikilink")，峰值是緊密的，能觀察到極大期的時間不超過12個小時"\[4\]。

## 參考資料

## 外部連結

  - [Gary Kronk's Meteor Showers Online -
    Ursids](https://web.archive.org/web/20120320000533/http://meteorshowersonline.com/showers/ursids.html)
  - [International Meteor Organisation Calendar -
    Fall 2005](http://www.imo.net/calendar/2005/fall)
  - [NASA Ursid Airborne Campaign](http://ursid.seti.org) (2008 + Java
    applet)
  - [2011 Ursids Radio
    results](http://www5f.biglobe.ne.jp/~hro/Flash/2011/URS/) (RMOB)
  - [Ursids at Constellation
    Guide](http://www.constellation-guide.com/ursids/)

[U](../Category/流星雨.md "wikilink") [U](../Category/小熊座.md "wikilink")
[Category:12月事件](../Category/12月事件.md "wikilink")

1.
2.
3.
4.