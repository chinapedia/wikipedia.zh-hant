[T_Shirt.jpg](https://zh.wikipedia.org/wiki/File:T_Shirt.jpg "fig:T_Shirt.jpg")的角色\]\]
[Wikipedia-T-shirt.jpg](https://zh.wikipedia.org/wiki/File:Wikipedia-T-shirt.jpg "fig:Wikipedia-T-shirt.jpg")

**T恤**或稱**-{T卹}-**、**T字衫**（[英文](../Page/英文.md "wikilink")：**T-shirt**或**Tee**）是[衣衫的一種](../Page/衣服.md "wikilink")，通常是短袖而圓領的，故亦可稱**短袖圓領衫**。長及腰間，一般沒有[鈕扣](../Page/鈕扣.md "wikilink")、[領子或](../Page/領子.md "wikilink")[袋](../Page/袋.md "wikilink")。攤開時呈T形，因而得名。穿着時把頭部穿過領子即成。T恤一般以[棉或是](../Page/棉.md "wikilink")[人造纖維大規模製造](../Page/人造纖維.md "wikilink")，以[平針編織出柔軟的質地](../Page/平針.md "wikilink")。

T恤原本作為內衣穿着，但現在最常見的穿法是當成上半身唯一的[衣服](../Page/衣服.md "wikilink")（女性可能會在內面佩帶[胸罩](../Page/胸罩.md "wikilink")）。T恤常常配有文字或圖案作裝飾。特大型的、女性穿着較短而露出[肚臍的](../Page/肚臍.md "wikilink")、長袖的、無袖的等等，都是常見的T恤款式。

## T恤歷史

[美國士兵在](../Page/美國.md "wikilink")[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，注意到歐洲士兵使用的[棉質內衣輕巧舒適](../Page/棉.md "wikilink")，而自己的士兵則穿着[羊毛](../Page/羊毛.md "wikilink")（綿）制服，大汗淋漓。這種棉質汗衫在美國士兵之間馬上成為搶手貨，並其形狀而稱之為T恤。[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，T恤成為了[美國陸軍及](../Page/美國陸軍.md "wikilink")[海軍的標準](../Page/美國海軍.md "wikilink")[內衣](../Page/內衣.md "wikilink")。

二戰後T恤開始以外衣形式出現。[-{zh-hans:约翰·韦恩;zh-hk:尊·榮;zh-tw:約翰·韋恩;}-](../Page/约翰·韦恩_\(美国电影演员\).md "wikilink")、[馬龍·白蘭度](../Page/馬龍·白蘭度.md "wikilink")、[詹姆士·狄恩都曾在全國](../Page/詹姆士·狄恩.md "wikilink")[電視如此穿着過](../Page/電視.md "wikilink")。公眾當初對此大為驚訝，但到了1955年社會開始接受這種服裝。[占士·甸在電影](../Page/占士·甸.md "wikilink")《[-{zh-hans:无因的反抗;zh-hk:阿飛正傳;zh-tw:養子不教誰之過;}-](../Page/阿飛正傳_\(1955年電影\).md "wikilink")》中的穿着，讓T恤成為時尚的服裝。

## T恤裝飾

1960年代，T恤成為了西方潮流中年輕人和搖滾音樂愛好者的基本裝束。人們也開始以紮染和絲網印刷等方式改裝T恤，女裝[吊帶背心](../Page/吊帶背心.md "wikilink")、香港俗稱「雞翼袖」的[背心](../Page/背心.md "wikilink")、緊身T恤、[V領等也逐漸開始流行](../Page/V領.md "wikilink")。

自此，T恤成為了自我表達甚至宣傳的媒介，展示的圖案包羅萬有，可以是文字、圖畫甚至是照片，因此也被稱作**文化衫**。
[Paris_75018_Rue_du_Mont-Cenis_no_006_Meuniers_d'Antan_20050605.jpg](https://zh.wikipedia.org/wiki/File:Paris_75018_Rue_du_Mont-Cenis_no_006_Meuniers_d'Antan_20050605.jpg "fig:Paris_75018_Rue_du_Mont-Cenis_no_006_Meuniers_d'Antan_20050605.jpg")

## T恤制作的常用面料

用于T恤制作的常用面料包括：纯棉汗布、涤棉单双面以及纯棉、涤棉六角、四角网眼、人字纹、复合罗纹、提条网眼等。

## 服装搭配

T恤和牛仔裤的搭配几乎从他们的起源就开始了。牛仔裤源自一个[意大利港口的水手装束](../Page/意大利.md "wikilink")，当时称港口的水手为Genoese，GENES大概出自于此；17世纪中，美国安那波利斯港口茶叶码头工人穿一种短袖衫，据说这就是T恤的最初发源，由于TEA和TEE谐音，故有TEE（TEA）-SHIRT一说，后简称T-SHIRT或者TEE。19世纪后期，T-SHIRT逐渐成为英美海军的内衣制服。随着针织工业的兴起，T-shirt作为内衣开始被广泛接受。

## 延伸閱讀

  - Scott Fresener, Earl Smith, Nancy Hall（1995年）：*The T-Shirt
    Book*，Gibbs Smith Publisher出版，ISBN 0-87905-686-X。

[Category:上衣](../Category/上衣.md "wikilink")