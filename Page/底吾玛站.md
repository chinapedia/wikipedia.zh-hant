**底吾玛站**位于[中国](../Page/中国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[那曲地区](../Page/那曲地区.md "wikilink")[安多县](../Page/安多县.md "wikilink")，[海拔](../Page/海拔.md "wikilink")4585米，于2006年7月1日启用\[1\]。该站为青藏铁路的[无人驻守车站](../Page/无人站.md "wikilink")\[2\]，由[青藏铁路集团公司营运](../Page/青藏铁路集团公司.md "wikilink")。

## 使用情况

底吾玛站是[青藏铁路的](../Page/青藏铁路.md "wikilink")[无人站](../Page/无人站.md "wikilink")，有14列旅客列车经过此站，主要为[拉萨到发的各类旅客列车](../Page/拉萨站.md "wikilink")。

## 始发车次

目前底吾玛站未有任何始发车次。

## 历史

  - 2006年7月1日：底吾玛站建成启用。

## 相关条目

  - [青藏铁路](../Page/青藏铁路.md "wikilink")

## 邻近车站

## 参考资料

[Category:青藏铁路车站](../Category/青藏铁路车站.md "wikilink")
[Category:那曲市铁路车站](../Category/那曲市铁路车站.md "wikilink")
[Category:安多县](../Category/安多县.md "wikilink")
[Category:2006年启用的铁路车站](../Category/2006年启用的铁路车站.md "wikilink")

1.  中华建筑资讯网：〈[藏域风格建筑群
    璀璨“新天路”](http://www.rpccn.com/news/33/200673162447.htm)
    〉，于2007年6月25日存取
2.