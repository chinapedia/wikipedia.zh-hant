**琉球群島**，又稱**琉球列島**（）\[1\]、**南西諸島**（）\[2\]\[3\]\[4\]\[5\]，是[太平洋西部的一系列](../Page/太平洋.md "wikilink")[島嶼群](../Page/島嶼.md "wikilink")，位於[台灣島與](../Page/台灣島.md "wikilink")[九州島之間](../Page/九州_\(日本\).md "wikilink")\[6\]\[7\]\[8\]，包括[大隅群島](../Page/大隅群島.md "wikilink")、[吐噶喇群島](../Page/吐噶喇群島.md "wikilink")、[奄美群島](../Page/奄美群島.md "wikilink")（以上合称[萨南群岛](../Page/萨南群岛.md "wikilink")）、[沖繩群島](../Page/沖繩群島.md "wikilink")、[先岛群岛](../Page/先岛群岛.md "wikilink")（包含[宮古群島和](../Page/宮古群島.md "wikilink")[八重山群島](../Page/八重山群島.md "wikilink")）、[大東群島等](../Page/大東群島.md "wikilink")[群島](../Page/群島.md "wikilink")，居住人口以世居於此的[琉球族為主](../Page/琉球族.md "wikilink")。現今為[日本領土](../Page/日本.md "wikilink")\[9\]\[10\]\[11\]，當中萨南群島屬[鹿兒島縣管轄](../Page/鹿兒島縣.md "wikilink")，其餘隸屬[沖繩縣](../Page/沖繩縣.md "wikilink")。

琉球群岛的特殊地理位置，使其自古以來成為[东北亚和](../Page/东北亚.md "wikilink")[东南亚邻近国家的贸易枢杻](../Page/东南亚.md "wikilink")。其[歷史上曾建有](../Page/琉球历史.md "wikilink")[琉球国等國家](../Page/琉球国.md "wikilink")，與[中國](../Page/中國.md "wikilink")[明朝](../Page/明朝.md "wikilink")、[清朝及古代日本均有](../Page/清朝.md "wikilink")[朝貢關係](../Page/朝貢體系.md "wikilink")。17世纪末期，琉球国被位於日本[九州南部的](../Page/九州_\(日本\).md "wikilink")[萨摩藩侵略](../Page/萨摩藩.md "wikilink")，失去奄美群島之領土。日本[明治维新之后](../Page/明治维新.md "wikilink")，1871年将琉球國編為，次年改設[琉球藩](../Page/琉球藩.md "wikilink")，正式納入日本版图。1879年，日本廢除琉球藩，在奄美群島除外的琉球群島島群設置沖繩縣管轄，奄美群島则另劃歸鹿儿岛县。[太平洋战争期間](../Page/太平洋战争.md "wikilink")，琉球群岛成為唯一被[盟軍攻佔的日本](../Page/盟軍.md "wikilink")[領土](../Page/領土.md "wikilink")，戰後被[美國接管](../Page/美國統治琉球時期.md "wikilink")，後來在1953年與1972年分階段[交還日本治理](../Page/沖繩返還.md "wikilink")，但美國仍保留部分地区之行政权\[12\]。1972年5月15日，日本重新恢复对琉球群岛的主權。

## 名稱

由於位於日本本土的西南方，日本將琉球群島稱為「**南西諸島**」\[13\]\[14\]\[15\]，這與[中文環境及國際稱呼的習慣不同](../Page/中文.md "wikilink")。南西諸島一名是由舊[日本海軍的](../Page/日本海軍.md "wikilink")在1887年命名的，並於隔年發行的[海圖](../Page/海圖.md "wikilink")《石垣泊地
日本・南西諸島・石垣島》\[16\]首次記載於文字紀錄上\<ref
name="kadokawa-nansei550"/\[17\]\[18\]。值得注意的是，「南西諸島」僅為[日本政府使用的行政名稱](../Page/日本政府.md "wikilink")，並非[地理學與](../Page/地理學.md "wikilink")[地球科學的專門用語](../Page/地球科學.md "wikilink")\[19\]\[20\]，在地球科學、生態學和考古學領域則使用「[琉球弧](../Page/琉球弧.md "wikilink")」名稱。

由於[歷史上](../Page/#历史.md "wikilink")，日本先於17世紀從[琉球國取得奄美群島並併入](../Page/琉球國.md "wikilink")[薩摩藩](../Page/薩摩藩.md "wikilink")（[廢藩置縣後納入](../Page/廢藩置縣.md "wikilink")[鹿兒島縣管轄](../Page/鹿儿岛县.md "wikilink")），19世紀再併吞琉球國全域，取得沖繩群島、宮古群島、八重山群島並設置為[琉球藩](../Page/琉球藩.md "wikilink")（之後改設置[沖繩縣](../Page/沖繩縣.md "wikilink")），因此如今整個琉球群島在日本又可劃分為“薩南諸島”（意為[薩摩國之南的諸多島嶼](../Page/薩摩國.md "wikilink")）\[21\]、“琉球諸島”（意為屬[琉球國的諸多島嶼](../Page/琉球國.md "wikilink")）\[22\]、“大東諸島”等三大島群。詳細的島群劃分如下表所示：

<table>
<thead>
<tr class="header">
<th></th>
<th><p>[23]</p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><em><a href="../Page/薩南諸島.md" title="wikilink">薩南諸島</a></em></p></td>
<td><p><em><a href="../Page/大隅諸島.md" title="wikilink">大隅諸島</a></em></p></td>
<td><p><strong><a href="../Page/鹿兒島縣.md" title="wikilink">鹿兒島縣</a></strong></p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/吐噶喇列島.md" title="wikilink">吐噶喇列島</a></em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奄美群島.md" title="wikilink">奄美群島</a><sup>2</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/琉球諸島.md" title="wikilink">琉球諸島</a></em></p></td>
<td><p><em><a href="../Page/沖繩諸島.md" title="wikilink">沖繩諸島</a></em></p></td>
<td><p>（<a href="../Page/沖繩島.md" title="wikilink">沖繩島</a>、<a href="../Page/久米島.md" title="wikilink">久米島</a>、<a href="../Page/硫磺鳥島.md" title="wikilink">硫磺鳥島等</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/慶良間列島.md" title="wikilink">慶良間列島</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/先島諸島.md" title="wikilink">先島諸島</a></em></p></td>
<td><p><a href="../Page/宮古列島.md" title="wikilink">宮古列島</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八重山列島.md" title="wikilink">八重山列島</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尖閣諸島.md" title="wikilink">尖閣諸島</a><sup>4</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大東諸島.md" title="wikilink">大東諸島</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><li>
<p>註1：本列表使用<a href="../Page/日本政府.md" title="wikilink">日本政府採用的地名</a>，並以<a href="../Page/國土地理院.md" title="wikilink">國土地理院採用的地名為準</a>。</p>
<li>
<p>註2：奄美群島為日本政府在2010年同意採用的正式地名。[24]</p>
<li>
<p>註3：標示<em>斜體</em>者為日本政府尚未統一用法的地名。</p>
<li>
<p>註4：尖閣諸島即中國所稱的「<a href="../Page/釣魚臺列嶼.md" title="wikilink">-{zh-cn:钓鱼岛及其附属岛屿; zh-tw:釣魚臺列嶼; zh-hk:釣魚台及其附屬島嶼; zh-sg:钓鱼岛及其附属岛屿}-</a>」，而英語少量媒體則同時使用兩者（）。<a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a>、<a href="../Page/中華民國.md" title="wikilink">中華民國與日本</a><a href="../Page/釣魚臺列嶼主權問題.md" title="wikilink">皆主張擁有其主权</a>[25]。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

日文的「諸島」相當於中文的「群島」之意，至於日文的「南西諸島」為中文的「琉球群島」，故日文的「琉球諸島」中文亦可稱「琉球群島」，不同的是後者是狹義範圍、也就是僅[沖繩縣所轄的範圍](../Page/沖繩縣.md "wikilink")。此外，曾為[琉球國立國領土的奄美諸島](../Page/琉球國.md "wikilink")、沖繩諸島、宮古列島與八重山列島（以上即為琉球國最盛時期領土），在日本又被總稱為「**琉球列島**」。因此，中文的「琉球群島」就可能會有三種大小範圍的指稱。

## 主要島嶼

<small>注：[日本漢字名写法与](../Page/日本漢字.md "wikilink")[中文通用名稱略有不同](../Page/中文.md "wikilink")。</small>

### [薩南諸島](../Page/薩南群島.md "wikilink")

  - <small>（；中文稱「薩南群島」）</small>

<!-- end list -->

  - [大隅群島](../Page/大隅群島.md "wikilink")<small>（）</small>
      -
        [屋久島](../Page/屋久島.md "wikilink")、[種子島](../Page/種子島.md "wikilink")、[口永良部島](../Page/口永良部島.md "wikilink")、[馬毛島](../Page/馬毛島.md "wikilink")（[無人島](../Page/荒島.md "wikilink")）、[竹島](../Page/竹岛_\(鹿儿岛县\).md "wikilink")、[硫磺島](../Page/硫磺島_\(鹿兒島縣\).md "wikilink")（）、[黑島](../Page/黑岛_\(鹿儿岛县\).md "wikilink")

<!-- end list -->

  - [吐噶喇群島](../Page/吐噶喇群島.md "wikilink")<small>（）</small>
      -
        [口之島](../Page/口之島.md "wikilink")、[中之島](../Page/中之島.md "wikilink")、[臥蛇島](../Page/臥蛇島.md "wikilink")（無人島）、[小臥蛇島](../Page/小臥蛇島.md "wikilink")（無人島）、[平島](../Page/平島.md "wikilink")、[諏訪之瀨島](../Page/諏訪之瀨島.md "wikilink")、[惡石島](../Page/惡石島.md "wikilink")、小島（無人島）、[小寶島](../Page/小寶島.md "wikilink")、[寶島](../Page/寶島_\(鹿兒島縣\).md "wikilink")、[上之根島](../Page/上之根島.md "wikilink")（無人島）、[橫當島](../Page/橫當島.md "wikilink")（無人島）

<!-- end list -->

  - [奄美群島](../Page/奄美群島.md "wikilink")<small>（）</small>
      -
        [奄美大島](../Page/奄美大島.md "wikilink")、[枝手久島](../Page/枝手久島.md "wikilink")（無人島）、[喜界島](../Page/喜界島.md "wikilink")、[加計呂麻島](../Page/加計呂麻島.md "wikilink")、[江仁屋離島](../Page/江仁屋離島.md "wikilink")（無人島）、[須子茂島](../Page/須子茂島.md "wikilink")（無人島）、[與路島](../Page/與路島.md "wikilink")（）、[ハンミャ島](../Page/ハンミャ島.md "wikilink")（無人島）、[請島](../Page/請島.md "wikilink")、[木山島](../Page/木山島.md "wikilink")（無人島）、[德之島](../Page/德之島.md "wikilink")（）、[沖永良部島](../Page/沖永良部島.md "wikilink")、[與論島](../Page/與論島.md "wikilink")（）

### 琉球諸島

  - <small>（）</small>

#### [沖縄諸島](../Page/沖繩群島.md "wikilink")

  - <small>（；中文稱「沖繩群島」）</small>

<!-- end list -->

  - [硫磺鳥島](../Page/硫磺鳥島.md "wikilink")（無人島，）、[沖繩島](../Page/沖繩島.md "wikilink")（）、[瀨底島](../Page/瀨底島.md "wikilink")（）、[水納島](../Page/水納島_\(沖繩群島\).md "wikilink")、[宮城島](../Page/宮城島#宮城島_\(沖繩縣大宜味村\).md "wikilink")（[大宜味村](../Page/大宜味村.md "wikilink")）、[宮城島](../Page/宮城島#宮城島_\(沖繩縣宇流麻市\).md "wikilink")（[宇流麻市](../Page/宇流麻市.md "wikilink")）、[伊計島](../Page/伊計島.md "wikilink")、[濱比嘉島](../Page/濱比嘉島.md "wikilink")（）、[平安座島](../Page/平安座島.md "wikilink")、[藪地島](../Page/藪地島.md "wikilink")、[津堅島](../Page/津堅島.md "wikilink")、[古宇利島](../Page/古宇利島.md "wikilink")、[瀨長島](../Page/瀨長島.md "wikilink")（，無人島）、[屋我地島](../Page/屋我地島.md "wikilink")、[奧武島](../Page/奧武島#奧武島_\(名護市\).md "wikilink")（[名護市](../Page/名護市.md "wikilink")，無人島）、[久高島](../Page/久高島.md "wikilink")、[奧武島](../Page/奧武島#奧武島_\(南城市\).md "wikilink")（[南城市](../Page/南城市.md "wikilink")）、[伊江島](../Page/伊江村.md "wikilink")
  - [伊平屋伊是名群島](../Page/伊平屋伊是名群島.md "wikilink")<small>（）</small>：[伊平屋島、野甫島](../Page/伊平屋村.md "wikilink")（無人島）、[伊是名島](../Page/伊是名村.md "wikilink")
  - [慶良間群島](../Page/慶良間群島.md "wikilink")<small>（）</small>
      -
        [渡嘉敷島](../Page/渡嘉敷島.md "wikilink")、[前島](../Page/前島.md "wikilink")、[座間味島](../Page/座間味村.md "wikilink")、[阿嘉島](../Page/阿嘉島.md "wikilink")、[慶留間島](../Page/慶留間島.md "wikilink")、[外地島](../Page/外地島.md "wikilink")（無人島・慶良間空港）、[奧武島](../Page/奧武島#奧武島_\(座間味村\).md "wikilink")（[座間味村的無人島](../Page/座間味村.md "wikilink")）
  - [粟國島](../Page/粟國島.md "wikilink")（）、[渡名喜島](../Page/渡名喜島.md "wikilink")
  - [久米島](../Page/久米島.md "wikilink")、[奧武島](../Page/奧武島#奧武島_\(久米島町\).md "wikilink")（[久米島町](../Page/久米島町.md "wikilink")）、[東奥武島](../Page/東奥武島.md "wikilink")（）、[久米鳥島](../Page/久米鳥島.md "wikilink")

[Lastsunsetjapan.jpg](https://zh.wikipedia.org/wiki/File:Lastsunsetjapan.jpg "fig:Lastsunsetjapan.jpg")

#### [先島諸島](../Page/先島群島.md "wikilink")

  - <small>（；中文稱「先島群島」）</small>

<!-- end list -->

  - [宮古群島](../Page/宮古群島.md "wikilink")<small>（）</small>
      -
        [宮古島](../Page/宮古島.md "wikilink")、[池間島](../Page/池間島.md "wikilink")、[大神島](../Page/大神島.md "wikilink")、[來間島](../Page/來間島.md "wikilink")（）、[伊良部島](../Page/伊良部島.md "wikilink")、[下地島](../Page/下地島.md "wikilink")、[水納島](../Page/水納島_\(宮古群島\).md "wikilink")、[多良間島](../Page/多良間島.md "wikilink")
  - [八重山群島](../Page/八重山群島.md "wikilink")<small>（）</small>
      -
        [石垣島](../Page/石垣島.md "wikilink")、[竹富島](../Page/竹富島.md "wikilink")、[小濱島](../Page/小濱島.md "wikilink")（）、[黑島](../Page/黑島_\(八重山群島\).md "wikilink")、[新城島](../Page/新城島.md "wikilink")、[西表島](../Page/西表島.md "wikilink")、[鳩間島](../Page/鳩間島.md "wikilink")、[由布島](../Page/由布島.md "wikilink")、[波照間島](../Page/波照間島.md "wikilink")、[與那國島](../Page/與那國島.md "wikilink")（）
  - [-{zh-hans:钓鱼岛及其附属岛屿;zh-tw:釣魚臺列嶼;zh-hk:釣魚台及其附屬島嶼;}-](../Page/釣魚臺列嶼.md "wikilink")<small>（）</small>
    <small>※-{zh:中國;zh-hans:中国;zh-tw:中華民國;zh-hk:中國;}-與日本[皆主張擁有其主权](../Page/釣魚臺列嶼主權問題.md "wikilink")\[26\]\[27\]。</small>
      -
        [-{zh-hans:钓鱼岛;zh-tw:釣魚臺;zh-hk:釣魚台;}-](../Page/釣魚臺.md "wikilink")（）、[黃尾嶼](../Page/黄尾屿.md "wikilink")（）、[赤尾嶼](../Page/赤尾嶼.md "wikilink")（）、-{[北小島](../Page/北小島.md "wikilink")}-、-{[南小島](../Page/南小島.md "wikilink")}-、[-{zh-hans:北屿;zh-hant:沖北岩;}-](../Page/沖北岩.md "wikilink")（）、[-{zh-hans:南屿;zh-hant:沖南岩;}-](../Page/沖南岩.md "wikilink")（）、-{zh-cn:[飞屿](../Page/飛瀨.md "wikilink");zh-hk:[飛嶼](../Page/飛瀨.md "wikilink");zh-tw:[飛瀨](../Page/飛瀨.md "wikilink");}-（）

### [大東諸島](../Page/大東群島.md "wikilink")

  - <small>（；中文稱「大東群島」）</small>

<!-- end list -->

  - [北大東島](../Page/北大東村.md "wikilink")、[沖大東島](../Page/沖大東島.md "wikilink")、[南大東島](../Page/南大東村.md "wikilink")

<File:Yonaguni> Ruins Scuba.jpg|海底遺跡
[File:Suwanosejima.jpg|諏訪之瀬島](File:Suwanosejima.jpg%7C諏訪之瀬島)
<File:Amami> beach.jpg|奄美大島 <File:Fukuzato> Dam Okinawa 1.jpg|宮古島福里井
<File:Ishigaki> omotodake.jpg|茂登岳 <File:Miyakojima> sky view.jpg|宮古列島
<File:Miyako> ikema bridge.JPG|池間橋 <File:Ishigaki> kosetsu
ichiba.jpg|石垣市公設市場

## 历史

### 獨立國家時期

[琉球群島在歷史上曾有](../Page/琉球历史#早期历史.md "wikilink")[琉球国等獨立之國家](../Page/琉球国.md "wikilink")，古琉球國並與[中國建立長久的](../Page/中國.md "wikilink")[朝貢關係](../Page/朝貢體系.md "wikilink")，因水路之便而成為邻近国家的贸易枢杻。

1609年，[琉球国被位於](../Page/琉球国.md "wikilink")[日本](../Page/日本.md "wikilink")[九州最南部的](../Page/九州.md "wikilink")[薩摩藩侵略](../Page/薩摩藩.md "wikilink")\[28\]，失去[奄美群島之領土](../Page/奄美群島.md "wikilink")，琉球國也因此同時與中国（[明朝與](../Page/明朝.md "wikilink")[清朝](../Page/清朝.md "wikilink")）及日本薩摩藩、[江户幕府有朝貢關係](../Page/江户幕府.md "wikilink")。琉球国因其特殊的地理位置，以[东北亚和](../Page/东北亚.md "wikilink")[东南亚贸易的中转站著称](../Page/东南亚.md "wikilink")，贸易发达\[29\]\[30\]，具有重要的戰略地位。

### 日本帝国時期

[明治维新後](../Page/明治维新.md "wikilink")，日本出兵占领琉球群岛，於1872年强制“[册封](../Page/册封.md "wikilink")”琉球国王为[藩王](../Page/藩王.md "wikilink")（当时琉球国尚屬清日两国的附属国），設置[琉球藩](../Page/琉球藩.md "wikilink")；1879年琉球藩被廢除，3月先編入鹿儿岛县，4月又改置沖繩縣，原琉球藩奄美群島除外的其餘島嶼群均納入[沖繩縣管轄](../Page/沖繩縣.md "wikilink")，奄美群島则分属[鹿儿岛县轄下](../Page/鹿儿岛县.md "wikilink")。清政府雖对此[於1877年至1880年提出交涉](../Page/琉球案.md "wikilink")，但隨著清朝國勢的衰弱而無疾而終。這個琉球被日本合併的事件被稱為「[琉球處分](../Page/琉球處分.md "wikilink")」。

### 美國託管時期

[第二次世界大战](../Page/第二次世界大战.md "wikilink")[太平洋战争期間](../Page/太平洋战争.md "wikilink")，琉球群岛為交戰雙方重要的軍事據點。1945年發生[沖繩島戰役](../Page/沖繩島戰役.md "wikilink")，美軍登陸沖繩本島，4月成立[美國海軍政府](../Page/琉球列島美國軍政府.md "wikilink")，管轄的範圍包括奄美群岛（1945年至1953年），沖繩群島、宮古群島和八重山群島（1945年至1972年）。1946年7月轉為美國陸軍政府管辖，1950年12月改組為[琉球列島美國民政府](../Page/琉球列島美國民政府.md "wikilink")（）。[美國治理琉球時期](../Page/美國治理琉球時期.md "wikilink")，曾在琉球群島设立[奄美群島政府、沖繩群島政府、宮古群島政府、八重山群島](../Page/群島政府_\(琉球\).md "wikilink")，後来1952年4月改为[地方廳](../Page/地方廳_\(琉球政府\).md "wikilink")，由琉球群島美國民政府设立的[琉球政府管辖](../Page/琉球政府.md "wikilink")。

战後，根据《[波茨坦公告](../Page/波茨坦公告.md "wikilink")》第8条的补充规定「开罗宣言之条件必将实施，而日本之主权必将限于本州、北海道、九州、四国及吾人所决定其他小岛之内」\[31\]\[32\]，但未載明是哪些小島。

### 回歸日本治理

代表[同盟國的美国等](../Page/同盟國_\(第二次世界大戰\).md "wikilink")48個國家（[中華民國與](../Page/中華民國.md "wikilink")[蘇聯未參與](../Page/蘇聯.md "wikilink")）在1951年與日本签定《[舊金山和約](../Page/舊金山和約.md "wikilink")》，日本同意美國擁有對琉球群島的行政、立法、司法等權，美國則在1953年與1972年先後將[奄美群島與](../Page/奄美群島.md "wikilink")[沖繩群島的管理權](../Page/沖繩群島.md "wikilink")“[移交日本](../Page/琉球群島和小笠原群島政權移交.md "wikilink")”（），直至今日\[33\]。琉球群島目前分屬日本國的[鹿儿岛县辖下与](../Page/鹿儿岛县.md "wikilink")[沖繩縣全县](../Page/沖繩縣.md "wikilink")：北部的奄美群島成為鹿儿岛县的一部分，而餘下的群島均隸屬於沖繩縣。

## 参考文献

## 外部連結

  - [沖繩旅遊│沖繩琉球│沖繩石垣島‧和昇清風會館](http://breeze.sunrise-inn.com.tw//)

  - [琉球群島亞熱帶長綠森林(WWF)](http://www.worldwildlife.org/wildworld/profiles/terrestrial/im/im0170_full.html)

  - [琉球文化網](http://rca.open.ed.jp/)

  - [琉球情報網](http://www.ryukyu.net/)

## 参见

  - [琉球海溝](../Page/琉球海溝.md "wikilink")
  - [琉球漢文文獻](../Page/琉球漢文文獻.md "wikilink")
  - [琉球国](../Page/琉球国.md "wikilink")
  - [琉球藩](../Page/琉球藩.md "wikilink")
  - [琉球君主列表](../Page/琉球君主列表.md "wikilink")
  - [琉球語](../Page/琉球語.md "wikilink")
  - [沖繩縣](../Page/沖繩縣.md "wikilink")
  - [琉球独立運動](../Page/琉球独立運動.md "wikilink")
  - [沖繩縣祖國復歸協議會](../Page/沖繩縣祖國復歸協議會.md "wikilink")
  - [宮古海峽](../Page/宮古海峽.md "wikilink")

{{-}}

[琉球群岛](../Category/琉球群岛.md "wikilink")
[Category:沖繩縣島嶼](../Category/沖繩縣島嶼.md "wikilink")
[Category:鹿兒島縣島嶼](../Category/鹿兒島縣島嶼.md "wikilink")
[Category:琉球國](../Category/琉球國.md "wikilink")

1.

2.  日本稱整個琉球群島為「南西諸島」、琉球群島中屬原[琉球國部分](../Page/琉球國.md "wikilink")（大致同今沖繩縣轄域）為「琉球諸島」。

3.

4.

5.

6.

7.  大英|大英百科全書{{\!}}大英\]\]線上| accessdate=2012年5月17日| language=en | quote
    = Ryukyu Islands, also called Nansei Islands, …… archipelago,
    extending some 700 miles (1,100 km) southwestward from the southern
    Japanese island of Kyushu to northeastern Taiwan. }}

8.  大英|大英百科全書{{\!}}大英\]\]線上| accessdate=2008年7月25日| language=中文}}

9.

10.

11.

12. 参见[沖繩返還](../Page/沖繩返還.md "wikilink")。

13.
14.
15.
16.

17. 菅田（1995年）p.380

18.

19.
20.
21.

22.

23.
24.

25.
26.

27.

28.

29. 《中國與琉球》，第317至334頁，附錄一：「明代在琉球任使者通事火長的中國移民」。

30. Okamoto, Hiromichi. "Foreign Policy and Maritime Trade in the Early
    Ming Period Focusing on the Ryukyu Kingdom." *Acta Asiatica* vol. 95
    (2008), p. 35.

31.

32.

33.