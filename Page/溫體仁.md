**溫體仁**（），字長卿，號園嶠，[浙江](../Page/浙江.md "wikilink")[烏程縣](../Page/烏程縣.md "wikilink")（今[湖州市](../Page/湖州市.md "wikilink")）[南浔辑里村人](../Page/南浔.md "wikilink")，[明朝政治人物](../Page/明朝.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。崇禎年間擔任內閣首輔。崇禎朝閣員五十人，唯溫體仁最久。崇禎十年免職。卒謚文忠。

## 生平

溫體仁為[萬曆二十五年](../Page/萬曆.md "wikilink")（1597年）[举人](../Page/举人.md "wikilink")，时年二十四，补[博士](../Page/博士.md "wikilink")[弟子员](../Page/弟子员.md "wikilink")。[萬曆二十六年](../Page/萬曆.md "wikilink")（1598年）中[进士](../Page/进士.md "wikilink")，[萬曆二十八年](../Page/萬曆.md "wikilink")（1600年）授[翰林院编修](../Page/翰林院编修.md "wikilink")，[萬曆四十四年](../Page/萬曆.md "wikilink")（1616年）升[少詹事](../Page/少詹事.md "wikilink")，掌[南京](../Page/南京.md "wikilink")[翰林院印](../Page/翰林院.md "wikilink")。[天启二年](../Page/天启_\(明朝\).md "wikilink")（1622年）升[礼部右侍郎](../Page/礼部右侍郎.md "wikilink")，协理[詹事](../Page/詹事.md "wikilink")，次年回部任[左侍郎](../Page/左侍郎.md "wikilink")。天啟七年（1627年）晋[南京礼部尚书](../Page/南京礼部尚书.md "wikilink")。[崇祯三年](../Page/崇祯.md "wikilink")（1630年）任[礼部尚书兼](../Page/礼部尚书.md "wikilink")[东阁大学士](../Page/东阁大学士.md "wikilink")，加[太子太保](../Page/太子太保.md "wikilink")，进[文渊阁](../Page/文渊阁.md "wikilink")，[崇禎五年](../Page/崇禎.md "wikilink")（1632年）加[少保兼](../Page/少保.md "wikilink")[太子太保](../Page/太子太保.md "wikilink")，[户部尚书](../Page/户部尚书.md "wikilink")，进[武英殿](../Page/武英殿.md "wikilink")。史載“体仁辅政数年，念朝士多与为怨，不敢恣肆，用廉谨自结于上，苞苴不入门。”

溫體仁與[禮部侍郎](../Page/禮部侍郎.md "wikilink")[周延儒兩人在](../Page/周延儒.md "wikilink")[明毅宗即位之初選擇閣輔時](../Page/明毅宗.md "wikilink")，以[錢謙益收賄名義打擊錢謙益](../Page/錢謙益.md "wikilink")，並且全力打擊錢謙益同黨，而被不愛黨爭的毅宗信任，[崇禎元年](../Page/崇禎.md "wikilink")（1628年）便與周延儒入閣並出任[禮部尚書](../Page/禮部尚書.md "wikilink")[東閣大學士](../Page/東閣大學士.md "wikilink")。而後來溫體仁又排擠周延儒，並出任閣輔。

溫體仁善於利用毅宗的個性而當政八年，並得到毅宗的恩禮優渥，而且毅宗也以為溫體仁是個不組黨羽的孤立之士；但是「同官非病免物故，即以他事去」，引發朝臣的不滿與批評；[崇祯十年](../Page/崇祯.md "wikilink")（1637年）[劉宗周等人上奏指出溫體仁的十二大罪](../Page/劉宗周.md "wikilink")，而最後[司禮監](../Page/司禮監.md "wikilink")[太監](../Page/太監.md "wikilink")[曹化淳向毅宗告密](../Page/曹化淳.md "wikilink")，指稱溫體仁自有黨羽，思宗大驚說“體仁有黨”，最後免除溫體仁的閣輔之職\[1\]。[崇祯十年](../Page/崇祯.md "wikilink")（1637年）六月，一日温体仁正在吃饭。忽有[太监传旨](../Page/太监.md "wikilink")，令削去温体仁官职。温体仁一驚，手中[筷子掉在地上](../Page/筷子.md "wikilink")。[崇禎十一年](../Page/崇禎.md "wikilink")（1638年）溫體仁便病故[烏程家中](../Page/烏程.md "wikilink")。《明史》有傳。\[2\]

## 評價

[御史](../Page/御史.md "wikilink")[吴履中曾總結](../Page/吴履中.md "wikilink")[明亡的教訓說](../Page/明.md "wikilink")：“温体仁托严正之义，行媚嫉之私，使朝廷不得任人以治事，酿成祸源，体仁之罪也。”\[3\]溫體仁為政嚴苛，人皆稱「温体仁全师江陵（張居正）之术而加甚焉」[崇禎朝有](../Page/崇禎.md "wikilink")[民謠](../Page/民謠.md "wikilink")“[禮部重開天榜](../Page/禮部.md "wikilink")，[狀元](../Page/狀元.md "wikilink")[探花](../Page/探花.md "wikilink")[榜眼](../Page/榜眼.md "wikilink")，有些惶恐。[內閣翻成妓館](../Page/內閣.md "wikilink")，烏龜王八篾片，總是遭瘟”便是諷刺溫體仁當政的情況。溫體仁主政期間，只有少部分的清流敢出來告溫體仁，但下場都非常淒慘，如太學生鄔曼就曾告發溫體仁結黨，但下場卻是被溫體仁投入牢獄中，不知所終。

溫體仁死後，[明思宗贈為](../Page/明思宗.md "wikilink")[太傅](../Page/太傅.md "wikilink")，[諡](../Page/諡.md "wikilink")**文忠**。[福王](../Page/福王.md "wikilink")[朱由崧即位於](../Page/朱由崧.md "wikilink")[南京後便削其](../Page/南京.md "wikilink")[諡號](../Page/諡號.md "wikilink")，弘光元年（1645年）三月又恢复了温体仁的[諡號](../Page/諡號.md "wikilink")\[4\]\[5\]。《[明史](../Page/明史.md "wikilink")》將溫體仁列入奸臣傳，評價曰：“[流寇躏畿辅](../Page/流寇.md "wikilink")，扰[中原](../Page/中原.md "wikilink")，边警杂沓，民生日困，未尝建一策，惟日与善类为仇”。

[王夫之](../Page/王夫之.md "wikilink")《讀通鑑論》評論[董仲舒時提到](../Page/董仲舒.md "wikilink")：「溫體仁行保薦以亂之，重武科以亢之，[楊嗣昌設社塾以淆之](../Page/楊嗣昌.md "wikilink")，於是乎士氣偷，姦民逞，而生民之禍遂極。」

## 墓葬

[光绪](../Page/光绪.md "wikilink")《乌程县志》记有“明大学士温体仁墓在毗山下”，现有“明大学士温体仁墓遗址”石碑。

## 参考文献

{{-}}

[Category:萬曆二十五年丁酉科舉人](../Category/萬曆二十五年丁酉科舉人.md "wikilink")
[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:明朝詹事府少詹事](../Category/明朝詹事府少詹事.md "wikilink")
[Category:南京禮部尚書](../Category/南京禮部尚書.md "wikilink")
[Category:明朝禮部尚書](../Category/明朝禮部尚書.md "wikilink")
[Category:明朝東閣大學士](../Category/明朝東閣大學士.md "wikilink")
[Category:明朝吏部尚書](../Category/明朝吏部尚書.md "wikilink")
[Category:明朝文淵閣大學士](../Category/明朝文淵閣大學士.md "wikilink")
[Category:明朝武英殿大學士](../Category/明朝武英殿大學士.md "wikilink")
[Category:明朝中極殿大學士](../Category/明朝中極殿大學士.md "wikilink")
[Category:明朝太子三師](../Category/明朝太子三師.md "wikilink")
[Category:明朝太保](../Category/明朝太保.md "wikilink")
[Category:明朝内阁首辅](../Category/明朝内阁首辅.md "wikilink")
[Category:湖州人](../Category/湖州人.md "wikilink")
[T](../Category/溫姓.md "wikilink")
[Category:諡文忠](../Category/諡文忠.md "wikilink")

1.  《[明季北略](../Page/明季北略.md "wikilink")》（卷4）：“十一月，上御暖阁，召问温体仁参钱谦益浙闱关节之事。先是，有旨会推枚卜，钱谦益名列第二，而温体仁不与。体仁因参谦益，受田千秋数千金之贿，以一朝平步上青天为关节。取中之，结党欺君，故上召对诘问。体仁与谦益质辩不已。上问诸臣。周延儒对曰：田千秋关节是真。辅臣钱龙锡等对曰：关节实与钱谦益无干。上曰：关节既真，他为主考，如何说不是他。遂命拟旨。钱谦益既有物议，回籍听勘。田千秋下法司再问，科臣章允儒辩体仁以党字加诸臣。是从来小人害君子榜样。上怒其胡扯，着锦衣卫拿下。”
2.  《明史·列傳308卷》7931-7937
3.  《明史纪事本末》
4.  《明季南略》卷2：“初四丁亥，吏部尚书张捷奏：‘故辅温体仁清忠谨恪，当复‘文忠’之谥，顾锡畴以私憾议削；文震孟宜改谥。’上命温允复，文免议。”
5.  《爝火录》卷9：“吏部尚书张捷覆奏：‘故辅温体仁清执忠谨，当复‘文忠’之谥；文震孟宜改谥。’蔡奕琛票旨：‘温体仁准复原谥，文震孟免议。’”