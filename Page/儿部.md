[儿-order.gif](https://zh.wikipedia.org/wiki/File:儿-order.gif "fig:儿-order.gif")
-{**儿部**，是為漢字索引的[部首之一](../Page/部首.md "wikilink")，[康熙字典](../Page/康熙字典.md "wikilink")214個部首第十個（二劃的則為第四個）。就[正體中文中](../Page/正體.md "wikilink")，儿部歸於二劃部首。儿部只以下方為部字，且無其他部首可用者將部首歸為儿部。}-

## 部首單字解釋

1.ㄖㄣˊ，為人字古文。見[說文](../Page/說文.md "wikilink")。

2.ㄐㄧㄝˋ，為仁人。見[集韻](../Page/集韻.md "wikilink")。

## 字形

[File:儿-oracle.svg|甲骨文](File:儿-oracle.svg%7C甲骨文)
[File:儿-bronze.svg|金文](File:儿-bronze.svg%7C金文)
[File:儿-bigseal.svg|大篆](File:儿-bigseal.svg%7C大篆)
[File:儿-seal.svg|小篆](File:儿-seal.svg%7C小篆)

## 名稱

  - 漢語：-{儿}-部（拼音：　注音：ㄖㄣˊ ㄅㄨˋ）
  - 日語：
  - 朝鲜語：

## 字音

  - 漢語：拼音：　注音：ㄖㄣˊ
  - 日語：吳音：　漢音：
  - 朝鲜語：

## 部首字元及變形

  - <span style="font-size:x-large;">⼉</span>（康熙部首）：KANGXI RADICAL LEGS
    (U+2F09)

### 位於文字區

  - <span style="font-size:x-large;">-{儿}-</span>：CJK UNIFIED
    IDEOGRAPH-513F

## 字例

| 除部首外之筆劃 | 字例 -{   |
| ------- | ------- |
| 0       | 儿       |
| 1       | 兀       |
| 2       | 允兂元     |
| 3       | 兄       |
| 4       | 㒫兊充兆兇先光 |
| 5       | 克兌免兎兏児兑 |
| 6       | 兔㒬兒兓兕兖𠒇 |
| 7       | 𠒎兗兘兙    |
| 8       | 㒭党兛𠒑𠒒   |
| 9       | 㒮兜兝兞    |
| 10      | 𠒣兟兠     |
| 11      | 𠒥𠒦兡     |
| 12      | 𠒬𠒯兢     |
| 13      | 𠒷       |
| 14      | 兣       |
| 15      | 𠓀𠓁𠓅𠓇    |
| 16      | 㒯       |
| 18      | 𠓐       |
| 19      | 𠓑兤      |
| 20      | 𠓔       |
| 22      | 𠓗 }-    |

[Category:部首](../Category/部首.md "wikilink")