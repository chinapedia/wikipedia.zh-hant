**阿尔内·弗雷德里希**（[德语](../Page/德语.md "wikilink")：**Arne
Friedrich**，），已退役[德国](../Page/德国.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，司职后卫，曾效力于[比勒费尔德](../Page/比勒费尔德足球俱乐部.md "wikilink")、[哈化柏林及](../Page/柏林赫塔足球俱乐部.md "wikilink")[禾夫斯堡](../Page/沃尔夫斯堡足球俱乐部.md "wikilink")。弗雷德里希在2002年入選[德國國家隊](../Page/德國國家足球隊.md "wikilink")，至今代表國家隊82次，參加了2004年歐洲足球錦標賽、2006年世界杯、2008年歐洲足球錦標賽和2010年世界杯。

## 生平

### 球會

在2000/01年球季，費特列治簽下首份職業球員合約。當時[德乙的](../Page/德乙.md "wikilink")[比勒费尔德教練從](../Page/比勒费尔德足球俱乐部.md "wikilink")[德国足球地区联赛一間業餘球會將他羅致](../Page/德国足球地区联赛.md "wikilink")，取得正選席位。2002年，費特列治加盟[德甲](../Page/德甲.md "wikilink")[哈化柏林](../Page/柏林赫塔足球俱乐部.md "wikilink")，協助球隊當年擊敗[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")、[史浩克04](../Page/史浩克04.md "wikilink")、[多蒙特](../Page/多蒙特.md "wikilink")，取得[德國足球協會聯賽盃](../Page/德國足球協會聯賽盃.md "wikilink")。2004/05年球季，費特列治走上事業顛峰，成為球隊[隊長](../Page/隊長_\(足球\).md "wikilink")。

哈化柏林於2010年降班後，費特列治以200萬[歐元轉投](../Page/歐元.md "wikilink")[禾夫斯堡](../Page/沃尔夫斯堡足球俱乐部.md "wikilink")，合約為期3年\[1\]。但於[南非世界盃後費特列治一直受到傷患困擾](../Page/2010年世界盃足球賽.md "wikilink")，因腰椎間盤突出而需要接受[手術治理](../Page/手術.md "wikilink")，缺席整個上半季賽事，復出後未能恢復狀態。[2011/12年球季展開前](../Page/2011年至2012年德國足球甲級聯賽.md "wikilink")，他又因舊患復發而再度休戰，只能跟隨二隊訓練以保持狀態。於2011年9月19日費特列治與禾夫斯堡協議提前解約\[2\]。
2013年6月23日，費特列治宣佈退休。

### 國家隊

費特列治於2002年8月21日首次代表[德國國家隊上陣出戰](../Page/德國國家足球隊.md "wikilink")[保加利亞](../Page/保加利亞國家足球隊.md "wikilink")，初期與[-{zh-hans:欣克尔;
zh-hk:軒基;}-輪席擔任右翼衛](../Page/安德烈亚斯·欣克尔.md "wikilink")，其後獲主教練[-{zh-hans:勒夫;
zh-hk:路維;}-指派出任擅長的中堅位置](../Page/约阿希姆·勒夫.md "wikilink")。

費特列治曾出席多項大型國際賽事，協助國家隊取得[2005年洲際國家盃季軍](../Page/2005年洲際國家盃.md "wikilink")、[2008年歐國盃亞軍及兩屆](../Page/2008年欧洲足球锦标赛.md "wikilink")[世界盃季軍](../Page/世界盃足球賽.md "wikilink")，合共上陣82場，並曾4次擔任國家隊[隊長](../Page/隊長_\(足球\).md "wikilink")。

|- |1. || 2010年7月3日 ||
[南非](../Page/南非.md "wikilink")[開普敦](../Page/開普敦.md "wikilink")[開普敦球場](../Page/開普敦球場.md "wikilink")
||  || **3**–0 || 4–0 || [2010年世界盃](../Page/2010年世界盃足球賽.md "wikilink")
|}

## 參考資料

## 外部链接

  - [阿尔内·弗雷德里希官方网站](http://www.arnefriedrich.de/)

[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:德国足球运动员](../Category/德国足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:比勒費爾德球員](../Category/比勒費爾德球員.md "wikilink")
[Category:哈化柏林球員](../Category/哈化柏林球員.md "wikilink")
[Category:禾夫斯堡球員](../Category/禾夫斯堡球員.md "wikilink")
[Category:芝加哥火焰球員](../Category/芝加哥火焰球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:美職球員](../Category/美職球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:银月桂叶获得者](../Category/银月桂叶获得者.md "wikilink")

1.
2.