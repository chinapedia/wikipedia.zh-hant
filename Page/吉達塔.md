**吉達塔**（，），前稱**一哩塔**（，）、**王國塔**（，），位於[沙特阿拉伯城市](../Page/沙特阿拉伯.md "wikilink")[吉達](../Page/吉達.md "wikilink")（Jeddah）一项[摩天大樓計劃](../Page/摩天大樓.md "wikilink")，計劃中的大樓高約1,600[米](../Page/米.md "wikilink")（5,250[呎](../Page/呎.md "wikilink")），高度接近全球第一高峰──[珠穆朗瑪峰](../Page/珠穆朗瑪峰.md "wikilink")（Mount
Everest，8848米）的五分之一，但因為當地地質無法承受過重的建築物，且在沙漠中興建高樓也是難題，在後續的修改中被縮減到1,008米（3,307呎）。\[1\]大樓建成後將超越[杜拜的](../Page/杜拜.md "wikilink")[哈里發塔成為全球最高建築物](../Page/哈里發塔.md "wikilink"),亦是全球第一座高度1000米的建築物。\[2\]但位於[杜拜的](../Page/杜拜.md "wikilink")[杜拜河港塔於](../Page/杜拜河港塔.md "wikilink")2016年10月10日動工，設計高度約1,100\~1,200米以上，意味著兩座塔的[世界第一高樓與](../Page/世界第一高樓.md "wikilink")[人工構造物的競爭正式開始](../Page/人工構造物.md "wikilink")。2018年2月，由於建築工人欠薪發生罷工事件。截至2018年8月，大厦周圍基礎設施建設進行中，但主樓仍然沒有動靜。

吉達塔是耗资200億美元王國城市發展區的地標。內部設計從底層至樓頂階段分別為四季酒店、四季服務公寓、特級辦公樓、豪華公寓和世界最高的觀望台（位於652米的157層）。

## 建造過程

2011年8月2日，沙特阿拉伯王子阿勒瓦利德·本·塔拉勒宣布大楼工程近期開始動土。沙烏地阿拉伯最大建筑商－[沙特本拉登集团是参与兴建工程的主要建筑商](../Page/沙特本拉登集团.md "wikilink")。建造期限估計需要大約5年3個月的時間才能完成。\[3\]

经过长时间的酝酿和准备，沙特吉达市的吉達塔确定将于2014年4月27日开建，预计将于2020年完工。吉達塔建成后将成为世界最高建筑，其高度在1008米左右，比[哈里發塔](../Page/哈里發塔.md "wikilink")（828米）高将近180米。\[4\]

## 大樓規劃

吉達塔電梯規劃將長達660米，為世界最長電梯。傳統電梯需使用鋼索進行牽拖，其重量將限制傳統電梯的極限長度。吉達塔將採用高科技碳纖維電纜技術(UltraRope)降低電梯整體重量、增加電梯長度與速度，以及節省因電纜過重而消耗的能源。電梯升降速度將達每秒升降10公尺，經測試，將不會有身體不適的問題。\[5\]

## 註解

  -
    **A.** ^ The actual height of the Burj Khalifa was not revealed
    until the opening ceremony. The developers of Jeddah Tower are
    similarly keeping the final height secret, stating only that it will
    be at least  high. In a television interview for WTTW in Chicago,
    architect Adrian Smith stated that the tower will be a little over
    twice the height of the [Burj Khalifa](../Page/哈里發塔.md "wikilink"),
    which is  high counting the antennas, suggesting that Jeddah Tower
    may be closer to . Some sources have suggested that Jeddah Tower
    will break Burj Khalifa's record by over .\[6\]In 2014, online
    sources were using the number .\[7\]

<!-- end list -->

  -
    **B.** ^ This figure has been referred to as the "total building
    area" in publications, which is probably another term for [gross
    floor area](../Page/gross_floor_area.md "wikilink"); the actual
    usable floor space, which is what buildings are typically measured
    by, will presumably be less. This area has also been reported as
    \[8\]

<!-- end list -->

  -
    **C.** ^ This assumes a 2012 start date and the estimated completion
    time of 63 months.

<!-- end list -->

  -
    **D.** ^ Figure attained using the formula for the area of a circle
    (π*r*<sup>2</sup>) and the given value of  as the diameter.

<!-- end list -->

  -
    **E.** ^ No official floor count has been given; however, Adrian
    Smith said it will be about 50 floors taller than the (163 floor)
    [Burj Khalifa](../Page/哈里發塔.md "wikilink").\[9\]It is uncertain but
    probable that this counts unoccupied [mechanical
    floors](../Page/mechanical_floor.md "wikilink") in the spire. One
    source states, "Tower residents and visitors will occupy 160 floors
    within the tower."\[10\]In November 2010, an older kilometre-high
    design was said to contain over 200 floors.\[11\]

## 參考資料

## 外部連結

  - [吉達市王國塔](http://www.kingdomtowerskyscraper.com/)
  - [Kingdom Holding Company
    website](https://web.archive.org/web/20110813095642/http://www.kingdom.com.sa/en/)
  - [Kingdom
    Tower](https://web.archive.org/web/20130728034923/http://skyscrapercenter.com/jeddah/kingdom-tower/)
    on [CTBUH](../Page/CTBUH.md "wikilink") Skyscraper Center
  - [Slideshow image gallery of
    renderings](http://www.foxnews.com/slideshow/world/2011/08/03/kingdom-tower/#slide=1)
    by [Fox News](../Page/Fox_News.md "wikilink")
  - [13:40 WTTW video interview with Adrian Smith about Kingdom
    Tower](http://www.youtube.com/watch?v=DqP5DjCx2r0) on
    [YouTube](../Page/YouTube.md "wikilink")

[Category:沙特阿拉伯摩天大樓](../Category/沙特阿拉伯摩天大樓.md "wikilink")
[Category:未來建設](../Category/未來建設.md "wikilink")
[Category:超過350米高的摩天大樓](../Category/超過350米高的摩天大樓.md "wikilink")
[Category:吉達建築物](../Category/吉達建築物.md "wikilink")

1.

2.  [沙特欲建1600米世界最高大厦
    乘电梯12分钟](http://news.sohu.com/20110412/n280238403.shtml)，搜狐新闻

3.  [拉丹集团参与兴建世界最高大厦](http://news.ifeng.com/gundong/detail_2011_08/03/8145705_0.shtml)，凤凰网

4.  [沙特将建1007米世界最高楼
    挑战828米迪拜塔(图)](http://news.163.com/14/0404/15/9P0GBPRT0001121M.html)，网易网

5.

6.

7.
8.
9.

10.
11.