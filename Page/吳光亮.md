[缩略图](https://zh.wikipedia.org/wiki/File:玉里協天宮_\(5\)_沿革碑記.jpg "fig:缩略图")沿革史碑文\]\]
**吳光亮**（），字**霽軒**，為[清朝](../Page/清朝.md "wikilink")[軍事人物](../Page/軍事.md "wikilink")，[廣東省](../Page/廣東省_\(清\).md "wikilink")[南韶連道](../Page/南韶連道.md "wikilink")[韶州府](../Page/韶州府.md "wikilink")[英德縣](../Page/英德縣.md "wikilink")（今[廣東省](../Page/廣東省.md "wikilink")[清遠市](../Page/清遠市.md "wikilink")[英德市](../Page/英德市.md "wikilink")）人。早期受知於[左宗棠的部下](../Page/左宗棠.md "wikilink")[康器國](../Page/康器國.md "wikilink")，整頓順昌新營，後被任命為南澳鎮總兵。

## 生平

[同治十三年](../Page/同治.md "wikilink")（1874年）因[沈葆楨奏調來台](../Page/沈葆楨.md "wikilink")，負責中路[林圮埔](../Page/林圮埔.md "wikilink")（今[南投縣](../Page/南投縣.md "wikilink")[竹山鎮](../Page/竹山鎮.md "wikilink")）到[璞石閣](../Page/璞石閣.md "wikilink")（今[花蓮縣](../Page/花蓮縣.md "wikilink")[玉里鎮](../Page/玉里鎮.md "wikilink")）開山撫番，全長265華里。[光緒元年](../Page/光緒.md "wikilink")（1875年）元月開始，同年11月完工，以其功受[福寧鎮總兵](../Page/福寧鎮總兵.md "wikilink")。三年（1877年）奉旨接[張其光擔任](../Page/張其光.md "wikilink")[臺灣鎮總兵](../Page/臺灣鎮總兵.md "wikilink")。\[1\]吳光亮亦為[玉里協天宮開基者](../Page/玉里協天宮.md "wikilink")。\[2\]

光緒四年（1878年）在[大港口事件中](../Page/大港口事件.md "wikilink")，有計畫的設下飯局屠殺[阿美族精英青壯年一百多人](../Page/阿美族.md "wikilink")，迫使港口一帶阿美族的搬遷，瓦解當地青壯年對[朝廷的反抗勢力](../Page/朝廷.md "wikilink")。以及對[撒奇萊雅族及](../Page/撒奇萊雅族.md "wikilink")[噶瑪蘭族進行屠殺的](../Page/噶瑪蘭族.md "wikilink")[加禮宛事件](../Page/加禮宛事件.md "wikilink")。

光緒十四年（1888年），吳光亮回任[臺灣鎮總兵](../Page/臺灣鎮總兵.md "wikilink")，卻因為「向屬員函借[銀兩](../Page/銀兩.md "wikilink")」被朝廷交予[台灣巡撫](../Page/台灣巡撫.md "wikilink")[劉銘傳查辦](../Page/劉銘傳.md "wikilink")。後吳光亮被[兵部議處](../Page/兵部.md "wikilink")，得旨，降三級調用。

光緒十六年（1890年）受[劉銘傳再度起用](../Page/劉銘傳.md "wikilink")，二十年（1895年）受[唐景崧之邀募勇來台](../Page/唐景崧.md "wikilink")，號「飛虎軍」，其部趁危劫餉風紀不佳，至日軍攻陷[八卦山時潰敗而離台](../Page/八卦山.md "wikilink")\[3\]。

## 參考資料

  - 書目

<!-- end list -->

  - 劉寧顏編，《重修台灣省通志》，台北市，[台灣省文獻委員會](../Page/台灣省文獻委員會.md "wikilink")，1994年。
  - 林文龍著，《吳光亮傳》，南投市中興新村，台灣省文獻委員會，1994年。

<!-- end list -->

  - 引用

## 相關條目

  - [大港口事件](../Page/大港口事件.md "wikilink")
  - [加禮宛事件](../Page/加禮宛事件.md "wikilink")
  - [玉里協天宮](../Page/玉里協天宮.md "wikilink")

[Category:台灣鎮總兵](../Category/台灣鎮總兵.md "wikilink")
[Category:吳姓](../Category/吳姓.md "wikilink")

1.  [郭廷以《郭廷以先生百歲冥誕紀念史學論文集:　左宗棠與台灣》](https://books.google.com.tw/books?id=V0wFAzs3UDMC&pg=PA183&lpg=PA183&dq=%E7%BE%85%E5%A4%A7%E6%98%A5+%E5%A4%AA%E5%B9%B3%E5%A4%A9%E5%9C%8B&source=bl&ots=aM1EhGZ7XT&sig=rHoCUX58dXBdhwMGA3jyM39hJB4&hl=zh-TW&sa=X&ved=0ahUKEwiR38vx2OLUAhUEsJQKHePuDXoQ6AEIOzAF#v=onepage&q=%E7%BE%85%E5%A4%A7%E6%98%A5%20%E5%A4%AA%E5%B9%B3%E5%A4%A9%E5%9C%8B&f=false)

2.

3.