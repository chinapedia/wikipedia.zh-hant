**KISS Radio
聯播網**是以[台灣](../Page/台灣.md "wikilink")[高雄市為基地的音樂性](../Page/高雄市.md "wikilink")[廣播聯播網](../Page/廣播聯播網.md "wikilink")，以**大眾廣播**為中心，定頻為99.9兆赫，台呼分為「就是KISS最音樂」、「擋不住的魅力」兩種。「KISS」為[英文](../Page/英文.md "wikilink")「**K**aohsiung
**I**nteractive **S**uper **S**tation」縮寫，早期KISS
Radio於高雄市部分[7-ELEVEN便利商店門市內播放](../Page/統一超商.md "wikilink")（2011年起已停止播放）。

## 介紹

KISS Radio
主要以音樂為重心，多次在[台灣各地協辦歌手簽唱會](../Page/台灣.md "wikilink")，上午七點至晚間七點每半小時會播出半點新聞或娛樂新聞，主持人也會在廣播中提供當地的活動及演唱會資訊，上下班時間主持人會提供路況及交通安全互動遊戲。

目前KISS
Radio的台呼由多位歌手演唱，也有一些台呼由現有歌曲改編，團體[八三夭的](../Page/八三夭.md "wikilink")「東區東區」一曲即改編為台呼，並由八三夭親自演唱。

## 歷史

  - 1995年2月14日開播，是高雄市第一家合法的民營調頻電台。
  - 1996年，大眾廣播公司曾一度想和台北「人人廣播電台」進行策略聯盟，讓人人廣播電台聯播KISS
    Radio部份節目，最後兩家電台未合作\[1\]。（人人廣播電台已於2002年9月成為[好事聯播網台北台](../Page/好事聯播網.md "wikilink")）。
  - 1997年起，大眾廣播公司陸續與台南知音、大苗栗廣播、南投廣播進行[策略聯盟聯播大眾廣播公司部份節目](../Page/策略聯盟.md "wikilink")，四家電台合稱「KISS
    Radio 聯播網」。
  - 2000年，KISS
    Radio跨足[數位廣播](../Page/數位廣播.md "wikilink")（[DAB](../Page/DAB.md "wikilink")），並致力發展[網路電台](../Page/網路電台.md "wikilink")。同年與台北淡水河廣播電台（FM89.7）簽約聯播。
  - 2001年，KISS Radio與淡水河電台解約，淡水河電台轉型為閩南語電台。
  - 2006年，KISS
    Radio與[公共電視文化事業基金會合作](../Page/公共電視文化事業基金會.md "wikilink")，播出《公視晚間新聞》的[聲音部分](../Page/聲音.md "wikilink")。(之後和[中天電視合作](../Page/中天電視.md "wikilink")，由中天主播播報當天的新聞（合作至2016年12月底）)
  - 2006年，「網路音樂台」正式開播，並持續榮登HiNet網路廣播金榜的榜首。
  - 2006年，與新加坡動力883（的前身）進行節目交流，聯合製播《KISS發燒榜－華語榜》節目於新加坡播出。
  - 2007年，「KISS美少女啦啦隊全國選拔賽」，入圍廣播金鐘獎「電台行銷創新獎」。
  - 2008年，KK和JOHNNIE以「KISS K歌榜」節目獲得廣播金鐘獎流行音樂主持人獎。
  - 2008年，高雄[夢時代OPEN](../Page/夢時代.md "wikilink") STUDIO正式啓用。
  - 2008年，舉行「暑期青少年嘉年華-青春專案」活動，經行政院內政部評鑑獲選為全國唯一廣播績優團體。
  - 2010年12月1日起，結束與「大苗栗廣播公司」及「臺南知音廣播」節目聯播關係，大苗栗及臺南知音聯播城市廣播網。（合作時名為GoldFM聯播網）
  - 2017年11月20日，KISS Radio更改12:32、18:30的《焦點新聞》片頭音樂。

## 收聽頻率

KISS
Radio屬於中、小功率聯播，部分地區會因地形因素無法收聽，收聽範圍集中在發射中心附近。若距離稍遠會出現雜訊及干擾，有些地區雖位於收聽範圍內卻完全無法收聽。**大眾廣播為目前全台唯一使用99.9
Mhz的電台**。

| 地區     | 電台名稱     | 收聽頻率                  | 收聽地區               |
| ------ | -------- | --------------------- | ------------------ |
| **南部** | **大眾廣播** | FM 99.9 Mhz ／3Kw (兆赫) | 南部高雄市、屏東縣及台南市部分地區  |
| **中部** | **南投廣播** | FM 99.7 Mhz ／3Kw (兆赫) | 中部臺中市海線區以外、南投縣、彰化縣 |

  - **網路音樂台**：KISS Radio 網路音樂臺
  - 線上收聽：大眾廣播\[2\]（[LIVEhouse.in](../Page/LIVEhouse.in.md "wikilink")）、南投廣播\[3\]（Youtube）、南投廣播\[4\]（hiChannel）
      - 雲林縣及嘉義縣因為「神農廣播電台」99.5 Mhz的關係，只能收聽高雄總台
      - 原**大苗栗廣播**與**台南之音**為KISS
        Radio聯播網，後退出聯播改與[城市廣播網聯播](../Page/城市廣播網.md "wikilink")

## 新聞時段

  - KISS Radio焦點新聞：平日15:33\~15:35、18:33\~18:35、19:33～19:35

## 特色單元

  - 發燒時間
  - 交通安全GO GO GO
  - 生活常識百貨公司
  - 生活理財家
  - 法律知多少
  - 聽歌學英文
  - 百萬財經學堂
  - 高雄大小事
  - 數字說話
  - KISS 一把照
  - 星星一族（每天22:30播出，隔日03：30、10:15重播）
  - KISS電影院

## 節目表

### 大眾

| 時段          | 星期一                                   | 星期二            | 星期三               | 星期四 | 星期五 | 星期六 | 星期日 |
| ----------- | ------------------------------------- | -------------- | ----------------- | --- | --- | --- | --- |
| **上午00:00** | KISS音樂世界                              | KISS夜未眠        |                   |     |     |     |     |
| **上午02:00** | KISS A GO GO                          | KISS A GO GO   | 音樂滿點              |     |     |     |     |
| **上午06:00** | KISS MORNING                          |                |                   |     |     |     |     |
| **上午07:00** | KISS MORNING - JOJO                   | 樂來樂動聽          | 音樂狂想曲             |     |     |     |     |
| **上午10:00** | KISS i PLAY - KIKI                    | KISS FUN音樂- 歐啦 | 音樂.COM- 宗正        |     |     |     |     |
| **中午12:00** | KISS音樂風- 楊淳                           |                |                   |     |     |     |     |
| **下午01:00** | 音樂嘉年華 曲曲                              | 非常星期天 - POLAR  |                   |     |     |     |     |
| **下午02:00** | KISS 放輕鬆 - 星亞                         |                |                   |     |     |     |     |
| **下午04:00** | 就是週末 最HAPPY - RICH                    | 歡樂電波           |                   |     |     |     |     |
| **下午05:00** | KISS DAY & NIGHT - VANESSA            |                |                   |     |     |     |     |
| **下午06:00** | KISS DAY & NIGHT - 強尼                 |                |                   |     |     |     |     |
| **下午07:00** | 娛樂PARTY - 強尼                          |                |                   |     |     |     |     |
| **下午08:00** | 流行磁場 - [KK](../Page/KK.md "wikilink") | 音樂同樂會 - 強尼     | KISS SUNDAY NIGHT |     |     |     |     |

### 南投

| 時段          | 星期一                                   | 星期二                | 星期三               | 星期四 | 星期五 | 星期六 | 星期日 |
| ----------- | ------------------------------------- | ------------------ | ----------------- | --- | --- | --- | --- |
| **上午00:00** | 愈晚愈動聽                                 | 音樂YOU\&ME          | 愈晚愈動聽             |     |     |     |     |
| **上午02:00** | 音樂YOU\&ME                             | 音樂YOU\&ME          |                   |     |     |     |     |
| **上午07:00** | KISS MORNING - JOJO                   | 音樂歐嗨喲              | 音樂早晨              |     |     |     |     |
| **上午10:00** | KISS i PLAY - KIKI                    | KISS FUN音樂         | 音樂.COM- 宗正        |     |     |     |     |
| **中午12:00** | KISS音樂風- 楊淳                           |                    |                   |     |     |     |     |
| **下午01:00** | 音樂嘉年華 曲曲                              | 非常星期天 - POLAR      |                   |     |     |     |     |
| **下午02:00** | KISS 放輕鬆 - 星亞                         |                    |                   |     |     |     |     |
| **下午04:00** | 絕對音樂 - 芳翎                             | 就是週末 最HAPPY - RICH | 歡樂電波              |     |     |     |     |
| **下午05:00** | KISS DAY & NIGHT - VANESSA            |                    |                   |     |     |     |     |
| **下午06:00** | KISS DAY & NIGHT - 強尼                 |                    |                   |     |     |     |     |
| **下午07:00** | 娛樂PARTY - 強尼                          |                    |                   |     |     |     |     |
| **下午08:00** | 流行磁場 - [KK](../Page/KK.md "wikilink") | 音樂同樂會 - 強尼         | KISS SUNDAY NIGHT |     |     |     |     |

## DJ

  - **大眾廣播**

| **DJ** |
| :----: |
|   KK   |
|  RICH  |
|   星亞   |
|   歐啦   |

  - **南投廣播**

| **DJ** |
| :----: |
|   芳翎   |

## 聯播制

KISS
Radio與南投廣播是採取聯播制，因法律規定聯播分臺需抽比例製作自製節目，因此部份時段南投廣播聽眾是無法收聽到高雄總台節目，目前週一00:00～07:00、14:00～17:00；週二至週五02:00～07:00、14:00～17:00；週六02:00～13:00、23:00～00:00（原13:00\~16:00的「自播時段」已改聯播高雄總台的《音樂嘉年華》）；週日00:00～11:00、23:00～00:00南投廣播為自製節目\[5\]。（[農曆新年](../Page/新春.md "wikilink")（除夕到初五）期間，南投廣播改為24小時聯播高雄總台的節目）

## 參考資料

<div class="references-small">

<references />

</div>

## 參閱

  - [臺灣廣播電台列表](../Page/臺灣廣播電台列表.md "wikilink")

## 外部連結

  - [KISS Radio首頁](http://www.kiss.com.tw)

  -
  -
[Category:台灣廣播電台](../Category/台灣廣播電台.md "wikilink")
[Category:電台網](../Category/電台網.md "wikilink")

1.  1998年，**蔡念中**等合著，《大眾傳播概論》，ISBN 978-957-11-1673-0，第112頁
2.  [大眾廣播](http://www.kiss.com.tw/radio_hq.php?radio_id=156)
3.  [南投廣播](http://www.youtube.com/channel/UCR-UrFgPFE9M-IIKbqWvaIA/live)
4.  [南投廣播](http://hichannel.hinet.net/radio/index.do?id=238)
5.  [南投廣播104年7月份節目時間表](http://nccstat.ncc.gov.tw/ncc/stnccpr.jsp?sys=1&datef=1040726&datet=1040802&timef=0&timet=24&area=203&f1=1&f2=1&w0=1&w1=1&w2=1&w3=1&w4=1&w5=1&w6=1&k1=1&k2=2&k3=3&k4=4&i1=1&s1=1&s2=2&s3=3&aplid=1059&freq=&cname=&freqnm=&pgmnm=&ldrpsn=&rdm=BeLnNrpb)