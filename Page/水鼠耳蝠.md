**水鼠耳蝠**（**''Myotis
daubentonii**''），又稱**道氏鼠耳蝠**，是一種廣泛分佈[歐亞大陸的一種](../Page/歐亞大陸.md "wikilink")[鼠耳蝠](../Page/鼠耳蝠.md "wikilink")。其學名***daubentonii***是來自[法國一名自然學家Louis](../Page/法國.md "wikilink")-Jean-Marie
**Daubenton**。

## 特徵

水鼠耳蝠是一種細至中等身形的蝙蝠。其背部毛色為啡色至黃啡色不等。牠們的肚皮的顏色較淺，眼睛細小，耳殼長而向慢彎。牠們在小時的毛色會比成年的較深色。牠們身長平均
50 毫米長，翼伸長時平均 260 毫米，體重平均 7-15 [克](../Page/克.md "wikilink")。

## 生活習性

水鼠耳蝠廣泛分佈歐亞大陸：[歐洲記錄有](../Page/歐洲.md "wikilink")[英國及](../Page/英國.md "wikilink")[愛爾蘭](../Page/愛爾蘭.md "wikilink")；[亞洲記錄有](../Page/亞洲.md "wikilink")[南韓](../Page/南韓.md "wikilink")、[日本](../Page/日本.md "wikilink")、[中國與](../Page/中國.md "wikilink")[越南等地](../Page/越南.md "wikilink")。牠們喜歡住在樹洞、引水道及礦坑。牠們一般獨居，但有時會三四隻在一齊。

## 食物

與很多[蝙蝠一樣](../Page/蝙蝠.md "wikilink")，水鼠耳蝠夜間會利用[動物回聲定位找尋獵物](../Page/回聲定位.md "wikilink")。牠們會用這種方法畫出一幅「[聲納圖](../Page/聲納.md "wikilink")」，所發出的頻率大約
32 至 85 kHz， 一般的叫聲以 45 到 50 kHz 為最高，有 3.3 ms 這樣久。

牠們主要是食蟲性，以[烏鷹](../Page/烏鷹.md "wikilink")，[蜉蝣等](../Page/蜉蝣.md "wikilink")[昆蟲等為食物](../Page/昆蟲.md "wikilink")，但也有捕捉[魚的記錄](../Page/魚.md "wikilink")。牠們會用腳捉著往水面的魚，而且可以一邊飛一邊吃。外國研究顯示，一隻7克重的水鼠耳蝠經過一個小時獵食後，體重可增加至11克，相當於本身體重的57%。

## 繁殖

於外國，水鼠耳蝠主要在[秋天時候](../Page/秋天.md "wikilink")[交配](../Page/交配.md "wikilink")，至[春天生產](../Page/春天.md "wikilink")。牠們通常一胎一隻，蝙蝠出生後大約三個星期就懂得飛行，至6至8個星期後就可以完全獨立。

## 保育狀況

由1981年起，所有[英國境內的](../Page/英國.md "wikilink")[蝙蝠都係受到](../Page/蝙蝠.md "wikilink")《[野生動物及郊野公園條例](../Page/野生動物及郊野公園條例.md "wikilink")》第5條保護，而[香港也有相似法例](../Page/香港.md "wikilink")：《[野生動物保護條例](../Page/野生動物保護條例.md "wikilink")》，但香港自1990年後再沒有水鼠耳蝠的記錄。此外，水鼠耳蝠於[德國及](../Page/德國.md "wikilink")[奧地利屬於瀕危物種](../Page/奧地利.md "wikilink")。

## 參見

  - [香港哺乳動物](../Page/香港哺乳動物.md "wikilink")

[Category:翼手目](../Category/翼手目.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國哺乳動物](../Category/中國哺乳動物.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")