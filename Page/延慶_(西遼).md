**延慶**（1124年二月\[1\]-1133年）是[西辽君主辽德宗](../Page/西辽.md "wikilink")[耶律大石的](../Page/耶律大石.md "wikilink")[年號](../Page/年號.md "wikilink")，共計10年。

## 紀元

元代史籍《[遼史](../Page/辽史.md "wikilink")》以1124年二月為改元延慶之年，計10年。錢大昕《十駕齋養新錄》卷八〈西遼紀年〉考訂認為1125年（保大五年）改元延慶，至1127年止，計3年。近代學者如紀安宗、李錫厚、柴平等皆認為在1131年或1132年之間改元延慶，計3年。下表紀年仍從《遼史》。

  -
    {| class="wikitable" style="text-align:center"

|- \!延慶||元年||二年||三年||四年||五年||六年||七年||八年||九年||十年 |-
\![公元](../Page/公元.md "wikilink")
|1124年||1125年||1126年||1127年||1128年||1129年||1130年||1131年||1132年||1133年
|- \![干支](../Page/干支.md "wikilink") |[甲辰](../Page/甲辰.md "wikilink")||
[乙巳](../Page/乙巳.md "wikilink")|| [丙午](../Page/丙午.md "wikilink")||
[丁未](../Page/丁未.md "wikilink")|| [戊申](../Page/戊申.md "wikilink")||
[己酉](../Page/己酉.md "wikilink")|| [庚戌](../Page/庚戌.md "wikilink")||
[辛亥](../Page/辛亥.md "wikilink")|| [壬子](../Page/壬子.md "wikilink")||
[癸丑](../Page/癸丑.md "wikilink") |}

## 參看

  - [中国年号列表](../Page/中国年号列表.md "wikilink")
  - 其他政权使用的[延庆年号](../Page/延庆.md "wikilink")
  - 同期存在的其他政权年号
      - [宣和](../Page/宣和_\(北宋\).md "wikilink")（1119年二月至1125年十二月）：[宋](../Page/北宋.md "wikilink")—[宋徽宗趙佶](../Page/宋徽宗.md "wikilink")、[宋欽宗趙桓之年號](../Page/宋欽宗.md "wikilink")
      - [靖康](../Page/靖康.md "wikilink")（1126年正月至1127年四月）：宋—宋欽宗趙桓之年號
      - [建炎](../Page/建炎.md "wikilink")（1127年五月至1130年十二月）：[宋](../Page/南宋.md "wikilink")—[宋高宗趙構之年號](../Page/宋高宗.md "wikilink")
      - [明受](../Page/明受.md "wikilink")（1129年三月至四月）：宋—元懿太子[赵旉之年號](../Page/赵旉.md "wikilink")
      - [紹興](../Page/紹興_\(南宋\).md "wikilink")（1131年正月至1162年十二月）：宋—宋高宗趙構之年號
      - [阜昌](../Page/阜昌.md "wikilink")（1130年十一月至1137年十一月）：宋時期—[劉豫之年號](../Page/劉豫.md "wikilink")
      - [天載](../Page/天载.md "wikilink")（1130年二月至三月）：南宋時期—[鍾相之年號](../Page/鍾相.md "wikilink")
      - [正法](../Page/正法_\(李合戎\).md "wikilink")：南宋時期—[李合戎](../Page/李合戎.md "wikilink")、[雷進之年號](../Page/雷進.md "wikilink")
      - [人知](../Page/人知.md "wikilink")：南宋時期—[雷進之年號](../Page/雷進.md "wikilink")
      - [太平](../Page/太平_\(李婆備\).md "wikilink")：南宋時期—[李婆備之年號](../Page/李婆備.md "wikilink")
      - [大聖天王](../Page/大聖天王.md "wikilink")（1133年四月至1135年六月）：宋時期—[杨幺之年號](../Page/岳飛剿滅楊幺.md "wikilink")
      - [天符睿武](../Page/天符睿武.md "wikilink")（1120年至1126年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李乾德之年號](../Page/李乾德.md "wikilink")
      - [天符慶壽](../Page/天符慶壽.md "wikilink")（1127年）：李朝—李乾德之年號
      - [天順](../Page/天順_\(李陽煥\).md "wikilink")（1128年至1132年）：李朝—[李陽煥之年號](../Page/李陽煥.md "wikilink")
      - [天彰寶嗣](../Page/天彰寶嗣.md "wikilink")（1133年至1138年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李陽煥之年號](../Page/李陽煥.md "wikilink")
      - [保大](../Page/保大_\(辽朝\).md "wikilink")（1121年正月至1125年二月）：[遼](../Page/辽朝.md "wikilink")—[遼天祚帝耶律延禧之年號](../Page/遼天祚帝.md "wikilink")
      - [天會](../Page/天会_\(金朝\).md "wikilink")（1123年九月至1137年十二月）：[金](../Page/金朝.md "wikilink")—[金太宗吳乞買](../Page/金太宗.md "wikilink")、[金熙宗完颜亶之年號](../Page/金熙宗.md "wikilink")
      - [元德](../Page/元德_\(西夏\).md "wikilink")（1119年正月至1127年三月）：[西夏](../Page/西夏.md "wikilink")—[夏崇宗李乾順之年號](../Page/夏崇宗.md "wikilink")
      - [正德](../Page/正德_\(西夏\).md "wikilink")（1127年四月至1134年十二月）：西夏—夏崇宗李乾順之年號
      - [天治](../Page/天治.md "wikilink")（1124年四月三日至1126年一月二十二日）：日本崇德天皇年号
      - [大治](../Page/大治_\(崇德天皇\).md "wikilink")（1126年一月二十二日至1131年正月二十九日）：日本崇德天皇年号
      - [天承](../Page/天承.md "wikilink")（1131年一月二十九日至1132年八月十一日）：日本崇德天皇年号
      - [長承](../Page/長承.md "wikilink")（1132年八月十一日至1135年四月二十七日）：日本崇德天皇年号

## 参考文献

[Category:西辽年号](../Category/西辽年号.md "wikilink")
[Category:12世纪中国年号](../Category/12世纪中国年号.md "wikilink")
[Category:1120年代中国](../Category/1120年代中国.md "wikilink")
[Category:1130年代中国](../Category/1130年代中国.md "wikilink")

1.