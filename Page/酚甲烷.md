**双酚A**（**Bisphenol
A**，缩写为**BPA**），-{zh-hans:[台灣多稱之為](../Page/台灣.md "wikilink")**酚甲烷**;
zh-hant:[中國大陸多稱為](../Page/中國大陸.md "wikilink")**雙酚A**;}-。一种[化工原料](../Page/化工原料.md "wikilink")，是已知的[内分泌干扰素](../Page/内分泌干扰素.md "wikilink")（[环境荷尔蒙](../Page/环境荷尔蒙.md "wikilink")）。它是一种[有机化合物](../Page/有机化合物.md "wikilink")，具有两个[酚](../Page/酚.md "wikilink")[官能团](../Page/官能团.md "wikilink")。双酚A被用于合成[聚碳酸酯塑料和](../Page/聚碳酸酯.md "wikilink")[环氧树脂等](../Page/环氧树脂.md "wikilink")。

20世纪30年代中期人们就发现了双酚A可以发挥[雌激素的作用](../Page/雌激素.md "wikilink")，它在各个领域应用的争论也就此开始。到2008年，一些政府开始对它在消费领域的安全性提出正式的质疑，并陆续采取措施让相关产品下架。2010年，[美國食品藥品監督管理局依据在多胎儿](../Page/美國食品藥品監督管理局.md "wikilink")、婴儿和幼儿中收集到的数据资料提出进一步的担忧\[1\]；当年9月，[加拿大成为首个将之列为有毒物质的国家](../Page/加拿大.md "wikilink")\[2\]\[3\]。在[欧盟和加拿大](../Page/欧盟.md "wikilink")，双酚A被禁止用于生产[婴儿奶瓶](../Page/婴儿奶瓶.md "wikilink")。2012年9月24日，美國[華盛頓州立大學等機構刊登其研究](../Page/華盛頓州立大學.md "wikilink")，公布其在[獼猴進行的實驗結果](../Page/獼猴.md "wikilink")，證實雙酚A會影響獼猴雌性後代的[生殖系統](../Page/生殖系統.md "wikilink")，導致[卵子](../Page/卵子.md "wikilink")[染色體異常](../Page/染色體.md "wikilink")\[4\]\[5\]。

## 生产

20世纪80年代，这种化合物在全世界的产量超过了一百万吨\[6\]，而2009年全世界更是出产了超过二百二十万吨双酚A\[7\]。2003年，[美国对双酚A的总消耗为](../Page/美国.md "wikilink")
856,000 吨，其中的 72% 被用于生产[聚碳酸酯塑料](../Page/聚碳酸酯.md "wikilink")， 21%
被用于生产[环氧树脂](../Page/环氧树脂.md "wikilink")\[8\]。在美国，双酚A应用于与食品接触的领域的比例少于
5% \[9\]。

双酚A在1891年被俄罗斯化学家首次合成\[10\]\[11\]。合成这种[化合物时](../Page/化合物.md "wikilink")，可以采用[丙酮与两](../Page/丙酮.md "wikilink")[当量的](../Page/化學當量.md "wikilink")[苯酚](../Page/苯酚.md "wikilink")[缩合](../Page/缩合.md "wikilink")（丙酮的英文单词为
acetone
，首字母为A，也就是“双酚A”名字中“A”的来源）\[12\]。这个反应需要无机强酸，比如[盐酸](../Page/盐酸.md "wikilink")（HCl），或者[聚苯乙烯磺酸钠的](../Page/聚苯乙烯磺酸钠.md "wikilink")[催化](../Page/催化.md "wikilink")。工业生产中，通常会在投料时加入过量的苯酚以保证丙酮被充分反应，而[异丙苯法得到的粗产品](../Page/异丙苯法.md "wikilink")（丙酮与苯酚的混合物）也常被直接用作原料：\[13\]

  -
    [Synthesis_Bisphenol_A.svg](https://zh.wikipedia.org/wiki/File:Synthesis_Bisphenol_A.svg "fig:Synthesis_Bisphenol_A.svg")

很多种类的[酮也可以发生类似的缩合反应](../Page/酮.md "wikilink")。无论是来自于树脂生产中副产品的[超真空提纯还是从溶液中提取](../Page/超真空.md "wikilink")，商品双酚A需要进一步[蒸馏](../Page/蒸馏.md "wikilink")[提纯](../Page/提纯.md "wikilink")。\[14\]

## 用途

双酚A的基本用途是生产各种塑料，这样的商品早在1957年就已经广泛出现在商业领域\[15\]。制造业每年至少要使用80亿磅的双酚A\[16\]。它是生产[环氧树脂的关键](../Page/环氧树脂.md "wikilink")[单体](../Page/单体.md "wikilink")\[17\]\[18\]，更是[聚碳酸酯塑料最常用的原料](../Page/聚碳酸酯.md "wikilink")\[19\]\[20\]\[21\]。生产聚碳酸酯的总反应可以写成：

  -
    [Polycarbonatsynthese.svg](https://zh.wikipedia.org/wiki/File:Polycarbonatsynthese.svg "fig:Polycarbonatsynthese.svg")

聚碳酸酯塑料是一种透明且不易碎的材料，常被用于生产各种水瓶（包括婴儿用的奶瓶）、运动装备、医疗器械、[牙充填的材料](../Page/牙充填.md "wikilink")、[密封剂](../Page/密封.md "wikilink")、[眼镜镜片](../Page/眼镜.md "wikilink")、[CD与](../Page/CD.md "wikilink")[DVD和家用电器外壳](../Page/DVD.md "wikilink")\[22\]。双酚A也被常用于合成[聚砜和](../Page/聚砜.md "wikilink")[聚醚](../Page/聚醚.md "wikilink")、在[塑化剂中作](../Page/塑化剂.md "wikilink")[抗氧化剂](../Page/抗氧化剂.md "wikilink")、在[PVC的生产中作](../Page/PVC.md "wikilink")[阻聚剂](../Page/阻聚剂.md "wikilink")。其中环氧树脂几乎被用于所有食品与罐装饮料包装的内层涂料\[23\]。然而，处于对人体健康的考虑，在[日本](../Page/日本.md "wikilink")，环氧树脂几乎完全被[PET替代](../Page/聚对苯二甲酸乙二酯.md "wikilink")\[24\]。双酚A也是合成[阻燃剂](../Page/阻燃剂.md "wikilink")[四溴双酚A的前体](../Page/四溴双酚A.md "wikilink")。以前它也曾被用于[杀菌](../Page/杀菌.md "wikilink")\[25\]。生产[无碳复写纸和](../Page/无碳复写纸.md "wikilink")[感热纸时](../Page/感热纸.md "wikilink")，双酚A可以作为显色剂以实现复写或对热源产生反应\[26\]，前者被广泛应用在[销售时点情报系统的收银小票上](../Page/销售时点情报系统.md "wikilink")\[27\]\[28\]\[29\]。另外，双酚A也是输水管道的内层涂料\[30\]。

由於酚甲烷是一種[脂溶性的](../Page/脂溶性.md "wikilink")[有機化合物](../Page/有機化合物.md "wikilink")，當高脂肪含量的食物用塑膠容器盛載，有可能會令塑膠內的酚甲烷釋出。[紐西蘭就曾因為市面上的椰漿含有較一般食物為高的酚甲烷而展在](../Page/紐西蘭.md "wikilink")2005年展開調查，結果發現酚甲烷是來自椰漿包裝內層的塑料膜\[31\]。透過減少塑料膜的酚甲烷，其後的調查發現椰漿的酚甲烷水平下降到安全水平以下\[32\]。

### 塑料中的标识

[Plastic-recyc-03.svg](https://zh.wikipedia.org/wiki/File:Plastic-recyc-03.svg "fig:Plastic-recyc-03.svg")塑料在生产过程中可能接触过双酚A|left\]\]
[Plastic-recyc-07.svg](https://zh.wikipedia.org/wiki/File:Plastic-recyc-07.svg "fig:Plastic-recyc-07.svg")塑料在生产过程中可能接触过双酚A|right\]\]
一般来讲，[塑胶分类标志为](../Page/塑胶分类标志.md "wikilink")1、2、4、5、6的[塑料不太可能在生产中与双酚A接触](../Page/塑料.md "wikilink")，而有一些（不是全部）[塑胶分类标志为](../Page/塑胶分类标志.md "wikilink")3或7的塑料生产中可能接触到了双酚A。\[33\]

塑料制品上往往可以看到[循环再造标志和塑胶分类标志](../Page/循环再造标志.md "wikilink")，标志中央印有包括1至7七个阿拉伯数字中的一个。其中的“7”代表所有的“其它塑料”，包括[聚碳酸酯](../Page/聚碳酸酯.md "wikilink")（有时会在循环再造标志附近注明“PC”字样）和[环氧树脂](../Page/环氧树脂.md "wikilink")，它们的生产原料都是双酚A。\[34\]
\[35\]

3号（[PVC](../Page/PVC.md "wikilink")）和一些7号塑料同样可能含有双酚A，因为在生产软质PVC的过程中，双酚A会作为[抗氧化剂被添加到](../Page/抗氧化剂.md "wikilink")[塑化剂中](../Page/塑化剂.md "wikilink")。\[36\]

## 参见

  - [食品添加剂](../Page/食品添加剂.md "wikilink")

  -
  -
  - [感热纸](../Page/感热纸.md "wikilink")

## 参考资料

[BPA](../Category/双酚.md "wikilink") [BPA](../Category/塑化劑.md "wikilink")
[BPA](../Category/环境激素.md "wikilink")
[BPA](../Category/俄羅斯發明.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.
9.
10.

11.

12.

13.
14.

15.

16.

17.

18.

19.
20.

21.

22.

23.

24.

25. [Pesticideinfo.org: Bisphenol
    A](http://pesticideinfo.org/Detail_Chemical.jsp?Rec_Id=PC33756)

26.

27.

28.

29.

30.

31.

32.
33. <http://www.hhs.gov/safety/bpa/>

34.
35.

36.