**庫爾納專區**（）是[孟加拉國六個一級行政區之一](../Page/孟加拉國.md "wikilink")，位於該國西南部。北界[恆河](../Page/恆河.md "wikilink")，西隔[賴蒙戈爾河與](../Page/賴蒙戈爾河.md "wikilink")[印度相望](../Page/印度.md "wikilink")，南臨[孟加拉灣](../Page/孟加拉灣.md "wikilink")，東界[哈林-{卡達}-河](../Page/哈林卡達河.md "wikilink")。面積22285.41平方公里，2001年人口14,705,229人。\[1\]首府[庫爾納市](../Page/庫爾納市.md "wikilink")。
下分10縣、28自治市、6,941村。

Shat_Gombuj_Mosque_(ষাট_গম্বুজ_মসজিদ)_002.jpg|Shat清真寺
Tagore_Kuthibari.jpg|[罗宾德拉纳特·泰戈尔故居](../Page/罗宾德拉纳特·泰戈尔.md "wikilink")
Tomb_Lalon.jpg|拉隆之墓
Kushtia_Islamic_University_Auditorium,_Kushtia,_Bangladesh.jpg|伊斯蘭大學

## 参考文献

[分类:孟加拉国专区](../Page/分类:孟加拉国专区.md "wikilink")

[\*](../Category/库尔纳专区.md "wikilink")

1.  [Area, Population and Literacy Rate by
    Upazila/Thana-2001](http://www.bbs.gov.bd/dataindex/census/ce_uzila.pdf)