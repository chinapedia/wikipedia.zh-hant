[MoorishAmbassador_to_Elizabeth_I.jpg](https://zh.wikipedia.org/wiki/File:MoorishAmbassador_to_Elizabeth_I.jpg "fig:MoorishAmbassador_to_Elizabeth_I.jpg")会见[伊丽莎白一世的](../Page/伊丽莎白一世.md "wikilink")[柏柏尔人大使](../Page/柏柏尔人.md "wikilink")。在油画中称为“摩尔人大使”。\]\]
**摩尔人**（，）是指[中世纪](../Page/中世纪.md "wikilink")[伊比利亚半岛](../Page/伊比利亚半岛.md "wikilink")（今[西班牙和](../Page/西班牙.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")）、[西西里島](../Page/西西里島.md "wikilink")、[撒丁尼亞](../Page/撒丁尼亞.md "wikilink")、[馬爾他](../Page/馬爾他.md "wikilink")、[科西嘉島](../Page/科西嘉島.md "wikilink")、[马格里布和](../Page/马格里布.md "wikilink")[西非的](../Page/西非.md "wikilink")[穆斯林居民](../Page/穆斯林.md "wikilink")。历史上，摩尔人主要指在伊比利亚半岛的伊斯兰征服者。直到3世纪，“毛利”这个名字罗马人用来对所有未罗马化的由自己首领统治的[北非土著的称呼](../Page/北非.md "wikilink")。摩尔人主要由[埃塞俄比亚人](../Page/埃塞俄比亚人.md "wikilink")、[撒哈拉人](../Page/撒哈拉人.md "wikilink")、[阿拉伯人和](../Page/阿拉伯人.md "wikilink")[柏柏尔人组成](../Page/柏柏尔人.md "wikilink")，也有伊比利半岛出身的土著穆斯林（[穆拉迪人](../Page/穆拉迪人.md "wikilink")）。

历史学家认为，摩尔人是一个阶级和文化的统称。尽管非洲摩尔人占大多数，摩尔人这一概念其实并没有人类学和民族学上的意义。\[1\]的著作《自然界不论颜色》中阐述了此一观点。继而《非洲：西方文明之母》的作者证实这个族群是来自[埃塞俄比亚然后移居到非洲西北部和东北部](../Page/埃塞俄比亚.md "wikilink")。事实上，直到640年阿拉伯人入侵北非之后，摩尔人才和阿拉伯扯上关系，即他们都是穆斯林。今天的摩尔人并不认同自己为[白人或](../Page/白人.md "wikilink")[黑人](../Page/黑人.md "wikilink")。

摩尔人一词在欧洲使用很广泛且略带贬义，一般指[穆斯林](../Page/穆斯林.md "wikilink")\[2\]，特别是[西班牙或](../Page/西班牙.md "wikilink")[北非的](../Page/北非.md "wikilink")[阿拉伯人或](../Page/阿拉伯人.md "wikilink")[柏柏尔人](../Page/柏柏尔人.md "wikilink")\[3\]。在葡萄牙殖民印时代，[葡萄牙人在](../Page/葡萄牙人.md "wikilink")[斯里兰卡和印度](../Page/斯里兰卡.md "wikilink")[果阿使用](../Page/果阿.md "wikilink")“锡兰摩尔人”和“印度摩尔人”两个词，[孟加拉国的穆斯林也被称为](../Page/孟加拉国.md "wikilink")“摩尔人”\[4\]。[莎士比亚的](../Page/莎士比亚.md "wikilink")《[奥赛罗](../Page/奥赛罗.md "wikilink")》中的主人公就是“[威尼斯的摩尔人](../Page/威尼斯.md "wikilink")”。

## 历史

[Map_Iberian_Peninsula_750-es.svg](https://zh.wikipedia.org/wiki/File:Map_Iberian_Peninsula_750-es.svg "fig:Map_Iberian_Peninsula_750-es.svg")
公元前46年，[罗马人进入](../Page/罗马人.md "wikilink")[北非西部在看到非洲人之后](../Page/北非.md "wikilink")，罗马人把他们称作“毛利人”，这个词源自[希腊语](../Page/希腊语.md "wikilink")“毛罗人”（意思是“黑暗的、淡淡的、朦朦朧朧的”）。在北非、东非、西非的一部分，尽管人们属于不同的种族，但大多有深色的皮肤。

640年，[伊斯兰教兴起之后](../Page/伊斯兰教.md "wikilink")，新兴的[阿拉伯帝国四面扩张](../Page/阿拉伯帝国.md "wikilink")。708年伊斯兰教传入北非。大量当地人把[阿拉伯语接受为母语](../Page/阿拉伯语.md "wikilink")，并皈依伊斯兰教。其中从埃塞俄比亚来的摩尔人成了最有影响的一支。在伊斯兰教在非洲传播的过程中，多种文化的族群融入其中。

711年，摩尔人入侵[基督教的](../Page/基督教.md "wikilink")[伊比利亚半岛](../Page/伊比利亚半岛.md "wikilink")（今天的[西班牙和](../Page/西班牙.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")）。一个非洲[柏柏尔人将军](../Page/柏柏尔人.md "wikilink")[塔里克·伊本·齐亚德率领](../Page/塔里克·伊本·齐亚德.md "wikilink")6500名北非柏柏尔人和500名阿拉伯人北渡[直布罗陀海峡在伊比利亚半岛登陆](../Page/直布罗陀海峡.md "wikilink")。登陆后他立刻焚烧战船，以示破釜沉舟、背水一战的决心。经过八年的征战，摩尔人征服了南部大半个西班牙。他们试图向东北进军，跨越[比利牛斯山](../Page/比利牛斯山.md "wikilink")，但732年在[图尔战役中被](../Page/图尔战役.md "wikilink")[法兰克人击败](../Page/法兰克人.md "wikilink")。数十年中摩尔人统治了北非以及西班牙除了西北部和比利牛斯山区的[巴斯克地区](../Page/巴斯克.md "wikilink")。摩尔人内部从750年代开始兄弟阋墙。

[Map_Iberian_Peninsula_1037-es.svg](https://zh.wikipedia.org/wiki/File:Map_Iberian_Peninsula_1037-es.svg "fig:Map_Iberian_Peninsula_1037-es.svg")
这个国家后来分裂成几个伊斯兰[泰法](../Page/泰法.md "wikilink")，这些泰法名义上臣服于[科尔多瓦](../Page/科爾多瓦_\(西班牙\).md "wikilink")[哈里发](../Page/哈里发.md "wikilink")。位于北部和西部的基督教王国则在伊比利亚半岛上逐渐扩张势力。[加利西亚](../Page/加利西亚自治区.md "wikilink")、[莱昂王国](../Page/莱昂王国.md "wikilink")、[纳瓦拉](../Page/纳瓦拉.md "wikilink")、[阿拉贡](../Page/阿拉贡自治区.md "wikilink")、[加泰罗尼亚和](../Page/加泰罗尼亚.md "wikilink")[卡斯提爾在之后的几个世纪中逐渐扩张](../Page/卡斯蒂利亚.md "wikilink")。这一时期[基督徒](../Page/基督徒.md "wikilink")、[穆斯林和](../Page/穆斯林.md "wikilink")[犹太人得以和睦相处](../Page/犹太人.md "wikilink")。1031年科尔多瓦哈里发垮台，在西班牙的伊斯兰领土被北非的[穆拉比特王朝统治](../Page/穆拉比特王朝.md "wikilink")。

[Spain_Andalusia_Granada_BW_2015-10-25_15-39-55.jpg](https://zh.wikipedia.org/wiki/File:Spain_Andalusia_Granada_BW_2015-10-25_15-39-55.jpg "fig:Spain_Andalusia_Granada_BW_2015-10-25_15-39-55.jpg")
1212年，基督教各王国联盟在卡斯蒂利亚国王[阿方索八世的带领下将穆斯林赶出西班牙中部](../Page/阿方索八世_\(卡斯蒂利亚\).md "wikilink")。但在[格拉纳达的摩尔人王国在此之后仍在伊比利亚南部](../Page/格拉纳达.md "wikilink")[安達魯西亞得以保持了三个多世纪的繁荣](../Page/安達魯西亞.md "wikilink")。这个王国以像[阿尔罕布拉宫这样的美轮美奂的建筑而声名远播](../Page/阿尔罕布拉宫.md "wikilink")。1492年1月2日，这个在格拉那达的最后一个穆斯林堡垒臣服于新近统一的基督教西班牙王国。穆斯林们被迫在离开西班牙和皈依基督教中选择。这些穆斯林的后代被称为“[摩里斯科人](../Page/摩里斯科人.md "wikilink")”，受到歧视。他们在如[阿拉贡](../Page/阿拉贡自治区.md "wikilink")、[巴伦西亚或安達魯西亞这样的地区大部分从事农业](../Page/巴伦西亚.md "wikilink")。在1609年到1614年间他们被有组织地迫害，当时全部800万西班牙人中有30万被波及。

与此同时，征服伊斯兰的浪潮不光西进到了西班牙，也东征亚洲：经由[印度](../Page/印度.md "wikilink")、[马来半岛和](../Page/马来半岛.md "wikilink")[印度尼西亚](../Page/印度尼西亚.md "wikilink")，直到[菲律宾的](../Page/菲律宾.md "wikilink")[棉兰老岛](../Page/棉兰老岛.md "wikilink")。1521年，[麦哲伦的船队经](../Page/麦哲伦.md "wikilink")[新大陆到达菲律宾群岛](../Page/新大陆.md "wikilink")，将当地的土著称为摩尔人（后来的[摩洛人](../Page/摩洛人.md "wikilink")）。

## 近代摩尔人

“摩尔人”是十六世纪欧洲人指以为母语的人。他们主要生活在[西撒哈拉地区和](../Page/西撒哈拉.md "wikilink")[毛里塔尼亚](../Page/毛里塔尼亚.md "wikilink")（[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[马里](../Page/马里.md "wikilink")、[尼日尔](../Page/尼日尔.md "wikilink")、[突尼西亚一部分](../Page/突尼西亚.md "wikilink")）。毛里塔尼亚因此而得名。他们也是毛里塔尼亚主要居民。

从历史的角度，欧洲的学者通常把摩尔人分为非洲摩尔人（[哈拉廷人](../Page/哈拉廷人.md "wikilink")）和欧洲—阿拉伯摩尔人两类。[斯里兰卡摩尔人追溯他们的祖先为阿拉伯的摩尔人](../Page/斯里兰卡摩尔人.md "wikilink")。在毛里塔尼亚欧洲—阿拉伯摩尔人地位较高（欧洲—阿拉伯摩尔人多数是貴族與自由民，黑摩尔人多数是奴隶）。

在現代伊比利半島，摩尔人指阿拉伯人与北非穆斯林（特別是摩洛哥人），在葡萄牙，北方居民也稱南方葡萄牙人為摩爾。

在荷蘭殖民斯里蘭卡時代，將[孟加拉族出身的穆斯林稱為穆爾人](../Page/孟加拉族.md "wikilink")（在葡萄牙殖民印度時，果阿穆斯林也稱穆爾人）。

## 参见

  - [伊斯兰建筑](../Page/伊斯兰建筑.md "wikilink")
  - [奥赛罗](../Page/奥赛罗.md "wikilink")
  - [毛里塔尼亚](../Page/毛里塔尼亚.md "wikilink")
  - [西撒哈拉](../Page/西撒哈拉.md "wikilink")
  - [西班牙伊斯蘭教](../Page/西班牙伊斯蘭教.md "wikilink")
  - [柏柏尔人](../Page/柏柏尔人.md "wikilink")

## 参考文献

[摩爾人](../Category/摩爾人.md "wikilink")
[Category:北非民族](../Category/北非民族.md "wikilink")
[Category:西班牙歷史](../Category/西班牙歷史.md "wikilink")
[Category:葡萄牙歷史](../Category/葡萄牙歷史.md "wikilink")
[Category:北非历史](../Category/北非历史.md "wikilink")
[Category:伊斯蘭教歷史](../Category/伊斯蘭教歷史.md "wikilink")
[Category:地中海歷史](../Category/地中海歷史.md "wikilink")

1.
2.
3.
4.  Pieris, P.E. *Ceylon and the Hollanders 1658-1796*. American Ceylon
    Mission Press, Tellippalai Ceylon 1918