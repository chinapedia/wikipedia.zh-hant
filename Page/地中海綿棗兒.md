**地中海綿棗兒**（[学名](../Page/学名.md "wikilink")：**）又名**地中海蓝钟花**、**地金球**、**野風信子**、**葡萄牙綿棗兒**，是[綿棗兒屬的一種植物](../Page/綿棗兒屬.md "wikilink")，原產於西[地中海地區的](../Page/地中海.md "wikilink")[伊比利亞半島](../Page/伊比利亞半島.md "wikilink")、[義大利及](../Page/義大利.md "wikilink")[非洲西北部](../Page/非洲.md "wikilink")。也有人根據[學名](../Page/學名.md "wikilink")*peruviana*（[秘魯的](../Page/秘魯.md "wikilink")），將本種的名稱翻譯為**秘魯綿棗兒**，不過秘魯並不是本種的原產地，這裡所說的秘魯和[南美洲國家秘魯並沒有關係](../Page/南美洲.md "wikilink")，而是指一艘名為秘魯（*Peru*）的船舶，1753年[林奈在描述本種性狀時所用的](../Page/林奈.md "wikilink")[標本](../Page/標本.md "wikilink")，就是由這艘船從[西班牙運來的](../Page/西班牙.md "wikilink")。

[多年生](../Page/多年生植物.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")，地下莖為鱗皮[鱗莖](../Page/鱗莖.md "wikilink")。鱗莖直徑6-8[公分](../Page/公分.md "wikilink")，鱗莖白色，外表包覆棕色的鱗皮。[葉線形](../Page/葉.md "wikilink")，長20-60公分，寬1-4公分，葉由鱗莖長出，在一個春季期間，每個鱗莖約可以長出5-15片葉子。[花序為總狀花序](../Page/花序.md "wikilink")，花莖長15-40公分，每一花莖上有40-100朵[花](../Page/花.md "wikilink")。花藍色，花徑1-2公分，有花被六片。

## 栽培與用途

葡萄牙綿棗兒在春季時開花，通常栽培做為春天時的觀賞花卉，在市面上可以買到一些不同花色的品種，顏色由白色至淡藍色、深藍色或紫色。

## 參考文獻

[Category:绵枣儿属](../Category/绵枣儿属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")