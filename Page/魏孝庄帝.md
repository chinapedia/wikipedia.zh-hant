**魏孝莊帝元子攸**（），字**彦达**，[河南郡](../Page/河南郡.md "wikilink")[洛阳县](../Page/洛阳县.md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[洛阳市东](../Page/洛阳市.md "wikilink")）人，[魏献文帝拓跋弘之孙](../Page/魏献文帝.md "wikilink")，[彭城王](../Page/彭城王.md "wikilink")[元勰第三子](../Page/元勰.md "wikilink")，母親為王妃[李媛華](../Page/李媛華.md "wikilink")。孝莊帝是[尔朱荣拥立的](../Page/尔朱荣.md "wikilink")[傀儡皇帝](../Page/傀儡皇帝.md "wikilink")，最终被[尔朱兆绞杀](../Page/尔朱兆.md "wikilink")。

## 生平

元子攸姿貌很俊美，有勇力。自幼在宫为[孝明帝元诩担任伴读](../Page/元诩.md "wikilink")，与魏孝明帝颇为友爱，官至中书侍郎，封武城县开国公，527年，被特封**长乐王**。

### 继位

528年5月15日（農曆四月十一日、建義元年），元子攸被[尔朱荣拥立为皇帝](../Page/尔朱荣.md "wikilink")。

### 刺杀尔朱荣

孝莊帝[永安二年](../Page/永安_\(北魏\).md "wikilink")（529年）鑄永安[五銖錢](../Page/五铢.md "wikilink")。九月二十五日（530年11月1日、永安三年），孝莊帝伏兵明光殿，聲稱皇后[大尔朱氏生下了太子](../Page/大尔朱氏.md "wikilink")，派[元徽向爾朱榮報喜](../Page/元徽.md "wikilink")。爾朱榮跟[元天穆一起入朝](../Page/元天穆.md "wikilink")。元子攸听说尔朱荣进宫臉色緊張，連忙喝酒以遮掩。尔朱荣见到光祿少卿[魯安](../Page/魯安.md "wikilink")、典御[李侃晞從東廂門执刀闖入](../Page/李侃晞.md "wikilink")，便撲向元子攸。元子攸用藏在膝下的刀砍到尔朱荣，魯安等揮刀亂砍，殺爾朱榮與元天穆等人。

### 囚禁

十月三十日（530年12月5日、永安三年），[爾朱兆另立](../Page/爾朱兆.md "wikilink")[元曄為帝](../Page/元曄.md "wikilink")。十二月三日（531年1月6日），爾朱兆攻入洛陽，殺死孝莊帝在襁褓中的儿子，孝莊帝被俘囚於[永寧寺](../Page/永宁寺_\(洛阳\).md "wikilink")、後解送囚於[晉陽](../Page/太原市.md "wikilink")[三級寺](../Page/三級寺.md "wikilink")。

### 被杀

永安三年十二月甲子（531年1月26日），魏孝莊帝於[晋阳城](../Page/晋阳城.md "wikilink")（今[太原市](../Page/太原市.md "wikilink")[晋源区境](../Page/晋源区.md "wikilink")）三級寺被尔朱兆絞殺，虚岁二十四\[1\]。臨終前孝莊帝向佛祖禮拜，發願生生世世不做皇帝，並賦詩明志：「權去生道促，憂來死路長。懷恨出國門，含悲入鬼鄉。隧門一時閉，幽庭豈復光。思鳥吟青松，哀風吹白楊。昔來聞死苦，何言身自當。」[中兴二年](../Page/中兴_\(北魏\).md "wikilink")（532年），[元朗给元子攸上谥号为](../Page/元朗_\(安定王\).md "wikilink")**武怀皇帝**，[魏孝武帝元修即位后](../Page/魏孝武帝.md "wikilink")，因为武怀谥号犯魏孝武帝父亲[元怀名讳](../Page/元怀.md "wikilink")，于[太昌元年](../Page/太昌.md "wikilink")（532年）改元子攸谥号为**孝庄皇帝**，庙号**敬宗**。十一月，葬于静陵。

## 皇后

  - [大尔朱氏](../Page/大尔朱氏.md "wikilink")

## 參考資料

## 參見

  - [洛陽伽藍記](../Page/洛陽伽藍記.md "wikilink")

## 外部連結

  -
[Category:北魏宗室](../Category/北魏宗室.md "wikilink")
[Category:河南元氏](../Category/河南元氏.md "wikilink")
[Category:北魏县公](../Category/北魏县公.md "wikilink")
[Category:北魏郡王](../Category/北魏郡王.md "wikilink")
[Category:北魏皇帝](../Category/北魏皇帝.md "wikilink")
[Category:北朝被杀害皇帝](../Category/北朝被杀害皇帝.md "wikilink")
[Category:谥孝庄](../Category/谥孝庄.md "wikilink")

1.  《[資治通鑒](../Page/資治通鑒.md "wikilink")》-{卷}-一百五十四