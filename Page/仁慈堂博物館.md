**仁慈堂博物館**（）是位於[澳門](../Page/澳門.md "wikilink")[議事亭前地仁慈堂右巷二號](../Page/議事亭前地.md "wikilink")[仁慈堂大樓二樓的一所以](../Page/仁慈堂大樓.md "wikilink")[天主教為主題的](../Page/天主教.md "wikilink")[博物館](../Page/博物館.md "wikilink")，由澳門慈善機構[仁慈堂值理會管轄](../Page/仁慈堂.md "wikilink")。

## 歷史

[Santa_Casa_da_Misericórdia_de_Macau.JPG](https://zh.wikipedia.org/wiki/File:Santa_Casa_da_Misericórdia_de_Macau.JPG "fig:Santa_Casa_da_Misericórdia_de_Macau.JPG")
於1569年成立的[仁慈堂為了讓居民了解仁慈堂的宗旨和歷史以及其發揚助人仗義的仁愛精神](../Page/仁慈堂.md "wikilink")\[1\]\[2\]，因而耗資約200萬[澳門幣在建於](../Page/澳門幣.md "wikilink")18世紀中葉的[仁慈堂大樓進行裝修及改善保安系統以開設](../Page/仁慈堂大樓.md "wikilink")**仁慈堂博物館**\[3\]\[4\]；博物館於2001年12月14日由[澳門行政長官](../Page/澳門行政長官.md "wikilink")[何厚鏵](../Page/何厚鏵.md "wikilink")、[天主教澳門教區主教](../Page/天主教澳門教區主教.md "wikilink")[林家駿](../Page/林家駿.md "wikilink")、仁慈堂值理會主席[飛安達等主持揭幕](../Page/飛安達.md "wikilink")\[5\]；此博物館是繼[天主教藝術博物館與墓室及](../Page/天主教藝術博物館與墓室.md "wikilink")[聖物寶庫後](../Page/聖物寶庫.md "wikilink")，澳門第三所以及至今唯一一所非公營機構開設以宗教為題材的博物館。

為了展出更多藏品以及提升博物館質量，仁慈堂博物館於2011年上半年決定擴充博物館\[6\]。澳門行政長官[崔世安](../Page/崔世安.md "wikilink")、[社會文化司司長](../Page/社會文化司司長.md "wikilink")[張裕等於](../Page/張裕.md "wikilink")2011年12月6日為仁慈堂博物館新展廳主持落成揭幕禮，為該博物館增添展出超過120件藏品\[7\]。

## 設施

仁慈堂博物館位於[仁慈堂大樓二樓](../Page/仁慈堂大樓.md "wikilink")，博物館設有與[天主教](../Page/天主教.md "wikilink")、[耶穌會以及](../Page/耶穌會.md "wikilink")[仁慈堂相關的的瓷器](../Page/仁慈堂.md "wikilink")、書籍文獻等合共2,000多件收藏品\[8\]；展品當中展有於1627年1月草擬通過並於1662年手抄而成的《澳門仁慈堂章程》正本、繪於18世紀的身兼首任[天主教澳門教區主教的仁慈堂創始人賈耐勞主教全身油畫](../Page/天主教澳門教區主教列表.md "wikilink")、[頭顱遺骨和其陪葬的](../Page/頭顱.md "wikilink")[十字架](../Page/十字架.md "wikilink")、[聖辣非醫院內的銅鐘外](../Page/聖辣非醫院.md "wikilink")，也有展出大部分由[土生葡人飛安達及](../Page/土生葡人.md "wikilink")[馬若龍捐贈從](../Page/馬若龍.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")、[日本及](../Page/日本.md "wikilink")[中國大陸等地搜購天主教傳教士過去在](../Page/中國大陸.md "wikilink")[中國](../Page/中國.md "wikilink")[江西](../Page/江西.md "wikilink")[景德鎮製造到的天主教東方禮的祭獻儀式所用的器具以及刻有耶穌會徽號的](../Page/景德鎮.md "wikilink")[陶瓷器皿等藏品](../Page/陶瓷.md "wikilink")\[9\]\[10\]。

於2011年年底開設新展廳佔地800[平方呎](../Page/平方呎.md "wikilink")\[11\]；其展品大多突顯天主教在東西文化影響而製成的瓷器、象牙、木雕、銀器物品，也展有104.5厘米高於[同治年間燒製瓷器粉彩的](../Page/同治.md "wikilink")[耶穌聖心雕像](../Page/耶穌聖心.md "wikilink")、直徑達38厘米也於同治年間燒製的[耶穌受洗青花碟以及於](../Page/耶穌受洗.md "wikilink")1936年以[中](../Page/中文.md "wikilink")、[法文繪製的中華天主教教區全圖等展藏品](../Page/法文.md "wikilink")\[12\]。

## 参见

  - [澳門博物館列表](../Page/澳門博物館列表.md "wikilink")

## 資料來源

<div class="references-small">

<references>

</references>

</div>

[Category:澳門博物館](../Category/澳門博物館.md "wikilink")
[Category:天主教澳门教区](../Category/天主教澳门教区.md "wikilink")
[Category:2001年完工建築物](../Category/2001年完工建築物.md "wikilink")

1.

2.

3.
4.
5.

6.

7.

8.

9.
10.

11.
12.