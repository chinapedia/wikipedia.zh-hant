**顧邵**（），字**孝則**，三国时[吳郡吳县](../Page/吳郡.md "wikilink")（今苏州）人。[顧雍长子](../Page/顧雍.md "wikilink")，[顾承](../Page/顾承.md "wikilink")、[顧譚之父](../Page/顧譚.md "wikilink")。

## 生平

少年时与舅父[陆绩齐名](../Page/陆绩.md "wikilink")，胜过[陆逊](../Page/陆逊.md "wikilink")、[张敦](../Page/张敦.md "wikilink")、[卜静等人](../Page/卜静.md "wikilink")。在周边远近闻名。[孙权许以](../Page/孙权.md "wikilink")[孙策之長女為妻](../Page/孙策.md "wikilink")。[周瑜病死之后去弔喪时](../Page/周瑜.md "wikilink")，与全琮陆绩一起拜访庞统，被[庞统评价为](../Page/庞统.md "wikilink")“驽牛能负重致远”又说：“马虽然走得快但只能背负一人，牛能背负的，难道仅仅一个人吗？”

同一年，二十七岁的时候出仕，接替孫邻拜为[豫章太守](../Page/豫章.md "wikilink")。顧劭有知人之能，提拔的平民[张秉](../Page/张秉.md "wikilink")、[殷礼后来都当了太守](../Page/殷礼.md "wikilink")，[吾粲升到太子少傅](../Page/吾粲.md "wikilink")，而[丁谞官至](../Page/丁谞.md "wikilink")[典军中郎](../Page/典军中郎.md "wikilink")。[豫章太守在任的第五年逝世](../Page/豫章.md "wikilink")，享年三十一歲。

## 家親

### 父親

  - [顧雍](../Page/顧雍.md "wikilink")，字-{元}-歎，三國時東吳第二任丞相，逝世後謚號肅侯。

### 從父

  - [顧徽](../Page/顧徽.md "wikilink")，字子歎，父親同胞弟。孫權統事，召署主簿轉東曹掾，拜輔義都尉。遙領巴東太守。
  - [顧悌](../Page/顧悌.md "wikilink")，字子通，父親同宗族人。少聰敏，以孝廉聞名。官拜郎中，領偏將軍。孫權將歿時，孫權對立嗣舉棋不定，曾向朱據陳述吳國立嗣問題的利弊。

### 從兄弟

  - [顧濟](../Page/顧濟.md "wikilink")，騎都尉，嗣侯。
  - [顧裕](../Page/顧裕.md "wikilink")，字季則，顧徽之子，顧雍之姪，少知名，位至鎮東將軍。

### 妻

  - 陆氏，[陆逊姐妹](../Page/陆逊.md "wikilink")，生顾谭、顾承。
  - 孫氏，继室，孫策長女。

### 兒子

  - [顧譚](../Page/顧譚.md "wikilink")，字子默，長子，太子四友之一，曾任太常。後被[全琮誣陷而流放到交州](../Page/全琮.md "wikilink")。
  - [顧承](../Page/顧承.md "wikilink")，字子直，次子，官至侍中。與顧譚一樣被[全琮誣陷而流放到交州](../Page/全琮.md "wikilink")。

## 評價

  - [龐統](../Page/龐統.md "wikilink")：「而顧邵則有資質駑鈍的牛能負重走遠路的能力。」

## 参考

  - 《[三國志](../Page/三國志.md "wikilink")·吳書七·張顧諸葛步傳》
  - 《三国志·蜀书·庞统传》

[G](../Category/东吴政治人物.md "wikilink")
[S](../Category/吳郡顧氏.md "wikilink")
[G](../Category/東吳太守.md "wikilink")