**jtL**（[韓語](../Page/韓語.md "wikilink")：**제이티엘**）是[韩国前人氣偶像組合](../Page/韩国.md "wikilink")。[H.O.T.解散后](../Page/H.O.T..md "wikilink")，成员[张佑赫](../Page/张佑赫.md "wikilink")、[安胜浩和](../Page/安胜浩.md "wikilink")[李在元组成了新團體jtL](../Page/李載沅.md "wikilink")\[1\]，以三人英文名字開頭的字母命名。2001年12月20日，jtL的首張專輯《龍爭虎鬥》（Enter
The
Dragon）發行，第一週銷量已經有48萬張，此時卻消失於大眾眼前，他們的音樂錄影帶、音樂放送和節目錄製等宣傳活動全部都沒有曝光或無故取消，連頒獎典禮都沒有出席，受歡迎的歌曲也在排行榜上出現明顯疑點。傳聞直指媒體對jtL的封殺行動是三人的前東家[SM娛樂聯合電視台所為](../Page/SM娛樂.md "wikilink")，不過SM娛樂和電視台都予以全盤否認\[2\]\[3\]。2002年2月17日，jtL挑戰《[萬人召集演唱會](../Page/MBC_游擊演唱會.md "wikilink")》，成為首次亮相的機會，最終成功號召了12,133人\[4\]。

他們的著名歌曲〈A Better
Day〉曾獲得[SBS人氣歌謠冠軍](../Page/SBS人氣歌謠.md "wikilink")，被[香港歌手](../Page/香港.md "wikilink")[劉德華翻唱為](../Page/劉德華.md "wikilink")〈黑蝙蝠中隊〉。2003年獲得[MTV亞洲大獎](../Page/MTV亞洲大獎.md "wikilink")「韓國最受歡迎歌手」獎項。2004年7月，jtL解散。

## 成員列表

  - [张佑赫](../Page/张佑赫.md "wikilink")
  - [Tony An](../Page/安胜浩.md "wikilink")
  - [李在元](../Page/李載沅.md "wikilink")

## jtL歷史

### 2001年

  - 於12月發行第一張專輯《**Enter The Dragon**》

<!-- end list -->

1.  A Better Day
2.  叛逆少年隊
3.  Just Say Goodbye
4.  Enter The Dragon
5.  Mistake
6.  Really Want U
7.  Hard For Me
8.  From Fan
9.  My Lecon
10. Bow Wow
11. A Better Day(Instrumental Ver.)
12. A Better Day(Chinese Ver.)

### 2002年

  - －受邀參與第四屆[CCTV](../Page/CCTV.md "wikilink") & MTV中國 音樂大賞獲得「亞洲最pop團體」獎
  - 第1.5張專輯《**Love Story**》

<!-- end list -->

1.  幸福過的記憶
2.  Just Say Goodbye
3.  Santa Baby
4.  Bow Wow
5.  Mery Christmas
6.  From. FAN
7.  祈禱
8.  Mistake
9.  一段時間
10. Enter The Dragon
11. A Better Day
12. Santa Baby

### 2003年

  - 1月24日2003「Asia Awards」獲得韓國最高人氣藝人獎
  - 8月14日發行jtL第二張專輯《**Run Away**》

<!-- end list -->

1.  One Night Lover
2.  My Life Blues
3.  Without You Love
4.  奇蹟（One Of Kind）
5.  少年啊\!鼓起勇氣吧\!(Up Front)
6.  My Lady
7.  不能用言語傳達
8.  還是別離
9.  My Lady
10. C.I.D
11. 幻生（祈禱 2nd Story）

<!-- end list -->

  - 演唱会Live CD：《 **2003 jtL concert live**》

## 參考資料

## 外部链接

  - [H.O.T 歷史 History](../Page/:H.O.T..md "wikilink")
  - [H.O.T官方网站](https://web.archive.org/web/20070212041557/http://www.smtown.com/smtown/hot/index.html)
  - [jtL官方网站](http://www.yjmusic.co.kr/jtl/intro.html)
  - [李在元的Instagram帳戶](https://www.instagram.com/jw_jayone1/)

[Category:韓國男子演唱團體](../Category/韓國男子演唱團體.md "wikilink")
[Category:已解散的男子演唱團體](../Category/已解散的男子演唱團體.md "wikilink")
[Category:2004年解散的音樂團體](../Category/2004年解散的音樂團體.md "wikilink")
[Category:MTV亞洲大獎獲得者](../Category/MTV亞洲大獎獲得者.md "wikilink")

1.
2.
3.
4.