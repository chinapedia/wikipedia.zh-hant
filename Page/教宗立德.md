教宗[聖](../Page/聖人.md "wikilink")**立德**（，）於96年或99年―108年出任教宗。他在任內最重要的貢獻，是確立了[樞機團的組織](../Page/樞機團.md "wikilink")，而這個組織日後亦演變成推舉教宗的機構。他的瞻禮日在10月27日。

现代对教宗立德所知甚少，根据《教宗实录》所载，他出生于一个居住在[伯利恒](../Page/伯利恒.md "wikilink")（白冷）的希腊犹太人家庭，在多米仙（图密善）皇帝在位期间，他继承克肋孟一世，当选为罗马主教，继承时值第二次大迫害时期。根据《教宗实录》记载，他将罗马分为若干个领衔堂区，每一个堂区指派一位神父负责。同时，他在罗马城任命7名执事。

据优西比乌在《教会史》中记载，教宗立德在图拉真12年，也就是他当选教宗的第八年去世。他通常被认为是殉道者，然而并没有切实证据。他可能被安葬在梵蒂冈圣伯多禄墓附近。此外据信，圣若望宗徒可能是在教宗立德的任期内离世的。

## 譯名列表

  - 立德：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作立德。

[Category:教宗](../Category/教宗.md "wikilink")
[Category:亞洲出生的教宗](../Category/亞洲出生的教宗.md "wikilink")
[Category:犹太基督徒](../Category/犹太基督徒.md "wikilink")
[Category:基督教聖人](../Category/基督教聖人.md "wikilink")
[Category:西亚犹太人](../Category/西亚犹太人.md "wikilink")
[Category:希腊裔](../Category/希腊裔.md "wikilink")
[Category:伯利恒人](../Category/伯利恒人.md "wikilink")