**康悌凡尼号邮船自沉事件**發生於1941年12月、[太平洋战争爆发不久後的上海](../Page/太平洋战争.md "wikilink")[黄浦江](../Page/黄浦江.md "wikilink")，為江上的[意大利籍船员不欲船隻为](../Page/意大利.md "wikilink")[日军征用](../Page/日军.md "wikilink")，自行凿沉的事件。

## 事件经过

### 自沉黄浦江

康悌凡尼号是一艘2万吨级的意大利籍巨型邮船，定期航行于欧亚之间，1940年为躲避[公海上的英军潜艇的袭击](../Page/公海.md "wikilink")，长期停泊在[爱多亚路外滩的黄浦江中](../Page/爱多亚路.md "wikilink")。1941年12月，太平洋战争爆发，日军打算征用该船作为运输舰使用，被意大利船长拒绝；于是日本方面转而和意大利政府联系，意大利船长得知消息后，命令船员自行凿沉该船。

### 打捞修复又被击沉

由于该船体型过于庞大，高度甚至超过黄浦江的深度，而且设有双层隔板防沉装置，因此并未沉入江底，而是倾斜横倒在江面上，阻塞了黄浦江的交通。为了恢复黄浦江的畅通，当时曾经特制一批特粗的铁链，一头系在外滩1号的[亚细亚大楼上](../Page/亚细亚大楼_\(外滩\).md "wikilink")，一头系在康悌凡尼号上，终于将其扶正。只是横在外滩马路上的铁链又造成陆上交通严重阻塞。日军修复该船后，准备投入使用，但刚驶出黄浦江不久，就被公海中的盟军潜艇击中，葬身海底。

## 后续船员情况

邮船自沉后船员登陆自寻出路。以船上餐厅和厨房人员为主体的登陆人员被比利时商人克莱门的侄女招募到[上海法租界内的](../Page/上海法租界.md "wikilink")[克莱门公寓](../Page/克莱门公寓.md "wikilink")，开设了一家名为森内饭店（Sunny
Restaurant）的[意大利菜餐厅](../Page/意大利菜.md "wikilink")。\[1\]1949年，中华人民共和国成立后，船员陆续离开中国。

## 参考资料

[Category:中华民国日占时期历史](../Category/中华民国日占时期历史.md "wikilink")
[Category:1941年上海](../Category/1941年上海.md "wikilink")
[Category:1940年代中国交通事故](../Category/1940年代中国交通事故.md "wikilink")
[Category:上海民国时期政治事件](../Category/上海民国时期政治事件.md "wikilink")
[Category:上海交通事故](../Category/上海交通事故.md "wikilink")
[Category:意大利王国外交事件](../Category/意大利王国外交事件.md "wikilink")
[Category:1940年代日本外交事件](../Category/1940年代日本外交事件.md "wikilink")
[Category:日意关系](../Category/日意关系.md "wikilink")
[Category:自沉船](../Category/自沉船.md "wikilink")
[Category:1941年12月](../Category/1941年12月.md "wikilink")

1.