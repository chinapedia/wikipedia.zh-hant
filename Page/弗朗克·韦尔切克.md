[FrankWilczek_cropped.png](https://zh.wikipedia.org/wiki/File:FrankWilczek_cropped.png "fig:FrankWilczek_cropped.png")
**弗兰克·安东尼·维尔切克**（，），美国[理论物理学家](../Page/理论物理.md "wikilink")，現任[麻省理工学院物理系教授](../Page/麻省理工学院.md "wikilink")。[西屋科学奖获得者](../Page/西屋科学奖.md "wikilink")，大学毕业于[芝加哥大学](../Page/芝加哥大学.md "wikilink")。在[普林斯顿大学读博士期间](../Page/普林斯顿大学.md "wikilink")，他和他的导师[戴维·格娄斯发现了](../Page/戴维·格娄斯.md "wikilink")[量子色动力学中的](../Page/量子色动力学.md "wikilink")[渐近自由](../Page/渐近自由.md "wikilink")，他们因此获得了2004年诺贝尔物理学奖。他在[粒子物理学和](../Page/粒子物理学.md "wikilink")[凝聚体物理学都有所建树](../Page/凝聚体物理学.md "wikilink")。

## 生平

[FrankStockholm2004.jpg](https://zh.wikipedia.org/wiki/File:FrankStockholm2004.jpg "fig:FrankStockholm2004.jpg")

维尔切克出生于[纽约州](../Page/纽约州.md "wikilink")[米里奥拉](../Page/米里奥拉_\(纽约州\).md "wikilink")，他的祖先来自波兰和意大利。他在[昆斯區上中小学](../Page/昆斯區.md "wikilink")。1970年他在芝加哥大学获得[数学学士学位](../Page/数学.md "wikilink")。1972年他在普林斯顿大学获得数学硕士学位，1974年物理学博士学位。他在[麻省理工学院理论物理学中心任教授](../Page/麻省理工学院.md "wikilink")。此前他曾经在[普林斯顿高等研究院和](../Page/普林斯顿高等研究院.md "wikilink")[凯维里理论物理研究所工作过](../Page/凯维里理论物理研究所.md "wikilink")。

1973年7月3日维尔切克结婚，两人有两个孩子。

## 研究

1973年维尔切克在普林斯顿大学作为戴维·格娄斯的博士生时发现了渐进自由，即[夸克之间的距离越近](../Page/夸克.md "wikilink")，其[强相互作用越弱](../Page/强相互作用.md "wikilink")，假如两个夸克之间的距离极其近的话，那么它们之间的核力是如此之弱，以至于它们可以被看作是自由粒子。这个理论（[休·波利策与维尔切克独立地发现了这个理论](../Page/休·波利策.md "wikilink")）对于[量子色动力学的发展是极其重要的](../Page/量子色动力学.md "wikilink")。

维尔切克参与了[轴子](../Page/轴子.md "wikilink")、[任意子](../Page/任意子.md "wikilink")、渐进自由、[夸克物质的](../Page/夸克物质.md "wikilink")[色超导性和](../Page/色超导性.md "wikilink")[量子场论的其它理论的发展](../Page/量子场论.md "wikilink")。他的工作范围异常广泛，包括了[凝聚体物理学](../Page/凝聚体物理学.md "wikilink")、[天体物理学和](../Page/天体物理学.md "wikilink")[粒子物理学](../Page/粒子物理学.md "wikilink")。

他目前的研究内容包括：

  - “纯”粒子物理学：理论与可观察的现象之间的联系
  - 物质的特性：极高温度、密度和相态结构
  - 粒子物理学在[宇宙学中的应用](../Page/宇宙学.md "wikilink")
  - 场论技术在凝聚体理论中的应用
  - [黑洞的量子场论](../Page/黑洞.md "wikilink")。

## 參考文獻

## 著作

  - 2008\. *The Lightness of Being: Mass, Ether, and the Unification of
    Forces*. [Basic Books](../Page/Basic_Books.md "wikilink"). ISBN
    978-0-465-00321-1.
  - 2007\. *La musica del vuoto*. Roma: Di Renzo Editore.
  - 2006\. *Fantastic Realities: 49 Mind Journeys And a Trip to
    Stockholm*. World Scientific. ISBN 978-981-256-655-3.
  - 2002, "On the world's numerical recipe (an ode to physics),"
    *[Daedalus](../Page/Daedalus.md "wikilink")* 131(1): 142-47.
  - 1989 (with [Betsy Devine](../Page/Betsy_Devine.md "wikilink")).
    *Longing for the Harmonies: Themes and Variations from Modern
    Physics*. W W Norton. ISBN 978-0-393-30596-8.
  - 1988\. *Geometric Phases in Physics*.
  - 1990\. *Fractional Statistics and Anyon Superconductivity*.

<div class="references-small">

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -

</div>

## 外部链接

  - [2004年诺贝尔物理奖介紹─渐近自由](http://psroc.phys.ntu.edu.tw/bimonth/download.php?d=1&cpid=142&did=3)
  - [Biography and Bibliographic
    Resources](http://www.osti.gov/accomplishments/wilczek.html), from
    the [Office of Scientific and Technical
    Information](../Page/Office_of_Scientific_and_Technical_Information.md "wikilink"),
    [United States Department of
    Energy](../Page/United_States_Department_of_Energy.md "wikilink")
  - [Longer biography at Lifeboat Foundation
    website](https://web.archive.org/web/20060304085255/http://lifeboat.com/ex/bios.frank.wilczek)
  - [Frank Wilczek explains Einstein's massive contributions to
    science](https://web.archive.org/web/20100325141056/http://simplycharly.com/einstein/frank_wilczek_interview.htm)
  - [Papers in
    ArXiv](http://arxiv.org/find/all/1/all:+AND+Frank+Wilczek/0/1/0/all/0/1)
  - [Frank Wilczek discusses his book "The Lightness of Being" on
    the 7th Avenue Project Radio
    Show](http://7thavenueproject.com/post/479973045/frank-wilczek-incredible-lightness-of-being)
  - [The World's Numerical
    Recipe](https://web.archive.org/web/20030824073148/http://mitworld.mit.edu/video/78/)
  - [Scientific
    articles](http://www.slac.stanford.edu/spires/find/hep/www?rawcmd=find+a+wilczek%2Cf&FORMAT=WWW&SEQUENCE=)
    by Wilczek in the SLAC database
  - [Wilczek on anyons and
    superconductivity](https://web.archive.org/web/20050216093804/http://www.sciencewatch.com/interviews/frank_wilczek1.htm)
  - [Blog of the Wilczek family's Nobel
    adventures](http://betsydevine.com/blog/category/nobel/)
  - [Freeman Dyson](../Page/Freeman_Dyson.md "wikilink"), "[Leaping into
    the Grand Unknown: Review of *The Lightness of
    Being*,](http://www.nybooks.com/articles/22575)" *[The New York
    Review of
    Books](../Page/The_New_York_Review_of_Books.md "wikilink")* 56(6),
    April 9, 2009.
  - [ForaTV: The Large Hadron Collider and Unified Field
    Theory](https://web.archive.org/web/20121021004342/http://fora.tv/2008/09/25/Frank_Wilczek_The_LHC_and_Unified_Field_Theory)
  - [A radio interview with Frank
    Wilczeck](http://lewisfrumkes.com/radioshow/frank-wilczeck-interview)
    Aired on the [Lewis Burke
    Frumkes](../Page/Lewis_Burke_Frumkes.md "wikilink") Radio Show the
    10th of April 2011.
  - [A television interview with Frank
    Wilczek](http://www.youtube.com/watch?v=jqEGpvu_Bgo) from February
    2011 for Cambridge University Television

[W](../Page/category:美国物理学家.md "wikilink")

[W](../Category/美國諾貝爾獎獲得者.md "wikilink")
[W](../Category/普林斯顿大学校友.md "wikilink")
[W](../Category/芝加哥大學校友.md "wikilink")
[W](../Category/粒子物理學家.md "wikilink")
[Category:普林斯顿高等研究院教职员](../Category/普林斯顿高等研究院教职员.md "wikilink")
[Category:洛伦兹奖章获得者](../Category/洛伦兹奖章获得者.md "wikilink")
[Category:樱井奖获得者](../Category/樱井奖获得者.md "wikilink")
[Category:ICTP狄拉克奖章获得者](../Category/ICTP狄拉克奖章获得者.md "wikilink")
[Category:奥斯卡·克莱因纪念讲座](../Category/奥斯卡·克莱因纪念讲座.md "wikilink")