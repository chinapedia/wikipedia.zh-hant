**全美反对阵线（The All-American Rejects）**是由Tyson Ritter和Nick
Wheeler于2000年在[美國](../Page/美國.md "wikilink")[奥克拉荷马州](../Page/奥克拉荷马州.md "wikilink")[斯蒂爾沃特发起的四人乐团](../Page/:en:Stillwater,_Oklahoma.md "wikilink")。乐团在2005年发行的专辑
[Move Along](../Page/:en:Move_Along.md "wikilink") 获得双白金成绩。

## 简介

### 2001年－2004年：商业成功

2001年夏天，该团发行了EP《[Same Girl, New
Songs](../Page/:en:Same_Girl,_New_Songs.md "wikilink")》并开始在美国中西部巡回演出。
Mike Kennerty和Chris Gaylor于2002年加入乐团。 2003年，發行同名專輯《[The All-American
Rejects](../Page/:en:The_All-American_Rejects_\(album\).md "wikilink")》。

### 2005年－2006年：专辑《Move Along》（進行曲）

2005年7月12日，发行第二張專輯《[Move Along](../Page/:en:Move_Along.md "wikilink")》
，其中包括单曲 "[Dirty Little
Secret](../Page/:en:Dirty_Little_Secret.md "wikilink")" 。

### 2007年－2009年：專輯《When the World Comes Down》（傾城傳說）

2008年12月16日，發行第三張專輯《[When the World Comes
Down](../Page/:en:When_the_World_Comes_Down.md "wikilink")》。當中的單曲"[Gives
You
Hell](../Page/:en:Gives_You_Hell.md "wikilink")"先在2008年10月8日發行，此單曲在2009年的[billboard](../Page/billboard.md "wikilink").com的流行曲類別、iTunes上取得第一名，同時成為2009年iTunes上最多下載的第五名。

### 2010年至今：專輯《Kids in the Street》（無家可歸的小孩）

2012年3月27日，發行第四張專輯《[Kids in the
Street](../Page/:en:Kids_in_the_Street.md "wikilink")》。據主唱Tyson
Ritter指，這張專輯和他們過去的有很大的分別。

## 成员

### 现任成员

  - Tyson Ritter –
    [主唱](../Page/主唱.md "wikilink")，[贝斯](../Page/贝斯.md "wikilink")，[钢琴](../Page/钢琴.md "wikilink")
  - Nick Wheeler –
    [吉他手](../Page/吉他手.md "wikilink")，[和聲歌手](../Page/和聲歌手.md "wikilink")，[键盘](../Page/:en:Keyboardist.md "wikilink")
  - Mike Kennerty –
    [吉他手](../Page/吉他手.md "wikilink")，[和聲歌手](../Page/和聲歌手.md "wikilink")
  - Chris Gaylor –
    [鼓手](../Page/鼓手.md "wikilink")，[打击乐器](../Page/打击乐器.md "wikilink")
  - Kevin "Toad" Saulnier – [键盘](../Page/:en:Keyboardist.md "wikilink")
  - Ello Hibert – [营销](../Page/营销.md "wikilink")

## 唱片列表

### 专辑

<table>
<thead>
<tr class="header">
<th><p>专辑資訊</p></th>
<th><p>排行榜成绩</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><small><a href="../Page/公告牌二百强专辑榜.md" title="wikilink">U.S. 公告牌二百强专辑榜</a></small>[1]</p></td>
<td><p><small><a href="../Page/:en:Top_Heatseekers.md" title="wikilink">U.S. Heatseekers</a></small></p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/:en:The_All-American_Rejects_(album).md" title="wikilink">全美反对阵线（The All-American Rejects）</a></em></p>
<ul>
<li>發行日期: 2002年10月15日</li>
<li>唱片公司: <a href="../Page/:en:Dreamworks_Records.md" title="wikilink">Dreamworks Records</a></li>
</ul></td>
<td><p>25</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/:en:Move_Along.md" title="wikilink">進行曲（Move Along）</a></em></p>
<ul>
<li>發行日期: 2005年7月12日</li>
<li>唱片公司: <a href="../Page/:en:Interscope_Records.md" title="wikilink">Interscope Records</a></li>
</ul></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/:en:When_the_World_Comes_Down.md" title="wikilink">傾城傳說（When the World Comes Down）</a></em></p>
<ul>
<li>發行日期: 2008年12月16日</li>
<li>唱片公司: <a href="../Page/:en:Interscope_Records.md" title="wikilink">Interscope Records</a></li>
</ul></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/:en:Kids_in_the_Street.md" title="wikilink">無家可歸的小孩（Kids in the Street）</a></em></p>
<ul>
<li>發行日期: 2012年3月26日</li>
<li>唱片公司: <a href="../Page/:en:Interscope_Records.md" title="wikilink">Interscope Records</a></li>
</ul></td>
<td><p>18</p></td>
</tr>
</tbody>
</table>

### EP

<table>
<thead>
<tr class="header">
<th><p>专辑資訊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><em><a href="../Page/:en:Same_Girl,_New_Songs.md" title="wikilink">Same Girl, New Songs</a></em></p>
<ul>
<li>發行日期: 2001年5月17日</li>
<li>唱片公司: 無</li>
</ul></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/:en:The_Bite_Back_EP.md" title="wikilink">The Bite Back EP</a></em></p>
<ul>
<li>發行日期: 2005年12月13日</li>
<li>唱片公司: <a href="../Page/:en:Interscope_Records.md" title="wikilink">Interscope Records</a></li>
</ul></td>
</tr>
</tbody>
</table>

### DVD

<table>
<thead>
<tr class="header">
<th><p>DVD資訊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><em><a href="../Page/:en:Live_from_Oklahoma..._The_Too_Bad_for_Hell_DVD!.md" title="wikilink">Live from Oklahoma... The Too Bad for Hell DVD!</a></em></p>
<ul>
<li>發行日期: 2003年9月30日</li>
<li>唱片公司: <a href="../Page/:en:Dreamworks_Records.md" title="wikilink">Dreamworks Records</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/:en:Tournado.md" title="wikilink">Tournado</a></em></p>
<ul>
<li>發行日期: 2007年7月17日</li>
<li>唱片公司: <a href="../Page/:en:Interscope_Records.md" title="wikilink">Interscope Records</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 单曲

| **年份**                                         | **标题**                                                                                             | **排行榜成绩**                                                        | **专辑**                                                             |
| ---------------------------------------------- | -------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ------------------------------------------------------------------ |
| [U.S. Hot 100](../Page/公告牌百强单曲榜.md "wikilink") | [U.S. Pop 100](../Page/:en:Pop_100.md "wikilink")                                                  | [U.S. Modern Rock](../Page/:en:Modern_Rock_Tracks.md "wikilink") | [:en:UK Singles Chart](../Page/:en:UK_Singles_Chart.md "wikilink") |
| 2002年                                          | "[Swing Swing](../Page/:en:Swing,_Swing.md "wikilink")"                                            | 60                                                               | \-                                                                 |
| 2003年                                          | "[The Last Song](../Page/:en:The_Last_Song_\(The_All-American_Rejects_song\).md "wikilink")"       | \-                                                               | \-                                                                 |
| 2003年                                          | "[My Paper Heart](../Page/:en:My_Paper_Heart.md "wikilink")"                                       | \-                                                               | \-                                                                 |
| 2003年                                          | "[Time Stands Still](../Page/:en:Time_Stands_Still_\(song\).md "wikilink")"                        | \-                                                               | \-                                                                 |
| 2005年                                          | "[Dirty Little Secret](../Page/:en:Dirty_Little_Secret.md "wikilink")"                             | 9                                                                | 4                                                                  |
| 2006年                                          | "[Move Along](../Page/:en:Move_Along_\(song\).md "wikilink")"                                      | 15                                                               | 12                                                                 |
| 2006年                                          | "[It Ends Tonight](../Page/:en:It_Ends_Tonight.md "wikilink")"                                     | 8                                                                | 7                                                                  |
| 2006年                                          | "[Top of the World](../Page/:en:Top_of_the_World_\(The_All-American_Rejects_song\).md "wikilink")" | \-                                                               | \-                                                                 |
| 2007年                                          | "[Dance Inside](../Page/:en:Dance_Inside.md "wikilink")"                                           | \-                                                               | \-                                                                 |
| 2008年                                          | "[Gives You Hell](../Page/:en:Gives_You_Hell.md "wikilink")"                                       | 50                                                               | 48                                                                 |

### B面以及其他音轨

  - "Happy Endings" (Demo) – on official AAR website
  - "[Swing, Swing](../Page/:en:Swing,_Swing.md "wikilink")" (DJ Loopy
    Remix) – on official AAR website
  - "The Cigarette Song" – released on non-U.S. versions of *[The
    All-American
    Rejects](../Page/:en:The_All-American_Rejects_\(album\).md "wikilink")*
    (2002)
  - "Eyelash Wishes" and "Kiss Yourself Goodbye" – released on non-U.S.
    versions of *[Move Along](../Page/:en:Move_Along.md "wikilink")*
    (2005)
  - "Bite Back" – released on the single of "Dirty Little Secret" and on
    the iTunes [The Bite Back
    EP](../Page/:en:The_Bite_Back_EP.md "wikilink") (2006)
  - "The Future Has Arrived" – released on the original soundtrack to
    *[Meet the
    Robinsons](../Page/:en:Meet_the_Robinsons_\(soundtrack\).md "wikilink")*
    (2007)
  - "Mona Lisa" – a track on AAR's upcoming 3rd studio album (2008)
  - "Damn, Girl" – a track on AAR's upcoming 3rd studio album (2008)

## 参考资料

<references/>

## 外部链接

  - [Official website](http://www.allamericanrejects.com/)
  - [AAR On RecCenter.com](http://www.reccenter.com/allamericanrejects)
  - [Interview on
    TruePunk.com](https://web.archive.org/web/20080314003957/http://www.truepunk.com/interviews/all_american_rejects/)
  - [All-American Rejects
    Myspace](http://www.myspace.com/allamericanrejects)
  - [Trash the Stage interview with Nick and
    Mike](https://web.archive.org/web/20080212083826/http://www.trashthestage.net/allamericanrejectsinterview07.html)
  - [The All-American Rejects Interview at
    SaintRockNRoll.com](https://web.archive.org/web/20080212092149/http://www.saintrocknroll.com/interview.php?band=All%20American%20Rejects&date=2006-12-11)
  - [All American Reject Becomes Big-Screen
    'Boy'](http://www.mtv.com/news/articles/1570167/20070920/all_american_rejects.jhtml)
  - [The All-American
    Rejects](http://www.rollingstone.com/artists/theallamericanrejects)
    at [Rolling Stone](../Page/:en:Rolling_stone.md "wikilink")
  - [Site Download](http://www.powerpopme.blogspot.com)

[Category:美國搖滾樂團](../Category/美國搖滾樂團.md "wikilink")
[Category:美國庞克摇滚乐团](../Category/美國庞克摇滚乐团.md "wikilink")
[Category:美國另類搖滾樂團](../Category/美國另類搖滾樂團.md "wikilink")

1.