**周国富**（），[浙江](../Page/浙江.md "wikilink")[诸暨人](../Page/诸暨.md "wikilink")，1972年6月加入中国共产党，1967年9月参加工作，浙江农业大学农学系农学专业毕业，大学学历。曾任[政协第十届浙江省委员会主席](../Page/政协.md "wikilink")（2008年
- 2011年），现任[全国政协文史和学习委员会副主任委员](../Page/全国政协.md "wikilink")。

早年毕业于[浙江农业大学](../Page/浙江农业大学.md "wikilink")（今[浙江大学](../Page/浙江大学.md "wikilink")）。曾任浙江省[义乌县徐村公社城阳区农技站负责人](../Page/义乌县.md "wikilink")，中国援助[乍得专家组成员](../Page/乍得.md "wikilink")，诸暨县农业局农技股副股长，诸暨县副县长，[绍兴市委常委](../Page/绍兴市.md "wikilink")、组织部部长，省委组织部副部长，[湖州市委副书记](../Page/湖州市.md "wikilink")、书记，[新疆维吾尔自治区党委常委](../Page/新疆维吾尔自治区.md "wikilink")、组织部部长，浙江省委常委、组织部部长，省委副书记、组织部部长，政法委书记、[省纪委书记](../Page/省纪委.md "wikilink")、省政协主席、党组书记等职。

2008年，当选第十一届全国政协委员\[1\]，代表中国共产党，分入第二组。并担任文史和学习委员会副主任。\[2\]

## 參考文獻

{{-}}

[Z周](../Category/中华人民共和国政府官员.md "wikilink")
[Z周](../Category/诸暨人.md "wikilink")
[H](../Category/周姓.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:浙江省政協主席](../Category/浙江省政協主席.md "wikilink")
[Category:浙江农业大学校友](../Category/浙江农业大学校友.md "wikilink")
[Category:中共湖州市委书记](../Category/中共湖州市委书记.md "wikilink")
[Category:中共浙江省委副书记](../Category/中共浙江省委副书记.md "wikilink")
[Category:中共浙江省委組織部部長](../Category/中共浙江省委組織部部長.md "wikilink")
[Category:中共浙江省委政法委书记](../Category/中共浙江省委政法委书记.md "wikilink")

1.
2.