**臺北府**\[1\]為[清廷於](../Page/清廷.md "wikilink")[光緒元年](../Page/光緒.md "wikilink")12月20日（西元1876年1月16日），[沈葆禎奏請在](../Page/沈葆禎.md "wikilink")[臺灣島北部設置之](../Page/臺灣島.md "wikilink")[官署及](../Page/官署.md "wikilink")[行政區劃](../Page/行政區劃.md "wikilink")\[2\]。於官署方面，臺北府設置[臺北府知府等官員](../Page/臺北府知府.md "wikilink")，[衙門地點則位於](../Page/衙門.md "wikilink")[臺北城內](../Page/臺北城.md "wikilink")，與[臺灣巡撫衙門](../Page/臺灣巡撫衙門.md "wikilink")、[臺灣布政使司衙門](../Page/臺灣布政使司衙門.md "wikilink")，[淡水縣知縣衙門比鄰](../Page/淡水縣.md "wikilink")。（今臺北中山堂附近）。另外，於行政區劃方面，則為[臺灣道之下](../Page/臺灣道.md "wikilink")，與[臺灣府並立之區劃](../Page/臺灣府.md "wikilink")。1885年，[臺灣建省後](../Page/福建臺灣省.md "wikilink")，臺北府轉為隸屬「福建臺灣省」之下，也是清朝218個府建制之一。轄區涵蓋今日之[臺北市](../Page/臺北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[基隆市](../Page/基隆市.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")、[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[新竹縣及](../Page/新竹縣.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")。

:\*1876年1月16日（清光緒元年12月20日）臺北府設三縣一廳\[3\]

|                                         |
| :-------------------------------------: |
|    [臺灣道](../Page/臺灣道.md "wikilink")     |
|                 **臺北府**                 |
| [宜蘭縣](../Page/宜蘭縣_\(清朝\).md "wikilink") |

:\*1885年（[光緒十一年](../Page/光緒.md "wikilink")）台灣建[省](../Page/福建台灣省.md "wikilink")，臺北府設三縣一廳

|                                         |
| :-------------------------------------: |
|  [福建臺灣省](../Page/福建臺灣省.md "wikilink")   |
|                 **臺北府**                 |
| [宜蘭縣](../Page/宜蘭縣_\(清朝\).md "wikilink") |

:\*1894年（[光緒二十年](../Page/光緒.md "wikilink")）臺北府設三縣二廳

|                                         |
| :-------------------------------------: |
|  [福建臺灣省](../Page/福建臺灣省.md "wikilink")   |
|                 **臺北府**                 |
| [宜蘭縣](../Page/宜蘭縣_\(清朝\).md "wikilink") |

## 相關條目

  - [臺北州](../Page/臺北州.md "wikilink")
  - [臺灣府](../Page/臺灣府.md "wikilink")

## 參考來源

  - 注釋

<!-- end list -->

  - 引用

[臺北府](../Category/臺北府.md "wikilink")
[Category:清朝的府](../Category/清朝的府.md "wikilink")
[Category:1876年建立的行政区划](../Category/1876年建立的行政区划.md "wikilink")
[Category:1895年废除的行政区划](../Category/1895年废除的行政区划.md "wikilink")
[Category:1876年台灣建立](../Category/1876年台灣建立.md "wikilink")
[Category:1895年台灣廢除](../Category/1895年台灣廢除.md "wikilink")

1.  清代臺灣相關地名專用「臺」字，「台」為日治之後用字。

2.  《[臺灣文獻叢刊](../Page/臺灣文獻叢刊.md "wikilink")》〈臺灣輿地彙鈔〉，台灣省文獻委員會（今[國史館臺灣文獻館](../Page/國史館臺灣文獻館.md "wikilink")），台北：台灣銀行經濟研究室，1996年重印，第113-116頁，《[清朝續文獻通考](../Page/清朝續文獻通考.md "wikilink")》卷315，劉錦藻，〈臺灣省輿地考〉乙文

3.