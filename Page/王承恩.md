**王承恩**（），[明末](../Page/明朝.md "wikilink")[宦官](../Page/宦官.md "wikilink")，屬太監[曹化淳名下](../Page/曹化淳.md "wikilink")，累官[司禮監](../Page/司禮監.md "wikilink")[秉筆太監](../Page/秉筆太監.md "wikilink")，[甲申之變](../Page/甲申之變.md "wikilink")[李闖攻入](../Page/李闖.md "wikilink")[帝都](../Page/帝都.md "wikilink")，[崇禎帝](../Page/崇禎帝.md "wikilink")[自縊](../Page/自縊.md "wikilink")，他也殉主自盡。南明[弘光帝闻之](../Page/弘光帝.md "wikilink")，赐谥**忠愍**。

[清朝](../Page/清朝.md "wikilink")[顺治帝题碑](../Page/顺治帝.md "wikilink")“贞臣为主，捐躯以从”，[康熙帝曾在](../Page/康熙帝.md "wikilink")[思陵附近為王承恩豎碑立傳](../Page/明思陵.md "wikilink")。

## 生平

王承恩於[明](../Page/明朝.md "wikilink")[萬曆年間進宮](../Page/萬曆.md "wikilink")，擔任[信王府的](../Page/朱由檢.md "wikilink")[掌管太監](../Page/掌管太監.md "wikilink")，即自[朱由檢](../Page/朱由檢.md "wikilink")（後來的[崇禎帝](../Page/崇禎帝.md "wikilink")）一出生開始就照顧著他。於[天啟年間](../Page/天启_\(明\).md "wikilink")，他也是[魏忠賢勢力的其中一份子](../Page/魏忠賢.md "wikilink")。但身為太監的魏忠賢想要篡位，王承恩卻幫助朱由檢，使得天啟年間的司禮監太監魏忠賢只好在[熹宗朱由校臨死前宣佈由信王朱由檢繼位](../Page/明熹宗.md "wikilink")，且熹宗之-{后}-[張皇后也發布懿旨](../Page/孝哀悊皇后.md "wikilink")：「召信王入繼大統」，因此魏忠賢與王承恩決裂。

他於[崇禎年間擔任](../Page/崇禎.md "wikilink")[司禮監太監](../Page/司禮監太監.md "wikilink")（是明代時太監的最高職位），侍奉了思宗朱由檢大半輩子，他並未似魏忠賢一般的奪權造反，而幫助思宗辦公。但是有些官臣覺得他的權力過甚，有越權之行為，如[戶部尚書](../Page/戶部.md "wikilink")[周延儒曾向崇禎進言](../Page/周延儒.md "wikilink")，認為王承恩的權勢過大，是另一個[魏忠賢](../Page/魏忠賢.md "wikilink")。周延儒上書告發王承恩，愛猜忌的思宗雖然認為周延儒講的話有道理，一度想殺掉王承恩，但是思宗最後還是軟了心，任用王承恩。

崇禎十七年（1644年）三月十九日早上，崇禎帝[朱由檢由太監王承恩陪伴登上](../Page/朱由檢.md "wikilink")[煤山](../Page/煤山.md "wikilink")（今[北京](../Page/北京.md "wikilink")[景山](../Page/景山.md "wikilink")），吊死在山腰[壽皇亭附近的歪斜的老](../Page/壽皇亭.md "wikilink")[槐樹上](../Page/槐樹.md "wikilink")。随后王承恩也吊死於旁邊的[海棠樹上](../Page/海棠樹.md "wikilink")\[1\]。中午，[李自成進入皇宮](../Page/李自成.md "wikilink")，下令“獻帝者賞萬金，封[伯爵](../Page/伯爵.md "wikilink")。匿者滅族”。崇禎屍體在樹上吊了三天，直至三月二十一日才被發現，被停放在[東華門外](../Page/東華門.md "wikilink")，裝入[柳木棺內](../Page/柳木棺.md "wikilink")。

四月四日，李自成下令打开田贵妃的坟墓，将崇禎帝夫妇葬了进去（贵妃墓因此改称[思陵](../Page/明思陵.md "wikilink")），并将王承恩附葬陵侧。

## 參考書目

  - [吳梅村](../Page/吳梅村.md "wikilink")：《綏寇紀略補遺》

## 註腳

[W](../Category/明朝宦官.md "wikilink")
[W](../Category/明朝自杀人物.md "wikilink")
[Category:上吊自殺者](../Category/上吊自殺者.md "wikilink")
[C承恩](../Category/王姓.md "wikilink")
[Category:諡忠愍](../Category/諡忠愍.md "wikilink")

1.  一說是[王之心](../Page/王之心.md "wikilink")，有的說是[王之俊](../Page/王之俊.md "wikilink")