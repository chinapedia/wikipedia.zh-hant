**男性生殖系統**是由男性許多[生殖器官或組織組成](../Page/生殖器.md "wikilink")，和[人類繁殖有關的系統](../Page/人類繁殖.md "wikilink")。有些在體外，有些則在[骨盆腔內](../Page/骨盆腔.md "wikilink")。

男性主要的性器官是製造[精子的](../Page/精子.md "wikilink")[睪丸](../Page/睪丸.md "wikilink")，以及分泌[精液的](../Page/精液.md "wikilink")[陰莖](../Page/人類陰莖.md "wikilink")，在和女性[性交](../Page/性交.md "wikilink")，精子可以使女性體內的[卵细胞](../Page/卵细胞.md "wikilink")[受精](../Page/受精.md "wikilink")，[受精卵會發展成為](../Page/受精卵.md "wikilink")[胎兒](../Page/胎兒.md "wikilink")。

男性生殖系統可以分為外生殖系統以及內生殖系統。外生殖系統包括陰莖及[阴囊](../Page/阴囊.md "wikilink")，內生殖系統包括[附睾](../Page/附睾.md "wikilink")、[输精管及](../Page/输精管.md "wikilink")[精囊](../Page/精囊.md "wikilink")、[前列腺](../Page/前列腺.md "wikilink")、[尿道球腺等組織](../Page/尿道球腺.md "wikilink")。

## 結構系統

<table>
<tbody>
<tr class="odd">
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Male_anatomy_number.svg" title="fig:Male_anatomy_number.svg">Male_anatomy_number.svg</a><br />
男性生殖器解剖圖</p>
</center>
<p>　</p>
</td></td>
<td><p>1、<a href="../Page/膀胱.md" title="wikilink">膀胱</a></p></td>
<td><p>10、<a href="../Page/精囊腺.md" title="wikilink">精囊腺</a></p></td>
</tr>
<tr class="even">
<td><p>2、<a href="../Page/恥骨.md" title="wikilink">恥骨</a></p></td>
<td><p>11、<a href="../Page/射精管.md" title="wikilink">射精管</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3、<a href="../Page/陰莖.md" title="wikilink">陰莖</a></p></td>
<td><p>12、<a href="../Page/前列腺.md" title="wikilink">前列腺</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4、陰莖<a href="../Page/海綿體.md" title="wikilink">海綿體</a></p></td>
<td><p>13、<a href="../Page/尿道球腺.md" title="wikilink">尿道球腺</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5、<a href="../Page/龜頭.md" title="wikilink">龜頭</a></p></td>
<td><p>14、<a href="../Page/肛門.md" title="wikilink">肛門</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6、<a href="../Page/包皮.md" title="wikilink">包皮</a></p></td>
<td><p>15、<a href="../Page/輸精管.md" title="wikilink">輸精管</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7、<a href="../Page/尿道口.md" title="wikilink">尿道口</a></p></td>
<td><p>16、<a href="../Page/附睾.md" title="wikilink">附睾</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8、乙狀<a href="../Page/結腸.md" title="wikilink">結腸</a></p></td>
<td><p>17、<a href="../Page/睾丸.md" title="wikilink">睾丸</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9、<a href="../Page/直腸.md" title="wikilink">直腸</a></p></td>
<td><p>18、<a href="../Page/陰囊.md" title="wikilink">陰囊</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 外生殖器官

[Flaccid_penis_cropped.jpg](https://zh.wikipedia.org/wiki/File:Flaccid_penis_cropped.jpg "fig:Flaccid_penis_cropped.jpg")

### 陰莖

陰莖是男性的，呈長條形，且有膨脹的球狀尖端，稱為[龟头](../Page/龟头.md "wikilink")，其外圍有[包皮保護](../Page/包皮.md "wikilink")。當男性接收到[性刺激時](../Page/性刺激.md "wikilink")，陰莖會[勃起](../Page/勃起.md "wikilink")，預備進行性行為。勃起是因為陰莖勃起組織的窦中充满了血液。此時陰莖的動脈擴張，而靜脈收縮，因此血液在受壓力下流進勃起[软骨中](../Page/软骨.md "wikilink")。陰莖的營養是由供應。

### 陰囊

陰囊是袋狀結構，掛在陰莖後面。陰囊支撐睪丸，而且有保護作用，其中也有許多的神經及血管。若溫度較低時，[提睾肌收縮陰囊靠近身體](../Page/提睾肌.md "wikilink")，而讓陰囊表面有皺紋。當溫度上昇時，提睾肌及肉膜肌都會放鬆，使陰囊遠離身體，此時也就沒有皺紋了。

陰囊通過維持與腹部及骨盆腔的連通。由[精索動脈](../Page/精索動脈.md "wikilink")（Spermatic
artery）、靜脈、神經與結締組織形成的[精索](../Page/精索.md "wikilink")，會通過腹股溝管進入睾丸。

## 內生殖器官

[Sobo_1909_571.png](https://zh.wikipedia.org/wiki/File:Sobo_1909_571.png "fig:Sobo_1909_571.png")

### 附睾

[附睾是白色緊密盤繞的管子](../Page/附睾.md "wikilink")，在睾丸旁邊，可以使精子成熟，並且在精子進入[输精管前儲存精子](../Page/输精管.md "wikilink")，输精管會將精子運送到[壺腹腺](../Page/壺腹腺.md "wikilink")（ampullary
gland）和。

### 输精管

[输精管是長度約](../Page/输精管.md "wikilink")30公分的細管，從附睪一直到骨盆腔。將精子從附睪運送到[射精管](../Page/射精管.md "wikilink")。

### 男性附屬腺體

有三個分泌有潤滑作用的液體，並且提供精子營養，這些腺體分別是[精囊](../Page/精囊.md "wikilink")、[前列腺及](../Page/前列腺.md "wikilink")[尿道球腺](../Page/尿道球腺.md "wikilink")（考伯氏腺）。

## 參見

  - [女性生殖系統](../Page/女性生殖系統.md "wikilink")

  - [繁殖](../Page/繁殖.md "wikilink")

  - [射精](../Page/射精.md "wikilink")

  - [勃起](../Page/勃起.md "wikilink")

  - [人類陰莖長度](../Page/人類陰莖長度.md "wikilink")

  - [有性生殖的演化](../Page/有性生殖的演化.md "wikilink")

  -
  -
  - [精子发生](../Page/精子发生.md "wikilink")

## 參考文獻

  -
  -
  -
  -
  -
  -
## 外部連接

  - [Аномалии мужских половых
    органов](http://www.eurolab.ua/encyclopedia/Urology.patient/3014/)
  - [Причины мужского
    бесплодия](https://web.archive.org/web/20170901112235/http://organizm.name/medicina/muj-besplodie.html)

[Category:男性生殖系统](../Category/男性生殖系统.md "wikilink")
[Category:生殖系統](../Category/生殖系統.md "wikilink")