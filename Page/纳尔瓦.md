**纳尔瓦市**（，），[爱沙尼亚第三大城市](../Page/爱沙尼亚.md "wikilink")，是该国[東維魯縣最大城市](../Page/東維魯縣.md "wikilink")。与[俄罗斯的](../Page/俄罗斯.md "wikilink")[伊万哥罗德市](../Page/伊万哥罗德.md "wikilink")（；，音译雅尼林）隔河相望。

纳尔瓦有大約90%
\[[https://news.err.ee/111954/estonia-losing-information-war-in-narva-says-head-of-narva-college\]的人說俄語](https://news.err.ee/111954/estonia-losing-information-war-in-narva-says-head-of-narva-college%5D的人說俄語)。

13世纪后期，征服爱沙尼亚北部的[丹麦人在现在纳尔瓦老城区建立要塞](../Page/丹麦.md "wikilink")。1347年，丹麦国王将其在爱沙尼亚的领地卖给[利沃尼亚骑士团](../Page/利沃尼亚骑士团.md "wikilink")。在[利沃尼亚战争中](../Page/利沃尼亚战争.md "wikilink")，纳尔瓦被[俄国占领](../Page/俄国.md "wikilink")。1581年归属[瑞典](../Page/瑞典.md "wikilink")。现在仍然残存的纳尔瓦古城遗址基本上是在瑞典统治时期修建的。

在1700年的[纳尔瓦战役中](../Page/纳尔瓦战役.md "wikilink")，瑞典国王[查理十二世在纳尔瓦及其北边的](../Page/查理十二世.md "wikilink")[納爾瓦約埃蘇轻松击溃了俄国](../Page/納爾瓦約埃蘇.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[彼得一世的大军](../Page/彼得一世.md "wikilink")。但在1704年，俄国最终占领了纳尔瓦，直至1918年爱沙尼亚独立。[一战和](../Page/一战.md "wikilink")[二战中](../Page/二战.md "wikilink")，爱沙尼亚都曾被[德国占领](../Page/德国.md "wikilink")。1944年，[苏联与德国在此展开激战](../Page/苏联.md "wikilink")，苏军获胜，并由此攻入爱沙尼亚。二战结束后，苏联政府将雅尼林及纳尔瓦东部的一部分爱沙尼亚领土划入俄罗斯，作为对爱沙尼亚人支持德国的惩罚。

## 纳尔瓦要塞

[Narva,_pevnost.jpg](https://zh.wikipedia.org/wiki/File:Narva,_pevnost.jpg "fig:Narva,_pevnost.jpg")
纳尔瓦要塞是瑞典与俄国激战的重要地点。要塞现存一个塔楼（赫尔曼塔楼）、几段残缺的城墙和护城河。防御工事的大部分完成于17世纪的瑞典统治时期。赫尔曼塔及附近的城墙保存比较完好，现在作为纳尔瓦市博物馆。

[N](../Category/伊达-维鲁县城市.md "wikilink")