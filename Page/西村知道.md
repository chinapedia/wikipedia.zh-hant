**西村知道**（，），[日本資深男性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")、[演員](../Page/演員.md "wikilink")、[旁白](../Page/旁白.md "wikilink")。[ARTSVISION所屬](../Page/ARTSVISION.md "wikilink")\[1\]。

出身於[千葉縣](../Page/千葉縣.md "wikilink")[松戶市](../Page/松戶市.md "wikilink")\[2\]。[身高](../Page/身高.md "wikilink")169cm。A型[血](../Page/血型.md "wikilink")。

## 人物簡介

電影學科畢業。出道以來有豐富的配音經驗，經常出演很多或溫厚或搞笑或冷酷掌權人的角色，因此在各世代的動畫迷中有很高的知名度。

特長有[武打](../Page/武打.md "wikilink")（比劍場面）、下[將棋](../Page/將棋.md "wikilink")\[3\]。[麥金塔電腦的使用者](../Page/麥金塔電腦.md "wikilink")\[4\]，目前很少在使用。

同時在[ARTSVISION其附屬](../Page/ARTSVISION.md "wikilink")[日本播音演技研究所兼任講師全力栽培後進新人](../Page/日本播音演技研究所.md "wikilink")，其中[鳥海浩輔正是西村門下的學生](../Page/鳥海浩輔.md "wikilink")\[5\]。

## 演出作品

### 電視動畫

  - [動畫秘密花園](../Page/動畫秘密花園.md "wikilink")（愛德華·索爾比）
  - [魔神英雄传](../Page/魔神英雄传.md "wikilink")（劍部武一郎）
  - [魔神英雄傳2](../Page/魔神英雄傳2.md "wikilink")（劍部武一郎）
  - [超魔神英雄傳](../Page/超魔神英雄傳.md "wikilink")（劍部武一郎）
  - [機甲創世紀](../Page/機甲創世紀.md "wikilink")（吉姆）
  - [閃電霹靂車](../Page/閃電霹靂車.md "wikilink")（皮塔里亞·羅貝）
  - [铁臂阿童木](../Page/铁臂阿童木.md "wikilink")（曼尼市長）
  - [甜甜小公主](../Page/甜甜小公主.md "wikilink")（大福国的家老 \#32、キャプテンガーリック
    \#39、大岡越前の守 \#44）
  - [頭文字D](../Page/頭文字D.md "wikilink") 系列（立花祐一）
  - [宇宙战舰大和号III](../Page/宇宙战舰大和号.md "wikilink")（レバルス警備隊長）
  - [福星小子](../Page/福星小子.md "wikilink")（友引高中校長）
  - [少年劍道王直角](../Page/少年劍道王直角.md "wikilink")（旁白）
  - [金田一少年之事件簿](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（不動高校校長、小宮山吾郎、弁護士、長老、辻口鈴置、金森民夫）
  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（全藏的父親、定食屋的老爹）
  - [鬼太郎](../Page/鬼太郎.md "wikilink")（第4作）（あずきとぎ、好閑人）
  - 鬼太郎（第5作）（おどろおどろ、百百爺）
  - [攻殻機動隊 S.A.C. 2nd
    GIG](../Page/攻殻機動隊_S.A.C._2nd_GIG.md "wikilink")（片倉）
  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")（秋本飛飛丸(初代)）
  - [魁！！男塾](../Page/魁！！男塾.md "wikilink")（モヒカン）※第12話
  - [地獄少女 二籠](../Page/地獄少女_二籠.md "wikilink")（龜田一人）\*第12話
  - [死后文](../Page/死后文.md "wikilink")（綾瀨竹藏）
  - [十二國記](../Page/十二國記.md "wikilink")（遠甫、小松家的老臣、慶國地官長）
  - [少年陰陽師](../Page/少年陰陽師.md "wikilink")（藤原道長）
  - [新世紀福音战士](../Page/新世紀福音战士.md "wikilink")（O.T.R艦長）
  - [北斗神拳](../Page/北斗神拳.md "wikilink")（ゴッドアーミー少佐、ゼンギョウ、リゾ、郡都隊長、アルフ）
  - [名侦探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（昭夫的父親、津田秀夫、日下昇平、海原福夫、阪東京一、坂東彥衛門、番頭）
  - [棒球大联盟](../Page/棒球大联盟.md "wikilink")（道森）
  - [豆芽小文](../Page/豆芽小文.md "wikilink")（樹慶藏）
      - [豆芽小文Returns](../Page/豆芽小文.md "wikilink")（樹慶藏）
  - [旋風管家](../Page/旋風管家.md "wikilink")（三千院帝）
  - [MONSTER](../Page/MONSTER.md "wikilink")（パテラ部長刑事）
  - [ONE
    PIECE](../Page/ONE_PIECE.md "wikilink")（カモネギ、船斬りTボーン、ドン・アッチーノ、頓·阿齊諾）
  - [世界名作劇場系列](../Page/世界名作劇場.md "wikilink")
      - [法蘭德斯之犬](../Page/法蘭德斯之犬.md "wikilink")（審查員）
      - [南方彩虹的露西](../Page/南方彩虹的露西.md "wikilink")（克萊頓、柏德）
      - [牧場上的少女卡特莉](../Page/牧場上的少女卡特莉.md "wikilink")（卡洛‧庫賽拉）
      - [小公主莎拉](../Page/小公主莎拉.md "wikilink")（卡麥可律師(第一代)）
      - [愛少女波麗安娜物語](../Page/愛少女波麗安娜物語.md "wikilink")（威利斯）
      - [七海的堤可](../Page/七海的堤可.md "wikilink")（喬狄諾）
      - [羅密歐的藍天](../Page/羅密歐的藍天.md "wikilink")（馬傑歐、史卡拉）
      - [名犬萊西](../Page/名犬萊西.md "wikilink")（班傑明）
      - [無家可歸的孩子蕾米](../Page/無家可歸的孩子蕾米.md "wikilink")（維克多）
      - [悲慘世界 少女珂賽特](../Page/悲慘世界_少女珂賽特.md "wikilink")（貝蘭傑）
      - [波菲的漫長旅程](../Page/波菲的漫長旅程.md "wikilink")（安東尼奧）
  - [企業傭兵](../Page/企業傭兵.md "wikilink")（坂東）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（グラワルド＝サーペント）
  - [超時空要塞Frontier](../Page/超時空要塞Frontier.md "wikilink")（霍華‧葛拉斯）
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")（「第三代土影」大軒）
  - [我愛美樂蒂](../Page/我愛美樂蒂.md "wikilink")（美樂蒂的爺爺）
  - [SKET DANCE](../Page/SKET_DANCE.md "wikilink")（毛塚老師）
  - [摩登大法師](../Page/摩登大法師.md "wikilink")[聖槍修女](../Page/聖槍修女.md "wikilink")（The
    Elder/中譯：長老爺爺）

<!-- end list -->

  - 1983年

<!-- end list -->

  - （[羅得](../Page/羅得.md "wikilink")）

<!-- end list -->

  - 1985年

<!-- end list -->

  - [機動戰士Z 鋼彈](../Page/機動戰士Z_鋼彈.md "wikilink")（賈米托夫·海曼）

<!-- end list -->

  - 1986年

<!-- end list -->

  - [相聚一刻](../Page/相聚一刻.md "wikilink")（五代的父親）

<!-- end list -->

  - 1992年

<!-- end list -->

  - [幽☆遊☆白書](../Page/幽遊白書.md "wikilink")（旁白、竹中老師、喬治早乙女、亞尾連邪）

<!-- end list -->

  - 1993年

<!-- end list -->

  - [灌籃高手](../Page/灌籃高手.md "wikilink")（安西教練〈安西光義〉）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [他和她的故事](../Page/他和她的故事.md "wikilink")（洋之的祖父）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [棋魂](../Page/棋魂.md "wikilink")（椿俊郎）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [驚爆危機！](../Page/驚爆危機#驚爆危機！.md "wikilink")（理查·亨利·馬德加斯）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [DEAR BOYS](../Page/灌籃少年.md "wikilink")（校長）
  - 驚爆危機？校園篇（理查·馬德加斯）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [妄想代理人](../Page/妄想代理人.md "wikilink")（平沼芳雄）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [侵略！花枝娘](../Page/侵略！花枝娘.md "wikilink")（校長）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [「C」](../Page/C_\(動畫\).md "wikilink")（菊池義行、副總裁）
  - [日常](../Page/日常.md "wikilink")（江木正晴）
  - [碎之星](../Page/Fractale.md "wikilink")（祖爺爺）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [玉子市場](../Page/玉子市場.md "wikilink")（北白川福）
  - [琴浦小姐](../Page/琴浦小姐.md "wikilink")（琴浦善三）
  - [超速變形螺旋傑特](../Page/超速變形螺旋傑特.md "wikilink")（觀光協會會長）
  - [Walkure
    Romanze](../Page/Walkure_Romanze_少女騎士物語.md "wikilink")（葛雷托福·穆利斯·阿斯科特）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [黑色子彈](../Page/黑色子彈.md "wikilink")（松崎）

  - （田中的父親）

  - [GLASSLIP](../Page/GLASSLIP.md "wikilink")（白崎又三郎）

  - [蟲師 續章](../Page/蟲師.md "wikilink")（陽吉）

  - [熱情傳奇〜導師的黎明〜](../Page/熱情傳奇.md "wikilink")（旁白）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [絕對雙刃](../Page/絕對雙刃.md "wikilink")（裝鋼技師）
  - [The Rolling Girls](../Page/The_Rolling_Girls.md "wikilink")（車持不比等）
  - [銀魂°](../Page/銀魂_\(動畫\).md "wikilink")（全藏之父）
  - [Classroom☆Crisis](../Page/Classroom☆Crisis.md "wikilink")（笹山正剛）
  - [Charlotte](../Page/Charlotte_\(動畫\).md "wikilink")（堤內）
  - [那就是聲優！](../Page/那就是聲優！.md "wikilink")（關上）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [粗點心戰爭](../Page/粗點心戰爭.md "wikilink")（刻糖店主人）
  - [最終騎士](../Page/最終騎士.md "wikilink")（老人）
  - [鬼牌遊戲](../Page/鬼牌遊戲.md "wikilink")（阿久津泰政中將）
  - [境界之輪迴](../Page/境界之輪迴.md "wikilink") 第2期（黑星）
  - [不愉快的妖怪庵](../Page/不愉快的妖怪庵.md "wikilink")（豬柄嶽夕日瀧翁神）
  - [烙印勇士](../Page/烙印勇士.md "wikilink")（佛斯）
  - [野球少年](../Page/野球少年.md "wikilink")（校長）
  - [啟航吧！編舟計畫](../Page/啟航吧！編舟計畫.md "wikilink")（山下局長）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [鬼平](../Page/鬼平犯科帳.md "wikilink")（利右衛門）
  - [帶著智慧型手機闖蕩異世界](../Page/帶著智慧型手機闖蕩異世界.md "wikilink")（公爵家管家 雷姆、萊姆）
  - [地獄少女 宵伽](../Page/地獄少女.md "wikilink")（龜岡一人）
  - [THE REFLECTION](../Page/THE_REFLECTION.md "wikilink")（神秘先生）
  - 名侦探柯南（竹隈殿辅 ）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [三顆星彩色冒險](../Page/三顆星彩色冒險.md "wikilink")（博物館的男性）
  - [愛在雨過天晴時](../Page/愛在雨過天晴時.md "wikilink")（古書店主）
  - [多田君不戀愛](../Page/多田君不戀愛.md "wikilink")（主辦人）
  - [美男高校地球防衛部HAPPY
    KISS！](../Page/美男高校地球防衛部HAPPY_KISS！.md "wikilink")（カモパパ王）
  - 驚爆危機！Invisible Victory（理查·馬德加斯）
  - [鬼太郎
    (第6作)](../Page/鬼太郎.md "wikilink")（[惡魔彼列](../Page/彼列.md "wikilink")\[6\]）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [傀儡馬戲團](../Page/傀儡馬戲團.md "wikilink")（村長）

### OVA

  - 2017年

<!-- end list -->

  - [夏目友人帳 一夜盃](../Page/夏目友人帳.md "wikilink")（日土）
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink") OAD4（咲耶姬）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [幽遊白書](../Page/幽遊白書.md "wikilink")「TWO SHOTS」（旁白）
  - 幽遊白書「是成是敗」（-{zh-tw:喬琪早乙女 ;zh-hk:佐治早乙女}-）

### 電影動畫

  - 2014年

<!-- end list -->

  - [電影 玉子愛情故事](../Page/玉子市場.md "wikilink")（北白川福）

### 網路動畫

  - 2006年

<!-- end list -->

  - [幕末機關說](../Page/幕末機關說.md "wikilink")（[巴夏禮](../Page/巴夏禮.md "wikilink")）

<!-- end list -->

  - 2016年

<!-- end list -->

  - （神明、得意先\[7\]）

<!-- end list -->

  - 2017年

<!-- end list -->

  - （**遠藤豆吉**\[8\]）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [B：彼之初](../Page/B：彼之初.md "wikilink")（星名莉莉的父親）

### 遊戲

  - [街头霸王系列](../Page/街头霸王.md "wikilink")（Vega司令、豪鬼）
  - [私立正義學園](../Page/私立正義學園.md "wikilink")（忌野雷藏）
  - [英雄聯盟](../Page/英雄聯盟.md "wikilink") （雷尼克頓）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [神秘海域4：盜賊末路](../Page/神秘海域4：盜賊末路.md "wikilink")（海克特·阿卡札）

  - 火影忍者疾風傳 終極風暴4（三代土影大野木\[9\]）

  - （執政官）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [超級機器人大戰V](../Page/超級機器人大戰V.md "wikilink")（理查·亨利·馬德加斯）

  - （拉吉爾）

<!-- end list -->

  - 2018年

<!-- end list -->

  - 真紅之焰 真田忍法帳（戶澤白雲齋）
  - [超級機器人大戰X](../Page/超級機器人大戰X.md "wikilink")（劍部錫巴）
  - 驚爆危機！戰鬥的 Who Dares Wins（理查·亨利·馬德加斯\[10\]）

### 廣播劇

  - NHK-FM 「假想騎士」（2003年）

  - 聲優刑事（太田黑忠夫）

  - （岸田群平）

  - [驚爆危機！舞動的Very merry
    Christmas](../Page/驚爆危機#廣播劇.md "wikilink")（理查·亨利·馬德加斯）

### 廣播劇CD

  - [浪客劍心](../Page/浪客劍心.md "wikilink")（谷十三郎）

### 外語配音

※作品依英文名稱及英文原名排序。

#### 演員

  - **[保羅·加法葉](../Page/保羅·加法葉.md "wikilink")**
      - （Mike O'Brien）

      - [王牌對王牌](../Page/王牌對王牌.md "wikilink")（Nathan 'Nate'
        Roenick）※[朝日電視台版](../Page/朝日電視台.md "wikilink")。

      - （Andrew Cassedy博士）※[富士電視台版](../Page/富士電視台.md "wikilink")。

<!-- end list -->

  - ****
      - （Stump Sisson）※DVD、VHS版。

      - [逝者之證 (第3季)](../Page/逝者之證.md "wikilink")（Earl Brown）

      - （Vince Lombino）

#### 電影

  - 三角突擊隊系列
      - [三角突擊隊](../Page/三角突擊隊.md "wikilink")（Bobby〈Steve James 飾演〉）

      - （Kadal）※東京電視台版。
  - [骯髒哈利系列](../Page/骯髒哈利.md "wikilink")
      - [賭彩黑名單](../Page/賭彩黑名單.md "wikilink")（記者）
      - [全面追捕令](../Page/全面追捕令.md "wikilink")
      - [緊急搜捕令](../Page/緊急搜捕令.md "wikilink")（記者、機港職員）※富士電視台版。
  - [詹姆士·龐德系列](../Page/占士邦.md "wikilink")
      - [詹姆士·龐德大戰皇家賭場](../Page/007大戰皇家賭場.md "wikilink")（René
        Mathis〈Giancarlo Giannini 飾演〉）※朝日電視台版。
      - [詹姆士·龐德：金眼睛](../Page/新鐵金剛之金眼睛.md "wikilink")（Ourumov將軍〈Gottfried
        John 飾演〉）※DVD版
      - [詹姆士·龐德：勇破間諜網](../Page/鐵金剛勇破間諜網.md "wikilink")（車掌、西洋棋工作人員）※TBS新錄製版。
      - [詹姆士·龐德大戰金手指](../Page/鐵金剛大戰金手指.md "wikilink")（）※DVD、BD版。
      - [詹姆士·龐德大戰殺人狂魔](../Page/鐵金剛勇戰殺人狂魔.md "wikilink")（Joe
        Butcher博士〈Wayne Newton 飾演〉）※TBS版。
      - [詹姆士·龐德：勇破爆炸黨](../Page/鐵金剛勇破爆炸黨.md "wikilink")（Kabir Bedi as
        Gobinda〈Kabir Bedi 飾演〉）※TBS版
      - [詹姆士·龐德：量子殺機](../Page/新鐵金剛之量子殺機.md "wikilink")（René
        Mathis〈Giancarlo Giannini 飾演〉）※[King
        Records版](../Page/國王唱片.md "wikilink")。
      - [詹姆士·龐德：勇破火箭嶺](../Page/鐵金剛勇破火箭嶺.md "wikilink")（NO.4〈[Michael
        Chow](../Page/周英華.md "wikilink") 飾演〉）※TBS版。
  - [全面攻佔：倒數救援](../Page/全面攻佔：倒數救援.md "wikilink")（Edward
    Clegg陸軍參謀總長〈[Robert
    Forster](../Page/羅伯特·佛斯特.md "wikilink") 飾演〉）
      - [全面攻佔2：倫敦救援](../Page/全面攻佔2：倫敦救援.md "wikilink")
  - [頑皮豹系列](../Page/頑皮豹.md "wikilink")
      - [頑皮豹之探長的詛咒](../Page/頑皮豹之探長的詛咒.md "wikilink")（Bufoni前探長〈[Roger
        Moore](../Page/羅傑·摩爾.md "wikilink") 飾演〉）

      - （Sharki上校）※DVD、VHS版。
  - 機器戰警系列
      - [機器戰警 (1987年電影)](../Page/機器戰警_\(1987年電影\).md "wikilink")（Joe P.
        Cox）※VHS版、（律師、Clarence幫人士、記者）※朝日電視台版。

      - （Schenk博士）※朝日電視台版。

      - （Coontz〈[Stephen Root](../Page/斯蒂芬·魯特.md "wikilink")
        飾演〉）※東京電視台版。

### 電視影集

  - [大偵探波洛](../Page/大偵探波洛.md "wikilink")
    ※[NHK版](../Page/日本放送協會.md "wikilink")。

      - 第1季（警官）－第4集。
      - 第2季（刑事）－第13集。
      - 第3季（電台廣播）－第29集。
      - 第4季（Bellows巡査長、飯店人員）－第33集。
      - 第5季（記者）－第41集。

  - [犯罪心理](../Page/犯罪心理_\(電視劇\).md "wikilink")

      - 第3季（Brady刑事）－第10集。
      - 第4季（Mark署長〈Joe Regalbuto 飾演〉）－第10集。
      - 第8季（Adam Rain〈[Brad Dourif](../Page/布拉德·道里夫.md "wikilink") 飾演〉）

  - （第1季：Consolmango神父〈[Tony Plana](../Page/托尼·普蘭納.md "wikilink")
    飾演〉）－第12集。

  - [重返犯罪現場](../Page/重返犯罪現場.md "wikilink")（Mike Franks〈Muse Watson
    飾演〉）、（第2季：Cheney警部補〈John Doman 飾演〉）－第12集。

  - [重返犯罪現場：紐奧良](../Page/海軍罪案調查處：新奧爾良.md "wikilink")（第1季：Hume將軍）－第8集、（第3季：法官）

  - [神探可倫坡](../Page/神探可倫坡.md "wikilink")（Logan刑事、新聞主播）－第66集、（Fiddle）－第68集。
    ※[WOWOW版](../Page/WOWOW.md "wikilink")。

#### 動畫

  - [救難小福星](../Page/救難小福星.md "wikilink")（廣播）※新日語配音版。
  - [飛哥與小佛](../Page/飛哥與小佛.md "wikilink")（Francis Monogram少校）
      - [劇場版 飛哥與小佛：超時空之謎](../Page/飛哥與小佛電影版：超時空之謎.md "wikilink")
  - [辛普森一家](../Page/辛普森一家.md "wikilink")（Birch Barlow ）
      - [辛普森一家大電影](../Page/辛普森一家大電影.md "wikilink")（Russ Cargill）※劇院公映版。
  - [變形金剛系列](../Page/變形金剛.md "wikilink")
      - [變形金剛](../Page/變形金剛_\(卡通\).md "wikilink")（Silverbolt／Sphereon）
      - [變形金剛
        (第3季)](../Page/變形金剛_\(卡通\)#第三季.md "wikilink")（Silverbolt／Sphereon、Sinnertwin、Runamuck
        等）

### 特攝影集

  - 1981年

<!-- end list -->

  - [超人力霸王80](../Page/愛迪·奧特曼.md "wikilink")（宇宙忍者的配音）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [炎神戰隊轟音者](../Page/炎神戰隊轟音者.md "wikilink")（炎神運輸鯨無霸的配音）

  - （炎神運輸鯨無霸的配音）

  - 炎神戰隊轟音者 BONBON！BONBON！網路BONG\!\!（炎神運輸鯨無霸的配音）

<!-- end list -->

  - 2009年

<!-- end list -->

  - （炎神運輸鯨無霸的配音）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [侍戰隊真劍者VS轟音者
    銀幕BANG\!\!](../Page/侍戰隊真劍者VS轟音者_銀幕BANG!!.md "wikilink")（炎神運輸鯨無霸的配音）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [假面騎士Wizard](../Page/假面騎士Wizard.md "wikilink")（Beast Chimera的配音）
  - [假面騎士×假面騎士 鎧武 & Wizard
    天下決勝之戰國MOVIE大合戰](../Page/假面騎士×假面騎士_鎧武_&_Wizard_天下決勝之戰國MOVIE大合戰.md "wikilink")（Beast
    Chimera的配音）

### 電視劇

  - 「最棒的星期日」（旁白）

  -
  -
  - （[日本電視台版](../Page/日本電視台.md "wikilink")）

### 電影

  - [天與地](../Page/天與地_\(日本電影\).md "wikilink")（旁白）

### 天象儀

  - 「前往星星的路」（父）

  - 德島明日多夢樂園「來自宇宙的贈禮：隕石」（父）

  - 岩出山天文曆学者 名取春仲物語 ～天窺人知～（藤廣則）

### 旁白

#### 國內電視台

  - [NHK系列](../Page/日本放送協會.md "wikilink")

<!-- end list -->

  - **[NHK-BS1](../Page/NHK_BS1.md "wikilink")**
      -   - 紀錄片劇場「[紐倫堡審判](../Page/紐倫堡審判.md "wikilink")」（2007年）－歷史學者
            的日語配音 等

<!-- end list -->

  - **[NHK教育](../Page/NHK教育頻道.md "wikilink")**
      -   - 「尋找[圖坦卡蒙的寶藏](../Page/圖坦卡蒙.md "wikilink")」（2004年）
          - 「」（2005年）
          - 「魔法 ～世界口笛大～」（2005年）
          - 「誰殺了圖坦卡蒙」（2007年）
          - 「圖坦卡蒙的DNA大解析」（2012年）
          - 「蒸機車“引越"\!?」（2013年）－吉姆·米歇爾的日語配音
          - 「先立謎文明」（2013年）
          - 「神秘の天体 月〜神話から科学まで〜」（2015年）

      - [摩根費里曼之穿越蟲洞](../Page/摩根費里曼之穿越蟲洞.md "wikilink")

<!-- end list -->

  - **[NHK BS Premium](../Page/NHK_BS_Premium.md "wikilink")**
      - 皇室醜聞 ～[伊麗莎白二世的煩惱](../Page/伊麗莎白二世.md "wikilink")～（2012年）－的日語配音

<!-- end list -->

  - 其它電視台

<!-- end list -->

  - （[朝日電視台](../Page/朝日電視台.md "wikilink")）

      - 「米街 金融危機責任者」（2013年）
      - 「海怪物 」（2013年）

  - [黛安娜王妃的最後一天](../Page/黛安娜王妃.md "wikilink")（[TBS](../Page/TBS電視台.md "wikilink")）－[穆罕默德·法耶兹的日語配音](../Page/穆罕默德·法耶兹.md "wikilink")

#### 衛星電視

  - [Discovery Channel](../Page/探索頻道.md "wikilink")

<!-- end list -->

  - 希特勒帝國

  - 登上[聖母峰](../Page/珠穆朗瑪峰.md "wikilink")：極限挑戰（Discovery Channel）

  - （Discovery Channel）－主持人 韋恩·卡里尼的日語配音

<!-- end list -->

  - [國家地理頻道](../Page/國家地理頻道.md "wikilink")

<!-- end list -->

  - VIDEO『找出[阿托查號的寶藏](../Page/馬德里阿托查車站.md "wikilink")』
  - [諾亞方舟的洪水](../Page/諾亞方舟.md "wikilink")（國家地理頻道）

### 廣告

  - [萬代南夢宮娛樂](../Page/萬代南夢宮娛樂.md "wikilink") 「」（廣告旁白）

  - 全書套組（廣告旁白，1982年）

  - （電台廣告旁白，1982年）

  - [Visa廣告](../Page/Visa.md "wikilink")（[野口英世](../Page/野口英世.md "wikilink")）

  - 廣告

      - 「省」篇（冷藏庫的配音）
      - 「省中考！」篇（電器製品回收業者的配音）
      - 「省·整備燃費差」篇（指導員的配音）

  - [日立製作所](../Page/日立製作所.md "wikilink") 「日立電子コControl
    Carpet」（廣告旁白，1986年）

  - [獅王](../Page/獅王.md "wikilink") 「男語／議／」（電台廣告旁白，1975年）

  - （以團員身份，參加1984年度廣告「迷惑之輪」的拍攝）

  - [象印](../Page/象印.md "wikilink")（廣告旁白、兼演員與[岩下志麻共演](../Page/岩下志麻.md "wikilink")，2006年）

  - （廣告旁白，2009年）

  - JARO（的配音，2017年、2018年）

#### 其它

  - 「星空3次元」（2016年，）

### 電視節目

  - （在節目中介紹西村他的配音代表作《[灌籃高手](../Page/灌籃高手.md "wikilink")》角色安西教練，[朝日電視台](../Page/朝日電視台.md "wikilink")，2008年4月6日播出）

### 柏青哥、角子機

  - 柏青哥

<!-- end list -->

  - CR[蓋特機器人](../Page/蓋特機器人.md "wikilink")（帝王哥魯）

  - （劍部錫巴）

  - CR[亂馬½](../Page/亂馬½.md "wikilink")（）

### 其它

  - 新魯邦三世 紙芝居（）\[11\]

## 腳註

### 注釋

### 參考來源

## 外部連結

  - [ARTSVISION公式官網的聲優簡介](http://www.artsvision.co.jp/talent/711/)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")
[Category:旁白](../Category/旁白.md "wikilink")
[Category:ARTSVISION所屬聲優](../Category/ARTSVISION所屬聲優.md "wikilink")

1.
2.
3.
4.
5.

6.

7.

8.

9.

10.

11. 收錄在紙唱片。