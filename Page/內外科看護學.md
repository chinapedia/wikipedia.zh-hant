《**內外科看護學**》（[白話字](../Page/白話字.md "wikilink")：、[台羅](../Page/台灣閩南語羅馬字拼音方案.md "wikilink")：；[英文](../Page/英文.md "wikilink")：**），由英籍[醫師](../Page/醫師.md "wikilink")[戴仁壽](../Page/戴仁壽.md "wikilink")（G.
Gushue-Taylor）與台籍[陳大鑼先生合編](../Page/陳大鑼.md "wikilink")。於1917年10月5日在[日本](../Page/日本.md "wikilink")[橫濱印刷](../Page/橫濱.md "wikilink")，同年10月8日在[台灣發行](../Page/台灣.md "wikilink")。部份章節有[拉丁字母](../Page/拉丁字母.md "wikilink")、[華文](../Page/華文.md "wikilink")、[英文對照](../Page/英文.md "wikilink")，以[白話字編寫](../Page/白話字.md "wikilink")。全書分40章、657頁、503張圖。內文主要在講解西醫之[解剖學](../Page/解剖學.md "wikilink")、[生理學](../Page/生理學.md "wikilink")、[護理學等之](../Page/護理學.md "wikilink")[人體功能與機制的](../Page/人體.md "wikilink")[醫學專書](../Page/醫學.md "wikilink")。

## 本書主旨

本書‘第1章’開頭內文即點出其主旨：

（第1章 “身軀（身體）普通的構造”：解剖學，就是科學的一項，它的目的是要講究身軀的構造……）

該書對[台灣早年醫學](../Page/台灣.md "wikilink")[本土化的推廣](../Page/本土化.md "wikilink")、與開拓具有承先啟後的[歷史地位](../Page/歷史.md "wikilink")。

## 參見

  - [解剖學](../Page/解剖學.md "wikilink")
  - [生理學](../Page/生理學.md "wikilink")
  - [護理學](../Page/護理學.md "wikilink")

## 參考文獻

  - Gushue-Taylor,G., Tân Toā-lô,《Lāi-goā-kho Khàn-hō͘-ha̍k (The
    Principles and Practice of
    Nursing)》,[台南](../Page/台南.md "wikilink"),[台灣](../Page/台灣.md "wikilink"),1917年。

## 外部連結

  - [內外科看護學（Tēⁿ Si-chong 鄭詩宗）](http://lgkkhanhouhak.blogspot.com/)
  - [《內外科看護學》成書經過-賴永祥講書（014）](http://www.laijohn.com/works/kangsu/14.htm)

[category:醫學書籍](../Page/category:醫學書籍.md "wikilink")

[Category:閩南語著作](../Category/閩南語著作.md "wikilink")
[Category:臺灣醫學教育](../Category/臺灣醫學教育.md "wikilink")
[Category:臺灣日治時期出版品](../Category/臺灣日治時期出版品.md "wikilink")
[Category:1917年書籍](../Category/1917年書籍.md "wikilink")
[Category:台灣白話字出版物](../Category/台灣白話字出版物.md "wikilink")
[Category:白話字著作](../Category/白話字著作.md "wikilink")