**T细胞**（、T淋巴細胞/）是[淋巴细胞的一种](../Page/淋巴细胞.md "wikilink")，在[免疫反應中扮演着重要的角色](../Page/免疫反應.md "wikilink")。T细胞在胸腺内分化成熟，成熟后移居于周围[淋巴组织中](../Page/淋巴组织.md "wikilink")。T是“[胸腺](../Page/胸腺.md "wikilink")”（thymus）而不是[甲狀腺](../Page/甲狀腺.md "wikilink")（thyroid）的[英文](../Page/英文.md "wikilink")[缩写](../Page/缩写.md "wikilink")。T细胞膜表面分子与T细胞的功能相关，也是T细胞的[表面标志](../Page/表面标志.md "wikilink")（cell-surface
marker），可以用以分离、鉴定不同亚群的T细胞。

## 分类

T细胞按照功能和表面标志可以分成很多种类：

  - [细胞毒性T细胞](../Page/细胞毒性T细胞.md "wikilink")（或殺手T细胞、胞殺T細胞）（cytotoxic T
    cell）：消灭受感染或突變的细胞。这些细胞的功能就像一个“杀手”或细胞毒素那样，因为它们可以对产生特殊[抗原反应的目标细胞进行杀灭](../Page/抗原.md "wikilink")。细胞毒性T细胞的主要表面标志是[CD8](../Page/CD8受體.md "wikilink"),也被稱為殺手T細胞。
  - [辅助T细胞](../Page/辅助T细胞.md "wikilink")（helper T
    cell）在免疫反应中扮演中间过程的角色：它可以增生扩散来激活其它类型的产生直接免疫反应的免疫细胞。辅助T细胞的主要表面标志是[CD4](../Page/CD4受體.md "wikilink")。T细胞调控或“辅助”其它淋巴细胞发挥功能。它们是已知的[HIV病毒的目標细胞](../Page/人体免疫缺陷病毒.md "wikilink")，在[艾滋病发病时会急剧减少](../Page/艾滋病.md "wikilink")。協助活化B細胞產生抗體，也可協助殺傷性T細胞及巨噬細胞發揮免疫功能。輔助T細胞也可以分泌細胞激素(cytokines)活化B細胞導向體液型免疫反應，幫助B細胞分化成漿細胞(plasma
    cell)，或是增強巨噬細胞(macrophage)的吞噬功能(endocytosis)。
  - [调节/抑制T细胞](../Page/调节/抑制T细胞.md "wikilink")（regulatory/suppressor T
    cell）：负责调节机体免疫反应。通常起着维持自身耐受和避免免疫反应过度损伤机体的重要作用。调节／抑制T细胞有很多种，目前研究最活跃的是CD25+[CD4](../Page/CD4受體.md "wikilink")+T细胞。對各種T細胞和B細胞都有抑制作用，調節和控制免疫反應，維持免疫自穩性（即免疫耐受性）。
  - [记忆T细胞](../Page/记忆T细胞.md "wikilink")（memory T
    cell）：在再次[免疫反應中起重要作用](../Page/免疫反應.md "wikilink")。记忆T细胞由于暂时没有非常特异的表面标志，目前还有很多未知之处。連同記憶性B細胞一起，是在抗原刺激後，保存特異抗原訊息的淋巴細胞，壽命可長達數十年。當它們再次接受與原來相同的抗原刺激後，就可以分增殖為對付抗原的功能性T細胞或能產生抗體的漿細胞。

## 参见

  - [白细胞](../Page/白细胞.md "wikilink")

## 参考文献

## 外部連結

  - \[<http://www.ncbi.nlm.nih.gov/books/bv.fcgi?call=bv.View>..ShowTOC\&rid=imm.TOC\&depth=2
    Immunobiology, 5th Edition\]

  - [niaid.nih.gov](https://web.archive.org/web/20070103005411/http://www.niaid.nih.gov/Publications/immune/the_immune_system.pdf)
    – The Immune System

  - [T-cell Group – Cardiff University](http://www.tcells.org)

  - [(Successful\!) Treatment of Metastatic Melanoma with Autologous
    CD4+ T Cells against
    NY-ESO-1](http://content.nejm.org/cgi/content/full/358/25/2698).

  - [The Center for Modeling Immunity to Enteric Pathogens
    (MIEP)](http://www.modelingimmunity.org)

  -
{{-}}

[T细胞](../Category/T细胞.md "wikilink")