[缩略图](https://zh.wikipedia.org/wiki/File:Patriarch_John_X_of_Antioch_2.jpg "fig:缩略图")

**安条克希腊正教会**，又译**安提阿正教会**，是[东正教最古老的](../Page/东正教.md "wikilink")[教会之一](../Page/教会.md "wikilink")，其领袖拥有[牧首称号](../Page/牧首.md "wikilink")，称“[安条克和全东方基督徒的宗主教](../Page/安条克牧首.md "wikilink")”。按照[历史荣誉](../Page/历史荣誉.md "wikilink")，安提阿东正教会在16个[东正教自主教会中排名第三](../Page/东正教自主教会.md "wikilink")，仅次于[君士坦丁堡教会和](../Page/君士坦丁堡教会.md "wikilink")[亚历山大教会](../Page/亚历山大教会.md "wikilink")。

[安提阿教会早在](../Page/安提阿.md "wikilink")[基督教建立之时代](../Page/基督教.md "wikilink")（1世纪）便已存在。根据传统说法，安提阿的[主教教座由](../Page/主教教座.md "wikilink")[圣伯多禄本人所立](../Page/圣伯多禄.md "wikilink")。由于安提阿是接受基督教的[罗马帝国在东方的重镇](../Page/罗马帝国.md "wikilink")，其主教很快就与[罗马](../Page/罗马.md "wikilink")、[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")、[亚历山大和](../Page/亚历山大.md "wikilink")[耶路撒冷的](../Page/耶路撒冷.md "wikilink")[主教并列](../Page/主教.md "wikilink")，成为[教会内最受尊敬的人物](../Page/教会.md "wikilink")。451年[卡尔西顿会议之后](../Page/卡尔西顿会议.md "wikilink")，安提阿的主教获牧首称号，其辖区称安提阿牧首区。在[阿拉伯征服之前](../Page/阿拉伯征服.md "wikilink")，该牧首区包括[叙利亚](../Page/叙利亚.md "wikilink")、[安纳托利亚和](../Page/安纳托利亚.md "wikilink")[美索不达米亚](../Page/美索不达米亚.md "wikilink")。但至今仍然保佑那些領土的教會治權（如果正教會重新再立於那些穆斯林國家的話）。

今天，安提阿教会管辖着3个[都主教区](../Page/都主教.md "wikilink")，22个[主教区](../Page/主教.md "wikilink")，以及北美的很多小教会和[教堂](../Page/教堂.md "wikilink")。現今的牧首驻地在[叙利亚首都](../Page/叙利亚.md "wikilink")[大马士革](../Page/大马士革.md "wikilink")。在安提阿教会内的信徒，大多为[阿拉伯人](../Page/阿拉伯人.md "wikilink")。

## 参考文献

## 参见

  - [安条克牧首](../Page/安条克牧首.md "wikilink")
  - [东正教自主教会](../Page/东正教自主教会.md "wikilink")

{{-}}

[Category:东正教自主教会](../Category/东正教自主教会.md "wikilink")
[Category:以大馬士革為根據地的組織](../Category/以大馬士革為根據地的組織.md "wikilink")