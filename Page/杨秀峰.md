**杨秀峰**（）中国[直隶](../Page/直隶省.md "wikilink")[迁安](../Page/迁安县.md "wikilink")（今属[河北省](../Page/河北省.md "wikilink")[唐山市](../Page/唐山市.md "wikilink")）人，教育家，法学家。

## 生平

早年参加了[五四运动](../Page/五四运动.md "wikilink")。1929年赴[法国留学](../Page/法国.md "wikilink")。1930年加入[中国共产党](../Page/中国共产党.md "wikilink")。曾参加领导[留法学生反帝同盟](../Page/留法学生反帝同盟.md "wikilink")。后来转赴[苏联学习](../Page/苏联.md "wikilink")。1934年回国，他在[河北法商学院](../Page/河北法商学院.md "wikilink")、[北平师范大学](../Page/北平师范大学.md "wikilink")、[北平中国大学](../Page/北平中国大学.md "wikilink")、[东北大学等校任教](../Page/东北大学_\(中国\).md "wikilink")，以大学教授的公开身分，从事革命活动。参与发起和领导华北各界救国会。历任[冀西抗日游击队司令员](../Page/冀西抗日游击队.md "wikilink")，[河北抗战学院院长](../Page/河北抗战学院.md "wikilink")，[冀南行署主任](../Page/冀南行署.md "wikilink")，[中共冀南区党委常委](../Page/中共冀南区党委.md "wikilink")，[冀南、太行、太岳行政联合办事处主任](../Page/冀南、太行、太岳行政联合办事处.md "wikilink")，[晋冀鲁豫边区政府主席](../Page/晋冀鲁豫边区.md "wikilink")，[中共晋冀鲁豫中央局常委](../Page/中共晋冀鲁豫中央局.md "wikilink")，[华北人民政府副主席](../Page/华北人民政府.md "wikilink")。

中华人民共和国建国后，历任[河北省人民政府主席](../Page/河北省人民政府.md "wikilink")，[高等教育部](../Page/中华人民共和国高等教育部.md "wikilink")、[教育部部长](../Page/中华人民共和国教育部.md "wikilink")，[国务院文教办公室副主任](../Page/国务院文教办公室.md "wikilink")，[最高人民法院院长](../Page/最高人民法院院长.md "wikilink")，[全国政协副主席](../Page/全国政协副主席.md "wikilink")、[全国人大常委会委员](../Page/全国人大常委会.md "wikilink")、[全国人大法制委员会副主任](../Page/全国人大法制委员会.md "wikilink")，[中华教育学会和](../Page/中华教育学会.md "wikilink")[中国法学会名誉会长](../Page/中国法学会.md "wikilink")。是中共第八届中央委员。

儿子[杨为民](../Page/杨为民.md "wikilink")。

## 参考文献

{{-}}

[Category:中共中央顾问委员会委员](../Category/中共中央顾问委员会委员.md "wikilink")
[Category:第五屆全國政協副主席](../Category/第五屆全國政協副主席.md "wikilink")
[Category:全国人大常委会委员](../Category/全国人大常委会委员.md "wikilink")
[Category:中華人民共和國教育部部長](../Category/中華人民共和國教育部部長.md "wikilink")
[Category:中国共产党党员
(1930年入党)](../Category/中国共产党党员_\(1930年入党\).md "wikilink")
[Category:北京師範大學教授](../Category/北京師範大學教授.md "wikilink")
[Category:国际列宁学校校友](../Category/国际列宁学校校友.md "wikilink")
[Category:迁安人](../Category/迁安人.md "wikilink")
[X秀峰](../Category/楊姓.md "wikilink")