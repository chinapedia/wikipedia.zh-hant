[Labiodentaal.svg](https://zh.wikipedia.org/wiki/File:Labiodentaal.svg "fig:Labiodentaal.svg")

**唇齒音**（古漢語：[輕唇音](../Page/三十六字母.md "wikilink")）為[發音部位的一種](../Page/發音部位.md "wikilink")，藉由[唇與](../Page/唇.md "wikilink")[齒的咬合而發出的](../Page/齒.md "wikilink")[輔音](../Page/輔音.md "wikilink")（子音）。

## 唇齒音在IPA上的表示

<table>
<thead>
<tr class="header">
<th><p>IPA</p></th>
<th><p>IPA（圖像）</p></th>
<th><p>名稱</p></th>
<th><p>例子</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>語言</p></td>
<td><p>原文</p></td>
<td><p>發音</p></td>
<td><p>意思</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:IPA_Unicode_0x0070+0x032A.svg" title="fig:IPA_Unicode_0x0070+0x032A.svg">IPA_Unicode_0x0070+0x032A.svg</a></p></td>
<td><p><a href="../Page/清唇齒塞音.md" title="wikilink">清唇齒塞音</a></p></td>
<td><p><a href="../Page/希臘語.md" title="wikilink">希臘語</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:IPA_Unicode_0x0062+0x032A.svg" title="fig:IPA_Unicode_0x0062+0x032A.svg">IPA_Unicode_0x0062+0x032A.svg</a></p></td>
<td><p><a href="../Page/濁唇齒塞音.md" title="wikilink">濁唇齒塞音</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><big></big></p></td>
<td><p><a href="../Page/清唇齒塞擦音.md" title="wikilink">清唇齒塞擦音</a></p></td>
<td><p><a href="../Page/德语.md" title="wikilink">德语</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><big></big></p></td>
<td><p><a href="../Page/濁唇齒塞擦音.md" title="wikilink">濁唇齒塞擦音</a></p></td>
<td><p><a href="../Page/宗加語.md" title="wikilink">宗加語XiNkuna方言</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xsampa-F.png" title="fig:File:Xsampa-F.png"><a href="File:Xsampa-F.png">File:Xsampa-F.png</a></a></p></td>
<td><p><a href="../Page/唇齒鼻音.md" title="wikilink">唇齒鼻音</a></p></td>
<td><p><a href="../Page/英語.md" title="wikilink">英語</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xsampa-f2.png" title="fig:File:Xsampa-f2.png"><a href="File:Xsampa-f2.png">File:Xsampa-f2.png</a></a></p></td>
<td><p><a href="../Page/清唇齒擦音.md" title="wikilink">清唇齒擦音</a></p></td>
<td><p><a href="../Page/汉语.md" title="wikilink">汉语</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xsampa-v.png" title="fig:File:Xsampa-v.png"><a href="File:Xsampa-v.png">File:Xsampa-v.png</a></a></p></td>
<td><p><a href="../Page/濁唇齒擦音.md" title="wikilink">濁唇齒擦音</a></p></td>
<td><p><a href="../Page/英語.md" title="wikilink">英語</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xsampa-Porvslash.png" title="fig:File:Xsampa-Porvslash.png"><a href="File:Xsampa-Porvslash.png">File:Xsampa-Porvslash.png</a></a></p></td>
<td><p><a href="../Page/唇齒近音.md" title="wikilink">唇齒近音</a></p></td>
<td><p><a href="../Page/荷蘭語.md" title="wikilink">荷蘭語</a></p></td>
</tr>
</tbody>
</table>

## 參見

  - [发音部位](../Page/发音部位.md "wikilink")
  - [語音學主題索引](../Page/語音學主題索引.md "wikilink") ([Index of phonetics
    articles](../Page/:en:Index_of_phonetics_articles.md "wikilink"))
  - [齒唇音](../Page/齒唇音.md "wikilink")([Dentolabial
    consonant](../Page/:en:Dentolabial_consonant.md "wikilink"))

## 參考文獻

  - Ladefoged, Peter; Ian Maddieson (1996). The Sounds of the World's
    Languages. Oxford: Blackwell. ISBN 0-631-19814-8.

[Category:輔音](../Category/輔音.md "wikilink")
[Category:調音部位](../Category/調音部位.md "wikilink")