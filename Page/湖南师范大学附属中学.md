**湖南师大附中**（全称**[湖南师范大学附属中学](../Page/湖南师范大学.md "wikilink")**），是一所公立全日制普通[高中](../Page/高中.md "wikilink")，为湖南省直属重点中学。湖南师范大学附属中学是国家教育部基础教育司“国家级示范性普通高中建设”项目执行学校、湖南省实施素质教育的示范学校。湖南师大附中和[长沙市第一中学](../Page/长沙市第一中学.md "wikilink")，[雅礼中学](../Page/雅礼中学.md "wikilink")，[长郡中学并称为](../Page/长郡中学.md "wikilink")[长沙四大名校](../Page/长沙四大名校.md "wikilink")\[1\]。

## 历史沿革

[位于湖南师大附中内的禹之谟雕像](https://zh.wikipedia.org/wiki/File:Yuzhimo_diaoxiang.jpg "fig:位于湖南师大附中内的禹之谟雕像")

  - 1905年，晚清革命者[禹之谟创惟一学堂](../Page/禹之谟.md "wikilink")。
  - 1912年（民国元年），更名为广益中学。
  - 1924年，更名为执中中学。
  - 1926年，更名为湖南私立广益中学。
  - 1951年11月，改名为湖南省立广益中学。
  - 1952年10月，湖南省长沙市第十初级中学并入，并更名为湖南省长沙市第四中学 。
  - 1955年1月，成为湖南师范学院附属中学。
  - 1960年8月，改名为湖南师范学院附属实验中学。
  - 1963年8月，复名湖南师范学院附属中学 。
  - 1968年12月，改名为长沙市纺织厂“五·七”中学 。
  - 1969年11月，复名湖南师范学院附属中学。
  - 1984年9月，更名湖南师范大学附属中学。
  - 1997年，创办湖南广益实验中学。
  - 2005年，湖南师范大学附属中学被确认为首批湖南省示范性普通高级中学。
  - 2008年，创办湖南师范大学附属中学海口中学、湖南师大附中星城实验中学。
  - 2009年，创办湖南师大附中博才实验中学。
  - 2012年，创办湖南师大附中高新实验中学。
  - 2014年，创办湖南师大附中耒阳分校、湖南师大附中梅溪湖中学。

## 著名校友

### 政治人物

  - [李立三](../Page/李立三.md "wikilink")
  - [朱鎔基](../Page/朱鎔基.md "wikilink")\[2\] \[3\]
  - [柳直荀](../Page/柳直荀.md "wikilink")

### 两院院士

  - [朱作言](../Page/朱作言.md "wikilink")，[中国科学院院士](../Page/中国科学院.md "wikilink")，[第三世界科学院院士](../Page/第三世界科学院.md "wikilink")，细胞及发育生物学家。
  - [中国工程院院士](../Page/中国工程院.md "wikilink")“黎氏三兄弟”（黎鳌、黎介寿、黎磊石）
  - [张履谦](../Page/张履谦.md "wikilink")，雷达专家。
  - [朱之悌](../Page/朱之悌.md "wikilink")，[中国工程院院士](../Page/中国工程院.md "wikilink")，[北京林业大学教授](../Page/北京林业大学.md "wikilink")，[林木育种专家](../Page/林木育种.md "wikilink")，[毛白杨专家](../Page/毛白杨.md "wikilink")。
  - [何继善](../Page/何继善.md "wikilink")，应用[地球物理学家](../Page/地球物理.md "wikilink")。博士生导师，1994年当选为中国工程院院士。现任湖南省科协主席，中国工程院能源与矿业学部副主任、工程管理学部常委，第四届国家安全生产专家组综合组组长,中国地球物理学会副理事长、中国有色金属学会副理事长、国家教委科技委员会委员、美国勘探地球物理学家协会（SEG）会员。

### 作家

  - [康濯](../Page/康濯.md "wikilink")，湖南省[文联主席](../Page/文联.md "wikilink")，[中国作协书记处书记](../Page/中国作协.md "wikilink")，[现代作家](../Page/现代作家.md "wikilink")。

### 演藝人士

  - [欧阳予倩](../Page/欧阳予倩.md "wikilink")，中国戏剧家。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，欧阳予倩出任[中央戏剧学院院长等职](../Page/中央戏剧学院.md "wikilink")。
  - [何炅](../Page/何炅.md "wikilink")，知名[主持人](../Page/主持人.md "wikilink")，目前主持的最为观众所熟知的节目包括[湖南卫视的](../Page/湖南卫视.md "wikilink")《[快樂大本營](../Page/快樂大本營.md "wikilink")》、《[勇往直前](../Page/勇往直前.md "wikilink")》、《[背后的故事](../Page/背后的故事.md "wikilink")》、《[快樂男聲](../Page/快樂男聲.md "wikilink")》、《[舞動奇跡](../Page/舞動奇跡.md "wikilink")》和[吉林卫视](../Page/吉林卫视.md "wikilink")《[超级乐8点](../Page/超级乐8点.md "wikilink")》等，同时担任[北京外國語大學](../Page/北京外國語大學.md "wikilink")[阿拉伯語系](../Page/阿拉伯語.md "wikilink")“阿拉伯国家概况”一科[教师](../Page/教师.md "wikilink")。另有[歌手](../Page/歌手.md "wikilink")（代表作《栀子花开》）及[演員身份](../Page/演員.md "wikilink")。
  - [龙丹妮](../Page/龙丹妮.md "wikilink")，知名[制作人](../Page/制作人.md "wikilink")，与何炅同届。1994年毕业于浙江传媒学院，1995年成为湖南经济电视台的第一批员工。曾制作《真情对对碰》、《越策越开心》、《绝对男人》、《明星学院》等多档省内外知名娱乐栏目。2008年9月至2017年2月，担任天娱传媒总经理。
  - [李维嘉](../Page/李维嘉.md "wikilink")，[湖南卫视](../Page/湖南卫视.md "wikilink")《[快乐大本营](../Page/快乐大本营.md "wikilink")》主持人。
  - [张艺兴](../Page/张艺兴.md "wikilink")，韩国S.M.Entertainment旗下偶像组合EXO及其分队EXO-M成员，舞蹈擔當，该组合于2012年4月8日出道。于2015年湖南师大附中建校110周年之际，为感念母校栽培，特别设立张艺兴奖学金，未来将每年捐款用以激励有梦想的学子追求艺术梦想。
  - [易烊千玺](../Page/易烊千玺.md "wikilink")，人气少年偶像组合[TFBOYS成员](../Page/TFBOYS.md "wikilink")，组合中的[舞蹈担当](../Page/舞蹈担当.md "wikilink")，该组合于2013年8月6日出道。2005年童星出道，2009年加入[飞炫少年组合](../Page/飞炫少年.md "wikilink")，2011年年底退出。后参加《[向上吧！少年](../Page/向上吧！少年.md "wikilink")》，成为日后加入[TFBOYS的契机](../Page/TFBOYS.md "wikilink")。現录取[中央戏剧学院](../Page/中央戏剧学院.md "wikilink")。

## 参见

  - [湖南师范大学](../Page/湖南师范大学.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [湖南师大附中主頁](http://www.hnsdfz.org)

[Category:长沙教育](../Category/长沙教育.md "wikilink")
[Category:湖南中等教育](../Category/湖南中等教育.md "wikilink")
[Category:湖南师范大学](../Category/湖南师范大学.md "wikilink")
[Category:1905年創建的教育機構](../Category/1905年創建的教育機構.md "wikilink")
[Category:岳麓区](../Category/岳麓区.md "wikilink")
[Category:1905年中國建立](../Category/1905年中國建立.md "wikilink")

1.  《湖南长沙四大名校08高考51人保送北大清华》
    星辰在线--《长沙晚报》2008年02月21日<http://edu.sina.com.cn/gaokao/2008-02-21/1038123385.shtml>
2.  刘磊 彭鸣皋，《朱镕基和他的同学们:历经曲折坎坷
    始终心不能忘》，《南方周末》2003年3月8日<http://news.eastday.com/epublish/gb/paper148/20030308/class014800018/hwz899754.htm>

3.  张立，《朱镕基中学时代:能背圆周率到100位的沉默少年》，《南方周末》2003年3月8日<http://news.sohu.com/60/83/news207178360.shtml>