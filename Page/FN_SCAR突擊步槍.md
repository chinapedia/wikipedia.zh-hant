**特種部隊戰鬥突擊步槍**（[英文](../Page/英文.md "wikilink")：**[Special operations
forces](../Page/特種部隊.md "wikilink") Combat Assault
Rifle**，[縮寫](../Page/縮寫.md "wikilink")：**SCAR**）\[1\]，是[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale公司為了滿足](../Page/Fabrique_Nationale.md "wikilink")[美国特种作戰司令部的SCAR標案而製造的現代步槍](../Page/美国特种作戰司令部.md "wikilink")，此槍族有兩種主要版，一種是使用[5.56×45
NATO的SCAR](../Page/5.56×45mm_NATO.md "wikilink")-L（輕型版），和使用[7.62×51
NATO的SCAR](../Page/7.62×51mm_NATO.md "wikilink")-H（重型版），兩種都可以改裝成「狙擊型態」或「近戰型態」（Close
Quarters Combat），FN SCAR首次於2007年7月開始小批量量產以有限配發部隊。\[2\]

## 特徵

SCAR可以有兩種口徑，每種都能換裝成狙擊和[近身距離作戰型態](../Page/近身距離作戰.md "wikilink")，因此2003年贏得US
SOCOM的特戰部隊新型槍支標案，其他競爭者有[柯爾特和](../Page/柯爾特.md "wikilink")[Robinson
Arms公司](../Page/Robinson_Arms.md "wikilink")。

[Heckler &
Koch公司則提出了](../Page/黑克勒-科赫.md "wikilink")[XM8突擊步槍外掛榴彈發射器以全面取代美國陸軍的](../Page/XM8突擊步槍.md "wikilink")[M4卡賓槍和](../Page/M4卡賓槍.md "wikilink")[M16突擊步槍方案](../Page/M16突擊步槍.md "wikilink")（包括特種部隊），但是此案後來被取消。

SCAR的兩種版本；**輕型（Light，SCAR-L，Mk 16 Mod 0）**和**重型（Heavy，SCAR-H，Mk 17 Mod
0）**。L型發射[5.56×45毫米北約彈藥](../Page/5.56×45mm_NATO.md "wikilink")，使用[M16的彈匣](../Page/M16突擊步槍.md "wikilink")（[STANAG彈匣](../Page/STANAG彈匣.md "wikilink")），H型發射火力更大的[7.62×51毫米北約彈藥使用專用的](../Page/7.62×51mm_NATO.md "wikilink")20發彈匣（修改自[FN
FAL彈匣](../Page/FN_FAL.md "wikilink")）\[3\]，不同槍管長度可以用於近戰和狙擊模式，SCAR-L還能使用6.8毫米彈藥，最初測試發現SCAR-H應該也能用[7.62×39毫米的](../Page/7.62×39mm.md "wikilink")[M43彈匣](../Page/M43.md "wikilink")，然而FN原廠並不建議這種方式。

直到2015年10月，FN公司终于正式展出了他们自己生产的7.62×39mm口径的SCAR，这是以Mk17
SCAR-H的上机匣，安装一个可以直接使用7.62×39mm弹匣的下机匣，并更换枪管、枪机等部件组成，但將原有SCAR槍型的「插口式」下机匣安裝方式改為「卡口式」以使用7.62×39mm彈匣，甚至还能直接通用[AK-47的弹匣](../Page/AK-47突击步枪.md "wikilink")。\[4\]

Mk 16 Mod 0預計要替換[M4A1](../Page/M4卡賓槍.md "wikilink")、[Mk
18近戰步槍](../Page/CQBR.md "wikilink")（CQBR）和[Mk12
SPR](../Page/Mk_12特別用途步槍.md "wikilink")，同時於SOCOM服役，Mk 17 Mod
0預計替換[M14和](../Page/M14自动步枪.md "wikilink")[Mk
11狙擊步槍](../Page/SR-25狙擊步槍.md "wikilink")。

SCAR的特徵為從頭到尾不間斷的戰術導軌在鋁製外殼的正上方排開，兩條可拆式導軌在側面，下方還可掛任何MIL-STD-1913標準的相容配件，握把部分可和M16互換，彈匣和彈匣釋放鈕都亦與M16相同，前準星可以折下，不會擋到瞄準鏡或是光學瞄準器，此槍使用的「Tappet式」的氣體閉鎖系統類似早期[M1卡宾枪](../Page/M1卡宾枪.md "wikilink")，反而不類似其競爭對手[Stoner
63或](../Page/斯通納63武器系統.md "wikilink")[HK
G36等現代突擊步槍](../Page/HK_G36突擊步槍.md "wikilink")。

SCAR由FN位於南[加州](../Page/加州.md "wikilink")[哥倫比亞廠製造](../Page/哥倫比亞.md "wikilink")，2008年，FN公佈計畫把半自動的SCAR販售到美國民間市場。

2007年9月至11月期間美國陸軍進行一項沙塵測試，於「亞伯丁測試場」由[M4卡賓槍](../Page/M4卡賓槍.md "wikilink")、FN
SCAR、[HK416和已停止開發的](../Page/HK416突擊步槍.md "wikilink")[HK
XM8](../Page/XM8突擊步槍.md "wikilink")，這四種槍型各取10支，總計發射60,000發，用來評定M4步槍的槍械性能。
 該次測試中，SCAR卡彈226次，遜於XM8的127次卡彈，但是贏過M4的882次卡彈(彈匣故障239次)和HK
416的233次，此測試結果和之前對現行M4和M16的2006年測試結果一樣；和2007年秋季三強限定標之前的一次小測試也一樣，第二次測試時M4加強潤滑後減為307次卡彈，但是一調到相同條件下現有M4還是卡彈882次上下。\[5\]\[6\]SCAR被認為可取代M4步槍的候選之一。\[7\]\[8\]

<table>
<caption>SCAR槍管全長</caption>
<thead>
<tr class="header">
<th><p>模式</p></th>
<th><p>Mk 16 Mod 0</p></th>
<th><p>Mk 17 Mod 0</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>近戰模式</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>標準模式</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>狙擊／長槍管模式</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外掛榴彈發射器

2004年出現的**EGLM**又稱**Mk 13 Mod 0**，是一種改良自[FN
F2000設計的](../Page/FN_F2000突擊步槍.md "wikilink")[40毫米榴彈發射器](../Page/40毫米榴彈.md "wikilink")，EGLM可以直接單手按發榴彈而不會被中央的子彈彈匣擋住，EGLM的發射管左右皆可裝彈，可以掛於步槍下或是利用配件改裝成獨立式榴彈發射器。

## IAR衍生型

2008年，FN SCAR的一種衍生型，熱適應模塊化步槍（Heat Adaptive Modular
Rifle，HAMR），是步兵自動步槍（Infantry Automatic
Rifle，IAR）計劃的競標上最後一輪之中的四枝競標的步槍之一。步兵自動步槍是[美国海军陆战队在班用用途的輕型自動步槍方面的招標](../Page/美国海军陆战队.md "wikilink")。\[9\]FN的樣槍是以FN
SCAR作為基礎改進而成，它結合了和[開放式槍機操作](../Page/開放式槍機.md "wikilink")，在[全自動射擊時會將槍機轉換成開放式操作](../Page/自動火器.md "wikilink")，以防止全自動射擊時槍管升溫過快。此前的槍械設計師已經可以令槍支混合了閉鎖/開放式槍機兩種操作模式，但會自動地以溫度為基礎的操作模式開關是一個創新。預期IAR的競爭會令美國海軍陸戰隊在超過5年採購高達6,500枝自動步槍。\[10\]不過，最後競標之中得標的是[黑克勒-科赫的](../Page/黑克勒-科赫.md "wikilink")[HK416的衍生型](../Page/HK416突擊步槍.md "wikilink")，[HK
IAR](../Page/HK_M27步兵自動步槍.md "wikilink")，並且被美国海军陆战队命名為M27
IAR。\[11\]\[12\]\[13\]

## Mk 20狙擊支援步槍

**Mk 20狙擊支援步槍**（，簡稱：SSR）是以7.62毫米[口徑的FN](../Page/口徑.md "wikilink")
SCAR-H（Mk 17）自動步槍為其基礎改進而成的[精確射手步槍](../Page/精確射手步槍.md "wikilink")。

## 美國採用

[1stbat75thregSCAR.jpg](https://zh.wikipedia.org/wiki/File:1stbat75thregSCAR.jpg "fig:1stbat75thregSCAR.jpg")攜行SCAR突擊步槍\]\]
2004年1月23日，US SOCOM發出提案意見書（RFP）USZA22-04-R-0001，採購如下：

<table>
<thead>
<tr class="header">
<th><p>形式</p></th>
<th><p>工程測試用</p></th>
<th><p>低量生產數（LRIP）</p></th>
<th><p>量產數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SCAR-L（輕型版）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>標準模式<br />
（Standard）</p></td>
<td><p>12</p></td>
<td><p>250</p></td>
<td><p>83,738</p></td>
</tr>
<tr class="odd">
<td><p>近戰模式<br />
（Close Quarters Combat，CQC）</p></td>
<td><p>6</p></td>
<td><p>80</p></td>
<td><p>27,914</p></td>
</tr>
<tr class="even">
<td><p>狙擊／長槍管模式<br />
（Sniper Variant，SV／Long Barrel，LB）</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td><p>11,989</p></td>
</tr>
<tr class="odd">
<td><p>SCAR-H（重型版）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>標準模式（Standard）</p></td>
<td><p>1</p></td>
<td><p>68</p></td>
<td><p>14,931</p></td>
</tr>
<tr class="odd">
<td><p>近戰模式<br />
（Close Quarters Combat，CQC）</p></td>
<td><p>0</p></td>
<td><p>10</p></td>
<td><p>6,990</p></td>
</tr>
<tr class="even">
<td><p>狙擊／長槍管模式<br />
（Sniper Variant，SV／Long Barrel，LB）</p></td>
<td><p>0</p></td>
<td><p>10</p></td>
<td><p>11,990</p></td>
</tr>
<tr class="odd">
<td><p>標準模式<br />
（Standard）<br />
（7.62×51）</p></td>
<td><p>0</p></td>
<td><p>68</p></td>
<td><p>2,932</p></td>
</tr>
</tbody>
</table>

## 使用國

  -   - [阿富汗國家安全局](../Page/國家安全局_\(阿富汗\).md "wikilink")

  -   - \[14\]

      - [比利時國防軍](../Page/比利時國防軍.md "wikilink")（L型；正逐步取代[FN
        FNC](../Page/FN_FNC突擊步槍.md "wikilink")）\[15\]\[16\]

      - [特種部隊群](../Page/特種部隊群.md "wikilink")（L型、H型）

  -   - 特別調查和保護局（L型、H型）

  -   - （L型、H型）\[17\]

  -
  -   - [芬蘭陸軍](../Page/芬蘭陸軍.md "wikilink")（L型）\[18\]

  -   - \[19\]

      - \[20\]

  -   - [格魯吉亞武裝部隊](../Page/格魯吉亞軍.md "wikilink")\[21\]

  -   - [德國聯邦警察](../Page/德國聯邦警察.md "wikilink")[第九國境守備隊](../Page/德國聯邦警察第九國境守備隊.md "wikilink")（L型）\[22\]

      - [特別行動突擊隊](../Page/特別行動突擊隊.md "wikilink")\[23\]

      - 德國州警察機動突擊隊\[24\]

  -   - 洪都拉斯陸軍第1特種部隊營

  -   - [特別邊防部隊](../Page/特別邊防部隊.md "wikilink")

  -   - [義大利陸軍](../Page/義大利陸軍.md "wikilink")

  -   - [日本陸上自衛隊](../Page/日本陸上自衛隊.md "wikilink")[特殊作戰群](../Page/陸上自衛隊特殊作戰群.md "wikilink")\[25\]

  -   - 特種部隊單位（H型）\[26\]

  -   - [大韓民國陸軍](../Page/大韓民國陸軍.md "wikilink")[第707特殊任務營](../Page/第707特殊任務營.md "wikilink")（L型）\[27\]

  -   - （H型）\[28\]

  -   - [馬來西亞皇家警察](../Page/馬來西亞皇家警察.md "wikilink")[特別行動指揮部](../Page/特別行動指揮部.md "wikilink")（H型）

  -
  -   - 特種部隊營（L型、H型）

  -   - [秘魯軍隊特種部隊群](../Page/秘魯軍事.md "wikilink")（L型、H型）\[29\]

      - \[30\]

  -   - [菲律賓海軍陸戰隊](../Page/菲律賓海軍陸戰隊.md "wikilink")

  -   - [波蘭武裝部隊](../Page/波蘭軍隊.md "wikilink")[行動應變及機動組](../Page/行動應變及機動組.md "wikilink")

      - \[31\]

  -   - （L型、H型）\[32\]\[33\]

  -   - [新加坡警察部隊](../Page/新加坡警察部隊.md "wikilink")\[34\]

  -   - 特種部隊單位

  -   - [泰國皇家陸軍](../Page/泰國皇家陸軍.md "wikilink")（L型）

      - （L型、H型）\[35\]

  -   - \[36\]\[37\]

  -   - [英國陸軍](../Page/英國陸軍.md "wikilink")[特種空勤團](../Page/特種空勤團.md "wikilink")（H型）\[38\]\[39\]
      - [英國海軍](../Page/英國海軍.md "wikilink")[特別舟艇隊](../Page/特別舟艇隊.md "wikilink")（H型）

  -   - [美國特種作戰司令部](../Page/美國特種作戰司令部.md "wikilink")（L型、H型）

      - [美國海關及邊境保衛局](../Page/美國海關及邊境保衛局.md "wikilink")\[40\]

      - [洛杉磯警察局](../Page/洛杉磯警察局.md "wikilink")[特種武器和戰術部隊](../Page/特種武器和戰術部隊.md "wikilink")（L型、H型）
        \[41\]\[42\]

      - （L型）\[43\]

      - [美國緝毒局](../Page/美國緝毒局.md "wikilink")

## 流行文化

### [電影](../Page/電影.md "wikilink")

  - 2010年—《[-{zh-cn:盗梦空间; zh-hk:潛行兇間;
    zh-tw:全面啓動}-](../Page/全面啓動.md "wikilink")》（Inception）：型號為第三代SCAR-L，沙色槍身，裝有[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")，被亞瑟（[喬瑟夫·高登-拉維特飾演](../Page/喬瑟夫·高登-拉維特.md "wikilink")）於[夢境中使用](../Page/夢境.md "wikilink")。
  - 2012年—《[-{zh-cn:敢死队; zh-hk:轟天猛將;
    zh-tw:浴血任務;}-2](../Page/浴血任務2.md "wikilink")》（The
    Expendables
    2）：型號為第三代SCAR-H，黑色槍身，裝有[EO-TECH全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")，被桑族武裝份子所使用。
  - 2014年—《[震撼殺戮](../Page/震撼殺戮.md "wikilink")》：型號為SCAR-H，被[美國緝毒局機動執行組使用](../Page/美國緝毒局.md "wikilink")。
  - 2015年—《[-{zh-hk:猛火鎗;zh-tw:全面逃殺;zh-cn:使命召唤;}-](../Page/全面逃殺.md "wikilink")》（The
    Gunman）：型號為SCAR-L
    CQC型，黑色槍身，在吉姆·泰瑞爾於[剛果民主共和國活動期間被一名派來試圖殺害他的打手所使用](../Page/剛果民主共和國.md "wikilink")。
  - 2015年—《[-{zh-cn:王牌特工：特工学院; zh-hk:皇家特工：間諜密令;
    zh-tw:金牌特務;}-](../Page/皇家特工：間諜密令.md "wikilink")》（Kingsman:
    The Secret
    Service）：型號為SCAR-L，噴上雪地塗裝並以馬格普PMAG彈匣供彈，被里奇蒙·范倫坦的手下所使用，其中一枝被主角蓋瑞·「伊格西」·安文（[泰隆·艾格頓飾演](../Page/泰隆·艾格頓.md "wikilink")）所繳獲。
  - 2016年—《[-{zh-cn:谍影重重5; zh-hk:叛諜追擊5：身份重啟;
    zh-tw:神鬼認證：傑森包恩;}-](../Page/神鬼認證：傑森包恩.md "wikilink")》：SCAR-L被[美國中央情報局特別探員企圖暗殺目標與傑森包恩時所使用](../Page/美國中央情報局.md "wikilink")。
  - 2016年—《[-{zh-cn:会计刺客; zh-hk:暗算;
    zh-tw:會計師;}-](../Page/會計師_\(2016年電影\).md "wikilink")》：型號SCAR-MK16CQC被[私人軍事服務公司布拉克斯頓](../Page/私人軍事服務公司.md "wikilink")·布拉克斯·沃夫的手下所使用。
  - 2018年—《[红海行动](../Page/红海行动.md "wikilink")》（Operation Red
    Sea）：型号为SCAR-L，在执行撤侨任务时被[中国人民解放軍海军陸戰隊](../Page/中国人民解放軍海军陸戰隊.md "wikilink")“蛟龙突击队”队员所使用，加装了[消音器与](../Page/抑制器.md "wikilink")[全息瞄准镜](../Page/全息瞄准镜.md "wikilink")，部分队员加装了[FN40GL榴弹发射器或者](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")[辅助握把](../Page/辅助握把.md "wikilink")。
  - 2018年—《[侏羅紀世界：殞落國度](../Page/侏羅紀世界：殞落國度.md "wikilink")》：型號為SCAR
    MK16CQC，被[私人軍事服務公司使用](../Page/私人軍事服務公司.md "wikilink")。

### [電子遊戲](../Page/電子遊戲.md "wikilink")

  - 2007年—《[-{zh-cn:反恐精英Online;
    zh-tw:絕對武力Online}-](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：型號為第一代SCAR L及SCAR
    H近戰型，黑色槍身，奇怪的沒有空倉掛機。為反恐小組的初始化武器，能依戰況更換[下機匣和](../Page/下機匣.md "wikilink")[槍管轉換成L型或H型任一模式](../Page/槍管.md "wikilink")。
  - 2007年—《[战地之王](../Page/战地之王.md "wikilink")》，型号为SCAR-L，并有其一系列特殊涂色版本，不可改造。目前韩服与台服已经重做。
  - 2007年—《[穿越火线](../Page/穿越火线.md "wikilink")》（Crossfire）：型號為第一代SCAR
    L及SCAR H近戰型，均在商城内以点券贩卖，同时两枪均拥有多种改型：
      - SCAR L命名为“SCAR Light”，银色枪身，使用30发弹匣却奇怪的装填35发弹药；
      - SCAR H近戰型命名为“SCAR Heavy”，土黄色枪身，使用20发弹匣却奇怪的装填35发弹药。
  - 2008年—《[-{zh-cn:镜之边缘;
    zh-tw:靚影特務}-](../Page/镜之边缘.md "wikilink")》（Mirror's
    Edge）：型號為第一代SCAR L，黑色槍身。故事模式中被PK組織的突擊手所使用，亦可被女主角**緋絲·康納斯**（Faith
    Connors）所繳獲。
  - 2008年—《[-{zh-hans:战地：叛逆连队;
    zh-hant:戰地風雲：惡名昭彰;}-](../Page/战地：叛逆连队.md "wikilink")》（Battlefield:
    Bad Company）：型號為SCAR L近戰型。
  - 2009年—《[-{zh-cn:使命召唤：现代战争2; zh-tw:決勝時刻：現代戰爭2;
    zh-hk:使命召喚：現代戰爭2}-](../Page/決勝時刻：現代戰爭2.md "wikilink")》（Call
    of Duty: Modern Warfare 2）：型號為第三代SCAR
    H近戰型，沙色槍身，命名為「SCAR-H」，造型上使用20發彈匣卻奇怪的裝填30發彈藥，而且沒有空倉掛機。故事模式中被[美國陸軍](../Page/美國陸軍.md "wikilink")[第75遊騎兵團](../Page/第75遊騎兵團.md "wikilink")、141特遣部隊及所使用。聯機模式時於等級8解鎖，並可使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、[下掛式霰彈槍](../Page/Masterkey槍管下掛式霰彈槍.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[心跳探測儀](../Page/心率.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、及延長[彈匣](../Page/彈匣.md "wikilink")（增至45發）。
  - 2009年—《[-{zh-hans:闪点行动：龙之崛起;
    zh-hant:閃擊點行動：龍之崛起」;}-](../Page/閃點行動：龍之崛起.md "wikilink")》（Operation
    Flashpoint 2: Dragon Rising）：型号有SCAR L战术型和SCAR
    H远战型，沙色槍身。故事模式中被[美國海軍陸戰隊武裝偵察部隊所使用](../Page/美國海軍陸戰隊武裝偵察部隊.md "wikilink")。
  - 2010年—《[-{zh-hans:武装突袭2;
    zh-hant:武裝行動2;}-：箭头行动](../Page/武装突袭2：箭头行动.md "wikilink")》（ArmA
    II: Operation Arrowhead）：是武装突袭2的续集资料片。该资料片中的美国陆军步兵主力步枪是SCAR H和SCAR
    L的多种变种型号。
  - 2010年—《[-{zh-hans:战地：叛逆连队2;
    zh-hant:戰地風雲：惡名昭彰2;}-](../Page/战地：叛逆连队2.md "wikilink")》（Battlefield:
    Bad Company 2）：型號為SCAR L近戰型。
  - 2011年—《[-{zh-cn:使命召唤：现代战争3; zh-tw:決勝時刻：現代戰爭3;
    zh-hk:使命召喚：現代戰爭3}-](../Page/決勝時刻：現代戰爭3.md "wikilink")》（Call
    of Duty: Modern Warfare 3）：型號為第三代SCAR
    L，黑色槍身，命名為「SCAR-L」，奇怪的沒有空倉掛機。故事模式中被[美國陸軍](../Page/美國陸軍.md "wikilink")[三角洲特種部隊合金分隊和](../Page/三角洲特種部隊.md "wikilink")「同心圈」成員所使用。聯機模式時於等級5解鎖，而在生存模式則在等級6解鎖，可使用[榴彈發射器](../Page/M320榴彈發射器.md "wikilink")、[下掛式霰彈槍](../Page/Masterkey槍管下掛式霰彈槍.md "wikilink")、[紅點鏡](../Page/紅點鏡.md "wikilink")、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[心跳探測儀](../Page/心率.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、混合式瞄準鏡及延長[彈匣](../Page/彈匣.md "wikilink")（增至45發）。
  - 2011年—《[-{zh-cn:战地3;
    zh-tw:戰地風雲3}-](../Page/戰地風雲3.md "wikilink")》（Battlefield
    3）：型號為第一代SCAR L及SCAR
    H近戰型，黑色槍身。前者於聯機模式當中被歸類為突擊步槍，可被突擊兵所使用；後者於故事模式當中被[美國海軍陸戰隊第一偵察營](../Page/美國海軍陸戰隊.md "wikilink")「盲流1-3」所使用，聯機模式時被歸類為工程兵武器。
  - 2012年—《[-{zh-cn:荣誉勋章：战士;
    zh-tw:榮譽勳章：鐵血悍將}-](../Page/榮譽勳章：鐵血悍將.md "wikilink")》（Medal
    of Honor: Warfighter）：型號為SCAR H及SCAR PDW，兩者均為沙色槍身。SCAR
    H只於故事模式登場，被馬科特遣隊所屬的[美國海軍特種作戰研究大隊隊員於](../Page/美國海軍特種作戰研究大隊.md "wikilink")[也門執行任務期間所使用](../Page/也門.md "wikilink")。SCAR
    PDW命名為“Mk16
    PDW”，於故事模式中被馬科特遣隊於[波斯尼亞行動時所使用](../Page/波斯尼亞.md "wikilink")，在聯機模式時為FSK/HJK、[SFOD-D和](../Page/三角洲部隊.md "wikilink")[OGA爆破兵的可用武器](../Page/中央情報局特別行動科.md "wikilink")。
  - 2012年—《[-{zh-cn:使命召唤：黑色行动II; zh-tw:決勝時刻：黑色行動II;
    zh-hk:使命召喚：黑色行動II}-](../Page/決勝時刻：黑色行動II.md "wikilink")》（Call
    of Duty: Black Ops II）：型號為第三代SCAR-L及SCAR-H：
      - SCAR-L命名為「HAMR」，沙色槍身，被歸類為[輕機槍並以](../Page/輕機槍.md "wikilink")[彈鼓供彈](../Page/彈鼓.md "wikilink")，載彈量為75發（聯機模式時可使用改裝：延長彈匣增至100發），初始攜彈量為225發（故事模式）和150發（聯機模式），最高攜彈量為525發（故事模式）和375發（聯機模式）。故事模式之中被[美国海軍特戰開發小組](../Page/美国海軍特戰開發小組.md "wikilink")、[也門陸軍和](../Page/也門陸軍.md "wikilink")[也門](../Page/也門.md "wikilink")[民兵所使用](../Page/民兵.md "wikilink")；聯機模式時於等級37解鎖，並可以使用[EOTech全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[前握把](../Page/輔助握把.md "wikilink")、、[反射式瞄準鏡](../Page/紅點鏡.md "wikilink")、快速抽出握把、目標搜索器、可調節[槍托](../Page/槍托.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[激光瞄準器](../Page/激光瞄準器.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[可變倍率式瞄準鏡](../Page/望远镜放大倍数.md "wikilink")（狙擊鏡）、[延長彈匣](../Page/彈匣.md "wikilink")、[混合式光學瞄準鏡](../Page/Leupold_HAMR瞄準鏡.md "wikilink")、射速增加、[雙波段式瞄準鏡](../Page/雙波段式瞄準鏡.md "wikilink")。
      - SCAR-H為沙色槍身，造型上使用20發[彈匣但載彈量為](../Page/彈匣.md "wikilink")30發（聯機模式時可使用改裝：延長彈匣增至40發），初始攜彈量為180發（故事模式）、90發（聯機模式）和120發（殭屍模式），最高攜彈量為390發（故事模式）和240發（聯機模式和殭屍模式）。故事模式之中完成「與我一起受苦」（Suffer
        With
        Me）戰役以後解鎖，被[美國海軍特戰開發小組及](../Page/美國海軍特戰開發小組.md "wikilink")[美國海軍所使用](../Page/美國海軍.md "wikilink")；聯機模式時於等級40解鎖，並可以使用[反射式瞄準鏡](../Page/紅點鏡.md "wikilink")、快速抽出握把、快速重裝彈匣、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[前握把](../Page/輔助握把.md "wikilink")、可調節[槍托](../Page/槍托.md "wikilink")、目標搜索器、[激光瞄準器](../Page/激光瞄準器.md "wikilink")、[擊發調變](../Page/擊發調變槍械.md "wikilink")、[EOTech全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、、[混合式光學瞄準鏡](../Page/Leupold_HAMR瞄準鏡.md "wikilink")、[延長彈匣](../Page/彈匣.md "wikilink")、[榴彈發射器](../Page/M320榴彈發射器.md "wikilink")、毫米波掃描器。
  - 2012年—《[戰爭前線](../Page/戰爭前線.md "wikilink")》（Warface）：型號為第三代SCAR-H近战型和SCAR
    PDW：
      - 第三代SCAR-H近战型命名為“FN
        SCAR-H”，土黄色槍身，為步槍手專用武器，使用20發[彈匣](../Page/彈匣.md "wikilink")，抽奖获得，并可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")、[突击刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[枪挂型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
        SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
        ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")），拥有皇冠币专属迷彩与黄金版本，强化威力、射程与载弹量。
          - 奇怪的是，属于枪口配件的消音器和制退器能够直接套在上使用
      - SCAR PDW命名為“SCAR-L
        PDW”，银灰色槍身，奇怪的归类为冲锋枪。為工程兵專用武器，使用30發[彈匣](../Page/彈匣.md "wikilink")，抽奖获得，并可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（MAGPUL弹匣握把、[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")、冲锋枪握把架）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")），默认自带MAGPUL弹匣握把。，拥有黄金版本，强化威力、射程与载弹量。
  - 2013年—《[-{zh-cn:战地4;
    zh-tw:戰地風雲4}-](../Page/戰地風雲4.md "wikilink")》（Battlefield
    4）：型號為第三代SCAR H，沙色槍身，具16吋及20吋[槍管兩種版本](../Page/槍管.md "wikilink")：
      - 16吋槍管型命名為「SCAR-H」，歸類為[突擊步槍](../Page/突擊步槍.md "wikilink")，20+1發彈匣，單機模式中能夠被主角丹尼爾·雷克（Daniel
        Recker）所使用（被設定為預設武器）。聯機模式中為突擊兵的可解鎖武器。
      - 20吋槍管型命名為「SCAR-H
        SV」，歸類為[特等射手步槍](../Page/精確射手步槍.md "wikilink")，20+1發彈匣，單機模式中能夠被主角丹尼爾·雷克（Daniel
        Recker）所使用。聯機模式中為所有兵種的可解鎖武器。
  - 2013年—《[-{zh-hans:收获日;
    zh-hant:劫薪日;}-2](../Page/劫薪日2.md "wikilink")》（Payday
    2）：型號為SCAR-H，沙色槍身，命名為「Eagle Heavy Rifle」。
  - 2013年—《[-{zh-cn:使命召唤Online; zh-tw:決勝時刻Online;
    zh-hk:使命召喚Online}-](../Page/使命召唤Online.md "wikilink")》（Call
    of Duty Online）：型號為第三代SCAR H近戰型，沙色槍身，命名為「MK
    17-CQC」，使用[雷明登ACR突擊步槍式樣的伸縮](../Page/雷明登ACR突擊步槍.md "wikilink")[槍托](../Page/槍托.md "wikilink")。於等級7解鎖並需要2200GP購買，可使用[榴彈發射器](../Page/M203榴彈發射器.md "wikilink")、[ORS紅點鏡](../Page/紅點鏡.md "wikilink")、[反射型紅點鏡](../Page/Aimpoint_Comp_M2红点镜.md "wikilink")、[COS瞄準鏡](../Page/ELCAN_Specter_DR光學瞄準鏡.md "wikilink")、中距望遠式快瞄鏡、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[夜視瞄準鏡](../Page/夜視儀.md "wikilink")、[熱成像瞄準鏡](../Page/熱成像瞄準鏡.md "wikilink")、[EOTech全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[快慢機](../Page/擊發調變槍械.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、、[擴容彈匣](../Page/彈匣.md "wikilink")、[雙彈匣](../Page/彈匣.md "wikilink")、[快瞄握把](../Page/手槍握把.md "wikilink")、[激光指示器](../Page/激光瞄準器.md "wikilink")、金屬被甲彈等技能。
  - 2015年—《[-{zh-hans:战地：硬仗;
    zh-hant:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》（Battlefield:
    Hardline）：型號為SCAR
    H近戰型及標準型（使用重槍管後），沙色槍身，命名為「SCAR-H」，歸類為[戰鬥步槍](../Page/戰鬥步槍.md "wikilink")，20+1發彈匣，被警方執行者（Enforcer）所使用（罪犯解鎖條件為：以任何陣營進行遊戲使用該槍擊殺1250名敵人後購買武器執照），價格為$26,400。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
    T1、SRS 02、[Comp
    M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
    、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、[槍托](../Page/槍托.md "wikilink")、[電筒](../Page/電筒.md "wikilink")、[戰術燈](../Page/戰術燈.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")、[穿甲](../Page/穿甲彈.md "wikilink")[曳光彈](../Page/曳光彈.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。劇情模式當中則能夠被主角尼古拉斯·門多薩所使用。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six: Siege）：型號為SCAR-H近戰型，沙色槍身，命名為「Mk17
    CQB」，被[海豹部隊所使用](../Page/海豹部隊.md "wikilink")。
  - 2015年—《[殺戮空間2](../Page/殺戮空間2.md "wikilink")》：突擊兵（職業）屬下 Tier 4 武器，型號為
    SCAR-H，命名為「SCAR-H Assault Rifle」，具備全自動與半自動射擊模式。
  - 2016年—《[全境封鎖](../Page/全境封鎖.md "wikilink")》：型號為SCAR-L及SCAR-H，前者被歸類為突擊步槍，後者歸類被為狙擊步槍。
  - 2017年—《[绝地求生](../Page/绝地求生.md "wikilink")》：型号为SCAR-L，可装备紅點瞄准镜、全息瞄準鏡、二倍瞄准镜、三倍瞄準鏡、四倍瞄准镜、六倍瞄准镜、快速弹夹、扩容弹夹、快速扩容弹夹、垂直握把、直角握把、拇指握把、輕型握把、半截握把。
  - 2017年-《幽灵行动：荒野》（Ghost Recon：WILDLANDS）：型号为SCAR-H，命名为“MK17”，可于武器箱取得。
  - 2019年-《幽灵行动：荒野》（Ghost
    Recon：WILDLANDS）：型号为SCAR-L，命名为“MK16”，由SCAR-H的上机匣和SCAR-L的下机匣组成。

### [動畫](../Page/動畫.md "wikilink")

  - 2015年—《[劇場版-{zh-hans:心理测量者;
    zh-hant:心靈判官;}-](../Page/心理测量者剧场版.md "wikilink")》（Psycho-Pass:
    The Movie，劇場版 PSYCHO-PASS
    サイコパス）：型號為SCAR-H，沙色槍身，裝上[EOTech全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")，被SEAUn憲兵隊士兵所使用。

### [輕小說](../Page/輕小說.md "wikilink")

  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：登場型號分別為SCAR-L和SCAR-H，前者被“MMTM”小隊的“薩門”所使用；後者被新型AI“哈珊”所使用。

## 相關連結

  - [FN FAL自動步槍](../Page/FN_FAL自動步槍.md "wikilink")
  - [FN CAL突擊步槍](../Page/FN_CAL突擊步槍.md "wikilink")
  - [FN FNC突擊步槍](../Page/FN_FNC突擊步槍.md "wikilink")
  - [多口径单兵武器](../Page/多口径单兵武器.md "wikilink")
  - [HK G36突擊步槍](../Page/HK_G36突擊步槍.md "wikilink")
  - [CZ-805 Bren突擊步槍](../Page/CZ-805_Bren突擊步槍.md "wikilink")
  - [CETME輕機槍](../Page/CETME輕機槍.md "wikilink")
  - [XT-97突擊步槍](../Page/XT-97突擊步槍.md "wikilink")

## 參考文獻

## 外部連結

  - —[Official FN assault rifles page, including
    SCAR](http://www.fnherstal.com/index.php?id=182)

  - —[Official FNH USA military
    page](https://web.archive.org/web/20081219145355/http://www.fnhusa.com/mil/products/firearms/group.asp?gid=FNG020&cid=FNC01)

  - —[Official FNH USA commercial
    page](https://web.archive.org/web/20080210162745/http://www.fnhusa.com/le/products/firearms/group.asp?gid=FNG007&cid=FNC01)

  - —[*Operators Test New Commando Rifle* -
    Military.com](http://www.military.com/news/article/operators-test-new-commando-rifle.html?ESRC=dod.nl)

  - —[Official FNH USA
    page](https://web.archive.org/web/20080210162745/http://www.fnhusa.com/le/products/firearms/group.asp?gid=FNG007&cid=FNC01)

      - [High-res photos from FNH USA of all 3rd Generation FN SCAR
        variants](https://web.archive.org/web/20080212050006/http://www.fnhusa.com/le/press/images_group.asp?gid=FNG007&cid=FNC01)

  - —[FN SCAR brochure (PDF
    file)](http://www.fnhusa.com/downloads/FNH_SCAR_Brochure.pdf)

  - —[FN SCAR on
    Defense-Update](https://web.archive.org/web/20080509164656/http://www.defense-update.com/products/s/scar.htm)

  - —[FN SCAR video
    demonstration](http://files.filefront.com/SCAR+Demowmv/;9074210;/fileinfo.html)

  - —[FN SCAR
    images](https://web.archive.org/web/20071013202304/http://imagestation.com/album/pictures.html?id=2104970073)

  - —[FN SCAR video and
    review](https://web.archive.org/web/20080405171942/http://www.gunslot.com/guns/fn-scar)

  - —[FN SCAR Powerpoint Presentation (PDF
    file)](http://www.dtic.mil/ndia/2006smallarms/smith.pdf)

  - —[Modern Firearms—FN SCAR](http://world.guns.ru/assault/as70-e.htm)

  - —[D Boy Gun World（槍炮世界）—FN
    SCAR计划](http://firearmsworld.net/usa/r/scar/fn_scar.htm)

[Category:自动步枪](../Category/自动步枪.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:戰鬥步槍](../Category/戰鬥步槍.md "wikilink")
[Category:精確射手步槍](../Category/精確射手步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:美國槍械](../Category/美國槍械.md "wikilink")
[Category:美國步槍](../Category/美國步槍.md "wikilink")
[Category:比利時槍械](../Category/比利時槍械.md "wikilink")

1.

2.

3.

4.  [FN SCAR量产型](http://firearmsworld.net/fn/scar/prd.htm)

5.

6.  [Newer carbines outperform M4 in dust test - Army News, opinions,
    editorials, news from Iraq, photos, reports - Army
    Times](http://www.armytimes.com/news/2007/12/army_carbine_dusttest_071217/)

7.  <http://www.armytimes.com/news/2008/11/army_carbineday_112308w/>

8.

9.  [FN Herstal Announces FN IAR
    Award](http://www.fnhusa.com/mil/press/detail.asp?id=49) , accessed
    February 5, 2009

10.

11. [D Boy Gun World—FN SCAR计划（FN
    IAR）](http://firearmsworld.net/usa/r/scar/fn_iar.htm)

12.

13. [美国海军陆战队选择HK-IAR取代M249机枪](http://war.news.163.com/09/1204/14/5PMRLA7700011MTO.html)

14.

15.
16.

17.

18.

19.

20.

21. geo-army.ge

22.

23.
24.

25. <http://www.mod.go.jp/gsdf/gmcc/hoto/hkou/14hk113.pdf>

26.

27.

28. <http://www.kam.lt/lt/naujienos_874/aktualijos_875/i_lietuva_atgabenti_naujieji_taikliojo_saulio_ginklai.html>
    Į Lietuvą atgabenti naujieji taikliojo šaulio ginklai

29.

30.

31.

32. <http://specijalne-jedinice.com/Oprema/Jurisne-puske/FN-SCAR.html#sthash.jtCTmYNi.dpbs>

33. <http://specijalne-jedinice.com/Srbija/Bataljon-vojne-policije-za-specijalne-namene-Kobre.html#sthash.WfX6r14t.vO282Xto.dpbs>

34. <http://www.imgrum.net/user/kurogears/3159694102/1291791250500675450_3159694102>

35. <http://www.royaldefence.com/partials/theme/banner/banner_06.jpg>

36.

37.

38.

39.

40.

41.

42.

43.