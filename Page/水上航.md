**水上航**，日本女性[漫畫家](../Page/漫畫家.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")。

## 作品

  - [奇幻蛙2堂](../Page/奇幻蛙2堂.md "wikilink")、全1冊、原名《》
  - [比基尼！](../Page/比基尼！.md "wikilink")、1冊待續、原名《》、日文版全2冊
  - [青空紙飛機](../Page/青空紙飛機.md "wikilink")、全1冊、原名《》
  - [甜姐新嫁娘](../Page/甜姐新嫁娘.md "wikilink")、全4冊、原名《》
  - [第六感指令](../Page/第六感指令.md "wikilink")、全3冊、原名《》
  - [Working Girl](../Page/Working_Girl.md "wikilink")、全3冊、原名《》
  - [眼鏡王子](../Page/眼鏡王子.md "wikilink")、全4冊、原名《》

## 外部連結

  - [](http://shop.kodansha.jp/bc2_bc/search_list.jsp?n=20&type=a&a=&t=&word=%90%85%8F%E3%8Dq&search=%8C%9F%8D%F5)
    - [講談社](../Page/講談社.md "wikilink")

  - [](http://www.amazon.co.jp/exec/obidos/search-handle-url?%5Fencoding=UTF8&search-type=ss&index=books-jp&field-author=%E6%B0%B4%E4%B8%8A%20%E8%88%AA)

  - [博客來網路書店 - 目前您搜尋的關鍵字為:
    水上航](http://search.books.com.tw/exep/prod_search_author.php?cat=all&key=%A4%F4%A4W%AF%E8)

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")