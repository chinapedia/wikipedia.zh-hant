**qGo**為一個開放原始碼且支援多平台的的IGS網路圍棋相容用戶端（IGS-like）。

{{ Infobox_Software | name = qGo | logo =
[QGo.svg](https://zh.wikipedia.org/wiki/File:QGo.svg "fig:QGo.svg") |
screenshot = | caption = qGo 1.0.4 | latest_release_version = 1.5.4 |
latest_release_date =  | operating_system =
[跨平台](../Page/跨平台.md "wikilink") | genre =
[遊戲](../Page/遊戲.md "wikilink") | license = [GNU
GPL](../Page/GNU_GPL.md "wikilink") | website =
<http://qgo.sourceforge.net/> }}

## 軟體

  - 已支援語系：英語、法語、德語、義大利語、丹麥語、荷蘭語、捷克語、漢語（正體中文）、葡萄牙語、俄語、拉丁語
  - 支援伺服器：IGS、NNGS及其他IGS相容架構

## 支援的棋譜格式

  - 讀取： SGF、MGT、XML
  - 打譜： SGF(GTP)

## 參見

  - [圍棋軟體](../Page/圍棋軟體.md "wikilink")

## 外部連結

  - [官方网站（英）](http://qgo.sourceforge.net/)
  - [qGo中文頁面](https://archive.is/20070928231219/http://192.192.45.33/~b9201187/qGo/)
  - [Internet Go
    Server（英）](https://web.archive.org/web/20070820033528/http://www.pandanet.co.jp/English/)
  - [gGo（英）](https://web.archive.org/web/20080120120057/http://panda-igs.joyjoy.net/java/gGo/)
  - [Ruby::Go（英）](https://archive.is/20130113081322/http://rubygo.rubyforge.org/)
  - [PANDA-glGo（英）](http://www.pandanet.co.jp/English/glgo/)
  - [KDE@Taiwan](http://kde.linux.org.tw/)（內有QT翻譯文件）

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:围棋软件](../Category/围棋软件.md "wikilink")
[Category:Linux遊戲](../Category/Linux遊戲.md "wikilink") [Category:Mac
OS遊戲](../Category/Mac_OS遊戲.md "wikilink")
[Category:免费游戏](../Category/免费游戏.md "wikilink")