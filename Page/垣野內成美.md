**垣野內成美**（本名：平野成美（），）為[日本的女性](../Page/日本.md "wikilink")[動畫師](../Page/動畫師.md "wikilink")、[作畫監督](../Page/作畫監督.md "wikilink")、[漫畫家](../Page/漫畫家.md "wikilink")，出生於[大阪府](../Page/大阪府.md "wikilink")，丈夫為[動畫監督](../Page/動畫監督.md "wikilink")[平野俊貴](../Page/:ja:平野俊貴.md "wikilink")。

高校畢業後加入由動畫師[湖川友謙創立的作畫工作室](../Page/:ja:湖川友謙.md "wikilink")「ビーボォー」\[1\]。之後參與Io（）\[2\]、[ARTLAND](../Page/ARTLAND.md "wikilink")\[3\]、[AIC等動畫工作室製作電視動畫系列及OVA的](../Page/AIC.md "wikilink")[原畫和](../Page/原畫.md "wikilink")[作畫監督工作](../Page/作畫監督.md "wikilink")。1988年開始連載《[吸血姬美夕](../Page/吸血姬美夕.md "wikilink")》成為漫畫家。她筆下的人物造型纖細唯美，以夢幻典雅的風格獲得人氣而廣為人知，也在漫畫和動畫之外擔任插圖和角色設計活耀。

## 簡歴

  - 1980年於製作[電視動畫](../Page/電視動畫.md "wikilink")「[傳說巨神伊甸王](../Page/傳說巨神伊甸王.md "wikilink")」時出道\[4\]。
  - 1988年因擔任OVA「[吸血姬美夕](../Page/吸血姬美夕.md "wikilink")」的[角色設計](../Page/角色設計.md "wikilink")・[分鏡](../Page/分鏡.md "wikilink")・[作畫監督而受到注目](../Page/作畫監督.md "wikilink")。
  - 1988年3月，漫畫《[吸血姬美夕](../Page/吸血姬美夕.md "wikilink")》在日本漫畫雜誌《[Suspiria](../Page/:ja:サスペリアミステリー.md "wikilink")》4月號開始連載而成為漫畫家。
  - 1990年，擔任OVA「[綠野原迷宮](../Page/:ja:緑野原迷宮.md "wikilink")」的[監督](../Page/監督.md "wikilink")。除了監督之外還擔任了角色設計、劇本、分鏡、作畫監督四項工作。
  - 2002年電視動畫《[奇鋼仙女ロウラン](../Page/:ja:奇鋼仙女ロウラン.md "wikilink")》之後有12年的時間專心在漫畫創作上，直到2014年製作《[獻給某飛行員的戀歌](../Page/獻給某飛行員的戀歌.md "wikilink")》時返回動畫界。

## 動畫作品

### 電視動畫

  - [傳說巨神伊甸王](../Page/傳說巨神伊甸王.md "wikilink")（1980年） - 動畫
  - [超时空要塞](../Page/超时空要塞.md "wikilink")（1982年） -
    第4話、7話、12話、19話、26話、31話、36話[原畫](../Page/原畫.md "wikilink")
  - [魔法のプリンセス ミンキーモモ](../Page/:ja:魔法のプリンセス_ミンキーモモ.md "wikilink")（1982年）
    - 第10話、15話原畫
  - [怪博士與機器娃娃](../Page/怪博士與機器娃娃.md "wikilink")（1983年） - 動畫、原畫
  - [福星小子](../Page/福星小子.md "wikilink")（1983年） - 動畫、原畫
  - [プラレス3四郎](../Page/:ja:プラレス3四郎.md "wikilink")（1983年） - 第28話原畫
  - [忍者戰士飛影](../Page/忍者戰士飛影.md "wikilink")（1985年） - 第25話、41話作畫監督
  - [魔法のスターマジカルエミ](../Page/:ja:魔法のスターマジカルエミ.md "wikilink")（1985年） -
    第34話[作畫監督](../Page/作畫監督.md "wikilink")
  - [魔法のアイドルパステルユーミ](../Page/:ja:魔法のアイドルパステルユーミ.md "wikilink")（1986年） -
    第25話作畫監督
  - [古靈精怪](../Page/古靈精怪.md "wikilink")（1987年） - OP、ED、第1話～8話原畫
  - [吸血姬美夕 (TV版)](../Page/:ja:吸血姫美夕_\(TV版\).md "wikilink")（1998年） -
    角色原案，OP、第25話原畫
  - [奇鋼仙女ロウラン](../Page/:ja:奇鋼仙女ロウラン.md "wikilink")（2002年） - OP原畫
  - [獻給某飛行員的戀歌](../Page/獻給某飛行員的戀歌.md "wikilink")（2014年） -
    6話作話監督，第1話、12話、13話原畫
  - [黑子的籃球（第2期）](../Page/黑子的籃球.md "wikilink")（2014年） - 第40話第二原畫
  - [偽戀](../Page/偽戀.md "wikilink")（2014年） - 第5話第二原畫
  - [約會大作戰II](../Page/約會大作戰.md "wikilink")（2014年） -
    第1話作畫監督補佐，第4話、6話、7話作畫監督，ED原畫
  - [目隱都市的演繹者](../Page/陽炎計劃.md "wikilink")（2014年） - 第9話第二原畫
  - [精靈使的劍舞](../Page/精靈使的劍舞.md "wikilink")（2014年） - OP、第8話分鏡
  - [ALDNOAH.ZERO](../Page/ALDNOAH.ZERO.md "wikilink")（2014年） - 第3話作畫監督
  - [戰國BASARA Judge End](../Page/戰國BASARA.md "wikilink")（2014年） - 8話作畫監督
  - [我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")（2014年） - ED原畫，第4話作畫監督補佐
  - [心靈判官 PSYCHO-PASS 2](../Page/PSYCHO-PASS.md "wikilink")（2014年） -
    第3話作畫監督
  - [白銀的意志 ARGEVOLLEN](../Page/:ja:白銀の意思_アルジェヴォルン.md "wikilink")（2014年）
    - 第17話作畫監督補佐、第22話作畫監督、第22話原畫
  - [電波少女與錢仙大人](../Page/電波少女與錢仙大人.md "wikilink")（2014年） - 第10話分鏡、原畫
  - [新妹魔王的契约者](../Page/新妹魔王的契约者.md "wikilink")（2015年） -
    OP原畫・第2話作畫監督補佐（平野成美名義）
  - [惡魔高校D×D BorN](../Page/惡魔高校D×D.md "wikilink")（2015年） - 第10話分鏡、原畫
  - [城下町的蒲公英](../Page/城下町的蒲公英.md "wikilink")（2015年） - ED原畫（平野成美名義）

### 劇場版

  - [傳說巨神伊甸王 發動編](../Page/傳說巨神伊甸王.md "wikilink")（1982年7月） - 作畫
  - [クラッシャージョウ](../Page/:ja:クラッシャージョウ.md "wikilink")（1983年3月） - 原畫
  - [超时空要塞 愛，還記得嗎？](../Page/超时空要塞.md "wikilink")（1984年7月） - 作畫監督補佐
  - [宇宙戰艦大和號2199 遊星方舟](../Page/宇宙戰艦大和號2199.md "wikilink")（2014年12月） -
    作畫監督

### OVA

  - [くりいむレモン いけないマコちゃん
    MAKO・セクシーシンフォニー](../Page/:ja:くりいむレモン.md "wikilink")（1985年）
    - [原畫](../Page/原畫.md "wikilink")、[作畫監督](../Page/作畫監督.md "wikilink")
    ※「幕野内味美」名義
  - [戦え\!\!イクサー1](../Page/:ja:戦え!!イクサー1.md "wikilink")（1985年） - 原畫、作畫監督
  - [メガゾーン23](../Page/:ja:メガゾーン23.md "wikilink")（1985年） - 原畫、作畫監督
  - [コスモスピンクショック](../Page/:ja:コスモスピンクショック.md "wikilink")（1986年） -
    原畫、作畫監督
  - [大魔獣激闘 鋼の鬼](../Page/:ja:大魔獣激闘_鋼の鬼.md "wikilink")（1987年） - 原畫
  - [破邪大星ダンガイオー](../Page/:ja:破邪大星ダンガイオー.md "wikilink")（1987年） -
    第1話、3話作畫監督
  - [吸血姬美夕（OVA版）](../Page/吸血姬美夕.md "wikilink")（1988年） -
    [角色設計](../Page/角色設計.md "wikilink")、[分鏡](../Page/分鏡.md "wikilink")、作畫監督
  - [緑野原迷宮](../Page/:ja:緑野原迷宮.md "wikilink")（1990年） -
    [監督](../Page/監督.md "wikilink")、角色設計、劇本、分鏡、作畫監督
  - [冒険\!イクサー3](../Page/:ja:冒険!イクサー3.md "wikilink")（1990年） - 原畫
  - [ねこねこ幻想曲](../Page/:ja:ねこねこ幻想曲.md "wikilink")（1991年） - 角色設計

### 自主製作作品

  - [DAICON IV OPENING
    ANIMATION](../Page/:ja:DAICON_FILM.md "wikilink")（1983年） - 原畫

## 作品列表

### 漫畫

  - [吸血姬美夕](../Page/吸血姬美夕.md "wikilink")（《Suspiria》1988年 -
    2000年，[秋田書店](../Page/秋田書店.md "wikilink")） -
    全10卷，中文版由[長鴻出版社發行](../Page/長鴻出版社.md "wikilink")。
  - [吸血姬夕維](../Page/吸血姬夕維.md "wikilink") - 全5卷，中文版由長鴻出版社發行。
  - 吸血姬夕維 香音抄（《Suspiria Mystery》，秋田書店） -
    全8卷，中文版由[長鴻出版社發行](../Page/長鴻出版社.md "wikilink")。
  - 人魚之旅，原名《》 - 原作/原案：[平野俊弘](../Page/平野俊貴.md "wikilink")
  - 除魔美少女，原名《》 - 全4卷，中文版由長鴻出版社發行。
  - 新・吸血姬美夕 - 原作：平野俊弘，全5卷，中文版由長鴻出版社發行。
  - 玫瑰物語，原名《》 - 全2卷，過去中文版由[台灣東販發行](../Page/台灣東販.md "wikilink")。
  - 月亮公主 -紅月- ，原名《》 - 全1卷，中文版由[東立出版社發行](../Page/東立出版社.md "wikilink")。
  - 午後3時的魔法，原名《》（《[月刊Afternoon](../Page/:ja:月刊アフタヌーン.md "wikilink")》，[講談社](../Page/講談社.md "wikilink")）
    - 全4卷，中文版由東立出版社發行。
  - 面幻想 - 全1卷，中文版由長鴻出版社發行。
  - 面具(假面)，原名《》
  - 魔幻吸血姬，原名《》 - 全1卷，中文版由長鴻出版社發行。
  - 吸血姬里亞 ，原名《》 - 全1卷，中文版由長鴻出版社發行。
  - 吸血浪人，原名《》 - 全3卷，中文版由長鴻出版社發行。
  - 戀水蓮，原名《》 - 全4卷，中文版由長鴻出版社發行。
  - 格闘少女JULINE，原名《》（《[Amie](../Page/:ja:Amie.md "wikilink")》1997年2月號 -
    1998年聖誕特別號，講談社） - 全5卷，中文版由台灣東販發行。
  - 風雲三姊妹LIN³，原名《》（《[月刊Magazine
    Z](../Page/:ja:月刊マガジンZ.md "wikilink")》，講談社） -
    原作：平野俊貴，全5卷，中文版由台灣東販發行。
  - 新・風雲三姊妹 特，原名《新・風雲三姊妹 特Lin》（《月刊Magazine Z》） - 原作：平野俊貴，全4卷，中文版由台灣東販發行。
  - 吸血舞姬・茉莉花，原名《》 - 全1卷，台灣中文版由長鴻出版社正式授權發行。
  - 歌姬Fight\!（短篇集） - 全1卷，中文版由東立出版社發行。
      -
      -
      - ─ 短篇，為「吸血浪人」之後的故事
  - Keep Trust -信頼の絆-
  - Snow Sugar
  - 異星學園，原名《異星学園エリスターズ》（）
      - 漫畫
          -
          -
          - \- 台譯本未收錄

          - \- 台譯本未收錄
      - 插畫小說
          - \- 台譯本未收錄

          - \- 台譯本未收錄
  - [藥師寺涼子怪奇事件簿](../Page/藥師寺涼子怪奇事件簿.md "wikilink")，原名《》（《月刊Magazine
    Z》→《月刊Afternoon（至2009年5月號）》，講談社） -
    原作：[田中芳樹](../Page/田中芳樹.md "wikilink")。目前發行至11卷，中文版由[尖端出版正式授權發行](../Page/尖端出版.md "wikilink")。
  - 紅瞳吸血少女，原名《》 - 全1卷，台灣中文版由長鴻出版社正式授權發行。
  - 吸血姫　ヴァンパイア・プリンセス（《[FlexComixフレア](../Page/:ja:COMIC_ポラリス.md "wikilink")》）-
    原作：平野俊弘
  - 吸血鬼狩獵者
    Cross，原名《レイスイーパー》・《レイスイーパーCROSS》（《[月刊少年FANG](../Page/:ja:月刊少年ファング.md "wikilink")》
    → 《[月刊Comic RUSH](../Page/:ja:月刊コミックラッシュ.md "wikilink")》） -
    全7卷，台灣中文版由[東立出版社正式授權發行](../Page/東立出版社.md "wikilink")。
  - Fairy Jewel（2011年 - ，電子數位漫畫誌《[電撃Comic
    Japan](../Page/:ja:電撃コミックジャパン.md "wikilink")》，[ASCII
    Media Works](../Page/ASCII_Media_Works.md "wikilink")）

### 插畫

  - 織田加繪原作

<!-- end list -->

  -
  -
  -
<!-- end list -->

  - 原作

<!-- end list -->

  -
  -
  -
  -
<!-- end list -->

  - [田中芳樹原作](../Page/田中芳樹.md "wikilink")

<!-- end list -->

  - 藥師寺涼子怪奇事件簿 摩天樓
  - 藥師寺涼子怪奇事件簿 東京夜未眠
  - 藥師寺涼子怪奇事件簿 巴黎妖都變
  - 藥師寺涼子怪奇事件簿 克麗奧佩特拉的葬送
  - 藥師寺涼子怪奇事件簿 黒蜘蛛島
  - 藥師寺涼子怪奇事件簿 夜光曲
  - 藥師寺涼子怪奇事件簿 霧之訪問者
  - 藥師寺涼子怪奇事件簿 小心水妖之日

<!-- end list -->

  - 其他

<!-- end list -->

  - （）

  - （）

  - （赤木理繪）

### 插畫集

  - 吸血姫美夕─垣野内成美插畫集

  - FILM COLLECTION－美夕 上・下巻（OVA版）

  - フィルムストーリー吸血姫美夕 壹巻 （收錄TV版１話至３話，沒有續刊）

  - 續初戀物語 〜修學旅行〜 公式數位原畫集

  - Flawless─薬師寺涼子的怪奇事件簿插畫集

  - MAKO編

  - Cream Lemon MAKO SEXY SYMPHONY（PART 7・PART 12）

## CD封面圖片

  - 《EVERYTHING》（[Fluke Beauty](http://www.flukebeauty.com/) ─ 美國）

## 遊戲

  - アークスII - 角色設定、原画（PC-8801版・PC-9801版・X68000版・MSX2版）
  - [續 初戀物語 ～修學旅行～](../Page/:ja:續初戀物語〜修學旅行〜.md "wikilink") -
    角色設計（PC-FX版・PS版・SS版）
  - クリックまんが オペラ座の怪人（PS版・Windows版）
  - [吸血姬夕維](../Page/吸血姬夕維.md "wikilink") 〜千夜抄〜 初回限定版（PS2版・Windows版）

## 腳註

## 外部連結

  - [](http://www.aprildd.co.jp/~kakinouchi/) － 垣野內成美官方網站。

  - [](https://web.archive.org/web/20050804233252/http://sinamura.hp.infoseek.co.jp/)

  - [吸血姫のお部屋](http://tenshi.net/)

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:日本動畫導演](../Category/日本動畫導演.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")

1.
2.
3.
4.