**鄭宜農**（，），[臺灣](../Page/臺灣.md "wikilink")[獨立音樂家](../Page/獨立音樂.md "wikilink")、[作家](../Page/作家.md "wikilink")、[演員](../Page/女演員.md "wikilink")，[臺灣導演](../Page/臺灣導演.md "wikilink")[鄭文堂之女](../Page/鄭文堂.md "wikilink")，[宜蘭人](../Page/宜蘭縣.md "wikilink")。曾就讀於[淡江大學中國文學系](../Page/淡江大學.md "wikilink")，並修習[法語](../Page/法語.md "wikilink")。2007年自淡江大學休學，專心寫作。

於2007年初登大螢幕，在同時身兼劇本創作、並由父親執導的青春電影《[夏天的尾巴](../Page/夏天的尾巴.md "wikilink")》中親自演出女主角，獲得[第44屆金馬獎最佳新人獎提名](../Page/第44屆金馬獎.md "wikilink")；並為電影創作了五首歌曲，收錄在[原聲帶中](../Page/原聲帶.md "wikilink")，同時參與演唱及錄製工作。鄭宜農也於[Legacy、The
Wall等](../Page/這牆音樂藝文展演空間.md "wikilink")[Live
House與各大音樂祭演出](../Page/音樂展演空間.md "wikilink")。2013年組成[猛虎巧克力發行](../Page/猛虎巧克力.md "wikilink")《夜工廠》專輯。2013年12月與[滅火器樂團主唱](../Page/滅火器樂團.md "wikilink")[楊大正結婚](../Page/楊大正.md "wikilink")，2016年1月底雙方簽字離婚。

## 感情

2016年1月3日於[粉絲專頁公開貼文](../Page/粉絲專頁.md "wikilink")，表示與[楊大正的愛是真實的](../Page/楊大正.md "wikilink")，卻一直無法愛上他的身體，坦言自己[性傾向模糊](../Page/性傾向.md "wikilink")，更容易受女性吸引。鄭宜農為此事掙扎許久，與楊大正坦誠後，兩人選擇面對，並感謝雙方家人的理解；選擇公開這段心路歷程，希望能帶給社會一些正面影響。楊大正隨後轉發文章表達支持，兩人永遠是對方重要的人生夥伴，婚姻關係並沒有進行手續上的處理，表示：「婚姻只是一張紙，將來彼此若有對象，隨時可辦離婚」。2016年1月底雙方已簽字離婚。\[1\]

## 作品

### 戲劇

  - 《[夏天的尾巴](../Page/夏天的尾巴.md "wikilink")》 (電影) （2007年）飾 張家月
  - 《[我可以跟你在一起嗎？](../Page/我可以跟你在一起嗎？.md "wikilink")》 (短片) （2008年）
  - 《[兒子](../Page/兒子.md "wikilink")》 (短片) （2008年）
  - 《[查無此人](../Page/查無此人.md "wikilink")》 (電影) （2008年）飾 方瑜
  - 《[親親](../Page/親親_\(電影\).md "wikilink")》（2009年）
  - 《[眼淚](../Page/眼淚_\(電影\).md "wikilink")》（2010年）飾 小雯
  - 《[我在這裡幹嘛？](../Page/我在這裡幹嘛？.md "wikilink")》 (短片) （2010年）
  - 《[Check](../Page/Check.md "wikilink")》 (短片) （2011年）
  - 《[五樓之二](../Page/五樓之二.md "wikilink")》 (短片) （2011年）
  - 《[過站](../Page/過站.md "wikilink")》 (短片) （2012年）
  - 《[菜鳥](../Page/菜鳥_\(電影\).md "wikilink")》 (客串) （2015年）
  - 《[魂囚西門](../Page/魂囚西門.md "wikilink")》 （2019年）
  - 《[鏡子森林](../Page/鏡子森林.md "wikilink")》 （2019年）

### 編劇、文字作品

  - 2005年 短片 《[風中小米田](../Page/風中小米田.md "wikilink")》 原創劇本
  - 2007年 《[夏天的尾巴](../Page/夏天的尾巴.md "wikilink")》 原創劇本
  - 2007年 《[夏天的尾巴](../Page/夏天的尾巴.md "wikilink")》 電影書
  - 2009年 《風中小米田》 童書（改編自同名短片風中小米田）
  - 2017年 《幹上俱樂部 3D妖獸變形實錄》 短篇集

### 音樂

#### 原聲帶

  - 《[夏天的尾巴](../Page/夏天的尾巴電影原聲帶.md "wikilink")》（2007年）
  - 《[眼淚](../Page/眼淚_\(電影\).md "wikilink")》（2010年）

#### 配樂

  - 電影《菜鳥》
  - 公視週播劇《燦爛時光》主題曲 - 〈光〉
  - 擔任公視迷你劇集《奇蹟的女兒》音樂統籌，製作編寫片頭曲〈人生很難〉及片尾曲〈玉仔的心〉

#### 單曲

  - 《[我在路上撿到一片天空](../Page/我在路上撿到一片天空.md "wikilink")》EP 限量500張（2009年）

#### 專輯

  - 《[海王星](../Page/海王星\(專輯\).md "wikilink")》首張創作專輯（2011年7月8日）

1.太陽 Your Sun

2.路 Long Road

3.春天 Spring Love

4.大雨城市 City of Rain

5.還是會害怕失去你 Still Afraid

6.獨立日 Independence Day

7.撒唷那拉 SAYONARA

8.別忘了你自己 I Am

9.Beautiful Song

10.城堡 Castle

  - 《[Pluto](../Page/Pluto\(專輯\).md "wikilink")》（2017年4月28日）

1.太空垃圾之死 Recycling in the Universe

2.那些酒精成癮的日子 Golden Old Days

3.獸眠 Fantastic Beasts and Where to Find Them

4.雲端漫舞 Her

5.冬眠 If Winter Comes, Can Spring be Far Behind?

6.Our Pop Song(feat.盧凱彤)

7.飛行少年 (Feat. 大象體操 Elephant Gym) Catch Me If You Can

8.溫州街五巷 The Warmest Street in Taipei

9.光 Pride

10.酒店關門之後 Last Order

### 書籍

  - [夏天的尾巴](../Page/夏天的尾巴\(電影\).md "wikilink")（2007年）
  - [風中的小米田](../Page/風中的小米田.md "wikilink")（2008年）
  - [幹上俱樂部:3D妖獸變形實錄](../Page/幹上俱樂部:3D妖獸變形實錄.md "wikilink") (2017年)

### 作詞

| 歌曲名稱 | 演唱者                              | 收錄專輯 | 作詞  | 作曲  | 年份   |
| ---- | -------------------------------- | ---- | --- | --- | ---- |
| 黑羊   | [鄧福如](../Page/鄧福如.md "wikilink") | 自成一派 | 鄭宜農 | 鄧福如 | 2015 |

## 獎項

2007年入圍[第44屆金馬獎](../Page/第44屆金馬獎.md "wikilink")「最佳新進演員」。

2009年入圍[第46屆金馬獎](../Page/第46屆金馬獎.md "wikilink")「最佳原創電影歌曲」。

2011年第31屆[金穗獎](../Page/金穗獎.md "wikilink")「最佳女演員獎」。

## 外部連結

  - [鄭宜農 - 樂多日誌](http://blog.roodo.com/ennogogo)

  -
  -
  - [Flickr上的](../Page/Flickr.md "wikilink")[鄭宜農](http://www.flickr.com/photos/27870541@N08/)

  - [鄭宜農on iNDIEVOX](https://www.indievox.com/enno)

  -
  -
  -
  -
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:台湾女歌手](../Category/台湾女歌手.md "wikilink")
[Category:台灣創作歌手](../Category/台灣創作歌手.md "wikilink")
[Category:華語歌手](../Category/華語歌手.md "wikilink")
[Category:台灣LGBT音樂家](../Category/台灣LGBT音樂家.md "wikilink")
[Category:台灣女同性戀者](../Category/台灣女同性戀者.md "wikilink")
[Category:淡江大學校友](../Category/淡江大學校友.md "wikilink")
[Category:臺北市立中正高級中學校友](../Category/臺北市立中正高級中學校友.md "wikilink")
[Category:宜蘭人](../Category/宜蘭人.md "wikilink")
[Yi宜](../Category/鄭姓.md "wikilink")

1.  [楊大正簽字離婚沒掉淚　知老婆出櫃早哭過](http://www.appledaily.com.tw/realtimenews/article/entertainment/20160225/803447/%E3%80%90%E6%9B%B4%E6%96%B0%E3%80%91%E6%A5%8A%E5%A4%A7%E6%AD%A3%E7%B0%BD%E5%AD%97%E9%9B%A2%E5%A9%9A%E6%B2%92%E6%8E%89%E6%B7%9A%E3%80%80%E7%9F%A5%E8%80%81%E5%A9%86%E5%87%BA%E6%AB%83%E6%97%A9%E5%93%AD%E9%81%8E)