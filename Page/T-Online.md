[Logo-t-online.png](https://zh.wikipedia.org/wiki/File:Logo-t-online.png "fig:Logo-t-online.png")
[T-Online_Zentrale.jpg](https://zh.wikipedia.org/wiki/File:T-Online_Zentrale.jpg "fig:T-Online_Zentrale.jpg")
**T-Online**
是德国最大的[因特网服务提供商](../Page/因特网服务提供商.md "wikilink")。[网络门户站点](../Page/网络门户.md "wikilink")。

## 企业信息

2000年1月公司上市成为
*T-Online国际股份有限公司*。公司由大约2600名员工组成（其中在[德国略少于](../Page/德国.md "wikilink")2000个）。[德国电信控股](../Page/德国电信.md "wikilink")74%。公司位于[达姆城](../Page/达姆城.md "wikilink")（2005年起）.

*T-Online*在欧洲拥有大约134万用户（Q1, 2004年），2004财年收入大约158万欧元，是欧洲最大的因特网服务提供商之一。

T-Online在[法国使用商标](../Page/法国.md "wikilink")[Club
Internet](../Page/Club_Internet.md "wikilink")，在[西班牙是](../Page/西班牙.md "wikilink")[Ya.com](../Page/Ya.com.md "wikilink")，在[葡萄牙是](../Page/葡萄牙.md "wikilink")[Terravista](../Page/Terravista.md "wikilink")，
在[奥地利和](../Page/奥地利.md "wikilink")[瑞士是](../Page/瑞士.md "wikilink")
*T-Online*。

## 产品

[T-Online_Zentrale1.jpg](https://zh.wikipedia.org/wiki/File:T-Online_Zentrale1.jpg "fig:T-Online_Zentrale1.jpg")
T-Online为企业和个人提供网络接入服务。通过接入技术[T-DSLT](../Page/T-DSL.md "wikilink")-Online与母公司德国电信合作为德国大众提供[DSL宽带接入服务](../Page/数字用户线路.md "wikilink")。T-Online还是网络门户，使得用户享受个性化服务。

像[AOL一样T](../Page/AOL.md "wikilink")-Online也给客户提供自己的网络连接软件，除了[浏览器和](../Page/浏览器.md "wikilink")[电子邮件程序外还有扩展功能如](../Page/电子邮件程序.md "wikilink")[电子银行](../Page/电子银行.md "wikilink")。没有这些软件网络接入当然也可以使用。

## 服务

T-Online 提供免费或者付费（一般只对于成员）服务。

  - 免费服务：
      - [T-Online信使](../Page/T-Online信使.md "wikilink")
      - [网络电话](../Page/网络电话.md "wikilink")
        使用单独号码"032"-[电话区号](../Page/电话区号.md "wikilink")

## 历史

T-Online从[德国电信分离出来](../Page/德国电信.md "wikilink")。

## 参看

  - [德国电信](../Page/德国电信.md "wikilink")

## 外部链接

  - [T-Online门户站点](http://www.t-online.de/)
  - [企业网站](http://www.t-online.net/)
  - [不同操作系统连接设置信息](http://www.albert-rommel.de/ppp-noscript.htm)

[Category:ISP](../Category/ISP.md "wikilink")
[Category:德國公司](../Category/德國公司.md "wikilink")