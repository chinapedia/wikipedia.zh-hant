**嘉道理家族**，[猶太人](../Page/猶太人.md "wikilink")，是一個富裕的巴格達米茲拉希猶太族裔。祖先世居[伊拉克首都](../Page/伊拉克.md "wikilink")[巴格達](../Page/巴格達.md "wikilink")。18世紀時遷居至[印度](../Page/印度.md "wikilink")[孟買](../Page/孟買.md "wikilink")，後在[上海跟隨](../Page/上海.md "wikilink")[沙遜家族發跡](../Page/沙遜家族.md "wikilink")。他們成功建立歐亞商業王朝。連同上海的沙遜家族，嘉道理家族成為顯赫的富豪家族。

嘉道理家族又從[英國人手上購入廣州電力廠](../Page/英國人.md "wikilink")，但在[廣州的生意出現虧蝕](../Page/廣州.md "wikilink")，轉到[香港發展](../Page/香港.md "wikilink")，並建成鶴園街電廠，主要供電給[半島酒店](../Page/半島酒店.md "wikilink")，[埃利·嘉道理在電廠重組之後成為當時的大](../Page/埃利·嘉道理.md "wikilink")[股東](../Page/股東.md "wikilink")。嘉道理家族於1866年創辦[香港上海大酒店有限公司](../Page/香港上海大酒店有限公司.md "wikilink")，早期在上海經營酒店。旗下的[半島酒店品牌](../Page/半島酒店.md "wikilink")，在世界各地管理多家豪華酒店，包括香港[尖沙咀](../Page/尖沙咀.md "wikilink")[半島酒店](../Page/半島酒店.md "wikilink")。

## 主要人物

  - (1865-1922)

  - [埃利·嘉道理](../Page/埃利·嘉道理.md "wikilink")(1867-1944) -
    香港著名[慈善家](../Page/慈善家.md "wikilink")，成立[育才書社及多所學校](../Page/育才書社.md "wikilink")，又設《復興基金》，幫助猶太難民創辦一些中小企業。

      - (1899-1993) -
        英籍香港猶太人，曾是香港首富，經營能源工業、地產、船務、工程、建築、酒店等，曾獲英皇室及各國冊封為[勳爵](../Page/勳爵.md "wikilink")，熱心公益。

          - [米高·嘉道理](../Page/米高·嘉道理.md "wikilink") -
            現[中電控股](../Page/中電控股.md "wikilink")（）及[香港上海大酒店](../Page/香港上海大酒店.md "wikilink")（）主席。

      - (1902-1995) -
        羅蘭士·嘉道理胞弟，[上海援助歐洲難民委員會召集人](../Page/上海援助歐洲難民委員會.md "wikilink")。曾創立嘉道理農業試驗推廣農場、[嘉道理農業輔助會](http://www.hkmemory.hk/collections/KAAA/about/index_cht.html)及嘉道理農業貸款基金會等使新界30萬農民受惠而獲[OBE勳銜](../Page/OBE.md "wikilink")。

## 資產

  - [嘉道理父子有限公司](../Page/嘉道理父子有限公司.md "wikilink")
  - [中電控股](../Page/中電控股.md "wikilink")：持股33.21%
  - [香港上海大酒店有限公司](../Page/香港上海大酒店有限公司.md "wikilink")53.82％
  - [直昇機服務（香港）](../Page/直昇機服務（香港）.md "wikilink")
  - [美捷香港商用飛機](../Page/美捷香港商用飛機.md "wikilink")
  - [龍門滙成](../Page/龍門滙成.md "wikilink")：嘉道理家族的私人公司，在陝西韓城地區擁有豐富的[煤層氣資源](../Page/煤層氣.md "wikilink")。
  - [嘉道理置業](../Page/嘉道理置業.md "wikilink")
  - [聖佐治大廈](../Page/聖佐治大廈.md "wikilink")
  - [凌霄閣](../Page/凌霄閣.md "wikilink")
  - [山頂纜車](../Page/山頂纜車.md "wikilink")
  - [太平地氈](../Page/太平地氈.md "wikilink")
  - [嘉道理農場暨植物園](../Page/嘉道理農場暨植物園.md "wikilink")
  - [嘉道理保育中國](../Page/嘉道理保育中國.md "wikilink") （[Kadoorie Conservation
    China](../Page/嘉道理中國保育.md "wikilink")，KCC）
  - [嘉道理爵士公館](../Page/嘉道理爵士公館.md "wikilink")
  - [深水灣道](../Page/深水灣道.md "wikilink")68.70.72號
  - [赫蘭道](../Page/赫蘭道.md "wikilink")7號，地盤面積25186方呎，以0.75倍地積比計算，可建樓面18890方呎
  - [舂坎角道](../Page/舂坎角道.md "wikilink")11號A號洋房
  - [加多利山共有](../Page/加多利山.md "wikilink")86幢洋房和一座39個單位聖佐治閣

## 與嘉道理家族相關的事物

  - [育才中學 (沙田)](../Page/育才中學_\(沙田\).md "wikilink")
  - [官立嘉道理爵士中學（西九龍）](../Page/官立嘉道理爵士中學（西九龍）.md "wikilink")
  - [官立嘉道理爵士小學](../Page/官立嘉道理爵士小學.md "wikilink")
  - [上海育才中學](../Page/上海育才中學.md "wikilink")
  - [嘉道理盆距蘭](../Page/嘉道理盆距蘭.md "wikilink")：以賀理士·嘉道理命名的植物。

## 外部連結

  - [傳奇家族嘉道理](http://info.ceo.hc360.com/2006/03/13101222267.shtml)
  - [香港第一隱秘家族--嘉道理](https://web.archive.org/web/20070303170956/http://big5.ce.cn/finance/financing/salon/200603/28/t20060328_6518781.shtml)
  - [香港嘉道理之家](http://www.21ct.cn/info/infoview.asp?id=92728)

[Category:猶太家族](../Category/猶太家族.md "wikilink")
[Category:嘉道理家族](../Category/嘉道理家族.md "wikilink")
[Category:香港家族](../Category/香港家族.md "wikilink")
[Category:香港猶太人](../Category/香港猶太人.md "wikilink")
[Category:伊拉克猶太人](../Category/伊拉克猶太人.md "wikilink")