**露絲**（英語：**Ruth**、
**Rose**、**Lucie**），是一個常見的女性化[譯名](../Page/譯名.md "wikilink")。

露絲可能指：

## 地名

  - [露絲嶺](../Page/露絲嶺.md "wikilink")，[南極洲的一座山嶺](../Page/南極洲.md "wikilink")。
  - [露絲撞擊坑](../Page/露絲撞擊坑.md "wikilink")，[金星上的一個](../Page/金星.md "wikilink")[撞擊坑](../Page/撞擊坑.md "wikilink")。
  - [露絲·禮來法學圖書館](../Page/印第安納大學麥肯尼法學院.md "wikilink")，美國[印第安納大學麥肯尼法學院內的一所圖書館](../Page/印第安納大學麥肯尼法學院.md "wikilink")。

## 人名

  - [露絲·貝爾維爾](../Page/露絲·貝爾維爾.md "wikilink")（1854年—1943年），出售**時間**的倫敦商人。

  - [露絲·聖·丹尼斯](../Page/露絲·聖·丹尼斯.md "wikilink")（1879年—1968年），美國舞蹈家，現代舞蹈的奠基者。

  - [露絲·潘乃德](../Page/露絲·潘乃德.md "wikilink")（1887年—1948年），美國女性[人類學家](../Page/人類學家.md "wikilink")。

  - （1891年—1976年），美國兒童文學作家，[綠野仙蹤系列的創作者之一](../Page/綠野仙蹤.md "wikilink")。

  - [貝比·魯斯](../Page/貝比·魯斯.md "wikilink")（1895年—1948年），美國史上最有名的棒球選手之一。

  - （1915年—2009年），美國女性慈善家。

  - [露絲·鮑爾·賈華拉](../Page/露絲·鮑爾·賈華拉.md "wikilink")（1927年—2013年），英國作家與編劇。

  - [露絲·倫德爾](../Page/露絲·倫德爾.md "wikilink")（1930年—2015年），英語驚悚小說和犯罪小說作家。

  - [露絲·拜德·金斯伯格](../Page/露絲·拜德·金斯伯格.md "wikilink")（1933年—），美國最高法院[大法官](../Page/大法官.md "wikilink")。

  - [露絲·安·明納](../Page/露絲·安·明納.md "wikilink")（1935年—），美國政治家。

  - [露絲·福勞爾](../Page/露絲·福勞爾.md "wikilink")（1940年—2014年），英國DJ。

  - [尾關露絲](../Page/尾關露絲.md "wikilink")（1956年—），日裔美國小說家及電影導演。

  - [露絲·波拉特](../Page/露絲·波拉特.md "wikilink")（1957年—），美國職業財務主管。

  - [露絲·西蒙](../Page/露絲·西蒙.md "wikilink")（1962年—），非洲[厄利垂亞女記者](../Page/厄利垂亞.md "wikilink")。

  - [露絲·戴維森](../Page/露絲·戴維森.md "wikilink")（1978年—），蘇格蘭政治家。

  - [露絲·貝蒂亞](../Page/露絲·貝蒂亞.md "wikilink")（1979年—），西班牙女子跳高運動員。

  - [露絲·奈嘉](../Page/露絲·奈嘉.md "wikilink")（1982年—），[衣索比亞裔愛爾蘭女演員](../Page/衣索比亞.md "wikilink")。

  - [露絲·威爾遜](../Page/露絲·威爾遜.md "wikilink")（1982年—），英國女演員。

  - [孔泰薩·露絲](../Page/孔泰薩·露絲.md "wikilink")（1984年—），美國色情演員。

  - [露絲·哈迪卡](../Page/露絲·哈迪卡.md "wikilink")（1985年—），[捷克網球女選手](../Page/捷克.md "wikilink")。

  - [露絲·莎法洛法](../Page/露絲·莎法洛法.md "wikilink")（1987年—），[捷克網球女選手](../Page/捷克.md "wikilink")。

  - [露絲·漢丁頓-維莉](../Page/露絲·漢丁頓-維莉.md "wikilink")（1987年—），英國超級名模。

  - [露絲·巴比](../Page/露絲·巴比.md "wikilink")（1994年—），[象牙海岸女子跆拳道運動員](../Page/象牙海岸.md "wikilink")。

  - [露絲·傑貝特](../Page/露絲·傑貝特.md "wikilink")（1996年—），[肯亞及](../Page/肯亞.md "wikilink")[巴林長跑運動員](../Page/巴林.md "wikilink")。

  - [露絲·韓德森](../Page/露絲·韓德森.md "wikilink")，是[美國廣播公司懸疑類電視連續劇](../Page/美國廣播公司.md "wikilink")《[LOST檔案](../Page/LOST檔案.md "wikilink")》的角色。

## 其他

  - [颱風露絲 (1971年)](../Page/颱風露絲_\(1971年\).md "wikilink")
  - [小行星798](../Page/小行星798.md "wikilink")

[R](../Category/人名譯名消歧義.md "wikilink")