**蔡少綿**，**Linda
Choy**，（），前任[香港](../Page/香港.md "wikilink")[環境局政治助理](../Page/環境局.md "wikilink")。現任[香港迪士尼樂園公共事務副總裁](../Page/香港迪士尼樂園.md "wikilink")。

## 簡歷

蔡少綿在[香港浸會學院學士](../Page/香港浸會大學.md "wikilink")[畢業](../Page/畢業.md "wikilink")，主修[傳理](../Page/傳理.md "wikilink")，其後於[倫敦大學](../Page/倫敦大學.md "wikilink")[倫敦政治經濟學院修畢](../Page/倫敦政治經濟學院.md "wikilink")[國際關係](../Page/國際關係.md "wikilink")[碩士課程](../Page/碩士.md "wikilink")。

1998年，蔡少綿出任[香港政府政務主任職系](../Page/香港政府.md "wikilink")，於2004年轉任《[南華早報](../Page/南華早報.md "wikilink")》[編輯](../Page/編輯.md "wikilink")。2007年，她加入[香港迪士尼樂園任職政府事務總監](../Page/香港迪士尼樂園.md "wikilink")\[1\]。

蔡少綿在任職環境局政治助理期間，共跟隨局長[邱騰華外訪](../Page/邱騰華.md "wikilink")21次，出訪地方包括日本、澳洲、加拿大、美國、韓國、歐洲、巴西等，涉及開支93萬元，外訪開支較所有副局長高\[2\]。

2013年7月，蔡少綿重返[香港迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")，接替退休的[盧炳松擔任公共事務副總裁](../Page/盧炳松.md "wikilink")，帶領專責政府事務、對外事務、社區關係、傳媒關係及演藝人員傳訊的團隊\[3\]
\[4\]。

## 參考資料

[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:香港浸會大學校友](../Category/香港浸會大學校友.md "wikilink")
[Category:倫敦政治經濟學院校友](../Category/倫敦政治經濟學院校友.md "wikilink")
[Category:香港編輯](../Category/香港編輯.md "wikilink")
[少綿](../Category/蔡姓.md "wikilink")

1.
2.
3.
4.