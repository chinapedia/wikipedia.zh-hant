**維拉本紀**
*Valaquenta*，是《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》的第二部分，主要是描述[維拉](../Page/維拉.md "wikilink")
*Valar*（主神）和[邁雅](../Page/邁雅.md "wikilink") *Maiar*（次等神）。

## 內容

維拉（代表力量）這個名字是表示十四位首先進入[阿爾達的](../Page/阿爾達.md "wikilink")[埃努](../Page/埃努.md "wikilink")。阿爾達由眾生萬物之父[一如·伊露維塔創造的](../Page/伊露維塔.md "wikilink")(參見[埃努的大樂章](../Page/埃努的大樂章.md "wikilink"))。最強而有力維拉是[米爾寇](../Page/米爾寇.md "wikilink")；他後來轉向罪惡，因此被除名。除了他，還有其它十四位維拉，人類稱他們為諸神。

[伊露維塔創造有男性和女性的形式](../Page/伊露維塔.md "wikilink")。因而，有七位男性（維拉），和七位女性（[維麗](../Page/維麗.md "wikilink")
*Valier*)。一些維拉被認為是兄弟姐妹或互相結成配偶，它不是意味他們像凡人那種聯繫，他們不過是由性情相似而結伴而已。十四位維拉之中，有八位維拉擁有有最強大的力量(他們統稱叫[雅睿塔爾](../Page/雅睿塔爾.md "wikilink")
*Aratar*).
每個[雅睿塔爾負責掌管阿爾達生活某一屬性](../Page/雅睿塔爾.md "wikilink")，譬如工藝和採礦或農業成長。眾維拉和[邁雅及阿爾達所有生靈的最高君皇是](../Page/邁雅.md "wikilink")[曼威](../Page/曼威.md "wikilink")。

除維拉之外，還有更多數的[埃努成為邁雅](../Page/埃努.md "wikilink")，維拉統治邁雅。他們以維拉的學生和助理的身份協助維拉治理阿爾達。維拉（和最初的米爾寇）都有能力改變他們的形態，甚至放棄形體。還有一些邁雅力量可以與維拉媲美;但他們能力並非不可限量的。

## 一神

  - [一如·伊露維塔](../Page/伊露維塔.md "wikilink")　 *Eru Ilúvatar*

## 維拉及邁雅

  - [維拉](../Page/維拉.md "wikilink")
      - [曼威](../Page/曼威.md "wikilink")·甦利繆　*Manwë Súlimo*
      - [烏歐牟](../Page/烏歐牟.md "wikilink")　*Ulmo*
      - [奧力](../Page/奧力.md "wikilink")·馬哈爾　*Aulë Mahal*
      - [歐羅米](../Page/歐羅米.md "wikilink")·奧達隆　*Oromë Aldaron*
      - 內牟（[曼督斯](../Page/曼督斯.md "wikilink")）　*Námo (Mandos)*
      - 伊爾牟（[羅瑞安](../Page/羅瑞安.md "wikilink")）　*Irmo (Lórien)*
      - [托卡斯](../Page/托卡斯.md "wikilink")·阿斯佗多　*Tulkas Astaldo*
  - [維麗](../Page/維麗.md "wikilink")
      - [瓦爾妲](../Page/瓦爾妲.md "wikilink")·埃蘭帖瑞　*Varda Elentári*
      - [雅凡娜](../Page/雅凡娜.md "wikilink")·齊門泰芮　*Yavanna Kementári*
      - [妮娜](../Page/妮娜.md "wikilink")　*Nienna*
      - [伊絲緹](../Page/伊絲緹.md "wikilink")　*Estë*
      - [薇瑞](../Page/薇瑞.md "wikilink")　*Vairë*
      - [威娜](../Page/威娜.md "wikilink")　*Vána*
      - [妮莎](../Page/妮莎.md "wikilink")　*Nessa*
  - 天魔王
      - 米爾寇·[魔苟斯](../Page/魔苟斯.md "wikilink")·包格力爾　*Melkor Morgoth
        Bauglir*
  - [邁雅](../Page/邁雅.md "wikilink")
      - [伊昂威](../Page/伊昂威.md "wikilink")　*Eönwë*
      - [伊爾瑪瑞](../Page/伊爾瑪瑞.md "wikilink")　*Ilmarë*
      - [歐希](../Page/歐希.md "wikilink")　*Ossë*
      - [烏妮](../Page/烏妮.md "wikilink")　*Uinen*
      - [索瑪爾](../Page/索瑪爾.md "wikilink")　*Salmar*
      - [索倫](../Page/索倫.md "wikilink")　*Sauron*
      - [美麗安](../Page/美麗安.md "wikilink")　*Melian*
      - [雅瑞恩](../Page/雅瑞恩.md "wikilink")　*Arien*
      - [提里昂](../Page/提里昂.md "wikilink")　*Tilion*
      - [勾斯魔格](../Page/勾斯魔格.md "wikilink")　*Gothmog*
      - 庫路耐爾（[薩魯曼](../Page/薩魯曼.md "wikilink")）　*Curunír*（*Saruman*）
      - 歐絡因（[甘道夫](../Page/甘道夫.md "wikilink")）　*Olórin*（*Gandalf*）
      - 阿溫代（[瑞達加斯特](../Page/瑞達加斯特.md "wikilink")）　*Aiwendel*（*Radagast*）
      - 阿拉塔（[摩列達](../Page/摩列達.md "wikilink")）　*Alatar*（*Morinehtar*）
      - 伯兰多（[羅密斯達奴](../Page/羅密斯達奴.md "wikilink")）　*Pallando*（*Rómestámo*）
      - [炎魔](../Page/炎魔.md "wikilink") *Durin's Bane*

[it:Il
Silmarillion\#Valaquenta](../Page/it:Il_Silmarillion#Valaquenta.md "wikilink")
[pl:Valaquenta](../Page/pl:Valaquenta.md "wikilink")
[ru:Валаквента](../Page/ru:Валаквента.md "wikilink")

[Category:中土大陸書本](../Category/中土大陸書本.md "wikilink") [Valaquenta,
The](../Category/精靈寶鑽.md "wikilink")