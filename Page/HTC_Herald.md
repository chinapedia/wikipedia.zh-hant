**HTC Herald**，原廠型號**HTC P4350，HTC
P4351**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink") 5，[HTC
Wizard的升級版](../Page/HTC_Wizard.md "wikilink")，配備[TI](../Page/TI.md "wikilink")
OMAP850
201MHz處理器，配有側滑動式[QWERTY鍵盤](../Page/QWERTY.md "wikilink")，性能強大。2006年12月於歐洲首度發表。已知客製版本HTC
P4350，HTC P4351，Dopod C800，Dopod C858，O2 Xda Terra，Vodafone VPA Compact
IV，T-Mobile Wing。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP850 201MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：109 毫米 X 59 毫米 X 17 毫米
  - 重量：168g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/EDGE
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，不支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1130mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC P4350 概觀](http://www.htc.com/www/product.aspx?id=484)
  - [HTC P4350 技術規格](http://www.htc.com/www/product.aspx?id=488)
  - [HTC P4351
    概觀](https://web.archive.org/web/20081114095758/http://www.htc.com/la/product.aspx?id=18920)
  - [HTC P4351
    技術規格](https://web.archive.org/web/20081120062138/http://www.htc.com/la/product.aspx?id=18924)

[H](../Category/智能手機.md "wikilink")
[Herald](../Category/宏達電手機.md "wikilink")