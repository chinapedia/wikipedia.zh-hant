[Aigle_fascié_MHNT.jpg](https://zh.wikipedia.org/wiki/File:Aigle_fascié_MHNT.jpg "fig:Aigle_fascié_MHNT.jpg")

**非洲隼雕**（[學名](../Page/學名.md "wikilink")：）是一種[猛禽](../Page/猛禽.md "wikilink")，牠們一般會在多[樹木的](../Page/樹木.md "wikilink")[山上出現](../Page/山.md "wikilink")，在樹木的交叉[枝幹上建造一個](../Page/枝幹.md "wikilink")[直徑大約](../Page/直徑.md "wikilink")1[米的](../Page/米.md "wikilink")[鳥巢](../Page/巢.md "wikilink")。

非洲隼雕身長55到68[公分](../Page/公分.md "wikilink")，上半身呈[黑色](../Page/黑色.md "wikilink")。牠們會[狩獵](../Page/狩獵.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")、[爬行動物及](../Page/爬行動物.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，牠們會發出[尖銳的](../Page/尖銳.md "wikilink")*kluu-kluu-kluu*的[叫聲](../Page/叫聲.md "wikilink")。

## 參考資料

  - Database entry includes justification for why this species is of
    least concern

  - Barlow, Wacher and Disley. *Birds of The Gambia*. ISBN 1-873403-32-1

  - Collinson, M. ''Splitting headaches? Recent taxonomic changes
    affecting the British and Western Palaearctic lists'. [British
    Birds](../Page/British_Birds_\(magazine\).md "wikilink") vol 99
    (June 2006), 306-323

  - Lerner, H. R. L. and D. P. Mindell. (2005) *Phylogeny of eagles, Old
    World vultures, and other Accipitridae based on nuclear and
    mitochondrial DNA*. Molecular Phylogenetics and Evolution (37)
    327–346.

[Category:真鵰屬](../Category/真鵰屬.md "wikilink")