**千年女優**（），為[日本](../Page/日本.md "wikilink")[動畫](../Page/動畫.md "wikilink")[導演](../Page/導演.md "wikilink")[今敏執導的第二部](../Page/今敏.md "wikilink")[動畫](../Page/動畫.md "wikilink")[電影](../Page/電影.md "wikilink")，以回憶、[劇中劇的方式](../Page/劇中劇.md "wikilink")，講述了女主角藤原千代子曲折的一生。2002年9月14日在日本首映。

## 劇情簡介

《千年女優》的劇情結構屬於一種[戲中戲風格](../Page/戲中戲.md "wikilink")。

立花源也導演為了替女[演員藤原千代子拍攝](../Page/演員.md "wikilink")[紀錄片](../Page/紀錄片.md "wikilink")，於是造訪她的住所。藤原千代子曾是相當知名的女演員，後來卻在事業的巔峰時選擇引退，這讓立花源也感到困惑。於是立花源也藉由與藤原千代子的對談中慢慢進入了她的內心世界。

## 登場人物

  - 藤原千代子

<!-- end list -->

  -
    配音：（老年）、[小山茉美](../Page/小山茉美.md "wikilink")（成年）、[折笠富美子](../Page/折笠富美子.md "wikilink")（少女）
    是一位具有傳奇色彩的女演員，曾經紅極一時，卻又在事業的巔峰時期選擇退隱。
    曾說過自己從出生開始跟地震少不了關係，曾在拍攝最後一部電影發生地震時被當時新來的源也拯救。

<!-- end list -->

  - 立花源也

<!-- end list -->

  -
    配音：[飯塚昭三](../Page/飯塚昭三.md "wikilink")（中年）、
    是一位[導演](../Page/導演.md "wikilink")，目前所屬於自己開的〈LOTUS工作室〉。為了拍攝[紀錄片拜訪藤原千代子](../Page/紀錄片.md "wikilink")，也是她的忠實影迷。
    年輕時曾擔任過銀映製片廠的其中一位工作人員之一。
    後來從老憲兵口中得知千代子所仰慕的那位畫家死於拷問。
    鑰匙在千代子拍攝最後一部電影時因地震的掉落，後被他撿走。

<!-- end list -->

  - 井田恭二

<!-- end list -->

  -
    配音：[小野坂昌也](../Page/小野坂昌也.md "wikilink")
    立花源也的攝影師助理，說話時操著關西腔調，唯一不入戲的正常人。

<!-- end list -->

  - 島尾詠子

<!-- end list -->

  -
    配音：[津田匠子](../Page/津田匠子.md "wikilink")
    與千代子一起合作演出的女演員。

<!-- end list -->

  - 大滝諄一

<!-- end list -->

  -
    配音：[鈴置洋孝](../Page/鈴置洋孝.md "wikilink")
    電影導演，同時也是銀映専務的外甥。於戰後的50年代與千代子結婚。

<!-- end list -->

  - 美濃

<!-- end list -->

  -
    配音：[片岡富枝](../Page/片岡富枝.md "wikilink")
    於千代子退隱之後，在故居照顧千代子的婦人。

<!-- end list -->

  - 番頭

<!-- end list -->

  -
    配音：[石森達幸](../Page/石森達幸.md "wikilink")
    任職於千代子家的僕人領頭。在千代子得知擁有鑰匙的那人在自家的藏身處被發現後以眼神示意千代子說擁有鑰匙的那人平安無事，然後快去火車站找他。

<!-- end list -->

  - 銀映専務

<!-- end list -->

  -
    配音：
    銀映製片廠的專務，看中了千代子的才華後就挖角進去。

<!-- end list -->

  - 千代子的母親

<!-- end list -->

  -
    配音：[京田尚子](../Page/京田尚子.md "wikilink")
    起初不同意女兒千代子投入演藝圈的工作，後來在千代子遇到擁有鑰匙的那人、堅持決定加入銀映製片廠，然後去[滿州國拍戲後就不再有意見](../Page/滿州國.md "wikilink")。

<!-- end list -->

  - 擁有鑰匙的人

<!-- end list -->

  -
    配音：[山寺宏一](../Page/山寺宏一.md "wikilink")
    千代子在少女時期遇到的年輕人。喜歡繪畫，曾說過在戰爭結束後想要回到北海道故鄉畫雪原風景的想法，唯一的信物是鑰匙。因為被追殺常在千代子的回憶裡面交錯出現，後證實已經在二戰時被拷問死亡，而當時已經失去鑰匙的千代子仍然還不知情。

<!-- end list -->

  - 疤臉男

<!-- end list -->

  -
    配音：
    從一開始就以不同的身分來阻擋千代子與擁有鑰匙的那人相見的中年男子，最後以白髮蒼蒼的老人身分把書信交給當時的千代子，同時也向源也說出了關於擁有鑰匙的那人後來的真相。
    其身分是憲兵，專門逮捕反戰份子。

## 風格

藤原千代子的一生從頭到尾貫穿了《千年女優》，並表現在場景、角色與視覺風格的快速變換中。一些場景以日本[浮世繪版畫來呈現](../Page/浮世繪.md "wikilink")，並讓人聯想到[黑澤明的電影](../Page/黑澤明.md "wikilink")（特別是《[蜘蛛巢城](../Page/蜘蛛巢城.md "wikilink")》）。今敏承認的確是受到《蜘蛛巢城》的影響，但是也說大部分並未受到其他特定的影響。

藤原千代子這個角色多少暗喻著[原節子這位演員](../Page/原節子.md "wikilink")，她於1940年代至1960年代相當活躍，而且也同樣突然宣佈退出影壇。今敏在接受訪問時承認受到原節子的影響，也參考了[高峰秀子](../Page/高峰秀子.md "wikilink")，但他也堅持藤原千代子是一個普通的人類角色。

## 反應

《千年女優》普遍得到相當正面的評價，在[爛番茄得到](../Page/爛番茄.md "wikilink")94%的正面評價\[1\]。[洛杉磯時報的影評人](../Page/洛杉磯時報.md "wikilink")[肯尼思·圖蘭](../Page/肯尼思·圖蘭.md "wikilink")（Kenneth
Turan）說這部電影「表達對於個人及集體潛意識的反思，《千年女優》這樣迷人的風格在過去並不常見」\[2\]。[芝加哥論壇報的凱文](../Page/芝加哥論壇報.md "wikilink")·M·威廉斯給予《千年女優》4顆星的評價，並評論道：「這部電影是電影藝術的展現，它是日本現代動畫中最好的作品……它是一部動畫，但也具有人性，而且將會觸碰任何深愛別人的人的[靈魂](../Page/靈魂.md "wikilink")\[3\]。」

《千年女優》名列在[網路電影資料庫的史上最佳](../Page/網路電影資料庫.md "wikilink")50大動畫中，而且獲得7.9的分數\[4\]。

在台灣，此片曾在[公共電視台播放](../Page/公共電視台.md "wikilink")。\[5\]\[6\]

### 票房

《千年女優》只在美國有限度的上映（僅有6家戲院），在3個禮拜中的票房為37,285[美元](../Page/美元.md "wikilink")\[7\]。這部動畫幾乎只在[紐約與](../Page/紐約.md "wikilink")[洛杉磯上映](../Page/洛杉磯.md "wikilink")，並只有非常少數的[廣告公開播放](../Page/廣告.md "wikilink")。

## 獎項

《千年女優》也被提名參加[奧斯卡獎最佳動畫獎](../Page/奧斯卡獎最佳動畫獎.md "wikilink")，不過最終並未入圍這個獎項。

  - 第5屆2001年[文部省](../Page/文部省.md "wikilink")[文化廳媒體藝術祭動畫部門大賞](../Page/文化廳媒體藝術祭.md "wikilink")。（與《[神隱少女](../Page/神隱少女.md "wikilink")》並列）
  - 第6屆[幻想電影節](../Page/幻想電影節.md "wikilink")（Fantasia
    Festival）・最優秀動曼作品賞與藝術的革新賞
  - 第33屆[加泰隆尼亞國際電影節](../Page/加泰隆尼亞國際電影節.md "wikilink")・最優秀動畫電影作品賞
  - 2003年度[東京國際影展](../Page/東京國際影展.md "wikilink")・劇情電影部門最優秀作品賞
  - 第57屆[每日電影獎](../Page/每日電影獎.md "wikilink")・[大藤信郎賞](../Page/大藤信郎賞.md "wikilink")
  - 第8屆[動畫神戶獎作品賞](../Page/動畫神戶獎.md "wikilink")・劇場部門
  - 入圍2002年度[安妮獎最佳動畫成就獎](../Page/安妮獎.md "wikilink")、最佳動畫導演獎、最佳動畫配音獎、最佳動畫編劇獎
  - 入圍2004年度[衛星獎最佳動畫獎](../Page/衛星獎.md "wikilink")
  - 入圍[網絡影評人協會獎](../Page/網絡影評人協會獎.md "wikilink")（Online Film Critics
    Society）最佳動畫獎

## 製作群

  - 原案・編劇・人物設定・導演：[今敏](../Page/今敏.md "wikilink")
  - 編劇：[村井貞之](../Page/村井貞之.md "wikilink")
  - 演出：[松尾衡](../Page/松尾衡.md "wikilink")
  - 人物設定・作畫監督：[本田雄](../Page/本田雄.md "wikilink")
  - 作畫監督：[井上俊之](../Page/井上俊之.md "wikilink")、濱洲英喜、[小西賢一](../Page/小西賢一.md "wikilink")、古屋勝悟
  - 美術監督：[池信孝](../Page/池信孝.md "wikilink")
  - 攝影監督：白井久男
  - 音響監督：[三間雅文](../Page/三間雅文.md "wikilink")
  - 音樂：[平澤進](../Page/平澤進.md "wikilink")
  - 動畫製作：[MADHOUSE](../Page/MADHOUSE.md "wikilink")
  - 發行：[KLOCK WORX](../Page/KLOCK_WORX.md "wikilink")
  - 製作：[角川書店](../Page/角川書店.md "wikilink")・[WOWOW](../Page/WOWOW.md "wikilink")・KLOCK
    WORX・[萬代影視](../Page/萬代影視.md "wikilink")・[GENCO](../Page/GENCO.md "wikilink")

## 配音

  - 藤原千代子（70歲）：[莊司美代子](../Page/莊司美代子.md "wikilink")
  - 藤原千代子（20～40歲）：[小山茉美](../Page/小山茉美.md "wikilink")
  - 藤原千代子（10～20歲）：[折笠富美子](../Page/折笠富美子.md "wikilink")
  - 立花源也：[飯塚昭三](../Page/飯塚昭三.md "wikilink")
  - 立花源也（青年期）：[佐藤政道](../Page/佐藤政道.md "wikilink")
  - 井田恭二：[小野坂昌也](../Page/小野坂昌也.md "wikilink")
  - 島尾詠子：[津田匠子](../Page/津田匠子.md "wikilink")
  - 大滝諄一：[鈴置洋孝](../Page/鈴置洋孝.md "wikilink")
  - 美濃：[片岡富枝](../Page/片岡富枝.md "wikilink")
  - 番頭：[石森達幸](../Page/石森達幸.md "wikilink")
  - 銀映専務：
  - 千代子的母：[京田尚子](../Page/京田尚子.md "wikilink")
  - 擁有鑰匙的人：[山寺宏一](../Page/山寺宏一.md "wikilink")
  - 疤脸男：
  - 其他：[小形滿](../Page/小形滿.md "wikilink")、、[遊佐浩二](../Page/遊佐浩二.md "wikilink")、、、、[木村亞希子](../Page/木村亞希子.md "wikilink")、[佐伯智](../Page/佐伯智.md "wikilink")、[野島裕史](../Page/野島裕史.md "wikilink")、[淺野琉璃](../Page/淺野琉璃.md "wikilink")、[大中寬子](../Page/大中寬子.md "wikilink")、、

## 主題歌

  - 『ロタティオン［LOTUS-2］』

<!-- end list -->

  -
    作詞・作曲・編曲：[平澤進](../Page/平澤進.md "wikilink")

## 參考資料

<references/>

## 外部連結

  -
  - 《[千年女優](https://web.archive.org/web/20060520031712/http://1000.kingnet.com.tw/)》
    的中文介紹網站

  - [千年女優](https://web.archive.org/web/20070927024657/http://www.animax.co.jp/feature/index.php?program=NN10000177)
    在[Animax的官方網站](../Page/Animax.md "wikilink")

  - [千年女優官方網站](https://web.archive.org/web/20080329134336/http://www.millenniumactress-themovie.com/)

  - [Chiyoko, the Millennium
    Actress](http://www.dan42.com/millennium.actress/)

  - \[<http://www.keyframeonline.com/Animation/Millennium_Actress/228/>*Millennium
    Actress*\] at Keyframe - the Animation Resource

  -
  -
[Category:2002年日本劇場動畫](../Category/2002年日本劇場動畫.md "wikilink")
[Category:電影題材電影](../Category/電影題材電影.md "wikilink")
[Category:今敏電影](../Category/今敏電影.md "wikilink")
[Category:日本原創動畫電影](../Category/日本原創動畫電影.md "wikilink")

1.
2.
3.
4.
5.
6.
7.