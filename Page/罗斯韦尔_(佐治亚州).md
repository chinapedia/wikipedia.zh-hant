**罗斯韦尔**（）是[美國](../Page/美國.md "wikilink")[佐治亚州](../Page/佐治亚州.md "wikilink")[富爾頓縣
(喬治亞州)的其中一個](../Page/富爾頓縣_\(喬治亞州\).md "wikilink")[城市](../Page/城市.md "wikilink")。

## 參考資料

  - [The New Georgia Encyclopedia entry for Roswell,
    Georgia](http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-956)

  - [Roswell Covention & Vistors Bureau Brief History of
    Roswell](https://web.archive.org/web/20060615072133/http://www.cvb.roswell.ga.us/about2.html)

  - [Official Roswell Memorial Day Celebration
    site](http://www.roswellmemorialday.com/)

  - <https://web.archive.org/web/20070307234507/http://www.tomprice.com/>

  - [Official Roswell Recreation and Parks
    site](https://web.archive.org/web/20070105134304/http://roswellgov.com/index.php/p/26/t/Recreation%20and%20Parks)

  - Sherron D. Lawson, A Guide to the Historic Textile Mill Town of
    Roswell, Georgia (Roswell, Ga.: Roswell Historical Society, 1996).

  -
## 外部链接

  - [City website](http://www.roswellgov.com/)
  - [Historic Roswell Convention & Visitors
    Bureau](https://web.archive.org/web/20060404121214/http://www.cvb.roswell.ga.us/)
  - [Keep Roswell Beautiful](http://www.keeproswellbeautiful.org)
  - [History of Roswell,
    Georgia](http://www.roadsidegeorgia.com/city/roswell.html)
  - [Chattahoochee Nature Center](http://www.chattnaturecenter.com/)
  - [Roadside Georgia's list of things to do in
    Roswell](http://roadsidegeorgia.com/sitecity/Roswell)
  - [State of Georgia website info on
    Roswell](http://roswell.georgia.gov/05/home/0,2230,8343605,00.html)
  - [Georgiainfo.com maps of Fulton County and surrounding area over the
    years](https://web.archive.org/web/20070812011056/http://www.cviog.uga.edu/Projects/gainfo/histcountymaps/fultonhistmaps.htm)
  - [Assorted publications about Roswell by Michael
    Hitt](https://web.archive.org/web/20070807001519/http://www.hittdesign.com/Publications.htm)
  - [Alpharetta Roswell Revue & News](http://www.northfulton.com/)

[R](../Category/佐治亚州城市.md "wikilink")