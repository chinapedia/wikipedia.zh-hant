**島田洋七**（），本名**德永昭廣**，日本漫才（[相聲](../Page/相聲.md "wikilink")）師、[作家](../Page/作家.md "wikilink")。日本漫才大師[島田洋之助門下](../Page/島田洋之助.md "wikilink")，與師弟[島田洋八](../Page/島田洋八.md "wikilink")（本名：藤井健次）組成漫才二人組[B\&B](../Page/B&B.md "wikilink")。

他在[NHK的漫才大賽獲得最優秀新人賞](../Page/NHK.md "wikilink")，在1980年代的[日本掀起相聲熱潮](../Page/日本.md "wikilink")，雖曾一度解散，但現仍活躍於電視和舞台上。

島田洋七將童年與外婆生活8年的故事寫成《[佐賀的超級阿嬤](../Page/佐賀的超級阿嬤.md "wikilink")》，在2003年夏天由[日本最受歡迎的](../Page/日本.md "wikilink")[談話節目](../Page/談話節目.md "wikilink")「[徹子的房間](../Page/徹子的房間.md "wikilink")」主持人[黑柳徹子專訪](../Page/黑柳徹子.md "wikilink")，《[佐賀的超級阿嬤](../Page/佐賀的超級阿嬤.md "wikilink")》的內容掀起話題。起今熱銷超過50萬冊，已經被改編成[電影上映](../Page/電影.md "wikilink")。

## 生平

### 佐賀

島田洋七生於1950年的[日本](../Page/日本.md "wikilink")[廣島縣](../Page/廣島縣.md "wikilink")。1945年父親在[美軍在](../Page/美國.md "wikilink")[廣島投下](../Page/廣島.md "wikilink")[原子彈之後](../Page/原子彈.md "wikilink")，一個人回[廣島](../Page/廣島.md "wikilink")，因當時仍有[輻射污染而得了](../Page/輻射.md "wikilink")[原爆症](../Page/原爆症.md "wikilink")，不久後死亡。

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，島田洋七曾寄住在其他[阿姨的家裡](../Page/阿姨.md "wikilink")。後來他的[媽媽在](../Page/媽媽.md "wikilink")[廣島](../Page/廣島.md "wikilink")[原爆紀念館旁邊開了家](../Page/原爆紀念館.md "wikilink")[居酒屋撫養他與他的哥哥](../Page/居酒屋.md "wikilink")，他與哥哥租住在媽媽開的[居酒屋附近](../Page/居酒屋.md "wikilink")，一間六個[榻榻米大小的公寓](../Page/榻榻米.md "wikilink")。因其年幼，非常依戀母親，故其在上[小學後會在](../Page/小學.md "wikilink")[三更](../Page/三更.md "wikilink")[半夜溜出公寓到媽媽開的居酒屋](../Page/半夜.md "wikilink")。其母非常擔心，於是與洋七的阿姨進行了一個秘密計畫。洋七的阿姨在洋七小學二年級的時候來到[廣島照顧洋七](../Page/廣島.md "wikilink")，後來洋七的媽媽在跟洋七的阿姨送行時偷偷將洋七推上火車，由洋七的阿姨將洋七送到在[佐賀的外婆家由外婆撫養](../Page/佐賀.md "wikilink")。

洋七的[外婆是個](../Page/外婆.md "wikilink")[樂觀的](../Page/樂觀.md "wikilink")[赤貧階級人士](../Page/赤貧.md "wikilink")，在洋七住在[佐賀的八年間辛苦撫養洋七長大](../Page/佐賀.md "wikilink")，洋七長大後將這八年間的事情寫成《[佐賀的超級阿嬤](../Page/佐賀的超級阿嬤.md "wikilink")》、《[佐賀阿嬤　笑著活下去！](../Page/佐賀阿嬤_笑著活下去！.md "wikilink")》與《[佐賀阿嬤的幸福旅行箱](../Page/佐賀阿嬤的幸福旅行箱.md "wikilink")》。他在期間就讀[佐賀市立赤松小學校與](../Page/佐賀市立赤松小學校.md "wikilink")[佐賀市立城南中學校](../Page/佐賀市立城南中學校.md "wikilink")，是[城南中學校的棒球隊長](../Page/佐賀市立城南中學校.md "wikilink")，後來因獲准以[公費生的名義進入](../Page/公費生.md "wikilink")[廣島的](../Page/廣島.md "wikilink")[廣陵高校而離開](../Page/廣陵高校.md "wikilink")。

### 廣陵高校

島田洋七在進到廣陵高校的棒球隊時，在練習時被打傷後，便退出棒球社。

## 著作

  - [佐賀的超級阿嬤](../Page/佐賀的超級阿嬤.md "wikilink")
      - 陳寶蓮譯
      - [先覺出版](../Page/先覺.md "wikilink")
      - 原書名「」
      - 原書出版商：
        1.  [月光工廠](../Page/月光工廠.md "wikilink")
        2.  [德間書店](../Page/德間書店.md "wikilink")
  - [佐賀阿嬤　笑著活下去！](../Page/佐賀阿嬤_笑著活下去！.md "wikilink")
      - 陳寶蓮譯
      - [先覺出版](../Page/先覺.md "wikilink")
      - 原書名「」
      - 原書出版商：
        1.  [月光工廠](../Page/月光工廠.md "wikilink")
        2.  [德間書店](../Page/德間書店.md "wikilink")
  - [佐賀阿嬤的幸福旅行箱](../Page/佐賀阿嬤的幸福旅行箱.md "wikilink")
      - 陳寶蓮譯
      - [先覺出版](../Page/先覺.md "wikilink")
  - [媽媽，我很想妳](../Page/媽媽，我很想妳.md "wikilink")
      - 陳寶蓮譯
      - [先覺出版](../Page/先覺.md "wikilink")
  - [阿嬤，我要打棒球！](../Page/阿嬤，我要打棒球！.md "wikilink")
      - 羊恩媺譯
      - [平安文化出版](../Page/平安文化.md "wikilink")

## 參見

  - [佐賀的超級阿嬤](../Page/佐賀的超級阿嬤.md "wikilink")

## 外部連結

[島田洋七的官方網站](http://www.gabai-youchan.com/)

[Category:日本男性搞笑藝人](../Category/日本男性搞笑藝人.md "wikilink")
[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")