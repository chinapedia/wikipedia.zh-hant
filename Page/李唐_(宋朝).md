**李唐**（），字**晞古**，亦作**希古**，[宋朝著名](../Page/宋朝.md "wikilink")[画家](../Page/画家.md "wikilink")，河阳三城（今[河南](../Page/河南.md "wikilink")[孟县](../Page/孟县.md "wikilink")）人。

## 生平

李唐原供奉[北宋](../Page/北宋.md "wikilink")[徽宗的画院](../Page/宋徽宗.md "wikilink")，1127年金兵攻陷[汴京](../Page/汴京.md "wikilink")，李唐颠沛流离，逃往临安（今[杭州](../Page/杭州.md "wikilink")），以卖画度日。[南宋恢复画院后](../Page/南宋.md "wikilink")，李唐经人举荐，进入画院，授成忠郎职务。

李唐精于[山水画](../Page/山水画.md "wikilink")，变[荆浩](../Page/荆浩.md "wikilink")、[范宽之法](../Page/范宽.md "wikilink")，山水画最初用峭劲的笔墨，写出北方山河雄峻气势；晚年去繁就简，创[大斧劈皴](../Page/斧劈皴.md "wikilink")；画水打破鱼鳞纹程式，而得盘涡动荡之状；兼工人物画，初似[李公麟](../Page/李公麟.md "wikilink")，后衣褶变为方折劲硬；并以画牛著称。

描绘青绿山水，如《[万壑松风图](../Page/万壑松风图.md "wikilink")》、《长夏江寺图》等，是北派山水的著名代表人之一，其后用笔及取景变的简括凝炼、[构图精练](../Page/构图.md "wikilink")、意境优美，开辟南宋的新画风，并创作人物画《[胡笳十八拍](../Page/胡笳十八拍.md "wikilink")》、《采薇图》等，借[历史抒发怀念故国](../Page/历史.md "wikilink")，希图中兴的感情。

李唐的画风对后世有很大影响，他也培养了一些傑出弟子如蕭照，其后[马远](../Page/马远.md "wikilink")、[夏圭继承和发展了他的画风](../Page/夏圭.md "wikilink")，和他一起，并称为「南宋四家」（李唐、[刘松年](../Page/刘松年.md "wikilink")、[马远](../Page/马远.md "wikilink")、[夏圭](../Page/夏圭.md "wikilink")）。

{{-}}

[L李](../Category/宋朝畫家.md "wikilink") [L李](../Category/孟州人.md "wikilink")
[T唐](../Category/李姓.md "wikilink")