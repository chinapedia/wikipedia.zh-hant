**M1卡賓槍**（M1
Carbine）是[美國在](../Page/美國.md "wikilink")[二戰及](../Page/二戰.md "wikilink")[韓戰的制式輕型半自動卡宾枪](../Page/韓戰.md "wikilink")，有多種衍生型，是二戰中美國使用最廣泛的武器之一。

## 歷史

1938年，美國陸軍要求為軍官、軍士、車組成員、機槍手、通訊兵及其他不便攜帶全尺寸步槍的士兵，配備一種介乎於[步槍與](../Page/步槍.md "wikilink")[手槍之間的重量不超過](../Page/手槍.md "wikilink")2.5公斤的輕型步槍，作為輕型自衛武器用以取代手槍和[衝鋒槍](../Page/衝鋒槍.md "wikilink")。1940年美國軍方批准展開研製計劃，由[溫徹斯特連發武器公司設計的樣槍以及彈藥被美軍選中](../Page/溫徹斯特連發武器公司.md "wikilink")，1941年定型，命名為M1卡賓槍，並於1942年進入現役裝備部隊。直到[二戰結束](../Page/二戰.md "wikilink")，M1卡賓槍共生產了超過六百萬支，这个数字超过了M1加兰德步枪的产量。M1卡賓槍，主要被二戰時的美國海軍陸戰隊使用。

在太平洋战场，M1卡宾枪因体积小，重量轻，火力较强而受到不经常与敌军直接作战的士兵或游击队员的好评。\[1\]但日常与敌军作战的前线士兵（特别是在菲律宾作战的士兵）发现该枪的穿透力及均不足。\[2\]\[3\]虽然M1卡宾枪使用的.30卡宾枪弹可以轻松穿透钢盔以及当时日军所使用的护甲，\[4\]\[5\]但是敌军在中弹甚至多次中弹后仍未丧失战斗力的记录多次出现在美军各方战后报告（after-action
reports）中。\[6\]\[7\]

起初，M1卡宾枪原打算具有全自动射击的能力，但由于新型卡宾枪需要快速生产，该性能遂被取消。但在战争中，为应对德军广泛采用的全自动武器，诸如[StG44突击步枪](../Page/StG44突击步枪.md "wikilink")，美军于1944年10月采用M2卡宾枪与30发弹匣。M2卡宾枪在全自动模式下射速可以达到850至900发/分钟。虽然M2卡宾枪在战争尾声（1945年4月）才开始生产，但美国军械署分发了M1/2卡宾枪转换部件套装，士兵可用该工具在前线将M1卡宾枪改装而使其可全自动射击。这些改装的M1/2卡宾枪在二战尾声时得到了有限使用。\[8\]

朝鲜战争中，出现很多抱怨指称卡宾枪弹无法阻止身着厚重衣物\[9\]\[10\]\[11\]\[12\]或装备\[13\]\[14\]\[15\]的朝鲜人民军和[中国人民志愿军士兵](../Page/中国人民志愿军.md "wikilink")，甚至是在近距离以及多次命中的情况下。\[16\]\[17\]\[18\][海军陆战队第1师的陆战队员亦报告有卡宾枪弹无法阻止敌军士兵的情况](../Page/美国海军陆战队第1师.md "wikilink")，部分单位甚至对卡宾枪射手下达作战规定，要求其瞄准头部射击。\[19\]\[20\]志愿军中使用美军装备的步兵也因相同的原因而厌恶该型卡宾枪，缴获的M1/M2型卡宾枪一般装备传令员和迫击炮组员。\[21\]尽管毁誉参半，但是M2卡宾枪因其火力而常常成为美军在朝鲜战场夜间巡逻时的首选武器。\[22\]\[23\]士兵们经常会将两到三个弹匣用胶带固定在一起以提高换弹速度，即[弹匣并联](../Page/弹匣并联.md "wikilink")。（这一方式一般被称作
"Jungle Style"）\[24\]

越南战争期间，[南越接收了至少](../Page/越南共和国陆军.md "wikilink")793,994支M1与M2型卡宾枪，这些卡宾枪在越战期间得到了广泛使用。
战争期间[越共](../Page/越共.md "wikilink")\[25\]曾缴获该型卡宾枪，并将枪管或枪托锯短以方便使用。\[26\]虽然卡宾枪较轻的重量和较高的射速使得其适合身材相对较小的亚洲人使用，但是因为缺乏动能及穿透力使得这些卡宾枪无法与AK47自动步枪相比较。\[27\]

## 設計

M1卡賓槍採用導氣式操作原理，槍機回轉閉鎖方式，單發射擊，自動裝填子彈，由15發彈匣供彈，[槍托上可以附加攜帶兩個備用彈匣的彈匣袋](../Page/槍托.md "wikilink")。

M1卡賓槍所發射的[.30卡賓槍彈](../Page/.30卡賓槍彈.md "wikilink")（7.62×33毫米）基本上是使用的的无突缘版本。\[28\]标准的.30卡宾枪弹弹头重110格令（7.1克），使用M1卡宾枪的18英寸枪管射击时，枪口初速度为610
m/s，动能为1,311焦耳。\[29\]在的距离上，M1卡宾枪的散布在3英寸至5英寸之间，这对于一款近距离自卫武器来说已经足够。M1卡宾枪的最大射程为，但是弹头在超过后下坠严重，因此M1卡宾枪的实际有效射程大约为200码。\[30\]

M1卡宾枪最初服役时，使用15发标准直弹匣。1944年10月，\[31\]美军采用M2卡宾枪，30发弯曲弹匣（绰号“香蕉弹匣”）在此时进入美军服役。\[32\]二战后，30发弹匣很快成为了M1与M2卡宾枪的标准弹匣，但15发的直弹匣直到越战结束后才停止使用。\[33\]在初期的战斗报告中，士兵们经常会在战斗中误将弹匣释放按钮当做保险按下。随后，旋转式保险替代按钮式保险。\[34\]\[35\]

早期的M1卡賓槍不配[刺刀](../Page/刺刀.md "wikilink")，後來根據軍隊的要求，在槍管下方增加了\[36\]，並配備[M4刺刀](../Page/M4刺刀.md "wikilink")。M1卡賓槍具有質量輕、射擊時容易控制等優點。在[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，M1卡賓槍被認為是一種有效的近戰武器。但是在[朝鮮戰爭期間](../Page/朝鮮戰爭.md "wikilink")，在嚴寒低溫環境下M1卡賓槍的可靠性表現差劣。\[37\]\[38\]\[39\]主要原因是复进簧较为脆弱，润滑油在低温下冻结以及弹药在零度以下反冲力不足。\[40\]\[41\]

## 出口

[越南戰爭初期](../Page/越南戰爭.md "wikilink")，美國政府也有將M1卡賓槍作為軍事援助輸出，是[南越軍隊的主要武器](../Page/南越.md "wikilink")。M1卡賓槍也曾經是[聯邦德國](../Page/聯邦德國.md "wikilink")[巴伐利亞邦](../Page/巴伐利亞.md "wikilink")[警察以及](../Page/警察.md "wikilink")[以色列警察使用的武器](../Page/以色列.md "wikilink")。直至現在，[以色列仍擁有大量M](../Page/以色列.md "wikilink")1卡賓槍及其彈藥。

## 中国的M1卡宾枪

1945年，中国国民革命军宪兵装备了M1卡宾枪，在[国共内战期间流入](../Page/国共内战.md "wikilink")[解放军手中](../Page/中國人民解放军.md "wikilink")。

1949年夏季，中共中央军委下令研究仿制M1卡宾枪，属下兵工厂并曾试产了少量枪支。后因苏联提供兵工设备及技术（[53式步骑枪及](../Page/53式步骑枪.md "wikilink")[56式半自动步枪](../Page/56式半自动步枪.md "wikilink")），解放军才停止研究仿制M1卡宾枪的工作。此枪差点成为第一支解放军的制式半自动步枪/卡宾枪。

## 改進型號

### M1A1卡賓槍

[M1A1_Carbine_tri_army.jpg](https://zh.wikipedia.org/wiki/File:M1A1_Carbine_tri_army.jpg "fig:M1A1_Carbine_tri_army.jpg")

  - 1942年定型，共制造約150000把
  - 摺疊[槍托](../Page/槍托.md "wikilink")
  - 金屬製骨架式槍托，向左摺疊
  - 供空降部隊使用

### M1A2卡賓槍

  - 改進機械照門，加入風偏調節
  - 沒有投產

### M1A3卡賓槍

  - 由M1A1用的摺疊[槍托改為](../Page/槍托.md "wikilink")
  - 配備15發彈匣
  - 沒有投產，原意為替代M1A1卡賓槍但沒有落實

### M2卡賓槍

[M2CarbineGTAPage3.jpg](https://zh.wikipedia.org/wiki/File:M2CarbineGTAPage3.jpg "fig:M2CarbineGTAPage3.jpg")

  - 1944年定型
  - 增加[快慢機](../Page/擊發調變模式.md "wikilink")，可選擇單發、連發射擊模式
  - 配30發彈匣，但可與15發彈匣通用
  - 共制造約60000把（因為有眾多M1卡賓槍被改造成M2，所以實際數目比官方公佈為多）
  - 雖然有連射模式，但因為無，比有握把的突擊步槍較難控制

尽管军队对于该枪有需求，但M2卡宾枪在二战中的使用十分有限，且大多是在战争即将结束时的[太平洋战场](../Page/太平洋战场.md "wikilink")。\[42\]M2卡宾枪在后勤维护方面与M1卡宾枪并无太大差异，且其射程精度以及穿透性均强于诸如[汤普森冲锋枪与](../Page/汤普森冲锋枪.md "wikilink")[M3冲锋枪等使用手枪弹的冲锋枪](../Page/M3冲锋枪.md "wikilink")。\[43\]
因此，在二战之后，M2卡宾枪在美军中很大程度上替代了冲锋枪，直到其被M16步枪取代。\[44\]

### M3卡賓槍

  - 1945年定型
  - 裝有紅外夜視裝置，以T3鏡架配合
  - 加裝喇叭形
  - 服役於[韓戰及](../Page/韓戰.md "wikilink")[越戰](../Page/越戰.md "wikilink")
  - 共制造約2100把\[45\]

### 其他

[M1_carbine_kahr.jpg](https://zh.wikipedia.org/wiki/File:M1_carbine_kahr.jpg "fig:M1_carbine_kahr.jpg")

  - [以色列擁有大量M](../Page/以色列.md "wikilink")1卡賓槍及[.30
    Carbine彈藥](../Page/.30_Carbine.md "wikilink")，但由於過於老舊及只能單發射擊，以色列決定以保留原槍機部件的方式作改進，設計出[HEZI
    SM-1及](../Page/HEZI_SM-1.md "wikilink")[IMI
    Magal](../Page/IMI_Magal.md "wikilink")。HEZI
    SM-1是一種以M1卡賓槍改造成[犢牛式結構的連發](../Page/犢牛式.md "wikilink")[個人防衛武器](../Page/個人防衛武器.md "wikilink")；而IMI
    Magal則是以[Galil改造成使用](../Page/IMI_Galil突擊步槍.md "wikilink").30
    Carbine彈藥的卡賓槍。
  - [美國的](../Page/美國.md "wikilink")[儒格Mini-14](../Page/儒格Mini-14半自動步槍.md "wikilink")[半自动步槍以M](../Page/半自动步槍.md "wikilink")1卡賓槍及[M14自動步槍為基礎改進而成](../Page/M14自動步槍.md "wikilink")，而其連射版本AC-556/AC-556F也與M2卡賓槍相似。
  - 美國的民間市場亦有M1卡賓槍發售，更有多種口徑改裝版本，如[.22
    LR](../Page/.22_LR.md "wikilink")\[46\]。

## 使用彈藥類型

[.30卡賓槍彈軍用](../Page/.30卡賓槍彈.md "wikilink")：

  - M1普通彈（）
  - M6空包彈（用於發射槍榴彈，）
  - M13訓練彈（）
  - M18高壓試驗彈（）
  - M27曳光彈（）

## 使用國

[First_Iwo_Jima_Flag_Raising.jpg](https://zh.wikipedia.org/wiki/File:First_Iwo_Jima_Flag_Raising.jpg "fig:First_Iwo_Jima_Flag_Raising.jpg")的美國海軍陸戰隊士兵手持M1卡賓槍\]\]
[Carbine-iwo-jima-194502.jpg](https://zh.wikipedia.org/wiki/File:Carbine-iwo-jima-194502.jpg "fig:Carbine-iwo-jima-194502.jpg")\]\]
[Ethiopian_Soldiers_Korean_War.jpg](https://zh.wikipedia.org/wiki/File:Ethiopian_Soldiers_Korean_War.jpg "fig:Ethiopian_Soldiers_Korean_War.jpg")（右）參與[韓戰的埃塞俄比亞士兵](../Page/韓戰.md "wikilink")，攝於1953年\]\]
[M1-carbine02.jpg](https://zh.wikipedia.org/wiki/File:M1-carbine02.jpg "fig:M1-carbine02.jpg")

  -
  - －於[阿爾及利亞独立戰爭時從](../Page/阿爾及利亞独立戰爭.md "wikilink")[法軍手上繳獲到大量的M](../Page/法軍.md "wikilink")1卡賓槍。

  -
  -
  - －於1950年代至1970年代期間被奧地利軍隊及警察所採用。

  -
  - －於1944年至1945年期間被巴西遠征軍採用；現在則被[巴西聯邦憲兵特種部隊所採用](../Page/巴西聯邦憲兵特種部隊.md "wikilink")。

  -
  -
  -
  -
  - －[中華民國陸軍自](../Page/中華民國陸軍.md "wikilink")1945年裝備至1968年；而[中華民國憲兵則裝備至](../Page/中華民國憲兵.md "wikilink")1980年代，[內政部警政署亦曾裝備](../Page/內政部警政署.md "wikilink")。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - －於1954年至1962年期間裝備法國軍隊。

  - －於1950年代至1960年代期間裝備西德邊防衛隊、警察部隊和[空降兵](../Page/空降兵.md "wikilink")。

  - －被希臘空軍裝備至1990年代中期。

  - －格陵蘭警察裝備至今。

  -
  -
  - [英屬香港](../Page/英屬香港.md "wikilink") -
    曾被[皇家香港警察所採用](../Page/皇家香港警察.md "wikilink")，後來被[AR-15自動步槍所取代](../Page/AR-15自動步槍.md "wikilink")。\[47\]

  -
  -
  -
  - －於1969年至1980年代期間被臨時愛爾蘭共和軍所採用。

  - －[以色列國防軍由](../Page/以色列國防軍.md "wikilink")1945年裝備至1957年；以色列警察由1970年代裝備至今；以色列國民警衛隊由1974年裝備至今。

  - －被[卡賓槍騎兵裝備至](../Page/卡賓槍騎兵.md "wikilink")1992年。

  - －於1950年至1989年期間被[陸上自衛隊所採用](../Page/陸上自衛隊.md "wikilink")。

  -
  -
  -
  -
  -
  - －裝備警察部門和安全部隊。

  -
  - －繳獲自美軍。

  - －於1940年代至1970年代期間裝備[荷蘭軍隊及警察](../Page/荷蘭軍隊.md "wikilink")。

  -
  -
  - －[挪威國防軍自](../Page/挪威國防軍.md "wikilink")1951年裝備至1970年，而一些警察部門則裝備至1990年代。

  -
  -
  -
  - －二戰後使用。

  -
  - －於[韓戰時期大量作為](../Page/韓戰.md "wikilink")[韓國國軍之裝備](../Page/韓國國軍.md "wikilink")，至今[後備部隊仍有配備](../Page/大韓民國預備軍.md "wikilink")。

  - －於1960年代至70年代期間裝備[南越軍](../Page/南越軍.md "wikilink")，直至[越戰結束](../Page/越戰.md "wikilink")。

  -
  -
  -
  -
  - －於1943年至1960年代期間有限地被[英軍採用](../Page/英軍.md "wikilink")，皇家阿爾斯特警察則裝備至1980年代。

  - －於1941年被美軍採用，直至1970年代。而一些執法部門則裝備至今。

  -
  - －通過[租借法案獲得](../Page/租借法案.md "wikilink")7支M1卡賓槍。

  -
  -
## 流行文化

M1卡賓槍曾出現在二戰後拍攝的眾多電影和電視劇當中，例如：

  - 《[現代啟示錄](../Page/現代啟示錄.md "wikilink")》
  - 《[-{zh-cn:兄弟连; zh-tw:諾曼第大空降;
    zh-hk:雷霆傘兵;}-](../Page/兄弟连.md "wikilink")》
  - 《[-{zh-cn:父辈的旗帜; zh-tw:硫磺島的英雄們;
    zh-hk:雷霆戰海;}-](../Page/硫磺島的英雄們_\(電影\).md "wikilink")》
  - 《[來自硫磺島的信](../Page/來自硫磺島的信.md "wikilink")》
  - 《[登陸之日](../Page/登陸之日.md "wikilink")》
  - 《[-{zh-cn:血战太平洋; zh-tw:太平洋戰爭;
    zh-hk:戰火旗蹟;}-](../Page/太平洋戰爭_\(電視劇\).md "wikilink")》
  - 《[-{zh-cn:无耻混蛋; zh-tw:惡棍特工;
    zh-hk:希魔撞正殺人狂;}-](../Page/惡棍特工.md "wikilink")》
  - 《[喋血孤城](../Page/喋血孤城.md "wikilink")》
  - 《[-{zh-cn:实尾岛; zh-tw:實尾島風雲;
    zh-hk:實尾島;}-](../Page/實尾島風雲.md "wikilink")》
  - 《[驚異世紀](../Page/驚異世紀.md "wikilink")》

此外也有出現於電子遊戲，例如：

  - 《[-{zh-cn:战地; zh-hk:戰地風雲; zh-tw:戰地風雲;
    zh-mo:戰地風雲;}-系列](../Page/戰地風雲系列.md "wikilink")》
      - 《[-{zh-cn:战地：硬仗;
        zh-tw:戰地風雲：強硬路線;}-](../Page/战地：硬仗.md "wikilink")》
      - 《[-{zh-hans:战地5; zh-hant:戰地風雲5;}-](../Page/戰地風雲5.md "wikilink")》
  - 《[-{zh-cn:荣誉勋章; zh-tw:榮譽勳章;
    zh-hk:榮譽勳章;}-系列](../Page/荣誉勋章_\(游戏\).md "wikilink")》
      - 《[-{zh-cn:荣誉勋章：太平洋突袭; zh-tw:榮譽勳章：太平洋戰役;
        zh-hk:榮譽勳章：太平洋戰役;}-](../Page/荣誉勋章：太平洋突袭.md "wikilink")》
  - 《[-{zh-cn:使命召唤; zh-hk:使命召喚;
    zh-tw:決勝時刻;}-系列](../Page/決勝時刻系列.md "wikilink")》
      - 《[-{zh-cn:使命召唤; zh-hk:使命召喚;
        zh-tw:決勝時刻;}-](../Page/使命召喚_\(遊戲\).md "wikilink")》
      - 《[-{zh-cn:使命召唤：联合进攻;
        zh-tw:決勝時刻：聯合行動;}-](../Page/決勝時刻：聯合行動.md "wikilink")》
      - 《[-{zh-cn:使命召唤; zh-hk:使命召喚;
        zh-tw:決勝時刻;}-2](../Page/決勝時刻2.md "wikilink")》
      - 《[-{zh-cn:使命召唤：世界战争;
        zh-tw:決勝時刻：戰爭世界;}-](../Page/決勝時刻：戰爭世界.md "wikilink")》
      - 《[-{zh-cn:使命召唤：黑色行动;
        zh-tw:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》
      - 《[-{zh-cn:使命召唤：二战;
        zh-tw:決勝時刻：二戰;}-](../Page/使命召喚：二戰.md "wikilink")》
  - 《[-{zh-hans:胜利之日; zh-hant:決勝之日;}-](../Page/決勝之日.md "wikilink")》
  - 《[-{zh-hans:胜利之日：起源;
    zh-hant:決勝之日：次世代;}-](../Page/決勝之日：次世代.md "wikilink")》

## 参考文献

<div class="references-small">

<references />

</div>

## 參见

  - [M1加蘭德步槍](../Page/M1加兰德步枪.md "wikilink")
  - [M14自動步槍](../Page/M14自动步枪.md "wikilink")
  - [儒格Mini-14半自動步槍](../Page/儒格Mini-14半自動步槍.md "wikilink")
  - [湯普森輕型步槍](../Page/湯普森輕型步槍.md "wikilink")

## 外部連結

  - —[M1卡賓槍說明書](http://m1.50webs.com/)

  - —[M1、M1A1、M2、M3卡賓槍介詔](http://www.olive-drab.com/od_other_firearms_rifle_m1carbine.php3)

  - —[美國陸軍戰場手冊23-7（1942年）](http://www.90thidpg.us/Reference/Reference.html)

  - —[枪炮世界—M1卡宾枪](http://firearmsworld.net/usa/r/m1carbine/inde.htm)

[Category:半自动步枪](../Category/半自动步枪.md "wikilink")
[Category:美國槍械](../Category/美國槍械.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")
[Category:中國二戰槍械](../Category/中國二戰槍械.md "wikilink")
[Category:抗戰時期中國武器](../Category/抗戰時期中國武器.md "wikilink")
[Category:美國二戰武器](../Category/美國二戰武器.md "wikilink")
[Category:英國二戰武器](../Category/英國二戰武器.md "wikilink")
[Category:法國二戰武器](../Category/法國二戰武器.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[Category:韓戰武器](../Category/韓戰武器.md "wikilink")
[Category:美國海軍陸戰隊裝備](../Category/美國海軍陸戰隊裝備.md "wikilink")
[Category:卡宾枪](../Category/卡宾枪.md "wikilink") [Category:.30
Carbine口徑槍械](../Category/.30_Carbine口徑槍械.md "wikilink")

1.  Chapman, F. Spencer, *The Jungle Is Neutral: A Soldier's Two-Year
    Escape from the Japanese Army*, Lyons Press, 1st ed., ,  (2003), p.
    300

2.  Dunlap, Roy, *Ordnance Went Up Front*, Samworth Press (1948), p. 297

3.  McManus, John C., *The Deadly Brotherhood: The American Combat
    Soldier in World War II*, New York: Random House Publishing,
    (1998), p. 52: Private Richard Lovett of the U.S. [Americal
    Division](../Page/Americal_Division.md "wikilink") noted that "It
    didn't have stopping power. Enemy soldiers were shot many times but
    kept on coming."

4.  U.S. Army, *Handbook on Japanese Military Forces: Body armor*,
    Technical Manual, 15 September 1944, Chap. X, sec. 4(b)
    <http://www.ibiblio.org/hyperwar/Japan/IJA/HB/HB-10.html>

5.  George, John, *Shots Fired In Anger* NRA Press (1981), p. 450

6.
7.  McManus, John C., *The Deadly Brotherhood*, p. 52

8.  Dunlap, Roy, *Ordnance Went Up Front*, Plantersville, SC: Small-Arms
    Technical Pub. Co., The Samworth Press,  (1948), p. 240

9.  O'Donnell, Patrick, *Give Me Tomorrow: The Korean War's Greatest
    Untold Story: The Epic Stand of the Marines of George Company*, Da
    Capo Press 1st ed., ,  (2010), pp. 88, 168, 173

10. Clavin, Tom, *Last Stand of Fox Company*, New York: Atlantic Monthly
    Press, ,  (2009), p. 113: In addition to their bulky cotton-padded
    *telegroika* coats, which could freeze solid with perspiration,
    Chicom infantry frequently wore vests or undercoats of thick
    goatskin.

11. Jowett, Philip S., *The Chinese Army 1937–49: World War II and Civil
    War*, Osprey Publishing,  (2005), p. 47

12. Thomas, Nigel, *The Korean War 1950–53*, Osprey Publishing Ltd., ,
    (1986), p. 47

13. Andrew, Martin (Dr.), *Logistics in the PLA*, Army Sustainment, Vol.
    42, Issue 2, March–April 2010

14. Thomas, Nigel, *The Korean War 1950–53*, Osprey Publishing Ltd., ,
    (1986), pp. 37, 47: Many Chinese troops carried either rice or
    *shaoping*, an unleavened bread flour mixture in a fabric tube slung
    over the shoulder.

15. Chinese troops frequently wore bandolier-type ammunition pouches and
    carried extra [PPSh](../Page/PPSh-41.md "wikilink") or
    [Thompson](../Page/Thompson_submachine_gun.md "wikilink") magazines
    in addition to 4–5 stick grenades.

16.
17.
18. Russ, Martin, *Breakout: The Chosin Reservoir Campaign: Korea 1950*,
    Penguin Publishing, ,  (2000), p. 40: The failure of the .30 carbine
    round to stop enemy soldiers may not have been due to inadequate
    penetration. Marine Lt. James Stemple reported that he shot an enemy
    soldier with his M2 carbine four times in the chest and saw the
    padding fly out the back of the soldier's padded jacket as the
    bullets penetrated his body, yet the enemy soldier kept on coming.

19. Clavin, Tom, *Last Stand of Fox Company*, New York: Atlantic Monthly
    Press, ,  (2009), pp. 82, 113

20. O'Donnell, Patrick, *Give Me Tomorrow: The Korean War's Greatest
    Untold Story*, p. 88

21. Spurr, Russell, *Enter the Dragon: China's Undeclared War Against
    the U.S. in Korea, 1950–51*, New York, NY: Newmarket Press,  (1998),
    p.182: Chinese frontline PLA troops disliked the M1/M2 carbine, as
    they believed its cartridge had inadequate stopping power. Captured
    U.S. carbines were instead issued to runners and mortar crews.

22. S.L.A. Marshall, *Commentary on Infantry and Weapons in Korea
    1950–51*, 1st Report ORO-R-13 of 27 October 1951, Project Doughboy
    \[Restricted\], Operations Research Office (ORO), U.S. Army (1951)

23. Leroy Thompson (2011). The M1 Carbine. Osprey Publishing. p. 57. .

24.
25.

26. Leroy Thompson (2011). The M1 Carbine. Osprey Publishing. p. 67. .

27. Green Beret in Vietnam: 1957–73. Gordon Rottman. Osprey Publishing,
    2002. p. 41

28. Barnes, Frank C., *Cartridges of the World*, Iola WI: DBI Books
    Inc., ,  (6th ed., 1989), p. 52

29.

30. Jane's Gun Recognition Guide. Ian Hogg & Terry Gander. HarperCollins
    Publishers. 2005. p. 330

31.

32. US Marine Corps 1941–45, By Gordon Rottman Osprey Publishing.
    Copyright 1995, page 14.

33. Green Beret in Vietnam: 1957–73, By Gordon Rottman Osprey
    Publishing. 2002, p. 41.

34. Leroy Thompson (2011). The M1 Carbine. Osprey Publishing. p. 26,27.
    .

35. <http://www.gunsandammo.com/blogs/history-books/m1-carbine-americas-unlikely-warrior/>
    Guns & Ammo. M1 Carbine: America's Unlikely Warrior. by Garry James.
    October 6th, 2014

36. [WWII American M1
    Carbine](http://www.deactivated-guns.co.uk/detail/m1_carbine_newspec_2.htm)

37. Dill, James, *Winter of the Yalu*, Changjin Journal 06.22.00

38. Canfield, Bruce, *Arms of the Chosin Few* American Rifleman, 2
    November 2010, retrieved 10 May 2011

39. Hammel, Eric, *Chosin: Heroic Ordeal of the Korean War*, Zenith
    Press, 1st ed., ,  (2007), p. 205

40. S.L.A. Marshall, *Commentary on Infantry and Weapons in Korea
    1950–51*, 1st Report ORO-R-13 of 27 October 1951, *Project
    Doughboy* \[Restricted\], Operations Research Office (ORO), U.S.
    Army (1951)

41. Clavin, Tom, *Last Stand of Fox Company*, New York: Atlantic Monthly
    Press, ,  (2009), p. 161

42.

43. America's Favorite Gun. by Gold V. Sanders. Popular Science Aug
    1944. pp. 84–87, 221

44. The M16. by Gordon Rottman. Osprey Publishing 2011. p. 6

45. [Infrared Sniperscope
    M3](http://www.rt66.com/~korteng/SmallArms/m3irsnip.htm)

46. [Deactivated Silenced M1 Carbine
    .22](http://www.deactivated-guns.co.uk/detail/m1_carbine_22.htm)

47. <http://qkzz.net/article/549069ec-2828-4f0d-8772-c51b7b9e59bc_3.htm>