教宗**聖波尼法爵一世**，又譯**博義一世**、**波尼法西斯一世**（，原名不詳，？—422年），於418年12月28日至422年9月4日為[教宗](../Page/教宗.md "wikilink")。他是[希坡的圣奥斯定的同时代者](../Page/圣奥斯定.md "wikilink")，后者将其一些作品献给了他。

在[教宗佐西](../Page/教宗佐西.md "wikilink")（Pope
Zosimus）去世后，两派都推出了自己的教宗候选人，一位是博義一世，另一位是[恩赖](../Page/对立教宗恩赖.md "wikilink")（Eulalius）。[加拉·普拉西狄亚](../Page/加拉·普拉西狄亚.md "wikilink")（Galla
Placidia），[西罗马皇帝](../Page/罗马皇帝.md "wikilink")[康斯坦丁三世的皇后](../Page/君士坦提乌斯三世.md "wikilink")，要求皇帝[洪诺留](../Page/弗拉維烏斯·奧古斯都·霍諾留.md "wikilink")（Honorius）干涉，而他发布诏书责成两人都离开[罗马](../Page/罗马.md "wikilink")。在接下来的[复活节时](../Page/复活节.md "wikilink")，恩赖回到该城以行[洗礼并庆祝节日](../Page/洗礼.md "wikilink")；当皇帝听闻此事后，恩赖被剥夺了品秩并被驱逐出罗马，在418年12月28日，博義一世成为了教宗。

博義一世继续与[伯拉糾派对立](../Page/伯拉糾派.md "wikilink")，劝告皇帝[狄奥多西二世回复](../Page/狄奥多西二世.md "wikilink")[伊利里亚](../Page/伊利里亚.md "wikilink")（Illyricum）至西方管辖权下，并捍卫[圣座的权利](../Page/圣座.md "wikilink")。

## 參見

  - [Opera Omnia by Migne Patrologia
    Latina](http://www.documentacatholicaomnia.eu/01_01_0418-0422-_Bonifacius_I,_Sanctus.html)

## 譯名列表

  - 博義一世：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作博義。
  - 波尼法爵一世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)、[-{梵蒂岡}-廣播電台](https://web.archive.org/web/20060505032144/http://www.radiovaticana.org/cinesebig5/santuari/57chiesemondo.html)作波尼法爵。
  - 卜尼法斯一世：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=boniface&mode=3)、[中國大百科智慧藏](http://wordpedia.pidc.org.tw/Content.asp?History=A1&Query=9)、《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作卜尼法斯。
  - 朋尼非斯一世：[國立編譯舘](http://www.nict.gov.tw/tc/dic/search.php?p=2&ps=20&w1=boniface&w2=&w3=&c1=&c2=c2&l=&u=&pr=&r=&o=1&pri=undefined)作朋尼非斯。

<!-- end list -->

  - 博尼法斯一世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作卜尼法斯。
  - 博尼費斯一世、博尼费斯一世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作博尼費斯。
  - 博尼費切一世、博尼费切一世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作博尼費切。

[B](../Category/422年逝世.md "wikilink")
[Category:5世纪总主教](../Category/5世纪总主教.md "wikilink")
[Category:教宗圣人](../Category/教宗圣人.md "wikilink")