**漢登集團控股有限公司**（**Hang Ten Group Holdings
Limited**，），是在[香港上市的一家公司](../Page/香港.md "wikilink")，经营“Hang
Ten”、“H\&T”和“Arnold
Palmer”等服裝品牌，業務集中在[臺灣和](../Page/臺灣.md "wikilink")[韓國](../Page/韓國.md "wikilink")，在[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[中國](../Page/中國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門等地也有業務](../Page/澳門.md "wikilink")。\[1\]

“Hang
Ten”品牌于1960年初在[美國](../Page/美國.md "wikilink")[加利福尼亞州創立](../Page/加利福尼亞州.md "wikilink")，當時為[冲浪服裝品牌](../Page/冲浪.md "wikilink")，其後開始涉足其他休閒服裝；品牌名稱源於英語“Hanging
Ten”一詞，意義為雙腳緊貼著衝浪板，代表著衝浪的精神。\[2\]目前在香港上市的汉登前身為[雅佳控股](../Page/雅佳控股.md "wikilink")，該公司在2002年被[強制清盤](../Page/強制清盤.md "wikilink")，上市地位在2003年由漢登取代。\[3\]公司現於[百慕達註冊](../Page/百慕達.md "wikilink")，主席為
[陳永燊](../Page/陳永燊.md "wikilink")，行政總裁為[孔仕傑](../Page/孔仕傑.md "wikilink")。

2010年12月19日，[利豐宣佈以每股](../Page/利豐.md "wikilink")2.7港元，總代價27億從[YGM貿易收購漢登集團](../Page/YGM貿易.md "wikilink")。\[4\]收購完成後，漢登集團於03月20日除牌。

## 品牌故事

「HANGING TEN」意指雙腳緊貼住衝浪板，也代表衝浪的精神所在-捉住夢想，勇敢向前！HANG
TEN這個源自美國加州的世界性休閒服裝品牌，以濃厚美式自由風格與自然簡約設計路線，做為品牌精神與產品設計的發想概念，深受全球各地崇尚自然生活態度、熱愛運動休閒的消費者們支持與喜愛。HANG
TEN年輕、自由又充滿活力的加州風格及原創精神，是全球各地消費者長期喜愛HANG TEN商品的原因。而熱愛HANG
TEN品牌的消費者，也同樣擁有認真經營生活、強烈個人風格、活力十足、獨特流行品味等個人特色，這群人涵蓋在6\~45歲年齡間，與HANG
TEN致力成為「適合每一個人的品牌」之理想目標相互呼應。

## 品牌歷史

1959年在美國加州[衝浪運動蔚為風氣](../Page/衝浪.md "wikilink")，但讓喜愛衝浪的年輕人最困擾的事，就是沒有適合衝浪穿著的[褲子](../Page/褲子.md "wikilink")，當時也沒有服裝品牌針對衝浪設計出輕巧方便、適合炎熱天氣的專用褲。衝浪愛好者[Duke
Boyd深受其擾並希望徹底解決這個問題](../Page/Duke_Boyd.md "wikilink")，他找到了一個裁縫師Doris
Boeck，依照Duke的構想創造出第一條衝浪運動穿著的短褲，不僅在衝浪圈中大受歡迎，也迅速在加州打響了名號。
Duke與Doris便開始合作進行衝浪相關服飾的設計與銷售，Duke以高爾夫球的最高境界「HOLE IN
ONE-一桿進洞」做為發想，「HANGING
TEN」-意指雙腳緊貼住[衝浪板表衝浪的精神所在](../Page/衝浪板.md "wikilink")，開始使用雙腳做為品牌商標，並將商標名訂為HANG
TEN。當晚Doris用手工繡了一雙金色的腳丫子在褲子上，此後HANG
[TEN以這個LOGO為消費大眾週知](../Page/TEN.md "wikilink")，這個LOGO不僅代表著一個美國海灘及衝浪專用品牌的誕生，也是消費者心中對HANG
TEN商品認知的開端。

## 脚注

## 外部連結

  - [Hang Ten](http://www.hangten.com.tw/)

  - [Hang Ten 台灣](https://www.shophangten.com/)

  -
[Category:香港服装公司](../Category/香港服装公司.md "wikilink")
[Category:香港時裝品牌](../Category/香港時裝品牌.md "wikilink")
[Category:台灣服裝品牌](../Category/台灣服裝品牌.md "wikilink")
[Category:韓國服裝品牌](../Category/韓國服裝品牌.md "wikilink")
[Category:韓國公司](../Category/韓國公司.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:利豐](../Category/利豐.md "wikilink")

1.
2.
3.
4.