<div style="float: right; margin: 0 0 1em 2em; width: 24em; text-align: right; font-size: 0.86em; line-height: normal;">

<div style="border: 1px solid #ccd2d9; background: #f0f6fa; text-align: left; padding: 0.5em 1em; text-align: center;">

<big>**国立看護大学校**</big>

`   `

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

</div>

</div>

**国立护理大学校**（、National College of
Nursing）位于[东京都](../Page/东京都.md "wikilink")[清濑市的](../Page/清濑市.md "wikilink")[厚生劳动省直辖学校](../Page/厚生劳动省.md "wikilink")，与一般大学不同。厚生劳动省的机构中，属于国立国际医疗中心的一部分。主要目的为培养国立高度专门医疗中心中与政策医疗相关的护理师。学习年限为4年，授予[护理师](../Page/护理师.md "wikilink")・[助产师的国家考试资格](../Page/助产师.md "wikilink")。学士（护理学）学位，需要通过申请[大学评价学位授予机构才能获得](../Page/大学评价学位授予机构.md "wikilink")。

## 沿革

  - 2001年　设立（1学年定员100名）
  - 2005年　研究课程部护理学研究科（相当于硕士课程）开设

## 大学负责人

  - 大学校长：竹尾惠子

## 关联机构

  - [国立国际医疗中心](../Page/国立国际医疗中心.md "wikilink")

## 关联项目

  - [护理学](../Page/护理学.md "wikilink")
  - [护理大学](../Page/护理大学.md "wikilink")

## 外部链接

[国立护理大学校](http://www.ncn.ac.jp/)

[Category:東京都的大學](../Category/東京都的大學.md "wikilink")
[Category:省廳大學校](../Category/省廳大學校.md "wikilink")
[Category:2001年創建的教育機構](../Category/2001年創建的教育機構.md "wikilink")
[Category:清瀨市](../Category/清瀨市.md "wikilink")