**汉弗里·戴维**爵士，**第一代[從男爵](../Page/從男爵.md "wikilink")**（，），[英国](../Page/英国.md "wikilink")[化学家](../Page/化学家.md "wikilink")。是发现[化学元素最多的人](../Page/化学元素.md "wikilink")，被譽為「無機化學之父」。一般認為戴維是燈泡和第一代礦工燈的發明者。

## 生平

  - 1778年12月17日，出生在[英格蘭](../Page/英格蘭.md "wikilink")[彭赞斯附近的鄉村](../Page/彭赞斯.md "wikilink")。
  - 1798年，在布裡托爾一所氣體療病研究室當管理員。
  - 1801年，被皇家學會聘請，擔任化學講師兼管實驗室。
  - 1803年，被選為[英國皇家學會會員](../Page/英國皇家學會.md "wikilink")。
  - 1807年，出任皇家學會秘書。
  - 1812年，受封為爵士，出版了《[化学哲学原理](../Page/化学哲学原理.md "wikilink")》。
  - 1813年，任命[法拉第為他的助手](../Page/法拉第.md "wikilink")，使這個貧窮的訂書工逐漸成為著名的科學家。晚年的戴維曾說過：「我雖然在科學上有很多發現，但是我這輩子最大的發現，就是發現了法拉第。」
  - 1820年，被選為皇家學會會長。
  - 1829年，在[日內瓦逝世](../Page/日內瓦.md "wikilink")，享年51歲。

## 貢獻

1799年，戴维发现了[一氧化二氮](../Page/一氧化二氮.md "wikilink")（又名[笑气](../Page/笑气.md "wikilink")）具有麻醉作用，能使病人丧失痛觉。1802年開創了[農業化學](../Page/農業化學.md "wikilink")。1807年用[電解法離析出金屬](../Page/電解.md "wikilink")[鉀和](../Page/鉀.md "wikilink")[鈉](../Page/鈉.md "wikilink")；1808年分離出金屬[鈣](../Page/鈣.md "wikilink")、[鍶](../Page/鍶.md "wikilink")、[鋇和](../Page/鋇.md "wikilink")[鎂](../Page/鎂.md "wikilink")。1813年他在[法国研究](../Page/法国.md "wikilink")[碘](../Page/碘.md "wikilink")，指出碘是與氯類似的元素，並製備出[碘化鉀和](../Page/碘化鉀.md "wikilink")[碘酸鉀等許多碘的化合物](../Page/碘酸鉀.md "wikilink")。後還證實[金剛石和](../Page/金剛石.md "wikilink")[木炭的化學成分相同](../Page/木炭.md "wikilink")。1815年發明安全礦燈。1817年發現[鉑能促使醇蒸氣在空氣中氧化的](../Page/鉑.md "wikilink")[催化作用](../Page/催化.md "wikilink")。

## 参考文献

## 外部連結

<http://blog.sina.com.cn/s/blog_6670547701019yub.html>

  -
  - (Davy's first name is spelled incorrectly in this book.)

  -
  - [*The Collected Works of Humphry
    Davy*](http://books.google.com/books?id=HcMJAAAAIAAJ)

  - [Obituary](http://books.google.com/books?id=94cpDF5kaAEC&pg=PA39)
    (1830)

  - [''Journal of a Tour made in the years 1828, 1829, through Styria,
    Carniola, and Italy, whilst accompanying the late Sir Humphry
    Davy](http://www.archive.org/details/journaloftourmad00tobi) by J.J.
    Tobin (1832)

  - [*Humphry Davy, Poet and
    Philosopher*](http://books.google.com/books?id=ZXMKkOFnrEcC&pg=PA9)
    by Thomas Edward Thorpe, New York: Macmillan, 1896

  - [*Young Humphry Davy: The Making of an Experimental
    Chemist*](http://books.google.com/books?id=NlVCnYZx-sQC&pg=PP1) by
    June Z. Fullmer, Philadelphia: American Philosophical Society, 2000

  - [BBC – Napoleon's medal 'cast into
    sea'](http://news.bbc.co.uk/2/hi/uk_news/7298375.stm)

  -
[Category:19世纪化学家](../Category/19世纪化学家.md "wikilink")
[Category:英格蘭化學家](../Category/英格蘭化學家.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:英国皇家学会院士](../Category/英国皇家学会院士.md "wikilink")
[Category:英国皇家学会会长](../Category/英国皇家学会会长.md "wikilink")
[Category:化學元素發現者](../Category/化學元素發現者.md "wikilink")
[Category:康沃爾郡人](../Category/康沃爾郡人.md "wikilink")
[Category:丹麦皇家科学院院士](../Category/丹麦皇家科学院院士.md "wikilink")
[Category:皇家奖章获得者](../Category/皇家奖章获得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:拉姆福德奖章获得者](../Category/拉姆福德奖章获得者.md "wikilink")