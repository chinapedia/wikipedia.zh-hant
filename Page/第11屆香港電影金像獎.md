**第11屆香港電影金像獎**於1992年4月5日晚上在[香港文化中心大劇院舉行](../Page/香港文化中心#大劇院.md "wikilink")，提名及獲獎名單如下。

## 提名及獲獎名單

### 最佳電影

<table style="width:64%;">
<colgroup>
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</p></td>
<td><ul>
<li>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a>》</li>
<li>《<a href="../Page/婚姻勿語.md" title="wikilink">婚姻勿語</a>》</li>
<li>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</li>
<li>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</li>
<li>《<a href="../Page/縱橫四海_(電影).md" title="wikilink">縱橫四海</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳導演

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/徐克.md" title="wikilink">徐克</a></p></td>
<td><p>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</p></td>
<td><ul>
<li><a href="../Page/吳宇森.md" title="wikilink">吳宇森</a>《<a href="../Page/縱橫四海.md" title="wikilink">縱橫四海</a>》</li>
<li><a href="../Page/徐克.md" title="wikilink">徐克</a>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</li>
<li><a href="../Page/陳嘉上.md" title="wikilink">陳嘉上</a>《<a href="../Page/逃學威龍.md" title="wikilink">逃學威龍</a>》</li>
<li><a href="../Page/潘文傑_(導演).md" title="wikilink">潘文傑</a>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</li>
<li><a href="../Page/劉國昌.md" title="wikilink">劉國昌</a>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳編劇

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/麥當雄.md" title="wikilink">麥當雄</a><br />
<a href="../Page/蕭若元.md" title="wikilink">蕭若元</a></p></td>
<td><p>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</p></td>
<td><ul>
<li><a href="../Page/李志毅.md" title="wikilink">李志毅</a>《<a href="../Page/婚姻勿語.md" title="wikilink">婚姻勿語</a>》</li>
<li><a href="../Page/陳文強.md" title="wikilink">陳文強</a>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a>》</li>
<li><a href="../Page/麥當雄.md" title="wikilink">麥當雄</a>、<a href="../Page/蕭若元.md" title="wikilink">蕭若元</a>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</li>
<li><a href="../Page/黃炳耀_(編劇).md" title="wikilink">黃炳耀</a>、李志毅《<a href="../Page/雙城故事.md" title="wikilink">雙城故事</a>》</li>
<li>黃炳耀、<a href="../Page/陳嘉上.md" title="wikilink">陳嘉上</a>《<a href="../Page/逃學威龍.md" title="wikilink">逃學威龍</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳男主角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a></p></td>
<td><p>《<a href="../Page/雙城故事.md" title="wikilink">雙城故事</a>》</p></td>
<td><ul>
<li><a href="../Page/呂良偉.md" title="wikilink">呂良偉</a>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</li>
<li><a href="../Page/周星馳.md" title="wikilink">周星馳</a>《<a href="../Page/逃學威龍.md" title="wikilink">逃學威龍</a>》</li>
<li><a href="../Page/周潤發.md" title="wikilink">周潤發</a>《<a href="../Page/縱橫四海.md" title="wikilink">縱橫四海</a>》</li>
<li><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a>《<a href="../Page/雙城故事.md" title="wikilink">雙城故事</a>》</li>
<li><a href="../Page/劉德華.md" title="wikilink">劉德華</a>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳女主角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/葉童.md" title="wikilink">葉童</a></p></td>
<td><p>《<a href="../Page/婚姻勿語.md" title="wikilink">婚姻勿語</a>》</p></td>
<td><ul>
<li><a href="../Page/張敏.md" title="wikilink">張敏</a>《<a href="../Page/與龍共舞.md" title="wikilink">與龍共舞</a>》</li>
<li><a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a>《<a href="../Page/何日君再來.md" title="wikilink">何日君再來</a>》</li>
<li><a href="../Page/葉童.md" title="wikilink">葉童</a>《<a href="../Page/婚姻勿語.md" title="wikilink">婚姻勿語</a>》</li>
<li><a href="../Page/鄭裕玲.md" title="wikilink">鄭裕玲</a>《<a href="../Page/表姐你好嘢!續集.md" title="wikilink">表姐你好嘢!續集</a>》</li>
<li><a href="../Page/劉嘉玲.md" title="wikilink">劉嘉玲</a>《<a href="../Page/雞鴨戀.md" title="wikilink">雞鴨戀</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳男配角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/關海山.md" title="wikilink">關海山</a></p></td>
<td><p>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a> 》</p></td>
<td><ul>
<li><a href="../Page/吳孟達.md" title="wikilink">吳孟達</a>《<a href="../Page/逃學威龍.md" title="wikilink">逃學威龍</a>》</li>
<li><a href="../Page/郭富城.md" title="wikilink">郭富城</a>《<a href="../Page/九一神鵰俠侶.md" title="wikilink">九一神鵰俠侶</a>》</li>
<li><a href="../Page/張學友.md" title="wikilink">張學友</a>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</li>
<li><a href="../Page/湯鎮業.md" title="wikilink">湯鎮業</a>《<a href="../Page/五虎將之決裂.md" title="wikilink">五虎將之決裂</a>》</li>
<li><a href="../Page/鄭則仕.md" title="wikilink">鄭則仕</a>《<a href="../Page/跛豪.md" title="wikilink">跛豪</a>》</li>
<li><a href="../Page/關海山.md" title="wikilink">關海山</a>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳女配角

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/葉德嫻.md" title="wikilink">葉德嫻</a></p></td>
<td><p>《<a href="../Page/與龍共舞.md" title="wikilink">與龍共舞</a>》</p></td>
<td><ul>
<li><a href="../Page/吳家麗.md" title="wikilink">吳家麗</a>《<a href="../Page/何日君再來.md" title="wikilink">何日君再來</a>》</li>
<li><a href="../Page/邱淑貞.md" title="wikilink">邱淑貞</a>《五億探長雷洛傳》</li>
<li><a href="../Page/葉童.md" title="wikilink">葉童</a>《跛豪》</li>
<li><a href="../Page/葉德嫻.md" title="wikilink">葉德嫻</a>《<a href="../Page/與龍共舞.md" title="wikilink">與龍共舞</a>》</li>
<li><a href="../Page/關之琳.md" title="wikilink">關之琳</a>《<a href="../Page/婚姻勿語.md" title="wikilink">婚姻勿語</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳新人

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/梁琤.md" title="wikilink">梁琤</a></p></td>
<td><p>《<a href="../Page/黑貓_(電影).md" title="wikilink">黑貓</a>》</p></td>
<td><ul>
<li><a href="../Page/呂頌賢.md" title="wikilink">呂頌賢</a>《<a href="../Page/拳王_(1991年電影).md" title="wikilink">拳王</a>》</li>
<li><a href="../Page/陳德興.md" title="wikilink">陳德興</a>《拳王》</li>
<li><a href="../Page/桑妮.md" title="wikilink">桑妮</a>《<a href="../Page/Yes一族.md" title="wikilink">Yes一族</a>》</li>
<li><a href="../Page/梁琤.md" title="wikilink">梁琤</a>《<a href="../Page/黑貓_(電影).md" title="wikilink">黑貓</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳攝影

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鮑起鳴.md" title="wikilink">鮑起鳴</a></p></td>
<td><p>《<a href="../Page/九一神鵰俠侶.md" title="wikilink">九一神鵰俠侶</a>》</p></td>
<td><ul>
<li><a href="../Page/李子衡.md" title="wikilink">李子衡</a>、<a href="../Page/劉偉強.md" title="wikilink">劉偉強</a>《<a href="../Page/五億探長雷洛傳.md" title="wikilink">五億探長雷洛傳</a>》</li>
<li><a href="../Page/黃仲標.md" title="wikilink">黃仲標</a>、<a href="../Page/鍾志文.md" title="wikilink">鍾志文</a>、<a href="../Page/鮑起鳴.md" title="wikilink">鮑起鳴</a>、<a href="../Page/敖志君.md" title="wikilink">敖志君</a>《<a href="../Page/何日君再來.md" title="wikilink">何日君再來</a>》</li>
<li>鮑起鳴《<a href="../Page/九一神鵰俠侶.md" title="wikilink">九一神鵰俠侶</a>》</li>
<li>鮑起鳴《<a href="../Page/衛斯理之霸王卸甲.md" title="wikilink">衛斯理之霸王卸甲</a>》</li>
<li>鍾志文、黃仲標、<a href="../Page/黃岳泰.md" title="wikilink">黃岳泰</a>、<a href="../Page/林國華_(香港).md" title="wikilink">林國華</a>、<a href="../Page/陳東村.md" title="wikilink">陳東村</a>、<a href="../Page/陳沛佳.md" title="wikilink">陳沛佳</a>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳剪接

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/麥子善.md" title="wikilink">麥子善</a></p></td>
<td><p>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</p></td>
<td><ul>
<li><a href="../Page/胡大為.md" title="wikilink">胡大為</a>《<a href="../Page/縱橫四海.md" title="wikilink">縱橫四海</a>》</li>
<li><a href="../Page/麥子善.md" title="wikilink">麥子善</a>《<a href="../Page/倩女幽魂Ⅲ：道道道.md" title="wikilink">倩女幽魂Ⅲ道道道</a>》</li>
<li>麥子善《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</li>
<li><a href="../Page/潘雄.md" title="wikilink">潘雄</a>《跛豪》</li>
<li>潘雄、<a href="../Page/奚傑偉.md" title="wikilink">奚傑偉</a>《<a href="../Page/九一神鵰俠侶.md" title="wikilink">九一神鵰俠侶</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳美術指導

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/奚仲文.md" title="wikilink">奚仲文</a></p></td>
<td><p>《<a href="../Page/九一神鵰俠侶.md" title="wikilink">九一神鵰俠侶</a>》</p></td>
<td><ul>
<li><a href="../Page/奚仲文.md" title="wikilink">奚仲文</a>《九一神鵰俠侶》</li>
<li>奚仲文《黃飛鴻》</li>
<li><a href="../Page/馬磐超.md" title="wikilink">馬磐超</a>《<a href="../Page/何日君再來.md" title="wikilink">何日君再來</a>》</li>
<li><a href="../Page/莫少奇.md" title="wikilink">莫少奇</a>《五億探長雷洛傳》</li>
<li><a href="../Page/陸文華.md" title="wikilink">陸文華</a>《<a href="../Page/雙城故事.md" title="wikilink">雙城故事</a>》</li>
<li><a href="../Page/梁華生.md" title="wikilink">梁華生</a> 、<a href="../Page/張叔平.md" title="wikilink">張叔平</a>《<a href="../Page/倩女幽魂Ⅲ：道道道.md" title="wikilink">倩女幽魂Ⅲ道道道</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳動作指導

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/袁祥仁.md" title="wikilink">袁祥仁</a><br />
<a href="../Page/袁信義.md" title="wikilink">袁信義</a><br />
<a href="../Page/劉家榮.md" title="wikilink">劉家榮</a></p></td>
<td><p>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</p></td>
<td><ul>
<li><a href="../Page/元德.md" title="wikilink">元德</a>《<a href="../Page/九一神鵰俠侶.md" title="wikilink">九一神鵰俠侶</a>》</li>
<li><a href="../Page/成家班.md" title="wikilink">成家班</a>《<a href="../Page/飛鷹計劃.md" title="wikilink">飛鷹計劃</a>》</li>
<li><a href="../Page/林滿華.md" title="wikilink">林滿華</a>《<a href="../Page/監獄風雲Ⅱ逃犯.md" title="wikilink">監獄風雲Ⅱ逃犯</a>》</li>
<li><a href="../Page/袁祥仁.md" title="wikilink">袁祥仁</a>、<a href="../Page/袁信義.md" title="wikilink">袁信義</a>、<a href="../Page/劉家榮.md" title="wikilink">劉家榮</a>《<a href="../Page/黃飛鴻_(1991年電影).md" title="wikilink">黃飛鴻</a>》</li>
<li><a href="../Page/程小東.md" title="wikilink">程小東</a>、<a href="../Page/元彬.md" title="wikilink">元彬</a>、<a href="../Page/馬玉成.md" title="wikilink">馬玉成</a>、<a href="../Page/張耀星.md" title="wikilink">張耀星</a>《<a href="../Page/倩女幽魂Ⅲ：道道道.md" title="wikilink">倩女幽魂Ⅲ道道道</a>》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳電影配樂

<table style="width:80%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 24%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/黃霑.md" title="wikilink">黃霑</a></p></td>
<td><p>《黃飛鴻》</p></td>
<td><ul>
<li><a href="../Page/包以正.md" title="wikilink">包以正</a>《婚姻勿語》</li>
<li><a href="../Page/李宗盛.md" title="wikilink">李宗盛</a>、Kenshima《<a href="../Page/莎莎嘉嘉站起來.md" title="wikilink">莎莎嘉嘉站起來</a>》</li>
<li><a href="../Page/倫永亮.md" title="wikilink">倫永亮</a>、<a href="../Page/陳明道.md" title="wikilink">陳明道</a>《<a href="../Page/何日君再來.md" title="wikilink">何日君再來</a>》</li>
<li><a href="../Page/黃霑.md" title="wikilink">黃霑</a>《黃飛鴻》</li>
<li><a href="../Page/鮑比達.md" title="wikilink">鮑比達</a>《<a href="../Page/衛斯理之霸王卸甲.md" title="wikilink">衛斯理之霸王卸甲</a>》</li>
<li><a href="../Page/戴樂民.md" title="wikilink">戴樂民</a>、黃霑《倩女幽魂Ⅲ道道道》</li>
</ul></td>
</tr>
</tbody>
</table>

### 最佳電影歌曲

<table style="width:80%;">
<colgroup>
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獲獎電影</p></th>
<th><p>提名名單</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>似是故人來《<a href="../Page/雙鐲.md" title="wikilink">雙鐲</a>》<br />
作曲：<a href="../Page/羅大佑.md" title="wikilink">羅大佑</a><br />
填詞：<a href="../Page/林夕.md" title="wikilink">林夕</a><br />
主唱：<a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a></p></td>
<td><ul>
<li>一起走過的日子《<a href="../Page/至尊無上II之永霸天下.md" title="wikilink">至尊無上II之永霸天下</a>》<br />
作曲：<a href="../Page/胡偉立.md" title="wikilink">胡偉立</a><br />
填詞：<a href="../Page/小美.md" title="wikilink">小美</a><br />
主唱：<a href="../Page/劉德華.md" title="wikilink">劉德華</a></li>
<li>何日《<a href="../Page/何日君再來.md" title="wikilink">何日君再來</a>》<br />
作曲：<a href="../Page/倫永亮.md" title="wikilink">倫永亮</a><br />
填詞：<a href="../Page/黃霑.md" title="wikilink">黃霑</a><br />
主唱：<a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a></li>
<li>似是故人來《<a href="../Page/雙鐲.md" title="wikilink">雙鐲</a>》<br />
作曲：<a href="../Page/羅大佑.md" title="wikilink">羅大佑</a><br />
填詞：<a href="../Page/林夕.md" title="wikilink">林夕</a><br />
主唱：梅艷芳</li>
<li>春風秋雨《<a href="../Page/莎莎嘉嘉站起來.md" title="wikilink">莎莎嘉嘉站起來</a>》<br />
作曲：<a href="../Page/李宗盛.md" title="wikilink">李宗盛</a><br />
填詞：<a href="../Page/林振強.md" title="wikilink">林振強</a><br />
主唱：<a href="../Page/葉倩文.md" title="wikilink">葉倩文</a></li>
<li>與龍共舞《<a href="../Page/與龍共舞.md" title="wikilink">與龍共舞</a>》<br />
作曲：<a href="../Page/盧冠廷.md" title="wikilink">盧冠廷</a><br />
填詞：小美<br />
主唱：<a href="../Page/鄺美雲.md" title="wikilink">鄺美雲</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 專業精神獎

<table>
<thead>
<tr class="header">
<th><p>獲獎者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/屠梅卿.md" title="wikilink">屠梅卿</a><br />
<a href="../Page/黃炳耀_(編劇).md" title="wikilink">黃炳耀</a></p></td>
</tr>
</tbody>
</table>

### 十大華語片

1.  《[跛豪](../Page/跛豪.md "wikilink")》

2.  《[黃飛鴻](../Page/黃飛鴻_\(1991年電影\).md "wikilink")》

3.  《[五億探長雷洛傳](../Page/五億探長雷洛傳.md "wikilink")》

4.  《[大红灯笼高高挂](../Page/大红灯笼高高挂.md "wikilink")》

5.  《婚姻勿語》

6.  《[逃學威龍](../Page/逃學威龍.md "wikilink")》

7.  《[九一神雕俠侶](../Page/九一神雕俠侶.md "wikilink")》

8.  《[縱橫四海](../Page/縱橫四海_\(電影\).md "wikilink")》

9.  《北京，你早》

10. 《[双旗镇刀客](../Page/双旗镇刀客.md "wikilink")》

### 十大外語片

1.  《[沉默的羔羊](../Page/沉默的羔羊.md "wikilink")》

2.  《[與狼共舞](../Page/與狼共舞.md "wikilink")》

3.  《[塞尔玛与路易丝](../Page/塞尔玛与路易丝.md "wikilink")》

4.  《[终结者2：审判日](../Page/终结者2：审判日.md "wikilink")》

5.  《[危情十日](../Page/危情十日.md "wikilink")》

6.  《[教父3](../Page/教父3.md "wikilink")》

7.  《[綠卡](../Page/綠卡_\(電影\).md "wikilink")》

8.  《[无语问苍天](../Page/无语问苍天.md "wikilink")》

9.  / 《》

10. 《》

## 獎項統計

|        |                                                                                                                                                                                                                                                                                                                                                     |
| ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **數目** | **電影名稱**                                                                                                                                                                                                                                                                                                                                            |
| **獲獎** |                                                                                                                                                                                                                                                                                                                                                     |
| 4      | [黃飛鴻](../Page/黃飛鴻_\(1991年電影\).md "wikilink")                                                                                                                                                                                                                                                                                                        |
| 2      | [跛豪](../Page/跛豪.md "wikilink")、[九一神雕俠侶](../Page/九一神雕俠侶.md "wikilink")                                                                                                                                                                                                                                                                               |
| 1      | [雙城故事](../Page/雙城故事.md "wikilink")、[婚姻勿語](../Page/婚姻勿語.md "wikilink")、[五億探長雷洛傳](../Page/五億探長雷洛傳.md "wikilink")、[與龍共舞](../Page/與龍共舞.md "wikilink")、[黑貓](../Page/黑貓.md "wikilink")、[雙鐲](../Page/雙鐲.md "wikilink")                                                                                                                                     |
| **提名** |                                                                                                                                                                                                                                                                                                                                                     |
| 8      | [五億探長雷洛傳](../Page/五億探長雷洛傳.md "wikilink")、[黃飛鴻](../Page/黃飛鴻_\(1991年電影\).md "wikilink")                                                                                                                                                                                                                                                               |
| 7      | [跛豪](../Page/跛豪.md "wikilink")                                                                                                                                                                                                                                                                                                                      |
| 6      | [何日君再來](../Page/何日君再來.md "wikilink")                                                                                                                                                                                                                                                                                                                |
| 5      | [婚姻勿語](../Page/婚姻勿語.md "wikilink")、[九一神雕俠侶](../Page/九一神雕俠侶.md "wikilink")                                                                                                                                                                                                                                                                           |
| 4      | [縱橫四海](../Page/縱橫四海.md "wikilink")、[逃學威龍](../Page/逃學威龍.md "wikilink")、[倩女幽魂Ⅲ道道道](../Page/倩女幽魂Ⅲ：道道道.md "wikilink")                                                                                                                                                                                                                                   |
| 3      | [雙城故事](../Page/雙城故事.md "wikilink")、[與龍共舞](../Page/與龍共舞.md "wikilink")                                                                                                                                                                                                                                                                               |
| 2      | [拳王](../Page/拳王.md "wikilink")、[衛斯理之霸王卸甲](../Page/衛斯理之霸王卸甲.md "wikilink")、[莎莎嘉嘉站起來](../Page/莎莎嘉嘉站起來.md "wikilink")                                                                                                                                                                                                                                  |
| 1      | [表姐你好嘢\!續集](../Page/表姐你好嘢!續集.md "wikilink")、[雞鴨戀](../Page/雞鴨戀.md "wikilink")、[五虎將之決裂](../Page/五虎將之決裂.md "wikilink")、[YES一族](../Page/YES一族.md "wikilink")、[黑貓](../Page/黑貓.md "wikilink")、[飛鷹計劃](../Page/飛鷹計劃.md "wikilink")、[監獄風雲Ⅱ逃犯](../Page/監獄風雲Ⅱ逃犯.md "wikilink")、[至尊無上II之永霸天下](../Page/至尊無上II之永霸天下.md "wikilink")、[雙鐲](../Page/雙鐲.md "wikilink") |
|        |                                                                                                                                                                                                                                                                                                                                                     |

## 相關條目

  - [香港電影金像獎](../Page/香港電影金像獎.md "wikilink")
  - [第28屆金馬獎](../Page/第28屆金馬獎.md "wikilink")

## 參考資料

## 外部連結

  - 第十一屆香港電影金像獎得獎名單（[新版](http://www.hkfaa.com/winnerlist11.html)·[舊版](http://www.hkfaa.com/history/list_11.html)）

[Category:香港電影金像獎](../Category/香港電影金像獎.md "wikilink")
[Category:1991年電影](../Category/1991年電影.md "wikilink")
[港](../Category/1992年電影獎項.md "wikilink")
[Category:1992年香港](../Category/1992年香港.md "wikilink")