**南小国町**（）是位于[日本](../Page/日本.md "wikilink")[熊本縣東北部](../Page/熊本縣.md "wikilink")，[阿蘇外輪山北側](../Page/阿蘇外輪山.md "wikilink")[高原地區](../Page/高原.md "wikilink")，屬[阿蘇郡的一個](../Page/阿蘇郡.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")。為[日本最美村莊聯合成員之一](../Page/日本最美村莊聯合.md "wikilink")。

## 历史

過去屬於「小國鄉」，小國鄉在1870年將原本的25個村合併為9個村，並在1889年進一步將其中六村合併為北小國村（現在的[小國町](../Page/小國町.md "wikilink")），另外三村合併為南小國村，也就是現在的南小國町之前身。\[1\]

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，設置**南小國村**。
  - 1969年11月1日：改制為**南小國町**。

## 觀光資源

[Kurokawa_onsen_001.JPG](https://zh.wikipedia.org/wiki/File:Kurokawa_onsen_001.JPG "fig:Kurokawa_onsen_001.JPG")

  - [黑川溫泉](../Page/黑川溫泉.md "wikilink")
  - [滿願寺溫泉](../Page/滿願寺溫泉.md "wikilink")
  - [田之原溫泉](../Page/田之原溫泉.md "wikilink")
  - [白川溫泉](../Page/白川溫泉_\(熊本縣\).md "wikilink")
  - [小田溫泉](../Page/小田溫泉.md "wikilink")
  - [湯田溫泉](../Page/湯田溫泉_\(熊本縣\).md "wikilink")
  - [瀨之本高原](../Page/瀨之本高原.md "wikilink")
  - [清流之森](../Page/清流之森.md "wikilink")

## 姊妹、友好都市

### 日本

  - 「[日本最美村莊聯合](../Page/日本最美村莊聯合.md "wikilink")」的其他成員
      - [美瑛町](../Page/美瑛町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[上川郡](../Page/上川郡_\(石狩國\).md "wikilink")）
      - [赤井川村](../Page/赤井川村.md "wikilink")（北海道-{[余市郡](../Page/余市郡.md "wikilink")}-）
      - [大藏村](../Page/大藏村.md "wikilink")（[山形縣](../Page/山形縣.md "wikilink")[最上郡](../Page/最上郡.md "wikilink")）
      - [白川村](../Page/白川村.md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")[大野郡](../Page/大野郡_\(岐阜縣\).md "wikilink")）
      - [大鹿村](../Page/大鹿村.md "wikilink")（[長野縣](../Page/長野縣.md "wikilink")[下伊那郡](../Page/下伊那郡.md "wikilink")）
      - [上勝町](../Page/上勝町.md "wikilink")（[德島縣](../Page/德島縣.md "wikilink")[勝浦郡](../Page/勝浦郡.md "wikilink")）

## 參考資料

## 外部連結

  - [南小國町商工會](http://www.kumashoko.or.jp/minami/)

  - [南小國町觀光協會](https://web.archive.org/web/20081221211410/http://www.roten.or.jp/)

  - [日本最美村莊聯合](https://web.archive.org/web/20090317084951/http://www.utsukushii-mura.jp/modules/news/)

[\*](../Category/南小國町.md "wikilink")

1.