[Gilbert-GS.JPG](https://zh.wikipedia.org/wiki/File:Gilbert-GS.JPG "fig:Gilbert-GS.JPG")\]\]
[Sir_Arthur_Seymour_Sullivan.jpg](https://zh.wikipedia.org/wiki/File:Sir_Arthur_Seymour_Sullivan.jpg "fig:Sir_Arthur_Seymour_Sullivan.jpg")\]\]
**吉爾伯特與沙利文**（Gilbert and
Sullivan）指[维多利亚时代幽默劇作家](../Page/维多利亚时代.md "wikilink")[威廉·S·吉尔伯特](../Page/威廉·S·吉尔伯特.md "wikilink")（William
S. Gilbert）與英國作曲家[阿瑟·萨利文](../Page/阿瑟·萨利文.md "wikilink")（Arthur
Sullivan）的合作。从1871年到1896年長達二十五年的合作中，共同创作了14部[喜剧](../Page/喜剧.md "wikilink")，其中最著名的为《[皮纳福号军舰](../Page/皮纳福号军舰.md "wikilink")》（H.M.S.
Pinafore）、《[彭赞斯的海盗](../Page/彭赞斯的海盗.md "wikilink")》（The Pirates of
Penzance）和《日本天皇》（The Mikado）。\[1\]

吉爾伯特负责剧本，喜歡裝傻，愛諷刺人，一改過去歌劇嚴肅的風格，劇中將很多現實中荒誕的一面演得合情合理。沙利文比吉尔伯特年轻7岁，负责编曲，他创作了著名的旋律，\[2\]
将愉悦和悲伤都演绎得淋漓尽致。\[3\]

制作人[理查德·多伊利·卡特](../Page/理查德·多伊利·卡特.md "wikilink")（Richard D'Oyly
Carte）促成了吉尔伯特与萨利文的合作。\[4\]1881年，他建立了[萨沃伊剧院](../Page/萨沃伊剧院.md "wikilink")（Savoy
Theatre）来演出他们合作的作品，作品也因此被称为[萨沃伊歌剧](../Page/萨沃伊歌剧.md "wikilink")。他还成立了多伊利·卡特歌剧团，演出推广他们的作品长达一个世纪。

吉尔伯特与萨利文的歌剧获得了全球的成功，如今在使用英语的国家依旧经常演出。\[5\]他们的合作创造了内容和形式的革新，直接影响了整个20世纪[音乐剧的发展](../Page/音乐剧.md "wikilink")。\[6\]他们的歌剧还影响了政治演讲、文学、电影和电视，并被幽默作家大量模仿。

## 作品

[Trial_by_Jury_-_Chaos_in_the_Courtroom.png](https://zh.wikipedia.org/wiki/File:Trial_by_Jury_-_Chaos_in_the_Courtroom.png "fig:Trial_by_Jury_-_Chaos_in_the_Courtroom.png")创作。\]\]

  - 《[泰斯庇斯](../Page/泰斯庇斯.md "wikilink")》（*Thespis*或*The Gods Grown Old*）
    1871年
  - 《[陪審團的判決](../Page/陪審團的判決.md "wikilink")》（*Trial by Jury*）1875年
  - *[The Sorcerer](../Page/The_Sorcerer.md "wikilink")* (1877)
  - 《[皮纳福号军舰](../Page/皮纳福号军舰.md "wikilink")》（*H.M.S. Pinafore*或*The Lass
    That Loved a Sailor*）1878年
  - 《[彭赞斯的海盗](../Page/彭赞斯的海盗.md "wikilink")》（*The Pirates of
    Penzance*或*The Slave of Duty*）1879年
  - *[The Martyr of
    Antioch](../Page/The_Martyr_of_Antioch.md "wikilink")* (cantata)
    (1880) (Gilbert modified the poem by [Henry Hart
    Milman](../Page/Henry_Hart_Milman.md "wikilink"))
  - *[Patience](../Page/Patience_\(operetta\).md "wikilink")*, or
    *Bunthorne's Bride* ([1881](../Page/1881.md "wikilink"))
  - *[Iolanthe](../Page/Iolanthe.md "wikilink")*, or, *The Peer and the
    Peri* (1882)
  - *[Princess Ida](../Page/Princess_Ida.md "wikilink")*, or, *Castle
    Adamant* (1884)
  - 《[日本天皇](../Page/日本天皇_\(歌劇\).md "wikilink")》（*Mikado*或*The Town of
    Titipu*）1885年
  - *[Ruddigore](../Page/Ruddigore.md "wikilink")*, or, *The Witch's
    Curse* (1887)
  - *[The Yeomen of the
    Guard](../Page/The_Yeomen_of_the_Guard.md "wikilink")*, or, *The
    Merryman and his Maid* (1888)
  - *[The Gondoliers](../Page/The_Gondoliers.md "wikilink")*, or, *The
    King of Barataria* (1889)
  - *[Utopia, Limited](../Page/Utopia,_Limited.md "wikilink")*, or, *The
    Flowers of Progress* (1893)
  - 《[偉大的公爵](../Page/偉大的公爵.md "wikilink")》（*The Grand Duke*或*The
    Statutory Duel*）1896年

## 录音

[Three little Maids from school - sung by the
dwsChorale](http://upload.wikimedia.org/wikipedia/en/0/0d/Three_little_maids_Performed_by_the_dwsChorale.ogg)

## 脚注

## 参考资料

  - Baily, Leslie （1966年），《The Gilbert and Sullivan
    Book》，新版本，出版社：London: Spring Books。

[Category:英語歌劇](../Category/英語歌劇.md "wikilink")

1.  Davis Peter G，[《Smooth
    Sailing》](http://nymag.com/nymetro/arts/music/classical/reviews/5596/)，2002年1月14日发布，2008年6月4日查阅。
2.  [《Arthur
    Sullivan 1842–1900》](http://www.musicaltimes.co.uk/archive/obits/190012sullivan.html)
    ，《The Musical Times》1900年12月发表，2008年6月4日查阅，文中引用Sir George Grove的评价:
    "Form and symmetry he seems to possess by instinct; rhythm and
    melody clothe everything he touches; the music shows not only
    sympathetic genius, but sense, judgement, proportion, and a complete
    absence of pedantry and pretension; while the orchestration is
    distinguished by a happy and original beauty hardly surpassed by the
    greatest masters."
3.  Gian Andrea Mazzucato在1899年12月16日的《The Musical
    Standard》提到："\[Sullivan\]... will... be classed among the
    epoch-making composers, the select few whose genius and strength of
    will empowered them to find and found a national school of music,
    that is, to endow their countrymen with the undefinable, yet
    positive means of evoking in a man's soul, by the magic of sound,
    those delicate nuances of feeling which are characteristic of the
    emotional power of each different race."《Sir Arthur Sullivan Society
    Journal》第34期，1992年春季刊第11-12页引用该段文字。
4.  Crowther Andrew，[《The Gilbert and Sullivan Archive-The Carpet
    Quarrel
    Explained》](http://math.boisestate.edu/gas/html/quarrel.html)
    ，Boisestate于1997年6月28日发布，2008年6月4日查阅。
5.  Bradley (2005), 第一章。
6.  Downs Peter，《Actors Cast Away Cares》，报刊《Hartford
    Courant》于2006年10月18日发表。