**Y染色體**是属于[XY性別決定系統的大多數](../Page/XY性別決定系統.md "wikilink")[哺乳动物](../Page/哺乳动物.md "wikilink")（包括[人类](../Page/人类.md "wikilink")）的两条[性染色體之一](../Page/性染色體.md "wikilink")。在哺乳动物的Y染色体中含有的[SRY基因能觸發](../Page/SRY基因.md "wikilink")[睪丸的](../Page/睪丸.md "wikilink")[生长](../Page/動物胚胎發生.md "wikilink")，并由此決定[雄性](../Page/雄性.md "wikilink")[性狀](../Page/性狀.md "wikilink")。人类的Y染色体中包含约6千万个[碱基对](../Page/碱基对.md "wikilink")。Y染色体上的基因只能由[亲代中的雄性传递给](../Page/亲代.md "wikilink")[子代中的雄性](../Page/子代.md "wikilink")（即由[父亲传递给](../Page/父亲.md "wikilink")[儿子](../Page/儿子.md "wikilink")），因此在Y染色体上留下了基因的族谱，[Y-DNA分析现在已应用于家族历史的研究](../Page/Y-DNA分析.md "wikilink")。

## 概观

大多数哺乳动物每个[体细胞中含有一对](../Page/体细胞.md "wikilink")[性染色体](../Page/性染色体.md "wikilink")，其中雌性擁有兩条[X染色體](../Page/X染色體.md "wikilink")；而雄性则擁有一条X染色體與一条Y染色體。哺乳动物的Y染色體的DNA上含有能夠促使[胚胎發育成雄性的基因](../Page/胚胎.md "wikilink")，這個基因稱為[SRY基因](../Page/SRY基因.md "wikilink")。Y染色体上的其他基因还包含製造正常[精子所必需的基因](../Page/精子.md "wikilink")。

动物中也有少数具有多对性染色体的物种。例如，[鸭嘴兽依靠包含](../Page/鸭嘴兽.md "wikilink")5对染色体的[XY性別决定系统](../Page/XY性別决定系统.md "wikilink")\[1\]来决定性别。鸭嘴兽的性染色体其实与[禽类的](../Page/禽类.md "wikilink")[Z染色体具有更高的](../Page/ZW性別决定系统.md "wikilink")[同源性](../Page/同源性.md "wikilink")\[2\]，在其他哺乳动物的性别决定系统中处于核心地位的SRY基因不再参与鸭嘴兽的性别决定\[3\]。在人类中，由于[染色体变异](../Page/染色体变异.md "wikilink")，一些[男性每个](../Page/男性.md "wikilink")[体细胞中拥有一对X染色体及一条Y染色体](../Page/体细胞.md "wikilink")（“XXY”型，见[XXY综合征](../Page/XXY综合征.md "wikilink")），或一条X染色体和一对Y染色体（“XYY”型，见[XYY综合症](../Page/XYY综合症.md "wikilink"))。一些[女性每个体细胞中只有一条而不是一对X染色体](../Page/女性.md "wikilink")（“X0”型，見[特納氏症候群](../Page/特納氏症候群.md "wikilink")）或有三条X染色体（“XXX”型，见[X-三体](../Page/X-三体.md "wikilink")）。在人类的一些[疾病中](../Page/疾病.md "wikilink")，还曾出现因为SRY基因被破坏（导致[XY雌性](../Page/XY雌性.md "wikilink")）或因SRY被复制到了X染色体上（导致[XX雄性](../Page/XX雄性.md "wikilink")）而产生的反常例子。相关现象参见[雄激素不敏感综合症与](../Page/雄激素不敏感综合症.md "wikilink")[雌雄同体](../Page/雌雄同体.md "wikilink")。

## 起源与进化

### Y染色体出现之前

许多属于[变温动物的](../Page/变温动物.md "wikilink")[脊椎动物是没有性染色体的](../Page/脊椎动物.md "wikilink")。它们的性别由外界[环境因素而不是个体](../Page/环境.md "wikilink")[基因型决定](../Page/基因型.md "wikilink")。这种动物中的一部分（例如[爬行动物](../Page/爬行动物.md "wikilink")）的性别可能取决于[孵化时的](../Page/孵化.md "wikilink")[温度](../Page/温度.md "wikilink")；其他则是雌雄同体的（亦即它们每个个体中同时能产生雄性和雌性的[配子](../Page/配子.md "wikilink")）。

### 起源

X染色体和Y染色体现在被认为是由一对相同的染色体演变而来的\[4\]\[5\]，这对染色体原属于[常染色体](../Page/常染色体.md "wikilink")（区别于“性染色体”）。某个远古哺乳动物的祖先发生了[等位基因的](../Page/等位基因.md "wikilink")[变异](../Page/变异.md "wikilink")（即所谓的“[性别基因座](../Page/性别基因座.md "wikilink")”）——只要拥有这对等位基因的个体就会成为雄性\[6\]。包含这对等位基因之一的染色体最终形成了Y染色体，而包含等位基因另一半的染色体最终形成了X染色体。随着时间的推移和环境对物种的选择，对雄性个体有利而雌性个体有害（或没有明显作用）的基因在Y染色体上不断得到继承和发展，Y染色体也仍不断通过[染色体易位获得这些基因](../Page/染色体易位.md "wikilink")\[7\]。

X染色体和Y染色体之前一度被认为已向不同方向演化了大约3亿年。不过最近的研究\[8\]（尤其是鸭嘴兽[基因组测序](../Page/基因组测序.md "wikilink")\[9\]）中表明，XY性别决定系统只是在大约1.66亿年以前出现的，是在[单孔目动物](../Page/单孔目动物.md "wikilink")（[原兽亚纲](../Page/原兽亚纲.md "wikilink")）从其他哺乳动物（[兽亚纲](../Page/兽亚纲.md "wikilink")）中分离出来开始的\[10\]。这次对[兽亚纲哺乳动物XY性别决定系统诞生的](../Page/兽亚纲.md "wikilink")[重定年是基于](../Page/重定年.md "wikilink")[有袋动物](../Page/有袋动物.md "wikilink")（[后兽下纲](../Page/后兽下纲.md "wikilink")）和[胎盘动物](../Page/胎盘动物.md "wikilink")（[真兽下纲](../Page/真兽下纲.md "wikilink")）的X染色体中的某些[基因序列也出现在鸭嘴兽和飞禽类的常染色体中的发现的](../Page/基因序列.md "wikilink")\[11\]，而较早以前的估算则是基于鸭嘴兽的X染色体含有胎盘动物的某些基因序列\[12\]\[13\]。

### 重组抑制

X染色体和Y染色体之间的[基因重组已被证实是对](../Page/基因重组.md "wikilink")[生命体有害的](../Page/生命体.md "wikilink")，它会导致雄性动物丢失Y染色体在重组之前所含有的[必需基因](../Page/必需基因.md "wikilink")、雌性动物多出原本只会出现在Y染色体上的非必需基因甚至是有害基因。所以，在进化过程中，对雄性有利的基因就逐渐在性别决定基因附近聚集，后来这个区域的基因发展出了[重组抑制机制以保护这个雄性特有的区域](../Page/重组抑制机制.md "wikilink")\[14\]。Y染色体不断沿着这种路线演化，抑制Y染色体上的基因与X染色体上的基因发生重组。这个过程最终使得Y染色体上约95%的基因不能发生重组。

### 退化

同源染色体的基因重组本是用于降低有害[突变保留的几率](../Page/突变.md "wikilink")、维持遗传完整性的，但Y染色体因不能与X染色体发生重组，被认为容易发生损毁而导致退化。人类的Y染色体在其演变的过程中丢失了原本拥有的1,438个基因中的1,393个，減少到45個基因，约每一百万年丢失4.6个基因。据推算，若Y染色体仍以这样的速率丢失基因，它有可能在一千万年后完全丧失功能\[15\]。对比基因分析的资料显示，许多哺乳动物都在丧失它们各自[杂合性染色体的功能](../Page/杂合性染色体.md "wikilink")。但，有研究指出，退化可能只会出现在受到以下三种主要[进化原动力作用下的不可重组的性染色体上](../Page/进化.md "wikilink")：高[突变率](../Page/突变率.md "wikilink")，低效率的[自然选择以及](../Page/自然选择.md "wikilink")[遗传漂变](../Page/遗传漂变.md "wikilink")\[16\]。另一方面，一项关于人类和[黑猩猩Y染色体的比较显示](../Page/黑猩猩.md "wikilink")：人类的Y染色体在六七百万年前人类从类人猿中分离、开始独自进化前并没有丢失任何基因\[17\]，这是可能证明[线性外推模型是错误的直接证据](../Page/线性外推.md "wikilink")。

另外，最近一项关于Y染色体退化的研究表明，人类的Y染色体DNA上具有大量复杂的重复基因序列及特殊的回文结构，这些结构使Y染色体可以在自身内部进行[自我基因重组等过程](../Page/自我基因重组.md "wikilink")（这些过程被称为“[Y-Y基因转换](../Page/Y-Y基因转换.md "wikilink")”），这种基因重组被认为能维持其稳定性\[18\]。

#### 高突变率

人类Y染色体由于其特殊存在环境而具有较高的突变概率。Y染色体通过男性的精子传递，而精子在[配子发生过程中经过多次](../Page/配子发生.md "wikilink")[细胞分裂](../Page/细胞分裂.md "wikilink")，每次细胞分裂都使其累积更高的碱基突变概率。此外，由于精子是被储存在高氧化环境的睾丸中，这亦使Y染色体具有更高突变概率。这两种因素的共同作用使得Y染色体具有比基因组其他部分更高的突变概率，\[19\]
根据Graves的报道指出，这种概率提高可达4.8倍。\[20\]

#### 低效自然选择

#### 遗传漂变

### 基因转换

### 未来进化方向

Y染色体的退化可能导致其他染色体将其原有基因“接管”，最终会使得Y染色体完全消失，而一个新性别决定系统则会诞生\[21\]。几种同[属的](../Page/属.md "wikilink")[鼠科及](../Page/鼠科.md "wikilink")[仓鼠科的](../Page/仓鼠科.md "wikilink")[啮齿目动物已经通过下列途径达到Y染色体演化终端](../Page/啮齿目动物.md "wikilink")\[22\]\[23\]：

  - [鼴形田鼠](../Page/鼴形田鼠.md "wikilink")\[24\]\[25\]如[土黄鼹形田鼠](../Page/土黄鼹形田鼠.md "wikilink")（*Ellobius
    lutescens*）及[坦氏鼹形田鼠](../Page/坦氏鼹形田鼠.md "wikilink")（*Ellobius
    tancrei*）的鼠类不论雄性或雌性的基因型皆为XO\[26\]，而所有坦氏鼹形田鼠的基因型皆为XX\[27\]。由第二號染色體的代替[SRY基因](../Page/SRY基因.md "wikilink")，發揮了決定雄性性別的作用。\[28\]\[29\]
  - [裔鼠屬中的](../Page/裔鼠屬.md "wikilink")和和[德之島裔鼠](../Page/德之島裔鼠.md "wikilink")，已完全丢失它们的Y染色体（包括SRY基因）\[30\]\[31\]\[32\]\[33\]，而将其余的一些原来在Y染色体上的基因转移到了X染色体上\[34\]。由代替[SRY基因](../Page/SRY基因.md "wikilink")，發揮了決定雄性性別的作用。\[35\]\[36\]
  - [林旅鼠](../Page/林旅鼠.md "wikilink")（*Myopus
    schisticolor*）、[鄂毕环颈旅鼠](../Page/鄂毕环颈旅鼠.md "wikilink")（*Dicrostonyx
    torquatus*），和[南美原鼠属](../Page/南美原鼠属.md "wikilink")（*Akodon*）中的众多物种通过X染色体和Y染色体复杂改变，演化出除了基因型为XX的雌性以外的另一种拥有一般雄性才拥有的XY基因型的雌性<ref>Hoekstra,
    H. E., and S. V. Edwards. 2000. Multiple origins of XY female mice
    (genus *Akodon*): phylogenetic and chromosomal evidence. Proceedings
    of the Royal Society of London Series B-Biological Sciences
    267:1825-1831.

</ref>\[37\]\[38\]\[39\]。

  - 在雌性[潜田鼠](../Page/潜田鼠.md "wikilink")（*Microtus
    oregoni*）中，每个个体的单个体细胞只有一条X染色体，只产生一种X配子；而雄性的潜田鼠基因型仍为XY，但可以通过[不分离现象](../Page/不分离现象.md "wikilink")（Nondisjunction）产生Y配子和不含任何性染色体的配子\[40\]。

在啮齿目动物之外，[黑麂](../Page/黑麂.md "wikilink")（*Muntiacus
crinifrons*）通过融合原有的性染色体和常染色体演化出了新的X染色体和Y染色体\[41\]。[灵长目动物](../Page/灵长目.md "wikilink")（包括人类）的Y染色体已严重退化这一现象预示着，这类动物会相对较快地发展出新的性别决定系统。学者估计，人类将在约1.4千万年后获得新的性别决定系统\[42\]\[43\]。

## 人类的Y染色体

人类的Y染色体拥有约0.58亿个[碱基对](../Page/碱基对.md "wikilink")（DNA基本结构），约占人类男性体细胞中DNA的2%\[44\]。人类Y染色体上有86个基因\[45\]，这些基因只[编码了](../Page/编码.md "wikilink")23种不同的[蛋白质](../Page/蛋白质.md "wikilink")。只有拥有Y染色体才能可能继承的[性状被称为](../Page/性状.md "wikilink")[雄性性状](../Page/雄性性状.md "wikilink")。

人类的Y染色体除了在[端粒上的](../Page/端粒.md "wikilink")[拟常染色体区的少部分片段](../Page/拟常染色体区.md "wikilink")（只占有染色体长度约5%）能与相应的X染色体发生重组，其外都不能发生重组。这些片区是由原本X染色体与Y染色体同源的片段遗留下来的。Y染色体中不能发生重组的其他部分被称为“NRY区”（non-recombining
region，[非重组区](../Page/非重组区.md "wikilink")）\[46\]或称“男性特异性区域”（Male-specific
region of the Y chromosome, MSY）。
这个区域中的[单核苷酸多态性被用于](../Page/单核苷酸多态性.md "wikilink")[父系祖先的追溯](../Page/父系祖先.md "wikilink")。MSY包括：

  - 异染色质序列，长约40Mb，未被测序
  - 常染色质序列，长约23Mb，包括156个转录单位，78个蛋白编码基因，但多数是假基因和重复拷贝，仅编码27种蛋白。分为：
      - X转作区，长约3.4Mb，含2个基因
      - X退化区，含16个在X染色体上有同源基因的单拷贝基因，包括性别决定因子SRY
      - 扩增区，为高度重复序列，长约10.2Mb，含60个基因，分属9个基因家族

## 统计资料

| **长度**      | 59,363,566 |
| ----------- | ---------- |
| **已测序序列**   | 25,653,566 |
| **已注释序列**   | 24,659,617 |
| **备份总数**    | 237        |
| **完全注释备份**  | 227        |
| **基因总数**    | 453        |
| **编码蛋白质总数** | 55         |
|             |            |

### 基因

（*不包括[拟常染色体基因](../Page/拟常染色体基因.md "wikilink")*）

  - 在X染色体上有相应基因的NRY区基因：
      - /（amelogenin，[成釉蛋白](../Page/成釉蛋白.md "wikilink")）

      - //（Ribosomal protein S4，核糖体蛋白S4）

<!-- end list -->

  - NRY区的其他基因：
      - （azoospermia factor 1，[无精症因子1](../Page/无精症因子1.md "wikilink")）

      - （basic protein on the Y chromosome)

      - （deleted in azoospermia，无精子缺失）

      -
      - （protein kinase, Y-linked，伴Y染色体蛋白激酶）

      -
      - [SRY](../Page/SRY基因.md "wikilink")（Y染色体性别决定区）

      - (testis-specific protein，睾丸特异蛋白)

      -
      - （ubiquitously transcribed TPR gene on Y chromosome）

      - （zinc finger protein，[锌指蛋白](../Page/锌指蛋白.md "wikilink")）

### 伴Y染色体[疾病](../Page/疾病.md "wikilink")

伴Y染色体的疾病有很常见的也有很罕见的。然而，通过研究罕见的伴Y染色体疾病在了解Y染色体在正常情况下的职能仍然具有十分重要的作用。

#### 较常见疾病

"生命"的必需基因在X染色體上，而Y染色体上並沒有，因而人類單個有X染色體即可以存活，表示**Y染色體並不是必需存在的染色體**。唯一明确的与Y染色体的缺陷有关的人类疾病是[睾丸发育不良](../Page/睾丸发育不良.md "wikilink")（由SRY基因的缺失或有害突变导致），但拥有两条X染色体和一条Y染色体也会导致类似症状，也就是只要有Y染色體即可能有此一症狀。另外，拥有多条Y染色体除了会造成更雄性化以外，还会带来其他影响。

##### Y染色体缺陷

即使某个个体的[核型为XY](../Page/核型.md "wikilink")，不完備的Y染色體無法影響個體雄性化，而会讓一个个体的[表现型還原为雌性](../Page/表现型.md "wikilink")（拥有类似女性的生殖器）；但如果一个女性只有一条X染色体（一般女性拥有两条X染色体）会使其生殖功能不完備而可能無法生殖。从反面来看，意即有缺陷的Y染色體會使個體雄性化失败。

造成这种疾病可以认为是由Y染色体不完整造成的：Y染色体只剩下部分片段往往导致[睾丸的](../Page/睾丸.md "wikilink")[发育不良](../Page/发育不良.md "wikilink")，因此，婴儿可能会获得内部或外部没有完全成型的男性生殖器，也可能产生结构模糊化现象（尤其是存在[镶嵌现象的情况下](../Page/镶嵌现象.md "wikilink")）。如果剩下的Y染色体片段过小或失去功能，该婴儿可能发育为具有[特纳氏综合症和](../Page/特纳氏综合症.md "wikilink")[混合性性腺发育不良症症状的女孩](../Page/混合性性腺发育不良症.md "wikilink")。

##### XXY基因型

#### 较罕见疾病

下列这些Y染色体所造成的疾病较罕见，但是由于其Y染色体的性质而表现显著。

##### Y染色体多于两条

Y染色体多体症（在每个细胞中有多于两条的Y染色体，如XYYYY）很大程度上是罕见的。在这种情况下，额外的遗传物质会导致骨骼畸形，智力下降，生长迟缓，但是其所表现出来的症状的严重性会有差异。

##### XX男性综合症

是当[基因重组发生在男性的](../Page/基因重组.md "wikilink")[配子上所引发的](../Page/配子.md "wikilink")，这造成了Y染色体上的[SRY基因](../Page/SRY基因.md "wikilink")（Y染色体性别决定区）移动到了X染色体上。当这样的一个X染色体发育成一个孩子，这个孩子将会因为SRY基因而发展成一个男孩。

### 遗传谱系

在人类[遗传谱系](../Page/遗传谱系.md "wikilink")（其应用从[遗传学到](../Page/遗传学.md "wikilink")[系譜學](../Page/系譜學.md "wikilink")）的研究中，Y染色体所包含的信息尤其使人感兴趣。\[47\]因为与其他基因不同，Y染色体只由父亲遗传给儿子。（另外，[线粒体DNA只进行母系遗传](../Page/线粒体DNA.md "wikilink")，这使人们可以用一种类似的方法来追踪母系遗传谱系信息。）

## 非哺乳动物物种的Y染色体

除了哺乳动物，自然界中还有其他生物群体也具有XY性别决定系统，但是这些系统中的Y染色体和哺乳动物的Y染色体的祖先并不相同。

具有Y染色体的非哺乳动物包括部分[昆虫](../Page/昆虫.md "wikilink")（如[果蝇](../Page/果蝇.md "wikilink")）、[鱼类](../Page/鱼类.md "wikilink")、[爬行动物](../Page/爬行动物.md "wikilink")。另外，部分[植物也具有Y染色体](../Page/植物.md "wikilink")。

就[黑腹果蝇而言](../Page/黑腹果蝇.md "wikilink")，Y染色体并不引起雄性果蝇的发育。相反地，性别决定于X染色体的数目，但黑腹果蝇的Y染色体包含了对雄性[生育能力必要的基因](../Page/生育能力.md "wikilink")。所以，基因型为XXY的黑腹果蝇为雌性，只拥有单个X染色体（XO）染色体的为雄性但不能生育。但，一些种类的果蝇在只拥有单个X基因时依然具有生育能力。

### ZW型性染色体

其他物种含有[镜像性染色体](../Page/镜像性染色体.md "wikilink")：即雌性的基因型为XY，而雄性的基因型则为XX。但为避免混淆，生物学家把这类物种中雌性的Y染色体称作[W染色体](../Page/W染色体.md "wikilink")，对应的另一个染色体称作[Z染色体](../Page/Z染色体.md "wikilink")。

部分雌性的[鸟](../Page/鸟.md "wikilink")、[蛇及](../Page/蛇.md "wikilink")[蝴蝶含有Z染色体及W染色体](../Page/蝴蝶.md "wikilink")，雄性的含有两个Z性染色体。这些物种的性别决定系统属于[ZW性别决定系统](../Page/ZW性别决定系统.md "wikilink")。

## 参考文献

{{-}}

[Category:人類染色體](../Category/人類染色體.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.  [Human Male Still A Work in
    Progress](http://www.npr.org/blogs/health/2010/01/human_male_still_a_work_in_pro.html)

9.
10.
11.
12. Nature 432, 913-917 (16 December 2004) | <doi:10.1038/nature03021>

13. DOI 10.1007/BF00360536

14.
15. Graves, J.A.M. 2004. The degenerate Y chromosome- can conversion
    save it? Reproduction Fertility and Development 16:527-534.

16. Graves, J.A.M. 2006. Sex chromosome specialization and degeneration
    in mammals. Cell 124:901-914

17.

18.

19.
20.
21. Graves, J. A. M. 2006. Sex chromosome specialization and
    degeneration in mammals. Cell 124:901-914.

22. Marchal, J. A., M. J. Acosta, M. Bullejos, R. D. de la Guardia, and
    A. Sanchez. 2003, Sex chromosomes, sex determination, and sex-linked
    sequences in Microtidae:266-273.

23. Wilson, M. A., and K. D. Makova. 2009. Genomic analyses of sex
    chromosome evolution. Annual Review of Genomics and Human Genetics
    10:333-354.

24. [第三性别将出现
    未来男性会消失？-中国新闻网](http://www.chinanews.com/m/life/2016/07-02/7925369.shtml)

25. [人類Ｙ染色體並未減少／安啦 男人不會滅絕 - 國際 -
    自由時報電子報](http://news.ltn.com.tw/news/world/paper/563225)

26.
27.
28. [第三性别将出现
    未来男性会消失？-中国新闻网](http://www.chinanews.com/m/life/2016/07-02/7925369.shtml)

29. [人類Ｙ染色體並未減少／安啦 男人不會滅絕 - 國際 -
    自由時報電子報](http://news.ltn.com.tw/news/world/paper/563225)

30. Just, W., A. Baumstark, A. Suss, A. Graphodatsky, W. Rens, N.
    Schafer, I. Bakloushinskaya et al. 2007. *Ellobius lutescens*: Sex
    determination and sex chromosome. Sexual Development 1:211-221.

31. Graves 2006

32. Arakawa, Y., C. Nishida-Umehara, Y. Matsuda, S. Sutou, and H.
    Suzuki. 2002. X-chromosomal localization of mammalian Y-linked genes
    in two XO species of the Ryukyu spiny rat. Cytogenetic and Genome
    Research 99:303-309.

33. [Additional copies of CBX2 in the genomes of males of mammals
    lacking SRY, the Amami spiny rat (Tokudaia osimensis) and the
    Tokunoshima spiny rat (Tokudaia
    tokunoshimensis).](http://www.ncbi.nlm.nih.gov/pubmed/21656076)

34. Arakawa et al. 2002

35. [第三性别将出现
    未来男性会消失？-中国新闻网](http://www.chinanews.com/m/life/2016/07-02/7925369.shtml)

36. [人類Ｙ染色體並未減少／安啦 男人不會滅絕 - 國際 -
    自由時報電子報](http://news.ltn.com.tw/news/world/paper/563225)

37. Marchal et al. 2003

38. Ortiz, M. I., E. Pinna-Senn, G. Dalmasso, and J. A. Lisanti. 2009.
    Chromosomal aspects and inheritance of the XY female condition in
    *Akodon azarae* (Rodentia, Sigmodontinae). Mammalian Biology
    74:125-129.

39. [Y染色體會最終消失嗎？男人呢？](http://blog.sciencenet.cn/blog-274385-1083367.html)

40. Charlesworth, B., and N. D. Dempsey. 2001. A model of the evolution
    of the unusual sex chromosome system of *Microtus oregoni*. Heredity
    86:387-394.

41. Zhou, Q., J. Wang, L. Huang, W. H. Nie, J. H. Wang, Y. Liu, X. Y.
    Zhao et al. 2008. Sex chromosomes in the black muntjac recapitulate
    incipient evolution of mammalian sex chromosomes. Genome Biology
    9:11.

42.
43. Goto, H., L. Peng, and K. D. Makova. 2009. Evolution of X-degenerate
    Y chromosome genes in greater apes: conservation of gene content in
    human and gorilla, but not chimpanzee. Journal of Molecular
    Evolution 68:134-144.

44. [National Library of Medicine's Genetic Home
    Reference](http://ghr.nlm.nih.gov/chromosome=Y)

45.

46. [ScienceDaily.com
    Apr. 3, 2008](http://www.sciencedaily.com/releases/2008/04/080401184955.htm)

47. [www.smgf.org
    查阅相关系谱学信息](http://www.smgf.org/page.jspx?name=together)