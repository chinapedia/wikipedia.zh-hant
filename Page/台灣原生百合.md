**臺灣原生百合**是對[臺灣](../Page/臺灣.md "wikilink")[原生種的](../Page/原生物種.md "wikilink")[百合之統稱](../Page/百合.md "wikilink")，包括以下3個[物種](../Page/物種.md "wikilink")（區分成4個[變種](../Page/變種.md "wikilink")）：

  - [條葉百合](../Page/條葉百合.md "wikilink") (*Lilium callosum* Sieb. et
    Zucc., Fl. Jap. 1: 86, t. 41. 1939. ——*L. tatanense* Hay., Icon. Pl.
    Form. 4: 98. 1914)

<!-- end list -->

  -
    產臺灣、廣東、浙江、安徽、江蘇、河南和東北。生山坡或草叢中，海拔182-640米。朝鮮和日本也有分佈。\[1\]
      - [野小百合](../Page/野小百合.md "wikilink") (*Lilium callosum*
        var.*flaviflorum* Makino)：
    <!-- end list -->
      -
        葉無柄，線形，長 5 ～ 12 公分，寬 3 ～ 6 毫米。花柄長 2.5 ～ 4 公分；花被片長 3 ～ 5
        公分，紅色或黃紅色，具少許斑點。蒴果狹長橢圓形，長 2.5 ～ 3 公分。
        臺灣北部低海拔山區。\[2\]

<!-- end list -->

  - [麝香百合](../Page/麝香百合.md "wikilink") (*Lilium longiflorum*
    Thunb.)：又稱鐵炮百合，分布於日本[琉球群島與臺灣](../Page/琉球群島.md "wikilink")。

:\* [臺灣百合](../Page/臺灣百合.md "wikilink") (*Lilium longlflorum* var.
*formosanum* Baker)：

::葉無柄，基部抱莖，線形至披針線形，長 7 ～ 20 公分，寬 4 ～ 15 毫米。花柄長 4 ～ 9 公分；花被片長 10 ～ 15
公分，白色，中肋外面紅褐色，內面淺綠色。蒴果長橢圓形，長 4 ～ 7 公分。

::台灣全島海平面至高海拔山野（高山頂除外）開闊處。

:\* [粗莖麝香百合](../Page/粗莖麝香百合.md "wikilink") (*Lilium longiflorum* var.
*scabrum* Masamune)：

  -

      -
        葉長 14 ～ 25 公分，寬 10 ～ 25 毫米。花柄長 2.5 ～ 6 公分。蒴果長 3 ～ 6 公分。
        臺灣北部、東部海邊。\[3\]

<!-- end list -->

  - [美麗百合](../Page/美麗百合.md "wikilink")(*Lilium speciosum*
    Thunb.)：分布於大陸安徽、江西、浙江、湖南、廣西及臺灣\[4\]、日本四國、九州及[琉球群島](../Page/琉球群島.md "wikilink")。日本稱[鹿子百合](../Page/鹿子百合.md "wikilink")。

:\* [豔紅百合](../Page/豔紅百合.md "wikilink") (*Lilium speciosum* var.
*gloriosoides* Baker)\[5\]:大陸稱藥百合\[6\]

  -

      -
        葉具短柄，橢圓形，長 7 ～ 12 公分，寬 2 ～ 4 公分，花柄長 1 ～ 3 公分；花被片長 5 ～ 9
        公分，白色，1/3至1/2處有紅色斑點和斑塊。蒴果橢圓球形，長 4 ～ 5 公分。
        臺灣北部低山潮濕林下及草坡。\[7\]

<center>

<File:Lilium> callosum 2.jpg|*L. callosum* var. *flaviflorum*
<File:Lilium> formosanum sphl.jpg|*Lilium formosanum* <File:Lilium>
speciosum var gloriosoides.jpg|*L. speciosum* var. *gloriosoides* China
form

</center>

## 參考資料

<references/>

## 外部連結

  - [條葉百合](http://frps.eflora.cn/frps/Lilium%20callosum) 中国植物志。
  - [藥百合](http://frps.eflora.cn/frps/Lilium%20speciosum) 中国植物志。
  - [16.
    LILIUM　百合屬](http://subject.forest.gov.tw/species/vascular/5/32.htm#16)
    臺灣維管束植物簡誌.第伍卷。
  - [中興大學 園藝學系 臺灣原生百合屬植物種原中心及分享平台](http://web.nchu.edu.tw/~twlilium01/)

[T](../Category/台灣植物.md "wikilink") [T](../Category/百合屬.md "wikilink")
[Category:球根花卉](../Category/球根花卉.md "wikilink")

1.  [條葉百合](http://frps.eflora.cn/frps/Lilium%20callosum)，中国植物志。

2.  [百合屬](http://subject.forest.gov.tw/species/vascular/5/index-1.htm)，臺灣維管束植物簡誌.第伍卷

3.
4.  [藥百合](http://frps.eflora.cn/frps/Lilium%20speciosum)，中国植物志。

5.
6.
7.