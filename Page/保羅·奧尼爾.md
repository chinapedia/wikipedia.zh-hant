**保羅·亨利·奧尼爾**(，)，生於[密蘇里州](../Page/密蘇里州.md "wikilink")[聖路易斯](../Page/聖路易斯.md "wikilink")，第72任[美國財政部部長](../Page/美國財政部.md "wikilink")，總統[喬治·沃克·布殊的首個幕僚](../Page/喬治·沃克·布殊.md "wikilink")。2002年12月因為政府的壓力及成了被嚴苛批評對象而辭職。\[1\]在1987年至1999年期間，奧尼爾曾是一間總部位於美國[匹茲堡的](../Page/匹茲堡.md "wikilink")[美国铝业公司](../Page/美国铝业公司.md "wikilink")（Alcoa）的[行政總監及](../Page/行政總監.md "wikilink")[董事局主席](../Page/董事局.md "wikilink")。1995年，他曾任職[蘭德公司的主席](../Page/蘭德公司.md "wikilink")。

## 歷史

### 早年

雖然奧尼爾現在居於[匹茲堡](../Page/匹茲堡.md "wikilink")，但他出生於[密蘇里州](../Page/密蘇里州.md "wikilink")[聖路易斯](../Page/聖路易斯.md "wikilink")。他在[美國](../Page/美國.md "wikilink")[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")[安克拉治的](../Page/安克拉治.md "wikilink")[安克拉治高校認識了他的妻子](../Page/安克拉治高校.md "wikilink")。他們雙雙於1954年畢業。他當時與父母住在軍事基地。後來，他在[加州大學夫雷士諾分校考取經濟學學士](../Page/加州大學夫雷士諾分校.md "wikilink")，並在[印第安納大學考取](../Page/印第安納大學.md "wikilink")[公共行政碩士](../Page/公共行政.md "wikilink")。他與妻子南希（Nancy）育有4名子女及12名孫。

1961年至1966年，他在[退伍軍人事務部門擔任電腦](../Page/退伍軍人事務部門.md "wikilink")[系統分析人員](../Page/系統分析人員.md "wikilink")。1967年，他加入了[美國預算管理局](../Page/美國預算管理局.md "wikilink")，並且在1974年至1977年期間出任處理主席。當總統[傑拉爾德·福特在](../Page/傑拉爾德·福特.md "wikilink")1976年競選失敗時候，他在[紐約市加入了](../Page/紐約市.md "wikilink")[美國國際紙業公司擔任行政工作](../Page/美國國際紙業公司.md "wikilink")。1977年至1985年，他出任副主席。1985年至1987年，他則出任主席。

### 中年

1988年，總統[喬治·布殊邀請奧尼爾擔任](../Page/喬治·布殊.md "wikilink")[國防部長](../Page/美國國防部長.md "wikilink")。奧尼爾拒絕了但卻推薦[迪克·切尼](../Page/迪克·切尼.md "wikilink")。後來布殊游說他擔任教育諮詢小組的主席。當時小組成員有[拉馬爾·亞歷山大](../Page/拉馬爾·亞歷山大.md "wikilink")、[比尔·布洛克](../Page/比尔·布洛克.md "wikilink")（Bill
Brock）和[理查德·赖利](../Page/理查德·赖利.md "wikilink")（Richard
Riley）。在奧尼爾的領導下，小組建議一些國家標準及統一測試標準。

當奧尼爾在2000年末不再擔任[美国铝业公司的](../Page/美国铝业公司.md "wikilink")[董事局主席後](../Page/董事局.md "wikilink")，公司的收益從1987年的150億[美元上升至](../Page/美元.md "wikilink")2000年的230億美元。他的個人財富增加至6000萬美元。

### 社區服務事業

1997年12月，奧尼爾與[猶太人保健基金會主席](../Page/猶太人保健基金會.md "wikilink")[卡伦·沃克·费因斯坦](../Page/卡伦·沃克·费因斯坦.md "wikilink")（Karen
Wolk
Feinstein）成立了[匹茲堡區域保健倡議組織](../Page/匹茲堡區域保健倡議組織.md "wikilink")（PRHI）。
\[2\]
他們結合了不同保建需求，並指出保健問題。PRHI把豐田生產系統注入了「病人護理完善化的制度」。\[3\]奧尼爾成了國家及地區領袖，指出了保健的質素及安全問題。\[4\]

奧尼爾與[匹茲堡崗位公報的出版者一起被](../Page/匹茲堡崗位公報.md "wikilink")[湯姆·墨菲市長選為](../Page/湯姆·墨菲.md "wikilink")[匹茲堡的](../Page/匹茲堡.md "wikilink")[濱水工作小組的共同領導](../Page/濱水工作小組.md "wikilink")。

## 布殊時期

奧尼爾被布殊委任為財政部部長。他為人敢言，常常在傳媒面前直說與執政黨相左的言論，有時候做出奇怪的行為。譬如與歌手[波諾一起開始非洲旅程](../Page/波諾.md "wikilink")。

2002年，作為財長的他委託製作的報告指出美國將會面臨5千億[美元的](../Page/美元.md "wikilink")[赤字](../Page/赤字.md "wikilink")。報告建議大幅加稅，大幅削減支出；為了保障美國的下一代，這兩個措施都是不能避免。研究估計拉近預算差距需要大幅加稅達66百分比。布殊把這個研究結果從2003年2月發行的2004年度財政預算報告中剔除。

在布殊的減稅政策上，他與布殊私下鬥爭。他更推動調查[阿爾蓋達組織來自](../Page/阿爾蓋達組織.md "wikilink")[阿拉伯聯合酋長國的資金](../Page/阿拉伯聯合酋長國.md "wikilink")。這些都導致他在2002年辭職，由[約翰·威廉·史諾所取代](../Page/約翰·威廉·史諾.md "wikilink")。

### 《忠誠的代價》

2004年出版的《忠誠的代價：美國前財長保羅·奧尼爾眼中的布殊和白宮》描述了他任期內布殊政府。\[5\]這本書由前*[華爾街日報](../Page/華爾街日報.md "wikilink")*
記者及獲得[美國普立茲獎的新聞工作者](../Page/美國普立茲獎.md "wikilink")[朗·薩斯金德所編寫的](../Page/朗·薩斯金德.md "wikilink")。該書描述布殊的經濟政策很不負責任的，對事情不聞不問，當伊拉克戰爭計劃在[美國國家安全委員會首次會議中制定後](../Page/美國國家安全委員會.md "wikilink")，不久布殊政府便接管了委員會。\[6\]\[7\]

奧尼爾因為最初曾經宣稱布殊過早希望入侵伊拉克而遭到批評。如果得到證實，[唐寧街備忘錄都支持奧尼爾及](../Page/唐寧街備忘錄.md "wikilink")[理察·克拉克的立場](../Page/理察·克拉克.md "wikilink")。雖然兩者都是布殊總統的幕僚，但現在也抨擊布殊。

## 評論與見解

根據2001年7月25日的一份[國際先驅論壇報文章](../Page/國際先驅論壇報.md "wikilink")，他談及全球性[金融市場不可避免的金融](../Page/金融市場.md "wikilink")「傳染」的理論。他也認為當投資者憂慮在[阿根廷和](../Page/阿根廷.md "wikilink")[土耳其的金融危機會向](../Page/土耳其.md "wikilink")[巴西及其他地方擴散而撤出新興市場](../Page/巴西.md "wikilink")。

他說這個看法只是一個「時尚」並且「我們需要淘汰那些像呼拉圈的時尚」。「配以放大鏡，你不能發現土耳其和阿根廷之間的聯繫，除了可能在腦袋裡的聯繫」，在一個妥善管理的全球性制度，投資者簡單地因為這些個別的麻煩而不從新興市場的貸款上撤走。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:美国财政部长](../Category/美国财政部长.md "wikilink")
[Category:美國商人](../Category/美國商人.md "wikilink")
[Category:兰德公司人物](../Category/兰德公司人物.md "wikilink")
[Category:克萊蒙研究大學校友](../Category/克萊蒙研究大學校友.md "wikilink")

1.  [美國之音中文網:保羅·奧尼爾 --
    最近去職的美國財政部長](http://www.voanews.com/chinese/archive/2002-12/a-2002-12-18-8-1.cfm)
2.  [匹茲堡區域保健倡議組織官方網站](http://www.prhi.org)
3.  [病人護理完善化的制度](http://www.prhi.org/ppc.cfm)
4.  [現代保健在線](http://www.modernhealthcare.com/page.cms?pageId=1063)
5.  [中國圖書網
    忠誠的代價：美國前財長保羅·奧尼爾眼中的布殊和白宮](http://www.china-tushu.com/bookdetail254257/)

6.  [Salon.com
    article](http://www.salon.com/politics/war_room/2006/06/20/911pdb/index.html?source=war_room.rss)

7.  [衛報在線](http://www.guardian.co.uk/usa/story/0,12271,1120959,00.html)