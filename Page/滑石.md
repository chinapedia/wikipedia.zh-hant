**滑石**（，矽酸鎂）是已知最软的[矿物](../Page/矿物.md "wikilink")，其[莫氏硬度标为](../Page/莫氏硬度标.md "wikilink")1。用指甲可以在滑石上留下划痕。滑石一般为[白色](../Page/白色.md "wikilink")，略带[青色或](../Page/青色.md "wikilink")[绿色](../Page/绿色.md "wikilink")。

## 形成

滑石是其他矿物在地下发生长期的物理、[化学变化而成的](../Page/化学变化.md "wikilink")，属于一种“变质矿物”，通常由[镁的岩石经变质而成](../Page/镁.md "wikilink")。

比如由[碳酸镁构成的](../Page/碳酸镁.md "wikilink")[白云岩](../Page/白云岩.md "wikilink")，在富含[硅的高温](../Page/硅.md "wikilink")[热液作用下](../Page/热液.md "wikilink")，主要成分就可能渐变成[水合硅酸镁](../Page/水合硅酸镁.md "wikilink")，即滑石。

而一些含有[橄榄石](../Page/橄榄石.md "wikilink")、[顽火辉石](../Page/顽火辉石.md "wikilink")、[角闪石等其他](../Page/角闪石.md "wikilink")[含镁硅酸盐的岩石](../Page/含镁硅酸盐.md "wikilink")，也会在类似过程中发生成分重组，向滑石转变。

## 用途

### 滑石粉

滑石内部的[晶体结构非常松散](../Page/晶体.md "wikilink")，受摩擦很容易破碎。
因此滑石常磨成[滑石粉](../Page/滑石粉.md "wikilink")，被广泛应用于各方面。

由于滑石粉细腻，常用于减小[摩擦](../Page/摩擦.md "wikilink")。
比如[医生和](../Page/医生.md "wikilink")[电工用的手套](../Page/电工.md "wikilink")，内面抹有滑石粉，戴起来不易粘手。
滑石粉也常會添加[氧化鋅](../Page/氧化鋅.md "wikilink")、[香精等成分](../Page/香精.md "wikilink")，用來作為嬰兒[爽身粉](../Page/爽身粉.md "wikilink")。

滑石粉本身对人体无害，因此也被添加在食品、药品和化妆品中，用来美化色泽，改善口感或触感。
不過，由於滑石是天然礦石，未經過濾的天然滑石粉，可能會含有[石棉](../Page/石棉.md "wikilink")，被人吸进[肺裏会危及健康](../Page/肺.md "wikilink")，不適宜直接使用。
現時歐盟要求所有含有滑石粉的嬰兒用品及化粧品均必須使用經過過濾的滑石粉，以免混雜有石棉。 ，如有些爽身粉裹就有這種成份。

### 雕刻

由于软滑细腻，滑石雕刻起来要比其他石头容易。
但因为质地太软，制成[摆件不够结实](../Page/摆件.md "wikilink")，于是中国古代的滑石雕刻，大多作为[明器](../Page/明器.md "wikilink")（随葬品）。

### 药用

[中医学用作清热](../Page/中医学.md "wikilink")、利湿药，性寒、味甘，主治小便淋沥热痛、暑热烦渴、泻痢等症。

### 滑石筆

由於滑石可在较粗糙的地面上刻画出[白色痕迹](../Page/白色.md "wikilink")，遂将天然滑石塊切割或打磨成一定形狀，制成[滑石笔](../Page/滑石笔.md "wikilink")，广泛使用于工业领域。利用其耐高溫特性，應用於[電焊切割劃線](../Page/電焊.md "wikilink")，記錄等。划痕比[石膏制成的](../Page/石膏.md "wikilink")[粉笔要清晰](../Page/粉笔.md "wikilink")，而且不损伤表面。有時也用於普通牆面、黑板面、地面的書寫。但其生产成本较高，目前还不能代替用于在黑板上写字的[粉笔](../Page/粉笔.md "wikilink")。

<center>

Talc.jpg|晶體滑石粉 Talcum Powder.JPEG|滑石粉 TalcoEZ.jpg Mineraly.sk -
mastenec.jpg

</center>

## 註釋

## 参考资料

  - [滑石 - chinatalc](http://www.chinatalc.com/info/20080530/7.html)
  - [滑石 - Webmineral.com](http://webmineral.com/data/Talc.shtml)
  - [滑石 - Mindat.org](http://www.mindat.org/min-3875.html)
  - [Talc inc](http://www.talc-powder.com) by Scotu.

## 外部連結

  - [滑石
    Huashi](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00401)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

[Category:层状硅酸盐](../Category/层状硅酸盐.md "wikilink")