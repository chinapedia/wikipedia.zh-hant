**孟加拉管辖区**，**（Bengal
Presidency）**，正式名称为**[威廉堡管辖区](../Page/威廉堡_\(印度\).md "wikilink")**，是[英属印度的一个地区](../Page/英属印度.md "wikilink")，最大时包括现在的[孟加拉国](../Page/孟加拉国.md "wikilink")，印度的[西孟加拉邦](../Page/西孟加拉邦.md "wikilink")、[阿萨姆邦](../Page/阿萨姆邦.md "wikilink")、[比哈尔邦](../Page/比哈尔邦.md "wikilink")、[奥里萨邦](../Page/奥里萨邦.md "wikilink")、[北方邦](../Page/北方邦.md "wikilink")、[Uttarakhand](../Page/Uttarakhand.md "wikilink")、[旁遮普邦](../Page/旁遮普邦.md "wikilink")、[哈里亚纳邦和](../Page/哈里亚纳邦.md "wikilink")[喜马偕尔邦和](../Page/喜马偕尔邦.md "wikilink")[Chhatisgarh](../Page/Chhatisgarh.md "wikilink")，[Madhya
Pradesh和](../Page/Madhya_Pradesh.md "wikilink")[马哈拉施特拉邦的一部分](../Page/马哈拉施特拉邦.md "wikilink")，[巴基斯坦的](../Page/巴基斯坦.md "wikilink")[西北边境省和](../Page/西北边境省.md "wikilink")[旁遮普省](../Page/旁遮普省.md "wikilink")。[缅甸](../Page/缅甸.md "wikilink")，[槟榔屿和](../Page/槟榔屿.md "wikilink")[新加坡在行政上也曾是管辖区的一部分](../Page/新加坡.md "wikilink")，直到它们在1867年组成[海峡殖民地](../Page/海峡殖民地.md "wikilink")，作为[英国直辖殖民地](../Page/英国直辖殖民地.md "wikilink")。1699年，东印度公司宣布加尔各答为[Presidency
Town](../Page/英属印度诸省.md "wikilink")，但孟加拉管辖区的开始于[英国东印度公司与](../Page/英国东印度公司.md "wikilink")[莫卧儿帝国皇帝和](../Page/莫卧儿帝国.md "wikilink")[Oudh行政长官之间的](../Page/Oudh.md "wikilink")1765年条约，将[孟加拉](../Page/孟加拉.md "wikilink")、[比哈尔和](../Page/比哈尔.md "wikilink")[奥里萨置于公司管辖之下](../Page/奥里萨.md "wikilink")。孟加拉管辖区，与[马德拉斯管辖区和](../Page/马德拉斯管辖区.md "wikilink")[孟买管辖区对照](../Page/孟买管辖区.md "wikilink")，最后包括[中央省以北所有英国领地](../Page/中央省.md "wikilink")，从[恒河和](../Page/恒河.md "wikilink")[布拉马普特拉河口到](../Page/布拉马普特拉河.md "wikilink")[喜马拉雅山脉和旁遮普](../Page/喜马拉雅山脉.md "wikilink")。1831年西北省成立，随后包括[Oudh](../Page/Oudh.md "wikilink")
in （北方邦）；第一次世界大战前夕整个印度北部被分为4个lieutenant-governorships ：旁遮普、联合省、孟加拉和东孟加拉和
[阿萨姆和](../Page/阿萨姆.md "wikilink")[西北边境省](../Page/西北边境省.md "wikilink")
under a Commissioner.

## 名称起源

## 早期历史

17世纪上半叶，东印度公司在孟加拉建立最早的居留地。这些居留地完全是商业性的。1620年，一位公司代理人在巴特那立足；1624-1636年公司

## 行政改革和永久居留地

## 1905年孟加拉分治

## 参考

  - C.A. Bayly *Indian Society and the Making of the British Empire*
    (Cambridge) 1988
  - C. E. Buckland *Bengal under the Lieutenant-Governors* (London) 1901
  - Sir James Bourdillon *The Partition of Bengal* (London: Society of
    Arts) 1905
  - Susil Chaudhury *From Prosperity to Decline. Eighteenth Century
    Bengal* (Delhi) 1995
  - Sir William Wilson Hunter *Annals of Rural Bengal* (London) 1868,
    and *Orissa* (London) 1872
  - P.J. Marshall *Bengal, the British Bridgehead 1740-1828* (Cambridge)
    1987
  - John R. McLane *Land and Local Kingship in eighteenth-century
    Bengal* (Cambridge) 1993

[Category:英属印度诸省](../Category/英属印度诸省.md "wikilink")
[Category:18世纪建立的行政区划](../Category/18世纪建立的行政区划.md "wikilink")
[Category:1947年废除的行政区划](../Category/1947年废除的行政区划.md "wikilink")