**秭一大宇商用车公司**（，[英文](../Page/英文.md "wikilink")：Zyle Daewoo Bus Commercial
Vehicle）是以生產大型巴士整車與底盤為主的製造商，總部位於[韓國](../Page/韓國.md "wikilink")[富川](../Page/富川.md "wikilink")，成立於2002年。主要以大型運輸業為主要服務對象，在[企業識別標誌方面則近似](../Page/企業識別標誌.md "wikilink")[通用大宇汽車](../Page/韓國通用汽車.md "wikilink")。

## 各地製造商

  - [秭一大宇巴士公司](../Page/秭一大宇巴士公司.md "wikilink")（[釜山](../Page/釜山.md "wikilink")）
  - [桂林大宇](../Page/桂林大宇.md "wikilink")（）
  - [上海万象大宇](../Page/上海万象大宇.md "wikilink")（）
  - [成運汽車](../Page/成運汽車.md "wikilink")（）
  - [大宇巴士公司](../Page/大宇巴士公司.md "wikilink")（）
  - [Vidamco通用大宇](../Page/Vidamco通用大宇.md "wikilink")（[河內](../Page/河內.md "wikilink")）
  - [FIDC](../Page/FIDC.md "wikilink")（）
  - [WILLER ALLIANCE
    Inc.](../Page/WILLER_ALLIANCE_Inc..md "wikilink")（[東京](../Page/東京.md "wikilink")）
  - [秭一大宇巴士公司](../Page/秭一大宇巴士公司.md "wikilink")（）

## 各類巴士底盤／成車

[Daewoo_BH117K_320KK_Front.jpg](https://zh.wikipedia.org/wiki/File:Daewoo_BH117K_320KK_Front.jpg "fig:Daewoo_BH117K_320KK_Front.jpg")
[KKBus_478AC_Front.jpg](https://zh.wikipedia.org/wiki/File:KKBus_478AC_Front.jpg "fig:KKBus_478AC_Front.jpg")的Daewoo
BH120-3T\]\]
[AirbusGroup_FuheBus_2U251_Front.jpg](https://zh.wikipedia.org/wiki/File:AirbusGroup_FuheBus_2U251_Front.jpg "fig:AirbusGroup_FuheBus_2U251_Front.jpg")「基隆─新店」線採用的Daewoo
BH117H\]\]
[Metropolitan_Transport_AG-268_20050610.jpg](https://zh.wikipedia.org/wiki/File:Metropolitan_Transport_AG-268_20050610.jpg "fig:Metropolitan_Transport_AG-268_20050610.jpg")（現為[大都會客運](../Page/大都會客運.md "wikilink")）採購，現已報廢\]\]
[Daewoo-BX212-Sugizaki-Kanko.JPG](https://zh.wikipedia.org/wiki/File:Daewoo-BX212-Sugizaki-Kanko.JPG "fig:Daewoo-BX212-Sugizaki-Kanko.JPG")
[Korea-Old_Daewoo_BM090_Royal_Midi_11-04941.JPG](https://zh.wikipedia.org/wiki/File:Korea-Old_Daewoo_BM090_Royal_Midi_11-04941.JPG "fig:Korea-Old_Daewoo_BM090_Royal_Midi_11-04941.JPG")
[Seoul_City_Bus03.jpg](https://zh.wikipedia.org/wiki/File:Seoul_City_Bus03.jpg "fig:Seoul_City_Bus03.jpg")
[CSGB-CNBus_208_533FL_Front.jpg](https://zh.wikipedia.org/wiki/File:CSGB-CNBus_208_533FL_Front.jpg "fig:CSGB-CNBus_208_533FL_Front.jpg")的Daewoo
BC211MA\]\]
[CapitalBus_708FM.jpg](https://zh.wikipedia.org/wiki/File:CapitalBus_708FM.jpg "fig:CapitalBus_708FM.jpg")的Daewoo
BS120CN\]\]
[Daewoo_BC212_3_doors.jpg](https://zh.wikipedia.org/wiki/File:Daewoo_BC212_3_doors.jpg "fig:Daewoo_BC212_3_doors.jpg")

  - 大型運輸用，旅遊巴規格

<!-- end list -->

  - BX212H/S Royal High-decker
  - BH120F Royal Cruiser
  - BH119 Royal Special
  - BH117H Royal Cruistar
  - BH117L（For Hong Kong）
  - BH116 Royal Luxury
  - BH115E Royal Ecomomy
  - BH115H Royal Express

<!-- end list -->

  - 中型運輸用

<!-- end list -->

  - BF116
  - BH090 Royal Star

<!-- end list -->

  - 一般公共汽車

<!-- end list -->

  - BS120CN（全低地台，Natural gas vehicle, NGV）
  - BS110CN（全低地台，NGV/Diesel）
  - BS120SN(低入口巴士)
  - BV120MA(非低地台，2踏)
  - BC211M（非低地台，NGV/Diesel）
  - BS106/106L Royal City（非低地台，NGV/Diesel）
  - BS090 Royal Midi(非低地台)

### 新進汽車集團（1955-1971）

  - Shinjin Micro Bus（1962）
  - Shinjin Light Bus（1965）
  - Pioneer（1965）
  - FB100LK（1966）
  - B-FB-50（1966）
  - DB102L（1968）
  - DHB400C（1970）
  - DAB（1970）
  - RC420TP（1971）

### 韓國通用汽車（1972-1976）

  - DB105LC（1972）
  - BD50DL（1973）
  - BLD24（1973）
  - BD098（1976）
  - BD101（1976）
  - BU100/110（1976）

### 世韓汽車公司（1976-1983）

  - BU120（1976）
  - BL064（1977）
  - BF101（1977）
  - BR101（1980）
  - BH120（1981）
  - BV113（1982）
  - BF105（1982）

### [大宇汽車公司](../Page/大宇汽車公司.md "wikilink")（Next 1, 1983-1994）

  - BV101（1983）
  - BH115H（1986）
  - BH120H（1985）
  - BS105（1986）
  - BU113（1986）
  - BF120（1987）
  - BS106（1991）
  - BH120F（1992）
  - BH113（1994）

### 大宇重工業（1994-1999）

  - BH117H（1995）
  - BM090（1996）
  - BH116（1997）
  - BH115E（1998）

### 大宇汽車公司（Next 2, 1999-2002）

  - BF106（2001）
  - BH090（2001）
  - BS090（2002）
  - BV120MA（2002）
  - BS120CN（2002）

### 大宇巴士（今天）

  - BS120CN（2002）
  - BH119（2003）
  - BX212（2004）【BX212H,BX212S,BX212MT(2009)】
  - BC211M（2005）
  - BC211MA（2007）
  - FX SERIES（2007）【FX115,FX116,FX117,FX118,FX120,FX212】
  - BH212（2010）
  - DM 1724 urban bus
  - DM 1731 suburban bus

## 參閱

  - [大宇集團](../Page/大宇集團.md "wikilink")
  - [康明斯](../Page/康明斯.md "wikilink")
  - [塔塔大宇商用車](../Page/塔塔大宇商用車.md "wikilink")

## 外部連結

  - [秭一大宇商用车韓文網](http://www.daewoobus.co.kr)
  - [秭一大宇商用车哈萨克斯坦網](http://www.daewoobus.kz)

[Category:韓國汽車公司](../Category/韓國汽車公司.md "wikilink")
[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[Category:大宇集团](../Category/大宇集团.md "wikilink")