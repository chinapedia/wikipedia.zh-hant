**加拉廷縣**（**Gallatin County,
Montana**）是[美國](../Page/美國.md "wikilink")[蒙大拿州南部的一個縣](../Page/蒙大拿州.md "wikilink")，西南鄰[愛達荷州](../Page/愛達荷州.md "wikilink")，東南鄰[懷俄明州](../Page/懷俄明州.md "wikilink")。面積6,816平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口67,831人。縣治[博茲曼](../Page/博茲曼.md "wikilink")
(Bozeman)。

建於1865年2月2日，是原來的九個縣之一。縣名紀念代表[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[聯邦眾議員](../Page/美國眾議院.md "wikilink")、[財政部長](../Page/美國財政部長.md "wikilink")、駐[英國](../Page/英國.md "wikilink")、[法國大使的](../Page/法國.md "wikilink")[阿伯特·加拉廷](../Page/阿伯特·加拉廷.md "wikilink")
（Albert
Gallatin）。1997年11月7日，[黃石國家公園蒙大拿州部分分別併入本縣和](../Page/黃石國家公園_\(蒙大拿州\).md "wikilink")[帕克縣](../Page/帕克縣_\(蒙大拿州\).md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/蒙大拿州行政區劃.md "wikilink")

1.  [縣政府網站](http://www.gallatin.mt.gov/Public_Documents/GallatinCoMT_WebDocs/GCHistory)