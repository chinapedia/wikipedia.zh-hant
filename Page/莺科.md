**莺科**（[学名](../Page/学名.md "wikilink")：）是[鸟类DNA分类系统中](../Page/鸟类DNA分类系统.md "wikilink")[鸟纲](../Page/鸟纲.md "wikilink")[雀形目的一个](../Page/雀形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")。在[鸟类传统分类系统中](../Page/鸟类传统分类系统.md "wikilink")，莺科的所有种作为[鹟科下的一个亚科](../Page/鹟科.md "wikilink")[莺亚科](../Page/莺亚科.md "wikilink")(*Sylviinae*)的成员。莺科鸟类分布在[欧洲](../Page/欧洲.md "wikilink")、[亚洲和](../Page/亚洲.md "wikilink")[非洲的一小部分](../Page/非洲.md "wikilink")，大多为[候鸟](../Page/候鸟.md "wikilink")。

## 分类

### 内部分类

主要包括以下几属：

  - [莺属](../Page/莺属.md "wikilink") *Sylvia*：大约20种；
  - *Parisoma*
  - *Pseudoalcippe*
  - [山鹛属](../Page/山鹛属.md "wikilink")
    *Rhopophilus*：一般分入[鹟科](../Page/鹟科.md "wikilink")；
  - *Lioparus*
  - [鸦雀属](../Page/鸦雀属.md "wikilink") *Paradoxornis*：约18种；
  - [红嘴鸦雀属](../Page/红嘴鸦雀属.md "wikilink") Conostoma
  - *Fulvetta*
  - [鹛雀属](../Page/鹛雀属.md "wikilink") *Chrysomma*
  - *Chamaea*

### 沿革

其他一些传统上属于本属的鸟类，被归入其他一些科。主要包括：

以下属归入[画眉科](../Page/画眉科.md "wikilink")(*Timaliidae*)：

  - [大草莺属](../Page/大草莺属.md "wikilink") *Graminicola*

以下属归入[苇莺科](../Page/苇莺科.md "wikilink")(*Acrocephalidae*)：

  - [苇莺属](../Page/苇莺属.md "wikilink") *Acrocephalus*：约35种；
  - [篱莺属](../Page/篱莺属.md "wikilink") *Hippolais*：8种；
  - *Chloropeta*

以下属归入[大尾莺科](../Page/大尾莺科.md "wikilink")(*Megaluridae*)：

  - [短翅莺属](../Page/短翅莺属.md "wikilink") *Bradypterus*：20多种；
  - [蝗莺属](../Page/蝗莺属.md "wikilink") *Locustella*：9种；
  - [大尾莺属](../Page/大尾莺属.md "wikilink") *Megalurus*：5种。

以下属归入[树莺科](../Page/树莺科.md "wikilink")(*Cettiidae*)：

  - *Pholidornis*
  - *Hylia*
  - *Abroscopus*
  - *Erythrocercus*
  - *Urosphena*
  - [地莺属](../Page/地莺属.md "wikilink") *Tesia*
  - [树莺属](../Page/树莺属.md "wikilink") *Cettia*：约15种；
  - [宽嘴鶲莺属](../Page/宽嘴鶲莺属.md "wikilink") *Tickellia*

<!-- end list -->

  - [雀莺属](../Page/雀莺属.md "wikilink") *Leptopoecile*

以下两属归入[柳莺科](../Page/柳莺科.md "wikilink")(*Phylloscopidae*)：

  - [柳莺属](../Page/柳莺属.md "wikilink") *Phylloscopus*
  - [鶲莺属](../Page/鶲莺属.md "wikilink") *Seicercus*

[\*](../Category/莺科.md "wikilink")
[Category:雀形目](../Category/雀形目.md "wikilink")