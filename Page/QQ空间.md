{{Infobox Website | name = QQ空间 | logo
=[<File:Qzone-logo.png>](https://zh.wikipedia.org/wiki/File:Qzone-logo.png "fig:File:Qzone-logo.png")
| logocaption = QQ空间登录页LOGO | screenshot =Qzone_screenshot.PNG |
traded_as = | company_type =[上市公司](../Page/上市公司.md "wikilink") |
collapsible = | collapsetext = | caption = 网站首页（2015年6月14日） |
native_name = | url =  | slogan = 分享生活,留住感动 | commercial = 是 | type =
[社交網絡服務](../Page/社交網絡服務.md "wikilink") | registration = 是 | language =
[簡體中文](../Page/簡體中文.md "wikilink") | num_users = | content_license = |
programming language = | owner = 腾讯公司 | author = | editor =
[腾讯公司](../Page/腾讯公司.md "wikilink") | launch_date = | revenue =
| alexa =全球排名7\[1\]
中國排名2
() | ip = | current_status = 活躍 | footnotes = | location
=[廣東省](../Page/廣東省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")
}}
**QQ空间**（）是[腾讯计算机通讯公司](../Page/腾讯公司.md "wikilink")（）于2005年推出的一个[博客系统](../Page/博客.md "wikilink")，目前活跃于中国大陆。其推出的另一个博客系统**[TM空间](../Page/TM空间.md "wikilink")**（I-zone）目前已与QQ空间合并。

## 應用及服務

QQ空间拥有说说、日志、分享、相册、视频、留言板、音乐盒、互动、个人档等功能。此外其应用中心有许多第三方应用接入其中，著名的应用有[QQ农场](../Page/QQ农场.md "wikilink")、[QQ牧场](../Page/QQ牧场.md "wikilink")、[QQ餐厅](../Page/QQ餐厅.md "wikilink")、[抢车位](../Page/抢车位.md "wikilink")、[洛克王国等](../Page/洛克王国.md "wikilink")\[2\]。

QQ空间拥有装饰物品，如首页动画、皮肤、导航栏、漂浮物、花藤\[3\]等。但是多数装饰需使用[Q币付费](../Page/Q币.md "wikilink")，只有少数免费物品。同时亦有“[黄钻](../Page/黄钻_\(QQ\).md "wikilink")”付费业务，其拥有QQ空间装饰免费或折扣、大容量相册、个性[域名申请等诸多特权](../Page/域名.md "wikilink")。

QQ空间可由用戶提出關閉申請\[4\]。

## 版本

自QQ空间诞生以来，页面风格有多次的更新换代。第六代后，腾讯取消了自定义空间代码的功能。当前大部分用户都在使用第八代界面，第八代界面运用了较多的[HTML5技术](../Page/HTML5.md "wikilink")，界面较之前版本美观大气，官方提供了大量的模版，可自定义程度较第七代有所提升。

## 近况

近年来，随着[微信对QQ的冲击](../Page/微信.md "wikilink")，QQ空间下降趋势明显\[5\]。2013年，QQ空间成为百度网页搜索“十大热搜词”\[6\]，同时QQ空间活跃账户达6.06亿\[7\]。

2015年初，以“帮你说出爱”为目的的QQ空间[表白墙](../Page/表白墙.md "wikilink")，在全国各大学校爆红。各地学校学子也纷纷在空间上建立[表白墙](../Page/表白墙.md "wikilink")。

自2014年底开始，大量QQ用户发现登录wap版QQ空间（[z.qq.com](http://z.qq.com)）后页面不正常，频繁出现“网络繁忙”等现象且有多项功能无法使用（如：留言版、图片说说发表等）。虽有用户经过手机社交网站QQ家园下的产品论坛向客服反映后短暂恢复正常，但其后又出现了更多的问题。迄今为止，用户登录QQ空间可能会被连接重置或出现“Internal
Server
Error”的字样；通过MTK版[QQ浏览器则会定向至各大](../Page/QQ浏览器.md "wikilink")[ISP的上网出错导航](../Page/ISP.md "wikilink")。由于腾讯公司将旗下所有产品的移动版向触屏网页和客户端实施过渡并逐步关闭各产品的wap版，指出，wap版QQ空间不正常可能是腾讯有意而为之，由此造成功能手机服务不稳定的假象促使用户使用智能手机的触屏版和客户端。此举引起了大量功能机用户的不满。2018年，此页面已经改为征途官网。

## 评价

### 正面

  - QQ空间与[腾讯QQ软件高度整合](../Page/腾讯QQ.md "wikilink")，使得QQ好友能迅速了解你的文章更新。
  - QQ空间有较为细分的权限管理，可针对性设置空间及说说可见范围。
  - 信息高度整合，可与微博、人人网等其他社交账户链接。

### 负面

  - QQ空间几乎所有装饰是付费的，而一般的博客均为免费。（黄钻用户一般可免费使用所有空间装饰及功能）。
  - 部份應用需要开通“[黃鑽](../Page/黄钻_\(QQ\).md "wikilink")”後才能優先使用（有時需要黃鑽達到一定級別）。

## 注释

## 外部連結

  - [QQ空间首页](https://qzone.qq.com)

## 参见

  - [腾讯](../Page/腾讯.md "wikilink")
  - [騰訊QQ](../Page/騰訊QQ.md "wikilink")
  - [用户过亿的虚拟社区列表](../Page/用户过亿的虚拟社区列表.md "wikilink")

[Category:腾讯](../Category/腾讯.md "wikilink")
[Category:網誌](../Category/網誌.md "wikilink")

1.  [alexa.com](http://www.alexa.com/siteinfo/qzone.qq.com)
2.
3.
4.
5.  [微信用户数达9.63亿！腾讯2017第二季度净利润164亿元](http://www.techweb.com.cn/internet/2017-08-16/2574449.shtml)
6.  [百度2013搜索风云榜：“天气”成年度最热搜索词](http://www.vsharing.com/k/vertical/2013-12/692929.html)
7.  [微信用户数达9.63亿！腾讯2017第二季度净利润164亿元](http://www.techweb.com.cn/internet/2017-08-16/2574449.shtml)