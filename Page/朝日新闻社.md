**朝日新聞社**（）是一家[日本](../Page/日本.md "wikilink")[報社](../Page/報社.md "wikilink")，主要發行《[朝日新聞](../Page/朝日新聞.md "wikilink")》，也出版[雜誌](../Page/雜誌.md "wikilink")、[書籍](../Page/書籍.md "wikilink")，舉辦藝術作品的展覽、公演、運動會等活動。創立於1879年（明治12年1月25日），在日本共有5個本社與支社，284個報道室。在日本以外有31個報道室。共有21個印刷廠，是日本最大的新聞社。在日本擁有極強的影響力。其報紙銷售店的名稱為「ASA」（Asahi
Shimbun Service Anchor）。

該社與[朝日生命保險](../Page/朝日生命保險.md "wikilink")、[朝日啤酒](../Page/朝日啤酒.md "wikilink")、[朝日飲料等企業沒有任何關連](../Page/朝日飲料.md "wikilink")。

[Asahi_Shimbun_Tokyo_Head_Office.JPG](https://zh.wikipedia.org/wiki/File:Asahi_Shimbun_Tokyo_Head_Office.JPG "fig:Asahi_Shimbun_Tokyo_Head_Office.JPG")
[Asahi_shinbun01s3200.jpg](https://zh.wikipedia.org/wiki/File:Asahi_shinbun01s3200.jpg "fig:Asahi_shinbun01s3200.jpg")

## 集團成員

  - 報紙、出版社

<!-- end list -->

  - 株式會社[朝日學生新聞社](../Page/朝日學生新聞社.md "wikilink")
  - 株式會社[朝日新聞出版](../Page/朝日新聞出版.md "wikilink")
  - 株式會社[朝日Sonorama](../Page/朝日Sonorama.md "wikilink")（2007年9月30日結束營業）
  - 株式會社[英文朝日](../Page/英文朝日.md "wikilink")（2011年4月26日特別清算）
  - 株式會社[日刊體育新聞社](../Page/日刊體育新聞社.md "wikilink")（《[日刊體育](../Page/日刊體育.md "wikilink")》[東日本版發行單位](../Page/東日本.md "wikilink")）
  - 株式會社[北海道日刊體育新聞社](../Page/北海道日刊體育新聞社.md "wikilink")（《日刊體育》[北海道版發行單位](../Page/北海道.md "wikilink")）
  - 株式會社[日刊體育新聞西日本](../Page/日刊體育新聞西日本.md "wikilink")（《日刊體育》[西日本版發行單位](../Page/西日本.md "wikilink")）
  - 株式會社[日刊編集中心](../Page/日刊編集中心.md "wikilink")
  - Asahi Shimbun America, Inc.
  - Asahi Shimbun International
    Limited（總部設於[倫敦](../Page/倫敦.md "wikilink")）
  - Asahi Shimbun International
    Inc.（總部設於[紐約市](../Page/紐約市.md "wikilink")）
  - Asahi Shimbun International Pte.
    Ltd.（總部設於[新加坡](../Page/新加坡.md "wikilink")）
  - Asahi Shimbun Asia Limited（總部設於[香港](../Page/香港.md "wikilink")）

<!-- end list -->

  - 販賣

<!-- end list -->

  - [朝日新聞販賣服務株式會社](../Page/朝日新聞販賣服務.md "wikilink")
  - 株式會社[朝日新聞販賣服務名古屋](../Page/朝日新聞販賣服務名古屋.md "wikilink")
  - 株式會社[朝日販賣服務中心](../Page/朝日販賣服務中心.md "wikilink")
  - 株式會社[朝日販賣服務](../Page/朝日販賣服務.md "wikilink")
  - [大阪朝日販賣開發株式會社](../Page/大阪朝日販賣開發.md "wikilink")
  - [朝日新聞西部販賣事業株式會社](../Page/朝日新聞西部販賣事業.md "wikilink")
  - [朝日Tops株式會社](../Page/朝日Tops.md "wikilink")
  - 株式會社新販

<!-- end list -->

  - 不動產管理

<!-- end list -->

  - 株式會社[朝日大樓](../Page/朝日大樓.md "wikilink")
  - [朝日建物管理株式會社](../Page/朝日建物管理.md "wikilink")
  - 株式會社[朝日新聞房地產](../Page/朝日新聞房地產.md "wikilink")
  - [赤坂溜池大樓管理株式會社](../Page/赤坂溜池大樓管理.md "wikilink")
  - [有樂町中心大樓管理株式會社](../Page/有樂町中心大樓.md "wikilink")
  - 千-{里}-朝日阪急大樓管理株式會社

<!-- end list -->

  - 勞動派遣、保險

<!-- end list -->

  - [朝日新聞綜合服務株式會社](../Page/朝日新聞綜合服務.md "wikilink")
  - 株式會社[宮本商行](../Page/宮本商行.md "wikilink")
  - [海外新聞普及株式會社](../Page/海外新聞普及.md "wikilink")
  - 株式會社[朝日機場服務](../Page/朝日機場服務.md "wikilink")

<!-- end list -->

  - 網路事業

<!-- end list -->

  - [朝日互動株式會社](../Page/朝日互動.md "wikilink")

<!-- end list -->

  - 文化事業

<!-- end list -->

  - 株式會社[朝日文化中心](../Page/朝日文化中心.md "wikilink")
  - 株式會社[朝日文化中心千葉](../Page/朝日文化中心千葉.md "wikilink")

<!-- end list -->

  - 廣告

<!-- end list -->

  - 株式會社[朝日廣告社](../Page/朝日廣告社.md "wikilink")（Asahi Advertising Inc.）
  - 株式會社[朝日廣告服務](../Page/朝日廣告服務.md "wikilink")
  - 株式會社[東朝事務所](../Page/東朝事務所.md "wikilink")
  - 株式會社[朝日Adtec](../Page/朝日Adtec.md "wikilink")
  - 株式會社[關東朝日廣告社](../Page/關東朝日廣告社.md "wikilink")
  - 株式會社[東日本朝日廣告社](../Page/東日本朝日廣告社.md "wikilink")
  - 株式會社[三和廣告社](../Page/三和廣告社.md "wikilink")
  - 株式會社[朝日區域廣告](../Page/朝日區域廣告.md "wikilink")
  - 株式會社[大阪朝日廣告社](../Page/大阪朝日廣告社.md "wikilink")
  - 株式會社[朝日廣告社 (福岡縣)](../Page/朝日廣告社_\(福岡縣\).md "wikilink")（Asahi
    Advertising Agency Co., Ltd.）
  - 株式會社[中部朝日廣告](../Page/中部朝日廣告.md "wikilink")

<!-- end list -->

  - 旅遊

<!-- end list -->

  - 株式會社[朝日旅行社](../Page/朝日旅行社.md "wikilink")

<!-- end list -->

  - 公益

<!-- end list -->

  - 財團法人[朝日新聞文化財團](../Page/朝日新聞文化財團.md "wikilink")
  - 社会福祉法人[朝日新聞厚生文化事業團](../Page/朝日新聞厚生文化事業團.md "wikilink")

## 外部連結

  - [朝日新聞社](http://www.asahi-np.co.jp/)

  - [asahi.com](http://www.asahi.com/)

  - 2013年7月18日[被报道](http://news.takungpao.com/world/focus/2013-07/1769868.html)无法访问

[朝日新闻社](../Category/朝日新闻社.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:中央區公司 (東京都)](../Category/中央區公司_\(東京都\).md "wikilink")
[Category:日本報社](../Category/日本報社.md "wikilink")
[Category:1879年成立的公司](../Category/1879年成立的公司.md "wikilink")