**腕隧道症候群**（），縮寫為**CTS**）又稱**腕道症候群**、**腕管综合症**，俗稱**滑鼠手**，是一種常見的[職業病](../Page/職業病.md "wikilink")，多發於[電腦](../Page/電腦.md "wikilink")（[鍵盤](../Page/鍵盤.md "wikilink")、[滑鼠](../Page/滑鼠.md "wikilink")）使用者、木匠、裝配員等需要做**重覆性腕部活動**的職業。是指[正中神經在傳導至](../Page/正中神經.md "wikilink")[腕的](../Page/腕.md "wikilink")發生神經壓迫的症狀\[1\]。主要症狀包含在大拇指、食指、中指及無名指靠中指側會發生[疼痛](../Page/疼痛.md "wikilink")、[麻木及](../Page/麻木.md "wikilink")[麻刺感等狀況](../Page/感覺倒錯.md "wikilink")\[2\]。典型症狀通常是漸進式的，而且發生於晚上\[3\]，疼痛感可能延伸至手臂\[4\]患者抓握力量可能减弱且在長時間後拇指根部肌肉会萎缩\[5\]。超過一半的病例是兩手都有症狀\[6\]。

風險因子包含[肥胖](../Page/肥胖症.md "wikilink")、腕部重複動作、[妊娠](../Page/妊娠.md "wikilink")，以及[類風濕性關節炎](../Page/類風濕性關節炎.md "wikilink")\[7\]\[8\]。有證據顯示[甲狀腺機能低下也會增加風險](../Page/甲狀腺機能低下症.md "wikilink")\[9\]。[糖尿病是否與此病有關目前尚不明朗](../Page/糖尿病.md "wikilink")
。[口服避孕藥目前顯示與此無關](../Page/复合口服避孕药.md "wikilink")
。高風險職業包含電腦作業、操作震動機械，以及需要用力抓握之工作\[10\]。根據病徵、症狀和特定的理學檢查而懷疑有此診斷，也許可藉來確診\[11\]。若有拇指根部肌肉萎縮則有此症的可能性較高\[12\]。

進行[體能訓練可以降低腕隧道症候群的發生風險](../Page/體能鍛煉.md "wikilink")
。症狀可以透過穿戴[腕帶或是施打](../Page/夾板.md "wikilink")[皮質類固醇獲得改善](../Page/皮質類固醇.md "wikilink")
。使用[非類固醇消炎藥（NSAIDs）或](../Page/非甾体抗炎药.md "wikilink")[加巴喷丁並沒有效果](../Page/加巴喷丁.md "wikilink")
。切斷[手屈肌支持带的外科手術效果](../Page/手屈肌支持带.md "wikilink")，比一年的非手術治療效果來得更好
。手術後不需要再使用腕帶
。沒有任何證據支持[磁療有用](../Page/磁療.md "wikilink")\[13\]。

在美國大約有5%的人患有腕隧道症候群\[14\]。通常是在成人其發生，而女性比男性更容易罹患\[15\]，女性發生此疾病的比例為男性的3\~10倍。病患時常夜間時痛醒，但初期甩一甩就可以減輕症狀，大多數的人會以為自己睡姿不良壓迫手腕，而延誤就醫。超過33%的患者可以在沒有特別治療的情況下於約一年的時間後改善病況\[16\]。其他如[懷孕後期的女性](../Page/懷孕.md "wikilink")、[風濕性關節炎](../Page/風濕性關節炎.md "wikilink")、[糖尿病](../Page/糖尿病.md "wikilink")、[內分泌異常](../Page/內分泌.md "wikilink")、[多發性神經炎](../Page/多發性神經炎.md "wikilink")、[腫瘤及手腕](../Page/腫瘤.md "wikilink")[骨折或脫位等](../Page/骨折.md "wikilink")，都可能造成腕隧道症候群。腕隧道症候群的敘述初見於[第二次世界大战](../Page/第二次世界大战.md "wikilink")\[17\]。

## 成因

[正中神經](../Page/正中神經.md "wikilink")（Median
nerve）是主要控管大拇指、食指、中指以及一部份無名指的感覺[神經](../Page/神經.md "wikilink")。手部的正中神經在手腕處，會穿過由[腕骨與韌帶圍成的](../Page/腕骨.md "wikilink")「腕隧道」，當遭受到外來的壓迫時，就可能出現腕隧道症候群。可能原因包含：腕骨骨折，退化，變形與[關節炎可能導致腕隧道狹窄造成壓迫正中神經](../Page/關節炎.md "wikilink")，以及糖尿病，甲狀腺疾病，酒精濫用等等。

## 症狀

早期[大拇指](../Page/大拇指.md "wikilink")、[食指](../Page/食指.md "wikilink")、[中指及](../Page/中指.md "wikilink")[無名指的橈側會有麻木刺痛感](../Page/無名指.md "wikilink")，症狀會在夜間加劇，患者常常會在夜間睡覺或清晨快起床時，因手麻痛而醒來；中期則出現持續性手指疼痛麻木，且如扣釦子、拿杯子等細微動作出現障礙，麻木、疼痛症狀會延伸至手肘或肩膀；後期大拇指基端的肌肉消瘦、伸展困難，手部感覺喪失。

## 診斷

  - 斐倫式試驗法（Phalen試驗，英语：Phalen
    maneuver）：將雙肘放於桌上，雙手垂直，手腕自然下垂彎曲90度三十秒到一分鐘，會出現酸麻症狀。
  - [提內耳氏徵象](../Page/提內耳氏徵象.md "wikilink")（Tinel徵象，英语：Tinel
    sign）：輕敲患者的正中神經控管的區域，患者會有觸電或刺痛感，可能已罹患腕隧道症候群。
  - [德爾坎測試](../Page/德爾坎測試.md "wikilink")（Durkan's
    test）：施測者以拇指對加壓30秒\[18\]，若受試者感受到疼痛或[正中神經支配部分](../Page/正中神經.md "wikilink")即為陽性。
  - [神經傳導檢查](../Page/神經傳導檢查.md "wikilink")（[電學診斷](../Page/電學診斷.md "wikilink")）：檢查若發現腕部區段的正中神經傳導速度變緩甚至波形振幅變小，可判定為腕隧道症候群。有5%到10%的患者因尚在疾病初期，神經傳導速度可能尚未變慢。此檢查目前在臨床上被認為是最準確的診斷方式，但因為檢查中的電刺激可能讓病患感到不舒服，所以通常一般會優先以臨床症狀來診斷並治療。神經傳導檢查除可用來診斷，尚可用來追蹤，也可用來決定病情是否已經嚴重到需要開刀治療。

## 治療

### 日常生活

首要是去除日常生活當中可能的誘發因子，減少腕部不當的姿勢及重複性動作，當症狀輕微不影響日常生活時，治療的重點只需注意避免腕部過度勞累即可。

### 職能治療師所製作的副木

  - 可用特製的手腕護具（豎腕副木，手托），避免腕部過度的伸展或屈曲並減少腕部活動量，以降低局部的發炎與疾病的惡化。白天使用時需注意每兩個小時需休息半個小時，以免造成末端肢體循環不好以及腕關節活動度的減少。
  - 全日使用會有更好治療效果（Walker W.C., 2000），但一般臨床上病患難以遵從，所以睡眠使用最常被建議。

### 藥物

#### 口服

  - 急性期若懷疑局部的發炎正在發展中，可以加上口服的**非類固醇抗發炎藥物**，不建議長期使用。
  - 部分神經減敏藥物可以嘗試使用，但並不治本。

#### 注射

如果症狀沒有改善時，可以局部注射治療解除麻痛的感覺。局部注射後如果症狀獲得改善，也是一種診斷佐證。

### 物理治療

有時可使用物理治療來減輕正中神經的發炎，但目前大多數的醫學文獻發現未達顯著療效，包括：雷射、超音波、電療等。

### 手術

  - 當上述治療無法改善症狀或疾病過於嚴重時，應考慮手術切開橫向的腕[關節](../Page/關節.md "wikilink")[韌帶](../Page/韌帶.md "wikilink")，以減輕正中神經受到的壓迫。
      - 可以直接經過皮膚切開。
      - 可以使用關節鏡伸入切開。
  - 兩種手術方法的治癒效果相近，但前者傷口較大，傷口復原較慢。後者因為比較不能明確看到韌帶上的切點，所以有機會切到其他正常組織（如血管），是其缺點。

### 中醫食療

**當歸排骨湯**

踏入秋冬季節，中醫建議湯水佐膳多選用溫經絡、補益氣血的藥材，適合平素手足冰涼的人士服用。

材料： 當歸三錢、熟地三錢、懷牛膝三錢、肉桂一錢、玉竹三錢、黑胡椒粒一錢、白酒適量、豬肉排半斤。 製作：
材料洗淨，肉排汆水，材料可剪小塊放入布袋，清水十二碗水滾後煲約一小時，調味即可。
功效： 溫經通絡，體質溫燥人士忌服。

**黑豆豬骨湯**

常從事手部勞動的人士，如偶爾出現上肢酸楚麻痹，陰雨天加重者，可考慮選用袪風利濕通絡的藥材。

材料：黑豆一兩，桑枝三錢、枸杞子三錢、當姓氏为第歸三錢、獨活二錢，豬瘦肉四兩，生薑四片。 製作：
材料洗淨，瘦肉汆水，材料放入布袋，清水八碗，水滾後煲約一小時，調味即可。
功效：袪風、利濕、通絡

**烏雞雞血藤湯**

部份腕管綜合症患者後期出現腕力減弱，掌部肌肉痿縮，宜補益氣血，濡養經絡。

材料：烏雞一份（約半斤）、雞血藤五錢、當歸三錢、白芍五錢、龍眼肉三錢、大紅棗四、生薑四片。
製作：材料洗淨，烏雞去內臟、洗淨汆水，材料可剪小塊放入布袋，清水八碗，水滾後煲約一小時，調味即可。
功效：補血活血。女士伴見月經不適者，宜先諮詢中醫意見。

## 參考資料

## 外部链接

  - [腕隧道症候群簡介](http://www.skh.org.tw/Neuro/CTS.htm)
  - [KingNet國家網路醫院](http://hospital.kingnet.com.tw/)
  - [郵政醫院](http://www.postal.com.tw/)
  - [台大醫院骨科部](https://web.archive.org/web/20071019044601/http://ntuh.mc.ntu.edu.tw/Orth/health/hsm_cts.htm)
  - [腕隧道症候群 -
    漫談職業](http://www.24drs.com/consumer/disease/occupational/8.htm)
  - [中醫生活](http://herballiving.hk/%E8%85%95%E7%AE%A1%E7%B6%9C%E5%90%88%E7%97%87-%E6%BB%91%E9%BC%A0%E6%89%8B/)

[Category:肌肉骨骼系统和结缔组织疾病](../Category/肌肉骨骼系统和结缔组织疾病.md "wikilink")
[Category:症候群](../Category/症候群.md "wikilink")
[Category:職業病](../Category/職業病.md "wikilink")

1.
2.

3.
4.
5.

6.
7.
8.

9.

10.
11.
12.
13.

14.

15.
16.
17.

18.