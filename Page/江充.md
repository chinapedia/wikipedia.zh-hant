**江充**（），字**次倩**，[西漢趙國邯鄲](../Page/西漢.md "wikilink")（今[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[邯郸市](../Page/邯郸市.md "wikilink")）人。

## 生平

本名**江齐**，通曉醫術，其妹善歌舞鼓琴，嫁与赵國太子[劉丹](../Page/劉丹_\(西漢\).md "wikilink")。江齐成為赵敬肅王[刘彭祖的上賓](../Page/刘彭祖.md "wikilink")。太子劉丹派人追殺江齊，江齊逃入[长安](../Page/长安.md "wikilink")，更名江充，汉武帝在[上林苑](../Page/上林苑.md "wikilink")[犬台宫召见江充](../Page/犬台宫.md "wikilink")。

曾出使匈奴，官至[水衡都尉](../Page/水衡都尉.md "wikilink")。江充上書[漢武帝劉徹舉發劉丹和姐妹及](../Page/漢武帝.md "wikilink")[後宮相姦](../Page/後宮.md "wikilink")，劉徹立即逮捕劉丹下獄。

武帝晚年患病，江充指使胡巫檀何欺騙武帝說：“皇宮中大有蠱氣，不除之，上疾終不差（病不愈）。”武帝信以為真，派江充成立專案小組，嚴加查察。一時江充權傾朝野。

江充與太子[劉據有仇隙](../Page/劉據.md "wikilink")，遂陷害太子，江充在太子宮掘蠱，掘出桐木做的人偶。劉據恐懼，發兵誅殺江充。江充的黨羽逃往[甘泉宮報告皇帝](../Page/甘泉宫遗址.md "wikilink")，太子已起兵造反。

汉武帝命[丞相](../Page/丞相.md "wikilink")[刘屈氂调兵平乱](../Page/刘屈氂.md "wikilink")，兩軍在长安混战五日，死者以數万人。最後太子兵敗逃亡，在长安东边的湖县泉鸠悬梁自尽。皇后[衛子夫亦自殺](../Page/衛子夫.md "wikilink")，漢朝陷入嚴重政治動亂，史称“[巫蛊之祸](../Page/巫蛊之祸.md "wikilink")”。

後來漢武帝知道太子劉據根本無謀反之心，深感後悔，故滅[江充三族](../Page/江充.md "wikilink")，處死[蘇文](../Page/苏文_\(汉朝\).md "wikilink")，在湖縣建“思子宮”。

## 參考資料

  -
[Category:生年不详](../Category/生年不详.md "wikilink")
[Category:前91年逝世](../Category/前91年逝世.md "wikilink")
[Category:西汉政治人物](../Category/西汉政治人物.md "wikilink")
[Category:邯鄲人](../Category/邯鄲人.md "wikilink")
[C充](../Category/江姓.md "wikilink")