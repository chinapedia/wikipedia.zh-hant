[Siu_Hei_Shopping_Centre.JPG](https://zh.wikipedia.org/wiki/File:Siu_Hei_Shopping_Centre.JPG "fig:Siu_Hei_Shopping_Centre.JPG")
[Siu_Hei_Court_Fishpond.jpg](https://zh.wikipedia.org/wiki/File:Siu_Hei_Court_Fishpond.jpg "fig:Siu_Hei_Court_Fishpond.jpg")池\]\]
[Siu_Hei_Court_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Siu_Hei_Court_Basketball_Court.jpg "fig:Siu_Hei_Court_Basketball_Court.jpg")
[Siu_Hei_Court_Badminton_Court.jpg](https://zh.wikipedia.org/wiki/File:Siu_Hei_Court_Badminton_Court.jpg "fig:Siu_Hei_Court_Badminton_Court.jpg")
[Siu_Hei_Court_Playground.jpg](https://zh.wikipedia.org/wiki/File:Siu_Hei_Court_Playground.jpg "fig:Siu_Hei_Court_Playground.jpg")
**兆禧苑**（）是[香港的一個](../Page/香港.md "wikilink")[居屋屋苑](../Page/居者有其屋.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門.md "wikilink")[青山灣](../Page/青山灣.md "wikilink")[填海地上](../Page/填海.md "wikilink")\[1\]。屋苑橫切面呈[風車型](../Page/風車.md "wikilink")，分兩期興建（即第7期丙及第8期甲），1985年入伙，由[置邦物業管理有限公司管理](../Page/置邦物業管理有限公司.md "wikilink")，並已成立[業主立案法團](../Page/業主立案法團.md "wikilink")，擁有1個8層[停車場](../Page/停車場.md "wikilink")，一個小型街市以及一個小型購物商場。

## 座數

| 樓宇名稱\[2\] | 樓宇類型 | 落成年份\[3\] |
| --------- | ---- | --------- |
| 安禧閣（A座）   | 風車型  | 1985      |
| 宏禧閣（B座）   |      |           |
| 順禧閣（C座）   | 1986 |           |
| 雅禧閣（D座）   |      |           |
| 樂禧閣（E座）   |      |           |
|           |      |           |

## 教育

### 幼稚園

  - [香海蓮社兆禧苑幼稚園](https://www.facebook.com/香海蓮社兆禧苑幼稚園-403064166562552/?hc_ref=ARRlCfpFfEe9DcGhgh0_1hu2S4sFszYTlaP50UW_b4rJYnp0naP0pqiV6L2S3feMRxs&fref=nf)（1986年創辦）（位於兆禧苑商業中心地下）
  - [世佛會真言宗幼兒學校](http://www.wfb.edu.hk/3/home.php)（1994年創辦）（位於兆禧苑商場地下S2）

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [輕鐵](../Page/香港輕鐵.md "wikilink")[屯門碼頭站](../Page/屯門碼頭站.md "wikilink")
  - [輕鐵](../Page/香港輕鐵.md "wikilink")[兆禧站](../Page/兆禧站.md "wikilink")

<!-- end list -->

  - [屯門碼頭](../Page/屯門碼頭.md "wikilink")
    渡輪

<!-- end list -->

  - 屯門至[東涌](../Page/東涌新發展碼頭.md "wikilink")/[沙螺灣](../Page/沙螺灣.md "wikilink")/[大澳線](../Page/大澳.md "wikilink")\[4\]
  - 屯門至[澳門](../Page/澳門.md "wikilink")[氹仔線](../Page/氹仔.md "wikilink")
  - 屯門至澳門[外港線](../Page/外港.md "wikilink")\[5\]
  - 屯門至珠海線\[6\]

<!-- end list -->

  - [屯門碼頭巴士總站](../Page/屯門碼頭巴士總站.md "wikilink")

<!-- end list -->

  - [湖翠路](../Page/湖翠路.md "wikilink")

<!-- end list -->

  - [湖景路](../Page/湖景路.md "wikilink")

</div>

</div>

## 軼事

兆禧苑的[大廈公契中](../Page/大廈公契.md "wikilink")，有條款曰不准養狗，欲購置此屋苑單位的養狗人士須注意。唯其他非滋擾性動物如鳥類、貓等則可。該苑的民選區議員為[甄紹南](../Page/甄紹南.md "wikilink")，於2015年區議會選舉中擊敗民建聯[侯國東](../Page/侯國東.md "wikilink")，接替已退休的前區局議員嚴天生。

## 名人住客

  - [林子善](../Page/林子善.md "wikilink")：香港男演員（曾居住於安禧閣A座，現已搬離）

## 屋苑周邊

屋苑鄰近有[仁濟醫院羅陳楚思小學](../Page/仁濟醫院羅陳楚思小學.md "wikilink")、[世界龍岡學校劉德容紀念小學](../Page/世界龍岡學校劉德容紀念小學.md "wikilink")、[加拿大神召會嘉智中學](../Page/加拿大神召會嘉智中學.md "wikilink")、[南屯門官立中學和提供跨境渡輪服務的](../Page/南屯門官立中學.md "wikilink")[屯門碼頭](../Page/屯門碼頭.md "wikilink")；鄰近屋苑有[湖景邨](../Page/湖景邨.md "wikilink")、[啟豐園](../Page/啟豐園.md "wikilink")、[悅湖山莊和](../Page/悅湖山莊.md "wikilink")[海翠花園](../Page/海翠花園.md "wikilink")。附近的商場有海趣坊，啟豐商場。

## 參考

[en:Public housing estates in Tuen Mun\#Siu Hei
Court](../Page/en:Public_housing_estates_in_Tuen_Mun#Siu_Hei_Court.md "wikilink")

[Category:青山灣](../Category/青山灣.md "wikilink")
[Category:蝴蝶灣](../Category/蝴蝶灣.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")

1.  [1](http://www.legco.gov.hk/yr96-97/chinese/lc_sitg/floor/970409ca.doc)
2.  [兆禧苑](http://www.housingauthority.gov.hk/b5/interactivemap/court/0,,3-0-4_5521,00.html)

3.  <http://www.rvd.gov.hk/doc/tc/nt_201504.pdf>
4.  [屯門至東涌/沙螺灣/大澳線](http://www.fortuneferry.com.hk/timetable.php)
5.  [屯門 \<=\>
    澳門](https://www.turbojet.com.hk/tc/routing-sailing-schedule/tuen-mun-macau/sailing-schedule-fares.aspx)
6.  [屯門 =\>
    珠海](https://www.turbojet.com.hk/tc/routing-sailing-schedule/tuen-mun-zhuhai/sailing-schedule-fares.aspx)