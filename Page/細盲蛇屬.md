**細盲蛇屬**（[學名](../Page/學名.md "wikilink")：*Leptotyphlops*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[細盲蛇科下的一個無毒蛇](../Page/細盲蛇科.md "wikilink")[屬](../Page/屬.md "wikilink")，主要分布於[北美洲](../Page/北美洲.md "wikilink")、[南美洲](../Page/南美洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[西南亞等](../Page/西南亞.md "wikilink")。目前共有86個品種已被確認。\[1\]''

## 特徵

大部分細盲蛇外表都像有光澤的[蚯蚓](../Page/蚯蚓.md "wikilink")。牠們多以[棕色或](../Page/棕色.md "wikilink")[粉紅色為主](../Page/粉紅色.md "wikilink")，體型幼小，身體上的鱗片有明顯的節狀紋理。其它種類的[盲蛇大概也有類似的特徵](../Page/盲蛇.md "wikilink")，不過該類盲蛇多以[黑色為主](../Page/黑色.md "wikilink")。細盲蛇的[眼睛極小](../Page/眼睛.md "wikilink")，僅成點狀，而且幾乎完全沒有視力。

## 地理分布

細盲蛇廣泛分布於[美洲](../Page/美洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[西南亞](../Page/西南亞.md "wikilink")。在美洲裡，細盲蛇分布於[美國西南部](../Page/美國.md "wikilink")、大部分[中美洲及](../Page/中美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")（包括[烏拉圭及](../Page/烏拉圭.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")）一帶。\[2\]

## 習性

盲蛇都善於掘洞，並棲息於地下。平時牠們大都生活於地洞之中，只有在雨天時才可能走上地面。盲蛇的主要食糧是[螞蟻及](../Page/螞蟻.md "wikilink")[白蟻](../Page/白蟻.md "wikilink")，與及牠們的蟲卵和幼蟲。

## 物種

| 物種\[3\]                                                                 | 學名及作者\[4\]                                                                   | 亞種數\[5\] | 異稱     | 地理分布 |
| ----------------------------------------------------------------------- | ---------------------------------------------------------------------------- | -------- | ------ | ---- |
| **[委內瑞拉細盲蛇](../Page/委內瑞拉細盲蛇.md "wikilink")**                            | Leptotyphlops affinis，<small>Boulenger，1884</small>                          | 0        |        |      |
| **[瓦氏細盲蛇](../Page/瓦氏細盲蛇.md "wikilink")**                                | Leptotyphlops albifrons，<small>Wagler，1824</small>                           | 0        |        |      |
| **L. albipunctus**                                                      | Leptotyphlops albipunctus，<small>Jan，1861</small>                            | 0        |        |      |
| **L. albiventer**                                                       | Leptotyphlops albiventer，<small>Hallermann & Rödel，1995</small>              | 0        |        |      |
| **[拜氏細盲蛇](../Page/拜氏細盲蛇.md "wikilink")**                                | Leptotyphlops anthracinus，<small>Bailey，1946</small>                         | 0        |        |      |
| **L. asbolepis**                                                        | Leptotyphlops asbolepis，<small>Thomas, McDiarmid & Thompson，1985</small>     | 0        |        |      |
| **[費氏細盲蛇](../Page/費氏細盲蛇.md "wikilink")**                                | Leptotyphlops australis，<small>Freiberg & Orejas-Miranda，1968</small>        | 0        |        |      |
| **[雙色細盲蛇](../Page/雙色細盲蛇.md "wikilink")**                                | Leptotyphlops bicolor，<small>Jan，1860</small>                                | 0        |        |      |
| **[雙線細盲蛇](../Page/雙線細盲蛇.md "wikilink")**                                | Leptotyphlops bilineatus，<small>Schlegel，1839</small>                        | 0        |        |      |
| **[布氏細盲蛇](../Page/布氏細盲蛇.md "wikilink")**                                | Leptotyphlops blanfordii，<small>Boulenger，1890</small>                       | 0        |        |      |
| **L. borapeliotes**                                                     | Leptotyphlops borapeliotes，<small>Vanzolini，1996</small>                     | 0        |        |      |
| **[迪氏細盲蛇](../Page/迪氏細盲蛇.md "wikilink")**                                | Leptotyphlops borrichianus，<small>Degerbl，1923</small>                       | 0        |        |      |
| **[曼達粉紅細盲蛇](../Page/曼達粉紅細盲蛇.md "wikilink")**                            | Leptotyphlops boulengeri，<small>Boettger，1913\]</small>                      | 0        | 博氏細盲蛇  |      |
| **[巴西細盲蛇](../Page/巴西細盲蛇.md "wikilink")**                                | Leptotyphlops brasiliensis，<small>Laurent，1949</small>                       | 0        |        |      |
| **[米卻肯州細盲蛇](../Page/米卻肯州細盲蛇.md "wikilink")**                            | Leptotyphlops bressoni，<small>Taylor，1939</small>                            | 0        |        |      |
| **[短尾細盲蛇](../Page/短尾細盲蛇.md "wikilink")**                                | Leptotyphlops brevicaudus，<small>Bocage，1887</small>                         | 0        |        |      |
| **[卡克塔細盲蛇](../Page/卡克塔細盲蛇.md "wikilink")**                              | Leptotyphlops brevissimus，<small>Shreve，1964</small>                         | 0        |        |      |
| **L. broadleyi**                                                        | Leptotyphlops broadleyi，<small>Wallach & Hahn，1997</small>                   | 0        |        |      |
| **[阿拉伯細盲蛇](../Page/阿拉伯細盲蛇.md "wikilink")**                              | Leptotyphlops burii，<small>Boulenger，1905</small>                            | 0        |        |      |
| **[開羅細盲蛇](../Page/開羅細盲蛇.md "wikilink")**                                | Leptotyphlops cairi，<small>A.M.C. Duméril & Bibron，1844</small>              | 0        |        |      |
| **[卡拉細盲蛇](../Page/卡拉細盲蛇.md "wikilink")**                                | Leptotyphlops carlae，<small>Hedges，2008</small>                              | 0        |        | 巴巴多斯 |
| **[土衛細盲蛇](../Page/土衛細盲蛇.md "wikilink")**                                | Leptotyphlops calypso，<small>Thomas, McDiarmid & Thompson，1985</small>       | 0        |        |      |
| **[領細盲蛇](../Page/領細盲蛇.md "wikilink")**                                  | Leptotyphlops collaris，<small>Hoogmoed，1977</small>                          | 0        |        |      |
| **[聖薩爾瓦多細盲蛇](../Page/聖薩爾瓦多細盲蛇.md "wikilink")**                          | Leptotyphlops columbi，<small>Klauber，1939</small>                            | 0        |        |      |
| **[角細盲蛇](../Page/角細盲蛇.md "wikilink")**                                  | Leptotyphlops conjunctus，<small>Jan，1861</small>                             | 3        |        |      |
| **[馬托葛洛索細盲蛇](../Page/馬托葛洛索細盲蛇.md "wikilink")**                          | Leptotyphlops cupinensis，<small>Bailey & Carvalho，1946</small>               | 0        |        |      |
| **[西非細盲蛇](../Page/西非細盲蛇.md "wikilink")**                                | Leptotyphlops debilis，<small>Chabanaud，1918</small>                          | 0        |        |      |
| **L. diaplocius**                                                       | Leptotyphlops diaplocius，<small>Orejas-Miranda，1969</small>                  | 0        |        |      |
| **[丹堤細盲蛇](../Page/丹堤細盲蛇.md "wikilink")**                                | Leptotyphlops dimidiatus，<small>Jan，1861</small>                             | 0        |        |      |
| **[蘇丹細盲蛇](../Page/蘇丹細盲蛇.md "wikilink")**                                | Leptotyphlops dissimilis，<small>Bocage，1886</small>                          | 0        |        |      |
| **L. distanti**                                                         | Leptotyphlops distanti，<small>Boulenger，1892</small>                         | 0        |        |      |
| **L. drewesi**                                                          | Leptotyphlops drewesi，<small>Wallach，1996</small>                            | 0        |        |      |
| **L. dugandi**                                                          | Leptotyphlops dugandi，<small>Dunn，1944</small>                               | 0        |        |      |
| **[德州細盲蛇](../Page/德州細盲蛇.md "wikilink")**                                | Leptotyphlops dulcis，<small>Baird & Girard，1853</small>                      | 2        |        |      |
| **[艾密尼細盲蛇](../Page/艾密尼細盲蛇.md "wikilink")**                              | Leptotyphlops emini，<small>Boulenger，1890</small>                            | 0        |        |      |
| **[索科特拉島細盲蛇](../Page/索科特拉島細盲蛇.md "wikilink")**                          | Leptotyphlops filiformis，<small>Boulenger，1899</small>                       | 0        |        |      |
| **L. fitzingeri**                                                       | Leptotyphlops fitzingeri，<small>Jan，1861</small>                             | 0        |        |      |
| **L. gestri**                                                           | Leptotyphlops gestri，<small>Boulenger，1906</small>                           | 0        |        |      |
| **[古氏細盲蛇](../Page/古氏細盲蛇.md "wikilink")**                                | Leptotyphlops goudotii，<small>A.M.C. Duméril & Bibron，1844</small>           | 3        | 黑細盲蛇   |      |
| **[幼線細盲蛇](../Page/幼線細盲蛇.md "wikilink")**                                | Leptotyphlops gracilior，<small>Boulenger，1910</small>                        | 0        |        |      |
| **L. guayaquilensis**                                                   | Leptotyphlops guayaquilensis，<small>Orejas-Miranda & Peters，1970</small>     | 0        |        |      |
| **L. hamulirostris**                                                    | Leptotyphlops hamulirostris，<small>Nikolsky，1907</small>                     | 0        |        |      |
| **[西部細盲蛇](../Page/西部細盲蛇.md "wikilink")**                                | Leptotyphlops humilis，<small>Baird & Girard，1853</small>                     | 8        |        |      |
| **[約書亞細盲蛇](../Page/約書亞細盲蛇.md "wikilink")**                              | Leptotyphlops joshuai，<small>Dunn，1944</small>                               | 0        |        |      |
| **[亞氏細盲蛇](../Page/亞氏細盲蛇.md "wikilink")**                                | Leptotyphlops koppesi，<small>Amaral，1955</small>                             | 0        |        |      |
| **[唇細盲蛇](../Page/唇細盲蛇.md "wikilink")**                                  | Leptotyphlops labialis，<small>Sternfeld，1908</small>                         | 0        | 達馬拉細盲蛇 |      |
| **L. leptepileptus**                                                    | Leptotyphlops leptepileptus，<small>Thomas, McDiarmid & Thompson，1985</small> | 0        |        |      |
| **[長尾細盲蛇](../Page/長尾細盲蛇.md "wikilink")**                                | Leptotyphlops longicaudus，<small>Peters，1854</small>                         | 0        |        |      |
| **[大鱗細盲蛇](../Page/大鱗細盲蛇.md "wikilink")**                                | Leptotyphlops macrolepis，<small>Peters，1857</small>                          | 0        |        |      |
| **[大眼細盲蛇](../Page/大眼細盲蛇.md "wikilink")**                                | Leptotyphlops macrops，<small>Broadley & Wallach，1996</small>                 | 0        |        |      |
| **[大吻細盲蛇](../Page/大吻細盲蛇.md "wikilink")**                                | Leptotyphlops macrorhynchus，<small>Jan，1860</small>                          | 0        |        |      |
| **[大尾細盲蛇](../Page/大尾細盲蛇.md "wikilink")**                                | Leptotyphlops macrurus，<small>Boulenger，1903</small>                         | 0        | 博氏細盲蛇  |      |
| **[巨細盲蛇](../Page/巨細盲蛇.md "wikilink")**                                  | Leptotyphlops maximus，<small>Loveridge，1932</small>                          | 0        |        |      |
| **[拉丁美洲細盲蛇](../Page/拉丁美洲細盲蛇.md "wikilink")**                            | Leptotyphlops melanotermus，<small>Cope，1862</small>                          | 0        |        |      |
| **[暗細盲蛇](../Page/暗細盲蛇.md "wikilink")**                                  | Leptotyphlops melanurus，<small>Schmidt & Walker，1943</small>                 | 0        |        |      |
| **L. munoai**                                                           | Leptotyphlops munoai，<small>Orejas-Miranda，1961</small>                      | 0        |        |      |
| **L. narirostris**                                                      | Leptotyphlops narirostris，<small>Peters，1867</small>                         | 1        |        |      |
| **[泰氏細盲蛇](../Page/泰氏細盲蛇.md "wikilink")**                                | Leptotyphlops nasalis，<small>Taylor，1940</small>                             | 0        |        |      |
| **[岡比亞細盲蛇](../Page/岡比亞細盲蛇.md "wikilink")**                              | Leptotyphlops natatrix，<small>Andersson，1937</small>                         | 0        |        |      |
| **[桑坦德細盲蛇](../Page/桑坦德細盲蛇.md "wikilink")**                              | Leptotyphlops nicefori，<small>Dunn，1946</small>                              | 0        |        |      |
| **[細盲蛇](../Page/細盲蛇.md "wikilink")**<font size="-1"><sup>T</sup></font> | Leptotyphlops nigricans，<small>Schlegel，1839</small>                         | 0        | 黑細盲蛇   |      |
| **[努爾氏細盲蛇](../Page/努爾氏細盲蛇.md "wikilink")**                              | Leptotyphlops nursii，<small>Anderson，1896</small>                            | 0        |        |      |
| **L. occidentalis**                                                     | Leptotyphlops occidentalis，<small>FitzSimons，1962</small>                    | 0        | 西部細盲蛇  |      |
| **L. perreti**                                                          | Leptotyphlops perreti，<small>Wilhelm Roux-Estève，1979</small>                | 0        |        |      |
| **[秘魯細盲蛇](../Page/秘魯細盲蛇.md "wikilink")**                                | Leptotyphlops peruvianus，<small>Orejas-Miranda，1969</small>                  | 0        |        |      |
| **[湯馬斯細盲蛇](../Page/湯馬斯細盲蛇.md "wikilink")**                              | Leptotyphlops pyrites，<small>Thomas，1965</small>                             | 0        |        |      |
| **[網狀細盲蛇](../Page/網狀細盲蛇.md "wikilink")**                                | Leptotyphlops reticulatus，<small>Boulenger，1906</small>                      | 0        |        |      |
| **[長吻細盲蛇](../Page/長吻細盲蛇.md "wikilink")**                                | Leptotyphlops rostratus，<small>Bocage，1886</small>                           | 0        | 保氏細盲蛇  |      |
| **[紅線細盲蛇](../Page/紅線細盲蛇.md "wikilink")**                                | Leptotyphlops rubrolineatus，<small>Werner，1901</small>                       | 0        |        |      |
| **[玫瑰細盲蛇](../Page/玫瑰細盲蛇.md "wikilink")**                                | Leptotyphlops rufidorsus，<small>Taylor，1940</small>                          | 0        |        |      |
| **[聖埃斯皮里圖州細盲蛇](../Page/聖埃斯皮里圖州細盲蛇.md "wikilink")**                      | Leptotyphlops salgueiroi，<small>Amaral，1955</small>                          | 0        |        |      |
| **[彼得細盲蛇](../Page/彼得細盲蛇.md "wikilink")**                                | Leptotyphlops scutifrons，<small>Peters，1854</small>                          | 0        |        |      |
| **[七帶細盲蛇](../Page/七帶細盲蛇.md "wikilink")**                                | Leptotyphlops septemstriatus，<small>Schneider，1801</small>                   | 0        |        |      |
| **[南美細盲蛇](../Page/南美細盲蛇.md "wikilink")**                                | Leptotyphlops signatus，<small>Jan，1861</small>                               | 0        |        |      |
| **[條紋細盲蛇](../Page/條紋細盲蛇.md "wikilink")**                                | Leptotyphlops striatulus，<small>Smith & Laufe，1945</small>                   | 0        |        |      |
| **L. subcrotillus**                                                     | Leptotyphlops subcrotillus，<small>Klauber，1939</small>                       | 0        |        |      |
| **L. sundewalli**                                                       | Leptotyphlops sundewalli，<small>Jan，1861</small>                             | 0        |        |      |
| **[北部細盲蛇](../Page/北部細盲蛇.md "wikilink")**                                | Leptotyphlops teaguei，<small>Orejas-Miranda，1964</small>                     | 0        |        |      |
| **L. telloi**                                                           | Leptotyphlops telloi，<small>Broadley & Watson，1976</small>                   | 0        |        |      |
| **[鋸齒細盲蛇](../Page/鋸齒細盲蛇.md "wikilink")**                                | Leptotyphlops tesselatus，<small>Tschudi，1845</small>                         | 0        |        |      |
| **[三色細盲蛇](../Page/三色細盲蛇.md "wikilink")**                                | Leptotyphlops tricolor，<small>Orejas-Miranda & Zug，1974</small>              | 0        |        |      |
| **[十一帶細盲蛇](../Page/十一帶細盲蛇.md "wikilink")**                              | Leptotyphlops undecimstriatus，<small>Schlegel，1839</small>                   | 0        |        |      |
| **[南部細盲蛇](../Page/南部細盲蛇.md "wikilink")**                                | Leptotyphlops unguirostris，<small>Boulenger，1902</small>                     | 0        |        |      |
| **[雜色細盲蛇](../Page/雜色細盲蛇.md "wikilink")**                                | Leptotyphlops variabilis，<small>Scortecci，1928</small>                       | 0        |        |      |
| **L. vellardi**                                                         | Leptotyphlops vellardi，<small>Laurent，1984</small>                           | 0        |        |      |
| **[阿根廷細盲蛇](../Page/阿根廷細盲蛇.md "wikilink")**                              | Leptotyphlops weyrauchi，<small>Orejas-Miranda，1964</small>                   | 0        |        |      |
| **[威爾遜細盲蛇](../Page/威爾遜細盲蛇.md "wikilink")**                              | Leptotyphlops wilsoni，<small>Hahn，1978</small>                               | 0        |        |      |
|                                                                         |                                                                              |          |        |      |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：細盲蛇屬](http://reptile-database.reptarium.cz/search.php?&genus=Leptotyphlops&submit=Search)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:細盲蛇科](../Category/細盲蛇科.md "wikilink")

1.

2.
3.
4.
5.