**毛氈苔粉屬**（[拉丁語](../Page/拉丁語.md "wikilink")：*Droserapites*）為一個[已滅絕的](../Page/滅絕.md "wikilink")[植物](../Page/植物.md "wikilink")[屬](../Page/屬.md "wikilink")，與[毛氈苔類有點不確定的相似](../Page/毛氈苔.md "wikilink")。

棒狀毛氈苔粉（*D.
clavatus*）的化石[花粉發現於](../Page/花粉.md "wikilink")[台灣的](../Page/台灣.md "wikilink")[中新世構成物](../Page/中新世.md "wikilink")。它是「四個、無孔型，帶有棍棒狀、棒狀以及發芽生殖的過程」，並在型態學上與[現生的](../Page/現生種.md "wikilink")[毛氈苔相配](../Page/毛氈苔.md "wikilink")。\[1\]

## 注釋

[Category:茅膏菜科](../Category/茅膏菜科.md "wikilink")
[Category:食虫植物](../Category/食虫植物.md "wikilink")
[Category:史前植物](../Category/史前植物.md "wikilink")
[Category:中新世生物](../Category/中新世生物.md "wikilink")

1.  Song, Z.-C., W.-M. Wang & F. Huang 2004. [Fossil Pollen Records of
    Extant Angiosperms in
    China.](http://www.bioone.org/perlserv/?request=get-document&doi=10.1663%2F0006-8101\(2004\)070%5B0425%3AFPROEA%5D2.0.CO%3B2)
    *The Botanical Review* **70**(4): 425–458.