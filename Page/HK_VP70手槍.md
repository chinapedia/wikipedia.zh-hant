**VP70**是一把純雙動槍機的[9毫米](../Page/9mm鲁格弹.md "wikilink")[手槍](../Page/手槍.md "wikilink")，由[德國](../Page/德國.md "wikilink")[黑克勒-科赫在](../Page/黑克勒-科赫.md "wikilink")1970年推出，槍身採用聚合物料制造，裝上[槍托後可以進行三點發射擊](../Page/槍托.md "wikilink")。VP70意思是1970年推出的****（人民手槍）。

## 歷史

[VP70Mphoto.jpg](https://zh.wikipedia.org/wiki/File:VP70Mphoto.jpg "fig:VP70Mphoto.jpg")
大部份人認為[奧地利](../Page/奧地利.md "wikilink")[格洛克是第一種聚合物料制骨架的手槍](../Page/格洛克.md "wikilink")，空槍只重820克，而其實第一種以聚合物料制造的手槍是VP70。

VP70在1970年公開，但在1973年才開始正式推出市場，除VP70M（軍用型）及VP70Z（民用型），還有一種[意大利](../Page/意大利.md "wikilink")[9×21毫米IMI民用口徑型號](../Page/9×21mm_IMI.md "wikilink")。VP70系列在1989年停產。

## 設計

VP70系列採用反衝式槍機設計，並在[套筒後方附有大型複進簧](../Page/手槍套筒.md "wikilink")。除了使用聚合物料制造外，最獨特的是VP70M的一體式槍套連[槍托](../Page/槍托.md "wikilink")，手槍獨立使用時只可單發半自動射擊，裝上塑聚合物槍托（槍套）後可進行[三點發射擊](../Page/三發點放.md "wikilink")，理論射速達到每分鐘2,200發，而射擊模式選擇鈕裝在槍托上。VP70為純雙動式扳機設計，以避免新手使用時發生走火的意外，因此扳機扣力較重。然而，因其扳機扣動力量須些為吃力，而且扣動距離過長，使得射手反而有點難以進行準確瞄準。

VP70系列在1973年推出，在1989年停產，[9×21毫米IMI口徑版本VP](../Page/9×21mm_IMI.md "wikilink")70Z約制造了400把，可裝上槍托但不能三點發射擊，主要在[意大利民用市場發售](../Page/意大利.md "wikilink")，而[9×19毫米口徑版本主要提供給軍隊及警隊](../Page/9×19mm鲁格弹.md "wikilink")。

## 使用國

  -
  -
  -
  -
  -
## 參考

  - [衝鋒手槍](../Page/全自動手槍.md "wikilink")
  - [黑克勒-科赫](../Page/黑克勒-科赫.md "wikilink")
  - [格洛克18](../Page/格洛克18.md "wikilink")
  - [三點發](../Page/三發點放.md "wikilink")

## 資料來源

  - —[HKPRO.com—HK
    VP70](http://www.hkpro.com/index.php?option=com_content&view=article&id=42:the-vp70&catid=6:the-pistols&Itemid=5)

  - —[Modern Firearms—HK
    VP70](http://world.guns.ru/handguns/hg/de/hk-vp-70-e.html)

  - —[Remtek.com—HK
    VP70Z](http://www.remtek.com/arms/hk/civ/vp/vp70z.htm)

  - —[D Boy's Gun World—HK VP70
    手枪](https://web.archive.org/web/20110714035731/http://firearmsworld.net/german/hk/vp70/VP70.htm)

[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:衝鋒手槍](../Category/衝鋒手槍.md "wikilink")
[Category:黑克勒-科赫半自动手枪](../Category/黑克勒-科赫半自动手枪.md "wikilink")
[Category:9毫米魯格彈槍械](../Category/9毫米魯格彈槍械.md "wikilink")