**高倉天皇**（；，[應保元年九月三日](../Page/應保.md "wikilink")－[治承五年一月十四日](../Page/治承.md "wikilink")），日本第80代天皇（1168年3月30日－1180年3月18日，[仁安三年二月十九日](../Page/仁安_\(日本\).md "wikilink")－[治承四年二月二十一日在位](../Page/治承.md "wikilink")）。諱**憲仁**（）。

## 略歷

高倉天皇的母親[平滋子](../Page/平滋子.md "wikilink")，是[後白河天皇讓位給](../Page/後白河天皇.md "wikilink")[二條天皇之後的寵妃](../Page/二條天皇.md "wikilink")。平滋子由於天生美貌而且聰明，頗得後白河上皇的寵倖。

當時二條天皇試圖親政，與後白河上皇之間發生衝突，朝廷的公卿亦分為支持二條天皇的[親政派和支持後白河上皇的](../Page/親政.md "wikilink")[院政派](../Page/院政.md "wikilink")。[應保元年](../Page/應保.md "wikilink")（1161年），憲仁親王出生，引起了政壇不小的波動。二條天皇發現上皇陰謀立憲仁為太子，將院政派的[平時忠](../Page/平時忠.md "wikilink")、[平教盛](../Page/平教盛.md "wikilink")、[藤原成親](../Page/藤原成親.md "wikilink")、[藤原信隆等人罷免了官職](../Page/藤原信隆.md "wikilink")，並廢止了上皇的[院政](../Page/院政.md "wikilink")。

[永萬](../Page/永萬.md "wikilink")2年（1165年），二條天皇傳位給自己的兒子順仁親王（[六條天皇](../Page/六條天皇.md "wikilink")），不久駕崩。由於六條天皇生母地位不高導致政局不穩定，院政派勢力再次抬頭，在平清盛等人的支持下，後白河上皇趁機冊立憲仁親王為皇太子；並於次年[攝政](../Page/攝政.md "wikilink")[近衛基實去世之後將親政派排擠出朝廷](../Page/近衛基實.md "wikilink")。[仁安三年](../Page/仁安_\(日本\).md "wikilink")（1168年），在後白河上皇的主導下，六條天皇禪位給了憲仁親王，是為高倉天皇。

高倉天皇在位期間，朝政皆掌握在父親[後白河法皇手裏](../Page/後白河法皇.md "wikilink")。同時，[平清盛在將高倉天皇擁上皇位中扮演著重要的角色](../Page/平清盛.md "wikilink")，[平家一門的勢力越來越大](../Page/平家.md "wikilink")。[平重盛夫妻分別成為了高倉天皇的](../Page/平重盛.md "wikilink")[乳父和](../Page/乳父.md "wikilink")[乳母](../Page/乳母.md "wikilink")。1171年，平清盛的女兒[平德子成為了高倉天皇的中宮](../Page/平德子.md "wikilink")，1178年為高倉天皇生下了第一皇子言仁親王（[安德天皇](../Page/安德天皇.md "wikilink")），平家的勢力達到全盛。次年平清盛發動[政變](../Page/治承三年政變.md "wikilink")，廢止了後白河法皇的[院政並幽禁了法皇](../Page/院政.md "wikilink")。

1180年，高倉天皇遜位給了[言仁親王](../Page/安德天皇.md "wikilink")，成為上皇。高倉上皇本人則開始院政。其間發生了[以仁王](../Page/以仁王.md "wikilink")、[源賴政](../Page/源賴政.md "wikilink")[舉兵討伐平家的事件](../Page/以仁王舉兵.md "wikilink")，[源賴朝](../Page/源賴朝.md "wikilink")、[源義仲等源氏族人紛紛](../Page/木曾義仲.md "wikilink")[起兵反對平家](../Page/源平合戰.md "wikilink")。為了能夠有效的控制朝政，在平清盛的主持下，一度將首都遷往平家的據點[福原](../Page/福原京.md "wikilink")，但不久又遷回[平安京](../Page/平安京.md "wikilink")。次年高倉上皇駕崩於[六波羅的池殿](../Page/六波羅.md "wikilink")。

高倉天皇膚色白皙而且容貌俊美，深受公卿們的羡慕。此外，高倉天皇也是個善良溫和而且多情的人，他與[葵姬](../Page/葵姬.md "wikilink")、[小督局的愛情故事在](../Page/小督局.md "wikilink")《[平家物語](../Page/平家物語.md "wikilink")》中有記載。

## 系譜

高倉天皇是[後白河天皇的第七皇子](../Page/後白河天皇.md "wikilink")。其生母[建春門院平滋子](../Page/平滋子.md "wikilink")，是[平清盛繼室](../Page/平清盛.md "wikilink")[二位尼](../Page/二位尼.md "wikilink")（平時子）的異母妹。高倉天皇的中宮[平德子](../Page/平德子.md "wikilink")，則是平清盛與二位尼的女兒。因此從輩分上來說，平德子（建禮門院）是高倉天皇的從姐。高倉天皇也是[安德天皇](../Page/安德天皇.md "wikilink")、[後鳥羽天皇以及](../Page/後鳥羽天皇.md "wikilink")[後高倉院的父親](../Page/守貞親王.md "wikilink")。

## 后妃・皇子女

  - 中宮：[平德子](../Page/平德子.md "wikilink")（建礼門院）（1155-1213） -
    [平清盛女](../Page/平清盛.md "wikilink")
      - 第一皇子：言仁親王（[安德天皇](../Page/安德天皇.md "wikilink")）（1178-1185）
  - 典侍：[藤原殖子](../Page/藤原殖子.md "wikilink")（七条院）（1157-1228）-
    [坊門信隆女](../Page/坊門信隆.md "wikilink")
      - 第二皇子：[守貞親王](../Page/守貞親王.md "wikilink")（後高倉院）（1179-1223） -
        [後堀河天皇實父](../Page/後堀河天皇.md "wikilink")
      - 第四皇子：尊成親王（[後鳥羽天皇](../Page/後鳥羽天皇.md "wikilink")）（1180-1239）
  - 准三后：[近衛通子](../Page/近衛通子.md "wikilink")（1163-?） -
    [近衛基實女](../Page/近衛基實.md "wikilink")、言仁親王准母
  - 典侍：[堀河豐子](../Page/堀河豐子.md "wikilink")（按察典侍） -
    [堀河賴定女](../Page/堀河賴定.md "wikilink")
      - 第三皇女：[潔子內親王](../Page/潔子內親王.md "wikilink")（1179-?） -
        [伊勢齋宮](../Page/齋宮.md "wikilink")
  - 掌侍：[平範子](../Page/平範子.md "wikilink")（少将内侍） -
    [平義輔女](../Page/平義輔.md "wikilink")
      - 第三皇子：[惟明親王](../Page/惟明親王.md "wikilink")（1179-1221）
  - 宮人：藤原公子?（帥局）（?-1179） -
    [藤原公重女](../Page/藤原公重.md "wikilink")、高倉天皇[乳母](../Page/乳母.md "wikilink")
      - 第一皇女：[功子內親王](../Page/功子內親王.md "wikilink")（1176-?） - 伊勢齋宮
  - 宮人：藤原氏（[小督局](../Page/小督.md "wikilink")） -
    [藤原成範女](../Page/藤原成範.md "wikilink")
      - 第二皇女：[範子內親王](../Page/範子內親王.md "wikilink")（坊門院）（1177-1210） -
        [賀茂齋院](../Page/齋院.md "wikilink")、[土御門天皇准母](../Page/土御門天皇.md "wikilink")

## 在位期間年號

  - [仁安](../Page/仁安_\(日本\).md "wikilink")
  - [嘉應](../Page/嘉應.md "wikilink")
  - [承安](../Page/承安.md "wikilink")
  - [安元](../Page/安元.md "wikilink")
  - [治承](../Page/治承.md "wikilink")

[Category:平安時代天皇](../Category/平安時代天皇.md "wikilink")