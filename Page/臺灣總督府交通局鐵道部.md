**臺灣總督府交通局鐵道部**（簡稱**鐵道部**）是[臺灣](../Page/臺灣.md "wikilink")[日治時期的](../Page/台灣日治時期.md "wikilink")[官營鐵路專責機構](../Page/國鐵.md "wikilink")，隸屬於[臺灣總督府轄下](../Page/臺灣總督府.md "wikilink")，為現今[臺灣鐵路管理局的前身](../Page/臺灣鐵路管理局.md "wikilink")。其辦公廳舍位於[臺北](../Page/臺北市.md "wikilink")[北門旁](../Page/臺北府城北門.md "wikilink")，目前列為[國定古蹟保護](../Page/國定古蹟.md "wikilink")。

## 歷史

[thumb](../Page/檔案:Redraw_railway_logo1.jpg.md "wikilink")
為火車車輪與鐵軌斷面；用於服飾帽章車輪外圍加上櫻葉、櫻蕾合抱桐紋\]\]
[thumb](../Page/檔案:Redraw_railway_logo2.jpg.md "wikilink")
為櫻葉、櫻蕾合抱桐紋與台字章\]\]
鐵道部成立於日治時代的[1899年](../Page/1899年臺灣.md "wikilink")[11月8日](../Page/11月8日.md "wikilink")，原為總督府直屬機關，[1924年改隸於總督府交通局之下](../Page/1924年.md "wikilink")。在[日本](../Page/日本.md "wikilink")[治台近](../Page/台灣日治時期.md "wikilink")50年的期間中，[1908年完成](../Page/1908年.md "wikilink")[縱貫線的全線貫通](../Page/縱貫線_\(鐵路\).md "wikilink")，形成台灣第一次「[空間革命](../Page/空間革命.md "wikilink")」。此外，鐵道部還修築了[淡水線](../Page/臺鐵淡水線.md "wikilink")、[宜蘭線](../Page/宜蘭線.md "wikilink")、[台東線等鐵路路線](../Page/臺東線.md "wikilink")；亦曾進行[北迴線](../Page/北迴線.md "wikilink")、[南迴線與](../Page/南迴線.md "wikilink")[中央山脈橫貫線的路線探查與規劃](../Page/中央山脈.md "wikilink")。

儘管難以脫離殖民統治者的心態，但鐵道部對於台灣的交通與經濟發展有一定貢獻，尤其歷任部長如[後藤新平](../Page/後藤新平.md "wikilink")、[長谷川謹介以及當時的鐵道部官員](../Page/長谷川謹介.md "wikilink")，皆為相當稱職的[技術官僚](../Page/技術官僚.md "wikilink")。

1945年，[二戰結束](../Page/二戰.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[國民政府接收台灣](../Page/國民政府.md "wikilink")，鐵道部先改制為[臺灣省行政長官公署交通處](../Page/臺灣省行政長官公署.md "wikilink")[鐵路管理委員會](../Page/鐵路管理委員會.md "wikilink")、後改制為[台灣鐵路管理局](../Page/台灣鐵路管理局.md "wikilink")。雖然在改制過程中，受到[國民政府](../Page/國民政府.md "wikilink")[東北鐵路概念分工影響](../Page/中國東北.md "wikilink")，部分組織工作分配與原本的鐵道部不同，但總體來說，總督府鐵道部仍然堪稱台鐵的前身；

### 總督府交通局組織

1939年（昭和14年）時點

  - 鐵道部本部：庶務課、經理課、運輸課、運轉課、工務課、工作課、建設改良課、自動車課、監督課
  - 花蓮港鐵道出張所
  - 台北鐵道工場
  - 高雄鐵道工場：鐵道從業員養成所

### 日治時期鐵道部長

| 任次                                                       | 姓名                                   | 任期                     | 備註       |
| -------------------------------------------------------- | ------------------------------------ | ---------------------- | -------- |
| align ="center" colspan="7" bgcolor="\#EFECD1"|總督府直屬鐵道部  |                                      |                        |          |
| 1                                                        | [後藤新平](../Page/後藤新平.md "wikilink")   | 1899年11月8日－1906年11月13日 | （兼）      |
| 2                                                        | [長谷川謹介](../Page/長谷川謹介.md "wikilink") | 1906年11月14日－1908年12月5日 |          |
| 3                                                        | [大島久滿次](../Page/大島久滿次.md "wikilink") | 1908年12月6日－1910年7月26日  | （兼）      |
| 4                                                        | [宮尾舜治](../Page/宮尾舜治.md "wikilink")   | 1910年7月27日－1910年8月24日  | （兼・事務取扱） |
| 5                                                        | [内田嘉吉](../Page/内田嘉吉.md "wikilink")   | 1910年8月25日－1915年10月20日 | （兼）      |
| 6                                                        | [下村宏](../Page/下村宏.md "wikilink")     | 1915年10月20日－1919年3月10日 | （兼）      |
| 7                                                        | [新元鹿之助](../Page/新元鹿之助.md "wikilink") | 1919年3月10日－1924年12月28日 |          |
| align ="center" colspan="7" bgcolor="\#EFECD1"|總督府交通局鐵道部 |                                      |                        |          |
| 8                                                        | [白勢黎吉](../Page/白勢黎吉.md "wikilink")   | 1925年1月2日－1932年9月13日   | （兼）      |
| 9                                                        | [堀田鼎](../Page/堀田鼎.md "wikilink")     | 1932年3月14日－1936年10月15日 | （兼）      |
| 10                                                       | [泊武治](../Page/泊武治.md "wikilink")     | 1936年10月16日－1938年5月24日 | （兼）      |
| 11                                                       | [渡部慶之進](../Page/渡部慶之進.md "wikilink") | 1938年5月25日－1940年1月24日  |          |
| 12                                                       | [石川定俊](../Page/石川定俊.md "wikilink")   | 1940年1月25日－1942年9月7日   |          |
| 13                                                       | [滿尾君亮](../Page/滿尾君亮.md "wikilink")   | 1942年9月8日－1944年8月16日   |          |
| 14                                                       | [武部英治](../Page/武部英治.md "wikilink")   | 1944年8月16日－1945年10月31日 |          |
|                                                          |                                      |                        |          |

### 掌管路線

  - 鐵道

[縱貫線](../Page/縱貫線_\(鐵路\).md "wikilink")(包含了[劉銘傳鐵路](../Page/劉銘傳鐵路.md "wikilink")\`[基隆臨港線](../Page/基隆臨港線.md "wikilink")\`[高雄臨港線的一部份](../Page/高雄臨港線.md "wikilink"))、[宜蘭線](../Page/宜蘭線.md "wikilink")、[淡水線](../Page/臺鐵淡水線.md "wikilink")\`[亞細亞支線](../Page/亞細亞支線.md "wikilink")\`[三張犁支線](../Page/三張犁支線.md "wikilink")\`[松山飛機場線](../Page/松山飛機場線.md "wikilink")\`[大同興運上埤頭倉庫支線](../Page/大同興運上埤頭倉庫支線.md "wikilink")\`[新北投支線](../Page/新北投支線\(臺鐵\).md "wikilink")\`[大同鐵工場側線](../Page/大同鐵工場側線.md "wikilink")\`[新竹飛機場線](../Page/新竹飛機場線.md "wikilink")\`[海線](../Page/海岸線_\(臺鐵\).md "wikilink")、[臺中線](../Page/臺中線.md "wikilink")(包含了[舊山線](../Page/舊山線.md "wikilink"))、[集集線](../Page/集集線.md "wikilink")、[屏東線](../Page/屏東線.md "wikilink")(包含了[屏東線舊線](../Page/屏東線.md "wikilink"))\`[屏東飛機場線](../Page/屏東飛機場線.md "wikilink")\`、[東港線](../Page/東港線.md "wikilink")、[平溪線](../Page/平溪線.md "wikilink")、[臺東線](../Page/臺東線.md "wikilink")(東拓前)、[竹東線](../Page/内灣線.md "wikilink")、[東勢線與](../Page/東勢線.md "wikilink")[新高築港專用鐵道](../Page/新高築港專用鐵道.md "wikilink")

  - 自動車

<!-- end list -->

  - 北部線：基北線、北新線、北淡線
  - 中部線：豐中線、中員線、員二線、苑王線
  - 南部線：嘉南線、南高線、高屏線
  - 南廻線：屏枋線、南廻本線、恒春線、四重溪線、滿洲線、鵞鑾鼻線

## 原鐵道部廳舍

[臺灣總督府交通局鐵道部廳舍壁龕式窗.jpg](https://zh.wikipedia.org/wiki/File:臺灣總督府交通局鐵道部廳舍壁龕式窗.jpg "fig:臺灣總督府交通局鐵道部廳舍壁龕式窗.jpg")
[臺灣總督府交通局鐵道部戰時指揮中心.jpg](https://zh.wikipedia.org/wiki/File:臺灣總督府交通局鐵道部戰時指揮中心.jpg "fig:臺灣總督府交通局鐵道部戰時指揮中心.jpg")
位於[台北市](../Page/台北市.md "wikilink")[北門附近的台灣鐵路管理局舊廳舍](../Page/台北府城北門.md "wikilink")，日治時期即為鐵道部辦公場所。原公告指定為[三級古蹟](../Page/台灣古蹟列表.md "wikilink")，後改為[國定古蹟](../Page/國定古蹟.md "wikilink")，登錄部分包含主建築廳舍、工務室、電源室、戰時指揮中心（防空洞）、食堂與八角樓（廁所）。

完工於1919年3月的這棟仿[都鐸式](../Page/都鐸式.md "wikilink")（Tudor
architecture）半木構造建築，自落成以來一直都是台灣鐵路的行政中心，直到1990年第五代[台北車站完工之後](../Page/台北車站.md "wikilink")，台鐵局本部的辦公廳舍才從這棟建築搬遷到台北車站五樓現址。

這座因為年久失修而略嫌破舊的鐵道部舊廳舍，除了本身擁有的日治時期鐵道記憶外，內部如大禮堂、[玄關等建物尚有](../Page/玄關.md "wikilink")[清代](../Page/臺灣清治時期.md "wikilink")「台北火車票房」（第一代台北車站）與「[機器局](../Page/機器局.md "wikilink")」留存下來的珍貴遺構，不但形成相當難得的[2D歷史空間標定](../Page/維度.md "wikilink")，知名[鐵道迷](../Page/鐵道迷.md "wikilink")[吳小虹也將舊廳舍形容為](../Page/吳小虹.md "wikilink")「清骨日皮」的建築。

### 古蹟簡介與現況

  - 該建築採寫實式的都鐸式建築：磚砌外牆，入口為弧狀圓拱門洞，衛牆配置[洗石子圓柱天窗](../Page/洗石子.md "wikilink")，另外，二樓屋頂與內部裝潢建構皆以[檜木為主](../Page/檜木.md "wikilink")，也有中央露臺與玄關走廊。
  - 鐵道部舊廳舍所在區域，[清治時期稱為](../Page/台灣清治時期.md "wikilink")「[河溝頭](../Page/河溝頭.md "wikilink")」。[台北城尚未拆除時](../Page/台北城.md "wikilink")，城外還有一條引自[淡水河的狹窄護城河](../Page/淡水河.md "wikilink")。
  - 1990年代初的著名電影《[牯嶺街少年殺人事件](../Page/牯嶺街少年殺人事件.md "wikilink")》主要場景即在此拍攝。
  - 依照[桃園機場捷運及](../Page/桃園機場捷運.md "wikilink")[臺北捷運松山線的興建規劃](../Page/台北捷運松山線.md "wikilink")，舊廳舍區塊西北角的部分遺跡，會進行移地重建。
  - 2005年6月，[臺北市政府文化局將其附近的](../Page/台北市文化局.md "wikilink")「台北工場」遺構（戰後改作為大禮堂使用）一併列入古蹟範圍；並將原來的古蹟名稱「台灣總督府交通局鐵道部」簡化為「台灣總督府鐵道部」。
  - 園區目前由[國立臺灣博物館代管並進行修復工程](../Page/國立臺灣博物館.md "wikilink")，2014年1月9日動工\[1\]，預計於2020年做為「鐵道部博物館」並對外開放。\[2\]\[3\]

## 參考資料

## 外部連結

  -
{{-}}

[category:1899年成立的公司](../Page/category:1899年成立的公司.md "wikilink")
[category:1945年台灣廢除](../Page/category:1945年台灣廢除.md "wikilink")

[Category:國立臺灣博物館](../Category/國立臺灣博物館.md "wikilink")
[\*](../Category/臺灣鐵路管理局.md "wikilink")
[Category:台灣鐵道史](../Category/台灣鐵道史.md "wikilink")
[Category:臺灣總督府](../Category/臺灣總督府.md "wikilink")
[Category:已不存在的日本鐵路公司](../Category/已不存在的日本鐵路公司.md "wikilink")
[Category:台灣日治時期官署建築](../Category/台灣日治時期官署建築.md "wikilink")
[Category:台北車站特定專用區](../Category/台北車站特定專用區.md "wikilink")
[北](../Category/臺灣鐵路文化資產.md "wikilink")

1.
2.
3.