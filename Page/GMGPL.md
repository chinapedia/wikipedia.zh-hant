[Heckert_GNU_white.svg](https://zh.wikipedia.org/wiki/File:Heckert_GNU_white.svg "fig:Heckert_GNU_white.svg")標誌\]\]

**GNAT Modified General Public License**（簡稱：**Modified
GPL**、**GMGPL**、**MGPL**）是一種軟體授權方式，它是原有[GNU通用公共许可证](../Page/GNU通用公共许可证.md "wikilink")（簡稱：[GPL](../Page/GPL.md "wikilink")）授權方式的一種特別修訂版，是針對[Ada程式語言的類屬特性而修訂](../Page/Ada.md "wikilink")，其修訂的內容主要如下：

  -
    *As a special exception, if other files instantiate generics from
    this unit, or you link this unit with other files to produce an
    executable, this unit does not by itself cause the resulting
    executable to be covered by the GNU General Public License. This
    exception does not however invalidate any other reasons why the
    executable file might be covered by the GNU Public License.*

大意是：

  -
    *以一個特殊例外條款來看待，如果其他檔案的形成須引用這個單元，或其他檔案在編譯過程中將這個單元加入連結，進而產生可執行的程式檔，這個單元無法要求自身之外的引用者、連結者也採行GPL授權。這個例外不能以任何理由而無效。*

使用[編譯程式定向](../Page/編譯程式定向.md "wikilink")（[compiler
directive](../Page/編譯程式定向.md "wikilink")）的控制碼：「`pragma License
(Modified_GPL);`」就能夠使[GNAT編譯器以Modified](../Page/GNAT.md "wikilink")
GPL的授權方式進行查核確認。

## 參見

  -
  - [GNU自由文档许可证](../Page/GFDL.md "wikilink")（GFDL）

  - [GNU宽通用公共许可证](../Page/GNU_Lesser_General_Public_License.md "wikilink")（LGPL）

  - [OpenSSL exception](../Page/OpenSSL.md "wikilink")

  -
[GNU專案](../Category/GNU.md "wikilink")
[Category:自由軟體授權](../Category/自由軟體授權.md "wikilink")
[Category:Ada程式語言](../Category/Ada程式語言.md "wikilink")