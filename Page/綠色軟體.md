[Lingoes.png](https://zh.wikipedia.org/wiki/File:Lingoes.png "fig:Lingoes.png")\]\]
[IrfanView.png](https://zh.wikipedia.org/wiki/File:IrfanView.png "fig:IrfanView.png")\]\]
**可攜式軟體**（、）或稱**-{zh-cn:便携软件;
zh-tw:綠色軟體;}-**，指一類小型[軟體](../Page/軟體.md "wikilink")，多數為[免費軟體](../Page/免費軟體.md "wikilink")，最大特點是軟體無需安裝便可使用，可存放於可移除式儲存媒體中（因此稱為-{zh-cn:便携软体;
zh-tw:可攜式軟體;}-），移除後也不會將任何紀錄（[登錄檔訊息等](../Page/注册表.md "wikilink")）留在本機電腦上。

## 特性

綠色軟體的優點是檔案比較細小、不用安裝、刪除方便和只佔用少量[系統資源](../Page/系統資源.md "wikilink")，故此大部分都可以存放在可移除式儲存媒體（如隨身碟等卸載式儲存裝置）中讀取。部份綠色軟體更以[開放原始碼的形式發佈](../Page/開放原始碼.md "wikilink")，歡迎任何人士參與改進或增加功能，並以相關的[自由軟體授權](../Page/自由軟體.md "wikilink")(如:[GPL](../Page/GPL.md "wikilink"))容許在網路上自由修改、發佈。

## 定義

起初人們把「綠色軟件」定義為「不會在使用者的電腦上留下難以清除的冗余資訊的軟體」，特別是在中国大量被[恶意软件荼毒的人们](../Page/恶意软件.md "wikilink")。原因是一如愛好環保般不棄置污染物，所以冠上「綠色」之名。後來綠色軟體的定義變得寬廣而模糊，不同性質的小容量軟體如：[捐贈軟體](../Page/捐贈軟體.md "wikilink")、[開放原始碼軟體](../Page/開放原始碼.md "wikilink")、[免費軟體等也被納入綠色軟體的意義之內](../Page/免費軟體.md "wikilink")。亦有對綠色軟體的定義為所有不需安裝的免費軟體。

現時，綠色軟件的定義和[可攜式軟件](../Page/可攜式軟件.md "wikilink")（Portableware）十分接近，一般指可以連同設定資料一併置於可移除式儲存媒體內轉移至不同電腦使用或可在[網絡硬碟上運行的應用軟體](../Page/網絡硬碟.md "wikilink")。由於設定檔（如ini檔或xml檔）是置於程式資料夾內而非寫入登錄或其他位置，因此轉移到不同電腦後仍可正常執行；移除儲存裝置後紀錄也不會將資料留在本機電腦上。這種設計架構的軟體可存取即用，使很多人都把程式「帶著走」，以便自己在不同地方使用相同的設定，尤其是具備人性化設定的瀏覽器軟件，這使得可攜式軟體得到普及。

基本上，可攜式軟體與綠色軟體都有同樣的特性:

1.  不需要[安裝](../Page/安裝程式.md "wikilink")
2.  不需要建立或更改任何在該程式所在資料夾以外的檔案

## 綠化軟體

**綠化軟體**或**綠色化軟體**是指軟體經過一些修改從而使符合綠色軟體的要求，如將該軟體修改成免安裝。綠化軟體一般是從免費軟體，特別是[自由軟體中進行開發的](../Page/自由軟體.md "wikilink")，但亦有違法的開發者從[共享軟體或](../Page/共享軟體.md "wikilink")[收費軟體中](../Page/收費軟體.md "wikilink")，在未經著作者同意下進行侵權，將軟體修改成綠色軟體，並免費發佈到網絡上。

## 著名綠色／可攜式軟體列表

{{ 未完成列表 }}

  - *按數字順序排列*

<table>
<thead>
<tr class="header">
<th><p>數字開頭</p></th>
<th><p>軟體名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/7.md" title="wikilink">7</a></p></td>
<td><ul>
<li><a href="../Page/7-Zip.md" title="wikilink">7-Zip Portable</a> - <a href="../Page/7-Zip.md" title="wikilink">7-Zip檔案壓縮程式的綠色化版本</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/8.md" title="wikilink">8</a></p></td>
<td><ul>
<li><a href="../Page/8start_Launcher.md" title="wikilink">8start Launcher</a> - 美化桌面軟體</li>
</ul></td>
</tr>
</tbody>
</table>

  - *按英文字母順序排列*

<table>
<thead>
<tr class="header">
<th><p>字母開頭</p></th>
<th><p>軟體名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>A</p></td>
<td><ul>
<li><a href="../Page/AbiWord.md" title="wikilink">AbiWord Portable</a> - AbiWord文字處理器軟體的綠色化版本</li>
<li><a href="../Page/AirPlay.md" title="wikilink">AirPlay</a> - <a href="../Page/音頻.md" title="wikilink">音頻</a><a href="../Page/媒體播放器.md" title="wikilink">播放器</a></li>
<li><a href="../Page/airWRX.md" title="wikilink">airWRX</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/ASuite.md" title="wikilink">ASuite</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/Audacity.md" title="wikilink">Audacity Portable</a> - Audacity聲音編輯軟體的綠色化版本</li>
</ul></td>
</tr>
<tr class="even">
<td><p>C</p></td>
<td><ul>
<li><a href="../Page/ClamWin.md" title="wikilink">ClamWin Portable</a> - Clam AntiVirus開放原始碼的防毒軟體的綠色化版本</li>
<li><a href="../Page/Comicsviewer.md" title="wikilink">Comicsviewer</a> - <a href="../Page/漫畫.md" title="wikilink">漫畫瀏覽工具</a></li>
<li><a href="../Page/ConvertZ.md" title="wikilink">ConvertZ</a> - 中文內碼轉換工具，可進行繁簡互換</li>
<li><a href="../Page/CPU-Z.md" title="wikilink">CPU-Z</a> - 著名的處理器偵測程序，可以快捷的檢測出處理器的各項參數</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>F</p></td>
<td><ul>
<li><a href="../Page/FastCopy.md" title="wikilink">FastCopy</a> - 開源最速移檔，<a href="../Page/開源軟體.md" title="wikilink">開源軟體中號稱最快速的移檔軟體</a></li>
<li><a href="../Page/FileZilla.md" title="wikilink">FileZilla</a> - FTP 傳輸工具，類似QuiteFTP 的使用介面</li>
<li><a href="../Page/Folding@home.md" title="wikilink">Folding@home Console version</a> - <strong><a href="../Page/史丹佛大學.md" title="wikilink">史丹佛大學</a></strong>分散式運算計畫的<a href="../Page/命令行界面.md" title="wikilink">命令行界面版本</a>，具<strong>免安裝</strong>的特性，可借由<span style="text-decoration: underline;">全世界個人電腦</span>的少量資源，找出可製成<a href="../Page/Folding@home#意義.md" title="wikilink"><strong>癌症解藥</strong>的物質</a>。</li>
<li><a href="../Page/foobar2000.md" title="wikilink">foobar2000</a> - 能高度自訂的開源<a href="../Page/音訊.md" title="wikilink">音訊播放器</a></li>
<li><a href="../Page/Foxit_Reader.md" title="wikilink">Foxit PDF Reader</a> - <a href="../Page/PDF.md" title="wikilink">PDF的閱讀工具</a></li>
<li><a href="../Page/FreeCommander.md" title="wikilink">FreeCommander Portable</a> - 檔案總管加強版</li>
<li><a href="../Page/Freegate.md" title="wikilink">Freegate</a> - 自由門，翻牆軟件</li>
</ul></td>
</tr>
<tr class="even">
<td><p>G</p></td>
<td><ul>
<li><a href="../Page/GreenBrowser.md" title="wikilink">GreenBrowser</a> - <a href="../Page/瀏覽器.md" title="wikilink">網頁瀏覽工具</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>H</p></td>
<td><ul>
<li><a href="../Page/HD_Tune.md" title="wikilink">HD Tune</a> - 硬碟測試專家</li>
<li><a href="../Page/HijackThis.md" title="wikilink">HijackThis</a> - <a href="../Page/趨勢科技.md" title="wikilink">趨勢科技的</a><a href="../Page/間諜軟體.md" title="wikilink">間諜軟體移除工具</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>I</p></td>
<td><ul>
<li><a href="../Page/InfraRecorder.md" title="wikilink">InfraRecorder Portable</a> - 開源光碟燒錄工具</li>
<li><a href="../Page/IrfanView.md" title="wikilink">IrfanView</a> - 圖檔管理工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>J</p></td>
<td><ul>
<li><a href="../Page/JkDefrag.md" title="wikilink">JkDefrag Portable</a> - 開源磁碟重組軟體</li>
<li><a href="../Page/Java.md" title="wikilink">Portable Java IDE</a> - Java開發環境隨身帶</li>
</ul></td>
</tr>
<tr class="even">
<td><p>K</p></td>
<td><ul>
<li><a href="../Page/KMPlayer.md" title="wikilink">KMPlayer</a> - 媒體播放工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>L</p></td>
<td><ul>
<li><a href="../Page/Lianliankan.md" title="wikilink">Lianliankan</a> - <a href="../Page/連連看.md" title="wikilink">連連看的綠色化版本</a></li>
<li><a href="../Page/靈格斯.md" title="wikilink">Lingoes 靈格斯</a> - 桌面<a href="../Page/詞典.md" title="wikilink">詞典</a><a href="../Page/翻譯.md" title="wikilink">翻譯軟體</a></li>
<li><a href="../Page/賴.md" title="wikilink">Line</a> - <a href="../Page/即時通訊.md" title="wikilink">即時通訊工具</a></li>
<li><a href="../Page/薄荷操作系統.md" title="wikilink">Linux Mint</a> - <a href="../Page/操作系統.md" title="wikilink">操作系統工具</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>M</p></td>
<td><ul>
<li><a href="../Page/Mcool.md" title="wikilink">Mcool</a> - 极简音乐播放器</li>
<li><a href="../Page/Media_Player_Classic.md" title="wikilink">Media Player Classic</a> - 開源媒體播放工具</li>
<li><a href="../Page/Mikogo_3.0.md" title="wikilink">Mikogo 3.0</a> - <a href="../Page/可攜式遠端桌面控制.md" title="wikilink">可攜式遠端桌面控制工具</a></li>
<li><a href="../Page/Miranda_IM.md" title="wikilink">Miranda IM</a> - <a href="../Page/即時通訊.md" title="wikilink">即時通訊工具</a></li>
<li><a href="../Page/Mozilla_Firefox.md" title="wikilink">Mozilla Firefox</a> Portable Edition - Mozilla Firefox瀏覽器的綠色化版本</li>
<li><a href="../Page/Mozilla_Sunbird.md" title="wikilink">Mozilla Sunbird</a> Portable Edition - Mozilla Sunbird行事歷應用程式的綠色化版本</li>
<li><a href="../Page/MPlayer.md" title="wikilink">MPlayer</a> - 開源媒體播放工具</li>
<li><a href="../Page/μTorrent.md" title="wikilink">μTorrent</a> - <a href="../Page/BT.md" title="wikilink">BT下載工具</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>N</p></td>
<td><ul>
<li><a href="../Page/Notepad++.md" title="wikilink">Notepad++ Portable</a> - Notepad++純文字編輯器的綠色化版本</li>
<li><a href="../Page/Nvu.md" title="wikilink">Nvu Portable</a> - Nvu網頁編輯器的綠色化版本</li>
</ul></td>
</tr>
<tr class="even">
<td><p>O</p></td>
<td><ul>
<li><a href="../Page/Omziff.md" title="wikilink">Omziff</a> - 加密或銷毀檔工具</li>
<li><a href="../Page/OpenOffice.org.md" title="wikilink">OpenOffice.org Portable</a> - OpenOffice.org開放原始碼的辦公室軟體的綠色化版本</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>P</p></td>
<td><ul>
<li><a href="../Page/Pidgin.md" title="wikilink">Pidgin Portable</a> - Pidgin即時通訊軟體的綠色化版本(原<a href="../Page/Gaim.md" title="wikilink">Gaim</a>)</li>
<li><a href="../Page/PortableApps.com.md" title="wikilink">PortableApps.com Platform</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/Portable_Firefox.md" title="wikilink">Portable Firefox</a> - <a href="../Page/Mozilla_Firefox.md" title="wikilink">Mozilla Firefox網頁瀏覽工具的綠色化版本</a></li>
<li><a href="../Page/Portable_Start_Menu.md" title="wikilink">Portable Start Menu</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/Portable_Thunderbird.md" title="wikilink">Portable Thunderbird</a> - <a href="../Page/Mozilla_Thunderbird.md" title="wikilink">Mozilla Thunderbird郵件工具的綠色化版本</a></li>
<li><a href="../Page/PStart.md" title="wikilink">PStart</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/PuTTY.md" title="wikilink">PuTTY</a> - PuTTYTelnet/SSH/rlogin/純TCP以及串列阜連線軟體</li>
</ul></td>
</tr>
<tr class="even">
<td><p>R</p></td>
<td><ul>
<li><a href="../Page/RegDefrag.md" title="wikilink">Quicksys RegDefrag</a> - 登錄檔重組程式</li>
<li><a href="../Page/ReSysInfo_Information_Viewer.md" title="wikilink">ReSysInfo Information Viewer</a> - 電腦系統資訊檢索工具</li>
<li><a href="../Page/Revo_Uninstaller.md" title="wikilink">Revo Uninstaller</a> - 移除獵人，具方便的獵人模式（指檔移除功能）</li>
<li><a href="../Page/RoboForm.md" title="wikilink">RoboForm2Go RF</a> - 自動填表工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>S</p></td>
<td><ul>
<li><a href="../Page/SilentNight_Micro_Burner.md" title="wikilink">SilentNight Micro Burner</a> - <a href="../Page/光碟燒錄.md" title="wikilink">光碟燒錄工具</a></li>
<li><a href="../Page/Sleipnir.md" title="wikilink">Sleipnir</a> - 網頁瀏覽工具（壓縮檔版本可直接解壓縮至特定資料夾）</li>
<li><a href="../Page/Snippy.md" title="wikilink">Snippy</a> - 截圖工具</li>
<li><a href="../Page/Subpad.md" title="wikilink">Subpad</a> - 記事本工具</li>
<li><a href="../Page/Sumatra_PDF.md" title="wikilink">Sumatra PDF Portable</a>- Sumatra PDF 是PDF檢視器的綠色化版本</li>
<li><a href="../Page/SuperCopier.md" title="wikilink">Portable SuperCopier</a> - 開源移檔強化，最易上手、功能強大的移檔軟體</li>
<li><a href="../Page/SyMenu.md" title="wikilink">SyMenu</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/System_Information_Viewer.md" title="wikilink">System Information Viewer</a> - 硬件規格檢測工具</li>
</ul></td>
</tr>
<tr class="even">
<td><p>T</p></td>
<td><ul>
<li><a href="../Page/TheWorld_Browser.md" title="wikilink">TheWorld Browser</a><a href="../Page/网页浏览.md" title="wikilink">网页浏览世界之窗浏览器</a></li>
<li><a href="../Page/Tor.md" title="wikilink">Portable Tor</a> - <a href="../Page/Tor.md" title="wikilink">Tor</a><a href="../Page/洋蔥路由器.md" title="wikilink">洋蔥路由器</a></li>
<li><a href="../Page/Trillian.md" title="wikilink">Trillian Anywhere</a> - <a href="../Page/Trillian.md" title="wikilink">Trillian</a><a href="../Page/即時通訊.md" title="wikilink">即時通訊軟體的綠色化版本</a></li>
<li><a href="../Page/TrueCrypt.md" title="wikilink">TrueCrypt</a> - 跨平台開源磁區加密軟體</li>
<li><a href="../Page/TTPlayer.md" title="wikilink">TTPlayer Portable</a> - 音樂播放器（<a href="../Page/千千靜聽.md" title="wikilink">千千靜聽</a>）</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>U</p></td>
<td><ul>
<li><a href="../Page/U3.md" title="wikilink">U3</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/Ultrasurf.md" title="wikilink">Ultrasurf</a> - 無界瀏覽，翻牆軟件</li>
<li><a href="../Page/UPP.md" title="wikilink">UPP</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>V</p></td>
<td><ul>
<li><a href="../Page/VirtualDub.md" title="wikilink">VirtualDub</a> - VirtualDub多媒體剪輯及線性處理軟件本</li>
<li><a href="../Page/VirtuaWin.md" title="wikilink">VirtuaWin Portable</a> - 開源虛擬桌面，產生多工作區的好幫手</li>
<li><a href="../Page/VLC_Media_Player.md" title="wikilink">VLC Media Player Portable</a> - VLC Media Player開源多媒體播放器的綠色化版本</li>
<li><a href="../Page/Vuze_(軟體).md" title="wikilink">Portable Vuze</a> - <a href="../Page/Azureus.md" title="wikilink">Azureus</a><a href="../Page/籃箭樹蛙.md" title="wikilink">籃箭樹蛙</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>W</p></td>
<td><ul>
<li><a href="../Page/WinPenPack.md" title="wikilink">WinPenPack</a> - <a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></li>
<li><a href="../Page/WinSCP.md" title="wikilink">WinSCP Portable</a> - WinSCPSFTP客戶端軟件的綠色化版本</li>
</ul></td>
</tr>
<tr class="even">
<td><p>X</p></td>
<td><ul>
<li><a href="../Page/XAMPP.md" title="wikilink">XAMPP Portable</a>- XAMPP網頁伺服器軟件的綠色化版本</li>
<li><a href="../Page/XMPlay.md" title="wikilink">XMPlay</a> - 媒體播放工具</li>
<li><a href="../Page/XnView.md" title="wikilink">XnView</a> - 圖檔管理工具（壓縮檔版本可直接解壓縮至特定資料夾）</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>Z</p></td>
<td><ul>
<li><a href="../Page/Zip2Dir.md" title="wikilink">Zip2Dir</a> - 壓縮與解壓工具</li>
</ul></td>
</tr>
</tbody>
</table>

  - *按分類排列*

<table>
<thead>
<tr class="header">
<th><p>軟體分類</p></th>
<th><p>軟體名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>系統工具</p></td>
<td><ul>
<li><a href="../Page/8start_Launcher.md" title="wikilink">8start Launcher</a> - 美化桌面軟體</li>
<li><a href="../Page/FreeCommander.md" title="wikilink">FreeCommander Portable</a> - 檔案總管加強版</li>
<li><a href="../Page/HD_Tune.md" title="wikilink">HD Tune</a> - 硬碟測試專家</li>
<li><a href="../Page/FastCopy.md" title="wikilink">FastCopy</a> - 開源最速移檔，<a href="../Page/開源軟體.md" title="wikilink">開源軟體中號稱最快速的移檔軟體</a></li>
<li><a href="../Page/SuperCopier.md" title="wikilink">Portable SuperCopier</a> - 開源移檔強化，最易上手、功能強大的移檔軟體</li>
<li><a href="../Page/ReSysInfo_Information_Viewer.md" title="wikilink">ReSysInfo Information Viewer</a> - 電腦系統資訊檢索工具</li>
<li><a href="../Page/Revo_Uninstaller.md" title="wikilink">Revo Uninstaller</a> - 移除獵人，具方便的獵人模式（指檔移除功能）</li>
<li><a href="../Page/System_Information_Viewer.md" title="wikilink">System Information Viewer</a> - 硬體規格檢測工具</li>
<li><a href="../Page/VirtuaWin.md" title="wikilink">VirtuaWin Portable</a> - 開源虛擬桌面，產生多工作區的好幫手</li>
<li><a href="../Page/ntpclock.md" title="wikilink">ntpclock</a> - 網路校時程式</li>
<li><a href="../Page/Codestuff_Starter.md" title="wikilink">Codestuff Starter</a> - startup manager for Windows</li>
<li><a href="../Page/EVEREST_Home_Edition.md" title="wikilink">EVEREST Home Edition</a> - 讓你對電腦內部所有硬體規格一目了然</li>
</ul></td>
</tr>
<tr class="even">
<td><p>檔案管理</p></td>
<td><ul>
<li><a href="../Page/DoubleKiller.md" title="wikilink">DoubleKiller</a> - 清理重覆檔案、還你硬碟空間</li>
<li><a href="../Page/Everything.md" title="wikilink">Everything</a> - 最快的檔名搜尋工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>磁碟重組</p></td>
<td><ul>
<li><a href="../Page/JkDefrag.md" title="wikilink">JkDefrag Portable</a> - 開源磁碟重組軟體</li>
<li><a href="../Page/RegDefrag.md" title="wikilink">Quicksys RegDefrag</a> - 登錄檔重組程式</li>
<li><a href="../Page/Power_Defrag_2005.md" title="wikilink">Power Defrag 2005</a> - 免費又快速的磁碟重組軟體</li>
</ul></td>
</tr>
<tr class="even">
<td><p>程式語言</p></td>
<td><ul>
<li><a href="../Page/Java.md" title="wikilink">Portable Java IDE</a> - Java開發環境隨身帶</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>文書工具</p></td>
<td><ul>
<li><a href="../Page/AbiWord.md" title="wikilink">AbiWord Portable</a> - AbiWord文字處理器軟體的綠色化版本</li>
<li><a href="../Page/ConvertZ.md" title="wikilink">ConvertZ</a> - 中文內碼轉換工具，可進行繁簡互換</li>
<li><a href="../Page/Foxit_Reader.md" title="wikilink">Foxit PDF Reader</a> - <a href="../Page/PDF.md" title="wikilink">PDF的閱讀工具</a></li>
<li><a href="../Page/靈格斯.md" title="wikilink">Lingoes 靈格斯</a> - 桌面<a href="../Page/詞典.md" title="wikilink">詞典</a><a href="../Page/翻譯.md" title="wikilink">翻譯軟體</a></li>
<li><a href="../Page/Notepad++.md" title="wikilink">Notepad++ Portable</a> - Notepad++純文字編輯器的綠色化版本</li>
<li><a href="../Page/OpenOffice.org.md" title="wikilink">OpenOffice.org Portable</a> - OpenOffice.org開放原始碼的辦公室軟體的綠色化版本</li>
<li><a href="../Page/Subpad.md" title="wikilink">Subpad</a> - 記事本工具</li>
<li><a href="../Page/Sumatra_PDF.md" title="wikilink">Sumatra PDF Portable</a>- Sumatra PDF 是PDF檢視器的綠色化版本</li>
<li><a href="../Page/GreenPad.md" title="wikilink">GreenPad</a> - Simple Text editor with Syntax Highlighting, RegExp Search, Unicode Support etc...</li>
</ul></td>
</tr>
<tr class="even">
<td><p>影音播放 ～～可參閱<a href="../Page/媒體播放器比較.md" title="wikilink">媒體播放器比較</a>，開源多有<a href="../Page/Portable.md" title="wikilink">Portable</a>（<a href="../Page/可攜式.md" title="wikilink">可攜式</a>）版本。</p></td>
<td><ul>
<li><a href="../Page/AirPlay.md" title="wikilink">AirPlay</a> - <a href="../Page/音頻.md" title="wikilink">音頻</a><a href="../Page/媒體播放器.md" title="wikilink">播放器</a></li>
<li><a href="../Page/foobar2000.md" title="wikilink">foobar2000</a> - 能高度自訂的開源<a href="../Page/音訊.md" title="wikilink">音訊播放器</a></li>
<li><a href="../Page/KMPlayer.md" title="wikilink">KMPlayer</a> - 媒體播放工具</li>
<li><a href="../Page/Mcool.md" title="wikilink">Mcool</a> - 极简音乐播放器</li>
<li><a href="../Page/Media_Player_Classic.md" title="wikilink">Media Player Classic</a> - 開源媒體播放工具</li>
<li><a href="../Page/Media_Player_Classic_Home_Cinema.md" title="wikilink">Media Player Classic Home Cinema</a> - 開源媒體播放工具</li>
<li><a href="../Page/MPlayer.md" title="wikilink">MPlayer</a> - 開源媒體播放工具</li>
<li><a href="../Page/TTPlayer.md" title="wikilink">TTPlayer Portable</a> - 音樂播放器（<a href="../Page/千千靜聽.md" title="wikilink">千千靜聽</a>）</li>
<li><a href="../Page/VLC_Media_Player.md" title="wikilink">VLC Media Player Portable</a> - VLC Media Player開源多媒體播放器的綠色化版本</li>
<li><a href="../Page/XMPlay.md" title="wikilink">XMPlay</a> - 媒體播放工具</li>
<li><a href="../Page/ExtraPlayer.md" title="wikilink">ExtraPlayer</a> - 集合影音管理、影音播放、影音轉檔、影音錄音、影音合併、影音剪輯（分割與裁剪）功能功能與操作簡便的影音軟體</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>影音編輯</p></td>
<td><ul>
<li><a href="../Page/Audacity.md" title="wikilink">Audacity Portable</a> - Audacity聲音編輯軟體的綠色化版本</li>
<li><a href="../Page/VirtualDub.md" title="wikilink">VirtualDub Portable</a> - VirtualDub多媒體剪輯及線性處理軟件的綠色化版本</li>
<li><a href="../Page/Messer.md" title="wikilink">Messer</a> - 提供 MP3 格式的預定錄音功能</li>
<li><a href="../Page/MediaEncoder2006.md" title="wikilink">MediaEncoder2006</a> - 音樂轉檔精靈，支援29種音樂格式的中文免費轉檔軟體</li>
<li><a href="../Page/MediaInfo.md" title="wikilink">MediaInfo</a> -用來分析視頻和音頻文件的編碼和內容信息</li>
</ul></td>
</tr>
<tr class="even">
<td><p>防毒防駭</p></td>
<td><ul>
<li><a href="../Page/ClamWin.md" title="wikilink">ClamWin Portable</a> - Clam AntiVirus開放原始碼的防毒軟體的綠色化版本</li>
<li><a href="../Page/HijackThis.md" title="wikilink">HijackThis</a> - <a href="../Page/趨勢科技.md" title="wikilink">趨勢科技的</a><a href="../Page/間諜軟體.md" title="wikilink">間諜軟體移除工具</a></li>
<li><a href="../Page/Omziff.md" title="wikilink">Omziff</a> - 加密或銷毀檔工具</li>
<li><a href="../Page/RoboForm.md" title="wikilink">RoboForm2Go RF</a> - 自動填表工具</li>
<li><a href="../Page/TrueCrypt.md" title="wikilink">TrueCrypt Portable</a> - 跨平台開源磁區加密軟體</li>
<li><a href="../Page/小鬼當差.md" title="wikilink">小鬼當差</a> - 輔助性輕巧<a href="../Page/防火牆.md" title="wikilink">防火牆</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>圖檔瀏覽與管理</p></td>
<td><ul>
<li><a href="../Page/Comicsviewer.md" title="wikilink">Comicsviewer</a> - 漫畫瀏覽工具</li>
<li><a href="../Page/IrfanView.md" title="wikilink">IrfanView</a> - 圖檔瀏覽與管理工具</li>
<li><a href="../Page/XnView.md" title="wikilink">XnView</a> - 圖檔瀏覽與管理工具（壓縮檔版本可直接解壓縮至特定資料夾）</li>
<li><a href="../Page/Vallen_JPegger.md" title="wikilink">Vallen JPegger</a> - 圖檔瀏覽</li>
</ul></td>
</tr>
<tr class="even">
<td><p>截圖工具</p></td>
<td><ul>
<li><a href="../Page/Snippy.md" title="wikilink">Snippy</a> - 截圖工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>圖片編輯</p></td>
<td><ul>
<li><a href="../Page/VirtualDub.md" title="wikilink">VirtualDub</a> - VirtualDub視頻捕捉及線性處理軟體</li>
<li><a href="../Page/GIMP_Portable.md" title="wikilink">GIMP_Portable</a> - Photo and Image Editor</li>
</ul></td>
</tr>
<tr class="even">
<td><p>壓縮工具</p></td>
<td><ul>
<li><a href="../Page/7-Zip.md" title="wikilink">7-Zip</a> - <a href="../Page/7-Zip.md" title="wikilink">7-Zip开源的檔案壓縮程式</a></li>
<li><a href="../Page/Zip2Dir.md" title="wikilink">Zip2Dir</a> - 壓縮與解壓工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瀏覽器.md" title="wikilink">瀏覽器</a></p></td>
<td><ul>
<li><a href="../Page/GreenBrowser.md" title="wikilink">GreenBrowser</a> - 網頁瀏覽工具</li>
<li><a href="../Page/Mozilla_Firefox.md" title="wikilink">Mozilla Firefox</a> Portable Edition - Mozilla Firefox瀏覽器的綠色化版本</li>
<li><a href="../Page/Portable_Firefox.md" title="wikilink">Portable Firefox</a> - Mozilla Firefox網頁瀏覽工具的綠色化版本</li>
<li><a href="../Page/Sleipnir.md" title="wikilink">Sleipnir</a> - 網頁瀏覽工具（壓縮檔版本可直接解壓縮至特定資料夾）</li>
<li><a href="../Page/GreatNews.md" title="wikilink">GreatNews</a> - RSS 瀏覽工具</li>
</ul></td>
</tr>
<tr class="even">
<td><p>網頁編輯</p></td>
<td><ul>
<li><a href="../Page/Nvu.md" title="wikilink">Nvu Portable</a> - Nvu網頁編輯器的綠色化版本</li>
<li><a href="../Page/XAMPP.md" title="wikilink">XAMPP Portable</a>- XAMPP網頁伺服器軟件的綠色化版本</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>續傳工具</p></td>
<td><ul>
<li><a href="../Page/FileZilla.md" title="wikilink">FileZilla</a> - <a href="../Page/FTP.md" title="wikilink">FTP</a> 傳輸工具，類似QuiteFTP 的使用介面</li>
<li><a href="../Page/Vuze_(軟體).md" title="wikilink">Portable Vuze</a> - <a href="../Page/Azureus.md" title="wikilink">Azureus</a><a href="../Page/籃箭樹蛙.md" title="wikilink">籃箭樹蛙</a></li>
<li><a href="../Page/PuTTY.md" title="wikilink">PuTTY</a> - PuTTYTelnet/SSH/rlogin/純TCP以及串列阜連線軟體</li>
<li><a href="../Page/μTorrent.md" title="wikilink">μTorrent</a> - BT下載工具</li>
<li><a href="../Page/BitComet.md" title="wikilink">BitComet</a> - BT下載工具</li>
<li><a href="../Page/D.S.Lite.md" title="wikilink">D.S.Lite</a> - 功能比FlashGet更強大的免費全中文多點續傳軟體</li>
<li><a href="../Page/Free_Download_Manager.md" title="wikilink">Free Download Manager</a> - <a href="../Page/FTP.md" title="wikilink">FTP</a> 多功能傳輸工具</li>
</ul></td>
</tr>
<tr class="even">
<td><p>即時通訊</p></td>
<td><ul>
<li><a href="../Page/Miranda_IM.md" title="wikilink">Miranda IM</a> - <a href="../Page/即時通訊.md" title="wikilink">即時通訊工具</a></li>
<li><a href="../Page/Pidgin.md" title="wikilink">Pidgin Portable</a> - Pidgin即時通訊軟體的綠色化版本(原<a href="../Page/Gaim.md" title="wikilink">Gaim</a>)</li>
<li><a href="../Page/Trillian.md" title="wikilink">Trillian Anywhere</a> - Trillian即時通訊軟體的綠色化版</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>遠端桌面控制</p></td>
<td><ul>
<li><a href="../Page/Mikogo_3.0.md" title="wikilink">Mikogo 3.0</a> - <a href="../Page/可攜式遠端桌面控制.md" title="wikilink">可攜式遠端桌面控制工具</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>行事曆</p></td>
<td><ul>
<li><a href="../Page/Mozilla_Sunbird.md" title="wikilink">Mozilla Sunbird</a> Portable Edition - Mozilla Sunbird行事歷應用程式的綠色化版本</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>郵件工具</p></td>
<td><ul>
<li><a href="../Page/Portable_Thunderbird.md" title="wikilink">Portable Thunderbird</a> - Mozilla Thunderbird郵件工具的綠色化版本</li>
<li><a href="../Page/DreamMail.md" title="wikilink">DreamMail</a> - 免費且支援多語系與 Unicode 的POP3及web電子郵件收發軟體</li>
<li><a href="../Page/Becky!.md" title="wikilink">Becky!</a> - 支援POP3多信箱電子郵件收發軟體</li>
<li><a href="../Page/Vallen_e-Mailer.md" title="wikilink">Vallen e-Mailer</a> - a freeware tool for sending the same e-mail to a list of recipients. You may attach any file(s) to the e-mail you want to send.</li>
</ul></td>
</tr>
<tr class="even">
<td><p>燒錄工具</p></td>
<td><ul>
<li><a href="../Page/InfraRecorder.md" title="wikilink">InfraRecorder Portable</a> - 開源光碟燒錄工具</li>
<li><a href="../Page/SilentNight_Micro_Burner.md" title="wikilink">SilentNight Micro Burner</a> - 光碟燒錄工具</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>遊戲</p></td>
<td><ul>
<li><a href="../Page/Lianliankan.md" title="wikilink">Lianliankan</a> - <a href="../Page/連連看.md" title="wikilink">連連看的綠色化版本</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/隨身碟選單软件.md" title="wikilink">隨身碟選單软件</a></p></td>
<td><ul>
<li><a href="../Page/airWRX.md" title="wikilink">airWRX</a></li>
<li><a href="../Page/ASuite.md" title="wikilink">ASuite</a></li>
<li><a href="../Page/Portable_Start_Menu.md" title="wikilink">Portable Start Menu</a></li>
<li><a href="../Page/PortableApps.com.md" title="wikilink">PortableApps.com Platform</a></li>
<li><a href="../Page/PStart.md" title="wikilink">PStart</a></li>
<li><a href="../Page/SyMenu.md" title="wikilink">SyMenu</a></li>
<li><a href="../Page/U3.md" title="wikilink">U3</a></li>
<li><a href="../Page/UPP.md" title="wikilink">UPP</a></li>
<li><a href="../Page/WinPenPack.md" title="wikilink">WinPenPack</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>其它</p></td>
<td><ul>
<li><a href="../Page/Folding@home.md" title="wikilink">Folding@home Console version</a> - <strong><a href="../Page/史丹佛大學.md" title="wikilink">史丹佛大學</a></strong>分散式運算計畫的<a href="../Page/命令行界面.md" title="wikilink">命令行界面版本</a>，具<strong>免安裝</strong>的特性，可借由<span style="text-decoration: underline;">全世界個人電腦</span>的少量資源，找出可製成<a href="../Page/Folding@home#意義.md" title="wikilink"><strong>癌症解藥</strong>的物質</a>。</li>
<li><a href="../Page/Tor.md" title="wikilink">Portable Tor</a> - <a href="../Page/Tor.md" title="wikilink">Tor</a><a href="../Page/路由器.md" title="wikilink">洋蔥路由器</a></li>
<li><a href="../Page/WinSCP_Portable.md" title="wikilink">WinSCP Portable</a> - WinSCPSFTP客戶端軟體的綠色化版本</li>
<li><a href="../Page/SPFDisk.md" title="wikilink">SPFDisk</a> - 綜合『硬碟分割工具(FDISK)』及『啟動管理程式(Boot Manager)』的軟體</li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references />

  - 更勝Google Pack OpenCD + TTCS OSSWIN CD
    單碟收錄過百免費軟件，2006-06-27，《[電腦廣場](../Page/電腦廣場.md "wikilink")》
  - 簡化系統升級程序數據跨平台轉移提案，《[e-zone](../Page/e-zone.md "wikilink")》，2006-11-10
  - 打造最強救機碟Super
    Disc@Vista制作超攻略，《[e-zone](../Page/e-zone.md "wikilink")》，2007-05-31
  - 網路科技「綠色軟件」商業模式「燎原」中國共享軟件\[J\].計算機與網

## 外部連結

### 繁體中文網站

  - [Portable 軟體的中文官網](http://www.cosa.org.tw/portable/) -
    具有最適合『[PortableApps](../Page/PortableApps.md "wikilink")』的可攜式軟體。
  - [Portable 軟體的中文官網](http://www.winpenpack.com/main/news.php) -
    具有最適合『[WinPenPack](../Page/WinPenPack.md "wikilink")』的可攜式軟體。
  - [OpenFoundry --
    PortableAppsMenu](http://of.openfoundry.org/projects/771/download) -
    繁中版『[PortableApps](../Page/PortableApps.md "wikilink")』的發源地 ～～
    **[自由軟體鑄造場](../Page/自由軟體鑄造場.md "wikilink")**。
  - [PChome -
    不占資源的好用綠色軟體](http://toget.pchome.com.tw/topic/topic_92.html)
  - [太平洋電腦網 -
    裝機必備綠色軟體專題](http://big5.pconline.com.cn/b5/www.pconline.com.cn/pcedu/redian/applied/green/)
  - [實用軟體介紹](http://www.chweng.idv.tw/swintro/)

### 简体中文网站

  - [霏凡旗下绿色下载站](http://www.greendown.cn/)
  - [绿色软件联盟](http://www.xdowns.com/)
  - [绿色软体站](http://www.onegreen.net/)

### 英文网站

  - [PortableApps.com](http://portableapps.com/) - Portable software for
    USB drives（[PortableApps](../Page/PortableApps.md "wikilink") 官網，具
    **[Portable](../Page/Portable.md "wikilink")** 軟體的最新資訊。）
  - [TinyApps.Org](http://tinyapps.org/)
  - [freewarefiles.com](http://www.freewarefiles.com/)
  - [Best Free Utilities](https://www.bestvpn.com/best-free-utilities/)
  - [Pricelessware.org](http://www.pricelessware.org/thelist/index.htm)
  - [sourceforge.net](http://www.sourceforge.net)
  - [Portable Applications on
    Linux](http://hacktolive.org/wiki/Portable_Applications_\(Linux\))
  - [PortableAppZ.blogspot.com](http://portableappz.blogspot.com)

[綠色軟件](../Category/綠色軟件.md "wikilink")
[Category:電腦術語](../Category/電腦術語.md "wikilink")