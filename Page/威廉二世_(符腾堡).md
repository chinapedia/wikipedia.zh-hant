[Wilhelm_II_of_Wurtt.png](https://zh.wikipedia.org/wiki/File:Wilhelm_II_of_Wurtt.png "fig:Wilhelm_II_of_Wurtt.png")
[Grabkapelle-koenig-wilhelm-II-wuerttemberg.jpg](https://zh.wikipedia.org/wiki/File:Grabkapelle-koenig-wilhelm-II-wuerttemberg.jpg "fig:Grabkapelle-koenig-wilhelm-II-wuerttemberg.jpg")
'''威廉二世 *'（Wilhelm II，），全名威廉·卡尔·保罗·亨利·弗里德里希（*Wilhelm Karl Paul Heinrich
Friedrich''）。[符腾堡末代国王](../Page/符腾堡.md "wikilink")（1891年-1918年）。

## 生平

威廉是[弗里德里希一世的曾孙](../Page/弗里德里希一世_\(符腾堡\).md "wikilink")，保尔王子的孙子，弗里德里希王子的儿子，母亲是符腾堡国王[威廉一世之女凯瑟琳](../Page/威廉一世_\(符腾堡\).md "wikilink")(1821-1898年)，威廉出生于首都[斯图加](../Page/斯图加.md "wikilink")。一出生就被他无嗣的堂叔兼舅舅国王[卡尔一世作为王位继承人进行教育](../Page/卡尔一世_\(符腾堡\).md "wikilink")。年轻时在[波茨坦服役于](../Page/波茨坦.md "wikilink")[普鲁士军队](../Page/普鲁士.md "wikilink")。在老国王日渐衰老的时候，年轻的威廉王子开始接手国王的事务。

威廉王子于1877年2月15日在[阿罗尔森娶](../Page/阿罗尔森.md "wikilink")[瓦尔德克-皮尔蒙特侯爵](../Page/瓦尔德克统治者列表.md "wikilink")[格奥尔格·维克多与表妹海伦娜公主的第三女](../Page/格奥尔格·维克多_\(瓦尔德克-皮尔蒙特\).md "wikilink")，格奥尔金·亨利埃特·瑪莉（1857-1882年）為妻，婚后有一子一女：

  - 女**保琳娜**（*Pauline Olga Helene
    Emma*，1877年12月19日-1965年5月7日），1898年嫁给[维德侯爵](../Page/维德侯爵.md "wikilink")[弗里德里希](../Page/弗里德里希_\(维德\).md "wikilink")。
  - 子**乌尔里希**（*Christoph Ulrich Ludwig*，1880年7月28日-1880年12月28日）

1882年瑪莉因难产去世。威廉王子于1886年4月8日在比克堡（Bückeburg）與表妹[紹姆堡-利珀的夏洛特](../Page/紹姆堡-利珀.md "wikilink")（1864-1946年）結婚，婚后没有子嗣。他的堂弟，第一任国王[弗里德里希一世七弟亚历山大王子的曾孙](../Page/弗里德里希一世_\(符腾堡\).md "wikilink")，符腾堡的[阿尔布雷希特公爵成为王位的下一顺位继承人](../Page/阿尔布雷希特_\(符腾堡\).md "wikilink")。
[König_Wilhelm_II._Postkarte_Silberhochzeit_1911.JPG](https://zh.wikipedia.org/wiki/File:König_Wilhelm_II._Postkarte_Silberhochzeit_1911.JPG "fig:König_Wilhelm_II._Postkarte_Silberhochzeit_1911.JPG")
1891年威廉的叔叔国王[卡尔一世去世](../Page/卡尔一世_\(符腾堡\).md "wikilink")，他继承了王位，成为**符腾堡国王威廉二世**。他在位时，符腾堡王国的大部分权利已为[德意志帝国所有](../Page/德意志帝国.md "wikilink")。1904年，他被[英国国王授予](../Page/英国.md "wikilink")[嘉德勋章](../Page/嘉德勋章.md "wikilink")。[一战中他被皇帝](../Page/一战.md "wikilink")[威廉二世封为](../Page/威廉二世_\(德国\).md "wikilink")[德國元帥](../Page/德國元帥.md "wikilink")。他與德意志其他統治者於1918年被废黜。他於1921年在贝奔豪森城堡逝世。

|-

[Category:符腾堡国王](../Category/符腾堡国王.md "wikilink")
[Category:斯圖加特人](../Category/斯圖加特人.md "wikilink")
[W](../Category/廢君.md "wikilink") [W](../Category/末代帝王.md "wikilink")
[W](../Category/嘉德騎士.md "wikilink")