**1928年**是一個[閏年](../Page/閏年.md "wikilink")，第一天從[星期日開始](../Page/星期日.md "wikilink")。

## 大事记

  - [土耳其放弃](../Page/土耳其.md "wikilink")[阿拉伯字母改用](../Page/阿拉伯字母.md "wikilink")[拉丁字母](../Page/拉丁字母.md "wikilink")。
  - 在[英国所有妇女获得](../Page/英国.md "wikilink")[选举权](../Page/选举权.md "wikilink")。
  - [埃德温·哈勃发现遥远](../Page/愛德文·哈勃.md "wikilink")[星系](../Page/星系.md "wikilink")[红移](../Page/红移.md "wikilink")。
  - [保罗·狄拉克提出](../Page/保罗·狄拉克.md "wikilink")[电子](../Page/电子.md "wikilink")[自旋的理论](../Page/自旋.md "wikilink")。
  - [乔治·伽莫夫等使用](../Page/乔治·伽莫夫.md "wikilink")[隧道效应来解释](../Page/量子穿隧效應.md "wikilink")[Α衰变](../Page/Α衰变.md "wikilink")。
  - [摩托罗拉成立](../Page/摩托罗拉.md "wikilink")。
  - 弗莱明发现[青霉素](../Page/青霉素.md "wikilink")。
  - [华特·迪士尼制作了世界第一部有声动画片](../Page/华特·迪士尼.md "wikilink")《蒸汽船威利》（也译作《威利汽船》《威廉号汽艇》），米老鼠诞生。
  - [香港廣播電台啟播](../Page/香港電台.md "wikilink")

### 1月

  - [1月8日](../Page/1月8日.md "wikilink")——[國民政府特任](../Page/國民政府.md "wikilink")[蔣介石為](../Page/蔣介石.md "wikilink")[國民革命軍總司令](../Page/國民革命軍.md "wikilink")\[1\]。
  - [1月10日](../Page/1月10日.md "wikilink")——[列夫·托洛茨基逃离](../Page/列夫·托洛茨基.md "wikilink")（有被逐出之说法）[苏联](../Page/苏联.md "wikilink")。

### 2月

  - [2月2日](../Page/2月2日.md "wikilink")——[中國國民黨二屆中央執行委員會第四次全體會議於上午](../Page/中國國民黨.md "wikilink")10時在[南京舉行開會式](../Page/南京.md "wikilink")；下午，整理黨務審查委員會開會，決定中央黨部除宣傳、組織兩部及秘書處不予更改外，請增設訓練委員會、民眾運動委員會以代替農、工、商、青、婦部，當晚蔣介石召集外交方針討論委員會開會，磋商清黨後外交方針\[2\]。
  - [2月3日](../Page/2月3日.md "wikilink")——中國國民黨二屆四中全會舉行首次會議，中執委員23人出席，中監委員8人列席，譚延闓主席，決議開除隸屬共產黨之中央執行委員[譚平山](../Page/譚平山.md "wikilink")、[林祖涵](../Page/林祖涵.md "wikilink")、[于樹德](../Page/于樹德.md "wikilink")、[吳玉章](../Page/吳玉章.md "wikilink")、[楊匏安](../Page/楊匏安.md "wikilink")、[惲代英](../Page/惲代英.md "wikilink")6人，候補中央執行委員[毛澤東](../Page/毛澤東.md "wikilink")、許蘇魂、[夏曦](../Page/夏曦.md "wikilink")、[韓麟符](../Page/韓麟符.md "wikilink")、[董必武](../Page/董必武.md "wikilink")、[屈武](../Page/屈武.md "wikilink")、[鄧穎超](../Page/鄧穎超.md "wikilink")7人黨籍\[3\]。
  - [2月4日](../Page/2月4日.md "wikilink")——中國國民黨二屆四中全會下午繼續開會，譚延闓主席，決議：修正通過「整理黨務根本計劃」審查委員會提出之《整理各地黨務決議案》，規定各地各級黨部一律暫行停止活動，聽候中央派人組成黨務指導委員會，辦理黨員重新登記及整理事宜\[4\]。
  - [2月7日](../Page/2月7日.md "wikilink")——中國國民黨二屆四中全會閉幕，上午舉行第四次會議，決議：推舉戴季陶、丁惟汾、于右任、譚延闓、蔣介石為中央執行委員會常務委員，推舉丁惟汾、于右任、王伯群等49人為國民政府委員，推定譚延闓、蔡元培、張靜江、李烈鈞、于右任為國民政府常務委員，譚延闓為國民政府主席，推選于右任、方振武、方聲濤等73人為軍事委員會委員，指定于右任、白崇禧、李宗仁、李濟深、何應欽、朱培德、程潛、馮玉祥、楊樹莊、蔣介石、閻錫山、譚延闓為軍事委員會常務委員，蔣介石為軍事委員會主席\[5\]。二屆四中全會通過《制止共産黨陰謀案》等決議。全會排斥了[汪精衛派系](../Page/汪精衛.md "wikilink")。爲抵制二屆四中全會，汪精衛派建立了“改組派”。
  - [2月11日](../Page/2月11日.md "wikilink")——[冬季奥林匹克运动会在](../Page/冬季奥林匹克运动会.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[圣莫里茨开幕](../Page/圣莫里茨.md "wikilink")。

### 3月

  - [3月12日](../Page/3月12日.md "wikilink")——[美國](../Page/美國.md "wikilink")[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[圣法兰西斯大堤崩溃](../Page/圣法兰西斯大堤.md "wikilink")，400人丧生。

### 4月

  - [4月1日](../Page/4月1日.md "wikilink")——[香港](../Page/香港.md "wikilink")[九廣鐵路](../Page/九廣鐵路.md "wikilink")[沙頭角支線關閉](../Page/沙頭角支線.md "wikilink")。
  - [4月6日](../Page/4月6日.md "wikilink")——[馮玉祥下令第二集團軍河南北部各軍進攻磁州](../Page/馮玉祥.md "wikilink")、大名，以分奉軍優勢，期解山西之危；[日本首相](../Page/日本首相.md "wikilink")[田中義一](../Page/田中義一.md "wikilink")、陸軍大臣[白川義則](../Page/白川義則.md "wikilink")、參謀總長[鈴木莊六開會](../Page/鈴木莊六.md "wikilink")，就「山東形勢遽變」研討對策，一致同意「迅行派兵保護濟南及膠濟沿線日僑」\[6\]。
  - [4月7日](../Page/4月7日.md "wikilink")——蔣介石下總攻擊令，第一、二、三集團軍分別沿津浦、京漢、正太鐵路挺進，同日蔣發布誓師詞稱，黨國存亡，主義成敗，人民禍福，同志榮辱，在此一戰；國民政府令：嗣後舊曆[清明](../Page/清明.md "wikilink")[植樹節](../Page/植樹節.md "wikilink")，應改為總理逝世紀念式，所有植樹節應即廢止，清明節各機關照常辦公\[7\]。
  - [4月18日](../Page/4月18日.md "wikilink")——國民政府外交部長[黃郛致電蔣介石](../Page/黃郛.md "wikilink")，請於軍事進行時，注意膠濟沿線日僑權益，免生事端，電曰：「查[膠濟鐵路沿線](../Page/膠濟鐵路.md "wikilink")，除[青島](../Page/青島.md "wikilink")、[濟南外](../Page/濟南.md "wikilink")，尚有章邱、張店、博山、周村、[濰縣](../Page/濰縣.md "wikilink")、淄川、金嶺鎮、坊子八處，為中日約定自開之商埠，日僑極多。又有地名『四方』者，日人亦夥，且為膠濟工廠所在地，與青島關係尤重。膠濟鐵路對於日本尚有債務關係。至淄川、金嶺鎮、坊子三處，更有中日合辦之礦業。是等處所，於軍事進行時最易發生事件，除飭知戰地政務委員會（[蔣作賓主持](../Page/蔣作賓.md "wikilink")）、外交處[蔡主任特加注意外](../Page/蔡公時.md "wikilink")，應請通飭前方將領，藉備參考。」；國民政府據密報獲悉：[張宗昌深恐不能固守濟南](../Page/張宗昌.md "wikilink")，派參謀長金壽良到青島與日本密約，即以青島及膠濟鐵路權益作代價，要求日本派兵驅逐北伐軍，所有青島、濟南、龍口、煙台等地完全歸日軍負責防守，不許北伐軍駐濟南\[8\]。
  - [4月19日](../Page/4月19日.md "wikilink")——日本政府下令第二次出兵山東，是日日本首相田中、參謀總長鈴木覲見天皇，奏請出兵山東獲准，內閣召集臨時閣議，通過第二次出兵山東決議，旋鈴木向熊本師團頒發出兵令：加派第六師團長福田中將統率所部5,000人從門司出發，向山東進兵，另以駐天津之三個中隊由小永伍率領向濟南進兵，所需經費230萬元除責任內閣支出70萬元外，餘部160萬元追加預算於特別議會；日軍建川少將由大連抵達北京訪日使芳澤及北京軍政府，接洽日本出兵山東事宜\[9\]。
  - [4月21日](../Page/4月21日.md "wikilink")——駐南京日本領事岡本一策偕海軍少將中島訪[何應欽](../Page/何應欽.md "wikilink")、[李烈鈞](../Page/李烈鈞.md "wikilink")，就國民政府外交部嚴重抗議日本出兵事進行解釋，釋在青島登陸之日本海軍，確係保僑，並無助逆情事；日本先遺艦「球磨」、「古鷹」、「對馬」三艦駛抵青島，旋該三艦陸戰隊員660名登陸\[10\]。
  - [4月25日](../Page/4月25日.md "wikilink")——日本熊本[第6師團](../Page/第6師團.md "wikilink")1,600餘名，由福田師團長率領，分乘三輪抵青島；日本駐青島司令福田布告，中國時局急變，戰禍將及山東，本軍警備濟南、青島以及膠濟鐵路，負責保護日僑生命財產，若有累及日僑情事，立即嚴辦，決不寬貸\[11\]。
  - [4月26日](../Page/4月26日.md "wikilink")——國民政府外交部照會日本外務大臣田中義一，第二次嚴重抗議日本出兵山東；日軍第6師團齋藤旅團長率領第二批日軍600餘人由青島開抵濟南\[12\]。
  - [4月28日](../Page/4月28日.md "wikilink")——[毛澤東率井岡山紅軍經](../Page/毛澤東.md "wikilink")[酃縣回到礱市](../Page/酃縣.md "wikilink")，是日與[朱德率領之](../Page/朱德.md "wikilink")[南昌起義部隊](../Page/南昌起義.md "wikilink")[會師](../Page/井冈山会师.md "wikilink")\[13\]。

### 5月

  - [5月1日](../Page/5月1日.md "wikilink")——國民革命軍第一集團軍克復[济南](../Page/济南.md "wikilink")\[14\]。
  - [5月3日](../Page/5月3日.md "wikilink")——日軍恣意屠殺中國軍民，製造濟南[慘案](../Page/五三慘案.md "wikilink")\[15\]。
  - [5月9日](../Page/5月9日.md "wikilink")——日本駐美大使松平就[濟南慘案訪](../Page/濟南慘案.md "wikilink")[美國國務卿](../Page/美國國務卿.md "wikilink")，稱出兵中國純係保護日僑，一俟秩序恢復即行撤軍\[16\]。
  - [5月14日](../Page/5月14日.md "wikilink")——[台灣發生](../Page/台灣.md "wikilink")[趙明河事件](../Page/趙明河事件.md "wikilink")。
  - [5月17日](../Page/5月17日.md "wikilink")——[第九届夏季奥林匹克运动会开幕](../Page/1928年夏季奥林匹克运动会.md "wikilink")。
  - 5月起，[马仲英在](../Page/马仲英.md "wikilink")[甘肃省反叛](../Page/甘肅省_\(中華民國\).md "wikilink")，是为[第四次河湟事变](../Page/第四次河湟事变.md "wikilink")。

### 6月

  - [6月1日](../Page/6月1日.md "wikilink")——[國民革命軍佔領北京](../Page/國民革命軍.md "wikilink")。
  - [6月3日](../Page/6月3日.md "wikilink")——[张作霖出京歸](../Page/张作霖.md "wikilink")[奉](../Page/关外.md "wikilink")\[17\]。
  - [6月4日](../Page/6月4日.md "wikilink")——張作霖在[皇姑屯車站遇炸](../Page/皇姑屯站.md "wikilink")\[18\]。史稱「[皇姑屯事件](../Page/皇姑屯事件.md "wikilink")」。
  - [6月6日](../Page/6月6日.md "wikilink")——[阎锡山移節保定](../Page/阎锡山.md "wikilink")，在行營就京津卫戍總司令職\[19\]。
  - [6月8日](../Page/6月8日.md "wikilink")——國民革命军第三集团军[孫楚部进入](../Page/孫楚.md "wikilink")[北京](../Page/北京.md "wikilink")\[20\]。
  - [6月15日](../Page/6月15日.md "wikilink")——[國民政府發表對外宣言](../Page/國民政府.md "wikilink")，略謂：現在軍事時期將告終結，國民政府正從事整頓與建設，謀求完成建設新國家之目的，國民政府對外之[關係](../Page/關係.md "wikilink")，自應另辟一新紀元\[21\]。
  - [6月18日](../Page/6月18日.md "wikilink")——[中国共产党第六次全国代表大会在](../Page/中国共产党第六次全国代表大会.md "wikilink")[苏联](../Page/苏联.md "wikilink")[莫斯科舉行](../Page/莫斯科.md "wikilink")，出席正式代表84人，候補代表34人\[22\]。
  - [6月30日](../Page/6月30日.md "wikilink")——[香港第一個](../Page/香港.md "wikilink")[電台誕生](../Page/電台.md "wikilink")，台號為[GOW](../Page/香港電台.md "wikilink")。

### 7月

  - [7月7日](../Page/7月7日.md "wikilink")——中國[國民政府外交部發表宣言](../Page/國民政府外交部.md "wikilink")，宣布废除一切[不平等条约](../Page/不平等条约.md "wikilink")，重訂新約；新疆政變，[新疆省政府主席](../Page/新疆省政府.md "wikilink")[楊增新出席迪化俄文專修學校畢業典禮](../Page/楊增新.md "wikilink")，正頒發文憑時，為交涉員兼軍事廳長樊耀南狙擊斃命\[23\]。
  - [7月22日](../Page/7月22日.md "wikilink")——駐中國[湖南平江獨立第五師第一團團長](../Page/湖南.md "wikilink")[彭德怀舉行閙餉起義](../Page/彭德怀.md "wikilink")（即[平江起義](../Page/平江起義.md "wikilink")）\[24\]。彭德懷加入[中国共产党](../Page/中国共产党.md "wikilink")。

### 8月

  - [8月8日](../Page/8月8日.md "wikilink")——[中國國民黨二屆五中全會在](../Page/中國國民黨.md "wikilink")[南京召開](../Page/南京.md "wikilink")。會議通過《政治問題決議案》和《整理軍事案》，任命[蔣介石爲](../Page/蔣介石.md "wikilink")[國民政府主席](../Page/國民政府主席.md "wikilink")。通過《依照總理主張訓政時期頒布約法決議案》。
  - [8月22日](../Page/8月22日.md "wikilink")——[日本冶金工業股份有限公司在](../Page/日本冶金工業股份有限公司.md "wikilink")[東京成立](../Page/東京.md "wikilink")。
  - [8月27日](../Page/8月27日.md "wikilink")——[法國外交部長](../Page/法國.md "wikilink")[白里安](../Page/白里安.md "wikilink")、[美國國務卿](../Page/美國.md "wikilink")[凱洛格發起簽訂](../Page/凱洛格.md "wikilink")[非戰公約](../Page/非戰公約.md "wikilink")，規定放棄以戰爭作為國家政策的手段和只能以和平方法解決國際爭端或衝突，又稱《凱洛格—白里安公約》。
  - [8月28日](../Page/8月28日.md "wikilink")——在[巴黎](../Page/巴黎.md "wikilink")，[凯洛格一白里安公约被签定](../Page/非戰公約.md "wikilink")，禁止主动性战争。
  - [8月30日](../Page/8月30日.md "wikilink")——[尼赫鲁创立](../Page/贾瓦哈拉尔·尼赫鲁.md "wikilink")[印度独立联盟](../Page/印度独立联盟.md "wikilink")，对抗[英国统治](../Page/英国.md "wikilink")，争取独立自由。

### 10月

  - [10月8日](../Page/10月8日.md "wikilink")——[蒋介石任南京](../Page/蒋介石.md "wikilink")[國民政府主席](../Page/國民政府主席.md "wikilink")。
  - [10月10日](../Page/10月10日.md "wikilink")——蒋介石就任国民政府委员会主席。

### 11月

  - [11月6日](../Page/11月6日.md "wikilink")——[美国总统大选](../Page/美国总统.md "wikilink")，[赫伯特·胡佛获胜](../Page/赫伯特·胡佛.md "wikilink")。
  - [11月10日](../Page/11月10日.md "wikilink")——[日本](../Page/日本.md "wikilink")[昭和天皇加冕](../Page/昭和天皇.md "wikilink")。

### 12月

  - [12月29日](../Page/12月29日.md "wikilink")——[张学良下令奉天](../Page/张学良.md "wikilink")（今[瀋陽](../Page/瀋陽.md "wikilink")）总部上空升起[国民政府國旗](../Page/中華民國國旗.md "wikilink")，并公开宣布支持国民政府，[中国形式上統一](../Page/中国.md "wikilink")，史稱[東北易幟](../Page/東北易幟.md "wikilink")。

## 出生

*參見：[:Category:1928年出生](../Category/1928年出生.md "wikilink")*

  - [1月2日](../Page/1月2日.md "wikilink")——[池田大作](../Page/池田大作.md "wikilink")，國際[創價學會會長](../Page/創價學會.md "wikilink")，[民主音樂協會創辦人](../Page/民主音樂協會創辦人.md "wikilink")，[東京富士美術館創辦人](../Page/東京富士美術館創辦人.md "wikilink")，[桂冠詩人](../Page/桂冠詩人.md "wikilink")。
  - [1月5日](../Page/1月5日.md "wikilink")——[佐勒菲卡尔·阿里·布托](../Page/佐勒菲卡尔·阿里·布托.md "wikilink")，[巴基斯坦政治家](../Page/巴基斯坦.md "wikilink")。（[1979年逝世](../Page/1979年.md "wikilink")）
  - [1月5日](../Page/1月5日.md "wikilink")——[錢其琛](../Page/錢其琛.md "wikilink")，[中國外交家](../Page/中國.md "wikilink")，前[中華人民共和國](../Page/中華人民共和國.md "wikilink")[副總理兼](../Page/副總理.md "wikilink")[外交部長](../Page/外交部長.md "wikilink")。（[2017年逝世](../Page/2017年.md "wikilink")）
  - [1月24日](../Page/1月24日.md "wikilink")——[德斯蒙德·莫利斯](../Page/德斯蒙德·莫利斯.md "wikilink")，英國著名動物學家。
  - [1月25日](../Page/1月25日.md "wikilink")——[謝瓦納茲](../Page/爱德华·谢瓦尔德纳泽.md "wikilink")，喬治亞政治家。（[2014年逝世](../Page/2014年.md "wikilink")）
  - [2月14日](../Page/2月14日.md "wikilink")——[黃秉乾](../Page/黃秉乾_\(律師\).md "wikilink")，香港事務律師。（[2007年逝世](../Page/2007年.md "wikilink")）
  - [2月25日](../Page/2月25日.md "wikilink")——[許文龍](../Page/許文龍.md "wikilink")，[台灣企業家](../Page/台灣.md "wikilink")，[奇美集團創辦人](../Page/奇美集團.md "wikilink")。
  - [2月26日](../Page/2月26日.md "wikilink")——[-{zh:阿里埃勒·沙龙; zh-cn:阿里埃勒·沙龙;
    zh-tw:艾里爾·夏隆;
    zh-hk:阿里埃勒·沙龍;}-](../Page/艾里爾·夏隆.md "wikilink")，以色列政治家。（[2014年逝世](../Page/2014年.md "wikilink")）
  - [3月18日](../Page/3月18日.md "wikilink")——[菲德尔·瓦尔德斯·拉莫斯](../Page/菲德尔·瓦尔德斯·拉莫斯.md "wikilink")，前[菲律賓總統](../Page/菲律賓.md "wikilink")。
  - [3月19日](../Page/3月19日.md "wikilink")——[林文镜](../Page/林文镜.md "wikilink")，中國僑領、融僑集團創始人。（[2018年逝世](../Page/2018年.md "wikilink")）
  - [4月4日](../Page/4月4日.md "wikilink")——[馬婭·安傑盧](../Page/馬婭·安傑盧.md "wikilink")，美國作家（[2014年逝世](../Page/2014年.md "wikilink")）
  - [4月6日](../Page/4月6日.md "wikilink")——[詹姆斯·沃森](../Page/詹姆斯·沃森.md "wikilink")，遗传学家。
  - [4月23日](../Page/4月23日.md "wikilink")——[秀兰·邓波儿](../Page/秀兰·邓波儿.md "wikilink")，美国电影明星，全世界第一位获得奥斯卡奖的童星。（[2014年逝世](../Page/2014年.md "wikilink")）
  - [5月2日](../Page/5月2日.md "wikilink")——[吉格梅·多吉·旺楚克](../Page/吉格梅·多吉·旺楚克.md "wikilink")，[不丹國王](../Page/不丹.md "wikilink")。（[1972年逝世](../Page/1972年.md "wikilink")）
  - [5月4日](../Page/5月4日.md "wikilink")——[穆巴拉克](../Page/穆巴拉克.md "wikilink")，[埃及總統](../Page/埃及總統.md "wikilink")。
  - [6月14日](../Page/6月14日.md "wikilink")——[切·格瓦拉](../Page/切·格瓦拉.md "wikilink")，[古巴革命家](../Page/古巴.md "wikilink")。（[1967年逝世](../Page/1967年.md "wikilink")）
  - [7月26日](../Page/7月26日.md "wikilink")——[康士](../Page/康士.md "wikilink")，香港最高法院上訴庭副庭長。
  - [7月26日](../Page/7月26日.md "wikilink")——[斯坦利·库布里克](../Page/斯坦利·库布里克.md "wikilink")，[美国电影导演](../Page/美国电影.md "wikilink")。（[1999年逝世](../Page/1999年.md "wikilink")）
  - [7月29日](../Page/7月29日.md "wikilink")——[李嘉誠](../Page/李嘉誠.md "wikilink")，[香港](../Page/香港.md "wikilink")[國際](../Page/國際.md "wikilink")[企業家](../Page/企業家.md "wikilink")，香港及[亞洲首富](../Page/亞洲.md "wikilink")，亦是世界上最富有的[華人](../Page/華人.md "wikilink")。
  - [8月4日](../Page/8月4日.md "wikilink")——[卡达尔·弗洛拉](../Page/卡达尔·弗洛拉.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")[女演员](../Page/演员.md "wikilink")（[2002年逝世](../Page/2002年.md "wikilink")）
  - [8月6日](../Page/8月6日.md "wikilink")——[安迪·沃荷](../Page/安迪·沃荷.md "wikilink")，美国[波普藝術的開創者](../Page/波普藝術.md "wikilink")（[1987年逝世](../Page/1987年.md "wikilink")）
  - [8月20日](../Page/8月20日.md "wikilink")——[黃信介](../Page/黃信介.md "wikilink")，台灣政治家，前民進黨主席。（[1999年逝世](../Page/1999年.md "wikilink")）
  - [8月31日](../Page/8月31日.md "wikilink")——[辛海棉](../Page/辛海棉.md "wikilink")，菲律賓籍樞機主教，被視為第一次和第二次的「[人民力量革命](../Page/人民力量革命.md "wikilink")」的精神領袖。（[2005年逝世](../Page/2005年.md "wikilink")）
  - [10月1日](../Page/10月1日.md "wikilink")——[朱镕基](../Page/朱镕基.md "wikilink")，[中華人民共和國第九任國務院總理](../Page/中华人民共和国国务院总理.md "wikilink")、前[中国人民银行行長](../Page/中国人民银行.md "wikilink")。
  - [10月20日](../Page/10月20日.md "wikilink")——[李鹏](../Page/李鹏.md "wikilink")，[中国政治人物](../Page/中国.md "wikilink")，前[全國人民代表大會常務委員會委員長](../Page/全國人民代表大會常務委員會.md "wikilink")、[國務院總理](../Page/國務院總理.md "wikilink")。
  - [10月21日](../Page/10月21日.md "wikilink")——[余光中](../Page/余光中.md "wikilink")，中國當代作家、詩人。（[2017年逝世](../Page/2017年.md "wikilink")）
  - [11月3日](../Page/11月3日.md "wikilink")——[手塚治虫](../Page/手塚治虫.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")、[動畫師](../Page/動畫師.md "wikilink")、[醫學博士](../Page/醫學博士.md "wikilink")，被稱為「漫畫之神」。（[1989年逝世](../Page/1989年.md "wikilink")）
  - [11月13日](../Page/11月13日.md "wikilink")——[許昭榮](../Page/許昭榮.md "wikilink")，前[台籍日本兵](../Page/台籍日本兵.md "wikilink")，為台籍日本兵權益奔走，[自焚抗議而死](../Page/自焚.md "wikilink")。（[2008年逝世](../Page/2008年.md "wikilink")）
  - [11月29日](../Page/11月29日.md "wikilink")——[尾崎秀樹](../Page/尾崎秀樹.md "wikilink")，[日本文學評論家](../Page/日本.md "wikilink")。（[1999年逝世](../Page/1999年.md "wikilink")）
  - [12月7日](../Page/12月7日.md "wikilink")——[诺姆·乔姆斯基](../Page/诺姆·乔姆斯基.md "wikilink")，美國語言學家。
  - [12月11日](../Page/12月11日.md "wikilink")——[杜維運](../Page/杜維運.md "wikilink")，台灣歷史學家。（[2012年逝世](../Page/2012年.md "wikilink")）
  - [12月15日](../Page/12月15日.md "wikilink")——[弗里顿斯莱希·洪德特瓦塞尔](../Page/弗里顿斯莱希·洪德特瓦塞尔.md "wikilink")，[奥地利建筑师和艺术家](../Page/奥地利.md "wikilink")。（[2000年逝世](../Page/2000年.md "wikilink")）

## 逝世

  - [2月4日](../Page/2月4日.md "wikilink")——[洛伦兹](../Page/亨得里克·洛仑兹.md "wikilink")，荷兰物理学家（[1853年出生](../Page/1853年.md "wikilink")）
  - [5月21日](../Page/5月21日.md "wikilink")——[野口英世](../Page/野口英世.md "wikilink")：[日本著名醫學家](../Page/日本.md "wikilink")，曾被三度提名诺贝尔医学奖（[1876年出生](../Page/1876年.md "wikilink")）
  - [6月3日](../Page/6月3日.md "wikilink")——[黎元洪](../Page/黎元洪.md "wikilink")，中华民国第三任[总统](../Page/中華民國總統.md "wikilink")（[1864年出生](../Page/1864年.md "wikilink")）
  - [6月4日](../Page/6月4日.md "wikilink")——[张作霖](../Page/张作霖.md "wikilink")，當時的中華民國[北洋政府國家元首](../Page/北洋政府.md "wikilink")，[北洋军阀](../Page/北洋军阀.md "wikilink")[奉系首领](../Page/奉系军阀.md "wikilink")（[1875年出生](../Page/1875年.md "wikilink")）
  - [6月18日](../Page/6月18日.md "wikilink")——[罗尔德·亚孟森](../Page/罗尔德·亚孟森.md "wikilink")，挪威极地探险家（[1872年出生](../Page/1872年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[歐文·瑞查森](../Page/歐文·瑞查森.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[阿道夫·奧托·賴因霍爾德·溫道斯](../Page/阿道夫·奧托·賴因霍爾德·溫道斯.md "wikilink")
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")：[罗伯特·巴拉尼](../Page/罗伯特·巴拉尼.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[西格里德·温塞特](../Page/西格丽德·温塞特.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：未颁发

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第1届，[1929年颁发](../Page/1929年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[翼](../Page/翅膀.md "wikilink")》（Wings）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[弗兰克·鮑才奇](../Page/弗兰克·鮑才奇.md "wikilink")（Frank
    Borzage）《[七重天](../Page/七重天.md "wikilink")》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[艾米尔·詹宁斯](../Page/艾米尔·詹宁斯.md "wikilink")（Emil
    Jannings）《[最后的命令](../Page/最后的命令.md "wikilink")》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[詹妮·盖诺](../Page/詹妮·盖诺.md "wikilink")（Janet
    Gaynor）《七重天》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——未设此项奖
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——未设此项奖

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖.md "wikilink")）

## 參考文獻

[\*](../Category/1928年.md "wikilink")
[8年](../Category/1920年代.md "wikilink")
[2](../Category/20世纪各年.md "wikilink")

1.
2.
3.
4.
5.
6.

7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.