**隐孢子虫病**（Cryptosporidiosis）是由单细胞[寄生虫隐孢子虫](../Page/寄生虫.md "wikilink")([Cryptosporidium](../Page/Cryptosporidium.md "wikilink")
)造成的[脊椎动物肠道疾病](../Page/脊椎动物.md "wikilink")，是导致人腹泻的主要原因之一。这是一类通过粪口途径传播的疾病，主要发生在胎盘动物当中。病原体通常寄生在[小肠肠上皮细胞的带虫空泡内](../Page/小肠.md "wikilink")。免疫缺陷患者（如[艾滋病病人](../Page/艾滋病.md "wikilink")）经常患有此类并发症，同时，由于免疫功能的不完善，寄生虫还可能会进入病人的[肝](../Page/肝.md "wikilink")、[肺](../Page/肺.md "wikilink")、[胰和](../Page/胰.md "wikilink")[胆囊等器官](../Page/胆囊.md "wikilink")，造成更为严重的病理反应。隐孢子虫病是一种水源性疾病（waterborne
disease），病原体在[水中以卵囊](../Page/水.md "wikilink")（oocyte）形式存在并得以传播，对自然水中环境有较好的耐受性，因此饮用不洁净的水成为该病的主要患病原因。

## 历史

隐孢子虫病最早发现于家鼠身上（Edward E Tyzzer,
1907）之后被证实为许多动物腹泻的主要原因之一。1976年在美国一名儿童身上发现第一例人类病例。1980年代人们对[艾滋病患者的研究发现](../Page/艾滋病.md "wikilink")，隐孢子虫是导致艾滋病病人肠炎的主要原因。历史上最出名的隐孢子虫病爆发发生在美国[密尔沃基市](../Page/密尔沃基市.md "wikilink")（Milwaukee），1993年春，由于取自[密歇根湖的饮用水遭到污染](../Page/密歇根湖.md "wikilink")，有大约403,000名的市民（占城市总人口的26％）罹患该病。这一事件迫使[密尔沃基市政府对饮用水处理工厂进行了大规模的改造](../Page/密尔沃基.md "wikilink")。

## 病源

[隐孢子虫是属于](../Page/隐孢子虫.md "wikilink")[顶复合器门](../Page/顶复合器门.md "wikilink")（*Apicomplexa*）隐孢子属（*Cryptosporidium*）的细胞外寄生虫。遗传信息分析指出与其亲缘关系最近的是寄生在节肢动物中的[簇虫](../Page/簇虫.md "wikilink")（Gregarinasina）。隐孢子虫的生命周期比较复杂，包括很难在体外进行培育。在宿主体内，它们一般是以单倍体的多核单细胞形式存在的，被称之为第一型分裂体（type
1
meront）。这一型的分裂体可以进行[无性繁殖](../Page/无性繁殖.md "wikilink")，每个细胞核同一部分细胞质结合成裂殖子（merozoite），之后在细胞内再进行染色体复制，形成新的细胞核和第一型分裂体。这个无性繁殖循环可以一直持续下去。

如果外部环境发生变化，隐孢子虫会转化为第二型分裂体（type 2
meront），这时候分裂体会形成大配子母细胞(macro-gametocytes)和小配子母亲细胞(micro-gametocyte)，二者结合形成双倍体[合子](../Page/合子.md "wikilink")。这是[有性繁殖阶段](../Page/有性繁殖.md "wikilink")，合子再进行孢子生殖，形成含有大量单倍体孢子的卵囊，随粪便排出宿主体外。

隐孢子虫的卵囊对外界环境有较好的耐受性，在8摄氏度的淡水和海水中可以存活1年以上，在零下20度的低温环境下亦可存活。然而臭氧和过氧化氢可以有效杀灭卵囊，零下70度的低温，或者对水进行加热（64度5分钟，72度10秒）也可以杀死寄生虫。然而，由于西方社会没有煮白开水饮用的习惯，造成这种疾病疫情一而再，再而三地在英美等发达国家中爆发。

可以感染人类的隐孢子虫有[人隐孢子虫](../Page/人隐孢子虫.md "wikilink")（*Cryptosporidium
hominis*）和[小隐孢子虫](../Page/小隐孢子虫.md "wikilink")（*Cryptosporidium
parvum*）。另外，寄生于鸟类的鹌鹑源火鸡隐孢子虫(Cryptosporidium
meleagridis)和其他隐孢子虫也偶尔会感染人体。

## 病征

隐孢子虫症的主要症状是腹泻，在患者粪便和体液中可以检测出大量病原体。免疫缺陷患者则易于引发肠炎，肺炎的疾病。潜伏期3天到6周，通常会延续4到6周，有转化为慢性的风险，没有特别的药物可以给予治疗。

隐孢子主要附在小肠壁微绒毛之间吸取宿主细胞的养分，由于某种未知的机制，虫体和宿主细胞形成带虫空泡，虫体寄生于带虫空泡内，实质上是细胞内寄生。

## 控制与防范

主要的防范措施是控制水和食物的质量，政府应积极倡导个人饮食卫生

[Category:寄生虫病](../Category/寄生虫病.md "wikilink")