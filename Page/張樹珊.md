**張樹珊**（），[字](../Page/表字.md "wikilink")**海柯**，[安徽](../Page/安徽.md "wikilink")[合肥人](../Page/合肥.md "wikilink")，是平定[太平天国](../Page/太平天国.md "wikilink")[捻軍之](../Page/捻軍.md "wikilink")[淮軍的清軍名將](../Page/淮軍.md "wikilink")，兄為[張树声](../Page/張树声.md "wikilink")。

## 安徽[鄉勇](../Page/鄉勇.md "wikilink")

  - [咸丰三年](../Page/咸丰.md "wikilink")，[太平軍入安徽](../Page/太平軍.md "wikilink")，树珊与兄树声练乡兵自卫，淮军之兴，自张氏始。
  - 咸豐五年，击[太平軍於](../Page/太平軍.md "wikilink")[巢湖](../Page/巢湖.md "wikilink")，率壮士数十人败太平軍，擒斩太平軍目五人，进破巢县太平軍营，叙外委。
  - 六年，复[来安](../Page/来安.md "wikilink")，随官军克[无为州](../Page/无为州.md "wikilink")，擢[千总](../Page/千总.md "wikilink")。又克[潜山](../Page/潜山.md "wikilink")，至[太湖](../Page/太湖.md "wikilink")，遇太平軍数万，树珊仅五百人，军粮火药皆尽。太平軍屯堤上，树珊选死士缘堤下蛇行入贼中，大呼击杀，太平軍惊溃。
  - 七年，败捻首[张洛行於](../Page/张洛行.md "wikilink")[官亭](../Page/官亭.md "wikilink")。太平軍方与捻相勾结，皖北几无完区，独合肥西乡以团练筑堡差安，时出境从剿太平軍。
  - 九年，克[霍山](../Page/霍山.md "wikilink")。
  - 十年，两解[六安围](../Page/六安.md "wikilink")。
  - 十一年，赴援[寿州](../Page/寿州.md "wikilink")，克[三河](../Page/三河.md "wikilink")，擢[都司](../Page/都司.md "wikilink")，赐花翎。

## 駐守[上海](../Page/上海.md "wikilink")

  - 同治元年，从[李鸿章赴上海](../Page/李鸿章.md "wikilink")，名其军曰树字营。[李秀成犯上海](../Page/李秀成.md "wikilink")，会诸军夹击走之。七月，会克[青浦](../Page/青浦.md "wikilink")。太平軍围[北新泾](../Page/北新泾.md "wikilink")，树珊偕[程学启力战旬馀](../Page/程学启.md "wikilink")，太平軍始遁，擢[游击](../Page/游击.md "wikilink")。进克[嘉定](../Page/嘉定.md "wikilink")，太平軍大举围[四江口](../Page/四江口.md "wikilink")，树珊偪太平軍而营，会诸军奋击，连破二十馀垒，遂解围，擢参将，赐号悍勇[巴图鲁](../Page/巴图鲁.md "wikilink")。

### 收復[常熟](../Page/常熟.md "wikilink")、[江陰](../Page/江陰.md "wikilink")、[无锡](../Page/无锡.md "wikilink")、[常州](../Page/常州.md "wikilink")

  - 是年冬，[常熟及](../Page/常熟.md "wikilink")[福山太平軍以城降](../Page/福山.md "wikilink")，而福山太平軍复叛，围常熟。二年正月，树珊率军航海抵福山西洋港，风潮作，飘舟近贼巢，潮退不得行。树珊曰：“兵法危地则战。”登岸结垒未就，太平軍大至，树珊疾捣中坚，枪伤左肘不少卻，拔出诸营之被围者，进解常熟之围，擢[副将](../Page/副将.md "wikilink")。
  - 会诸军进攻[江阴](../Page/江阴.md "wikilink")，树珊扼南门，太平軍去路，城复，太平軍无得脱者，以[总兵记名](../Page/总兵.md "wikilink")。
  - 进攻[无锡](../Page/无锡.md "wikilink")，悍酋[陈坤书](../Page/陈坤书.md "wikilink")、[李世贤方以十万众围](../Page/李世贤.md "wikilink")[大桥角](../Page/大桥角.md "wikilink")，树珊助剿，火太平軍轮船二、砲船十，歼毙甚众，解其围。李秀成复率众数万至，连营数十里，树珊与诸军夹击，太平軍大溃。
  - 会[苏州已下](../Page/苏州.md "wikilink")，秀成率死党入[太湖](../Page/太湖.md "wikilink")，结[常州太平軍](../Page/常州.md "wikilink")，水陆分进，援[无锡](../Page/无锡.md "wikilink")；时铭传专击外援太平軍，树珊与诸军合围，十一月，拔之，以[提督记名](../Page/提督.md "wikilink")。偕兄树声及[刘铭传进攻](../Page/刘铭传.md "wikilink")[常州](../Page/常州.md "wikilink")，同治三年(1864年)四月，克之，予一品封典，授广西[右江镇总兵](../Page/右江.md "wikilink")。

## 剿[捻軍戰歿](../Page/捻軍.md "wikilink")

  - 四年，[曾国藩督师剿捻](../Page/曾国藩.md "wikilink")，驻[徐州](../Page/徐州.md "wikilink")，以树珊所部为亲军，令援山东，破捻軍於[鱼台](../Page/鱼台.md "wikilink")。议设四镇，[陈州之](../Page/陈州.md "wikilink")[周家口为最要](../Page/周家口.md "wikilink")，初以[刘铭传驻之](../Page/刘铭传.md "wikilink")，既改铭传为游击之师，乃令树珊移驻。五年三月，击捻軍[沙河](../Page/沙河.md "wikilink")，捻軍窜扑周家口，回军夹击败之。五月，又败捻軍於[沙河东](../Page/沙河.md "wikilink")，树珊以贼骑飘勿靡常，耻株守，请改为游击之师。九月，驰解[许州之围](../Page/许州.md "wikilink")。十月，逐捻軍山东境，连败之[丰南](../Page/丰南.md "wikilink")、[定陶](../Page/定陶.md "wikilink")、[曹县](../Page/曹县.md "wikilink")。
  - 五年十一月，回军[周家口](../Page/周家口.md "wikilink")。捻軍窜湖北，偕总兵[周盛波追剿](../Page/周盛波.md "wikilink")。会[郭松林败绩於](../Page/郭松林.md "wikilink")[臼口](../Page/臼口.md "wikilink")(湖北[鐘祥](../Page/鐘祥.md "wikilink"))，捻軍焰愈炽，六年，一月树珊自[黄冈追至](../Page/黄冈.md "wikilink")[枣阳](../Page/枣阳.md "wikilink")，捻軍窜[黄州](../Page/黄州.md "wikilink")、[德安](../Page/德安.md "wikilink")，树珊驰援。诸将皆言捻軍悍且众，宜持重，树珊率亲军二百人穷追，抵[新家徬](../Page/新家徬.md "wikilink")。贼横走抄官军后，树珊力战陷阵，至夜半，马立积尸中不能行，下马鬥而死。后队据乡庄发枪砲拒捻軍，捻軍亦寻退，全军未败。
  - 事闻，[同治皇帝诏惜其忠勇](../Page/同治.md "wikilink")，从优议恤，予[骑都尉兼一云骑尉世职](../Page/骑都尉.md "wikilink")，建专祠，谥勇烈。七年，捻平，加赠太子[少保](../Page/少保.md "wikilink")。

[Z張](../Page/category:右江鎮總兵.md "wikilink")
[Z張](../Page/category:戰爭身亡者.md "wikilink")
[Z張](../Page/category:合肥人.md "wikilink")
[Z張](../Page/category:淮軍人物.md "wikilink")

[S樹](../Category/張姓.md "wikilink")
[Category:諡勇烈](../Category/諡勇烈.md "wikilink")
[Category:汉字巴图鲁勇号](../Category/汉字巴图鲁勇号.md "wikilink")