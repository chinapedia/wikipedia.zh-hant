**拉杰沙希**（[孟加拉语](../Page/孟加拉语.md "wikilink")：****）是[孟加拉国西北部的一座城市](../Page/孟加拉国.md "wikilink")，位于[博多河畔](../Page/博多河.md "wikilink")，靠近与[印度的国境](../Page/印度.md "wikilink")。是[拉杰沙希专区的首府](../Page/拉傑沙希專區.md "wikilink")，2011年人口763,952人\[1\]，是孟加拉国内人口排名第四的城市。若是加上其整个都市圈的其他卫星城（如katakhali及Nowhata等）的人口，其总人口可超百万人。为孟加拉国西部地区的政治经济文化中心。当地的[丝织品有名](../Page/丝织品.md "wikilink")。

## 历史

[Ruins_at_puthia.jpg](https://zh.wikipedia.org/wiki/File:Ruins_at_puthia.jpg "fig:Ruins_at_puthia.jpg")寺庙遗址\]\]
拉杰沙希古时在[奔那伐檀那](../Page/奔那伐檀那.md "wikilink")（《[新唐书](../Page/新唐书.md "wikilink")·地理志》），《[大唐西域记](../Page/大唐西域记.md "wikilink")》译为奔那伐弹那国（Punnavardhana）的境内。中世纪时曾被称为**兰普尔博阿利亚**（Rampur
Boalia）。该城建城时间，根据[苏菲派圣人](../Page/苏菲派.md "wikilink")[沙赫·马赫杜姆](../Page/沙赫·马赫杜姆.md "wikilink")（Hazrat
Shah
Makhdum）的纪念碑文来看，可追溯至1634年。拉杰沙希这个地名的来源在一些学者之间有争议。一般认为是来自印度[拉者的和波斯的](../Page/拉者.md "wikilink")[沙阿两词的合并](../Page/沙阿.md "wikilink")，这两个词均是指“国王”之意\[2\]。在18世纪曾有一处[荷兰租界](../Page/荷兰.md "wikilink")\[3\]。该地成为区域政治中心是在[莫卧儿王朝的](../Page/莫卧儿王朝.md "wikilink")1825年。在[印度民族起义期间](../Page/印度民族起义.md "wikilink")，英军在此置一司令部，以镇压周边的叛乱。1876年升为城镇，1991年升为市。

在1897年6月12日发生的地震中，大部分的公共建筑都被摧毁\[4\]。

## 参考文献

## 外部链接

  - [E-Rajshahi
    （英文）](http://www.erajshahi.gov.bd/app/index/index.php)，电子政府门户网站管理的拉杰沙希市公司

[Category:孟加拉国城市](../Category/孟加拉国城市.md "wikilink")

1.
2.
3.
4.