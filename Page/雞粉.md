[Knorr_Chicken_Powder.JPG](https://zh.wikipedia.org/wiki/File:Knorr_Chicken_Powder.JPG "fig:Knorr_Chicken_Powder.JPG")產品[家樂牌雞粉](../Page/家樂牌.md "wikilink")\]\]

**雞粉**，是[烹飪常用的一種](../Page/烹飪.md "wikilink")[味精](../Page/味精.md "wikilink")\[1\]，在[中国大陆一般叫做](../Page/中国大陆.md "wikilink")**鸡精**。不同厂家的配方略有不同，常见品牌的主要成份有[谷氨酸鈉](../Page/谷氨酸鈉.md "wikilink")（味精）、[谷氨酸](../Page/谷氨酸.md "wikilink")、[鹽](../Page/鹽.md "wikilink")、[雞油](../Page/雞油.md "wikilink")、[糖](../Page/糖.md "wikilink")、[薑](../Page/薑.md "wikilink")、[二鈉肌二酸](../Page/二鈉肌二酸.md "wikilink")、[薑黃粉及](../Page/薑黃粉.md "wikilink")[白胡椒](../Page/白胡椒.md "wikilink")，有些含有少量鸡肉成分，是雞肉口味的[味精](../Page/味精.md "wikilink")。

颜色从白色、淡黄色、黄色不等，一般是易碎的颗粒状，含有鲜味[核苷酸作为增鲜剂](../Page/核苷酸.md "wikilink")，具有增鲜作用，比味精的纯度低，更易吸收空气中的水份。

## 健康風險

雞粉是一種[增味劑](../Page/增味劑.md "wikilink")，而且含有大量[味精成分](../Page/味精.md "wikilink")\[2\]，但雞精生產商經常聲稱雞精、雞粉會較鹽或味精健康，可是根據[香港消費者委員會的檢測報告](../Page/香港消費者委員會.md "wikilink")，雞精或雞粉含[鈉量偏高](../Page/鈉.md "wikilink")\[3\]，含鈉量甚至比[魚露及](../Page/魚露.md "wikilink")[醬油高出一倍](../Page/醬油.md "wikilink")\[4\]。消委會抽查的8個雞粉樣本，平均每100克含鈉量達約一萬七千毫克，只要食用12克雞粉已超出[世界衞生組織建議](../Page/世界衞生組織.md "wikilink")，成年人每日應攝取少於2千毫克鈉的標準\[5\]。因此過量食用雞精調味的食品，同樣會增加患上如[高血壓等循環系統疾病的風險](../Page/高血壓.md "wikilink")。

## 參見

  - [味精](../Page/味精.md "wikilink")

## 參考資料

[ME](../Category/調味料.md "wikilink") [ME](../Category/德国发明.md "wikilink")
[ME](../Category/尤斯图斯·冯·李比希.md "wikilink")

1.  [調味料含鈉超標
    雞粉最勁](http://health.takungpao.com.hk/hot/q/2015/0916/3168313.html)，大公報，2015-09-16
2.  [鸡精的基础成分是味精](http://news.xinhuanet.com/health/2012-12/12/c_124085033.htm)
    ，新华网，2012-12-12
3.  [雞粉鈉含量驚人
    食得健康7件事](http://topick.hket.com/article/695893/%E9%9B%9E%E7%B2%89%E9%88%89%E5%90%AB%E9%87%8F%E9%A9%9A%E4%BA%BA%E3%80%80%E9%A3%9F%E5%BE%97%E5%81%A5%E5%BA%B77%E4%BB%B6%E4%BA%8B)，香港經濟日報，2015-09-15
4.  [調味料含鈉偏高
    雞粉最勁](http://paper.wenweipo.com/2015/09/16/HK1509160029.htm)，文匯報，2015-09-16
5.  [消委會：雞粉含鈉偏高
    食12克已超每日上限](http://hk.on.cc/hk/bkn/cnt/news/20150915/bkn-20150915100821692-0915_00822_001.html)，東方日報，2015-09-15