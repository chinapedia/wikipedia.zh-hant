**株式會社TBS電視**（，），通稱**TBS**、**TBS電視台**（），是[日本一家以](../Page/日本.md "wikilink")[關東地方為播出區域的](../Page/關東地方.md "wikilink")[無線](../Page/地面電視.md "wikilink")[電視臺](../Page/電視臺.md "wikilink")，為日本五大[電視聯播網之一的](../Page/電視聯播網.md "wikilink")[JNN之](../Page/日本新聞網_\(TBS\).md "wikilink")[核心局](../Page/核心局.md "wikilink")。1955年4月1日開播，當時為[東京放送](../Page/東京放送.md "wikilink")（今[東京放送控股](../Page/東京放送控股.md "wikilink")）主要的經營業務之一；現在的[公司](../Page/公司.md "wikilink")[法人則成立於](../Page/法人.md "wikilink")2000年3月21日，為東京放送控股的全資[子公司](../Page/子公司.md "wikilink")。

## 沿革

  - 1955年4月1日，東京電台於[東京](../Page/東京.md "wikilink")[赤坂開始電視廣播業務](../Page/赤坂.md "wikilink")，台名為「**KR電視台**」（），以「**JOKR-TV**」為[呼號](../Page/無線電台呼號.md "wikilink")。
  - 1959年8月1日，為了與擁有全國性電視網的[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）競爭轉播皇太子[明仁的](../Page/明仁.md "wikilink")[成婚](../Page/結婚.md "wikilink")[慶祝遊行](../Page/巡遊.md "wikilink")，在東京電台的主導下，日本第一個[電視聯播網](../Page/電視聯播網.md "wikilink")「[日本新聞網](../Page/日本新聞網_\(TBS\).md "wikilink")」（JNN）成立。
  - 1960年9月10日，KR電視台開始[彩色電視廣播](../Page/彩色電視.md "wikilink")。
  - 1960年11月29日，東京電台改名為株式會社**東京放送**（），略稱「**TBS**」，當時還使用「東京電視台」（）、「東京電台」（）等簡稱。
  - 1961年12月1日，東京放送正式統一以「TBS」做為簡稱。
  - 2000年3月21日，TBS成立兩家全資子公司：

<!-- end list -->

  -
    株式會社**TBS娛樂**（），負責TBS電視娛樂節目的製作。
    株式會社**TBS體育**（），負責TBS電視體育節目的製作。

<!-- end list -->

  - 2001年3月22日：TBS成立株式會社**TBS LIVE**（）成立，負責TBS電視資訊與直播節目的製作。
  - 2001年10月1日，TBS的電視頻道呼號改為「JORX-TV」，其頻道名稱亦同步從「東京放送」改為「**TBS電視台**」（）。
  - 2003年12月1日上午11時（[日本時間](../Page/日本時間.md "wikilink")），TBS開始無線[數位電視廣播](../Page/數位電視.md "wikilink")，呼號為「**JORX-DTV**」。
  - 2004年10月1日：TBS娛樂與TBS體育、TBS
    LIVE合併，並將公司名稱改為「**TBS電視**」（），全面承接TBS的電視節目製作業務。
  - 2009年4月1日：TBS導入[控股公司制度](../Page/控股公司.md "wikilink")，東京放送更名為[東京放送控股株式會社](../Page/東京放送控股.md "wikilink")，TBS電視則承接原東京放送的電視廣播業務，同時接收原東京放送使用的「TBS」簡稱。
  - 2011年7月24日：TBS結束無線[類比電視廣播](../Page/類比電視.md "wikilink")，全面由數位電視廣播取代。
  - 2013年5月31日：TBS的電視訊號發射站從[東京鐵塔轉移至](../Page/東京鐵塔.md "wikilink")[東京晴空塔](../Page/東京晴空塔.md "wikilink")，原在東京鐵塔的設備改為備援之用。

## 訊號發射站

  - [東京晴空塔](../Page/東京晴空塔.md "wikilink") 遙控器頻道：6 ch / 物理頻道：22 ch

## 姊妹台

  - ：

      - [中華電視公司](../Page/中華電視公司.md "wikilink")
      - <s>[八大電視](../Page/八大電視.md "wikilink")</s>：2014年2月25日，TBS電視台與台灣[八大電視簽訂合作契約](../Page/八大電視.md "wikilink")，八大電視自同年3月17日起在旗下的[戲劇台開闢TBS戲劇時段](../Page/八大戲劇台.md "wikilink")，於每日晚間11點推出「TBS劇場」，並將嘗試將TBS當季戲劇與日本同時播出\[1\]\[2\]；但雙方在同年6月底終止合作\[3\]。

  - ：[優酷](../Page/優酷.md "wikilink")\[4\]

  - ：[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")

## 參考資料

## 參見

  - [東京放送控股](../Page/東京放送控股.md "wikilink")
  - [日本新聞網 (TBS)](../Page/日本新聞網_\(TBS\).md "wikilink")

## 外部連結

  - [TBS電視臺](http://www.tbs.co.jp/)
  - [TBS電視臺企業網站](http://www.tbs.co.jp/company/)

[TBS電視台](../Category/TBS電視台.md "wikilink")
[\*](../Category/東京放送控股.md "wikilink")
[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:日本新聞網_(TBS)](../Category/日本新聞網_\(TBS\).md "wikilink")
[Category:赤坂](../Category/赤坂.md "wikilink")

1.  [八大電視與日本TBS電視台跨國合作 開闙日劇時段「TBS劇場」 -
    八大電視新聞稿 2014.02.25](http://www.gtv.com.tw/News/20140225001/)

2.  [八大合作TBS 半澤直樹打頭陣 -
    自由時報 2014.02.26](http://www.libertytimes.com.tw/2014/new/feb/26/today-show5.htm)

3.  [日劇收視失靈 八大賠千萬喊卡 -
    中國時報 2014.07.17](http://www.chinatimes.com/newspapers/20140717000777-260112)
4.  [福利！日本TBS将通过优酷土豆在华发布影视作品](http://tech.huanqiu.com/internet/2016-11/9682135.html)