**nProtect
GameGuard**（簡稱**GameGuard**或**GG**，其[驅動程序為](../Page/驅動程序.md "wikilink")**GameMon.des**）是由[韓國](../Page/韓國.md "wikilink")[INCA互聯網](../Page/INCA互聯網.md "wikilink")（INCA
Internet）開發的遊戲反作弊的軟體。隨著[網絡遊戲的興起](../Page/網絡遊戲.md "wikilink")，愈來愈多人利用[外掛從中作弊](../Page/遊戲外掛.md "wikilink")，這促使GameGuard等反作弊軟體的誕生，GameGuard開發完成後，很快就被[日本及南韓網絡遊戲商引入](../Page/日本.md "wikilink")。隨後開設「網絡遊戲用戶通報中心」，能傳送關於不正當或作弊工具之訊息。

## 功能

nProtect
GameGuard含有即時變換偵測規則、可置於遊戲執行檔前使用，利用動態加密的方式達到防止遊戲外掛的目的，有效防堵作弊程式（如加速器），以及偵測玩

家電腦有沒有使用遊戲外掛程式等。

nProtect GameGuard具有多種功能，例如：

  - 透過持續掃瞄任何事先有登入過的程式碼、系統內部時間器運作等方式，偵測玩家電腦有沒有使用遊戲外掛程式。
  - 檢測及阻擋惡意程式碼。
  - 自動掃瞄工具。
  - 即時變換偵測。
  - 可停止滑鼠及鍵盤的驅動程序及側錄程式。
  - 可阻擋玩家及雙重核心[處理器](../Page/處理器.md "wikilink")（CPU）之不正當的操作。
  - 佔用甚少CPU，不會拖慢電腦及遊戲。
  - 監視玩家之操作環境，以及一舉一動\[1\]。

## 系統

GameGuard的驅動方法，與[rootkit相似](../Page/rootkit.md "wikilink")；縱使有些遊戲商提供了GameGuard移除程式（gguninst.exe）供玩家移除GameGuard\[2\]，但其驅動程式無法移除，即使移除了含GameGuard的遊戲後還是會留有一些隱藏資料夾。有時開啟遊戲時，因[防毒軟體或](../Page/防毒軟體.md "wikilink")[入侵預防系統阻攔GameGuard的運行而無法進入遊戲](../Page/入侵預防系統.md "wikilink")，它可以通過設置防毒軟件來允許GameGuard的加載。GameGuard在攔截外掛不斷更新的同時，也大大提高了它本身與各個公司防毒軟件的兼容性，減少了此類情況的發生。

## 常見GameGuard的錯誤訊息

以[台灣](../Page/台灣.md "wikilink")[LUNA2
Online為例](../Page/LUNA2_Online.md "wikilink") ，錯誤訊息代碼可能隨遊戲不同，只能作為參考：

<table>
<thead>
<tr class="header">
<th><p>代碼</p></th>
<th><p>內容</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>100</p></td>
<td><p>電腦中了病毒</p></td>
</tr>
<tr class="even">
<td><p>110</p></td>
<td><p>遊戲程式已在執行</p></td>
</tr>
<tr class="odd">
<td><p>112</p></td>
<td><p>在讀取「確認病毒、駭客功能」的模組失敗，<br />
可能是記憶體不足或是電腦中毒</p></td>
</tr>
<tr class="even">
<td><p>114</p></td>
<td><p>可能某些<a href="../Page/程式.md" title="wikilink">程式消耗了太多CPU的資源</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/顯示卡.md" title="wikilink">顯示卡</a>、音效卡並未正確安裝</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>可能是因為電腦正在執行掃毒</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>115</p></td>
<td><p>遊戲已多次執行</p></td>
</tr>
<tr class="even">
<td><p>GameGuard已在執行</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>124</p></td>
<td><p>無法正常<a href="../Page/更新.md" title="wikilink">更新</a></p></td>
</tr>
<tr class="even">
<td><p>150</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>153</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>155</p></td>
<td><p><a href="../Page/作業系統.md" title="wikilink">作業系統的</a><a href="../Page/檔案.md" title="wikilink">檔案受損</a></p></td>
</tr>
<tr class="odd">
<td><p>170</p></td>
<td><p>執行GameGuard流程時失敗</p></td>
</tr>
<tr class="even">
<td><p>200</p></td>
<td><p>一個不合法的程式被發現</p></td>
</tr>
<tr class="odd">
<td><p>340</p></td>
<td><p>下載失敗（連線品質也許不夠穩定）</p></td>
</tr>
<tr class="even">
<td><p>350</p></td>
<td><p>GameGuard停止更新（可能因為更新的速度過慢）</p></td>
</tr>
<tr class="odd">
<td><p>360</p></td>
<td><p>更新不成功</p></td>
</tr>
<tr class="even">
<td><p>GameGuard檔案遭篡改</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>361</p></td>
<td><p>初始化錯誤</p></td>
</tr>
<tr class="even">
<td><p>GameGuard的下載程序並不完整</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>380</p></td>
<td><p>GameGuard無法更新</p></td>
</tr>
<tr class="even">
<td><p>像是「Sygate防火牆」阻擋了GameGuard的傳輸及存取</p></td>
<td></td>
</tr>
</tbody>
</table>

## 爭議

雖然GameGuard有多種反作弊的功能，[台灣地區一些網絡遊戲代理商如](../Page/台灣地區.md "wikilink")[茂為歐買尬數位科技都樂於使用GameGuard來防止玩家使用外掛](../Page/茂為科技.md "wikilink")，但對於某些網絡遊戲（如《[巨商](../Page/巨商.md "wikilink")》、《[魔物獵人Frontier](../Page/魔物獵人Frontier.md "wikilink")》）來說成效卻不大，且GameGuard漏洞多多，新型的外掛在GameGuard下都能使用，令GameGuard一向被嘲為「紙老虎」，亦帶來很多在系統上的麻煩。

例子包括：

  - 2007年4月14日，台灣楓之谷GameGuard更新後，部份玩家因無法更新，造成出現「遊戲初始化錯誤」等錯誤訊息，無法進行遊戲。同年12月14日台灣楓之谷GameGuard更新後，部份玩家更持續出現以下異常狀況：「初始化錯誤360」的訊息、遊戲畫面出現藍底白字的情形、當機及更新無法正常執行等，因而無法正常進行遊戲。\[3\]
  - 2007年7月26日，日本大型網絡遊戲商之一[NEXON於旗下遊戲](../Page/NEXON.md "wikilink")《[天翼之鍊](../Page/天翼之鍊.md "wikilink")》（Tales
    Weaver）引入GameGuard後，玩家之遊戲程式、作業系統受到破壞，很多玩家出現操作不正常的情況，甚至連遊戲也無法登入，此情況至今都有發生。
  - 同為NEXON代理的網絡遊戲《[瑪奇](../Page/瑪奇.md "wikilink")》引入GameGuard後，亦發生遊戲程式受到破壞等情況，導致NEXON在三天後火速收回成命。（同一套遊戲中國大陸版本亦發生相同狀況，使得代理商世紀天成也在兩週不到的時間停止使用。北美版亦於2009年5月7日停用了GameGuard並由[HackShield取而代之](../Page/HackShield.md "wikilink")。）同樣問題亦遍及[世嘉公司的](../Page/世嘉公司.md "wikilink")《[夢幻之星
    新宇宙](../Page/夢幻之星_新宇宙.md "wikilink")》和[Capcom的](../Page/Capcom.md "wikilink")《魔物獵人》\[4\]。由於GameGuard有太多漏洞，有些魔物獵人的玩家能使用某些外掛或在遊戲中作弊。
  - 2007年8月，《巨商》中、港、台的伺服器在GameGuard的防護下都受到外掛的影響，其中香港伺服器因外掛造成垃圾資料增多而多次異常封閉。香港代理商遊戲橘子表示多年前巨商亦曾經因外掛橫行被當機纏繞著，當時每日當機次數高達十次，經過代理商與韓國開發商Joyon多月的努力，引入nProtect來阻擋外掛，得以解決及改善當機的問題。但是不知當機情況現在還會發生。\[5\]
  - 2007年末，[香港](../Page/香港.md "wikilink")[戲谷旗下遊戲](../Page/戲谷.md "wikilink")《[拿拿林Online](../Page/拿拿林Online.md "wikilink")》自從引入GameGuard，部份玩家因GameGuard問題無法登入遊戲，引起玩家反感。\[6\]
    日本的《[夢之歷險](../Page/夢之歷險.md "wikilink")》（Nostale）亦出現同樣情況。
  - 在較新的GameGuard版本（1007版本或以後），更出現遊戲關閉後GameGuard仍繼續運行的情況。
  - 2008年9月24日
    [戲谷代理的](../Page/戲谷.md "wikilink")[跑online在更新NProtect](../Page/跑online.md "wikilink")
    GameGuard有大批玩家無法進行遊戲，因此官方於2009年10月轉用其他服務。

此外，GameGuard的翻譯亦被人批評其意思令人費解，除了[韓文以外的信息都有錯字或是文法錯誤](../Page/韓文.md "wikilink")（例如『Game
is already
**rimmomg**.』，『rimmomg'實際上指running（運行）），英文版GameGuard亦被指部份原文為韓文的信息更加未曾翻譯。

[Linux和](../Page/Linux.md "wikilink")[Mac
OS使用者也因為GameGuard而無法透過](../Page/Mac_OS.md "wikilink")[兼容层執行Windows](../Page/兼容层.md "wikilink")
程式，在其他同類遊戲正常運作的情況下，使用GameGuard*保護*的那些遊戲完全無法運作。

因以上問題，加上有些遊戲代理商對GameGuard問題態度曖昧，對GameGuard的爭議就愈來愈多，多數反對使用GameGuard者都認為GameGuard是「有百害而無一利」的。

## 已知使用GameGuard之遊戲

以下是有使用GameGuard之網絡遊戲列表：

  - [王牌對決](../Page/王牌對決.md "wikilink")([英雄大亂鬥](../Page/英雄大亂鬥.md "wikilink"))
    [連結官網](https://web.archive.org/web/20161015105129/http://lostsaga.garena.tw/)

  - [CABAL 2 黑潮來襲](../Page/CABAL_2_黑潮來襲.md "wikilink")

  - [TERA Online](../Page/TERA_Online.md "wikilink")

  - [永恆之塔](../Page/Aion.md "wikilink")（Aion）

  - [HEVA Online](../Page/HEVA_Online.md "wikilink")

  - [LUNA2 Online](../Page/LUNA2_Online.md "wikilink")

  -
  - [PRIUS守護之星 Online](../Page/PRIUS守護之星_Online.md "wikilink")

  - [R2](../Page/易吉網.md "wikilink")－＞\[7\]

  - [R2BEAT](../Page/R2BEAT.md "wikilink")

  - [RF Online](../Page/RF_Online.md "wikilink")

  - [SD鋼彈 Online](../Page/SD鋼彈_Online.md "wikilink")

  - Soldier Front

  - [XEN傳說 Online](../Page/XEN傳說_Online.md "wikilink")－＞已關閉

  - [十二之天](../Page/十二之天.md "wikilink")－＞\[8\]

  - [天堂](../Page/天堂_\(遊戲\).md "wikilink")

  - [天堂II](../Page/天堂II.md "wikilink")

  - [天翼之鍊](../Page/天翼之鍊.md "wikilink")

  - [幻想大陸](../Page/幻想大陸.md "wikilink")（Fantasy Earth:
    Zero）－＞（台灣版於7月16日更新後新增）

  - [火線特戰隊](../Page/火線特戰隊.md "wikilink")

  - [王者世界](../Page/王者世界.md "wikilink")

  - [全民打棒球](../Page/全民打棒球.md "wikilink")

  - [新瑪奇mabinogi](../Page/瑪奇.md "wikilink")
    －＞(台灣版僅實施3日取消，2015年6月25日實施HackShield防掛系統，2015年12月21日另採用Nexon
    Game Security取代至今)

  - [光線飛車](../Page/光線飛車.md "wikilink")

  - [希望Online](../Page/希望Online.md "wikilink")

  - [奇蹟](../Page/奇蹟_\(遊戲\).md "wikilink")－＞\[9\]

  - [迷你格鬥](../Page/迷你格鬥.md "wikilink")

  - [神泣](../Page/神泣.md "wikilink")

  - [真·三國無雙 Online](../Page/真·三國無雙_Online.md "wikilink")（台灣代理區）

  -
  - [絲路Online](../Page/絲路Online.md "wikilink")

  - [黑色陰謀Online](../Page/黑色陰謀Online.md "wikilink")－＞\[10\]

  - [新飛飛 Online](../Page/飞飞.md "wikilink")

  - [夢幻之星 新宇宙](../Page/夢幻之星_新宇宙.md "wikilink")

  - [夢夢Online](../Page/夢夢Online.md "wikilink")－＞於2010年4月1日維修更新後造成許多玩家電腦CPU使用率達到100%
    並且會和NOD32防毒軟體相衝造成系統異常重新開機

  - [亂Online](../Page/亂_Online.md "wikilink")（Ran Online）

  - [精靈II](../Page/精靈II.md "wikilink")－＞\[11\]

  - [魔法飛球](../Page/魔法飛球.md "wikilink")

  - [墨香Online](../Page/墨香Online.md "wikilink")

  - [希望戀曲Online](../Page/希望戀曲Online.md "wikilink")

  - [魔物獵人Online](../Page/魔物獵人Online.md "wikilink")　nProtect Game Rev
    1790 支援 x64 目前尚無人破解！

  - [夢之物語](../Page/夢之物語.md "wikilink")

  - [大航海時代Online](../Page/大航海時代Online.md "wikilink")

  - [phantasy star 2](../Page/phantasy_star_2.md "wikilink")

  - [Mstar](../Page/Mstar.md "wikilink")

  - [艾爾之光](../Page/艾爾之光.md "wikilink")－＞（原為不使用，原野系統更新後導入此程式）

  - [NBA2K Online](../Page/NBA2K_Online.md "wikilink")\[12\]

  - [劍靈(台服)](../Page/劍靈\(台服\).md "wikilink")

  - [ELOA艾洛亞online](../Page/ELOA艾洛亞online.md "wikilink")

  - [FreeStyle online](../Page/FreeStyle_online.md "wikilink")

  - [Soul Worker(台版)](../Page/Soul_Worker.md "wikilink")

## 已知停用GameGuard之遊戲

以下是[大中華地區停用GameGuard之網絡遊戲列表](../Page/大中華地區.md "wikilink")：

  - [永恆冒險](../Page/永恆冒險.md "wikilink")－＞（永恆冒險於2011年1月26日停止用GameGuard，改以[X-trap防外掛軟體取代](../Page/X-trap.md "wikilink")
  - [光之冒險](../Page/NosTale.md "wikilink")（Nos
    Tale）（已停用GameGuard，並改以[X-trap防外掛軟體取代](../Page/X-trap.md "wikilink")）
  - [SF Online](../Page/SF_Online.md "wikilink")－＞台灣版SF
    Online於2012年5月14日已移除GameGuard，取而代之是XIGNCODE3防外掛軟體
  - [巨商](../Page/巨商.md "wikilink")－＞港服已經停止使用有關防外掛之程式
  - [拿拿林Online](../Page/拿拿林Online.md "wikilink")－＞已經停止使用有關防外掛之軟體
  - [跑Online](../Page/跑Online.md "wikilink")－＞跑Online於2009年10月14日已移除GameGuard，取而代之是[HackShield防外掛軟體](../Page/HackShield.md "wikilink")，並再改以[X-trap防外掛軟體取代](../Page/X-trap.md "wikilink")
  - [跑跑卡丁車](../Page/跑跑卡丁車.md "wikilink")－＞台灣版跑跑卡丁車於2009年4月17日已移除GameGuard，取而代之是[HackShield防外掛軟體](../Page/HackShield.md "wikilink")，並再改以[XIGNCODE3](../Page/XIGNCODE3.md "wikilink")+Nexon
    Game Security防外掛軟體
  - [新楓之谷](../Page/新楓之谷.md "wikilink")－＞台灣新楓之谷在Ver0.92版2009年4月28日已移除GameGuard，取而代之是[HackShield防外掛軟體](../Page/HackShield.md "wikilink")
    ，並再改以[XIGNCODE3防外掛軟體取代](../Page/XIGNCODE3.md "wikilink")
  - [夢想之翼Online](../Page/夢想之翼Online.md "wikilink")－＞已停用GameGuard
  - [爆爆王](../Page/爆爆王.md "wikilink")－＞台灣版爆爆王於2009年4月9日已移除GameGuard，取而代之是HackShield防外掛軟體，並再改以[XIGNCODE3](../Page/XIGNCODE3.md "wikilink")+Nexon
    Game Security防外掛軟體
  - [絕對武力Online](../Page/絕對武力Online.md "wikilink")（CSOnline）－＞台灣版CSO於2009年4月22日已移除GameGuard，取而代之是HackShield防外掛軟體，並再改以[XIGNCODE3](../Page/XIGNCODE3.md "wikilink")+Nexon
    Game Security防外掛軟體，並再於13-3-2018移除XIGNCODE3
  - [大航海時代Online](../Page/大航海時代Online.md "wikilink")－＞台灣版大航海時代Online於2015年8月20日「亞特蘭提斯」改版已停止使用有關防外掛之軟件
  - [新瑪奇 mabinogi](../Page/瑪奇.md "wikilink") －＞台灣版瑪奇於2015年12月21日已由Nexon
    Game Security取代HackShield防外掛軟體
  - [彩虹島物語](../Page/彩虹岛.md "wikilink")
    －＞[快樂玩於](../Page/快樂玩.md "wikilink")2017年12月28日改採[XIGNCODE3](../Page/XIGNCODE3.md "wikilink")。

## 已知封鎖應用程式

部份已知被GameGuard 封鎖之應用程式列表如下：
（註：封鎖與否則視遊戲目前的GameGuard版本而定。）

  - [Adobe Flash
    Player](../Page/Adobe_Flash_Player.md "wikilink")（被廣泛使用的多媒體程序播放器）

  - Logitech'G'系列（滑鼠、鍵盤）

  - [Wireshark](../Page/Wireshark.md "wikilink")（網絡封包分析軟件，原稱Etherea

  - （一調試器）l）\<需要在遊戲執行後才能開啟,否則會wireshark當掉\>

  - 部份[VNC](../Page/VNC.md "wikilink")[伺服器](../Page/伺服器.md "wikilink")，例如（不會封鎖或異常關機，但令電腦無法輸入資訊）

  - [LimeWire](../Page/LimeWire.md "wikilink")（[Java平臺](../Page/Java平臺.md "wikilink")[點對點](../Page/點對點.md "wikilink")[檔案分享客戶端](../Page/檔案分享.md "wikilink")，不會異常關機或顯示『企圖作弊』錯誤字句，但會關閉此程式）

  - [Cheat Engine](../Page/Cheat_engine.md "wikilink")（遊戲作弊軟件）

  - The Core Media Player（顧名思義，此為音樂播放器）

  - [Media Player
    Classic](../Page/Media_Player_Classic.md "wikilink")（顧名思義，此皆為音樂播放器）

  - （按鍵精靈）

  - [Tsearch](../Page/Tsearch.md "wikilink")（一個[記憶體掃瞄軟件及調試器](../Page/記憶體.md "wikilink")）

  - [NetLimiter](../Page/NetLimiter.md "wikilink")（一個管理網絡流量軟件）

  - [Symantec Endpoint
    Protection](../Page/Symantec_Endpoint_Protection.md "wikilink")

  - Online Armor

  - [ClubBox](../Page/ClubBox.md "wikilink")（一種傳輸軟體）

  - [GoGoBox](../Page/GoGoBox.md "wikilink")（近似於Clubbox 的傳輸軟體）

  - [易语言及由其编写的任何程序](../Page/易语言.md "wikilink")（一种在中国大陆有一定流传的汉语编程工具）

  - [Google
    Chrome](../Page/Google_Chrome.md "wikilink")（一種網頁瀏覽器軟體）。\[13\]

  - [Yahoo\! Messenger](../Page/Yahoo!_Messenger.md "wikilink")
    (使用時無法登入)

  - [Visual Basic](../Page/Visual_Basic.md "wikilink") 系列
    (VB編譯器以及所編繹出之程式無法運行)

  - [Skype](../Page/Skype.md "wikilink")

  - [RC語音](../Page/RC語音.md "wikilink")

  - 部份遠控軟體(例如:[:TeamViewer](../Page/:TeamViewer.md "wikilink"),等等..)(GameGuard啟動時,目標電腦無法被遠端控制,但可以看到畫面)

  - [Windows 7小工具有機率顯示異常](../Page/Windows_7.md "wikilink")

## 已知 GameGuard 對作業系統的危害

部份已知 GameGuard 對作業系統照成的危害有：
\*[Volume Mixer](../Page/Volume_Mixer.md "wikilink") 崩潰

  - 使正版 [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
    作業系統激活失效
  - 使 Microsoft Windows 作業系統直接進入藍白當機畫面
  - 永久破壞 Microsoft Windows 系統，導致無法啟動，必須重新安裝Microsoft Windows

## 參考文獻

## 參見

  - [nProtect](../Page/nProtect.md "wikilink")
  - [PunkBuster](../Page/PunkBuster.md "wikilink")
  - [HackShield](../Page/HackShield.md "wikilink")
  - [遊戲作弊](../Page/遊戲作弊.md "wikilink")
  - [網絡遊戲](../Page/網絡遊戲.md "wikilink")
  - [jp:nProtect](../Page/jp:nProtect.md "wikilink")
  - [jp:ゲームガード](../Page/jp:ゲームガード.md "wikilink")

## 外部連結

### 官方網頁

  - [nProtect
    GameGuard官方網站](https://web.archive.org/web/20110828112414/http://global.nprotect.com/index.php)

  - [nProtect
    GameGuard網絡遊戲用戶通報中心](http://www.gameguard.jp/user/user.asp)

  - [韓國nProtect網站](http://www.nprotect.com/index.html)

  - [日本nProtect網站](http://nprotect.jp/)

  - [INCA互聯網官方網站](http://www.inca.co.kr/)

  - [中文GameGuard問答集](https://web.archive.org/web/20090822032540/http://www.gameguard.co.kr/gameguard/faq/tw/index.htm)

:\*[nP錯誤代號114解決方法](http://www.gameguard.co.kr/gameguard/faq/tw/FAQ_114.htm)

:\*[nP錯誤代號3XX解決方法](http://www.gameguard.co.kr/gameguard/faq/cn/FAQ_3xx.htm)

:\*[錯誤紀錄ERL檔案回報機制](http://www.gameguard.co.kr/gameguard/faq/cn/FAQ_erl.htm)

### 玩家社群

  - [nProtect
    保安](http://blog.aboutchennai.com/2006/11/26/gameguard-nprotect-a-rootkit-underground-exploits-have-been-released-that-allow-attackers-to-compromise-security-of-the-victims-computer/)

[ja:NProtect\#nProtect
GameGuard](../Page/ja:NProtect#nProtect_GameGuard.md "wikilink")

[N](../Category/反作弊软件.md "wikilink")

1.
2.  [日本版夢之歷險關於GameGuard刪除程式之公告](http://nostale.jalecoonline.jp/topics.php?tt_seq=74)
    ，2007年3月8日

3.  詳見台灣[楓之谷官方網頁](../Page/楓之谷.md "wikilink")，[於2007年4月15日](http://tw.maplestory.gamania.com/BullentinDetail.aspx?id=2766)
    及
    [12月20日的](http://tw.maplestory.gamania.com/BullentinDetail.aspx?id=3443)
    「GameGuard更新問題公告」。

4.  [魔物獵人關於nProtect
    GameGuard問題之公告](http://members.mh-frontier.jp/information/?id=0440)、Capcom魔物獵人日本官方網頁，2007年8月17日

5.  詳見 [香港巨商官方網頁](http://www.gamania.com.hk/gersang/index.asp)
    於2007年8月28日的「有關當機情況說明」公告。

6.  詳見香港拿拿林Online官方討論版的
    [拿拿林問題回報區](http://forum.nanaimo.com.hk/forumdisplay.php?fid=9)


7.  [《R2》會員突破20萬
    攻城戰等你決定時間](http://gnn.gamer.com.tw/4/27504.html)，巴哈姆特電玩資訊站，2007年8月6日

8.  [《十二之天》宣佈採用防外掛前導程式
    nProtect](http://gnn.gamer.com.tw/9/27399.html)，[巴哈姆特電玩資訊站](../Page/巴哈姆特電玩資訊站.md "wikilink")，2007年7月26日

9.  [哈韓風潮的創造者---因思銳(下)](http://gnn.gamer.com.tw/5/5185.html)，巴哈姆特電玩資訊站，2002年8月2日

10. [《黑色陰謀Online》幕後探秘](http://gnn.gamer.com.tw/7/25127.html)，巴哈姆特電玩資訊站，2006年11月15日

11. [河智元美麗重生「精靈II」](http://gnn.gamer.com.tw/6/13006.html)，巴哈姆特電玩資訊站，2003年12月15日

12. [NBA2K Online官方網站](http://nba2kol.competgames.com/)

13. [Google Chrome 的沙盒機制與 GameGuard
    的阻擋沙盒偵測技術發生衝突。](http://www.google.com/support/forum/p/chrome/thread?tid=40e485e123c4f087&hl=zh-TW)
    Google 說明論壇