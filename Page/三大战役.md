[Three_Campaigns_of_Chinese_Civil_War.png](https://zh.wikipedia.org/wiki/File:Three_Campaigns_of_Chinese_Civil_War.png "fig:Three_Campaigns_of_Chinese_Civil_War.png")

**三大會戰**，[中國共產黨称为](../Page/中國共產黨.md "wikilink")**三大戰役**，指的是在1948年9月至1949年1月間，[中国人民解放军與](../Page/中国人民解放军.md "wikilink")[中華民國國軍之間在](../Page/中華民國國軍.md "wikilink")[第二次國共内戰中的三次關鍵](../Page/第二次國共内戰.md "wikilink")[戰役](../Page/戰役.md "wikilink")。三次會戰是指[遼西會戰](../Page/遼西會戰.md "wikilink")、[徐蚌會戰与](../Page/徐蚌會戰.md "wikilink")[平津會戰](../Page/平津會戰.md "wikilink")，均以中國共產黨的胜利告终。

1947年7月，解放軍轉入戰略進攻\[1\]。由1948年起，解放軍在三大战役结束后，國軍損失超過150万，精锐部队丧失殆尽，[中國国民党在中國大陆的统治逐漸崩溃](../Page/中國国民党.md "wikilink")。而[中國共產黨勢力經年累月積攢](../Page/中國共產黨.md "wikilink")，及在抗战中磨练的灵活的游击战术之優勢完全顯現，開始向全中國进一步进军。以至1950年打下[海南島與](../Page/海南島.md "wikilink")[西康](../Page/西康.md "wikilink")，中國共產黨統治整個中國大陸。

## 遼瀋會戰（辽沈战役）

从1948年9月12日至11月2日（52天），[林彪](../Page/林彪.md "wikilink")、[罗荣桓率领的](../Page/罗荣桓.md "wikilink")[东北野战军向](../Page/东北野战军.md "wikilink")[中國東北的](../Page/中國東北.md "wikilink")[锦州](../Page/锦州.md "wikilink")、[长春等地發起進攻](../Page/长春.md "wikilink")，与國軍[東北剿匪總司令部](../Page/東北剿匪總司令部.md "wikilink")(總司令[衛立煌](../Page/衛立煌.md "wikilink"))麾下的主力部隊进行了五十二天的作战，國軍傷亡被俘共计47万2千餘人，東北全境為中共所佔，从而使解放军之優勢顯現，此战役称为**辽沈战役**，國府方面称作「**遼瀋會戰**」。\[2\]

## 徐蚌會戰（淮海战役）

从1948年11月6日至1949年1月10日（66天），[刘伯承](../Page/刘伯承.md "wikilink")、[陈毅](../Page/陈毅.md "wikilink")、[粟裕](../Page/粟裕.md "wikilink")、[邓小平等率领的](../Page/邓小平.md "wikilink")[华东野战军和](../Page/华东野战军.md "wikilink")[中原野战军](../Page/中原野战军.md "wikilink")60万人，在淮河、[海州一线附近与国军](../Page/海州.md "wikilink")[徐州剿匪总司令部](../Page/徐州剿匪总司令部.md "wikilink")(總司令[劉峙](../Page/劉峙.md "wikilink"))所屬的主力部隊和[华中剿匪总司令部](../Page/华中剿匪总司令部.md "wikilink")(總司令[白崇禧](../Page/白崇禧.md "wikilink"))所屬的增援部队共80万人进行战略决战，以伤亡13万4千余人的代价，消灭及改编國軍[黄百韬](../Page/黄百韬.md "wikilink")、[黄维](../Page/黄维.md "wikilink")、[邱清泉](../Page/邱清泉.md "wikilink")、[李弥](../Page/李弥.md "wikilink")、[孙元良等部](../Page/孙元良.md "wikilink")55万5千人。战役结束后，解放军控制了长江以北的广大地区，直接威脅首都[南京](../Page/南京.md "wikilink")。此战役称为淮海战役，历时65天，奠定了解放长江以南各省的基础，對中國形勢有深遠影響。粟裕在此战役中战功卓著，毛泽东评“粟裕同志立了第一功”。

## 平津會戰（平津战役）

从1948年12月5日至1949年1月31日（64天），林彪、羅榮桓与[聶榮臻等人率东北野战军和](../Page/聶榮臻.md "wikilink")[华北野战军共](../Page/华北野战军.md "wikilink")100万，包围了國軍[华北剿匪总司令部](../Page/华北剿匪总司令部.md "wikilink")(總司令[傅作义](../Page/傅作义.md "wikilink"))統領之50餘萬國軍，先后通过新保安、天津等战役，消灭了国军部分主力，最终解放军与傅作义达成协议，傅作义开城投降。通过这次战役，國軍[陈长捷](../Page/陈长捷.md "wikilink")、[傅作义等部共](../Page/傅作义.md "wikilink")52万餘人被歼灭及改编，解放军佔領了[北平](../Page/北平.md "wikilink")、[天津及](../Page/天津.md "wikilink")[張家口等地](../Page/張家口.md "wikilink")。这一战役被称为[平津战役](../Page/平津會戰.md "wikilink")。

## 参考文献

{{-}}

[三大战役](../Category/三大战役.md "wikilink")
[Category:1948年国共内战战役](../Category/1948年国共内战战役.md "wikilink")
[Category:1949年国共内战战役](../Category/1949年国共内战战役.md "wikilink")

1.  中國大百科全書總編輯委員會《軍事》編輯委員會，《中國大百科全書•軍事I》，北京，中國大百科全書出版社，1989年5月，第472頁，ISBN
    7500002424
2.  [行政院退輔會榮民文化網--遼瀋會戰](http://lov.vac.gov.tw/Protection/Content.aspx?i=25&c=3)