**基隆港**是位於[臺灣北部](../Page/臺灣.md "wikilink")[基隆市的](../Page/基隆市.md "wikilink")[海港](../Page/港口.md "wikilink")，為[臺灣四座國際](../Page/臺灣.md "wikilink")[商港之一](../Page/商港.md "wikilink")、以及[北臺灣首要的](../Page/北臺灣.md "wikilink")[海運樞紐](../Page/海運.md "wikilink")，營運機構為[臺灣港務公司](../Page/臺灣港務公司.md "wikilink")[基隆港務分公司](../Page/臺灣港務股份有限公司基隆港務分公司.md "wikilink")。整個[港區被基隆](../Page/港區.md "wikilink")[市中心環繞](../Page/市中心.md "wikilink")，運輸方面以[貨櫃為主](../Page/貨櫃.md "wikilink")、散貨為輔，並有數條國內外[客輪航線固定彎靠](../Page/客輪.md "wikilink")。至2010年止，基隆港為世界第54大貨櫃港及臺灣第三大港。

## 港區環境及設施

[基隆港](https://www.taxicar-trip.com/)為利用自然之[谷灣地形所修築的港口](../Page/谷灣.md "wikilink")，共有57座[碼頭](../Page/碼頭.md "wikilink")、1個入港航道，港區總面積為572.17[公頃](../Page/公頃.md "wikilink")。[港灣形狀大致呈北寬南窄的漏斗狀](../Page/港灣.md "wikilink")，港區水域則分為外港、內港、牛稠港等3大區域。碼頭方面可分為東岸碼頭、西岸碼頭等2大部分，其中以西岸運量較大；使用類型則分為商用、軍用、漁用等3大類。
[KeelungHarbor_west_002.JPG](https://zh.wikipedia.org/wiki/File:KeelungHarbor_west_002.JPG "fig:KeelungHarbor_west_002.JPG")
[Keelung_Harbor_West_Passengers_Terminal_20120526.jpg](https://zh.wikipedia.org/wiki/File:Keelung_Harbor_West_Passengers_Terminal_20120526.jpg "fig:Keelung_Harbor_West_Passengers_Terminal_20120526.jpg")

### 碼頭簡介

  - 貨櫃碼頭：15座

<!-- end list -->

  -
    設有三處貨櫃基地，配置有35至40噸可裝卸13至18排貨櫃之高性能式[貨櫃](../Page/貨櫃.md "wikilink")[起重機](../Page/起重機.md "wikilink")
    ，每年可裝卸200萬至300萬標準貨櫃箱（TEU）。
    1.  西岸貨櫃儲運場
        1.  北櫃場（舊稱第一貨櫃中心）
        2.  南櫃場（舊稱第三貨櫃中心）
    2.  東岸貨櫃儲運場（舊稱第二貨櫃中心）

<!-- end list -->

  - 雜貨碼頭：24座

<!-- end list -->

  -
    設置有通棧、露置堆貨場，可供[汽車](../Page/汽車.md "wikilink")、[遊艇](../Page/遊艇.md "wikilink")、[鋼鐵等](../Page/鋼鐵.md "wikilink")[貨物裝卸之用](../Page/貨物.md "wikilink")。另設有[水泥](../Page/水泥.md "wikilink")、卸[煤](../Page/煤.md "wikilink")、[油品及其他散貨碼頭](../Page/石油.md "wikilink")，配置水泥圓庫、自動卸煤機、化油儲槽、自動卸水泥設施等。

<!-- end list -->

  - 客運碼頭：2座

<!-- end list -->

  -
    東岸及西岸各有一座（東2碼頭、西2碼頭），各設有一棟客運大廈。經常有國內外郵輪、客貨輪在此靠泊。

<!-- end list -->

  - 其他用途碼頭：16座

<!-- end list -->

  -
    分別供港務公司所有港勤船，及工程船、軍艦、海巡艦、緝私艦等特種船隻靠泊。

### 軍港

基隆港東岸與西岸均有軍用碼頭。東岸為東5碼頭（入口標示為「海軍碼頭」）；東5碼頭入口對面設有海軍威海營區，是[中華民國海軍](../Page/中華民國海軍.md "wikilink")[一三一艦隊部](../Page/海軍一三一艦隊.md "wikilink")[所在地](../Page/海軍基地.md "wikilink")；西岸則為西5—6與9—11B碼頭，由[海軍基隆後勤支援指揮部](../Page/海軍基隆後勤支援指揮部.md "wikilink")（原海軍第三造船廠，簡稱「海三廠」）、[陸軍第三地區支援指揮部運輸兵群第二營使用](../Page/陸軍後勤指揮部.md "wikilink")。

### 漁港

大沙灣以北，與[和平島](../Page/和平島.md "wikilink")、[八尺門鄰近的水域為基隆港的](../Page/八尺門.md "wikilink")[漁用碼頭區](../Page/渔业.md "wikilink")（1998年正式劃為「漁業專用區」），又名「[正濱漁港](../Page/正濱漁港.md "wikilink")」，為台灣重要[遠洋漁業基地之一](../Page/遠洋漁業.md "wikilink")。

<File:Keelung> Boats Pier 20080322.jpg|基隆港地標。 <File:Keelung> Maritime
Plaza 20110103.jpg|[基隆海洋廣場一景](../Page/基隆海洋廣場.md "wikilink")。
<File:K602>, Transport Ship of Port of Keelung, TIPC at Keelung
20131227a.jpg|臺灣港務股份有限公司基隆港務分公司（原基隆港務局）交通船（遊艇）基602航行於基隆港中，攝於港務大樓北側停車場。
<File:ROCN> Yi Yang (FFG-939) in Keelung Port Pushing by Tugboat YTL49
20140327.jpg|[濟陽級巡防艦宜陽](../Page/濟陽級巡防艦.md "wikilink")（FFG-939）接近基隆港東4號碼頭泊位，艦尾海軍港務拖船YTL49協助轉向。
<File:Keelung> Blueway No.1 Atami Cruise Leaving Port
20120526a.jpg|基隆藍色公路熱海壹號遊艇離岸啟航，右方綠色高樓是[長榮桂冠酒店](../Page/長榮桂冠酒店.md "wikilink")（基隆）。
<File:Rubber> Duck and Keelung Maritime Plaza View from East
20131227a.jpg|基隆港東岸看臺西望[黃色小鴨與海洋廣場](../Page/黃色小鴨.md "wikilink")。
<File:Voyager> of the Seas and Sapphire Princess Shipped in Keelung
Harbor 20140518.jpg|停泊於基隆港東1號、2號碼頭的皇家加勒比海郵輪海洋航行者號（前）與公主郵輪藍寶石公主號（後）。
<File:ROCN> Tugboats YTL45 and YTL49 Go to Home Pier after Work
20140327.jpg|海軍港務拖船YTL49（左）、YTL45（右）。
<File:Taroko-express-unloading.jpg>|[台鐵](../Page/台鐵.md "wikilink")[太魯閣列車於基隆港碼頭卸船](../Page/太魯閣列車.md "wikilink")。
<File:Keelung> city02.jpg|基隆港一景。 <File:Keelung> Chungcheng
Park.JPG|從基隆中正公園俯瞰基隆港。 <File:Lodbrog> at Port of Keelung
20090517.jpg|法國籍羅布輪（Lodbrog）電纜維修船停泊於基隆港。 <File:Nautica> at
Keelung.jpg|諾蒂卡號（Nautica）停泊於基隆港。 <File:Hiryu21> at
Keelung.jpg|飛龍號（Hiryu21）停泊於基隆港。 <File:Volvox> Asia at
Keelung.jpg|Volvox Asia浚挖船停泊於基隆港。 <File:SuperStar> Aquarius left in
Keelung 20131013.jpg|麗星郵輪寶瓶星號停泊基隆港。 <File:Natzutec> Shipped in Keelung
Harbor 20131227a.JPG|停泊於基隆港西岸碼頭的上海有田海運（Shanghai Anrita Shipping Co.,
Ltd）香港籍散裝貨輪Natzutec。

### 港區地圖

## 聯外交通

### 鐵路

  - [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")[台鐵](../Page/台鐵.md "wikilink")
    - [基隆臨港線](../Page/基隆臨港線.md "wikilink")（已廢除\[1\]）
  - [ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")[台鐵](../Page/台鐵.md "wikilink")
    - [縱貫線](../Page/縱貫線_\(北段\).md "wikilink") -
    [基隆車站](../Page/基隆車站.md "wikilink")

### 高快速公路

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[中山高速公路](../Page/中山高速公路.md "wikilink")
      - [港西高架橋](../Page/基隆港西高架橋.md "wikilink")：連接中山高[基隆端及西岸碼頭](../Page/基隆端.md "wikilink")、中山一路
      - [東岸高架道路](../Page/東岸高架道路.md "wikilink")（中正高架橋）：連接中山高基隆端及東岸碼頭、中正路（[北部濱海公路](../Page/北部濱海公路.md "wikilink")）
  - [TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[福爾摩沙高速公路](../Page/福爾摩沙高速公路.md "wikilink")
      - 基隆港西岸聯外道路（[台2己線](../Page/台2線.md "wikilink")）：連接福高及西岸碼頭
  - [TW_PHW62a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW62a.svg "fig:TW_PHW62a.svg")[基隆港東岸聯外道路](../Page/台62線#甲線.md "wikilink")
      - [台62線](../Page/台62線.md "wikilink")：於[四腳亭交流道連接台](../Page/四腳亭交流道.md "wikilink")62甲線，可接上中山高及福高
      - [台2丁線](../Page/台2線#丁線.md "wikilink")：於四腳亭交流道連接台62甲線

### 一般幹道

  - 東岸沿岸：[中正路](../Page/中正路.md "wikilink")、東海街
  - 西岸沿岸：中山一路、中山二路、中山三路、光華路
  - 市區銜接：[港西街](../Page/港西街.md "wikilink")、[忠一路](../Page/忠一路.md "wikilink")、[仁二路](../Page/仁二路.md "wikilink")
  - 聯外幹線：[台2線](../Page/台2線.md "wikilink")（北部濱海公路）、[台5線](../Page/台5線.md "wikilink")（[北基公路](../Page/台北市.md "wikilink")）、[縣道102線](../Page/縣道102線.md "wikilink")（基[瑞公路](../Page/瑞芳區.md "wikilink")）

## 營運航線

  - [麗星郵輪](../Page/麗星郵輪.md "wikilink") 「寶瓶星號」

基隆港 → [石垣港](../Page/石垣港.md "wikilink") →
[那霸港](../Page/那霸港.md "wikilink") → 基隆港

基隆港 → [那霸港](../Page/那霸港.md "wikilink") → 基隆港

基隆港 → [石垣港](../Page/石垣港.md "wikilink") → 基隆港

基隆港 → [與那國島](../Page/與那國島.md "wikilink")(久部良漁港)→ 基隆港

基隆港 → [花蓮港](../Page/花蓮港.md "wikilink") →
[與那國島](../Page/與那國島.md "wikilink")(久部良漁港) → 基隆港

  - [中國遠洋](../Page/中國遠洋.md "wikilink") 「遠洋之星」

[廈門港](../Page/厦门港.md "wikilink") → 基隆港 →
[台州港](../Page/台州港.md "wikilink")(大麥嶼港區)

※每週一班、每週五出發

[台州港](../Page/台州港.md "wikilink")(大麥嶼港區) → 基隆港 →
[廈門港](../Page/廈門港.md "wikilink")

※每週一班、每週日出發

  - 新華航業 「台馬之星」

基隆港 → [福澳港](../Page/福澳港.md "wikilink")([馬祖](../Page/馬祖.md "wikilink")) →
中柱港([東引島](../Page/東引島.md "wikilink"))

## 發展歷程

[Keelung-Garnot-Kelung-1894.jpg](https://zh.wikipedia.org/wiki/File:Keelung-Garnot-Kelung-1894.jpg "fig:Keelung-Garnot-Kelung-1894.jpg")
[Map_of_Kiirun(Keelung)_City_02.jpg](https://zh.wikipedia.org/wiki/File:Map_of_Kiirun\(Keelung\)_City_02.jpg "fig:Map_of_Kiirun(Keelung)_City_02.jpg")
[Kiirun_1_10000_1945.jpg](https://zh.wikipedia.org/wiki/File:Kiirun_1_10000_1945.jpg "fig:Kiirun_1_10000_1945.jpg")

基隆港在昔日稱為「**雞籠港**」或「**雞籠灣**」。雞籠港在[十七世紀即有外人足跡](../Page/十七世紀.md "wikilink")，[西班牙人](../Page/西班牙.md "wikilink")[佔領臺灣時就曾對雞籠港進行調查](../Page/台灣荷西殖民時期.md "wikilink")，並進行了部分建設。[清治後期](../Page/台灣清治時期.md "wikilink")，西方列強的東來，逐漸開啟了雞籠港的發展。在[同治](../Page/同治.md "wikilink")2年（1863年），基隆港以[淡水附港的名義正式開放為商港](../Page/淡水區.md "wikilink")。而在1886年當時就任[台灣巡撫的](../Page/台灣巡撫.md "wikilink")[劉銘傳進行基隆港的建港規劃](../Page/劉銘傳.md "wikilink")，並委由當時的台灣首富[林維源總辦基隆港的建港事宜](../Page/林維源.md "wikilink")，也將當時興建中的[縱貫鐵路分出支線至港區](../Page/縱貫線.md "wikilink")；但後來建港工程因劉銘傳的離職而未全面實行，主要設施中只完成一座陸海聯運碼頭。後來統治台灣的[中華民國政府](../Page/中華民國政府.md "wikilink")，將1886年認定為基隆港建立之年\[2\]。

[日治時代](../Page/台灣日治時期.md "wikilink")，[日本當局計劃將基隆建設成為台灣與](../Page/台灣總督府.md "wikilink")[日本](../Page/日本.md "wikilink")[本土的聯絡門戶](../Page/內地.md "wikilink")，故基隆港正式開始進行現代化港口的建設，也就是從1899年到1944年間共五期的基隆港築港工程（第五期工程因[二戰爆發而未全部完工](../Page/第二次世界大戰.md "wikilink")），清除了原本密佈內港區內的礁石，並在外港陸續整建了大型造船廠及[軍港](../Page/軍港.md "wikilink")、[漁港區等設施](../Page/漁港.md "wikilink")，從碼頭貨棧到港區[鐵路系統皆相當完備](../Page/鐵路.md "wikilink")（現存的[西二西三碼頭倉庫即在此時期興建](../Page/基隆港西岸碼頭倉庫.md "wikilink")）。這五期的築港工程，不但奠定了日後基隆港的發展基礎，也使基隆港在1970年代前穩坐台灣第一大港寶座。較特別的是，基隆港的築港工程並不由政府行政系統負責，而是由[日本軍方主導](../Page/日軍.md "wikilink")，並被列為[日本海軍列管軍港之一](../Page/日本海軍.md "wikilink")。

基隆港原由其所在的[台北州設立](../Page/台北州.md "wikilink")「港務部」來管理，並由[台灣總督府](../Page/台灣總督府.md "wikilink")[交通局海務部監督](../Page/台灣總督府交通局海務部.md "wikilink")，1941年[太平洋戰爭爆發後](../Page/太平洋戰爭.md "wikilink")，改由直屬於台灣總督府的[基隆港務局負責](../Page/基隆港務局.md "wikilink")。基隆港由於當時台灣主要物資吞吐港及海軍基地的地位，在大戰末期首當其衝，成為[美軍轟炸的首要目標](../Page/美軍.md "wikilink")。港埠設施及港內停泊船隻皆毀損嚴重，港區幾成廢墟。

<File:Kiirun(Keelung)> Harbor on postcard 02.jpg <File:Kiirun(Keelung)>
Harbor on postcard 03.jpg <File:Kiirun(Keelung)> Harbor on postcard
01.jpg <File:Keelung> Harbor 03.jpg <File:Keelung> Harbor 04.jpg
<File:Keelung> Harbor 05.jpg <File:Keelung> Harbor 02.jpg <File:Keelung>
Harbor 01.jpg|日治時期基隆港明信片

1945年，[中華民國接管臺灣](../Page/臺灣戰後時期.md "wikilink")，續設基隆港務局，隸屬於[台灣省政府交通處](../Page/臺灣省政府.md "wikilink")。港務局改組時以[打撈港內一百多艘沉船及復建](../Page/打撈.md "wikilink")[碼頭](../Page/碼頭.md "wikilink")、橋樑、船渠、船塢、運河、防波堤、修理場及倉庫等原有設施為主，1953年以後才開始增設港埠設施，如增建西29、30號碼頭、漁港區突堤碼頭、興建通棧倉庫等，並填築大沙灣海水浴場以興建船渠。

復建完成之後，基隆港開始復興。1961年至1974年間，港務局改建內港設施，並開始在外港及東岸興築碼頭\[3\]。1974年至1981年間開始積極擴建外港，並先後興建兩座大型突堤碼頭，以消除內港瓶頸及島內成長的運輸需求；連接港區及[高速公路的東岸](../Page/中山高速公路.md "wikilink")、西岸高架橋也興建於同時期。1982年至1992年，為因應貨櫃運輸時代的來臨，以改建及增建貨櫃碼頭為主。基隆港的營運量在1980年代到達高峰，到了1984年，基隆港更成為世界第七大貨櫃港。

發展至今，基隆港的碼頭總數從[日治時期全座落在西岸的](../Page/台灣日治時期.md "wikilink")18座，擴增為現在的57座（西岸37座、東岸20座）。[民國](../Page/民國紀元.md "wikilink")95年（2006年）年度全[中華民國](../Page/中華民國.md "wikilink")[關稅總收入之中](../Page/關稅.md "wikilink")，經基隆港之收入為[新台幣](../Page/新台幣.md "wikilink")821億餘元，佔總收入之60.54％\[4\]，足見基隆港在台灣經濟體系的重要地位。

基隆港的營運機構為[臺灣港務公司基隆港務分公司](../Page/臺灣港務股份有限公司基隆港務分公司.md "wikilink")。其前身為基隆港務局，最初為[臺灣省政府附屬之事業機構](../Page/臺灣省政府.md "wikilink")，1999年[精省後改隸](../Page/精省.md "wikilink")[中華民國交通部](../Page/中華民國交通部.md "wikilink")；2012年[行政院組織再造實施後](../Page/行政院組織再造.md "wikilink")，中華民國交通部於同年3月1日起實施「政企分離」之航港管理作法，將各港務局公司化合併成立[國營之](../Page/國營.md "wikilink")「臺灣港務公司」，原基隆港務局則改制為基隆港務分公司。

## 發展瓶頸與對策

[Ym_People_at_Keelung.jpg](https://zh.wikipedia.org/wiki/File:Ym_People_at_Keelung.jpg "fig:Ym_People_at_Keelung.jpg")貨櫃船\]\]
基隆港與基隆市唇齒相依，基隆市區的街廓即沿著港區發展，而早期基隆市民的生計更是與基隆港息息相關，昔日廣佈市區的「[委託行](../Page/委託行.md "wikilink")」及[酒吧就是一例](../Page/酒吧.md "wikilink")\[5\]。但進入1990年代之後，基隆港除了要面對國內外傳統港口的競爭，還要應付[中國大陸東南各新興港口的快速崛起](../Page/中國大陸.md "wikilink")。而基隆港的港區規模已到達極限，卻因為港埠用地緊鄰市區及山區而無法擴建，港內大部分碼頭的吃水深度又無法停泊巨型貨櫃輪，導致許多船隻被迫彎靠[高雄港與](../Page/高雄港.md "wikilink")[台中港](../Page/台中港.md "wikilink")。

### 興建新港

基隆港於1980年代，正逢港埠營運的高峰期，當時港內船席不足的情形常常發生；基隆港務局於1980年代中期，為了解決基隆港的擁擠狀況，提出在基隆港以西的[外木山海岸興建](../Page/外木山.md "wikilink")「基隆超級深水港」，即「基隆新港」的興建計畫，在基隆港營運狀況開始衰弱時，曾被認為是重振基隆港營運的最佳方法，但是之後一直無實際動作。在1997年，交通部以建設新港的難度過高、經費太過龐大等理由，否決了興建新港的提案，以在[新北市](../Page/新北市.md "wikilink")[八里區興建淡水新港](../Page/八里區.md "wikilink")（即今[台北港](../Page/台北港.md "wikilink")）作為替代方案。此舉引起了基隆市各界的不滿，認為此舉將會封殺了基隆港，甚至基隆市的未來發展。雖然如此，基隆市各界目前仍未放棄興建新港的可能。

### 開放觀光

近年來，為了提升競爭力，基隆港開始轉型為結合[觀光](../Page/觀光.md "wikilink")、親水性之港口。港務局在2002年正式開放港區觀光，讓遊客可以搭船遊覽基隆的港埠風光。另外，為了吸引國際航運業者落腳，基隆港在2003年時設立了[自由貿易港區](../Page/自由港.md "wikilink")\[6\]。

### 港市合一

「[港市合一](../Page/港市合一.md "wikilink")」的構想在1970年代時已經存在，\[7\]在1989年時已是選舉政見要點。\[8\]為了促進基隆市港兩方的發展，基隆市各界自1990年代開始積極推動。2001年6月28日，港市合一的專責機構「基隆港管理委員會」正式成立，由[基隆市市長](../Page/基隆市市長.md "wikilink")（時任市長為[李進勇](../Page/李進勇.md "wikilink")）兼任主任委員。但交通部於2002年2月21日，函各港管理委員會暫行停止委員會運作\[9\]。

## 未來展望

由於現有港區短期內無法擴建，為了因應台北港將對基隆港的[貨運業務造成的威脅](../Page/貨運.md "wikilink")、與鄰近國家港口的競爭，以及[兩岸三通後所帶來的](../Page/兩岸三通.md "wikilink")[客運榮景](../Page/客運.md "wikilink")，基隆港將從原本偏重[貨物運輸](../Page/貨物.md "wikilink")，轉型成為客運、貨運並重的「加值型物流港」\[10\]\[11\]。未來基隆港將朝向「東客西貨」的方向來發展，東岸以客運與觀光遊憩為主，西岸則繼續作為貨運運輸的基地\[12\]；而配合基隆市中心的[都市更新計畫](../Page/都市更新.md "wikilink")，未來基隆港部分碼頭的功能將進行調整，港區部分[後線區域也將開放商業開發](../Page/後線.md "wikilink")，以達致基隆港、市發展之雙贏\[13\]。

下列為基隆港進行中的發展計畫：
[New_Keelung_Harbor_Service_Building_model_20131013_1.jpg](https://zh.wikipedia.org/wiki/File:New_Keelung_Harbor_Service_Building_model_20131013_1.jpg "fig:New_Keelung_Harbor_Service_Building_model_20131013_1.jpg")

  - **市中心港區開發計畫**

<!-- end list -->

  -
    基隆港南岸的港區（東4碼頭—西4碼頭之間）鄰近市中心，為配合[基隆市政府推動的](../Page/基隆市政府.md "wikilink")[基隆火車站暨西二西三碼頭都市更新案](../Page/基隆火車站暨西二西三碼頭都市更新案.md "wikilink")，鄰近[基隆車站的小艇碼頭](../Page/基隆車站.md "wikilink")、西1碼頭、西1B碼頭的後線將解除管制，成為向公眾開放的水岸廣場（碼頭岸線仍繼續維持現有功能）\[14\]。西2—西4碼頭將配合此都更案改造成為「客運專區」，並新建容納客運航廈、港務機構[合署辦公之新港務大樓](../Page/合署辦公.md "wikilink")，將成為基隆港新地標\[15\]；東2—東4碼頭則規劃成為國際[郵輪基地](../Page/郵輪.md "wikilink")，並引進民間資金來開發\[16\]。新港務大樓原規劃於2016年完工啟用\[17\]，但因計畫拆除做為其建地的[西三碼頭倉庫在](../Page/基隆港西岸碼頭倉庫.md "wikilink")2014年底列為[歷史建築保護](../Page/歷史建築.md "wikilink")，使整個工程必須重新規劃，目前規劃西二、三碼頭倉庫改為旅客中心，海港大樓合併至規劃興建的西岸會展與旅運智慧大樓\[18\]。

<!-- end list -->

  - **牛稠港軍事專業區**

<!-- end list -->

  -
    [中華民國國防部已在](../Page/中華民國國防部.md "wikilink")[2011年6月同意將](../Page/2011年6月.md "wikilink")[海軍原有在基隆港西岸呈分散狀的](../Page/中華民國海軍.md "wikilink")5處軍用碼頭集中至牛稠港水域的西9—11碼頭，並在此劃設「軍事專業區」，以利港區[棧埠與後線的整合](../Page/棧埠.md "wikilink")，預計2013年6月完成\[19\]。

<!-- end list -->

  -
    此外，臺灣港務公司亦規劃將海軍使用的東5碼頭收回自用，以使基隆港所有的軍用碼頭都能遷移至牛稠港軍事專業區；但海軍以東5碼頭的戰略地位無法取代而拒絕。未來如得以遷移，該軍事專業區將擴大至西5碼頭\[20\]。

## 姐妹港

  - [奧克蘭港](../Page/奧克蘭港.md "wikilink")（Port of Oakland）- 1970年6月10日締結

  - [南安普敦港](../Page/南安普敦港.md "wikilink")（Port of Southampton）-
    1985年4月16日締結

  - [洛杉磯港](../Page/洛杉磯港.md "wikilink")（Port of Los Angeles）-
    1988年7月28日締結

  - [北臨海港](../Page/北臨海港.md "wikilink")（Port of Bellingham）- 1990年8月15日締結

  - [舊金山港](../Page/舊金山港.md "wikilink")（Port of San Francisco）-
    1992年9月15日締結

## 參見

  - [臺灣港務股份有限公司基隆港務分公司](../Page/臺灣港務股份有限公司基隆港務分公司.md "wikilink")
  - [基隆港西岸碼頭倉庫](../Page/基隆港西岸碼頭倉庫.md "wikilink")
  - [台灣港口](../Page/台灣港口.md "wikilink")
  - [基隆市交通](../Page/基隆市交通.md "wikilink")

## 参考文献

### 引用

### 来源

  - 重修《基隆市志》- 交通篇，基隆市政府印行

  - [《基隆外海新港區細部規劃及初步設計》研究報告](http://www.iot.gov.tw/ct.asp?xItem=5258&ctNode=611)，交通部運輸研究所發行

  -
## 外部連結

  - [台灣港務公司基隆港分公司](http://kl.twport.com.tw/)

  - [基隆港務警察總隊](http://www.klhpb.gov.tw/)

  - [基隆港務消防隊](http://www.klhfd.gov.tw/)

  - [中央氣象局24H全天監視基隆港即時影像](http://www.cwb.gov.tw/V7/observe/webcam/index.htm)

  - [世界主要港口列表](https://web.archive.org/web/20071015074210/http://iaphworldports.org/members_profile/regular/index.html)

{{-}}

[Category:1913年台灣建立](../Category/1913年台灣建立.md "wikilink")
[Category:1913年完工港口](../Category/1913年完工港口.md "wikilink")
[Category:1913年啟用的港口](../Category/1913年啟用的港口.md "wikilink")
[Category:台灣商港](../Category/台灣商港.md "wikilink")
[基隆港](../Category/基隆港.md "wikilink")
[港](../Category/基隆市港口.md "wikilink")
[港](../Category/基隆市交通.md "wikilink")
[港](../Category/基隆市水路.md "wikilink")
[港](../Category/基隆市經濟.md "wikilink")
[Category:基隆市旅遊景點](../Category/基隆市旅遊景點.md "wikilink")
[Category:基隆北海岸景點](../Category/基隆北海岸景點.md "wikilink")
[Category:中華民國海軍基地](../Category/中華民國海軍基地.md "wikilink")
[Category:中山區 (基隆市)](../Category/中山區_\(基隆市\).md "wikilink")
[Category:中正區 (基隆市)](../Category/中正區_\(基隆市\).md "wikilink")
[Category:仁愛區](../Category/仁愛區.md "wikilink")

1.

2.  《基隆港建港百年紀念文集》，民國74年（1985年）11月9日出版，基隆港務局發行。

3.

4.  關於[中華民國的](../Page/中華民國.md "wikilink")[海關](../Page/海關.md "wikilink")，詳見[財政部關稅總局條目](../Page/財政部關稅總局.md "wikilink")。

5.

6.  [台灣自由貿易港區網站 - 自由貿易港區在哪裡](http://www.taiwanftz.nat.gov.tw/cwhe.asp)

7.

8.

9.  [《國家政策論壇》-「市港合一」回歸基本精神](http://old.npf.org.tw/monthly/00207/theme-175.htm)


10. [基隆港務分公司-施政構想](http://www.klhb.gov.tw/Html/H01/H0105.aspx)

11. [基隆港市共同發展策略](http://www.cmri.org.tw/kmportal-deluxe/download/attdown/0/Mq2010-3-4.pdf)

12. [北北基生活圈跨域空間發展整體策略規劃\>現況分析\>交通運輸](http://nnk.ironman.tw/CH3/Ch3_4.html)

13. [基隆港務分公司-基港局整體規劃及未來展望](http://www.klhb.gov.tw/Html/H01/H0106.aspx)

14.
15. [基港32樓地標新港務大樓105年完工 -
    自由時報 2011.04.12](http://www.libertytimes.com.tw/2011/new/apr/12/today-north9.htm)


16. [基港東岸碼頭完成轉型規劃](https://web02.mtnet.gov.tw/new/readnews.jsp?newsid=46417&tid=boardnews_auto)

17. [基港西岸客運港務大樓興建修正計畫經建會通過 -
    台灣新生報2012.03.20](http://www.tssp.com.tw/news/shipping/201203/2012032004.htm)


18. [基隆港西二、三碼頭倉庫拉皮 明年規劃旅客中心 -
    自由時報2017-07-18](http://news.ltn.com.tw/news/life/breakingnews/2135836)

19. [基隆港轉型西岸都更跨出第一步 -
    聯合晚報2011.07.11](http://blog.udn.com/chenkwn/5417440)

20.