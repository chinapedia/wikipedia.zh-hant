[Fishing_at_Saint_Mary's_River,_Sault_Sainte_Marie,_Michigan,_1901.jpg](https://zh.wikipedia.org/wiki/File:Fishing_at_Saint_Mary's_River,_Sault_Sainte_Marie,_Michigan,_1901.jpg "fig:Fishing_at_Saint_Mary's_River,_Sault_Sainte_Marie,_Michigan,_1901.jpg")渔民\]\]
[Sault_Ste_Marie_Museum_8.JPG](https://zh.wikipedia.org/wiki/File:Sault_Ste_Marie_Museum_8.JPG "fig:Sault_Ste_Marie_Museum_8.JPG")
**蘇聖瑪麗**（，[IPA](../Page/IPA.md "wikilink")：）是[加拿大](../Page/加拿大.md "wikilink")[安大略省北部的一座城市](../Page/安大略省.md "wikilink")，為[阿爾戈馬區的行政中心](../Page/阿爾戈馬區.md "wikilink")。該市位於[蘇必略湖的東端盡頭](../Page/蘇必略湖.md "wikilink")，坐落[聖瑪麗河北岸](../Page/聖瑪麗河.md "wikilink")，南望[美國](../Page/美國.md "wikilink")[密西根州](../Page/密西根州.md "wikilink")[同名的城市](../Page/蘇聖瑪麗_\(密西根州\).md "wikilink")。據2011年加拿大人口普查所示，蘇聖瑪麗市內人口為75141人\[1\]，在全國排名第73位\[2\]；都會區人口則達79800人\[3\]，在全國排名第47位\[4\]。蘇聖瑪麗是安省北部繼[薩德伯里和](../Page/大薩德伯里.md "wikilink")[雷灣後的第三大城市](../Page/桑德貝.md "wikilink")。

## 歷史

蘇聖瑪麗一帶原為[歐及布威族原住民的地域](../Page/歐及布威族.md "wikilink")，該族將此處稱為“Baawitigong”，意即“激流之地”\[5\]。法國探索家布魯雷（）於1623年以法國國王[路易十三的弟親](../Page/路易十三.md "wikilink")[奧爾良公爵加斯東將此處名為加斯東瀑布](../Page/加斯東_\(奧爾良公爵\).md "wikilink")（）\[6\]。1669年，法國[耶穌會傳教士在聖瑪麗河南岸](../Page/耶穌會.md "wikilink")（即現[密芝根州](../Page/密芝根州.md "wikilink")[蘇聖瑪麗](../Page/蘇聖瑪麗_\(密西根州\).md "wikilink")）設立聚落，並將此處名為“”，指的是聖瑪麗河上的急流\[7\]。此處後來亦設立一個皮毛交易站，而聚落亦伸展至聖瑪麗河北岸；這是白人在安大略最早的定居點。隨著英國在18世紀中期的[七年戰爭中戰勝法國](../Page/七年戰爭.md "wikilink")，此帶地域亦落入英國的控制。

[1812年戰爭結束後](../Page/1812年戰爭.md "wikilink")，英美兩國就美國和[英屬北美之間的邊界展開商討](../Page/英屬北美.md "wikilink")，並於1817年確認兩地在[蘇必略湖和](../Page/蘇必略湖.md "wikilink")[休倫湖之間以聖瑪麗河為界](../Page/休倫湖.md "wikilink")，坐落該河兩岸的蘇聖瑪麗聚落自此分治。安大略省的蘇聖瑪麗於1887年正式建制為鎮，再於1912年改設為市\[8\]。

## 交通

安大略17號省道（[橫加公路](../Page/橫加公路.md "wikilink")）為蘇聖瑪麗市的主要幹道，西通[雷灣](../Page/桑德貝.md "wikilink")，東達[薩德伯里和](../Page/大薩德伯里.md "wikilink")[北灣](../Page/北灣_\(安大略省\).md "wikilink")。該市亦透過[蘇聖瑪麗國際大橋通往](../Page/蘇聖瑪麗國際大橋.md "wikilink")[密芝根州](../Page/密芝根州.md "wikilink")[蘇聖瑪麗](../Page/蘇聖瑪麗_\(密西根州\).md "wikilink")；大橋越過[美加邊境後成為](../Page/美加邊境.md "wikilink")[75號州際公路](../Page/75號州際公路.md "wikilink")，往南經[底特律一直伸延至](../Page/底特律.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")。

市內的公共交通服務由市營的蘇聖瑪麗交通局營運。航空交通方面，蘇聖瑪麗機場（，IATA代碼：YAM，ICAO代碼：CYAM）提供航線來往多倫多[皮爾遜國際機場和](../Page/皮爾遜國際機場.md "wikilink")[畢曉普機場](../Page/比利·畢曉普多倫多市機場.md "wikilink")、[渥太華](../Page/渥太華麥克唐納－卡蒂埃國際機場.md "wikilink")、薩德伯里、北灣和雷灣。

## 人口數據

據2011年加拿大人口普查所示，蘇聖瑪麗市內的族群構成如下\[9\]：

| 族群                                     | 人口                                 | 人口百分比     |
| -------------------------------------- | ---------------------------------- | --------- |
| [有色人種](../Page/有色人種.md "wikilink")     | [南亞裔](../Page/南亞裔.md "wikilink")   | 270       |
| [華裔](../Page/加拿大華人.md "wikilink")      | 290                                | 0.39      |
| 黑人                                     | 260                                | 0.35      |
| 菲律賓裔                                   | 35                                 | 0.05      |
| 拉丁美裔                                   | 110                                | 0.15      |
| 阿拉伯裔                                   | 85                                 | 0.12      |
| 東南亞裔                                   | 40                                 | 0.05      |
| [韓裔](../Page/韓裔加拿大人.md "wikilink")     | 40                                 | 0.05      |
| [日裔](../Page/日裔加拿大人.md "wikilink")     | 25                                 | 0.03      |
| **有色族群人口總計**                           | **1,215**                          | **1.65**  |
| [加拿大原住民](../Page/加拿大原住民.md "wikilink") | [第一民族](../Page/第一民族.md "wikilink") | 5,975     |
| [梅蒂人](../Page/梅蒂人.md "wikilink")       | 2,115                              | 2.87      |
| [因紐特人](../Page/因紐特人.md "wikilink")     | 25                                 | 0.03      |
| **原住民總計**                              | **7,970**                          | **10.83** |
| 白人                                     | 64,445                             | 87.53     |
| ***總人口***                              | ***73,625***                       | ***100*** |

0-14 岁人口占14.2%，15-64 岁人口占71.4%，69 以上占14.4%\[10\]。

## 參考來源

## 外部連結

  -
  - [City of Sault Ste
    Marie](http://www.city.sault-ste-marie.on.ca/)－蘇聖瑪麗市政府官方網站

[Category:安大略省城市](../Category/安大略省城市.md "wikilink")
[Category:加美邊境城鎮](../Category/加美邊境城鎮.md "wikilink")
[S](../Category/苏必利尔湖.md "wikilink")

1.
2.
3.
4.
5.

6.
7.
8.
9.

10.