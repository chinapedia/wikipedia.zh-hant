《**The Angry Video Game
Nerd**》（**AVGN**，原意“愤怒-{}-电玩宅”，中國大陸俗称“喷神-{}-James”、“怒之电玩煞星”）是一部[惡搞](../Page/惡搞文化.md "wikilink")[讽刺式怀旧](../Page/讽刺.md "wikilink")[电子游戏评论系列片](../Page/电子游戏.md "wikilink")，由业余制作人**[詹姆斯·罗弗](../Page/詹姆斯·罗弗.md "wikilink")**领衔制作及主持，在[GameTrailers和ScrewAttack两大平台网站发布](../Page/GameTrailers.md "wikilink")，影片亦在[YouTube播出](../Page/YouTube.md "wikilink")。

## 节目内容及特色

[James_D._Rolfe.jpg](https://zh.wikipedia.org/wiki/File:James_D._Rolfe.jpg "fig:James_D._Rolfe.jpg")
节目的主题是以疯狂搞怪的形式向观众介绍一些旧游戏主机上的低质素游戏。由詹姆斯出演的主角“The Angry Video Game
Nerd”是个有些[歇斯底里情绪的游戏玩家](../Page/歇斯底里.md "wikilink")，說[新泽西口音的](../Page/新泽西.md "wikilink")[英语](../Page/英语.md "wikilink")，总穿着一件衬衣还在口袋里插满笔，愛喝以“抚慰烂游戏带来的痛苦”（後期主要喝的是[云岭啤酒](../Page/云岭啤酒.md "wikilink")）。每集的内容通常是“The
Nerd”向观众介绍及演示游戏并被游戏的低下质素折磨得发疯而火冒三丈大爆[粗口](../Page/粗口.md "wikilink")，盛怒之下有时还会以各种方式“摧毁”本集内所评论的游戏，如把[卡匣扔進垃圾桶](../Page/卡匣.md "wikilink")、塞进[烤面包机](../Page/烤面包机.md "wikilink")、以鐵鎚或電鑽碎屍等。

有时节目中也会有一些“特别来宾”，往往与本集所介绍的游戏有关，比如[兔八哥](../Page/兔八哥.md "wikilink")、[蜘蛛侠](../Page/蜘蛛侠.md "wikilink")、弗莱迪（《[猛鬼街](../Page/猛鬼街.md "wikilink")》中的杀人狂）、[杰森](../Page/面具杰森魔.md "wikilink")（《[十三號星期五](../Page/十三號星期五_\(電影\).md "wikilink")》主角）等，这些人物通常由麦克·马特（Mike
Matei）扮演，他也是每集开头的手绘标题卡作者。

除游戏之外，有时节目的主题也会是关于烂电影、失败的游戏硬件等等。而且有時也會出現短篇影片製作，內容不外與主題或出現人物互動等。

节目有一首主题曲，由凯尔·贾斯汀（Kyle Justin）作曲并演唱，贾斯汀和罗弗两人共同作词。

他表示很少玩在公元2000年過後發表的遊戲，但有時候會因為觀眾要求而遊玩（如在第118集的《[大貨車極限競賽](../Page/大貨車極限競賽.md "wikilink")》）。

## 历史

节目最早于2004年5月诞生，当时詹姆斯在Cinemassacre.com上放了两段自己拍摄制作的[NES游戏评论影片](../Page/FC游戏机.md "wikilink")，当时他只是将影片在朋友间交流，并没有将影片系列化并广泛发布的打算，直到有一个朋友建议他在网上将影片大规模公开为止。2006年，詹姆斯在[YouTube上以](../Page/YouTube.md "wikilink")“Angry
[Nintendo](../Page/任天堂.md "wikilink")
Nerd”的名义将这两段影片发布，由于作品其独特的风格而迅速[爆红](../Page/网络爆红现象.md "wikilink")，詹姆斯也开始制作新的评论并形成系列。在YouTube发布了四个影片后，转而在Screwattack
Entertainment上发布影片，并受雇在隶属[MTV的](../Page/MTV.md "wikilink")[GameTrailers开辟专栏发布](../Page/GameTrailers.md "wikilink")，如今詹姆斯在YouTube上只会发布剪辑过的最新节目的预告片。

起初节目单纯介绍NES上的劣质游戏。后来，詹姆斯因版权问题将节目名字改为“The Angry Video Game
Nerd”，评论对象也从单纯的NES游戏扩展到了[雅达利2600](../Page/雅达利2600.md "wikilink")、[SNES](../Page/超级任天堂.md "wikilink")、[Mega
Drive](../Page/Mega_Drive.md "wikilink")、[PlayStation等主机的游戏以及Power](../Page/PlayStation.md "wikilink")
Glove等游戏硬體。

2007年，“The Angry Video Game
Nerd”发售了[DVD](../Page/DVD.md "wikilink")，收录了AVGN于2006年在网上发布的所有游戏评论影片以及一些新增加的DVD专有内容。

从第100期开始，The Angry Video Game Nerd以16:9格式制作，从103期开始，更以1080p高畫質制作。

## YouTube奖项

1.  43 - 最多訂閱 (所有時間) - 全世界
2.  14 - 最多訂閱 (所有時間) - 原創作者 - 全世界
3.  84 - 最多觀看次數 (本月) - 原創作者 - 全世界
4.  86 - 最多觀看次數 (所有時間) - 全世界
5.  39 - 最多觀看次數 (所有時間) - 原創作者 - 全世界

## 周边媒体

### 憤怒電玩宅大電影

Angry Video Game Nerd: The Movie
是一套由罗弗自編自導自演的[獨立電影](../Page/獨立電影.md "wikilink")，將於2014年夏天於[洛杉機和](../Page/洛杉機.md "wikilink")[加州少量電影院和](../Page/加州.md "wikilink")[Vimeo線上放映](../Page/Vimeo.md "wikilink")。故事講述電玩宅被一大尋粉絲要求他為史上最差電子遊戲《[E.T.外星人](../Page/E.T.外星人_\(游戏\).md "wikilink")》作評論，初時他常常拒絕，但是在三名粉絲熱心資助下，他下定決心前往遊戲卡帶所被掩埋的新墨西哥州沙漠裡拍片。誰知卻捲入被聯邦政府誤會他們調查[51區與UFO墜毀的風波](../Page/51區.md "wikilink")。

### 电子游戏

2013年9月20日，由FreakZone
Games开发的官方游戏《喷神James大冒险》在[Steam发布](../Page/Steam.md "wikilink")\[1\]。

### 音乐

## 影響

在日本動畫[THE UNLIMITED
兵部京介第五集中](../Page/楚楚可憐超能少女組.md "wikilink")，可以看到AVGN和他朋友懷舊評論狂作為路人出現。

## 注脚

## 外部链接

  -
  -
  -
  -
  - [JamesNintendoNerd -
    ScrewAttack](http://screwattack.roosterteeth.com/interest/78789)

  -
[Category:電子遊戲評論網站](../Category/電子遊戲評論網站.md "wikilink")
[Category:電子遊戲評論家](../Category/電子遊戲評論家.md "wikilink")
[Category:YouTuber](../Category/YouTuber.md "wikilink")

1.