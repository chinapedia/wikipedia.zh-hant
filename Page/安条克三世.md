**安條克三世（[大帝](../Page/大帝.md "wikilink")）**,
（[希臘語](../Page/希臘語.md "wikilink")：; 前241年－前187年, 統治期間
前223年－前187年），[塞琉古二世的小兒子](../Page/塞琉古二世.md "wikilink")。前223年時，安條克三世以18歲年齡即位為[塞琉古帝國第六位國王](../Page/塞琉古帝國.md "wikilink")，雖然年紀輕輕就即位，但安條克三世充滿雄心壯志。雖然在與[托勒密王國的戰爭失利](../Page/托勒密王國.md "wikilink")，但隨後的爭戰中，安條克三世証明自己是從開國的[塞琉古一世之後](../Page/塞琉古一世.md "wikilink")，歷屆塞琉古國王中最優秀的君主，他傳統稱號**大帝**可能是在東部征服行動結束後獲得綽號，安條克三世似乎還有一個頭銜****（英：Basileus），即波斯傳統稱號[大王的意思](../Page/沙阿.md "wikilink")，可能在征服[柯里敘利亞之後採用](../Page/柯里敘利亞.md "wikilink")。

## 早年

### 面臨危機的帝國

剛繼位的安條克三世面對的是一個逐漸分離的國家，除了[小亞細亞的城邦脫離](../Page/小亞細亞.md "wikilink")，連東部的省份也從帝國裡分裂，如[巴克特里亞在希臘人](../Page/巴克特里亞.md "wikilink")[狄奧多特一世領導下獨立](../Page/狄奧多特一世.md "wikilink")、[帕提亞遭受](../Page/帕提亞.md "wikilink")[阿爾沙克一世占據](../Page/阿爾沙克一世.md "wikilink")。在安條克三世即位後，東部的[米底總督](../Page/米底.md "wikilink")[莫倫](../Page/莫倫_\(總督\).md "wikilink")、[波西斯總督](../Page/法爾斯省.md "wikilink")[亞歷山大兩兄弟也發動叛亂](../Page/亞歷山大_\(總督\).md "wikilink")。

安條克三世在權臣[赫米亞斯誤導下](../Page/赫米亞斯.md "wikilink")，派遣兩位將軍率軍前往平息莫倫兄弟的叛亂，而自己親自進攻[托勒密王國的](../Page/托勒密王國.md "wikilink")[猶太](../Page/猶太_\(地區\).md "wikilink")，然而在這兩方面都遭到慘敗，靠著國王的表兄弟[阿凱夫斯在小亞細亞成功擊退](../Page/阿凱夫斯.md "wikilink")[帕加馬的勢力](../Page/帕加馬.md "wikilink")，使塞琉古帝國威信不至於崩潰。

### 與其他希臘化統治者交手

[AntiochusIII.jpg](https://zh.wikipedia.org/wiki/File:AntiochusIII.jpg "fig:AntiochusIII.jpg")
前220年安條克親自在[阿波羅尼亞戰役擊敗叛軍](../Page/阿波羅尼亞戰役.md "wikilink")，平定莫倫和的亞歷山大的叛亂，迫使原本為塞琉古帝國屬國的[阿特羅帕特尼君主](../Page/阿特羅帕特尼.md "wikilink")[阿爾托巴札涅斯取消獨立](../Page/阿爾托巴札涅斯.md "wikilink")。之後，安條克暗殺了心懷惡意的赫米亞斯，並在前220年回到敘利亞。此時小亞細亞的阿凱夫斯宣布叛亂，並冠上國王的稱號，但阿凱夫斯實力不足以向敘利亞進攻。此時埃及因宮廷陰謀及民心的不穩定，國力大幅衰落，安條克三世便決定趁著這個機會入侵托勒密王國的猶太地區，爆發了[第四次敘利亞戰爭](../Page/敘利亞戰爭#第四次敘利亞戰爭（前219-前217年）.md "wikilink")。

在前219年到前218年的行動中，塞琉古軍成功把托勒密王國逼入困境，使安條克佔領[柯里敘利亞](../Page/柯里敘利亞.md "wikilink")、[腓尼基和](../Page/腓尼基.md "wikilink")[巴勒斯坦等地](../Page/巴勒斯坦.md "wikilink")，但在前217年[托勒密四世在](../Page/托勒密四世.md "wikilink")[拉菲亞戰役與安條克三世對決中](../Page/拉菲亞戰役.md "wikilink")，塞琉古遭到徹底擊敗，除了使安條克喪失先前的戰果，還被趕回北[黎巴嫩](../Page/黎巴嫩.md "wikilink")。結束第四次敘利亞戰爭後，前216年安條克三世北上處理叛亂的阿凱夫斯，迫使阿凱夫斯退回根據地[薩第斯](../Page/薩第斯.md "wikilink")，在兩年的包圍下，在前213年奪取薩第斯。

## 對東部的再征服

[AntiochosIII.JPG](https://zh.wikipedia.org/wiki/File:AntiochosIII.JPG "fig:AntiochosIII.JPG")

### 進攻帕提亞和巴克特里亞

收復小亞細亞中部後，安條克默許[帕加馬](../Page/帕加馬.md "wikilink")、[比提尼亞和](../Page/比提尼亞.md "wikilink")[卡帕多細亞的存在](../Page/卡帕多細亞.md "wikilink")，並把注意力轉到北方和東部。前212年迫使南[亞美尼亞王國國王](../Page/亞美尼亞王國.md "wikilink")[薛西斯承認塞琉古為宗主國](../Page/薛西斯_\(亞美尼亞\).md "wikilink")。前209年安條克三世入侵[帕提亞並攻占首府](../Page/帕提亞.md "wikilink")[赫卡通皮洛斯](../Page/赫卡通皮洛斯.md "wikilink")，並在拉馬倪山擊敗安息國王[阿爾沙克二世](../Page/阿爾沙克二世.md "wikilink")，推進到[希爾卡尼亞](../Page/希爾卡尼亞.md "wikilink")，迫使阿爾達班求和並把安息納入屬國。

矛頭一轉，安條克三世接著在前209年開始入侵獨立已久的[巴克特里亞](../Page/巴克特里亞.md "wikilink")。而推翻狄奧多特王朝的[巴克特里亞國王](../Page/希腊-巴克特里亚王国.md "wikilink")[歐西德莫斯一世率領大軍禦敵](../Page/歐西德莫斯一世.md "wikilink")，在[阿利烏河戰役遭到安條克三世痛擊](../Page/阿利烏河戰役.md "wikilink")\[1\]
，歐西德莫斯只能撤退回首都[薄知](../Page/薄知.md "wikilink")（Bactra）堅守。在成功抵抗三年的圍困後，公元前206年，安條克給予歐西德莫斯一個體面的和約，承諾把女兒嫁給歐西德莫斯一世的兒子[德米特里一世](../Page/德米特里一世_\(巴克特里亞\).md "wikilink")\[2\]，並使巴克特里亞成為屬國。

### 遠征印度

如同步隨[亞歷山大大帝的足跡似的](../Page/亞歷山大大帝.md "wikilink")，安條克三世越過[興都庫什山脈侵入](../Page/興都庫什山脈.md "wikilink")[喀布爾山谷](../Page/喀布爾.md "wikilink")，與當地的印度王公[幸军王重續兩國友誼](../Page/幸军王.md "wikilink")，之後經[錫斯坦和](../Page/錫斯坦.md "wikilink")[克爾曼的道路回到西方](../Page/克爾曼.md "wikilink")。據古希臘歷史學家[波利比烏斯描述](../Page/波利比烏斯.md "wikilink"):

在前205、204年間，安條克三世從[底格里斯河畔](../Page/底格里斯河.md "wikilink")[塞琉西亞出發](../Page/塞琉西亞.md "wikilink")，針對波斯灣阿拉伯沿岸的[加爾拉進行一次短暫的遠征](../Page/加爾拉.md "wikilink")。安條克多年來對東方的爭戰，使帝國東部重新獲得的控制，這些成就讓他冠上**大帝**（）的稱號。
[Seleucid-Empire_200bc.jpg](https://zh.wikipedia.org/wiki/File:Seleucid-Empire_200bc.jpg "fig:Seleucid-Empire_200bc.jpg")

## 第五次敘利亞戰爭

隨著埃及[托勒密四世於前](../Page/托勒密四世.md "wikilink")204年的逝世，王位繼承人[托勒密五世仍只是一個五歲小孩子](../Page/托勒密五世.md "wikilink")，而爆發一場爭奪攝政權的血腥衝突，國力大幅衰退。趁著這個機會安條克三世立刻準備發起新的戰爭，拿下帝國百年來朝思暮想的腓尼基和巴勒斯坦領土。前202年安條克率大軍南侵，迂迴進入巴勒斯坦，並策動托勒密當地總督叛變。安條克繞過難以攻陷的要塞，一路直指有重要戰略價值的加薩，花了數月的時間攻陷了它，來阻止托勒密軍隊從埃及增援。安條克隨後回頭攻擊繞過的巴勒斯坦要地，成功拿下巴勒斯坦大部。

前201年冬季，托勒密將軍[斯科帕斯趁著安條克把一部份軍隊帶回敘利亞過冬](../Page/埃托利亞的斯科帕斯.md "wikilink")，從埃及發動大軍突襲，在短短的時間內幾乎奪回失去的領土。隔年春天，安條克從敘利亞帶著大軍南下巴勒斯坦對抗敵軍，在[約旦河源頭附近的](../Page/約旦河.md "wikilink")[帕尼翁戰役中擊敗斯科帕斯](../Page/帕尼翁戰役.md "wikilink")，使埃及軍隊損失慘重，更奪取巴勒斯坦和腓尼基等底。接著，安條克轉往餘下的托勒密的領土，奪取包含[奇里乞亞和](../Page/奇里乞亞.md "wikilink")[以弗所在內的小亞細亞沿岸托勒密領土](../Page/以弗所.md "wikilink")，才結束[第五次敘利亞戰爭](../Page/敘利亞戰爭#第五次敘利亞戰爭（前202-前195年）.md "wikilink")。

## 與羅馬的衝突

然而，安條克三世奪取托勒密在小亞細亞沿岸和其他獨立的希臘城邦如[士麥那和](../Page/士麥那.md "wikilink")[蘭薩庫斯等這個舉動](../Page/蘭薩庫斯.md "wikilink")，使[羅馬與塞琉古帝國關係愈趨緊張](../Page/羅馬共和國.md "wikilink")，而當安條克在前196年進軍[色雷斯時更為惡化](../Page/色雷斯.md "wikilink")，羅馬要求塞琉古勢力必須從歐洲撤離，而安條克三世聲稱色雷斯本為[塞琉古一世時的疆域](../Page/塞琉古一世.md "wikilink")，還收留流亡的[迦太基將軍](../Page/迦太基.md "wikilink")[漢尼拔](../Page/漢尼拔.md "wikilink")。

[Treaty_of_Apamea.png](https://zh.wikipedia.org/wiki/File:Treaty_of_Apamea.png "fig:Treaty_of_Apamea.png")
在前192年安條克三世率領10,000名步兵、500名騎兵和六頭戰象橫過[愛琴海並在](../Page/愛琴海.md "wikilink")[色薩利的](../Page/色薩利.md "wikilink")[德米特里阿斯登陸](../Page/德米特里阿斯.md "wikilink")，來支援[埃托利亞同盟對羅馬的戰事](../Page/埃托利亞同盟.md "wikilink")，爆發[羅馬-敘利亞戰爭](../Page/羅馬-敘利亞戰爭.md "wikilink")。

安條克三世原本打算以希臘解放者的姿態獲得更多資源，然而，其他希臘勢力卻選擇與站在羅馬那方，連前盟友馬其頓[腓力五世都願意對羅馬提供幫助](../Page/腓力五世.md "wikilink")，安條克三世期望能在[溫泉關抵擋羅馬軍](../Page/溫泉關.md "wikilink")，但戰役結果使他的大軍遭受慘重損失，安條克三世只能撤回小亞細亞重整旗鼓。隨後，羅馬和羅德島以及其他盟國聯合艦隊在一連串的海戰中擊敗塞琉古艦隊，使塞琉古海軍遭到徹底摧毀，使得羅馬大軍成功橫越[達達尼爾海峽](../Page/達達尼爾海峽.md "wikilink")，在羅馬新任[執政官](../Page/執政官.md "wikilink")[盧基烏斯·科爾內利烏斯·西庇阿率領下將戰事轉移到小亞細亞](../Page/卢基乌斯·科尔内利乌斯·西庇阿·亚细亚提库斯.md "wikilink")，其兄長[大西庇阿作為特使而隨軍行動](../Page/大西庇阿.md "wikilink")。

前190年，安條克三世率領從帝國各地招集大軍，在[馬格尼西亞戰役被羅馬徹底擊敗](../Page/馬格尼西亞戰役.md "wikilink")，安條克簽下難堪的[阿帕米亞和約](../Page/阿帕米亞和約.md "wikilink")，結束羅馬-敘利亞戰爭。根據條約，塞琉古帝國割讓歐洲及[托魯斯山脈以西的小亞細亞](../Page/托魯斯山脈.md "wikilink")，限定海軍的數量，並付出一大筆賠款給羅馬\[3\]。

## 晚年

與羅馬的戰爭失敗，使塞琉古帝國元氣大傷，原先被安條克三世所征服而從屬的國家，紛紛宣布獨立。而為籌給羅馬龐大賠款，安條克三世計畫一次新的東方遠征，前187年在遠征[洛雷斯坦時](../Page/洛雷斯坦省.md "wikilink")，安條克企圖掠奪[蘇薩](../Page/蘇薩.md "wikilink")[巴尔神廟的財富而被殺](../Page/巴尔.md "wikilink")，王位由其子[塞琉古四世繼承](../Page/塞琉古四世.md "wikilink")。

## 註腳

## 參考文獻

  - [阿庇安](../Page/阿庇安.md "wikilink"),History of Rome: The Syrian Wars
    *[Syriaca](http://www.livius.org/ap-ark/appian/appian_syriaca_00.html)*
  - <span id=Polybius>[波利比烏斯](../Page/波利比烏斯.md "wikilink")</span>,
    translated by Frank W. Walbank, (1979). The Rise of the Roman
    Empire. New York: Penguin Classics. ISBN 0-14-044362-2.
  - Peter Green, (1990). *Alexander to Actium: The Historical Evolution
    of the Hellenistic Age, (2nd edition)*. Los Angeles: University of
    California Press. ISBN 0-500-01485-X.
  - 《1911年版大英百科》

<!-- end list -->

  -
<!-- end list -->

  -
[Category:塞琉古國王](../Category/塞琉古國王.md "wikilink")
[Category:前241年出生](../Category/前241年出生.md "wikilink")
[Category:前187年逝世](../Category/前187年逝世.md "wikilink")

1.  [波利比烏斯](../Page/波利比烏斯.md "wikilink")《歷史》
    [10.49](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+10.49),
2.  [波利比烏斯](../Page/波利比烏斯.md "wikilink")
    《歷史》[11.34](http://www.perseus.tufts.edu/cgi-bin/ptext?lookup=Plb.+11.34)
3.  [阿庇安](../Page/阿庇安.md "wikilink"),Syriaca[38](http://www.livius.org/ap-ark/appian/appian_syriaca_08.html#%5B%A739%5D)