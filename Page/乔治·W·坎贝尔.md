**乔治·华盛顿·坎贝尔**（**George Washington
Campbell**，），[美国律师](../Page/美国.md "wikilink")、政治家，[美国民主-共和党成员](../Page/美国民主-共和党.md "wikilink")，曾任[美国参议员](../Page/美国参议员.md "wikilink")（1811年-1814年、1815年-1818年）和[美国财政部长](../Page/美国财政部长.md "wikilink")（1814年）。

[Category:蘇格蘭人](../Category/蘇格蘭人.md "wikilink")
[C](../Category/普林斯頓大學校友.md "wikilink")
[Category:美国民主共和党联邦众议员](../Category/美国民主共和党联邦众议员.md "wikilink")
[Category:美國民主共和黨聯邦參議員](../Category/美國民主共和黨聯邦參議員.md "wikilink")
[Category:田納西州聯邦參議員](../Category/田納西州聯邦參議員.md "wikilink")
[Category:田納西州聯邦眾議員](../Category/田納西州聯邦眾議員.md "wikilink")