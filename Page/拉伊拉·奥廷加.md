**拉伊拉·阿莫洛·奥廷加**（，）是[肯尼亞的政治人物](../Page/肯尼亞.md "wikilink")。

## 生平

他從1992年開始以來他擔任國會議員、從2001年至2002年擔任能源部長。他是[2007年總統選舉中主要反對黨](../Page/2007年肯亞總統選舉.md "wikilink")[橙色民主運動總統候選人](../Page/橙色民主運動.md "wikilink")，並懷疑選舉結果有舞弊爭議，引發[骚乱](../Page/肯亚骚乱.md "wikilink")。其後任[肯亞總理一職](../Page/肯亞總理.md "wikilink")，組建[聯合政府](../Page/聯合政府.md "wikilink")，并于五年任期結束後，2013年再次参选总统，但是以些微差距敗給了[烏胡魯·肯雅塔](../Page/烏胡魯·肯雅塔.md "wikilink")。

## 軼事

拉伊拉·奥廷加曾經自稱和[美國總統](../Page/美國總統.md "wikilink")[巴拉克·歐巴馬是親戚](../Page/巴拉克·歐巴馬.md "wikilink")，因為都是[盧歐人出身](../Page/盧歐人.md "wikilink")。

[Category:肯亞總理](../Category/肯亞總理.md "wikilink")
[Category:盧歐人](../Category/盧歐人.md "wikilink")
[Category:內羅畢大學校友](../Category/內羅畢大學校友.md "wikilink")
[Category:萊比錫大學校友](../Category/萊比錫大學校友.md "wikilink")