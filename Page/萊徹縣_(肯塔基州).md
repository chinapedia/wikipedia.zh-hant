**萊徹县**（**Letcher County,
Kentucky**）是位於[美國](../Page/美國.md "wikilink")[肯塔基州東南部的一個縣](../Page/肯塔基州.md "wikilink")，東南鄰[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")。面積878平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口25,277人。縣治[懷茨堡](../Page/懷茨堡_\(肯塔基州\).md "wikilink")
（Whitesburg），最大城市[詹金斯](../Page/詹金斯_\(肯塔基州\).md "wikilink") （Jenkins）。

成立於1842年3月3日。縣名紀念[聯邦眾議員](../Page/美國眾議院.md "wikilink")、[肯塔基州州長](../Page/肯塔基州州長.md "wikilink")、駐[墨西哥公使](../Page/墨西哥.md "wikilink")[羅伯特·P·萊徹](../Page/羅伯特·P·萊徹.md "wikilink")
（Robert Perkins Letcher）。\[1\]

## 参考文献

[L](../Category/肯塔基州行政区划.md "wikilink")
[Category:阿巴拉契亞文化](../Category/阿巴拉契亞文化.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.