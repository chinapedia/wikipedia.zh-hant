**谶纬**是[中国古代](../Page/中国.md "wikilink")[谶书和](../Page/谶书.md "wikilink")**纬书**的合称。

**谶**（現也稱籤）是[秦](../Page/秦.md "wikilink")[汉间](../Page/汉.md "wikilink")[巫师](../Page/巫师.md "wikilink")、[方士编造的预示吉凶的隐语](../Page/方士.md "wikilink")，後來民間發展在[廟宇或](../Page/廟宇.md "wikilink")[道觀裹](../Page/道觀.md "wikilink")[求神](../Page/杯筊.md "wikilink")[問卜](../Page/占卜.md "wikilink")，漸漸地更加簡化為[求籤](../Page/求籤.md "wikilink")。
《[說文解字](../Page/說文解字.md "wikilink")》：讖，驗也。有徵驗之書，河雒所出書曰讖\[1\]。**纬**是汉代附会[儒家经义衍生出来的一类书](../Page/儒家.md "wikilink")，被[汉光武帝](../Page/汉光武帝.md "wikilink")[刘秀之后的人称为](../Page/刘秀.md "wikilink")“内学”，而原本的经典反被称为“外学”。**谶纬之学**就是一种政治[预言](../Page/预言.md "wikilink")。

## 历史

在[秦朝](../Page/秦朝.md "wikilink")，[陈胜](../Page/陈胜.md "wikilink")、[吴广的](../Page/吴广.md "wikilink")[大泽乡起义就利用谶语制造了](../Page/大泽乡起义.md "wikilink")“大楚兴，陈胜王”等等天賜的預言。

[汉朝是谶纬之学最兴盛的时期](../Page/汉朝.md "wikilink")，尤以[西漢末年及東漢末年最盛](../Page/西漢.md "wikilink")\[2\]。例如[王莽称帝就利用谶语制造](../Page/王莽.md "wikilink")[舆论](../Page/舆论.md "wikilink")，制作了“告安汉公莽为皇帝”的石碑。之后的[汉光武帝也利用](../Page/汉光武帝.md "wikilink")《[赤伏符](../Page/赤伏符.md "wikilink")》即位。之后，在[中元元年](../Page/中元.md "wikilink")（56年）宣布图谶于天下，使之合法化。[建初四年](../Page/建初.md "wikilink")（79年），[汉章帝还主持召开了一次全国](../Page/汉章帝.md "wikilink")[经学讨论会](../Page/经学.md "wikilink")，即[白虎观会议](../Page/白虎观会议.md "wikilink")，会议纪录由[班固整理成](../Page/班固.md "wikilink")《[白虎通德论](../Page/白虎通德论.md "wikilink")》，以法令形式将谶纬之学定形，和正统经书具有同等地位。

[魏晋以后](../Page/魏晋.md "wikilink")，随着[玄学的兴起](../Page/玄学.md "wikilink")，对儒家传统经学有了全新的解释，于是宣扬宿命论的谶纬之书渐遭毁禁；至[宋](../Page/宋.md "wikilink")[欧阳修](../Page/欧阳修.md "wikilink")〈论删去九经正义中谶纬札子〉后，谶纬学说更是式微，其书籍文献多散失不传。

## 主要内容

谶纬之学的内容很庞杂，其内容比如：

  - [天人感应](../Page/天人感应.md "wikilink")，星象预测吉凶（参看[占星学](../Page/占星学.md "wikilink")）
  - [報應說](../Page/報應.md "wikilink")，人的善恶能够影响到人的寿命
  - [昆仑山是](../Page/昆仑山.md "wikilink")[神仙所在之地](../Page/神仙.md "wikilink")，[西王母则是指引修行的神仙](../Page/西王母.md "wikilink")
  - [黄帝是](../Page/黄帝.md "wikilink")[北斗之神](../Page/北斗七星.md "wikilink")
  - [孔子是](../Page/孔子.md "wikilink")“黑龙”之种

## 緯書

玆据[日本學者](../Page/日本.md "wikilink")、所辑《纬书集成》列出一些主要的緯書：\[3\]

### [易](../Page/周易.md "wikilink")

[易纬乾凿度](../Page/易纬乾凿度.md "wikilink")、[易纬乾坤凿度](../Page/易纬乾坤凿度.md "wikilink")、[易纬稽览图](../Page/易纬稽览图.md "wikilink")、[易纬辨终备](../Page/易纬辨终备.md "wikilink")、[易纬通卦验](../Page/易纬通卦验.md "wikilink")、[易纬乾元序制记](../Page/易纬乾元序制记.md "wikilink")、[易纬是类谋](../Page/易纬是类谋.md "wikilink")、[易纬坤灵图](../Page/易纬坤灵图.md "wikilink")、[易纬中孚传](../Page/易纬中孚传.md "wikilink")、[易纬天人应](../Page/易纬天人应.md "wikilink")、[易纬通统图](../Page/易纬通统图.md "wikilink")、[易纬运期](../Page/易纬运期.md "wikilink")、[易纬内传](../Page/易纬内传.md "wikilink")、[易纬萌气枢](../Page/易纬萌气枢.md "wikilink")、[易纬内篇](../Page/易纬内篇.md "wikilink")、[易纬太初篇](../Page/易纬太初篇.md "wikilink")、[易纬九戹谶](../Page/易纬九戹谶.md "wikilink")、[易纬礼观书](../Page/易纬礼观书.md "wikilink")、[易纬记](../Page/易纬记.md "wikilink")、[易纬纪表](../Page/易纬纪表.md "wikilink")、[易纬決象](../Page/易纬決象.md "wikilink")、[易纬统通卦验玄图](../Page/易纬统通卦验玄图.md "wikilink")、[易纬河图数等](../Page/易纬河图数.md "wikilink")[易纬](../Page/易纬.md "wikilink")（其他書名不詳者）。

### [尚书](../Page/尚書_\(經\).md "wikilink")

《[尚书大传](../Page/尚书大传.md "wikilink")》、[尚书考灵曜](../Page/尚书考灵曜.md "wikilink")、[尚书帝命验](../Page/尚书帝命验.md "wikilink")、[尚书璇玑钤](../Page/尚书璇玑钤.md "wikilink")、[尚书刑德放](../Page/尚书刑德放.md "wikilink")、[尚书运期授](../Page/尚书运期授.md "wikilink")、[尚书帝验期](../Page/尚书帝验期.md "wikilink")、[尚书洪范记](../Page/尚书洪范记.md "wikilink")、[尚书中候](../Page/尚书中候.md "wikilink")（[尚书中候握河纪](../Page/尚书中候握河纪.md "wikilink")、[尚书中候我应](../Page/尚书中候我应.md "wikilink")、[尚书中候考河命](../Page/尚书中候考河命.md "wikilink")、[尚书中候雒予命](../Page/尚书中候雒予命.md "wikilink")、[尚书中候雒师谋](../Page/尚书中候雒师谋.md "wikilink")、[尚书中候摘雒戒](../Page/尚书中候摘雒戒.md "wikilink")、[尚书中候仪明](../Page/尚书中候仪明.md "wikilink")、[尚书中候敕省图](../Page/尚书中候敕省图.md "wikilink")、[尚书中候稷起](../Page/尚书中候稷起.md "wikilink")、[尚书中候凖谶哲](../Page/尚书中候凖谶哲.md "wikilink")、[尚书中候合符后](../Page/尚书中候合符后.md "wikilink")、[尚书中候运衡](../Page/尚书中候运衡.md "wikilink")、[尚书中候契握](../Page/尚书中候契握.md "wikilink")、[尚书中候苗兴](../Page/尚书中候苗兴.md "wikilink")、[尚书中候赤雀命](../Page/尚书中候赤雀命.md "wikilink")、[尚书中候日角](../Page/尚书中候日角.md "wikilink")、[尚书中候霸免](../Page/尚书中候霸免.md "wikilink")、[尚书中候顗期](../Page/尚书中候顗期.md "wikilink")、[尚书中候亶甫](../Page/尚书中候亶甫.md "wikilink")、[尚书中候杂篇](../Page/尚书中候杂篇.md "wikilink")）等。

### [诗](../Page/詩經.md "wikilink")

[诗含神雾](../Page/诗含神雾.md "wikilink")、[诗推度灾](../Page/诗推度灾.md "wikilink")、[诗泛历枢等](../Page/诗泛历枢.md "wikilink")[诗纬](../Page/诗纬.md "wikilink")（其他書名不詳者）。

### [禮](../Page/周禮.md "wikilink")

[礼含文嘉](../Page/礼含文嘉.md "wikilink")、[礼稽命徵](../Page/礼稽命徵.md "wikilink")、[礼斗威仪等](../Page/礼斗威仪.md "wikilink")[礼纬](../Page/礼纬.md "wikilink")（其他書名不詳者）。

### [樂](../Page/樂經.md "wikilink")

[乐动声仪](../Page/乐动声仪.md "wikilink")、[乐稽耀嘉](../Page/乐稽耀嘉.md "wikilink")、[乐叶图徵等](../Page/乐叶图徵.md "wikilink")[乐纬](../Page/乐纬.md "wikilink")（其他書名不詳者）。

### [春秋](../Page/春秋_\(史書\).md "wikilink")

[春秋演孔图](../Page/春秋演孔图.md "wikilink")、[春秋元命苞](../Page/春秋元命苞.md "wikilink")、[春秋文曜钩](../Page/春秋文曜钩.md "wikilink")、[春秋运斗枢](../Page/春秋运斗枢.md "wikilink")、[春秋感精符](../Page/春秋感精符.md "wikilink")、[春秋合诚图](../Page/春秋合诚图.md "wikilink")、[春秋考异邮](../Page/春秋考异邮.md "wikilink")、[春秋保乾图](../Page/春秋保乾图.md "wikilink")、[春秋汉含孳](../Page/春秋汉含孳.md "wikilink")、[春秋佐助期](../Page/春秋佐助期.md "wikilink")、[春秋握诚图](../Page/春秋握诚图.md "wikilink")、[春秋潜潭巴](../Page/春秋潜潭巴.md "wikilink")、[春秋说题辞](../Page/春秋说题辞.md "wikilink")、[春秋命历序](../Page/春秋命历序.md "wikilink")、[春秋内事](../Page/春秋内事.md "wikilink")、[春秋錄图](../Page/春秋錄图.md "wikilink")、[春秋錄运法](../Page/春秋錄运法.md "wikilink")、[春秋孔錄法](../Page/春秋孔錄法.md "wikilink")、[春秋璇玑枢](../Page/春秋璇玑枢.md "wikilink")、[春秋揆命篇](../Page/春秋揆命篇.md "wikilink")、[春秋河图揆命篇](../Page/春秋河图揆命篇.md "wikilink")、[春秋玉版谶](../Page/春秋玉版谶.md "wikilink")、[春秋瑞应传](../Page/春秋瑞应传.md "wikilink")、[春秋感应图](../Page/春秋感应图.md "wikilink")、[春秋考灵曜](../Page/春秋考灵曜.md "wikilink")、[春秋圣洽符](../Page/春秋圣洽符.md "wikilink")、[春秋甄燿度](../Page/春秋甄燿度.md "wikilink")、[春秋纬](../Page/春秋纬.md "wikilink")、[春秋图等](../Page/春秋图.md "wikilink")。

### [孝经](../Page/孝经.md "wikilink")

[孝经援神契](../Page/孝经援神契.md "wikilink")、[孝经中契](../Page/孝经中契.md "wikilink")、[孝经左契](../Page/孝经左契.md "wikilink")、[孝经右契](../Page/孝经右契.md "wikilink")、[孝经钩命決](../Page/孝经钩命決.md "wikilink")、[孝经内事](../Page/孝经内事.md "wikilink")、[孝经内事图](../Page/孝经内事图.md "wikilink")、[孝经河图](../Page/孝经河图.md "wikilink")、[孝经中黄谶](../Page/孝经中黄谶.md "wikilink")、[孝经威嬉拒](../Page/孝经威嬉拒.md "wikilink")、[孝经古祕](../Page/孝经古祕.md "wikilink")、[孝经雌雄图](../Page/孝经雌雄图.md "wikilink")、[孝经雌雄图三光占](../Page/孝经雌雄图三光占.md "wikilink")、[孝经章句](../Page/孝经章句.md "wikilink")、[孝经纬等](../Page/孝经纬.md "wikilink")。

### [论语](../Page/论语.md "wikilink")

[论语比考](../Page/论语比考.md "wikilink")、[论语譔考](../Page/论语譔考.md "wikilink")、[论语摘辅象](../Page/论语摘辅象.md "wikilink")、[论语摘衰圣](../Page/论语摘衰圣.md "wikilink")、[论语素王受命谶](../Page/论语素王受命谶.md "wikilink")、[论语崇爵谶](../Page/论语崇爵谶.md "wikilink")、[论语纠滑谶](../Page/论语纠滑谶.md "wikilink")、[论语阴嬉谶](../Page/论语阴嬉谶.md "wikilink")、[论语谶等](../Page/论语谶.md "wikilink")。

### [河图](../Page/河圖洛書.md "wikilink")

[河图括地象](../Page/河图括地象.md "wikilink")、[河图始开图](../Page/河图始开图.md "wikilink")、[河图挺佐辅](../Page/河图挺佐辅.md "wikilink")、[河图稽耀钩](../Page/河图稽耀钩.md "wikilink")、[河图帝览嬉](../Page/河图帝览嬉.md "wikilink")、[河图握炬记](../Page/河图握炬记.md "wikilink")、[河图玉版](../Page/河图玉版.md "wikilink")、[龙鱼河图](../Page/龙鱼河图.md "wikilink")、[河图合古篇](../Page/河图合古篇.md "wikilink")、[河图令占篇](../Page/河图令占篇.md "wikilink")、[河图赤伏符](../Page/河图赤伏符.md "wikilink")、[河图闿苞受](../Page/河图闿苞受.md "wikilink")、[河图叶光纪](../Page/河图叶光纪.md "wikilink")、[河图龙文](../Page/河图龙文.md "wikilink")、[河图錄运法](../Page/河图錄运法.md "wikilink")、[河图帝通纪](../Page/河图帝通纪.md "wikilink")、[河图真纪钩](../Page/河图真纪钩.md "wikilink")、[河图龙帝纪](../Page/河图龙帝纪.md "wikilink")、[河图龙表](../Page/河图龙表.md "wikilink")、[河图考钩](../Page/河图考钩.md "wikilink")、[河图秘徵](../Page/河图秘徵.md "wikilink")、[河图说徵](../Page/河图说徵.md "wikilink")、[河图说徵祥](../Page/河图说徵祥.md "wikilink")、[河图说徵示](../Page/河图说徵示.md "wikilink")、[河图会昌符](../Page/河图会昌符.md "wikilink")、[河图稽命徵](../Page/河图稽命徵.md "wikilink")、[河图揆命篇](../Page/河图揆命篇.md "wikilink")、[河图要元篇](../Page/河图要元篇.md "wikilink")、[河图天灵](../Page/河图天灵.md "wikilink")、[河图提刘篇](../Page/河图提刘篇.md "wikilink")、[河图绛象](../Page/河图绛象.md "wikilink")、[图纬绛象](../Page/图纬绛象.md "wikilink")、[河图著明](../Page/河图著明.md "wikilink")、[河图皇持参](../Page/河图皇持参.md "wikilink")、[河图帝视萌](../Page/河图帝视萌.md "wikilink")、[河图灵武帝篇](../Page/河图灵武帝篇.md "wikilink")、[河图玉英](../Page/河图玉英.md "wikilink")、[河图稽纪钩](../Page/河图稽纪钩.md "wikilink")、[河图考灵曜](../Page/河图考灵曜.md "wikilink")、[河图纪命符](../Page/河图纪命符.md "wikilink")、[河图圣洽符](../Page/河图圣洽符.md "wikilink")、[河图表纪等](../Page/河图表纪.md "wikilink")。

### [洛书](../Page/洛书.md "wikilink")

[洛书灵準听](../Page/洛书灵準听.md "wikilink")、[洛书甄曜度](../Page/洛书甄曜度.md "wikilink")、[洛书摘六辟](../Page/洛书摘六辟.md "wikilink")、[洛书宝号命](../Page/洛书宝号命.md "wikilink")、[洛书说禾](../Page/洛书说禾.md "wikilink")、[洛书錄运法](../Page/洛书錄运法.md "wikilink")、[洛书錄运期](../Page/洛书錄运期.md "wikilink")、[孔子河洛谶](../Page/孔子河洛谶.md "wikilink")、[洛书雒罪级](../Page/洛书雒罪级.md "wikilink")、[洛书纪](../Page/洛书纪.md "wikilink")、[洛图三光占](../Page/洛图三光占.md "wikilink")、[洛书说徵示](../Page/洛书说徵示.md "wikilink")、[洛书兵钤势](../Page/洛书兵钤势.md "wikilink")、[洛书斗中图等](../Page/洛书斗中图.md "wikilink")。

## 相關條目

  - 《[李淳风推背圖](../Page/推背圖.md "wikilink")》
  - 《[李淳风藏头诗](../Page/李淳风藏头诗.md "wikilink")》
  - 《[步虛真君預言詩](../Page/天台步虛.md "wikilink")》
  - 《[刘伯温烧饼歌](../Page/燒餅歌.md "wikilink")》
  - 《[铁冠僧透天玄机](../Page/铁冠僧透天玄机.md "wikilink")》
  - 《[姜太公乾坤万年歌](../Page/乾坤万年歌.md "wikilink")》
  - 《[邵雍梅花诗](../Page/梅花诗.md "wikilink")》
  - 《[金陵塔碑文](../Page/金陵塔碑文.md "wikilink")》
  - 《[黄檗禅师诗](../Page/黄檗禅师诗.md "wikilink")》
  - 《[诸葛亮马前课和碑记](../Page/馬前課.md "wikilink")》

## 註釋

## 参考文献

## 研究書目

  - 安居香山 著，田人隆 譯：《緯書與中國神秘思想》（石家莊：河北人民出版社，1991）
  - Tiqiana
    Lippiello：〈[讖緯的不明起源和發展：從方士的傳統到漢代正統文化](http://www.daoist.org/BookSearch/BookSearch/list009/0436.pdf)〉。
  - 吉川忠夫：〈[蜀地的讖緯學傳統](http://www.nknu.edu.tw/~jingxue/100/download/essay/003/003009.pdf)〉。
  - 楠山春樹 著，洪春音
    譯：〈[《毛詩正義》所引用的緯書](http://www.litphil.sinica.edu.tw/home/publish/PDF/Newsletter/61/61-97-114.pdf)〉
  - 孙英刚：〈[神文时代：中古知识、信仰与政治世界之关联性](http://www.nssd.org/articles/article_read.aspx?id=47435160)〉。

[谶语](../Category/谶语.md "wikilink")
[Category:緯書](../Category/緯書.md "wikilink")

1.  [段玉裁注補引](../Page/段玉裁.md "wikilink")《[文選](../Page/文選.md "wikilink")•鵩鳥賦》、《文選•魏都賦》補後十二字。見段玉裁《說文解字注》第三篇上，中華書局，2014年，第91頁.[1](http://www.zdic.net/z/24/sw/8B96.htm)
2.  賈本曜，社會考科--學科能力測驗，頁32
3.  \[日\][安居香山](../Page/安居香山.md "wikilink")、[中村璋八](../Page/中村璋八.md "wikilink")
    辑，纬书集成（全三册），[石家莊](../Page/石家莊.md "wikilink")：[河北人民出版社](../Page/河北人民出版社.md "wikilink")，1994年