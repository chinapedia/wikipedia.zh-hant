[Kombucha_Mature.jpg](https://zh.wikipedia.org/wiki/File:Kombucha_Mature.jpg "fig:Kombucha_Mature.jpg")
[Kombucha_mushroom.jpg](https://zh.wikipedia.org/wiki/File:Kombucha_mushroom.jpg "fig:Kombucha_mushroom.jpg")
**紅茶菌**，又名**紅茶菇**、**茶菇**，因普遍在加糖的紅茶中培養而得名。紅茶菌不是單一的菌種，更不是菇類，經由分析其為[酵母屬](../Page/酵母屬.md "wikilink")、[醋桿菌屬和](../Page/醋桿菌屬.md "wikilink")[乳桿菌屬的共生物](../Page/乳桿菌屬.md "wikilink")，其中以[醋酸菌的含量最多](../Page/醋酸菌.md "wikilink")。這些菌利用茶湯中的糖分及紅茶的[單寧酸作為養分產生](../Page/單寧酸.md "wikilink")[酒精與](../Page/酒精.md "wikilink")[醋酸](../Page/醋酸.md "wikilink")，顯微鏡下可看見桿狀的[乳酸菌](../Page/乳酸菌.md "wikilink")、[醋酸菌和顆粒狀的](../Page/醋酸菌.md "wikilink")[酵母菌附著在一層薄膜上](../Page/酵母菌.md "wikilink")，若培養得宜會看見薄膜漸漸形成大朵菇狀的「茶菇」。紅茶菌[飲料味道微酸](../Page/飲料.md "wikilink")，和酸奶一樣是一種活菌飲品。\[1\]

## 功效

红茶菌是一种发酵茶饮料。主要功能是饮用后能够帮助人体排便正常，增强体抗力，帮助美颜。可治疗多种疾病，包括糖尿病、高血压、便秘、胃痛炎等。\[2\]

## 做法

靠一种名为“菌膜”的菌类，加入红茶，根据喜好加入糖过后，让它根据环境发酵，大约7至12天后即可饮用。除了红茶之外，其它茶类例如绿茶和普洱茶，都能用来发酵。成功发酵后，会增生新的菌膜，新菌膜能够分享给其他人，也能够用做新的茶菌。\[3\]

## 参考文獻

## 参见

  - [益生菌](../Page/益生菌.md "wikilink")
  - [克非尔](../Page/克非尔.md "wikilink")
  - [酸奶](../Page/酸奶.md "wikilink")

{{-}}

[Category:发酵食品](../Category/发酵食品.md "wikilink")
[菌](../Category/紅茶.md "wikilink")
[Category:茶學](../Category/茶學.md "wikilink")

1.  有趣的真菌（三版 2005）頁33，[國立自然科學博物館](../Page/國立自然科學博物館.md "wikilink") 發行
2.
3.