**碧堤半島**（**Bellagio**）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣區的一個私人豪華屋苑](../Page/荃灣區.md "wikilink")，\[1\]位於[深井市中心](../Page/深井.md "wikilink")，\[2\]
原址為[香港生力啤酒廠首代廠房](../Page/香港生力啤酒廠.md "wikilink")，由[會德豐地產及](../Page/會德豐地產.md "wikilink")[九龍倉集團發展](../Page/九龍倉集團.md "wikilink")，由[夏利文住宅管理有限公司負責](../Page/夏利文住宅管理有限公司.md "wikilink")。

屋苑於2003年至2006年建築完成及入伙，共有8座住宅樓宇，提供3,345個住宅單位，而本住宅之特點是鄰近屯門公路和汀九大橋，聲稱20分鐘直達香港國際機場、中區及上環。

## 屋苑資料

  - 住宅座數：8座

| 樓宇名稱 | 期數 | 落成年份 | 層樓 |
| ---- | -- | ---- | -- |
| 第1座  | 3期 | 2006 | 71 |
| 第3座  | 70 |      |    |
| 第2座  | 2期 | 2006 | 71 |
| 第5座  | 69 |      |    |
| 第6座  | 1期 | 2003 | 69 |
| 第7座  | 70 |      |    |
| 第8座  | 71 |      |    |
| 第9座  | 71 |      |    |
|      |    |      |    |

  - 單位總數：3,345個

## 景觀

屋苑南望[青馬大橋海景及遠眺中上環海景](../Page/青馬大橋.md "wikilink")，東邊是[嘉頓麵包廠](../Page/嘉頓麵包.md "wikilink")，西面是深井，北面是[屯門公路](../Page/屯門公路.md "wikilink")。

## 商場

設有面積達30,000平方呎的[商場](../Page/商場.md "wikilink")──[碧堤坊](../Page/碧堤坊.md "wikilink")。

土地註冊處資料顯示，會德豐於2012年6月初以2.8億元沽出深井碧堤半島商場碧堤坊連155個車位，商場面積約3.8萬方呎，呎價約7368元。
由Asia Wealth Development Limited購入

<File:Bellagio> 200608-4.jpg|一期平台花園 <File:Bellagio> 200608-2.jpg|二期平台花園
<File:Bellagio> 200608-3.jpg|兒童遊樂場 <File:Bellagio> 200608-1.jpg|平台游泳池

## 教育及福利設施

### 幼稚園

  - [香港保護兒童會深井幼兒學校](http://www.hkspc.org/st)

### 綜合青少年服務中心

  - [香港小童群益會深井青少年綜合服務中心](https://web.archive.org/web/20180405024336/http://shamtseng.bgca.org.hk/0312/home.aspx?l=C&id=3055)

## 知名人士

  - [麥家琪](../Page/麥家琪.md "wikilink")
  - [梁燕城](../Page/梁燕城.md "wikilink")
  - [梁榮忠](../Page/梁榮忠.md "wikilink")
  - [麥長青](../Page/麥長青.md "wikilink")
  - [祝文君](../Page/祝文君.md "wikilink")
  - [吳卓羲](../Page/吳卓羲.md "wikilink")
  - [陳豪](../Page/陳豪.md "wikilink")
  - [陳茵媺](../Page/陳茵媺.md "wikilink")
  - [黎耀祥](../Page/黎耀祥.md "wikilink")

## 交通

<div class="NavFrame" style="background:yellow;clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background:yellow; margin: 0 auto; padding: 0 10px">

**交通路線列表**

</div>

<div class="NavContent" style="background:yellow;margin: 0 auto; padding: 0 10px">

  - [碧堤半島巴士總站](../Page/碧堤半島巴士總站.md "wikilink")

<!-- end list -->

  - [青山公路](../Page/青山公路.md "wikilink")（深井段）

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 屯門至元朗線（24小時服務）\[3\]
  - 荃灣至元朗線（通宵服務）\[4\]
  - 旺角至元朗線（下午至通宵服務）\[5\]
  - 元朗至佐敦道線（24小時服務）\[6\]
  - 銅鑼灣至元朗線（下午至通宵服務）\[7\]

</div>

</div>

## 参考文献

## 外部參考

  - [碧堤半島官方網頁](http://www.hkbellagio.com/)
  - [碧堤半島物業資料](https://web.archive.org/web/20061106101330/http://www.midland.com.hk/agency/chi/residential/developer_project/bellagio/)
  - [碧堤半島業主討論區](http://www.bellagio-club.com/)
  - [碧堤半島近景地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=824362&cy=825326&zm=4&mx=824362&my=825326&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)
  - [碧堤半島遠景地圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=824362&cy=824275&zm=7&mx=824362&my=825326&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)
  - [雅虎網:ClubBellagio
    碧堤半島業主會](http://hk.groups.yahoo.com/group/ClubBellagio/)
  - [雅虎新聞:碧堤半島](http://hk.realestate.yahoo.com/060925/299/1tou1.html)
    [1](http://hk.realestate.yahoo.com/060912/299/1sz3j.html)

[Category:深井](../Category/深井.md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:荃灣區私人屋苑](../Category/荃灣區私人屋苑.md "wikilink")
[Category:九龍倉置業物業](../Category/九龍倉置業物業.md "wikilink")
[Category:會德豐地產(香港)物業](../Category/會德豐地產\(香港\)物業.md "wikilink")
[Category:香港屏风楼宇](../Category/香港屏风楼宇.md "wikilink")
[Category:2006年完工建築物](../Category/2006年完工建築物.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")

1.
2.
3.  [屯門置樂花園—元朗元朗康樂路](http://www.16seats.net/chi/rmb/r_n60.html)
4.  [荃灣荃灣千色匯＞屯門及元朗](http://www.16seats.net/chi/rmb/r_n53.html)
5.  [旺角新填地街＞屯門及元朗](http://www.16seats.net/chi/rmb/r_kn60.html)
6.  [佐敦道白加士街—屯門及元朗](http://www.16seats.net/chi/rmb/r_kn68.html)
7.  [銅鑼灣波斯富街＞屯門及元朗](http://www.16seats.net/chi/rmb/r_hn68.html)