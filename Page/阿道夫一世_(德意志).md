[Siegel_König_Adolf_von_Nassau.jpg](https://zh.wikipedia.org/wiki/File:Siegel_König_Adolf_von_Nassau.jpg "fig:Siegel_König_Adolf_von_Nassau.jpg")
**阿道夫一世**（**Adolf
I**，约1250年—1298年7月2日），1292年至1298年间任[罗马人民的国王](../Page/德国君主列表.md "wikilink")，登基之前为[拿骚伯爵](../Page/拿骚统治者列表.md "wikilink")，1276年起在任。

阿道夫是拿骚伯爵瓦尔拉姆二世与卡岑奈伦博根的阿德莱德（Adelheid of
Katzenelnbogen）的次子。[特里尔大主教狄特尔](../Page/特里尔大主教.md "wikilink")·冯·拿骚的弟弟。

[神圣罗马帝国七](../Page/神圣罗马帝国.md "wikilink")[选侯于](../Page/选侯.md "wikilink")1292年5月5日在[法兰克福选阿道夫为](../Page/法兰克福.md "wikilink")[罗马人民的國王](../Page/德国君主列表.md "wikilink")，他们认为阿道夫比[鲁道夫一世的长子](../Page/鲁道夫一世_\(德意志\).md "wikilink")[奥地利公爵](../Page/奥地利统治者列表.md "wikilink")[阿尔布雷希特一世更适合做国王](../Page/阿尔布雷希特一世_\(德意志\).md "wikilink")。因为德国贵族们对迅速崛起的[哈布斯堡王朝心怀疑虑](../Page/哈布斯堡王朝.md "wikilink")，不愿看到一个过于强大的王室加于他们头上。既无号召力又无军事势力的阿道夫成了国王的最好人选。阿道夫于7月1日在[亚琛举行了加冕礼](../Page/亚琛.md "wikilink")。他即位后忙于应付选民的要求和阿尔布雷希特的争斗。1294年，阿道夫与[英王](../Page/英国君主列表.md "wikilink")[爱德华一世结盟对付](../Page/爱德华一世.md "wikilink")[法国](../Page/法国.md "wikilink")，因此得到[英国的津贴](../Page/英国.md "wikilink")。在英国的资助下，他多次击败国内的反对势力。选侯对其日益增长的势力感到不安，又将他废黜，选举阿尔布雷希特为对立国王。1298年7月2日，两军在格尔海姆附近的哈森比尔展开了一场骑士会战。阿道夫被打败，在战斗中阵亡。

阿道夫1271年与[伊森堡-林堡的伊玛吉娜结婚](../Page/伊森堡.md "wikilink")，有五子三女：

  - 长子亨利，早夭
  - 次子鲁普雷希特，1304年去世
  - 三子格拉赫一世，1361年去世，拿骚伯爵
  - 四子阿道夫（1292年-1294年）
  - 五子瓦尔拉姆三世（1294年-1324年），拿骚伯爵
  - 长女阿德莱德，1338年卒
  - 次女伊玛吉娜，早夭
  - 三女玛蒂尔德（约1280年-1323年6月19日），1294年与[上巴伐利亚公爵](../Page/巴伐利亚统治者列表.md "wikilink")[鲁道夫一世结婚](../Page/鲁道夫一世_\(巴伐利亚\).md "wikilink")。

[A](../Category/德意志国王.md "wikilink")
[A](../Category/1250年出生.md "wikilink")
[A](../Category/1298年逝世.md "wikilink")
[Category:阵亡军人](../Category/阵亡军人.md "wikilink")