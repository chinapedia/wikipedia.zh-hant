**2007-08赛季斯诺克职业选手世界排名**根据[2006-07赛季与](../Page/2006-2007斯诺克赛季.md "wikilink")[2007-08赛季所有排名赛积分相加排序](../Page/2007-2008斯诺克赛季.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>#[1]</p></th>
<th style="text-align: left;"><p>球员</p></th>
<th style="text-align: left;"><p>国家</p></th>
<th><p>积分[2]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/约翰·希金斯.md" title="wikilink">约翰·希金斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>42,250</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/格雷姆·多特.md" title="wikilink">格雷姆·多特</a></p></td>
<td style="text-align: left;"></td>
<td><p>37,775</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td style="text-align: left;"><p><a href="../Page/肖恩·墨菲.md" title="wikilink">肖恩·墨菲</a></p></td>
<td style="text-align: left;"></td>
<td><p>37,700</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td style="text-align: left;"><p><a href="../Page/肯·达赫迪.md" title="wikilink">肯·达赫迪</a></p></td>
<td style="text-align: left;"></td>
<td><p>35,800</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td style="text-align: left;"><p><a href="../Page/罗尼·奥沙利文.md" title="wikilink">罗尼·奥沙利文</a></p></td>
<td style="text-align: left;"></td>
<td><p>33,600</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td style="text-align: left;"><p><a href="../Page/彼得·艾伯顿.md" title="wikilink">彼得·艾伯顿</a></p></td>
<td style="text-align: left;"></td>
<td><p>33,550</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td style="text-align: left;"><p><a href="../Page/尼尔·罗伯逊.md" title="wikilink">尼尔·罗伯逊</a></p></td>
<td style="text-align: left;"></td>
<td><p>33,125</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td style="text-align: left;"><p><a href="../Page/史蒂芬·亨德利.md" title="wikilink">史蒂芬·亨德利</a></p></td>
<td style="text-align: left;"></td>
<td><p>32,475</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td style="text-align: left;"><p><a href="../Page/丁俊晖.md" title="wikilink">丁俊晖</a></p></td>
<td style="text-align: left;"></td>
<td><p>30,800</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td style="text-align: left;"><p><a href="../Page/史蒂芬·马奎尔.md" title="wikilink">史蒂芬·马奎尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>30,550</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td style="text-align: left;"><p><a href="../Page/马克·塞尔比.md" title="wikilink">马克·塞尔比</a></p></td>
<td style="text-align: left;"></td>
<td><p>27,400</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td style="text-align: left;"><p><a href="../Page/马克·威廉姆斯.md" title="wikilink">马克·威廉姆斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>27,125</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td style="text-align: left;"><p><a href="../Page/史蒂芬·李.md" title="wikilink">史蒂芬·李</a></p></td>
<td style="text-align: left;"></td>
<td><p>26,150</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td style="text-align: left;"><p><a href="../Page/阿利斯特·卡特.md" title="wikilink">阿利斯特·卡特</a></p></td>
<td style="text-align: left;"></td>
<td><p>25,300</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td style="text-align: left;"><p><a href="../Page/史蒂夫·戴维斯.md" title="wikilink">史蒂夫·戴维斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>24,950</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td style="text-align: left;"><p><a href="../Page/瑞恩·戴.md" title="wikilink">瑞恩·戴</a></p></td>
<td style="text-align: left;"></td>
<td><p>24,500</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td style="text-align: left;"><p><a href="../Page/乔·斯维尔.md" title="wikilink">乔·斯维尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>24,350</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td style="text-align: left;"><p><a href="../Page/乔·佩里.md" title="wikilink">乔·佩里</a></p></td>
<td style="text-align: left;"></td>
<td><p>23,925</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td style="text-align: left;"><p><a href="../Page/巴里·霍金斯.md" title="wikilink">巴里·霍金斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>23,750</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td style="text-align: left;"><p><a href="../Page/马修·史蒂文斯.md" title="wikilink">马修·史蒂文斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>23,550</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td style="text-align: left;"><p><a href="../Page/马克·金.md" title="wikilink">马克·金</a></p></td>
<td style="text-align: left;"></td>
<td><p>22,425</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td style="text-align: left;"><p><a href="../Page/杰米·柯普.md" title="wikilink">杰米·柯普</a></p></td>
<td style="text-align: left;"></td>
<td><p>22,250</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td style="text-align: left;"><p><a href="../Page/斯图尔特·宾汉姆.md" title="wikilink">斯图尔特·宾汉姆</a></p></td>
<td style="text-align: left;"></td>
<td><p>22,150</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td style="text-align: left;"><p><a href="../Page/迈克尔·霍尔特.md" title="wikilink">迈克尔·霍尔特</a></p></td>
<td style="text-align: left;"></td>
<td><p>21,613</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td style="text-align: left;"><p><a href="../Page/奈吉尔·邦德.md" title="wikilink">奈吉尔·邦德</a></p></td>
<td style="text-align: left;"></td>
<td><p>20,688</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td style="text-align: left;"><p><a href="../Page/安东尼·汉密尔顿.md" title="wikilink">安东尼·汉密尔顿</a></p></td>
<td style="text-align: left;"></td>
<td><p>20,451</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td style="text-align: left;"><p><a href="../Page/傅家俊.md" title="wikilink">傅家俊</a></p></td>
<td style="text-align: left;"></td>
<td><p>20,425</p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td style="text-align: left;"><p><a href="../Page/伊恩·麦克洛克.md" title="wikilink">伊恩·麦克洛克</a></p></td>
<td style="text-align: left;"></td>
<td><p>19,126</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td style="text-align: left;"><p><a href="../Page/马克·艾伦.md" title="wikilink">马克·艾伦</a></p></td>
<td style="text-align: left;"></td>
<td><p>18,900</p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td style="text-align: left;"><p><a href="../Page/戴维·哈罗德.md" title="wikilink">戴维·哈罗德</a></p></td>
<td style="text-align: left;"></td>
<td><p>18,875</p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td style="text-align: left;"><p><a href="../Page/多米尼克·戴尔.md" title="wikilink">多米尼克·戴尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>18,075</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td style="text-align: left;"><p><a href="../Page/杰拉德·格林.md" title="wikilink">杰拉德·格林</a></p></td>
<td style="text-align: left;"></td>
<td><p>17,900</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td style="text-align: left;"><p><a href="../Page/詹姆斯·瓦塔纳.md" title="wikilink">詹姆斯·瓦塔纳</a></p></td>
<td style="text-align: left;"></td>
<td><p>17,688</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td style="text-align: left;"><p><a href="../Page/迈克尔·佳吉.md" title="wikilink">迈克尔·佳吉</a></p></td>
<td style="text-align: left;"></td>
<td><p>17,550</p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td style="text-align: left;"><p><a href="../Page/大卫·格雷.md" title="wikilink">大卫·格雷</a></p></td>
<td style="text-align: left;"></td>
<td><p>17,163</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td style="text-align: left;"><p><a href="../Page/里奇·沃登.md" title="wikilink">里奇·沃登</a></p></td>
<td style="text-align: left;"></td>
<td><p>16,965</p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td style="text-align: left;"><p><a href="../Page/费加尔·奥布莱恩.md" title="wikilink">费加尔·奥布莱恩</a></p></td>
<td style="text-align: left;"></td>
<td><p>16,950</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td style="text-align: left;"><p><a href="../Page/艾伦·麦克马努斯.md" title="wikilink">艾伦·麦克马努斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>16,525</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td style="text-align: left;"><p><a href="../Page/约翰·帕洛特.md" title="wikilink">约翰·帕洛特</a></p></td>
<td style="text-align: left;"></td>
<td><p>16,288</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td style="text-align: left;"><p><a href="../Page/阿德里安·戈恩内尔.md" title="wikilink">阿德里安·戈恩内尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>15,500</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td style="text-align: left;"><p><a href="../Page/安迪·希克斯.md" title="wikilink">安迪·希克斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,976 &lt;!-- Two columns looks bad on my screen</p></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th style="text-align: left;"><p>球员</p></th>
<th style="text-align: left;"><p>国家</p></th>
<th><p>积分--&gt;</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>42</p></td>
<td style="text-align: left;"><p><a href="../Page/安德鲁·诺曼.md" title="wikilink">安德鲁·诺曼</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,888</p></td>
</tr>
<tr class="even">
<td><p>43</p></td>
<td style="text-align: left;"><p><a href="../Page/马克·戴维斯.md" title="wikilink">马克·戴维斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,775</p></td>
</tr>
<tr class="odd">
<td><p>44</p></td>
<td style="text-align: left;"><p><a href="../Page/安德鲁·希金森.md" title="wikilink">安德鲁·希金森</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,725</p></td>
</tr>
<tr class="even">
<td><p>45</p></td>
<td style="text-align: left;"><p><a href="../Page/大卫·吉尔伯特.md" title="wikilink">大卫·吉尔伯特</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,550</p></td>
</tr>
<tr class="odd">
<td><p>46</p></td>
<td style="text-align: left;"><p><a href="../Page/杰米·伯内特.md" title="wikilink">杰米·伯内特</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,500</p></td>
</tr>
<tr class="even">
<td><p>47</p></td>
<td style="text-align: left;"><p><a href="../Page/罗伯特·米尔金斯.md" title="wikilink">罗伯特·米尔金斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,201</p></td>
</tr>
<tr class="odd">
<td><p>48</p></td>
<td style="text-align: left;"><p><a href="../Page/罗瑞·麦克罗德.md" title="wikilink">罗瑞·麦克罗德</a></p></td>
<td style="text-align: left;"></td>
<td><p>14,025</p></td>
</tr>
<tr class="even">
<td><p>49</p></td>
<td style="text-align: left;"><p><a href="../Page/罗德·罗勒尔.md" title="wikilink">罗德·罗勒尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,650</p></td>
</tr>
<tr class="odd">
<td><p>50</p></td>
<td style="text-align: left;"><p><a href="../Page/汤姆·福特.md" title="wikilink">汤姆·福特</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,475</p></td>
</tr>
<tr class="even">
<td><p>51</p></td>
<td style="text-align: left;"><p><a href="../Page/朱德·特鲁姆普.md" title="wikilink">朱德·特鲁姆普</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,450</p></td>
</tr>
<tr class="odd">
<td><p>52</p></td>
<td style="text-align: left;"><p><a href="../Page/马库斯·坎贝尔.md" title="wikilink">马库斯·坎贝尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,325</p></td>
</tr>
<tr class="even">
<td><p>53</p></td>
<td style="text-align: left;"><p><a href="../Page/斯图尔特·佩特曼.md" title="wikilink">斯图尔特·佩特曼</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,288</p></td>
</tr>
<tr class="odd">
<td><p>54</p></td>
<td style="text-align: left;"><p><a href="../Page/罗宾·赫尔.md" title="wikilink">罗宾·赫尔</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,113</p></td>
</tr>
<tr class="even">
<td><p>55</p></td>
<td style="text-align: left;"><p><a href="../Page/大卫·罗.md" title="wikilink">大卫·罗</a></p></td>
<td style="text-align: left;"></td>
<td><p>13,113</p></td>
</tr>
<tr class="odd">
<td><p>56</p></td>
<td style="text-align: left;"><p><a href="../Page/巴里·品吉斯.md" title="wikilink">巴里·品吉斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>12,638</p></td>
</tr>
<tr class="even">
<td><p>57</p></td>
<td style="text-align: left;"><p><a href="../Page/乔·亨利.md" title="wikilink">乔·亨利</a></p></td>
<td style="text-align: left;"></td>
<td><p>12,500</p></td>
</tr>
<tr class="odd">
<td><p>58</p></td>
<td style="text-align: left;"><p><a href="../Page/乔·德兰尼.md" title="wikilink">乔·德兰尼</a></p></td>
<td style="text-align: left;"></td>
<td><p>12,400</p></td>
</tr>
<tr class="even">
<td><p>59</p></td>
<td style="text-align: left;"><p><a href="../Page/麦克·邓恩.md" title="wikilink">麦克·邓恩</a></p></td>
<td style="text-align: left;"></td>
<td><p>12,288</p></td>
</tr>
<tr class="odd">
<td><p>60</p></td>
<td style="text-align: left;"><p><a href="../Page/吉米·怀特.md" title="wikilink">吉米·怀特</a></p></td>
<td style="text-align: left;"></td>
<td><p>12,175</p></td>
</tr>
<tr class="even">
<td><p>61</p></td>
<td style="text-align: left;"><p><a href="../Page/吉米·米奇.md" title="wikilink">吉米·米奇</a></p></td>
<td style="text-align: left;"></td>
<td><p>11,988</p></td>
</tr>
<tr class="odd">
<td><p>62</p></td>
<td style="text-align: left;"><p><a href="../Page/保罗·戴维斯.md" title="wikilink">保罗·戴维斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>11,713</p></td>
</tr>
<tr class="even">
<td><p>63</p></td>
<td style="text-align: left;"><p><a href="../Page/斯科特·麦肯齐.md" title="wikilink">斯科特·麦肯齐</a></p></td>
<td style="text-align: left;"></td>
<td><p>11,325</p></td>
</tr>
<tr class="odd">
<td><p>64</p></td>
<td style="text-align: left;"><p><a href="../Page/伊恩·普利斯.md" title="wikilink">伊恩·普利斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>11,050</p></td>
</tr>
<tr class="even">
<td><p>65</p></td>
<td style="text-align: left;"><p><a href="../Page/李·斯皮克.md" title="wikilink">李·斯皮克</a></p></td>
<td style="text-align: left;"></td>
<td><p>11,050</p></td>
</tr>
<tr class="odd">
<td><p>66</p></td>
<td style="text-align: left;"><p><a href="../Page/梁文博.md" title="wikilink">梁文博</a></p></td>
<td style="text-align: left;"></td>
<td><p>10,900</p></td>
</tr>
<tr class="even">
<td><p>67</p></td>
<td style="text-align: left;"><p><a href="../Page/乔·乔吉亚.md" title="wikilink">乔·乔吉亚</a></p></td>
<td style="text-align: left;"></td>
<td><p>10,738</p></td>
</tr>
<tr class="odd">
<td><p>68</p></td>
<td style="text-align: left;"><p><a href="../Page/托尼·德拉科.md" title="wikilink">托尼·德拉科</a></p></td>
<td style="text-align: left;"></td>
<td><p>10,450</p></td>
</tr>
<tr class="even">
<td><p>69</p></td>
<td style="text-align: left;"><p><a href="../Page/田鹏飞.md" title="wikilink">田鹏飞</a></p></td>
<td style="text-align: left;"></td>
<td><p>10,225</p></td>
</tr>
<tr class="odd">
<td><p>70</p></td>
<td style="text-align: left;"><p><a href="../Page/马修·考奇.md" title="wikilink">马修·考奇</a></p></td>
<td style="text-align: left;"></td>
<td><p>9,975</p></td>
</tr>
<tr class="even">
<td><p>71</p></td>
<td style="text-align: left;"><p><a href="../Page/戴维·莫里斯.md" title="wikilink">戴维·莫里斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>9,600</p></td>
</tr>
<tr class="odd">
<td><p>72</p></td>
<td style="text-align: left;"><p><a href="../Page/刘菘.md" title="wikilink">刘菘</a></p></td>
<td style="text-align: left;"></td>
<td><p>9,525</p></td>
</tr>
<tr class="even">
<td><p>73</p></td>
<td style="text-align: left;"><p><a href="../Page/马克·乔伊斯.md" title="wikilink">马克·乔伊斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>9,500</p></td>
</tr>
<tr class="odd">
<td><p>74</p></td>
<td style="text-align: left;"><p><a href="../Page/阿尔弗雷德·伯顿.md" title="wikilink">阿尔弗雷德·伯顿</a></p></td>
<td style="text-align: left;"></td>
<td><p>9,500</p></td>
</tr>
<tr class="even">
<td><p>75</p></td>
<td style="text-align: left;"><p><a href="../Page/本·伍勒斯登.md" title="wikilink">本·伍勒斯登</a></p></td>
<td style="text-align: left;"></td>
<td><p>9,075</p></td>
</tr>
<tr class="odd">
<td><p>76</p></td>
<td style="text-align: left;"><p><a href="../Page/克里斯·诺布里.md" title="wikilink">克里斯·诺布里</a></p></td>
<td style="text-align: left;"></td>
<td><p>8,663</p></td>
</tr>
<tr class="even">
<td><p>77</p></td>
<td style="text-align: left;"><p><a href="../Page/肖恩·斯多瑞.md" title="wikilink">肖恩·斯多瑞</a></p></td>
<td style="text-align: left;"></td>
<td><p>8,500</p></td>
</tr>
<tr class="odd">
<td><p>78</p></td>
<td style="text-align: left;"><p><a href="../Page/苏克·阿里.md" title="wikilink">苏克·阿里</a></p></td>
<td style="text-align: left;"></td>
<td><p>7,426</p></td>
</tr>
<tr class="even">
<td><p>79</p></td>
<td style="text-align: left;"><p><a href="../Page/杰夫·坎迪.md" title="wikilink">杰夫·坎迪</a></p></td>
<td style="text-align: left;"></td>
<td><p>6,475</p></td>
</tr>
<tr class="odd">
<td><p>80</p></td>
<td style="text-align: left;"><p><a href="../Page/保罗·威克斯.md" title="wikilink">保罗·威克斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>6,250</p></td>
</tr>
<tr class="even">
<td><p>81</p></td>
<td style="text-align: left;"><p><a href="../Page/詹姆斯·塔顿.md" title="wikilink">詹姆斯·塔顿</a></p></td>
<td style="text-align: left;"></td>
<td><p>3,513</p></td>
</tr>
<tr class="odd">
<td><p>82</p></td>
<td style="text-align: left;"><p><a href="../Page/休·艾伯奈斯.md" title="wikilink">休·艾伯奈斯</a></p></td>
<td style="text-align: left;"></td>
<td><p>1,800</p></td>
</tr>
</tbody>
</table>

## 事件

  - [丁俊晖](../Page/丁俊晖.md "wikilink")、[马克·塞尔比和](../Page/马克·塞尔比.md "wikilink")[瑞恩·戴首次进入前](../Page/瑞恩·戴.md "wikilink")16。
  - [格雷姆·多特](../Page/格雷姆·多特.md "wikilink")、[肖恩·墨菲](../Page/肖恩·墨菲.md "wikilink")、[尼尔·罗伯逊和](../Page/尼尔·罗伯逊.md "wikilink")[阿利斯特·卡特取得生涯最高排名](../Page/阿利斯特·卡特.md "wikilink")
  - 前世界第4[马修·史蒂文斯跌至](../Page/马修·史蒂文斯.md "wikilink")20名。
  - [马克·艾伦和](../Page/马克·艾伦.md "wikilink")[杰米·柯普进入前](../Page/杰米·柯普.md "wikilink")32。
  - [戴维·哈罗德](../Page/戴维·哈罗德.md "wikilink")、[多米尼克·戴尔和](../Page/多米尼克·戴尔.md "wikilink")[杰拉德·格林重回前](../Page/杰拉德·格林.md "wikilink")32。
  - [托尼·德拉科跌出前](../Page/托尼·德拉科.md "wikilink")64。

## 注释

[Category:斯诺克职业选手世界排名](../Category/斯诺克职业选手世界排名.md "wikilink")
[Category:2007年斯诺克](../Category/2007年斯诺克.md "wikilink")
[Category:2008年斯诺克](../Category/2008年斯诺克.md "wikilink")

1.
2.