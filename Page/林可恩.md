**林可恩**（，），原名**林佳媛**，藝名**可恩**，因參加《[le tea
girls](../Page/le_tea_girls.md "wikilink")》選拔賽後出道，雖未獲得名次，但因外型亮麗，進而參與電視偶像劇《[黑糖瑪奇朵](../Page/黑糖瑪奇朵.md "wikilink")》的演出。

## 作品

### 主持

  - Channel V
      - 《[流行 in house](../Page/流行_in_house.md "wikilink")》（助理主持）

### 戲劇

  - 民視／衛視
      - 《[黑糖瑪奇朵](../Page/黑糖瑪奇朵.md "wikilink")》（2007年）飾演女配角 可恩
  - 公視
      - 《[燃燒吧\!機車](../Page/燃燒吧!機車.md "wikilink")》（2007年）

### 電視CF

  - Letea奇異果茶（[張韶涵主演](../Page/張韶涵.md "wikilink")）

## 外部連結

  - [le tea
    girls選拔賽](https://web.archive.org/web/20071009130751/http://blackie.channelv.com/letea/letea_index.shtm)
  - [林可恩blog](http://www.pixnet.net/home/jiayuan0105/)
  - [伊美居傳播](https://web.archive.org/web/20081003135941/http://image20050214.pixnet.net/blog/post/18051303)

[L林](../Category/台灣电视女演員.md "wikilink")
[K可](../Category/林姓.md "wikilink")