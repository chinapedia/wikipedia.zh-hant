**Team NoA**是知名的職業性[絕對武力電玩遊戲的隊伍](../Page/絕對武力.md "wikilink")。創辦人為Jonas
"bsl" Alsaker，於2003年創辦，他們也是第一個電子競技的隊伍將別的隊伍的隊員挖角付錢到自己隊上的隊伍。當年是將Ola
"element" Moum從[SK Gaming挖角過來](../Page/SK_Gaming.md "wikilink")。
Team
NoA駐紮在挪威，再絕對武力的電子競技上的歐洲排名常常佔有一席之位，也是電玩大賽中[WCG](../Page/WCG.md "wikilink")、[CPL上的常客](../Page/CPL.md "wikilink")。

## 隊員

  -   - ""內為遊戲中暱稱。

### [絕對武力](../Page/絕對武力.md "wikilink")

  - Jano Ramos－"J a n i u X"

  - Danny Michael Sørensen－"zonic"

  - Muhamed Eid－"mJe"

  - Alexander Holdt－"ave"

  - Martin Cording－"guddo"（教練）

### [絕對武力：次世代](../Page/絕對武力：次世代.md "wikilink")

  - Jonas Nordqvist－"nordQvist"

  - David Olander－"olander"

  - Ted Svensson－"haunted"

  - Joacim Kroon－"majk"

  - Robin Johansson－"Fifflaren"

### [魔獸爭霸3](../Page/魔獸爭霸3.md "wikilink")

  - Enes 'JeBiGa' Karasuljic

  - Sascha 'Scarto' Vollmehr

<!-- end list -->

  - Min Hyuk 'MinHyuk' Jo

  - Park se 'Showbu-' Ryong

  - Park chul 'Shy' Woo

  - Park Sueng 'Space-' Hyun

  - Dong 'Ohjie.' Ryong Lim

  - Daeduck 'Kei' Yoon

### 著名隊員

  - Jonas "bsl" Alexander vikan（退休）

  - Lars "Naikon" Olaisen（目前在[MYM](../Page/MYM.md "wikilink")）

  - Jorgen "XeqtR" Johannessen（2006年7月19日退休）

  - Griffin "shaGuar" Benger（轉隊到[Team
    3D](../Page/Team_3D.md "wikilink")，目前在[Team
    EG](../Page/Team_EG.md "wikilink")）

  - Michael "method" So（轉隊到[Team 3D](../Page/Team_3D.md "wikilink")）

  - Ola "elemeNt" Moum（轉隊到MIBR，目前退休）

  - Hallvard "Knoxville" Dehli（退休）

  - Preben "prb" Gammelsæter（轉隊到MYM）

  - Paddy

## 世界冠軍

  - 2004年－[CPL](../Page/CPL.md "wikilink") \[1\]
  - 2005年－[WEG](../Page/WEG.md "wikilink")

## 得獎事蹟

  - 2007年法國[ESWC](../Page/ESWC.md "wikilink")－第二名
  - NGL ONE第二季決賽－第三名
  - 2007年SHG公開賽－第三名
  - 2006年[WCG](../Page/WCG.md "wikilink")[蒙扎站](../Page/蒙扎.md "wikilink")－第四名
  - 2006年Clanbase歐洲盃－第一名
  - 2006年SHG公開賽－第一名
  - 2005年[WEG第三季](../Page/WEG.md "wikilink")－第五名
  - 2005年WEG Qualifier Stockholm－第二名
  - 2005年[CPL](../Page/CPL.md "wikilink") UK Sheffield－第四名
  - 2005年[WEG第一季](../Page/WEG.md "wikilink")－第一名
  - 2004年[CPL冬季賽](../Page/CPL.md "wikilink")－第一名
  - 2004年美國[ESWC](../Page/ESWC.md "wikilink")－第一名
  - 2003年[CPL達拉斯冬季賽](../Page/CPL.md "wikilink")－第二名

## 世界排名

  - 世界排名：第四名（09-2007）\[2\]
  - 現役排名：第4名\[3\]

## 参考

<div class="references-small">

<references />

</div>

## 連結

  - [Team
    NoA官方網站](https://web.archive.org/web/20070202002842/http://www.teamnoa.eu/)
  - [GotFrag網站資訊](https://web.archive.org/web/20071011212727/http://www.gotfrag.com/cs/stats/team/11/)

[Category:戰慄時空系列](../Category/戰慄時空系列.md "wikilink")
[Category:电子竞技团队](../Category/电子竞技团队.md "wikilink")

1.  [CPL](http://www.thecpl.com/?p=1033)
2.  [ZoneRank](http://www.zonerank.com/demo/view_regular/1679)
3.  [Top Player Information: Counter Strike » September
    '07](http://www.zonerank.com/site/playerlist/221?ln=us)