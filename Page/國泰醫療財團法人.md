財團法人國泰綜合醫院，創立於西元1974年，由[國泰人壽捐助成立](../Page/國泰人壽.md "wikilink")。2008年12月更名為**國泰醫療財團法人**，目前旗下有三家醫院及一家診所。

## 歷史

[Cathay_General_Hospital_HQ_20160723.jpg](https://zh.wikipedia.org/wiki/File:Cathay_General_Hospital_HQ_20160723.jpg "fig:Cathay_General_Hospital_HQ_20160723.jpg")
[Cathay_General_Hospital_HQ_1st_Branch_20160723.jpg](https://zh.wikipedia.org/wiki/File:Cathay_General_Hospital_HQ_1st_Branch_20160723.jpg "fig:Cathay_General_Hospital_HQ_1st_Branch_20160723.jpg")
[Cathay_General_Hospital_HQ_2nd_Branch_20160723.jpg](https://zh.wikipedia.org/wiki/File:Cathay_General_Hospital_HQ_2nd_Branch_20160723.jpg "fig:Cathay_General_Hospital_HQ_2nd_Branch_20160723.jpg")
國泰綜合醫院於西元1977年2月15日開幕，由於與[臺大醫學院及](../Page/臺大醫學院.md "wikilink")[臺大醫院取得](../Page/國立臺灣大學醫學院附設醫院.md "wikilink")[建教合作關係](../Page/建教合作.md "wikilink")，因而奠定良好的基礎。成立之初，規劃為一般病床300床，初期開放180床，並配合臺大醫學院推行醫師專勤制度，提供臺大醫院醫師兼診，提升醫療水準。

國泰綜合醫院（院本部）分別於西元1984年、西元1986年、西元1995年完成擴建工程，病床數亦增加至六百床。西元1997年10月，正式成立內湖分院，設有一般病床100床，並增設精神科。西元2000年成立國泰臨床醫學研究中心。西元2002年8月，成立[新竹分院](../Page/新竹市.md "wikilink")（後改稱新竹國泰醫院），設有一般病床217床、特殊病床131床。西元2005年12月7日，成立[汐止分院](../Page/汐止區.md "wikilink")（後改稱汐止國泰醫院），設有一般病床480床、特殊病床111床。西元2008年4月1日，內湖分院歇業，改由內湖國泰診所繼續服務。西年2008年12月，配合《醫療法》修正條文，更名為國泰醫療財團法人，所屬醫療機構併同更名。

## 各院介紹

| 院區                | 創立日期         | 地址                                                                                | 備註                                                         |
| ----------------- | ------------ | --------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| **國泰綜合醫院本館暨第一分館** | 西元1977年2月15日 | [臺北市](../Page/臺北市.md "wikilink")[大安區仁愛路四段](../Page/大安區_\(臺北市\).md "wikilink")280號 | 西元2001年（民國90年）獲評為[醫學中心](../Page/臺灣醫學中心列表.md "wikilink")。   |
| **國泰綜合醫院第二分館**    |              | 臺北市大安區仁愛路四段266巷6號                                                                 |                                                            |
| **汐止國泰綜合醫院**      | 西元2005年12月7日 | [新北市](../Page/新北市.md "wikilink")[汐止區建成路](../Page/汐止區.md "wikilink")59巷2號          | 西元2009年（民國98年）獲評為新制醫院評鑑「優等」。                               |
| **新竹國泰綜合醫院**      | 西元2002年8月8日  | [新竹市](../Page/新竹市.md "wikilink")[東區中華路二段](../Page/東區_\(新竹市\).md "wikilink")678號   | 西元2003年（民國92年）獲評為地區教學醫院。                                   |
| **內湖國泰診所**        |              | [台北市](../Page/台北市.md "wikilink")[內湖區瑞光路](../Page/內湖區.md "wikilink")506號           | 原內湖分院成立於西元1997年10月，因原大樓租約到期，西元2008年3月31日結束營業，另覓新址設立內湖國泰診所。 |

## 參見

  - [臺灣醫院列表](../Page/臺灣醫院列表.md "wikilink")
  - [中華民國私法人醫院列表](../Page/中華民國私法人醫院列表.md "wikilink")
  - [眾恩祠](../Page/眾恩祠.md "wikilink")

## 外部連結

  - [國泰綜合醫院首頁](http://www.cgh.org.tw/)

[G國](../Category/霖園集團.md "wikilink")
[A](../Category/台灣醫學中心.md "wikilink")
[G國](../Category/臺北市醫院.md "wikilink")
[G國](../Category/新北市醫院.md "wikilink")
[G國](../Category/新竹市醫院.md "wikilink")
[G國](../Category/總部位於臺北市大安區的工商業機構.md "wikilink")
[G國](../Category/中華民國醫療財團法人.md "wikilink")
[G國](../Category/1977年台灣建立.md "wikilink")
[G國](../Category/1977年完工醫院.md "wikilink")
[G國](../Category/2002年台灣建立.md "wikilink")
[G國](../Category/2002年完工醫院.md "wikilink")
[G國](../Category/2005年台灣建立.md "wikilink")
[G國](../Category/2005年完工醫院.md "wikilink")