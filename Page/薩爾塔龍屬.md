**薩爾塔龍屬**（[屬名](../Page/學名.md "wikilink")：，意為「[薩爾塔的蜥蜴](../Page/薩爾塔.md "wikilink")」）又名**索他龍**，是種[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於晚[白堊紀](../Page/白堊紀.md "wikilink")。薩爾塔龍在蜥腳類恐龍當中相當小，但對人類而言還是很巨大。牠們擁有類似[梁龍科的頭部](../Page/梁龍科.md "wikilink")，牙齒僅位在嘴部的後方，而且牙齒是鈍的。薩爾塔龍是第一個被發現有鱗甲的蜥腳類恐龍，皮膚上嵌有小型骨版，這些骨板由[皮內成骨](../Page/皮內成骨.md "wikilink")（Osteoderms）構成，大型骨板四處散佈，如人的手掌大小，小型骨板緊湊排列，只有碗豆大小；其他[泰坦巨龍類恐龍身上也發現了骨板](../Page/泰坦巨龍類.md "wikilink")，某些梁龍科化石的背上也曾發現一排鱗甲。當薩爾塔龍類的骨板首次被發現時，因為是獨立於骨骸被發現，所以被推論屬於[甲龍類恐龍](../Page/甲龍類.md "wikilink")。

薩爾塔龍的屬名（*Saltasaurus*）取自於[阿根廷西北部的](../Page/阿根廷.md "wikilink")[薩爾塔省](../Page/薩爾塔省.md "wikilink")，也是首次發現牠們化石的地點。薩爾塔龍的化石也發現於[烏拉圭](../Page/烏拉圭.md "wikilink")。薩爾塔龍的屬名有時會與[三疊紀的](../Page/三疊紀.md "wikilink")[跳龍](../Page/跳龍.md "wikilink")（*Saltopus*）產生混淆，然而這兩個屬非常地不相似。

## 敘述

[Saltasaurus_SIZE_01.jpg](https://zh.wikipedia.org/wiki/File:Saltasaurus_SIZE_01.jpg "fig:Saltasaurus_SIZE_01.jpg")
薩爾塔龍是由[約瑟·波拿巴](../Page/約瑟·波拿巴.md "wikilink")（José
Bonaparte）與[傑米·鮑威爾](../Page/傑米·鮑威爾.md "wikilink")（Jaime
E.
Powell）在1980年首次敘述、命名，牠們被估計身長為12公尺，而體重有7公噸。如同所有蜥腳類恐龍，薩爾塔龍也是[草食性動物](../Page/草食性.md "wikilink")。[烏拉圭也有發現部分化石](../Page/烏拉圭.md "wikilink")。牠們頸部結構顯示牠們無法將頭部高抬過肩膀。

薩爾塔龍屬目前僅有一個種，**護甲薩爾塔龍**（*S. loricatus*）。強壯薩爾塔龍（*S.
robustus*）不再認為是個獨立的種，而南方薩爾塔龍（*S.
australis*）現在被認為是獨立的[內烏肯龍屬](../Page/內烏肯龍.md "wikilink")。

目前所發現的薩爾塔龍化石包含：[脊椎](../Page/脊椎.md "wikilink")、四肢骨頭、數個頜部骨頭、以及不同的骨甲。某些骨甲具有尖刺。薩爾塔龍的化石都發現於阿根廷的Lecho地層，該地屬於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[坎潘階到](../Page/坎潘階.md "wikilink")[馬斯垂克階](../Page/馬斯垂克階.md "wikilink")。

中段[尾椎的椎體延長](../Page/尾椎.md "wikilink")。脊椎側孔的外形接近淺凹處，[馬拉威龍](../Page/馬拉威龍.md "wikilink")、[阿拉摩龍](../Page/阿拉摩龍.md "wikilink")、[風神龍](../Page/風神龍.md "wikilink")、[岡瓦納巨龍也有類似的脊椎特徵](../Page/岡瓦納巨龍.md "wikilink")。毒癮龍也有類似的脊椎特徵，但脊椎側孔更深入脊椎，形成兩個脊椎腔室\[1\]。

薩爾塔龍的其他特徵包含：每節[頸椎都有一個骨質棘](../Page/頸椎.md "wikilink")、髖帶多出一節脊椎骨、尾椎擁有互相交鎖球窩關節。曾有理論認為，牠們可能以後肢站起，並將尾巴當作第三支柱，以接觸到較高的樹枝。

與[毒癮龍相比](../Page/毒癮龍.md "wikilink")，薩爾塔龍的體型較為粗壯\[2\]。
[Titanosaur_nesting.jpg](https://zh.wikipedia.org/wiki/File:Titanosaur_nesting.jpg "fig:Titanosaur_nesting.jpg")挖掘洞穴、產蛋的想像圖\]\]
[Saltasaurus_loricatus_egg.JPG](https://zh.wikipedia.org/wiki/File:Saltasaurus_loricatus_egg.JPG "fig:Saltasaurus_loricatus_egg.JPG")

## 不斷改變的敘述

在[白堊紀時期](../Page/白堊紀.md "wikilink")，[北美洲的](../Page/北美洲.md "wikilink")[蜥腳類恐龍失去優勢](../Page/蜥腳類.md "wikilink")[草食性恐龍地位](../Page/草食性.md "wikilink")，這個時期的北美洲優勢草食性動物是[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")，例如[埃德蒙頓龍](../Page/埃德蒙頓龍.md "wikilink")。然而，[澳洲與](../Page/澳洲.md "wikilink")[南美洲在當時是島嶼大陸](../Page/南美洲.md "wikilink")，從未出現過鴨嘴龍類恐龍，而蜥腳類持續在南方大陸繼續牠們的演化途徑。

薩爾塔龍是高度演化的蜥腳類恐龍之一，牠們生存於7,500萬到6,500萬年前。當1980年首次發現薩爾塔龍的化石時，[古生物學家開始重新思考蜥腳類恐龍的定義](../Page/古生物學家.md "wikilink")，因為薩爾塔龍的確是種蜥腳類恐龍，但身上卻有骨板，直徑介於0.5到11公分。在這之前，蜥腳類恐龍被認為以牠們巨大的體型作為防禦手段。之後，古生物學家們開始重新思考其他的蜥腳類恐龍可能也擁有骨板，例如：[阿根廷的](../Page/阿根廷.md "wikilink")[拉布拉達龍](../Page/拉布拉達龍.md "wikilink")。

## 蛋化石

在1997年，[路易斯·齊亞比](../Page/路易斯·齊亞比.md "wikilink")（Luis
Chiappe）與他的團隊在[阿根廷](../Page/阿根廷.md "wikilink")[巴塔哥尼亞的Auca](../Page/巴塔哥尼亞.md "wikilink")
Mahuevo附近，發現了一個大面積的[泰坦巨龍類集體蛋巢](../Page/泰坦巨龍類.md "wikilink")。每個蛋巢平均有約25顆恐龍蛋。這些小型恐龍蛋，長度約11到12公分，內部有時[化石化的胚胎](../Page/化石.md "wikilink")，這些完整胚胎擁有皮膚痕跡，但無法顯示是否有任何真皮組織或是羽毛。這些恐龍蛋被認為屬於薩爾塔龍。這個遺跡很明顯地是數百隻雌性個體挖掘洞穴，產下牠們的蛋，並用泥土或植被復蓋恐龍蛋。這顯示出牠們是群居動物，牠們可能就由群體行動以及骨板，來抵抗大型掠食動物的攻擊，例如[阿貝力龍類](../Page/阿貝力龍類.md "wikilink")\[3\]。除此之外，在[西班牙也有發現泰坦巨龍類的蛋巢](../Page/西班牙.md "wikilink")。

## 大眾文化

薩爾塔龍曾出現在[探索頻道的電視節目](../Page/探索頻道.md "wikilink")《[恐龍星球](../Page/恐龍星球.md "wikilink")》（*Dinosaur
Planet*）第二集。

## 相關書籍

  - *Walking on Eggs: The Astonishing Discovery of Thousands of Dinosaur
    Eggs in the Badlands of Patagonia*, by Luis Chiappe and Lowell
    Dingus. June 19, 2001, Scribner. ISBN 978-0-7432-1211-3.

  -
## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [Sauropodomorpha: Titanosauridae:
    'Saltasaurus''](https://web.archive.org/web/20080615211132/http://www.palaeos.com/Vertebrates/Units/330Sauropodomorpha/330.600.html#Saltasaurus),
    by M. Alan Kazlev, from Palæos.
  - [The late Cretaceous nesting grounds of
    Patagonia](http://www.luisrey.ndtilda.co.uk/html/patagonia.htm), by
    Luis Rey, from his art gallery.

[Category:薩爾塔龍亞科](../Category/薩爾塔龍亞科.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")

1.  Tidwell, V., Carpenter, K. & Meyer, S. 2001. New Titanosauriform
    (Sauropoda) from the Poison Strip Member of the Cedar Mountain
    Formation (Lower Cretaceous), Utah. In: Mesozoic Vertebrate Life. D.
    H. Tanke & K. Carpenter (eds.). Indiana University Press, Eds. D.H.
    Tanke & K. Carpenter. Indiana University Press. 139-165.

2.
3.  Coria, R.A. and Chiappe, L.M. 2007.[Embryonic Skin From Late
    Cretaceous Sauropods (Dinosauria) of Auca Mahuevo, Patgonia,
    Argentina.](http://jpaleontol.geoscienceworld.org/cgi/content/abstract/81/6/1528)
    Journal of Paleontology v81(6):1528-1532 DOI: 10.1666/05-150.1