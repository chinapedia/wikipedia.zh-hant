**TBS**可以指：

  - [東京放送控股](../Page/東京放送控股.md "wikilink")（**T**okyo **B**roadcasting
    **S**ystem Holdings,
    Inc.），前身為**東京廣播公司**（簡稱同樣為TBS），是[日本一家](../Page/日本.md "wikilink")[媒體](../Page/傳播媒體.md "wikilink")[控股公司](../Page/控股公司.md "wikilink")，旗下有兩大[子公司](../Page/子公司.md "wikilink")：

      - [TBS電視台](../Page/TBS電視台.md "wikilink")，是以[關東廣域圈為播放範圍的](../Page/關東廣域圈.md "wikilink")[無線](../Page/地面電視.md "wikilink")[電視台](../Page/電視台.md "wikilink")，同時為日本[五大商業電視台之一](../Page/核心局.md "wikilink")。
      - [TBS廣播](../Page/TBS廣播.md "wikilink")，是以關東廣域圈為播放範圍的[調幅（AM）](../Page/調幅廣播.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")。

  - [提比-{里}-西國際機場的](../Page/提比里西國際機場.md "wikilink")[IATA](../Page/國際航空運輸協會.md "wikilink")[機場代碼](../Page/国际航空运输协会机场代码.md "wikilink")。

  - [土魯斯商學院](../Page/土魯斯商學院.md "wikilink")（**T**oulouse **B**usiness
    **S**chool）

  - [真佛宗](../Page/真佛宗.md "wikilink")（**T**rue **B**uddha **S**chool）

  - [-{回合制}-策略遊戲](../Page/回合制策略遊戲.md "wikilink")（**t**urn-**b**ased
    **s**trategy）

  - [透納廣播公司](../Page/透納廣播公司.md "wikilink")（**T**urner **B**roadcasting
    **S**ystem）

      - [TBS
        (美國電視頻道)](../Page/TBS_\(美國電視頻道\).md "wikilink")，是透納廣播公司旗下的一個[美國](../Page/美國.md "wikilink")[電視頻道](../Page/電視頻道.md "wikilink")。

  - [交通放送](../Page/交通放送.md "wikilink")（**T**raffic **B**roadcasting
    **S**ystem），是[韓國的一個主要提供](../Page/韓國.md "wikilink")[道路](../Page/道路.md "wikilink")[交通情報的](../Page/交通.md "wikilink")[調頻](../Page/調頻.md "wikilink")[廣播聯播網](../Page/廣播聯播網.md "wikilink")。

  - （**T**he **B**asic
    **S**chool），是[美國海軍陸戰隊的](../Page/美國海軍陸戰隊.md "wikilink")[軍事學校](../Page/軍事學校.md "wikilink")。

  - [龐客星期天](../Page/龐客星期天.md "wikilink")（**T**aking **B**ack
    **S**unday），是[美國的一個](../Page/美國.md "wikilink")[另類搖滾](../Page/另類搖滾.md "wikilink")[樂團](../Page/樂團.md "wikilink")。

  - [臺灣書店](../Page/臺灣書店.md "wikilink")（**T**aiwan **B**ook **S**tore）

  - [台灣公共廣播電視集團](../Page/台灣公共廣播電視集團.md "wikilink")（**T**aiwan
    **B**roadcasting **S**ystem）

  - [艦艇間通話](../Page/艦艇間通話.md "wikilink")（**t**alk **b**etween
    **s**hips），是[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")[美國海軍](../Page/美國海軍.md "wikilink")[艦艇在](../Page/艦艇.md "wikilink")[戰術行動中的通話](../Page/戰術.md "wikilink")。

  - [腾讯浏览服务](../Page/腾讯浏览服务.md "wikilink")（**T**encent **B**rowsing
    **S**ervice），是[腾讯为开发人员提供的多能力浏览解决方案](../Page/腾讯.md "wikilink")。