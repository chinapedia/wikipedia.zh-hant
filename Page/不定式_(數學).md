在[微積分和](../Page/微積分.md "wikilink")[數學分析的其他](../Page/數學分析.md "wikilink")[分支中](../Page/分支.md "wikilink")，**不定式**（又稱**未定式**）是指這樣一類[極限](../Page/極限_\(數學\).md "wikilink")，其在按[極限的運算規則進行](../Page/極限_\(數學\)#常用性質.md "wikilink")[代入後](../Page/代数.md "wikilink")，還未能得到足夠[信息去確定](../Page/信息.md "wikilink")[極限值](../Page/極限值.md "wikilink")。这个[术语最初由](../Page/术语.md "wikilink")[柯西的学生](../Page/奥古斯丁·路易·柯西.md "wikilink")在[19世紀中葉提出](../Page/19世紀.md "wikilink")。常見的不定式有：\(\frac00,~\frac{\infty}{\infty},~0\times\infty,~1^\infty,~\infty-\infty,~0^0\text{ 和 }~\infty^0\)。
處理計算未定式的值常見的方法為使用[羅必達法則](../Page/羅必達法則.md "wikilink")。

## 例子

### 0除以0

\(\frac{0}{0}\) 是不定式。

### 0的0次方

\(0^0\)也是不定式。在絕大多數[網路及](../Page/網路.md "wikilink")[電腦](../Page/電腦.md "wikilink")[計算機內顯示為一](../Page/計算機.md "wikilink")。

## 物理

在[物理学上这是有一定的](../Page/物理学.md "wikilink")[解释](../Page/解释.md "wikilink")。比如说[电阻](../Page/电阻.md "wikilink")[定义](../Page/定义.md "wikilink")
\(R=\frac{V}{I}\)，当[电压和](../Page/电压.md "wikilink")[电流都为](../Page/电流.md "wikilink")
\(0\) 时 \(R\) 的存在[不确定性](../Page/不确定性.md "wikilink")。

例如，极限

\[\lim_{x\to c}{f(x) \over g(x)}\] \(f(c)=g(c)=0\,\)。若 \(f(x)\,\) 等于
\(g(x)\,\)，极限为一；若 \(f(x)\,\) [等于](../Page/等于.md "wikilink") \(g(x)\,\)
的[两倍](../Page/倍数.md "wikilink")，则极限为二。

更一般地，\(\frac00\) 的极限可以通过[洛必达法则求得](../Page/洛必达法则.md "wikilink")。

[Category:数学分析](../Category/数学分析.md "wikilink")
[Category:极限](../Category/极限.md "wikilink")