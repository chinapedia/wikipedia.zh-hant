**TOM-Skype**，是[TOM集团旗下北京讯能网络有限公司](../Page/TOM集团.md "wikilink")[TOM在线和](../Page/TOM在线.md "wikilink")[Skype于](../Page/Skype.md "wikilink")[2004年11月在](../Page/2004年11月.md "wikilink")[中国大陆联合推出的Skype中国大陆版即时通讯软件](../Page/中国大陆.md "wikilink")。从2007年开始，访问Skype官方网站skype.com的大陆用户会被重定向至Tom版Skype网站skype.tom.com。截至2007年6月底，TOM-Skype注册用户已经突破4200万，超越[美国成为Skype全球最大的市场](../Page/美国.md "wikilink")。TOM在线也为中国大陆用户推出了几款仅能在TOM-Skype使用的中国大陆特别优惠卡和电话月费计划（如Skype国内卡，国际版Skype无法使用）。然而，TOM-Skype由于其含有文字审查过滤系统，故该版本软件的安全性和隐私性一直备受诟病。2013年11月20日，微软与TOM集团终止合作。

## 审查

2008年10月，[加拿大](../Page/加拿大.md "wikilink")[多伦多大学Citizen实验室的研究人员发现](../Page/多伦多大学.md "wikilink")，中国版TOM-Skype（非简体中文国际版），会根据关键字将用户的文字聊天记录和用户信息上传并存放在可以从公司外部进入的8个中国大陆服务器上，被记录的聊天信息中包含了与政治相关的内容。由于缺乏足够保密措施，这些记录很容易被第3方获取，从而威胁到了用户的隐私。\[1\]\[2\]\[3\]

2011年8月，[美國](../Page/美國.md "wikilink")[新墨西哥大學以](../Page/新墨西哥大學.md "wikilink")[逆向工程獲得TOM](../Page/逆向工程.md "wikilink")-Skype的審查方法。\[4\]

### 关键词过滤

2006年6月，多伦多大学的互联网审查研究专家Nart
Villeneuve介绍了他对Tom-Skype的测验结果。他在下载Tom-Skype软件的[英文](../Page/英文.md "wikilink")2.0版和2.5
beta版后，对该软件进行测验。他查出：

  - Tom-Skype使用敏感词筛选装置来删除或屏蔽文字聊天内容。在Tom-Skype的根目录下，有ContentFilter.exe（过滤词程序），并且该程序与Tom-Skype同时运行。此程序將和Tom-Skype的伺服器建立兩個連接，並下載一個關鍵詞文件。

<!-- end list -->

  - Tom-Skype似乎不過濾用戶名。Villeneuve得以用“falun99”登錄Tom-Skype。
  - 只要含有一个过滤词，如“[fuck](../Page/wikt:en:fuck.md "wikilink")”，整句话就会被全部屏蔽。

<!-- end list -->

  - 過濾在客戶端完成。Skype用戶可以接收到Tom-skype用戶無法接收的、含有被禁關鍵詞的信息。\[5\]

2008年10月1日，Nart
Villeneuve發佈報告，在其2006年工作的基礎上，進一步研究了Tom-Skype的過濾和監視功能。他發現：

  - Tom-Skype掃描和過濾用戶的發出的文字信息，包括那些用正常Skype同Tom-Skype用戶之間的文字信息。掃描和過濾后產生的信息將上載到位于中國的伺服器並儲存在那裏。

<!-- end list -->

  - Tom-Skype獲得的用戶文字信息，以及其他各種含用戶個人信息的記錄，被存放在不安全的、公衆可以登錄的伺服器上。用於解密這些資料的密鈅也在該伺服器上。

<!-- end list -->

  - Tom-Skype截留的文字信息一般含有政治上敏感的關鍵詞，比如臺灣獨立、法輪功以及政治上反對中國共產黨的信息。
  - 分析顯示，Tom-Skype不僅僅簡單的用關鍵詞過濾。對某些用戶名的監視似乎也存在。\[6\]

2008年10月2日，《华尔街日報》和《[纽约时报](../Page/纽约时报.md "wikilink")》报道，中国政府现在已經开始全面监控该版本的Skype用户文本内容，并将含有政治的内容存储到政府所属服务器上，存储的内容包括所有交谈文本以及用户的认证信息。当前过滤的关键字有[法轮功及与其相关的词汇](../Page/法轮功.md "wikilink")、[藏獨](../Page/西藏獨立運動.md "wikilink")、[奶粉](../Page/三鹿奶粉污染事件.md "wikilink")、[中国共产党及](../Page/中国共产党.md "wikilink")[民运等](../Page/中国民主运动.md "wikilink")。\[7\]\[8\]\[9\]

2013年3月8日，《彭博商業周刊》和CNET網站同時報道，美國新墨西哥大學計算機科學係研究生Jeffrey
Knockel在歷時兩年后，成功破解了TOM-Skype的大部分關鍵詞名單。對於TOM-Skype違反言論自由的行爲，微軟不予置評。\[10\]\[11\]Jeffrey
Knockel在其個人網站上，定期發佈他獲得的TOM-Skype關鍵詞名單。\[12\]

## Skype公司的反应

Skype指TOM-Skype，在2006年已經按照[中华人民共和国政府的规定](../Page/中华人民共和国政府.md "wikilink")，使用文本过滤器来监控敏感信息。不过包含敏感关键字的信息会在客户端被丢弃，故对用户的隐私并无威胁。不过，TOM-Skype后来在未经Skype同意的情况下擅自改变了该过滤器的功能，存储了部分用户的信息。随后，Skype的总裁Josh
Silverman在Skype官方网志上发表声明，表示自己公司很“无辜”和“无奈”。\[13\]他又指出这份报告只涉及用户有使用TOM-Skype软件来进行文本通信的情况。如果双方都使用来自官方网站Skype.com的软件，那么不会受到任何影响。最后，Skype作出了道歉声明。\[14\]

## TOM集团和Skype的关系

Skype是一家[互联网通信公司](../Page/互联网.md "wikilink")，母公司是[微软](../Page/微软.md "wikilink")；Skype与Tom在线在中国设立了合营公司TOM-Skype，而Tom在线是这家合营公司的大[股东](../Page/股东.md "wikilink")。

TOM集团是TOM-Skype大股东TOM在线的母公司。TOM集团在一封电子邮件声明中称：“作为一家中国公司，我们必须遵从中国的规章制度。我们对此没有其他评论”。\[15\]
2013年11月20日，終止合作。

## 注释

## 参见

  - [Skype](../Page/Skype.md "wikilink")
  - [中华人民共和国网络审查](../Page/中华人民共和国网络审查.md "wikilink")

[Category:中國已結業公司](../Category/中國已結業公司.md "wikilink")

1.
2.
3.
4.  [Three Researchers, Five Conjectures: An Empirical Analysis of
    TOM-Skype Censorship and
    Surveillance](http://www.usenix.org/events/foci11/tech/final_files/Knockel.pdf)
5.
6.
7.  Dynamic Internet Technology Inc. Alleges Skype Redirects Users in
    China to Censorware Version - Ten Days After Users Are Able To
    Download Freegate Software Through Skype, TMCnet, September 24, 2007
8.  "Evidence Suggests China's SkypeIs Monitoring Internet Messages", by
    Jason Dean, Wall Street Journal, October 2, 2008
9.
10.
11.
12.
13.
14.
15. "Skype says texts are censored by China", FT.com, Financial Times
    (April 18, 2006).