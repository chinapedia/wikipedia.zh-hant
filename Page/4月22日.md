**4月22日**在一年当中是第112天（[闰年则是](../Page/闰年.md "wikilink")113天），距离一年的结束还有253天。

## 大事记

### 4世紀

  - [353年](../Page/353年.md "wikilink")：[王羲之书写](../Page/王羲之.md "wikilink")[兰亭集序](../Page/兰亭集序.md "wikilink")。

### 15世紀

  - [1500年](../Page/1500年.md "wikilink")：[葡萄牙航海家](../Page/葡萄牙.md "wikilink")[佩德罗·卡布拉尔率领的船队到达](../Page/佩德罗·卡布拉尔.md "wikilink")[巴西](../Page/巴西.md "wikilink")，成为首批到达的[欧洲人](../Page/欧洲.md "wikilink")。

### 16世紀

  - [1509年](../Page/1509年.md "wikilink")：[亨利八世接任前一天去世的父亲](../Page/亨利八世.md "wikilink")[亨利七世成为新的](../Page/亨利七世_\(英格兰\).md "wikilink")[英格兰国王](../Page/英格兰.md "wikilink")。

### 20世紀

  - [1906年](../Page/1906年.md "wikilink")：[1906年奥运会](../Page/届间运动会.md "wikilink")[希腊](../Page/希腊.md "wikilink")[雅典开幕](../Page/雅典.md "wikilink")。这次综合运动会是在第三届与第四届的奥林匹克运动会之间所举办的，故称为“届间运动会”，是届综合运动会也没有被[国际奥林匹克委员会承认](../Page/国际奥林匹克委员会.md "wikilink")。
  - [1912年](../Page/1912年.md "wikilink")：[俄国](../Page/俄国.md "wikilink")[布尔什维克在](../Page/布尔什维克.md "wikilink")[圣彼得堡出版首份](../Page/圣彼得堡.md "wikilink")《[真理报](../Page/真理报.md "wikilink")》。
  - [1915年](../Page/1915年.md "wikilink")：在[第一次世界大战中](../Page/第一次世界大战.md "wikilink")，[德军使用](../Page/德军.md "wikilink")[氯气攻擊](../Page/氯气.md "wikilink")[协约国联军](../Page/协约国.md "wikilink")，这是人类首次大规模使用[化学武器](../Page/化学武器.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")：[英国](../Page/英国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[日本](../Page/日本.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[法国召开旨在限制和削减](../Page/法国.md "wikilink")[海军军备的伦敦海军军备会议](../Page/海军.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[二戰](../Page/二戰.md "wikilink")[柏林戰役](../Page/柏林戰役.md "wikilink")：[蘇聯紅軍開入](../Page/蘇聯紅軍.md "wikilink")[柏林](../Page/柏林.md "wikilink")，守城的[納粹德國軍隊頑強抵抗](../Page/納粹德國.md "wikilink")。
  - [1951年](../Page/1951年.md "wikilink")：[朝鲜战争](../Page/朝鲜战争.md "wikilink")：[中国人民志愿军与部署前线的](../Page/中国人民志愿军.md "wikilink")[澳洲和](../Page/澳洲.md "wikilink")[加拿大军队在朝鲜](../Page/加拿大.md "wikilink")[加平河附近交战](../Page/加平河.md "wikilink")，[加平战斗爆发](../Page/加平战斗.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[人民英雄纪念碑在](../Page/人民英雄纪念碑.md "wikilink")[北京](../Page/北京.md "wikilink")[天安门广场建成](../Page/天安门广场.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[美国大规模](../Page/美国.md "wikilink")[环保运动](../Page/环保.md "wikilink")，促成该日成为[世界地球日Earth](../Page/世界地球日.md "wikilink")
    Day。
  - [1983年](../Page/1983年.md "wikilink")：[中华民国](../Page/中华民国.md "wikilink")[国军英雄](../Page/国军英雄.md "wikilink")[飞行员](../Page/飞行员.md "wikilink")[李大维驾驶](../Page/李大维_\(军人\).md "wikilink")[U-6A侦察机逃离](../Page/U-6A.md "wikilink")[台湾](../Page/台湾.md "wikilink")，在[福建](../Page/福建.md "wikilink")[宁德迫降](../Page/宁德.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[中共中央在](../Page/中共中央.md "wikilink")[北京](../Page/北京.md "wikilink")[人民大會堂為一周前逝世的](../Page/人民大會堂.md "wikilink")[中共中央前總書記](../Page/中國共產黨中央委員會總書記.md "wikilink")[胡耀邦舉行追悼會](../Page/胡耀邦.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[墨西哥發生](../Page/墨西哥.md "wikilink")[一系列可燃氣體爆炸](../Page/1992年瓜達拉哈拉大爆炸.md "wikilink")，造成206人死亡，500人受傷，15,000人無家可歸，幾十座建築物被夷為平地。
  - [1993年](../Page/1993年.md "wikilink")：[中國](../Page/中國.md "wikilink")[國家航天局](../Page/國家航天局.md "wikilink")（China
    National Space
    Administration，[CNSA](../Page/CNSA.md "wikilink")）正式成立。
  - [1995年](../Page/1995年.md "wikilink")：[日本强制搜查](../Page/日本.md "wikilink")[奥姆真理教总部](../Page/奥姆真理教.md "wikilink")。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[朝鲜](../Page/朝鲜.md "wikilink")[平安北道](../Page/平安北道.md "wikilink")[龙川郡发生](../Page/龙川郡.md "wikilink")[火车爆炸事故](../Page/2004年朝鮮龍川火車站爆炸事件.md "wikilink")，导致近200人死亡，1500多人受伤，另有8000多幢房屋被毁。
  - [2014年](../Page/2014年.md "wikilink")：[英超球隊](../Page/英超.md "wikilink")[曼聯宣佈解僱上任僅](../Page/曼聯.md "wikilink")11個月的領隊[大衛·莫耶斯](../Page/大衛·莫耶斯.md "wikilink")。

## 出生

  - [1513年](../Page/1513年.md "wikilink")：[立花道雪](../Page/立花道雪.md "wikilink")，[日本](../Page/日本.md "wikilink")[戰國武將](../Page/战国_\(日本\).md "wikilink")、大友氏軍師（[1585年去世](../Page/1585年.md "wikilink")）
  - [1707年](../Page/1707年.md "wikilink")：[亨利·菲爾丁](../Page/亨利·菲爾丁.md "wikilink")，[英國](../Page/英國.md "wikilink")[小說家](../Page/小說家.md "wikilink")、[劇作家](../Page/劇作家.md "wikilink")（[1754年去世](../Page/1754年.md "wikilink")）
  - [1724年](../Page/1724年.md "wikilink")：[伊曼努尔·康德](../Page/伊曼努尔·康德.md "wikilink")，[德国](../Page/德国.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")（[1804年去世](../Page/1804年.md "wikilink")）
  - [1870年](../Page/1870年.md "wikilink")：[列宁](../Page/列宁.md "wikilink")，[俄國革命家](../Page/俄國.md "wikilink")（[1924年去世](../Page/1924年.md "wikilink")）
  - [1904年](../Page/1904年.md "wikilink")：[罗伯特·奥本海默](../Page/罗伯特·奥本海默.md "wikilink")，[美国](../Page/美国.md "wikilink")[理论物理学家](../Page/理论物理学.md "wikilink")（[1967年去世](../Page/1967年.md "wikilink")）
  - [1909年](../Page/1909年.md "wikilink")：[麗塔·列維-蒙塔爾奇尼](../Page/麗塔·列維-蒙塔爾奇尼.md "wikilink")，[義大利神經生物學家](../Page/義大利.md "wikilink")，[1986年與](../Page/1986年.md "wikilink")[史丹利·科恩共同獲得](../Page/史丹利·科恩.md "wikilink")[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")。（[2012年逝世](../Page/2012年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[耶胡迪·梅纽因](../Page/耶胡迪·梅纽因.md "wikilink")，[美国](../Page/美国.md "wikilink")[音乐家](../Page/音乐家.md "wikilink")（[1999年去世](../Page/1999年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[白杨](../Page/白楊_\(演員\).md "wikilink")，[中国电影](../Page/中国电影.md "wikilink")[演员](../Page/演员.md "wikilink")（[1996年去世](../Page/1996年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[杰克·尼科尔森](../Page/杰克·尼科尔森.md "wikilink")，美国[电影演员](../Page/电影.md "wikilink")，三獲[奧斯卡男演員獎](../Page/奥斯卡金像奖.md "wikilink")。
  - [1938年](../Page/1938年.md "wikilink")：[三宅一生](../Page/三宅一生.md "wikilink")，日本[服裝設計師](../Page/服裝設計.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[吳思遠](../Page/吳思遠.md "wikilink")，[香港電影](../Page/香港.md "wikilink")[導演](../Page/導演.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[周融](../Page/周融.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[中田讓治](../Page/中田讓治.md "wikilink")，[日本男性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")、演員
  - [1955年](../Page/1955年.md "wikilink")：[杜琪峯](../Page/杜琪峯.md "wikilink")，香港電影導演、監製
  - 1955年：[博尼·卡浦爾](../Page/博尼·卡浦爾.md "wikilink")，[印度電影監製](../Page/印度.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[魯振順](../Page/魯振順.md "wikilink")，香港演員
  - [1960年](../Page/1960年.md "wikilink")：[李紀珠](../Page/李紀珠.md "wikilink")，[臺灣學者](../Page/臺灣.md "wikilink")，[女性政治人物](../Page/女性.md "wikilink")
  - [1966年](../Page/1966年.md "wikilink")：[杰弗里·迪恩·摩根](../Page/杰弗里·迪恩·摩根.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[西本智實](../Page/西本智實.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[艾力·馬比烏斯](../Page/艾力·馬比烏斯.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[智海](../Page/智海_\(香港漫畫家\).md "wikilink")，香港漫畫家
  - [1978年](../Page/1978年.md "wikilink")：[戴佩妮](../Page/戴佩妮.md "wikilink")，[馬來西亞創作](../Page/馬來西亞.md "wikilink")[歌手](../Page/歌手.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[陽建福](../Page/陽建福.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")
  - 1979年：[路嘉欣](../Page/路嘉欣.md "wikilink")，台灣演員
  - [1981年](../Page/1981年.md "wikilink")：[朱慧敏](../Page/朱慧敏.md "wikilink")，香港演員
  - [1982年](../Page/1982年.md "wikilink")：[下屋則子](../Page/下屋則子.md "wikilink")，日本女性聲優
  - [1982年](../Page/1982年.md "wikilink")：[卡西迪·弗里曼](../Page/卡西迪·弗里曼.md "wikilink")，[美國演員](../Page/美國.md "wikilink")
  - 1982年：[卡卡](../Page/卡卡.md "wikilink")，[巴西](../Page/巴西.md "wikilink")[足球選手](../Page/足球.md "wikilink")
  - 1982年：[米歇爾·雷恩](../Page/米歇爾·雷恩.md "wikilink")，[英國演員](../Page/英國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[曾愷玹](../Page/曾愷玹.md "wikilink")，台灣女演員
  - 1984年：[賈得熙](../Page/賈得熙.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[張弦子](../Page/張弦子.md "wikilink")，台灣歌手
  - 1986年：[艾梅柏·希爾德](../Page/艾梅柏·希爾德.md "wikilink")，[美國演員與模特兒](../Page/美國.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[中田翔](../Page/中田翔.md "wikilink")，[日本](../Page/日本.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[柳孝榮](../Page/柳孝榮.md "wikilink")，[韓國女子組合](../Page/韓國.md "wikilink")[5dolls成員](../Page/5dolls.md "wikilink")
  - 1993年：[柳和榮](../Page/柳和榮.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[T-ara前成員](../Page/T-ara.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[俞敏](../Page/俞敏.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[BP
    RANIA前成員](../Page/BP_RANIA.md "wikilink")

## 逝世

  - [835年](../Page/835年.md "wikilink")：[空海](../Page/空海.md "wikilink")，日本高野山[真言宗開山祖師](../Page/真言宗.md "wikilink")，[唐密第八祖](../Page/唐密.md "wikilink")（[774年出生](../Page/774年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：，[蘇丹民族英雄](../Page/苏丹共和国.md "wikilink")
  - [1933年](../Page/1933年.md "wikilink")：[亨利·萊斯爵士](../Page/亨利·萊斯.md "wikilink")，[英國](../Page/英國.md "wikilink")[工程師](../Page/工程師.md "wikilink")，[勞斯萊斯創辦人之一](../Page/勞斯萊斯.md "wikilink")（[1863年出生](../Page/1863年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[刘桂五](../Page/刘桂五.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军骑兵第](../Page/国民革命军.md "wikilink")5师师长（[1902年出生](../Page/1902年.md "wikilink")）
  - [1945年](../Page/1945年.md "wikilink")：[珂勒惠支](../Page/珂勒惠支.md "wikilink")，德國[版畫家](../Page/版畫.md "wikilink")（[1867年出生](../Page/1867年.md "wikilink")）
  - [1947年](../Page/1947年.md "wikilink")：[黃媽典](../Page/黃媽典.md "wikilink")，嘉義醫師，政治家，實業家，曾對嘉義撲殺鼠疫有所貢獻。（[1893年出生](../Page/1893年.md "wikilink")）
  - [1964年](../Page/1964年.md "wikilink")：[馬師曾](../Page/馬師曾.md "wikilink")，[粵劇演員](../Page/粵劇.md "wikilink")（[1900年出生](../Page/1900年.md "wikilink")）
  - [1983年](../Page/1983年.md "wikilink")：[林巧稚](../Page/林巧稚.md "wikilink")，[中國](../Page/中國.md "wikilink")[婦產科專家](../Page/婦產科.md "wikilink")（[1901年出生](../Page/1901年.md "wikilink")）
  - [1992年](../Page/1992年.md "wikilink")：[康克清](../Page/康克清.md "wikilink")，中國[婦女運動領導人](../Page/婦女運動.md "wikilink")，[朱德之妻](../Page/朱德.md "wikilink")（[1911年出生](../Page/1911年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：，[日本政治家](../Page/日本.md "wikilink")，[日中友協創始人](../Page/日中友好协会.md "wikilink")（[1906年出生](../Page/1906年.md "wikilink")）
  - [1994年](../Page/1994年.md "wikilink")：[尼克松](../Page/尼克松.md "wikilink")，[美國前](../Page/美國.md "wikilink")[總統](../Page/美國總統.md "wikilink")（[1913年出生](../Page/1913年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[包洛奇](../Page/包洛奇.md "wikilink")，[英國藝術家](../Page/英國.md "wikilink")（[1924年出生](../Page/1924年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[孟二冬](../Page/孟二冬.md "wikilink")，中國古代文學專家（[1957年出生](../Page/1957年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[蔣世豪](../Page/蔣世豪.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")（[1975年出生](../Page/1975年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[程乃珊](../Page/程乃珊.md "wikilink")，中國女作家（[1946年出生](../Page/1946年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[彭福](../Page/彭福_\(英國陸軍將領\).md "wikilink")，英國陸軍少將，[香港賽馬會首任總經理](../Page/香港賽馬會.md "wikilink")（[1916年出生](../Page/1916年.md "wikilink")）

## 节假日和习俗

  - [世界地球日](../Page/世界地球日.md "wikilink")─項世界性的環境保護活動，呼籲保護地球，勿失良機。

## 參考資料