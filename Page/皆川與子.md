**皆川與子**（，\[1\]），[日本长寿人物](../Page/日本.md "wikilink")，曾是世界[最年長者](../Page/最年長者.md "wikilink")（2007年1月29日—2007年8月13日）。

## 生平

皆川與子於1893年1月4日生於[日本](../Page/日本.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink")，為四姊妹的老大。早年喪夫，僅靠務農和販賣農作物養育五個孩子（四子一女），現只剩女兒在世。其家族繁衍至今共7名孫兒、12名曾孫及兩名玄孫，其孫[安永昭俊現為](../Page/安永昭俊.md "wikilink")[田川市議會事務局長](../Page/田川市.md "wikilink")。皆川平時愛好彈奏民族樂器[三味線](../Page/三味線.md "wikilink")，口頭禪是以日、英語說謝謝，據稱她的長壽秘訣是常喝[酒](../Page/酒.md "wikilink")。

2002年起入住[福岡縣](../Page/福岡縣.md "wikilink")[福智町](../Page/福智町.md "wikilink")[慶壽園](../Page/慶壽園.md "wikilink")。自2005年4月5日起成為[日本國內最年長人瑞](../Page/日本.md "wikilink")。在2007年1月28日[美國](../Page/美國.md "wikilink")[人瑞](../Page/人瑞.md "wikilink")[艾瑪·提爾曼逝世後](../Page/艾瑪·提爾曼.md "wikilink")，成為世界[最年長者](../Page/最年長者.md "wikilink")。同年8月13日下午5時47分在慶壽園安老院辭世，共活了114年221天。

## 参考資料

## 外部連結

  - [日本114歲老婆婆
    皆川米子成為全球最高齡人瑞](http://tw.news.yahoo.com/article/url/d/a/070130/19/9zfx.html)，2007年1月31日[法新社](../Page/法新社.md "wikilink")，[劉學源譯](../Page/劉學源.md "wikilink")

[M皆](../Category/福岡縣出身人物.md "wikilink")
[M皆](../Category/日本超级人瑞.md "wikilink")
[M皆](../Category/最年长者.md "wikilink")

1.  [東森新聞-日本114歲老婦辭世　全球最老人瑞今年三度換人當](http://www.nownews.com/2007/08/14/334-2141178.htm)