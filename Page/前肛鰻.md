**前肛鰻**（[学名](../Page/学名.md "wikilink")：），為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目](../Page/鰻鱺目.md "wikilink")[糯鰻亞目](../Page/糯鰻亞目.md "wikilink")[通鰓鰻科的一個](../Page/通鰓鰻科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於全球各大洋，包括西[大西洋區](../Page/大西洋.md "wikilink")：[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")、[德克薩斯州至](../Page/德克薩斯州.md "wikilink")[委內瑞拉](../Page/委內瑞拉.md "wikilink")；[印度洋區](../Page/印度洋.md "wikilink")：[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[印度西岸](../Page/印度.md "wikilink")、[太平洋區](../Page/太平洋.md "wikilink")：[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭等海域皆有其蹤跡](../Page/紐西蘭.md "wikilink")。

## 深度

水深30\~270公尺。

## 特徵

本魚無鱗，具胸鰭。上頜較下頜長。前上頜骨齒2枚。上頜骨齒細小，形成齒帶；下頜骨齒1列，為較大之犬齒；鋤骨齒5列。前上頜骨齒及鋤骨齒為複合齒，每一枚複合齒之基部有一小丘狀之組織。背鰭起點在胸鰭之前。脊椎骨數119\~130。體長為體高之15.6倍；尾長為頭與軀幹之5.1倍；吻長為眼徑之3.3倍。口裂超過眼之後緣。眼小。有尾鰭。肛門至鰓裂之距離小於頭長。體[褐色](../Page/褐色.md "wikilink")，下方較淡；背鰭、臀鰭為[白色](../Page/白色.md "wikilink")。體長可達52公分。

## 生態

屬於底棲魚類，多分布在1000\~200公尺之海底屬優勢魚種，以小型魚類、[甲殼類及軟體動物為食](../Page/甲殼類.md "wikilink")。

## 經濟利用

數量多，但無經濟價值。

## 外部連結

[前肛鰻的圖片](http://www.fishbase.org/Photos/ThumbnailsSummary.php?ID=2659)

## 參考資料

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:前肛鰻屬](../Category/前肛鰻屬.md "wikilink")