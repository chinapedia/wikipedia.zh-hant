[朱彝尊1.jpg](https://zh.wikipedia.org/wiki/File:朱彝尊1.jpg "fig:朱彝尊1.jpg")》第一集之朱彜尊\]\]
**朱彝尊**（），[字](../Page/表字.md "wikilink")**锡鬯**，[号](../Page/号.md "wikilink")**竹垞**，[浙江](../Page/浙江.md "wikilink")[嘉兴人](../Page/嘉兴.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[江蘇](../Page/江蘇.md "wikilink")[吳江](../Page/吳江.md "wikilink")，[明末](../Page/明.md "wikilink")[清初政治人物](../Page/清.md "wikilink")，[诗人](../Page/诗人.md "wikilink")、词人、[经学家](../Page/经学家.md "wikilink")。

## 生平

明崇禎二年(1629年)，八月二十一日生。先世[江蘇](../Page/江蘇.md "wikilink")[吳江人](../Page/吳江.md "wikilink")，[明](../Page/明.md "wikilink")[景泰四年](../Page/景泰.md "wikilink")（1453年）遷居[浙江](../Page/浙江.md "wikilink")[嘉興府](../Page/嘉興府.md "wikilink")[秀水縣](../Page/秀水縣.md "wikilink")，高祖[朱國祚](../Page/朱國祚.md "wikilink")，[字](../Page/表字.md "wikilink")**兆隆**，[號](../Page/號.md "wikilink")**養淳**，為[明](../Page/明.md "wikilink")[萬曆壬午科](../Page/萬曆.md "wikilink")[狀元](../Page/狀元.md "wikilink")。十七岁时，因家贫入赘归安县教谕冯镇鼎家。與妻冯福贞之妹冯寿常（字静志）渐生感情。《静志居琴趣》是寫這段苦恋之作。\[1\]彝尊讀書過目成誦，博通经史，擅长[诗词](../Page/诗词.md "wikilink")，为[浙西词派的创始者](../Page/浙西词派.md "wikilink")，又精於[金石考證之學](../Page/金石.md "wikilink")。诗与[王士禛齐名](../Page/王士禛.md "wikilink")。[顺治十三年](../Page/顺治.md "wikilink")（1656年）[海宁人](../Page/海宁.md "wikilink")[杨雍建聘朱彝尊为西席](../Page/杨雍建.md "wikilink")，教授其子[杨中讷](../Page/杨中讷.md "wikilink")。

[康熙元年](../Page/康熙.md "wikilink")（1662年）爆發[通海案](../Page/通海案.md "wikilink")，朱彝尊避禍於[浙江](../Page/浙江.md "wikilink")[永嘉](../Page/永嘉.md "wikilink")，奔走於[山西](../Page/山西.md "wikilink")、[河北](../Page/河北.md "wikilink")、[山東一帶](../Page/山東.md "wikilink")。康熙十三年（1674年）冬，朱彝尊客居潞河（今北京郊区），\[2\]館於[龚佳育幕府](../Page/龚佳育.md "wikilink")。康熙十六年，龔佳育升官江寧布政使，朱彝尊随之转客居[江宁](../Page/江宁.md "wikilink")。康熙十七年舉[博學鴻詞](../Page/博學鴻詞.md "wikilink")，授[翰林院檢討](../Page/翰林院檢討.md "wikilink")，入直[南书房](../Page/南书房.md "wikilink")，康熙二十三年（1684年）一月，因携带楷书手私入禁中抄录四方所进图书，为掌院学士[牛钮所劾](../Page/牛钮_\(康熙进士\).md "wikilink")，降级谪官。\[3\]與修《[明史](../Page/明史.md "wikilink")》。藏书八万卷。\[4\]《[清史稿](../Page/清史稿.md "wikilink")·朱彝尊傳》稱：“當時[王士禎工詩](../Page/王士禎.md "wikilink")；[汪琬工文](../Page/汪琬.md "wikilink")；[毛奇齡工考據](../Page/毛奇齡.md "wikilink")；獨彝尊兼有眾長。”
康熙四十八年，十月十三日子夜卒，年八十一。

## 著述

有《[經義考](../Page/經義考.md "wikilink")》（生前刊行一百六十七卷，後人補刻為三百卷）、《[日下舊聞](../Page/日下舊聞.md "wikilink")》四十二卷、《[明詩綜](../Page/明詩綜.md "wikilink")》一百卷、《[詞綜](../Page/詞綜.md "wikilink")》三十八卷、《[明词综](../Page/明词综.md "wikilink")》十二卷（书未成而卒，后由[王昶整理刊刻](../Page/王昶_\(清朝\).md "wikilink")）、《曝书亭集》八十卷等\[5\]。

朱彝尊出生相國之家，明季為反抗清廷暴政的一赤子。康熙之後入直史館，故國之思寓于行間。晚歲天下承平，自纂文集，將秀水家風刪削殆盡。彼時文網鉗制，有不可形容者，先生之風，山高水長，後世當有能辨之者。

## 遺跡

[Pushu_Pavilion_04_2017-10.jpg](https://zh.wikipedia.org/wiki/File:Pushu_Pavilion_04_2017-10.jpg "fig:Pushu_Pavilion_04_2017-10.jpg")
朱彝尊在京師的故居位于[宣武门外大街东侧海柏胡同](../Page/宣武门.md "wikilink")6号。居所中原有一小亭，湖石侧卧，古藤斜倚，春有花香，夏覆绿荫，名“曝书亭”，又名“古藤书屋”。后顺德会馆。

另一座朱彝尊故居（[曝书亭](../Page/曝书亭.md "wikilink")）位于[浙江省](../Page/浙江省.md "wikilink")[嘉兴市郊区](../Page/嘉兴市.md "wikilink")[王店镇广平路南端](../Page/王店镇.md "wikilink")，原名“竹垞”，为朱氏别业，曝书亭原是竹垞内的一座建筑物，因朱彝尊著作《曝书亭集》称名于世，后人遂以曝书亭作为园林名，现为浙江省文物保护单位。

## 注釋

## 參考書目

  - 杨谦《朱竹垞先生年谱》
  - 叶嘉莹：〈[朱彝尊之爱情词的美学特质](http://www.nssd.org/articles/article_read.aspx?id=1003299806)〉。
  - 叶嘉莹：〈[朱彝尊之爱情词的美学特质（续）](http://www.nssd.org/articles/article_read.aspx?id=1003299790)〉。
  - 叶嘉莹：〈[朱彝尊之爱情词的美学特质（续完）](http://www.nssd.org/articles/article_read.aspx?id=1003299771)〉。

## 外部链接

  - [诗词总汇](http://www.sczh.com/sczh/showpoet.asp?作者编号=737)

{{-}}

[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝作家](../Category/清朝作家.md "wikilink")
[Category:南書房行走](../Category/南書房行走.md "wikilink")
[Category:中国诗人](../Category/中国诗人.md "wikilink")
[Category:嘉兴人](../Category/嘉兴人.md "wikilink")
[Category:浙江詩人](../Category/浙江詩人.md "wikilink")
[Y](../Category/朱姓.md "wikilink")

1.  [冒广生](../Page/冒广生.md "wikilink")《小三吾亭词话》云：“世传竹诧《风怀二百韵》为其妻妹作，其实《静志居琴趣》一卷，皆《风怀》注脚也。”
2.  朱彝尊《鸳鸯湖棹歌》自序言：“甲寅岁暮，旅食潞河，言归未遂，爰忆土风，成绝句百首。语无诠次，以其多言舟楫之事，题曰‘鸳鸯湖棹歌’，聊比竹枝、浪淘沙之调。冀同里诸君子见而和之云尔。”
3.  《国朝耆献类征初编》卷一一八：“先生直史馆日，私以楷书手王纶自随，录四方经进书。牛钮劾其漏泄，吏议镌一级，时人谓之‘美贬’。”朱彝尊《严君（绳孙）墓志铭》：“二十二年春，予又入直南书房，赐居黄瓦门左。用是以资格自高者，合外内交构；逾年，予遂挂名学士牛钮弹事。”
4.  《曝书亭著录序》中说：“拥书八万卷，足以豪矣！”[查慎行](../Page/查慎行.md "wikilink")《敬业堂诗集》卷二十三《闻李辰山藏书多归竹垞》诗云：“万卷又增三箧富，千金直化两蚨飞。平生谬托知己在，恨不从渠借一瓻。”
5.  《鹤徵前录》：“朱彝尊字锡鬯，号竹垞，晚号小长芦钓鱼师，又号金风亭长，浙江秀水人。明太传国祚曾孙
    著有《经义考》、《日下旧闻》、《曝书亭集》。”注云：“先生又有《瀛洲道古录》、《五代史注》、《禾录》诸书，俱属稿未成。”