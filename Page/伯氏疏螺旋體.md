**伯氏疏螺旋体**（*Borrelia
burgdorferi*），也译作**博氏疏螺旋体**、**布氏疏螺旋体**，**巴格朵夫疏螺旋菌**，**莱姆病螺旋体**，是一种[螺旋體](../Page/螺旋體門.md "wikilink")。伯氏疏螺旋体是[莱姆病的病原体](../Page/莱姆病.md "wikilink")，由[蜱传播给人类](../Page/蜱.md "wikilink")。伯氏疏螺旋体的感染对人体多个系统有影响，经过不同的临床阶段。受影响的器官会发生长期的机能障碍。

[Borrelia_cycle.jpg](https://zh.wikipedia.org/wiki/File:Borrelia_cycle.jpg "fig:Borrelia_cycle.jpg")
1975年10月，在[康乃狄克州的](../Page/康乃狄克州.md "wikilink")[老莱姆镇](../Page/老莱姆镇.md "wikilink")，[莱姆镇和](../Page/莱姆镇.md "wikilink")[东哈丹姆附近地区爆发风湿性关节炎](../Page/东哈丹姆.md "wikilink")，19名儿童和12名成人被诊断为风湿性关节炎，1977年，这种病被命名为莱姆病。1982年，[美国国家卫生总局的](../Page/美国国家卫生总局.md "wikilink")[威利·伯格多费](../Page/威利·伯格多费.md "wikilink")（Willy
Burgdorfer）和同事从[丹敏硬蜱](../Page/丹敏硬蜱.md "wikilink")（*Ixodes
dammini*）分离到莱姆病病原体。1984年，[明尼苏达大学的Johnson以发现者的名字将莱姆病病原体命名为](../Page/明尼苏达大学.md "wikilink")*Borrelia
burgdorferi*。2014年，病原體隨氣候變化而北上影響加拿大\[1\]。

[阿诺·卡鲁恩写了一本书](../Page/阿诺·卡鲁恩.md "wikilink")《Biography of a
Germ》（《细菌的传记》或《病菌现形》）描述了伯氏疏螺旋体的来龙去脉。

## 参考文献

  -
[Category:螺旋體門](../Category/螺旋體門.md "wikilink")

1.