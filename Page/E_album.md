**E
Album**是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")5張[專輯](../Page/音樂專輯.md "wikilink")。於2001年7月25日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

本專輯與上張專輯只相隔短短七個月就推出。過往『[C album](../Page/C_album.md "wikilink")』與上張專輯『[D
album](../Page/D_album.md "wikilink")』相隔了一年以上的時間才推出，但今年短短前半年時間就已經推出了兩張單曲（『[在我背上的翅膀](../Page/在我背上的翅膀.md "wikilink")』及『[情熱](../Page/情熱.md "wikilink")』），撇除[單曲精選專輯外](../Page/KinKi_Single_Selection.md "wikilink")，就成為了現時為止與上張同類型作品相距時間最短的一次發行。

在這張專輯之後，與下一張專輯『[F
album](../Page/F_album.md "wikilink")』的發行時間又再次相隔一年以上，繼續這個原創專輯必定相隔一年才推出的不明文規定。而由於從翌年2002年開始成員二人的個人活動（包括[音樂以外的活動](../Page/音樂.md "wikilink")）又開始活躍起來，使以KinKi
Kids名義推出的單曲相隔時間隨之增長。另外，這張專輯的另外一個特色，就是從兩人的獨唱歌曲中，清楚地展現了兩人在各自音樂上的風格特徵。

本專輯是KinKi Kids的眾多作品之中，第一次沒有使用兩位成員的照片作封面的作品。雖然『[G Album
-24/7-](../Page/G_album.md "wikilink")』的通常版封本亦同樣沒有使用兩位成員照片作封面，但若果初回版及通常版封面也一併計算在內的話，本專輯是第一次同時亦成為唯一一次。

而且，自出道以來的專輯都能夠在[Oricon的](../Page/Oricon.md "wikilink")「週間專輯（包含單曲）銷量榜」上新上榜即奪第一位的紀錄，自本專輯起中斷。紀錄要待[G
Album
-24/7-推出才得以持續](../Page/G_album.md "wikilink")。而單曲方面的紀錄則不受影響，繼續持續。順提一帶，其實在本專輯推出之前，[卡拉OK專輯](../Page/卡拉OK.md "wikilink")『[KinKi
KaraoKe Single
Selection](../Page/KinKi_KaraoKe_Single_Selection.md "wikilink")』也沒有奪取第一位。不過由於嚴格一點來說該專輯不屬於KinKi
Kids名義，所以沒被計算在內。

本專輯收錄了『[在我背上的翅膀](../Page/在我背上的翅膀.md "wikilink")』和『[情熱](../Page/情熱.md "wikilink")』兩張大熱單曲，不過本專輯收錄的「在我背上的翅膀」為專輯混音版本；而「情熱」則除了單曲原因版本外，還額外多收錄了一個抒情吉他版本。其實像「在我背上的翅膀」一樣作些微混音改動的專輯版本一直也有存在（如『[C
album](../Page/C_album.md "wikilink")』裡的「[不要停止的純真](../Page/不要停止的PURE.md "wikilink")」一曲），不過像這次重新混音及重新收錄過的專輯混音版則屬首次。

至於在[Oricon銷量榜上的首批銷量比起上張專輯有所回升](../Page/Oricon.md "wikilink")，但就算是累積銷量都只是少量回升。而作為KinKi的原創專輯來說，本專輯更是最後一張銷量突破50萬張的作品。

## 收錄歌曲

1.  **LOVESICK**
      - 作曲：[Face 2 fAKE](../Page/Face_2_fAKE.md "wikilink")
      - 作詞：[戶澤暢美](../Page/戶澤暢美.md "wikilink")
      - 編曲：Face 2 fAKE
2.  **在我背上的翅膀（E Edit）**（****）
      - ※堂本剛參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『向井荒太的動物日記～愛犬羅斯蘭蒂的災難～』主題曲。第11張單曲。
      - 作曲：[織田哲郎](../Page/織田哲郎.md "wikilink")
      - 作詞：[松本　隆](../Page/松本隆.md "wikilink")
      - 編曲：[家原正樹](../Page/家原正樹.md "wikilink")
3.  **No Control**
      - 作曲：[長岡成貢](../Page/長岡成貢.md "wikilink")
      - 作詞：[牧穗繪美](../Page/牧穗繪美.md "wikilink")
      - 編曲：長岡成貢
4.  **百年之戀**（****）
      - ※堂本剛自己作曲作詞的獨唱作品。
      - 作曲：[堂本　剛](../Page/堂本剛.md "wikilink")
      - 作詞：堂本　剛
      - 編曲：[知野芳彥](../Page/知野芳彥.md "wikilink")
5.  **Father**
      - 作曲：[奧居　香](../Page/奧居香.md "wikilink")
      - 作詞：[篠崎隆一](../Page/篠崎隆一.md "wikilink")
      - 編曲：[林部直樹](../Page/林部直樹.md "wikilink")
      - 和音編排：[松下　誠](../Page/松下誠.md "wikilink")
6.  **揮手再見**（****）
      - 作曲：[Ooyagi Hiroo](../Page/Ooyagi_Hiroo.md "wikilink")
      - 作詞：Ooyagi Hiroo
      - 編曲：Ooyagi Hiroo
      - 弦樂編排：[村山達哉](../Page/村山達哉.md "wikilink")
7.  **Broken冰箱**（****）
      - 作曲：[宮崎　步](../Page/宮崎步.md "wikilink")
      - 作詞：戶澤暢美
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
      - 和音編排：[岩田雅之](../Page/岩田雅之.md "wikilink")
8.  **情熱**
      - ※堂本光一參與演出的[富士電視台](../Page/富士電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『Rookie\!』主題曲。第12張單曲。
      - 作曲：[Boris Dudevic](../Page/Boris_Dudevic.md "wikilink")
      - 作詞：[Satomi](../Page/Satomi.md "wikilink")
      - 編曲：[松本良喜](../Page/松本良喜.md "wikilink")
9.  **Love U4 Good**
      - 作曲：[松本良喜](../Page/松本良喜.md "wikilink")
      - 作詞：[double S](../Page/double_S.md "wikilink")
      - 編曲：[安部　潤](../Page/安部潤.md "wikilink")
      - 和音編排：知野芳彥
10. **-so young blues-**
      - ※堂本光一自己作曲的獨唱作品。
      - 作曲：[堂本光一](../Page/堂本光一.md "wikilink")
      - 作詞：[松岡　充](../Page/松岡充.md "wikilink")
      - 編曲：[鈴木雅也](../Page/鈴木雅也.md "wikilink")
      - 和音編排：知野芳彥
11. **HONEY RIDER**
      - 作曲：[原　一博](../Page/原一博.md "wikilink")
      - 作詞：[相田　毅](../Page/相田毅.md "wikilink")
      - 編曲：原　一博
12. **月光**
      - 作曲：[飯田建彥](../Page/飯田建彥.md "wikilink")
      - 作詞：[淺田信一](../Page/淺田信一.md "wikilink")
      - 編曲：[ha-j](../Page/ha-j.md "wikilink")
13. **情熱（抒情吉他版）**（****）
      - 作曲：[Boris Dudevic](../Page/Boris_Dudevic.md "wikilink")
      - 作詞：[Satomi](../Page/Satomi.md "wikilink")
      - 編曲：[吉田　建](../Page/吉田建.md "wikilink")

[Category:近畿小子專輯](../Category/近畿小子專輯.md "wikilink")
[Category:2001年音樂專輯](../Category/2001年音樂專輯.md "wikilink")
[Category:2001年Oricon專輯週榜冠軍作品](../Category/2001年Oricon專輯週榜冠軍作品.md "wikilink")