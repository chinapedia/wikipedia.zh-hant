**王家禧**（，），[筆名](../Page/筆名.md "wikilink")**王澤**，[漫畫家](../Page/漫畫家.md "wikilink")，生於[中国](../Page/中国.md "wikilink")[天津](../Page/天津.md "wikilink")，其父親是[北洋軍閥兼](../Page/北洋軍閥.md "wikilink")[東三省省長](../Page/東三省.md "wikilink")[王承斌](../Page/王承斌.md "wikilink")。1944年从[北京辅仁大学](../Page/北京辅仁大学.md "wikilink")[西方艺术系毕业](../Page/西方艺术.md "wikilink")，曾在天津任美術工作。1956年移居[香港](../Page/香港.md "wikilink")，他畫的《老夫子》聞名中外，風靡整個[華人社区](../Page/華人社区.md "wikilink")。

王家禧的六个儿子都拥有他的美术细胞。其笔名王泽亦是取于[其长子之名](../Page/王泽_\(学者\).md "wikilink")。王家禧在1980年代带着家人移民到[美国](../Page/美国.md "wikilink")[加州](../Page/加州.md "wikilink")，之後《老夫子》的发展是由其中两个儿子在[台湾负责](../Page/台湾.md "wikilink")。移民後王家禧平日最大嗜好除了畫漫畫就是[釣魚](../Page/釣魚.md "wikilink")，還迷上了做[陶器](../Page/陶器.md "wikilink")。\[1\]

王家禧於美國時間2017年1月1日凌晨5時57分因年老器官衰竭逝世，享耆壽93歲\[2\]\[3\]。

## 剽竊爭論

很早之前，早有不少[中國大陸畫家和讀者揭發](../Page/中國大陸.md "wikilink")《老夫子》的原創其實是已故漫畫家[朋弟](../Page/朋弟.md "wikilink")，質疑王澤有剽竊之嫌，但一直也沒有受到廣泛的關注。直至2001年，一位文化名人[馮驥才出版了一本書](../Page/馮驥才.md "wikilink")《文化發掘老夫子出土：為朋弟抱打不平》，指證王澤的《老夫子》是抄襲自朋弟《老夫子》，自此受到社會廣泛關注\[4\]\[5\]\[6\]。

## 參考資料

## 外部連結

  - [老夫子英文官方網站](http://www.oldmasterq.com/)
  - [老夫子中文官方網站](http://www.omqcomics.com/)
  - [老夫子 相片集](http://www.masterqa.idv.tw/masterq/qphotos.htm)
  - [誰是《老夫子》的原創者？（爭鳴錄）](http://www.people.com.cn/BIG5/paper39/3281/425707.html)

[Category:香港漫畫家](../Category/香港漫畫家.md "wikilink")
[Category:王姓](../Category/王姓.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:老夫子](../Category/老夫子.md "wikilink")
[Category:著作權侵害](../Category/著作權侵害.md "wikilink")
[Category:死於器官衰竭的人](../Category/死於器官衰竭的人.md "wikilink")

1.
2.  [老夫子漫畫作家王家禧辭世
    享壽93歲](http://www.cna.com.tw/news/firstnews/201701035014-1.aspx)，中央社，2017年1月3日
3.
4.
5.
6.