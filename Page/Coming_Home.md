《**Coming
Home**》（意思：回家）是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王靖雯的第四張大碟](../Page/王靖雯.md "wikilink")，[粵語專輯](../Page/粵語.md "wikilink")（1首[英語](../Page/英語.md "wikilink")），於1992年8月13日发行。這是王靖雯從[美國回來後的第一張專輯](../Page/美國.md "wikilink")，一共10首歌曲，6首為翻唱。其中翻唱日本歌-{后}-（中島美雪）70年代末的作品「容易受傷的女人」（原曲「ルージュ」）橫掃全港，令她一舉躍入香港一線女歌手的行列。專輯總體仍延續了上一張大碟《[You're
the Only
One](../Page/You're_the_Only_One.md "wikilink")》的[R\&B風格](../Page/R&B.md "wikilink")。她在專輯中第一次演唱了原創全英文歌曲「Kisses
in the Wind」。

## 唱片版本

  - [香港首版](../Page/香港.md "wikilink")：[韓國壓盤](../Page/韓國.md "wikilink")。
  - [台灣版](../Page/台灣.md "wikilink")：後期引進。
  - [日本版](../Page/日本.md "wikilink")：後期引進。
  - 大陸版：僅引進發行卡帶版本，改名為《容易受傷的女人》，封面改動，歌曲順序改動。

其它版本：

  - [卡帶](../Page/卡帶.md "wikilink") [錄音帶版](../Page/錄音帶.md "wikilink")
  - 《精彩全記錄》版（首版、再版）：《精彩全記錄》是將王菲前8張大碟捆綁發售。
  - 《從頭認識》版：《從頭認識》是將王菲在新藝寶一共9張粵語大碟捆綁發售。
  - 24K Gold 金碟版：1994年，金色盤片和金色托盤。
  - 寶麗金20世紀光輝印記版（[dCS處理](../Page/dCS.md "wikilink")）：1999年4月。
  - [DSD系列](../Page/DSD.md "wikilink")：2003年5月7日。
  - 環球24K Gold金碟週年紀念經典系列：2012年11月14日
  - [黑膠唱片](../Page/黑膠唱片.md "wikilink")：2013年8月9日

## 曲目

## 獎項

香港

  - [香港電台的](../Page/香港電台.md "wikilink")[十大中文金曲頒獎音樂會](../Page/十大中文金曲頒獎音樂會.md "wikilink")：
      - 1992年度：「十大中文金曲」—「容易受傷的女人」\[1\]
  - [無綫電視的](../Page/電視廣播有限公司.md "wikilink")[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink")：
      - 1992年度：「十大勁歌金曲」—「容易受傷的女人」\[2\]
  - [商業電台的](../Page/商業電台.md "wikilink")[叱咤樂壇流行榜](../Page/叱咤樂壇流行榜.md "wikilink")：
      - 1992年度：「全國專業推介銅獎」—「容易受傷的女人」\[3\]
      - 1992年度：「至尊歌曲大獎」—「容易受傷的女人」\[4\]
  - [新城電台的](../Page/新城電台.md "wikilink")[新城勁爆家族音樂大賞](../Page/新城勁爆家族音樂大賞.md "wikilink")
      - 1993年度：「勁爆大躍進女歌手大賞」

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:1992年音樂專輯](../Category/1992年音樂專輯.md "wikilink")

1.  [香港電台官方網站](http://www.rthk.org.hk/classicschannel/goldsong15.htm)
2.
3.  [商業電台官方網站](http://www1.881903.com/framework/pccs.gateway?url=jsp/lsc/index.jsp&menuID=36&pageURL=/fwcontent/lsc/html/lsc_20021206award1992.htm)

4.  [商業電台官方網站](http://www1.881903.com/framework/pccs.gateway?url=jsp/lsc/index.jsp&menuID=36&pageURL=/fwcontent/lsc/html/lsc_20021206award1992.htm)