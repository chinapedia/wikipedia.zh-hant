**标志**（）或**-{zh-cn:标识; zh-tw:標誌;
zh-hk:標識;}-**是[企業](../Page/企業.md "wikilink")、[組織](../Page/组织_\(社会学\).md "wikilink")、[個人等用作識別的一種](../Page/個人.md "wikilink")[圖像](../Page/圖像.md "wikilink")、[符號或](../Page/符號.md "wikilink")，在台灣已經接受以聲音作為商標之申請案，第一件准予註冊的聲音商標為綠油精。

## 語源

出自→→[法語](../Page/法語.md "wikilink")、。

「logotype」在印刷術語是「單語（logo-）活字（type）」意思，例如「on、if、an、the……」等簡短[語詞鑄為一個](../Page/語詞.md "wikilink")[活字](../Page/活字.md "wikilink")，類似「[連字](../Page/連字.md "wikilink")」（ligature，如æ）、「語標」（logogram，如$）的概念，因此又譯作「合成字體」並衍生出「文字標識」的意涵，\[1\]商業化即「文字商標」（wordmark）。

而「logo」本是「logotype」縮寫，後用來指文字與圖像的標記，常視為[商標或](../Page/商標.md "wikilink")[品牌的同義詞](../Page/品牌.md "wikilink")。日本習慣上採「logo」原義「文字標識」細分，另創[和製英語](../Page/和製英語.md "wikilink")強調其文字及圖形組合\[2\]，惟實際使用依然簡稱。

## 设计

就商业用途来说，一个优秀的標誌必须遵循以下原则：

  - 简洁
  - 在黑色和白色底色下均能良好显示
  - 在小尺寸下能良好显示
  - 在众多情况下能良好显示（如产品[包装](../Page/包装.md "wikilink")、[广告等](../Page/广告.md "wikilink")）
  - 通常要包含公司的名称
  - 作为公司的市场[营销和](../Page/营销.md "wikilink")[品牌管理](../Page/品牌管理.md "wikilink")，能充分展示公司的沟通意图
  - 有包含公司的性質（如[FedEx的E和x中間有一個小箭頭](../Page/FedEx.md "wikilink")，箭頭象徵速度也告知這是一家快遞公司。）

## 例子

<File:Adidas> Logo.svg|[愛迪達](../Page/愛迪達.md "wikilink")
|[福斯汽車](../Page/福斯汽車.md "wikilink")
<File:Wikipedia-logo.png>|[维基百科](../Page/维基百科.md "wikilink")
<File:Google> 2015 logo.svg|[谷歌](../Page/谷歌.md "wikilink")
<File:Microsoft> logo and wordmark.svg|[微軟](../Page/微軟.md "wikilink")
<File:Facebook> New
Logo(2015).svg[Facebook](../Page/Facebook.md "wikilink")

## 參見

  - [商標](../Page/商標.md "wikilink")
  - [圓標](../Page/圓標.md "wikilink")
  - [纹章](../Page/纹章.md "wikilink")
  - [徽章](../Page/徽章.md "wikilink")
  - [品牌](../Page/品牌.md "wikilink")
  - [台标](../Page/台标.md "wikilink")
  - [平面设计](../Page/平面设计.md "wikilink")
  - [市场营销](../Page/市场营销.md "wikilink")
  - [企业识別系統](../Page/企业识別系統.md "wikilink")
  - [台湾品牌](../Page/台湾品牌.md "wikilink")
  - [香港品牌](../Page/香港品牌产品.md "wikilink")
  - [交通标志](../Page/交通标志.md "wikilink")
  - [國籍標誌](../Page/國籍標誌.md "wikilink")

## 參考資料

\[3\]

[標誌](../Category/標誌.md "wikilink")
[Category:图形设计](../Category/图形设计.md "wikilink")
[Category:商業](../Category/商業.md "wikilink")

1.  黃鈴池. . 台南科大學報第29期. p075-096. 2010-10.
2.
3.  [經濟部智慧財產局-非傳統商標介紹-聲音商標](https://www.tipo.gov.tw/ct.asp?xItem=206915&ctNode=7482&mp=1)