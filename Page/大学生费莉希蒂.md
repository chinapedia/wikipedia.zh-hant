《**大学生费莉希蒂**》（）是由美国[华纳兄弟电视网](../Page/WB電視網.md "wikilink")（The WB
network）在1998－2002年间播出的青春系列电视剧，曾获得[金球奖](../Page/金球奖.md "wikilink")。

## 剧情简介

女主人公费莉希蒂·颇特（-{Felicity}-
Porter）在高中毕业典礼上鼓起勇气请自己的暗恋对象[本·卡文登](../Page/本·卡文登.md "wikilink")（Ben
Covington）在毕业纪念册上留言。本在留言中说他很遗憾以前没有机会更加深入了解费莉希蒂。费莉希蒂看了之后忽然决定放弃斯坦福大学，转而跟随本到纽约去读书。费莉希蒂在努力适应大学生活的同时，还要努力寻找自我。除了对本无法忘怀，另一个男生[诺尔·克雷因](../Page/诺尔·克雷因.md "wikilink")（Noel
Crane）也在费莉希蒂的心中占据了一个位置。

## 演员列表

### 领衔主演

| 剧中人物                                   | 饰演者                                                        | 入镜时间            |
| -------------------------------------- | ---------------------------------------------------------- | --------------- |
| 费莉希蒂·颇特(-{Felicity}- Porter)           | [凯莉·罗素](../Page/凯莉·罗素.md "wikilink")(Keri Russell)         | 84 集, 1998-2002 |
| 诺尔·克雷因(Noel Crane)                     | [史考特·佛利](../Page/史考特·佛利.md "wikilink")(Scott Foley)        | 84 集, 1998-2002 |
| 本·卡文登(Ben Covington)                   | [斯科特·斯皮德曼](../Page/斯科特·斯皮德曼.md "wikilink")(Scott Speedman) | 84 集, 1998-2002 |
| 爱莲娜·泰勒(Elena Tyler)                    | [汤姬·米勒](../Page/汤姬·米勒.md "wikilink")(Tangi Miller)         | 65 集, 1998-2002 |
| 梅根·罗坦迪(Meghan Rotundi)                 | [阿嫚妲·福尔曼](../Page/阿嫚妲·福尔曼.md "wikilink")(Amanda Foreman)   | 61 集, 1998-2002 |
| 肖恩·布隆博格(Sean Blumberg)                 | [Greg Grunberg](../Page/Greg_Grunberg.md "wikilink")       | 61 集, 1998-2002 |
| 朱莉·爱姆利克(Julie Emrick)                  | [Amy Jo Johnson](../Page/Amy_Jo_Johnson.md "wikilink")     | 50 集, 1998-2002 |
| 哈韦尔·克莱门迪·昆塔塔(Javier Clemente Quintata) | [Ian Gomez](../Page/Ian_Gomez.md "wikilink")               | 39 集, 1998-2002 |

## 参考资料

  - Julie Keller (2000-01-18). [E\!
    Online](../Page/E!_Online.md "wikilink").
  - [Goldfinger (band)](../Page/Goldfinger_\(band\).md "wikilink") asks
    about -{Felicity}- in the song "Spokesman" from the album [Open Your
    Eyes (Goldfinger
    album)](../Page/Open_Your_Eyes_\(Goldfinger_album\).md "wikilink")

## 外部链接

  - [TV.com:
    *-{Felicity}-*](http://www.tv.com/felicity/show/253/summary.html) –
    includes full episode guide

  -
  - [NoelCrane.com](http://www.noelcrane.com/) – site that was
    referenced several times in the show; it was updated during the
    show's run as if Noel Crane were a real person

[Category:1990年代美國電視劇](../Category/1990年代美國電視劇.md "wikilink")
[Category:1998年開播的美國電視影集](../Category/1998年開播的美國電視影集.md "wikilink")
[Category:2002年停播的美國電視影集](../Category/2002年停播的美國電視影集.md "wikilink")
[Category:英語電視劇](../Category/英語電視劇.md "wikilink")
[Category:美國劇情電視劇](../Category/美國劇情電視劇.md "wikilink")
[Category:紐約市背景電視節目](../Category/紐約市背景電視節目.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:WB電視節目](../Category/WB電視節目.md "wikilink")
[Category:大學背景電視劇](../Category/大學背景電視劇.md "wikilink")
[Category:青春剧](../Category/青春剧.md "wikilink")