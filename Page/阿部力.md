**阿部力**（本名李-{冬冬}-）是一位[中](../Page/中國.md "wikilink")、[日](../Page/日本.md "wikilink")[混血的演員](../Page/混血兒.md "wikilink")，父亲中国人，-{母}-亲中日混血，[阿部是外祖母的姓氏](../Page/阿部.md "wikilink")。他在[中國](../Page/中國.md "wikilink")[黑龍江省出生](../Page/黑龍江省.md "wikilink")，藝名「**李-{振冬}-**」。9歲時移居日本，改用現在的名字，之後歸化日本籍。

他在18歲時從日本回到中國，並在[北京電影學院修讀一年制課程](../Page/北京電影學院.md "wikilink")。2000年，他獲[陳果選中為電影](../Page/陳果.md "wikilink")《[人民公廁](../Page/人民公廁.md "wikilink")》的主角之一，開始了他在電影圈的生涯。之後，他在另一套[香港電影](../Page/香港電影.md "wikilink")《[頭文字D](../Page/頭文字D.md "wikilink")》裡飾演健二。除了電影之外，他亦有在電視劇演出，並曾接拍多個電視廣告及[MV的演出](../Page/MV.md "wikilink")。

2009年4月27日，他在日本招開記者會宣佈他和中國女演員[史可正式結婚](../Page/史可_\(1979年\).md "wikilink")。

## 感情生活

2006年他拍完電影《不完全戀人》，與該電影的女演員[史可在](../Page/史可_\(1979年\).md "wikilink")2006年[夏天兩人開始交往](../Page/夏天.md "wikilink")。
2009年4月27日，他在日本召開記者會宣佈他和中國女演員[史可正式結婚](../Page/史可_\(1979年\).md "wikilink")。\[1\]\[2\]\[3\]
2017年8月10日，遭媒體披露三年前與[上原多香子有婚外不正當兩性關係](../Page/上原多香子.md "wikilink")，並間接導致上原多香子的歌手丈夫TENN自殺身亡。TEEN的弟弟公布他自殺前的手機內容，顯示因精子問題不能生育後上原多香子就與[阿部力搞外遇關係](../Page/阿部力.md "wikilink")，疑似是TENN自殺原因。\[4\]\[5\]

## 作品列表

### 電影

  - 2000年：中國 《[人民公廁](../Page/人民公廁.md "wikilink")》 飾 李冬冬
  - 2005年：香港 《[頭文字D](../Page/頭文字D_\(電影\).md "wikilink")》 飾 健二
  - 2005年：日本 《》
  - 2006年：臺灣《[國士無雙](../Page/國士無雙.md "wikilink")》飾 刑事王子
  - 2006年：日本 《[-{zh:戀愛初稿 (ROUGH);zh-hans:恋爱初稿
    (ROUGH);zh-tw:我愛芳鄰;}-](../Page/我愛芳鄰_\(電影\).md "wikilink")》飾
    仲西弘樹
  - 2006年：日本 《》飾 本田耕治
  - 2006年：日本 《[親愛的貝斯：幸福的肉球](../Page/親愛的貝斯.md "wikilink")》飾 成田凌
  - 2007年：中國大陸 《[不完全戀人](../Page/不完全戀人.md "wikilink")》飾演 夏語
  - 2008年：日本 《[流星花園F](../Page/流星花園F.md "wikilink")》飾 美作明
  - 2010年：

:\*5月、《》飾 新實

:\*7月、《[-{zh:終有一天;zh-hans:终有一天;zh-tw:五個暴走少年;}-](../Page/終有一天.md "wikilink")》飾
若杉

:\*10月、《》飾 大迫雄大

  - 2011年：

:\*4月、《》飾 廣瀬譲治

:\*10月、《》飾 張福儀

:\*12月、《》飾

  - 2012年：臺灣《[騷人](../Page/騷人.md "wikilink")》飾 郝哥

<!-- end list -->

  - 2016年3月:暗殺教室\~畢業篇\~レッドアイ 飾

### 電視劇

  - 2004年：
      - 《[米迦勒之舞](../Page/米迦勒之舞.md "wikilink")》([台視](../Page/台視.md "wikilink"))
      - 《[記憶的證明](../Page/記憶的證明.md "wikilink")》([CCTV](../Page/中國中央電視台.md "wikilink"))

<!-- end list -->

  - 2005年：
      - 《[流星花園](../Page/流星花園#日本電視劇（流星花園）.md "wikilink")》([TBS](../Page/東京廣播公司.md "wikilink"))

<!-- end list -->

  - 2006年：
      - 《[役者魂\!](../Page/役者魂!.md "wikilink")》第1・7・9・10・11集
        ([富士電視台](../Page/富士電視台.md "wikilink"))

<!-- end list -->

  - 2007年：
      - 《[流星花園2](../Page/流星花園#日本電視劇（流星花園2）.md "wikilink")》([TBS](../Page/TBS.md "wikilink"))
      - 《[還以為要死了](../Page/還以為要死了.md "wikilink")》case06 (侵入)
        ([NTV](../Page/NTV.md "wikilink"))
      - 《[菊次郎與早紀](../Page/菊次郎與早紀.md "wikilink")3》([朝日電視台](../Page/朝日電視台.md "wikilink"))
      - 《[抹布女孩](../Page/抹布女孩.md "wikilink")》第10集
        ([朝日電視台](../Page/朝日電視台.md "wikilink"))

<!-- end list -->

  - 2008年：
      - 《[奇蹟之聲](../Page/奇蹟之聲.md "wikilink")》SP
        ([TBS](../Page/TBS.md "wikilink"))
      - 《[未來講師](../Page/未來講師.md "wikilink")》第2集
        ([朝日電視台](../Page/朝日電視台.md "wikilink"))
      - 《老師真偉大！》SP ([NTV](../Page/NTV.md "wikilink"))
      - 《[絕對彼氏](../Page/絕對彼氏.md "wikilink")》第8集
        ([富士電視台](../Page/富士電視台.md "wikilink"))
      - 《[王牌男公關](../Page/王牌男公關.md "wikilink")》第2・3・4集
        ([朝日電視台](../Page/朝日電視台.md "wikilink"))
      - 《[夢象成真](../Page/夢象成真.md "wikilink")》SP
        ([讀賣電視台](../Page/讀賣電視台.md "wikilink"))
      - 《[戀空](../Page/戀空.md "wikilink")》電視劇
        ([TBS](../Page/TBS.md "wikilink"))
      - 《[这里发现爱](../Page/这里发现爱.md "wikilink")》第5集
        ([華視](../Page/華視.md "wikilink"))
      - 《[麒麟館戀史](../Page/麒麟館戀史.md "wikilink")》(拍攝中)因為最後因劇本出現問題中斷拍攝，至今尚未重新開拍。

<!-- end list -->

  - 2009年
      - 《[Goro's Bar](../Page/Goro's_Bar.md "wikilink")》SP
        ([TBS](../Page/TBS.md "wikilink"))
      - 《[乌龙派出所](../Page/乌龙派出所.md "wikilink")》 健二
      - 松本清张诞辰100周年纪念特别剧\~黑色奔流SP 二宫正树

<!-- end list -->

  - 2010年
      - 《[月之恋人](../Page/月之恋人.md "wikilink")》飾明([富士電視台](../Page/富士電視台.md "wikilink"))

<!-- end list -->

  - 2011年
      - 《[幸福三顆星](../Page/幸福三顆星.md "wikilink")》([安徽衛視](../Page/安徽衛視.md "wikilink"))
        飾第一店長

<!-- end list -->

  - 2016年
      - 《[東京女子圖鑑](../Page/東京女子圖鑑.md "wikilink")》 飾 直樹

<!-- end list -->

  - 2017年
      - 《[四重奏
        —カルテット](../Page/四重奏_—カルテット.md "wikilink")》([TBS](../Page/TBS.md "wikilink"))
        友情客串 飾西村 06集

<!-- end list -->

  - 2017年
      - 《[貴族偵探](../Page/貴族偵探.md "wikilink")》-《[貴族探偵](../Page/貴族探偵.md "wikilink")》飾有戶秀司
        04集

### 音樂錄影帶

**2003年**

  - [容祖兒](../Page/容祖兒.md "wikilink")(香港歌手)「獨照」
  - [許茹芸](../Page/許茹芸.md "wikilink")(台灣歌手)「有你的天堂」

**2004年**

  - [S.H.E](../Page/S.H.E.md "wikilink")(台灣歌手)「NEVERMIND」

**2006年**

  - [A-Lin](../Page/A-Lin.md "wikilink")(台灣歌手)「位置」
  - [張信哲](../Page/張信哲.md "wikilink")(台灣歌手)「做你的男人」

## 腳注

## 外部連結

  -
  - [HORIPRO SQUARE 男性タレント：阿部　力](http://www.horipro.co.jp/talent/PM036/)

[Category:日本電影演員](../Category/日本電影演員.md "wikilink")
[Category:日本電視演員](../Category/日本電視演員.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")
[Category:Horipro](../Category/Horipro.md "wikilink")
[Category:北京电影学院校友](../Category/北京电影学院校友.md "wikilink")
[Category:归化日本公民的中华人民共和国人](../Category/归化日本公民的中华人民共和国人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:哈尔滨人](../Category/哈尔滨人.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.  <http://ent.icxo.com/htmlnews/2009/04/27/1355456_0.htm>
2.  <http://yule.sohu.com/20090428/n263671740.shtml>
3.  〈【專訪】婚事先斬後奏
    阿部力險毀演藝事業〉，《[時報周刊](../Page/時報周刊.md "wikilink")》1801期，2012年8月24日
4.  [多香子　芸能活動再開](http://www.sponichi.co.jp/entertainment/news/2015/06/29/kiji/K20150629010634850.html)
5.  [TEEN上LINE抓姦證據明確](http://www.chinatimes.com/realtimenews/20170810002336-260404)