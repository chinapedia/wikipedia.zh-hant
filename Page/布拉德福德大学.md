**布拉德福德大学**（**University of
Bradford**，英文缩写**UoB**，下文简称**布大**）位于[英国](../Page/英国.md "wikilink")[英格兰北部的](../Page/英格兰.md "wikilink")[西约克郡的](../Page/西约克郡.md "wikilink")[布拉德福德市](../Page/布拉德福德.md "wikilink")（Bradford）。该大学在1966年获得皇家宪章，使其成为英国的第40个大学，但它的起源可以追溯到19世纪初。有两个校区：主校区，和管理学院，位于里士满路，EMM
Lane。

学生人口包括10,045名本科生和3135名研究生。\[1\]
成人学生约占本科学生的三分之一。22％的学生是[国际学生](../Page/国际学生.md "wikilink")，来自超过110个不同的国家。在2010年通过[UCAS有](../Page/UCAS.md "wikilink")14406人申请该大学，其中3421人被录取。\[2\]

1973年布拉德福德大学建立了英国第一个大学和平学部门，是最大的一个专门致力于和平与冲突的研究中心，也是在英国唯一的一个研究中心
。该司作为一个杰出的国际关系，安全研究，解决冲突和发展与和平研究中心，有世界级的声誉。布拉德福德大学也是伦敦以外提供半日制学位课程的第一所大学。

布拉德福德大学不断地投资它的校园和设施，目前正在进行一个耗资8千4百万英镑的包括学生中心的重建计划，学生中心目前有全新的学生会，社交和研究区域以及酒吧。在2011年夏天，医学院移到主校区，并经过重大整修，提供最先进的设施。

## 历史及现状

[University_of_Bradford_Richmond_Building.jpg](https://zh.wikipedia.org/wiki/File:University_of_Bradford_Richmond_Building.jpg "fig:University_of_Bradford_Richmond_Building.jpg")
布大的前身为布拉德福德纺织、设计及建筑学院。该校于1882年更名为布拉德福德技术学院，再于1957年更名为布拉德福德理工大学。学校于1966年得到[皇家认证并正式改称布拉德福德大学](../Page/皇家认证.md "wikilink")。由于[音译的关系](../Page/音译.md "wikilink")，该校有时也被译做**布拉德福大学**，**布拉福大学**或**百拉福大学**。布大1966年并入附近的布拉德福德及阿尔代尔卫生学院，使之成为布大现在的卫生学院。1980年代学校为了给和平学习系更多的空间而关闭了物理系。被称作和平学习的这个专业很可能是布大目前所有专业中最出色的，该系和[联合国有着密切的联系](../Page/联合国.md "wikilink")。

2001年，布拉德福德的当地英国白人青年和亚裔（主要是[巴基斯坦人后裔](../Page/巴基斯坦.md "wikilink")）青年之间引发了严重的暴力冲突并造成多起流血事件，这些事件直接导致了该年布大生源的极度下降。但这种情况现在正在缓解。到2005年，布大本科入学的申请数量比2004年提高了35%。

2003年，布大计划同附近的[布拉德福德学院](../Page/布拉德福德学院.md "wikilink")（Bradford
College，以后简称布院）合并。那时，布大开始认证布院的学位，两校也共享教学资源。但是这个合并计划在该年年底被放弃。在2004年，布大增开了法律以及人力资源管理等科目和布院竞争生源。这样的举动造成了后来布院开始寻求从附近的[利兹都會大學](../Page/利兹都會大學.md "wikilink")（Leeds
Metropolitan
University）得到学位认证。布院目前是利兹城市大学的一个[联合学院](../Page/联合学院.md "wikilink")。

2005年，布大进行了大规模的校园建设。计划涵盖学生寝室，体育设施以及一个癌症治疗研究中心。

2009年9月，校方宣布该大学与利兹音乐学院合并。

## [校训](../Page/校训.md "wikilink")

在布大发行的出版物上，「Making Knowledge
Work（让知识工作）」是最常见的校训。这很有可能跟学校一直非常注重毕业生的就业有很大关系。根据[泰晤士报在](../Page/泰晤士报.md "wikilink")2005年进行的的调查，布大是2005年全英就业率第二高的大学，仅次于[剑桥大学](../Page/剑桥大学.md "wikilink")。布大的很多专业的学生在毕业后6个月就可以就业。

目前，在学校的[校徽下面出现的校训则是](../Page/校徽.md "wikilink")「Given Invention
Light（给发明予光芒）」。在近期出版的宣传物中，「Be Inspired（得到灵感）」和「Confronting
Inequality, Celebrating Diversity（对抗不平等，宣扬多样性）」这样的口号也经常出现。

## 学术

### 学院

布大一共分为8个学院，分别是：

1.  考古、地理及环境科学学院(The School of Archaeological, Geographical and
    Environmental Sciences)
2.  工程、设计及技术学院(The School of Engineering, Design and Technology)
3.  卫生学院(The School of Health Studies)
4.  信息科学学院(The School of Informatics)
5.  终身教育及发展学院(The School of Lifelong Education and Development)
6.  生命科学学院(The School of Life Sciences)
7.  管理学院(The School of Management)
8.  社会及国际关系学院(The School of Social and International Studies)。

## 学生组织及生活

最重要的学生组织可能是[布拉德福德大学联盟](../Page/布拉德福德大学联盟.md "wikilink")（University of
Bradford
Union），该学联在政治上非常活跃，这可能跟学校在政治以及[国际关系领域所取得的成就有一定关系](../Page/国际关系.md "wikilink")。该联盟也曾经因为「种族不平等的商业行为」等原因抵制过比如[雀巢和](../Page/雀巢.md "wikilink")[巴卡第](../Page/巴卡第.md "wikilink")（Bacardi）这样的公司。学生广播Ramair是英国现存的时间最久的学生广播。校内有两个酒吧和两个夜间俱乐部。另外学生们的月刊叫做Scrapie。

布拉德福德总体上来说没有其他大城市那样多好的酒吧和夜间俱乐部，虽然最近几年学校周边新开张了几家酒吧。但是你特别如果喜欢[印度和](../Page/印度.md "wikilink")[巴基斯坦风味的](../Page/巴基斯坦.md "wikilink")[咖喱的话](../Page/咖喱.md "wikilink")，布拉德福德绝对是全英国最好的去处。

布拉德福德曾经有一支[英格兰超级联赛足球队Bradford](../Page/英格兰超级联赛.md "wikilink")
City。但是经过降级和濒临倒闭的危机后，该队目前已经沦落成一支英格兰乙级联赛二流球队。布拉德福德最著名的是他们的[英式橄榄球球队Bradford](../Page/英式橄榄球.md "wikilink")
Bulls，该队曾经多次夺得过英国橄榄球联赛的冠军，为英格兰队夺得2004年[橄榄球世界杯的冠军培养了很多球员](../Page/橄榄球世界杯.md "wikilink")。

## 参考文献

## 外部链接

  - [官方网站](http://www.bradford.ac.uk/)
  - [布拉德福德大学中文信息](https://web.archive.org/web/20090903213143/http://bradford.go2uk.org/bradford-university/)
    提供布拉德福德大学留学的所有中文信息

[布拉德福德大学](../Category/布拉德福德大学.md "wikilink")
[Category:1966年創建的教育機構](../Category/1966年創建的教育機構.md "wikilink")

1.
2.  [1](http://www.telegraph.co.uk/education/universityeducation/universities-and-colleges/8473460/University-of-Bradford-guide.html)