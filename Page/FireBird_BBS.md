**火鸟BBS**（****）是目前中国大陸常见的[BBS系统之一](../Page/BBS.md "wikilink")，同时也有许多的变体（例如[水木清华BBS使用的](../Page/水木清华BBS.md "wikilink")[KBS论坛系统](../Page/KBS论坛系统.md "wikilink")）。该系统安装方便，支持平台多，编译、修改都比较容易，最新版本是2.66M简体中文版，该版本在
GNU General Public Licence下发行，可任意拷贝及修改。

## 沿革

    Power BBS
    　│
    　├─　Pirate BBS
    　│　　　│
    　│　　　└─　Eagle BBS 2.0
    　│　　　　　　　│
    　│　　　　　　　├─　Eagle BBS 3.0
    　│　　　　　　　│　　　│
    　│　　　　　　　│　　　└─　Palm BBS
    　│　　　　　　　│
    　│　　　　　　　├─　Pivot BBS
    　│　　　　　　　│　　　│
    　│　　　　　　　│　　　├─　Feeling BBS
    　│　　　　　　　│　　　└─　NCHU Pivot BBS
    　│　　　　　　　│
    　│　　　　　　　├─　NCTU CIS BBS
    　│　　　　　　　└─　Phoenix BBS
    　│　　　　　　　　　　　│
    　│　　　　　　　　　　　├─　Secret BBS
    　│　　　　　　　　　　　├─　Maple BBS
    　│　　　　　　　　　　　│　　　│
    　│　　　　　　　　　　　│　　　├─　Maple 3.01（3M）
    　│　　　　　　　　　　　│　　　├─　Maple SOB
    　│　　　　　　　　　　　│　　　│　　　│
    　│　　　　　　　　　　　│　　　│　　　├─　SOB domi
    　│　　　　　　　　　　　│　　　│　　　└─　Maple SOB Ptt
    　│　　　　　　　　　　　│　　　│
    　│　　　　　　　　　　　│　　　└─　Forest BBS
    　│　　　　　　　　　　　│
    　│　　　　　　　　　　　└─　FireBird BBS
    　│  　　　　　　　　　　　　　　　│
    　│　　　　　　　　　　　　　　　　├─　MagicBBS
    　│　　　　　　　　　　　　　　　　├─　SMTH BBS - KBS
    　│　　　　　　　　　　　　　　　　├─　YTHT BBS
    　│　　　　　　　　　　　　　　　　└─　PKUBBS
    　│
    　└─　Formosa(NSYSU) BBS

[Category:BBS](../Category/BBS.md "wikilink")
[Category:应用层协议](../Category/应用层协议.md "wikilink")