**托木尔峰**（；），“托木尔”维吾尔语意为“铁”，即“铁峰”，位于中国新疆[阿克苏地区](../Page/阿克苏地区.md "wikilink")[温宿县与吉尔吉斯斯坦边境线附近](../Page/温宿县.md "wikilink")，天山山脈第二高峰[汗腾格里峰西南](../Page/汗腾格里峰.md "wikilink")20公里。属天山山脉中天山区是[天山山脉最高峰](../Page/天山山脉.md "wikilink")，海拔7,443.8公尺。周围有6,000公尺以上的高峰10余座。托木尔峰地区的[冰川面积达](../Page/冰川.md "wikilink")2800多平方公里，是中国最大的现代冰川区。

其主峰及周围共10万公顷山地于1980年被列为自治区级自然保护区，主要保护高山冰川及下部森林，生物群落及生态环境。

## 物产资源

峰区以北为[西汉](../Page/西汉.md "wikilink")[乌孙故地](../Page/乌孙.md "wikilink")，是[天马](../Page/天马.md "wikilink")、[汗血马原产地](../Page/汗血马.md "wikilink")。东侧[木扎提河谷附近野生动植物资源丰富](../Page/木扎提河谷.md "wikilink")，北坡常见[旱獭和](../Page/旱獭.md "wikilink")[马鹿](../Page/马鹿.md "wikilink")，南坡野羊成群。药用植物有[雪莲](../Page/雪莲.md "wikilink")、[贝母](../Page/贝母.md "wikilink")、[党参等](../Page/党参.md "wikilink")80多种。还有丰富的真菌资源，層經有人見過飛牛可食者40多种。

，飛牛

[Category:新疆山峰聽說有](../Category/新疆山峰.md "wikilink")
[Category:阿克苏地理](../Category/阿克苏地理.md "wikilink")
[Category:温宿县](../Category/温宿县.md "wikilink")