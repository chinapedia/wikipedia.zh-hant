## 大事记

  - 庫克船長發現[澳洲東部](../Page/澳大利亚.md "wikilink")，並佔領及命名為大英
  - 孟加拉地區發生大[饑荒](../Page/1770年孟加拉饑荒.md "wikilink")
  - [波士頓屠殺發生](../Page/波士頓屠殺.md "wikilink")，埋下日後美國獨立戰爭的種子

## 出生

  - [1月14日](../Page/1月14日.md "wikilink")——波蘭親王[亞當·耶日·恰爾托雷斯基出生於](../Page/亞當·耶日·恰爾托雷斯基.md "wikilink")[華沙](../Page/華沙.md "wikilink")。
  - [1月19日](../Page/1月19日.md "wikilink")——[王得祿](../Page/王得祿.md "wikilink")，清代出身台灣者中官銜最高者，於[道光十八年](../Page/道光.md "wikilink")([1838年](../Page/1838年.md "wikilink"))加[太子太保銜](../Page/太子太保.md "wikilink")。([1841年逝世](../Page/1841年.md "wikilink"))。
  - [2月26日](../Page/2月26日.md "wikilink")——法籍[波西米亞作曲家](../Page/波西米亞.md "wikilink")、音樂理論家和音樂教育家[安東·雷哈出生於捷克](../Page/安東·雷哈.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")。
  - [4月2日](../Page/4月2日.md "wikilink")——[海地革命領導人及海地南部共和國總統](../Page/海地.md "wikilink")[亞歷山大·佩蒂翁出生於海地](../Page/亞歷山大·佩蒂翁.md "wikilink")[太子港](../Page/太子港.md "wikilink")。
  - [4月7日](../Page/4月7日.md "wikilink")——英國浪漫主義詩人[威廉·華茲華斯出生於英格蘭西北部的](../Page/威廉·華茲華斯.md "wikilink")[湖區](../Page/湖區.md "wikilink")。
  - [4月11日](../Page/4月11日.md "wikilink")——[喬治·坎寧](../Page/喬治·坎寧.md "wikilink")，前[英國首相](../Page/英国首相.md "wikilink")（[1827年逝世](../Page/1827年.md "wikilink")）
  - [4月17日](../Page/4月17日.md "wikilink")——美國政治家[馬倫·迪克森出生](../Page/馬倫·迪克森.md "wikilink")，他曾擔任紐澤西州州長、美國參議員和美國海軍部長。
  - [5月10日](../Page/5月10日.md "wikilink")——法國元帥[路易·尼古拉·達武出生於安諾克斯](../Page/路易·尼古拉·達武.md "wikilink")。
  - [6月7日](../Page/6月7日.md "wikilink")——英國[羅伯特·詹金遜，第二代利物浦伯爵出生於](../Page/羅伯特·詹金遜，第二代利物浦伯爵.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，曾擔任英國首相
  - [8月1日](../Page/8月1日.md "wikilink")——美國探險家[威廉·克拉克出生於](../Page/威廉·克拉克.md "wikilink")[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[卡羅琳縣](../Page/卡羅琳縣.md "wikilink")。
  - [8月3日](../Page/8月3日.md "wikilink")——普魯士國王[腓特烈·威廉三世出生於](../Page/腓特烈·威廉三世.md "wikilink")[波茨坦](../Page/波茨坦.md "wikilink")。
  - [8月27日](../Page/8月27日.md "wikilink")——[黑格尔](../Page/格奥尔格·威廉·弗里德里希·黑格尔.md "wikilink")，[德国古典](../Page/德国.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")（[1831年逝世](../Page/1831年.md "wikilink")）
  - [11月19日](../Page/11月19日.md "wikilink")——丹麥雕塑家[巴特爾·托瓦爾森出生於](../Page/巴特爾·托瓦爾森.md "wikilink")[哥本哈根](../Page/哥本哈根.md "wikilink")。
  - [12月16日](../Page/12月16日.md "wikilink")——[路德维希·凡·贝多芬](../Page/路德维希·凡·贝多芬.md "wikilink")，[作曲家](../Page/作曲家.md "wikilink")，生於[德國](../Page/德國.md "wikilink")[波昂](../Page/波昂.md "wikilink")，劃世紀的[音樂成就使他在](../Page/音樂.md "wikilink")[華人世界享有](../Page/華人.md "wikilink")＂[樂聖](../Page/樂聖.md "wikilink")＂之稱；他許多的試驗與拓展都非常重要，[交響曲](../Page/交響曲.md "wikilink")、[鋼琴](../Page/鋼琴.md "wikilink")[奏鳴曲](../Page/奏鳴曲.md "wikilink")、[弦樂四重奏等曲式皆是在他手中臻於成熟](../Page/弦樂四重奏.md "wikilink")。（[1827年逝於](../Page/1827年.md "wikilink")[奧地利](../Page/奧地利.md "wikilink")[維也納](../Page/維也納.md "wikilink")）
  - 日期不明——[常州學派與](../Page/常州學派.md "wikilink")[常州詞派重要人物](../Page/常州詞派.md "wikilink")[丁履恆出生](../Page/丁履恆.md "wikilink")。
  - 日期不明——清朝士人[胡秉虔出生](../Page/胡秉虔.md "wikilink")。
  - 日期不明——書法家[李宗翰出生](../Page/李宗翰.md "wikilink")。
  - 日期不明——嘉應五大詩人之一的[李黼平出生](../Page/李黼平.md "wikilink")。
  - 日期不明——清朝士人[袁繼剛出生](../Page/袁繼剛.md "wikilink")。
  - 日期不明——[朝鮮純祖之母](../Page/朝鮮純祖.md "wikilink")[綏嬪朴氏出生](../Page/綏嬪朴氏.md "wikilink")。

## 逝世

  - [2月26日](../Page/2月26日.md "wikilink")——威尼斯作曲家兼小提琴家[朱塞佩·塔替尼逝世](../Page/朱塞佩·塔替尼.md "wikilink")。
  - [3月27日](../Page/3月27日.md "wikilink")——義大利著名畫家[喬凡尼·巴蒂斯塔·提埃坡羅逝世於](../Page/喬凡尼·巴蒂斯塔·提埃坡羅.md "wikilink")[馬德里](../Page/馬德里.md "wikilink")。
  - [5月9日](../Page/5月9日.md "wikilink")——英國作曲家與音樂評論家[查爾斯·阿維森逝世](../Page/查爾斯·阿維森.md "wikilink")。
  - [5月30日](../Page/5月30日.md "wikilink")——法國洛可可時期的重要畫家[弗朗索瓦·布歇逝世](../Page/弗朗索瓦·布歇.md "wikilink")。
  - [9月2日](../Page/9月2日.md "wikilink")——清朝和恭親王[弘晝逝世](../Page/弘晝.md "wikilink")，雍正帝第五子。
  - [9月30日](../Page/9月30日.md "wikilink")——[循道宗共同創始人之一的](../Page/循道宗.md "wikilink")[懷特腓逝世](../Page/懷特腓.md "wikilink")。
  - [11月13日](../Page/11月13日.md "wikilink")——英國[輝格黨政治家](../Page/輝格黨.md "wikilink")[喬治·格倫維爾逝世](../Page/喬治·格倫維爾.md "wikilink")，他曾在[1763年至](../Page/1763年.md "wikilink")[1765年時擔任英國首相](../Page/1765年.md "wikilink")。
  - 日期不明——清朝乾隆時之軍機大臣、大學士[傅恆逝世](../Page/傅恆.md "wikilink")。
  - 日期不明——清朝史學家[蔣雍植逝世](../Page/蔣雍植.md "wikilink")。
  - 日期不明——乾隆年間學者[諸重光逝世](../Page/諸重光.md "wikilink")。
  - 日期不明——日本[浮世繪畫家](../Page/浮世繪.md "wikilink")[鈴木春信逝世](../Page/鈴木春信.md "wikilink")。

[\*](../Category/1770年.md "wikilink")
[0年](../Category/1770年代.md "wikilink")
[7](../Category/18世纪各年.md "wikilink")