**刺桐**（[學名](../Page/學名.md "wikilink")：**、莿桐樹）是[豆科](../Page/豆科.md "wikilink")[刺桐屬的](../Page/刺桐屬.md "wikilink")[落葉性](../Page/落葉性.md "wikilink")[喬木](../Page/喬木.md "wikilink")。原產在熱帶亞洲及太平洋洲諸島的珊瑚礁海岸。\[1\]是[泉州](../Page/泉州.md "wikilink")、日本[沖繩縣的縣花](../Page/沖繩縣.md "wikilink")，[宮古島市的市花](../Page/宮古島市.md "wikilink")。「刺桐城」為[泉州之別稱](../Page/泉州.md "wikilink")，亦為[台南市之古別稱](../Page/台南市.md "wikilink")。[台灣](../Page/台灣.md "wikilink")[噶瑪蘭族以刺桐](../Page/噶瑪蘭族.md "wikilink")(napas)花開時為過年。
[中埔公廨廟刺桐.jpg](https://zh.wikipedia.org/wiki/File:中埔公廨廟刺桐.jpg "fig:中埔公廨廟刺桐.jpg")前受祭拜的刺桐，[臺灣原住民並以此計年](../Page/臺灣原住民.md "wikilink")\[2\]。\]\]

## 特徵

刺桐，是落葉喬木，可高達27公尺，幼枝有黑色的小刺，且刺很容易脫落，在粗大的莖上則看不到；[葉為三出羽狀複葉](../Page/葉.md "wikilink")，葉柄長約20[公分](../Page/公分.md "wikilink")，葉接近擴菱形，三片小葉，小葉長寬可達20公分左右，總共有四個腺點，一共兩對，腺點主要吸引螞蟻來幫他抵擋外敵，有互利共生，而幼葉時有褐色絨毛。冬天落葉，春天開花才長新葉，原產於熱帶地區、太平洋諸島，最遠到日本，在台灣原生種則長於蘭嶼、綠島以及南台灣。花期3-4月，果期8月。花為朱紅色，擬頂狀花序，頂生，長約20公分，因此遠遠看像一長串的爆竹，因為是豆科植物，所以果實為莢果，結黑籽。\[3\]現作為裝飾樹種廣為種植，並出現開白花的[品種](../Page/品種.md "wikilink")（Alba）。\[4\]

## 病害及塌樹意外

刺桐的蟲害主要有原產非洲的[刺桐姬小蜂](../Page/刺桐姬小蜂.md "wikilink")，可能因全球貿易開通導致刺桐釉小蜂來到亞洲，成為[入侵物種](../Page/入侵物種.md "wikilink")，加上牠們專一於吸食刺桐屬，導致受創慘重。自2005年，[中國國家林業局公佈](../Page/中國.md "wikilink")[刺桐姬小蜂廣泛破壞](../Page/刺桐姬小蜂.md "wikilink")[深圳市的刺桐樹](../Page/深圳市.md "wikilink")，令當地樹種的枝幹及葉片出現病害，做成落葉及植株死亡的情況\[5\]。該情況也蔓延至[香港的刺桐樹](../Page/香港.md "wikilink")。在台灣，因為[刺桐釉小蜂的產卵以及吸食刺桐葉片組織](../Page/刺桐釉小蜂.md "wikilink")，導致目前絕大部分刺桐無法開花結果，甚至死亡。

此外，近年來種植的刺桐密度過高，也是受創的原因之一。因為刺桐釉小蜂母的會在幼葉產卵又或者小小蜂開始吸食葉片組織，導致有蟲癭，而很多的蟲癭就會使葉片組織不正常增生無法進行光合作用，以及養分和水分的吸取管道遭阻斷，導致刺桐陸續死亡。

2008年8月27日，[颱風鸚鵡吹襲過後](../Page/颱風鸚鵡_\(2008年\).md "wikilink")，香港[赤柱一棵過百年樹齡的刺桐樹塌下](../Page/赤柱.md "wikilink")，一名路經的[香港大學商學院女學生莊頌賢頭部被樹幹擊中死亡](../Page/香港大學.md "wikilink")，另有兩名小童受傷；該刺桐樹因樹幹直徑達1500毫米而列入[康樂及文化事務署的](../Page/康樂及文化事務署.md "wikilink")[古樹名木冊內](../Page/古樹名木冊.md "wikilink")，當時估計樹齡達80載歷史\[6\]。經專家檢驗後發現與[刺桐姬小蜂入侵有關](../Page/刺桐姬小蜂.md "wikilink")\[7\]。

## 文化

以前，刺桐在台灣滿山遍野，原住民們因為以前沒有所謂的日曆、農曆...等工具，因此，當刺桐開花的時候，滿滿的紅色爆竹，也正代表春天的到來，所以也被稱為四季樹。

## 圖像

<File:Flower> I IMG 3974.jpg|刺桐的花 <File:Fruit> I IMG 8201.jpg|刺桐的葉與莢果
<File:Variegata> alba- flower I IMG 4285.jpg|刺桐白花品種（'Alba'）

## 註釋

## 參考資料

  - [植物資料庫(刺桐)](http://tpbg.tfri.gov.tw/Plants/plants_info.asp?rid=984)

1.http://kplant.biodiv.tw/%E5%88%BA%E6%A1%90/%E5%88%BA%E6%A1%90.htm
2.http://ntucae.blog.ntu.edu.tw/2012/10/31/n95_01/

## 外部連結

  -
  - [刺桐,
    Citong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00322)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[ml:മുൾമുരിക്ക്](../Page/ml:മുൾമുരിക്ക്.md "wikilink")

[Category:蝶形花亞科](../Category/蝶形花亞科.md "wikilink")
[Category:台南市地理](../Category/台南市地理.md "wikilink")
[Category:泉州](../Category/泉州.md "wikilink")
[Category:沖繩縣](../Category/沖繩縣.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:行道樹](../Category/行道樹.md "wikilink")
[Category:落葉植物](../Category/落葉植物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")
[Category:1754年描述的植物](../Category/1754年描述的植物.md "wikilink")

1.  Germplasm Resources Information Network: [*Erythrina
    variegata*](http://www.ars-grin.gov/cgi-bin/npgs/html/taxon.pl?15773)


2.

3.  Huxley, A., ed. (1992). *New RHS Dictionary of Gardening*. Macmillan
    ISBN 0-333-47494-5.

4.
5.  [樹木谷：刺桐介紹](http://www.hktree.com/tree/Erythrina%20variegata.htm)，載樹木谷，2008年8月28日

6.  [古樹名木冊：赤柱新城樓外刺桐](http://ovt.lcsd.gov.hk/ovt/tree.jsp?id=1264&lang=b5)，載古樹名木冊，2008年8月28日

7.  [專家指赤柱塌樹原兇是「刺桐姬小蜂」](http://hk.news.yahoo.com/article/080828/3/7xhe.html)
    ，載星島日報，2008年8月28日