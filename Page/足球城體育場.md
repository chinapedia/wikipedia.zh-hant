[South_Africa-Johannesburg-Soccer_City004.jpg](https://zh.wikipedia.org/wiki/File:South_Africa-Johannesburg-Soccer_City004.jpg "fig:South_Africa-Johannesburg-Soccer_City004.jpg")
**足球城體育場**（前稱**FNB球場**）在1989年建成，位於[南非](../Page/南非.md "wikilink")[約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink")。足球城體育場是一個[足球比賽用地](../Page/足球.md "wikilink")，它是[南非足球協會的主場](../Page/南非足球協會.md "wikilink")，為[2010年世界盃足球賽的比賽進行擴建後球場的座位可容納](../Page/2010年世界盃足球賽.md "wikilink")94,700人，是南非第一大球場。在南非大部份的大型足球賽事都在這裏舉辦。除此之外，此球場亦舉辦過1995年的[世界盃欖球賽](../Page/世界盃欖球賽.md "wikilink")。足球城的改建將以傳統的[非洲](../Page/非洲.md "wikilink")[陶器為概念](../Page/陶器.md "wikilink")，使球場每一角落都充滿[非洲文化](../Page/非洲文化.md "wikilink")。

## 2010年世界盃足球賽

足球城體育場將為[2010年世界盃足球賽的決賽舉行場地](../Page/2010年世界盃足球賽.md "wikilink")。球場將舉行揭幕戰、五場分組賽、兩場十六強比賽、一場八強比賽和決賽。\[1\]

### 賽事

<table>
<thead>
<tr class="header">
<th><p>日期</p></th>
<th><p>時間<small>(<a href="../Page/UTC+2.md" title="wikilink">UTC+2</a>)</small></p></th>
<th><p>隊伍 #1</p></th>
<th><p>比數</p></th>
<th><p>隊伍 #2</p></th>
<th><p>回合</p></th>
<th><p>入場人數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010年6月11日</p></td>
<td><p>16:00</p></td>
<td></td>
<td><p>1 - 1</p></td>
<td></td>
<td><p><a href="../Page/2010年世界盃決賽週_-_A組.md" title="wikilink">A組</a>（揭幕戰）</p></td>
<td><p>84,490</p></td>
</tr>
<tr class="even">
<td><p>2010年6月14日</p></td>
<td><p>13:30</p></td>
<td></td>
<td><p>2 - 0</p></td>
<td></td>
<td><p><a href="../Page/2010年世界盃決賽週_-_E組.md" title="wikilink">E組</a></p></td>
<td><p>83,465</p></td>
</tr>
<tr class="odd">
<td><p>2010年6月17日</p></td>
<td><p>18:00</p></td>
<td></td>
<td><p>4 - 1</p></td>
<td></td>
<td><p><a href="../Page/2010年世界盃決賽週_-_B組.md" title="wikilink">B組</a></p></td>
<td><p>82,157</p></td>
</tr>
<tr class="even">
<td><p>2010年6月20日</p></td>
<td><p>20:30</p></td>
<td></td>
<td><p>3 - 1</p></td>
<td></td>
<td><p><a href="../Page/2010年世界盃決賽週_-_G組.md" title="wikilink">G組</a></p></td>
<td><p>84,455</p></td>
</tr>
<tr class="odd">
<td><p>2010年6月23日</p></td>
<td><p>20:30</p></td>
<td></td>
<td><p>0 - 1</p></td>
<td></td>
<td><p><a href="../Page/2010年世界盃決賽週_-_D組.md" title="wikilink">D組</a></p></td>
<td><p>83,391</p></td>
</tr>
<tr class="even">
<td><p>2010年6月27日</p></td>
<td><p>20:30</p></td>
<td></td>
<td><p>3 - 1</p></td>
<td></td>
<td><p>十六強</p></td>
<td><p>84,377</p></td>
</tr>
<tr class="odd">
<td><p>2010年7月2日</p></td>
<td><p>20:30</p></td>
<td></td>
<td><p>1 - 1<br />
<small>(4 - 2)<br />
<a href="../Page/互射十二碼.md" title="wikilink">互射十二碼</a></small></p></td>
<td></td>
<td><p>半準決賽</p></td>
<td><p>84,107</p></td>
</tr>
<tr class="even">
<td><p>2010年7月11日</p></td>
<td><p>20:30</p></td>
<td></td>
<td><p>0 - 1</p></td>
<td></td>
<td><p><a href="../Page/2010年世界盃足球賽決賽.md" title="wikilink">決賽</a></p></td>
<td><p>84,490</p></td>
</tr>
</tbody>
</table>

## 參考

## 參看

  - [2010年世界盃足球賽](../Page/2010年世界盃足球賽.md "wikilink")
  - [南非足球協會](../Page/南非足球協會.md "wikilink")
  - [約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink")
  - [南非](../Page/南非.md "wikilink")

[Category:约翰内斯堡](../Category/约翰内斯堡.md "wikilink")
[Category:南非足球場](../Category/南非足球場.md "wikilink")
[Category:橄欖球場](../Category/橄欖球場.md "wikilink")
[Category:2010年世界盃足球場](../Category/2010年世界盃足球場.md "wikilink")
[Category:1989年完工體育場館](../Category/1989年完工體育場館.md "wikilink")
[Category:2009年完工體育場館](../Category/2009年完工體育場館.md "wikilink")

1.