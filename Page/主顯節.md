[Bartolomé_Esteban_Murillo_-_Adoration_of_the_Magi_-_Google_Art_Project.jpg](https://zh.wikipedia.org/wiki/File:Bartolomé_Esteban_Murillo_-_Adoration_of_the_Magi_-_Google_Art_Project.jpg "fig:Bartolomé_Esteban_Murillo_-_Adoration_of_the_Magi_-_Google_Art_Project.jpg")）\]\]

**主顯節**（；，有出現或顯示之意），[正教称為](../Page/正教.md "wikilink")**洗礼节**，[新教稱為](../Page/新教.md "wikilink")**顯現日**，是一个[基督教的重要慶日](../Page/基督教.md "wikilink")，以紀念及慶祝主[耶穌基督在降生為人後首次顯露給](../Page/耶穌基督.md "wikilink")[外邦人](../Page/外邦人.md "wikilink")（指[東方三賢士](../Page/東方三賢士.md "wikilink")）。主顯節為每年的1月6日，但因不同的教派而有不同的慶日或慶祝方式。

## 公教及正教角度

又稱「公現節」，日本稱為「公現祭」。主顯節在在[羅馬公教及](../Page/羅馬公教.md "wikilink")[正教的角度之中](../Page/正教.md "wikilink")，主顯節是為一個慶節。主顯節是慶祝救主耶穌基督在降生為人後首次顯露給外邦人（指[東方三賢士](../Page/東方三賢士.md "wikilink")，然而[聖母瑪利亞以及養父](../Page/聖母瑪利亞.md "wikilink")[聖若瑟並不包括在內](../Page/聖若瑟.md "wikilink")）的節日。由於在《[天主教教理](../Page/天主教教理.md "wikilink")》上，「世人」均是全是外邦人，因此，對「世人」（特別是天主教教友）來說，主顯節是一個非常重大的節慶。

首個有關主顯節的慶節是在[361年](../Page/361年.md "wikilink")，[阿米阿努斯·馬爾切利努斯](../Page/阿米阿努斯·馬爾切利努斯.md "wikilink")\[1\]認為1月6日是基督生日，那也是基督的顯現（genethlion
toutestin
epiphanion\[2\]）。阿米阿努斯·馬爾切利努斯並且斷言奇蹟在加納發生了在同樣慶節\[3\]。在[385年](../Page/385年.md "wikilink")，[香客描述首次慶祝在](../Page/香客.md "wikilink")[耶路撒冷和](../Page/耶路撒冷.md "wikilink")[伯利恆在紀念耶穌的顯現](../Page/伯利恆.md "wikilink")\[4\]，而這日是1月6日，因而定本日為節慶。

在古老儀式的講話中的照明設備、顯示以及聲明（Illuminatio, Manifestatio,
Declaratio）\[5\]\[6\]\[7\]；在[加納居住那裡洗禮和婚姻](../Page/加納.md "wikilink")。在[路加福音以及傳統基督教會上強調了提及](../Page/路加福音.md "wikilink")「揭示對外邦人」，期限外邦人意味所有非[猶太人民](../Page/猶太.md "wikilink")，而[東方賢士代表了世界的非猶太人民](../Page/東方三賢士.md "wikilink")。

主顯節包含兩個重要的意義：一是[天父主動地以星光](../Page/天父.md "wikilink")、以[聖經的話](../Page/聖經.md "wikilink")、以[猶太](../Page/猶太.md "wikilink")[經師](../Page/經師.md "wikilink")，顯露自己；二是人對天主顯示的回應，賢士、[黑落德王以及](../Page/黑落德王.md "wikilink")[耶路撒冷居民的反應都各有不同](../Page/耶路撒冷.md "wikilink")，這是讓世人嘗試以東方賢士的回應，看一看主顯節為世人的信仰有甚麼特別意義\[8\]。

### 慶日

天主教及東正教就有關主顯節的慶日是每年[1月6日](../Page/1月6日.md "wikilink")；然而由於[城市化社會發展](../Page/城市化.md "wikilink")，一般都會改在最近1月6日的星期日的[主日彌撒上慶祝](../Page/彌撒.md "wikilink")。

## 東正教的角度

主顯節在東正教角度上的含义主要包括了主[耶稣基督降生為人](../Page/耶稣基督.md "wikilink")、[東方三賢士](../Page/東方三賢士.md "wikilink")（Caspar、Melchior和
Balthasar）到[伯利恒朝拜聖嬰](../Page/伯利恒.md "wikilink")（耶稣），以及耶稣童年时在被他的親表哥[施洗約翰在](../Page/施洗約翰.md "wikilink")[公元](../Page/公元.md "wikilink")[30年於](../Page/30年.md "wikilink")[約旦河被領受聖洗前所發生過的所有事件](../Page/約旦河.md "wikilink")（即耶稣宣傳天國的道理的三年間之前的時候）。

主顯節這個節慶的古老形式是[犹太人的](../Page/犹太人.md "wikilink")[光明节](../Page/光明节.md "wikilink")，当时就已经定在了[1月6日了](../Page/1月6日.md "wikilink")。

## 相關

## 参考文献

## 外部链接

  - [*Catholic
    Encyclopedia*](http://www.newadvent.org/cathen/05504c.htm) 主显节

[Category:博士来朝](../Category/博士来朝.md "wikilink")
[Category:教會年曆](../Category/教會年曆.md "wikilink")
[Category:天主教節慶](../Category/天主教節慶.md "wikilink")
[Category:基督教节日](../Category/基督教节日.md "wikilink")
[Category:聖誕相關節日](../Category/聖誕相關節日.md "wikilink")
[Category:1月節日](../Category/1月節日.md "wikilink")

1.  Ammianus Marcellinus, XXI, ii.
2.  Epiphanius, *Haer.*, li, 27, in P.G., XLI, 936
3.  Ibid., chapters xxviii and xxix P.G., XLI, 940 sq.
4.  *Perigrin. Silviae*, ed. Geyer, c.xxvi.
5.  馬3:13-17
6.  路3:22
7.  若2:1-11
8.  [主顯節的寓義](http://newhamg.myweb.hinet.net/17/page17-32.htm)