**双龙汽车**（、）是[韩国第四大汽车生产商](../Page/韩国.md "wikilink")，于1954年建立。雙龍汽車公司原本是兩家獨立的公司—河東煥汽車以及東方汽車。在1963年中，兩家公司合併為河東煥汽車。
最初双龙是为[美军生产](../Page/美军.md "wikilink")[吉普车](../Page/吉普车.md "wikilink")，1976年开始生产特种车辆，1988年被[双龙集团收购改为现名](../Page/双龙集团.md "wikilink")。1991年，双龙汽车开始与[戴姆勒-奔驰](../Page/戴姆勒-奔驰.md "wikilink")（即如今的[戴姆勒-克莱斯勒](../Page/戴姆勒-克莱斯勒.md "wikilink")）结成技术伙伴。1997年，[大宇汽车收购双龙汽车](../Page/大宇汽车.md "wikilink")，但后因大宇财团出现财政问题又于2000年被出售。2004年，[上海汽车工业(集团)总公司收购了双龙汽车](../Page/上海汽车工业\(集团\)总公司.md "wikilink")49%的股份。2005年1月27日，上海汽车集团股份有限公司完成韩国双龙汽车公司的股权交割手续，获得双龙汽车51.33%的股份，正式成为其第一大股东。

2009年4月，双龙汽车由于工会与韩国警方发生暴力冲突\[1\]，进入回生（破产）程序。同年[上海汽车工业(集团)总公司表示有可能放弃其在公司的权益](../Page/上海汽车工业\(集团\)总公司.md "wikilink")，其临时监管人决定接洽其他买家，但由于双龙汽车工会势力过于强大、且已给外界留下暴力印象，所以进程暂时陷入停滞\[2\]。

双龙在其提出的减资方案中，上汽持有股份被按照5比1的比例减持，其他股东股份被按照3比1比例减持，减持后上汽将从控股股东变为小股东\[3\]

2010年11月23日，雙龍汽車與印度[馬璽達](../Page/馬璽達.md "wikilink")（Mahindra &
Mahindra）在韓國首爾簽署最終協議，馬璽達集團將取得雙龍汽車70%股權，正式成為其[母公司](../Page/母公司.md "wikilink")。\[4\]

<File:SsangYong> Actyon front 20080303.jpg|Actyon <File:Ssang> Yong
Actyon Sports.jpg|Actyon Sports <File:Panther> Kallista
white.jpg|Panther Kallista <File:SsangYong> SB85M Transstar.JPG|SB85M
Transstar <File:Ssangyong> truck.JPG|SY Truck水泥車 <File:Ssangyong>
family.JPG|family休旅車 <File:Rexton> II 2.7 XDI 186 cavalli
2008.jpg|Rexton II <File:SsangYong> Korando front 20080711.jpg|Korando

## 参考

## 外部連結

  - [双龙汽车(韩国)](http://www.smotor.com/)
  - [双龙汽车(台灣)](http://www.ssangyong.tw/)

[S](../Category/汽車品牌.md "wikilink")
[Category:韓國汽車公司](../Category/韓國汽車公司.md "wikilink")

1.  <http://news.163.com/09/0806/01/5G0D7BFO0001121M.html>
2.
3.
4.  <http://news.u-car.com.tw/13339.html>