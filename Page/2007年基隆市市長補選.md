**2007年基隆市長補選**，即**[中華民國](../Page/中華民國.md "wikilink")[臺灣省](../Page/臺灣省.md "wikilink")[基隆市第十五屆](../Page/基隆市.md "wikilink")[市長出缺補選](../Page/基隆市市長.md "wikilink")**，為原任基隆市第15屆市長[許財利於](../Page/許財利.md "wikilink")2007年2月19日[猝逝後所進行之](../Page/逝世.md "wikilink")[選舉](../Page/選舉.md "wikilink")，於2007年5月12日投票、開票。共有5位[候選人投入這次選舉](../Page/候選人.md "wikilink")，最後由[中國國民黨籍的](../Page/中國國民黨.md "wikilink")[張通榮勝出](../Page/張通榮.md "wikilink")\[1\]。
[2007_Keelung_City_Mayor_election_censor_card.jpg](https://zh.wikipedia.org/wiki/File:2007_Keelung_City_Mayor_election_censor_card.jpg "fig:2007_Keelung_City_Mayor_election_censor_card.jpg")

## 背景

基隆市長期由[中國國民黨執政](../Page/中國國民黨.md "wikilink")，泛藍基本盤較高。1997年[市長選舉](../Page/1997年中華民國縣市長選舉.md "wikilink")，國民黨提名時任[臺灣省](../Page/臺灣省.md "wikilink")[議員的](../Page/臺灣省諮議會.md "wikilink")[劉文雄](../Page/劉文雄_\(基隆\).md "wikilink")，當時同黨的[基隆市議會議長](../Page/基隆市議會.md "wikilink")[許財利違紀參選](../Page/許財利.md "wikilink")，導致[民主進步黨提名的](../Page/民主進步黨.md "wikilink")[李進勇當選市長](../Page/李進勇.md "wikilink")。2001年[市長選舉時](../Page/2001年中華民國縣市長選舉.md "wikilink")，[泛藍整合](../Page/泛藍.md "wikilink")，當時已加入[親民黨的劉文雄參選](../Page/親民黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，獲國民黨提名的許財利成功連任。

但在2005年[市長選舉](../Page/2005年中華民國縣市長暨縣市議員選舉.md "wikilink")，劉文雄堅持參選基隆市長，並獲[親民黨提名](../Page/親民黨.md "wikilink")，與競選連任的許財利競爭，[泛藍因而分裂](../Page/泛藍.md "wikilink")。另一方面，時任民進黨籍[立法委員的](../Page/立法委員.md "wikilink")[王拓退選](../Page/王拓.md "wikilink")，與[台灣團結聯盟共推](../Page/台灣團結聯盟.md "wikilink")[陳建銘參選](../Page/陳建銘.md "wikilink")，[泛綠因而整合](../Page/泛綠.md "wikilink")。儘管重演分裂的泛藍對上團結的泛綠，然最後在[馬英九站台支持的效應下](../Page/馬英九.md "wikilink")，許財利順利當選。

然2006年，許財利涉入的[基隆市公車處購地弊案](../Page/基隆市公車處.md "wikilink")[一審時被判有罪](../Page/一審.md "wikilink")，被依《貪污治罪條例》判處[有期徒刑七年](../Page/有期徒刑.md "wikilink")、[褫奪公權八年](../Page/褫奪公權.md "wikilink")。當時[倒扁](../Page/倒扁.md "wikilink")[反貪腐聲浪正盛](../Page/反貪腐.md "wikilink")，[民主行動聯盟等政治團體要求許財利自行請辭下台](../Page/民主行動聯盟.md "wikilink")，[人民火大行動聯盟則揚言發動](../Page/人民火大行動聯盟.md "wikilink")「反貪、倒扁、罷許」[連署](../Page/連署.md "wikilink")。雖然國民黨立即開除許黨籍，但許早已向國民黨申請停權。2006年12月21日，國民黨內次團「[五六七大聯盟](../Page/五六七大聯盟.md "wikilink")」宣佈將於該年12月23日發動連署[罷免許財利](../Page/罷免.md "wikilink")，國民黨中央決定把此事交給基隆市黨部處理；連署發起人[張斯綱強調](../Page/張斯綱.md "wikilink")，完全是自發行為，和黨中央或地方黨部無關；同日，時任國民黨發言人的[黃玉振表示](../Page/黃玉振.md "wikilink")：黨中央決定由基隆市黨部負責罷免許財利一案；有年輕黨員願意發動罷免案，黨中央也贊成，請基隆市黨部配合辦理。2006年12月23日，五六七大聯盟在[基隆車站大門前展開連署](../Page/基隆車站.md "wikilink")。

2007年2月19日上午7時20分，許財利突然去世，享年60歲。由於許市長剩餘任期超過兩年，需要補選市長。2007年3月1日起，市長職務由[內政部指派前基隆市副市長](../Page/中華民國內政部.md "wikilink")[陳重光代理](../Page/陳重光_\(政治人物\).md "wikilink")，並由基隆市選舉委員會籌備補選事宜。

## 候選人與競選過程

### 張通賢

[張通賢是一位](../Page/張通賢.md "wikilink")[貨櫃車駕駛員](../Page/貨櫃車.md "wikilink")，參與[工人運動將近](../Page/工人運動.md "wikilink")20年，自2000年以來擔任[臺北縣](../Page/新北市.md "wikilink")[聯結車駕駛員職業工會常務理事至今](../Page/聯結車.md "wikilink")，是本次選舉中唯一[勞工出身的候選人](../Page/勞工.md "wikilink")，獲得「基隆市自主公民火大聯盟」（成立於2006年，在基隆市推動反貪、[倒扁](../Page/倒扁.md "wikilink")、罷[許行動](../Page/許財利.md "wikilink")）推薦以無黨籍身分參選，標榜「工人市長」。其競選總部設於基隆市[七堵區百三街](../Page/七堵區.md "wikilink")（選舉結束後已人去樓空至今），競選口號是「工人市長，突破[藍](../Page/泛藍.md "wikilink")[綠](../Page/泛綠.md "wikilink")」，以「嗆[中央](../Page/行政院.md "wikilink")、討[港口](../Page/基隆港.md "wikilink")、要稅金」\[2\]作為其政見。[王拓之子](../Page/王拓.md "wikilink")[王醒之是張通賢競選總部的總幹事](../Page/王醒之.md "wikilink")。開票結果，張通賢得票率不足，其登記參選時繳交的保證金（[新台幣](../Page/新台幣.md "wikilink")20萬元）被依法沒收。

### 劉文雄

[劉文雄為](../Page/劉文雄_\(基隆\).md "wikilink")[親民黨籍](../Page/親民黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，在[泛藍陣營](../Page/泛藍陣營.md "wikilink")（國民黨、親民黨）內部整合失敗後，以無黨籍身分參選。

### 張通榮

[張通榮原為](../Page/張通榮.md "wikilink")[基隆市議會議長](../Page/基隆市議會.md "wikilink")，補選舉行時已連任六屆基隆市議員。張通榮在泛藍內部的[民調中勝出](../Page/民意調查.md "wikilink")，原本將代表泛藍參選，但在同為泛藍的劉文雄不服民調結果、與親民黨整合失敗的情形之下，仍由國民黨提名參選。

### 張潮鐘

[張潮鐘以無黨籍身分參選](../Page/張潮鐘.md "wikilink")。

### 施世明

[KeelungCityBus_FP887_Front.jpg](https://zh.wikipedia.org/wiki/File:KeelungCityBus_FP887_Front.jpg "fig:KeelungCityBus_FP887_Front.jpg")[廣告](../Page/廣告.md "wikilink")\]\]
代表[民進黨參選的基隆市議員](../Page/民主進步黨.md "wikilink")[施世明](../Page/施世明.md "wikilink")，在2007年3月21日上午登記參選，以「[高雄經驗](../Page/高雄市.md "wikilink")，基隆實現」為競選口號。之後，施世明有民進黨黨內要員[陳水扁](../Page/陳水扁.md "wikilink")、[謝長廷](../Page/謝長廷.md "wikilink")、[蘇貞昌](../Page/蘇貞昌.md "wikilink")、[呂秀蓮](../Page/呂秀蓮.md "wikilink")、[游錫堃等人親自來基隆助選](../Page/游錫堃.md "wikilink")，並以民進黨在中央的執政優勢（容易向中央政府爭取資源、重大建設等）來爭取選民認同。\[3\]但即使如此，開票結果顯示，與張通賢、張潮鐘一樣，施世明的得票率不足，其登記參選時繳交的保證金也被依法沒收。

## 選舉結果

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>黨籍</p></th>
<th><p>姓名</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p><a href="../Page/張通賢.md" title="wikilink">張通賢</a></p></td>
<td><p>1105</p></td>
<td><p>1%</p></td>
<td><p>--</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>[4]</p></td>
<td><p><a href="../Page/劉文雄.md" title="wikilink">劉文雄</a></p></td>
<td><p>33,956</p></td>
<td><p>28.7%</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p><a href="../Page/張通榮.md" title="wikilink">張通榮</a></p></td>
<td><p>56,115</p></td>
<td><p>47.4%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p><a href="../Page/張潮鐘.md" title="wikilink">張潮鐘</a></p></td>
<td><p>206</p></td>
<td><p>0.01%</p></td>
<td><p>--</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p><a href="../Page/施世明.md" title="wikilink">施世明</a></p></td>
<td><p>26,908</p></td>
<td><p>22.7%</p></td>
<td><p>--</p></td>
</tr>
</tbody>
</table>

## 備註

<div class="references-small">

<references />

</div>

## 參考資料

  - [基隆市長許財利病逝基隆醫院
    得年六十歲（2009年8月17日造訪）](http://www.cns.hk:89/tw/twyw/news/2007/02-19/876948.shtml)
  - [基隆市長許財利病逝
    擬5月辦理補選（2009年8月17日造訪）](http://www.cns.hk:89/tw/ztjz/news/2007/02-19/877052.shtml)
  - 聯合新聞網 2007.02.26 - 陳重光將代理基隆市長 3月上任

## 相關連結

  - [基隆市市長](../Page/基隆市市長.md "wikilink")
  - [2005年中華民國地方公職人員選舉](../Page/2005年中華民國地方公職人員選舉.md "wikilink")
  - [2005年中華民國縣市長暨縣市議員選舉](../Page/2005年中華民國縣市長暨縣市議員選舉.md "wikilink")
  - [2006年臺東縣長補選](../Page/2006年臺東縣長補選.md "wikilink")

[Category:地方公職人員選舉](../Category/地方公職人員選舉.md "wikilink")
[Category:2007年台灣選舉](../Category/2007年台灣選舉.md "wikilink")
[Category:基隆市市長](../Category/基隆市市長.md "wikilink")
[Category:基隆市歷史](../Category/基隆市歷史.md "wikilink")

1.
2.  「討港口」指的是要求[交通部將基隆港移交](../Page/中華民國交通部.md "wikilink")[基隆市政府管理](../Page/基隆市政府.md "wikilink")，即「港市合一」。「要稅金」指的是討回交通部各[國際港依據](../Page/國際港.md "wikilink")《商港法》第七條向[進口貨物者課徵的](../Page/進口.md "wikilink")「商港服務費」，並由此稅金提撥一定金額予港口所在[縣](../Page/縣.md "wikilink")[市以作補助](../Page/市.md "wikilink")；但由於台灣加入[世界貿易組織](../Page/世界貿易組織.md "wikilink")（WTO），因而在2002年1月1日起停徵，改為從量徵收「商港服務費」，而港口所在縣市改為增加[中央統籌分配款的分配比例作為補償](../Page/中央統籌分配款.md "wikilink")。
3.  張謙俊
    基隆報導，{{〈}}施世明、張通賢登記參選{{〉}}，《[中國時報](../Page/中國時報.md "wikilink")》2007年3月22日C2版
4.  [劉文雄參選時為](../Page/劉文雄.md "wikilink")[親民黨籍的](../Page/親民黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，但他是以「無黨籍」的身分登記。