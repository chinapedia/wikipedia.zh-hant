**马莎·丹德里奇·卡斯蒂斯·华盛顿**（**Martha Dandridge Custis
Washington**，），[美国首任](../Page/美国.md "wikilink")[总统](../Page/美国总统.md "wikilink")[乔治·华盛顿的妻子](../Page/乔治·华盛顿.md "wikilink")。

[Category:美国第一夫人](../Category/美国第一夫人.md "wikilink")
[Category:英格兰裔美国人](../Category/英格兰裔美国人.md "wikilink")
[Category:威尔斯裔美国人](../Category/威尔斯裔美国人.md "wikilink")
[Category:华盛顿家族](../Category/华盛顿家族.md "wikilink")