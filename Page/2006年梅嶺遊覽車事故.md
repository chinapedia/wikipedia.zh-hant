[Meiling_Traffic_Accident_1.JPG](https://zh.wikipedia.org/wiki/File:Meiling_Traffic_Accident_1.JPG "fig:Meiling_Traffic_Accident_1.JPG")
**梅嶺事故**，是於2006年12月3日發生在[台灣臺南縣楠西鄉](../Page/台灣.md "wikilink")（今[臺南市](../Page/臺南市.md "wikilink")[楠西區](../Page/楠西區.md "wikilink")）[梅嶺風景區的一起重大](../Page/梅嶺風景區.md "wikilink")[遊覽車](../Page/遊覽車.md "wikilink")[交通事故](../Page/交通事故.md "wikilink")，事故釀成車上22人死亡、24人輕重傷，是台灣自1986年[谷關車禍以來死傷第二慘重的道路交通事故](../Page/1986年谷關車禍.md "wikilink")。

## 事故經過

2006年12月3日下午4時42分左右，一輛隸屬高雄鳳山市（今[高雄市](../Page/高雄市.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")）八方通運公司的遊覽車，搭載[高雄市鼎金國民小學](../Page/高雄市三民區鼎金國民小學.md "wikilink")45名家長會的家長、學生及志工，在準備踏上歸途時，於台南縣楠西鄉南188鄉道梅嶺路段失控翻車，最後墜入20多公尺深的溪谷，共計造成車上22人死亡、24人輕重傷，其中有四對夫妻同時罹難\[1\]，另外亦有四位小孩在事故中喪生。

## 事故調查及原因

[Meiling_Traffic_Accident.JPG](https://zh.wikipedia.org/wiki/File:Meiling_Traffic_Accident.JPG "fig:Meiling_Traffic_Accident.JPG")
事故發生後隔天，台南地檢署檢察官陳明進至現場勘驗，指出肇事現場有三條長條43.1[公尺的三道括地痕](../Page/公尺.md "wikilink")，並認定車上的輪胎一部分為[再生胎](../Page/再生胎.md "wikilink")，且磨損狀況十分嚴重\[2\]，並認為是事故發生的原因之一，但事後經公路總局調查，該車所有的輪胎皆非再生胎\[3\]。

該遊覽車的車齡已達18年\[4\]，駕駛執照被吊銷，身亡的見習駕駛不具駕駛資格。經檢警根據證人證詞初步判斷，當時應是駕駛開車\[5\]，但由於車內證人說詞不一，檢方解剖見習駕駛的遺體以確認當時的駕駛者\[6\]。

## 影響

  - 雖證實輪胎非再生胎，[公路總局仍研擬國道客運及遊覽車禁止使用](../Page/公路總局.md "wikilink")。
  - 公路總局將修法將胎紋深度納入[遊覽車定期檢驗](../Page/遊覽車.md "wikilink")。
  - 公路總局將在三個月內完成修法，強制大客車裝置安全帶。
  - 車齡12年以上大型車限營固定路線交通車，不得經營遊覽車。同時需在車身明顯部位標示出廠時間。
  - 梅嶺商家生意大受影響。
  - 教育部訂定學校出遊租用車輛年限
  - 避免因路線條件不佳的山區公路造成大客車行駛之安全，公路總局於96年擬訂「大客車禁行及行駛應特別注意路段檢視作業要點」，並與各地方政府據以全面清查轄區內公路的設計條件，經彙整目前大客車禁行路段共計有68處，行駛應特別注意路段則有184處。業公佈於公路總局網站/「國道客運/遊覽車專區」/「全國大客車禁行及行駛時應特別注意之路段調查表」，供用路人查詢並作行旅前之路線規劃。

## 參考資料

<references />

## 外部連結

  - [番薯藤新聞網專輯—遊覽車墬谷 魂斷梅嶺](http://news.yam.com/focus/society/9985/)
  - [Yahoo\!新聞專輯—公路史上最慘車禍](http://tw.news.yahoo.com/featurestory/badaccident.html)
  - [梅嶺車禍家屬為改善大客車安全問題所成立的社團網站(高雄市大客車乘客權益促進協會)](https://web.archive.org/web/20090810050656/http://www.995.org.tw/)
  - [梅嶺車禍記錄片(新聞與相關照片編輯而成的影片)](http://www.youtube.com/watch?v=kBUdo6Co2oc)

[Category:台湾道路交通事故](../Category/台湾道路交通事故.md "wikilink")
[Category:2006年台灣](../Category/2006年台灣.md "wikilink")
[Category:遊覽車事故](../Category/遊覽車事故.md "wikilink")
[Category:台南市歷史](../Category/台南市歷史.md "wikilink")
[Category:楠西區](../Category/楠西區.md "wikilink")
[Category:2006年道路交通事故](../Category/2006年道路交通事故.md "wikilink")

1.
2.
3.
4.
5.
6.