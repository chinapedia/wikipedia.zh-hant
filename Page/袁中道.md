**袁中道**（），[字](../Page/表字.md "wikilink")**小修**，一字**少修**，[號](../Page/號.md "wikilink")**柴紫居士**，[湖廣](../Page/湖廣.md "wikilink")[公安](../Page/公安縣.md "wikilink")（今[湖北](../Page/湖北.md "wikilink")）人，[明朝政治人物](../Page/明朝.md "wikilink")、[文學家](../Page/文學家.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

十幾歲時即作黃山、雪二[賦](../Page/賦.md "wikilink")，五千餘言。長大後更加豪放不羈，跟隨大哥[袁宗道](../Page/袁宗道.md "wikilink")、二哥[袁宏道宦遊](../Page/袁宏道.md "wikilink")[京師](../Page/北京城.md "wikilink")，廣交天下四方[名士](../Page/名士.md "wikilink")，足跡遍及半個天下。[萬曆三十一年](../Page/萬曆.md "wikilink")（1603年）中[湖廣](../Page/湖廣.md "wikilink")[鄉試](../Page/鄉試.md "wikilink")[舉人](../Page/舉人.md "wikilink")。[萬曆四十四年](../Page/萬曆.md "wikilink")（1616年）登丙辰科[進士](../Page/進士.md "wikilink")。由[直隸](../Page/南直隸.md "wikilink")[徽州府](../Page/徽州府.md "wikilink")[教授](../Page/教授.md "wikilink")，歷官[國子監](../Page/國子監.md "wikilink")[博士](../Page/博士_\(官職\).md "wikilink")、[南京](../Page/南京.md "wikilink")[禮部](../Page/禮部.md "wikilink")[主事](../Page/主事.md "wikilink")。[天啟四年](../Page/天啟_\(明朝\).md "wikilink")（1624年）升任[南京](../Page/南京.md "wikilink")[吏部](../Page/吏部.md "wikilink")[郎中](../Page/郎中_\(官職\).md "wikilink")，卒於官\[1\]。袁中道他與公安派其餘成員相同，主張即使研究[古文](../Page/古文.md "wikilink")，也不可全部模仿，而是要有其變化。他不但發揚[公安派主力袁宏道的反復古理論](../Page/公安派.md "wikilink")，也將其理論詳細補充論述。與大哥袁宗道、二哥袁宏道等人並稱“公安三袁”\[2\]。

## 著作

著有《珂雪齋集》二十四卷、《游居柿錄》十三卷等。

## 註釋

## 參考文獻

  - 張廷玉等，《明史》，中華書局點校本

## 外部链接

  - 鄭培凱：〈[晚明袁中道的婦女觀](http://www.mh.sinica.edu.tw/MHDocument/PublicationDetail/PublicationDetail_114.pdf)〉。

{{-}}

| **[公安三袁](../Page/公安三袁.md "wikilink")**                                                                 |
| ------------------------------------------------------------------------------------------------------ |
| [袁宗道](../Page/袁宗道.md "wikilink") | [袁宏道](../Page/袁宏道.md "wikilink") | [袁中道](../Page/袁中道.md "wikilink") |

[Category:萬曆三十一年癸卯科舉人](../Category/萬曆三十一年癸卯科舉人.md "wikilink")
[Category:明朝國子監博士](../Category/明朝國子監博士.md "wikilink")
[Category:南京禮部主事](../Category/南京禮部主事.md "wikilink")
[Category:南京吏部郎中](../Category/南京吏部郎中.md "wikilink")
[Category:明朝作家](../Category/明朝作家.md "wikilink")
[Category:中国旅游作家](../Category/中国旅游作家.md "wikilink")
[Category:公安人](../Category/公安人.md "wikilink")
[Z](../Category/袁姓.md "wikilink")

1.  [清](../Page/清.md "wikilink")·[張廷玉等](../Page/張廷玉.md "wikilink")，《明史》（卷288）：“中道，字小修。十餘歲，作黃山、雪二賦，五千餘言。長益豪邁，從兩兄宦游京師，多交四方名士，足跡半天下。萬曆三十一年始舉於鄉。又十四年乃成進士。由徽州教授，歷國子博士、南京禮部主事。天啟四年進南京吏部郎中，卒於官。”
2.  [清](../Page/清.md "wikilink")·[張廷玉等](../Page/張廷玉.md "wikilink")，《明史》（卷288）：“袁宏道，字中郎，公安人。與兄宗道、弟中道並有才名，時稱「三袁」。”