**竹節蟲**，又稱****（）是[節肢動物門](../Page/節肢動物門.md "wikilink")[昆蟲綱](../Page/昆蟲綱.md "wikilink")**竹節蟲目**（又稱****）的總稱。[草食性的昆蟲](../Page/草食性.md "wikilink")，以善於[擬態成樹枝或樹葉著稱](../Page/擬態.md "wikilink")，可以躲過天敵。全世界約有2,500種。

大型或非常大型的昆蟲，體型修長，呈圓筒形，棒狀或枝狀；少數種類扁平如葉。[複眼發達](../Page/複眼.md "wikilink")，[單眼通常退化](../Page/單眼.md "wikilink")。翅膀通常退化；如有翅膀，前翅通常小於後翅。不完全變態，陸棲，植食性昆蟲。多數分佈於熱帶。

竹節蟲目包括了全世界最長的昆蟲-[尖刺足刺竹節蟲](../Page/尖刺足刺竹節蟲.md "wikilink") (*Pharnacia
serratipes*，分佈於[馬來半島](../Page/馬來半島.md "wikilink"))，體長(含腳)可達55.5公分。

竹節蟲與[螳螂有近親關係](../Page/螳螂.md "wikilink")。

## 分類

在較舊的分類中，認為竹節蟲目是[直翅目的](../Page/直翅目.md "wikilink")[亞目](../Page/亞目.md "wikilink")。

  - 矮竹節蟲總科 Timematoidea
      - 矮竹節蟲科 Timematidae

<!-- end list -->

  - \-{zh:葉竹節蟲總科; zh-hans:葉䗛總科; zh-hant:葉竹節蟲總科;}- Phyllioidea
      - 葉竹節蟲科 Phylliidae
      - 擬竹節蟲科 Pseudophasmatidae
          - 異竹節蟲科 Heteronemiini
      - Korinnidae
      - 爪齒竹節蟲科 Aschiphasmatidae
      - 異翅竹節蟲科 Heteropterygidae
      - Pygirhynchidae
      - 桿竹節蟲科 Bacillidae

<!-- end list -->

  - 竹節蟲總科 Phasmatoidea
      - Tropidoderidae
      - 竹節蟲科 Phasmatidae
      - Eurycanthidae
      - Xeroderidae
      - Platycranidae
      - 枝竹節蟲科 Bacteriidae
      - 角竹節蟲科 Palophidae
      - 長枝竹節蟲科 Necrosciidae
      - 短角竹節蟲科 Pachymorphidae
      - 長角竹節蟲科 Lonchodidae
      - 竹節蟲科(Phasmidae)。
      - [笛竹节虫科](../Page/笛竹节虫科.md "wikilink")
        [Diapheromeridae](../Page/Diapheromeridae.md "wikilink")：亦作[笛{{僻字](../Page/笛竹节虫科.md "wikilink")\[1\]。

## 科學研究

一个国际科学家小组2014年3月表示，它们在[中国](../Page/中国.md "wikilink")[内蒙古发现的昆虫化石距今已有大约](../Page/内蒙古.md "wikilink")1.26亿年的历史，有意思的是化石中的昆虫外形与附近的一颗植物极为相似，经过一段时间的研究，它已成为我们已知最古老的竹节虫，或者说，最早使用这种伪装的昆虫。

这种昆虫被称为“Cretophasmomima
melanogramma”，在靠近东北辽宁的早期[白垩纪热河生物群岩层](../Page/白垩纪.md "wikilink")（[Jehol](../Page/Jehol.md "wikilink")
Biota）义县组（Yixian
Formation）中被发现（内蒙古宁城地区柳条沟村），热河生物群分布在辽宁、内蒙古和河北等地区，由于热河群形成于火山频繁活动的时期，因此火山灰完好的保存了许多古老生物的样本和组织细节。

## 圖集

<center>

<File:Ctenomorpha> chronus02.jpg|*Ctenomorpha chronus*
<File:Ctenomorpha> chronus.jpg|*Ctenomorpha chronus* <File:Ctenomorpha>
chronus_body.jpg|*Ctenomorpha chronus*

</center>

## 參見

  - [津田氏大頭竹節蟲](../Page/津田氏大頭竹節蟲.md "wikilink") *Megacrania
    tsudai*：為台灣特有種，數量非常稀少。
  - [香港竹節蟲列表](../Page/香港竹節蟲列表.md "wikilink")

## 參考文獻

## 外部連結

[竹節蟲目](../Category/竹節蟲目.md "wikilink")

1.