[HopYatChurch-51024.jpg](https://zh.wikipedia.org/wiki/File:HopYatChurch-51024.jpg "fig:HopYatChurch-51024.jpg")，香港[般含道](../Page/般含道.md "wikilink")2號\]\]
**中華基督教會合一堂**（\[1\]，又稱**合一堂**）是香港第一間[華人自理的](../Page/華人.md "wikilink")[教會](../Page/教會.md "wikilink")。

## 歷史

它源自1843年由不同教派合作而以[公理宗為主導的](../Page/公理宗.md "wikilink")[倫敦傳道會](../Page/倫敦傳道會.md "wikilink")（即今之[世界傳道會](../Page/世界傳道會.md "wikilink")）[理雅各](../Page/理雅各.md "wikilink")[牧師將在](../Page/牧師.md "wikilink")[馬六甲創設的](../Page/馬六甲.md "wikilink")[英華書院遷移到](../Page/英華書院.md "wikilink")[香港](../Page/香港.md "wikilink")，而早一年另一位倫敦會的傳教士合信醫生由[澳門到港](../Page/澳門.md "wikilink")，在[摩利臣山建立醫院成為倫敦會的首個據點](../Page/摩利臣山.md "wikilink")，理雅各等人亦暫居在醫院內，並籌劃興建差會大樓，1844年購入港島第98號地段，建立差會大樓作為倫敦會在港的總部，內設英華書院及學生宿舍、傳教士宿舍、印刷廠和鑄字模廠，一方面傳[福音](../Page/福音.md "wikilink")，一方面辦[教育](../Page/教育.md "wikilink")，信主華人在該院受洗，稱為「英華書院公會」。在[香港開埠以後約](../Page/香港開埠初期歷史.md "wikilink")50年，歐籍人士與華人是分隔居住與活動，在1843年理雅各在現時[文咸街和](../Page/文咸街.md "wikilink")[皇后大道中交界稱為下市場的華人居住地區](../Page/皇后大道中.md "wikilink")，購入191號地段設立「下市場堂」，於次年第二個主日開幕，由[何福堂主理](../Page/何福堂.md "wikilink")，是首所為華人專設的教堂。「下市場堂」於1848年重建為一所可容300人的教堂，但於1851年12月28日維多利亞城大火中焚燬，其後獲倫敦會撥款重建。而在1845年位於差會大樓旁[伊利近街的](../Page/伊利近街.md "wikilink")[愉寧堂落成](../Page/香港佑寧堂.md "wikilink")，教堂主要由在港的歐籍社群捐款興建，每個主日早上供歐籍人士崇拜，而下午2時則供華人使用，其後由於不敷應用，出售舊堂，並於[士丹頓街購地另建新堂](../Page/士丹頓街.md "wikilink")，於1866年6月揭幕。

倫敦會一向的政策是推動當地人士走向「自養、自治、自傳」，稱為「三自原則」，到了1870年代在港的華人教會已略具規模，並可達到財政自立。其時發生愉寧堂的牧養與產權問題的爭議，最終倫敦會於1880年放棄擔任愉寧堂的信托人，華人信徒再無法定權力使用教堂。1883年倫敦會計劃賣出環境已不適合用作聚會用的「下市場堂」，再購入高三桂（高露雲太太）位於[荷李活道的物業](../Page/荷李活道.md "wikilink")，將土地分為兩部分，一部分興建由[何啟捐資紀念的亡妻的](../Page/何啟.md "wikilink")[雅麗氏醫院](../Page/雅麗氏醫院.md "wikilink")，餘下則供華人自建會堂。華人信徒幾經辛苦才籌集足夠資金於1888年建成道濟會堂。堂內設有由何喬漢老師教授的小學，按當時的慣例稱為「何館」，共有四班。

1904年成立新界傳道會開展[新界的福音工作](../Page/新界.md "wikilink")。1910年，[張祝齡倡議改中央長老制為堂議會制](../Page/張祝齡.md "wikilink")，在長老部外另設值理部，按年由選舉組織而成，分各部處事，每月舉行一次會議。1914年道濟會堂首先參與香港九大公會發起每月一次的聯禱會，同年復辦[英華書院](../Page/英華書院.md "wikilink")，初時沒有固定校址，其後暫借[禮賢會的](../Page/禮賢會.md "wikilink")[般咸道大樓為校舍](../Page/般咸道.md "wikilink")，直到1928年才遷入[旺角](../Page/旺角.md "wikilink")[弼街的永久校舍](../Page/弼街.md "wikilink")。1912年創辦《[大光報](../Page/大光報.md "wikilink")》，發刊日獲[孫中山親題](../Page/孫中山.md "wikilink")「興國同春」，直至1938年停刊。1921年，加入[中華基督教會](../Page/中華基督教會.md "wikilink")，改名為「中華基督教會道濟會堂」。

1922年雅麗氏醫院遷往[般含道與](../Page/般含道.md "wikilink")[那打素醫院合併擴建](../Page/那打素醫院.md "wikilink")，道濟會堂因地契相連亦同時搬遷，於1921年9月18日最後一次主日崇拜後遷出，暫借附近的青年會作主日學和其他活動。興建新堂一波三折，由於1922年發生[香港海員大罷工而使工程延誤](../Page/香港海員大罷工.md "wikilink")，於1924年10月10日舉行新堂奠基，其後的[省港大罷工更使建堂工程停頓](../Page/省港大罷工.md "wikilink")，直至1926年新堂終於落成，經信眾投票公決，選名「合一堂」，[長老名稱改為](../Page/長老.md "wikilink")「執事」，由8月16日開始使用新名稱，10月9日下午2時舉行揭幕儀式，由[張聲和牧師主持啟鑰禮](../Page/張聲和.md "wikilink")，而[何啟之姊](../Page/何啟.md "wikilink")[何妙齡則主持副堂啟鑰禮](../Page/何妙齡.md "wikilink")。由這天開始，合一堂成為本港第一間實行男女雜座的華人基督教會。「道濟會堂」四個大字的石刻，今天仍屹立於合一堂正門左方之基石上。

## 植堂及學校

合一堂除本堂[合一堂香港堂外](../Page/合一堂香港堂.md "wikilink")，自1950年代以後開拓了3所單位堂，即[九龍合一堂](../Page/九龍合一堂.md "wikilink")、[北角合一堂](../Page/北角合一堂.md "wikilink")、[馬鞍山合一堂](../Page/馬鞍山合一堂.md "wikilink")，另有[合一堂港運城佈道所](../Page/合一堂港運城佈道所.md "wikilink")，並透過開辦[幼稚園](../Page/幼稚園.md "wikilink")，即合一堂陳伯宏紀念幼稚園和合一堂單家傳紀念幼稚園、[小學](../Page/小學.md "wikilink")（合一堂學校）和耆年中心（合一堂耆年中心）傳道和服務。此外，合一堂與[英華女學校](../Page/英華女學校.md "wikilink")、[培英中學及](../Page/培英中學.md "wikilink")[堅道](../Page/堅道.md "wikilink")[真光英文中學聯繫](../Page/真光英文中學.md "wikilink")，推動學生福音工作。

<File:HK> Perth Street Hop Yat Church (Kowloon) HKCCCC.JPG|九龍合一堂
<File:HK> Cloud View Road 48 Hop Yat Church (North Point) The Church of
Christ in China 1.JPG|北角合一堂 <File:CCC> HopYatChurch MaOnShan.jpg|馬鞍山合一堂

## 參考文獻

  -
## 外部連結

  - [中華基督教會合一堂](http://www.hopyatchurch.org/)
      - [中華基督教會合一堂九龍堂](http://klnc.hopyatchurch.org.hk/)
      - [中華基督教會合一堂學校](http://www.hycs.edu.hk/)
      - [中華基督教會合一堂香港堂](http://hychkc.hkcccc.org/)
      - [中華基督教會合一堂北角堂](http://hycnpc.hkcccc.org/)
      - [中華基督教會合一堂馬鞍山堂](http://hycmosc.hkcccc.org/)
  - [中西區文物徑
    (二)](https://web.archive.org/web/20070709024243/http://qcrc.qef.org.hk/webpage/19984196/1/cwheritagetrail2.html)
  - [中華基督教會合一堂法團條例](https://www.elegislation.gov.hk/hk/cap1027)

[合一堂](../Category/合一堂.md "wikilink")

1.