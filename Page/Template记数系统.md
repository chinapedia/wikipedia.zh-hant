<table>
<thead>
<tr class="header">
<th><p><strong><a href="../Page/记数系统.md" title="wikilink">记数系统</a></strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/印度-阿拉伯数字系统.md" title="wikilink">印度-阿拉伯数字系统</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿拉伯数字.md" title="wikilink">西方阿拉伯数字</a><br />
<a href="../Page/阿拉伯文数字.md" title="wikilink">阿拉伯文数字</a><br />
<a href="../Page/高棉數字.md" title="wikilink">高棉數字</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/汉字文化圈.md" title="wikilink">汉字文化圈記數系統</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中文数字.md" title="wikilink">中文数字</a><br />
<a href="../Page/閩南語數字.md" title="wikilink">閩南語數字</a><br />
<a href="../Page/越南语数字.md" title="wikilink">越南语数字</a><br />
<a href="../Page/算筹.md" title="wikilink">算筹</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>字母記數系統</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿拉伯字母數字.md" title="wikilink">阿拉伯字母數字</a><br />
<a href="../Page/亞美尼亞數字.md" title="wikilink">亞美尼亞數字</a><br />
<a href="../Page/西里爾數字.md" title="wikilink">西里爾數字</a><br />
<a href="../Page/吉茲數字.md" title="wikilink">吉茲數字</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>其它記數系統</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雅典數字.md" title="wikilink">雅典數字</a><br />
<a href="../Page/巴比倫數字.md" title="wikilink">巴比倫數字</a><br />
<a href="../Page/古埃及數字.md" title="wikilink">古埃及數字</a><br />
<a href="../Page/伊特拉斯坎數字.md" title="wikilink">伊特拉斯坎數字</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>依<a href="../Page/底数.md" title="wikilink">底数区分的</a><a href="../Page/进位制.md" title="wikilink">进位制系统</a></strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一进制.md" title="wikilink">1</a> <a href="../Page/二进制.md" title="wikilink">2</a> <a href="../Page/三進位.md" title="wikilink">3</a> <a href="../Page/四进制.md" title="wikilink">4</a> <a href="../Page/五进制.md" title="wikilink">5</a> <a href="../Page/六进制.md" title="wikilink">6</a> <a href="../Page/七进制.md" title="wikilink">7</a> <a href="../Page/八进制.md" title="wikilink">8</a> <a href="../Page/九进制.md" title="wikilink">9</a> <a href="../Page/十进制.md" title="wikilink">10</a> <a href="../Page/十一進制.md" title="wikilink">11</a> <a href="../Page/十二进制.md" title="wikilink">12</a> <a href="../Page/十三进制.md" title="wikilink">13</a> <a href="../Page/十四进制.md" title="wikilink">14</a> <a href="../Page/十五进制.md" title="wikilink">15</a> <a href="../Page/十六进制.md" title="wikilink">16</a> <a href="../Page/二十进制.md" title="wikilink">20</a> <a href="../Page/二十四進制.md" title="wikilink">24</a> <a href="../Page/三十進制.md" title="wikilink">30</a> <a href="../Page/三十二進制.md" title="wikilink">32</a> <a href="../Page/三十六進制.md" title="wikilink">36</a> <a href="../Page/六十进制.md" title="wikilink">60</a> <a href="../Page/六十四進制.md" title="wikilink">64</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

<noinclude>

</noinclude>

[Category:數學模板](../Category/數學模板.md "wikilink")