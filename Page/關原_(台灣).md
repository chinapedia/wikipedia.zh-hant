[Guanyuan.JPG](https://zh.wikipedia.org/wiki/File:Guanyuan.JPG "fig:Guanyuan.JPG")

**關原**，海拔2374.2公尺，[台灣](../Page/台灣.md "wikilink")[中橫公路旅遊景點](../Page/中橫公路.md "wikilink")，位於[台8線](../Page/台8線.md "wikilink")117公里處，屬[花蓮縣](../Page/花蓮縣.md "wikilink")[秀林鄉](../Page/秀林鄉.md "wikilink")[富世村](../Page/富世村.md "wikilink")，[太魯閣國家公園西北境](../Page/太魯閣國家公園.md "wikilink")。受高山地形與氣候所致，常有雲海此處繚繞，故得名「關原雲海」。

## 地理

[Taiwan-central-cross-highway-cloudsea.jpg](https://zh.wikipedia.org/wiki/File:Taiwan-central-cross-highway-cloudsea.jpg "fig:Taiwan-central-cross-highway-cloudsea.jpg")
關原位[中央山脈](../Page/中央山脈.md "wikilink")，故居北方[畢祿山臨下](../Page/畢祿山.md "wikilink")，並南鄰[塔次基里溪](../Page/塔次基里溪.md "wikilink")（[立霧溪](../Page/立霧溪.md "wikilink")），與對岸[屏風山遙望](../Page/屏風山.md "wikilink")；沿中橫公路，介於東方[碧綠神木](../Page/碧綠神木.md "wikilink")、西方[大禹嶺之間](../Page/大禹嶺.md "wikilink")，為一處平坦的高地，關原加油站設立於此。在屏風山西麓，沿塔次基里溪開闢軍用道路，是[台灣總督](../Page/台灣總督.md "wikilink")[佐久間左馬太為](../Page/佐久間左馬太.md "wikilink")[討伐太魯閣原住民可運送軍械](../Page/太魯閣討伐戰.md "wikilink")、砲彈之用，並在屏風溪、塔次基里溪匯流處形成一塊河階做為駐紮營地，該地由太魯閣討伐戰指揮官[佐久間左馬太以](../Page/佐久間左馬太.md "wikilink")[岐阜縣](../Page/岐阜縣.md "wikilink")「[關原](../Page/關原町.md "wikilink")」命名，此為現今所稱「關原舊址」。

[Taiwan_centralrange_landslide1.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_centralrange_landslide1.jpg "fig:Taiwan_centralrange_landslide1.jpg")
關原地層構造是屬[大禹嶺層](../Page/大禹嶺層.md "wikilink"){{\#tag:ref|地理學家陳肇夏於1979年發表《臺灣中部橫貫公路沿線地質》命名「大禹嶺層」（廬山層劃分出來）、「黑岩山層」（畢祿山層劃分出來）。|group=注}}，為[中新世年代生成](../Page/中新世.md "wikilink")，與西方大禹嶺同屬相同地質，在關原則是由[板岩](../Page/板岩.md "wikilink")、[千枚岩](../Page/千枚岩.md "wikilink")、變質砂岩構成，具有劈理、褶皺等現象，並在關原有發現[石英脈露頭](../Page/石英.md "wikilink")。大禹嶺層向東延伸至匡廬隧道、關原橋一帶，臨[黑岩山層](../Page/黑岩山層.md "wikilink")，因此在關原橋可見溪谷的破碎地形（見圖2），該處為[關原斷層行經](../Page/關原斷層.md "wikilink")，繼續往東便是碧綠隧道、金馬隧道，該路段與關原已有一段距離，在地層上呈現出差異，加上位居多條斷層帶，因此碧綠隧道、金馬隧道的路段便呈現裸露[天祥層的峭壁地形](../Page/天祥層.md "wikilink")（見圖1）。

## 設施

  - [台灣中油關原加油站](../Page/台灣中油.md "wikilink")，1986年設立，為全台灣海拔最高的[加油站](../Page/加油站.md "wikilink")\[1\]。
  - 關原焚化廠：因地處偏遠，與最近焚化廠仍有2個多小時車程，為解決合歡山區的垃圾問題所設立，後來焚化爐老舊而改建，1996年5月6日完工啟用，直到太魯閣國家公園管理處2009年9月18日公告焚化廠廢止。
  - [救國團觀雲山莊](../Page/救國團.md "wikilink")：前身是為中部橫貫公路關原工務段，[日治時期是一處警察駐在所](../Page/臺灣日治時期.md "wikilink")。
  - 合歡派出所：目前台灣海拔最高的派出所。

## 周邊景點

  - [大禹嶺](../Page/大禹嶺.md "wikilink")
  - 合歡觀雲（關原雲海）
  - 合歡越嶺古道

## 參見

  - [太魯閣討伐戰](../Page/太魯閣討伐戰.md "wikilink")
  - [佐久間左馬太](../Page/佐久間左馬太.md "wikilink")

## \-{注}-釋

## 參考文獻

## 外部連結

  - [關原森林遊樂區](http://www.taroko.gov.tw/TourismInformation/2_4_1_29/Page02.aspx)

[Category:花蓮縣地名](../Category/花蓮縣地名.md "wikilink")
[Category:花蓮縣旅遊景點](../Category/花蓮縣旅遊景點.md "wikilink")
[Category:太魯閣國家公園](../Category/太魯閣國家公園.md "wikilink")
[Category:秀林鄉](../Category/秀林鄉.md "wikilink")

1.