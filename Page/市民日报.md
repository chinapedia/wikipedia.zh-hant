**市民日報**（）是一份在[澳門出版的日报](../Page/澳門.md "wikilink")，於1944年8月15日由何曼公創刊兼任[社長](../Page/社長.md "wikilink")，而[總編輯由陳霞子出任](../Page/總編輯.md "wikilink")，2002年開設網站，現每份售價[澳門幣](../Page/澳門幣.md "wikilink")2元。日報創辦初期以[小說為主](../Page/小說.md "wikilink")，數月後加添國際與本地新聞，及後再增添體育新聞。1992年起採用電腦[雷射](../Page/雷射.md "wikilink")[排版](../Page/排版.md "wikilink")\[1\]。該報在2003年被[澳門博彩股份有限公司收購後](../Page/澳門博彩股份有限公司.md "wikilink")，印刷量與質素亦因此得以提升，但刊登之新聞、文章、廣告與立場也因而偏向該公司。2010年5月29日，該報網站被懷疑是[土耳其人的](../Page/土耳其.md "wikilink")[駭客入侵導致無法正常運作](../Page/駭客.md "wikilink")，[中央社指這是澳門媒體網站首次遭可能具有政治動機的黑客入侵](../Page/中央社.md "wikilink")。\[2\]

## 參考資料

  - [譚志強](../Page/譚志強.md "wikilink")，《中國澳門特區博彩業與社會發展》第六章，香港城市大學出版社，2010年。

## 外部連結

  - [市民日報](http://www.shimindaily.net/v1/)
  - [市民日報Facebook](https://www.facebook.com/市民日報-795087187273395/)

{{ Macau newspaper }}

[Category:中文报纸](../Category/中文报纸.md "wikilink")

1.
2.