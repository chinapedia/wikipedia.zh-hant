**Keroro**（）是[吉崎觀音的漫畫作品](../Page/吉崎觀音.md "wikilink")《[Keroro軍曹](../Page/Keroro軍曹.md "wikilink")》中登場的外星人主角，[動畫版的](../Page/Keroro軍曹_\(電視動畫\).md "wikilink")[配音員為](../Page/聲優.md "wikilink")[渡邊久美子](../Page/渡邊久美子.md "wikilink")，[台灣為](../Page/台灣.md "wikilink")[雷碧文](../Page/雷碧文.md "wikilink")，[香港為](../Page/香港.md "wikilink")[陸惠玲](../Page/陸惠玲.md "wikilink")。

## 人物簡介

  - Keroro為的隊長，K隆軍的[軍銜為](../Page/日本軍銜.md "wikilink")[軍曹](../Page/軍曹.md "wikilink")，是「伽瑪星雲第58號行星·宇宙侵略軍特殊先遣部隊隊長」。
  - 地球的[血型為O型](../Page/血型.md "wikilink")。身體脂肪率為30%（參考自:動畫版第154集）。
  - 其生日日期換算為地球曆為**12月9日。**（參考自:動畫版第128集）
  - 至於年齡方面並沒有詳細透露，但由於Keroro在[安哥爾·摩亞童年時已經認識了她](../Page/安哥爾·摩亞.md "wikilink")（摩亞現時的地球年齡為2000歲），可以推斷出Keroro的地球年齡大於2000歲（擔當Keroro的[聲優](../Page/聲優.md "wikilink")[渡邊久美子曾說到自己以](../Page/渡邊久美子.md "wikilink")45歲左右的感覺去聲演這個角色）。然而這是具爭議性的，因在漫畫單行本第7冊第56集中童年時代的Keroro曾玩過[Game
    & Watch](../Page/Game_&_Watch.md "wikilink")\[1\]。
  - 其年齡目前大於10500歲(見漫畫第230話)。
  - 雖然是小隊隊長，可是領導能力卻不高，隊長姿態不明顯。計劃侵略作戰時，失敗居多，並多數以自己利益為優先，不過在朋友有事，也會間中突顯不錯的隊長風範。
  - 最怕夏美，如果Keroro偷偷出外不做家務，夏美便會用酷刑對待他。
  - 因為每天都無無閒閒，所以Keroro爸爸曾要求Keroro與夏美相親，結果失敗。
  - 目前寄住在日向家，用Kero球在地下室弄了個侵略用的秘密基地，但基地的最大功用是放置他最喜愛的[鋼普拉](../Page/鋼普拉.md "wikilink")（[GUNDAM相關模型](../Page/機動戰士鋼彈.md "wikilink")）。
  - 對地球的流行事物很了解，例如知曉[Q太郎和](../Page/Q太郎.md "wikilink")[宇多田光](../Page/宇多田光.md "wikilink")。
  - 個人電腦使用[蘋果公司的](../Page/蘋果公司.md "wikilink")（初期為[iMac
    G3](../Page/iMac_G3.md "wikilink")），漫畫初期時也在架設自己的個人網站。
  - 擅長於做家事和組裝鋼普拉，
  - 個性頗懶散，而且也貪財。
  - 在濕度提升至超越K隆星的濕度時，會變成無敵狀態（動畫原創：並可發出類似「Tamama衝擊波」的「KinKin
    K隆波」）。但因為不喜歡塗上防濕液，所以常因此轉變為暴走狀態。（動畫原創：下場是被Nyororo吸乾水分，也因此十分害怕Nyororo）\[2\]
  - 非常有搞笑天份，看到香蕉皮時會忍不住去踩，一踩下去大家就會陷入爆笑漩渦。
  - 常因時、地、物需要，或是純粹為了製造趣味而穿上不同的「藍星人Suit」（見單行本第15集第123話第106頁）。
  - 擁有類似Dororo所有的「心靈創傷開關」，接上後會對自己的錯失作剖白和道歉。
  - 說話時習慣在句尾加上「是也」。
  - 伙伴為[日向冬樹](../Page/日向冬樹.md "wikilink")，所持有的多功能「Kero球」\[3\]目前並由他長期保管。（不過有時「Kero球」會返回Keroro手上）
  - 名稱乃取至[青蛙鳴叫聲](../Page/青蛙.md "wikilink")\[4\]的[擬聲詞](../Page/擬聲詞.md "wikilink")「Kero」。
  - 動畫中，肚皮上的K隆之☆差點被Syurara軍團奪取。（漫畫版Garuru小隊有意奪取K隆之☆，但失敗；動畫版Garuru小隊沒有此意）。那顆星彷彿是用「貼」的貼上去，必要時還會用膠帶固定（見單行本第15集第120話第49頁）。
  - 肚皮上的K隆之☆除了代表隊長身分外，在特殊情況時會不自覺啟動K隆軍的最高完全權限「超☆隊長命令」（目前僅出現於單行本第17集第140話、單行本第20集的劇場版長篇、動畫版第336集）。此命令發動會使持有者發出強大的威懾力，任何人皆無法違抗，但通常都是在無意識下啟動。
  - K隆之☆除了代表上述的隊長身分外，另內建了對地球[（舊）藍星人用的強化型態](../Page/恐龍.md "wikilink")，曾數次在無意識下變成巨龍。
  - 有著K隆軍中非常罕有的**隊長資質**，故被選上擔任K隆星先遣部隊隊長。

## Keroro軍曹稱呼一覽

<table>
<thead>
<tr class="header">
<th><p>名字</p></th>
<th><p>稱Keroro為</p></th>
<th><p>被Keroro稱為</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Giroro.md" title="wikilink">Giroro</a></p></td>
<td><p>常用：Keroro<br />
<a href="../Page/Cosplay.md" title="wikilink">Cosplay造型</a>、變身：藍天Kero子</p></td>
<td><p>常用：Giroro（）／紅色不倒翁／紅色的／Giroro老弟／赤達摩／Giroro伍長<br />
分身後：被惡搞專用小咖<br />
Cosplay造型、變身：厄魯莫團的伊郎</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Tamama.md" title="wikilink">Tamama</a></p></td>
<td><p>軍曹哥（<a href="../Page/有線電視.md" title="wikilink">有線電視</a>、<a href="../Page/台灣.md" title="wikilink">台灣版本</a>：軍曹大哥）／軍曹大人（<a href="../Page/超劇場版_Keroro軍曹_4_逆襲的龍勇士.md" title="wikilink">劇場版4</a>）／（很少）</p></td>
<td><p>常用：Tamama（二等兵）（）／小玉<br />
Cosplay造型、變身：南極Tama江</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Kururu.md" title="wikilink">Kururu</a></p></td>
<td><p>隊長／青BB（<a href="../Page/無綫電視.md" title="wikilink">無綫電視</a>）</p></td>
<td><p>常用：Kururu（曹長）／博士（）／黃色野／黃色四眼仔（無綫電視、）<br />
Cosplay造型、變身：Kururu子</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Dororo.md" title="wikilink">Dororo</a></p></td>
<td><p>隊長大人／Keroro（）</p></td>
<td><p>Dororo（兵長）／泥沼君（）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日向冬樹.md" title="wikilink">日向冬樹</a></p></td>
<td><p>軍曹</p></td>
<td><p>冬樹大人（）／有線電視：冬樹大人、冬樹少爺</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日向夏美.md" title="wikilink">日向夏美</a></p></td>
<td><p>傻瓜青蛙／笨青蛙／Keroro／軍曹(少)</p></td>
<td><p>夏美大人／有線電視：夏美小姐</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>小Kero／</p></td>
<td><p>媽媽大人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日向秋奈.md" title="wikilink">日向秋奈</a></p></td>
<td><p>小Kero</p></td>
<td><p>提督大人／外婆大人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安哥爾·摩亞.md" title="wikilink">安哥爾·摩亞</a></p></td>
<td><p>叔叔／軍曹叔叔（無綫電視）</p></td>
<td><p>常用：摩亞大人<br />
Cosplay造型、變身：海原摩亞</p></td>
</tr>
<tr class="even">
<td><p>623（<a href="../Page/北城睦實.md" title="wikilink">北城睦實</a>）</p></td>
<td><p>漫畫：Keroro／動畫：軍曹</p></td>
<td><p>漫畫：623大人／動畫：三郎大人（無線電視、台灣版本：睦實大人）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東谷小雪.md" title="wikilink">東谷小雪</a></p></td>
<td><p>Keroro（無線電視）／無線電視、</p></td>
<td><p>小雪大人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西澤桃華.md" title="wikilink">西澤桃華</a></p></td>
<td><p>軍曹（無線電視）／Keroro（）／有線電視：小Kero、軍曹、Keroro</p></td>
<td><p>桃華大人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Keroro軍曹角色列表#圍繞Keroro小隊的人們.md" title="wikilink">波爾·森山</a></p></td>
<td><p>Keroro（無線電視）／</p></td>
<td><p>波爾大人</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Keroro</p></td>
<td><p>556</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拉比.md" title="wikilink">拉比</a></p></td>
<td><p>哥哥的朋友</p></td>
<td><p>拉比大人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Garuru.md" title="wikilink">Garuru</a></p></td>
<td><p>常用：Keroro軍曹／<br />
洗腦後：Keroro大尉</p></td>
<td><p>Garuru中尉</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Pururu.md" title="wikilink">Pururu</a></p></td>
<td><p>Keroro／Keroro軍曹／</p></td>
<td><p>Pururu／Pururu護士長／</p></td>
</tr>
<tr class="even">
<td><p>新Keroro</p></td>
<td><p>叔叔</p></td>
<td><p>年輕人</p></td>
</tr>
</tbody>
</table>

## [Cosplay造型](../Page/Cosplay.md "wikilink")、變身

  -
  - Keroro軍曹重裝型（）

  -
  - 宇宙淳二（）

  -
  - 偽月影千草（）

  - 詹姆斯·鐵鉤船長（）

  - 偽[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")（）

  - 反派角色模式（）

  - 藍天Kero子（）

  - Keroro帝王Z（）

  - [賣火柴的小女孩](../Page/賣火柴的小女孩.md "wikilink")？（）

  - 偽[Nemo](../Page/冒險少女娜汀亞#Nemo.md "wikilink")（）

  - Kero夫人（）

  - [金田一耕助](../Page/金田一耕助.md "wikilink")（）

  - Keroro[將軍](../Page/將軍.md "wikilink")（）

  -
  - 首領（）

  - Kero子（）

  - 超能力者（）

  -
  - 黑[兔](../Page/兔.md "wikilink")（）

  - 忠臣藏（）

  -
  - 將軍[風箏](../Page/風箏.md "wikilink")（）

  -
  - Keroro犬（）

  -
  -
  - 未來椰子丸（）

  - 複製Keroro／Keroro大尉（）

  -
  - [博士](../Page/博士.md "wikilink")（）

  - Keroro保健先生（）

  - Keroro大元帥（）

  -
  - 廚師（）

  - Keroro[蟬](../Page/蟬.md "wikilink")（）

  - Kero吉（）

  - \[5\]

  - 蚊男Keroro（）

  - [德古拉](../Page/德古拉.md "wikilink")（）

  - [稻穗](../Page/稻.md "wikilink")→[米粒](../Page/米.md "wikilink")（）

  - 交涉人 Keroro軍曹（）

  - 臼（）

  -
  -
  - 赫伯特·馮·Kero揚（）

  - [桃面具](../Page/桃.md "wikilink")（）

  -
  - [戇豆先生](../Page/戇豆先生.md "wikilink")（）

  -
  - Keroro男商會（）

  - Keroro總統（，配音：）

  - 巨大化Keroro（）\[6\]\[7\]\[8\]

## Keroro的個人資料

  - 生日：12月9日（藍星曆）
  - 夥伴：[日向冬樹](../Page/日向冬樹.md "wikilink")
  - 漫畫版首次登場：第一冊第1話
  - 動畫版首次登場集數：第一集
  - 武器：Kero球／Kero ball（冬樹保管中）
  - 絕招：金金K隆波和談·合·板SA
  - 體色：綠
  - 標誌：黃色的星形（K隆之星）
  - 官階：軍曹，亦為的隊長
  - 喜歡的食物：楊桃（Star fruit）
  - 小時候喜歡的人：[Pururu護士長](../Page/Pururu.md "wikilink")
    (但漫畫版的Keroro表示對Pururu護士長只有青梅竹馬之情)

## Keroro的別個體

  - [Keroro大尉](../Page/Keroro大尉.md "wikilink")
  - [黑暗Keroro](../Page/黑暗Keroro.md "wikilink")
  - Keroro的分身

:\*[印度的Keroro](../Page/印度.md "wikilink")

:\*[美國的Keroro](../Page/美國.md "wikilink")

:\*[中國的Keroro](../Page/中國.md "wikilink")

:\*[智利的Keroro](../Page/智利.md "wikilink")

:\*[瑞士的Keroro](../Page/瑞士.md "wikilink")

:\*[埃及的Keroro](../Page/埃及.md "wikilink")

## 註釋

## 參見

  - [Keroro軍曹人物列表](../Page/Keroro軍曹人物列表.md "wikilink")
  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")
  - [日向冬樹](../Page/日向冬樹.md "wikilink")
  - [安哥爾·摩亞](../Page/安哥爾·摩亞.md "wikilink")

[分類:虛構外星生命](../Page/分類:虛構外星生命.md "wikilink")

[Category:Keroro軍曹登場人物](../Category/Keroro軍曹登場人物.md "wikilink")
[Category:虛構男性動漫角色](../Category/虛構男性動漫角色.md "wikilink")
[Category:虚构青蛙与蟾蜍](../Category/虚构青蛙与蟾蜍.md "wikilink")
[Category:虚构军人](../Category/虚构军人.md "wikilink")

1.  單行本第7冊第56集：《第五人報到！Dororo傳…之卷》，第51頁
2.  Nyororo跟毒蛇一樣是K隆星人的天敵。在漫畫中並沒有設定Nyororo、金K隆波
3.  K隆星先遣部隊隊長配給的武器，有足以摧毀掉藍星的力量。
4.  在日本青蛙鳴叫聲為Gero。
5.  被Dororo打敗，結果遭到宇宙女警察[玻央](../Page/Keroro軍曹角色列表#其他宇宙人.md "wikilink")（，）的殘酷迫害，這是玻央唯一一次鎮壓Keroro
6.  在動畫版第334集Keroro因為違反了由[玻唷](../Page/Keroro军曹角色列表#其他宇宙人.md "wikilink")（，）制定的《宇宙侵略法第78條-巨大化之項》，內容就是「在沒有特別指定的區域內實行巨大化期間，體型不能超過原來的30倍」而被鎮壓，這是玻唷第六次兼最後一次迫害Keroro
7.  此次迫害被東正教認為是「世界末日」
8.  在動畫版第60集、第145集、第148集、第224集和第283集之後出現，但及時被突然激怒的夏美制止了