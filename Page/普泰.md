**普泰**（531年二月—532年四月，一作**普嘉**）是[北魏時期节闵帝](../Page/北魏.md "wikilink")[元恭的](../Page/元恭.md "wikilink")[年号](../Page/年号.md "wikilink")，歷時1年零二个月。

《[历代建元考](../Page/历代建元考.md "wikilink")》作普嘉，注为：“诸书皆作普嘉。”

## 大事记

## 出生

## 逝世

## 纪年

| 普泰                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 531年                           | 532年                           |
| [干支](../Page/干支纪年.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [中大通](../Page/中大通.md "wikilink")（529年十月－534年十二月）：[南朝梁政權梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [神嘉](../Page/神嘉.md "wikilink")（525年十二月－535年三月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉蠡升年号](../Page/劉蠡升.md "wikilink")
      - [更興或](../Page/更興.md "wikilink")[更新](../Page/更兴.md "wikilink")（530年六月－532年十二月）：[北魏政权](../Page/北魏.md "wikilink")[汝南王](../Page/汝南.md "wikilink")[元悅年号](../Page/元悅.md "wikilink")
      - [章和](../Page/章和_\(麴坚\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴坚年号](../Page/麴坚.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:530年代中国政治](../Category/530年代中国政治.md "wikilink")
[Category:531年](../Category/531年.md "wikilink")
[Category:532年](../Category/532年.md "wikilink")