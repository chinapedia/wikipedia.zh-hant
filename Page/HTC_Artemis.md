**HTC Artemis**，原廠型號**HTC P3300，HTC
P3301**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
5，GPS手機，擁有RollR軌跡球滑鼠與360度滾輪設計，方便單手操控。2006年10月於歐洲首度發表。已知客製版本HTC
P3300，HTC P3301，Qtek G200，Dopod P800，Dopod P800W，Orange SPV
M650，T-Mobile MDA Compact III，O2 Xda Orbit。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP 850 201MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：108 毫米 X 58 毫米 X 16.8 毫米
  - 重量：130g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/EDGE
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1250mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC P3300
    概觀](https://web.archive.org/web/20110716060826/http://www.htc.com/www/product.aspx?id=392)
  - [HTC P3300 技術規格](http://www.htc.com/www/product.aspx?id=466)
  - [HTC P3301 概觀](http://www.htc.com/br/product.aspx?id=41460)
  - [HTC P3301 技術規格](http://www.htc.com/br/product.aspx?id=41464)

[H](../Category/智能手機.md "wikilink")
[Artemis](../Category/宏達電手機.md "wikilink")