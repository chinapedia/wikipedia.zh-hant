**希臘共產黨**（[希臘語](../Page/希臘語.md "wikilink")：），簡稱**希共**（），是[希臘的一个](../Page/希臘.md "wikilink")[共产主义政党](../Page/共产主义.md "wikilink")，也是希臘現存最老的政黨
（成立於1918年11月17日）。

该党初名**希臘社會主義工人黨**，1924年改名为希腊共产党，并加入[共产国际](../Page/共产国际.md "wikilink")。1936年，该党被希腊当局宣布为非法。[二战开始後](../Page/二战.md "wikilink")，该党组织[民族解放阵线和](../Page/民族解放阵线_\(希腊\).md "wikilink")[希腊人民解放军](../Page/希腊人民解放军.md "wikilink")，成为[希腊反法西斯抵抗运动的主要力量](../Page/希腊抵抗运动.md "wikilink")。二战后，该党與希臘政府爆發[武裝衝突](../Page/希臘內戰.md "wikilink")。1949年，该党领导的[希腊民主军在希腊内战中失败后](../Page/希腊民主军.md "wikilink")，党的中央领导机构及部分党员流亡国外，直至1974年希腊民主化。

该党近年来在希腊政坛及[国际共产主义运动中极其活跃](../Page/国际共产主义运动.md "wikilink")，多次主办[共产党和工人党国际会议](../Page/共产党和工人党国际会议.md "wikilink")，并发起成立[共产党和工人党倡议](../Page/共产党和工人党倡议.md "wikilink")。目前，该党在希腊第三大城市[帕特雷执政](../Page/帕特雷.md "wikilink")。

## 选举表现

| **自从1926年以来的选举结果</small>**                                                  |
| --------------------------------------------------------------------------- |
| 年份                                                                          |
| [1926](../Page/Greek_legislative_election,_1926.md "wikilink")              |
| [1928](../Page/Greek_legislative_election,_1928.md "wikilink")              |
| [1929](../Page/Greek_Senate_election,_1929.md "wikilink")                   |
| [1932](../Page/Greek_legislative_election,_1932.md "wikilink")              |
| [1932](../Page/Greek_Senate_election,_1932.md "wikilink")                   |
| [1933](../Page/Greek_legislative_election,_1933.md "wikilink")              |
| [1935](../Page/Greek_legislative_election,_1935.md "wikilink")              |
| [1936](../Page/Greek_legislative_election,_1936.md "wikilink")              |
| [1951](../Page/Greek_legislative_election,_1951.md "wikilink")              |
| [1952](../Page/Greek_legislative_election,_1952.md "wikilink")              |
| [1958](../Page/Greek_legislative_election,_1958.md "wikilink")              |
| [1974](../Page/Greek_legislative_election,_1974.md "wikilink")              |
| [1977](../Page/Greek_legislative_election,_1977.md "wikilink")              |
| [1981](../Page/Greek_legislative_election,_1981.md "wikilink")              |
| [1981](../Page/European_Parliament_election,_1981_\(Greece\).md "wikilink") |
| [1984](../Page/European_Parliament_election,_1984_\(Greece\).md "wikilink") |
| [1985](../Page/Greek_legislative_election,_1985.md "wikilink")              |
| [1989](../Page/European_Parliament_election,_1989_\(Greece\).md "wikilink") |
| [1989.06](../Page/Greek_legislative_election,_June_1989.md "wikilink")      |
| [1989.11](../Page/Greek_legislative_election,_November_1989.md "wikilink")  |
| [1990](../Page/1990年希腊议会选举.md "wikilink")                                   |
| [1993](../Page/1993年希腊议会选举.md "wikilink")                                   |
| [1994](../Page/1994年欧洲议会选举.md "wikilink")                                   |
| [1996](../Page/1996年希腊议会选举.md "wikilink")                                   |
| [1999](../Page/1999年欧洲议会选举.md "wikilink")                                   |
| [2000](../Page/2000年希腊议会选举.md "wikilink")                                   |
| [2004](../Page/2004年希腊立法选举.md "wikilink")                                   |
| [2004](../Page/2004年欧洲议会选举.md "wikilink")                                   |
| [2007](../Page/2007年希腊议会选举.md "wikilink")                                   |
| [2009](../Page/2009年欧洲议会选举.md "wikilink")                                   |
| [2009](../Page/2009年希腊议会选举.md "wikilink")                                   |
| [2012.05](../Page/2012年5月希腊议会选举.md "wikilink")                              |
| [2012.06](../Page/2012年6月希腊大选.md "wikilink")                                |
| [2015.01](../Page/2015年1月希腊立法选举.md "wikilink")                              |
| [2015.09](../Page/2015年9月希腊立法选举.md "wikilink")                              |

  -
    **注释:**
    <sup>‡</sup>与其它政党合作。
    <sup>↔</sup>作为联合阵线的一部分参选。
    <sup>\*</sup>以[统一民主左翼的名义参选](../Page/统一民主左翼.md "wikilink")。
    <sup>†</sup>作为[联合左翼的一部分参选](../Page/联合左翼_\(希腊\).md "wikilink")。
    <sup>††</sup>作为[左翼和进步联盟的一部分参选](../Page/左翼和进步联盟.md "wikilink")。

## 参见

  - [向导节](../Page/向导节.md "wikilink")
  - [流浪艺人](../Page/流浪艺人.md "wikilink")

[Category:希腊共产党](../Category/希腊共产党.md "wikilink")
[Category:歐洲懷疑主義政黨](../Category/歐洲懷疑主義政黨.md "wikilink")
[Category:希臘抵抗運動](../Category/希臘抵抗運動.md "wikilink")
[Category:希腊内战](../Category/希腊内战.md "wikilink")