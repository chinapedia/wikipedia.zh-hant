**Amigaguide**是为[Amiga設計的超文本格式](../Page/Amiga.md "wikilink")。文件储存在[ASCII编码](../Page/ASCII.md "wikilink")，因此文件不需要专用软件就可以读取和编辑。

自1992年的開始，Amigaguide viewer已預載於各位Amiga系列上\[1\]。至於旧版本使用者則可以下载**AmigaGuide
34**來配合使用。

虽然Amigaguide格式是几乎只用于編寫Amiga程序，不過其他平台也可使用：

  - [Java](../Page/Java.md "wikilink") -
    [JAGUaR](https://web.archive.org/web/20070312040433/http://home1.swipnet.se/~w-10967/Projex/Jaguar/)
  - [DOS](../Page/DOS.md "wikilink") -
    [AGView](ftp://k332.feld.cvut.cz/pub/local/lemming/AGView/AGview.arj)
  - [Windows](../Page/Microsoft_Windows.md "wikilink") - [WinGuide (LHA
    archive)](http://main.aminet.net/misc/emu/winguide.lha) [WinGuide
    (ZIP
    archive)](http://www.safalra.com/hypertext/amigaguide/winguide.html)
  - [Linux](../Page/Linux.md "wikilink") -
    [AGReader](http://main.aminet.net/misc/unix/AGReader.tgz)

## 參考資料

## 外部链接

  - [Amigaguide (at Safalra's
    Website)](http://www.safalra.com/hypertext/amigaguide/)
  - [Amigaguide
    manual](http://www.lysator.liu.se/amiga/code/guide/amigaguide.guide)

[分類:文件格式](../Page/分類:文件格式.md "wikilink")

[Category:超文字](../Category/超文字.md "wikilink")

1.