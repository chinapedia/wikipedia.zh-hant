**人帽蘭**（[学名](../Page/学名.md "wikilink")：**）是[蘭科下的一種](../Page/蘭科.md "wikilink")，有時被分類在近親的[紅門蘭屬中](../Page/紅門蘭屬.md "wikilink")。它取名人帽蘭是因其[花像人](../Page/花.md "wikilink")：[花瓣及](../Page/花瓣.md "wikilink")[花萼像人頭](../Page/花萼.md "wikilink")，而[唇瓣則像人的四肢](../Page/唇瓣.md "wikilink")。

## 描述

人帽蘭是[多年生植物](../Page/多年生植物.md "wikilink")，可生長至20-40厘米高。從6厘米[直徑的](../Page/直徑.md "wikilink")[塊莖長出](../Page/塊莖.md "wikilink")5厘米長的尖葉．組成了基生蓮座葉叢。中央花柱會於4月至6月間長出，當中會有達50朵小花。花朵呈綠色，[唇瓣呈黃綠色或有紫色斑汶](../Page/唇瓣.md "wikilink")。

[Aceras_anthropophorum_leaves.jpg](https://zh.wikipedia.org/wiki/File:Aceras_anthropophorum_leaves.jpg "fig:Aceras_anthropophorum_leaves.jpg")
[Aceras_anthropophora_(detail).jpg](https://zh.wikipedia.org/wiki/File:Aceras_anthropophora_\(detail\).jpg "fig:Aceras_anthropophora_(detail).jpg")

## 種植地

人帽蘭喜歡溫暖及有陽光的草地，較為乾旱的[鈣質](../Page/鈣質.md "wikilink")[土壤](../Page/土壤.md "wikilink")。它們分佈在[地中海地區](../Page/地中海.md "wikilink")、[歐洲中部及西部](../Page/歐洲.md "wikilink")，最北達至[英格蘭南部](../Page/英格蘭.md "wikilink")。在[高山氣候非高](../Page/高山氣候.md "wikilink")[海拔的地區亦會發現它們](../Page/海拔.md "wikilink")。

## 異名

  - *Aceras anthropomorpha* <span style="font-size:smaller;">(Pers.)
    Steud. 1840</span>
  - *Aceras anthropophorum* <span style="font-size:smaller;">(L.) Sm.
    1818</span>
  - *Arachnites anthropophora* <span style="font-size:smaller;">F.W.
    Schmidt 1793</span>
  - *Loroglossum anthropophorum* <span style="font-size:smaller;">(L.)
    Rich. 1818</span>
  - *Loroglossum brachyglotte* <span style="font-size:smaller;">Rich.
    1818</span>
  - *Ophrys anthropophora* <span style="font-size:smaller;">L.
    1753</span>
  - *Orchis anthropophora* <span style="font-size:smaller;">All.
    1785</span>
  - *Satyrium anthropomorpha* <span style="font-size:smaller;">Pers.
    1807</span>
  - *Satyrium anthropophora* <span style="font-size:smaller;">Pers.
    1807</span>
  - *Serapias anthropophora* <span style="font-size:smaller;">(L.) J.
    Jundz. 1791</span>

[Category:人唇蘭屬](../Category/人唇蘭屬.md "wikilink")