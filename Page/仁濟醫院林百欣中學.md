**仁濟醫院林百欣中學**（，簡稱YCHLPYSS），為[仁濟醫院創辦的第一所中學](../Page/仁濟醫院_\(香港\).md "wikilink")，位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[荃景圍](../Page/荃景圍.md "wikilink")，由[林百欣先生慨捐](../Page/林百欣.md "wikilink")100萬元作開辦費，創立於1982年。校內中一至中六級各設4班。

## 校本管理

## 抱負

貫徹「尊仁濟世」精神，提供優質全人教育，培育學生成為國家棟樑。

## 使命

秉承「尊仁濟世」的辦學精神，為學生提供優質的全人教育，讓學生能積極學習、自律守規、盡責明分、明辨是非、尊重別人。

## 歷任校長

1.  梁明淳先生　Mr. Leung Ming Shun（1982-1987）
2.  黃葉慧瑩女士　Mrs. Diana Wong（1987-1990）
3.  周徐立修女士　Mrs. Lisu Zee Chow（1990-2000）
4.  鄭偉光先生　Mr. Cheng Wai Kwong（2000-2005）
5.  譚淑賢女士　Ms. Tam Shuk Yin Anna（2005-2006）
6.  陳惠瑩女士　Ms.Chan Wai Ying（2006-2008）
7.  曹達明先生　Mr. Tso Tat Ming（2008至今）

## 屬校制服團體

  - [香港童軍荃灣第](../Page/香港童軍.md "wikilink")24旅\[1\]
  - [香港女童軍新界第](../Page/香港女童軍總會.md "wikilink")28隊
  - [香港紅十字青年團第](../Page/香港紅十字青年團.md "wikilink")252團

## 著名校友

  - [蔣家旻](../Page/蔣家旻.md "wikilink")：[香港女](../Page/香港.md "wikilink")[藝人](../Page/藝人.md "wikilink")，現為[無綫電視經理人合約女藝員](../Page/無綫電視.md "wikilink")。
  - [鄧鈞耀](../Page/鄧鈞耀.md "wikilink")（Dawn）：香港男歌手，[Faith前成員](../Page/Faith_\(組合\).md "wikilink")。
  - [唐　寧](../Page/唐寧_\(香港\).md "wikilink")：藝人、[香港著名女演員](../Page/香港.md "wikilink")，本名江麗娜，[童星出身](../Page/童星.md "wikilink")，現為[香港電視旗下合約女藝員](../Page/香港電視.md "wikilink")。
  - [湯道生](../Page/湯道生.md "wikilink")：[香港](../Page/香港.md "wikilink")[騰訊高級副總裁](../Page/騰訊.md "wikilink")，[香港中學會考六優](../Page/香港中學會考.md "wikilink")，[斯坦福大學](../Page/斯坦福大學.md "wikilink")
    ([Stanford
    University](../Page/Stanford_University.md "wikilink"))獲得[電子工程](../Page/電子工程.md "wikilink")[碩士學位](../Page/碩士學位.md "wikilink")。

## 軼聞

  - 2004年12月，一名學生參加學校舉辦的「西安五天遊學團」，隨團返回深圳後擅自離團失蹤，在廣州流浪二十多日後始因花盡身上金錢與廣州的叔父聯絡輾轉接回香港。該名學生聲稱因不滿父母管教過嚴及不堪功課壓力，繼而出走尋找獨立自主生活。\[2\]\[3\]

<!-- end list -->

  - 2013年5月3日，林百欣中學聯同其他仁濟醫院屬下中學、[何傳耀中學](../Page/荃灣公立何傳耀紀念中學.md "wikilink")、[柴灣角天主教小學等假荃灣](../Page/柴灣角天主教小學.md "wikilink")[城門谷運動場](../Page/城門谷運動場.md "wikilink")，舉辦「共建三十．同創世紀」齊破健力士世界紀錄活動，並成功刷新「最多人同時進行[數多酷](../Page/數獨.md "wikilink")」之世界紀錄，人數突破三千人。\[4\]

## 圖像

Yan Chai Hospital Lim Por Yen Secondary School.JPG|入口翻新前的校舍 Yan Chai
Hospital Lim Por Yen Secondary School under partial renovation in
January 2018.jpg|入口翻新中的校舍

## 參考資料

## 外部連結

  - [香港教育城學校概覽](http://www.chsc.hk/ssp/sch_detail1.php?lang_id=2&sch_id=254)

[Category:仁濟醫院學校](../Category/仁濟醫院學校.md "wikilink")
[Category:柴灣角](../Category/柴灣角.md "wikilink")
[Category:荃灣區中學](../Category/荃灣區中學.md "wikilink")
[Category:1982年創建的教育機構](../Category/1982年創建的教育機構.md "wikilink")

1.
2.
3.
4.  [仁濟醫院林百欣中學 攜手締造
    成功刷新健力士世界紀錄](http://www.ychlpyss.edu.hk/~lpy/lpy_guinness503.htm)