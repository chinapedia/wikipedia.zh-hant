**海河**又称**沽河**，是[中国华北地区最大的河流](../Page/中国.md "wikilink")，总流域面积达31.8万[平方公里](../Page/平方公里.md "wikilink")，涵盖了[天津](../Page/天津.md "wikilink")、[北京全部](../Page/北京.md "wikilink")，[河北绝大部分](../Page/河北.md "wikilink")，以及[河南](../Page/河南.md "wikilink")、[山东](../Page/山东.md "wikilink")、[山西](../Page/山西.md "wikilink")、[内蒙等](../Page/内蒙.md "wikilink")[省](../Page/省.md "wikilink")[区](../Page/自治区.md "wikilink")，海河流域的总人口将近1.24亿。

## 名称

海河由于地势不高，每当海中涨潮时，河水倒流，落潮时河水顺流，和海水的潮汐相同，因此取名为“海”河。海河支流的[永定河原名](../Page/永定河.md "wikilink")“无定河”，就是因为河道经常改变，只是皇帝为了祈愿它不再泛滥才人为改名为永定河。

## 流域概况

海河流域各河系大致分为两种类型：一种是发源于太行山与燕山的背风坡，这种河系源远流长，山区汇水面积大，水流集中，泥沙含量相对较多。另一种是发源于太行山与燕山的迎风坡，这种河系支流分散，源短流急，洪峰高、历时短、突发性强、泥沙含量较低。这两种类型河流在华北地区呈相间分布，清浊分明。\[1\]

### 干流

海河干流全位于天津市中部，全长73公里，从子牙河与北运河交汇的天津市内金刚桥[三岔河口算起](../Page/三岔河口.md "wikilink")，到[塘沽区大沽口入海](../Page/塘沽.md "wikilink")，实际直线距离只有不到50公里。三岔河口为天津市[红桥区](../Page/红桥区.md "wikilink")、[河北区和](../Page/河北区.md "wikilink")[南开区的交点](../Page/南开区.md "wikilink")，流经和平区、河东区、东丽区直到滨海新区汇入渤海。

### 主要支流

海河包括了五大支流：[北运河](../Page/北运河.md "wikilink")、[南运河](../Page/南运河.md "wikilink")、[大清河](../Page/大清河.md "wikilink")、[子牙河和](../Page/子牙河.md "wikilink")[永定河](../Page/永定河.md "wikilink")，以及许多更小的支流，如果从最长的支流浊漳河算起，全长为1329公里。
此外海河支流还有：[潮白河](../Page/潮白河.md "wikilink")、[洋河](../Page/洋河.md "wikilink")、[桑干河](../Page/桑干河.md "wikilink")、[拒马河](../Page/拒马河.md "wikilink")、[府河](../Page/府河.md "wikilink")、[唐河](../Page/唐河.md "wikilink")、[滹沱河](../Page/滹沱河.md "wikilink")、[滏阳河](../Page/滏阳河.md "wikilink")、[卫河](../Page/卫河.md "wikilink")、[清漳河](../Page/清漳河.md "wikilink")、[浊漳河等](../Page/浊漳河.md "wikilink")。

## 水文特征

海河流域地形没有过渡带，从[太行山区直降到华北平原](../Page/太行山.md "wikilink")，这个地区雨季集中在每年的7、8月份，尤其太行山麓常发生暴雨，洪水冲刷泥沙，到平原后水流突然变缓，泥沙淤积，所以大部分河道宽浅，河道经常变迁改道，留下许多积水洼地。华北地区年雨量分布也不平衡，据史料记载，从1368年[明代立国](../Page/明代.md "wikilink")，燕王迁都[北京到](../Page/北京.md "wikilink")1949年[中华人民共和国成立](../Page/中华人民共和国.md "wikilink")，海河流域共发生387次严重水灾，407次严重旱灾。

### 历史

[Tianjin,_with_Western_Ships_in_Hai_River_and_Grand,_Colonnaded_Western_Building_on_the_River_Bank._China,_1874_WDL2109.png](https://zh.wikipedia.org/wiki/File:Tianjin,_with_Western_Ships_in_Hai_River_and_Grand,_Colonnaded_Western_Building_on_the_River_Bank._China,_1874_WDL2109.png "fig:Tianjin,_with_Western_Ships_in_Hai_River_and_Grand,_Colonnaded_Western_Building_on_the_River_Bank._China,_1874_WDL2109.png")

1929年4月21日，国民政府为疏浚海河曾发行短期公债\[2\]。
1939年水灾冲毁津浦铁路，淹没天津市，市中心和平区比较低洼地带水深几达2米，街道行舟将近两个月。

由于海水倒灌不时淹死稻田，1959年在河口修建了拦河大坝，涨潮时关闸防止海水倒流。1963年太行山麓连下7天暴雨，[衡水一带一片汪洋](../Page/衡水.md "wikilink")，为保护天津市的工业，只好在上游炸堤放水，[京广铁路和](../Page/京广铁路.md "wikilink")[京沪铁路全部停运](../Page/京沪铁路.md "wikilink")，从[上海到](../Page/上海.md "wikilink")[北京乘火车需绕行](../Page/北京.md "wikilink")[兰州](../Page/兰州.md "wikilink")。[毛泽东做出](../Page/毛泽东.md "wikilink")“一定要根治海河”的指示，于是下游开始挖入海减河，上游修建水库，此后，海河流域再也没有发生过水灾，却开始了干旱。1970年代后，由于海河上游城市发展，用水量迅速加大，建起了一系列大中小型水库，大型水库就有为北京供水的[密云水库](../Page/密云水库.md "wikilink")、[官厅水库](../Page/官厅水库.md "wikilink")、[十三陵水库](../Page/十三陵水库.md "wikilink")，为[保定供水的](../Page/保定.md "wikilink")[西大洋水库](../Page/西大洋水库.md "wikilink")、[王快水库](../Page/王快水库.md "wikilink")，为[石家庄供水的](../Page/石家庄.md "wikilink")[黄壁庄水库](../Page/黄壁庄水库.md "wikilink")、[岗南水库](../Page/岗南水库.md "wikilink")，为[邢台供水的](../Page/邢台.md "wikilink")[朱庄水库](../Page/朱庄水库.md "wikilink")，为[邯郸供水的](../Page/邯郸.md "wikilink")[岳城水库](../Page/岳城水库.md "wikilink")，为[林县供水的](../Page/林县.md "wikilink")[红旗渠等](../Page/红旗渠.md "wikilink")，海河下游河道基本处于干涸，华北平原农业用水只得开采地下水，造成地下水位下降。位于“九河下梢”的天津面临严重缺水的问题。

[天津冬季的海河冰上垂钓.jpg](https://zh.wikipedia.org/wiki/File:天津冬季的海河冰上垂钓.jpg "fig:天津冬季的海河冰上垂钓.jpg")
1981年，为供津冀用水而修建的[密云水库为保障严重缺水的北京供水](../Page/密云水库.md "wikilink")，而停止向津冀供水，直接导致津冀两地用水困难\[3\]。1981年，为解决干流城市天津的用水，国务院决定兴修[引滦入津工程](../Page/引滦入津工程.md "wikilink")，引华北平原第二大河[滦河的水到海河下游的天津](../Page/滦河.md "wikilink")，1983年竣工，每年引水近10亿立方米，解决了天津缺水问题，为此天津市在三岔河口树立了[引滦入津纪念碑](../Page/引滦入津纪念碑.md "wikilink")，并对上游地区进行长期经济援助。但从此海河口闸永闭，海河干流实际成为一个大水库。海河和滦河河口基本不再向[渤海排放淡水](../Page/渤海.md "wikilink")，造成生态系统的变化，受影响最大的是[洄游性鱼类](../Page/洄游.md "wikilink")。原来海河盛产优良水产，三叉河口的银鱼品质最好，和[太湖的不同](../Page/太湖.md "wikilink")，是“金眼”银鱼，天津郊区[胜芳](../Page/胜芳镇.md "wikilink")（现属河北）的螃蟹可以和[江苏阳澄湖的大闸蟹媲美](../Page/江苏.md "wikilink")，现在这些品种都已经绝迹。

[炫彩津门6Tianjin_Jinwan_Plaza.jpg](https://zh.wikipedia.org/wiki/File:炫彩津门6Tianjin_Jinwan_Plaza.jpg "fig:炫彩津门6Tianjin_Jinwan_Plaza.jpg")

## 流域物产

[明代时](../Page/明代.md "wikilink")[徐光启将水](../Page/徐光启.md "wikilink")[稻引种到中国北方](../Page/稻.md "wikilink")，首先在海河下游落脚，由于北方水稻生长期长，稻米品质优良，闻名中外的“小站稻”就产于海河下游。但是海水倒灌不时淹死稻田，1959年在河口修建了拦河大坝，涨潮时关闸防止海水倒流。

## 海河干流上的桥梁

  - [永乐桥](../Page/永乐桥.md "wikilink")[天津之眼](../Page/天津之眼.md "wikilink")
  - [金钢桥](../Page/金钢桥.md "wikilink")
  - [狮子林桥](../Page/狮子林桥.md "wikilink")
  - [金汤桥](../Page/金汤桥.md "wikilink")
  - [进步桥](../Page/进步桥.md "wikilink")
  - [北安桥](../Page/北安桥.md "wikilink")
  - [大沽桥](../Page/大沽桥.md "wikilink")
  - [解放桥](../Page/解放桥.md "wikilink")
  - [赤峰桥](../Page/赤峰桥.md "wikilink")
  - [金汇桥](../Page/金汇桥.md "wikilink")（[保定桥](../Page/保定桥.md "wikilink")）
  - [大光明桥](../Page/大光明桥.md "wikilink")
  - [金阜桥](../Page/金阜桥.md "wikilink")（蚌埠桥）
  - [直沽桥](../Page/直沽桥.md "wikilink")（奉化桥）
  - [刘庄桥](../Page/刘庄桥.md "wikilink")
  - [光华桥](../Page/光华桥.md "wikilink")
  - [国泰桥](../Page/国泰桥.md "wikilink")
  - [富民桥](../Page/富民桥.md "wikilink")
  - [海津大桥](../Page/海津大桥.md "wikilink")
  - [柳林桥](../Page/柳林桥.md "wikilink")
  - [吉兆桥](../Page/吉兆桥.md "wikilink")

## 旅游

## 参考文献

## 外部链接

  - [水信息网](https://web.archive.org/web/20110414182057/http://www.hwcc.gov.cn/pub/hwcc/index.html)
    - [水利部海河水利委员会官方网站](../Page/水利部海河水利委员会.md "wikilink")

## 参见

  - [水利部海河水利委员会](../Page/水利部海河水利委员会.md "wikilink")

{{-}}

[Category:中国河流](../Category/中国河流.md "wikilink")
[海河水系](../Category/海河水系.md "wikilink")

1.
2.  《疏濬河北省海河工程短期公債條例》
3.  [密云水库今天五十岁](http://www.bjqx.org.cn/qxweb/n24112c215.aspx)，北青网，2010年9月23日查阅