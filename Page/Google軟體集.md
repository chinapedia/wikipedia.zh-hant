**Google軟體集**（Google
Pack）是[Google公司於](../Page/Google公司.md "wikilink")2006年[國際消費電子展](../Page/國際消費電子展.md "wikilink")（CES）所推出的服務，由該公司設立推薦軟件的下載平台，所提供的軟件均為對用戶免費授權使用的產品，
僅提供可在[Windows XP](../Page/Windows_XP.md "wikilink")、[Windows
Vista及](../Page/Windows_Vista.md "wikilink")[Windows
7使用的軟體產品](../Page/Windows_7.md "wikilink")。

2011年9月，Google公司宣佈停止此服務。

## 可選用軟體

使用者能夠選擇下列的程式來安裝。假設程式已經被安裝，[Google更新器檢查使用者是否擁有最新版本](../Page/#Google更新器.md "wikilink")，如果使用者同意更新時，將會進行軟體版本的更新。

## 語言／地區差異

已提供多種的語言／地區的軟體選項可以選擇使用。目前各版的Google Pack提供下列的軟體可供安裝。

### 簡體中文版

#### Google（谷歌）程序

  - [谷歌浏览器](../Page/谷歌浏览器.md "wikilink")
  - Google企业应用套件
  - [Google地球](../Page/Google地球.md "wikilink")
  - [谷歌拼音输入法](../Page/谷歌拼音输入法.md "wikilink")
  - 用于[Internet
    Explorer的](../Page/Internet_Explorer.md "wikilink")[Google工具栏](../Page/Google工具栏.md "wikilink")
  - [Google桌面](../Page/Google桌面.md "wikilink")
  - [Google Picasa](../Page/Google_Picasa.md "wikilink")

#### 第三方程序

  - [Spyware Doctor](../Page/Spyware_Doctor.md "wikilink")（带防毒组件包）
  - [傲遊瀏覽器](../Page/傲遊瀏覽器.md "wikilink")2
  - [WPS Office 2009个人版](../Page/WPS#WPS_Office个人版.md "wikilink")
  - [Adobe Reader](../Page/Adobe_Acrobat.md "wikilink")
  - [Mozilla
    Firefox浏览器](../Page/Mozilla_Firefox.md "wikilink")（带有[Google工具栏](../Page/Google工具栏.md "wikilink")）

##### 已經被移除的第三方程序

  - [瑞星殺毒軟件](../Page/瑞星殺毒軟件.md "wikilink")
  - [金山詞霸](../Page/金山詞霸.md "wikilink")

### 繁體中文版

#### Google程式

  - [Google地球](../Page/Google地球.md "wikilink")
  - 針對[Internet
    Explorer的](../Page/Internet_Explorer.md "wikilink")[Google工具列](../Page/Google工具列.md "wikilink")
  - [Google桌面](../Page/Google桌面.md "wikilink")
  - [Google Picasa](../Page/Google_Picasa.md "wikilink")
  - [Google瀏覽器](../Page/Google瀏覽器.md "wikilink")

#### 第三方程式

  - [Spyware Doctor with
    Anti-Virus](../Page/Spyware_Doctor.md "wikilink")
  - [Adobe Reader](../Page/Adobe_Acrobat.md "wikilink")
  - 附加[Google工具列的](../Page/Google工具列.md "wikilink")[Mozilla
    Firefox](../Page/Mozilla_Firefox.md "wikilink")
  - [RealPlayer](../Page/RealPlayer.md "wikilink")

### 美國版

#### Google程式

  - [Google Chrome](../Page/Google_Chrome.md "wikilink")
  - [Google Apps](../Page/Google_Apps.md "wikilink")
  - [Google Earth](../Page/Google_Earth.md "wikilink")
  - [Google Toolbar for IE](../Page/Google工具列.md "wikilink")
  - [Google Desktop](../Page/Google_Desktop.md "wikilink")
  - [Google Picasa](../Page/Google_Picasa.md "wikilink")
  - [Google Talk](../Page/Google_Talk.md "wikilink")
  - Google軟體集 螢幕保護程式—一份顯示使用者電腦圖片的螢幕保護程式。

#### 第三方程式

  - [Spyware Doctor with
    Anti-Virus](../Page/Spyware_Doctor.md "wikilink")
  - [Adobe Reader](../Page/Adobe_Acrobat.md "wikilink")
  - [Firefox with Google Toolbar](../Page/Mozilla_Firefox.md "wikilink")
  - [Skype](../Page/Skype.md "wikilink")
  - [RealPlayer](../Page/RealPlayer.md "wikilink")

Google称其未与上述軟體的开发商签订财务协议，软件开发商提供软件是为了Google用户的方便，並非任何財務理由。\[1\]

##### 已經被移除的第三方程式

  - [Ad-Aware](../Page/Ad-Aware.md "wikilink")
  - [Trillian](../Page/Trillian.md "wikilink")
  - [Norton Antivirus特別版](../Page/Norton_Antivirus.md "wikilink")2005 -
    有六個月試用期
  - [StarOffice](../Page/StarOffice.md "wikilink")
  - GalleryPlayer
  - Norton Security Scan

## Google更新器

Google Pack与Google
Updater一并发行。Updater帮助用户下载和安装，并定期检查升级，同時作為一個迷你的“新增移除程式”管理員，除了他只管理屬於Google軟體集內的程式。如果你移除了Google
Updater，其他屬於Google軟體集的程式將不會被更動。如果你想將Google軟體集的程式移除，可使用Window內置的新增移除程式，或使用Google
Updater進行。

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 參考資料

## 外部連結

  - [Google
    Pack](https://web.archive.org/web/20090523125255/http://pack.google.com/intl/en/pack_installer.html?hl=en)
      - [Google软件精选](https://web.archive.org/web/20090418185320/http://pack.google.com/intl/zh-cn/pack_installer.html?hl=zh-cn)
      - [Google軟體集](https://web.archive.org/web/20090523114409/http://pack.google.com/intl/zh-tw/pack_installer.html?hl=zh-tw)

[Google軟體集](../Category/已終止的Google服務.md "wikilink")

1.