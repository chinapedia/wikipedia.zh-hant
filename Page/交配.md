[Tortoise_mating.jpg](https://zh.wikipedia.org/wiki/File:Tortoise_mating.jpg "fig:Tortoise_mating.jpg")在交配\]\]
[Borboleta_px_cp_Sta_crz_040206_D.JPG](https://zh.wikipedia.org/wiki/File:Borboleta_px_cp_Sta_crz_040206_D.JPG "fig:Borboleta_px_cp_Sta_crz_040206_D.JPG")在花朵上交配\]\]

**交配**是指的是[生物的](../Page/生物.md "wikilink")[生殖细胞进行交换](../Page/生殖细胞.md "wikilink")，导致[受精和](../Page/受精.md "wikilink")[繁殖的活动](../Page/繁殖.md "wikilink")。家畜之间的交配活动通常也可称为**配种**。[昆蟲](../Page/昆蟲.md "wikilink")、[鸟类和](../Page/鸟类.md "wikilink")[卵胎生](../Page/卵胎生.md "wikilink")[鱼类的交配活动可以称为](../Page/鱼类.md "wikilink")**交尾**。

雄性的[精子会进入雌性体内](../Page/精子.md "wikilink")，并且与[卵细胞发生](../Page/卵细胞.md "wikilink")[受精作用](../Page/受精.md "wikilink")。但若受精过程发生在体外或是没有性交过程，则不再是交尾，例如鱼类（雄性和雌性将生殖细胞排到水中，完成受精）或是[人工授精](../Page/人工授精.md "wikilink")。

## 自然交配

在大部分的动物（如[鸟](../Page/鸟.md "wikilink")，[爬行动物](../Page/爬行动物.md "wikilink")）的交配，是双方生殖开口（[泄殖腔](../Page/泄殖腔.md "wikilink")）的对接，而在人类和其他哺乳动物则是雄性生殖器（[阴茎](../Page/阴茎.md "wikilink")）插入雌性生殖器（[阴道](../Page/阴道.md "wikilink")）、或者雌性生殖器包住雄性生殖器，即性交。

交配是由于[本能](../Page/本能.md "wikilink")，[性欲和相应的诱发因素引起的](../Page/性欲.md "wikilink")（如：[孔雀的开屏](../Page/孔雀.md "wikilink")，雌性狗散发的气味分子等）。

## 参见

  - [遗传学](../Page/遗传学.md "wikilink")
  - [授精](../Page/授精.md "wikilink")
  - [性交](../Page/性交.md "wikilink")

## 外部链接

  - [Introduction to Animal
    Reproduction](https://web.archive.org/web/20060216005917/http://tidepool.st.usm.edu/crswr/103animalreproduction.html)
  - [Advantages of Sexual
    Reproduction](http://www.pbs.org/wgbh/evolution/sex/advantage/)

## 参见

  - [遗传学](../Page/遗传学.md "wikilink")
  - [授精](../Page/授精.md "wikilink")
  - [性交](../Page/性交.md "wikilink")

## 外部链接

  - [Introduction to Animal
    Reproduction](https://web.archive.org/web/20060216005917/http://tidepool.st.usm.edu/crswr/103animalreproduction.html)
  - [Advantages of Sexual
    Reproduction](http://www.pbs.org/wgbh/evolution/sex/advantage/)

[交配](../Category/交配.md "wikilink")
[Category:發育生物學](../Category/發育生物學.md "wikilink")
[Category:动物行为学](../Category/动物行为学.md "wikilink")
[Category:生育力](../Category/生育力.md "wikilink")
[动](../Category/動物生殖.md "wikilink")
[Category:性學](../Category/性學.md "wikilink")
[交配](../Category/交配.md "wikilink")
[Category:發育生物學](../Category/發育生物學.md "wikilink")
[Category:动物行为学](../Category/动物行为学.md "wikilink")
[Category:生育力](../Category/生育力.md "wikilink")
[Category:性學](../Category/性學.md "wikilink")