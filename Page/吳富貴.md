**吳富貴**，[台灣](../Page/台灣.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，曾代表[民主進步黨任職](../Page/民主進步黨.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")；兄弟吳富亭也是前台中縣[議員](../Page/議員.md "wikilink")。

## 學歷

  - [中華大學工業工程管理研究所碩士](../Page/中華大學.md "wikilink")
  - [國立師範大學數學系畢業](../Page/國立師範大學.md "wikilink")
  - [台中一中](../Page/台中一中.md "wikilink")
  - 岸裡國小

## 經歷

  - 第三屆國大代表
  - [台中縣政府機要秘書](../Page/台中縣.md "wikilink")
  - 台中榮總電腦工程師
  - [私立明道高中教師](../Page/臺中縣私立明道高級中學.md "wikilink")
  - [民進黨台中縣黨部執行委員](../Page/民進黨.md "wikilink")
  - 省議員[郭俊銘及立委聯合服務處總幹事](../Page/郭俊銘.md "wikilink")
  - 文建會社造員

## 政治

### 2002年豐原市長選舉

| 2002年[豐原市市長選舉結果](../Page/豐原區.md "wikilink") |
| ------------------------------------------- |
| 號次                                          |
| 1                                           |
| 2                                           |
| 3                                           |
| **選舉人數**                                    |
| **投票數**                                     |
| **有效票**                                     |
| **無效票**                                     |
| **投票率**                                     |

### 2004年立法委員選舉

| 2004年臺中縣選舉區立法委員選舉結果 |
| ------------------- |
| 應選11席               |
| 號次                  |
| 1                   |
| 2                   |
| 3                   |
| 4                   |
| 5                   |
| 6                   |
| 7                   |
| 8                   |
| 9                   |
| 10                  |
| 11                  |
| 12                  |
| 13                  |
| 14                  |
| 15                  |
| 16                  |
| 17                  |
| 18                  |
| 19                  |
| 20                  |
| 21                  |
| 22                  |

## 外部連結

  - [立法院](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00033&stage=6)
  - [線上大國民 Mighty People Online - 立法委員資料 –
    吳富貴](http://www.mightypeople.org/congress/congress.php?id=201)

[W吳](../Category/第6屆中華民國立法委員.md "wikilink")
[W吳](../Category/民主進步黨黨員.md "wikilink")
[Category:中華大學校友](../Category/中華大學校友.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")