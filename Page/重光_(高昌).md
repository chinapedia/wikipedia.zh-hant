**重光**（620年—623年）是[高昌君主](../Page/高昌.md "wikilink")[麴伯雅的年號](../Page/麴伯雅.md "wikilink")，共4年。

也有文献\[1\]认为是[麴文泰的年号](../Page/麴文泰.md "wikilink")。

## 大事记

## 出生

## 逝世

## 纪年

|                                  |                                |                                |                                |                                |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 重光                               | 元年                             | 二年                             | 三年                             | 四年                             |
| [公元](../Page/公元纪年.md "wikilink") | 620年                           | 621年                           | 622年                           | 623年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") |

  - 同期存在的其他政权年号
      - [太平](../Page/太平_\(林士弘\).md "wikilink")（616年十二月—622年十月）：[隋朝時期](../Page/隋朝.md "wikilink")[楚政權](../Page/楚.md "wikilink")[林士弘年号](../Page/林士弘.md "wikilink")
      - [五凤](../Page/五凤_\(窦建德\).md "wikilink")（618年十一月—621年五月）：[隋朝時期](../Page/隋朝.md "wikilink")[夏政權](../Page/夏.md "wikilink")[窦建德年号](../Page/窦建德.md "wikilink")
      - [天兴](../Page/天兴_\(刘武周\).md "wikilink")（617年三月—620年四月）：[隋朝時期](../Page/隋朝.md "wikilink")[刘武周年号](../Page/刘武周.md "wikilink")
      - [永隆](../Page/永隆_\(梁師都\).md "wikilink")（618年—628年四月）：[隋朝時期](../Page/隋朝.md "wikilink")[梁政權](../Page/梁.md "wikilink")[梁師都年号](../Page/梁師都.md "wikilink")
      - [始兴](../Page/始兴_\(高开道\).md "wikilink")（618年十二月—624年二月）：[隋朝時期](../Page/隋朝.md "wikilink")[燕政權](../Page/燕.md "wikilink")[高开道年号](../Page/高开道.md "wikilink")
      - [开明](../Page/开明_\(王世充\).md "wikilink")（619年四月—621年五月）：[隋朝時期](../Page/隋朝.md "wikilink")[鄭政權](../Page/鄭.md "wikilink")[王世充年号](../Page/王世充.md "wikilink")
      - [延康](../Page/延康_\(沈法兴\).md "wikilink")（619年九月—620年十二月）：[隋朝時期](../Page/隋朝.md "wikilink")[梁政權](../Page/梁.md "wikilink")[沈法兴年号](../Page/沈法兴.md "wikilink")
      - [明政](../Page/明政.md "wikilink")（619年九月—621年十一月）：[隋朝時期](../Page/隋朝.md "wikilink")[吳政權](../Page/吳.md "wikilink")[李子通年号](../Page/李子通.md "wikilink")
      - [天造](../Page/天造.md "wikilink")（622年正月—623年正月）：[隋朝時期](../Page/隋朝.md "wikilink")[刘黑闼年号](../Page/刘黑闼.md "wikilink")
      - [天明](../Page/天明_\(輔公祏\).md "wikilink")（623年八月—624年三月）：[隋朝時期](../Page/隋朝.md "wikilink")[輔公祏年号](../Page/輔公祏.md "wikilink")
      - [進通](../Page/進通.md "wikilink")（623年）：[隋朝時期](../Page/隋朝.md "wikilink")[王摩沙年号](../Page/王摩沙.md "wikilink")
      - [武德](../Page/武德.md "wikilink")（618年五月—626年十二月）：[唐朝政权唐高祖](../Page/唐朝.md "wikilink")[李淵年号](../Page/李淵.md "wikilink")
      - [建福](../Page/建福_\(新羅真平王\).md "wikilink")（584年—634年）：[新羅](../Page/新羅.md "wikilink")[真平王之年號](../Page/新羅真平王.md "wikilink")

## 注釋

<references />

## 參見

  - [中国年号索引](../Page/中国年号索引.md "wikilink")

[Category:高昌年号](../Category/高昌年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:620年代中国政治](../Category/620年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月 ISBN 7101025129