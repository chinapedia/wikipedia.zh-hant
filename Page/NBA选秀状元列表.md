[LebronFT2.jpg](https://zh.wikipedia.org/wiki/File:LebronFT2.jpg "fig:LebronFT2.jpg")為近年來最強的狀元之一\]\]
**NBA选秀状元**（）是历届[NBA选秀大会的第一顺位球员](../Page/NBA选秀.md "wikilink")。是由上一年次NBA常规赛中未进入季后赛的较差战绩球队所参加，由一支球队抽得下赛季的第一选秀权，被这支球队选中的球员即NBA选秀状元，被选中的选秀状元通常都是球队、全球媒体和球迷的焦点人物\[1\]。

NBA选秀状元随后獲得[NBA最有價值球員獎的球员有](../Page/NBA最有價值球員.md "wikilink")[奥斯卡·罗伯逊](../Page/奥斯卡·罗伯逊.md "wikilink")、[卡里姆·阿卜杜勒·贾巴尔](../Page/卡里姆·阿卜杜勒·贾巴尔.md "wikilink")（六次），[比尔·沃尔顿](../Page/比尔·沃尔顿.md "wikilink")、[埃尔文·约翰逊](../Page/埃尔文·约翰逊.md "wikilink")（三次），[阿基姆·奥拉朱旺](../Page/阿基姆·奥拉朱旺.md "wikilink")、[沙奎尔·奥尼尔](../Page/沙奎尔·奥尼尔.md "wikilink")、[阿伦·艾弗森](../Page/阿伦·艾弗森.md "wikilink")、[蒂姆·邓肯](../Page/蒂姆·邓肯.md "wikilink")（两次）、[勒布朗·詹姆斯](../Page/勒布朗·詹姆斯.md "wikilink")（四次）、[德瑞克·羅斯](../Page/德瑞克·羅斯.md "wikilink")。

[中国球员](../Page/中华人民共和国.md "wikilink")[姚明](../Page/姚明.md "wikilink")（2002年）和
[意大利球员](../Page/意大利.md "wikilink")[安德里亚·巴格纳尼](../Page/安德里亚·巴格纳尼.md "wikilink")（2006年）是仅有的两位在选秀之前没有经历过任何美国篮球体系训练的状元。其他12位非美国本土出生的状元分别是1978年来自[巴哈马的](../Page/巴哈马.md "wikilink")[米卡尔·汤普森](../Page/米卡尔·汤普森.md "wikilink")、1984年来自[尼日利亚的](../Page/尼日利亚.md "wikilink")[阿基姆·奥拉朱旺](../Page/阿基姆·奥拉朱旺.md "wikilink")、1985年來自[牙買加的](../Page/牙買加.md "wikilink")[派屈克·尤英](../Page/派屈克·尤英.md "wikilink")、1997年来自[美属维京群岛的](../Page/美属维京群岛.md "wikilink")[提姆·鄧肯](../Page/提姆·鄧肯.md "wikilink")、1998年来自尼日利亚的[迈克尔·奥洛沃坎迪](../Page/迈克尔·奥洛沃坎迪.md "wikilink")、2005年来自[澳洲的](../Page/澳洲.md "wikilink")[安德鲁·博古特](../Page/安德鲁·博古特.md "wikilink")、2011年同樣來自[澳洲的](../Page/澳洲.md "wikilink")[凱里·厄文](../Page/凱里·厄文.md "wikilink")、2013年來自[加拿大的](../Page/加拿大.md "wikilink")[安东尼·贝内特](../Page/安东尼·贝内特.md "wikilink")、2014年來自[加拿大的](../Page/加拿大.md "wikilink")[安德魯·威金斯](../Page/安德魯·威金斯.md "wikilink")、2015年來自[多明尼加的](../Page/多明尼加.md "wikilink")[卡爾-安東尼·唐斯](../Page/卡爾-安東尼·唐斯.md "wikilink")、2016年來自[澳洲的](../Page/澳洲.md "wikilink")[班·西蒙斯以及](../Page/班·西蒙斯.md "wikilink")2018年來自[巴哈馬的](../Page/巴哈馬.md "wikilink")[德安德魯·艾頓](../Page/德安德魯·艾頓.md "wikilink")。而鄧肯出生即为美国人，但是仍然被定义为NBA「国际球员」，因为他来自于[美属维京群岛](../Page/美属维京群岛.md "wikilink")。

作為NBA前身的（BAA）舉辦了1947～1949年的選秀大會，而此紀錄也被NBA所承認並視為NBA選秀歷史的一部分。其中某些在NBA發展不順利的球員則被稱為「水貨狀元」\[2\]。

## 標示

<table>
<tbody>
<tr class="odd">
<td><p>^</p></td>
<td><p>表示該球員曾被選入<a href="../Page/NBA明星賽.md" title="wikilink">NBA明星賽或</a><a href="../Page/NBA最佳陣容.md" title="wikilink">NBA最佳陣容</a> &lt;!-- |-</p></td>
<td><p>*</p></td>
<td><p>Elected to the <a href="../Page/Naismith_Memorial_Basketball_Hall_of_Fame.md" title="wikilink">Naismith Memorial Basketball Hall of Fame</a> --&gt;</p></td>
</tr>
<tr class="even">
<td><p>^*</p></td>
<td><p>表示有參加過明星賽且被選入<a href="../Page/奈史密斯籃球名人紀念堂.md" title="wikilink">奈史密斯籃球名人紀念堂</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><em>球員</em><br />
<small>(斜體字)</small></p></td>
<td><p>表示為當年<a href="../Page/NBA最佳新秀.md" title="wikilink">NBA最佳新秀</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>PPG</p></td>
<td><p>新人球季平均得分[3]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>APG</p></td>
<td><p>新人球季平均助攻[4]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>RPG</p></td>
<td><p>新人球季平均籃板[5]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## NBA选秀状元列表

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>球員</p></th>
<th><p>圖片</p></th>
<th><p>國籍</p></th>
<th><p>位置</p></th>
<th><p>選中球隊</p></th>
<th><p>大學/高中/前球團</p></th>
<th><p>PPG</p></th>
<th><p>RPG</p></th>
<th><p>APG</p></th>
<th><p>備註/來源</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1947年BAA選秀.md" title="wikilink">1947</a></p></td>
<td><p><a href="../Page/克里夫頓·麥克內利.md" title="wikilink">克里夫頓·麥克內利</a>{{#tag:ref|克里夫頓·麥克內利並未在聯盟中出場過一分鐘，但是他選擇成為一名高中籃球隊教練。[6][7]|group="注"|name=note_b}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>—</p></td>
<td><p><a href="../Page/匹茲堡鐵人.md" title="wikilink">匹茲堡鐵人</a></p></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[8]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1948年BAA選秀.md" title="wikilink">1948</a></p></td>
<td><p><a href="../Page/安迪·唐克維奇.md" title="wikilink">安迪·唐克維奇</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a>/<a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td><p><a href="../Page/普羅維登斯蒸汽壓路機.md" title="wikilink">普羅維登斯蒸汽壓路機</a></p></td>
<td><p><a href="../Page/馬歇爾大學.md" title="wikilink">馬歇爾大學</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td><p>[9]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1949年BAA選秀.md" title="wikilink">1949</a></p></td>
<td><p><a href="../Page/豪伊·香農.md" title="wikilink">豪伊·香農</a>{{#tag:ref|豪伊·香農雖然是选秀状元，但<a href="../Page/愛德·麥考利.md" title="wikilink">愛德·麥考利及</a><a href="../Page/沃恩·米克尔森.md" title="wikilink">沃恩·麥克爾森已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[10]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p><a href="../Page/中鋒_(籃球).md" title="wikilink">中鋒</a></p></td>
<td><p><a href="../Page/普羅維登斯蒸汽壓路機.md" title="wikilink">普羅維登斯蒸汽壓路機</a></p></td>
<td><p><a href="../Page/堪薩斯州立大學.md" title="wikilink">堪薩斯州立大學</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td><p>[11]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1950年NBA選秀.md" title="wikilink">1950</a></p></td>
<td><p><a href="../Page/查克·謝爾.md" title="wikilink">查克·謝爾</a>{{#tag:ref|查克·謝爾並沒有在{{nbay|1950</p></td>
<td><p>1951</p></td>
<td><p>app=賽季}}開始。[12]謝爾雖然是选秀状元，但<a href="../Page/保羅·阿里金.md" title="wikilink">保羅·阿里金已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[13]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p><a href="../Page/鮑林格林州立大學.md" title="wikilink">鮑林格林州立大學</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1951年NBA選秀.md" title="wikilink">1951</a></p></td>
<td><p>{{#tag:ref|邁爾齊奧瑞因為參與被禁止出賽。[14]邁爾齊奧瑞雖然是选秀状元，但已經透過<a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[15]|group="注"|name=note_e}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td><p><a href="../Page/巴爾的摩子彈_(1944年–1954年).md" title="wikilink">巴爾的摩子彈</a></p></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1952年NBA選秀.md" title="wikilink">1952</a></p></td>
<td><p><a href="../Page/马克·沃克曼.md" title="wikilink">马克·沃克曼</a>{{#tag:ref|沃克曼雖然是选秀状元，但<a href="../Page/比爾·米克維.md" title="wikilink">比爾·米克維已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[17]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">密爾瓦基老鷹</a></p></td>
<td><p><a href="../Page/西維吉尼亞大學.md" title="wikilink">西維吉尼亞大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[18]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1953年NBA選秀.md" title="wikilink">1953</a></p></td>
<td><p><em><a href="../Page/雷·菲利克斯.md" title="wikilink">雷·菲利克斯</a></em>^{{#tag:ref|菲利克斯雖然是选秀状元，但<a href="../Page/厄爾尼·貝克.md" title="wikilink">厄爾尼·貝克與</a><a href="../Page/沃爾特·杜克斯.md" title="wikilink">沃爾特·杜克斯已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[19]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/巴爾的摩子彈_(1944年–1954年).md" title="wikilink">巴爾的摩子彈</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[20]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年NBA選秀.md" title="wikilink">1954</a></p></td>
<td><p>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/巴爾的摩子彈_(1944年–1954年).md" title="wikilink">巴爾的摩子彈</a></p></td>
<td><p><a href="../Page/傅爾曼大學.md" title="wikilink">傅爾曼大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[21]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1955年NBA選秀.md" title="wikilink">1955</a></p></td>
<td><p><a href="../Page/迪克·瑞克茨.md" title="wikilink">迪克·瑞克茨</a>{{#tag:ref|瑞克茨雖然是选秀状元，但<a href="../Page/迪克·加爾梅克.md" title="wikilink">迪克·加爾梅克與</a><a href="../Page/湯姆·古拉.md" title="wikilink">湯姆·古拉已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[22]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">聖路易老鷹</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[23]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1956年NBA選秀.md" title="wikilink">1956</a></p></td>
<td><p><a href="../Page/西伊·格林.md" title="wikilink">西伊·格林</a>{{#tag:ref|格林雖然是选秀状元，但<a href="../Page/湯姆·海因索恩.md" title="wikilink">湯姆·海因索恩已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[24]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">羅徹斯特皇家</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[25]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1957年NBA選秀.md" title="wikilink">1957</a></p></td>
<td><p>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hot_Rod_Hundley.jpg" title="fig:Hot_Rod_Hundley.jpg">Hot_Rod_Hundley.jpg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">羅徹斯特皇家</a></p></td>
<td><p><a href="../Page/西維吉尼亞大學.md" title="wikilink">西維吉尼亞大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[26]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1958年NBA選秀.md" title="wikilink">1958</a></p></td>
<td><p><em><a href="../Page/艾爾金·貝勒.md" title="wikilink">艾爾金·貝勒</a></em>^*{{#tag:ref|貝勒雖然是选秀状元，但<a href="../Page/蓋伊·羅傑斯.md" title="wikilink">蓋伊·羅傑斯已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[27]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">明尼亞波利斯湖人</a></p></td>
<td><p><a href="../Page/西雅圖大學.md" title="wikilink">西雅圖大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[28]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1959年NBA選秀.md" title="wikilink">1959</a></p></td>
<td><p><a href="../Page/鲍勃·布泽尔.md" title="wikilink">鲍勃·布泽尔</a>^{{#tag:ref|布瑟雖然是选秀状元，但<a href="../Page/威爾特·張伯倫.md" title="wikilink">威爾特·張伯倫與</a><a href="../Page/鮑伯·費里.md" title="wikilink">鮑伯·費里已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[29]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">羅徹斯特皇家</a></p></td>
<td><p><a href="../Page/堪薩斯州立大學.md" title="wikilink">堪薩斯州立大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[30]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年NBA選秀.md" title="wikilink">1960</a></p></td>
<td><p><em><a href="../Page/奧斯卡·羅伯森.md" title="wikilink">奧斯卡·羅伯森</a></em>^*{{#tag:ref|雖然羅伯森已經透過<a href="../Page/地域指名.md" title="wikilink">地域指名被</a><a href="../Page/沙加緬度國王.md" title="wikilink">羅切斯特皇家選中了</a>，但因為皇家隊擁有狀元籤所以還是視為選秀狀元。<ref>{{Cite_web|url=<a href="http://www.nba.com/history/players/robertson_bio.html%7Ctitle=Oscar">http://www.nba.com/history/players/robertson_bio.html|title=Oscar</a> Robertson Bio|work=NBA.com|publisher=Turner Sports Interactive, Inc</p></td>
<td><p>date=|accessdate=September 10, 2009}}</ref>[31]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Oscar_Robertson_1960s.jpeg" title="fig:Oscar_Robertson_1960s.jpeg">Oscar_Robertson_1960s.jpeg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">羅徹斯特皇家</a></p></td>
<td><p><a href="../Page/辛辛納提大學.md" title="wikilink">辛辛納提大學</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1961年NBA選秀.md" title="wikilink">1961</a></p></td>
<td><p><em><a href="../Page/華特·貝拉米.md" title="wikilink">華特·貝拉米</a></em>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">芝加哥包裝工</a></p></td>
<td><p><a href="../Page/印第安納大學.md" title="wikilink">印第安納大學布魯明頓校區</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[32]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年NBA選秀.md" title="wikilink">1962</a></p></td>
<td><p><a href="../Page/比尔·迈克吉尔.md" title="wikilink">比尔·迈克吉尔</a>{{#tag:ref|迈克吉尔雖然是选秀状元，但<a href="../Page/戴夫·德布斯切爾.md" title="wikilink">戴夫·德布斯切爾與</a><a href="../Page/傑里·盧卡斯.md" title="wikilink">傑里·盧卡斯已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[33]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">芝加哥西風</a></p></td>
<td><p><a href="../Page/猶他大學.md" title="wikilink">猶他大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[34]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1963年NBA選秀.md" title="wikilink">1963</a></p></td>
<td><p><a href="../Page/阿特·海曼.md" title="wikilink">阿特·海曼</a>{{#tag:ref|海曼雖然是选秀状元，但已經透過<a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[35]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒/後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p><a href="../Page/杜克大學.md" title="wikilink">杜克大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[36]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1964年NBA選秀.md" title="wikilink">1964</a></p></td>
<td><p><a href="../Page/吉姆·巴恩斯.md" title="wikilink">吉姆·巴恩斯</a>{{#tag:ref|巴恩斯雖然是选秀状元，但 與已經透過<a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[37]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒/前鋒</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p><a href="../Page/德克薩斯大學.md" title="wikilink">德克薩斯大學艾爾帕索分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[38]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1965年NBA選秀.md" title="wikilink">1965</a></p></td>
<td><p><a href="../Page/弗萊德·黑特澤爾.md" title="wikilink">弗萊德·黑特澤爾</a>{{#tag:ref|黑特澤爾雖然是选秀状元，但<a href="../Page/比爾·布拉德利.md" title="wikilink">比爾·布拉德利</a>、<a href="../Page/比爾·布恩汀.md" title="wikilink">比爾·布恩汀與</a><a href="../Page/蓋爾·古德里奇.md" title="wikilink">蓋爾·古德里奇已經透過</a><a href="../Page/地域指名.md" title="wikilink">地域指名被選中了</a>。[39]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">舊金山勇士</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[40]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1966年NBA選秀.md" title="wikilink">1966</a></p></td>
<td><p>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒/後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p><a href="../Page/密西根大學.md" title="wikilink">密西根大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[41]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1967年NBA選秀.md" title="wikilink">1967</a></p></td>
<td><p><a href="../Page/吉米·沃克.md" title="wikilink">吉米·沃克</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td><p><a href="../Page/普洛威頓斯學院.md" title="wikilink">普洛威頓斯學院</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[42]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年NBA選秀.md" title="wikilink">1968</a></p></td>
<td><p><a href="../Page/埃爾文·海耶斯.md" title="wikilink">埃爾文·海耶斯</a>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Elvin_Hayes_1973.jpeg" title="fig:Elvin_Hayes_1973.jpeg">Elvin_Hayes_1973.jpeg</a></p></td>
<td></td>
<td><p>中鋒/前鋒</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">聖地牙哥火箭</a></p></td>
<td><p><a href="../Page/休士頓大學.md" title="wikilink">休士頓大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[43]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1969年NBA選秀.md" title="wikilink">1969</a></p></td>
<td><p><em><a href="../Page/卡里姆·阿布都·賈霸.md" title="wikilink">卡里姆·阿布都·賈霸</a></em>^*{{#tag:ref|在1971–72賽季前，他將自己的名字從Lew Alcindor改成Kareem Abdul-Jabbar。[44]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kareem_Abdul-Jabbar_1974.jpeg" title="fig:Kareem_Abdul-Jabbar_1974.jpeg">Kareem_Abdul-Jabbar_1974.jpeg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td><p><a href="../Page/加州大學洛杉磯分校.md" title="wikilink">加州大學洛杉磯分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[45]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年NBA選秀.md" title="wikilink">1970</a></p></td>
<td><p><a href="../Page/鲍伯·兰尼尔.md" title="wikilink">鲍伯·兰尼尔</a>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:BobLanier.jpg" title="fig:BobLanier.jpg">BobLanier.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[46]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1971年NBA選秀.md" title="wikilink">1971</a></p></td>
<td><p><a href="../Page/奧斯汀·凱爾.md" title="wikilink">奧斯汀·凱爾</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p><a href="../Page/聖母大學.md" title="wikilink">聖母大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[47]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1972年NBA選秀.md" title="wikilink">1972</a></p></td>
<td><p><a href="../Page/拉鲁·马丁.md" title="wikilink">拉鲁·马丁</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p><a href="../Page/芝加哥洛约拉大学.md" title="wikilink">芝加哥洛约拉大学</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[48]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1973年NBA選秀.md" title="wikilink">1973</a></p></td>
<td><p><a href="../Page/道格·科林斯.md" title="wikilink">道格·科林斯</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Doug_Collins_gestures.jpg" title="fig:Doug_Collins_gestures.jpg">Doug_Collins_gestures.jpg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td><p><a href="../Page/伊利诺州立大学.md" title="wikilink">伊利诺州立大学</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[49]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1974年NBA選秀.md" title="wikilink">1974</a></p></td>
<td><p><a href="../Page/比尔·沃尔顿.md" title="wikilink">比尔·沃尔顿</a>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bill_Walton_–_Trail_Blazers_(1).jpg" title="fig:Bill_Walton_–_Trail_Blazers_(1).jpg">Bill_Walton_–_Trail_Blazers_(1).jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p><a href="../Page/洛杉磯加利福尼亞大學.md" title="wikilink">洛杉磯加利福尼亞大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[50]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1975年NBA選秀.md" title="wikilink">1975</a></p></td>
<td><p><a href="../Page/大卫·汤普森.md" title="wikilink">大卫·汤普森</a>^*{{#tag:ref|汤普逊之前在<a href="../Page/美國籃球協會.md" title="wikilink">美國籃球協會打球</a>，直到才開始他在NBA的新人球季。[51]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>前鋒/後衛</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p><a href="../Page/北卡羅來納州立大學.md" title="wikilink">北卡羅來納州立大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[52]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年NBA選秀.md" title="wikilink">1976</a></p></td>
<td><p><a href="../Page/約翰·盧卡斯三世_(1953年出生).md" title="wikilink">約翰·盧卡斯三世</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td><p><a href="../Page/馬里蘭大學學院市分校.md" title="wikilink">馬里蘭大學學院市分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[53]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1977年NBA選秀.md" title="wikilink">1977</a></p></td>
<td><p><a href="../Page/肯特·本森.md" title="wikilink">肯特·本森</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kent_Benson_attempts_a_hook_shot_over_Ken_Ferdinand.jpg" title="fig:Kent_Benson_attempts_a_hook_shot_over_Ken_Ferdinand.jpg">Kent_Benson_attempts_a_hook_shot_over_Ken_Ferdinand.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td><p><a href="../Page/印第安納大學.md" title="wikilink">印第安納大學布魯明頓校區</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[54]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年NBA選秀.md" title="wikilink">1978</a></p></td>
<td><p><a href="../Page/米卡尔·汤普森.md" title="wikilink">米卡尔·汤普森</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Mychal_Thompson.jpg" title="fig:Mychal_Thompson.jpg">Mychal_Thompson.jpg</a></p></td>
<td><p><br />
</p></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p><a href="../Page/明尼蘇達大學.md" title="wikilink">明尼蘇達大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[55]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1979年NBA選秀.md" title="wikilink">1979</a></p></td>
<td><p><a href="../Page/魔術強森.md" title="wikilink">魔術強森</a>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Magic_Lipofsky.jpg" title="fig:Magic_Lipofsky.jpg">Magic_Lipofsky.jpg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p><a href="../Page/密西根州立大學.md" title="wikilink">密西根州立大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[56]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1980年NBA選秀.md" title="wikilink">1980</a></p></td>
<td><p><a href="../Page/乔伊·巴里·卡罗尔.md" title="wikilink">乔伊·巴里·卡罗尔</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p><a href="../Page/普渡大學.md" title="wikilink">普渡大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[57]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1981年NBA選秀.md" title="wikilink">1981</a></p></td>
<td><p><a href="../Page/馬克·阿吉雷.md" title="wikilink">馬克·阿吉雷</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lipofsky_Mark_Aguirre.jpg" title="fig:Lipofsky_Mark_Aguirre.jpg">Lipofsky_Mark_Aguirre.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a></p></td>
<td><p><a href="../Page/帝博大學.md" title="wikilink">帝博大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[58]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1982年NBA選秀.md" title="wikilink">1982</a></p></td>
<td><p><a href="../Page/詹姆斯·沃西.md" title="wikilink">詹姆斯·沃西</a>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:James_Worthy_at_UNC_Basketball_game._February_10,_2007.jpg" title="fig:James_Worthy_at_UNC_Basketball_game._February_10,_2007.jpg">James_Worthy_at_UNC_Basketball_game._February_10,_2007.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p><a href="../Page/北卡羅來納大學教堂山分校.md" title="wikilink">北卡羅來納大學教堂山分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[59]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1983年NBA選秀.md" title="wikilink">1983</a></p></td>
<td><p><em><a href="../Page/雷夫·辛普森.md" title="wikilink">雷夫·辛普森</a>^*</em></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ralph_Sampson_2010.jpg" title="fig:Ralph_Sampson_2010.jpg">Ralph_Sampson_2010.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td><p><a href="../Page/維吉尼亞大學.md" title="wikilink">維吉尼亞大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[60] |-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1985年NBA選秀.md" title="wikilink">1985</a></p></td>
<td><p><em><a href="../Page/派屈克·尤英.md" title="wikilink">派屈克·尤英</a></em>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Patrick_Ewing_ca._1995.jpg" title="fig:Patrick_Ewing_ca._1995.jpg">Patrick_Ewing_ca._1995.jpg</a></p></td>
<td><p><br />
{{#tag:ref|尤英出生於<a href="../Page/牙買加.md" title="wikilink">牙買加</a>，但在讀大學時成為美國公民，[61]並加入美國隊參加<a href="../Page/1984年夏季奧林匹克運動會.md" title="wikilink">1984年夏季奧林匹克運動會</a>。[62]|group="注"}}</p></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p><a href="../Page/乔治城大学.md" title="wikilink">乔治城大学</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[63]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1986年NBA選秀.md" title="wikilink">1986</a></p></td>
<td><p><a href="../Page/布拉德·多赫蒂.md" title="wikilink">布拉德·多赫蒂</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Brad_Daugherty_basketball_270.jpg" title="fig:Brad_Daugherty_basketball_270.jpg">Brad_Daugherty_basketball_270.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p><a href="../Page/北卡羅來納大學教堂山分校.md" title="wikilink">北卡羅來納大學教堂山分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[64]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1987年NBA選秀.md" title="wikilink">1987</a></p></td>
<td><p><em><a href="../Page/大衛·羅賓森.md" title="wikilink">大衛·羅賓森</a></em>^*{{#tag:ref|羅賓森並未在當季出賽，而是加入<a href="../Page/美國海軍.md" title="wikilink">美國海軍</a>。[65]他的新人球季是從。[66]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:David_Robinson_(Team_USA).jpg" title="fig:David_Robinson_(Team_USA).jpg">David_Robinson_(Team_USA).jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td><p><a href="../Page/美國海軍學院.md" title="wikilink">美國海軍學院</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[67]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年NBA選秀.md" title="wikilink">1988</a></p></td>
<td><p><a href="../Page/丹尼·曼寧.md" title="wikilink">丹尼·曼寧</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Danny_Manning_2014.jpg" title="fig:Danny_Manning_2014.jpg">Danny_Manning_2014.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
<td><p><a href="../Page/堪薩斯州立大學.md" title="wikilink">堪薩斯州立大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[68]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1989年NBA選秀.md" title="wikilink">1989</a></p></td>
<td><p><a href="../Page/佩維斯·埃里森.md" title="wikilink">佩維斯·埃里森</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Basketball_player.svg" title="fig:Basketball_player.svg">Basketball_player.svg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td><p><a href="../Page/路易斯維爾大學.md" title="wikilink">路易斯維爾大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[69]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年NBA選秀.md" title="wikilink">1990</a></p></td>
<td><p><em><a href="../Page/德瑞克·柯曼.md" title="wikilink">德瑞克·柯曼</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Derrick_Coleman_2014.jpg" title="fig:Derrick_Coleman_2014.jpg">Derrick_Coleman_2014.jpg</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/布魯克林籃網.md" title="wikilink">紐澤西籃網</a></p></td>
<td><p><a href="../Page/雪城大學.md" title="wikilink">雪城大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[70]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1991年NBA選秀.md" title="wikilink">1991</a></p></td>
<td><p><em><a href="../Page/拉里·约翰逊_(1969年出生).md" title="wikilink">賴瑞·強森</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Larry_Johnson_(cropped).jpg" title="fig:Larry_Johnson_(cropped).jpg">Larry_Johnson_(cropped).jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/紐奧良鵜鶘.md" title="wikilink">夏洛特黃蜂</a></p></td>
<td><p><a href="../Page/内华达大学拉斯维加斯分校.md" title="wikilink">内华达大学拉斯维加斯分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[71]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1992年NBA選秀.md" title="wikilink">1992</a></p></td>
<td><p><em><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a></em>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Shaq_Heat_(cropped).jpg" title="fig:Shaq_Heat_(cropped).jpg">Shaq_Heat_(cropped).jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></p></td>
<td><p><a href="../Page/路易斯安那州立大學.md" title="wikilink">路易斯安那州立大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[72]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1993年NBA選秀.md" title="wikilink">1993</a></p></td>
<td><p><em><a href="../Page/克里斯·韋伯.md" title="wikilink">克里斯·韋伯</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chris_Webber_NBA_Asia_Challenge_2010.jpg" title="fig:Chris_Webber_NBA_Asia_Challenge_2010.jpg">Chris_Webber_NBA_Asia_Challenge_2010.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></p></td>
<td><p><a href="../Page/密西根大學.md" title="wikilink">密西根大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[73]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年NBA選秀.md" title="wikilink">1994</a></p></td>
<td><p><a href="../Page/格伦·罗宾逊.md" title="wikilink">格伦·罗宾逊</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Glenn_Robinson_2003_cropped.jpg" title="fig:Glenn_Robinson_2003_cropped.jpg">Glenn_Robinson_2003_cropped.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td><p><a href="../Page/普渡大學.md" title="wikilink">普渡大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[74]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1995年NBA選秀.md" title="wikilink">1995</a></p></td>
<td><p><a href="../Page/喬·史密斯.md" title="wikilink">喬·史密斯</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Joe_Smith_Cavs.jpg" title="fig:Joe_Smith_Cavs.jpg">Joe_Smith_Cavs.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p><a href="../Page/馬里蘭大學學院市分校.md" title="wikilink">馬里蘭大學學院市分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[75]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1996年NBA選秀.md" title="wikilink">1996</a></p></td>
<td><p><em><a href="../Page/艾倫·艾佛森.md" title="wikilink">艾倫·艾佛森</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Allen_Iverson_Lipofsky.jpg" title="fig:Allen_Iverson_Lipofsky.jpg">Allen_Iverson_Lipofsky.jpg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td><p><a href="../Page/乔治城大学.md" title="wikilink">乔治城大学</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[76]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1997年NBA選秀.md" title="wikilink">1997</a></p></td>
<td><p><em><a href="../Page/提姆·鄧肯.md" title="wikilink">提姆·鄧肯</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tim_Duncan.jpg" title="fig:Tim_Duncan.jpg">Tim_Duncan.jpg</a></p></td>
<td><p>{{#tag:ref|鄧肯出生即為美國人，但是仍然被定義為NBA「國際球員」，因為他來自於<a href="../Page/美屬維京群島.md" title="wikilink">美屬維京群島</a>。[77]|group="注"}}</p></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td><p><a href="../Page/維克弗斯特大學.md" title="wikilink">維克弗斯特大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[78]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年NBA選秀.md" title="wikilink">1998</a></p></td>
<td><p><a href="../Page/迈克尔·奥洛沃坎迪.md" title="wikilink">麥可·歐拉坎迪</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Michael_Olowokandi_with_Earl_Barron.jpg" title="fig:Michael_Olowokandi_with_Earl_Barron.jpg">Michael_Olowokandi_with_Earl_Barron.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
<td><p><a href="../Page/太平洋大學.md" title="wikilink">太平洋大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[79]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1999年NBA選秀.md" title="wikilink">1999</a></p></td>
<td><p><em><a href="../Page/艾爾頓·布蘭德.md" title="wikilink">艾爾頓·布蘭德</a></em>^{{#tag:ref|布蘭德與<a href="../Page/史蒂夫·弗朗西斯.md" title="wikilink">史蒂夫·法蘭西斯共同獲得當季</a><a href="../Page/NBA最佳新秀.md" title="wikilink">NBA最佳新秀</a>。[80]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Elton_Brand_10.jpg" title="fig:Elton_Brand_10.jpg">Elton_Brand_10.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/杜克大學.md" title="wikilink">杜克大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[81]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2000年NBA選秀.md" title="wikilink">2000</a></p></td>
<td><p><a href="../Page/肯楊·馬丁.md" title="wikilink">肯楊·馬丁</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kenyon_Martin_Nuggets.jpg" title="fig:Kenyon_Martin_Nuggets.jpg">Kenyon_Martin_Nuggets.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/布魯克林籃網.md" title="wikilink">紐澤西籃網</a></p></td>
<td><p><a href="../Page/辛辛納提大學.md" title="wikilink">辛辛納提大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[82]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2001年NBA選秀.md" title="wikilink">2001</a></p></td>
<td><p><a href="../Page/夸米·布朗.md" title="wikilink">夸米·布朗</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kwame_Brown_cropped.jpg" title="fig:Kwame_Brown_cropped.jpg">Kwame_Brown_cropped.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[83]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年NBA選秀.md" title="wikilink">2002</a></p></td>
<td><p><a href="../Page/姚明.md" title="wikilink">姚明</a>^*</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:YaoMingonoffense2.jpg" title="fig:YaoMingonoffense2.jpg">YaoMingonoffense2.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td><p><a href="../Page/上海大鯊魚籃球俱樂部.md" title="wikilink">上海大鯊魚</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[84]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2003年NBA選秀.md" title="wikilink">2003</a></p></td>
<td><p><em><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LeBron_James_18112009_1.jpg" title="fig:LeBron_James_18112009_1.jpg">LeBron_James_18112009_1.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[85]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年NBA選秀.md" title="wikilink">2004</a></p></td>
<td><p><a href="../Page/德懷特·霍華德.md" title="wikilink">德懷特·霍華德</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dwight_Howard_2.jpg" title="fig:Dwight_Howard_2.jpg">Dwight_Howard_2.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[86]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005年NBA選秀.md" title="wikilink">2005</a></p></td>
<td><p><em><a href="../Page/安德魯·博古特.md" title="wikilink">安德魯·博古特</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Andrew_Bogut_with_the_ball.jpg" title="fig:Andrew_Bogut_with_the_ball.jpg">Andrew_Bogut_with_the_ball.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
<td><p><a href="../Page/猶他大學.md" title="wikilink">猶他大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[87]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年NBA選秀.md" title="wikilink">2006</a></p></td>
<td><p><a href="../Page/安德里亞·巴格納尼.md" title="wikilink">安德里亞·巴格納尼</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Andrea_Bargnani.JPG" title="fig:Andrea_Bargnani.JPG">Andrea_Bargnani.JPG</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a></p></td>
<td><p><a href="../Page/特雷維索貝納通.md" title="wikilink">特雷維索貝納通</a> (<a href="../Page/義大利籃球甲級聯賽.md" title="wikilink">義大利</a>)</p></td>
<td></td>
<td></td>
<td></td>
<td><p>[88]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年NBA選秀.md" title="wikilink">2007</a></p></td>
<td><p><a href="../Page/格雷格·奧登.md" title="wikilink">格雷格·歐登</a>{{#tag:ref|歐登做膝蓋手術，奧登缺席了2007-08賽季全部的NBA賽事。他的新人球季是從開始。[89]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Greg_Oden.jpg" title="fig:Greg_Oden.jpg">Greg_Oden.jpg</a></p></td>
<td></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p><a href="../Page/俄亥俄州立大學.md" title="wikilink">俄亥俄州立大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[90]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年NBA選秀.md" title="wikilink">2008</a></p></td>
<td><p><em><a href="../Page/德瑞克·羅斯.md" title="wikilink">德瑞克·羅斯</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Derrick_Rose_2.jpg" title="fig:Derrick_Rose_2.jpg">Derrick_Rose_2.jpg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/曼非斯大學.md" title="wikilink">曼非斯大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[91]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年NBA選秀.md" title="wikilink">2009</a></p></td>
<td><p><em><a href="../Page/布雷克·葛里芬.md" title="wikilink">布雷克·葛里芬</a></em>^{{#tag:ref|葛里芬在季前賽中，灌籃著地時弄傷膝蓋，並缺席，他的新人球季是從開始。[92]|group="注"}}</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Blake_Griffin_2.jpg" title="fig:Blake_Griffin_2.jpg">Blake_Griffin_2.jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
<td><p><a href="../Page/奧克拉荷馬大學.md" title="wikilink">奧克拉荷馬大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[93]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年NBA選秀.md" title="wikilink">2010</a></p></td>
<td><p><a href="../Page/約翰·沃爾.md" title="wikilink">約翰·沃爾</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:John_Wall_Wizards.jpg" title="fig:John_Wall_Wizards.jpg">John_Wall_Wizards.jpg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td><p><a href="../Page/肯塔基大學.md" title="wikilink">肯塔基大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[94]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2011年NBA選秀.md" title="wikilink">2011</a></p></td>
<td><p><em><a href="../Page/凱里·厄文.md" title="wikilink">凱里·厄文</a></em>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Kyrie_Irving_(10355742694).jpg" title="fig:Kyrie_Irving_(10355742694).jpg">Kyrie_Irving_(10355742694).jpg</a></p></td>
<td><p><br />
{{#tag:ref|厄文是在澳洲出生，但後來搬到美國，並在高中時就加入美國隊。[95]|group="注"}}</p></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p><a href="../Page/杜克大學.md" title="wikilink">杜克大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[96]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2012年NBA選秀.md" title="wikilink">2012</a></p></td>
<td><p><a href="../Page/安東尼·戴維斯_(籃球員).md" title="wikilink">安東尼·戴維斯</a>^</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Anthony_Davis_Hornets.jpg" title="fig:Anthony_Davis_Hornets.jpg">Anthony_Davis_Hornets.jpg</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/紐奧良鵜鶘.md" title="wikilink">紐奧良鹈鹕</a></p></td>
<td><p><a href="../Page/肯塔基大學.md" title="wikilink">肯塔基大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[97]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2013年NBA選秀.md" title="wikilink">2013</a></p></td>
<td><p><a href="../Page/安東尼·貝內特.md" title="wikilink">安東尼·貝內特</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Anthony_Bennett_(10355636284).jpg" title="fig:Anthony_Bennett_(10355636284).jpg">Anthony_Bennett_(10355636284).jpg</a></p></td>
<td></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p><a href="../Page/内华达大学拉斯维加斯分校.md" title="wikilink">内华达大学拉斯维加斯分校</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[98]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年NBA選秀.md" title="wikilink">2014</a></p></td>
<td><p><em><a href="../Page/安德鲁·威金斯.md" title="wikilink">安德鲁·威金斯</a></em></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LeBron_James_and_Andre_Wiggins.jpg" title="fig:LeBron_James_and_Andre_Wiggins.jpg">LeBron_James_and_Andre_Wiggins.jpg</a></p></td>
<td></td>
<td><p>後衛/前鋒</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p><a href="../Page/堪薩斯大學.md" title="wikilink">堪薩斯大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[99]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015年NBA選秀.md" title="wikilink">2015</a></p></td>
<td><p><em><a href="../Page/卡爾-安東尼·唐斯.md" title="wikilink">卡爾-安東尼·唐斯</a></em></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Karl_Towns.jpg" title="fig:Karl_Towns.jpg">Karl_Towns.jpg</a></p></td>
<td><p><br />
{{#tag:ref|卡爾-安東尼·唐斯出生與長大於美國，但他的母親為多明尼加人（父親則是美國人），因此安東尼有資格代表多明尼加或美國出賽，而他選擇了前者。[100]|group="注"}}</p></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/明尼蘇達木狼.md" title="wikilink">明尼蘇達木狼</a></p></td>
<td><p><a href="../Page/肯塔基大學.md" title="wikilink">肯塔基大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[101]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年NBA選秀.md" title="wikilink">2016</a></p></td>
<td><p><a href="../Page/班·西蒙斯.md" title="wikilink">班·西蒙斯</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Ben_Simmons.jpg" title="fig:Ben_Simmons.jpg">Ben_Simmons.jpg</a></p></td>
<td><p><br />
</p></td>
<td><p>前鋒</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td><p><a href="../Page/路易斯安那州立大學.md" title="wikilink">路易斯安那州立大學</a></p></td>
<td><p>15.8</p></td>
<td><p>8.1</p></td>
<td><p>8.2</p></td>
<td><p>[102]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年NBA選秀.md" title="wikilink">2017</a></p></td>
<td><p><a href="../Page/馬基爾·富斯.md" title="wikilink">馬凱爾·富爾茲</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:20160330_MCDAAG_Markelle_Fultz_dribbling.jpg" title="fig:20160330_MCDAAG_Markelle_Fultz_dribbling.jpg">20160330_MCDAAG_Markelle_Fultz_dribbling.jpg</a></p></td>
<td></td>
<td><p>後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td><p><a href="../Page/華盛頓大學.md" title="wikilink">華盛頓大學</a></p></td>
<td><p>7.1</p></td>
<td><p>3.1</p></td>
<td><p>3.8</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年NBA選秀.md" title="wikilink">2018</a></p></td>
<td><p><a href="../Page/德安德魯·艾頓.md" title="wikilink">德安德魯·艾頓</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:20170329_MCDAAG_Deandre_Ayton_on_the_wing.jpg" title="fig:20170329_MCDAAG_Deandre_Ayton_on_the_wing.jpg">20170329_MCDAAG_Deandre_Ayton_on_the_wing.jpg</a></p></td>
<td></td>
<td><p>前鋒/中鋒</p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td><p><a href="../Page/亞利桑那大學.md" title="wikilink">亞利桑那大學</a></p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2019年NBA選秀.md" title="wikilink">2019</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td></td>
</tr>
</tbody>
</table>

## 註解

## 資料來源

  - 全文

<!-- end list -->

  -
<!-- end list -->

  - 細節

[Category:NBA选秀](../Category/NBA选秀.md "wikilink")
[Category:職業列表](../Category/職業列表.md "wikilink")

1.

2.

3.  除有特別說明外，數據皆為新人球季數值。

4.
5.
6.

7.

8.
9.

10.

11.

12.
13.

14.

15.

16.
17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.
52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.
67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.

88.

89.

90.

91.

92.

93.

94.

95.

96.

97.

98.

99.

100. \[<http://m.startribune.com/sports/wolves/309936841.html?interstitial=true>|
     Karl-Anthony Towns taken No.1 by Wolves; Tyus Jones acquired in
     trade with Cavaliers\], *Star Tribune*, June 26, 2015

101.

102.