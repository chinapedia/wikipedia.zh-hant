[Organization_american_states_building.jpg](https://zh.wikipedia.org/wiki/File:Organization_american_states_building.jpg "fig:Organization_american_states_building.jpg")
**美洲国家组织**（，简称：；，简称：；，简称：；，简称：），是一个以[美洲](../Page/美洲.md "wikilink")[国家为成员的](../Page/国家.md "wikilink")[国际组织](../Page/国际组织.md "wikilink")，总部位于[美国](../Page/美国.md "wikilink")[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")，成员为美洲的35个独立主權国家。

## 歷史

1890年4月14日，[美国同拉美](../Page/美国.md "wikilink")17个国家在[华盛顿举行的第一次美洲会议上](../Page/华盛顿.md "wikilink")，决定建立美洲共和国国际联盟及其常设机构——美洲共和国商务局，这便是美洲国家组织的前身。此后，4月14日被定为“泛美日”。1910年在[布宜诺斯艾利斯举行的第四次大会上](../Page/布宜诺斯艾利斯.md "wikilink")，改名为美洲共和国联盟。1923年在智利[圣地亚哥举行的第五次大会上改名为美洲大陆共和国联盟](../Page/圣地亚哥.md "wikilink")。1948年在哥伦比亚[波哥大举行的第九次会议上](../Page/波哥大.md "wikilink")，通过了《美洲国家组织宪章》，遂改称为美洲国家组织。1967年第三次泛美特別會議通過了憲章修改議定書，規定以「美洲國家大會」取代「美洲國際會議」，常設機構改稱「秘書處」，1970年生效。\[1\]

美洲国家组织宗旨是：加强美洲大陆的和平与安全；确保成员国之间和平解决争端；成员国遭侵略时，组织声援行动；谋求解决成员国之间的政治、经济、法律问题以及致力着重转化，促进各国间经济、社会、文化的合作以及清洁组织社会，加速美洲国家一体化进程。

最高权力机构为大会。每年召开一次年会，由各成员国参加。经三分之二成员国同意，可召开特别大会。常设理事会由成员国各派一名大使级代表组成，定期召开。秘书处为常设机构。

[冷戰期間在美國主導下](../Page/冷戰.md "wikilink")，1962年美洲國家組織宣布中止[古巴的會籍](../Page/古巴.md "wikilink")。2009年美洲國家組織以民主改革為條件同意[古巴可以重新申請入會](../Page/古巴.md "wikilink")，但古巴拒絕重新入會。\[2\]此後古巴和美國關係緩和及恢復外交關係後，古巴亦有列席會議。

值得注意的是，作為美國盟友的[加拿大在冷戰期間並無加入美國主導的美洲國家組織](../Page/加拿大.md "wikilink")，認為加入會削弱加拿大較為獨立性的外交政策，加拿大直至1990年冷戰結束才加入。

2004年10月8日，[米格尔·安赫尔·罗德里格斯由于面临国内的一系列腐败指控](../Page/米格尔·安赫尔·罗德里格斯.md "wikilink")，罗德里格斯向该组织递交了一封信件，表示将从15日起辞去秘书长职务。其后职位一直空缺。2005年4月29日[智利内政部长](../Page/智利.md "wikilink")[何塞·米格尔·因苏尔萨成为该地区组织新秘书长唯一候选人](../Page/何塞·米格尔·因苏尔萨.md "wikilink")。5月因苏尔萨被正式选为该组织新任秘书长。

2009年7月4日，美洲国家组织宣布中止[洪都拉斯的會籍](../Page/洪都拉斯.md "wikilink")，以抗议该国反對派透過[軍事政變推翻](../Page/2009年宏都拉斯軍事政變.md "wikilink")[宏都拉斯总统](../Page/宏都拉斯总统.md "wikilink")[塞拉亞](../Page/曼努埃爾·塞拉亞.md "wikilink")。\[3\]2011年6月，因洪都拉斯民選政府已經上台而恢復。\[4\]

## 成员国

共有35個美洲獨立主權國家是美洲國家組織的成員。1948年5月5日成立起，共有21個創始成員國。

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - (目前已退出)

美洲國家組織此後擴張，包括[加拿大及新獨立的](../Page/加拿大.md "wikilink")[加勒比海國家](../Page/加勒比海國家.md "wikilink")，以下為國家的加入年份。

  - (1967年起加入)

  - (1967)

  - (1969)

  - (1975)

  - (1977)

  - (1979)

  - (1979)

  - (1981)

  - (1981)

  - (1982)

  - (1984)

  - (1990)

  - (1991)

  - (1991)

## 備註

## 參見

  - [門羅主義](../Page/門羅主義.md "wikilink")
  - [華盛頓共識](../Page/華盛頓共識.md "wikilink")
  - [歐洲聯盟](../Page/歐洲聯盟.md "wikilink")
  - [北大西洋公約組織](../Page/北大西洋公約組織.md "wikilink")
  - [南美洲國家聯盟](../Page/南美洲國家聯盟.md "wikilink")
  - [非洲聯盟](../Page/非洲聯盟.md "wikilink")
  - [東南亞國家聯盟](../Page/東南亞國家聯盟.md "wikilink")

## 外部連結

  - [美洲國家組織](http://www.oas.org/en/)

  -
  -
  -
## 参考文献

[美洲国家组织](../Category/美洲国家组织.md "wikilink")
[Category:美洲国际性组织](../Category/美洲国际性组织.md "wikilink")
[Category:1948年美国建立](../Category/1948年美国建立.md "wikilink")

1.  《戰後國際關係地圖（1945-1985年）》，[張志](../Page/張志.md "wikilink")、[李謀源主編](../Page/李謀源.md "wikilink")，[中國地圖出版社](../Page/中國地圖出版社.md "wikilink")，1999年
2.  [OAS lifts 47-year-old suspension of
    Cuba](http://www.cnn.com/2009/WORLD/americas/06/03/cuba.oas/)
    [CNN](../Page/CNN.md "wikilink")
3.
4.