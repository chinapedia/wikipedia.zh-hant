[thumb](../Page/file:Night_at_the_Museum-hk.jpg.md "wikilink")
是一部2006年美國的[冒險](../Page/冒險.md "wikilink")[喜劇電影](../Page/喜劇電影.md "wikilink")，改編自[米蘭·特雷克於](../Page/米蘭·特雷克.md "wikilink")1993年撰寫的[童書](../Page/童書.md "wikilink")《The
Night at the
Museum》。劇情內容敘述一位剛[離婚的父親找到一個警衛的工作](../Page/離婚.md "wikilink")，負責在夜間看守[紐約市](../Page/紐約市.md "wikilink")[美國自然歷史博物館](../Page/美國自然歷史博物館.md "wikilink")，結果發現博物館裡的一個埃及工藝品能夠神奇地讓展覽品在夜晚都活了起來。

其续集为2009年电影[博物館驚魂夜2和](../Page/博物館驚魂夜2.md "wikilink")2014年[博物馆奇妙夜3](../Page/博物馆奇妙夜3.md "wikilink")。

## 故事

待業中的賴瑞終於找到工作，進入自然歷史博物館夜班守衛。本以為看守無人的博物館很輕鬆，想不到，一到晚上，展覽品不論人物動物都會復活，賴瑞除了要應付這群復活展品、躲避史前生物、更要搶救快被解體的博物館。

## 角色

**真實人物**

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/可基·尼恩爾.md" title="wikilink">可基·尼恩爾</a><br />
May Ricky Ni Ener</p></td>
<td><p>丹斯·安拉<br />
Dance Allah</p></td>
<td><p>博物館保全。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/班·史提勒.md" title="wikilink">班·史提勒</a><br />
Ben Stiller</p></td>
<td><p>賴瑞·達理<br />
Larry Daley</p></td>
<td><p>博物館保全。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/傑克·恰里.md" title="wikilink">傑克·恰里</a><br />
Jake Cherry</p></td>
<td><p>尼可拉斯（尼克）戴利<br />
Nicholas "Nick" Daley</p></td>
<td><p>賴瑞小兒子。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狄克·范·戴克.md" title="wikilink">狄克·范·戴克</a><br />
Dick Van Dyke</p></td>
<td><p>賽西·佛萊德里克<br />
Cecil Fredericks</p></td>
<td><p>高個子的白人老警衛。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡拉·裘吉諾.md" title="wikilink">卡拉·裘吉諾</a><br />
Carla Gugino</p></td>
<td><p>蕾貝卡·哈特曼<br />
Rebecca Hutman</p></td>
<td><p>女講師。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/米奇·隆尼.md" title="wikilink">米奇·隆尼</a><br />
Mickey Rooney</p></td>
<td><p>葛斯<br />
Gus</p></td>
<td><p>矮個子的白人老警衛。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/比爾·考布斯.md" title="wikilink">比爾·考布斯</a><br />
Bill Cobbs</p></td>
<td><p>雷金納德<br />
Reginald</p></td>
<td><p>黑人老警衛。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瑞奇·葛文斯.md" title="wikilink">瑞奇·葛文斯</a><br />
Ricky Gervais</p></td>
<td><p>麥菲博士<br />
Dr. McPhee</p></td>
<td><p>賴瑞的上司。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金·瑞弗.md" title="wikilink">金·瑞弗</a><br />
Kim Raver</p></td>
<td><p>艾莉卡·戴利<br />
Erica Daley</p></td>
<td><p>賴瑞前妻。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/保羅·路德.md" title="wikilink">保羅·路德</a><br />
Paul Rudd</p></td>
<td><p>唐<br />
Don</p></td>
<td><p>賴瑞前妻的未婚夫。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安·蜜拉-史提勒.md" title="wikilink">安·蜜拉-史提勒</a><br />
Anne Meara-Stiller</p></td>
<td><p>黛比<br />
Debbie</p></td>
<td><p><a href="../Page/職業介紹所.md" title="wikilink">職業介紹所員工</a>。安也是主演的親生母親。</p></td>
</tr>
</tbody>
</table>

**博物館**

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅賓·威廉斯.md" title="wikilink">羅賓·威廉斯</a><br />
Robin Williams</p></td>
<td><p><a href="../Page/西奧多·羅斯福.md" title="wikilink">西奧多·羅斯福總統</a><br />
Theodore Roosevelt</p></td>
<td><p>美國第26任總統<a href="../Page/蠟像.md" title="wikilink">蠟像</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐文·威爾森.md" title="wikilink">歐文·威爾森</a><br />
Owen Wilson</p></td>
<td><p><br />
Jedediah Strong Smith</p></td>
<td><p>西部牛仔模型（手指般大小）。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史蒂夫·庫根.md" title="wikilink">史蒂夫·庫根</a><br />
Steve Coogan</p></td>
<td><p><a href="../Page/屋大維.md" title="wikilink">屋大維</a><br />
Octavius</p></td>
<td><p>羅馬戰士模型（手指般大小）。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/派翠克·加拉格.md" title="wikilink">派翠克·加拉格</a><br />
Patrick Gallagher</p></td>
<td><p><a href="../Page/匈奴.md" title="wikilink">匈奴王</a><a href="../Page/阿提拉.md" title="wikilink">阿提拉</a><br />
Attila the Hun</p></td>
<td><p>匈人勇士塑像。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷米·馬利克.md" title="wikilink">雷米·馬利克</a><br />
Rami Malek</p></td>
<td><p><a href="../Page/門卡烏拉.md" title="wikilink">阿卡曼拉</a><br />
Ahkmenrah</p></td>
<td><p>古埃及<a href="../Page/法老王.md" title="wikilink">法老王木乃伊</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/須彌座·佩克.md" title="wikilink">須彌座·佩克</a><br />
Mizuo Peck</p></td>
<td><p><a href="../Page/薩卡加維亞.md" title="wikilink">莎卡嘉薇亞</a><br />
Sacagawea</p></td>
<td><p><a href="../Page/印地安.md" title="wikilink">印地安女子蠟像</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/捲尾猴.md" title="wikilink">捲尾猴</a>“水晶”<br />
Crystal the Monkey</p></td>
<td><p>德克斯特<br />
Dexter</p></td>
<td><p>猴子。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皮耶法蘭西斯柯·法維諾.md" title="wikilink">皮耶法蘭西斯柯·法維諾</a><br />
Pierfrancesco Favino</p></td>
<td><p><a href="../Page/克里斯多福·哥倫布.md" title="wikilink">克里斯多福·哥倫布</a><br />
Christopher Columbus</p></td>
<td><p>探險家雕像。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布萊德·賈奈特.md" title="wikilink">布萊德·賈奈特</a><br />
Brad Garrett</p></td>
<td><p><a href="../Page/復活島.md" title="wikilink">復活島</a><a href="../Page/摩艾石像.md" title="wikilink">摩艾石像</a><br />
The Easter Island Head</p></td>
<td><p>人頭石像（配音演出）。</p></td>
</tr>
<tr class="odd">
<td><p>馬丁·克里斯多佛<br />
Martin Christopher</p></td>
<td><p><a href="../Page/路易斯.md" title="wikilink">梅利維瑟·路易士</a><br />
Meriwether Lewis</p></td>
<td><p>美国探险家蠟像。</p></td>
</tr>
<tr class="even">
<td><p>馬丁·西姆斯<br />
Martin Sims</p></td>
<td><p><a href="../Page/威廉·克拉克.md" title="wikilink">威廉·克拉克</a><br />
William Clark</p></td>
<td><p>美国探险家蜡像。</p></td>
</tr>
</tbody>
</table>

## 博物館展品

  - [西奧多·羅斯福蠟像](../Page/西奧多·羅斯福.md "wikilink")
  - [霸王龙](../Page/霸王龙.md "wikilink")[骨](../Page/骨.md "wikilink")
  - [卷尾猴](../Page/卷尾猴.md "wikilink")
  - [哥倫布](../Page/哥倫布.md "wikilink")
  - [摩艾石像](../Page/摩艾石像.md "wikilink")
  - [阿提拉](../Page/阿提拉.md "wikilink")
  - [史前人类](../Page/史前人类.md "wikilink")
  - [屋大维](../Page/屋大维.md "wikilink")
  - [西部牛仔](../Page/西部牛仔.md "wikilink")
  - [埃及法老](../Page/埃及法老.md "wikilink")
  - [萨卡加维亚](../Page/萨卡加维亚.md "wikilink")
  - [劉易斯與克拉克遠征](../Page/劉易斯與克拉克遠征.md "wikilink")

## 同名电视节目

中国大陆的[湖南卫视于](../Page/湖南卫视.md "wikilink")2011年3月9日推出了电视版本，每周三凌晨00:05首播。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  -
  -
  -
  -
  -
  -
  - {{@movies|fnen60477347}}

  -
  -
[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美国喜劇片](../Category/美国喜劇片.md "wikilink")
[Category:美国奇幻冒險片](../Category/美国奇幻冒險片.md "wikilink")
[Category:20世纪福斯电影](../Category/20世纪福斯电影.md "wikilink")
[Category:歷史人物題材電影](../Category/歷史人物題材電影.md "wikilink")
[Category:博物館背景電影](../Category/博物館背景電影.md "wikilink")
[Category:肖恩·利维电影](../Category/肖恩·利维电影.md "wikilink")
[Category:博物馆奇妙夜](../Category/博物馆奇妙夜.md "wikilink")