**俾路支语**（Baluchi）是[巴基斯坦](../Page/巴基斯坦.md "wikilink")[俾路支族的民族语](../Page/俾路支人.md "wikilink")，分布在[俾路支地区](../Page/俾路支地区.md "wikilink")，属于[印欧语系](../Page/印欧语系.md "wikilink")[印度-伊朗语族的](../Page/印度-伊朗语族.md "wikilink")[伊朗语支](../Page/伊朗语支.md "wikilink")。

## 語音系統

### 元音

### 輔音

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/唇音.md" title="wikilink">唇音</a></p></th>
<th><p><a href="../Page/齿音.md" title="wikilink">齿音</a></p></th>
<th><p><a href="../Page/齿龈音.md" title="wikilink">齿龈音</a></p></th>
<th><p><a href="../Page/捲舌音.md" title="wikilink">捲舌音</a></p></th>
<th><p><a href="../Page/齒齦後音.md" title="wikilink">齒齦後音</a></p></th>
<th><p><a href="../Page/硬腭音.md" title="wikilink">硬腭音</a></p></th>
<th><p><a href="../Page/软腭音.md" title="wikilink">软腭音</a></p></th>
<th><p><a href="../Page/喉音.md" title="wikilink">喉音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>[1]</p></td>
<td></td>
<td></td>
<td><p>[2]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/颤音_(语音学).md" title="wikilink">颤音</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[3]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/近音.md" title="wikilink">近音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 文字

19世紀以前無文字。雖然官方口語使用俾路支語，但書面文字用[波斯語](../Page/波斯語.md "wikilink")。[大英帝國的語言學家和政治史學家曾用拉丁字母記錄俾路支語](../Page/大英帝國.md "wikilink")。[巴基斯坦獨立後](../Page/巴基斯坦.md "wikilink")，使用波斯阿拉伯字母書寫俾路支語。[阿富汗則使用書寫](../Page/阿富汗.md "wikilink")[普什圖語的那套字母拼寫](../Page/普什圖語.md "wikilink")。

## 關聯項目

  - [俾路支人](../Page/俾路支人.md "wikilink")
  - [俾路支方言](../Page/俾路支方言.md "wikilink")([Balochi
    dialects](../Page/:en:Balochi_dialects.md "wikilink"))

## 註釋

## 外部連結

  - [Baluchi glossary](http://dsal.uchicago.edu/dictionaries/mumtaz/) -
    a searchable, elementary-level Baluchi-English glossary
  - [EuroBalúči online translation
    tool](http://www.eurobaluchi.com/dictionary/index.htm) - translate
    Balochi words to or from English, Persian, Spanish, Finnish and
    Swedish

[Category:伊朗语支](../Category/伊朗语支.md "wikilink")
[Category:巴基斯坦語言](../Category/巴基斯坦語言.md "wikilink")

1.  Words with /ʒ/ are uncommon.
2.  Word-initial /h/ is dropped in Balochi as spoken in Karachi.
3.  The retroflex tap has a very limited distribution.