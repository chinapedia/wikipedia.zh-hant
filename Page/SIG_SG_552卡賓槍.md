**SIG SG 552**是[SIG SG
550的縮短版本](../Page/SIG_SG_550突擊步槍.md "wikilink")[卡賓槍](../Page/卡賓槍.md "wikilink")，由[Swiss
Arms](../Page/Swiss_Arms.md "wikilink")（前身為[SIG
Arms](../Page/SIG_Arms.md "wikilink")）制造。

## 設計

SIG SG
552發射[5.56×45毫米](../Page/5.56×45mm_NATO.md "wikilink")[北約標準步槍彈](../Page/北大西洋公约组织.md "wikilink")，裝有摺疊[槍托](../Page/槍托.md "wikilink")，9英吋的[槍管](../Page/槍管.md "wikilink")、[扳機護圈亦可翻開以便](../Page/扳機護環.md "wikilink")[手套時操作](../Page/手套.md "wikilink")，備有可裝配件的[戰術導軌](../Page/皮卡汀尼導軌.md "wikilink")。SIG
SG 552是SIG SG
550槍族一份子，同樣使用改良自[AK-47的導氣系统](../Page/AK-47突击步枪.md "wikilink")。

目前的SIG SG 552常見於瑞士軍隊及各國的特警隊，在瑞士不向民間發售。

## 使用國

  -
  -
  -
  -
  -
  -   - [法國國家憲兵干預組](../Page/國家憲兵干預組.md "wikilink")

      -
  -   - [德國聯邦警察第九國境守備隊](../Page/德國聯邦警察第九國境守備隊.md "wikilink")

  -
  -
  -
  -   - [治安警察局](../Page/治安警察局.md "wikilink")[特別行動組](../Page/特別行動組.md "wikilink")

  -
  -
  -   - 特警單位

  -
  -   - 反恐特警單位

  -
  -   - [瑞士武裝部隊特種部隊單位](../Page/瑞士軍事.md "wikilink")
      - 特警單位

  -   - 德比郡警察

  -   - [美國海軍特種作戰研究大隊](../Page/美國海軍特種作戰研究大隊.md "wikilink")
      - 部份地方警察局的[特種武器和戰術部隊](../Page/特種武器和戰術部隊.md "wikilink")

  -
  -   - [瑞士近衛隊](../Page/瑞士近衛隊.md "wikilink")

## 流行文化

### 電影

  - 2004年—《[-{zh-cn:生化危机2：启示录; zh-tw:惡靈古堡2：啟示錄;
    zh-hk:生化危機之殲滅生還者}-](../Page/惡靈古堡2：啟示錄.md "wikilink")》：被[保護傘公司和S](../Page/安布雷拉.md "wikilink").T.A.R.S.的隊員所使用。
  - 2010年—《[-{zh-cn:盗梦空间; zh-tw:全面啓動;
    zh-hk:潛行兇間}-](../Page/全面啓動.md "wikilink")》：雪地塗裝並附有[皮卡汀尼導軌](../Page/皮卡汀尼導軌.md "wikilink")，被伊姆斯（[湯姆·哈迪飾演](../Page/湯姆·哈迪.md "wikilink")）於第三層[夢境期間所使用](../Page/夢境.md "wikilink")。
  - 2011年—《》：裝有ACOG
    TA11[瞄準鏡](../Page/瞄準鏡.md "wikilink")、[戰術燈和](../Page/戰術燈.md "wikilink")[抑制器](../Page/抑制器.md "wikilink")。被一名法國海軍突擊隊隊員於[科索沃行動期間所使用](../Page/科索沃.md "wikilink")。
  - 2012年—《[-{zh-cn:生化危机5：惩罚; zh-tw:惡靈古堡5：天譴日;
    zh-hk:生化危機之滅絕真相}-](../Page/惡靈古堡5：天譴日.md "wikilink")》：裝上EO-TECH[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")，被里昂·史考特·甘迺迪（[約翰·厄柏飾演](../Page/約翰·厄柏.md "wikilink")）所使用。
  - 2012年ㄧ《[逆戰](../Page/逆戰.md "wikilink")》：被王尚恩（[安志傑飾演](../Page/安志傑.md "wikilink")）在約旦時所使用。
  - 2012年—《[-{zh-hk:轟天猛將2; zh-tw:浴血任務2;
    zh-cn:敢死队2;}-](../Page/敢死队2.md "wikilink")》：被桑族武裝份子所使用。
  - 2013年—《[-{zh-hk:白宮末日; zh-tw:白宮末日;
    zh-cn:惊天危机;}-](../Page/白宮末日.md "wikilink")》：被瓦迪姆所使用，後來被主角約翰·凱爾（[查寧·塔圖姆飾演](../Page/查寧·塔圖姆.md "wikilink")）所繳獲。
  - 2014年—《[-{zh-hk:轟天猛將3; zh-tw:浴血任務3;
    zh-cn:敢死队3;}-](../Page/敢死队3.md "wikilink")》：被托爾·羅德（[蘭迪·寇楚飾演](../Page/蘭迪·寇楚.md "wikilink")）所使用，裝有[全息瞄準鏡和](../Page/全息瞄準鏡.md "wikilink")[前握把](../Page/輔助握把.md "wikilink")。
  - 2017年—《[-{zh-cn:疾速特攻; zh-tw:捍衛任務2：殺神回歸; zh-hk:殺神John Wick
    2;}-](../Page/捍衛任務2：殺神回歸.md "wikilink")》：於故事後期被桑提諾·德安托紐（[里卡多·史卡馬西奧飾演](../Page/里卡多·史卡馬西奧.md "wikilink")）的其中一名保鏢所使用。

### 電子遊戲

  - 《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-](../Page/絕對武力.md "wikilink")》及其後續作品（《[-{zh-hans:零点行动;
    zh-hant:一觸即發;}-](../Page/絕對武力：一觸即發.md "wikilink")》、《[-{zh-hans:起源;
    zh-hant:次世代;}-](../Page/絕對武力：次世代.md "wikilink")》、[Online](../Page/絕對武力Online.md "wikilink")；而在《[全球攻勢](../Page/絕對武力：全球攻勢.md "wikilink")》中則被誤稱為“SG
    553”的[SIG SG
    556突擊步槍所取代](../Page/SIG_SG_556突擊步槍.md "wikilink")）：命名為「Krieg
    552」，武器模組採用鏡像佈局（右手持槍時槍機拉柄和拋殼口在左邊），配有1.5倍放大倍率的[ACOG光學瞄凖鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")，奇怪地為恐怖份子的專屬武器。
  - 2008年—《[AVA Online](../Page/戰地之王.md "wikilink")》：型號為SG 552
    Commando，被歸類為[衝鋒槍](../Page/衝鋒槍.md "wikilink")，在商城以歐元作為販賣，可進行改裝，與真槍比起來[射速偏高](../Page/射速.md "wikilink")，可能是因為AVA戰地之王的設計者當初認為[射速設定為](../Page/射速.md "wikilink")68才比較符合[衝鋒槍的性質](../Page/衝鋒槍.md "wikilink")。改版過後該槍械可進行升級，以SG556
    Commando的型號重新現身在商城，可進行改裝（部分零件偏向瞄準射擊能力強化，但是用過的玩家們認為這把槍無法與步槍相匹敵，且射程很短，只能用作近距離交戰），仍然保持高射速的優勢。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》：型号为SG 552
    Commando，命名为“SIG
    552”，使用30发[弹匣](../Page/弹匣.md "wikilink")。为工程兵专用武器，K点购买，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")、冲锋枪握把架）以及瞄准镜（[EoTech
    553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")）。
  - 2013年—《[-{zh-hans:收获日2;
    zh-hant:劫薪日2;}-](../Page/劫薪日2.md "wikilink")》（Payday
    2）：型號為SG
    552-2，附有[皮卡汀尼導軌以用作裝上各種戰術配件](../Page/皮卡汀尼導軌.md "wikilink")，命名為「Commando
    553」。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six: Siege）：命名為「552
    Commando」，附有[皮卡汀尼導軌](../Page/皮卡汀尼導軌.md "wikilink")。造型上使用20發彈匣卻可裝彈30發。被[德國聯邦警察第九國境守備隊和](../Page/德國聯邦警察第九國境守備隊.md "wikilink")“白面具”[恐怖份子所使用](../Page/恐怖份子.md "wikilink")。

## 参见

  - [SIG SG
    510](../Page/SIG_SG_510自動步槍.md "wikilink")[自动步枪](../Page/自动步枪.md "wikilink")（）
  - [SIG SG
    550](../Page/SIG_SG_550突擊步槍.md "wikilink")[突击步枪](../Page/突击步枪.md "wikilink")（）
  - [SIG SG 553](../Page/SIG_SG_553卡賓槍.md "wikilink")
  - [SIG SG 556](../Page/SIG_SG_556突擊步槍.md "wikilink")
  - [SIG 522LR](../Page/SIG_522LR半自動步槍.md "wikilink")
  - [SIG SG 540](../Page/SIG_SG_540突擊步槍.md "wikilink")
  - [SIG GL 5040](../Page/SIG_GL_5040附加型榴彈發射器.md "wikilink")

## 外部链接

  - —[Modern
    Firearms—SIG 550／551／552頁面](https://web.archive.org/web/20070611154129/http://world.guns.ru/assault/as25-e.htm)

  - —[SIG
    ARMS官方網頁](https://web.archive.org/web/20061015170824/http://www.sigarms.com/)

  - —[SIG 552使用手冊（PDF格式）](https://web.archive.org/web/20071007210510/http://www.sigarms.com/CustomerService/documents/SG552OM.pdf)

  - —D Boy Gun World（槍炮世界）—

      - [SG552/553
        Commando](http://firearmsworld.net/sigsauer/sg552/SG552.htm)
      - [SG552/553-LB](http://firearmsworld.net/sigsauer/sg552/sg552lb.htm)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:卡宾枪](../Category/卡宾枪.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:瑞士槍械](../Category/瑞士槍械.md "wikilink")