**蔣雅文**（，），生於[英屬香港](../Page/英屬香港.md "wikilink")，中學就讀[粉嶺官立中學](../Page/粉嶺官立中學.md "wikilink")，曾經為[香港](../Page/香港.md "wikilink")[流行](../Page/流行.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員及](../Page/演員.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")，並曾參與多部[電影和](../Page/電影.md "wikilink")[電視劇集](../Page/電視劇集.md "wikilink")，現居於[台灣](../Page/台灣.md "wikilink")[花蓮](../Page/花蓮市.md "wikilink")\[1\]。

## 簡介

蔣雅文曾是[英皇娛樂旗下歌手](../Page/英皇娛樂.md "wikilink")，2002年與[鄭希怡](../Page/鄭希怡.md "wikilink")、[劉思惠](../Page/劉思惠.md "wikilink")（已退出娛樂圈）組成[3T組合](../Page/3T_\(香港\).md "wikilink")。

2005年起，經常獲安排跟[李逸朗](../Page/李逸朗.md "wikilink")（Don）以情侶形象發行大碟、拍攝電影、廣告及出席活動，星運一直平平。

2007年起作獨立發展，翌年宣佈向[英皇娛樂請假半年](../Page/英皇娛樂.md "wikilink")，到[台灣學習設計](../Page/台灣.md "wikilink")。\[2\]

2009年，蔣雅文在[台北市](../Page/台北市.md "wikilink")[大安區](../Page/大安區_\(臺北市\).md "wikilink")[新生南路與朋友合資開設](../Page/新生南路.md "wikilink")[咖啡店及](../Page/咖啡店.md "wikilink")[服飾店](../Page/服飾店.md "wikilink")「Little
Secret」，以網名「文迪」活躍於網上，而且亦在其網誌中表明不會重返娛樂圈。她為了推廣「Little
Secret」，用網名「文迪」出現在台灣[綜藝節目](../Page/綜藝節目.md "wikilink")《[Welcome
外星人](../Page/Welcome_外星人.md "wikilink")》和《[我猜我猜我猜猜猜](../Page/我猜我猜我猜猜猜.md "wikilink")》。

2012年6月4日於「Little Secret」網站公佈店舖在該年12月23日結業。退出 Little
Sercet後，蔣雅文離開台北，搬到花蓮居住，及後創立自家服飾品牌
Kōgōsei。\[3\]\[4\]

2014年年中，蔣雅文為[索尼](../Page/索尼.md "wikilink")拍攝平面及電視[廣告](../Page/廣告.md "wikilink")。

2015年2月，蔣雅文與丈夫朱經雄（淺野經雄），在台北大同區赤峰街開設了一家服飾店，名為「Ferment Store」。

2015年11月22日，蔣雅文和[周柏豪出席](../Page/周柏豪.md "wikilink")《2015 Sony Photo Fest
《創攝紀》》活動時，蔣雅文另一位女攝影師合作，她指出事前兩人不准用文字交談，只能上傳相片溝通，從而得出這次的作品\[5\]。

2016年7月24日，蔣雅文在出席《香港書展》活動時，為首本相集加散文集《圖文不符》，原本只會寫出5萬字，後加8萬字，內容提及有關過往情史，但對丈夫朱經雄所過往的事並不太感興趣。

2016年8月24日，蔣雅文回香港，在中環開服飾店 \[6\]。

## 個人生活

2014年底，和朱經雄登記結婚\[7\]。

## 唱片

### 大碟

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱#</p></th>
<th style="text-align: left;"><p>單曲名稱</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/冬天的故事.md" title="wikilink">冬天的故事</a></p></td>
<td style="text-align: left;"><p><a href="../Page/紅音樂唱片.md" title="wikilink">紅音樂唱片</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p>2007年12月20日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>浮生若夢</li>
<li>空中愛人</li>
<li>這個展覽叫愛情</li>
<li>記得第一次嗎</li>
<li>青草最好是青色</li>
<li>懶貓</li>
<li>樓下雅座</li>
<li>爽約</li>
<li>傾通宵</li>
<li>毅行者</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>空中愛人MV</li>
<li>記得第一次嗎 MV</li>
</ol></td>
</tr>
</tbody>
</table>

### EP

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱#</p></th>
<th style="text-align: left;"><p>單曲名稱</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Other_Half.md" title="wikilink">Other Half</a></p></td>
<td style="text-align: left;"><p><a href="../Page/紅音樂唱片.md" title="wikilink">紅音樂唱片</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p>2007年4月20日</p></td>
<td style="text-align: left;"><p><strong>AVCD</strong></p>
<ol>
<li>Computer Data, Not Playable</li>
<li>白日夢 (MV)</li>
<li>盲目 (MV)</li>
<li>失散 (MV)</li>
<li>白日夢</li>
<li>盲目</li>
<li>失散</li>
<li>復仇記</li>
<li>一個人旅行</li>
<li>天不造美</li>
</ol></td>
</tr>
</tbody>
</table>

### 合輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯名稱#</p></th>
<th style="text-align: left;"><p>單曲名稱</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/少女蝶.md" title="wikilink">少女蝶</a><br />
（與<a href="../Page/鄭希怡.md" title="wikilink">鄭希怡及</a><a href="../Page/劉思惠.md" title="wikilink">劉思惠的合唱</a><a href="../Page/音樂專輯.md" title="wikilink">專輯</a>）</p></td>
<td style="text-align: left;"><p><a href="../Page/音樂家唱片.md" title="wikilink">音樂家唱片</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p>2002年9月17日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>相對濕度 (鄭希怡)</li>
<li>I Wanna Be (劉思惠)</li>
<li>寵物男孩 (蔣雅文)</li>
<li>花心機 (鄭希怡)</li>
<li>情人進修 (劉思惠)</li>
<li>短訊Summer Time(蔣雅文)</li>
<li>旋轉樂園 (三人合唱)</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/雙對論.md" title="wikilink">雙對論</a><br />
（與<a href="../Page/李逸朗.md" title="wikilink">李逸朗合輯</a>）</p></td>
<td style="text-align: left;"><p><a href="../Page/紅音樂唱片.md" title="wikilink">紅音樂唱片</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p>2003年10月10日</p></td>
<td style="text-align: left;"><p><strong>AVCD</strong></p>
<ol>
<li>Computer data, not playable</li>
<li>我蠢我認 (MV)</li>
<li>懶醒 (MV)</li>
<li>傷對論 (MV)</li>
<li><a href="../Page/超合金曲.md" title="wikilink">超合金曲</a> (MV)</li>
<li>傷對論</li>
<li>我蠢我認</li>
<li>傾偈</li>
<li>懶醒</li>
<li>在校園渡假</li>
<li>超合金曲</li>
<li>丁香花</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a><br />
（英皇群星）</p></td>
<td style="text-align: left;"><p><a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>新曲+精選</p></td>
<td style="text-align: left;"><p>2006年6月9日</p></td>
<td style="text-align: left;"><p><strong>AVCD</strong></p>
<ol>
<li>夢幻組合 (容祖兒/Twins/關智斌/吳浩康/文彼得/李逸朗&amp;蔣雅文/泳兒/鄭希怡)</li>
<li>摩登時代 (容祖兒)</li>
<li>一時無兩 (Twins)</li>
<li>愛回來 (謝霆鋒)</li>
<li>當玫瑰遇上真愛 (鄭希怡)</li>
<li>一切還好 (陳奕迅)</li>
<li>赤地雪 (容祖兒)</li>
<li>月光光 (關智斌)</li>
<li>超合金曲 (李逸朗&amp;蔣雅文)</li>
<li>新手 (吳浩康)</li>
<li>幼稚園 (Twins)</li>
<li>靈犀 (關智斌)</li>
<li>第三者 (吳浩康)</li>
<li>救生圈 (Twins)</li>
<li>阿拉伯市場 (鄭希怡)</li>
<li>天之驕女 (容祖兒)</li>
<li>懶醒 (李逸朗&amp;蔣雅文)</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/Rainy_Lover.md" title="wikilink">Rainy Lover</a><br />
（與<a href="../Page/李逸朗.md" title="wikilink">李逸朗合輯</a>）</p></td>
<td style="text-align: left;"><p><a href="../Page/紅音樂唱片.md" title="wikilink">紅音樂唱片</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p>2006年8月9日</p></td>
<td style="text-align: left;"><p><strong>AVCD</strong></p>
<ol>
<li>Computer data, not playable</li>
<li>傾心 (MV)</li>
<li>雨中的戀人們 (MV)</li>
<li>Messenger (MV)</li>
<li>白眼 (MV)</li>
<li>粉飾太平 (MV)</li>
<li>雪割花 (MV)</li>
<li>Messenger</li>
<li>白眼</li>
<li>傾心</li>
<li>雨中的戀人們</li>
<li>半路中途</li>
<li>相約在那座橋</li>
<li>緋聞男女</li>
<li>有天屋邨落雪</li>
<li>粉飾太平 (Don Solo)</li>
<li>雪割花 (Mandy Solo)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5th</p></td>
<td style="text-align: left;"><p><a href="../Page/冬之戀人.md" title="wikilink">Winter Lover</a><br />
（與<a href="../Page/李逸朗.md" title="wikilink">李逸朗合輯</a>）</p></td>
<td style="text-align: left;"><p><a href="../Page/紅音樂唱片.md" title="wikilink">紅音樂唱片</a><br />
<a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p>2006年8月9日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>風頭火勢</li>
<li>普世歡騰</li>
<li>少女的祈禱</li>
<li>再見Puppy Love</li>
<li>每隔兩秒</li>
<li>愛的故事上集</li>
<li>塞拉耶佛的羅密歐與茱麗葉</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>愛的故事上集</li>
<li>風頭火勢</li>
<li>少女的祈禱</li>
<li>普世歡騰</li>
</ol></td>
</tr>
</tbody>
</table>

## 派台歌

  - 白日夢
  - 失散
  - 盲目
  - 記得第一次嗎
  - 空中愛人
  - 樓下雅座

### 派台歌四台成績

| **派台歌曲成績**                                         |
| -------------------------------------------------- |
| 唱片                                                 |
| **2005年**                                          |
| [雙對論](../Page/雙對論.md "wikilink")                   |
| 雙對論                                                |
| 雙對論                                                |
| 雙對論                                                |
| 雙對論                                                |
| **2006年**                                          |
| [Rainy Lover](../Page/Rainy_Lover.md "wikilink")   |
| Rainy Lover                                        |
|                                                    |
| Rainy Lover                                        |
| [冬之戀人](../Page/冬之戀人.md "wikilink")                 |
| **2007年**                                          |
| [冬之戀人](../Page/冬之戀人.md "wikilink")                 |
| [Other Half](../Page/Other_Half.md "wikilink")     |
| Other Half                                         |
| Other Half                                         |
| [Winter Story](../Page/Winter_Story.md "wikilink") |
| **2008年**                                          |
| Winter Story                                       |
| Winter Story                                       |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 廣告演出

### 2002年

  - [恆生銀行](../Page/恆生銀行.md "wikilink")3輯（電視及平面廣告）
  - [百老匯](../Page/百老匯攝影器材.md "wikilink")[Reebok夏季宣傳](../Page/Reebok.md "wikilink")（平面廣告）
  - [爽浪無糖香口珠](../Page/爽浪.md "wikilink")（電視及平面廣告）
  - [Clean and Clear](../Page/Clean_and_Clear.md "wikilink") Clear
    Fairness Line（平面廣告）

### 2003年

  - [Clean and Clear](../Page/Clean_and_Clear.md "wikilink") Clear
    Fairness Line（香港及中國）（電視及平面廣告）
  - [新世界傳動網星Mobile](../Page/新世界傳動網.md "wikilink")（香港）（電視及平面廣告）
  - [Beauty Tech](../Page/Beauty_Tech.md "wikilink")
    蠶絲修護睫毛液及雪糕修護遮瑕霜（香港）（電視及平面廣告）
  - [Travis休閒運動鞋](../Page/Travis.md "wikilink")（香港）（平面廣告）

### 2004年

  - [修身堂纖型及月餅](../Page/修身堂.md "wikilink")（電視及平面廣告）
  - [Travis休閒運動鞋](../Page/Travis.md "wikilink")（平面廣告）
  - 瘦身教館「一樽消腩」瘦身飲品（平面廣告）

### 2005年

  - [Zaap運動鞋](../Page/Zaap.md "wikilink")（香港）（平面廣告）
  - [Password](../Page/Password.md "wikilink") 休閒服（香港）（平面廣告）
  - [法國婚紗](../Page/法國.md "wikilink")（香港）（平面廣告）
  - [新世界傳動網](../Page/新世界傳動網.md "wikilink") 星Mobile（香港）（電視及平面廣告）
  - [Botox瘦面](../Page/Botox.md "wikilink")（香港）（平面廣告）

### 2006年

  - Budweiser 2006世界盃（香港）（電視及平面廣告）
  - Zaap 休閒運動鞋（香港）
  - Password 休閒服（香港）
  - 法國婚紗（香港）
  - Botox瘦面（香港）（平面廣告）

### 2007年

  - J\&J 1-Day Acuvue Define隱形眼鏡

### 2008年

  - [煤氣](../Page/香港中華煤氣.md "wikilink")（電視及平面廣告，與[洪卓立合作](../Page/洪卓立.md "wikilink")）

### 2014年

  - [Sony](../Page/索尼.md "wikilink") α5100相機（電視及平面廣告）

## 電影演出

  - 《[千機變](../Page/千機變.md "wikilink")》（[張承勷及龍比意監制](../Page/張承勷.md "wikilink")、林超賢導演，2003年6月24日上映）教堂房仲業者
  - 《[安娜與武林](../Page/安娜與武林.md "wikilink")》（陳慶嘉及羅國強監制、葉偉民導演，2003年12月24日上映）飾峨嵋細飛
  - 《[新警察故事](../Page/新警察故事.md "wikilink")》（陳自強及蘇志鴻監製、陳木勝導演，2004年9月24日上映）飾女警
  - 《[純白的聲音](../Page/純白的聲音.md "wikilink")》（林子皓導演，未上映）
  - 《[A貨B貨](../Page/A貨B貨.md "wikilink")》（黃家輝導演，沒有上映，只出碟）
  - 《[寶貝計劃](../Page/寶貝計劃.md "wikilink")》（陳木勝導演，2006年9月29日上映）飾三妹
  - 《[人在江湖](../Page/人在江湖.md "wikilink")》（邱禮濤編劇；第三十一屆香港國際電影節作品；沒有上映，只出碟）
  - 《[性工作者十日談](../Page/性工作者十日談.md "wikilink")》（邱禮濤及楊漪珊編劇；第三十一屆香港國際電影節作品，2007年5月17日上映）飾
    Nana
  - 《[惡男事件](../Page/惡男事件.md "wikilink")》（李光耀導演，2008年1月4日上映）飾Charlie
  - 《[黑道之無悔今生](../Page/黑道之無悔今生.md "wikilink")》（梁鴻華導演，2008年1月10日上映）
  - 《[十分鍾情](../Page/十分鍾情.md "wikilink")》（2008年9月4日上映）
  - 《[異空危情](../Page/異空危情.md "wikilink")》 (2010年12月10日上映)

## 電視演出

  - [港台劇集](../Page/香港電台.md "wikilink")《[Y2K+01](../Page/Y2K+01.md "wikilink")》
  - [無綫電視劇](../Page/無綫電視.md "wikilink")《[怪俠一枝梅](../Page/怪俠一枝梅_\(電視劇\).md "wikilink")》（2004年1月播放）
  - now.com網劇《[一起喝采](../Page/一起喝采.md "wikilink")》飾 Monique（2003年7月播放）
  - 寰宇電視劇《[功夫足球](../Page/功夫足球.md "wikilink")》（2004年12月於TVB播放）
  - 《[伙頭智多星](../Page/伙頭智多星_\(電視劇\).md "wikilink")》
  - 《[上海灘之俠醫傳奇](../Page/上海灘之俠醫傳奇.md "wikilink")》（2011年10月3日於aTV播放）
  - 港台劇集《[鐵窗邊緣](../Page/鐵窗邊緣.md "wikilink")》（2004年11月播放）
  - 港台戲劇節目《[十封信](../Page/十封信.md "wikilink")》（2005年7月播放）
  - 港台戲劇節目《[積金創未來](../Page/積金創未來.md "wikilink")》（第五集）（2005年4月播放）
  - 無綫電視劇《[奇幻潮](../Page/奇幻潮.md "wikilink")》－ 情比金堅
  - 港台劇集《[女人多自在III](../Page/女人多自在III.md "wikilink")》（客串）

## 曾參與音樂會及演唱會列表

  - 3T帶動少女夢飛行音樂會（[大學會堂](../Page/大學會堂.md "wikilink")）
  - 英皇三週年慈善演唱會（[香港紅磡體育館](../Page/香港紅磡體育館.md "wikilink")）
  - 星Mobile「相」事偶像Show（[香港會議展覽中心](../Page/香港會議展覽中心.md "wikilink")）
  - Budweise Bar
    Pacific迎接FIFA世界盃2006演唱會（九龍灣[國際展貿中心](../Page/國際展貿中心.md "wikilink")
    21/05/2006）

## 曾獲獎項

### 2002年

  - PM新勢力頒獎典禮——PM新世代玉女新人
  - 世界相機博覽會——2002焦點新星
  - [IFPI](../Page/IFPI.md "wikilink")[香港唱片銷量大獎](../Page/香港唱片銷量大獎.md "wikilink")2002——最暢銷本地新人組合（3T）
  - [壹本便利娛樂新人王](../Page/壹本便利.md "wikilink")2002頒獎禮——最合眼緣大獎

### 2005年

  - 學勢力x Friday大專生熱愛歌手選——大專生最熱愛型格女新人獎
  - 學勢力x Friday大專生熱愛歌手選——大專生最熱愛新組合獎
  - PM 05樂壇頒獎禮——PM 校園強勢合拍組合
  - PM 05樂壇頒獎禮——PM 用心演繹女歌手
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2005——勁爆新登場組合

### 2006年

  - [RoadShow至尊音樂頒獎禮](../Page/RoadShow至尊音樂頒獎禮.md "wikilink")2005——RoadShow至尊女新人
  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")2006——勁爆組合
  - [新城勁爆兒歌頒獎禮](../Page/新城勁爆兒歌頒獎禮.md "wikilink")2006——新城勁爆兒歌（在校園渡假）
  - [兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")2006——十大兒歌金曲（在校園渡假）

### 2007年

  - 2006年度[RoadShow至尊音樂頒獎禮](../Page/RoadShow至尊音樂頒獎禮.md "wikilink")——至尊合唱歌曲
    《Messenger》
  - 2007年度[PM樂壇頒獎禮](../Page/PM樂壇頒獎禮.md "wikilink")——PM2007年度清新人氣女歌手
  - 2007年度[新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink") ──新城勁爆合唱歌曲 《風頭火勢》
  - 2007年度IFPI香港唱片銷量大獎 —— 最暢銷本地女新人

## 攝影集

  - 2007年 《在》
  - 2010年 《樂活·道》

## 曾擔任大使列表

  - 2004年
    [東華三院委任為](../Page/東華三院.md "wikilink")《[東華三院學校健康大使](../Page/東華三院.md "wikilink")》
  - 2005年 [九龍樂善堂委任為](../Page/九龍樂善堂.md "wikilink")「榮譽甜心大使」
  - 2005年 「傷健共融·清新空氣大行動」 － 清新空氣大使
  - 2005年 [長者安居服務協會](../Page/長者安居服務協會.md "wikilink") － 愛心大使
  - 2007年
    香港[黏多醣症暨罕有遺傳病互助小組委任](../Page/黏多醣症.md "wikilink")「黏多醣症及罕有遺傳病關懷大使」

## 參考

## 外部連結

  - [化妝分享部落格](http://80sgirls.blogspot.hk/)

  -
  - [blogspot個人部落格](http://ngaman922.blogspot.tw/)

  -
  -
[Ya](../Category/蔣姓.md "wikilink")
[Category:台灣裔香港人](../Category/台灣裔香港人.md "wikilink")
[Category:粉嶺官立中學校友](../Category/粉嶺官立中學校友.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:前英皇藝人](../Category/前英皇藝人.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")

1.
2.
3.  [遇見第三波香港移民潮](http://hk.bbwc.cn/1ck2qd.html)
    ，歐佩佩、范榮靖、呂陽，[彭博商业周刊中文版](../Page/彭博商业周刊.md "wikilink")，2013年11月

4.

5.  [愛與女友selfie　周柏豪未必操刀結婚相（其中有關蔣雅文）](http://hk.on.cc/hk/bkn/cnt/entertainment/20151122/bkn-20151122184632400-1122_00862_001.html)[on.cc](../Page/on.cc.md "wikilink")
    2015年11月22日

6.  [蔣雅文寫書爆情史：老公睇到皺眉](http://hk.on.cc/hk/bkn/cnt/entertainment/20160724/bkn-20160724161046127-0724_00862_001.html?eventsection=hk_entertainment&eventid=402883474e4e5218014e70995b113363)[on.cc](../Page/on.cc.md "wikilink")
    2016年7月25日

7.