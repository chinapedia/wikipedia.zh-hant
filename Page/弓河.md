[Bow-river-banff-np.jpg](https://zh.wikipedia.org/wiki/File:Bow-river-banff-np.jpg "fig:Bow-river-banff-np.jpg")。\]\]
**弓河**（）是[加拿大](../Page/加拿大.md "wikilink")[艾伯塔的一条河流](../Page/艾伯塔.md "wikilink")。它是[南萨斯喀彻温河的一条支流](../Page/南萨斯喀彻温河.md "wikilink")，并被认为是[纳尔逊河的源头](../Page/纳尔逊河.md "wikilink")。\[1\]

弓河的名字来源于河流两岸的[芦苇](../Page/芦苇.md "wikilink")，这些芦苇被当地的[第一民族用作制作弓](../Page/第一民族.md "wikilink")；[佩甘](../Page/佩甘.md "wikilink")（Peigan）民族将河流取名为"Makhabn"，意为“制作弓的野草生长的河流”。\[2\]

## 流经路线

[Saskatchewanrivermap.png](https://zh.wikipedia.org/wiki/File:Saskatchewanrivermap.png "fig:Saskatchewanrivermap.png")
弓河起源于加拿大[落基山脉的](../Page/落基山脉.md "wikilink")[弓河冰川和](../Page/弓河冰川.md "wikilink")[弓湖](../Page/弓湖.md "wikilink")，向南流至[路易斯湖](../Page/路易斯湖_\(艾伯塔\).md "wikilink")，然后转而向东，相继穿过[班夫镇和](../Page/班夫镇.md "wikilink")[坎莫尔](../Page/坎莫尔.md "wikilink")。之后汇入[科克伦上方的](../Page/科克伦.md "wikilink")[戈斯特湖水库](../Page/戈斯特湖.md "wikilink")，继续向东到达[卡尔加里](../Page/卡尔加里.md "wikilink")。随后弓河和[Oldman河在阿尔伯塔南部汇合](../Page/Oldman河.md "wikilink")，之后汇入[南萨斯喀彻温河](../Page/南萨斯喀彻温河.md "wikilink")。南萨斯喀彻温河的河水通过[萨斯喀彻温河](../Page/萨斯喀彻温河.md "wikilink")、[溫尼伯湖和](../Page/溫尼伯湖.md "wikilink")[纳尔逊河](../Page/纳尔逊河.md "wikilink")，最终流入[哈德逊湾](../Page/哈德逊湾.md "wikilink")。

弓河两岸的城镇保括[路易斯湖](../Page/路易斯湖_\(艾伯塔\).md "wikilink")、[班夫镇](../Page/班夫镇.md "wikilink")、[坎莫尔](../Page/坎莫尔.md "wikilink")、[科克伦](../Page/科克伦.md "wikilink")、[卡尔加里](../Page/卡尔加里.md "wikilink")、[阿罗伍德](../Page/阿罗伍德.md "wikilink")。

弓河全长587公里，流域面积为26,200平方公里。\[3\]

## 参考资料

[Category:班夫国家公园](../Category/班夫国家公园.md "wikilink")

1.  加拿大地图，[《Rivers in
    Canada》（加拿大河流）](http://atlas.nrcan.gc.ca/site/english/learningresources/facts/rivers.html)
    ，于2008年2月2日查阅。

2.  [《About the Bow
    River》（弓河介绍）](http://www.bowriverkeeper.org/about-river)
    ，于2008年2月2日查阅。

3.