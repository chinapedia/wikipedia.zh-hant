****是历史上第一次[航天飞机任务](../Page/航天飞机.md "wikilink")，于1981年4月12日发射。在这次54.5小时的任务中，[哥伦比亚号航天飞机环绕地球](../Page/哥伦比亚号航天飞机.md "wikilink")36周。还是1975年7月的[阿波罗-联盟测试计划之后的第一次美国太空任务](../Page/阿波罗-联盟测试计划.md "wikilink")。STS-1是美国目前唯一一次在一种新的航天系统第一次发射就进行载人实验的任务。

## 任务成员

  - **[约翰·杨](../Page/约翰·杨.md "wikilink")**（，曾执行[双子星3号](../Page/双子星3号.md "wikilink")、[10号](../Page/双子星10号.md "wikilink")、[阿波罗10号](../Page/阿波罗10号.md "wikilink")、[16号](../Page/阿波罗16号.md "wikilink")、以及任务），指令长
  - **[罗伯特·克里彭](../Page/罗伯特·克里彭.md "wikilink")**（，曾执行、、以及任务），飞行员

### 替补成员

<small>替补成员同样接受任务训练，在主力成员因各种原因无法执行任务时接替。</small>
<small>的替补团队执行了任务。</small>

  - **[约瑟夫·恩格](../Page/约瑟夫·恩格.md "wikilink")**（，曾执行以及任务），指令长
  - **[理-{查}-德·特鲁利](../Page/理查德·特鲁利.md "wikilink")**（，曾执行以及任务），飞行员

[Category:1981年佛罗里达州](../Category/1981年佛罗里达州.md "wikilink")
[Category:1981年科学](../Category/1981年科学.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1981年4月](../Category/1981年4月.md "wikilink")