**Smart
1**或**SMART-1**是[欧洲空间局一个借助](../Page/欧洲空间局.md "wikilink")[太阳能](../Page/太阳能.md "wikilink")[离子推进器进入](../Page/离子推进器.md "wikilink")[月球](../Page/月球.md "wikilink")[轨道的环月](../Page/轨道.md "wikilink")[人造卫星](../Page/人造卫星.md "wikilink")，该探测器由[瑞典负责设计](../Page/瑞典.md "wikilink")，于2003年9月27日23时14分（[UTC](../Page/协调世界时.md "wikilink")）发射升空。'"SMART'"是**用于先进技术研究的小型任务**（**S**mall
**M**issions for **A**dvanced **R**esearch in **T**echnology）的缩写。（[LISA
Pathfinder是SMART](../Page/LISA_Pathfinder.md "wikilink")-2的另一个名字，计划其将于2008年发射升空）SMART-1是欧洲第一个飞向[月球的](../Page/月球.md "wikilink")[太空飞船](../Page/太空飞船.md "wikilink")。该飞船于2006年9月3日5时42分（[UTC](../Page/协调世界时.md "wikilink")）成功撞击月球表面，为它的探月任务划上句号。

## 飞行器设计

“SMART-1”属于一台轻量级的探测器，它长约1[米](../Page/米.md "wikilink")，发射时重367[公斤](../Page/公斤.md "wikilink")（807.4磅），当中有287公斤（631.4磅）为非推进物质。

它使用了由太阳能推动的[霍尔效应推进器作为动力来源](../Page/霍尔效应推进器.md "wikilink")，在发射时它携带了50公升（82公斤，气压150巴）的液态惰性[氙作推进剂](../Page/氙.md "wikilink")。这些推进器使用了[静电场把氙原子变成](../Page/电场.md "wikilink")[离子](../Page/离子.md "wikilink")，并把这些氙离子加至高速，使它的[离子推进器之](../Page/离子推进器.md "wikilink")[比冲值达至](../Page/比冲.md "wikilink")16.1 kN·s/kg（1640秒），为传统化学火箭的三倍以上，因此1公斤的推进剂（占探测器总重量的350到300分之一）可产生出约每秒45米的速度变化（[ΔV](../Page/ΔV.md "wikilink")）。推进器重29公斤，最高功率为1,200瓦。

探测器上的太阳能集光板为推进器提供1,190瓦的功率，使它能保持68
mN的推力，加速度为0.2 mm/s²（每小时0.7 m/s）。由于它是全离子推动，因此不能以短时间喷射来[变轨](../Page/变轨.md "wikilink")，需要慢慢地进行。

SMART-1的总造价也相当低，只有1[亿](../Page/亿.md "wikilink")1000万[欧元](../Page/欧元.md "wikilink")（1亿2600万[美元](../Page/美元.md "wikilink")）.
SMART-1是[欧洲空间局关于建造比类似](../Page/欧洲空间局.md "wikilink")[美国](../Page/美国.md "wikilink")[NASA但更小更便宜的](../Page/NASA.md "wikilink")[太空飞船计划的一部分](../Page/太空飞船.md "wikilink")。

## 任务

作为"用于先进技术研究的小型任务"计划的一部分，SMART-1将会测试新的太空飞行技术。SMART-1主要目的是测试太阳能离子推进器。它也将测试小型科学设备的使用，这些设备可能更为有效。如果成功，这些技术将会在将来的ESA任务中使用。

任务的第二个目标是获取关于月球的更多信息，例如其是如何形成的这类问题。SMART-1将会对月球表面进行[X射线和](../Page/X射线.md "wikilink")[红外线遥感采样绘制地图](../Page/红外线.md "wikilink")，从不同的角度拍摂图片并依此即可建立月球表面地图的三维模型。它还将使用X射线[分光镜决定月球的化学组成](../Page/分光镜.md "wikilink")。一个特别的目标是使用红外线搜寻月球南极固态[水的存在](../Page/水.md "wikilink")，那里从未被太阳辐射直接照射过。它也将对月球的[Peak
of Eternal
Light进行地图采样](../Page/Peak_of_Eternal_Light.md "wikilink")，该地形的山顶永久性的曝露在太阳辐射之下，而周遭的环形山则永远出于阴影中。

## 飞行

SMART-1在2003年9月27日与[INSAT](../Page/印度太空研究组织#INSAT.md "wikilink")-3E以及[e-BIRD使用](../Page/Eutelsat.md "wikilink")[亞利安5號運載火箭火箭在](../Page/亞利安5號運載火箭.md "wikilink")[法属圭亚那的](../Page/法属圭亚那.md "wikilink")[Kourou一起发射升空](../Page/Kourou.md "wikilink")。其发射後42分钟即进入654
x 35 885
km的离地静止轨道。在那里，SMART-1将在13个月中使用SEPP逐渐螺旋脱离。2004年11月11日它穿过[拉格朗日点](../Page/拉格朗日点.md "wikilink")
L<sub>1</sub>并且进入月球[重力影响区域](../Page/重力.md "wikilink")，在[UT时间](../Page/UT.md "wikilink")11月15日1748其穿越了月球轨道的第一个[近月点](../Page/近月点.md "wikilink")。

欧洲空间局于2005年2月15日宣布，已经批准了关于延长一年SMART-1的使命到2006年8月的提案。

## 撞擊

## 重要事件与发现

  - 2004年6月17日：SMART-1使用传回了对[地球照下的测试图像](../Page/地球.md "wikilink")，该相机将用于对月球的近距离拍照。这张地球照片显示了[欧洲和](../Page/欧洲.md "wikilink")[非洲的部分](../Page/非洲.md "wikilink")。它是在5月21日使用AMIE相机照下的，AMIE是一个重量只有450[克的图像设备](../Page/克.md "wikilink")。
  - 2004年11月2日：绕行地球轨道的最後一个近地点。
  - 2004年11月15日：绕行月球轨道的第一个近月点。
  - 2005年1月26日：发回月球表面第一个近距离图像。
  - 2005年2月27日：达到最终的环月轨道，轨道周期为5小时。
  - 2006年9月3日：成功撞擊月球表面，任務結束。

## 參見

  - [太陽系探測器列表](../Page/太陽系探測器列表.md "wikilink")

## 外部链接

  - [ESA的SMART-1主页](http://sci.esa.int/science-e/www/area/index.cfm?fareaid=10)
  - [新浪新闻
    欧洲探测器SMART-1号成功撞击月球专题](http://tech.sina.com.cn/d/focus/smart-1/index.shtml)

[Category:月球航天器](../Category/月球航天器.md "wikilink")
[Category:歐洲太空總署](../Category/歐洲太空總署.md "wikilink")
[Category:霍爾效應](../Category/霍爾效應.md "wikilink")