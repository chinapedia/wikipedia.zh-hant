**GeForce**（中國大陸又稱**精视**），是由[英伟达公司開發的](../Page/英伟达.md "wikilink")[個人電腦的](../Page/個人電腦.md "wikilink")[圖形處理器的品牌](../Page/圖形處理器.md "wikilink")。第一款GeForce產品是為高邊際利潤（高端）遊戲社群和計算機用戶的市場設計開發的，但是後來的產品發佈擴展了產品線，覆蓋圖形市場的所有細分市場，從低端、中端到高端。到2013年為止，GeForce的設計已經包含16個世代。它现在的竞争對手是[AMD的](../Page/AMD.md "wikilink")[Radeon系列](../Page/Radeon.md "wikilink")[圖形處理器](../Page/圖形處理器.md "wikilink")。NVIDIA亦擁有定位於專業圖形處理領域的[Quadro系列](../Page/Quadro.md "wikilink")，多數產品使用與GeForce相同的核心，GeForce的早期產品甚至可以藉由改刷[韌體的方式軟改為Quadro系列](../Page/韌體.md "wikilink")。

英伟达通常會為一個新的系列，研發一個旗艦級產品。然後將其的功能（如核心、記憶體）削減，成為中端或低端產品。

## 名称

“GeForce”这个名字源于Nvidia在1999年初举办的名为“Name That Chip”的比赛。该公司呼吁公众为[RIVA
TNT2系列显卡的继任者命名](../Page/RIVA_TNT2.md "wikilink")。在收到的超过12,000个参赛作品中，7名获奖者获得了RIVA
TNT2 Ultra显卡作为奖励。\[1\] \[2\]

## 恶搞

[NVIDIA GeForce
500系列曾出现由于设计供电不足在超频时发生爆炸的现象](../Page/GeForce_500.md "wikilink")，\[3\]\[4\]\[5\]\[6\]因此被网友经常恶搞讽刺，甚至比喻为核弹，也引发了对[黃仁勳的恶搞](../Page/黃仁勳.md "wikilink")。

2013年12月11日，[中国大陆甘肃卫视的](../Page/中国大陆.md "wikilink")《解密真相》节目中，主持人在介绍火箭推进榴弹时，一字不差地引用了[百度百科](../Page/百度百科.md "wikilink")“航母杀手”词条中网友对于[英伟达](../Page/英伟达.md "wikilink")[Geforce系列显卡](../Page/Geforce.md "wikilink")[GTX690的](../Page/NVIDIA_GeForce_600.md "wikilink")[恶搞评论](../Page/恶搞文化.md "wikilink")，以举例佐证[航空母舰的脆弱](../Page/航空母舰.md "wikilink")，引起舆论哗然。\[7\]\[8\]

## 完整型號列表

## 相關條目

  - [NVIDIA Quadro](../Page/NVIDIA_Quadro.md "wikilink")
  - [NVIDIA Tesla](../Page/NVIDIA_Tesla.md "wikilink")

## 参考资料

## 外部連結

  - [英伟达GeForce®（精视™）中国大陆官方网站](http://www.GeForce.cn)

  - [NVIDIA GeForce
    台湾官方網站](http://www.nvidia.com.tw/object/geforce_family_tw.html)

[Category:英伟达](../Category/英伟达.md "wikilink")

1.
2.
3.  [GTX590-加压-超频-爆炸-烧毁！ - DIY经验谈 - 读编互动 - 话题 - slan7777 - 我的空间 -
    微型计算机官方网站](http://member.mcplive.cn/space.php?uid=10442&do=thread&id=5919)
4.  [NV官方回应：GTX590“烧毁门”事件](http://diy.pconline.com.cn/front/1103/2371820.html)
    - 太平洋電腦網
5.  [NVIDIA官方回应GeForce
    GTX 590烧毁问题](http://news.mydrivers.com/1/189/189502.htm) -
    驅動之家
6.  [视频:
    nVIDIA新旗舰GTX590显卡烧毁实录，转自Youtube](http://v.youku.com/v_show/id_XMjUzNjc0OTQw.html)
7.  [甘肃卫视，某节目主持人念稿子被坑，说成了690战术核显卡](http://www.bilibili.com/video/av870268/)
8.  [690战术核显卡？人类已经无法阻止N黑了](http://news.mydrivers.com/1/286/286328.htm)，驱动之家