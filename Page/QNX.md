**QNX**是商業[類Unix實時作業系統](../Page/類Unix.md "wikilink")，主要針對嵌入式系統市場。該產品開發於20世紀80年代初，後來改名為QNX軟件系統公司，公司已被[黑莓公司併購](../Page/黑莓公司.md "wikilink")。

## 功能简述

QNX採取[微核心架構](../Page/微核心.md "wikilink")，作業系統中的多數功能是以許多小型的task來執行，它們被稱為server。這樣的架構使得用戶和開發者可以關閉不需要的功能，而不需要改​​變作業系統本身。

QNX Neutrino（2001）已经被移植到许多平台并且运行在嵌入式市场中使用的各种现代处理器上，如PowerPC和x86。

QNX为学术界以及非商业用途的用户提供了一个特殊的许可。

QNX的應用範圍極廣，包含了：控制[保時捷跑車的音樂和媒體功能](../Page/保時捷.md "wikilink")、[福特汽車的](../Page/福特汽車.md "wikilink")[SYNC
3車載系統](../Page/SYNC_3.md "wikilink")、[核電站和](../Page/核電站.md "wikilink")[美國陸軍無人駕駛Crusher坦克的控制系統](../Page/美國陸軍.md "wikilink")，還有[BlackBerry
PlayBook和作業系統](../Page/BlackBerry_PlayBook.md "wikilink")。\[1\]

## 历史

1980年，加拿大滑鐵盧大學的學生戈登貝爾和丹道奇都上了作業系統設計的課程\[2\]，課程要求學生構建一個基本的實時內核。

2007年9月，QNX軟件系統公司發布其部分源代碼的可用性。\[3\]

2010年4月，Research In Motion公司發布將收購QNX軟件系統公司。

2010年9月，Research In Motion公司發布[BlackBerry
PlayBook和作業系統](../Page/BlackBerry_PlayBook.md "wikilink")，基於QNX。\[4\]

## 技术细节

雖然QNX本身並不屬於[UNIX](../Page/UNIX.md "wikilink")，但由於其提供了[POSIX的支援](../Page/POSIX.md "wikilink")，使得多數傳統UNIX程式在微量修改（甚至不需修改）後即可在QNX上面編譯與執行。

## 参考文献

## 外部链接

  - [QNX网络资源](http://mama.indstate.edu/users/liug/qnx4.html)
  - [QNXZone](https://web.archive.org/web/20060616015327/http://www.qnxzone.com/)
  - [开源应用](http://www.sf.net/projects/openqnx)

{{-}}

[Category:计算平台](../Category/计算平台.md "wikilink")
[Category:操作系统](../Category/操作系统.md "wikilink")
[Category:Unix](../Category/Unix.md "wikilink")
[Category:实时操作系统](../Category/实时操作系统.md "wikilink")
[Category:嵌入式操作系统](../Category/嵌入式操作系统.md "wikilink")
[Category:行動作業系統](../Category/行動作業系統.md "wikilink")
[Category:微內核](../Category/微內核.md "wikilink")
[Category:黑莓手機](../Category/黑莓手機.md "wikilink")

1.
2.  CS350: <https://www.student.cs.uwaterloo.ca/~cs350>
3.  QNX Press Releases: [source code
    availability](http://www.qnx.com/news/pr_2471_1.html)
4.  [RIM Unveils The BlackBerry
    PlayBook](http://www.marketwire.com/press-release/RIM-Unveils-The-BlackBerry-PlayBook-NASDAQ-RIMM-1325727.htm),
    official press release, September 27, 2010