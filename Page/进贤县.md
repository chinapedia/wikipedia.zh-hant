**进贤县**是[中国](../Page/中华人民共和国.md "wikilink")[江西省](../Page/江西省.md "wikilink")[南昌市下辖的一个](../Page/南昌市.md "wikilink")[县](../Page/县.md "wikilink")，位于江西省中部偏北，鄱阳湖南岸，抚河与信江下游汇合处，具有1700多年的建县历史。面积为1952平方公里，人口约为84万。

## 历史

进贤原名[钟陵县](../Page/钟陵县_\(西晋\).md "wikilink")，始建于西晋[太康元年](../Page/太康_\(西晋\).md "wikilink")（208年），属[豫章郡](../Page/豫章郡.md "wikilink")。不久被废。南朝梁复置，隋初又废。唐[武德五年](../Page/武德.md "wikilink")(622年)再置，属[洪州](../Page/洪州_\(隋朝\).md "wikilink")。八年废，入[南昌县](../Page/南昌县.md "wikilink")。北宋改为进贤镇，[崇宁二年](../Page/崇宁.md "wikilink")（1103年）升南昌县进贤镇为进贤县，因境内有[栖贤山](../Page/栖贤山.md "wikilink")，唐抚州刺史[戴叔伦携家居此得名](../Page/戴叔伦.md "wikilink")\[1\]。从此，进贤县制开始稳定延续：元属[龙兴路](../Page/龙兴路.md "wikilink")，明属[南昌府](../Page/南昌府.md "wikilink")，清属[南昌府](../Page/南昌府.md "wikilink")，民国属[豫章道](../Page/豫章道.md "wikilink")，中共上台初属[南昌地区](../Page/南昌地区.md "wikilink")，1958年改属[宜春地区](../Page/宜春地区.md "wikilink")，1968年再改属[抚州地区](../Page/抚州地区.md "wikilink")，1983年划回[南昌市至今](../Page/南昌市.md "wikilink")。

设县之始，距今已近1800多年的悠久历史，文化底蕴深厚。境内分布着罗溪古民居群、文港晏殊故里、文港华夏笔都、七里明代昼锦坊、李渡元代烧酒窖遗址、赵埠古石塔、钟陵牌坊、金山寺、静乐寺等诸多名胜古迹。

李渡元代酒窖遗址，因破解了中国白酒酿造之谜，被列为中国2002年十大重大考古挖掘之一。

### 名人

涌现出如南唐画家[巨然](../Page/巨然.md "wikilink")、[徐熙](../Page/徐熙_\(五代十国\).md "wikilink")、[董源](../Page/董源.md "wikilink")；北宋宰相著名词人[晏殊](../Page/晏殊.md "wikilink")、[晏几道父子](../Page/晏几道.md "wikilink")；明代状元[舒芬](../Page/舒芬.md "wikilink")；晚清著名画家、教育家、[两江师范学堂](../Page/两江师范学堂.md "wikilink")（[东南大学前身](../Page/东南大学.md "wikilink")）校长[李瑞清](../Page/李瑞清.md "wikilink")；现代著名小提琴家[盛中国等众多杰出名人](../Page/盛中国.md "wikilink")。

## 行政区划

2013年，进贤县辖9个镇、11个乡：

  - 镇：[民和镇](../Page/民和镇.md "wikilink")、[李渡镇](../Page/李渡镇.md "wikilink")、[温圳镇](../Page/温圳镇.md "wikilink")、[文港镇](../Page/文港镇.md "wikilink")、[梅庄镇](../Page/梅庄镇.md "wikilink")、[张公镇](../Page/张公镇.md "wikilink")、[罗溪镇](../Page/罗溪镇.md "wikilink")、[架桥镇](../Page/架桥镇.md "wikilink")、[前坊镇](../Page/前坊镇.md "wikilink")
  - 乡：[三里乡](../Page/三里乡.md "wikilink")、[二塘乡](../Page/二塘乡.md "wikilink")、[钟陵乡](../Page/钟陵乡.md "wikilink")、[池溪乡](../Page/池溪乡.md "wikilink")、[南台乡](../Page/南台乡.md "wikilink")、[三阳集乡](../Page/三阳集乡.md "wikilink")、[七里乡](../Page/七里乡.md "wikilink")、[下埠集乡](../Page/下埠集乡.md "wikilink")、[衙前乡](../Page/衙前乡.md "wikilink")、[白圩乡](../Page/白圩乡.md "wikilink")、[长山晏乡](../Page/长山晏乡.md "wikilink")、[泉岭乡](../Page/泉岭乡.md "wikilink")

## 参考文献

## 外部链接

  - [进贤信息港](http://www.jxren.com/)
  - [进贤县政府网](https://web.archive.org/web/20090309105601/http://www.jinxian.gov.cn/)

[进贤县](../Page/category:进贤县.md "wikilink")
[县](../Page/category:南昌区县.md "wikilink")
[南昌](../Page/category:江西省县份.md "wikilink")

1.  据《今县释名》：“唐废钟陵入南昌，宋改置进贤镇，寻升镇为县。有栖贤山，唐抚州刺史戴叔伦携家居此因名。”