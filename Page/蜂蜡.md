**蜂蜡**（），是[蜜蜂](../Page/蜜蜂.md "wikilink")[工蜂分泌的](../Page/工蜂.md "wikilink")[蜡](../Page/蜡.md "wikilink")。蜜蜂用蜂蜡在[蜂巢裡建分隔的房间](../Page/蜂巢.md "wikilink")，用来育幼或储存[花粉](../Page/花粉.md "wikilink")。
[Honeycombs-rayons-de-miel-1.jpg](https://zh.wikipedia.org/wiki/File:Honeycombs-rayons-de-miel-1.jpg "fig:Honeycombs-rayons-de-miel-1.jpg")

## 来源

[Bienenvolk-Gemuell.jpg](https://zh.wikipedia.org/wiki/File:Bienenvolk-Gemuell.jpg "fig:Bienenvolk-Gemuell.jpg")
[HonningSkraelle.JPG](https://zh.wikipedia.org/wiki/File:HonningSkraelle.JPG "fig:HonningSkraelle.JPG")
工蜂拥有四對[蜡腺](../Page/蜡腺.md "wikilink")，位于腹部第4至7节。蜂蜡即蜡腺的分泌物。刚羽化成的最年輕的工蜂，其主要職責是照管和飼養幼蟲。當看管幼虫的工作要告一段落时，年輕的工蜂要離巢作短距離的飛翔，十天以后，蜡腺开始分泌蜂蜡。工蜂分泌蜂蜡的理想環境温度為33至36摄氏度。一只工蜂要食用八份[蜂蜜来分泌一份蜂蜡](../Page/蜂蜜.md "wikilink")。

新鲜蜂蜡透明无色（见左图），工蜂咀嚼后变为不透明。蜂巢中的蜡接近于白色，但在[花粉的油及](../Page/花粉.md "wikilink")[蜂胶的作用下逐渐变成黄或棕色](../Page/蜂胶.md "wikilink")。养蜂人在从[蜂巢中采集蜂蜜的同时](../Page/蜂巢.md "wikilink")，也会将蜂蜡刮下作其他用途。

## 属性及成分

|                                                          |     |
| -------------------------------------------------------- | --- |
| **成分**                                                   | 含量  |
| **[烃类](../Page/烃.md "wikilink")**                        | 14% |
| **[单酯](../Page/酯.md "wikilink")（monoesters）**            | 35% |
| **[二酯](../Page/酯.md "wikilink")（diesters）**              | 14% |
| **[三酯](../Page/酯.md "wikilink")（triesters）**             | 3%  |
| **[羟基酯](../Page/酯.md "wikilink")（hydroxy monoesters）**   | 4%  |
| **[羟基聚酯](../Page/聚酯.md "wikilink")（hydroxy polyesters）** | 8%  |
| **[酯酸](../Page/酯.md "wikilink")（acid esters）**           | 1%  |
| **[聚酯酸](../Page/聚酯.md "wikilink")（acid polyesters）**     | 2%  |
| **[脂肪酸](../Page/脂肪酸.md "wikilink")（fatty acids）**        | 12% |
| **[脂肪醇](../Page/脂肪醇.md "wikilink")（fatty alcohols）**     | 1%  |
| **不明**                                                   | 6%  |

蜂蜡的大致化学分子式为：C<sub>15</sub>H<sub>31</sub>COOC<sub>30</sub>H<sub>61</sub>。其主要成分是[棕榈酸](../Page/棕榈酸.md "wikilink")（palmitate）、[棕榈油酸](../Page/棕榈油酸.md "wikilink")（palmitoleate）、[羟基棕榈酸](../Page/羟基棕榈酸.md "wikilink")（hydroxypalmitate）及长链[脂肪醇类](../Page/脂肪醇.md "wikilink")。
蜂蜡的[熔点在](../Page/熔点.md "wikilink")62至64摄氏度之间。当加热到85摄氏度以上时蜂蜡开始变色。蜂蜡的[闪点为](../Page/闪点.md "wikilink")204.4摄氏度。在15摄氏度的环境下其密度为0.958至0.970克/厘米<sup>3</sup>。

## 用途

[Beeswax.jpg](https://zh.wikipedia.org/wiki/File:Beeswax.jpg "fig:Beeswax.jpg")
[Kurps_in_Warsaw-11-Niedzwiedzcy-Pasieka.jpg](https://zh.wikipedia.org/wiki/File:Kurps_in_Warsaw-11-Niedzwiedzcy-Pasieka.jpg "fig:Kurps_in_Warsaw-11-Niedzwiedzcy-Pasieka.jpg")

  - 食品。蜂蠟無色無味，質地稍韌、較難咬斷。
  - [蜡烛](../Page/蜡烛.md "wikilink")
  - [食品添加剂](../Page/食品添加剂.md "wikilink")，编号E901（[上光剂](../Page/上光剂.md "wikilink")）
  - [发蜡](../Page/发蜡.md "wikilink")、[鞋油](../Page/鞋油.md "wikilink")、[木蜡](../Page/木蜡.md "wikilink")、模型蜡等的成分
  - [蜡染](../Page/蜡染.md "wikilink")
  - 蜂蜡布

### 中药

蜂蜡，是一味中药，味甘、淡，性平，归脾、胃、大肠经，有养脾胃，润肺腑，止泄痢之功效。\[1\]

## 参考资料

## 参见

  - [蜜蜂](../Page/蜜蜂.md "wikilink")
  - [蜂巢](../Page/蜂巢.md "wikilink")
  - [蜂胶](../Page/蜂胶.md "wikilink")

[category:材料](../Page/category:材料.md "wikilink")
[category:可分解材料](../Page/category:可分解材料.md "wikilink")

[Category:蜡](../Category/蜡.md "wikilink")

1.  《纲目》：蜜之气味俱厚，故养脾。蜡之气味俱薄，故养胃。厚者味甘而性缓质柔，故润脏腑。薄者味淡而性啬质坚，故止泄痢。张仲景治痢有调气饮，《千金方》治痢有胶蜡汤，其效甚捷，盖有见于此欤。