[Panel4.jpg](https://zh.wikipedia.org/wiki/File:Panel4.jpg "fig:Panel4.jpg")马塞纳高中，一所提供预科班教育的公立高中\]\]
**大学校预科班**（[法語](../Page/法語.md "wikilink")：Classe préparatoire aux
grandes écoles，縮寫 CPGE，簡稱 classes prépas 或
prépas），是[法國教育體系特有的機制](../Page/法國教育.md "wikilink")。法国高中毕业的学生在经过两年比高中更辛苦的学习后需要通过淘汰率很高的后进入四种提供等同[硕士文凭](../Page/硕士.md "wikilink")（bac+5）的[大学校](../Page/大学校.md "wikilink")：高等师范学院（École
normale supérieure）、工程师学校（École d'Ingérieur）、商校（École de
commerce）、国立兽医学校（École Nationale
Vétérinaire）。相较于直接通过Parcoursup录取的大学（universités）而言，大学校通常被视为精英教育。著名的[巴黎高等师范学院](../Page/巴黎高师.md "wikilink")、[巴黎综合理工](../Page/巴黎综合理工.md "wikilink")、[巴黎中央理工](../Page/巴黎中央理工.md "wikilink")、[巴黎高等商業研究學院等即为](../Page/巴黎高等商業研究學院.md "wikilink")[大学校](../Page/大学校.md "wikilink")。

## 录取

因历史原因，大学校预科班虽属于[高等教育](../Page/高等教育.md "wikilink")（即需要持持有高中会考毕业证书（[Bac](../Page/:fr:Baccalauréat_en_France.md "wikilink")）才可进入学习的教育机构），但是被设置于[高中内部](../Page/高中.md "wikilink")。[拿破仑将大学校预科班定位为高中第四到第六年级](../Page/拿破仑.md "wikilink")。即便是只提供预科班而不提供高一到高三教育的私立学校也被称为高中。

预科班的录取需要高三学生在平台注册，根据高中二、三年级成绩以及教师评语决定录取。因法国高等教育机构录取通常在4至5月，而高考成绩在7月才能得知，因此高考成绩不属于录取标准之一。

每年平均500,000高考生中，约只有35,000会被预科班录取学习。\[1\]
在一些较为出名的学校如[亨利四世中学](../Page/亨利四世中学.md "wikilink")，平均40人的班级是从1,500份申请中选出的。

## 理科CPGE

[Classes_préparatoires_scientifiques.svg](https://zh.wikipedia.org/wiki/File:Classes_préparatoires_scientifiques.svg "fig:Classes_préparatoires_scientifiques.svg")
最初[拿破仑设立预科班是为了为选拔军事人才](../Page/拿破仑.md "wikilink")，因此理科是最早存在的预科班。
理科预科班分为以下几科：

  - **MPSI**（Mathématiques, Physiques, Sciences de
    l'Ingénieur；[数学](../Page/数学.md "wikilink")、[物理](../Page/物理.md "wikilink")、[工程师科学](../Page/工程师科学.md "wikilink")），第二年MPSI学生可以选择
    **MP**（数学、物理）或者**PSI**（物理、工程师科学）
  - **PCSI**（Physique, chimie, sciences de
    l'ingénieur；物理、[化学](../Page/化学.md "wikilink")、工程师科学），第二年可以选择
    **PC**（物理、化学）或者 **PSI**
  - **PTSI**（Physiques, technologie, sciences de
    l'ingénieur；物理、[技術](../Page/技術.md "wikilink")、工程师科学），第二年可以选择**PT**（物理、技術）或
    **PSI**
  - **TPC1**（Technologie, physique et chimie；技術、物理、化學），第二年續修**TPC2**
  - **TSI1**（Physiques, Technologie, sciences
    industrielles；物理、技術、[科學工業](../Page/科學工業.md "wikilink")），第二年續修**TST2**
  - **BCPST1**（Biologie, Chimie, Physique, sciences de la
    terre；生物、化学、物理、[地质学](../Page/地质学.md "wikilink")），第二年續修**BCPST2**

## 參考文獻

[Category:法国高等学校类型](../Category/法国高等学校类型.md "wikilink")
[大学校预科班](../Category/大学校预科班.md "wikilink")

1.  [Figures](http://www.enseignementsup-recherche.gouv.fr/cid20709/taux-d-inscription-immediate-des-bacheliers-dans-les-filieres-d-enseignement-superieur.html)