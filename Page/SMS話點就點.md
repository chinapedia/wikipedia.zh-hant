《**話點就點**》，[香港](../Page/香港.md "wikilink")[電視廣播有限公司製作的音樂節目](../Page/電視廣播有限公司.md "wikilink")，是[香港第一個以](../Page/香港.md "wikilink")[電話簡訊點唱的音樂節目](../Page/簡訊.md "wikilink")。

《話點就點》於2002年9月23日至2004年7月17日期間，初期在星期一至星期五的17:45-18:15在[電視廣播有限公司旗下頻道](../Page/電視廣播有限公司.md "wikilink")[翡翠台播出](../Page/翡翠台.md "wikilink")。後來在2003年5月10日開始改在[香港時間逢星期六](../Page/香港時間.md "wikilink")14:55-15:55在[電視廣播有限公司旗下頻道](../Page/電視廣播有限公司.md "wikilink")[翡翠台播出](../Page/翡翠台.md "wikilink")，其後由同類型音樂節目《[音樂潮giv](../Page/音樂潮@giv.md "wikilink")》取代。

## 主持

  - [丘凱敏](../Page/丘凱敏.md "wikilink")
  - [劉曉彤](../Page/劉曉彤.md "wikilink")
  - [胡蓓蔚](../Page/胡蓓蔚.md "wikilink")
  - [吳家樂](../Page/吳家樂.md "wikilink")

## 同類節目

  - [音樂潮giv](../Page/音樂潮@giv.md "wikilink")
  - [360º音樂無邊](../Page/360º音樂無邊.md "wikilink")

## 參見

[無綫電視音樂節目列表](../Page/無綫電視音樂節目列表.md "wikilink")

## 外部連結

[無綫電視官方網頁 - 話點就點](http://app2.tvb.com/board/music/sms/)

[Category:無綫電視音樂節目](../Category/無綫電視音樂節目.md "wikilink")