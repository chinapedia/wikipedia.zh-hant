****是历史上第二十一次航天飞机任务，也是[亚特兰蒂斯号航天飞机的首次太空飞行](../Page/亞特蘭提斯號太空梭.md "wikilink")。

## 任务成员

  - **[卡罗尔·鲍勃科](../Page/卡罗尔·鲍勃科.md "wikilink")**（，曾执行、以及任务），指令长
  - **[唐纳德·格拉比](../Page/唐纳德·格拉比.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[大卫·希尔默斯](../Page/大卫·希尔默斯.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[罗伯特·斯图尔特](../Page/罗伯特·斯图尔特.md "wikilink")**（，曾执行以及任务），任务专家
  - **[威廉·派利斯](../Page/威廉·派利斯.md "wikilink")**（，曾执行任务），有效载荷专家

[Category:1985年科學](../Category/1985年科學.md "wikilink")
[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:亚特兰蒂斯号航天飞机任务](../Category/亚特兰蒂斯号航天飞机任务.md "wikilink")
[Category:1985年10月](../Category/1985年10月.md "wikilink")