[CEIBS.JPG](https://zh.wikipedia.org/wiki/File:CEIBS.JPG "fig:CEIBS.JPG")

**中欧国际工商学院**是中國政府和[欧盟委员会合辦的一所国际性工商學院](../Page/欧盟委员会.md "wikilink")，总部位於[中国上海](../Page/上海.md "wikilink")[浦東新區](../Page/浦東新區.md "wikilink")。中欧国际工商学院在英國[金融時報上的MBA排名為亞洲第一](../Page/金融時報.md "wikilink")。\[1\][法國](../Page/法國.md "wikilink")[巴黎的Eduniversal](../Page/巴黎.md "wikilink")
評價為最高5 PALMES OF
EXCELLENCE，為[中國](../Page/中國.md "wikilink")（含[港澳](../Page/港澳.md "wikilink")）排名第一。\[2\]

## 历史

1984年9月，中国－欧洲共同体管理项目成立，后更名中国－欧洲共同体管理中心。\[3\]

1994年11月，[中华人民共和国政府](../Page/中华人民共和国政府.md "wikilink")（曾由[上海交通大学代表](../Page/上海交通大学.md "wikilink")）与欧洲管理發展基金会合作建立中欧国际工商学院。中欧国际工商学院最早在中国大陆开设全英文全日制[工商管理硕士课程](../Page/工商管理硕士.md "wikilink")（MBA）、[高层管理人员工商管理硕士课程](../Page/高层管理人员.md "wikilink")（EMBA）等培训课程。

2009年底，[中华人民共和国教育部批复中欧国际工商学院为在中国境内的非独立法人](../Page/中华人民共和国教育部.md "wikilink")，结束日期为2014年12月31日，隸屬於上海交通大學。\[4\]

## 校园

全球校園分布共有亞洲、歐洲及非洲等5大校區。\[5\]

### 亞洲

  - [上海本部](../Page/上海.md "wikilink")

位于[浦东新区](../Page/浦东新区.md "wikilink")

  - [北京](../Page/北京.md "wikilink")

位于[中关村](../Page/中关村.md "wikilink")

  - [深圳](../Page/深圳.md "wikilink")

位于[福田区](../Page/福田区.md "wikilink")

### 歐洲

  - [瑞士](../Page/瑞士.md "wikilink")

位於[豪爾根](../Page/豪爾根.md "wikilink")

### 非洲

  - [迦納](../Page/迦納.md "wikilink")

位於[阿克拉](../Page/阿克拉.md "wikilink")

## 参考文献

## 外部链接

  - [中欧国际工商学院官方网站](http://www.ceibs.edu)

{{-}}

[Category:中国商学院](../Category/中国商学院.md "wikilink")
[Category:1994年創建的教育機構](../Category/1994年創建的教育機構.md "wikilink")
[Category:上海交通大学院系](../Category/上海交通大学院系.md "wikilink")

1.
2.
3.  [中欧国际工商学院：里程碑](http://www.ceibs.edu/today_c/establishment/milestones/index.shtml)

4.  [添加中欧国际工商学院至院系设置栏目](http://www.sjtu.edu.cn/info/1161/5868.htm)
5.  [中欧国际工商学院：学院设施](http://www.ceibs.edu/today_c/establishment/facilities/index.shtml)