[Katayama_Saburō.jpg](https://zh.wikipedia.org/wiki/File:Katayama_Saburō.jpg "fig:Katayama_Saburō.jpg")

**片山三郎**(かたやま
さぶろう)，東京帝國大學畢業，[日本](../Page/日本.md "wikilink")[昭和初期之](../Page/昭和.md "wikilink")[日本政府官員](../Page/日本.md "wikilink")，本籍日本[京都府](../Page/京都府.md "wikilink")。片山三郎1925年任臺灣總督府殖產局長\[1\]，1927年7月27日至1929年4月20日任[台南州知事](../Page/台南州知事.md "wikilink")。此後接替[高橋親吉](../Page/高橋親吉.md "wikilink")，於[台灣擔任](../Page/台灣.md "wikilink")[台北州知事](../Page/台北州知事.md "wikilink")，管轄今[臺北市](../Page/臺北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[宜蘭縣及](../Page/宜蘭縣.md "wikilink")[基隆市等地的行政事務](../Page/基隆市.md "wikilink")。

## 事蹟

臺灣總督府於昭和2年（1927年），因應臺灣工業建設之人才需求希望設置與工業相關之專門學校。，1928年，川村竹治新上任臺灣總督，認為臺灣工業發展應建立於工業教育的背景下，欲將臺灣原有兩所高等商業學校之一所變更為高等工業學校，隨後決定將臺南高等商業學校廢校，改設[臺南高等工業學校](../Page/臺南高等工業學校.md "wikilink")。
時任臺南州知事的片山三郎隨即函臺灣總督府，表明已預購臺南市約4萬7千坪土地，可供高等工業學校用地。1929年4月26日，臺灣總督府總務長官河原田稼吉協同文教局長石黑英彥至臺南視察預定地，決定設校，派任前南滿洲工業專門學校校長兼任南滿洲工業學校校長[今景彥負責創校事務](../Page/今景彥.md "wikilink")，1930年籌設組織再擴編為委員會。\[2\]

## 參見

  - [台北州](../Page/台北州.md "wikilink")
  - [台南州](../Page/台南州.md "wikilink")

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

<!-- end list -->

  - 引用

[Category:台南州知事](../Category/台南州知事.md "wikilink")
[Category:台北州知事](../Category/台北州知事.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")

1.  [中央研究院《台灣史研究所》](http://who.ith.sinica.edu.tw/mpView.action)
2.  [成功大學《校園發展歷程日治時期1930-1945》](http://www.ncku.edu.tw/~document/ncku-envi-ch/index_A.html)