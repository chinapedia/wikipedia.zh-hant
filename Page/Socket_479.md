**Socket 479**為一[英特爾](../Page/英特爾.md "wikilink")[Pentium
M處理器使用的插座](../Page/Pentium_M.md "wikilink")，主要在移動平台上出現。相比[Socket
478](../Page/Socket_478.md "wikilink")，它使用另一種插腳的配置方法，所以一般情形下Pentium
M都不可以使用正常的Socket 478主機板。但由於Socket
479的主機板量少價昂，因此[華碩等電腦廠商曾開發轉接板](../Page/華碩.md "wikilink")，使Socket
478主機板也能使用Socket 479的CPU。只有855GME、915GM、945GT等的流動處理器晶片組才支援。

另外，英特爾推出另一全新的Socket 479 mPGA，又稱「Socket M」，以供[Intel
Core](../Page/Intel_Core.md "wikilink")、[Core 2
Duo使用](../Page/Intel_Core_2.md "wikilink")，這個插座有一支插腳與本來的Socket
479插座不同，故新舊兩者互不支援。新的Socket
479支援667MHz的[前端匯流排](../Page/前端匯流排.md "wikilink")。

## 使用Socket 479的處理器

  - *Banias* Celeron M ULV
  - *Banias* Celeron M
  - *Dothan* Celeron M ULV
  - *Dothan* Celeron M
  - *Yonah* Celeron M
  - *Banias* [Pentium M](../Page/Pentium_M.md "wikilink") ULV
  - *Banias* Pentium M LV
  - *Banias* Pentium M
  - *Dothan* Pentium M ULV
  - *Dothan* Pentium M LV
  - *Dothan* Pentium M
  - *Yonah* [Intel Core Solo](../Page/Intel_Core.md "wikilink")
  - *Yonah* [Intel Core Duo](../Page/Intel_Core.md "wikilink")
  - *Merom* [Intel Core 2 Duo](../Page/Intel_Core_2.md "wikilink")
  - *Silverthorne* [Intel Atom](../Page/Intel_Atom.md "wikilink")

## 可使用Socket 479的晶片組

  - 英特爾
      - i852系列

<!-- end list -->

  -

      -
        i852GM/i852GMV

<!-- end list -->

  -   - i855系列

<!-- end list -->

  -

      -
        i855GM/i855GME/i855PM

<!-- end list -->

  -   - i915系列

<!-- end list -->

  -

      -
        i915GM/i915GMS/i915PM

<!-- end list -->

  -   - i945系列

<!-- end list -->

  -

      -
        i945GM/i945PM

<!-- end list -->

  - ATi
      - Mobility Radeon 9100 IGP

<!-- end list -->

  -

      -
        RV280/RS300M

<!-- end list -->

  - SiS
      - SiS 648MX/661MX
  - ULi
      - ULi M1685
  - VIA
      - VIA VN896/VN800

## 参见

  - [Pentium M](../Page/Pentium_M.md "wikilink")
  - [Intel Core](../Page/Intel_Core.md "wikilink")
  - [Intel Core 2](../Page/Intel_Core_2.md "wikilink")
  - [Celeron M](../Page/Celeron_M.md "wikilink")
  - [MoDT](../Page/MoDT.md "wikilink")

[Category:CPU插座](../Category/CPU插座.md "wikilink")