是[CAPCOM在](../Page/CAPCOM.md "wikilink")1984年推出的縱向[捲軸射擊遊戲](../Page/捲軸射擊遊戲.md "wikilink")，平台是[大型電玩](../Page/大型電玩.md "wikilink")。隨後1942也被移植到[紅白機](../Page/紅白機.md "wikilink")、[PC-8801等平台](../Page/PC-8801.md "wikilink")。

## 概要

遊戲背景以[二次世界大戰為背景](../Page/二次世界大戰.md "wikilink")，玩家操縱美軍的[P-38閃電式戰鬥機](../Page/P-38閃電式戰鬥機.md "wikilink")，敵機則是日本風格的虛構機。全部有32關，關卡則以[太平洋戰爭的戰場為主](../Page/太平洋戰爭.md "wikilink")。另外1942在「[炸彈系統](../Page/炸彈.md "wikilink")」還沒有廣泛的被導入射擊遊戲時，率先加入了能夠緊急迴避敵人攻擊的「空中迴旋」，算是「炸彈」的前身。

## 系統

和大多數射擊遊戲相同，1942的操作主要使用[八方向搖桿控制方向](../Page/八方向搖桿.md "wikilink")。另外有兩顆按鍵用途分別射擊和空中迴旋。自機主要武裝是2連裝機槍，對大部分的小型敵機都是1發就可以破壞，但是遊戲中也有許多中、大型敵人需要攻擊許多次才能破壞。空中迴旋可以讓自機擁有短暫的無敵時間以迴避敵人的攻擊，但是有使用次數上的限制，每過一關都會回復數量到初始值。

### 強化道具

  -
    遊戲中有時會出現紅色的戰鬥機編隊，如果全數擊破會掉落強化道具「POW」，得到「POW」可以獲得隨機的獎賞。

<!-- end list -->

  - 黑色 POW：武裝變成4連裝的機槍，提升攻擊力與攻擊範圍。
  - 紅色 POW：畫面上出現的敵人全滅。
  - 白色
    POW：子機：出現兩台我方的戰機跟隨在自機兩旁協助攻擊，類似[宇宙巡航機系列的子機系統](../Page/宇宙巡航機.md "wikilink")。但是子機如果被敵彈命中會被破壞，子機如果和中型以下的敵機相撞則會同歸於盡。
  - 空中迴旋的使用次數增加一次，但是如果保留到過關都沒有使用會被回復到初始值。
  - 分數1000分。
  - 加1台。（出現機率低）

## 系列作品

  - 1987年－[1943 中途島海戰](../Page/1943_中途島海戰.md "wikilink")
  - 1988年－[1943改](../Page/1943改.md "wikilink")
  - 1990年－[1941 Counter
    Attack](../Page/1941_Counter_Attack.md "wikilink")
  - 1996年－[19XX -THE WAR AGAINST
    DESTINY-](../Page/19XX_-THE_WAR_AGAINST_DESTINY-.md "wikilink")
  - 2000年－[1944 The Loop Master](../Page/1944_征服世界.md "wikilink")

## 外部連結

  - Arcade-History上的《[1942](http://www.arcade-history.com/index.php?page=detail&id=6)》
  - [CAPCOM Generarion ～第１集
    撃墜王の時代～](https://web.archive.org/web/20080514022555/http://www.capcom.co.jp/newproducts/consumer/generation/generation.html)－1942在[PlayStation復刻的版本](../Page/PlayStation_\(遊戲機\).md "wikilink")
  - [CAPCOM Classic
    Collection](https://web.archive.org/web/20080405135503/http://www.capcom.co.jp/ccc/)－CAPCOM在2005年推出的跨平台懷舊遊戲合集。

[Category:1984年电子游戏](../Category/1984年电子游戏.md "wikilink")
[Category:红白机游戏](../Category/红白机游戏.md "wikilink")
[Category:Game Boy Color游戏](../Category/Game_Boy_Color游戏.md "wikilink")
[Category:IOS遊戲](../Category/IOS遊戲.md "wikilink")
[Category:MSX游戏](../Category/MSX游戏.md "wikilink") [Category:Wii
Virtual Console游戏](../Category/Wii_Virtual_Console游戏.md "wikilink")
[Category:Windows Mobile
Professional游戏](../Category/Windows_Mobile_Professional游戏.md "wikilink")
[Category:二战题材电子游戏](../Category/二战题材电子游戏.md "wikilink")
[Category:卡普空遊戲](../Category/卡普空遊戲.md "wikilink")
[Category:縱向捲軸射擊遊戲](../Category/縱向捲軸射擊遊戲.md "wikilink")
[Category:行動電話遊戲](../Category/行動電話遊戲.md "wikilink")
[Category:街機遊戲](../Category/街機遊戲.md "wikilink")