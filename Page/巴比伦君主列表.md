以下是在[巴比伦城建都的王朝的国王列表](../Page/巴比伦.md "wikilink")

## [巴比伦第一王朝(古巴比伦王国)](../Page/古巴比倫.md "wikilink")

  - [苏姆阿布姆](../Page/苏姆阿布姆.md "wikilink")(Sumu-abum)
    [1894](../Page/19th_century_BC.md "wikilink")-[1881
    BC](../Page/19th_century_BC.md "wikilink")
  - [苏姆拉埃尔](../Page/苏姆拉埃尔.md "wikilink")(Sumu-la-El)
    [1880](../Page/19th_century_BC.md "wikilink")-[1845
    BC](../Page/19th_century_BC.md "wikilink")
  - [萨比乌姆](../Page/萨比乌姆.md "wikilink")(Sabium)
    [1844](../Page/19th_century_BC.md "wikilink")-[1831
    BC](../Page/19th_century_BC.md "wikilink")
  - [阿皮尔·辛](../Page/阿皮尔·辛.md "wikilink")(Apil-Sîn)
    [1830](../Page/19th_century_BC.md "wikilink")-[1813
    BC](../Page/19th_century_BC.md "wikilink")
  - [辛·穆巴里特](../Page/辛·穆巴里特.md "wikilink")
    [1812](../Page/19th_century_BC.md "wikilink")-[1793
    BC](../Page/18th_century_BC.md "wikilink")
  - [漢摩拉比](../Page/漢摩拉比.md "wikilink")
    [1792](../Page/18th_century_BC.md "wikilink")-[1750
    BC](../Page/18th_century_BC.md "wikilink")
  - [萨姆苏·伊路那](../Page/萨姆苏·伊路那.md "wikilink")(Samsu-Iluna)
    [1749](../Page/18th_century_BC.md "wikilink")-[1712
    BC](../Page/18th_century_BC.md "wikilink")
  - [阿比·埃苏赫](../Page/阿比·埃苏赫.md "wikilink")(Abi-Eshuh)
    [1711](../Page/18th_century_BC.md "wikilink")-[1684
    BC](../Page/1680s_BC.md "wikilink")
  - [阿米狄塔那](../Page/阿米狄塔那.md "wikilink")(Ammi-Ditana)
    [1683](../Page/1680s_BC.md "wikilink")-[1647
    BC](../Page/1640s_BC.md "wikilink")
  - [阿米萨杜卡](../Page/阿米萨杜卡.md "wikilink")(Ammi-Saduqa)\]\]
    [1646](../Page/1640s_BC.md "wikilink")-[1626
    BC](../Page/1620s_BC.md "wikilink")
  - [萨姆苏·地塔那](../Page/萨姆苏·地塔那.md "wikilink")(Samsu-Ditana)
    [1625](../Page/1620s_BC.md "wikilink")-[1595
    BC](../Page/1590s_BC.md "wikilink")

## [前喀西特王朝](../Page/前喀西特王朝.md "wikilink")

*以下的君主并未实际统治巴比伦本身，但是他们的后继者建立喀西特巴比伦王朝，因此也列于此*

  - [甘达什](../Page/甘达什.md "wikilink")(Gandash) [1730
    BC](../Page/18th_century_BCE.md "wikilink")
  - [阿贡一世](../Page/阿贡一世.md "wikilink")(Agum I)
  - [卡什提里亚什一世](../Page/卡什提里亚什一世.md "wikilink")(Kashtiliash I)
  - [Ushshi](../Page/Ushshi.md "wikilink")
  - [Abirattash](../Page/Abirattash.md "wikilink")
  - [卡什提里亚什二世](../Page/卡什提里亚什二世.md "wikilink")(Kashtiliash II)
  - [乌尔兹古如玛什](../Page/乌尔兹古如玛什.md "wikilink")(Urzigurumash)
  - Harbashihu
  - [Tiptakzi](../Page/Tiptakzi.md "wikilink")

## 海地王朝 ([巴比伦第二王朝](../Page/巴比伦第二王朝.md "wikilink"))

''此王朝也并没有实际统治巴比伦，由于它实际上统治着南部的[苏美尔地区](../Page/苏美尔.md "wikilink")，传统上把它称作巴比伦第二王朝，所以也列于此

  - [伊鲁马·伊鲁姆](../Page/伊鲁马·伊鲁姆.md "wikilink")(Iluma-ilum) fl. c.[1732
    BC](../Page/18th_century_BCE.md "wikilink")
  - [Itti-ili-nibi](../Page/Itti-ili-nibi.md "wikilink")
  - [Damiq-ilishu](../Page/Damiq-ilishu.md "wikilink")
  - [Ishkibal](../Page/Ishkibal.md "wikilink")
  - [Shushushi](../Page/Shushushi.md "wikilink")
  - [Gulkishar](../Page/Gulkishar.md "wikilink")
  - Peshgaldaramash
  - Adarakalamma
  - Ekurduanna
  - Melamkurkukka
  - \[1 unnamed king between Gulkishar and Ea-gamil(?)\]
  - [伊亚·加米尔](../Page/伊亚·加米尔.md "wikilink")(Ea-gamil) fl. c. [1460
    BC](../Page/1460s_BC.md "wikilink")

## [喀西特王朝](../Page/喀西特王朝.md "wikilink") ([巴比伦第三王朝](../Page/巴比伦第三王朝.md "wikilink"))

  - [阿贡二世](../Page/阿贡二世.md "wikilink")(Agum II) fl. c.[1570
    BC](../Page/1570s_BCE.md "wikilink")
  - [布尔那布里亚什一世](../Page/布尔那布里亚什一世.md "wikilink")(Burna-Buriaš I)
  - Kaštiliaš III
  - [Ulam-Buriaš](../Page/Ulam-Buriaš.md "wikilink")
  - [阿贡三世](../Page/阿贡三世.md "wikilink")(Agum III)
  - [卡达什曼·哈尔伯一世](../Page/卡达什曼·哈尔伯一世.md "wikilink")(Kadašman-harbe I)
  - [卡拉因达什](../Page/卡拉因达什.md "wikilink")[Karaindaš](../Page/Karaindaš.md "wikilink")
  - [库瑞噶尔祖一世](../Page/库瑞噶尔祖一世.md "wikilink")(Kurigalzu I) d.[1377
    BC](../Page/1377_BC.md "wikilink")
  - [卡达什曼·恩利尔一世](../Page/卡达什曼·恩利尔一世.md "wikilink")(Kadashman-Enlil
    I|Kadašman-Enlil I)[1377](../Page/1377_BC.md "wikilink")-[1361
    BC](../Page/1361_BC.md "wikilink")
  - [布尔那布瑞亚什二世](../Page/布尔那布瑞亚什二世.md "wikilink")(Burna-Buriaš II)
    [1361](../Page/1361_BC.md "wikilink")-[1333
    BC](../Page/1333_BC.md "wikilink")
  - Karahardaš [1333](../Page/1333_BC.md "wikilink")-[1331
    BC](../Page/1331_BC.md "wikilink")
  - [Nazibugaš](../Page/Nazibugaš.md "wikilink") [1331
    BC](../Page/1331_BC.md "wikilink")
  - [库瑞噶尔祖二世](../Page/库瑞噶尔祖二世.md "wikilink")(Kurigalzu II)
    [1331](../Page/1331_BC.md "wikilink")-[1306
    BC](../Page/1306_BC.md "wikilink")
  - [那兹·玛鲁塔什](../Page/那兹·玛鲁塔什.md "wikilink")(Nazimaruttaš)
    [1306](../Page/1306_BC.md "wikilink")-[1280
    BC](../Page/1280_BC.md "wikilink")
  - [卡达什曼·图古尔](../Page/卡达什曼·图古尔.md "wikilink")(Kadašman-Turgu)
    [1280](../Page/1280_BC.md "wikilink")-[1262
    BC](../Page/1262_BC.md "wikilink")
  - [卡达什曼·恩利尔二世](../Page/卡达什曼·恩利尔二世.md "wikilink")(Kadašman-Enlil II)
    [1262](../Page/1262_BC.md "wikilink")-[1254
    BC](../Page/1254_BC.md "wikilink")
  - [库杜尔·恩利尔](../Page/库杜尔·恩利尔.md "wikilink")(Kudur-Enlil)
    [1254](../Page/1250s_BC.md "wikilink")-[1245
    BC](../Page/1240s_BC.md "wikilink")
  - [沙噶拉克提·舒瑞亚什](../Page/沙噶拉克提·舒瑞亚什.md "wikilink")(Šagarakti-Šuriaš)
    [1245](../Page/1240s_BC.md "wikilink")-[1232
    BC](../Page/1230s_BC.md "wikilink")
  - [卡什提里亚什四世](../Page/卡什提里亚什四世.md "wikilink")(Kaštiliaš IV)
    [1232](../Page/1230s_BC.md "wikilink")-[1224
    BC](../Page/1220s_BC.md "wikilink")
  - [恩利尔·那丁·舒米](../Page/恩利尔·那丁·舒米.md "wikilink")(Enlil-nadin-šumi)
    [1224](../Page/1224_BC.md "wikilink")-[1221](../Page/1221_BC.md "wikilink")
  - [阿达德-舒马-伊丁那](../Page/阿达德-舒马-伊丁那.md "wikilink")(Adad-šuma-iddina)
    [1221](../Page/1221_BC.md "wikilink")-[1215](../Page/1215_BC.md "wikilink")
  - [阿达德·苏姆·乌苏尔](../Page/阿达德·苏姆·乌苏尔.md "wikilink")(Adad-šuma-usur)
    [1215](../Page/1210s_BC.md "wikilink")-[1185
    BC](../Page/1180s_BC.md "wikilink")
  - [美里·什帕克](../Page/美里·什帕克.md "wikilink")(Melišipak)
    [1185](../Page/1180s_BC.md "wikilink")-[1170
    BCE](../Page/1170s_BC.md "wikilink")
  - [Marduk](../Page/Marduk.md "wikilink")-apal-iddina I
    [1170](../Page/1170s_BC.md "wikilink")-[1157
    BC](../Page/1150s_BC.md "wikilink")
  - [Zababa](../Page/Zababa.md "wikilink")-šuma-iddina
    [1157](../Page/1150s_BC.md "wikilink")-[1156
    BC](../Page/1150s_BC.md "wikilink")
  - [Enlil](../Page/Enlil.md "wikilink")-nadin-ahhe
    [1156](../Page/1150s_BC.md "wikilink")-[1153
    BC](../Page/1150s_BC.md "wikilink")

## [巴比伦第四王朝](../Page/巴比伦第四王朝.md "wikilink")(或称[伊辛第二王朝](../Page/伊辛第二王朝.md "wikilink"))

*從此王朝起，至亞述统治之前的六個王朝都是由巴比倫尼亞本地居民建立的政權，“巴比倫屬於巴比倫人”，也是歷史上唯一由巴比倫尼亞本地居民主政的時期\[1\]。*

  - [马尔杜克·卡比特·阿海舒](../Page/马尔杜克·卡比特·阿海舒.md "wikilink")(Marduk-kabit-ahhešu)
    [1155](../Page/1150s_BCE.md "wikilink")-[1146
    BCE](../Page/1130s_BCE.md "wikilink")
  - [伊提·马尔杜克·巴拉图](../Page/伊提·马尔杜克·巴拉图.md "wikilink")(Itti-Marduk-balatu)
    [1146](../Page/1146_BC.md "wikilink")-[1132
    BCE](../Page/1132_BCE.md "wikilink")
  - [尼努尔塔·那丁·舒米](../Page/尼努尔塔·那丁·舒米.md "wikilink")(Ninurta-nadin-šumi)
    [1132](../Page/1132_BC.md "wikilink")-[1126
    BCE](../Page/1126_BCE.md "wikilink")
  - [尼布甲尼撒一世](../Page/尼布甲尼撒一世.md "wikilink")(Nabu-kudurri-usur)
    [1126](../Page/1120s_BCE.md "wikilink")-[1103
    BCE](../Page/1100s_BCE.md "wikilink")
  - [恩利尔·那丁·阿普利](../Page/恩利尔·那丁·阿普利.md "wikilink")(Enlil-nadin-apli)
    [1103](../Page/1103_BC.md "wikilink")-[1100
    BCE](../Page/1100_BCE.md "wikilink")，其在位末年统治的人口达到100万人
  - [马尔杜克·那丁·阿海](../Page/马尔杜克·那丁·阿海.md "wikilink")(Marduk-nadin-ahhe)
    [1100](../Page/1100_BC.md "wikilink")-[1082
    BCE](../Page/1082_BCE.md "wikilink")
  - [马尔杜克·沙皮克·泽瑞](../Page/马尔杜克·沙皮克·泽瑞.md "wikilink")(Marduk-šapik-zeri)
    [1082](../Page/1082_BC.md "wikilink")-[1069
    BCE](../Page/1069_BCE.md "wikilink")
  - [阿达德·阿普拉·伊地那](../Page/阿达德·阿普拉·伊地那.md "wikilink")(Adad-apla-iddina)
    [1069](../Page/1060s_BC.md "wikilink")-[1046
    BCE](../Page/1040s_BC.md "wikilink")，其统治下的人口依然有100万人
  - [马尔杜克·阿海·埃瑞巴](../Page/马尔杜克·阿海·埃瑞巴.md "wikilink")(Marduk-ahhe-eriba)
    [1046 BCE](../Page/1040s_BC.md "wikilink")
  - [马尔杜克·泽尔·X](../Page/马尔杜克·泽尔·X.md "wikilink")(Marduk-zer-X)
    [1046](../Page/1040s_BC.md "wikilink")-[1033
    BCE](../Page/1030s_BC.md "wikilink")
  - [那布·舒穆·里布尔](../Page/那布·舒穆·里布尔.md "wikilink")(Nabu-šum-libur)
    [1033](../Page/1030s_BC.md "wikilink")-[1025
    BCE](../Page/1020s_BC.md "wikilink")

## [巴比伦第五王朝](../Page/巴比伦第五王朝.md "wikilink")

  - [西姆巴尔·什帕克](../Page/西姆巴尔·什帕克.md "wikilink")(Simbar-šipak)
    [1025](../Page/1020s_BCE.md "wikilink")-[1008
    BCE](../Page/1000s_BCE.md "wikilink")
  - [埃阿·穆金·泽瑞](../Page/埃阿·穆金·泽瑞.md "wikilink")(Ea-mukin-šumi) [1008
    BCE](../Page/1008_BCE.md "wikilink")
  - [卡什舒·那丁·阿海](../Page/卡什舒·那丁·阿海.md "wikilink")(Kaššu-nadin)
    [1008](../Page/100s_BCE.md "wikilink")-[1004
    BCE](../Page/1000s_BCE.md "wikilink")

## [巴比伦第六王朝](../Page/巴比伦第六王朝.md "wikilink")

  - [埃乌尔玛什·沙金·舒米](../Page/埃乌尔玛什·沙金·舒米.md "wikilink")(Eulma-šakin-šumi)
    [1004](../Page/1000s_BCE.md "wikilink")-[987
    BCE](../Page/980s_BCE.md "wikilink")
  - [尼努尔塔·库杜瑞·乌苏尔](../Page/尼努尔塔·库杜瑞·乌苏尔.md "wikilink")(Ninurta-kudurri-usur)
    [987](../Page/980s_BCE.md "wikilink")-[985
    BCE](../Page/980s_BCE.md "wikilink")
  - [什瑞克提·舒卡穆纳](../Page/什瑞克提·舒卡穆纳.md "wikilink")(Širiqti-šuqamunu) [985
    BCE](../Page/980s_BCE.md "wikilink")

## [巴比伦第七王朝](../Page/巴比伦第七王朝.md "wikilink")

  - [玛尔·比提·阿普拉·乌苏尔](../Page/玛尔·比提·阿普拉·乌苏尔.md "wikilink")(Mar-biti-apla-usur)
    [985](../Page/980s_BCE.md "wikilink")-[979
    BCE](../Page/970s_BCE.md "wikilink")

## [巴比伦第八王朝](../Page/巴比伦第八王朝.md "wikilink")

  - [那布·穆金·阿普利](../Page/那布·穆金·阿普利.md "wikilink")(Nabu-mukin-apli)
    [979](../Page/970s_BCE.md "wikilink")-[943
    BCE](../Page/940s_BCE.md "wikilink")
  - [尼努尔塔·库杜瑞·乌苏尔二世](../Page/尼努尔塔·库杜瑞·乌苏尔二世.md "wikilink") 前943年
  - [玛尔·比提·阿海·伊地那](../Page/玛尔·比提·阿海·伊地那.md "wikilink") 前942年

## [巴比伦第九王朝](../Page/巴比伦第九王朝.md "wikilink")

  - [Ninurta](../Page/Ninurta.md "wikilink")-kudurri-usur [943
    BCE](../Page/940s_BCE.md "wikilink")
  - Mar-biti-ahhe-iddina [943](../Page/943_BCE.md "wikilink")-c.[920
    BCE](../Page/920_BCE.md "wikilink")
  - [Šamaš](../Page/Shamash.md "wikilink")-mudammiq
    c.[920](../Page/920_BCE.md "wikilink")-[900
    BCE](../Page/900_BCE.md "wikilink")
  - [Nabu](../Page/Nabu.md "wikilink")-šuma-ukin
    [900](../Page/900s_BCE.md "wikilink")-[888
    BCE](../Page/880s_BCE.md "wikilink")
  - [Nabu](../Page/Nabu.md "wikilink")-apla-iddina
    [888](../Page/880s_BCE.md "wikilink")-[855
    BCE](../Page/850s_BCE.md "wikilink")
  - [Marduk](../Page/Marduk.md "wikilink")-zakir-šumi I
    [855](../Page/850s_BCE.md "wikilink")-[819
    BCE](../Page/810s_BCE.md "wikilink")
  - [Marduk](../Page/Marduk.md "wikilink")-balassu-iqbi
    [819](../Page/819_BCE.md "wikilink")-[813
    BCE](../Page/813_BCE.md "wikilink")
  - Baba-aha-iddina [813](../Page/813_BCE.md "wikilink")-[811
    BCE](../Page/811_BCE.md "wikilink")
  - \[5 kings\] [811](../Page/811_BCE.md "wikilink")-c.[800
    BCE](../Page/800_BCE.md "wikilink")
  - [Ninurta](../Page/Ninurta.md "wikilink")-apla-X
    c.[800](../Page/800_BCE.md "wikilink")-c.[790
    BCE](../Page/790_BCE.md "wikilink")
  - [Marduk](../Page/Marduk.md "wikilink")-bel-zeri
    c.[790](../Page/790_BCE.md "wikilink")-c.[780
    BCE](../Page/780_BCE.md "wikilink")
  - [Marduk](../Page/Marduk.md "wikilink")-apla-usur
    c.[780](../Page/780_BCE.md "wikilink")-[769
    BCE](../Page/769_BCE.md "wikilink")
  - Eriba-[Marduk](../Page/Marduk.md "wikilink")
    [769](../Page/760s_BCE.md "wikilink")-[761
    BCE](../Page/760s_BCE.md "wikilink")
  - [Nabu](../Page/Nabu.md "wikilink")-šuma-iškun
    [761](../Page/760s_BCE.md "wikilink")-[748
    BCE](../Page/740s_BCE.md "wikilink")
  - [Nabonassar](../Page/Nabonassar.md "wikilink")
    ([Nabu](../Page/Nabu.md "wikilink")-nasir)
    [748](../Page/740s_BCE.md "wikilink")-[734
    BCE](../Page/730s_BCE.md "wikilink")
  - [Nabu](../Page/Nabu.md "wikilink")-nadin-zeri
    [734](../Page/734_BC.md "wikilink")-[732
    BCE](../Page/732_BCE.md "wikilink")
  - [Nabu](../Page/Nabu.md "wikilink")-šuma-ukin II [732
    BCE](../Page/732_BCE.md "wikilink")
  - [那布·穆金·泽瑞](../Page/那布·穆金·泽瑞.md "wikilink")，[732](../Page/730s_BCE.md "wikilink")-[729
    BCE](../Page/720s_BCE.md "wikilink")

## [巴比伦第十王朝](../Page/巴比伦第十王朝.md "wikilink") ([亚述统治下](../Page/亚述.md "wikilink"))

  - [提格拉特帕拉沙爾三世](../Page/提格拉特帕拉沙爾三世.md "wikilink")，[729](../Page/720s_BCE.md "wikilink")-[727
    BCE](../Page/720s_BCE.md "wikilink")
  - [薩爾瑪那薩爾五世](../Page/薩爾瑪那薩爾五世.md "wikilink")，[727](../Page/720s_BC.md "wikilink")-[722
    BCE](../Page/720s_BC.md "wikilink")
  - [麥若達赫·巴拉丹二世](../Page/麥若達赫·巴拉丹二世.md "wikilink")，
    [722](../Page/720s_BCE.md "wikilink")-[710
    BCE](../Page/710s_BCE.md "wikilink")
  - [薩爾貢二世](../Page/薩爾貢二世.md "wikilink")，[710](../Page/710s_BCE.md "wikilink")-[705
    BCE](../Page/700s_BCE.md "wikilink")
  - [辛那赫里布](../Page/辛那赫里布.md "wikilink")，[705](../Page/705_BCE.md "wikilink")-[703
    BCE](../Page/703_BCE.md "wikilink")
  - [馬爾杜克·扎基爾·舒米](../Page/馬爾杜克·扎基爾·舒米.md "wikilink")，[703
    BCE](../Page/700s_BCE.md "wikilink")
  - [麥若達赫·巴拉丹二世](../Page/麥若達赫·巴拉丹二世.md "wikilink")，[703
    BCE](../Page/700s_BCE.md "wikilink") (restored)
  - [貝爾·伊博尼](../Page/貝爾·伊博尼.md "wikilink")，[703](../Page/700s_BCE.md "wikilink")-[700
    BCE](../Page/700s_BCE.md "wikilink")
  - [阿淑爾·那丁·舒米](../Page/阿淑爾·那丁·舒米.md "wikilink")，[700](../Page/700s_BCE.md "wikilink")-[694
    BCE](../Page/690s_BCE.md "wikilink")
  - [內爾伽爾·烏塞吉布](../Page/內爾伽爾·烏塞吉布.md "wikilink")，[694](../Page/694_BC.md "wikilink")-[693
    BCE](../Page/693_BCE.md "wikilink")
  - [穆塞吉布·馬爾杜克](../Page/穆塞吉布·馬爾杜克.md "wikilink")，[693](../Page/690s_BCE.md "wikilink")-[689
    BCE](../Page/680s_BCE.md "wikilink")

*Assyrian Sack of Babylon, [689 BCE](../Page/680s_BCE.md "wikilink");
Babylon is rebuilt by [Esarhaddon](../Page/Esarhaddon.md "wikilink") of
Assyria in the [670s BCE](../Page/670s_BCE.md "wikilink")*

  - [辛那赫里布](../Page/辛那赫里布.md "wikilink")，[689](../Page/689_BC.md "wikilink")-[681
    BCE](../Page/681_BCE.md "wikilink")
  - [阿薩爾哈東](../Page/阿薩爾哈東.md "wikilink")，[681](../Page/681_BCE.md "wikilink")-[669
    BCE](../Page/669_BCE.md "wikilink")
  - [沙馬什·舒姆·烏金](../Page/沙馬什·舒姆·烏金.md "wikilink")，[668](../Page/660s_BCE.md "wikilink")-[648
    BCE](../Page/640s_BCE.md "wikilink")
  - [坎達拉努](../Page/坎達拉努.md "wikilink")，[648](../Page/640s_BCE.md "wikilink")-[627
    BCE](../Page/620s_BCE.md "wikilink")

## [巴比伦第十一王朝 (新巴比倫王國)](../Page/新巴比倫王國.md "wikilink")

  - [那波帕拉萨尔](../Page/那波帕拉萨尔.md "wikilink") (Nabû-apla-usur) 前626年 -
    前605年
  - [尼布甲尼撒二世](../Page/尼布甲尼撒二世.md "wikilink") (Nabû-kuddurî-usur II)
    前605年 - 前562年
  - [以未米羅達](../Page/以未米羅達.md "wikilink") (Amîl-Marduk) 前562年 - 前560年
  - [涅里格利沙爾](../Page/涅里格利沙爾.md "wikilink") (Nergal-šarra-usur) 前560年 -
    前556年
  - [拉巴施馬爾杜克](../Page/拉巴施馬爾杜克.md "wikilink") (La-ba-ši-Marduk) 前556年
  - [那波尼德](../Page/那波尼德.md "wikilink") (Nabû-nâ'id) 前556年 -
    前539年，其子[伯沙撒](../Page/伯沙撒.md "wikilink")
    (Bel-sarra-usur) 在前553年或前549年 - 前539年任攝政王。

公元前539年，[波斯帝国的](../Page/波斯帝国.md "wikilink")[居鲁士二世占领巴比伦](../Page/居鲁士二世.md "wikilink")。巴比伦并入波斯帝国。此后除[亚历山大大帝曾经以巴比伦作为自己的首都外](../Page/亚历山大大帝.md "wikilink")，巴比伦再没有过独立的国家或者作为某个国家的首都。

## 外部链接

  - [吴宇虹：古代两河流域文明史年代学研究的历史与现状](http://www.china001.com/show_hdr.php?xname=PPDDMV0&dname=GKJED41&xpos=26)

[\*](../Category/巴比伦君主.md "wikilink")
[B](../Category/亚洲君主列表.md "wikilink")
[B](../Category/聖經人物.md "wikilink")

1.  劉增泉著：《失落的文明—巴比倫帝國》，五南圖書出版社，2016年，第181頁。ISBN 978-957-11-8794-5