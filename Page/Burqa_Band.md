**布卡帶**（**''Burka Band**'')
或**藍布卡樂隊**或**罩袍樂團**）是來自於[阿富汗](../Page/阿富汗.md "wikilink")[喀布爾的一個全女班的](../Page/喀布爾.md "wikilink")[獨立搖滾](../Page/獨立搖滾.md "wikilink")[樂團](../Page/樂團.md "wikilink")。她們是[匿名表演](../Page/匿名.md "wikilink")，所有成員都身穿[波卡來表示對於](../Page/波卡_\(服飾\).md "wikilink")[塔利班的](../Page/塔利班.md "wikilink")[伊斯蘭服裝規定表達明顯的抗議](../Page/伊斯蘭.md "wikilink")。她們於2002年發行了一張單曲《布卡藍》（Burka
Blue）和一張同名專輯。2000年代，該樂隊在[歐洲已取得了一些人氣](../Page/歐洲.md "wikilink")，並且到[德國巡迴演出](../Page/德國.md "wikilink")，在那裡她們的一首歌由[DJ](../Page/DJ.md "wikilink")[芭芭拉·穆根士頓](../Page/芭芭拉·穆根士頓.md "wikilink")（[Barbara
Morgenstern](../Page/:en:Barbara_Morgenstern.md "wikilink")）混音\[1\]\[2\]。[YouTube中一段她們表演的錄像受到廣大的傳閱](../Page/YouTube.md "wikilink")。

## 參考文獻

<div class="references-small">

<references />

</div>

[Category:阿富汗文化](../Category/阿富汗文化.md "wikilink")
[Category:摇滚乐团](../Category/摇滚乐团.md "wikilink")
[Category:女子音乐团体](../Category/女子音乐团体.md "wikilink")

1.  [Female Afghan Burqa Band Breaks
    Barriers](http://www.salem-news.com/articles/february242007/burqa_band_22407.php).
    Salem News. Accessed May 16, 2007.
2.  [Burqa Blues Hit the
    Airwaves](http://www.news24.com/News24/Entertainment/Abroad/0,,2-1225-1243_1396602,00.html)
    . News24.com. Accessed May 16, 2007.