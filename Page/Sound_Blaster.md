[Sound_Blaster_Live\!_5.1.jpg](https://zh.wikipedia.org/wiki/File:Sound_Blaster_Live!_5.1.jpg "fig:Sound_Blaster_Live!_5.1.jpg")
**Sound
Blaster**（**聲霸卡**）是一個[電腦](../Page/電腦.md "wikilink")[音效卡系列產品](../Page/音效卡.md "wikilink")，曾經是[IBM](../Page/IBM.md "wikilink")[個人電腦聲效的非正式標準](../Page/個人電腦.md "wikilink")，由[新加坡](../Page/新加坡.md "wikilink")[創新科技開發](../Page/創新科技.md "wikilink")。首張Sound
Blaster音效卡在1989年11月面世，其後推出過多代版本，如Sound Blaster
16、Live\!系列及Audigy系列，介面亦由[ISA经](../Page/ISA.md "wikilink")[PCI演變為更高效能的](../Page/PCI.md "wikilink")[PCI-E](../Page/PCI-E.md "wikilink")。

近年其他音效標準的出現，加上[主板內置音效的流行](../Page/主板.md "wikilink")，令Sound
Blaster的地位不如往日。現時Sound
Blaster主要生產較高價的音效卡產品，提供虛擬環繞聲，還原音頻文件因壓縮而損失的自然動態範圍，增強低音，自動調整音量和增強電影中的語音的功能。

## Sound Blaster系列

[Sound_Blaster_AWE64.jpg](https://zh.wikipedia.org/wiki/File:Sound_Blaster_AWE64.jpg "fig:Sound_Blaster_AWE64.jpg")
[Sound_Blaster_Recon3D.jpg](https://zh.wikipedia.org/wiki/File:Sound_Blaster_Recon3D.jpg "fig:Sound_Blaster_Recon3D.jpg")

### 內置聲卡

  - Sound Blaster
  - Sound Blaster Pro
  - Sound Blaster 16
  - Sound Blaster AWE32
  - Sound Blaster 32
  - Sound Blaster AWE64
  - Sound Blaster AWE64 Gold
  - Sound Blaster PCI64
  - Sound Blaster PCI128 Digital
  - Sound Blaster PCI512
  - Sound Blaster Live\!
  - Sound Blaster Live\! Value
  - Sound Blaster Live\! 5.1
  - Sound Blaster Live\! 24bit
  - Sound Blaster Audigy
  - Sound Blaster Audigy 2
  - Sound Blaster Audigy 4
  - Sound Blaster Audigy 4 SE
  - Sound Blaster X-Fi
  - Sound Blaster Recon3D
  - Sound Blaster Recon3D Professional Audio
  - Sound Blaster Recon3D Fatal1ty Professional / Fatal1ty Champion
  - Sound Blaster Z / Zx / ZxR

### USB 音頻設備

[Sound_Blaster_E1.jpg](https://zh.wikipedia.org/wiki/File:Sound_Blaster_E1.jpg "fig:Sound_Blaster_E1.jpg")

  - Sound Blaster X-Fi GO\! Pro
  - Sound Blaster X-Fi Surround 5.1 Pro
  - Sound Blaster Digital Music Premium HD\[1\]
  - Sound Blaster Recon3D USB\[2\]
  - Sound BlasterAxx SBX 8 / SBX 10 / SBX 20 音箱
  - Sound Blaster EVO / EVO Wireless / EVO Zx / EVO ZxR 耳機\[3\]
  - Sound Blaster Play\! 2
  - Sound Blaster Omni Surround 5.1
  - Sound Blaster Audigy 6 USB\[4\]
  - Sound BlasterAxx AXX 200 便攜藍牙音箱\[5\]
  - Sound Blaster Roar 便攜藍牙音箱
  - Sound Blaster E1\[6\] / E3\[7\] / E5
  - Sound Blaster X7

## EAX 支持

[Windows
Vista將不會支持](../Page/Windows_Vista.md "wikilink")[DirectSound和](../Page/DirectSound.md "wikilink")[DirectSound3D
HAL](../Page/DirectSound3D_HAL.md "wikilink")，而只支持[OpenAL](../Page/OpenAL.md "wikilink")。若遊戲只支持[DirectSound](../Page/DirectSound.md "wikilink")，音效會交由[CPU運算](../Page/CPU.md "wikilink")，而且没有[EAX功能](../Page/EAX.md "wikilink")。创新科技推出了新的插件－**[ALchemy](../Page/ALchemy.md "wikilink")**（炼金术）解決此問題。原理是驅動程序會截取遊戲的[DirectSound數據](../Page/DirectSound.md "wikilink")，並轉換為[OpenAL數據](../Page/OpenAL.md "wikilink")，再交由音效卡運算。

## 驅動程序

官方除了發佈驅動程序外，還發佈一系列的配套软件，來增加產品的功能。例如DVD-Audio的播放器和控制台，後者可以顯示和調節[音效卡的參數](../Page/音效卡.md "wikilink")。\[8\]

除了官方提供驅動程序外，有不少程序員會提供第三方的驅動程序。例如kX改版驱动，它是由一群來自[俄罗斯和世界其他地方的程序員合作編寫](../Page/俄罗斯.md "wikilink")\[9\]。該驱动可以支持[ASIO](../Page/ASIO.md "wikilink")，亦附設[DSP圖形設置介面](../Page/DSP.md "wikilink")。支持平台包括[Windows和](../Page/Windows.md "wikilink")[Mac
OS](../Page/Mac_OS.md "wikilink")\[10\]。創新科技以往並不歡迎用戶修改官方的驅動程序。直到2008年，公司的態度似乎有所轉變。它開始停止對ALchemy驅動程序的收費\[11\]，並退回先前收取用戶的款項。創新科技亦容許用戶開發扩充补丁，但不可以修改官方驅動的執行文件和[DLL文件](../Page/DLL.md "wikilink")。於是，有人開發了針對[Dolby
Digital
Live](../Page/杜比数字#Dolby_Digital_Live.md "wikilink")（DDL）功能的补丁。透過它，可以解除原版驅動的限制，使到其X-Fi[音效卡可以支持該功能](../Page/音效卡.md "wikilink")\[12\]。DDL技術可以使任何電腦遊戲的音效轉換成5.1聲道。其後，创新的X-Fi
Fatal1ty Pro音效卡正式官方支持該功能。\[13\]

另一個被解鎖的功能是X-Fi Crystalizer。透過补丁，可以使Audigy声卡的用戶，使用X-Fi声卡獨有的X-Fi
Crystalizer功能。該功能可以動態修補音效，針對有損壓縮音頻格式，例如[MP3](../Page/MP3.md "wikilink")。\[14\]

## 第三方產品

[Auzentech](../Page/Auzentech.md "wikilink")（爱必特）公司推出的Auzen X-Fi
Prelude音效卡，使用了創新科技的X-Fi晶片。

## 外部連結

  - [Sound Blaster 創新科技](http://www.soundblaster.com/)
  - [DearHoney
    數字音樂工作室](http://www.dearhoney.idv.tw)（裡面有許多數字音樂產品的評論及介紹，其中包括Creative系列產品）

## 參考資料

<references />

[Category:声卡](../Category/声卡.md "wikilink")
[Category:1989年成立的公司](../Category/1989年成立的公司.md "wikilink")
[Category:新加坡品牌](../Category/新加坡品牌.md "wikilink")

1.  [Creative 創新 Sound Blaster Digital Music Premium HD USB音效卡拆解
    圖集](http://www.soomal.com/doc/20100001585.htm)
2.  [Creative 創新 Sound Blaster Recon3D
    USB音效卡拆解](http://www.soomal.com/doc/20100003055.htm)
3.  [將 SoundBlaster 晶片塞入，Creative 發表 4 款 EVO
    系列耳麥](http://chinese.vr-zone.com/68754/creative-annoucend-soundblaster-evo-headset-06122013/)
4.  [Sound Blaster Audigy6 USB
    声霸卡发布](http://digital.it168.com/a2014/0319/1604/000001604220.shtml)
5.  [创新科技Sound
    BlasterAxx发布会在京举行](http://digital.yesky.com/487/35302487.shtml)
6.  [Creative Sound Blaster E1 |
    E-Choice](http://www.e-choice.com.hk/creative-sound-blaster-e1/)
7.  [创新E3耳机功放：让任何耳机都变成蓝牙耳机](http://digi.tech.qq.com/a/20150209/008399.htm)
8.  [创新X-Fi声卡Console控制台](http://news.mydrivers.com/1/113/113577.htm)
9.  [kX改版驱动](http://news.mydrivers.com/1/113/113787.htm)
10. [创新声卡kX 1.0b2内部测试版MAC驱动](http://news.mydrivers.com/1/113/113896.htm)
11. [Audigy声卡](http://news.mydrivers.com/1/111/111563.htm)
12. [创新为改版驱动松绑
    X-Fi声卡DDL补丁下载](http://news.mydrivers.com/1/105/105811.htm)
13. [创新X-Fi Fatal1ty
    Pro声卡官方支持DDL](http://news.mydrivers.com/1/106/106895.htm)
14. [X-FI Crystalizer补丁放出](http://news.mydrivers.com/1/106/106152.htm)