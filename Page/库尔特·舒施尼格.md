[KurtVonSchuschnigg1936.jpg](https://zh.wikipedia.org/wiki/File:KurtVonSchuschnigg1936.jpg "fig:KurtVonSchuschnigg1936.jpg")

**库尔特·舒施尼格**（**Kurt Schuschnigg**，），原名**库尔特·冯·舒施尼格**（**Kurt von
Schuschnigg**），是一名[奥地利政治家](../Page/奥地利.md "wikilink")，在1934年接替被刺杀的[恩格尔伯特·陶尔斐斯成为](../Page/恩格尔伯特·陶尔斐斯.md "wikilink")[奥地利第一共和国的](../Page/奥地利第一共和国.md "wikilink")[总理](../Page/奥地利总理.md "wikilink")。

## 生平

### 早期生活

他在[里瓦德尔加尔达](../Page/里瓦德尔加尔达.md "wikilink")（在今[意大利](../Page/意大利.md "wikilink")[特倫托自治省](../Page/特倫托自治省.md "wikilink")，当时属于[奥匈帝国](../Page/奥匈帝国.md "wikilink")）出生，在[一战期间加入奥匈帝国军队](../Page/一战.md "wikilink")。战后，他在[弗赖堡大学和](../Page/弗赖堡大学.md "wikilink")[因斯布魯克大學修讀法學](../Page/因斯布魯克大學.md "wikilink")，並在[因斯布鲁克做](../Page/因斯布鲁克.md "wikilink")[律师](../Page/律师.md "wikilink")。

### 政治工作

1927年，他加入[基督教社会党](../Page/奥地利基督教社会党.md "wikilink")，并获选为[奥地利国民议会成员](../Page/國民議會_\(奧地利\).md "wikilink")。他不信任半军事国防组织[保安团](../Page/保安团_\(奥地利\).md "wikilink")
（Heimwehr），在1930年创立[东部边区冲锋队](../Page/东部边区冲锋队.md "wikilink")（Ostmärkische
Sturmscharen）。

1932年，[恩格尔伯特·陶尔斐斯任命许士尼格为司法部长](../Page/恩格尔伯特·陶尔斐斯.md "wikilink")，后者在1933年成为教育部长。1934年，陶尔斐斯被刺杀。舒施尼格继任为新任联邦总理。1936年10月，他解散了保安团。

### 德奥合并

1938年2月12日，他与[阿道夫·希特勒在](../Page/阿道夫·希特勒.md "wikilink")[贝希特斯加登会面](../Page/贝希特斯加登.md "wikilink")。希特勒迫使他让親纳粹的[阿图尔·赛斯-英夸特加入内阁](../Page/阿图尔·赛斯-英夸特.md "wikilink")。\[1\]舒施尼格返回奧地利後，不久宣佈將於3月13日举行[公民投票](../Page/公民投票.md "wikilink")，尝试维持大局。

希特勒聞訊後，先是要求舒施尼格取消公投，\[2\]等到舒施尼格同意後又要求他辭職，改由[阿圖爾·賽斯-英夸特接任](../Page/阿圖爾·賽斯-英夸特.md "wikilink")。總統[威廉·米克拉斯初時不願任命賽斯](../Page/威廉·米克拉斯.md "wikilink")-英夸特為總理，最終在德國的軍事威脅下屈服。舒施尼格在3月11日辭職。賽斯-英夸特成為總理後，德国军隊在3月12日早上就进驻奥地利。舒施尼格被纳粹党囚禁，在1945年美军进驻后才获得释放。\[3\]

### 战后生活

[二战过后](../Page/二战.md "wikilink")，他移民到[美国](../Page/美国.md "wikilink")，在1948至1967年在[圣路易大学任](../Page/圣路易大学.md "wikilink")[政治学教授](../Page/政治学.md "wikilink")。他于1977年在因斯布鲁克去世。

## 注释

## 参考文献

{{-}}

[Category:奥地利总理](../Category/奥地利总理.md "wikilink")
[Category:奥地利贵族](../Category/奥地利贵族.md "wikilink")
[Category:弗萊堡大學校友](../Category/弗萊堡大學校友.md "wikilink")
[Category:因斯布魯克大學校友](../Category/因斯布魯克大學校友.md "wikilink")

1.

2.
3.