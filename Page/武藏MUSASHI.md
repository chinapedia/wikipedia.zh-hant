，是[NHK在](../Page/日本放送協會.md "wikilink")2003年製播的[大河劇](../Page/NHK大河劇.md "wikilink")，放送期間是2003年1月5日至12月7日。

本劇以吉川英治的《宮本武藏》做為原作，原作原本只寫到巖流島決鬥而已，而本劇則是在後半部份衍生出巖流島以後的故事。在巖流島決鬥的那一集，用了三十台攝影機，嘗試製作出特殊的效果。

在每一回的標題裡，決大部份都用了驚嘆號「！」。起初宣布以[市川新之助](../Page/市川海老藏_\(11代目\).md "wikilink")（現在為[市川海老藏
(11代目)](../Page/市川海老藏_\(11代目\).md "wikilink")）、[米倉涼子](../Page/米倉涼子.md "wikilink")、[松岡昌宏為主角](../Page/松岡昌宏.md "wikilink")，又請來顏尼歐・莫利克奈作音樂，喧騰一時，極有話題性。但是開演之後，收視率每況愈下，到後來可說欲振乏力。包括女主角米倉涼子等演員與編劇鎌田敏夫等人，將整齣戲包裝得像是偶像劇一般，而這也正是前一齣大河劇「利家與松」被批評詬病的部份所在。

本劇在播出期間也製造出一些負面的新聞：在第一集裡的強盜襲擊場面被認為有抄襲黑澤明導演的電影「七武士」之嫌，因此在全劇播畢之後，由黑澤製作公司提出著作權遭侵害的告訴，不過黑澤公司敗訴。其次是劇中角色吉野太夫脫衣向宮本武藏獻身，這一幕遭到部分藝文界人士抗議，認為身為文化人的吉野太夫不該被演成如此不堪的女性。此外在演員的方面，男主角市川新之助被爆出與一名女歌手有私生女的醜聞，並且又和女主角米倉涼子因戲結緣相戀，也著實熱鬧了一陣子。

## 製作人員

  - 原作・・・[吉川英治](../Page/吉川英治.md "wikilink")《宮本武蔵》
  - 劇本・・・[鎌田敏夫](../Page/鎌田敏夫.md "wikilink")
  - 音樂・・・[恩尼奥·莫里科内](../Page/恩尼奥·莫里科内.md "wikilink")
  - 片頭曲演奏・・・羅馬交響樂團　
  - 片頭曲指揮・・・恩尼奥·莫里科内
  - 時代考証・・・二木謙一
  - 題字・・・・吉川壽一
  - 旁白・・・[橋爪功](../Page/橋爪功.md "wikilink")
  - 導演・・・・木村隆文、野田雄介、尾崎充信
  - 制作統括・・・一井久司、古川法一郎
  - 協力・・・[岡山縣](../Page/岡山縣.md "wikilink")、[兵庫縣](../Page/兵庫縣.md "wikilink")[姬路市](../Page/姬路市.md "wikilink")、[山梨縣](../Page/山梨縣.md "wikilink")[小淵澤町](../Page/小淵澤町.md "wikilink")

## 演出人員

  - [宮本武藏](../Page/宮本武藏.md "wikilink") -
    [増田貴久](../Page/増田貴久.md "wikilink")→[市川新之助
    (7代目)](../Page/市川海老藏_\(11代目\).md "wikilink")
  - 阿通 –
    [櫻井真琴](../Page/櫻井真琴.md "wikilink")→[米倉涼子](../Page/米倉涼子.md "wikilink")
  - 本位田又八 –
    [內山真人](../Page/內山真人.md "wikilink")→[堤真一](../Page/堤真一.md "wikilink")
  - [佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink") -
    [大高力也](../Page/大高力也.md "wikilink")→[松岡昌宏](../Page/松岡昌宏.md "wikilink")
  - 八重/琴 - [仲間由紀惠](../Page/仲間由紀惠.md "wikilink")
  - 阿篠 - [宮澤里惠](../Page/宮澤里惠.md "wikilink")
  - 阿甲 - [片瀨梨乃](../Page/片瀨梨乃.md "wikilink")
  - 朱實 - [內山理名](../Page/內山理名.md "wikilink")
  - 亞矢 - [寺島忍](../Page/寺島忍.md "wikilink")
  - 阿杉 - [中村玉緒](../Page/中村玉緒.md "wikilink")
  - 權六 - [谷啓](../Page/谷啓.md "wikilink")
  - [澤庵](../Page/澤庵宗彭.md "wikilink") -
    [渡瀨恆彥](../Page/渡瀨恆彥.md "wikilink")
  - [柳生宗矩](../Page/柳生宗矩.md "wikilink") -
    [中井貴一](../Page/中井貴一.md "wikilink")
  - [柳生宗嚴](../Page/柳生石舟齋.md "wikilink") -
    [藤田眞](../Page/藤田眞.md "wikilink")
  - [柳生利嚴](../Page/柳生兵庫助.md "wikilink") -
    [高島政伸](../Page/高島政伸.md "wikilink")
  - 阿凜 - [和久井映見](../Page/和久井映見.md "wikilink")
  - [祇園藤次](../Page/祇園藤次.md "wikilink") -
    [阿部寬](../Page/阿部寬.md "wikilink")
  - 茜屋絃三-[江守徹](../Page/江守徹.md "wikilink")
  - [新免無二齋](../Page/新免無二齋.md "wikilink") -
    [北野武](../Page/北野武.md "wikilink")
  - [真田幸村](../Page/真田信繁.md "wikilink") -
    [中村雅俊](../Page/中村雅俊.md "wikilink")
  - [柳生十兵衛](../Page/柳生十兵衛.md "wikilink") -
    [岩田佳也](../Page/岩田佳也.md "wikilink")
  - [吉岡清十郎](../Page/吉岡清十郎.md "wikilink") -
    [榎木孝明](../Page/榎木孝明.md "wikilink")
  - [穴戶梅軒](../Page/穴戶梅軒.md "wikilink") -
    [吉田榮作](../Page/吉田榮作.md "wikilink")
  - 勝 - [水野美紀](../Page/水野美紀.md "wikilink")
  - 阿吟 - [菊池麻衣子](../Page/菊池麻衣子.md "wikilink")
  - 內山半兵衛-[西田敏行](../Page/西田敏行.md "wikilink")
  - [鐘卷自齋](../Page/鐘卷自齋.md "wikilink") -
    [津嘉山正種](../Page/津嘉山正種.md "wikilink")
  - [本阿彌光悦](../Page/本阿彌光悦.md "wikilink") -
    [津川雅彥](../Page/津川雅彥.md "wikilink")
  - [德川家康](../Page/德川家康.md "wikilink") -
    [北村和夫](../Page/北村和夫.md "wikilink")
  - [德川秀忠](../Page/德川秀忠.md "wikilink") - [中村獅童
    (2代目)](../Page/中村獅童_\(2代目\).md "wikilink")
  - [細川忠興](../Page/細川忠興.md "wikilink") -
    [夏八木勳](../Page/夏八木勳.md "wikilink")
  - [細川忠利](../Page/細川忠利.md "wikilink") -
    [阪本浩之](../Page/阪本浩之.md "wikilink")
  - [伊達政宗](../Page/伊達政宗.md "wikilink") -
    [西村和彥](../Page/西村和彥.md "wikilink")
  - [大久保忠鄰](../Page/大久保忠鄰.md "wikilink") -
    [石橋雅史](../Page/石橋雅史.md "wikilink")
  - [藤堂高虎](../Page/藤堂高虎.md "wikilink") -
    [誠直也](../Page/誠直也.md "wikilink")
  - [豐臣秀賴](../Page/豐臣秀賴.md "wikilink") -
    [新晉一郎](../Page/新晉一郎.md "wikilink")
  - [大野治長](../Page/大野治長.md "wikilink") -
    [近藤正臣](../Page/近藤正臣.md "wikilink")
  - [大野治房](../Page/大野治房.md "wikilink") -
    [佐佐木主浩](../Page/佐佐木主浩.md "wikilink")
  - [明石全登](../Page/明石全登.md "wikilink") -
    [京本政樹](../Page/京本政樹.md "wikilink")
  - [池田輝政](../Page/池田輝政.md "wikilink") - [中村勘九郎
    (5代目)](../Page/中村勘九郎_\(5代目\).md "wikilink")
  - [真田大助](../Page/真田大助.md "wikilink")（真田幸昌） -
    [東新良和](../Page/東新良和.md "wikilink")
  - [片桐且元](../Page/片桐且元.md "wikilink") -
    [入川保則](../Page/入川保則.md "wikilink")
  - [本多正信](../Page/本多正信.md "wikilink") -
    [西鄉輝彥](../Page/西鄉輝彥.md "wikilink")
  - [本多正純](../Page/本多正純.md "wikilink") -
    [鷹龍太郎](../Page/鷹龍太郎.md "wikilink")
  - [板倉勝重](../Page/板倉勝重.md "wikilink") -
    [坂部文昭](../Page/坂部文昭.md "wikilink")
  - [阿茶局](../Page/雲光院.md "wikilink") - [泉晶子](../Page/泉晶子.md "wikilink")
  - \-{[淀殿](../Page/淀殿.md "wikilink")}- -
    [若尾文子](../Page/若尾文子.md "wikilink")
  - [常高院](../Page/常高院.md "wikilink") - [姿晴香](../Page/姿晴香.md "wikilink")
  - [千姬](../Page/德川千姬.md "wikilink") -
    [橋本愛實](../Page/橋本愛實.md "wikilink")
  - [豐臣國松](../Page/豐臣國松.md "wikilink") -
    [齊藤大河](../Page/齊藤大河.md "wikilink")
  - [出雲阿國](../Page/出雲阿國.md "wikilink") -
    [片岡京子](../Page/片岡京子.md "wikilink")
  - [和賀忠親](../Page/和賀忠親.md "wikilink") -
    [畠中洋](../Page/畠中洋.md "wikilink")
  - [吉野太夫](../Page/吉野太夫.md "wikilink") –
    [小泉今日子](../Page/小泉今日子.md "wikilink")
  - [吉岡傳七郎](../Page/吉岡傳七郎.md "wikilink") –
    [光石研](../Page/光石研.md "wikilink")
  - 原田休雪 – [遠藤憲一](../Page/遠藤憲一.md "wikilink")
  - 妙秀 – [淡路惠子](../Page/淡路惠子.md "wikilink")
  - 半瓦彌次兵衛 – [哀川翔](../Page/哀川翔.md "wikilink")
  - 菰之十郎 – [山崎裕太](../Page/山崎裕太.md "wikilink")
  - 城太郎 – [三浦春馬](../Page/三浦春馬.md "wikilink")
  - 阿菊 – [廣末涼子](../Page/廣末涼子.md "wikilink")
  - 夢想權之助 – [大柴邦彥](../Page/大柴邦彥.md "wikilink")
  - [利世](../Page/大谷安岐.md "wikilink") –
    [坂口良子](../Page/坂口良子.md "wikilink")
  - 露西雅 – [高橋惠子](../Page/高橋惠子.md "wikilink")
  - [阿六之方](../Page/養儼院.md "wikilink") –
    [須藤溫子](../Page/須藤溫子.md "wikilink")
  - [結姬](../Page/天秀尼.md "wikilink") –
    [上別府里枝](../Page/上別府里枝.md "wikilink")
  - [林彌](../Page/二代目吉野太夫.md "wikilink") –
    [廠乃繪琉](../Page/廠乃繪琉.md "wikilink")

[Category:2003年開播的日本電視劇集](../Category/2003年開播的日本電視劇集.md "wikilink")
[Category:吉川英治小說改編電視劇](../Category/吉川英治小說改編電視劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:室町時代背景電視劇](../Category/室町時代背景電視劇.md "wikilink")
[Category:戰國時代背景電視劇 (日本)](../Category/戰國時代背景電視劇_\(日本\).md "wikilink")
[Category:安土桃山時代背景電視劇](../Category/安土桃山時代背景電視劇.md "wikilink")
[Category:江戶時代背景電視劇](../Category/江戶時代背景電視劇.md "wikilink")
[Category:宮本武藏題材作品](../Category/宮本武藏題材作品.md "wikilink")