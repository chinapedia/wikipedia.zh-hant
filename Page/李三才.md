**李三才**（），[字](../Page/表字.md "wikilink")**道甫**，[號](../Page/號.md "wikilink")**修吾**。[陕西](../Page/陕西.md "wikilink")[临潼人](../Page/临潼.md "wikilink")，寄籍[顺天府](../Page/顺天府.md "wikilink")[通州](../Page/通州.md "wikilink")（今[北京市](../Page/北京市.md "wikilink")[通州區](../Page/通州區.md "wikilink")），為人揮霍有大略，明史稱其「英遇豪俊，傾動士大夫，皆負重名」\[1\]，[明朝官員](../Page/明朝.md "wikilink")，[東林黨領導人之一](../Page/東林黨.md "wikilink")。與[汪文言被稱為](../Page/汪文言.md "wikilink")「漕汪二賢」。

## 生平

世代為武功右衛的[軍官](../Page/軍官.md "wikilink")，移家至[順天府](../Page/順天府.md "wikilink")[通州的](../Page/通州.md "wikilink")[張家灣](../Page/張家灣.md "wikilink")（今[北京市](../Page/北京市.md "wikilink")[通州區東南](../Page/通州區.md "wikilink")）。順天府鄉試第一百二名。萬曆二年（1574年）進士\[2\]\[3\]\[4\]。曾官[戶部](../Page/戶部.md "wikilink")[主事](../Page/主事.md "wikilink")、[山東](../Page/山東.md "wikilink")[僉事](../Page/僉事.md "wikilink")、[河南](../Page/河南.md "wikilink")[參議](../Page/參議.md "wikilink")、[大理寺](../Page/大理寺.md "wikilink")[少卿等職](../Page/少卿.md "wikilink")。

[萬曆二十七年](../Page/萬曆.md "wikilink")（1599年）以[右僉都御史擔任](../Page/右僉都御史.md "wikilink")[漕運總督](../Page/漕運總督.md "wikilink")，[巡撫鳳陽諸府](../Page/鳳陽巡撫.md "wikilink")。当时税使[宦官陳增](../Page/宦官.md "wikilink")、魯保等橫行无忌，公开掠夺。李三才“以气凌之”，謂如「一旦眾叛土崩，則小民皆為敵國」，制裁矿监税使爪牙，并劾治宦官陈增，使陈增为之夺气，不敢横行。又劾治陈增参随程守训，获赃数十万，守训及其党羽被正法，人心大快。

三才亦屡次疏陈[矿税之害](../Page/矿税.md "wikilink")，指责明神宗[朱翊钧](../Page/朱翊钧.md "wikilink")“溺志货财”，不顾人民死活，并要求“罢除天下矿税”。又疏陈朝政废坏，请神宗奋然有为，经营[遼東](../Page/遼東.md "wikilink")，但皆不被采纳。李三才善笼络朝士，结交者遍天下。时[顾宪成讲学](../Page/顾宪成.md "wikilink")[東林書院](../Page/東林書院.md "wikilink")，他深与相结，得宪成信任。并尝请补大僚，选科道，录遗佚，意在擢用[东林党](../Page/东林党.md "wikilink")。

[万历中](../Page/万历.md "wikilink")，[内阁缺人](../Page/内阁.md "wikilink")，建议者谓内阁不当专用[词臣](../Page/翰林.md "wikilink")，宜参用地方长官，准备推荐三才入阁。致忌者日众，谤议纷然。朝臣亦分为劾救两派，聚讼不已。[顾宪成致书](../Page/顾宪成.md "wikilink")[大学士](../Page/大学士.md "wikilink")[叶向高](../Page/叶向高.md "wikilink")，力称三才廉直，支持其入阁。因此议者益哗，又引起[东林党与](../Page/东林党.md "wikilink")[齐楚浙党官僚之争](../Page/齐楚浙党.md "wikilink")。[萬曆三十九年](../Page/萬曆.md "wikilink")（1611年）三才愤而辞职。

[天启元年](../Page/天启.md "wikilink")（1621年）[後金](../Page/後金.md "wikilink")[可汗](../Page/可汗.md "wikilink")[努尔哈赤攻占](../Page/努尔哈赤.md "wikilink")[辽阳](../Page/辽阳.md "wikilink")，[御史](../Page/御史.md "wikilink")[房可壮请起用三才为](../Page/房可壮.md "wikilink")[辽东](../Page/辽东.md "wikilink")[经略](../Page/经略.md "wikilink")，遭反对作罢。

[天啟三年](../Page/天啟.md "wikilink")（1623年）起为[南京户部尚书](../Page/南京户部尚书.md "wikilink")，未赴任卒。死後遭[阉党追论](../Page/阉党.md "wikilink")，被夺封诰。[崇祯初年](../Page/崇祯.md "wikilink")（1628年）复赠官。

## 家族

曾祖父[李端](../Page/李端.md "wikilink")；祖父[李祿](../Page/李祿.md "wikilink")；父親[李珣](../Page/李珣.md "wikilink")。母朱氏\[5\]。

## 参考文献

[Category:东林党人](../Category/东林党人.md "wikilink")
[Category:明朝戶部尚書](../Category/明朝戶部尚書.md "wikilink")
[Category:明朝戶部主事](../Category/明朝戶部主事.md "wikilink")
[Category:明朝大理寺少卿](../Category/明朝大理寺少卿.md "wikilink")
[Category:明朝鳳陽巡撫](../Category/明朝鳳陽巡撫.md "wikilink")
[Category:明朝山東按察使司僉事](../Category/明朝山東按察使司僉事.md "wikilink")
[Category:明朝河南布政使司參議](../Category/明朝河南布政使司參議.md "wikilink")
[Category:通州人](../Category/通州人.md "wikilink")
[Category:臨潼人](../Category/臨潼人.md "wikilink")
[S](../Category/李姓.md "wikilink")

1.  《进士题名碑录》写李三才的户籍为陕西武功卫，乡贯为陕西临渔
2.
3.
4.
5.