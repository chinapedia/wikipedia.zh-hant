[Frith_A_Private_View.jpg](https://zh.wikipedia.org/wiki/File:Frith_A_Private_View.jpg "fig:Frith_A_Private_View.jpg")在1881年所畫的，為一幅諷刺唯美主義對服裝影響的畫像，在畫中[奥斯卡·王尔德在右方被一群仰慕者圍繞](../Page/奥斯卡·王尔德.md "wikilink")。\]\]

**唯美主义**（）主張[藝術哲學應獨立於所有](../Page/藝術哲學.md "wikilink")[哲學之外](../Page/哲學.md "wikilink")，[藝術只能以藝術自身的標準來評判](../Page/藝術.md "wikilink")\[1\]。

**唯美主义运动**（英语：）在汉语中有“**美学运动**”、“**唯美运动**”等不同译法，是於19世纪后期出现在[英国艺术和文学领域中的一场组织松散的](../Page/英国.md "wikilink")[反社會的](../Page/反社會.md "wikilink")[運動](../Page/社會運動.md "wikilink")，发生于[维多利亚时代晚期](../Page/维多利亚时代.md "wikilink")，大致从1868年延续至1901年，通常[学术界认为唯美主义运动的结束以](../Page/学术界.md "wikilink")[奥斯卡·王尔德被捕为标志](../Page/奥斯卡·王尔德.md "wikilink")。

## 緣起

唯美主义运动呈現著頹廢風氣，也有著對當時席捲著歐洲的[樂觀進步主義的反思](../Page/樂觀進步主義.md "wikilink")。英国的[颓废派作家们受](../Page/颓废派.md "wikilink")[瓦尔特·佩特的影响非常大](../Page/瓦尔特·佩特.md "wikilink")。佩特在1867年至1868年之间发表了一系列文章，主张人们应该热情的拥抱生活，追求生活的艺术化。颓废主义者们接受了这一观点。[法国](../Page/法国.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")[维克多·库辛和](../Page/维克多·库辛.md "wikilink")[奥菲尔·戈蒂埃在](../Page/奥菲尔·戈蒂埃.md "wikilink")[法国推广了这一](../Page/法国.md "wikilink")[观念](../Page/观念.md "wikilink")，提出“为艺术而艺术”的口号，并声称[艺术与](../Page/艺术.md "wikilink")[道德之间没有关联](../Page/道德.md "wikilink")。

## 主張

唯美主义运动中的作家和艺术家认为：艺术的使命在于为人类提供感观上的愉悦，而非传递某种道德或情感上的信息。因此，唯美主义者们拒绝接受[约翰·罗斯金和马修](../Page/约翰·罗斯金.md "wikilink")·阿诺德提出的“艺术是承载道德的实用之物”的[功利主义观点](../Page/功利主义.md "wikilink")。相反，唯美主义者认为[艺术不应具有任何说教的因素](../Page/艺术.md "wikilink")，而是追求单纯的[美感](../Page/美感.md "wikilink")。他们如痴如醉的追求[艺术的](../Page/艺术.md "wikilink")“美”，认为“美”才是[艺术的](../Page/艺术.md "wikilink")[本质](../Page/本质.md "wikilink")，并且主张[生活应该](../Page/生活.md "wikilink")[模仿](../Page/模仿.md "wikilink")[艺术](../Page/艺术.md "wikilink")。

## 唯美主义文学特征

[唯美主义运动的主要](../Page/唯美主义运动.md "wikilink")[特征包括](../Page/特征.md "wikilink")：追求建议性而非陈述性、追求感官享受、对[象征手法的大量应用](../Page/象征.md "wikilink")、追求[事物之间的关联感应](../Page/事物.md "wikilink")——即探求[语汇](../Page/语汇.md "wikilink")、[色彩和](../Page/色彩.md "wikilink")[音乐之间内在的联系](../Page/音乐.md "wikilink")。

人们一般认为唯美主义和彼时发生在[法国的](../Page/法国.md "wikilink")[象征主义或](../Page/象征主义.md "wikilink")[颓废主义运动同属一脉](../Page/颓废主义运动.md "wikilink")，是这场国际性文艺运动在英国的分支。这场运动是反[维多利亚风格风潮的一部分](../Page/维多利亚.md "wikilink")，具有后[浪漫主义的特征](../Page/浪漫主义.md "wikilink")。

唯美主义有時被與[惡魔主義連結](../Page/惡魔主義.md "wikilink")，但是對惡魔主義和頹廢藝術不一定相符，倒不如說是感性的和[文藝復興運動一起進行](../Page/文藝復興運動.md "wikilink")。有時也和[神秘主義結合](../Page/神秘主義.md "wikilink")。

## 唯美主义文学代表人物

唯美主义视[浪漫主义](../Page/浪漫主义.md "wikilink")[诗人](../Page/诗人.md "wikilink")[约翰·济慈和](../Page/约翰·济慈.md "wikilink")[雪莱为先驱](../Page/雪莱.md "wikilink")，也受到了[拉斐尔前派的影响](../Page/拉斐尔前派.md "wikilink")。

在英国，唯美主义最杰出的代表人物是[阿尔杰农·查尔斯－斯温伯恩](../Page/阿尔杰农·查尔斯－斯温伯恩.md "wikilink")（），和[奥斯卡·王尔德](../Page/奥斯卡·王尔德.md "wikilink")，这两人都接受过[法国](../Page/法国.md "wikilink")[象征主义的影响](../Page/象征主义.md "wikilink")。

和唯美主义运动有关联的艺术家包括[詹姆斯·麦克尼尔·惠斯勒和](../Page/詹姆斯·麦克尼尔·惠斯勒.md "wikilink")[但丁·加布里埃尔·罗塞蒂](../Page/但丁·加布里埃尔·罗塞蒂.md "wikilink")。

## 对唯美主义文学的評價

[英國](../Page/英國.md "wikilink")[詩人兼評論者](../Page/詩人.md "wikilink")，被譽為[維多利亞時代中期](../Page/維多利亞時代.md "wikilink")[反叛](../Page/反叛.md "wikilink")[詩人](../Page/詩人.md "wikilink")[象徵的阿尔杰农](../Page/象徵.md "wikilink")·斯温伯恩對此派系的評論為「這個畫派的意義是『美』本身」，說明了唯美主義的[本質](../Page/本質.md "wikilink")。

[法國天主教](../Page/法國天主教.md "wikilink")[玫瑰十字會的](../Page/玫瑰十字會.md "wikilink")[創始人](../Page/創始人.md "wikilink")、[神秘小說](../Page/神秘小說.md "wikilink")[作家和](../Page/作家.md "wikilink")[藝術](../Page/藝術.md "wikilink")[評論家](../Page/評論家.md "wikilink")[約瑟芬·佩拉當](../Page/約瑟芬·佩拉當.md "wikilink")（），其對此派系評價為「美是產生[感情讓](../Page/感情.md "wikilink")[觀念](../Page/觀念.md "wikilink")[昇華的歡樂](../Page/昇華.md "wikilink")」。

唯美主义也曾经受到来自杂志《笨拙》以及吉尔伯特和苏利文的小[歌剧](../Page/歌剧.md "wikilink")《忍耐》的嘲讽。

## 唯美主义视觉艺术

唯美主义思潮对[室内设计也产生了影响](../Page/室内设计.md "wikilink")。唯美主义的[室内设计师们喜欢以](../Page/室内设计师.md "wikilink")[孔雀](../Page/孔雀.md "wikilink")[羽毛和](../Page/羽毛.md "wikilink")[蓝](../Page/蓝.md "wikilink")[白相间的](../Page/白.md "wikilink")[中国](../Page/中国.md "wikilink")[瓷器作为装饰](../Page/瓷器.md "wikilink")。

在英国，它发生于[维多利亚时代晚期](../Page/维多利亚时代.md "wikilink")，大致从1868年延续至1901年。设计史研究中往往也将同时在美国出现的具有相似倾向的设计潮流归属于唯美运动。

有必要指出的是，尽管唯美运动和[艺术与工艺运动有时候显得针锋相对](../Page/艺术与工艺运动.md "wikilink")，但二者并行发展，并且共同处于维多利亚时代的[折衷主义气氛中](../Page/折衷主义.md "wikilink")，设计师之间常常颇有交往，彼此互相影响，有时候使用着共同的设计语言。唯美运动的核心思想认为，艺术的使命在于为人类提供感观上的愉悦，而非传递某种道德或情感上的信息。因此，唯美主义者拒绝接受[约翰·拉斯金那种把艺术与道德相联系的观点](../Page/约翰·拉斯金.md "wikilink")。他们认为艺术或其他装饰不应具有任何说教的因素，提倡“为艺术而艺术”，如痴如醉地追求超然于生活的纯粹美，追求形式完美和艺术技巧。他们过着[波希米亚主义](../Page/波希米亚主义.md "wikilink")（Bohemianism）的生活，即使贫穷，也热爱美甚于热爱生活。简言之，就是“唯美是求”。\[2\]

## 参考文献

## 延伸阅读

  -
## 外部链接

  - [Books, Research &
    Information](http://www.achome.co.uk/architecture/aesthetic.htm)

## 参见

  - [英国文学](../Page/英国文学.md "wikilink")
  - [奥斯卡·王尔德](../Page/奥斯卡·王尔德.md "wikilink")
  - [闻一多](../Page/闻一多.md "wikilink")

{{-}}

[Category:主义](../Category/主义.md "wikilink")
[唯美主义](../Category/唯美主义.md "wikilink")
[Category:藝術運動](../Category/藝術運動.md "wikilink")
[Category:文学流派](../Category/文学流派.md "wikilink")
[A](../Category/文化運動.md "wikilink")

1.
2.