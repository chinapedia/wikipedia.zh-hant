屬於蛋糕三大類中[麵糊類蛋糕](../Page/麵糊類蛋糕.md "wikilink")()裡極具代表性的一種，基本上在製作時採用固體油脂，利用其打發後包裹空氣之特性，使糕體麵糊加熱後易膨脹，產生鬆軟之質地，為避免其高油脂導致[油水分離](../Page/油水分離.md "wikilink")，配方內[乳化劑](../Page/乳化劑.md "wikilink")(如:[蛋黃](../Page/蛋黃.md "wikilink"))不可或缺，在傳統上**磅蛋糕**（）指的是用各重一[磅的麵粉](../Page/磅.md "wikilink")、油脂、食糖和雞蛋製成的蛋糕。

## 歷史

磅蛋糕的歷史可以追溯至18世紀初，而 **磅蛋糕** 一詞最早由Hannah Glasse 在她的*Art of
Cookery*（1747年出版）裡提及[1](http://www.poundcake.net/)，因為配方裡麵粉、油脂、食糖和雞蛋中的每一種都是一磅，因此配方容易記憶，而使磅蛋糕受到青睞。

## 外部資料

  - [磅蛋糕歷史](http://www.poundcake.net/)

<!-- end list -->

  - [磅蛋糕介紹及做法](http://www.cooksinfo.com/pound-cake)

## 參見

  - [蛋糕列表](../Page/蛋糕列表.md "wikilink")

## 參考資料

[Category:蛋糕](../Category/蛋糕.md "wikilink")