**屏遮那車站**位於[台灣](../Page/台灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[阿里山鄉](../Page/阿里山鄉.md "wikilink")，為[林務局](../Page/行政院農業委員會林務局.md "wikilink")[阿里山森林鐵路](../Page/阿里山森林鐵路.md "wikilink")[阿里山線之](../Page/阿里山線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

無

## 利用狀況

  - 阿里山線(主線)的停靠站。
  - 因受[八八風災影響](../Page/八八風災.md "wikilink")，暫時停駛。

## 車站週邊

## 歷史

  - 1912年12月25日 - 設立「平遮那驛」。
  - 1965年6月1日 - 更名為屏遮那\[1\]。

## 參考

<references />

## 外部連結

  - [屏遮那車站（阿里山森林鐵路）](http://railway.forest.gov.tw/ContentView.aspx?Type=15)

## 鄰近車站



[Category:1912年啟用的鐵路車站](../Category/1912年啟用的鐵路車站.md "wikilink")
[Category:阿里山鄉鐵路車站](../Category/阿里山鄉鐵路車站.md "wikilink")

1.  林務局(54) 5 28林產字第一九○一八號呈；收錄於臺灣省政府公報五十四年夏字第六十八期