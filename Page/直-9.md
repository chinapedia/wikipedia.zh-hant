**直-9**〈[英文](../Page/英文.md "wikilink")[翻譯](../Page/翻譯.md "wikilink")：**Z-9**〉是由[哈尔滨飞机制造公司引入](../Page/哈尔滨飞机制造公司.md "wikilink")[法国](../Page/法国.md "wikilink")[海豚直升机的许可生产的](../Page/歐直AS365海豚直升機.md "wikilink")[通用直升機](../Page/通用直升機.md "wikilink")。

## 歷史

1980年10月15日，中法雙方簽約，授權中國方面生產SA365N1型直升機\[1\]。

1982年，哈爾濱飛機公司完成首架直升机的装配\[2\]。

1983年，直-9機正式進入中國人民解放軍服役。

1990年，原法國授權之50架直升機均以完成生產組裝，其中28架为基本型直-9，20架为直-9A（相当于AS365N2）。哈飞另生產了两架直-9A-100型之直升機，開始尝试自行生產製造此型直升機。

1992年1月16日，中國自行製造的直-9試飛成功\[3\]；然而，在1993年9月時，中法雙方再度簽約生產22架直-9型直升機，另外還有8架民用型直-9\[4\]。

1994年10月23日，直-9B試飛成功，從此中國邁入自製直-9直升機的時代，並自行衍生出數種不同功能之直升機系列。\[5\]

## 衍生型

### 直-9B

於1992年1月16日，中國試飛了大多數零件自己製造的直-9B。直-9B的特色是用11葉的複合材料尾螺旋槳取代了AS365N2海豚式直升機的13葉尾螺旋槳。為輕型戰術運輸直升機，可運載10名全副武裝人員。

### 直-9C

[缩略图](https://zh.wikipedia.org/wiki/File:Chinese_Helicopter_Lands_on_HMS_Cornwall_MOD_45150752.jpg "fig:缩略图")
直-9C舰载型实际上是以直-9为基础改进的，和法国“海豚”的舰载型“黑豹”无太大关系。1987年12月2日，为[中國人民解放軍海軍改装的直](../Page/中國人民解放軍海軍.md "wikilink")-9C舰载直升机首飞成功。12月24日在舰上顺利降落，采用中国直升机设计所研制的快速着舰系留装置。定型后的C型加装了机头雷达，可挂载2枚“鱼-7”鱼雷执行反潜任务。鱼-7仿自中国渔民在海南岛捞获的美军MK-46鱼雷，性能接近于MK-46。

### 直-9G

直-9G[武裝直升機在基型上加强了装甲防护](../Page/武裝直升機.md "wikilink")，驾驶舱顶部安装有红箭-8反坦克导弹的观瞄制导装置。机身内取消后排座位，改为武器挂架的承力结构，机身两侧挂架共可挂载4枚红箭-8反坦克导弹，或火箭弹发射器（57-1型57mm火箭弹、90-1型90mm火箭弹），或23mm机炮等武器。该机用于执行反坦克、压制地面火力、突袭地面零散目标等火力支援任务。也可以用于运输、兵力机动、直升机空战、通信和救护等任务。直-9G是中國解放軍拥有专用武装直升机前的一个过渡，能提早培养解放軍陆军航空兵武装直升机队伍，待武直-10研制成功即可大展拳脚。从2000年向马里出口了2架直9武装型直升机以来，目前为止已向[马里](../Page/马里.md "wikilink")、[毛里塔尼亚](../Page/毛里塔尼亚.md "wikilink")、[老挝](../Page/老挝.md "wikilink")、[巴基斯坦等国家出口了](../Page/巴基斯坦.md "wikilink")18架直升机。

### 直-9W

[PLA_Harbin_Z-9W_2012_Hong_Kong.JPG](https://zh.wikipedia.org/wiki/File:PLA_Harbin_Z-9W_2012_Hong_Kong.JPG "fig:PLA_Harbin_Z-9W_2012_Hong_Kong.JPG")使用的直-9W武裝直升機\]\]
直-9W是供中國自身軍隊所使用的武装直升机，在直-9基础上发展的直-9W武装直升机可携带8枚反坦克导弹，也可以挂载火箭发射器，以及航空机炮。直-9W的主要作战任务是用来对付敌方坦克，因此携带红箭-8L反坦克导弹数量要比直-9G多。

### 直-9WA

[PLAAF_Harbin_Z-9WA.jpg](https://zh.wikipedia.org/wiki/File:PLAAF_Harbin_Z-9WA.jpg "fig:PLAAF_Harbin_Z-9WA.jpg")
直-9WA是直-9W的升级版，机身改为黑色涂装，该型机换装了不穿透机舱的弯梁式器挂梁，与老型号的扁担式相比，具有挂弹多、拆卸方便的优点。机头下部转塔中安装了新的探测器转塔，配有白光、热成像、激光等多个探测设备。机头罩采用新颖的滑道式代替传统的铰链式开启方式，使机头罩打开角度更大，提高了空间的利用率。并在机头罩气动、结构、钢度、驾驶员视界等的限制范围内满足昼夜观瞄装置的转动范围要求。直-9WA具有夜战能力。驾驶员配备有夜视头盔。

### 武直-19

武[直-19是哈飞研制的轻型武装](../Page/直-19.md "wikilink")[攻击直升机](../Page/攻击直升机.md "wikilink")，其设计源自于直-9W，采用串列式座舱布局、四页复合材料旋翼、函道式尾桨、外置4个武器外挂点和改为后三点式起落架等武装直升机的典型特色。主要用于战场侦察或与武直-10等中、重型武装直升机配合作战，也可独立执行作战任务。武直19原型机与2010年7月成功首飞。目前已经量产，已经装备解放军陆军各陆航旅及新组建的陆航团。

## 使用国

[缩略图](https://zh.wikipedia.org/wiki/File:Aft-deck_of_the_PLAN_frigate_Yi_Yang_\(FF_548\).jpg "fig:缩略图")\]\]

  -

<!-- end list -->

  - [人民解放军](../Page/中國人民解放軍.md "wikilink")
  - [陸軍航空兵](../Page/中國人民解放軍陸軍.md "wikilink") - 包括武直-九型，共裝備60架以上。
  - [海軍航空兵](../Page/中國人民解放軍海軍航空兵.md "wikilink")
  - [中國人民解放軍陸軍航空兵學院](../Page/中國人民解放軍陸軍航空兵學院.md "wikilink")
  - [中國人民解放軍駐香港部隊](../Page/中國人民解放軍駐香港部隊.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [玻利维亚空军订购](../Page/玻利维亚空军.md "wikilink")6架H-425

<!-- end list -->

  -

<!-- end list -->

  - [佛得角军队](../Page/佛得角军队.md "wikilink") - 2× Z-9.

<!-- end list -->

  -

<!-- end list -->

  - [柬埔寨皇家空军订购](../Page/柬埔寨皇家空军.md "wikilink")12架\[6\];

<!-- end list -->

  -

<!-- end list -->

  - [肯尼亚国防军](../Page/肯尼亚国防军.md "wikilink")4架Z-9WA\[7\]

<!-- end list -->

  -

<!-- end list -->

  - [老挝人民军空军](../Page/老挝人民军空军.md "wikilink")6× Z-9A.

<!-- end list -->

  -

<!-- end list -->

  - [马里空军](../Page/马里空军.md "wikilink")2× Z-9.

<!-- end list -->

  -

<!-- end list -->

  - [毛里塔尼亚空军](../Page/毛里塔尼亚空军.md "wikilink")1× Z-9，另一架已被击毁

<!-- end list -->

  -

<!-- end list -->

  - [纳米比亚空军](../Page/纳米比亚空军.md "wikilink")2× Z-9.

<!-- end list -->

  -

<!-- end list -->

  - [巴基斯坦海航](../Page/巴基斯坦海航.md "wikilink")12× Z-9EC ASW反潜直升机。

## 事故

  - 2014年柬埔寨陆军一架直-9坠毁，包括两名将军在内的五名军人身亡，另有一人重伤\[8\]。
  - 2015年喀麦隆空军一架直-9坠毁，两人受伤\[9\]。

## 大眾文化

  - [第五空間](../Page/第五空間.md "wikilink")（中國大陸連續劇）
  - 《[战地风云4](../Page/战地风云4.md "wikilink")》中搭配[CS/LM12型转管速射机枪](../Page/CS/LM12型转管速射机枪.md "wikilink")（艙門機槍，第2及第3乘员武器）並且作为[中国人民解放军陆军航空兵武器出现](../Page/中国人民解放军.md "wikilink")，主要用于对地步兵压制和人员输送。
  - 在《[战狼](../Page/战狼.md "wikilink")》电影中出现作为主要用于对地步兵压制和人员输送。
  - 《[我是特種兵 利刃出鞘](../Page/我是特種兵_利刃出鞘.md "wikilink")》中出現，於第18集中 由飛行員
    宋凱飛（任柯諾飾演）駕駛。進行藍軍追捕紅軍狙擊手演習行動

## 参考文献

## 外部链接

## 参见

  - [歐直AS365海豚直升機](../Page/歐直AS365海豚直升機.md "wikilink")
  - [UH-1直升機](../Page/UH-1直升機.md "wikilink")
  - [UH-60黑鷹直升機](../Page/UH-60黑鹰直升机.md "wikilink")
  - [CH-53E直升機](../Page/CH-53E直升機.md "wikilink")
  - [直-10](../Page/直-10.md "wikilink")
  - [直-19](../Page/直-19.md "wikilink")

{{-}}

[Category:军用直升机](../Category/军用直升机.md "wikilink")
[Category:中國解放軍直昇機](../Category/中國解放軍直昇機.md "wikilink")
[Category:中國人民解放軍海軍飛機](../Category/中國人民解放軍海軍飛機.md "wikilink")

1.

2.  <http://www.airforceworld.com/pla/z9-helicopter-china.htm>

3.

4.
5.  [中国向柬埔寨移交12架“直-9”直升飞机](http://www.apdnews.com/news/48675.html)
    ，亚太日报，2013年11月25日

6.

7.  <http://news.xinhuanet.com/mil/2010-01/14/content_12807752.htm>

8.  [Two generals, two pilots die as helicopter crashes in
    Cambodia](https://www.scmp.com/news/asia/article/1554379/2-generals-2-pilots-die-helicopter-crashes-cambodia)

9.  [Cameroon Air Force Z-9 damaged in crash
    landing](http://www.defenceweb.co.za/index.php?option=com_content&view=article&id=38973:cameroon-air-force-z-9-damaged-in-crash-landing&catid=35:Aerospace&Itemid=107),
    2015-04-30