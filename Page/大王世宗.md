《**大王世宗**》（）是[韓國](../Page/大韓民國.md "wikilink")[KBS電視台從](../Page/韓國放送公社.md "wikilink")2008年1月5日起播出的[大河連續劇](../Page/KBS大河連續劇.md "wikilink")，劇集描述[朝鮮太宗](../Page/朝鮮太宗.md "wikilink")、[世宗時期的情況](../Page/朝鮮世宗.md "wikilink")。2009年8月27日起，[BS日視在](../Page/BS日本.md "wikilink")[日本首播](../Page/日本.md "wikilink")。

## 登場人物及演員

### 主要角色

  - [金相庆](../Page/金相庆.md "wikilink") 飾演
    [朝鮮世宗李祹](../Page/朝鮮世宗.md "wikilink")
  - [李玹雨](../Page/李玹雨.md "wikilink") 飾演 幼年李祹（忠宁大君）
  - [金永哲](../Page/金永哲.md "wikilink") 飾演
    [朝鮮太宗](../Page/朝鮮太宗.md "wikilink")
  - [朴尚民](../Page/朴尚民.md "wikilink") 飾演
    [让宁大君李褆](../Page/让宁大君.md "wikilink")
  - [李寅](../Page/李寅_\(藝人\).md "wikilink") 飾演 青年李褆
  - [郑赞宇](../Page/鄭粲右.md "wikilink") 飾演 幼年李褆
  - [崔明吉](../Page/崔明吉_\(藝人\).md "wikilink") 飾演
    [元敬王后](../Page/元敬王后.md "wikilink")
  - [李贞贤](../Page/李贞贤.md "wikilink") 飾演
    [慎嫔金氏](../Page/慎嫔金氏.md "wikilink")
  - [李允智](../Page/李允智.md "wikilink") 飾演
    [昭宪王后](../Page/昭宪王后.md "wikilink")
  - [南志鉉](../Page/南志鉉.md "wikilink") 飾演 幼年昭宪王后
  - [李天熙](../Page/李天熙.md "wikilink") 飾演 [蔣英實](../Page/蔣英實.md "wikilink")

### 朝鲜王族

  - [盧英國](../Page/盧英國.md "wikilink") 飾演
    [朝鮮定宗李芳果](../Page/朝鮮定宗.md "wikilink")
  - [安成民](../Page/安成民.md "wikilink") 飾演
    [孝寧大君李補](../Page/孝寧大君.md "wikilink")
  - [禹泰雄](../Page/禹泰雄.md "wikilink") 飾演 幼年李補
  - [白承道](../Page/白承道.md "wikilink") 飾演
    [誠寧大君李褈](../Page/誠寧大君.md "wikilink")
  - [柳瑞真](../Page/柳瑞真.md "wikilink") 飾演 嬪宮金氏
  - [金成鈴](../Page/金成鈴.md "wikilink") 飾演
    [孝嬪金氏](../Page/孝嬪金氏.md "wikilink")
  - [姜京憲](../Page/姜京憲.md "wikilink") 飾演 楚宮粧
  - [尹永峻](../Page/尹永峻.md "wikilink") 飾演
    [敬寧君李裶](../Page/敬寧君.md "wikilink")
  - [李相燁](../Page/李相燁.md "wikilink") 飾演 世子[李珦](../Page/李珦.md "wikilink")
  - [徐俊英](../Page/徐俊英.md "wikilink") 飾演
    [首陽大君](../Page/首陽大君.md "wikilink")
  - [朱多英](../Page/朱多英.md "wikilink") 飾演
    [貞昭公主](../Page/貞昭公主.md "wikilink")

### 朝鲜朝廷

  - [崔鍾元](../Page/崔鍾元.md "wikilink") 飾演 [河崙](../Page/河崙.md "wikilink")
  - [郑东焕](../Page/郑东焕.md "wikilink") 飾演 [赵末生](../Page/赵末生.md "wikilink")
  - [金甲洙](../Page/金甲洙.md "wikilink") 飾演 [黄喜](../Page/黄喜.md "wikilink")
  - [李振宇](../Page/李振宇.md "wikilink") 飾演 [郑麟趾](../Page/郑麟趾.md "wikilink")
  - [李聖旻](../Page/李聖旻.md "wikilink") 飾演 [崔万理](../Page/崔万理.md "wikilink")
  - [李秉旭](../Page/李秉旭.md "wikilink") 飾演 [金宗瑞](../Page/金宗瑞.md "wikilink")
  - [李對淵](../Page/李對淵.md "wikilink") 飾演 [崔海山](../Page/崔海山.md "wikilink")
  - [尹基元](../Page/尹基元.md "wikilink") 飾演 [嚴自治](../Page/嚴自治.md "wikilink")
  - [崔尚勳](../Page/崔尚勳.md "wikilink") 飾演 [沈溫](../Page/沈溫.md "wikilink")
  - [崔周鳳](../Page/崔周鳳.md "wikilink") 飾演 [金漢老](../Page/金漢老.md "wikilink")
  - [金應洙](../Page/金應洙.md "wikilink") 飾演 [閔無咎](../Page/閔無咎.md "wikilink")
  - [金炯逸](../Page/金炯逸.md "wikilink") 飾演 [閔無恤](../Page/閔無恤.md "wikilink")
  - [金河均](../Page/金河均.md "wikilink") 飾演 [許租](../Page/許租.md "wikilink")
  - [李原種](../Page/李原種.md "wikilink") 飾演 [尹淮](../Page/尹淮.md "wikilink")
  - [朴永智](../Page/朴永智.md "wikilink") 飾演 [朴訔](../Page/朴訔.md "wikilink")
  - [赵成夏](../Page/赵成夏.md "wikilink") 飾演 [李随](../Page/李随.md "wikilink")

### 明

  - [河勇振](../Page/河勇振.md "wikilink") 飾演 [海寿](../Page/海寿.md "wikilink")
  - [高仁範](../Page/高仁範.md "wikilink") 飾演 [黄俨](../Page/黄俨.md "wikilink")
  - [吳承允](../Page/吳承允.md "wikilink") 飾演 [明英宗](../Page/明英宗.md "wikilink")
  - [李大路](../Page/李大路.md "wikilink") 飾演 [王振](../Page/王振.md "wikilink")
  - [郑柔美](../Page/郑柔美.md "wikilink") 飾演 韩多燕
  - [李丙植](../Page/李丙植.md "wikilink") 飾演 [朱高煦](../Page/朱高煦.md "wikilink")
  - [金学哲](../Page/金学哲.md "wikilink") 飾演 [黄瓚](../Page/黄瓚.md "wikilink")
  - [沈宇昌](../Page/沈宇昌.md "wikilink") 飾演 [呂震](../Page/呂震.md "wikilink")

### 女真人

  - [申東勳](../Page/申東勳.md "wikilink") 飾演 [李滿住](../Page/李滿住.md "wikilink")
  - [方馨珠](../Page/方馨珠.md "wikilink") 飾演 [童猛哥](../Page/孟特穆.md "wikilink")
  - [咸錫勳](../Page/咸錫勳.md "wikilink") 飾演 [童凡察](../Page/凡察.md "wikilink")

## 參見

  - [朝鮮世宗](../Page/朝鮮世宗.md "wikilink")
  - [朝鮮太宗](../Page/朝鮮太宗.md "wikilink")

## 外部連結

  - [韓國KBS官方網站](http://www.kbs.co.kr/drama/3jong/)
  - [BS日視官方網站](https://web.archive.org/web/20090902071044/http://www.bs4.jp/guide/drama/sejong/)


[Daewang Sejong](../Category/2008年韓國電視劇集.md "wikilink") [Daewang
Sejong](../Category/朝鮮太宗時期背景電視劇.md "wikilink") [Daewang
Sejong](../Category/正統時期背景電視劇.md "wikilink") [Daewang
Sejong](../Category/王子主角題材電視劇.md "wikilink") [Daewang
Sejong](../Category/漢城背景作品.md "wikilink")
[Category:朝鮮定宗時期背景電視劇](../Category/朝鮮定宗時期背景電視劇.md "wikilink")
[Category:朝鮮世宗時期背景電視劇](../Category/朝鮮世宗時期背景電視劇.md "wikilink")
[Category:朝鮮王朝君主主角題材電視劇](../Category/朝鮮王朝君主主角題材電視劇.md "wikilink")
[Category:朝鮮世宗題材作品](../Category/朝鮮世宗題材作品.md "wikilink")