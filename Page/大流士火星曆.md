**大流士火星曆**是一套為了未來殖民在[火星上的拓荒者而設計的](../Page/火星.md "wikilink")[曆法](../Page/曆法.md "wikilink")。它是由[航太工程師及](../Page/航太工程.md "wikilink")[政治學家](../Page/政治學.md "wikilink")[湯瑪斯·干加利創造](../Page/湯瑪斯·干加利.md "wikilink")，並以他的兒子大流士（**Darius**）命名。

## 年份長度及置閏

這個曆法的基礎時間單位是火星[太陽日](../Page/太陽日.md "wikilink")（Martian solar day,
或稱sol）及火星[回歸年](../Page/回歸年.md "wikilink")，與普通的回歸年有些許差別。一火星日比一地球日長39分鐘35.244秒，而一火星年的長度則為668.5907火星日，因此基本的[置閏](../Page/置閏.md "wikilink")[公式就是每十個火星年均由](../Page/公式.md "wikilink")6個669火星日的火星年及4個668火星日的火星年所組成。前者（雖然比[平年更常出現](../Page/平年.md "wikilink")，可是仍然是被稱作[閏年](../Page/閏年.md "wikilink")）為[奇數年份及能被](../Page/奇數.md "wikilink")10整除的年份。惟能被100整除的年份規定為平年；能被1000整除的年份為閏年；能被3000整除的年份為平年。

## 月曆的佈局

### 月份及週的排佈

火星年分為24個[月](../Page/月.md "wikilink")，每6個月的最初5個月的長度為28火星日，而最後一個月，除非因是閏年的最後一個月而包含閏日，否則其長度則為27火星日。

雖然這曆法與[格里曆相同](../Page/格里曆.md "wikilink")，每[週由](../Page/週.md "wikilink")7火星日組成，可是在每一個月的開始時，該週就會從週首重先開始。這是由於假如該月的長度為27火星日，這就會使原本的最後一週的最後一天消失，因此這種做法就能使曆法看起來較為規則。

### 月曆

在下方的月曆中，一週的各日名稱分別為太陽日（Sol Solis, So），月亮日（Sol Lunae, Lu），火星日（Sol Martius,
Ma），水星日（Sol Mercurii, Me），木星日（Sol Jovis, Jo），金星日（Sol Veneris,
Ve）及土星日（Sol Saturni, Sa）。

24個月的名字由Gangale命名為拉丁文十二星座和梵文交替使用。單數月為十二星座，雙數月為梵文。\[1\]

| Sagittarius（1月） |      | Dhanus（2月）    |      | Capricornus（3月） |
| --------------- | ---- | ------------- | ---- | --------------- |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Makara（4月）      |      | Aquarius（5月）  |      | Kumbha（6月）      |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Pisces（7月）      |      | Mina（8月）      |      | Aries（9月）       |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Mesha（10月）      |      | Taurus（11月）   |      | Rishabha（12月）   |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Gemini（13月）     |      | Mithuna（14月）  |      | Cancer（15月）     |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Karka（16月）      |      | Leo（17月）      |      | Simha（18月）      |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Virgo（19月）      |      | Kanya（20月）    |      | Libra（21月）      |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |
|                 |      |               |      |                 |
| Tula（22月）       |      | Scorpius（23月） |      | Vrishika（24月）   |
| *So*            | *Lu* | *Ma*          | *Me* | *Jo*            |
| 1               | 2    | 3             | 4    | 5               |
| 8               | 9    | 10            | 11   | 12              |
| 15              | 16   | 17            | 18   | 19              |
| 22              | 23   | 24            | 25   | 26              |

24月的最後一日為閏日，並不是每年都有的。

### 節日

  - 1月1日：[春分](../Page/春分.md "wikilink")
  - 6月12日：[遠地點](../Page/遠地點.md "wikilink")
  - 7月27日：[夏至](../Page/夏至.md "wikilink")
  - 14月11日：[秋分](../Page/秋分.md "wikilink")
  - 18月12日：[近地點](../Page/近地點.md "wikilink")
  - 19月14日：[冬至](../Page/冬至.md "wikilink")

24月28日：[閏日](../Page/閏日.md "wikilink")

備註：這些節日的日期會因置閏而變動。

[Category:曆法](../Category/曆法.md "wikilink")
[Category:火星](../Category/火星.md "wikilink")

1.  The names of the 24 months were provisionally chosen by Gangale as
    the Latin names of constellations of the zodiac and their Sanskrit
    equivalents in alternation.