**方力鈞**1963年生于[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[邯郸市](../Page/邯郸市.md "wikilink")，他是一名职业[画家](../Page/画家.md "wikilink")。在[中国当代艺术界颇有影响](../Page/中国当代艺术.md "wikilink")，也是在[国际](../Page/国际.md "wikilink")[艺术舞台中最具知名度的中国画家之一](../Page/艺术.md "wikilink")。其作品曾在[世界各地巡回展出](../Page/世界.md "wikilink")，并被许多知名的[画廊和收藏家收藏](../Page/画廊.md "wikilink")。他曾在2003年电影《[绿茶](../Page/绿茶.md "wikilink")》（Green
Tea）中担任主演与[姜文](../Page/姜文.md "wikilink")、[赵薇等同台演出](../Page/赵薇.md "wikilink")。

## 生平

1989年毕业于[中央美术学院版画系](../Page/中央美术学院.md "wikilink")。

## 主要展览

  - 1993年《第45届[威尼斯双年展](../Page/威尼斯双年展.md "wikilink")》
    所在地：[意大利](../Page/意大利.md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink")
  - 1993年《中国前卫艺术展》
    所在地[德国](../Page/德国.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[英国](../Page/英国.md "wikilink")、[丹麦巡回展出](../Page/丹麦.md "wikilink")
  - 1994年《第22届[圣保罗双年展](../Page/圣保罗双年展.md "wikilink")》
    所在地：[巴西](../Page/巴西.md "wikilink")[圣保罗](../Page/圣保罗.md "wikilink")
  - 1995年《我们的世纪》
    所在地：[路德维希博物馆](../Page/路德维希博物馆.md "wikilink")，[德国](../Page/德国.md "wikilink")[科隆](../Page/科隆.md "wikilink")
  - 1995年个人作品展 [Bellefroid画廊](../Page/Bellefroid画廊.md "wikilink")
    所在地：[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")
  - 1995年个人作品展 [Serieuse
    Zaken画廊](../Page/Serieuse_Zaken画廊.md "wikilink")
    所在地：[荷兰](../Page/荷兰.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")
  - 1996年《中国！》
    所在地：[德国](../Page/德国.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、[波兰](../Page/波兰.md "wikilink")、[丹麦巡回展出](../Page/丹麦.md "wikilink")
  - 1996年《北京，不，不是肥皂剧》 所在地：[Marstall画廊](../Page/Marstall画廊.md "wikilink")
    [德国](../Page/德国.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")
  - 1996年《与中国对话》
    所在地：[路德维希论坛](../Page/路德维希论坛.md "wikilink")[德国](../Page/德国.md "wikilink")
  - 1996年《题目》 所在地：[广岛现代艺术博物馆](../Page/广岛现代艺术博物馆.md "wikilink")
    [日本](../Page/日本.md "wikilink")[广岛](../Page/广岛.md "wikilink")
  - 1996年《四个交叉点》
    所在地：[法兰西画廊](../Page/法兰西画廊.md "wikilink")[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")
  - 1996年《大艺术展》
    所在地：[试验版画特展](../Page/试验版画特展.md "wikilink")[美术之家](../Page/美术之家.md "wikilink")[德国](../Page/德国.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")
  - 1998年《黑与白：当代中国》
    所在地：[英国](../Page/英国.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")
  - 1999年《第48届[威尼斯双年展](../Page/威尼斯双年展.md "wikilink")》
    所在地：[意大利](../Page/意大利.md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink")
  - 2000年《二十世纪中国油画展》
    所在地：[中国美术馆](../Page/中国美术馆.md "wikilink")，中国北京[上海双年展](../Page/上海双年展.md "wikilink")
    所在地：[上海美术馆](../Page/上海美术馆.md "wikilink")，[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")
  - 2000年个人作品展
    所在地：[斯民艺苑](../Page/斯民艺苑.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")
  - 2001年《新形象：中国当代绘画二十年》
    所在地：[中国](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[成都](../Page/成都.md "wikilink")、[广州巡回展出](../Page/广州.md "wikilink")
  - 2002年首届《广州当代艺术三年展》
    所在地：[广东美术馆](../Page/广东美术馆.md "wikilink")[中国](../Page/中国.md "wikilink")[广州](../Page/广州.md "wikilink")
  - 2001年个人作品展
    所在地：[亚洲当代艺术](../Page/亚洲当代艺术.md "wikilink")，[德国](../Page/德国.md "wikilink")[柏林](../Page/柏林.md "wikilink")
  - 2002年个人作品展 所在地：[Ludwig
    Forum](../Page/Ludwig_Forum.md "wikilink")，[德国](../Page/德国.md "wikilink")
  - 2002年个人作品展
    所在地：[汉雅轩](../Page/汉雅轩.md "wikilink")，[中国](../Page/中国.md "wikilink")[香港](../Page/香港.md "wikilink")
  - 2003年《你好，中国？:中国当代艺术展》
    所在地：[蓬皮杜艺术中心](../Page/蓬皮杜艺术中心.md "wikilink")，[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")

## 外部链接

  - [Saatchi
    画廊](http://www.saatchi-gallery.co.uk/artists/fang_lijun.htm)
  - [Thomas Erben
    画廊](http://www.thomaserben.com/artists/fang_lijun/2005/fung.php)
  - [Hanmo 画廊](http://www.hanmo.com.cn/daili/efanglijun.htm)

[L](../Page/category:方姓.md "wikilink")

[Category:河北画家](../Category/河北画家.md "wikilink")
[Category:中央美术学院校友](../Category/中央美术学院校友.md "wikilink")
[Category:中国当代艺术家](../Category/中国当代艺术家.md "wikilink")