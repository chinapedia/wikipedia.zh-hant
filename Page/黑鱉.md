**黑鱉**（[學名](../Page/學名.md "wikilink")：**，曾属盾鳖属
Aspideretes），是一種鱉類，又叫黑龜，背甲最長91cm，生活在河邊，專門吃[魚](../Page/魚.md "wikilink")、[蝦](../Page/蝦.md "wikilink")、等。[繁殖期不詳](../Page/繁殖期.md "wikilink")。

2009年，世界上只有一个黑鳖群，在[孟加拉国](../Page/孟加拉国.md "wikilink")[吉大港附近一个寺庙](../Page/吉大港.md "wikilink")(Baizid
Bostami)里的人工池塘\[1\]。因当地的信仰，政府不准将这群鳖放出野外。然而，最近發現在[阿薩姆邦的](../Page/阿薩姆邦.md "wikilink")[雅魯藏布江仍然有一个种群](../Page/雅魯藏布江.md "wikilink")。

## 参考

[Category:鱉亞科](../Category/鱉亞科.md "wikilink")
[Category:孟加拉動物](../Category/孟加拉動物.md "wikilink")

1.