這些是經常用在[天文學](../Page/天文學.md "wikilink")，特別是專業天文學上的標誌或符號。

## 年齡（恆星的）

  - τ – 年齡

## 天體測量的參數

[天體測量](../Page/天體測量.md "wikilink") 的參數

  - R<sub>v</sub> - [徑向速度](../Page/徑向速度.md "wikilink")
  - μ - [自行](../Page/自行.md "wikilink")
  - π - [視差](../Page/視差.md "wikilink")
  - J - [曆元](../Page/曆元.md "wikilink")

## 距離的描述

距離的描述：使用在軌道與非軌道的參數

  - d – 距離
      - km = [公里](../Page/公里.md "wikilink")
      - mi = [英哩](../Page/英哩.md "wikilink")，
        *注意：這不是度量衡的[SI單位](../Page/國際單位.md "wikilink")*
      - AU = [天文單位](../Page/天文單位.md "wikilink")
      - ly = [光年](../Page/光年.md "wikilink")
      - kly = 千光年（1,000光年）
      - pc = [秒差距](../Page/秒差距.md "wikilink")
      - kpc = 千秒差距（1,000秒差距）
  - D<sub>L</sub> - [發光度距離](../Page/發光度距離.md "wikilink")，*僅使用外表的直觀得到的距離*

## 星系的分類

[星系的類型和光譜分類](../Page/星系.md "wikilink")：

  - 參考[星系分類](../Page/星系分類.md "wikilink")

## 發光度的分類

[發光度分類](../Page/發光度.md "wikilink")：

  - L<sub>S</sub> - 與[太陽比較的發光度](../Page/太陽.md "wikilink")
  - L<sub>ʘ</sub> - 與[太陽比較的發光度](../Page/太陽.md "wikilink")
  - L<sub>acc</sub> - Accretion luminosity

## 質量的分類

[質量分類](../Page/質量.md "wikilink")：

  - M<sub>E</sub> - 與[地球比較的質量](../Page/地球.md "wikilink")
  - M<sub>⊕</sub> -與[地球比較的質量](../Page/地球.md "wikilink")
  - M<sub>J</sub> - 與[木星比較的質量](../Page/木星.md "wikilink")
  - M<sub>♃</sub> - 與[木星比較的質量](../Page/木星.md "wikilink")
  - M<sub>S</sub> - 與[太陽比較的質量](../Page/太陽.md "wikilink")
  - M<sub>ʘ</sub> - 與[太陽比較的質量](../Page/太陽.md "wikilink")
  - M<sub>acc</sub> - 吸積質量

## 金屬性的分類

[金屬性分類](../Page/金屬性.md "wikilink")：

  - Fe/H – 鐵對氫的比例，以對數表示，*恆星與[太陽比較的鐵相對於氫的豐度](../Page/太陽.md "wikilink")*
  - Z - 金屬性
  - Z<sub>S</sub> - 與[太陽比較的金屬性](../Page/太陽.md "wikilink")
  - Z<sub>ʘ</sub> - 與[太陽比較的金屬性](../Page/太陽.md "wikilink")

## 軌道參數

宇宙中天體的[軌道參數](../Page/軌道元素.md "wikilink")：

  - α - [RA](../Page/赤經.md "wikilink")，*赤經，如果無法使用希臘字母呈現，可以表示為字母**á**。*
  - δ - [Dec](../Page/赤緯.md "wikilink")，*赤緯，如果無法使用希臘字母呈現，可以表示為字母**ä**。*

<!-- end list -->

  - P或P<sub>orb</sub> - [軌道週期](../Page/軌道週期.md "wikilink")
  - a - [半長徑](../Page/半長徑.md "wikilink")
  - b - [半短徑](../Page/半短徑.md "wikilink")
  - q - [近星點](../Page/近點.md "wikilink")，*最短的距離*
  - Q – [遠星點](../Page/遠點.md "wikilink")，*最大的距離*
  - e - [離心率](../Page/軌道離心率.md "wikilink")
  - i - [軌道傾角](../Page/軌道傾角.md "wikilink")
  - Ω - [升交點黃經](../Page/升交點黃經.md "wikilink")
  - ω - [近星點引數](../Page/近星點引數.md "wikilink")

<!-- end list -->

  - R<sub>L</sub> - [洛希瓣](../Page/洛希瓣.md "wikilink")（Roche lobe）
  - M - [平近點角](../Page/平近點角.md "wikilink")
  - M<sub>o</sub> - 在曆元的平近點角

## 半徑的分類

[半徑分類](../Page/半徑.md "wikilink")：

  - R<sub>E</sub> - 與[地球比較的半徑](../Page/地球.md "wikilink")
  - R<sub>⊕</sub> - 與[地球比較的半徑](../Page/地球.md "wikilink")
  - R<sub>J</sub> - 與[木星比較的半徑](../Page/木星.md "wikilink")
  - R<sub>♃</sub> - 與[木星比較的半徑](../Page/木星.md "wikilink")
  - R<sub>S</sub> - 與[太陽比較的半徑](../Page/太陽.md "wikilink")
  - R<sub>ʘ</sub> - 與[太陽比較的半徑](../Page/太陽.md "wikilink")

## 光譜的分類

[光譜分類](../Page/天文的光譜.md "wikilink")：

  - 參考[恆星光譜](../Page/恆星光譜.md "wikilink")
  - m - [視星等](../Page/視星等.md "wikilink")
  - M<sub>V</sub> - [絕對星等](../Page/絕對星等.md "wikilink")，*使用於星系和恆星*
  - H – 絕對星等，*使用於行星和非星天體*

## 溫度的描述

[溫度描述](../Page/溫度.md "wikilink")：

  - [T<sub>eff</sub>](../Page/有效溫度.md "wikilink") -有效溫度，*通常與發光的天體相關*
  - T<sub>max</sub> - 最高溫度，*通常與不發光的天體相關*
  - T<sub>avg</sub> - 平均溫度，*通常與不發光的天體相關*
  - T<sub>min</sub> - 最低溫度，*通常與不發光的天體相關*
  - [K](../Page/熱力學溫標.md "wikilink") –
    [凱氏溫度](../Page/熱力學溫標.md "wikilink")

## 相關條目

  - [天文學的縮寫列表](../Page/天文學的縮寫列表.md "wikilink")
  - [天文符號](../Page/天文符號.md "wikilink")
  - [恆星光譜](../Page/恆星光譜.md "wikilink")
  - [星系分類](../Page/星系分類.md "wikilink")

[C](../Category/天文學列表.md "wikilink") [天文學](../Category/符號.md "wikilink")