**法國法郎**（符号：₣，简写为FF或F），曾是[法国以及](../Page/法国.md "wikilink")[摩纳哥和](../Page/摩纳哥.md "wikilink")[安道尔的流通货币](../Page/安道尔.md "wikilink")。现在，这些国家也都已经转用[欧元](../Page/欧元.md "wikilink")。因為[法語的讀法](../Page/法語.md "wikilink")，書寫時法郎的符號習慣放置在數字之後（如22₣）。

## 历史

法國法郎的[硬幣最早在](../Page/硬幣.md "wikilink")1360年法國國王[約翰二世时鑄造](../Page/约翰二世_\(法兰西\).md "wikilink")。[法国大革命后推行公制](../Page/法国大革命.md "wikilink")，1795年，法郎開始作爲标准貨幣在法國流通，取代原有的[里弗尔](../Page/里弗尔.md "wikilink")。1法郎等于100生丁（centimes），含5克成色90%的[白银](../Page/白银.md "wikilink")。此外还铸造了重6.45克、含[黄金](../Page/黄金.md "wikilink")5.805克、价值20法郎的拿破仑金币。

法郎最初为[金银复本位货币](../Page/金银复本位.md "wikilink")。1865年在法国的倡导下，法国、[意大利](../Page/意大利.md "wikilink")、[瑞士和](../Page/瑞士.md "wikilink")[比利时在巴黎召开会议](../Page/比利时.md "wikilink")，宣布组建[拉丁货币同盟](../Page/拉丁货币同盟.md "wikilink")（[希腊](../Page/希腊.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[罗马尼亚](../Page/罗马尼亚.md "wikilink")、[委内瑞拉等国后来加入](../Page/委内瑞拉.md "wikilink")），实施基于法国体系的金银复本位货币制度。其成员国承诺只铸造100法郎、50法郎、20法郎、10法郎和5法郎的金币，以及5、2、1法郎和50、20生丁的银币。美国在[南北战争之后发行的](../Page/南北战争.md "wikilink")5美元鹰徽金币和英国的主權金币（Sovereign，合30先令或1.5英镑）价值均接近25法郎，因此两国提出以法国发行25法郎金币的方式同拉丁货币同盟建立联系，但这一建议被担心货币重铸成本的法国拒绝。

由于金银比价在19世纪后期不断跳动，导致金银复本位制度的崩溃。在法国，金银比价从1807年至1870年的1:15变为1874年的1:16.17，黄金大量流出，法国不得不于1876年实行[金本位制度](../Page/金本位.md "wikilink")，废止5法郎银币的流通，并规定法郎含金量为0.2903225克。

[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，法国采取纸币通货膨胀的政策，法郎发生贬值，到1922年时，其购买力已跌至1915年的43%，同英镑的比价从1914年的25.22:1跌到1926年的240.2:1。法国不得不终止金本位的实行。1928年法国制订新货币法，规定法郎含金量为0.065克，仅相当于一战前的1/5。

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，法国再度发生恶性通货膨胀，同英镑比价跌至接近1:1000。1960年發行新法郎（Nouveau
Franc），確定其幣值為舊法郎的100倍，相当于0.1802克黃金。

2002年1月1日歐元發行之後，法郎已逐漸的停止流通，法國的國家銀行，法蘭西銀行在2002年已停止兌換某些法郎舊鈔，而2005年後，舊鈔中的高乃依（Corneille）系列（有5法郎、100法郎）或10法郎白遼士（Berlioz）系列的鈔票也不能再換歐元。屆時在法國流通的法郎將只剩下幾種（可用到2012年2月17日），分別是：

  - 500法郎“[皮埃爾和](../Page/皮埃爾·居里.md "wikilink")[玛丽·居里](../Page/玛丽·居里.md "wikilink")”（Pierre
    et Marie Curie，1995年3月發行），
  - 200法郎的“[居斯塔夫·艾菲爾](../Page/居斯塔夫·艾菲爾.md "wikilink")”（Gustave
    Eiffel，1996年10月發行）
  - 100法郎的“[塞尚](../Page/塞尚.md "wikilink")”（Paul Cezanne，1997年12月發行），
  - 50法郎的“[聖·埃克蘇佩里](../Page/聖·埃克蘇佩里.md "wikilink")”（Saint-Exupery，1993年10月發行）
  - 20法郎的“[德彪西](../Page/德彪西.md "wikilink")” （Debussy，1981年10月發行）。

下列舊鈔則可用到2009年

  - 500法郎的“[帕斯卡](../Page/布莱士·帕斯卡.md "wikilink")”（Pascal，1969年1月發行），
  - 200法郎的“[孟德斯鸠](../Page/孟德斯鸠.md "wikilink")” （Montesquieu，1982年7月發行），
  - 100法郎的“[德拉克罗瓦](../Page/德拉克罗瓦.md "wikilink")”（Delacroix，1979年8月發行）
  - 50法郎的“[莫里斯·康坦·德·拉圖爾](../Page/莫里斯·康坦·德·拉圖爾.md "wikilink")” （Quentin
    de La Tour，1977年4月發行）

## 参考文献

## 外部链接

  - [法国纸币](http://www.bis-ans-ende-der-welt.net/Frankreich-B.htm)

[Category:已废止的法国货币](../Category/已废止的法国货币.md "wikilink")
[Category:法郎](../Category/法郎.md "wikilink")