**類固醇激素**（英語：**Steroid
hormone**、又稱**甾体激素**），是一类四环[脂肪烃](../Page/脂肪烃.md "wikilink")[化合物](../Page/化合物.md "wikilink")，具有[环戊烷多氢](../Page/环戊烷.md "wikilink")[菲母核](../Page/菲.md "wikilink")。类固醇激素是作为激素的类固醇。
这些包括性腺的性激素和肾上腺皮质的皮质类固醇。
根据它们结合的类固醇受体，哺乳动物类固醇激素可分为五组：[糖皮质激素](../Page/糖皮质激素.md "wikilink")，[盐皮质激素](../Page/盐皮质激素.md "wikilink")，[雄激素](../Page/雄激素.md "wikilink")，[雌激素和](../Page/雌激素.md "wikilink")[孕激素](../Page/孕激素.md "wikilink")。

## 分类

  - 按药理作用分
      - [性激素](../Page/性激素.md "wikilink")
      - [皮质激素](../Page/皮质激素.md "wikilink")
  - 按化学结构分 ([甾烷母核结构](../Page/:File:Gonane.png.md "wikilink"))
      - [雄甾烷类](../Page/雄甾烷类.md "wikilink")
        ([化学结构](../Page/:File:Androstane.png.md "wikilink"))
      - [雌甾烷类](../Page/雌甾烷类.md "wikilink")
        ([化学结构](../Page/:File:Estrane.png.md "wikilink"))
      - [孕甾烷类](../Page/孕甾烷类.md "wikilink")
        ([化学结构](../Page/:File:Pregnane.png.md "wikilink"))
  - 按药学分
      - [甾体雌激素](../Page/甾体雌激素.md "wikilink")
      - [非甾体雌激素](../Page/非甾体雌激素.md "wikilink")
      - [抗雌激素](../Page/抗雌激素.md "wikilink")
      - [雄性激素](../Page/雄性激素.md "wikilink")
      - [蛋白同化激素](../Page/蛋白同化激素.md "wikilink")
      - [孕激素](../Page/孕激素.md "wikilink")
      - [甾体避孕药](../Page/甾体避孕药.md "wikilink")
      - [抗孕激素](../Page/抗孕激素.md "wikilink")
      - [肾上腺皮质激素](../Page/肾上腺皮质激素.md "wikilink")

## 功能作用

  - 甾体激素具有极重要的医药价值，在维持[生命](../Page/生命.md "wikilink")、调节[性功能](../Page/性功能.md "wikilink")，对机体发展、[免疫调节](../Page/免疫.md "wikilink")、[皮肤疾病治疗及](../Page/皮膚病.md "wikilink")[生育控制方面有明确的作用](../Page/生育控制.md "wikilink")
  - 甾体激素药物的发现与发展也是[药物化学学科发展的重要阶段](../Page/药物化学.md "wikilink")

## 发现及发展

  - 甾体激素是在研究[哺乳动物](../Page/哺乳动物.md "wikilink")[内分泌系统时发现的](../Page/内分泌系统.md "wikilink")[内源性物质](../Page/内源性物质.md "wikilink")
  - 1932年至1939年间，从[腺体中获得](../Page/腺体.md "wikilink")[雌酮](../Page/雌酮.md "wikilink")([Estrone](../Page/w:Estrone.md "wikilink"),1932年)、[雌二醇](../Page/雌二醇.md "wikilink")([Estradiol](../Page/w:Estradiol.md "wikilink"),1932年)、[睾酮](../Page/睾酮.md "wikilink")([Testosterone](../Page/w:Testosterone.md "wikilink"),1935年)及[皮质酮](../Page/皮质酮.md "wikilink")([Corticosterone](../Page/w:Corticosterone.md "wikilink"),1939年)等的纯品[结晶](../Page/结晶.md "wikilink")，之后又阐明了其化学结构，从此开创了[甾体化学和](../Page/甾体化学.md "wikilink")[甾体药物化学的新领域](../Page/甾体药物化学.md "wikilink")。
  - 随后，又有许多重大的成就
  - 发明用[薯蓣皂甙](../Page/薯蓣皂甙.md "wikilink")(Diosgenin)为原料进行[半合成生产甾体药物](../Page/半合成.md "wikilink")，使生产规模扩大，成本降低
  - 发现[肾上腺皮质激素治疗](../Page/肾上腺皮质激素.md "wikilink")[风湿性关节炎及其在](../Page/风湿性关节炎.md "wikilink")[免疫调节上的重要价值](../Page/免疫调节.md "wikilink")，使甾体药物成为医院中不可缺少的药物
  - 甾体口服[避孕药的研究成功](../Page/避孕药.md "wikilink")，使人类生育控制达到了新水平

## 参见

  - [激素](../Page/激素.md "wikilink")
  - [药物列表](../Page/药物列表.md "wikilink")

## 延伸閱讀

  - Brook CG. Mechanism of puberty. Horm Res. 1999;51 Suppl 3:52–4.
    Review.PMID 10592444
  - Holmes SJ, Shalet SM. Role of growth hormone and sex steroids in
    achieving and maintaining normal bone mass. Horm Res.
    1996;45(1–2):86–93. Review. PMID 8742125
  - Ottolenghi C, Uda M, Crisponi L, Omari S, Cao A, Forabosco A,
    Schlessinger D. Determination and stability of sex. Bioessays. 2007
    Jan;29(1):15–25. Review. PMID 17187356
  - Couse JF, Korach KS. Exploring the role of sex steroids through
    studies of receptor deficient mice. J Mol Med. 1998
    Jun;76(7):497–511. Review. PMID 9660168
  - McEwen BS. Steroid hormones: effect on brain development and
    function. Horm Res. 1992;37 Suppl 3:1–10. Review. PMID 1330863
  - Simons SS Jr. What goes on behind closed doors: physiological versus
    pharmacological steroid hormone actions. Bioessays. 2008
    Aug;30(8):744–56. PMID 18623071

## 外部連結

  - [An animated and narrated tutorial about nuclear receptor
    signaling](https://web.archive.org/web/20090407060724/http://www.nursa.org/template.cfm?threadId=11320)
  - [Virtual
    Chembook](https://web.archive.org/web/20111023232815/http://www.elmhurst.edu/~chm/vchembook/556steroids.html)
  - [stedwards.edu](https://archive.is/20121217023647/http://www.cs.stedwards.edu/chem/Chemistry/CHEM43/CHEM43/Steroids/Steroids.HTML)
  - [How Steroid Hormones
    Work](http://biology.about.com/od/cellularprocesses/a/aa073004a.htm)

[Category:甾族化合物](../Category/甾族化合物.md "wikilink")