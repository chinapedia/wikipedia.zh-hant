**馬來西亞國家石油公司**（）简称**国油**（），成立於1974年8月17日，是[馬來西亞的](../Page/馬來西亞.md "wikilink")[國有](../Page/國有.md "wikilink")[石油及](../Page/石油.md "wikilink")[天然氣公司](../Page/天然氣.md "wikilink")。做為政府的全資控股公司，國油公司掌握了全國的所有石油及天然氣資源，負有開發及使這些資源增值的責任。

## 关于公司

國油自成立後，逐漸成長為綜合性的國際石油天然氣公司，在31個國家有業務往來。国油也是世界500强公司之一，公司财富排名全世界第75名，亚洲12最会赚钱公司排名之一。至2005年3月底，國家石油集團共由103個全資子公司，19個合資公司，及57個相關公司組成。國家石油的業務範圍廣泛，包括上游的石油天然氣探測及生產，下游的[石油提煉](../Page/炼油厂.md "wikilink")、石油製品營銷，交易，天然氣處理及[液化](../Page/液化.md "wikilink")，[輸氣管網運營](../Page/輸氣管.md "wikilink")，[液化天然氣營銷](../Page/液化天然氣.md "wikilink")，[石化製造與營銷](../Page/石化.md "wikilink")，運輸，[汽車](../Page/汽車.md "wikilink")[工程](../Page/工程.md "wikilink")，資產投資等。国油公司也是[马来西亚政府的最主要收入源](../Page/马来西亚.md "wikilink")。

國油公司建造[雙峰塔](../Page/雙峰塔.md "wikilink")（PETRONAS Twin
Towers）做為其總部，該塔一度是[世界最高建築](../Page/世界最高建築.md "wikilink")。该塔也于[马来西亚的第](../Page/马来西亚.md "wikilink")41届国庆日（1998年8月31日）正式启用。

## 参考资料

[Category:国家石油公司](../Category/国家石油公司.md "wikilink")
[Category:馬來西亞公司](../Category/馬來西亞公司.md "wikilink")
[Category:马来西亚国有企业](../Category/马来西亚国有企业.md "wikilink")