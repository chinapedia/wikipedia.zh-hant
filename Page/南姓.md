**南姓**為[漢姓之一](../Page/漢姓.md "wikilink")，屬於稀少姓氏。

## 起源

  - [姒姓](../Page/姒姓.md "wikilink")，[夏禹後代](../Page/夏禹.md "wikilink")，建國「有南氏」，後人南姓。
  - [子姓](../Page/子姓.md "wikilink")，[殷商](../Page/殷商.md "wikilink")[盤庚之子](../Page/盤庚.md "wikilink")**赤龍**的後人[南仲](../Page/南仲.md "wikilink")。
  - [姬姓](../Page/姬姓.md "wikilink")，[衛靈公之子](../Page/衛靈公.md "wikilink")[子南的後代](../Page/公子郢.md "wikilink")，後人改南姓。
  - [晉國隱士逃避徵召](../Page/晉國.md "wikilink")，隱居於[南乡县](../Page/南乡县.md "wikilink")，後人稱南姓。

## 改姓

  - 傳聞[明世宗時](../Page/明世宗.md "wikilink")，張皇后族人獲罪，為防[族滅](../Page/族滅.md "wikilink")，張姓族人改為東、南、西、北四姓的說法。
  - [辛亥革命後](../Page/辛亥革命.md "wikilink")，[滿族](../Page/滿族.md "wikilink")[那拉氏後人大多改為漢姓](../Page/那拉氏.md "wikilink")[那姓或南姓](../Page/那.md "wikilink")。

## 名人

  - [南霽雲](../Page/南霽雲.md "wikilink"):（？－757年），唐代軍官，魏州頓丘人。
  - [南袞](../Page/南袞.md "wikilink"):（1471年－1527年3月10日），韩国人。
  - [南逢吉](../Page/南逢吉.md "wikilink"): 明朝政治人物，陕西渭南人。
  - [南梦班](../Page/南梦班.md "wikilink"):湖广蕲水（湖北浠水）人，清朝政治人物，曾任华亭县（属上海）知县一职。
  - [南桂馨](../Page/南桂馨.md "wikilink"):（1884年3月23日－1966年9月14日），山西省宁武县城内人，卒于北京。
  - [南志信](../Page/南志信.md "wikilink"):（台灣南島民族-
    卑南語：Sising，1886年4月18日－1958年8月13日），臺灣臺東市卑南族人，為首位臺灣原住民西醫。

出生於臺灣清治時期的臺灣府卑南廳卑南社。

  - [南漢宸](../Page/南漢宸.md "wikilink"):（1895年－1967年1月27日），山西趙城（今洪洞县）人，卒于北京。
  - [南懷瑾](../Page/南懷瑾.md "wikilink"):（1918年3月18日－2012年9月29日），生于中国浙江樂清縣，1949年迁至台湾，卒于苏州。
  - [南太鉉](../Page/南太鉉.md "wikilink"):韓國偶像團體，[WINNER成員](../Page/WINNER.md "wikilink")。
  - [南優鉉](../Page/南優鉉.md "wikilink"):韓國偶像團體，[INFINITE成員之一](../Page/INFINITE.md "wikilink")。
  - [南振中](../Page/南振中.md "wikilink"):（1942年－），陕西灵宝人，原新华社社长、总编辑。
  - [南勇](../Page/南勇.md "wikilink"):（1962年6月16日－），吉林延边人。
  - [南策文](../Page/南策文.md "wikilink"):（1962年11月－），中国材料科学专家，清华大学教授，生于湖北浠水。
  - [南立新](../Page/南立新.md "wikilink"): 企業家，北京市人。
  - [南云飞](../Page/南云飞.md "wikilink"):1979年生于内蒙古自治区四子王旗。
  - [南云齐](../Page/南云齐.md "wikilink"):（1993年10月6日－），中國足球員，出生于辽宁大连。
  - [南柱赫](../Page/南柱赫.md "wikilink"):（1994年2月22日－），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")。
  - [南多凜](../Page/南多凜.md "wikilink"):（2002年6月13日 -）韓國男演員。

[ko:남 (성씨)](../Page/ko:남_\(성씨\).md "wikilink")

[N南](../Category/罕見或停用漢字姓氏.md "wikilink")
[南姓](../Category/南姓.md "wikilink")