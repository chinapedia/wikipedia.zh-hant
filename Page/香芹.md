**香芹**（[学名](../Page/学名.md "wikilink")：），是一种[蔬菜](../Page/蔬菜.md "wikilink")，又有**巴西利**、**巴西里**、**洋香菜**、**歐芹**、**洋芫荽**或**番芫荽**、**荷蘭芹**等名稱。[伞形科](../Page/伞形科.md "wikilink")[欧芹属的](../Page/欧芹属.md "wikilink")[二年生](../Page/二年生.md "wikilink")[草本](../Page/草本.md "wikilink")。原产[地中海沿岸](../Page/地中海.md "wikilink")。高15～20cm、具有清爽的香味和鲜綠色。[古罗马时代起用于](../Page/古罗马.md "wikilink")[烹调](../Page/烹调.md "wikilink")，也是世界上用的最广的一种[草药](../Page/草药.md "wikilink")。对地质和气候的适应性高，栽培也容易，故世界各地都有栽培，但对乾燥的适应性差。

## 种类

皱叶（Curly
parsley）和非皱叶的两种。前者是一般的**香芹**或一些烹飪節目所叫的**荷蘭香芹**\[1\]，後者是**意大利香芹**。

## 形态

<File:Petroselinum> crispum 001.JPG|植株 <File:Parsley> Flat.jpg|平葉
图像:Parsley_Curled.jpg|皺葉 <File:Wurzelpetersilie> Wurzel.jpg|根
<File:Petroselinum> crispum.jpeg|花序 <File:Petroselinum> neapolitanum
flower.jpg|花 [File:Parsley3.jpg|未成熟果實](File:Parsley3.jpg%7C未成熟果實)
[File:Parsley100.jpg|皺葉](File:Parsley100.jpg%7C皺葉)

## 用途・效果

<File:Chopping> parsley-01.jpg|碎香芹末 <File:Flickr> - cyclonebill - Ris
med bacon, majs og rød peber.jpg|培根炒飯灑上碎香芹

## 营养

营养价值较高，含有[维生素A](../Page/维生素A.md "wikilink")
（[β-胡萝卜素](../Page/β-胡萝卜素.md "wikilink")）、[维生素B<sub>1</sub>](../Page/维生素B1.md "wikilink")、[维生素B<sub>2</sub>](../Page/维生素B2.md "wikilink")、[维生素C](../Page/维生素C.md "wikilink")
等多种[维生素](../Page/维生素.md "wikilink")、[钙质](../Page/钙.md "wikilink")、[镁质](../Page/镁.md "wikilink")、[铁等](../Page/铁.md "wikilink")[灰分和](../Page/灰分.md "wikilink")[食物纤维](../Page/食物纤维.md "wikilink")、[叶绿素等](../Page/叶绿素.md "wikilink")，这些[营养素含量在蔬菜中属前列](../Page/营养素.md "wikilink")。

## 參見

  - [香菜](../Page/香菜.md "wikilink")

## 参考文献

[Category:可食用伞形科](../Category/可食用伞形科.md "wikilink")
[Category:香草](../Category/香草.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")
[Category:根菜類](../Category/根菜類.md "wikilink")
[Category:单胺氧化酶抑制剂](../Category/单胺氧化酶抑制剂.md "wikilink")

1.