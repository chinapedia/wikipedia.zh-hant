**常州市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[江苏省下辖的](../Page/江苏省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于江苏省南部，[大运河流经此地](../Page/大运河.md "wikilink")。[长三角中心地带](../Page/长三角.md "wikilink")、衔[太湖](../Page/太湖.md "wikilink")，与[上海](../Page/上海.md "wikilink")、[南京](../Page/南京.md "wikilink")、[杭州皆等距相邻](../Page/杭州.md "wikilink")，並与[苏州](../Page/苏州.md "wikilink")、[无锡构成](../Page/无锡.md "wikilink")[苏锡常都市圈](../Page/苏锡常都市圈.md "wikilink")。常州有文字记载的历史长达3200多年。常州有良好的水陆空交通条件，京沪高铁、沪宁城际高铁、京沪铁路、沪宁高速公路、312国道、京杭大运河等穿境而过，並有[常州港及](../Page/常州港.md "wikilink")[常州奔牛机场](../Page/常州奔牛机场.md "wikilink")，是[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")。

## 历史

常州有文字记载历史可以追溯到[春秋时期](../Page/春秋.md "wikilink")，公元前547年，[吴王](../Page/吴.md "wikilink")[寿梦将第四子](../Page/寿梦.md "wikilink")[季札封于](../Page/季札.md "wikilink")**延陵邑**。此后曾有过**毗陵**、**晋陵**、**长春**、**尝州**、**武进**等名称。

[隋文帝](../Page/隋文帝.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年）于[常熟县置](../Page/常熟县.md "wikilink")[常州](../Page/常州_\(江苏\).md "wikilink")，这是“常州”这一名称的由来，即“常熟州”而来，治所因常熟被划归苏州而搬迁到当时的[晋陵县](../Page/晋陵县_\(西晋\).md "wikilink")（现常州市区）。[宋](../Page/宋朝.md "wikilink")、[元](../Page/元朝.md "wikilink")、[明](../Page/明朝.md "wikilink")、[清分别为](../Page/清朝.md "wikilink")[常州](../Page/常州_\(江苏\).md "wikilink")、[常州路](../Page/常州路.md "wikilink")、[常州府的](../Page/常州府.md "wikilink")[治所](../Page/治所.md "wikilink")，所辖范围基本包括现在的[武进](../Page/武进区.md "wikilink")、[江阴](../Page/江阴市.md "wikilink")、[无锡](../Page/无锡市.md "wikilink")、[宜兴](../Page/宜兴市.md "wikilink")。清朝时又将武进县一分为二：[武进县和](../Page/武进县.md "wikilink")[阳湖县](../Page/阳湖县.md "wikilink")。这时[常州府共辖](../Page/常州府.md "wikilink")8县：[武进](../Page/武进县.md "wikilink")、[阳湖](../Page/阳湖县.md "wikilink")、[无锡](../Page/无锡县.md "wikilink")、[金匮](../Page/金匮县.md "wikilink")、[宜兴](../Page/宜兴县.md "wikilink")、[荆溪](../Page/荆溪县.md "wikilink")、[江阴](../Page/江陰縣.md "wikilink")、[靖江](../Page/靖江县.md "wikilink")。1912年废常州府和阳湖县，仅存武进县。1949年析武进县城区置常州市，建国后第一任市长[诸葛慎](../Page/诸葛慎.md "wikilink")。1983年开始辖县。

## 地理

常州位于[长江三角洲地区](../Page/长江三角洲.md "wikilink")，处[太湖平原西北部](../Page/太湖平原.md "wikilink")。北濒[长江](../Page/长江.md "wikilink")，南邻太湖，西南接[安徽省](../Page/安徽省.md "wikilink")、[南京市](../Page/南京市.md "wikilink")，东接[无锡市](../Page/无锡.md "wikilink")，西靠[镇江市](../Page/镇江.md "wikilink")。与[上海](../Page/上海.md "wikilink")、[南京两大都市等距相望](../Page/南京.md "wikilink")，距省会南京市144千米（高速公路）。西南部有宜溧山地，山峰有[茅山](../Page/茅山.md "wikilink")、[锅底山](../Page/锅底山.md "wikilink")、[伍员山等](../Page/伍员山.md "wikilink")。湖泊有[洮湖](../Page/洮湖.md "wikilink")、[滆湖](../Page/滆湖.md "wikilink")。河流有[京杭大运河](../Page/京杭大运河.md "wikilink")、[武宜运河](../Page/武宜运河.md "wikilink")、[太滆运河](../Page/太滆运河.md "wikilink")、[荆溪](../Page/荆溪.md "wikilink")、[南运河等](../Page/南运河.md "wikilink")。水网密布，有水乡之称。5世纪30年代，江南大水，[周忱到江南治水时](../Page/周忱.md "wikilink")，大兴围湖造田，堵江水以防涝。20年造田3.7万亩，使芙蓉湖、阳湖、临津湖逐渐缩小，以致消失。

### 气候

常州属北亚热带[湿润季风气候](../Page/亚热带季风气候.md "wikilink")，季风影响显著，四季分明：冬湿冷，夏闷热。年降水量主要在春夏两季；通常5，6月份梅雨带来的降雨最多。1月最冷，平均气温3.1℃，7月最热，平均气温28.0℃。年均温15.8℃，年均降水量1091毫米。全年日照总时数为1940.2小时。\[1\]

## 政治

### 现任领导

<table>
<caption>常州市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党常州市委员会.md" title="wikilink">中国共产党<br />
常州市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/常州市人民代表大会.md" title="wikilink">常州市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/常州市人民政府.md" title="wikilink">常州市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议常州市委员会.md" title="wikilink">中国人民政治协商会议<br />
常州市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/汪泉.md" title="wikilink">汪泉</a>[2]</p></td>
<td><p><a href="../Page/丁纯_(1970年).md" title="wikilink">丁纯</a>[3]</p></td>
<td><p><a href="../Page/俞志平.md" title="wikilink">俞志平</a>[4]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a><a href="../Page/黄山市.md" title="wikilink">黄山市</a><a href="../Page/黟县.md" title="wikilink">黟县</a></p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/江阴市.md" title="wikilink">江阴市</a></p></td>
<td><p>江苏省常州市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年2月</p></td>
<td><p>2017年2月</p></td>
<td><p>2017年2月</p></td>
<td></td>
</tr>
</tbody>
</table>

### 行政区划

现辖5个[市辖区](../Page/市辖区.md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。2015年4月28日，经[国务院批准](../Page/中华人民共和国国务院.md "wikilink")，原[戚墅堰区并入](../Page/戚墅堰区.md "wikilink")[武进区](../Page/武进区.md "wikilink")；撤销县级金坛市，设立[金坛区](../Page/金坛区.md "wikilink")。另外，原武进区的[奔牛镇划归新北区管辖](../Page/奔牛镇.md "wikilink")；原武进区的[郑陆镇划归天宁区管辖](../Page/郑陆镇.md "wikilink")；原武进区的[邹区镇划归钟楼区管辖](../Page/邹区镇.md "wikilink")\[5\]。

  - 市辖区：[天宁区](../Page/天宁区.md "wikilink")、[钟楼区](../Page/钟楼区.md "wikilink")、[新北區](../Page/新北區_\(常州市\).md "wikilink")、[武进区](../Page/武进区.md "wikilink")、[金坛区](../Page/金坛区.md "wikilink")
  - 县级市：[溧阳市](../Page/溧阳市.md "wikilink")

此外，常州市还设立以下经济管理区：[常州高新技术产业开发区](../Page/常州高新技术产业开发区.md "wikilink")（[国家级](../Page/国家级高新技术产业开发区.md "wikilink")，与新北区[合署办公](../Page/合署办公.md "wikilink")）、[武进高新技术产业开发区](../Page/武进高新技术产业开发区.md "wikilink")（国家级）。

<table>
<thead>
<tr class="header">
<th><p><strong>常州市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[6]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>320400</p></td>
</tr>
<tr class="odd">
<td><p>320402</p></td>
</tr>
<tr class="even">
<td><p>320404</p></td>
</tr>
<tr class="odd">
<td><p>320411</p></td>
</tr>
<tr class="even">
<td><p>320412</p></td>
</tr>
<tr class="odd">
<td><p>320413</p></td>
</tr>
<tr class="even">
<td><p>320481</p></td>
</tr>
<tr class="odd">
<td><p>注：武进区数字包含武进高新技术产业开发区所辖南夏墅街道及武进经济技术开发区所辖西湖街道。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>常州市各区（市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>常州市</p></td>
<td><p>4592431</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>天宁区</p></td>
<td><p>513794</p></td>
<td><p>11.19</p></td>
</tr>
<tr class="even">
<td><p>钟楼区</p></td>
<td><p>505762</p></td>
<td><p>11.01</p></td>
</tr>
<tr class="odd">
<td><p>戚墅堰区</p></td>
<td><p>105184</p></td>
<td><p>2.29</p></td>
</tr>
<tr class="even">
<td><p>新北区</p></td>
<td><p>597144</p></td>
<td><p>13.00</p></td>
</tr>
<tr class="odd">
<td><p>武进区</p></td>
<td><p>1569034</p></td>
<td><p>34.17</p></td>
</tr>
<tr class="even">
<td><p>溧阳市</p></td>
<td><p>749522</p></td>
<td><p>16.32</p></td>
</tr>
<tr class="odd">
<td><p>金坛市</p></td>
<td><p>551991</p></td>
<td><p>12.02</p></td>
</tr>
<tr class="even">
<td><p>注：武进区的常住人口数据中包含武进高新技术产业开发区143489人及武进经济技术开发区38633人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")4591972人\[9\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加746465人，增长19.41%，年平均增长率为1.79%。其中，男性人口为2341102人，占50.98%；女性人口为2250870人，占49.02%。总人口性别比（以女性为100）为104.01。0－14岁人口为528607人，占11.51%；15－64岁人口为3614296人，占78.71%；65岁及以上人口为449069人，占9.78%。

2010年末，常州户籍人口360.80万人，市区户籍人口227.75万人，其中市区非农业户籍人口121.96万人，居江苏第7位。

## 经济

| 区划名称 | [国内生产总值](../Page/国内生产总值.md "wikilink")（亿元） | 占比（%）  |
| ---- | ------------------------------------------ | ------ |
| 常州市  | 3,580.40                                   | 100.00 |
| 常州市区 | 2,715.04                                   | 75.83  |
| 金坛市  | 365.10                                     | 10.20  |
| 溧阳市  | 503.78                                     | 14.07  |

2011年常州[国内生产总值地区构成](../Page/国内生产总值.md "wikilink")

## 交通

[缩略图](https://zh.wikipedia.org/wiki/File:20090919_Changzhou_Imgp5266.jpg "fig:缩略图")，右下角为[江苏省常州高级中学](../Page/江苏省常州高级中学.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:20090919_Changzhou_5459.jpg "fig:缩略图")一帶\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Changzhou_5.JPG "fig:缩略图")

### 铁路

  - [京沪线](../Page/京沪铁路.md "wikilink")，常州为特等站。连接全国各主要城市。
  - [京沪高速铁路](../Page/京沪高速铁路.md "wikilink")[常州北站是沪宁线通过站中最大的车站](../Page/常州北站.md "wikilink")，到达北京南站最快4小时51分，到达上海虹桥站最快40分
  - [沪宁城际铁路](../Page/沪宁城际铁路.md "wikilink")[常州站](../Page/常州站.md "wikilink")，沪宁城际铁路主要车站，到达南京站最快39分，到达上海站最快58分，戚墅堰站，常州市城东的客运站。

### 公路

  - [312国道](../Page/312国道.md "wikilink")、通邻近各市县的公路网干道。
  - [沪宁高速公路经过城北](../Page/沪宁高速公路.md "wikilink")，到[上海和](../Page/上海.md "wikilink")[南京都为约](../Page/南京.md "wikilink")1小时。
  - 沿江高速公路，在常州南连接宁常高速公路。
  - 宁常高速公路，在常州南互通连接沿江高速，是连接南京、上海与沿江江阴、张家港、常熟等县市的主要通道。
  - 西绕城高速，是京杭高速公路的组成部分，连接苏北和浙江。

### 市内交通

  - 市内有总长近八十公里的井字形城市高架快速内环路网，外环绕城高架公路环已于2010年建成通车。
  - 2008年1月1日开通了[快速公交系统](../Page/快速公交系统.md "wikilink")（BRT），是江苏省内首个快速公交系统，现在已建成两条快速公交线，10余条支线，并且开通了环1、环2线，组成了庞大的城市公共运输交通的主干道。全市公交线路大多可以享受空调车1元、刷卡6折的优惠，是继北京以后全国第二个给予市民公共交通优惠的城市。

### 轨道交通

  - 2012年5月11日，国家发展改革委发文批准江苏省常州市城市轨道交通近期建设规划（2011-2018年），常州市成为全国第29个、江苏省第4个获批建设城市轨道交通的城市。根据规划，常州市远期将形成4条线路，共129公里的城市轨道交通线网。近期建设1号线一期和2号线一期工程，长约53.88公里，形成“十”字型的轨道交通基本骨架。

### 航道、港口

  - 市区北临[长江](../Page/长江.md "wikilink")，南濒[太湖](../Page/太湖.md "wikilink")，[京杭大运河穿境而过](../Page/京杭大运河.md "wikilink")。全市水网纵横交织，连江通海。[长江常州港作为国家一类开放口岸](../Page/长江常州港.md "wikilink")，年货物吞吐量超过百万吨。

### 机场

  - [常州奔牛机场是](../Page/常州奔牛机场.md "wikilink")4E级机场，现有通达[北京](../Page/北京.md "wikilink")、[广州](../Page/广州.md "wikilink")、[大连](../Page/大连.md "wikilink")、[厦门](../Page/厦门.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[海口](../Page/海口.md "wikilink")、[西安等国内](../Page/西安.md "wikilink")20多个大中城市的航线，以及通往[澳门](../Page/澳门.md "wikilink")、[芽庄](../Page/芽庄.md "wikilink")、[台北](../Page/台北.md "wikilink")、[台中](../Page/台中.md "wikilink")、[高雄](../Page/高雄.md "wikilink")、[首尔](../Page/首尔.md "wikilink")、[曼谷](../Page/曼谷.md "wikilink")、[万象](../Page/万象.md "wikilink")、[济州岛](../Page/济州岛.md "wikilink")、[襄阳郡等国际](../Page/襄陽郡_\(韓國\).md "wikilink")
    (地区)城市的航线。

## 友好合作

常州市目前共有27個友好城市，分-{布}-情形為[歐洲](../Page/歐洲.md "wikilink")11個，[大洋洲](../Page/大洋洲.md "wikilink")1個，[非洲](../Page/非洲.md "wikilink")2個，[亞洲](../Page/亞洲.md "wikilink")6個和[美洲](../Page/美洲.md "wikilink")7個\[10\]：

  - 歐洲

<!-- end list -->

  - [普拉托省](../Page/普拉托省.md "wikilink") [普拉托](../Page/普拉托.md "wikilink")

  - [布拉邦省](../Page/北布拉班特省.md "wikilink")
    [蒂尔堡](../Page/蒂尔堡.md "wikilink")

  - [西芬兰省](../Page/西芬兰省.md "wikilink")
    [萨塔昆塔地区](../Page/萨塔昆塔区.md "wikilink")

  - [下西里西亚省](../Page/下西里西亚省.md "wikilink")
    [耶莱尼亚古拉](../Page/耶莱尼亚古拉.md "wikilink")

  - [兰讷斯](../Page/兰讷斯.md "wikilink")

  - [托木斯克](../Page/托木斯克.md "wikilink")[托木斯克州](../Page/托木斯克州.md "wikilink")

  - [林堡省](../Page/林堡省.md "wikilink") [洛默尔](../Page/洛默尔.md "wikilink")

  - [斯塔夫罗波尔边疆区](../Page/斯塔夫罗波尔边疆区.md "wikilink")
    [斯塔夫罗波尔](../Page/斯塔夫罗波尔.md "wikilink")

  - [埃森](../Page/埃森.md "wikilink")

  - [北威州](../Page/北威州.md "wikilink")
    [明登](../Page/明登.md "wikilink")\[11\]

  - [西米德兰郡](../Page/西米德兰兹郡.md "wikilink")
    [索利哈尔](../Page/索利哈尔.md "wikilink")\[12\]

<!-- end list -->

  - 大洋洲

<!-- end list -->

  - [新南威尔士州](../Page/新南威尔士州.md "wikilink")
    [赫斯特维尔市](../Page/赫斯特维尔_\(新南威尔士州\).md "wikilink")

<!-- end list -->

  - 非洲

<!-- end list -->

  - [达累斯萨拉姆](../Page/达累斯萨拉姆.md "wikilink")

  - [博巴森-荷津市](../Page/博巴森-荷津市.md "wikilink")\[13\]

<!-- end list -->

  - 亚洲

<!-- end list -->

  - [高规市](../Page/高规市.md "wikilink")

  - [所泽市](../Page/所泽市.md "wikilink")

  - [京畿道](../Page/京畿道.md "wikilink") [南杨州市](../Page/南杨州市.md "wikilink")

  - [埃斯基谢希尔](../Page/埃斯基谢希尔.md "wikilink")
    [埃斯基谢希尔](../Page/埃斯基谢希尔.md "wikilink")

  - [江原道](../Page/江原道.md "wikilink") [春川](../Page/春川.md "wikilink")

  - [柔佛州](../Page/柔佛州.md "wikilink") [新山市](../Page/新山市.md "wikilink")

<!-- end list -->

  - 美洲

<!-- end list -->

  - [伊利诺伊州](../Page/伊利诺伊州.md "wikilink")
    [罗克福德](../Page/罗克福德.md "wikilink")

  - [南里约格朗德州](../Page/南里约格朗德州.md "wikilink")
    [南卡希亚斯](../Page/南卡希亚斯.md "wikilink")

  - [纽约州](../Page/纽约州.md "wikilink") [布法罗](../Page/布法罗.md "wikilink")

  - [恰帕斯州](../Page/恰帕斯州.md "wikilink")
    [塔帕丘拉](../Page/塔帕丘拉_\(恰帕斯州\).md "wikilink")

  - [安大略省](../Page/安大略省.md "wikilink")
    [尼亚加拉瀑布市](../Page/尼亚加拉瀑布城_\(安大略省\).md "wikilink")

  - [科金博区](../Page/科金博.md "wikilink") [拉塞雷纳](../Page/拉塞雷纳.md "wikilink")

  - [巴拉那州](../Page/巴拉那州.md "wikilink")
    [库里蒂巴](../Page/库里蒂巴.md "wikilink")

## 文化教育

### 高中

  - 江苏省[常州高级中学](../Page/常州高级中学.md "wikilink")
  - [常州市第一中学](../Page/常州市第一中学.md "wikilink")
  - [常州市北郊高级中学](../Page/常州市北郊高级中学.md "wikilink")
  - [常州市第二中学](../Page/常州市第二中学.md "wikilink")
  - [常州市第三中学](../Page/常州市第三中学.md "wikilink")
  - 常州市第四中学
  - [常州市第五中学](../Page/常州市第五中学.md "wikilink")
  - [常州市第八中学](../Page/常州市第八中学.md "wikilink")
  - 江苏省[前黄高级中学](../Page/前黄高级中学.md "wikilink")
  - [常州国际学校](../Page/常州国际学校.md "wikilink")
  - [常州市新桥中学](../Page/常州市新桥中学.md "wikilink")
  - [常州市清潭中学](../Page/常州市清潭中学.md "wikilink")
  - [常州市勤业中学](../Page/常州市勤业中学.md "wikilink")(原常州市第九中学)
  - 江苏省[常州市田家炳实验中学](../Page/常州市田家炳实验中学.md "wikilink")（原常州市第六中学）
  - 常州市潞城中学
  - 常州市[戚墅堰实验中学](../Page/戚墅堰实验中学.md "wikilink")（原戚墅堰机车车辆厂职工子弟中学）
  - 江苏省[武进高级中学](../Page/武进高级中学.md "wikilink")（原江苏省湖塘桥中学）
  - 江苏省[奔牛高级中学](../Page/奔牛高级中学.md "wikilink")
  - 江苏省横林高级中学
  - 常州市青龙中学
  - 江苏省[溧阳中学](../Page/溧阳中学.md "wikilink")
  - 江苏省[华罗庚中学](../Page/华罗庚中学.md "wikilink")
  - 常州市[金坛区第一中学](../Page/金坛区第一中学.md "wikilink")
  - 常州市[金坛区第四中学](../Page/金坛区第四中学.md "wikilink")
  - 常州市武进区三河口高级中学
  - 常州市武进区横山桥高级中学
  - 常州市武进区湟里高级中学
  - 常州市新北区西夏墅高级中学

### 高校

  - [常州大学](../Page/常州大学.md "wikilink")（原江苏工业学院）
  - [常州工学院](../Page/常州工学院.md "wikilink")
  - [河海大学常州校区](../Page/河海大学.md "wikilink")
  - [江苏理工学院](../Page/江苏理工学院.md "wikilink")
  - [常州信息职业技术学院](../Page/常州信息职业技术学院.md "wikilink")
  - [常州机电职业技术学院](../Page/常州机电职业技术学院.md "wikilink")
  - [常州纺织职业技术学院](../Page/常州纺织职业技术学院.md "wikilink")
  - [常州工程职业技术学院](../Page/常州工程职业技术学院.md "wikilink")
  - [常州轻工职业技术学院](../Page/常州轻工职业技术学院.md "wikilink")
  - [常州交通技师学院](../Page/常州交通技师学院.md "wikilink")
  - [江苏常州建设高等职业技术学校](../Page/江苏常州建设高等职业技术学校.md "wikilink")

## 名人

春秋时代的[季札是常州的人文始祖](../Page/季札.md "wikilink")，被公认为“延陵第一人”。[南朝](../Page/南朝.md "wikilink")[梁武帝](../Page/梁武帝.md "wikilink")[萧衍生于常州西北](../Page/萧衍.md "wikilink")（今万绥、孟河一带），其子昭明太子[萧统主编有](../Page/萧统.md "wikilink")《昭明文选》。常州文脉至明、清达到鼎盛，两朝进士数量仅次于杭州、苏州，为全国第三，成为江南重要的文化中心之一。因故龚自珍有“天下名士有部落，东南无与常匹俦”之句。[明代有主持编纂](../Page/明代.md "wikilink")《永乐大典》的[陈济](../Page/陈济.md "wikilink")，教育家[谢应芳](../Page/谢应芳.md "wikilink")，文学家、抗倭英雄[唐顺之](../Page/唐顺之.md "wikilink")，东林领袖顾宪成之师、经史学家[薛应旂](../Page/薛应旂.md "wikilink")，“东林八君子”之一的[薛敷教](../Page/薛敷教.md "wikilink")，政治家[孙慎行](../Page/孙慎行.md "wikilink")，“痛哭六军俱缟素，冲冠一怒为红颜”的乱世佳人[陈圆圆](../Page/陈圆圆.md "wikilink")。至[清代](../Page/清代.md "wikilink")，有“阳湖文派”的代表人[恽敬](../Page/恽敬.md "wikilink")，“常州词派”的代表人[张惠言](../Page/张惠言.md "wikilink")，“常州画派”的代表人物[恽寿平](../Page/恽寿平.md "wikilink")，“常州学派”（今文经派，后为康有为、梁启超等人继承，推动戊戌变法）的代表人物[庄存与](../Page/庄存与.md "wikilink")、[刘逢禄](../Page/刘逢禄.md "wikilink")，常州“孟河医派”的代表人物[费伯雄](../Page/费伯雄.md "wikilink")，有“江左才子”美誉的[钱名世](../Page/钱名世.md "wikilink")，留下“江山代有才人出，各领风骚数百年”著名诗句的文史学者[赵翼](../Page/赵翼.md "wikilink")，最早提出“人口论”的著名学者[洪亮吉](../Page/洪亮吉.md "wikilink")，政治家[赵申乔](../Page/赵申乔.md "wikilink")，著名诗人[黄仲则](../Page/黄仲则.md "wikilink")，著名文字训诂学家、经学家[段玉裁](../Page/段玉裁.md "wikilink")，晚清宫廷史官[恽毓鼎](../Page/恽毓鼎.md "wikilink")，讽刺小说家、中国报纸的创始人[李宝嘉](../Page/李宝嘉.md "wikilink")。近现代有洋务派代表人物、[天津大学和](../Page/天津大学.md "wikilink")[交通大学的创始人](../Page/交通大学.md "wikilink")[盛宣怀](../Page/盛宣怀.md "wikilink")，辛亥革命元老之一的[吴稚晖](../Page/吴稚晖.md "wikilink")，被誉为“常州三杰”的中国共产党早期重要领导人[瞿秋白](../Page/瞿秋白.md "wikilink")、[张太雷和](../Page/张太雷.md "wikilink")[恽代英](../Page/恽代英.md "wikilink")，“爱国七君子”中的[李公朴](../Page/李公朴.md "wikilink")、[史良](../Page/史良.md "wikilink")，与钱穆、陈垣、陈寅恪并称“中国四大史学家”的[吕思勉](../Page/吕思勉.md "wikilink")，蒙古史学者[屠寄](../Page/屠寄.md "wikilink")，清史学科奠基人[孟森](../Page/孟森.md "wikilink")，著名语言学家[赵元任](../Page/赵元任.md "wikilink")，爱国实业家[刘国钧](../Page/刘国钧.md "wikilink")，书画家[刘海粟](../Page/刘海粟.md "wikilink")，教育家、华东师范大学首任校长[孟宪承](../Page/孟宪承.md "wikilink")，近代女画家、诗人徐志摩之妻[陆小曼](../Page/陆小曼.md "wikilink")，中国电影的创始人、导演、剧作家[洪深](../Page/洪深.md "wikilink")，“十丈红尘千年青史，一生襟袍万里江山”的神童剧作家[吴祖光](../Page/吴祖光.md "wikilink")，
20世纪30年代著名明星“金嗓子”[周璇](../Page/周璇.md "wikilink")，著名音乐人、歌手[李志等](../Page/李志.md "wikilink")。当代有以著名数学家[华罗庚](../Page/华罗庚.md "wikilink")、医学家[吴阶平为代表的](../Page/吴阶平.md "wikilink")50余名[中国科学院和](../Page/中国科学院.md "wikilink")[中国工程院院士](../Page/中国工程院.md "wikilink")，以及语言学家[周有光等人](../Page/周有光.md "wikilink")。
此外，[北宋时期大诗人](../Page/北宋.md "wikilink")，[唐宋八大家之一的](../Page/唐宋八大家.md "wikilink")[苏东坡是在常州去世的](../Page/苏东坡.md "wikilink")。

## 特产

  - [浇切片](../Page/浇切片.md "wikilink")
  - [芝麻糖](../Page/芝麻糖.md "wikilink")
  - [大麻糕](../Page/大麻糕.md "wikilink")
  - 常州[萝卜干](../Page/萝卜干.md "wikilink")
  - [豆斋饼](../Page/豆斋饼.md "wikilink")
  - [梳篦](../Page/梳篦.md "wikilink")
  - [乱针绣](../Page/乱针绣.md "wikilink")
  - [锡剧](../Page/锡剧.md "wikilink")
  - [留青竹刻](../Page/留青竹刻.md "wikilink")
  - [蟹黄小笼包](../Page/小籠包.md "wikilink")（迎桂）
  - [横山](../Page/橫山橋鎮.md "wikilink")[肉松](../Page/肉松.md "wikilink")
  - 焦溪[葡萄](../Page/葡萄.md "wikilink")
  - 芙蓉[香螺](../Page/香螺.md "wikilink")
  - 湟里牛肉
  - [洛阳](../Page/洛阳镇_\(常州市\).md "wikilink")[水蜜桃](../Page/水蜜桃.md "wikilink")
  - [溧阳](../Page/溧阳.md "wikilink")[别桥咸鹅](../Page/别桥咸鹅.md "wikilink")
  - [溧阳](../Page/溧阳.md "wikilink")[社渚](../Page/社渚.md "wikilink")[西瓜](../Page/西瓜.md "wikilink")
  - [金坛](../Page/金坛.md "wikilink")[雀舌](../Page/雀舌.md "wikilink")
  - [天目湖](../Page/天目湖.md "wikilink")[笋干](../Page/笋干.md "wikilink")
  - [天目湖](../Page/天目湖.md "wikilink")[沙锅鱼头](../Page/沙锅鱼头.md "wikilink")
  - [金坛繁昌](../Page/金坛.md "wikilink")[芋头](../Page/芋头.md "wikilink")
  - 卜弋桥[猪头肉](../Page/猪头肉.md "wikilink")
  - 横山桥[百叶](../Page/百叶.md "wikilink")
  - 寨桥[寨桥老鹅](../Page/寨桥老鹅.md "wikilink")
  - 小河[河豚鱼](../Page/河豚鱼.md "wikilink")、[江鲜](../Page/江鲜.md "wikilink")、[羊肉](../Page/羊肉.md "wikilink")

## 名胜古迹

[Xi_Ying_Gate_in_Changzhou_2012-04.JPG](https://zh.wikipedia.org/wiki/File:Xi_Ying_Gate_in_Changzhou_2012-04.JPG "fig:Xi_Ying_Gate_in_Changzhou_2012-04.JPG")
常州文物古迹众多，有[国家级重点文物保护单位](../Page/江苏全国重点文物保护单位列表.md "wikilink")2处，[省级文物保护单位](../Page/江苏省文物保护单位.md "wikilink")20处，市级文物保护单位94处。其中，有国内外罕见的最为古老的地面城池——[淹城遗址](../Page/淹城遗址.md "wikilink")；号称“东南第一丛林”的[天宁寺](../Page/常州天宁寺.md "wikilink")；因[苏东坡来常州并泊舟而得名的](../Page/苏东坡.md "wikilink")[舣舟亭](../Page/舣舟亭.md "wikilink")；始建于[北宋的](../Page/北宋.md "wikilink")[文笔塔](../Page/文笔塔.md "wikilink")；建于[唐昭宗年间的](../Page/唐昭宗.md "wikilink")[红梅阁](../Page/红梅阁.md "wikilink")；《红楼梦》中[烟花巷的原型](../Page/烟花巷.md "wikilink")[篦箕巷等](../Page/篦箕巷.md "wikilink")。还有历史文化名人故居，[瞿秋白](../Page/瞿秋白.md "wikilink")、[张太雷](../Page/张太雷.md "wikilink")，抗倭名将[唐荆川](../Page/唐荆川.md "wikilink")，著名画家[恽南田](../Page/恽南田.md "wikilink")、汤雨生，著名诗人[黄仲则](../Page/黄仲则.md "wikilink")，著名史学家[赵翼](../Page/赵翼.md "wikilink")、[屠寄](../Page/屠寄.md "wikilink")，政治家、书法家[庄蕴宽](../Page/庄蕴宽.md "wikilink")，著名谴责小说家[李伯元及](../Page/李伯元.md "wikilink")“爱国七君子”中[李公朴](../Page/李公朴.md "wikilink")、[史良故居等](../Page/史良.md "wikilink")30多处。常州园林历史悠久，数量众多，[明](../Page/明.md "wikilink")[清两代盛极一时](../Page/清.md "wikilink")，有40多处，现保存较好的有[近园](../Page/近园.md "wikilink")、[约园](../Page/约园.md "wikilink")、[半园](../Page/半园.md "wikilink")、[亦园](../Page/亦园.md "wikilink")、[寄园](../Page/寄园.md "wikilink")、[止园](../Page/止园.md "wikilink")、[意园](../Page/意园.md "wikilink")、[聊园](../Page/聊园.md "wikilink")、[暂园](../Page/暂园.md "wikilink")、[未园等](../Page/未园.md "wikilink")。还有[香港](../Page/香港.md "wikilink")[财政司长](../Page/财政司长.md "wikilink")[唐英年捐助的](../Page/唐英年.md "wikilink")“[唐荆川纪念馆](../Page/唐荆川纪念馆.md "wikilink")”及[唐氏宗祠等](../Page/唐氏宗祠.md "wikilink")。

此外还有[天目湖](../Page/天目湖.md "wikilink")、[茅山](../Page/茅山.md "wikilink")、[中华恐龙园](../Page/中华恐龙园.md "wikilink")、[横山大林寺](../Page/横山大林寺.md "wikilink")、[白龙庙](../Page/白龙庙.md "wikilink")、[澄西县革命烈士纪念碑等著名景点](../Page/澄西县革命烈士纪念碑.md "wikilink")。

现如今新建的常州眼也成为一道亮丽的风景线。

| <span style="color:#000;">常州的全国重点文物保护单位</span> | **常州的国家级非物质文化遗产**                    |
| ---------------------------------------------- | ------------------------------------ |
| [淹城遗址](../Page/淹城遗址.md "wikilink")             | [瞿秋白故居](../Page/瞿秋白故居.md "wikilink") |
| [张太雷旧居](../Page/张太雷故居.md "wikilink")           | [刘海粟旧居](../Page/刘海粟故居.md "wikilink") |
| [三星村遗址](../Page/三星村遗址.md "wikilink")           |                                      |
|                                                |                                      |

## 宗教

佛教登记的活动场所53处，分别为[天宁寺](../Page/天宁寺_\(常州\).md "wikilink")、[清凉寺](../Page/清凉寺_\(常州\).md "wikilink")、[大林寺](../Page/大林寺.md "wikilink")、[万佛寺](../Page/万佛寺.md "wikilink")、[九龙禅寺](../Page/九龙禅寺.md "wikilink")、[永宁寺](../Page/永宁寺.md "wikilink")、[吴黄禅寺等](../Page/吴黄禅寺.md "wikilink")；基督教登记的活动场所52处，信徒17500人，有[常州市基督教堂等](../Page/常州市基督教堂.md "wikilink")；道教登记的活动场所6处。[茅山是中国](../Page/茅山.md "wikilink")[道教的发源地之一](../Page/道教.md "wikilink")，道教第八小洞天。天主教登记的活动场所5处，信徒2031人；伊斯兰教登记的活动场所1处，信徒1727人。

## 注解

## 参考资料

## 参看

  - [常州话](../Page/常州话.md "wikilink")
  - [常州奔牛机场](../Page/常州奔牛机场.md "wikilink")

## 外部链接

  - [常州市政府网站](http://www.changzhou.gov.cn/)

[Category:常州](../Category/常州.md "wikilink")
[Category:江苏地级市](../Category/江苏地级市.md "wikilink")
[Category:运河城市](../Category/运河城市.md "wikilink")
[Category:长江三角洲城市](../Category/长江三角洲城市.md "wikilink")
[3](../Category/全国文明城市.md "wikilink")
[苏](../Category/国家卫生城市.md "wikilink")
[苏](../Category/国家历史文化名城.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.