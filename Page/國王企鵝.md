{{ external media | width = 300px | image1=[國王企鵝 3D
model](http://fab3d.cc/zukan/40) }}
[Manchot_royal_MHNT.jpg](https://zh.wikipedia.org/wiki/File:Manchot_royal_MHNT.jpg "fig:Manchot_royal_MHNT.jpg")
[Courting_King_Penguins.jpg](https://zh.wikipedia.org/wiki/File:Courting_King_Penguins.jpg "fig:Courting_King_Penguins.jpg")
**国王企鹅**（[學名](../Page/學名.md "wikilink")：**），是企鹅家族中体型第二大的属种\[1\]，成年国王企鹅高约90厘米，重11至15千克，仅小于[皇帝企鹅](../Page/皇帝企鹅.md "wikilink")。全世界，国王企鹅数量约有400万隻，被分为两个亚种（*A.
p. patagonicus*和*A. p. halli*），种群数量仍在继续增加中。

## 分類學

國王企鵝首於1778年由英國自然學家及插圖畫家[約翰·弗雷德里克·米勒進行描述](../Page/約翰·弗雷德里克·米勒.md "wikilink")，其學名首字「*Aptenodytes*」起源於[古希臘語](../Page/古希臘語.md "wikilink")（*a*/α
是「沒有」的意思，*pteno-*/πτηνο- 是「羽毛」或「翅膀」的意思，而*dytes*/δυτης
則為「潛鳥」之意，全字的意思是「沒有羽翼的潛鳥」）\[2\]，而次字「*patagonicus*」則源於[巴塔哥尼亞](../Page/巴塔哥尼亞.md "wikilink")。

國王企鵝和跟牠們相似但體型略比牠們大的[皇帝企鵝](../Page/皇帝企鵝.md "wikilink")（*A.
forsteri*）同為[王企鵝屬最大的兩種企鵝](../Page/王企鵝屬.md "wikilink")。

## 参考文献

## 外部連結

  - [www.pinguins.info：有關所有企鵝的資料](http://www.pinguins.info)
  - [國王企鵝](http://www.penguins.cl/king-penguins.htm) - International
    Penguin Conservation官方網站
  - [國王企鵝](http://animaldiversity.ummz.umich.edu/site/accounts/information/Aptenodytes_patagonicus.html)
    - 生物多樣性（Animal Diversity）網站
  - [福克蘭群島企鵝](http://www.falklandsconservation.com/)
  - [國王企鵝](http://ibc.lynxeds.com/species/king-penguin-aptenodytes-patagonicus)
    - 取於Internet Bird Collection
  - [企鵝世界（Penguin
    World）：國王企鵝](http://www.penguinworld.com/types/king.html)
  - [See also the famous art-project 《背著企鵝去旅行》（*Penguins - Traveling the
    World*）](http://willypuchner.com/en/sdp1_index.htm) -
    圖攝自[威利·卜](../Page/威利·卜.md "wikilink")（Willy Puchner）

[AP](../Category/南極洲.md "wikilink")
[AP](../Category/IUCN無危物種.md "wikilink")
[patagonicus](../Category/王企鵝屬.md "wikilink")
[AP](../Category/南美洲鳥類.md "wikilink")
[AP](../Category/智利鳥類.md "wikilink")
[AP](../Category/阿根廷鳥類.md "wikilink")
[AP](../Category/企鵝目.md "wikilink")
[AP](../Category/企鵝科.md "wikilink")
[Category:南乔治亚和南桑威奇群岛鳥類](../Category/南乔治亚和南桑威奇群岛鳥類.md "wikilink")

1.
2.