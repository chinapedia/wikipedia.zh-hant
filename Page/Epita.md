**ÉPITA** 是 *École pour l'informatique et les techniques
avancées*的缩写，即法国尖端技术学校。是一所培养电脑工程师的私立精英学院，在全法国公立和私立所有计算机信息[高等院校中名列前茅](../Page/高等院校.md "wikilink")，该校是二十年来被法国企业认可的优秀高等教育院校，其毕业生很受企业青睐。校长让∙古赫杜瓦（Joel
Courtois）先生说：“我们学生的长项之一，就是被聘用后能马上投入工作。”

成立于1984年的EPITA (全称：法国高等信息工程师学院) ,
以扎实工程基础与高精专业技能相结合的教育法，致力于培养最优秀的工程师，推动信息通讯技术的未来。
跨国企业对EPITA有着很高评价，许多企业甚至在EPITA学生毕业前就决定聘用他们。至今已有超过6400名毕业于EPITA的工程师,
在全球40多个国家超过2400个企业工作（15%毕业生在海外工作）。

EPITA是法国最大的私营高等教育集团IONIS成员之一，有着约2万名法国在校生及6万名毕业生。IONIS教育集团覆盖12个法国城市的20所学校，涵盖商业，技术和创新教育方案三个主要领域。

地处巴黎南部边缘，紧靠巴黎十三区中国城，毗邻各大高新企业。校区内设有多个信息实验室及研究室，配备高速网络，为学生创业及实验研究提供便利及支持。每年各大企业提供给EPITA学生的实习或工作岗位超过3000个，每个学生在毕业前都会得到10个以上的工作机会，在法国他们的平均年薪超过37000欧。

EPITA电脑工程师属于科技专家和管理人才。毕业于EPITA学生的工程师头衔由工程师职称委员会授予。这一被同行专家确认、由工程师职称委员会授予的职称同时拥有[硕士](../Page/硕士.md "wikilink")（Master）文凭，并在国家职称确认委员会备案。
这一世所公认的第一等级文凭，证明了企业在EPITA的毕业生那里找到了新生力量，因为他们能够给越来越复杂的问题提供最合适的解决办法。该校毕业生在企业取得的成绩证实了EPITA在新技术领域所占的重要地位。

校址坐落于 [Le
Kremlin-Bicetre](http://www1.mappy.com/sidZ3frOUYFAybMn21w/CFGMA?csl=m1&fsl=m1&gsl=m1&msl=m1&ids=&xsl=1&posl=poi&recherche=0&show_poi=0&poi_rr=0.5&poi_rx=0.6&poi_ry=0.5&lr=0.5&flash=1&gb=&out=2&wnm1=&wcm1=&nom1=&tnm1=kremelin+bicetre&pcm1=&tcm1=&a10m1=&ccm1=250&brand=)
-- [巴黎意大利门南面](../Page/巴黎.md "wikilink")，紧靠巴黎十三区中国城。

## 课程内容

此校采用的是一种法国特色教育体制（Grande École）--精英大学。采取2+3的体制。
开始学生上两年预科，两年内其主要课程为理科，包括数学、物理、电子、计算机学。
第三、四年，学生主要学习所有计算机知识。第四年后学生在七个专业中自由地选取一个定向专业：

  - SRS: Systèmes, Réseaux et Sécurité 系统、网络与安全
  - MMA: Multimédia 多媒体与信息科技
  - SCIA: Sciences Cognitives et Informatique Avancée 认知科学与高端信息技术
  - GISTR: Génie Informatique des Systèmes Temps Réel 实时与嵌入式系统
  - SIGL: Systèmes d'Information et Génie Logiciel 信息系统与软件工程
  - TCOM: Télécoms 通讯
  - CSI: Calcul Scientifique et Image 科学计算与图像

## 口碑

EPITA 是法国最有名气的计算机工程重点大学
<https://web.archive.org/web/20061008070137/http://www.epita.fr/IMG/pdf/SOFRES_EPITA_2005.pdf>
毕业后学生就业率为95%以上，就业第一年平均年薪为37000欧元。

## 链接

  - [官网](http://www.epita.fr)
  - [EPITA 发展和专研实验室](http://www.lrde.epita.fr/)

[Category:法國大學](../Category/法國大學.md "wikilink")
[Category:1984年創建的教育機構](../Category/1984年創建的教育機構.md "wikilink")