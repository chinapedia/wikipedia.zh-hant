**公州市**（），舊称**熊津**、**熊川**。位于[韩国](../Page/大韩民国.md "wikilink")[忠清南道中央](../Page/忠清南道.md "wikilink")。是[百济的古都](../Page/百济.md "wikilink")。面積940.18[平方公里](../Page/平方公里.md "wikilink")、人口113,294人。

## 地理

公州市位於首都[首爾以南](../Page/首爾.md "wikilink")150[公里](../Page/公里.md "wikilink")，與[扶餘郡佔有](../Page/扶餘郡.md "wikilink")[錦江中](../Page/錦江.md "wikilink")、下游。由於[黄海水位的差異](../Page/黄海.md "wikilink")，有時可以沿錦江航行至公州。市的南部是[雞龍山國立公園](../Page/雞龍山國立公園.md "wikilink")，西北部與燕岐郡相接。公州市是忠清南道道廳所在地[大田廣域市的衛星城市](../Page/大田廣域市.md "wikilink")。韓國計劃於2030年把行政首都遷移至本市鄰近的[世宗](../Page/世宗_\(都市\).md "wikilink")，但之后该计划搁浅，迁入世宗的只会是文教科学类机构。

## 行政区划

  - [維鳩邑](../Page/維鳩邑.md "wikilink")（유구읍）
  - [利仁面](../Page/利仁面.md "wikilink")（이인면）
  - [灘川面](../Page/灘川面.md "wikilink")（탄천면）
  - [鷄龍面](../Page/鷄龍面.md "wikilink")（계룡면）
  - [反浦面](../Page/反浦面.md "wikilink")（반포면）
  - [長岐面](../Page/長岐面.md "wikilink")（장기면）
  - [儀堂面](../Page/儀堂面.md "wikilink")（의당면）
  - [正安面](../Page/正安面.md "wikilink")（정안면）
  - [牛城面](../Page/牛城面.md "wikilink")（우성면）
  - [寺谷面](../Page/寺谷面.md "wikilink")（사곡면）
  - [新豊面](../Page/新豊面.md "wikilink")（신풍면）
  - [中學洞](../Page/中學洞.md "wikilink")（중학동）
  - [熊津洞](../Page/熊津洞.md "wikilink")（웅진동）
  - [金鶴洞](../Page/金鶴洞.md "wikilink")（금학동）
  - [玉龍洞](../Page/玉龍洞.md "wikilink")（옥룡동）
  - [新官洞](../Page/新官洞.md "wikilink")（신관동）

## 歷史

  - 475年 -
    [百济](../Page/百济.md "wikilink")[文周王由漢城遷都至熊津](../Page/文周王.md "wikilink")(公州)。
  - 538年 -
    [百济聖王遷都](../Page/百济聖王.md "wikilink")[扶餘郡](../Page/扶餘郡.md "wikilink")。
  - 660年
    -[唐朝在百济故地建立设置了](../Page/唐朝.md "wikilink")5个都督府：“熊津、马韩、东明、金涟、德安五都督府”，纳入唐王朝直接管理。但随着后来的[百济复国运动](../Page/百济复国运动.md "wikilink")，唐朝在665年把五个都督府统一合并为[熊津都督府](../Page/熊津都督府.md "wikilink")。[罗唐战争后](../Page/罗唐战争.md "wikilink")，唐朝最终退出朝鲜半岛。
  - 686年 - [新羅获得百济旧地](../Page/新羅.md "wikilink")、熊津州設置都督府。
  - 757年 - 改称为**熊川**。
  - 940年 - [高麗太祖改称公州](../Page/高麗太祖.md "wikilink")、設置都督府。
  - 983年 - 昇格为公州府。
  - 1896年 - 忠清道南北分割, 忠清南道監営（相当於道廳）所在地。
  - 1931年 - 昇格为公州邑（町）。
  - 1932年 - 忠清南道道廳移轉大田邑。
  - 1971年 - 發掘[武寧王陵](../Page/武寧王.md "wikilink")。
  - 1986年 - 昇格为公州邑、市。
  - 1995年 - 公州市、公州郡統合。
  - 2004年 - 韓国通過未來將行政首都移至[世宗](../Page/世宗_\(都市\).md "wikilink")。

## 行政

### 市長

  - [吳英姬](../Page/吳英姬.md "wikilink"), 韓国少有的女性市長(前)
  - 李畯遠(2006.7.1\~2010.6.30,自由先進黨)

## 教育

  - [国立公州大学](../Page/国立公州大学.md "wikilink")
  - [国立公州教育大学](../Page/国立公州教育大学.md "wikilink")
  - [公州映像情報専門大学](../Page/公州映像情報専門大学.md "wikilink")

## 友好都市

  - [韓國](../Page/韓國.md "wikilink")[慶尙北道](../Page/慶尙北道.md "wikilink")[安東市](../Page/安東市.md "wikilink")
  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[遼寧省](../Page/遼寧省.md "wikilink")[瀋陽市](../Page/瀋陽市.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[滋賀縣](../Page/滋賀縣.md "wikilink")[守山市](../Page/守山市.md "wikilink")
  - 日本[熊本縣](../Page/熊本縣.md "wikilink")[和水町](../Page/和水町.md "wikilink")（締結當時[菊水町](../Page/菊水町.md "wikilink")）
  - 日本[山口縣](../Page/山口縣.md "wikilink")[山口市](../Page/山口市.md "wikilink")
  - [美國](../Page/美國.md "wikilink")[阿拉巴馬州](../Page/阿拉巴馬州.md "wikilink")[卡爾霍恩縣](../Page/卡爾霍恩縣_\(阿拉巴馬州\).md "wikilink")

## 旅遊

  - [雞龍山國立公園](../Page/雞龍山國立公園.md "wikilink")
  - 国立公州博物館
  - 宋山里古墳群
  - 武寧王陵
  - 公州山（百济都城）

## 工商

公州市出产的[熊津濾水器](../Page/熊津濾水器.md "wikilink")(Woonjin)占据韓國全国濾水器市場的大部份份额。

## 参考文献

## 外部链接

  - [公州市政府網站](https://web.archive.org/web/20050126163546/http://gongju.go.kr/)

[公州市](../Category/公州市.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")