**富士見野市**（）是[埼玉縣西南部的一個人口約](../Page/埼玉縣.md "wikilink")10万人的[市](../Page/市.md "wikilink")。2005年10月1日，由[上福岡市及](../Page/上福岡市.md "wikilink")[入間郡](../Page/入間郡.md "wikilink")[合併誕生](../Page/市町村合併.md "wikilink")。

## 警察

  -   - 福岡派出所
      - 霞丘派出所
      - 大井派出所
      - 東久保派出所

## 消防

  -   - 西消防站（本部）
      - 東消防站富士見野分局

## 自衛隊

  - [防衛省情報本部大井通信所](../Page/防衛省.md "wikilink")

## 選舉區

### 眾議院議員選舉

  - [埼玉縣第7區](../Page/埼玉縣第7區.md "wikilink")（舊上福岡市）
  - [埼玉縣第8區](../Page/埼玉縣第8區.md "wikilink")（舊大井町）

### 縣議會議員選舉

  - 西第5區

## 交通

### 道路

#### 一般國道

  - [國道254號](../Page/國道254號.md "wikilink")（[川越街道](../Page/川越街道.md "wikilink")、）

#### 都道府縣道

  - [主要地方道](../Page/主要地方道.md "wikilink")
      -
  - 一般縣道
      -
      -
      -
      -
### 鐵路

  - [東武鐵道](../Page/東武鐵道.md "wikilink")

<!-- end list -->

  - [Tobu_Tojo_Line_(TJ)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Tojo_Line_\(TJ\)_symbol.svg "fig:Tobu_Tojo_Line_(TJ)_symbol.svg")
    [東上本線](../Page/東上本線.md "wikilink")：[上福岡站](../Page/上福岡站.md "wikilink")

## 著名出身人士

  - 舊上福岡市
      - （） - 滑稽演員

      - [嶋重宣](../Page/嶋重宣.md "wikilink") - 前職棒選手
  - 舊大井町
      - [三澤興一](../Page/三澤興一.md "wikilink") - 前職棒選手