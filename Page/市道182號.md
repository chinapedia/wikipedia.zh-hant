**市道182號
臺南－七星洋**，是位於[臺灣](../Page/臺灣.md "wikilink")[臺南市](../Page/臺南市.md "wikilink")、[高雄市兩市之間的市道](../Page/高雄市.md "wikilink")。西起[臺南市](../Page/臺南市.md "wikilink")[中西區](../Page/中西區_\(台灣\).md "wikilink")，東至[高雄市](../Page/高雄市.md "wikilink")[內門區七星洋](../Page/內門區.md "wikilink")，全長共計34.864公里\[1\]。市道182號也可以說是[台86線的替代道路](../Page/台86線.md "wikilink")。

[Intersection_of_Dungmen_&_Chongsyue_Road.jpg](https://zh.wikipedia.org/wiki/File:Intersection_of_Dungmen_&_Chongsyue_Road.jpg "fig:Intersection_of_Dungmen_&_Chongsyue_Road.jpg")

[台灣市道182號-起點.JPG](https://zh.wikipedia.org/wiki/File:台灣市道182號-起點.JPG "fig:台灣市道182號-起點.JPG")

## 歷史

原名**縣道182號
臺南－中埔**，前身是[臺灣府城往來關帝廟庄](../Page/臺灣府城.md "wikilink")（今關廟）的古道，沿線及山區作物以此路運往府城販售。2013年起改名為**市道182線
臺南－七星洋**。

2018年10月25日，[中華民國交通部公告原](../Page/中華民國交通部.md "wikilink")**市道182號**起點延伸，自原起點銜接[台17甲線](../Page/台17甲線.md "wikilink")（金華路）處，路線往西經[府前路至銜接](../Page/府前路_\(臺南市\).md "wikilink")[台17線](../Page/台17線.md "wikilink")（[中華西路](../Page/中華西路_\(台南市\).md "wikilink")）止路線調整後，路線名稱不變仍為**臺南－七星洋**，原總里程為34.224公里，調整後總里程為34.864公里（長鍊0.640公里）\[2\]。

## 行經行政區域

  - [臺南市](../Page/臺南市.md "wikilink")

<!-- end list -->

  - [中西區](../Page/中西區_\(台灣\).md "wikilink")：府前[中華西路口](../Page/中華西路_\(臺南市\).md "wikilink")0.0k（**起點**，岔路）⇒府前[金華路口](../Page/金華路.md "wikilink")0.640k（岔路）⇒[府前路](../Page/府前路_\(台南市\).md "wikilink")
    ⇒[東門路](../Page/東門路_\(台南市\).md "wikilink")
  - [東區](../Page/東區_\(臺南市\).md "wikilink")：大東門⇒東門[中華東路口](../Page/中華東路.md "wikilink")4.1k（岔路）⇒清風庄5.784k
  - [仁德區](../Page/仁德區.md "wikilink")：[台南交流道](../Page/台南交流道.md "wikilink")6.94k（）⇒仁德7.157k（岔路）
  - [歸仁區](../Page/歸仁區.md "wikilink")：林仔頂/高鐵橋下9.38k（岔路）⇒歸仁12.34k（左岔路）⇒歸仁交流道12.75k（）
  - [關廟區](../Page/關廟區.md "wikilink")：關廟15.105k（與共線起點）⇒關廟15.197k（與共線終點）
  - [龍崎區](../Page/龍崎區.md "wikilink")：龍崎20.007k（左岔路）⇒龍船23.951k⇒牛埔25.3k（右岔路）⇒州界29.307k（南高市界）

<!-- end list -->

  - [高雄市](../Page/高雄市.md "wikilink")

<!-- end list -->

  - [內門區](../Page/內門區.md "wikilink")：七星洋34.864k（**終點**，岔路）

## 交流道

  - [台南交流道](../Page/台南交流道_\(國道1號\).md "wikilink")

  - [歸仁交流道](../Page/歸仁交流道.md "wikilink")

## 沿線風景與景點

  - [國立臺南生活美學館](../Page/國立臺南生活美學館.md "wikilink")
  - [原臺南地方法院](../Page/原臺南地方法院.md "wikilink")
  - [台南市美術館](../Page/台南市美術館.md "wikilink")2館
  - 文創PLUS-台南創意中心（原愛國婦人會館）
  - [臺南彌陀寺](../Page/臺南彌陀寺.md "wikilink")
  - [臺南龍山寺](../Page/臺南龍山寺.md "wikilink")
  - [台南神學院](../Page/台南神學院.md "wikilink")
  - [臺灣府城大東門](../Page/迎春門_\(臺南市\).md "wikilink")
  - [崁腳福祐清宮](../Page/崁腳福祐清宮.md "wikilink")
  - [歸仁美學館](../Page/歸仁美學館.md "wikilink")
  - [關廟轉運站](../Page/關廟轉運站.md "wikilink")
  - [虎形山公園](../Page/虎形山公園.md "wikilink")
  - [龍崎文衡殿](../Page/龍崎文衡殿.md "wikilink")

## 本路段客運

府城客運：0左.0右（南華街口站），3（東門教會＝富強市場），6（南華街口＝延平郡王祠）

興南客運：紅幹線，紅1\~紅3，紅11\~14，高鐵快捷公車市府線（僅設建興國中孔廟）

高雄客運：8050，高南雙城快線，239（僅設府前路站，站位位在鄰近的南門路）

## 相關

  - [關廟線](../Page/關廟線.md "wikilink") -
    [臺灣糖業原有營業線之一](../Page/臺灣糖業鐵路.md "wikilink")。縣道182號拓寬關廟、歸仁間路段時利用其路基。

## 參考

  - [臺南182縣道](http://gis.rchss.sinica.edu.tw/mapdap/?p=3561) -
    [中央研究院地圖與遙測影像數位典藏計畫](../Page/中央研究院.md "wikilink")

[Category:台灣市道](../Category/台灣市道.md "wikilink")
[Category:台南市道路](../Category/台南市道路.md "wikilink")
[Category:高雄市道路](../Category/高雄市道路.md "wikilink")

1.

2.