**清州國際機場**（，）是一座位於[韓國中部](../Page/韓國.md "wikilink")[忠清北道首府](../Page/忠清北道.md "wikilink")[清州市的國際機場](../Page/清州.md "wikilink")，機場前身為一軍用機場，於1984年決定改建成民用機場，並於1996年12月完工，初名為「襄陽機場」，其後在隔年4月更名為「清州國際機場」。該機場由（，[縮寫KAC](../Page/縮寫.md "wikilink")）管理，主要是負責前往[濟州島的內陸航班及部分來往](../Page/濟州島.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")、[日本與](../Page/日本.md "wikilink")[台灣的航班](../Page/台灣.md "wikilink")，因此現在有不少欲在韓國中部旅遊的旅客因希望免卻在[仁川國際機場繁忙及交通壅塞之苦](../Page/仁川國際機場.md "wikilink")，而轉搭距離[首爾兩小時車程的](../Page/首爾.md "wikilink")[清州旅遊包機](../Page/清州.md "wikilink")。

## 交通

**巴士：**

  - 往[首爾](../Page/首爾.md "wikilink")、[江南](../Page/江南區_\(首爾\).md "wikilink")、[仁川](../Page/仁川廣域市.md "wikilink")、[大田](../Page/大田广域市.md "wikilink")、[天安](../Page/天安市.md "wikilink")、[忠州巴士站](../Page/忠州市.md "wikilink")。

**市公車：**

  - 407(往新攤津)、750(往[五松站](../Page/五松站.md "wikilink"))、751(往世宗市一村)

**急行巴士：**

  - 405(往新攤津、[新攤津站](../Page/新滩津站.md "wikilink"))、747(往清州旅客巴士站、[五松站](../Page/五松站.md "wikilink"))

**鐵道：**

  - [韓國鐵道公社](../Page/韓國鐵道公社.md "wikilink")[忠北線](../Page/忠北線.md "wikilink")[清州機場站](../Page/清州机场站.md "wikilink")，步行600公尺。

## 航空公司與航點

**國內線**

**國際線**  **貨運**

## 參考來源

<div class="references-small">

<references />

</div>

## 外部連結

  - [清州國際機場](http://www.airport.co.kr/cheongju/index.do)

[Category:韓國機場](../Category/韓國機場.md "wikilink")
[Category:韓國軍用機場](../Category/韓國軍用機場.md "wikilink")
[Category:1996年启用的机场](../Category/1996年启用的机场.md "wikilink")