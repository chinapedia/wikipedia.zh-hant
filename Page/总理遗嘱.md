[Will_of_premier_1925.jpg](https://zh.wikipedia.org/wiki/File:Will_of_premier_1925.jpg "fig:Will_of_premier_1925.jpg")
《**总理遗嘱**》是[中國國民黨總理](../Page/中國國民黨總理.md "wikilink")[孙文的](../Page/孙文.md "wikilink")[遺囑](../Page/遺囑.md "wikilink")。1925年，孫文病篤，孫文之子[孫科與](../Page/孫科.md "wikilink")[文膽](../Page/文膽.md "wikilink")[汪兆銘](../Page/汪兆銘.md "wikilink")、[戴傳賢等](../Page/戴傳賢.md "wikilink")[中國國民黨要員恐怕總理未留遺囑即辭世](../Page/中國國民黨.md "wikilink")，遂商議替總理代擬遺囑。遺囑由[汪兆銘於](../Page/汪兆銘.md "wikilink")2月24日寫下。遺囑起草者有諸說，傳有[汪兆銘](../Page/汪兆銘.md "wikilink")、[戴傳賢](../Page/戴傳賢.md "wikilink")、[吳敬恆等](../Page/吳敬恆.md "wikilink")，至今仍眾說紛紜。據說[孫文很滿意遺囑內容](../Page/孫文.md "wikilink")，一字未改。隨後，又由[宋子文向孫請示](../Page/宋子文.md "wikilink")，另外留下一份家事遺囑，把底稿也讀給孫聽，孫又點頭表示贊成；[汪兆銘取筆來](../Page/汪兆銘.md "wikilink")，請孫親自簽名\[1\]。3月11日，孫病勢極其危險，中午孫在遺囑上[簽名](../Page/簽名.md "wikilink")\[2\]，由其妻[宋慶齡帮助](../Page/宋慶齡.md "wikilink")。[宋子文](../Page/宋子文.md "wikilink")、[孔祥熙](../Page/孔祥熙.md "wikilink")、[何香凝](../Page/何香凝.md "wikilink")、[鄒魯等](../Page/鄒魯.md "wikilink")，也在遺囑署字證明\[3\]。翌日逝世\[4\]。

1940年4月1日，[孫文被正式尊為](../Page/孫文.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[國父](../Page/國父.md "wikilink")，[中華民國政府亦稱](../Page/中華民國政府.md "wikilink")《總理遺囑》為《國父遺囑》。\[5\]\[6\]

## 遺囑內容

遺囑分两部分，第一部分总结40年来[國民革命成果](../Page/國民革命.md "wikilink")，并为日后的革命指明方向；第二部分交代身后家事。一般提到《總理遺囑》多指第一部份。

（一）

（二）
[Sun_Yat-sen_will.jpg](https://zh.wikipedia.org/wiki/File:Sun_Yat-sen_will.jpg "fig:Sun_Yat-sen_will.jpg")

### 其他遺囑

[Farewell_letter_to_Soviet_Russia.png](https://zh.wikipedia.org/wiki/File:Farewell_letter_to_Soviet_Russia.png "fig:Farewell_letter_to_Soviet_Russia.png")
2月24日，孙中山在完成前两篇遗嘱后，用英语口述，由[共產國際代表](../Page/共產國際.md "wikilink")[鮑羅廷](../Page/鮑羅廷.md "wikilink")、[陈友仁](../Page/陈友仁.md "wikilink")、[宋子文](../Page/宋子文.md "wikilink")、[孙科记录](../Page/孙科.md "wikilink")，起草了《致苏俄遗书》，孫中山于1925年3月11日簽字\[7\]\[8\]。此份遺囑當時在中國亦僅為少數國人所知\[9\]，並且在蘇聯刊發時出現了幾種不同版本。其[中文翻译如下](../Page/中文.md "wikilink")：

## 遺囑影響

[Sun_Yat-sen-誓遵總理遺囑.jpg](https://zh.wikipedia.org/wiki/File:Sun_Yat-sen-誓遵總理遺囑.jpg "fig:Sun_Yat-sen-誓遵總理遺囑.jpg")24年（1935年），黃埔軍校特別黨部執委合影\]\]
[Chiang_Kai-shek_Sun_Yat-sen總理遺囑.jpg](https://zh.wikipedia.org/wiki/File:Chiang_Kai-shek_Sun_Yat-sen總理遺囑.jpg "fig:Chiang_Kai-shek_Sun_Yat-sen總理遺囑.jpg")24年（1935年），蔣中正在總理悼念儀式上\]\]

## 参考文献

## 外部链接

## 参见

  - [孫中山](../Page/孫中山.md "wikilink")

{{-}}

[Category:1925年中国政治](../Category/1925年中国政治.md "wikilink")
[Category:孫中山著作](../Category/孫中山著作.md "wikilink")
[Category:中国国民党文献](../Category/中国国民党文献.md "wikilink")
[Category:中华民国大陆时期政治文献](../Category/中华民国大陆时期政治文献.md "wikilink")
[Category:中国政治遗嘱](../Category/中国政治遗嘱.md "wikilink")
[Category:1925年作品](../Category/1925年作品.md "wikilink")

1.

2.
3.
4.
5.

6.

7.  Yoshihiro Ishikawa: 〈Sun Yat-sen in Death: Final Testaments and the
    Weekly Commemorative Ceremony〉, 《The TohO GakuhO Journal of Oriental
    Studies (Kyoto)》, 2006, issue 79, pp 1-62, INSTITUTE FOR RESEARCH IN
    HUMANITIES KYOTO UNIVERSITY, ISSN 0304-2448

8.  [古屋奎二编著](../Page/古屋奎二.md "wikilink")：《[蒋总统秘录](../Page/蒋总统秘录.md "wikilink")－中日关系80年之证言》第六册，台北：[中央日报社](../Page/中央日报社.md "wikilink")，1976年，第32页

9.  中国共產党於1925年3月15日向中国国民党发出之吊唁公文中只言及《国事遗嘱》，关于《苏联遗书》及其存在没有言及（《中国共产党致唁中国国民党（3月15日）》，《向导》第107期，1925年3月)，这表明当初《苏联遗书》只为国民党内部极少数人所知