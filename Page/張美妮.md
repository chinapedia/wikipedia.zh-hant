**張美妮**（****，）[香港](../Page/香港.md "wikilink")[無綫電視部頭合約女演員](../Page/無綫電視.md "wikilink")、主持，1999年參加第13期[無綫電視藝員訓練班入行](../Page/無綫電視藝員訓練班.md "wikilink")\[1\]，現為髮型屋「Sigo
Hair
Salon」的老闆娘，該髮型屋是有數間分店的。\[2\]另外，她亦於2013年踏足飲食界，在沙田置富第一城商場開設餐廳「American
Seafood & Grill」。

## 簡介

張美妮小時候曾居於新界北區[粉嶺](../Page/粉嶺.md "wikilink")，自小於沙田長大，家有一位妹妹。父親經營餐廳（至2003年結業）和當餐廳經理。她曾就讀[胡素貞博士紀念學校](../Page/胡素貞博士紀念學校.md "wikilink")，中學畢業於[新法書院](../Page/新法書院.md "wikilink")\[3\]。1999年，參加第13期（1999年）[無綫電視藝員訓練班而入行](../Page/無綫電視藝員訓練班.md "wikilink")，同期同學有應屆視后[楊怡](../Page/楊怡.md "wikilink")、[林峯等](../Page/林峯.md "wikilink")。随後即進入[無綫電視](../Page/無綫電視.md "wikilink")，成為旗下女藝員。加入無綫電視之後，開始主持節目和參演[無線電視劇集](../Page/無線電視劇集.md "wikilink")。偶爾常受邀於節目中擔任嘉賓，其中張美妮更加是[獎門人系列的常客](../Page/獎門人系列.md "wikilink")。畢業後，所飾演的角色都是跑龍套的角色。於2009年始，張美妮飾演的角色漸漸露出頭角，如《[碧血鹽梟](../Page/碧血鹽梟.md "wikilink")》的妓女映月、《[真相](../Page/真相_\(無綫電視劇集\).md "wikilink")》的黃雅樂（細C）、《[衝呀！瘦薪兵團](../Page/衝呀！瘦薪兵團.md "wikilink")》中的閔家麗、《[心路GPS](../Page/心路GPS.md "wikilink")》的胡麗芬以及《[師父·明白了](../Page/師父·明白了.md "wikilink")》的冬梅等。\[4\]\[5\]張美妮曾經簽約成為香港唱片公司
Silly Thing 的歌手，與女歌手[吳日言為公司的同期藝人](../Page/吳日言.md "wikilink")。

2012年，張美妮在節目《[瘋狂歷史補習社](../Page/瘋狂歷史補習社.md "wikilink")》中的搞笑演出獲許多觀眾喜愛，亦令其人氣急升。

2016年，在劇集《[鐵馬戰車](../Page/鐵馬戰車.md "wikilink")》首度挑戰大反派。近年也是[王心慰及](../Page/王心慰.md "wikilink")[陳耀全的常用演員](../Page/陳耀全.md "wikilink")。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                                    |                                                    |                 |
| -------------------------------------------------- | -------------------------------------------------- | --------------- |
| **年份**                                             | **劇名**                                             | **角色**          |
| 1999年                                              | [人龍傳說](../Page/人龍傳說.md "wikilink")                 | 村　女             |
| [非常保鑣](../Page/非常保鑣.md "wikilink")                 | 接待員／酒會賓客／酒保                                        |                 |
| 2005年                                              | [甜孫爺爺](../Page/甜孫爺爺.md "wikilink")                 | Gigi            |
| [奇幻潮](../Page/奇幻潮.md "wikilink")                   | Sue                                                |                 |
| 2006年                                              | [滙通天下](../Page/滙通天下_\(電視劇\).md "wikilink")         | 齊素蘭             |
| 2007年                                              | [歲月風雲](../Page/歲月風雲.md "wikilink")                 | 江　敏             |
| [鐵咀銀牙](../Page/鐵咀銀牙.md "wikilink")                 | 連成碧                                                |                 |
| [律政新人王II](../Page/律政新人王II.md "wikilink")           | 張美倩（Cindy）                                         |                 |
| 2008年                                              | **[金石良緣](../Page/金石良緣.md "wikilink")**             | **張麗娟（Cherry）** |
| 2009年                                              | [碧血鹽梟](../Page/碧血鹽梟.md "wikilink")                 | 映　月             |
| 2010年                                              | [公主嫁到](../Page/公主嫁到.md "wikilink")                 | 方克蘭             |
| [天天天晴](../Page/天天天晴.md "wikilink")                 | 顧小姐                                                |                 |
| 2011年                                              | [居家兵團](../Page/居家兵團.md "wikilink")                 | Ronnie          |
| [Only You 只有您](../Page/Only_You_只有您.md "wikilink") | Candy                                              |                 |
| [誰家灶頭無煙火](../Page/誰家灶頭無煙火.md "wikilink")           | 孫　太                                                |                 |
| [真相](../Page/真相_\(無綫電視劇集\).md "wikilink")          | 黃雅樂（細C）                                            |                 |
| [布衣神相](../Page/布衣神相_\(電視劇\).md "wikilink")         | 盲歌女                                                |                 |
| [荃加福祿壽探案](../Page/荃加福祿壽探案.md "wikilink")           | 街　坊                                                |                 |
| 2012年                                              | [On Call 36小時](../Page/On_Call_36小時.md "wikilink") | 何敏妮（Bonnie）     |
| [當旺爸爸](../Page/當旺爸爸.md "wikilink")                 | 王月華                                                |                 |
| [愛·回家 (第一輯)](../Page/愛·回家_\(第一輯\).md "wikilink")   | 冰　冰                                                |                 |
| [衝呀！瘦薪兵團](../Page/衝呀！瘦薪兵團.md "wikilink")           | 閔家麗（Carrie）                                        |                 |
| [造王者](../Page/造王者_\(電視劇\).md "wikilink")           | 葉若媚                                                |                 |
| [大太監](../Page/大太監.md "wikilink")                   | 瓜爾佳·慶璇                                             |                 |
| 2013年                                              | **[心路GPS](../Page/心路GPS.md "wikilink")**           | **胡麗芬**         |
| [神探高倫布](../Page/神探高倫布.md "wikilink")               | 蔡秀婷                                                |                 |
| **[師父·明白了](../Page/師父·明白了.md "wikilink")**         | **冬　梅**                                            |                 |
| 2014年                                              | [守業者](../Page/守業者.md "wikilink")                   | 方綺紅（青年）         |
| [愛我請留言](../Page/愛我請留言.md "wikilink")               | 姚淑莊（Angel）                                         |                 |
| 2015年                                              | [拆局專家](../Page/拆局專家.md "wikilink")                 | 朱鳳儀（青年）         |
| [陪著你走](../Page/陪著你走_\(電視劇\).md "wikilink")         | 余加勤                                                |                 |
| 2016年                                              | [鐵馬戰車](../Page/鐵馬戰車.md "wikilink")                 | 肖　英             |
| [愛·回家 (第二輯)](../Page/愛·回家_\(第二輯\).md "wikilink")   | 冰　冰                                                |                 |
| [火線下的江湖大佬](../Page/火線下的江湖大佬.md "wikilink")         | Lucy                                               |                 |
| [巨輪II](../Page/巨輪II.md "wikilink")                 | 林淑嫻                                                |                 |
| [為食神探](../Page/為食神探.md "wikilink")                 | 唐紫縈（Keira）                                         |                 |
| [愛·回家之八時入席](../Page/愛·回家之八時入席.md "wikilink")       | 洪玫瑰                                                |                 |
| 2017年                                              | [全職沒女](../Page/全職沒女.md "wikilink")                 | 蘇　花             |
| [愛·回家之開心速遞](../Page/愛·回家之開心速遞.md "wikilink")       | Diana                                              |                 |
| [雜警奇兵](../Page/雜警奇兵.md "wikilink")                 | 萬　珠                                                |                 |
| [誇世代](../Page/誇世代.md "wikilink")                   | 張英鳳                                                |                 |
| 2018年                                              | [波士早晨](../Page/波士早晨.md "wikilink")                 | 何淑欣             |
| [逆緣](../Page/逆緣.md "wikilink")                     | 宋秋波                                                |                 |
| [是咁的，法官閣下](../Page/是咁的，法官閣下.md "wikilink")         | 徐愛玫                                                |                 |
| 未播映                                                | [丫鬟大聯盟](../Page/丫鬟大聯盟.md "wikilink")               | 五　常             |

### 節目主持 / 司儀（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1999年：[閃電傳真機](../Page/閃電傳真機.md "wikilink")
  - 2000-2003年：[至NET小人類](../Page/至NET小人類.md "wikilink")
  - 2001-2002年：[康泰最緊要好玩](../Page/康泰最緊要好玩.md "wikilink")
  - 2003年：[香港非常遊](../Page/香港非常遊.md "wikilink")
  - 2002-2005年：[娛樂大搜查](../Page/娛樂大搜查.md "wikilink")
  - 2006年：[全日偽裝之王](../Page/全日偽裝之王.md "wikilink")
  - 2005-2009年：[東張西望](../Page/東張西望.md "wikilink")（外景部分）
  - 2011年：[午夜出動](../Page/午夜出動.md "wikilink")
  - 2011年：[更上一層樓](../Page/更上一層樓.md "wikilink")
  - 2011年：[歡樂滿東華](../Page/歡樂滿東華.md "wikilink")2011
  - 2013年至2019年：[文化廣場](../Page/文化廣場_\(電視節目\).md "wikilink")
  - 2014年：[活得好Easy](../Page/活得好Easy.md "wikilink")
  - 2015年：[家家有喜事](../Page/家家有喜事.md "wikilink")
  - 2016年：[街坊導賞團](../Page/街坊導賞團.md "wikilink")
  - 2016年：[區區添食平安夜](../Page/區區添食平安夜.md "wikilink")
  - 2017年：慈善星輝仁濟夜
  - 2017年：[區區添食團年飯](../Page/區區添食團年飯.md "wikilink")
  - 2017年：[新春花車巡遊匯演](../Page/澳門農曆新年花車巡遊匯演.md "wikilink")

### 節目嘉賓 / 演出 （[無綫電視](../Page/無綫電視.md "wikilink")）

### 電台節目

  - 2005年：[二話不說愛上你](../Page/二話不說愛上你.md "wikilink")（[新城電台廣播劇](../Page/新城電台.md "wikilink")）
  - 2008年：[AiShiTeRu](../Page/AiShiTeRu.md "wikilink")（[商業電台](../Page/商業電台.md "wikilink")）
  - 2011年 -
    2012年：[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")（[新城電台](../Page/新城電台.md "wikilink")、嘉賓主持）
  - 2013年：咪芝蓮（[商業電台](../Page/商業電台.md "wikilink")）

### 歌曲及音樂錄像

  - 2000年：〈我是最強〉（與譚玉瑛、羅貫峰、黃樂兒、唐韋琪、張潔蓮、劉家聰、伍文生、蓋世寶、李天翔合唱）
  - 2001年：〈點解咁樣架〉（與[蓋世寶合唱](../Page/蓋世寶.md "wikilink")）
  - 2002年：〈森巴嘉年華〉（與[譚玉瑛](../Page/譚玉瑛.md "wikilink")、[羅貫峰](../Page/羅貫峰.md "wikilink")、[唐韋琪](../Page/唐韋琪.md "wikilink")、[蓋世寶](../Page/蓋世寶.md "wikilink")、[伍文生](../Page/伍文生.md "wikilink")、[黃樂兒](../Page/黃樂兒.md "wikilink")、[張國洪](../Page/張國洪.md "wikilink")、[張潔蓮](../Page/張潔蓮.md "wikilink")、[李天翔](../Page/李天翔.md "wikilink")、[黃佩珊](../Page/黃佩珊.md "wikilink")、[劉家聰](../Page/劉家聰.md "wikilink")）
  - 2006年：〈WaLa WaLa
    闖世界〉（與[陳思齊](../Page/陳思齊.md "wikilink")、[黃泆潼](../Page/黃泆潼.md "wikilink")、[陳玟希合唱](../Page/陳玟希.md "wikilink")）
  - 2015年：《[家家有喜事](../Page/家家有喜事.md "wikilink")》主題曲（與[曾華倩](../Page/曾華倩.md "wikilink")、[呂慧儀](../Page/呂慧儀.md "wikilink")、[黃山怡合唱](../Page/黃山怡.md "wikilink")）

### 其他

  - 1999年：[戀愛婚Fun紛](../Page/戀愛婚Fun紛.md "wikilink")（模特兒）
  - [2001年度萬千星輝賀台慶](../Page/2001年度萬千星輝賀台慶.md "wikilink")
  - [2003年度萬千星輝賀台慶](../Page/2003年度萬千星輝賀台慶.md "wikilink")
  - [2004年度萬千星輝賀台慶](../Page/2004年度萬千星輝賀台慶.md "wikilink")
  - [2009無綫節目巡禮](../Page/2009無綫節目巡禮.md "wikilink")
  - 《TVB周刊》精明生活派廣告雜誌
  - 2016年：萬眾同心公益金

### 廣告

## 感情生活

2013年11月21日，張美妮與比她大9歲的髮型屋老闆黃偉強（KK）結婚，結束自2008年開始的愛情長跑，並有多位好友及姊妹出席婚禮，其中包括[楊怡](../Page/楊怡.md "wikilink")、[楊思琦](../Page/楊思琦.md "wikilink")、[李思捷](../Page/李思捷.md "wikilink")、[姚子羚](../Page/姚子羚.md "wikilink")、[黎芷珊等](../Page/黎芷珊.md "wikilink")。\[6\]\[7\]\[8\]\[9\]
2018年12月11日，張美妮於社交媒體宣佈懷孕，預產期為2019年6月。

## 軼聞

張美妮曾被披露與著名藝人[曾志偉出席](../Page/曾志偉.md "wikilink")[澳門某賭場春茗](../Page/澳門.md "wikilink")，她被曾志偉帶去敬酒，全晚以女伴身份陪伴曾志偉左右。\[10\]\[11\]

## 註解

## 參考資料

## 外部連結

  -
  -
  -
  - [張美妮個人網誌](https://web.archive.org/web/20080523203453/http://hk.myblog.yahoo.com/meini8032)

  - [名人戰商界——張美妮從頭做起 5位數開髮型屋
    頭條日報](http://news.hkheadline.com/dailynews/headline_news_detail_columnist.asp?id=155564&section_name=wtt&kw=107)

[mei](../Category/張姓.md "wikilink")
[Category:紫金人](../Category/紫金人.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港客家人](../Category/香港客家人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港飲食界人士](../Category/香港飲食界人士.md "wikilink")
[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:無綫電視藝員訓練班](../Category/無綫電視藝員訓練班.md "wikilink")
[Category:新法書院校友](../Category/新法書院校友.md "wikilink")
[Category:香港基督徒](../Category/香港基督徒.md "wikilink")
[Category:21世紀女演員](../Category/21世紀女演員.md "wikilink")

1.  [頭條日報娛樂-眾星相](http://www.hkheadline.com/ent/ent_star_details.asp?starid=542)

2.

3.  無綫電視翡翠台節目《[香港演義](../Page/香港演義.md "wikilink")》第6集，2011年8月21日

4.  [張美妮
    輸人唔輸陣](http://veryy8.blogspot.com/2013/03/blog-post_8429.html#.UUwqSzdH574)

5.  [張美妮有倚靠
    誓要做好老婆](http://www.kui.name/event/2013-08-02/%E5%BC%B5%E7%BE%8E%E5%A6%AE%E6%9C%89%E5%80%9A%E9%9D%A0+%E8%AA%93%E8%A6%81%E5%81%9A%E5%A5%BD%E8%80%81%E5%A9%86_n770663.htm)

6.  [張美妮結婚11月嫁髮型師
    楊怡做伴娘](http://www.ihktv.com/zhang-minnie-married-in-november.html)

7.
8.  [張美妮出嫁急造人 -
    蘋果日報](http://hk.apple.nextmedia.com/entertainment/art/20131122/18518446)

9.  [張美妮結婚 恩愛吻老公 -
    Youtube](http://www.youtube.com/watch?v=BNmR6QnM3aQ&feature=youtube_gdata)

10. [张美妮被爆成功搭上曾志伟
    手挽手与男方深夜同返酒店](http://www.s1979.com/yule/yulebagua/201303/1980852419.shtml)


11. [张美妮搭上59岁曾志伟 同返酒店被拍](http://e.gmw.cn/2013-03/20/content_7064420.htm)