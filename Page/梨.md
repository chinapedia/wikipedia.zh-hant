**梨**是**梨属**（[学名](../Page/学名.md "wikilink")：**）植物的通称，通常是一种[落叶](../Page/落叶.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，极少数品种为[常绿](../Page/常绿.md "wikilink")，属于[蔷薇目](../Page/蔷薇目.md "wikilink")[蔷薇科](../Page/蔷薇科.md "wikilink")[苹果族](../Page/苹果族.md "wikilink")。[叶片多呈](../Page/叶.md "wikilink")[卵形](../Page/卵形.md "wikilink")，大小因品种不同而各异。[花为白色](../Page/花.md "wikilink")，或略带黄色、粉红色，有五瓣。[果实形状有圆形的](../Page/果实.md "wikilink")，也有基部较细尾部较粗的，即俗称的“梨形”；不同品种的果皮颜色大相径庭，有黄色、绿色、黄中带绿、绿中带黄、黄褐色、绿褐色、红褐色、褐色，个别品种亦有紫红色；野生梨的果径较小，在1到4厘米之间，而人工培植的品种果径可达8厘米，长度可达18厘米。

梨的果实通常用来食用，不仅味美汁多，甜中带酸，而且营养丰富，含有多种[维生素和](../Page/维生素.md "wikilink")[纤维素](../Page/纤维素.md "wikilink")，不同種類的梨味道和質感都完全不同。梨既可生食，也可蒸煮后食用。在医疗功效上，梨可以通便秘，利消化，对[心血管也有好处](../Page/心血管.md "wikilink")。加熱的梨汁含有大量的抗癌物質多酚，給注射過致癌物質的小白鼠喝這樣的梨汁，白鼠尿液中就能排出大量的1-羥基芘毒素，從而有效預防癌症。除了作为[水果食用以外](../Page/水果.md "wikilink")，梨还可以作观赏之用。

## 系统分类

**梨属**植物是一種[水果](../Page/水果.md "wikilink")。

  - 東方梨
      - 沙梨：原產地為中國大陸。
      - 白梨：原產地為中國大陸。
      - 秋子梨：原產地為中國大陸。
      - 日本梨：多種原產地中國大陸的梨種人工品種改良結果。
      - [高接梨](../Page/高接梨.md "wikilink")：地處亞熱帶的台灣，將溫帶梨梨穗接在平地梨樹上、以生產高品質溫帶梨的台灣特殊技術。

<!-- end list -->

  - [西洋梨](../Page/西洋梨.md "wikilink")：除作為水果生食外，亦作烹飪使用。

藥用：（本草綱目）梨果、皮均入藥。實解熱、止咳。性味：果：甘、微酸、涼；梨皮：甘、澀、涼；根：甘、淡、平。效用：果：生津潤燥，清熱化痰。治熱病，津傷煩渴，痰熱，便秘；葉：治小兒疝氣；梨皮：清心潤肺，降火生津。治暑熱煩渴，咳嗽，吐血，疔瘡；根：治疝氣，咳嗽。

## 關於梨的文化

### 典故

  - [孔融讓梨](../Page/孔融讓梨.md "wikilink")
  - [禍棗災梨](../Page/禍棗災梨.md "wikilink")：古人刻書多用棗木、梨木做材料，因此稱濫刻沒有價值的書籍，徒使梨、棗受到災禍為「禍棗災梨」。清．紀昀．閱微草堂筆記．卷六．灤陽消夏錄六：「至於交通聲氣，號召生徒，禍棗災梨，遞相神聖。」亦作「災梨禍棗」。

### 对联

  - 「莲子心中苦，梨儿腹内酸」。（「蓮」諧音「憐」，“梨”諧音“离”）

### 民俗

  - 一些地方在宴席的最后上一道[糖水梨](../Page/糖水梨.md "wikilink")，意思是“吉利（梨）”。
  - 古代某些地方有[禁忌](../Page/禁忌.md "wikilink")，通常不贈送梨子，也不會把梨子切片喫，因為“分梨”與“分離”音同。遇此情況可象徵性收一兩[角錢](../Page/角.md "wikilink")，以示[交易而非贈送](../Page/交易.md "wikilink")。但探病者可送，解曰「離病」。近年工商社會，遵循者已少。

## 常見品種

[PyrusBretschneioderi.jpg](https://zh.wikipedia.org/wiki/File:PyrusBretschneioderi.jpg "fig:PyrusBretschneioderi.jpg")

  - [西洋梨](../Page/西洋梨.md "wikilink")（）：又音譯為啤梨
  - [鴨梨](../Page/鴨梨.md "wikilink")（）\[1\]
  - [沙梨](../Page/沙梨.md "wikilink")（）：又稱日本梨
  - [雪梨](../Page/雪梨_\(水果\).md "wikilink")（）
  - [秋子梨](../Page/秋子梨.md "wikilink")（）：又称花盖梨、山梨、青梨、野梨、沙果梨、酸梨
  - [東方梨新世紀梨](../Page/東方梨.md "wikilink")：是日本繼水晶梨後培養出的新品種，核小皮薄而肉爽汁多，含糖量達13-14%。

## 參考

<references />

## 外部連結

  - [Flora of
    China：梨屬](http://www.efloras.org/browse.aspx?flora_id=2&start_taxon_id=127801)
  - [1](http://plant.tesri.gov.tw/plant/)

[Category:水果](../Category/水果.md "wikilink")
[梨屬](../Category/梨屬.md "wikilink")

1.  [農業試驗所果樹種原標本數位化](http://digifruit.tari.gov.tw/content.php?category=%E6%A2%A8)