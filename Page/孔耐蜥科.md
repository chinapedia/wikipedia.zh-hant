**孔耐蜥科**（Kuehneosauridae）又名**滑翔蜥科**，是群已滅絕[雙孔亞綱](../Page/雙孔亞綱.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，生存於[三疊紀](../Page/三疊紀.md "wikilink")。[孔耐蜥](../Page/孔耐蜥.md "wikilink")、[依卡洛蜥的身體兩側有多條類似](../Page/依卡洛蜥.md "wikilink")[肋骨的結構](../Page/肋骨.md "wikilink")，在生前可能連接者皮膜，使牠們可以短暫地滑翔飛行，但是無法以動力飛行。孔耐蜥科的牙齒呈針狀，因此牠們可能以[昆蟲為食](../Page/昆蟲.md "wikilink")。

*Pamelina*生存於三疊紀早期，是已知最古老的孔耐蜥科\[1\]。[棒臀龍可能也屬於孔耐蜥科](../Page/棒臀龍.md "wikilink")，但是化石已經遺失。而根據現有研究，棒臀龍並沒有孔耐蜥科的全部特徵。

## 參考資料

  - Steins, K., Palmer, C., Gill, P.G., and Benton, M.J. (2008). "The
    aerodynamics of the British Late Triassic Kuehneosauridae."
    *Palaeontology* 51(4): 967-981. DOI:
    10.1111/j.1475-4983.2008.00783.x

[\*](../Category/始蜥目.md "wikilink")

1.  Evans, S.E. 2009. [An early kuehneosaurid reptile (Reptilia:
    Diapsida) from the Early Triassic of
    Poland](http://palaeontologia.pan.pl/PP65/PP65_145-178.pdf).
    Palaeontologia Polonica **65**:145–178.