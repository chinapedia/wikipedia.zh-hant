**苏铁目**（[学名](../Page/学名.md "wikilink")：）統稱**蘇鐵**，是**苏铁纲**（）中一个唯一[目](../Page/目.md "wikilink")，也是蘇鐵門植物唯一現存的群類，共有3[科](../Page/科.md "wikilink")，10至12屬約305[种](../Page/种.md "wikilink")，分布在全球的[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")。本科[植物为常绿](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，典型特徵是[茎粗壮](../Page/茎.md "wikilink")，多數有羽状[叶集生于茎干顶部](../Page/叶.md "wikilink")，[孢子叶球也生于茎顶部](../Page/孢子.md "wikilink")，為雌雄异株植物。

蘇鐵類外觀是一根主幹垂直向上生長，沒有樹支而從主幹的頂部不形成樹冠，僅向外長出軟支長長的葉柄，再向外長對對生的細長[羽毛狀](../Page/羽毛.md "wikilink")[複葉](../Page/複葉.md "wikilink")，其與[被子植物的](../Page/被子植物.md "wikilink")[棕櫚目或](../Page/棕櫚目.md "wikilink")[蕨類植物的](../Page/蕨類植物.md "wikilink")[桫欏目神似](../Page/桫欏目.md "wikilink")，三者常被人認錯的超遠緣植物，但按[趨同演化出適合炎熱多雨的地區](../Page/趨同演化.md "wikilink")。

由于多数生长在人迹罕见的热带森林中，最近25年才逐渐被人们发现越来越多的品种，可能尚有许多品种没有被人们发现。蘇鐵被發現橫跨許多世界各地的亞熱帶和熱帶地區。蘇鐵在[南美洲和](../Page/南美洲.md "wikilink")[中美洲有最多樣性](../Page/中美洲.md "wikilink")，其餘分佈在[墨西哥](../Page/墨西哥.md "wikilink")，[安的列斯群島](../Page/安的列斯群島.md "wikilink")，[美國東南部](../Page/美國.md "wikilink")，[澳大利亞](../Page/澳大利亞.md "wikilink")，[美拉尼西亞](../Page/美拉尼西亞.md "wikilink")，[密克羅尼西亞聯邦](../Page/密克羅尼西亞聯邦.md "wikilink")，[日本](../Page/日本.md "wikilink")，[中國](../Page/中國.md "wikilink")，[東南亞](../Page/東南亞.md "wikilink")，[印度](../Page/印度.md "wikilink")，[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")，[馬達加斯加等地則至少有](../Page/馬達加斯加.md "wikilink")65
種。有些蘇鐵可以生存在半沙漠、氣候惡劣、乾旱、潮濕的熱帶雨林環境下。

苏铁纲植物传统分类被分到[裸子植物门](../Page/裸子植物门.md "wikilink")，但后来发现裸子植物门是个[并系群](../Page/并系群.md "wikilink")（[被子植物也是起源于裸子植物祖先](../Page/被子植物.md "wikilink")），且各种裸子植物的性质有很大的差别，所以最新的分类方法将其拆分，单独列为一个**苏铁门**（）。

## 起源

蘇鐵化石記錄可以追溯到[古生代末期的早](../Page/古生代.md "wikilink")[二疊紀](../Page/二疊紀.md "wikilink")，約2.8億年前。在[侏罗纪时发展最为茂盛](../Page/侏罗纪.md "wikilink")，以后逐渐衰退，现在仅存300余[种](../Page/种.md "wikilink")。雖然蘇鐵血統本身是古老的，但大多數現存物種都是於過去1200萬年內演化而成。

## 分佈

現存的蘇鐵目物種主要分佈於亞洲、美洲、澳洲及非洲的[熱帶及](../Page/熱帶.md "wikilink")[亞熱帶地區](../Page/亞熱帶.md "wikilink")，在溫帶地區只可在溫室內種植。

## 参考文献

## 外部链接

  - [APG网站中的鞘柄木科](http://www.mobot.org/MOBOT/Research/APWeb/orders/cycadales.html#Cycadales)
  - [NCBI中的鞘柄木科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=3297)

[\*](../Category/苏铁目.md "wikilink")
[Category:种子植物](../Category/种子植物.md "wikilink")
[Category:活化石](../Category/活化石.md "wikilink")
[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")