[Early-Historical-Israel-Dan-Beersheba-Judea-Chinese.png](https://zh.wikipedia.org/wiki/File:Early-Historical-Israel-Dan-Beersheba-Judea-Chinese.png "fig:Early-Historical-Israel-Dan-Beersheba-Judea-Chinese.png")
[西元前830年前後的迦南南部地圖。
](https://zh.wikipedia.org/wiki/File:Levant_830.svg "fig:西元前830年前後的迦南南部地圖。             ")
**非利士人**（Philistines，[希伯來語פלשתים](../Page/希伯來語.md "wikilink"),
*plishtim*），天主教譯名為**培肋舍特人**，又譯**菲力斯丁**、**菲力斯丁人**，是居住在[迦南南部海岸的古](../Page/迦南.md "wikilink")[民族](../Page/民族.md "wikilink")，其領土位於今日[加薩走廊及以北一帶](../Page/加薩走廊.md "wikilink")，並在後來的文獻中被稱為「非利士地」。非利士人的起源，現代[考古學認為與](../Page/考古學.md "wikilink")[希臘之南](../Page/希臘.md "wikilink")[愛琴海的](../Page/愛琴海.md "wikilink")[邁錫尼文化的早期有文化聯結](../Page/邁錫尼文明.md "wikilink")。在非利士人留下任何文字資料之前，他們已採用[迦南當地的文化和語言](../Page/迦南.md "wikilink")。非利士語的辭匯中，有許多證據證明他們起源於[印歐語系](../Page/印歐語系.md "wikilink")。

## 語源學

英語的非利士人一詞來源於古法語「Philistin」，法語又來源於拉丁語「Philistinus」，拉丁語又來源於希臘語「Philistinoi」，希臘語又來源於希伯來語「P'lishtim」（參見撒母耳記上17章26、36節，撒母耳記下1:20節；士師記14章3節）。「people
of
P'lesheth」（「Philistia」）；比較：阿卡德語：「Palastu」，埃及語「Palusata」；這個詞可能是這個民族的自稱\[1\]。

聖經學者通常將這個詞追溯到其[閃族根源](../Page/閃族.md "wikilink")「p-l-sh」（），意為「分開、經歷、蜂擁而來，覆蓋或侵略」，\[2\]這個名稱可能意為「移民」或「侵略者」\[3\]

鐘斯認為非利士人這個名稱是希臘語「phyle
histia」的變體（「爐邊的部落」，愛奧尼亞拼法為「hestia」）\[4\]。他進而認為可能是他們將爐子引進[黎凡特地區](../Page/黎凡特.md "wikilink")。這是由於他在非利士遺址找到了使用爐子的考古學證據以前。

## 歷史

如果非利士人被確定為「[海上民族](../Page/海上民族.md "wikilink")」之一（見下述「起源」部份），那佔領[迦南發生在](../Page/迦南.md "wikilink")[埃及第二十王朝的](../Page/埃及第二十王朝.md "wikilink")[拉美西斯三世統治期間](../Page/拉美西斯三世.md "wikilink")（前1180年代—前1150年代），可能是他們的海洋知識，使得他們對於[腓尼基人非常重要](../Page/腓尼基.md "wikilink")。

在埃及，有一個稱為「Peleset」（或「prst」）的民族，通常認為他們就是非利士人，出現在[拉美西斯三世的題字Medinet](../Page/拉美西斯三世.md "wikilink")
Habu中\[5\]，其中記述他對海洋民族的勝利，以及
[拉美西斯四世統治時所寫的](../Page/拉美西斯四世.md "wikilink")[拉美西斯三世統治的概述](../Page/拉美西斯三世.md "wikilink")。19世紀的聖經學者將非利士人之地（「Philistia」）等同於[亞述碑刻中的](../Page/亞述.md "wikilink")「Palastu」和「Pilista」\[6\]。

非利士人征服了沿著迦南西南部沿海帶狀地區的5座城市：[迦萨](../Page/迦萨.md "wikilink")、[亞實基倫](../Page/亞實基倫.md "wikilink")、[亞實突](../Page/亞實突.md "wikilink")、[以革倫和](../Page/以革倫.md "wikilink")[迦特](../Page/迦特.md "wikilink")，那裡直到[埃及第十九王朝結束](../Page/埃及第十九王朝.md "wikilink")（結束於西元前1185年）都屬於[古埃及](../Page/古埃及.md "wikilink")。[聖經中](../Page/聖經.md "wikilink")[參孫](../Page/參孫.md "wikilink")、[撒母耳](../Page/撒母耳.md "wikilink")、[掃羅和](../Page/掃羅.md "wikilink")[大衛的故事都涉及了大量的非利士人與](../Page/大衛.md "wikilink")[以色列人衝突](../Page/以色列人.md "wikilink")。非利士人長期壟斷了鍛造[鐵的技術](../Page/鐵.md "wikilink")（他們可能在征服[安納托利亞期間獲得](../Page/安納托利亞.md "wikilink")），聖經中描寫的[歌利亞的裝甲就是利用了這些鍛造鐵的技術](../Page/歌利亞.md "wikilink")。

這些部落組成強有力的同盟，抵抗[希伯來人的頻繁侵入](../Page/希伯來人.md "wikilink")。這兩個民族之間進行著幾乎始終處於[戰爭狀態](../Page/戰爭.md "wikilink")。非利士人的城邑由君長們（סְרָנִים、seranim，"lords"）所統治，他們為了共同利益而一起行動，惟未留下明白的文獻資料，不清楚他們對於「國家」的定義到何種程度。他們在被希伯來人的國王[大衛王擊敗之後](../Page/大衛王.md "wikilink")，「國王」這職稱取代了「seranim」管理各城。其中有一些國王稱為[亞比米勒](../Page/亞比米勒.md "wikilink"),
早先這只是個人的名字，後來成為朝代名。

西元前732年，非利士人被[亞述的](../Page/亞述.md "wikilink")[提格拉特帕拉沙尔三世征服](../Page/提格拉特帕拉沙尔三世.md "wikilink")，喪失[獨立](../Page/獨立.md "wikilink")，此後的歷次反叛均被鎮壓。最終，[迦勒底人的](../Page/迦勒底人.md "wikilink")[迦勒底王国國王](../Page/迦勒底王国.md "wikilink")[尼布甲尼撒二世征服了](../Page/尼布甲尼撒二世.md "wikilink")[猶大王國和整個](../Page/猶大王國.md "wikilink")[敘利亞](../Page/敘利亞.md "wikilink")，前非利士人城市成為[新巴比倫王國的疆土](../Page/迦勒底王国.md "wikilink")。在這時期以後，非利士人就很少出現。不過，以西結書25章16節、撒迦利亞書9章6節和I
Macabees
3還提到了非利士人，表明他們在巴比倫-{征}-服之後仍然作為一個民族存在。最後，非利士人作為一個民族的所有痕跡都消失了。後來這些城市先後處於[波斯帝國](../Page/阿契美尼德王朝.md "wikilink")、[猶太人](../Page/猶太人.md "wikilink")（Hasmonean王國）、[马其顿帝国与](../Page/马其顿帝国.md "wikilink")[塞琉古帝国](../Page/塞琉古帝国.md "wikilink")）、[羅馬帝国等帝國的控制之下](../Page/羅馬帝国.md "wikilink")。

## 非利士人的起源

大多數學者都同意，非利士人並非巴勒斯坦地區的[土著](../Page/土著.md "wikilink")。聖經中大約有250處涉及到非利士人或非利士地，並再三說到他們是「未受割禮的」，與[閃族不同](../Page/閃族.md "wikilink")，例如[以色列人在出埃及之後遭遇的迦南人](../Page/以色列人.md "wikilink")（[撒母耳記上](../Page/撒母耳記.md "wikilink")17章26、36節，[撒母耳記下](../Page/撒母耳記.md "wikilink")1章20節，[士師記](../Page/士師記.md "wikilink")14:3）。

有人認為非利士人是偉大的海軍同盟「海上民族」的一部份，在西元前12世紀初，他們從在[克里特島和](../Page/克里特島.md "wikilink")[愛琴海島嶼的家鄉來到](../Page/愛琴海.md "wikilink")[地中海海岸](../Page/地中海.md "wikilink")，在[埃及第十九王朝後期一再攻擊](../Page/埃及第十九王朝.md "wikilink")[埃及](../Page/埃及.md "wikilink")。儘管[拉美西斯三世最終擊退了他們](../Page/拉美西斯三世.md "wikilink")，但是他又重新安置了他們，重建起迦南的沿海城鎮。

Papyrus Harris
I詳細敍述了拉美西斯三世統治時期的成就。他簡短地敍述了8年戰役的成果，戰敗了海上民族。拉美西斯將俘虜的海上民族帶回埃及，他「將他們安置在以他的名字命名的要塞。
我對他們所有人徵稅，每年從倉庫和穀倉收取衣服和糧食。」一些學者認為很可能這些要塞就是迦南南部的防禦城鎮，並且最終成為非利士人的5座城市（Pentapolis）（Redford
1992, p. 289）。Israel
Finkelstein認為非利士人城市遭到洗劫25-50年後，又被非利士人收復。很可能在這期間的初期，非利士人居住在埃及，直到[拉美西斯三世統治的衰落的後期](../Page/拉美西斯三世.md "wikilink")，他們才被允許回到非利士地。

### 考古學

通過在[亞實突](../Page/亞實突.md "wikilink")、[亞實基倫](../Page/亞實基倫.md "wikilink")、[以革倫和Tell](../Page/以革倫.md "wikilink")
es-Safi（可能就是[迦特](../Page/迦特.md "wikilink")）的考古發掘，弄清了[邁錫尼文明與非利士文明之間的聯繫](../Page/邁錫尼文明.md "wikilink")，5座非利士人城市中的4座位於迦南。第5座城市是[加沙](../Page/加沙.md "wikilink")。特別顯著的是，早期的非利士人陶器，就是[愛琴海邁錫尼青銅文化陶器的當地版本](../Page/愛琴文明.md "wikilink")，用褐色或黑色進行裝飾。後來在[鐵器時代發展為獨特的非利士](../Page/鐵器時代.md "wikilink")[陶器](../Page/陶器.md "wikilink")，用黑色或紅色裝飾
。特別讓人感興趣的是一個結構精良的大型建築，面積有240平方米，在以革倫發現的。牆很厚，足以支撐第二層。由精緻的入口進入一個大廳，一部份被一排圓柱支撐的屋頂所覆蓋。在大廳的地板上有鋪著鵝卵石的圓形爐子，如同典型的邁錫尼中央大廳
。有一個發現是3個小青銅輪子和8個輪輻，這樣的輪子在這一時期的愛琴海地區用作攜帶型的祭台，因此可以假定該建築是用於宗教儀式。進一步的證據是關於以革倫的碑刻PYGN或PYTN，有人認為是指古代[邁錫尼一位](../Page/邁錫尼文明.md "wikilink")[女神的稱號](../Page/女神.md "wikilink")「Potnia」。在[亞實基倫](../Page/亞實基倫.md "wikilink")、[以革倫和](../Page/以革倫.md "wikilink")[迦特的發掘找到了](../Page/迦特.md "wikilink")[狗和](../Page/狗.md "wikilink")[豬的骨骼](../Page/豬.md "wikilink")，顯示它們是被屠宰的，意味著這些動物是居民的食物構成的一部份。

### 皮發斯基族

### 非利士語

有一些證據支持非利士人起源於印歐語系的民族的假設。聖經中許多與非利士人有關的詞語都不是閃族的。

### 聖經記載

希伯來的傳統記載在[創世記](../Page/創世記.md "wikilink")10章14節，那裡說到，「非利士人」（**פְּלִשְׁתִּים**，/pəlištim/）出自於「迦斯路希人」（פַּתְרֻסִים），而「迦斯路希人」又源於[含的兒子](../Page/含.md "wikilink")[麥西](../Page/麥西.md "wikilink")（מִצְרַיִם，埃及）。當[以色列人定居在猶大高地時](../Page/以色列人.md "wikilink")，非利士人居住在[地中海東部海岸的](../Page/地中海.md "wikilink")「Pelesheth」（希伯來語：**פְּלֶשֶׁת**，/pəléšet/
or /pəlášet）。

[大袞是非利士人的眾神之首](../Page/大袞.md "wikilink")。

在《[阿摩司書](../Page/阿摩司書.md "wikilink")》中，說到非利士人起源於[迦斐讬](../Page/迦斐讬.md "wikilink")：「耶和華說，以色列人哪，我豈不看你們如古實人麼？我豈不是領以色列人從埃及地上來，領非利士人從迦斐讬出來，領亞蘭人從吉珥出來麼？」\[7\]。後來，在前7世紀，《[耶利米書](../Page/耶利米書.md "wikilink")》也說到他們與迦斐讬的聯繫：「原來耶和華必毀滅非利士人，就是迦斐讬海島餘剩的人。」\[8\]。學者們認為迦斐讬是[賽普勒斯](../Page/賽普勒斯.md "wikilink")、[克里特島或地中海東部的其他地點](../Page/克里特島.md "wikilink")。

### 赫梯起源说

在土耳其Tell
Tayinat遗址和叙利亚[阿勒颇](../Page/阿勒颇.md "wikilink")，考古学家发现了一个被称为[帕里斯丁的新赫梯王国](../Page/帕里斯丁.md "wikilink")，存在于前12世纪至前10世纪的土耳其、叙利亚交界地区，所在地受到[爱琴海文明的影响](../Page/爱琴海文明.md "wikilink")。其名称Palistin与非利士的埃及语、希伯来语名称相近，因此有人提出“非利士”的名字可能来源于一批从爱琴海地区迁移至此的居民\[9\]\[10\]。

## 其他用法

  - 19世紀和20世紀初的英國作家有時將巴勒斯坦阿拉伯人稱為「非利士人」（Philistines）。這顯然並非由於相信他們與古代非利士人的聯繫，而只是反映過去認為「非利士人」就是「巴勒斯坦土著」的傳統觀點。巴勒斯坦在阿拉伯語中稱為「فلسطين」，讀作「Falasṭīn」，源於拉丁語「Palaestina」。
  - 在英语中有[philistine一词指俗气的人即源于非利士人](../Page/庸俗主義.md "wikilink")。

## 參條條目

  - [摩押人](../Page/摩押人.md "wikilink")
  - [亚扪人](../Page/亚扪人.md "wikilink")
  - [以東人](../Page/以東人.md "wikilink")
  - [阿拉米人](../Page/阿拉米人.md "wikilink")（即亞蘭人）
  - [士師記](../Page/士師記.md "wikilink")
  - [非利士文化博物馆](../Page/非利士文化博物馆.md "wikilink")

## 參考文獻

<references />

## 參考書目

  - Trude Krakauer Dothan. 1982. *The Philistines and Their Material
    Culture*. Jerusalem: [Israel Exploration
    Society](../Page/Israel_Exploration_Society.md "wikilink")
  - Dothan, Trude Krakauer, Moshe Dothan. 1992. *People of the Sea: The
    Search for the Philistines*. New York: Macmillan Publishing Company
  - Carl Ehrlich S. 1996. *The Philistines in Transition: A History from
    ca. 1000–730 B.C.E.* Studies in the History and Culture of the
    Ancient Near East 10, ser. eds. [Baruch
    Halpern](../Page/Baruch_Halpern.md "wikilink"), and [Manfred Hermann
    Emil Weippert](../Page/Manfred_Hermann_Emil_Weippert.md "wikilink").
    Leiden: E. J. Brill
  - Seymour Gitin, Amihai Mazar, Ephraim Stern, eds. 1998.
    *Mediterranean Peoples in Transition: Thirteenth to Early Tenth
    Centuries BCE*. Jerusalem: Israel Exploration Society
  - Aren M. Maeir 2005. Philister-Keramik. Pp. 528–36 in "Reallexikon
    der Assyriologie und vorderasiatischen Archäologie", Band 14.
    Berlin: [W. de Gruyter](../Page/W._de_Gruyter.md "wikilink").
  - Eliezer Oren D., ed. 2000. *The Sea Peoples and Their World: A
    Reassessment*. University Museum Monograph 108. Philadelphia: The
    University of Pennsylvania Museum of Archaeology and Anthropology
  - Donald Bruce Redford. 1992. *Egypt, Canaan, and Israel in Ancient
    Times*. Princeton: Princeton University Press
  - Claude Vandersleyen, "Keftiu: a cautionary note," *Oxford Journal of
    Archaeology* 22/21, 2003, 209-212.
  - George E. Mendenhall *The Tenth Generation: The Origins of the
    Biblical Tradition*, The Johns Hopkins University Press, 1973. ISBN.

## 外部連結

  - [非利士文化博物馆](https://web.archive.org/web/20141223172808/http://www.phcm.co.il/en)
  - [聖經中提及非利士人或非利士地之處列表](https://web.archive.org/web/20060516192340/http://www.dabar.org/Dyke/PHILISTINES/250PhilistineTexts.html)
  - [Tell es-Safi/迦特考古專案網站](http://www.dig-gath.org)
  - [海上民族和非利士人](https://web.archive.org/web/20040814180456/http://www.courses.psu.edu/cams/cams400w_aek11/www/index.htm)
  - [Neal
    Bierling,「給歌利亞應得的：關於非利士人新的考古學亮光」，1992](http://www.phoenixdatasystems.com/goliath/contents.htm)
  - [國王的曆法：庫姆蘭的秘密](http://www.kingscalendar.com/cgi-bin/index.cgi?action=viewnews&id=164)（以色列士師時代年表，前1412年到1039年）
  - [非利士人：約櫃的襲擊者還是文化的歌利亞？](http://www.people.cornell.edu/pages/bel9/index.html)

[Category:以色列历史](../Category/以色列历史.md "wikilink")
[Category:亚洲民族](../Category/亚洲民族.md "wikilink")
[Category:古代族群](../Category/古代族群.md "wikilink")
[Category:聖經](../Category/聖經.md "wikilink")

1.  [Etymology
    Online](http://www.etymonline.com/index.php?search=Philistine&searchmode=none)
2.  Jastrow, Marcus. A Dictionary of the Targumim, the Talmud Babli and
    Yerushalmi, and the Midrashic Literature. New York: Judaica Press,
    1989., p.1185
3.
4.  Jones, A. 1972. The Philistines and the Hearth: Their Journey to the
    Levant. Journal of Near Eastern Studies 31: 343–50}}
5.  [Texts from the Medinet Habu Temple with Reference to the Sea
    Peoples](http://www.courses.psu.edu/cams/cams400w_aek11/mhabtext.html)

6.  Easton's Bible Dictionary，1897
7.  阿摩司書9章7節
8.  耶利米書47章4節
9.  *Before and After the Storm: Crisis Years in Anatolia and Syria
    between the Fall of the Hittite Empire and the Beginning of a New
    Era (ca. 1220-1000 BC)*, A Symposium in Memory of Itamar Singer,
    University of Pavia, 2012
    [p. 7+8](http://www.academia.edu/5403302/The_Philistines_in_the_North_and_the_Kingdom_of_Taita_unpublished_paper_by_Itamar_Singer_zl_)
10. Emanuel, Jeffrey P. "King Taita and His Palistin: Philstine State or
    Neo-Hittite Kingdom?" *Antiguo Oriente* 13 (2015), 11-40
    [1](https://www.academia.edu/26654209/King_Taita_and_His_Palistin_Philistine_State_or_Neo-Hittite_Kingdom)