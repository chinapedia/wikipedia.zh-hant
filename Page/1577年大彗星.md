[Brahe2.gif](https://zh.wikipedia.org/wiki/File:Brahe2.gif "fig:Brahe2.gif")
**1577年大彗星**的[臨時名稱是](../Page/天文學臨時編號#彗星.md "wikilink")**C/1577
V1**，是在西元1577年非常靠近[地球的一顆彗星](../Page/地球.md "wikilink")。全[歐洲的人都看見了這顆](../Page/欧洲.md "wikilink")[彗星](../Page/彗星.md "wikilink")，著名的[丹麥天文學家](../Page/丹麦.md "wikilink")[第谷·布拉赫也不例外](../Page/第谷·布拉赫.md "wikilink")。\[1\]
經由對這顆彗星的觀測，第谷發現彗星和其他在天空移動的類似天體，都在地球的[大氣層之外](../Page/大气层.md "wikilink")。\[2\]

## 第谷的觀測

[Brahe_notebook.jpg](https://zh.wikipedia.org/wiki/File:Brahe_notebook.jpg "fig:Brahe_notebook.jpg")
在[第谷筆記本上一頁的素描中](../Page/第谷·布拉赫.md "wikilink")，這顆彗星被認為是緊挨著[行星中的](../Page/行星.md "wikilink")[金星在移動](../Page/金星.md "wikilink")。\[3\]這幅素描是以地球為中心，[太陽繞著地球](../Page/太阳.md "wikilink")，當時所知的其他的行星則繞著太陽。這是依據第谷自身的假說，以地球為中心所佈局的太陽系，是偏移了中心的[日心說](../Page/日心说.md "wikilink")\[4\]。儘管在第谷的模型中有如此的誤解，他所做的數以萬計的精確觀測，卻讓[克卜勒能夠推導出](../Page/约翰内斯·开普勒.md "wikilink")[行星運動定律](../Page/开普勒定律.md "wikilink")\[5\]。第谷儘管盡其所能的測量，卻不足以得知彗星在大氣層之外究竟有多遠，所以不能以距離來繪圖。\[6\]第谷發現彗星是一種天體，這種看法被普遍的接受，但也引起了許多其他的問題：

  - 彗星的的本質為何？
  - 彗星來自何處？
  - 彗星是否有一定的運行路徑？

這些問題，與其它一些問題，在17世紀引起了冗長的辯論，有許多理論流傳在當時的天文團體內，[伽利略更聲稱這只是一種](../Page/伽利略·伽利莱.md "wikilink")[光學現象](../Page/光学.md "wikilink")，不可能測量出彗星的[視差](../Page/視差.md "wikilink")，但是他的假設並沒有被廣泛的接受。\[7\]

1577年大彗星戲劇性的出現在[天空](../Page/天空.md "wikilink")，使彗星的地位和本質的理論被重新評估。

在其它的研究中，第谷得知彗星尾巴的方向是背向太陽\[8\]。

## 在文學和藝術上

[Great_Comet_of_1577.gif](https://zh.wikipedia.org/wiki/File:Great_Comet_of_1577.gif "fig:Great_Comet_of_1577.gif")看見的1577年大彗星。這是[Jiri
Daschitzky製作的版畫](../Page/Jiri_Daschitzky.md "wikilink")。\]\]

有許多肇因於[彗星經過的文學作品](../Page/彗星.md "wikilink")，也有許多天文學家提出的想法，引發了大量的爭辯。
然而，彗星是出現在天空的天體的想法是最被推崇的觀念，也是許多想發中最接近事實的概念。\[9\][事件啟發的藝術作品是](../Page/視覺藝術.md "wikilink")[Jiri
Daschitzky創作和雕刻的版畫](../Page/Jiri_Daschitzky.md "wikilink")，描繪的靈感來自於1577年11月12日在[布拉格見到的](../Page/布拉格.md "wikilink")1577年大彗星。

## 註解

## 相關條目

  - [彗星列表](../Page/彗星列表.md "wikilink")

[Category:彗星](../Category/彗星.md "wikilink")

1.

2.

3.
4.
5.
6.
7.

8.
9.