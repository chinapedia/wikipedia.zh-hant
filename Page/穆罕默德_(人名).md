**穆罕默德**（[阿拉伯文](../Page/阿拉伯文.md "wikilink")：****）是[阿拉伯人名之一](../Page/阿拉伯人名.md "wikilink")，通常指[伊斯兰教兴起人](../Page/伊斯兰教.md "wikilink")[先知](../Page/先知.md "wikilink")[穆罕默德](../Page/穆罕默德.md "wikilink")（[穆圣](../Page/穆圣.md "wikilink")），也因對此位先知的敬仰，成為了[伊斯蘭世界男性普遍的](../Page/伊斯蘭世界.md "wikilink")[本名和](../Page/本名.md "wikilink")[中间名](../Page/中间名.md "wikilink")，女性亦用作[中间名](../Page/中间名.md "wikilink")。中国[维吾尔族使用的](../Page/维吾尔族.md "wikilink")[维吾尔语属](../Page/维吾尔语.md "wikilink")[突厥语族](../Page/突厥语族.md "wikilink")，穆罕默德一名由此[突厥化](../Page/突厥化.md "wikilink")，中文翻译[维吾尔族人名时](../Page/维吾尔族常见人名列表.md "wikilink")，将“mehmet”译为“**买买提**”。

可以指：

## 阿富汗人名

  - [穆罕默德·塔拉基](../Page/穆罕默德·塔拉基.md "wikilink")（Nur Muhammad
    Taraki），[阿富汗民主共和国总统](../Page/阿富汗民主共和国.md "wikilink")
  - [穆罕默德·纳吉布拉](../Page/穆罕默德·纳吉布拉.md "wikilink")（Mohammad
    Najibullah），阿富汗民主共和国总统
  - [穆罕默德·伊德-{里}-斯·达乌德](../Page/穆罕默德·伊德里斯·达乌德.md "wikilink")，阿富汗共和国前总统
  - [穆罕默德·查希尔·沙阿](../Page/穆罕默德·查希尔·沙阿.md "wikilink")，阿富汗末代国王，1933-1973年在位

## 巴基斯坦人名

  - [穆罕默德·阿里·真纳](../Page/穆罕默德·阿里·真纳.md "wikilink")，[巴基斯坦国父](../Page/巴基斯坦.md "wikilink")
  - [穆罕默德·拉菲克·塔拉尔](../Page/穆罕默德·拉菲克·塔拉尔.md "wikilink")（Muhammad Rafiq
    Tarar），1998-2001年[巴基斯坦总统](../Page/巴基斯坦总统.md "wikilink")
  - [米拉吉·穆罕默德·罕](../Page/米拉吉·穆罕默德·罕.md "wikilink")（Meraj Muhammad
    Khan），巴基斯坦政治人物

## 埃及人名

  - [穆罕默德·巴拉迪](../Page/穆罕默德·巴拉迪.md "wikilink")（阿拉伯语：），[埃及人](../Page/埃及.md "wikilink")，[国际原子能机构总干事](../Page/国际原子能机构.md "wikilink")
  - [艾哈迈德·马哈茂德·穆罕默德·纳齐夫](../Page/艾哈迈德·马哈茂德·穆罕默德·纳齐夫.md "wikilink")（Dr.
    Ahmad Mahoud Muhammad Nazif），[埃及总理](../Page/埃及总理.md "wikilink")
  - [阿提夫·穆罕默德·奥贝德](../Page/阿提夫·穆罕默德·奥贝德.md "wikilink")（Atef Mohammed
    Obeid），前埃及总理
  - [穆罕默德·阿里](../Page/穆罕默德·阿里_\(埃及\).md "wikilink")，十九世纪埃及统治者
  - [穆罕默德·沙拿](../Page/穆罕默德·沙拿.md "wikilink")，埃及球員，現效力英超球會[利物浦](../Page/利物浦.md "wikilink")

## 伊朗人名

  - [阿迦·穆罕默德·汗](../Page/阿迦·穆罕默德·汗.md "wikilink")，伊朗[卡扎尔王朝建立者](../Page/卡扎尔王朝.md "wikilink")
  - [穆罕默德·阿里·沙](../Page/穆罕默德·阿里·沙.md "wikilink")，伊朗卡扎尔王朝第六任君主
  - [穆罕默德·礼萨·巴列维](../Page/穆罕默德·礼萨·巴列维.md "wikilink")（波斯语：），伊朗末代国王
  - [穆罕默德·阿里·拉贾伊](../Page/穆罕默德·阿里·拉贾伊.md "wikilink")（Mohammad Ali
    Rajai），第二任伊朗总统
  - [卡拉·穆罕默德](../Page/卡拉·穆罕默德.md "wikilink")，西亞[黑羊王朝第一任君主](../Page/黑羊王朝.md "wikilink")
  - [穆罕默德·哈塔米](../Page/穆罕默德·哈塔米.md "wikilink")（波斯语：），第五任[伊朗总统](../Page/伊朗总统.md "wikilink")

## 非洲人名

  - [迪莱塔·穆罕默德·迪莱塔](../Page/迪莱塔·穆罕默德·迪莱塔.md "wikilink")（Dileita Mohamed
    Dileita），非洲[吉布提总理](../Page/吉布提.md "wikilink")
  - [穆罕默德·西索科](../Page/穆罕默德·西索科.md "wikilink")（Mohamed
    Sissoko），非洲[马里足球运动员](../Page/马里.md "wikilink")，效力于[利物浦](../Page/利物浦.md "wikilink")
  - [穆罕默德·卡迪尔](../Page/穆罕默德·卡迪尔.md "wikilink")（Mohamed Abdel Kader
    Coubadja
    Touré），非洲[多哥足球运动员](../Page/多哥.md "wikilink")，为[多哥国家足球队历史上首位在](../Page/多哥国家足球队.md "wikilink")[世界杯比赛取得入球的球员](../Page/世界杯.md "wikilink")
  - [穆罕默德·布瓦吉吉](../Page/穆罕默德·布瓦吉吉.md "wikilink")，北非[突尼西亞籍的已故小販](../Page/突尼西亞.md "wikilink")，他的自焚引發[茉莉花革命](../Page/茉莉花革命.md "wikilink")，並在阿拉伯世界引發骨牌效應。

## 其他

  - [纳赛尔·穆罕默德·萨巴赫](../Page/纳赛尔·穆罕默德·萨巴赫.md "wikilink")（Nasser Mohammed
    Al-Ahmed Al-Sabah），[科威特首相](../Page/科威特.md "wikilink")
  - [穆罕默德·本·拉希德·阿勒马克图姆](../Page/穆罕默德·本·拉希德·阿勒马克图姆.md "wikilink")（Sheikh
    Mohammed bin Rashid Al
    Maktoum），[阿拉伯联合酋长国总理](../Page/阿拉伯联合酋长国.md "wikilink")
  - [艾哈迈德·阿卜杜拉·穆罕默德·桑比](../Page/艾哈迈德·阿卜杜拉·穆罕默德·桑比.md "wikilink")（Ahmed
    Abdallah Mohamed Sambi），[科摩罗总统](../Page/科摩罗.md "wikilink")
  - [穆罕默德·巴哈杜尔·沙·扎法](../Page/穆罕默德·巴哈杜尔·沙·扎法.md "wikilink")，印度[莫卧儿帝国末代皇帝](../Page/莫卧儿帝国.md "wikilink")
  - [穆罕默德·阿里](../Page/穆罕默德·阿里.md "wikilink")（محمد على），美国拳击手
  - [穆罕默德·赛义德·阿尔-萨哈夫](../Page/穆罕默德·赛义德·阿尔-萨哈夫.md "wikilink")（محمد سعيد
    الصحاف ），伊拉克政治家
  - [穆罕默德·尤纳斯](../Page/穆罕默德·尤纳斯.md "wikilink")（Muhammod
    Yunus），[孟加拉国银行家](../Page/孟加拉国.md "wikilink")、[经济学家](../Page/经济学家.md "wikilink")

[M](../Category/阿拉伯语人名.md "wikilink")