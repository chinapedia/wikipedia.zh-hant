**2007年亞洲盃外圍賽**是透過外圍賽決定出12隊參加[2007年亞洲盃決賽週的名額](../Page/2007年亞洲盃足球賽.md "wikilink")，上屆亞洲盃衛冕冠軍[日本不獲得一個直接出線決賽週名額](../Page/日本國家足球隊.md "wikilink")，需要與其他球隊參加外圍賽。另外[印尼](../Page/印尼.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國和](../Page/泰國.md "wikilink")[越南以東道主身份自動晉級決賽週](../Page/越南.md "wikilink")。而[澳洲是新加入](../Page/澳洲.md "wikilink")[亞洲足協後首次參加區內賽事](../Page/亞洲足協.md "wikilink")。

## 出線球隊

  - [Flag_of_Indonesia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Indonesia.svg "fig:Flag_of_Indonesia.svg") [印尼](../Page/印尼國家足球隊.md "wikilink")（主辦國）
  - [Flag_of_Malaysia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Malaysia.svg "fig:Flag_of_Malaysia.svg") [馬來西亞](../Page/馬來西亞國家足球隊.md "wikilink")（主辦國）
  - [Flag_of_Thailand.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Thailand.svg "fig:Flag_of_Thailand.svg") [泰國](../Page/泰國國家足球隊.md "wikilink")（主辦國）
  - [Flag_of_Vietnam.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Vietnam.svg "fig:Flag_of_Vietnam.svg") [越南](../Page/越南國家足球隊.md "wikilink")（主辦國）
  - [Flag_of_Qatar.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Qatar.svg "fig:Flag_of_Qatar.svg") [卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")
  - [Flag_of_Saudi_Arabia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Saudi_Arabia.svg "fig:Flag_of_Saudi_Arabia.svg") [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink")
  - [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") [日本](../Page/日本國家足球隊.md "wikilink")
  - [Flag_of_the_United_Arab_Emirates.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Arab_Emirates.svg "fig:Flag_of_the_United_Arab_Emirates.svg") [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")
  - [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") [澳洲](../Page/澳洲國家足球隊.md "wikilink")
  - [Flag_of_Iran.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Iran.svg "fig:Flag_of_Iran.svg") [伊朗](../Page/伊朗國家足球隊.md "wikilink")
  - [Flag_of_South_Korea.svg](https://zh.wikipedia.org/wiki/File:Flag_of_South_Korea.svg "fig:Flag_of_South_Korea.svg") [韓國](../Page/韓國國家足球隊.md "wikilink")
  - [Flag_of_Iraq.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Iraq.svg "fig:Flag_of_Iraq.svg") [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")
  - [Flag_of_Oman.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Oman.svg "fig:Flag_of_Oman.svg") [阿曼](../Page/阿曼國家足球隊.md "wikilink")
  - [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg") [中國](../Page/中國國家足球隊.md "wikilink")
  - [Flag_of_Uzbekistan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Uzbekistan.svg "fig:Flag_of_Uzbekistan.svg") [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink")
  - [Flag_of_Bahrain.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bahrain.svg "fig:Flag_of_Bahrain.svg") [巴林](../Page/巴林國家足球隊.md "wikilink")

## 沒有參加外圍賽球隊

由於決賽週共有四個主辦國直接出線參賽，因此外圍賽只安排二十五隊參加，令部分[國際足協世界排名較低的球隊沒有參加賽事](../Page/國際足協世界排名.md "wikilink")，以下是沒有參加今屆外圍賽的球隊（數目代表2005年12月20日時的世界排名）：

  - （82）

  - （116）

  - （133）

  - （144）

  - （147）

  - （157）

  - （170）

  - （175）

  - （179）

  - （188）

  - （189）

  - （190）

  - （191）

  - （192）

  - （199）

  - （204）

  - （沒有排名）

## 預賽

由於共有25隊參加外圍賽，不能夠平均分配每4隊分為6組，[亞洲足協特別加設預賽](../Page/亞洲足協.md "wikilink")，由所有參賽隊伍當中的兩隊世界排名最低的[孟加拉和](../Page/孟加拉國家足球隊.md "wikilink")[巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink")，兩隊以兩回合主客制爭奪一個分組賽名額，預賽原本定於2005年11月進行，但[巴基斯坦發生大地震而延期至](../Page/巴基斯坦.md "wikilink")12月舉行。

  - 首回合－2005年12月22日 孟加拉 0 - 0 巴基斯坦
  - 次回合－2005年12月26日 巴基斯坦 0 - 1 孟加拉

<!-- end list -->

  -
    **[孟加拉總比數以](../Page/孟加拉.md "wikilink")1比0晉級分組賽**。

## 分組賽

24隊被分為6組，每組各有4支隊伍，以雙循環主客制作賽，各小組成績最好的2隊（共12隊）可以晉級決賽週。2006年1月4日進行分組賽抽籤，同時並公佈2007年亞洲盃官方標誌。抽籤前24隊分為4個檔次，包括：

### 種子檔次

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>第 1 檔次</p></th>
<th><p>第 2 檔次</p></th>
<th><p>第 3 檔次</p></th>
<th><p>第 4 檔次</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

在四個檔次中各有一隊編入6個小組其中之一，而[澳洲已經鎖定編入為D組](../Page/澳洲.md "wikilink")，抽籤前[斯里蘭卡臨時退出](../Page/斯里蘭卡.md "wikilink")，[巴基斯坦替補其席位](../Page/巴基斯坦.md "wikilink")。

### A組

|   | 球隊                                                                                                                                                                     | 賽 | 勝 | 和 | 負 | 入球 | 失球 | 球差   | 積分     |
| - | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | -- | -- | ---- | ------ |
| 1 | [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") '''[日本](../Page/日本國家足球隊.md "wikilink")                             | 6 | 5 | 0 | 1 | 15 | 2  | \+13 | **15** |
| 2 | [Flag_of_Saudi_Arabia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Saudi_Arabia.svg "fig:Flag_of_Saudi_Arabia.svg") '''[沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | 6 | 5 | 0 | 1 | 21 | 4  | \+17 | **15** |
| 3 | [Flag_of_Yemen.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Yemen.svg "fig:Flag_of_Yemen.svg") [葉門](../Page/也門國家足球隊.md "wikilink")                                | 6 | 2 | 0 | 4 | 5  | 13 | \-8  | **6**  |
| 4 | [Flag_of_India.svg](https://zh.wikipedia.org/wiki/File:Flag_of_India.svg "fig:Flag_of_India.svg") [印度](../Page/印度國家足球隊.md "wikilink")                                | 6 | 0 | 0 | 6 | 2  | 23 | \-22 | **0**  |

|             |                                           |           |                                           |                                                                      |  |
| ----------- | ----------------------------------------- | --------- | ----------------------------------------- | -------------------------------------------------------------------- |  |
| 2006年2月22日  | [日本](../Page/日本國家足球隊.md "wikilink")       | **6 - 0** | [印度](../Page/印度國家足球隊.md "wikilink")       | [日本](../Page/日本.md "wikilink")[橫濱](../Page/橫濱.md "wikilink")         |  |
|             | [葉門](../Page/也門國家足球隊.md "wikilink")       | **0 - 4** | [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | [葉門](../Page/葉門.md "wikilink")[薩那](../Page/薩那.md "wikilink")         |  |
| 2006年3月1日   | [印度](../Page/印度國家足球隊.md "wikilink")       | **0 - 3** | [葉門](../Page/也門國家足球隊.md "wikilink")       | [印度](../Page/印度.md "wikilink")[新德里](../Page/新德里.md "wikilink")       |  |
| 2006年8月16日  | [印度](../Page/印度國家足球隊.md "wikilink")       | **0 - 3** | [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | [印度](../Page/印度.md "wikilink")[加爾各答](../Page/加爾各答.md "wikilink")     |  |
|             |                                           |           |                                           |                                                                      |  |
|             | [日本](../Page/日本國家足球隊.md "wikilink")       | **2 - 0** | [葉門](../Page/也門國家足球隊.md "wikilink")       | [日本](../Page/日本.md "wikilink")[新潟](../Page/新潟.md "wikilink")         |  |
| 2006年9月3日   | [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | **1 - 0** | [日本](../Page/日本國家足球隊.md "wikilink")       | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")[吉達](../Page/吉達.md "wikilink") |  |
| 2006年9月6日   | [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | **7 - 1** | [印度](../Page/印度國家足球隊.md "wikilink")       | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")[吉達](../Page/吉達.md "wikilink") |  |
|             |                                           |           |                                           |                                                                      |  |
|             | [葉門](../Page/也門國家足球隊.md "wikilink")       | **0 - 1** | [日本](../Page/日本國家足球隊.md "wikilink")       | [葉門](../Page/葉門.md "wikilink")[薩那](../Page/薩那.md "wikilink")         |  |
| 2006年10月11日 | [印度](../Page/印度國家足球隊.md "wikilink")       | **0 - 3** | [日本](../Page/日本國家足球隊.md "wikilink")       | [印度](../Page/印度.md "wikilink")[班加羅爾](../Page/班加羅爾.md "wikilink")     |  |
|             |                                           |           |                                           |                                                                      |  |
|             | [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | **5 - 0** | [葉門](../Page/也門國家足球隊.md "wikilink")       | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")[吉達](../Page/吉達.md "wikilink") |  |
| 2006年11月15日 | [日本](../Page/日本國家足球隊.md "wikilink")       | **3 - 1** | [沙特阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink") | [日本](../Page/日本.md "wikilink")[札幌](../Page/札幌.md "wikilink")         |  |
|             |                                           |           |                                           |                                                                      |  |
|             | [葉門](../Page/也門國家足球隊.md "wikilink")       | **2 - 1** | [印度](../Page/印度國家足球隊.md "wikilink")       | [葉門](../Page/葉門.md "wikilink")[薩那](../Page/薩那.md "wikilink")         |  |

### B組

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>球隊</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>入球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
<th><p>積分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong></strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>12</p></td>
<td><p>2</p></td>
<td><p>+10</p></td>
<td><p><strong>14</strong></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><strong></strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>15</p></td>
<td><p>5</p></td>
<td><p>+10</p></td>
<td><p><strong>11</strong></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Syria.svg" title="fig:Flag_of_Syria.svg">Flag_of_Syria.svg</a> <a href="../Page/敘利亞國家足球隊.md" title="wikilink">敘利亞</a></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>10</p></td>
<td><p>6</p></td>
<td><p>+4</p></td>
<td><p><strong>8</strong></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>24</p></td>
<td><p>-24</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

|             |                                         |           |                                         |                                                                    |  |
| ----------- | --------------------------------------- | --------- | --------------------------------------- | ------------------------------------------------------------------ |  |
| 2006年2月22日  | [伊朗](../Page/伊朗國家足球隊.md "wikilink")     | **4 - 0** | [中華台北](../Page/中華台北足球代表隊.md "wikilink") | [伊朗](../Page/伊朗.md "wikilink")[德黑蘭](../Page/德黑蘭.md "wikilink")     |  |
|             | [敘利亞](../Page/敘利亞國家足球隊.md "wikilink")   | **1 - 2** | [南韓](../Page/南韓國家足球隊.md "wikilink")     | [敘利亞](../Page/敘利亞.md "wikilink")[阿勒頗](../Page/阿勒頗.md "wikilink")   |  |
| 2006年3月1日   | [中華台北](../Page/中華台北足球代表隊.md "wikilink") | **0 - 4** | [敘利亞](../Page/敘利亞國家足球隊.md "wikilink")   | [台灣](../Page/台灣.md "wikilink")[台北](../Page/台北.md "wikilink")       |  |
| 2006年8月16日  | [中華台北](../Page/中華台北足球代表隊.md "wikilink") | **0 - 3** | [南韓](../Page/南韓國家足球隊.md "wikilink")     | [台灣](../Page/台灣.md "wikilink")[台北](../Page/台北.md "wikilink")       |  |
|             |                                         |           |                                         |                                                                    |  |
|             | [伊朗](../Page/伊朗國家足球隊.md "wikilink")     | **1 - 1** | [敘利亞](../Page/敘利亞國家足球隊.md "wikilink")   | [伊朗](../Page/伊朗.md "wikilink")[德黑蘭](../Page/德黑蘭.md "wikilink")     |  |
| 2006年9月2日   | [南韓](../Page/南韓國家足球隊.md "wikilink")     | **1 - 1** | [伊朗](../Page/伊朗國家足球隊.md "wikilink")     | [南韓](../Page/南韓.md "wikilink")[首爾](../Page/首爾.md "wikilink")       |  |
| 2006年9月6日   | [南韓](../Page/南韓國家足球隊.md "wikilink")     | **8 - 0** | [中華台北](../Page/中華台北足球代表隊.md "wikilink") | [南韓](../Page/南韓.md "wikilink")[水原](../Page/水原.md "wikilink")       |  |
|             |                                         |           |                                         |                                                                    |  |
|             | [敘利亞](../Page/敘利亞國家足球隊.md "wikilink")   | **0 - 2** | [伊朗](../Page/伊朗國家足球隊.md "wikilink")     | [敘利亞](../Page/敘利亞.md "wikilink")[大馬士革](../Page/大馬士革.md "wikilink") |  |
| 2006年10月11日 | [中華台北](../Page/中華台北足球代表隊.md "wikilink") | **0 - 2** | [伊朗](../Page/伊朗國家足球隊.md "wikilink")     | [台灣](../Page/台灣.md "wikilink")[台北](../Page/台北.md "wikilink")       |  |
|             |                                         |           |                                         |                                                                    |  |
|             | [南韓](../Page/南韓國家足球隊.md "wikilink")     | **1 - 1** | [敘利亞](../Page/敘利亞國家足球隊.md "wikilink")   | [南韓](../Page/南韓.md "wikilink")[首爾](../Page/首爾.md "wikilink")       |  |
| 2006年11月15日 | [伊朗](../Page/伊朗國家足球隊.md "wikilink")     | **2 - 0** | [南韓](../Page/南韓國家足球隊.md "wikilink")     | [伊朗](../Page/伊朗.md "wikilink")[德黑蘭](../Page/德黑蘭.md "wikilink")     |  |
|             |                                         |           |                                         |                                                                    |  |
|             | [敘利亞](../Page/敘利亞國家足球隊.md "wikilink")   | **3 - 0** | [中華台北](../Page/中華台北足球代表隊.md "wikilink") | [敘利亞](../Page/敘利亞.md "wikilink")[大馬士革](../Page/大馬士革.md "wikilink") |  |

### C組

|   | 球隊                                                                                                                                                                                                        | 賽 | 勝 | 和 | 負 | 入球 | 失球 | 球差   | 積分     |
| - | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | -- | -- | ---- | ------ |
| 1 | [Flag_of_the_United_Arab_Emirates.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Arab_Emirates.svg "fig:Flag_of_the_United_Arab_Emirates.svg") **[阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")** | 6 | 4 | 1 | 1 | 11 | 6  | \+5  | **13** |
| 2 | [Flag_of_Oman.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Oman.svg "fig:Flag_of_Oman.svg") **[阿曼](../Page/阿曼國家足球隊.md "wikilink")**                                                                  | 6 | 4 | 0 | 2 | 14 | 6  | \+8  | **12** |
| 3 | [Flag_of_Jordan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Jordan.svg "fig:Flag_of_Jordan.svg") [約旦](../Page/約旦國家足球隊.md "wikilink")                                                                | 6 | 3 | 1 | 2 | 10 | 5  | \+5  | **10** |
| 4 | [Flag_of_Pakistan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Pakistan.svg "fig:Flag_of_Pakistan.svg") [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink")                                                      | 6 | 0 | 0 | 6 | 4  | 22 | \-18 | **0**  |

|             |                                         |           |                                         |                                                                      |  |
| ----------- | --------------------------------------- | --------- | --------------------------------------- | -------------------------------------------------------------------- |  |
| 2006年2月22日  | [約旦](../Page/約旦國家足球隊.md "wikilink")     | **3 - 0** | [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink") | [約旦](../Page/約旦.md "wikilink")[安曼](../Page/安曼.md "wikilink")         |  |
|             | [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")   | **1 - 0** | [阿曼](../Page/阿曼國家足球隊.md "wikilink")     | [阿聯酋](../Page/阿聯酋.md "wikilink")[杜拜](../Page/杜拜.md "wikilink")       |  |
| 2006年3月1日   | [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink") | **1 - 4** | [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")   | [巴基斯坦](../Page/巴基斯坦.md "wikilink")[卡拉奇](../Page/卡拉奇.md "wikilink")   |  |
|             | [阿曼](../Page/阿曼國家足球隊.md "wikilink")     | **3 - 0** | [約旦](../Page/約旦國家足球隊.md "wikilink")     | [阿曼Wattayah](../Page/阿曼.md "wikilink")                               |  |
| 2006年8月16日  | [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink") | **1 - 4** | [阿曼](../Page/阿曼國家足球隊.md "wikilink")     | [巴基斯坦](../Page/巴基斯坦.md "wikilink")[伊斯蘭堡](../Page/伊斯蘭堡.md "wikilink") |  |
|             |                                         |           |                                         |                                                                      |  |
|             | [約旦](../Page/約旦國家足球隊.md "wikilink")     | **1 - 2** | [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")   | [約旦](../Page/約旦.md "wikilink")[安曼](../Page/安曼.md "wikilink")         |  |
| 2006年9月6日   | [阿曼](../Page/阿曼國家足球隊.md "wikilink")     | **5 - 0** | [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink") | [阿曼](../Page/阿曼.md "wikilink")[馬斯喀特](../Page/馬斯喀特.md "wikilink")     |  |
|             |                                         |           |                                         |                                                                      |  |
|             | [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")   | **0 - 0** | [約旦](../Page/約旦國家足球隊.md "wikilink")     | [阿聯酋](../Page/阿聯酋.md "wikilink")[杜拜](../Page/杜拜.md "wikilink")       |  |
| 2006年10月11日 | [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink") | **0 - 3** | [約旦](../Page/約旦國家足球隊.md "wikilink")     | [巴基斯坦](../Page/巴基斯坦.md "wikilink")[拉合爾](../Page/拉合爾.md "wikilink")   |  |
|             |                                         |           |                                         |                                                                      |  |
|             | [阿曼](../Page/阿曼國家足球隊.md "wikilink")     | **2 - 1** | [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")   | [阿曼](../Page/阿曼.md "wikilink")[馬斯喀特](../Page/馬斯喀特.md "wikilink")     |  |
| 2006年11月15日 | [約旦](../Page/約旦國家足球隊.md "wikilink")     | **3 - 0** | [阿曼](../Page/阿曼國家足球隊.md "wikilink")     | [約旦](../Page/約旦.md "wikilink")[安曼](../Page/安曼.md "wikilink")         |  |
|             |                                         |           |                                         |                                                                      |  |
|             | [阿聯酋](../Page/阿聯酋國家足球隊.md "wikilink")   | **3 - 2** | [巴基斯坦](../Page/巴基斯坦國家足球隊.md "wikilink") | [阿聯酋](../Page/阿聯酋.md "wikilink")[杜拜](../Page/杜拜.md "wikilink")       |  |

### D組

|   | 球隊                                                                                                                                                      | 賽     | 勝     | 和     | 負     | 入球    | 失球    | 球差    | 積分    |
| - | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| 1 | [Flag_of_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australia.svg "fig:Flag_of_Australia.svg") **[澳洲](../Page/澳洲國家足球隊.md "wikilink")** | 4     | 3     | 0     | 1     | 7     | 3     | \+4   | 9     |
| 2 | [Flag_of_Bahrain.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bahrain.svg "fig:Flag_of_Bahrain.svg") **[巴林](../Page/巴林國家足球隊.md "wikilink")**       | 4     | 1     | 1     | 2     | 3     | 6     | \-3   | 4     |
| 3 | [Flag_of_Kuwait.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kuwait.svg "fig:Flag_of_Kuwait.svg") [科威特](../Page/科威特國家足球隊.md "wikilink")            | 4     | 1     | 1     | 2     | 3     | 4     | \-1   | 4     |
| 4 | [Flag_of_Lebanon.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Lebanon.svg "fig:Flag_of_Lebanon.svg") [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink")         | **-** | **-** | **-** | **-** | **-** | **-** | **-** | **-** |

**8月1日亞洲足協宣佈黎巴嫩由於在[以黎衝突遭受嚴重襲擊導致無法參加餘下比賽](../Page/2006年以黎衝突.md "wikilink")，因此退出2007年亞洲盃外圍賽。**[亞洲足協官方網站](https://web.archive.org/web/20070930181816/http://www.the-afc.com/english/media/default.asp?mnsection=media&section=newsDetails&newsID=6618)

|             |                                       |                                       |                                       |                                                                    |  |
| ----------- | ------------------------------------- | ------------------------------------- | ------------------------------------- | ------------------------------------------------------------------ |  |
| 2006年2月22日  | [巴林](../Page/巴林國家足球隊.md "wikilink")   | **1 - 3**                             | [澳洲](../Page/澳洲國家足球隊.md "wikilink")   | [巴林](../Page/巴林.md "wikilink")[麥納麥](../Page/麥納麥.md "wikilink")     |  |
|             | [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink") | <font color="#ff0000">**賽果取消**</font> | [科威特](../Page/科威特國家足球隊.md "wikilink") | [黎巴嫩](../Page/黎巴嫩.md "wikilink")[貝魯特](../Page/貝魯特.md "wikilink")   |  |
| 2006年3月1日   | [科威特](../Page/科威特國家足球隊.md "wikilink") | **0 - 0**                             | [巴林](../Page/巴林國家足球隊.md "wikilink")   | [科威特](../Page/科威特.md "wikilink")[科威特城](../Page/科威特城.md "wikilink") |  |
| 2006年8月16日  | [澳洲](../Page/澳洲國家足球隊.md "wikilink")   | **2 - 0**                             | [科威特](../Page/科威特國家足球隊.md "wikilink") | [澳洲](../Page/澳洲.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")       |  |
|             |                                       |                                       |                                       |                                                                    |  |
|             | [巴林](../Page/巴林國家足球隊.md "wikilink")   | <font color="#ff0000">**取消**</font>   | [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink") | [巴林](../Page/巴林.md "wikilink")[麥納麥](../Page/麥納麥.md "wikilink")     |  |
| 2006年9月1日   | [澳洲](../Page/澳洲國家足球隊.md "wikilink")   | <font color="#ff0000">**取消**</font>   | [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink") | [澳洲](../Page/澳洲.md "wikilink")[珀斯](../Page/珀斯.md "wikilink")       |  |
| 2006年9月6日   | [科威特](../Page/科威特國家足球隊.md "wikilink") | **2 - 0**                             | [澳洲](../Page/澳洲國家足球隊.md "wikilink")   | [科威特](../Page/科威特.md "wikilink")[科威特城](../Page/科威特城.md "wikilink") |  |
|             |                                       |                                       |                                       |                                                                    |  |
|             | [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink") | <font color="#ff0000">**取消**</font>   | [巴林](../Page/巴林國家足球隊.md "wikilink")   | [黎巴嫩](../Page/黎巴嫩.md "wikilink")[貝魯特](../Page/貝魯特.md "wikilink")   |  |
| 2006年10月11日 | [澳洲](../Page/澳洲國家足球隊.md "wikilink")   | **2 - 0**                             | [巴林](../Page/巴林國家足球隊.md "wikilink")   | [澳洲](../Page/澳洲.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")       |  |
|             |                                       |                                       |                                       |                                                                    |  |
|             | [科威特](../Page/科威特國家足球隊.md "wikilink") | <font color="#ff0000">**取消**</font>   | [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink") | [科威特](../Page/科威特.md "wikilink")[科威特城](../Page/科威特城.md "wikilink") |  |
| 2006年11月15日 | [巴林](../Page/巴林國家足球隊.md "wikilink")   | **2 - 1**                             | [科威特](../Page/科威特國家足球隊.md "wikilink") | [巴林](../Page/巴林.md "wikilink")[麥納麥](../Page/麥納麥.md "wikilink")     |  |
|             |                                       |                                       |                                       |                                                                    |  |
|             | [黎巴嫩](../Page/黎巴嫩國家足球隊.md "wikilink") | <font color="#ff0000">**取消**</font>   | [澳洲](../Page/澳洲國家足球隊.md "wikilink")   | [黎巴嫩](../Page/黎巴嫩.md "wikilink")[貝魯特](../Page/貝魯特.md "wikilink")   |  |

### E組

|   | 球隊                                                                                                                                                                                                                         | 賽 | 勝 | 和 | 負 | 入球 | 失球 | 球差  | 積分     |
| - | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | -- | -- | --- | ------ |
| 1 | [Flag_of_Iraq.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Iraq.svg "fig:Flag_of_Iraq.svg") **[伊拉克](../Page/伊拉克國家足球隊.md "wikilink")**                                                                                 | 6 | 3 | 2 | 1 | 12 | 8  | \+4 | **11** |
| 2 | [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg") **[中國](../Page/中國國家足球隊.md "wikilink")** | 6 | 3 | 2 | 1 | 7  | 3  | \+4 | **11** |
| 3 | [Flag_of_Singapore.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Singapore.svg "fig:Flag_of_Singapore.svg") [新加坡](../Page/新加坡國家足球隊.md "wikilink")                                                                      | 5 | 1 | 1 | 3 | 4  | 6  | \-2 | **4**  |
| 4 | [Flag_of_Palestine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Palestine.svg "fig:Flag_of_Palestine.svg") [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink")                                                                    | 5 | 1 | 1 | 3 | 3  | 9  | \-6 | **4**  |

|             |                                         |           |                                         |                                                                     |  |
| ----------- | --------------------------------------- | --------- | --------------------------------------- | ------------------------------------------------------------------- |  |
| 2006年2月22日  | [中國](../Page/中國國家足球隊.md "wikilink")     | **2 - 0** | [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink") | [中國](../Page/中國.md "wikilink")[廣州](../Page/廣州.md "wikilink")        |  |
|             | [新加坡](../Page/新加坡國家足球隊.md "wikilink")   | **2 - 0** | [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")   | [新加坡](../Page/新加坡.md "wikilink")                                    |  |
| 2006年3月1日   | [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink") | **1 - 0** | [新加坡](../Page/新加坡國家足球隊.md "wikilink")   | [約旦](../Page/約旦.md "wikilink")[安曼](../Page/安曼.md "wikilink")（中立場）   |  |
|             | [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")   | **2 - 1** | [中國](../Page/中國國家足球隊.md "wikilink")     | [阿聯酋](../Page/阿聯酋.md "wikilink")[艾因](../Page/艾因.md "wikilink")（中立場） |  |
| 2006年8月16日  | [中國](../Page/中國國家足球隊.md "wikilink")     | **1 - 0** | [新加坡](../Page/新加坡國家足球隊.md "wikilink")   | [中國](../Page/中國.md "wikilink")[天津](../Page/天津.md "wikilink")        |  |
| 2006年8月17日  | [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink") | **0 - 3** | [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")   | [約旦](../Page/約旦.md "wikilink")[安曼](../Page/安曼.md "wikilink")（中立場）   |  |
| 2006年9月6日   | [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")   | **2 - 2** | [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink") | [阿聯酋](../Page/阿聯酋.md "wikilink")[艾因](../Page/艾因.md "wikilink")（中立場） |  |
|             | [新加坡](../Page/新加坡國家足球隊.md "wikilink")   | **0 - 0** | [中國](../Page/中國國家足球隊.md "wikilink")     | [新加坡](../Page/新加坡.md "wikilink")                                    |  |
| 2006年10月11日 | [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")   | **4 - 2** | [新加坡](../Page/新加坡國家足球隊.md "wikilink")   | [阿聯酋](../Page/阿聯酋.md "wikilink")[艾因](../Page/艾因.md "wikilink")（中立場） |  |
|             | [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink") | **0 - 2** | [中國](../Page/中國國家足球隊.md "wikilink")     | [約旦](../Page/約旦.md "wikilink")[安曼](../Page/安曼.md "wikilink")（中立場）   |  |
| 2006年11月15日 | [新加坡](../Page/新加坡國家足球隊.md "wikilink")   | **取消**    | [巴勒斯坦](../Page/巴勒斯坦國家足球隊.md "wikilink") | [新加坡](../Page/新加坡.md "wikilink")                                    |  |
|             | [中國](../Page/中國國家足球隊.md "wikilink")     | **1 - 1** | [伊拉克](../Page/伊拉克國家足球隊.md "wikilink")   | [中國](../Page/中國.md "wikilink")[長沙](../Page/長沙.md "wikilink")        |  |

### F組

|   | 球隊                                                                                                                                                             | 賽 | 勝 | 和 | 負 | 入球 | 失球 | 球差   | 積分     |
| - | -------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | -- | -- | ---- | ------ |
| 1 | [Flag_of_Qatar.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Qatar.svg "fig:Flag_of_Qatar.svg") **[卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")**                  | 6 | 5 | 0 | 1 | 14 | 4  | \+10 | **15** |
| 2 | [Flag_of_Uzbekistan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Uzbekistan.svg "fig:Flag_of_Uzbekistan.svg") **[烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink")** | 6 | 3 | 2 | 1 | 14 | 4  | \+10 | **11** |
| 3 | [Flag_of_Hong_Kong.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong.svg "fig:Flag_of_Hong_Kong.svg") [香港](../Page/香港足球代表隊.md "wikilink")           | 6 | 2 | 2 | 2 | 5  | 7  | \-2  | **8**  |
| 4 | [Flag_of_Bangladesh.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bangladesh.svg "fig:Flag_of_Bangladesh.svg") [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")       | 6 | 0 | 0 | 6 | 1  | 19 | \-18 | **0**  |

|             |                                         |            |                                         |                                                                    |  |
| ----------- | --------------------------------------- | ---------- | --------------------------------------- | ------------------------------------------------------------------ |  |
| 2006年2月22日  | [香港](../Page/香港足球代表隊.md "wikilink")     | **0 - 3**  | [卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")   | [香港](../Page/香港.md "wikilink")                                     |  |
|             | [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink") | **5 - 0**  | [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")   | [烏茲別克](../Page/烏茲別克.md "wikilink")[塔什干](../Page/塔什干.md "wikilink") |  |
| 2006年3月1日   | [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")   | **0 - 1**  | [香港](../Page/香港足球代表隊.md "wikilink")     | [孟加拉](../Page/孟加拉.md "wikilink")[達卡](../Page/達卡.md "wikilink")     |  |
|             | [卡達](../Page/卡塔爾國家足球隊.md "wikilink")    | **2 - 1**  | [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink") | [卡達](../Page/卡達.md "wikilink")[多哈](../Page/多哈.md "wikilink")       |  |
| 2006年8月16日  | [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")   | **1 - 4**  | [卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")   | [孟加拉](../Page/孟加拉.md "wikilink")[達卡](../Page/達卡.md "wikilink")     |  |
|             |                                         |            |                                         |                                                                    |  |
|             | [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink") | **2 - 2**  | [香港](../Page/香港足球代表隊.md "wikilink")     | [烏茲別克](../Page/烏茲別克.md "wikilink")[塔什干](../Page/塔什干.md "wikilink") |  |
| 2006年9月6日   | [香港](../Page/香港足球代表隊.md "wikilink")     | **0 - 0**  | [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink") | [香港](../Page/香港.md "wikilink")                                     |  |
|             |                                         |            |                                         |                                                                    |  |
|             | [卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")   | **3 - 0**  | [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")   | [卡達](../Page/卡達.md "wikilink")[多哈](../Page/多哈.md "wikilink")       |  |
| 2006年10月11日 | [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")   | **0 - 4**  | [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink") | [孟加拉](../Page/孟加拉.md "wikilink")[達卡](../Page/達卡.md "wikilink")     |  |
|             |                                         |            |                                         |                                                                    |  |
|             | [卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")   | **2 - 0**  | [香港](../Page/香港足球代表隊.md "wikilink")     | [卡達](../Page/卡達.md "wikilink")[多哈](../Page/多哈.md "wikilink")       |  |
| 2006年11月15日 | [香港](../Page/香港足球代表隊.md "wikilink")     | *' 2 - 0*' | [孟加拉](../Page/孟加拉國家足球隊.md "wikilink")   | [香港](../Page/香港.md "wikilink")                                     |  |
|             |                                         |            |                                         |                                                                    |  |
|             | [烏茲別克](../Page/烏茲別克國家足球隊.md "wikilink") | **2 - 0**  | [卡塔爾](../Page/卡塔爾國家足球隊.md "wikilink")   | [烏茲別克](../Page/烏茲別克.md "wikilink")[塔什干](../Page/塔什干.md "wikilink") |  |

## 外部連結

  - [2007年亞洲盃賽程](https://web.archive.org/web/20060818031843/http://www.the-afc.com/english/competitions/asiancup_draw.asp)

[Category:亞足聯亞洲盃](../Category/亞足聯亞洲盃.md "wikilink")
[亞](../Category/2006年足球.md "wikilink")
[亞](../Category/2007年足球.md "wikilink")