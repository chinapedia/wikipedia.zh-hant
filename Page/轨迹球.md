[Trackball-Kensington-ExpertMouse5.jpg](https://zh.wikipedia.org/wiki/File:Trackball-Kensington-ExpertMouse5.jpg "fig:Trackball-Kensington-ExpertMouse5.jpg")
**轨迹球**（**Trackball**）是和[鼠标等一样的](../Page/鼠标.md "wikilink")[计算机定点设备的一种](../Page/计算机定点设备.md "wikilink")。

## 概要

和机械式鼠标一样，轨迹球通过读取可滚动的球滚动的方向和速度来定位。不同的是，鼠标是基座和球一起动，而轨迹球只是球在基座上滚动，基座相对桌面不动。\[1\]

## 特征和優缺點

轨迹球的特点是，通过手指带动球滚动而带动屏幕上指针的移动。这样不需要整个手臂的移动。

### 優點

  - 减少了整个手的疲劳度，特別是有效避免[腕隧道症候群](../Page/腕隧道症候群.md "wikilink")、也就是俗稱[滑鼠手的](../Page/滑鼠手.md "wikilink")[職業傷害](../Page/職業傷害.md "wikilink")。對於已患滑鼠手的使用者，則有助於減緩病狀。
  - 軌跡球不需要放置在平坦表面上亦可使用。例如沙發上或大腿上。
  - 軌跡球不須移動，相對可以減少一些桌面空間，一些高度電腦化的飛機，如[空中巴士A380就以軌跡球做為飛行員的操作工具](../Page/空中巴士A380.md "wikilink")。
  - 滑順的軌跡球和一般滑鼠上的[滾輪相比](../Page/滾輪.md "wikilink")，可以更快速、精準地捲動頁面。
  - 可以比一般滑鼠使用長久，主因底部的不會與桌面摩擦.Rubber 不會掉
  - 軌跡球滑鼠設計上比較符合人體工學，尤其是食指軌跡球，可以長時間操作使用

### 缺點

  - 雖然降低了手腕的負擔，但相對也增加了連續使用電腦的時間。
  - 不利於繪製自由曲線，如[小畫家](../Page/小畫家.md "wikilink")、[Painter等軟體](../Page/Painter.md "wikilink")。
  - 有些舊型的軌跡球產品基於前述優點4的理由，忽略了滾輪的設計，這使得無內建滾輪的軌跡球在使用一些需大量配合滾輪和中鍵指令的軟體時會遇到困難，如[Autocad](../Page/Autocad.md "wikilink")、[Illustrator](../Page/Illustrator.md "wikilink")、[Archicad等繪圖軟體](../Page/Archicad.md "wikilink")。
  - 相較於傳統滾球式滑鼠使用久了會吸附灰塵使得轉動或感應不順而需取下滾球清潔，直接接觸皮膚的軌跡球除灰塵外更會吸附油脂、毛髮等，清潔滾球的頻率比滾球式滑鼠更高。（也因此軌跡球產品大多有易拆式設計）
  - 不利於玩某些遊戲（例如：[osu\!](../Page/osu!.md "wikilink")）
  - 市面上的產品較少，目前常見的廠牌只有Logitech及Kensington。
  - 價格較高。

## 现状

因軌跡球滑鼠種類不多, 部份愛好者會收集早期已停產的產品, 如羅技有名的無限金星和水星, 微軟的Trackball Explorer系列,
和Kensington 產品 (Kensington在兩岸翻譯各不同:在大陸稱為"肯辛通", 在台灣稱為"肯辛頓".

## 操作性

軌跡球的使用感覺和鼠標的使用感覺很不一樣，從鼠標轉到軌跡球感覺很不習慣，而也習慣了軌跡球之後说：“以後再也不用傳統的滑鼠”。手轉動軌跡球滑鼠比較有科技感.

## 种类（按形状分）

### 手掌操作类型

### 食指（中指，无名指）操作类型

  - Logitech

:\*最新木星軌跡球4個按鍵,銀色已停產, 目前是灰色-生產中

:\*第一代木星軌跡球,PS2 接頭, 只有兩個按鍵 - 已停產

:\*無限金星軌跡球 (已停產)

:\*Cordless TrackMan Fx (已停產)

  - Kensington

:\*Expert mouse wireless trackball, P/N: K72359 - (生產中)

:\*SlimBlade Trackball mouse, P/N: K72281 - (已停產)

:\*Orbit trackball mouse with scroll King. P/N: K72337 - 生產中

:\*Orbit Wireless mouse with touch scroll king. P/N: K72352 - 生產中

:\*Orbit Ttrackball mosue with 2 buttons only. module\# 64225. - 已停產

  - SANWA

:\*MA-TB38

  - Microsoft

:\*Intellimouse Trackball, part No.: Mouse-S (PS2)- 已停產

:\*Microsoft Trackball Explorer 1.0 PS2/USB Compatible - 已停產

  - Belkin

:\*PS2 TrackMaster, Model: F3E189 - 已停產

  - ELECOM

:\* HUGE
(M-HT1DRBK)無線版[M-HT1DRBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-HT1DRBK.html)

:\* HUGE
(M-HT1URBK)有線版[M-HT1URBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-HT1URBK.html)

:\* DEFT PRO
(M-DPT1MRBK)[M-DPT1MRBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-DPT1MRBK.html)

:\* DEFT 無線版
(M-DT1DRBK)[M-DT1DRBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-DT1DRBK.html)

:\* DEFT 有線版
(M-DT1URBK)[M-DT1URBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-DT1URBK.html)

### 拇指操作类型

[Logitech-trackball.jpg](https://zh.wikipedia.org/wiki/File:Logitech-trackball.jpg "fig:Logitech-trackball.jpg")

  - Logitech

:\*火星軌跡球（已停產）

:\*無線火星軌跡球（M570）

:\*MX ERGO

  - ELECOM

:\*EX-G
(M-XT2DRBK)無線軌跡球滑鼠[M-XT2DRBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-XT2DRBK.html)

:\*EX-G
(M-XT4DRBK)無線軌跡球滑鼠(左手)[M-XT4DRBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-XT4DRBK.html)

:\*EX-G
(M-XT2URBK)有線線軌跡球滑鼠[M-XT2URBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-XT2URBK.html)

:\*EX-G
pro(M-XPT1MRBK)[M-XPT1MRBK](https://www2.elecom.co.jp.e.gj.hp.transer.com/products/M-XPT1MRBK.html)

## 注解

<div class="references-small">

<references/>

</div>

2\.[軌跡球不死！10款軌跡球介紹，羅技、Kensington、SANWA
登場](http://www.techbang.com/posts/11369-trackball-does-not-die-8-trackball-kensington-introduced-logitech-trackball)
3\.[Kensington
兩種截然不同的軌跡球體驗，你無法想像的美妙～](http://www.mobile01.com/newsdetail.php?id=7914)
4\.[使用 Kensington
軌跡球三年多的心得](http://appleuser.com/2013/10/08/kensington-trackball/)
5\.[軌跡球再一發～Kensington
Orbit升級版殺出！](http://www.mobile01.com/topicdetail.php?f=497&t=1380860)
6\.[羅技火星軌跡球](http://www.mobile01.com/topicdetail.php?f=351&t=1051382)
7\.[羅技 M570
無線軌跡球簡單開箱](http://www.mobile01.com/topicdetail.php?f=497&t=2053045)
8\.[\[開箱\]
舒適取勝：黯然消魂的羅技無線軌跡球M570](http://home.gamer.com.tw/creationDetail.php?sn=2019864)

## 关联项目

  - [键盘](../Page/键盘.md "wikilink")
  - [鼠标](../Page/鼠标.md "wikilink")

## 外部链接

[Category:電腦輸入裝置](../Category/電腦輸入裝置.md "wikilink")
[Category:计算机定点设备](../Category/计算机定点设备.md "wikilink")

1.