**香港專業教育學院（摩理臣山）**（[英語](../Page/英語.md "wikilink")：Hong Kong Institute of
Vocational Education (Morrison
Hill)，簡稱：IVE(MH)）。該校的前身是[教育署於](../Page/教育署.md "wikilink")1969年成立的**摩理臣山工業學院**，是香港成立的第一所工業學院，於1982年移交予新成立的[職業訓練局](../Page/香港職業訓練局.md "wikilink")，1999年該校與另外六所工業學院及兩所科技學院合併成為[香港專業教育學院](../Page/香港專業教育學院.md "wikilink")，並改為現稱。

## 校舍歷史

摩理臣山校舍分為兩座，低座為正門[愛群道的校舍](../Page/愛群道.md "wikilink")，高座則為[職業訓練局總部大樓下](../Page/職業訓練局_\(香港\).md "wikilink")（一至六樓）的校舍，從前是人稱「紅磚屋」的建築物，於1937年落成，曾是[香港理工學院](../Page/香港理工學院.md "wikilink")、[鄧肇堅維多利亞官立中學及](../Page/鄧肇堅維多利亞官立中學.md "wikilink")[金文泰中學的所在地](../Page/金文泰中學.md "wikilink")。[紅磚屋原址在](../Page/紅磚屋.md "wikilink")1987年拆卸，於1992年重建完成，成為現今的兩幢大樓。本校工場亦曾經用作[香港工商師範學院的工科師範課程校址之一](../Page/香港工商師範學院.md "wikilink")，直至併入[香港教育學院後於](../Page/香港教育學院.md "wikilink")1997年遷入大埔為止。

## 設施

摩利臣山分校的圖書館設在新翼5樓及6樓（入口設於6樓），內裡設施包括電腦室、學生事務處以及英語自學中心。

## 歷任院長

  - [潘輝達](../Page/潘輝達.md "wikilink")(Michael J. Pomfret)（1999年9月－2001年）
  - [勞虔基](../Page/勞虔基.md "wikilink")（2001年－2002年1月）
  - [梁美智](../Page/梁美智.md "wikilink")（2002年1月－2006年2月）
  - [梁任城](../Page/梁任城.md "wikilink")（2006年2月－2007年12月）
  - [鄧勝森](../Page/鄧勝森.md "wikilink")（2008年1月－2009年8月）
  - [郭啟興](../Page/郭啟興.md "wikilink")（2009年9月－2014年8月）
  - [李志華](../Page/李志華.md "wikilink")（2014年9月－）

[File:HK_IVE_MorrisonHill_Entrance.JPG|正門](File:HK_IVE_MorrisonHill_Entrance.JPG%7C正門)
<File:HK> WC IVE facilities.jpg|大堂

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[灣仔站](../Page/灣仔站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 外部連結

  - [摩理臣山分校官方網頁](http://www.vtc.edu.hk/ti/mhti/homepage/chinese/index.htm)
  - [摩理臣山分校位置圖](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=836364&cy=815299&zm=2&mx=836364&my=815299&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)

[摩理臣山](../Category/香港專業教育學院.md "wikilink")
[Category:摩理臣山](../Category/摩理臣山.md "wikilink")
[Category:1969年創建的教育機構](../Category/1969年創建的教育機構.md "wikilink")