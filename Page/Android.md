****（讀音：英：，美：），常見的非官方中文名称为**安卓**，是一個基於[Linux核心的](../Page/Linux核心.md "wikilink")[開放原始碼](../Page/開放原始碼.md "wikilink")[行動作業系統](../Page/行動作業系統.md "wikilink")，由成立的（，開放手機聯盟）持續領導與開發，主要設計用於觸控螢幕行動裝置如[智慧型手機和](../Page/智慧型手機.md "wikilink")[平板電腦與其他可攜式裝置](../Page/平板電腦.md "wikilink")。

Android Inc.於2003年10月由[Andy
Rubin](../Page/Andy_Rubin.md "wikilink")、Rich Miner、Nick Sears和Chris
White
在[加州](../Page/加州.md "wikilink")[帕羅奧圖創建](../Page/帕羅奧圖.md "wikilink")。Android最初由[安迪·鲁宾等人開發製作](../Page/安迪·鲁宾.md "wikilink")\[1\]，最初開發這個系統的早期方向是創建一個[數位相機的先進作業系統](../Page/數位相機.md "wikilink")，但是後來發現市場需求不夠大，加上智慧型手機市場快速成長，於是Android成為一款面向智慧型手機的作業系統。於2005年7月11日Android
Inc.被[美國科技企業Google收購](../Page/美國.md "wikilink")\[2\]
\[3\]。2007年11月，Google與84家硬體製造商、軟體開發商及電信營運商成立[開放手機聯盟來共同研發改良Android](../Page/開放手機聯盟.md "wikilink")，隨後，Google以[Apache免費開放原始碼許可證的授權方式](../Page/Apache許可證.md "wikilink")，發佈了Android的原始碼\[4\]，開放原始碼加速了Android普及，讓生產商推出搭載Android的智慧型手機\[5\]\[6\]\[7\]\[8\]，Android後來更逐漸拓展到平板電腦及其他領域上\[9\]。

2010年末數據顯示，僅正式推出兩年的作業系統在市場佔有率上已經超越稱霸逾十年的[諾基亞](../Page/諾基亞.md "wikilink")系統\[10\]\[11\]，成為全球第一大智慧型手機作業系統\[12\]。

在2014年[Google
I/O開發者大會上Google宣布過去](../Page/Google_I/O.md "wikilink")30天裡有10億台活跃的安卓设备，相較於2013年6月則是5.38億\[13\]。

2017年3月，Android全球網路流量和設備超越[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")，正式成為全球第一大作業系統\[14\]。

2018年8月6日，[Android 9发行](../Page/Android_9.md "wikilink")。

## 歷史

### 成立

[HTC_HT722G700375_20080211.jpg](https://zh.wikipedia.org/wiki/File:HTC_HT722G700375_20080211.jpg "fig:HTC_HT722G700375_20080211.jpg")

2003年10月，有「Android之父」之稱的[安迪·鲁宾](../Page/安迪·鲁宾.md "wikilink")（Andy
Rubin）\[15\]、利奇·米纳尔（Rich Miner）\[16\]、尼克·席爾斯（Nick
Sears）\[17\]、[克里斯·怀特](../Page/克里斯·怀特.md "wikilink")（Chris
White）\[18\]在[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[帕羅奧圖共同成立了Android科技公司](../Page/帕羅奧圖_\(加利福尼亞州\).md "wikilink")（Android
Inc.），鲁宾把Android項目描述為「有極大的潛能以開發更智能的行動設備，以更了解其用家的位置及偏好\[19\]。」該公司早期的方向是為[數碼相機開發先進的作業系統](../Page/數碼相機.md "wikilink")，此亦是2004年4月該公司向投資者介紹的基礎\[20\]。儘管Android科技公司的創始人和員工過去都具有各自的科技成就，但是Android科技公司的經營只顯露出它在智慧型手機軟體的方面，該公司隨後認為相機市場不足以實現其目標，並且在5個月之後努力把Android轉移到一款可跟[Symbian及](../Page/Symbian.md "wikilink")[微軟](../Page/微軟.md "wikilink")[Windows
Mobile互相媲美的手機作業系統](../Page/Windows_Mobile.md "wikilink")\[21\]\[22\]。

在Android
Inc.成立初期，魯賓在吸引投資者方面遇到困難，他也為公司花光所有錢，使公司面臨著被驅逐的景況。就在此時，魯賓的一位密友，史蒂夫·帕爾曼（Steve
Perlman）給他一個裡面裝有1萬[美元現金的信封](../Page/美元.md "wikilink")，此後不久，此筆未披露的金額被視為種子資金。然而帕爾曼拒絕魯賓所給他的股份，因他並非是以[投資的方式給魯賓這筆錢](../Page/投資.md "wikilink")，他表示：「我這樣做是因為我相信這件事，並且我想幫安迪\[23\]\[24\]\[25\]。」

### Android命名由來

Android一詞最早出現於法國作家維利耶·德·利爾－阿達姆·利爾亞當（）在1886年發表的科幻小說《未來夏娃（）》中。他把外表像人的機器人取名為Android。

### Android標誌

Android是一個全身綠色的機器人，半圓形的頭部，有兩支天線和空白的點作為眼睛。它的頭部與身體之間有空白的區域，相似於一枚有平底的雞蛋，兩側各有兩個圓角矩形，作為它的雙臂。Android的標誌是由[Ascender公司設計](../Page/Ascender公司.md "wikilink")，顏色採用了PMS
376C和RGB中十六進制的\#A4C639來繪製，這是Android作業系統的品牌象徵\[26\]。當中的文字使用了Ascender公司專門製作的稱之為“Google
[Droid](../Page/Droid.md "wikilink")”的字型\[27\]。有時候，它會以純文字的標誌展示\[28\]。

### Google的收購及發展

2005年7月11日，[Google以高於](../Page/Google.md "wikilink")$5,000萬美元的價錢收購了Android科技公司\[29\]，使其成為Google旗下的一部分。Android的關鍵人物包括安迪·鲁賓、利奇·米納爾和克里斯·懷特，以及所有Android科技公司的員工都一併加入Google，作為收購的一部分。當時並沒有人知道Google為何會作出這項收購，引來許多的猜測，後來證實Google正是借助該次收購正式進入行動領域。根据前Google
CEO[施密特的说法](../Page/埃里克·施密特.md "wikilink")，收购Android目的是抗衡微软，试图阻止微软在移动市场复制桌面市场的成功\[30\]。在Google，由鲁賓領導的團隊開發了一個基於[Linux核心驅動的行動作業系統的平台](../Page/Linux核心.md "wikilink")，該開發項目便是Android作業系統。Google向[手機製造商及手機網絡供應商推出該平台](../Page/代工生產.md "wikilink")，承諾提供一個靈活可靠並可升級的系統\[31\]，為Android提供一個廣闊的市場。Google組織了一系列硬件及軟件的合作夥伴，並向運營商示意指公司開放到各種程度的合作\[32\]。

2006年12月，《[華爾街日報](../Page/華爾街日報.md "wikilink")》和[英國廣播公司](../Page/英國廣播公司.md "wikilink")（BBC）報導了Google有機會進入流動領域的傳聞\[33\]，Google有可能推廣行動領域下的各種Google服務，並且將推出一款名為Google電話的[自有品牌智慧型手機](../Page/自有品牌.md "wikilink")。另有傳聞指出，按照Google的作風，將有可能呈現出新的智慧型手機製造商和運營商模式。傳聞指其早期[原型跟](../Page/原型.md "wikilink")[BlackBerry手機非常相似](../Page/BlackBerry.md "wikilink")，沒有觸控螢幕和物理[QWERTY鍵盤](../Page/QWERTY鍵盤.md "wikilink")，但这其实也是针对微软而做的，因为当时的[Windows
Mobile手机有很多都是这样的设计](../Page/Windows_Mobile.md "wikilink")\[34\]。然而2007年[蘋果公司推出](../Page/蘋果公司.md "wikilink")[iPhone](../Page/iPhone.md "wikilink")，这令Android创始人[安迪·鲁宾对原型机大为不满](../Page/安迪·鲁宾.md "wikilink")\[35\]，意味著Android系統的設計「不得不回到繪圖板」\[36\]\[37\]。Google後來修改了Android系統的規範文檔，指出「支援觸控螢幕」，雖然「產品的設計是以離散物理按鈕作為假設，因此觸控螢幕不能完全取代物理按鈕」\[38\]。

2007年9月，Google提交了多項行動領域的[專利申請](../Page/專利.md "wikilink")。但並沒有人知道，Google將會推出的是一款名為Android的智慧型手機作業系統。更多的猜測是，Google會推出一款像[iPhone一樣的自有品牌智慧型手機系列](../Page/iPhone.md "wikilink")。\[39\]\[40\]\[41\]

### 成立開放手持裝置聯盟

[T-Mobile_G1_launch_event_2.jpg](https://zh.wikipedia.org/wiki/File:T-Mobile_G1_launch_event_2.jpg "fig:T-Mobile_G1_launch_event_2.jpg")。\]\]

該年內，開放手機聯盟正面對著另外兩個其他的[開源碼的競爭對手](../Page/開放源碼.md "wikilink")，包括[Symbian基金會和](../Page/Symbian基金會.md "wikilink")[LiMo基金會](../Page/LiMo基金會.md "wikilink")，LiMo基金會開發了一個基於[Linux的行動作業系統](../Page/Linux.md "wikilink")，就如Google。

2007年9月，數碼雜誌《Information
Week》報導了一項Evalueserve的研究報告，指Google已在行動電話領域上提交了多份[專利申請](../Page/專利.md "wikilink")\[42\]\[43\]。

2007年11月5日，在Google的領導下，成立了[開放手機聯盟](../Page/開放手機聯盟.md "wikilink")（Open
Handset
Alliance），那是包括Google在內的科技公司聯盟，其他成員包括[HTC](../Page/HTC.md "wikilink")、[摩托羅拉](../Page/摩托羅拉.md "wikilink")、[Samsung等設備製造商](../Page/Samsung.md "wikilink")，無線運營商則包括[Sprint及](../Page/Sprint.md "wikilink")[T-Mobile](../Page/T-Mobile.md "wikilink")，晶片製造商[高通及](../Page/高通.md "wikilink")[德州儀器](../Page/德州儀器.md "wikilink")，目標是為行動裝置開發「首個真正開放和全面的行動設備平台」\[44\]\[45\]\[46\]。

隨後，其他廠商加入，包括[Broadcom](../Page/Broadcom.md "wikilink")、[Intel](../Page/Intel.md "wikilink")、[LG](../Page/LG.md "wikilink")、[Marvell等](../Page/Marvell_Technology_Group.md "wikilink")。聯盟開放手持裝置聯盟的建立目的是為了創建一個更加開放自由的行動電話環境。而在開放手持裝置聯盟創建的同一日，聯盟對外展示了他們的第一個產品：一部搭載了以Linux
2.6為核心基礎的Android作業系統的智慧型手機。

2008年12月9日，新一批成員加入開放手持裝置聯盟，包括[ARM](../Page/ARM.md "wikilink")、[華為](../Page/華為.md "wikilink")、[索尼等公司](../Page/索尼移动.md "wikilink")\[47\]\[48\]。

自2008年以來，Android已有許多更新以逐步改進其作業系統，並增加了新功能及修復了以前版本的錯誤。每個主要版本的名稱都以甜品或含糖的小食並按字母順序來命名，最初的幾個Android版本按此順序稱為「紙杯蛋糕（Cupcake）」、「甜甜圈（Donut）」、「閃電泡芙（Eclair）」和「優格霜淇淋（Froyo）」。

為了跟[iPhone
3G能互相媲美](../Page/iPhone_3G.md "wikilink")，[諾基亞和黑莓手機於](../Page/諾基亞.md "wikilink")2008年均宣布有關觸控螢幕的智能手機的資訊，Android的焦點最終也轉向觸控螢幕。第一款運行Android系統的商用智能手機是[HTC
Dream](../Page/HTC_Dream.md "wikilink")，亦名為T-Mobile
G1，該智能手機於2008年9月23日發布\[49\]\[50\]。

同時，一個負責持續發展Android作業系統的開源代码專案成立了[AOSP](../Page/AOSP.md "wikilink")（Android
Open Source
Project）。\[51\]除了開放手持裝置聯盟之外，Android還擁有全球各地開發人員組成的開源社群來專門負責開發Android應用程式和第三方Android作業系統來延長和擴展Android的功能和性能。\[52\]\[53\]

隨著2013年Android發布新版本[奇巧巧克力](../Page/Android_KitKat.md "wikilink")，Google解釋「由於這些設備使我們的生活如此甜蜜，每個Android版本都以甜品來命名」，雖然Google的發言人在接受[CNN訪談時指](../Page/CNN.md "wikilink")：「這有點像內部團隊的事情，我們更願意做多一點—我該怎樣說呢—我會說在這個問題上有點難以捉摸的\[54\]。」

2010年，Google推出了[Nexus系列的裝置](../Page/Google_Nexus.md "wikilink")，他們與不同的合作設備製造商一起生產新設備並推出新的Android版本。該系列被形容為「透過引入新軟件迭代和硬件標準使其在Android的歷史中發揮了關鍵作用，並成為不會因"歷代的更新"導致軟件膨脹而聞名\[55\]」。在2013年5月的[Google開發者大會上](../Page/Google_I/O.md "wikilink")，他們宣布了[Samsung
Galaxy
S4的特別版](../Page/Samsung_Galaxy_S4.md "wikilink")，特别版在于它搭载了原生安卓，並承諾能快速接收新的系統更新\[56\]。裝置將成為「Google
Play Edition」的開始，其他設備也會隨之而來，包括[HTC One
(M7)以及](../Page/HTC_One_\(M7\).md "wikilink")[Moto
G](../Page/Moto_G.md "wikilink")\[57\]。2015年，Google Play Edition版步入了历史。

從2008至2013年，[雨果·巴拉擔任Android產品發言人](../Page/雨果·巴拉.md "wikilink")，參與Google年度開發人員會議的[Google開發者大會](../Page/Google_I/O.md "wikilink")。他於2013年8月離開Google，並隨即加入中國手機製造商[小米](../Page/小米集團.md "wikilink")\[58\]\[59\]。在他離任不足六個月前，Google時任[CEO](../Page/行政總裁.md "wikilink")[賴利·佩吉在一篇博客文章中宣布](../Page/賴利·佩吉.md "wikilink")，安迪·鲁宾從Android部門轉移到Google的新項目，而[桑德爾·皮蔡將會成為Android的新主管](../Page/桑德爾·皮蔡.md "wikilink")\[60\]\[61\]。皮蔡本人最終轉換了職位，隨著Google重組大型聯合的[Alphabet集團](../Page/Alphabet.md "wikilink")\[62\]\[63\]，他在2015年8月成為了Google的新任CEO，讓成為Android的新任負責人\[64\]\[65\]。

2014年6月，Google發布了[Android
One](../Page/Android_One.md "wikilink")，那是一組「硬件參考模型」，這將容許（設備製造商）以低成本輕鬆打造高品質手機，專為發展中國家的消費者而設\[66\]\[67\]\[68\]。同年9月，Google宣布推出首款在印度發布的Android
One手機\[69\]\[70\]。然而科技網站《》在2015年6月報導該項目「令人失望」，引用指「消費者和製造夥伴不情願」及「搜索公司不奏效因從未破解硬件」\[71\]。Google計劃於2015年8月重推Android
One\[72\]，一周之後，該公司宣布非洲成為該計劃的下一個地點\[73\]\[74\]。2017年1月，《資訊（The
Information）》報導稱Google正在將其低成本的Android One計劃擴展至美國，儘管《邊緣（The
Verge）》指該公司可能不會生產實體的設備\[75\]\[76\]。

2016年10月，Google發布智能手機[Pixel](../Page/Pixel_\(智能手機\).md "wikilink")，作為Google推出的第一款手機\[77\]\[78\]，並且在更廣泛推出之前，專門展示某些軟件功能，例如[Google助理](../Page/Google助理.md "wikilink")\[79\]\[80\]。Pixel手機取代了Nexus系列。

## 系統特點

### 界面

Android的預設用戶界面主要基於直接操作，透過觸控鬆散地對應現實動作以作出輸入，例如滑動、點擊、捏動和反向擠壓，隨著[虛擬鍵盤](../Page/虛擬鍵盤.md "wikilink")，以操控屏幕上的物件\[81\]。[遊戲控制器及物理鍵盤都能透過](../Page/遊戲控制器.md "wikilink")[藍牙或](../Page/藍牙.md "wikilink")[USB得到支援](../Page/USB.md "wikilink")\[82\]\[83\]。在回應用家的輸入方面，設計旨在提供立即流暢的觸摸界面，經常使用設備的振動功能向用戶提供觸覺反饋。內部硬件，例如是[加速規](../Page/加速規.md "wikilink")、[陀螺儀](../Page/陀螺儀.md "wikilink")、[距離傳感器都能被某些應用程式來回應用戶的操作](../Page/接近傳感器.md "wikilink")，例如根據裝置的方向來把屏幕由縱向調整為橫向\[84\]，或容許用戶透過旋轉裝置，在[賽車遊戲中駕駛車輛](../Page/競速遊戲.md "wikilink")\[85\]。

當Android裝置啟動就會進入主畫面，那是裝置的主要導航及信息「樞紐」，類似於個人電腦的[桌面](../Page/桌面比擬.md "wikilink")。Android的主畫面通常由應用程序圖標及小工具（widget）組成，應用程序圖標啟動相關的應用程序，而小工具則會實時顯示，並會自動更新內容，例如[天氣預報](../Page/天氣預報.md "wikilink")、用戶的電子郵件，或是直接在主畫面上看新聞摘要\[86\]。主畫面可以由若干頁面組成，用戶可以在這些頁面之間來回滑動\[87\]。[Google
Play上提供的第三方應用程式](../Page/Google_Play.md "wikilink")，而其他的應用程式商店可以廣泛使用重新定義的
主畫面[主題包](../Page/主題包.md "wikilink")\[88\]，甚至模仿其他作業系統的外觀，例如[Windows
Phone](../Page/Windows_Phone.md "wikilink")\[89\]。大多數製造商會定制他們Android設備的外觀和功能，以從競爭對手中脫穎而出\[90\]。

屏幕頂部是狀態欄，顯示有關裝置及其連接的資訊。用戶可把狀態欄「拉下」，以顯示應用程式顯示的重要信息，或屏幕更新的通知\[91\]。通知是「當用戶的應用程序在非使用時所提供簡短、及時和相關的訊息\[92\]。」當點擊了，用戶會被引導到該app內與該通知相關的屏幕。從[Android
Jelly
Bean開始](../Page/Android_Jelly_Bean.md "wikilink")，可擴展通知「允許用戶透過點擊通知上的圖標，以便它擴展和顯示從通知中得知的更多信息以及有可能的應用操作\[93\]。」

「所有應用程序」的屏幕將會所有已安裝的應用程式，用戶可以將應用程序從列表拖曳到主畫面上。「最近」的屏幕讓用戶在最近使用過的應用程序之間進行切換\[94\]。

[The-Android-software-stack.png](https://zh.wikipedia.org/wiki/File:The-Android-software-stack.png "fig:The-Android-software-stack.png")
[Android_Storage.png](https://zh.wikipedia.org/wiki/File:Android_Storage.png "fig:Android_Storage.png")
[Android_notification_area.png](https://zh.wikipedia.org/wiki/File:Android_notification_area.png "fig:Android_notification_area.png")

### 應用程式

應用程式（簡稱[apps](../Page/流動應用程式.md "wikilink")）是擴展裝置功能的軟件，都是利用[Android軟件開發工具包](../Page/軟體開發套件.md "wikilink")（SDK）編寫的\[95\]，通常是[Java編程語言](../Page/Java.md "wikilink")\[96\]。Java可以與[C語言或](../Page/C語言.md "wikilink")[C++](../Page/C++.md "wikilink")\[97\]結合使用，並且可以選擇非默認的[執行時函式庫共用](../Page/運行時庫.md "wikilink")，以允許獲得更好的C++支援\[98\]。雖然Google的[Go是有一組有限的](../Page/Go.md "wikilink")[應用程式介面](../Page/應用程式介面.md "wikilink")（API），然而它也支持編程語言\[99\]。2017年5月，Google宣布支援[Kotlin程式語言](../Page/Kotlin.md "wikilink")\[100\]\[101\]。

SDK包含一套全面的開發工具\[102\]，包括[除錯器](../Page/除錯器.md "wikilink")、[函數庫](../Page/函數庫.md "wikilink")、基於[虛擬機器鏡像的](../Page/QEMU.md "wikilink")[仿真器](../Page/仿真器.md "wikilink")、文檔，示例代碼和教程。最初，Google支援的[整合開發環境](../Page/整合開發環境.md "wikilink")（IDE）是使用Android開發工具（ADT）插件的[Eclipse](../Page/Eclipse.md "wikilink")；在2014年12月，Google基於[IntelliJ
IDEA而發布](../Page/IntelliJ_IDEA.md "wikilink")[Android
Studio](../Page/Android_Studio.md "wikilink")，作為Android應用程序開發的主要IDE。其他可用的開發工具，包括（NDK），或是用於C或C++中的擴展，[MIT應用開發者](../Page/MIT應用開發者.md "wikilink")，那是讓新手程序員的視覺環境，以及各種。2014年1月，Google推出了一個基於的框架，把[Google
Chrome](../Page/Google_Chrome.md "wikilink")、[HTML5及](../Page/HTML5.md "wikilink")[網絡應用程式移植到Android](../Page/網絡應用程式.md "wikilink")，包裝於本機應用程序的外殼中\[103\]。

Android擁有越來越多第三方應用程序的選擇，用戶可以透過下載和安裝應用程序的[APK](../Page/APK.md "wikilink")（Android應用程序包），或利用應用程式商店來下載，允許用戶在那裡進行[安裝、更新和移除](../Page/軟件套件管理系統.md "wikilink")。[Google
Play是安裝在Android裝置上的主要應用程式商店](../Page/Google_Play.md "wikilink")，那些程式都符合Google的兼容性要求，並獲得Google行動服務軟件許可\[104\]\[105\]。Google
Play商店允許用戶瀏覽、下載及更新Google和第三方開發者發布的應用程序；，Google
Play商店中有超過1,000,000個適用於Android的應用程序\[106\]。，已有500億個應用程序獲得安裝\[107\]\[108\]。一些電訊商為Google
Play應用程式提供直接結算，把應用程式的成本添加到用戶的月結單中\[109\]。，每月使用Gmail、Android、Chrome、Google
Play和Google地圖的活躍用戶超過10億。

由於Android系統的開放性質，使它吸引許多第三方應用程式市場的競爭，及由於違反了Google
Play商店的政策或是其他原因而不允許發布的應用程式替代品，第三方應用程式商店的例子包括是[亞馬遜應用商店](../Page/亞馬遜應用商店.md "wikilink")、及SlideMe。另一個替代市場[F-Droid](../Page/F-Droid.md "wikilink")，旨在提供僅在和[開源許可下分發的應用程式](../Page/自由及開放源碼軟件.md "wikilink")\[110\]\[111\]\[112\]\[113\]。

### 内存管理

於Android設備通常採用電池供電，因此Android旨在管理流程以將耗電降至最低。當應用程式未使用時，系統會暫停其操作，雖然可以在關閉期間立即使用，但它並不會使用電池電源或CPU資源\[114\]\[115\]。當内存不足時，系統將會自動隱藏地開始關閉長時間內處於非活躍狀態下的進程\[116\]\[117\]。Lifehacker在2011年的報告指，第三方任務殺手應用程序是弊大於利\[118\]。

## 硬件支持

Android的主要硬件平台為[ARM架構](../Page/ARM架構.md "wikilink")（包括[ARMv7及ARMv8-A](../Page/ARM架構#ARM內核的授權與應用.md "wikilink")），在更高階版本的Android中也正式支援[x86及](../Page/x86.md "wikilink")[x86-64的架構](../Page/x86-64.md "wikilink")\[119\]\[120\]\[121\]\[122\]。非官方的[Android-x86的項目在得到官方正式支援之前為x](../Page/Android-x86.md "wikilink")86架構提供支援\[123\]\[124\]。ARMv5TE和MIPS32/64架構於過去得到支援，但在後來的Android版本中遭到刪除\[125\]。自2012年以來，隨著擁有[英特爾處理器的Android裝置開始出現](../Page/英特爾.md "wikilink")，包括手機\[126\]及平板電腦。在得到對64位元平台的支援同時，Android最初是在64位元x86上運行，後來就在ARM64。自從Android
5.0 “Lollipop”以來，除32位元變體（32-bit variants）外，還支持所有64位元變體（64-bit
variants）的平台。

運作Android
7.1的裝置對[RAM最低要求的範圍從最佳硬件的](../Page/RAM.md "wikilink")2GB降到最常見屏幕的1GB；最低規格的32位元智能手機最小需要512MB。Android
4.4的建議是至少需要512MB的RAM，而對於「低RAM」設備，340MB是必需的最低容量，不包括專用內存的各種硬件組件，例如基帶處理器\[127\]。Android
4.4 需要[32位元的ARMv](../Page/32位元.md "wikilink")7,
[MIPS或](../Page/MIPS架構.md "wikilink")[x86的架構處理器](../Page/x86.md "wikilink")（後兩者是非官方連接埠）\[128\]\[129\]，以及與[OpenGL
ES](../Page/OpenGL_ES.md "wikilink")
2.0兼容的[圖形處理器](../Page/圖形處理器.md "wikilink")（GPU）一同使用\[130\]。Android支援OpenGL
ES 1.1、2.0、3.0、3.1及截至現時為止最新的主要版本3.2及自Android 7.0的[Vulkan
(API)](../Page/Vulkan_\(API\).md "wikilink")（和版本1.1適用於某些裝置\[131\]）。某些應用程式可能會明確要求使用某個版本的OpenGL
ES，並且運行此類應用程序需要合適的GPU硬件\[132\]。

Android裝置包括了許多可選的硬件部件，包括靜止或視頻錄像機、[GPS](../Page/GPS.md "wikilink")、、專用遊戲控制器、[加速規](../Page/加速規.md "wikilink")、[陀螺儀](../Page/陀螺儀.md "wikilink")、氣壓計、[磁強計](../Page/磁強計.md "wikilink")、[接近傳感器](../Page/接近傳感器.md "wikilink")、[壓力傳感器](../Page/壓力傳感器.md "wikilink")、[溫度計和](../Page/溫度計.md "wikilink")[觸控螢幕](../Page/觸控螢幕.md "wikilink")。某些硬件組件不是必需的，但在某些類別的設備（如智能手機）中成為了標準組件，若存在其他要求，則適用。最初需要一些其他硬件，但那些要求都經已放寬或完全取消。例如，由於Android最初是作為手機作業系統而開發的，因此必須麥克風等那些硬件，而隨著時間的推移，這些手機功能變成可選的項目\[133\]。Android曾經需要一台[自動對焦的相機](../Page/自動對焦.md "wikilink")，這款相機已經放寬到成為[固定焦距相機](../Page/定焦鏡頭.md "wikilink")\[134\]，若是現存的話，當Android開始在[數位視訊轉換盒](../Page/數位視訊轉換盒.md "wikilink")（俗稱機頂盒）上使用時，相機就能完全被丟棄了。

Android除了在智能手機和平板電腦上運作外，還可以在一些附有鍵盤和滑鼠的普通PC硬件上運作\[135\]\[136\]\[137\]\[138\]。除了可在商用硬件上使用外，Android也有類似於PC硬件友好的版本，可以從Android-x86的項目中免費提供，包括個人訂制的Android
4.4版本\[139\]。使用作為一部分的[仿真器](../Page/仿真器.md "wikilink")，或第三方的仿真器，Android也可以在x86架構上非本機地執行\[140\]\[141\]。中國的一些公司基於Android，正在構建PC與行動裝置的作業系統，是「Google
Android與Microsoft Windows之間的直接競爭\[142\]。」中國工程學院指出，隨著中國禁止在政府電腦上使用Windows
8，數十家公司正在定制Android系統\[143\]\[144\]\[145\]。

## 發展

[Android_open_source_project.png](https://zh.wikipedia.org/wiki/File:Android_open_source_project.png "fig:Android_open_source_project.png")
Android是由[Google開發的](../Page/Google.md "wikilink")，直至準備發布前最新的修改及更新，在這一點上[原始碼可供Android開源項目](../Page/原始碼.md "wikilink")（AOSP）使用\[146\]，這是一個由Google領導的[開源計劃](../Page/開放源碼.md "wikilink")\[147\]。AOSP代碼可於無需的修改選定設備中找到，主要是[Google
Nexus及](../Page/Google_Nexus.md "wikilink")[Google
Pixel系列的裝置](../Page/Google_Pixel.md "wikilink")\[148\]。反過來說，那些開源碼是由[代工生產定制和調整以在他們的硬件上執行](../Page/代工生產.md "wikilink")\[149\]\[150\]。此外，Android的源代碼並不包含某些硬件組件所需專有的[驅動程式](../Page/驅動程式.md "wikilink")\[151\]，因此大多數的Android裝置（包括Google自己的裝置）最終都會附帶著[自由及開放源碼軟件及](../Page/自由及開放源碼軟件.md "wikilink")[專有軟件的組合](../Page/專有軟件.md "wikilink")，伴隨著用於進入Google服務所需的軟件，都會落入後者的一類。

### 更新時間表

Google每年都會宣布對Android進行重大的增量升級\[152\]。程式更新可以透過[空中編程以無線傳輸於裝置上安裝](../Page/空中編程.md "wikilink")\[153\]。最新的主要版本是在2018年8月發布的[Android
9 Pie](../Page/Android_9.md "wikilink")。

Android與其主要的競爭對手[蘋果公司的](../Page/蘋果公司.md "wikilink")[iOS相比](../Page/iOS.md "wikilink")，Android的更新通常會明顯的延遲以讓各種設備安裝。除了Google的Nexus和Pixel品牌的裝置外，更新通常會在新版本發布的幾個月後到達，或者根本沒有\[154\]。這部分是由於Android裝置中[硬件的廣泛變化](../Page/硬件.md "wikilink")\[155\]，每次升級必須專門定制，那是個耗費時間和資源的過程\[156\]。製造商經常會優先考慮他們最新的設備並遺下舊設備\[157\]。在收到製造商的更新後，無線運營商可以讓裝置額外延遲推出，以便於升級發送給用戶之前，商戶能透過進一步根據自己品牌的需求而定制及在他們的網絡上進行廣泛的測試\[158\]\[159\]。由於一個製造合作夥伴沒有為[驅動程式提供必要的更新](../Page/驅動程式.md "wikilink")，故此會有一些情況下無法進行升級\[160\]。

隨著新版本的作業系統，Android設備中硬件的廣泛變化會導致軟件升級有嚴重的延誤，而[修補程式通常需時幾個月才能到達消費者手中](../Page/修補程式.md "wikilink")，有時甚至根本沒有。製造商和運營商缺乏售後支援服務受到消費者群及科技媒體的廣泛批評\[161\]\[162\]\[163\]。一些評論員指出了一點，指這個行業有一個金融刺激而不會為他們的裝置升級，由於缺乏為現有裝置更新，故推動購買新產品\[164\]，這種態度被形容為「侮辱」\[165\]。《[衛報](../Page/衛報.md "wikilink")》抱怨指更新的分發方法很複雜只是因為製造商和運營商都是這樣設計\[166\]。2011年，Google與眾多業內人士合作宣布推出「Android更新聯盟」，承諾在每台裝置發布後18個月內提供及時更新，然而，自宣布以來，關於那個聯盟並沒有另外一個官方的說法\[167\]\[168\]。

2012年，Google開始將作業系統的某些方面（特別是其核心應用程式方面）脫鉤，這樣他們就可以通過[Google
Play商店獨立地為作業系統作更新](../Page/Google_Play.md "wikilink")。其中一個組件—[Google
Play服務](../Page/Google_Play服務.md "wikilink")，是個封閉源系統級的程序，可為Google服務提供[應用程式介面](../Page/應用程式介面.md "wikilink")，為執行Android
Froyo或更高版本的裝置上自動安裝更新。通過這些改，Google可以透過Play服務添加新的系統功能及更新應用程式而毋需將升級分發到作業系統本身\[169\]，因此，[Android
Jelly
Bean包含相對較少面向用戶的變化](../Page/Android_Jelly_Bean.md "wikilink")，對平台改進及細微變化更為側重\[170\]。

2016年5月，[彭博報導稱Google正在努力讓Android保持最新狀態](../Page/彭博商業周刊.md "wikilink")，包括加快安全更新率、推出技術的解決方案、減少對手機測試的要求，並對電話製造商進行排名，試圖利用「羞辱」他們來讓行為變得更好。正如《彭博》所說：「隨著智能手機變得越來越強大，複雜和可攻擊，讓最新的軟件能與硬件密切合作變得越來越重要。」Android的領導人承認指「這不是一個理想的情況」，他進一步評論指缺乏更新是「Android上安全性最薄弱的環節」。無線運營商在報告中被描述指那是「最具挑戰性的討論」，因為運營商的網絡測試審批時間較慢，儘管有一些運營商包括[威訊無線及](../Page/威訊無線.md "wikilink")[Sprint](../Page/Sprint.md "wikilink")，已經縮短了他們各自的審批時間。[HTC的時任執行官賈森](../Page/HTC.md "wikilink")·麥肯齊（Jason
Mackenzie）於2015年稱每月的安全性更新是「不切實際」，而Google試圖說服運營商從完整的測試程序中排除安全補丁。為了進一步說服，Google分享了與其Android合作夥伴更新設備的頂級手機製造商的一份清單，並正在考慮將名單向公眾發布。手機製造商Nextbit的聯合創始人及前Android開發人員Mike
Chan說，「解決這個問題的最佳方法是對作業系統進行大規模的重組架構」或Google可以投資培訓製造商和運營商「成為優秀的Android人民\[171\]\[172\]\[173\]」。

2017年5月，隨著[Android
Oreo發布](../Page/Android_Oreo.md "wikilink")，谷歌推出了Project
Treble，旨在使製造商能夠更輕鬆，更快捷，及更低成本地將裝置更新到Android的最新版本。Project
Treble通過新的「供應商界面」將供應商實施與Android
OS框架分開（由矽製造商所寫供設備專用的較低級別軟件）。在Android
7.0及更早版本中，並不存在正式的供應商界面，因此設備製造商必須更新大部分Android代碼，以將較新版本的作業系統移至設備中。借助Project
Treble，新的穩定供應商界面可以進入Android特定部分的硬件，使設備製造商能夠簡單地通過更新Android作業系統框架來為裝置提供新的Android版本，而「毋須矽製造商的任何額外工作\[174\]。」

2017年9月，Google的Project
Treble團隊透露，作為改善Android設備安全生命週期努力的一部分，Google已經設法讓Linux基金會同意延長Linux長期支援（LTS）的內核分支的生命週期從2年開始，對於未來版本的LTS內核，歷史上已經持續了6年，並從Linux內核4.4開始\[175\]。

### Linux核心

Android的[內核是根據](../Page/內核.md "wikilink")[Linux核心的長期支援的分支](../Page/Linux核心.md "wikilink")，具有典型的Linux排程和功能\[176\]。截至2018年，Android的目標是Linux內核的4.4、4.9或是4.14版本\[177\]。實際的內核取決於單個設備\[178\]。

Linux內核的Android的變種具進一步改進了由Google實施在典型的Linux內核開發週期之外實現的架構變化，比如包含類似組件的裝置樹（device
trees）、ashmem、ION，以及不同的（OOM）的處理\[179\]\[180\]。除此之外，Google為了能讓Linux在行動裝置上良好的運行，對其進行了修改和擴充。Android去除了Linux中的本地[X
Window
System](../Page/X_Window系統.md "wikilink")，也不支持標準的[GNU庫](../Page/Gnulib.md "wikilink")，這使得Linux平台上的應用程式移植到Android平台上變得困難\[181\]。Google也有某些功能貢獻回到Linux內核，特別是一種稱為「喚醒鎖（wakelocks）」的電源管理功能\[182\]，它最初被主線內核開發人員拒絕，部分原因是因為他們認為Google並沒有表現出維護他們自己代碼的任何意圖\[183\]\[184\]。2008年，Patrick
Brady於[Google I/O上演說題為](../Page/Google_I/O.md "wikilink")「Anatomy &
Physiology of an Android」，並提出Android
HAL的架構圖。[HAL以](../Page/硬體抽象層.md "wikilink")\*.so副檔名的形式存在，可以把Android框架跟Linux內核隔開，這種中介層的方式使得Android能在行動裝置上獲得更高的執行效率。這種獨特的系統結構受到Linux內核開發者[葛雷格·克羅哈曼及其他核心維護者稱賞](../Page/葛雷格·克羅哈曼.md "wikilink")。Google還在Android的核心中正式加入了自己開發製作的一個名為喚醒鎖（wakelocks）的功能，該功能用於管理流動裝置的電池性能，但是該功能並沒有被加入到Linux核心的主線開放和維護中，因為Linux核心維護者認為Google沒有向他們展示這個功能的意圖和代碼。Google於2010年4月宣布他們將會聘請兩名員工跟Linux內核社團合作\[185\]，但目前Linux內核對於穩定分支維護者的[葛雷格·克羅哈曼](../Page/葛雷格·克羅哈曼.md "wikilink")，他於2010年12月說他擔心Google不再試圖讓他們的代碼更改包含在主流Linux中\[186\]。Google的工程師帕特里克·布雷迪（Patrick
Brady）曾在該公司的[開發者大會中表示過](../Page/Google_I/O.md "wikilink")「Android不是Linux\[187\]」，而他在周刊《》補充指「讓我替你簡化一下，要是沒有Linux，這裡就沒有Android\[188\]」。《[Ars
Technica](../Page/Ars_Technica.md "wikilink")》寫道：「儘管Android是建立在Linux內核之上的，但該平台與傳統的Linux堆棧桌面幾乎沒有共同之處\[189\]。」

2010年2月3日，由於Google在Android核心開發方面和Linux社群方面開發的不同步，Linux內核開發者葛雷格·克羅哈曼把Android的驅動程序從Linux內核「[狀態樹](../Page/狀態樹.md "wikilink")」（staging
tree）上除去。\[190\]2010年4月，Google宣布將派遣2名開發人員加入Linux核心社群，以便重返Linux核心。2010年9月，Linux核心開發者Rafael
J. Wysocki添加了一個修復程序，使得Android的“wakelocks”可以輕鬆地與主線Linux核心合併。2011年，[Linus
Torvalds說](../Page/Linus_Torvalds.md "wikilink")：“Android的核心和Linux的核心將最終回歸到一起，但可能不會是4-5年。”在Linux
3.3中大部分代码的整合完成。\[191\]

2011年8月，Linux內核最早作者的[林納斯·托瓦茲說](../Page/林納斯·托瓦茲.md "wikilink")：「Android和Linux最終都會重新回到一個共同的內核，但它可能不會持續4-5年\[192\]」。2011年12月，葛雷格·克羅哈曼（Greg
Kroah-Hartman）宣布啟動Android的主流項目，其目的是要將一些Android[驅動程式](../Page/驅動程式.md "wikilink")、修補程式及功能重新放進Linux內核中，從Linux
3.3開始\[193\]。經過從前多次的嘗試合併後，Linux在3.5內核中，包括自動休眠和喚醒鎖功能功能。其界面是相同的，但是上游的Linux實現了容許兩種不同的中止模式：記憶體（Android使用的傳統中止模式）及磁碟（已知桌面上的冬眠）\[194\]。Google維護著一個公共代碼存儲庫，包含著他們對Android的[Rebasing到最新穩定的Linux版本的實驗工作](../Page/Rebasing.md "wikilink")\[195\]\[196\]。

Android裝置的[快閃記憶體被分成幾個分區](../Page/快閃記憶體.md "wikilink")，例如`/system`用於作業系統本身，而`/data`是用於用戶數據和應用程式的安裝\[197\]。跟Linux桌面發行版相比，Android設備的擁有人都沒有給予[超級用戶的進入作業系統的權限](../Page/超級用戶.md "wikilink")，以及例如`/
system`是[唯讀記憶體的敏感分區](../Page/唯讀記憶體.md "wikilink")。然而，[檔案系統階層標準是可以透過利用Android中的](../Page/文件系统层次结构标准.md "wikilink")[安全漏洞來獲取](../Page/计算机安全隐患.md "wikilink")，那是開源社區經常使用它來增強其設備的功能\[198\]，惡意的一方還可以透過安裝[電腦病毒及](../Page/電腦病毒.md "wikilink")[惡意軟件來惡意獲取系統資料](../Page/惡意軟件.md "wikilink")\[199\]。

根據[Linux基金會的說法](../Page/Linux基金會.md "wikilink")，Android是個[Linux發行版](../Page/Linux發行版.md "wikilink")\[200\]，該基金會由Google的開源部門主管\[201\]及幾位記者組成\[202\]\[203\]，其他人例如Google的工程師帕特里克·布拉迪（Patrick
Brady）等，布拉迪表示在傳統[類Unix系統的Linux發行意義上](../Page/類Unix系統.md "wikilink")，Android並不是Linux；Android不包括[GNU
C函數庫](../Page/GNU_C函數庫.md "wikilink")（它利用[Bionic作為替代C數據庫](../Page/Bionic_\(軟件\).md "wikilink")），以及Linux發行版中常見的一些其他組件\[204\]。

隨著2017年[Android
Oreo的推出](../Page/Android_Oreo.md "wikilink")，Google基於安全考慮，開始要求新附帶[系統單晶片的設備具有Linux內核版本](../Page/系統單晶片.md "wikilink")4.4或更高版本，現有的裝置升級到Android
Oreo，以及與舊的系統單晶片一起推出的新產品，均不受此規則所限\[205\]\[206\]。

### 軟件堆棧

[Android-System-Architecture.svg](https://zh.wikipedia.org/wiki/File:Android-System-Architecture.svg "fig:Android-System-Architecture.svg")
在Linux內核之上，有一些由[C所寫的](../Page/C語言.md "wikilink")[中介軟體](../Page/中介軟體.md "wikilink")、[函數庫和](../Page/函數庫.md "wikilink")[應用程式介面](../Page/應用程式介面.md "wikilink")，以及運行包含Java兼容庫的應用框架上應用程式。Linux內核的開發則繼續獨立於Android的其他源代碼項目。

截至5.0版本，Android利用[Dalvik虛擬機器作為](../Page/Dalvik虛擬機器.md "wikilink")[程式虛擬機器](../Page/虛擬機器#程式虛擬機器.md "wikilink")，它與（JIT）來執行Dalvik「DEX-代碼」（Dalvik的可執行程式），這通常是由[Java位元組碼一同翻譯而來](../Page/Java位元組碼.md "wikilink")。繼基於跟踪的JIT的原則，除了解讀大多數應用程序代碼外，Dalvik執行編譯及每當應用程序啟動時，選擇本機執行的頻繁執行代碼段（痕跡）\[207\]\[208\]\[209\]。Android
4.4引入[Android
Runtime](../Page/Android_Runtime.md "wikilink")（ART）作為新的運作環境，在安裝應用程式時，它會使用（AOT）來把應用程序字節碼完全編譯為[機器語言](../Page/機器語言.md "wikilink")\[210\]。在Android
4.4中，ART是一項實驗性功能，默認情況下不啟用；它成為Android
5.0的下一個主要版本中唯一的運作選項\[211\]。2015年12月，Google宣布Android的下一個版本將會切換到基於[OpenJDK項目的Java實行方式](../Page/OpenJDK.md "wikilink")\[212\]。

Android的[C標準函式庫及](../Page/C標準函式庫.md "wikilink")[Bionic都是由Google專門為Android而開發的軟件](../Page/Bionic_\(軟件\).md "wikilink")，作為[BSD標準C程式庫代碼的推導](../Page/BSD.md "wikilink")。Bionic本身已跟特定於Linux內核的幾個主要特點而設計。使用Bionic而不是[GNU
C函數庫](../Page/GNU_C函數庫.md "wikilink")（glibc）或[uClibc的主要好處是](../Page/uClibc.md "wikilink")：它運行時間的足跡較小，以及對低頻CPU進行優化。與此同時，Bionic根據BSD許可條款而獲得許可，當中Google找到更適合Android的整體許可模式\[213\]。

針對不同的許可模式，Google於2012年底將Android中的藍牙堆棧從GPL許可的轉移到Apache許可的BlueDroid\[214\]。

Android默認情況下並沒有本機[X
Window系統](../Page/X_Window系統.md "wikilink")，也不支援整套標準[GNU庫](../Page/GNU.md "wikilink")。這使現有Linux應用程式或程式庫都難以移植到Android\[215\]，直至Android的的r5版本完全以[C或](../Page/C語言.md "wikilink")[C++編寫的應用程序以獲得支援](../Page/C++.md "wikilink")\[216\]
由C所編寫的程式庫也可以透過注入一個小[墊片及使用](../Page/墊片_\(計算機\).md "wikilink")[Java本地介面](../Page/Java本地介面.md "wikilink")（JNI）以在應用程式中使用\[217\]。

自Android
Marshmallow發布以來，一系列指令實用程序的「」取代了之前Android版本中類似「工具箱」系列（*當Android於默認情況下不提供[命令列介面時](../Page/命令列介面.md "wikilink")，它主要供應予應用程序使用*）\[218\]。

Android也有另一個作業系統，名為「Trusty作業系統」，作為「Trusty」當中的一部分，軟件組件在行動裝置中支援著一個可信執行環境（TEE）。「Trusty和Trusty
API是 可調整的......」Trusty
OS的應用程式可以由C或C++所編寫（C++的支援是有限的），他們可以進入一個小型的C程式庫......所有Trusty應用程式都是單線程的；多線程的用戶空間目前並不支援......第三方應用程式的開發並非支援當前版本，而作業系統和處理器上使用軟件，為「受保護的內容執行[DRM框架](../Page/數位版權管理.md "wikilink")」。TEE還有許多其他用途，例如行動支付、銀行業的保安、全磁碟加密、多重身份驗證，裝置重設保護、重播保護的持久性存儲、無線顯示受保護的內容（強制轉換）、安全的PIN和指紋處理，甚至是惡意軟件檢測\[219\]。

### 開源社區

Android的[原始碼是由Google在](../Page/原始碼.md "wikilink")下發布，其開放的性質鼓勵著一個龐大的開發者社區及[發燒友利用開源碼作為社區驅動項目的基礎](../Page/發燒友.md "wikilink")，它可為舊設備提供更新、為高級用戶增加新功能，或最初隨附其他作業系統的裝置引入Android系統\[220\]。這些社區開發的版本通常比較透過官方製造商／運營商的渠道更快為裝置帶來新功能和更新的，亦具有相當質量的水平\[221\]；為不能再接收官方更新的舊裝置提供持續性支援；或把Android帶到正式發布使用其他作業系統的裝置上，例如[TouchPad](../Page/TouchPad.md "wikilink")。社區發布經常出現[Root前並包含由原始供應商並未提供的修改](../Page/Root_\(Android\).md "wikilink")，例如讓裝置內處理器[超頻或是](../Page/超頻.md "wikilink")[調高／低電壓的能力](../Page/動態電壓調節.md "wikilink")\[222\]。[CyanogenMod是社區中使用最廣泛的韌體](../Page/CyanogenMod.md "wikilink")\[223\]，惟現已停產並由[LineageOS繼任](../Page/LineageOS.md "wikilink")\[224\]。

從歷史上看，裝置製造商及行動運營商通常都不支持第三方韌體的開發。製造商對使用非官方軟件的裝置功能不正常，以及由此產生的支援費用表示擔憂\[225\]。此外，如CyanogenMod般經修改的韌體有時也會提供例如[Tethering等](../Page/Tethering.md "wikilink")...的功能，否則運營商將收取額外費用。因此，許多裝置中常見的技術障礙，包括鎖定[啟動程式及限制訪問root的權限都是常見於許多設備中](../Page/啟動程式.md "wikilink")。然而，隨著社區開發的軟件越來越受歡迎，[美國國會圖書館館長在發表聲明之後](../Page/美國.md "wikilink")，允許行動裝置進行[越獄](../Page/越獄_\(iOS\).md "wikilink")\[226\]，製造商和運營商經已對第三方開發軟件的立場軟化，包括[HTC](../Page/宏達國際電子.md "wikilink")\[227\]、[摩托羅拉](../Page/摩托羅拉.md "wikilink")\[228\]、[三星](../Page/三星集團.md "wikilink")\[229\]\[230\]及[索尼](../Page/索尼流動通訊.md "wikilink")\[231\]，他們提供支持和鼓勵發展。因此，隨著時間的推移，由於越來越多的裝置隨附著已解鎖或可解鎖的啟動程式，繞過的規避而安裝非官方韌體的需要降低了，這類似於[Nexus系列手機](../Page/Google_Nexus.md "wikilink")，雖然他們通常要求用戶放棄為裝置進行保修\[232\]。然而，儘管製造商已經接受，但美國的一些運營商仍然要求手機被鎖定，此讓開發人員和客戶感到沮喪\[233\]。

## 安全和隱私

### 公共機構的監督範圍

2013年9月，美國及英國的情報機構—[美國國家安全局](../Page/美國國家安全局.md "wikilink")（NSA）及[英國政府通訊總部](../Page/政府通訊總部.md "wikilink")（GCHQ）分別披露作為更廣泛的一部分，他們可以進入iPhone、黑莓手機及Android裝置中，存取用戶的數據。據報導，他們幾乎能夠閱讀所有智能手機上的資訊，包括短訊、位置、電郵及備忘錄\[234\]。2014年1月，進一步的報告顯示情報機構擁有攔截個人資訊傳輸的能力，他們透過社交網絡及其他流行的應用程式如《[憤怒鳥](../Page/愤怒的小鸟系列.md "wikilink")》，以用於廣告和其他商業原因而收集用戶的個人資訊。根據《[衛報](../Page/衛報.md "wikilink")》的報導，GCHQ有不同應用程式和廣告網絡的[Wiki風格指南](../Page/Wiki.md "wikilink")，以及可以從每個數據中抽取不同的資訊\[235\]。在該周後期，芬蘭的憤怒鳥開發者[Rovio娛樂宣布](../Page/Rovio娛樂.md "wikilink")，鑑於這些啟示，他們正重新考慮與其廣告平台的關係，並呼籲更廣泛的同業也應這樣做\[236\]。

這些文件顯示情報機構進一步努力攔截從Android和其他智能手機中Google地圖的搜索及查詢，以收集大量的位置信息\[237\]。儘管《衛報》指出「技術部門是如何收集和使用信息，特別是對於美國以外的人，他們所享有的隱私保護比美國人少，這些最新的披露也可能會引起公眾越來越多的關注」，然而NSA及GCHQ堅持認為他們的活動都符合所有相關的國內和國際法律\[238\]。

維基解密公佈了2013-2016年代號為的洩露文件，當中詳細說明了[中央情報局](../Page/中央情報局.md "wikilink")（CIA）進行電子監視和[網絡戰的能力](../Page/網絡戰.md "wikilink")，包括損害大多數智能手機的作業系統（包括Android）的能力\[239\]\[240\]。

### 常見的安全威脅

由於Android作業系統的自由和普及性，一些惡意程式和[病毒也隨之出現](../Page/病毒.md "wikilink")。2010年8月，[卡巴斯基病毒實驗室報告指發現到Android系統上首個](../Page/卡巴斯基實驗室.md "wikilink")[木馬程式](../Page/特洛伊木馬_\(電腦\).md "wikilink")，並將其命名為「Trojan-SMS.AndroidOS.FakePlayer.a\[241\]」，這是一個通過短訊方式感染智慧型手機的木馬程式，並且經已感染了一定數量的Android設備。除了透過短訊的感染方式，這些Android木馬程式還可以偽裝成一些主流的應用程式，並且還可以隱藏於一些正規的應用程式之中\[242\]\[243\]。

來自保安公司[趨勢科技的研究](../Page/趨勢科技.md "wikilink")，他們列出最常見Android韌體的類型為踐踏優質服務，在未經用戶同意或甚至是用戶不知情下從受感染的手機發送短訊至。其他惡意軟件於裝置上顯示不需要或侵入性廣告，或將用戶的個人信息發送至未經授權的第三方\[244\]。據報導，Android上的安全威脅正在呈幾何級數地增長；然而，Google的工程師辯論著安保公司出於商業原因而[誇大Android系統上的惡意軟件和病毒威脅](../Page/FUD.md "wikilink")\[245\]\[246\]，並指責安保行業玩弄恐懼而向用戶推銷病毒防護軟件\[247\]。Google堅持危險的惡意軟件其實是非常罕見的\[248\]，而[芬安全曾進行的一項調查顯示](../Page/芬氏安全.md "wikilink")，只有0.5％的Android惡意軟件是來自Google
Play商店\[249\]。

儘管Google通過定期的檢查撤除這些存在於Google
Play商店上的惡意程式和病毒，但是這並不能完全阻止其他病毒通過第三方網路的方式產生並且傳播\[250\]。2015年8月，Google宣布[Google
Nexus系列中的裝置將會開始每月收到安全性](../Page/Google_Nexus.md "wikilink")[修補程式](../Page/修補程式.md "wikilink")。Google還寫道：「Nexus裝置將會在至少兩年內繼續獲得重大更新，而安全性修補程式從裝置透過[Google
Store購買起的](../Page/Google_Store.md "wikilink")18個月內，或從初始可用性起計算三年以上的安全補丁\[251\]\[252\]\[253\]。」接下來的10月，[劍橋大學的研究人員得出結論](../Page/劍橋大學.md "wikilink")，由於缺乏更新和支援，87.7％使用中的Android手機有已知但未修補[安全漏洞](../Page/计算机安全隐患.md "wikilink")\[254\]\[255\]\[256\]。2015年8月，《[Ars
Technica](../Page/Ars_Technica.md "wikilink")》的榮恩·阿馬德奧（Ron
Amadeo）也寫道：「Android原先設計最重要的是被廣泛採用。Google從頭開始，由0%的市場佔有率，所以它很高興放棄控制權並讓每個人於採納的桌上佔一席位......現在，儘管Android擁有全球智能手機市場約75-80％的市場佔有率—使它不僅是世上最流行的行動作業系統，亦可說是現時最流行的作業系統。因此，安全性已成為了一個大問題。當Android生態系統沒有設備要更新，指令鏈的設計回歸，但Android仍然沿用軟件更新，它只是起不了作用\[257\]。」隨著Google每月更新時間表的消息出來後，包括三星和LG在內的一些製造商承諾每月發布安全性更新\[258\]。但是正如傑瑞·希爾登布蘭德（Jerry
Hildenbrand）於2016年2月的《Android中央》所指，「相反，我們在幾種極少數機型的特定版本中得到了一些更新，還有一堆破碎了的承諾」\[259\]。

2017年3月，在Google的安保博客的帖子上，Android的安保主管阿德里安·路德維希（Adrian Ludwig）及梅爾·米勒（Mel
Miller）寫道：「在2016年，來自200多家製造商，有超過735,000,000個裝置獲得平台的安全性更新」，並且「我們的運營商及硬件合作夥伴幫忙為這些更新擴展了部署，在2016年最後一個季度為全球排名首50位的超過一半裝置發布更新」。他們還寫道：「截至2016年底，大約有一半使用中的裝置在過去的一年並沒有接收到平台的安全性更新」，並指他們的工作將繼續專注於精簡安全性更新程序，以便製造商進行部署\[260\]。此外，對於《[TechCrunch](../Page/TechCrunch.md "wikilink")》的評論中，路德維希表示，安全更新的等待時間已從「6至9星期縮減至數天」，截至2016年底，在北美約有78％的旗艦裝置都獲得最新的安全性更新\[261\]。

修補核心作業系統中發現的錯誤通常都不會送達到老舊及低價裝置的用戶手上\[262\]\[263\]。然而，Android的開源特性容許安保承包商採用現有設備，並把它們改編用於高度安全性的用途上，例如：三星跟[通用動力合作](../Page/通用動力.md "wikilink")，透過他們的[開放核心實驗室的收購](../Page/開放核心實驗室.md "wikilink")，以在「Knox」的項目上重建「果凍豆」\[264\]\[265\]。

Android的智能手機能夠報告[Wi-Fi接入點的位置](../Page/Wi-Fi.md "wikilink")（在電話用戶四處行動時遇到），構建包含數億個此類進入點物理位置的數據庫。這些數據庫構成了用於智能手機中電子地圖的定位，讓他們使用應用程式如[Foursquare](../Page/Foursquare.md "wikilink")、[Google定位](../Page/Google定位.md "wikilink")、[Facebook定位](../Page/Facebook.md "wikilink")，並能提供基於該位置的廣告\[266\]。第三方的監控軟件，例如TaintDroid\[267\]，那是由學術研究資助的項目，在某些情況下，可以檢測到用戶的個人資訊何時從應用程式發送至遠端伺服器\[268\]。

目前Android作業系統上已經擁有不同公司的[殺毒軟件來防止裝置中毒](../Page/殺毒軟件.md "wikilink")，例如[Avast](../Page/Avast.md "wikilink")、[F-Secure](../Page/F-Secure.md "wikilink")、[Kaspersky](../Page/Kaspersky_Internet_Security.md "wikilink")、[Trend
Micro](../Page/Trend_Micro.md "wikilink")、[Symantec](../Page/Symantec.md "wikilink")、[金山毒霸等防護軟體也已經發布了Android版本](../Page/金山毒霸.md "wikilink")。

某些病毒，目前經過Cross-Compile的測試，證實它們在[Linux上無法正常運作](../Page/Linux.md "wikilink")。

2018年11月，知名安卓軟體Magisk（ROOT神器）的開發者topjohnwu在XDA上發帖稱，現在很多安卓手機存在系統漏洞，該漏洞可使第三方應用程序在不被用戶授權的情況下監視其它程序的運行，其中可能涉及到個人隱私的泄露問題，值得關注。\[269\]

### 技術性保安功能

[Android_App_Permissions.png](https://zh.wikipedia.org/wiki/File:Android_App_Permissions.png "fig:Android_App_Permissions.png")
Android系統可利用[沙盒](../Page/沙盒_\(計算機安全\).md "wikilink")（sandbox）機制，沙盒於系統中是個分離的區域，該區域無法進入系統的其他資源，除非在安裝應用程式時，用戶明確地授予進入的權限，然而對於預先安裝的應用程式則可能無法實現。這是不可能的，例如預先安裝的相機應用程式，在沒有完全禁用相機的情況下，關閉麥克風的進入。這情況在Android的版本7和8當中也有效\[270\]。所有的應用程式都可先被簡單地解壓縮到沙盒中進行檢查，並且將應用程序所需的權限提交給系統，再將其所需權限以列表的形式展現出來供用戶查看。例如一個第三方的瀏覽器需要「連接網絡」的權限，或者一些軟體需要撥打電話，或發送短訊...等。用戶可以根據所需權限來考慮自己是否需要安裝，應用程式只能在用戶同意之後才能進行安裝\[271\]。

2012年1月，[美國國家安全局發布了SE](../Page/美國國家安全局.md "wikilink")
Android（原本名為Security Enhanced Android，後來改名為SE for
Android或Security Enhancements for
Android\[272\]）的開放源碼專案及程式碼，使Android系統支援[強制存取控制](../Page/強制存取控制.md "wikilink")（Mandatory
Access Control）以增加系統安全性\[273\]\[274\]。

自2012年2月起，Google已使用其[惡意軟體掃描器Google](../Page/惡意軟體.md "wikilink")
Bouncer監察和掃描在Google
Play商店上的應用程式\[275\]\[276\]。2012年11月，Google於[Android
Jelly
Bean推出](../Page/Android_Jelly_Bean.md "wikilink")「驗證應用程式」的功能，作為作業系統的一部分，掃描來自Google
Play和第三方來源的所有應用程式，以防止惡意行為\[277\]。「驗證應用程式」最初只在安裝期間進行，然而它在2014年的更新後不斷地掃描應用程式，並在2017年透過「設定」中的選項，讓用戶可見到該功能\[278\]\[279\]。

在安裝「驗證應用程式」前，Google
Play商店會顯示一系列應用程序所需的要求，查閱這些權限後，用戶可以選擇接受或拒絕，但必須要在接受下才能安裝應用程式\[280\]。

在[Android Jelly
Bean中](../Page/Android_Jelly_Bean.md "wikilink")，其原生應用程式管理機制App
Ops首次被引入，但並未開放讓一般用戶使用\[281\]，直至[Android
Marshmallow](../Page/Android_Marshmallow.md "wikilink")，系統啟用其原生應用程式的權限控制（並非App
Ops），它容許用戶在程式安裝後仍能對特定類別的權限使用進行開關，若應用程式的某組權限使用被關閉並準備進入相應權限的方式時，系統會詢問用戶是否容許\[282\]。

在[Android
Marshmallow之前](../Page/Android_Marshmallow.md "wikilink")，其權限系統是以「一刀切」的方式進行，應用程式開發商會申請一些非必需的進入權限，或是申請把用戶的私隱資訊作暗中收集、使用、發送至開發商的伺服器；高端用户可以透過[取得root權限增加控制程式或修改程式的安裝文件](../Page/Root_\(Android\).md "wikilink")，以仔細限制程式的使用權限，或當中涉及數據的運用\[283\]，然而程式對權限只能在安裝時選擇「全部同意／全部拒絕」的選項經常為人詬病。

在Marshmallow中，權限系統已更改；應用程式不再在安裝時自動授予其所有指定權限，它會以「選擇加入系統」來代替，當他們首次使用時，系統會提示用戶授予或拒絕該應用程式的權限。應用程式會記著所授權限，這可以由用戶隨時撤銷授權。但是，預先安裝的應用程式不能總是以此方法卸載。在某些情況下，用戶可能無法拒絕預先安裝的應用程式的某些權限，甚至無法禁用它們。就如Google
Play的應用程式，無法卸載或禁用。任何強制停止的嘗試的結果只會導致應用程式自動重啟\[284\]\[285\]。新權限僅供利用[軟體開發套件](../Page/軟體開發套件.md "wikilink")（SDK）為Marshmallow開發的應用程序使用，舊的應用程式將繼續使用以前沿用沒有中間或妥協的方式。對於這些應用程式，其權限仍然可以被撤銷，但這可能會妨礙它們正常運作，並會因此顯示警告\[286\]\[287\]。

2013年8月，Google發布了Android設備管理器，那是一項於同年12月發布的Android應用程序\[288\]\[289\]，它容許用戶利用他們的Android裝置作遠程跟踪、定位及抹除的服務\[290\]\[291\]，該應用程式於2017年5月重新命名為「尋找我的手機」\[292\]\[293\]。在2016年12月，Google推出可信聯繫人的應用程式，讓用戶在緊急情況下要求取得親人的位置追踪\[294\]\[295\]。

## 許可證

Android作業系統的[原始碼是](../Page/原始碼.md "wikilink")[開源碼](../Page/開放源碼.md "wikilink")：那是由Google私下開發的，在發布新版Android的同時公開發布源代碼，一切程式碼為公開免費\[296\]的。Google發布了大部分的源代碼（*根據[開放手機聯盟所開發的非](../Page/開放手機聯盟.md "wikilink")[Copyleft的](../Page/Copyleft.md "wikilink")[Apache授權條款](../Page/Apache授權條款.md "wikilink")2.0下，允許修改和再分配，剩下的Linux内核部分則延續[GPL第](../Page/GPL.md "wikilink")2版的許可*\[297\]\[298\]\[299\]\[300\]），當中包括網絡和電話\[301\]\[302\]。該許可證並不授予「Android」的商標權利，所以設備製造商和無線運營商必須根據個人合同跟Google獲得許可。Google典型地與硬件製造商合作生產一款旗艦設備（Nexus系列的一部分），它以包含新版本的Android為特色，然後在該裝置發布後公開其源代碼\[303\]。Google也不斷發問卷和開放修改清單、更新情況及程式碼來讓任何人看到並且提出意見和評論，以便按照用戶的需求改進Android作業系統。Android版本中唯一沒有立即發布其源代碼的是僅限於平板電腦3.0使用的「蜂窩（Honeycomb）」版本，其原因是根據[安迪·魯賓於Android的官方博客文章指](../Page/安迪·魯賓.md "wikilink")，是因為生產[摩托羅拉Xoom而倉促生產](../Page/摩托羅拉Xoom.md "wikilink")\[304\]，他們並不希望第三方試圖把適用於智能手機的Android版本套用在平板電腦上以創造「非常糟糕的用戶體驗」\[305\]。

只有基礎的Android作業系統（包括一些應用程式）才是開源軟件，任何廠商都不須經過Google和開放手持設備聯盟的授權隨意使用Android作業系統；大多數Android裝置都附帶著大量的專有軟件，例如是[Google流動服務](../Page/Google流動服務.md "wikilink")，當中包括[Google
Play商店](../Page/Google_Play.md "wikilink")、Google搜尋，以及[Google
Play服務](../Page/Google_Play服務.md "wikilink") —
那是一個提供與Google提供的服務[應用程式介面集成的軟件層](../Page/應用程式介面.md "wikilink")。這些應用程式必須由裝置製造商從Google得到許可，並且只能在符合其兼容性指引及其他要求的配備裝置上\[306\]，任何廠商都不能在未授權的情況下在其產品上使用Google的標誌和應用程序。除非生產商能證明其生產的裝置符合Google兼容性定義文件（CDD），才能在智慧型手機上預載屬於Google的應用程式。所有符合Google生產規定的智能手機廠商才可以在其產品上印有「With
Google」的標誌\[307\]。由製造商生產的定制，認證的Android發行版（例如是[TouchWiz及](../Page/TouchWiz.md "wikilink")[HTC
Sense](../Page/HTC_Sense.md "wikilink")），也可以使用他們自己的專有軟件，以及新增並不包括在Android作業系統中的股票應用程式，以替換Android中某些股票的應用程式\[308\]。對於設備中的某些硬件組件，這裡可能還需要的驅動程式的使用\[309\]\[310\]。

### 對製造商的影響力

Google僅將其流動服務軟件以及Android的商標授權予符合Android兼容性計劃中指定的Google的兼容性標準\[311\]，因此這使作業系統本身有著重大變化的Android分支（不包括任何Google的非免費組件）跟其所需的應用程式保持不兼容，並且必須隨附替代軟件，以替代Google
Play商店中的市場\[312\]。此類Android分支的例子有[亞馬遜的](../Page/亞馬遜公司.md "wikilink")[Fire
OS](../Page/Fire_OS.md "wikilink")（那是用於為亞馬遜服務的[Kindle
Fire系列平板電腦](../Page/Kindle_Fire.md "wikilink")）、[諾基亞X軟件平台](../Page/諾基亞X軟件平台.md "wikilink")（的分支，主要用於為[諾基亞及](../Page/諾基亞.md "wikilink")[微軟服務](../Page/微軟.md "wikilink")），以及由於在某些地區一般都無法使用Google服務（例如[中國大陸](../Page/中華人民共和國網絡審查.md "wikilink")），故排除了Google應用程式的其他分支\[313\]\[314\]。2014年，Google也開始要求所有授權使用Google流動服務軟件的Android裝置的啟動畫面上顯示一個突出的「由Android提供支援」的標誌\[315\]。Google還強制執行了在裝置上設置Google流動服務及優惠捆綁計劃，包括強制捆綁整個Google主要應用程式套件，和Google搜索的捷徑，以及Google
Play商店的應用程式必須以預設配置或在主畫面的預設配置附近\[316\]。

從前在Android的早期版本中使用AOSP代碼的一些股票應用程式及元件，例如搜尋、音樂，行事曆及位置API都被Google遺棄，以支持透過Play商店分發的非免費替代品（Google搜尋、Google
Play音樂及Google行事曆）和Google
Play服務，它們不再是開源的。此外，某些應用程序的開源變體還排除非自由版本中存在的功能，例如相機中的全景相片（Photosphere），和在預設主畫面上的[Google即時資訊](../Page/Google即時資訊.md "wikilink")（*由「Google
Now
Launcher」的專有版本專用，其代碼嵌入於主要Google應用程式的代碼中*\[317\]\[318\]\[319\]\[320\]）。這些措施可能旨在阻止分叉並鼓勵符合Google商業許可的要求，作為大多數作業系統的核心功能（以及第三方軟件）依賴於Google獨家授權的專有組件，並且它需要大量的開發資源來開發一套替代套件和API來複製或把它們替換。不使用Google組件的應用程式也會處於功能劣勢，因為他們只能使用作業系統本身包含的API\[321\]。

2018年3月，據報導Google開始阻止「未經認證」的Android裝置使用Google流動服務軟件，並顯示一條警告指，「設備製造商已預載了未經Google認證的Google應用程式及服務」。自定義ROM的用戶能夠將他們的裝置ID以他們的Google帳戶註冊來刪除此障礙\[322\]。

開放手機聯盟的成員，其中包括大多數Android
OEM，從基於作業系統分支的Android設備生成的合同也是被禁止的\[323\]\[324\]；2012年，[宏碁被Google強行停止由](../Page/宏碁.md "wikilink")[阿里巴巴集團生產的](../Page/阿里巴巴集團.md "wikilink")[AliOS驅動的設備](../Page/AliOS.md "wikilink")，以威脅要從OHA中移除，因為Google認為該平台是Android的不兼容版本。阿里巴巴集團為這些指控進行辯護，爭辯指其作業系統是Android的獨特平台（主要使用[HTML5應用程式](../Page/HTML5.md "wikilink")），但是它整合了Android平台的部分內容，容許向後兼容第三方Android軟件。事實上，這些裝置確實附帶了一個提供Android應用程式的應用商店；然而，它們大多數都是[盜版軟件](../Page/盜版.md "wikilink")\[325\]\[326\]\[327\]。

## 系统架构

### 中介软件

作業系統与應用程式的沟通桥樑，并用分為两层：[函式层和](../Page/函式.md "wikilink")[虚拟机器](../Page/虚拟机器.md "wikilink")。

[Bionic是Android改良libc的版本](../Page/Bionic_\(軟體\).md "wikilink")。Android包含了Chrome浏览器引擎。Surface
flinger是就2D或3D的内容显示到萤幕上。Android使用工具链為Google自制的Bionic Libc。

Android采用OpenCORE作為基础多媒体框架。OpenCORE可分7大块：PVPlayer、PVAuthor、Codec、PacketVideo
Multimedia Framework（PVMF）、Operating System Compatibility
Library（OSCL）、Common、OpenMAX。

Android使用[Skia為核心图形引擎](../Page/Skia_Graphics_Library.md "wikilink")，搭配OpenGL/ES。Skia与Linux
Cairo功能相当，但相较于Linux Cairo，Skia功能还只是阳春型的。2005年Skia公司被Google收购，2007年初，Skia
GL源码被公开，目前Skia也是Google Chrome的图形引擎。

Android的多媒体资料库采用[SQLite资料库系统](../Page/SQLite.md "wikilink")。资料库又分為共用资料库及私用资料库。使用者可透过ContentProvider类别取得共用资料库。

Android的中间层多以Java實作，4.4版之前使用特殊的[Dalvik虚拟机器](../Page/Dalvik虚拟机.md "wikilink")。Dalvik虚拟机器是一种“暂存器型态”的Java虚拟机器，变数皆存放于[暂存器中](../Page/暂存器.md "wikilink")，虚拟机器的指令相对减少。5.0版起改用[Android
Runtime](../Page/Android_Runtime.md "wikilink")（ART）。

Dalvik虚拟机器可以有多个实例，每个Android應用程式都用一个自属的Dalvik虚拟机器来执行，让系统在执行程式时可达到最佳化。Dalvik虚拟机器并非执行Java字节码，而是执行一种称為.dex格式的档案。

### 硬體抽像層（HAL）

Android的硬體抽像層是能以封闭源码形式提供硬體驅動模組。HAL的目的是為了把Android framework与Linux
kernel隔开，让Android不至过度依赖Linux kernel，以达成“内核独立”（kernel
independent）的概念，也让Android
framework的开发能在不考量驅動程式實作的前提下进行发展，以達到壟斷GPU市場的目的。

HAL stub是一种代理人的概念，stub是以\*.so档的形式存在。Stub向HAL“提供”操作函数，并由Android
runtime向HAL取得stub的操作，再[回调这些操作函数](../Page/回调函数.md "wikilink")。HAL里包含了许多的stub（代理人）。Runtime只要说明“类型”，即module
ID，就可以取得操作函数。

### 程式語言

Android是执行于[Linux
kernel之上](../Page/Linux_kernel.md "wikilink")，但并不是[GNU/Linux](../Page/GNU/Linux.md "wikilink")。因為在一般GNU/Linux裏支持的功能，Android大都没有支援，包括[Cairo](../Page/Cairo_\(繪圖\).md "wikilink")、[X11](../Page/X11.md "wikilink")、[Alsa](../Page/ALSA.md "wikilink")、[FFmpeg](../Page/FFmpeg.md "wikilink")、[GTK](../Page/GTK.md "wikilink")、[Pango及](../Page/Pango.md "wikilink")[Glibc等都被移除掉了](../Page/Glibc.md "wikilink")。Android又以bionic取代Glibc、以Skia取代Cairo、再以opencore取代FFmpeg等等。Android為了达到商业应用，必须移除被[GNU
GPL授权证所约束的部份](../Page/GNU_GPL.md "wikilink")，Android並沒有用户层驱动（user space
driver）這種東西。所有的驱动還是在内核空间中，並以[HAL隔開版權問題](../Page/硬體抽象層.md "wikilink")。bionic/libc/kernel/
并非标準的内核头文件（kernel header
files）。Android的内核头文件是利用工具由Linux内核的头文件所产生的，这样做是為了保留常数、资料结构与巨集。

Android的核心基於[Linux](../Page/Linux.md "wikilink")，除了核心之外，則是中介層、資料庫元和用[C/C++編寫的](../Page/C/C++.md "wikilink")[API以及應用程式框架](../Page/API.md "wikilink")。Android的應用程序通常以[Java資料庫元為基礎編寫](../Page/Java.md "wikilink")，運行程序時，應用程式的代碼會被即時轉變為Dalvik
dex-code（Dalvik Executable），然後Android作業系統通過使用即時編譯的Dalvik虛擬機來將其運行。\[328\]

目前Android的Linux
kernel控制包括安全、[記憶體管理](../Page/記憶體管理.md "wikilink")、[进程管理](../Page/进程管理.md "wikilink")、[网络堆叠](../Page/网络堆叠.md "wikilink")、[驅動程式模型等](../Page/驅動程式模型.md "wikilink")。下载Android源码之前，先要安装其构建工具[Repo来初始化源码](../Page/Repo_\(腳本\).md "wikilink")。Repo是Android用来辅助[Git工作的一个工具](../Page/Git.md "wikilink")。

## 應用程序

### Google Play

[缩略图](https://zh.wikipedia.org/wiki/File:Samsung_Galaxy_Note_3_\(N9009\).jpg "fig:缩略图")（N9009，[中国电信双卡双待定制行货版](../Page/中国电信.md "wikilink")）上的菜单，所有Google应用程序全部删除，并替换成功能类似或者相同的应用。|299x299像素\]\]

透過前身為Android
Market的網上商店平台，提供應用程式和遊戲供用戶下載，截至2013年7月，官方認證應用程式数量突破100万，超过苹果App
Store成为全球最大应用商店\[329\]\[330\]。

Google Play軟件的中文為Play商店，因Google Play的圖示像菜市场用的袋子，所以部分中国用户又称之为“菜市场”。

2009年2月，[Google推出Android](../Page/Google.md "wikilink")
Market线上應用程式商店，用户可在该平台网页寻找、购买、下载及评级使用智慧型手機应用程序及其他内容\[331\]。第三方軟體開發商和自由開發者則可以通過Android
Market發布其開發的應用程序。在2011年12月，Android
Market上的應用程序下載量超過100億次。同時，全球已有1億3千萬部Android設備在Android
Market即現在的Google Play上下載過軟體。

只有通過Google許可並且認證的廠家才能在其產品設備上安裝Google服務框架和Google
Play。同時受到部分地區和國家的政策影響，Google根據部分地區和國家的政策對Google
Play上的內容進行了過濾，因此各地區和國家看到的內容不一樣。此外，受到部分地區和國家的電信運營商的影響，Google
Play在部分地區和國家可能不可使用。

Google
Play內的付費程序在許多國家與地區內提供，如[美國](../Page/美國.md "wikilink")、[英國](../Page/英國.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[德國](../Page/德國.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台湾](../Page/台湾.md "wikilink")等\[332\]。由於受到[Google退出中國大陸事件影響](../Page/Google退出中國大陸事件.md "wikilink")，目前Google
Play的服務還沒有擴展到中國大陸地區，但大陸用戶仍可通過國際[信用卡和相關的免費軟體來完成購買](../Page/信用卡.md "wikilink")。需要注意的是，通過這種方法購買的軟體可能不會得到當地承認的[發票](../Page/發票.md "wikilink")。

除了Google的Google Play之外，還有其他公司的應用程式市場，如亞馬遜公司的Amazon
Appstore，三星公司的三星应用商店，Fetch，AppBrian，与中国国内的[酷安](../Page/酷安.md "wikilink")、[豌豆荚等](../Page/豌豆荚.md "wikilink")\[333\]。

此外，Google Play还提供在Android系统后台对安装的应用程序进行验证的服务，以最大程度防范恶意软件的侵害\[334\]。

2014年11月，Google已表示将支持中国开发者开发Google Play应用。

### 程序開發

[Android_Easter_egg.jpg](https://zh.wikipedia.org/wiki/File:Android_Easter_egg.jpg "fig:Android_Easter_egg.jpg")
在早期的Android應用程序開發中，通常通過在Android
SDK（Android軟體開發包）中使用Java作為編程語言來開發應用程序。開發者亦可以通過在Android
NDK（Android
Native開發包）中使用C語言或者C++語言來作為編程語言開發應用程序。同時Google還推出了適合初學者編程使用的[Simple語言](../Page/Simple語言.md "wikilink")，該語言類似微軟公司的[Visual
Basic語言](../Page/Visual_Basic.md "wikilink")。此外，Google還推出了Google App
Inventor開發工具，該開發工具可以快速地構建應用程式，方便新手開發者。

### 移植到Chrome OS

2016年5月20日，Google在Google I/O上表示，將會把 Google Play商店和Android App帶到[Chrome
OS中](../Page/Chrome_OS.md "wikilink")，使Chrome OS可以執行Android
APP增加推行[Chromebook和](../Page/Chromebook.md "wikilink")[Chromebox的動力](../Page/Chromebox.md "wikilink")，解決該平台應用程式不足的問題；此外Chromebook在美國市場出貨量已經超越蘋果Mac\[335\]。

Google把Android應用程式整合進Chrome OS有市場傳言Google的目的是想將兩者合併，但Android、Chrome OS
與Google Play部門的資深副總裁Hiroshi Lockheimer在專訪中駁斥外界傳言，稱並不會將兩者合併，Chrome
OS更像是一台電腦，以桌面、檔案管理、鼠標和鍵盤為基礎，再搭配App，而Android則是以觸控App為核心，兩者的市場模式不同並不會嘗試合併\[336\]。

### 應用程式安全機制

[Android_App_Permissions.png](https://zh.wikipedia.org/wiki/File:Android_App_Permissions.png "fig:Android_App_Permissions.png")

#### SEAndroid

[美國國家安全局在](../Page/美國國家安全局.md "wikilink")2012年1月發布SE Android（Security
Enhanced Android，后改名为SE for Android，Security Enhancements for
Android\[337\]）開放原始碼專案和程式碼，使Android系統支援[強制存取控制](../Page/強制存取控制.md "wikilink")（Mandatory
Access Control）以增加系統安全性\[338\]\[339\]。

### 在中国大陆的情况

在中国大陆生产并售卖，或在境外生产并由当地代理商入口，在中国大陆售卖的的裝有Android作業系統的智慧型手機（俗称[行货](../Page/行货.md "wikilink")），均把境外版常附帶的部分Google应用程式（包括Google服务框架）删除，替换为中国大陆功能相同或相似的应用，即使這些裝置已獲得Google相容性認證及使用授權（CTS及GMS）。\[340\]\[341\]因此，中國大陸用户需要自行重新安裝Google程式（部分品牌还需要将手机root之后重新刷入Google服务框架）才能使用Google相关的应用服务。能使用Google服务的标志是可以在系统设置中的法律信息选项中找到“Google法律信息”选项。如果没有，则此机已删除Google服务框架。但通過非正式渠道流入中國大陸販售市場的Android裝置（俗稱[水貨](../Page/水貨.md "wikilink")），則基本不受此影響（但部分裝置的Android系統內則被一些非正規供貨商額外預裝了某些應用程式，可能會精簡Google服務框架等組件\[342\]）。而在中國大陸生產，並在境外銷售的裝有Android作業系統的智慧型手機，亦不受此影響，但少數裝有Android作業系統的智慧型手機（例如在臺灣銷售的[美圖手機](../Page/美圖手機.md "wikilink")2），仍然需要自行重新安裝程式才能使用Google相關的應用服務\[343\]。

## 招待會

Android於2007年亮相時獲得的反應冷淡，雖然Google曾與備受尊敬的科技公司合作組建[開放手機聯盟](../Page/開放手機聯盟.md "wikilink")，分析師們對此留下深刻印象，但目前仍不清楚手機製造商是否願意用Android取代現有的作業系統\[344\]。對於開源碼的想法，基於Linux[開發平台引起了人們的興趣](../Page/系統平台.md "wikilink")\[345\]，但面對著來自智能手機市場的老牌企業（如諾基亞和微軟），以及正在開發中的Linux行動作業系統作為其競爭對手\[346\]。這些老牌廠商持著懷疑的態度，諾基亞被引述說：「我們不認為這是一種威脅」，而微軟Windows
Mobile團隊成員指「我不明白他們將會產生的影響」\[347\]。

從那時起，Android已經發展成為使用最廣泛的智能手機作業系統\[348\]\[349\]，及成為「最快的可用流動體驗之一\[350\]」。評審強調了作業系統的開源性質是其定義優勢之一，它容許例如[諾基亞](../Page/諾基亞.md "wikilink")（）\[351\]、[亞馬遜](../Page/亞馬遜公司.md "wikilink")（[Kindle
Fire](../Page/Kindle_Fire.md "wikilink")）、[巴諾書店](../Page/巴諾書店.md "wikilink")（[Nook](../Page/Nook.md "wikilink")）、[Ouya](../Page/Ouya.md "wikilink")、[百度及其他](../Page/百度.md "wikilink")[複刻軟件和發布他們運行自己的Android定製版本的硬件](../Page/复刻_\(软件开发\).md "wikilink")。結果是，科技網站《[科技藝術](../Page/Ars_Technica.md "wikilink")》把其描述為：對於沒有自己的行動平台的公司來說，「幾乎是推出新硬件的預設作業系統\[352\]」。這種開放性和靈活性也出現在最終用戶的層面：Android容許裝置作廣泛的客製化，他們的擁有者及應用程式可以從非Google的應用程式商店和第三方網站中獲得。這些被引用為Android手機較其他手機的主要優勢之一\[353\]\[354\]。

儘管Android很受歡迎，包括其激活率是[iOS的三倍](../Page/iOS.md "wikilink")，有報告指Google仍未能利用他們的其他產品及網絡服務成功地將Android變成分析師所預期的賺錢機器\[355\]。《[The
Verge](../Page/The_Verge.md "wikilink")》建議指由於廣泛的客製化及非Google應用程式及服務的激增，使Google正在失去對Android的控制權，亞馬遜的Kindle
Fire系列使用的是Fire
OS，這是一款經過大量修改的Android分支版本，它不包含或支援任何Google的專有組件，並要求用戶從Google
Play商店的競爭對手[亞馬遜應用商店中取得軟件](../Page/亞馬遜應用商店.md "wikilink")\[356\]
。2014年，為了提高Android品牌的知名度，Google開始要求具有其專屬組件的裝置在啟動屏幕上顯示Android的標誌\[357\]。

Android遇到「碎片化」的情況\[358\]，那是在各種Android裝置出現的情況，無論是硬件變化還是其運行軟件上的差異而言，讓開發整個生態系統中一致的應用程序任務較其競爭平台上硬件和軟件變化較少的[iOS更難](../Page/iOS.md "wikilink")，例如，根據2013年7月的，這裡有11,868個Android的裝置型號，眾多不同的屏幕尺寸，同時使用8種Android作業系統，而絕大多數的iOS用戶已升級到作業系統的最新版本\[359\]。如的評論家斷言硬件和軟件的碎片化，推動著Android透過大量的低端、低價的產品使用舊版的Android使用率不斷上升。他們維持這一點，迫使Android開發人員編寫「最平庸的消費者群體」以取得盡可能多的用戶，然而他們在裝置上僅利用較少比例的最新硬件或軟件功能，這樣他們的動機似乎太少\[360\]。然而，開發Android及iOS應用程式的OpenSignal得出的結論是，儘管碎片化的情況可能會使軟件開發更加棘手，然而Android在全球更廣泛的影響力增加了潛在的回報\[361\]。

### 市場份額

[Samsung_Galaxy_logo.jpg](https://zh.wikipedia.org/wiki/File:Samsung_Galaxy_logo.jpg "fig:Samsung_Galaxy_logo.jpg")系列為目前銷量和市佔率最高的Android智能手機。\]\]

2009年的第二季，市場研究公司Canalys估計，Android佔全球智能手機作業系統市場的2.8％\[362\]。根據本土市場研究公司ComScore的數據顯示，2009年11月時，Android在美國的智慧型手機作業系統市場的佔有率為5.2%\[363\]，在2010年2月時，這個數字變成了9.0%，而在2010年第三季度末時，Android已經佔據了美國市場的21.4%的份額\[364\]。到了2010年5月，Android擁有全球智能手機市場的10％，超越了[Windows
Mobile](../Page/Windows_Mobile.md "wikilink")\[365\]，而在美國，Android佔有28％的份額，超越了[iOS](../Page/iOS.md "wikilink")\[366\]。在2010年第四季，其全球份額已經增長佔市場的33％，成為最暢銷的智能手機平台\[367\]，超越了[Symbian](../Page/Symbian.md "wikilink")\[368\]。根據comScore的數據，Android在美國於2011年4月成為最暢銷的平台，以31.2％的智能手機份額超越[BlackBerry
OS](../Page/BlackBerry_OS.md "wikilink")\[369\]。

對於第三方市場的流行，部份Android用戶不願意付費購買應用程式，轉而直接下載已被駭客破解的軟體。2010年8月，有遊戲開發者針對其作品進行的調查指出，在下載盜版方面，亞洲玩家佔97%，歐洲玩家佔70%，而北美玩家佔47%\[370\]。

截止至2011年6月，Google表示每天透過Google伺服器以激活的Android裝置多達到了55萬部\[371\]，並且以每週4.4%的速度增長\[372\]。2011年8月1日，Canalys的數據顯示，Android已佔據美國48%的智能手機市場的份額\[373\]。

2011年7月，Google表示每天有550,000個Android裝置被激活\[374\]，高於5月份的每天有400,000個\[375\]，在當時為止已超過1億個裝置被激活\[376\]</ref>，每週增長4.4％\[377\]。2012年9月，每天激活130萬個裝置，在當時為止已激活5億台裝置\[378\]\[379\]。截至2011年第三季，[高德納估計超過一半](../Page/高德納諮詢公司.md "wikilink")（52.5％）的智能手機銷量是屬於Android的\[380\]。

2011年10月13日，Google表示全球市場上有1.9億部Android裝置透過Google認證\[381\]，而在2011年11月16日，全球市場上已有2億部Android裝置透過Google認證\[382\]。然而，配備Android作業系統的平板電腦在所有Android裝置中佔的比例卻只有1.8%，只有380萬Android蜂巢平板被售出，遠遠低於蘋果公司[iPad的銷量](../Page/iPad.md "wikilink")\[383\]。

Android的市場會因地區而有差異。2012年5月，根據市場調查公司的數據顯示，Android於全球智能手機作業系統中的份額已經過半，達到了60%，即全球有一半的智能手機正在使用Android\[384\]。2012年7月，美國使用Android「13歲以上的流動用戶」高達52％\[385\]，在[中國則上升至](../Page/中國.md "wikilink")90％\[386\]。2012年6月，Google在2012
Google
I/O大會上表示全球市場上有4億部Android設備被啟動，每日啟動約100萬部。另一方面Android系統的平板電腦的市佔率為45.8%，相較之下iOS的市佔率為52.8%\[387\]。截至2012年第三季，根據研究公司IDC的數據，Android佔據了75％的全球智能手機市場\[388\]，總共有7.5億個裝置被激活，而在2013年4月，Android每天有150萬次激活\[389\]。，Android在中國的市佔率為71.5%，超越其競爭對手[蘋果公司的](../Page/蘋果公司.md "wikilink")50%，在全世界的市佔率接近70%。\[390\]；同時，Google
Play商店中已有480億個應用程式經已被安裝\[391\]；在[Google
I/O上](../Page/Google_I/O.md "wikilink")，[桑德爾·皮蔡宣布](../Page/桑德爾·皮蔡.md "wikilink")9億台Android裝置已被激活\[392\]。2013年9月，已有10億個Android裝置被激活\[393\]。2013年，Android系統的平板電腦市佔率高達61.9%，超過[iOS的](../Page/iOS.md "wikilink")36%\[394\]。

在大多數市場中，包括美國，Android裝置佔智能手機銷量的一半以上，「只有在[日本](../Page/日本.md "wikilink")，[蘋果公司才能名列前茅](../Page/蘋果公司.md "wikilink")」（根據2013年9月至11月的數字\[395\]）。在2013年底，自2010年的四年內已銷售超過15億部Android智能手機\[396\]\[397\]，使Android成為最暢銷的手機和平板電腦作業系統。預計到2014年底，Android智能手機將有30億個的銷售量（包括前幾年）。據Gartner研究公司稱，自2012年以來，Android的裝置每年都超越所有競爭者\[398\]。在2013年，它超過了Windows的比率為2.8比1，或說是5.73億\[399\]\[400\]\[401\]。，Android擁有所有作業系統中最大的[現有用戶群](../Page/現有用戶群.md "wikilink")\[402\]；自2013年以來，銷售並使用它的裝置也超過使用Windows、iOS及Mac
OS X合併起來的數字\[403\]。

根據[StatCounter](../Page/StatCounter.md "wikilink")，只是僅跟踪瀏覽網頁的用途，Android是自2013年8月以來最受歡迎的行動作業系統\[404\]。Android在印度和其他幾個國家是最受歡迎的網頁瀏覽作業系統（除日本和朝鮮外，幾乎整個亞洲）。根據StatCounter，Android是在所有非洲國家中最多使用的行動裝置系統，它並表示「在一些國家中，手機使用率已經超越桌面，包括印度，南非和沙特阿拉伯\[405\]」，幾乎所有非洲國家已經是這樣做了（包括埃及在內的7個國家除外），例如埃塞俄比亞和肯尼亞，其中包括平板電腦的行動使用率為90.46％（僅限Android佔所有用途的75.81％）\[406\]\[407\]。

雖然[西方世界中的Android手機通常包括Google專有的附加組件](../Page/西方世界.md "wikilink")（如Google
Play）到其他開源作業系統，然而在新興市場的事實並非如此；「ABI
Research聲稱，在2014年第二季中，全球共有6,500萬個裝置使用開源的Android，高於第一季的5,400萬台」；根據國家，估計只基於AOSP源代碼的手機百分比放棄Android商標的地區：泰國（44％）、菲律賓（38％）、印尼（31％）、印度（21％）、馬來西亞（24％）、墨西哥（18％）、巴西（9％）\[408\]。

根據2015年1月Gartner的報告指，「Android在2014年有超過了10億個裝置，並將於2015年繼續以雙位數字的速度增長，較去年同期增加了26％。」這是作業系統在一年內達到超過十億最終用戶的首次；在2014年達到接近11.6億的最終用戶，Android的出貨量超越[iOS及](../Page/iOS.md "wikilink")[OS
X加起來的四倍](../Page/macOS.md "wikilink")，亦高於[Microsoft
Windows的三倍以上](../Page/Microsoft_Windows.md "wikilink")。Gartner預計整個手機市場將「在2016年達到20億個裝置」，當中包括了Android\[409\]。描述統計數據，並於《[紐約時報](../Page/紐約時報.md "wikilink")》中寫道：「今天所售出的每兩台電腦中，就有一台正在運行Android。
「它」已成為了世上主導的電腦平臺。\[410\]」據高級分析軟件估計，2015年以Android作為智能手機安裝基礎的數量為18億個，這是預算全球智能手機總數的76％\[411\]\[412\]。根據2012年、2013年、2014年的銷售數字\[413\]
，Android擁有所有[流動作業系統中最高的安裝量](../Page/流動作業系統.md "wikilink")，並自2013年以來，成為了整體銷售最暢銷的作業系統\[414\]\[415\]\[416\]\[417\]\[418\]，接近所有PC的安裝數字\[419\]。

2015年9月，Google宣布Android每月活躍用戶數量為1.4億\[420\]\[421\]，這在2017年5月每月活躍用戶上升變成2億\[422\]\[423\]。

在2014年第二季，Android在全球智能手機出貨市場的份額為84.7％，創下了新紀錄\[424\]\[425\]。截至2016年第三季，全球市場份額已增長至87.5％\[426\]，拋離其主要競爭對手的[iOS只有](../Page/iOS.md "wikilink")12.1％的市場份額\[427\]。

，Google Play商店中的應用程式已下載超過65億次\[428\]。，Google
Play商店已發布了超過270萬個Android應用程式\[429\]，作業系統的成功使技術公司之間興起所謂的「」的一部分，並使其成為專利訴訟的目標\[430\]\[431\]。根據[StatCounter於](../Page/StatCounter.md "wikilink")2017年4月的報告，Android超越了Microsoft
Windows，成為最受歡迎的總互聯網使用的作業系統\[432\]\[433\]。從那時起它一直保持著多元化\[434\]。

### 平台使用率

### 平板電腦各作業系統比例

| 作業系統    | 2012年銷售量   | 2012年市佔率 | 2013年銷售量    | 2013年市佔率 |
| ------- | ---------- | -------- | ----------- | -------- |
| Android | 53,341,250 | 45.8%    | 120,961,445 | 61.9%    |
| iOS     | 61,465,632 | 52.8%    | 70,400,159  | 36.0%    |
| Windows | 1,162,435  | 1.0%     | 4,031,802   | 2.1%     |
| 其他      | 379,000    | 0.3%     | 41,598      | 0.1%     |

### 用户使用系統版本比例

此Android版本的細目表只根據截至2018年9月28日的七天內進入Google
Play商店的裝置資料\[435\]，因此，這些統計數據並不包括沒有進入Google
Play商店的各種Android分支的裝置，例如是亞馬遜的[Kindle
Fire](../Page/Kindle_Fire.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>發行日期</p></th>
<th><p>版本</p></th>
<th><p>作業系統名稱</p></th>
<th><p>API等級</p></th>
<th><p>運作所需</p></th>
<th><p>比率</p></th>
<th><p>發行裝置</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>style="text-align:center; |<strong><a href="../Page/Android歷史版本#Android_9_Pie_(API_28).md" title="wikilink">9</a></strong></p></td>
<td><p><a href="../Page/Android_Pie.md" title="wikilink">Pie</a></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>N/A</p></td>
<td><p><a href="../Page/Sony_Xperia_XZ3.md" title="wikilink">Sony Xperia XZ3</a>、<a href="../Page/Pixel_3.md" title="wikilink">Pixel 3、Pixel 3 XL</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_8.1_Oreo_(API_27).md" title="wikilink">8.1</a></strong></p></td>
<td><p><a href="../Page/Android_Oreo.md" title="wikilink">Oreo</a></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>5.8%</p></td>
<td><p><a href="../Page/Sony_Xperia_XZ1.md" title="wikilink">Sony Xperia XZ1</a>、<a href="../Page/Pixel_2.md" title="wikilink">Pixel 2、Pixel 2 XL</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_8.0_Oreo_(API_26).md" title="wikilink">8.0</a></strong></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>13.4%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_7.1_Nougat_(API_25).md" title="wikilink">7.1</a></strong></p></td>
<td><p><a href="../Page/Android_Nougat.md" title="wikilink">Nougat</a></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>10.3%</p></td>
<td><p><a href="../Page/LG_V20.md" title="wikilink">LG V20</a>、<a href="../Page/Pixel_(智能手機).md" title="wikilink">Pixel、Pixel XL</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_7.0_Nougat_(API_24).md" title="wikilink">7.0</a></strong></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>19.0%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_6.0_Marshmallow_(API_23).md" title="wikilink">6.0</a></strong></p></td>
<td><p><a href="../Page/Android_Marshmallow.md" title="wikilink">Marshmallow</a></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>21.6%</p></td>
<td><p><a href="../Page/Nexus_5X.md" title="wikilink">Nexus 5X</a>、<a href="../Page/Nexus_6P.md" title="wikilink">Nexus 6P</a>、<a href="../Page/HTC_One_A9.md" title="wikilink">HTC One A9</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_5.1_Lollipop_(API_22).md" title="wikilink">5.1</a></strong></p></td>
<td><p><a href="../Page/Android_Lollipop.md" title="wikilink">Lollipop</a></p></td>
<td></td>
<td><p><a href="../Page/Android_Runtime.md" title="wikilink">ART</a></p></td>
<td><p>14.7%</p></td>
<td><p><a href="../Page/Nexus_6.md" title="wikilink">Nexus 6</a>、<a href="../Page/Nexus_9.md" title="wikilink">Nexus 9</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_5.0_Lollipop_(API_21).md" title="wikilink">5.0</a></strong></p></td>
<td></td>
<td><p>ART 2.1.0</p></td>
<td><p>3.6%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_4.4_KitKat_(API_19).md" title="wikilink">4.4</a></strong></p></td>
<td><p><a href="../Page/Android_KitKat.md" title="wikilink">KitKat</a></p></td>
<td></td>
<td><p><a href="../Page/Dalvik虛擬機器.md" title="wikilink">Dalvik</a> (及ART 1.6.0)</p></td>
<td><p>7.8%</p></td>
<td><p><a href="../Page/Nexus_5.md" title="wikilink">Nexus 5</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_4.3_Jelly_Bean_(API_18).md" title="wikilink">4.3</a></strong></p></td>
<td><p><a href="../Page/Android_Jelly_Bean.md" title="wikilink">Jelly Bean</a></p></td>
<td></td>
<td><p><a href="../Page/Dalvik虛擬機器.md" title="wikilink">Dalvik</a></p></td>
<td><p>0.5%</p></td>
<td><p><a href="../Page/Nexus_7_(2013).md" title="wikilink">Nexus 7</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_4.2_Jelly_Bean_(API_17).md" title="wikilink">4.2</a></strong></p></td>
<td></td>
<td><p><a href="../Page/Dalvik虛擬機器.md" title="wikilink">Dalvik</a></p></td>
<td><p>1.6%</p></td>
<td><p><a href="../Page/Nexus_7_(2012).md" title="wikilink">Nexus 7</a>、<a href="../Page/Nexus_4.md" title="wikilink">Nexus 4</a>、<a href="../Page/Nexus_10.md" title="wikilink">Nexus 10</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_4.1_Jelly_Bean_(API_16).md" title="wikilink">4.1</a></strong></p></td>
<td></td>
<td><p><a href="../Page/Dalvik虛擬機器.md" title="wikilink">Dalvik</a></p></td>
<td><p>1.1%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_4.0_Ice_Cream_Sandwich_(API_14).md" title="wikilink">4.0</a></strong></p></td>
<td><p><a href="../Page/Android_Ice_Cream_Sandwich.md" title="wikilink">Ice Cream Sandwich</a></p></td>
<td></td>
<td><p><a href="../Page/Dalvik虛擬機器.md" title="wikilink">Dalvik</a></p></td>
<td><p>0.3%</p></td>
<td><p><a href="../Page/Galaxy_Nexus.md" title="wikilink">Galaxy Nexus</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong><a href="../Page/Android歷史版本#Android_2.3.3_Gingerbread_(API_10).md" title="wikilink">2.3</a></strong></p></td>
<td><p><a href="../Page/Android_Gingerbread.md" title="wikilink">Gingerbread</a></p></td>
<td></td>
<td><p><a href="../Page/Dalvik虛擬機器.md" title="wikilink">Dalvik</a> 1.4.0</p></td>
<td><p>0.3%</p></td>
<td><p><a href="../Page/Nexus_S.md" title="wikilink">Nexus S</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

截至2018年9月12日，以下是不同的Android版本的用戶比例\[436\]。

| 版本號 | 版本名稱               | [API](../Page/application_programming_interface.md "wikilink") | 用戶分佈  |
| --- | ------------------ | -------------------------------------------------------------- | ----- |
| 9   | Pie                | 28                                                             | 未有數據  |
| 8.1 | Oreo               | 27                                                             | 3.2%  |
| 8.0 | 26                 | 11.4%                                                          |       |
| 7.1 | Nougat             | 25                                                             | 10.5% |
| 7.0 | 24                 | 20.3%                                                          |       |
| 6.0 | Marshmallow        | 23                                                             | 22.7% |
| 5.1 | Lollipop           | 22                                                             | 15.4% |
| 5.0 | 21                 | 3.8%                                                           |       |
| 4.4 | KitKat             | 19                                                             | 8.6%  |
| 4.3 | Jelly Bean         | 18                                                             | 0.5%  |
| 4.2 | 17                 | 1.8%                                                           |       |
| 4.1 | 16                 | 1.2%                                                           |       |
| 4.0 | Ice Cream Sandwich | 15                                                             | 0.3%  |
| 2.3 | Gingerbread        | 10                                                             | 0.3%  |

，有78.4%的裝置已有[OpenGL ES 3.0或更高的配置](../Page/OpenGL_ES.md "wikilink")。

## 專利糾紛

### 盜版應用

一般來說，付費的Android應用程式很容易被[盜版](../Page/盜版.md "wikilink")\[437\]。在2012年5月的《[Eurogamer](../Page/Eurogamer.md "wikilink")》的訪問中，《[足球經理系列](../Page/足球經理系列.md "wikilink")》的開發者表示，在他們的遊戲「掌上足球經理」上，盜版玩家與合法玩家的比例為9：1\[438\]。然而，並非所有開發者都同意盜版率是一個問題；例如，在2012年7月，遊戲《》的開發者表示，他們的遊戲盜版率僅為12％，大部分盜版來自中國，那裡的人們無法從Google
Play購買應用程式\[439\]。

在2010年，Google發布了一款用於驗證在應用程式內使用授權購買的工具，但開發人員抱怨說對而言，這是還不足夠及微不足道的。Google回應指該工具，特別是其初始版本，旨在作為開發人員根據他們的需求進行修改和構建的示例框架，而並非完整的盜版解決方案\[440\]。Android的Jelly
Bean引入了加密付費應用程式的功能，因此它們只能在購買它們的裝置上使用\[441\]\[442\]。

### 專利爭議

《PC世界》杂志网站2011年9月22日报道，Android卷入1000多件专利诉讼中。

2011年8月，Google斥资125亿美元收购[摩托罗拉移动](../Page/摩托罗拉移动.md "wikilink")（Motorola
Mobility）及其拥有的约2.45万份专利组合\[443\]。

### 与甲骨文公司的Java纠纷

关于[甲骨文公司就Android所使用的开发语言平台](../Page/甲骨文公司.md "wikilink")[Java为最引人关注的权利纠纷事件](../Page/Java.md "wikilink")。

2010年8月，甲骨文公司就开始对Google无授权使用Java语言实现侵犯了公司的专利在美国加州北区地方法院提起控诉，\[444\]要求高达90亿美元的赔偿，其中牵涉了原供职于Sun公司的Java开发人员在转职Google开发Android的Java实现使用了原公司的实现，\[445\]API接口的实现是否具有专利版权性，Android的Java实现是否对甲骨文公司的Java移动平台系列产品做成冲击而形成不正当垄断等问题。

2012年5月的诉讼结果为陪审团支持Google的诉求，认为API只是系统或操作的方法，不受版权保护。2012年10月甲骨文公司上诉，2014年5月，[美国联邦巡回上诉法院认为API属于](../Page/美国联邦巡回上诉法院.md "wikilink")“计算机程序”仍受版权保护，判决Android侵犯了甲骨文公司Java的版权，但并不排除谷歌对其拥有合理使用性的权利。\[446\]\[447\]2014年10月Google向[美国最高法院申请调卷令](../Page/美国最高法院.md "wikilink")，请求最高院介入。2015年6月29日调卷令被拒绝，发往旧金山联邦法院进行审理。\[448\]\[449\]

2016年5月，[旧金山联邦法院陪审团认定Android实现Java的API命名结构属于合理使用](../Page/旧金山联邦法院.md "wikilink")，不构成侵权，最终判Google胜诉。\[450\]

2016年8月22日，Google在Android 7.0
Nougat中將專利的[JDK替換成開源方案的](../Page/JDK.md "wikilink")[OpenJDK](../Page/OpenJDK.md "wikilink")，以徹底解決Java的專利問題。\[451\]

## 分支平台

### Wear OS

[Wear_OS_by_Google_logo_only.svg](https://zh.wikipedia.org/wiki/File:Wear_OS_by_Google_logo_only.svg "fig:Wear_OS_by_Google_logo_only.svg")
Wear OS是專為智慧型手表等[可穿戴式設備所設計的一個Android系統分支](../Page/可穿戴式電腦.md "wikilink")。

### Android TV

Android TV是專為家用[電視所設計的一個Android系統分支](../Page/電視.md "wikilink")。

### Android Auto

Android Auto是專為汽車所設計的一個Android系統功能。

## 參見

  - [Android歷史版本](../Page/Android歷史版本.md "wikilink")

  - [Google Play](../Page/Google_Play.md "wikilink")

  -
  - [LIMEIME](../Page/LIMEIME.md "wikilink")

  - [Google Fuchsia](../Page/Google_Fuchsia.md "wikilink")

## 注释

## 參考文獻

## 外部链接

  -   - [Android开源計劃](http://source.android.com/)
      - [Android Developers](http://developer.android.com/)
      - [Android-x86](http://www.android-x86.org)

  - [Google Projects for
    Android](https://web.archive.org/web/20100208034950/http://code.google.com/android/)
    - [Google Code](../Page/Google_Code.md "wikilink")

  -
  -
{{-}}

[Android](../Category/Android.md "wikilink")
[Category:行動作業系統](../Category/行動作業系統.md "wikilink")
[Category:嵌入式Linux發行版](../Category/嵌入式Linux發行版.md "wikilink")
[Category:開放原始碼](../Category/開放原始碼.md "wikilink")
[Category:Google軟體](../Category/Google軟體.md "wikilink")

1.  [Android之父Andy Rubin：生而Geek](http://www.programmer.com.cn/3970/)
    作者：Wuzhimin本文來自《程式員》雜誌2010年9期

2.

3.

4.

5.

6.

7.

8.

9.  [1](http://www.ibm.com/developerworks/cn/opensource/os-android-devel／Android開發簡介)
    developerWorks IBM 2009年6月08日

10.

11.

12. [Android市場份額已達48%](http://www.canalys.com/newsroom/android-takes-almost-50-share-worldwide-smart-phone-market)Canalys數據2011/02/01

13. [Android過去1個月啟用裝置達10億台](http://www.techspot.com/news/57228-google-shows-off-new-version-of-android-announces-1-billion-active-monthly-users.html)Android數據2014/06/25

14. [Android overtakes Windows for first
    time](http://gs.statcounter.com/press/android-overtakes-windows-for-first-time)

15.

16.

17.

18.

19.

20.

21.
22.

23.

24.

25.

26.

27.

28.

29.

30. [谷歌十年前做安卓只是为了杀死微软Windows
    Mobile](https://new.qq.com/omn/20180928/20180928A0RA63.html).腾讯网.

31.

32.

33.

34.
35. [“乔布斯宿敌”：安卓之父的苦情创业史](https://www.sohu.com/a/257748630_100255965?spm=smmt.mt-it.fd-d.12.1538697600023WRl5Apr)

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53. [Google将Android 4.0.4源代码送交AOSP开源项目](http://cn.engadget.com/2012/04/01/google-android-4-0-4-android/)ENGADGET中国版2012

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.
88.

89.

90.

91.

92.

93.

94.
95.

96.

97.

98.

99.

100.

101.

102.

103.

104.
105.
106.

107.

108.

109.

110.
111.

112.

113.

114.

115.

116.

117.

118.

119.

120.

121.

122.

123.

124.

125.

126.

127.

128.
129.

130.

131.

132.
133.
134.
135.

136.

137.

138.

139.

140.

141.

142.

143.

144.

145.

146.

147.

148.

149.

150.

151.

152.

153.

154.

155.

156.

157.

158.
159.

160.

161.

162.

163.

164.

165.
166.
167.
168.

169.

170.

171.

172.

173.

174.

175.

176.

177.

178.

179.

180.

181.

182.

183.

184.

185.

186.
187.

188.

189.
190.

191. <http://elinux.org/Android_Mainlining_Project#Patch.2FFeature_Status_Chart>

192.

193.

194.

195.

196.

197.

198. 參見[rooting](../Page/Rooting_\(Android_OS\).md "wikilink")

199.

200.

201.

202.

203.

204.

205.

206.

207.

208.

209.

210.

211.

212.

213.
214.

215.
216.

217.

218. [Android gets a toybox](https://lwn.net/Articles/629362/)  on
     [lwn.net](../Page/lwn.net.md "wikilink") by Jake Edge (January 14,
     2015)

219.

220.

221.

222.

223.

224.

225.

226.

227.
228.

229.

230.

231.

232.
233.

234.

235.

236.

237.
238.
239.

240.

241.

242.

243.

244.

245.

246.

247.
248.
249.

250.

251.

252.

253.

254.

255.

256.

257.

258.

259.

260.

261.

262.

263.

264.

265.

266.

267.

268.

269.

270.

271.

272.

273. [SE for Android, SELinux
     Wiki](http://selinuxproject.org/page/SEforAndroid)

274. [The Case for SE
     Android](http://selinuxproject.org/~jmorris/lss2011_slides/caseforseandroid.pdf)

275.

276.

277.

278.

279.

280.

281.

282.

283.

284.

285.

286.

287.

288.

289.

290.

291.

292.

293.

294.

295.

296.

297. . [Mirror
     link](https://sites.google.com/a/android.com/opensource/posts/opensource).

298.

299.

300.

301.

302.

303.

304.

305.

306.

307.

308.
309.
310.
311.

312.
313.

314.

315.
316.

317.
318.

319.

320.

321.

322.

323.
324.
325.

326.

327.

328.

329.

330.

331. [Google推出Anroid
     Market在线软件平台](http://net.zol.com.cn/104/1048506.html)新浪科技2008年8月29日09:31

332. [Paid App Availability (In
     English)](http://www.google.com/support/androidmarket/bin/answer.py?hl=en&answer=143779&topic=1100168)
     Google Inc. 2011

333.
334. [Google Play Services更新
     大幅提高安全性](http://www.cnbeta.com/articles/274489.htm)
     cnBeta2014-03-02 10:12:37

335. [進一步整合？Android App及Play Store可執行在Chrome
     OS上](http://www.ithome.com.tw/news/106053)

336. [Google 高層：Android 不會與 Chrome OS 合併，而 Pixel 是為了打擊 iPhone - TechNews
     科技新報](https://ccc.technews.tw/2016/12/14/android-issues/)

337.

338. [SE for Android, SELinux
     Wiki](http://selinuxproject.org/page/SEforAndroid)

339. [The Case for SE
     Android](http://selinuxproject.org/~jmorris/lss2011_slides/caseforseandroid.pdf)

340.

341. ，列表中不僅有大量在中國大陸發售的機型，當中還有非常多在中國大陸本土廠商研發製造的裝置。目前支援列表需要下載。

342.

343. （見內圖）

344.

345.

346.

347.

348.

349.

350.

351.

352.
353.
354.

355.

356.
357.
358.

359.

360.

361.
362.

363.

364.

365.

366.

367.
368.

369.

370.

371.

372.

373.

374.

375.

376.
377.
378.

379.
380.

381.

382.

383.

384.

385.

386.

387.
388.

389.

390. <http://techcrunch.com/2013/07/01/android-led-by-samsung-continues-to-storm-the-smartphone-market-pushing-a-global-70-market-share/>

391.

392.

393.

394.

395.

396.

397.

398.

399.

400.

401.

402.
403.
404.

405.

406.

407.

408. /

409.

410.
411.

412.

413.

414.
415.

416.

417.

418.

419.

420.

421.

422.

423.

424.

425.

426.

427.

428.

429.

430.

431.

432.

433.

434.

435.

436.

437.

438.

439.

440.

441.

442.

443. [最近比较烦?细数Android成长之烦恼](http://zdc.zol.com.cn/251/2517372.html)王彦恩中关村在线2011年9月29日

444.

445.

446. [谷歌惹麻烦：被判侵犯甲骨文Java版权](http://news.zol.com.cn/454/4548142.html)

447. [Java专利侵权案：甲骨文赢得对谷歌的上诉](http://tech.sina.com.cn/i/2014-05-10/10339370852.shtml)

448.

449.
450.

451.