[Come_unto_me,_ye_opprest.jpg](https://zh.wikipedia.org/wiki/File:Come_unto_me,_ye_opprest.jpg "fig:Come_unto_me,_ye_opprest.jpg")企图炸毁[纽约](../Page/纽约.md "wikilink")[自由女神像的宣传画](../Page/自由女神像.md "wikilink")，1919年\]\]
**紅色恐慌**（英語：Red
Scare）是指於[美國興起的](../Page/美國.md "wikilink")[反共產主義風潮](../Page/反共產主義.md "wikilink")，分为两段。第一段自1917年[俄国十月革命爆发后延续至](../Page/俄国十月革命.md "wikilink")1920年，恐慌受到欧洲的影响，美国[工人以及](../Page/工人.md "wikilink")[社会主义者可能爆发政治](../Page/社会主义.md "wikilink")[激进主义或](../Page/激进主义.md "wikilink")[革命运动](../Page/革命.md "wikilink")。第二段是指开始于1947年，并几乎贯穿全部1950年代的[麥卡錫主義後遺症](../Page/麥卡錫主義.md "wikilink")。恐慌来自于美国国内外[共产主义者对美国社会的影响以及对联邦政府的](../Page/共产主义.md "wikilink")[间谍行为](../Page/间谍.md "wikilink")。在此後遺症下，美國政府制定形形色色的反共政策，使國民接受了國家安全與反共密不可分的思想觀念。而此觀念以美國共和黨参议员[約瑟夫·麥卡錫發起的](../Page/約瑟夫·麥卡錫.md "wikilink")[麥卡錫主義有密切的關係](../Page/麥卡錫主義.md "wikilink")。

## 第一次红色恐慌

[第一次世界大战后期](../Page/第一次世界大战.md "wikilink")，[欧洲](../Page/欧洲.md "wikilink")[无政府主义者和](../Page/无政府主义.md "wikilink")[左翼政治的躁动以及暴力加剧了现存国家的社会和政治紧张局势](../Page/左翼政治.md "wikilink")。

## 第二次红色恐慌

麥卡錫主義是在1950年代初，由約瑟夫·麥卡錫煽起的，在美國全國性的反共“[十字軍運動](../Page/十字軍.md "wikilink")”。以他為首腦的該運動大肆渲染[共產黨侵入](../Page/共產黨.md "wikilink")[美國政府和美國](../Page/美國政府.md "wikilink")[輿論界](../Page/輿論.md "wikilink")，促使成立“非美調查委員會”，在[文藝界和政府部門煽動人們互相揭發](../Page/文藝.md "wikilink")。許多著名[演員如](../Page/演員.md "wikilink")[查理·卓別林和發明](../Page/查理·卓別林.md "wikilink")[原子彈的](../Page/原子彈.md "wikilink")[科學家](../Page/科學家.md "wikilink")[羅伯特·奧本海默等都受到迫害](../Page/羅伯特·奧本海默.md "wikilink")，被指控爲向[蘇聯透露機密和爲蘇聯充當](../Page/蘇聯.md "wikilink")[間諜](../Page/間諜.md "wikilink")。1953年6月19日，科學家[艾瑟爾與朱利葉斯·羅森堡夫婦爲此被判上](../Page/艾瑟爾與朱利葉斯·羅森堡夫婦.md "wikilink")[電椅](../Page/電椅.md "wikilink")[死刑](../Page/死刑.md "wikilink")。

1954年，麥卡錫主義達到頂端。他指控[美軍和政府官員從事顛覆活動](../Page/美軍.md "wikilink")，爲此舉行了長達36天的[聽證會](../Page/聽證會.md "wikilink")，同時向全國進行[電視直播](../Page/電視.md "wikilink")。美國國內外的輿論開始指責他是“蠱惑民心的煽動家”。11月中期選舉，共和黨失去[參議院的多數](../Page/美國參議院.md "wikilink")，麥卡錫被免去非美調查委員會主席的職務。12月2日，參議院以67票對22票通過決議，正式譴責麥卡錫“違反參議院傳統”的行爲。雖說麥卡錫主義時代結束，不過紅色恐慌影響層面甚廣，「反共為美國的惟一選擇」成為日後多年的美國國家政策。

## 参见

  - [卧倒并掩护](../Page/卧倒并掩护.md "wikilink")

{{-}}

[Category:美國歷史
(1918年-1945年)](../Category/美國歷史_\(1918年-1945年\).md "wikilink")
[Category:美國歷史
(1945年-1964年)](../Category/美國歷史_\(1945年-1964年\).md "wikilink")
[Category:美國反共主義](../Category/美國反共主義.md "wikilink")
[Category:20世纪美国政治](../Category/20世纪美国政治.md "wikilink")