[Shitao-autoportrait.jpg](https://zh.wikipedia.org/wiki/File:Shitao-autoportrait.jpg "fig:Shitao-autoportrait.jpg")
**石涛**（），清初画家，[原姓](../Page/原姓.md "wikilink")**朱**，[名](../Page/名.md "wikilink")**若极**，[广西](../Page/广西.md "wikilink")[桂林人](../Page/桂林.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[安徽](../Page/安徽.md "wikilink")[鳳陽](../Page/鳳陽.md "wikilink")，[小字](../Page/小字.md "wikilink")**阿长**，别号很多，如**大涤子**、**清湘老人**、**苦瓜和尚**、**瞎尊者**，法号有**元济**、**原济**等。与[弘仁](../Page/弘仁.md "wikilink")、[髡残](../Page/髡残.md "wikilink")、[朱耷合称](../Page/朱耷.md "wikilink")“清初四僧”。

石涛是中国绘画史上一位十分重要的人物，他既是绘画实践的探索者、革新者，又是艺术理论家。

## 生平

石涛是[明靖江王](../Page/明.md "wikilink")[朱赞仪的十世孙](../Page/朱赞仪.md "wikilink")，[朱亨嘉的长子](../Page/朱亨嘉.md "wikilink")。清初，其父朱亨嘉企圖稱監國失敗被唐王處死，若極被宦官（即喝濤和尚）救出，由[桂林逃到](../Page/桂林.md "wikilink")[全州](../Page/全州.md "wikilink")，在[湘山寺削发为僧](../Page/湘山寺.md "wikilink")，改名**石涛**。

石涛一生浪迹天涯，云游四方，在[安徽](../Page/安徽.md "wikilink")[宣城](../Page/宣城.md "wikilink")[敬亭山及](../Page/敬亭山.md "wikilink")[黄山住了](../Page/黄山.md "wikilink")10年左右，结交画家，后来到了江宁（[南京](../Page/南京.md "wikilink")）。晚年棄僧還俗，成為職業畫家。[清圣祖于康熙二十三年](../Page/清圣祖.md "wikilink")（1684年）、二十八年（1689年）两次南巡时，他在南京、[扬州两次接驾](../Page/扬州.md "wikilink")，献诗画，自称“臣僧”。后又北上京师，结交达官贵人，为他们作画。但终不得仕进，返回南京。最后定居扬州，以卖画为生，有大量作品傳世，但偽作亦多。他還总结与整理他多年来绘画实践的经验与理论，使他晚年的作品更加成熟和丰富多采。石濤死，[高翔每歲春掃其墓](../Page/高翔.md "wikilink")。著有《[画语录](../Page/画语录.md "wikilink")》十八章。

## 画风

石涛擅长山水，常体察自然景物，主张“笔墨当随时代”，画山水者应“脱胎于山川”，“搜尽奇峰打草稿”，进而“法自我立”。所画的山水、兰竹、花果、人物，讲求新格，构图善于变化，笔墨笔墨恣肆，意境苍莽新奇，一反当时仿古之風。[鄭板橋蘭竹畫均取法石濤](../Page/鄭板橋.md "wikilink")。[張大千生前收藏多件石濤書畫](../Page/張大千.md "wikilink")，最多時約五百幅。張大千亦擅長仿石濤作品，幾可亂真。

## 作品图片

<File:10000points> mechants.jpg| <File:Rive> aux fleurs de pecher.jpg|
<File:Shitao-Qinhuai.jpg>| <File:Shitao02.jpg>|

<center>

**黄山八胜图之一**, (日)泉屋博古馆藏

</center>

<File:Shitao03.jpg>|

<center>

**杜甫诗意图之一**

</center>

<File:Shitao04.jpg>|

<center>

**杜甫诗意图之二**

</center>

<File:Shitao05.jpg>|

<center>

**杜甫诗意图之三**

</center>

<File:Shitao07.jpg>|

<center>

**墨兰图**

</center>

<File:Shitao0.jpg>|

<center>

**爱莲图**

</center>

<File:Cascade> au mont Lu.jpg| <File:Guimet> shitao monts jinting.jpg|

<center>

</center>

## 參考書目

  - 鄭拙廬：《石濤研究》
  - 朱良志：《傳世石濤款作品真偽考》
  - 方闻：〈[石涛《归棹》册页研究](http://www.nssd.org/articles/article_read.aspx?id=23414600)〉。
  - 賴賢宗：〈[禪藝合流與石濤畫論的禪美學](http://enlight.lib.ntu.edu.tw/FULLTEXT/JR-MAG/mag202017.pdf)〉。

[Category:广西画家](../Category/广西画家.md "wikilink")
[Category:清朝画家](../Category/清朝画家.md "wikilink")
[Category:明朝宗室](../Category/明朝宗室.md "wikilink")
[R若](../Category/朱姓.md "wikilink")