**[晚清四大谴责小说](../Page/晚清.md "wikilink")**，是指[刘鹗](../Page/刘鹗.md "wikilink")《[老残游记](../Page/老残游记.md "wikilink")》、[李寶嘉](../Page/李寶嘉.md "wikilink")《[官場現形記](../Page/官場現形記.md "wikilink")》、[吳趼人](../Page/吳趼人.md "wikilink")《[二十年目睹之怪現狀](../Page/二十年目睹之怪現狀.md "wikilink")》和[曾樸](../Page/曾樸.md "wikilink")《[孽海花](../Page/孽海花.md "wikilink")》。

## 起源

[鲁迅在](../Page/鲁迅.md "wikilink")1923年出版的《[中国小说史略](../Page/中国小说史略.md "wikilink")》第二十八篇：《清末之譴責小說》中评论[刘鹗的](../Page/刘鹗.md "wikilink")《[老残游记](../Page/老残游记.md "wikilink")》、[李寶嘉的](../Page/李寶嘉.md "wikilink")《[官場現形記](../Page/官場現形記.md "wikilink")》、[吴沃堯的](../Page/吴沃堯.md "wikilink")《[二十年目睹之怪現狀](../Page/二十年目睹之怪現狀.md "wikilink")》和[曾樸的](../Page/曾樸.md "wikilink")《[孽海花](../Page/孽海花.md "wikilink")》等四部小说时指出，在[光绪](../Page/光绪.md "wikilink")[庚子](../Page/庚子.md "wikilink")（1900年）-{zh-hans:后;zh-hant:後}-，谴责小说特别盛行；其产生的历史背景，在[戊戌变法失败](../Page/戊戌变法.md "wikilink")，庚子[义和团运动爆发](../Page/义和团运动.md "wikilink")，“群乃知政府不足与图，顿有掊击之意矣。其在小说，则揭发伏藏，显其弊恶……虽命意在于匡世，似与[讽刺小说同伦](../Page/讽刺小说.md "wikilink")，而辞气浮躁，笔无藏锋，甚且过甚其辞，以合时人嗜好，则其度量技术之相去亦远矣，故别谓之**谴责小说**”。鲁迅在1920年最早将此类小说称为“谴责小说”以别于《[儒林外史](../Page/儒林外史.md "wikilink")》一类[讽刺小说](../Page/讽刺.md "wikilink")\[1\]。此后，此四本书统称为：“晚清四大譴責小說”。

四十年后，[夏志清在](../Page/夏志清.md "wikilink")1961年出版的《中國現代小說史》也指出，“古代讀書人受到[孔孟教育的影響](../Page/孔孟.md "wikilink")，個個都覺得應該甘為人民喉舌，揭露朝廷、社會上所見到的黑暗現象。但他們生活在皇帝[專制政體下](../Page/專制.md "wikilink")，大半變得‘明哲保身’、‘溫柔敦厚’起來；也有很多人無意官場，或者官場失意，人變得消極，求個‘怡然自得’就夠了；到了晚清末年，專制政體即將瓦解，讀書人才真敢放膽寫出他們心裏要說的話來，所謂‘譴責小說’的盛行，不是沒有道理的。”“大體說來，中國現代文學是揭露黑暗，諷刺社會，維護人的[尊嚴的](../Page/尊嚴.md "wikilink")[人道主義文學](../Page/人道主義.md "wikilink")。”\[2\]

## 四大谴责小说

  - 《[老殘遊記](../Page/老殘遊記.md "wikilink")》，作者[劉鶚](../Page/劉鶚.md "wikilink")（1857年－1909年），全书20回，描述[江湖](../Page/江湖.md "wikilink")[郎中](../Page/郎中.md "wikilink")「老殘」的故事，艺术成就最高。[胡適認為此書描寫工夫最精到](../Page/胡適.md "wikilink")。
  - 《[官場現形記](../Page/官場現形記.md "wikilink")》，作者[李寶嘉](../Page/李寶嘉.md "wikilink")（1867年－1907年），全书60[回](../Page/章回小說.md "wikilink")，由各章獨立的短篇故事集結而成。
  - 《[二十年目睹之怪現狀](../Page/二十年目睹之怪現狀.md "wikilink")》，作者[吳沃堯](../Page/吳沃堯.md "wikilink")（1866年－1910年），全书108回，反映了[中法戰爭](../Page/中法戰爭.md "wikilink")-{zh-hans:后;zh-hant:後}-二十年中國社會現象。
  - 《[孽海花](../Page/孽海花.md "wikilink")》，作者[曾樸](../Page/曾樸.md "wikilink")（1872年－1935年），全书30回，附录5回。即[赛金花與状元金雯青的故事](../Page/赛金花.md "wikilink")。

## 注釋

<div class="references-small">

<references />

</div>

## 外部链接

{{-}}

[Category:清朝小說](../Category/清朝小說.md "wikilink")
[Category:政治小說](../Category/政治小說.md "wikilink")

1.  《[魯迅全集](../Page/魯迅全集.md "wikilink")》9：《[中國小說史略](../Page/中國小說史略.md "wikilink")》第二十八篇：《清末之譴責小說》，1920年完成讲义，1923年最先由北新书局出版
2.  夏志清著，劉紹銘編譯《中國現代小說史》（A History of Modern Chinese Fiction） 1961