**北斗市**（）是位于[北海道](../Page/北海道.md "wikilink")[渡島綜合振興局中部的城市](../Page/渡島綜合振興局.md "wikilink")，是北海道最早栽種[水稻的地區](../Page/水稻.md "wikilink")\[1\]，轄區東南部以平原為主，西部是山區地形，南側面對[函館灣](../Page/函館灣.md "wikilink")。於2006年2月1日由[上磯郡](../Page/上磯郡.md "wikilink")[上磯町和](../Page/上磯町.md "wikilink")[龜田郡](../Page/龜田郡.md "wikilink")[大野町合併成立](../Page/大野町_\(北海道\).md "wikilink")，是渡島支廳第二個誕生的城市，人口在廳內僅次於[函館市](../Page/函館市.md "wikilink")。

在城市成立之時，由於希望新成立的城市在未來可以像[北斗七星一樣散發光芒](../Page/北斗七星.md "wikilink")，成為其他城市的典範，因此以「北斗」作為新城市的名稱。\[2\]

其[日文發音與位於日本](../Page/日文.md "wikilink")[山梨縣的](../Page/山梨縣.md "wikilink")[北杜市同樣皆為](../Page/北杜市.md "wikilink")「Hokuto-shi」。

## 歷史

  - 1600年代：[松前藩藩主指示在文月地區進行](../Page/松前藩.md "wikilink")[稻作的試耕](../Page/稻作.md "wikilink")，並在1692年的文獻上記載有「產米十袋」的紀錄，成為北海道最早的稻作生產紀錄。\[3\]
  - 1900年7月1日：
      - [上磯郡上磯村](../Page/上磯郡.md "wikilink")、中野村、清川村、谷好村、富川村合併為上磯村，成為北海道一級村。
      - [龜田郡大野村](../Page/龜田郡.md "wikilink")、本鄉村、市渡村、文月村、千代田村、一本木村合併為大野村，成為北海道一級村。
  - 1906年4月1日：[茂別村成立為北海道二級村](../Page/茂別村.md "wikilink")。
  - 1918年1月1日：上磯村改制為[上磯町](../Page/上磯町.md "wikilink")。
  - 1955年4月1日：茂別村併入上磯町。
  - 1957年1月1日：大野村改制為[大野町](../Page/大野町_\(北海道\).md "wikilink")。
  - 2006年2月1日：上磯町和大野町合併為北斗市。

## 產業

[農業和](../Page/農業.md "wikilink")[漁業是主要產業](../Page/漁業.md "wikilink")，由於緊鄰[函館市](../Page/函館市.md "wikilink")，成為函館市的[衛星城市](../Page/衛星城市.md "wikilink")，近年來東部地區野開始開發為函館市的工業區。

## 交通

### 機場

  - [函館機場](../Page/函館機場.md "wikilink")（位於[函館市](../Page/函館市.md "wikilink")）

### 鐵路

  - [道南漁火鐵道](../Page/道南漁火鐵道.md "wikilink")
      - [道南漁火鐵道線](../Page/道南漁火鐵道線.md "wikilink")：[渡島當別車站](../Page/渡島當別車站.md "wikilink")
        - [茂邊地車站](../Page/茂邊地車站.md "wikilink") -
        [上磯車站](../Page/上磯車站.md "wikilink") -
        [清川口車站](../Page/清川口車站.md "wikilink") -
        [久根別車站](../Page/久根別車站.md "wikilink") -
        [東久根別車站](../Page/東久根別車站.md "wikilink") -
        [七重濱車站](../Page/七重濱車站.md "wikilink")
  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [北海道新幹線](../Page/北海道新幹線.md "wikilink")、[函館本線](../Page/函館本線.md "wikilink")：[新函館北斗車站](../Page/新函館北斗車站.md "wikilink")

### 道路

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li><a href="../Page/函館江差自動車道.md" title="wikilink">函館江差自動車道</a>：大野交流道、上磯交流道</li>
</ul>
<dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道227號.md" title="wikilink">國道227號</a></li>
<li><a href="../Page/國道228號.md" title="wikilink">國道228號</a></li>
</ul></td>
<td><dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道29號上磯厚澤部線</li>
<li>北海道道96號上磯峠下線</li>
<li>北海道道100號函館上磯線</li>
</ul>
<dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道262號渡島大野停車場線</li>
<li>北海道道530號上磯停車場線</li>
<li>北海道道676號七飯大野線</li>
<li>北海道道756號大野上磯線</li>
<li>北海道道969號大野大中山線</li>
</ul></td>
</tr>
</tbody>
</table>

### 巴士

  - 函館巴士

## 觀光景點

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="觀光">觀光</h3>
<ul>
<li>釜的仙境</li>
<li>陣屋跡櫻花街道</li>
<li>七重濱溫泉</li>
<li>苦修會修道院：日本最早的男子苦修會修道院</li>
<li>三木露風的詩碑</li>
<li>男爵資料館</li>
<li>台風海難者慰靈碑</li>
<li>七重濱海濱公園</li>
<li>佐佐木観光鳶尾花園</li>
<li>北海道水田發祥之地碑</li>
<li>八郎沼公園</li>
<li>匠之森公園</li>
</ul></td>
<td><h3 id="文化遺產">文化遺產</h3>
<ul>
<li>熊谷家住宅（登陸有形文化財）</li>
<li>松前藩戶切地陣屋跡（國指定史跡）</li>
<li>茂別館跡（國指定史跡）</li>
<li>茂邊地遺跡 - 人形裝飾異形注口土器（國家重要文化財、收藏於<a href="../Page/東京國立博物館.md" title="wikilink">東京國立博物館</a>）</li>
<li>上磯奴（北斗市指定無形民俗文化財）</li>
<li>有川天滿林（北斗市指定無形民俗文化財）</li>
<li>開拓使三角測量一本木基準點（北海道指定史跡）</li>
<li>大野祗園林（北斗市指定無形民俗文化財）</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 高等學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.kamiiso.hokkaido-c.ed.jp/">道立北海道上磯高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.hakodatesuisan.hokkaido-c.ed.jp/">道立北海道函館水産高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.oononougyou.hokkaido-c.ed.jp/">道立北海道大野農業高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20080113121818/http://www.city.hokuto.hokkaido.jp/gakko/hamatyu/default/default.htm">北斗市立濱分中學校</a></li>
<li>北斗市立石別中學校</li>
</ul></td>
<td><ul>
<li><a href="https://web.archive.org/web/20080101101237/http://www.city.hokuto.hokkaido.jp/gakko/kamityu/default.htm">北斗市立上磯中學校</a></li>
<li>北斗市立茂邊地中學校</li>
</ul></td>
<td><ul>
<li><a href="http://www.city.hokuto.hokkaido.jp/school/ono-chu/index.htm">北斗市立大野中學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20071219231456/http://www.city.hokuto.hokkaido.jp/gakko/ishisyo/index.html">北斗市立石別小學校</a></li>
<li><a href="https://web.archive.org/web/20080107073521/http://www.city.hokuto.hokkaido.jp/gakko/okisyo/default.htm">北斗市立沖川小學校</a></li>
<li><a href="http://www.city.hokuto.hokkaido.jp/gakko/kamisyo/">北斗市立上磯小學校</a></li>
<li><a href="http://www.city.hokuto.hokkaido.jp/gakko/kunesyo/p/default.htm">北斗市立久根別小學校</a></li>
</ul></td>
<td><ul>
<li>北斗市立谷川小學校</li>
<li><a href="https://web.archive.org/web/20080107073459/http://www.city.hokuto.hokkaido.jp/gakko/hamasyo/default.htm">北斗市立濱分小學校</a></li>
<li>北斗市立茂邊地小學校</li>
<li>北斗市立市渡小學校</li>
</ul></td>
<td><ul>
<li><a href="http://www.city.hokuto.hokkaido.jp/school/ono-syo/index.htm">北斗市立大野小學校</a></li>
<li><a href="https://web.archive.org/web/20080107073541/http://www.city.hokuto.hokkaido.jp/school/shima-syo/index.html">北斗市立島川小學校</a></li>
<li><a href="https://web.archive.org/web/20080107073526/http://www.city.hokuto.hokkaido.jp/school/hagi-syo/index.htm">北斗市立萩野小學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 本地出身的名人

  - [熊谷孝太郎](../Page/熊谷孝太郎.md "wikilink")：[業餘](../Page/業餘.md "wikilink")[攝影家](../Page/攝影家.md "wikilink")

  - [三橋美智也](../Page/三橋美智也.md "wikilink")：[歌手](../Page/歌手.md "wikilink")

  - ：[女演員](../Page/女演員.md "wikilink")

  - ：[DJ](../Page/DJ.md "wikilink")

  - ：歌手

  - [佐佐木翔](../Page/佐佐木翔.md "wikilink")：[羽毛球選手](../Page/羽毛球.md "wikilink")

  - [野口裕史](../Page/野口裕史.md "wikilink")：田徑[鉛球選手](../Page/鉛球.md "wikilink")

  - [成田可奈繪](../Page/成田可奈繪.md "wikilink")：短距離徑賽選手，全日本中學連續兩年[冠軍](../Page/冠軍.md "wikilink")

  - ：[相撲力士](../Page/相撲力士.md "wikilink")、[小結](../Page/小結.md "wikilink")，相撲史上第一位從初土俵後連續出場1000回

  - [川原由美子](../Page/川原由美子.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")

  - [佐佐木明](../Page/佐佐木明.md "wikilink")：[高山滑雪選手](../Page/高山滑雪.md "wikilink")

  - ：前[賽馬騎士](../Page/賽馬騎士.md "wikilink")

  - ：[東京交響樂團前常任](../Page/東京交響樂團.md "wikilink")[指揮](../Page/指揮.md "wikilink")

  - ：

## 參考資料

## 外部連結

  - [北斗市觀光協會](https://web.archive.org/web/20071222135407/http://www5.ncv.ne.jp/~aid-03/kanko/index.html)

  - [北斗市社會福祉協會](https://web.archive.org/web/20071213183643/http://www.hokutosyakyo.com/)

<!-- end list -->

1.  上磯町、大野町合併協議會 - 北斗市城市發展計畫
    <http://www.city.hokuto.hokkaido.jp/gappei-hp2/matidukurikeikaku.pdf>


2.  上磯町、大野町合併協議會 -
    合併協議會報告12月號<http://www.city.hokuto.hokkaido.jp/gappei-hp2/dayori12-2.pdf>

3.