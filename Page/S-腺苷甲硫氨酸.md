***S*-腺苷甲硫氨酸**（又名***S*-腺苷蛋氨酸**，，缩写为**SAM**）带有一个活化了的[甲基](../Page/甲基.md "wikilink")（右图标注），是一种参与甲基转移反应的[辅酶](../Page/辅酶.md "wikilink")，存在于所有的[真核细胞中](../Page/真核细胞.md "wikilink")。在失去该活化甲基后，SAM变为[S-腺苷-L-高半胱氨酸](../Page/S-腺苷-L-高半胱氨酸.md "wikilink")（SAH）。

*S*-腺苷甲硫氨酸最早于1952年被科学家（[Cantoni](../Page/Cantoni.md "wikilink")）发现。它由[三磷酸腺苷（ATP）和](../Page/三磷酸腺苷.md "wikilink")[甲硫氨酸在细胞内通过蛋氨酸腺苷基转移酶](../Page/甲硫氨酸.md "wikilink")（Methionine
Adenosyl
Transferase）催化合成，在作为辅酶参与甲基转移反应的时候丢失一个甲基变成*S*-腺苷基高半胱氨酸。大部分的*S*-腺苷甲硫氨酸在肝脏生成。

在高级有机体内，40种以上的合成代谢或分解代谢的化学反应涉及将*S*-腺苷甲硫氨酸的甲基转移到[核酸](../Page/核酸.md "wikilink")、[蛋白质和](../Page/蛋白质.md "wikilink")[脂肪等底物上](../Page/脂肪.md "wikilink")。

## 作为膳食补充品

研究表明，定期食用*S*-腺苷甲硫氨酸可抗抑郁，肝脏疾病，和关节炎／关节疼痛\[1\]。在美国市场上用SAM-e的名字按营养补品销售，有改善情绪、保养肝脏和舒适关节的功效。
随着[营养补充品在大众消费中增长](../Page/营养补充品.md "wikilink")，用于治疗目的的*S*-腺苷甲硫氨酸亦有增加，特别是在1999年[营养补充品健康及教育法通过以后](../Page/营养补充品健康及教育法.md "wikilink")。这部法令允许使用和分配*S*-腺苷甲硫氨酸做为一种可于柜台出售的食物添加剂，并使其通过了[FDA的限制性法令](../Page/FDA.md "wikilink")。*S*-腺苷甲硫氨酸已被很多研究性文章证明在治疗上对[肝部疾病](../Page/肝部疾病.md "wikilink")、[情绪失调与](../Page/情绪失调.md "wikilink")[骨质疏松症有效](../Page/骨质疏松症.md "wikilink")。由于其结构上的不稳定性，*S*-腺苷甲硫氨酸的稳定的盐形式成为其药用的主要形式。尽管其稳定的盐形式已经被制作出来，SAM仍然是易于分解的，因此其售卖者经常要求服食较可能被真正吸收的更多的剂量。广泛报道的副作用是肠胃不适，但是有[双相障碍病史的人有发生狂躁症的风险](../Page/躁鬱症.md "wikilink")。治疗剂量的范围是800[毫克](../Page/毫克.md "wikilink")／天到1600毫克／天。

## 參見

  - [DNA甲基转移酶](../Page/DNA甲基转移酶.md "wikilink")
  - [甲基化](../Page/甲基化.md "wikilink")
  - [S-腺苷-L-高半胱氨酸](../Page/S-腺苷-L-高半胱氨酸.md "wikilink")

## 外部連結

  -
  -
  - [S-Adenosyl Methionine as a food supplement: Pros and
    Cons](https://web.archive.org/web/20020420035537/http://stoneclinic.com/sam_e.htm)
    S-腺苷基蛋氨酸作为补品的利与弊

  - [S-Adenosyl methionine (SAMe) versus celecoxib for the treatment of
    osteoarthritis symptoms: A double-blind cross-over
    trial](http://www.biomedcentral.com/1471-2474/5/6)

  - [S-Adenosyl-Methionine in Depression: A Comprehensive Review of the
    Literature](https://web.archive.org/web/20050126224719/http://www.biomedcentral.com/1523-3812/5/460/abstract)
    (Abstract only)

  - [Testimony before the House of Representatives Committee on
    Government Reform Hearing on Dietary Supplements. (HRG Publication
    \#1560)](http://www.citizen.org/publications/release.cfm?ID=6763)

  - [About.com SAM-e Resource
    Index](http://arthritis.about.com/od/same/)

[Category:辅酶](../Category/辅酶.md "wikilink")
[Category:膳食补充品](../Category/膳食补充品.md "wikilink")
[Category:双相障碍生物学](../Category/双相障碍生物学.md "wikilink")
[Category:锍化合物](../Category/锍化合物.md "wikilink")

1.  [1](http://www.ahcpr.gov/clinic/epcsums/samesum.htm)