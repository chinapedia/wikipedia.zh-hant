| native_name_lang = it | settlement_type = {{\#if:
|[意大利區劃](意大利行政區劃.md "wikilink")}} |
image_skyline =  | image_alt = | image_caption =  | image_flag =  |
flag_alt = | image_shield =  | shield_size =  | shield_alt = |
image_blank_emblem =  | blank_emblem_size =  | blank_emblem_type =
| blank_emblem_alt = | nickname = | motto = | anthem =  | image_map =
 | mapsize = | map_alt = | map_caption = | pushpin_map = |
pushpin_label_position = | pushpin_map_alt = | pushpin_mapsize = |
pushpin_map_caption = | latd = |latm = |lats = |latNS = N | longd =
|longm = |longs = |longEW = E | coor_pinpoint = | coordinates_type = |
coordinates_display = inline,title | coordinates_footnotes = |
subdivision_type = 國家 | subdivision_name = [意大利](意大利.md "wikilink") |
established_title = | established_date = | founder = | named_for = |
seat_type = 首府 | seat = []({{{capital}}}.md "wikilink") |
government_footnotes = | government_type = | leader_party =  |
leader_title = 首腦 | leader_name =  | leader_title1 = | leader_name1
= | assembly_majority = []({{{assembly_majority}}}.md "wikilink")
majority | assembly_title = 議會 | assembly_seats = {{\#if:| seats}} |
total_type = | unit_pref = | area_magnitude = | area_footnotes = |
area_total_km2 =  | elevation_footnotes = | elevation_max_m = |
elevation_min_m = | population_footnotes =  | population_total =  |
population_as_of =  | population_density_km2 = auto |
population_demonym =  | population_note = | population_blank1_title
= {{\#if: |官方語言{{\#if: |  |  }} }} | population_blank1 = {{\#if: | }} |
demographics_type1 = {{\#if: |市民}} | demographics1_footnotes = {{\#if:
| {{\#if: |  |  }} }} | demographics1_title1 = {{\#if: |意大利人}} |
demographics1_info1 = {{\#if: | }} | demographics1_title2 = {{\#if: |
}} | demographics1_info2 = {{\#if: | }} | demographics1_title3 =
{{\#if: | }} | demographics1_info3 = {{\#if: | }} | timezone1 =
[CET](歐洲中部時間.md "wikilink") | utc_offset1 = +1 | timezone1_DST =
[CEST](歐洲中部夏令時間.md "wikilink") | utc_offset1_DST = +2 |
postal_code_type = | postal_code = | area_code_type = | area_code
= | blank_name_sec1 = GDP | blank_info_sec1 = €億（） |
blank1_name_sec1 = {{\#if: |人均GDP}} | blank1_info_sec1 = {{\#if: |€
()}} | blank2_name_sec1 = | blank2_info_sec1 = | blank_name_sec2 =
[NUTS地區](歐盟一級NUTS.md "wikilink") | blank_info_sec2 =  | website =  |
footnotes = }}<noinclude>  </noinclude>