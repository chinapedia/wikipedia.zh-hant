**Crytek**是一个[電子遊戲開發商](../Page/電子遊戲開發商.md "wikilink")，由Yerli兄弟于1999年建立。Crytek總部位于[德國](../Page/德國.md "wikilink")[法蘭克福](../Page/法蘭克福.md "wikilink")，目前有一家位於[烏克蘭](../Page/烏克蘭.md "wikilink")[基輔的分部](../Page/基輔.md "wikilink")。\[1\]著名作品為遊戲《[極地戰嚎](../Page/極地戰嚎.md "wikilink")》及《[孤岛危机系列](../Page/孤岛危机系列.md "wikilink")》和其遊戲引擎[CryENGINE](../Page/CryENGINE.md "wikilink")。

## 得獎

2002年，Crytek因極地戰嚎遊戲獲得的獎項：

  - [E3](../Page/E3.md "wikilink") Best Action Runner up
  - E3 Technological Excellence Runner up
  - E3 Best Surprise Runner up

2003年，極地戰嚎釋出並獲得：

  - [GameSpot](../Page/GameSpot.md "wikilink") "The top ten of PC GAME
    E3
  - [ECTS](../Page/ECTS.md "wikilink") Best PC Game
  - [IGN](../Page/IGN.md "wikilink") E3 Best Action Runner up
  - IGN E3 Technological Excellence Runner up

2004年，Crytek獲得「企業卓越獎項」（Industry Excellence Awards）：

  - Best New European Studio
  - Best Independent Developer
  - Best New PC IP - Far Cry\[2\]

數個月後，得到四項德國開發者獎項：

  - Best Full Price Game
  - Best Game Design
  - Best Technology
  - Best Graphic\[3\]

同年7月，Crytek在五個獎項獲得亞軍：

  - Best PC game
  - Use of online
  - Art and Sound
  - New Euro Studio (Debut Title)
  - Independent Developer.

2005年，Crytek獲得[GDC](../Page/GDC.md "wikilink")（Game Developers
Conference）的「2004年最佳工作室」獎項。

2006年，Crytek在E3展因末日之戰獲得以下獎項：

  - [1UP](../Page/1UP.com.md "wikilink") Best Visuals E3 2006
  - [GameSpy](../Page/GameSpy.md "wikilink") Top 10 PC Game E3 2006
  - GameSpy Best Graphics" E3 2006
  - [Gametrailers](../Page/Gametrailers.md "wikilink") Best PC Game
  - Gametrailers Best Graphics
  - Gametrailers Best Shooter
  - IGN Best PC FPS Game
  - IGN Best Technical Graphics PC
  - IGN Technological Excellence PC
  - IGN Best FPS（整體）
  - IGN Best Graphics Technology（整體）
  - GameSpot Best Graphics
  - GameSpot Best Shooter（被提名）
  - GameSpot Best PC Game（被提名）
  - Gametrailers Best of Show（被提名）
  - 1UP Best PC Game E3 2006（被提名）
  - 1UP Best Shooting Game E3 2006（被提名）
  - IGN Technical Excellence（整體）（被提名）
  - IGN PC Game of the Show（被提名）
  - IGN Best of Show（被提名）

## 歷史

Crytek在1999年由Yerli兄弟在[科堡所創立](../Page/科堡.md "wikilink")，首次露面在[NVIDIA的攤位上以他的技術影片讓出版商印象深刻](../Page/NVIDIA.md "wikilink")，接著他們持續的釋出技術影片，當時這款遊戲叫做X-Isle，接著X-Isle發展成現在極地戰嚎（Far
Cry）。2002年5月2日，Crytek宣佈了他們的CryENGINE遊戲引擎。

2003年，Crytek參加了Game Developers Conference，並且展示了他們引擎的功能。

2004年2月，Crytek辦公室被德國警方突襲檢查，表示Crytek使用了違法軟體，警方搜查了許多份軟體，但最終沒有發現任何東西，極地戰嚎的開發延遲了三個小時，但沒有人受傷。\[4\]同年2月，Crytek和美商藝電（EA）也宣佈了合作關係。同年9月，Crytek和[ATI創立了一個](../Page/ATI.md "wikilink")[好萊塢式的遊戲影片來表示未來的PC遊戲發展](../Page/好萊塢.md "wikilink")。\[5\]

2006年1月23日，Crytek宣佈了末日之戰的開發，Crytek表示這將會是一款具有新玩法的嶄新的[第一人稱射擊遊戲](../Page/第一人稱射擊遊戲.md "wikilink")\[6\]，並且從E3和Games
Convention（GC）獲得了許多獎項。三個月後Crytek的辦公室從科堡搬至法蘭克福。1月23日，Crytek展示了CryENGINE2的功能，而且目前已經和許多公司簽約來開發產品或遊戲。

2007年5月11日，Crytek宣佈旗下位於基輔的工作室成為一個新的開發工作室，並著重於開發一個基於Crytek智慧財產下的新遊戲。接著，Crytek又宣布了在布達佩斯的工作室，這個工作室將著重在CryENGINE2引擎的開發。\[7\]

2009年2月4日，Crytek宣布收购英国游戏开发商，而后者更名为Crytek UK。

2016年12月20日，Crytek宣布关闭旗下位于上海、首尔、布达佩斯、索非亚和伊斯坦布尔的5家工作室，仅保留法兰克福和基辅两地的工作室；同时进一步裁员。\[8\]

2019年2月6日，Crytek基辅工作室宣布从Crytek独立，成立Blackwood
Games工作室，而旗下的《[战争前线](../Page/战争前线.md "wikilink")》（Warface）则将会继续由Blackwood
Games提供后续开发及更新维护，至此《战争前线》将不再属于Crytek旗下之作品。\[9\]

## 游戏開發

<div style="max-width:115em; -width:100%">

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>中文名称</p></th>
<th><p>发行日期</p></th>
<th><p>制作商</p></th>
<th><p>发行商</p></th>
<th><p><a href="../Page/电子游戏类型.md" title="wikilink">类型</a></p></th>
<th><p>平臺</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/孤岛惊魂_(游戏).md" title="wikilink">孤岛惊魂</a></p></td>
<td><p>2004年3月23日</p></td>
<td><p><a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a></p></td>
<td><p><a href="../Page/Ubisoft.md" title="wikilink">Ubisoft</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孤岛危机.md" title="wikilink">孤岛危机</a></p></td>
<td><p>2007年11月13日<br />
 2007年11月16日<br />
 2007年11月15日</p></td>
<td><p><a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a></p></td>
<td><p><a href="../Page/Electronic_Arts.md" title="wikilink">Electronic Arts</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孤岛危机：弹头.md" title="wikilink">孤岛危机：弹头</a></p></td>
<td><p>2008年9月16日<br />
 2008年9月16日</p></td>
<td><p><a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a></p></td>
<td><p><a href="../Page/Electronic_Arts.md" title="wikilink">Electronic Arts</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孤岛危机2.md" title="wikilink">孤岛危机2</a></p></td>
<td><p>2011年3月22日<br />
 2011年3月24日</p></td>
<td><p><a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a></p></td>
<td><p><a href="../Page/Electronic_Arts.md" title="wikilink">Electronic Arts</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a>、<a href="../Page/PlayStation_3.md" title="wikilink">PlayStation 3</a>、<a href="../Page/Xbox_360.md" title="wikilink">Xbox 360</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孤岛危机3.md" title="wikilink">孤岛危机3</a></p></td>
<td><p>2013年2月19日<br />
 2013年2月22日</p></td>
<td><p><a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a></p></td>
<td><p><a href="../Page/Electronic_Arts.md" title="wikilink">Electronic Arts</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a>、<a href="../Page/PlayStation_3.md" title="wikilink">PlayStation 3</a>、<a href="../Page/Xbox_360.md" title="wikilink">Xbox 360</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戰爭前線.md" title="wikilink">戰爭前線</a></p></td>
<td><p>2012年1月25日</p></td>
<td><p><a href="../Page/Crytek_Kiev.md" title="wikilink">Crytek Kiev</a>→ <a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a>→ <a href="../Page/Crytek_Kiev.md" title="wikilink">Crytek Kiev</a>→ <a href="../Page/Blackwood_Games.md" title="wikilink">Blackwood Games</a></p></td>
<td><p><a href="../Page/mail.ru.md" title="wikilink">mail.ru</a>、Crytek</p></td>
<td><p><a href="../Page/網路遊戲.md" title="wikilink">網路</a><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a>、<s><a href="../Page/XBOX_360.md" title="wikilink">XBOX 360</a></s></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/崛起：罗马之子.md" title="wikilink">崛起：罗马之子</a></p></td>
<td><p>2013年11月22日<br />
 2014年9月23日</p></td>
<td><p><a href="../Page/Crytek_Frankfurt.md" title="wikilink">Crytek Frankfurt</a></p></td>
<td><p><a href="../Page/Microsoft_Game_Studios.md" title="wikilink">Microsoft Game Studios</a></p></td>
<td><p><a href="../Page/動作遊戲.md" title="wikilink">動作遊戲</a></p></td>
<td><p><a href="../Page/Xbox_One.md" title="wikilink">Xbox One</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/时空分裂者4.md" title="wikilink">时空分裂者4</a></p></td>
<td><p>待定</p></td>
<td><p>Crytek UK</p></td>
<td><p>待定</p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>未知</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/家園戰線：革命.md" title="wikilink">家園戰線：革命</a></p></td>
<td><p>2016年5月17日<br />
 2016年5月20日</p></td>
<td><p><a href="../Page/Crytek_UK.md" title="wikilink">Crytek UK</a></p></td>
<td><p><a href="../Page/Deep_Silver.md" title="wikilink">Deep Silver</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/PlayStation_4.md" title="wikilink">PlayStation 4</a>,<a href="../Page/Xbox_One.md" title="wikilink">Xbox One</a>,<a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/猎杀：对决.md" title="wikilink">猎杀：对决</a></p></td>
<td><p>待定</p></td>
<td><p>Crytek UK</p></td>
<td><p>待定</p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p><a href="../Page/PlayStation_4.md" title="wikilink">PlayStation 4</a>,<a href="../Page/Xbox_One.md" title="wikilink">Xbox One</a>,<a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/命运竞技场.md" title="wikilink">命运竞技场</a></p></td>
<td><p>待定</p></td>
<td><p>Crytek UK</p></td>
<td><p>待定</p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">多人在线战斗竞技场游戏</a></p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Microsoft Windows</a></p></td>
</tr>
</tbody>
</table>

</div>

## 參見

## 外部連結

  -
[Category:1999年開業電子遊戲公司](../Category/1999年開業電子遊戲公司.md "wikilink")
[Category:德國電子遊戲公司](../Category/德國電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  [Crytek closing five studios, will refocus on ‘premium IPs’ and
    CryEngine](http://www.polygon.com/2016/12/20/14023746/crytek-studios-shutdown).Polygon.2016-12-20.\[2016-12-21\].
9.