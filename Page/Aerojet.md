**Aerojet**，是美国传统的军火公司，主营固体、液体[火箭](../Page/火箭.md "wikilink")[发动机设计](../Page/发动机.md "wikilink")、制造及生产。

## 历史

Aerojet于1942年由[西奥多·冯·卡门](../Page/西奥多·冯·卡门.md "wikilink")（Theodore von
Karman）博士组建，总部设在[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[萨克拉门托市](../Page/萨克拉门托.md "wikilink")，中国两弹一星元勋[钱学森在](../Page/钱学森.md "wikilink")1940年代为Aerojet公司的技术顾问。在1950至1960年代，曾是美国最大的火箭发动机研究、试验、生产基地。

1960年代，Aerojet公司参与了[双子星座计划及](../Page/双子星座计划.md "wikilink")[阿波罗计划](../Page/阿波罗计划.md "wikilink")，为其生产推进系统。1970－1980年代，Aerojet公司介入电子产品领域，包括卫星传感器与导弹检测系统。其分公司Aerojet
Ordnance
Tennessee，生产各种弹药及导弹弹头，如[-{zh-hant:BGM-71拖式飛彈;zh-cn:TOW式反坦克导弹;}-](../Page/BGM-71拖式飛彈.md "wikilink")，并继续为[NASA提供航天飞机轨道变轨发动机](../Page/NASA.md "wikilink")。

1990年代后，Aerojet公司进行了一系列的收购动作，先后收购了通用动力公司空间系统业务（General Dynamics' Space
Systems business）、大西洋研究公司（Atlantic Research
Corporation）的推进系统和[导弹防御业务](../Page/导弹防御.md "wikilink")。

## 现状

现阶段，Aerojet公司为[NASA的发现号航天飞机提供推进系统](../Page/NASA.md "wikilink")。并参与了[NMD](../Page/NMD.md "wikilink")（美国国家导弹防御系统）。公司的分公司Aerojet精细化学品公司负责生产火箭发动机燃料并参与化学产品及药物成分制造。

## 参与的项目

公司参与的项目：

:\*[SM-3導彈](../Page/SM-3導彈.md "wikilink")－标准3导弹

:\*Atlas V火箭发动机

:\*[-{zh-hant:BGM-71拖式飛彈;zh-cn:TOW式反坦克导弹;}-](../Page/BGM-71拖式飛彈.md "wikilink")

:\*[NMD](../Page/国家导弹防御系统.md "wikilink")－美国国家导弹防御系统

:\*THAAD－战区高空区域防御

:\*IHPRPT－高性能火箭推进技术

## 外部链接

  -
[Category:美国飞机公司](../Category/美国飞机公司.md "wikilink")
[Category:美国军事](../Category/美国军事.md "wikilink")