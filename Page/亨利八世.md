**亨利八世**（；），是[英格蘭](../Page/英格兰王国.md "wikilink")[亨利七世次子](../Page/亨利七世_\(英格兰\).md "wikilink")，[都铎王朝第二任](../Page/都铎王朝.md "wikilink")[国王](../Page/英格蘭君主列表.md "wikilink")，1509年4月22日繼位。他也是[愛爾蘭領主](../Page/愛爾蘭領地.md "wikilink")，後來更成為愛爾蘭國王。亨利八世為了[休妻而另娶新皇后而與當時的](../Page/离婚.md "wikilink")[羅馬教皇反目](../Page/教宗.md "wikilink")，推行[英格兰宗教改革](../Page/英格兰宗教改革.md "wikilink")，並通过一些重要法案，容許自己另娶，並将當時英國主教立為[英國國教大主教](../Page/英國國教.md "wikilink")，使[英国教会脱离](../Page/英格兰教会.md "wikilink")[罗马教廷](../Page/罗马教廷.md "wikilink")，自己成为英格蘭最高宗教领袖，並解散[教廷在國內的](../Page/教廷.md "wikilink")[修道院](../Page/修道院.md "wikilink")，使[英國王室的权力因此达到顶峰](../Page/英國王室.md "wikilink")。他在位期間，把[威尔士并入](../Page/威爾士.md "wikilink")[英格兰](../Page/英格兰.md "wikilink")。雖然亨利八世是[英格蘭宗教改革的推行者](../Page/英格蘭宗教改革.md "wikilink")，[英國國教](../Page/英國國教.md "wikilink")（又作[聖公宗](../Page/聖公宗.md "wikilink")、[盎格魯宗](../Page/盎格魯宗.md "wikilink")）的領導人，但是他對[教義沒甚麼創見](../Page/教義.md "wikilink")，一生都提倡[天主教儀式及教條](../Page/天主教.md "wikilink")，只是反對[教皇的權威而已](../Page/教皇.md "wikilink")。他的後裔[愛德華六世](../Page/爱德华六世.md "wikilink")，以及[伊麗莎白一世都繼續推行改革](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")，讓英國國教[新教化](../Page/新教.md "wikilink")。但在他的女兒[瑪麗一世在位期間曾恢復天主教的地位](../Page/玛丽一世_\(英格兰\).md "wikilink")。亨利八世曾經有六次婚姻，其中有兩個妻子被其下令[斬首](../Page/斬首.md "wikilink")。

他在位期间，除了推行[宗教改革外](../Page/宗教改革.md "wikilink")，更積極鼓励[人文主义研究](../Page/人文主义.md "wikilink")。亨利八世还合并了[英格兰和](../Page/英格兰.md "wikilink")[威尔士](../Page/威爾士.md "wikilink")，使[英国皇室的权力达到顶峰](../Page/英國王室.md "wikilink")。国家的权力扩大了，而[中产阶级对政治的参与度也加强了](../Page/中產階級.md "wikilink")；亨利成功的参与了欧洲大陆上的政治外交，然而却为此耗尽了国库，给以后继位的英国君主带来了麻烦。

## 生平

### 早年及第一次婚姻

[HenryVIII_1509.jpg](https://zh.wikipedia.org/wiki/File:HenryVIII_1509.jpg "fig:HenryVIII_1509.jpg")
1491年，**亨利八世**生于[格林尼治的](../Page/格林尼治.md "wikilink")[普拉森舍宫](../Page/普拉森舍宫.md "wikilink")(Palace
of
Placentia)，是[亨利七世和王后](../Page/亨利七世_\(英格兰\).md "wikilink")[约克的伊丽莎白的第三个孩子](../Page/約克的伊麗莎白.md "wikilink")。他們兄弟姐妹七人夭折三人，只剩下亨利及其兄长[亚瑟](../Page/亚瑟·都铎.md "wikilink")（封[威爾斯親王](../Page/威爾士親王.md "wikilink")），姐妹[玛格丽特和](../Page/玛格丽特·都铎.md "wikilink")[玛丽](../Page/玛丽·都铎.md "wikilink")。1493年，他受封[多佛堡总管和](../Page/多佛堡.md "wikilink")[五港同盟长官](../Page/五港同盟.md "wikilink")，1494年，受封[约克公爵](../Page/约克公爵.md "wikilink")，之后又获得[英国纹章院院长](../Page/英国纹章院院长.md "wikilink")（Earl
Marshal）和[爱尔兰总督头衔](../Page/爱尔兰总督.md "wikilink")。他接受一流教育，操流利[拉丁语](../Page/拉丁语.md "wikilink")、[法语和](../Page/法语.md "wikilink")[西班牙语](../Page/西班牙语.md "wikilink")。原先由於其兄长亚瑟是[太子](../Page/太子.md "wikilink")，依照當時[貴族的習慣](../Page/貴族.md "wikilink")，亨利極可能被安排[剃髮](../Page/剃髮.md "wikilink")[出家](../Page/出家.md "wikilink")，成為一個[祭司](../Page/祭司.md "wikilink")，邁向[主教](../Page/主教.md "wikilink")、[紅衣主教之路](../Page/紅衣主教.md "wikilink")。1502年，15歲的亚瑟突然去世，11歲的亨利繼任威爾斯親王，成為王储。

[英格蘭当时已从一](../Page/英格蘭.md "wikilink")[欧洲偏远小国发展为有影响的大国](../Page/欧洲.md "wikilink")。[亨利七世積極睦邻](../Page/亨利七世_\(英格兰\).md "wikilink")，把两个女儿嫁給[苏格兰和](../Page/苏格兰王国.md "wikilink")[法国的王储](../Page/法蘭西王國.md "wikilink")，长子亚瑟娶了[西班牙公主](../Page/西班牙.md "wikilink")[阿拉貢的凱瑟琳](../Page/阿拉貢的凱瑟琳.md "wikilink")（[斐迪南二世和](../Page/费尔南多二世_\(阿拉贡\).md "wikilink")[伊莎贝拉一世之女](../Page/伊莎贝拉一世_\(卡斯蒂利亚\).md "wikilink")）。1502年，15歲的亚瑟病逝時，西班牙正與法国不和。亨利七世想兩面友好维持中立，因此要次子續娶西班牙籍长媳凯瑟琳。

然而在当时，如此的[婚姻違反](../Page/婚姻.md "wikilink")[天主教教規](../Page/天主教.md "wikilink")，除非得到[教皇特许](../Page/教宗.md "wikilink")\[1\]。凯瑟琳因此宣稱其婚姻并未圆房，无需教皇特许，只要宣布婚姻无效即可。后来英西合议教皇特许仍是必要，以建立再次結婚的合法性。凯瑟琳的母亲[伊莎贝拉一世遂求得教皇發布](../Page/伊莎贝拉一世_\(卡斯蒂利亚\).md "wikilink")[教皇训令表示允准](../Page/教宗訓令.md "wikilink")。凯瑟琳在第一任丈夫死后14个月，与只有12岁的小叔订婚。

1505年，亨利七世失去与西班牙联盟的兴趣，其次子乃宣布并未同意这項婚约，英西進行外交斡旋。1509年，亨利七世去世。同年6月11日，18歲的亨利與24歲的西班牙公主[阿拉貢的凱瑟琳正式結婚](../Page/阿拉貢的凱瑟琳.md "wikilink")，6月24日在[倫敦的](../Page/伦敦.md "wikilink")[西敏寺加冕](../Page/西敏寺.md "wikilink")。

### 宗教改革及第二次婚姻

凯瑟琳與亨利八世懷孕六次，最後只生下女儿[玛丽](../Page/玛丽一世_\(英格兰\).md "wikilink")，其餘都早夭。情勢對她非常不利，因為亨利害怕女性繼承王位，會引發第二次[玫瑰戰爭](../Page/玫瑰战争.md "wikilink")，而且[舊約聖經说](../Page/舊約聖經.md "wikilink")：「弟娶兄嫂者會无后代」\[2\]。1525年，亨利认定凯瑟琳不能为他生下男性继承人，便和侍女[安妮·博林勾搭上](../Page/安妮·博林.md "wikilink")，和她發生關係了。安妮·博林以美色勾引诱惑亨利，慫恿他與凱瑟琳離婚，並與羅馬教皇決裂。于是他指使親信兼首相[托马斯·沃尔西](../Page/托馬斯·沃爾西.md "wikilink")（[枢机主教](../Page/樞機.md "wikilink")、[大法官](../Page/大法官_\(英國\).md "wikilink")）向教皇申请离婚，並派遣大臣威廉·奈特前去罗马教廷遊說，理由是凯瑟琳与亚瑟的短暂婚姻裡有[圆房事实](../Page/圆房.md "wikilink")。这给教皇[克勉七世出了大难题](../Page/克雷芒七世.md "wikilink")，因為之前1527年他曾在[羅馬之劫中被西班牙軍俘虜](../Page/羅馬之劫_\(1527年\).md "wikilink")，不想再得罪勢力強大的[西班牙帝國](../Page/西班牙帝國.md "wikilink")（西班牙国王[查理五世是凯瑟琳的姪子](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")），况且此一婚姻是前任教皇亲自批准的。

教廷迟不批准[离婚](../Page/离婚.md "wikilink")，首相[沃尔西也無能為力](../Page/托马斯·沃尔西.md "wikilink")，至1533年初安妮·博林已经怀孕四個月了。此前亨利就采取多項行動好能再次結婚，为了强迫凯瑟琳离婚，逼迫瑪麗她们母女分离，不给俸禄。1529年，他撤职並[禁錮沃尔西](../Page/禁錮.md "wikilink")。但继任者[托马斯·摩尔](../Page/托马斯·莫尔.md "wikilink")[爵士为人正直且忠于](../Page/爵士.md "wikilink")[羅馬教會](../Page/羅馬教會.md "wikilink")，仍不願配合。亨利繼續提拔其他的人，如[托马斯·克兰麦和](../Page/托马斯·克兰麦.md "wikilink")[托马斯·克伦威尔](../Page/托马斯·克伦威尔.md "wikilink")。這兩人都支持安妮·博林，也同情当时[歐洲大陆兴起的](../Page/歐洲大陸.md "wikilink")[新教改革風潮](../Page/宗教改革.md "wikilink")。克兰麦建议寻求大陆的神学院支持。

通过[贿赂和施惠](../Page/贿赂.md "wikilink")，此一策略获得成功。1530年，[英国国会提交了有利亨利的报告](../Page/英国国会.md "wikilink")。他投桃报李任命克兰麦为皇家法院特使，進而在[坎特伯雷大主教](../Page/坎特伯里大主教.md "wikilink")[威廉·沃兰死后让其继任](../Page/威廉·沃兰.md "wikilink")。克伦威尔是亨利的亲信谋臣，其建议更加大胆，要他废除教皇在英国的崇高地位並取代之。另方面，即使亨利威胁不繳交[教廷的](../Page/教廷.md "wikilink")[什一税](../Page/什一税.md "wikilink")，[教皇仍不改初衷](../Page/教皇.md "wikilink")。他於是下定决心决裂。

1533年1月，為了不讓孩子成為私生子，亨利秘密与安妮·博林结婚，[英国国会随即立法脱离](../Page/英国国会.md "wikilink")[罗马教廷](../Page/罗马教廷.md "wikilink")，[大主教克兰麦接著宣布亨利与凯瑟琳的婚姻无效与安妮](../Page/大主教.md "wikilink")·博林的婚姻合法。6月1日，安妮·博林加冕为王后。三个月后，安妮·博林生下一個女儿[伊丽莎白](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")。同年7月，教皇將亨利處以[破門律](../Page/破門律.md "wikilink")，但史学家对于具体日期有争议\[3\]。

1534年春，克伦威尔力促国会通过一系列法案：“上诉限制条例”\[4\]
禁止英国教会法庭上诉到教皇、禁止教会不经英王允许发布规章；“教职任命法案”\[5\]
规定教会必須根据英王指定的候选人推选主教；“[至尊法案](../Page/至尊法案.md "wikilink")”\[6\]
宣布英王是英国教会唯一最高权威；“[王位继承法](../Page/王位继承法.md "wikilink")”\[7\]
亨利將與[凯瑟琳所生的女儿](../Page/阿拉貢的凱瑟琳.md "wikilink")[玛丽貶為私生女](../Page/玛丽一世_\(英格兰\).md "wikilink")，指定與[安妮所生的女儿](../Page/安妮·博林.md "wikilink")[伊丽莎白將來繼承王位](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")；“叛国罪法案”\[8\]
规定不承认英王的最高权威即是[叛国罪](../Page/叛國.md "wikilink")，可判处死刑；并且取消给教皇的献金（Peter's
Pence）。

英国的教会从此轉變为[盎格魯宗](../Page/盎格魯宗.md "wikilink")，即[聖公宗](../Page/聖公宗.md "wikilink")，又稱[英國國教](../Page/英國國教.md "wikilink")。它是大公教會(catholic)的一个独立於羅馬天主教的教派，也是改革最不彻底的，因为其目的只放在合法化亨利与安的婚姻。聖公宗和舊教[天主教沒有理论原则的任何分歧](../Page/天主教.md "wikilink")，只是不承认教皇权威，再簡化部份的禮儀。很關鍵的《王位继承法》否认“外国的王室和君主”对[英国王位有继承权](../Page/英国王位.md "wikilink")，英国成人都必须宣誓承认该法案条款，拒绝者将处以[终身监禁](../Page/無期徒刑.md "wikilink")；任何人出版或印刷文字宣称亨利与安妮·博林的婚姻非法，必然處以[叛国罪](../Page/叛國.md "wikilink")，可以处死刑。亨利進而快速镇压對其宗教政策不满的人。部分不妥协的[修士被拷问并处死](../Page/修士.md "wikilink")，一些拒绝立誓的主要異議者被判处叛国罪，于
1535年在[泰伯恩刑场](../Page/泰伯恩.md "wikilink")[斩首](../Page/斬首.md "wikilink")。包括罗彻斯特主教[约翰·费舍尔](../Page/约翰·费舍尔.md "wikilink")、前任大法官[托马斯·摩尔爵士](../Page/托马斯·莫尔.md "wikilink")。

亨利新創一个职位“宗教特使”（Vicegerent in
Spirituals）給[克伦威尔](../Page/克伦威尔.md "wikilink")，授权他访问[修道院](../Page/修道院.md "wikilink")。表面為了确保法令实施，实际则在评估修道院财产。1536年，国会通过法案，授權亨利剥夺小型修道院（年收入少于
200
英镑）的财产。[解散修道院运动引发反抗](../Page/解散修道院运动.md "wikilink")，尤其同年10月在英格兰北部的[求恩巡礼事件](../Page/求恩巡礼.md "wikilink")（）。亨利宣布要宽恕叛乱者，并感谢他们提出值得注意的问题。他邀请其首领罗伯特·阿斯克（）参加皇家宴会，當場请對方写下事件情况以便改进问题。阿斯克相信亨利言行一致，乃解散叛乱團體。亨利卻認定其叛乱為叛国，無需遵守承诺。叛乱于是再生，但声势已弱終被镇压，其首领都被拘捕且以叛国罪处死，包括阿斯克。當初他書寫的資料成為其认罪书。1539年
4月，亨利再依据国会新法案先後解散较大的修道院。

亨利为了迎娶新王后，便進行激进的[宗教改革](../Page/宗教改革.md "wikilink")，安妮·博林甚至以陰毒的手段大力推廣新教，但兩人的关系很快就冷卻了，這時安妮的傲慢很快使得她在宫廷中变得不受欢迎，而且亨利已经对她失去兴趣并开始找其他的女人，但安妮深信一个儿子的出生还是可以挽救这场婚姻，可是安妮·博林在生下女兒[伊麗莎白后](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")，一次[死产](../Page/死產.md "wikilink")，兩次的[流产](../Page/流产.md "wikilink")，打擊了亨利尋求男性继承人的希望，這時**亨利八世**又看上一个漂亮女人，就是[珍·西摩](../Page/简·西摩_\(英格兰王后\).md "wikilink")，皮肤白皙，一头金发，体态瘦弱，[珍·西摩并无倾国之色](../Page/珍·西摩.md "wikilink")，但机敏温顺，使亨利倾心，[珍·西摩外表并不出众](../Page/简·西摩_\(英格兰王后\).md "wikilink")，但贤惠温柔，也可能是克伦威尔鼓动，亨利下令拘禁安妮·博林，一步步展開構陷安妮的計畫，並以高價賄賂支持[瑪麗公主](../Page/瑪麗一世.md "wikilink")、反對[安妮王后的人士](../Page/安妮·博林.md "wikilink")，要他們捏造對[安妮王后不實的指控](../Page/安妮·博林.md "wikilink")，之後安妮被冠上的罪名幾乎都是[叛国罪](../Page/叛国.md "wikilink")，但在審判過程中並沒有提出任何證據，所以有可能是揑造的。包括：以[巫术诱使国王结婚](../Page/巫術.md "wikilink")、和五个男人通奸、与兄弟[乔治·博林](../Page/乔治·博林.md "wikilink")[乱伦](../Page/乱伦.md "wikilink")、與他們有姦情、伤害国王、過度干政、預備[谋杀国王](../Page/谋杀.md "wikilink")、瞞著國王與一百個男人發生[性關係](../Page/性關係.md "wikilink")，以親吻，贈禮等方式誘惑與她私通的五個男人，並應允誰能成功暗殺亨利八世便和他成親，還有试图毒害亨利八世的[私生子亨利](../Page/非婚生子女.md "wikilink")·菲茨罗伊，之後安妮被以對丈夫不貞的罪名，被亨利八世處死，就連她的女兒[伊麗莎白公主也被趕出王宮](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")。

审判法庭由安妮·博林的舅舅[托马斯·霍华德主持](../Page/第三代诺福克公爵托马斯·霍华德.md "wikilink")。1536年
5月，法庭宣判安妮·博林及其兄弟死刑，由国王決定是[火刑或](../Page/火刑.md "wikilink")[斬首](../Page/斬首.md "wikilink")。兩人很快被斩首。安妮·博林知道自己難逃一死，卻沒有流淚驚慌，還是盛裝打扮，優雅地登上刑場，而且仍在最后[弥撒時](../Page/弥撒.md "wikilink")，對[牧师和其他見証人坚称自己无罪](../Page/牧师.md "wikilink")。五名通奸男子判處[车裂](../Page/車裂.md "wikilink")，后来[减刑为斩首](../Page/减刑.md "wikilink")。

### 合并威尔士及第三次婚姻

安妮·博林被斩首的第二天，亨利与他的侍女[珍·西摩](../Page/简·西摩_\(英格兰王后\).md "wikilink")[订婚](../Page/訂婚.md "wikilink")，十天后正式结婚，並以新通过的第二部王位继承法\[9\]
宣布新王后的子女将是顺位继承人；之前的子女[玛丽和](../Page/玛丽一世_\(英格兰\).md "wikilink")[伊丽莎白乃私生女](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")，剥夺其继承权；国王且擁有以[遗嘱重新指定继承人的权力](../Page/遗嘱.md "wikilink")。隔年的
1537年，珍生下儿子爱德华（后来的[爱德华六世](../Page/爱德华六世.md "wikilink")），卻在同年10月24日因[产褥热逝世於](../Page/产褥热.md "wikilink")[格林威治](../Page/格林尼治.md "wikilink")。亨利哀悼了比较长的时间，因為珍生下他渴望已久的唯一男性继承人，被他視为唯一“真正的”妻子。

亨利与珍结婚同时，國會通过《威尔士法案》\[10\]
將[威尔士并入](../Page/威爾士.md "wikilink")[英格兰](../Page/英格兰.md "wikilink")，並指定[英语為其官方语言](../Page/英语.md "wikilink")，给威尔士众多使用[威尔士语的人带来不便](../Page/威尔士语.md "wikilink")。1537年，亨利命令[坎特伯里大主教交出](../Page/坎特伯里大主教.md "wikilink")[奥特福德宫](../Page/奥特福德宫.md "wikilink")（）。1540年，亨利拆除天主教圣徒的一些圣地。1542年，英格兰剩余的修道院全數解散，其财产收归皇室。

修道院和隐修院在[英國上議院的席位取消](../Page/英國上議院.md "wikilink")，只有教会的[大主教和](../Page/總主教.md "wikilink")[主教能保留席位](../Page/主教.md "wikilink")。上议院[神职议员的席次首度少于](../Page/灵职议员.md "wikilink")**世俗议员**（）。克伦威尔則因此功勞受封新创的头衔[埃塞克斯伯爵](../Page/埃塞克斯伯爵.md "wikilink")。

### 第四次婚姻

[Hans_Holbein_d._J._026.jpg](https://zh.wikipedia.org/wiki/File:Hans_Holbein_d._J._026.jpg "fig:Hans_Holbein_d._J._026.jpg")为[克里维斯的安妮作的画像](../Page/克里维斯的安妮.md "wikilink")\]\]

亨利急于再婚，以确保男性继承人無虞。[克伦威尔推荐](../Page/克伦威尔.md "wikilink")[克里维斯的安妮](../Page/克里維斯的安妮.md "wikilink")，她是[神聖羅馬帝国](../Page/神圣罗马帝国.md "wikilink")[克里维斯公爵](../Page/克里维斯公爵.md "wikilink")（）的姐姐。公爵信奉新教，是亨利对抗罗马教廷的重要盟友。他派遣画家[小汉斯·霍尔拜因去给安妮画像](../Page/小汉斯·霍尔拜因.md "wikilink")。画得很美，朝臣也争相赞美，于是亨利同意结婚。但安妮抵达英国后，他对其外貌大為失望，她脸上的痘疤完全没画出。据说他私下批她为
“Flanders Mare”（法蘭德斯的夢魘）。然而協約已訂，兩人还是于1540年1月6日结婚。

亨利事後急于结束这桩婚姻，不仅由于个人審美喜好，也基于政治考虑。因为此時[公爵与](../Page/公爵.md "wikilink")[神圣罗马帝国皇帝不睦](../Page/神聖羅馬皇帝.md "wikilink")，亨利不想夾纏其中。安妮王后很识时务的配合，顺势声称两人并未[圆房](../Page/洞房.md "wikilink")，他僅每晚到她臥房吻一下额头而已。这桩婚姻于是被宣布无效，理由是安妮已和另一[欧洲](../Page/欧洲.md "wikilink")[贵族订婚](../Page/貴族.md "wikilink")。她被授予“国王的姐妹”荣衔，亨利还赠予她博林家族舊宅[赫弗城堡](../Page/赫弗城堡.md "wikilink")。克伦威尔则因此失宠被[剥夺财产](../Page/抄家.md "wikilink")，又被判[死刑](../Page/死刑.md "wikilink")，1540年7月28日[斩首](../Page/斬首.md "wikilink")。其担任的宗教特使一职此后没有继任者。

### 第五次婚姻

克伦威尔被斩首當天，亨利迎娶安妮·博林的表妹[凯瑟琳·霍华德](../Page/凯瑟琳·霍华德.md "wikilink")。亨利八世对新王后很满意，但當時他因過度肥胖、1536年1月24日以來積疾難癒的腿又嚴重感染，婚後有好幾個星期不見凱瑟琳\[11\]。凱瑟琳和侍臣发生[婚外情](../Page/婚外情.md "wikilink")，还任命從前情人弗兰西斯·迪勒姆（）為[秘书](../Page/秘书.md "wikilink")。大臣托马斯·克兰麦一向不喜天主教的霍华德家族，遂向亨利密报王后[通奸](../Page/通奸.md "wikilink")。他起初不相信，但克兰麦调查获得有力证据。凯瑟琳也承认婚前与迪勒姆有[婚约](../Page/婚约.md "wikilink")，这讓她与亨利的婚姻无效。她又声称受到迪勒姆强迫才与之通奸。迪勒姆则揭发她和卡尔佩珀的奸情。這項婚姻於是被宣布无效。既然婚姻无效，則通奸罪名理應不成立。但随后通过的《剥夺公民权法》（bill
of
attainder）使其确定无疑地获[叛国罪](../Page/叛国.md "wikilink")。1542年2月13日，凯瑟琳很快就被[处死](../Page/处死.md "wikilink")，当时只有22岁。

### 晚年及第六次婚姻

[Enrique_VIII_de_Inglaterra,_por_Hans_Holbein_el_Joven.jpg](https://zh.wikipedia.org/wiki/File:Enrique_VIII_de_Inglaterra,_por_Hans_Holbein_el_Joven.jpg "fig:Enrique_VIII_de_Inglaterra,_por_Hans_Holbein_el_Joven.jpg")

1543年，亨利第六次也是最後一次結婚，妻子是富有的[寡妇](../Page/寡妇.md "wikilink")[凯瑟琳·帕尔](../Page/凯瑟琳·帕尔.md "wikilink")。她的宗教觀點激进，亨利卻是保守派。兩人经常争论，幾次險些让她丧命，但她总能及时让步。受到王后影响，亨利和两个女儿[玛丽](../Page/玛丽一世.md "wikilink")、[伊丽莎白和解](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")。1544年，国会通过第三部《王位继承法》\[12\]
重新賦予她们次於爱德华王子的王位继承权，但仍被视为私生子。亨利則獲授權以遗嘱重新設立继承人。

亨利晚年严重[超重](../Page/超重.md "wikilink")，腰围达 54
[英寸](../Page/英寸.md "wikilink")（137
[公分](../Page/公分.md "wikilink")），也可能有[痛风](../Page/痛风.md "wikilink")。他超重始于
1536年。1536年1月24日亨利在格林威治一次[马上长矛比武发生意外](../Page/马上长矛比武.md "wikilink")，大腿受伤，从此无法运动\[13\]。其去世或许跟該伤口的溃烂有關。他去世
100 年后，传言甚广他患有[梅毒](../Page/梅毒.md "wikilink")。\[14\]亨利身体状况渐差之後，也越发残暴。

史学家[何林塞](../Page/何林塞.md "wikilink")（Raphael Holinshed）聲称，从
1513年的第三任[沙福克公爵埃德蒙](../Page/沙福克公爵.md "wikilink")·德拉·波尔（Edmund
de la Pole）到 1547年的萨里伯爵[亨利·霍华德](../Page/亨利·霍华德.md "wikilink")（Henry
Howard），他總共处决 72,000 名[政治犯](../Page/政治犯.md "wikilink")。

### 死亡及继承人

[The_Old_Palace_of_Whitehall_by_Hendrik_Danckerts.jpg](https://zh.wikipedia.org/wiki/File:The_Old_Palace_of_Whitehall_by_Hendrik_Danckerts.jpg "fig:The_Old_Palace_of_Whitehall_by_Hendrik_Danckerts.jpg")\]\]

1547年1月28日于[怀特霍尔宫](../Page/懷特霍爾宮.md "wikilink")，亨利在他父亲亨利七世 90
冥诞那天去世，埋葬在[温莎堡的圣乔治教堂](../Page/温莎堡.md "wikilink")，与第三任妻子珍·西摩合葬。约
100 年后，[查理一世也被葬於此](../Page/查理一世_\(英格蘭\).md "wikilink")。

亨利八世死后十来年间，其三个合法子女都坐上英国王位，但都没有子嗣。亨利唯一合法的儿子爱德华根据第三部《王位继承法》\[15\]，继承其王位成为[爱德华六世](../Page/爱德华六世.md "wikilink")，是英国历史第一位新教君主。他當時仅九岁，无法執政，16位执行者依據亨利遗嘱代议政事直到他满18岁。珍·西摩的兄长[爱德华·西摩被遗嘱指定为](../Page/第一代薩默塞特公爵爱德华·西摩.md "wikilink")[护国公](../Page/护国公.md "wikilink")。爱德华之后的顺位继承人是[凯瑟琳所生的女兒](../Page/阿拉貢的凱瑟琳.md "wikilink")[玛丽及其后代](../Page/玛丽一世_\(英格兰\).md "wikilink")；若玛丽無后，死后由[安妮所生的女兒](../Page/安妮·博林.md "wikilink")[伊麗莎白及其后代继位](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")；若伊丽莎白無后，死后由亨利八世的妹妹[玛丽·都铎的后代继位](../Page/玛丽·都铎.md "wikilink")。因為亨利姐姐[玛格丽特·都铎](../Page/玛格丽特·都铎.md "wikilink")（苏格兰王室）的后代已被剥夺对英国王位的继承权。最後，亨利的三个子女都没有后代。而[珍·葛雷於爱德华六世死後](../Page/珍·葛雷.md "wikilink")，在短暫的九天內自稱女王，很快就被推翻；玛丽·都铎的后代也被排除在了继承人之列。伊丽莎白一世去世后，王位传给她表侄女的儿子、苏格兰王室的继承人[詹姆士一世](../Page/詹姆士六世及一世.md "wikilink")。[都铎王朝從此终结](../Page/都铎王朝.md "wikilink")。

## 遗产和影响

[CoffinHenryVIIIStGeorgesChapelWindsor.png](https://zh.wikipedia.org/wiki/File:CoffinHenryVIIIStGeorgesChapelWindsor.png "fig:CoffinHenryVIIIStGeorgesChapelWindsor.png")。圖中景象位於聖喬治教堂之下，可見數人的棺材：亨利八世（中央，已損毀）、珍·西摩（右）、[查理一世和一個](../Page/查理一世_\(英格蘭\).md "wikilink")[安妮女王的孩子](../Page/安妮女王.md "wikilink")（左）。\[16\]\]\]
亨利对后世的最大影响在於英国宗教改革。1533年，他脱离[罗马教廷產生的影響远超过](../Page/罗马教廷.md "wikilink")[都铎王朝本身的份量](../Page/都铎王朝.md "wikilink")。尽管亨利最初动机是解决王朝和自身的继承人问题，也尽管他从未放弃[天主教信仰的本质](../Page/天主教.md "wikilink")，他的改革是所有英国君主最激进且最具决定性。

一方面，英国转变為独特且生气勃勃的国家，修道院被解散和土地财产充公，更使英国经济政治的权力重心从[教会移到](../Page/教会.md "wikilink")[贵族](../Page/貴族.md "wikilink")，社会效應非常深遠。另方面，亨利授權新教人士為其年幼儿子[爱德华六世](../Page/爱德华六世.md "wikilink")[摄政](../Page/摄政.md "wikilink")，确保了國教改革的持续。

亨利的政策和结果卻時常不一致。他鼓励研究[人文主义](../Page/人文主义.md "wikilink")，但也处死几位杰出的英国人文学家；他一心要确保王室延续，但只留下一个年轻的儿子（9歲继位，不到16岁就過世）和两个宗教信仰对立的女儿（且三人都無子嗣）；他扩大君主的权力，但中产阶级也加强参与政治（在其死后）；他成功加入[欧洲大陆的政治外交](../Page/歐洲大陸.md "wikilink")，但[国库因此空耗](../Page/国库.md "wikilink")，使继位的英国君主相當麻烦。

亨利和[阿佛列大帝](../Page/阿尔弗雷德大王.md "wikilink")、[查理二世一般被视为](../Page/查理二世_\(英格兰\).md "wikilink")[英国皇家海军的创始人](../Page/英國皇家海軍.md "wikilink")。在他统治下，海军參與一些重要战役，興建成果也增多，包括海军舰船（如著名的[玛丽玫瑰号](../Page/玛丽玫瑰号.md "wikilink")）、造船所（如在[朴次茅斯军港者](../Page/朴次茅斯.md "wikilink")）、海军建制改革（如舰上配备[大炮](../Page/火炮.md "wikilink")，原本的武器主流是[弓箭](../Page/弓.md "wikilink")）。但他主要作為仅在一些军舰，并未留下真正意义上的“海军”，没有建立正规的海军组织、[军衔](../Page/军衔.md "wikilink")、[军需等体制](../Page/军需.md "wikilink")。后来的[伊丽莎白一世仍然需要召募私人舰船去对抗](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")[西班牙无敌舰队](../Page/西班牙艦隊.md "wikilink")（包括130艘战舰和改装商船）。英国海军要到十七世纪[英荷争夺海上霸权时才算成型](../Page/英荷戰爭.md "wikilink")。

亨利脱离罗马教廷引起[法国和](../Page/法国.md "wikilink")[西班牙大规模入侵](../Page/西班牙.md "wikilink")。他因此加強海岸防卫堡垒，如[多佛堡和](../Page/多佛堡.md "wikilink")[多佛的](../Page/多佛尔.md "wikilink")
Moat Bulwark 和 Archcliffe
Fort，甚且亲自监管数個月。多佛堡至今尚有相关的纪念馆。亨利同时也新建一系列棱堡、炮台等，分布在[东英格兰到](../Page/东英格兰.md "wikilink")[康沃尔的南部海岸线上](../Page/康沃爾郡.md "wikilink")，大多使用拆除修道院所得的材料。这些設施另称为
。

亨利唯一留存的衣物是一顶冠冕。1536年，他连带一把[佩剑赏赐给](../Page/佩剑.md "wikilink")[沃特福德市长](../Page/沃特福德.md "wikilink")。目前冠冕保存于該市珍宝博物馆（Waterford
Museum of Treasures）。

## 称呼头衔和纹章

[Royal_Arms_of_England_(1399-1603).svg](https://zh.wikipedia.org/wiki/File:Royal_Arms_of_England_\(1399-1603\).svg "fig:Royal_Arms_of_England_(1399-1603).svg")

亨利是首位经常使用 “Majesty” 称呼的英国君主，他也不时使用 “Highness” 或
“Grace”。亨利改革一些皇室的礼仪，最初使用的头衔是“亨利八世，蒙上帝恩典，英格兰和法兰西国王，爱尔兰领主”（Henry
the Eighth, by the Grace of God, King of England, France and Lord of
Ireland）。1521年，亨利撰文攻击[马丁·路德以护卫罗马天主教廷](../Page/馬丁·路德.md "wikilink")，因此得到教皇[良十世赐封](../Page/良十世.md "wikilink")“信仰的守護者”（Defender
of the
Faith）。這也加入其正式头衔中。英国脱离罗马教廷之后，[保罗三世撤回該封赐](../Page/保祿三世.md "wikilink")，但英國国会立法認定那仍然有效。

1535年，亨利加添王权至高的字句在其头衔，即 “英格兰教会之首”（of the Church of England in Earth
Supreme
Head）。1536年，进一步改成“英格兰和爱尔兰教会之首”。1541年，亨利促成[爱尔兰国会將其頭銜](../Page/爱尔兰议会.md "wikilink")“爱尔兰领主”改为“爱尔兰国王”\[17\]。這事的意義重大。之前基於历史因素，很多[爱尔兰人认为](../Page/爱尔兰人.md "wikilink")[教皇是他們的国家元首](../Page/教宗.md "wikilink")，领主只是元首的代表。因為12世纪時，教皇[亚德四世封赐爱尔兰给英格兰国王](../Page/亚德四世.md "wikilink")[亨利二世](../Page/亨利二世_\(英格兰\).md "wikilink")，但自己仍保留名义的最高权位。这次承认亨利为爱尔兰国王的過程中，英國[盎格鲁人贵族首次參與了爱尔兰](../Page/盎格鲁人.md "wikilink")[凯尔特人的酋长会议](../Page/凯尔特人.md "wikilink")。直到亨利去世，其头衔一直是“亨利八世，蒙上帝恩典，英格兰、法兰西和爱尔兰国王，信仰的守护者，英格兰和爱尔兰在地上教会的最高元首”（Henry
the Eighth, by the Grace of God, King of England, France and Ireland,
Defender of the Faith and of the Church of England and also of Ireland
in Earth Supreme Head）。

亨利的[训辞是](../Page/训辞.md "wikilink")“Coeur
Loyal”（忠贞的心），以心型內置文字“Loyal”的形式縫绣在其衣物。他的徽章是[都铎玫瑰加上其祖母波福家族的](../Page/都铎玫瑰.md "wikilink")[吊闸图案](../Page/吊闸.md "wikilink")（Beaufort
portcullis）。他的[纹章採用](../Page/紋章.md "wikilink")[亨利四世開始的](../Page/亨利四世_\(英格兰\).md "wikilink")[英国王室纹章](../Page/英国皇家徽章.md "wikilink")：表面四等分，對角显示蓝底的三支[百合花飾](../Page/百合花飾.md "wikilink")（代表[法国](../Page/法国.md "wikilink")）和红底的三只头朝前面、向左做行走状的狮子（代表英格兰）。

## 宫廷生活和嗜好

亨利博学多才，其宫廷成為学术艺术的创新中心。[发现美洲](../Page/美洲殖民.md "wikilink")（新世界）引領出社會各领域的探索风气，更為亨利的创新精神提供全新的方向，實踐在其宫廷和日常生活中。他是最早研习全球地理的欧洲统治者。1507年，地图测绘师[马丁·瓦尔德泽米勒和](../Page/马丁·瓦尔德泽米勒.md "wikilink")[马蒂亚斯·林曼](../Page/马蒂亚斯·林曼.md "wikilink")（Matthias
Ringmann）出版最早的全球地图，描绘美洲大陆并将[大西洋和](../Page/大西洋.md "wikilink")[太平洋分別开来](../Page/太平洋.md "wikilink")，这理论在当年非常先进\[18\]。

亨利有个非常著名的[弄臣](../Page/弄臣.md "wikilink")[威尔·薩默斯](../Page/威尔·薩默斯.md "wikilink")（Will
Somers）。亨利热爱[赌博](../Page/赌博.md "wikilink")，喜欢玩[骰子](../Page/骰子.md "wikilink")。他年轻时擅长运动，尤其[摔跤](../Page/摔跤.md "wikilink")、[打猎](../Page/打猎.md "wikilink")、[室内网球](../Page/室内网球.md "wikilink")。他也是[音乐家](../Page/音乐家.md "wikilink")、[作家](../Page/作家.md "wikilink")、[诗人](../Page/诗人.md "wikilink")。他最有名的曲子是“与好伙伴一起消磨时光”（Pastime
with Good
Company），被称为**国王的歌谣**。亨利另参与新建和改建一些重要建筑物，如[无双宫](../Page/无双宫.md "wikilink")（Nonsuch
Palace）、[剑桥大学国王学院教堂](../Page/剑桥大学国王学院.md "wikilink")（King's College
Chapel）和伦敦[西敏寺](../Page/西敏寺.md "wikilink")。其改建者很多是查抄[樞機](../Page/樞機.md "wikilink")[托马斯·沃尔西的财产](../Page/托馬斯·沃爾西.md "wikilink")，如牛津的基督堂、[汉普顿宫](../Page/漢普敦宮.md "wikilink")、[怀特霍尔宫和](../Page/懷特霍爾宮.md "wikilink")[剑桥大学三一学院](../Page/剑桥大学三一学院.md "wikilink")。

## 家庭

### 祖先

<center>

</center>

### 妻子与情婦

亨利先后娶了六位妻子，但全都没好结果：第一位被迫[离婚](../Page/离婚.md "wikilink")（宣布无效），第四位[德意志新教公主协议离婚](../Page/神圣罗马帝国.md "wikilink")（也是宣布无效），第三位因病去世，最后一位因他去世而倖免於難，其餘两位則被他[处死](../Page/处死.md "wikilink")。有一句方便的[记忆口诀描述亨利的妻子们](../Page/记忆术.md "wikilink")：“离婚、[砍头](../Page/斬首.md "wikilink")、死、离婚、砍头、活”（"divorced,
beheaded, died, divorced, beheaded, survived"，或简化為“兩個斬首，一病歿，兩個被休，一存活。”
Two beheaded, one died, two divorced, one
survived."）还有说法是“亨利八世，结婚六次，一死一活，两個离婚，两個砍头。”（King
Henry the Eighth, to six wives he was wedded: One died, one survived,
two divorced, two beheaded，）

但严格來說，这些[打油诗並不是那麼確實](../Page/打油詩.md "wikilink")。因為在原则上，亨利从未与哪任妻子离婚：[朝廷只承認两次婚姻存在](../Page/朝廷.md "wikilink")，另外四次都宣布无效而不存在。安妮·博林（安·波林）和凯瑟琳·霍华德[处死前](../Page/死刑.md "wikilink")，是先被宣布婚姻无效的。

|  |
|  |
|  |
|  |
|  |
|  |
|  |

史学家能确定的亨利的[情婦有](../Page/情婦.md "wikilink")和[玛丽·博林](../Page/玛丽·博林.md "wikilink")（[安妮·博林的姐姐](../Page/安妮·博林.md "wikilink")）。前者为亨利生下私生子[亨利·菲茨罗伊](../Page/第一代里士满和萨默赛特公爵亨利·菲茨罗伊.md "wikilink")。1525年6月，他受封为里奇蒙公爵，有可能被正式承认为继承人，然而並沒有。1533年，菲茨罗伊与诺福克的玛丽·霍华德（）结婚，三年后去世，并無子嗣。當時亨利正想立法允许私生子继承王位。

亨利傳聞的[情人还很多](../Page/情人.md "wikilink")，如1510年的[法国人简](../Page/法国人.md "wikilink")·波皮库尔（Jane
Popicourt）、1514年的[哈斯丁斯勋爵的妻子](../Page/哈斯丁斯勋爵.md "wikilink")[安妮·斯塔福德夫人](../Page/安妮·斯塔福德.md "wikilink")（Anne
Stafford）（乃[白金汉公爵的姐妹](../Page/第三代白金汉公爵爱德华·斯塔福德.md "wikilink")）、1534\~1535年的安妮·博林的表姐玛格丽特·谢尔顿（Margaret
Shelton）。

### 子女

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出生</p></th>
<th><p>去世</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>妻子：<strong><a href="../Page/阿拉貢的凱瑟琳.md" title="wikilink">阿拉贡的凯瑟琳王后</a></strong> （1509年6月11日结婚，1533年取消婚姻，死于1536年1月6日）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>未命名的女儿</p></td>
<td><p>1510年1月31日</p></td>
<td><p>1510年1月31日</p></td>
<td><p><small>流产</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亨利_(康沃尔公爵).md" title="wikilink">亨利 (康沃尔公爵)</a></p></td>
<td><p>1511年1月1日</p></td>
<td><p>1511年2月22日</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>未命名的儿子</p></td>
<td><p>1513年11月</p></td>
<td><p>1513年11月</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>亨利 (康沃尔公爵)</p></td>
<td><p>1514年12月</p></td>
<td><p>1514年12月</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玛丽一世_(英格兰).md" title="wikilink">玛丽一世</a></p></td>
<td><p>1516年2月18日</p></td>
<td><p>1558年11月17日</p></td>
<td><p><small>1554年嫁西班牙<a href="../Page/腓力二世_(西班牙).md" title="wikilink">腓力二世</a>；无后代</small></p></td>
</tr>
<tr class="odd">
<td><p>未命名的孩子</p></td>
<td><p>1518年11月10日</p></td>
<td><p>1518年11月10日</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>妻子：<strong><a href="../Page/安妮·博林.md" title="wikilink">安妮·博林王后</a></strong> （1533年1月25日结婚；1536年5月19日被处死）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伊丽莎白一世_(英格兰).md" title="wikilink">伊丽莎白一世</a></p></td>
<td><p>1533年9月7日</p></td>
<td><p>1603年3月24日</p></td>
<td><p><small>没有结婚</small></p></td>
</tr>
<tr class="even">
<td><p>亨利·都铎</p></td>
<td><p>1534年</p></td>
<td><p>1534年</p></td>
<td><p><small>历史家不确定这个孩子是出生不久後死亡，或是流产。这件事情當時被隐瞒，連孩子性别也未能确定。</small></p></td>
</tr>
<tr class="odd">
<td><p>爱德华·都铎</p></td>
<td><p>1536年1月29日</p></td>
<td><p>1536年1月29日</p></td>
<td><p><small>死产</small></p></td>
</tr>
<tr class="even">
<td><p>妻子：<strong><a href="../Page/简·西摩_(英格兰王后).md" title="wikilink">珍·西摩王后</a></strong> （1536年5月20日结婚；1537年10月25日病死）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/爱德华六世.md" title="wikilink">爱德华六世</a></p></td>
<td><p>1537年10月12日</p></td>
<td><p>1553年7月6日</p></td>
<td><p><small>没有结婚</small></p></td>
</tr>
<tr class="even">
<td><p>妻子：<strong><a href="../Page/克里維斯的安妮.md" title="wikilink">克里维斯的安妮王后</a></strong> （1540年1月6日结婚，同年7月9日离婚，死于1557年7月17日）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>没有子女</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>妻子：<strong><a href="../Page/凯瑟琳·霍华德.md" title="wikilink">凯瑟琳·霍华德王后</a></strong> （1540年7月28日结婚，1541年离婚，1542年2月13日被处死）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>没有子女</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>妻子：<strong><a href="../Page/凯瑟琳·帕尔.md" title="wikilink">凯瑟琳·帕尔王后</a></strong> （1543年7月12日结婚；亨利八世死于 1547年1月28日；她再婚，1548年9月5日去世）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>没有子女</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>情人：<strong><a href="../Page/伊丽莎白·布伦特.md" title="wikilink">伊丽莎白·布伦特</a></strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>里奇蒙和萨默塞特公爵<br />
<a href="../Page/第一代里士满和萨默赛特公爵亨利·菲茨罗伊.md" title="wikilink">亨利·菲茨罗伊</a></p></td>
<td><p>1519年6月15日</p></td>
<td><p>1536年6月18日</p></td>
<td><p><small>私生子；1533年和<a href="../Page/玛丽·霍华德.md" title="wikilink">玛丽·霍华德夫人</a>（Lady Mary Howard）结婚；没有子女</small></p></td>
</tr>
<tr class="even">
<td><p>情人：<strong><a href="../Page/玛丽·博林.md" title="wikilink">玛丽·博林</a></strong> （Alison Weir认为这两个子女不是亨利八世所生。[19]）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凯瑟琳·凯里.md" title="wikilink">凯瑟琳·凯里</a><br />
（Catherine Carey）</p></td>
<td><p>1524年</p></td>
<td><p>1568年1月15日</p></td>
<td><p><small>未被承认的私生女；嫁<a href="../Page/弗朗西斯·诺利斯.md" title="wikilink">弗朗西斯·诺利斯</a>（Francis Knollys）爵士；有子女</small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亨利·凯里_(亨斯顿男爵).md" title="wikilink">亨斯顿男爵亨利·凯里</a></p></td>
<td><p>1526年3月4日</p></td>
<td><p>1596年7月23日</p></td>
<td><p><small>未被承认的私生子；1545年娶安·莫冈（Ann Morgan）；有子女</small></p></td>
</tr>
<tr class="odd">
<td><p>情人：<strong><a href="../Page/玛丽·伯克利.md" title="wikilink">玛丽·伯克利</a>（Mary Berkeley）</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/托马斯·斯图克利.md" title="wikilink">托马斯·斯图克利爵士</a></p></td>
<td><p>1525年</p></td>
<td><p>1578年8月4日</p></td>
<td><p><small>未被承认的私生子；娶安妮·克迪斯（Anne Curtis）；不知有无子女</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/约翰·佩罗特.md" title="wikilink">约翰·佩罗特爵士</a></p></td>
<td><p>1527年</p></td>
<td><p>1592年9月</p></td>
<td><p><small>未被承认的私生子；第一任妻子安·切尼（Ann Cheyney）、第二任妻子简·普鲁埃特（Jane Pruet）；有子女</small></p></td>
</tr>
<tr class="even">
<td><p>情人：<strong><a href="../Page/琼·丁格利.md" title="wikilink">琼·丁格利</a>（Joan Dyngley）</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>艾黛德里达·马尔特<br />
（Etheldreda Malte）</p></td>
<td><p>1529年</p></td>
<td><p>1555年</p></td>
<td><p><small>未被承认的私生女；在1546年至1548年之間嫁给约翰·哈林顿（John Harrington）；不知有无子女</small></p></td>
</tr>
</tbody>
</table>

**\***
<small>备注：关于私生子，亨利八世只正式承认里奇蒙和萨默塞特公爵[亨利·菲茨罗伊](../Page/亨利·菲茨罗伊.md "wikilink")，其他都沒有。除以上列表，亨利八世也可能与別的短期情人育有私生子。</small>

## 大众文化

亨利的生平经常出现在文学里。[威廉·莎士比亚也写了讲他的历史剧](../Page/威廉·莎士比亚.md "wikilink")。

### 电影

  - “[亨利八世的私生活](../Page/亨利八世的私生活.md "wikilink")”（**），1933年。[查尔斯·劳顿主演](../Page/查尔斯·劳顿.md "wikilink")，获得[奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")。
  - “[千日安妮](../Page/千日安妮.md "wikilink")”（），1969
    年。[理查·伯顿扮演亨利](../Page/理查·伯顿.md "wikilink")，[珍妮薇芙·褒祖德](../Page/珍妮薇芙·褒祖德.md "wikilink")（Geneviève
    Bujold）扮演安妮·博林，分别获得[奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")、[奥斯卡最佳女主角奖提名](../Page/奥斯卡最佳女主角奖.md "wikilink")。
  - “[日月精忠](../Page/日月精忠.md "wikilink")”，1966年，[奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")。改编自[罗伯特·鲍特](../Page/罗伯特·鲍特.md "wikilink")（Robert
    Bolt）的同名戏剧，主要围绕[托马斯·摩尔爵士展开](../Page/托马斯·莫尔.md "wikilink")，亨利八世由[罗伯特·肖](../Page/罗伯特·肖.md "wikilink")（Robert
    Shaw）扮演。1988
    年该片重拍，亨利八世由[马廷·张伯伦](../Page/马廷·张伯伦.md "wikilink")（Martin
    Chamberlain）扮演。
  - “[年轻的贝斯](../Page/年轻的贝斯.md "wikilink")”（），1953
    年。关于[伊丽莎白一世的年轻时代](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")，[查尔斯·劳顿再次扮演亨利八世](../Page/查尔斯·劳顿.md "wikilink")。
  - 《亨利八世》（[*Henry
    VIII*](http://www.imdb.com/title/tt0382737/)），2003年。[雷蒙·安德鲁·温斯顿饰演亨利八世](../Page/雷·温斯顿.md "wikilink")，[海伦娜·博纳姆·卡特饰演](../Page/海伦娜·博纳姆·卡特.md "wikilink")[安妮·博林](../Page/安妮·博林.md "wikilink")。
  - 《[-{zh-cn:鸠占鹊巢; zh-tw:美人心機;
    zh-hk:華麗孽緣;}-](../Page/華麗孽緣.md "wikilink")》，2008
    年。[史嘉蕾·喬韓森扮演瑪莉](../Page/史嘉蕾·喬韓森.md "wikilink")·博林，[娜塔莉·波曼扮演安妮](../Page/娜塔莉·波曼.md "wikilink")·博林，[艾瑞克·巴納扮演亨利八世](../Page/艾瑞克·巴納.md "wikilink")。

### 西冷牛排

英文為 Sirloin。傳聞中，亨利八世品嚐某一美味的牛排後，封之為爵士，乃 Sir +
loin。中文譯為西冷牛排（或沙朗牛排），另一種說法是亨利八世爱上一位女侍官[安妮·博林](../Page/安妮·博林.md "wikilink")，传说[安妮·博林对牛排情有独钟](../Page/安妮·博林.md "wikilink")，亨利为了取悦心上人，也开始吃起了牛排。其實，西冷此名稱來自[法語的](../Page/法語.md "wikilink")
（上的意思）和 （柳肉），表示[牛柳上方的肉](../Page/牛柳.md "wikilink")。

### 电视

  - “亨利八世的六个妻子”（[*The Six Wives of Henry
    VIII*](http://www.imdb.com/title/tt0066714/)），1970 年的
    [BBC](../Page/英国广播公司.md "wikilink") 电视剧，6 集。基斯·米歇尔（Keith
    Michell）主演，每集讲一个妻子，每集的编剧不同。
  - “另一个博林姑娘”（[*The Other Boleyn
    Girl*](http://imdb.com/title/tt0357392/)），2003
    年的[BBC](../Page/英国广播公司.md "wikilink") 电视剧。
  - “[都铎王朝](../Page/都鐸王朝_\(電視劇\).md "wikilink")”，2006 年的
    [Showtime](../Page/Showtime.md "wikilink")
    连续剧，[強納森·萊斯-梅爾斯主演](../Page/強納森·萊斯-梅爾斯.md "wikilink")，是
    [Showtime](../Page/Showtime.md "wikilink")
    三年来评价最高的剧集，总共四季。\[20\]。雖有历史家批评很多历史错误，但大多数人則赞赏其劇情演绎。
  - “[-{zh-cn:辛普森一家; zh-tw:辛普森家庭;
    zh-hk:阿森一族;}-](../Page/辛普森一家.md "wikilink")”，电视动画。某一集中，主角荷马扮演亨利八世，玛吉扮演[阿拉贡的凯瑟琳](../Page/阿拉貢的凱瑟琳.md "wikilink")，女儿丽莎扮演[玛丽一世](../Page/玛丽一世.md "wikilink")。
  - “亨利八世的六个妻子”（[*The Six Wives of Henry
    VIII*](http://www.channel4.com/history/microsites/S/sixwives/)），[英国第四台的电视纪录片](../Page/英国第四台.md "wikilink")。制作人為研究亨利八世的专家大卫·斯达克（David
    Starkey）。

### 音乐

1910年，弗莱德·穆雷（Fred Murray）和韦斯顿（R. P. Weston）写一首歌“我是亨利八世”（I'm Henery the
Eighth, I Am.），歌词说一名女子先后与八个名叫亨利的男子结婚。这首歌是英国歌手哈利·钱皮恩（Harry
Champion）的保留曲目。1965年，英国摇滚乐团 “Herman's Hermits”
重新演绎这首歌，登上美国[公告牌音乐排行榜的榜首](../Page/公告牌音乐排行榜.md "wikilink")。

1973年，[前卫摇滚乐队](../Page/前衛搖滾.md "wikilink") “Yes” 的里克·韦克曼（Rick
Wakeman）发行概念专辑“亨利八世的六个妻子”，包含六首以其王后名字命名的乐曲。

英国民谣“[绿袖子](../Page/绿袖子.md "wikilink")”傳說是亨利八世为其未来的王后[安妮·博林所作](../Page/安妮·博林.md "wikilink")，但没有什么依据。这首歌最早出现于1580年。

## 注释

## 參考文獻

  - 中文译名参考
    [大英百科全书线上版](https://web.archive.org/web/20070804032241/http://tw.britannica.com/)
  - Bowle, John. *Henry VIII: A Study of Power in Action* Little, Brown,
    1964.
  - John Lloyd and John Mitchinson "The Book of General Ignorance".
    faber and faber, 2006.
  - Bryant, M. *Private Lives*. Cassell, 2001.
  - [Eakins, L. E. (2004). "The Six Wives of Henry
    VIII".](http://tudorhistory.org/wives/)
  - Farrow, John V. *The Story of Thomas More*. Collins, 1956.
  - "Henry VIII". (1911). *Encyclopædia Britannica,* 11th ed. London:
    Cambridge University Press.
  - Kranes, Marsha et al. *Know It All*. New York: Tess Press, 1998.
  - [Jokinen, A. (2004). "Henry VIII
    (1491–1547)".](http://www.luminarium.org/renlit/tudor.htm)
  - Moorhouse, Geoffrey. *Great Harry's Navy: How Henry VIII Gave
    England Seapower*
  - [Public Broadcasting Service. (2003). "The Six Wives of Henry
    VIII".](http://www.pbs.org/wnet/sixwives/)
  - Nostro, Rit "at only 17 years old. He assumed the throne of England
    and married his brother's widow, Catherine of Aragon"
    [2](https://web.archive.org/web/20070816013622/http://www.hyperhistory.net/apwh/bios/b2henry8.htm)
  - [Thurston, H. (1910). "Henry VIII". *The Catholic Encyclopedia*.
    (Vol. VII). New York: Robert Appleton
    Company.](http://www.newadvent.org/cathen/07222a.htm)
  - [Vallieres, S. (1999). "Tudor Succession
    Problems"](http://www.luminarium.org/renlit/vallieres.htm)
  - Wagner, John A. (2003). "Bosworth Field to Bloody Mary: An
    Encyclopedia of the Early Tudors." (Greenwood). ISBN
    978-1-57356-540-0.
  - Weir, Alison. *The Six Wives of Henry VIII*. Bodley Head, 1991.
  - [Ask Ireland: Waterford Museum of Treasures Collection: Cap of
    Maintenance](https://web.archive.org/web/20070817220327/http://www.askaboutireland.ie/show_narrative_page.do?page_id=2863)
  - C. D. C. Armstrong, ‘Gardiner, Stephen (c.1495x8–1555)’, Oxford
    Dictionary of National Biography, Oxford University Press, Sept
    2004.
  - Henry VIII, "Assertio septem sacramentorum aduersus Martin. Luther"
    (1521)
    [Treasure 9](https://web.archive.org/web/20070520054923/http://libraries.theeuropeanlibrary.org/VaticanCity/treasures_en.xml)
    [欧洲图书馆](http://www.theeuropeanlibrary.org/) 网站上的梵蒂冈国家图书馆藏

## 延伸閱讀

  - ; Robert Henry Brodie; . *Letters and papers, foreign and domestic,
    of the reign of Henry VIII*，保存在
    、[大英博物馆及其他地方](../Page/大英博物馆.md "wikilink")。1965
    年第二版 ([TannerRitchie
    Publishing](http://www.tannerritchie.com/books/letterspapershenryviii.php))

  - Childs, Jessie. *Henry VIII's Last Victim: The Life and Times of
    Henry Howard, Earl of Surrey*. London: Jonathan Cape, 2006
    (hardback, ISBN 978-0-224-06325-8).

      - [评论](http://books.guardian.co.uk/reviews/history/0,,1927631,00.html?gusrc=rss&feed=10)
        C.J. Sansom发表于 [卫报](http://www.guardian.co.uk/)，2006年10月21日。

  - [马丁·路德](../Page/馬丁·路德.md "wikilink"). *Luther's Correspondence and
    Other Contemporary Letters,* 2 vols., tr.and ed. by Preserved Smith,
    Charles Michael Jacobs, The Lutheran Publication Society,
    Philadelphia, Pa. 1913, 1918. [vol. 1
    (1507–1521)](http://books.google.com/books?vid=OCLC02338418&id=m4r3cwHjnvUC&pg=PA1&lpg=PA1&dq=%22Luther%27s+Correspondence+and+Other+Contemporary+Letters%22)
    and [vol. 2
    (1521–1530)](http://books.google.com/books?vid=OCLC02338418&id=oEy_3aDT61sC&printsec=titlepage&dq=%22%09Luther%27s+Correspondence+and+Other+Contemporary+Letters%22)
    from [Google Books](../Page/Google圖書.md "wikilink"). Reprint of
    Vol.1, Wipf & Stock Publishers (March 2006). ISBN 978-1-59752-601-2

  - Wagner, John A. "Bosworth Field to Bloody Mary: An Encyclopedia of
    the Early Tudors." Greenwood, 2003.

  - Weir, Alison. *Henry VIII: The King and His Court*. Ballantine
    Books, 2001.

  - Williams, Neville. *Henry VIII and His Court*. Macmillan, 1971.

## 外部連結

  - [亨利八世年表](http://www.badley.info/history/Henry-VIII-England.biog.html)
  - [Luminarium网站：亨利八世](http://www.luminarium.org/renlit/tudor.htm)
    生平、作品、文章和研究材料
  - [历史学习网站：亨利八世和他的妻子](http://www.historylearningsite.co.uk/henry8.htm)
  - [Buehler, Edward. (2004).
    “都铎王朝和伊丽莎白时代的肖像”](http://www.tudor-portraits.com)
  - [Castelli, Jorge H. (2004).
    “亨利八世”](http://www.tudorplace.com.ar/aboutHenryVIII.htm)
  - [Stevens, Garry. (2003).
    “亨利八世：都铎王廷的阴谋”](https://web.archive.org/web/20070927222651/http://www.archsoc.com/games/Henry.html)
  - [Perrott, Terry. (2004). "Sir John
    Perrott".](https://web.archive.org/web/20060917151400/http://members.ozemail.com.au/~tperrott/sirjohn.htm)
  - [带有插画的亨利八世时期的历史](http://www.englishmonarchs.co.uk/tudor_4.htm)
  - [Find A Grave
    网站：亨利八世](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=473)
  - [马丁·路德](../Page/馬丁·路德.md "wikilink")
    [1525年9月1日给亨利八世的信](http://books.google.com/books?id=oEy_3aDT61sC&vid=OCLC02338418&dq=%22%09Luther%27s+Correspondence+and+Other+Contemporary+Letters%22&jtp=333)
  - [1526年8月，亨利八世给马丁·路德的信](http://books.google.com/books?id=oEy_3aDT61sC&vid=OCLC02338418&dq=%22%09Luther%27s+Correspondence+and+Other+Contemporary+Letters%22&jtp=374)
  - [亨利八世相关的传记类材料](https://web.archive.org/web/20070927010513/http://www.biographyshelf.com/king_henry_VIII_biography.html)
  - [亨利八世创作的音乐作品](http://www.icking-music-archive.org/ByComposer/Henry_VIII.php)

[H](../Category/英格蘭君主.md "wikilink") [H](../Category/都铎王朝.md "wikilink")
[H](../Category/文学角色.md "wikilink") [H](../Category/威尔斯亲王.md "wikilink")
[H](../Category/约克公爵.md "wikilink")
[H](../Category/英格蘭作曲家.md "wikilink")
[Category:被天主教会处以绝罚者](../Category/被天主教会处以绝罚者.md "wikilink")

1.  根据罗马天主教的**教会法**（）规定，男女一旦发生性关系，双方的血亲即成为姻亲（），不可结婚。

2.  圣经《利未记》，20:21，“人若娶弟兄之妻，这本是污秽的事，羞辱了他的弟兄；二人必无子女。”

3.  [温斯顿·丘吉尔在其](../Page/温斯顿·丘吉尔.md "wikilink")《》說，1533
    年的教皇训令仅是草稿，且未提惩罚。1535年才正式廢除亨利教籍。但另有人认为，是
    1538年的教皇[保罗三世為之](../Page/保祿三世.md "wikilink")。

4.  《上诉限制条例》，1534 年，。

5.  《教职任命法案》，1534 年，。

6.  《[至尊法案](../Page/至尊法案.md "wikilink")》，1534
    年，[至尊法案](../Page/至尊法案.md "wikilink")。

7.  第一部《王位继承法》，1534年，
    第二部《王位继承法》，1536年，
    第三部《王位继承法》，1543年，。

8.  《叛国罪法案》，1534 年，。

9.
10. 《威尔士法案》，1535年，。

11. The Private Lives of the Tudors Episode 2

12.
13. The Private Lives of the Tudors Episode 2

14.

15.
16. <https://thefreelancehistorywriter.com/2016/07/29/where-is-king-henry-viii-buried-and-why-doesnt-he-have-a-tomb/>

17. 《爱尔兰王位法案》，1542年，。

18. “这一地图反映了认知上的一个飞跃，认可了新发现的美洲大陆，同时也永远改变了人类对世界本身的认识。”[美国国会图书馆](../Page/國會圖書館_\(美國\).md "wikilink")
    [1](http://www.loc.gov/loc/lcib/0309/maps.html)。

19. 《亨利八世：国王和宫廷》（*Henry VIII: The King and His Court*），作者 Alison Weir，第
    216 页

20.