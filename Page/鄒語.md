[thumb遷台之前的台灣南島語言分布圖](../Page/文件:Formosan_languages.png.md "wikilink")(按
Blust,
1999)\[1\]\[2\].東台灣"[蘭嶼島](../Page/蘭嶼.md "wikilink")(深紅色)表示為使用[馬來-玻里尼西亞語族](../Page/馬來-玻里尼西亞語族.md "wikilink")[巴丹語群](../Page/巴丹語群.md "wikilink")[達悟語的區域](../Page/達悟語.md "wikilink").\]\]

**鄒語**（[鄒文](../Page/原住民族語羅馬字.md "wikilink")：），前稱「**曹語**」，為[臺灣](../Page/台灣.md "wikilink")[鄒族人所使用的語言](../Page/鄒族.md "wikilink")，屬於[南島語系的](../Page/南島語系.md "wikilink")[鄒語群](../Page/鄒語群.md "wikilink")，以[拉丁文字書寫](../Page/拉丁文字.md "wikilink")。\[3\]

## 鄒語變遷沿革

鄒語一般分為北鄒語群與南鄒語群。「北鄒語群」分佈於南投縣信義鄉望美部落；以及嘉義縣阿里山鄉的部落：特富野部落、達邦部落、里佳部落、茶山部落、新美部落、樂野部落、來吉部落、山美部落。而在高雄市[那瑪夏區的](../Page/那瑪夏區.md "wikilink")[卡那卡那富語與桃源鄉的](../Page/卡那卡那富語.md "wikilink")[拉阿魯哇語則叫](../Page/拉阿魯哇語.md "wikilink")「南鄒語群」。各自的語言歷經長年的變遷，自成一個系統。\[4\]

### 北鄒語群的變遷

北鄒語群又稱「鄒語」，有特富野社(Tfuya,Tfuea)、達邦社(Tapangu)、伊姆諸社(Imutsu,Limutsu)、與魯富都社(Luhtsu,Luhtu)四個方言語群，而四社中的伊姆諸社與魯富都社均於20世紀初廢社，人口分散到各社。根據[民族語學院](../Page/民族語.md "wikilink")(Ethnologue)記錄1981年伊姆諸社方言語群消失。目前北鄒語方言群分為三支：久美(望美)方言語群、特富野方言語群、達邦方言語群，不過各方言語群差異很小。

特富野方言和達邦方言之間文法差異不大。這兩方言語音上有些微差異，但變化規律，例如許多特富野方言讀z的音，在達邦方言則讀為i。
久美方言保留較多古音。

### 臺灣鄒族語典

1927年6月[俄國](../Page/俄國.md "wikilink")[語言學家](../Page/語言學家.md "wikilink")[聶甫斯基經由](../Page/聶甫斯基.md "wikilink")[日本來臺灣](../Page/日本.md "wikilink")，並與正就讀[台南師範之特富野鄒族青年](../Page/國立臺南大學.md "wikilink")[吾雍·雅達烏猶卡那](../Page/高一生.md "wikilink")（Uyongu
Yata'uyungana；高一生/矢多一生/矢多一夫），於1927年7月至8月初，從事1個多月的田野鄒語調查。聶甫斯基精通日文，兩人以[日語溝通](../Page/日語.md "wikilink")。期間記錄了15個故事，使用[拉丁字母與](../Page/拉丁字母.md "wikilink")[IPA記寫](../Page/IPA.md "wikilink")，並收集鄒語發音、語法、及詞彙等，以及鄒族的民族文物30件。聶甫斯基1929年從日本回[蘇聯後](../Page/蘇聯.md "wikilink")、於1935年在[聖彼德堡由](../Page/聖彼德堡.md "wikilink")[蘇聯科學院的出版社出版了一部](../Page/俄羅斯科學院.md "wikilink")《[臺灣鄒族語典](../Page/臺灣鄒族語典.md "wikilink")》（*N.A.
Nevskij:Materaly po govoram jazyka cou, Trudy Instituta Vostokovedenija.
X1.
Moskva-Leningrad,1935*/**），可謂最早的一部專門研究鄒語[語言學的專書](../Page/語言學.md "wikilink")。兩人先後於10年之後（1937年10月4日），聶甫斯基與日本籍妻子在[史達林](../Page/史達林.md "wikilink")[大清洗期間均被](../Page/大清洗.md "wikilink")[蘇聯政府以](../Page/蘇聯.md "wikilink")[日本](../Page/日本.md "wikilink")[間諜及叛國罪逮捕](../Page/間諜.md "wikilink")，於11月24日在[聖彼得堡夫妻均遭](../Page/聖彼得堡.md "wikilink")[槍決](../Page/槍斃.md "wikilink")；及27年之後、1954年4月17日高一生被[中華民國政府以](../Page/中華民國政府.md "wikilink")[228事件為由](../Page/二二八事件.md "wikilink")，定以[叛亂罪槍決](../Page/叛亂罪.md "wikilink")。\[5\]

## 語音系統

鄒語的音系之[音位大都使用適當的](../Page/音位.md "wikilink")[Unicode符號來標示](../Page/Unicode.md "wikilink")。在[臺灣南島語的](../Page/台灣南島語.md "wikilink")[書寫系統的訂定上](../Page/書寫系統.md "wikilink")，元輔音之音系表原則上都是先「[發音部位](../Page/發音部位.md "wikilink")」(橫列)、後「[發音方法](../Page/發音方法.md "wikilink")」(縱列)、再考量「[清濁音](../Page/清濁音.md "wikilink")」，來訂定其音系之音位架構。\[6\]

## 鄒語語法

在語法的分類上[臺灣南島語並不同於一般的](../Page/台灣南島語.md "wikilink")[分析語或其它](../Page/分析語.md "wikilink")[綜合語裡的動詞](../Page/綜合語.md "wikilink")、名詞、形容詞、介詞和副詞等之基本[詞類分類](../Page/詞類.md "wikilink")。比如臺灣南島語裡普遍沒有副詞，而副詞的概念一般以動詞方式呈現、可稱之為「副動詞」，類之於[俄語裡的副動詞](../Page/俄語.md "wikilink")。\[7\]
鄒語語法分類是將基礎的語法之詞類、詞綴、字詞結構及分類法，對比[分析語等之詞類分類法加以條析判別](../Page/分析語.md "wikilink")。

## 鄒語文獻

  - [鄒語新約聖經](../Page/鄒語新約聖經.md "wikilink")(buacou seiso faeva esvʉtʉ)

## 註釋

## 參考文獻

  - 沈家煊,"名詞和動詞"(On the Noun and Verb),北京商務印書館,2016年6月. ISBN
    978-7-100-11363-2

  - Henry Y. Chang(張永利),"Triadic Encoding in
    Tsou(鄒語三元述語的論元表現)"[5](http://www.ling.sinica.edu.tw/eip/FILES/journal/2011.10.31.49760072.9257849.pdf),中研院語言所期刊(第十二卷第四期)/12.4:845-876,July
    15/2011.<small>2011-0-012-004-000315-1</small>

  - Zeitoun, Elizabeth. 2005. "Tsou". In Adelaar, K. Alexander and
    Nikolaus Himmelmann, eds. 2005. *The Austronesian languages of Asia
    and Madagascar*. London: Routledge.

  - [齊莉莎](../Page/齊莉莎.md "wikilink")(Zeitoun,
    Elizabeth)，"鄒語參考語法"，[遠流出版公司](../Page/遠流出版公司.md "wikilink")，台北，p.40，2000.

  - Richard Wright & Peter Ladefoged (1994). "A phonetic study of Tsou".
    In *UCLA Working Papers in Phonetics 87: Fieldwork Studies of
    Targeted Languages II.*

  - 陳佑民,"邵語和鄒語的否定句(Negation in Thao and
    Tsou)"[6](http://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=Xrdr6v/record?r1=2&h1=2),碩士論文,中正大學,2000.

  - 翁翠霞,"鄒語和邵語的時,態,貌系統之比較與研究(A Contrastive Study of Tense, Mood and
    Aspect Systems in Tsou and
    Thao)"[7](http://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=Xrdr6v/record?r1=3&h1=2),碩士論文,中正大學,2000.

  -
  - 余文儀([臺灣府](../Page/臺灣府.md "wikilink")[知府](../Page/知府.md "wikilink"))主修,"[續修臺灣府志](../Page/臺灣府志.md "wikilink")"卷2,8,14,15,16,"26卷本",[乾隆](../Page/乾隆.md "wikilink")29年(1764年).

## 延伸閱讀

  - Dong Tonghe (董同龢). 1964. *A descriptive study of the Tsou language,
    Formosa*. Taipei, Taiwan: Institute of History and Philology,
    Academia Sinica.
  - Tsuchida, S. (1976). *Reconstruction of Proto-Tsouic phonology*.
    Tokyo: Institute for the Study of Languages and Cultures of Asia and
    Africa, Tokyo Gaikokugo Daigaku.

## 參見

  - [斯瓦迪士核心詞列表](../Page/斯瓦迪士核心詞列表.md "wikilink")
  - [原住民族語能力認證](../Page/原住民族語能力認證.md "wikilink")
  - [原住民族語言書寫系統 (中華民國)](../Page/原住民族語言書寫系統_\(中華民國\).md "wikilink")
  - [族語新聞](../Page/族語新聞.md "wikilink")
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")
  - [原住民部落社區大學](../Page/原住民部落社區大學.md "wikilink")
  - [原始人類語言](../Page/原始人類語言.md "wikilink")
  - [湯英伸事件](../Page/湯英伸事件.md "wikilink")
  - [小行星175586](../Page/小行星175586.md "wikilink")(175586 Tsou/鄒族小行星)

## 外部連結

  - [鄒族網路社群](https://web.archive.org/web/20060213012227/http://www.tsou.hohayan.net.tw/)
  - [魯富都北鄒語](https://web.archive.org/web/20051105001749/http://www.riccibase.com/docfile/lin-hi05.htm)
  - [原住民族語認證考古題與練習題](https://web.archive.org/web/20051118114157/http://www.ndhu.edu.tw/~lci/new_page_18.htm)
  - [台灣原住民博物館](https://web.archive.org/web/20070701230733/http://symuseum.myweb.hinet.net/)
  - [中央研究院南島語數位典藏](https://web.archive.org/web/20070115071850/http://formosan.sinica.edu.tw/)
  - [中央研究院語言學研究所](https://web.archive.org/web/20070205024307/http://www.ling.sinica.edu.tw/index-2.html)
  - [聶甫斯基(台灣原住民歷史語言文化大辭典)](http://134.208.27.115/citing_content.asp?id=1689&keyword=聶甫斯基)
  - [台灣原住民符號系統鍵盤配置程式](http://blog.xuite.net/uioiu/oika/18147233)
  - [台灣南島語言的奧秘(影片)](http://www.youmaker.com/video/sv?id=cc0c9909af5e4c6d8758ef0def64ead0001)
  - [公共電視知識的饗宴-第19集/台灣南島語言的奧秘](http://web.pts.org.tw/~web01/knowledge/19.htm)
  - [鄒語/台大台灣南島語多媒體資料庫](http://corpus.linguistics.ntu.edu.tw/choose.php?q=tsou)
  - [ABVD: Tsou/Blust, from: Tung
    (1963)](http://language.psy.auckland.ac.nz/austronesian/language.php?id=138)
  - [ABVD: Tsou (Duhtu)/Li
    (2004)](http://language.psy.auckland.ac.nz/austronesian/language.php?id=824)
  - [10-ICAL/Papers/Tenth International Conference on Austronesian
    Linguistics 17-20 January 2006 Palawan,
    Philippines](https://web.archive.org/web/20111210165330/http://www.sil.org/asia/Philippines/ical/papers.html)
  - [原民語e-learning](https://web.archive.org/web/20111208220112/http://e-learning.apc.gov.tw/)(鄒語1-9階/認證題庫)
  - [Cou Lemohen’o ta sinvun
    (Cou)/鄒語新聞](http://sinbung.titv.org.tw/Tribal_audio.aspx?id=364&mod=1#more)
  - [原住民族族語線上詞典](http://e-dictionary.apc.gov.tw/Index.htm)
  - [政大原住民族語研究中心九階教材](http://www.alcd.nccu.edu.tw/index.php?routing=Char&action=index/)

{{-}}

[Category:鄒語](../Category/鄒語.md "wikilink")

1.  Blust, R. (1999). "Subgrouping, circularity and extinction: some
    issues in Austronesian comparative linguistics" in E. Zeitoun &
    P.J.K Li (Ed.) Selected papers from the Eighth International
    Conference on Austronesian Linguistics (pp. 31-94). Taipei: Academia
    Sinica.
2.  Paul Jen-kuei Li," Some Remarks on the DNA Study on Austronesian
    Origins"[1](http://www.ling.sinica.edu.tw/eip/FILES/journal/2007.3.9.65909975.1080359.pdf),Languages
    and Linguistics 2.1:237-239,2001.
3.  國立台灣師範大學進修推廣學院,"98年度原住民族語言能力認證考試",臺北市,2009.
4.  行政院原住民族委員會,"原住民族語言書寫系統"[2](http://www.nter.tw/thao/attachments/003_%E5%8E%9F%E4%BD%8F%E6%B0%91%E8%AA%9E%E8%A8%80%E6%9B%B8%E5%AF%AB%E7%B3%BB%E7%B5%B1.pdf),台語字第0940163297號,原民教字第09400355912號公告,中華民國94年12月15日.
5.  [聶甫斯基](../Page/聶甫斯基.md "wikilink")([俄國人](../Page/俄國人.md "wikilink"))，"臺灣鄒族語典"，臺原藝術文化基金會，台北，7月，1993。
6.  行政院原住民族委員會,"原住民族語言書寫系統"[3](http://www.edu.tw/files/list/M0001/aboriginal.pdf),台語字第0940163297號,原民教字第09400355912號公告,中華民國94年12月15日.
7.  張永利,"台灣南島語言語法：語言類型與理論的啟示(Kavalan)"[4](http://vietnam.nsc.gov.tw/public/Data/012714292271.pdf),語言學門熱門前瞻研究,2010年12月/12卷1期,pp.112-127.