**王荣**（），[江苏省](../Page/江苏省.md "wikilink")[盐城](../Page/盐城市.md "wikilink")[滨海人](../Page/滨海县.md "wikilink")，1975年7月参加工作，1976年10月加入[中国共产党](../Page/中国共产党.md "wikilink")。南京农业大学农经系农业经济管理专业毕业，研究生学历，农学博士，教授、研究员。曾任[江苏省教育厅厅长](../Page/江苏省教育厅.md "wikilink")、江苏省[无锡市长](../Page/无锡市.md "wikilink")、[无锡](../Page/无锡市.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")、[江苏省委常委](../Page/江苏省委.md "wikilink")、[苏州](../Page/苏州市.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")、[深圳市委书记](../Page/深圳市委.md "wikilink")。现任[广东省政协主席](../Page/中国人民政治协商会议广东省委员会.md "wikilink")。中国共产党第十七届、十八届中央委员会候补委员。

## 生平

王荣在1975年至1978年间在滨海县天场公社[上山下乡](../Page/上山下乡.md "wikilink")、插队劳动。1976年加入[中国共产党](../Page/中国共产党.md "wikilink")。

1978年考入[南京农学院](../Page/南京农业大学.md "wikilink")，就读于农业经济系农业经济管理专业，1982年获[學士学位](../Page/學士.md "wikilink")。

1982年至1985年间，在同校（1984年7月改称[南京农业大学](../Page/南京农业大学.md "wikilink")）同专业继续硕士研究生学习，获[硕士学位](../Page/硕士.md "wikilink")。

1985年至1988年间，在同校同专业继续博士研究生学习，获[博士学位](../Page/博士.md "wikilink")，留校任教。

1989年至1990年间，任南京农业大学经贸学院副院长。

1990年调任南京农业大学校长助理，并在1991年获国家公派留学资助，以高级访问学者身份赴荷兰Tilburg大学进修并合作研究。

1992年兼任校长办公室主任。1994年任副校长。1997年调任[江苏省农业科学院院长](../Page/江苏省农业科学院.md "wikilink")、党委副书记。

1999年兼任任[江苏省农林厅副厅长](../Page/江苏省农林厅.md "wikilink")、党组副书记。

2000年转任[江苏省教育厅厅长](../Page/江苏省教育厅.md "wikilink")、党组书记兼江苏省委教育工委书记。

2001年出任中共[无锡市委副书记](../Page/无锡市.md "wikilink")、代市长。2002年任[无锡](../Page/无锡市.md "wikilink")[市长](../Page/市长.md "wikilink")。2003年，升任无锡市委书记。

2004年调任[江苏省委常委](../Page/中国共产党江苏省委员会.md "wikilink")，[苏州市委书记](../Page/苏州市.md "wikilink")。

2009年6月[中共中央决定](../Page/中国共产党中央委员会.md "wikilink")，调任[广东省委委员](../Page/中国共产党广东省委员会.md "wikilink")、常委。广东省委决定，任[深圳市委委员](../Page/中国共产党深圳市委员会.md "wikilink")、常委、副书记\[1\]。深圳市人大常委会决定，任[深圳市代市长](../Page/深圳市.md "wikilink")。2010年4月任深圳市委书记\[2\]。2010年6月，兼任[深圳警备区党委第一书记](../Page/深圳警备区.md "wikilink")，不再兼任[深圳市代市长](../Page/深圳市.md "wikilink")。

2015年2月10日下午3时，当选为[政协十一届广东省委员会主席](../Page/中国人民政治协商会议广东省委员会.md "wikilink")。\[3\]

2015年3月26日，深圳召开全市领导干部大会，宣布中央和省委关于深圳市委主要领导职务调整的决定。中央批准，王荣同志不再担任广东省委常委、深圳市委书记职务。广东省委批准，王荣同志不再担任深圳市委常委、委员职务。\[4\]

## 相关条目

  - [中国共产党广东省委员会](../Page/中国共产党广东省委员会.md "wikilink")
  - [中国人民政治协商会议广东省委员会](../Page/中国人民政治协商会议广东省委员会.md "wikilink")
  - [中国共产党深圳市委员会](../Page/中国共产党深圳市委员会.md "wikilink")
  - [深圳市人民政府](../Page/深圳市人民政府.md "wikilink")
  - [中国共产党江苏省委员会](../Page/中国共产党江苏省委员会.md "wikilink")
  - [深圳市](../Page/深圳市.md "wikilink")
  - [苏州市](../Page/苏州市.md "wikilink")
  - [无锡市](../Page/无锡市.md "wikilink")
  - [盐城市](../Page/盐城市.md "wikilink")
  - [南京农业大学](../Page/南京农业大学.md "wikilink")
  - [仇和](../Page/仇和.md "wikilink")

## 参考文献

## 外部链接

  - [王荣简历](https://web.archive.org/web/20150929062340/http://gbzl.people.com.cn/grzy.php?id=121000458)
    人民网中国领导干部资料库 2015-10-15

{{-}}

[W](../Category/中华人民共和国无锡市市长.md "wikilink")
[W](../Category/南京农业大学校友.md "wikilink")
[Category:南京农业大学教授](../Category/南京农业大学教授.md "wikilink")
[W](../Category/中国共产党第十七届中央委员会候补委员.md "wikilink")
[W](../Category/中国共产党第十八届中央委员会候补委员.md "wikilink")
[Category:中共苏州市委书记](../Category/中共苏州市委书记.md "wikilink")
[Category:中共无锡市委书记](../Category/中共无锡市委书记.md "wikilink")
[Category:中共深圳市委书记](../Category/中共深圳市委书记.md "wikilink")
[Category:广东省政协主席](../Category/广东省政协主席.md "wikilink")
[Category:中国共产党党员
(1976年入党)](../Category/中国共产党党员_\(1976年入党\).md "wikilink")
[Category:滨海人](../Category/滨海人.md "wikilink")
[R荣](../Category/王姓.md "wikilink")

1.
2.
3.
4.