**波爾克縣**（）是[美國](../Page/美國.md "wikilink")[北卡羅萊納州西南部的一個縣](../Page/北卡羅萊納州.md "wikilink")，西、南鄰[南卡羅萊納州](../Page/南卡羅萊納州.md "wikilink")。面積618平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口18,324人。縣治[哥倫布](../Page/哥倫布_\(北卡羅萊納州\).md "wikilink")（Columbus）。

本縣成立於1847年，1849年被廢，1855年又重建。縣名[美國獨立戰爭時期將領](../Page/美國獨立戰爭.md "wikilink")[威廉·波爾克](../Page/威廉·波爾克.md "wikilink")（William
Polk）。

[P](../Category/北卡羅萊納州行政區劃.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")
[Category:1855年北卡羅萊納州建立](../Category/1855年北卡羅萊納州建立.md "wikilink")