《**雙面情人**》
（**）是1998年首映的[浪漫](../Page/浪漫.md "wikilink")[愛情](../Page/愛情.md "wikilink")[电影](../Page/电影.md "wikilink")，以「一同進行的兩個[時空](../Page/時空.md "wikilink")」（[Parallel
universe](../Page/:en:Parallel_universe_\(fiction\).md "wikilink")）講述一個女人不一樣的[命運](../Page/命運.md "wikilink")，由[彼得·豪伊特執導及編劇](../Page/彼得·豪伊特.md "wikilink")，[桂莉芙·柏德露](../Page/桂莉芙·柏德露.md "wikilink")、[約翰·漢納與](../Page/約翰·漢納.md "wikilink")[約翰·林奇主演](../Page/約翰·林奇.md "wikilink")。本部电影的主题曲则为[丹麦乐团](../Page/丹麦.md "wikilink")[水叮当的代表作之一](../Page/水叮当.md "wikilink")——**，并收录在他们的首张专辑——*[Aquarium](../Page/Aquarium.md "wikilink")*中。片尾曲為
Dido 所唱的 Thank you。

## 情節

假設由[桂莉芙·柏德露飾演的海倫](../Page/桂莉芙·柏德露.md "wikilink")(Helen)在兩個時空所遭遇的不同命運。(There
are two sides to every romance. Helen is about to live both of them...at
the same time)

某個星期三的早上，在[公關公司工作的海倫](../Page/公關公司.md "wikilink")(Helen)被[解僱](../Page/解僱.md "wikilink")，失落的她打算坐[地鐵回家](../Page/倫敦地鐵.md "wikilink")，當她來到車站的時候，[列車則好來到](../Page/鐵路列車.md "wikilink")，Sliding
Door (緣份之門)一開一合，電影分開兩個不同時空，並交替進行：

  - 第一個時空：**海倫(Helen)趕上了那班列車**，並在車箱內遇到詹姆士。其後，海倫提早回到家，發現其[同居男友傑瑞正與其他女人](../Page/同居.md "wikilink")-{zh-hans:偷情;zh-hk:偷情;zh-tw:偷情;}-。傷心的她後來到[酒吧喝酒解悶](../Page/酒吧.md "wikilink")，並再次遇到詹姆士(James)，與他發展成一對[情侶](../Page/情侶.md "wikilink")，更在他的提議下，開設自己的公關公司……

<!-- end list -->

  - 第二個時空：**海倫(Helen)錯過了那班列車**，並因遇上列車服務受阻而選擇坐計程車回家。當海倫(Helen)準備揚手的時候，她被賊人[打劫](../Page/打劫.md "wikilink")，並受了傷。在[醫院把傷口治理好後](../Page/醫院.md "wikilink")，她回到家，與傑瑞-{zh-hans:偷情;zh-hk:偷情;zh-tw:偷情;}-的女人已經走了，海倫(Helen)依然被瞞騙著。之後，海倫(Helen)身兼數職，以維持生計。

到了電影的結局，海倫(Helen)在兩個時空同樣發現自己[懷孕](../Page/懷孕.md "wikilink")，並且遇到嚴重的意外。

  - 第一個時空與詹姆士(James)成為戀人的海倫(Helen)，因一次誤會而差點分手。當兩人冰釋前嫌後，海倫(Helen)卻遇上[交通意外](../Page/交通意外.md "wikilink")。在醫院裏，醫生告訴詹姆士(James)，受重傷海倫(Helen)小產，他的孩子沒有了，而且海倫(Helen)返魂乏術，最後她死在詹姆士(James)的懷裏。

<!-- end list -->

  - 第二個時空的海倫(Helen)終於發現傑瑞-{zh-hans:偷情;zh-hk:偷情;zh-tw:偷情;}-的事，在傷心與方寸盡失的情況下，她[失足從二樓的樓梯](../Page/失足從二樓的樓梯.md "wikilink")-{zh-hans:跌落;zh-hk:跌落;zh-tw:跌落;}-地下。受重傷的海倫(Helen)失去傑瑞的骨肉，幸好她在[手術後康復過來](../Page/手術.md "wikilink")，並與傑瑞分手。在海倫(Helen)出院的時候，她在電梯中遇到來醫院探望母親的詹姆士(James)，而電影並沒有交代他們往後的發展……只知道Sliding
    Door(緣份之門)再次開啟。

<!-- end list -->

  - 片中詹姆士(James)最後問海倫(Helen)的一句對白﹕(Nobody expects the Spanish
    Inquisition)「是福不是禍，是禍躲不過」是電影中心思想，原文出自英國喜劇團體[蒙提·派森](../Page/蒙提·派森.md "wikilink")（台灣版本譯成「歡樂特攻隊」），在香港出的英語中文字幕VCD版翻譯成﹕「是福不是禍，是禍躲不過」，台灣的版本翻成「沒有人喜歡西班牙宗教法庭。」。而[蒙提·派森在他們的作品中藉由喜劇反諷一種人遭受侵犯](../Page/蒙提·派森.md "wikilink")、不悅或不公平的質疑/問時的反應。劇情中的這些時候時候主角便會說"I
    didn't expect a kind of Spanish
    Inquisition"，而其他的劇員便會繞著場景發出一些令人不悅的聲音，之後其中一位主角會用高音大聲喊出"Nobody
    expects the Spanish
    Inquisition"。這句在電影中重點不在字面的意思，而是海倫(Helen)預期詹姆士(James)要說一些大道理來鼓勵人，為了逗她開心才冒出無關的對白。

## 演員表

### 主要角色

  - **[桂莉芙·柏德露](../Page/桂莉芙·柏德露.md "wikilink")**（[Gwyneth
    Paltrow](../Page/:en:Gwyneth_Paltrow.md "wikilink")） 飾演 **海倫·奎裡**
  - **[約翰·漢納](../Page/約翰·漢納.md "wikilink")**（[John
    Hannah](../Page/:en:John_Hannah.md "wikilink")） 飾演 **詹姆士·漢默頓**
  - **[約翰·林奇](../Page/約翰·林奇.md "wikilink")**（[John
    Lynch](../Page/:en:_John_Lynch.md "wikilink")） 飾演 **傑瑞**

### 其他角色

  - **[珍妮·特里普里霍恩](../Page/珍妮·特里普里霍恩.md "wikilink")**（[Jeanne
    Tripplehorn](../Page/:en:_Jeanne_Tripplehorn.md "wikilink")） 飾演
    **麗迪亞**
  - **[扎拉·特納](../Page/扎拉·特納.md "wikilink")**（[Zara
    Turner](../Page/:en:Zara_Turner.md "wikilink")） 飾演 **安娜**

## 外部連結

  -
[Category:1998年电影](../Category/1998年电影.md "wikilink")
[Category:英國電影作品](../Category/英國電影作品.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:1990年代喜劇劇情片](../Category/1990年代喜劇劇情片.md "wikilink")
[Category:1990年代浪漫喜劇片](../Category/1990年代浪漫喜劇片.md "wikilink")
[Category:英國喜劇劇情片](../Category/英國喜劇劇情片.md "wikilink")
[Category:英國浪漫喜劇片](../Category/英國浪漫喜劇片.md "wikilink")
[Category:美國喜劇劇情片](../Category/美國喜劇劇情片.md "wikilink")
[Category:美國浪漫喜劇片](../Category/美國浪漫喜劇片.md "wikilink")
[Category:米拉麥克斯電影](../Category/米拉麥克斯電影.md "wikilink")
[Category:平行世界題材電影](../Category/平行世界題材電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:欧洲电影奖最佳编剧获奖电影](../Category/欧洲电影奖最佳编剧获奖电影.md "wikilink")