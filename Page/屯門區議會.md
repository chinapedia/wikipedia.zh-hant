屯門區議會（英語：Tuen Mun District
Council）是[香港十八個區議會之一](../Page/香港.md "wikilink")，負責就[新界](../Page/新界.md "wikilink")[屯門區的事務向政府提供意見](../Page/屯門區.md "wikilink")。此區議會由29名民選議員及1名當然議員組成，最新一次的屯門區議會選舉在2015年11月22日舉行。

## 組織

### 領導層

| 主席                               | 副主席  |
| -------------------------------- | ---- |
| 姓名                               | 所屬政黨 |
| [劉皇發](../Page/劉皇發.md "wikilink") |      |
| [梁健文](../Page/梁健文.md "wikilink") |      |
| [劉皇發](../Page/劉皇發.md "wikilink") |      |
| [梁健文](../Page/梁健文.md "wikilink") |      |

### 委員會

<table>
<thead>
<tr class="header">
<th><p>委員會</p></th>
<th><p>主席</p></th>
<th><p>副主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>屯門區發展及規劃工作小組</p></td>
<td><p><a href="../Page/梁健文.md" title="wikilink">梁健文</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>屯門大型活動工作小組</p></td>
<td><p><a href="../Page/李洪森.md" title="wikilink">李洪森</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>社區危機處理工作小組</p></td>
<td><p><a href="../Page/龍瑞卿.md" title="wikilink">龍瑞卿</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>青山灣工作小組</p></td>
<td><p><a href="../Page/蘇炤成.md" title="wikilink">蘇炤成</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>工商業及房屋委員會</strong></p></td>
<td><p><a href="../Page/程志紅.md" title="wikilink">程志紅</a></p></td>
<td><p><a href="../Page/陳文偉.md" title="wikilink">陳文偉</a></p></td>
</tr>
<tr class="even">
<td><p>職業安全及健康工作小組</p></td>
<td><p><a href="../Page/李洪森.md" title="wikilink">李洪森</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>屯門經濟發展工作小組</p></td>
<td><p><a href="../Page/陳文偉.md" title="wikilink">陳文偉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>大廈管理工作小組</p></td>
<td><p><a href="../Page/程志紅.md" title="wikilink">程志紅</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地區設施管理委員會</strong></p></td>
<td><p><a href="../Page/陶錫源.md" title="wikilink">陶錫源</a></p></td>
<td><p><a href="../Page/巫成鋒.md" title="wikilink">巫成鋒</a></p></td>
</tr>
<tr class="even">
<td><p>社區參與工作小組</p></td>
<td><p><a href="../Page/巫成鋒.md" title="wikilink">巫成鋒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>社區閱讀督導小組</p></td>
<td><p><a href="../Page/曾憲康.md" title="wikilink">曾憲康</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>地區藝術督導小組</p></td>
<td><p><a href="../Page/林頌鎧.md" title="wikilink">林頌鎧</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>康體發展督導小組</p></td>
<td><p><a href="../Page/李洪森.md" title="wikilink">李洪森</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>設施及工程工作小組</p></td>
<td><p><a href="../Page/陶錫源.md" title="wikilink">陶錫源</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>屯門區興建行人通道上蓋督導小組</p></td>
<td><p><a href="../Page/龍瑞卿.md" title="wikilink">龍瑞卿</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>屯門綠化事宜工作小組</p></td>
<td><p><a href="../Page/蘇炤成.md" title="wikilink">蘇炤成</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西九文化區管理局跨界實驗空間計劃督導小組</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>環境、衞生及地區發展委員會</strong></p></td>
<td><p><a href="../Page/龍瑞卿.md" title="wikilink">龍瑞卿</a></p></td>
<td><p><a href="../Page/甘文鋒.md" title="wikilink">甘文鋒</a></p></td>
</tr>
<tr class="odd">
<td><p>屯門環境保護工作小組</p></td>
<td><p><a href="../Page/陳文華.md" title="wikilink">陳文華</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>街市及違例擺賣工作小組</p></td>
<td><p><a href="../Page/林頌鎧.md" title="wikilink">林頌鎧</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第54區發展及配套設施工作小組</p></td>
<td><p><a href="../Page/陶錫源.md" title="wikilink">陶錫源</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>財務、行政及宣傳委員會</strong></p></td>
<td><p><a href="../Page/歐志遠.md" title="wikilink">歐志遠</a></p></td>
<td><p><a href="../Page/何君堯.md" title="wikilink">何君堯</a></p></td>
</tr>
<tr class="odd">
<td><p>社會服務委員會</p></td>
<td><p><a href="../Page/蘇嘉雯.md" title="wikilink">蘇嘉雯</a></p></td>
<td><p><a href="../Page/曾憲康.md" title="wikilink">曾憲康</a></p></td>
</tr>
<tr class="even">
<td><p>醫療及復康服務工作小組</p></td>
<td><p><a href="../Page/巫成鋒.md" title="wikilink">巫成鋒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>社區關懷工作小組</p></td>
<td><p><a href="../Page/曾憲康.md" title="wikilink">曾憲康</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>教育及青少年服務工作小組</p></td>
<td><p><a href="../Page/甘文鋒.md" title="wikilink">甘文鋒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>交通及運輸委員會</strong></p></td>
<td><p><a href="../Page/蘇炤成.md" title="wikilink">蘇炤成</a></p></td>
<td><p><a href="../Page/葉文斌.md" title="wikilink">葉文斌</a></p></td>
</tr>
<tr class="even">
<td><p>屯門對外交通工作小組</p></td>
<td><p><a href="../Page/林頌鎧.md" title="wikilink">林頌鎧</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>屯門區內交通問題工作小組</p></td>
<td><p><a href="../Page/陳有海.md" title="wikilink">陳有海</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 現屆議員

### 議員列表

| 選區號碼 | 選區                                                              | 議員                                       | 所屬政黨 | 備註                                                                                |
| ---- | --------------------------------------------------------------- | ---------------------------------------- | ---- | --------------------------------------------------------------------------------- |
| L01  | [屯門市中心](../Page/屯門市中心.md "wikilink")                            | [歐志遠](../Page/歐志遠.md "wikilink")         |      | [新界社團聯會](../Page/新界社團聯會.md "wikilink")                                            |
| L02  | [兆](../Page/兆安苑.md "wikilink")[置](../Page/置樂花園.md "wikilink")   | [林頌鎧](../Page/林頌鎧.md "wikilink")         |      | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                                           |
| L03  | [兆](../Page/兆麟苑.md "wikilink")[翠](../Page/翠寧花園.md "wikilink")   | [葉文斌](../Page/葉文斌.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")/[新界社團聯會](../Page/新界社團聯會.md "wikilink")           |
| L04  | [安定](../Page/安定邨.md "wikilink")                                 | [江鳳儀](../Page/江鳳儀.md "wikilink")         |      | [民協](../Page/民協.md "wikilink")                                                    |
| L05  | [友愛南](../Page/友愛邨.md "wikilink")                                | [曾憲康](../Page/曾憲康.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")/[公屋聯會](../Page/公屋聯會.md "wikilink")               |
| L06  | [友愛北](../Page/友愛邨.md "wikilink")                                | [譚駿賢](../Page/譚駿賢.md "wikilink")         |      | [工黨](../Page/工黨.md "wikilink")                                                    |
| L07  | [翠](../Page/翠林花園.md "wikilink")[興](../Page/大興花園.md "wikilink")  | [朱耀華](../Page/朱耀華_\(區議員\).md "wikilink") |      | [實政圓桌](../Page/實政圓桌.md "wikilink")                                                |
| L08  | [山景](../Page/山景邨.md "wikilink")                                 | [吳觀鴻](../Page/吳觀鴻.md "wikilink")         |      | 獨立                                                                                |
| L09  | [景](../Page/山景邨.md "wikilink")[興](../Page/大興邨.md "wikilink")    | [陳有海](../Page/陳有海.md "wikilink")         |      | [工聯會](../Page/工聯會.md "wikilink")                                                  |
| L10  | [興](../Page/大興邨.md "wikilink")[澤](../Page/澤豐花園.md "wikilink")   | [徐　帆](../Page/徐帆_\(區議員\).md "wikilink")  |      | [工聯會](../Page/工聯會.md "wikilink")                                                  |
| L11  | [新墟](../Page/屯門新墟.md "wikilink")                                | [古漢強](../Page/古漢強.md "wikilink")         |      | [新界社團聯會](../Page/新界社團聯會.md "wikilink")                                            |
| L12  | [三聖](../Page/三聖邨.md "wikilink")                                 | [蘇炤成](../Page/蘇炤成.md "wikilink")         |      | [新民黨](../Page/新民黨_\(香港\).md "wikilink")                                           |
| L13  | [恆福](../Page/恆福.md "wikilink")                                  | [朱順雅](../Page/朱順雅.md "wikilink")         |      | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                                           |
| L14  | [富](../Page/富健花園.md "wikilink")[新](../Page/新屯門中心.md "wikilink") | [甘文鋒](../Page/甘文鋒.md "wikilink")         |      | [新民黨](../Page/新民黨_\(香港\).md "wikilink")                                           |
| L15  | [悅湖](../Page/悅湖山莊.md "wikilink")                                | [張恆輝](../Page/張恆輝.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")                                                  |
| L16  | [兆禧](../Page/兆禧苑.md "wikilink")                                 | [甄紹南](../Page/甄紹南.md "wikilink")         |      | [民協](../Page/民協.md "wikilink")                                                    |
| L17  | [湖景](../Page/湖景_\(選區\).md "wikilink")                           | [梁健文](../Page/梁健文.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")                                                  |
| L18  | [蝴蝶](../Page/蝴蝶_\(選區\).md "wikilink")                           | [楊智恒](../Page/楊智恒.md "wikilink")         |      | [民協](../Page/民協.md "wikilink")                                                    |
| L19  | [樂](../Page/美樂花園.md "wikilink")[翠](../Page/海翠花園.md "wikilink")  | [何君堯](../Page/何君堯.md "wikilink")         |      | [新論壇](../Page/新世紀論壇.md "wikilink")/[新界關注大聯盟](../Page/新界關注大聯盟.md "wikilink")       |
| L20  | [龍門](../Page/龍門居.md "wikilink")                                 | [龍瑞卿](../Page/龍瑞卿.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")                                                  |
| L21  | [新](../Page/新圍苑.md "wikilink")[景](../Page/良景邨.md "wikilink")    | [黃麗嫦](../Page/黃麗嫦.md "wikilink")         |      | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                                           |
| L22  | [良景](../Page/良景邨.md "wikilink")                                 | [程志紅](../Page/程志紅.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")                                                  |
| L23  | [田景](../Page/田景_\(選區\).md "wikilink")                           | [李洪森](../Page/李洪森.md "wikilink")         |      | [工聯會](../Page/工聯會.md "wikilink")                                                  |
| L24  | [寶田](../Page/寶田邨.md "wikilink")                                 | [蘇嘉雯](../Page/蘇嘉雯.md "wikilink")         |      | [實政圓桌](../Page/實政圓桌.md "wikilink")                                                |
| L25  | [建生](../Page/建生邨.md "wikilink")                                 | [陳文華](../Page/陳文華_\(民建聯\).md "wikilink") |      | [民建聯](../Page/民建聯.md "wikilink")                                                  |
| L26  | [兆康](../Page/兆康苑.md "wikilink")                                 | [巫成鋒](../Page/巫成鋒.md "wikilink")         |      | [民建聯](../Page/民建聯.md "wikilink")                                                  |
| L27  | [景峰](../Page/景峰花園.md "wikilink")                                | [何杏梅](../Page/何杏梅.md "wikilink")         |      | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                                           |
| L28  | [富泰](../Page/富泰邨.md "wikilink")                                 | [陳文偉](../Page/陳文偉.md "wikilink")         |      | [工聯會](../Page/工聯會.md "wikilink")                                                  |
| L29  | [屯門鄉郊](../Page/藍地.md "wikilink")                                | [陶錫源](../Page/陶錫源.md "wikilink")         |      | [城鄉居民共和協會](../Page/城鄉居民共和協會.md "wikilink")/[新界社團聯會](../Page/新界社團聯會.md "wikilink") |
| 當然議員 | 屯門鄉事委員會主席                                                       | [劉業強](../Page/劉業強.md "wikilink")         |      | [經民聯](../Page/經民聯.md "wikilink")                                                  |

### 政黨組成

屯門區議會以[民建聯為第一大黨](../Page/民主建港協進聯盟.md "wikilink")，其次為[民主黨及](../Page/民主黨_\(香港\).md "wikilink")[工聯會](../Page/工聯會.md "wikilink")。按議員表決時的投票傾向，大致可分類為[建制派及](../Page/建制派.md "wikilink")[非建制派兩大陣營](../Page/非建制派.md "wikilink")。

[Tuen_Mun_District_Council_2015.svg](https://zh.wikipedia.org/wiki/File:Tuen_Mun_District_Council_2015.svg "fig:Tuen_Mun_District_Council_2015.svg")

  -
  -
  -
  -
  -
  -
  -
  -

<div style="overflow:auto; width:100%">

<table>
<thead>
<tr class="header">
<th><p>政黨</p></th>
<th><p>議員</p></th>
<th><p>人數（百份比）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>建制派</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/民建聯.md" title="wikilink">民建聯</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/工聯會.md" title="wikilink">工聯會</a></p></td>
<td><p><a href="../Page/陳有海.md" title="wikilink">陳有海</a>、<a href="../Page/徐帆_(區議員).md" title="wikilink">徐帆</a>、<a href="../Page/李洪森.md" title="wikilink">李洪森</a>、<a href="../Page/陳文偉.md" title="wikilink">陳文偉</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/新民黨_(香港).md" title="wikilink">新民黨</a></p></td>
<td><p><a href="../Page/蘇炤成.md" title="wikilink">蘇炤成</a>、<a href="../Page/甘文鋒.md" title="wikilink">甘文鋒</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/實政圓桌.md" title="wikilink">實政圓桌</a></p></td>
<td><p><a href="../Page/朱耀華_(區議員).md" title="wikilink">朱耀華</a>、<a href="../Page/蘇嘉雯.md" title="wikilink">蘇嘉雯</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/經民聯.md" title="wikilink">經民聯</a></p></td>
<td><p><a href="../Page/劉業強.md" title="wikilink">劉業強</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>獨立</p></td>
<td><p><a href="../Page/歐志遠.md" title="wikilink">歐志遠</a>、<a href="../Page/吳觀鴻.md" title="wikilink">吳觀鴻</a>、<a href="../Page/古漢強.md" title="wikilink">古漢強</a>、<a href="../Page/何君堯.md" title="wikilink">何君堯</a>、<a href="../Page/陶錫源.md" title="wikilink">陶錫源</a></p></td>
</tr>
<tr class="even">
<td><p><strong>總計</strong></p></td>
<td><p>22 (73.3%)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>民主派</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/民主黨_(香港).md" title="wikilink">民主黨</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/民協.md" title="wikilink">民協</a></p></td>
<td><p><a href="../Page/江鳳儀.md" title="wikilink">江鳳儀</a>、<a href="../Page/甄紹南.md" title="wikilink">甄紹南</a>、<a href="../Page/楊智恆.md" title="wikilink">楊智恆</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/工黨_(香港).md" title="wikilink">工黨</a></p></td>
<td><p><a href="../Page/譚駿賢.md" title="wikilink">譚駿賢</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>總計</strong></p></td>
<td><p>8 (26.6%)</p></td>
<td></td>
</tr>
</tbody>
</table>

</div>

## 過往議員

| 選區號碼 | 選區                                                              | 議員                                     | 所屬政黨                                     | 備註                                         |
| ---- | --------------------------------------------------------------- | -------------------------------------- | ---------------------------------------- | ------------------------------------------ |
| L01  | [屯門市中心](../Page/屯門市中心.md "wikilink")                            |                                        | [歐志遠](../Page/歐志遠.md "wikilink")         | [新界社團聯會](../Page/新界社團聯會.md "wikilink")     |
| L02  | [兆](../Page/兆安苑.md "wikilink")[置](../Page/置樂花園.md "wikilink")   |                                        | [林頌鎧](../Page/林頌鎧.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L03  | [兆](../Page/兆麟苑.md "wikilink")[翠](../Page/翠寧花園.md "wikilink")   |                                        | [盧民漢](../Page/盧民漢.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L04  | [安定](../Page/安定邨.md "wikilink")                                 |                                        | [江鳳儀](../Page/江鳳儀.md "wikilink")         | [民協](../Page/民協.md "wikilink")             |
| L05  | [友愛南](../Page/友愛邨.md "wikilink")                                |                                        | [曾憲康](../Page/曾憲康.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L06  | [友愛北](../Page/友愛邨.md "wikilink")                                |                                        | [陳雲生](../Page/陳雲生.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L07  | [翠](../Page/翠林花園.md "wikilink")[興](../Page/大興花園.md "wikilink")  |                                        | [朱耀華](../Page/朱耀華_\(區議員\).md "wikilink") | [新民黨](../Page/新民黨_\(香港\).md "wikilink")    |
| L08  | [山景](../Page/山景邨.md "wikilink")                                 |                                        | [吳觀鴻](../Page/吳觀鴻.md "wikilink")         | 獨立                                         |
| L09  | [景](../Page/山景邨.md "wikilink")[興](../Page/大興邨.md "wikilink")    |                                        | [陳有海](../Page/陳有海.md "wikilink")         | [工聯會](../Page/工聯會.md "wikilink")           |
| L10  | [興](../Page/大興邨.md "wikilink")[澤](../Page/澤豐花園.md "wikilink")   |                                        | [徐　帆](../Page/徐帆_\(區議員\).md "wikilink")  | [工聯會](../Page/工聯會.md "wikilink")           |
| L11  | [新墟](../Page/屯門新墟.md "wikilink")                                |                                        | [古漢強](../Page/古漢強.md "wikilink")         | [新界社團聯會](../Page/新界社團聯會.md "wikilink")     |
| L12  | [三聖](../Page/三聖邨.md "wikilink")                                 |                                        | [蘇炤成](../Page/蘇炤成.md "wikilink")         | [新民黨](../Page/新民黨_\(香港\).md "wikilink")    |
| L13  | [恆福](../Page/恆福.md "wikilink")                                  |                                        | [朱順雅](../Page/朱順雅.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L14  | [富](../Page/富健花園.md "wikilink")[新](../Page/新屯門中心.md "wikilink") |                                        | [龍更新](../Page/龍更新.md "wikilink")         | 獨立                                         |
| L15  | [悅湖](../Page/悅湖山莊.md "wikilink")                                |                                        | [張恆輝](../Page/張恆輝.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L16  | [兆禧](../Page/兆禧苑.md "wikilink")                                 |                                        | [嚴天生](../Page/嚴天生.md "wikilink")         | [民協](../Page/民協.md "wikilink")             |
| L17  | [湖景](../Page/湖景_\(選區\).md "wikilink")                           |                                        | [梁健文](../Page/梁健文.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L18  | [蝴蝶](../Page/蝴蝶_\(選區\).md "wikilink")                           |                                        | [蘇愛群](../Page/蘇愛群.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L19  | [樂](../Page/美樂花園.md "wikilink")[翠](../Page/海翠花園.md "wikilink")  |                                        | [何俊仁](../Page/何俊仁.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L20  | [龍門](../Page/龍門居.md "wikilink")                                 |                                        | [龍瑞卿](../Page/龍瑞卿.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L21  | [新](../Page/新圍苑.md "wikilink")[景](../Page/良景邨.md "wikilink")    |                                        | [黃麗嫦](../Page/黃麗嫦.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L22  | [良景](../Page/良景邨.md "wikilink")                                 |                                        | [程志紅](../Page/程志紅.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")           |
| L23  | [田景](../Page/田景邨.md "wikilink")                                 |                                        | [李洪森](../Page/李洪森.md "wikilink")         | [工聯會](../Page/工聯會.md "wikilink")           |
| L24  | [寶田](../Page/寶田邨.md "wikilink")                                 |                                        | [蘇嘉雯](../Page/蘇嘉雯.md "wikilink")         | [新民黨](../Page/新民黨_\(香港\).md "wikilink")    |
| L25  | [建生](../Page/建生邨.md "wikilink")                                 |                                        | [陳文華](../Page/陳文華_\(民建聯\).md "wikilink") | [民建聯](../Page/民建聯.md "wikilink")           |
| L26  | [兆康](../Page/兆康苑.md "wikilink")                                 |                                        | [陳樹英](../Page/陳樹英.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L27  | [景峰](../Page/景峰花園.md "wikilink")                                |                                        | [何杏梅](../Page/何杏梅.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")    |
| L28  | [富泰](../Page/富泰邨.md "wikilink")                                 |                                        | [陳文偉](../Page/陳文偉.md "wikilink")         | [工聯會](../Page/工聯會.md "wikilink")           |
| L29  | [屯門鄉郊](../Page/藍地.md "wikilink")                                |                                        | [陶錫源](../Page/陶錫源.md "wikilink")         | [城鄉居民共和協會](../Page/城鄉居民共和協會.md "wikilink") |
| 委任議員 |                                                                 | *懸空*                                   |                                          | \[1\]                                      |
|      | [周錦祥](../Page/周錦祥.md "wikilink")                                | 獨立                                     |                                          |                                            |
|      | [林德亮](../Page/林德亮.md "wikilink")                                | [新界社團聯會](../Page/新界社團聯會.md "wikilink") |                                          |                                            |
|      | [羅煌楓](../Page/羅煌楓.md "wikilink")                                | [經民聯](../Page/經民聯.md "wikilink")       |                                          |                                            |
|      | [雲天壯](../Page/雲天壯.md "wikilink")                                | [新界社團聯會](../Page/新界社團聯會.md "wikilink") |                                          |                                            |
| 當然議員 | 屯門鄉事委員會主席                                                       |                                        | [劉皇發](../Page/劉皇發.md "wikilink")         | [經民聯](../Page/經民聯.md "wikilink")           |

| 選區號碼 | 選區                                                              | 議員                                     | 所屬政黨                                     | 備註                                                                |
| ---- | --------------------------------------------------------------- | -------------------------------------- | ---------------------------------------- | ----------------------------------------------------------------- |
| L01  | [屯門市中心](../Page/屯門市中心.md "wikilink")                            |                                        | [歐志遠](../Page/歐志遠.md "wikilink")         | [新界社團聯會](../Page/新界社團聯會.md "wikilink")                            |
| L02  | [兆](../Page/兆安苑.md "wikilink")[置](../Page/置樂花園.md "wikilink")   |                                        | [林頌鎧](../Page/林頌鎧.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L03  | [兆](../Page/兆麟苑.md "wikilink")[翠](../Page/翠寧花園.md "wikilink")   |                                        | [盧民漢](../Page/盧民漢.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L04  | [安定](../Page/安定邨.md "wikilink")                                 |                                        | [江鳳儀](../Page/江鳳儀.md "wikilink")         | [民協](../Page/民協.md "wikilink")                                    |
| L05  | [友愛南](../Page/友愛邨.md "wikilink")                                |                                        | [蔣月蘭](../Page/蔣月蘭.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L06  | [友愛北](../Page/友愛邨.md "wikilink")                                |                                        | [陳雲生](../Page/陳雲生.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L07  | [翠](../Page/翠林花園.md "wikilink")[興](../Page/大興花園.md "wikilink")  |                                        | [朱耀華](../Page/朱耀華_\(區議員\).md "wikilink") | 獨立                                                                |
| L08  | [山景](../Page/山景邨.md "wikilink")                                 |                                        | [吳觀鴻](../Page/吳觀鴻.md "wikilink")         | 獨立                                                                |
| L09  | [景](../Page/山景邨.md "wikilink")[興](../Page/大興邨.md "wikilink")    |                                        | [陳有海](../Page/陳有海.md "wikilink")         | [工聯會](../Page/工聯會.md "wikilink")/[民建聯](../Page/民建聯.md "wikilink") |
| L10  | [興](../Page/大興邨.md "wikilink")[澤](../Page/澤豐花園.md "wikilink")   |                                        | [徐　帆](../Page/徐帆_\(區議員\).md "wikilink")  | [工聯會](../Page/工聯會.md "wikilink")/[民建聯](../Page/民建聯.md "wikilink") |
| L11  | [新墟](../Page/屯門新墟.md "wikilink")                                |                                        | [古漢強](../Page/古漢強.md "wikilink")         | [新界社團聯會](../Page/新界社團聯會.md "wikilink")                            |
| L12  | [三聖](../Page/三聖邨.md "wikilink")                                 |                                        | [蘇炤成](../Page/蘇炤成.md "wikilink")         | [新界社團聯會](../Page/新界社團聯會.md "wikilink")                            |
| L13  | [恆福](../Page/恆福花園.md "wikilink")                                |                                        | [李桂芳](../Page/李桂芳_\(區議員\).md "wikilink") | 獨立                                                                |
| L14  | [富](../Page/富健花園.md "wikilink")[新](../Page/新屯門中心.md "wikilink") |                                        | [龍更新](../Page/龍更新.md "wikilink")         | 獨立                                                                |
| L15  | [悅湖](../Page/悅湖山莊.md "wikilink")                                |                                        | [張恆輝](../Page/張恆輝.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L16  | [兆禧](../Page/兆禧苑.md "wikilink")                                 |                                        | [嚴天生](../Page/嚴天生.md "wikilink")         | [民協](../Page/民協.md "wikilink")                                    |
| L17  | [湖景](../Page/湖景邨.md "wikilink")                                 |                                        | [梁健文](../Page/梁健文.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L18  | [蝴蝶](../Page/蝴蝶邨.md "wikilink")                                 |                                        | [蘇愛群](../Page/蘇愛群.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L19  | [樂](../Page/美樂花園.md "wikilink")[翠](../Page/海翠花園.md "wikilink")  |                                        | [何俊仁](../Page/何俊仁.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L20  | [龍門](../Page/龍門居.md "wikilink")                                 |                                        | [龍瑞卿](../Page/龍瑞卿.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L21  | [新](../Page/新圍苑.md "wikilink")[景](../Page/良景邨.md "wikilink")    |                                        | [黃麗嫦](../Page/黃麗嫦.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L22  | [良景](../Page/良景邨.md "wikilink")                                 |                                        | [程志紅](../Page/程志紅.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L23  | [田景](../Page/田景邨.md "wikilink")                                 |                                        | [李洪森](../Page/李洪森.md "wikilink")         | [工聯會](../Page/工聯會.md "wikilink")/[民建聯](../Page/民建聯.md "wikilink") |
| L24  | [寶田](../Page/寶田邨.md "wikilink")                                 |                                        | [陳秀雲](../Page/陳秀雲.md "wikilink")         | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L25  | [建生](../Page/建生邨.md "wikilink")                                 |                                        | [陳文華](../Page/陳文華_\(民建聯\).md "wikilink") | [民建聯](../Page/民建聯.md "wikilink")                                  |
| L26  | [兆康](../Page/兆康苑.md "wikilink")                                 |                                        | [陳樹英](../Page/陳樹英.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L27  | [景峰](../Page/景峰花園.md "wikilink")                                |                                        | [何杏梅](../Page/何杏梅.md "wikilink")         | [民主黨](../Page/民主黨_\(香港\).md "wikilink")                           |
| L28  | [富泰](../Page/富泰邨.md "wikilink")                                 |                                        | [陳文偉](../Page/陳文偉.md "wikilink")         | [工聯會](../Page/工聯會.md "wikilink")/[民建聯](../Page/民建聯.md "wikilink") |
| L29  | [屯門鄉郊](../Page/藍地.md "wikilink")                                |                                        | [陶錫源](../Page/陶錫源.md "wikilink")         | [新界社團聯會](../Page/新界社團聯會.md "wikilink")                            |
| 委任議員 |                                                                 | [劉智鵬](../Page/劉智鵬.md "wikilink")       | 獨立                                       | 屯門區議會副主席                                                          |
|      | [葉順興](../Page/葉順興.md "wikilink")                                | [民建聯](../Page/民建聯.md "wikilink")       |                                          |                                                                   |
|      | [劉業強](../Page/劉業強.md "wikilink")                                | 獨立                                     |                                          |                                                                   |
|      | [羅煌楓](../Page/羅煌楓.md "wikilink")                                | [經濟動力](../Page/經濟動力.md "wikilink")     |                                          |                                                                   |
|      | [周錦祥](../Page/周錦祥.md "wikilink")                                | 獨立                                     |                                          |                                                                   |
|      | [林德亮](../Page/林德亮.md "wikilink")                                | [新界社團聯會](../Page/新界社團聯會.md "wikilink") |                                          |                                                                   |
|      | [雲天壯](../Page/雲天壯.md "wikilink")                                | [新界社團聯會](../Page/新界社團聯會.md "wikilink") |                                          |                                                                   |
| 當然議員 | 屯門鄉事委員會主席                                                       |                                        | [何君堯](../Page/何君堯.md "wikilink")         | 獨立                                                                |

## 爭議事件

2019年3月5日下午，區議會開會討論[明日大嶼願景](../Page/明日大嶼願景.md "wikilink")，有在場市民進行Facebook直播期間被建制派議員陳文偉和葉文斌阻止拍攝，主席兼民建聯議員梁健文更召喚保安和報警處理。最後警察到場，將在Facebook進行直播的市民驅逐離場。泛民區議員江鳳儀認為區議會不允許傳媒以外的人士在會議期間上拍攝，以至驅逐市民離場行為是不公道。旁聽明日大嶼願景，「守護大嶼聯盟」成員陳嘉琳批評議事守則過時，讓市民失去知情權。\[2\]

## 腳註

## 注釋

<references/>

## 外部連結

  - [屯門區議會網站](http://www.districtcouncils.gov.hk/tm/tc_chi/welcome/welcome.html)
  - [屯門區議會網站議員資料](http://www.districtcouncils.gov.hk/tm/tc_chi/members/info/dc_member_list.php)
  - [屯門區議會通訊錄](http://www.districtcouncils.gov.hk/tm/tc_chi/members/info/files/MemberContactDirectory_TM.pdf)

[Category:屯門區議會選區](../Category/屯門區議會選區.md "wikilink")
[Category:屯門區](../Category/屯門區.md "wikilink")

1.  因劉皇發於2015年鄉事委員會改選後成為屯門鄉事委員會主席而出缺。
2.