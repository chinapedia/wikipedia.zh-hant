[缩略图](https://zh.wikipedia.org/wiki/File:The_Communist_Party_Mobilizes_All_Its_Members_to_Practice_Agriculture_in_Order_to_Fight_for_the_Popularization_of_Dazhai_County,_China,_undated_-_Jordan_Schnitzer_Museum_of_Art-_Eugene,_Oregon_-_DSC09540.jpg "fig:缩略图")
**农业学大寨**是[中国在](../Page/中华人民共和国.md "wikilink")20世纪60年代开展的一场运动，依据的是1963年[中国共产党中央委员会主席](../Page/中国共产党中央委员会主席.md "wikilink")[毛泽东发布的一项指示](../Page/毛泽东.md "wikilink")“[工业学大庆](../Page/工业学大庆.md "wikilink")，农业学大寨，全国学[人民解放军](../Page/中国人民解放军.md "wikilink")”。

## 概述

大寨是[山西省](../Page/山西省.md "wikilink")[昔阳县大寨](../Page/昔阳县.md "wikilink")[公社的一个大队](../Page/公社.md "wikilink")（现在[大寨镇下属的](../Page/大寨镇_\(昔阳县\).md "wikilink")[大寨村](../Page/大寨村.md "wikilink")），全村位于山西东部[太行山环境气候恶劣的区域](../Page/太行山.md "wikilink")，包括“七沟八梁一面坡”的[石山](../Page/石山.md "wikilink")。在50、60年代，在[中国共产党山西省昔阳县大寨大队支部书记](../Page/中国共产党.md "wikilink")[陈永贵的领导下](../Page/陈永贵.md "wikilink")，大寨[农民从山下担土到石山上造田](../Page/农民.md "wikilink")，在山顶上开辟[蓄水池](../Page/蓄水池.md "wikilink")，所谓“万里千担一亩田”，改造了本村的生活状态，受到政府的重视。毛泽东认为大寨符合“艰苦奋斗，自力更生”的原则，因此号召全国农民向大寨学习；并在毛泽东的提名下，任命陈永贵为[国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")。

农业学大寨运动当时主要是在冬季农闲时，组织农民进行农田[基本建设](../Page/基本建设.md "wikilink")、兴修[水利](../Page/水利.md "wikilink")。大寨运动将农民组织起来，在当时缺乏政府投资的情况下，也确实利用农民劳动力兴建了许多重要的农田水利设施；如[河南省](../Page/河南省.md "wikilink")[林县的](../Page/林县.md "wikilink")[红旗渠就是利用农民自身的力量](../Page/红旗渠.md "wikilink")，解决了深山区农民的用水问题。这场运动一直持续到[文化大革命结束](../Page/文化大革命.md "wikilink")。

## 图库

<File:洪水铺> 农业学大寨 1975 - panoramio.jpg|洪水铺 农业学大寨 1975 <File:1966-02>
农业学大寨.jpg|周恩来視察农业学大寨運動 <File:1968-01> 1968年 农业学大寨
陈永贵.jpg|1968年 农业学大寨 陈永贵 <File:1968-01> 1968年
山西昔阳县武家坪大队农业学大寨.jpg|1968年 山西昔阳县武家坪大队农业学大寨
<File:1968-01> 1967年 大寨农田收获玉米.jpg|1967年 大寨农田收获玉米
<File:pingyaoxuedazhai.JPG>|[平遥县街头仍然可以看到当年的标语](../Page/平遥县.md "wikilink")
<File:1968-03> 1968年 大寨学习毛泽东语录.jpg|1968年 大寨学习毛泽东语录

{{-}}

[Category:中华人民共和国政治运动](../Category/中华人民共和国政治运动.md "wikilink")
[Category:中华人民共和国农业史](../Category/中华人民共和国农业史.md "wikilink")
[Category:1963年中国](../Category/1963年中国.md "wikilink")
[Category:中华人民共和国政治口号](../Category/中华人民共和国政治口号.md "wikilink")