[Pneumothorax_CT.jpg](https://zh.wikipedia.org/wiki/File:Pneumothorax_CT.jpg "fig:Pneumothorax_CT.jpg")影像。在胸腔的邊緣有著引流管，而圖中黑色一片就是鄰近於肺膜間(黑)和肋骨(白)的內腔。心臟則在圖中央。\]\]
**氣胸**（又稱**肺膜穿**、**爆肺**）是指氣體不正常地進入[胸膜腔](../Page/胸膜腔.md "wikilink")，導致[肺葉跟](../Page/肺葉.md "wikilink")[胸壁分離](../Page/胸壁.md "wikilink")，形成積氣狀態，更可能影響患者呼吸。\[1\]

氣胸可以分為**原發性氣胸**跟**繼發性氣胸**兩種。\[2\]\[3\]前者一般並無明顯原因，而後者則通常因現存肺部的病理問題而誘發，例如[慢性阻塞性肺病](../Page/慢性阻塞性肺病.md "wikilink")，又或者[胸腔受到外在的物理創傷所引致](../Page/胸腔.md "wikilink")。在特定的情況下，當胸膜腔內的壓力高過外界的大氣壓力時，並將縱隔推向健側，使健側肺亦受壓縮，從而使通氣面積減少和產生肺內分流；同時，心臟及大靜脈和肺血管受到壓迫，造成回心靜脈血流受阻，減少[心排出量](../Page/心排出量.md "wikilink")，導致患者缺氧、[休克](../Page/休克.md "wikilink")，這種情況稱為**[張氣胸](../Page/張氣胸.md "wikilink")**。張力性氣胸是一個醫療緊急情況，除非通過有效的治療方法逆轉，否則患者會因為供氧不足和低血壓而不斷惡化，甚至死亡
\[4\]。

一般而言，氣胸可以從仔細的病史調查和檢查診斷出來，而[X光造影則可以確認診斷](../Page/X光造影.md "wikilink")；[電腦斷層掃描亦可以監視體積較小的氣胸](../Page/電腦斷層掃描.md "wikilink")。輕微的氣胸一般不需要特別治療，但需要連續地以胸部X光片監察狀況。較嚴重的情況，就可利用[胸腔穿刺或](../Page/胸腔穿刺.md "wikilink")[胸廓造口術把胸膜腔內空氣引流至體外](../Page/胸廓造口術.md "wikilink")。如果氣胸是重複發生的，則需要進行治療或進行預防措施，例如[胸膜固定术](../Page/肋膜黏連術.md "wikilink")。

## 外傷性氣胸

### 閉合性氣胸

如果患者胸部的傷口在漏入一定量的外界[空氣後愈合了](../Page/空氣.md "wikilink")，並不再有外界空氣入侵，此類氣胸稱作閉合性氣胸（亦稱作單純性氣胸或閉合型氣胸）。患者胸部傷口的大小與愈合時間有關。胸膜腔中積累的氣壓的高低與氣胸對患者的影響程度也有關。外界空氣流入胸膜腔量較少，肺組織萎陷不及30%的患者，氣胸對患者的影響並不是很大，患者可能會感覺到不同程度的胸悶，但並不影響肺臟和呼吸系統的正常運作，慢慢地胸膜腔中積累的空氣便會被自周圍的肺組織自然地吸收；但是外界空氣入胸膜腔量較多，肺組織萎陷超過30%的患者，可能在不同程度上影響患者身體的正常呼吸循環，單靠肺組織自然的吸收是不夠的，需要人工進行胸部穿刺，抽取胸膜腔中積累的空氣，降低肺壓，使肺組織擴張，繼續正常的呼吸。

### 開放性氣胸

開放性氣胸是三類氣胸現像中較為嚴重的一類，也是最少見的一類。指患者的胸壁受到損傷後，外界空氣自由地由胸壁上的傷口進入患者的胸膜腔，積累，肺臟[萎陷](../Page/萎陷.md "wikilink")，不時便會使患者呼吸困難。胸壁傷口的大小與氣胸對患者肺臟是施加的壓力而導致的肺萎陷有著直接的關係。如不及時愈合胸壁的傷口、穿刺胸部做漏氣解壓，（單靠胸穿刺而不事先愈合傷口便抽氣是難以解除胸部壓力的）胸膜腔會迅速地被外界空氣填滿，胸部的壓力逐漸上升。當胸膜腔內的壓力高過外界的[大氣壓力時](../Page/大氣壓力.md "wikilink")，患者身體的整個肺組織、循環和呼吸系統均會受到嚴重的障礙。對患者的呼吸循環周期產生縱隔擺動，引致患者神志不清，甚至缺氧、[休克](../Page/休克.md "wikilink")。這時氣胸已非常嚴重，已為[高壓性氣胸](../Page/高壓性氣胸.md "wikilink")。

### 高壓性氣胸

當患者胸壁的傷口未能及時愈合，胸膜腔內積累的外界空氣對肺組織施加的壓力高過外界的大氣壓力，這時胸部已形成高壓區域，產生對患者的生命危險。這種高壓性氣胸（亦稱張力性氣胸、壓力性氣胸、活瓣性氣胸）是指患者胸部的傷口已形成單向活瓣，即單向體內吸進空氣，而空氣一旦侵入患者體內傷口的活瓣便隨即閉合，使空氣無法逸出。在這種情況下，即使醫療人員能夠通過胸部穿刺為患者的胸部解壓一時，但在解壓後不久，胸部的壓力會再度上升。

## 自發性氣胸

自發性氣胸是一種慢性的胸部[內科疾病](../Page/內科疾病.md "wikilink")，成因較多。病人的支氣管、胸膜或肺大皰破裂，空氣進入胸膜腔壓縮肺組織，使患者呼吸困難。如果侵入胸膜腔的空氣是因為肺部組織的破裂，胸部的氣體需要被抽出，一般是用[抽氣針來做](../Page/抽氣針.md "wikilink")。自發性氣胸一般發生在高而瘦的青年人身上，由於一般[男性比](../Page/男性.md "wikilink")[女性高](../Page/女性.md "wikilink")，男性發病率較高。可分為原發性氣胸和繼發性氣胸二種。原發性氣胸（也作特發性氣胸）通常具有疝氣或是肺皰的特性；繼發性氣胸的可能因慢性阻塞性肺病而致。原發性的一般會在沒有肺病病史，高而瘦，約15至40歲的人身上發生，特別會在年輕人身上發生。其他自發性氣胸的病因包括：

  - [肺結核](../Page/肺結核.md "wikilink")
  - [肺炎](../Page/肺炎.md "wikilink")
  - [哮喘](../Page/哮喘.md "wikilink")
  - [肺纖維化](../Page/肺纖維化.md "wikilink")
  - [肺癌](../Page/肺癌.md "wikilink")
  - [空隙性肺病](../Page/空隙性肺病.md "wikilink")

同時自發性氣胸也可能發生在沒有外傷和胸部撞傷的場合，這類性的氣胸是由[肺泡破裂](../Page/肺泡.md "wikilink")，導致肺部收縮。如果病人的肺同時發生自發性氣胸，醫生會建議通過胸外科手術用切除、縫扎、燒結等方法處理肺皰。

### 月經性氣胸

由女性月經引起的氣胸屬自發性氣胸中的一種特殊類型，影響著月經期間的女子，並且隨著月經周期反復發作，這種氣胸較為少見。其特征是患者月經期來臨並非意味著氣胸的發作，然而患者氣胸的發作卻普見於月經期間\[5\]。其發生與胸腔子宮內膜症及膈肌小孔的有著密切的關係。醫療人員無法在找出患者肺組織的漏氣部位，因為積累於胸膜腔的氣體多來自肺組織外，與另兩類自發性氣胸明顯不同\[6\]。

## 成因

  - [胸部](../Page/胸部.md "wikilink")[受傷](../Page/受傷.md "wikilink")
  - [肺部](../Page/肺部.md "wikilink")[氣壓損傷](../Page/氣壓.md "wikilink")
  - 慢性[肺病](../Page/肺病.md "wikilink")，如[肺氣腫](../Page/肺氣腫.md "wikilink")、[哮喘等](../Page/哮喘.md "wikilink")
  - 急性[感染](../Page/感染.md "wikilink")
  - 肺部被刺穿
  - 慢性感染，如[結核病](../Page/結核病.md "wikilink")
  - [癌症](../Page/癌症.md "wikilink")
  - [月經](../Page/月經.md "wikilink")，
    因為[子宮內膜組織異位形成](../Page/子宮內膜組織異位形成.md "wikilink")
  - [嚴重急性呼吸系統綜合症](../Page/嚴重急性呼吸系統綜合症.md "wikilink")[1](http://yukz.com/ap/dr/chy/chy_0602or01.html)

有一些氣胸是和受傷和病變無關， 即自發性氣胸(多數發生於瘦高的青壯年男性)。

## 症狀

氣胸的症狀主要是[胸痛和](../Page/胸痛.md "wikilink")[呼吸困難](../Page/呼吸.md "wikilink")，嚴重的面色會發青。肺部穿孔時，空氣在傷口流入[胸腔的聲音](../Page/胸腔.md "wikilink")（胸腔擠壓的聲音）可以分辨出氣胸。
微弱的穿肺聲也有時被聽到。如果氣胸得不到治療，身體會漸漸[缺氧](../Page/缺氧.md "wikilink")，最終會[休克和](../Page/休克.md "wikilink")[昏迷](../Page/昏迷.md "wikilink")。
同時，
[縱隔膜從傷處移走會阻塞肺](../Page/縱隔膜.md "wikilink")[靜脈](../Page/靜脈.md "wikilink")，導致心臟容量和泵[血量減少](../Page/血.md "wikilink")。嚴重的氣胸可以在數分鐘內置人於[死地](../Page/死.md "wikilink")。

氣胸也可以在某些治療過程中出現，例如於頸靜脈或胸部的靜脈插入導管。在不常見的病例中，氣胸被視為嚴重的併發症，需要即時治療。其他可能的原因包括[通風系統](../Page/通風系統.md "wikilink")、肺氣腫和[肺炎](../Page/肺炎.md "wikilink")。

## 診斷

通過聽診器診斷，有氣胸那邊的肺是聽不到呼吸聲的。配合[超聲波對](../Page/超聲波.md "wikilink")[胸壁的衝擊可以使診斷更準確](../Page/胸壁.md "wikilink")。（緊急時或無手術用具時，可以敲一敲胸部附近，如有特殊回音，即有可能是氣胸）如果對症狀有懷疑，可以照[X光](../Page/X射線.md "wikilink")，但是對於嚴重的氣胸，應該先做緊急救護。

在胸部X光片裏深刻的溝是氣胸的特徵， 為一個向肋膈低則描繪受影響的那一邊肺。
從圖中觀察，肋骨和膜片集會在X-射線圖以一個深刻的溝標誌來顯示患處。

## 區分

氣胸的症狀同時是以下疾病的症狀:

  - [心肌梗死](../Page/心肌梗死.md "wikilink")：同時有呼吸困難和胸痛，但是心肌梗死的胸痛是緊束的、發自中心的，同時延伸去左手、下巴和腹部。當然心肌梗死的患者可能會有肺病。
  - [肺氣腫](../Page/肺氣腫.md "wikilink")：肺組織失去功能，流失和被空氣取代，導致呼吸困難、吸氣量減少和吸氣次數增加，但是肺氣腫是慢性的，
    而氣胸是急性的。
  - [肺栓塞](../Page/肺栓塞.md "wikilink")：有胸痛、呼吸困難和紫紺等酷似自發性氣胸的臨床表現，但病人往往有咯血和低熱，並常有下肢或[盆腔栓塞性](../Page/盆腔.md "wikilink")[靜脈炎](../Page/靜脈.md "wikilink")、[骨折](../Page/骨折.md "wikilink")、嚴重[心臟病](../Page/心臟病.md "wikilink")、[心室纖維顫震等病史](../Page/心室纖維顫震.md "wikilink")，或發生在長期臥床的老年患者。體檢和X線檢查有助於鑒別。
  - [肺大皰](../Page/肺大皰.md "wikilink")：位於肺周邊部位的肺大皰有時在X線下被誤為氣胸。從不同角度作胸部透視，可見肺大皰或[支氣管源囊腫為圓形或卵圓形透光區](../Page/支氣管.md "wikilink")，在大皰的邊緣看不到發線狀氣胸線，皰內有細小的條紋理，為肺小葉或血管的殘遺物。肺大皰向周圍膨脹，將肺壓向肺尖區、肋膈角和心膈角，而氣胸則呈胸外側的透光帶，其中無肺紋理可見。

其他如消化性潰瘍穿孔，膈疝、胸膜炎和肺癌等，有時因急起的胸痛，上腹痛和氣急等，亦應注意與自發性氣胸鑒別。小心的病史判斷和一張胸部X光片可以使診斷更準確。

## 急救

### 胸部損傷

胸部如果被刺穿，需要立即覆蓋傷處，並以[凡士林或](../Page/凡士林.md "wikilink")[膠布密封](../Page/膠布.md "wikilink")，以免空氣繼續經傷處流入。無菌的膠布是較理想的選擇，
但是所有氣密的物質，例如玻璃紙和香煙盒也可以用。密封後，需要開一個小孔（振動筏）來使吸氣時排出空氣。

胸部刺穿的患者需要密切監察，防止引發對生命有危險的張力性氣胸。

### 入院前護理

多數救護員可以進行針刺抽氣， 以減低胸部的壓力。
如果情況惡化，[導管抽氣也可能需要](../Page/導管抽氣.md "wikilink")，包括有知覺的病人。可能的話，進行額外的治療和即時送病人到[醫院治療](../Page/醫院.md "wikilink")。沒有經過適當的治療的話，氣胸患者是不能用飛機運送的。

## 治療

輕微的氣胸一般不需要特別治療，但需要連續地以胸部X光片監察狀況，但是醫生會向絕大部分的病人提供純氧，
以加快康復進度。嚴重些，就以[胸腔穿刺抽氣治療](../Page/胸腔穿刺.md "wikilink")，即以針筒刺入胸膜腔內抽走空氣。

嚴重的氣胸需要[胸廓造口術](../Page/胸廓造口術.md "wikilink")（又稱為胸腔管手術、胸腔閉式引流朮），
方法是插入一根導管，接入液封引流瓶，以抽出胸腔內部的空氣，以使肺部重新張開，同時以X光片監察狀況。如果情況好轉到一定程度，可以停止抽出空氣。

如果肺部被刺穿，是需要注意狀況的，但是一般氣道是安全的，導管也被插入之後，只需要繼續監察狀況便可。

如果氣胸是重複發生的，需要進行治療或進行預防措施，例如[肋膜黏連術](../Page/肋膜黏連術.md "wikilink")。如果氣胸是由肺泡引起的話，
就需要切除肺泡。肋膜黏連術有兩種：[化學肋膜黏連術和](../Page/化學肋膜黏連術.md "wikilink")[機械肋膜黏連術](../Page/機械肋膜黏連術.md "wikilink")。[化學肋膜黏連術是利用化學物質](../Page/化學肋膜黏連術.md "wikilink")，來刺激炎症，
達到修補肺膜的效果。用來進行化學肋膜黏連術的物質包括[滑石](../Page/滑石.md "wikilink")、[血液](../Page/血液.md "wikilink")、[四環素和](../Page/四環素.md "wikilink")[博來黴素](../Page/博來黴素.md "wikilink")。[機械肋膜黏連術不需要化學物](../Page/機械肋膜黏連術.md "wikilink")，而是粗糙化胸壁，使肺部組織攻擊胸壁，使其結疤，來達到修補肺膜的效果。
同時，也可以進行胸腔鏡手術，來移除包著肺部的肋膜內層。以上手術都可以以微創手術進行，來減輕病人的痛苦。

## 流行病學

經年齡調整後的年均發病率﹝AAIR﹞顯示，男性患上氣胸的機會較女性高出三至六倍。菲什曼\[7\]\[8\]在研究中提出以每十萬人年計算，男女患上原發性氣胸的AAIR分別為7.4和1.2。身高比平均值較高的人，他們患上氣胸的AAIR也相對增加
- 至少為76英寸（1.93米）高的人，每十萬人年約有200個案例。瘦削的身材也似乎增加了患上原發性氣胸的風險。\[9\]

此外，男性和女性煙民患上原發性氣胸，相對於同性別的非吸煙者患上的機會高出約22倍和9倍。\[10\]而個人吸煙的程度越兇，風險會有「大於線性」的效果：比方說每天吸食10支香煙的人，會比非吸煙者高出20倍患上氣胸的機會；每天消耗20支香煙的，則會高出約100倍。\[11\]

在繼發自發性氣胸的病例當中，男性和女性的AAIR大概為6.3和2.0\[12\]\[13\]，而復發的風險則取決於患者本身有否任何潛在的肺病及其嚴重性。一旦發生了第二次氣胸，病人之後再復發的機會極高。目前來說沒有周詳的研究調查兒童的發病率\[14\]，但估計是每年約十萬份之5至10。\[15\]

除外張力性氣胸以外，因為氣胸而死亡的案例是非常罕見的。\[16\]據英國統計數字顯示，每年每一百萬人當中會有1.26位男性和0.62位女性會因為氣胸死亡，其中中老年和繼發性氣胸的患者會有較高的死亡風險。\[17\]

## 歷史

[Jean_marc_gaspard_itard_1775_hi.jpg](https://zh.wikipedia.org/wiki/File:Jean_marc_gaspard_itard_1775_hi.jpg "fig:Jean_marc_gaspard_itard_1775_hi.jpg")
[尚·伊塔爾](../Page/尚·伊塔爾.md "wikilink")，一名[何內·雷納克的學生](../Page/雷納克.md "wikilink")，於1803年最先識別氣胸。之後雷納克於1819年親自描述這個病\[18\]。在反肺結核療程之前出現，故意造成的氣胸是用來治療肺結核，這被稱作「肺部放鬆」。

## 注释

## 外部链接

  - [自發性氣胸怎麼辦？](https://web.archive.org/web/20070528131721/http://www.ncku.edu.tw/~surgery/www/pdoc/p06.htm)
  - [新华网：疾病大全－氣胸](http://news.xinhuanet.com/health/2005-09/14/content_3489218.htm)
  - [上足赛酿班古拉式惨剧
    申花小将可能提前告别绿茵](http://sports.sina.com.cn/j/2006-08-06/01342380557.shtml)

[Category:呼吸系统疾病](../Category/呼吸系统疾病.md "wikilink")
[Category:筋膜障礙](../Category/筋膜障礙.md "wikilink")

1.

2.

3.

4.

5.  “月經性氣胸1例報告”，《[人民軍醫](../Page/人民軍醫.md "wikilink")》，1994年，[劉文欽](../Page/劉文欽.md "wikilink")、[黨政華](../Page/黨政華.md "wikilink")、[景冬櫻等撰](../Page/景冬櫻.md "wikilink")

6.  [認識氣胸](http://ill.91.cn/surgery/ptwk/qx/qxfl/2005-11-05/110882.htm)，就醫網

7.

8.

9.
10.

11.
12.

13.

14.

15.

16.
17.
18. *Traite de l'auscultation mediate et des maladies des poumons et du
    coeur*，Laennec RTH，第二部，[巴黎](../Page/巴黎.md "wikilink")，1819年