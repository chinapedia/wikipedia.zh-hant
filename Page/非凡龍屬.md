**非凡龍屬**（[學名](../Page/學名.md "wikilink")：*Quaesitosaurus*）又名**異常龍**，意為「奇特的蜥蜴」，是[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")[納摩蓋吐龍科](../Page/納摩蓋吐龍科.md "wikilink")[恐龍的一個屬](../Page/恐龍.md "wikilink")，在1983年由Kurzanov與Bannikov所敘述、命名。[模式種為](../Page/模式種.md "wikilink")**東方非凡龍**（*Q.
orientalis*）。非凡龍能夠長到23公尺，生活於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[桑托階到](../Page/桑托階.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")，約8500萬至6500萬年前。

非凡龍的化石只有一個部份頭骨，發現於[蒙古的](../Page/蒙古.md "wikilink")[巴魯恩戈約特組](../Page/巴魯恩戈約特組.md "wikilink")（Barun
Goyot
Formation）。牠們的頭骨長而低矮，外形類似[馬](../Page/馬.md "wikilink")，嘴部前段有釘狀牙齒。這些頭部特徵類似[梁龍](../Page/梁龍.md "wikilink")，而遺失的身體部分也被認為類似[梁龍科](../Page/梁龍科.md "wikilink")。

同樣也只發現頭骨的[納摩蓋吐龍](../Page/納摩蓋吐龍.md "wikilink")，若不是非凡龍的生物變異個體，就是非凡龍的近親。

## 參考資料

  - Hunt, A.P., Meyer, C.A., Lockley, M.G., and Lucas, S.G. (1994)
    "Archaeology, toothmarks and sauropod dinosaur taphonomy". *Gaia:
    Revista de Geociencias*, Museu Nacional de Historia Natural, Lisbon,
    Portugal, 10: 225-232.
  - Kurzanov, S. and Bannikov, A. (1983). "A new sauropod from the Upper
    Cretaceous of Mongolia". *Paleontologicheskii Zhurnal* 2: 90-96.

## 外部連結

  - [Saltasauridae](https://web.archive.org/web/20080603194923/http://www.users.qwest.net/~jstweet1/saltasauridae.htm)
    from *Thescelosaurus*\! (向下滾動)
  - [*Quaesitosaurus*](http://www.dinodata.org/index.php?option=com_content&task=view&id=7304&Itemid=67)
    from DinoData.org (技術)

[Category:納摩蓋吐龍科](../Category/納摩蓋吐龍科.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")