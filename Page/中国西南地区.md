[Southwest_China.svg](https://zh.wikipedia.org/wiki/File:Southwest_China.svg "fig:Southwest_China.svg")
在[中国地理概念中](../Page/中国地理.md "wikilink")，西南地区涵盖[中国西南部的广大腹地](../Page/中華人民共和國.md "wikilink")，主要包括[四川盆地](../Page/四川盆地.md "wikilink")、[云贵高原和](../Page/云贵高原.md "wikilink")[青藏高原南部地区](../Page/青藏高原.md "wikilink")。

在中国行政区划概念中，狭义的西南地区（亦称为**大西南**），即指传统的“西南三省”：川（[四川省](../Page/四川省.md "wikilink")）、滇（[云南省](../Page/云南省.md "wikilink")）、黔（[贵州省](../Page/贵州省.md "wikilink")）。其中，这里四川省在地理上的范围为[中华民国时期的省域](../Page/中华民国.md "wikilink")，不包括原[西康省之辖域](../Page/西康省.md "wikilink")（含今[雅安市](../Page/雅安市.md "wikilink")、[攀枝花市](../Page/攀枝花市.md "wikilink")、[甘孜藏族自治州和](../Page/甘孜藏族自治州.md "wikilink")[凉山彝族自治州](../Page/凉山彝族自治州.md "wikilink")），但包括今[重庆市之辖域](../Page/重庆市.md "wikilink")。

而行政区划中广义的西南地区则有“西南四省（区）”、“西南五省（区、市）”和“西南六省（区、市）”三个版本，其中后两种说法常出现在如今中国区域经济合作的相关活动之中。

  - [中华人民共和国成立之初](../Page/中华人民共和国.md "wikilink")，曾在省级行政区划之上建立了[大区一级政府](../Page/大行政區.md "wikilink")，其中[西南行政区管辖当时的](../Page/西南行政区.md "wikilink")[重庆市](../Page/重庆主城區.md "wikilink")、[川东行署区](../Page/川东行署区.md "wikilink")、[川西行署区](../Page/川西行署区.md "wikilink")、[川南行署区](../Page/川南行署区.md "wikilink")、[川北行署区](../Page/川北行署区.md "wikilink")、贵州省、云南省、西康省、[西藏地方和](../Page/西藏地方.md "wikilink")[昌都地区等十个省级行政单位](../Page/昌都地区.md "wikilink")，直至1954年各大行政区被撤销。西南行政区的原十个省级行政单位经过历年的调整、合并，在1955年形成了包含四川省、贵州省、云南省和西藏自治区（1955年设筹备委员会，1965年正式成立）共四个省级行政单位的稳定区划格局，川、黔、滇、藏四地合称为“西南四省（区）”。
  - 1997年国务院设立重庆直辖市，西南地区持续稳定四十余年的三省一区的行政区划格局部分调整，在此之后重庆市与四川省、贵州省、云南省和西藏自治区合称为“西南五省（区、市）”。
  - [改革开放后特别是近十年来](../Page/改革开放.md "wikilink")，由于[西部大开发战略的实施及](../Page/西部大开发.md "wikilink")[西南出海大通道建设等诸多因素](../Page/西南出海大通道.md "wikilink")，本身地处[华南地区西部的](../Page/华南地区.md "wikilink")[广西壮族自治区也经常与](../Page/广西壮族自治区.md "wikilink")[渝](../Page/渝.md "wikilink")、[川](../Page/川.md "wikilink")、[黔](../Page/黔.md "wikilink")、[滇](../Page/滇.md "wikilink")、[藏五地合称为](../Page/藏.md "wikilink")“西南六省（区、市）”。由于西藏独特的政治、经济和区位的条件，西南地区的区域经济合作大多数时候并未见西藏自治区的身影，因此西南五省（区、市）往往也指桂、渝、川、黔、滇五地。

西南地区是中国内部经济差距最大的地理大区，2016年，[四川省GDP为](../Page/四川省.md "wikilink")32680.5亿元，[西藏区GDP为](../Page/西藏区.md "wikilink")1150亿元，[重庆市GDP为](../Page/重庆市.md "wikilink")17558.76亿元，[云南省GDP为](../Page/云南省.md "wikilink")14869.95.91亿元，[贵州省GDP为](../Page/贵州省.md "wikilink")11734.43亿元。

## 参考文献

{{-}}

[西南](../Category/中国六大地理区.md "wikilink")
[Category:中国西南地区](../Category/中国西南地区.md "wikilink")