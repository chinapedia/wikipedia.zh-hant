**顾仁恩**是1930年代和1940年代中国著名的自由传道人，1950年代初成为[控诉运动中最早被打击的](../Page/控诉运动.md "wikilink")4名基督徒领袖之一（另外三人是[陈文渊](../Page/陈文渊.md "wikilink")、[梁小初](../Page/梁小初.md "wikilink")、[朱友渔](../Page/朱友渔.md "wikilink")）。

顾仁恩在少年时在上海圣公会开办的学校读书，但对宗教没有兴趣。青年時期曾因为失恋跳黄浦江自杀。后来由于长相不错，考入电影公司，出演过影片《[空谷兰](../Page/空谷兰.md "wikilink")》，成为默片时代一个小有名气的電影明星，也常有小報登载他的緋聞。1930年代，顾仁恩在一次[宋尚节带领的奋兴会中聽道大受感動悔改](../Page/宋尚节.md "wikilink")，脫離娱乐圈，并奉獻自己作傳道人，成为一个当时有影响的奋兴布道家。顾仁恩没有读过神学，讲道的风格也类似演戏，自由发挥，与宋尚节类似。他最出名的一次布道会是1939年—1940年间在[昆明](../Page/昆明.md "wikilink")，和[赵君影](../Page/赵君影.md "wikilink")、[石新我三人一連五十三天講](../Page/石新我.md "wikilink")「在財主門口的拉撒路」。此后回到上海。1942年，顾仁恩在北平布道，常去宋尚节的寓所"恩典院"交谈。期间其家人由香港乘日本船北上，途中为盟国鱼雷击沉，全家溺死。

1951年2月24日-3月，顾仁恩应邀前往青岛，在济宁路[浸信会和](../Page/浸信会.md "wikilink")[地方教会的](../Page/地方教会.md "wikilink")[龙山路聚会所布道](../Page/龙山路聚会所.md "wikilink")，被政府以散布反动言论的罪名逮捕，是“帝国主义留下来的反动走狗”，在4月控诉运动开始时，即成为批判对象。甚至接受美国教士范爱莲赠送巧克力，即被指为美国特务。会上控诉者问：“像顾仁恩这样的人，该杀不该杀？”，台下怒吼：“该杀！该杀！”\[1\]。
《又四十年》记载“一位从青岛来的姓王的代表。他控诉顾仁恩在青岛造谣和被捕的事。控诉完了, 就问听众:“这样的人,
该杀不该杀?”台下有一个人喊了一声:“该杀\!”
第二天[人民日报上就有消息来说](../Page/人民日报.md "wikilink"):“台下一片怒吼:‘该杀\!
该杀\!’”这些控诉实际上成了以后全国控诉的样板。”所以可以确定这是[共党的构陷](../Page/共党.md "wikilink")。
顾仁恩被判十五年，送到[青海](../Page/青海.md "wikilink")[勞改](../Page/勞改.md "wikilink")，修鐵路。[刑满释放后到](../Page/刑满释放.md "wikilink")[北京](../Page/北京.md "wikilink")，才发现老婆早已被[共党安排與他離婚](../Page/共党.md "wikilink")，由于无[户口可以投奔](../Page/户口.md "wikilink")，被迫回到青海，最后在那里去世。

## 外部链接

  - [于力工：顾仁恩先生](https://web.archive.org/web/20050915063048/http://www.steering.org/163/st16309.htm)
  - [钮志芳、乔维熊：控诉基督教败类顾仁恩（1950年）](http://libproject.hkbu.edu.hk/trsimage/christian/er00346.pdf)

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/上海人.md "wikilink")
[G](../Category/中国基督教新教人物.md "wikilink")

1.  \[
    <http://libproject.hkbu.edu.hk/was40/detail?channelid=7336&searchword=idno=00344>
    王重生：控诉美国特务顾仁恩（1950年）\]