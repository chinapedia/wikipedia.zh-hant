[广州地区](../Page/广州.md "wikilink")[体育](../Page/体育.md "wikilink")[运动源远流长](../Page/运动.md "wikilink")，是[中国较早发展近代体育的](../Page/中国.md "wikilink")[地方](../Page/地方.md "wikilink")。根据[文献资料记载](../Page/文献.md "wikilink")，中国民间传统体育中的[武术](../Page/中国武术.md "wikilink")、[划龙舟](../Page/划龙舟.md "wikilink")、[舞狮](../Page/舞狮.md "wikilink")、[游泳](../Page/游泳.md "wikilink")、[舞龙](../Page/舞龙.md "wikilink")、[踢毽](../Page/踢毽.md "wikilink")、[信鸽](../Page/信鸽.md "wikilink")、[秋千](../Page/秋千.md "wikilink")、[放风筝](../Page/放风筝.md "wikilink")、[抢花炮](../Page/抢花炮.md "wikilink")、[抽陀螺](../Page/抽陀螺.md "wikilink")、[跳绳](../Page/跳绳.md "wikilink")、[登高等项目在古代广州相当流行](../Page/登高.md "wikilink")。[端午节](../Page/端午节.md "wikilink")[赛龙舟的活动在](../Page/赛龙舟.md "wikilink")[南汉的史籍中确有记载](../Page/南汉.md "wikilink")。目前[羽毛球运动成绩位于](../Page/羽毛球.md "wikilink")[全国前列](../Page/中国大陆.md "wikilink")。

## 体育场馆

  - [天河体育中心](../Page/天河体育中心.md "wikilink")
  - [广东奥林匹克体育中心](../Page/广东奥林匹克体育中心.md "wikilink")
  - [广州新体育馆](../Page/广州新体育馆.md "wikilink")
  - [二沙头体育训练中心](../Page/二沙头体育训练中心.md "wikilink")
  - [黄村体育训练基地](../Page/黄村体育训练基地.md "wikilink")
  - [广东省体育学校培训基地](../Page/广东省体育学校培训基地.md "wikilink")
  - [广东省人民体育场](../Page/广东省人民体育场.md "wikilink")
  - [芳村体育中心](../Page/芳村体育中心.md "wikilink")
  - [黄埔体育场](../Page/黄埔体育场.md "wikilink")
  - [英东体育场](../Page/英东体育场.md "wikilink")
  - [黄村体育场](../Page/黄村体育场.md "wikilink")
  - [越秀山体育场](../Page/越秀山体育场.md "wikilink")
  - [广州大学城体育中心](../Page/广州大学城体育中心.md "wikilink")
  - [广州工人体育场](../Page/广州工人体育场.md "wikilink")
  - [燕子岗体育场](../Page/燕子岗体育场.md "wikilink")
  - [沙面网球场](../Page/沙面网球场.md "wikilink")
  - [珠江游泳场](../Page/珠江游泳场.md "wikilink")
  - [越秀游泳场](../Page/越秀游泳场.md "wikilink")
  - [海角红楼游泳场](../Page/海角红楼游泳场.md "wikilink")
  - [沙面泳场](../Page/沙面泳场.md "wikilink")
  - [广州市航海俱乐部](../Page/广州市航海俱乐部.md "wikilink")

## 运动会

  - 1987年：中华人民共和国[第六届全国运动会](../Page/中华人民共和国第六届全运会.md "wikilink")
  - 1991年：[第一届](../Page/1991年女子世界杯足球赛.md "wikilink")[女子世界杯足球赛](../Page/女子世界杯足球赛.md "wikilink")
  - 2001年：中华人民共和国[第九届全国运动会](../Page/中华人民共和国第九届全运会.md "wikilink")
  - 2007年：[中华人民共和国第八届全国少数民族传统体育运动会](../Page/中华人民共和国第八届全国少数民族传统体育运动会.md "wikilink")
  - 2010年：[第十六届](../Page/2010年亚洲运动会.md "wikilink")[亚洲（夏季）运动会](../Page/亚洲运动会.md "wikilink")、[第一屆亞洲殘疾人運動會](../Page/2010年亚洲残疾人运动会.md "wikilink")

## 体育竞赛

  - [中国羽毛球公开赛](../Page/中国羽毛球公开赛.md "wikilink")
  - [广州国际女子网球公开赛](../Page/广州国际女子网球公开赛.md "wikilink")
  - [五羊杯全国象棋冠军赛](../Page/五羊杯全国象棋冠军赛.md "wikilink")
  - [2006年世界摔跤锦标赛](../Page/2006年世界摔跤锦标赛.md "wikilink")
  - [第49届世界乒乓球锦标赛团体赛](../Page/第49届世界乒乓球锦标赛团体赛.md "wikilink")

## 群众体育

  -
## 运动团体

### 足球

  - [中超球队](../Page/中国足球超级联赛.md "wikilink")：[广州恒大足球俱乐部](../Page/广州恒大足球俱乐部.md "wikilink")、[广州富力足球俱乐部](../Page/广州富力足球俱乐部.md "wikilink")
  - [中甲球队](../Page/中国足球甲级联赛.md "wikilink")：[广东日之泉足球俱乐部](../Page/广东日之泉足球俱乐部.md "wikilink")
  - [中乙球队](../Page/中国足球乙级联赛.md "wikilink")：广州青年

### 篮球

  - [CBA球队](../Page/中国男子篮球职业联赛.md "wikilink")：
    [广州证券男子篮球队](../Page/广州证券男子篮球队.md "wikilink")

### 排球

  - [中国女子排球联赛球队](../Page/中国女子排球联赛.md "wikilink")：[广东恒大职业排球俱乐部](../Page/广东恒大职业排球俱乐部.md "wikilink")

### 棒球

  - [CBL球队](../Page/中国棒球联赛.md "wikilink")：
    [广东猎豹队](../Page/广东猎豹队.md "wikilink")

### 羽毛球

  - [中国羽毛球俱乐部超级联赛球队](../Page/中国羽毛球俱乐部超级联赛.md "wikilink")：
    [广州粤羽俱乐部](../Page/广州粤羽俱乐部.md "wikilink")

[\*](../Category/广州体育.md "wikilink")