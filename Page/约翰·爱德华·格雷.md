<small></small> |image = Gray John Edward 1800-1875.png |image_size =
|caption = |birth_date = 1800年2月12日 |birth_place =
西密德蘭郡[沃爾薩爾](../Page/沃爾薩爾.md "wikilink") |death_date =
 |death_place = 倫敦 |residence = |citizenship = |nationality = 英國
|ethnicity = |fields = 動物學 |workplaces = |alma_mater =
|doctoral_advisor = |academic_advisors = |doctoral_students =
|notable_students = |known_for = |author_abbrev_bot =
|author_abbrev_zoo = |influences = |influenced = |awards = |religion =
|signature= John Edward Gray Signature.svg |footnotes = }} **约翰·爱德华·格雷**
[FRS](../Page/Fellow_of_the_Royal_Society.md "wikilink")（**John Edward
Gray**，），[英国动物学家](../Page/英国.md "wikilink")，1833年和他人一起創立[英国皇家昆虫学会](../Page/英国皇家昆虫学会.md "wikilink")。1840年至1874年聖誕節任[大英博物馆动物学部门负责人](../Page/大英博物馆.md "wikilink")，之後倫敦博物館的自然歷史藏品轉入了[倫敦自然歷史博物館](../Page/倫敦自然歷史博物館.md "wikilink")。他是藥理學家及植物學家[塞繆爾·弗雷德里克·格雷的兒子](../Page/塞繆爾·弗雷德里克·格雷.md "wikilink")，[喬治·羅伯特·格雷的長兄](../Page/喬治·羅伯特·格雷.md "wikilink")。

## 參考文獻

## 外部連結

  -
  - [John Edward Gray, the Indian Pond Heron and
    Walsall](http://www.rspb-walsall.org.uk/gray/index.htm)
    ([RSPB](../Page/RSPB.md "wikilink") Walsall Local Group)

  - [Works by J. E.
    Gray](https://archive.org/search.php?query=creator%3A%22Gray%2C%20John%20Edward%2C%201800-1875%22)
    on [Internet Archive](../Page/Internet_Archive.md "wikilink")

[G](../Category/1800年出生.md "wikilink")
[G](../Category/1875年逝世.md "wikilink")
[G](../Category/英國動物學家.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:沃爾索爾人](../Category/沃爾索爾人.md "wikilink")
[Category:英國植物學家](../Category/英國植物學家.md "wikilink")
[Category:英國昆蟲學家](../Category/英國昆蟲學家.md "wikilink")