**朱玲**（），[中国](../Page/中国.md "wikilink")[山东](../Page/山东.md "wikilink")[莱芜人](../Page/莱芜.md "wikilink")，前[中国女排](../Page/中国女排.md "wikilink")[副攻](../Page/排球#副攻手.md "wikilink")，现任[四川省体育局局长](../Page/四川省.md "wikilink")。

朱于1970年开始在[重庆接受](../Page/重庆.md "wikilink")[排球训练](../Page/排球.md "wikilink")，1975年进入四川省队，1979年进入国家队。朱随队先后获得了1981年[世界杯和](../Page/世界盃排球賽.md "wikilink")1984年[洛杉矶奥运会冠军](../Page/1984年夏季奥林匹克运动会.md "wikilink")。退役后，朱进入[四川大学中文系学习](../Page/四川大学.md "wikilink")，毕业后进入四川省体育局工作，2004年晋升为四川省体育局局长。

## 個人三大賽榮譽

| 年份    | 世界大賽（主辦國）                                                                                                                                                                                                                  |
| ----- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1981年 | **第3屆世界盃女排賽**冠軍（）                                                                                                                                                                                                          |
| 1984年 | [洛杉磯奧運會冠軍](../Page/1984年夏季奧林匹克運動會.md "wikilink")（[Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg") [美國](../Page/美國.md "wikilink")） |
|       |                                                                                                                                                                                                                            |

## 参考资料

  - [钱江晚报·朱玲的寻常一天](http://www.zjol.com.cn/gb/node2/node802/node807/node209853/node209900/userobject15ai2440938.html)
  - [体坛周报·老女排队员朱玲成川军火车头
    梦想培养冠军](http://sports.tom.com/2007-08-01/0G1F/85063542.html)

[Category:中国女子排球运动员](../Category/中国女子排球运动员.md "wikilink")
[Category:中国奥运排球运动员](../Category/中国奥运排球运动员.md "wikilink")
[Category:四川省人民政府官员](../Category/四川省人民政府官员.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:1984年夏季奧林匹克運動會獎牌得主](../Category/1984年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1984年夏季奥林匹克运动会排球运动员](../Category/1984年夏季奥林匹克运动会排球运动员.md "wikilink")
[Category:奧林匹克運動會排球獎牌得主](../Category/奧林匹克運動會排球獎牌得主.md "wikilink")
[Category:四川大学校友](../Category/四川大学校友.md "wikilink")
[Category:莱芜籍运动员](../Category/莱芜籍运动员.md "wikilink")
[L玲](../Category/朱姓.md "wikilink")