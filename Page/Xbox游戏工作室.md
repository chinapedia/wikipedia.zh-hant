**Xbox游戏工作室**（），原名**微軟工作室**（Microsoft Studios）、**微軟遊戲工作室**（Microsoft
Game Studios）和**微軟遊戲**（Microsoft
Games），是[微軟的全資子公司](../Page/微軟.md "wikilink")，负责[Windows](../Page/Microsoft_Windows.md "wikilink")、[Xbox](../Page/Xbox.md "wikilink")、[Xbox
360](../Page/Xbox_360.md "wikilink")、[Xbox
One多个平台遊戲的](../Page/Xbox_One.md "wikilink")[開發與](../Page/開發.md "wikilink")[發行](../Page/發行.md "wikilink")。Xbox游戏工作室為數個微軟所有的第一方開發商發行遊戲，包括了[Lionhead
Studios與](../Page/Lionhead_Studios.md "wikilink")[Rare](../Page/Rare.md "wikilink")，亦有發行由如[Bioware與](../Page/Bioware.md "wikilink")[Bizarre
Creations等第三方開發商製作的遊戲](../Page/Bizarre_Creations.md "wikilink")。

## 历史

[Microsoft_Game_Studios.svg](https://zh.wikipedia.org/wiki/File:Microsoft_Game_Studios.svg "fig:Microsoft_Game_Studios.svg")
[Microsoft_Studios_logo.svg](https://zh.wikipedia.org/wiki/File:Microsoft_Studios_logo.svg "fig:Microsoft_Studios_logo.svg")
微软工作室自成立以来，曾多次收购其他游戏工作室。微軟遊戲工作室於2001年收購了[Bungie](../Page/Bungie.md "wikilink")。[Bungie后于](../Page/Bungie.md "wikilink")2007年脱离微软，成为独立公司。2002年8月MGS自[任天堂與Stamper兄弟手中收購了](../Page/任天堂.md "wikilink")[Rare公司](../Page/Rare.md "wikilink")，兩者分別擁有該公司49%及51%。微軟遊戲工作室在2006年4月6日收購了[Lionhead
Studios](../Page/Lionhead_Studios.md "wikilink")，他們希望以此加強[Xbox
360的發佈量](../Page/Xbox_360.md "wikilink")。微軟亦在2006年收購了遊戲內置廣告公司。

微軟工作室在2014年9月15日宣布以25億美元收購當前十分有名的[Minecraft的製作團隊](../Page/Minecraft.md "wikilink")[Mojang](../Page/Mojang.md "wikilink")。

在2018年[E3发布会上](../Page/E3.md "wikilink")，微软宣布已收购了四家游戏工作室：（Undead
Labs）、[游乐场游戏工作室](../Page/游乐场游戏工作室.md "wikilink") (Playground
Games)、[忍者理论](../Page/忍者理论.md "wikilink") (Ninja Theory)
和，\[1\]并成立由前任[晶体动力总监Darrell](../Page/晶体动力.md "wikilink")
Gallagher领导的新工作室[The
Initiative](../Page/The_Initiative.md "wikilink")。\[2\]

2018年11月，微软宣布收购[黑曜石娱乐和](../Page/黑曜石娱乐.md "wikilink")[InXile娛樂](../Page/InXile娛樂.md "wikilink")\[3\]。

## 內部工作室

  - **[Rare](../Page/Rare.md "wikilink")** -
    [黄金眼007](../Page/黄金眼007.md "wikilink")、[殺手本能系列](../Page/殺手本能.md "wikilink")，微軟於2002年从[任天堂收购而來](../Page/任天堂.md "wikilink")。
  - **[Turn 10](../Page/Turn_10.md "wikilink")** -
    [極限競速系列](../Page/極限競速系列.md "wikilink")，微軟於2004年建立的工作室，目的是為[Xbox家族打造出色的競速遊戲](../Page/Xbox.md "wikilink")。
  - **[343 Industries](../Page/343_Industries.md "wikilink")** -
    自Bungie獨立之後，設立以開發未來的[最後一戰系列作品的遊戲工作室](../Page/最後一戰系列.md "wikilink")，成立于2007年。
  - **[Launchworks](../Page/Launchworks.md "wikilink")** - 2008年成立。
  - **[The Coalition](../Page/The_Coalition.md "wikilink")** -
    接手[战争机器系列](../Page/戰爭機器4.md "wikilink")，微软于2011年收购，当时更名为“微软游戏工作室温哥华”，后在2014年更为现公司名。
  - **[Mojang](../Page/Mojang.md "wikilink")** -
    [我的世界](../Page/我的世界.md "wikilink")，微软于2014年收购。
  - **[游乐场游戏工作室](../Page/游乐场游戏工作室.md "wikilink")** - [极限竞速
    地平线系列](../Page/极限竞速_地平线.md "wikilink")，微软于2018年收购。
  - **[忍者理論](../Page/忍者理論.md "wikilink")** -
    [地狱之刃](../Page/地狱之刃：塞娜的献祭.md "wikilink")、[DmC：惡魔獵人](../Page/DmC：惡魔獵人.md "wikilink")，微软于2018年收购。
  - **** - 系列，微软于2018年收购。
  - **** - ，微软于2018年收购。
  - **[The Initiative](../Page/The_Initiative.md "wikilink")** -
    2018年成立。
  - **[黑曜石娱乐](../Page/黑曜石娱乐.md "wikilink")** -
    [辐射：新维加斯](../Page/辐射：新维加斯.md "wikilink")、系列，微软于2018年收购。
  - **[InXile娛樂](../Page/InXile娛樂.md "wikilink")** - ，微软于2018年收购。

## 曾經擁有的內部工作室

  - **[ACES Game Studio](../Page/Aces_Studio.md "wikilink")** -
    [模擬飛行系列](../Page/微軟模擬飛行.md "wikilink")、戰鬥模擬飛行系列與發售的微軟模擬列車2，於2009年1月23日解散。
  - **[Bungie](../Page/Bungie.md "wikilink")**
    -「馬拉松」、「[最後一戰系列](../Page/最後一戰系列.md "wikilink")」
    ，於2007年10月1日脫離，成為獨立開發商。
  - **[Carbonated Games](../Page/Carbonated_Games.md "wikilink")** -
    旋轉泡泡球、[UNO](../Page/UNO.md "wikilink")，於2008年3月27日解散。
  - **[Digital Anvil](../Page/Digital_Anvil.md "wikilink")** -
    於2006年1月31日解散。
  - **[全效工作室](../Page/全效工作室.md "wikilink")** -
    [世紀帝國系列](../Page/世紀帝國.md "wikilink")、[神話世紀系列](../Page/神話世紀.md "wikilink")、[Halo
    Wars](../Page/Halo_Wars.md "wikilink")，於2008年9月9日關閉，成員回歸總部或是由微软帮助另尋出路。
  - **[Indie Built](../Page/Indie_Built.md "wikilink")** -
    2001年10月賣給[Take
    2](../Page/Take-Two_Interactive.md "wikilink")。
  - **[FASA Studio](../Page/FASA_Studio.md "wikilink")** -
    於2007年9月12日關閉，旗下員工分配至其他微軟所有的遊戲工作室。
  - **[Hired Gun](../Page/Hired_Gun.md "wikilink")** - Windows
    Vista版[最後一戰2](../Page/最後一戰2.md "wikilink")
  - **** - 負責遊戲內置廣告。
  - **[BigPark](../Page/BigPark.md "wikilink")** - joy
    Ride等Kinect游戏，已被整合入微软旗下其它工作室。
  - **[Xbox娱乐工作室](../Page/Xbox娱乐工作室.md "wikilink")** - 负责Xbox
    Live网络的互动电视内容。于2014年10月被正式关闭。
  - **[Lionhead](../Page/Lionhead_Studios.md "wikilink")** -
    [善與惡系列](../Page/善與惡.md "wikilink")、[電影夢工廠](../Page/電影夢工廠.md "wikilink")、[神鬼寓言系列](../Page/神鬼寓言.md "wikilink")，為微軟於2006年4月6日購併；于2016年4月被正式关闭。
  - **[Press Play](../Page/Press_Play.md "wikilink")** -
    马克思与魔术马克笔、马克思：兄弟魔咒，2012年被微软收购；2016年4月被正式关闭。

## 發行的遊戲

本列表按平台排序以及包含由微軟發行的遊戲，該些不以微軟名稱開發的遊戲其開發商記錄在發售日期後。

### PC

  - [帝国时代](../Page/帝国时代.md "wikilink") (1997年10月15日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

      - [世紀帝國:羅馬霸主](../Page/帝国时代：罗马复兴.md "wikilink") (1998年11月5日) -
        [全效工作室](../Page/全效工作室.md "wikilink")

  - [帝国时代:金版](../Page/帝国时代:金版.md "wikilink") (1999年8月24日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

  - [帝国时代II:帝王世紀](../Page/帝国时代II:帝王世紀.md "wikilink") (1999年10月16日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

      - [帝国时代II:征服者入侵](../Page/帝国时代II:征服者入侵.md "wikilink") (2000年8月25日)
        - [全效工作室](../Page/全效工作室.md "wikilink")

  - [帝国时代III](../Page/帝国时代III.md "wikilink") (2005年10月18日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

      - [帝国时代III:群酋爭霸](../Page/帝国时代III:群酋爭霸.md "wikilink") (2006年) -
        [全效工作室](../Page/全效工作室.md "wikilink")
      - [帝国时代III:亚洲王朝](../Page/帝国时代III:亚洲王朝.md "wikilink") (2007年) -
        [全效工作室](../Page/全效工作室.md "wikilink")

  - [帝国时代III收藏版](../Page/帝国时代III收藏版.md "wikilink") (2005年10月18日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

  - [神話世紀](../Page/神話世紀.md "wikilink") (2002年10月31日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

      - [神話世紀:泰坦](../Page/神話世紀:泰坦.md "wikilink") (2003年9月30日) -
        [全效工作室](../Page/全效工作室.md "wikilink")

  - [神話世紀金版](../Page/神話世紀金版.md "wikilink") (2004年6月30日) -
    [全效工作室](../Page/全效工作室.md "wikilink")

  - [帝国时代Online](../Page/帝国时代Online.md "wikilink") (2011年8月16日) -
    [羅伯特娛樂](../Page/羅伯特娛樂.md "wikilink")

  - [心靈殺手](../Page/心靈殺手.md "wikilink") (2007年) - [Remedy
    Entertainment](../Page/Remedy_Entertainment.md "wikilink")

  - [怒首領蜂MAXINUM](../Page/怒首領蜂MAXINUM.md "wikilink")(2012年) -

  - (2000年3月16日)

  - [Asheron's Call](../Page/Asheron's_Call.md "wikilink") (1999年12月1日)
    - [Turbine](../Page/Turbine.md "wikilink")

  - [Asheron's Call 2](../Page/Asheron's_Call_2.md "wikilink")

  - [Chip's Challenge](../Page/Chip's_Challenge.md "wikilink") (1989年)

  - [Close Combat](../Page/Close_Combat.md "wikilink") (1996年) - [Atomic
    Games](../Page/Atomic_Games.md "wikilink")

  - [Close Combat: A Bridge Too
    Far](../Page/Close_Combat:_A_Bridge_Too_Far.md "wikilink") (1997年) -
    Atomic Games

  - [Close Combat III: The Russian
    Front](../Page/Close_Combat_III:_The_Russian_Front.md "wikilink")
    (1998年) - Atomic Games

  - [Combat Flight Simulator: WWII Europe
    Series](../Page/Combat_Flight_Simulator.md "wikilink") (1998年)

  - [Combat Flight Simulator 2: WWII Pacific
    Theater](../Page/Combat_Flight_Simulator_2.md "wikilink") (2000年)

  - [Combat Flight Simulator 3: Battle for
    Europe](../Page/Combat_Flight_Simulator_3:_Battle_for_Europe.md "wikilink")
    (2002年)

  - [Combat Flight Simulator
    4](../Page/Combat_Flight_Simulator_4.md "wikilink") - (TBA)

  - [Deadly Tide](../Page/Deadly_Tide.md "wikilink") (1996年)

  - [Dungeon Siege](../Page/Dungeon_Siege.md "wikilink") (2002年) - [Gas
    Powered Games](../Page/Gas_Powered_Games.md "wikilink")

  - [Dungeon Siege: Legends of
    Aranna](../Page/Dungeon_Siege:_Legends_of_Aranna.md "wikilink")
    (2003年) - [Gas Powered
    Games](../Page/Gas_Powered_Games.md "wikilink")

  - [Dungeon Siege II](../Page/Dungeon_Siege_II.md "wikilink") (2005年) -
    [Gas Powered Games](../Page/Gas_Powered_Games.md "wikilink")

  - [神鬼寓言:失落之章](../Page/神鬼寓言:失落之章.md "wikilink")([Fable:The Lost
    Chapters](../Page/Fable:The_Lost_Chapters.md "wikilink")2005年) -
    [Lionhead Studios](../Page/Lionhead_Studios.md "wikilink")

  - [Freelancer](../Page/Freelancer.md "wikilink") (2003年) - [Digital
    Anvil](../Page/Digital_Anvil.md "wikilink")

  - [Fighter Ace](../Page/Fighter_Ace.md "wikilink") (1997年12月31日)

  - [Fury<sup>3</sup>](../Page/Fury3.md "wikilink") (1996年6月24日)

  - [戰爭機器](../Page/戰爭機器.md "wikilink") (2007年11月6日) - [EPIC
    Games](../Page/EPIC_Games.md "wikilink")

  - [最後一戰：戰鬥進化](../Page/最後一戰：戰鬥進化.md "wikilink") (2003年) -
    [Bungie](../Page/Bungie.md "wikilink")

  - [最後一戰2](../Page/最後一戰2.md "wikilink") (2007年) - [Hired
    Gun](../Page/Hired_Gun.md "wikilink") /
    [Bungie](../Page/Bungie.md "wikilink")

  - \- (1996年) [Terminal
    Reality](../Page/Terminal_Reality.md "wikilink")

  - [Impossible Creatures](../Page/Impossible_Creatures.md "wikilink")
    (2003年) - [Relic
    Entertainment](../Page/Relic_Entertainment.md "wikilink")

      - [Impossible Creatures: Insect
        Invasion](../Page/Impossible_Creatures:_Insect_Invasion.md "wikilink")
        (2003年) - [Relic
        Entertainment](../Page/Relic_Entertainment.md "wikilink")

  - [Kings & Myths: The Age
    Collection](../Page/Kings_&_Myths:_The_Age_Collection.md "wikilink")
    (2003年) - [全效工作室](../Page/全效工作室.md "wikilink")

  - [Links 2003](../Page/Links_2003.md "wikilink") (2002年) - [Indie
    Built](../Page/Indie_Built.md "wikilink")

  - [Links 2003: Championship
    Courses](../Page/Links_2003:_Championship_Courses.md "wikilink")
    (2003年)

  - [Links Championship
    Edition](../Page/Links_Championship_Edition.md "wikilink") (2003年)

  - [Links LS Classic](../Page/Links_LS_Classic.md "wikilink") (?) -
    [Indie Built](../Page/Indie_Built.md "wikilink")

  - [Marvel Universe
    Online](../Page/Marvel_Universe_Online.md "wikilink") (2007年) -
    [Cryptic Studios](../Page/Cryptic_Studios.md "wikilink") and [Marvel
    Interactive](../Page/Marvel_Interactive.md "wikilink")

  - (?)

  - [MechCommander 2](../Page/MechCommander_2.md "wikilink") (2001年)

  - [MechWarrior 4:
    Vengeance](../Page/MechWarrior_4:_Vengeance.md "wikilink") (2000年)

      - [MechWarrior 4: Black
        Knight](../Page/MechWarrior_4:_Black_Knight.md "wikilink")
        (2001年) - [Cyberlore
        Studios](../Page/Cyberlore_Studios.md "wikilink")
      - [MechWarrior 4: Clan Mech
        Pak](../Page/MechWarrior_4:_Clan_Mech_Pak.md "wikilink") (2002年)
        - [Cyberlore Studios](../Page/Cyberlore_Studios.md "wikilink") /
        [FASA Interactive](../Page/FASA_Interactive.md "wikilink")
      - [MechWarrior 4: Inner Sphere Mech
        Pack](../Page/MechWarrior_4:_Inner_Sphere_Mech_Pack.md "wikilink")
        (2002年) - [Cyberlore
        Studios](../Page/Cyberlore_Studios.md "wikilink") / [FASA
        Interactive](../Page/FASA_Interactive.md "wikilink")
      - [MechWarrior 4:
        Mercenaries](../Page/MechWarrior_4:_Mercenaries.md "wikilink")
        (2002年) - [Cyberlore
        Studios](../Page/Cyberlore_Studios.md "wikilink")

  - [Microsoft Baseball
    2000](../Page/Microsoft_Baseball_2000.md "wikilink") (1999年)

  - [Microsoft Baseball
    2001](../Page/Microsoft_Baseball_2001.md "wikilink") (2000年)

  - [Microsoft CART Precision
    Racing](../Page/Microsoft_CART_Precision_Racing.md "wikilink")
    (1997年)

  - [Microsoft Entertainment Pack: The Puzzle
    Collection](../Page/Microsoft_Entertainment_Pack:_The_Puzzle_Collection.md "wikilink")
    (1997年12月31日)

  - [微軟模擬飛行'95](../Page/微軟模擬飛行'95.md "wikilink") (1996年)

  - [微軟模擬飛行'98](../Page/微軟模擬飛行'98.md "wikilink") (1997年)

  - [微軟模擬飛行2000](../Page/微軟模擬飛行2000.md "wikilink") (1999年)

  - [微軟模擬飛行2002](../Page/微軟模擬飛行2002.md "wikilink") (2001年)

  - [微軟模擬飛行2004:百年飛行史](../Page/微軟模擬飛行2004:百年飛行史.md "wikilink") (2003年)

  - [微軟模擬飛行X](../Page/微軟模擬飛行X.md "wikilink") (2006年) - [ACES
    Studio](../Page/ACES_Studio.md "wikilink")

  - [微軟模擬飛行X:加速度](../Page/微軟模擬飛行X:加速度.md "wikilink") (2007年) - [ACES
    Studio](../Page/ACES_Studio.md "wikilink")

  - [Microsoft Golf](../Page/Microsoft_Golf.md "wikilink") ([June
    1](../Page/June_1.md "wikilink") 1995年)

  - [Microsoft Golf Version
    3.0](../Page/Microsoft_Golf_Version_3.0.md "wikilink") (1996年12月12日)

  - [Microsoft Golf 1998
    Edition](../Page/Microsoft_Golf_1998_Edition.md "wikilink")
    (1998年6月16日)

  - [Microsoft Pinball
    Arcade](../Page/Microsoft_Pinball_Arcade.md "wikilink") (1998年)

  - [Microsoft Soccer](../Page/Microsoft_Soccer.md "wikilink") (December
    12, 1996年)

  - [Microsoft Space
    Simulator](../Page/Microsoft_Space_Simulator.md "wikilink") (1994年)

  - [微軟模擬列車](../Page/微軟模擬列車.md "wikilink") (2001年) - [Kuju
    Entertainment](../Page/Kuju_Entertainment.md "wikilink")

  - [微軟模擬列車2](../Page/微軟模擬列車2.md "wikilink") (重新宣佈，截至2007年) - [ACES
    Studio](../Page/ACES_Studio.md "wikilink")

  - [瘋狂城市賽車](../Page/瘋狂城市賽車.md "wikilink") (1999年) - [Angel
    Studios](../Page/Angel_Studios.md "wikilink")

  - [瘋狂城市賽車2](../Page/瘋狂城市賽車2.md "wikilink") (2000年) - [Angel
    Studios](../Page/Angel_Studios.md "wikilink")

  - [Monster Truck Madness](../Page/Monster_Truck_Madness.md "wikilink")
    (1996年) - [Terminal Reality](../Page/Terminal_Reality.md "wikilink")

  - [Monster Truck Madness
    2](../Page/Monster_Truck_Madness_2.md "wikilink") (1998年) -
    [Terminal Reality](../Page/Terminal_Reality.md "wikilink")

  - [Motocross Madness](../Page/Motocross_Madness.md "wikilink") (1998年)
    - [Rainbow Studios](../Page/Rainbow_Studios.md "wikilink")

  - [Motocross Madness 2](../Page/Motocross_Madness_2.md "wikilink")
    (2000年) - [Rainbow Studios](../Page/Rainbow_Studios.md "wikilink")

  - [Mythica](../Page/Mythica.md "wikilink") (已取消)

  - [NBA Full Court Press](../Page/NBA_Full_Court_Press.md "wikilink")
    (1998年3月30日)

  - [NBA Inside Drive 2000](../Page/NBA_Inside_Drive_2000.md "wikilink")
    (1999年)

  - [NFL Fever 2000](../Page/NFL_Fever_2000.md "wikilink") (1999年)

  - (1998年)

  - [Pandora's Box](../Page/Pandora's_Box.md "wikilink") (1999年8月31日)

  - [Racing Madness 2](../Page/Racing_Madness_2.md "wikilink") (?)

  - [RalliSport Challenge](../Page/RalliSport_Challenge.md "wikilink")
    (2002年) - Digital Illusions CE

  - [Return of Arcade Anniversary
    Edition](../Page/Return_of_Arcade_Anniversary_Edition.md "wikilink")
    (1997年) - [Namco](../Page/Namco.md "wikilink")

  - [王國的興起](../Page/王國的興起.md "wikilink") (2003年) - [Big Huge
    Games](../Page/Big_Huge_Games.md "wikilink")

  - [王國的興起：政權保衛戰](../Page/王國的興起：政權保衛戰.md "wikilink") (2004年) - [Big Huge
    Games](../Page/Big_Huge_Games.md "wikilink")

  - [王國的興起：傳說再現](../Page/王國的興起：傳說再現.md "wikilink") (2006年) - [Big Huge
    Games](../Page/Big_Huge_Games.md "wikilink")

  - [SkiFree](../Page/SkiFree.md "wikilink") (1991年12月31日)

  - (2007年) - FASA Studio

  - [Urban Assault](../Page/Urban_Assault.md "wikilink") (1998年7月31日) -

  - [動物園大亨](../Page/動物園大亨.md "wikilink") (2001年) - [Blue Fang
    Games](../Page/Blue_Fang_Games.md "wikilink")

      - [Zoo Tycoon: Dinosaur
        Digs](../Page/Zoo_Tycoon:_Dinosaur_Digs.md "wikilink") (2002年) -
        [Blue Fang Games](../Page/Blue_Fang_Games.md "wikilink")
      - [Zoo Tycoon: Marine
        Mania](../Page/Zoo_Tycoon:_Marine_Mania.md "wikilink") (2002年) -
        [Blue Fang Games](../Page/Blue_Fang_Games.md "wikilink")

  - [Zoo Tycoon Complete
    Collection](../Page/Zoo_Tycoon_Complete_Collection.md "wikilink")
    (2003年) - [Blue Fang Games](../Page/Blue_Fang_Games.md "wikilink")

  - [動物園大亨2](../Page/動物園大亨2.md "wikilink") (2004年) - Blue Fang Games

      - [Zoo Tycoon 2: Endangered
        Species](../Page/Zoo_Tycoon_2:_Endangered_Species.md "wikilink")
        (2005年) - [Blue Fang
        Games](../Page/Blue_Fang_Games.md "wikilink")
      - [Zoo Tycoon 2: African
        Adventure](../Page/Zoo_Tycoon_2:_African_Adventure.md "wikilink")
        (2006年) - [Blue Fang
        Games](../Page/Blue_Fang_Games.md "wikilink")

  - [Zoo Tycoon 2: Zookeeper
    Collection](../Page/Zoo_Tycoon_2:_Zookeeper_Collection.md "wikilink")
    (2006年) - [Blue Fang Games](../Page/Blue_Fang_Games.md "wikilink")

      - [Zoo Tycoon 2: Dino Danger
        Pack](../Page/Zoo_Tycoon_2:_Dino_Danger_Pack.md "wikilink")
        (2006) - [Blue Fang
        Games](../Page/Blue_Fang_Games.md "wikilink")
      - [Zoo Tycoon 2: Marine
        Mania](../Page/Zoo_Tycoon_2:_Marine_Mania.md "wikilink") (2006年)
        - [Blue Fang Games](../Page/Blue_Fang_Games.md "wikilink")
      - [Zoo Tycoon 2: Extinct
        Animals](../Page/Zoo_Tycoon_2:_Extinct_Animals.md "wikilink")
        (2007年) - [Blue Fang
        Games](../Page/Blue_Fang_Games.md "wikilink")
      - [spore](../Page/spore.md "wikilink") (2008年) - [Blue Fang
        Games](../Page/Blue_Fang_Games.md "wikilink")

### Xbox

  - [Amped: Freestyle Snowboarding](../Page/Amped.md "wikilink") (2001年)
    - [Indie Built](../Page/Indie_Built.md "wikilink")
  - [Amped 2](../Page/Amped_2.md "wikilink") (2003年) - [Indie
    Built](../Page/Indie_Built.md "wikilink")
  - [Azurik:Rise of
    Perathia](../Page/Azurik:Rise_of_Perathia.md "wikilink") (2001年) -
    Adrenium Games
  - [霹靂酷炫貓:穿越時空](../Page/霹靂酷炫貓:穿越時空.md "wikilink") (2002年) - Artoon
  - [霹靂酷炫貓:穿越時空 2](../Page/霹靂酷炫貓:穿越時空_2.md "wikilink") (2004年) - Artoon
  - [致命快艇](../Page/致命快艇.md "wikilink") (2001年) - Stormfront Studios
  - [雷霆戰隊](../Page/雷霆戰隊.md "wikilink") (2003年) - Digital Anvil
  - [百戰壞松鼠](../Page/百戰壞松鼠.md "wikilink") (2005年) -
    [Rare](../Page/Rare.md "wikilink")
  - [反恐精英](../Page/反恐精英.md "wikilink") (2003年) - Valve
  - [王牌飛行員:復仇大道](../Page/王牌飛行員:復仇大道.md "wikilink") (2003年)
  - [Dinosaur Hunting](../Page/Dinosaur_Hunting.md "wikilink") (2003年)
  - [神鬼寓言](../Page/神鬼寓言.md "wikilink") (2004) - [Lionhead
    Studios](../Page/Lionhead_Studios.md "wikilink")/Big Blue Box
    Studios
  - [極限競速](../Page/極限競速.md "wikilink") (2005年)
  - [瘋狂大亂鬥](../Page/瘋狂大亂鬥.md "wikilink") (2001年) - Blitz Games
  - [鬼屋大冒險](../Page/鬼屋大冒險.md "wikilink") (2003年) - Rare
  - [最後一戰：戰鬥進化](../Page/最後一戰：戰鬥進化.md "wikilink") (2001年) -
    [Bungie](../Page/Bungie.md "wikilink")
  - [最後一戰2](../Page/最後一戰2.md "wikilink") (2004年) -
    [Bungie](../Page/Bungie.md "wikilink")
  - [Inside Pitch 2003](../Page/Inside_Pitch_2003.md "wikilink") (2003年)
  - [翡翠帝國](../Page/翡翠帝國.md "wikilink") (2005年) - BioWare
  - [Kakuto Chojin](../Page/Kakuto_Chojin.md "wikilink") (2002年) - Dream
    Publishing
  - [熾焰帝國:英雄傳說](../Page/熾焰帝國:英雄傳說.md "wikilink") (2005年) - Phantagram
  - [熾焰帝國:十字軍東征](../Page/熾焰帝國:十字軍東征.md "wikilink") (2004年) - Phantagram
  - [功夫大亂鬥](../Page/功夫大亂鬥.md "wikilink") (2003年) - Just Add Monsters
  - [Links 2004](../Page/Links_2004.md "wikilink") (2003年) - [Indie
    Built](../Page/Indie_Built.md "wikilink")
  - [Magatama](../Page/Magatama.md "wikilink") (2003年)
  - [機甲先鋒](../Page/機甲先鋒.md "wikilink") (2002年) - Day 1
  - [機甲先鋒2:孤狼](../Page/機甲先鋒2:孤狼.md "wikilink") (2004年) - Day 1
  - [瘋狂城市賽車3](../Page/瘋狂城市賽車3.md "wikilink") (2003年) - Digital Illusions
    CE
  - [N.U.D.E.@Natural Ultimate Digital
    Entertainment](../Page/N.U.D.E.@Natural_Ultimate_Digital_Entertainment.md "wikilink")
    (2003年) - Rocket Studio, Inc. / Red Company
  - [NBA Inside Drive 2002](../Page/NBA_Inside_Drive_2002.md "wikilink")
    (2002年) - High Voltage Software
  - [NBA Inside Drive 2003](../Page/NBA_Inside_Drive_2003.md "wikilink")
    (2002年) - High Voltage Software
  - [NBA Inside Drive 2004](../Page/NBA_Inside_Drive_2004.md "wikilink")
    (2003年) - High Voltage Software
  - [NFL Fever 2002](../Page/NFL_Fever_2002.md "wikilink") (2001年)
  - [NFL Fever 2003](../Page/NFL_Fever_2003.md "wikilink") (2002年)
  - [NFL Fever 2004](../Page/NFL_Fever_2004.md "wikilink") (2003年)
  - [NHL Rivals 2004](../Page/NHL_Rivals_2004.md "wikilink") (2003年)
  - [NightCaster](../Page/NightCaster.md "wikilink") (2002年) - VR1
  - [Oddworld:Munch's
    Oddysee](../Page/Oddworld:Munch's_Oddysee.md "wikilink") (2001年) -
    Oddworld Inhabitants
  - [OutRun 2](../Page/OutRun_2.md "wikilink") (2004年) - (co-published
    with Sega) Sega-AM2/Sumo Digital
  - [幻影沙塵](../Page/幻影沙塵.md "wikilink") (2004年)
  - [世界街頭賽車](../Page/世界街頭賽車.md "wikilink") (2001年) - Bizarre Creations
  - [世界街頭賽車2](../Page/世界街頭賽車2.md "wikilink") (2003年) - Bizarre Creations
  - [Quantum Redshift](../Page/Quantum_Redshift.md "wikilink") (2002年) -
    Curly Monsters
  - [越野挑戰賽](../Page/越野挑戰賽.md "wikilink") (2002年) - Digital Illusions CE
  - [越野挑戰賽2](../Page/越野挑戰賽2.md "wikilink") (2004年) - Digital Illusions
    CE
  - [莎木II](../Page/莎木II.md "wikilink") (2003年) - Sega-AM2
  - [Sneakers](../Page/Sneakers.md "wikilink") (2002年) - Mediavision
    Corporation
  - [魔幻戰士](../Page/魔幻戰士.md "wikilink") (2004年) - Climax Studios
  - [道風蓮拳](../Page/道風蓮拳.md "wikilink") (2003年) - Studio Gigante
  - [The Wild Rings](../Page/The_Wild_Rings.md "wikilink") (2003年)
  - [職業網球大聯盟](../Page/職業網球大聯盟.md "wikilink") (2003年) - Power & Magic,
    [Indie Built](../Page/Indie_Built.md "wikilink")
  - [True Fantasy Live
    Online](../Page/True_Fantasy_Live_Online.md "wikilink") (已取消) -
    Level-5
  - [巫毒大冒險](../Page/巫毒大冒險.md "wikilink") (2003年) - Beep Industries
  - [Whacked\!](../Page/Whacked!.md "wikilink") (2002年) - Presto Studios
  - [Xbox Music Mixer](../Page/Xbox_Music_Mixer.md "wikilink") (2003年) -
    Wild Tangent

### Xbox 360

  - [心靈殺手](../Page/心靈殺手.md "wikilink") (TBA) - [Remedy
    Entertainment](../Page/Remedy_Entertainment.md "wikilink")
  - [Banjo-Kazooie 3](../Page/Banjo-Kazooie_3.md "wikilink") (2007年) -
    [Rare](../Page/Rare.md "wikilink")
  - [藍龍](../Page/藍龍.md "wikilink") (2006年) -
    [Mistwalker](../Page/Mistwalker.md "wikilink") /
    [Artoon](../Page/Artoon.md "wikilink")
  - [除暴戰警](../Page/除暴戰警.md "wikilink") (2007年) - [Real Time
    Worlds](../Page/Real_Time_Worlds.md "wikilink")
  - [全民派對](../Page/全民派對.md "wikilink") (2005年) - [Game
    Republic](../Page/Game_Republic.md "wikilink")
  - [神鬼寓言2](../Page/神鬼寓言2.md "wikilink") (2007年) - [Lionhead
    Studios](../Page/Lionhead_Studios.md "wikilink")
  - [極限競速2](../Page/極限競速2.md "wikilink") (2007年) - [Turn
    10](../Page/Turn_10.md "wikilink")
  - [極限競速3](../Page/極限競速3.md "wikilink") (2009年) - [Turn
    10](../Page/Turn_10.md "wikilink")
  - [極限競速4](../Page/極限競速4.md "wikilink") (2011年) - [Turn
    10](../Page/Turn_10.md "wikilink")
  - [瘋狂大亂鬥2](../Page/瘋狂大亂鬥2.md "wikilink") (2007年) - [Hudson
    Soft](../Page/Hudson_Soft.md "wikilink")
  - [戰爭機器](../Page/戰爭機器.md "wikilink") (2006年) - [Epic
    Games](../Page/Epic_Games.md "wikilink")
  - [戰爭機器2](../Page/戰爭機器2.md "wikilink") (2008年) - [Epic
    Games](../Page/Epic_Games.md "wikilink")
  - [戰爭機器3](../Page/戰爭機器3.md "wikilink") (2011年) - [Epic
    Games](../Page/Epic_Games.md "wikilink")
  - [最後一戰3](../Page/最後一戰3.md "wikilink") (2007年) -
    [Bungie](../Page/Bungie.md "wikilink")
  - [最後一戰3: ODST](../Page/最後一戰3:_ODST.md "wikilink") (2009年) -
    [Bungie](../Page/Bungie.md "wikilink")
  - [最後一戰：瑞曲之戰](../Page/最後一戰：瑞曲之戰.md "wikilink") (2010年) -
    [Bungie](../Page/Bungie.md "wikilink")
  - [最後一戰：星環戰役](../Page/最後一戰：星環戰役.md "wikilink") (2008年) -
    [全效工作室](../Page/全效工作室.md "wikilink")
  - [最後一戰 復刻版](../Page/最後一戰_復刻版.md "wikilink") (2011年) - [343
    Industries](../Page/343_Industries.md "wikilink")
  - [最後一戰4](../Page/最後一戰4.md "wikilink") (2012年) - [343
    Industries](../Page/343_Industries.md "wikilink")
  - [凱密歐傳說: 力量元素](../Page/凱密歐傳說:_力量元素.md "wikilink") (2005年) -
    [Rare](../Page/Rare.md "wikilink")
  - [熾焰帝國:末日之環](../Page/熾焰帝國:末日之環.md "wikilink") (2007年) -
    [Phantagram](../Page/Phantagram.md "wikilink")
  - [失落的奧德賽](../Page/失落的奧德賽.md "wikilink") (2007年) -
    [Mistwalker](../Page/Mistwalker.md "wikilink") / [Feel
    Plus](../Page/Feel_Plus.md "wikilink")
  - [Marvel Universe
    Online](../Page/Marvel_Universe_Online.md "wikilink") (2008年) -
    [Cryptic Studios](../Page/Cryptic_Studios.md "wikilink") / [Marvel
    Interactive](../Page/Marvel_Interactive.md "wikilink")
  - [質量效應](../Page/質量效應.md "wikilink") (2007年) -
    [Bioware](../Page/Bioware.md "wikilink")
  - [九十九夜](../Page/九十九夜.md "wikilink") (2006年) - [Q
    Entertainment](../Page/Q_Entertainment.md "wikilink") /
    [Phantagram](../Page/Phantagram.md "wikilink")
  - [完美女煞星](../Page/完美女煞星.md "wikilink") (2005年) -
    [Rare](../Page/Rare.md "wikilink")
  - [世界街頭賽車3](../Page/世界街頭賽車3.md "wikilink") (2005年) - [Bizarre
    Creations](../Page/Bizarre_Creations.md "wikilink")
  - [世界街頭賽車4](../Page/世界街頭賽車4.md "wikilink") (2007年) - [Bizarre
    Creations](../Page/Bizarre_Creations.md "wikilink")
  - [銀星戰將](../Page/銀星戰將.md "wikilink") (2007) -
    [Gamearts](../Page/Gamearts.md "wikilink") /
    [Seta](../Page/Seta.md "wikilink") / [Square
    Enix](../Page/Square_Enix.md "wikilink") 只負責北美地區發行
  - [暗影狂奔](../Page/暗影狂奔.md "wikilink") (2007年) - [FASA
    Interactive](../Page/FASA_Interactive.md "wikilink")
  - [無間戰神](../Page/無間戰神.md "wikilink") (2007年) - [Silicon
    Knights](../Page/Silicon_Knights.md "wikilink")
  - [天誅千亂](../Page/天誅千亂.md "wikilink") (2007年) - [From
    Software](../Page/From_Software.md "wikilink") 只負責北美地區發行
  - [吸血鬼之雨](../Page/吸血鬼之雨.md "wikilink") (2007年) -
    [Artoon](../Page/Artoon.md "wikilink") / [AQ
    Interactive](../Page/AQ_Interactive.md "wikilink") 只負責北美地區發行
  - [寶貝萬歲](../Page/寶貝萬歲.md "wikilink") (2006年) -
    [Rare](../Page/Rare.md "wikilink")

## 参考资料

## 外部連結

  - [Microsoft Game Studios
    ](https://web.archive.org/web/20070501034403/http://www.microsoft.com/games/)
  - [Microsoft Game Studios 台灣](http://www.microsoft.com/taiwan/Games/)
  - [Microsoft的官方Xbox網站](http://www.xbox.com/)
  - [MobyGames上的](../Page/MobyGames.md "wikilink")[Microsoft Game
    Studios](http://www.mobygames.com/company/microsoft-game-studios)簡介

[Category:2002年開業電子遊戲公司](../Category/2002年開業電子遊戲公司.md "wikilink")
[Game](../Category/微软的部门与子公司.md "wikilink")
[\*](../Category/微軟遊戲.md "wikilink")
[Category:第一方開發商](../Category/第一方開發商.md "wikilink")
[Category:美國電子遊戲公司](../Category/美國電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")

1.
2.
3.