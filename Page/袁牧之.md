[Sons_and_Daughters_in_a_Time_of_Storm.ogv](https://zh.wikipedia.org/wiki/File:Sons_and_Daughters_in_a_Time_of_Storm.ogv "fig:Sons_and_Daughters_in_a_Time_of_Storm.ogv")》电影片断，男主角为**袁牧之**\]\]
**袁牧之**（），原名**袁家莱**，[浙江](../Page/浙江.md "wikilink")[宁波人](../Page/宁波.md "wikilink")。[中国现代](../Page/中国.md "wikilink")[戏剧](../Page/戏剧演员.md "wikilink")、[电影编剧](../Page/电影编剧.md "wikilink")、[导演](../Page/导演.md "wikilink")、[演员](../Page/演员.md "wikilink")。第一任夫人[陈波儿](../Page/陈波儿.md "wikilink")1951年去世。第二任夫人[朱世藕是著名](../Page/朱世藕.md "wikilink")[昆曲](../Page/昆曲.md "wikilink")[艺术家](../Page/艺术家.md "wikilink")。长女[袁牧女](../Page/袁牧女.md "wikilink")，次女[袁小牧](../Page/袁小牧.md "wikilink")，儿子[袁牧男](../Page/袁牧男.md "wikilink")。

## 生平

祖籍浙江鄞县。袁牧之早年间参加了[戏剧协社和](../Page/戏剧协社.md "wikilink")[辛酉剧社](../Page/辛酉剧社.md "wikilink")，曾主演过《[文舅舅](../Page/文舅舅.md "wikilink")》（即《[万尼亚舅舅](../Page/万尼亚舅舅.md "wikilink")》）等剧。

20世纪30年代初期参加[左翼](../Page/左翼.md "wikilink")[戏剧活动](../Page/戏剧.md "wikilink")，主演《[五奎桥](../Page/五奎桥.md "wikilink")》、《[怒吼吧！中国](../Page/怒吼吧！中国.md "wikilink")》等进步[话剧](../Page/话剧.md "wikilink")。

后来致力于[电影工作](../Page/电影.md "wikilink")，主演《[桃李劫](../Page/桃李劫.md "wikilink")》、《[风云儿女](../Page/风云儿女.md "wikilink")》、《[生死同心](../Page/生死同心.md "wikilink")》，还编导了《[都市风光](../Page/都市风光.md "wikilink")》、《[马路天使](../Page/马路天使.md "wikilink")》等进步电影。

1935年的抗日名歌《[義勇軍進行曲](../Page/義勇軍進行曲.md "wikilink")》最初的演唱者就是袁牧之和[顧夢鶴](../Page/顧夢鶴.md "wikilink")。

[抗日战争爆发后](../Page/中國抗日戰爭.md "wikilink")，他又转而从事抗战[文艺宣传活动](../Page/文艺.md "wikilink")。1938年到[延安](../Page/延安.md "wikilink")，创建延安电影团。编导了大型[纪录片](../Page/纪录片.md "wikilink")《[延安与八路军](../Page/延安与八路军.md "wikilink")》，1940年加入[中国共产党](../Page/中国共产党.md "wikilink")，[国共内战时期参加了](../Page/国共内战.md "wikilink")[东北电影制片厂](../Page/东北电影制片厂.md "wikilink")(今[長春電影製片廠](../Page/長春電影製片廠.md "wikilink"))的创建和领导工作。

[中华人民共和国建国后任](../Page/中华人民共和国.md "wikilink")[文化部首任电影局局长](../Page/中华人民共和国文化部.md "wikilink")。后被迫害排挤，1954年被迫离职。文革时被下放到湖北丹江五七干校。1978年春因病求医不成（医院受到政治干扰），未得到及时救治，于同年6月30日病逝於[北京](../Page/北京.md "wikilink")。终年69岁。

## 演出影片

  - 1934年：[桃李劫](../Page/桃李劫.md "wikilink")
  - 1935年：[风云儿女](../Page/风云儿女.md "wikilink")、都市风光
  - 1936年：[生死同心](../Page/生死同心.md "wikilink")
  - 1938年：[八百壮士](../Page/八百壮士.md "wikilink")

## 编导故事片

  - 1935年：[都市风光](../Page/都市风光.md "wikilink")
  - 1936年：[马路天使](../Page/马路天使.md "wikilink")

[M牧](../Page/category:袁姓.md "wikilink")

[Category:浙江歌手](../Category/浙江歌手.md "wikilink")
[Category:宁波人](../Category/宁波人.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")
[Category:中国电影男演员](../Category/中国电影男演员.md "wikilink")
[Category:莫斯科东方大学校友](../Category/莫斯科东方大学校友.md "wikilink")
[Category:鄞县人](../Category/鄞县人.md "wikilink")
[M](../Category/城南袁氏.md "wikilink")