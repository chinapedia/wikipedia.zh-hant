**天建**（524年六月—527年九月）是[北魏時期領導](../Page/北魏.md "wikilink")[莫折念生的年号](../Page/莫折念生.md "wikilink")，共计3年餘。

## 大事记

## 出生

## 逝世

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|天建||元年||二年||三年||四年 |-
style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||524年||525年||526年||527年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[甲辰](../Page/甲辰.md "wikilink")||[乙巳](../Page/乙巳.md "wikilink")||[丙午](../Page/丙午.md "wikilink")||[丁未](../Page/丁未.md "wikilink")
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [普通](../Page/普通_\(萧衍\).md "wikilink")（520年正月—527年三月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [大通](../Page/大通_\(萧衍\).md "wikilink")（527年三月—529年九月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [正光](../Page/正光.md "wikilink")（520年七月—525年六月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝明帝元詡年号](../Page/北魏孝明帝.md "wikilink")
      - [孝昌](../Page/孝昌.md "wikilink")（525年六月—528年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏孝明帝元詡年号](../Page/北魏孝明帝.md "wikilink")
      - [真王](../Page/真王_\(破六韩拔陵\).md "wikilink")（523年三月—525年六月）：[北魏時期](../Page/北魏.md "wikilink")[六鎮領導](../Page/六鎮.md "wikilink")[破六韩拔陵年号](../Page/破六韩拔陵.md "wikilink")
      - [天啓](../Page/天启_\(元法僧\).md "wikilink")（525年正月—三月）：[北魏時期領導](../Page/北魏.md "wikilink")[元法僧年号](../Page/元法僧.md "wikilink")
      - [真王](../Page/真王_\(杜洛周\).md "wikilink")（525年八月—528年二月）：[北魏時期領導](../Page/北魏.md "wikilink")[杜洛周年号](../Page/杜洛周.md "wikilink")
      - [神嘉](../Page/神嘉.md "wikilink")（525年十二月—535年三月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉蠡升年号](../Page/劉蠡升.md "wikilink")
      - [魯興或普興](../Page/魯興.md "wikilink")（526年正月—八月）：[北魏時期領導](../Page/北魏.md "wikilink")[鮮于修禮年号](../Page/鮮于修禮.md "wikilink")
      - [始建](../Page/始建.md "wikilink")：[北魏時期領導](../Page/北魏.md "wikilink")[陳雙熾年号](../Page/陳雙熾.md "wikilink")
      - [廣安](../Page/广安_\(葛荣\).md "wikilink")（526年九月—528年九月）：[北魏時期領導](../Page/北魏.md "wikilink")[葛荣年号](../Page/葛荣.md "wikilink")
      - [天授](../Page/天授_\(刘获\).md "wikilink")（527年七月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉獲](../Page/劉獲.md "wikilink")、[鄭辯年号](../Page/鄭辯.md "wikilink")
      - [隆緒](../Page/隆緒.md "wikilink")（527年十月—528年正月）：[北魏時期領導](../Page/北魏.md "wikilink")[萧宝夤年号](../Page/萧宝夤.md "wikilink")
      - [義熙](../Page/义熙_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")
      - [甘露](../Page/甘露_\(麴光\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴光年号](../Page/麴光.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏民变政权年号](../Category/北魏民变政权年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:520年代中国政治](../Category/520年代中国政治.md "wikilink")