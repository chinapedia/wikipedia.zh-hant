**查懋聲**（
），香港人，[籍貫](../Page/籍貫.md "wikilink")[浙江](../Page/浙江.md "wikilink")[海寧](../Page/海寧.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[江西](../Page/江西.md "wikilink")[婺源](../Page/婺源.md "wikilink")，[查濟民的兒子](../Page/查濟民.md "wikilink")。他是[中國人民政治協商會議全國委員會委員](../Page/中國人民政治協商會議.md "wikilink")，也是[香港國際主題樂園有限公司之獨立非執行董事](../Page/香港國際主題樂園有限公司.md "wikilink")，以及[香港興業主席](../Page/香港興業.md "wikilink")，[香港](../Page/香港.md "wikilink")[亞洲電視前第二大股東](../Page/亞洲電視.md "wikilink")\[1\]\[2\]。

2007年，查懋聲透過旗下的[名力集團](../Page/名力集團.md "wikilink")，以超過十億港元入股[亞洲電視](../Page/亞洲電視.md "wikilink")，並兼任當時的主席。但自從2009年12月主席張永霖辭職，結束其於亞視短短一年的職務後，亞視董事局主席之位則從缺。

## 事件

1979年7月21日，查懋聲在[中環](../Page/中環.md "wikilink")[聯邦大廈](../Page/聯邦大廈.md "wikilink")26樓[香港興業總部遇槍擊](../Page/香港興業.md "wikilink")，腹部中兩槍，送院獲救。[凶手是](../Page/凶手.md "wikilink")[大白灣一間](../Page/大白灣.md "wikilink")[飯堂的小商人](../Page/飯堂.md "wikilink")。
\[3\]

## 曾任

  - [興勝創建非執行主席](../Page/興勝創建.md "wikilink")\[4\]\[5\]
  - 鷹君資產管理（冠君）有限公司獨立[非執行董事](../Page/非執行董事.md "wikilink")
  - [新世界發展獨立非執行董事](../Page/新世界發展.md "wikilink")
  - [冠君產業信託管理人](../Page/冠君產業信託.md "wikilink")\[6\]

## 外部参考

[category:香港興業](../Page/category:香港興業.md "wikilink")

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")
[Category:海宁查氏](../Category/海宁查氏.md "wikilink")
[Category:名力集團](../Category/名力集團.md "wikilink")
[Category:全国政协香港委员](../Category/全国政协香港委员.md "wikilink")
[Category:第十屆全國政協委員](../Category/第十屆全國政協委員.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:前亞洲電視有限公司人物](../Category/前亞洲電視有限公司人物.md "wikilink")
[Category:香港城市大學榮譽博士](../Category/香港城市大學榮譽博士.md "wikilink")
[Category:太平紳士](../Category/太平紳士.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")

1.  [廣管局批准黃炳均入主亞視](http://hk.news.yahoo.com/article/100910/4/k5f6.html)
2.  [亞視歡迎廣管局批准轉讓股份予黃炳均](http://hk.news.yahoo.com/article/100910/18/k5g2.html)
3.  [1979年
    查懋聲曾遭槍擊](http://paper.wenweipo.com/2010/03/09/HK1003090014.htm)
4.  [Hanison Construction Holdings
    Limited](http://www.hanison.com.hk/chi/dsm.aspx#d1)
5.  [Hanison Construction Holdings
    Limited](http://www.hanison.com/dsm.aspx#d1)
6.  [Champion
    REIT](http://www.championreit.com/html/chi/directors_if_06.jsp)