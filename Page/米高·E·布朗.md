<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Michael_E_Brown_1.jpg" title="fig:Michael_E_Brown_1.jpg">Michael_E_Brown_1.jpg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>已發現的<a href="../Page/外海王星天體.md" title="wikilink">外海王星天體數目</a>: 14</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/小行星50000.md" title="wikilink">创神星</a> <sup>1</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/小行星84719.md" title="wikilink">小行星84719</a> <sup>1</sup></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/小行星90377.md" title="wikilink">赛德娜</a> <sup>1</sup> <sup>2</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/小行星90482.md" title="wikilink">厄耳枯斯</a> <sup>1</sup> <sup>2</sup></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/妊神星.md" title="wikilink">妊神星</a><sup>1</sup> <sup>2</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/鬩神星.md" title="wikilink">鬩神星</a><sup>12</sup></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/鸟神星.md" title="wikilink">鸟神星</a> <sup>1</sup> <sup>2</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/阋卫一.md" title="wikilink">阋卫一</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/(126154)_2001_YH140.md" title="wikilink">2004 YH<sub>140</sub></a> <sup>1</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/(126155)_2001_YJ140.md" title="wikilink">2004 YJ<sub>140</sub></a> <sup>1</sup> <sup>5</sup></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/(119951)_2002_KX14.md" title="wikilink">2002 KX<sub>14</sub></a> <sup>1</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/(120178)_2003_OP32.md" title="wikilink">2003 OP<sub>32</sub></a> <sup>1</sup> <sup>2</sup></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/(120347)_2004_SB60.md" title="wikilink">2004 SB<sub>60</sub></a> <sup>3</sup> <sup>4</sup></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/(120348)_2004_TY364.md" title="wikilink">2004 TY<sub>364</sub></a> <sup>1</sup> <sup>2</sup></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ol>
<li><p>與<a href="../Page/乍德·特魯希略.md" title="wikilink">乍德·特魯希略共同發現</a></p></li>
<li><p>與<a href="../Page/大衛·拉比諾維茨.md" title="wikilink">大衛·拉比諾維茨共同發現</a></p></li>
<li><p>與<a href="../Page/Henry_G._Roe.md" title="wikilink">Henry G. Roe共同發現</a></p></li>
<li><p>與<a href="../Page/Kristina_M._Barkume.md" title="wikilink">Kristina M. Barkume共同發現</a></p></li>
<li><p>與 <a href="../Page/Glenn_Smith.md" title="wikilink">Glenn Smith</a> 共同發現</p></li>
</ol></td>
</tr>
</tbody>
</table>

**米高·E·布朗**（，），美国天文学家，現為[加州理工學院](../Page/加州理工學院.md "wikilink")[行星天文學教授](../Page/行星科學.md "wikilink")（2003年迄今），早先曾任助理教授（1997至2002年）與副教授（2002至2003年）。\[1\]

## 教育

布朗來自[阿拉巴馬州的](../Page/阿拉巴馬州.md "wikilink")[漢茨維爾](../Page/漢茨維爾.md "wikilink")，1983年畢業於[維吉尼亞的格里索姆高中](../Page/維吉尼亞.md "wikilink")；1987年在[普林斯頓大學取得](../Page/普林斯頓大學.md "wikilink")[文學士學位](../Page/文學士.md "wikilink")（物理學），並於1990年在[加利福尼亞大學柏克萊分校取得](../Page/加利福尼亞大學柏克萊分校.md "wikilink")[天文學](../Page/天文學.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")，1994年取得博士學位。
\[2\]

## 發現

布朗以探勘圍繞太陽運轉的遙遠天體而知名於科學界，他帶領的團隊發現了許多[外海王星天體](../Page/外海王星天體.md "wikilink")。特別著名的有：

  - [鬩神星](../Page/鬩神星.md "wikilink")：在[太陽系內比](../Page/太陽系.md "wikilink")[冥王星還要大的](../Page/冥王星.md "wikilink")[矮行星](../Page/矮行星.md "wikilink")。
  - [塞德娜](../Page/小行星90377.md "wikilink")：遠日點約為900[天文單位](../Page/天文單位.md "wikilink")，被認為是[歐特雲內層的天體](../Page/奧爾特雲.md "wikilink")。
  - [亡神星](../Page/小行星90482.md "wikilink")：[類冥天體](../Page/類冥天體.md "wikilink")，與海王星的軌道有3：2的共振關係。

### 爭議

在[胡斯·路易斯·奧蒂斯宣布發現](../Page/胡斯·路易斯·奧蒂斯.md "wikilink")[妊神星](../Page/小行星136108.md "wikilink")（2003
EL<sub>61</sub>）之前，布朗和他的團隊也曾觀測該天體近半年之久。因此，誰才是發現者仍有爭議。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [布朗的官方網站](http://www.gps.caltech.edu/~mbrown)

[Category:美国天文学家](../Category/美国天文学家.md "wikilink")
[Category:小行星發現者](../Category/小行星發現者.md "wikilink")
[Category:行星科學家](../Category/行星科學家.md "wikilink")
[Category:加州理工學院教師](../Category/加州理工學院教師.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")
[Category:阿拉巴馬州人](../Category/阿拉巴馬州人.md "wikilink")
[Category:海王星外天體發現者](../Category/海王星外天體發現者.md "wikilink")
[Category:阋神星](../Category/阋神星.md "wikilink")
[Category:科维理奖获得者](../Category/科维理奖获得者.md "wikilink")
[Category:马克·阿伦森纪念讲座得主](../Category/马克·阿伦森纪念讲座得主.md "wikilink")
[Category:哈罗德·C·尤里奖获得者](../Category/哈罗德·C·尤里奖获得者.md "wikilink")

1.

2.