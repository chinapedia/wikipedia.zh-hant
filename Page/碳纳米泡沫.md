**碳纳米泡沫**，[碳元素的](../Page/碳.md "wikilink")[同素异形体之一](../Page/同素异形体.md "wikilink")，1997年由[澳大利亚国立大学的Andrei](../Page/澳大利亚.md "wikilink")
V. Rode及其合作者发现\[1\]。

碳纳米泡沫呈蛛网状，具有[分形结构](../Page/分形.md "wikilink")，有[铁磁性](../Page/铁磁性.md "wikilink")。泡沫由许多原子团簇构成，每个含有约4000个碳原子，直径约6到9[纳米](../Page/纳米.md "wikilink")；其中很多原子团连在一起，形成了纤细的网。在碳纳米泡沫中，有许多七边形的结构。研究者认为，七边形的结构造是它有很多未成对的电子的原因；泡沫也因此而具有了磁性，这是其他任何一种碳的同素异形体所没有的特性。研究者还发现，在低于-183℃时，泡沫还具有永久磁性，但這種磁性在室溫下卻會慢慢消失。

碳纳米泡沫的[密度很低](../Page/密度.md "wikilink")，与[碳气凝胶很相似](../Page/碳气凝胶.md "wikilink")，但密度是它的百分之一；它是目前世上最轻的物质之一，密度约为2–10 mg/cm<sup>3</sup>\[2\]
\[3\]，仅是海平面上空气密度的几倍。

这种泡沫还是[电的不良](../Page/电.md "wikilink")[导体](../Page/导体.md "wikilink")，可以积聚[静电而吸附在其他物质上](../Page/静电.md "wikilink")；它的[热传导性也很差](../Page/热传导.md "wikilink")。

由于纳米泡沫具有的铁磁性，将来有可能把它们的颗粒注射入人体，用来改善磁共振成像的清晰程度。它们也可能被应用到利用[电子自旋或电子磁性的](../Page/电子.md "wikilink")[自旋器件中](../Page/自旋.md "wikilink")。

## 参考文献

## 扩展阅读

  -
  - Rode, Andrei; Gamaly, Eugene; Luther-Davies, Barry. "Method for
    deposition of thin films", International Patent Application No.
    PCT/AU98/00739, priority date 11 September, 1997; "Method of
    deposition of thin films of amorphous and crystalline
    microstructures based on ultrafast pulsed laser deposition",
    (2001).

  -
  -
  -
  -
  -
  -
  -
  -
[Category:纳米技术](../Category/纳米技术.md "wikilink")
[Category:碳的同素异形体](../Category/碳的同素异形体.md "wikilink")

1.

2.
3.