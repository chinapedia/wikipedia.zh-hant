**借喻**是[修辞手法的一种](../Page/修辞手法.md "wikilink")，用与被修饰对象相关的其他事物来指代被修饰对象，也称“换喻”、“轉喻”。

## 举例

<table>
<thead>
<tr class="header">
<th><p>词语</p></th>
<th><p>原义</p></th>
<th><p>借喻用法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/汗水.md" title="wikilink">汗水</a></p></td>
<td><p>人体皮肤分泌物</p></td>
<td><p>努力工作</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中南海.md" title="wikilink">中南海</a></p></td>
<td><p>北京市地名</p></td>
<td><p>中华人民共和国中央政府</p></td>
</tr>
<tr class="odd">
<td><p>失足</p></td>
<td><p>走路摔倒</p></td>
<td><p>犯错误、犯罪</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇格蘭場.md" title="wikilink">蘇格蘭場</a></p></td>
<td><p>倫敦大蘇格蘭場</p></td>
<td><p>大倫敦地區警察</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/克里姆林宫.md" title="wikilink">克里姆林宫</a></p></td>
<td><p><a href="../Page/帝俄.md" title="wikilink">帝俄時期沙皇住所</a> (1712年前)</p></td>
<td><p>蘇聯、俄羅斯聯邦中央政府</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿爺.md" title="wikilink">阿爺</a></p></td>
<td><p>祖父</p></td>
<td><p>香港人對北京政府的稱呼</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同志.md" title="wikilink">同志</a></p></td>
<td><p>志同道合的人</p></td>
<td><p>中國大陸: 中共黨員之間稱呼<br />
港澳台新: 同性戀者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/矽谷.md" title="wikilink">矽谷</a></p></td>
<td><p>美國南<a href="../Page/加里福尼亞州.md" title="wikilink">加里福尼亞州一處早期為</a><br />
研發和生產<a href="../Page/集成電路.md" title="wikilink">半導體晶片的地區</a></p></td>
<td><p>高科技事業集中地</p></td>
</tr>
</tbody>
</table>

## 词源

英文metonymy一词来源于[古希腊文μετά](../Page/古希腊文.md "wikilink")（改变）和ωνυμία（名称）两个词根。

## 参考文献

### 引用

### 来源

  -
  -
  -
  -
## 参见

  - [修辞](../Page/修辞.md "wikilink")
      - [比喻](../Page/比喻.md "wikilink")
          - [明喻](../Page/明喻.md "wikilink")
          - [暗喻](../Page/暗喻.md "wikilink")
          - [略喻](../Page/略喻.md "wikilink")
          - [博喻](../Page/博喻.md "wikilink")

{{-}}

[Category:修辞](../Category/修辞.md "wikilink")
[Category:比喻](../Category/比喻.md "wikilink")