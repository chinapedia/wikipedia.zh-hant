[Winfield_Commercial_Building.JPG](https://zh.wikipedia.org/wiki/File:Winfield_Commercial_Building.JPG "fig:Winfield_Commercial_Building.JPG")已結業多時\]\]

**尖沙咀卡拉OK縱火案**又稱**寶勒巷卡拉OK縱火案**，發生於1997年的[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[油尖旺區](../Page/油尖旺區.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")，共17人死，13人傷。

## 經過

1997年1月25日凌晨，位於尖沙咀[寶勒巷](../Page/寶勒巷.md "wikilink")6-8號[盈豐商業大廈內的](../Page/盈豐商業大廈.md "wikilink")[新一代卡拉OK起火](../Page/新一代卡拉OK.md "wikilink")，火勢於數分鐘內蔓延。[消防處人員接報後抵達現場](../Page/香港消防處.md "wikilink")，當時火勢十分猛烈；事發前一日（1月24日）為星期五晚上，當時[卡拉OK內約有](../Page/卡拉OK.md "wikilink")80名顧客與20名職員，部分顧客或者因為樂極忘形或者房間內的音樂聲量過大，導致沒有聽聞火警鐘聲，亦有顧客因為醉酒而未能夠迅速前往緊急出口逃生。火警導致17人死亡、13人受傷，年齡在16歲至42歲之間。

[香港總督](../Page/香港總督.md "wikilink")[彭定康事後巡視災場](../Page/彭定康.md "wikilink")，並且責成[皇家香港警務處處長](../Page/皇家香港警務處處長.md "wikilink")[許淇安盡快破案](../Page/許淇安.md "wikilink")。

## 起因

此縱火案乃涉及[黑幫的報復行動](../Page/三合會.md "wikilink")，源於案發前五日，在事發的卡拉OK內疑因為有同門[黑社會](../Page/黑社會.md "wikilink")[新義安成員蔡錦輝與董勝飛與女子搭訕而發生內訌](../Page/新義安.md "wikilink")，當時被黑社會[和勝和的一名](../Page/和勝和.md "wikilink")「睇場」（場監）干涉，雙方大打出手，發生內訌的兩名男子均被打至頭破血流，其中蔡錦輝其後兩次計劃糾黨向和勝和的睇場伏擊報復，但是因為先後遇上警察掃蕩而被迫取消行動，故此就以縱火尋仇，成為縱火案的起因。
\[1\]

## 調查結果

消防處人員調查後發現，該卡拉OK走廊的牆壁和房間門均屬於易燃物料；店內防火設備嚴重不足及失效多時，在火警發生時未能夠發揮作用。最重要是，起火位置為正門入口，而入口至前樓梯均鋪了[地毯](../Page/地毯.md "wikilink")，火勢沿著前樓梯的地毯蔓延至樓上，加上卡拉OK內顧客眾多，卡拉OK內又未有設置緊急照明系統，使到顧客被逼摸黑逃生，釀成嚴重傷亡。

## 災後檢討

事發後，[香港政府全面檢討](../Page/香港政府.md "wikilink")[娛樂場所的安全和防火指引](../Page/娛樂場所.md "wikilink")，加強檢查娛樂場所的防火設施，立例規定所有娛樂場所（包括[卡拉OK場所](../Page/卡拉OK.md "wikilink")）的防火設施必須定期被全面檢查，必須設置緊急照明系統及緊急出口指示牌，以確保發生事故時，顧客可以安全及迅速地逃離現場。同時，所有卡拉OK場所必須加設緊急截電系統。當火警發生時，全場所有音響和電視會自動關閉，使到顧客知道有火警發生，並且立即逃生。

事發後約一個月，警察拘捕了4名涉案男子，其中3名被告李家豪、董勝飛和柳應達被裁定[謀殺罪名成立](../Page/謀殺.md "wikilink")，判處[終身監禁](../Page/終身監禁.md "wikilink")，另外鄭偉成則被裁定[誤殺罪名成立](../Page/誤殺.md "wikilink")，判處入獄11年。其餘三名在逃疑犯蔡錦輝、陳福清和綽號「媚媚」的陳惠良則被[通緝](../Page/通緝.md "wikilink")，於案發10年後懸紅60萬港元緝兇。

[2008年12月](../Page/2008年12月.md "wikilink")，策劃縱火案的主謀，綽號「上帝」的蔡錦輝在[深圳被公安拘捕](../Page/深圳.md "wikilink")，於同年12月19日被引渡返回香港\[2\]\[3\]。於2010年4月15日，案件於高等法院開審，同年5月3日，蔡錦輝被裁定[謀殺罪名成立](../Page/謀殺.md "wikilink")\[4\]，判處終身監禁。

## 參考資料

## 外部連結

  - [燒死17男女
    策劃人勢判終身](http://www.orientaldaily.on.cc/cnt/news/20100504/00176_038.html)
  - [寶勒巷卡拉ok大火新聞](http://www.youtube.com/watch?v=oj1CfXOcTck)
  - [一九九七年一月二十五日縱火及謀殺案通緝](http://www.police.gov.hk/ppp_tc/06_appeals_public/wanted/detail.html?id=20140329160648)

[Category:1997年香港](../Category/1997年香港.md "wikilink")
[Category:香港謀殺案](../Category/香港謀殺案.md "wikilink")
[Category:香港纵火案](../Category/香港纵火案.md "wikilink")
[Category:尖沙咀](../Category/尖沙咀.md "wikilink")
[Category:卡拉OK](../Category/卡拉OK.md "wikilink")
[Category:1997年谋杀案](../Category/1997年谋杀案.md "wikilink")
[Category:1997年屠杀](../Category/1997年屠杀.md "wikilink")
[Category:1997年1月](../Category/1997年1月.md "wikilink")
[Category:夜总会火灾](../Category/夜总会火灾.md "wikilink")
[Category:中国无差别杀人事件](../Category/中国无差别杀人事件.md "wikilink")

1.  [燒死17男女
    策劃人勢判終身](http://www.orientaldaily.on.cc/cnt/news/20100504/00176_038.html)
2.  [97年寶勒巷慘案　放火燒死17人　元凶落網
    香港頭號通緝犯潛逃12年　下周深圳移交](http://hk.apple.nextmedia.com/news/first/20081215/11969006)
3.  [上帝深圳落網
    引渡無障礙？](http://www.881903.com/Page/ZH-TW/audiocolumndetail.aspx?ItemId=83134)
4.  [縱火奪17命主謀囚終身](http://orientaldaily.on.cc/cnt/news/20100505/00176_014.html)