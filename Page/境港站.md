**境港站**（）是一個位於[日本](../Page/日本.md "wikilink")[鳥取縣](../Page/鳥取縣.md "wikilink")[境港市大正町的](../Page/境港市.md "wikilink")[車站](../Page/車站.md "wikilink")。是[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）[境線的終點站](../Page/境線.md "wikilink")。鄰接的境港交流館能與[隱岐汽船接駁](../Page/隱岐汽船.md "wikilink")。車站的外觀是燈塔造型。

境港站東側延伸出來的道路「[水木茂路](../Page/水木茂路.md "wikilink")」（），在兩側的人行步道擺放了許多妖怪造型的[裝置藝術](../Page/裝置藝術.md "wikilink")。境港站也被暱稱為**[鬼太郎站](../Page/鬼太郎.md "wikilink")**。

## 車站構造

擁有頭端式1面2線的[月台](../Page/月台.md "wikilink")。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>目的地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1、2</p></td>
<td><p>境線</p></td>
<td><p><a href="../Page/米子站.md" title="wikilink">米子方向</a></p></td>
<td><p>通常在1號月台開出</p></td>
</tr>
</tbody>
</table>

設置有[綠色窗口](../Page/綠色窗口.md "wikilink")。

## 車站週邊

  - 境港交流館
      - [隱岐汽船](../Page/隱岐汽船.md "wikilink")
  - [境港警察署境港站前派出所](../Page/境港警察署.md "wikilink")
  - 水木路郵局
  - [鳥取銀行](../Page/鳥取銀行.md "wikilink")
  - [島根銀行](../Page/島根銀行.md "wikilink")
  - [鳥取縣道、島根縣道2號境美保關線](../Page/鳥取縣道、島根縣道2號境美保關線.md "wikilink")
  - [鳥取縣道285號米子空港境港停車場線](../Page/鳥取縣道285號米子空港境港停車場線.md "wikilink")

## 接駁公車

### 隱岐汽船接駁公車

  - [一畑公車](../Page/一畑公車.md "wikilink")
  - [日之丸自動車](../Page/日之丸自動車.md "wikilink")

### 一般路線公車

  - 境港市通商課 （）

  - [日之丸自動車](../Page/日之丸自動車.md "wikilink")

  - （[八束計程車](../Page/八束計程車.md "wikilink")、[中海計程車](../Page/中海計程車.md "wikilink")）

## 歷史

  - 1902年11月1日 以**境站**之名稱開業。
  - 1919年7月1日 改名為境港站。

## 相鄰車站

  - 西日本旅客鐵道

    境線

      -

        （Kijimuna站）－**境港（鬼太郎站）**

## 外部連結

  - [JR西日本（境港站）](http://www.jr-odekake.net/eki/top.php?id=0641710)

[Category:鳥取縣鐵路車站](../Category/鳥取縣鐵路車站.md "wikilink")
[Kaiminato](../Category/日本鐵路車站_Sa.md "wikilink")
[Category:境線車站](../Category/境線車站.md "wikilink")
[Category:1902年啟用的鐵路車站](../Category/1902年啟用的鐵路車站.md "wikilink")
[Category:境港市](../Category/境港市.md "wikilink")