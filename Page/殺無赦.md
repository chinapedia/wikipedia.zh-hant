《**不可饶恕**》（***Unforgiven***）是1992年[美國](../Page/美國.md "wikilink")[導演](../Page/導演.md "wikilink")[克林特·伊斯特伍德所執導的](../Page/克林特·伊斯特伍德.md "wikilink")[西部片](../Page/西部片.md "wikilink")，由[大衛·畢波斯](../Page/大衛·畢波斯.md "wikilink")（David
Peoples）編寫[劇本](../Page/劇本.md "wikilink")。《殺無赦》由[克林特·伊斯特伍德](../Page/克林特·伊斯特伍德.md "wikilink")、[金·哈克曼](../Page/金·哈克曼.md "wikilink")、[摩根·弗里曼與](../Page/摩根·弗里曼.md "wikilink")[李察·哈里斯所主演](../Page/李察·哈里斯.md "wikilink")。

克林特·伊斯特伍德將本片獻給著名電影導演及導師[唐西格爾](../Page/唐西格爾.md "wikilink")（Don
Siegel）與[塞吉歐·李昂尼](../Page/塞吉歐·李昂尼.md "wikilink")。《不可饶恕》也入圍9項[奧斯卡獎](../Page/奧斯卡獎.md "wikilink")，並獲得[奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")、[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")、[奧斯卡最佳男配角獎與](../Page/奧斯卡最佳男配角獎.md "wikilink")[奧斯卡最佳剪輯獎等](../Page/奧斯卡最佳剪輯獎.md "wikilink")4項大獎。克林特·伊斯特伍德也入圍[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")，雖然最後敗給《[女人香](../Page/女人香_\(1992年電影\).md "wikilink")》的男主角[艾爾·帕西諾](../Page/艾爾·帕西諾.md "wikilink")。《不可饶恕》在2004年被永久保存在[國家影片登記部](../Page/國家影片登記部.md "wikilink")。\[1\]

《不可饶恕》被[美國電影學會選為美國影史上最偉大的](../Page/美國電影學會.md "wikilink")[100部電影之一](../Page/AFI百年百大電影.md "wikilink")，被認為是美國史上最重要的[西部片之一](../Page/西部片.md "wikilink")。2008年6月该协会还将这部电影评为[各類型電影十大佳片西部片類型的第](../Page/AFI百年各類型電影十大佳片.md "wikilink")4名，僅次於《[搜索者](../Page/搜索者.md "wikilink")》、《[日正當中](../Page/日正當中_\(電影\).md "wikilink")》與《[原野奇俠](../Page/原野奇俠.md "wikilink")》（*Shane*）。\[2\]\[3\]

## 劇情簡介

1880年代，威廉（克林特·伊斯特伍德飾演）過去是一個惡名昭彰、無惡不作的盜賊及殺人犯，但是他自從與漂亮的克勞迪亞結婚並生下兩個孩子後，一家過著和樂融融的生活。威廉的妻子不久之後因病去世，於是他帶著兩個孩子到[堪薩斯州郊外經營養豬場](../Page/堪薩斯州.md "wikilink")，靠著自己辛勤工作把孩子撫養成人。

有兩個[牛仔在](../Page/牛仔.md "wikilink")[懷俄明州的](../Page/懷俄明州.md "wikilink")[酒吧鬧事](../Page/酒吧.md "wikilink")，其中一人用刀劃傷了妓女的臉。妓院老闆非常憤怒，恨不得吊死那兩個牛仔，但是警長「小比爾」達格特（[金·哈克曼飾演](../Page/金·哈克曼.md "wikilink")）讓他們賠償7匹馬作結。於是眾妓女氣憤難平，想要集資懸賞一千元，找人幫她們殺死這兩個牛仔。

一個自大的年輕人「斯科菲爾德小子」（[詹姆斯·伍維特飾演](../Page/詹姆斯·伍維特.md "wikilink")）想要找威廉一起去完成這件差事，但是威廉一開始並不想再捲入罪惡的勾當，於是斯科菲爾德小子只好獨自離去。但是因為豬隻生病，威廉為了讓年幼的子女過著富裕生活，於是告別殺手生涯11年的威廉決定重操舊業，參與這次殺人行動。於是，斯科菲爾德小子、威廉以及威廉的老搭檔內德·洛根（[摩根·弗里曼飾演](../Page/摩根·弗里曼.md "wikilink")）一起追蹤其中一个牛仔到峽谷地區，威廉解決了他。后来威廉和斯科菲爾德小子又来到另一个目标所在的地方，斯科菲爾德小子趁那个人上厕所的时机把他杀死。然而不幸的是，提早離開的洛根卻被警長「小比爾」達格特的手下抓走，並且被他們折磨至死。

为给老友內德报仇，威廉嘱托斯科菲爾德小子先将酬金安全带走，随后他孤身一人前去找警长小比尔报仇。看到洛根被棄屍酒吧間外，因此憤怒至極。於是他進入酒吧，殺死了「小比爾」達格特為朋友洛根報仇。他後来領取了賞金，並且帶兩個孩子到[舊金山經商](../Page/舊金山.md "wikilink")。

## 風格

《不可饶恕》是一部相當反[英雄主義的](../Page/英雄主義.md "wikilink")[西部片](../Page/西部片.md "wikilink")，大大顛覆傳統西部片的形象，將[美國老西部到處可見的真實面貌](../Page/美國老西部.md "wikilink")，例如殺人道德問題、警長的偽善等現象赤裸裸的展現出來，破除西部傳統的神話，強烈探討殺人者及被殺者的心境。尤其本片是由以西部片起家的克林·伊斯威特自製自導自演，格外顯的有內省意義，因此也被一些人稱為『最後的西部片』。[傑克·葛林](../Page/傑克·葛林.md "wikilink")（Jack
N. Green）灰暗的攝影風格也受到影評家的讚賞。

## 獎項及評價

| 奧斯卡獎紀錄                                                    |
| --------------------------------------------------------- |
| **1. 最佳剪輯**（[喬伊·考克斯](../Page/喬伊·考克斯.md "wikilink")）       |
| **2. 最佳男配角**（[金·哈克曼](../Page/金·哈克曼.md "wikilink")）        |
| **3. 最佳導演**（[克林特·伊斯特伍德](../Page/克林特·伊斯特伍德.md "wikilink")） |
| **4. 最佳影片**                                               |
| 金球獎紀錄                                                     |
| **1. 最佳導演**（[克林特·伊斯特伍德](../Page/克林特·伊斯特伍德.md "wikilink")） |
| **2. 最佳男配角**（[金·哈克曼](../Page/金·哈克曼.md "wikilink")）        |
| 英國電影和電視藝術學院獎                                              |
| **1. 最佳男配角**（[金·哈克曼](../Page/金·哈克曼.md "wikilink")）        |
|                                                           |

  - 入圍9項[奧斯卡獎](../Page/奧斯卡獎.md "wikilink")，並獲得[奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")、[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")、[奧斯卡最佳男配角獎與](../Page/奧斯卡最佳男配角獎.md "wikilink")[奧斯卡最佳剪輯獎等](../Page/奧斯卡最佳剪輯獎.md "wikilink")4項大獎。克林·伊斯威特同時入圍奧斯卡最佳導演獎與[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")，最後艾爾·帕西諾以《女人香》擊敗克林特·伊斯特伍德奪得最佳男主角獎。

<!-- end list -->

  - 入圍[英國電影和電視藝術學院](../Page/英國電影和電視藝術學院.md "wikilink")6項大獎，包括最佳影片獎及最佳導演獎，雖然最終只獲得最佳男配角獎。《不可饶恕》也入圍4項[金球獎](../Page/金球獎_\(影視獎項\).md "wikilink")，最後獲得最佳導演獎及最佳男配角獎。

<!-- end list -->

  - 1998年被美國電影學會選為美國影史上最偉大的100部電影之一，名列在第98位。然後在2007年再度被美國電影學會選為美國影史上最偉大的100部電影之一，名列在第68位。《不可饶恕》被廣泛的認為是美國史上最重要的西部片之一。

<!-- end list -->

  - 2008年6月被美國電影學會選為各類型電影十大佳片西部片類型的第4名，僅次於《搜索者》、《日正當中》與《原野奇俠》。

<!-- end list -->

  - 長期與克林特·伊斯特伍德合作的設計師[比爾·金以](../Page/比爾·金.md "wikilink")《不可饶恕》電影[海報獲得](../Page/海報.md "wikilink")《[好萊塢記者](../Page/好萊塢記者.md "wikilink")》雜誌頒發的1992年重點藝術獎\[4\]。

這部電影因為探討道德議題上的爭議與氣氛（類似許多[黑色電影的情節](../Page/黑色電影.md "wikilink")）而受到許多電影評論家的稱讚。他們稱讚《不可饶恕》是一部優秀的西部電影，然而這部電影並非沒有任何批評。[基恩·西斯克爾](../Page/基恩·西斯克爾.md "wikilink")（Gene
Siskel）與[羅傑·艾伯特都批評這部電影過於冗長](../Page/羅傑·艾伯特.md "wikilink")，而且具有太多多餘的角色。然而著名電影評論家羅傑·艾伯特仍然認為《不可饶恕》是一部偉大的電影\[5\]。《不可饶恕》在[爛番茄](../Page/爛番茄.md "wikilink")[網站獲得](../Page/網站.md "wikilink")96%的正面評價（共有56篇評論），用戶平均分數則為8.5分\[6\]。《不可饶恕》在[互聯網電影數據庫中則獲得觀眾給予](../Page/互聯網電影數據庫.md "wikilink")8.3的高分，名列前150位。《不可饶恕》在《[帝國雜誌](../Page/帝國雜誌.md "wikilink")》史上500大電影中名列第158位\[7\]。《不可饶恕》也被《[時代雜誌](../Page/時代雜誌.md "wikilink")》選為史上百大電影\[8\]。《不可饶恕》被[美國作家公會評選為史上百大優秀劇本之一](../Page/美國作家公會.md "wikilink")\[9\]。

## 參見

  -
## 參考資料

## 外部链接

  -
  -
  -
  -
  - [*Unforgiven*](http://www.filmsite.org/unfo.html) at
    [Filmsite.org](../Page/Filmsite.org.md "wikilink")

  - [*Unforgiven*](https://web.archive.org/web/20060422151447/http://www.artsandfaith.com/t100/2005/entry.php?film=91)
    at the [Arts & Faith Top100 Spiritually Significant
    Films](http://archive.wikiwix.com/cache/20110223153741/http://artsandfaith.com/t100/)
    list

  - [Psychoanalytic review of
    Unforgiven](https://web.archive.org/web/20140909031159/http://internationalpsychoanalysis.net/2009/11/29/unforgiven-identification-with-death/)

[Category:1992年電影](../Category/1992年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美国西部片](../Category/美国西部片.md "wikilink")
[Category:美国剧情片](../Category/美国剧情片.md "wikilink")
[Category:克林·伊斯威特電影](../Category/克林·伊斯威特電影.md "wikilink")
[Category:1990年代剧情片](../Category/1990年代剧情片.md "wikilink")
[Category:1880年代背景电影](../Category/1880年代背景电影.md "wikilink")
[Category:堪萨斯州背景電影](../Category/堪萨斯州背景電影.md "wikilink")
[Category:懷俄明州背景電影](../Category/懷俄明州背景電影.md "wikilink")
[Category:亞伯達省取景電影](../Category/亞伯達省取景電影.md "wikilink")
[Category:奧斯卡最佳影片](../Category/奧斯卡最佳影片.md "wikilink")
[Category:奧斯卡最佳導演獲獎電影](../Category/奧斯卡最佳導演獲獎電影.md "wikilink")
[Category:奧斯卡最佳男配角獲獎電影](../Category/奧斯卡最佳男配角獲獎電影.md "wikilink")
[Category:奧斯卡最佳剪辑獲獎電影](../Category/奧斯卡最佳剪辑獲獎電影.md "wikilink")
[Category:金球奖最佳導演獲獎電影](../Category/金球奖最佳導演獲獎電影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[Category:華納兄弟電影](../Category/華納兄弟電影.md "wikilink")
[Category:電影旬報十佳獎最佳外語片](../Category/電影旬報十佳獎最佳外語片.md "wikilink")

1.
2.
3.
4.  [The Hollywood Reporter Key Art
    Awards](http://www.hollywoodreporter.com/hr/awards_festivals/key_art/index.jsp)
5.
6.  [Rotten Tomatoes -
    Unforgiven](http://www.rottentomatoes.com/m/1041911-unforgiven/)
7.  [500 Best Movies of All Time – Empire
    Magazine](http://www.cinemarealm.com/best-of-cinema/empires-500-greatest-movies-of-all-time/)
8.  [Best 100 Movies Ever Made – TIME
    Magazine](http://www.cinemarealm.com/best-of-cinema/best-100-movies-ever-made-time-magazine/)
9.