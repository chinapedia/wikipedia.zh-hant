**拉斐特**（**Lafayette**）位于[美国](../Page/美国.md "wikilink")[印第安纳州西部](../Page/印第安纳州.md "wikilink")[沃巴什河畔](../Page/沃巴什河.md "wikilink")，是[蒂珀卡努县的首府](../Page/蒂珀卡努县.md "wikilink")，人口56,397人（2000年），與[西拉法葉相鄰](../Page/西拉法葉.md "wikilink")。

## 地理

### 氣候

## 交通

### 機場

  - [普度大學機場](../Page/普度大學.md "wikilink")：位於[西拉法葉](../Page/西拉法葉.md "wikilink")，目前沒有商業航班。

### 公路

  - [I-65.svg](https://zh.wikipedia.org/wiki/File:I-65.svg "fig:I-65.svg")[州際公路I](../Page/州際公路.md "wikilink")-65，連接[芝加哥與](../Page/芝加哥.md "wikilink")[印第安納波利斯的主要道路](../Page/印第安納波利斯.md "wikilink")。
  - [US_52.svg](https://zh.wikipedia.org/wiki/File:US_52.svg "fig:US_52.svg")[美國國道US](../Page/美國國道.md "wikilink")
    52，連接[芝加哥與](../Page/芝加哥.md "wikilink")[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")。
  - [US_231.svg](https://zh.wikipedia.org/wiki/File:US_231.svg "fig:US_231.svg")[美國國道US](../Page/美國國道.md "wikilink")
    231，連接[芝加哥近郊](../Page/芝加哥.md "wikilink")[US_41.svg](https://zh.wikipedia.org/wiki/File:US_41.svg "fig:US_41.svg")[美國國道US](../Page/美國國道.md "wikilink")
    41與[肯塔基州](../Page/肯塔基州.md "wikilink")[US_60.svg](https://zh.wikipedia.org/wiki/File:US_60.svg "fig:US_60.svg")[美國國道US](../Page/美國國道.md "wikilink")
    60。在[拉法葉為市區西南邊的聯外道路](../Page/拉斐特_\(印第安纳州\).md "wikilink")。
  - [Indiana_25.svg](https://zh.wikipedia.org/wiki/File:Indiana_25.svg "fig:Indiana_25.svg")印第安納州州道SR
    25，連接[華沙_(印第安那州)與](../Page/華沙_\(印第安那州\).md "wikilink")[Indiana_32.svg](https://zh.wikipedia.org/wiki/File:Indiana_32.svg "fig:Indiana_32.svg")印第安納州州道SR
    32的南北向道路，亦為[US_24.svg](https://zh.wikipedia.org/wiki/File:US_24.svg "fig:US_24.svg")[美國國道US](../Page/美國國道.md "wikilink")
    24至[拉法葉的聯絡道路](../Page/拉斐特_\(印第安纳州\).md "wikilink")，該段道路為[美國國道規格](../Page/美國國道.md "wikilink")。
  - [Indiana_26.svg](https://zh.wikipedia.org/wiki/File:Indiana_26.svg "fig:Indiana_26.svg")印第安納州州道SR
    26，連接[西拉法葉](../Page/西拉法葉.md "wikilink")、[普度大學與](../Page/普度大學.md "wikilink")[科科莫的主要道路](../Page/科科莫_\(印第安納州\).md "wikilink")。往西可連接[Illinois_9.svg](https://zh.wikipedia.org/wiki/File:Illinois_9.svg "fig:Illinois_9.svg")伊利諾州9號州道，往東可連接[OH-119.svg](https://zh.wikipedia.org/wiki/File:OH-119.svg "fig:OH-119.svg")俄亥俄州119號州道。在[西拉法葉與](../Page/西拉法葉.md "wikilink")[拉法葉](../Page/拉斐特_\(印第安纳州\).md "wikilink")，這條路命名為State
    St、South St與Columbia
    St.，亦為市中心及[普度大學東西向重要連外道路](../Page/普度大學.md "wikilink")。
  - [Indiana_38.svg](https://zh.wikipedia.org/wiki/File:Indiana_38.svg "fig:Indiana_38.svg")印第安納州州道SR
    38，連接[拉法葉與](../Page/拉斐特_\(印第安纳州\).md "wikilink")[US_421.svg](https://zh.wikipedia.org/wiki/File:US_421.svg "fig:US_421.svg")[美國國道US](../Page/美國國道.md "wikilink")
    421的主要道路。在[拉法葉](../Page/拉斐特_\(印第安纳州\).md "wikilink")，這條路命名為Main
    St，亦為市中心與市區南郊的主要聯絡道路。

### 鐵路

  - [Amtrak](../Page/Amtrak.md "wikilink")
      - [印第安那州號](../Page/印第安那州號.md "wikilink")：提供來回[芝加哥與](../Page/芝加哥.md "wikilink")[印第安納波利斯的每日班次](../Page/印第安納波利斯.md "wikilink")。
      - [紅雀號](../Page/紅雀號.md "wikilink")：來回[芝加哥與](../Page/芝加哥.md "wikilink")[紐約之間的每週固定班次](../Page/紐約.md "wikilink")。透過鐵路可由拉法葉直達[紐約](../Page/紐約.md "wikilink")、[費城](../Page/費城.md "wikilink")、[巴爾的摩](../Page/巴爾的摩.md "wikilink")、[華盛頓特區](../Page/華盛頓特區.md "wikilink")、[辛辛那提](../Page/辛辛那提.md "wikilink")、[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")、[芝加哥等大城市](../Page/芝加哥.md "wikilink")，或於[芝加哥轉乘至其他各州](../Page/芝加哥.md "wikilink")。

### 市內大眾運輸與聯外客運

  - CityBus：提供[拉法葉與](../Page/拉法葉.md "wikilink")[西拉法葉地區市內公共運輸服務](../Page/西拉法葉.md "wikilink")。
  - [灰狗巴士](../Page/灰狗巴士.md "wikilink")（Greyhound）：提供聯外客運服務。
  - Lafayette Limo：
    提供前往[印第安納波利斯國際機場的機場快捷巴士](../Page/印第安納波利斯國際機場.md "wikilink")。
  - Suburban Express：
    提供假日連接[普度大學與](../Page/普度大學.md "wikilink")[芝加哥的客運服務](../Page/芝加哥.md "wikilink")。
  - Star of America：
    提供前往[印第安納波利斯國際機場的機場快捷巴士](../Page/印第安納波利斯國際機場.md "wikilink")。

## 名人

  - [雷·尤里](../Page/雷·尤里.md "wikilink")：田径运动员。
  - [罗杰·D·布兰尼金](../Page/罗杰·D·布兰尼金.md "wikilink")：[印第安纳州前州长](../Page/印第安纳州.md "wikilink")。
  - [薛尼·波勒](../Page/薛尼·波勒.md "wikilink")：知名電影導演，曾榮獲第58屆奧斯卡金像獎最佳電影獎。
  - Alvah Curtis Roebuck：美國Sears公司共同創辦人。
  - Brian Lamb：美國有線電視頻道[C-SPAN創辦人](../Page/C-SPAN.md "wikilink")。
  - Christopher L.
    Eisgruber：第20任[普林斯頓大學校長](../Page/普林斯頓大學.md "wikilink")。
  - Eric J.
    Barron：第14任[佛羅里達州立大學校長](../Page/佛羅里達州立大學.md "wikilink")、第18任[賓州州立大學校長](../Page/賓州州立大學.md "wikilink")。
  - [唐纳德·威廉士](../Page/唐纳德·威廉姆斯.md "wikilink")：太空人。

## 姊妹市

  - [太田市](../Page/太田市.md "wikilink")

  - [龍口市](../Page/龍口市.md "wikilink")

## 參考資料

[L](../Category/印第安纳州城市.md "wikilink")