**倫敦皇家內科醫學院** (Royal College of Physicians of London)
是一所位於[倫敦的](../Page/倫敦.md "wikilink")[內科](../Page/內科.md "wikilink")[醫學院](../Page/醫學院.md "wikilink")。英國倫敦皇家內科醫學院於一五一八年，由[亨利八世成立](../Page/亨利八世.md "wikilink")，初時稱為內科醫學院，直至一六七四年才獲冠以“皇家”的稱號。現時英國倫敦皇家內科醫學院是全球最活躍的醫學組織之一，其成立的主要目的，是制定對[內科這門專科的專業水準](../Page/內科.md "wikilink")。每年均會舉辦多次的專科考試，醫生要通過一連串的考試評核後，才可成為英國倫敦皇家內科醫學院院士，英文簡稱MRCP(UK)，全寫為
Membership of the Royal College of Physicians。
[Lasdun_Royal_College_of_Physicians_front_Dec_2005.jpg](https://zh.wikipedia.org/wiki/File:Lasdun_Royal_College_of_Physicians_front_Dec_2005.jpg "fig:Lasdun_Royal_College_of_Physicians_front_Dec_2005.jpg")

## 外部連結

  - [英國倫敦皇家內科醫學院](http://www.rcplondon.ac.uk)

[Category:醫學院校](../Category/醫學院校.md "wikilink")
[Category:英國學會](../Category/英國學會.md "wikilink")