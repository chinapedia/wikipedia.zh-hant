**弗朗索瓦二世（François
II）**（），[法国](../Page/法国.md "wikilink")[瓦卢瓦王朝国王](../Page/瓦卢瓦王朝.md "wikilink")（1559年—1560年在位）。他是[亨利二世与](../Page/亨利二世_\(法兰西\).md "wikilink")[凯瑟琳·德·美第奇之子](../Page/凯瑟琳·德·美第奇.md "wikilink")，生于[枫丹白露](../Page/枫丹白露.md "wikilink")。

## 苏格兰配王

1548年弗朗索瓦4岁时，他的父亲亨利二世安排了他与表姐[苏格兰女王](../Page/苏格兰.md "wikilink")[玛丽一世的婚姻](../Page/玛丽一世_\(苏格兰\).md "wikilink")。玛丽一世是在只有9个月大的时候就成为苏格兰女王（1543年9月9日）。一俟这一婚约被正式批准，苏格兰[摄政](../Page/摄政.md "wikilink")——玛丽一世的母亲[玛丽·德·吉斯就把她](../Page/玛丽·德·吉斯.md "wikilink")6岁的女儿送到法国与法国王室一同生活，直到弗朗索瓦与玛丽一世长大履行婚约。1558年4月24日，14岁的法国王储与15岁的苏格兰女王举行了婚礼。这一连姻带有强烈的政治色彩：未来的法国国王同时也将得到苏格兰的王冠，并对[英格兰的王位构成要求](../Page/英格兰.md "wikilink")（玛丽一世对英格兰王位拥有继承权）。

## 少年国王

弗朗索瓦结婚一年后，他的父亲亨利二世去世。15岁的[王储加冕为法兰西国王弗朗索瓦二世](../Page/法国王太子.md "wikilink")。王太后凯瑟琳·德·美第奇担任摄政，但是据信掌握实权的人是权臣[吉斯公爵弗朗索瓦和](../Page/吉斯公爵_\(第二\).md "wikilink")[吉斯红衣主教查理](../Page/夏爾·德·洛林.md "wikilink")（他们是苏格兰女王[玛丽一世的舅舅](../Page/瑪麗一世_\(蘇格蘭\).md "wikilink")）兩兄弟，弗朗索瓦二世从未对政事進行實質干涉。在他任內，受到父親於1559年四月與西班牙所定的和平條約影響，法國的國際影響力逐漸下降；法國財政因為過去與西班牙四十多年的戰爭，負債高達四千八百萬[里弗](../Page/里弗.md "wikilink")（國王一年收入只有一千二百萬里弗），執政團隊因此實施緊縮政策，裁撤許多機構與軍隊，並且拒償借款利息，導致政府實質性破產。政府破產與1560年西歐開始[物價革命造成的動亂](../Page/物價革命.md "wikilink")，促發了法國的宗教大分裂，不久即爆發了長期內戰[胡格諾戰爭](../Page/胡格諾戰爭.md "wikilink")，王權降到谷底。

1560年，一直体弱多病的弗朗索瓦二世在[奥尔良去世](../Page/奥尔良.md "wikilink")，年仅16岁。死因是耳部感染引起的脑病变。他被葬在[圣但尼修道院](../Page/圣但尼修道院.md "wikilink")。

也許是因為健康問題，又或許是可能的[隱睪症](../Page/隱睪症.md "wikilink")，弗朗索瓦二世没有和玛丽一世生下任何孩子，\[1\]他的弟弟[查理九世继承了王位](../Page/查理九世_\(法兰西\).md "wikilink")。

## 註釋

[F](../Category/法国君主.md "wikilink") [F](../Category/瓦卢瓦王朝.md "wikilink")
[F](../Category/苏格兰君主配偶.md "wikilink")
[F](../Category/巴黎人.md "wikilink")

1.  Farquhar, Michael (2001). A Treasure of Royal Scandals: The Shocking
    True Stories History's Wickedest, Weirdest, Most Wanton Kings,
    Queens, Tsars, Popes, and Emperors,New York: Penguin Books,p.81