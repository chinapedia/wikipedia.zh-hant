**泰國國旗**（，音译**通代隆**，即三色旗之意）是一面[三色旗](../Page/三色旗.md "wikilink")，由紅－白－藍－白－紅五條橫帶組成，藍帶比紅白帶寬一倍。紅色代表著泰國的國土與[民族](../Page/民族.md "wikilink")；白色原指[上座部佛教](../Page/上座部佛教.md "wikilink")，後來則被解釋為[宗教之意](../Page/宗教.md "wikilink")；藍色則是指[王室和](../Page/泰國王室.md "wikilink")[國王](../Page/泰國君主.md "wikilink")。

现行國旗採用於1917年9月28日，由國王[拉瑪六世設計](../Page/拉瑪六世.md "wikilink")。

2017年起，每年9月28日是[泰国国旗纪念日](../Page/泰国国旗日.md "wikilink")，为非法定假日。\[1\]

## 设计

[Flag_of_Thailand_(construction).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Thailand_\(construction\).svg "fig:Flag_of_Thailand_(construction).svg")

| 色彩  | [CIELAB](../Page/CIELAB.md "wikilink") |       | 在其他颜色模式中 |
| --- | -------------------------------------- | ----- | -------- |
| L\* | a\*                                    | b\*   | ΔE\*     |
|     | 红                                      | 36.4  | 55.47    |
|     | 白                                      | 96.61 | \-0.15   |
|     | 蓝                                      | 18.63 | 7.89     |

## 历史演变

[大城王国时期](../Page/大城王国.md "wikilink")，泰国还没有国旗；当时以纯红色旗帜作为商船用旗，而非真正意义上的国旗。

1782年，[拉玛一世以红色为底设计了](../Page/拉玛一世.md "wikilink")[环刃旗](../Page/环刃.md "wikilink")，为王室船只专用旗。

1817年，[拉玛二世在旗帜中加入了](../Page/拉玛二世.md "wikilink")，为王室船只专用旗。

1855年，[拉玛四世设计了白象旗](../Page/拉玛四世.md "wikilink")，民间可使用红底白象旗，而王室则使用蓝底白象旗。

1916年，[拉玛六世为白象旗增添了一些装饰](../Page/拉玛六世.md "wikilink")。后来拉玛六世于某次外出某府访问时，接待方倒挂了旗帜。于是改成红白条旗。

1917年9月28日，拉玛六世把旗帜中间的条带改成蓝色，形成了现在的三色旗。\[2\]

## 其他旗帜

### 海軍旗

海軍旗（） 與國旗相同，中間有紅圈，中有寶象。

### 国籍旗

## 纪念

2017年9月28日，曼谷总理府为泰国国旗诞生100周年祭举行庆祝活动\[3\]。

## 相似旗帜

泰国国旗与[哥斯达黎加国旗形制相似](../Page/哥斯达黎加国旗.md "wikilink")，而比例和颜色组成（蓝-白-红-白-蓝）不同。

## 參考

## 外部連結

  - [泰国国旗博物馆官网](http://www.thaiflag.org)

[Category:泰国国家象征](../Category/泰国国家象征.md "wikilink")
[T](../Category/國旗.md "wikilink")
[Category:亞洲國旗](../Category/亞洲國旗.md "wikilink")
[Category:水平三色旗](../Category/水平三色旗.md "wikilink")
[Category:1917年面世的旗幟](../Category/1917年面世的旗幟.md "wikilink")
[Category:2017年面世的旗幟](../Category/2017年面世的旗幟.md "wikilink")

1.

2.
3.  [泰国国旗诞生100周年
    曼谷总理府9月28日将举行庆祝活动\!](http://www.taijuba.com/taijudongtai/20170920/taijuinfo_17598.html)