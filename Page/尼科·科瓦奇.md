**尼高·高華**（[克罗地亚语](../Page/克罗地亚语.md "wikilink")：，），是一名[克罗地亚职业足球运动员](../Page/克罗地亚.md "wikilink")，前任[克罗地亚国家队队长](../Page/克罗地亚国家足球队.md "wikilink")。在球场上他司职防守型中场，以抢截和传球技术而著称。現時執教[德甲](../Page/德甲.md "wikilink")[拜仁慕尼黑](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。

他和弟弟[罗伯特·科瓦奇都出生于德国柏林](../Page/罗伯特·科瓦奇.md "wikilink")，父母是从[前南斯拉夫到西德打工的克罗地亚人](../Page/南斯拉夫.md "wikilink")。科瓦奇十七岁时为柏林本地的球队效力，之后被[柏林赫塔签入](../Page/柏林赫塔.md "wikilink")，之后的职业生涯中曾效力于多家德甲俱乐部。

## 球員生涯

科瓦奇于1996年12月11日首次代表[克罗地亚国家队上场](../Page/克罗地亚国家足球队.md "wikilink")，但是他却因伤错过了[1998年世界杯而无缘铜牌](../Page/1998年世界杯.md "wikilink")。[2004年欧锦赛后](../Page/2004年欧洲足球锦标赛.md "wikilink")，他成为国家队队长，并率队打入并参加了[2006年世界杯和](../Page/2006年世界杯.md "wikilink")[2008年欧锦赛](../Page/2008年欧锦赛.md "wikilink")，是中场重要球员。

## 執教生涯

2013年10月17日，克罗地亚足協任命尼高·高華為[克罗地亚国家队新領隊](../Page/克罗地亚国家足球队.md "wikilink")，合約為期三年至2016年歐洲國家盃決賽週為止\[1\]。

2018年7月1日尼科·科瓦奇正式出任拜仁慕尼黑俱乐部主教练。合约为期三年到2021赛季结束为止

## 參考資料

## 參見

  - [罗伯特·科瓦奇](../Page/罗伯特·科瓦奇.md "wikilink")

[Category:2014年世界盃足球賽主教練](../Category/2014年世界盃足球賽主教練.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:克羅地亞國家足球隊球員](../Category/克羅地亞國家足球隊球員.md "wikilink")
[Category:克罗地亚足球运动员](../Category/克罗地亚足球运动员.md "wikilink")
[Category:克羅地亞旅外足球運動員](../Category/克羅地亞旅外足球運動員.md "wikilink")
[Category:哈化柏林球員](../Category/哈化柏林球員.md "wikilink")
[Category:利華古遜球員](../Category/利華古遜球員.md "wikilink")
[Category:漢堡球員](../Category/漢堡球員.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:薩爾斯堡紅牛球員](../Category/薩爾斯堡紅牛球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:在德國的克羅地亞人](../Category/在德國的克羅地亞人.md "wikilink")
[Category:柏林人](../Category/柏林人.md "wikilink")
[Category:德甲主教練](../Category/德甲主教練.md "wikilink")
[Category:在德國執教的外籍足球主敎練](../Category/在德國執教的外籍足球主敎練.md "wikilink")
[Category:法蘭克福主教練](../Category/法蘭克福主教練.md "wikilink")
[Category:拜仁慕尼黑主教練](../Category/拜仁慕尼黑主教練.md "wikilink")

1.