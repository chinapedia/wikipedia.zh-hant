[Y\!Dome-RoofOpen.JPG](https://zh.wikipedia.org/wiki/File:Y!Dome-RoofOpen.JPG "fig:Y!Dome-RoofOpen.JPG")

**福岡軟銀鷹**（，），是[軟銀旗下一支隸屬](../Page/軟銀.md "wikilink")[日本職棒](../Page/日本職棒.md "wikilink")[太平洋聯盟的](../Page/太平洋聯盟.md "wikilink")[職業棒球隊](../Page/職業棒球.md "wikilink")。最早的前身「南海軍」1938年9月即加入原[日本棒球聯盟](../Page/日本棒球聯盟.md "wikilink")；[二戰後](../Page/二戰.md "wikilink")，1950年太平洋聯盟、[中央聯盟正式分立](../Page/中央聯盟.md "wikilink")，「南海軍」改名「南海鷹」加入太平洋聯盟。[Fukuoka_SoftBank_Hawks_Ranking.svg](https://zh.wikipedia.org/wiki/File:Fukuoka_SoftBank_Hawks_Ranking.svg "fig:Fukuoka_SoftBank_Hawks_Ranking.svg")

## 球隊名沿革

  - 1938年：南海隊
  - 1944年：更名近畿日本隊（球季中改名）
  - 1946年：更名近畿GREAT RING隊<近畿大車輪>
  - 1947年：更名南海鷹隊（球季中改名）
  - 1989年：更名福岡大榮鷹隊
  - 2005年：更名**福岡軟銀鷹隊**

## 歷代總教練

<table>
<tbody>
<tr class="odd">
<td><p>教練名</p></td>
<td><p>接任年份</p></td>
<td><p>解任年份</p></td>
<td><p>日本一</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高須一雄.md" title="wikilink">高須一雄</a></p></td>
<td><p><a href="../Page/1939年.md" title="wikilink">1939年</a></p></td>
<td><p><a href="../Page/1940年.md" title="wikilink">1940年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三谷八郎.md" title="wikilink">三谷八郎</a></p></td>
<td><p><a href="../Page/1941年.md" title="wikilink">1941年</a></p></td>
<td><p><a href="../Page/1942年.md" title="wikilink">1942年</a></p></td>
<td></td>
<td><p>季中解任</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加藤喜作.md" title="wikilink">加藤喜作</a></p></td>
<td><p><a href="../Page/1942年.md" title="wikilink">1942年</a></p></td>
<td><p><a href="../Page/1942年.md" title="wikilink">1942年</a></p></td>
<td></td>
<td><p>季中接任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高田勝生.md" title="wikilink">高田勝生</a></p></td>
<td><p><a href="../Page/1943年.md" title="wikilink">1943年</a></p></td>
<td><p><a href="../Page/1943年.md" title="wikilink">1943年</a></p></td>
<td></td>
<td><p>季中解任</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加藤喜作.md" title="wikilink">加藤喜作</a></p></td>
<td><p><a href="../Page/1943年.md" title="wikilink">1943年</a></p></td>
<td><p><a href="../Page/1945年.md" title="wikilink">1945年</a></p></td>
<td></td>
<td><p>季中接任</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山本一人.md" title="wikilink">山本一人</a></p></td>
<td><p><a href="../Page/1946年.md" title="wikilink">1946年</a></p></td>
<td><p><a href="../Page/1965年.md" title="wikilink">1965年</a></p></td>
<td><p>2次</p></td>
<td><p><a href="../Page/1959年.md" title="wikilink">1959年後恢復舊姓鶴岡</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔭山和夫.md" title="wikilink">蔭山和夫</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年</a></p></td>
<td></td>
<td><p>代理</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔭山和夫.md" title="wikilink">蔭山和夫</a></p></td>
<td><p><a href="../Page/1965年.md" title="wikilink">1965年</a></p></td>
<td><p><a href="../Page/1965年.md" title="wikilink">1965年</a></p></td>
<td></td>
<td><p>就職4日後即急症病逝<br />
由前任鶴岡一人續掌兵符</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鶴岡一人.md" title="wikilink">鶴岡一人</a></p></td>
<td><p><a href="../Page/1966年.md" title="wikilink">1966年</a></p></td>
<td><p><a href="../Page/1968年.md" title="wikilink">1968年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飯田徳治.md" title="wikilink">飯田徳治</a></p></td>
<td><p><a href="../Page/1969年.md" title="wikilink">1969年</a></p></td>
<td><p><a href="../Page/1969年.md" title="wikilink">1969年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野村克也.md" title="wikilink">野村克也</a></p></td>
<td><p><a href="../Page/1970年.md" title="wikilink">1970年</a></p></td>
<td><p><a href="../Page/1977年.md" title="wikilink">1977年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穴吹義雄.md" title="wikilink">穴吹義雄</a></p></td>
<td><p><a href="../Page/1977年.md" title="wikilink">1977年</a></p></td>
<td><p><a href="../Page/1977年.md" title="wikilink">1977年</a></p></td>
<td></td>
<td><p>代理</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/広瀬叔功.md" title="wikilink">広瀬叔功</a></p></td>
<td><p><a href="../Page/1978年.md" title="wikilink">1978年</a></p></td>
<td><p><a href="../Page/1980年.md" title="wikilink">1980年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Don_Blazer.md" title="wikilink">Don Blazer</a></p></td>
<td><p><a href="../Page/1981年.md" title="wikilink">1981年</a></p></td>
<td><p><a href="../Page/1982年.md" title="wikilink">1982年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/穴吹義雄.md" title="wikilink">穴吹義雄</a></p></td>
<td><p><a href="../Page/1983年.md" title="wikilink">1983年</a></p></td>
<td><p><a href="../Page/1985年.md" title="wikilink">1985年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杉浦忠.md" title="wikilink">杉浦忠</a></p></td>
<td><p><a href="../Page/1986年.md" title="wikilink">1986年</a></p></td>
<td><p><a href="../Page/1989年.md" title="wikilink">1989年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田淵幸一.md" title="wikilink">田淵幸一</a></p></td>
<td><p><a href="../Page/1990年.md" title="wikilink">1990年</a></p></td>
<td><p><a href="../Page/1992年.md" title="wikilink">1992年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/根本陸夫.md" title="wikilink">根本陸夫</a></p></td>
<td><p><a href="../Page/1993年.md" title="wikilink">1993年</a></p></td>
<td><p><a href="../Page/1994年.md" title="wikilink">1994年</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王貞治.md" title="wikilink">王貞治</a></p></td>
<td><p><a href="../Page/1995年.md" title="wikilink">1995年</a></p></td>
<td><p><a href="../Page/2008年.md" title="wikilink">2008年</a></p></td>
<td><p>2次</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/森脇浩司.md" title="wikilink">森脇浩司</a></p></td>
<td><p><a href="../Page/2006年.md" title="wikilink">2006年</a></p></td>
<td><p><a href="../Page/2006年.md" title="wikilink">2006年</a></p></td>
<td></td>
<td><p>代理</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秋山幸二.md" title="wikilink">秋山幸二</a></p></td>
<td><p><a href="../Page/2009年.md" title="wikilink">2009年</a></p></td>
<td><p><a href="../Page/2014年.md" title="wikilink">2014年</a></p></td>
<td><p>2次</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/工藤公康.md" title="wikilink">工藤公康</a></p></td>
<td><p><a href="../Page/2015年.md" title="wikilink">2015年</a></p></td>
<td></td>
<td><p>3次</p></td>
<td><p>現任</p></td>
</tr>
</tbody>
</table>

<noinclude></noinclude>

## 現役球員／教練團

### 教練團

  - 總教練

<!-- end list -->

  -
<!-- end list -->

  - 一軍教練

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 二軍監督

<!-- end list -->

  -
<!-- end list -->

  - 二軍教練

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 三軍監督

<!-- end list -->

  -
<!-- end list -->

  - 三軍教練

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
### 現役球員

  - 投手

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 捕手

<!-- end list -->

  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 內野手

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 外野手

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 陣中臺灣球員

  - [劉瀨章](../Page/劉瀨章.md "wikilink")（1938年～1940年）
  - [高英傑](../Page/高英傑.md "wikilink")（1980年～1983年）
  - [李來發](../Page/李來發.md "wikilink")（1980年～1983年）
  - [陳文賓](../Page/陳文賓.md "wikilink")（2002年～2003年）
  - [陽耀勳](../Page/陽耀勳.md "wikilink")（2005年～2013年）
  - [蕭一傑](../Page/蕭一傑.md "wikilink")（2012年～2013年）
  - [李杜軒](../Page/李杜軒.md "wikilink")（2007年～2016年）

## 參考文獻

## 國際交流

  - [軟銀所屬子公司](../Page/軟銀.md "wikilink")[整體開發贊助](../Page/整體開發.md "wikilink")[中國棒球協會來經營](../Page/中國棒球協會.md "wikilink")[中國棒球聯賽](../Page/中國棒球聯賽.md "wikilink")，[日本及](../Page/日本.md "wikilink")[中國的棒球交流愈來愈密切](../Page/中國.md "wikilink")。

## 外部連結

  - [福岡軟銀鷹隊官方網站](http://www.softbankhawks.co.jp/)

  -
[Category:紀錄模板](../Category/紀錄模板.md "wikilink")
[Category:日本職棒球隊](../Category/日本職棒球隊.md "wikilink")
[Category:SoftBank](../Category/SoftBank.md "wikilink") [Category:中央區
(福岡市)](../Category/中央區_\(福岡市\).md "wikilink")
[Category:1938年日本建立](../Category/1938年日本建立.md "wikilink")
[Category:小倉北區](../Category/小倉北區.md "wikilink")
[Category:日本職業體育大獎得獎者](../Category/日本職業體育大獎得獎者.md "wikilink")