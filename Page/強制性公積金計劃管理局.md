[Kowloon_Commerce_Centre.jpg](https://zh.wikipedia.org/wiki/File:Kowloon_Commerce_Centre.jpg "fig:Kowloon_Commerce_Centre.jpg")1座（圖左）\]\]

**強制性公積金計劃管理局**（簡稱**強積金管理局**或**積金局**，英文：，縮寫**MPFA**）是[香港法定機構之一](../Page/香港法定機構.md "wikilink")，根據《[香港法例](../Page/香港法例.md "wikilink")》《強制性公積金計劃條例》於1998年9月成立，專門負責規管及監督[強制性公積金計劃的運作](../Page/強制性公積金.md "wikilink")。

## 歷任首長

主席︰

  - [李業廣](../Page/李業廣.md "wikilink")（1998年－2007年）
  - [范鴻齡](../Page/范鴻齡.md "wikilink")（2007年－2009年3月17日）
  - [胡红玉](../Page/胡红玉.md "wikilink")（2009年3月17日－2015年3月17日）
  - [黃友嘉](../Page/黃友嘉.md "wikilink")（2015年3月17日－）

行政總監︰

  - 不設行政總監一職（1998年9月－2000年5月）
  - [許仕仁](../Page/許仕仁.md "wikilink")（2000年6月－2003年6月30日）\[1\]
  - [陳唐芷青](../Page/陳唐芷青.md "wikilink")（署理行政總監︰2003年7月1日－2004年6月30日，行政總監︰2004年7月1日－2018年6月30日）\[2\]
  - [羅盛梅](../Page/羅盛梅.md "wikilink")（行政總監︰2018年7月1日－）

## 組織架構

  - 主席

<!-- end list -->

  - [黃友嘉博士](../Page/黃友嘉.md "wikilink")

<!-- end list -->

  - 董事

（[非執行董事](../Page/非執行董事.md "wikilink")）

  - [陳鑑林先生](../Page/陳鑑林.md "wikilink")
  - [劉麥嘉軒女士](../Page/劉麥嘉軒.md "wikilink")
  - [黃傑龍先生](../Page/黃傑龍.md "wikilink")
  - [關百豪先生](../Page/關百豪.md "wikilink")
  - [黃旭倫先生](../Page/黃旭倫.md "wikilink")
  - [潘兆平議員](../Page/潘兆平.md "wikilink")
  - [石禮謙議員](../Page/石禮謙.md "wikilink")
  - [黃國先生](../Page/黃國.md "wikilink")
  - [劉怡翔議員](../Page/劉怡翔.md "wikilink")

（候補成員：[財經事務及庫務局](../Page/財經事務及庫務局.md "wikilink")[常任秘書長](../Page/常任秘書長.md "wikilink")（財經事務））

  - [羅致光議員](../Page/羅致光.md "wikilink")

（候補成員：[勞工及福利局常任秘書長](../Page/勞工及福利局.md "wikilink")）

（[執行董事](../Page/執行董事.md "wikilink")）

  - [羅盛梅女士](../Page/羅盛梅.md "wikilink")（行政總監、董事局副主席）
  - [余家寶女士](../Page/余家寶.md "wikilink")（執行董事（政策））
  - [許慧儀女士](../Page/許慧儀.md "wikilink") （執行董事（成員））
  - [鄭恩賜先生](../Page/鄭恩賜.md "wikilink")（機構事務總監）

## 参考资料

## 外部連結

  - [強制性公積金計劃管理局](http://www.mpfa.org.hk)
  - [香港強積金網](http://www.mpf.hk)

[Category:香港法定機構](../Category/香港法定機構.md "wikilink")
[Category:香港執法機構](../Category/香港執法機構.md "wikilink")

1.  [積金局任命署理行政總監](http://www.info.gov.hk/gia/general/200302/17/0217189.htm)
2.  [陳唐芷青出任積金局行政總監](http://www.info.gov.hk/gia/general/brandhk/100604c4.htm)