[Taiwan_2009_East_Coast_ShihTiPing_Giant_Stone_Steps_Algae_FRD_6581.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_East_Coast_ShihTiPing_Giant_Stone_Steps_Algae_FRD_6581.jpg "fig:Taiwan_2009_East_Coast_ShihTiPing_Giant_Stone_Steps_Algae_FRD_6581.jpg")石頭上的綠藻類\]\]

*'
綠藻門*'（[学名](../Page/学名.md "wikilink")：）是[植物中的一門](../Page/植物.md "wikilink")\[1\]，包含有約8000個[物種](../Page/物種.md "wikilink")\[2\][1](https://www.webcitation.org/5kwQwljiE?url=http://encarta.msn.com/media_461543986/Major_Algae_Phyla.html)，大部份都是[水生](../Page/水生生態系.md "wikilink")[光合](../Page/光合作用.md "wikilink")[真核生物](../Page/真核.md "wikilink")。和[陸生植物](../Page/陸生植物.md "wikilink")
（[苔蘚植物和](../Page/苔蘚植物.md "wikilink")[維管植物](../Page/維管植物.md "wikilink")）一樣，綠藻也含有[葉綠素](../Page/葉綠素.md "wikilink")
*a* 和 *c*
，且將能源轉化為[澱粉存在其](../Page/澱粉.md "wikilink")[色素體內](../Page/色素體.md "wikilink")。\[3\]綠藻門和[輪藻門與](../Page/輪藻門.md "wikilink")[有胚植物相關](../Page/有胚植物.md "wikilink")，一起合稱為[绿色植物](../Page/绿色植物.md "wikilink")。

此門包含單細胞和多細胞的物種。大部份物種生活在淡水裡，其他許多物種則居住在海水中，另外還有一些物種適應了許多的環境，如[雪藻居住在夏天的高山雪原中](../Page/雪藻.md "wikilink")。其他還有依附在岩石或樹木上頭的。一些[地衣和真菌與綠藻有共生關係](../Page/地衣.md "wikilink")，綠藻門中的藻類也會和[原生動物](../Page/原生動物.md "wikilink")、海綿及[刺胞動物等形成共生關係](../Page/刺胞動物門.md "wikilink")。有些會有鞭毛而利於移動，有些會行同配生殖或異配生殖等有性生殖。

## 分类

  - [羽藻綱](../Page/羽藻綱.md "wikilink") （Bryopsidophyceae）
  - [綠藻綱](../Page/綠藻綱.md "wikilink") （Chlorophyceae）
  - [平藻綱](../Page/平藻綱.md "wikilink") （Pedinophyceae）
  - [胸膜肌腱](../Page/胸膜肌腱.md "wikilink")（Pleurastrophyceae）
  - [綠色鞭毛藻綱](../Page/綠色鞭毛藻綱.md "wikilink")，或称[绿枝藻纲](../Page/绿枝藻纲.md "wikilink")
    （Prasinophyceae）
  - [共球藻纲](../Page/共球藻纲.md "wikilink")
    （[Trebouxiophyceae](../Page/Trebouxiophyceae.md "wikilink")）
  - [石蓴綱](../Page/石蓴綱.md "wikilink")
    （[Ulvophyceae](../Page/Ulvophyceae.md "wikilink")）

## 參考文獻

## 相關閱讀

**Burrows, E.M.** 1991. *Seaweeds of the British Isles.* Volume 2
Chlorophyta. Natural History Museum, London. ISBN o-565-00981-8

[L](../Category/綠藻.md "wikilink") [綠藻門](../Category/綠藻門.md "wikilink")

1.
2.  **Hoek, C. van den, Mann, D.G. and Jahns, H.M.** 1995. *Algae An
    Introduction to Phycology.* Cambridge University Press, Cambridge.
    ISBN 0-521-30419-9

3.