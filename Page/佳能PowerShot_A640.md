[Canon_PowerShot_A640_img_0782.jpg](https://zh.wikipedia.org/wiki/File:Canon_PowerShot_A640_img_0782.jpg "fig:Canon_PowerShot_A640_img_0782.jpg")

**佳能 PowerShot
A640**是一款[佳能出品的](../Page/佳能.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，属于[Canon
PowerShot系列](../Page/Canon_PowerShot.md "wikilink")，它于[2006年8月发布](../Page/2006年8月.md "wikilink")。

**A640**的前任是[Canon PowerShot
A620](../Page/Canon_PowerShot_A620.md "wikilink")。

佳能在A640上，第一次将千万[像素引入](../Page/像素.md "wikilink")[PowerShot系列](../Page/Canon_PowerShot.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")。虽然相机成像质量好坏不能单纯凭借像素指标，但是“千万像素”确实是一个足够吸引人的“广告数字”。

## 主要性能参数

  - 1 千万 有效[像素](../Page/像素.md "wikilink")
  - 4倍光学[变焦](../Page/变焦.md "wikilink")
  - 1/1.8 英寸 [CCD](../Page/CCD.md "wikilink")
  - 2.5寸可旋转TFT液晶屏，分辨率11.5万象素
  - 快门：15～1/2500秒
  - [ISO](../Page/ISO.md "wikilink")：80/100/200/400/800
  - 9点智能[对焦](../Page/对焦.md "wikilink")
  - 有声短片记录（[Motion JPEG编码与单声道音频](../Page/Motion_JPEG.md "wikilink")）
  - 使用[SDHC卡](../Page/SDHC卡.md "wikilink")／[SD卡](../Page/SD卡.md "wikilink")／[MMC卡作为存储介质](../Page/MMC卡.md "wikilink")
  - 使用[DIGIC II数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用4节[AA电池](../Page/AA电池.md "wikilink")
  - [Exif 2.2](../Page/EXIF.md "wikilink")
  - 不带电池约245克

## 改机

佳能PowerShot A6x0系列拥有较大尺寸的CCD，为优质成像提供了保证。有爱好者研究出对 A6x0 及[S2
IS与](../Page/Canon_PowerShot_S2_IS.md "wikilink")[S3
IS进行修改而使其能输出](../Page/Canon_PowerShot_S3_IS.md "wikilink")[RAW格式图像的方法](../Page/RAW.md "wikilink")。这使A6x0变得更加具有吸引力。

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon PowerShot A620](../Page/Canon_PowerShot_A620.md "wikilink")

## 外部链接

  - [佳能（中国）](http://www.canon.com.cn)
  - [Canon PowerShot A640
    Review](http://www.dpreview.com/reviews/canona640/) - Digital
    Photography Review
  - [A6x0、S3IS破解](http://www.xitek.com/forum/showthread.php?threadid=418343)
    - 色影无忌上转载的破解方法及讨论

[en:Canon PowerShot A640](../Page/en:Canon_PowerShot_A640.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")