[LAH_screenshot.PNG](https://zh.wikipedia.org/wiki/File:LAH_screenshot.PNG "fig:LAH_screenshot.PNG")

**LHC@home**是一个[分布式计算项目](../Page/分布式计算.md "wikilink")。

40多年来，[粒子物理实验及](../Page/粒子物理实验.md "wikilink")[粒子物理学不断的取得进展](../Page/粒子物理学.md "wikilink")，与此同时，每前进一步，[加速器实验都面临新的技术挑战](../Page/加速器实验.md "wikilink")。物理学家们越来越迫切地需要更高强度的[高能粒子](../Page/高能粒子.md "wikilink")，而目前的实验设备已经无法满足这一需求。随着[分布式计算技术的日益成熟及全球参与人数的逐渐增加](../Page/分布式计算.md "wikilink")，LHC@home项目应运而生。

LHC@home将被用来模拟设计[大型強子對撞機](../Page/大型強子對撞機.md "wikilink")（LHC），它将是本世纪初[粒子物理与天文理事会](../Page/粒子物理与天文理事会.md "wikilink")[粒子物理计划中的主要部分](../Page/粒子物理计划.md "wikilink")。预计将于2007年建成的LHC是质心能量为14
TeV的质子-质子对撞机。由于[能域的提高](../Page/能域.md "wikilink")，将有助于帮助科学家将由较低能量的实验所预言的现象更为方便地揭示出来。

LHC@home项目的主持单位是[歐洲核子研究組織](../Page/歐洲核子研究組織.md "wikilink")（[CERN](../Page/CERN.md "wikilink")）。LHC@home运行平台为[BOINC平台](../Page/BOINC.md "wikilink")。
LHC@home的Sixtrack程序一般每次模拟60个粒子绕加速器环运行100,000（有时可能达到1,000,000）圈的运行情况。这看似是一个很长的过程，但在真实世界中它们只需要不到10秒的时间。但这已足以检验到底粒子束是在长时间内保持稳定的运行，还是会失去控制并导致加速器由于损坏而停止。没有LHC@home分布式计算项目的运行，对撞机粒子束的不稳定性是一个很严重的问题，可能会严重减慢人类建立大型对撞机的进程。

## 参考文献

## 外部链接

  - [LHC@home项目官方介绍主页](https://web.archive.org/web/20110706143500/http://lhcathome.web.cern.ch/lhcathome/)
  - [LHC@home
    Classic项目官方参与主页](http://lhcathomeclassic.cern.ch/sixtrack/)
  - [Virtual LHC@home项目官方参与主页](http://lhcathome2.cern.ch/vLHCathome/)
  - [LHC@home中文官方介绍主页](http://boinc.equn.com/lhc/)
  - [LHC@home中文官方论坛](http://www.equn.com/forum/)
  - [LHC@home中文详细介绍](http://www.equn.com/distributed/ap-science.html#lhcathome)
  - [伯克利开放式网络计算平台（BOINC）](http://boinc.equn.com/)
  - [BOINC计算平台使用详解](http://www.equn.com/forum/viewthread.php?tid=5753)
  - [LHC@home中国分布式计算总站](http://equn.com/wiki/LHC@home)

## 参见

  - [分布式计算](../Page/分布式计算.md "wikilink")
  - [网格计算](../Page/网格计算.md "wikilink")
  - [BOINC](../Page/BOINC.md "wikilink")

{{-}}

[Category:粒子加速器](../Category/粒子加速器.md "wikilink")
[Category:伯克利开放式网络计算平台](../Category/伯克利开放式网络计算平台.md "wikilink")