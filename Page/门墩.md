[Menduner.JPG](https://zh.wikipedia.org/wiki/File:Menduner.JPG "fig:Menduner.JPG")的抱鼓形门礅儿（[抱鼓石](../Page/抱鼓石.md "wikilink")）\]\]
[강릉_임영관_삼문_03.jpg](https://zh.wikipedia.org/wiki/File:강릉_임영관_삼문_03.jpg "fig:강릉_임영관_삼문_03.jpg")[江原道](../Page/江原道.md "wikilink")[江陵市](../Page/江陵市.md "wikilink")的祥雲形木门礅\]\]
**门墩**，又称**门座**、**门台**，是用于東亞传统建築，的[大门底部](../Page/大门.md "wikilink")，起到支撑[门框](../Page/门框.md "wikilink")，[门轴作用的一个构件](../Page/门轴.md "wikilink")，多為石製，但也有木製者。整体称[门枕石](../Page/门枕石.md "wikilink")，门外部分称为门墩，主要有箱形和抱鼓形（[抱鼓石](../Page/抱鼓石.md "wikilink")）两种。门枕石在中间有一个槽用于支撑门框，门内部分有一海窝用于插入[门纂](../Page/门纂.md "wikilink")（门轴的下端），与固定在[中槛上的](../Page/中槛.md "wikilink")[连楹一起起到固定门轴](../Page/连楹.md "wikilink")，便于门的开关的作用。

[中國的門墩常見於](../Page/中國.md "wikilink")[四合院建築](../Page/四合院.md "wikilink")，是[门楼中比较有特色的一个组成部件](../Page/四合院#门楼.md "wikilink")，门礅上通常雕刻一些中国传统的吉祥图案，因此是了解中国传统文化的石刻艺术品。北京的门礅主要以箱形和抱鼓形居多，但还有狮子形，多角柱形，水瓶形门礅。

[File:Mendun.jpg|箱形门墩](File:Mendun.jpg%7C箱形门墩)
[File:Zhuxingmendun.JPG|六角柱形门墩](File:Zhuxingmendun.JPG%7C六角柱形门墩)
[File:Shizimendun.JPG|狮子门墩](File:Shizimendun.JPG%7C狮子门墩)
<File:Korea-Gyeongbokgung-14.jpg>|[韓國](../Page/韓國.md "wikilink")[景福宮的木製抱鼓形门墩](../Page/景福宮_\(韓國\).md "wikilink")

[Baogu.JPG](https://zh.wikipedia.org/wiki/File:Baogu.JPG "fig:Baogu.JPG")
门墩通常由[须弥座](../Page/须弥座.md "wikilink")，抱鼓或方箱，以及兽吻或[狮子](../Page/狮子.md "wikilink")（有说是[狻猊](../Page/狻猊.md "wikilink")）几部分构成。根据门楼的形制不同门墩的形制也有差异。

[须弥座是整个门礅的基础](../Page/须弥座.md "wikilink")，一般会刻有[莲花的形状](../Page/莲花.md "wikilink")，上面通常有锦铺。抱鼓是一个竖立着的鼓，再鼓面和鼓的侧面通常雕刻有各种吉祥纹样，有福在眼前，岁岁平安，福禄寿等图案。方箱式一立方体，四面都刻有不同的纹样。在抱鼓或箱体上面雕刻有兽吻或狮子。整个门枕石由一块整石雕刻而成。

<File:Mendun>
fuzaiyanqian.JPG|[福在眼前](../Page/福在眼前.md "wikilink")（取蝠在眼钱的谐音）
<File:Mendun_suisuipingan.JPG>|[岁岁平安](../Page/岁岁平安.md "wikilink")（取穗穗瓶鹌的谐音）
<File:Mendun_fulushou.JPG>|[福禄寿](../Page/福禄寿.md "wikilink")（取蝠鹿寿（桃）的谐音）

由于[文化大革命中的](../Page/文化大革命.md "wikilink")“破四旧”，现在北京[四合院门墩上的狮子大多已残破](../Page/四合院.md "wikilink")。随着老城改造和拆迁，门墩越来越少，[北京石刻艺术博物馆和](../Page/真觉寺.md "wikilink")[北京语言文化大学内的枕石园收藏和保护了一些门礅](../Page/北京语言文化大学.md "wikilink")，并向公众展出。
[Mendun_102.JPG](https://zh.wikipedia.org/wiki/File:Mendun_102.JPG "fig:Mendun_102.JPG")

## 参考文献

## 外部链接

  - [门墩艺术展](https://web.archive.org/web/20050204181348/http://lib.blcu.edu.cn/per0/md/md.htm)
  - [中国の門墩](http://mendun.jimdo.com/)

[Category:東亞傳統建築裝飾](../Category/東亞傳統建築裝飾.md "wikilink")
[Category:東亞傳統建築構件](../Category/東亞傳統建築構件.md "wikilink")