\-{H|干諾道}-
[HK_Route_4_Western_End.jpg](https://zh.wikipedia.org/wiki/File:HK_Route_4_Western_End.jpg "fig:HK_Route_4_Western_End.jpg")
**4號幹線**（）是[香港的一條](../Page/香港.md "wikilink")[主要幹線](../Page/香港幹線編號系統.md "wikilink")，沿[香港島連接](../Page/香港島.md "wikilink")[港島東區的](../Page/東區_\(香港\).md "wikilink")[柴灣和](../Page/柴灣.md "wikilink")[中西區的](../Page/中西區_\(香港\).md "wikilink")[堅尼地城](../Page/堅尼地城.md "wikilink")，是[港島區的重要幹道](../Page/香港島.md "wikilink")，全長14.48公里\[1\]。

這條幹線途經的主要道路有：（柴灣方向顯示）[干諾道西天橋](../Page/干諾道.md "wikilink")、[林士街天橋](../Page/林士街天橋.md "wikilink")、[中環灣仔繞道及](../Page/中環灣仔繞道.md "wikilink")[東區走廊](../Page/東區走廊.md "wikilink")。

4號幹線分別與[2號幹線及](../Page/2號幹線.md "wikilink")[3號幹線連接](../Page/3號幹線.md "wikilink")。2019年1月20日[中環灣仔繞道通車前](../Page/中環灣仔繞道.md "wikilink")，也與[1號幹線連接](../Page/1號幹線.md "wikilink")。

另外，4號幹線的部份路標並不正確（在[中環灣仔繞道通車後](../Page/中環灣仔繞道.md "wikilink")，4號幹線共縮短了300米，但沒有修改這些路標（路標12.5-14.5），因此這些路標實際應減0.3（即12.2-14.2）才是正確的）。

## 歷史

  - 東區走廊[維園道至](../Page/維園道.md "wikilink")[太古城段於](../Page/太古城.md "wikilink")1984年6月通車
  - 東區走廊太古城至[筲箕灣段於](../Page/筲箕灣.md "wikilink")1985年7月通車
  - 東區走廊筲箕灣至柴灣段於1989年10月通車
  - [林士街天橋於](../Page/林士街天橋.md "wikilink")1990年1月21日通車
  - 干諾道西天橋於1997年3月通車
  - [中環灣仔繞道於](../Page/中環灣仔繞道.md "wikilink")2019年1月20日首階段通車，2019年2月24日全線通車

### 中環灣仔繞道

[中環灣仔繞道連接干諾道西天橋和](../Page/中環灣仔繞道.md "wikilink")[東區走廊](../Page/東區走廊.md "wikilink")[炮台山出口](../Page/炮台山.md "wikilink")，以紓緩干諾道西、[夏慤道](../Page/夏慤道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[維園道一段](../Page/維園道.md "wikilink")4號幹線、[金鐘道和](../Page/金鐘道.md "wikilink")[軒尼詩道的擠塞和阻隔往銅鑼灣](../Page/軒尼詩道.md "wikilink")、跑馬地和香港仔隧道車輛，方便往來[東區走廊之車輛](../Page/東區走廊.md "wikilink")，取代有關路段成為4號幹線的一部份。繞道於2010年1月奠基，2019年1月20日首階段通車，屆時將不再與1號幹線連接，並在建成後需要拆除原有林士街天橋東行線落斜橋面，以令繞道西行能直接上橋，所有工程於2019年2月24日完成。

## 未來發展

### 堅尼地城至香港仔段

4號幹線的[堅尼地城至香港仔段至今仍未興建](../Page/堅尼地城.md "wikilink")。這是一條長8公里的雙程雙線行車路，由[堅尼地城經香港島西岸](../Page/堅尼地城.md "wikilink")，經[沙灣](../Page/沙灣_\(香港\).md "wikilink")、[鋼綫灣及](../Page/鋼綫灣.md "wikilink")[華富](../Page/華富邨.md "wikilink")，延伸至[香港仔](../Page/香港仔.md "wikilink")，與1號幹線連接。當中在[數碼港](../Page/數碼港.md "wikilink")[資訊道及](../Page/資訊道.md "wikilink")[華貴邨海旁設有預留空間接駁幹線](../Page/華貴邨.md "wikilink")。惟現時為止，有關工程尚未有確實時間表。[香港特別行政區政府曾表示將這部份幹線和](../Page/香港特別行政區政府.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[南港島綫一同研究](../Page/南港島綫.md "wikilink")，短期只會選其中一項優先興建，後來當局同意興建[南港島綫東段](../Page/南港島綫東段.md "wikilink")，但與4號幹線走線相近的[南港島綫西段暫未有興建時間表](../Page/南港島綫西段.md "wikilink")。

2010年3月26日，[城市規劃委員會宣佈修訂](../Page/城市規劃委員會.md "wikilink")[摩星嶺分區計劃大綱核准圖](../Page/摩星嶺.md "wikilink")，刪除昔日擬議的7號幹線路線（即現稱的4號幹線堅尼地城至香港仔段）\[2\]，意味著當局擱置此段幹線的興建。2019年，政府宣佈發展[數碼港第](../Page/數碼港.md "wikilink")5期，選址正是4號幹線於鋼綫灣的預留地段（現為[數碼港公園](../Page/數碼港公園.md "wikilink")），進一步引證當局已放棄興建此段幹線。

## 出口

\! colspan="4" style="border-bottom: 5px solid \#B58B25;" |  4號幹線 |-
style="text-align:center;" \! colspan=2 |
往[堅尼地城方向](../Page/堅尼地城.md "wikilink") \!\!
colspan=2 | 往[柴灣方向](../Page/柴灣.md "wikilink") |-
style="text-align:center;" | width=30 | 編號 || width=300 | 前往地區 ||
width=300 | 前往地區 || width=30 | 編號 |- style="text-align:center;" \!
colspan=4 | 連接[柴灣道迴旋處](../Page/柴灣道.md "wikilink") |- | 1A || 利眾街 || ||
|- | 2 || [東區醫院](../Page/東區醫院.md "wikilink") ||
[杏花邨](../Page/杏花邨.md "wikilink")、[小西灣](../Page/小西灣.md "wikilink")
|| 2 |- | 2A ||
[愛秩序灣](../Page/愛秩序灣.md "wikilink")、[阿公岩](../Page/阿公岩.md "wikilink")
|| || |- | 2B || [筲箕灣](../Page/筲箕灣.md "wikilink") || || |- | || ||
[石澳](../Page/石澳.md "wikilink")、[赤柱](../Page/赤柱.md "wikilink") || 3A
|- | || ||
[筲箕灣](../Page/筲箕灣.md "wikilink")、[愛秩序灣](../Page/愛秩序灣.md "wikilink")
| 3B |- | rowspan=2 | || rowspan=2 | || [康山](../Page/康山.md "wikilink")
|| rowspan=2 | 3C（右） |- |
[西灣河](../Page/西灣河.md "wikilink")、[太古城](../Page/太古城.md "wikilink")
|- | 4 || 太古城 || 太古城 || 4 |- | 5 ||
[九龍（東）](../Page/東九龍.md "wikilink")（經[東區海底隧道](../Page/東區海底隧道.md "wikilink")）
 || 九龍（東）（經東區海底隧道）  || 5（右） |- | 6 || [北角](../Page/北角.md "wikilink") ||
[鰂魚涌](../Page/鰂魚涌.md "wikilink") || 6 |- | || || 北角 || 6A |- | 6B ||
[銅鑼灣](../Page/銅鑼灣.md "wikilink")、[跑馬地](../Page/跑馬地.md "wikilink")、[九龍](../Page/九龍.md "wikilink")（經[紅磡海底隧道](../Page/紅磡海底隧道.md "wikilink")）、香港仔及黃竹坑（經香港仔隧道）
|| || |- | 7 || [灣仔（北）](../Page/灣仔北.md "wikilink")||
[灣仔（北）](../Page/灣仔北.md "wikilink")（必須經過出口8前往） || 7 |- |
8 || 中區、機場快綫站 || 中區 || 8 |- | 8A ||
[西營盤](../Page/西營盤.md "wikilink")、[薄扶林](../Page/薄扶林.md "wikilink")
|| || |- | 9 || [九龍（西）](../Page/西九龍.md "wikilink")（經西區海底隧道）  ||
九龍（西）（經西區海底隧道）  || 9 |- style="text-align:center;" \!
colspan=4 | 連接[城西道](../Page/城西道.md "wikilink")

### 中環灣仔繞道通車前的出口

\! colspan="4" style="border-bottom: 5px solid \#B58B25;" |  4號幹線 |-
style="text-align:center;" \! colspan=2 |
往[堅尼地城方向](../Page/堅尼地城.md "wikilink") \!\!
colspan=2 | 往[柴灣方向](../Page/柴灣.md "wikilink") |-
style="text-align:center;" | width=30 | 編號 || width=300 | 前往地區 ||
width=300 | 前往地區 || width=30 | 編號 |- style="text-align:center;" \!
colspan=4 | 連接[柴灣道迴旋處](../Page/柴灣道.md "wikilink") |- | 1A || 利眾街 || ||
|- | 2 || [東區醫院](../Page/東區醫院.md "wikilink") ||
[杏花邨](../Page/杏花邨.md "wikilink")、[小西灣](../Page/小西灣.md "wikilink")
|| 2 |- | 2A ||
[愛秩序灣](../Page/愛秩序灣.md "wikilink")、[阿公岩](../Page/阿公岩.md "wikilink")
|| || |- | 2B || [筲箕灣](../Page/筲箕灣.md "wikilink") || || |- | || ||
[石澳](../Page/石澳.md "wikilink")、[赤柱](../Page/赤柱.md "wikilink") || 3A
|- | || ||
[筲箕灣](../Page/筲箕灣.md "wikilink")、[愛秩序灣](../Page/愛秩序灣.md "wikilink")
| 3B |- | rowspan=2 | || rowspan=2 | || [康山](../Page/康山.md "wikilink")
|| rowspan=2 | 3C（右） |- |
[西灣河](../Page/西灣河.md "wikilink")、[太古城](../Page/太古城.md "wikilink")
|- | 4 || 太古城 || 太古城 || 4 |- | 5 ||
[九龍（東）](../Page/東九龍.md "wikilink")（經[東區海底隧道](../Page/東區海底隧道.md "wikilink")）
 || 九龍（東）（經東區海底隧道）  || 5（右） |- | 6 || [北角](../Page/北角.md "wikilink") ||
[鰂魚涌](../Page/鰂魚涌.md "wikilink") || 6 |- | || || 北角 || 6A |- | 6B ||
[銅鑼灣](../Page/銅鑼灣.md "wikilink") || || |- | || ||
[維園道](../Page/維園道.md "wikilink")、[電氣道](../Page/電氣道.md "wikilink")
|| 6C |- | 7 ||
銅鑼灣、[跑馬地](../Page/跑馬地.md "wikilink")、[香港仔](../Page/香港仔.md "wikilink")
|| 銅鑼灣、[大坑](../Page/大坑.md "wikilink") || 7 |- | rowspan=2 | 8 ||
rowspan=2 |
[九龍](../Page/九龍.md "wikilink")（經[紅磡海底隧道](../Page/紅磡海底隧道.md "wikilink")）
 || 九龍（經紅磡海底隧道）  || rowspan=2 | 8 |- | 跑馬地、香港仔  |- | || ||
[貨物裝卸區](../Page/灣仔貨物裝卸區.md "wikilink")、[鴻興道](../Page/鴻興道.md "wikilink")
|| 8A |- | 9 || [灣仔（北）](../Page/灣仔北.md "wikilink") ||
[港灣道](../Page/港灣道.md "wikilink") || 9 |- | || ||
灣仔（北）、[香港會議展覽中心](../Page/香港會議展覽中心.md "wikilink")
|| 9A |- | 9B || [灣仔](../Page/灣仔.md "wikilink") || || |- | || ||
[香港演藝學院](../Page/香港演藝學院.md "wikilink") || 9C |- | || ||
[愛丁堡廣場](../Page/愛丁堡廣場.md "wikilink") || 9D |- | 10 ||
[金鐘](../Page/金鐘.md "wikilink")、[紅棉路](../Page/紅棉路.md "wikilink") ||
金鐘 || 10 |- | 10A || [美利道](../Page/美利道.md "wikilink") || || |- | 11A
|| 會所街 || || |- | 12A ||
[康樂廣場](../Page/康樂廣場.md "wikilink")、[機場快綫站](../Page/香港站.md "wikilink")
|| || |- | 12B || [上環](../Page/上環.md "wikilink") || || |- | || ||
[康樂廣場](../Page/康樂廣場.md "wikilink")、機場快綫站 ||
<abbr title="{{#tag:nowiki|現時為4號幹線主線}}">12C</abbr> |- |
<abbr title="{{#tag:nowiki|已更改編號為8A}}">12D</abbr> ||
[西營盤](../Page/西營盤.md "wikilink")、[薄扶林](../Page/薄扶林.md "wikilink")
|| || |- | <abbr title="{{#tag:nowiki|已更改編號為9}}">13</abbr> ||
[九龍（西）](../Page/西九龍.md "wikilink")（經西區海底隧道）  || 九龍（西）（經西區海底隧道）
 || <abbr title="{{#tag:nowiki|已更改編號為9}}">13</abbr> |-
style="text-align:center;" \! colspan=4 |
連接[城西道](../Page/城西道.md "wikilink")

## 參考文獻

## 外部链接

  - 運輸署：4號幹線路線圖
    [JPG版本](http://www.td.gov.hk/mini_site/hksrens/2008/TC/images/Route4.jpg)
    [PDF版本](http://www.td.gov.hk/mini_site/hksrens/2008/TC/images/Route4.pdf)



[Category:香港幹線](../Category/香港幹線.md "wikilink")

1.  [1](https://www.google.com.hk/maps/dir/22.28689,114.1316934/22.2869902,114.1544322/22.2636577,114.2378517/@22.2789628,114.1854546,12z/data=!4m2!4m1!3e0?hl=zh-TW)
2.  [摩星嶺大綱圖刪七號幹線](http://archive.news.gov.hk/isd/ebulletin/tc/category/infrastructureandlogistics/100326/html/100326tc06009.htm)，香港政府新聞網，2010年3月26日