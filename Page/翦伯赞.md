**翦伯赞**（），名**象时**，字**伯赞**，[以字行](../Page/以字行.md "wikilink")，\[1\][维吾尔族](../Page/维吾尔族.md "wikilink")，[湖南省](../Page/湖南省.md "wikilink")[桃源县](../Page/桃源县.md "wikilink")[枫树乡回维村人](../Page/枫树维吾尔族回族乡.md "wikilink")，曾任北京大学副校长（1952年至1968年）、历史系主任。[中国著名馬克思主義](../Page/中国.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")，与[范文澜一起系统地应用马克思主义方法](../Page/范文澜.md "wikilink")，重新解释了中国历史，是中国马克思主义历史学的主要奠基人之一，曾参与[北伐战争](../Page/北伐战争.md "wikilink")，大革命失败后，在历史学家[吕振羽等人影响下](../Page/吕振羽.md "wikilink")，开始用马克思主义观点研究中国社会和历史问题。五四运动以后被称为史学界的“马列五老”之一，与[郭沫若](../Page/郭沫若.md "wikilink")、[吕振羽](../Page/吕振羽.md "wikilink")、[侯外庐](../Page/侯外庐.md "wikilink")、[范文澜并称为当时的马克思主义史学的代表人物](../Page/范文澜.md "wikilink")。\[2\]\[3\]

## 生平

### 家庭

翦伯赞的祖先是[高昌](../Page/高昌.md "wikilink")[维吾尔族](../Page/维吾尔族.md "wikilink")[哈勒巴士](../Page/哈勒巴士.md "wikilink")。明初，[朱元璋册封哈勒八士为](../Page/朱元璋.md "wikilink")“[荆襄都督](../Page/荆襄都督.md "wikilink")”，赐姓“翦”，把义女[杜叶公主赐给哈勒八士为妻](../Page/杜叶公主.md "wikilink")。1373年，哈勒八士被封为[镇南定国将军](../Page/镇南定国将军.md "wikilink")，加[太子太保衔](../Page/太子太保.md "wikilink")，在[湖广的](../Page/湖广.md "wikilink")[辰州](../Page/辰州.md "wikilink")、[常德一带镇守](../Page/常德.md "wikilink")。翦氏传至第七代时因事被革去官爵，后代在湖南桃源定居，或从商，或务农。翦伯赞的父亲[翦万效是](../Page/翦万效.md "wikilink")[晚清](../Page/晚清.md "wikilink")[秀才](../Page/秀才.md "wikilink")，兼通数学，中华民国成立后受聘担任中学数学教师，被称为“翦几何”，还曾担任常德中学、常桃汉沅联合县立中学校长。\[4\]\[5\]

### 早年

1916年进[北京法政专门学校学习](../Page/北京法政专门学校.md "wikilink")，不久又转入[国立武昌商业专门学校](../Page/国立武昌商科大学.md "wikilink")（今[武汉大学经济管理学院前身](../Page/武汉大学.md "wikilink")），1919年毕业。1924年赴[美国](../Page/美国.md "wikilink")[加利福尼亚大学攻读经济专业](../Page/加利福尼亚大学.md "wikilink")。回国後研究[史学和](../Page/歷史學.md "wikilink")[历史哲学](../Page/历史哲学.md "wikilink")。

### 民國時期

他在1936年出版的《历史哲学教程》一书，从中国国情出发、系统阐述历史唯物主义的教科书，是中国第一部系统的马克思主义史学理论著作。1937年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，从1940年起，他长期在[周恩来的直接领导下](../Page/周恩来.md "wikilink")，在重庆、上海等地从事统一战线和理论宣传工作，完成了《中国史纲》第一卷、第二卷和《中国史论集》等。\[6\][抗日战争期间](../Page/抗日戰爭_\(中國\).md "wikilink")，他撰写文章批判[中国国民党](../Page/中国国民党.md "wikilink")，稱其政策“亲日独裁”。

### 中華人民共和國時期

1952年，全国高校院系调整，翦伯赞转到北京大学，任历史学系教授兼系主任长达16年，任北大副校长6年。翦伯赞协同[郭沫若](../Page/郭沫若.md "wikilink")、[范文澜等人筹建了](../Page/范文澜.md "wikilink")[中国历史学会](../Page/中国历史学会.md "wikilink")。历任第一届[全国政协委员](../Page/全国政协.md "wikilink")，第一、二、三届[全国人民代表大会代表](../Page/全国人民代表大会.md "wikilink")，中央民族事务委员会委员，[中国科学院哲学社会科学部学部委员](../Page/中国科学院.md "wikilink")。

翦伯贊1952年嚴厲批判[張東蓀的中間路線](../Page/張東蓀.md "wikilink")\[7\]，1957年[反右運動時著文嚴厲批判](../Page/反右運動.md "wikilink")[雷海宗](../Page/雷海宗.md "wikilink")、[向達](../Page/向達.md "wikilink")、榮孟源為史學界的右派。\[8\]

#### 文革前观点

1961年高等学校文科教材编选计划会议决定，委托翦伯赞主编《中国史纲要》，作为高校文科中国通史教材之用。上级部门鼓励翦伯赞按照共产党平素所主张的观点来写，于是中国从西周时期进入封建社会的学说写进了教材。郭沫若、翦伯赞肯定吕振羽的主张“殷商是奴隶社会”，也为教科书所用。1958年毛泽东多次为曹操翻案，说曹操是天下大乱时期出现的“非常之人”、“超世之杰”，是“代表正义一方的。”翦伯赞在1959年2月发表《应该替曹操恢复名誉》一文，说：“曹操不仅是三国豪族中第一流的政治家、军事家和诗人，并且是中国封建统治阶级中有数的杰出人物，”长期把这样一个杰出人物当做奸臣，是不公平的。“我们应该替曹操摘去奸臣的帽子，替曹操恢复名誉。”毛泽东看了翦伯赞的文章后进一步发挥自己评曹操的观点，从评曹操这一特定历史人物的角度告诉人们：无产阶级就是要讲专政。\[9\]

翦伯赞继承了[顾颉刚的](../Page/顾颉刚.md "wikilink")“华夏族羌人起源说”\[10\]，指出“在野蛮时代之初，分布于甘肃、青海一带的诸羌之族，亦开始新的迁徙，一批批羌人走下昆仑山，来到东方，创造了中华民族的早期文明。”\[11\]翦伯赞与费孝通、谭其骧、白寿彝、翁独健一同支撑起了‘自古论’和‘共创论’大厦的框架。\[12\]

翦伯赞指出，“过去以至现在，都是以大汉族主义为中心，处理中国的历史，因此，过去以至现在的中国史著述(都不是中国史，而是大汉族史。但是大汉族史，不是中国史）。”“研究中国史，首先应该抛弃那种以大汉族主义为中心之狭义的种族主义的立场，把自己超然于种族主义之外，用极客观的眼光，把大汉族及其以外之中国境内的诸种族，都当作中国史构成的历史单位”。\[13\]

#### 文革中被迫害自杀

[文化大革命前](../Page/文化大革命.md "wikilink")，翦伯贊是历史学界[历史主义派的主帅](../Page/历史主义.md "wikilink")，研究了古代的许多历史事件。因其所持的让步政策的观点与[毛泽东观点相左](../Page/毛泽东.md "wikilink")，以及反对[姚文元对](../Page/姚文元.md "wikilink")[吴晗的](../Page/吴晗.md "wikilink")《[海瑞罢官](../Page/海瑞罢官.md "wikilink")》的批判，1960年代後期即被批判。

1951年2月，翦伯赞在《学习》杂志上发表《论中国古代的农民战争》，指出：“每一次大暴动都或多或少推动了中国封建社会的发展。因为在每一次大暴动之后，新的统治者，为了恢复封建秩序，必须对农民作某种程度的让步，这就是说，必须或多或少减轻对农民的剥削和压迫，这样就减轻了封建生产关系对生产力的拘束，使得封建社会的生产力又有继续发展的可能，这样就推动了中国历史的前进，因而中国历史上的每一个农民暴动或农民战争，可以说，都是中国封建社会向前发展的里程碑。”\[14\]

1961年以来，翦伯赞先后为历史专业组撰写了《对处理若干历史问题的初步意见》、《目前史学研究中存在的几个问题》、《关于处理中国史上的民族关系问题》等文章，针对当时史学研究中的一些观点，提出既要重视阶级观点，又要注意历史主义。这些文章很受时任中央宣传部副部长、中央文教小组成员的[周扬的赞赏](../Page/周扬.md "wikilink")，分别刊发于《简报》和《通讯》。

翦伯赞在《对处理若干历史问题的初步意见》中，对他十年前在《论中国古代的农民战争》中的说法作了重要修改。他说：“在经历了一次大的农民战争以后，封建统治阶级为了恢复封建秩序，有时对农民作出一定程度的让步，但不是对每一次农民战争都让步，他们对于那些小的局部的农民战争是不会让步的。让不让，让多少，这要决定于阶级对抗的形势，决定于农民战争带来的阶级力量对比的变化。”\[15\]让步政策的主要意义为，每次农民革命战争推翻地主阶级的统治，改朝换代之后，新的王朝为了缓和阶级矛盾，恢复生产力的发展，会吸取前朝的教训，对农民阶级做一些让步，正是这些让步政策有力推动了历史的发展。

1965年12月，《[红旗](../Page/紅旗_\(雜誌\).md "wikilink")》杂志发表[戚本禹的文章](../Page/戚本禹.md "wikilink")《为革命而研究历史》，对翦伯赞的历史观点进行了批判，攻击翦伯赞的观点是“超阶级”、“纯客观”的资产阶级观点。12月21日，毛泽东发话：“戚本禹的文章很好，我看了三遍，缺点是没有点名。”1966年3月，《红旗》杂志又发表戚本禹等三人的文章《翦伯赞同志的历史观点应当批判》，给翦伯赞扣上“资产阶级史学代表人物”的帽子，说他的上述两篇文章是“反马克思主义的史学纲领”。\[16\]1966年3月28日－30日，[毛泽东在上海三次同](../Page/毛泽东.md "wikilink")[康生](../Page/康生.md "wikilink")、[江青等人谈话](../Page/江青.md "wikilink")，他说：北京市针插不进，水泼不进，要解散市委；[中宣部是](../Page/中宣部.md "wikilink")“阎王殿”，要“打倒阎王，解放小鬼”；说[吴晗](../Page/吴晗.md "wikilink")、翦伯赞是学阀，上面还有包庇他们的大党阀（指[彭真](../Page/彭真.md "wikilink")）。\[17\]

文化大革命初，被当作“[反动学术权威](../Page/反动学术权威.md "wikilink")”，被扣上“反对[马克思主义](../Page/马克思主义.md "wikilink")”的帽子，备受[肉体摧残](../Page/肉体.md "wikilink")，[人格凌辱](../Page/人格.md "wikilink")。毛泽东曾於中共八屆十二中全會上发出“最高指示”，特别提到“对北京大学的翦伯赞、[冯友兰要给出路](../Page/冯友兰.md "wikilink")”。但江青的手下依然通过秘密成立的“翦伯赞专案组”，对其进行[逼供](../Page/逼供.md "wikilink")，要求他证明1935年[刘少奇与](../Page/刘少奇.md "wikilink")[国民政府谈判时有变节行为](../Page/国民政府.md "wikilink")。翦於1968年12月18日夜，夫妻双双吃下大量[安眠药](../Page/安眠药.md "wikilink")[自杀身亡](../Page/自杀.md "wikilink")。死时口袋内有两张纸条，一张说“实在没有什么可交代的”；一张三呼毛主席[万岁](../Page/万岁.md "wikilink")。

#### 官方平反

1978年8月，[中共中央领导人](../Page/中共中央.md "wikilink")[邓小平亲自批示](../Page/邓小平.md "wikilink")：“我认为应予昭雪”，为他彻底平反昭雪。\[18\]1979年2月22日官方举办了他的追悼会。\[19\]

## 名言

  - 史料譬如一堆散亂在地上的大錢，必須用一根繩才能把它贯串起來，這根繩就是[馬克思主義](../Page/馬克思主義.md "wikilink")。\[20\]

## 著作

主编《中国史纲要》，-{著}-有《中国历史哲学教程》、《中国史论集》、《中国史纲》、《历史问题论丛》、《先秦史》、《秦漢史》等。

## 評價

[余英時認為](../Page/余英時.md "wikilink")，翦伯赞對中國史的分期，代表了當時中國大陸史學界的正統，其中心系統不是從中國史的內部整理出來的，而是借自西方的現成模式。他在史學上的地位如何，恐怕要看後人是不是能從他的著作中繼續得到啟發。\[21\][余英時称](../Page/余英時.md "wikilink")，燕京大學有「四大真空管」的說法，翦伯贊即為其中之一。「真空」指的是學問空疏。因翦伯贊「眉眼擠在一處」，[鄧之誠的姨太太私下稱翦伯贊為](../Page/鄧之誠.md "wikilink")「臭蟲」。\[22\]

## 参考文献

## 外部链接

  - [著名史学家翦伯赞自杀之谜](http://news.sina.com.cn/richtalk/news/culture/9903/031120.html)（[南方周末](../Page/南方周末.md "wikilink")）

{{-}}

[Category:中華人民共和國歷史學家](../Category/中華人民共和國歷史學家.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:第一屆全國政協委員](../Category/第一屆全國政協委員.md "wikilink")
[Category:第一届全国人大代表](../Category/第一届全国人大代表.md "wikilink")
[Category:第二屆全國人大代表](../Category/第二屆全國人大代表.md "wikilink")
[Category:第三屆全國人大代表](../Category/第三屆全國人大代表.md "wikilink")
[Category:中國共產黨黨員](../Category/中國共產黨黨員.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:武汉大學校友](../Category/武汉大學校友.md "wikilink")
[Category:反动学术权威](../Category/反动学术权威.md "wikilink")
[Category:文革被迫害学者](../Category/文革被迫害学者.md "wikilink")
[Category:文革自杀者](../Category/文革自杀者.md "wikilink")
[Category:维吾尔族人](../Category/维吾尔族人.md "wikilink")
[Category:桃源县人](../Category/桃源县人.md "wikilink")
[B伯赞](../Category/翦姓.md "wikilink")
[Category:自杀作家](../Category/自杀作家.md "wikilink")
[Category:马克思主义史学家](../Category/马克思主义史学家.md "wikilink")

1.

2.  [马克思主义史学家](http://zgsxs.net.cn/news_more.asp?lm2=137)

3.  中国人民大学. 书报资料中心,1999,《历史学》

4.

5.

6.  [北京纪念翦伯赞诞辰110周年](http://cpc.people.com.cn/GB/64093/64387/7121955.html)

7.

8.

9.  [三种猜测和两种误区
    毛泽东缘何重评曹操](http://cpc.people.com.cn/GB/64162/64172/85037/85038/6027451.html)

10. 顾颉刚，《从古籍中探索我国的西部民族——羌族》

11. [强卫在青海省文化改革发展大会上的讲话](http://cpc.people.com.cn/GB/64093/64102/16462035.html)

12. 孙勇,《怎样认识西藏自古以来就是中国的一部分》

13. 《翦伯赞全集》第3卷

14.

15.
16.
17. [中国共产党大事记1966年](http://cpc.people.com.cn/GB/64162/64164/4416081.html)

18.

19.

20. 《那時我們正年輕——北京大學歷史系系友回憶錄》，現代教育出版社，2007年

21.

22. 余英時回憶錄，頁85。