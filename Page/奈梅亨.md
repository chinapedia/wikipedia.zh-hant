**奈美根**（，\[1\]，[Nijmeegs](../Page/South_Guelderish.md "wikilink"):
*Nimwegen*
，[英語化後](../Page/英語化.md "wikilink")，曾被拼為Nimeguen\[2\]）是位于[荷兰东部](../Page/荷兰.md "wikilink")，瓦尔河沿岸，靠近[德国边陲的城市](../Page/德国.md "wikilink")。城市被认为是荷兰历史最悠久的城市，2005年奈美根庆祝了建城2000周年。西歐著名消費電子製造商
[Packard Bell](../Page/Packard_Bell.md "wikilink") 誕生于奈梅根。

## 城市杂记

  - 奈美根是荷兰足球队[NEC奈梅亨的主场城市](../Page/NEC奈梅亨.md "wikilink")。
  - 奈美根拒绝荷兰政治家，前[欧盟委员](../Page/欧盟.md "wikilink")[弗里特·博克斯坦的提议将城市名改为](../Page/弗里特·博克斯坦.md "wikilink")"Marxograd
    at the Waal".
  - 奈美根是第一个公开进行[麻将锦标赛的欧洲国家](../Page/麻将.md "wikilink")，赛事是在2006年6月15-26日举行的。
  - 奈美根刚建成时叫做'Noviomagus'。
  - [EA公司游戏](../Page/电子艺界.md "wikilink")[荣誉勋章：前线中的](../Page/荣誉勋章：前线.md "wikilink")**奈美根**Waalbridge所展现的是**奈美根**桥的一部分。

## 重要节日

## 友好城市

奈美根有下列5座友好城市：

  - [俄罗斯](../Page/俄罗斯.md "wikilink")，[普斯科夫](../Page/普斯科夫.md "wikilink")

  - [尼加拉瓜](../Page/尼加拉瓜.md "wikilink")，[马萨亚](../Page/马萨亚.md "wikilink")

  - [日本](../Page/日本.md "wikilink")，[東松山市](../Page/東松山市.md "wikilink")

  - [美国](../Page/美国.md "wikilink")，[奥尔巴尼](../Page/奥尔巴尼.md "wikilink")

  - [中国](../Page/中国.md "wikilink")，[苏州市](../Page/苏州市.md "wikilink")。

## 画廊

<File:Nijmegen> Marktplatz.jpg|奈梅亨市场广场 <File:Nijmegen> - Belvedère
1900.jpg|瞭望塔，现在是饭店 <File:View> on the waal janvangoyen.jpg|瓦尔河
<File:Nijmegen> - Stadhuis en Grote Markt 1900.jpg|1900年的奈梅亨
[File:Goffertnijmegen.jpg|足球场](File:Goffertnijmegen.jpg%7C足球场)
<File:Nijmegen> Goffertpark, overzicht grote weide.JPG|Goffertpark公园
<File:Nijmegen>, Kruittoren in Kronenburgerpark foto9 2010-12-20
13.55.JPG|Kronenburgerpark公园 <File:2013.05.30.181624> Ruin Valkhof Park
Nijmegen NL.jpg|Valkhof Park公园 <File:Park> West, Kometenstraat, Hees,
Nijmegen.jpg|Park West公园

## 外部链接

## 外部链接

  - [Official site](http://www.nijmegen.nl/)
  - [Novio site](http://www.novio.nl/) - information about the city bus
    service
  - [NEC Nijmegen site](http://www.nec-nijmegen.nl/)
  - [News from Nijmegen](http://www.nijmegennieuws.nl)

[Category:海爾德蘭省城市](../Category/海爾德蘭省城市.md "wikilink")
[奈梅亨](../Category/奈梅亨.md "wikilink")

1.  舊式拼法：Nijmwegen，Nymegen，Nieumeghen，這些拼法仍然保存在[德語中](../Page/德語.md "wikilink")。在[法語中](../Page/法語.md "wikilink")，拼為，在[西班牙語與](../Page/西班牙語.md "wikilink")[義大利語中](../Page/義大利語.md "wikilink")，*Nimega*。
2.  [荷蘭鐵路圖，年代早於1930年，在此拼為"Nimeguen"](http://www.everythinginenglish.nl/forum/trainpictures.php#comment-1682839051)