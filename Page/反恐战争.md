**反恐战争**（[英文](../Page/英文.md "wikilink")：**War on
Terror**）是[美国及其盟友用来称呼一场进行中的](../Page/美国.md "wikilink")、以“消灭国际[恐怖主义](../Page/恐怖主义.md "wikilink")”为目标的全球性[战争](../Page/战争.md "wikilink")，起因于[九一一事件](../Page/九一一事件.md "wikilink")。

反恐战争的现阶段目标包括：阻止那些美国所认定为[恐怖组织的团体](../Page/恐怖组织.md "wikilink")（其中大部分是像[蓋達組織](../Page/蓋達組織.md "wikilink")、[塔利班等伊斯兰极端军事组织](../Page/塔利班.md "wikilink")）对美国及其盟国发动恐怖袭击；為传播“[自由与](../Page/自由.md "wikilink")[民主](../Page/民主.md "wikilink")”且终结那些支持恐怖主义的[流氓國家与](../Page/流氓國家.md "wikilink")[失败國家](../Page/失败國家.md "wikilink")”的現有政权。反恐战争中的首次军事行动是[北大西洋公约组織在](../Page/北大西洋公约组織.md "wikilink")2003年10月进行的代号“积极奋进”的海军演习，旨在阻止恐怖分子获得[大规模杀伤性武器](../Page/大规模杀伤性武器.md "wikilink")；此外另一個標誌是推翻为[奧薩瑪·賓·拉登提供庇护的](../Page/奧薩瑪·賓·拉登.md "wikilink")[塔利班所進行的](../Page/塔利班.md "wikilink")[阿富汗戰爭](../Page/阿富汗戰爭_\(2001年\).md "wikilink")。

反恐战争是由[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亞](../Page/澳大利亞.md "wikilink")、[英國](../Page/英國.md "wikilink")、[歐盟](../Page/歐盟.md "wikilink")（包括[法國](../Page/法國.md "wikilink")、[德國及](../Page/德國.md "wikilink")[意大利](../Page/意大利.md "wikilink")）及其他盟国的支持下发动的。反恐战争包括多种手段，如外交手段、经济制裁、加强国土安全及与其他国家开展安全合作等。

## 词源

“反恐戰爭”这个词已被用來專指由美國、英國及其盟友對判定為恐怖組織或制度的实体進行的一切軍事行動，但其他獨立的反恐行動如俄羅斯和印度的军事行动並不属于这个范畴。反恐戰爭也被稱為：

  - [第三次世界大戰](../Page/第三次世界大戰.md "wikilink")\[1\]
  - 第四次世界大戰\[2\]（假設[冷戰是第三次世界大戰](../Page/冷戰.md "wikilink")）
  - 布希的反恐戰爭\[3\]
  - 長期戰爭\[4\]\[5\]
  - 全球反恐戰爭\[6\]
  - 打擊蓋達組織的戰爭\[7\]

## 相關條目

  - [伊斯蘭恐怖主義](../Page/伊斯蘭恐怖主義.md "wikilink")
  - [伊斯蘭法西斯主義](../Page/伊斯蘭法西斯主義.md "wikilink")
  - [九一一襲擊事件](../Page/九一一襲擊事件.md "wikilink")
  - [阿富汗戰爭](../Page/阿富汗戰爭_\(2001年\).md "wikilink")
  - [伊拉克戰爭](../Page/伊拉克戰爭.md "wikilink")
  - [以黎衝突](../Page/2006年以黎衝突.md "wikilink")
  - [加薩危機](../Page/2006年加薩危機.md "wikilink")
  - [加薩戰爭](../Page/加薩戰爭.md "wikilink")
  - [雲柱行動](../Page/雲柱行動.md "wikilink")
  - [以巴衝突](../Page/2014年以巴衝突.md "wikilink")
  - [利比亞內戰](../Page/利比亞內戰.md "wikilink")
  - [敘利亞內戰](../Page/敘利亞內戰.md "wikilink")
  - [伊拉克內戰](../Page/伊拉克內戰.md "wikilink")
      - [伊拉克北部內戰](../Page/2014年伊拉克北部內戰.md "wikilink")
  - [小布希](../Page/小布希.md "wikilink")
  - [歐巴馬](../Page/歐巴馬.md "wikilink")

## 参考资料

[Category:2003年伊拉克](../Category/2003年伊拉克.md "wikilink")
[Category:2005年伊拉克](../Category/2005年伊拉克.md "wikilink")
[Category:2006年伊拉克](../Category/2006年伊拉克.md "wikilink")
[Category:基地组织](../Category/基地组织.md "wikilink")
[Category:反恐](../Category/反恐.md "wikilink")
[Category:恐怖主义](../Category/恐怖主义.md "wikilink")
[Category:奧地利戰爭](../Category/奧地利戰爭.md "wikilink")
[Category:保加利亞戰爭](../Category/保加利亞戰爭.md "wikilink")
[Category:加拿大戰爭](../Category/加拿大戰爭.md "wikilink")
[Category:丹麥戰爭](../Category/丹麥戰爭.md "wikilink")
[Category:衣索比亞戰爭](../Category/衣索比亞戰爭.md "wikilink")

1.

2.

3.

4.
5.  "Abizaid Credited With Popularizing the Term 'Long War'", 3 February
    2006: *Washington Post* traces history of the phrase "Long War"
    [1](http://www.washingtonpost.com/wp-dyn/content/article/2006/02/02/AR2006020202242.html)
6.

7.