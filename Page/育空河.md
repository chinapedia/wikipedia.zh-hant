**育空河**（[英語](../Page/英語.md "wikilink")：**Yukon
River**）是[北美洲主要的水系之一](../Page/北美洲.md "wikilink")，發源於[加拿大的](../Page/加拿大.md "wikilink")[育空地區](../Page/育空.md "wikilink")（而育空地區即以發源於該地區的育空河來命名）。育空河長3,700[公里](../Page/公里.md "wikilink")，並由發源地向北流於[育空－庫斯科奎姆沖積扇](../Page/育空－庫斯科奎姆沖積扇.md "wikilink")（Yukon-Kuskokwim
Delta）注入[白令海](../Page/白令海.md "wikilink")，有超過一半的河道位於[美國](../Page/美國.md "wikilink")[阿拉斯加州的境內](../Page/阿拉斯加州.md "wikilink")，而其他的部份則大都位於育空地區，[英屬哥倫比亞也佔了一小部份](../Page/英屬哥倫比亞.md "wikilink")。

育空河平均流速為6,430[立方公尺](../Page/立方公尺.md "wikilink")/秒\[1\]，流域面積為832,700平方公里\[2\]，其中323,800平方公里位在加拿大境內，超過[德州或](../Page/德州.md "wikilink")[亞伯達省面積的](../Page/亞伯達省.md "wikilink")25%。育空河是阿拉斯加州或育空地區最長的河流，所以它也是1896年至1903年間的[克朗代克淘金潮主要的運輸途徑](../Page/克朗代克淘金潮.md "wikilink")，而[河輪直到](../Page/河輪.md "wikilink")1950年代還繼續往返於育空河上，後來才被[克朗代克高速公路所取代](../Page/克朗代克高速公路.md "wikilink")。

育空河在[哥威迅語](../Page/哥威迅語.md "wikilink")（Gwich'in）中意為「大河」。育空河上游河道原先的名稱是劉易斯河（Lewes
River），這段河道從[馬歇爾湖開始](../Page/馬歇爾湖.md "wikilink")，並在[些吉港](../Page/些吉港.md "wikilink")（Fort
Selkirk）與[佩利河](../Page/佩利河.md "wikilink")（Pelly River）合流。

## 流域

[Yukon_River_drainage_basin.gif](https://zh.wikipedia.org/wiki/File:Yukon_River_drainage_basin.gif "fig:Yukon_River_drainage_basin.gif")

育空河的源頭公認是在[英屬哥倫比亞](../Page/英屬哥倫比亞.md "wikilink")[阿特林湖](../Page/阿特林湖.md "wikilink")（Atlin
Lake）南端的利維爾恩冰河。有些人則認為位在[科里克特路徑](../Page/科里克特路徑.md "wikilink")（Chilkoot
Trail）北端的林德曼湖才是育空河的源頭。阿特林湖最後經由阿特林河注入[塔吉胥湖](../Page/塔吉胥湖.md "wikilink")（Tagish
Lake），而林德曼湖則流入[班奈特湖](../Page/班奈特湖.md "wikilink")（Bennett
Lake）。塔吉胥湖的水則經由塔吉胥河流入[馬歇湖](../Page/馬歇湖.md "wikilink")（Marsh
Lake）。所以育空河的開端應該是在馬歇湖的北端，大約在[白馬市
(加拿大)的南方](../Page/白馬市_\(加拿大\).md "wikilink")。有些人則爭論說育空河的源頭其實是[塔斯林湖](../Page/塔斯林湖.md "wikilink")（Teslin
Lake）與[塔斯林河](../Page/塔斯林河.md "wikilink")（Teslin
River），它們在抵達育空地區時的水勢比其他支流都要大。育空河流域的北緣被認定是在劉易斯河（Lewes
River）。育空河在白馬市的北方流入[拉巴基湖](../Page/拉巴基湖.md "wikilink")（Laberge
Lake）。其他屬於育空河流域的大湖則包括Kusawa湖（流入[塔克希尼河](../Page/塔克希尼河.md "wikilink")）及[庫魯涅湖](../Page/庫魯涅湖.md "wikilink")（Kluane
Lake，流入白河）。

育空河在育空地區流過白馬市、[喀馬克斯](../Page/喀馬克斯.md "wikilink")（Carmacks）及[道森市](../Page/道森市.md "wikilink")，在[阿拉斯加州則通過](../Page/阿拉斯加州.md "wikilink")[老鷹城](../Page/老鷹城.md "wikilink")、[瑟科](../Page/瑟科.md "wikilink")（Circle）、[育空港](../Page/育空港.md "wikilink")、[史蒂文斯鎮](../Page/史蒂文斯鎮.md "wikilink")、[塔納納](../Page/塔納納.md "wikilink")（Tanana）、（Ruby）、[加利納](../Page/加利納.md "wikilink")（Galena）、[奴拉托](../Page/奴拉托.md "wikilink")（Nulato）、[格雷陵](../Page/格雷陵.md "wikilink")（Grayling）、[荷里克羅斯](../Page/荷里克羅斯.md "wikilink")（Holy
Cross）、[俄羅斯教區](../Page/俄羅斯教區.md "wikilink")（Russian
Mission）、[馬歇爾](../Page/馬歇爾.md "wikilink")（Marshall）、[領港站](../Page/領港站.md "wikilink")（Pilot
Station）、[聖瑪麗](../Page/聖瑪麗.md "wikilink")（St. Mary's）及Mountain
Village等地。育空河通過Mountain Village後就分成許多河道散佈在沖積扇上。

## 污染

[CharleyRiverAtYukon1.jpg](https://zh.wikipedia.org/wiki/File:CharleyRiverAtYukon1.jpg "fig:CharleyRiverAtYukon1.jpg")上鳥瞰育空河流域\]\]

育空河曾經被挖掘[金礦](../Page/金.md "wikilink")、[軍事建設](../Page/軍事.md "wikilink")、掩埋場及廢水所[汙染](../Page/汙染.md "wikilink")，不過[美國國家環境保護局並未將育空河列入遭受污染的河川名單中](../Page/美國國家環境保護局.md "wikilink")\[3\]。而[美國地質調查局的相關資料則顯示育空河的濁度](../Page/美國地質調查局.md "wikilink")、金屬含量及溶氧量都位在一個相對優良的階段\[4\]。

由64個加拿大及美國的[第一民族及](../Page/第一民族.md "wikilink")[印地安人部落所組成](../Page/印地安人.md "wikilink")[育空河部落流域議會企圖讓育空河流域的水可以再度被人類安全的飲用](../Page/育空河部落流域議會.md "wikilink")。

## 地理及生態

育空河流域的一些分水嶺被[黑雲杉](../Page/黑雲杉.md "wikilink")（*Picea
mariana*）所覆蓋\[5\]，牠們分布在靠近[蘇華德半島](../Page/蘇華德半島.md "wikilink")（Seward
Peninsula）的地區，這也是幾乎是黑雲杉分布地區的最西端\[6\]。黑雲杉也是北美洲分布最廣泛的[松柏植物之一](../Page/松柏植物.md "wikilink")。

## 參考資料

## 外部連結

  - [Canadian Council for Geographic Education page with a series of
    articles on the history of the Yukon
    River](https://web.archive.org/web/20091201231305/http://www.ccge.org/ccge/english/resources/rivers/tr_rivers_yukonRiver.asp)
  - [The Yukon River Bridge at Dawson
    City](http://www.yukonriverbridge.com/)
  - [1](http://www.yukonriverlodge) "Experience Alaska's Yukon River &
    the Nowitna National Wildlife Refuge" at ["Yukon River
    Lodge"](http://www.yukonriverlodge.com)
  - [Yukon River Inter-Tribal Watershed Council](http://www.yritwc.org/)

[Category:白令海](../Category/白令海.md "wikilink")
[Category:北美洲跨國河流](../Category/北美洲跨國河流.md "wikilink")
[Category:阿拉斯加河流](../Category/阿拉斯加河流.md "wikilink")
[Category:加拿大河流](../Category/加拿大河流.md "wikilink")
[Category:育空地區地理](../Category/育空地區地理.md "wikilink")
[Category:不列顛哥倫比亞省地理](../Category/不列顛哥倫比亞省地理.md "wikilink")

1.  Brabets, *et al*, 2000 [Environmental and Hydrologic Overview of the
    Yukon River Basin, Alaska and
    Canada](http://pubs.usgs.gov/wri/wri994204/) p56.
2.  Brabets, *et al*, 2000 [Environmental and Hydrologic Overview of the
    Yukon River Basin, Alaska and
    Canada](http://pubs.usgs.gov/wri/wri994204/) p48.
3.  [National Assessment Database | WATERS | US
    EPA](http://iaspub.epa.gov/tmdl_waters10/w305b_report_V6.huc?p_huc=Not%20Reported&p_state=AK)
4.  [USGS Water Quality Samples - 1 sites
    found](http://nwis.waterdata.usgs.gov/ak/nwis/qwdata/?site_no=15565447&agency_cd=USGS)
5.  Scott R. Robinson. 1988. *Movement and Distribution of Western
    Arctic Caribou Herd across the Buckland Valley and Nulato Hills*,
    U.S. Bureau of Land Management Open file Report 23, Alaska
6.  C. Michael Hogan, [*Black Spruce: Picea mariana*,
    GlobalTwitcher.com, ed. Nicklas Stromberg, November,
    2008](http://globaltwitcher.auderis.se/artspec_information.asp?thingid=44751)