**委内瑞拉统一社会主义党**（，缩写为PSUV）是目前[委内瑞拉的執政黨和最大的](../Page/委内瑞拉.md "wikilink")[左翼政党](../Page/左翼.md "wikilink")。该党成立于2007年10月20日，由[委内瑞拉总统](../Page/委内瑞拉总统.md "wikilink")[乌戈·查韦斯在](../Page/乌戈·查韦斯.md "wikilink")2006年委内瑞拉总统选举后提议组建，目的是整合所有支持[玻利瓦尔革命的政党](../Page/玻利瓦尔革命.md "wikilink")，但[委內瑞拉共產黨等政党拒絕加入](../Page/委內瑞拉共產黨.md "wikilink")。

[Category:乌戈·查韦斯](../Category/乌戈·查韦斯.md "wikilink")
[Category:委内瑞拉政党](../Category/委内瑞拉政党.md "wikilink")
[Category:2007年建立的政黨](../Category/2007年建立的政黨.md "wikilink")