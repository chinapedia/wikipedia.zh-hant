[Yau_Tong_Estate_Open_Plaza_201412.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Estate_Open_Plaza_201412.jpg "fig:Yau_Tong_Estate_Open_Plaza_201412.jpg")
[Yau_Tong_Estate_playground.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Estate_playground.jpg "fig:Yau_Tong_Estate_playground.jpg")
**油塘邨**（）是[香港一個](../Page/香港.md "wikilink")[公共屋邨](../Page/香港公共屋邨.md "wikilink")，項目編號為KL34RR，位於[觀塘區](../Page/觀塘區.md "wikilink")[油塘西部](../Page/油塘.md "wikilink")，[港鐵](../Page/港鐵.md "wikilink")[油塘站之上](../Page/油塘站.md "wikilink")，由房屋署總建築師（設計及標準策劃）設計，由[香港房屋委員會發展](../Page/香港房屋委員會.md "wikilink")，並由創毅物業服務顧問有限公司負責屋邨管理。

油塘邨因[地理位置的關係](../Page/地理.md "wikilink")，部分住户可望到[維多利亞港海景](../Page/維多利亞港.md "wikilink")。

## 歷史

油塘邨舊稱**[油塘灣徙置區](../Page/油塘灣.md "wikilink")**，原本興建了23座第三型及第六型[徙置大廈](../Page/徙置大廈.md "wikilink")，分別於1964年至1971年期間落成。

1993年至1999年，原有23座舊式公屋陸續清拆重建。舊油塘邨拆卸後由於機場搬遷使樓宇高度限制放寬，在可建地積比率下，提供更多公屋單位及[地鐵](../Page/香港地鐵.md "wikilink")（現稱[港鐵](../Page/港鐵.md "wikilink")）[將軍澳綫的計劃定綫關係](../Page/將軍澳綫.md "wikilink")，使油塘邨重建工程押後完成。地盤亦丟空多年，以配合港鐵將軍澳綫之興建，在設計上也能把油塘邨重建計劃與[油塘站結合](../Page/油塘站.md "wikilink")，地盡其用，方便居民出入。可是，原油塘邨居民不能原邨安置，而第一期重建（17、19-21、22-26座）的居民被編配至[高怡邨](../Page/高怡邨.md "wikilink")（17、19-21座）、[德田邨及](../Page/德田邨.md "wikilink")[景林邨](../Page/景林邨.md "wikilink")（22-26座）、而第二及三期重建（1-9、11、12、14-16座）的居民則可以透過「自選單位計劃」揀選[翠屏南邨及](../Page/翠屏邨.md "wikilink")[啟田邨單位](../Page/啟田邨.md "wikilink")\[1\]等同區屋邨。

油塘邨第一期拆卸後，地盤空置了約5年時間，等到將軍澳綫定線規劃完成後及動工興建時，油塘邨的重建工程才跟[油塘站工程](../Page/油塘站.md "wikilink")、[高超道邨重建](../Page/高超道邨.md "wikilink")、[東區海底隧道建屋計劃](../Page/東區海底隧道.md "wikilink")（現稱[油麗邨](../Page/油麗邨.md "wikilink")）及[鯉魚門邨幾個在油塘區的大型發展計劃一併展開](../Page/鯉魚門邨.md "wikilink")，發揮協同效應，也減低對附近居民的影響，建成後則以有蓋行人道將幾個屋邨／屋苑跟油塘站及各商場連成一體，方便市民出入；而在鐵路隧道上方及附近的公屋及商場地基則由房委會委托前地鐵公司負責興建，以配合地鐵將軍澳綫的工序、通車時間及確保上蓋建築工程不會對鐵路保護區造成不良影響；而原在油塘的普照書院及聖安當小學／幼稚園，已遷往前地鐵公司在油塘邨所興建的新校舍繼續上課。

重建後的油塘邨共設五座（其中一座原屬[油美苑](../Page/油美苑.md "wikilink")，因停售居屋而撥作公屋出租），分別於2000年至2002年期間落成。

[民政署署長](../Page/民政署.md "wikilink")[陳甘美華](../Page/陳甘美華.md "wikilink")2012年11月5日為[油塘社區會堂主持開幕典禮](../Page/大本型.md "wikilink")。

## 屋邨工程

該邨於2011年年底進行外牆檢查及維修工程。於2012年中旬進行外牆油色工程。於2013年6月在富塘樓與貴塘樓對出空地進行優化計劃。於2015年年度進行全方位維修計劃。於2016年2月6日開始由中標的非牟利機構營運學生[自修室](../Page/自修室.md "wikilink")。於2016年年底為富塘樓、貴塘樓、榮塘樓、華塘樓各單位進行更換晾衣架計劃。

## 屋邨資料

### 現時樓宇

| 樓宇名稱（座號） | 樓宇類型                                 | 落成年份 |
| -------- | ------------------------------------ | ---- |
| 富塘樓（第1座） | [和諧一型](../Page/和諧式大廈.md "wikilink")  | 2000 |
| 貴塘樓（第2座） |                                      |      |
| 榮塘樓（第3座） | 2002                                 |      |
| 華塘樓（第4座） |                                      |      |
| 美塘樓（第5座） | [新十字型](../Page/新十字型大廈.md "wikilink") |      |
|          |                                      |      |

為增加非長者一人單位供應，而三睡房單位需求持續下降，[房委會將](../Page/房委會.md "wikilink")10個[和諧式及新和諧式設計項目的當中過剩三睡房單位改作小型單位](../Page/和諧式大廈.md "wikilink")，而油塘邨華塘樓便是其中之一。而房委會就把華塘樓的翼尾3B單位分拆作1個1/2P單位及1個2/3P單位。所以華塘樓與油塘邨其餘三幢採用和諧式設計的樓宇單位數目不同。（採用和諧式設計的富塘樓、貴塘樓、榮塘樓每層有20個單位，華塘樓則有24個單位，而採用[新十字型的美塘樓就只有](../Page/新十字型.md "wikilink")10個單位）

油塘邨美塘樓原為[居屋](../Page/居屋.md "wikilink")[油美苑A座澄美閣](../Page/油美苑及油翠苑.md "wikilink")，因停售居屋關係把該座轉為公屋出租。由於該大廈原先為居屋，每層單位數目僅10個，而且用料較普通公屋優質（與同一地段上維持作居屋出售的[油翠苑B至E座相同](../Page/油翠苑.md "wikilink")），更設有廚櫃、[開利或](../Page/開利.md "wikilink")[National冷氣機等](../Page/National.md "wikilink")，所以單位租金比其他公屋單位稍貴。

### 樓宇設施

**富塘樓** :

  - 油塘邨屋邨辦事處
  - 房屋署東九龍(二)區租約事務管理處
  - 謝淑珍議員辦事處
  - 富塘樓互助委員會

**貴塘樓** :

  - 油塘區街坊褔利會
  - 貴塘樓互助委員會

**榮塘樓** :

  - 油塘郵政局
  - 呂東孩議員辦事處
  - 榮塘樓互助委員會

**華塘樓**:

  - 黃國健、何啟明議員辦事處
  - 華塘樓互助委員會

**美塘樓**:

  - 美塘樓互助委員會

### 歷代樓宇

| 重建前的油塘邨 |
| ------- |
| 樓宇座號    |
| 第1座     |
| 第2座     |
| 第3座     |
| 第4座     |
| 第5座     |
| 第6座     |
| 第7座     |
| 第8座     |
| 第9座     |
| 第11座    |
| 第12座    |
| 第14座    |
| 第15座    |
| 第16座    |
| 第17座    |
| 第19座    |
| 第20座    |
| 第21座    |
| 第22座    |
| 第23座    |
| 第24座    |
| 第25座    |
| 第26座    |
|         |

## 教育設施

### 幼稚園

  - [聖安當幼稚園](https://web.archive.org/web/20160709123912/http://www.stakg.edu.hk/Home.htm)（1959年創辦）（位於油塘道1號1樓）
  - [明愛油塘幼兒學校](http://ytns.caritas.org.hk/)（2003年創辦）（位於鯉魚門道60號第2層平台）
  - [基督教中心幼稚園（油塘）](http://www.cyckg.edu.hk/cyckgyt/home.php)（2013年創辦）（位於油塘邨第5期3樓）

### 小學

  - [聖安當小學](http://www.saps.edu.hk/)（1959年創辦）
  - [香港道教聯合會圓玄學院陳呂重德紀念學校](http://www.clcts.edu.hk)（1996年創辦）
  - [福建中學附屬學校](../Page/福建中學附屬學校.md "wikilink")（2009年創辦）

### 中學

  - [天主教普照中學](../Page/天主教普照中學.md "wikilink")（1970年創辦）
  - [聖安當女書院](../Page/聖安當女書院.md "wikilink")（1972年創辦）
  - [佛教何南金中學](http://www.bhnkc.edu.hk)（1988年創辦）

### 特殊學校

  - [基督教中國佈道會聖道學校](http://www.holyword.edu.hk)（1979年創辦）

## 區議會議席

本邨於觀塘區議會屬[油塘西區](../Page/油塘西.md "wikilink")。區議員為[創建力量](../Page/創建力量.md "wikilink")、[九龍社團聯會呂東孩](../Page/九龍社團聯會.md "wikilink")。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")、<font color="{{將軍澳綫色彩}}">█</font>[將軍澳綫](../Page/將軍澳綫.md "wikilink")：[油塘站](../Page/油塘站.md "wikilink")

<!-- end list -->

  - [油塘公共運輸交匯處](../Page/油塘公共運輸交匯處.md "wikilink")

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - 東九龍快綫：油塘至[深圳灣口岸](../Page/深圳灣口岸.md "wikilink")

<!-- end list -->

  - [鯉魚門邨公共運輸交匯處](../Page/鯉魚門邨公共運輸交匯處.md "wikilink")/[欣榮街](../Page/欣榮街.md "wikilink")

<!-- end list -->

  - [公共小巴](../Page/香港小型巴士.md "wikilink")

<!-- end list -->

  - [觀塘至油塘](../Page/觀塘.md "wikilink")/鯉魚門線\[2\]
  - [佐敦道](../Page/佐敦道.md "wikilink")/[土瓜灣至油塘線](../Page/土瓜灣.md "wikilink")\[3\]
  - [荃灣至油塘線](../Page/荃灣.md "wikilink") (上下午繁時服務)\[4\]
  - [旺角至油塘線](../Page/旺角.md "wikilink")\[5\]

<!-- end list -->

  - [鯉魚門道](../Page/鯉魚門道.md "wikilink")/[高超道](../Page/高超道.md "wikilink")

<!-- end list -->

  - [公共小巴](../Page/香港小型巴士.md "wikilink")

<!-- end list -->

  - [觀塘至油塘](../Page/觀塘.md "wikilink")/鯉魚門線\[6\]
  - [佐敦道](../Page/佐敦道.md "wikilink")/[土瓜灣至油塘線](../Page/土瓜灣.md "wikilink")\[7\]
  - [青山道至油塘線](../Page/青山道.md "wikilink")\[8\]
  - [荃灣至油塘線](../Page/荃灣.md "wikilink") (上下午繁時服務)\[9\]
  - [旺角至油塘線](../Page/旺角.md "wikilink")\[10\]
  - [灣仔至油塘線](../Page/灣仔.md "wikilink") (通宵線)\[11\]

<!-- end list -->

  - [茶果嶺道](../Page/茶果嶺道.md "wikilink")

<!-- end list -->

  - 鄰近[鯉魚門](../Page/鯉魚門.md "wikilink")

<!-- end list -->

  - 三家村碼頭(設有渡輪服務前往西灣河及東龍島)

<!-- end list -->

  - 步行

<!-- end list -->

  - [油麗邨至油塘邨升降機塔](../Page/油麗邨.md "wikilink")
  - [大本型購物廊](../Page/大本型.md "wikilink")

</div>

</div>

## 參考資料

## 外部連結

  - [房委會油塘邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2789)
  - [觀塘區巴士路線](http://www.681busterminal.com/klnkt.html)
  - [香港地方－ 討論文庫 － 油塘村及麗港城](http://www.hk-place.com/db.php?post=d002019)

{{-}}

[Category:油塘](../Category/油塘.md "wikilink")
[Category:2002年完工建築物](../Category/2002年完工建築物.md "wikilink")
[Category:2000年完工建築物](../Category/2000年完工建築物.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [再見油塘邨](http://www.youtube.com/watch?v=tq-Y6K47Tf0)，Youtube
2.  [觀塘協和街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k14.html)
3.  [佐敦道吳松街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k19.html)
4.  [荃灣荃灣街市街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_kn14.html)
5.  [旺角先達廣場　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k40.html)
6.  [觀塘協和街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k14.html)
7.  [佐敦道吳松街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k19.html)
8.  [青山道香港紗廠　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_k38.html)
9.  [荃灣荃灣街市街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_kn14.html)
10. [旺角先達廣場　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k40.html)
11. [灣仔　—　藍田及油塘](http://www.16seats.net/chi/rmb/r_kh63.html)