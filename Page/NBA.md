**國家籃球協會**（，[缩写](../Page/缩写.md "wikilink")：）是[北美的男子](../Page/北美.md "wikilink")[職業](../Page/職業運動聯盟.md "wikilink")[籃球組織](../Page/籃球.md "wikilink")，拥有30支球队，分属两个分區（Conference）：[东部联盟和](../Page/东部联盟_\(NBA\).md "wikilink")[西部联盟](../Page/西部联盟_\(NBA\).md "wikilink")；而每个分區各由三个赛区（Division）组成，每个赛区有五支球队。所屬球隊中除了一支来自[加拿大的](../Page/加拿大.md "wikilink")[多伦多速龍之外](../Page/多伦多速龍.md "wikilink")，其餘均位於[美國](../Page/美國.md "wikilink")。其赛事**美国职业篮球联赛**（或称**美国篮球联赛**，简称**美職籃**）也被直接称为**NBA**。

NBA正式赛季於每年10月中开始，分为[常規賽](../Page/NBA常規賽.md "wikilink")、[季後賽兩大部分](../Page/NBA季后赛.md "wikilink")。常规赛为循环赛制，每支球队都要完成82场比赛；常规赛到次年的4月结束，每个联盟的前八名将有资格进入接下来进行的季后赛。季后赛采用七戰四勝赛制，共分四轮；季后赛的最后一轮也称为[总决赛](../Page/NBA总决赛.md "wikilink")，由两个联盟的冠军争夺NBA的最高荣誉──[总冠军](../Page/NBA总冠军.md "wikilink")。整個NBA賽季當中，常規賽完結之後分区冠军不設獎盃，只給予得獎球隊錦旗一個，但联盟冠军及总冠军均設有獎盃加錦旗。

NBA前身是1946年成立的**全美籃球協會**（簡稱**BAA**）\[1\]，1949年改為現名\[2\]。协会总部位于[纽约市](../Page/纽约市.md "wikilink")[第五大道](../Page/第五大道.md "wikilink")645号的奥林匹克塔大厦，现任总裁为[亞當·萧華](../Page/亞當·萧華.md "wikilink")。NBA是[北美四大職業運動之一](../Page/北美四大職業運動.md "wikilink")，又因美國在世界籃球界的領導地位，而被視為全世界水準最高的職業籃球賽事。

## NBA历史

1898年，美國第一個籃球組織**[國家籃球聯盟](../Page/國家籃球聯盟.md "wikilink")**（**NBL**，1898-1904）成立，但當時籃球規則還不完善，組織機構也不健全，經過幾個賽季後，該組織就名存實亡了。

1946年，**[全美籃球協會](../Page/全美籃球協會.md "wikilink")**（**BAA**）成立；1949年，BAA與**[國家籃球聯盟](../Page/國家籃球聯盟.md "wikilink")**（**NBL**）合併，改名“國家籃球協會”（**NBA**）。

### 非白人球员

在成立初期，NBA的球員和教練全部由白人组成。[1947年](../Page/1947年NBA選秀.md "wikilink")，美籍日裔的[三阪亙被](../Page/三阪亙.md "wikilink")[紐約尼克首輪選中](../Page/紐約尼克.md "wikilink")，成為了NBA歷史上首位非白人球員，同時他也是第一个加盟NBA的亞裔球員\[3\]。不過[三阪亙在](../Page/三阪亙.md "wikilink")[1947-48赛季僅僅為纽约尼克斯出場三次貢獻了七分後就被裁掉了](../Page/1947-48_NBA赛季.md "wikilink")。[1950年](../Page/1950年NBA選秀.md "wikilink")，[波士頓塞爾提克在第二輪選中了来自](../Page/波士頓塞爾提克.md "wikilink")[杜肯大学的黑人球員](../Page/杜肯大学.md "wikilink")[查克·庫珀](../Page/查克·庫珀.md "wikilink")（Charles
Cooper），这是NBA歷史上第一位入選的黑人\[4\]，同年一起被選中的还有“糖水”[納特·克利福頓](../Page/納特·克利福頓.md "wikilink")（Nat
Clifton，紐約尼克隊）和[厄爾·勞埃德](../Page/厄爾·勞埃德.md "wikilink")（Earl
Lloyd，[華盛頓首都隊](../Page/華盛頓奇才.md "wikilink")），克利福頓在三人当中最先獲得了球隊合同\[5\]。[1950年](../Page/1950-51_NBA赛季.md "wikilink")10月31日，勞埃德在華盛顿首都隊與[罗徹斯特皇家队的比赛中登場](../Page/罗徹斯特皇家队.md "wikilink")，黑人球员首次踏上了NBA赛场\[6\]。儘管當時美國的種族歧視仍然相當嚴重，不過随着[比爾·羅素](../Page/比爾·羅素.md "wikilink")（Bill
Russell）、[威爾特·張伯倫](../Page/威爾特·張伯倫.md "wikilink")（Wilt
Chamberlain）等人在球场上不斷創造佳績，黑人球员逐漸被球迷们接受並成為了NBA的主角。進入90年代后，黑人球員在數量上開始超越白人球员。[2006-07赛季](../Page/2006-07_NBA赛季.md "wikilink")，黑人球員占NBA所有球员的比率已经達到75%，黑人主教练亦占NBA所有主教練的40%\[7\]。

1966年，比爾·羅素成為NBA首位黑人主教練，他作为球員兼教練帶領波士頓塞爾提克獲得了1968和1969年度的[總冠軍](../Page/NBA總冠軍.md "wikilink")；而NBA亦成為了第一个擁有黑人主教練的職業體育联盟。NBA的首个黑人總經理和球隊老板分别出现在1972年和2002年。

### 標誌

NBA的标志是由一位[纽约平面设计师阿蘭](../Page/纽约.md "wikilink")·西格爾（[Alan
Siegel](../Page/Alan_Siegel.md "wikilink")）设计的，并在1969年首次出现在NBA官方的宣传资料上。不过NBA官方没有正式公布过标志的设计者。标志的图案是一名侧身控球的篮球员的剪影；整个标志由红、白、蓝三种颜色构成，这与[MLB](../Page/MLB.md "wikilink")（美国职业棒球大联盟）的配色一样，西格尔解释说是为了“在两联盟的标志之间营造一种视觉和谐”。很多人认为标志上的人物原型就是前[洛杉磯湖人的明星球員](../Page/洛杉磯湖人.md "wikilink")[傑瑞·威斯特](../Page/傑瑞·威斯特.md "wikilink")（Jerry
West），而西格爾也曾透露这一消息；不过NBA官方一直拒绝承认，并表示标志上的人物形象是一个“谜团”，因为NBA“没有任何关于标志设计者的确切记录”。\[8\]

### 1940年代：BAA以及NBL

1946年，美國東北部及中西部一些大型體育球館的老板在紐約商討成立一個籃球組織的事宜；不過這些球館的老板對籃球並不是十分的感興趣，他們原本就擁有冰球隊伍，因此老板們最初的目的只是為了使他們的球館在冰球休賽期間得到更充分的利用。

1946年6月11日，包括著名的[纽约](../Page/纽约.md "wikilink")[麦迪逊花园广场在内的](../Page/麦迪逊花园广场.md "wikilink")11家球館老板們聯合成立了****（**BAA**，1946-1949）。美国冰球联盟（**AHL**）的主席[莫里斯·波杜夫担任了NBA](../Page/莫里斯·波杜夫.md "wikilink")（BAA）的第一任主席\[9\]\[10\]。

1946年11月1日，NBA（BAA）历史上的第一场正式比赛在[加拿大](../Page/加拿大.md "wikilink")[多伦多的枫叶花园球场举行](../Page/多伦多.md "wikilink")，由[多倫多愛斯基摩人主场迎战](../Page/多倫多愛斯基摩人.md "wikilink")[紐約尼克](../Page/紐約尼克.md "wikilink")，共有7,090名观众湧入球馆观看了这场赛事，结果纽约以68-66取得了胜利\[11\]。[费城勇士获得了第一个NBA](../Page/费城勇士.md "wikilink")（BAA）[总冠军](../Page/NBA总冠军.md "wikilink")。当时BAA的竞争对手有“[美國籃球聯盟](../Page/美國籃球聯盟.md "wikilink")”（**ABL**，1933-1953）、“[國家籃球聯盟](../Page/國家籃球聯盟.md "wikilink")”（**NBL**，1937-1949）等，BAA最先尝试了在大城市的大球馆进行比赛。

1949年8月3日，BAA同意与当时的竞争对手、主要位于中西部的**[國家籃球聯盟](../Page/國家籃球聯盟.md "wikilink")**（**NBL**）合并，组成了一个拥有17支球队的大联盟，并命名为“國家籃球協會”（**NBA**）\[12\]。不过新協會成立的第二個賽季，球隊數量就縮減到11支；隨著小球隊的相繼倒閉，1954年NBA只剩了下8支球隊。這8支球队也一直保留到现在（[亞特蘭大老鷹](../Page/亞特蘭大老鷹.md "wikilink")、[波士頓塞爾提克](../Page/波士頓塞爾提克.md "wikilink")、[底特律活塞](../Page/底特律活塞.md "wikilink")、[金州勇士](../Page/金州勇士.md "wikilink")，[洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")、[紐約尼克](../Page/紐約尼克.md "wikilink")、[費城76人以及](../Page/費城76人.md "wikilink")[沙加緬度國王](../Page/沙加緬度國王.md "wikilink")），当然很多球队的主场已经变迁，为了更好的发展，那些深处偏僻小城的球队都相继搬到了大城市。譬如老鷹隊由三城先后迁往[密尔沃基](../Page/密尔沃基.md "wikilink")、[圣路易斯和](../Page/圣路易斯_\(密苏里州\).md "wikilink")[亞特蘭大](../Page/亞特蘭大.md "wikilink")，活塞隊則從[韋恩堡搬到](../Page/韋恩堡.md "wikilink")[底特律](../Page/底特律.md "wikilink")，湖人由[明尼阿波利斯迁徙到](../Page/明尼阿波利斯.md "wikilink")[洛杉矶](../Page/洛杉矶.md "wikilink")，國王隊从[罗彻斯特先後移至](../Page/羅徹斯特_\(紐約州\).md "wikilink")[辛辛那提和](../Page/辛辛那提.md "wikilink")[沙加緬度](../Page/沙加緬度.md "wikilink")。在[1961-62赛季](../Page/1961-62_NBA赛季.md "wikilink")[芝加哥包装工人队加入联盟之前](../Page/华盛顿奇才.md "wikilink")，NBA一直由这8支球队来维持场面。并且在[1967-68赛季之前](../Page/1967-68_NBA赛季.md "wikilink")，NBA球队数量一直停留在10支以下。

[1953-54赛季](../Page/1953-54_NBA赛季.md "wikilink")，[电视台首次转播NBA赛事](../Page/电视台.md "wikilink")，当时的杜蒙特电视台与NBA签订了一份39,000美元的合同，在当个赛季播出了13场NBA比赛。[1962-63赛季起](../Page/1962-63_NBA赛季.md "wikilink")，[美国广播公司](../Page/美国广播公司.md "wikilink")（ABC）成为了NBA的主要电视合作伙伴，并于[1970年首次向全美观众完整直播了](../Page/1970年NBA总决赛.md "wikilink")[总决赛](../Page/NBA总决赛.md "wikilink")。

### 1950年代：24秒鐘裝置投入使用，[湖人王朝](../Page/湖人.md "wikilink")，[塞爾提克王朝](../Page/波士顿凯尔特人.md "wikilink")

[1950_Minneapolis_Lakers.jpeg](https://zh.wikipedia.org/wiki/File:1950_Minneapolis_Lakers.jpeg "fig:1950_Minneapolis_Lakers.jpeg")的[明尼阿波利斯湖人](../Page/明尼阿波利斯湖人.md "wikilink")。\]\]

为了鼓励进攻，[1954年NBA迎来了第一次历史性的改革](../Page/1954-55_NBA赛季.md "wikilink")[24秒钟装置](../Page/24秒钟装置.md "wikilink")。当时由于比赛中没有进攻时间限制，控球一方往往都不轻易[投篮以免失去进攻机会](../Page/投篮.md "wikilink")，[防守一方唯有采用犯规来夺回控球权](../Page/防守_\(運動\).md "wikilink")，因此比赛基本都是在[罚球线上度过的](../Page/罚球线.md "wikilink")，不仅节奏缓慢，得分也很低。为了改变这种状况，使比赛节奏加快并流畅，条例规定，如果控球一方在24秒内未能把球接触到篮筐将会被判违例并失去控球权。NBA除了制定了24秒进攻时间的新规则外，同时还将球队单节犯规次数限制为6次。改革的效果是明显的，实行24秒新规则的第一个赛季，NBA球队的平均得分达到了93.1分，比前个赛季多了13.6分，[波士頓塞爾提克更成为了首支赛季平均得分过百的球队](../Page/波士頓塞爾提克.md "wikilink")\[13\]。

50年代初，在中锋[喬治·麥肯的带领下](../Page/喬治·麥肯.md "wikilink")，[明尼阿波利斯湖人在](../Page/洛杉磯湖人.md "wikilink")1949到1954六年间五次夺得总冠军，缔造了NBA历史上第一个湖人王朝。[1955-56赛季](../Page/1955-56_NBA赛季.md "wikilink")，[波士頓塞爾提克得到了新秀中锋](../Page/波士頓塞爾提克.md "wikilink")[比爾·羅素](../Page/比爾·羅素.md "wikilink")，在跑轟传奇教练[阿诺·奥尔巴赫的带领下](../Page/阿诺·奥尔巴赫.md "wikilink")，波士頓塞爾提克队拿到了接下来13年中11个总冠军，包括1959到1966年的八连霸。比爾·羅素一生最伟大的对手[威爾特·張伯倫也在](../Page/威爾特·張伯倫.md "wikilink")[1959年加入到NBA](../Page/1959-60_NBA赛季.md "wikilink")，与比爾·羅素疯狂的夺冠记录不同，張伯倫最闪耀的是他的个人数据，包括单场最高分（100分），单场最高篮板（55个）都由他来保持。聯盟生態方面
，例行賽表現東區分組總體比西區分組略強，總冠軍則是五五波。

### 1960年代：ABA的挑战

[Boston_celtics_1960.JPG](https://zh.wikipedia.org/wiki/File:Boston_celtics_1960.JPG "fig:Boston_celtics_1960.JPG")的[波士頓凱爾特人](../Page/波士頓凱爾特人.md "wikilink")。\]\]
1967年，NBA受到了“[美國籃球協會](../Page/美國籃球協會.md "wikilink")”（**ABA**，1967-1976）的强力挑战。[1967年成立的ABA采用非传统的红](../Page/1967年体育.md "wikilink")、白、蓝三色球，设立三分线，使用30秒的进攻时间；这个新兴的职业联盟更强调进攻，以开放性以及自由度来吸引球迷和球星。而ABA的开放性、娱乐性以及自由度是当时NBA无法相比的。由于当时NBA不允许球员未毕业的大学球员直接进入联盟，不少极具天赋的年轻人选择了ABA。NBA则更注重大学里的新星，传奇中锋“天勾”[卡里姆·阿布都-賈霸](../Page/卡里姆·阿布都-賈霸.md "wikilink")（Kareem
Abdul-Jabbar）在[1969年加入联盟](../Page/1969-70_NBA赛季.md "wikilink")。聯盟生態方面則是明顯東強西弱，包括60年代全部的冠軍都由東區包辦。
[Wilt_Chamberlain_Bill_Russell.jpg](https://zh.wikipedia.org/wiki/File:Wilt_Chamberlain_Bill_Russell.jpg "fig:Wilt_Chamberlain_Bill_Russell.jpg")的[比爾·羅素與](../Page/比爾·羅素.md "wikilink")[費城76人的](../Page/費城76人.md "wikilink")[威爾特·張伯倫](../Page/威爾特·張伯倫.md "wikilink")。\]\]

### 1970年代：兼并ABA

尽管“[美國籃球協會](../Page/美國籃球協會.md "wikilink")”（**ABA**）吸引了包括「J博士」[朱利叶斯·欧文这样的天才球星](../Page/朱利叶斯·欧文.md "wikilink")，不过由于得不到电视转播的支持以及本身财政出现问题，ABA于[1976年破产](../Page/1976年体育.md "wikilink")\[14\]\[15\]。

[1976年](../Page/1976-77_NBA赛季.md "wikilink")，**NBA**接纳了4支[ABA球队](../Page/美国篮球协会.md "wikilink")[印第安那溜馬](../Page/印第安那溜馬.md "wikilink")，[圣安东尼奥马刺](../Page/圣安东尼奥马刺.md "wikilink")，[紐約籃網](../Page/紐約籃網.md "wikilink")（後改名[紐澤西籃網](../Page/紐澤西籃網.md "wikilink")、[布魯克林籃網](../Page/布魯克林籃網.md "wikilink")）以及[丹佛火箭](../Page/丹佛金塊.md "wikilink")（由於火箭名稱已為[休士頓使用](../Page/休士頓火箭.md "wikilink")，故改名丹佛金塊）後，联盟球队达到了22支。

ABA的一些规则后来也被NBA接纳和采用：
[1979-80赛季NBA首次设立三分线](../Page/1979-80_NBA赛季.md "wikilink")，目前三分线顶弧距离篮筐23英尺9英吋(7.24米)，边线距离篮筐22英尺(6.71米)。
[1983-84赛季又开始举办最先由ABA开创的](../Page/1983-84_NBA赛季.md "wikilink")[扣篮大赛](../Page/扣篮大赛.md "wikilink")。

### 1980年代：大鸟VS魔术师

這時的西區在經過十幾年的時間，終於慢慢強於東區。

[1979年](../Page/1979年NBA选秀.md "wikilink")，两个未来巨星[賴瑞·柏德](../Page/賴瑞·柏德.md "wikilink")（Larry
Bird）和[魔術強森](../Page/魔術強森.md "wikilink")（Magic
Johnson）分别加盟[波士頓塞爾提克和](../Page/波士頓塞爾提克.md "wikilink")[洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")。

[賴瑞·柏德带领](../Page/賴瑞·柏德.md "wikilink")[波士頓塞爾提克](../Page/波士頓塞爾提克.md "wikilink")5次杀入总决赛3次夺冠，[魔術強森联同](../Page/魔術強森.md "wikilink")[卡里姆·阿布都-賈霸将](../Page/卡里姆·阿布都-賈霸.md "wikilink")[洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")9次送进总决赛并捧得5次奖杯。這時東區因波士頓塞爾提克、費城76人、底特律活塞、密瓦爾基公鹿等強隊，總體實力是略高於包含洛杉磯湖人的西區，他们两个共同缔造**NBA**的80年代辉煌时代。

[1984年](../Page/1984年NBA选秀.md "wikilink")，又一个超级巨星进入联盟，他就是[迈克尔·乔丹](../Page/迈克尔·乔丹.md "wikilink")（Michael
Jordan）。乔丹在90年代两次率领[芝加哥公牛完成](../Page/芝加哥公牛.md "wikilink")3连霸。

自1980年起，隨著[達拉斯小牛](../Page/達拉斯小牛.md "wikilink")、[夏洛特黃蜂](../Page/夏洛特黃蜂.md "wikilink")、[邁阿密熱火](../Page/邁阿密熱火.md "wikilink")、[明尼蘇達灰狼](../Page/明尼蘇達灰狼.md "wikilink")、[奧蘭多魔術等球隊相繼加入](../Page/奧蘭多魔術.md "wikilink")，至[1989-90赛季](../Page/1989-90_NBA赛季.md "wikilink")，**NBA**已經增加到27支球隊。

### 1990年代：全球化，WNBA创立

进入90年代，NBA开始迈向国际化。[1992年巴塞罗那奥运会](../Page/1992年巴塞罗那奥运会.md "wikilink")，[美国篮协首次派出由NBA球星组成的](../Page/美国篮协.md "wikilink")[梦之队参赛](../Page/梦之队.md "wikilink")，包括迈克尔·乔丹、賴瑞·柏德、魔術強森、安德魯·道森等明星。并以平均每场胜出44分的巨大优势获得了冠军\[16\]，这支由NBA顶级球星组成的梦之队让世界认识到了NBA的魅力。

這時雖然東區西區實力雖然各有千秋，不過東區可有由迈克尔·乔丹所領軍的超級強隊公牛，在91-93球季拿下三連霸，誇張的是，喬丹跑去打棒球之後復出，又在96-98這三年拿下三連霸，並創下當時NBA最高的72勝10負，其他東區強隊只能感嘆生不逢時。這紀錄一直到2015-16年的金州勇士隊創下73勝9負，才將之改寫。

为获得更多的商业利益，NBA把部分[季前赛甚至常规赛安排在](../Page/NBA季前赛.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")、[亚太以及](../Page/亚太.md "wikilink")[墨西哥等地举行](../Page/墨西哥.md "wikilink")，这些比赛亦称为[NBA海外赛](../Page/NBA海外赛.md "wikilink")；[1990-91赛季](../Page/1990-91_NBA赛季.md "wikilink")，NBA把[菲尼克斯太阳与](../Page/菲尼克斯太阳.md "wikilink")[犹他爵士的两场常规赛的比赛场地安排在](../Page/犹他爵士.md "wikilink")[日本](../Page/日本.md "wikilink")[东京](../Page/东京.md "wikilink")，这是NBA正式赛事首次在非[美国及](../Page/美国.md "wikilink")[加拿大本地举行](../Page/加拿大.md "wikilink")。同时全世界对NBA的关注随着电视转播而扩大，[2006-07赛季](../Page/2006-07_NBA赛季.md "wikilink")，全球共有215个国家41种语言188家电视台转播NBA赛事或制作NBA相关节目\[17\]。

随着NBA的影响扩大，越来越多的来自世界各地的球星纷纷加入到NBA。近年来，NBA为了吸引更多来自世界各地的优秀球员，将国际球员的限制逐渐放宽；原先国际球员一般都是先进入[NCAA篮球联赛才能参加NBA选秀](../Page/NCAA.md "wikilink")，现在已经可以直接参选了，NBA球探都亲自到各国联赛去挖掘有潜力的新星。很多国际球星在NBA闯出了名堂，例如来自[尼日利亚的中锋](../Page/尼日利亚.md "wikilink")[1994年](../Page/1993-94_NBA赛季.md "wikilink")[MVP](../Page/NBA最有价值球员.md "wikilink")[哈基姆·歐拉朱萬](../Page/哈基姆·歐拉朱萬.md "wikilink")（Hakkem
Olajuwon）带领[休斯顿火箭两次问鼎NBA总冠军](../Page/休斯顿火箭.md "wikilink")，[德国球星](../Page/德国.md "wikilink")[德克·諾威斯基](../Page/德克·諾威斯基.md "wikilink")（Dirk
Nowitzki）把[达拉斯独行侠带进](../Page/达拉斯独行侠.md "wikilink")[2006年NBA总决赛并获得了](../Page/2006年NBA总决赛.md "wikilink")[2007年MVP以及](../Page/2006-07_NBA赛季.md "wikilink")2011年總冠軍及FMVP，还有连续当选[2005年和](../Page/2004-05_NBA赛季.md "wikilink")[2006年MVP的](../Page/2005-06_NBA赛季.md "wikilink")[加拿大控卫](../Page/加拿大.md "wikilink")[史蒂夫·奈許](../Page/史蒂夫·奈許.md "wikilink")（Steve
Nash），四次荣获[最佳防守球员的](../Page/NBA最佳防守球员.md "wikilink")[薩伊中锋](../Page/薩伊.md "wikilink")[迪肯貝·穆湯波](../Page/迪肯貝·穆湯波.md "wikilink")（Dikembe
Mutombo）等等。2006-07赛季NBA的国际球员达到60名，他们来自28个不同的国家\[18\]。

1995年，[多倫多暴龍與](../Page/多倫多暴龍.md "wikilink")[溫哥華灰熊加入NBA](../Page/曼非斯灰熊.md "wikilink")，使得NBA的版圖再次擴展至美國本土之外。

1996年，NBA创办了**[國家女子籃球協會](../Page/國家女子籃球協會.md "wikilink")**（**WNBA**）。

1999年，NBA开办了自己的电视频道**[NBA-TV](../Page/NBA-TV.md "wikilink")**。

2001年，NBA又创立了**[國家籃球協會發展聯盟](../Page/國家籃球協會發展聯盟.md "wikilink")**，即現在的[NBA
G聯盟](../Page/NBA_G聯盟.md "wikilink")（**G-League**）。

### 1990年代末：第一次封館危機

1990年代末也爆发了一场劳资纠纷。1998年由于赛前劳资谈判徹底失败，勞方和資方拒絕任何往來，因此NBA進入全面性封館。在205天後，勞資雙方靠著多方面牽線進行許多密集會議，直到1999年1月25日勞資雙方達成十年收益分配協議，[1998-99赛季推迟到](../Page/1998-99_NBA赛季.md "wikilink")1999年2月5日才开始，当年的常规赛也由82场压缩到50场。[聖安東尼奧馬刺在該年奪下封館後的NBA總冠軍](../Page/聖安東尼奧馬刺.md "wikilink")。

### 第30支球队，新球试用

[2004-05赛季](../Page/2004-05_NBA赛季.md "wikilink")，随着[夏洛特山猫的加入](../Page/夏洛特山猫.md "wikilink")，NBA球队达到了30支。

在千禧年之後，很明顯地變成了西強東弱的局面，舉例來說09-10年的賽季，高達50勝的[奧克拉荷馬城雷霆竟然只能以老八之姿進入季後賽](../Page/奧克拉荷馬城雷霆.md "wikilink")。由三巨頭GDP[馬努·吉諾比利](../Page/馬努·吉諾比利.md "wikilink")（Manu
Ginóbili）、[東尼·帕克](../Page/東尼·帕克.md "wikilink")（Tony
Parker）、[提姆·鄧肯](../Page/提姆·鄧肯.md "wikilink")（Tim
Duncan）的[聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink")，禪師[菲爾·傑克森](../Page/菲爾·傑克森.md "wikilink")、[柯比·布萊恩](../Page/柯比·布萊恩.md "wikilink")、[俠客·歐尼爾的湖人](../Page/俠客·歐尼爾.md "wikilink")，跟德克·諾威斯基所領軍的達拉斯獨行俠，中國狀元[姚明](../Page/姚明.md "wikilink")、[特雷西·麥克格雷迪領軍的](../Page/特雷西·麥克格雷迪.md "wikilink")[休士頓火箭隊則是在近代西區列強中較為人所知](../Page/休士頓火箭隊.md "wikilink")。

2006年6月29日，NBA官方介绍了将在[2006-07赛季投入使用的新球](../Page/2006-07_NBA赛季.md "wikilink")。这也是NBA35年来首次更换比赛用球，在NBA60年历史中也仅仅是第二次。由斯伯丁公司负责设计生产的新球使用新的合成纤维材料，他们称新球可以让球员抓的更紧，更有球感。事实上，很多球员对新球并不好感，抱怨新球干的时候太黏手，沾到汗水却又太滑。于是NBA官方不得不在2007年1月1日重新使用皮革制球。2006年，由於[銳跑被](../Page/銳跑.md "wikilink")[愛迪達所併購](../Page/愛迪達.md "wikilink")，所以NBA的球衣供應商也由原本合作多年的銳跑改換為愛迪達。

### 第二次封館危機：聯盟停擺

由於1999年制訂十年勞資收益分配已到期，即將進行勞資談判。
**2011年7月1日**，NBA執行長[大卫·斯特恩](../Page/大卫·斯特恩.md "wikilink")（David
Stern）再次宣佈NBA進入封館，其原因主要為球團認為球員薪資飆升，使得球團無利可圖，甚至造成聯盟總虧損加倍，雙方在勞資談判過程中意見嚴重分歧，導致無法達成共識，進而導致**NBA聯盟第二次封館的命運**。

**2011年8月10日**，進行第二場勞資會議，但由於資方和勞方又無法達成共識，其主要原因為資方提高球團豪華稅、球員奢侈稅的稅收，球員工會拒絕再提高其他不必要稅收，因此NBA執行長David
Stern再次宣佈取消季前熱身賽兩週，導致第二場會議不歡而散。

**2011年9月20日**，進行第三場勞資會議，有15位NBA現役球員參與這次勞資談判中，球員工會目的在於描述球員主觀和客觀的想法和意見，為爭取球員最大權益，但是不為資方所理解，由於資方對於電視轉播權益金和收益比例無法達成共識，且各球團老闆各有分歧意見，球團工會全體氣憤而退席，NBA執行長大卫·斯特恩再次宣佈取消季前所有熱身賽和例行賽10月\~11月的賽程。

**2011年10月12日**，球員工會召開商業會議，有部份球員提議另組新NBA聯盟和解散球員公會，訴諸法律運動裁決法庭審判。

**2011年10月31日**進行第四場勞資會議，雖然雙方以談定電視轉播權利金和收益比例，但最主要薪資分配始終無法達成協定，資方的理想收益與勞方的理想收益相差甚多，球員工會堅持要佔收益52.5％比例，但是資方堅持佔50.5％比例為底限，最後分歧意見過大，NBA執行長大卫·斯特恩決定取消例行賽一半以上的賽程，球員工會則是決定解散公會，也已經向**美國勞動仲裁機構**提交訴訟。

**2011年11月26日**，經過25天的勞資互不往來。於26日進行第五場勞資會議，再度聚集紐約集會，結果在超過15小時的協商後，雙方終於達成初步協議。NBA執行長大卫·斯特恩表示：「新版勞資協議的產生，對於新球季在12月25日開打感到樂觀」，2011-12年球季各隊約有66場例行賽。

### 聯盟新氣息

**2011年11月30日**，新版勞資協議達成，NBA聯盟正式公開，球員工會確定同意，例行賽於12月25日聖誕節正式開打。

**2016年4月16日**，NBA總裁[亞當·蕭華](../Page/蕭華_\(NBA\).md "wikilink")(Adam
Silver)宣布自2017-18年球季起，聯盟將開放各隊在球衣左上方印上贊助商廣告。這將是北美四大職業運動，首個正規賽球衣廣告創舉。球衣補丁計畫目前仍有些許問題需要解決，例如球星代言衝突，此計畫將進行三個球季，再對球迷反應與實際成效進行檢討。另外，販售球衣部分，球團可自行選擇，有廣告贊助或無廣告標誌商品，賣給球迷。

**2017年9月24日**，自1985年起，每年NBA总冠军会接受总统会见，该传统在2017年被打破\[19\]。2017年冠军金州勇士内部就是否前往白宫产生分歧，而特朗普推特上宣布"由于库里犹豫不决，所以取消邀请金州勇士前往白宫"。特朗普撤销邀请引发大量NBA球员抨击，NBA聯盟總裁席佛也對此表示對此事件感到遺憾。NBA球員工會表達尊重雙方意見，表示對此事件感到惋惜\[20\]。

**2018年8月24日**，NBA官方向各隊球團與球團負責人與球團總經理與球團現任教練團提出一份備忘錄，上面提到聯盟將針對3項規則做出修改，進攻方在搶到進攻籃板後的進攻時間從原本的24秒縮短為14秒、簡化快攻蓄意犯規的規則定義、擴大對惡意行為的定義（包括球員惡意語言謾罵），NBA賽事委員會將會在9月下旬召開會議以針對這3項規則進行投票，只要通過三分之二的門檻，新規則就將於新賽季正式施行，並且先[NBA
G聯盟](../Page/NBA_G聯盟.md "wikilink")（NBA G
League）測試。[NBA球員工會也對NBA聯盟聯盟這三項規則修改提議表示尊重與關切](../Page/NBA球员工会.md "wikilink")。

## 球队

[NBA_Conferences_Divisions_zh-tw.png](https://zh.wikipedia.org/wiki/File:NBA_Conferences_Divisions_zh-tw.png "fig:NBA_Conferences_Divisions_zh-tw.png")

NBA由成立之初的11隊發展到現在的30隊，當中經歷了許多新球隊的建立，老球隊的變遷或是倒閉。70年歷史中[波士頓塞爾提克是成功的球隊](../Page/波士頓塞爾提克.md "wikilink")，他們一共獲得了17個總冠軍，包括1959－1966年的八連冠。[洛杉磯湖人以](../Page/洛杉磯湖人.md "wikilink")16次總冠軍僅次其後。2012年最值錢的球隊是[洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")，市值9億美元。2014年，微軟前執行長鮑默爾將以20億美元（約台幣600億元）天價買下快艇，也寫下NBA球隊轉手價格新紀錄。\[21\]

現時NBA不是以自己身處城市名稱作為球隊名，而是以州份名稱作為球隊名的球隊，為[明尼蘇達灰狼](../Page/明尼蘇達灰狼.md "wikilink")、[猶他爵士](../Page/猶他爵士.md "wikilink")、[印第安納溜馬和](../Page/印第安納溜馬.md "wikilink")[金州勇士](../Page/金州勇士.md "wikilink")(使用了[加利福尼亞州的別名](../Page/加利福尼亞州.md "wikilink")**金州**)。

NBA目前採用的是[2004-05賽季劃分的區域](../Page/2004-05_NBA賽季.md "wikilink")，分為東西2個聯盟，每個聯盟各有3個賽區，每個賽區各有5隊。

<table>
<thead>
<tr class="header">
<th><p>賽區</p></th>
<th><p>球隊</p></th>
<th><p>所在城市(區域)</p></th>
<th><p>球館(主場)</p></th>
<th><p>建立時間</p></th>
<th><p>加入时间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/東部聯盟_(NBA).md" title="wikilink"><span style="color: white;">東區聯盟</a></span></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大西洋賽區.md" title="wikilink">大西洋組</a></p></td>
<td><p>'''<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a> '''<br />
Boston Celtics</p></td>
<td><p><a href="../Page/麻塞諸塞州.md" title="wikilink">麻塞諸塞州</a><a href="../Page/波士頓.md" title="wikilink">波士頓</a><br />
Massachusetts, Boston</p></td>
<td><p><a href="../Page/TD花園.md" title="wikilink">TD花園</a><br />
TD Garden</p></td>
<td><p>1946年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a></strong><br />
Brooklyn Nets</p></td>
<td><p><a href="../Page/紐約州.md" title="wikilink">紐約州</a><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a><br />
State of New York, Brooklyn</p></td>
<td><p><a href="../Page/巴克萊中心.md" title="wikilink">巴克萊中心</a><br />
Barclays Center</p></td>
<td><p>1967年<sup>*</sup></p></td>
<td><p>1976年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></strong><br />
New York Knicks</p></td>
<td><p><a href="../Page/紐約州.md" title="wikilink">紐約州</a><a href="../Page/紐約市.md" title="wikilink">紐約市</a><br />
State of New York, New York</p></td>
<td><p><a href="../Page/麥迪遜廣場花園.md" title="wikilink">麥迪遜廣場花園</a><br />
Madison Square Garden</p></td>
<td><p>1946年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/費城76人.md" title="wikilink">費城76人</a></strong><br />
Philadelphia 76ers</p></td>
<td><p><a href="../Page/賓夕法尼亞州.md" title="wikilink">賓夕法尼亞州</a><a href="../Page/費城.md" title="wikilink">費城</a><br />
Pennsylvania, Philadelphia</p></td>
<td><p><a href="../Page/富國銀行中心.md" title="wikilink">富國銀行中心</a><br />
Wells Fargo Center</p></td>
<td><p>1946年<sup>*</sup></p></td>
<td><p>1949年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>'''<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a> '''<br />
Toronto Raptors</p></td>
<td><p><a href="../Page/安大略省.md" title="wikilink">安大略省</a><a href="../Page/多倫多.md" title="wikilink">多倫多</a><br />
Ontario, Toronto</p></td>
<td><p><a href="../Page/豐業銀行體育館.md" title="wikilink">豐業銀行體育館</a><br />
Scotiabank Arena[22]</p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中部賽區.md" title="wikilink">中央組</a></p></td>
<td><p><strong><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></strong><br />
Chicago Bulls</p></td>
<td><p><a href="../Page/伊利諾斯州.md" title="wikilink">伊利諾斯州</a><a href="../Page/芝加哥.md" title="wikilink">芝加哥</a><br />
Illinois, Chicago</p></td>
<td><p><a href="../Page/联合中心.md" title="wikilink">联合中心</a><br />
United Center</p></td>
<td><p>1966年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></strong><br />
Cleveland Cavaliers</p></td>
<td><p><a href="../Page/俄亥俄州.md" title="wikilink">俄亥俄州</a><a href="../Page/克里夫蘭.md" title="wikilink">克里夫蘭</a><br />
Ohio, Cleveland</p></td>
<td><p><a href="../Page/速貸競技館.md" title="wikilink">速貸競技館</a><br />
Quicken Loans Arena</p></td>
<td><p>1970年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></strong><br />
Detroit Pistons</p></td>
<td><p><a href="../Page/密歇根州.md" title="wikilink">密歇根州</a><a href="../Page/底特律.md" title="wikilink">底特律</a><br />
Michigan, Detroit</p></td>
<td><p><a href="../Page/小凱薩體育館.md" title="wikilink">小凱薩體育館</a><br />
Little Caesars Arena</p></td>
<td><p>1941年<sup>*</sup></p></td>
<td><p>1948年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a></strong><br />
Indiana Pacers</p></td>
<td><p><a href="../Page/印第安那州.md" title="wikilink">印第安那州</a><a href="../Page/印第安那波利斯.md" title="wikilink">印第安那波利斯</a><br />
Indiana, Indianapolis</p></td>
<td><p><a href="../Page/班克斯人壽球館.md" title="wikilink">班克斯人壽球館</a><br />
Bankers Life Fieldhouse</p></td>
<td><p>1967年</p></td>
<td><p>1976年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></strong><br />
Milwaukee Bucks</p></td>
<td><p><a href="../Page/威斯康辛州.md" title="wikilink">威斯康辛州</a><a href="../Page/密爾瓦基.md" title="wikilink">密爾瓦基</a><br />
Wisconsin, Milwaukee</p></td>
<td><p><br />
Fiserv Forum</p></td>
<td><p>1968年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東南賽區.md" title="wikilink">東南組</a></p></td>
<td><p><strong><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></strong><br />
Atlanta Hawks</p></td>
<td><p><a href="../Page/喬治亞州.md" title="wikilink">喬治亞州</a><a href="../Page/亞特蘭大.md" title="wikilink">亞特蘭大</a><br />
Georgia, Atlanta</p></td>
<td><p><a href="../Page/州立農業球館.md" title="wikilink">州立農業球館</a><br />
State Farm Arena[23]</p></td>
<td><p>1946年<sup>*</sup></p></td>
<td><p>1949年</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a></strong><br />
Charlotte Hornets</p></td>
<td><p><a href="../Page/北卡羅萊納州.md" title="wikilink">北卡羅萊納州</a><a href="../Page/夏洛特_(北卡羅來納州).md" title="wikilink">夏洛特</a><br />
North Carolina, Charlotte</p></td>
<td><p><a href="../Page/光譜中心.md" title="wikilink">光譜中心</a><br />
Spectrum Center[24]</p></td>
<td><p>2004年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></strong><br />
Miami Heat</p></td>
<td><p><a href="../Page/佛羅里達州.md" title="wikilink">佛羅里達州</a><a href="../Page/邁阿密.md" title="wikilink">邁阿密</a><br />
Florida, Miami</p></td>
<td><p><a href="../Page/美國航空球場.md" title="wikilink">美國航空球場</a><br />
American Airlines Arena</p></td>
<td><p>1988年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></strong><br />
Orlando Magic</p></td>
<td><p><a href="../Page/佛羅里達州.md" title="wikilink">佛羅里達州</a><a href="../Page/奧蘭多.md" title="wikilink">奧蘭多</a><br />
Florida, Orlando</p></td>
<td><p><a href="../Page/安麗中心.md" title="wikilink">安麗中心</a><br />
Amway Center</p></td>
<td><p>1989年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></strong><br />
Washington Wizards</p></td>
<td><p><a href="../Page/华盛顿哥伦比亚特区.md" title="wikilink">华盛顿哥伦比亚特区</a><br />
Washington, D.C.</p></td>
<td><p><a href="../Page/第一資本競技館.md" title="wikilink">第一資本競技館</a><br />
Capital One Arena[25]</p></td>
<td><p>1961年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><font color=white><a href="../Page/西部聯盟_(NBA).md" title="wikilink">西區聯盟</a></font></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西北賽區.md" title="wikilink">西北組</a></p></td>
<td><p>'''<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a> '''<br />
Denver Nuggets</p></td>
<td><p><a href="../Page/科羅拉多州.md" title="wikilink">科羅拉多州</a><a href="../Page/丹佛.md" title="wikilink">丹佛</a><br />
Colorado, Denver</p></td>
<td><p><a href="../Page/百事中心.md" title="wikilink">百事中心</a><br />
Pepsi Center</p></td>
<td><p>1967年</p></td>
<td><p>1976年</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a></strong><br />
Minnesota Timberwolves</p></td>
<td><p><a href="../Page/明尼蘇達州.md" title="wikilink">明尼蘇達州</a><a href="../Page/明尼阿波利斯.md" title="wikilink">明尼阿波利斯</a><br />
Minnesota, Minneapolis</p></td>
<td><p><a href="../Page/標靶中心.md" title="wikilink">標靶中心</a><br />
Target Center</p></td>
<td><p>1989年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/俄克拉何马城雷霆.md" title="wikilink">俄克拉何马城雷霆</a></strong><br />
Oklahoma City Thunder</p></td>
<td><p><a href="../Page/奧克拉荷馬州.md" title="wikilink">奧克拉荷馬州</a><a href="../Page/奧克拉荷馬市.md" title="wikilink">奧克拉荷馬市</a><br />
Oklahoma, Oklahoma City</p></td>
<td><p><a href="../Page/契薩皮克能源公司球場.md" title="wikilink">契薩皮克能源公司球場</a><br />
Chesapeake Energy Arena[26]</p></td>
<td><p>1974年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></strong><br />
Portland Trail Blazers</p></td>
<td><p><a href="../Page/俄勒岡州.md" title="wikilink">俄勒岡州</a><a href="../Page/波特蘭_(俄勒岡州).md" title="wikilink">波特蘭</a><br />
Oregon, Portland</p></td>
<td><p><a href="../Page/摩達中心.md" title="wikilink">摩達中心</a><br />
Moda Center[27]</p></td>
<td><p>1970年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></strong><br />
Utah Jazz</p></td>
<td><p><a href="../Page/猶他州.md" title="wikilink">猶他州</a><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a><br />
Utah, Salt Lake City</p></td>
<td><p><a href="../Page/生活智能家居球館.md" title="wikilink">生活智能家居球館</a><br />
Vivint Smart Home Arena[28]</p></td>
<td><p>1974年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/太平洋賽區.md" title="wikilink">太平洋組</a></p></td>
<td><p><strong><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></strong><br />
Golden State Warriors</p></td>
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a><a href="../Page/奧克蘭_(加利福尼亞州).md" title="wikilink">奧克蘭</a><br />
California, Oakland</p></td>
<td><p><a href="../Page/甲骨文競技館.md" title="wikilink">甲骨文競技館</a><br />
Oracle Arena</p></td>
<td><p>1946年<sup>*</sup></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></strong><br />
Los Angeles Clippers</p></td>
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a><br />
California, Los Angeles</p></td>
<td><p><a href="../Page/史坦波中心.md" title="wikilink">史坦波中心</a><br />
Staples Center</p></td>
<td><p>1970年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></strong><br />
Los Angeles Lakers</p></td>
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a><br />
California, Los Angeles</p></td>
<td><p><a href="../Page/史坦波中心.md" title="wikilink">史坦波中心</a><br />
Staples Center</p></td>
<td><p>1947年<sup>*</sup></p></td>
<td><p>1948年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></strong><br />
Phoenix Suns</p></td>
<td><p><a href="../Page/亞利桑那州.md" title="wikilink">亞利桑那州</a><a href="../Page/鳳凰城_(亞利桑那州).md" title="wikilink">鳳凰城</a><br />
Arizona, Phoenix</p></td>
<td><p><a href="../Page/托金斯迪克度假酒店球館.md" title="wikilink">托金斯迪克度假酒店球館</a><br />
Talking Stick Resort Arena[29]</p></td>
<td><p>1968年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></strong><br />
Sacramento Kings</p></td>
<td><p><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a><a href="../Page/沙加緬度.md" title="wikilink">沙加緬度</a><br />
California, Sacramento</p></td>
<td><p><a href="../Page/金州第一中心.md" title="wikilink">金州第一中心</a><br />
Golden 1 Center</p></td>
<td><p>1923年<sup>*</sup></p></td>
<td><p>1948年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西南賽區.md" title="wikilink">西南組</a></p></td>
<td><p><strong><a href="../Page/達拉斯獨行俠.md" title="wikilink">達拉斯獨行俠</a></strong><br />
Dallas Mavericks</p></td>
<td><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a><a href="../Page/達拉斯.md" title="wikilink">達拉斯</a><br />
Texas, Dallas</p></td>
<td><p><a href="../Page/美國航空中心.md" title="wikilink">美國航空中心</a><br />
American Airlines Center</p></td>
<td><p>1980年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></strong><br />
Houston Rockets</p></td>
<td><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a><a href="../Page/休士頓.md" title="wikilink">休士頓</a><br />
Texas, Houston</p></td>
<td><p><a href="../Page/豐田中心.md" title="wikilink">豐田中心</a><br />
Toyota Center</p></td>
<td><p>1967年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/曼非斯灰熊.md" title="wikilink">曼非斯灰熊</a></strong><br />
Memphis Grizzlies</p></td>
<td><p><a href="../Page/田納西州.md" title="wikilink">田納西州</a><a href="../Page/孟菲斯_(田納西州).md" title="wikilink">曼非斯</a><br />
Tennessee, Memphis</p></td>
<td><p><a href="../Page/聯邦快遞廣場.md" title="wikilink">聯邦快遞廣場</a><br />
FedExForum</p></td>
<td><p>1995年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/紐奧良鹈鹕.md" title="wikilink">紐奧良鹈鹕</a></strong><br />
New Orleans Pelicans</p></td>
<td><p><a href="../Page/路易斯安那州.md" title="wikilink">路易斯安那州</a><a href="../Page/紐奧良.md" title="wikilink">紐奧良</a><br />
Louisiana, New Orleans</p></td>
<td><p><a href="../Page/冰沙國王中心.md" title="wikilink">冰沙國王中心</a><br />
Smoothie King Center[30]</p></td>
<td><p>1988年<sup>*</sup></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></strong><br />
San Antonio Spurs</p></td>
<td><p><a href="../Page/德克薩斯州.md" title="wikilink">德克薩斯州</a><a href="../Page/聖安東尼奧.md" title="wikilink">聖安東尼奧</a><br />
Texas, San Antonio</p></td>
<td><p><a href="../Page/AT&amp;T中心.md" title="wikilink">AT&amp;T中心</a><br />
AT&amp;T Center[31]</p></td>
<td><p>1967年<sup>*</sup></p></td>
<td><p>1976年</p></td>
<td></td>
</tr>
</tbody>
</table>

### 曾經更名的球隊

  - [底特律活塞](../Page/底特律活塞.md "wikilink")（前韋恩堡活塞），[洛杉磯湖人](../Page/洛杉磯湖人.md "wikilink")（前明尼阿波利斯湖人），[萨克拉门托國王](../Page/萨克拉门托國王.md "wikilink")（前羅徹斯特皇家）1948年由[国家篮球联盟加盟BAA](../Page/国家篮球联盟.md "wikilink")（NBA）。
  - [費城76人](../Page/費城76人.md "wikilink")（前錫拉丘茲民族），[亞特蘭大老鷹](../Page/亞特蘭大老鷹.md "wikilink")（前三城黑鷹）在1949年BAA和NBL合併的時候由NBL併入。
  - [俄克拉何马城雷霆前身原是](../Page/俄克拉何马城雷霆.md "wikilink")[西雅圖超音速](../Page/西雅圖超音速.md "wikilink")，因試圖於西雅圖新建球場被拒後，索性將球隊遷移至奧克拉荷馬市並更名為[俄克拉何马城雷霆](../Page/俄克拉何马城雷霆.md "wikilink")，而NBA總部已和西雅圖官員達成協議，保留超音速隊名及隊徽顏色，希望未來新增或有球隊遷至[西雅圖時](../Page/西雅圖.md "wikilink")，能繼續沿用。
  - [印第安纳步行者](../Page/印第安纳步行者.md "wikilink")、[聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink")、[布魯克林籃網](../Page/布魯克林籃網.md "wikilink")（前紐約/新澤西籃網）和[丹佛掘金](../Page/丹佛掘金.md "wikilink")，在1976年NBA兼併[ABA的時候由ABA併入](../Page/美国篮球协会.md "wikilink")。
  - 由於球隊主場館[新奥尔良競技館受到](../Page/新奥尔良競技館.md "wikilink")[卡崔娜颶風的嚴重破壞](../Page/卡崔娜颶風.md "wikilink")，紐奧良黃蜂（現[新奥尔良鵜鶘](../Page/新奥尔良鵜鶘.md "wikilink")）於[2005-06賽季及](../Page/2005-06_NBA賽季.md "wikilink")[2006-07賽季的主場比賽是安排在](../Page/2006-07_NBA賽季.md "wikilink")[俄克拉荷馬市的](../Page/俄克拉荷馬市.md "wikilink")[福特中心進行](../Page/福特中心.md "wikilink")\[32\]，且暫時更名為「**新奥尔良/俄克拉荷馬市黃蜂**」。
  - [夏洛特山貓在](../Page/夏洛特山貓.md "wikilink")2004年成立，在迈克尔·乔丹買下後，於2013-14賽季後正式改回[夏洛特黃蜂](../Page/夏洛特黃蜂.md "wikilink")。

### 已经解散的球队

## 常规赛和全明星周末

[夏季联赛](../Page/NBA夏季联赛.md "wikilink")（NBA Summer
League）在选秀之后举行，一般在7月份，由[新秀和](../Page/新秀.md "wikilink")3年以下球龄的球员参加，主要为考察新人而设，又称为“拉斯维加斯联赛”。9月末每队会开始夏季训练营，给球员热身，让教练评估球员表现，考察新人，确定赛季轮换12球员和3名备用球员的名单。之后队伍可以安排参加夏季联赛和季前赛。[季前赛](../Page/NBA季前赛.md "wikilink")（Preseason）在每年的10月份举行，为常规赛前最后的热身赛。每支球队参加季前赛的球员名单基本与常规赛相同，不过主力上场的时间很少以避免受伤，[主教练也借此机会来演练不同的阵容](../Page/主教练.md "wikilink")。夏季联赛和季前赛允许非NBA球队参加，而NBA还会把部分季前赛安排在海外进行。
[Celtics_game_versus_the_Timberwolves,_February,_1_2009.jpg](https://zh.wikipedia.org/wiki/File:Celtics_game_versus_the_Timberwolves,_February,_1_2009.jpg "fig:Celtics_game_versus_the_Timberwolves,_February,_1_2009.jpg")與[明尼蘇達灰狼的比赛](../Page/明尼蘇達灰狼.md "wikilink")\]\]
每年10月的最后一周[常规赛](../Page/NBA常规赛.md "wikilink")（Regular
season）正式开始。在常规赛中，每支球队需要打满82场比赛，主客场各占一半，其中与同一赛区的4队要打4场，共16场；与同一个联盟（东部或西部）不同赛区的10队打3或4场，共36场；与不同联盟的球队打两场，共30场；一个赛季每支球队在自己的主场至少与其它29队进行一次交锋。2008年后，NBA是[北美四大体育联盟唯一一个所有球队在常规赛全部碰面的联盟](../Page/北美四大职业运动.md "wikilink")。常规赛的赛程紧凑，球队在每个星期有3到4场比赛，而且每个赛季都有不同程度的“背靠背作赛”（球队连续2天在不同的城市客场作战）、连续客场作赛等艰难赛程。除揭幕战外，圣诞节比赛也是常规赛的焦点赛事；NBA是北美四大体育联盟中唯一在圣诞节安排比赛的，通常是当前最具人气的球星之间的对垒，如[2008-09赛季的圣诞节比赛由](../Page/2008-09_NBA赛季.md "wikilink")[勒布朗·詹姆斯率领的](../Page/勒布朗·詹姆斯.md "wikilink")[克里夫兰骑士对阵](../Page/克里夫兰骑士.md "wikilink")[德怀恩·韦德领衔的](../Page/德怀恩·韦德.md "wikilink")[迈阿密热火](../Page/迈阿密热火.md "wikilink")\[33\]。常规赛还设有每周东（西）部[最有价值球员和每月东](../Page/最有价值球员.md "wikilink")（西）部最有价值球员。

次年2月，常规赛将会暂停1周来举行一年一度（[1999年曾因劳资纠纷停办](../Page/1998-99_NBA赛季.md "wikilink")\[34\]）的[NBA全明星赛](../Page/NBA全明星赛.md "wikilink")（All-Star
Game）。[美国及](../Page/美国.md "wikilink")[加拿大当地球迷对全明星先发阵容进行投票](../Page/加拿大.md "wikilink")，全世界球迷也可以通过[互联网来投票](../Page/互联网.md "wikilink")。东西岸各个位置得票最高的球员将获得首发，其余14名球员由各队教练投票选出，全明星教练则是由全明星赛两周前东西岸成绩最好的球队教练担当（可以多任，但不能连任）。东西岸获胜的一方表现最好的球员将获得[全明星赛MVP](../Page/NBA全明星赛最有价值球员.md "wikilink")。全明星赛前还有一系列的表演及活动来吸引球迷们的眼光，整个全明星周末的活动包括：

  - [新秀挑战赛](../Page/NBA新秀挑战赛.md "wikilink")，比赛的形式不定，历史上大多由应届新秀挑战二年级球员，也有应届新秀按东西部分组对抗的情况，2012年则是由两位NBA名宿[查尔斯·巴克利和](../Page/查尔斯·巴克利.md "wikilink")[奥尼尔在优秀的年轻球员中挑选各自属意的队员进行比赛](../Page/沙奎尔·奥尼尔.md "wikilink")。2015年改为NBA本土新秀对阵NBA外籍新秀。
  - [技巧挑战赛](../Page/NBA技巧赛.md "wikilink")，参赛者必须在最快的时间内完成[运球](../Page/运球.md "wikilink")，[传球](../Page/传球.md "wikilink")，[投篮](../Page/投篮.md "wikilink")，[上篮等一系列动作方能获胜](../Page/上篮.md "wikilink")，2015年改为两人同场对抗。
  - [三分大赛](../Page/NBA三分球大赛.md "wikilink")，规定时间内在5个角度投中最多[三分球者胜出](../Page/三分球.md "wikilink")，2015年调整为任意一个角度可以放5个加分球总得分由30分变成34分
  - [扣篮大赛](../Page/NBA灌篮大赛.md "wikilink")，参赛者尝试做出各种高难度、极具观赏性的[扣篮动作](../Page/扣篮.md "wikilink")，通常邀请历届扣篮大赛冠军作为评判，每人只有3次机会，否则，打出最低分30分
  - [混合投篮赛](../Page/NBA投篮之星赛.md "wikilink")，由一名现役NBA球员、一名现役WNBA球员和一名退役的NBA球星组成队伍，按要求在不同的投篮点投篮，完成最快组胜出
  - [NBA
    H-O-R-S-E大赛](../Page/NBA_H-O-R-S-E大赛.md "wikilink")，第1位参赛者先于球场的任意一个地方投中一球，并由后面的参赛者进行模仿。如不中则会得到一个字母，如拿满H-O-R-S-E者将会被淘汰，最后剩余的一位选手将成为冠军

除此之外，在整个全明星周末期间，每天都有[NBA嘉年华的演出](../Page/NBA嘉年华.md "wikilink")。

全明星周末后很快来到球员交易的最后期限，即赛季的第16个星期四。这一天之后，球队之间不能再交易球员。不过仍可解雇球员，或者跟自由球员签订[十日合同](../Page/十日合同.md "wikilink")\[35\]。

四月份常规赛结束，紧接着进行[季后赛](../Page/NBA季后赛.md "wikilink")。NBA官方会陆续颁发一些奖项来表扬常规赛表现出色的球员，以及评选[最佳阵容](../Page/NBA最佳阵容.md "wikilink")，[最佳防守阵容及](../Page/NBA最佳防守阵容.md "wikilink")[最佳新秀阵容](../Page/NBA最佳新秀阵容.md "wikilink")。

  - [NBA最有价值球员](../Page/NBA最有价值球员.md "wikilink")，年度MVP，所有球员最梦寐以求的奖项。从球员个人能力，对球队的影响以及球队成绩等多方面综合评选
  - [NBA最佳新秀](../Page/NBA最佳新秀.md "wikilink")，年度表现最好的新秀
  - [NBA最佳防守球员](../Page/NBA最佳防守球员.md "wikilink")，年度最佳防守球员，多数由中锋获得
  - [NBA最佳第六人](../Page/NBA最佳第六人.md "wikilink")，年度最佳板凳球员，替补出场次数必需大于首发次数
  - [NBA进步最快球员](../Page/NBA进步最快球员.md "wikilink")，年度数据提升最多的球员
  - [NBA最佳教练](../Page/NBA最佳教练.md "wikilink")，全联盟成绩最好的球队教练不一定能获此殊荣，一般由善于挖掘球队潜力，带领球队取得意想不到成绩的教练获得
  - [NBA最佳经理](../Page/NBA最佳经理.md "wikilink")，由体育新闻杂志（Sporting
    News）颁发的非官方奖项，不过已得到广泛认可
  - [NBA体育精神奖](../Page/NBA体育精神奖.md "wikilink")，表扬公平竞赛的球员
  - [沃特·肯尼迪公民奖](../Page/沃特·肯尼迪公民奖.md "wikilink")，表扬热心慈善公益活动的球员，由[职业篮球作家协会颁发](../Page/职业篮球作家协会.md "wikilink")

## 季后赛

### NBA总冠军

| 队伍                                                                         | 夺冠次数 | 夺冠年份                                                                  |
| -------------------------------------------------------------------------- | ---- | --------------------------------------------------------------------- |
| [波士顿凯尔特人](../Page/波士顿凯尔特人.md "wikilink")                                   | 17   | 1957、1959-1966、1968、1969、 1974、1976、1981、1984、1986、2008               |
| [明尼阿波利斯/洛杉矶湖人](../Page/洛杉磯湖人.md "wikilink")                                | 16   | 1949、1950、1952-1954、1972、1980、1982、1985、1987、1988、2000-2002、2009、2010 |
| [芝加哥公牛](../Page/芝加哥公牛.md "wikilink")                                       | 6    | 1991-1993、1996-1998                                                   |
| [费城/舊金山/金州勇士](../Page/金州勇士.md "wikilink")                                  | 6    | 1947、1956、1975、2015、2017、2018                                         |
| [達拉斯/德克薩斯矮樹叢/圣安东尼奥马刺](../Page/圣安东尼奥马刺.md "wikilink")                       | 5    | 1999、2003、2005、2007、2014                                              |
| [锡拉丘兹民族队/费城76人](../Page/费城76人.md "wikilink")                               | 3    | 1955、1967、1983                                                        |
| [韋恩堡(左納)/底特律活塞](../Page/底特律活塞.md "wikilink")                               | 3    | 1989、1990、2004                                                        |
| [迈阿密热火](../Page/迈阿密热火.md "wikilink")                                       | 3    | 2006、2012、2013                                                        |
| [纽约尼克斯](../Page/纽约尼克斯.md "wikilink")                                       | 2    | 1970、1973                                                             |
| [聖地牙哥/休士頓火箭](../Page/休士頓火箭.md "wikilink")                                  | 2    | 1994、1995                                                             |
| （[巴尔的摩子弹](../Page/巴尔的摩子弹_\(1944年–1954年\).md "wikilink")）（已解散）<sup>\*</sup> | 1    | 1948                                                                  |
| [罗切斯特/辛辛那提皇家队/堪薩斯-奧馬哈/堪薩斯/沙加緬度國王](../Page/沙加緬度國王.md "wikilink")            | 1    | 1951                                                                  |
| [三城黑鹰队/密尔瓦基/圣路易/亚特兰大鹰](../Page/亚特兰大老鹰.md "wikilink")                       | 1    | 1958                                                                  |
| [密爾瓦基公鹿](../Page/密爾瓦基公鹿.md "wikilink")                                     | 1    | 1971                                                                  |
| [波特兰开拓者](../Page/波特兰开拓者.md "wikilink")                                     | 1    | 1977                                                                  |
| [华盛顿子弹/巫師](../Page/华盛顿巫師.md "wikilink")                                    | 1    | 1978                                                                  |
| [西雅图超音速/俄克拉何马城雷霆](../Page/俄克拉何马城雷霆.md "wikilink")                          | 1    | 1979                                                                  |
| [达拉斯小牛](../Page/达拉斯小牛.md "wikilink")                                       | 1    | 2011                                                                  |
| [克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink")                                     | 1    | 2016                                                                  |

  -
    附注：

<!-- end list -->

  - 巴尔的摩子弹队是唯一一支已经解散、获得过NBA总冠军的球队，這裡所指的是1947年成立的巴尔的摩子弹隊，並非現行華盛頓巫師的前身巴尔的摩子弹隊(1963-1973)。
  - 详细冠军数据请参看[NBA总冠军](../Page/NBA总冠军.md "wikilink")

## 劳资关系

NBA球员工会称作**国家篮球球员协会**（**[NBPA](../Page/美國國家籃球協會球員工會.md "wikilink")**），由前[波士顿凯尔特人球星](../Page/波士顿凯尔特人.md "wikilink")[鲍勃·库锡于](../Page/鲍勃·库锡.md "wikilink")1954年建立\[36\]，负责代表球员与球队老板进行谈判，并维护球员们的利益。工会执行主管是，工会现任主席是来自[休斯顿火箭队的](../Page/休斯顿火箭队.md "wikilink")[克里斯·保罗](../Page/克里斯·保罗.md "wikilink")。劳资协议（Collective
Bargaining
Agreement，简称**[CBA](../Page/NBA劳资协议.md "wikilink")**）是NBPA和NBA达成的一个合约，是所有球员合同的基础。

[1998年](../Page/1998年体育.md "wikilink")，由于NBPA拒绝NBA提出的将球员工资由占**[BRI](../Page/BRI.md "wikilink")**的57%降到48%和设立硬[工资帽的提议](../Page/工资帽.md "wikilink")，NBA从7月1日起关闭球馆并停止一切商业活动，直到第二年的1月20日双方达成协议签订了新的CBA。这次长达7个月191天的停摆是NBA历史上最长的一次停止运作，[1998-99赛季常规赛最终被压缩到](../Page/1998-99_NBA赛季.md "wikilink")50场，同时[全明星周末亦停办](../Page/NBA全明星周末.md "wikilink")；球员损失工资达到四亿美元，而球队及NBA联盟损失接近十亿美元\[37\]。

而上一份劳资协议使用期限是[2005-06赛季至](../Page/2005-06_NBA赛季.md "wikilink")[2010-11赛季](../Page/2010-11_NBA赛季.md "wikilink")，2010-11赛季後双方在谈判过程中意见分歧很大，未能达成共识，因此造成停摆，使得[2011-12賽季從](../Page/2011-12_NBA赛季.md "wikilink")82場被壓縮到剩66場例行賽。

最新的一份劳资协议是在2011年签订的，其中對奢侈稅的徵收變得更嚴厲，並採階梯式上升，將在[2014-15賽季實施](../Page/2014-15_NBA赛季.md "wikilink")。

### 球员合同

\[38\]\[39\]

<table>
<caption><strong><a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08赛季工资限制</a></strong>[40]</caption>
<thead>
<tr class="header">
<th><p>NBA<br />
球龄</p></th>
<th><p>最低年薪<br />
（美元）</p></th>
<th><p>最高年薪<br />
（美元）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0</p></td>
<td><p>427,163</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>687,456</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>770,610</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>798,328</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>826,046</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>895,341</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>964,636</p></td>
<td><p>13,041,250</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>1,033,930</p></td>
<td><p>15,649,500</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>1,103,225</p></td>
<td><p>15,649,500</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>1,108,718</p></td>
<td><p>15,649,500</p></td>
</tr>
<tr class="odd">
<td><p>10+</p></td>
<td><p>1,219,590</p></td>
<td><p>18,257,750</p></td>
</tr>
</tbody>
</table>

根据最新的劳资协议，[自由球员与球队签订的合同最长年限为五年](../Page/自由球员.md "wikilink")（使用[拉里·伯德条款为六年](../Page/拉里·伯德条款.md "wikilink")），年薪涨幅最高为8%（使用拉里·伯德条款为10.5%）；年限超过四年（包括四年）的合同经过三个赛季后可以被延长，延长合同的第一年薪金不得高于原合同最后一年的110.5%；合同里可以包含球队选项、球员选项以及球员提前终止合约选项中的其中一项或多项附加条款。劳资协议同时规定了球员工资的上下限。[2007-08赛季在NBA效力十年以上的球员将至少可以得到](../Page/2007-08_NBA赛季.md "wikilink")1,219,590美元的年薪，而首次参加NBA的球员（包括新秀）的最低年薪是427,163美元；底薪每年上涨3.5%。球员可以得到的最高工资随着NBA球龄而不同，少于六年球龄为900万与工资帽的25%中的最大值，七到九年球龄为1100万与工资帽的30%中的最大值，十年以上为1400万与工资帽的35%中的最大值。同时NBA亦规定了球员最高工资不得少于上一年工资的105%，由于薪金涨幅关系，球员可以拿到超过当年最高年薪上限的工资。如[2007-08赛季全联盟薪水最高的](../Page/2007-08_NBA赛季.md "wikilink")[凯文·加内特](../Page/凯文·加内特.md "wikilink")（[波士顿凯尔特人](../Page/波士顿凯尔特人.md "wikilink")）的工资是2200万美元，远远高于最高年薪上限的1838.8万美元\[41\]。

第一轮[新秀的新秀合同期限为四年](../Page/新秀.md "wikilink")（后两年可选择执行或放弃），球员在第二个赛季前可以选择执行第三年的合约或成为自由球员，如果执行了，在第三个赛季前还可以选择执行第四年的合约或成为自由球员。当新秀完成四年合同后，他将成为[受限制自由球员](../Page/受限制自由球员.md "wikilink")。

[十日合同](../Page/十日合同.md "wikilink")（10-day
Contract）是赛季中后段球队为了应付由于伤病或其它原因缺乏人手的问题与自由球员签订的临时合约，期限为十天（或三场比赛，取较大值）。一支球队与一名球员在一个赛季内只能签订两次十日合同，如果第三次签约则必须把期限延长到赛季结束，同时球队的十日合同的数量不得超过伤病名单的人数。十日合同在每年1月5日后才允许签订。

#### 自由球员

自由球员（Free Agency）包括受限制和非受限制的，区别在于，当受限制自由球员（Restricted Free
Agency）与新球队达成协议（新球队对球员只能提供[邀请合同](../Page/邀请合同.md "wikilink")（Offer
Sheet））的时候，如果球员原属球队在15天内开出同等价值的合同，那么将可以留住这名球员；非受限制自由球员则不受原属球队的任何约束。

首轮新秀在完成四年的新秀合同后将自动成为受限制自由球员，而其他自由球员如果在NBA效力的时间不足三年的也是受限制的。不过，没有执行新秀合同第三或第四年选择条款的首轮新秀、或者被球队选中后放弃的新秀将成为非受限制自由球员。

### 球队交易

\[42\]\[43\]

在NBA，[薪金上限以下的球队可以任意进行球员交换](../Page/薪金上限.md "wikilink")，不过鉴于NBA球队的总薪金基本上都在工资帽以上，球队在交易过程中使用最多的就是[交易球员特例](../Page/交易球员特例.md "wikilink")（Traded
Player
Exception），另外还有底薪球员特例和伤残球员特例。按照交易球员特例，球队在[即时交易](../Page/即时交易.md "wikilink")（Simultaneous
Trade）中转入的总年薪不得高于转出总年薪的125%+10万，可以涉及多名球员；而球队在[非即时交易](../Page/非即时交易.md "wikilink")（Non-simultaneous
Trade）中只能送出一名球员（交易筹码甚至可以不是球员），交易出现的合同差额将作为一个“交易补偿”，由转出高合同球员的球队获得，这个“补偿”可以用到同年或下一年的交易中，不过通常是跟选秀权搭配来使用，因为[选秀权在交换过程中薪金价值为零](../Page/选秀权.md "wikilink")。NBA还规定连续两年的首轮选秀权不能同时送出。球员在交易过程中最多可以使用300万现金，但是现金的薪金价值同样为零。

### 球隊薪資上限及豪華税

  -
    參見 : [NBA工资帽](../Page/NBA工资帽#薪資和奢侈稅.md "wikilink")

#### 特殊条款

[特殊条款是针对工资帽而制定的特例](../Page/NBA特殊条款.md "wikilink")。包括有[拉里·伯德条款](../Page/拉里·伯德条款.md "wikilink")、[中产阶级条款](../Page/中产阶级条款.md "wikilink")、[一百万特例](../Page/一百万特例.md "wikilink")、[底薪特例等](../Page/底薪特例.md "wikilink")，总薪金超过薪金上限的球队可以使用这些条款来与自由球员签约。不过球队对这些条款的运用也有限制，如中产阶级条款每个赛季只能对一名球员使用，而一百万特例不能连续两个赛季采用。

## 球员和教练

NBA球员大多数来自美国大学篮球联赛（包括[NCAA](../Page/NCAA.md "wikilink")、[NAIA](../Page/NAIA.md "wikilink")、[NJCAA等](../Page/NJCAA.md "wikilink")），当然也有直接从高中加盟NBA的，不过新的[劳资协议对高中球员作了限制](../Page/NBA劳资协议.md "wikilink")，要求必需在高中毕业一年后才能参加选秀\[44\]。近年来随着NBA在国际市场的不断开拓，参加NBA的国际球员也日益增多；海外球员一般来自各国的篮球联赛，他们可以参加选秀或者直接跟球队签约。球员比例方面，黑人球员是NBA的主导，一直保持在70%以上。
[Phil_Jackson.jpg](https://zh.wikipedia.org/wiki/File:Phil_Jackson.jpg "fig:Phil_Jackson.jpg")在与裁判争论\]\]
NBA球衣及服装由耐克统一赞助，不过球鞋是一个例外——球员可以拥有自己的球鞋赞助商。另外NBA制定有球员服装令（Player Dress
Code）\[45\]，球员在参加联盟及球队的非比赛及训练活动中必须穿“商务休闲装”。球员在比赛场上的不文明行为（辱骂、打人等）都会受到NBA的罚款甚至禁赛处罚；NBA还规定了球员在比赛期间不得冲上看台。

与球员不同，NBA主教练尽管没有受到服装令的约束，但他们在每场比赛都穿上整齐的西服在场边指挥，颇显绅士风度。NBA对主教练的一条重要规定是，在比赛进行过程中主教练不得离开替补席周围28码的范围，或越过球场边线\[46\]，包括和裁判争论的时候，否则就会被判技术犯规。

### NBA选秀

NBA选秀是一年一度的盛事，于每年的6月底在[纽约的](../Page/纽约.md "wikilink")[麦迪逊花园广场举行](../Page/麦迪逊花园广场.md "wikilink")。参加选秀的球员必需年满19岁，美国本土球员还必需满足高中毕业至少一年的要求。（[2006年選秀前可以允許高中生參與選秀](../Page/2006年選秀.md "wikilink")，因此[德怀特·霍华德成爲最後一個高中新秀狀元](../Page/德怀特·霍华德.md "wikilink")）。至2018/19賽季，僅有10名外籍球員獲選狀元，其中包括中國的[姚明](../Page/姚明.md "wikilink")（[卡爾-安東尼·唐斯](../Page/卡爾-安東尼·唐斯.md "wikilink")、[哈金·奧拉祖雲](../Page/哈金·奧拉祖雲.md "wikilink")、[蒂姆·鄧肯](../Page/蒂姆·鄧肯.md "wikilink")、[凯里·歐文均擁有美國籍](../Page/凯里·歐文.md "wikilink")），他和意大利球員[安德里亞·巴格納尼](../Page/安德里亞·巴格納尼.md "wikilink")（2006年）是僅有的兩位在選秀之前沒有經歷過任何美國籃球體系訓練的狀元。

理论上所有球队每年都会分得两个[选秀权](../Page/选秀权.md "wikilink")，但实际上由于选秀权可以用来交换，往往会造成有些球队在某年选秀大会上成为看客。

选秀大会分两轮，每轮30个顺位。第二轮（31～60顺位）是直接按照球队在常规赛的成绩的倒序来排列的。第一轮后十六个顺位（15～30）是进入[季后赛的十六支球队按照常规赛的成绩的倒序来排定](../Page/NBA季后赛.md "wikilink")；1～14顺位(又稱為樂透區)则属于未能进入季后赛的十四支球队，不过并非是直接按照常规赛成绩来排列，而是通过抽签决定顺位的归属。也就是说，没有进入季后赛的十四支球队都有机会获得前三号选秀权，只是成绩较差的球队比较好者赢取的機率大。根据规则，常规赛成绩最差的球队将有25%的几率获得头号选秀权（即所谓的状元签），成绩倒数第二的球队有19.9%的機率，而十四队中成绩最好的球队获取的機率仅有0.5%\[47\]。前三号的选秀权的抽签通常在每年的5月进行。

球队在选中新秀之后可以选择和他签约，亦可以放弃或者直接交换出去，如果新秀遭到放弃，他将成为[自由球员](../Page/自由球员.md "wikilink")（非受限制），如果是交易的话还没获得合同的新秀与选秀权一样是薪金价值为零的。球队与新秀签约后，60天内不能把他交易出去。如果新秀不愿意与选中他的球队签约，而球队又不同意交易或放弃他的话，该新秀在一年内将不能和其它NBA球队签约，挑选他的球队保留有一年的签约权；该新秀还可以参加下一年的选秀。

### 籃球名人堂

NBA球員可以獲得的最高榮譽就是入選[籃球名人堂](../Page/籃球名人堂.md "wikilink")，全稱奈史密斯籃球名人紀念堂（Naismith
Memorial Basketball Hall of
Fame）。不過名人堂並非專門為NBA而設，而是表彰全世界為籃球事業做出卓越貢獻的個人和團隊，包括籃球運動員、籃球教練、籃球裁判、籃球隊伍以及在其它方面為籃球奉獻的人。2007年入選名人堂的NBA名人是曾10次帶領球隊奪得[NBA總冠軍](../Page/NBA總冠軍.md "wikilink")、前任[洛杉磯湖人隊主教練](../Page/洛杉磯湖人隊.md "wikilink")[菲尔·杰克逊](../Page/菲尔·杰克逊.md "wikilink")\[48\]。

[NBA50大巨星是NBA在](../Page/NBA50大巨星.md "wikilink")[1996-97賽季為紀念NBA成立](../Page/1996-97_NBA賽季.md "wikilink")50週年而評選出來的，這50名球員總共拿到了107個總冠軍，49次最有價值球員，17次最佳新秀以及36次得分王，他們一共447次入選全明星賽，得分總和達到923,791分，籃板總數410,327個（評選當年的數據）\[49\]。

除了名人堂和NBA官方的榮譽外，NBA球隊也會為本隊的球星進行[球衣退役儀式](../Page/球衣退役.md "wikilink")，以表揚他們對球隊的貢獻，他們的球衣將會被懸掛在球場頂棚中央。為了表示對這些球星的尊重，退役後的球衣號碼將不能再次使用。不過也有一些特殊的例子：一是球員重穿已退役的球衣號碼，如[芝加哥公牛隊的超級巨星](../Page/芝加哥公牛隊.md "wikilink")-[迈克尔·乔丹在第一次復出的時候](../Page/迈克尔·乔丹.md "wikilink")，由于在比赛中发挥的不好，还被人说45号终究不如23号，于是就不顾联盟的10万美元罚款穿回了已經退役的23號球衣。[洛杉磯湖人隊的](../Page/洛杉磯湖人隊.md "wikilink")[埃尔文·约翰逊曾特地把自己退役的](../Page/埃尔文·约翰逊.md "wikilink")32號球衣拿下來，讓給新加盟的[卡爾·馬龍做展示](../Page/卡爾·馬龍.md "wikilink")，但最後馬龍還是謝絕了。当在太阳队签下自由球员[格兰特·希尔时候](../Page/格兰特·希尔.md "wikilink")，[阿尔万·亚当斯授权允许把自己已经退役的](../Page/阿尔万·亚当斯.md "wikilink")33号球衣暂时给希尔穿上。活塞队中锋-[格雷格·门罗穿的是](../Page/格雷格·门罗.md "wikilink")10号，尽管10号球衣已经被[丹尼斯·罗德曼退役了](../Page/丹尼斯·罗德曼.md "wikilink")，不过由于联盟相关规定，他仍然可以继续身披10号战袍。二是因某種原因而把球衣號碼退役，如[邁阿密熱火就把](../Page/邁阿密熱火.md "wikilink")23號球衣退休，以表揚從未替該隊效力的[迈克尔·乔丹對NBA以至整個籃壇的貢獻](../Page/迈克尔·乔丹.md "wikilink")。

<File:Wang>
ZhiZhi.jpg|NBA首位[中國籍](../Page/中國籍.md "wikilink")、非美籍亞洲球員[王治郅](../Page/王治郅.md "wikilink")
[File:YaoMingonoffense2.jpg|首位入選NBA全明星賽亞洲球員、7次NBA全明星賽及名人堂](File:YaoMingonoffense2.jpg%7C首位入選NBA全明星賽亞洲球員、7次NBA全明星賽及名人堂)[中國籍球員](../Page/中國籍.md "wikilink")[姚明](../Page/姚明.md "wikilink")
<File:Yi> Jianlian Wizards
2.jpg|NBA[中國籍球員](../Page/中國籍.md "wikilink")[易建聯](../Page/易建聯.md "wikilink")
<File:Sun> Yue with the Beijing Aoshen
Olympians.jpg|第二位獲得NBA總冠軍的亞洲球員[中國籍](../Page/中國籍.md "wikilink")[孫悅](../Page/孙悦_\(篮球运动员\).md "wikilink")
<File:Jeremy> Lin with the Knicks and
reporters.jpg|NBA首位[台裔球員](../Page/台裔.md "wikilink")[林书豪](../Page/林书豪.md "wikilink")
[File:ZhouQi2017.png|NBA](File:ZhouQi2017.png%7CNBA)[中國籍球員](../Page/中國籍.md "wikilink")[周琦](../Page/周琦_\(篮球运动员\).md "wikilink")

## NBA领导人

\[50\]

  - **[莫里斯·波杜夫](../Page/莫里斯·波杜夫.md "wikilink")**（1946－63年，主席），NBA（BAA）第一任[主席](../Page/NBA总裁.md "wikilink")。出生于[俄罗斯的波杜夫是一名](../Page/俄罗斯.md "wikilink")[律师](../Page/律师.md "wikilink")，同时也是一名精明的商人和出色的谈判专家。1946年，他与一些[冰球队的老板商议并成立了美国篮球协会](../Page/冰球.md "wikilink")（BAA），被推选为主席；同时他还是当时美国冰球联盟（[AHL](../Page/AHL.md "wikilink")）的主席。1949年，波杜夫成功说服竞争对手[国家篮球联盟](../Page/国家篮球联盟.md "wikilink")（NBL），促成两个篮球组织的合并，建立了新的国家篮球协会（NBA）；1954年，波杜夫推出了一项足以改变篮球历史的措施——[24秒钟装置](../Page/24秒钟装置.md "wikilink")，为NBA后来的成功埋下了坚实的基础。NBA为表扬波杜夫的贡献，把[最有价值球员奖杯命名为](../Page/NBA最有价值球员.md "wikilink")[莫里斯·波杜夫杯](../Page/莫里斯·波杜夫杯.md "wikilink")\[51\]。
  - **[沃尔特·肯尼迪](../Page/沃尔特·肯尼迪.md "wikilink")**（1963－67年，主席，1967－75年，总裁）,1963年上任，是NBA第一位公选的领导人。在成为NBA主席之前肯尼迪还担任过[康涅狄格州](../Page/康涅狄格州.md "wikilink")[斯坦福德的市长](../Page/斯坦福德.md "wikilink")。肯尼迪在任期间得到NBA老板的全力支持，1971年他被赋予极大的权力，成为当时全美最有统治力的职业体育联盟领导人；而他也极力扩充NBA的规模，在他离任的时候NBA球队已经由当初的9支扩大到18支。以他命名的[沃尔特·肯尼迪公民奖是用来表扬热心慈善公益活动的球员或教练](../Page/沃尔特·肯尼迪公民奖.md "wikilink")。
  - **[拉里·奥布赖恩](../Page/拉里·奥布赖恩.md "wikilink")**（1975－84年，总裁），NBA第三任总裁。与其他三位总裁乃至所有体育领导人相比，奥布莱恩拥有极不寻常的背景，他曾担任[美国民主党主席和](../Page/民主党_\(美国\).md "wikilink")[美国邮政局主席](../Page/美国邮政局.md "wikilink")，并长期作为已故[美国总统](../Page/美国总统.md "wikilink")[约翰·肯尼迪的顾问](../Page/约翰·肯尼迪.md "wikilink")。奥布莱恩上任后的第一件大事就是成功兼并[ABA](../Page/美国篮球协会.md "wikilink")（美国篮球协会），解决了由于两联盟的恶性竞争而导致的工资飞涨、票房下降等问题，当中包括非常棘手的“[奥斯卡·罗伯特森球衣](../Page/奥斯卡·罗伯特森球衣.md "wikilink")”事件。1983年，奥布莱恩与NBA球员工会达成的新的[劳资协议](../Page/NBA劳资协议.md "wikilink")，被认为是NBA联盟发展的蓝图和基础；而他对NBA规则的改革的最大贡献在于1979年设立的[三分线](../Page/三分线.md "wikilink")。奥布莱恩离任后不久，NBA把[总冠军奖杯命名为](../Page/NBA总冠军.md "wikilink")[拉里·奥布赖恩冠军奖杯](../Page/拉里·奥布赖恩冠军奖杯.md "wikilink")\[52\]。
  - **[大卫·斯特恩](../Page/大卫·斯特恩.md "wikilink")**（1984－2014年，总裁），1984年上任，是目前在位时间最长的NBA总裁。斯特恩把NBA的商业利益最大化，并成功把NBA推向[全球市场](../Page/全球市场.md "wikilink")，使NBA成为一个具有国际性影响力的体育联盟。
  - **[亞當·席爾瓦](../Page/亞當·席爾瓦.md "wikilink")**（2014年至今，总裁），2014年上任，2014年2月1日斯特恩卸任NBA总裁，席尔瓦将接替这项职务。

## 电视转播

[美国广播公司](../Page/美国广播公司.md "wikilink")（ABC）、[特纳电视网](../Page/特纳电视网.md "wikilink")（TNT）以及[ESPN是现今NBA在](../Page/ESPN.md "wikilink")[美国本土的电视合作伙伴](../Page/美国.md "wikilink")，他们于2002年与NBA签订了为期6年价值46亿美元的合同\[53\]，2007年又将合约以8年74亿美元的价格延长至2016年。2014年10月6日,
NBA宣布与[ESPN](../Page/ESPN.md "wikilink")、[美国广播公司](../Page/美国广播公司.md "wikilink")（ABC）以及[特纳电视网](../Page/特纳电视网.md "wikilink")（TNT）签订价值达到9年240亿美元(27亿/年)的续约合同，新合同从[2016-17赛季开始生效一直延续到](../Page/2016-17_NBA赛季.md "wikilink")2024-25赛季，\[54\]这也使得NBA拥有世界上第二昂贵的电视转播合同,
仅次于[NFL并同](../Page/國家美式橄欖球聯盟.md "wikilink")[英超新电视转播合同](../Page/英格兰足球超级联赛.md "wikilink")（[2016-17赛季到](../Page/2016年至2017年英格兰足球超级联赛.md "wikilink")2018-19赛季）的年度价格相当。\[55\]

联盟年度日历当中最重要的比赛包括开幕赛，[圣诞节比赛](../Page/NBA圣诞节赛事.md "wikilink")，[NBA全明星周末和](../Page/NBA全明星周末.md "wikilink")[总决赛](../Page/NBA总决赛.md "wikilink")。对于那些全国直播的常规赛，TNT安排在周四晚上，ESPN安排在周三和周五晚上,
ABC的首两场直播会安排在圣诞节,
之后会从一月份开始在周六黄金时段（[2015-16赛季开始](../Page/2015-16_NBA赛季.md "wikilink")）和周日下午进行常规性播出。季后赛所有比赛都会全国直播,
在前两轮ABC通常会安排在周末下午（外加1-2场周六黄金时段），ESPN通常安排在周四和周五，TNT转播剩余的比赛, 另外NBA
TV也有少量直播。TNT和ESPN每年轮流转播东西部决赛，比如2016年东部决赛为ESPN，西部决赛为TNT，而2017年东部决赛变成TNT，西部决赛为ESPN。总决赛全部由ABC直播（ESPN制作）,
国际转播中不会显示ABC, 因为其使用的是NBA TV International的信号。三家电视网播出的NBA相关节目包括赛后节目Inside
the
NBA（TNT，每周四晚播出，[查尔斯·巴克利](../Page/查尔斯·巴克利.md "wikilink")、[肯尼·史密斯担任嘉宾](../Page/肯尼·史密斯.md "wikilink")），赛前节目NBA
Countdown（ABC和ESPN），NBA Wednsday（ESPN）、NBA Friday（ESPN）、NBA Saturday
Primetime（ABC）、NBA Sunday Showcase（ABC）和NBATV的众多节目等。

NBA拥有自己的电视台[NBA TV和](../Page/NBA_TV.md "wikilink")[NBA TV
Canada](../Page/NBA_TV_Canada.md "wikilink"),
另外[加拿大的](../Page/加拿大.md "wikilink")[Sportsnet和](../Page/Sportsnet.md "wikilink")[TSN也拥有NBA常规赛的转播权](../Page/TSN.md "wikilink")，另外在市场外体育套餐方面（Out-of-market
sports package）有NBA League Pass和NBA League Pass International。

## NBA相关的游戏及娱乐

NBA很多球队都拥有自己的[吉祥物](../Page/吉祥物.md "wikilink")，吉祥物一般出自队名或者当地的珍稀动物。每当比赛暂停时间，吉祥物就会出来场地中央表演，表演的花样很多，包括超高难度的花式灌篮（当然有弹簧垫助跳），另外还有美女啦啦队的表演。

[EA公司开发的](../Page/EA.md "wikilink")[NBA
Live以及](../Page/NBA_Live.md "wikilink")[世嘉开发](../Page/世嘉.md "wikilink")（后转为[Take-Two](../Page/Take-Two.md "wikilink")）的[NBA
2K是目前较受欢迎的NBA游戏](../Page/NBA_2K.md "wikilink")，不过NBA
2K直到2K9方才推出[PC版](../Page/个人电脑.md "wikilink"),而到2K12時則推出了手機版。
另外还有[NBA篮球经理](../Page/NBA篮球经理.md "wikilink")、[NBA
Street等](../Page/NBA_Street.md "wikilink")。

## 参考文献

## 外部链接

  - [NBA官方網站](http://www.nba.com/)

  - [NBA台灣官方](http://nba.udn.com/)

  - [NBA中國官方網站](http://china.nba.com/)

  - [NBA-台灣籃球維基館](http://wikibasketball.dils.tku.edu.tw/index.php/NBA)

  - [痞客邦NBA美國職籃](http://nba.pixnet.net/)

  - [虎扑NBA](http://www.hoopchina.com/)

  -
  - [NBA Player's Association](http://www.nbpa.com/)

  - [The National Basketball Referees Association](http://www.nbra.net/)

  - [NBAhoopsonline, Basketball History](http://www.nbahoopsonline.com/)

  - [USA
    Basketball](https://web.archive.org/web/20140205122549/http://www.usabasketball.com/)

## 参见

  - [NBA聖誕節賽事](../Page/NBA聖誕節賽事.md "wikilink")
  - [NBA选秀](../Page/NBA选秀.md "wikilink")
  - [NBA赛季列表](../Page/NBA赛季列表.md "wikilink")
  - [NBA数据统计](../Category/NBA数据统计.md "wikilink")
      - [NBA总得分榜](../Page/NBA总得分榜.md "wikilink")，[NBA总篮板榜](../Page/NBA总篮板榜.md "wikilink")，[NBA总助攻榜](../Page/NBA总助攻榜.md "wikilink")
      - [NBA历届得分王](../Page/NBA历届得分王.md "wikilink")，[NBA历届篮板王](../Page/NBA历届篮板王.md "wikilink")，[NBA历届助攻王](../Page/NBA历届助攻王.md "wikilink")
      - [NBA纪录](../Page/NBA纪录.md "wikilink")
  - [NBA球员工会](../Page/NBA球员工会.md "wikilink")，[NBA裁判协会](../Page/NBA裁判协会.md "wikilink")
  - [NBA劳资协议](../Page/NBA劳资协议.md "wikilink")，[1998年封馆事件](../Page/1998年封馆事件.md "wikilink")
  - [NBA规则](../Page/NBA规则.md "wikilink")
  - [NBA战术](../Page/NBA战术.md "wikilink")
  - [篮球术语](../Category/篮球术语.md "wikilink")，[篮球规则](../Page/篮球规则.md "wikilink")
  - [NBA历史](../Page/NBA历史.md "wikilink")
  - [BAA](../Page/BAA_\(美国\).md "wikilink")（1946-49），[NBL](../Page/国家篮球联盟.md "wikilink")（1937-49），[ABA](../Page/美国篮球协会.md "wikilink")（1967-76）
  - [美国大学篮球联赛](../Page/美国大学篮球联赛.md "wikilink")：[NCAA](../Page/NCAA.md "wikilink")，[NAIA](../Page/NAIA.md "wikilink")，[NJCAA](../Page/NJCAA.md "wikilink")
  - [麦当劳全美高中篮球联赛](../Page/麦当劳全美高中篮球联赛.md "wikilink")
  - [NBA全球](../Page/NBA全球.md "wikilink")
  - [NBA电视转播](../Page/NBA电视转播.md "wikilink")：
      - [NBA on CBS](../Page/NBA_on_CBS.md "wikilink")
      - [NBA on NBC](../Page/NBA_on_NBC.md "wikilink")
      - [NBA on ABC](../Page/NBA_on_ABC.md "wikilink")
      - [NBA on TNT](../Page/NBA_on_TNT.md "wikilink")
      - [NBA on ESPN](../Page/NBA_on_ESPN.md "wikilink")
      - [NBA TV](../Page/NBA_TV.md "wikilink")
  - [NBA尼尔森收视调查](../Page/NBA尼尔森收视调查.md "wikilink")
  - [NBA Live](../Page/NBA_Live.md "wikilink")、[NBA
    2K](../Page/NBA_2K.md "wikilink")

{{-}}

[NBA](../Category/NBA.md "wikilink")
[Category:美國職業運動聯盟](../Category/美國職業運動聯盟.md "wikilink")
[Category:加拿大職業運動聯盟](../Category/加拿大職業運動聯盟.md "wikilink")
[1](../Category/美国篮球赛事.md "wikilink")
[Category:加拿大籃球聯賽](../Category/加拿大籃球聯賽.md "wikilink")
[Category:1946年建立的體育聯賽](../Category/1946年建立的體育聯賽.md "wikilink")

1.  [The First
    Game](http://www.nba.com/history/firstgame_feature.html)--NBA.com

2.  [NBL](http://hoopedia.nba.com/index.php/NBL_%28USA%29) --Hoopedia

3.  ["Wat" A
    Player](http://www.nba.com/features/global_misaka_010417.html)--NBA.com

4.  <http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=7095656>

5.  <http://www.espnstar.com.cn/pub/2007/0223/19275.htm> --ESPN中文网

6.

7.  <http://sports.tom.com/2007-05-11/0426/20101198.html>
    --TOM鲨威体坛，2007年05月11日

8.  <http://www.cnhubei.com/200602/ca1007591.htm>

9.
10. [Maurice
    Podoloff](http://hoopedia.nba.com/index.php/Maurice_Podoloff)
    --Hoopedia

11.
12.
13. [24-Second Clock Revived the
    Game](http://www.nba.com/history/24secondclock.html)--NBA.com

14. [Remember the ABA](http://www.remembertheaba.com/)

15. [ABA(1967-1976)](http://hoopedia.nba.com/index.php/ABA) --Hoopedia

16. [策划：美国篮球梦系列回顾](http://sports.people.com.cn/GB/31928/58095/58098/4682727.html)--人民网

17. [Global NBA
    Programming](http://www.nba.com/schedules/international_nba_tv_schedule.html)--NBA.com

18. [NBA Players From Around the
    World](http://www.nba.com/players/int_players_0607.html)--NBA.com

19. [NBA总冠军勇士拒绝前往白宫接受特朗普接见
    官方回应：未接受邀请](http://www.guancha.cn/sports/2017_06_14_413163.shtml).观察者.

20. [白宫拒绝接见NBA总冠军
    特朗普又被万人喷](http://sports.ifeng.com/a/20170925/52153360_0.shtml).凤凰体育.

21. [微軟前執行長
    砸600億掌舵快艇](http://mag.udn.com/mag/sports/storypage.jsp?f_ART_ID=529627)--聯合報，2014年8月14日

22. Air Canada Centre

23. Philips Arena

24. Time Warner Cable Arena

25. Verizon Center

26. Ford Center

27. Rose Garden Arena

28. Delta Center, EnergySolutions Arena

29. US Airways Center

30. New Orleans Arena

31. SBC Center

32. <http://www.nba.com/schedules/2005_2006_game_schedule/October.html#scheds>
    &<http://www.nba.com/schedules/> **NOTE**: All Hornets home games
    played at Oklahoma City unless noted

33. <http://sports.163.com/07/0725/20/3K99T4C200051CA1.html>
    --网易体育，2007年7月25日

34.
35.
36. [NBPA History](http://www.nbpa.com/history.php) --NBA Player's
    Association

37.
38. [NBA Salary Cap FAQ](http://members.cox.net/lmcoon/salarycap.htm)

39. [CBA1999](http://www.nbpa.com/downloads/CBA.pdf)

40.
41. [Top 25 players in
    salaries](http://asp.usatoday.com/sports/basketball/nba/salaries/top25.aspx?year=2006-07)--USA
    TODAY Salaries Databases

42.
43.
44.
45. [NBA Player Dress
    Code](http://www.nba.com/news/player_dress_code_051017.html)--NBA.com

46. <http://www.nba.com/analysis/rules_3.html?nav=ArticleList--NBA.com>

47. [NBA选秀规则](http://sports.sina.com.cn/k/2007-04-04/18242848580.shtml)--新浪体育

48. [NBA公佈07籃球名人堂名單
    禪師菲爾-傑克遜領銜](http://sports.sohu.com/20070403/n249150718.shtml)--搜狐體育，2007年4月3日

49. [The NBA
    at 50](http://www.nba.com/history/players/50greatest.html)--NBA.com

50. [The
    Commissioners](http://www.nba.com/history/commissioners.html)--NBA.com

51.
52. [Larry O'Brien](http://hoopedia.nba.com/index.php/Larry_O%27Brien)
    --Hoopedia

53. [NBA同3家电视网签定6年46亿美元的电视转播合同](http://www.people.com.cn/GB/tiyu/20020123/653864.html)--人民网体育在线，2002年1月23日

54.

55.