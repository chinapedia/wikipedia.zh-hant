**東久邇成子**（[大正](../Page/大正.md "wikilink")14年（1925年）12月6日－[昭和](../Page/昭和.md "wikilink")36年（1961年）7月23日）為[昭和天皇第一皇女](../Page/昭和天皇.md "wikilink")，日本原皇族，盛厚王妃，母[香淳皇后](../Page/香淳皇后.md "wikilink")。舊稱**照宮成子[內親王](../Page/內親王.md "wikilink")**，印號為紅梅。

## 生平

大正14年（1925年）12月6日，照宮成子內親王生於[東京市](../Page/東京市.md "wikilink")[迎宾馆赤坂离宫](../Page/迎宾馆赤坂离宫.md "wikilink")，作為昭和天皇和香淳皇后之間的第1個孩子，原本昭和天皇與香淳皇后希望能夠親自撫養；皇子女由父母養育長大，在當時仍是尚未被接受的觀念，因而有意見指出，由天皇養育的皇子女將會變得嬌生慣養不易照顧，成子內親王只好在昭和5年（1930年）與天皇夫婦分居，和妹妹們在吳竹寮內一起成長。

[昭和](../Page/昭和.md "wikilink")18年（1943年）3月於[女子學習院中等科畢業](../Page/女子學習院.md "wikilink")，同年10月12日因與東久邇宮盛厚王結婚而被授予[勳一等](../Page/勳一等.md "wikilink")[寶冠章](../Page/寶冠章.md "wikilink")，次日正式舉行婚禮。昭和20年（1945年）3月10日，在[東京大轟炸時](../Page/東京大轟炸.md "wikilink")，於[防空壕內生下了與盛厚王之間的第](../Page/防空壕.md "wikilink")1個孩子——[東久邇宮信彥王](../Page/東久邇信彥.md "wikilink")。昭和22年（1947年）10月，根據現行的[皇室典範規定](../Page/皇室典範.md "wikilink")，盛厚王及成子、信彥王等三人，還有其他東久邇宮的成員皆失去皇族身分。

成子與盛厚王之間一共有[信彥王](../Page/信彥王.md "wikilink")、[文子女王](../Page/高木文子.md "wikilink")、東久邇秀彥、[東久邇真彥](../Page/東久邇真彥.md "wikilink")、[優子共](../Page/東優子.md "wikilink")5個孩子，[東久邇真彥和](../Page/東久邇真彥.md "wikilink")[優子出生時](../Page/優子.md "wikilink")，她與丈夫已為平民，因此不曾有過皇族頭銜。昭和35年（1960年）11月成子病重，昭和36年（1961年）7月23日，在天皇夫婦、皇太子夫婦、東久邇家族及妹妹鷹司和子的陪伴下，因結腸粘連和腹壁腫瘤於7月23日午前3時15分於宮內廳醫院逝世。享年35歲，因而帶給昭和天皇和香淳皇后很大的打擊。成子被葬於豐島岡墓地，按照香淳皇后的意思，墳墓邊種了許多紅梅。

成子逝世後，其夫東久邇盛厚與[寺尾佳子再婚](../Page/寺尾佳子.md "wikilink")，並生下三男東久邇厚彥（後為寺尾家繼子），後來盛厚在昭和44年（1969年）逝世。成子的次子秀彥後成為[壬生家養子並改名](../Page/壬生家.md "wikilink")[壬生基博](../Page/壬生基博.md "wikilink")；長女文子先後嫁[大村和敏](../Page/大村和敏.md "wikilink")（[大村益次郎後代](../Page/大村益次郎.md "wikilink")）以及[高木代代吉](../Page/高木代代吉.md "wikilink")，而么女優子和[東作興結婚](../Page/東作興.md "wikilink")。\[1\]\[2\][平成](../Page/平成.md "wikilink")13年（2001年）7月23日，即成子死後40年，作為弟弟的[明仁天皇和弟媳的](../Page/明仁天皇.md "wikilink")[美智子皇后在前往祭拜](../Page/美智子.md "wikilink")。

## 參考來源

<references />

[Category:1925年出生](../Category/1925年出生.md "wikilink")
[Category:1961年逝世](../Category/1961年逝世.md "wikilink")
[Category:日本近現代內親王](../Category/日本近現代內親王.md "wikilink")
[Category:久邇宮](../Category/久邇宮.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本親王妃與王妃](../Category/日本親王妃與王妃.md "wikilink")

1.  <http://www.geocities.jp/operaseria_020318/hollyhock/royal/royalty/higashikuni.html>
2.  <http://www.geocities.jp/ahmadjan_aqsaqal/ssr/2s4/se141001.html>