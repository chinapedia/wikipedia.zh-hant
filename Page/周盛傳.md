**周盛傳**（），[字](../Page/表字.md "wikilink")**薪如**，安徽[合肥人](../Page/合肥.md "wikilink")，兄[周盛波](../Page/周盛波.md "wikilink")，皆同平定[太平天国](../Page/太平天国.md "wikilink")[捻軍之](../Page/捻軍.md "wikilink")[淮軍](../Page/淮軍.md "wikilink")、清軍名將。

## 安徽[鄉勇](../Page/鄉勇.md "wikilink")

  - 盛传偕诸兄集丁壮团练。咸丰三年，太平軍扰[合肥](../Page/合肥.md "wikilink")，率百馀人击败之，擒太平軍目[马千禄](../Page/马千禄.md "wikilink")。五年，兄盛华阵亡，盛传与盛波分领团从，防战数有功，奖叙[把总](../Page/把总.md "wikilink")。十一年，赴援[寿州](../Page/寿州.md "wikilink")，擢[千总](../Page/千总.md "wikilink")。
  - [同治元年](../Page/同治.md "wikilink")，盛波从李鸿章援江苏，盛传充亲兵营[哨官](../Page/哨官.md "wikilink")(前鋒)，**从克[嘉定](../Page/嘉定.md "wikilink")**及战[四江口](../Page/四江口.md "wikilink")，累擢游击。二年，回籍增募勇丁，会攻[太仓](../Page/太仓.md "wikilink")，太平軍酋[蔡元隆诈降](../Page/蔡元隆.md "wikilink")，设伏狙击官军，盛传独严备，不为所挫。越数日，偕诸军一鼓克之，驻军[双凤镇](../Page/双凤镇.md "wikilink")，为太平軍所围，连战三昼夜，破之，**克[昆山](../Page/昆山.md "wikilink")**，赐号勋勇[巴图鲁](../Page/巴图鲁.md "wikilink")。攻[江阴](../Page/江阴.md "wikilink")，毁东门太平軍营，城复，擢[参将](../Page/参将.md "wikilink")。迭战[东亭镇](../Page/东亭镇.md "wikilink")、[兴隆桥](../Page/兴隆桥.md "wikilink")、鸭城桥、[西仓](../Page/西仓.md "wikilink")，**遂克[无锡](../Page/无锡.md "wikilink")，功尤多，超擢以[总兵](../Page/总兵.md "wikilink")[记名](../Page/记名.md "wikilink")**。
  - **进攻[常州](../Page/常州.md "wikilink")，三年，进逼郡城南门，太平軍突出拒，盛传且战且筑营，贼屡抄后路，皆击退。登石桥督战，桥断堕水，又受砲伤，绝而复甦。越数日，裹创会攻，攀城先登。克[常州](../Page/常州.md "wikilink")**，[同治皇帝诏以总兵遇缺先行题奏](../Page/同治.md "wikilink")，加[提督衔](../Page/提督.md "wikilink")。**以抚标亲兵三营改为\[传字营\]，盛传始独领一军**，移防[溧阳](../Page/溧阳.md "wikilink")。寻会铭军克[广德州](../Page/广德.md "wikilink")。

## 剿[捻軍](../Page/捻軍.md "wikilink")

  - 四年，调剿捻軍，偕兄盛波援[雉河集](../Page/雉河集.md "wikilink")，自[睢宁](../Page/睢宁.md "wikilink")、[宿州转战而前](../Page/宿州.md "wikilink")。将至，捻酋[任柱以马队突犯](../Page/任柱.md "wikilink")，盛传坚阵不动，出奇兵抄捻軍后，捻軍始卻，会诸军夹击，捻軍溃走，以[提督记名](../Page/提督.md "wikilink")。移防[归德](../Page/归德.md "wikilink")。
  - 五年春，迭败捻軍於考城、[钜野](../Page/钜野.md "wikilink")、城武、[菏泽](../Page/菏泽.md "wikilink")，诏嘉盛传兄弟苦战，同被珍赉。
  - **五月，偕盛波破[牛洛红於](../Page/牛洛红.md "wikilink")[亳州](../Page/亳州.md "wikilink")，洛红被创夜遁，道死**。追捻軍[扶沟](../Page/扶沟.md "wikilink")、[鄢陵](../Page/鄢陵.md "wikilink")、[许州](../Page/许州.md "wikilink")，扼防[周家口](../Page/周家口.md "wikilink")。时以长围困贼，盛传筑[贾鲁河长墙](../Page/贾鲁河.md "wikilink")，檄调为游击之师，解[柘城](../Page/柘城.md "wikilink")、[罗山围](../Page/罗山.md "wikilink")。六年，授广西[右江镇总兵](../Page/右江.md "wikilink")，偕盛波蹙贼信阳[谭家河](../Page/谭家河.md "wikilink")，斩馘逾万。追贼入山东，至江北[海州](../Page/海州.md "wikilink")，捻軍大衰。是年冬，任柱、[赖文光均就歼](../Page/赖文光.md "wikilink")。
  - 七年春，偕盛波渡河会剿[张宗禹](../Page/张宗禹.md "wikilink")，败捻軍於山东、直隶之间，**守[运河长墙](../Page/运河.md "wikilink")。盛传伏炸砲於[吴桥毛家](../Page/吴桥.md "wikilink")，合马步逼贼入伏，砲发，捻軍尸蔽野。既而茌平合围，总愚走死，赐[黄马褂](../Page/黄马褂.md "wikilink")**。盛波乞假养亲，盛传代统全军，从[李鸿章移师湖北](../Page/李鸿章.md "wikilink")。

## 平[陝甘回變](../Page/陝甘回變.md "wikilink")

  - 九年，从鸿章赴[陕西剿回](../Page/陕西.md "wikilink")，回回踞山中，督军进剿，破之於[河兒川](../Page/河兒川.md "wikilink")、[孔岩寨](../Page/孔岩寨.md "wikilink")，分兵於宜、[洛](../Page/洛.md "wikilink")、[鄜](../Page/鄜.md "wikilink")、延之间，以远势兜围，先后擒贼酋[马志龙](../Page/马志龙.md "wikilink")、[戴得胜](../Page/戴得胜.md "wikilink")，北山悉平。

## 修[大沽砲台](../Page/大沽.md "wikilink")

  - 是年秋，鸿章移督直隶，疏调盛传率所部屯卫畿辅。十年，移屯[青县马厂](../Page/青县.md "wikilink")。十二年，兴修大沽北塘砲台，筑内外土城各一，大砲台三，环置小砲七十有一。兵房、药库、仓廒、义塾及城外沟、河、桥、徬悉备，以所部任其役，捐\[盛军\]欠饷以济工费。十三年九月，工竣，诏遇提督缺出先行简放。

## 兴复京畿水利

  - 时鸿章奉敕兴复京畿水利，盛传任津沽屯田事，履勘天津东南纵横百馀里，沮洳芜废，议疏潦、濬河渠，引淡涤咸，以变斥卤。
  - 光绪二年，调[天津镇](../Page/天津.md "wikilink")，移屯兴工，开南运减河，自[靳官屯抵大沽海口](../Page/靳官屯.md "wikilink")，减河两岸各开支河一、横河六，沟氵会河渠悉如法。**建桥徬五十馀处，备蓄泄，使淡水咸水不相渗混，成稻田六万馀亩**。滨河斥卤地沾水利，可垦以亿计。至六年工竣。
  - 八年，擢湖南提督，仍留镇训练士卒，悉用西法，著操枪章程十二篇，军中以为法式。
  - 十年，丁母忧，命改署理，予假回籍治丧。盛传事亲孝，未几，以哀毁伤发卒，[光緒皇帝诏优恤](../Page/光緒.md "wikilink")，谥武壮，建专祠。

## 參考文獻

  - [清史稿](../Page/清史稿.md "wikilink")

[Z周](../Category/右江鎮總兵.md "wikilink")
[Z周](../Category/淮軍人物.md "wikilink")
[Z周](../Category/清朝湖南提督.md "wikilink")
[Z周](../Category/合肥人.md "wikilink")
[S盛](../Category/周姓.md "wikilink")
[Category:諡武壯](../Category/諡武壯.md "wikilink")
[Category:汉字巴图鲁勇号](../Category/汉字巴图鲁勇号.md "wikilink")