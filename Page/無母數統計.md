**無母數統計分析**（），或稱-{zh-hans:**非参数统计学**;
zh-tw:**無母數統計分析**}-，[統計學的分支](../Page/統計學.md "wikilink")，適用於母群體分佈情況未明、小樣本、母群體分佈不為常態也不易轉換為常態。特點在於儘量減少或不修改其建立之模型，較具穩健特性；在樣本數不大時，計算過程較簡單。

無母數統計推論時所使用的統計量的抽樣分配通常與母體分配無關，不必推論其中位數、適合度、獨立性、隨機性，更廣義的說，無母數統計又稱為「不受分配限制統計法」（distribution
free）。無母數統計缺乏一般之機率表。檢定時是以等級（Rank）為主要統計量。

無母數統計在推論統計模型時，常用到的檢定有下列方式：

  -
  - [:en:Anderson-Darling
    test](../Page/:en:Anderson-Darling_test.md "wikilink")

  - [:en:Cochran's Q](../Page/:en:Cochran's_Q.md "wikilink")

  - [:en:Cohen's kappa](../Page/:en:Cohen's_kappa.md "wikilink")

  - [:en:Fisher's exact
    test](../Page/:en:Fisher's_exact_test.md "wikilink")

  - [Friedman two-way analysis of
    variance](../Page/:en:Friedman_test.md "wikilink") by ranks

  - [Kendall's tau](../Page/:en:rank_correlation.md "wikilink")

  - [:en:Kendall's W](../Page/:en:Kendall's_W.md "wikilink")

  - [柯爾莫諾夫-斯米爾諾夫檢驗](../Page/柯爾莫諾夫-斯米爾諾夫檢驗.md "wikilink")

  - [:en:Kruskal-Wallis one-way analysis of
    variance](../Page/:en:Kruskal-Wallis_one-way_analysis_of_variance.md "wikilink")
    by ranks

  - [:en:Kuiper's test](../Page/:en:Kuiper's_test.md "wikilink")

  - [:en:Mann-Whitney U](../Page/:en:Mann-Whitney_U.md "wikilink") or
    Wilcoxon rank sum test（[威尔克科逊检验](../Page/威尔克科逊检验.md "wikilink")）

  - [:en:McNemar's test](../Page/:en:McNemar's_test.md "wikilink") (a
    special case of the chi-squared test)

  - [:en:median test](../Page/:en:median_test.md "wikilink")

  - [重抽样](../Page/重抽样.md "wikilink")

  - [:en:Siegel-Tukey test](../Page/:en:Siegel-Tukey_test.md "wikilink")

  - [斯皮爾曼等級相關係數](../Page/斯皮爾曼等級相關係數.md "wikilink")

  - [Student-Newman-Keuls (SNK)
    test](../Page/:en:Student-Newman-Keuls_test.md "wikilink")

  - [:en:Wald-Wolfowitz runs
    test](../Page/:en:Wald-Wolfowitz_runs_test.md "wikilink")

  - [:en:Wilcoxon signed-rank
    test](../Page/:en:Wilcoxon_signed-rank_test.md "wikilink").

## 缺點

  - 檢定力較弱
  - 處理方式無一致性

## 參考文獻

  - Bagdonavicius, V., Kruopis, J., Nikulin, M.S. (2011).
    "Non-parametric tests for complete data", ISTE & WILEY: London &
    Hoboken. .

  -
  - [Gibbons, Jean Dickinson](../Page/Jean_D._Gibbons.md "wikilink");
    Chakraborti, Subhabrata (2003). *Nonparametric Statistical
    Inference*, 4th Ed. CRC Press. .

  - also .

  - Hollander M., Wolfe D.A., Chicken E. (2014). *Nonparametric
    Statistical Methods*, John Wiley & Sons.

  - Sheskin, David J. (2003) *Handbook of Parametric and Nonparametric
    Statistical Procedures*. CRC Press.

  - Wasserman, Larry (2007). *All of Nonparametric Statistics*,
    Springer. .

[Category: 統計學](../Category/_統計學.md "wikilink")