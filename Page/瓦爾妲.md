**瓦爾達·埃蘭帖瑞** （*Varda
Elentári*），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·鲁埃爾·托爾金的史詩式奇幻小說](../Page/約翰·羅納德·鲁埃爾·托爾金.md "wikilink")《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》中的人物。[曼威之妻](../Page/曼威.md "wikilink")，[邁雅](../Page/邁雅.md "wikilink")[美麗安是她的近親](../Page/美麗安.md "wikilink")。瓦爾達意思是「超群」（
*sublime*）或「崇高」 （*lofty*），埃蘭帖瑞意思是「星辰之后」 （*Star-queen*），她又稱伊爾碧綠絲
（*Elbereth*），姬爾松耐爾 （*Gilthoniel*），「点燃者」 （*The Kindler*），「星辰夫人」 *Lady
of the Stars*，「星辰之后」 （*Queen of the Stars*）和婷托律（
*Tintallë*）。她將繁星設置在天空上。[維拉之一](../Page/維拉.md "wikilink")，[維麗之首](../Page/維麗.md "wikilink")，[雅睿塔爾之一](../Page/雅睿塔爾.md "wikilink")，星辰女神，光之女神。\[1\]

瓦爾達的美麗非筆能形容，[伊露維塔之光在她臉上閃耀](../Page/伊露維塔.md "wikilink")，她在[埃努的大樂章之前就認識](../Page/埃努的大樂章.md "wikilink")[米爾寇](../Page/魔苟斯.md "wikilink")，米爾寇鍾情於她，但瓦爾達一眼看出他的內在，並拒絕了他，因此米爾寇對她又恨又怕。

她與曼威一同居住在[泰尼魁提爾山上](../Page/泰尼魁提爾山.md "wikilink")，用[雙聖樹的露水打造星辰](../Page/雙聖樹.md "wikilink")，如遇到危險劫難時呼喚她的名，她也總是回應他們。在《[魔戒](../Page/魔戒.md "wikilink")》中，瓦爾達曾回應[山姆的要求](../Page/山姆衛斯·詹吉.md "wikilink")，透過[凱蘭崔爾女王給](../Page/凱蘭崔爾.md "wikilink")[佛羅多的](../Page/佛羅多·巴金斯.md "wikilink")[星光寶瓶](../Page/凱蘭崔爾的水晶瓶.md "wikilink")，救山姆脫離[屍羅和](../Page/屍羅_\(魔戒\).md "wikilink")[半獸人魔掌](../Page/半獸人.md "wikilink")\[2\]\[3\]。

## 相關影響

在托爾金作品之後，一顆2003年發現的[小行星被命名為](../Page/小行星.md "wikilink")「[174567瓦爾妲](../Page/小行星174567.md "wikilink")」\[4\]。

## 參見

  - [精靈寶鑽](../Page/精靈寶鑽.md "wikilink")

  - [維拉本紀](../Page/維拉本紀.md "wikilink")

  -
## 參考資料

<references />

[ja:ヴァラ\#ヴァルダ](../Page/ja:ヴァラ#ヴァルダ.md "wikilink")
[la:Ainur\#Valar](../Page/la:Ainur#Valar.md "wikilink")
[pl:Valar\#Varda](../Page/pl:Valar#Varda.md "wikilink") [sv:Valar
(Tolkien)\#Varda](../Page/sv:Valar_\(Tolkien\)#Varda.md "wikilink")

[Category:中土大陸的維拉](../Category/中土大陸的維拉.md "wikilink")
[Category:精灵宝钻中的人物](../Category/精灵宝钻中的人物.md "wikilink")
[Category:虛構女王](../Category/虛構女王.md "wikilink")

1.
2.
3.
4.