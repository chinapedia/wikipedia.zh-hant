**林志華**（），現為[無綫電視監製兼戲劇製作部經理](../Page/無綫電視.md "wikilink")。於1988年加入[無綫電視](../Page/無綫電視.md "wikilink")，入職時擔任助理編導，於2000年獲晉升為監製\[1\]。

## 特色

林志華擅長製作小成本劇集，題材多元化，作品包括以師奶為主的《[師奶兵團](../Page/師奶兵團.md "wikilink")》、《[師奶股神](../Page/師奶股神.md "wikilink")》、《[居家兵團](../Page/居家兵團.md "wikilink")》，家族劇《[掌上明珠](../Page/掌上明珠.md "wikilink")》，推理劇《[隔世追兇](../Page/隔世追兇.md "wikilink")》、《[謎情家族](../Page/謎情家族.md "wikilink")》、《[施公奇案](../Page/施公奇案_\(無綫電視劇集\).md "wikilink")》系列，和警匪劇《[飛虎](../Page/飛虎_\(電視劇\).md "wikilink")》、《[雷霆掃毒](../Page/雷霆掃毒.md "wikilink")》。

常與其合作的演員包括[馬德鐘](../Page/馬德鐘.md "wikilink")、[滕麗名](../Page/滕麗名.md "wikilink")、[王浩信](../Page/王浩信.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[徐子珊](../Page/徐子珊.md "wikilink")、[郭晉安](../Page/郭晉安.md "wikilink")、[張家輝](../Page/張家輝.md "wikilink")、[商天娥](../Page/商天娥.md "wikilink")、[李思捷](../Page/李思捷.md "wikilink")、[林嘉華](../Page/林嘉華.md "wikilink")、[單立文](../Page/單立文.md "wikilink")、[許家傑](../Page/許家傑.md "wikilink")、[朱晨麗](../Page/朱晨麗.md "wikilink")、[朱千雪等](../Page/朱千雪.md "wikilink")。

林志華所監製作品與編審如[李綺華](../Page/李綺華.md "wikilink")、[葉天成](../Page/葉天成.md "wikilink")、[陳金鈴及](../Page/陳金鈴.md "wikilink")[伍立光等合作較多](../Page/伍立光.md "wikilink")。

## 作品列表

### 助理編導

  - 1989年：《[晉文公傳奇](../Page/晉文公傳奇.md "wikilink")》
  - 1990年：《[燃燒歲月](../Page/燃燒歲月.md "wikilink")》《[笑傲在明天](../Page/笑傲在明天.md "wikilink")》
  - 1991年：《[天龍奇俠](../Page/天龍奇俠.md "wikilink")》
  - 1992年：《[他來自天堂](../Page/他來自天堂.md "wikilink")》《[火玫瑰](../Page/火玫瑰.md "wikilink")》《[血璽金刀](../Page/血璽金刀.md "wikilink")》

### 編導

  - 1993年：《[居者冇其屋](../Page/居者冇其屋.md "wikilink")》《[精靈酒店](../Page/精靈酒店.md "wikilink")》
  - 1994年：《[恨鎖金瓶](../Page/恨鎖金瓶.md "wikilink")》
  - 1996年：《[地獄天使](../Page/地獄天使.md "wikilink")》《[新上海灘](../Page/新上海灘.md "wikilink")》
  - 1997年：《[刑事偵緝檔案III](../Page/刑事偵緝檔案III.md "wikilink")》《[大鬧廣昌隆](../Page/大鬧廣昌隆.md "wikilink")》
  - 1998年：《[天地豪情](../Page/天地豪情.md "wikilink")》《[妙手仁心](../Page/妙手仁心.md "wikilink")》
  - 1999年：《[先生貴性](../Page/先生貴性.md "wikilink")》《[刑事偵緝檔案IV](../Page/刑事偵緝檔案IV.md "wikilink")》《[創世紀](../Page/創世紀.md "wikilink")》

### 監製

  - 2001年：《[美味情緣](../Page/美味情緣.md "wikilink")》《[勇探實錄](../Page/勇探實錄.md "wikilink")》
  - 2002年：《[情牽百子櫃](../Page/情牽百子櫃.md "wikilink")》《[鄭板橋](../Page/鄭板橋_\(電視劇\).md "wikilink")》
  - 2003年：《[十萬噸情緣](../Page/十萬噸情緣.md "wikilink")》《[戀愛自由式](../Page/戀愛自由式.md "wikilink")》
  - 2004年：《[隔世追兇](../Page/隔世追兇.md "wikilink")》《[天涯俠醫](../Page/天涯俠醫.md "wikilink")》
  - 2005年：《[情迷黑森林](../Page/情迷黑森林.md "wikilink")》
  - 2006年：《[謎情家族](../Page/謎情家族.md "wikilink")》《[施公奇案](../Page/施公奇案.md "wikilink")》《[愛情全保](../Page/愛情全保.md "wikilink")》
  - 2007年：《[師奶兵團](../Page/師奶兵團.md "wikilink")》《[奸人堅](../Page/奸人堅.md "wikilink")》
  - 2008年：《[師奶股神](../Page/師奶股神.md "wikilink")》《[少年四大名捕](../Page/少年四大名捕_\(無綫電視劇\).md "wikilink")》
  - 2010年：《[掌上明珠](../Page/掌上明珠.md "wikilink")》《[施公奇案II](../Page/施公奇案II.md "wikilink")》《[女人最痛](../Page/女人最痛.md "wikilink")》《[居家兵團](../Page/居家兵團.md "wikilink")》
  - 2012年：《[換樂無窮](../Page/換樂無窮.md "wikilink")》《[飛虎](../Page/飛虎_\(電視劇\).md "wikilink")》《[雷霆掃毒](../Page/雷霆掃毒.md "wikilink")》
  - 2013年：《[熟男有惑](../Page/熟男有惑.md "wikilink")》
  - 2014年：《[再戰明天](../Page/再戰明天.md "wikilink")》《[飛虎II](../Page/飛虎II.md "wikilink")》
  - 2015年：《[以和為貴](../Page/以和為貴.md "wikilink")》
  - 2016年：《[EU超時任務](../Page/EU超時任務.md "wikilink")》《[致命復活](../Page/致命復活.md "wikilink")》
  - 2017年：《[踩過界](../Page/踩過界.md "wikilink")》
  - 2018年：《[兄弟](../Page/兄弟_\(無綫電視劇\).md "wikilink")》
  - 未播映：《[極道怪咖](../Page/極道怪咖.md "wikilink")》

## 常用主要演員

<small>只計算在任監製時期（2010年代），含參與客串</small>

  - 10部：[王浩信](../Page/王浩信.md "wikilink")
    、[許家傑](../Page/許家傑.md "wikilink")
    、[何君誠](../Page/何君誠.md "wikilink")\*
  - 6部：[郭晉安](../Page/郭晉安.md "wikilink")
    、[馬德鐘](../Page/馬德鐘.md "wikilink")\*
    、[楊潮凱](../Page/楊潮凱.md "wikilink")
  - 5部：[單立文](../Page/單立文.md "wikilink")
    、[翟威廉](../Page/翟威廉.md "wikilink")
    、[袁偉豪](../Page/袁偉豪.md "wikilink")
    、[李思捷](../Page/李思捷.md "wikilink")
    、[馬國明](../Page/馬國明.md "wikilink")
  - 4部：[朱晨麗](../Page/朱晨麗.md "wikilink")
    、[朱千雪](../Page/朱千雪.md "wikilink")
    、[苟芸慧](../Page/苟芸慧.md "wikilink")\*
    、[林峰](../Page/林峰_\(香港演員\).md "wikilink")\*
    、[黃宗澤](../Page/黃宗澤.md "wikilink")\*
  - 3部：[陳庭欣](../Page/陳庭欣.md "wikilink")
    、[譚凱琪](../Page/譚凱琪.md "wikilink")
    、[黃德斌](../Page/黃德斌.md "wikilink")\*
    、[徐子珊](../Page/徐子珊.md "wikilink")\*
    、[黃智雯](../Page/黃智雯.md "wikilink")
    、[羅仲謙](../Page/羅仲謙.md "wikilink")
    、[滕麗名](../Page/滕麗名.md "wikilink")
    、[宣萱](../Page/宣萱.md "wikilink")\*
    、[張家輝](../Page/張家輝.md "wikilink")\*
  - 2部：[石修](../Page/石修.md "wikilink")
    、[張國強](../Page/張國強_\(香港演員\).md "wikilink")
    、[江美儀](../Page/江美儀.md "wikilink")
    、[梁烈唯](../Page/梁烈唯.md "wikilink")
    、[李施嬅](../Page/李施嬅.md "wikilink")
    、[湯盈盈](../Page/湯盈盈.md "wikilink")
    、[陳山聰](../Page/陳山聰.md "wikilink")
    、[黃心穎](../Page/黃心穎.md "wikilink")
    、[李佳芯](../Page/李佳芯.md "wikilink")
    、[陳慧珊](../Page/陳慧珊.md "wikilink")\*
    、[胡杏兒](../Page/胡杏兒.md "wikilink")\*
    、[張可頤](../Page/張可頤.md "wikilink")\*
    、[歐陽震華](../Page/歐陽震華.md "wikilink")
    、[吳卓羲](../Page/吳卓羲.md "wikilink")\*
    、[秦煌](../Page/秦煌.md "wikilink")
    、[張武孝](../Page/張武孝.md "wikilink")

<small>\*為已退出TVB</small>

## 參考

## 外部連結

  -
[Category:林姓](../Category/林姓.md "wikilink")
[Category:無綫電視監製](../Category/無綫電視監製.md "wikilink")

1.  [【踩過界】炮製過
    《EU超時任務》林志華監製必屬TVB佳品？](http://bka.mpweekly.com/focus/local/20170621-63888)