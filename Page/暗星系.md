**暗星系**是指一種含很少[恆星](../Page/恆星.md "wikilink")（甚至沒有）的星系級的天體，一般認為它們是由[暗物質構成](../Page/暗物質.md "wikilink")，且這些物質也會如同一般[星系一樣繞著](../Page/星系.md "wikilink")[星系核心旋轉](../Page/星系核.md "wikilink")，而它們可能含有一些氣體（例如[氫](../Page/氫.md "wikilink")），因此它們能藉由無線電波波段來偵測它們的存在。根據觀測結果在宇宙中暗物質佔的比例非常大，因此暗物質由自己的[重力塌縮而形成暗星系也不是不可能](../Page/重力.md "wikilink")。

## 暗星系的候選者

### HE0450-2958

[HE0450-2958是一個不尋常的](../Page/HE0450-2958.md "wikilink")[類星體](../Page/類星體.md "wikilink")，在它周圍沒有發現任何宿主星系，一种说法是HE0450-2958的宿主星系其实是一个暗星系直径至少有50,000
光年。

### HVC 127-41-330

[HVC
127-41-330是一個在](../Page/HVC_127-41-330.md "wikilink")[仙女座星系和](../Page/仙女座星系.md "wikilink")[三角座星系之間的高速運行的星雲](../Page/三角座星系.md "wikilink")。

### 史密斯云

[史密斯云是一個黑暗星系的候選](../Page/史密斯云.md "wikilink")，這是由於其預計質量和能夠在如此接近銀河系下生存。

### VIRGOHI21

[VIRGOHI21是一個在](../Page/VIRGOHI21.md "wikilink")[仙女座的暗星系](../Page/仙女座.md "wikilink")，他是在[2005年2月被發現的](../Page/2005年2月.md "wikilink")。

## 参考文献

  -
  -
  -
  -
[Category:暗物質](../Category/暗物質.md "wikilink")
[暗星系](../Category/暗星系.md "wikilink")
[Category:物理宇宙學](../Category/物理宇宙學.md "wikilink")