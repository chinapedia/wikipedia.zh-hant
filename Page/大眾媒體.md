**大眾媒體**，又稱**大眾傳媒**，是指向[大量受眾廣播之](../Page/大眾傳播.md "wikilink")[傳播媒體](../Page/傳播媒體.md "wikilink")，同時是[第三產業](../Page/第三產業.md "wikilink")、[知識產業和](../Page/知識產業.md "wikilink")[訊息產業](../Page/訊息產業.md "wikilink")。實現交流的技術手段各異：[電台廣播](../Page/電台廣播.md "wikilink")、錄制[音樂](../Page/音樂.md "wikilink")、[電影和](../Page/電影.md "wikilink")[電視等廣播媒體通過電子途徑傳輸信息](../Page/電視.md "wikilink")；[報紙](../Page/報紙.md "wikilink")、[書籍](../Page/書籍.md "wikilink")、宣傳冊和[漫畫等紙質](../Page/漫畫.md "wikilink")[媒體採用實物傳播信息](../Page/媒體.md "wikilink")；\[1\][公告牌](../Page/公告牌.md "wikilink")、標誌和招貼畫等戶外媒體則主要放置在商業建築、體育場館、商店和公交車上。\[2\][公共演說和事件組織也可以被看作是一種形式的大眾媒體](../Page/公共演說.md "wikilink")。\[3\][數碼媒體則包括](../Page/數碼媒體.md "wikilink")[互聯網和](../Page/互聯網.md "wikilink")[流動電話網絡](../Page/流動電話網絡.md "wikilink")。

控制這些技術的組織，例如[電視台或](../Page/電視台.md "wikilink")[出版社](../Page/出版社.md "wikilink")，通常也被稱作是大眾媒體。\[4\]\[5\]

## 定义

。随着数字交流科技在20世纪末21世纪初的快速发展，对于何种形式的媒体可以被归类为大众媒体的问题开始变得突出。例如，对于移动电话、视频游戏和电脑游戏（例如网络游戏）列入其中。2000年代，一种被称作“”的分类开始流行，根据发明时间排序，它们分别是：

1.  [印刷](../Page/印刷.md "wikilink")（包括[圖書](../Page/圖書.md "wikilink")、小冊、[報紙](../Page/報紙.md "wikilink")、[雜誌等](../Page/雜誌.md "wikilink")）：自十五世紀後期開始。
2.  [錄音](../Page/錄音.md "wikilink")（包括[黑膠唱片](../Page/黑膠唱片.md "wikilink")、[磁帶](../Page/磁帶.md "wikilink")、[卡式錄音帶](../Page/卡式錄音帶.md "wikilink")、[CD](../Page/CD.md "wikilink")、[DVD等](../Page/DVD.md "wikilink")）：自十九世紀後期開始。
3.  [電影約從](../Page/電影.md "wikilink")1900年開始。
4.  [電台廣播約從](../Page/電台廣播.md "wikilink")1910年開始。
5.  [電視約從](../Page/電視.md "wikilink")1950年開始。
6.  [網際網路約從](../Page/網際網路.md "wikilink")1990年開始。
7.  [行動電話約從](../Page/行動電話.md "wikilink")2000年開始。

### 特征

根据[剑桥大学约翰](../Page/剑桥大学.md "wikilink")·汤普森的研究，大众媒体共有5大特征：\[6\]

  - “包括生产和传播的技术及制度方法”，这在媒体发展史上反应的很明显，它们都有自己的商业用途。
  - 包含“符号形式的商品化”，材料的生产依赖于其制造和销售大量作品的能力。例如电台依赖其出售给广告商的播出时间，而报纸则依赖于其出售的版面。
  - “隔离开信息的生产制造和接收”。
  - 同制造者相比，它“在时间和空间上的广泛传播” 。
  - “信息分发”——是一种“一对多”的交流模式，产品大量生产并分发给大量受众。

## 形式

### 广播媒体

广播媒体的内容播出顺序被称为档期。[电视和](../Page/电视.md "wikilink")[电台广播节目都是通过广播频道传播的](../Page/电台广播.md "wikilink")。

[有线电视节目通常会和电视](../Page/有线电视.md "wikilink")、电台同时播出，但是受众更窄。

某个播出组织会在同一时间通过不同频道播出多套节目。而同时也会有两个或以上的组织共享一个频道，他们各自在一天内固定的时段使用该频道。

### 电影

电影既可指某一单一动态影像作品，也可以指整个领域。电影是由录制人员创作，使用[摄像机记录的](../Page/摄像机.md "wikilink")，或者是使用[动画技巧或者](../Page/动画.md "wikilink")[特殊效果创作出的](../Page/特殊效果.md "wikilink")。其包含一系列单一的画面，但是当这些画面连续快速播放时，就给观众一种动态影像的感觉。由于[視覺暫留效果](../Page/視覺暫留.md "wikilink")，画面之间的闪烁并不会被察觉。另外造成人们感觉画面是动态的心理效果是[贝塔运动](../Page/贝塔运动.md "wikilink").

很多人都将电影视作一种重要的[艺术形式](../Page/艺术.md "wikilink")：电影可以娱乐、教育、开化并启发观众。通过[配音和](../Page/配音.md "wikilink")[字幕翻译电影传达的信息](../Page/字幕.md "wikilink")，任何电影都可以获得全世界的关注。电影也是由某一特定文化创造出的手工品，可以反映并反过来影响其文化。

### 电子游戏

电子游戏是指人通过电子设备（如电脑、游戏机及手机等）进行的一种娱乐方式。在日常使用中，“[电脑游戏](../Page/电脑游戏.md "wikilink")”或者“PC游戏”通常指在[个人电脑上进行的游戏](../Page/个人电脑.md "wikilink")；[电视游戏则是在专门设计的游戏平台上玩的游戏](../Page/电视游戏.md "wikilink")，通过和标准的[电视机互动完成](../Page/电视.md "wikilink")；[街机游戏则是指专门为设计给街头路人每次付费有偿玩的游戏](../Page/街机.md "wikilink")。电子游戏还包括：移动电话、个人数字助理、高级计算器等上的游戏。

### 音频录制与发布

### 互联网

#### 博客

随着互联网的快速发展，博客也变得无处不在。博客是一般由个人维护的网站，其中内容主要是评论、时间描述或者一些如图片、视频等互动媒体。博客中的文章往往按照时间先后逆向排序，最新撰写的博文会出现在最上面。许多博客提供有关某一特定话题的评论或欣慰；其他功能主要是个人在线日记。一个典型的博客包括有文字、图片、图标、链往其他博客、网页或相关媒体的链接。对于许多博客而言，读者可以在留言区同博主交流是很重要的一部分。博客主要以文字为主，但是其他形式的博客也开始兴起。[微博客是另一种类型的博客](../Page/微博客.md "wikilink")，其中的博文长度都很短小。

#### RSS订阅

[RSS是一種聚合新聞和類新聞網站的格式](../Page/RSS.md "wikilink")，類新聞網站包括像是[Wired之類主要的新聞網站](../Page/连线.md "wikilink")、像[Slashdot社群網站](../Page/Slashdot.md "wikilink")、以及個人的部落格。RSS檔案包含了全文或是節錄的文字，再加上發用者所訂閱之網摘佈資料和授權的元數據。讀者可以依需求訂閱不同網站的RSS，當網站有更新時，網站會送出RSS，讀者可以配合[瀏覽器或是其他軟體更新的部份內容或全部內容](../Page/瀏覽器.md "wikilink")。RSS可以使讀者用自動的方式和網站互動，不需固定進入一網站也可以知道網站更新的內容，而且訂閱內容時不需提供[電子信箱等個人資訊](../Page/電子信箱.md "wikilink")\[7\]。

#### 播客

[播客是一系列电子媒体文件](../Page/播客.md "wikilink")，通过互联网传播，在便携式媒体播放器和电脑上播放。播客可以指上述用互联网传播媒文件的形式、电子媒体文件本身，也可指製作上述電子媒體文件的人。

#### 移动电话

### 印刷媒体

#### 图书

[Brockhaus_Lexikon.jpg](https://zh.wikipedia.org/wiki/File:Brockhaus_Lexikon.jpg "fig:Brockhaus_Lexikon.jpg")[社交词典](../Page/布罗克豪斯社交词典.md "wikilink")，1902年\]\]

图书是一系列印有文字的纸张，一侧装订在一起，通过封面包裹起来。电子格式发布的书籍被称作[电子书](../Page/电子书.md "wikilink")。

#### 杂志

杂志是一个周期性的出版物，其中包含多样的文章，主要通过广告和/或读者购买来获得资金。

杂志一般分为消费者杂志和商业杂志两类。

#### 报纸

报纸是包含新闻、信息和广告的出版物，通常印刷在成本较低的新闻纸上。报纸可能有或没有特定兴趣指向，通常按日或周发行。第一张印刷报纸出版于1605年，在电台和电视的竞争之下，此类媒体依然繁荣发展。但是近来互联网的兴起对于报纸商业模式有着重大打击。

### 户外媒体

## 目的

[911-Panel.JPG](https://zh.wikipedia.org/wiki/File:911-Panel.JPG "fig:911-Panel.JPG")新闻博物馆中陈列的911次日全美及世界各地区的报纸头条。\]\]

大众媒体不仅仅局限于新闻，尽管人们往往会误解这一点。它们可以用作如下目的：

  - [倡導](../Page/倡導.md "wikilink")，可以为商业或社会服务。包括有[广告](../Page/广告.md "wikilink")、[市场营销](../Page/市场营销.md "wikilink")、[政治宣传](../Page/政治宣传.md "wikilink")、[公共关系以及](../Page/公共关系.md "wikilink")[政治沟通](../Page/政治.md "wikilink")。
  - [娱乐](../Page/娱乐.md "wikilink")，传统的方式是通过表演、音乐、体育、电视节目以及阅读；自从20世纪后期，电子游戏开始兴起。
  - 公共服务公告以及预警。\[8\]

## 分級制度

各國政府為了區分出，各個適當年齡層閱聽的大眾媒體[出版品](../Page/出版品.md "wikilink")，進而[法律制定出](../Page/法律.md "wikilink")[分級](../Page/分級.md "wikilink")。

  - [電視分級制度](../Page/電視分級制度.md "wikilink")
      - [電視節目分級處理辦法](../Page/s:電視節目分級處理辦法.md "wikilink")

      - [出版品及錄影節目帶分級辦法](../Page/出版品及錄影節目帶分級辦法.md "wikilink")

      - [香港電視節目分類制度](../Page/香港電視節目分類制度.md "wikilink")

      - [美國電視分級制度](../Page/美國電視分級制度.md "wikilink")
  - [電影分級制度](../Page/電影分級制度.md "wikilink")
      - [電影片分級處理辦法](../Page/s:電影片分級處理辦法.md "wikilink")

      - [香港電影分級制度](../Page/香港電影分級制度.md "wikilink")

      - [美國電影分級制度](../Page/美國電影分級制度.md "wikilink")
  - [電子遊戲分級系統](../Page/電子遊戲分級系統.md "wikilink")
      - [遊戲軟體分級管理辦法](../Page/遊戲軟體分級管理辦法.md "wikilink")

## 道德问题和批评

缺乏对本地或特定话题的关注是对大众媒体的常见批评。大众新闻媒体为了满足更广大读者群的需求，不得不选择播报国家或者国际新闻。这样它们常常会忽视一些很有趣或者很重要的本地新闻，因为对于大多数读者来说他们并没有兴趣。\[9\]

但有時大眾媒體會出現過於重視社會新聞，忽略其他議題的情形：大眾媒體（尤其是電視）很注重[收視率](../Page/收視率.md "wikilink")，而當一些社會新聞出現時，相關報導的收視率都會上昇，因此新聞會集中在特定社會新聞的相關報導，忽略大眾不一定有興趣的事件\[10\]。

“大众”一词就意味着媒体产品的受众是由大量被动接受、无差别的个体组成。这种印象连同早年对于大众文化和世俗社会的批评，让人们认为大众媒体对于现代社会生活有着负面影响，创造一种模糊而同质的文化，取悦个体而不会让他们有挑战的机会。但是，交互式的数字媒体的读者已经不再只是被动接受。\[11\]

20世纪50年代开始，在一些达到更高工业化程度的国家里，影院、电台、电视等大众媒体在政治力量中扮演关键角色。\[12\]近期的研究显示，媒体所有权有着不断集中化的趋势，许多媒体产业已经高度集中，由少数公司掌控。\[13\]

## 參看

  - [媒體](../Page/媒體.md "wikilink")
  - [傳播媒體](../Page/傳播媒體.md "wikilink")
  - [公民媒體](../Page/公民媒體.md "wikilink")
  - [大眾傳播](../Page/大眾傳播.md "wikilink")

## 注释

## 参考资料

  - [Eco, Umberto](../Page/Umberto_Eco.md "wikilink") (1967) hi

*Per una guerriglia semiologica* (English tr. *[Towards a Semiological
Guerrilla
Warfare](http://www.american-buddha.com/lit.towardsemiologicalguerrilla.htm)*)
first given as a lecture at conference *Vision '67* in New York.

  -
  - Riesman, David and Gitlin, Todd and Glazer, Nathan (1950) *[The
    Lonely Crowd](../Page/The_Lonely_Crowd.md "wikilink")*, [preview at
    google books](http://books.google.com/books?id=EPiul5-CxMYC)

  -
## 延伸阅读

  -
  -
  -
  -
  - Violaine Hacker, « Citoyenneté culturelle et politique européenne
    des médias : entre compétitivité et promotion des valeurs »,
    Nations, cultures et entreprises en Europe, sous la direction de
    Gilles Rouet, Collection Local et Global, L’Harmattan, Paris,
    pp. 163–184

## 外部链接

[\*](../Category/大眾媒体.md "wikilink")

1.  Riesman *et al.* (1950) ch.2 p.50

2.

3.

4.  "Mass media", Oxford English Dictionary, online version November
    2010

5.

6.  Thompson: [*The Media and
    Modernity*](http://books.google.com/books?id=iXHzjIwQae4C) pp.26-8,
    74

7.

8.

9.
10.

11.
12. Eco, U. (1967) quote:

13.