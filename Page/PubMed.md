''' PubMed
'''是一個免費的[搜尋引擎](../Page/搜尋引擎.md "wikilink")，提供[生物](../Page/生物.md "wikilink")[醫學方面的](../Page/醫學.md "wikilink")[論文](../Page/論文.md "wikilink")[搜索以及摘要](../Page/搜索_\(计算机\).md "wikilink")。它的[資料庫來源為](../Page/資料庫.md "wikilink")。其核心主題為醫學，但亦包括其他與醫學相關的領域，像是[護理學或者其他健康學科](../Page/護理學.md "wikilink")。它同時也提供對於相關生物醫學資訊上相當全面的支援，像是[生化學與](../Page/生化.md "wikilink")[細胞生物學](../Page/細胞生物學.md "wikilink")。這搜尋引擎是由[美國國立醫學圖書館提供](../Page/美國國立醫學圖書館.md "wikilink")，作為
Entrez 資訊檢索系統的一部分。PubMed
的資訊並不包括[期刊論文的全文](../Page/期刊.md "wikilink")，但可能提供指向全文提供者（付費或免費）的連結。

## 数据授权协议

PubMed允许其他组织对其数据在非商业的前提下使用。详见[授权协议说明](http://www.nlm.nih.gov/databases/journal.html)。

## 外部連結

  - [MEDLINE/PubMed](http://pubmed.gov/)
  - [iPubMed: Interactive, fuzzy search](http://ipumed.ics.uci.edu/)
  - [PubMed.cn](http://pubmed.cn)
  - [PubMed.china.search](http://www.zklib.com/pubmed.html)

[Category:国立卫生研究院](../Category/国立卫生研究院.md "wikilink")
[Category:美国网站](../Category/美国网站.md "wikilink")
[Category:文献检索数据库](../Category/文献检索数据库.md "wikilink")
[Category:特定领域搜索引擎](../Category/特定领域搜索引擎.md "wikilink")
[Category:医学网站](../Category/医学网站.md "wikilink")
[Category:医学数据库](../Category/医学数据库.md "wikilink")
[Category:美国数据库](../Category/美国数据库.md "wikilink")
[Category:線上文獻資料庫](../Category/線上文獻資料庫.md "wikilink")