**-{zh-hans:内部网关路由协议;zh-hant:內部網關路由協議;zh-tw:內部閘道路由協定;}-**（，縮寫為），又譯**閘道間選徑協定**，是一種[-{zh-hans:内部网关协议;zh-hant:內部網關協議;zh-tw:內部閘道協定;}-](../Page/內部網關協議.md "wikilink")，採用[距離向量演算法](../Page/距離向量路由協定.md "wikilink")。以[自治系統](../Page/自治系統.md "wikilink")
（Autonomous
System）的方式提供路由選擇[路由協議](../Page/路由協議.md "wikilink")，由[思科系統公司發展而成的專利協議](../Page/思科系統.md "wikilink")。其演算法與[路由信息协议](../Page/路由信息协议.md "wikilink")（RIP）類似，透過用戶配置，如延遲、頻寬、可靠性及負載量等於各路由器進行的路由管理。

## 發展

於20世纪80年代中葉，[內部網關協議中最常使用的是](../Page/內部網關協議.md "wikilink")[路由信息协议](../Page/RIP.md "wikilink")（[RIP](../Page/RIP.md "wikilink")）。而且[RIP对于实现相同机种的中小型互联网络的路由选择是非常有用的](../Page/RIP.md "wikilink")，而隨著网络的不断发展，其受到的限制也越加明显。[思科](../Page/思科.md "wikilink")[路由器的实用性舆IGRP的强大功能性](../Page/路由器.md "wikilink")，吸引众多小型互联网络组织采用IGRP來取代[RIP](../Page/RIP.md "wikilink")。並在上世纪90年代，[思科就推出了增强的](../Page/思科.md "wikilink")
IGRP，进一步提高了 IGRP 的操作效率。

IGRP
使用[距離向量演算法](../Page/距離向量路由協定.md "wikilink")，已支援-{zh-hans:内部网关协议;zh-hant:內部網關協議;zh-tw:內部閘道協定;}-（IGP），以選取路由協議進行距離標準比較路徑長度，進行距離向量。相對該協議下的標準為[狀態路由選擇協定](../Page/狀態路由選擇協定.md "wikilink")（[link-state
routing
protocol](../Page/link-state_routing_protocol.md "wikilink")），為了IGRP支援多路徑路由選擇服務的靈活性，在[循環制](../Page/循環制.md "wikilink")（round
robin）方式下，使用兩條同等頻寬運行單通信流下，假若一根線路傳輸失敗，系統會到線路上自動切換另一根。使用多路徑可以是具有不同方法，但仍然奏效的多路徑線路。例如，一條線路比另一條線路優先
3 倍（即標準低 3 級），那麼意味著這條路徑可以使用 3
次。只有符合某特定最佳路徑範圍或在差量範圍之內的路徑才可以用作多路徑。差量（Variance）是網路管理員可以設定的另一個值。

## IGRP更新机制

於預設的情況下，IGRP會每90秒廣播給同網內的所有路由器進行路由更新，假若於3個週期後(270秒後)沒有任何回報，即不能進行路由(Routing),在7個週期後(630秒)後，Cisco
IOS 會自行清除路由。

於Cisco IOS image 軟件中查看IGRP的功能:

`Router# `**`Show``   ``ip``   ``protocols`**
`Routing Protocol is "IGRP 101"`
`Sending updates every 90 seconds, next due in 51 seconds`
`Invalid after 270 second, holddown 280 second, flushed `
`after 630 seconds`

## 參考

  - [网络大典](http://www.net130.com/2004/11-15/94614.html)
  - [CNPAF.net](http://www.cnpaf.net/Class/IGRP/0532918533258748.html)

[Category:Cisco协议](../Category/Cisco协议.md "wikilink")
[Category:路由协议](../Category/路由协议.md "wikilink")