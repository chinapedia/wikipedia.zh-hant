**六分仪座**是17世纪由[約翰·赫維留创立的一个南天小星座](../Page/約翰·赫維留.md "wikilink")。最亮的星（[六分仪座α](../Page/六分仪座α.md "wikilink")）视星等4.49。包含好几对[双星系统](../Page/双星系统.md "wikilink")：[六分仪座γ](../Page/六分仪座γ.md "wikilink"),
[六分仪座35和](../Page/六分仪座35.md "wikilink")[六分仪座40](../Page/六分仪座40.md "wikilink")，另外还有几个[变星](../Page/变星.md "wikilink")。代表着赫維留经常用的测量仪器“[六分仪](../Page/六分仪.md "wikilink")”。

## 深空天体

[星系包括](../Page/星系.md "wikilink")[NGC
3115](../Page/NGC_3115.md "wikilink")，位于大约2千万光年外，9.1等，呈侧面观。其余还有[NGC
3156](../Page/NGC_3156.md "wikilink"), [NGC
3165](../Page/NGC_3165.md "wikilink"), [NGC
3166](../Page/NGC_3166.md "wikilink"), [NGC
3169等星系](../Page/NGC_3169.md "wikilink")；其中以NGC 3166，NGC
3169最为明亮，在10等左右。这两个星系之间距离只有5万光年，互相影响结构。NGC 3156为12.4等，NGC
3165为13.9等。

| 命名                                         | 类型 | [赤经](../Page/赤经.md "wikilink")（[J2000.0](../Page/J2000.0.md "wikilink")） | [赤纬](../Page/赤纬.md "wikilink")（J2000.0） | 大小        | [视星等](../Page/视星等.md "wikilink") |
| ------------------------------------------ | -- | ------------------------------------------------------------------------ | --------------------------------------- | --------- | -------------------------------- |
| [NGC 3115](../Page/NGC_3115.md "wikilink") | 星系 | 10<sup>h</sup> 05<sup>m</sup> 14.1<sup>s</sup>                           | \-07° 43′ 09″                           | 8.3′×3.2′ | 9.1                              |
| [NGC 3166](../Page/NGC_3166.md "wikilink") | 星系 | 10<sup>h</sup> 13<sup>m</sup> 45.6<sup>s</sup>                           | \+03° 25′ 31″                           | 5.2′×2.7′ | 10.4                             |
| [NGC 3169](../Page/NGC_3169.md "wikilink") | 星系 | 10<sup>h</sup> 14<sup>m</sup> 14.8<sup>s</sup>                           | \+03° 28′ 00″                           | 4.8′×3.2′ | 10.2                             |

[Grand_Turk(35).jpg](https://zh.wikipedia.org/wiki/File:Grand_Turk\(35\).jpg "fig:Grand_Turk(35).jpg")

## 重要主星

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/拜耳命名法.md" title="wikilink">拜耳命名法</a></p></th>
<th><p><a href="../Page/弗兰斯蒂德恒星命名法.md" title="wikilink">弗兰斯蒂德恒星命名法</a></p></th>
<th><p><a href="../Page/视星等.md" title="wikilink">视星等</a></p></th>
<th><p>其他命名</p></th>
<th><p><a href="../Page/赤经.md" title="wikilink">赤经</a>（<a href="../Page/J2000.0.md" title="wikilink">J2000.0</a>）</p></th>
<th><p><a href="../Page/赤纬.md" title="wikilink">赤纬</a>（J2000.0）</p></th>
<th><p><a href="../Page/光年.md" title="wikilink">光年距离</a></p></th>
<th><p>注释</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>α</p></td>
<td><p>15</p></td>
<td><p>4.49</p></td>
<td><p><a href="../Page/六分仪座α.md" title="wikilink">六分仪座α</a></p></td>
<td><p>10<sup>h</sup> 07<sup>m</sup> 56.3<sup>s</sup></p></td>
<td><p>−00° 22' 18"</p></td>
<td><p>287</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>γ</p></td>
<td><p>8</p></td>
<td><p>5.05</p></td>
<td><p><a href="../Page/六分仪座γ.md" title="wikilink">六分仪座γ</a>, ADS 7555</p></td>
<td><p>09<sup>h</sup> 52<sup>m</sup> 30.4<sup>s</sup></p></td>
<td><p>−08° 06' 18"</p></td>
<td><p>262</p></td>
<td><ul>
<li><a href="../Page/双星系统.md" title="wikilink">双星系统</a>，分别为+5.58和+6.07等。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>β</p></td>
<td><p>30</p></td>
<td><p>5.09v</p></td>
<td><p><a href="../Page/六分仪座β.md" title="wikilink">六分仪座β</a></p></td>
<td><p>10<sup>h</sup> 30<sup>m</sup> 17.5<sup>s</sup></p></td>
<td><p>−00° 38' 13"</p></td>
<td><p>345</p></td>
<td><ul>
<li>α<sup>2</sup>CVn型<a href="../Page/变星.md" title="wikilink">变星</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>δ</p></td>
<td><p>29</p></td>
<td><p>5.21</p></td>
<td><p><a href="../Page/六分仪座δ.md" title="wikilink">六分仪座δ</a></p></td>
<td><p>10<sup>h</sup> 29<sup>m</sup> 28.7<sup>s</sup></p></td>
<td><p>−02° 44' 21"</p></td>
<td><p>300</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ε</p></td>
<td><p>22</p></td>
<td><p>5.24</p></td>
<td><p><a href="../Page/六分仪座ε.md" title="wikilink">六分仪座ε</a></p></td>
<td><p>10<sup>h</sup> 17<sup>m</sup> 37.8<sup>s</sup></p></td>
<td><p>−08° 04' 08"</p></td>
<td><p>183</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>18</p></td>
<td><p>5.65</p></td>
<td><p><a href="../Page/六分仪座18.md" title="wikilink">六分仪座18</a></p></td>
<td><p>10<sup>h</sup> 10<sup>m</sup> 55.8<sup>s</sup></p></td>
<td><p>−08° 25' 06"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>19</p></td>
<td><p>5.77</p></td>
<td><p><a href="../Page/六分仪座19.md" title="wikilink">六分仪座19</a></p></td>
<td><p>10<sup>h</sup> 12<sup>m</sup> 48.3<sup>s</sup></p></td>
<td><p>+04° 36' 53"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>35</p></td>
<td><p>5.79</p></td>
<td><p><a href="../Page/六分仪座35.md" title="wikilink">六分仪座35</a>, ADS 7902</p></td>
<td><p>10<sup>h</sup> 43<sup>m</sup> 20.9<sup>s</sup></p></td>
<td><p>+04° 44' 52"</p></td>
<td></td>
<td><ul>
<li><a href="../Page/双星系统.md" title="wikilink">双星系统</a>，分别为+5.79，+7.14等</li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><p>41</p></td>
<td><p>5.79</p></td>
<td><p><a href="../Page/六分仪座41.md" title="wikilink">六分仪座41</a>, ADS 7942</p></td>
<td><p>10<sup>h</sup> 50<sup>m</sup> 18.1<sup>s</sup></p></td>
<td><p>−08° 53' 52"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>17</p></td>
<td><p>5.91</p></td>
<td><p><a href="../Page/六分仪座17.md" title="wikilink">六分仪座17</a></p></td>
<td><p>10<sup>h</sup> 10<sup>m</sup> 07.5<sup>s</sup></p></td>
<td><p>−08° 24' 30"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>25</p></td>
<td><p>5.97v</p></td>
<td><p><a href="../Page/六分仪座SS.md" title="wikilink">六分仪座SS</a>/25</p></td>
<td><p>10<sup>h</sup> 23<sup>m</sup> 26.5<sup>s</sup></p></td>
<td><p>−04° 04' 27"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>6</p></td>
<td><p>6.01</p></td>
<td><p><a href="../Page/六分仪座6.md" title="wikilink">六分仪座6</a></p></td>
<td><p>09<sup>h</sup> 51<sup>m</sup> 14.0<sup>s</sup></p></td>
<td><p>−04° 14' 36"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>7</p></td>
<td><p>6.02</p></td>
<td><p><a href="../Page/六分仪座7.md" title="wikilink">六分仪座7</a></p></td>
<td><p>09<sup>h</sup> 52<sup>m</sup> 12.2<sup>s</sup></p></td>
<td><p>+02° 27' 15"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>14</p></td>
<td><p>6.21</p></td>
<td><p><a href="../Page/六分仪座14.md" title="wikilink">六分仪座14</a></p></td>
<td><p>10<sup>h</sup> 06<sup>m</sup> 47.4<sup>s</sup></p></td>
<td><p>+05° 36' 41"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>4</p></td>
<td><p>6.24</p></td>
<td><p><a href="../Page/六分仪座4.md" title="wikilink">六分仪座4</a></p></td>
<td><p>09<sup>h</sup> 50<sup>m</sup> 30.1<sup>s</sup></p></td>
<td><p>+04° 20' 37"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>33</p></td>
<td><p>6.26</p></td>
<td><p><a href="../Page/六分仪座33.md" title="wikilink">六分仪座33</a></p></td>
<td><p>10<sup>h</sup> 41<sup>m</sup> 24.2<sup>s</sup></p></td>
<td><p>−01° 44' 30"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>36</p></td>
<td><p>6.28</p></td>
<td><p><a href="../Page/六分仪座36.md" title="wikilink">六分仪座36</a></p></td>
<td><p>10<sup>h</sup> 45<sup>m</sup> 09.4<sup>s</sup></p></td>
<td><p>+02° 29' 17"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>26</p></td>
<td><p>6.33</p></td>
<td><p><a href="../Page/六分仪座26.md" title="wikilink">六分仪座26</a></p></td>
<td><p>10<sup>h</sup> 26<sup>m</sup> 36.9<sup>s</sup></p></td>
<td><p>−00° 59' 17"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>37</p></td>
<td><p>6.38</p></td>
<td><p><a href="../Page/六分仪座37.md" title="wikilink">六分仪座37</a></p></td>
<td><p>10<sup>h</sup> 46<sup>m</sup> 05.7<sup>s</sup></p></td>
<td><p>+06° 22' 24"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>13</p></td>
<td><p>6.45</p></td>
<td><p><a href="../Page/六分仪座13.md" title="wikilink">六分仪座13</a></p></td>
<td><p>10<sup>h</sup> 04<sup>m</sup> 08.4<sup>s</sup></p></td>
<td><p>+03° 12' 04"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>27</p></td>
<td><p>6.55</p></td>
<td><p><a href="../Page/六分仪座27.md" title="wikilink">六分仪座27</a></p></td>
<td><p>10<sup>h</sup> 26<sup>m</sup> 46.9<sup>s</sup></p></td>
<td><p>−04° 23' 18"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>34</p></td>
<td><p>6.57</p></td>
<td><p><a href="../Page/六分仪座34.md" title="wikilink">六分仪座34</a></p></td>
<td><p>10<sup>h</sup> 42<sup>m</sup> 37.5<sup>s</sup></p></td>
<td><p>+03° 34' 59"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>40</p></td>
<td><p>6.61</p></td>
<td><p><a href="../Page/六分仪座40.md" title="wikilink">六分仪座40</a>, ADS 7936</p></td>
<td><p>10<sup>h</sup> 49<sup>m</sup> 17.3<sup>s</sup></p></td>
<td><p>−04° 01' 27"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>23</p></td>
<td><p>6.66v</p></td>
<td><p><a href="../Page/六分仪座RS.md" title="wikilink">六分仪座RS</a>/23</p></td>
<td><p>10<sup>h</sup> 21<sup>m</sup> 02.0<sup>s</sup></p></td>
<td><p>+02° 17' 23"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>12</p></td>
<td><p>6.70</p></td>
<td><p><a href="../Page/六分仪座12.md" title="wikilink">六分仪座12</a></p></td>
<td><p>09<sup>h</sup> 59<sup>m</sup> 43.1<sup>s</sup></p></td>
<td><p>+03° 23' 05"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>9</p></td>
<td><p>6.72</p></td>
<td><p><a href="../Page/六分仪座9.md" title="wikilink">六分仪座9</a></p></td>
<td><p>09<sup>h</sup> 54<sup>m</sup> 06.7<sup>s</sup></p></td>
<td><p>+04° 56' 43"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>21</p></td>
<td><p>6.97</p></td>
<td><p><a href="../Page/六分仪座21.md" title="wikilink">六分仪座21</a></p></td>
<td><p>10<sup>h</sup> 14<sup>m</sup> 08.4<sup>s</sup></p></td>
<td><p>−07° 59' 36"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>31</p></td>
<td><p>6.98</p></td>
<td><p><a href="../Page/六分仪座31.md" title="wikilink">六分仪座31</a></p></td>
<td><p>10<sup>h</sup> 30<sup>m</sup> 31.0<sup>s</sup></p></td>
<td><p>+02° 09' 01"</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>20</p></td>
<td><p>7.37</p></td>
<td><p><a href="../Page/六分仪座20.md" title="wikilink">六分仪座20</a></p></td>
<td><p>10<sup>h</sup> 13<sup>m</sup> 44.4<sup>s</sup></p></td>
<td><p>−07° 23' 02"</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>15.6</p></td>
<td><p><a href="../Page/LHS_292.md" title="wikilink">LHS 292</a></p></td>
<td><p>10<sup>h</sup> 48<sup>m</sup> 13<sup>s</sup></p></td>
<td><p>−11° 20' 12"</p></td>
<td><p>14.81</p></td>
<td><ul>
<li><a href="../Page/耀星.md" title="wikilink">耀星</a></li>
<li>附近</li>
</ul></td>
</tr>
</tbody>
</table>

来源：<cite>The Bright Star Catalogue, 5th Revised Ed.</cite>, <cite>The
Hipparcos Catalogue, ESA SP-1200</cite>

## 外在链接

  - [The Deep Photographic Guide to the Constellations:
    Sextans](http://www.allthesky.com/constellations/sextans/)

[Liu4](../Category/星座.md "wikilink")
[六分儀座](../Category/六分儀座.md "wikilink")