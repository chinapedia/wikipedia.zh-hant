**上海色拉**，又称**洋山芋色拉**，是[海派西餐中的一道名菜](../Page/海派西餐.md "wikilink")，与俄国菜裡的[奥利维耶色拉较为相似](../Page/俄国沙拉.md "wikilink")，是一种源自西餐却又根据上海人的习惯口味改良的上海混合西餐。

## 配方

主要配方是切丁的煮熟[马铃薯和](../Page/马铃薯.md "wikilink")[红肠丁](../Page/红肠.md "wikilink")，以[蛋黄酱为主的](../Page/蛋黄酱.md "wikilink")[色拉酱拌匀](../Page/沙律#沙律醬.md "wikilink")。出于色彩考虑，亦经常拌入一些煮熟的[豌豆](../Page/豌豆.md "wikilink")。而[苹果也因其甜脆口感](../Page/苹果.md "wikilink")，也时常以丁状出现其中。\[1\]

## 歷史

上海色拉是一道携带历史记忆的上海菜，是开埠时代华洋杂处的产物。1930年，上海知名西餐厅[红房子开张时](../Page/红房子西菜馆.md "wikilink")，已有这道菜供应，作为开胃的头菜。它与[罗宋汤](../Page/罗宋汤.md "wikilink")、[炸猪排等一起构成上海海派西餐](../Page/炸猪排.md "wikilink")，也是上海人经常烹调的家常菜。

## 参考

[Category:上海菜](../Category/上海菜.md "wikilink")
[Category:混合菜](../Category/混合菜.md "wikilink")
[Category:沙律](../Category/沙律.md "wikilink")
[S](../Category/海派西餐.md "wikilink")

1.