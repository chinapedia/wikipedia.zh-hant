[DSB-Stadion.jpg](https://zh.wikipedia.org/wiki/File:DSB-Stadion.jpg "fig:DSB-Stadion.jpg")

**阿爾克馬爾辛斯卓克**（-{zh-cn:）; zh-tw:）;zh-hk:，澳門譯作**艾克馬亞**）;
zh-mo:，又譯**阿爾克馬爾**）}-，國內通稱***AZ***，國際上則稱為AZ阿爾克馬爾（*AZ
Alkmaar*）。是一間位於[荷蘭](../Page/荷蘭.md "wikilink")[阿爾克馬爾市的足球會](../Page/阿爾克馬爾.md "wikilink")，於1967年5月10日成立，目前在[荷蘭甲組足球聯賽作賽](../Page/荷蘭甲組足球聯賽.md "wikilink")。當時的名稱是***AZ
'67***，由兩間球會*Alkmaar '54*和*FC Zaanstreek*合併而成。阿爾克馬爾是2008-09年球季的荷甲冠軍。

## 歷史

阿爾克馬爾於1967年成立，由兩間球會*Alkmaar '54*和*FC Zaanstreek*合併而成。當時球會由兩位商人莫倫拿兄弟（Cees
Molenaar、Klaas
Molenaar）打理，球會也在1970年代以後獲得成功，如在1981年不但贏得聯賽冠軍及[荷蘭盃](../Page/荷蘭盃.md "wikilink")，且打入[歐洲足協杯決賽](../Page/歐洲足協杯.md "wikilink")。不過在莫倫拿兄弟離開後，球隊遂走下坡，1988年更降至乙組作賽。

1990年代後期在另一名商人[迪尔克·斯赫林加](../Page/迪尔克·斯赫林加.md "wikilink")（Dirk
Scheringa）加入球會下，球會始獲重生。阿爾克馬爾在1998年重返甲組，在2004－05年球季再晉身[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")，打進四強階段。而這一季他們在大半季排頭兩名的情況下，最後在荷甲排第三，結果再獲得歐洲足協杯的參賽資格。這個成就殊不容易，因為阿爾克馬爾不是大球會，就算在本土也沒有龐大的球迷人數。而在們在2005－06年賽季的主場──[德侯特球場的平均入場人數](../Page/德侯特球場.md "wikilink")，僅為8,390人。

2006年夏天，球會把主場搬至DSB球場，有17,000個座位。不過因為對門票的需求高於預期，球會主席迪克·舒寧加（Dirk
Scheringa）表示球會希望增加球場座位，並於2010年完工。

後主教練一職由[雲高爾](../Page/雲高爾.md "wikilink")（Louis van
Gaal）出任，2005-06球季始上位，以第三名完成，成績大有進步，一度被視為荷甲第四巨頭（原先的三巨頭乃[PSV燕豪芬](../Page/PSV燕豪芬.md "wikilink")、[阿積士及](../Page/阿積士.md "wikilink")[飛燕諾](../Page/飛燕諾.md "wikilink")，從前荷甲只有一屆不是這三隊勝出，就是阿爾克馬爾，後來的[川迪成為僅有的第五隊](../Page/川迪.md "wikilink")）。2006/07球季戰至尾二輪仍與雙雄燕豪芬和阿積士並駕齊驅，更因對賽成績優勢而暫列榜首，可惜在最後一輪仍掌握主導權之下卻爆冷不敵護級份子[精英隊](../Page/精英隊.md "wikilink")，最終只能以第三名完成，但能與阿積士、燕豪芬在聯賽鬥至最後一輪才分出勝負已是球會近年代表作。但2007/08球季成績大倒退，最終只能失望地以第11名完成。

但踏進2008/09球季，球隊雖然開局兩戰不勝，但自第三輪主場1比0擊敗[PSV燕豪芬後](../Page/PSV燕豪芬.md "wikilink")，竟在往後的比賽錄得24勝4和，成為荷甲一支神奇球隊，領先其後的球隊（於[川迪](../Page/川迪.md "wikilink")、[阿積士](../Page/阿積士.md "wikilink")、[PSV燕豪芬等](../Page/PSV燕豪芬.md "wikilink")）多分。直到聯賽第31輪，雖然阿爾克馬爾諷刺地於這輪聯賽落敗，被終斷聯賽28場不敗紀錄，但球隊卻藉著該輪後一天才比賽的[阿積士也同樣落敗失分](../Page/阿積士.md "wikilink")，正式宣佈奪得聯賽錦標，事隔二十八年後再成為[荷甲冠軍](../Page/荷蘭足球甲級聯賽.md "wikilink")，並同時打破PSV燕豪芬連續4年對冠軍的攏斷地位，以及取得直接進身[歐洲聯賽冠軍盃的資格](../Page/歐洲聯賽冠軍盃.md "wikilink")\[1\]\[2\]。

2009/10球季，隨著功勳領隊雲高爾的轉教德甲班霸[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")，球會改由前荷蘭國家隊教練[艾禾卡特執掌教鞭](../Page/艾禾卡特.md "wikilink")。球會本季需要應付聯賽外，還要兼顧[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")，球會在32強的分組賽階段被編入H組，面對[英格蘭勁旅](../Page/英格蘭.md "wikilink")[阿仙奴](../Page/阿仙奴.md "wikilink")、[希臘的](../Page/希臘.md "wikilink")[奧林比亞高斯以及](../Page/奧林比亞高斯.md "wikilink")[比利時的](../Page/比利時.md "wikilink")[標準列治](../Page/標準列治.md "wikilink")，球隊表現強差人意，只能敬陪末席，黯然被淘汰。而聯賽方面，球會戰績亦相應下滑，但仍保持在中上游位置，前線方面仍舊依靠[艾咸達奧負責爭取入球](../Page/艾咸達奧.md "wikilink")，可惜這位炙手可熱的當家射手卻在季尾轉投勁旅[阿積士](../Page/阿積士.md "wikilink")，令AZ的實力進一步削弱，亦從此退出爭標行列，只能退居國內二線（在荷蘭三巨頭之後）。

## 成就

  - **[荷蘭甲組足球聯賽](../Page/荷蘭甲組足球聯賽.md "wikilink")**
      - **冠軍（2）**：1981年、2009年
  - **[荷蘭乙組足球聯賽](../Page/荷蘭乙組足球聯賽.md "wikilink")**
      - **冠軍（2）**：1996年，1998年
  - **[荷蘭盃](../Page/荷蘭盃.md "wikilink")**
      - **冠軍（4）**：1978年，1981年，1982年，2013年
  - **[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")**
      - **亞軍（1）**：1981年
      - **四強（1）**：2005年

## 著名球員

  - [雲高爾](../Page/路易斯·范加尔.md "wikilink") Louis van Gaal (1986-1987)
  - [-{zh-hans:吉米·弗洛伊德·哈塞尔巴因克;
    zh-hant:哈索賓基;}-](../Page/哈索賓基.md "wikilink") Jimmy
    Floyd Hasselbaink (1990-1993)
  - [-{zh-hans:菲利普·科库; zh-hant:高古;}-](../Page/菲利普·科库.md "wikilink")
    Phillip Cocu (1988-1990)
  - [奧丹](../Page/奧丹.md "wikilink") Barry Opdam (1996-2008)
  - [雲加倫](../Page/雲加倫.md "wikilink") Barry van Galen (1997-2006)
  - [-{zh-hans:克隆坎普; zh-hant:高朗金;}-](../Page/扬·克隆坎普.md "wikilink") Jan
    Kromkamp (2000-2005)
  - [添馬](../Page/汉克·蒂默.md "wikilink") Henk Timmer (2000-2006)
  - [華拿亞](../Page/華拿亞.md "wikilink") Ron Vlaar (2002-2006)
  - [-{zh-hans:德克勒; zh-hant:迪奇利;}-](../Page/蒂姆·德克勒.md "wikilink") Tim de
    Cler (2002-2007)
  - [-{zh-hans:丹尼·兰扎特; zh-hant:蘭沙特;}-](../Page/丹尼·兰扎特.md "wikilink")
    Denny Landzaat (2003-2006)
  - [-{zh-hans:亚林斯; zh-hant:查利安斯;}-](../Page/克乌·亚林斯.md "wikilink") Kew
    Jaliens (2004-2011)
  - [-{zh-hans:马泰森; zh-hant:馬菲臣;}-](../Page/约里斯·马泰森.md "wikilink") Joris
    Mathijsen (2004-2006)

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  -  [官方網站](http://www.az.nl/)

  - [阿爾克馬爾新聞](https://archive.is/20061014115139/http://www.epitch.co.uk/eredivisie/az-alkmaar/)

  - [球迷網站](http://www.azfanpage.nl/)

  - [球迷網站](http://www.forza-az.nl/)

  - [阿爾克馬爾電視](http://www.az.tv/)

  - [球迷網站](http://www.az.pagina.nl/)

[Category:荷蘭足球俱樂部](../Category/荷蘭足球俱樂部.md "wikilink")
[Category:1967年建立的足球俱樂部](../Category/1967年建立的足球俱樂部.md "wikilink")

1.  [阿賈克斯慘敗PSV
    阿爾克馬爾荷甲第二冠](http://www.espnstar.com.tw/news/football/2009/0420/165489.htm)
    ，ESPNSTAR，2009年4月20日
2.  [荷甲：阿爾克馬爾捧杯](http://news.xinhuanet.com/sports/2009-05/11/content_11351926.htm)，新華網，2009年5月11日