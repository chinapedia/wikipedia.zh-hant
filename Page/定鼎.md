**定鼎**，亦作**神鼎**，（391年十月-392年六月）是[十六国时期](../Page/十六国.md "wikilink")[翟魏君主](../Page/翟魏.md "wikilink")[翟釗的](../Page/翟釗.md "wikilink")[年號](../Page/年號.md "wikilink")，共計2年，也是翟魏的第二個年號。《[資治通鑒](../Page/資治通鑒.md "wikilink")·晋孝武帝太元十六年》：“翟遼卒，子釗代立，[改元定鼎](../Page/改元.md "wikilink")。”[鍾淵映](../Page/鍾淵映.md "wikilink")《歷代建元考》：“翟釗‘神鼎’，一作‘定鼎’”

## 纪年

| 定鼎                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 391年                           | 392年                           |
| [干支](../Page/干支纪年.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他使用[神鼎年號的政權](../Page/神鼎.md "wikilink")
  - 同期存在的其他政权年号
      - [太元](../Page/太元_\(晋孝武帝\).md "wikilink")（376年正月-396年十二月）：東晉皇帝[晋孝武帝司马曜的年号](../Page/晋孝武帝.md "wikilink")
      - [太初](../Page/太初_\(苻登\).md "wikilink")：（386年十一月-394年六月）：前秦政权[苻登的年号](../Page/苻登.md "wikilink")
      - [建義](../Page/建义_\(乞伏国仁\).md "wikilink")（385年九月-388年六月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏國仁年号](../Page/乞伏國仁.md "wikilink")
      - [太初](../Page/太初_\(乞伏乾歸\).md "wikilink")（388年六月-400年七月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏乾歸年号](../Page/乞伏乾歸.md "wikilink")
      - [建初](../Page/建初_\(姚萇\).md "wikilink")（386年四月-394年四月）：[后秦政权](../Page/后秦.md "wikilink")[姚苌年号](../Page/姚苌.md "wikilink")
      - [中興](../Page/中興_\(慕容永\).md "wikilink")（386年十月-394年八月）：[西燕政权](../Page/西燕.md "wikilink")[慕容永年号](../Page/慕容永.md "wikilink")
      - [建兴](../Page/建興_\(慕容垂\).md "wikilink")（386年二月-396年四月）：[后燕政权](../Page/后燕.md "wikilink")[慕容垂年号](../Page/慕容垂.md "wikilink")
      - [太安](../Page/太安_\(呂光\).md "wikilink")（386年十月-389年正月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [麟嘉](../Page/麟嘉_\(呂光\).md "wikilink")（389年二月-396年六月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [登国](../Page/登国.md "wikilink")（386年正月-396年六月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

## 消歧义

  - [后凉政權的呂隆的**神鼎**年號](../Page/神鼎.md "wikilink")

[Category:翟魏年號](../Category/翟魏年號.md "wikilink")
[Category:390年代中国政治](../Category/390年代中国政治.md "wikilink")
[Category:391年](../Category/391年.md "wikilink")
[Category:392年](../Category/392年.md "wikilink")