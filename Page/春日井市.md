**春日井市**（）是[名古屋市以北的一個城市](../Page/名古屋市.md "wikilink")。目前是[施行時特例市](../Page/施行時特例市.md "wikilink")。[庄內川流過該市南部](../Page/庄內川.md "wikilink")。[JR](../Page/JR.md "wikilink")[中央線及國道](../Page/中央本線.md "wikilink")19號橫貫該市。不少工作在名古屋市的人居住于該市。[平安時代活躍的書法家](../Page/平安時代.md "wikilink")[小野道風出身于此](../Page/小野道風.md "wikilink")。全日本有八成的[仙人掌在此生產](../Page/仙人掌.md "wikilink")。現任市長為伊藤太。

## 地理

### 山

  - [高座山](../Page/高座山.md "wikilink")
  - [彌勒山](../Page/彌勒山.md "wikilink")
  - [道樹山](../Page/道樹山.md "wikilink")
  - [大谷山](../Page/大谷山.md "wikilink")
  - [東高森山](../Page/東高森山.md "wikilink")
  - [高森山](../Page/高森山.md "wikilink")
  - [北山](../Page/北山.md "wikilink")

### 河川

  - 庄內川
  - [繁田川](../Page/繁田川.md "wikilink")
  - [新繁田川](../Page/新繁田川.md "wikilink")
  - [內津川](../Page/內津川.md "wikilink")
  - [大谷川](../Page/大谷川.md "wikilink")
  - [東大谷川](../Page/東大谷川.md "wikilink")
  - [八田川](../Page/八田川.md "wikilink")
  - [生地川](../Page/生地川.md "wikilink")
  - 新木津用水
  - [地蔵川](../Page/地蔵川.md "wikilink")
  - 愛知用水

### 相鄰的自治體

  - 愛知縣
      - 名古屋市北區、守山區
      - 瀨戶市
      - 小牧市
      - 犬山市
      - 西春日井郡豐山町
  - 岐阜縣
      - 多治見市

## 歷史

  - 1943年（昭和18年）6月1日 - 東春日井郡勝川町、鳥居松村、篠木村、鷹來村合併，施行市制
  - 1958年（昭和33年）1月1日 - 東春日井郡坂下町及高藏寺町編入該市。

## 外部連結

  - [春日井市公所](https://web.archive.org/web/20031018015908/http://www.city.kasugai.aichi.jp/)