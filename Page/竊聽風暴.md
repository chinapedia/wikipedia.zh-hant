《**窃听风暴**》（，又名《他人的生活》、《別人的生活》），[2006年](../Page/2006年电影.md "wikilink")[德國影片](../Page/德國.md "wikilink")。影片是[德國導演](../Page/德國.md "wikilink")[弗洛里安·亨克爾·馮·杜能斯馬克的处女作](../Page/弗洛里安·亨克爾·馮·杜能斯馬克.md "wikilink")，并由他亲自編劇。影片於[2006年](../Page/2006年电影.md "wikilink")3月23日在[德國發行](../Page/德國.md "wikilink")。获得了[第79届奥斯卡金像奖最佳外語片奖](../Page/第79届奥斯卡金像奖.md "wikilink")。

本片主要讲述了1984年[東德](../Page/東德.md "wikilink")[東柏林](../Page/東柏林.md "wikilink")[史塔西](../Page/史塔西.md "wikilink")（国家安全局）的一名秘密探員負責監聽一男劇作家及其女友（知名演员），逐渐被他们的生活所吸引，转而同情他们的遭遇，以至最后暗中对他们施以援手的故事，整个影片从侧面影射了当时东德对于媒体和言论自由的政治气氛，从本质上来讲，整个电影体现的是东德的思想解放的一个缩影。

## 角色

  - [烏爾里希·穆埃](../Page/烏爾里希·穆埃.md "wikilink") - Hauptmann Gerd Wiesler

  - [Martina Gedeck](../Page/Martina_Gedeck.md "wikilink") -
    Christa-Maria Sieland

  - [Sebastian Koch](../Page/Sebastian_Koch.md "wikilink") - Georg
    Dreyman

  - [Ulrich Tukur](../Page/Ulrich_Tukur.md "wikilink") - Oberstleutnant
    Anton Grubitz

  - [Thomas Thieme](../Page/Thomas_Thieme.md "wikilink") - Minister
    Bruno Hempf

  - \- Paul Hauser

  - \- Albert Jerska

  - \- Karl Wallner

  - \- Udo

  - [Herbert Knaup](../Page/Herbert_Knaup.md "wikilink") - Gregor
    Hessenstein

  - [Bastian Trost](../Page/Bastian_Trost.md "wikilink") - Häftling 227

  - Marie Gruber - Frau Meineke

  - \- typewriter expert

## 拍摄场地

影片在2004年10月26日到2004年12月17日短短的37天内拍摄完成，整个摄影过程几乎都在[柏林](../Page/柏林.md "wikilink")。剧作家德雷曼的家坐落在Marchlewskistraße。有一部分场景在[卡尔马克思街的](../Page/卡尔马克思街.md "wikilink")[法兰克福门](../Page/法兰克福门.md "wikilink")（）。影片拍摄还借用了原史塔西的办公楼，位于诺曼街（）。但是，原史塔西监狱纪念馆的负责人却拒绝了馮·杜能斯馬克在原来的监狱内拍摄，理由是据他所知，从来没有任何一位史塔西的官员暗中保护过他所监视的人。\[1\]

## 奖项

  - 2006年德国国家电影奖“金萝拉奖”
      - 最佳影片
      - 最佳导演
      - 最佳劇本
      - 最佳男主角
      - 最佳男配角
      - 最佳摄影
      - 最佳製作設計

<!-- end list -->

  - 2006年[歐洲電影獎](../Page/歐洲電影獎.md "wikilink")
      - 最佳影片
      - 最佳劇本
      - 最佳男主角

<!-- end list -->

  - 2006年金球奖
      - 最佳外語片提名

<!-- end list -->

  - 2007年[凯撒电影奖](../Page/凯撒电影奖.md "wikilink")
      - 最佳外語片

<!-- end list -->

  - 2007年[英国电影学院奖](../Page/英国电影学院奖.md "wikilink")
      - 最佳外語片

<!-- end list -->

  - 2007年美國[奧斯卡](../Page/第79屆奧斯卡金像獎.md "wikilink")
      - 最佳外語片

## 资料来源

<div class="references-small">

<references />

</div>

## 外部參考

  - [官方主页](http://www.movie.de/filme/dlda/)

  -
  -
  -
  - [yahoo电影介绍](https://web.archive.org/web/20070206213930/http://hk.movies.yahoo.com/070121/28/20dt9.html)

[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:德语电影](../Category/德语电影.md "wikilink")
[Category:2000年代劇情片](../Category/2000年代劇情片.md "wikilink")
[Category:德國劇情片](../Category/德國劇情片.md "wikilink")
[Category:冷戰電影](../Category/冷戰電影.md "wikilink")
[Category:政治驚悚片](../Category/政治驚悚片.md "wikilink")
[Category:作家題材電影](../Category/作家題材電影.md "wikilink")
[Category:柏林背景電影](../Category/柏林背景電影.md "wikilink")
[Category:柏林取景电影](../Category/柏林取景电影.md "wikilink")
[Category:1980年代背景電影](../Category/1980年代背景電影.md "wikilink")
[Category:欧洲电影奖最佳影片](../Category/欧洲电影奖最佳影片.md "wikilink")
[Category:奧斯卡最佳外語片獲獎電影](../Category/奧斯卡最佳外語片獲獎電影.md "wikilink")
[Category:英国电影学院奖最佳外语片](../Category/英国电影学院奖最佳外语片.md "wikilink")
[Category:導演處女作](../Category/導演處女作.md "wikilink")
[Category:獅門電影](../Category/獅門電影.md "wikilink")
[Category:凯撒电影奖最佳外语片](../Category/凯撒电影奖最佳外语片.md "wikilink")
[Category:德國歷史題材作品](../Category/德國歷史題材作品.md "wikilink")
[Category:洛杉矶影评人协会奖最佳外语片](../Category/洛杉矶影评人协会奖最佳外语片.md "wikilink")
[Category:纽约影评人协会奖最佳外语片](../Category/纽约影评人协会奖最佳外语片.md "wikilink")
[Category:欧洲电影奖最佳男主角获奖电影](../Category/欧洲电影奖最佳男主角获奖电影.md "wikilink")
[Category:欧洲电影奖最佳编剧获奖电影](../Category/欧洲电影奖最佳编剧获奖电影.md "wikilink")

1.  <http://news.bbc.co.uk/2/hi/europe/4919692.stm>