**中国共产党第十五次全国代表大会**于1997年9月12日至18日在[北京召开](../Page/北京.md "wikilink")，出席大会的代表2,048人、特邀代表60人，代表當時全国5900万党员。此外，中央邀请党内有关负责同志和党外人士列席这次大会。列席这次大会的有：不是十五大代表的十四届中央委员会委员、候补委员和中央纪律检查委员会委员；不是十五大代表、特邀代表的原中央顾问委员会委员；不是十五大代表或特邀代表的党内部分老同志，以及其他有关的同志，共296人。还邀请了国家副主席、全国人大常委会党外副委员长、全国政协党外副主席，各民主党派、全国工商联负责人和无党派人士，以及全国人大、全国政协常委中在京党外人士和部分少数民族、宗教界人士等140位作为来宾列席大会开幕式和闭幕式。

大会的议程是：（1）听取和审查十四届中央委员会的报告；（2）审查中央纪律检查委员会的工作报告（书面）；（3）审议通过《中国共产党章程修正案》；（4）选举第十五届中央委员会；（5）选举新一届中央纪律检查委员会。

[李鹏主持大会开幕式](../Page/李鹏.md "wikilink")。[江泽民代表第十四届中央委员会向大会作了题为](../Page/江泽民.md "wikilink")《高举邓小平理论伟大旗帜　把建设有中国特色社会主义事业全面推向二十一世纪》的报告。

大会批准了江泽民代表十四届中央委员会所作的报告；通过了关于十四届中央委员会报告的决议；通过了关于中央纪律检查委员会工作报告的决议；通过了关于《中国共产党章程修正案》的决议。

大会通过决议，把[邓小平理论确立为中国共产党的指导思想并写入党章](../Page/邓小平理论.md "wikilink")，明确规定中国共产党以[马克思列宁主义](../Page/马克思列宁主义.md "wikilink")、[毛泽东思想](../Page/毛泽东思想.md "wikilink")、邓小平理论作为自己的行动指南。

大会以无记名投票方式，选举出由193名委员、151名候补委员组成的十五届中央委员会。选举出中央纪律检查委员会委员115名。

中共十五大高举邓小平理论伟大旗帜，总结了中国改革和建设的新经验，把邓小平理论确定为党的指导思想，把依法治国确定为治国的基本方略，把坚持公有制为主体、多种所有制经济共同发展，坚持按劳分配为主体、多种分配方式并存，确定为中国在[社会主义初级阶段的基本经济制度和分配制度](../Page/社会主义初级阶段.md "wikilink")。中共十五大对建设有中国特色的社会主义事业的跨世纪发展作出了全面部署。

## 大会机构

  - 中国共产党第十五次全国代表大会主席团名单

（共217人）
（按姓氏笔画为序）

[丁文昌](../Page/丁文昌.md "wikilink")　[丁关根](../Page/丁关根.md "wikilink")　[-{于}-　珍](../Page/于珍.md "wikilink")　[于永波](../Page/于永波.md "wikilink")（满族）　[万　里](../Page/万里.md "wikilink")　[习仲勋](../Page/习仲勋.md "wikilink")　[王　克](../Page/王克.md "wikilink")　[王丙乾](../Page/王丙乾.md "wikilink")　[王乐泉](../Page/王乐泉.md "wikilink")　[王汉斌](../Page/王汉斌.md "wikilink")　[王兆国](../Page/王兆国.md "wikilink")　[王启人](../Page/王启人.md "wikilink")　[王启民](../Page/王启民.md "wikilink")　[王茂林](../Page/王茂林.md "wikilink")　[王忠禹](../Page/王忠禹.md "wikilink")　[王梦奎](../Page/王梦奎.md "wikilink")　[王森浩](../Page/王森浩.md "wikilink")　[王瑞林](../Page/王瑞林.md "wikilink")　[乌力吉](../Page/乌力吉.md "wikilink")（蒙古族）　[方祖岐](../Page/方祖岐.md "wikilink")　[尹克升](../Page/尹克升.md "wikilink")　[邓朴方](../Page/邓朴方.md "wikilink")　[左焕琮](../Page/左焕琮.md "wikilink")　[布　赫](../Page/布赫.md "wikilink")（蒙古族）　[卢荣景](../Page/卢荣景.md "wikilink")　[卢瑞华](../Page/卢瑞华.md "wikilink")　[叶选平](../Page/叶选平.md "wikilink")　[田成平](../Page/田成平.md "wikilink")　[田纪云](../Page/田纪云.md "wikilink")　[史大桢](../Page/史大桢.md "wikilink")　[史玉孝](../Page/史玉孝.md "wikilink")　[白立忱](../Page/白立忱.md "wikilink")（回族）　[白清才](../Page/白清才.md "wikilink")　[令狐安](../Page/令狐安.md "wikilink")　[包叙定](../Page/包叙定.md "wikilink")　[司马义·艾买提](../Page/司马义·艾买提.md "wikilink")（维吾尔族）　[邢世忠](../Page/邢世忠.md "wikilink")　[邢贲思](../Page/邢贲思.md "wikilink")　[成克杰](../Page/成克杰.md "wikilink")（壮族）　[朱　训](../Page/朱训.md "wikilink")　[朱开轩](../Page/朱开轩.md "wikilink")　[朱光亚](../Page/朱光亚.md "wikilink")　[朱丽兰](../Page/朱丽兰.md "wikilink")（女）　[朱育理](../Page/朱育理.md "wikilink")　[朱镕基](../Page/朱镕基.md "wikilink")　[乔　石](../Page/乔石.md "wikilink")　[伍绍祖](../Page/伍绍祖.md "wikilink")　[伍修权](../Page/伍修权.md "wikilink")　[任仲夷](../Page/任仲夷.md "wikilink")　[任建新](../Page/任建新_\(法官\).md "wikilink")　[多吉才让](../Page/多吉才让.md "wikilink")（藏族）　[刘　江](../Page/刘江_\(农业部长\).md "wikilink")　[刘　英](../Page/刘英_\(张闻天夫人\).md "wikilink")（女）　[刘　淇](../Page/刘淇.md "wikilink")　[刘云山](../Page/刘云山.md "wikilink")　[刘中一](../Page/刘中一.md "wikilink")　[刘方仁](../Page/刘方仁.md "wikilink")　[刘延东](../Page/刘延东.md "wikilink")（女）　[刘仲藜](../Page/刘仲藜.md "wikilink")　[刘华秋](../Page/刘华秋.md "wikilink")　[刘华清](../Page/刘华清.md "wikilink")　[刘纪原](../Page/刘纪原.md "wikilink")　[刘明祖](../Page/刘明祖.md "wikilink")　[刘忠德](../Page/刘忠德.md "wikilink")　[刘精松](../Page/刘精松.md "wikilink")　[江村罗布](../Page/江村罗布.md "wikilink")（藏族）　[江泽民](../Page/江泽民.md "wikilink")　[汝　信](../Page/汝信.md "wikilink")　[阮崇武](../Page/阮崇武.md "wikilink")　[孙晋芳](../Page/孙晋芳.md "wikilink")（女）　[孙家正](../Page/孙家正.md "wikilink")　[杜铁环](../Page/杜铁环.md "wikilink")　[李　鹏](../Page/李鹏.md "wikilink")　[李长春](../Page/李长春.md "wikilink")　[李克强](../Page/李克强.md "wikilink")　[李岚清](../Page/李岚清.md "wikilink")　[李伯勇](../Page/李伯勇.md "wikilink")　[李国安](../Page/李国安.md "wikilink")　[李泽民](../Page/李泽民.md "wikilink")　[李建国](../Page/李建国.md "wikilink")　[李贵鲜](../Page/李贵鲜.md "wikilink")　[李素丽](../Page/李素丽.md "wikilink")（女）　[李铁映](../Page/李铁映.md "wikilink")　[李瑞环](../Page/李瑞环.md "wikilink")　[李锡铭](../Page/李锡铭.md "wikilink")　[李新良](../Page/李新良.md "wikilink")　[杨白冰](../Page/杨白冰.md "wikilink")　[杨汝岱](../Page/杨汝岱.md "wikilink")　[杨怀庆](../Page/杨怀庆.md "wikilink")　[杨尚昆](../Page/杨尚昆.md "wikilink")　[杨国庆](../Page/杨国庆_\(中华全国台湾同胞联谊会会长\).md "wikilink")　[杨静仁](../Page/杨静仁.md "wikilink")（回族）　[束怀德](../Page/束怀德.md "wikilink")　[肖　扬](../Page/肖扬.md "wikilink")　[肖　克](../Page/肖克.md "wikilink")　[吴　仪](../Page/吴仪.md "wikilink")（女）　[吴邦国](../Page/吴邦国.md "wikilink")　[吴金印](../Page/吴金印.md "wikilink")　[吴学谦](../Page/吴学谦.md "wikilink")　[吴官正](../Page/吴官正.md "wikilink")　[吴贻弓](../Page/吴贻弓.md "wikilink")　[吴基传](../Page/吴基传.md "wikilink")　[邱娥国](../Page/邱娥国.md "wikilink")　[何添发](../Page/何添发.md "wikilink")　[何椿霖](../Page/何椿霖.md "wikilink")　[邹家华](../Page/邹家华.md "wikilink")　[汪家镠](../Page/汪家镠.md "wikilink")（女）　[宋　平](../Page/宋平.md "wikilink")　[宋　健](../Page/宋健.md "wikilink")　[宋任穷](../Page/宋任穷.md "wikilink")　[宋瑞祥](../Page/宋瑞祥.md "wikilink")　[宋德福](../Page/宋德福.md "wikilink")　[迟浩田](../Page/迟浩田.md "wikilink")　[张　震](../Page/张震_\(上将\).md "wikilink")　[张丁华](../Page/张丁华.md "wikilink")　[张万年](../Page/张万年.md "wikilink")　[张立昌](../Page/张立昌.md "wikilink")　[张全景](../Page/张全景.md "wikilink")　[张志坚](../Page/张志坚.md "wikilink")　[张思卿](../Page/张思卿.md "wikilink")　[张俊九](../Page/张俊九.md "wikilink")　[张维庆](../Page/张维庆.md "wikilink")　[张皓若](../Page/张皓若.md "wikilink")　[张德江](../Page/张德江.md "wikilink")　[张德邻](../Page/张德邻.md "wikilink")　[阿不来提·阿不都热西提](../Page/阿不来提·阿不都热西提.md "wikilink")（维吾尔族）　[陈　虹](../Page/陈虹_\(国家民族事务委员会副主任\).md "wikilink")　[陈云林](../Page/陈云林.md "wikilink")　[陈邦柱](../Page/陈邦柱.md "wikilink")　[陈光毅](../Page/陈光毅.md "wikilink")　[陈明义](../Page/陈明义_\(福建\).md "wikilink")　[陈奎元](../Page/陈奎元.md "wikilink")　[陈俊生](../Page/陈俊生_\(全国政协副主席\).md "wikilink")　[陈敏章](../Page/陈敏章.md "wikilink")　[陈焕友](../Page/陈焕友.md "wikilink")　[陈锦华](../Page/陈锦华.md "wikilink")　[陈慕华](../Page/陈慕华.md "wikilink")（女）　[邵华泽](../Page/邵华泽.md "wikilink")　[罗　干](../Page/罗干.md "wikilink")　[周永康](../Page/周永康.md "wikilink")　[周光召](../Page/周光召.md "wikilink")　[郑必坚](../Page/郑必坚.md "wikilink")　[项怀诚](../Page/项怀诚.md "wikilink")　[胡　绳](../Page/胡绳.md "wikilink")　[胡启立](../Page/胡启立.md "wikilink")　[胡思得](../Page/胡思得.md "wikilink")　[胡富国](../Page/胡富国.md "wikilink")　[胡锦涛](../Page/胡锦涛.md "wikilink")　[钮茂生](../Page/钮茂生.md "wikilink")（满族）　[侯　捷](../Page/侯捷_\(官员\).md "wikilink")　[侯宗宾](../Page/侯宗宾.md "wikilink")　[逄先知](../Page/逄先知.md "wikilink")　[闻世震](../Page/闻世震.md "wikilink")　[姜春云](../Page/姜春云.md "wikilink")　[姜恩柱](../Page/姜恩柱.md "wikilink")　[洪学智](../Page/洪学智.md "wikilink")　[贺美英](../Page/贺美英.md "wikilink")（女）　[耿　飚](../Page/耿飚.md "wikilink")　[耿昭杰](../Page/耿昭杰.md "wikilink")　[桂世镛](../Page/桂世镛.md "wikilink")　[贾　军](../Page/贾军.md "wikilink")　[贾庆林](../Page/贾庆林.md "wikilink")　[贾志杰](../Page/贾志杰.md "wikilink")　[贾春旺](../Page/贾春旺.md "wikilink")　[顾秀莲](../Page/顾秀莲.md "wikilink")（女）　[钱正英](../Page/钱正英.md "wikilink")（女）　[钱其琛](../Page/钱其琛.md "wikilink")　[钱学森](../Page/钱学森.md "wikilink")　[铁木尔·达瓦买提](../Page/铁木尔·达瓦买提.md "wikilink")（维吾尔族）　[倪志福](../Page/倪志福.md "wikilink")　[徐才厚](../Page/徐才厚.md "wikilink")　[徐永清](../Page/徐永清.md "wikilink")　[徐匡迪](../Page/徐匡迪.md "wikilink")　[徐有芳](../Page/徐有芳.md "wikilink")　[徐惠滋](../Page/徐惠滋.md "wikilink")　[徐鹏航](../Page/徐鹏航.md "wikilink")　[高占祥](../Page/高占祥.md "wikilink")　[郭东坡](../Page/郭东坡.md "wikilink")　[郭振乾](../Page/郭振乾.md "wikilink")　[郭超人](../Page/郭超人.md "wikilink")　[唐家璇](../Page/唐家璇.md "wikilink")　[陶驷驹](../Page/陶驷驹.md "wikilink")　[黄　菊](../Page/黄菊.md "wikilink")　[黄火青](../Page/黄火青.md "wikilink")　[黄启璪](../Page/黄启璪.md "wikilink")（女）　[黄镇东](../Page/黄镇东.md "wikilink")　[曹　志](../Page/曹志_\(中共\).md "wikilink")　[曹刚川](../Page/曹刚川.md "wikilink")　[曹庆泽](../Page/曹庆泽.md "wikilink")　[曹伯纯](../Page/曹伯纯.md "wikilink")　[龚育之](../Page/龚育之.md "wikilink")　[盛华仁](../Page/盛华仁.md "wikilink")　[阎海旺](../Page/阎海旺.md "wikilink")　[尉健行](../Page/尉健行.md "wikilink")　[隋永举](../Page/隋永举.md "wikilink")　[彭珮云](../Page/彭珮云.md "wikilink")（女）　[葛洪升](../Page/葛洪升.md "wikilink")　[蒋心雄](../Page/蒋心雄.md "wikilink")　[韩杼滨](../Page/韩杼滨.md "wikilink")　[程维高](../Page/程维高.md "wikilink")　[傅全有](../Page/傅全有.md "wikilink")　[舒惠国](../Page/舒惠国.md "wikilink")　[曾庆存](../Page/曾庆存.md "wikilink")　[曾庆红](../Page/曾庆红.md "wikilink")　[曾建徽](../Page/曾建徽.md "wikilink")　[曾培炎](../Page/曾培炎.md "wikilink")　[温家宝](../Page/温家宝.md "wikilink")　[谢　非](../Page/谢非.md "wikilink")　[谢世杰](../Page/谢世杰.md "wikilink")　[路甬祥](../Page/路甬祥.md "wikilink")　[廖　晖](../Page/廖晖.md "wikilink")　[赛福鼎·艾则孜](../Page/赛福鼎·艾则孜.md "wikilink")（维吾尔族）　[翟泰丰](../Page/翟泰丰.md "wikilink")　[滕文生](../Page/滕文生.md "wikilink")　[薄一波](../Page/薄一波.md "wikilink")　[戴秉国](../Page/戴秉国.md "wikilink")（土家族）　[戴相龙](../Page/戴相龙.md "wikilink")

  - 中国共产党第十五次全国代表大会主席团常务委员会名单

（共33人）

江泽民　李　鹏　乔　石　李瑞环　朱镕基　刘华清　胡锦涛　丁关根　田纪云　李岚清　李铁映　杨白冰　吴邦国　邹家华　姜春云　钱其琛　黄　菊　尉健行　谢　非　温家宝　王汉斌　杨尚昆　万　里　宋　平　薄一波　宋任穷　张　震　张万年　迟浩田　任建新　叶选平　吴学谦　侯宗宾

  - 中国共产党第十五次全国代表大会秘书长名单

胡锦涛

  - 中国共产党第十五次全国代表大会副秘书长名单

丁关根　温家宝　曾庆红

  - 中国共产党第十五次全国代表大会代表资格审查委员会名单

（共19人）

  - 主　任：尉健行
  - 副主任：张全景　[周子玉](../Page/周子玉.md "wikilink")
  - 委　员：（按姓氏笔画为序）[毛致用](../Page/毛致用.md "wikilink")　乌力吉（蒙古族）　[尹凤岐](../Page/尹凤岐.md "wikilink")　尹克升　[关壮民](../Page/关壮民.md "wikilink")（满族）　李长春　李克强　[李铁林](../Page/李铁林.md "wikilink")　张丁华　张德江　阿不来提·阿不都热西提（维吾尔族）　[热　地](../Page/热地.md "wikilink")（藏族）　贾　军　贾庆林　黄启璪（女）　曹庆泽

## 参考文献

## 外部链接

  - [十五大报告](https://web.archive.org/web/20150427145305/http://www.sznews.com/zhuanti/content/2007-08/15/content_1431932.htm)
  - [十五届中共中央](http://cpc.people.com.cn/GB/64162/64168/64568/index.html)

## 参见

  - [中国共产党第十五届中央委员会](../Page/中国共产党第十五届中央委员会.md "wikilink")
  - [中国共产党第十五届中央纪律检查委员会](../Page/中国共产党第十五届中央纪律检查委员会.md "wikilink")
  - [五二九讲话](../Page/五二九讲话.md "wikilink")

[中国共产党第十五次全国代表大会](../Category/中国共产党第十五次全国代表大会.md "wikilink")
[\#15](../Category/中华人民共和国时期中国共产党全国代表大会.md "wikilink")
[Category:1997年中国政治事件](../Category/1997年中国政治事件.md "wikilink")
[Category:1997年北京](../Category/1997年北京.md "wikilink")
[Category:1997年9月](../Category/1997年9月.md "wikilink")