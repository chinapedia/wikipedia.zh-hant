**Masterkey**是由****出產的槍管下掛式[霰彈槍](../Page/霰彈槍.md "wikilink")，發射[12鉛徑霰彈](../Page/霰彈.md "wikilink")。

## 歷史及設計

Masterkey項目在80年代出現，主要目的是在步槍或卡賓槍上下掛一把用於破門、重量較輕的霰彈槍，而不需另外獨立攜帶霰彈槍。Masterkey由泵動的[雷明登870截短改成](../Page/雷明登870泵動式霰彈槍.md "wikilink")，以掛架安裝在[M16突擊步槍及](../Page/M16突擊步槍.md "wikilink")[M4卡賓槍系列的槍管下](../Page/M4卡賓槍.md "wikilink")，安裝方法類似[M203榴彈發射器](../Page/M203榴彈發射器.md "wikilink")。發射發射[12號口徑霰彈](../Page/霰彈.md "wikilink")，內置[管式彈倉](../Page/彈匣#管式彈倉.md "wikilink")，可裝填3發子彈，若加上槍膛中待射的1發，共4發子彈。

由於發射時需要以彈匣充當握把，Masterkey拆下時無法作獨立發射。

從馬克·鮑頓的軍事小說\[1\]中提到，美軍[三角洲特種部隊隊員](../Page/三角洲特種部隊.md "wikilink")（Delta
SFC. Paul
Howe）的[CAR-15卡賓槍上也安裝了Masterkey霰彈槍](../Page/CAR-15卡賓槍.md "wikilink")。

## 替代品

Masterkey很可能被[M26模組配件霰彈槍系統取代](../Page/M26模組配件霰彈槍系統.md "wikilink")，但M26模組式霰彈槍系統仍在試驗當中，M26目前只小量地出現在[持久自由行動的美軍士兵手上](../Page/持久自由行動.md "wikilink")。

## 使用國

  - —被[特種空勤團和其他](../Page/特種空勤團_\(澳大利亞\).md "wikilink")[特種部隊所採用](../Page/特種部隊.md "wikilink")。

  - —被[美國特種作戰司令部所採用](../Page/美國特種作戰司令部.md "wikilink")。

## 登場作品

Masterkey在一些电子游戏中有登场。早在1999年的[三角洲特种部队](../Page/三角洲特种部队.md "wikilink")2中就以M4
Masterkey出现（是该游戏中唯一的霰弹枪）\[2\]，后亦有在《[-{zh-cn:使命召唤;
zh-tw:決勝時刻;}-](../Page/決勝時刻.md "wikilink")》系列（《[現代戰爭2](../Page/決勝時刻：現代戰爭2.md "wikilink")》、《[黑色行動](../Page/決勝時刻：黑色行動.md "wikilink")》、
《[現代戰爭3](../Page/決勝時刻：現代戰爭3.md "wikilink")》）中以[突擊步槍的可添加槍掛武器登場](../Page/突擊步槍.md "wikilink")。

## 來源

<div class="references-small">

<references />

</div>

## 參考

  - [M26模組配件霰彈槍系統](../Page/M26模組配件霰彈槍系統.md "wikilink")
  - [連發式配件槍管下掛式發射器](../Page/連發式配件槍管下掛式發射器.md "wikilink")
  - [M203榴彈發射器](../Page/M203榴彈發射器.md "wikilink")
  - [雷明登870泵動式霰彈槍](../Page/雷明登870泵動式霰彈槍.md "wikilink")

## 外部連結

  - [knightarmco.com-KAC
    Masterkey頁面](https://web.archive.org/web/20120926032554/http://www.knightarmco.com/m203_12ga.html)

  - [Modern Firearms -
    雷明登870泵動式霰彈槍](https://web.archive.org/web/20071023181102/http://world.guns.ru/shotgun/sh17-e.htm)

[Category:泵動式霰彈槍](../Category/泵動式霰彈槍.md "wikilink")
[Category:美國霰彈槍](../Category/美國霰彈槍.md "wikilink")

1.  馬克·鮑頓， *:Black Hawk Down: A Story of Modern War*，Corgi
    Books，[倫敦](../Page/倫敦.md "wikilink")，2000年 ISBN 0-552-14750-8,
    第31頁
2.  <http://www.chuzhaobiao.com/miji/2026.html>