**星系年表**是[星系](../Page/星系.md "wikilink")、[星系團](../Page/星系團.md "wikilink")、和[宇宙大尺度結構的年代學](../Page/大尺度結構.md "wikilink")。

## 20世紀之前

  - [400s BC](../Page/前5世紀.md "wikilink") -
    [德謨克利特建議天空中明亮的](../Page/德謨克利特.md "wikilink")[銀河](../Page/銀河系.md "wikilink")
    可能包含許多的[星星](../Page/恆星.md "wikilink")。
  - [300s BC](../Page/前4世紀.md "wikilink") -
    [亞里斯多德認為銀河是由一些散發出氣體的巨大的星星聚集在一起引燃的火焰造成的](../Page/亞里斯多德.md "wikilink")，並且這些點燃的氣體都存在於的[與天上的世界聯繫在一起運動的](../Page/月下半球.md "wikilink")[大氣層](../Page/大氣層.md "wikilink")"\[1\]。
  - [964 AD](../Page/964年.md "wikilink") -
    [波斯天文學家](../Page/伊斯蘭天文學.md "wikilink") [Abd
    al-Rahman al-Sufi](../Page/Abd_al-Rahman_al-Sufi.md "wikilink")
    (Azophi)在他的[恆星之書中記錄了](../Page/恆星之書.md "wikilink")[仙女座星系](../Page/仙女座星系.md "wikilink")
    \[2\]和[大麥哲倫雲](../Page/大麥哲倫雲.md "wikilink")\[3\]\[4\]。這是除了銀河系之外，最早被看見的其他[星系](../Page/星系.md "wikilink")
    。
  - [1000s](../Page/11世紀.md "wikilink") – 另一位波斯天文學家[Abū Rayhān
    al-Bīrūnī描述](../Page/Abū_Rayhān_al-Bīrūnī.md "wikilink")[銀河是許多](../Page/銀河系.md "wikilink")[星雲星星的集合體](../Page/星雲.md "wikilink")\[5\]。
  - [1000s](../Page/11世紀.md "wikilink")
    –一位[阿拉伯天文學家](../Page/伊斯蘭天文學.md "wikilink")
    [Ibn al-Haytham](../Page/Ibn_al-Haytham.md "wikilink") (Alhazen)
    第一次嘗試測量銀河的[視差反駁亞里斯多德的銀河理論](../Page/視差.md "wikilink")\[6\]，因為測不出銀河的視差，因此他認為銀河距離[地球很遠](../Page/地球.md "wikilink")，不屬於地球的大氣層\[7\]。
  - [1100s](../Page/12世紀.md "wikilink") –[Islamic
    Spain的](../Page/Al-Andalus.md "wikilink")[伊本·巴哲](../Page/伊本·巴哲.md "wikilink")(Avempace)提出[銀河是由許多的恆星組成](../Page/銀河系.md "wikilink")，因為受到[地球大氣層的](../Page/地球大氣層.md "wikilink")[折射而呈現連續的影像](../Page/折射.md "wikilink")\[8\]。
  - [1300s](../Page/14世紀.md "wikilink") –
    [敘利亞的](../Page/敘利亞.md "wikilink")[Ibn Qayyim
    Al-Jawziyya](../Page/Ibn_Qayyim_Al-Jawziyya.md "wikilink")
    提出[銀河是由許多小星星聚集在一起](../Page/銀河系.md "wikilink")，並且固定在包含恆星的天球上，而且這些小星星都比[行星巨大](../Page/行星.md "wikilink")\[9\]。
  - [1521年](../Page/1521年.md "wikilink") －
    [斐迪南·麥哲倫在環球的探險中觀察到](../Page/斐迪南·麥哲倫.md "wikilink")[麥哲倫雲](../Page/麥哲倫雲.md "wikilink")。
  - [1610年](../Page/1610年.md "wikilink") －
    [伽利略使用](../Page/伽利略·伽利莱.md "wikilink")[望遠鏡觀察](../Page/望遠鏡.md "wikilink")[天空上的亮帶](../Page/天空.md "wikilink")（[銀河](../Page/銀河.md "wikilink")），發現是許多暗淡的[恆星](../Page/恆星.md "wikilink")。
  - [1750年](../Page/1750年.md "wikilink") －
    [湯姆斯·萊特](../Page/湯姆斯·萊特_\(天文學家\).md "wikilink")()論述星系和銀河的形狀。
  - [1755年](../Page/1755年.md "wikilink") －
    接續Wright的工作，[伊曼努尔·康德臆測星系是被](../Page/伊曼努尔·康德.md "wikilink")[引力聚集在](../Page/引力.md "wikilink")
    一起旋轉的恆星盤，星雲是被分離的星系。
  - [1845年](../Page/1845年.md "wikilink") －
    [威廉·帕森思發現有星雲有螺旋的形狀](../Page/威廉·帕森思.md "wikilink")。（即為[M51](../Page/M51.md "wikilink")）

## 20世紀前期

  - [1918年](../Page/1918年.md "wikilink") －
    [哈罗·沙普利顯示](../Page/哈罗·沙普利.md "wikilink")[球狀星團分布的扁球體或暈](../Page/球狀星團.md "wikilink")，不是以[地球為中心](../Page/地球.md "wikilink")，正確的說，他是以[銀河系的中心為中心](../Page/銀河系.md "wikilink")。
  - [1920年](../Page/1920年.md "wikilink")
    －哈罗·沙普利和[希伯·柯蒂斯辯論](../Page/希伯·柯蒂斯.md "wikilink")[螺旋星雲是否在銀河系之內](../Page/螺旋星雲.md "wikilink")。（參見[沙普利-柯蒂斯之争](../Page/沙普利-柯蒂斯之争.md "wikilink")。）
  - [1923年](../Page/1923年.md "wikilink") －
    [愛德文·哈勃在](../Page/愛德文·哈勃.md "wikilink")[仙女座星系發現](../Page/仙女座星系.md "wikilink")[造父變星](../Page/造父變星.md "wikilink")，解決了[沙普利-柯蒂斯之争](../Page/沙普利-柯蒂斯之争.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink") － [Robert
    Trumpler利用](../Page/Robert_Trumpler.md "wikilink")[疏散星團的](../Page/疏散星團.md "wikilink")[吸收譜線對](../Page/吸收譜線.md "wikilink")[銀河平面上的](../Page/銀河平面.md "wikilink")[星際塵埃進行定量的觀察](../Page/星際塵埃.md "wikilink")。這種吸收對早期的銀河系模型造成極大的困擾。
  - [1932年](../Page/1932年.md "wikilink") －
    [卡尔·央斯基發現來自銀河中心的](../Page/卡尔·央斯基.md "wikilink")[電波噪音](../Page/電波噪音.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink") －
    [弗里茨·兹威基將](../Page/弗里茨·兹威基.md "wikilink")[維里定理運用在](../Page/維里定理.md "wikilink")[后髮座星系團](../Page/后髮座星系團.md "wikilink")，獲得看不見的[物質存在的證據](../Page/物質.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink") －愛德文·哈勃提出螺旋、棒旋、橢圓和不規則星系的分類法。
  - [1939年](../Page/1939年.md "wikilink") － [Grote
    Reber發現](../Page/Grote_Reber.md "wikilink")[天鵝座A電波源](../Page/天鵝座A.md "wikilink")
  - [1943年](../Page/1943年.md "wikilink") － [Carl Keenan
    Seyfert辨認出六個](../Page/Carl_Keenan_Seyfert.md "wikilink")[螺旋星系有不尋常的寬廣](../Page/螺旋星系.md "wikilink")[發射譜線](../Page/發射譜線.md "wikilink")，命名為[賽佛特星系](../Page/賽佛特星系.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink") － J.G. Bolton、G.J.
    Stanley、和O.B. Slee 確認[M87和](../Page/M87.md "wikilink")[NGC
    5128是銀河系外的電波源](../Page/NGC_5128.md "wikilink")。

## 20世紀後期

  - [1953年](../Page/1953年.md "wikilink") － [Gerard de
    Vaucouleurs發現直徑大約是](../Page/Gerard_de_Vaucouleurs.md "wikilink")200萬[光年的](../Page/光年.md "wikilink")[室女座星系團是被約束住的巨大](../Page/室女座星系團.md "wikilink")[超星系團](../Page/超星系團.md "wikilink")。
  - [1954年](../Page/1954年.md "wikilink") －
    [沃爾特·巴德和](../Page/沃爾特·巴德.md "wikilink")[魯道夫·閔可夫斯基辨認出銀河系外電波源天鵝座A的光學對應體](../Page/魯道夫·閔可夫斯基.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink") －
    [劍橋大學的](../Page/劍橋大學.md "wikilink")[干涉儀發現數百個電波源](../Page/劍橋干涉儀.md "wikilink")，並編輯成[3C目錄](../Page/3C目錄.md "wikilink")。其中有許多在後來被發現是[類星體和](../Page/類星體.md "wikilink")[電波星系](../Page/電波星系.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink") － [Thomas
    Matthews測量出電波源](../Page/Thomas_Matthews.md "wikilink")[3C
    48的位置](../Page/3C_48.md "wikilink")，經確度達到5"。
  - [1960年](../Page/1960年.md "wikilink") － [Allan
    Sandage已可見光研究](../Page/Allan_Sandage.md "wikilink")3C
    48，並且觀察到異常的藍類星體。
  - [1962年](../Page/1962年.md "wikilink") － [Cyril
    Hazard](../Page/Cyril_Hazard.md "wikilink")、 M.B. Mackey、和A.J.
    Shimmins利用[月掩星精確的測量](../Page/掩星.md "wikilink")[3C
    273的位置](../Page/3C_273.md "wikilink")，並推論出他有雙重的來源。
  - [1962年](../Page/1962年.md "wikilink") － [Olin
    Eggen](../Page/Olin_Eggen.md "wikilink")、[Donald
    Lynden-Bell和](../Page/Donald_Lynden-Bell.md "wikilink")[Allan
    Sandage提出](../Page/Allan_Sandage.md "wikilink")[星系形成的理論](../Page/星系形成.md "wikilink")，以單一（相對的）快速的整體崩潰，先形成了暈，然後才是星系盤面。
  - [1963年](../Page/1963年.md "wikilink") － [Maarten
    Schmidt辨識出](../Page/Maarten_Schmidt.md "wikilink")[3C
    273內](../Page/3C_273.md "wikilink")[紅移的](../Page/紅移.md "wikilink")[巴耳末線](../Page/巴耳末系.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink") － [Jeremiah
    Ostriker和](../Page/Jeremiah_Ostriker.md "wikilink")[James
    Peebles發現在](../Page/Philip_James_Edwin_Peebles.md "wikilink")[螺旋星系中可見物質的總質量不足以產生足夠的牛頓萬有引力](../Page/螺旋星系.md "wikilink")，使盤中的物質被吸引住而不會四散飛離或改變盤的形狀。
  - [1974年](../Page/1974年.md "wikilink") － [B.L.
    Fanaroff](../Page/Bernie_Fanaroff.md "wikilink") and [J.M.
    Riley區別出邊緣昏暗](../Page/Julia_Riley.md "wikilink")（FR
    I）和邊緣明亮（FR II）的電波源。
  - [1976年](../Page/1976年.md "wikilink") － [Sandra
    Faber和](../Page/Sandra_Faber.md "wikilink")[羅伯特·傑克遜發現橢圓星系的亮度和相對於中心](../Page/羅伯特·傑克遜.md "wikilink")[速度離散的費伯](../Page/速度.md "wikilink")-傑克遜關係。
  - [1977年](../Page/1977年.md "wikilink") － [R. Brent
    Tully和](../Page/R._Brent_Tully.md "wikilink")[理查德·費雪發表獨立的螺旋星系的亮度與盤面部分的](../Page/理查德·費雪.md "wikilink")[速度與](../Page/速度.md "wikilink")[自轉曲線的關係式](../Page/自轉曲線.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink") －
    [史蒂夫·葛利格-{里}-和](../Page/史蒂夫·葛利格里（天文學家）.md "wikilink")[萊爾德·湯普森描述后髮座超星系團](../Page/萊爾德·湯普森.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink") －
    [維拉·路賓](../Page/維拉·路賓.md "wikilink")、[肯特·福特](../Page/肯特·福特.md "wikilink")、
    N.
    Thonnard、和[艾伯特·玻斯曼測量了一些螺旋星系的自轉曲線](../Page/艾伯特·玻斯曼.md "wikilink")，並且發現可以看見的恆星數量與牛頓萬有引力預測的數量之間有巨大的偏差。
  - [1978年](../Page/1978年.md "wikilink") －
    [倫納德·瑟爾和](../Page/倫納德·瑟爾.md "wikilink")[羅伯特·齊恩推論](../Page/羅伯特·齊恩.md "wikilink")[星系形成是由許多較小的合併組合成大的](../Page/星系形成.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink") －
    [羅伯特·科什那](../Page/羅伯特·科什那.md "wikilink")、[August
    Oemler](../Page/August_Oemler.md "wikilink")、[Paul
    Schechter和](../Page/Paul_Schechter.md "wikilink")[Stephen
    Shectman發現](../Page/Stephen_Shectman.md "wikilink")[牧夫座有一個直徑達到一億光年的巨大](../Page/牧夫座.md "wikilink")[空洞](../Page/空洞.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink") － [Robert
    Antonucci和J](../Page/Robert_Antonucci.md "wikilink").米勒發現塞佛特II星系[NGC
    1068有高度](../Page/NGC_1068.md "wikilink")[偏振只出現在反射光中的寬譜線](../Page/偏振.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink") － [Amos
    Yahil](../Page/Amos_Yahil.md "wikilink")、[大衛·沃克和](../Page/大衛·沃克.md "wikilink")[Michael
    Rowan-Robinson發現](../Page/Michael_Rowan-Robinson.md "wikilink")[IRAS星系密度的](../Page/紅外線天文衛星.md "wikilink")[偶極方向與](../Page/偶極.md "wikilink")[宇宙微波背景溫度偶極的方向一致](../Page/宇宙微波背景.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink") － [David
    Burstein](../Page/David_Burstein.md "wikilink")、[Roger
    Davies](../Page/Roger_Davies.md "wikilink")、[Alan
    Dressler](../Page/Alan_Dressler.md "wikilink")、[Sandra
    Faber](../Page/Sandra_M._Faber.md "wikilink")、[Donald
    Lynden-Bell](../Page/Donald_Lynden-Bell.md "wikilink")、R.J.
    Terlevich和[Gary
    Wegner聲稱在](../Page/Gary_Wegner.md "wikilink")[銀河系周圍](../Page/銀河系.md "wikilink")200萬光年內的星系一起都朝向[長蛇座和](../Page/長蛇座.md "wikilink")[半人馬座方向](../Page/半人馬座.md "wikilink")，被稱為[巨引源的星系團移動](../Page/巨引源.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink") －
    [瑪格麗特·杰勒](../Page/瑪格麗特·杰勒.md "wikilink")（）和[約翰·修茲勞](../Page/約翰·修茲勞.md "wikilink")（）發現[長城](../Page/长城_\(天文学\).md "wikilink")，是一個由星系組成的薄片，長五億光年、寬三億光年，但只有一千五百萬光年厚。
  - [1990年](../Page/1990年.md "wikilink") － [Michael
    Rowan-Robinson和](../Page/Michael_Rowan-Robinson.md "wikilink")[Tom
    Broadhurst發現IRAS星系](../Page/Tom_Broadhurst.md "wikilink")[IRAS
    F10214+4724是](../Page/IRAS_F10214+4724.md "wikilink")[宇宙中已知的天體中最亮的](../Page/宇宙.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink") －
    第一次在[宇宙微波背景中偵測到大尺度的結構](../Page/宇宙微波背景輻射.md "wikilink")，表明第一群的星系團種子出現在宇宙的早期。
  - [1995年](../Page/1995年.md "wikilink") －
    第一次在[宇宙微波背景中偵測到小尺度的結構](../Page/宇宙微波背景輻射.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink") －
    [哈柏深空在](../Page/哈伯深空.md "wikilink")144弧秒的視場中進行[星系的調查](../Page/星系.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink") －
    [2度視場星系紅移巡天測繪出](../Page/2度視場星系紅移巡天.md "wikilink")[宇宙在鄰近](../Page/宇宙.md "wikilink")[銀河系附近的大尺度結構](../Page/銀河系.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink") －
    [哈柏南天深空](../Page/哈柏南天深空.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink") －
    發現[宇宙的加速](../Page/宇宙的加速.md "wikilink")
  - [2000年](../Page/2000年.md "wikilink") －
    一些來自[宇宙微波背景實驗的數據](../Page/宇宙微波背景輻射.md "wikilink")，提供宇宙是平的有力證據（空間沒有彎曲，但時-空是），這對大尺度結構的形成有重要的含意。

## 21世紀

  - [2001年](../Page/2001年.md "wikilink") －
    持續進行中的[史隆數位巡天釋出第一批資料](../Page/史隆數位巡天.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink") －
    [歐洲南方天文臺發現](../Page/歐洲南方天文臺.md "wikilink")[Abell
    1835 IR1916](../Page/Abell_1835_IR1916.md "wikilink") －
    從地球能看見的最遙遠星系。
  - [2004年](../Page/2004年.md "wikilink") － [Arcminute Microkelvin
    Imager](../Page/Arcminute_Microkelvin_Imager.md "wikilink")
    開始描繪遙遠星系團的分佈。
  - [2013年](../Page/2013年.md "wikilink") －
    天文學家以[光譜確認](../Page/光譜.md "wikilink")[z8_GND_5296是至今確認距離地球最遠的星系](../Page/z8_GND_5296.md "wikilink")。該星系形成於[大爆炸後](../Page/大爆炸.md "wikilink")7億年，不過它的觀測位置距離地球約300億年。\[10\]

## 参考文献

[Category:宇宙大尺度結構](../Category/宇宙大尺度結構.md "wikilink")
[Category:天文學年表](../Category/天文學年表.md "wikilink")
[Category:星系天文学](../Category/星系天文学.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.