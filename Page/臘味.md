[HK_Kwun_Tong_Shui_Wo_Street_Market_Taiwanese_Dried_Chickens.JPG](https://zh.wikipedia.org/wiki/File:HK_Kwun_Tong_Shui_Wo_Street_Market_Taiwanese_Dried_Chickens.JPG "fig:HK_Kwun_Tong_Shui_Wo_Street_Market_Taiwanese_Dried_Chickens.JPG")
**臘味**一詞泛指所有經過臘製的[肉食](../Page/肉食.md "wikilink")，常見的有[臘腸](../Page/臘腸.md "wikilink")、臘鴨\[1\]、臘雞、[臘肉](../Page/臘肉.md "wikilink")、臘魚等，[中國](../Page/中國.md "wikilink")[廣東](../Page/廣東.md "wikilink")[清遠](../Page/清遠.md "wikilink")[連州市著名的臘味之鄉](../Page/連州市.md "wikilink")[東陂鎮甚至有臘狗和臘蛋等品種](../Page/東陂鎮.md "wikilink")。

[中國人製作臘味的](../Page/中國人.md "wikilink")[歷史已有兩千多年之久](../Page/歷史.md "wikilink")\[2\]，臘味在[華人社群深受歡迎](../Page/華人地區.md "wikilink")，是過[年必備的](../Page/春節.md "wikilink")[年貨](../Page/年貨.md "wikilink")\[3\]。

## 何謂「臘製」

「臘」是一種[肉類食物的處理方法](../Page/肉類食物.md "wikilink")，指把[肉類以](../Page/肉類.md "wikilink")[鹽或](../Page/鹽.md "wikilink")[醬](../Page/醬.md "wikilink")[腌漬後再風乾](../Page/腌.md "wikilink")。[農曆十二月稱爲](../Page/農曆.md "wikilink")“[臘月](../Page/臘月.md "wikilink")”，而且如廣州一帶，素有“秋風起，食臘味”的俗語，每年農曆十一月到十二月秋冬季節，[天氣寒冷且](../Page/天氣.md "wikilink")[乾燥](../Page/乾燥.md "wikilink")，[肉類不易變質且](../Page/肉類.md "wikilink")[蚊蟲不多](../Page/蚊.md "wikilink")，適合風乾製臘味，因以為名。

由於[華南地區](../Page/華南地區.md "wikilink")[潮濕溫暖](../Page/潮濕.md "wikilink")，缺乏風乾的天然[條件](../Page/條件.md "wikilink")，因此也有以太陽曬乾的方法製作臘味。由於戶外乾製容易招來蟲子，[衛生](../Page/衛生.md "wikilink")[條件較難保證](../Page/條件.md "wikilink")，因此近年也有使用專門的[烘焙室來乾製或燻製](../Page/烘焙.md "wikilink")\[4\]。然而，據一些[加拿大的](../Page/加拿大.md "wikilink")[香港](../Page/香港.md "wikilink")[移民表示](../Page/移民.md "wikilink")，在[加國](../Page/加國.md "wikilink")[乾燥大陸性氣候下風乾的臘味](../Page/乾燥.md "wikilink")，風味比[香港在](../Page/香港.md "wikilink")[潮濕](../Page/潮濕.md "wikilink")[天氣下晒製的為佳](../Page/天氣.md "wikilink")。

## 廣式臘味

[HK_Aberdeen_東勝道_Tung_Sing_Road_得記燒臘飯店_Tak_Kee_Rice_Restaurant_Nov-2012_腊鴨比_Dried_Duck_legs.JPG](https://zh.wikipedia.org/wiki/File:HK_Aberdeen_東勝道_Tung_Sing_Road_得記燒臘飯店_Tak_Kee_Rice_Restaurant_Nov-2012_腊鴨比_Dried_Duck_legs.JPG "fig:HK_Aberdeen_東勝道_Tung_Sing_Road_得記燒臘飯店_Tak_Kee_Rice_Restaurant_Nov-2012_腊鴨比_Dried_Duck_legs.JPG")[髀](../Page/髀.md "wikilink")\]\]
廣式臘味流行於[廣東](../Page/廣東.md "wikilink")、[香港和](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")，不但可用於餸菜，更是製作[蘿蔔糕](../Page/蘿蔔糕.md "wikilink")、[生炒糯米飯的主要配料](../Page/生炒糯米飯.md "wikilink")。

廣式臘味的口味偏甜，主要配料包括[油](../Page/食用油.md "wikilink")、[鹽](../Page/鹽.md "wikilink")、[醬油](../Page/醬油.md "wikilink")、[糖等](../Page/蔗糖.md "wikilink")。其中臘腸較常見，製作方法為將豬肉切粒腌製，然後塞入腸衣（一般用[豬](../Page/豬.md "wikilink")、[羊等牲畜的](../Page/羊.md "wikilink")[小腸製作](../Page/小腸.md "wikilink")）内風乾。長度有長有短，顔色比較鮮豔。

除腊肠外，广式腊味还包括[腊肉](../Page/广式腊肉.md "wikilink")、[腊鸭](../Page/腊鸭.md "wikilink")、腊制内脏（如[腊鸭胗](../Page/腊鸭胗.md "wikilink")、[腊鸭肠](../Page/腊鸭肠.md "wikilink")），以至于[腊鱼](../Page/腊鱼.md "wikilink")、[腊田鼠等多个种类](../Page/腊田鼠.md "wikilink")。此外，还有使用多种原料的复合腊味（像是在鸭肝中塞入肥猪肉后腊制而成的“[金银润](../Page/金银润.md "wikilink")”）等。

廣式臘味在[秋](../Page/秋.md "wikilink")[冬時節特別受歡迎](../Page/冬.md "wikilink")，是烹調臘味飯的重要食材\[5\]，但部分商家為增加臘味的色澤和改善口感，從而賣得更好的價錢，謀取更高利潤，於是使用含有[瘦肉精的豬肉製作臘肉](../Page/瘦肉精.md "wikilink")，並對臘味加入各種[染料和](../Page/染料.md "wikilink")[防腐劑](../Page/防腐劑.md "wikilink")\[6\]。

## 注释

<references/>

[臘味](../Category/臘味.md "wikilink")
[Category:廣東食品](../Category/廣東食品.md "wikilink")

1.
2.  [](http://contact.com.tw/contact/china/ch_newscontent.php?area=%B4%F2%ABn&subject=%AC%FC%AD%B9%A1B%A4%F4%AAG%A1B%AFS%B2%A3&code=001346)
3.
4.
5.  [冬暖甘香
    臘味糯米飯](http://www.eastweek.my-magazine.me/index.php?aid=31298)，東周網，2014年1月22日
6.  [廣東臘肉 4成不合格](http://www.appledaily.com.tw/appledaily/article/international/20090116/31323156/)，蘋果日報，2009年1月16日