**陳小韻**（****，），籍貫[上海](../Page/上海.md "wikilink")，是香港商人[陳耀旻的女兒](../Page/陳耀旻.md "wikilink")，曾經因為在[馬莎百貨高買而留有案底](../Page/馬莎百貨.md "wikilink")\[1\]。她在1999年加入[模特兒行列](../Page/模特兒.md "wikilink")。現进入商界，現時配偶為銀行界Kin，也有一名兒子。\[2\]

## 演出作品

### 電影

  - 2004年：《[A1頭條](../Page/A1頭條.md "wikilink")》
  - 2004年：《[長春花](../Page/長春花.md "wikilink")》

## 參考

[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Xiao小韻](../Category/陳姓.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港罪犯](../Category/香港罪犯.md "wikilink")

1.  [娛樂怪史：陳小韻偷內褲人贓並獲 2007年05月22日
    蘋果日報](http://hk.apple.nextmedia.com/entertainment/art/20070522/7126564)
2.  [陳小韻做全職媽咪湊仔 2011年10月25日
    明報](http://ol.mingpao.com/cfm/star5.cfm?File=20111025/saa01/mcc1.txt)