**七鳃鳗科**（學名：Petromyzontidae），又名**八目鳗**、**七星子**，是[圓口綱](../Page/圓口綱.md "wikilink")[七鰓鰻亞綱](../Page/七鰓鰻亞綱.md "wikilink")[七鳃鳗目的一個科](../Page/七鳃鳗目.md "wikilink")，其下生物是至今少数仅存的[无颌类脊椎鱼形动物之一](../Page/無頷總綱.md "wikilink")。最古老的七鳃鳗化石，有3.6亿年历史，在[恐龙出现之前](../Page/恐龙.md "wikilink")。所以七鳃鳗被称为“[活化石](../Page/活化石.md "wikilink")”，对于研究脊椎動物的演化有重要作用\[1\]。

## 與人類的關係

一种原来生活在[海洋裡](../Page/海洋.md "wikilink")\[2\]的東亞叉牙七鳃鳗被不小心带入[北美洲的](../Page/北美洲.md "wikilink")[五大湖之後](../Page/五大湖.md "wikilink")，成了[入侵物种](../Page/入侵物种.md "wikilink")。由于成年東亞叉牙七鳃鳗靠吸食其它鱼类的\[3\]\[4\]血而存活，他们的入侵对五大湖的渔业造成了很大损失，受害尤重的是[湖红点鲑](../Page/湖红点鲑.md "wikilink")（*Salvelinus
namaycush*）、[貝加爾白鮭](../Page/貝加爾白鮭.md "wikilink")（*Coregonus
migratorius*）。

歐洲七鰓鰻亦是一种美食，[欧洲的上流社会从](../Page/欧洲.md "wikilink")[中世纪开始就视其为珍馐](../Page/中世纪.md "wikilink")。文献记载[英格兰国王亨利一世爱吃歐洲七鰓鰻](../Page/亨利一世_\(英格兰\).md "wikilink")，结果有一次在[诺曼底吃了太多的歐洲七鰓鰻后死去](../Page/诺曼底.md "wikilink")。直到今天，在[南欧的一些国家](../Page/南欧.md "wikilink")（[法国](../Page/法国.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙等](../Page/葡萄牙.md "wikilink")），歐洲七鰓鰻仍然是一道昂贵的名菜。由于过度捕捞，欧洲的七鳃鳗数量一直在减少。[韩国人同样也有食用](../Page/韩国.md "wikilink")[七鳃鳗](../Page/東亞叉牙七鰓鰻.md "wikilink")。

## 影視形象

2014年美國恐怖電影《[血湖-殺人七鰓鰻的攻擊](../Page/血湖-殺人七鰓鰻的攻擊.md "wikilink")》（）以七鳃鳗為題材。

## 参考资料

[七鰓鰻科](../Category/七鰓鰻科.md "wikilink")
[LF](../Category/入侵魚種.md "wikilink")
[LF](../Category/北極淡水魚.md "wikilink")
[Category:寄生動物](../Category/寄生動物.md "wikilink")

1.  [科学家在南非发现最古老鱼化石](http://news.xinhuanet.com/collection/2006-10/30/content_5265371.htm)
2.  Silva, S., Servia, M. J., Vieira-Lanero, R., Barca, S. & Cobo, F.
    (2013). Life cycle of the sea lamprey Petromyzon marinus: duration
    of and growth in the marine life stage. Aquatic Biology 18: 59–62.
    doi: 10.3354/ab00488.
3.  Silva, S., Servia, M. J., Vieira-Lanero, R. & Cobo, F. (2013).
    Downstream migration and hematophagous feeding of newly
    metamorphosed sea lampreys (Petromyzon marinus Linnaeus, 1758).
    Hydrobiologia 700: 277–286. Doi: 10.1007/s10750-012-1237-3.
4.  Silva, S., Servia, M. J., Vieira-Lanero, R., Nachón, D. J. & Cobo,
    F. (2013). Haematophagous feeding of newly metamorphosed European
    sea lampreys Petromyzon marinus on strictly freshwater species.
    Journal of Fish Biology. <doi:10.1111/jfb.12100>.