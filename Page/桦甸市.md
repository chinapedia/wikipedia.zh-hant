**桦甸市**是[吉林省](../Page/吉林省.md "wikilink")[吉林市下辖的一个县级市](../Page/吉林市.md "wikilink")。位于吉林省中部。县北有桦皮甸子，故名。

## 历史

[唐代时属](../Page/唐.md "wikilink")[渤海国领土](../Page/渤海国.md "wikilink")。近代因发现这里出产[黄金](../Page/黄金.md "wikilink")，而引来淘金者，并促使了城市的发展。[清](../Page/清.md "wikilink")[光绪年间始置县](../Page/光绪.md "wikilink")。1988年撤县设市，现隶属于地级[吉林市](../Page/吉林市.md "wikilink")。

這里也是[金日成](../Page/金日成.md "wikilink")[打倒帝國主義同盟的建立地](../Page/打倒帝國主義同盟.md "wikilink")。

## 资源

桦甸地处[长白山地中部](../Page/长白山.md "wikilink")、[松花江上游](../Page/松花江.md "wikilink")，建有东北最大的[水电站](../Page/水电站.md "wikilink")——[白山水电站](../Page/白山水电站.md "wikilink")。农业经济以[玉米种植和](../Page/玉米.md "wikilink")[森林采伐为主](../Page/森林.md "wikilink")，是吉林省重要的粮食和木材生产基地。还出产[人参](../Page/人参.md "wikilink")、“[中国林蛙](../Page/中国林蛙.md "wikilink")”油等。

境内[黄金保有储量较大](../Page/黄金.md "wikilink")，位居中国前十位。“[夹皮沟](../Page/夹皮沟.md "wikilink")”金矿在中国近现代历史上颇为有名。

## 行政区划

下辖5个街道、8个镇、8个乡：\[1\] 。

## 参考资料

[桦甸市](../Category/桦甸市.md "wikilink")
[市](../Category/吉林市县级行政区.md "wikilink")
[吉林市](../Category/吉林县级市.md "wikilink")

1.