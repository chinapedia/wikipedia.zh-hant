**林建良**（），[台灣](../Page/台灣.md "wikilink")[醫師](../Page/醫師.md "wikilink")、[評論家及](../Page/評論家.md "wikilink")[台灣獨立運動者](../Page/台灣獨立運動.md "wikilink")，旅居[日本](../Page/日本.md "wikilink")，曾任[在日台灣同鄉會會長](../Page/在日台灣同鄉會.md "wikilink")、在日台灣同鄉會顧問、[電子報](../Page/電子報.md "wikilink")《[台灣之聲](../Page/台灣之聲.md "wikilink")》（）總編輯、電子報《[日本之聲](../Page/日本之聲.md "wikilink")》（）總編輯、[台灣獨立建國聯盟日本本部國際部長](../Page/台灣獨立建國聯盟.md "wikilink")、[日本李登輝之友會常務理事](../Page/日本李登輝之友會.md "wikilink")、[世界台灣同鄉會副會長](../Page/世界台灣同鄉會.md "wikilink")。現居住日本[栃木縣](../Page/栃木縣.md "wikilink")，從事[醫生工作並熱心於台灣獨立運動](../Page/醫生.md "wikilink")。

林建良出生於台灣[台中市](../Page/台中市.md "wikilink")，1987年成為日本[交流協會](../Page/交流協會.md "wikilink")[獎學金](../Page/獎學金.md "wikilink")[學生而來到日本](../Page/學生.md "wikilink")，此後修完[東京大學](../Page/東京大學.md "wikilink")[醫學院](../Page/醫學院.md "wikilink")[博士課程](../Page/博士.md "wikilink")。2001年，林建良擔任[台灣團結聯盟日本後援會會長](../Page/台灣團結聯盟.md "wikilink")。2001年11月22日，林建良以台灣團結聯盟日本後援會會長身分發表聲明〈為什麼我們支持「台灣團結聯盟」〉，表態全力支持台灣團結聯盟的候選人\[1\]。

2008年1月15日，林建良評論[2008年中華民國立法委員選舉結果](../Page/2008年中華民國立法委員選舉.md "wikilink")，認為[民主進步黨的敗選不是如](../Page/民主進步黨.md "wikilink")《[產經新聞](../Page/產經新聞.md "wikilink")》所說「台灣人選擇了[經濟](../Page/經濟.md "wikilink")，不要[台灣意識](../Page/台灣意識.md "wikilink")」，而是因為「民進黨只把台灣意識當成選舉工具，侮蔑了台灣意識\[2\]」。2008年4月10日，林建良評論[2008年中華民國總統選舉結果](../Page/2008年中華民國總統選舉.md "wikilink")，認為，更直指當時喊出[四要一沒有](../Page/四要一沒有.md "wikilink")、標榜支持台灣獨立建國的民進黨籍[中華民國總統](../Page/中華民國總統.md "wikilink")[陳水扁是](../Page/陳水扁.md "wikilink")\[3\]。

## 著作

  - 《母親ê名叫台灣：「正名運動」緣由》（[一橋出版社](../Page/一橋出版社.md "wikilink")2003年9月17日初版，ISBN
    957-8251-48-3）
  - 《日本啊，這是你所了解的中國嗎？―台灣人醫師的直言》（）（[並木書房](../Page/並木書房.md "wikilink")2006年7月初版，ISBN
    4-89063-201-8）
  - 《中國的目標是民族滅絕：西藏、維吾爾、蒙古、台灣，爭自由之戰》（）（與Tenzing、Ilham Mahmut、Dash
    Donorob合著，2009年3月初版，ISBN 978-4-944235-45-2）
  - 《中國癌：台灣人醫師的處方箋》（）（並木書房2012年12月初版，ISBN 978-4-89063-300-5）

## 參考文獻和注釋

## 外部連結

  - [電子報《台灣之聲》首頁](https://web.archive.org/web/20090228182737/http://www.emaga.com/info/3407.html)（2001年6月22日創刊）
  - [電子報《日本之聲》部落格](http://nihonnokoe.blogspot.tw/)

[分類:在日本的台灣人](../Page/分類:在日本的台灣人.md "wikilink")

[L林](../Category/日本醫生.md "wikilink")
[L林](../Category/日本政治評論家.md "wikilink")
[L林](../Category/臺灣獨立運動者.md "wikilink")
[L林](../Category/台中市人.md "wikilink")
[L林](../Category/東京大學校友.md "wikilink")
[J建](../Category/林姓.md "wikilink")

1.
2.
3.