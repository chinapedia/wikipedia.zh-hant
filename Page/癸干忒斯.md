[Pergamonaltar-Gigantomachie-Hektate_contra_Klytios.jpg](https://zh.wikipedia.org/wiki/File:Pergamonaltar-Gigantomachie-Hektate_contra_Klytios.jpg "fig:Pergamonaltar-Gigantomachie-Hektate_contra_Klytios.jpg")
[Dionysos_Giant_Louvre_G434.jpg](https://zh.wikipedia.org/wiki/File:Dionysos_Giant_Louvre_G434.jpg "fig:Dionysos_Giant_Louvre_G434.jpg")
**癸干忒斯**（[希腊语](../Page/希腊语.md "wikilink")：Γίγαντες，也可译为**巨灵**）[希腊神话中身形雄伟](../Page/希腊神话.md "wikilink")、力大无穷的[巨人族](../Page/巨人.md "wikilink")。癸干忒斯与奥林帕斯神之间的战争，即所谓[癸干忒斯战争](../Page/癸干忒斯战争.md "wikilink")（Gigantomachy），是希腊神话中继[泰坦战争之后最伟大的神祇战争](../Page/泰坦战争.md "wikilink")。

癸干忒斯共有150人，是大地女神[盖亚与天神](../Page/盖亚.md "wikilink")[乌拉诺斯的孩子](../Page/乌拉诺斯.md "wikilink")。盖亚与乌拉诺斯先后生育了12个[提坦](../Page/提坦.md "wikilink")、3个[独眼巨人和](../Page/独眼巨人.md "wikilink")3个[百臂巨人](../Page/百臂巨人.md "wikilink")；乌拉诺斯无休止的[生殖使盖亚非常痛苦](../Page/生殖.md "wikilink")，于是她怂恿提坦去[阉割他们的父亲](../Page/阉割.md "wikilink")。只有最小的泰坦[克罗诺斯敢去做这件事](../Page/克罗诺斯.md "wikilink")，他用一把[大镰刀阉割了乌拉诺斯](../Page/镰刀.md "wikilink")。乌拉诺斯喷出的鲜血被盖亚吸收，后者因此生下了癸干忒斯。\[1\]

在古希腊神话集《[书库](../Page/书库.md "wikilink")》（传统上认为此书为[阿波洛多罗斯所作](../Page/阿波洛多罗斯.md "wikilink")，但现代学者大多已否定此说）中详细地描述了癸干忒斯的出生。癸干忒斯出生于[帕利尼半岛](../Page/帕利尼半岛.md "wikilink")（现名卡桑德拉半岛），有人的上身和龙的尾巴，相貌极其可怕。

命运决定癸干忒斯最终将死于与奥林帕斯眾神之间的战争，当人类英雄前来帮助众神战斗时，就是癸干忒斯战死之时。盖亚知道这件事，于是就寻找一种不死仙草，希望救癸干忒斯的命；但是[宙斯提前割走了仙草](../Page/宙斯.md "wikilink")，破坏了盖亚的计划。

后来癸干忒斯果然起来反抗宙斯的统治，这就是癸干忒斯战争。癸干忒斯们的领袖是[阿尔库俄纽斯和](../Page/阿尔库俄纽斯.md "wikilink")[波耳费里翁](../Page/波耳费里翁.md "wikilink")；在这两个的领导下，他们企图把[色萨利山](../Page/色萨利山.md "wikilink")、[珀利翁山和](../Page/珀利翁山.md "wikilink")[俄萨山疊在一起](../Page/俄萨山.md "wikilink")，好爬到[奥林波斯山上去](../Page/奥林波斯山.md "wikilink")。诸神为了迎战，把独眼巨人和百臂巨人都找来帮忙。按照[雅典娜的建议](../Page/雅典娜.md "wikilink")，宙斯召来了他的儿子、凡人英雄[赫拉克勒斯](../Page/赫拉克勒斯.md "wikilink")，因为[神谕显示必须有他参与才能战胜癸干忒斯](../Page/神谕.md "wikilink")。

这场战斗极其惨烈，其规模宏大可与泰坦战争相比。战场在佛勒格拉平原（这个地名的意思是“火场”）。癸干忒斯们用巨大的石头和着火的林木向奥林帕斯山上砸去，众神则用他们强大的武器作战。由于赫拉克勒斯的帮助，奥林波斯神取得了胜利，所有癸干忒斯都被消灭。《书库》里提到了13个被杀的癸干忒斯的名字：

  - [阿尔库俄纽斯](../Page/阿尔库俄纽斯.md "wikilink")，被赫拉克勒斯所杀；
  - 波耳费里翁，被宙斯用雷电击死；
  - 厄菲阿尔忒斯，被[阿波罗用箭射死](../Page/阿波罗.md "wikilink")；
  - 欧律托斯，被[狄俄倪索斯所杀](../Page/狄俄倪索斯.md "wikilink")；
  - 克吕提厄，被[赫卡忒用火炬烧死](../Page/赫卡忒.md "wikilink")（一说是提堤俄斯被[勒托用火炬烧死](../Page/勒托.md "wikilink")）；
  - 弥马斯，被[赫淮斯托斯所杀](../Page/赫淮斯托斯.md "wikilink")；
  - [恩克拉多斯](../Page/恩克拉多斯.md "wikilink")，被雅典娜用[西西里岛砸死](../Page/西西里岛.md "wikilink")；
  - [帕拉斯](../Page/帕拉斯_\(癸干忒斯\).md "wikilink")，雅典娜活剥了他的皮；
  - 玻吕玻忒斯，被[波塞冬所杀](../Page/波塞冬.md "wikilink")；
  - 希波吕托斯，被[赫耳墨斯所杀](../Page/赫耳墨斯.md "wikilink")；
  - 格拉提翁，被[阿耳忒弥斯所杀](../Page/阿耳忒弥斯.md "wikilink")；
  - 阿格里俄斯，被[摩伊赖所杀](../Page/摩伊赖.md "wikilink")；
  - 托翁，被摩伊赖杀死。

其他的癸干忒斯都被宙斯用雷击死，然后赫拉克勒斯又用箭射死了那些奄奄一息者。\[2\]

古代居住于[意大利南部](../Page/意大利.md "wikilink")（所谓[大希腊](../Page/大希腊.md "wikilink")）的希腊移民相信，战死的癸干忒斯被众神埋葬在[火山下面](../Page/火山.md "wikilink")，因此引发了火山喷发和其它地热现象。

在古代人们经常把癸干忒斯反抗宙斯的叛乱理解为“泰坦的复仇”，因为如果癸干忒斯获胜，其结果将很类似于泰坦复辟。但是没有任何神话学的证据支持这种联系。不过有的[苏联学者认为](../Page/苏联.md "wikilink")，泰坦战争的神话可能影响了癸干忒斯神话的形成\[3\]。

癸干忒斯战争是古希腊人最热衷的艺术题材之一，许多希腊瓶画都以其为主题。描述癸干忒斯战争的最著名的艺术品是[帕加马大祭坛](../Page/帕加马大祭坛.md "wikilink")，今天人们可以在[柏林](../Page/柏林.md "wikilink")[帕加马博物馆中观赏到该祭坛上表现这场战争的宏伟浮雕](../Page/帕加马博物馆.md "wikilink")。

关于癸干忒斯，有另一种不同的说法值得注意。[荷马在](../Page/荷马.md "wikilink")[奥德赛中提到](../Page/奥德赛.md "wikilink")，癸干忒斯是一个野蛮民族，他们的国王叫欧律墨冬，最终被全部消灭。\[4\]

## 大眾文化

  - 手機遊戲《[龍族拼圖](../Page/龍族拼圖.md "wikilink")》中的怪物

<!-- end list -->

  - 小說《[混血營英雄](../Page/混血營英雄.md "wikilink")》中的敵人

## 注释

<references/>

## 资料来源

  - 《神话辞典》，（[苏联](../Page/苏联.md "wikilink")）M·H·鲍特文尼克等，统一书号：2017·304
  - 《世界神话辞典》，辽宁人民出版社，ISBN 7-205-00960-X
  - 《希腊宗教》，Walter Burkert，128页
  - [The Gigantes in classical literature and
    art](http://www.theoi.com/Gigante/Gigantes.html)
  - [Harpers Dictionary of Classical
    Antiquities](http://www.perseus.tufts.edu/cgi-bin/ptext?doc=Perseus%3Atext%3A1999.04.0062%3Aid%3Dgigantes):
    "Gigantes"

[G](../Category/希腊神话巨人.md "wikilink")
[G](../Category/希腊神话生物.md "wikilink")

1.  [赫西俄德](../Page/赫西俄德.md "wikilink")，《[神谱](../Page/神谱.md "wikilink")》
2.  [伪阿波罗多洛斯](../Page/伪阿波罗多洛斯.md "wikilink")，《[书库](../Page/书库.md "wikilink")》，I
3.  《神话辞典》，巨灵条
4.  荷马，《奥德赛》，VII