**李奎翰**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。

## 演出作品

### 電視劇

  - 1998年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[愛情與成功](../Page/愛情與成功.md "wikilink")》
  - 1999年：MBC《[青春](../Page/青春_\(韓國電視劇\).md "wikilink")》
  - 2000年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[KAIST](../Page/KAIST_\(電視劇\).md "wikilink")》
  - 2000年：MBC《[熱情似火](../Page/熱情似火_\(韓國電視劇\).md "wikilink")》
  - 2005年：MBC《[我的名字叫金三順](../Page/我的名字叫金三順.md "wikilink")》飾 閔賢佑
  - 2005年：SBS《[愛需要奇蹟](../Page/愛需要奇蹟.md "wikilink")》飾 甄正彪
  - 2007年：MBC《[順其自然](../Page/順其自然_\(電視劇\).md "wikilink")》飾 申洙亨
  - 2007年：MBC《[魂](../Page/魂_\(電視劇\).md "wikilink")》飾 徐俊熙
  - 2009年：SBS《[你笑了](../Page/你笑了.md "wikilink")》飾 李翰世
  - 2010年：MBC《[越看越可愛](../Page/越看越可愛.md "wikilink")》飾 李奎翰
  - 2011年：MBC《[是否聽見我的心](../Page/是否聽見我的心.md "wikilink")》飾 李勝哲
  - 2011年：SBS《[如果明日來臨](../Page/如果明日來臨.md "wikilink")》飾 李日奉
  - 2012年：[tvN](../Page/tvN.md "wikilink")《[结婚的策略](../Page/结婚的策略.md "wikilink")》飾
    李江在
  - 2012年：SBS《[家族的誕生](../Page/家族的誕生_\(電視劇\).md "wikilink")》飾演 姜允宰
  - 2014年：SBS《[就要相愛](../Page/就要相愛.md "wikilink")》飾演 崔載敏
  - 2015年：SBS《[我有愛人了](../Page/我有愛人了.md "wikilink")》飾演 白石
  - 2017年：tvN《[內向的老闆](../Page/內向的老闆.md "wikilink")》飾演 禹記者
  - 2017年：tvN《[沒禮貌的英愛小姐16](../Page/沒禮貌的英愛小姐16.md "wikilink")》飾演 李奎翰
  - 2018年：MBC《[富家公子](../Page/富家公子.md "wikilink")》飾演 太日
  - 2019年：tvN《[成為王的男人](../Page/成為王的男人.md "wikilink")》飾演朱虎乬
  - 2019年：tvN《[改變已經開始了](../Page/改變已經開始了.md "wikilink")》

### 電影

  - 2006年：《太陽的兩面》飾演 秀賢
  - 2007年：《孟婆島2》飾演 全基榮
  - 2011年：《白色：詛咒的旋律》飾演 崔Spawn
  - 2013年：《[共犯](../Page/共犯_\(2013年電影\).md "wikilink")》飾演 金在慶
  - 2018年：《[似曾相識](../Page/似曾相識_\(韓國電影\).md "wikilink")》

### 綜藝

  - 2015年：[KBS](../Page/韓國放送公社.md "wikilink")《[我們小區藝體能](../Page/我們小區藝體能.md "wikilink")》
  - 2015年：[JTBC](../Page/JTBC.md "wikilink")《[我去上學啦](../Page/我去上學啦.md "wikilink")》
  - 2015年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[我獨自生活](../Page/我獨自生活.md "wikilink")》
  - 2015年：MBC《[真正的男人](../Page/真正的男人.md "wikilink")2》
  - 2015年：JTBC《[拜託了冰箱](../Page/拜託了冰箱.md "wikilink")》
  - 2016年：[SBS](../Page/SBS株式會社.md "wikilink")《[Running
    Man](../Page/Running_Man.md "wikilink")》
  - 2016-2017年：[SBS](../Page/SBS.md "wikilink")《[Scene
    Stealer-戲劇戰爭](../Page/Scene_Stealer.md "wikilink")》
  - 2017年：[tvN](../Page/TVN.md "wikilink")《[家常飯白老師3](../Page/家常飯白老師.md "wikilink")》
  - 2017年：MBC《[哥哥的想法](../Page/哥哥的想法.md "wikilink")》
  - 2017年：tvN《[島劍客](../Page/島劍客.md "wikilink")》

### MV

  - 2004年：Flower《這裡》
  - 2004年：[金鐘國](../Page/金鐘國.md "wikilink")《中毒》
  - 2009年：Someday《不能像男人》（與[全慧彬](../Page/全慧彬.md "wikilink")）

## 獲獎

  - 2005年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－New Star賞《愛需要奇蹟》
  - 2010年：[MBC演藝大賞](../Page/MBC演藝大賞.md "wikilink")－喜劇部門 男子優秀賞《越看越可愛》

## 外部連結

  - [經紀公司Namoo Actors個人網頁](http://www.namooactors.com/star/leegyuhan)

[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/國民大學校友.md "wikilink")
[L](../Category/李姓.md "wikilink")