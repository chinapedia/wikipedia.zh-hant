[Hong_Kong_Railway_Museum_Overview2_201208.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Railway_Museum_Overview2_201208.jpg "fig:Hong_Kong_Railway_Museum_Overview2_201208.jpg")
[Hong_Kong_Railway_Museum,_Ficus_microcarpa_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Railway_Museum,_Ficus_microcarpa_\(Hong_Kong\).jpg "fig:Hong_Kong_Railway_Museum,_Ficus_microcarpa_(Hong_Kong).jpg")
**香港鐵路博物館**（），位於[香港](../Page/香港.md "wikilink")[新界東](../Page/新界東.md "wikilink")[大埔墟市中心](../Page/大埔墟.md "wikilink")，是[香港文化博物館轄下](../Page/香港文化博物館.md "wikilink")[分館](../Page/博物館.md "wikilink")，有着風格獨特金字頂型的[中國傳統建築](../Page/中國傳統建築.md "wikilink")，專門介紹[香港鐵路運輸歷史](../Page/香港鐵路運輸.md "wikilink")。現時由[康樂及文化事務署管理](../Page/康樂及文化事務署.md "wikilink")。

## 歷史

該館前身為舊**[大埔墟火車站](../Page/大埔墟火車站.md "wikilink")**，於1913年建成，以往是[九廣鐵路重要車站之一](../Page/九廣鐵路.md "wikilink")。但由於九鐵於1982至84年引進電氣化火車，新[大埔墟火車站於](../Page/大埔墟火車站.md "wikilink")1983年啟用，因此舊大埔墟火車站於同年4月6日關閉。其後該火車站原址於1984年被列為[法定古蹟](../Page/香港法定古蹟.md "wikilink")，並改建成博物館，相關工程包括在車站周邊加建路軌連接[東鐵線](../Page/東鐵線.md "wikilink")（以便將退役車卡經鐵路運至本館入藏，但目前已再被拆除），並對原車站建築進行復修及翻新工作。1986年12月20日由[區域市政局主席](../Page/區域市政局.md "wikilink")[張人龍](../Page/張人龍.md "wikilink")、九廣鐵路公司主席[霍士傑](../Page/霍士傑.md "wikilink")、[區域市政總署署長](../Page/區域市政總署.md "wikilink")[孫明揚等主持](../Page/孫明揚.md "wikilink")，21日起開放予市民參觀。博物館專門介紹九廣鐵路歷史。由於建築、藏品有一定的歷史和觀賞價值，故該館經常成為香港影視工作者的取景地。隨著[兩鐵合併](../Page/兩鐵合併.md "wikilink")，館內展覽廳於2010年新增了[香港地鐵的簡史](../Page/香港地鐵.md "wikilink")，緊貼香港鐵路的急速發展，展出一系列文物和歷史圖片，重塑昔日鐵路的軌跡，和透過不同時期的鐵路標誌、新支線概況等互動展品，帶領參觀人士穿梭鐵路今昔。

## 設施及藏品

博物館中保留了6輛歷史車卡、手動及機動工程車各一輛。在1997年新增了[沙頭角支線曾使用的窄軌蒸氣機火車頭](../Page/沙頭角支線.md "wikilink")，並於2004年新增了九鐵於1955年引進的首台柴油機車——名為[亞歷山大爵士號的](../Page/G12型柴油機車.md "wikilink")51號機車。除此之外，博物館擺放了不少當年使用過的工具、火車模型、圖片歷史介紹以及曾使用的信號系統等。

至於原有的電氣化樣版火車頭，早年曾開放對外參觀。但到了1990年代時改為教學室，不再對外開放。電氣化火車頭最終在於2004年拆卸，騰出空間給[亞歷山大爵士號的](../Page/G12型柴油機車.md "wikilink")51號機車。

<File:Old> Tai Po Market Station banner.JPG|原「大埔墟火車站」站牌 <File:51> SIR
ALEXANDER.JPG|亞歷山大（葛量洪）爵士號 <File:KCR> NGS
train.JPG|已廢棄的[沙頭角支線](../Page/沙頭角支線.md "wikilink")（窄軌）所使用的蒸汽火車頭
<File:Hong> Kong Railway Museum Train Office 2012.jpg|舊大埔墟車站售票處
<File:Hong> Kong Railway Museum Exhibit 2012.jpg|館內展覽廳於2010年新增了香港地鐵的簡史
<File:RM> SSL.JPG|[號誌臂控制桿](../Page/鐵路訊號.md "wikilink") <File:RM>
train223.JPG|歷史車箱223號 <File:RM> mobile repair.JPG|機動四輪車 <File:RM> hand
move.JPG|手搖四輪車 <File:Hong> Kong Railway Museum 3rd class train
2012.jpg|1911年的普通等車廂車廂內部 <File:Hong> Kong Railway Museum an
ordinary-class compartment of 1974.jpg|1974年的普通等車廂內部 <File:Hong> Kong
Railway Museum a first-class compartment of 1964.jpg|1964年的頭等車廂內部

## 外部連結

  - [香港鐵路博物館](http://www.heritagemuseum.gov.hk/zh_TW/web/hm/museums/railway.html)
  - [2008年11月：香港鐵路博物館的影像](http://www.youtube.com/watch?gl=HK&hl=zh-HK&v=m6xj3kl9ZPk)
  - [中国の鉄道トピックス：香港鉄路博物館（日文）](http://www.2427junction.com/chinareporthr.html)
  - [鐵道之路：香港鐵路博物館（日文）](http://www.kurogane-rail.jp/hkrm/hkrm-idx.html)
  - [大埔墟香港鐵路博物館 | Hong Kong Railway Museum, Tai Po
    Market](https://www.youtube.com/watch?v=TXXB3ywgKm0)

[Category:東鐵綫](../Category/東鐵綫.md "wikilink") [Category:大埔
(香港)](../Category/大埔_\(香港\).md "wikilink")
[Category:中国铁路博物馆](../Category/中国铁路博物馆.md "wikilink")
[Category:香港中式建築](../Category/香港中式建築.md "wikilink")
[Category:香港鐵路廢站](../Category/香港鐵路廢站.md "wikilink")
[Category:香港公共交通博物館](../Category/香港公共交通博物館.md "wikilink")
[Category:1986年建立](../Category/1986年建立.md "wikilink")