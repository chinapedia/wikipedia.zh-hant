**太安**（或作**大安**；302年十二月－303年）是[西晋皇帝](../Page/西晋.md "wikilink")[晋惠帝司马衷的第六个](../Page/晋惠帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计兩年。

## 纪年

| 太安                                                            | 元年                                       | 二年                             |
| ------------------------------------------------------------- | ---------------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink")                              | 302年                                     | 303年                           |
| [干支](../Page/干支纪年.md "wikilink")                              | [壬戌](../Page/壬戌.md "wikilink")           | [癸亥](../Page/癸亥.md "wikilink") |
| [刘尼](../Page/刘尼.md "wikilink")、[张昌](../Page/张昌.md "wikilink") | |[神凤元年](../Page/神凤_\(张昌\).md "wikilink") |                                |
| [成漢](../Page/成漢.md "wikilink")                                | |[建初元年](../Page/建初_\(李特\).md "wikilink") |                                |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[太安年号](../Page/太安.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129

[Category:西晋年号](../Category/西晋年号.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:300年代中国政治](../Category/300年代中国政治.md "wikilink")
[Category:302年](../Category/302年.md "wikilink")
[Category:303年](../Category/303年.md "wikilink")
[Category:有爭議的年號](../Category/有爭議的年號.md "wikilink")