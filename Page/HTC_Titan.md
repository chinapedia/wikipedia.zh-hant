**HTC Titan**，原廠型號**HTC
P4000**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
6.0，配備[Qualcomm](../Page/Qualcomm.md "wikilink") MSM7500
400MHz處理器，配有側滑動式[QWERTY鍵盤](../Page/QWERTY.md "wikilink")，性能強大。2007年6月於北美首度發表。已知客製版本HTC
P4000，Telus＆HTC P4000，Sprint＆HTC Mogul，Qwest＆HTC Mogul，Alltel＆HTC
PPC6800，Bell＆HTC PPC6800，UTStarcom PPC6800，UTStarcom＆Verizon
XV6800，Telecom New Zealand＆HTC Titan。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7500 400MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：64MB
  - 尺寸：110mm X 59mm X 18.5mm
  - 重量：150g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：CDMA2000 1x RTT/CDMA2000 1x EV-DO
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，不支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1560mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC P4000
    概觀](https://web.archive.org/web/20081115011603/http://www.htc.com/us/product.aspx?id=33626)
  - [HTC P4000
    技術規格](https://web.archive.org/web/20081204103853/http://www.htc.com/us/product.aspx?id=33630)

[H](../Category/智能手機.md "wikilink")
[Titan](../Category/宏達電手機.md "wikilink")