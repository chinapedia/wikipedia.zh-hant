**Windows
2.0**是一套基于[MS-DOS](../Page/MS-DOS.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，[微軟于](../Page/微軟.md "wikilink")1987年發行，類似[Mac
OS](../Page/Mac_OS.md "wikilink")[图形用户界面的](../Page/图形用户界面.md "wikilink")[Windows版本](../Page/Windows.md "wikilink")。这个[用户界面較](../Page/用户界面.md "wikilink")[Windows
1.0有著更多的功能](../Page/Windows_1.0.md "wikilink")，且已近似[Windows
3.0](../Page/Windows_3.x.md "wikilink")。它已有一些的改进，而紧接着它就是更受欢迎的[3.0版本但此版本的支援週期長達](../Page/Windows_3.x.md "wikilink")14年，由1987年支援到2001年最後一天為止。

## 参見

  - [Microsoft Windows的历史](../Page/Microsoft_Windows的历史.md "wikilink")
  - [微軟](../Page/微軟.md "wikilink")
  - [操作系統](../Page/操作系統.md "wikilink")
  - [微軟操作系統列表](../Page/微軟操作系統列表.md "wikilink")
  - [操作系統列表](../Page/操作系統列表.md "wikilink")

[Category:磁盘操作系统](../Category/磁盘操作系统.md "wikilink") [Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")