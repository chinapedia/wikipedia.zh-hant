border |insignia_size = 145px |insigniacaption = |image = Miguel Diaz
Canel.jpg |imagesize = 165px |incumbent =
[米格尔·迪亚斯-卡内尔](../Page/米格尔·迪亚斯-卡内尔.md "wikilink")
|incumbentsince = 2018年4月19日 |department =
[古巴国务委员会](../Page/古巴国务委员会.md "wikilink")
|style =
[主席](../Page/主席.md "wikilink")[同志](../Page/革命同志.md "wikilink")；[阁下](../Page/阁下.md "wikilink")
|type = 集体[国家元首代表](../Page/国家元首.md "wikilink") |residence =
[哈瓦那](../Page/哈瓦那.md "wikilink") |appointer =
[全国人民政权代表大会](../Page/全国人民政权代表大会.md "wikilink")
|termlength = 5年，可连任1次 |precursor = [古巴总统](../Page/古巴总统.md "wikilink")
|formation = 1976年 |inaugural =
[菲德尔·卡斯特罗](../Page/菲德尔·卡斯特罗.md "wikilink")
|deputy = 国务委员会副主席 }}
**古巴國務委員會主席**（）是[全國人民政權代表大會常設機關](../Page/全國人民政權代表大會.md "wikilink")[國務委員會的領導人](../Page/全国人民政权代表大会.md "wikilink")，也是現時[古巴共和國的](../Page/古巴共和國.md "wikilink")[國家元首](../Page/國家元首.md "wikilink")，該國於1976年廢除[總統制](../Page/總統制.md "wikilink")，但仍有部分傳媒將職務名稱翻譯為「[古巴總統](../Page/古巴總統.md "wikilink")」。直至現在，三任主席均兼任[古巴部長會議主席](../Page/古巴部長會議主席.md "wikilink")（），同時行使[政府首腦](../Page/政府首腦.md "wikilink")（總理）的職權，兩個職務的任期同為一屆五年，由[全國人民政權代表大會選舉產生](../Page/全國人民政權代表大會.md "wikilink")。該職由[古巴共產黨最高負責人](../Page/古巴共產黨.md "wikilink")，[中央委員會第一書記兼任](../Page/古巴共產黨中央委員會第一書記.md "wikilink")。首任主席是[菲德尔·卡斯特罗](../Page/菲德尔·卡斯特罗.md "wikilink")，现任主席是[米格尔·迪亚斯-卡内尔](../Page/米格尔·迪亚斯-卡内尔.md "wikilink")，于2018年4月当选\[1\]。

## 历任国务委员会主席

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>肖像</p></th>
<th><p>姓名<br />
<small>(生–卒年)</p></th>
<th><p>所属政党</p></th>
<th><p>国籍</p></th>
<th><p>任期</p></th>
<th><p>第一副主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>古巴共和国国务委员会主席</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1</strong></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Fidel_Castro.jpg" title="fig:Fidel_Castro.jpg">Fidel_Castro.jpg</a></p></td>
<td><p><a href="../Page/菲德尔·卡斯特罗.md" title="wikilink">菲德尔·卡斯特罗</a><br />
<small>(1926–2016)</p></td>
<td></td>
<td></td>
<td><p>1976年12月3日– 2008年2月24日</p></td>
<td><p><a href="../Page/勞尔·卡斯特罗.md" title="wikilink">勞尔·卡斯特罗</a><br />
<a href="../Page/何塞·马查多.md" title="wikilink">何塞·马查多</a></p></td>
</tr>
<tr class="odd">
<td><p>—</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Raúl_Castro.JPG" title="fig:Raúl_Castro.JPG">Raúl_Castro.JPG</a></p></td>
<td><p><a href="../Page/勞尔·卡斯特罗.md" title="wikilink">勞尔·卡斯特罗</a><br />
<small>(1931– )</p></td>
<td><p>2006年7月31日– 2008年2月24日<br />
代理主席</p></td>
<td><p><a href="../Page/何塞·马查多.md" title="wikilink">何塞·马查多</a><br />
<a href="../Page/米格尔·迪亚斯-卡内尔.md" title="wikilink">米格尔·迪亚斯-卡内尔</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2</strong></p></td>
<td><p>2008年2月24日 – 2018年4月19日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>3</strong></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Miguel_Diaz_Canel.jpg" title="fig:Miguel_Diaz_Canel.jpg">Miguel_Diaz_Canel.jpg</a></p></td>
<td><p><a href="../Page/米格尔·迪亚斯-卡内尔.md" title="wikilink">米格尔·迪亚斯-卡内尔</a><br />
<small>(1960– )</p></td>
<td><p>2018年4月19日 -</p></td>
<td><p><a href="../Page/萨尔瓦多·瓦尔德斯·梅萨.md" title="wikilink">萨尔瓦多·瓦尔德斯·梅萨</a><br />
（ Salvador Valdés Mesa）</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注释

## 脚注

## 外部連結

  - [Cuba - WORLD
    STATESMEN.org](http://www.worldstatesmen.org/Cuba.html)

{{-}}

[古巴国务委员会主席](../Category/古巴国务委员会主席.md "wikilink")
[Category:古巴政治](../Category/古巴政治.md "wikilink")
[Category:領袖列表](../Category/領袖列表.md "wikilink")

1.