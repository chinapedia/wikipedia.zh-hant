**伊桑巴德·金德姆·布鲁内尔**（**Isambard Kingdom
Brunel**，，）是一名[英国工程师](../Page/英国.md "wikilink")，[皇家学会会员](../Page/皇家学会会员.md "wikilink")\[1\]\[2\]\[3\]。在2002年[英国广播公司举办的](../Page/英国广播公司.md "wikilink")“[最伟大的100名英国人](../Page/最伟大的100名英国人.md "wikilink")”评选中名列第二（仅次于[温斯顿·丘吉尔](../Page/温斯顿·丘吉尔.md "wikilink")）。他的贡献在于主持修建了[大西部鐵路](../Page/大西部鐵路.md "wikilink")、系列蒸汽轮船（包括第一艘螺旋桨横跨大西洋大蒸汽船）和众多的重要桥梁。他革命性地推动了公共交通、现代工程等领域。

## 生平

1820年，布鲁内尔前往法国。1822年，布鲁内尔和他的父亲一起工作。他的父亲也是一名伟大的工程师\[4\]\[5\]。

1830年，24岁的布鲁内尔成为英国皇家科学院成员。

经过了一百多年的时间，布鲁内尔设计的众多建筑物仍然正常使用。布鲁内尔的第一个工程项目，[泰晤士隧道](../Page/泰晤士隧道.md "wikilink")，现在成为[伦敦地上铁](../Page/伦敦地上铁.md "wikilink")[東倫敦線的一部分](../Page/東倫敦線.md "wikilink")。

布鲁内尔还研究了空气铁路，最终没有成功。布鲁内尔还建立了医院。

布鲁内尔的烟瘾很大。他1859年得了中风，并在53岁生日过后不久去世。

## 纪念

目前英国各地都有布鲁内尔的雕像。人民纪念这位伟大的工程师。
[Isambard_Kingdom_Brunel_-_Bronze_-_Temple_-_London.jpg](https://zh.wikipedia.org/wiki/File:Isambard_Kingdom_Brunel_-_Bronze_-_Temple_-_London.jpg "fig:Isambard_Kingdom_Brunel_-_Bronze_-_Temple_-_London.jpg")附近的布鲁内尔像\]\]
1957年成立布魯內爾技術學院（Brunel College of
Technology），教授特許工程師。1962年學校改稱布魯內爾先進技術學院（Brunel
College of Advanced
Technology）。1966年布魯內爾先進技術學院升格為[布魯內爾大學](../Page/布魯內爾大學.md "wikilink")\[6\]。

2006年，英国皇家造币厂为布鲁内尔铸造了2英镑的纪念币，纪念他诞生200周年。

在[2012年伦敦奥运会开幕典禮上](../Page/2012年夏季奧林匹克運動會.md "wikilink")，布鲁内尔再次“登场”，在第二章《绿色而愉悦的土地》中，扮演布鲁内尔的英国莎士比亚戏剧演员[肯尼斯·布萊纳](../Page/肯尼斯·布萊纳.md "wikilink")，朗诵了[莎士比亚喜剧](../Page/莎士比亚.md "wikilink")《[暴风雨](../Page/暴風雨_\(莎士比亞\).md "wikilink")》台词。他的第一句台词「不要怕，这岛上充满了各种声音」也被刻在现场悬挂的奥林匹克大钟上。

## 参考文献

  - Only the first of a planned three volumes was published

  -
  -
  - (This is Isambard Brunel Junior, IKB's son.)

  -
  -
  -
  -
  -
  -
  -
  -
  - (373 pages) Online at Internet Archive

  - {{ cite book

` | first = L. T. C. |last=Rolt`
` | title = Isambard Kingdom Brunel`
` | year = 1989`
` | origyear = 1957`
` | publisher = Prentice Hall Press`
` | isbn = 978-0-582-10744-1`
` | ref = harv`

}}

  -
  -
  -
## 伸延閱讀

  - Written by Brunel's son

  - Written by Brunel's granddaughter, it adds some family anecdotes and
    personal information over the previous volume

  - A technical presentation of Brunel's opus

  - A study of how early photography portrayed Victorian industry and
    engineering, including the celebrated picture of Brunel and the
    launching chains of the *Great Eastern*

  -
  -
  -
  -
  -
  -
## 外部連結

  -
  -
  - *[The Times](../Page/The_Times.md "wikilink")* 19 September 1859

  - [Brunel biography with additional
    images](http://www.designmuseum.org/design/isambard-kingdom-brunel)
    from the [Design Museum](../Page/Design_Museum.md "wikilink")

  -
  - [Brunel portal](http://www.ikbrunel.org.uk)

  -
  -
  -
[Category:英国工程師](../Category/英国工程師.md "wikilink")
[Category:桥梁工程师](../Category/桥梁工程师.md "wikilink")
[Category:亨利四世中学校友](../Category/亨利四世中学校友.md "wikilink")
[Category:法國裔英格蘭人](../Category/法國裔英格蘭人.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")

1.
2.
3.
4.
5.
6.