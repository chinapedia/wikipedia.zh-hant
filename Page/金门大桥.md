**金門大橋**（）是[美國](../Page/美國.md "wikilink")[三藩市的](../Page/旧金山.md "wikilink")[地標](../Page/地標.md "wikilink")。它跨越聯接[舊金山灣和](../Page/舊金山灣.md "wikilink")[太平洋的](../Page/太平洋.md "wikilink")[金門海峽](../Page/金门海峡_\(美国\).md "wikilink")，南端連接舊金山的北端，北端接通[加州的](../Page/加利福尼亞州.md "wikilink")[馬林縣](../Page/馬林縣_\(加利福尼亞州\).md "wikilink")。金門大橋的[橋墩跨距長](../Page/橋墩.md "wikilink")1280.2米，建成时曾是世界上跨距最大的[懸索橋](../Page/悬索桥.md "wikilink")，寬度27.5米，雙向共6條行車線，橋身呈褐紅色，金門大橋擁有世界第四高的[橋塔](../Page/橋塔.md "wikilink")，高達227.4米，全橋總長度是2737.4米。

## 歷史

金門大橋的最初的構想來源於[橋樑](../Page/橋.md "wikilink")[工程師](../Page/工程师.md "wikilink")。斯特勞斯在此前設計了400多座內陸的小型橋樑。他花了10多年時間遊說北[加州的居民](../Page/加利福尼亞州.md "wikilink")。這座橋的其他主要設計者包括決定其藝術造型和顏色的[艾爾文·莫羅](../Page/艾爾文·莫羅.md "wikilink")、合作進行複雜的[數學推算的工程師](../Page/数学.md "wikilink")[查爾斯·埃里斯](../Page/查爾斯·埃里斯.md "wikilink")、橋樑設計師[里昂·莫伊塞弗](../Page/里昂·莫伊塞弗.md "wikilink")。

大橋於1933年1月5日開始施工，1937年4月完工，同年5月27日對外開放予行人。斯特勞斯在南橋墩澆築[混凝土之前放入了一塊取自他的母校](../Page/混凝土.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[辛辛那提大學的磚頭](../Page/辛辛纳提大學.md "wikilink")。翌日，隨著[羅斯福總統在](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")[華盛頓按下一個電鈕](../Page/华盛顿哥伦比亚特区.md "wikilink")，該大橋正式對外開放予汽車使用。

三藩市地區的選民們以自己的[住宅](../Page/住宅.md "wikilink")、[農場和](../Page/农场.md "wikilink")[公司為抵押](../Page/公司.md "wikilink")，發行了最初的3500萬[美元的](../Page/美元.md "wikilink")[工程債券](../Page/工程.md "wikilink")。1977年，最後一筆債券被付清，其中3500萬美元的本金和3900萬美元的利息全來自過橋費的收入。

該橋施工時的一項獨特的設計是橋下有一個安全網。施工中有11人摔死，而19人則因安全網而獲救。這19人成為了一個「去地獄的半路俱樂部」（Halfway
to Hell Club）的榮譽會員。

1957年之前金門大橋是世界上最長的懸索橋，兩個橋墩在1964年之前擁有世界上懸索橋中最長的跨度。這兩個橋墩直到不久之前還是世界上最高的懸索橋橋墩。

## 建築美學

金門大橋橋身的[顏色為](../Page/顏色.md "wikilink")[國際橘](../Page/國際橘.md "wikilink")，因建築師艾爾文·莫羅認為此色既和周邊環境協調，又可使大橋在金門海峽常見的大[霧中顯得更醒目](../Page/霧.md "wikilink")。由於這座大橋新穎的結構和超凡脫俗的外觀，它被國際橋樑工程界廣泛認為是美的典範，更被美國建築工程師協會評為現代的世界奇跡之一。它也是世界上最上鏡的大橋之一。

金門大橋維護工作中，給橋身不斷塗刷油漆是其中一項內容。2011年5月開始首次進行整體重漆，仍然使用國際橘。預計將耗費四年完工。這是金門大橋開通近75年來首次重漆
金門大橋的維護工作還包括不斷的加固工作，在1989年底發生後，當局聘請專家對金門大橋的脆弱性進行了詳細評估，並制定了加固計劃，分三期工程實施，第二期加固工程已於2006年中完成。

Image:Ggb by night.jpg|金門大橋夜景。左方遠處是旧金山市市區。 Image:Ggb in fog.JPG|霧中的金門大橋。
Image:Lightmatter_Golden_gate_bridge.jpg|金門大橋全景 Image:Golden Gate
Bridge Front Traffic.jpeg|金門大橋北端

[Golden-Gate-Bridge.svg](https://zh.wikipedia.org/wiki/File:Golden-Gate-Bridge.svg "fig:Golden-Gate-Bridge.svg")

## 自殺

[The_Bridge_(August_2013).jpg](https://zh.wikipedia.org/wiki/File:The_Bridge_\(August_2013\).jpg "fig:The_Bridge_(August_2013).jpg")
金門大橋是世界上最著名的[自殺場所之一](../Page/自殺場所列表.md "wikilink")\[1\]\[2\]，當金門大橋上自殺的人數在1993年達到1000人之後便不再有正式統計。估計在2008年已達1300多人。在2005年之前的5年，平均每兩個星期就有一個人從大橋上跳下投海。從67米高的橋面上跳下4秒後，自殺者會以每小時120公里的速度衝落海面。截至2013年為止，共有33人自殺未死。\[3\]2005年3月11日，金門大橋管理會決定投資200萬美元，研究安裝護欄的可行性。但因为经济衰退，该计划的实施被延迟。

## 參考文獻

## 外部链接

  -
## 參見

  - [國際土木工程歷史古蹟](../Page/國際土木工程歷史古蹟.md "wikilink")
  - [舊金山-奧克蘭海灣大橋](../Page/舊金山-奧克蘭海灣大橋.md "wikilink")
  - [金門海峽 (美國)](../Page/金門海峽_\(美國\).md "wikilink")
  - [懸索橋](../Page/懸索橋.md "wikilink")
  - [世界大橋列表](../Page/世界大橋列表.md "wikilink")
  - [世界懸索橋列表](../Page/世界懸索橋列表.md "wikilink")

{{-}}

[Category:旧金山湾区桥梁](../Category/旧金山湾区桥梁.md "wikilink")
[Category:美国跨海大桥](../Category/美国跨海大桥.md "wikilink")
[Category:美国悬索桥](../Category/美国悬索桥.md "wikilink")
[Category:舊金山建築物](../Category/舊金山建築物.md "wikilink")
[Category:美國地標](../Category/美國地標.md "wikilink")
[Category:桁架橋](../Category/桁架橋.md "wikilink")
[Category:1937年完工橋梁](../Category/1937年完工橋梁.md "wikilink")
[Category:桥梁之最](../Category/桥梁之最.md "wikilink")
[Category:美国公路桥](../Category/美国公路桥.md "wikilink")

1.
2.
3.