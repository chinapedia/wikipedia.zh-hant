<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>浙江工业大学</strong><br />
<a href="https://zh.wikipedia.org/wiki/File:Symbol_of_ZJUT.png" title="fig:Symbol_of_ZJUT.png">Symbol_of_ZJUT.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/校训.md" title="wikilink">校训</a>：<strong>厚德健行</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>现任校长</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>学校类型</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>建校时间</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>所在地</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>学生数</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>教职工数</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>校园地理位置</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>校园面积</p></td>
</tr>
</tbody>
</table>

[缩略图](https://zh.wikipedia.org/wiki/File:Headquarters_ZJUT.jpg "fig:缩略图")
**浙江工业大学**，简称**浙工大**，是[浙江省内一所综合性公立大学](../Page/浙江省.md "wikilink")。学校强项是工程技术学科，尤其在[化学工程](../Page/化学工程.md "wikilink")、[机械工程](../Page/机械工程.md "wikilink")、[生物工程](../Page/生物工程.md "wikilink")、[制药工程等领域](../Page/制药工程.md "wikilink")，影响较大。浙工大与工业界企业界联系紧密，是[长江三角洲地区和浙江省内重要大学之一](../Page/长江三角洲.md "wikilink")。校名由[郭沫若题写](../Page/郭沫若.md "wikilink")。\[1\]

## 校史

浙江工业大学是一所综合性的浙江省属重点大学，创建于1953年，其前身可以追溯到1910年创立的浙江中等工业学堂，先后经历了杭州化工学校（1953年6月～1958年6月），浙江化工专科学校（1958年6月～1960年8月）、浙江化工学院（1960年2月～1980年10月）、浙江工学院（1978年2月～1993年2月）和浙江工业大学（1993年12月至今）等发展阶段，几易校址，数历分合。浙江省经济管理干部学院、杭州船舶工业学校、浙江建材工业学校分别于1994年、1997年和2001年并入浙江工业大学。学校目前已发展成为国内有一定影响力的综合性的教学研究型大学，综合实力稳居全国高校百强行列。2009年6月8日，浙江省人民政府和教育部签订共建协议，浙江工业大学进入省部共建高校行列。2013年5月，由学校牵头建设的长三角绿色制药协同创新中心入选国家2011计划，成为全国首批14家2011协同创新中心之一。2015年4月，浙江工业大学入选浙江省首批“省重点建设高校

## 校区

  - 朝晖校区（本部）
  - 屏峰校区
  - 之江校区（之江学院）
  - 柯桥校区（之江学院新校区）
  - 德清校区（建设中，尚未投入使用）

## 院部

[缩略图](https://zh.wikipedia.org/wiki/File:Teaching_building_ZJUT.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Sir_Run_Run_Shaw_Hall_ZJUT.jpg "fig:缩略图")
学校现设有二十七个学院一个部：

  - 化学工程学院
  - 材料科学与工程学院
  - 机械工程学院
  - 信息工程学院
  - 经贸管理学院
  - 建筑工程学院
  - 生物工程学院
  - 环境学院
  - 海洋学院
  - 人文学院
  - 药学院
  - 理学院
  - 法学院
  - 外国语学院
  - 设计艺术学院
  - 健行学院
  - 健行书院
  - 创业学院
  - 政治与公共管理学院
  - 计算机科学与技术（软件学院）
  - 国际学院
  - 教育科学与技术学院（职业技术教育学院）
  - 成人教育学院
  - 马克思主义学院
  - 绿色制药协同创新中心
  - 之江学院
  - 研究生院
  - 体育军训部

## 历任校长

  - 杭州化工学校

<!-- end list -->

  - [刘亚东](../Page/刘亚东.md "wikilink") 1954.3-1958.6

<!-- end list -->

  - 浙江化工专科学校

<!-- end list -->

  - 刘亚东 1958.6—1960.8

<!-- end list -->

  - 浙江化工学院

<!-- end list -->

  - [刘德甫](../Page/刘德甫.md "wikilink") 1960.2—1964.7
  - [李寿恒](../Page/李寿恒.md "wikilink") 1964.7—1967.7
  - [周学山](../Page/周学山.md "wikilink") 1978.4—1980

<!-- end list -->

  - 浙江工学院

<!-- end list -->

  - [李恩良](../Page/李恩良.md "wikilink") 1981.12—1983.12
  - [邓汉馨](../Page/邓汉馨.md "wikilink") 1983.12—1988.1
  - [洪起超](../Page/洪起超.md "wikilink") 1988.1—1993.2

<!-- end list -->

  - 浙江工业大学

<!-- end list -->

  - [洪起超](../Page/洪起超.md "wikilink") 1993.2—1994.9
  - [吴添祖](../Page/吴添祖.md "wikilink") 1995.6—2000.12
  - [沈寅初](../Page/沈寅初.md "wikilink") 2000.12—2005.3
  - [张立彬](../Page/张立彬.md "wikilink") 2005.3—2015.7
  - [蔡袁强](../Page/蔡袁强.md "wikilink") 2015.7—2017.7
  - [李小年](../Page/李小年.md "wikilink") 2017.7—至今\[2\]

## 知名校友

  - 知名毕业生

<!-- end list -->

  - [周光耀](../Page/周光耀.md "wikilink")：无机化学工程专家，[中国工程院院士](../Page/中国工程院院士.md "wikilink")。
  - [茅临生](../Page/茅临生.md "wikilink")：曾任杭州市市长、中共浙江省委宣传部部长和浙江省人大常委会副主任。
  - [张鸿铭](../Page/张鸿铭.md "wikilink")：曾任杭州市市长、现任浙江省政协副主席。
  - [张志祥](../Page/张志祥_\(企业家\).md "wikilink")：企业家、亿万富豪、全国人大代表。

<!-- end list -->

  - 知名校长

<!-- end list -->

  - [李寿恒](../Page/李寿恒.md "wikilink")：中国化工奠基人。
  - [沈寅初](../Page/沈寅初.md "wikilink")：中国绿色农药之父，中国工程院院士。

<!-- end list -->

  - 知名教授

<!-- end list -->

  - [吴忠超](../Page/吴忠超.md "wikilink")：给出量子宇宙学中太初黑洞量子创生的最一般结果，2001年又利用超引力量子宇宙学，首次证明了可观察时空的四维性。

## 参考资料

## 外部链接

  - [浙江工业大学主页](http://www.zjut.edu.cn)
  - [浙江工业大学之江学院主页](http://www.zjc.zjut.edu.cn)
  - [浙江工业大学-精弘网络](https://web.archive.org/web/20110107102858/http://www.zjut.com/)

[浙江工业大学](../Category/浙江工业大学.md "wikilink")
[Category:1953年創建的教育機構](../Category/1953年創建的教育機構.md "wikilink")
[Category:浙江省重点建设大学](../Category/浙江省重点建设大学.md "wikilink")
[Category:工业院校](../Category/工业院校.md "wikilink")

1.
2.