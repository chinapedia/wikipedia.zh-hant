[Gas_Chromatography_Laboratory.jpg](https://zh.wikipedia.org/wiki/File:Gas_Chromatography_Laboratory.jpg "fig:Gas_Chromatography_Laboratory.jpg")实验室\]\]

**分析化学**是開發分析物質成分、結構的方法，使化學成分得以定性和定量，化學結構得以確定\[1\]。[定性分析可以找到样品中有何化学成分](../Page/定性分析.md "wikilink")；[定量分析可以确定这些成分的含量](../Page/定量分析.md "wikilink")。在分析样品时一般先要想法分离不同的成分。分析化學是化學家最基礎的訓練之一，化學家在實驗技術和基礎知識上的訓練，皆得力於分析化學。

分析的方式大概可分为两大类，经典方法和仪器分析方法\[2\]。仪器分析方法使用仪器去测量分析物的物理属性，比如[光吸收](../Page/吸收_\(光学\).md "wikilink")、[荧光](../Page/荧光.md "wikilink")、[電導等](../Page/電導率.md "wikilink")。仪器分析法常使用如电泳、色谱法、[场流分级等方法来分离样品](../Page/场流分级.md "wikilink")。當代分析化學著重儀器分析，常用的分析儀器有幾大類，包括[原子與分子光譜儀](../Page/原子吸收光谱法.md "wikilink")，[電化學分析儀器](../Page/電化學分析.md "wikilink")，[核磁共振](../Page/核磁共振.md "wikilink")，X光，以及[質譜儀](../Page/質譜儀.md "wikilink")。儀器分析之外的分析化學方法，現在統稱為[古典分析化學](../Page/古典分析化學.md "wikilink")。古典方法（也常被称为[湿化学方法](../Page/湿化学.md "wikilink")）常根据颜色，气味，或[熔点等来分离样品](../Page/熔点.md "wikilink")（比如[萃取](../Page/萃取.md "wikilink")、[沉淀](../Page/沉淀.md "wikilink")、[蒸馏等方法](../Page/蒸馏.md "wikilink")）。这类方法常通过测量[重量或](../Page/重量.md "wikilink")[体积来做定量分析](../Page/体积.md "wikilink")。

## 历史

[Bunsen-Kirchhoff.jpg](https://zh.wikipedia.org/wiki/File:Bunsen-Kirchhoff.jpg "fig:Bunsen-Kirchhoff.jpg")(左)和[罗伯特·本生](../Page/罗伯特·本生.md "wikilink")(右)|alt=Black-and-white
image of two middle-aged men, either one leaning with one elbow on a
wooden column in the middle. Both wear long jackets, and the shorter man
on the left has a beard.\]\]

[无机化学的知识在](../Page/无机化学.md "wikilink")19世纪逐渐系统化，當時[永斯·貝采利烏斯发明](../Page/永斯·貝采利烏斯.md "wikilink")[分析天平](../Page/分析天平.md "wikilink")，使测量得到的实验数据更加接近真实值，也可以用實驗的事实來證實化學定律。[永斯·貝采利烏斯把测定原子量的很多新方法](../Page/永斯·貝采利烏斯.md "wikilink")，新试剂，新仪器引用到分析化学中来，使定量分析精确度达到了一个新的高度。而后来人们都尊称他为分析化学之父\[3\]。

在定性分析方面，1829年[德国化学家](../Page/德国.md "wikilink")编写了一本《分析化学教程》，首次提出了系统定性分析方法。这与目前通用的分析方法已经基本相同了。而到18世纪末，酸碱滴定的各种形式和原则也基本确定\[4\]。

而对于分析化学的一个重要部分[光谱分析](../Page/光谱.md "wikilink")，则是从[牛顿开始的](../Page/牛顿.md "wikilink")。[牛顿从](../Page/牛顿.md "wikilink")1666年开始研究光谱，并于1672年发表了他第一篇论文《光和色的新理论》。从此，观察和研究光谱的人也越来越多，观测的技术也越来越高明。而在1825年[英国物理学家](../Page/英国.md "wikilink")制造了一种研究[光谱的仪器](../Page/光谱.md "wikilink")，对[碱金属火焰进行研究](../Page/碱金属.md "wikilink")，发现了元素有特征光谱的现象。后来德国科学家[罗伯特·本生与](../Page/罗伯特·威廉·本生.md "wikilink")[古斯塔夫·基爾霍夫利用](../Page/古斯塔夫·基爾霍夫.md "wikilink")[本生灯发现了元素](../Page/本生灯.md "wikilink")[铯和](../Page/铯.md "wikilink")[铷](../Page/铷.md "wikilink")。[光谱学作为分析化学的一个重要分支从此诞生](../Page/光谱学.md "wikilink")\[5\]。

进入20世纪之后，随着科学技术和工业的发展，新的分析方法--仪器分析产生了，包括[吸光光度法](../Page/吸光光度法.md "wikilink")，[发射光度法](../Page/发射光度法.md "wikilink")，[极谱分析法](../Page/极谱分析法.md "wikilink")，[放射分析法](../Page/放射分析法.md "wikilink")，[红外光谱](../Page/红外光谱.md "wikilink")，[紫外可见光光谱](../Page/紫外可见光光谱.md "wikilink")，[核磁共振等现代化分析方法](../Page/核磁共振.md "wikilink")。这些分析方法超越了经典分析方法的局限，几乎都不再是通过定量化学反应来确定成分含量，而是根据被检测组分的物理的或化学的特性（如[光学](../Page/光学.md "wikilink")、[电学和](../Page/电学.md "wikilink")[放射性等方面的特性](../Page/放射性.md "wikilink")），灵敏度可以达到很高的水平\[6\]。

目前分析化学还处于第三次变革，这意味着分析化学不再局限于测定物质的组成和含量，而还要对物质的[状态](../Page/状态.md "wikilink")，[结构](../Page/结构.md "wikilink")，[微区](../Page/微区.md "wikilink")，[薄层和表面的组成与结构以及](../Page/薄层.md "wikilink")[化学行为和](../Page/化学行为.md "wikilink")[生物活性等到做出瞬时的追踪](../Page/生物活性.md "wikilink")，无损的和在线监测等分析及过程控制。甚至是要求直接观察原子或分子形态和排列。

## 分析化學型式

  - [定性分析](../Page/定性分析.md "wikilink") - 以确定某一元素或化合物的存在
  - [定量分析](../Page/定量分析.md "wikilink") - 以确定某一元素或化合物的数量

## 當代分析化學

當代分析化學將研究分為兩個範疇，一是分析的對象，一是分析的方法。科学期刊《分析化學》（*Analytical
Chemistry*）每年在第12期會在兩個範疇輪流做一次回顧評述。

### 分析的對象

  - 生物分析化學
  - 材料分析
  - 化學分析
  - 環境分析
  - 法医学分析

### 分析的方法

  - [光譜學](../Page/光譜學.md "wikilink")
  - [質譜學](../Page/質譜.md "wikilink")
  - [分光度和比色法](../Page/分光度和比色法.md "wikilink")
  - [層析和](../Page/層析.md "wikilink")[電泳法](../Page/電泳.md "wikilink")
  - [結晶學](../Page/結晶學.md "wikilink")
  - [顯微術](../Page/顯微術.md "wikilink")
  - [電化學分析](../Page/電化學分析.md "wikilink")

## 古典分析方法

[Flame_test.jpg](https://zh.wikipedia.org/wiki/File:Flame_test.jpg "fig:Flame_test.jpg")的存在。\]\]

虽然现代分析化学大多使用成熟精密的仪器，但一些被用于现代仪器中的原则还是来源于一些至今仍在使用的传统技术。这些技术也仍然是大多数大学[本科分析化学教学实验室的骨干](../Page/本科.md "wikilink")。

### 定性分析

定性分析被用来确定一种特定成分的存在，而不是它的重量或浓度。简单的说，就是与数量无关。

#### 化学测试

现有的定性化学测试有很多种，比如中学教育中常用[石蕊试纸来显示溶液的酸碱性](../Page/石蕊试纸.md "wikilink")。

#### 焰色测试

焰色測試是化學上用來測試某種金屬是否存在在於化合物的方法\[7\]。其原理是每種元素都有其個別的光譜，因此藉由高溫燃燒後火焰的顏色判含有哪一種金屬，例如金黃色火焰表示含有鈉，磚紅火焰表示含有鈣等。樣本通常是粉或小塊的形式。以一條清潔且對化學惰性的金屬線（例如鉑或鎳鉻合金）盛載樣本，再放到[本生燈的無光焰](../Page/本生燈.md "wikilink")（藍色火焰）中。

### 定量分析

#### 重量分析

重量分析法通过测量变化前后的样品重量来确定特定物质的含量。大学本科教育中常见的一个例子是通过加热[水合物前后的重量变化来确定水合物中的水的含量](../Page/水合物.md "wikilink")。

#### 容量分析

滴定在化学分析中，是一种分析溶液成分的方法。将标准溶液逐滴加入被分析溶液中，用颜色变化、[沉淀物的生成](../Page/沉淀.md "wikilink")、[电导率或](../Page/电导率.md "wikilink")[溫度的变化等来确定反应的终点](../Page/溫度.md "wikilink")。滴定通过两种溶液的定量反应来确定某种溶质的含量。較常见的滴定分析包括[酸鹼中和滴定及](../Page/酸鹼中和滴定.md "wikilink")[氧化还原滴定等](../Page/氧化还原滴定.md "wikilink")。

## 儀器分析

[Analytical_instrument.png](https://zh.wikipedia.org/wiki/File:Analytical_instrument.png "fig:Analytical_instrument.png")

  - [原子吸收光譜法](../Page/原子吸收光譜法.md "wikilink")
  - [原子熒光光譜法](../Page/原子熒光光譜法.md "wikilink")
  - [α質子-X射線光譜儀](../Page/α質子-X射線光譜儀.md "wikilink")
  - [毛細管電泳分析儀](../Page/毛細管電泳.md "wikilink")
  - [色譜法](../Page/色譜法.md "wikilink")
  - [比色法](../Page/比色法.md "wikilink")
  - [循環伏安法](../Page/循環伏安法.md "wikilink")
  - [差示掃描量熱法](../Page/差示掃描量熱法.md "wikilink")
  - [電子順旋共振儀](../Page/電子順旋共振儀.md "wikilink")
  - [電子自旋共振](../Page/電子自旋共振.md "wikilink")
  - [橢圓偏振技術](../Page/橢圓偏振技術.md "wikilink")
  - [場流分離](../Page/場流分離.md "wikilink")
  - [傳式轉換紅外線光譜術](../Page/傳式轉換紅外線光譜術.md "wikilink")
  - [氣相色譜法](../Page/氣相色譜法.md "wikilink")
  - [氣相色譜法-質譜聯用](../Page/氣相色譜法-質譜聯用.md "wikilink")
  - [高效液相色譜法](../Page/高效液相色譜.md "wikilink")
  - [液相色譜法-質譜聯用](../Page/液相色譜法-質譜聯用.md "wikilink")
  - [離子微探針](../Page/離子微探針.md "wikilink")

<!-- end list -->

  - [感應耦合電漿](../Page/感應耦合電漿.md "wikilink")
  - [選擇性電極](../Page/選擇性電極.md "wikilink")
  - [激光誘導擊穿光譜儀](../Page/激光誘導擊穿光譜儀.md "wikilink")
  - [質譜儀](../Page/質譜儀.md "wikilink")
  - [穆斯堡爾譜學](../Page/穆斯堡爾譜學.md "wikilink")
  - [核磁共振](../Page/核磁共振.md "wikilink")
  - [粒子誘發X-射線產生](../Page/粒子誘發X-射線產生.md "wikilink")
  - [熱裂解-氣相色譜-質譜儀](../Page/熱裂解-氣相色譜-質譜儀.md "wikilink")
  - [拉曼光譜](../Page/拉曼光譜.md "wikilink")
  - [折射率](../Page/折射率.md "wikilink")
  - [共振增強多光子電離譜](../Page/共振增強多光子電離譜.md "wikilink")
  - [掃瞄穿透X射線顯微鏡](../Page/掃瞄穿透X射線顯微鏡.md "wikilink")
  - [薄层色谱法](../Page/薄层色谱法.md "wikilink")
  - [透射电子显微镜法](../Page/穿透式電子顯微鏡.md "wikilink")
  - [X射线晶体学](../Page/X射线晶体学.md "wikilink")
  - [X射線熒光光譜儀](../Page/X射線熒光光譜儀.md "wikilink")
  - [X射線顯微鏡](../Page/X射線顯微鏡.md "wikilink")

## 信號和雜訊

分析化學中一個重要的內容是使需要的[訊號增到最大值](../Page/訊號.md "wikilink")，同時使相關[雜訊得到最小值](../Page/雜訊.md "wikilink")\[8\]。訊號和雜訊的比例稱為[訊雜比](../Page/訊雜比.md "wikilink")（S/N或SNR）。

雜訊可能來自環境的因素，也可能是由來基本的物理反應。

### 熱噪声

熱噪声也稱為，來自電路中因熱產生的載子（通常是電子）的移動。熱噪声屬於[白雜訊](../Page/白雜訊.md "wikilink")，其[频谱範圍內的功率](../Page/频谱.md "wikilink")[谱密度為定值](../Page/谱密度.md "wikilink")。

電阻產生熱噪声的[均方根值為](../Page/均方根.md "wikilink")\[9\]：

\[v_{{RMS}} = \sqrt { 4 k_B T R \Delta f },\]

其中\(k_B\)為[波茲曼常數](../Page/波茲曼常數.md "wikilink")，\(T\)是[溫度](../Page/溫度.md "wikilink")，\(R\)是[電阻](../Page/電阻.md "wikilink")，\(\Delta f\)為頻率\(f\)的[带宽](../Page/带宽.md "wikilink")。

### 散粒噪声

散粒噪声是儀器中的粒子（例如光學設備中的[光子或是電路中的電子](../Page/光子.md "wikilink")）夠小，因此產生信號的統計波動，也屬於[白雜訊](../Page/白雜訊.md "wikilink")。

散粒噪声是[泊松过程](../Page/泊松过程.md "wikilink")，載子產生的電流會依照[泊松分佈](../Page/泊松分佈.md "wikilink")，電流的均方根值如下\[10\]：

\[i_{{RMS}}=\sqrt{2\,e\,I\,\Delta f}\]

其中\(e\)為[基本電荷](../Page/基本電荷.md "wikilink")，\(I\)為平均電流。

### 闪烁噪声

[闪烁噪声屬於](../Page/闪烁噪声.md "wikilink")\(1/f\)噪声，也稱[粉紅雜訊](../Page/粉紅雜訊.md "wikilink")\[11\]，當頻率上昇時．噪声會下降。闪烁噪声的因素有許多種，例如導線中的雜質，電晶體因基極電流產生的[載流子复合噪声等](../Page/复合.md "wikilink")。闪烁噪声可以由將信號[調變到較高頻率來避免](../Page/調變.md "wikilink")，例如應用[Lock-in](../Page/Lock-in.md "wikilink")[放大器](../Page/放大器.md "wikilink")。

### 環境噪聲

[Analyse_thermo_gravimetrique_bruit.png](https://zh.wikipedia.org/wiki/File:Analyse_thermo_gravimetrique_bruit.png "fig:Analyse_thermo_gravimetrique_bruit.png")中雜訊。中間的部份噪聲較小，是因夜間人員活動較少，產生的環境噪聲也較少\]\]

來由自分析儀器環境的噪聲。環境噪聲的來源包括[電力線](../Page/電力線.md "wikilink")、廣播及電視信號、[無線通訊](../Page/無線通訊.md "wikilink")、[节能灯](../Page/节能灯.md "wikilink")\[12\]及[馬達](../Page/馬達.md "wikilink")。其中許多噪聲的頻寬很窄，因此可以避免。有些儀器可能需要固定溫度及處理。

### 降噪

[降噪可以由](../Page/降噪.md "wikilink")[電腦硬體或](../Page/電腦硬體.md "wikilink")[軟體進行](../Page/軟體.md "wikilink")。硬體的降噪包括使用、[類比濾波器及信號的調變](../Page/類比濾波器.md "wikilink")。軟體的降噪包括[数字滤波器](../Page/数字滤波器.md "wikilink")、、[厢车平均法](../Page/厢车平均法.md "wikilink")（boxcar
average）及[相關係數法](../Page/相關.md "wikilink")\[13\]。

## 應用

分析化學的研究大部份是因性能（靈敏度、選擇性、強健性、、[準確與精密及速度](../Page/準確與精密.md "wikilink")）及成本（購買、操作、訓練、時間及空間）所帶動。在當代分析化學原子光譜法的主要分支中，最廣泛及普遍的是光譜法及質譜法\[14\]。在固體様品的直接元素分析中，領先的是[激光诱导击穿光谱及](../Page/激光诱导击穿光谱.md "wikilink")質譜，以及[感應耦合電漿質譜分析儀中激光消融的相關技術](../Page/感應耦合電漿質譜分析儀.md "wikilink")。

分析化學領域為了將分析設備縮小到[積體電路的大小](../Page/積體電路.md "wikilink")，已花了許多的心力。目前已有上一些這類的設備（如微及[晶片實驗室](../Page/晶片實驗室.md "wikilink")\[15\]），其效果和傳統分析技術相當，且大小、速度及成本有潛在的優勢。可以減少試様的使用數量。

許多的研究和生物系統的分析有關。這些快速進展的領域包括：

  - [基因組學](../Page/基因組學.md "wikilink")：[DNA測序及相關的技術](../Page/DNA測序.md "wikilink")。[遺傳指紋分析及](../Page/遺傳指紋分析.md "wikilink")[DNA微陣列是其中重要的工具及研究領域](../Page/DNA微陣列.md "wikilink")。

  - [蛋白质组学](../Page/蛋白质组学.md "wikilink")：蛋白质集中及修改的分析，特別是針對多種应激物，在不同的發展階段下等
    。

  - [代谢物组学](../Page/代谢物组学.md "wikilink")：類似蛋白质组学，但主要針對[代謝產物的分析](../Page/代謝產物.md "wikilink")。

  - [轉錄組](../Page/轉錄組.md "wikilink")：[mRNA及其相關領域](../Page/信使RNA.md "wikilink")。

  - ：[脂质及其相關領域](../Page/脂质.md "wikilink")。

  - ：[多肽及其相關領域](../Page/多肽.md "wikilink")。

  - [糖组学](../Page/糖组学.md "wikilink")：[糖及其相關領域](../Page/糖.md "wikilink")。

  - ，類似蛋白质组学及代谢物组学，但專注于金屬濃度，以及金屬和蛋白质及其他分子的鍵結。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《基础化学》化学工业出版社

## 外部链接

  - [Annual Review of Analytical
    Chemistry](http://arjournals.annualreviews.org/loi/anchem)

  - [American Chemical Society: Division of Analytical
    Chemistry](http://www.analyticalsciences.org/)

  - [Royal Society of Chemistry: Analytical
    Gateway](http://rsc.org/Gateway/Subject/Analytical/)

## 参见

  - [感官分析](../Page/感官分析.md "wikilink")
  - [虚拟仪器](../Page/虚拟仪器.md "wikilink")
  - [计量学](../Page/计量学.md "wikilink")
  - [测量不确定度](../Page/测量不确定度.md "wikilink")

{{-}}

[A](../Category/化学分支.md "wikilink")
[分析化学](../Category/分析化学.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.

9.
10.
11. [安捷倫科技 參數量測手冊
    第三版](http://cp.literature.agilent.com/litweb/pdf/5990-5278ZHA.pdf)
     page.29 "此外，電子學通常將1/f雜訊稱為閃爍雜訊......也有人將 1/f雜訊稱為粉紅雜訊"

12.

13.
14.

15.