**《大史》**（，[英文譯名](../Page/英文.md "wikilink")：Mahavamsa/Mahawansha，在巴利三藏英譯目錄或辭典中常縮寫爲Mhvs.或Mhv.），[斯里蘭卡古代](../Page/斯里蘭卡.md "wikilink")[巴利文歷史文獻](../Page/巴利文.md "wikilink")，採用[編年](../Page/編年.md "wikilink")[史詩體](../Page/史詩.md "wikilink")，作者是[摩訶那摩](../Page/摩訶那摩.md "wikilink")（[英文譯名](../Page/英文.md "wikilink")：Mahanama）等。該書涉及[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[印度等](../Page/印度.md "wikilink")[南亞等地區](../Page/南亞.md "wikilink")，以及[南傳上座部佛教的歷史](../Page/上座部佛教.md "wikilink")。此外，由於該書辭藻華麗，因而亦被奉為[斯里蘭卡古典文學的瑰寶](../Page/斯里蘭卡.md "wikilink")。\[1\]

## 關於篇幅問題

關於《大史》的篇幅，是有所爭議。其爭論焦點在於，《大史》應包括現今流傳的全部101章，抑或只包括由[摩訶那摩](../Page/摩訶那摩.md "wikilink")（Mahanama）所寫的第1章至第37章第50頌。

  - 主張僅包括[摩訶那摩所撰的第](../Page/摩訶那摩.md "wikilink")1章至第37章第50頌：最先提出的是[德國學者](../Page/德國.md "wikilink")[威廉·蓋格爾](../Page/威廉·蓋格爾.md "wikilink")（Wilhelm
    Geiger），他的主要理由是在第37章第51頌前，出現了一句「向[薄伽梵](../Page/薄伽梵.md "wikilink")、[阿羅漢](../Page/阿羅漢.md "wikilink")、[三藐三佛陀致敬](../Page/三藐三佛陀.md "wikilink")」。而這句句子，是一般[巴利文書籍中的開首語](../Page/巴利文.md "wikilink")，因而從第37章第51頌開始，便應被視為另一部著作。[威廉·蓋格爾將第](../Page/威廉·蓋格爾.md "wikilink")37章第51頌以後的部份列為《小史》。\[2\][斯里蘭卡學者](../Page/斯里蘭卡.md "wikilink")[E·F·C·盧多維克更認為](../Page/E·F·C·盧多維克.md "wikilink")「《小史》，係《大史》的近代部份。」\[3\]

<!-- end list -->

  - 主張全部101章：[斯里蘭卡學者](../Page/斯里蘭卡.md "wikilink")[羅睺羅認為](../Page/羅睺羅_\(現代\).md "wikilink")，從第1章至第101章，每章的末尾都注明《大史》，因而只能說這101章合起來是一部大書。另外，第37章第51頌之前的「向[薄伽梵](../Page/薄伽梵.md "wikilink")、[阿羅漢](../Page/阿羅漢.md "wikilink")、[三藐三佛陀致敬](../Page/三藐三佛陀.md "wikilink")」這一句，只不過是補寫者所添加罷了，不能因此而將全書分為《大史》及《小史》。（據學者[韓廷傑所說](../Page/韓廷傑.md "wikilink")，[威廉·蓋格爾後來亦承認自己](../Page/威廉·蓋格爾.md "wikilink")「在《大史》的問題上犯了一些錯誤。」）\[4\]

## 編撰過程與內容

《大史》從大約公元6世紀開始，到18世紀，由不同時期的作者續寫而成。據[韓廷傑](../Page/韓廷傑.md "wikilink")[中譯本序言](https://web.archive.org/web/20071108045126/http://www.hkbuddhist.org/magazine/484/484_16.html)裡的分析，全書101章，是分成五個階段寫成的，而內容亦大致可分成五個部份：

  - 第一部份：從第1章至第37章第50頌，作者是生活在大約公元5世紀的宗教長老[摩訶那摩](../Page/摩訶那摩.md "wikilink")
    （Mahanama），根據《[島史](../Page/島史.md "wikilink")》（Dipavamsa／Deepavamsa）等材料寫成，內容為公元前5世紀至公元4世紀的史事。
  - 第二部份：從第37章第51頌至第79章第84頌，作者是高僧[達摩祇底](../Page/達摩祇底.md "wikilink")（Dhammakitti），內容是4世紀至12世紀的史事。
  - 第三部份：從第80章至第90章，作者不詳，內容是12世紀至14世紀的史事。
  - 第四部份：從第91章至第100章，作者是[T·蘇曼伽羅](../Page/T·蘇曼伽羅.md "wikilink")（Tibbotuvare
    Sumangala）。內容是14世紀至1758年的史事。
  - 第五部份：第101章，作者是[H·S·蘇曼伽羅](../Page/H·S·蘇曼伽羅.md "wikilink")（Hikkaduve
    Siri
    Sumangala），內容是[英國入侵](../Page/英國.md "wikilink")[斯里蘭卡時的情況](../Page/斯里蘭卡.md "wikilink")。\[5\]

## 史料價值

  - 構建了較完整的[斯里蘭卡歷史及為](../Page/斯里蘭卡歷史.md "wikilink")[南亞史提供了資料](../Page/南亞.md "wikilink")：在[南亞地區的多個國家中](../Page/南亞.md "wikilink")，古代的修史事業並不蓬勃，而由《大史》所帶起的續寫風氣，在一千多年間蔚為[斯里蘭卡的修史傳統](../Page/斯里蘭卡.md "wikilink")。學者[韓廷傑更指出](../Page/韓廷傑.md "wikilink")，「在[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[老撾](../Page/老撾.md "wikilink")、[柬埔寨等一些信仰](../Page/柬埔寨.md "wikilink")[上座部佛教的國家中](../Page/上座部佛教.md "wikilink")，只有[斯里蘭卡有這樣一部完整的歷史](../Page/斯里蘭卡.md "wikilink")」；以及它「涉及到其他的[上座部佛教國家](../Page/上座部佛教.md "wikilink")」\[6\]，這就突顯了《大史》在[南亞地區的史學地位](../Page/南亞.md "wikilink")。到了20世紀，[斯里蘭卡學者](../Page/斯里蘭卡.md "wikilink")[E·F·C·盧多維克編修自](../Page/E·F·C·盧多維克.md "wikilink")18世紀以來的《錫蘭現代史》時，亦參考了這部文獻。
  - 對[上座部佛教的研究提供重要史料](../Page/上座部佛教.md "wikilink")：由於《大史》是以[佛教歷史發展為基礎而寫成](../Page/佛教.md "wikilink")，因而它亦是重要的「研究[上座部佛教最主要的參考書](../Page/上座部佛教.md "wikilink")」。\[7\]

## [中文](../Page/中文.md "wikilink")[翻譯版本的情況](../Page/翻譯.md "wikilink")

[《大史》中譯本.jpg](https://zh.wikipedia.org/wiki/File:《大史》中譯本.jpg "fig:《大史》中譯本.jpg")
《大史》一書，在20世紀上半期已被翻譯成[德文](../Page/德文.md "wikilink")、[英文](../Page/英文.md "wikilink")、[俄文](../Page/俄文.md "wikilink")、[日文等版本](../Page/日文.md "wikilink")。而[中文譯本方面](../Page/中文.md "wikilink")，則有[台北](../Page/台北.md "wikilink")[佛光文化事業有限公司出版的](../Page/佛光文化事業有限公司.md "wikilink")[韓廷傑譯本](../Page/韓廷傑.md "wikilink")，這一版本包括了從6世紀至18世紀寫成的101章。此外，[北京](../Page/北京.md "wikilink")[商務印書館所翻譯出版的](../Page/商務印書館.md "wikilink")《古印度[吠陀時代和列國時代史料選輯](../Page/吠陀時代.md "wikilink")》，亦收錄了一部份《大史》的內容。

## 參見

  - [斯里蘭卡](../Page/斯里蘭卡.md "wikilink")
  - [佛教](../Page/佛教.md "wikilink")
  - [上座部佛教](../Page/上座部佛教.md "wikilink")
  - [島史](../Page/島史.md "wikilink")

## 注釋

## 參考文獻

<div class="references-small">

  -
  -
  -
  -
## 外部連結

  - [香港佛教聯合會：韓廷傑《大史》中譯本譯者《序言》](https://web.archive.org/web/20071108045126/http://www.hkbuddhist.org/magazine/484/484_16.html)（繁體中文）
  - [Geiger/Bode Translation of the Mahavamsa
    《大史》第1章至第37章](http://lakdiva.org/mahavamsa/)（英文）
  - [The Mahavamsa - The Great Chronicle of Sri Lanka
    《大史》第1章至第37章](http://www.vipassana.com/resources/mahavamsa/index.php)（英文）
  - [The History of Sri Lanka 斯里蘭卡歷史](http://www.mahavamsa.org/)（英文）

[Category:斯里蘭卡史書](../Category/斯里蘭卡史書.md "wikilink")
[Category:上座部佛教典籍](../Category/上座部佛教典籍.md "wikilink")

1.  [香港佛教聯合會：韓廷傑《大史》中譯本譯者《序言》](http://www.hkbuddhist.org/magazine/484/484_16.html)


2.
3.  《[錫蘭現代史](../Page/錫蘭現代史.md "wikilink")》，[E·F·C·盧多維克著](../Page/E·F·C·盧多維克.md "wikilink")，[四川大學外語系翻譯組譯](../Page/四川大學.md "wikilink")，[四川](../Page/四川.md "wikilink")[人民出版社](../Page/人民出版社.md "wikilink")1980年版，第498頁。

4.
5.
6.
7.