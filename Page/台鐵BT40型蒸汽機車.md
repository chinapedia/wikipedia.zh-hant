**台鐵BT40型蒸汽機車**是[日治時期引進的四輪煤水車式](../Page/台灣日治時期.md "wikilink")[蒸汽機車](../Page/蒸氣機車.md "wikilink")。

## 簡介

[台灣總督府](../Page/台灣總督府.md "wikilink")[鐵道部向](../Page/台灣總督府交通局鐵道部.md "wikilink")[英國](../Page/英國.md "wikilink")於1908年（70號～71號）、1909年（72號～73號）購入共四輛蒸汽機車。與為同型機車，用來牽引北高急行列車。這款車在台灣唯一4輪煤水車式、使用的機車。早年在台北、嘉義機務段各2部。之後全配置高雄機務段使用[潮州線](../Page/屏東線.md "wikilink")，1935年左右移遷宜蘭機務段[宜蘭線服務](../Page/宜蘭線.md "wikilink")。1931年全數加空氣軔機。

## 性能諸元

[Taiwan-E70SL.jpg](https://zh.wikipedia.org/wiki/File:Taiwan-E70SL.jpg "fig:Taiwan-E70SL.jpg")

  - 車輪配置 2B（4-4-0）

  - 飽合過熱別 飽合式

  - （汽門）

  - 車長 14,891mm

  - 車寬 2,286mm

  - 車高 3,810mm

  - 車輪直徑

      - 動輪 1,372mm
      - 臺車 711mm
      - 煤水車 914mm

  - 汽缸

      - 數目 2個
      - 直徑 406mm
      - 行程 599mm

  - 鍋爐使用壓力（實用汽壓） 11.3kg/cm²

  - 傳熱面積（總計92.62m²）

      - 火箱 9.10m²
      - 煙管 83.52m²

  - 整備重量 36.07t+25.94t

  - 空車重量 30.33t+13.04t

  - 爐箆面積 1.58m²

  - 煙管直徑×長度×數目

      - 小煙管 45mm×3,251mm×184

  - 容量

      - 煤櫃 3.15ton
      - 水櫃 12.7m³

## 参考文献

  - rail No23　出版部 ISBN 4871121732　（日文圖書）

[Steam locomotive](../Category/臺灣鐵路管理局車輛.md "wikilink") [Tender
locomotive](../Category/台灣蒸汽機車.md "wikilink")
[Category:2-4-0輪式機車](../Category/2-4-0輪式機車.md "wikilink")