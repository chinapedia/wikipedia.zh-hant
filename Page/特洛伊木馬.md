[Theprocessionofthetrojanhorseintroybygiovannidomenicotiepolo.jpg](https://zh.wikipedia.org/wiki/File:Theprocessionofthetrojanhorseintroybygiovannidomenicotiepolo.jpg "fig:Theprocessionofthetrojanhorseintroybygiovannidomenicotiepolo.jpg")
**特洛伊木馬**是**木馬屠城記**裡，[希臘軍隊在](../Page/希臘.md "wikilink")[特洛伊戰爭中](../Page/特洛伊戰爭.md "wikilink")，用來攻破[特洛伊城的那隻大木馬](../Page/特洛伊城.md "wikilink")。值得注意的是，木馬屠城記並非於[古希臘](../Page/古希臘.md "wikilink")[詩人](../Page/詩人.md "wikilink")[荷馬的兩部著作](../Page/荷馬.md "wikilink")[伊利亞特與](../Page/伊利亞特.md "wikilink")[奧德赛裡記載](../Page/奧德赛.md "wikilink")，而是在[羅馬帝國時期的詩人](../Page/羅馬帝國.md "wikilink")[維吉爾所寫的史詩](../Page/維吉爾.md "wikilink")《[埃涅阿斯纪](../Page/埃涅阿斯纪.md "wikilink")》中，
才第一次被記載。木馬屠城記一直被[現代科學人視為](../Page/科學.md "wikilink")[神話故事](../Page/神話故事.md "wikilink")，直至十九世紀時，業餘考古學者[海因里希·施利曼](../Page/海因里希·施利曼.md "wikilink")（Heinrich
Schliemann）才證實特洛伊城的遺址。

[Homeric_Greece-en.svg](https://zh.wikipedia.org/wiki/File:Homeric_Greece-en.svg "fig:Homeric_Greece-en.svg")

## 戰爭起因

據[荷馬與](../Page/荷馬.md "wikilink")[希臘神話所載](../Page/希臘神話.md "wikilink")，這個故事的起因是源自一個金蘋果。
這個故事的開端，就是海洋女神[忒提斯](../Page/忒提斯.md "wikilink")（Thetis）與希臘國王[佩琉斯](../Page/佩琉斯.md "wikilink")（Peleus）的婚禮，原本[宙斯與忒提斯相戀](../Page/宙斯.md "wikilink")，但那時傳說忒提斯的兒子（也就是未來的[阿基里斯](../Page/阿基里斯.md "wikilink")（Achilles））會比他的父親還強大，宙斯害怕當年推翻他父親的事重演，於是將她嫁給了著名英雄佩琉斯，避免影響他的政權。婚禮上邀請了很多神，唯獨麻煩女神[埃里斯](../Page/厄里斯_\(希臘神祇\).md "wikilink")（Eris）沒有被邀請。她很生氣，便拋出一個金蘋果，刻著「獻給最美麗的女神」。智慧女神[雅典娜](../Page/雅典娜.md "wikilink")、愛神[阿芙羅狄忒和天后](../Page/阿芙羅狄忒.md "wikilink")[赫拉都認為自己最有資格冠上蘋果上最美麗女神的美譽](../Page/赫拉.md "wikilink")。為了解決這個難題，最後她們飛到艾達山請求[特洛伊王子](../Page/特洛伊.md "wikilink")[帕里斯仲裁](../Page/帕里斯.md "wikilink")。三個女神都試圖賄賂帕里斯：雅典娜答應讓帕里斯成為世界上最睿智的學者；希拉答應讓帕里斯成為天底下最有權勢的君王；阿芙羅狄忒則以世界上最美麗的女子作為賄賂。最後帕里斯忠於感官天性選擇了阿芙羅狄忒。作為回報，阿芙羅狄忒施行魔咒，讓斯巴達王國的王后，公認為世界上最漂亮的女人[海倫和帕里斯共墜愛河](../Page/海伦_\(神话\).md "wikilink")。海倫為了愛情拋棄了她的家鄉，丈夫[墨涅拉俄斯還有稚女](../Page/墨涅拉俄斯.md "wikilink")。帕里斯的行動惹怒了斯巴達國王[墨涅拉俄斯](../Page/墨涅拉俄斯.md "wikilink")，其怒不可抑，於是向兄長[阿伽門農求援](../Page/阿伽門農.md "wikilink")，並聯合希臘各城邦向特洛伊宣戰。

## 戰爭經過

斯巴達國王墨涅拉俄斯因為其妻子海倫被帕里斯所帶走，因此向希臘各[城邦求助](../Page/城邦.md "wikilink")，共同出兵特洛伊。總計有一千艘希臘戰船及五萬名士兵參戰。這場戰爭一打就是十年。但特洛伊因為有[亞馬遜女戰士和黎明女神兒子](../Page/亞馬遜人.md "wikilink")[梅農的幫忙](../Page/梅農.md "wikilink")，與[維納斯暗中協助](../Page/維納斯.md "wikilink")，所以能抵抗希臘聯軍。聯軍统帅阿加门农因为将亲身女儿献祭等原因获得宙斯赫拉等神助，雅典娜因為得不到金蘋果不願放過特洛伊，而且指示[奧德修斯向希臘聯軍獻上木馬屠城之計](../Page/奧德修斯.md "wikilink")。他們打造一隻巨大的木馬，裏面躲著伏兵，並佯裝撒退，讓特洛伊人將其當作戰利品帶回城內，藉此攻入特洛伊。希臘人進入特洛伊城後，燒殺擄掠，最後帶著戰利品滿載而歸。中途雅典娜因为希腊军人冒犯其祭司征得宙斯同意发起风暴，只有极少数人生还。聯軍统帅阿加门农回国后被王后及其奸夫所杀，后二人又被阿加门农的儿子所杀。

## 對其他地方的影響

據[古羅馬傳說所載](../Page/古羅馬.md "wikilink")，就在特洛伊城城破之際，有一人名為伊尼亞士（Aeneas）在亂軍中逃脱，並到達今天的[義大利](../Page/義大利.md "wikilink")，成為羅馬人的始祖。而在特洛伊戰爭後，東地中海成為希臘人的天下，並使為希臘人能夠向[小亞細亞殖民](../Page/小亞細亞.md "wikilink")，這亦使東西方的文化有初步交流。

## 重新發掘

[海因里希．施利曼因為自小對這神話感興趣](../Page/海因里希·施利曼.md "wikilink")，並堅信荷馬所著的這篇木馬屠城記並不是憑空捏造，而是真實記載。因此他花了很多時間賺錢，在儲足發掘所需的金錢後，開始著手研究特洛伊城所在地，最後終於在土耳其愛琴海畔挖出特洛伊古城，並在第六層考古地層證實與史實吻合。

## 參看條目

  - [特洛伊戰爭](../Page/特洛伊戰爭.md "wikilink")
  - [希臘神話](../Page/希臘神話.md "wikilink")

## 外部連結

  - [The Trojan Horse for
    Kids:](http://www.historyforkids.org/learn/greeks/religion/myths/trojanhorse.htm)
    Another ancient image of the Trojan Horse

[T](../Category/希腊神话.md "wikilink") [T](../Category/傳說物品.md "wikilink")