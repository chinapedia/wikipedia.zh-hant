**金莎**（），[中國女](../Page/中華人民共和國.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")。因2002年出演《[十八歲的天空](../Page/十八歲的天空.md "wikilink")》中的藍菲琳而出名。2003年拍攝[阿杜的](../Page/阿杜_\(新加坡歌手\).md "wikilink")《走向前》MV，其間被海蝶音樂中國大陸公司總裁[畢曉世賞識而後進軍歌壇](../Page/畢曉世.md "wikilink")，成為海蝶音樂在中國大陸簽約的第一位歌手，並使用真名金莎作為歌手藝名。

2010年因演唱電視劇《[神话](../Page/神話_\(電視劇\).md "wikilink")》中片尾曲《星月神话》將歌唱事業推向一個高峰。2011年9月15日宣告與合作八年的海蝶結束合約，自立門户成立[金莎工作室](http://baike.baidu.com/view/7732378.htm)，簽約於[天浩盛世](../Page/天浩盛世.md "wikilink")。

## 演藝事業

金莎最初是[上海电视台海选出来的一批美少女主持人之一](../Page/上海电视台.md "wikilink")。当时在上海的戏剧频道中的《灿烂星河》节目组内担任外景主持，曾经采访过[秦沛](../Page/秦沛.md "wikilink")，[曾江等来上海的港台艺人](../Page/曾江.md "wikilink")。\[1\]

2005年3月，大师兄阿杜在成为2005年[新加坡旅游大使之后](../Page/新加坡旅游大使.md "wikilink")，即力邀金莎共同为新加坡旅游广告代言。金莎在《非常新加坡》的系列TVC中担任女主角。同年5月發行首張個人專輯《[空氣](../Page/空氣_\(金莎專輯\).md "wikilink")》，半年内销量轻松突破20万张，成为新人人气歌手。当中歌曲《被风吹过的夏天》尤为走红，它是新加坡歌手[林俊杰创作的第一首男女对唱歌曲](../Page/林俊杰.md "wikilink")，创作灵感来自于金莎在[三亚拍摄广告的一段感情邂逅](../Page/三亚.md "wikilink")。2006年推出第二張專輯《[不可思議](../Page/不可思議_\(金莎專輯\).md "wikilink")》。

2007年3月9日，海蝶音乐将金莎从[中国大陆推向](../Page/中国大陆.md "wikilink")[台湾市场](../Page/台湾.md "wikilink")，推出精选辑《[不可思議金選](../Page/不可思議_\(金莎專輯\).md "wikilink")》，当中集合了之前发售的两张专辑中部分曲目。同年5月在中国大陆发行第三張专辑《[換季](../Page/換季_\(金莎專輯\).md "wikilink")》。

2008年向劇戲方面發展，先後參與兩部劇集是《[幸福的眼泪](../Page/幸福的眼泪.md "wikilink")》和《[大城市小浪漫](../Page/大城市小浪漫.md "wikilink")》擔任女主角。

2009年回到歌壇，於[情人節發行單曲](../Page/情人節.md "wikilink")《[這種愛](../Page/這種愛_\(金莎EP\).md "wikilink")》。隔年推出第四张专辑《[星月神話](../Page/星月神話_\(金莎專輯\).md "wikilink")》。

2012年3月14日發行第五張专辑《[他不愛我](../Page/他不愛我_\(金莎專輯\).md "wikilink")》。这是金莎离开[海蝶音乐转投](../Page/海蝶音乐.md "wikilink")[天浩盛世后发布的第一张专辑](../Page/天浩盛世.md "wikilink")。

## 音樂作品

### EP

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>EP #</p></th>
<th style="text-align: left;"><p>EP資料</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>首張EP《<strong><a href="../Page/這種愛_(金莎EP).md" title="wikilink">這種愛</a></strong>》</p>
<ul>
<li>發行日期：2009年2月9日</li>
<li>語言：国语</li>
<li>唱片公司：海蝶音乐</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2nd</strong></p></td>
<td style="text-align: left;"><p>第二張EP《<strong><a href="../Page/等你說愛我_(金莎EP).md" title="wikilink">等你說愛我</a></strong>》</p>
<ul>
<li>發行日期：2012年7月30日</li>
<li>語言：国语</li>
<li>唱片公司：天浩盛世</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 专辑

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>专辑 #</p></th>
<th style="text-align: left;"><p>专辑資料</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>首張专辑《<strong><a href="../Page/空氣_(金莎專輯).md" title="wikilink">空氣</a></strong>》</p>
<ul>
<li>發行日期：2005年4月23日</li>
<li>語言：国语</li>
<li>唱片公司：海蝶音乐</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>2nd</strong></p></td>
<td style="text-align: left;"><p>第二張专辑《<strong><a href="../Page/不可思議_(金莎專輯).md" title="wikilink">不可思議</a></strong>》</p>
<ul>
<li>發行日期：2006年5月28日</li>
<li>語言：国语</li>
<li>唱片公司：海蝶音乐</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>3th</strong></p></td>
<td style="text-align: left;"><p>第三張专辑《<strong><a href="../Page/換季_(金莎專輯).md" title="wikilink">換季</a></strong>》</p>
<ul>
<li>發行日期：2007年5月30日</li>
<li>語言：国语</li>
<li>唱片公司：海蝶音乐</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>4th</strong></p></td>
<td style="text-align: left;"><p>第四張专辑《<strong><a href="../Page/星月神話_(金莎專輯).md" title="wikilink">星月神話</a></strong>》</p>
<ul>
<li>發行日期：2010年10月12日</li>
<li>語言：国语</li>
<li>唱片公司：海蝶音乐</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>5th</strong></p></td>
<td style="text-align: left;"><p>第五張专辑《<strong><a href="../Page/他不愛我_(金莎專輯).md" title="wikilink">他不愛我</a></strong>》</p>
<ul>
<li>發行日期：2012年3月14日</li>
<li>語言：国语</li>
<li>唱片公司：天浩盛世文化传播</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 精選輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>专辑 #</p></th>
<th style="text-align: left;"><p>专辑資料</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><strong>1st</strong></p></td>
<td style="text-align: left;"><p>第一張精選辑《<strong><a href="../Page/不可思議_(金莎專輯).md" title="wikilink">不可思議金選</a></strong>》</p>
<ul>
<li>發行日期：2007年3月9日</li>
<li>語言：国语</li>
<li>唱片公司：海蝶音樂</li>
</ul></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 其他單曲

| 發表年份     | 歌曲                                 | 專輯名稱                                                                               | 備註                                             |
| -------- | ---------------------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------- |
| 2002年    | 橙色呼吸                               |                                                                                    |                                                |
| 回到慢歌     |                                    |                                                                                    |                                                |
| 2006年    | 聆聽世界                               |                                                                                    | 與海蝶群星合唱 (海蝶15周年主題曲)                            |
| 2007年    | 發現愛                                | 《[西界](../Page/西界.md "wikilink")》                                                   | 與[林俊傑合唱](../Page/林俊傑.md "wikilink") (收錄於林俊傑專輯) |
| 復活       | 《[淋雨中](../Page/淋雨中.md "wikilink")》 | 與[林宇中合唱](../Page/林宇中.md "wikilink") (收錄於林宇中專輯)                                     |                                                |
| 迷戀       |                                    | 換季Demo                                                                             |                                                |
| 2008年    | 唱響世界                               | 《海蝶夯之唱響世界》                                                                         | 與海蝶群星合唱 (收錄於《海蝶夯之唱響世界》專輯)                      |
| 留下來      |                                    | 電視劇《[幸福的眼淚](../Page/幸福的眼淚.md "wikilink")》插曲                                        |                                                |
| 幸福的眼淚    |                                    | 與[劉恩佑合唱](../Page/劉恩佑.md "wikilink") (電視劇《[幸福的眼淚](../Page/幸福的眼淚.md "wikilink")》主題曲) |                                                |
| 期待爱      | 《[JJ陸](../Page/JJ陸.md "wikilink")》 | 與[林俊傑合唱](../Page/林俊傑.md "wikilink") (收錄於林俊傑專輯)                                     |                                                |
| 我有我主張    |                                    | 迪士尼動畫片《[小美人魚](../Page/小美人魚.md "wikilink")》中文版主题曲                                   |                                                |
| 北京歡迎你    |                                    | 與群星合唱                                                                              |                                                |
| 2010年    | 夢想的翅膀                              |                                                                                    | 與海蝶群星合唱 (2010年上海世博會主題曲)                        |
| 2011年    | 跨越新世界                              |                                                                                    | 與群星合唱                                          |
| 我愛上海     |                                    | 與群星合唱 (2011上海春晚主題曲)                                                                |                                                |
| 2012年    | 我們結婚吧                              | 我們結婚吧                                                                              | 與劉佳合唱                                          |
| 壯鄉美酒喝不夠  |                                    | 魅力邊關·友誼憑祥文藝晚會主題曲                                                                   |                                                |
| 夢千年之戀    |                                    |                                                                                    |                                                |
| 2013年    | 白蛇愛情物語                             |                                                                                    | 改編舊作星月神話                                       |
| 2014年    | 你是我的糖                              |                                                                                    | 电视剧《红色青橄榄》主题曲，大白兔奶糖的原创歌曲                       |
| 想聽聽你說謊   |                                    |                                                                                    |                                                |
| 2016年    | 全世界我只貪一個他                          |                                                                                    |                                                |
| 諾        |                                    | 電影《[臥虎藏龍2](../Page/臥虎藏龍2.md "wikilink")》主題曲                                        |                                                |
| 只屬於你     |                                    |                                                                                    |                                                |
| 2018年    | 獨孤                                 |                                                                                    | 《[獨孤天下](../Page/獨孤天下.md "wikilink")》插曲         |
| 今生等你一次回眸 |                                    | 《[遠大前程](../Page/遠大前程.md "wikilink")》插曲                                             |                                                |
| 漫游       |                                    | 《[盛唐幻夜](../Page/盛唐幻夜.md "wikilink")》插曲                                             |                                                |
| 貼身輔助     |                                    | 《問道》手遊助威曲                                                                          |                                                |
| 若只如初見    |                                    | 《[回到明朝當王爺之楊凌傳](../Page/回到明朝當王爺之楊凌傳.md "wikilink")》片尾曲                              |                                                |
| 著迷       |                                    | 電視劇《[你和我的傾城時光](../Page/你和我的傾城時光.md "wikilink")》插曲                                  |                                                |

### MV

  - [阿杜](../Page/阿杜.md "wikilink")：《走向前》
  - [林宇中](../Page/林宇中.md "wikilink")：《復活》
  - [林俊杰](../Page/林俊杰.md "wikilink")：《被风吹过的夏天》
  - [林俊杰](../Page/林俊杰.md "wikilink")：《發現愛》
  - [北京奧運](../Page/北京奧運.md "wikilink")：《[北京歡迎你](../Page/北京歡迎你.md "wikilink")》參與合唱

## 影視作品

### 电视剧

  - 2002年 《[十八岁的天空](../Page/十八岁的天空.md "wikilink")》饰演 蓝菲琳
  - 2002年 《[粉红女郎](../Page/粉红女郎.md "wikilink")》饰演 女明星（客串）
  - 2003年 《[幸福家庭](../Page/幸福家庭.md "wikilink")》饰演 陈龙的秘书
  - 2009年 《[幸福的眼泪](../Page/幸福的眼泪.md "wikilink")》饰演
    何童瑶，为《[一公升的眼泪](../Page/一公升的眼泪.md "wikilink")》中国版
  - 2009年 《[神话](../Page/神话_\(电视剧\).md "wikilink")》饰演 吕素
  - 2010年 《[大城市小浪漫](../Page/大城市小浪漫.md "wikilink")》饰演 孟小萱
  - 2010年 《[唐琅探案](../Page/唐琅探案.md "wikilink")》饰演 齐美伦
  - 2010年 《[香格里拉](../Page/香格里拉_\(電視劇\).md "wikilink")》饰演 和梅
  - 2012年 《[王的女人](../Page/王的女人_\(中国电视剧\).md "wikilink")》饰演 戚喜冰
  - 2013年 《那是一个春天/春天的战争》 饰演 慕容春天 (待播中)（女主角）
  - 2018年 《[上海女子图鉴](../Page/上海女子图鉴.md "wikilink")》(網絡劇)

### 电影

  - 2004年 《[命运呼叫转移](../Page/命运呼叫转移.md "wikilink")》饰演 米泓【客串】
  - 2010年 《[财神到](../Page/财神到.md "wikilink")》【客串】
  - 2011年 《[大人物](../Page/大人物.md "wikilink")》
  - 2012年 《》 飾演 曲珍
  - 2016年 《[王妃的男人](../Page/王妃的男人.md "wikilink")》待播中

### 綜藝節目

  - 2015年 [星星的密室第二季](../Page/星星的密室.md "wikilink") 参赛嘉宾
  - 2016年 [谁是大歌神第三期](../Page/谁是大歌神.md "wikilink") 特别嘉宾

## 广告

  - [肯德基系列](../Page/肯德基.md "wikilink")
  - [富士胶卷](../Page/富士胶卷.md "wikilink")
  - [康师傅绿茶](../Page/康师傅.md "wikilink")
  - [蒙牛酸酸乳](../Page/蒙牛.md "wikilink")
  - [和路雪可爱多](../Page/联合利华.md "wikilink")
  - [格力高饼干](../Page/江崎格力高.md "wikilink")
  - [大白兔奶糖](../Page/大白兔.md "wikilink")
  - [白兰氏鸡精](../Page/白兰氏.md "wikilink")

## 参考文献

## 外部链接

  - 早安日記

  -
[Category:中國女歌手](../Category/中國女歌手.md "wikilink")
[Category:中國華語流行音樂歌手](../Category/中國華語流行音樂歌手.md "wikilink")
[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:上海演员](../Category/上海演员.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[S莎](../Category/金姓.md "wikilink")

1.  [范湉湉微博](http://fx.weico.net/share/40257389.html?weibo_id=3806905107756230)