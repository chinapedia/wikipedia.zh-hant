**葉足動物**（[學名](../Page/學名.md "wikilink")：）是可以追溯至早期[寒武紀時代的](../Page/寒武紀.md "wikilink")[動物](../Page/動物.md "wikilink")，對於當中絕大部分至今仍然所知不多。此[門下的動物具體節](../Page/門_\(生物\).md "wikilink")，有足，同時卻很難被分類到[節肢動物的範疇](../Page/節肢動物.md "wikilink")。基本上這種動物就像「長著[足的](../Page/足.md "wikilink")[蠕蟲](../Page/蠕蟲.md "wikilink")」，外觀上亦與其近親很接近。當中大部分被認為與[節肢動物及](../Page/節肢動物.md "wikilink")[有爪動物有親緣關係](../Page/有爪動物.md "wikilink")。葉足動物可能包括了[奇蝦](../Page/奇蝦.md "wikilink")、[歐巴賓海蠍及一些似有爪動物的動物](../Page/歐巴賓海蠍.md "wikilink")，如(*Xenusion*)、(*Microdictyon*)、(*Aysheaia*)、(*Onychodictyon*)、[貧腿蟲](../Page/貧腿蟲.md "wikilink")(*Paucipoda*)、(*Cardiodictyon*)、[啰哩山虫](../Page/啰哩山虫.md "wikilink")(*Luolishania*)、[怪誕蟲](../Page/怪誕蟲.md "wikilink")(*Hallucigenia*)等，可能与今天的[有爪动物有亲缘关系](../Page/有爪动物.md "wikilink")。\[1\]

由於一些微網蟲有骨片的鱗，故有些學者將一些寒武紀早期有骨片的動物分類在此屬中。

葉足動物的體型一般大於2[厘米](../Page/厘米.md "wikilink")，且能自由[游泳或](../Page/游泳.md "wikilink")[運動](../Page/運動.md "wikilink")。目前所知的葉足動物都是[掠食者](../Page/掠食者.md "wikilink")。至於有關牠們的生長、習性或與其他早期生物的關係則不得而知。

## 参考资料

[Category:动物](../Category/动物.md "wikilink")
[Category:蛻皮動物](../Category/蛻皮動物.md "wikilink")
[Category:无脊椎动物](../Category/无脊椎动物.md "wikilink")
[\*](../Category/叶足动物.md "wikilink")

1.  刘建妮，韩健，张志飞，舒德干《寒武纪叶足动物与现生有爪类的亲缘关系再讨论》，《西北大学学报:自然科学版》，2009，39(6)：1037-1041