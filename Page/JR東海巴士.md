[Jrtokaibus-shinjukulinerhama-20070202.jpg](https://zh.wikipedia.org/wiki/File:Jrtokaibus-shinjukulinerhama-20070202.jpg "fig:Jrtokaibus-shinjukulinerhama-20070202.jpg")
**JR東海巴士**（）是[JR東海全資持有負責經營](../Page/JR東海.md "wikilink")[巴士業務的](../Page/巴士.md "wikilink")[公司](../Page/公司.md "wikilink")\[1\]。

## 歷史

  - 1987年4月1日：[日本國鐵分割](../Page/日本國鐵.md "wikilink")[民營化](../Page/民營化.md "wikilink")，東海旅客鐵道汽車事業部（通稱JR東海巴士）成立。
  - 1988年2月24日：JR東海巴士株式會社成立。
  - 1988年4月1日：JR東海巴士繼承原先的事業部門，開始營業。
  - 2002年9月30日：大野線、天龍線、名金急行線停止營業。
  - 2006年9月30日：[中部國際空港線停止營業](../Page/中部國際空港.md "wikilink")，成為[JR集團之內唯一沒有地方路線巴士](../Page/JR集團.md "wikilink")、僅有高速巴士的公司。

### 参考来源

## 外部連結

  - [JR東海巴士](http://www.jrtbinm.co.jp/)

[Category:日本巴士公司](../Category/日本巴士公司.md "wikilink")
[Category:JR東海集團](../Category/JR東海集團.md "wikilink")
[Category:愛知縣公司](../Category/愛知縣公司.md "wikilink")

1.