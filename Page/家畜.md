**家畜**一般是指由[人类飼養](../Page/人类.md "wikilink")[馴化](../Page/馴化.md "wikilink")，且可以人為控制其繁殖的[動物](../Page/動物.md "wikilink")，一般用於食用、勞役、毛皮、[寵物](../Page/寵物.md "wikilink")、[實驗等功能](../Page/實驗.md "wikilink")。另一種較狹義的家畜，是指相對於[鳥類動物的](../Page/鳥類.md "wikilink")[家禽而言的](../Page/家禽.md "wikilink")[哺乳類動物](../Page/哺乳類.md "wikilink")，亦即將雞、[鴨等等被排除在外](../Page/鴨.md "wikilink")。除此此外，[哺乳類和](../Page/哺乳類.md "wikilink")[鳥類之外的](../Page/鳥類.md "wikilink")[魚類](../Page/魚類.md "wikilink")、[昆蟲等也通常不被視為家畜](../Page/昆蟲.md "wikilink")。

一般較常見家畜飼養方式的舍飼，[圈飼](../Page/圈飼.md "wikilink")、[繫養](../Page/繫養.md "wikilink")、[放牧等](../Page/放牧.md "wikilink")。

人類最早飼養家畜最早起源於一萬多年前，代表了人類走向[文明的重要發展之一](../Page/文明.md "wikilink")，家畜尤其對於提供了較穩定的食物來源作出重大貢獻。

[中國人古代所稱的](../Page/中國.md "wikilink")「[六畜](../Page/六畜.md "wikilink")」是指[馬](../Page/馬.md "wikilink")、[牛](../Page/牛.md "wikilink")、[羊](../Page/羊.md "wikilink")、[雞](../Page/雞.md "wikilink")、[狗](../Page/狗.md "wikilink")、[豬](../Page/豬.md "wikilink")，亦即中國古代最常見的六種家畜。

## 列表

現在並無統一的“家畜”定義，其範圍可寬可窄。照一般說法，家畜為經濟等目的或為消遣所飼養的牲畜總稱;尤指菜畜、奶牛、役畜豬、狗、貓等。包括了家養動物，半野生半家養家畜，或圈養的動物。半馴化是指只輕微馴化或有爭議的動物。

<table>
<thead>
<tr class="header">
<th><p>动物</p></th>
<th><p>驯化状态</p></th>
<th><p>野生祖先</p></th>
<th><p>最早驯化时间</p></th>
<th><p>最初驯化地点</p></th>
<th><p>主要用途</p></th>
<th><p>照片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/羊驼.md" title="wikilink">羊驼</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/小羊駝.md" title="wikilink">小羊駝</a></p></td>
<td><p>公元前5000到4000年</p></td>
<td><p><a href="../Page/安第斯山脉.md" title="wikilink">安第斯山区</a></p></td>
<td><p><a href="../Page/羊毛.md" title="wikilink">羊毛</a>, <a href="../Page/肉.md" title="wikilink">肉</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Corazon_Full.jpg" title="fig:Corazon_Full.jpg">Corazon_Full.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/爪哇野牛.md" title="wikilink">爪哇野牛</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/爪哇野牛.md" title="wikilink">爪哇野牛</a></p></td>
<td><p>未知</p></td>
<td><p><a href="../Page/东南亚.md" title="wikilink">东南亚</a>，<a href="../Page/爪哇岛.md" title="wikilink">爪哇岛</a></p></td>
<td><p><a href="../Page/肉.md" title="wikilink">肉</a>, <a href="../Page/牛奶.md" title="wikilink">牛奶</a>, <a href="../Page/役用動物.md" title="wikilink">農耕</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:KA_Zoo_Huftieranlage.jpg" title="fig:KA_Zoo_Huftieranlage.jpg">KA_Zoo_Huftieranlage.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/美洲野牛.md" title="wikilink">美洲野牛</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>仅猎获<br />
（另见）</p></td>
<td><p>未知</p></td>
<td><p>19世纪晚期</p></td>
<td><p><a href="../Page/北美洲.md" title="wikilink">北美洲</a></p></td>
<td><p><a href="../Page/肉.md" title="wikilink">肉</a>, <a href="../Page/皮革.md" title="wikilink">皮革</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:American_bison_k5680-1.jpg" title="fig:American_bison_k5680-1.jpg">American_bison_k5680-1.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/骆驼.md" title="wikilink">骆驼</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p>野生<a href="../Page/單峰駱駝.md" title="wikilink">單峰駱駝和</a><a href="../Page/双峰骆驼.md" title="wikilink">双峰骆驼</a></p></td>
<td><p>公元前4000年和公元前1400年</p></td>
<td><p><a href="../Page/亚洲.md" title="wikilink">亚洲</a></p></td>
<td><p><a href="../Page/役用動物.md" title="wikilink">骑</a>, <a href="../Page/役用動物.md" title="wikilink">背負</a>, <a href="../Page/肉.md" title="wikilink">肉</a>, <a href="../Page/乳製品.md" title="wikilink">乳製品</a>, <a href="../Page/駝絨.md" title="wikilink">駝絨</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chameau_de_bactriane.JPG" title="fig:Chameau_de_bactriane.JPG">Chameau_de_bactriane.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/猫.md" title="wikilink">家猫</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/肉食動物.md" title="wikilink">肉食動物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/非洲野貓.md" title="wikilink">非洲野貓</a></p></td>
<td><p>公元前7500年 [1]<ref>{{cite news |first=Hazel |last=Muir</p></td>
<td><p>title=Ancient remains could be oldest pet cat |url=<a href="http://www.newscientist.com/article/dn4867.html">http://www.newscientist.com/article/dn4867.html</a> |publisher=<a href="../Page/New_Scientist.md" title="wikilink">New Scientist</a> |date=2004-04-08 |accessdate=2007-11-23 }}</ref>[2]</p></td>
<td><p><a href="../Page/近東.md" title="wikilink">近東</a></p></td>
<td><p><a href="../Page/除害蟲.md" title="wikilink">除害蟲</a>, <a href="../Page/寵物.md" title="wikilink">陪伴</a>, <a href="../Page/肉.md" title="wikilink">肉</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/家牛.md" title="wikilink">家牛</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/原牛.md" title="wikilink">原牛</a></p></td>
<td><p>公元前6000年</p></td>
<td><p><a href="../Page/西南亞.md" title="wikilink">西南亞</a>，<a href="../Page/印度.md" title="wikilink">印度</a>，<a href="../Page/北非.md" title="wikilink">北非</a>（？）</p></td>
<td><p><a href="../Page/肉.md" title="wikilink">肉</a>（<a href="../Page/牛肉.md" title="wikilink">牛肉</a>，<a href="../Page/小牛肉.md" title="wikilink">小牛肉</a>，<a href="../Page/血液.md" title="wikilink">血液</a>），<a href="../Page/奶製品.md" title="wikilink">奶製品</a>，<a href="../Page/皮革.md" title="wikilink">皮革</a>，<a href="../Page/役用動物.md" title="wikilink">農耕</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Long_horned_european_wild_ox.jpg" title="fig:Long_horned_european_wild_ox.jpg">Long_horned_european_wild_ox.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鹿科.md" title="wikilink">鹿</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>仅猎获</p></td>
<td><p>未知</p></td>
<td><p>公元1世纪</p></td>
<td><p><a href="../Page/英国.md" title="wikilink">英国</a></p></td>
<td><p><a href="../Page/肉類.md" title="wikilink">肉類</a>（<a href="../Page/鹿肉.md" title="wikilink">鹿肉</a>），<a href="../Page/皮革.md" title="wikilink">皮革</a>，<a href="../Page/鹿角.md" title="wikilink">鹿角</a>，<a href="../Page/天鵝絨（鹿角覆蓋）.md" title="wikilink">鹿角天鵝絨</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Silz_cerf22.jpg" title="fig:Silz_cerf22.jpg">Silz_cerf22.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/犬.md" title="wikilink">犬</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/雜食動物.md" title="wikilink">雜食動物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/灰狼.md" title="wikilink">灰狼</a></p></td>
<td><p>公元前12000年</p></td>
<td><p>全球</p></td>
<td><p><a href="../Page/役用動物.md" title="wikilink">農耕</a>，<a href="../Page/役用動物.md" title="wikilink">背负</a>，<a href="../Page/役用動物.md" title="wikilink">狩獵</a>，<a href="../Page/役用動物.md" title="wikilink">放牧</a>，<a href="../Page/役用動物.md" title="wikilink">搜尋/收集</a>，<a href="../Page/役用動物.md" title="wikilink">看守/守衛</a>，<a href="../Page/肉.md" title="wikilink">肉</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Pembroke_Welsh_Corgi_600.jpg" title="fig:Pembroke_Welsh_Corgi_600.jpg">Pembroke_Welsh_Corgi_600.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/驴.md" title="wikilink">驴</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/非洲野驴.md" title="wikilink">非洲野驴</a></p></td>
<td><p>公元前4000年</p></td>
<td><p><a href="../Page/埃及.md" title="wikilink">埃及</a></p></td>
<td><p><a href="../Page/working_animal.md" title="wikilink">mount</a>, <a href="../Page/working_animal.md" title="wikilink">pack animal</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a>, <a href="../Page/meat.md" title="wikilink">meat</a>, <a href="../Page/dairy.md" title="wikilink">dairy</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Donkey_1_arp_750px.jpg" title="fig:Donkey_1_arp_750px.jpg">Donkey_1_arp_750px.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/大额牛.md" title="wikilink">大额牛</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/印度野牛.md" title="wikilink">印度野牛</a></p></td>
<td><p>未知</p></td>
<td><p><a href="../Page/Southeast_Asia.md" title="wikilink">Southeast Asia</a></p></td>
<td><p><a href="../Page/meat.md" title="wikilink">meat</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bandipur_2.jpg" title="fig:Bandipur_2.jpg">Bandipur_2.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/家山羊.md" title="wikilink">家山羊</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/野山羊.md" title="wikilink">野山羊</a></p></td>
<td><p>公元前8000年</p></td>
<td><p><a href="../Page/Southwest_Asia.md" title="wikilink">Southwest Asia</a></p></td>
<td><p><a href="../Page/Dairy.md" title="wikilink">Dairy</a>, <a href="../Page/meat.md" title="wikilink">meat</a>, <a href="../Page/wool.md" title="wikilink">wool</a>, <a href="../Page/leather.md" title="wikilink">leather</a>, <a href="../Page/working_animal.md" title="wikilink">light draught</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Capra,_Crete_4.jpg" title="fig:Capra,_Crete_4.jpg">Capra,_Crete_4.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/豚鼠.md" title="wikilink">豚鼠</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td></td>
<td><p>公元前5000年</p></td>
<td><p><a href="../Page/South_America.md" title="wikilink">South America</a></p></td>
<td><p><a href="../Page/Meat.md" title="wikilink">Meat</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Caviaklein.jpg" title="fig:Caviaklein.jpg">Caviaklein.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/家马.md" title="wikilink">家马</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/野马.md" title="wikilink">野马</a></p></td>
<td><p>公元前4000年</p></td>
<td><p><a href="../Page/Eurasian_Steppes.md" title="wikilink">Eurasian Steppes</a></p></td>
<td><p><a href="../Page/Riding_animal.md" title="wikilink">Mount</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a>, <a href="../Page/dairy.md" title="wikilink">dairy</a>, <a href="../Page/meat.md" title="wikilink">meat</a>, <a href="../Page/pack_animal.md" title="wikilink">pack animal</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Nokota_Horses_cropped.jpg" title="fig:Nokota_Horses_cropped.jpg">Nokota_Horses_cropped.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/大羊駝.md" title="wikilink">大羊駝</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/原駝.md" title="wikilink">原駝</a></p></td>
<td><p>公元前3500年</p></td>
<td><p><a href="../Page/安第斯山区.md" title="wikilink">安第斯山区</a></p></td>
<td><p><a href="../Page/working_animal.md" title="wikilink">light mount</a>, <a href="../Page/working_animal.md" title="wikilink">pack animal</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a>, <a href="../Page/meat.md" title="wikilink">meat</a>, <a href="../Page/wool.md" title="wikilink">wool</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Pack_llamas_posing_near_Muir_Trail.jpg" title="fig:Pack_llamas_posing_near_Muir_Trail.jpg">Pack_llamas_posing_near_Muir_Trail.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/骡.md" title="wikilink">骡</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/驴.md" title="wikilink">驴和</a><a href="../Page/马.md" title="wikilink">马的杂交种</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p><a href="../Page/working_animal.md" title="wikilink">mount</a>, <a href="../Page/working_animal.md" title="wikilink">pack animal</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:09.Moriles_Mula.JPG" title="fig:09.Moriles_Mula.JPG">09.Moriles_Mula.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/家猪.md" title="wikilink">家猪</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/雜食動物.md" title="wikilink">雜食動物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/野豬.md" title="wikilink">野豬</a></p></td>
<td><p>公元前7000年</p></td>
<td><p><a href="../Page/Eastern_Anatolia.md" title="wikilink">Eastern Anatolia</a></p></td>
<td><p><a href="../Page/Meat.md" title="wikilink">Meat</a> (<a href="../Page/pork.md" title="wikilink">pork</a>, <a href="../Page/bacon.md" title="wikilink">bacon</a>, etc.), <a href="../Page/leather.md" title="wikilink">leather</a>, <a href="../Page/pet.md" title="wikilink">pet</a>, <a href="../Page/research.md" title="wikilink">research</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sow_with_piglet.jpg" title="fig:Sow_with_piglet.jpg">Sow_with_piglet.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/兔.md" title="wikilink">家兔</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/野兔.md" title="wikilink">野兔</a></p></td>
<td><p>公元400 - 900年</p></td>
<td><p><a href="../Page/法国.md" title="wikilink">法国</a></p></td>
<td><p><a href="../Page/Meat.md" title="wikilink">Meat</a>, <a href="../Page/fur.md" title="wikilink">fur</a>, <a href="../Page/leather.md" title="wikilink">leather</a>, <a href="../Page/pet.md" title="wikilink">pet</a>, <a href="../Page/research.md" title="wikilink">research</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Miniature_Lop_-_Side_View.jpg" title="fig:Miniature_Lop_-_Side_View.jpg">Miniature_Lop_-_Side_View.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/驯鹿.md" title="wikilink">驯鹿</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>半驯化</p></td>
<td><p><a href="../Page/驯鹿.md" title="wikilink">驯鹿</a></p></td>
<td><p>公元前3000年</p></td>
<td><p>Northern <a href="../Page/Russia.md" title="wikilink">Russia</a></p></td>
<td><p><a href="../Page/Meat.md" title="wikilink">Meat</a>, <a href="../Page/leather.md" title="wikilink">leather</a>, <a href="../Page/antler.md" title="wikilink">antlers</a>, <a href="../Page/dairy.md" title="wikilink">dairy</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a>,</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Caribou_using_antlers.jpg" title="fig:Caribou_using_antlers.jpg">Caribou_using_antlers.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/綿羊.md" title="wikilink">綿羊</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p>Asiatic <a href="../Page/mouflon.md" title="wikilink">mouflon</a> sheep</p></td>
<td><p>公元前11000 - 9000年</p></td>
<td><p><a href="../Page/Southwest_Asia.md" title="wikilink">Southwest Asia</a></p></td>
<td><p><a href="../Page/Wool.md" title="wikilink">Wool</a>, <a href="../Page/dairy.md" title="wikilink">dairy</a>, <a href="../Page/leather.md" title="wikilink">leather</a>, <a href="../Page/meat.md" title="wikilink">meat</a> (<a href="../Page/lamb_(food).md" title="wikilink">mutton</a>, <a href="../Page/lamb_(meat).md" title="wikilink">lamb</a>)</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Pair_of_Icelandic_Sheep.jpg" title="fig:Pair_of_Icelandic_Sheep.jpg">Pair_of_Icelandic_Sheep.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/水牛.md" title="wikilink">水牛</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/河水牛.md" title="wikilink">河水牛</a></p></td>
<td><p>公元前4000年</p></td>
<td><p><a href="../Page/South_Asia.md" title="wikilink">South Asia</a></p></td>
<td><p><a href="../Page/working_animal.md" title="wikilink">mount</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a>, <a href="../Page/meat.md" title="wikilink">meat</a>, <a href="../Page/dairy.md" title="wikilink">dairy</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:BUFFALO159.JPG" title="fig:BUFFALO159.JPG">BUFFALO159.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/牦牛.md" title="wikilink">牦牛</a></strong><br />
<a href="../Page/哺乳动物.md" title="wikilink">哺乳动物</a>，<a href="../Page/食草动物.md" title="wikilink">食草动物</a></p></td>
<td><p>已驯化</p></td>
<td><p><a href="../Page/牦牛.md" title="wikilink">牦牛</a></p></td>
<td><p>公元前2500年</p></td>
<td><p><a href="../Page/西藏.md" title="wikilink">西藏</a>、<a href="../Page/尼泊尔.md" title="wikilink">尼泊尔</a></p></td>
<td><p><a href="../Page/Meat.md" title="wikilink">Meat</a>, <a href="../Page/dairy.md" title="wikilink">dairy</a>, <a href="../Page/wool.md" title="wikilink">wool</a>, <a href="../Page/working_animal.md" title="wikilink">mount</a>, <a href="../Page/working_animal.md" title="wikilink">pack animal</a>, <a href="../Page/working_animal.md" title="wikilink">draught</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bos_grunniens_-_Syracuse_Zoo.jpg" title="fig:Bos_grunniens_-_Syracuse_Zoo.jpg">Bos_grunniens_-_Syracuse_Zoo.jpg</a></p></td>
</tr>
</tbody>
</table>

 
{|border="0" |}

## 参见

  - [畜牧業](../Page/畜牧業.md "wikilink")
  - [役用動物](../Page/役用動物.md "wikilink")
  - [家禽](../Page/家禽.md "wikilink")

## 参考文献

[Category:馴養動物](../Category/馴養動物.md "wikilink")
[Category:畜牧學](../Category/畜牧學.md "wikilink")

1.
2.