**偏二甲肼**，或称**1,1-二甲基[联氨](../Page/联氨.md "wikilink")**、**偏二甲基聯胺**、**偏二甲基肼**，分子式(CH<sub>3</sub>)<sub>2</sub>NNH<sub>2</sub>，英文缩写**UDMH**（Unsymmetrical
dimethylhydrazine），无色易燃液体。

## 制法

### [二甲胺亚硝化法](../Page/二甲胺.md "wikilink")

二甲胺与[亚硝酸作用后经还原而得](../Page/亚硝酸.md "wikilink")。

\[\rm (CH_3)_2NH + HNO_2 {=\!=\!=} (CH_3)_2NNO + H_2O\]

\[\rm (CH_3)_2NNO + 2Zn + 4HCl {=\!=\!=} (CH_3)_2NNH_2 + 2ZnCl_2 + H_2O\]

### 液态[氯胺法](../Page/氯胺.md "wikilink")

二甲胺和[氯胺反应而得](../Page/氯胺.md "wikilink")。

\[\rm (CH_3)_2NH + NH_2Cl {=\!=\!=} (CH_3)_2NNH_2 + HCl\]

## 用途

  - 高比冲液体[火箭燃料](../Page/火箭.md "wikilink")：优点在于有高[比冲值](../Page/比冲.md "wikilink")，与[氧化剂接触即自动着火](../Page/氧化剂.md "wikilink")。

\[\rm (CH_3)_2NNH_2 + 4O_2 {=\!=\!=} N_2 + 2CO_2 + 4H_2O\]

  - 是“嫦娥一号”、“神舟九号”运载火箭发动机推进剂之一，常规氧化剂为四氧化二氮

\[\rm (CH_3)_2NNH_2 + 2N_2O_4 {=\!=\!=} 3N_2\uparrow + 2CO_2\uparrow + 4H_2O\uparrow\]

  - 做为液体火箭燃料，在常温下保存、使用。这与低温的[液氧](../Page/液氧.md "wikilink")-[煤油的液体火箭燃料方案相比](../Page/煤油.md "wikilink")，具有更便捷的军事用途。所以苏联的[SS-19](../Page/SS-19.md "wikilink")、中国的[东风-5液体燃料战略导弹](../Page/东风-5.md "wikilink")、[长征二号丙运载火箭](../Page/长征二号丙运载火箭.md "wikilink")，都使用偏二甲肼-[四氧化二氮常温方案](../Page/四氧化二氮.md "wikilink")，不用低温液氧-煤油方案。
  - [有机合成原料](../Page/有机合成.md "wikilink")

## 危险性

偏二甲肼是一种强还原剂，有毒，可致癌，且能透过皮肤直接进入人体内。故使用时需要作好防护措施

若偏二甲肼被释放到大气中，会成为[N-亚硝基二甲胺](../Page/N-亚硝基二甲胺.md "wikilink")，一种持久的致癌物与地下水污染物\[1\]

## 外部連結

  - [偏二甲肼、57-14-7
    CAS查询、偏二甲肼物化性质-中国化工制造网](http://www.chemmade.com/assistant/chemdic/casDetail-57-14-7.html)

[Category:肼](../Category/肼.md "wikilink")
[Category:火箭推进剂](../Category/火箭推进剂.md "wikilink")

1.