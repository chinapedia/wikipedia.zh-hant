**鲍喜顺**（），生于[中国](../Page/中国.md "wikilink")[内蒙古](../Page/内蒙古.md "wikilink")[赤峰市](../Page/赤峰市.md "wikilink")[翁牛特旗阿什罕苏木](../Page/翁牛特旗.md "wikilink")，[蒙古族](../Page/蒙古族.md "wikilink")，吉尼斯世界纪录持有者，曾是全世界身高最高的人（2米36），现为世界第二身高者，次於[蘇丹·科塞](../Page/蘇丹·科塞.md "wikilink")。

## 世界之最：236巨人

[XI.jpeg](https://zh.wikipedia.org/wiki/File:XI.jpeg "fig:XI.jpeg")
鲍喜顺在2005年、2008年两度獲得吉尼斯世界纪录認證被承認是[地球上因自然原因而長得最高的活人](../Page/地球.md "wikilink")（其他擁有世界之最頭銜的長人不是已逝，如[罗伯特·瓦德罗](../Page/罗伯特·瓦德罗.md "wikilink")，就是因特殊病變而过度发育稱為[巨人症](../Page/巨人症.md "wikilink")，如[列昂尼德·斯塔德尼克](../Page/列昂尼德·斯塔德尼克.md "wikilink")）。实际上，这一纪录在2007年即被乌克兰人[列昂尼德·斯塔德尼克](../Page/列昂尼德·斯塔德尼克.md "wikilink")（Leonid
Stadnyk）取代，但因其后来拒绝接受吉尼斯纪录组织测量身高而并未被该组织继续认可，故鲍喜顺重新成为世界最高人，直至2009年该纪录持有者被土耳其人[蘇丹·科塞](../Page/蘇丹·科塞.md "wikilink")（Sultan
Kösen）取代。

鲍喜顺[身高達](../Page/身高.md "wikilink")7尺9寸（236.1公分），[體重達](../Page/體重.md "wikilink")364磅（165公斤），在2005年1月15日時经过[赤峰醫院](../Page/赤峰.md "wikilink")[测量後](../Page/测量.md "wikilink")，获得[吉尼斯世界记录認證的世界之最頭銜](../Page/吉尼斯世界记录.md "wikilink")。

鲍喜顺自称16岁及之前身高完全正常，其后的身高变化源于目前未知的原因，持续7年後达到了现在的高度2米36。

在回到内蒙古前他曾在[军队服役三年](../Page/军队.md "wikilink")，担任[饲养员工作](../Page/饲养员.md "wikilink")。其后加入解放军篮球队，但身高开始出现异常并因此导致[退役](../Page/退役.md "wikilink")\[1\]。

鲍喜顺的[腿不好](../Page/腿.md "wikilink")，走路需要借助两根[拐杖](../Page/拐杖.md "wikilink")。

## 活動

鲍喜顺曾參與[极速前进16的拍攝](../Page/极速前进16.md "wikilink")，他是第十一賽段的當地接待員。

## 注释

## 参考书目

  - [BBC中文部对喜顺的访谈](http://news.bbc.co.uk/chinese/simp/hi/newsid_4280000/newsid_4281000/4281018.stm)
  - 吉尼斯世界纪录, (2005年),
    [最高的活人](http://www.guinnessworldrecords.com/content_pages/record.asp?recordid=48406&Reg=1)
  - [照片](http://img250.imageshack.us/img250/4190/xi1gh.jpg)

[Category:吉尼斯世界纪录保持者](../Category/吉尼斯世界纪录保持者.md "wikilink")
[Category:中华人民共和国的世界之最](../Category/中华人民共和国的世界之最.md "wikilink")
[Category:翁牛特旗人](../Category/翁牛特旗人.md "wikilink")
[Category:中华人民共和国蒙古族人物](../Category/中华人民共和国蒙古族人物.md "wikilink")
[X喜](../Category/鮑姓.md "wikilink")
[Category:巨人症患者](../Category/巨人症患者.md "wikilink")
[Category:中国士兵](../Category/中国士兵.md "wikilink")

1.