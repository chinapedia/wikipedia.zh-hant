**陳重光**（），曾任[臺視董事長](../Page/臺視.md "wikilink")、[養樂多公司董事長](../Page/養樂多公司.md "wikilink")、[寶島商業銀行](../Page/寶島商業銀行.md "wikilink")（今[日盛國際商業銀行](../Page/日盛國際商業銀行.md "wikilink")）董事長、[中華職棒會長](../Page/中華職業棒球大聯盟.md "wikilink")、[臺灣省議員](../Page/臺灣省議員.md "wikilink")、[臺北市議員等](../Page/臺北市議員.md "wikilink")，曾為臺北市[陳家班要員](../Page/臺灣地方派系.md "wikilink")。

## 經歷

  - [自立報系董事長](../Page/自立報系.md "wikilink")
  - [寶島商業銀行](../Page/寶島商業銀行.md "wikilink")（今[日盛國際商業銀行](../Page/日盛國際商業銀行.md "wikilink")）董事長
  - [臺灣電視公司第十一](../Page/臺灣電視公司.md "wikilink")、十二屆董事長（1990年12月～1995年8月）
  - [中華民國槌球協會第一](../Page/中華民國槌球協會.md "wikilink")、二屆理事長（1988年～1996年）
  - [泰北高中董事長](../Page/泰北高中.md "wikilink")
  - [養樂多公司董事長](../Page/養樂多公司.md "wikilink")
  - [中華職棒第二任會長](../Page/中華職棒.md "wikilink")（1993年10月7日～1998年2月22日）
  - 陳重光文教基金會創辦人（1993年）
  - 臺灣省議員
  - 臺北市議員

## 家庭

女兒[陳玉梅為前臺北市議員](../Page/陳玉梅.md "wikilink")、[僑務委員會副委員長](../Page/僑務委員會.md "wikilink")。

兒子[陳炳甫為基創實業](../Page/陳炳甫.md "wikilink")（[大魯閣棒壘球打擊場](../Page/大魯閣棒壘球打擊場.md "wikilink")）創辦人，後亦於2014年底當選臺北市議員迄今。

## 其他

  - 1954年[臺北市長選舉時](../Page/1954年中華民國縣市長選舉#臺北市.md "wikilink")，當時[王民寧](../Page/王民寧.md "wikilink")（曾任[臺灣警備總部副官處處長](../Page/臺灣警備總部.md "wikilink")）陣營以請吃選舉飯試圖爭取支持。據住在[雙園區](../Page/雙園區.md "wikilink")[堀江町的康寧祥回憶](../Page/堀江町_\(台北市\).md "wikilink")，當時每到中午一到，左鄰右舍就會到王民寧的競選總部報到，吃大魚大肉。飽餐一頓後再抱王民寧的選舉傳單出來，直接送去[高玉樹的競選總部的火爐裡](../Page/高玉樹.md "wikilink")，然後換高玉樹的傳單去發，也就是「吃王民寧的飯，投高玉樹的票」。此事後來獲陳重光向他證實，陳重光是國民黨員，奉命幫王民寧競選，但私下又跟高玉樹很熟，兩邊的競選總部都會去跑。有次他在高玉樹的競選總部後頭發現一個[焚化爐一直在燒](../Page/焚化爐.md "wikilink")，走近一看發現燒的都是王民寧的傳單。想不到國民黨員再多，[特務再多](../Page/特務.md "wikilink")，還是敵不過民心向背。

## 外部連結

## 參考資料

|- |colspan="3"
style="text-align:center;"|**[中華職業棒球大聯盟](../Page/中華職業棒球大聯盟.md "wikilink")**
|-

[Category:臺灣省議員](../Category/臺灣省議員.md "wikilink")
[Category:台視董事長](../Category/台視董事長.md "wikilink")
[Category:中華職棒會長](../Category/中華職棒會長.md "wikilink")
[Category:臺北人](../Category/臺北人.md "wikilink")
[Chong重](../Category/陳姓.md "wikilink")