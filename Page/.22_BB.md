**.22 BB Cap**（Bulleted Breech
Cap）是一種低速、低噪音，口徑0.22英吋[底緣底火式](../Page/凸緣式底火.md "wikilink")（Rimfire）的[子彈](../Page/子弹.md "wikilink")。外型與[.22
LR類似](../Page/.22_LR.md "wikilink")，但是較短。力量與口徑0.22英吋的空氣槍相近。通常用於室內練習。.22
BB
Cap是於1845年發展的**第一個底緣底火式子彈**。由於彈殼內沒有[發射藥](../Page/裝藥.md "wikilink")，僅靠[底火產生的力量將彈頭射出](../Page/底火.md "wikilink")，槍口初速低於
700 英尺/秒 (210 米/秒)。

## 彈藥規格

  - 彈殼長度：0.284 寸（7.2 毫米）
  - 全 長：.343 寸（8.7 毫米）
  - 彈頭重量：18 [格令](../Page/格令.md "wikilink")（1.17 克）

## 相關資訊

  - [手槍子彈列表](../Page/手槍子彈列表.md "wikilink")
  - [.22 CB](../Page/.22_CB.md "wikilink")
  - [.22 Short](../Page/.22_Short.md "wikilink")
  - [.22 Long](../Page/.22_Long.md "wikilink")
  - [.22 LR](../Page/.22_LR.md "wikilink")
  - [.22 Magnum](../Page/.22_Magnum.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  - Cartridges of the World 11th Edition, Book by Frank C. Barnes,
    Edited by Stan Skinner, Gun Digest Books, 2006, ISBN 0-89689-297-2
    pp. 490, 492

</div>

[Category:子彈](../Category/子彈.md "wikilink")