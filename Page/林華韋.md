**林華韋**（Professor, Hua-Wei
Lin，1958年5月27日－），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")、教練、大學教授。

## 生平

小學時原本是桌球隊成員，曾拿過地區性桌球比賽冠軍，但因對棒球有濃厚興趣，於高年級加入台南市永福國小少棒隊，1970年在南部七縣市選拔賽中奪得第二名，於是因此被挑進南部七縣市的代表隊，稱為『七虎隊』。後來贏得全國冠軍，代表我國參加[威廉波特少棒賽](../Page/威廉波特少棒賽.md "wikilink")，可惜首戰輸給尼加拉瓜，當年七虎隊只拿到第五名。青少棒時期加入華興中學青少棒隊，大學時期進入[輔仁大學棒球隊](../Page/輔仁大學.md "wikilink")（葡萄王）。

1982年參加漢城世界盃，取得第四名；1983年到日本打業餘棒球，第一年加入山葉發動機隊（YAMAHA集團），後來轉隊至河合樂器打了兩年。

1987年進入[日本](../Page/日本.md "wikilink")[筑波大學體育研究所攻讀碩士班](../Page/筑波大學.md "wikilink")。

學成歸國後，自1990年起至2001年止，長達十二年期間擔任國家代表隊教練。2001年11月請辭[中華隊總教練一職](../Page/中華隊.md "wikilink")，至[國立台灣體育學院任教至今](../Page/國立台灣體育學院.md "wikilink")。

## 學歷

  - [日本](../Page/日本.md "wikilink")[茨城縣](../Page/茨城縣.md "wikilink")[筑波大學](../Page/筑波大學.md "wikilink")
    體育學碩士
  - [台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[輔仁大學](../Page/輔仁大學.md "wikilink")
    體育系學士
  - [台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[私立華興中學](../Page/私立華興中學.md "wikilink")
    高中部
  - [台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[私立華興中學](../Page/私立華興中學.md "wikilink")
    國中部
  - [台灣](../Page/台灣.md "wikilink")[台南市立建興國民中學](../Page/台南市立建興國民中學.md "wikilink")
  - [台灣](../Page/台灣.md "wikilink")[台南市永福國民小學](../Page/台南市.md "wikilink")

## 經歷

  - [台中市](../Page/台中市.md "wikilink")[國立臺灣體育運動大學](../Page/國立臺灣體育運動大學.md "wikilink")
    [校長](../Page/校長.md "wikilink") (2013/8/2-)
  - [台中市](../Page/台中市.md "wikilink")[國立臺灣體育學院](../Page/國立臺灣體育運動大學.md "wikilink")
    體育室訓練組組長
  - [台中市](../Page/台中市.md "wikilink")[國立臺灣體育大學台中校區](../Page/國立臺灣體育運動大學.md "wikilink")
    制競技運動學系專任教授兼主任 (2008)
  - [台中市](../Page/台中市.md "wikilink")[國立臺灣體育學院](../Page/國立臺灣體育運動大學.md "wikilink")
    制競技運動學系專任教授兼主任
  - [台中市](../Page/台中市.md "wikilink")[國立臺灣體育專科學校](../Page/國立臺灣體育運動大學.md "wikilink")
    教務處註冊組組長
  - 第一屆世界棒球經典賽[中華成棒隊總教練](../Page/中華成棒隊.md "wikilink") (2006)

## 外部連結

[L](../Category/中華成棒隊總教練.md "wikilink")
[L](../Category/台灣棒球選手.md "wikilink")
[L](../Category/1958年出生.md "wikilink")
[L](../Category/在世人物.md "wikilink")
[L](../Category/輔仁大學校友.md "wikilink")
[L](../Category/國立臺灣體育學院校友.md "wikilink")
[Category:台灣公立大專院校校長](../Category/台灣公立大專院校校長.md "wikilink")