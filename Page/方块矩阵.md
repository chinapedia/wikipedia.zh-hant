**方塊矩陣**，或简称**方阵**\[1\]，是行數及列數皆相同的[矩陣](../Page/矩陣.md "wikilink")。由\(n \times n\,\)矩陣組成的[集合](../Page/集合.md "wikilink")，連同[矩陣加法和](../Page/矩陣加法.md "wikilink")[矩陣乘法](../Page/矩陣乘法.md "wikilink")，构成[環](../Page/環_\(代數\).md "wikilink")。除了\(n=1\,\)，此環並不是[交换環](../Page/交换環.md "wikilink")。

M(*n*,
**R**)，即實方塊矩陣環，是個實[有单位的](../Page/幺环.md "wikilink")[結合代數](../Page/結合代數.md "wikilink")。M(*n*,
**C**)，即複方塊矩陣環，則是複結合代數。

**[單位矩陣](../Page/單位矩陣.md "wikilink")**\(I_n\,\)的對角線全是1而其他位置全是0，對所有\(m \times n\,\)矩陣\(M\,\)及\(n \times k\,\)矩陣\(N\,\)都有\(MI_n=M\,\)及\(I_nN=N\,\)。
例如，若\(n=3\,\)：

\[I_3 =
  \begin{bmatrix}
    1 & 0 & 0 \\
    0 & 1 & 0 \\
    0 & 0 & 1
  \end{bmatrix}\,\] 單位矩陣是方塊矩陣環的單位元。

方塊矩陣環的[可逆元稱為](../Page/可逆元.md "wikilink")**[可逆矩陣](../Page/可逆矩陣.md "wikilink")**或**非奇异方阵**。\(n \times n\,\)矩陣\(A\,\)是可逆當且僅當存在矩陣\(B\,\)使得

\[AB=I_n(=BA)\,\]。
此時\(B\,\)稱為\(A\,\)的**[逆矩陣](../Page/逆矩陣.md "wikilink")**，並記作\(A^{-1}\,\)。
所有\(n\times n\,\)矩陣在乘法上組成一個[群](../Page/群.md "wikilink")（亦是一個[李群](../Page/李群.md "wikilink")），稱為[一般線性群](../Page/一般線性群.md "wikilink")。

若數字\(\lambda\,\)和非零向量**\(\vec v\,\)**满足\(A\vec v=\lambda\vec v\,\)，則**\(\vec v\,\)**為\(A\,\)的一個[特征向量](../Page/特征向量.md "wikilink")，\(\lambda\,\)是其對應的[特征值](../Page/特征值.md "wikilink")。數字\(\lambda\,\)為\(A\,\)的特征值當且僅當\(A-\lambda I_n\,\)可逆，又當且僅當\(p_A(\lambda)=0\,\)。這裏，\(p_A(x)\,\)是\(A\,\)的[特征多項式](../Page/特征多項式.md "wikilink")。特征多項式是一個\(n\,\)次多項式，有\(n\,\)个复根（考虑重根），即\(A\,\)有\(n\,\)個特征值。

方塊矩陣\(A\,\)的[行列式是其](../Page/行列式.md "wikilink")\(n\,\)個特征值的積，但亦可經由[莱布尼茨公式計算出來](../Page/莱布尼茨公式.md "wikilink")。可逆矩陣正好是那些行列式非零的矩陣。

[高斯-若爾當消元法非常重要](../Page/高斯-若爾當消元法.md "wikilink")，可以用来計算矩阵的[行列式](../Page/行列式.md "wikilink")，[秩](../Page/秩_\(线性代数\).md "wikilink")，逆矩陣，并解決[線性方程組](../Page/線性方程組.md "wikilink")。

[矩陣的迹是](../Page/矩陣的迹.md "wikilink")\(n \times n\,\)矩陣的对角线元素之和，也是其\(n\,\)個特征值之和。

所有[正交矩阵都是方块矩阵](../Page/正交矩阵.md "wikilink")。

## 方块矩阵的等价命题

[线性代数中](../Page/线性代数.md "wikilink")，下列关于方块矩阵*A*的命题是等价的（同时成立，或同时不成立）：

1.  *A* [可逆](../Page/可逆矩阵.md "wikilink") ;
    *A*的[反矩陣存在](../Page/逆矩阵.md "wikilink")。
2.  [det](../Page/行列式.md "wikilink")(*A*)≠ 0.
3.  [rank](../Page/矩阵的秩.md "wikilink")(*A*)= n.
4.  Null(*A*) = 0.
5.  *A*的[特征值中没有](../Page/特征值.md "wikilink")0。
6.  对任意**b**属于**F**<sup>n</sup>，*A***x** = **b**有唯一解。
7.  *A***x** = **0**只有平凡解。
8.  *A*<sup>T</sup>*A*可逆。
9.  *A*与单位矩阵行（列）等价。
10. A的行向量或列向量張成**F**<sup>n</sup>.
11. *A*的零空间只有零向量。
12. A的值域為**F**<sup>n</sup>.
13. *A*的行（列）向量构成**F**<sup>n</sup> (**F**<sup>n</sup>)中向量的线性无关集。

这裡，**F**为矩阵元素所属的[域](../Page/域.md "wikilink")。通常，这个域为[实数域或](../Page/实数.md "wikilink")[复数域](../Page/复数.md "wikilink")。

## 參考資料

[F](../Category/矩陣.md "wikilink")

1.  ，[國家教育研究院](../Page/國家教育研究院.md "wikilink")