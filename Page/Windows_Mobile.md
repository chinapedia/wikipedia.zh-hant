**Windows
Mobile**是[微軟針對移動産品而開發的精簡](../Page/微軟.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。2009年2月，微軟發佈了Windows
Mobile 6.5系統。Windows Mobile捆綁了一系列針對移動設備而開發的應用軟件，這些應用軟件建立在Microsoft
[Win32](../Page/Windows_API.md "wikilink")
[API的基礎上](../Page/API.md "wikilink")。可以運行Windows
Mobile的設備包括[Pocket
PC](../Page/Pocket_PC.md "wikilink")、[Smartphone和](../Page/SmartPhone.md "wikilink")[Portable
Media
Center](../Page/Portable_Media_Center.md "wikilink")。該作業系統的設計初衷是盡量接近于[桌面版本的](../Page/桌面.md "wikilink")[Windows](../Page/Windows.md "wikilink")。繼任者[Windows
Phone作業系統出現後](../Page/Windows_Phone.md "wikilink")，Windows
Mobile系列正式退出[手機系統市場](../Page/手機.md "wikilink")。2010年10月，微軟宣布終止對Windows
Mobile的所有技術支持。

## Windows Mobile的常见功能

Windows Mobile通常有以下特点：

### [Pocket PC及](../Page/Pocket_PC.md "wikilink")[Pocket PC Phone系列](../Page/Pocket_PC_Phone.md "wikilink")

  - 主页面（今日，類似Symbian OS的Active
    Standby，用來顯示[個人信息管理系統資料](../Page/個人信息管理系統.md "wikilink")）
  - Internet Explorer Mobile（和PC版Internet Explorer相似）
  - 收件箱（訊息中心，整合Outlook E-mail與簡訊功能）
  - Windows Media Player（和PC版Windows Media Player相似）
  - 资源管理器（和PC版Windows Explorer相似）
  - [Windows Live
    Messenger](../Page/Windows_Live_Messenger.md "wikilink") / Windows
    Live（和PC版Windows Live Messenger相似）
  - [Office
    Mobile](../Page/Microsoft_Office_Mobile.md "wikilink")（和PC版Microsoft
    Office相似，有Word, PowerPoint, Excel和OneNote，由廠方選配）
  - [ActiveSync](../Page/ActiveSync.md "wikilink")（與PC連接並用於交換資料）

### [Smart Phone系列](../Page/Smart_Phone.md "wikilink")

  - [开始菜单](../Page/开始菜单.md "wikilink")：开始菜单是Smartphone使用者运行各种[程序的快捷方法](../Page/程序.md "wikilink")。类似于[桌面版本的](../Page/桌面.md "wikilink")[Windows](../Page/Windows.md "wikilink")，Windows
    Mobile for
    Smartphone的开始菜单主要也由程序[快捷方式的](../Page/快捷方式.md "wikilink")[图标组成](../Page/图标.md "wikilink")，并且为图标分配了数字序号，便于快速运行。

<!-- end list -->

  - [标题栏](../Page/标题栏.md "wikilink")：标题栏是Smartphone显示各种信息的地方。包括当前运行程序的标题以及各种托盘[图标](../Page/图标.md "wikilink")，如电池电量图标，手机信号图标，[输入法图标以及应用程序放置的特殊图标](../Page/输入法.md "wikilink")。在Smartphone中标题栏的作用类似于桌面windows中的标题栏加上[系统托盘](../Page/系统托盘.md "wikilink")。

<!-- end list -->

  - [电话功能](../Page/电话.md "wikilink")：Smartphone系统的应用对象均为[智能手机](../Page/智能手机.md "wikilink")，故电话功能是Smartphone的重要功能。电话功能很大程度上与Outlook集成，可以提供拨号，联系人，拨号历史等功能。

<!-- end list -->

  - [Outlook](../Page/Outlook.md "wikilink")：Windows Mobile均内置了[Outlook
    Mobile](../Page/Outlook.md "wikilink")。包括[任务](../Page/任务.md "wikilink")、[日历](../Page/日历.md "wikilink")、[联系人和](../Page/联系人.md "wikilink")[收件箱](../Page/收件箱.md "wikilink")。Outlook
    Mobile可以同桌面Windows系统的Outlook[同步以及同](../Page/同步.md "wikilink")[Exchange
    Server同步](../Page/Exchange_Server.md "wikilink")（此功能需要[Internet连接](../Page/Internet.md "wikilink")）Microsoft
    Outlook的桌面版本往往由Windows Mobile产品设备附赠。

<!-- end list -->

  - [Windows Media
    Player](../Page/Windows_Media_Player.md "wikilink")：WMP是Windows
    Mobile的[捆绑软件](../Page/捆绑软件.md "wikilink")。其起始版本为9，但大多数新的设备均为10版本。针对现有的设备，用户可以由网上[下载](../Page/下载.md "wikilink")[升级到WMP](../Page/升级.md "wikilink")10。WMP支持[WMA](../Page/WMA.md "wikilink")，[WMV](../Page/WMV.md "wikilink")，[MP3以及](../Page/MP3.md "wikilink")[AVI文件的播放](../Page/AVI.md "wikilink")。目前[MPEG文件不被支持](../Page/MPEG.md "wikilink")，但可经由第三方插件可以获得支持。某些版本的WMP同时兼容[M4A音频](../Page/M4A.md "wikilink")。

## 版本历史

Windows Mobile採用[Windows
CE](../Page/Windows_CE.md "wikilink")[內核](../Page/內核.md "wikilink")，最早被用作Pocket
PC
2000的作業系統，后开发出适用于[手机及其他](../Page/手机.md "wikilink")[掌上设备操作系统](../Page/掌上设备.md "wikilink")，之后又将其整合于一起。在Windows
Mobile 2003版本之前操作系统名称为[Pocket
PC](../Page/Pocket_PC.md "wikilink")、[Smartphone等](../Page/SmartPhone.md "wikilink")，后改为现名。Windows
Mobile包括[掌上电脑的](../Page/掌上电脑.md "wikilink")[Windows Mobile
Standard](../Page/Windows_Mobile_Standard.md "wikilink")、用于手机的[Windows
Mobile Classic](../Page/Smartphone.md "wikilink")、[Windows Mobile
Professional和用于移动媒体的](../Page/Pocket_PC_Phone.md "wikilink")[Portable
Media Center三大版本](../Page/Portable_Media_Center.md "wikilink")。

### Pocket PC 2000

Pocket PC 2000最初代號為"Rapier"（雙刃劍）\[1\]，發布於2000年4月19日。這是後來被稱為Windows
Mobile的作業系統的首次亮相，意味著要繼承[智慧型手機的作業系統](../Page/智慧型手機.md "wikilink")。Pocket
PC 2000年主要用於Pocket PC，但是一些掌上型電腦系統有能力加以更新。此外，一些手機的Pocket PC
2000年被發布，但智慧型手機的[硬體平台尚未完全建立](../Page/硬體.md "wikilink")。这个版本只支援[QVGA](../Page/QVGA.md "wikilink")（320x240）的解析度。
设计上，Pocket PC 2000跟[Windows
98](../Page/Windows_98.md "wikilink")，[Windows
Me和](../Page/Windows_Me.md "wikilink")[Windows
2000類似](../Page/Windows_2000.md "wikilink")。

功能及內建的應用程式：\[2\]

  - Pocket [Office](../Page/Microsoft_Office.md "wikilink")
      - Pocket [Word](../Page/Word.md "wikilink")
      - Pocket [Excel](../Page/Excel.md "wikilink")
      - Pocket [Outlook](../Page/Outlook.md "wikilink")
  - Pocket [Internet Explorer](../Page/Internet_Explorer.md "wikilink")
  - [Windows Media Player](../Page/Windows_Media_Player.md "wikilink")
  - [Microsoft Reader](../Page/Microsoft_Reader.md "wikilink")
  - [Microsoft Money](../Page/Microsoft_Money.md "wikilink")
  - [Notes](../Page/Notes.md "wikilink")
  - 支援[手寫辨識](../Page/手寫.md "wikilink")
  - [紅外線](../Page/紅外線.md "wikilink")（IR）文件傳輸能力

### Pocket PC 2002

Pocket PC
2002的代號為"Merlin"（默林）\[3\]，發布於2001年10月。雖然主要對象為240×320（英寸，[QVGA](../Page/QVGA.md "wikilink")）[屏幕](../Page/屏幕.md "wikilink")[解像度的](../Page/解像度.md "wikilink")[Pocket
PC](../Page/Pocket_PC.md "wikilink")，2002也可以用於[掌上電腦的手機](../Page/掌上電腦.md "wikilink")，並首次可适用于用於[智能手機](../Page/智能手機.md "wikilink")\[4\]。這些掌上電腦智能手機，主要是2002年的[GSM設備](../Page/GSM.md "wikilink")。就像當時新推出的[Windows
XP一樣](../Page/Windows_XP.md "wikilink")，Pocket PC 2002的界面图标採用了相似的新設計。

新功能及內置的應用程序：\[5\]\[6\]\[7\]\[8\]

  - 增強的用戶界面與主題的支持
  - Pocket Word中的拼寫檢查和字數統計工具
  - 可保存和下載的Pocket [Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")
  - 支援[虛擬專用網絡](../Page/虛擬專用網絡.md "wikilink")（VPN）
  - [同步文件夾](../Page/同步.md "wikilink")
  - [MSN Messenger](../Page/MSN_Messenger.md "wikilink")
  - [終端服務](../Page/終端.md "wikilink")
  - 支持流媒体的[Windows Media
    Player](../Page/Windows_Media_Player.md "wikilink") 8
  - Microsoft Reader 2
  - 支援對[Palm OS的文件傳輸](../Page/Palm_OS.md "wikilink")
  - 改進Pocket [Outlook](../Page/Outlook.md "wikilink")
  - 支援[數字版權管理](../Page/數字版權管理.md "wikilink")（DRM）的Microsoft Reader

### Windows Mobile 2003

[2004-2008_Ford_Territory_TX_wagon_(Windows_Mobile)_01.jpg](https://zh.wikipedia.org/wiki/File:2004-2008_Ford_Territory_TX_wagon_\(Windows_Mobile\)_01.jpg "fig:2004-2008_Ford_Territory_TX_wagon_(Windows_Mobile)_01.jpg")
Windows Mobile 2003的原代號為"Ozone"\[9\]，發布於2003年6月23日，是第一個版本的Windows
Mobile。它共有四個版本："Windows Mobile 2003 for Pocket PC Premium Edition",
"Windows Mobile 2003 for Pocket PC Professional Edition", "Windows
Mobile 2003 for Smartphone"及"Windows Mobile 2003 for Pocket PC Phone
Edition"。除專業版外，其他版本都缺少了一些功能，如[客戶端的](../Page/客戶端.md "wikilink")[L2TP及](../Page/L2TP.md "wikilink")[IPsec](../Page/IPsec.md "wikilink")
[VPN等](../Page/VPN.md "wikilink")。

新功能及內置的應用程序：\[10\]

  - 支援附加的鍵盤
  - 增強的通信接口與[藍牙設備管理](../Page/藍牙.md "wikilink")
  - 支持藍牙文件傳輸
  - 支持藍牙耳機
  - 支援圖片浏览、剪裁及傳送
  - 碎石機遊戲（[Jawbreaker](../Page/Jawbreaker.md "wikilink")）
  - 增強Pocket
    [Outlook對](../Page/Outlook.md "wikilink")[vCard的支援](../Page/vCard.md "wikilink")
  - 改進的Pocket [Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")
  - [Windows Media Player](../Page/Windows_Media_Player.md "wikilink")
    9.0及優化
  - [SMS](../Page/SMS.md "wikilink")[短信回复可選擇電話](../Page/短信.md "wikilink")
  - [MIDI文件支援](../Page/MIDI.md "wikilink")

### Windows Mobile 2003 SE

Windows Mobile 2003 SE是Windows Mobile 2003的第二版，發布於2004年3月24日。

新功能及內置的應用程序：

  - 对于Pocket PC，可以切换屏幕方向
  - 支援单列布局的Pocket [Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")
  - 支援[VGA](../Page/VGA.md "wikilink")（640x480），240x240和480x480的屏幕解像度
  - 支援[Wi-Fi](../Page/Wi-Fi.md "wikilink") Protected
    Access（[WPA](../Page/WPA.md "wikilink")）

### Windows Mobile 5

Windows Mobile 5的代號為"Magneto"（磁電機），釋放於Microsoft's Mobile and Embedded
Developers Conference
2005[拉斯維加斯](../Page/拉斯維加斯.md "wikilink")（2005年5月9日至5月12日）。微軟打算提供主流支持的Windows
Mobile 5，直到2010年10月12日，並擴展支持到2015年10月13日。

Windows Mobile 5包括[Microsoft Exchange
Server的](../Page/Microsoft_Exchange_Server.md "wikilink")[push
mail功能](../Page/push_mail.md "wikilink")，改進與Exchange 2003
SP2的工作\[11\]。push mail功能也需要供應商/設備支持。隨著AKU2軟件升級所有Windows Mobile
5都支持DirectPush\[12\] 。 由於需要持續的存儲能力，Windows Mobile 5精選增加電池壽命。

新功能及內置的應用程序：

  - 新版本的Office被稱為“Office Mobile”
      - [PowerPoint](../Page/PowerPoint.md "wikilink") Mobile
      - 支援圖表功能的[Excel](../Page/Excel.md "wikilink") Mobile
      - 支援圖表插入的[Word](../Page/Word.md "wikilink") Mobile
  - [Windows Media Player](../Page/Windows_Media_Player.md "wikilink")
    10 Mobile
  - 照片來電顯示
  - 支援[DirectShow](../Page/DirectShow.md "wikilink")
  - 圖片和視頻包，它融合了管理的影片和照片
  - 增強支持[藍牙](../Page/藍牙.md "wikilink")
  - [全球定位系統](../Page/全球定位系統.md "wikilink")（GPS）的管理界面
  - 支援[默認的](../Page/默認.md "wikilink")[QWERTY](../Page/QWERTY.md "wikilink")[鍵盤](../Page/鍵盤.md "wikilink")
  - 类似[Windows的錯誤報告機制類](../Page/Windows.md "wikilink")
  - ActiveSync 4.2增加了15 ％的速度同步

### Windows Mobile 6

[T-01B.jpg](https://zh.wikipedia.org/wiki/File:T-01B.jpg "fig:T-01B.jpg")
T-0B\]\] Windows Mobile
6的原代號為"Crossbow"（石弓）\[13\]，釋放於在2007年2月12日的2007年3GSM世界大會（3GSM
World Congress）\[14\]。它有三個不同的版本：“Windows Mobile 6
Standard”(用在智能手機，沒有觸控摸屏），“Windows Mobile 6
Professional”（用在智能手機，有觸控摸屏，以及“Windows Mobile 6
Classic”（用在掌上电脑，沒有[無線電](../Page/無線電.md "wikilink")）\[15\]。

就像當時新推出的[Windows
Vista作業系統一樣](../Page/Windows_Vista.md "wikilink")，Windows
Mobile 6的含義是相同的設計。功能上，它很接近Windows Mobile 5，但有更好的穩定性和更美觀。

新功能及內置的應用程序：\[16\]

  - 支援320x320和800x480像素屏幕解析度
  - 支援移動辦公的[智能手機](../Page/智能手機.md "wikilink")
  - 作業系統定時[更新](../Page/更新.md "wikilink")\[17\]
  - 改進的遠端桌面訪問\[18\]（只為某些智能手機）\[19\]
  - 支援[VoIP](../Page/VoIP.md "wikilink")（[網絡電話](../Page/網絡電話.md "wikilink")）與AEC（Acoustic
    Echo Cancelling）和MSRT音頻編解碼器
  - Windows Mobile版本的[Windows Live](../Page/Windows_Live.md "wikilink")
    \[20\]
  - 客戶[反饋選項](../Page/反饋.md "wikilink")\[21\]
  - 增強微軟藍牙Stack
  - 存儲卡加密（encryption keys are lost if device is cold-booted）
  - Smartfilter內的[搜索](../Page/搜索.md "wikilink")[程序](../Page/程序.md "wikilink")
  - 改進[互聯網共享](../Page/互聯網.md "wikilink")
  - 令用[Outlook.com支援](../Page/Outlook.com.md "wikilink")[HTML](../Page/HTML.md "wikilink")[電子郵件](../Page/電子郵件.md "wikilink")
  - Search ability for contacts in an Exchange Server Address Book
  - 在[Internet Explorer](../Page/Internet_Explorer.md "wikilink")
    Mobile上支援[AJAX](../Page/AJAX.md "wikilink")，[JavaScript及XMLDOM](../Page/JavaScript.md "wikilink")
  - Out of Office Replies with Microsoft Exchange 2007
  - 支援選擇非[授權移動接入](../Page/授權.md "wikilink")（Unlicensed Mobile Access）運營商
  - Microsoft Exchange
    2007的[伺服器](../Page/伺服器.md "wikilink")[搜索](../Page/搜索.md "wikilink")
  - [.NET](../Page/.NET.md "wikilink") Compact Framework v2 SP2預先安裝在光盤
  - Microsoft [SQL](../Page/SQL.md "wikilink") Server
    2005[壓縮版預先安裝在光盤](../Page/壓縮.md "wikilink")
  - 新增OneNote Mobile到Microsoft Office
  - 支持[Microsoft Office](../Page/Microsoft_Office.md "wikilink")
    2007、2010、2013的文件格式（pptx，docx，xlsx）
  - 同時，程式設計的客戶端軟體只支持 Windows XP Professional SP3以上之作業系統。

### Windows Mobile 6.1

Windows Mobile 6.1于2008年4月1日推出。這是從Windows Mobile
6一個小的升級，但帶來了各種性能增強，重新設計的主屏幕採用橫向設計，擴大對點擊的支援能顯示更多信息，這部分更新僅在Windows
Mobile的Standard版本。但是，這項功能卻是莫名其妙地排除在專業版中。\[22\]
其他一些改進，如会话式的短信界面，支持整页缩放的[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")
Mobile和'域名註冊'。還補充說，隨著"mobile"版本的Microsoft
OneNote及互動"入門"嚮導的域名註冊功能\[23\]將設備連接到System Center Mobile Device Manager
2008, a product to manage mobile devices.
最突出的差異是，用戶的標準版（如舊版）仍然與電話號碼中的任務和約會創造了自動鏈接，從而能夠很容易（和安全駕駛時）點擊和撥打在電話中儲存的電話號碼。出於某種原因，專業版也消除了這一重要功能。Windows
Mobile
6.1還精選提高push-email的利用效率，在其隨身電郵議定書“ActiveSync的”的增加高達40％，這減少了數據的使用，所以造成電池壽命大大改善。\[24\]
除了視覺和功能的區別，基本上可以用[Windows
CE的版本來區分Windows](../Page/Windows_CE.md "wikilink") Mobile
6和Windows Mobile 6.1。Windows Mobile 6的Windows
CE號碼[小數點後有](../Page/小數點.md "wikilink")4個數字（如[HTC
Wings的](../Page/HTC_Wings.md "wikilink")5.2.1622），而Windows Mobile
6.1的Windows
CE號碼[小數點就是有](../Page/小數點.md "wikilink")5個數字（如[Palm](../Page/Palm.md "wikilink")
Treo 800w的5.2.19216）。

### Windows Mobile 6.5

[T-Mobile_Dash_wiki.jpg](https://zh.wikipedia.org/wiki/File:T-Mobile_Dash_wiki.jpg "fig:T-Mobile_Dash_wiki.jpg")
Windows Mobile 6.5是一個对现有Wndows Mobile
6.1平台的升级\[25\]，釋放给製造商的時間是09年5月，但在9月之後才正式推出。\[26\]儘管可以說是一個更新，但據說包括大量新的增值功能。\[27\]它也將包括新的Internet
Explorer Mobile browser dubbed IE "6 On 6"。\[28\]微軟在MWC
09宣佈推出這版本的Windows Mobile\[29\]
同時，Motorola宣佈下半年推出的手機大多都會使用Windows
Mobile 6.5。\[30\]而使用這個版本的Windows Mobile的手機會被稱為"Windows phones"。隨著Windows
mobile
6.5的推出，微軟宣布了幾個[雲計算服務代號](../Page/雲計算.md "wikilink")"[SkyBox](../Page/SkyBox.md "wikilink")",
"[SkyLine](../Page/SkyLine.md "wikilink")",
"[SkyMarket](../Page/SkyMarket.md "wikilink")"。\[31\]"SkyBox"已被證實是一个类似[My
Phone的功能](../Page/My_Phone.md "wikilink")，\[32\]而"SkyMarket"则被證實是Windows
Marketplace的Windows
Mobile版本\[33\]。主屏幕已被重新設計，使此版本將被設計為令手指更容易使用。而据推测微软将渐渐放弃非触摸屏式的Windows
Mobile手机（即Windows Mobile
Standard系列，亦即在更早的版本中被称为Smartphone的手机）\[34\]微軟還計劃把幾個來自[Zune的軟件或功能新增至這個版本](../Page/Zune.md "wikilink")。\[35\]

### Windows Mobile 6.5.3

2010年2月2日，索尼愛立信M1i手機搭載Windows Mobile 6.5.3正式發布，成為世界上第一台Windows Phone
6.5.3智能手機。2009年11月以來，一些6.5.3版本開始洩漏（28nnn），並已經非正式地移植到一些Windows手機。

Windows Mobile
6.5.3帶來更友好的用戶界面，一些新的易於使用的功能，如支持多點觸摸，完全觸摸控制（即無需手寫筆），和拖放開始菜單圖標。Internet
Explorer Mobile 6也有一些重大更新，包括減少頁面載入時間，改進的內存管理和平滑手勢。

較新的附加功能的Windows Mobile 6.5.3版本包括線上電子郵件和Office Mobile 2010。

### 版本比较

| 版本                     | 發布日期       | 附註                      | 基於                                                 |
| ---------------------- | ---------- | ----------------------- | -------------------------------------------------- |
| Pocket PC 2000         | 2000年4月19日 | 里程碑                     | [Windows CE](../Page/Windows_CE.md "wikilink") 3.0 |
| Pocket PC 2002         | 2002年10月   |                         | [Windows CE](../Page/Windows_CE.md "wikilink") 4.0 |
| Windows Mobile 2003    | 2003年6月23日 | 第一個版本的Windows Mobile    | [Windows CE](../Page/Windows_CE.md "wikilink") 4.1 |
| Windows Mobile 2003 SE | 2004年3月24日 | Windows Mobile 2003的第二版 | [Windows CE](../Page/Windows_CE.md "wikilink") 4.2 |
| Windows Mobile 5       | 2005年5月12日 |                         | [Windows CE](../Page/Windows_CE.md "wikilink") 5.0 |
| Windows Mobile 6       | 2007年2月12日 |                         | [Windows CE](../Page/Windows_CE.md "wikilink") 5.1 |
| Windows Mobile 6.1     | 2008年4月1日  |                         | [Windows CE](../Page/Windows_CE.md "wikilink") 5.2 |
| Windows Mobile 6.5     | 2009年5月11日 |                         | [Windows CE](../Page/Windows_CE.md "wikilink") 5.2 |
| Windows Mobile 6.5.3   | 2010年2月2日  |                         | [Windows CE](../Page/Windows_CE.md "wikilink") 5.2 |
|                        |            |                         |                                                    |

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>Pocket PC 2000</p></td>
<td><p>Pocket PC 2002</p></td>
<td><p>Windows Mobile 2003</p></td>
<td><p>Windows Mobile 2003 S</p></td>
<td><p>Windows Mobile 5</p></td>
<td><p>Windows Mobile 6</p></td>
<td><p>Windows Mobile 6.1</p></td>
<td><p>Windows Mobile 6.5</p></td>
</tr>
<tr class="even">
<td><p>Pocket PC<br />
（不支持电话功能）</p></td>
<td><p>Pocket PC 2000</p></td>
<td><p>Pocket PC 2002</p></td>
<td><p>Windows Mobile 2003 for Pocket PC</p></td>
<td><p>N/A</p></td>
<td><p>Windows Mobile 5 for Pocket PC</p></td>
<td><p>Windows Mobile 6 Classic</p></td>
<td><p>Windows Mobile 6.1 Classic</p></td>
<td><p>N/A</p></td>
</tr>
<tr class="odd">
<td><p>Pocket PC Phone<br />
（支援<a href="../Page/觸摸屏.md" title="wikilink">觸摸屏</a>）</p></td>
<td><p>Pocket PC 2000 Phone Edition</p></td>
<td><p>Pocket PC 2002 Phone Edition</p></td>
<td><p>Windows Mobile 2003 for Pocket PC Phone Edition</p></td>
<td><p>Windows Mobile 2003 SE for Pocket PC Phone Edition</p></td>
<td><p>Windows Mobile 5 for Pocket PC Phone Edition</p></td>
<td><p>Windows Mobile 6 Professional</p></td>
<td><p>Windows Mobile 6.1 Professional</p></td>
<td><p>Windows Mobile 6.5 Professional</p></td>
</tr>
<tr class="even">
<td><p>Smartphone<br />
（不支援觸摸屏）</p></td>
<td><p>N/A</p></td>
<td><p>Smartphone 2002</p></td>
<td><p>Windows Mobile 2003 for Smartphone</p></td>
<td><p>Windows Mobile 2003 SE for Smartphone</p></td>
<td><p>Windows Mobile 5 for Smartphone</p></td>
<td><p>Windows Mobile 6 Standard</p></td>
<td><p>Windows Mobile 6.1 Standard</p></td>
<td><p>Windows Mobile 6.5 Standard</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## Windows Mobile的继任者

### Windows Phone

2010年10月11日，微軟公司正式發布了新的智慧型手機作業系統[Windows
Phone](../Page/Windows_Phone.md "wikilink")，用以接續舊版的Windows Mobile
系列，Windows Mobile就此走入了歷史。

## 市場競爭

微軟研發的Windows Mobile平台，比蘋果[iOS和和Google](../Page/iOS.md "wikilink")
[Android平台早推出數年](../Page/Android.md "wikilink")，但根據市場研究機構IDC估計，2012年Windows
Phone加上Windows
Mobile只佔智能手機市場的5.2%，落後Android的61%、iOS的20.5%，2012年2月，微軟創辦人[比尔盖茨接受哥倫比亞廣播公司](../Page/比尔盖茨.md "wikilink")（CBS）訪-{}-問，對於流動通訊新時代，他坦言微軟創新不足。\[36\]

## Windows Mobile裝置列表

使用**Windows Mobile**為作業系統之裝置列表：\[37\]

[A3100](../Page/Motorola_A3100.md "wikilink"){{·}}[Ace](../Page/Samsung_SPH-i325.md "wikilink"){{·}}
[Athena](../Page/HTC_Advantage_X7500.md "wikilink"){{·}}
[BlackJack](../Page/Samsung_SGH-i607.md "wikilink"){{·}} [BlackJack
II](../Page/Samsung_SGH-i617.md "wikilink"){{·}}
[Dash](../Page/HTC_Excalibur.md "wikilink"){{·}}
[DX900](../Page/Acer_DX900.md "wikilink"){{·}}
[eXpo](../Page/LG_eXpo.md "wikilink"){{·}}
[HD2](../Page/HTC_HD2.md "wikilink"){{·}}
[i600](../Page/Samsung_SGH-i600.md "wikilink"){{·}}
[i780](../Page/Samsung_SGH-i780.md "wikilink"){{·}}
[Incite](../Page/LG_CT810_Incite.md "wikilink"){{·}}
[Jack](../Page/Samsung_SGH-i637.md "wikilink"){{·}}
[KS20](../Page/LG_KS20.md "wikilink"){{·}} [MAX
4G](../Page/HTC_Max_4G.md "wikilink"){{·}} [nüvifone
M10](../Page/Garmin_Nüvifone.md "wikilink"){{·}} [nüvifone
M20](../Page/Garmin_Nüvifone.md "wikilink"){{·}}
[Omnia](../Page/Samsung_SGH-i900.md "wikilink"){{·}} [Omnia
II](../Page/Samsung_i8000.md "wikilink"){{·}} [Omnia
Lite](../Page/Samsung_B7300.md "wikilink"){{·}} [Omnia Pro
B7330](../Page/Samsung_GT-B7330.md "wikilink"){{·}} [Omnia Pro
B7610](../Page/Samsung_B7610.md "wikilink"){{·}} [Propel
Pro](../Page/Samsung_SGH-i627.md "wikilink"){{·}}
[Saga](../Page/Samsung_SCH-i770.md "wikilink"){{·}}
[Shadow](../Page/HTC_Phoebus.md "wikilink"){{·}}
[Smartflip](../Page/HTC_Startrek.md "wikilink"){{·}}
[TG01](../Page/Toshiba_TG01.md "wikilink"){{·}}
[Touch](../Page/HTC_Touch.md "wikilink"){{·}}
[Touch2](../Page/HTC_Touch2.md "wikilink"){{·}} [Touch
3G](../Page/HTC_Touch_3G.md "wikilink"){{·}} [Touch
Cruise](../Page/HTC_Touch_Cruise.md "wikilink"){{·}} [Touch
Diamond](../Page/HTC_Touch_Diamond.md "wikilink"){{·}} [Touch
Diamond2](../Page/HTC_Touch_Diamond2.md "wikilink"){{·}} [Touch
Dual](../Page/HTC_Touch_Dual.md "wikilink"){{·}} [Touch
HD](../Page/HTC_Touch_HD.md "wikilink"){{·}} [Touch
Pro](../Page/HTC_Touch_Pro.md "wikilink"){{·}} [Touch
Pro2](../Page/HTC_Touch_Pro2.md "wikilink"){{·}} [Touch
Viva](../Page/HTC_Touch_Viva.md "wikilink"){{·}} [Treo
Pro](../Page/Palm_Treo_Pro.md "wikilink"){{·}}
[TyTN](../Page/HTC_TyTN.md "wikilink"){{·}} [TyTN
II](../Page/HTC_TyTN_II.md "wikilink"){{·}}
[Vox](../Page/HTC_S710.md "wikilink"){{·}}
[Wing](../Page/HTC_P4350.md "wikilink"){{·}}
[Wings](../Page/HTC_S730.md "wikilink"){{·}} [Xperia
X1](../Page/Sony_Ericsson_Xperia_X1.md "wikilink"){{·}} [Xperia
X2](../Page/Sony_Ericsson_Xperia_X2.md "wikilink"){{·}}[LG
GW825v](../Page/LG_GW825v.md "wikilink")

## 参考文献

{{-}}

[Windows_Mobile](../Category/Windows_Mobile.md "wikilink")
[Category:微軟作業系統](../Category/微軟作業系統.md "wikilink")
[Category:嵌入式操作系统](../Category/嵌入式操作系统.md "wikilink")
[Category:行動作業系統](../Category/行動作業系統.md "wikilink")

1.
2.  De Herrera, Chris. [*More Than a
    PDA\!*](http://www.pocketpcmag.com/cms/_archives/May00/MorethanPDA.asp)
    . Pocket PC Magazine. Retrieved 14 September 2007.

3.

4.  Morris, John; Taylor, Josh, [*Microsoft jumps in the all-in-one
    game*](https://web.archive.org/web/20011217080411/http://www.zdnet.com/products/stories/reviews/0,4161,2824082,00.html),
    zdnet.com, Retrieved from the [Internet
    Archive](../Page/Internet_Archive.md "wikilink") 6 September 2007.

5.  [*Announcing the New Pocket
    PC 2002*](https://web.archive.org/web/20020124155709/http://www.microsoft.com/mobile/pocketpc/pocketpc2002/default.asp),
    Microsoft, Retrieved from the [Internet
    Archive](../Page/Internet_Archive.md "wikilink") 6 September 2007.

6.  Gray, Douglas. [*HP to unveil Jornada 560 series of
    handhelds*](http://www.itworld.com/Comp/1279/IDG010906jornada/) .
    ITWorld.com. Retrieved 14 September 2007.

7.  Gray, Douglas. [*Palming new handhelds: Pocket
    PC 2002*](http://archives.cnn.com/2001/TECH/ptech/10/05/new.handhelds.idg/)
    . CNN. Retrieved 14 September 2007.

8.  De Herrera, Chris. [*The Pocket PC 2002 Gets More Features for Work
    and
    Play*](http://www.pocketpcmag.com/_archives/jan02/PocketPC2002.asp)
    . Pocket PC Magazine. Retrieved 14 September 2007.

9.
10. De Herrera, Chris. [*Windows
    Mobile 2003*](http://www.pocketpcmag.com/_archives/nov03/windowsmobile2003.asp)
    . Pocket PC Magazine. Retrieved 14 September 2007.

11.

12.

13.
14.

15.

16.

17. ["Image Gallery: Windows Mobile 6 Professional screenshots, Windows
    Update,
    screen 1"](http://content.zdnet.com/2346-12553_22-54353-24.html) .
    *ZDNet*. Retrieved 10 October 2007.

18. [Improved Remote Desktop
    access](http://content.zdnet.com/2346-12553_22-54353-19.html)

19. [RDP Client is not included in many WM6
    devices](http://www.theunwired.net/?itemid=4029)

20.

21. [Customer Feedback Option](http://www.microsoft.com/products/ceip)

22. [Experiencing the goodness that is Windows
    Mobile 6.1](http://windowsvistablog.com/blogs/windowsexperience/archive/2008/04/01/experiencing-the-goodness-that-is-windows-mobile-6-1.aspx)

23.

24. <http://blogs.msdn.com/devmktg/archive/2008/04/04/windows-mobile-6-1-announced.aspx>

25.

26.

27.

28.

29.

30.
31.

32.

33.

34.

35.

36. [蓋茨：微軟錯了手機發展創新不足](https://archive.is/20130428163457/hk.news.yahoo.com/%E8%93%8B%E8%8C%A8-%E5%BE%AE%E8%BB%9F%E9%8C%AF%E4%BA%86-%E6%89%8B%E6%A9%9F%E7%99%BC%E5%B1%95-%E5%89%B5%E6%96%B0%E4%B8%8D%E8%B6%B3-211849394.html)Yahoo\!
    新聞香港明報2013年2月20日

37. <http://chinese.engadget.com/2009/10/08/windows-mobile-6-5-hk-list/>