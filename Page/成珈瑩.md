**成珈瑩**（'''Shing Ka Ying
'''，），小時候曾為[香港童星](../Page/香港童星.md "wikilink")，曾就讀[馬鞍山靈糧小學](../Page/馬鞍山靈糧小學.md "wikilink")，曾於[宣道會鄭榮之中學就讀初中](../Page/宣道會鄭榮之中學.md "wikilink")，[東莞工商總會劉百樂中學就讀高中](../Page/東莞工商總會劉百樂中學.md "wikilink")。

首個[無綫電視](../Page/無綫電視.md "wikilink")[電視劇演出作品是](../Page/電視劇.md "wikilink")《[無業樓民](../Page/無業樓民.md "wikilink")》，不過最為人認識的是在《[烈火雄心II](../Page/烈火雄心II.md "wikilink")》中饰演紀瑶（[王喜飾演之紀德田的女兒](../Page/王喜.md "wikilink")），她還演出多個廣告，她的哥哥[成展權也是香港童星](../Page/成展權.md "wikilink")。

## 曾参演剧集

  - 2000年：《[无业楼民](../Page/无业楼民.md "wikilink")》 饰 古慧真
  - 2000年：《[十月初五的月光](../Page/十月初五的月光.md "wikilink")》 饰 祝君好（童年）
  - 2000年：《[庙街·妈·兄弟](../Page/庙街·妈·兄弟.md "wikilink")》
  - 2001年：《[美丽人生](../Page/美丽人生_\(香港電視劇\).md "wikilink")》
  - 2001年：《[廉政追击](../Page/廉政追击.md "wikilink")—補習班》 饰 恩恩
  - 2001年：《[勇往直前](../Page/勇往直前.md "wikilink")》 饰 真真
  - 2001年：《[陀枪师姐III](../Page/陀枪师姐III.md "wikilink")》 饰 程莎莎
  - 2002年：《[烈火雄心II](../Page/烈火雄心II.md "wikilink")》 饰 纪瑤
  - 2002年：《[轉世驚情](../Page/轉世驚情.md "wikilink")》 飾 畢應來（畢兒）
  - 2003年：《[扑水冤家](../Page/扑水冤家.md "wikilink")》 飾 常喜寧
  - 2003年：《[黑夜彩虹](../Page/黑夜彩虹.md "wikilink")》 饰 常小蕾
  - 2003年：《[鳳舞香羅](../Page/鳳舞香羅.md "wikilink")》 饰 天愛
  - 2004年：《[翡翠恋曲](../Page/翡翠恋曲.md "wikilink")》 饰 孙燕秋（童年）
  - 2006年：《[天幕下的恋人](../Page/天幕下的恋人.md "wikilink")》 饰 高逸诗（童年）

### [香港電台節目](../Page/香港電台.md "wikilink")

  - 2003年：《[黃金歲月](http://app4.rthk.hk/special/rthkmemory/details/tv-drama/497)：友情/活力/生活與藝術》
    飾 瑩瑩

## 曾参演廣播劇

  - 2003年：香港電台《[精靈一點．一塵不染樂安居](../Page/精靈一點．一塵不染樂安居.md "wikilink")》 饰 小南

## 曾獲獎項

  - [2002年度萬千星輝賀台慶](../Page/2002年度萬千星輝賀台慶得獎名單.md "wikilink")：本年度我最喜愛的電視角色
    - 《[烈火雄心II](../Page/烈火雄心II.md "wikilink")》：紀瑤（瑤瑤）
  - [演藝動力大獎2002](../Page/演藝動力大獎.md "wikilink")：我最支持的演藝動力大獎 -
    《[烈火雄心II](../Page/烈火雄心II.md "wikilink")》

## 參考

  - [图揭港剧小童星容貌变化
    对比惊人](http://www.hinews.cn/news/system/2014/08/29/016908885.shtml)

[Category:香港前兒童演員](../Category/香港前兒童演員.md "wikilink")
[ka](../Category/成姓.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:宣道會鄭榮之中學校友](../Category/宣道會鄭榮之中學校友.md "wikilink")
[Category:東莞工商總會劉百樂中學校友](../Category/東莞工商總會劉百樂中學校友.md "wikilink")