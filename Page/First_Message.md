《**First
Message**》（初次告白）為日本歌手[絢香第一張原創專輯](../Page/絢香.md "wikilink")，於2006年11月1日發行。在日本[公信榜最高的紀錄為第](../Page/公信榜.md "wikilink")1名。

## 解說

  - 這張唱片是絢香出道後的第一張專輯。收錄了先前發行過的4張單曲，以及絢香初次作曲的「message」等15首歌。但『I
    believe』中的「夢的碎片」、『Real voice』收錄的「Peace loving
    people」、『三日月』中的「」並沒有收錄。

<!-- end list -->

  - 這張專輯的首批30萬張CD中，每100張會有1張附贈「GOLD
    TICKET」，讓幸運買到的樂迷，可以到2006年11月18日開始的全國巡迴演唱會現場，兌換絢香的神祕DVD。

## 發行版本

  - CD ONLY

## 曲目

1.  **Start to 0(Love)**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
        [日本電視台與民間放送](../Page/日本電視台.md "wikilink")43社共同制作「第85回全国高校足球選手權大會」形象歌曲。
        另外，隔年發行5th單曲「[Jewelry
        day](../Page/Jewelry_day.md "wikilink")」的第3曲收錄本曲的Acoustic
        Ver.。
2.  **[Real voice](../Page/Real_voice.md "wikilink")**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
        [富士電視台連續劇](../Page/富士電視台.md "wikilink")「[戀愛維他命](../Page/戀愛維他命.md "wikilink")」主題歌。
3.  **Sha la la**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：L.O.E
4.  **Blue days**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
        [富士電視台連續劇](../Page/富士電視台.md "wikilink")「[戀愛維他命](../Page/戀愛維他命.md "wikilink")」插入曲。
5.  **[I believe](../Page/I_believe.md "wikilink")**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：L.O.E
        [TBS電視台連續劇](../Page/TBS電視台.md "wikilink")「[輪舞曲](../Page/輪舞曲.md "wikilink")」主題曲。
6.  **Stay with me**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：L.O.E
7.  **[melody](../Page/melody〜SOUNDS_REAL〜.md "wikilink")**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
8.  **你的力量與成熟模樣（）**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：L.O.E
9.  **永遠的故事（）**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
10. **讓時光倒流（）**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：上杉洋史
11. **1・2・3・4**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
        至今唯一以男性觀點所寫的曲子。
12. **Story**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：L.O.E
13. **Lai la lai（）**
      -
        作詞：絢香/作曲：西尾芳彦/編曲：L.O.E
14. **[三日月](../Page/三日月.md "wikilink")**
      -
        作詞：絢香/作曲：西尾芳彦、絢香/編曲：L.O.E
15. **message**
      -
        作詞：絢香/作曲：絢香/編曲：絢香
        以[無伴奏合唱構成的歌曲](../Page/無伴奏合唱.md "wikilink")。

[Category:絢香音樂專輯](../Category/絢香音樂專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:2006年Oricon專輯月榜冠軍作品](../Category/2006年Oricon專輯月榜冠軍作品.md "wikilink")
[Category:2006年Oricon專輯週榜冠軍作品](../Category/2006年Oricon專輯週榜冠軍作品.md "wikilink")
[Category:日本華納音樂音樂專輯](../Category/日本華納音樂音樂專輯.md "wikilink")