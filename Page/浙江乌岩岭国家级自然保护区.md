[Baiyun_Peak,_Wuyanling_National_Nature_Reserve.JPG](https://zh.wikipedia.org/wiki/File:Baiyun_Peak,_Wuyanling_National_Nature_Reserve.JPG "fig:Baiyun_Peak,_Wuyanling_National_Nature_Reserve.JPG")
**浙江乌岩岭国家级自然保护区**位于[中国](../Page/中国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[温州市](../Page/温州市.md "wikilink")[泰顺县西北部](../Page/泰顺县.md "wikilink")，浙南沿海山地，与[福建省](../Page/福建省.md "wikilink")[福安市](../Page/福安市.md "wikilink")、[温州](../Page/温州.md "wikilink")[文成县](../Page/文成县.md "wikilink")、[丽水景宁](../Page/丽水.md "wikilink")[少数民族](../Page/少数民族.md "wikilink")[自治县相邻](../Page/自治县.md "wikilink")。保护区面积18861.5[公顷](../Page/公顷.md "wikilink")，主要保护对象为黄腹角雉及其生态环境。

## 概况

乌岩岭处泰顺境内洞宫山脉，属“南岭闽瓯中亚[热带](../Page/热带.md "wikilink")[海洋性](../Page/海洋.md "wikilink")[季风气候](../Page/季风.md "wikilink")”：四季分明、雨水充沛。年平均气温15.2[℃](../Page/℃.md "wikilink")，年降水量2200毫米，非常适宜动植物的生长，故该区物种丰富。[动](../Page/动物.md "wikilink")[植物资源的分布与组成上有明显的](../Page/植物.md "wikilink")[区域性和过渡性等特征](../Page/区域性.md "wikilink")，系典型的华南山区，为理想的生物多样性保护和科研基地。

## 动物

乌岩岭国家级自然保护区内有[脊椎动物约占浙江省种类的](../Page/脊椎动物.md "wikilink")53%，共27目342种，另有[昆虫](../Page/昆虫.md "wikilink")15目1041种，[蝴蝶类约](../Page/蝴蝶.md "wikilink")110种。保护区内还栖息着约400多只[中国特有珍禽](../Page/中国.md "wikilink")­—[黄腹角雉](../Page/黄腹角雉.md "wikilink")（Tragopan
caboti），是国际濒危物种《红皮书》上列名的“濒危物种”，乌岩岭是[中国唯一的黄腹角雉保护区和主要繁殖地](../Page/中国.md "wikilink")。

  - 国家一级保护野生动物8种：[黄腹角雉](../Page/黄腹角雉.md "wikilink")、[云豹](../Page/云豹.md "wikilink")、[金钱豹](../Page/金钱豹.md "wikilink")、[华南虎](../Page/华南虎.md "wikilink")、[黑麂](../Page/黑麂.md "wikilink")、[金雕](../Page/金雕.md "wikilink")、[白颈长尾雉](../Page/白颈长尾雉.md "wikilink")、[鼋](../Page/鼋.md "wikilink")。
  - 国家二级保护野生动物42种：[穿山甲](../Page/穿山甲.md "wikilink")、[豺](../Page/豺.md "wikilink")、[猕猴](../Page/猕猴.md "wikilink")、[白鹇](../Page/白鹇.md "wikilink")、[斑羚等](../Page/斑羚.md "wikilink")。

## 植物

乌岩岭国家级自然保护区地处南北[植物交汇区](../Page/植物.md "wikilink")，植物种类繁多，中亚热带常绿阔叶林保存完整。保护区内植物种类约占浙江省植物种类的50%：[种子植物](../Page/种子植物.md "wikilink")158科1863种，[蕨类植物](../Page/蕨类植物.md "wikilink")45科287种；[苔藓植物](../Page/苔藓植物.md "wikilink")58科358种，真菌61科212种，其中包括24种国家重点保护的野生植物，是天然的“生物基因库”。

  - 国家一级保护野生植物4种：[中华水韭](../Page/中华水韭.md "wikilink")、[莼菜](../Page/莼菜.md "wikilink")、[南方红豆杉](../Page/南方红豆杉.md "wikilink")、[伯乐树](../Page/伯乐树.md "wikilink")。
  - 国家二级保护野生植物20种：[金毛狗](../Page/金毛狗.md "wikilink")、[粗齿桫椤](../Page/粗齿桫椤.md "wikilink")、[金钱松](../Page/金钱松.md "wikilink")、[华东黄杉](../Page/华东黄杉.md "wikilink")、[香樟](../Page/香樟.md "wikilink")、[浙江楠](../Page/浙江楠.md "wikilink")、[花榈木](../Page/花榈木.md "wikilink")、[鹅掌楸](../Page/鹅掌楸.md "wikilink")、[金荞麦](../Page/金荞麦.md "wikilink")、[香果树](../Page/香果树.md "wikilink")、[蛛网萼等](../Page/蛛网萼.md "wikilink")。

## 历史

  - 1975年建立省级自然保护区
  - 1994年成为[中国国家级自然保护区](../Page/中国国家级自然保护区.md "wikilink")

## 参考文献

[Category:泰顺县](../Category/泰顺县.md "wikilink")
[W](../Category/温州旅游.md "wikilink")
[W](../Category/浙江地理.md "wikilink")