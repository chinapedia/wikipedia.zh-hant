**阿比维尔**（**City of
Abbeville**）是[美国](../Page/美国.md "wikilink")[亚拉巴马州](../Page/亚拉巴马州.md "wikilink")[亨利县的一座城市](../Page/亨利县_\(亚拉巴马州\).md "wikilink")。根据[美国2000年人口普查](../Page/美国2000年人口普查.md "wikilink")，共有人口2987人。根据2005年的估计，阿比维尔共有人口2,963人。\[1\]阿比维尔是[亨利县的](../Page/亨利县_\(亚拉巴马州\).md "wikilink")[县治](../Page/县治.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[City of Abbeville](../Category/亚拉巴马州城市.md "wikilink") [City of
Abbeville](../Category/亨利县_\(亚拉巴马州\).md "wikilink")

1.