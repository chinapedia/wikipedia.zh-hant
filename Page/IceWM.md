**IceWM**是由Marko Maček开发的，用于[X
Window系统的堆叠式窗口管理器](../Page/X_Window系统.md "wikilink")，使用[C++编写并依据](../Page/C++.md "wikilink")[GNU宽通用公共许可证的条款发布](../Page/GNU宽通用公共许可证.md "wikilink")。\[1\]它对CPU和内存的占用比较小，并且带有主题支持，可以模仿[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
XP](../Page/Windows_XP.md "wikilink")、[Windows
7](../Page/Windows_7.md "wikilink")、[OS/2](../Page/OS/2.md "wikilink")、[Motif和其它](../Page/Motif.md "wikilink")[图形用户界面](../Page/图形用户界面.md "wikilink")。\[2\]IceWM意在提升感观和体验，同时兼顾了轻量和可定制性。

IceWM可以通过存放在[家目录的纯文本文件进行配置](../Page/家目录.md "wikilink")\[3\]，以便于自定义和复制设置。IceWM有一个可选的，带有菜单、任务管理器、网络和CPU仪表、邮件检查和可配置时钟的[任务栏](../Page/任务栏.md "wikilink")。对于[GNOME和](../Page/GNOME.md "wikilink")[KDE程序菜单的支持](../Page/KDE.md "wikilink")，曾在一个单独的软件包中提供，但是在最近的IceWM版本中已经内置这些功能。另外也有用于配置和编辑菜单的外部图形界面工具提供。\[4\]

## 用途

IceWM被用于和轻量版的默认窗口管理器。

[華碩](../Page/華碩.md "wikilink")[Eee
PC的简单模式桌面使用IceWM](../Page/華碩Eee_PC.md "wikilink")。\[5\]

用于树莓派1、2、3的[openSUSE使用IceWM为默认轻量用户界面](../Page/openSUSE.md "wikilink")。\[6\]用于树莓派3的[SUSE
Linux Enterprise服务器同样使用IceWM](../Page/SUSE.md "wikilink")。\[7\]

## 截图

Icewmstartmenu.png|IceWM的开始菜单与Windows 95相似 IceWM 1.3.ogv|IceWM
1.3在Debian 7 Linux上运行

## 参见

  -
  -
  -
  - Spri，[轻量级Linux发行版的前身](../Page/轻量级Linux发行版.md "wikilink")，使用IceWM作为默认用户界面

## 参考文献

## 外部链接

  -
  -
  - [IceWM主题](https://www.opendesktop.org/s/Window-Managers)

  - [IceWM控制面板](http://www.phrozensmoke.com/projects/icewmcp/)

  - [IceWM主题](https://web.archive.org/web/20040826225210/http://www.kde-look.org/index.php?xcontentmode=18)

[Category:1997年软件](../Category/1997年软件.md "wikilink")
[Category:自由桌面环境](../Category/自由桌面环境.md "wikilink")
[Category:用C++編程的自由軟體](../Category/用C++編程的自由軟體.md "wikilink")

1.

2.
3.
4.
5.

6.  [1](https://en.opensuse.org/HCL:Raspberry_Pi3)

7.  [2](http://www.marksei.com/suse-linux-enterprise-server-gains-raspberry-pi-3-support/)