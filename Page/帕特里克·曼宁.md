[Patrick_Manning_2008.jpg](https://zh.wikipedia.org/wiki/File:Patrick_Manning_2008.jpg "fig:Patrick_Manning_2008.jpg")
**帕特里克·奥古斯都·默文·曼宁**（，），生于[特立尼达和多巴哥的](../Page/特立尼达和多巴哥.md "wikilink")[圣费尔南多](../Page/圣费尔南多.md "wikilink")，1991年－1995年，2001年－2010年間擔任該國总理。

2001年12月提前大选後，被总统再次任命为总理，2010年大選落敗後下台。

1986年－2010年是人民民族運動黨領導人。

2016年，他因[急性骨髓性白血病](../Page/急性骨髓性白血病.md "wikilink")，病逝於（San Fernando
General Hospital）\[1\]。

## 参考文献

[category:特立尼达和多巴哥总理](../Page/category:特立尼达和多巴哥总理.md "wikilink")

[Category:罹患白血病逝世者](../Category/罹患白血病逝世者.md "wikilink")
[Category:西印度群島大學校友](../Category/西印度群島大學校友.md "wikilink")

1.