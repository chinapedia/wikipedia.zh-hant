**默西賽德**（**郡**）（），是[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西北部的](../Page/西北英格蘭.md "wikilink")[郡](../Page/英格蘭的郡.md "wikilink")，西臨[愛爾蘭海](../Page/愛爾蘭海.md "wikilink")，郡名源自[梅西河](../Page/梅西河.md "wikilink")，有1,353,600[人口](../Page/人口.md "wikilink")，[佔地](../Page/面積.md "wikilink")645[平方公里](../Page/平方公里.md "wikilink")。在1974年4月1日起成為[名譽郡](../Page/名譽郡.md "wikilink")、[都市郡](../Page/都市郡.md "wikilink")。以人口計算，[利物浦第](../Page/利物浦.md "wikilink")1大（亦是唯一一個）[城市](../Page/英國的城市地位.md "wikilink")、[自治市鎮](../Page/英國的自治市鎮.md "wikilink")（Borough），[威勒爾是第](../Page/威勒爾都會自治市.md "wikilink")2大自治市鎮；[聖海倫斯](../Page/聖海倫斯.md "wikilink")（人口102,629）是第1大鎮（Town），[紹斯波特](../Page/紹斯波特.md "wikilink")（人口99,456）是第2大鎮，[伯肯希德](../Page/伯肯希德.md "wikilink")（人口83,729）是第3大鎮，[布特爾](../Page/布特爾.md "wikilink")（人口77,640）是第4大鎮。

## 地方沿革

《1958年地方政府法案》（Local Government Act 1958）指明英格蘭地方政府委員會（Local Government
Commission for
England）要特別評審默西賽德郡，主要是[布特爾](../Page/布特爾.md "wikilink")、[伯肯希德](../Page/伯肯希德.md "wikilink")（Birkenhead）、[利物浦](../Page/利物浦.md "wikilink")、[沃拉西](../Page/沃拉西.md "wikilink")（Wallasey）一帶。

《1972年地方政府法案》在1974年4月1日生效後，默西賽德成為[都市郡](../Page/都市郡.md "wikilink")，下轄的次級行政區升格為5個[都市自治市](../Page/都市自治市.md "wikilink")（同時自動升格為[單一管理區](../Page/單一管理區.md "wikilink")），有獨立的都市自治市議會，實際不受默西賽德的管轄。默西賽德郡議會在1986年被廢除。不過，都市自治市仍以「默西賽德」名義組成聯合部門（Joint-boards）統籌、協調牽涉多個都市自治市的民政事務，負責警務、消防、旅遊、廢物處理等。

每一個[名譽郡都有一個代表](../Page/名譽郡.md "wikilink")[英國皇室但沒有實權的](../Page/英國皇室.md "wikilink")[郡尉](../Page/郡尉_\(英國\).md "wikilink")（Lord
Lieutenant）常駐，默西賽德是其一。從地方政府或郡尉的角度看，默西賽德都只是「有郡界的地理範圍」而已。

[File:Seaforth_Docks.jpg|Seaforth碼頭](File:Seaforth_Docks.jpg%7CSeaforth碼頭)
<File:Halewood> transmission plant, Jaguar - geograph.org.uk -
145380.jpg|Halewood汽車廠
<File:Jaguar_Halewood_-_geograph.org.uk_-_145382.jpg>|[Jaguar車廠](../Page/Jaguar.md "wikilink")
<File:Tata_car_works_(previously_Land_Rover-Jaguar)_-_geograph.org.uk_-_1368704.jpg>|[塔塔汽車設計中心](../Page/塔塔汽車.md "wikilink")
[File:Sutton_Manor_Colliery,_air_compressor_-_geograph.org.uk_-_1764275.jpg|Sutton礦場](File:Sutton_Manor_Colliery,_air_compressor_-_geograph.org.uk_-_1764275.jpg%7CSutton礦場)
[File:Door_to_Hornby_Library_from_Picton_Library.jpg|Hornby圖書館](File:Door_to_Hornby_Library_from_Picton_Library.jpg%7CHornby圖書館)
[File:Shops_on_Old_Chester_Road,_Bebington_(4).JPG|印度菜餐廳](File:Shops_on_Old_Chester_Road,_Bebington_\(4\).JPG%7C印度菜餐廳)
[File:Restaurant_beside_the_Mersey_-_geograph.org.uk_-_1368109.jpg|海岸晶華酒店](File:Restaurant_beside_the_Mersey_-_geograph.org.uk_-_1368109.jpg%7C海岸晶華酒店)

## 行政區劃

[ 1.
[利物浦](../Page/利物浦.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
2\.
[塞夫頓](../Page/塞夫頓都會自治市.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
3\.
[諾斯利](../Page/諾斯利都市自治市.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
4\.
[聖海倫斯](../Page/聖海倫斯都會自治市.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
5\.
[威勒爾](../Page/威勒爾都會自治市.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:Merseyside_numbered_districts.svg "fig: 1. 利物浦（單一管理區） 2. 塞夫頓（單一管理區） 3. 諾斯利（單一管理區） 4. 聖海倫斯（單一管理區） 5. 威勒爾（單一管理區） ")
默西賽德包含5個[都市自治市](../Page/都市自治市.md "wikilink")（也就同時是[單一管理區](../Page/單一管理區.md "wikilink")）：[利物浦](../Page/利物浦.md "wikilink")、[塞夫頓都會自治市](../Page/塞夫頓都會自治市.md "wikilink")（Sefton）、[諾斯利](../Page/諾斯利都市自治市.md "wikilink")（Knowsley）、[聖海倫斯](../Page/聖海倫斯都會自治市.md "wikilink")（St.
Helens）與[威勒爾](../Page/威勒爾都會自治市.md "wikilink")（Wirral）。

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。默西賽德，東北與[蘭開夏郡相鄰](../Page/蘭開夏郡.md "wikilink")，東與[大曼徹斯特郡相鄰](../Page/大曼徹斯特.md "wikilink")，南與[柴郡相鄰](../Page/柴郡.md "wikilink")，西臨[愛爾蘭海](../Page/愛爾蘭海.md "wikilink")。[威勒爾都市自治市位於](../Page/威勒爾都市自治市.md "wikilink")[威勒爾半島](../Page/威勒爾半島.md "wikilink")，東隔[默西河與利物浦對望](../Page/默西河.md "wikilink")，西邊的[迪河是](../Page/迪河.md "wikilink")[威爾斯](../Page/威爾斯.md "wikilink")[弗林特郡的邊界](../Page/弗林特郡.md "wikilink")。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>colspan= | 都會自治市</p></th>
<th><p>行政中心</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:MerseysideNumbered.png" title="fig:MerseysideNumbered.png">MerseysideNumbered.png</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Merseyside_County.png" title="fig:Merseyside_County.png">Merseyside_County.png</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/利物浦.md" title="wikilink">利物浦</a></p></td>
<td><p>利物浦市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/塞夫頓都會自治市.md" title="wikilink">塞夫頓</a></p></td>
<td><p><a href="../Page/布特爾.md" title="wikilink">布特爾鎮</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/諾斯利都市自治市.md" title="wikilink">諾斯利</a></p></td>
<td><p><a href="../Page/海頓(默西賽德).md" title="wikilink">海頓鎮</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/聖海倫斯都會自治市.md" title="wikilink">聖海倫斯</a></p></td>
<td><p><a href="../Page/聖海倫斯.md" title="wikilink">聖海倫斯鎮</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/威勒爾都會自治市.md" title="wikilink">威勒爾</a></p></td>
<td><p><a href="../Page/沃拉西.md" title="wikilink">沃拉西鎮</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 地方政府

雖然默西賽德郡議會被廢除，但都市自治市仍以「默西賽德」名義組成聯合部門（Joint-boards）統籌、協調牽涉多個都市自治市的民政事務：

  - [默西賽德警察局](../Page/默西賽德警察局.md "wikilink")
  - [默西賽德消防局](../Page/默西賽德消防局.md "wikilink")
  - [默西賽德旅客運輸執行委員會](../Page/默西賽德旅客運輸執行委員會.md "wikilink")
  - [默西賽德廢物處理局](../Page/默西賽德廢物處理局.md "wikilink")

## 外部連結

[category:英格蘭的郡](../Page/category:英格蘭的郡.md "wikilink")

[Category:都市郡](../Category/都市郡.md "wikilink")
[梅西賽郡](../Category/梅西賽郡.md "wikilink")