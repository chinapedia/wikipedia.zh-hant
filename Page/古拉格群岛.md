《**古拉格群岛**》（）是由[苏联作家](../Page/苏联.md "wikilink")[亚历山大·索尔仁尼琴编著的一部反映苏联奴隶劳动和集中营故事的书](../Page/亚历山大·索尔仁尼琴.md "wikilink")。[古拉格指的是苏联的](../Page/古拉格.md "wikilink")[集中营](../Page/集中营.md "wikilink")，實際上[前蘇聯並沒有古拉格群島這個地理名稱](../Page/前蘇聯.md "wikilink")，它是索尔仁尼琴的一種比喻說法，索尔仁尼琴把整個蘇聯比作海洋，在這個海洋上處處皆是監獄和集中營的岛屿，他把这些岛屿稱為古拉格群岛。作者亲自在[古拉格集中营中生活过](../Page/古拉格.md "wikilink")，并且是书中事件的目击者和第一手材料的获得者，出獄後採訪了270位人士，为书中所写的事提供了证词。本书创作于1962年至1973年间，于1973年在西方出版。在苏联公开出版本书的1989年之前，本书一直作为地下出版物在苏联流传。

## 内容

索爾仁尼琴一共列舉31種刑訊方法，從心理上的折磨到肉體上的摧殘無所不包、無所不用其極。秘密員警在生理上耗盡犯人的體力，在精神上徹底摧垮其僥倖心理。

## 评价

俄罗斯总统[普京](../Page/普京.md "wikilink")：“这是一本非常需要的书。不研究书中所记录的现实，我们无法全面了解我们的国家。不全面了解我们的国家，思考未来必将困难重重。”\[1\]

在中国，有理论认为《古拉格群岛》记录的不是前苏联体制改变人，而是消灭人的历史。\[2\]

## 参考文献

## 外部链接

  - *The Gulag Archipelago* in original Russian, [parts 1
    and 2](http://lib.ru/PROZA/SOLZHENICYN/gulag.txt), [parts 3
    and 4](http://lib.ru/PROZA/SOLZHENICYN/gulag2.txt), and [parts 5, 6,
    and 7](http://lib.ru/PROZA/SOLZHENICYN/gulag3.txt).
  - [Aleksandr Solzhenitsyn: "Saving the Nation Is the Utmost Priority
    for the
    State"](https://web.archive.org/web/20060527214210/http://english.mn.ru/english/issue.php?2006-15-35)
    **"Moscow News"**(2.05.2006)

## 参見

  - [古拉格](../Page/古拉格.md "wikilink")
  - [集中營](../Page/集中營.md "wikilink")

[Category:1973年书籍](../Category/1973年书籍.md "wikilink")
[Category:苏联小说](../Category/苏联小说.md "wikilink")
[Category:古拉格](../Category/古拉格.md "wikilink")

1.
2.