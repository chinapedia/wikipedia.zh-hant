**资源描述框架**（，縮寫：），是[万维网联盟](../Page/万维网联盟.md "wikilink")（[W3C](../Page/W3C.md "wikilink")）提出的一组[标记语言的](../Page/标记语言.md "wikilink")，基於XML語法及XML
Schema的資料類型以便更为丰富地描述和表达的内容与结构。

## 参考文献

  - [W3C RDF Primer中文版](http://zh.transwiki.org/cn/rdfprimer.htm)

## 延伸閱讀

  - [W3C's RDF at W3C](http://www.w3.org/RDF/): specifications, guides,
    and resources
  - [RDF Semantics](http://www.w3.org/TR/2004/REC-rdf-mt-20040210/):
    specification of semantics, and complete systems of inference rules
    for both RDF and RDFS

## 外部連結

  -
## 参见

  - [元数据](../Page/元数据.md "wikilink")

  - [语义网](../Page/语义网.md "wikilink")

  - [简单知识组织系统](../Page/简单知识组织系统.md "wikilink")

  - [Web 3.0](../Page/Web_3.0.md "wikilink")

  -
  - [本体语言](../Page/本体语言.md "wikilink")

      - [OWL](../Page/网络本体语言.md "wikilink")

{{-}}

[Category:知识表示](../Category/知识表示.md "wikilink")
[Category:XML](../Category/XML.md "wikilink")
[Category:W3C标准](../Category/W3C标准.md "wikilink")
[資源描述架構](../Category/資源描述架構.md "wikilink")