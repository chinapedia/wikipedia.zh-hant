《**李克勤演奏廳Ⅱ**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[李克勤於](../Page/李克勤.md "wikilink")2006年11月3日發行的個人[音樂專輯](../Page/音樂專輯.md "wikilink")（CD+DVD）。幕後班底源用《[李克勤演奏廳](../Page/李克勤演奏廳.md "wikilink")》（普遍香港樂迷稱為“演奏廳
1”），包括[趙增熹及](../Page/趙增熹.md "wikilink")[金培達擔任唱片監製](../Page/金培達.md "wikilink")，[陳少琪任創作總監](../Page/陳少琪.md "wikilink")，與2005年推出的[李克勤演奏廳一樣](../Page/李克勤演奏廳.md "wikilink")，採用與演奏者作同步現場錄音的製作方式。專輯於[韓國錄製](../Page/韓國.md "wikilink")，合作的演奏家有[郭靜](../Page/郭靜.md "wikilink")（[豎琴手](../Page/豎琴.md "wikilink")）、[David
Hodges](../Page/David_Hodges.md "wikilink")（[阿根廷](../Page/阿根廷.md "wikilink")[手風琴手](../Page/手風琴.md "wikilink")）、[蘇敬珍](../Page/蘇敬珍.md "wikilink")（[韓國大鼓手](../Page/韓國大鼓.md "wikilink")）以及其他[色士風](../Page/色士風.md "wikilink")、[大提琴](../Page/大提琴.md "wikilink")、[中提琴](../Page/中提琴.md "wikilink")、[小提琴樂手](../Page/小提琴.md "wikilink")。雖然全於韓國製作，但碟中不乏地道的香港歌名、歌詞，如「天水、圍城」、「香港仔」等。

當中，「天水、圍城」一曲以豎琴為主要伴奏樂器，與及「公主太子」前奏、中段和結尾有男童聲和唱，為[香港](../Page/香港.md "wikilink")[流行音樂所少見](../Page/流行音樂.md "wikilink")。

## 曲目

## 派台歌成績

| 歌曲           | 903 專業推介 | [RTHK](../Page/RTHK.md "wikilink") 中文歌曲龍虎榜 | 997 勁爆流行榜 | [TVB](../Page/TVB.md "wikilink") 勁歌金榜 |
| ------------ | -------- | ------------------------------------------ | --------- | ------------------------------------- |
| 天水、圍城        | **1**    | **1**                                      | **1**     | **1**                                 |
| 公主太子         | **1**    | **1**                                      | **1**     | **1**                                 |
| Paris Hilton | \-       | 4                                          | \-        | \-                                    |
| 一場兄弟         | \-       | \-                                         | \-        | \-                                    |

## 所獲獎項

  - 新城勁爆頒獎禮2006 ── 新城勁爆歌曲 《天水·圍城》
  - 新城勁爆頒獎禮2006 ── 新城勁爆年度專輯《李克勤演奏廳II》
  - 新城勁爆頒獎禮2006 ── 新城勁爆具創意市場策略大獎《李克勤演奏廳II》
  - 2006年度十大勁歌金曲頒獎典禮 ── 金曲獎《天水·圍城》
  - 2006十大中文金曲頒獎典禮 ── 十大中文金曲獎《天水·圍城》
  - 2006 IFPI - 十大銷量廣東唱片-《李克勤演奏廳II》
  - 2007年度勁歌金曲優秀選第一回入選歌曲 - 《公主太子》
  - 第七屆華語音樂傳媒大獎 - 年度粵語專輯 《演奏廳II》

## 外部連結

  - [李克勤官方網頁](https://web.archive.org/web/20050317051540/http://www.hackenleenet.com/)

[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:李克勤音樂專輯](../Category/李克勤音樂專輯.md "wikilink")