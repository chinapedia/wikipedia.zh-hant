[Korean_culture.jpg](https://zh.wikipedia.org/wiki/File:Korean_culture.jpg "fig:Korean_culture.jpg")与[當代文化事物](../Page/韓國文化.md "wikilink")，從左上角順時針方向：[三星Galaxy
Tab](../Page/三星Galaxy_Tab.md "wikilink")、兩名在跳的女子、一鍋[朝鮮拌飯](../Page/朝鮮拌飯.md "wikilink")、[K-pop歌手](../Page/韓國流行音樂.md "wikilink")[金俊秀](../Page/金俊秀.md "wikilink")、K-pop男子組合[Super
Junior](../Page/Super_Junior.md "wikilink")、兩名穿[韓服的孩子](../Page/韓服.md "wikilink")\]\]

**韓流**，是指[韩国文化在](../Page/韩国文化.md "wikilink")[亚洲和](../Page/亚洲.md "wikilink")[世界范围内流行的现象](../Page/世界.md "wikilink")\[1\]\[2\]\[3\]。韩流一般以[韩国电影](../Page/韩国电影.md "wikilink")、[电视剧为代表](../Page/韩剧.md "wikilink")，与[韩国音乐](../Page/韩国流行乐.md "wikilink")、[图书](../Page/图书.md "wikilink")、[电子游戏](../Page/大韩民国#網路遊戲.md "wikilink")、[服饰](../Page/服饰.md "wikilink")、[饮食](../Page/韩国料理.md "wikilink")、[体育](../Page/韩国体育.md "wikilink")、[旅游观光](../Page/韩国旅游业.md "wikilink")、化妆美容、[韩语等形成一个彼此影响和带动的循环体系](../Page/韩语.md "wikilink")，因而具有极为强大的流行力量\[4\]。

“韩流”一词最初为[中文媒体所提出](../Page/中文.md "wikilink")，用來形容[男孩團體](../Page/男孩團體.md "wikilink")[H.O.T.帶到中國的韓國潮流](../Page/H.O.T..md "wikilink")\[5\]\[6\]。后来被[韩国媒体广泛用于指代本国文化产业的输出](../Page/韩国.md "wikilink")\[7\]\[8\]。“Hallyu”已经在英语中被用来指代“韩流”\[9\]。韩流在世界范围的流行使韩国[软实力世界排名和国家形象得到提高](../Page/软实力.md "wikilink")\[10\]\[11\]\[12\]，韩流作为文化产业得到韩国政府的扶植\[13\]。韩国政府期望通过韩流的发展，促进韩国文化在外国的推广，消除部分外国人的[反韩情绪](../Page/反韩情绪.md "wikilink")\[14\]，促进[朝鲜半岛的统一](../Page/朝鮮半島統一問題.md "wikilink")\[15\]和推动世界和平与繁荣\[16\]。

## 发展史

### 历史背景

1962年，[朴正熙发动政变上台后逐步实行遏制电影业发展的政策](../Page/朴正熙.md "wikilink")。在军人独裁下的韩国，电影业发展低迷。这段时期被称为韩国电影业“失去的二十年”\[17\]。1980年代随着军人独裁统治在韩国的终结，[韩国电影业开始复苏](../Page/韩国电影.md "wikilink")。不过这一时期的韩国电影业主要由成产空白[录像带](../Page/录像带.md "wikilink")、[VCR的](../Page/VCR.md "wikilink")[韩国财阀控制](../Page/韩国财阀.md "wikilink")。这些财阀制作电影的目的其实就是为了销售他们的录像带、VCR而非[电影本身](../Page/电影.md "wikilink")。韩国对国外电影采取制度限制外国电影准入的数量\[18\]。当时的韩国电影产业不足以筹集大量资金出品高成本的电影，这一政策有利于保护本国电影业免收外国[大片的冲击](../Page/大片.md "wikilink")\[19\]。

1986年，[美国电影协会向美国参议院提交一份抗议](../Page/美国电影协会.md "wikilink")，旨在抗议韩国政府对外国电影的“不公平”政策\[20\]。介于美国联邦政府的施压，韩国政府逐渐放宽对美国好莱坞电影的准入。1988年，[二十世纪福克斯成为首个在韩国设立办事处的美国电影公司](../Page/二十世纪福克斯.md "wikilink")，之后[华纳兄弟](../Page/华纳兄弟.md "wikilink")、[哥伦比亚电影公司](../Page/哥伦比亚电影公司.md "wikilink")、[迪士尼电影公司纷纷进入韩国](../Page/迪士尼电影公司.md "wikilink")\[21\]。

### 1994年–1999年：初期开始

1994年，在[韩国电影市场上](../Page/韩国电影.md "wikilink")，好莱坞所占有的份额已经达到80%，而韩国本国的电影份额却只有可怜的15.9%\[22\]
。同年，韩国总统[金泳三展示了一份总统顾问委员会的报告](../Page/金泳三.md "wikilink")，其中建议支持和资助韩国的文化与传媒产业，并将其当做国家的战略出口产业\[23\]；随后，[韩国文化体育观光部设立文化产业局以推动文化产业发展](../Page/韩国文化体育观光部.md "wikilink")\[24\]。1997年，受[亚洲金融危机影响](../Page/亚洲金融危机.md "wikilink")，[韩国财阀从传媒和娱乐产业退出](../Page/韩国财阀.md "wikilink")，大批以[好莱坞模式营运得独立制片公司如雨后春笋般涌现出来](../Page/好莱坞.md "wikilink")\[25\]。

1993年，韩国电影之父[林权泽导演的](../Page/林权泽.md "wikilink")《[西便制](../Page/西便制.md "wikilink")》打开了韩国电影100万观众的新纪元\[26\]。1999年，韩国著名导演[姜帝圭自编自导的韩国第一部本土大片](../Page/姜帝圭.md "wikilink")《[生死谍变](../Page/生死谍变.md "wikilink")》取得巨大成功，票房收入超过1000万美元\[27\]，票房人数达到660万，打破了1997年《泰坦尼克号》在韩国创下的417万人次记录。这是韩国电影第一次打败[好莱坞电影](../Page/好莱坞电影.md "wikilink")，使韩国人对本土电影的信心陡然激增\[28\]。

[韩国电影业的兴起也带动了](../Page/韩国电影.md "wikilink")[韩剧的兴起](../Page/韩剧.md "wikilink")。[韩国文化的向外输出开始于](../Page/韩国文化.md "wikilink")[韩剧在](../Page/韩剧.md "wikilink")[中国的热播](../Page/中国.md "wikilink")。1997年，中央电视台在第八套影视频道播出电视剧《[爱情是什么](../Page/爱情是什么.md "wikilink")》，在中国观众引起轰动，收视率高达4.2%。同年，韩国宇田公司为代表将大量韩国音乐人作品介绍到中国，韩国作品遂如雨后春笋。这个时期主要是电视剧与音乐为主，特別是偶像組合[H.O.T.在](../Page/H.O.T..md "wikilink")[北京舉行演唱會的高人氣](../Page/北京.md "wikilink")\[29\]，開創了偶像文化、粉絲文化、應援文化\[30\]，受众为青年女性和中学生。“韩流”或“韩风”一词最初其实是由海峡两岸的中文媒体率先开始使用（取其寒流或寒风自北方而来的谐音双关义），例如1999年11月19日，《[北京青年报](../Page/北京青年报.md "wikilink")》以“韩流”一词描述突显的韩国文化影响力\[31\]，为应对激增的韩国进口文化产品，中国[国家广播电影电视总局开始限制](../Page/国家广播电影电视总局.md "wikilink")[韩剧在中国电视台的播放数量](../Page/韩剧.md "wikilink")。\[32\]

### 2000年–2009年：风靡亚洲

[2005TIBE_KoreaPavilion_Corridor_DaeJangGeumPoster.JPG](https://zh.wikipedia.org/wiki/File:2005TIBE_KoreaPavilion_Corridor_DaeJangGeumPoster.JPG "fig:2005TIBE_KoreaPavilion_Corridor_DaeJangGeumPoster.JPG")韓國館懸掛《大長今》的巨型海報\]\]
进入21世紀后，[韩剧和](../Page/韩剧.md "wikilink")[韩国流行音乐开始在](../Page/韩国流行音乐.md "wikilink")[亚洲其它国家和地区逐渐流行](../Page/亚洲.md "wikilink")。2000年，[宝儿签约](../Page/宝儿.md "wikilink")[SM娱乐](../Page/SM娱乐.md "wikilink")。两年后[宝儿的](../Page/宝儿.md "wikilink")《Listen
to My
Heart》荣登日本Oricon排行榜首位，成为[日本史上首位拥有百万专辑的韩籍艺人](../Page/日本.md "wikilink")。\[33\]

2002年，韩剧持续在亚洲升温。《蓝色生死恋》在中国内地有21个电视频道同时播出。而中国大陆在2002年共播放了316次韩剧，这一纪录远远领先于第二名61次的日剧。在日本，韩剧《[冬季恋歌](../Page/冬季恋歌.md "wikilink")》上映取得巨大成功，相关产品销售额超过350万美元\[34\]。时任[日本首相](../Page/日本首相.md "wikilink")[小泉纯一郎曾评价说](../Page/小泉纯一郎.md "wikilink")《[冬季恋歌](../Page/冬季恋歌.md "wikilink")》男主角[裴勇浚在日本的受欢迎度要比他高](../Page/裴勇浚.md "wikilink")\[35\]。之后的韩剧比如《[浪漫满屋](../Page/浪漫满屋.md "wikilink")》和《[大长今](../Page/大长今.md "wikilink")》也都取得了同样的成功\[36\]。

随着[韩剧的流行](../Page/韩剧.md "wikilink")，[韩国流行音乐也在](../Page/韩国流行音乐.md "wikilink")[亚洲兴起](../Page/亚洲.md "wikilink")。[东方神起](../Page/东方神起.md "wikilink")、[SS501和](../Page/SS501.md "wikilink")[Super
Junior在这一时期开始走红亚洲](../Page/Super_Junior.md "wikilink")\[37\]\[38\]。2003年，[Baby
V.O.X的中文单曲](../Page/Baby_V.O.X.md "wikilink")「I'm Still Loving
You」成功打入中国市场，为韩流在華培养了一大批粉丝。[Baby
V.O.X的韩文单曲](../Page/Baby_V.O.X.md "wikilink")「What Should I
Do」和中文单曲「I'm Still Loving
You」在[泰国取得同样很好的成绩](../Page/泰国.md "wikilink")\[39\]。随后的韩国组合像[Big
Bang和](../Page/Big_Bang.md "wikilink")[Super
Junior受到了广大青年人的喜欢](../Page/Super_Junior.md "wikilink")，并在开始在[南美洲](../Page/南美洲.md "wikilink")、[东欧部分地区](../Page/东欧.md "wikilink")、[中东和一小部分西方国家地区拥有颇具规模的粉丝](../Page/中东.md "wikilink")。

### 2009年–2010年：首次尝试走出亚洲

韩国流行乐在亚洲的走红，诱使一些韩国歌手像[宝儿](../Page/宝儿.md "wikilink")、[崔东昱尝试发行英文歌曲进入西方国家流行音乐市场](../Page/崔东昱.md "wikilink")。但这些尝试最初都不成功。\[40\]韩国流行乐在美国只是局限在[洛杉矶和](../Page/洛杉矶.md "wikilink")[纽约韩裔人口较多的城市而已](../Page/纽约.md "wikilink")。\[41\]

不过在北美之外，韩流旋风却很猛。\[42\]韩剧《[朱蒙](../Page/朱蒙_\(電視劇\).md "wikilink")》在[穆斯林世界国家很受欢迎](../Page/穆斯林世界.md "wikilink")，在[伊朗取得超过](../Page/伊朗.md "wikilink")80%的收视率。\[43\]\[44\]
2009年的韩剧《[花樣男子](../Page/花樣男子.md "wikilink")》在183个国家播出，被翻译成69种语言，经过了6年仍然持续不断的受到全球影迷的欢迎，人气不减，获第11届（2015）Soompi颁奖典礼韩流特别奖。\[45\]韓星李敏鎬因此與李英愛於2015年榮獲首尔电视节「韓流10年功勞大赏」。\[46\]

[Kpop_fans_in_poland.jpg](https://zh.wikipedia.org/wiki/File:Kpop_fans_in_poland.jpg "fig:Kpop_fans_in_poland.jpg")、[B1A4和](../Page/B1A4.md "wikilink")[2PM组合旗帜的华沙歌迷](../Page/2PM.md "wikilink")\]\]
在[东欧](../Page/东欧.md "wikilink")，韩流的掀起是从韩剧开始的。[罗马尼亚电视台播放的韩剧每集都可以吸引](../Page/罗马尼亚电视台.md "wikilink")50多万的观众观看。\[47\]

韩流在西方国家的流行得利于现代的互联网媒体。\[48\]
[YouTube这样的互联网媒体平台可以让年轻人绕开传统传媒的DJ](../Page/YouTube.md "wikilink")，无拘无束地轻松欣赏自己喜欢的任何语言、任何形式的作品。正如一位参加2012年美国加州[尔湾](../Page/尔湾_\(加利福尼亚州\).md "wikilink")的CNN记者说的，“如果你上前询问这里的人是从哪里了解到[韩国流行乐的](../Page/韩国流行乐.md "wikilink")，他们会说是[YouTube](../Page/YouTube.md "wikilink")”。\[49\]

### 2011年至今：流行世界

[Search_volume_for_kpop.svg](https://zh.wikipedia.org/wiki/File:Search_volume_for_kpop.svg "fig:Search_volume_for_kpop.svg")

至2011年底，[韩国流行音乐在](../Page/韩国流行音乐.md "wikilink")[YouTube的观看次数已经超越](../Page/YouTube.md "wikilink")23亿大关，几乎是2010年的3倍。\[50\]

2013年2月，[秘鲁副总统](../Page/秘鲁.md "wikilink")在接受[韩国联合通讯社采访时说韩国流行音乐和韩剧在秘鲁很受欢迎](../Page/韩国联合通讯社.md "wikilink")，已经成为秘鲁人们想更多了解韩国的主要原因之一，并表示欢迎“韩流”在秘鲁发展。\[51\]

在[土耳其](../Page/土耳其.md "wikilink")，韩国歌迷俱乐部注册的歌迷人数已经超过了10万人，\[52\]其中一个俱乐部歌迷人数达到近1.3万人。\[53\]在[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")，已有几部韩剧开始在4家乌兹别克斯坦国家电视台每日播出。\[54\]

[Psy_does_Gangnam_Style_at_KIIS_FM_Jingle_Ball_2012.jpg](https://zh.wikipedia.org/wiki/File:Psy_does_Gangnam_Style_at_KIIS_FM_Jingle_Ball_2012.jpg "fig:Psy_does_Gangnam_Style_at_KIIS_FM_Jingle_Ball_2012.jpg")」\]\]
在西方国家，韩流的粉丝在[法国已经超过了](../Page/法国.md "wikilink")10万。\[55\]
在[美国](../Page/美国.md "wikilink")，韩流的流行已经从[亚裔美国人群体向更广泛的美国白人和黑人群体扩展](../Page/亚裔美国人.md "wikilink")。\[56\]\[57\]\[58\]

2013年2月6日，美国[白宫负责管理第一夫人](../Page/白宫.md "wikilink")[米歇爾·奧巴馬](../Page/米歇爾·奧巴馬.md "wikilink")[Twitter账户的官员透露](../Page/Twitter.md "wikilink")[白宫正在从](../Page/白宫.md "wikilink")搬运用来做韩式泡菜的大白菜。\[59\]\[60\]《[时尚先生](../Page/时尚先生.md "wikilink")》的一位主编曾对此评论说这与白宫以往的活动真是大不相同。\[61\]在此之前不多久，[韩国料理在英国超市](../Page/韩国料理.md "wikilink")[乐购的销量翻了一翻多](../Page/乐购.md "wikilink")。\[62\]德国国家公共广播电台有报道说“韩流正在征服世界”。\[63\]
[法国最大的电视台](../Page/法国.md "wikilink")[法国电视一台报道说韩流正在在整个年轻人群体流行](../Page/法国电视一台.md "wikilink")。\[64\]

随着[缅甸民主活动家](../Page/缅甸.md "wikilink")[诺贝尔和平奖获得者](../Page/诺贝尔和平奖.md "wikilink")[昂山素季](../Page/昂山素季.md "wikilink")2013年首次访问韩国，韩流在亚洲达到了新高度，包括《[星梦奇缘](../Page/星梦奇缘.md "wikilink")》男主角[安在旭在内的韩国一线演员与](../Page/安在旭.md "wikilink")[昂山素季共进晚餐](../Page/昂山素季.md "wikilink")。据[法新社报道](../Page/法新社.md "wikilink")，《[星梦奇缘](../Page/星梦奇缘.md "wikilink")》在[缅甸的播放非常成功](../Page/缅甸.md "wikilink")。[安在旭由于长相貌似](../Page/安在旭.md "wikilink")[昂山素姬的父亲](../Page/昂山素姬.md "wikilink")[昂山而被特别邀请和](../Page/昂山.md "wikilink")[昂山素季共进晚餐](../Page/昂山素季.md "wikilink")。\[65\]

2013年，[英国广播公司报道说](../Page/英国广播公司.md "wikilink")[耶路撒冷希伯来大学的研究学者认为韩流在以色列的流行使得](../Page/耶路撒冷希伯来大学.md "wikilink")[以色列和](../Page/以色列.md "wikilink")[巴勒斯坦领土的年轻人有了共同的爱好](../Page/巴勒斯坦领土.md "wikilink")，为地区和平带了希望。\[66\]

2014年，全明星韩剧《[來自星星的你](../Page/來自星星的你.md "wikilink")》通过互联网媒体热播，在亚洲再次掀起韩流热潮。《星星》在中国的点击率突破20亿，创下中国视频网站电视剧部门点击率最高记录\[67\]。剧中女主角千颂伊爱吃的[韩式炸鸡加](../Page/韩式炸鸡.md "wikilink")[啤酒](../Page/啤酒.md "wikilink")，穿的服装，使用的红唇膏在亚洲地区也受到粉丝的追捧\[68\]。部分男團([EXO](../Page/EXO.md "wikilink"))興起([防彈少年團](../Page/防彈少年團.md "wikilink"))開拓世界市場引來大班粉絲。

2017年，因為[韓國部署薩德反導彈系統事件](../Page/韓國部署薩德反導彈系統事件.md "wikilink")，在中國市場受到嚴重影響及一連串反制措施。

## 韩流内容

韩流的主要中心是以韩国电视剧为中心展开，通过电视剧中展示出的服装、家居、时常消费等，刺激着观众对于韩国商品或文化的向往，包括手机、服饰、化妆品、游戏、漫画、饮食、甚至美容技术等。

### 韩国影视

1993年，[中国中央电视台尝试首次引入韩剧](../Page/中国中央电视台.md "wikilink")《[嫉妒](../Page/嫉妒.md "wikilink")》，直至1997年的《爱情是什么》方引起巨大轰动。韩国电视剧在各地的走红也引起了部分电影的热卖。包括2003年的《[实尾岛](../Page/实尾岛.md "wikilink")》、2004年《[太极旗飘扬](../Page/太极旗飘扬.md "wikilink")》。在韩国[韩亚航空公司一架命名为](../Page/韩亚航空公司.md "wikilink")“[大长今](../Page/大长今.md "wikilink")”波音767客机上，机身上画满[李英爱的巨幅照片](../Page/李英爱.md "wikilink")。2009年的《花樣男子》不僅風迷東南亞更於2015穫頒Soompi韓流特別獎，說明该剧雖然已經經過了6年仍然持續不斷的受到全球韓迷的歡迎，人氣不減
。\[69\]。

### 韩国音乐

韩国人素以擅长音乐与舞蹈著称，早在[抗日战争期间](../Page/抗日战争.md "wikilink")，就有大量韩国流亡海外音乐家坚持以音乐鼓舞士气，其中《[中国人民解放军军歌](../Page/中国人民解放军军歌.md "wikilink")》创作者[郑律成便是韩国光州人](../Page/郑律成.md "wikilink")。在1980年代，很多港台歌曲即是从韩国歌曲翻唱而来，比如[谭咏麟的](../Page/谭咏麟.md "wikilink")《爱在深秋》、[姜育恒的](../Page/姜育恒.md "wikilink")《[再回首](../Page/再回首.md "wikilink")》等。而随着电视剧的全面流行时，剧中的主题曲等音乐也广为流传，比如《[我的野蛮女友](../Page/我的野蛮女友.md "wikilink")》中的《I
Believe》。

之后韩国音乐逐渐从影视中剥离而出。2000年2月，[韩国SM娱乐有限公司旗下的偶像组合](../Page/韩国SM娱乐有限公司.md "wikilink")[H.O.T.在](../Page/H.O.T..md "wikilink")[北京举办演唱会](../Page/北京.md "wikilink")，门票全部售空。这是首次在[韩国以外地区舉行的韩国音乐演唱会](../Page/韩国.md "wikilink")\[70\]。

### 韩国舞蹈

韩国舞蹈主要以宫廷舞蹈、民族舞蹈等为主，主张讲究舞步与音乐的协调；或者民俗舞蹈中的感情表达。韩国政府也对舞蹈非常看重，1998年政府颁布国家重要无形财产中，保护大量民俗舞蹈与游戏文化，并颁布《文化财保护法》，国家付出成本保护演出、并保护传承。而在1970年代，在西方现代舞观念下，部分舞蹈家锐意改革，崇尚个性，各类现代创作舞蹈纷纷呈现。包括有的学院派，主张肢体语言寓意；另一种是[李贞贤](../Page/李贞贤.md "wikilink")，结合音乐与舞蹈的融合与偶像制作；还有一类的街舞表现，炫耀与体育技能相关的高难度动作。其中后两者得到了大量追逐时尚、热衷表达情绪的年轻人喜爱，并广为流行。

### 韩国服饰

韩剧的走红使得韩剧中男女主角的服饰打扮直接影响到观众对衣着的审美，包括《冬日恋歌》中裴永俊的围巾热、到《巴黎恋人》带来的女式短装风靡。美国、中国是韩国纺织服装的两大市场，韩国对于面料服装的技巧和理念，始终在中韩贸易中占有优势。2003年以后，韩国服装节就在中国举办每年一度的“韩国时装纺织品博览会”。

### 韩国餐饮

韩国在其他国家最初的餐饮受众仍然是韩国海外人士或者各国朝鲜族人群，随之韩国电视剧《大长今》热播，其中对于韩国宫廷宴的讲述也影响到亚洲各国。韩式汤料、炖菜、泡菜等逐渐传播各地；而对于韩国烧烤和参汤更是远传全球各地。韩国餐饮业也因此进军各国市场，包括韩国食品企业中农心（[辛拉面](../Page/辛拉面.md "wikilink")）、[好麗友](../Page/好麗友.md "wikilink")、[乐天](../Page/乐天集团.md "wikilink")、[巴黎贝甜](../Page/巴黎可頌公司.md "wikilink")（）、[多乐之日](../Page/多乐之日.md "wikilink")（）、[必品阁](../Page/必品阁.md "wikilink")（）等逐渐流行。吸引食客的不仅是食物美味本身，还有各店内浓郁的韩国传统风格。

### 韩国游戏与旅游

游戏竞技业在韩国非常昌盛，韩国重视游戏产业，支援大学开设课程，培养游戏设计人才。各国都采取与韩国游戏产业合作方式盈利。其中包括中国新浪公司于韩国**NCSOFT**合资组建上海新浪乐谷公司。

## 韩流与国际外交

[President_Obama_Korean_Wave.ogv](https://zh.wikipedia.org/wiki/File:President_Obama_Korean_Wave.ogv "fig:President_Obama_Korean_Wave.ogv")在韩国总统[朴槿惠访美期间聊侃](../Page/朴槿惠.md "wikilink")“韩流”\[71\]\]\]
自2011年以来，[韩国外交部开始邀请世界各地的](../Page/韩国外交部.md "wikilink")[K-pop粉丝参加](../Page/K-pop.md "wikilink")[K-pop选拔赛](../Page/K-pop.md "wikilink")，经过几轮较量后，韩国政府将赞助获胜者到韩国参加每年一次的。2012年5月，韩国政府还举行了一个“韩流外交”音乐会。\[72\]

### 东亚

#### 中国大陆

2005年，韩剧《[大长今](../Page/大长今.md "wikilink")》在[湖南卫视播出](../Page/湖南卫视.md "wikilink")，引来粉丝无数，其中就包括中国前领导人[胡锦涛和夫人](../Page/胡锦涛.md "wikilink")[刘永清](../Page/刘永清.md "wikilink")。2005年，[胡锦涛在接受韩国记者采访时说他很喜欢看](../Page/胡锦涛.md "wikilink")《大长今》，但是因公务繁忙，一直未能连续看完。\[73\]
2008年8月，胡锦涛访问[韩国期间](../Page/韩国.md "wikilink")，时任韩国总统[李明博特意邀请](../Page/李明博.md "wikilink")《[大长今](../Page/大长今.md "wikilink")》女主角[李英爱出席欢迎晚宴](../Page/李英爱.md "wikilink")。在当天的晚宴，胡锦涛还用韩语向在晚宴上演唱的韩星代表[张娜拉说谢谢](../Page/张娜拉.md "wikilink")。\[74\]

2007年，原中国国务院总理[温家宝在接受媒体采访时说](../Page/温家宝.md "wikilink")“对于‘韩流’这种文化现象，中国人民特别是年轻人都很喜欢，中国政府会继续鼓励包括韩流在内的两国文化交流活动。”\[75\]

2013年，演出韓劇《[繼承者們](../Page/繼承者們.md "wikilink")》的男主角[李敏鎬紅遍中國](../Page/李敏鎬.md "wikilink")，2013年繼裴勇俊之後成為第二個進駐杜莎夫人臘像館的韓國藝人，2014成為第一位獲邀登上中國央視[春晚表演的韓國藝人](../Page/春晚.md "wikilink")。同年7月，中國領導人[習近平訪韓](../Page/習近平.md "wikilink")。\[76\]\[77\]

2014年，[彭丽媛在随](../Page/彭丽媛.md "wikilink")[习近平访韩期间参观](../Page/习近平.md "wikilink")[昌德宫时说](../Page/昌德宫.md "wikilink")“仿佛走进了韩剧《[大长今](../Page/大长今.md "wikilink")》”，还提及和女儿一起看丈夫年轻时的照片，觉得习近平很像《[来自星星的你](../Page/来自星星的你.md "wikilink")》的主人公[都敏俊](../Page/都敏俊.md "wikilink")。\[78\]

#### 臺灣

韓劇《[真愛on
air](../Page/On_Air.md "wikilink")》，這部片由台灣觀光局贊助拍攝，場景從韓國拉到台灣，像是裝潢高雅又氣派的日月潭五星級飯店，以及山水美景盡收眼底的水社碼頭都曾入鏡。超人氣韓國演員宋允兒、朴容夏聯手演出，搭配劇中美景，果然讓這部片在韓國一上演，就創下30%超高收視率，甚至吸引觀光客。[MBC連續劇](../Page/文化廣播_\(韓國\).md "wikilink")《[女王之花](../Page/女王之花.md "wikilink")》在高雄拍外景取景愛河、情人觀景台、英國領事館、駁二藝術特區、旗津燈塔、蓮池潭龍虎塔、美濃紙傘店、旗山老街、小港國際機場、凱旋夜市、六合夜市及義大世界等景點。透過戲劇曝光，讓韓國朋友來南台灣深度旅遊。

#### 日本

原[日本首相](../Page/日本首相.md "wikilink")[鸠山由纪夫及其夫人](../Page/鸠山由纪夫.md "wikilink")[鸠山幸都是韩国迷](../Page/鸠山幸.md "wikilink")。据日本媒体报道，几乎所有造访日本的韩国男星都会被安排与鸠山幸见面。\[79\]鸠山幸在接受[韩国媒体采访时还称看](../Page/韩国.md "wikilink")[韩剧是保持年轻的秘诀](../Page/韩剧.md "wikilink")。\[80\]另外目前的日本第一夫人[安倍昭惠也是个韩剧迷](../Page/安倍昭惠.md "wikilink")，还会说一口流利的[韩语](../Page/韩语.md "wikilink")。\[81\]

### 中东

2005年后，[埃及和](../Page/埃及.md "wikilink")[伊朗是韩流在中东的两大目的地](../Page/伊朗.md "wikilink")。\[82\]经过几部韩剧的成功播放后，韩国政府将《[冬季恋歌](../Page/冬季恋歌.md "wikilink")》的演播权无偿赠送给了埃及几家国家电视台，并出资做了[阿拉伯语](../Page/阿拉伯语.md "wikilink")[字幕](../Page/字幕.md "wikilink")。据《[纽约时报](../Page/纽约时报.md "wikilink")》报道，韩国此举是为了使[阿拉伯世界对](../Page/阿拉伯.md "wikilink")[韓國國軍在](../Page/韓國國軍.md "wikilink")[伊拉克北部驻兵产生正面的感情](../Page/伊拉克.md "wikilink")。\[83\]

#### 埃及

经过5个月谈判，最终说服埃及国家电视台播放较早到达[中东的韩剧](../Page/中东.md "wikilink")《[蓝色生死恋](../Page/蓝色生死恋.md "wikilink")》。\[84\]在最后一集播完没多久，韩国大使馆就收到来自埃及各地的400多个[电话和](../Page/电话.md "wikilink")[情书](../Page/情书.md "wikilink")。\[85\]据韩国驻埃及大使介绍，當地民众眼中的韓國形象曾因韩軍参加[伊拉克战争而大打折扣](../Page/伊拉克战争.md "wikilink")，不过《[蓝色生死恋](../Page/蓝色生死恋.md "wikilink")》在埃及的成功，对这一局面的扭转起了非常顯著的效果。\[86\]

#### 伊朗

[韩剧在](../Page/韩剧.md "wikilink")[伊朗收到很高的](../Page/伊朗.md "wikilink")[收视率](../Page/收视率.md "wikilink")。据《》报道，2012年10月1日[伊朗伊斯兰共和国广播电视台](../Page/伊朗伊斯兰共和国广播电视台.md "wikilink")（IRIB）的几位记者还专程来到韩国参观《[朱蒙](../Page/朱蒙_\(电视剧\).md "wikilink")》和《[同伊](../Page/同伊.md "wikilink")》等韩剧的拍摄地，\[87\]并探讨与[KBS的进一步合作](../Page/KBS.md "wikilink")。\[88\]

伊朗国家电视台选择在黄金时段播放韩剧的主要原因之一是韩剧宣扬传统[儒家理念](../Page/儒家理念.md "wikilink")，其中许多与[伊斯兰文化很相近](../Page/伊斯兰文化.md "wikilink")，比如社会利益先于个人利益，尊敬上级等。\[89\]另外由于[伊朗文化及伊斯兰教令部对西方影视作品的禁闭](../Page/伊朗文化及伊斯兰教令部.md "wikilink")，韩剧在伊朗也被用来填补西方影视作品的空缺。\[90\]

据法国《[路透社](../Page/路透社.md "wikilink")》报道，伊朗的观众除了观看国家电视台认为合适的节目外，所能观看的选择很少。\[91\]因此韩剧在伊朗的收视率非常高，甚至高于在韩国的收视率，比如《[朱蒙](../Page/朱蒙_\(电视剧\).md "wikilink")》在伊朗的收视率达到80-90%而在韩国则是40%。《[朱蒙](../Page/朱蒙_\(电视剧\).md "wikilink")》男主角[宋一国也因此在伊朗成为超级明星](../Page/宋一国.md "wikilink")。\[92\]

2008年，[韩国国立中央博物馆举办了一个](../Page/韩国国立中央博物馆.md "wikilink")“辉煌波斯”的展览。前[韩国文化部部长](../Page/韩国文化部.md "wikilink")评价说此次展览是向韩国人民介绍伊朗历史和文化的最重要活动之一。\[93\]据伊朗国家媒体报道，展览吸引了35万多名参观者，到伊朗旅游的韩国人人数也翻了两翻。\[94\]

#### 伊拉克

韩剧在伊拉克最初是为驻扎在伊拉克北部参加[伊拉克战争的韓軍官兵播放的](../Page/伊拉克战争.md "wikilink")。战争结束，韩国撤军后，有呼声让所有伊拉克民众都能收看到韩剧。巴格达的韓國駐伊拉克大使館称已经计划在2012年下半年在伊拉克播放《[大长今](../Page/大长今.md "wikilink")》和《[冬季恋歌](../Page/冬季恋歌.md "wikilink")》。\[95\]

2012年，韩剧《[医道](../Page/医道.md "wikilink")》在伊拉克[库尔德斯坦地区取得了](../Page/库尔德斯坦.md "wikilink")90%的收视率。\[96\]《[医道](../Page/医道.md "wikilink")》男主角[田光烈还应伊拉克联邦政府第一夫人的邀请出访了](../Page/田光烈_\(韩国\).md "wikilink")[库尔德斯坦的](../Page/库尔德斯坦.md "wikilink")[苏莱曼尼亚](../Page/苏莱曼尼亚.md "wikilink")。\[97\]

### 大洋洲

2012年3月，时任[澳大利亚总理](../Page/澳大利亚.md "wikilink")[朱莉亞·吉拉德在访问韩国](../Page/朱莉亞·吉拉德.md "wikilink")[延世大学时说澳大利亚已经被韩流迷住了](../Page/延世大学.md "wikilink")。\[98\]新西兰外交部副部长於同年11月在[奥克兰大学接见韩国代表团时](../Page/奥克兰大学.md "wikilink")，也说韩流已经成为新西兰人生活的一部分。新西兰目前已经有4000多个[K-pop歌迷俱乐部](../Page/K-pop.md "wikilink")。\[99\]

### 东欧

#### 白俄罗斯

[白俄罗斯与韩国建交](../Page/白俄罗斯.md "wikilink")20周年之际，白俄罗斯外交部确认白俄罗斯国家电视台将与韩国[阿里郎电视台展开外交互放活动](../Page/阿里郎电视台.md "wikilink")。在2月13至17日期间，白俄罗斯国家电视台将播放一系列韩国电视节目，特别是风靡世界的K-pop节目。\[100\]
[阿里郎电视台则在同一时间会播放一些白俄罗斯电影](../Page/阿里郎电视台.md "wikilink")。\[101\]

#### 罗马尼亚

据当地报纸报道，2009年8月，[韩剧首次在](../Page/韩剧.md "wikilink")[罗马尼亚电视台播出](../Page/罗马尼亚电视台.md "wikilink")。一个月后，其收视率就在罗马尼亚排到了第3高。\[102\]
2010年的时候，已有多部韩剧在罗马尼亚国家电视台的几个频道播出，其中一些是黄金时段里收视率最高的。\[103\]

2011年，由于韩剧的风靡，[罗马尼亚电视台加大了韩剧的引进量](../Page/罗马尼亚电视台.md "wikilink")。\[104\]

### 西欧

2012年11月，[英国外交大臣施维尔在](../Page/英国外交大臣.md "wikilink")[英国上议院会见韩国代表团时说](../Page/英国上议院.md "wikilink")[K-pop已经在世界流行](../Page/K-pop.md "wikilink")。\[105\]2014年，[法国外交部官网文章报道说](../Page/法国外交部.md "wikilink")[韩国流行文化的成功使韩流已经全球化](../Page/韩国流行文化.md "wikilink")\[106\]。[德国外交部於同年發布的文章亦评论说韩流](../Page/德国外交部.md "wikilink")（包括韩剧，K-pop等）目前已经走出[亚洲开始在世界范围内流行](../Page/亚洲.md "wikilink")。\[107\]

### 美国

2013年5月，美国总统奥巴马在[白宫会见韩国总统](../Page/白宫.md "wikilink")[朴槿惠时说](../Page/朴槿惠.md "wikilink")[韩国文化](../Page/韩国文化.md "wikilink")（韩流）正在影响全球的人，并说他的女儿教了他很多的《[江南Style](../Page/江南Style.md "wikilink")》。\[108\]
2013年8月，[美国国务卿](../Page/美国国务卿.md "wikilink")[约翰·克里在为](../Page/约翰·克里.md "wikilink")[韩国光复节电视致辞讲话时说韩国文化已经通过韩流到达世界的每个角落](../Page/韩国光复节.md "wikilink")。\[109\]

## 影响

### 国家形象

2012年，[BBC针对各国民众对](../Page/BBC.md "wikilink")[韩国国家形象态度的调查显示](../Page/韩国.md "wikilink")，自2009年开始韩国的国家形象逐年在提升。在[俄罗斯](../Page/俄罗斯.md "wikilink")、[印度](../Page/印度.md "wikilink")、[中国和](../Page/中国.md "wikilink")[法国等国家民众对韩国的态度已从以前的负数变成正的](../Page/法国.md "wikilink")。报告指出韩国文化和传统是其中重要的原因之一。\[110\]

韩国国家形象的提升与[韩国文化输出是相连的](../Page/韩国文化.md "wikilink")。2011年，韩国文化产业的出口额达42亿美元。\[111\]下表的数据来源于韩国政府文化产业输出统计数据和韩国在Monocle杂志的[软实力排名](../Page/软实力.md "wikilink")。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>2008[112]</p></th>
<th><p>2009[113]</p></th>
<th><p>2010[114]</p></th>
<th><p>2011[115]</p></th>
<th><p>2012[116][117]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>文化出口额<br />
（十亿美元单位）</p></td>
<td><p>1.8</p></td>
<td><p>2.6</p></td>
<td><p>3.2</p></td>
<td><p>4.3</p></td>
<td><p>5.02</p></td>
</tr>
<tr class="even">
<td><p>韩国软实力世界排名</p></td>
<td></td>
<td></td>
<td><p>19</p></td>
<td><p>14</p></td>
<td><p>11</p></td>
</tr>
</tbody>
</table>

### 朝韩统一

韩国现代文化可以促进朝鮮半岛南北统一的作用，早在韩国总统[卢武铉时就已经被认识到了](../Page/卢武铉.md "wikilink")。\[118\]\[119\]

2007年5月，[KBS以一位朝鲜作家的小说改编的电视剧](../Page/KBS.md "wikilink")《[黃真伊](../Page/黃真伊_\(電視劇\).md "wikilink")》成为首部公开向[朝鲜播放的](../Page/朝鲜.md "wikilink")[韩剧](../Page/韩剧.md "wikilink")。\[120\]随着韩国政府对朝鲜“[阳光政策](../Page/阳光政策.md "wikilink")”的结束，南北韩关系恶化，韩流也遭到朝鲜当局的封杀。不过[自由亚洲电台的一篇报道说韩流可能已经在封闭的朝鲜打下根基](../Page/自由亚洲电台.md "wikilink")。\[121\]

2010年，[韩国统一研究院对](../Page/韩国统一研究院.md "wikilink")33名[脱北者的调查发现](../Page/脱北者.md "wikilink")，《[冬季恋歌](../Page/冬季恋歌.md "wikilink")》等韩剧对脱北者决定逃到韩国起到影响作用。调查还发现少数居住在[朝韩非军事区附近的北韩人可以接收附近韩国电视台的信号](../Page/朝韩非军事区.md "wikilink")。\[122\]另外从[中国走私来的](../Page/中国.md "wikilink")[CD和](../Page/CD.md "wikilink")[DVD也使朝鲜人能更多地接触到](../Page/DVD.md "wikilink")[韩国现代文化](../Page/韩国现代文化.md "wikilink")。\[123\]

2012年，韩国统一研究院做了一个更大规模的调查（一共100名脱北者），发现朝鲜一些富裕的精英阶层每天或者一周至少一次观看韩国影视作品。调查还确认与其它地区相比，居住在离中朝边境较近的朝鲜人接触到的韩流产品是最多的。\[124\]

2012年10月，[金正恩在](../Page/金正恩.md "wikilink")[朝鲜人民军安全部总部发布讲话时说要坚决抗击敌人的意识形态和文化渗透](../Page/朝鲜人民军.md "wikilink")。\[125\]
在这不久前，[美国国务院派遣的一个国际咨询组织发现朝鲜当局越来越担心信息的封锁问题](../Page/美国国务院.md "wikilink")。但朝鲜当局却越来越没有能力进行控制，因为在朝鲜有着对韩国影视产品的巨大需求，而且有积极从中国走私韩国影视产品到朝鲜的商人来满足这些需求。\[126\]

2013年2月，[韩国联合通讯社引用人权组织的话说中朝边境的](../Page/韩国联合通讯社.md "wikilink")[K-popCD和其它韩流产品的走私活动](../Page/K-pop.md "wikilink")，已经使[Psy](../Page/Psy.md "wikilink")2012年风靡全球的神曲《[江南Style](../Page/江南Style.md "wikilink")》在朝鲜深深蔓延。\[127\]

2013年5月15日，非政府组织[人权观察确认说韩流产品在朝鲜很流行](../Page/人权观察.md "wikilink")，起到了抵消朝鲜当局在朝鲜民众对韩国的负面-{}-塑造作用。\[128\]

時值[朝韓關係解凍時期](../Page/朝韓關係.md "wikilink")，韓國藝術團於2018年4月1日在東平壤大劇院舉行，演出陣容包括南韓知名女團[Red
Velvet](../Page/Red_Velvet.md "wikilink")、[少女時代成員](../Page/少女時代.md "wikilink")[徐賢](../Page/徐賢.md "wikilink")、[趙容弼](../Page/趙容弼.md "wikilink")、[李仙姬](../Page/李仙姬.md "wikilink")、[崔辰熙等](../Page/崔辰熙.md "wikilink")。[金正恩携夫人](../Page/金正恩.md "wikilink")[李雪主到場觀看表演頻頻鼓掌](../Page/李雪主.md "wikilink")，演出結束後還一一與表演藝人握手，還建議兩國加強文化活動交流\[129\]。

### 反面效应

在一些有[反韩情绪的国家和地区](../Page/反韩情绪.md "wikilink")，韩流在反韩人群中也产生了反作用，特別是中國和日本，韓流盛行對中國和日本構成一定的影響。

2005年7月26日，《[漫画嫌韩流](../Page/漫画嫌韩流.md "wikilink")》在[日本发行](../Page/日本.md "wikilink")，受到[日本右翼组织的炒作](../Page/日本右翼组织.md "wikilink")，成为当时日本[亚马逊网站销量第一的图书](../Page/亚马逊公司.md "wikilink")\[130\]。2011年，日本演员[高冈奏辅在自己的](../Page/高冈奏辅.md "wikilink")[Twitter网站上公开批评日本](../Page/Twitter.md "wikilink")[富士电视台偏重播放](../Page/富士电视台.md "wikilink")[韩剧](../Page/韩剧.md "wikilink")，引发2500名日本人在富士电视台前游行示威。\[131\]同年，日本电视剧《[我和明星的99天](../Page/我和明星的99天.md "wikilink")》挑选韩星[金泰希为女主角](../Page/金泰希.md "wikilink")，也引来对日本富士电视台的抗议活动，因为泰希曾和弟弟在瑞士参加声援[独岛主权活动而被认为是](../Page/独岛.md "wikilink")“反日分子”。\[132\]
。另外由於[韓國並未完全開放日本的流行文化進入韓國文化市場](../Page/日本大眾文化流入限制.md "wikilink")，部分日本人認為韓流在日本的發展會使兩國之間的文化貿易中不對等性的加劇。\[133\]

2012年，[半岛电视台发布评论说韩国娱乐圈艺人拍戏工作压力大](../Page/半岛电视台.md "wikilink")，并披露娱乐圈内的[潜规则和](../Page/潜规则.md "wikilink")[贪污](../Page/贪污.md "wikilink")[腐败现象](../Page/腐败.md "wikilink")。\[134\]

根據《[東亞日報](../Page/東亞日報.md "wikilink")》等媒體報道，部分中國人認為韓流是一種“文化侵略”現象，會阻礙中國流行文化的發展。並且由於韓中兩國文化較為類似，因此韓流可能對中國流行文化甚至中華文化產生影響\[135\]\[136\]\[137\]。韓國文化觀光研究院經分析後認為韓流在中國的發展過程中，曾出現過題材內容同質性高，以及由於兩國歷史觀不同而在一些韓國影視作品中出現過度的[民族主義表現或是與中國人普遍認識的歷史觀點不符的現象](../Page/南韓民族主義.md "wikilink")（例如《[太王四神記](../Page/太王四神記.md "wikilink")》、《[朱蒙](../Page/朱蒙_\(電視劇\).md "wikilink")》等電視劇宣傳[高句麗的歷史為](../Page/高句麗.md "wikilink")[韓國歷史的一部分而導致中國宣布停止引進這些電視劇](../Page/韓國歷史.md "wikilink")\[138\]\[139\]），這些現象對韓流在中國的發展產生了一些不利的影響。\[140\]

2012年，华语歌星[周杰伦在出席某颁奖仪式时感叹](../Page/周杰伦.md "wikilink")：“虽然《江南Style》蛮好笑，但华语歌曲更厉害。不要被韩流追上，所有艺人要联合起来，不要再《江南Style》了。”\[141\]为对抗韩流，周杰伦还改了造型。\[142\]不过“艺术无国界”的声音在[中国还是要更强些](../Page/中国.md "wikilink")。\[143\]

在西方，由于韩流有韩国政府的背后支持，有些人将其和[美国](../Page/美国.md "wikilink")[CIA对前](../Page/CIA.md "wikilink")[苏联的](../Page/苏联.md "wikilink")相比较，也有人担心[维多利亚时代](../Page/维多利亚时代.md "wikilink")[黄祸的到来](../Page/黄祸.md "wikilink")，西方文化会被吞噬。\[144\]

## 参考文献

## 外部連結

  - 沈博斗：[韓國流行文化在亞洲的崛起混合](http://mcs.sagepub.com/content/28/1/25.full.pdf)，「媒體，文化與社會」，2006年1月，Vol.
    28，no. 1，pp. 25–44

## 参见

  - [反韓](../Page/反韓.md "wikilink")
  - [日流](../Page/日流.md "wikilink")
  - [韩国文化](../Page/韩国文化.md "wikilink")、[朝鲜文化](../Page/朝鲜文化.md "wikilink")
  - [日本文化](../Page/日本文化.md "wikilink")
  - [中国文化](../Page/中国文化.md "wikilink")
  - [台灣文化](../Page/台灣文化.md "wikilink")

{{-}}

[Category:韓國文化](../Category/韓國文化.md "wikilink")
[Category:喜爱外国文化](../Category/喜爱外国文化.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.
10.

11. [Korea Ranks 11th in Global Soft
    Power](http://english.chosun.com/site/data/html_dir/2012/11/20/2012112000616.html),
    The Chosun Ilbo

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22. [What is the future of Korean
    film?](http://nwww.koreaherald.com/common_prog/newsprint.php?ud=20120917000815&dt=2),
    *The Korea Herald*

23.

24.

25.

26.

27.

28.

29.

30.

31.

32. [When the Korean wave
    ripples](http://www.iias.nl/nl/42/IIAS_NL42_15.pdf), International
    Institute for Asian Studies

33.

34.

35.

36.

37. ["WBW: SM vs.
    DSP"](http://www.allkpop.com/article/2013/02/wbw-sm-vs-dsp).*Allkpop*.Retrieved
    2013-07-18.

38.

39.

40.

41.

42.

43.

44. [Iranians hooked on Korean TV
    drama](http://www.globalpost.com/dispatch/middle-east/091216/iran-korea-tv),
    *Global Post*

45. [《花样男子》获韩流粉丝肯定
    李敏镐发感言意味深长](http://m.allthatstar.com/view.php?action=304&postID=214772)

46. [首尔电视节落幕
    李敏镐李英爱获10年大赏](http://ent.sina.com.cn/v/j/2015-09-11/doc-ifxhupik6714143.shtml)

47.

48.

49.
50.

51.

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.
75.

76. 新華網新聞: <http://news.xinhuanet.com/world/2014-07/07/c_1111498042.htm>

77. TVBS新聞: <http://news.tvbs.com.tw/china/517899>

78.

79.

80.

81.

82.

83.

84.

85.
86.

87. [IRIB director visits location of South Korean TV series popular in
    Iran](http://www.tehrantimes.com/arts-and-culture/102496-irib-director-visits-location-of-south-korean-tv-series-popular-in-iran-)
    , *The Tehran Times*

88.

89.

90.

91.
92.

93.

94.

95. [Korean wave finds welcome in
    Iraq](http://www.korea.net/NewsFocus/Society/view?articleId=101264),
    KOREA.net

96.
97.
98.

99.

100.

101.
102.

103.
104.

105.

106.

107.

108.

109.

110.

111.

112. [South Korea’s pop-cultural
     exports](http://www.economist.com/node/15385735), *The Economist*

113. [South Korea’s K-pop takes off in the
     west](http://www.ft.com/intl/cms/s/0/ddf11662-53c7-11e1-9eac-00144feabdc0.html#axzz2KQsRXiiT),
     *Financial Times*

114. [Korean Cultural Exports Still
     Booming](http://english.chosun.com/site/data/html_dir/2012/12/12/2012121201407.html),
     *The Chosun Ilbo*

115.
116. [Korea Ranks 11th in Global Soft
     Power](http://english.chosun.com/site/data/html_dir/2012/11/20/2012112000616.html),
     *The Chosun Ilbo*

117.

118.
119.

120.

121.

122.
123.
124.

125.

126.

127.

128.

129.

130. 知日部屋：[《嫌韓流》：日本ACG的關東軍](http://www.cuhkacs.org/~benng/Bo-Blog/read.php?436)
     作者：吳偉明（香港中文大學日本研究學系副教授）（2005年8月8日）

131.

132.

133. [「韓流偏重批判に考慮を」
     自民・片山さつき議員が民放連に要請](http://www.j-cast.com/2011/08/31105803.html)

134.

135. [韓流에서 寒流로…"이젠 한국 드라마 안
     봐요"](http://www.dongabiz.com/TrendnIssue/IndustryTechnology/article_content.php?atno=1102010401&chap_no=1&sdkey=13224122596)

136. [新한류 중심이 된 스타, 그들을 주목해야하는
     이유](http://news.donga.com/3/all/20140522/63678405/1)

137. [항한류, 혐한류, 그리고 헐리웃의 문화
     침략](http://pub.chosun.com/client/news/viw.asp?cate=C03&mcate=m1004&nNewsNumb=20140414480&nidx=14436)

138. [《太王四神记》涉窜改历史
     国内可能被禁播](http://ent.163.com/07/0914/10/3OBHDKSD000327RA.html)

139. [韩国热播电视剧歪曲高句丽史实
     成针对中国武器](http://news.cctv.com/world/20070426/100224.shtml)

140. [반한류 현황분석 및 대응방안
     연구(한국문화관광연구원)](https://www.kcti.re.kr/webdata/newweb/reportEng/file/g09-25.pdf)

141.

142.

143.
144.