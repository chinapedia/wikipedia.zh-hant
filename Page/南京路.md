**南京路**可以指

## 行政區劃

  - [南京路
    (金朝)](../Page/南京路_\(金朝\).md "wikilink")，或称南京開封路，历史上金朝行政区域之一，今河南省開封市附近。

## 道路名称

### 中國大陆

  - [南京路
    (上海)](../Page/南京路_\(上海\).md "wikilink")，[上海市道路](../Page/上海市.md "wikilink")，包括[南京东路与](../Page/南京东路_\(上海\).md "wikilink")[南京西路](../Page/南京西路_\(上海\).md "wikilink")，上海当地所称的南京路一般特指[南京东路](../Page/南京东路_\(上海\).md "wikilink")。
  - [南京路
    (天津)](../Page/南京路_\(天津\).md "wikilink")，[天津市道路](../Page/天津市.md "wikilink")。
  - [南京路
    (重慶)](../Page/南京路_\(重慶\).md "wikilink")，[重慶市](../Page/重慶市.md "wikilink")[北碚區道路](../Page/北碚區.md "wikilink")。
  - [南京路
    (青島)](../Page/南京路_\(青島\).md "wikilink")，[山東省](../Page/山東省.md "wikilink")[青島市道路](../Page/青島市.md "wikilink")。
  - [南京路
    (渭南)](../Page/南京路_\(渭南\).md "wikilink")，[陕西省](../Page/陕西省.md "wikilink")[渭南市道路](../Page/渭南市.md "wikilink")。
  - [南京路
    (哈爾濱)](../Page/南京路_\(哈爾濱\).md "wikilink")，[黑龍江省](../Page/黑龍江省.md "wikilink")[哈爾濱市道路](../Page/哈爾濱市.md "wikilink")。
  - [南京路
    (锦州)](../Page/南京路_\(锦州\).md "wikilink")，[辽宁省](../Page/辽宁省.md "wikilink")[锦州市道路](../Page/锦州市.md "wikilink")。
  - [南京路
    (商丘)](../Page/南京路_\(商丘\).md "wikilink")，[河南省](../Page/河南省.md "wikilink")[商丘市道路](../Page/商丘市.md "wikilink")。
  - [南京路
    (信阳)](../Page/南京路_\(信阳\).md "wikilink")，[河南省](../Page/河南省.md "wikilink")[信阳市道路](../Page/信阳市.md "wikilink")。
  - [南京路
    (淮安)](../Page/南京路_\(淮安\).md "wikilink")，[江苏省](../Page/江苏省.md "wikilink")[淮安市道路](../Page/淮安市.md "wikilink")。
  - [南京路
    (徐州)](../Page/南京路_\(徐州\).md "wikilink")，[江苏省](../Page/江苏省.md "wikilink")[徐州市道路](../Page/徐州市.md "wikilink")。
  - [南京路
    (湖州)](../Page/南京路_\(湖州\).md "wikilink")，[浙江省](../Page/浙江省.md "wikilink")[湖州市道路](../Page/湖州市.md "wikilink")。
  - [南京路
    (溫嶺)](../Page/南京路_\(溫嶺\).md "wikilink")，[浙江省](../Page/浙江省.md "wikilink")[溫嶺市道路](../Page/溫嶺市.md "wikilink")。
  - [南京路
    (瑞安)](../Page/南京路_\(瑞安\).md "wikilink")，[浙江省](../Page/浙江省.md "wikilink")[瑞安市道路](../Page/瑞安市.md "wikilink")。
  - [南京路
    (南昌)](../Page/南京路_\(南昌\).md "wikilink")，[江西省](../Page/江西省.md "wikilink")[南昌市道路](../Page/南昌市.md "wikilink")，分為南京東路、南京西路。
  - [南京路
    (贛州)](../Page/南京路_\(贛州\).md "wikilink")，[江西省](../Page/江西省.md "wikilink")[贛州市道路](../Page/贛州市.md "wikilink")。
  - [南京路
    (景德镇)](../Page/南京路_\(景德镇\).md "wikilink")，[江西省](../Page/江西省.md "wikilink")[景德镇市道路](../Page/景德镇市.md "wikilink")。
  - [南京路
    (黄石)](../Page/南京路_\(黄石\).md "wikilink")，[湖北省](../Page/湖北省.md "wikilink")[黄石市道路](../Page/黄石市.md "wikilink")。
  - [南京路
    (武漢)](../Page/南京路_\(武漢\).md "wikilink")，[湖北省](../Page/湖北省.md "wikilink")[武漢市道路](../Page/武漢市.md "wikilink")。
  - [南京路
    (荆门)](../Page/南京路_\(荆门\).md "wikilink")，[湖北省](../Page/湖北省.md "wikilink")[荆门市道路](../Page/荆门市.md "wikilink")。
  - [南京路
    (十堰)](../Page/南京路_\(十堰\).md "wikilink")，[湖北省](../Page/湖北省.md "wikilink")[十堰市道路](../Page/十堰市.md "wikilink")。
  - [南京路
    (广州)](../Page/南京路_\(广州\).md "wikilink")，[廣東省](../Page/廣東省.md "wikilink")[廣州市道路](../Page/廣州市.md "wikilink")。
  - [南京路
    (湛江)](../Page/南京路_\(湛江\).md "wikilink")，[廣東省](../Page/廣東省.md "wikilink")[湛江市道路](../Page/湛江市.md "wikilink")。
  - [南京路
    (南寧)](../Page/南京路_\(南寧\).md "wikilink")，[廣西省](../Page/廣西省.md "wikilink")[南寧市道路](../Page/南寧市.md "wikilink")。
  - [南京路
    (北海)](../Page/南京路_\(北海\).md "wikilink")，[廣西省](../Page/廣西省.md "wikilink")[北海市道路](../Page/北海市.md "wikilink")。
  - [南京路
    (廣漢)](../Page/南京路_\(廣漢\).md "wikilink")，[四川省](../Page/四川省.md "wikilink")[廣漢市道路](../Page/廣漢市.md "wikilink")。
  - [南京路
    (绵竹)](../Page/南京路_\(绵竹\).md "wikilink")，[四川省](../Page/四川省.md "wikilink")[绵竹市道路](../Page/绵竹市.md "wikilink")。
  - [南京路
    (遵义)](../Page/南京路_\(遵义\).md "wikilink")，[贵州省](../Page/贵州省.md "wikilink")[遵义市道路](../Page/遵义市.md "wikilink")。
  - [南京路
    (墨竹工卡)](../Page/南京路_\(墨竹工卡\).md "wikilink")，[西藏](../Page/西藏.md "wikilink")[墨竹工卡县道路](../Page/墨竹工卡县.md "wikilink")。

### 台灣

  - [南京西路
    (臺北市)](../Page/南京西路_\(臺北市\).md "wikilink")，[臺北市街道](../Page/臺北市.md "wikilink")。
  - [南京東路
    (臺北市)](../Page/南京東路_\(臺北市\).md "wikilink")，[臺北市街道](../Page/臺北市.md "wikilink")。
  - [南京路
    (台中市)](../Page/南京路_\(台中市\).md "wikilink")，[臺中市街道](../Page/臺中市.md "wikilink")。
  - [南京東路
    (台中市)](../Page/南京東路_\(台中市\).md "wikilink")，[臺中市街道](../Page/臺中市.md "wikilink")。
  - [南京路
    (斗六市)](../Page/南京路_\(斗六市\).md "wikilink")，[雲林縣](../Page/雲林縣.md "wikilink")[斗六市街道](../Page/斗六市.md "wikilink")。
  - [南京路
    (嘉義市)](../Page/南京路_\(嘉義市\).md "wikilink")，[嘉義市街道](../Page/嘉義市.md "wikilink")。
  - [南京路
    (鳳山區)](../Page/南京路_\(鳳山區\).md "wikilink")，[高雄市](../Page/高雄市.md "wikilink")[鳳山區街道](../Page/鳳山區.md "wikilink")。
  - [南京路
    (屏東市)](../Page/南京路_\(屏東市\).md "wikilink")，[屏东县](../Page/屏东县.md "wikilink")[屏東市街道](../Page/屏東市.md "wikilink")。
  - [南京路
    (臺東市)](../Page/南京路_\(臺東市\).md "wikilink")，[臺東縣](../Page/臺東縣.md "wikilink")[臺東市街道](../Page/臺東市.md "wikilink")。

## 其他

  - 有多條名為[南京街的街道](../Page/南京街.md "wikilink")。