**線外貨運站**（，Off-Rail-Station，縮寫為**ORS**）是[日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")（JR貨物）的一種[貨運車站型態](../Page/貨運車站.md "wikilink")。雖然是貨運鐵路公司所設的車站，但ORS實際上並沒有[貨運列車停靠](../Page/貨運列車.md "wikilink")，或甚至連貨運專用的鐵路路線都不見得有鋪設，取而代之的是以每天固定往返的[卡車或](../Page/卡車.md "wikilink")[貨櫃車等替代方式](../Page/貨櫃車.md "wikilink")，將ORS收到的貨物透過[公路運輸送到最靠近的JR貨物所屬之](../Page/公路運輸.md "wikilink")[貨運總站](../Page/貨運總站.md "wikilink")（）再裝卸到鐵路列車上。

ORS是JR貨物在2006年4月1日之後才統一開始啟用的車站命名，在此之前這些車站原名**貨櫃中心**（）或**汽車代行車站**（）。ORS通常是一些隨著時代改變、貨運業務減少，不再值得開辦貨運列車班次的貨運車站，因此改以發車彈性較高、比較適合低運量的公路運輸方式替代。在（[日本國鐵時代最後一次大規模的時刻表修正](../Page/日本國鐵.md "wikilink")）時，當時日本國鐵將全日本13個業務量不足的貨運站降級為貨櫃中心。而在2006年4月正式改名為ORS時，日本全國共有34座這樣的貨運站存在（之後增加至37個，另外3個在ORS成立後再被取消），這些數字包含了實際上具有車站地位的ORS，與實際上並非車站定位、但仍然提供此類貨運服務的JR貨物據點。

## ORS列表

### 營業中ORS

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>ORS站名</p></th>
<th><p>所在車站</p></th>
<th style="text-align: center;"><p>實際集散點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>中文</p></td>
<td><p>日文</p></td>
<td style="text-align: center;"><p>中文</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>北海道支社</strong></p></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/中斜里線外貨運站.md" title="wikilink">中斜里線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/中斜里車站.md" title="wikilink">中斜里車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/名寄線外貨運站.md" title="wikilink">名寄線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/名寄車站.md" title="wikilink">名寄車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/小樽築港線外貨運站.md" title="wikilink">小樽築港線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/小樽築港車站.md" title="wikilink">小樽築港車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>東北支社</strong></p></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/六原線外貨運站.md" title="wikilink">六原線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/六原站.md" title="wikilink">六原站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/山形線外貨運站.md" title="wikilink">山形線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p>無</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/橫手線外貨運站.md" title="wikilink">橫手線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/橫手車站.md" title="wikilink">橫手車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/羽後本莊線外貨運站.md" title="wikilink">羽後本莊線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/羽後本莊車站.md" title="wikilink">羽後本莊車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/東福島線外貨運站.md" title="wikilink">東福島線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/東福島車站.md" title="wikilink">東福島車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/名取線外貨運站.md" title="wikilink">名取線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/名取站.md" title="wikilink">名取站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/會津若松線外貨運站.md" title="wikilink">會津若松線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/會津若松車站.md" title="wikilink">會津若松車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/古川線外貨運站.md" title="wikilink">古川線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/古川車站.md" title="wikilink">古川車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>關東支社</strong></p></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/柏崎線外貨運站.md" title="wikilink">柏崎線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/柏崎車站.md" title="wikilink">柏崎車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/中條線外貨運站.md" title="wikilink">中條線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/中條車站.md" title="wikilink">中條車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/羽前水澤線外貨運站.md" title="wikilink">羽前水澤線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/羽前水澤站.md" title="wikilink">羽前水澤站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/水戶線外貨運站.md" title="wikilink">水戶線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/水戶站.md" title="wikilink">水戶站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/羽生線外貨運站.md" title="wikilink">羽生線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p>無</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/矢板線外貨運站.md" title="wikilink">矢板線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/矢板車站.md" title="wikilink">矢板車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/八王子線外貨運站.md" title="wikilink">八王子線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/八王子車站.md" title="wikilink">八王子車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/岡谷線外貨運站.md" title="wikilink">岡谷線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/岡谷車站.md" title="wikilink">岡谷車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>東海支社</strong></p></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/豐橋線外貨運站.md" title="wikilink">豐橋線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/豐橋車站.md" title="wikilink">豐橋車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/刈谷線外貨運站.md" title="wikilink">刈谷線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/刈谷車站.md" title="wikilink">刈谷車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>關西支社</strong></p></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/青海線外貨運站.md" title="wikilink">青海線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/青海站_(新潟縣).md" title="wikilink">青海車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/魚津站.md" title="wikilink">魚津線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/魚津站.md" title="wikilink">魚津車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/敦賀港線外貨運站.md" title="wikilink">敦賀港線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/敦賀港車站.md" title="wikilink">敦賀港車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/和歌山線外貨運站.md" title="wikilink">和歌山線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p>無</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/福知山線外貨運站.md" title="wikilink">福知山線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p>無</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/湖山線外貨運站.md" title="wikilink">湖山線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/湖山車站.md" title="wikilink">湖山車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/東松江線外貨運站.md" title="wikilink">東松江線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/東松江車站.md" title="wikilink">東松江車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/高知線外貨運站.md" title="wikilink">高知線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p>無</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/德島線外貨運站.md" title="wikilink">德島線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p>無</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/糸崎線外貨運站.md" title="wikilink">糸崎線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/糸崎車站.md" title="wikilink">糸崎車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/防府線外貨運站.md" title="wikilink">防府線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/防府貨物車站.md" title="wikilink">防府貨物車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>九州支社</strong></p></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/大牟田線外貨運站.md" title="wikilink">大牟田線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/大牟田車站.md" title="wikilink">大牟田車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/長崎線外貨運站.md" title="wikilink">長崎線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/長崎車站.md" title="wikilink">長崎車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/有田線外貨運站.md" title="wikilink">有田線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/有田車站.md" title="wikilink">有田車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/佐土原線外貨運站.md" title="wikilink">佐土原線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/佐土原車站.md" title="wikilink">佐土原車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/都城線外貨運站.md" title="wikilink">都城線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/都城車站.md" title="wikilink">都城車站</a></p></td>
</tr>
</tbody>
</table>

除了上述37個ORS之外，位於北海道地區的[北見車站與](../Page/北見車站.md "wikilink")[富良野車站雖然並非ORS](../Page/富良野車站.md "wikilink")，但在淡季貨運量較少時，會暫時性地以類似ORS的運作方式，靠公路運輸將貨物運到[北旭川車站與](../Page/北旭川車站.md "wikilink")[札幌貨物總站](../Page/札幌貨物總站.md "wikilink")（）集散裝卸。

### 已停用的ORS

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>ORS站名</p></th>
<th><p>所在車站</p></th>
<th style="text-align: center;"><p>實際集散點</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>中文</p></td>
<td><p>日文</p></td>
<td style="text-align: center;"><p>中文</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/東三條線外貨運站.md" title="wikilink">東三條線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/東三條車站.md" title="wikilink">東三條車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/東能代線外貨運站.md" title="wikilink">東能代線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/東能代車站.md" title="wikilink">東能代車站</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/廣線外貨運站.md" title="wikilink">廣線外貨運站</a></p></td>
<td></td>
<td style="text-align: center;"><p><a href="../Page/廣車站.md" title="wikilink">廣車站</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 參考文獻

  -
[線外貨運站](../Category/線外貨運站.md "wikilink")
[Category:日本貨物鐵道車站](../Category/日本貨物鐵道車站.md "wikilink")