**尹海英**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/演員.md "wikilink")。2010年2月20日，從[數位首爾文化藝術大學美容藝術學系畢業](../Page/數位首爾文化藝術大學.md "wikilink")，取得[學士](../Page/學士.md "wikilink")[學位](../Page/學位.md "wikilink")。

## 演出作品

### 電視劇

  - 1995年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[期待重逢](../Page/期待重逢.md "wikilink")》
  - 1996年：[KBS](../Page/韓國放送公社.md "wikilink")《[今天是南東風](../Page/今天是南東風.md "wikilink")》
  - 2001年：KBS《[人生多美好](../Page/人生多美好.md "wikilink")》飾演 劉秀晶
  - 2001年：KBS《[小氣家族](../Page/小氣家族.md "wikilink")》
  - 2003年：KBS《[百萬朵玫瑰](../Page/百萬朵玫瑰.md "wikilink")》
  - 2005年：SBS《[鑽石的眼淚](../Page/鑽石的眼淚.md "wikilink")》
  - 2006年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[愛情無法擋](../Page/愛情無法擋.md "wikilink")》
  - 2006年：SBS《[愛情與野望](../Page/愛情與野望#2006年.md "wikilink")》飾演 在恩
  - 2007年：KBS《[比天高比地厚](../Page/比天高比地厚.md "wikilink")》飾演 朴明珠
  - 2008年：MBC《[大象](../Page/大象_\(電視劇\).md "wikilink")》飾演 海英
  - 2008年：KBS《[我的愛金枝玉葉](../Page/我的愛金枝玉葉.md "wikilink")》飾演 朴千麻
  - 2009年：KBS《[薔花紅蓮](../Page/薔花紅蓮.md "wikilink")》飾演 紅蓮
  - 2010年：KBS《Drama Special－熱咖啡》
  - 2012年：KBS《[Big](../Page/Big.md "wikilink")》飾演 李靜慧（多蘭母）
  - 2012年：SBS《[因為是你才喜歡](../Page/因為是你才喜歡.md "wikilink")》飾演 姜珍珠
  - 2013年：KBS《[總理與我](../Page/總理與我.md "wikilink")》飾演 羅允熙
  - 2014年：MBC《[狎鷗亭白夜](../Page/狎鷗亭白夜.md "wikilink")》飾演 都敏玖
  - 2015年：KBS《[依然綠茵的日子](../Page/依然綠茵的日子.md "wikilink")》飾演 鄭德希／鄭愛心
  - 2015年：MBC《[華麗的誘惑](../Page/華麗的誘惑.md "wikilink")》飾演 白清美
  - 2016年：SBS《[Doctors](../Page/Doctors.md "wikilink")》飾演 尹智英
  - 2018年：SBS《[Ms.Ma，復仇的女神](../Page/Ms.Ma，復仇的女神.md "wikilink")》

### 電影

  - 2008年：《[很抱歉的城市](../Page/很抱歉的城市.md "wikilink")》（客串）

## 外部連結

  - [EPG](https://web.archive.org/web/20050924131113/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=986)


  - [NAVER](http://people.naver.com/DetailView.nhn?id=748)

  -
[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:京畿大學校友](../Category/京畿大學校友.md "wikilink")
[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")
[Hae](../Category/尹姓.md "wikilink")