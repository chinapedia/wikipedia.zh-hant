**干福熹**（），中国光学材料、非晶态物理学家，[中国科学院院士](../Page/中国科学院.md "wikilink")，上海光学精密机械研究所研究员，[复旦大学信息学院教授](../Page/复旦大学.md "wikilink")，博士生导师。

## 生平

1933年出生于[浙江](../Page/浙江.md "wikilink")[杭州](../Page/杭州.md "wikilink")，1952年毕业于[浙江大学工学院化学工程学系](../Page/浙江大学.md "wikilink")，1959年获前[苏联科学院硅酸盐化学研究所副博士学位](../Page/苏联.md "wikilink")。1980年当选为中国科学院院士。

## 參考資料

[Category:中国物理学家](../Category/中国物理学家.md "wikilink")
[Category:浙江大学校友](../Category/浙江大学校友.md "wikilink")
[Category:浙江科学家](../Category/浙江科学家.md "wikilink")
[Category:杭州人](../Category/杭州人.md "wikilink")
[F](../Category/干姓.md "wikilink")