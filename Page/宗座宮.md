[0_Palazzo_Apostolico_-_Piazza_San_Pietro_(1).JPG](https://zh.wikipedia.org/wiki/File:0_Palazzo_Apostolico_-_Piazza_San_Pietro_\(1\).JPG "fig:0_Palazzo_Apostolico_-_Piazza_San_Pietro_(1).JPG")

**宗座宮**（；）是[教宗的正式](../Page/教宗.md "wikilink")[官邸](../Page/官邸.md "wikilink")，位於[梵蒂岡城](../Page/梵蒂岡城.md "wikilink")，又稱**教宗宮**、**使徒宮**、**梵蒂岡宮**或**神聖宮殿**\[1\]。最早於5世紀由[教宗西玛克籌建](../Page/教宗西玛克.md "wikilink")，做為[拉特朗宮的替代居所](../Page/拉特朗宮.md "wikilink")；後來經過多次毀壞及重建，直到1447年，教宗[尼古拉五世決定興建新的建築](../Page/尼古拉五世.md "wikilink")，即今日所見之宗座宮。

該官邸結搆複雜。內設、[羅馬教廷各機構](../Page/羅馬教廷.md "wikilink")、眾多[小聖堂](../Page/小聖堂.md "wikilink")、[梵蒂岡博物館](../Page/梵蒂岡博物館.md "wikilink")、[梵蒂岡圖書館等](../Page/梵蒂岡圖書館.md "wikilink")。在這1千多個房間中，最著名是繪有[米開朗基羅壁畫](../Page/米開朗基羅.md "wikilink")《[最後的審判](../Page/最後的審判.md "wikilink")》和《[創世紀](../Page/創世紀_\(壁畫\).md "wikilink")》的[西斯廷小堂](../Page/西斯廷小堂.md "wikilink")，以及[拉斐爾房間等](../Page/拉斐爾房間.md "wikilink")。

其他教宗宅邸尚有拉特朗宮、以及[羅馬城外東南](../Page/羅馬.md "wikilink")30公里處的[岡道爾夫堡教宗別墅](../Page/岡道爾夫堡教宗別墅.md "wikilink")。梵蒂岡宮在15世紀取代了拉特朗宮，但後來它曾一度被[奎里納爾宮搶了風頭](../Page/奎里納爾宮.md "wikilink")，但奎里納爾宮在[義大利統一後成為義大利國家元首的官邸](../Page/義大利統一.md "wikilink")。

## 現狀

現任教宗[方济各就任後並沒有入住宗座宮](../Page/方濟各_\(教宗\).md "wikilink")，而是選擇居住在[聖瑪爾大之家](../Page/聖瑪爾大之家.md "wikilink")\[2\]。

## 参考文献

## 参见

  - [教宗宮殿](../Page/教宗宮殿.md "wikilink")
  - [波奇亚寓所](../Page/波奇亚寓所.md "wikilink")
  - [克萊孟大廳](../Page/克萊孟大廳.md "wikilink")
  - [聖瑪爾大之家](../Page/聖瑪爾大之家.md "wikilink")
  - [教宗绅士](../Page/教宗绅士.md "wikilink")

{{-}}

[Category:教宗](../Category/教宗.md "wikilink")
[Category:梵蒂岡建築物](../Category/梵蒂岡建築物.md "wikilink")

1.

2.