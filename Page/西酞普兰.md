**西酞普兰**（**Citalopram**）是一种很强的[选择性5-羟色胺再吸收抑制剂](../Page/选择性5-羟色胺再吸收抑制剂.md "wikilink")（SSRI）型的[抗抑郁药](../Page/抗抑郁药.md "wikilink")，其药物形态为**氢溴酸西酞普兰**（**Citalopram
Hydrobromide**），商品名为“**喜普妙**”（**Celexa**）。在临床上常用于[抑郁性精神障碍](../Page/抑郁性精神障碍.md "wikilink")。

## 歷史

西酞普兰最初是在1989年由药厂研制成\[1\]。相应的专利已经在2003年过期，此后其它公司可以合法的制造同类药物。

## 立体化学

西酞普兰以外消旋的形式出售，其中含有 50% 的 (*R*)-(−)-西酞普兰 与 50% 的
(*S*)-(+)-西酞普兰。只有右旋异构体具有预期的抗抑郁的功效\[2\]。灵北药厂生产的仅含右旋体的产品已经上市，通用名为[艾司西酞普兰](../Page/艾司西酞普兰.md "wikilink")，与消旋体一般制成[氢溴酸盐不同](../Page/氢溴酸.md "wikilink")，艾司西酞普兰一般以[草酸盐的形式出售](../Page/草酸盐.md "wikilink")，相应的商品名为“**来士普**”（**Lexapro**）。将西酞普兰或艾司西酞普兰制成[盐主要是为了增加其](../Page/盐.md "wikilink")[水溶性](../Page/水溶性.md "wikilink")。

## 參考資料

[category:选择性血清素重摄取抑制剂](../Page/category:选择性血清素重摄取抑制剂.md "wikilink")

[Category:腈](../Category/腈.md "wikilink")
[Category:有机氟化合物](../Category/有机氟化合物.md "wikilink")
[Category:异苯并呋喃](../Category/异苯并呋喃.md "wikilink")
[Category:胺](../Category/胺.md "wikilink")

1.
2.