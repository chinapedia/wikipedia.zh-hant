**香港網絡大典**（，英文簡稱，中文簡稱**網典**）是一個以[香港網路內容為主題](../Page/香港互聯網文化.md "wikilink")，於[Wikia提供的平台成立的網絡共筆型](../Page/Wikia.md "wikilink")[百科全書](../Page/百科全書.md "wikilink")，始於2006年3月21日。使用[正體中文](../Page/正體中文.md "wikilink")，僅供認證會員編輯，不支持[IP及匿名寫手](../Page/IP.md "wikilink")。

[馬尼拉人質事件事發當日](../Page/馬尼拉人質事件.md "wikilink")，[香港電台播出](../Page/香港電台.md "wikilink")《[鏗鏘集](../Page/鏗鏘集.md "wikilink")
-
網絡審判團》，節目中多次以香港網絡大典網頁[截圖試圖把](../Page/截圖.md "wikilink")[起底與網典拉上關係](../Page/起底.md "wikilink")，香港網絡大典事後在網頁抬頭展出聲明，指一眾網典編輯者及其管理團隊認為該集是向網典作出不實指控，並表示深切遺憾\[1\]，[東方報業集團入稟](../Page/東方報業集團.md "wikilink")[香港高等法院](../Page/香港高等法院.md "wikilink")，指「香港網絡大典」刊登誹謗該集團的文章，控告其網站登記人Wikia公司。翌年7月7日，法院判決東方報業集團勝訴，同時頒佈禁制令、賠償令及訟費命令，金額稍後由聆案官決定。\[2\]

2011年8月12日起，香港網絡大典已經實施外部限制，只容許已註冊並通過電郵驗證的用戶編輯，其他很多註冊已久但沒有經過電郵驗證的用戶已無法編輯，會被返回「You
do not have permission to edit pages」。

截至2015年10月，香港網絡大典條目數量已超越11,438條，流量曾於各中文wikia網站之中最高。\[3\]

## 相關

  - [用於中文維基的香港網絡大典的模板](../Page/:Template:Evchk.md "wikilink")
  - [香港互聯網文化](../Page/香港互聯網文化.md "wikilink")
  - [小丑神](../Page/小丑神.md "wikilink")
  - [高登討論區](../Page/高登討論區.md "wikilink")
  - [恶搞文化](../Page/恶搞文化.md "wikilink")

## 參考文獻

## 外部連結

  - [香港網絡大典](https://evchk.fandom.com)

  -
  -
[Category:香港網絡文化](../Category/香港網絡文化.md "wikilink")
[Category:香港網站](../Category/香港網站.md "wikilink")
[Category:恶搞文化](../Category/恶搞文化.md "wikilink")
[Category:Wikia](../Category/Wikia.md "wikilink")

1.  [(高清)
    鏗鏘集：網絡審判團 2010-08-22(1)](http://www.youtube.com/watch?v=4gmvgROzm_8)，《鏗鏘集－各師各法》[Youtube影片](../Page/Youtube.md "wikilink")
2.
3.