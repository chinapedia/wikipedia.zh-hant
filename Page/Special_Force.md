《**Special
Force**》是一個具有軍事性質的[第一人稱射擊遊戲](../Page/第一人稱射擊遊戲.md "wikilink")，據宣稱是由[伊斯蘭組織](../Page/伊斯蘭教.md "wikilink")[真主黨發行](../Page/真主黨.md "wikilink")\[1\]，使用的繪圖引擎是。

## 簡介

《Special
Force》讓玩家帶領一部分的伊斯蘭反抗軍，協助[黎巴嫩抵抗](../Page/黎巴嫩.md "wikilink")[以色列的入侵行動](../Page/以色列.md "wikilink")。在遊戲的盒裝封面上，給使用者的標語說明是「遊戲的設計者感到非常自豪，把這個特別的產品提供給各位玩家，在以色列入侵黎巴嫩的行動裡，這款遊戲以客觀角度具體呈現以色列敵軍的失敗，以及呈現出伊斯蘭反抗軍英雄的英勇事蹟」。還增加了其他的標語，「成為勝利的伙伴。在武裝遊戲裡，與你的敵人戰鬥、反抗敵人以及毀滅你的敵人並取得勝利」。\[2\]

真主黨辦公室的官員Mahmoud Rayya說：「這個遊戲是經由媒體傳達出反抗以色列佔領行動的意念，某種程度上，《Special
Force》提供玩家包含心理的個人訓練，讓玩家感受到遊戲裡的世界只不過是相當於反抗鬥士的鞋子那麼小而已」\[3\]。

遊戲的語言包含[阿拉伯語](../Page/阿拉伯語.md "wikilink")、[英語](../Page/英語.md "wikilink")、[法語和](../Page/法語.md "wikilink")[波斯語](../Page/波斯語.md "wikilink")。2003年初，遊戲在首发一週內賣出八千套\[4\]，發行的地區有[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[敘利亞](../Page/敘利亞.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[巴林和](../Page/巴林.md "wikilink")[阿拉伯聯合大公國](../Page/阿拉伯聯合大公國.md "wikilink")。

## 相關項目

  - 《[美國陸軍系列](../Page/美國陸軍系列.md "wikilink")》

  -
## 參考資料

## 擴展資源

  - [數位伊斯蘭網站，數位化的阿拉伯人在電玩遊戲裡的呈現方式（Digital Arabs: Representation in Video
    Games）](http://www.digitalislam.eu/article.do?articleId=1704)

## 外部連結

  - [官方網站（網頁位在Wayback
    Machine網站）](https://web.archive.org/web/20050105091655/http://www.specialforce.net/english/indexeng.htm)

  -
[Category:2003年电子游戏](../Category/2003年电子游戏.md "wikilink")
[Category:廣告遊戲](../Category/廣告遊戲.md "wikilink")
[Category:政治宣傳遊戲](../Category/政治宣傳遊戲.md "wikilink")
[Category:第一人称射击游戏](../Category/第一人称射击游戏.md "wikilink")
[Category:黎巴嫩文化](../Category/黎巴嫩文化.md "wikilink")

1.
2.  <http://www.worldnetdaily.com/news/article.asp?ARTICLE_ID=31323>
    "Hezbollah's new computer game"\].
    *[WorldNetDaily](../Page/WorldNetDaily.md "wikilink")*, [March
    3](../Page/March_3.md "wikilink"), 2003. Accessed March 24, 2007.
3.
4.