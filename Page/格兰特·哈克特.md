**格兰特·乔治·哈克特**（**Grant George
Hackett**，）生於[昆士兰州](../Page/昆士兰州.md "wikilink")[绍斯波特](../Page/绍斯波特_\(昆士兰州\).md "wikilink")，[澳大利亚](../Page/澳大利亚.md "wikilink")[游泳运动员](../Page/游泳.md "wikilink")。
格兰特獲得[2004年雅典奥运会男子](../Page/2004年雅典奥运会.md "wikilink")1500米自由泳的[金牌以及](../Page/金牌.md "wikilink")[2008年北京奥运会](../Page/2008年北京奥运会.md "wikilink")1500米[自由泳的](../Page/自由泳.md "wikilink")[银牌](../Page/银牌.md "wikilink")。

## 近况

2006年，哈克特在训练中左肺坏死，因此变成伤残人；
不过他依然保持刻苦训练，并最终得到[2008年夏季奥林匹克运动会的](../Page/2008年夏季奥林匹克运动会.md "wikilink")1500米自由泳亚军。

## 參考資料

[Category:游泳世界紀錄保持者](../Category/游泳世界紀錄保持者.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:澳大利亚奥林匹克运动会金牌得主](../Category/澳大利亚奥林匹克运动会金牌得主.md "wikilink")
[Category:澳大利亚奥林匹克运动会银牌得主](../Category/澳大利亚奥林匹克运动会银牌得主.md "wikilink")
[Category:澳大利亚奥林匹克运动会铜牌得主](../Category/澳大利亚奥林匹克运动会铜牌得主.md "wikilink")
[Category:澳大利亚奥运游泳运动员](../Category/澳大利亚奥运游泳运动员.md "wikilink")
[Category:昆士蘭人](../Category/昆士蘭人.md "wikilink")
[Category:2000年夏季奧林匹克運動會游泳運動員](../Category/2000年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會游泳運動員](../Category/2004年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會游泳運動員](../Category/2008年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:奧林匹克運動會游泳獎牌得主](../Category/奧林匹克運動會游泳獎牌得主.md "wikilink")
[Category:世界游泳錦標賽游泳賽事獎牌得主](../Category/世界游泳錦標賽游泳賽事獎牌得主.md "wikilink")
[Category:世界短道游泳錦標賽獎牌得主](../Category/世界短道游泳錦標賽獎牌得主.md "wikilink")
[Category:男子自由式游泳運動員](../Category/男子自由式游泳運動員.md "wikilink")
[Category:英联邦运动会游泳奖牌得主](../Category/英联邦运动会游泳奖牌得主.md "wikilink")
[Category:英联邦运动会澳大利亚金牌得主](../Category/英联邦运动会澳大利亚金牌得主.md "wikilink")
[Category:奥林匹克运动会游泳铜牌得主](../Category/奥林匹克运动会游泳铜牌得主.md "wikilink")