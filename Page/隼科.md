**隼科**（[学名](../Page/学名.md "wikilink")：）是[鸟纲](../Page/鸟纲.md "wikilink")**隼形目**（[学名](../Page/学名.md "wikilink")：）底下的唯一單[科](../Page/科.md "wikilink")，共10属63种，在中国有2属14种分布。

在旧有的[鸟类传统分类系统中隼形目包含现在已经分开的](../Page/鸟类传统分类系统.md "wikilink")[鷹形目](../Page/鷹形目.md "wikilink")（Accipitriformes），包括了汉语中常说的[鹰](../Page/鹰.md "wikilink")、[隼](../Page/隼.md "wikilink")、[鵟](../Page/鵟.md "wikilink")、[鵰](../Page/鵰.md "wikilink")、[鹫](../Page/鹫.md "wikilink")、[鸢等肉食性猛禽](../Page/鸢.md "wikilink")，直至2008年，[DNA研究显示](../Page/DNA.md "wikilink")，隼科與其他猛禽的亲缘关系并不接近，反而是與[鸚形目和](../Page/鸚形目.md "wikilink")[雀形目等較相近](../Page/雀形目.md "wikilink")。，因而将鷹形目與隼形目分開，隼形目下只剩隼科一科。

## 分布

隼形目的鸟在世界上分布广泛，除[南极和少数岛屿外](../Page/南极.md "wikilink")，世界各地都有分布。

## 外形

隼形目的鸟体形差别很大，如雕、鹫等体形较大，体长可达120厘米；鸢和隼的体形較小约30\~50厘米，有的如[红腿小隼和](../Page/红腿小隼.md "wikilink")[白腿小隼只有](../Page/白腿小隼.md "wikilink")10\~20厘米，比[麻雀大不了多少](../Page/麻雀.md "wikilink")。隼形目的鸟[羽色以棕](../Page/羽毛.md "wikilink")、黑、白为主，腹面的颜色比背面的颜色浅，有利于猎捕中的隐蔽。隼形目的鸟的腿较长，粗壮有力，都有弯曲带钩、强壮且锐利的[喙和爪](../Page/喙.md "wikilink")，这是它们捕猎中致命的工具。主要以腐肉为食的[秃鹫](../Page/秃鹫.md "wikilink")、[兀鹫等](../Page/兀鹫.md "wikilink")，其喙和爪的锋利程度要比捕猎技巧高超的鹰、雕逊色很多。隼形目的鸟视力敏锐，可以在远距离发现快速移动的猎物，并能快速调整对焦。

## 习性

隼形目所具有的一切特征都说明它们是自然界中的好猎手，多在白天活动。它们善于捕猎，飞行技巧高超，给人以凶猛的印象。依据体形的不同，它们的食物从[哺乳动物到](../Page/哺乳动物.md "wikilink")[昆虫各有差异](../Page/昆虫.md "wikilink")。有些鸟自己不主动猎食，专吃已死亡的动物的尸体。

隼形目鸟的寿命较长，但繁殖成功率不高。野生的隼形目鸟寿命会远远小于人工喂养的情况，野生状况下威胁最大的是在捕猎中受伤后引起的[感染](../Page/感染.md "wikilink")。

隼形目的鸟多是一夫一妻，雌雄共同哺育后代。幼鸟生长得很快，有些种类的幼鸟离巢前要比成鸟体形大。幼鸟要经过1-3年的时间才会性成熟。隼形目的鸟多在高树或悬崖上营巢。

## 人类活动的影响

隼形目的鸟身姿矫健，在各大文明中都被视为神明顶礼膜拜，现在许多国家还把它们选为国鸟，象征国家精神。

人类很早就利用隼形目的鸟善于捕猎的特点来驯化它们为自己服务。隼形目威武的外形和神奇的捕猎能力一直为人们所鍾爱。现在世界各地隼形目鸟类的驯养依旧普遍，并且对其[标本的需求也很多](../Page/标本.md "wikilink")，以隼形目鸟进行的[鸟类贸易也难以禁绝](../Page/鸟类贸易.md "wikilink")。

隼形目鸟类的习性决定了它们在[食物链顶层的地位](../Page/食物链.md "wikilink")，是自然选择的重要力量，它们的存在除去了许多老、弱、病、残的个体，使整个生态得以平衡。长期的进化使它们具有许多成功的特性，对[微生物的抵抗能力就是其中之一](../Page/微生物.md "wikilink")，一生中它们会吃下许多带疫患的个体，但却能保证自己不受感染，很好的控制了疫病的传播。

就是这样进化上的强者，也受到了人类活动的威胁。上世纪农药的广泛使用就对这些食物链顶层的鸟造成了灭顶之灾，有毒物质在体内的快速积累（[生物放大作用](../Page/生物放大作用.md "wikilink")）对它们造成了严重的伤害。如[美国国鸟白头海雕就因](../Page/美国.md "wikilink")[DDT的毒害而无法产下正常的卵](../Page/DDT.md "wikilink")。

## 分类

  - [巨隼亚科](../Page/巨隼亚科.md "wikilink") Polyborinae
      - [黑巨隼属](../Page/黑巨隼属.md "wikilink") *Daptrius*
      - [卡拉鹰属](../Page/卡拉鹰属.md "wikilink") *Phalcoboenus*
      - [凤头卡拉鹰属](../Page/凤头卡拉鹰属.md "wikilink") *Polyborus*
      - [卡拉卡拉鹰属](../Page/卡拉卡拉鹰属.md "wikilink") *Caracara*
      - [叫隼属](../Page/叫隼属.md "wikilink") *Milvago*
      - [林隼属](../Page/林隼属.md "wikilink") *Micrastur*
  - [隼亚科](../Page/隼亚科.md "wikilink") Falconinae
      - [笑隼属](../Page/笑隼属.md "wikilink") *Herpetotheres*
      - [花隼属](../Page/花隼属.md "wikilink") *Spiziapteryx*
      - [侏隼属](../Page/侏隼属.md "wikilink") *Polihierax*
      - [小隼属](../Page/小隼属.md "wikilink") *Microhierax*
      - [隼属](../Page/隼属.md "wikilink") *Falco*

## 參考

  -
  -
  -
  -
  -
## 相关条目

  - [猛禽](../Page/猛禽.md "wikilink")

[\*](../Category/隼形目.md "wikilink") [\*](../Category/隼科.md "wikilink")