  -
    *同名城市请参见：[瑙姆堡 (黑森州)](../Page/瑙姆堡_\(黑森州\).md "wikilink")*

**瑙姆堡**（）是位于[德国](../Page/德国.md "wikilink")[萨克森-安哈尔特州](../Page/萨克森-安哈尔特州.md "wikilink")[萨勒河畔的一个镇](../Page/萨勒河.md "wikilink")，人口约3万（2006年）。瑙姆堡的產業包含食品製造，[紡織](../Page/紡織.md "wikilink")，[機械及](../Page/機械.md "wikilink")[玩具](../Page/玩具.md "wikilink")，瑙姆堡也位於葡萄酒的產區。

对瑙姆堡最早的文字记录可以追溯到1012年，1114年开始形成一个城镇，在[中世纪时成为一个重要的](../Page/中世纪.md "wikilink")[商业中心](../Page/商业.md "wikilink")，1278年开始出现瑙姆堡市集，但[三十年战争和](../Page/三十年战争.md "wikilink")1500年以后形成的[莱比锡市场](../Page/莱比锡.md "wikilink")，使瑙姆堡失去了重要的商业地位。[第二次世界大战后隶属于](../Page/第二次世界大战.md "wikilink")[东德](../Page/东德.md "wikilink")，当地集中了机械制造、制药和制鞋业，前[苏联的一个空军基地驻扎在这里](../Page/苏联.md "wikilink")。1989年两德合并后，这里新建了许多[教堂](../Page/教堂.md "wikilink")。

每年6月的再后一周，是[樱桃成熟的季节](../Page/樱桃.md "wikilink")，瑙姆堡有一个源于16世纪的胡斯樱桃节，

## 外部連結

  - [瑙姆堡旅遊資訊](https://web.archive.org/web/20090207140840/http://naumburg-tourismus.de/Naumburg2003/web/en/tourismus/tourismus.php)

  - [瑙姆堡博物館](http://www.museumnaumburg.de/)

[N](../Category/萨克森-安哈尔特州市镇.md "wikilink")