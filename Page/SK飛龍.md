**SK飛龍**（[韓文](../Page/韓文.md "wikilink")：****，[英文](../Page/英文.md "wikilink")：**SK
Wyverns**）為[韓國職棒的球團之一](../Page/韓國職棒.md "wikilink")，於2000年正式加盟韓國職棒，主場地設於[仁川廣域市的](../Page/仁川廣域市.md "wikilink")[文鶴棒球場](../Page/文鶴棒球場.md "wikilink")，累積至2018年共拿下4座年度總冠軍。

## 球隊簡介

'''SK飛龍 '''（ＳＫ 와이번스；SK
Wyverns）為[韓國職棒球團](../Page/韓國職棒.md "wikilink")，於2000年加入韓國職棒，主球場為仁川市的[文鶴棒球場](../Page/文鶴棒球場.md "wikilink")。

2003年首度打入季後賽，但在韓國大賽3：4輸給[現代獨角獸](../Page/現代獨角獸.md "wikilink")。

2007年以4勝2敗打贏[斗山熊隊奪得隊史第一座總冠軍金盃](../Page/斗山熊隊.md "wikilink")。

2008年再以4勝1敗打贏[斗山熊隊](../Page/斗山熊隊.md "wikilink")，衛冕成功。2008年的SK飛龍隊號稱為[韓國職棒史上最強的球隊](../Page/韓國職棒.md "wikilink")。

2010年以直落四擊敗[三星獅](../Page/三星獅.md "wikilink")，拿下隊史第三座總冠軍金盃。

2018年以4勝2敗擊敗[斗山熊](../Page/斗山熊.md "wikilink")，睽違8年拿下隊史第四座總冠軍金盃。

## 球隊簡史

  - 1999年成立，隊名ＳＫ飛龍隊。ＳＫ是以通信、石化為主的財團。
  - 2000年加入韓國職棒，頂替了甫解散的[雙鈴突擊者留下的空缺](../Page/雙鈴突擊者.md "wikilink")，並吸收了雙鈴的所有球員；但兩隊之間並不被視為有繼承關係，以兩支各自獨立的球隊來處理。

## 歷代教練

|                                        |       |       |    |    |
| -------------------------------------- | ----- | ----- | -- | -- |
| 教練名                                    | 接任年份  | 解任年份  | 冠軍 | 備註 |
| 姜秉哲                                    | 2000年 | 2002年 |    |    |
| 曹凡鉉                                    | 2003年 | 2006年 |    |    |
| [金星根](../Page/金星根.md "wikilink")       | 2007年 | 2011年 | 3次 |    |
| 李萬洙                                    | 2011年 | 2011年 |    | 代理 |
| 李萬洙                                    | 2012年 | 2014年 |    |    |
| 金用熙                                    | 2015年 | 2016年 |    |    |
| [特雷·希爾曼](../Page/特雷·希爾曼.md "wikilink") | 2017年 | 2018年 | 1次 |    |
| 廉京燁                                    | 2019年 |       |    |    |

<noinclude></noinclude>

## 球員名單

## 歷年成績

|       |    |     |    |    |   |       |       |
| ----- | -- | --- | -- | -- | - | ----- | ----- |
| 年度    | 名次 | 出賽  | 勝  | 敗  | 和 | 勝率    | 備註    |
| 2000年 | 4  | 133 | 44 | 86 | 3 | 0.338 | 魔法聯盟  |
| 2001年 | 7  | 133 | 60 | 71 | 2 | 0.458 |       |
| 2002年 | 6  | 133 | 61 | 69 | 3 | 0.469 |       |
| 2003年 | 4  | 133 | 66 | 64 | 3 | 0.508 |       |
| 2004年 | 5  | 133 | 61 | 64 | 8 | 0.488 |       |
| 2005年 | 3  | 126 | 70 | 50 | 6 | 0.583 |       |
| 2006年 | 6  | 126 | 60 | 65 | 1 | 0.480 |       |
| 2007年 | 1  | 126 | 73 | 48 | 5 | 0.603 | 年度總冠軍 |
| 2008年 | 1  | 126 | 83 | 43 | 0 | 0.659 | 年度總冠軍 |
| 2009年 | 2  | 133 | 80 | 47 | 0 | 0.602 |       |
| 2010年 | 1  | 133 | 84 | 47 | 0 | 0.632 | 年度總冠軍 |
| 2011年 | 3  | 133 | 71 | 59 | 3 | 0.546 |       |
| 2012年 | 2  | 133 | 71 | 59 | 3 | 0.546 |       |
| 2013年 | 6  | 128 | 62 | 63 | 3 | 0.496 |       |
| 2014年 | 5  | 128 | 61 | 65 | 2 | 0.484 |       |
| 2015年 | 5  | 144 | 69 | 73 | 2 | 0.486 |       |
| 2016年 | 6  | 144 | 69 | 75 | 0 | 0.479 |       |
| 2017年 | 5  | 144 | 75 | 68 | 1 | 0.524 |       |
| 2018年 | 2  | 144 | 78 | 65 | 1 | 0.545 | 年度總冠軍 |

## 外部連結

  - [SK飛龍官方網站](http://www.skwyverns.com/)
  - [SK飛龍官方臉書](https://www.facebook.com/SKwyverns)
  - [SK飛龍官方YouTube](https://www.youtube.com/user/Wyvernsstory)
  - [SK飛龍官方IG](https://www.instagram.com/skwyverns_official/)

[Category:紀錄模板](../Category/紀錄模板.md "wikilink")
[Category:韓國職棒球隊](../Category/韓國職棒球隊.md "wikilink")
[Category:SK集團](../Category/SK集團.md "wikilink")