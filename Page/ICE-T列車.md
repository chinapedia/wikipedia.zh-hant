**ICE-T**是德國國鐵411及415型鐵路列車的代號。此系列是德國
[城際列車特快](../Page/ICE列車.md "wikilink")（Inter City
Express）家族最新推出的列車。但與其他家族成員不同的是，它不是以速度為設計重點；它是唯一擁有[傾斜技術的ICE列車](../Page/擺式列車.md "wikilink")。

## 服務線路

ICE-T的主要服務區域包括了德國南面著名的黑森林、巴伐利亞森林及[阿爾卑斯山等山區多彎的路線](../Page/阿爾卑斯山.md "wikilink")。另外德國東面亦可發現ICE-T的足跡。

  - 東面：ICE-T東面路線由柏林為中心向四方伸展。西北方經[漢堡至](../Page/漢堡.md "wikilink")[基爾](../Page/基爾.md "wikilink")；東方經[萊比錫至](../Page/萊比錫.md "wikilink")[德累斯頓](../Page/德累斯頓.md "wikilink")。路線亦向南方經[萊比錫](../Page/萊比錫.md "wikilink")、[紐倫堡至](../Page/紐倫堡.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")。

<!-- end list -->

  - 南面：ICE-T線路由德國南面[巴登符騰堡省](../Page/巴登-符騰堡.md "wikilink")[海德堡開始經](../Page/海德堡.md "wikilink")[斯圖加特](../Page/斯圖加特.md "wikilink")、瑞士[蘇黎世至Chur一段](../Page/蘇黎世.md "wikilink")
    [阿爾卑斯山山區路線](../Page/阿爾卑斯山.md "wikilink")，瑞士段與瑞士國鐵ICN傾斜列車路線重疊。

<!-- end list -->

  - 西方：西方經[萊比錫](../Page/萊比錫.md "wikilink")、Fulda至[法蘭克福](../Page/法蘭克福.md "wikilink")，或季節性地經Kassel
    Welhelmshöhe至[魯爾區](../Page/魯爾區.md "wikilink")。另外亦會由法蘭克福經曼海姆至Saarbrücken。

<!-- end list -->

  - 北方：北方平原較少ICE-T，但亦有較少的班次由[漢諾威經](../Page/漢諾威.md "wikilink")[漢堡至](../Page/漢堡.md "wikilink")[基爾](../Page/基爾.md "wikilink")。

## 技術

[ICE-T2_Deutz.jpg](https://zh.wikipedia.org/wiki/File:ICE-T2_Deutz.jpg "fig:ICE-T2_Deutz.jpg")中央車站\]\]
ICE-T採用[合體列車設計](../Page/合體列車.md "wikilink")。但它不會行走比如[科隆至](../Page/科隆.md "wikilink")[法蘭克福或](../Page/法蘭克福.md "wikilink")[紐倫堡至](../Page/紐倫堡.md "wikilink")[英戈尔施塔特等高速路線](../Page/英戈尔施塔特.md "wikilink")。相反它只行走已鋪設了的路線，特別在地理上多彎、上坡斜度大的山林區路線，ICE-T便可以表現它獨有的
[傾斜技術](../Page/擺式列車.md "wikilink")。與其他家族成員一樣，它建了電力臂於車頂上，屬電力推動列車；但它亦有一個為未鋪設電纜地區設計成、採用柴油推動的「雙生兄弟」[ICE-TD](../Page/ICE-TD列車.md "wikilink")。
德國國鐵的原形設計是在普通InterCity列車上內[傾斜技術](../Page/擺式列車.md "wikilink")，並命名為IC-T。但是計劃最終因市場策略而與Siemens公司合作製成ICE家族列車，並以「T」命名，成為現在的ICE-T。

## 型號

[Db-411xxx-01.jpg](https://zh.wikipedia.org/wiki/File:Db-411xxx-01.jpg "fig:Db-411xxx-01.jpg")
在德國國鐵列車列表中，ICE-T系統共有三種型號：

  - 11列5節車廂的415型
  - 32列7節車廂的411型

<!-- end list -->

  - 28列在2004－2006年之間額外訂製、有7節車廂的441.5型列車

[Category:擺式列車](../Category/擺式列車.md "wikilink")
[Category:德國電力動車組](../Category/德國電力動車組.md "wikilink")
[Category:西門子製鐵路車輛](../Category/西門子製鐵路車輛.md "wikilink")
[Category:龐巴迪製鐵路車輛](../Category/龐巴迪製鐵路車輛.md "wikilink")
[Category:阿爾斯通製鐵路車輛](../Category/阿爾斯通製鐵路車輛.md "wikilink")
[Category:城际快车](../Category/城际快车.md "wikilink")
[Category:15千伏16.7赫兹交流电力动车组](../Category/15千伏16.7赫兹交流电力动车组.md "wikilink")