**简单邮件传输协议**（，缩写：**SMTP**）是一个在[互联网上传输](../Page/互联网.md "wikilink")[电子邮件的标准](../Page/电子邮件.md "wikilink")。

SMTP是一个相对简单的基于[文本的](../Page/文本.md "wikilink")[协议](../Page/网络传输协议.md "wikilink")。在其之上指定了一条[消息的一个或多个接收者](../Page/消息.md "wikilink")（在大多数情况下被确认是存在的），然后消息文本会被传输。可以很简单地通过[telnet程序来测试一个SMTP服务器](../Page/telnet.md "wikilink")。SMTP使用[TCP端口](../Page/传输控制协议.md "wikilink")25。要为一个给定的域名决定一个SMTP服务器，需要使用[DNS的](../Page/DNS.md "wikilink")[MX记录](../Page/MX记录.md "wikilink")。

在八十年代早期SMTP开始被广泛地使用。当时，它只是作为[UUCP的补充](../Page/UUCP.md "wikilink")，UUCP更适合于处理在间歇连接的机器间传送邮件。相反，SMTP在发送和接收的机器在持續連線的网络情况下工作得最好。

[Sendmail是最早使用SMTP的邮件传输代理之一](../Page/Sendmail.md "wikilink")。到2001年至少有50个程序将SMTP实现为一个客户端（消息的发送者）或一个服务器（消息的接收者）。一些其他的流行的SMTP服务器程序包括了Philip
Hazel的exim，[IBM的Postfix](../Page/IBM.md "wikilink")， [D. J.
Bernstein的](../Page/D._J._Bernstein.md "wikilink")[Qmail](../Page/Qmail.md "wikilink")，以及[Microsoft
Exchange Server](../Page/Microsoft_Exchange_Server.md "wikilink")。

由于这个协议开始是基于纯[ASCII文本的](../Page/ASCII.md "wikilink")，它在[二进制文件上处理得并不好](../Page/二进制.md "wikilink")。诸如[MIME的标准被开发来编码二进制文件以使其通过SMTP来传输](../Page/MIME.md "wikilink")。今天，大多数SMTP服务器都支持8位MIME扩展，它使二进制文件的传输变得几乎和纯文本一样简单。

SMTP是一个“推”的协议，它不允许根据需要从远程服务器上“拉”来消息。要做到这点，邮件客户端必须使用[POP3或](../Page/郵局協定.md "wikilink")[IMAP](../Page/IMAP.md "wikilink")。另一个SMTP服务器可以使用ETRN在SMTP上触发一个发送。

## SMTP通信举例

在发送方（客户端）和接收方（服务器）间建立连接之后，接下来是一个合法的SMTP会话。在下面的对话中，所有客户端发送的都以“C:”作为前缀，所有服务器发送的都以“S:”作为前缀。在多数计算机系统上，可以在发送的机器上使用telnet命令来建立连接，比如：

    telnet www.example.com 25

它打开一个从发送的机器到主机www.example.com的SMTP连接。

    S: 220 www.example.com ESMTP Postfix
    C: HELO mydomain.com
    S: 250 Hello mydomain.com
    C: MAIL FROM: <sender@mydomain.com>
    S: 250 Ok
    C: RCPT TO: <friend@example.com>
    S: 250 Ok
    C: DATA
    S: 354 End data with <CR><LF>.<CR><LF>
    C: Subject: test message
    C: From:""< sender@mydomain.com>
    C: To:""< friend@example.com>
    C:
    C: Hello,
    C: This is a test.
    C: Goodbye.
    C: .
    S: 250 Ok: queued as 12345
    C: quit
    S: 221 Bye

虽然是可选的，但几乎所有的客户端都会使用HELO问候消息（而不是上面所示的HELO）来询问服务器支持何种SMTP扩展，邮件的文本体（接着DATA）一般是典型的MIME格式。

## SMTP安全和垃圾邮件

最初的SMTP的局限之一在于它没有对发送方进行[身份验证的机制](../Page/身份验证.md "wikilink")。因此，后来定义了扩展。

尽管有了身份认证机制，垃圾邮件仍然是一个主要的问题。但由于庞大的SMTP安装数量带来的[网络效应](../Page/网络效应.md "wikilink")，大刀阔斧地修改或完全替代SMTP被认为是不现实的。就是一个替代SMTP的建议方案。

因此，出现了一些同SMTP工作的辅助协议。的反垃圾邮件研究小组正在研究一些建议方案，以提供简单、灵活、轻量级的、可升级的源端认证。最有可能被接受的建议方案是[发件人策略框架协议](../Page/发件人策略框架.md "wikilink")。

## 参见

  -
### 相关 RFC

  - RFC 5321 - 简单邮件传输协议，在最近（2008.8）代替了 RFC 2821
  - RFC 2821 - 简单邮件传输协议，在最近（2001）代替了 RFC 821，RFC 1869，RFC 974
  - RFC 2822 - Internet（比如 e-mail）消息格式，代替了 RFC 822
  - RFC 3461 - SMTP的发送状态通知（DSN）扩展，代替了 RFC 1891

## 参考文献

  - [SMTP 协议资料](http://www.cnpaf.net/class/smtp)

[Category:电子邮件协议](../Category/电子邮件协议.md "wikilink")