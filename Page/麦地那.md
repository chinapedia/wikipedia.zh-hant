**麦地那**（，意謂「光芒四射的城市」）原名**雅特里布**，伊斯蘭教第二大聖城，後又稱聖城（ *Madinat
al-Nabi*），該城位于[沙烏地阿拉伯西部](../Page/沙烏地阿拉伯.md "wikilink")，為內陸高原城市。与[麦加](../Page/麦加.md "wikilink")，[耶路撒冷一起被称为](../Page/耶路撒冷.md "wikilink")[伊斯兰教三大圣地](../Page/伊斯兰教.md "wikilink")。

非穆斯林被沙特中央政府禁止進入麥地那的神聖核心及市中心。

## 地理

位於沙烏地阿拉伯王國[漢志](../Page/漢志.md "wikilink")，即北緯24.28°、東經39.36°，四面環山，海拔約六百至八百公尺，屬於山區高原城市，西距[紅海約一百六十公里](../Page/紅海.md "wikilink")，南距[麥加約四百公里](../Page/麥加.md "wikilink")。

## 社會經濟

  - 該地區的財經税收多仰賴每年伊斯蘭教教徒朝聖人潮與觀光事業。
  - 著名的麥地那綠洲地下泉水之蓄積極為豐富，其產物除[椰棗之外](../Page/椰棗.md "wikilink")，尚有水果、蔬菜及穀物等。
  - 麥地那是宗教的作為城市發展之中心。

## 重要地標

  - 最著名的有[穆罕默德親自督建的](../Page/穆罕默德.md "wikilink")[先知清真寺](../Page/先知清真寺.md "wikilink")，寺內有穆罕默德等陵墓，為伊斯蘭世界的第二大聖寺，是信徒主要的朝拜地点之一。
  - 阿拉伯帝國的[阿布·伯克爾與](../Page/阿布·伯克爾.md "wikilink")[歐麥爾·本·赫塔卜前兩位](../Page/歐麥爾·本·赫塔卜.md "wikilink")[哈里發之墓地亦在此](../Page/哈里發.md "wikilink")。\[1\]
  - 附近還有穆罕默德於622年9月遷徙麥地那途中修建的[庫巴清真寺](../Page/庫巴清真寺.md "wikilink")、[雙朝向清真寺](../Page/雙朝向清真寺.md "wikilink")。
  - 聞名的[伍侯德戰役陣亡烈士陵園](../Page/武侯德山.md "wikilink")。\[2\]

## 歷史

[المدينة_المنورة.PNG](https://zh.wikipedia.org/wiki/File:المدينة_المنورة.PNG "fig:المدينة_المنورة.PNG")

  - 早在公元前，猶太人已定居在麥地那綠洲，而其古名稱為雅特里布（或亞斯利普）是一個重要的交通樞紐。到公元[6世紀](../Page/6世紀.md "wikilink")，麥地那仍然是阿、猶混居之狀態。
  - 公元622年，因穆罕默德在麥加傳教受威脅與迫害，而逃來此地，並改名為「麥地那——納比」，意為「先知之城」，簡稱「麥地那」，
    麥地那成為繼[希吉拉](../Page/希吉拉.md "wikilink")（即出走麦加）之后穆罕默德活動和壯大的場所，在此屢次擊敗了來自[麥加城的阿拉伯古萊氏族人之攻擊](../Page/麥加.md "wikilink")，並成立最早的伊斯蘭教政權，所以成為[大食帝國第一個首都](../Page/大食.md "wikilink")。到661年，始遷至[大馬士革](../Page/大馬士革.md "wikilink")。
  - 1919至1924年，麥地那為[漢志王國所統治](../Page/漢志王國.md "wikilink")。
  - 1924年，麥地那被[伊本·沙特所佔領](../Page/阿卜杜勒-阿齐兹·本·阿卜杜-拉赫曼·本·费萨尔·阿勒沙特.md "wikilink")，他后來成為[沙特阿拉伯的第一位國王](../Page/沙特阿拉伯.md "wikilink")。

## 参考文献

## 外部链接

  -
  - [維客旅行上的](../Page/維客旅行.md "wikilink")[麦地那](http://wikitravel.org/en/Medina)

  - \[//maps.google.com/maps?q=麦地那 谷歌地圖\]

[hu:Medina](../Page/hu:Medina.md "wikilink")

[Category:沙特阿拉伯城市](../Category/沙特阿拉伯城市.md "wikilink")
[Category:伊斯蘭教聖地](../Category/伊斯蘭教聖地.md "wikilink")
[麦地那](../Category/麦地那.md "wikilink")

1.  [中華百科全書典藏版-麥地那簡介](http://ap6.pccu.edu.tw/Encyclopedia/data.asp?id=9801)
2.  [伊斯蘭之光
    麥地那簡介](http://www.islam.org.hk/index.php?action-viewnews-itemid-4857)