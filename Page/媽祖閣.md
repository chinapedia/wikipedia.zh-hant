[A-Ma_Temple_(1386860279).jpg](https://zh.wikipedia.org/wiki/File:A-Ma_Temple_\(1386860279\).jpg "fig:A-Ma_Temple_(1386860279).jpg")
 **媽祖閣**（或），俗稱**媽閣廟**（粵語拼音：*ma5 gok3
miu6*，「媽」變調為「馬」），是位於[澳門半島西南方的](../Page/澳門半島.md "wikilink")[媽祖廟](../Page/媽祖廟.md "wikilink")，為[澳門標誌性](../Page/澳門.md "wikilink")[建築物之一](../Page/建築物.md "wikilink")，現為澳門三大古剎中最古老者，澳門的葡萄牙語名稱「」即來自於廟名「媽閣」的音譯\[1\]。2005年做為[澳門歷史城區的部分被列入](../Page/澳門歷史城區.md "wikilink")[世界文化遺產名錄內](../Page/世界文化遺產.md "wikilink")。

媽閣廟始建於1488年，時值[明朝](../Page/明朝.md "wikilink")[弘治](../Page/弘治.md "wikilink")[元年](../Page/元年.md "wikilink")。當時媽閣廟門前已為澳門之海岸線，是當地及附近居住之漁民作業之上岸補給、歇息和祈福之處。廟宇背山面海，沿崖建築，古木參天，風光優美。整座廟宇包括大殿（又稱正覺禪林）、石殿（又稱神山第一殿）、弘仁殿、觀音閣等4座主要建築。石獅鎮門、飛檐凌空，是一座富有[中國文化特色的古建築](../Page/中國文化.md "wikilink")。

經過廟門及花崗石牌坊，便是庭院，循着山麓的石階小徑，拾級而上，即可抵達建於巉岩巨石間、就石窟鑿成的弘仁殿。殿内四壁，雕刻着海魔神將，色彩斑斕，中央供奉[天后](../Page/媽祖.md "wikilink")。自弘仁殿至觀音閣，沿着山崖有不少石刻，或為名流政要詠題，或為騷人墨客遣興，楷草篆隸，諸體俱備。觀音閣位於廟之最高處，供奉。不少西洋畫家亦曾描繪了廟前繁華景象，而媽閣廟亦出現在最早一批在中國拍攝的照片。

## 傳說

[A_Ma_Temple_200907.jpg](https://zh.wikipedia.org/wiki/File:A_Ma_Temple_200907.jpg "fig:A_Ma_Temple_200907.jpg")\]\]

相傳媽祖乃[福建](../Page/福建.md "wikilink")[莆田女子](../Page/莆田.md "wikilink")，又稱**娘媽**，本姓[林](../Page/林姓.md "wikilink")，生時能[預言吉凶](../Page/預言.md "wikilink")，[物化登仙後常顯靈海上](../Page/物化.md "wikilink")，幫助[海難](../Page/海難.md "wikilink")[商人及](../Page/商人.md "wikilink")[漁民消災解難](../Page/漁民.md "wikilink")，化險為夷，[閩南人船員為了能在靠岸時祭祀](../Page/閩南人.md "wikilink")，遂與當地居民商議，在現址立廟。而庭院内有中國帆船石刻浮雕，傳說娘媽曾乘此船自家鄉出海，經歷[颱風巨浪](../Page/颱風.md "wikilink")，平安抵澳。

## 注釋

媽閣廟的正確發音應為「馬」閣廟，並不是「媽」閣廟，以往的孫中山口音的澳門人都是這樣叫的。由於字面上的確運用了「媽」字，而且受到了部份香港「[正音](../Page/粵語正音運動.md "wikilink")」影響，現在很多人都正讀成「媽」閣廟。

## 景觀危機

[SeeingfromAMaTemple.JPG](https://zh.wikipedia.org/wiki/File:SeeingfromAMaTemple.JPG "fig:SeeingfromAMaTemple.JPG")[神功戲戲棚](../Page/神功戲.md "wikilink")\]\]

由於媽閣廟正門對開為西環海面，沿山而上能眺望對岸（珠海[灣仔](../Page/灣仔鎮.md "wikilink")）景色，近三十年開始雖有政府船塢、[澳門海事博物館](../Page/澳門海事博物館.md "wikilink")、填海與環島公路建築，但因只有一至兩層，對正門前至山上風景只有些微影響。

2006年6月，澳門立法議員[高天賜指澳門特區政府已秘密地與](../Page/高天賜.md "wikilink")[葡萄牙駐港澳總領事館等達成共識](../Page/葡萄牙駐港澳總領事館.md "wikilink")，同意在媽閣廟對面建新的[葡文學校教學大樓](../Page/澳門葡文學校.md "wikilink")，因涉及私人利益而置古跡及附近環境的景觀于不顧，是為繼5月的[藍屋仔事件後又一次引起澳門社會對文物保護的關注](../Page/藍屋仔.md "wikilink")。另根據傳媒可靠消息，媽閣廟對開海面還將繼續向[灣仔方向填海](../Page/灣仔鎮.md "wikilink")，作為未來海底隧道或橋樑落腳點，另外還會在此興建一幢五星級大酒店。

2006年6月14日，立法議員[區錦新在議程前發言中指出](../Page/區錦新.md "wikilink")[澳葡政府時的](../Page/澳葡政府.md "wikilink")[葡國人興建澳門海事博物館也尊重中國傳統](../Page/葡國.md "wikilink")，把博物館放在媽閣廟之左側，不擋媽閣廟向海的朝向。但到澳門特區政府，卻對傳統漫不經心，要把葡文學校教學大樓建在媽閣廟前，擋去媽閣廟之朝向。6月22日，澳博行政總裁[何鴻燊在](../Page/何鴻燊.md "wikilink")[里斯本表示不能接受以風水作理由來阻止葡文學校在該地段建新校舍](../Page/里斯本.md "wikilink")，還指立法議員的工作本是立法，而非看風水。6月23日，正在葡國訪問的行政長官[何厚鏵公開回應學校的選址與設計原則不會影響交通及整個文化以至文物保護政策](../Page/何厚鏵.md "wikilink")。

### 紙幣取材

媽閣廟正門被發鈔銀行多次用作澳門幣紙幣正面的圖案，包括：

  - [澳門大西洋銀行](../Page/澳門大西洋銀行.md "wikilink")
      - 1945年（壹圓、伍圓、拾圓、貳拾伍圓、伍拾圓；壹佰圓、伍佰圓）
      - 1990年與1999年（伍佰圓）

另1981年大西洋銀行發行的伍圓紙幣為媽閣廟側門正面照。

  - [澳門中國銀行](../Page/中國銀行澳門分行.md "wikilink")
      - 1999年與2003年（貳拾圓）
      - 2008年 （拾圓）

## 外部連結

<references />

  - [區錦新議程前發言](https://web.archive.org/web/20080220044443/http://newmacau.org/mainbody.php?inner=whatnew%2Fa20060614)（2006年6月14日）
  - [葡文學校選址破壞媽閣廟景觀有違申遺精神](https://web.archive.org/web/20070928041715/http://www.waou.com.mo/detail.asp?id=13811)新華澳報　華澳人語（2006年6月14日）

[Category:澳門媽祖廟](../Category/澳門媽祖廟.md "wikilink")
[Category:澳門歷史城區](../Category/澳門歷史城區.md "wikilink")
[Category:澳門旅遊景點](../Category/澳門旅遊景點.md "wikilink")

1.  [澳門特別行政區政府旅遊局 \> 認識澳門 \>
    歷史簡介](http://zh.macaotourism.gov.mo/plan/aboutmacao_detail.php)