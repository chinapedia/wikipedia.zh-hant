[碑林区](../Page/碑林区.md "wikilink"){{.w}}[莲湖区](../Page/莲湖区.md "wikilink"){{.w}}[灞桥区](../Page/灞桥区.md "wikilink"){{.w}}[雁塔区](../Page/雁塔区.md "wikilink"){{.w}}[阎良区](../Page/阎良区.md "wikilink"){{.w}}[临潼区](../Page/临潼区.md "wikilink"){{.w}}[长安区](../Page/长安区_\(西安市\).md "wikilink"){{.w}}[高陵区](../Page/高陵区.md "wikilink"){{.w}}[鄠邑区](../Page/鄠邑区.md "wikilink"){{.w}}[蓝田县](../Page/蓝田县.md "wikilink"){{.w}}[周至县](../Page/周至县.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[王益区](../Page/王益区.md "wikilink"){{.w}}[印台区](../Page/印台区.md "wikilink"){{.w}}[宜君县](../Page/宜君县.md "wikilink")

|group4 = [宝鸡市](../Page/宝鸡市.md "wikilink") |list4 =
[金台区](../Page/金台区.md "wikilink"){{.w}}[渭滨区](../Page/渭滨区.md "wikilink"){{.w}}[陈仓区](../Page/陈仓区.md "wikilink"){{.w}}[凤翔县](../Page/凤翔县.md "wikilink"){{.w}}[岐山县](../Page/岐山县.md "wikilink"){{.w}}[扶风县](../Page/扶风县.md "wikilink"){{.w}}[眉县](../Page/眉县.md "wikilink"){{.w}}[陇县](../Page/陇县.md "wikilink"){{.w}}[千阳县](../Page/千阳县.md "wikilink"){{.w}}[麟游县](../Page/麟游县.md "wikilink"){{.w}}[凤县](../Page/凤县.md "wikilink"){{.w}}[太白县](../Page/太白县.md "wikilink")

|group5 = [咸阳市](../Page/咸阳市.md "wikilink") |list5 =
[秦都区](../Page/秦都区.md "wikilink"){{.w}}[杨陵区](../Page/杨陵区.md "wikilink"){{.w}}[渭城区](../Page/渭城区.md "wikilink"){{.w}}[兴平市](../Page/兴平市.md "wikilink"){{.w}}[彬州市](../Page/彬州市.md "wikilink"){{.w}}[三原县](../Page/三原县.md "wikilink"){{.w}}[泾阳县](../Page/泾阳县.md "wikilink"){{.w}}[乾县](../Page/乾县.md "wikilink"){{.w}}[礼泉县](../Page/礼泉县.md "wikilink"){{.w}}[永寿县](../Page/永寿县.md "wikilink"){{.w}}[长武县](../Page/长武县.md "wikilink"){{.w}}[旬邑县](../Page/旬邑县.md "wikilink"){{.w}}[淳化县](../Page/淳化县.md "wikilink"){{.w}}[武功县](../Page/武功县.md "wikilink")

|group6 = [渭南市](../Page/渭南市.md "wikilink") |list6 =
[临渭区](../Page/临渭区.md "wikilink"){{.w}}[华州区](../Page/华州区.md "wikilink"){{.w}}[韩城市](../Page/韩城市.md "wikilink"){{.w}}[华阴市](../Page/华阴市.md "wikilink"){{.w}}[潼关县](../Page/潼关县.md "wikilink"){{.w}}[大荔县](../Page/大荔县.md "wikilink"){{.w}}[合阳县](../Page/合阳县.md "wikilink"){{.w}}[澄城县](../Page/澄城县.md "wikilink"){{.w}}[蒲城县](../Page/蒲城县.md "wikilink"){{.w}}[白水县](../Page/白水县.md "wikilink"){{.w}}[富平县](../Page/富平县.md "wikilink")

|group7= [延安市](../Page/延安市.md "wikilink") |list7 =
[宝塔区](../Page/宝塔区.md "wikilink"){{.w}}[安塞区](../Page/安塞区.md "wikilink"){{.w}}[延长县](../Page/延长县.md "wikilink"){{.w}}[延川县](../Page/延川县.md "wikilink"){{.w}}[子长县](../Page/子长县.md "wikilink"){{.w}}[志丹县](../Page/志丹县.md "wikilink"){{.w}}[吴起县](../Page/吴起县.md "wikilink"){{.w}}[甘泉县](../Page/甘泉县.md "wikilink"){{.w}}[富县](../Page/富县.md "wikilink"){{.w}}[洛川县](../Page/洛川县.md "wikilink"){{.w}}[宜川县](../Page/宜川县.md "wikilink"){{.w}}[黄龙县](../Page/黄龙县.md "wikilink"){{.w}}[黄陵县](../Page/黄陵县.md "wikilink")

|group8 = [汉中市](../Page/汉中市.md "wikilink") |list8 =
[汉台区](../Page/汉台区.md "wikilink"){{.w}}[南郑区](../Page/南郑区.md "wikilink"){{.w}}[城固县](../Page/城固县.md "wikilink"){{.w}}[洋县](../Page/洋县.md "wikilink"){{.w}}[西乡县](../Page/西乡县.md "wikilink"){{.w}}[勉县](../Page/勉县.md "wikilink"){{.w}}[宁强县](../Page/宁强县.md "wikilink"){{.w}}[略阳县](../Page/略阳县.md "wikilink"){{.w}}[镇巴县](../Page/镇巴县.md "wikilink"){{.w}}[留坝县](../Page/留坝县.md "wikilink"){{.w}}[佛坪县](../Page/佛坪县.md "wikilink")

|group9 = [榆林市](../Page/榆林市.md "wikilink") |list9 =
[榆阳区](../Page/榆阳区.md "wikilink"){{.w}}[横山区](../Page/横山区.md "wikilink"){{.w}}[神木市](../Page/神木市.md "wikilink"){{.w}}[府谷县](../Page/府谷县.md "wikilink"){{.w}}[靖边县](../Page/靖边县.md "wikilink"){{.w}}[定边县](../Page/定边县.md "wikilink"){{.w}}[绥德县](../Page/绥德县.md "wikilink"){{.w}}[米脂县](../Page/米脂县.md "wikilink"){{.w}}[佳县](../Page/佳县.md "wikilink"){{.w}}[吴堡县](../Page/吴堡县.md "wikilink"){{.w}}[清涧县](../Page/清涧县.md "wikilink"){{.w}}[子洲县](../Page/子洲县.md "wikilink")

|group10 = [安康市](../Page/安康市.md "wikilink") |list10 =
[汉滨区](../Page/汉滨区.md "wikilink"){{.w}}[汉阴县](../Page/汉阴县.md "wikilink"){{.w}}[石泉县](../Page/石泉县.md "wikilink"){{.w}}[宁陕县](../Page/宁陕县.md "wikilink"){{.w}}[紫阳县](../Page/紫阳县.md "wikilink"){{.w}}[岚皋县](../Page/岚皋县.md "wikilink"){{.w}}[平利县](../Page/平利县.md "wikilink"){{.w}}[镇坪县](../Page/镇坪县.md "wikilink"){{.w}}[旬阳县](../Page/旬阳县.md "wikilink"){{.w}}[白河县](../Page/白河县.md "wikilink")

|group11 = [商洛市](../Page/商洛市.md "wikilink") |list11 =
[商州区](../Page/商州区.md "wikilink"){{.w}}[洛南县](../Page/洛南县.md "wikilink"){{.w}}[丹凤县](../Page/丹凤县.md "wikilink"){{.w}}[商南县](../Page/商南县.md "wikilink"){{.w}}[山阳县](../Page/山阳县.md "wikilink"){{.w}}[镇安县](../Page/镇安县.md "wikilink"){{.w}}[柞水县](../Page/柞水县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below=
注1：[西安市为](../Page/西安市.md "wikilink")[副省级城市](../Page/副省级城市.md "wikilink")。
注2：[杨陵区实际上由正厅级陕西省](../Page/杨陵区.md "wikilink")[杨凌农业高新技术产业示范区管辖](../Page/杨凌农业高新技术产业示范区.md "wikilink")，而非由[咸阳市管辖](../Page/咸阳市.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[陕西省乡级以上行政区列表](../Page/陕西省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/陕西行政区划.md "wikilink")
[陕西行政区划模板](../Category/陕西行政区划模板.md "wikilink")