**劉忠生**（），[中國國家射擊隊成員](../Page/中華人民共和國.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[山東](../Page/山東.md "wikilink")[萊蕪](../Page/萊蕪.md "wikilink")。

## 生涯

劉忠生在1990年至1998年這數年間於山東省技術體育學校學習射擊，教練是劉明軍。1999年，劉轉至[廣東的](../Page/廣東.md "wikilink")[黃川訓練基地](../Page/黃川.md "wikilink")，教練為施龍水。2005年11月，他入選國家隊，成為盛浩明教練的運動員。

同年，他於[十運會中以](../Page/十運會.md "wikilink")779.2環為廣東隊取得男子個人25米手槍小項的[銅牌](../Page/銅牌.md "wikilink")。入選國家隊後的一年，劉忠生先後在[德國](../Page/德國.md "wikilink")[慕尼克站與](../Page/慕尼克.md "wikilink")[意大利](../Page/意大利.md "wikilink")[米蘭站世界盃的男子個人](../Page/米蘭.md "wikilink")25手槍小項得一面[銅牌](../Page/銅牌.md "wikilink")。回國後，他於第二站的射擊系統賽中以788.3環刷新了男子個人25米手槍小項的[世界紀錄](../Page/世界紀錄.md "wikilink")，並贏得該賽事的[冠軍](../Page/冠軍.md "wikilink")\[1\]。而在第四站中，他因0.8環之差被新晉臨手[丁峰擊敗](../Page/丁峰.md "wikilink")，獲得第二。

隨後的[克羅地亞世界杯中](../Page/克羅地亞.md "wikilink")，他與[張鵬輝參與男子個人](../Page/張鵬輝.md "wikilink")25米手槍小項，並取得了一面[銀牌](../Page/銀牌.md "wikilink")。同時，他與[劉國輝及張鵬輝以](../Page/劉國輝.md "wikilink")1143環奪得了男子團體25米手槍的[金牌](../Page/金牌.md "wikilink")，創出了新的世界紀錄\[2\]。同一年的12月，劉在[多哈亞運中](../Page/2006年亞洲運動會.md "wikilink")，同日先後奪得了男子團體25米手槍小項和男子個人25米手槍小項的金牌，是中國射擊隊於當屆亞運中的第23面和24面金牌。翌年4月[美國站世界杯中](../Page/美國.md "wikilink")，劉由於失手而無法晉身男子個人25米手槍小項的決賽。

## 資料來源

[L](../Category/中國射擊運動員.md "wikilink")
[L](../Category/中国奥运射击运动员.md "wikilink")
[L](../Category/莱芜人.md "wikilink") [Z忠生](../Category/刘姓.md "wikilink")
[L](../Category/2008年夏季奧林匹克運動會射擊運動員.md "wikilink")
[L](../Category/2006年亞洲運動會射擊運動員.md "wikilink")
[L](../Category/2006年亚洲运动会金牌得主.md "wikilink")
[L](../Category/2006年亚洲运动会铜牌得主.md "wikilink")
[L](../Category/亞洲運動會射擊獎牌得主.md "wikilink")

1.  [全國射擊系列賽第二站暨團體錦標賽手槍賽結束](http://www.jssports.gov.cn/tbsy/jstydt/2006-06-22/2158.htm)
2.  [射擊世錦賽
    中國男隊打破手槍速射團體世界紀錄](http://sports.people.com.cn/BIG5/22155/22161/4652054.html)