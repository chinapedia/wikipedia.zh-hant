**汉斯·莫拉维克**（**Hans Moravec**）
[卡内基梅隆大学移动](../Page/卡内基梅隆大学.md "wikilink")[机器人实验室主任](../Page/机器人.md "wikilink")。著作有《智力后裔：机器人和人类智能的未来》、《机器人：通向非凡思维的纯粹机器》（ISBN
0195136306）。

## 参见

  - [莫拉维克悖论](../Page/莫拉维克悖论.md "wikilink")

## 參考資料

## 外部链接

  - [汉斯·莫拉维克主页](http://www.frc.ri.cmu.edu/~hpm/)

[Category:美国机械学家](../Category/美国机械学家.md "wikilink")