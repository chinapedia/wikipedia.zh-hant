|                                                                                                               |                                                                                                                |
| :-----------------------------------------------------------------------------------------------------------: | -------------------------------------------------------------------------------------------------------------- |
| [Flag_of_England.svg](https://zh.wikipedia.org/wiki/File:Flag_of_England.svg "fig:Flag_of_England.svg")\]\] | [Flag_of_Wales_2.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Wales_2.svg "fig:Flag_of_Wales_2.svg")\]\] |

[UKEngland_and_Wales.png](https://zh.wikipedia.org/wiki/File:UKEngland_and_Wales.png "fig:UKEngland_and_Wales.png")、[威爾斯](../Page/威爾斯.md "wikilink")（<span style="color: Red;">紅色</span>）和[聯合王國的其餘部分](../Page/聯合王國.md "wikilink")（<span style="color: Pink;">粉紅色</span>）\]\]
**英格蘭和威爾斯**是[英國的一個政治和法律用語](../Page/英國.md "wikilink")。它包含[英格蘭和](../Page/英格蘭.md "wikilink")[威爾斯](../Page/威爾斯.md "wikilink")，英國四個[構成國家中的兩個](../Page/構成國家.md "wikilink")。不同于[蘇格蘭和](../Page/蘇格蘭.md "wikilink")[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")，威爾士採用[英格蘭法律](../Page/英格蘭法律.md "wikilink")，和英格蘭一起是[英格蘭王國的憲制繼承者](../Page/英格蘭王國.md "wikilink")，因此在[國際私法中英格蘭和威爾士被視為同一實體](../Page/國際私法.md "wikilink")。在聯合王國的演化過程中，威爾士被視為一個公國（或親王國，Principality）[威爾斯公國](../Page/威爾斯公國.md "wikilink")，[威爾士親王為其元首](../Page/威爾士親王.md "wikilink")，而非一個被合併的國家，儘管威爾士有異于英格蘭的種族和文化。1999年，[威爾斯國民議會根據](../Page/威爾斯國民議會.md "wikilink")《[1998年威爾斯地方政府法案](../Page/1998年威爾斯地方政府法案.md "wikilink")》，由[英國國會](../Page/英國國會.md "wikilink")[下放權力成立](../Page/權力下放.md "wikilink")，使威爾士開始了一定程度的[自治](../Page/自治.md "wikilink")，包括更改[國會所訂](../Page/國會.md "wikilink")[英格蘭法律的權力](../Page/英格蘭法律.md "wikilink")。這些權力在《[2006年威爾斯地方政府法案](../Page/2006年威爾斯地方政府法案.md "wikilink")》通過後獲進一步擴展，現在[威爾斯議會政府已經可以就區內事務自行立法](../Page/威爾斯議會政府.md "wikilink")。

英格蘭和威爾斯是首先在[古罗马時期被合併管治](../Page/罗马统治下的不列颠.md "wikilink")，當時是作為[羅馬帝國的一個](../Page/羅馬帝國.md "wikilink")[省](../Page/省.md "wikilink")。[威爾斯法律是起初由當時統治威爾士大部分地區的國王](../Page/威爾斯法律.md "wikilink")[豪厄尔达](../Page/豪厄尔达.md "wikilink")（Hywel
Dda，942年－950年在位）轉錄為成文法，然而1284年的《[罗德兰法令](../Page/罗德兰法令.md "wikilink")》將威爾士的刑事法律以英格蘭法律取代。[民事法律則繼續使用威爾士本身的法律](../Page/民法.md "wikilink")，直至16世紀併入英格蘭為止。

## 法律

作為英格蘭王國的共同憲制繼承者，英格蘭和威爾士在絕大多數的事務上都是被視為一體的。[蘇格蘭法律的存續在](../Page/蘇格蘭法律.md "wikilink")《[1707年聯合法案](../Page/1707年聯合法案.md "wikilink")》中獲得了保証，所以和英格蘭法律（及1801年之後的愛爾蘭法律）依舊分開。除了1967年和1993年的《威爾斯語法案》、《1998年威爾斯地方政府法》和2006年以後威爾士議會通過的法案不在英格蘭執行外，威爾士和英格蘭是使用同一個[法律系統](../Page/法律體系.md "wikilink")。

英格蘭征服威爾士並通過1284年的《罗德兰法令》後，威爾士和英格蘭成為了一個[共主聯邦](../Page/共主聯邦.md "wikilink")，又在《[1535-1542年威爾士法律法案](../Page/1535年-1542年威尔士法案.md "wikilink")》通過後在法律體系上與英格蘭合併，但成文法中的「英格蘭」仍然不包括威爾士。《1746年威爾士和貝維克法案》通過後，規定所有將來的法律中，「英格蘭」預設包括威爾士，直到1967年法案廢除。現今法律皆使用「英格蘭和威爾斯」，以明文規定適用的法律實體。

聯合王國的其他構成國家（蘇格蘭和北愛爾蘭）及各[皇家屬地在國際私法上都被視為單獨的實體](../Page/皇家屬地.md "wikilink")，但是在[國際法上還是被視為統一的英國的一分子](../Page/國際法.md "wikilink")。

### 公司註冊

在公司註冊的法定程序上，英格蘭和威爾士被視為單一實體，使用著同一份公司註冊記錄（載列的公司都「在英格蘭和威爾士註冊」），與蘇格蘭和北愛爾蘭分開。然而在威爾士則可以[威爾斯語名字註冊公司](../Page/威爾斯語.md "wikilink")。

## 其他團體

法律以外的其他團體的取態不一。某些團體以「英格蘭和威爾士」的地域組成，某些則否。

體育方面，[板球由](../Page/板球.md "wikilink")[英格蘭和威爾斯板球理事會組成](../Page/英格蘭和威爾斯板球理事會.md "wikilink")[英格兰板球队出賽](../Page/英格兰板球队.md "wikilink")，而[足球](../Page/足球.md "wikilink")、[橄欖球和其他體育活動一般都實行分隊出賽](../Page/橄欖球.md "wikilink")。

一些宗教團體以「英格蘭和威爾士」區域作為教區，例如[羅馬天主教會](../Page/羅馬天主教會.md "wikilink")。1920年[威爾斯聖公會成立以前](../Page/威爾斯聖公會.md "wikilink")，[英國聖公會統一管理英格蘭和威爾士的教區](../Page/英國聖公會.md "wikilink")。

[選舉委員會保持著一份按活動地區排列的政黨名單](../Page/選舉委員會_\(英國\).md "wikilink")。直至2007年3月，共有8個團體在「英格蘭和威爾士」活動，相對于只在英格蘭活動的197個和只在威爾士活動的11個，當中最大的是[英格蘭和威爾斯綠黨](../Page/英格蘭和威爾斯綠黨.md "wikilink")。

英格蘭和威爾士有自己的[排名表](../Page/排名表.md "wikilink")（詳見[英格蘭和威爾斯排名表](../Page/英格蘭和威爾斯排名表.md "wikilink")），和北愛爾蘭、蘇格蘭及其他[英聯邦王國的有所不同](../Page/英聯邦王國.md "wikilink")。

[英格蘭和威爾斯的國家公園有自己特色的法律框架和歷史](../Page/英格蘭和威爾斯的國家公園.md "wikilink")。

## 地理

如果把英格蘭和威爾斯當作英國的一個分區，該區將有人口53,390,300人，佔整個聯合王國的89%\[1\]。總面積則有151,174平方公里。

1955年[加的夫被確立為威爾士的首府](../Page/加的夫.md "wikilink")\[2\]。

## 参考文献

## 參閱

  - [英格蘭及威爾斯的法院](../Page/英格蘭及威爾斯的法院.md "wikilink")

[Category:英國法律](../Category/英國法律.md "wikilink")
[Category:英國政治](../Category/英國政治.md "wikilink")
[Category:威爾斯](../Category/威爾斯.md "wikilink")
[Category:英格蘭](../Category/英格蘭.md "wikilink")
[Category:英格兰和威尔士](../Category/英格兰和威尔士.md "wikilink")

1.  <http://www.statistics.gov.uk/CCI/nugget.asp?ID=6> Official mid-2005
    population estimate; England=50,431,700 Wales=2,958,600
    UK=60,209,500
2.  "Cardiff as Capital of Wales: Formal Recognition by
    Government."，[泰晤士報](../Page/泰晤士報.md "wikilink")，1955年12月21日