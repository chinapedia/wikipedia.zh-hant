**Yahoo 人氣大獎**（[英文](../Page/英語.md "wikilink")：），前稱 **Yahoo
搜尋人氣大獎**，由[雅虎香港於](../Page/雅虎香港.md "wikilink")2005年創辦，是[香港首個](../Page/香港.md "wikilink")[網站所舉辦之綜合頒獎禮](../Page/網站.md "wikilink")。Yahoo
人氣大獎主要根據提名名單在[互聯網的熱門程度](../Page/互聯網.md "wikilink")，向受歡迎的[娛樂圈人物頒發相關年度獎項](../Page/娛樂圈.md "wikilink")。

## 評審準則

由2012年起，「Yahoo 人氣大獎」的評審準則，分為三部分：

1.  候選名單在雅虎搜尋器的搜尋數據
2.  候選藝人的[資訊](../Page/資訊.md "wikilink")，在雅虎香港內的瀏覽量
3.  投票環節的得票

## 發展

雅虎香港於2005年首次舉辦「Yahoo\! Buzz
Awards」，根據雅虎[搜尋器的搜尋數據作為評審準則](../Page/搜尋器.md "wikilink")，就當年互聯網的熱門搜尋娛樂資訊，電影、電視、歌曲、人物、社會話題及訊息等等頒發獎項。2006年，雅虎香港更加為人氣大獎設立網站，以增加公眾對大獎的互動性，並定於每年十二月舉辦較大型的頒獎典禮。

[Yahoo！Buzz_Awards.jpg](https://zh.wikipedia.org/wiki/File:Yahoo！Buzz_Awards.jpg "fig:Yahoo！Buzz_Awards.jpg")

2007年6月起，雅虎香港推出「YAHOO\!搜尋人氣榜」，每小時提供人氣排名及各人氣榜的過去走勢，並以此作為搜尋人氣大獎所頒發的獎項為依據。更首次以[音樂會形式舉行](../Page/音樂會.md "wikilink")，並把門券收入轉至[慈善團體](../Page/慈善團體.md "wikilink")。

2009年，搜尋人氣大獎易命為「Yahoo\! Asia Buzz
Awards」，並開始設立網上投票環節，作為其中一個頒發獎項的參考數據之一。2010年更首次網上直播頒獎禮，另外取消社會焦點獎項，完全改為[娛樂圈的頒獎禮](../Page/娛樂圈.md "wikilink")。

2009年-2011年，搜尋人氣大獎推展至[台灣雅虎及](../Page/台灣.md "wikilink")[韓國雅虎](../Page/韓國.md "wikilink")，在香港、台灣、韓國各設立一個搜尋人氣大獎及頒獎禮。

2012年，雅虎香港把人氣大獎的評審準則，由原來只參考候選名單的搜尋數據及投票環節的得票外，擴展至計算候選藝人的[資訊](../Page/資訊.md "wikilink")，在雅虎香港內的瀏覽量。

## 歷屆主要獎項

### 音樂類

| 年份   | 本地男歌手                                                                                                                                            | 本地女歌手                                                                                                                                            | 本地組合樂隊                               |
| ---- | ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------ |
| 2005 | [古巨基](../Page/古巨基.md "wikilink")                                                                                                                 | [杨千嬅](../Page/楊千嬅.md "wikilink")                                                                                                                 | [Twins](../Page/Twins.md "wikilink") |
| 2006 | [容祖兒](../Page/容祖兒.md "wikilink")                                                                                                                 | [Twins](../Page/Twins.md "wikilink")、[軟硬天師](../Page/軟硬天師.md "wikilink")                                                                          |                                      |
| 2007 | [陳奕迅](../Page/陳奕迅.md "wikilink")                                                                                                                 | [Twins](../Page/Twins.md "wikilink")、[农夫](../Page/農夫.md "wikilink")                                                                              |                                      |
| 2008 | [古巨基](../Page/古巨基.md "wikilink")                                                                                                                 | [农夫](../Page/農夫_\(組合\).md "wikilink")、[Hotcha](../Page/HotCha.md "wikilink")                                                                     |                                      |
| 2009 | [Mr.](../Page/Mr..md "wikilink")、[RubberBand](../Page/RubberBand.md "wikilink")、[Hotcha](../Page/HotCha.md "wikilink")                           |                                                                                                                                                  |                                      |
| 2010 | [Mr.](../Page/Mr..md "wikilink")、[RubberBand](../Page/RubberBand.md "wikilink")、[野仔](../Page/野仔.md "wikilink")                                   |                                                                                                                                                  |                                      |
| 2011 | [Twins](../Page/Twins.md "wikilink")、[RubberBand](../Page/RubberBand.md "wikilink")、[C AllStar](../Page/C_AllStar.md "wikilink")                 |                                                                                                                                                  |                                      |
| 2012 |                                                                                                                                                  |                                                                                                                                                  |                                      |
| 2013 | [周柏豪](../Page/周柏豪.md "wikilink")                                                                                                                 | [Dear Jane](../Page/Dear_Jane.md "wikilink")、[RubberBand](../Page/RubberBand.md "wikilink")、[C AllStar](../Page/C_AllStar.md "wikilink")         |                                      |
| 2014 |                                                                                                                                                  |                                                                                                                                                  |                                      |
| 2015 | [Dear Jane](../Page/Dear_Jane.md "wikilink")、[RubberBand](../Page/RubberBand.md "wikilink")、[Supper Moment](../Page/Supper_Moment.md "wikilink") |                                                                                                                                                  |                                      |
| 2016 | [張敬軒](../Page/張敬軒.md "wikilink")                                                                                                                 |                                                                                                                                                  |                                      |
| 2017 | [李克勤](../Page/李克勤.md "wikilink")                                                                                                                 | [Dear Jane](../Page/Dear_Jane.md "wikilink")、[Supper Moment](../Page/Supper_Moment.md "wikilink")、[C AllStar](../Page/C_AllStar.md "wikilink")   |                                      |
| 2018 | [張敬軒](../Page/張敬軒.md "wikilink")                                                                                                                 | [Dear Jane](../Page/Dear_Jane.md "wikilink")、[RubberBand](../Page/RubberBand.md "wikilink")、[Supper Moment](../Page/Supper_Moment.md "wikilink") |                                      |

### 電影類

| 年份   | 本地男演員                            | 本地女演員                                   | 本地電影                                                                        |
| ---- | -------------------------------- | --------------------------------------- | --------------------------------------------------------------------------- |
| 2005 | [劉德華](../Page/劉德華.md "wikilink") | [林嘉欣](../Page/林嘉欣.md "wikilink")        |                                                                             |
| 2006 | [郭富城](../Page/郭富城.md "wikilink") | [蔡卓妍](../Page/蔡卓妍.md "wikilink")        |                                                                             |
| 2007 | [古天樂](../Page/古天樂.md "wikilink") | [楊千嬅](../Page/楊千嬅.md "wikilink")        | 《[每當變幻時](../Page/每當變幻時_\(電影\).md "wikilink")》                               |
| 2008 | [方力申](../Page/方力申.md "wikilink") | [鄧麗欣](../Page/鄧麗欣.md "wikilink")        | 《[我的最愛](../Page/我的最愛_\(電影\).md "wikilink")》                                 |
| 2009 | [張家輝](../Page/張家輝.md "wikilink") | 《[保持爱你](../Page/保持愛你.md "wikilink")》    |                                                                             |
| 2010 | [王祖藍](../Page/王祖藍.md "wikilink") | [薛凱琪](../Page/薛凱琪.md "wikilink")        | 《[志明與春嬌](../Page/志明與春嬌_\(電影\).md "wikilink")》                               |
| 2011 | [吳彥祖](../Page/吳彥祖.md "wikilink") | [周秀娜](../Page/周秀娜.md "wikilink")        | 《[窃听风云2](../Page/竊聽風雲2.md "wikilink")》                                      |
| 2012 | [鄭中基](../Page/鄭中基.md "wikilink") | [楊千嬅](../Page/楊千嬅.md "wikilink")        | 《[春嬌與志明](../Page/春嬌與志明.md "wikilink")》《[低俗喜剧](../Page/低俗喜劇.md "wikilink")》  |
| 2013 | [周秀娜](../Page/周秀娜.md "wikilink") | 《[狂舞派](../Page/狂舞派.md "wikilink")》      |                                                                             |
| 2014 | [廖启智](../Page/廖啟智.md "wikilink") | [周秀娜](../Page/周秀娜.md "wikilink")        | 《[那夜凌晨，我坐上了旺角开往大埔的红VAN](../Page/那夜凌晨，我坐上了旺角開往大埔的紅VAN_\(電影\).md "wikilink")》 |
| 2015 | [王宗尧](../Page/王宗堯.md "wikilink") | [蔡卓妍](../Page/蔡卓妍.md "wikilink")        | 《[五個小孩的校長](../Page/五個小孩的校長.md "wikilink")》                                  |
| 2016 | [林嘉欣](../Page/林嘉欣.md "wikilink") | 《[十年](../Page/十年_\(電影\).md "wikilink")》 |                                                                             |
| 2017 | [惠英红](../Page/惠英紅.md "wikilink") | 《[29+1](../Page/29+1.md "wikilink")》    |                                                                             |
| 2018 | [古天樂](../Page/古天樂.md "wikilink") | 《[逆流大叔](../Page/逆流大叔.md "wikilink")》    |                                                                             |

### 電視類

| 年份   | 电视男藝人                                | 電視女藝人                                          | 本地電視劇                                          | 本地綜藝節目                                                               |
| ---- | ------------------------------------ | ---------------------------------------------- | ---------------------------------------------- | -------------------------------------------------------------------- |
| 2005 | [吳卓羲](../Page/吳卓羲.md "wikilink")     | [胡杏兒](../Page/胡杏兒.md "wikilink")               |                                                |                                                                      |
| 2006 | [鄭嘉穎](../Page/鄭嘉穎.md "wikilink")     |                                                |                                                |                                                                      |
| 2007 | [林峯](../Page/林峯.md "wikilink")       | [李司棋](../Page/李司棋.md "wikilink")               | 《[溏心風暴](../Page/溏心風暴.md "wikilink")》           | 《[味分高下](../Page/味分高下.md "wikilink")》                                 |
| 2008 | [佘诗曼](../Page/佘詩曼.md "wikilink")     | 《[溏心風暴之家好月圓](../Page/溏心風暴之家好月圓.md "wikilink")》 | 《[铁甲无敌奖门人](../Page/鐵甲無敵獎門人.md "wikilink")》     |                                                                      |
| 2009 | [黎耀祥](../Page/黎耀祥.md "wikilink")     | [鄧萃雯](../Page/鄧萃雯.md "wikilink")               | 《[巾帼枭雄](../Page/巾幗梟雄.md "wikilink")》           | 《[美女廚房](../Page/美女廚房_\(第一輯\).md "wikilink")》                         |
| 2010 | [林峯](../Page/林峯.md "wikilink")       | [楊怡](../Page/楊怡.md "wikilink")                 | 《[巾幗梟雄之義海豪情](../Page/巾幗梟雄之義海豪情.md "wikilink")》 | 《[荃加福禄寿](../Page/荃加福祿壽.md "wikilink")》                               |
| 2011 | [陳豪](../Page/陳豪.md "wikilink")       | [胡杏兒](../Page/胡杏兒.md "wikilink")               | 《[潜行狙击](../Page/潛行狙擊.md "wikilink")》           | 《[華麗明星賽](../Page/華麗明星賽.md "wikilink")》                               |
| 2012 | [佘诗曼](../Page/佘詩曼.md "wikilink")     | 《[天与地](../Page/天與地_\(無綫電視劇\).md "wikilink")》   |                                                |                                                                      |
| 2013 | [李思捷](../Page/李思捷.md "wikilink")     | [田蕊妮](../Page/田蕊妮.md "wikilink")               | 《[衝上雲霄II](../Page/衝上雲霄II.md "wikilink")》       |                                                                      |
| 2014 | [王祖藍](../Page/王祖藍.md "wikilink")     | [佘诗曼](../Page/佘詩曼.md "wikilink")               | 《[使徒行者](../Page/使徒行者.md "wikilink")》           |                                                                      |
| 2015 | [陳展鵬](../Page/陳展鵬.md "wikilink")     | [胡定欣](../Page/胡定欣.md "wikilink")               | 《[導火新聞線](../Page/導火新聞線.md "wikilink")》         |                                                                      |
| 2016 | 《[城寨英雄](../Page/城寨英雄.md "wikilink")》 |                                                |                                                |                                                                      |
| 2017 | [王浩信](../Page/王浩信.md "wikilink")     | [唐詩詠](../Page/唐詩詠.md "wikilink")               | 《[踩過界](../Page/踩過界.md "wikilink")》             |                                                                      |
| 2018 | [陳展鵬](../Page/陳展鵬.md "wikilink")     | [李佳芯](../Page/李佳芯.md "wikilink")               | 《[宮心計2深宮計](../Page/宮心計2深宮計.md "wikilink")》     | 《[Good Night Show 全民造星](../Page/Good_Night_Show_全民造星.md "wikilink")》 |

## 外部連結

  - [YAHOO 搜尋人氣大獎 2018](https://hk.promotions.yahoo.net/buzz2018/)

[Category:Yahoo\!搜尋人氣大獎](../Category/Yahoo!搜尋人氣大獎.md "wikilink")
[Category:雅虎](../Category/雅虎.md "wikilink")