[HK_Olympic_Garden01.JPG](https://zh.wikipedia.org/wiki/File:HK_Olympic_Garden01.JPG "fig:HK_Olympic_Garden01.JPG")
[Image-HK_Olympic_Garden02.JPG](https://zh.wikipedia.org/wiki/File:Image-HK_Olympic_Garden02.JPG "fig:Image-HK_Olympic_Garden02.JPG")
[Banner_Olympic_Garden.jpg](https://zh.wikipedia.org/wiki/File:Banner_Olympic_Garden.jpg "fig:Banner_Olympic_Garden.jpg")告示\]\]

**世運花園**（）俗稱「**世運公園**」，位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[九龍城以南](../Page/九龍城.md "wikilink")，鄰近舊[啟德機場](../Page/啟德機場.md "wikilink")，是香港首座以[奧林匹克命名的地名](../Page/奧林匹克.md "wikilink")。公園位於九龍城迴旋處之內，上方有[亞皆老街至](../Page/亞皆老街.md "wikilink")[太子道東的天橋](../Page/太子道.md "wikilink")（1972年4月25日啟用），太子道西、[亞皆老街及](../Page/亞皆老街.md "wikilink")[馬頭涌道都在此交匯](../Page/馬頭涌道.md "wikilink")。

## 設計

公園內奧運的元素不多，僅有[五環及](../Page/奧林匹克五環.md "wikilink")[火炬作為圖案的花壇](../Page/火炬.md "wikilink")。此公園附近為舊啟德機場，鄰近[宋王臺公園](../Page/宋王臺公園.md "wikilink")。由於九龍城有眾多泰國店舖及食肆，因而每個星期日都會有大量[泰國女傭在此聚集](../Page/香港外籍家庭傭工.md "wikilink")，故場內的告示牌也以[中文](../Page/中文.md "wikilink")、[英文及](../Page/英文.md "wikilink")[泰文書寫](../Page/泰文.md "wikilink")。

## 歷史

[1964年夏季奧林匹克運動會聖火在](../Page/1964年夏季奧林匹克運動會聖火傳遞.md "wikilink")[希臘採集過後](../Page/希臘.md "wikilink")，運往主辦地[日本](../Page/日本.md "wikilink")[東京途中於同年](../Page/東京.md "wikilink")8月路經香港傳遞，是[香港歷史上首次迎來](../Page/香港歷史.md "wikilink")[奧運聖火](../Page/奧運聖火.md "wikilink")。[香港政府為了紀念此項盛事](../Page/香港政府.md "wikilink")，將九龍城啟德機場附近一個迴旋處公園命名為世運公園，並且將附近的街道命名為[世運道](../Page/世運道.md "wikilink")。當中的世運是當時香港對[奧林匹克運動會的俗稱](../Page/奧林匹克運動會.md "wikilink")，與[世界運動會無關係](../Page/世界運動會.md "wikilink")。

1970年12月4日，時任[教宗](../Page/教宗.md "wikilink")[保祿六世訪問香港](../Page/保祿六世.md "wikilink")，港府當時派開篷跑車接載教宗，當教宗離開[啟德機場時](../Page/啟德機場.md "wikilink")，不少市民都聚集在世運花園迎接教宗。

2012年11月26日，[康樂及文化事務署宣布世運花園由即日至](../Page/康樂及文化事務署.md "wikilink")2016年12月30日關閉，以配合港鐵[沙田至中環線興建工程](../Page/沙田至中環綫.md "wikilink")\[1\]。

未來[宋皇臺站落成後](../Page/宋皇臺站.md "wikilink")，世運花園或將成為連接車站通道的一部分。

## 出入口

世運花園設有幾個行人出入口，均為[行人隧道](../Page/行人隧道.md "wikilink")。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: white; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: white; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: white; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")：

<!-- end list -->

  - <font color="{{屯馬綫色彩}}">█</font>[屯馬綫](../Page/屯馬綫.md "wikilink")：[宋皇臺站B出入口](../Page/宋皇臺站.md "wikilink")（2019年通車\[2\]）

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

<File:HK> Kln City 世運公園行人隧道 Olympic Garden Subway night Pedestrian
tunnel entrance.JPG <File:HK> Kln City 世運公園行人隧道 Olympic Garden Subway
night Pedestrian tunnel interior visitors.JPG

## 禁煙安排

此場地（除吸煙區外）禁止[吸煙](../Page/吸煙.md "wikilink")。

## 相關條目

  - [九龍城](../Page/九龍城.md "wikilink")
  - [1964年夏季奧林匹克運動會聖火傳遞](../Page/1964年夏季奧林匹克運動會聖火傳遞.md "wikilink")

## 注釋

<references />

[分類:香港主題公園](../Page/分類:香港主題公園.md "wikilink")

[Category:馬頭圍](../Category/馬頭圍.md "wikilink")
[Category:九龍城區公園](../Category/九龍城區公園.md "wikilink")

1.  <http://www.info.gov.hk/gia/general/201211/26/P201211260542.htm>
2.