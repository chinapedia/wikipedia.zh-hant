**《最緊要進步》**（[英文](../Page/英文.md "wikilink")：**），是一個取材自[香港](../Page/香港.md "wikilink")[社會的](../Page/社會.md "wikilink")[遊戲](../Page/遊戲.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")。此節目於2007年7月3日晚上22:30-23:00，逢星期二至星期五於[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台播映](../Page/翡翠台.md "wikilink")，[主持人為](../Page/主持人.md "wikilink")[王貽興及](../Page/王貽興.md "wikilink")[沈卓盈](../Page/沈卓盈.md "wikilink")。[遊戲內的](../Page/遊戲.md "wikilink")[問答題目主題是](../Page/問題.md "wikilink")[香港主權移交之後](../Page/香港主權移交.md "wikilink")10年內發生的事件。

## 每集嘉賓

<table>
<tbody>
<tr class="odd">
<td><p><strong>集數</strong></p></td>
<td><p><strong>播映日期／星期</strong></p></td>
<td><p><strong>隊伍</strong></p></td>
<td><p><strong>嘉賓</strong></p></td>
<td><p><strong>公眾團體隊伍</strong></p></td>
<td><p><strong>勝出隊伍</strong></p></td>
<td><p><strong>所獲徽章及獎金（如有）</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>07月3日<br />
（星期二）</p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍隊</a></p></td>
<td><p><a href="../Page/陳敏之.md" title="wikilink">陳敏之</a>、<a href="../Page/黄宗泽.md" title="wikilink">黄宗泽</a></p></td>
<td><p><a href="../Page/香港泰拳理事會.md" title="wikilink">香港泰拳理事會隊</a></p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/學警出更.md" title="wikilink">學警出更隊</a></p></td>
<td><p><a href="../Page/李詩韻.md" title="wikilink">李詩韻</a>、<a href="../Page/陳鍵鋒.md" title="wikilink">陳鍵鋒</a></p></td>
<td><p><a href="../Page/香港獅子會.md" title="wikilink">香港獅子會隊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>07月4日<br />
（星期三）</p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍隊</a></p></td>
<td><p><a href="../Page/梁烈唯.md" title="wikilink">梁烈唯</a>、<a href="../Page/陳敏之.md" title="wikilink">陳敏之</a></p></td>
<td><p><a href="../Page/中國香港跳繩總會.md" title="wikilink">中國香港跳繩總會隊</a></p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍隊</a></p></td>
<td><p>銀進步徽章</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/司儀.md" title="wikilink">司儀隊</a></p></td>
<td><p><a href="../Page/鄧梓峰.md" title="wikilink">鄧梓峰</a>、<a href="../Page/丘凱敏.md" title="wikilink">丘凱敏</a></p></td>
<td><p><a href="../Page/康宏晨曦足球隊.md" title="wikilink">康宏晨曦足球隊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>07月5日<br />
（星期四）</p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍隊</a></p></td>
<td><p><a href="../Page/梁烈唯.md" title="wikilink">梁烈唯</a>、<a href="../Page/陳敏之.md" title="wikilink">陳敏之</a></p></td>
<td><p><a href="../Page/聖保羅書院.md" title="wikilink">聖保羅書院隊</a></p></td>
<td><p><a href="../Page/體育世界_(體育節目).md" title="wikilink">體育世界隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/彭健新.md" title="wikilink">彭健新</a>、<a href="../Page/官恩娜.md" title="wikilink">官恩娜</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/體育世界_(體育節目).md" title="wikilink">體育世界隊</a></p></td>
<td><p><a href="../Page/鍾志光.md" title="wikilink">鍾志光</a>、<a href="../Page/韓毓霞.md" title="wikilink">韓毓霞</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>07月6日<br />
（星期五）</p></td>
<td><p><a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a></p></td>
<td><p><a href="../Page/楊思琦.md" title="wikilink">楊思琦</a>、<a href="../Page/曹敏莉.md" title="wikilink">曹敏莉</a></p></td>
<td><p><a href="../Page/北少林門龍子祥國術總會.md" title="wikilink">北少林門龍子祥國術總會隊</a><br />
<a href="../Page/香港體育舞蹈聯盟.md" title="wikilink">香港體育舞蹈聯盟隊</a></p></td>
<td><p><a href="../Page/香港體育舞蹈聯盟.md" title="wikilink">香港體育舞蹈聯盟隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/體育世界_(體育節目).md" title="wikilink">體育世界隊</a></p></td>
<td><p><a href="../Page/鍾志光.md" title="wikilink">鍾志光</a>、<a href="../Page/韓毓霞.md" title="wikilink">韓毓霞</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>07月10日<br />
（星期二）</p></td>
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/孫耀威.md" title="wikilink">孫耀威</a>、<a href="../Page/關心妍.md" title="wikilink">關心妍</a></p></td>
<td><p><a href="../Page/香港體育舞蹈聯盟.md" title="wikilink">香港體育舞蹈聯盟隊</a></p></td>
<td><p><a href="../Page/DJ.md" title="wikilink">DJ隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/師奶兵團.md" title="wikilink">師奶兵團</a></p></td>
<td><p><a href="../Page/謝天華.md" title="wikilink">謝天華</a>、<a href="../Page/商天娥.md" title="wikilink">商天娥</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DJ.md" title="wikilink">DJ隊</a></p></td>
<td><p><a href="../Page/I_Love_U_Boyz.md" title="wikilink">I Love U Boyz</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>07月11日<br />
（星期三）</p></td>
<td><p><a href="../Page/DJ.md" title="wikilink">DJ隊</a></p></td>
<td><p><a href="../Page/I_Love_U_Boyz.md" title="wikilink">I Love U Boyz</a></p></td>
<td><p><a href="../Page/香港極限運動聯會.md" title="wikilink">香港極限運動聯會隊</a><br />
<a href="../Page/香港福建聯會.md" title="wikilink">香港福建聯會隊</a></p></td>
<td><p>「心心相印」隊</p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>「心心相印」隊</p></td>
<td><p><a href="../Page/陳國邦.md" title="wikilink">陳國邦</a>、<a href="../Page/羅敏莊.md" title="wikilink">羅敏莊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>07月12日<br />
（星期四）</p></td>
<td><p>「心心相印」隊</p></td>
<td><p><a href="../Page/陳國邦.md" title="wikilink">陳國邦</a>、<a href="../Page/羅敏莊.md" title="wikilink">羅敏莊</a></p></td>
<td><p><a href="../Page/香港跆拳道協會.md" title="wikilink">香港跆拳道協會隊</a><br />
<a href="../Page/香港土風舞協會.md" title="wikilink">香港土風舞協會隊</a></p></td>
<td><p>「心心相印」隊</p></td>
<td><p>銀進步徽章</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勁歌金曲.md" title="wikilink">勁歌金曲隊</a></p></td>
<td><p><a href="../Page/崔建邦.md" title="wikilink">崔建邦</a>、<a href="../Page/林二汶.md" title="wikilink">林二汶</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>07月13日<br />
（星期五）</p></td>
<td><p><a href="../Page/強劍.md" title="wikilink">強劍隊</a></p></td>
<td><p><a href="../Page/李思捷.md" title="wikilink">李思捷</a>、<a href="../Page/羅冠蘭.md" title="wikilink">羅冠蘭</a></p></td>
<td><p><a href="../Page/香港影藝聯盟.md" title="wikilink">香港影藝聯盟隊</a><br />
<a href="../Page/春風獅子會.md" title="wikilink">春風獅子會隊</a></p></td>
<td><p>「心心相印」隊</p></td>
<td><p>金進步徽章</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>「心心相印」隊</p></td>
<td><p><a href="../Page/陳國邦.md" title="wikilink">陳國邦</a>、<a href="../Page/羅敏莊.md" title="wikilink">羅敏莊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>07月17日<br />
（星期二）</p></td>
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/戴夢夢.md" title="wikilink">戴夢夢</a>、<a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td></td>
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東張西望.md" title="wikilink">東張西望隊</a></p></td>
<td><p><a href="../Page/高皓正.md" title="wikilink">高皓正</a>、<a href="../Page/張美妮.md" title="wikilink">張美妮</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/麥包.md" title="wikilink">麥長青</a>、<a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>07月18日<br />
（星期三）</p></td>
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/戴夢夢.md" title="wikilink">戴夢夢</a>、<a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td></td>
<td></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/TVB8.md" title="wikilink">TVB8隊</a></p></td>
<td><p><a href="../Page/李亞男.md" title="wikilink">李亞男</a>、<a href="../Page/王浩信.md" title="wikilink">王浩信</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/盧巧音.md" title="wikilink">盧巧音</a>、<a href="../Page/張繼聰.md" title="wikilink">張繼聰</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>07月19日<br />
（星期四）</p></td>
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/盧巧音.md" title="wikilink">盧巧音</a>、<a href="../Page/張繼聰.md" title="wikilink">張繼聰</a></p></td>
<td></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/蔡子健.md" title="wikilink">蔡子健</a>、<a href="../Page/朱凱婷.md" title="wikilink">朱凱婷</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>07月20日<br />
（星期五）</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/蔡子健.md" title="wikilink">蔡子健</a>、<a href="../Page/朱凱婷.md" title="wikilink">朱凱婷</a></p></td>
<td></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p>32萬獎金及獎品<br />
銀進步徽章</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DJ.md" title="wikilink">DJ隊</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>07月24日<br />
（星期二）</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/蔡子健.md" title="wikilink">蔡子健</a>、<a href="../Page/朱凱婷.md" title="wikilink">朱凱婷</a></p></td>
<td></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p>金進步徽章</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/都市閒情.md" title="wikilink">都市閒情隊</a></p></td>
<td><p><a href="../Page/安德尊.md" title="wikilink">安德尊</a>、<a href="../Page/黃宇詩.md" title="wikilink">黃宇詩</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>07月25日<br />
（星期三）</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/喬寶寶.md" title="wikilink">喬寶寶</a>、<a href="../Page/康華.md" title="wikilink">康華</a></p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>07月26日<br />
（星期四）</p></td>
<td><p><a href="../Page/星級廚房.md" title="wikilink">星級廚房隊</a></p></td>
<td><p><a href="../Page/袁彩雲.md" title="wikilink">袁彩雲</a>、<a href="../Page/周中.md" title="wikilink">周中</a></p></td>
<td><p><a href="../Page/香港游泳協會.md" title="wikilink">香港游泳協會隊</a><br />
<a href="../Page/音樂農莊.md" title="wikilink">音樂農莊隊</a></p></td>
<td><p><a href="../Page/香港游泳協會.md" title="wikilink">香港游泳協會隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/喬寶寶.md" title="wikilink">喬寶寶</a>、<a href="../Page/康華.md" title="wikilink">康華</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>07月27日<br />
（星期五）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>07月31日<br />
（星期二）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>08月1日<br />
（星期三）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>08月2日<br />
（星期四）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>08月7日<br />
（星期二）</p></td>
<td><p><a href="../Page/香港先生.md" title="wikilink">香港先生隊</a><br />
<a href="../Page/我外母唔係人.md" title="wikilink">我外母唔係人隊</a>（主持稱「外隊組」）</p></td>
<td><p><a href="../Page/黃長興.md" title="wikilink">黃長興</a>、<a href="../Page/高鈞賢.md" title="wikilink">高鈞賢</a><br />
<a href="../Page/薛家燕.md" title="wikilink">薛家燕</a>、<a href="../Page/梁榮忠.md" title="wikilink">梁榮忠</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>08月8日<br />
（星期三）</p></td>
<td><p><a href="../Page/歌手.md" title="wikilink">歌星隊</a><br />
<a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/李逸朗.md" title="wikilink">李逸朗</a>、<a href="../Page/蔣雅文.md" title="wikilink">蔣雅文</a><br />
<a href="../Page/鄧兆尊.md" title="wikilink">鄧兆尊</a>、<a href="../Page/姚樂怡.md" title="wikilink">姚樂怡</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>08月9日<br />
（星期四）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>08月10日<br />
（星期五）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>08月14日<br />
（星期二）</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a><br />
<a href="../Page/東張西望.md" title="wikilink">東張西望隊</a><br />
<a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td></td>
<td><p>公民足球隊</p></td>
<td><p><a href="../Page/東張西望.md" title="wikilink">東張西望隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>08月15日<br />
（星期三）</p></td>
<td><p><a href="../Page/東張西望.md" title="wikilink">東張西望隊</a><br />
<a href="../Page/歌手.md" title="wikilink">歌星隊</a><br />
<a href="../Page/TVB.md" title="wikilink">TVB隊</a><br />
<a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a></p></td>
<td><p><a href="../Page/宋芝齡.md" title="wikilink">宋芝齡</a>、<a href="../Page/李忠希.md" title="wikilink">李忠希</a><br />
<a href="../Page/裕美.md" title="wikilink">裕美</a>、Eddie@<a href="../Page/EO2.md" title="wikilink">EO2</a><br />
<a href="../Page/汪琳.md" title="wikilink">汪琳</a><br />
<a href="../Page/鄧上文.md" title="wikilink">鄧上文</a>、<a href="../Page/葉翠翠.md" title="wikilink">葉翠翠</a></p></td>
<td><div style="text-align: center;"></td>
<td><p><a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a></p></td>
<td><p>18萬獎金及獎品<br />
銅進步徽章</p>
</div></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p>08月16日<br />
（星期四）</p></td>
<td><p><a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a></p></td>
<td><p><a href="../Page/鄧上文.md" title="wikilink">鄧上文</a>、<a href="../Page/葉翠翠.md" title="wikilink">葉翠翠</a></p></td>
<td></td>
<td><p><a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a></p></td>
<td><p>銀進步徽章</p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p>08月17日<br />
（星期五）</p></td>
<td><p><a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a><br />
<a href="../Page/歲月風雲.md" title="wikilink">歲月風雲隊</a><br />
<a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/鄧上文.md" title="wikilink">鄧上文</a>、<a href="../Page/葉翠翠.md" title="wikilink">葉翠翠</a><br />
<a href="../Page/陳美琪.md" title="wikilink">陳美琪</a>、<a href="../Page/伍衛國.md" title="wikilink">伍衛國</a><br />
<a href="../Page/曹永廉.md" title="wikilink">曹永廉</a>、<a href="../Page/宋熙年.md" title="wikilink">宋熙年</a></p></td>
<td></td>
<td><p><a href="../Page/歲月風雲.md" title="wikilink">歲月風雲隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>08月21日<br />
（星期二）</p></td>
<td><p><a href="../Page/歲月風雲.md" title="wikilink">歲月風雲隊</a><br />
<a href="../Page/TVB.md" title="wikilink">TVB隊</a><br />
<a href="../Page/歌手.md" title="wikilink">歌星隊</a></p></td>
<td><p><a href="../Page/陳美琪.md" title="wikilink">陳美琪</a>、<a href="../Page/伍衛國.md" title="wikilink">伍衛國</a><br />
<a href="../Page/黃浩然.md" title="wikilink">黃浩然</a>、<a href="../Page/姚嘉妮.md" title="wikilink">姚嘉妮</a><br />
</p></td>
<td><p><a href="../Page/藝進同學會.md" title="wikilink">藝進同學會隊</a></p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p>08月22日<br />
（星期三）</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a><br />
<a href="../Page/一擲千金_(香港).md" title="wikilink">一擲千金隊</a></p></td>
<td><p><a href="../Page/黃浩然.md" title="wikilink">黃浩然</a>、<a href="../Page/姚嘉妮.md" title="wikilink">姚嘉妮</a><br />
<a href="../Page/陳文靜.md" title="wikilink">陳文靜</a>、<a href="../Page/周家怡.md" title="wikilink">周家怡</a><br />
</p></td>
<td></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p>銀進步徽章</p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p>08月23日<br />
（星期四）</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB台主隊</a><br />
<a href="../Page/香港小姐.md" title="wikilink">香港小姐隊</a><br />
<a href="../Page/都市閒情.md" title="wikilink">都市閒情隊</a><br />
<a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p><a href="../Page/黃浩然.md" title="wikilink">黃浩然</a>、<a href="../Page/姚嘉妮.md" title="wikilink">姚嘉妮</a><br />
<a href="../Page/呂慧儀.md" title="wikilink">呂慧儀</a>、<a href="../Page/林莉_(藝人).md" title="wikilink">林莉</a><br />
<a href="../Page/羅貫峰.md" title="wikilink">羅貫峰</a>、<a href="../Page/李錦聯.md" title="wikilink">李錦聯</a><br />
<a href="../Page/蓋世寶.md" title="wikilink">蓋世寶</a>、<a href="../Page/李思欣.md" title="wikilink">李思欣</a></p></td>
<td><div style="text-align: center;">
<p>-</p></td>
<td><p><a href="../Page/TVB.md" title="wikilink">TVB隊</a></p></td>
<td><p>金進步徽章</p>
</div></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p>08月24日<br />
（星期五）</p></td>
<td><p><a href="../Page/香港先生.md" title="wikilink">香港先生隊</a><br />
<a href="../Page/TVB.md" title="wikilink">TVB隊</a><br />
<a href="../Page/無綫生活台.md" title="wikilink">生活台隊</a><br />
<a href="../Page/體育世界_(體育節目).md" title="wikilink">體育世界隊</a></p></td>
<td><p><a href="../Page/鄭健樂.md" title="wikilink">鄭健樂</a>、<a href="../Page/袁偉豪.md" title="wikilink">袁偉豪</a><br />
<a href="../Page/鄧英敏.md" title="wikilink">鄧英敏</a>、<a href="../Page/劉彩玉.md" title="wikilink">劉彩玉</a><br />
<a href="../Page/李雨陽.md" title="wikilink">李雨陽</a><br />
<a href="../Page/黃安琪.md" title="wikilink">黃安琪</a></p></td>
<td><div style="text-align: center;">
<p>-</p></td>
<td><p><a href="../Page/體育世界_(體育節目).md" title="wikilink">體育世界隊</a></p></td>
<td><p>銅進步徽章</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 作品的變遷

[Category:2007年無綫電視節目](../Category/2007年無綫電視節目.md "wikilink")
[Category:無綫電視遊戲節目](../Category/無綫電視遊戲節目.md "wikilink")
[Category:香港主權移交十周年](../Category/香港主權移交十周年.md "wikilink")