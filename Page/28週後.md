《**28週後**》（）是[丹尼·波爾在](../Page/丹尼·波爾.md "wikilink")2002年拍的《[28日後](../Page/28日後.md "wikilink")》的續集。此片由[尊·卡路斯·費拿迪奧執導](../Page/尊·卡路斯·費拿迪奧.md "wikilink")，在2007年5月11日於美國及英國上映。

## 劇情

影片從一罐蕃茄醬罐頭的晚餐開始。

外界，僵尸病毒正在肆虐，Don和他的妻子Alice，与另外一对情侣，和一个男人，躲在一对年老夫妻的农村房舍里。五天前女孩的男友离开房子，再也没有回来。他们正在准备用餐，突然一个小男孩前來敲门，六個人起初不敢开门，因为怕染上狂暴病毒的[僵尸袭击他们](../Page/僵尸.md "wikilink")，后来出于同情，還是將门給打开，救下了男孩。但隨之而來的一群僵尸利用空隙闯了进来，並攻擊這些在屋內僅存的生還者。Alice與小孩被殭屍們追趕無路可逃，Don也無能為力，無法救出Alice與小孩，此刻的他也受到大批殭屍的追趕，自身難保的Don只得拼命的往前奔跑，最後乘坐一艘快艇逃跑，成为唯一的幸存者。

病毒爆发15天后，英伦全岛被隔离。28天后，英伦全岛被僵尸病毒摧毁。5周后，被感染者因饥饿而死亡。11周后，美国领导北约军队进驻伦敦。18周后，官方宣布英伦全岛无感染者。而28周后，英國的重建工作，由此開始。

[美国领导的](../Page/美国.md "wikilink")[北约组织把感染爆發前身處國外的英國人送回英国的管制居住区](../Page/北约组织.md "wikilink")“第一区(道格斯岛)”。第一区的首席医疗官Scarlet
Ross少校发现这些人口中包括两个孩子，Tammy和Andy，
他们是Don和Alice的孩子，在疾病暴发的那段时间他们在[西班牙參加學校旅行](../Page/西班牙.md "wikilink")。

12岁的Andy是現在英国年龄最小的人，在接下来的体检中，Lima发现Andy的[虹膜异色症](../Page/虹膜异色症.md "wikilink")，这是由他的母亲遗传下来的。Andy和Tammy随即搬进了第一区，那是一个戒备森严的住宅区。在住宅区的卫队中有一个名叫Doyle的狙擊手，和一个直升机侦察员Flynn。两个孩子找到了他们的父亲Don，他现在是这个区的管理者。Don告诉别人说他看到Alice是被僵尸杀死的（其实他不想让别人认为他撇下他的妻子一个人溜走而撒谎）。

第二天，那两个孩子跑回了他们以前的家，卻意外发现Alice还活着，隨後兩人遭到美国军方拘捕，Alice要接受净化和觀察。验血证明她已经受到狂暴病毒的感染，而她還是个[带原者](../Page/带原者.md "wikilink")，因为她的[眼睛是红色的](../Page/眼睛.md "wikilink")，但沒有出現狂暴症狀。虽然Scarlet想治疗Alice，但是遭到了史東(Stone)將軍的阻止，Stone想杀死Alice以绝后患。

Don来看守室看他的孩子，在那里他遭到了孩子们的指责。Don良心有愧，于是去找Alice，但是在接吻的时候感染上了狂暴病毒。Don殺死了Alice及幾名哨兵，军队立刻開始戒严，所有的市民被集中在安全的房间，但Don闯了进去感染其他人。雖然Scarlet成功地营救了Tammy，但是Andy走失了。Doyle和其他士兵开始執行戒嚴任務，开始只是针对那些被感染者，后来事态扩大，不論是否感染病毒一律予以射殺。Andy最終被人指引逃進一家商店，找到了Tammy。Doyle无法控制局势，於是只好選擇和Scarlet帶領Andy和Tammy等一群人逃離。Flynn聯繫了Doyle，表示Stone已經命令戰機投放[燃烧弹](../Page/燃烧弹.md "wikilink")，杀死所有的人。Doyle一隊人穿過河底隧道逃離第一區，但是大批感染者，包括Don，也逃脱了轰炸。隨後新的命令再次下達，授權軍隊使用化學武器消滅所有人。

Scarlet来到[摄政公园和Flynn的直升机会合](../Page/摄政公园.md "wikilink")，她告诉Doyle医治传染病的关键在于Andy，因为他可能有他妈妈一样的免疫能力。Flynn开[直升机来接Doyle](../Page/直升机.md "wikilink")，但是拒绝其他人。突然一个生還者冲了过来缠住直升機，Flynn不斷將这个生還者企圖赶下，并且用直升机的螺旋桨杀死了一群僵尸。他告诉Doyle去[温布利球场](../Page/温布利球场.md "wikilink")，Doyle便帶領僅剩的Scarlet和Tammy姐弟走進倫敦市區街頭，但此時毒氣開始擴散，四人躲進一輛車內躲避毒氣和僵尸。但是車子無法啟動，Doyle走出車外推動車子幫助汽車發動，而被火槍手追擊燒死。手里拿着Doyle的[M4步枪的Scarlet开车衝進了伦敦地铁来躲避攻擊他们的](../Page/M4步枪.md "wikilink")[AH-64阿帕契直昇機](../Page/AH-64阿帕契直昇機.md "wikilink")，那些孩子们步行跟着她。在黑暗中，她使用步枪的夜视功能。但是他们还是走散了，Scarlet遭到Don的埋伏而身亡。Don也攻击了Andy并且咬傷了他。Tammy开枪射击Don并拯救了Andy，但是他还是被感染了。Andy像他母亲一样没有症状，但是他的眼睛也是和她母亲一样出現感染後的出血症狀。后来他们来到[温布利球场](../Page/温布利球场.md "wikilink")，Flynn驾驶直升机帶走他們撤出，去往法國。28天後，三人下落不明。他們乘坐的已著陸的直升機上的對講機仍然開著，傳出一些人的求助。镜头切換，大批僵尸從地下隧道跑出來，穿過夏悠宮，面前則是巴黎鐵塔。

## 演員表

  - [蘿絲·拜恩](../Page/蘿絲·拜恩.md "wikilink")

  - [羅拔·卡奧爾](../Page/Robert_Carlyle.md "wikilink")

  -
  - [傑瑞米·雷納](../Page/Jeremy_Renner.md "wikilink")

  - [伊莫珍·蓋伊·波茨](../Page/Imogen_Gay_Poots.md "wikilink")

## 製作與發行

2006年9月1日，電影製片廠正式宣佈在倫敦拍演《28週後》。\[1\]

在放出這部電影之前，[美國電影協會向](../Page/美國電影協會.md "wikilink")《28週後》給予「R」的等級，表示電影內容涉及嚴重暴力和血腥鏡頭，以及一些言語不潔和裸露場面。電影在英國被評為["(18)"等級](../Page/18_certificate.md "wikilink")，而在香港則被評為III級（只准18歲以上人士觀看）。另外，電影已在美國公映了約2000次。但是片中的美國陸軍的部分軍人的裝備還要再真實一點，最好請真的軍隊來演。\[2\]

## 獎項

榮獲[美國](../Page/美國.md "wikilink")[電視台Spike](../Page/電視台.md "wikilink")
TV主辦的2007年度尖叫頒獎禮{{〈}}**Scream Awards**{{〉}}最佳驚懼電影獎。\[3\]

## 參見

  - [28天毀滅倒數](../Page/28天毀滅倒數.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  -
  -
  -
  -
  -
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:喪屍片](../Category/喪屍片.md "wikilink")
[Category:英國科幻恐怖片](../Category/英國科幻恐怖片.md "wikilink")
[Category:英國驚悚片](../Category/英國驚悚片.md "wikilink")
[Category:巴黎背景電影](../Category/巴黎背景電影.md "wikilink")
[Category:倫敦取景電影](../Category/倫敦取景電影.md "wikilink")
[Category:威爾斯取景電影](../Category/威爾斯取景電影.md "wikilink")
[Category:倫敦背景電影](../Category/倫敦背景電影.md "wikilink")
[Category:2000年代科幻片](../Category/2000年代科幻片.md "wikilink")
[Category:2000年代恐怖片](../Category/2000年代恐怖片.md "wikilink")
[Category:2000年代驚悚片](../Category/2000年代驚悚片.md "wikilink")
[Category:文明崩潰後世界題材電影](../Category/文明崩潰後世界題材電影.md "wikilink")
[Category:傳染病題材電影](../Category/傳染病題材電影.md "wikilink")
[Category:病毒題材作品](../Category/病毒題材作品.md "wikilink")
[Category:20世纪福斯电影](../Category/20世纪福斯电影.md "wikilink")

1.
2.
3.  [火紅美瑾辣㷫尖叫盛典
    《變形金剛》掃五獎威盡](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20071022&sec_id=462&subsec_id=465&art_id=10318658)
    ，《[蘋果日報 (香港)](../Page/蘋果日報_\(香港\).md "wikilink")》，2007年10月22日，C13版。