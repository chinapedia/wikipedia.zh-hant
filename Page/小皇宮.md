[Petit-Palais-facade.jpg](https://zh.wikipedia.org/wiki/File:Petit-Palais-facade.jpg "fig:Petit-Palais-facade.jpg")

**小皇宮**（Petit Palais）現為**小皇宮美術館**。

## 建築

小皇宮由[建築師查理吉羅](../Page/建築師.md "wikilink")（Charles
Girault）建造，和[大皇宮一樣曾是](../Page/大皇宮.md "wikilink")[1900年萬國博覽會展覽場](../Page/1900年萬國博覽會.md "wikilink")。與許多人想像的相反，在原巴黎工業宮(Palais
de
l'Industrie)舊址上興建的小皇宮當年並不是作為一座只與[巴黎](../Page/巴黎.md "wikilink")[博覽會相關的臨時性建築物而設計的](../Page/博覽會.md "wikilink")。曾獲得[羅馬大獎的建築設計師查理吉羅於](../Page/羅馬.md "wikilink")1897年贏得小皇宮設計競獎，推出了一項造型上刻板，但在材料設計上卻又很現代的建築方案：小皇宮建築群形如一個規則梯形，圍繞一個內花園，構成一個雙層環圈，底層為大樓基座，設各類辦公室、貯藏室和工作室，陳展空間全部位於一層，並通過一座大型主樓梯進入。設計師試圖把矗立於[塞納河對岸的](../Page/塞納河.md "wikilink")[榮軍院的圓頂與](../Page/榮軍院.md "wikilink")[凡爾賽宮鏡廊](../Page/凡爾賽宮.md "wikilink")（Galerie
des Glaces）的風格揉合在一起，而使用的卻是磚石、鑄鐵和玻璃等當時尚屬現代的材料。

為了克服作品只能受日光射照這一強制性約束，吉羅使用了許多大膽手法，例如：玻璃天棚、大玻璃觀景窗、朝內花園敞開的列柱廊（péristyle）等；從各處大量湧入的光線使整座小皇宮通體透亮，宛如一曲“光亮的頌歌”。同時，作為建築藝術精華，小皇宮也曾因採用古典柱形和雕塑裝飾占主導地位而被視為兼容並蓄或“美術派”風格（sytle
éclectique ou beaux arts）的宣言。

1901年[法國](../Page/法國.md "wikilink")[政府將這棟建築物送給](../Page/政府.md "wikilink")[巴黎市](../Page/巴黎市.md "wikilink")[政府](../Page/政府.md "wikilink")，從此小皇宮就成為[巴黎市政府的美術宮殿](../Page/巴黎市.md "wikilink")，為[巴黎](../Page/巴黎.md "wikilink")15所市政府[博物館之一](../Page/博物館.md "wikilink")。

## 內部

小皇宮內部的繪畫裝飾完成於1903年至1925年間。裝飾設計師的意圖是使小皇宮博物館既具備宮殿的華麗，又有公共[建築的某種莊嚴](../Page/建築.md "wikilink")。宮內各主體建築裝飾畫以“藝術之都[巴黎](../Page/巴黎.md "wikilink")”為總主題。

[巴黎市議會選擇了一些在名勝裝飾方面經驗豐富的成名](../Page/巴黎市.md "wikilink")[藝術家](../Page/藝術家.md "wikilink")，承擔小皇宮各部分的裝飾。

例如，大門正廳裝飾畫出自阿爾貝爾·貝斯納（Albert
BESNARD）之手（1903年定制，1910年完成），四幀壁板畫面象徵“人類思想狀態與自然力量：藝術的源泉”。

北側長廊的3幅天花板頂飾畫和10幅壁板畫分別敘述[巴黎從呂岱斯](../Page/巴黎.md "wikilink")（Lutèce）直至現代的發展歷史，均為費爾南·科爾蒙（Fernand
CORMON）的作品（1906年定制，1911年完成）。

南側長廊以“摩登[巴黎](../Page/巴黎.md "wikilink")”為題的3幅天花板頂飾畫由亨利·羅爾（Henri
Roll）完成於1920年。

南北閣樓的幾幅天花板頂飾畫分別由喬治·皮卡爾（Georges PICARD）和費爾迪南·恩貝爾（Ferdinand
HUMBERT）創作，其畫題分別是：“女性的勝利”和“[巴黎的知識成就](../Page/巴黎.md "wikilink")”；杜督伊圓頂（Coupole
Dutuit）裝飾畫則由莫利斯·德尼（Maurice
DENIS）繪製，畫題是“[法國](../Page/法國.md "wikilink")[藝術史](../Page/藝術史.md "wikilink")”，定購於1919年，完成於1925年。

除[繪畫外](../Page/繪畫.md "wikilink")，大多具有新[巴洛克風格的雕塑在小皇宮](../Page/巴洛克.md "wikilink")[博物館的內部裝飾中也佔居重要地位](../Page/博物館.md "wikilink")。此外，曾由察理·吉羅親自繪圖設計的鐵飾（ferronnerie）也是小皇宮[建築裝飾的一個重要因素](../Page/建築.md "wikilink")，尤其是那扇深受新[藝術風格影響的正面鐵柵欄大門](../Page/藝術.md "wikilink")，歷來被視為[法國](../Page/法國.md "wikilink")[建築物鐵飾](../Page/建築物.md "wikilink")[藝術的傑作之一](../Page/藝術.md "wikilink")。

隨著時間的推移，小皇宮[建築也不可避免地經歷了由盛而衰的變遷](../Page/建築.md "wikilink")。除[建築材料的自然質變之外](../Page/建築.md "wikilink")，功能佈局的變換也使小皇宮逐漸失去了起初給人的那種“透明感”：宮內有的窗戶被封死，有些長廊不是被分隔，就是被加裝吊頂。

## 整修

到了1990年代，年久失修的小皇宮出現嚴重的建築紊亂，懸空花園下陷、地基滲水、表面牆磚跌落，也日漸遭受一定程度的冷落或遺忘；公眾雖然常被小皇宮舉辦的臨時展覽所吸引，但卻大多不知道宮內豐富的常設展品：因為許多藏品不是因陳列條件不理想，便是由於缺少陳展空間而常年沈睡庫房，不得與觀眾見面。

為了改變這一狀況，拯救小皇宮博物館，[巴黎市](../Page/巴黎市.md "wikilink")[議會於](../Page/議會.md "wikilink")1997年通過了一項由[法國文物保管主任官員](../Page/法國.md "wikilink")（conservateur
général）、小皇宮[博物館館長吉爾](../Page/博物館.md "wikilink")·夏查爾（Gilles
CHAZAL）提出的文化計劃；1998年5月，[巴黎市](../Page/巴黎市.md "wikilink")[政府文化遺產與建築局推出小皇宮整體翻新](../Page/政府.md "wikilink")[建築設計競獎](../Page/建築.md "wikilink")；1999年，評審委員會選定謝（Chaix）與莫萊爾（Morel）建築設計工作室的方案。

2001年，小皇宮博物館對公眾；2003年，整修工程正式啟動。按標書規定，設計師須達到的強制性目標有三：一是恢復原始建築－包括光亮在內－的優點，二為贏得額外空間，三是[博物館陳展形式的現代化](../Page/博物館.md "wikilink")。承擔整修的[建築設計師們通過打破隔板和吊頂](../Page/建築.md "wikilink")，拆除窗戶擋板，恢復穹頂採光，使小皇宮再現通體透亮的原始特性；同時，還大膽對地下層、底層和頂層作出重新規劃佈局，不僅使展室面積從原來的15,000[平方米增加到了](../Page/平方米.md "wikilink")22,000[平方米](../Page/平方米.md "wikilink")(常設展廳陳列作品由850件增加至1300件)，而且還新增一個187座的報告廳、一間書店和一個咖啡廳。

整修工程耗資7千2百萬[歐元](../Page/歐元.md "wikilink")（全部由[巴黎市](../Page/巴黎市.md "wikilink")[政府承擔](../Page/政府.md "wikilink")）、持續了2年多的小皇宮博物館修繕工程於2005年6月份竣工。

2005年12月8日，[巴黎市](../Page/巴黎市.md "wikilink")[市長](../Page/市長.md "wikilink")[贝特朗·德拉诺埃](../Page/贝特朗·德拉诺埃.md "wikilink")（Bertrand
DELANOE）主持了整修一新的小皇宮博物館的重新開館儀式。

在四年並經過兩年多整體翻修工程之後，[巴黎小皇宮](../Page/巴黎.md "wikilink")[博物館](../Page/博物館.md "wikilink")(Petit
Palais)已於2005年12月10日以嶄新面貌重新對公眾開放。

## 展覽

由於自2001年以來，[左派主政的](../Page/左派.md "wikilink")[巴黎市](../Page/巴黎市.md "wikilink")[政府推行](../Page/政府.md "wikilink")“讓人人享受文化(culture
pour
tous)”政策，因此凡屬[巴黎市](../Page/巴黎市.md "wikilink")[政府](../Page/政府.md "wikilink")[博物館的常設展品館](../Page/博物館.md "wikilink")，一概對所有人免費開放，13歲以下的[兒童還可免費參觀各類臨時展覽](../Page/兒童.md "wikilink")。

小皇宮藏有非常多的[美術作品](../Page/美術.md "wikilink")，雖然以[法國](../Page/法國.md "wikilink")[美術品為主](../Page/美術.md "wikilink")，但永久展覽館仍收集不少[荷蘭](../Page/荷蘭.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[義大利的](../Page/義大利.md "wikilink")[藝術家作品](../Page/藝術家.md "wikilink")。小皇宮展場以年代劃分展區，1樓陳列18、19世紀和1900年代[巴黎](../Page/巴黎.md "wikilink")[繪畫](../Page/繪畫.md "wikilink")[藝術作品](../Page/藝術.md "wikilink")；地下1樓的館藏相當豐富，從[古希臘和](../Page/古希臘.md "wikilink")[羅馬帝國時期的文物](../Page/羅馬帝國.md "wikilink")、東西方[基督教世界文物](../Page/基督教.md "wikilink")、[文藝復興時期](../Page/文藝復興.md "wikilink")，到17、19世紀的[巴黎](../Page/巴黎.md "wikilink")[繪畫或](../Page/繪畫.md "wikilink")[雕塑](../Page/雕塑.md "wikilink")。

19世紀的藝術品，[浪漫主義的](../Page/浪漫主義.md "wikilink")[傑利柯及](../Page/傑利柯.md "wikilink")[德拉克洛瓦](../Page/德拉克洛瓦.md "wikilink")，[新古典主義的](../Page/新古典主義.md "wikilink")[安格爾](../Page/安格爾.md "wikilink")，[寫實主義庫爾培](../Page/寫實主義.md "wikilink")，[巴比松畫派](../Page/巴比松畫派.md "wikilink")（Barbizon）及[印象派](../Page/印象派.md "wikilink")（Impressionism）畫作，[新藝術及](../Page/新藝術.md "wikilink")[象徵主義](../Page/象徵主義.md "wikilink")[畫家魯東或](../Page/畫家.md "wikilink")[雕塑家卡波](../Page/雕塑家.md "wikilink")（Carpeaux）及[達魯](../Page/達魯.md "wikilink")（Dalou）等。小皇宮還有[中世紀文物](../Page/中世紀.md "wikilink")、[文藝復興](../Page/文藝復興.md "wikilink")[繪畫](../Page/繪畫.md "wikilink")、[素描](../Page/素描.md "wikilink")、[彩陶及](../Page/彩陶.md "wikilink")18世紀文物。

## 相關條目

  - [世界博覽會](../Page/世界博覽會.md "wikilink")

## 外部連結

  - [巴黎大小皇宮 Grand et Petit
    Palais](http://www.paris-my-city.com/paris/Palais/p1.html)

[Category:巴黎建筑物](../Category/巴黎建筑物.md "wikilink")