**Nucleus即時作業系統**（Nucleus RTOS）是Mentor Graphics旗下Accelerated
Technology公司所推出的嵌入式作業系統。Nucleus的好處是程式師不用再撰寫板支持套裝軟體（[BSP](../Page/BSP.md "wikilink")）。目前Nucleus
RTOS已支持USB OTG。

## 組成

### 核心

  - 实时內核
  - [C++和](../Page/C++.md "wikilink")[POSIX接口](../Page/POSIX.md "wikilink")
  - 動態下載

### 連結

  - [USB 3.0主机](../Page/USB_3.0.md "wikilink")
  - [USB 2.0主机](../Page/USB_2.0.md "wikilink")
  - 功能和OTG堆栈
  - 类驱动程序
  - 多媒体传输（[MTP和](../Page/媒体传输协议.md "wikilink")[PictBridge](../Page/PictBridge.md "wikilink")）
  - [PCI和](../Page/外设组件互连标准.md "wikilink")[PCI-X](../Page/PCI-X.md "wikilink")
  - [CAN和](../Page/控制器區域網路.md "wikilink")[CANopen](../Page/CANopen.md "wikilink")

### 網路

擁有超過60種網路驅動程式與協定，包含[TCP/IP
stack](../Page/TCP/IP协议族.md "wikilink")，[IPv6](../Page/IPv6.md "wikilink")
and [IEEE 802.11](../Page/IEEE_802.11.md "wikilink") wireless

### 檔案系統

  - [FAT](../Page/FAT.md "wikilink")（檔案配置表）
  - [ISO 9660](../Page/ISO_9660.md "wikilink")
    [CD-ROM](../Page/CD-ROM.md "wikilink")
  - [虛擬檔案系統](../Page/虛擬檔案系統.md "wikilink")（Virtual file
    system）[API](../Page/應用程式介面.md "wikilink")

### 圖形

  - 低階生成（Low-level rendering）
  - 全視窗系統（Full windowing system）

### 安全

[加密](../Page/加密.md "wikilink")（Encryption）、[雜湊](../Page/雜湊.md "wikilink")（hash）和[签名算法](../Page/签名算法.md "wikilink")（signature
algorithms）、[密钥交换协议](../Page/密钥交换协议.md "wikilink")

## 参考资料

## 外部連結

  - [Nucleus
    RTOS](http://www.mentor.com/products/embedded_software/nucleus_rtos/kernels/index.cfm)

[Category:ARM 操作系统](../Category/ARM_操作系统.md "wikilink")
[Category:嵌入式操作系统](../Category/嵌入式操作系统.md "wikilink")
[Category:微內核](../Category/微內核.md "wikilink")
[Category:專有作業系統](../Category/專有作業系統.md "wikilink")
[Category:实时操作系统](../Category/实时操作系统.md "wikilink")