**五指狸藻**（[學名](../Page/學名.md "wikilink")：***Utricularia
pentadactyla***）為[狸藻屬](../Page/狸藻屬.md "wikilink")[一年生小型](../Page/一年生植物.md "wikilink")[陆生](../Page/陆生植物.md "wikilink")[食虫植物](../Page/食虫植物.md "wikilink")。其[种加词](../Page/种加词.md "wikilink")“*pentadactyla*”来源于[希腊文](../Page/希腊文.md "wikilink")“*penta-*”和“*daktylos*”，意为“五”和“手指”，指其五指状的花朵。五指狸藻原產於[非洲熱帶](../Page/非洲.md "wikilink")，存在于[安哥拉](../Page/安哥拉.md "wikilink")、[蒲隆地](../Page/蒲隆地.md "wikilink")、[剛果民主共和國](../Page/剛果民主共和國.md "wikilink")、[衣索比亞](../Page/衣索比亞.md "wikilink")、[肯亞](../Page/肯亞.md "wikilink")、[馬拉威](../Page/馬拉威.md "wikilink")、[蘇丹](../Page/蘇丹共和國.md "wikilink")、[坦尚尼亞](../Page/坦尚尼亞.md "wikilink")、[烏干達](../Page/烏干達.md "wikilink")、[尚比亞以及](../Page/尚比亞.md "wikilink")[辛巴威](../Page/辛巴威.md "wikilink")。五指狸藻陆生于在潮濕、砂质或[泥炭质草原或岩石表面的浅表土壤中](../Page/泥炭.md "wikilink")，海拔分布范围为1500米至2100米。1954年，[彼得·泰勒最先发表了五指狸藻的描述](../Page/彼得·泰勒_\(植物學家\).md "wikilink")。其也是彼得·泰勒描述的第一个狸藻属物种。\[1\]

## 参考文献

## 外部連結

  - 食虫植物照片搜寻引擎中[五指狸藻的照片](http://www.cpphotofinder.com/utricularia-pentadactyla-268.html)

[Utricularia pentadactyla](../Category/非洲食虫植物.md "wikilink")
[Category:安哥拉植物](../Category/安哥拉植物.md "wikilink")
[Category:布隆迪植物](../Category/布隆迪植物.md "wikilink")
[Category:刚果民主共和国植物](../Category/刚果民主共和国植物.md "wikilink")
[Category:埃塞俄比亚植物](../Category/埃塞俄比亚植物.md "wikilink")
[Category:肯尼亚植物](../Category/肯尼亚植物.md "wikilink")
[Category:马拉维植物](../Category/马拉维植物.md "wikilink")
[Category:苏丹植物](../Category/苏丹植物.md "wikilink")
[Category:坦桑尼亚植物](../Category/坦桑尼亚植物.md "wikilink")
[Category:乌干达植物](../Category/乌干达植物.md "wikilink")
[Category:赞比亚植物](../Category/赞比亚植物.md "wikilink")
[Category:津巴布韦植物](../Category/津巴布韦植物.md "wikilink")
[pentadactyla](../Category/狸藻屬.md "wikilink")
[pentadactyla](../Category/瓮盘狸藻组.md "wikilink")

1.  Taylor, Peter. (1989). *[The genus Utricularia - a taxonomic
    monograph](../Page/狸藻属——分类学专著.md "wikilink")*. Kew Bulletin
    Additional Series XIV: London.