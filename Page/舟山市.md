**舟山市**（[吴语拼音](../Page/吴语.md "wikilink"): Cieusae
Zy）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[浙江省下辖的](../Page/浙江省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，其行政区域范围为整个[舟山群岛](../Page/舟山群岛.md "wikilink")。群岛之中，以[舟山岛最大](../Page/舟山岛.md "wikilink")，其“形如舟楫”，故名舟山。

## 歷史

### 古代

春秋时，舟山属[越](../Page/越國.md "wikilink")，称“甬东”（[甬江之东](../Page/甬江.md "wikilink")），又喻称“海中洲”。

公元738年，唐[开元二十六年](../Page/开元.md "wikilink")，置县，以境内有翁山而命名为“翁山县”，史上群岛置县之始。至公元771年，[大历六年](../Page/大历.md "wikilink")，翁山县因被[袁晁率起义军占领而被撤废县治](../Page/袁晁.md "wikilink")。

公元1073年，北宋熙宁六年，重新置縣，名昌國縣。「意其东控日本，北接登莱，南连瓯闽，西通吴会，实海中之巨障，足以昌壮国势焉。」

[元朝初年升格为昌國州](../Page/元朝.md "wikilink")。

[明朝](../Page/明朝.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")(1369)又降为昌國縣，二十年更罢县。洪武二十年将舟山岛城区和镇外的居民和其他46山(岛)的居民徙迁内陆。其後，浙江海商及葡萄牙人，建立港口。公元1548年，明[嘉靖二十七年](../Page/嘉靖.md "wikilink")，浙江巡抚[朱纨派都指挥卢镗率兵进攻舟山](../Page/朱纨.md "wikilink")[佛渡岛](../Page/佛渡岛.md "wikilink")[双屿港](../Page/双屿港.md "wikilink")，港口以木石淤塞，城市被大火烧光。数百葡萄牙人被杀，侥幸活命的乘船逃走。

[清朝](../Page/清朝.md "wikilink")[康熙取得](../Page/康熙.md "wikilink")[台湾后](../Page/台湾.md "wikilink")，沿海解禁，舟山群岛人口日增。公元1687年，清康熙二十六年，再次设县，因认为“舟”字动而不稳，定名为定海县。「海定则波宁」,
以祈「海波永定」。定海之名称实际移自浙江大陆，为此原定海县同时更名为鎮海縣（即今宁波市[镇海区的前身](../Page/镇海区_\(宁波市\).md "wikilink")）。

### 近代

[第一次鸦片战争期间](../Page/第一次鸦片战争.md "wikilink")，卽1840年至1842年，英军兩度占领宁波的[定海](../Page/定海区.md "wikilink")。第一次英軍不攻廣州，轉攻沿海各城，及攻陷舟山。後清庭打發回廣州海域等候撤軍。第二次因雙方不滿[穿鼻草約](../Page/穿鼻草約.md "wikilink")，而重新開戰。北上攻陷沿岸各城，再佔舟山，攻到江寧城外，清廷議和。兩次英軍以清制，[自行設衙門管治](../Page/定海军政府.md "wikilink")。按[江寧條約所定](../Page/南京条约.md "wikilink")，道光二十六年（1846年）英军撤军。

鸦片战争之後，[道光二十一年](../Page/道光.md "wikilink")（1841年），海防紧張，定海县升格为定海直隸厅，直属浙江省。

[中華民國成立後](../Page/中華民國.md "wikilink")，废厅改县，復改定海縣。归属于宁波市管辖。

1937年日本全面侵華，舟山落入日軍之手。1942年10月1日，[日本軍船](../Page/日本.md "wikilink")[里斯本丸运](../Page/里斯本丸.md "wikilink")[英国战俘往日本](../Page/英国.md "wikilink")，中途在舟山被[美国](../Page/美国.md "wikilink")[太平洋舰队](../Page/美國太平洋艦隊.md "wikilink")[潜艇部队第](../Page/潜艇.md "wikilink")81分队的“鲈鱼”号发射[鱼雷击中](../Page/鱼雷.md "wikilink")，\[1\]
日軍棄船，千多名英军战俘遇害。 \[2\] 當地[中国渔民義救出](../Page/中国.md "wikilink")300多名英国军人。

1945年，日本戰敗投降，舟山復歸中華民國。1949年，民国三十八年，因战时需要，定海县分为定海、滃州（岱山县前身）两县，[马迹山以北苏属各岛设立](../Page/马迹山.md "wikilink")[江苏省](../Page/江蘇省_\(中華民國\).md "wikilink")[嵊泗县](../Page/嵊泗县.md "wikilink")。1950年5月13日夜至16日拂晓，驻舟山的[中华民国国军秘密自舟山撤退](../Page/中华民国国军.md "wikilink")，滃州县实际解散，是爲[舟山撤退](../Page/舟山撤退.md "wikilink")。

[中華人民共和國成立后](../Page/中華人民共和國.md "wikilink")，[中國共產黨進占舟山群岛](../Page/中國共產黨.md "wikilink")，成立定海县人民政府，属宁波专区管辖。1953年3月经政务院批准，定海县辖区分为定海县、普陀县、[岱山县](../Page/岱山县.md "wikilink")3县，并从江苏省划入嵊泗县，成立舟山专区（为今日舟山地级市的前身）。1954年[象山县自](../Page/象山县.md "wikilink")[宁波专区划入](../Page/宁波市.md "wikilink")，但在1958年转划归[台州专区](../Page/台州专区.md "wikilink")。1959年撤定海、普陀、岱山、嵊泗县，合并成立舟山县，舟山专区仅辖舟山1县。1960年1月4日，舟山专区撤消，舟山县改属宁波专区。同年11月，所属嵊泗人民公社划归[上海市](../Page/上海市.md "wikilink")。1962年5月重新设立[舟山专区](../Page/舟山专区.md "wikilink")，并撤销舟山县，改分为定海、普陀、岱山、[大衢](../Page/大衢县.md "wikilink")、嵊泗（自上海归还后设县）5县。1964年大衢县撤销，其辖区分别划归岱山、嵊泗2县。1967年3月起舟山专区改称[舟山地区](../Page/舟山地区.md "wikilink")。

1987年1月，经国务院批准，撤销舟山地区和定海、普陀2县，成立舟山市，辖2区（[定海区](../Page/定海区.md "wikilink")、[普陀区](../Page/普陀区_\(舟山市\).md "wikilink")）2县（岱山县、嵊泗县），实行以市领导区、县新体制。\[3\]

2017年4月，[浙江自贸区成立](../Page/浙江自贸区.md "wikilink")，涵盖舟山离岛片区78.98平方公里，舟山岛北部片区15.62平方公里，舟山岛南部片区25.35平方公里共三个片区119.95平方公里\[4\]。

## 地理

  - 位置：位于[浙江省](../Page/浙江省.md "wikilink")[舟山群岛](../Page/舟山群岛.md "wikilink")，地处中国长江口南侧，[杭州湾外的东海海域](../Page/杭州湾.md "wikilink")，是中国第一个以群岛设市的地级行政区划。行政范围为舟山群岛全部，共有大小岛屿1391个，其中有人居住的岛屿有103个。
  - 面积：区域总面积2.22万平方公里，其中陆地面积1440.12平方公里
  - 行政中心：临城新区行政大楼

### 气候

舟山气候夏季高温冬季温和，四季分明，海洋性较强，季风显著，属于[副热带季风气候](../Page/副热带季风气候.md "wikilink")。最冷（1月）平均气温5.8℃，最热（8月）平均气温27.1℃，年平均气温16.4℃。极端最高气温42.3℃（2013年8月8日），极端最低气温-6.1℃（1955年1月16日、1958年1月16日）。年降水量1442.5毫米。

### 主要海岛

[1962-07_1962年_浙江舟山渔港.jpg](https://zh.wikipedia.org/wiki/File:1962-07_1962年_浙江舟山渔港.jpg "fig:1962-07_1962年_浙江舟山渔港.jpg")

  - [舟山岛](../Page/舟山岛.md "wikilink")
  - [岱山岛](../Page/岱山岛.md "wikilink")
  - [六横岛](../Page/六横岛.md "wikilink")
  - [金塘岛](../Page/金塘岛.md "wikilink")
  - [衢山岛](../Page/衢山岛.md "wikilink")
  - [大长涂山](../Page/大长涂山.md "wikilink")
  - [小长涂山](../Page/小长涂山.md "wikilink")
  - [朱家尖岛](../Page/朱家尖岛.md "wikilink")
  - [桃花岛](../Page/桃花島.md "wikilink")
  - [秀山岛](../Page/秀山岛.md "wikilink")
  - [泗礁山](../Page/泗礁山.md "wikilink")
  - [普陀山](../Page/普陀山.md "wikilink")

## 政治

### 现任领导

<table>
<caption>舟山市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党舟山市委员会.md" title="wikilink">中国共产党<br />
舟山市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/舟山市人民代表大会.md" title="wikilink">舟山市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/舟山市人民政府.md" title="wikilink">舟山市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议舟山市委员会.md" title="wikilink">中国人民政治协商会议<br />
舟山市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/俞东来.md" title="wikilink">俞东来</a>[5]</p></td>
<td><p><a href="../Page/钟达.md" title="wikilink">钟达</a>[6]</p></td>
<td><p><a href="../Page/何中伟.md" title="wikilink">何中伟</a>[7]</p></td>
<td><p><a href="../Page/江建国.md" title="wikilink">江建国</a>[8]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/浙江省.md" title="wikilink">浙江省</a><a href="../Page/杭州市.md" title="wikilink">杭州市</a></p></td>
<td><p>浙江省<a href="../Page/宁波市.md" title="wikilink">宁波市</a></p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/赵县.md" title="wikilink">赵县</a></p></td>
<td><p><a href="../Page/江苏省.md" title="wikilink">江苏省</a><a href="../Page/泗阳县.md" title="wikilink">泗阳县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年2月</p></td>
<td><p>2012年2月</p></td>
<td><p>2018年2月</p></td>
<td><p>2014年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

舟山群岛史上曾三度建县，两度被废。三次设县时期，均隶属浙江省大陆的明州（或宁波府）管辖。

目前，舟山市辖2个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")。2005年舟山市建立[渔农村新型社区](../Page/渔农村新型社区.md "wikilink")，在一定程度上行使村民委员会的公共管理和服务功能。

  - 市辖区：[定海区](../Page/定海区.md "wikilink")、[普陀区](../Page/普陀区_\(舟山市\).md "wikilink")
  - 县：[岱山县](../Page/岱山县.md "wikilink")、[嵊泗县](../Page/嵊泗县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>舟山市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[9]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>330900</p></td>
</tr>
<tr class="odd">
<td><p>330902</p></td>
</tr>
<tr class="even">
<td><p>330903</p></td>
</tr>
<tr class="odd">
<td><p>330921</p></td>
</tr>
<tr class="even">
<td><p>330922</p></td>
</tr>
<tr class="odd">
<td><p>注：普陀区数字包含普陀山风景名胜区所辖普陀山镇。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>舟山市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[10]（2010年11月）</p></th>
<th><p>户籍人口[11]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>舟山市</p></td>
<td><p>1121261</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>定海区</p></td>
<td><p>464184</p></td>
<td><p>41.40</p></td>
</tr>
<tr class="even">
<td><p>普陀区</p></td>
<td><p>378805</p></td>
<td><p>33.78</p></td>
</tr>
<tr class="odd">
<td><p>岱山县</p></td>
<td><p>202164</p></td>
<td><p>18.03</p></td>
</tr>
<tr class="even">
<td><p>嵊泗县</p></td>
<td><p>76108</p></td>
<td><p>6.79</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")112.1万人\[12\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加11.98万人，增长11.95%，年平均增长率为1.14%。其中，男性人口为58.84万人，占52.48%；女性人口为53.29万人，占47.52%。总人口性别比（以女性为100）为110.43。0－14岁人口为11.43万人，占10.19%；15－59岁人口为83.07万人，占74.08%；60岁及以上人口为17.63万人，占15.73%，其中65岁及以上人口为11.78万人，占10.50%。居住在城镇的人口为71.31万人，占63.59%；居住在乡村的人口为40.82万人，占36.41%。
根据2015年舟山市统计年鉴，全市[常住人口为](../Page/常住人口.md "wikilink")115.2万人。

## 交通

[20090606_Putuoshan_8807.jpg](https://zh.wikipedia.org/wiki/File:20090606_Putuoshan_8807.jpg "fig:20090606_Putuoshan_8807.jpg")
[Jintang_Bridge-3.jpg](https://zh.wikipedia.org/wiki/File:Jintang_Bridge-3.jpg "fig:Jintang_Bridge-3.jpg")\]\]

  - 海运
    是主要的对外交通渠道，有[定海](../Page/定海_\(城区\).md "wikilink")、[沈家门](../Page/沈家门.md "wikilink")、[高亭](../Page/高亭.md "wikilink")、[泗礁等客运港口](../Page/菜园.md "wikilink")，有班轮通[上海](../Page/上海.md "wikilink")、[宁波](../Page/宁波.md "wikilink")、[温州](../Page/温州.md "wikilink")、[福州等外埠](../Page/福州.md "wikilink")。
  - 公路
    公路通过车渡联运可以通浙江省内大部分地区。岛上交通四通八达，主要岛屿上有班车。向外有舟山跨海大桥（岑港大桥、响礁门大桥、桃夭门大桥、西堠门大桥、金塘大桥）与大陆连接。
  - 航空
    群岛现有民用机场1个，即[舟山普陀山机场](../Page/舟山普陀山机场.md "wikilink")，位于[朱家尖岛](../Page/朱家尖岛.md "wikilink")。在岱山岛上有军用机场，是国民政府退往台湾前修建。目前舟山正在计划开辟岛际直升飞机业务。
  - 高速公路
    舟山通过[甬舟高速公路](../Page/甬舟高速公路.md "wikilink")(G9211)与大陆相连。高速公路的主体部分为[舟山跨海大桥](../Page/舟山跨海大桥.md "wikilink")，由岑港大桥、响礁门大桥、桃夭门大桥、西堠门大桥和金塘大桥五座大桥组成，于2009年12月25日正式试通车。

## 旅游

舟山市有[国家级风景名胜区两个](../Page/国家级风景名胜区.md "wikilink")，[普陀山风景名胜区](../Page/普陀山风景名胜区.md "wikilink")(含[朱家尖](../Page/朱家尖岛.md "wikilink"))和[嵊泗列岛风景名胜区](../Page/嵊泗列岛风景名胜区.md "wikilink")。两个省级风景名胜区为[岱山岛风景区和](../Page/岱山岛风景区.md "wikilink")[桃花岛风景区](../Page/桃花岛风景区.md "wikilink")。

## 教育

[1963-05_1963年_舟山群岛气象站.jpg](https://zh.wikipedia.org/wiki/File:1963-05_1963年_舟山群岛气象站.jpg "fig:1963-05_1963年_舟山群岛气象站.jpg")

### 大学

[浙江海洋大学是舟山境内唯一总部设在舟山的](../Page/浙江海洋学院.md "wikilink")[本科高校](../Page/本科教育.md "wikilink")，[浙江大学海洋学院](../Page/浙江大学海洋学院.md "wikilink")（浙江大学舟山校区）亦在舟山境内，另有浙江国际海运职业技术学院，主要培养高职高专类海运人才。

### 高中

[舟山中学](../Page/舟山中学.md "wikilink") [普陀中学](../Page/普陀中学.md "wikilink")
[南海实验学校](../Page/南海实验学校.md "wikilink")
[普陀三中](../Page/普陀三中.md "wikilink")
[白泉高中](../Page/白泉高中.md "wikilink")
[定海一中](../Page/定海一中.md "wikilink")
[镇鳌中学](../Page/镇鳌中学.md "wikilink")
[舟山二中](../Page/舟山二中.md "wikilink")
[东海中学](../Page/东海中学.md "wikilink")
[开元中学](../Page/开元中学.md "wikilink")
[岱山中学](../Page/岱山中学.md "wikilink")
[大衢中学](../Page/大衢中学.md "wikilink")
[东沙中学](../Page/东沙中学.md "wikilink")
[嵊泗中学](../Page/嵊泗中学.md "wikilink") 六横中学 金塘中学

### 初中

南海实验学校 定海二中 定海三中 定海五中 定海六中 岑港中学 普陀二中 白泉中学 南海中学 沈家门一中 朱家尖中学 东海中学 开元中学
虾峙中学 台门中学 峧头中学 大衢中学 东港中学

### 职业学校

舟山职业技术学校 沈家门中学 舟山航海学校 凯灵中学 普陀职业教育中心 岱山县职业技术学校 嵊泗县职教中心

## 宗教

[普陀山是](../Page/普陀山.md "wikilink")[观音菩萨的道场](../Page/观音菩萨.md "wikilink")，所以舟山民众信奉[佛教的尤多](../Page/佛教.md "wikilink")。同时[道教也有一定基础](../Page/道教.md "wikilink")，各岛各村神庙林立。民众在佛事活动中，不称自己为“某某乡某某村人”，而称“某某地方某某庙界下弟子”。另外[基督教也有一定的传播](../Page/基督教.md "wikilink")，尤其是在[衢山岛上](../Page/衢山岛.md "wikilink")，基督教有上百年传教历史，该岛不少民众成为[基督徒](../Page/基督徒.md "wikilink")。

  - [普济寺](../Page/普济寺_\(普陀山\).md "wikilink")
  - [法雨寺](../Page/法雨寺_\(普陀山\).md "wikilink")
  - [慧济寺](../Page/慧济寺.md "wikilink")
  - [祖印寺](../Page/祖印寺.md "wikilink")
  - [不肯去观音院](../Page/不肯去观音院.md "wikilink")
  - [超果寺](../Page/超果寺.md "wikilink")

## 著名人物

  - [刘鸿生](../Page/刘鸿生.md "wikilink")
  - [金维映](../Page/金维映.md "wikilink")
  - [周劲羽](../Page/周劲羽.md "wikilink")
  - [乔石](../Page/乔石.md "wikilink")
  - [每個人](../Page/每個人.md "wikilink")
  - [三毛](../Page/三毛_\(作家\).md "wikilink")
  - [董浩云](../Page/董浩云.md "wikilink")
  - [董建华](../Page/董建华.md "wikilink")
  - [苗僑偉](../Page/苗僑偉.md "wikilink")
  - [王家卫](../Page/王家卫.md "wikilink")
  - [林郑月娥](../Page/林鄭月娥.md "wikilink")
  - [潘羿捷](../Page/潘羿捷.md "wikilink")

## 友好城市

舟山共有5个友好城市\[13\]。

1.  [里士满](../Page/列治文_\(加利福尼亞州\).md "wikilink")（友好城市）

2.  [宮城縣](../Page/宮城縣.md "wikilink")[气仙沼市](../Page/氣仙沼市.md "wikilink")（友好城市）

3.  [江华郡](../Page/江华郡.md "wikilink")（友好交流城市）

4.  [谷城郡](../Page/谷城郡.md "wikilink")（普陀友好交流城市）

5.  [泗川市](../Page/泗川市.md "wikilink")（单项友好交流城市）

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [舟山市人民政府](http://www.zhoushan.gov.cn)

{{-}}

{{-}}

[Category:浙江地级市](../Category/浙江地级市.md "wikilink")
[舟山](../Category/舟山.md "wikilink")
[浙](../Category/中国中等城市.md "wikilink")
[Category:東海沿海城市](../Category/東海沿海城市.md "wikilink")
[Category:长江三角洲城市](../Category/长江三角洲城市.md "wikilink")
[浙](../Category/国家卫生城市.md "wikilink")
[Category:1987年建立的行政區劃](../Category/1987年建立的行政區劃.md "wikilink")
[Category:浙江省省级历史文化名城](../Category/浙江省省级历史文化名城.md "wikilink")

1.

2.
3.  [舟山概况](http://www.zhoushan.gov.cn/renshizhoushan/zsgl.jsp?iInfoClass=926)
    ，舟山市人民政府官方网站，2011年7月16日查询

4.

5.

6.

7.

8.

9.

10.

11.

12.

13. \[<http://www.zhoushan.gov.cn/html/42637.html> **舟山市现有友城情况\] **