**Handel-C**是一個[程式語言](../Page/程式語言.md "wikilink")，是一個專門編譯用在[FPGA以及](../Page/FPGA.md "wikilink")[ASIC上的](../Page/ASIC.md "wikilink")[硬體描述語言](../Page/硬體描述語言.md "wikilink")。它是一個[C語言的子集](../Page/C語言.md "wikilink")，並且有一些非標準的控制硬體即時性以及平行性的特性。

為了要描述複雜的[演算法](../Page/演算法.md "wikilink")，C語言的子集包含了所有C語言常用的特性。像許多嵌入式編譯器，[浮點數資料型態都會被忽略掉](../Page/浮點數.md "wikilink")。透過外部函式庫的支援，浮點數運算會變的更有效率。

In order to facilitate a way to describe parallel behaviour some of the
[CSP](../Page/Communicating_sequential_processes.md "wikilink") keywords
are used, along with the general file structure of
[Occam](../Page/Occam_programming_language.md "wikilink")。

## 歷史

Handel-C是裡面的硬體編譯群組所發展一系列的硬體描述語言。大約在1996年早期，Handel HDL進化到Handel-C。

Handel-C在被ESL發表之後，被許多大學的硬體研究單位被採用，

其他的C
HDL子集也大約在同一時間被發展出來，像是1994年在[多倫多大學的Transmogrifier](../Page/多倫多大學.md "wikilink")
C（現在變為開源碼專案[FpgaC](../Page/FpgaC.md "wikilink")），還有在[Los Alamos National
Laboratory的Streams](../Page/Los_Alamos_National_Laboratory.md "wikilink")-C（現在已經得到[Impulse
Accelerated
Technologies的認可](../Page/Impulse_Accelerated_Technologies.md "wikilink")，並且重命名為[Impulse
C](../Page/Impulse_C.md "wikilink")）

## 外部連結

  - [Oxford
    Handel-C](http://citeseer.ist.psu.edu/rd/49527001%2C78794%2C1%2C0.25%2CDownload/http://citeseer.ist.psu.edu/cache/papers/cs/3848/ftp:zSzzSzftp.comlab.ox.ac.ukzSzpubzSzDocumentszSztechpaperszSzIan.PagezSzumist.pdf/page96hardwaresoftware.pdf)
  - [Agility Design Solutions](http://www.agilityds.com) Company behind
    the DK Design Suite, a Handel-C IDE
  - [Handel-C Language Reference
    Manual](https://web.archive.org/web/20100331015609/http://www.agilityds.com/literature/HandelC_Language_Reference_Manual.pdf)
    From Agility Design Solutions

[Category:C語言家族](../Category/C語言家族.md "wikilink")
[Category:硬件描述语言](../Category/硬件描述语言.md "wikilink")