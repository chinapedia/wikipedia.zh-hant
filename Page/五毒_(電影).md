為香港1978年[邵氏出品的一部武打電影](../Page/邵氏公司.md "wikilink")。

## 劇情簡介

五毒門為[江湖上一神祕且陰毒的門派](../Page/江湖.md "wikilink")，教中龍蛇混雜，教徒也並非全為善類，加之其武功陰毒，江湖上人人為之喪膽。然而，掌門人（屠龍飾）在臨終前自感本門子弟貽害江湖，於是命關門弟子楊得（江生飾）清理門戶，剷除為非作歹的師兄。五位師兄互不相識，各習[蜈蚣](../Page/蜈蚣.md "wikilink")、[蛇](../Page/蛇.md "wikilink")、[蠍子](../Page/蠍子.md "wikilink")、[壁虎以及](../Page/壁虎.md "wikilink")[蛤蟆中的一門武功](../Page/蛤蟆.md "wikilink")，小弟子雖得師父真傳，通曉五門武藝，但時日尚淺，因而必須得到一名師兄相助，方能對抗另外其他師兄。

小師弟依師父遺命首先尋找已退出江湖、持有五毒門贓款的老師叔，啟用贓款廣為善事；而眾師兄為奪贓款，必定亦潛伏左近。豈知在小師弟到達不久後，地方上即發生一起滅門血案，慘遭殺害的乃縣衙師爺原老夫子一家。小師弟在街坊間探查，遇得一兇案目擊證人萬發（劉晃世飾），逐與其將此事稟報衙門捕快何遠新（郭追飾）。何遠新實乃熟習壁虎功的五毒門四弟子，夥同五弟子蛤蟆功李豪（羅莽飾），欲尋得老師叔後以大筆財寶行善天下，在原家血案發生後，始知原來原老夫子即為五毒門眾弟子探訪已久的老師叔，而由仵作的檢驗，亦得知原家一家乃喪生於擅蜈蚣功的百足千手大師兄以及蛇功的靈蛇郎君二師兄手下。據目擊證人的證詞，兇手為常出沒於豪門公子洪文通（韋白飾）府上的湯山魁（鹿峰飾）。在李豪以蛤蟆功相助下，眾捕快成功捕獲使蜈蚣功的五毒門大弟子湯山魁。

然而使蛇功的二弟子洪文通，因懼湯山魁共出自己乃兇案同謀，和蠍子功三弟子商議後，重金勾結知縣（王龍威飾），支走何遠新，並以李豪替湯山魁結案。再縣衙上下多方密謀下，何遠新歸來後，才知原家命案已結，遭污陷為兇手的師弟李豪則喪生於牢獄之中，悲憤不已。而在此時，經多方觀察的小師弟亦已明瞭緣由，於是與四師兄相認。兩人於是勤加練武，終擊敗無惡不作的三位師兄，取回老師叔藏寶，更為師門清理門戶，替已喪生的李豪報仇。

## 人物角色

| 角色名稱 | 演員                                    | 備註                             |
| ---- | ------------------------------------- | ------------------------------ |
| 楊德   | [江生](../Page/江生.md "wikilink")        | 小師弟                            |
| 何遠新  | [郭追](../Page/郭追.md "wikilink")        | 捕快 四師兄壁虎                       |
| 馬騰   | [孫建](../Page/孫建.md "wikilink")        | 捕頭 三師兄蠍子                       |
| 李豪   | [羅莽](../Page/羅莽.md "wikilink")        | 江湖好漢 五師兄蛤蟆                     |
| 洪文通  | [韋白](../Page/韋白.md "wikilink")        | 靈蛇郎君 二師兄毒蛇                     |
| 湯山魁  | [鹿峰](../Page/鹿峰.md "wikilink")        | 百手千足 大師兄蜈蚣                     |
| 王大老爺 | [王龍威](../Page/王龍威.md "wikilink")      | 知縣                             |
| 原老夫子 | [谷峰](../Page/谷峰.md "wikilink")        | 縣衙師爺 老師叔                       |
|      | [狄威](../Page/狄威.md "wikilink")        | 五毒門掌門人                         |
| 林光   | [孫樹培](../Page/孫樹培.md "wikilink")      | 捕快                             |
| 萬發   | [劉晃世](../Page/劉晃世.md "wikilink")      | 客棧小二                           |
|      | [林輝煌](../Page/林輝煌.md "wikilink")      | 捕快                             |
|      | [王清河](../Page/王清河.md "wikilink")      | 捕快                             |
|      | [沈勞](../Page/沈勞.md "wikilink")        | 水果小販                           |
|      | [王憾塵](../Page/王憾塵.md "wikilink")      | [仵作](../Page/仵作.md "wikilink") |
|      | [蕭玉龍](../Page/蕭玉龍.md "wikilink")      | 捕快                             |
|      | 陳志樂                                   |                                |
|      | [譚鎮渡](../Page/譚鎮渡.md "wikilink")      |                                |
|      | 黎友興                                   |                                |
|      | 陳鴻                                    |                                |
|      | 周麥利                                   | 捕快                             |
|      | 夏國榮                                   | 捕快                             |
|      | [余太平](../Page/余太平.md "wikilink")      | 捕快                             |
|      | [楊雄](../Page/楊雄_\(演員\).md "wikilink") | 捕快                             |
|      | 劉準                                    | 獄卒                             |
|      | [杜永亮](../Page/杜永亮.md "wikilink")      |                                |
|      | 丁東                                    | 店伴                             |
|      | 馮明                                    | 店伴                             |
|      | 呂紅                                    | 原老夫子家人                         |
|      | 方茹                                    | 原老夫子家人                         |
|      |                                       |                                |

## 其他製作人員

  - 執行製片：陳列
  - 助導：[趙岡生](../Page/江生.md "wikilink")、陳友文
  - 武術指導：[梁挺](../Page/梁挺.md "wikilink")、[鹿峰](../Page/鹿峰.md "wikilink")、戴其賢
  - 劇務：黃錦盤
  - 燈光：陳芬、關英全
  - 錄音：王永華
  - 置景：曹莊生
  - 服裝：劉季友
  - 道具：黎沃、劉炳威
  - 化妝：吳緒清
  - 梳妝：彭雁聯

## 相關條目

  - [五毒](../Page/五毒_\(消歧義\).md "wikilink")

## 備註

<references/>

## 外部連結

  - {{@movies|ffhk20077559|五毒}}

  -
  -
  -
  -
  -
  -
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:功夫片](../Category/功夫片.md "wikilink")
[8](../Category/1970年代香港電影作品.md "wikilink")
[Category:张彻电影](../Category/张彻电影.md "wikilink")