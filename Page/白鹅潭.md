[Bai_e_tan.jpg](https://zh.wikipedia.org/wiki/File:Bai_e_tan.jpg "fig:Bai_e_tan.jpg")，正前方为[芳村](../Page/芳村.md "wikilink")，右方为[沙面](../Page/沙面.md "wikilink")[白天鹅宾馆](../Page/白天鹅宾馆.md "wikilink")
\]\]
**白鹅潭**，又簡稱**鵝潭**，位于[中国](../Page/中国.md "wikilink")[广州市](../Page/广州市.md "wikilink")[沙面岛以南的](../Page/沙面岛.md "wikilink")[珠江河面上](../Page/珠江.md "wikilink")，是西航道、前航道、后航道等3段[珠江](../Page/珠江.md "wikilink")[广州河道的交汇处](../Page/广州.md "wikilink")。古人称之为“巨浸”。该段河面宽阔，水深流急，烟波浩淼，风景秀丽，入夜，清风送爽，月色朗朗。“鹅潭夜月”被评为1963年的[羊城八景之一](../Page/羊城八景.md "wikilink")\[1\]。广州市政府通常在此举行[烟花汇演](../Page/烟花.md "wikilink")。

## 传说及由来

相传在[明朝](../Page/明朝.md "wikilink")[正统年间](../Page/正统_\(年号\).md "wikilink")，[广东](../Page/广东.md "wikilink")[南海连接发生几场天灾](../Page/南海区.md "wikilink")，人民生活困苦。[正统十三年](../Page/正统_\(年号\).md "wikilink")(1448年)九月，一名叫[黄萧养的青年发动起义](../Page/黄萧养.md "wikilink")，聚集了万余人向广州进攻。次年六月，起义军在白鹅潭的江面上打败了前来镇压的官兵。传说在这次战斗中，两只经常在江面游弋的大白[鹅为起义军引航导路](../Page/鹅.md "wikilink")。后来因寡不敌众，起义被[明朝政府镇压](../Page/明朝.md "wikilink")，[黄萧养亦战死](../Page/黄萧养.md "wikilink")。民间则传说当[黄萧养撤退到](../Page/黄萧养.md "wikilink")[珠江边的紧急关头](../Page/珠江.md "wikilink")，两只大白[鹅从江心浮出背着](../Page/鹅.md "wikilink")[黄萧养向江心游去](../Page/黄萧养.md "wikilink")，消失在大雾中。以后人们就根据这个传说把这段珠江河面称为**白鹅潭**。

事实没有什麼传说，只是当地漁民喜欢在那养白鹅，因此就叫**白鹅潭**了。

## 历史

**白鹅潭**是[珠江流经](../Page/珠江.md "wikilink")[广州最宽和水位最深的河面](../Page/广州.md "wikilink")，自古以来是[广州对外通商的重要交通水道](../Page/广州.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[道光年间](../Page/道光.md "wikilink")，白鹅潭一带有数千[花艇聚集于此](../Page/花艇.md "wikilink")，热闹非常。[花艇装饰清雅秀丽](../Page/花艇.md "wikilink")，船上莺歌燕舞，笙歌彻夜。

[清代時歸屬](../Page/清代.md "wikilink")[廣州府](../Page/广州府.md "wikilink")[南海縣](../Page/南海縣.md "wikilink")[順德協右營](../Page/順德.md "wikilink")（駐防）管轄\[2\]。

[第二次鸦片战争爆发后](../Page/第二次鸦片战争.md "wikilink")，[沙面被劃为](../Page/沙面.md "wikilink")[英](../Page/广州英租界.md "wikilink")、[法](../Page/广州法租界.md "wikilink")[租界](../Page/租界.md "wikilink")，白鹅潭一帶成为外国舰只避风停泊的港湾。

1949年以后，当时白鹅潭上仍有很多[疍民以船为家](../Page/疍民.md "wikilink")。1950年代，[周恩来总理决定由中央拨款兴建住宅](../Page/周恩来.md "wikilink")，陆续安排[疍民上岸定居](../Page/疍民.md "wikilink")。至80年代改革開放，由於興建了白天鵝賓館，白鵝潭自此作為[春節燃放煙火的地方](../Page/春節.md "wikilink")。

## 周边

  - [白天鹅宾馆](../Page/白天鹅宾馆.md "wikilink")
  - [黄沙码头](../Page/黄沙码头.md "wikilink")
  - [珠江隧道](../Page/珠江隧道.md "wikilink")
  - [白鹅潭酒吧街](../Page/白鹅潭酒吧街.md "wikilink")
  - [洲头咀码头](../Page/洲头咀码头.md "wikilink")
  - [人民桥 (广州)](../Page/人民桥_\(广州\).md "wikilink")

## 參考資料

[B白](../Category/广州地理.md "wikilink")
[B白](../Category/广州军事史.md "wikilink")

1.  [1963年羊城八景－－鹅潭夜月](http://eblog.cersp.com/userlog10/104993/archives/2007/470374.shtml)
2.  廣東海圖說·白鵝潭 張之洞 廣雅書局 1889