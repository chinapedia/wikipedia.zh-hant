**黑羊王朝**是一古代的[土庫曼部族的王朝](../Page/土庫曼族.md "wikilink")。約于1375年-1468年統治今天的[阿塞拜疆](../Page/阿塞拜疆.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")[西北部與](../Page/西北.md "wikilink")[伊拉克东北地區](../Page/伊拉克.md "wikilink")。王朝早期为[札剌亦儿王朝的藩属](../Page/札剌亦儿王朝.md "wikilink")，曾被[帖木儿帝国击败](../Page/帖木儿帝国.md "wikilink")，最后被[白羊王朝灭亡](../Page/白羊王朝.md "wikilink")

## 歷史

1375年，黑羊王朝于[波斯地區东部的](../Page/波斯.md "wikilink")[赫拉特建都](../Page/赫拉特.md "wikilink")，成為當時統治[巴格達與](../Page/巴格達.md "wikilink")[大不里士二大](../Page/大不里士.md "wikilink")[城市的札剌亦兒王朝之藩屬](../Page/城市.md "wikilink")。

當[卡拉·優素福](../Page/卡拉·優素福.md "wikilink")在位時，土庫曼族造反，攻下了大不里士，結束了對[札剌亦兒王朝的藩屬地位](../Page/札剌亦兒王朝.md "wikilink")。

1400年，黑羊王朝被来自于[渴石的](../Page/渴石.md "wikilink")[帖木儿打敗](../Page/帖木儿.md "wikilink")，卡拉·優素福逃到[埃及](../Page/马木留克王朝.md "wikilink")。

1406年，卡拉·優素福又重新佔領大不里士。

1410年，黑羊王朝佔領巴格達。

1420年，卡拉·優素福去世。

黑羊王朝的領導者[傑汗·沙赫](../Page/傑汗·沙赫.md "wikilink")與[帖木儿帝国的](../Page/帖木儿帝国.md "wikilink")[沙哈鲁定了和平協議](../Page/沙哈鲁.md "wikilink")，但很快協議就被違背。

1447年，傑汗·沙赫佔領了今日的部份伊拉克地區及[阿拉伯半島東部沿海地區](../Page/阿拉伯半島.md "wikilink")。时帖木儿帝国則控制了伊朗西部。

1466年傑汗·沙赫試圖奪取屬于[白羊王朝的](../Page/白羊王朝.md "wikilink")[迪亞巴克爾](../Page/迪亞巴克爾.md "wikilink")，結果戰敗被殺。

1468年黑羊王朝被也是土庫曼族建立的[白羊王朝所滅](../Page/白羊王朝.md "wikilink")。

《[明史](../Page/明史.md "wikilink")·西域传》作**讨来思**。当即[大不里士](../Page/大不里士.md "wikilink")（Tabriz）古名Tauris之对音。[明宣宗](../Page/明宣宗.md "wikilink")[宣德六年](../Page/宣德.md "wikilink")（1431年）曾遣使入贡于[明朝](../Page/明朝.md "wikilink")，是时建都于此的正是黑羊王朝。《[明史](../Page/明史.md "wikilink")·外国传》将黑羊王朝占领的[巴格达作](../Page/巴格达.md "wikilink")**黑葛達**\[1\]。黑葛达在宣德年间也遣使入贡。《明史》黑葛達“记载国小民贫，尚佛畏刑。多[牛](../Page/牛.md "wikilink")[羊](../Page/羊.md "wikilink")，以[铁铸钱](../Page/铁.md "wikilink")”。

## 註解

[英文](../Page/英文.md "wikilink")：Kara Yusuf，1390年-1400年在位；1406年-1420年再次在位。

英文：Jihan Shah，約1438年-1467年在位。

[Category:中亞歷史](../Category/中亞歷史.md "wikilink")
[Category:已不存在的亞洲國家](../Category/已不存在的亞洲國家.md "wikilink")
[Category:已不存在的國家](../Category/已不存在的國家.md "wikilink")
[Category:已不存在的亞洲君主國](../Category/已不存在的亞洲君主國.md "wikilink")

1.  《[明会典](../Page/明会典.md "wikilink")》卷106
    《[明史](../Page/明史.md "wikilink")》卷326
    《象胥录》卷7
    《明四夷考》卷下
    《[续通典](../Page/续通典.md "wikilink")》卷149
    《明续通考》卷237
    《译史》卷2
    《皇舆考》卷12
    《四夷广记》
    《咸宾录》卷4
    《裔乘》卷7