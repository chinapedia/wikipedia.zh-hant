<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>塔巴斯科州<br />
Tabasco</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_Tabasco.svg" title="fig:Coat_of_arms_of_Tabasco.svg">Coat_of_arms_of_Tabasco.svg</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>位置</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Tabasco_in_Mexico.png" title="fig:Tabasco_in_Mexico.png">Tabasco_in_Mexico.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>基本資料</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>州府</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>面積</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>人口<br />
<small>(2005年)</small></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/HDI.md" title="wikilink">HDI</a> (2004)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/墨西哥各州州長列表.md" title="wikilink">州長</a><br />
<small>(2001-2007)</small></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/墨西哥眾議院.md" title="wikilink">眾議員</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/墨西哥參議院.md" title="wikilink">參議員</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/ISO_3166-2.md" title="wikilink">ISO 3166-2</a><br />
<small>郵政縮寫</small></p></td>
</tr>
</tbody>
</table>

**塔巴斯科州**（）是[墨西哥的一個](../Page/墨西哥.md "wikilink")[州](../Page/墨西哥行政區劃.md "wikilink")，位於[特萬特佩克地峽北部](../Page/特萬特佩克地峽.md "wikilink")，北臨[墨西哥灣](../Page/墨西哥灣.md "wikilink")，東南與[瓜地馬拉接壤](../Page/瓜地馬拉.md "wikilink")。下分4區17市。

在語言上與[韋拉克魯斯州一樣](../Page/韋拉克魯斯州.md "wikilink")，由於受[古巴影響](../Page/古巴.md "wikilink")，屬[加勒比西班牙語地區](../Page/加勒比西班牙語.md "wikilink")。

在當地生長的塔巴斯科變種[小米辣](../Page/小米辣.md "wikilink")（Capsicum frutescens var.
tabasco）是馳名世界的[塔巴斯科辣椒醬的主要材料](../Page/塔巴斯科辣椒醬.md "wikilink")。

## 参考资料

[Category:墨西哥行政區劃](../Category/墨西哥行政區劃.md "wikilink")
[Category:1824年建立的一級行政區](../Category/1824年建立的一級行政區.md "wikilink")