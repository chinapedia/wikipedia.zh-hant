**查尔斯顿**（**Charleston**）是[美国](../Page/美国.md "wikilink")[西弗吉尼亚州的首府](../Page/西弗吉尼亚州.md "wikilink")，也是该州最大的城市和[卡诺瓦县的县治所在](../Page/卡诺瓦县_\(西弗吉尼亚州\).md "wikilink")。根据[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年统计，共有人口53,421（2000年），其中[白人占](../Page/白人.md "wikilink")80.63%、[非裔美国人占](../Page/非裔美国人.md "wikilink")15.07%、[亚裔美国人占](../Page/亚裔美国人.md "wikilink")1.83%。

[C](../Category/西弗吉尼亚州城市.md "wikilink")
[Category:1788年建立](../Category/1788年建立.md "wikilink")
[C](../Category/美国各州首府.md "wikilink")