**娘子關战役**為[太原会战系列战役之一](../Page/太原会战.md "wikilink")，也是[抗日战争早期的大型戰役之一](../Page/中国抗日战争.md "wikilink")。地點是在中國戰略要點[太行山](../Page/太行山.md "wikilink")[娘子關](../Page/娘子關.md "wikilink")，该关口位于山西与河北交界支处。

起始時間為1937年10月6日。攻擊部隊為日軍[川岸文三郎率領的關東軍第](../Page/川岸文三郎.md "wikilink")20師團。正面守軍為中國[孫連仲第](../Page/孫連仲.md "wikilink")26路军與[曾萬鍾率領的第](../Page/曾萬鍾.md "wikilink")3军。

激戰於河北井陘雪花山，原杨虎城部十七路军缩编后的國民革民軍17師（师长赵寿山）傷亡超過千人，最後雪花山失守，國軍再退。其中，一０二團長[張世俊貽誤軍機](../Page/張世俊.md "wikilink")，遭國軍處決。

10月8日－10月22日，日軍於關外被[國民革命軍包圍](../Page/國民革命軍.md "wikilink")，10月23日日軍第109師團前來解圍，10月26日，日軍攻陷娘子關。

## 參考文獻

  - 何應欽，《日軍侵華八年抗戰史》，1982年，臺北，黎明文化事業公司。

[Category:抗日战争主要战役](../Category/抗日战争主要战役.md "wikilink")
[Category:1937年抗日战争战役](../Category/1937年抗日战争战役.md "wikilink")
[Category:山西抗日战争战役](../Category/山西抗日战争战役.md "wikilink")
[Category:阳泉军事史](../Category/阳泉军事史.md "wikilink")
[Category:平定县](../Category/平定县.md "wikilink")
[Category:1937年10月](../Category/1937年10月.md "wikilink")