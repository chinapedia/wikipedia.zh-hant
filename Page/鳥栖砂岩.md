**鳥栖砂岩**（拉丁字：Sagan
Tosu），是[日本職業足球球隊](../Page/日本.md "wikilink")，成立於1997年，現正於[日本職業足球聯賽作賽](../Page/日本職業足球聯賽.md "wikilink")。

## 簡介

1997年2月，球隊接替解散的[鳥栖未來](../Page/鳥栖未來.md "wikilink")，命名為鳥栖砂岩，但法人團體遭到解散，1998年設立株式會社鳥栖砂岩，Sagan在日語中可解作砂岩。球隊的主場位於[島栖球場](../Page/島栖球場.md "wikilink")，根據地在[佐賀縣](../Page/佐賀縣.md "wikilink")[鳥栖市](../Page/鳥栖市.md "wikilink")。1999年當日職聯乙級成立時，球隊成為日職聯隊伍之一。2005年球隊的經營權交由砂岩Dreams管理。

2011年，鳥栖砂岩正式委任[韓國籍的](../Page/韓國.md "wikilink")[尹晶煥為主教練](../Page/尹晶煥.md "wikilink")，尹晶煥是這年[日本職業足球聯賽](../Page/日本職業足球聯賽.md "wikilink")38支球隊(包括J1及J2)主教練中最年輕的一位。球隊在尹晶煥帶領下得到J2聯賽亞軍，歷史性首次升級到J1聯賽。

在升級J1的首季中，在被預測為升降機的情況下，幾乎全季都位在中上游的位置。甚至在最後兩輪排名第三、有機會參加[2013年亞足聯冠軍聯賽](../Page/2013年亞足聯冠軍聯賽.md "wikilink")，可惜因輸掉最後一輪無緣更進一步。但第五名的成績已是近年升班球隊最優秀的。

2018年7月，鳥栖砂岩羅致現年34歲的前西班牙國腳[費蘭度·托利斯](../Page/費南多·托雷斯.md "wikilink")\[1\]

## 球員名單

### 現役（2018）

<table style="width:253%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 80%" />
<col style="width: 50%" />
<col style="width: 28%" />
<col style="width: 10%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font style="color:white;">編號</p></th>
<th><p><font style="color:white;">国籍</p></th>
<th><p><font style="color:white;">球員名字</p></th>
<th><p><font style="color:white;">出生日期</p></th>
<th><p><font style="color:white;">加盟年份</p></th>
<th><p><font style="color:white;">前屬球會</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>門　將</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><a href="../Page/赤星拓.md" title="wikilink">赤星拓</a></p></td>
<td></td>
<td><p>2007年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>18</strong></p></td>
<td></td>
<td><p><a href="../Page/高丘陽平.md" title="wikilink">高丘陽平</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/橫濱FC.md" title="wikilink">橫濱FC</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><a href="../Page/權田修一.md" title="wikilink">權田修一</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/SV霍恩.md" title="wikilink">SV霍恩</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>30</strong></p></td>
<td></td>
<td><p><a href="../Page/大村燦.md" title="wikilink">大村燦</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/切塞納足球俱樂部.md" title="wikilink">切塞納</a></p></td>
</tr>
<tr class="even">
<td><p><strong>後　衛</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2</strong></p></td>
<td></td>
<td><p><a href="../Page/三丸拡.md" title="wikilink">三丸拡</a></p></td>
<td></td>
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>3</strong></p></td>
<td></td>
<td><p><a href="../Page/高橋祐治.md" title="wikilink">高橋祐治</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/京都不死鳥.md" title="wikilink">京都不死鳥</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><a href="../Page/金敏赫.md" title="wikilink">金敏赫</a></p></td>
<td></td>
<td><p>2014年</p></td>
<td><p><a href="../Page/崇實大學.md" title="wikilink">崇實大學</a></p></td>
</tr>
<tr class="even">
<td><p><strong>35</strong></p></td>
<td></td>
<td><p><a href="../Page/藤田優人.md" title="wikilink">藤田優人</a></p></td>
<td></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/柏太陽王.md" title="wikilink">柏雷索爾</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>13</strong></p></td>
<td></td>
<td><p><a href="../Page/小林祐三.md" title="wikilink">小林祐三</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/横濱水手.md" title="wikilink">横濱水手</a></p></td>
</tr>
<tr class="even">
<td><p><strong>23</strong></p></td>
<td></td>
<td><p><a href="../Page/吉田豐.md" title="wikilink">吉田豐</a><a href="https://zh.wikipedia.org/wiki/File:Captain_sports.svg" title="fig:Captain_sports.svg">Captain_sports.svg</a></p></td>
<td></td>
<td><p>2015年</p></td>
<td><p><a href="../Page/清水心跳.md" title="wikilink">清水心跳</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>24</strong></p></td>
<td></td>
<td><p><a href="../Page/安在和樹.md" title="wikilink">安在和樹</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/東京綠茵.md" title="wikilink">東京綠茵</a></p></td>
</tr>
<tr class="even">
<td><p><strong>33</strong></p></td>
<td></td>
<td><p><a href="../Page/瓊安·奧馬里.md" title="wikilink">瓊安·奧馬里</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/阿爾納沙體育會.md" title="wikilink">阿爾納沙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>38</strong></p></td>
<td></td>
<td><p><a href="../Page/乾大知.md" title="wikilink">乾大知</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/長崎成功丸.md" title="wikilink">長崎成功丸</a></p></td>
</tr>
<tr class="even">
<td><p><strong>43</strong></p></td>
<td></td>
<td><p><a href="../Page/平瀨大.md" title="wikilink">平瀨大</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p>鳥栖砂岩U-18</p></td>
</tr>
<tr class="odd">
<td><p><strong>中　場</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>4</strong></p></td>
<td></td>
<td><p><a href="../Page/原川力.md" title="wikilink">原川力</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/川崎前鋒.md" title="wikilink">川崎前鋒</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><a href="../Page/福田晃斗.md" title="wikilink">福田晃斗</a></p></td>
<td></td>
<td><p>2013年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>7</strong></p></td>
<td></td>
<td><p><a href="../Page/河野廣貴.md" title="wikilink">河野廣貴</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/東京足球俱樂部.md" title="wikilink">東京FC</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><a href="../Page/高橋義希.md" title="wikilink">高橋義希</a></p></td>
<td></td>
<td><p>2012年</p></td>
<td><p><a href="../Page/仙台維加泰.md" title="wikilink">仙台維加泰</a></p></td>
</tr>
<tr class="even">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><a href="../Page/加藤恆平.md" title="wikilink">加藤恆平</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/舊扎戈拉貝羅足球俱樂部.md" title="wikilink">舊扎戈拉貝羅</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>26</strong></p></td>
<td></td>
<td><p><a href="../Page/伊藤遼哉.md" title="wikilink">伊藤遼哉</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/福爾圖娜杜塞道夫.md" title="wikilink">福爾圖娜杜塞道夫U</a>-19</p></td>
</tr>
<tr class="even">
<td><p><strong>28</strong></p></td>
<td></td>
<td><p><a href="../Page/石川啓人.md" title="wikilink">石川啓人</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p>鳥栖砂岩U-18</p></td>
</tr>
<tr class="odd">
<td><p><strong>29</strong></p></td>
<td></td>
<td><p><a href="../Page/谷口博之.md" title="wikilink">谷口博之</a></p></td>
<td></td>
<td><p>2014年</p></td>
<td><p><a href="../Page/柏太陽王.md" title="wikilink">柏雷索爾</a></p></td>
</tr>
<tr class="even">
<td><p><strong>36</strong></p></td>
<td></td>
<td><p><a href="../Page/高橋秀人.md" title="wikilink">高橋秀人</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/神戶勝利船.md" title="wikilink">神戶勝利船</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>42</strong></p></td>
<td></td>
<td><p><a href="../Page/松岡大起.md" title="wikilink">松岡大起</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p>鳥栖砂岩U-18</p></td>
</tr>
<tr class="even">
<td><p><strong>前　锋</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>9</strong></p></td>
<td></td>
<td><p><a href="../Page/費南多·托雷斯.md" title="wikilink">費蘭度·托利斯</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/馬德里競技.md" title="wikilink">馬德里競技</a></p></td>
</tr>
<tr class="even">
<td><p><strong>11</strong></p></td>
<td></td>
<td><p><a href="../Page/豊田陽平.md" title="wikilink">豊田陽平</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/蔚山現代足球俱樂部.md" title="wikilink">蔚山現代</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>19</strong></p></td>
<td></td>
<td><p><a href="../Page/趙東建.md" title="wikilink">趙東建</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/水原三星藍翼足球俱樂部.md" title="wikilink">水原三星</a></p></td>
</tr>
<tr class="even">
<td><p><strong>22</strong></p></td>
<td></td>
<td><p><a href="../Page/池田圭.md" title="wikilink">池田圭</a></p></td>
<td></td>
<td><p>2009年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>27</strong></p></td>
<td></td>
<td><p><a href="../Page/田川亨介.md" title="wikilink">田川亨介</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p>鳥栖砂岩U-18</p></td>
</tr>
<tr class="even">
<td><p><strong>40</strong></p></td>
<td></td>
<td><p><a href="../Page/小野裕二.md" title="wikilink">小野裕二</a></p></td>
<td></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/皇家聖圖爾登足球會.md" title="wikilink">皇家聖圖爾登</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>41</strong></p></td>
<td></td>
<td><p><a href="../Page/石井快征.md" title="wikilink">石井快征</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p>鳥栖砂岩U-18</p></td>
</tr>
<tr class="even">
<td><p><strong>44</strong></p></td>
<td></td>
<td><p><a href="../Page/金崎夢生.md" title="wikilink">金崎夢生</a></p></td>
<td></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/鹿島鹿角.md" title="wikilink">鹿島鹿角</a></p></td>
</tr>
</tbody>
</table>

## 著名球員

  - [費蘭度·托利斯](../Page/費南多·托雷斯.md "wikilink")
  - [權田修一](../Page/權田修一.md "wikilink")
  - [金崎夢生](../Page/金崎夢生.md "wikilink")

## 队徽

SaganTosu.png|旧队徽 Sagan_Tosu.png|新队徽

## 參考資料

## 外部連結

  - [官方網站](http://www.sagan-tosu.net/)

[分類:1997年建立的足球俱樂部](../Page/分類:1997年建立的足球俱樂部.md "wikilink")

[Category:日本足球俱樂部](../Category/日本足球俱樂部.md "wikilink")
[Category:鳥栖市](../Category/鳥栖市.md "wikilink")
[Category:1997年日本建立](../Category/1997年日本建立.md "wikilink")

1.