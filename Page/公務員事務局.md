**公務員事務局**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[香港特別行政區政府的](../Page/香港特別行政區政府.md "wikilink")[決策局之一](../Page/決策局.md "wikilink")，專責[香港公務員事務](../Page/香港公務員.md "wikilink")。現任公務員事務局局長為[羅智光](../Page/羅智光.md "wikilink")。

## 歷史

公務員事務局成立於1997年7月1日，前身是隸屬[布政司署的](../Page/布政司署.md "wikilink")**公務員事務科**，首長為**公務員事務司**。1991年前稱為**[銓敍科](../Page/銓敍.md "wikilink")**，首長為**銓敍司**。

2002年，行政長官[董建華推行](../Page/董建華.md "wikilink")[主要官員問責制](../Page/主要官員問責制.md "wikilink")，將所有局長由公務員職位改為政治委任。但為平息公務員疑慮，規定公務員事務局局長須由[公務員轉任](../Page/公務員.md "wikilink")；而且委任期滿後可返回公務員崗位\[1\]（即俗稱「旋轉門」）。公務員事務局並不設有副局長及局長政治助理的職位，是唯一有這樣安排的決策局。

## 歷任首長

  - [銓敘司](../Page/銓敘.md "wikilink")

<!-- end list -->

1.  [施恪](../Page/施恪.md "wikilink")（Scott，1973年6月—1977年8月）
2.  [布立之](../Page/布立之.md "wikilink")（R. G. B. Bridge，1977年8月—1978年3月）
3.  [羅能士](../Page/羅能士.md "wikilink")（1978年3月—1985年8月）
4.  [霍德](../Page/霍德.md "wikilink")（DAVID ROBERT
    FORD\[2\]，1985年8月—1986年12月）
5.  [高禮和](../Page/高禮和.md "wikilink")（HARNAM SINGH
    GREWAL\[3\]，1987年2月—1990年4月）
6.  [屈珩](../Page/屈珩.md "wikilink")（EDWARD BARRIE
    WIGGHAM\[4\]，1990年4月—1991年6月18日）

<!-- end list -->

  - 公務員事務司

<!-- end list -->

  - [屈珩](../Page/屈珩.md "wikilink")（Wiggham，1991年6月19日—1993年4月）
  - [陳方安生](../Page/陳方安生.md "wikilink")（1993年4月19日—1993年10月）
  - [施祖祥](../Page/施祖祥.md "wikilink")（1994年1月28日—1996年）
  - [林煥光](../Page/林煥光.md "wikilink")（1996年2月—1997年6月30日）

<table>
<tbody>
<tr class="odd">
<td style="text-align: left;"><dl>
<dt>公務員事務局局長</dt>

</dl>
<ol>
<li><a href="../Page/林煥光.md" title="wikilink">林煥光</a>（1997年7月1日—2000年7月9日）[5]</li>
<li><a href="../Page/王永平_(香港).md" title="wikilink">王永平</a>（2000年8月1日—2006年1月24日）[6]</li>
<li><a href="../Page/俞宗怡.md" title="wikilink">俞宗怡</a>（2006年1月24日—2012年6月30日）</li>
<li><a href="../Page/鄧國威.md" title="wikilink">鄧國威</a>（2012年7月1日—2015年7月21日）</li>
<li><a href="../Page/張雲正.md" title="wikilink">張雲正</a>（2015年7月21日—2017年6月30日）</li>
<li><a href="../Page/羅智光.md" title="wikilink">羅智光</a>（2017年7月1日—）</li>
</ol></td>
<td style="text-align: left;"><dl>
<dt>公務員事務局<a href="../Page/常任秘書長.md" title="wikilink">常任秘書長</a></dt>

</dl>
<ol>
<li><a href="../Page/王倩儀.md" title="wikilink">王倩儀</a>（署理，2002年—2003年）</li>
<li><a href="../Page/黎高穎怡.md" title="wikilink">黎高穎怡</a>（2003年2月—2006年6月）</li>
<li><a href="../Page/黃灝玄.md" title="wikilink">黃灝玄</a>（2006年2月—2010年12月23日）</li>
<li><a href="../Page/黃鴻超.md" title="wikilink">黃鴻超</a>（2011年1月17日—2015年9月）</li>
<li><a href="../Page/周達明.md" title="wikilink">周達明</a>（2015年9月14日—）</li>
</ol></td>
</tr>
</tbody>
</table>

## 注释

## 参考文献

## 外部連結

  - [香港特別行政區政府 公務員事務局](http://www.csb.gov.hk/)
  - [香港公務員總工會](https://web.archive.org/web/20070108221856/http://www.hkcsgu.org/)

{{-}}     |-

[Category:香港公務員](../Category/香港公務員.md "wikilink")
[Category:香港特别行政区主要官员列表](../Category/香港特别行政区主要官员列表.md "wikilink")

1.  王永平，2008年，《平心直說——一名香港特區政府局長為官十二年的反思集》，香港：經濟日報出版社，ISBN
    978-962-678-511-9
2.  [OFFICIAL REPORT OF PROCEEDINGS, 16
    July 1987](http://www.legco.gov.hk/yr86-87/english/lc_sitg/hansard/h870716.pdf)
3.  見[環境運輸及工務局一文](../Page/環境運輸及工務局.md "wikilink")
4.  [OFFICIAL RECORD OF PROCEEDINGS, 8
    July 1992](http://www.legco.gov.hk/yr91-92/english/lc_sitg/hansard/h920708.pdf)
5.
6.