**城关区**是[中国](../Page/中国.md "wikilink")[甘肃省](../Page/甘肃.md "wikilink")[兰州市下辖的一个区](../Page/兰州市.md "wikilink")，位于兰州市区的最东面，包括[黄河南北两岸](../Page/黄河.md "wikilink")，旧兰州城，以及[天兰铁路修通后新建的城区](../Page/天兰铁路.md "wikilink")。全区面积220平方公里，其中城区面积60平方公里。[甘肃省政府和](../Page/甘肃.md "wikilink")[兰州市政府都位于城关区](../Page/兰州.md "wikilink")，此外[兰州大学](../Page/兰州大学.md "wikilink")、[西北民族大学](../Page/西北民族大学.md "wikilink")、兰州铁路局、兰州军区等行政机关都处于城关区。原来位于黄河中心的一个大岛雁滩是兰州郊区的一个乡，是兰州的主要[蔬菜供应地](../Page/蔬菜.md "wikilink")，现在由于黄河淤积，已经和陆地连成一片，也划入到城关区。

城关区是兰州市的中心区，有汉、回、满、蒙古、藏、维吾尔等36个民族，总人口93.6万人。

## 行政区划

[缩略图](https://zh.wikipedia.org/wiki/File:兰州市第一人民医院_-_panoramio.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:俯视永昌路12_-_panoramio.jpg "fig:缩略图")

城关区现辖24个街道办事处，129个社区居委会、39个村委会

  - 街道：[临夏路街道](../Page/临夏路街道.md "wikilink")、[张掖路街道](../Page/张掖路街道.md "wikilink")、[白银路街道](../Page/白银路街道.md "wikilink")、[伏龙坪街道](../Page/伏龙坪街道.md "wikilink")、[酒泉路街道](../Page/酒泉路街道.md "wikilink")、[广武门街道](../Page/广武门街道.md "wikilink")、[东岗西路街道](../Page/东岗西路街道.md "wikilink")、[皋兰路街道](../Page/皋兰路街道.md "wikilink")、[渭源路街道](../Page/渭源路街道.md "wikilink")、[雁南街道](../Page/雁南街道.md "wikilink")、[雁北街道](../Page/雁北街道.md "wikilink")、[盐场路街道](../Page/盐场路街道.md "wikilink")、[草场街街道](../Page/草场街街道.md "wikilink")、[靖远路街道](../Page/靖远路街道.md "wikilink")、[团结新村街道](../Page/团结新村街道.md "wikilink")、[铁路东村街道](../Page/铁路东村街道.md "wikilink")、[铁路西村街道](../Page/铁路西村街道.md "wikilink")、[五泉街道](../Page/五泉街道.md "wikilink")、[火车站街道](../Page/火车站街道.md "wikilink")、[拱星墩街道](../Page/拱星墩街道.md "wikilink")、[嘉峪关路街道](../Page/嘉峪关路街道.md "wikilink")、[焦家湾街道](../Page/焦家湾街道.md "wikilink")、[东岗街道](../Page/东岗街道.md "wikilink")、[青白石街道](../Page/青白石街道.md "wikilink")。

## 外部链接

  - [城关区政府网站](http://chengguan.gansu.gov.cn/)

[兰州市城关区](../Category/兰州市城关区.md "wikilink")
[区](../Category/兰州区县.md "wikilink")
[兰州](../Category/甘肃省市辖区.md "wikilink")