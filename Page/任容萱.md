**任容萱**，[臺灣知名](../Page/臺灣.md "wikilink")[女演員](../Page/女演員.md "wikilink")、[女歌手](../Page/女歌手.md "wikilink")，出生於[臺北市](../Page/臺北市.md "wikilink")[士林區](../Page/士林區.md "wikilink")。
2005年因[元衛覺醒](../Page/元衛覺醒.md "wikilink")《夏天的風》MV導演的老婆，在天母逛街時看到了這個讓人眼睛為之一亮的女生，才16歲清新脫俗的任容萱邀來擔任MV女主角而亮相娱樂圈，原來她是[S.H.E團員](../Page/S.H.E.md "wikilink")[Selina的妹妹](../Page/任家萱.md "wikilink")，一切可都說是個美麗的巧合。
2009年參與《[終極三國](../Page/終極三國.md "wikilink")》演出，正式出道；2013年參演了《[我的自由年代](../Page/我的自由年代.md "wikilink")》而人氣急升，成功打響知名度；2015年在《[聽見幸福](../Page/聽見幸福.md "wikilink")》中一人分飾兩角，同年演藝事業轉往中國大陸發展。

## 影視作品

### 電視劇

|                                                                                                              |                                                                         |                                                    |         |
| ------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------- | -------------------------------------------------- | ------- |
| 年份                                                                                                           | 電視台                                                                     | 劇名                                                 | 角色      |
| 2009年                                                                                                        | [民視](../Page/民視無線台.md "wikilink")/[八大](../Page/八大綜合台.md "wikilink")     | 《[終極三國](../Page/終極三國.md "wikilink")》               | 貂蟬      |
| 2011年                                                                                                        | [中視](../Page/中視數位台.md "wikilink")/[公視](../Page/公視主頻.md "wikilink")      | 《[珍愛林北](../Page/珍愛林北.md "wikilink")》               | 林千羽     |
| [台視主頻](../Page/台視主頻.md "wikilink")、[三立都會台](../Page/三立都會台.md "wikilink")                                      | 《[勇士們](../Page/勇士們_\(電視劇\).md "wikilink")》                              | 王小桃                                                |         |
| [八大](../Page/八大娛樂台.md "wikilink")/[華視](../Page/華視主頻.md "wikilink")                                           | 《[為愛固執](../Page/為愛固執.md "wikilink")》                                    | 丁以紫                                                |         |
| 2013年                                                                                                        | 三立、[東森綜合台](../Page/東森綜合台.md "wikilink")                                 | 《[我的自由年代](../Page/我的自由年代.md "wikilink")》           | 林嘉恩     |
| 2014年                                                                                                        | [中視HD台](../Page/中視HD台.md "wikilink")                                    | 《[七個朋友](../Page/七個朋友.md "wikilink")》               | 屈心彤     |
| 中国大陸                                                                                                         | 《[青春風暴](../Page/青春風暴.md "wikilink")》                                    | 舒品凡                                                |         |
| 2015年                                                                                                        | [台視主頻](../Page/台視主頻.md "wikilink")、[三立都會台](../Page/三立都會台.md "wikilink") | 《[聽見幸福](../Page/聽見幸福.md "wikilink")》               | 梁洛涵/陳禹希 |
| [騰訊視頻](../Page/騰訊視頻.md "wikilink")、[中視數位台](../Page/中視數位台.md "wikilink")、[中天綜合台](../Page/中天綜合台.md "wikilink") | 《[超級大英雄](../Page/超級大英雄.md "wikilink")》                                  | 莫涵                                                 |         |
| [山东卫视](../Page/山东卫视.md "wikilink")、[黑龙江卫视](../Page/黑龙江卫视.md "wikilink")                                      | 《[背着奶奶进城](../Page/背着奶奶进城.md "wikilink")》                                | 钟雨萱                                                |         |
| 2017年                                                                                                        | [浙江卫视](../Page/浙江卫视.md "wikilink")、[北京卫视](../Page/北京卫视.md "wikilink")   | 《[深夜食堂](../Page/深夜食堂_\(亚洲华语版\).md "wikilink")》     | 杨颖      |
| 待播                                                                                                           |                                                                         | 《[我曾爱过你，想起就心酸](../Page/我曾爱过你，想起就心酸.md "wikilink")》 | 吳斯佳     |
|                                                                                                              | 《[如果可以，絕不愛你](../Page/如果可以，絕不愛你.md "wikilink")》                          | 陳斐                                                 |         |
|                                                                                                              |                                                                         |                                                    |         |

### 電影

|                                        |                                            |                |                                     |
| -------------------------------------- | ------------------------------------------ | -------------- | ----------------------------------- |
| 年份                                     | 戲名                                         | 角色             | 註解                                  |
| 2012年                                  | 《[變羊記](../Page/變羊記.md "wikilink")》         | 姚洛棻            |                                     |
| 2014年                                  | 《[早安，冬日海](../Page/早安，冬日海.md "wikilink")》   | 薇薇安            | [大陆](../Page/中华人民共和国.md "wikilink") |
| 2016年                                  | 《[他媽²的藏寶圖](../Page/他媽²的藏寶圖.md "wikilink")》 | 陳雅希            | 1月8日上映                              |
| 《[神廚](../Page/神廚.md "wikilink")》       | 姜七寶                                        | 2月5日春節賀歲片，馮凱導演 |                                     |
| 2017年                                  | 《[麻煩家族](../Page/麻煩家族.md "wikilink")》       | 林叢             | 中國大陆5月11日上映                         |
| 2019年                                  | 《[玩命貼圖](../Page/玩命貼圖.md "wikilink")》       | 沈靈             | 1月11日上映                             |
| 待上映                                    | 《[遇見下一個你](../Page/遇見下一個你.md "wikilink")》   | 韓朵朵            | 中國大陆                                |
| 《[驚夢49天](../Page/驚夢49天.md "wikilink")》 | 劉奕臻                                        |                |                                     |
| 《[捉妖大仙2](../Page/捉妖大仙2.md "wikilink")》 |                                            |                |                                     |

### 微電影

|       |           |     |
| ----- | --------- | --- |
| 年份    | 劇名        | 角色  |
| 2012年 | 《皇者風范》    | 薇薇  |
| 2013年 | 《本可改變的結局》 | 顧可薇 |

### 綜藝節目

|                    年份                    |                                         節目名稱                                          |              播出日期              |
| :--------------------------------------: | :-----------------------------------------------------------------------------------: | :----------------------------: |
|                  2006年                   |                        《[國光幫幫忙](../Page/國光幫幫忙.md "wikilink")》                         |             1月19日              |
|   《[康熙來了](../Page/康熙來了.md "wikilink")》   |                                         8月25日                                         |                                |
|                  2007年                   |                        《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》                         |             7月18日              |
|                  2008年                   |                         《[康熙來了](../Page/康熙來了.md "wikilink")》                          |              2月6日              |
|  《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》  |                                      4月11日、7月14日                                      |                                |
|                  2009年                   |                        《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》                         |       2月19日、3月10日、3月21日        |
|                 《東風娛樂通》                  |                                         2月27日                                         |                                |
|  《[大學生了沒](../Page/大學生了沒.md "wikilink")》  |                                         4月1日                                          |                                |
|   《[完全娛樂](../Page/完全娛樂.md "wikilink")》   |                                         5月27日                                         |                                |
|   《[豬哥會社](../Page/豬哥會社.md "wikilink")》   |                                         9月5日                                          |                                |
|                  2010年                   |                       《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》                        |              9月8日              |
|                  2011年                   |                                      《沈春華Life秀》                                       |             8月13日              |
|   《[康熙來了](../Page/康熙來了.md "wikilink")》   |                                         8月23日                                         |                                |
|                  2012年                   |                                        《哈林哈時尚》                                        |             6月22日              |
|                  2013年                   |                       《[小宇宙33號](../Page/小宇宙33號.md "wikilink")》                        |             5月27日              |
|   《[完全娛樂](../Page/完全娛樂.md "wikilink")》   |                               11月14日、11月27日、12月3日、12月4日                               |                                |
|  《[綜藝大熱門](../Page/綜藝大熱門.md "wikilink")》  |                                        11月15日                                         |                                |
| 《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》 |                                        11月20日                                         |                                |
|                  《我愛偶像》                  |                                        11月22日                                         |                                |
|   《[華劇大賞](../Page/華劇大賞.md "wikilink")》   |                                        12月28日                                         |                                |
|                  2014年                   |                                        《我愛偶像》                                         |             1月24日              |
|            《自由過年ing-三立初一特別節目》            |                                         1月30日                                         |                                |
| 《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》 |                                         4月4日                                          |                                |
|   《[華劇大賞](../Page/華劇大賞.md "wikilink")》   |                                        12月14日                                         |                                |
|                  2015年                   |                                        《台北映時尚》                                        |             1月24日              |
|  《[型男大主廚](../Page/型男大主廚.md "wikilink")》  |                                         2月9日                                          |                                |
|   《[康熙來了](../Page/康熙來了.md "wikilink")》   |                                         6月2日                                          |                                |
| 《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》 |                                         6月4日                                          |                                |
|                  2016年                   |                      《[別讓身體不開心](../Page/別讓身體不開心.md "wikilink")》                       | 02月1日，神廚！？（任容萱）與傻跟班！？（任家萱）姐妹PK |
| 《[SS小燕之夜](../Page/SS小燕之夜.md "wikilink")》 |                                         02月2日                                         |                                |
|   《[夜光家族](../Page/夜光家族.md "wikilink")》   |   02月4日，[飛碟電台](../Page/飛碟電台.md "wikilink")-[光禹主持](../Page/光禹.md "wikilink")。電台初體驗。    |                                |
|  《[娛樂百分百](../Page/娛樂百分百.md "wikilink")》  |                                         02月6日                                         |                                |
|  《[型男大主廚](../Page/型男大主廚.md "wikilink")》  |               02月8日\~10日，【型男大主廚 10週年公益辦桌】大年初一到初三 年節特別節目，型男幫遇上史上最大勁敵神廚隊                |                                |
|                 《 當掌聲響起》                 | 03月26日，[TVBS節目](../Page/TVBS.md "wikilink")，演藝圈好姊妹[任家萱一起](../Page/任家萱.md "wikilink")。 |                                |
|                  2017年                   |                   《[王牌對王牌第二季](../Page/王牌對王牌_\(真人秀\).md "wikilink")》                   |             3月31日              |
|                                          |                                                                                       |                                |

## 音樂作品

### 單曲

|            |                                             |                                                                      |                                                                                                                                                                                                    |
| ---------- | ------------------------------------------- | -------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **發行日期**   | **歌曲名稱**                                    | **專輯名稱**                                                             | **備註**                                                                                                                                                                                             |
| 2007年8月17日 | 愛在一起                                        | [唐禹哲](../Page/唐禹哲.md "wikilink")《愛我》專輯                               | 與[唐禹哲合唱](../Page/唐禹哲.md "wikilink")                                                                                                                                                                |
| 2009年4月3日  | 小心願                                         | [強辯之【終極三國】三團鼎立SUPER大鬥陣](../Page/強辯之【終極三國】三團鼎立SUPER大鬥陣.md "wikilink") | 與[強辯樂團合唱](../Page/強辯樂團.md "wikilink")                                                                                                                                                              |
| 2013年10月9日 | 北鼻麻吉 Baby Match                             | 電視劇《[七個朋友](../Page/七個朋友.md "wikilink")》主題曲                           | 與[付辛博](../Page/付辛博.md "wikilink")、[劉忻](../Page/劉忻.md "wikilink")、[陳奕](../Page/陳奕.md "wikilink")、[沈建宏](../Page/沈建宏.md "wikilink")、[傅穎](../Page/傅穎.md "wikilink")、[唐振剛合唱](../Page/唐振剛.md "wikilink") |
| 摩天輪        | 電視劇《[七個朋友](../Page/七個朋友.md "wikilink")》片尾曲  |                                                                      |                                                                                                                                                                                                    |
| 2015年1月23日 | 愛不愛都寂寞                                      | 《[聽見幸福](../Page/聽見幸福.md "wikilink")》 電視劇原聲帶                          | 分原版和清唱版                                                                                                                                                                                            |
| 聽見幸福       | 《[聽見幸福](../Page/聽見幸福.md "wikilink")》 電視劇原聲帶 | 與[柯有倫合唱](../Page/柯有倫.md "wikilink")                                  |                                                                                                                                                                                                    |
| 未發行        | 青春時光                                        | 電視劇《[青春風暴](../Page/青春風暴.md "wikilink")》主題曲                           | 與[徐申東](../Page/徐申東.md "wikilink")、[唐熙合唱](../Page/唐熙.md "wikilink")                                                                                                                                 |
| 未發行        | 愛情E大調                                       | 電視劇《[青春風暴](../Page/青春風暴.md "wikilink")》插曲                            |                                                                                                                                                                                                    |
| 未發行        | 戀愛流感                                        | 電視《[超級大神探](../Page/超級大神探.md "wikilink")》 片尾曲                         |                                                                                                                                                                                                    |
| 未發行        | 再一次                                         | 電影《[他媽²的藏寶圖](../Page/他媽²的藏寶圖.md "wikilink")》 插曲                      | 與張育群合唱                                                                                                                                                                                             |

### 參演MV

|                                    |                                                                                                                                                                        |                   |
| ---------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| 年度                                 | 歌手                                                                                                                                                                     | 歌名                |
| 2005年                              | [元衛覺醒](../Page/元衛覺醒.md "wikilink")                                                                                                                                     | 《夏天的風》            |
| 2006年                              | [張智成](../Page/張智成.md "wikilink")                                                                                                                                       | 《痊癒》              |
| 2008年                              | [吳建飛](../Page/吳建飛.md "wikilink")                                                                                                                                       | 《偏偏愛上你》           |
| 《你離開以後》                            |                                                                                                                                                                        |                   |
| [黃乙玲](../Page/黃乙玲.md "wikilink")   | 《酒歌》悲情第一部曲                                                                                                                                                             |                   |
| 《感情線》悲情第二部曲                        |                                                                                                                                                                        |                   |
| 《無情兄》悲情第三部曲                        |                                                                                                                                                                        |                   |
| 2009年                              | [Tank](../Page/Tank.md "wikilink")                                                                                                                                     | 《如果我變成回憶》         |
| [強辯樂團](../Page/強辯樂團.md "wikilink") | 《小心願》                                                                                                                                                                  |                   |
| 2011年                              | [周傳雄](../Page/周傳雄.md "wikilink")                                                                                                                                       | 《微涼的記憶》           |
| [傅宇昊](../Page/傅宇昊.md "wikilink")   | 《好的愛》                                                                                                                                                                  |                   |
| 2012年                              | [陳坤](../Page/陳坤.md "wikilink")                                                                                                                                         | 《選擇出色》            |
| [鍾鎮濤](../Page/鍾鎮濤.md "wikilink")   | 《你是個男人》                                                                                                                                                                |                   |
| 2013年                              | **任容萱**、[AK](../Page/AK.md "wikilink")、[傅穎](../Page/傅穎.md "wikilink")、[付辛博](../Page/付辛博.md "wikilink")、[唐振剛](../Page/唐振剛.md "wikilink")、[劉忻](../Page/劉忻.md "wikilink") | 《北鼻麻吉 Baby Match》 |
| 2014年                              | [李祥祥](../Page/李祥祥.md "wikilink")                                                                                                                                       | 《想想我》             |
| 2015年                              | [光良](../Page/光良.md "wikilink")                                                                                                                                         | 《未完成的愛情》          |

## 廣告代言

### 廣告

| 年份                                                                                      | 廣告                                                                            |
| --------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| 2007年                                                                                   | 卡巴拉島Online                                                                    |
| 2010年                                                                                   | Bear Box 行李箱                                                                  |
| 麥當勞（大陸）                                                                                 |                                                                               |
| [宏碁](../Page/宏碁.md "wikilink") Aspire 輕薄筆電                                              |                                                                               |
| 水美媒 OGUMA幫助吸收篇                                                                          |                                                                               |
| [acer筆記型電腦廣告](../Page/acer.md "wikilink")                                               |                                                                               |
| 2012年                                                                                   | 566洗髮乳廣告                                                                      |
| 2014年                                                                                   | 美肌之誌-特優人氣款面膜廣告                                                                |
| [7-ELEVEN](../Page/7-ELEVEN.md "wikilink")-拉拉熊全新商品活動（與[李國毅](../Page/李國毅.md "wikilink")） |                                                                               |
| 天地合補-含鐵四物飲（與[李國毅](../Page/李國毅.md "wikilink")）                                           |                                                                               |
| [屈臣氏](../Page/屈臣氏.md "wikilink")-周年慶蛋黃哥懶懶過生活集點送活動                                       |                                                                               |
| 2015年                                                                                   | [Panasonic](../Page/Panasonic.md "wikilink")-家電新品搭載ECO NAVI 與 nanoe雙科技的冰箱與洗衣機 |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                                         |                                                                               |
| [7-ELEVEN](../Page/7-ELEVEN.md "wikilink")（加熱早餐）一起篇 25秒                                 |                                                                               |
| 2016年                                                                                   | [Panasonic](../Page/Panasonic.md "wikilink")-變頻空調（日本同步篇&未來篇）                  |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                                         |                                                                               |
| 2017年                                                                                   | [Panasonic](../Page/Panasonic.md "wikilink")-家電                               |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                                         |                                                                               |
| Ladies[蕾黛絲](../Page/蕾黛絲.md "wikilink")-內衣                                               |                                                                               |
| 2018年                                                                                   | [Panasonic](../Page/Panasonic.md "wikilink")-家電                               |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                                         |                                                                               |
| Ladies[蕾黛絲](../Page/蕾黛絲.md "wikilink")-內衣                                               |                                                                               |

### 代言

| 年份                                                                            | 產品/活動                                                           |
| ----------------------------------------------------------------------------- | --------------------------------------------------------------- |
| 2011年                                                                         | [SOGONew](../Page/SOGO.md "wikilink") Life形象大使                  |
| 2012年                                                                         | [飛利浦Saeco全自動義式咖啡機代言人](../Page/飛利浦.md "wikilink")                |
| Elizabeth Arden奇蹟賦活系列代言人                                                      |                                                                 |
| 奧林比亞之星Olympia Star 台灣地區形象大使                                                   |                                                                 |
| 2013年                                                                         | 奧林比亞之星Olympia Star 台灣地區形象大使                                     |
| 2014年                                                                         | 奧林比亞之星Olympia Star 台灣地區形象大使                                     |
| 美肌之誌Beautymate 2014年品牌代言人                                                     |                                                                 |
| PANASONIC產品年度代言人                                                              |                                                                 |
| 第二屆MOD微電影創作大賽-頒獎大使                                                            |                                                                 |
| [屈臣氏](../Page/屈臣氏.md "wikilink")「蛋黃哥」集點活動代言人                                  |                                                                 |
| 《[國家地理頻道](../Page/國家地理頻道.md "wikilink")》-《I Like NAT GEO》2014最強精選節目推薦人        |                                                                 |
| 2015年                                                                         | 手機遊戲APP-白貓Project-年度代言人                                         |
| 快樂工作"努力築夢，一塊挺唐寶寶一圓就業夢，公益大使 and [王傳一](../Page/王傳一.md "wikilink")               |                                                                 |
| [Panasonic](../Page/Panasonic.md "wikilink")-家電新品搭載ECO NAVI 與 nanoe雙科技的冰箱與洗衣機 |                                                                 |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                               |                                                                 |
| [7-ELEVEN](../Page/7-ELEVEN.md "wikilink")-遇見冬天的暖氣團-暖心大使                      |                                                                 |
| 2016年                                                                         | [Panasonic](../Page/Panasonic.md "wikilink")-變頻空調（日本同步篇&未來篇）代言人 |
| [凡士林](../Page/凡士林.md "wikilink")-Vaseline凡士林潤膚噴霧系列產品代言人                       |                                                                 |
| 瑞士腕錶品牌Mido 美度表產品代言人                                                           |                                                                 |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                               |                                                                 |
| 2017年                                                                         | [Panasonic](../Page/Panasonic.md "wikilink")-家電                 |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                               |                                                                 |
| Ladies[蕾黛絲](../Page/蕾黛絲.md "wikilink")-內衣                                     |                                                                 |
| 2018年                                                                         | [Panasonic](../Page/Panasonic.md "wikilink")-家電                 |
| Divinia[蒂芬妮亞](../Page/蒂芬妮亞.md "wikilink")-保養全系列                               |                                                                 |
| Ladies[蕾黛絲](../Page/蕾黛絲.md "wikilink")-內衣                                     |                                                                 |

## 獎項

### 大型頒獎禮獎項

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>名稱</p></th>
<th style="text-align: left;"><p>獎項</p></th>
<th style="text-align: left;"><p>作品</p></th>
<th style="text-align: left;"><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><strong>2014</strong></p></td>
<td style="text-align: left;"><p>華劇大賞</p></td>
<td style="text-align: left;"><p>最佳螢幕情侶獎</p></td>
<td style="text-align: left;"><p>《<a href="../Page/我的自由年代.md" title="wikilink">我的自由年代</a>》 (與<a href="../Page/李國毅.md" title="wikilink">李國毅</a>)</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

## 外部連結

  -
  -
  -
  -
  -
  -
  -
  - [娛樂線官方部落格](http://blog.pixnet.net/badaclub)

  - [痞客邦個人相簿](https://web.archive.org/web/20080112153956/http://www.pixnet.net/home/Kirsten1122)

[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:臺灣新教徒](../Category/臺灣新教徒.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")
[Category:臺北市立中正高級中學校友](../Category/臺北市立中正高級中學校友.md "wikilink")
[Category:臺北市立蘭雅國民中學校友](../Category/臺北市立蘭雅國民中學校友.md "wikilink")
[Category:臺北市士林區芝山國民小學校友](../Category/臺北市士林區芝山國民小學校友.md "wikilink")
[Category:四川裔台灣人](../Category/四川裔台灣人.md "wikilink")
[Category:士林人](../Category/士林人.md "wikilink")
[R容萱](../Category/任姓.md "wikilink")