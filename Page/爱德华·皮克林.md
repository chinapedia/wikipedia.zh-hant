[Edward_Charles_Pickering_06050r.jpg](https://zh.wikipedia.org/wiki/File:Edward_Charles_Pickering_06050r.jpg "fig:Edward_Charles_Pickering_06050r.jpg")

**爱德华·查尔斯·皮克林**（，），美国天文学家。

## 生平

皮克林出生于[美国](../Page/美国.md "wikilink")[马萨诸塞州的](../Page/马萨诸塞州.md "wikilink")[波士顿](../Page/波士顿.md "wikilink")。皮克林1876年到1918年期间担任[哈佛大学天文台的台长](../Page/哈佛大学天文台.md "wikilink")，对19世纪末期美国的天文学发展产生了重要影响。在担任哈佛大学天文台台长期间，皮克林积极鼓励女性从事天文研究，招募了一些聋哑女性对天文台拍摄的照相底片进行测量和分类工作，被戏称为皮克林的后宫（參見[哈佛計算員](../Page/哈佛計算員.md "wikilink")，Harvard
Computers。）

她们当中很多人都在天文学上取得了重要的发现，其中包括主持恒星光谱分类的[安妮·坎农](../Page/安妮·坎农.md "wikilink")、发现[造父变星](../Page/造父变星.md "wikilink")[周光关系的](../Page/周光关系.md "wikilink")[亨丽爱塔·勒维特](../Page/亨丽爱塔·勒维特.md "wikilink")，發現馬頭星雲的[威廉敏娜·弗萊明](../Page/威廉敏娜·弗萊明.md "wikilink")、分析聯星漸台二光譜的[安東妮亞·莫里](../Page/安東妮亞·莫里.md "wikilink")。

他发起并主持编制了[亨利·德雷伯星表](../Page/亨利·德雷伯星表.md "wikilink")，提出用[色指数的方法对](../Page/色指数.md "wikilink")[恒星光谱进行分类](../Page/恒星光谱.md "wikilink")。皮克林还发现了第一个[分光双星](../Page/分光双星.md "wikilink")。

爱德华·皮克林曾获得1886年和1901年[英國皇家天文學會金質獎章](../Page/英國皇家天文學會金質獎章.md "wikilink")、1888年[亨利·德雷伯獎章](../Page/亨利·德雷伯獎章.md "wikilink")，以及1908年[布鲁斯奖](../Page/布鲁斯奖.md "wikilink")。

[Edward_Charles_Pickering's_Harem_13_May_1913.jpg](https://zh.wikipedia.org/wiki/File:Edward_Charles_Pickering's_Harem_13_May_1913.jpg "fig:Edward_Charles_Pickering's_Harem_13_May_1913.jpg")

爱德华·皮克林与美国另一位天文学家[威廉·亨利·皮克林是亲兄弟](../Page/威廉·亨利·皮克林.md "wikilink")。为纪念他们，[月球和](../Page/月球.md "wikilink")[火星上各有一座环形山以他们的姓氏](../Page/火星.md "wikilink")“皮克林”命名。第784号[小行星也命名为](../Page/小行星.md "wikilink")“皮克林”。

## 参见

  - [威廉·亨利·皮克林](../Page/威廉·亨利·皮克林.md "wikilink")
  - [哈佛大学天文台](../Page/哈佛大学天文台.md "wikilink")

[P](../Category/美国天文学家.md "wikilink")
[P](../Category/波士頓人.md "wikilink")
[P](../Category/哈佛大學教師.md "wikilink")
[P](../Category/英國皇家天文學會金質獎章獲得者.md "wikilink")
[Category:亨利·德雷伯奖章获得者](../Category/亨利·德雷伯奖章获得者.md "wikilink")
[Category:拉姆福德奖获得者](../Category/拉姆福德奖获得者.md "wikilink")