**瓜达卢佩·维多利亚**(**Guadalupe Victoria**) ()
原名**何塞·米格尔·拉蒙·阿道克托·曼努埃爾·費爾南德斯·費利克斯José
Miguel Ramón Adaucto Fernández y
Félix**。[墨西哥軍人和政治人物](../Page/墨西哥.md "wikilink")。他在1824年至1829年担任第一任[墨西哥总统](../Page/墨西哥总统.md "wikilink")。

## 早年

维多利亚曾在法律學校就讀，後離校參加墨西哥獨立運動。1811年參加[墨西哥獨立戰爭](../Page/墨西哥獨立戰爭.md "wikilink")，成為重要將領之一。1821年8月，支持[伊圖爾維德發起的](../Page/阿古斯汀一世_\(墨西哥\).md "wikilink")“伊瓜拉計畫”。為實現獨立，聯合“三保證軍”進據普埃布拉。1823年，參加推翻伊圖爾維德帝制的鬥爭，被選為墨西哥聯邦共和國總統，1824年就職。1827年，平肅了以副總統[尼古拉斯·布拉沃為首的保守派叛亂](../Page/副總統.md "wikilink")。1829年卸任。


[V](../Category/墨西哥總統.md "wikilink")
[Category:軍人出身的總統](../Category/軍人出身的總統.md "wikilink")
[Category:死于癫痫](../Category/死于癫痫.md "wikilink")