| headerstyle = text-align:left; background:; | labelstyle =
white-space: nowrap; width: 5em

| header1 =

<center>

\[ 官方网站\]

</center>

| label2 = 创办 | data2 =

| label3 = 地点 | data3 =

| label4 = 场地 | data4 =

| label5 = 地况 | data5 =

| label6 = 奖金 | data6 =

| label7 = 备注 | data7 =

| header10 =

| header11 =

| header12 =

| header13 =

| header14 = {{\#if: |

<center>

上届赛事

</center>

}} | data15 = {{\#if: | *[\<\!--]({{{Last}}}.md "wikilink")*}}

| header16 = {{\#if: |

<center>

目前赛事

</center>

}} | data17 = {{\#if: |
![Tennisball_current_event.svg](Tennisball_current_event.svg
"Tennisball_current_event.svg") *[}}}]({{{Current.md "wikilink")*}}
}}<noinclude>

## 語法參數

    {{GrandSlamTournaments
    | Name        =
    | Current     =
    | Logo        =
    | Logo size   =
    | Bar Color   =
    | City        =
    | Country     =
    | Venue       =
    | Surface     =
    | Men Draw    =
    | Women Draw  =
    | Mixed Draw  =
    | Prize Money =
    | Web site    =
    | Notes       =
    }}

## 模板顏色

  - 澳網：\#F9D251
  - 法網：\#FF915F
  - 溫網：\#C0D077
  - 美網：\#B2C8FF

[hu:Sablon:GrandSlamTenisz](hu:Sablon:GrandSlamTenisz.md "wikilink")
[sh:Template:Grand Slam](sh:Template:Grand_Slam.md "wikilink") [vi:Tiêu
bản:Các giải Grand Slam quần
vợt](vi:Tiêu_bản:Các_giải_Grand_Slam_quần_vợt.md "wikilink")
</noinclude>

[Category:網球模板](../Category/網球模板.md "wikilink")