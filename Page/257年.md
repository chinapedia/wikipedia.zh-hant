## 大事记

  - **[中国](../Page/中国.md "wikilink")**
      - [5月24日](../Page/5月24日.md "wikilink")，[曹魏武將](../Page/曹魏.md "wikilink")[諸葛誕於](../Page/諸葛誕.md "wikilink")[壽春叛亂](../Page/壽春.md "wikilink")，並聯結東吳為外援，東吳派遣文欽、唐諮、[全端與](../Page/全端.md "wikilink")[全懌等人帶領三萬人去拯救](../Page/全懌.md "wikilink")。
      - [三國時代](../Page/三國時代.md "wikilink")[蜀漢](../Page/蜀漢.md "wikilink")[中散大夫](../Page/中散大夫.md "wikilink")[譙周做](../Page/譙周.md "wikilink")[仇國論](../Page/仇國論.md "wikilink")，力陳[姜維北伐之失](../Page/姜維.md "wikilink")。
      - [姜維趁](../Page/姜維.md "wikilink")[諸葛誕叛亂發動](../Page/諸葛誕.md "wikilink")[第10次北伐](../Page/姜維北伐.md "wikilink")，進攻[秦川](../Page/秦川.md "wikilink")。魏軍據守不戰。
      - [孫亮親政](../Page/孫亮.md "wikilink")，孫綝害怕會對自己不利，回到[建業並稱病不上朝](../Page/建業.md "wikilink")。

<!-- end list -->

  - **[罗马帝国](../Page/罗马帝国.md "wikilink")**
      - [加里恩努斯及其父](../Page/加里恩努斯.md "wikilink")[瓦勒良共同成为](../Page/瓦勒良.md "wikilink")[罗马执政官](../Page/罗马执政官.md "wikilink")，整顿了[多瑙河地区的军团](../Page/多瑙河.md "wikilink")。
      - 未来的皇帝[奥勒良击败](../Page/奥勒良.md "wikilink")[哥特人](../Page/哥特人.md "wikilink")，并将俘虏带回[罗马](../Page/罗马.md "wikilink")。
      - [巴伐利亚](../Page/巴伐利亚.md "wikilink")[伊勒河段](../Page/伊勒河.md "wikilink")[日耳曼長城被](../Page/日耳曼長城.md "wikilink")[罗马人废弃](../Page/罗马人.md "wikilink")。
      - [皇帝瓦勒良从](../Page/罗马皇帝.md "wikilink")[萨珊波斯王](../Page/萨珊波斯.md "wikilink")[沙普尔一世手中收复了](../Page/沙普尔一世.md "wikilink")[叙利亚的](../Page/叙利亚.md "wikilink")[安条克](../Page/安条克.md "wikilink")。
      - [哥特人在](../Page/哥特人.md "wikilink")[黑海建造了一支舰队](../Page/黑海.md "wikilink")。
      - 哥特人分裂为[东哥特及](../Page/东哥特.md "wikilink")[西哥特](../Page/西哥特.md "wikilink")。

<!-- end list -->

  - 其它
      - 罗马皇帝[瓦勒良迫害](../Page/瓦勒良.md "wikilink")[基督教](../Page/基督教.md "wikilink")，他敕令所有主教及祭司要按罗马异教的方式献祭，并禁止基督教徒前往他们的墓地，否则处死。
      - [教宗斯德望一世及](../Page/教宗斯德望一世.md "wikilink")[迦太基主教](../Page/迦太基.md "wikilink")[居普良被逮捕处死](../Page/居普良.md "wikilink")。
      - [8月30日](../Page/8月30日.md "wikilink")，[教宗西斯笃二世继](../Page/教宗西斯笃二世.md "wikilink")[教宗斯德望一世成为第](../Page/教宗斯德望一世.md "wikilink")24任[教宗](../Page/教宗.md "wikilink")。

## 出生

  - [賈南風](../Page/賈南風.md "wikilink")，[晋惠帝皇后](../Page/晋惠帝.md "wikilink")。（[300年去世](../Page/300年.md "wikilink")，43岁）
  - [启蒙者额我略](../Page/格列高利_\(启蒙者\).md "wikilink")，[亚美尼亚使徒教会的](../Page/亚美尼亚使徒教会.md "wikilink")[主保聖人](../Page/主保聖人.md "wikilink")。

## 逝世

  - [張昌蒲](../Page/張昌蒲.md "wikilink")，[鍾會生母](../Page/鍾會.md "wikilink")。（[199年出生](../Page/199年.md "wikilink")）
  - [盧毓](../Page/盧毓.md "wikilink")，曹魏官員。（[183年出生](../Page/183年.md "wikilink")）
  - [樂綝](../Page/樂綝.md "wikilink")，曹魏將領。
  - [朱異](../Page/朱異.md "wikilink")，東吳將領。（）
  - [8月2日](../Page/8月2日.md "wikilink")，[教宗斯德望一世](../Page/教宗斯德望一世.md "wikilink")

[\*](../Category/257年.md "wikilink")
[7年](../Category/250年代.md "wikilink")
[5](../Category/3世纪各年.md "wikilink")