**堀江贵文**（）出生於[日本](../Page/日本.md "wikilink")[九州](../Page/九州.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink")[八女市](../Page/八女市.md "wikilink")，是日本知名[门户网站](../Page/门户网站.md "wikilink")“[Livedoor](../Page/Livedoor.md "wikilink")”（中譯：“[活力門](../Page/活力門.md "wikilink")”）的前[總經理](../Page/總經理.md "wikilink")。因長得跟[哆啦A夢](../Page/哆啦A夢.md "wikilink")（Doraemon）相似，而被暱稱為“Horiemon”。

## 生平

堀江的父親是[上班族](../Page/上班族.md "wikilink")，母親來自農家。他就讀於[久留米大學附設](../Page/久留米大學.md "wikilink")[高中](../Page/高中.md "wikilink")，之後考上[東京大學文學院](../Page/東京大學.md "wikilink")，但在1995年與兩名東京大學同學集資600萬[日元成立只有七](../Page/日元.md "wikilink")[坪大的](../Page/坪.md "wikilink")[網頁製作](../Page/網頁製作.md "wikilink")[工作室](../Page/工作室.md "wikilink")“Livin'
on the
Edge”之後就[輟學](../Page/輟學.md "wikilink")。他的公司迅速發展，在2000年時成功登上[東京證交所](../Page/東京證交所.md "wikilink")，成為一家[上市公司](../Page/上市公司.md "wikilink")，也就是現在的[LiveDoor](../Page/LiveDoor.md "wikilink")。

2005年9月，堀江受到[自民黨的邀請](../Page/自由民主党_\(日本\).md "wikilink")，在[廣島縣第6區參選](../Page/廣島縣第6區.md "wikilink")[第44屆日本眾議院議員總選舉](../Page/第44屆日本眾議院議員總選舉.md "wikilink")；但是最後敗於[無黨籍參選人](../Page/無黨籍.md "wikilink")[龜井靜香](../Page/龜井靜香.md "wikilink")，無緣進入政壇。

2006年1月以前，少年得志的堀江難免盛氣凌人，私人生活極盡奢華之能事，還不時宣揚「[金錢萬能](../Page/金錢萬能.md "wikilink")」的驚人豪語，[拜金主義的作風引人側目](../Page/拜金主義.md "wikilink")。他在日本國內以[法拉利](../Page/法拉利.md "wikilink")[跑車代步](../Page/跑車.md "wikilink")，旅行時搭的是[私人飛機](../Page/私人飛機.md "wikilink")。

2006年4月27日，堀江被[羈押約三個月後](../Page/羈押.md "wikilink")，被以三億日圓[交保](../Page/交保.md "wikilink")，從此幾乎足不出戶的待在[東京都](../Page/東京都.md "wikilink")[港區](../Page/港區.md "wikilink")[六本木新城的寓所內](../Page/六本木新城.md "wikilink")，生活低調。

堀江曾和E[罩杯](../Page/罩杯.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")[西村美保同居](../Page/西村美保.md "wikilink")，也曾和女星[吉川雛乃傳過](../Page/吉川雛乃.md "wikilink")[緋聞](../Page/緋聞.md "wikilink")。他在1999年跟LiveDoor的一位女職員[奉子成婚](../Page/奉子成婚.md "wikilink")，但這段婚姻只維持了兩年。

淈江也是一名太空發展推動者，發表過不少言論支持發展火箭及太空科技。而他自己在受到邀請後亦在1999年加入由[淺利義遠](../Page/淺利義遠.md "wikilink")、[野田篤司](../Page/野田篤司.md "wikilink")、[笹本祐一等人組成的夏天火箭團](../Page/笹本祐一.md "wikilink")，並親手製作[液態燃料火箭](../Page/液態燃料火箭.md "wikilink")。在火箭引擎一號機初步測試成功之後，堀江成立[SNS公司以得到更正式的對外溝通途徑](../Page/SNS公司.md "wikilink")。雖然夏天火箭團的火箭一號在[北海道發射時他因為要在東京召開相關記者會而未能親眼見證](../Page/北海道.md "wikilink")，二號至五號機發展時他正被監禁中，但是在六號機發射時他有在現場出現。\[1\]

## 事業發展

"Livin' on the
Edge"在2000年4月於[東京證券交易所的Mothers板](../Page/東京證券交易所.md "wikilink")（新興企業板）股票上市。在2002年，堀江收購了原本提供免費撥接上網但卻陷入財務困境的網路公司Livedoor，並且在2004年2月將自己的公司改名為「」（Livedoor
Co.,Ltd.），總部設於[東京都](../Page/東京都.md "wikilink")[港區](../Page/港區.md "wikilink")[六本木新城](../Page/六本木新城.md "wikilink")，並且陸續購併了25家公司，迅速擴張規模。並且在2004年6月試圖收購[日本職棒](../Page/日本職棒.md "wikilink")[近鐵野牛隊](../Page/歐力士野牛.md "wikilink")，在2005年試圖收購[富士電視台而與公司派大股東發生股權爭奪戰](../Page/富士電視台.md "wikilink")，因衝擊日本商界傳統倫理而聞名於日本。在2005年時，公司規模已經擴張到2500人，營業額800億日幣。

## 經營手法

堀江經營公司的手法，極注重[數字管理](../Page/數字管理.md "wikilink")，嚴控公司的支出，沒有一般日本企業交際費用浮濫的情形。他也採[實力主義](../Page/實力主義.md "wikilink")，只要是業績表現優異的員工，就會很快被拔擢。

## 陷入官司

2006年1月16日，Livedoor辦公室以及堀江的住宅遭日本檢方搜索，原因是2004年Livedoor購併一家出版公司時做了不實[財報](../Page/財報.md "wikilink")。數天之後1月23日，堀江及同公司三名高階主管被以違反《[證券交易法](../Page/證券交易法_\(日本\).md "wikilink")》遭逮捕。此事件被稱為[活力門危機](../Page/活力門事件.md "wikilink")（Livedoor
shock），並且被檢方視為重大[經濟犯罪事件](../Page/經濟犯罪.md "wikilink")。隨後，堀江請辭總經理一職，由Livedoor旗下財務會計軟體公司“彌生”的社長[平松庚三接任](../Page/平松庚三.md "wikilink")，法人代表也換成[熊谷史人](../Page/熊谷史人.md "wikilink")。

堀江被[羈押三個月後](../Page/羈押.md "wikilink")，在4月27日繳交3億[日元保釋金獲得交保](../Page/日元.md "wikilink")。同年9月4日，堀江首度到[東京地方法院出庭應訊](../Page/東京地方法院.md "wikilink")。他辯稱無罪，也沒有指示部屬去做。他並指稱，檢方的起訴書充滿惡意內容，讓他很意外。

2007年3月16日上午，東京地方法院作出一審宣判，判處堀江貴文[有期徒刑兩年六個月](../Page/有期徒刑.md "wikilink")\[2\]，不得[緩刑](../Page/緩刑.md "wikilink")；堀江貴文堅稱自己無罪，揚言將提出上訴。該日下午，堀江貴文以五億日元保釋金獲得交保。

2011年4月26日，[最高裁判所二审驳回堀江上诉](../Page/最高裁判所.md "wikilink")，判處堀江貴文有期徒刑兩年六個月定讞\[3\]。2011年6月20日，堀江正式收监，在[長野刑務所服刑](../Page/長野刑務所.md "wikilink")\[4\]。

2013年3月27日，堀江获得[假释](../Page/假释.md "wikilink")，召开[新闻发布会道歉并称](../Page/新闻发布会.md "wikilink")“给大家添麻烦了”\[5\]\[6\]。

## 著作

  - 《HTML4.0對應
    網頁製作實務技巧》（），[技術評論社](../Page/技術評論社.md "wikilink")1998年11月出版，ISBN
    4774106631
  - 《賺錢，是最重要的美德》（），[光文社](../Page/光文社.md "wikilink")2004年8月7日出版，ISBN
    4334974600
  - 《買下職業棒球隊！》（），あうん2004年9月出版，ISBN 4901318209
  - 《價值100億的工作智慧》（），[SoftBank
    Creative](../Page/SoftBank_Creative.md "wikilink") 2005年2月19日出版，ISBN
    4797330848
  - 《我不死》（），[活力門出版](../Page/活力門出版.md "wikilink")2005年3月出版，ISBN
    4779400007
  - 《堀江式英文單字學習手冊》（），活力門出版2005年3月出版，ISBN 4779400015
  - 《賺錢入門：價值100億的思考方法》（），[PHP研究所](../Page/PHP研究所.md "wikilink")2005年3月12日出版，ISBN
    456964094X
  - 《堀江本：2004.1.1－2005.2.28 活力門震撼的400日！》（），[Goma
    Books](../Page/Goma_Books.md "wikilink")2005年3月18日出版，ISBN 4777101185
  - 《堀江貴文的新資本主義》（），光文社2005年4月26日出版，ISBN 4334974813
  - 《堀江式英文片語學習手冊》（），活力門出版2005年6月1日出版，ISBN 4779400074
  - 《決勝的網際網路[PageRank術](../Page/PageRank.md "wikilink")》（），與[神原彌奈子合著](../Page/神原彌奈子.md "wikilink")，[日經BP出版中心](../Page/日經BP出版中心.md "wikilink")2005年6月23日出版，ISBN
    4822244598
  - 《堀江的出乎意料美食店》（），活力門出版2005年7月出版，ISBN 4779400090
  - 《堀江式速讀英語訓練》（），活力門出版2005年7月30日出版，ISBN 4779400112
  - 《徹底抗戰》（），[集英社](../Page/集英社.md "wikilink")2009年3月5日出版，ISBN
    978-4087805185
  - 《新資本論：我知道金錢的本質了》（），[寶島社](../Page/寶島社.md "wikilink")2009年7月10日出版，ISBN
    978-4796672207

等

## 参考文献

## 外部連結

  - [堀江貴文日記](http://blog.livedoor.jp/takapon_ceo/)（日文，到2006年1月22日止，但已刪除）

  - [ホリエモンドットコム│堀江貴文](http://horiemon.com/) - 堀江貴文正式网页。（日文，2013年11月～）

  - （日文）

## 參見

  - [活力門](../Page/活力門.md "wikilink")
  - [證券交易法](../Page/證券交易法.md "wikilink")
  - [富士電視台](../Page/富士電視台.md "wikilink")
  - [名人DNA](../Page/名人DNA.md "wikilink")

{{-}}

[Category:日本企业家](../Category/日本企业家.md "wikilink")
[Category:日本億萬富豪](../Category/日本億萬富豪.md "wikilink")
[Category:日本經濟犯罪罪犯](../Category/日本經濟犯罪罪犯.md "wikilink")
[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:日本男性電視藝人](../Category/日本男性電視藝人.md "wikilink")
[Category:福岡縣出身人物](../Category/福岡縣出身人物.md "wikilink")

1.  ISBN 978-4-05-405702-9
2.  [Horie handed 2 1/2
    years](http://search.japantimes.co.jp/cgi-bin/nn20070317a1.html)
    日本時報。
3.  [朝日新闻-2011年4月27日-ホリエモン獄中からメルマガ　無罪主張](http://www.asahi.com/showbiz/nikkan/NIK201104270050.html)
4.  [朝日新闻-2011年6月21日-ホリエモン仕掛け成功\!?「刑務所に行け」](http://www.asahi.com/showbiz/nikkan/NIK201106210045.html)
5.  [朝日新闻腾讯微博-2013年3月28日-“活力门”前社长获假释](http://t.qq.com/p/t/188231104043724)
6.  [朝日新闻中文网-2013年3月28日-“活力门”前社长获假释](http://asahichinese.com/article/news/AJ201303280034)