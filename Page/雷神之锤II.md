《**雷神之锤II**》（），是[id
Software于](../Page/id_Software.md "wikilink")1997年冬发布的一款划时代的[第一人稱射擊遊戲作品](../Page/第一人稱射擊遊戲.md "wikilink")。这个游戏是[id
Software在其创始人之一](../Page/id_Software.md "wikilink")、著名游戏策划设计师[约翰·罗梅洛](../Page/约翰·罗梅洛.md "wikilink")（）离开后发布的第一个游戏。游戏的引擎被[约翰·卡马克](../Page/约翰·卡马克.md "wikilink")（）完全重写，实现了第一代游戏无法比拟的效果。并且首次实现了彩色光影效果，使得游戏中的物体在反射光影时候能展现出不同样的色彩效果。

雷神之锤II的关卡设计是由id
Software的新任地图设计师[提姆·威利兹](../Page/提姆·威利兹.md "wikilink")（）完成，展现了与[雷神之锤I完全不同的风格](../Page/雷神之锤I.md "wikilink")。单人游戏中充满了陷阱和机会，玩家在享受绚丽效果和屠杀乐趣的同时可以拥有解谜的乐趣。

雷神之锤II还发布了针对[任天堂64的版本](../Page/任天堂64.md "wikilink")，同样取得了巨大成功。其在Gamespot上面的评分比[PC版本还要高出](../Page/PC.md "wikilink")0.2分。

## 单人游戏

### 游戏背景

### 游戏关卡

### 敌方怪物

#### Shotgun Guard

#### Light Guard

#### Machine-gun Guard

#### Enforcer

#### Parasite

#### Gunner

#### Flyer

#### Barracuda Shark

#### Technician

#### Berserker

#### Tank

#### Gladiator

#### Icarus

#### Medic

#### Mutant

#### Brains

#### Iron Maiden

#### Tank Commander

## 游戏系统

### 物理移动

### 武器系统

|                     | 子弹       | 简介                                                        | 备注                                                                                                                                  |
| ------------------- | -------- | --------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| 爆能槍（Blaster）        | N/A      | 初始武器，无限弹药。威力比单颗机枪子弹大，有弹道飞行的时间。                            | 雷神之锤II取消了[前作的近战武器](../Page/雷神之锤_\(游戏\).md "wikilink")——双面战斧，取而代之的是远程武器爆能枪。接下来的[雷神之锤III又恢复了近战武器这一设定](../Page/雷神之锤III.md "wikilink")。 |
| 霰彈槍                 | Shells   | 游戏前期较为依赖的武器。射速远不及前作。                                      |                                                                                                                                     |
| 雙管霰彈槍               | Shells   | 一发射出两颗霰弹。弹道分布密集。                                          |                                                                                                                                     |
| 輕機槍                 | Bullets  | 手感轻便，射击时有明显的跳枪手感。                                         |                                                                                                                                     |
| 轉輪機槍                | Bullets  | 按住射击键射速极快，弹药消耗量巨大。                                        | [雷神之锤III将其改为初始武器](../Page/雷神之锤III.md "wikilink")。                                                                                   |
| 手榴彈                 | Grenades | 抛物线弹道。按住射击键会蓄力，投掷时没有声音。一直按住不放会在手上爆炸。                      | 蓄力投掷这一设定被之后的许多[FPS游戏继承](../Page/第一人称射击游戏.md "wikilink")。                                                                            |
| 榴彈發射器               | Grenades | 抛物线弹道。用来攻击难以到达的区域或清除潜在的伏击。                                | 与火箭炮一样，该武器设定贯穿了整个雷神之锤系列。                                                                                                            |
| 火箭發射器               | Rockets  | 每一发火箭会给目标带来沉重的伤害，玩家也可以以伤害自己为代价，利用火箭的爆炸推动效果到达通常情况下无法到达的地方。 | 火箭发射器一向是雷神之锤系列的灵魂。游戏进行到第九关的某地图，利用火箭跳到达某隐藏地点会有如下提示：                                                                                  |
| 等離子槍（Hyper Blaster） | Cells    | 有弹道飞行时间。伤害无视护甲。                                           |                                                                                                                                     |
| 電磁炮（Rail Gun）       | Slugs    | 射速慢，伤害沉重。无弹道飞行时间。                                         |                                                                                                                                     |
| BFG10K              | Cells    | 重型能量武器，每发打出50单位Cells.                                     | 由[毁灭戰士中的武器BFG](../Page/毁灭戰士.md "wikilink")9000继承而来。                                                                                 |

### 物品系统

### 地图机关

## 多人游戏

## 开发背景

## 所获荣誉

## 外部链接

  - [Official
    website](https://web.archive.org/web/20110824141015/http://www.idsoftware.com/games/quake/quake2/)
  - [Demo, patches and other resources on id Software's
    FTP](ftp://ftp.idsoftware.com/idstuff/quake2/)
  - [Source code of the engine
    version 3.21](ftp://ftp.idsoftware.com/idstuff/source/q2source-3.21.zip)
  - [*Quake II* at PlanetQuake](http://planetquake.gamespy.com/quake2/)
  - [Quake2DS Official
    website](https://web.archive.org/web/20090803124038/http://quake.drunkencoders.com/index_q2.html)
  - [雷神之鎚論壇 - QUAKE.iDV.TW](http://quake.idv.tw/)

[Quake](../Category/雷神之锤系列.md "wikilink")
[Quake](../Category/1997年电子游戏.md "wikilink")
[Quake](../Category/第一人称射击游戏.md "wikilink")
[Quake](../Category/电脑游戏.md "wikilink")
[Quake](../Category/Mac_OS遊戲.md "wikilink")
[Quake](../Category/Linux遊戲.md "wikilink")
[Quake](../Category/PlayStation_\(游戏机\)游戏.md "wikilink")
[Quake](../Category/任天堂64遊戲.md "wikilink")
[Category:动视游戏](../Category/动视游戏.md "wikilink")
[Quake](../Category/电子竞技游戏.md "wikilink")