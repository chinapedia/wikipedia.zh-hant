**所罗门群岛**（英语：Solomon
Islands）是南[太平洋的一個岛国](../Page/太平洋.md "wikilink")，位於[澳大利亚东北方](../Page/澳大利亚.md "wikilink")，[巴布亞紐幾內亞東方](../Page/巴布亞紐幾內亞.md "wikilink")，是[英联邦成员之一](../Page/英联邦.md "wikilink")。首都[霍尼亞拉所在地](../Page/霍尼亞拉.md "wikilink")[瓜達康納爾島曾是](../Page/瓜達康納爾島.md "wikilink")[第二次世界大戰在](../Page/第二次世界大戰.md "wikilink")[太平洋的转折点所在地](../Page/太平洋.md "wikilink")。\[1\]

所罗门群岛共有超過990個島，陸地總面積共有28,450平方公里，人口64万。所罗门群岛是世界上[最不发达国家之一](../Page/最不发达国家.md "wikilink")，2017年数据的[人類發展指數為](../Page/人類發展指數.md "wikilink")0.546。

## 地理

[Solomon_Isles.jpg](https://zh.wikipedia.org/wiki/File:Solomon_Isles.jpg "fig:Solomon_Isles.jpg")
所罗门群岛是一个辽阔的[岛国](../Page/岛国.md "wikilink")，位于[巴布亚新几内亚的东边](../Page/巴布亚新几内亚.md "wikilink")，西临[所罗门海](../Page/所罗门海.md "wikilink")，西南临[珊瑚海](../Page/珊瑚海.md "wikilink")，由多个岛屿组成。包括：[乔伊索岛](../Page/舒瓦瑟爾島.md "wikilink")、[肖特兰群岛](../Page/肖特兰群岛.md "wikilink")、[新喬治亞群島](../Page/新喬治亞群島.md "wikilink")、[圣伊莎贝尔岛](../Page/圣伊莎贝尔岛.md "wikilink")、[拉塞尔群岛](../Page/拉塞尔群岛.md "wikilink")、[佛罗里达群岛](../Page/佛罗里达群岛.md "wikilink")、[马莱塔岛](../Page/马莱塔岛.md "wikilink")、[瓜达尔卡纳尔岛](../Page/瓜达尔卡纳尔岛.md "wikilink")、、、Ulawa、Uki、[马基拉岛](../Page/马基拉岛.md "wikilink")、Santa
Ana、[拉纳尔岛](../Page/拉纳尔岛.md "wikilink")、[贝罗纳岛及](../Page/贝罗纳岛.md "wikilink")[聖塔克魯茲群島](../Page/圣克鲁斯群岛.md "wikilink")。

岛屿多为[火山岛](../Page/火山岛.md "wikilink")，不同活动程度的[火山位于一些较大的岛屿](../Page/火山.md "wikilink")，而许多较小的岛屿只是由沙子与[棕榈树覆盖的小](../Page/棕榈树.md "wikilink")[環礁島](../Page/環礁島.md "wikilink")。最西与最东之间的岛屿大约相距1500公里。位于[瓦努阿图以北的聖塔克魯茲群島远离其他岛屿多于](../Page/瓦努阿图.md "wikilink")200公里。最大的瓜达尔卡纳尔岛面积达6475平方千米，岛上有全国最高峰波波马纳休山，海拔2330米。\[2\]

气候为[赤道多雨气候](../Page/赤道多雨气候.md "wikilink")，终年炎热，盛行[赤道海洋气团](../Page/赤道海洋气团.md "wikilink")，无[旱季](../Page/旱季.md "wikilink")。年均降水量在3000毫米左右。\[3\]

所罗门群岛由兩組截然不同的[陆地生态区組成](../Page/陆地生态区.md "wikilink")。當中大多数岛屿，連同属于[巴布亚新几内亚的領土的](../Page/巴布亚新几内亚.md "wikilink")[布干维尔岛及](../Page/布干维尔岛.md "wikilink")[布卡岛](../Page/布卡岛.md "wikilink")，都属于[所羅門群島雨林生態區](../Page/所羅門群島雨林生態區.md "wikilink")；聖塔克魯茲群島是所罗门东边主要的一群，與臨近的[瓦努阿图群島屬於](../Page/瓦努阿图.md "wikilink")[瓦努阿图雨林生态区](../Page/瓦努阿图雨林生态区.md "wikilink")。这两个生态区，連同鄰近的[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[俾斯麦群岛](../Page/俾斯麦群岛.md "wikilink")、[新几内亚](../Page/新几内亚.md "wikilink")、[澳大利亚和](../Page/澳大利亚.md "wikilink")[新西兰](../Page/新西兰.md "wikilink")，都屬於[澳亞大陸生態區的範圍](../Page/澳亞大陸生態區.md "wikilink")。\[4\]

## 历史

### 殖民时期

[Marines_rest_in_the_field_on_Guadalcanal.jpg](https://zh.wikipedia.org/wiki/File:Marines_rest_in_the_field_on_Guadalcanal.jpg "fig:Marines_rest_in_the_field_on_Guadalcanal.jpg")期间[美國海軍陸戰隊在](../Page/美國海軍陸戰隊.md "wikilink")[瓜達康納爾島戰役期間在野外休息](../Page/瓜達康納爾島戰役.md "wikilink")。\]\]
[美拉尼西亞人最先来到所罗门群岛定居](../Page/美拉尼西亞人.md "wikilink")。16世纪中叶开始，欧洲人相继发现所罗门群岛。1568年，[西班牙航海探险家](../Page/西班牙.md "wikilink")[阿尔瓦罗·德·门达尼亚到此](../Page/阿尔瓦罗·德·门达尼亚.md "wikilink")，见到当地人均身佩黄金装饰物，认为是找到了[所罗门王巨大财富的金库](../Page/所罗门王.md "wikilink")，故命名为所罗门群岛。\[5\]此后的两个世纪中，所罗门群岛与外部世界联系中断。1767年，[英国航海家](../Page/英国.md "wikilink")[菲利普·卡特列特重新发现了所罗门群岛](../Page/菲利普·卡特列特.md "wikilink")。1768年，[法国探险家](../Page/法国.md "wikilink")[路易斯·安托万·布干维尔发现了所罗门群岛北部岛屿](../Page/路易斯·安托万·布干维尔.md "wikilink")。

1885年，北所罗门成为德国“保护地”，同年转归英国（布卡和布干维尔岛除外）。1893年“英属所罗门群岛保护地”成立。[二次大战期间](../Page/二次大战.md "wikilink")，[日军于](../Page/日军.md "wikilink")1942年1月攻占所羅門群島，并試圖以此為基地向南進攻澳大利亞。同年8月7日，以美國为首的盟軍部隊在[所羅門群島的瓜達爾卡納爾島](../Page/所羅門群島.md "wikilink")、[圖拉吉島和](../Page/圖拉吉島.md "wikilink")[恩格拉群島登陸](../Page/恩格拉群島.md "wikilink")。开启了長達6個月的[瓜達爾卡納爾島戰役](../Page/瓜達爾卡納爾島戰役.md "wikilink")。1943年，日軍戰敗，[山本五十六在指揮撤退時](../Page/山本五十六.md "wikilink")，其座機在瓜島上空被擊落。1945年，日軍全部撤出所羅門群島。\[6\]

### 独立自治

1975年6月22日，英属所罗门群岛更名为所罗门群岛。1976年1月2日实行自治。1976年6月，举行了立法会议大选。1978年7月7日获得独立，成为[英联邦成员国](../Page/英联邦.md "wikilink")。1999年，瓜達爾卡納爾島土著居民同馬萊塔島的移民間發生內戰，雙方於當年6月28日達成停火協議。

2006年4月，首都霍尼亚拉因为对新任总理选举结果出现争议，再次爆发了大规模暴动\[7\]。冲突中，大批當地華僑及來自[中國的新移民遭受殃及](../Page/中國.md "wikilink")\[8\]\[9\]。但根據香港新聞節目訪問當地僑民，台商在該島只有兩家企業，並位於島的另一個地方，所以未受波及。

2007年12月13日，所罗门群岛议会以25票对22票通过对总理梅纳西·索格瓦雷的不信任案。索格瓦雷成为这个南太平洋岛国自1978年独立以来第一个因议会不信任投票下台的总理。
在当天的议会会议上，一名索格瓦雷内阁前部长以总理严重损害所罗门群岛外交关系为由，对索格瓦雷提出不信任动议，并获得通过。所罗门群岛议会12月20日选举德里克·西库阿担任这个南太平洋岛国的总理，接替因议会不信任投票下台的梅纳西·索格瓦雷。支持西库阿的党派当天获得了议会47张选票中的32张赞成票。西库阿时年48岁，哲学博士，曾担任教育与人力资源开发部长。\[10\]

## 政治

[Solomon_Islands_House_of_Parliament_(inside).jpg](https://zh.wikipedia.org/wiki/File:Solomon_Islands_House_of_Parliament_\(inside\).jpg "fig:Solomon_Islands_House_of_Parliament_(inside).jpg")
现行宪法是1978年7月7日独立时颁布的宪法。宪法规定，国体为君主立宪制，女王[伊丽莎白二世为](../Page/伊丽莎白二世.md "wikilink")[国家元首](../Page/国家元首.md "wikilink")，由[所罗门群岛总督代行職務](../Page/所罗门群岛总督.md "wikilink")。按照慣例，总督必须是所罗门群岛公民，由议会提名，女王任命，任期五年。一院制的国民议会行使立法权，[总理领导下的内阁行使行政权](../Page/所罗门群岛总理.md "wikilink")，总理由议会从议员中选举产生，部长由总督根据总理的推荐任命。

所罗门群岛政府以弱的政黨和非常不穩定的議會聯盟為特點。他們受頻繁的[不信任投票影響](../Page/不信任投票.md "wikilink")，內閣因此經常[改組](../Page/改組.md "wikilink")。

### 国旗及国徽

所罗门群岛国旗呈横长方形，长宽比为9:5。旗地由浅蓝、绿色两个三角形构成。一道黄色宽条从靠旗杆一边的下旗角与旗地右上角连接。靠旗杆一边的浅蓝色部分有五颗白色的五角星，上、下排各两颗，中间为一颗。浅蓝色象征蓝天和环绕所罗门群岛的海洋，绿色象征覆盖这个岛国的森林。五颗星象征组成这个岛国的五个区域岛群（即东、西、中央、马莱塔和外围的岛群）。

国徽中心图案为盾徽，盾面上蓝、黄、绿三色是国旗颜色。图案分别代表该国的五个区域：[军舰鸟代表东区](../Page/军舰鸟.md "wikilink")，[鹰代表马莱塔区](../Page/鹰.md "wikilink")，[海龟代表西区](../Page/海龟.md "wikilink")，[长矛及](../Page/长矛.md "wikilink")[弓箭和](../Page/弓箭.md "wikilink")[盾牌象征中央区和其他地区](../Page/盾牌.md "wikilink")。盾徽两侧为[鳄鱼和](../Page/鳄鱼.md "wikilink")[鲨鱼](../Page/鲨鱼.md "wikilink")，上端为太阳、船型和头盔，基部为一只军舰鸟的造型，黄色饰带上用英文写着“领导就是服务”。\[11\]

## 外交

所罗门群岛属于[英联邦](../Page/英联邦.md "wikilink")\[12\]，

### 與關係

1983年3月24日，[中華民國與所羅門群島建立](../Page/中華民國.md "wikilink")[總領事級外交關係](../Page/中華民國－索羅門群島關係.md "wikilink")。目前所羅門群島为中華民國17个邦交國之一。

## 行政區劃

[缩略图](https://zh.wikipedia.org/wiki/File:Honiara_general_view.jpg "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Mobile_Phone_Tower_\(31811063564\).jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Roadside_Stand_\(31811063104\).jpg "fig:缩略图")
全国分为9个省、1市。

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>省</p></th>
<th><p>首府</p></th>
<th><p>地图</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/中部群島省_(所羅門群島).md" title="wikilink">中部群岛</a><br />
<small>Central Islands</p></td>
<td><p><a href="../Page/图拉吉岛.md" title="wikilink">图拉吉</a><br />
<small>Tulagi</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:SolomonIslandsMap.png" title="fig:SolomonIslandsMap.png">SolomonIslandsMap.png</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/舒瓦瑟爾省.md" title="wikilink">乔伊索</a><br />
<small>Choiseul（Lauru）</p></td>
<td><p><br />
<small>Taro</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/瓜达尔卡纳尔.md" title="wikilink">瓜达尔卡纳尔</a><br />
<small>Guadalcanal</p></td>
<td><p><a href="../Page/霍尼亚拉.md" title="wikilink">霍尼亚拉</a><br />
<small>Honiara</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/霍尼亚拉.md" title="wikilink">霍尼亚拉</a>（首都直辖区）<br />
<small>Honiara</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="../Page/伊莎貝爾省.md" title="wikilink">伊莎貝爾</a><br />
<small>Isabel</p></td>
<td><p><br />
<small>Buala</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/馬基拉-烏拉瓦省.md" title="wikilink">马基拉</a><br />
<small>Makira</p></td>
<td><p><a href="../Page/基拉基拉.md" title="wikilink">基拉基拉</a><br />
<small>Kirakira</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><a href="../Page/馬萊塔省.md" title="wikilink">马莱塔</a><br />
<small>Malaita</p></td>
<td><p><br />
<small>Auki</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p><a href="../Page/拉納爾和貝羅納省.md" title="wikilink">拉纳尔和贝罗纳</a><br />
<small>Rennell and Bellona</p></td>
<td><p><br />
<small>Tigoa</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><a href="../Page/泰莫图.md" title="wikilink">泰莫图</a><br />
<small>Temotu</p></td>
<td><p><br />
<small>Lata</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="../Page/西部省_(所羅門群島).md" title="wikilink">西部省</a><br />
<small>Western</p></td>
<td><p><br />
<small>Gizo</p></td>
<td></td>
</tr>
</tbody>
</table>

## 经济

[Solomon_Islands_Exports_Treemap_(2009)..jpg](https://zh.wikipedia.org/wiki/File:Solomon_Islands_Exports_Treemap_\(2009\)..jpg "fig:Solomon_Islands_Exports_Treemap_(2009)..jpg")
大多数人口依靠务农、捕鱼和种植为生，主要产品有[椰肉](../Page/椰子.md "wikilink")、[木材](../Page/木材.md "wikilink")、[大米](../Page/大米.md "wikilink")、[可可](../Page/可可.md "wikilink")、[加工鱼](../Page/渔业.md "wikilink")、[甘薯](../Page/甘薯.md "wikilink")、[大蕉](../Page/大蕉.md "wikilink")、[菠萝和](../Page/菠萝.md "wikilink")[马蹄螺的壳](../Page/马蹄螺.md "wikilink")（用作饰物）。国民经济以[种植业](../Page/种植业.md "wikilink")、[渔业和](../Page/渔业.md "wikilink")[黄金开采为主](../Page/黄金.md "wikilink")。大部分制造与[石油产品依赖进口](../Page/石油.md "wikilink")。该群岛尚未开发的礦产资源丰富，如[铅](../Page/铅.md "wikilink")、[锌](../Page/锌.md "wikilink")、[镍以及](../Page/镍.md "wikilink")[金](../Page/金.md "wikilink")。\[13\]

1990年代初，木材曾经占所羅門群島出口收入的一半。但由于森林砍伐已处于不可持续发展的状态，政府于1994年頒發禁令，自1997年起禁止採伐森林。1998年因东南亚经济萧条又导致其木材工业陡然进入低迷期，国民经济因此下降约10%，政府亦为此削减了公共服务等方面的费用。1999年，得益于国际[金价的上扬和](../Page/金价.md "wikilink")[金岭金矿](../Page/金岭金矿.md "wikilink")（Gold
Ridge）的首次全年开采，国民经济得以部分恢复。然而，该国主要[棕榈油种植地于](../Page/棕榈油.md "wikilink")1999年年中关闭，这又给其经济前景蒙上了一层阴影。

## 宗教

所罗门群岛的居民所信的宗教96%是[基督教](../Page/基督教.md "wikilink")（其中45%是[聖公會](../Page/聖公會.md "wikilink")，18%是[羅馬](../Page/羅馬.md "wikilink")[天主教](../Page/天主教.md "wikilink")，12%是United，10%是[浸信會](../Page/浸信會.md "wikilink")，7%是[安息日會](../Page/安息日會.md "wikilink")，4%其他宗派教會）以及大約4%當地土著信仰。

## 另见

  - [大英國協](../Page/大英國協.md "wikilink")

## 註解

[\*](../Category/索羅門群島.md "wikilink")
[S所](../Category/大洋洲岛国.md "wikilink")
[Category:前英國殖民地](../Category/前英國殖民地.md "wikilink")
[S](../Category/君主立憲國.md "wikilink")

1.  [所羅門戰場參觀飛機大炮](http://www.wenweipo.com/news.phtml?news_id=FD0603020001&loc=any&cat=220FD&no_combo=1)

2.
3.  [World Weather Information
    Service](http://www.worldweather.org/139/c00295.htm)
4.
5.  [所罗门骚乱夷平唐人街
    澳宣布出兵干预](http://abroad.163.com/06/0420/11/2F58IQAM00271J4D.html)
6.  Hogue, *Pearl Harbor to Guadalcanal*, p. 235–236.
7.
8.
9.
10.
11.
12.
13.