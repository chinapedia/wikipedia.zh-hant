《**Black
Cherry**》(黑色櫻桃)是[日本歌手](../Page/日本.md "wikilink")[倖田來未的第](../Page/倖田來未.md "wikilink")5張原創專輯，收錄《[戀愛花蕾](../Page/戀愛花蕾.md "wikilink")》、《[4
hot wave
熱浪四射](../Page/4_hot_wave.md "wikilink")》、《[悲夢之歌/我倆...](../Page/悲夢之歌/我倆....md "wikilink")》、《[Cherry
Girl/命運](../Page/Cherry_Girl/命運.md "wikilink")》以及與EXILE合唱的《[WON'T BE
LONG](../Page/WON'T_BE_LONG.md "wikilink")》之改編版等5張單曲。

此張專輯銷售首週便空降公信榜冠軍，且佔據冠軍達4週，銷量突破100萬張，打平日本樂壇睽違6年3個月的紀錄。而此張專輯是倖田來未至目前銷售成績最好的一張原創專輯，由於收錄多張冠軍單曲，使這張專輯的銷售更加可觀。而先發單曲《Cherry
Girl / 命運》單曲於此專輯發行前不久推出，使得倖田來未的作品同時登上公信榜單曲與專輯榜單。

## 發行版本

### CD+2DVD （枚數生産限定盤）

※日本限定版本

  - **DISC1：CD**
      - 初回盤含Bonus Track 3曲全18曲
  - **DISC2：DVD**
      - Music Clip 9曲+初回盤Bonus Video（making影像）
  - **DISC3：DVD**
      - DVD MOVIE「Cherry Girl」。
  - 購入者特典：附贈「Cherry Girl」特製小冊子。
  - 封入特典：特設網站「Cherry Web」進入密碼封入。

### CD+DVD（初回盤・通常盤）

  - **DISC1：CD**
      - 全15曲收錄+初回盤Bonus Track 3曲
  - **DISC2：DVD**
      - Music Clip 9曲+初回盤Bonus Video（making影像）
  - 封入特典：特設網站「Cherry Web」進入密碼。

### CD ONLY（初回盤・通常盤）

  - 全15曲收錄+初回盤Bonus Track 3曲
  - 封入特典：特設網站「Cherry Web」進入密碼。

## 曲目

### CD

※僅收錄於初回盤及枚數限定盤。

1.  **INTRODUCTION**
      -
        為專輯序曲。全英語歌詞，本曲的完整版收錄於6th原創專輯『Kingdom（倖感王國）』的Bonus
        Track。本曲結束時同時接續第2首「Get Up & Move\!\!」。
2.  **Get Up & Move\!\!**
      -
        スズキシボレーMV-廣告曲
        之後製作成[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")，收錄於『BEST 〜BOUNCE &
        LOVERS〜（影音雙體驗～媚影&情歌極選～）』的DVD中。
3.  **(人魚公主)**
      -
        32nd單曲「4 hot wave（熱浪四射）」收錄曲。
        東芝手機Vodafone 705T 電視廣告曲（本人出演）。
4.  ** (悲夢之歌)**
      -
        33rd單曲
        朝日電視台連續劇「だめんず・うぉ～か～」主題歌。
5.  **(月亮和太陽)**
      -
        ジェムケリー電視廣告曲
6.  **Puppy**
      -
        [高絲](../Page/高絲.md "wikilink")・ヴィセ「ラッシュ エスカレーション」電視廣告曲
7.  ** (戀愛花蕾)**
      -
        31st單曲。
        連續劇「ブスの瞳に恋してる（愛上恐龍妹）」主題歌。
8.  **WON'T BE LONG〜Black Cherry Version〜**
      -
        [EXILE](../Page/EXILE.md "wikilink")&倖田來未名義的作品『WON'T BE
        LONG』的專輯版本。與單曲的編曲編排方式不同、歌詞稍作變更。
9.  **JUICY**
      -
        32nd單曲「4 hot wave（熱浪四射）」收錄曲。
        ジェムケリー廣告曲（本人出演）。
        オリコカード（倖田來未カード）廣告曲。
10. **Candle Light**
      -
        森永製菓「ウイダー プロテインバー」廣告曲 （首都圏限定on air）。
11. **Cherry Girl**
      -
        34th單曲。
        本人出演的東芝手機「SoftBank 811T」電視廣告曲。
        自行製作電影「Cherry Girl」主題歌。
12. **I'll be there**
      -
        32nd單曲「4 hot wave（熱浪四射）」收錄曲。
        [資生堂](../Page/資生堂.md "wikilink") 2006 SEABREEZE 電視廣告曲。
13. ** (命運)**
      -
        34th單曲。
        電影「大奧」主題歌。
14. **With your smile**
      -
        32nd單曲「4 hot wave（熱浪四射）」收錄曲。
        日本電視台「PRIDE\&SPIRIT 日本職業棒球2006」形象歌曲。
15. ** (奶茶)**
      -
        倖田的初次作詞・作曲的樂曲。
16. **Twinkle 〜English Version〜** ※
      -
        オムニバス動畫・Amazing Nuts\!「たとえ君が世界中の敵になっても」主題歌
        原曲版本為[日本語](../Page/日本語.md "wikilink")、收錄於同年12月6日發行之オムニバス單曲「Amazing
        Nuts\!」中。
17. **GO WAY\!\!** ※
      -
        電影「[蠟筆小新](../Page/蠟筆小新.md "wikilink") 風起雲湧！跳舞吧！朋友！」主題歌。
        本作釋出於8個月前的mu-mo的音樂配信限定樂曲，首度CD化。
18. **WON'T BE LONG|WON'T BE LONG〜Red Cherry Version〜** ※
      -
        [EXILE](../Page/EXILE.md "wikilink")&倖田來未名義的作品「WON'T BE
        LONG」之倖田的獨唱。
        編曲與單曲相同、歌詞稍作變更。

### DVD DISC1 MUSIC VIDEO

1.  (戀愛花蕾)

2.  JUICY MV

3.  With your smile

4.  I'll be there

5.  (人魚公主)

6.  (悲夢之歌)

7.  Cherry Girl

8.  (命運)

9.  Twinkle feat.SHOW

10. Premium making (初回收錄)

### DVD DISC2 (日本版)

1.  DVD MOVIE「Cherry Girl」
      -
        CD+2DVD版本的DISC3收錄的為倖田初主演的DVD MOVIE「Cherry
        Girl」。由豪華的演員陣「稻垣吾郎、MEGUMI、武田真治、伊藤裕子、JAI
        WEST、星野真里、濱田マリ、ルー大柴」等，聚集而成的連續劇。
        製作群為倖田所擔任的主題曲連續劇，[關西電視台](../Page/關西電視台.md "wikilink")・[富士電視台連續劇](../Page/富士電視台.md "wikilink")「ブスの瞳に恋してる（愛上恐龍妹）」的製作團隊。脚本為鈴木おさむ、監督由タカハタ秀太擔任。

## 外部連結

  - [Cherry
    Web](https://web.archive.org/web/20070616020206/http://ss.mu-mo.net/koda_cherry/index.php)
  - [ミュウモ
    倖田來未ディスコグラフィページ](https://archive.is/20061108171328/http://mu-mo.net/id/koda/discography/)

## 相關條目

[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:倖田來未音樂專輯](../Category/倖田來未音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:2007年Oricon專輯月榜冠軍作品](../Category/2007年Oricon專輯月榜冠軍作品.md "wikilink")
[Category:2007年Oricon專輯週榜冠軍作品](../Category/2007年Oricon專輯週榜冠軍作品.md "wikilink")