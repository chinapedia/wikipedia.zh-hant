**灵台县**在[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省东北部](../Page/甘肃省.md "wikilink")，是[平凉市下属的一个](../Page/平凉市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。

## 地理位置

位于甘肃省灵台县位于陇东黄土高原南缘，[东经](../Page/东经.md "wikilink")107°00'-107°57′，[北纬](../Page/北纬.md "wikilink")34°54′-35°14′。地势西北高、东南低，海拔在890～1520米之间。面积2038平方公里。与之接壤的县市有：东南边[陕西省的](../Page/陕西省.md "wikilink")[长武县](../Page/长武县.md "wikilink")、[彬县](../Page/彬县.md "wikilink")、[麟游县](../Page/麟游县.md "wikilink")、[千阳县和](../Page/千阳县.md "wikilink")[陇县](../Page/陇县.md "wikilink")，北边[泾川县](../Page/泾川县.md "wikilink")，西边[崇信县](../Page/崇信县.md "wikilink")。

## 行政区划

下辖9个[镇](../Page/镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 人口

灵台县有2个居委会184个行政村，1429个村民小组。全县总户数61825户，总人口23.21万人，其中，非农业人口18230人；民族以汉族居多，占96.7%，少数民族有回、藏、满、苗、蒙古族等占3.3%。

## 县情概况

灵台县历史悠久，人文荟萃。远古即有先民生息，商周之际建立古密须国、[密国](../Page/密国.md "wikilink")，史有文王伐密筑[灵台的记载](../Page/灵台.md "wikilink")。灵台因此得名，有上千年的历史。灵台县灵台人杰地灵，英才辈出，[晋代名医](../Page/晋代.md "wikilink")[皇甫谧](../Page/皇甫谧.md "wikilink")，曾开中国[针灸](../Page/针灸.md "wikilink")[医学的先河](../Page/医学.md "wikilink")，以医学宝典《[针灸甲乙经](../Page/针灸甲乙经.md "wikilink")》而蜚声古今中外，[唐代名臣](../Page/唐代.md "wikilink")[牛弘](../Page/牛弘.md "wikilink")，博学多闻、贯通古今，官居[吏部尚书](../Page/吏部尚书.md "wikilink")。名相[牛僧孺](../Page/牛僧孺.md "wikilink")，为官清正，著作等身，其《[玄怪录](../Page/玄怪录.md "wikilink")》在古代文学史上享有盛誉。

## 著名景点

  - [荆山](../Page/荆山.md "wikilink")[公园](../Page/公园.md "wikilink")
  - 古密须国[遗址](../Page/遗址.md "wikilink")

## 外部链接

  - [灵台县政府网站](https://web.archive.org/web/20060813034800/http://www.lingtai.gansu.gov.cn/new/index.asp)

## 参考资料

[灵台县](../Category/灵台县.md "wikilink") [县](../Category/平凉区县.md "wikilink")
[平凉](../Category/甘肃省县份.md "wikilink")