**Tasman**，是[微軟的](../Page/微軟.md "wikilink")[Internet Explorer for
Mac](../Page/Internet_Explorer_for_Mac.md "wikilink")[瀏覽器所使用的](../Page/網頁瀏覽器.md "wikilink")[排版引擎](../Page/排版引擎.md "wikilink")，也是為嘗試支援[W3C所制定的網頁標準而設計的](../Page/W3C.md "wikilink")。在Tasman推出時，一度是最切合[HTML及](../Page/HTML.md "wikilink")[CSS等標準的排版引擎](../Page/CSS.md "wikilink")\[1\]
。現時微軟方面也停止為Internet Explorer for Mac提供支援，但新版本的Tasman引擎仍被應用在一些微軟產品上\[2\]。

## 版本歷史

首個Tasman引擎被稱為「v0」版本，於2000年3月27日隨[Mac版的IE](../Page/麥金塔電腦.md "wikilink")5推出，而隨後的IE5.1則使用Tasman
0.1版引擎。

2003年5月15日，微軟推出Mac OS X版的MSN瀏覽器，它使用了Tasman
0.9排版引擎。在官方討論區，開發小組發言人吉米·格雷瓦爾（Jimmy
Grewal）在改進方面指出以下幾點：

  - 完整[Unicode支援](../Page/Unicode.md "wikilink")
  - 加強[CSS支援](../Page/CSS.md "wikilink")，包括CSS 3 Selectors、CSS TV
    Profile及@media
  - 加強[DOM支援](../Page/文档对象模型.md "wikilink")，包括DOM 1及DOM
    2的內核、樣式及事件，以及改進與Windows IE DOM的兼容性。
  - 支援[XHTML](../Page/XHTML.md "wikilink")1.0及1.1（Mac OS X的MSN並未提供此支援）
  - 加強與Mac OS
    X特色的支援，如[CoreGraphics](../Page/Quartz.md "wikilink")、ATSUI及CFSocket網絡。

在Mac版的Microsoft Office 2004中，當中的電子郵件客戶端[Microsoft
Entourage使用了Tasman排版引擎](../Page/Microsoft_Entourage.md "wikilink")\[3\]\[4\]。

## 參考文獻

## 參見

  - [排版引擎列表](../Page/排版引擎列表.md "wikilink")

[Category:排版引擎](../Category/排版引擎.md "wikilink") [Category:Internet
Explorer](../Category/Internet_Explorer.md "wikilink")

1.
2.
3.
4.