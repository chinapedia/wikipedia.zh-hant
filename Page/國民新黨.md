**國民新黨**（[日文](../Page/日文.md "wikilink")：，）是2005年至2013年存在的[日本政党](../Page/日本.md "wikilink")。由反对[小泉纯一郎首相推行的](../Page/小泉纯一郎.md "wikilink")[邮政民营化法案的脱离](../Page/日本邮政民营化.md "wikilink")[自民党的四名原](../Page/自由民主黨_\(日本\).md "wikilink")[众议院议员](../Page/日本众议院.md "wikilink")、现[参议院议员](../Page/日本参议院.md "wikilink")、和脱离民主党一名参议员组成。第一任代表（党首）为原众议院议长[绵贯民辅](../Page/绵贯民辅.md "wikilink")，干事长为原国土厅长官[龟井久兴](../Page/龟井久兴.md "wikilink")。中心成员是没有担任党内职务的原自民党政调会长、原建设大臣、自民党龟井派原会长的[龟井静香](../Page/龟井静香.md "wikilink")。[绵贯民辅在众议员大选中落选后](../Page/绵贯民辅.md "wikilink")，[龟井静香继任党首](../Page/龟井静香.md "wikilink")。现有国会议员6名。2009年9月起，与民主党联合执政。2010年7月在参议院选举中惨败，未获一席，其参议院总议席下降一半。2012年4月，因[消費稅增稅問題而是否退出執政聯盟的爭議而發生黨內分裂](../Page/消費稅.md "wikilink")，黨代表龜井靜香和國對委員長[龜井亞紀子主張脫離](../Page/龜井亞紀子.md "wikilink")，但其他議員皆不贊成；結果龜井退出國民新黨。黨代表由[自見庄三郎接任](../Page/自見庄三郎.md "wikilink")。[第46届日本众议院议员总选举中](../Page/第46届日本众议院议员总选举.md "wikilink")，国民新党的候选人全部失利，只剩下一名参议员；作为国民新党成立条件之一的改正邮政民营化法也已于2012年4月成立，国民新党于2013年3月21日正式宣布解散。\[1\]

## 参考文献

## 外部链接

  - [国民新党](https://web.archive.org/web/20061116094934/http://www.kokumin.or.jp/)

[Category:日本已解散政黨](../Category/日本已解散政黨.md "wikilink")
[Category:2005年建立的政黨](../Category/2005年建立的政黨.md "wikilink")
[Category:2005年日本建立](../Category/2005年日本建立.md "wikilink")
[Category:2013年日本廢除](../Category/2013年日本廢除.md "wikilink")
[Category:區域主義政黨](../Category/區域主義政黨.md "wikilink")
[Category:日本保守政黨](../Category/日本保守政黨.md "wikilink")

1.  [](http://www.jiji.com/jc/c?g=pol_30&k=2013032100595)