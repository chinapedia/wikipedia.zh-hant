**安娜·尤利耶芙娜·涅特列布科**（，）是來自[俄羅斯](../Page/俄羅斯.md "wikilink")，當紅於世界歌劇界的[女高音](../Page/女高音.md "wikilink")[歌手](../Page/歌手.md "wikilink")。她不僅具有華麗的嗓音，扎實的歌唱基本功，而她的美貌更是為人所稱道。她與更因[男高音](../Page/男高音.md "wikilink")[羅蘭度·維拉臣多次成功合作](../Page/羅蘭度·維拉臣.md "wikilink")，而和其被並稱為當今歌劇界的「夢幻情侶」\[1\]\[2\]。2006年她在[萨尔兹堡取得](../Page/萨尔兹堡.md "wikilink")[奥地利国籍](../Page/奥地利.md "wikilink")。

## 生平

涅特列布科出生於俄羅斯西南部的[克拉斯诺达尔市](../Page/克拉斯诺达尔.md "wikilink")，後入讀[聖彼得堡音樂學院](../Page/聖彼得堡音樂學院.md "wikilink")。求學時期曾在[聖彼得堡](../Page/聖彼得堡.md "wikilink")[馬林斯基劇院兼職洗地板的女工](../Page/馬林斯基劇院.md "wikilink")。不過後來她在劇院求職面試時，被劇院的藝術總監[瓦列里·格吉耶夫发现](../Page/瓦列里·格吉耶夫.md "wikilink")。格氏相當欣賞其嗓音，自此成為她的聲樂入門導師。而在1994年，涅特列布科得到首次[歌劇演出機會](../Page/歌劇.md "wikilink")，就是於在格吉耶夫指揮下的馬林斯基劇院出演《[费加罗的婚礼](../Page/费加罗的婚礼.md "wikilink")》中的苏姗娜。而涅特列布科還在馬林斯基劇院擔任過《[夢遊女](../Page/夢遊女.md "wikilink")》，《[魔笛](../Page/魔笛.md "wikilink")》，《[拉美莫爾的露琪亞](../Page/拉美莫爾的露琪亞.md "wikilink")》和《[塞维利亚的理发师](../Page/塞维利亚的理发师.md "wikilink")》當中的女主角。

涅特列布科在1995年首次在[美國登台](../Page/美國.md "wikilink")，在[三藩市歌劇院飾演](../Page/三藩市歌劇院.md "wikilink")《[魯斯蘭與柳德米拉](../Page/魯斯蘭與柳德米拉.md "wikilink")》中的柳德米拉一角。此後她開始在美國各地的巡迴演出，並因对角色准确的理解和艺术形象的塑造而成為俄語歌劇最有力的代言人。她在《[戰爭與和平](../Page/戰爭與和平.md "wikilink")》，《[沙皇的新娘](../Page/沙皇的新娘.md "wikilink")》和《[情定修道院](../Page/情定修道院.md "wikilink")》等俄語歌劇的演出，更被奉為經典。而涅特列布科的[美聲技巧也毫不遜色](../Page/美聲.md "wikilink")。她在《[弄臣](../Page/弄臣_\(歌剧\).md "wikilink")》，普契尼的《[波希米亞人](../Page/波希米亚人_\(歌剧\).md "wikilink")》和《[卡普烈特與蒙太奇](../Page/卡普烈特與蒙太奇.md "wikilink")》中出演的美聲角色也是為人津津樂道。

2002年，涅特列布科出演了[紐約](../Page/紐約.md "wikilink")[大都會歌劇院首次排演的](../Page/大都會歌劇院.md "wikilink")《戰爭與和平》，並作為自己在該院的首次演出。同年，她首次參與[萨尔茨堡音樂節](../Page/萨尔茨堡音樂節.md "wikilink")，並擔任了[尼古劳斯·哈农库特指揮的](../Page/尼古劳斯·哈农库特.md "wikilink")《[唐璜](../Page/唐·喬望尼.md "wikilink")》中的女主角。

2003年，涅特列布科發行了第一張唱片《歌劇詠嘆調集》。這張唱片成為了當年最暢銷的經典音樂唱片。2004年，承接前一年的成功，她與指揮家[克劳迪奥·阿巴多合作](../Page/克劳迪奥·阿巴多.md "wikilink")，發行了另一張詠嘆調什錦唱片《向來自由》（*Sempre
Libera*）。2005年，她與[墨西哥籍](../Page/墨西哥.md "wikilink")[男高音](../Page/男高音.md "wikilink")[羅蘭度·維拉臣共同出演了](../Page/羅蘭度·維拉臣.md "wikilink")《[羅密歐與茱麗葉](../Page/羅密歐與茱麗葉.md "wikilink")》和《[愛情靈藥](../Page/愛情靈藥.md "wikilink")》，獲得廣泛好評。同年稍後，她與維拉臣共同主演了萨尔茨堡音樂節重新編排的現代版《[茶花女](../Page/茶花女_\(歌劇\).md "wikilink")》，更引起轟動。兩人「夢幻情侶」之說，亦由此而興。

2007年底，涅特列布科宣布和[烏拉圭](../Page/烏拉圭.md "wikilink")[男中音埃爾文](../Page/男中音.md "wikilink")·施羅特（Erwin
Schrott）訂婚。2008年2月，涅特列布科宣布自己已經懷孕，而她也因此无法参加同年的[萨尔茨堡音樂節](../Page/萨尔茨堡音樂節.md "wikilink")，她所饰演的角色只能由[尼诺·马查伊泽代演](../Page/尼诺·马查伊泽.md "wikilink")，同年9月5日，涅特列布科在維也納誕下麟兒，堤雅各·阿魯拉·涅特列布科（Tiago
Aruã Netrebko）。\[3\]

涅特列布科被譽為當今俄罗斯声乐学派正宗传人。她的嗓音兼具浑厚清亮、华丽圆润，因此她既可作為抒情女高音，也可出演莊重女高音的角色，而她的花腔唱段的生命力和穿透力更是一絕。而音域方面，她能毫不费力得唱到降E6甚至E6的高音。

她还在[2014年冬季奥林匹克运动会开幕式上献唱](../Page/2014年冬季奥林匹克运动会开幕式.md "wikilink")[奥林匹克圣歌](../Page/奥林匹克圣歌.md "wikilink")。

## 備註

## 參考文獻

  - [維也納國立歌劇院官方網頁簡介](http://www.staatsoper.at/Content.Node2/home/ensemble/4611.php)

## 外部連結

  - [官方網站](http://www.annanetrebko.com)
  - [歌迷網](http://www.anna-netrebko.com)
  - [anna-netrebko.wbs.cz](http://www.anna-netrebko.wbs.cz) - Czech
  - [紐約時報：新類型的歌劇女神](http://www.nytimes.com/2007/12/02/magazine/02netrebko-t.html?_r=1&scp=2&sq=anna%20netrebko&st=cse&oref=slogin)

[Category:俄羅斯女歌手](../Category/俄羅斯女歌手.md "wikilink")
[Category:俄國女高音](../Category/俄國女高音.md "wikilink")

1.  《BBC音樂雜誌》2005年11月1日
2.  DG公司《茶花女》DVD（2005舒爾茨堡音樂節版）說明小冊子 第15頁
3.  [Anna Netrebko Announces Her
    Pregnancy](http://www.nytimes.com/aponline/arts/AP-People-Anna-Netrebko.html?_r=1&oref=slogin)