<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")<small>本條目記敘的是**莱什州**。其他同名的行政區尚有[列澤區及](../Page/列澤區.md "wikilink")[列澤市](../Page/列澤市.md "wikilink")。</small>

</div>

**列澤州**（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：Lezhë）位於[阿爾巴尼亞中北部](../Page/阿爾巴尼亞.md "wikilink")，由[庫爾賓區](../Page/庫爾賓區.md "wikilink")、[列澤區](../Page/列澤區.md "wikilink")、[米爾迪塔區所組成](../Page/米爾迪塔區.md "wikilink")，與[-{zh-hans:迪勃拉;
zh-hant:第巴爾;}-](../Page/第巴爾州.md "wikilink")、[-{zh-hans:都拉斯;
zh-hant:杜勒斯;}-](../Page/杜勒斯州.md "wikilink")、[庫克斯州](../Page/庫克斯州.md "wikilink")、[-{zh-hans:斯库台;
zh-hant:士科德;}-州相鄰](../Page/士科德州.md "wikilink")。