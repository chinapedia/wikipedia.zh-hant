**中国共产党中央委员会候补委员**由[中国共产党全国代表大会选出](../Page/中国共产党全国代表大会.md "wikilink")，党员必需具备五年以上党龄。中央委员会候补委员和中央委员会的任期同样是五年。當中央委员会委员出缺时，由中央委员会候补委员按照得票多少依次递补。

## 人员结构

中共中央候补委员一般是：

  - [中共中央直属机构](../Page/中共中央直属机构.md "wikilink")、[国务院组成部门的副部長級負責人](../Page/国务院组成部门.md "wikilink")；
  - 各[省](../Page/省.md "wikilink")（[自治區](../Page/自治區.md "wikilink")、[直轄市](../Page/直轄市.md "wikilink")）黨委副書記，省級黨委宣傳部長、組織部長、副省長（自治區副主席、副市長）；
  - [副省級城市的市委書記](../Page/副省級城市.md "wikilink")；
  - [中國人民解放軍正大军区级副职导人员](../Page/中國人民解放軍.md "wikilink")；
  - 各[人民團體負責人](../Page/人民團體.md "wikilink")；
  - 金融机构、国有重要骨干企业、高等院校、科研单位领导人员；
  - 各条战线专家学者；
  - 在基层工作的优秀代表。

## 历届中共中央候补委员

  - [中国共产党第五届中央委员会候补委员列表](../Page/中国共产党第五届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第六届中央委员会候补委员列表](../Page/中国共产党第六届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第七届中央委员会候补委员列表](../Page/中国共产党第七届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第八届中央委员会候补委员列表](../Page/中国共产党第八届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第九届中央委员会候补委员列表](../Page/中国共产党第九届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十届中央委员会候补委员列表](../Page/中国共产党第十届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十一届中央委员会候补委员列表](../Page/中国共产党第十一届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十二届中央委员会候补委员列表](../Page/中国共产党第十二届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十三届中央委员会候补委员列表](../Page/中国共产党第十三届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十四届中央委员会候补委员列表](../Page/中国共产党第十四届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十五届中央委员会候补委员列表](../Page/中国共产党第十五届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十六届中央委员会候补委员列表](../Page/中国共产党第十六届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十七届中央委员会候补委员列表](../Page/中国共产党第十七届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十八届中央委员会候补委员列表](../Page/中国共产党第十八届中央委员会候补委员列表.md "wikilink")
  - [中国共产党第十九届中央委员会候补委员列表](../Page/中国共产党第十九届中央委员会候补委员列表.md "wikilink")

## 被整肃的中央候补委员一览

  - 1929年，王仲一因背叛中共被撤销中央候补委员资格
  - 1950年，与[高岗发生冲突的](../Page/高岗.md "wikilink")[黎玉](../Page/黎玉.md "wikilink")、[刘子久撤销中央候补委员资格](../Page/刘子久.md "wikilink")
  - 1999年，[许运鸿因滥用职权](../Page/许运鸿.md "wikilink")，给国家造成巨大损失而被撤销中央候补委员资格
  - 2000年，[徐鹏航因纵容家属收受湖北](../Page/徐鹏航.md "wikilink")[黄石康赛集团公司的职工内部股而被撤销中央候补委员资格](../Page/黄石.md "wikilink")
  - 2001年，[石兆彬因卷入](../Page/石兆彬.md "wikilink")[远华案而被撤销中央候补委员资格](../Page/远华案.md "wikilink")，[李嘉廷因收受巨额贿赂而被撤销中央候补委员资格](../Page/李嘉廷.md "wikilink")
  - 2002年，[王雪冰因收受贿赂而被撤销中央候补委员资格](../Page/王雪冰.md "wikilink")
  - 2007年，[杜世成因纵容](../Page/杜世成.md "wikilink")、庇护以[聂磊为首的黑社会犯罪组织而被撤销中央候补委员资格](../Page/聂磊.md "wikilink")
  - 2014年，[李春城](../Page/李春城.md "wikilink")、[王永春](../Page/王永春_\(官僚\).md "wikilink")、[万庆良因严重违法违纪而被撤销中央候补委员资格](../Page/万庆良.md "wikilink")
  - 2015年，[朱明国](../Page/朱明国.md "wikilink")、[王敏](../Page/王敏_\(济南市委书记\).md "wikilink")、[陈川平](../Page/陈川平.md "wikilink")、[仇和](../Page/仇和.md "wikilink")、[杨卫泽](../Page/杨卫泽.md "wikilink")、[潘逸阳](../Page/潘逸阳.md "wikilink")、[余远辉因严重违法违纪而被撤销中央候补委员资格](../Page/余远辉.md "wikilink")
  - 2016年，[吕锡文](../Page/吕锡文.md "wikilink")、[范长秘](../Page/范长秘.md "wikilink")、[牛志忠因严重违法违纪而被撤销中央候补委员资格](../Page/牛志忠.md "wikilink")
  - 2017年，[李云峰](../Page/李云峰.md "wikilink")、[杨崇勇](../Page/杨崇勇.md "wikilink")、[莫建成因严重违法违纪而被撤销中央候补委员资格](../Page/莫建成_\(中共\).md "wikilink")

## 参考文献

## 参见

  - [中国共产党中央委员会委员](../Page/中国共产党中央委员会委员.md "wikilink")

{{-}}

[中国共产党中央委员会候补委员](../Category/中国共产党中央委员会候补委员.md "wikilink")