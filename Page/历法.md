[Hindu_calendar_1871-72.jpg](https://zh.wikipedia.org/wiki/File:Hindu_calendar_1871-72.jpg "fig:Hindu_calendar_1871-72.jpg")\]\]

**历法**是用[年](../Page/年.md "wikilink")、[月](../Page/月.md "wikilink")、[日等時間單位计算时间的方法](../Page/日.md "wikilink")。

主要分为[阳历](../Page/阳历.md "wikilink")、[阴历和](../Page/阴历.md "wikilink")[阴阳历三种](../Page/阴阳历.md "wikilink")。阳历亦即太阳历，其曆年为一个[回归年](../Page/回归年.md "wikilink")，现时国际通用的[公历](../Page/公历.md "wikilink")（[西历](../Page/公历.md "wikilink")）即为太阳历的一种，亦简称为阳历；阴历亦称[月亮曆](../Page/月亮曆.md "wikilink")，或称太阴历，其曆月是一个[朔望月](../Page/朔望月.md "wikilink")，曆年为12个朔望月，其大月30天，小月29天，[伊斯兰历即为阴历的一种](../Page/伊斯兰历.md "wikilink")；阴阳历的平均曆年为一个回归年，曆月为朔望月，因为12个朔望月与回归年相差太大，所以阴阳历中设置闰月，所以这种历法与月相相符，也与地球绕太阳周期运动相符合。中国的[农历就是阴阳历的一种](../Page/农历.md "wikilink")。

历法中包含的其他時間元素（單位）尚有：

  - [节气](../Page/节气.md "wikilink")
  - [世纪](../Page/世纪.md "wikilink")
  - [年代](../Page/年代.md "wikilink")

## 历法系統

一個历法系統會為每一天設計一個历法上的日期，因此[星期本身不算是完整的历法系統](../Page/星期.md "wikilink")。若一個系統為一年內的每一天命名，但沒有標別年份的方式，也不是完整的历法系統。

最簡單的历法系統是以某一參考日或時間為準，計算經過了多少個時間單位，像[儒略日和](../Page/儒略日.md "wikilink")[UNIX时间就是這種系統](../Page/UNIX时间.md "wikilink")，在時間單位不變的情形下，唯一可能的變化是更改參考日或時間，使計算的時間單位少一點，計算历法系統只需要加法及減法。

其他历法系統有一個或是多個較大的時間單位。

有一個較大的時間單位的历法系統。

  - 週次和每週的第幾天：此系統不常見，其特點是沒有年，每隔一週，週次就會加一。
  - [年和此年中的第幾天](../Page/年.md "wikilink")：例如[ISO
    8601的順序日期表示法](../Page/ISO_8601#順序日期表示法.md "wikilink")。

有二個較大的時間單位的历法系統。

  - [年](../Page/年.md "wikilink")、[月和](../Page/月.md "wikilink")[日](../Page/日.md "wikilink")：大部份的历法系統都屬於這一種，包括[公历](../Page/公历.md "wikilink")（及在公历以前，相當類似的[儒略曆](../Page/儒略曆.md "wikilink")、[伊斯兰历](../Page/伊斯兰历.md "wikilink")、[農曆](../Page/農曆.md "wikilink")、及[希伯來曆](../Page/希伯來曆.md "wikilink")。
  - 年、週次和每週的第幾天：例如[ISO week date](../Page/ISO_week_date.md "wikilink")。

較大的週期也可以和自然現象同步：

  - [太陰曆是和](../Page/太陰曆.md "wikilink")[月亮的運動](../Page/月亮.md "wikilink")（[月相](../Page/月相.md "wikilink")）同步，例如[伊斯兰历](../Page/伊斯兰历.md "wikilink")。
  - [太陽曆是依和太陽運動有關的](../Page/太陽曆.md "wikilink")[季節變化同步](../Page/季節.md "wikilink")，例如[伊朗曆](../Page/伊朗曆.md "wikilink")。
  - [陰陽曆是合併了月亮和太陽的變化](../Page/陰陽曆.md "wikilink")，例如[農曆](../Page/農曆.md "wikilink")、[印度曆或](../Page/印度曆.md "wikilink")[希伯來曆](../Page/希伯來曆.md "wikilink")。
  - 也有一些历法系統似乎是和[金星的運動同步](../Page/金星.md "wikilink")，例如一些[古埃及历法](../Page/古埃及历法.md "wikilink")，和金星出現在[赤道上的時間同步](../Page/赤道.md "wikilink")。

[星期是少數沒有和自然現象同步的時間週期](../Page/星期.md "wikilink")。

常常历法系統會包括一個以上的週期，或是同時有週期內及不在週期內的日期，像[法國共和曆的一個月有](../Page/法國共和曆.md "wikilink")30天，一年有五天或六天不屬於任何一個月。

大部份的历法系統會整合更多複雜的時間週期，例如大部份的历法系統都會有年、月、星期、日，但定義可能不同。許多历法系統都有一星期的七天，已使用超過幾千年\[1\]。

### 太陽曆

#### 太陽曆中的日

太陽曆會為每一個[太陽日定義一個日期](../Page/太陽日.md "wikilink")，一日會以二次連續事件（如[日落](../Page/日落.md "wikilink")）之間的時間為準，一年當中，二次連續事件的間隔時間可能略有變化，或者會平均為[平均太陽日](../Page/平均太陽日.md "wikilink")，其他的历法也使用太陽日為時間單位。

#### 曆法改革

有許多有關曆法改革的提議，像是[世界曆](../Page/世界曆.md "wikilink")、[國際固定曆](../Page/國際固定曆.md "wikilink")、[全新世紀年及](../Page/全新世紀年.md "wikilink")[漢克亨利萬年曆](../Page/漢克亨利萬年曆.md "wikilink")（
Hanke-Henry Permanent
Calendar）。類似的想法在不同時期都有出現，但因為沒有連續性、實施時的大規模調整，或是宗教反對等原因，最後都沒有實現。

### 太陰曆

不是所有的历法系統都用太陽年為單位。[太陰曆就是以](../Page/太陰曆.md "wikilink")[月相變化來計算日期的曆法](../Page/月相.md "wikilink")。因為[回归年的長度不是月相週期的整數倍](../Page/回归年.md "wikilink")。單純的太陰曆很快就會無法和季節對齊．不過和其他現象會對齊的很好，例如[潮汐](../Page/潮汐.md "wikilink")，像[伊斯蘭曆就是太陰曆](../Page/伊斯蘭曆.md "wikilink")。

Alexander Marshack在一個很有爭議性的書籍中\[2\]認為一個骨棒上的痕跡(c. 25,000
BC)代表太陰曆，而Michael
Rappenglueck也認為一幅15,000年前的洞穴畫中就有太陰曆\[3\]。

### 陰陽曆

[陰陽曆是陰曆](../Page/陰陽曆.md "wikilink")，但為了讓月份和季節可以對應，會以依一定規則加一個月的方式來調整，像[希伯來曆就有](../Page/希伯來曆.md "wikilink")[19年的週期](../Page/章_\(曆法\).md "wikilink")，而[農曆的](../Page/農曆.md "wikilink")[閏月也有類似的規則](../Page/閏月.md "wikilink")。

## 历法的時間單位

幾乎所有的历法系統都會將數日整合為[月或是](../Page/月.md "wikilink")[年](../Page/年.md "wikilink")。在[太陽曆中](../Page/太陽曆.md "wikilink")，一年接近地球的[回歸年](../Page/回歸年.md "wikilink")（也就是一個完整季節循環需要的時間），一般會用在[農業活動的規劃上](../Page/農業.md "wikilink")。太陰曆則是以月相變化為主，一些历法系統也會有其他的時間週期，例如[星期](../Page/星期.md "wikilink")。

因為[回歸年的長度不是一日的整數倍](../Page/回歸年.md "wikilink")，因此太陽曆有些年的天數會和其他的年的天數不一様，例如在[閏年要加一天](../Page/閏年.md "wikilink")（[閏日](../Page/閏日.md "wikilink")）。若像陰曆的月或是陰陽曆中一年的月份數，也會有類似的情形，這稱為[置閏](../Page/置閏.md "wikilink")。像大多數太陽曆的一年也無法分為長度相同，不會變動的十二個月。

一些文化會定義其他的時間單位，例如[星期](../Page/星期.md "wikilink")，而中國以往使用的一[干支是](../Page/干支.md "wikilink")60，因此有[干支紀年及](../Page/干支#干支紀年.md "wikilink")[干支紀日](../Page/干支#干支紀日.md "wikilink")。有些文化會用不同的年代起算日期，例日本的年份就是以[天皇即位為準](../Page/天皇.md "wikilink")，並且有對應的[年號](../Page/年號.md "wikilink")，例如[明仁天皇的年號是](../Page/明仁.md "wikilink")[平成](../Page/平成.md "wikilink")，2006年就是平成18年。

有些曆法會定義特定的日期，例如[農曆中就會針對季節的變化](../Page/農曆.md "wikilink")，將一太陽年中選出二十四個日期，定為二十四個[節氣](../Page/節氣.md "wikilink")。

## 其他历法分類

### 計算曆法及天文曆法

天文曆法（astronomical
calendar）是以天文觀測為準的曆法，例如使用定氣定朔的現代[農曆](../Page/農曆.md "wikilink")、宗教性的伊斯蘭曆及[第二聖殿時的古猶太曆](../Page/第二聖殿.md "wikilink")。這種曆法也稱為是以觀測為準的的曆法，好處是完美而且永遠準確，缺點是沒有一定的公式，若要回推多久以前某一天的日期比較困難。

計算曆法（arithmetic
calendar）是以嚴格的數學公式計算的曆法，例如現在的猶太曆，也稱為是以規則為準的曆法，好處是容易計算特定時間是哪一天，不過和自然變化的精準性就比較差，即使曆法本身非常的精準，也會因為地球自轉及公轉的略為變化，造成其精準性慢慢變差，因此一個計算曆法使用的期間有限，可能只有數千年，之後就要用新的曆法系統代替。

### 完整曆法及不完整曆法

曆法也分為完整及不完整。完整曆法會為每一天設定一個日期，而不完整曆法就不會。像古[羅馬曆沒有為冬天的日子設計日期](../Page/羅馬曆.md "wikilink")，直接跳過，統稱為冬日，這就是不完整曆，大部份的曆法就是完整曆法。

## 用途

历法的主要用途是識別日期，記錄已經發生過的事，告知或同意末來的某一事件。日期可能有農業上、生活上、宗教上或社會上的重要性。例如历法可以用來決定何時要播種或是收割，哪幾天是[法定假日或是宗教假日](../Page/法定假日.md "wikilink")，日期可以標示會計年度的開始及結束，有些日期有法律上的重要性．例如需繳稅的日子或是合約的期限。一天的日期也可以提供一些相關的資訊，例如其季節。

曆法也是完整計時系統的一部份，有日期及時間即可精確的定義某一特定的時刻，現代的計時器可以顯示日期、時間及星期幾。

## 中国古代的历法

| 朝代                                   | 曆名                                   | 編者                                      | 使用年份                                     |
| ------------------------------------ | ------------------------------------ | --------------------------------------- | ---------------------------------------- |
| [西漢](../Page/西漢.md "wikilink")       | [太初曆](../Page/太初曆.md "wikilink")/三統曆 | [鄧平](../Page/鄧平.md "wikilink")          | [前104年](../Page/前104年.md "wikilink")-84年 |
| [東漢](../Page/東漢.md "wikilink")       | [四分曆](../Page/四分曆.md "wikilink")     | [編訢](../Page/編訢.md "wikilink")          | 85年-205年                                 |
| [乾象曆](../Page/乾象曆.md "wikilink")     | [劉洪](../Page/劉洪.md "wikilink")       | 206年-236年                               |                                          |
| [曹魏](../Page/曹魏.md "wikilink")       | [景初曆](../Page/景初曆.md "wikilink")     | [楊偉](../Page/楊偉.md "wikilink")          | 237年-442年                                |
| [劉宋](../Page/劉宋.md "wikilink")       | [元嘉曆](../Page/元嘉曆.md "wikilink")     | [何承天](../Page/何承天_\(南朝\).md "wikilink") | 443年-462年                                |
| [大明曆](../Page/大明曆.md "wikilink")     | [祖沖之](../Page/祖沖之.md "wikilink")     | 463年-520年                               |                                          |
| [北魏](../Page/北魏.md "wikilink")       | [正光曆](../Page/正光曆.md "wikilink")     | [李業興](../Page/李業興.md "wikilink")        | 521年-539年                                |
| [興和曆](../Page/興和曆.md "wikilink")     | [李業興](../Page/李業興.md "wikilink")     | 540年-549年                               |                                          |
| [北齊](../Page/北齊.md "wikilink")       | [天保曆](../Page/天保曆.md "wikilink")     | [宋景業](../Page/宋景業.md "wikilink")        | 550年-565年                                |
| [後周](../Page/後周.md "wikilink")       | [天和曆](../Page/天和曆.md "wikilink")     | [甄鸞](../Page/甄鸞.md "wikilink")          | 556年-578年                                |
| [大象曆](../Page/大象曆.md "wikilink")     | [馮顯](../Page/馮顯.md "wikilink")       | 579年-583年                               |                                          |
| [隋](../Page/隋朝.md "wikilink")        | [開皇曆](../Page/開皇曆.md "wikilink")     | [張賓](../Page/張賓.md "wikilink")          | 584年-607年                                |
| [皇極曆](../Page/皇極曆.md "wikilink")     | [劉焯](../Page/劉焯.md "wikilink")       | 605年-617年                               |                                          |
| [大業曆](../Page/大業曆.md "wikilink")     | [張冑玄](../Page/張冑玄.md "wikilink")     | 608年-618年                               |                                          |
| [唐](../Page/唐.md "wikilink")         | [戊寅曆](../Page/戊寅曆.md "wikilink")     | [傅仁鈞](../Page/傅仁鈞.md "wikilink")        | 619年-665年                                |
| [麟德曆](../Page/麟德曆.md "wikilink")     | [李淳風](../Page/李淳風.md "wikilink")     | 666年-728年                               |                                          |
| [大衍曆](../Page/大衍曆.md "wikilink")     | [一行](../Page/一行.md "wikilink")       | 728年-761年                               |                                          |
| [五紀曆](../Page/五紀曆.md "wikilink")     | [郭獻之](../Page/郭獻之.md "wikilink")     | 762年-784年                               |                                          |
| [貞元曆](../Page/貞元曆.md "wikilink")     | [徐承嗣](../Page/徐承嗣.md "wikilink")     | 785年-821年                               |                                          |
| [宣明曆](../Page/宣明曆.md "wikilink")     | [徐昂](../Page/徐昂.md "wikilink")       | 822年-892年                               |                                          |
| [崇玄曆](../Page/崇玄曆.md "wikilink")     | [邊岡](../Page/邊岡.md "wikilink")       | 893年-955年                               |                                          |
| [五代](../Page/五代.md "wikilink")       | [欽天曆](../Page/欽天曆.md "wikilink")     | [王樸](../Page/王樸_\(後周\).md "wikilink")   | 956年-959年                                |
| [北宋](../Page/北宋.md "wikilink")       | [應天曆](../Page/應天曆.md "wikilink")     | [王處訥](../Page/王處訥.md "wikilink")        | 960年-980年                                |
| [乾元曆](../Page/乾元曆.md "wikilink")     | [吳昭素](../Page/吳昭素.md "wikilink")     | 981年-1000年                              |                                          |
| [儀天曆](../Page/儀天曆.md "wikilink")     | [史序](../Page/史序.md "wikilink")       | 1001年-1023年                             |                                          |
| [崇天曆](../Page/崇天曆.md "wikilink")     | [宋行古](../Page/宋行古.md "wikilink")     | 1024年-1063年                             |                                          |
| [明天曆](../Page/明天曆.md "wikilink")     | [周琮](../Page/周琮.md "wikilink")       | 1064年-1073年                             |                                          |
| [奉元曆](../Page/奉元曆.md "wikilink")     | [衛朴](../Page/衛朴.md "wikilink")       | 1074年-1091年                             |                                          |
| [觀天曆](../Page/觀天曆.md "wikilink")     | [皇后卿](../Page/皇后卿.md "wikilink")     | 1092年-1102年                             |                                          |
| [占天曆](../Page/占天曆.md "wikilink")     | [姚舜輔](../Page/姚舜輔.md "wikilink")     | 1103年-1105年                             |                                          |
| [紀元曆](../Page/紀元曆.md "wikilink")     | [姚舜輔](../Page/姚舜輔.md "wikilink")     | 1106年-1126年                             |                                          |
| [金](../Page/金國.md "wikilink")        | [大明曆](../Page/大明曆.md "wikilink")     | [楊級](../Page/楊級.md "wikilink")          | 1127年-1179年                              |
| [重修大明曆](../Page/重修大明曆.md "wikilink") | [趙知徵](../Page/趙知徵.md "wikilink")     | 1180年-1280年                             |                                          |
| [南宋](../Page/南宋.md "wikilink")       | [統元曆](../Page/統元曆.md "wikilink")     | [陳德一](../Page/陳德一.md "wikilink")        | 1135年-1160年                              |
| [乾道曆](../Page/乾道曆.md "wikilink")     | [劉孝榮](../Page/劉孝榮.md "wikilink")     | 1167年-1177年                             |                                          |
| [淳熙曆](../Page/淳熙曆.md "wikilink")     | [劉孝榮](../Page/劉孝榮.md "wikilink")     | 1177年-1190年                             |                                          |
| [會元曆](../Page/會元曆.md "wikilink")     | [劉孝榮](../Page/劉孝榮.md "wikilink")     | 1191年-1198年                             |                                          |
| [統天曆](../Page/統天曆.md "wikilink")     | [楊忠輔](../Page/楊忠輔.md "wikilink")     | 1199年-1206年                             |                                          |
| [開禧曆](../Page/開禧曆.md "wikilink")     | [包翰元](../Page/包翰元.md "wikilink")     | 1207年-1250年                             |                                          |
| [淳祐曆](../Page/淳祐曆.md "wikilink")     | [李德卿](../Page/李德卿.md "wikilink")     | 1251年-1252年                             |                                          |
| [會天曆](../Page/會天曆.md "wikilink")     | [譚玉](../Page/譚玉.md "wikilink")       | 1253年-1270年                             |                                          |
| [成天曆](../Page/成天曆.md "wikilink")     | [陳鼎](../Page/陳鼎.md "wikilink")       | 1271年-1274年                             |                                          |
| [乙未曆](../Page/乙未曆.md "wikilink")     | [耶律履](../Page/耶律履.md "wikilink")     | 1180年-                                  |                                          |

  - [授时曆](../Page/授时曆.md "wikilink")
  - [时宪曆](../Page/时宪曆.md "wikilink")

## 一些特殊 的曆法

### 會計年度

會計年度是指政府或企業為預算、會計或納稅而設的年度。一個會計年度共有12個月，開始及結束時間則依各國而不定，例如美國的會計年度是從10月1日開始，到9月30日結束。印度和香港的會計年度是從4月1日開始，到3月31日結束，不過有些小公司的會計年度會從[屠妖節開始](../Page/屠妖節.md "wikilink")，到隔年的屠妖節前一天結束。

在會計上常會用，每一個月會有固定的週數，以便各月之間和各年之間的比較。例如一月固定有四週，二月固定有四週，三月固定有五週等，每五至六年會自動加上第53週，[ISO
8601是有關日期的](../Page/ISO_8601.md "wikilink")[國際標準化組織辦法](../Page/國際標準化組織.md "wikilink")，一週固定從週一開始，在週日結束。第一週是包括[公曆中一月四日的那一週](../Page/公曆.md "wikilink")。

## 参考文献

### 引用

### 来源

  -
  - with [Online
    Calculator](https://web.archive.org/web/20050216102054/http://emr.cs.iit.edu/home/reingold/calendar-book/Calendrica.html)

  -
  -
  -
  -
  -
  -
  -
## 延伸閱讀

  -
  -
## 外部链接

  -
## 参见

  - [历法列表](../Page/历法列表.md "wikilink")
  - [历法改革](../Page/历法改革.md "wikilink")
  - [公历](../Page/公历.md "wikilink")（格里高利历）
  - [農曆](../Page/農曆.md "wikilink")（夏曆）
  - [保加爾暦法](../Page/保加爾暦法.md "wikilink")
  - [實時時鐘](../Page/實時時鐘.md "wikilink")

{{-}}

[曆法](../Category/曆法.md "wikilink")
[Category:自然史](../Category/自然史.md "wikilink")

1.  Zerubavel, *The Seven Day Circle* (University of Chicago Press,
    1985).
2.  James Elkins, *[Our beautiful, dry, and distant
    texts](http://books.google.com/books?id=5Ku6YdWurMgC&pg=PA63&lpg=PA63)*
    (1998) 63ff.
3.