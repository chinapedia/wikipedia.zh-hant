**人工智能**（，缩写为 ）亦稱**機器智能**，指由人製造出來的機器所表現出來的智能。通常人工智能是指-{zh-hans:通过;
zh-hant:透過;}-普通電腦程式來呈現人類智能的技術。該詞也指出研究這樣的智能系統是否能夠實現，以及如何實現。同时，有些預測則認為[人類的無數](../Page/人類.md "wikilink")[職業也逐漸被其取代](../Page/職業.md "wikilink")。\[1\]

人工智能在一般教材中的定义领域是“智慧主体（intelligent
agent）的研究与设计”\[2\]，智慧主体指一个可以观察周遭环境并作出行动以达致目标的系统\[3\]。[约翰·麦卡锡于](../Page/约翰·麦卡锡.md "wikilink")1955年的定义是\[4\]「制造智能机器的科学与工程」\[5\]。安德里亚斯•卡普兰（Andreas
Kaplan）和迈克尔•海恩莱因（Michael
Haenlein）将人工智能定义为“系统正确解释外部数据，从这些数据中学习，并利用这些知识-{zh-hans:通过;
zh-hant:透過;}-灵活适应实现特定目标和任务的能力”。\[6\]

人工智能的研究是高度技术性和专业的，各分支领域都是深入且各不相通的，因而涉及範圍極廣\[7\]。人工智能的研究可以分为几个技术问题。其分支领域主要集中在解决具体问题，其中之一是，如何使用各种不同的工具完成特定的应用程序。

AI的核心问题包括建構能夠跟人類似甚至超越的推理、知识、规划、学习、交流、感知、移动和操作物体的能力等\[8\]。[人工智能目前仍然是该领域的长远目标](../Page/人工智能.md "wikilink")\[9\]。目前強人工智慧已經有初步成果，甚至在一些影像辨識、語言分析、棋類遊戲等等單方面的能力達到了超越人類的水平，而且人工智慧的通用性代表著，能解決上述的問題的是一樣的AI程式，無須重新開發算法就可以直接使用現有的AI完成任務，與人類的處理能力相同，但達到具備思考能力的統合強人工智慧還需要時間研究，比较流行的方法包括统计方法，计算智能和传统意义的AI。目前有大量的工具应用了人工智能，其中包括搜索和数学优化、逻辑推演。而基於[仿生學](../Page/仿生學.md "wikilink")、[認知心理學](../Page/認知心理學.md "wikilink")，以及基于概率论和经济学的演算法等等也在逐步探索當中。
思维来源于大脑，而思维控制行为，行为需要意志去实现，而思维又是对所有数据采集的整理，相当于数据库，所以人工智能最后会演变为机器替换人类。

## 概論

人工智能的定義可以分為兩部分，即「人工」和「-{zh:智能; zh-hans:智能; zh-hant:智能;
zh-tw:智慧}-」。「人工」比較好理解，爭議性也不大。有時我們會要考慮什麼是人力所能及製造的，或者人自身的智能程度有沒有高到可以創造人工智能的地步，等等。但總括來說，「人工系統」就是通常意義下的人工系統。

關於什麼是「-{zh:智能; zh-hans:智能; zh-hant:智能;
zh-tw:智慧}-」，就問題多多了。這涉及到其它諸如[意識](../Page/意識.md "wikilink")（consciousness）、[自我](../Page/自我.md "wikilink")（self）、[心靈](../Page/心靈.md "wikilink")（mind），包括[無意識的精神](../Page/潛意識.md "wikilink")（unconscious
mind）等等問題。人唯一瞭解的智能是人本身的智能，這是普遍認同的觀點。但是我們對我們自身智能的理解都非常有限，對構成人的智能必要元素的瞭解也很有限，所以就很難定義什麼是「人工」製造的「-{zh:智能;
zh-hans:智能; zh-hant:智能;
zh-tw:智慧}-」了。因此人工智能的研究往往涉及對人智能本身的研究。其它關於[動物或其它人造系統的智能也普遍被認為是人工智能相關的研究課題](../Page/動物.md "wikilink")。

人工智慧目前在[計算機領域內](../Page/計算機.md "wikilink")，得到了愈加廣泛的发挥。並在[機器人](../Page/機器人.md "wikilink")、經濟政治決策、[控制系統](../Page/控制系統.md "wikilink")、[仿真系統中得到應用](../Page/仿真系統.md "wikilink")。

## 發展史

<table>
<thead>
<tr class="header">
<th><p>年代</p></th>
<th><p>20世纪40年代</p></th>
<th><p>20世纪50年代</p></th>
<th><p>20世纪60年代</p></th>
<th><p>20世纪70年代</p></th>
<th><p>20世纪80年代</p></th>
<th><p>20世纪90年代</p></th>
<th><p>21世紀00年代</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>計算機</p></td>
<td><p>1945 <a href="../Page/計算機.md" title="wikilink">計算機</a>（<a href="../Page/電子數值積分計算機.md" title="wikilink">ENIAC</a>）</p></td>
<td><p>1957 <a href="../Page/FORTRAN語言.md" title="wikilink">FORTRAN語言</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>人工智慧研究</p></td>
<td></td>
<td><p>1953 <a href="../Page/博弈論.md" title="wikilink">博弈論</a><br />
1956 <a href="../Page/达特矛斯会议.md" title="wikilink">达特矛斯会议</a></p></td>
<td></td>
<td><p>1977 宣言</p></td>
<td><p>1982 <a href="../Page/第五代電腦.md" title="wikilink">第五代電腦計劃開始</a></p></td>
<td><p>1991 <a href="../Page/人工神经网络.md" title="wikilink">人工神经网络</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>人工智慧語言</p></td>
<td></td>
<td></td>
<td><p>1960 <a href="../Page/LISP語言.md" title="wikilink">LISP語言</a></p></td>
<td><p>1973 <a href="../Page/PROLOG語言.md" title="wikilink">PROLOG語言</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/知識表達.md" title="wikilink">知識表達</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>1973 <a href="../Page/生產系統.md" title="wikilink">生產系統</a><br />
1976 <a href="../Page/框架理論.md" title="wikilink">框架理論</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/專家系統.md" title="wikilink">專家系統</a></p></td>
<td></td>
<td></td>
<td><p>1965 </p></td>
<td><p>1975 </p></td>
<td><p>1980 <a href="../Page/Xcon.md" title="wikilink">Xcon</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 研究課題

目前人工智慧的研究方向已經被分成幾個子領域，研究人員希望一個人工智慧系統應該具有某些特定能力，以下將這些能力列出並說明。\[10\]

### 演绎、推理和解决问题

早期的人工智慧研究人员直接模仿人类进行逐步的推理，就像是玩棋盘游戏或进行逻辑推理时人类的思考模式。\[11\]到了1980和1990年代，利用[概率和经济学上的概念](../Page/概率.md "wikilink")，人工智慧研究还发展了非常成功的方法处理[不确定或不完整的资讯](../Page/不确定性.md "wikilink")。\[12\]

对于困难的问题，有可能需要大量的运算资源，也就是发生了「可能组合爆增」：当问题超过一定的规模时，电脑会需要天文数量级的存储器或是运算时间。寻找更有效的演算法是优先的人工智慧研究项目。\[13\]

人类解决问题的模式通常是用最快捷、直观的判断，而不是有意识的、一步一步的推导，早期人工智慧研究通常使用逐步推导的方式。\[14\]人工智慧研究已经于这种「次表征性的」解决问题方法取得进展：实体化Agent研究强调[感知运动的重要性](../Page/认知发展论.md "wikilink")。神经网络研究试图以模拟人类和动物的大脑结构重现这种技能。

### 知識表示法

[GFO_taxonomy_tree.png](https://zh.wikipedia.org/wiki/File:GFO_taxonomy_tree.png "fig:GFO_taxonomy_tree.png")

知识表示是人工智能领域的核心研究问题之一，它的目标是让机器存储相应的知识，并且能够按照某种规则推理演绎得到新的知识。有许多需要解决的问题需要大量的对世界的知识,这些知识包括事先存储的先验知识和-{zh-hans:通过;
zh-hant:透過;}-智能推理得到的知识。事先存储的先验知识指：人类-{zh-hans:通过;
zh-hant:透過;}-某种方式告诉给机器的知识。-{zh-hans:通过;
zh-hant:透過;}-智能推理得到的知识指：结合先验知识和某种特定的推理规则（逻辑推理）得到的知识。首先，先验知识可以指描述目标，特征，种类及物件之间的关系的知识，
也可以描述事件，时间，状态，原因和结果，
以及任何知识你想要机器存储的。比如：今天没有太阳，没有太阳就是阴天。那么以命题逻辑语言，这些知识可以被表示为：今天--\>没有太阳，
没有太阳--\>阴天。这些知识是先验知识，那么-{zh-hans:通过;
zh-hant:透過;}-推理可以得到新知识：今天--\>阴天。由此例子可以看出，先验知识的正确性非常重要，这个例子中没有太阳就是阴天，这个命题是不严谨的、比较笼统的，因为没有太阳可能是下雨，也可能下雪。逻辑命题表示在知识表示中非常重要，逻辑推理规则是目前主要推理规则。可以在机器中用逻辑符号定义每一个逻辑命题，然后再让机器存储相应的逻辑推理规则，那么自然而然机器便可进行推理。目前知识表达有许多困境尚无法解决，比如：建立一个完备的知识库几乎不可能，所以知识库的资源受到限制；先验知识的正确性需要进行检验，而且先验知识有时候不一定是只有对或者错两种选择。

### 规划

智能Agent必须能够制定目标和实现这些目标。\[15\]他们需要一种方法来建立一个可预测的世界模型（将整个世界状态用数学模型表现出来，并能预测它们的行为将如何改变这个世界），这样就可以选择功效最大的行为。\[16\]
在传统的规划问题中，智能Agent被假定它是世界中唯一具有影响力的，所以它要做出什么行为是已经确定的。\[17\]但是，如果事实并非如此，它必须定期检查世界模型的状态是否和自己的预测相符合。如果不符合，它必须改变它的计划。因此[智能代理必须具有在不确定结果的状态下推理的能力](../Page/智能代理.md "wikilink")。\[18\]
在多Agent中，多个Agent规划以合作和竞争的方式去完成一定的目标，使用演化演算法和群体智慧可以达成一个整体的[突现行为目标](../Page/突现.md "wikilink")。\[19\]

### 學習

机器学习的主要目的是为了让机器从使用者和输入数据等处获得知识，从而让机器自动地去判断和输出相应的结果。这一方法可以帮助解决更多问题、减少错误，提高解决问题的效率。对于人工智能来说，机器学习从一开始就很重要。1956年，在最初的达特茅斯夏季会议上，雷蒙德·索洛莫诺夫写了一篇关于不监视的概率性机器学习：一个归纳推理的机器。

机器学习的方法各种各样，主要分为监督学习和非监督学习两大类。监督学习指事先给定机器一些训练样本并且告诉样本的类别，然后根据这些样本的类别进行训练，提取出这些样本的共同属性或者训练一个分类器，等新来一个样本，则通过训练得到的共同属性或者分类器进行判断该样本的类别。监督学习根据输出结果的离散性和连续性，分为分类和回归两类。非监督学习是不给定训练样本，直接给定一些样本和一些规则，让机器自动根据一些规则进行分类。无论哪种学习方法都会进行误差分析，从而知道所提的方法在理论上是否误差有上限。

### 自然語言處理

自然語言處理探討如何處理及運用[自然語言](../Page/自然語言.md "wikilink")，自然語言認知則是指讓電腦「懂」人類的[語言](../Page/語言.md "wikilink")。自然語言生成系統把計算機數據轉化為自然語言。自然語言理解系統把自然語言轉化為計算機程序更易于處理的形式。

### 運動和控制

### 知覺

[機器感知](../Page/機器感知.md "wikilink")\[20\]是指能夠使用感測器所輸入的資料（如照相機、麥克風、聲納以及其他的特殊感測器）然後推斷世界的狀態。[計算機視覺](../Page/計算機視覺.md "wikilink")\[21\]能夠分析影像輸入。另外還有[語音識別](../Page/語音識別.md "wikilink")\[22\]、[人臉辨識和](../Page/人臉辨識.md "wikilink")[物體辨識](../Page/物體辨識.md "wikilink")。\[23\]

### 社交

[，一个具有表情等社交能力的机器人\[24\]](https://zh.wikipedia.org/wiki/File:Kismet_robot_at_MIT_Museum.jpg "fig:，一个具有表情等社交能力的机器人")
情感和社交技能對於一個智能agent是很重要的。首先，-{zh-hans:通过;
zh-hant:透過;}-了解他們的動機和情感狀態，代理人能夠預測別人的行動（這涉及要素
博弈論、決策理論以及能夠塑造人的情感和情緒感知能力檢測）。此外，為了良好的[人機互動](../Page/人機互動.md "wikilink")，智慧代理人也需要表現出情緒來。至少它必須出現禮貌地和人類打交道。至少，它本身應該有正常的情緒。

### 創造力

一個人工智慧的子領域，代表了理論（從哲學和心理學的角度）和實際（通過特定的實現產生的系統的輸出是可以考慮的創意，或系統識別和評估創造力）所定義的創造力。相關領域研究的包括了[人工直覺和](../Page/人工直覺.md "wikilink")[人工想像](../Page/人工想像.md "wikilink")。

### 倫理管理

[史蒂芬·霍金](../Page/史蒂芬·霍金.md "wikilink")、[比爾蓋茲](../Page/比爾蓋茲.md "wikilink")、[馬斯克](../Page/馬斯克.md "wikilink")、
Jaan Tallinn 以及 Nick Bostrom
等人都對於人工智慧技術的未來公開表示憂心\[25\]，人工智慧若在許多方面超越人類智慧水平的智能、不斷更新、自我提升，進而取得控制管理權，人類是否有足夠的能力及時停止人工智慧領域的「軍備競賽」，能否保有最高掌控權，現有事實是：機器常失控導致人員傷亡，這樣的情況是否會更加擴大規模出現，歷史顯然無法給出可靠的樂觀答案。特斯拉電動車馬斯克（Elon
Musk）在麻省理工學院（MIT）航空航天部門百年紀念研討會上稱人工智能是「召喚惡魔」行為，英國發明家Clive
Sinclair認為一旦開始製造抵抗人類和超越人類的智能機器，人類可能很難生存，蓋茲同意馬斯克和其它人所言，且不知道為何有些人不擔憂這個問題。\[26\]

[DeepMind的人工智慧](../Page/DeepMind.md "wikilink")（AI）系統在2016年「[AlphaGo](../Page/AlphaGo.md "wikilink")」對戰南韓棋王[李世乭獲勝](../Page/李世乭.md "wikilink")，開發商表示在內部設立倫理委員會，針對人工智慧的應用制定政策，防範人工智慧淪為犯罪開發者。\[27\]

科技進步，人工智慧科技產生「自主武器」軍備競賽已悄悄展開，英國、以色列與挪威，都已部署自主飛彈與無人操控的無人機，具「射後不理」（fire-and-forget）能力的飛彈，多枚飛彈還可互相溝通，分享找到攻擊目標。這些武器還未被大量投入，但很快就會出現在戰場上，且並非使用人類所設計的程序，而是完全利用機器自行決策。
[霍金等人在英國獨立報發表文章警告未來人工智慧可能會比人類金融市場](../Page/霍金.md "wikilink")、科學家、人類領袖更能操縱人心、甚至研發出人們無法理解的武器。專家恐發展到無法控制的局面，援引聯合國禁止研發某些特定武器的「特定常規武器公約」加以限制。\[28\]新南威爾斯大學（New
South Wales）人工智慧的沃爾什（Toby Walsh）教授認為這是一種欺騙，因為機器無區別戰敵和平民的技術。\[29\]

### 經濟衝擊

據CNN財經網數字媒體未來學家兼Webbmedia集團創始人艾米·韋伯（Amy
Webb）；美國在線\[30\]...等紛紛預測一些即將被機器人取代的職業，日本野村總合研究所也與英國牛津大學的研究學者共同調查指出，10至20年後，日本有49%的職業(235種職業)可能會被機械和人工智慧取代而消失，直接影響約達2500萬人，\[31\]例如：超市店員、一般事務員、計程車司機、收費站運營商和收銀員、市場營銷人員、客服人員、製造業工人、金融中間人和分析師、新聞記者、電話公司職員、麻醉師、士兵和保安、律師、醫生、軟體開發者和操盤手、股票交易員等等高薪酬的腦力職業將最先受到衝擊\[32\]。

2017年6月份[马云在美國](../Page/马云.md "wikilink")[底特律舉行](../Page/底特律.md "wikilink")「鏈結世界」（Gateway
17）產業大會，會上提出人工智慧可能導致[第三次世界大戰](../Page/第三次世界大戰.md "wikilink")，因為前兩次產業革命都導致兩次大戰，戰爭原因並非這些創新發明本身，而是發明對社會上許多人的生活方式衝擊處理不當，新科技在社會上產生新工作也取代舊工作，產生了新的輸家和贏家，若是輸家的人數太多將造成一股社會不穩的能量而這股能量被有心人利用可能導致各種事件。他認為各國應該強制訂定規定AI機器只能用於人類不能做的工作，避免短時間大量人類被取代的[失業大潮](../Page/失業.md "wikilink")，但馬雲沒有提出這種世界性規定將如何實現並確保遵守的細節方案。\[33\]

[数据科学和](../Page/数据科学.md "wikilink")[人工智能被](../Page/人工智能.md "wikilink")[哈佛商業評論称为](../Page/哈佛商業評論.md "wikilink")《二十一世纪最Sexy的职业》\[34\]，人工智能需求量大，鼓励了不少大学诸如[伯克利大学专门成立](../Page/加州大學柏克萊分校.md "wikilink")[数据科学系](../Page/数据科学.md "wikilink")。硅谷和纽约为主的《》公司於2012年成立，焦点是[数据科学](../Page/数据科学.md "wikilink")，[大数据](../Page/大数据.md "wikilink")，和[人工智能企业培训](../Page/人工智能.md "wikilink")，提供国际大数据培训服务。

## 強人工智能和弱人工智能

人工智能的一個比較流行的定義，也是該領域較早的定義，是由當時[麻省理工學院的](../Page/麻省理工學院.md "wikilink")[約翰·麥卡錫在](../Page/約翰·麥卡錫.md "wikilink")1956年的[達特矛斯會議上提出的](../Page/达特矛斯会议.md "wikilink")：人工智能就是要讓機器的行為看起來就像是人所表現出的智能行為一樣。但是這個定義似乎忽略了[強人工智能的可能性](../Page/強人工智慧.md "wikilink")（見下）。另一個定義指人工智能是人造機器所表現出來的智能。總體來講，目前對人工智能的定義大多可劃分為四類，即機器「像人一樣思考」、「像人一樣行動」、「理性地思考」和「理性地行動」。這裡「行動」應廣義地理解為採取行動，或制定行動的決策，而不是肢體動作。

### 強人工智能

強人工智能觀點認為「有可能」製造出「真正」能[推理](../Page/推理.md "wikilink")（Reasoning）和[解決問題的智能機器](../Page/解決問題.md "wikilink")，並且，這樣的機器將被認為是具有知覺、有自我意識的。強人工智能可以有兩類：

  - 人類的人工智能，即機器的思考和推理就像人的[思維一樣](../Page/思維.md "wikilink")。
  - 非人類的人工智能，即機器產生了和人完全不一樣的知覺和意識，使用和人完全不一樣的推理方式。

### 弱人工智能

弱人工智能觀點認為「不可能」製造出能「真正」地[推理和](../Page/推理.md "wikilink")[解決問題的智能機器](../Page/解決問題.md "wikilink")，這些機器只不過「看起來」像是智能的，但是並不真正擁有智能，也不會有自主意識。

弱人工智能是對比強人工智能才出現的，因為人工智能的研究一度處於停滯不前的狀態下，直到類神經網路有了強大的運算能力加以模擬後，才開始改變並大幅超前。但人工智能研究者不一定同意弱人工智能，也不一定在乎或者了解強人工智能和弱人工智能的內容與差別，對定義爭論不休。

就當下的人工智能研究領域來看，研究者已大量造出「看起來」像是智能的機器，取得相當豐碩的理論上和實質上的成果，如2009年[康乃爾大學教授Hod](../Page/康乃爾大學.md "wikilink")
Lipson 和其博士研究生Michael Schmidt 研發出的
Eureqa電腦程式，只要給予一些資料，這電腦程式自己只用幾十個小時計算就推論出[牛頓花費多年研究才發現的牛頓力學公式](../Page/牛頓.md "wikilink")，等於只用幾十個小時就自己重新發現牛頓力學公式，這電腦程式也能用來研究很多其他領域的科學問題上。這些所謂的弱人工智慧在神經網路發展下已經有巨大進步，但對於要如何整合成強人工智慧，現在還沒有明確定論。

### 對強人工智能的哲學爭論

「強人工智能」一詞最初是[約翰·羅傑斯·希爾勒針對](../Page/約翰·羅傑斯·希爾勒.md "wikilink")[計算機和其它](../Page/計算機.md "wikilink")[信息處理機器創造的](../Page/信息處理.md "wikilink")，其定義為：

「強人工智能觀點認為計算機不僅是用來研究人的思維的一種工具；相反，只要-{zh-hans:运行; zh-hant:運行;
zh-tw:執行}-適當的程序，計算機本身就是有思維的。」（J Searle in Minds Brains and
Programs. The Behavioral and Brain Sciences, vol. 3, 1980）

關於強人工智能的爭論，不同於更廣義的[一元論和](../Page/一元論.md "wikilink")[二元論的爭論](../Page/二元論.md "wikilink")。其爭論要點是：如果一台機器的唯一工作原理就是轉換編碼數據，那麼這台機器是不是有思維的？希爾勒認為這是不可能的。他舉了個[中文房間的例子來說明](../Page/中文房間.md "wikilink")，如果機器僅僅是轉換數據，而數據本身是對某些事情的一種編碼表現，那麼在不理解這一編碼和這實際事情之間的對應關係的前提下，機器不可能對其處理的數據有任何理解。基於這一論點，希爾勒認為即使有機器通過了[圖靈測試](../Page/圖靈試驗.md "wikilink")，也不一定說明機器就真的像人一樣有自我思維和自由意識。

也有哲學家持不同的觀點。[丹尼爾·丹尼特在其著作](../Page/丹尼爾·丹尼特.md "wikilink")《》（Consciousness
Explained）裡認為，人也不過是一台有靈魂的機器而已，為什麼我們認為：「人可以有智能，而普通機器就不能」呢？他認為像上述的數據轉換機器是有可能有思維和意識的。

有的哲學家認為如果弱人工智能是可實現的，那麼強人工智能也是可實現的。比如（Simon
Blackburn）在其哲學入門教材Think裡說道，一個人的看起來是「智能」的行動並不能真正說明這個人就真的是智能的。我永遠不可能知道另一個人是否真的像我一樣是智能的，還是說她／他僅僅是「看起來」是智能的。基於這個論點，既然弱人工智能認為可以令機器「看起來」像是智能的，那就不能完全否定這機器是真的有智能的。布莱克本認為這是一個主觀認定的問題。

需要指出的是，弱人工智能並非和強人工智能完全對立，也就是說，即使強人工智能是可能的，弱人工智能仍然是有意義的。至少，今日的計算機能做的事，像算術運算等，在一百多年前是被認為很需要智能的。並且，即使強人工智能被證明為可能的，也不代表強人工智能必定能被研製出來。

## 研究方法

目前没有统一的原理或[范式指导人工智能研究](../Page/范式.md "wikilink")。许多问题上研究者都存在争论。\[35\]

其中几个长久以来仍没有结论的问题是：是否应从[心理或](../Page/心理.md "wikilink")[神经方面模拟人工智能](../Page/神经.md "wikilink")?或者像鸟类生物学对于[航空工程一样](../Page/航空工程.md "wikilink")，人类生物学对于人工智能研究是没有关系的？\[36\]智能行为能否用简单的原则（如[逻辑或](../Page/逻辑.md "wikilink")[优化](../Page/优化.md "wikilink")）来描述？还是必须解决大量完全无关的问题？\[37\]

智能是否可以使用高级符号表达，如词和想法？还是需要“子符号”的处理？\[38\] 約翰·豪格兰德（John
Haugeland）提出了GOFAI（出色的老式人工智能）的概念，也提议人工智能应归类为，这个概念后来被某些非GOFAI研究者采纳。\[39\]\[40\]

### 控制论与大脑模拟

20世纪40年代到50年代，许多研究者探索[神经学](../Page/神经学.md "wikilink")、[信息理论及](../Page/信息理论.md "wikilink")[控制论之间的联系](../Page/控制论.md "wikilink")。其中还造出一些使用电子网络构造的初步智能，如[格雷·華特](../Page/格雷·華特.md "wikilink")（W.
Grey Walter）的烏龜（turtle）和[約翰霍普金斯野獸](../Page/約翰霍普金斯野獸.md "wikilink")（Johns
Hopkins Beast）。

这些研究者还经常在[普林斯顿大学和英国的Ratio](../Page/普林斯顿大学.md "wikilink")
Club举行技术协会会议。\[41\]直到1960，大部分人已经放弃这个方法，尽管在80年代再次提出这些原理。

### 符号处理

当20世纪50年代，數位计算机研制成功，研究者开始探索人类智能是否能简化成符号处理。研究主要集中在[卡内基梅隆大学](../Page/卡内基梅隆大学.md "wikilink")，[斯坦福大学和](../Page/斯坦福大学.md "wikilink")[麻省理工學院](../Page/麻省理工學院.md "wikilink")，而各自有独立的研究风格。約翰·豪格兰德（John
Haugeland）称这些方法为GOFAI（出色的老式人工智能）\[42\]。60年代，符号方法在小型证明程序上模拟高级思考有很大的成就。基于[控制论或](../Page/控制论.md "wikilink")[神经网络的方法则置于次要](../Page/神经网络.md "wikilink")\[43\]。60－70年代的研究者确信符号方法最终可以成功创造[强人工智能的机器](../Page/强人工智能.md "wikilink")，同时这也是他们的目标。

  - 认知模拟：经济学家[赫伯特·西蒙和](../Page/赫伯特·西蒙.md "wikilink")[艾伦·纽厄尔研究人类问题解决能力和尝试将其形式化](../Page/艾伦·纽厄尔.md "wikilink")，同时他们为人工智能的基本原理打下基础，如[认知科学](../Page/认知科学.md "wikilink")、[运筹学和](../Page/运筹学.md "wikilink")[经营科学](../Page/经营科学.md "wikilink")。他们的研究团队使用[心理学实验的结果开发模拟人类解决问题方法的程序](../Page/心理学.md "wikilink")。这方法一直在[卡内基梅隆大学沿袭下来](../Page/卡内基梅隆大学.md "wikilink")，并在80年代于Soar发展到高峰\[44\]\[45\]。

<!-- end list -->

  - 基于逻辑：不像[艾伦·纽厄尔和](../Page/艾伦·纽厄尔.md "wikilink")[赫伯特·西蒙](../Page/赫伯特·西蒙.md "wikilink")，[约翰·麦卡锡认为机器不需要模拟人类的思想](../Page/约翰·麦卡锡.md "wikilink")，而应尝试找到抽象推理和解决问题的本质，不管人们是否使用同样的算法\[46\]。他在[斯坦福大学的实验室致力于使用形式化逻辑解决多种问题](../Page/斯坦福大学.md "wikilink")，包括[知识表示](../Page/知识表示.md "wikilink")，[智能规划和](../Page/智能规划.md "wikilink")[机器学习](../Page/机器学习.md "wikilink")\[47\]。致力于逻辑方法的还有[爱丁堡大学](../Page/爱丁堡大学.md "wikilink")，而促成欧洲的其他地方开发编程语言[Prolog和](../Page/Prolog.md "wikilink")[逻辑编程科学](../Page/逻辑编程.md "wikilink")\[48\]。

<!-- end list -->

  - “反逻辑”: [斯坦福大学的研究者](../Page/斯坦福大学.md "wikilink")
    （如[马文·闵斯基和](../Page/马文·闵斯基.md "wikilink")[西摩爾·派普特](../Page/西摩爾·派普特.md "wikilink")）\[49\]发现要解决[计算机视觉和](../Page/计算机视觉.md "wikilink")[自然语言处理的困难问题](../Page/自然语言处理.md "wikilink")，需要专门的方案：他们主张不存在简单和通用原理（如逻辑）能够达到所有的智能行为。羅杰·單克（Roger
    Schank）描述他们的“反逻辑”方法为“scruffy”\[50\]。[常识知识库](../Page/常识知识库.md "wikilink")（如[道格拉斯·莱纳特的](../Page/道格拉斯·莱纳特.md "wikilink")[Cyc](../Page/Cyc.md "wikilink")）就是“scruffy”AI的例子，因为他们必须人工一次编写一个复杂的概念\[51\]。

<!-- end list -->

  - 基于知识：大约在1970年出现大容量内存计算机，研究者分别以三个方法开始把[知识构造成应用软件](../Page/知识表示.md "wikilink")\[52\]。这场“知识革命”促成[专家系统的开发与计划](../Page/专家系统.md "wikilink")，这是第一个成功的人工智能软件形式\[53\]。“知识革命”同时让人们意识到许多简单的人工智能软件可能需要大量的知识。

### 子符号方法

1980年代符号人工智能停滞不前，很多人认为符号系统永远不可能模仿人类所有的认知过程，特别是感知、机器人、机器学习和模式识别。很多研究者开始关注子符号方法解决特定的人工智能问题\[54\]。

  - 自下而上、接口agent、嵌入环境（机器人）、[行为主义](../Page/行为主义.md "wikilink")、新式AI：机器人领域相关的研究者，如[罗德尼·布鲁克斯](../Page/罗德尼·布鲁克斯.md "wikilink")（Rodney
    Brooks），否定符号人工智能而专注于机器人移动和求生等基本的工程问题。\[55\]他们的工作再次关注早期控制论研究者的观点，同时提出了在人工智能中使用控制理论。这与认知科学领域中的表征感知论点是一致的:更高的智能需要个体的表征（如移动，感知和形象）。

<!-- end list -->

  - 计算智能：1980年代中大衛·鲁姆哈特（David E.
    Rumelhart）等再次提出[神经网络和](../Page/神经网络.md "wikilink")[联结主义](../Page/联结主义.md "wikilink")\[56\]。这和其他的子符号方法，如模糊控制和进化计算，都属于计算智能学科研究范畴\[57\]。

### 统计学方法

1990年代，人工智能研究发展出复杂的数学工具来解决特定的分支问题。这些工具是真正的科学方法，即这些方法的结果是可测量的和可验证的，同时也是近期人工智能成功的原因。共用的数学语言也允许已有学科的合作（如数学，经济或运筹学）。Stuart
J. Russell和Peter
Norvig指出这些进步不亚于“革命”和“neats的成功”\[58\]。有人批评这些技术太专注于特定的问题，而没有考虑长远的强人工智能目标\[59\]。

### 集成方法

  - 智能agent范式：智能agent是一个会感知环境并作出行动以达致目标的系统。最简单的智能agent是那些可以解决特定问题的程序。更复杂的agent包括人类和人类组织（如[公司](../Page/公司.md "wikilink")）。这些范式可以让研究者研究单独的问题和找出有用且可验证的方案，而不需考虑单一的方法。一个解决特定问题的agent可以使用任何可行的方法-一些agent用符号方法和逻辑方法，一些则是子符号[神经网络或其他新的方法](../Page/神经网络.md "wikilink")。范式同时也给研究者提供一个与其他领域沟通的共同语言--如[决策论和经济学](../Page/决策论.md "wikilink")（也使用abstract
    agents的概念）。1990年代智能agent范式被广泛接受。\[60\]

<!-- end list -->

  - [agent体系结构和](../Page/agent体系结构.md "wikilink")[认知体系结构](../Page/认知体系结构.md "wikilink")：研究者设计出一些系统来处理多agent系统中智能agent之间的相互作用。\[61\]一个系统中包含符号和子符号部分的系统称为[混合智能系统](../Page/混合智能系统.md "wikilink")，而对这种系统的研究则是[人工智能系统集成](../Page/人工智能系统集成.md "wikilink")。[分级控制系统则给反应级别的子符号AI和最高级别的传统符号AI提供桥梁](../Page/分级控制系统.md "wikilink")，同时放宽了规划和世界建模的时间。

## 实际应用

[机器视觉](../Page/机器视觉.md "wikilink")、[指纹识别](../Page/指纹识别.md "wikilink")、[人脸识别](../Page/人脸识别.md "wikilink")、[视网膜识别](../Page/视网膜识别.md "wikilink")、[虹膜识别](../Page/虹膜识别.md "wikilink")、[掌纹识别](../Page/掌纹识别.md "wikilink")、[专家系统](../Page/专家系统.md "wikilink")、[自动规划等](../Page/自动规划.md "wikilink")。

## 學科範疇

人工智能是一門邊緣學科，屬於[自然科學和](../Page/自然科學.md "wikilink")[社會科學的交叉](../Page/社會科學.md "wikilink")。

### 涉及學科

  - [數學](../Page/數學.md "wikilink")，[统计学](../Page/统计学.md "wikilink")，[計算機科學](../Page/計算機科學.md "wikilink")
  - [物理学](../Page/物理学.md "wikilink")
  - [哲學和](../Page/哲學.md "wikilink")[認知科學](../Page/認知科學.md "wikilink")
  - [逻辑学](../Page/逻辑学.md "wikilink")
  - [心理學](../Page/心理學.md "wikilink")
  - [控制論](../Page/控制論.md "wikilink")
  - [決定論](../Page/決定論.md "wikilink")
  - [不確定性原理](../Page/不確定性原理.md "wikilink")
  - [社會學](../Page/社會學.md "wikilink")
  - [犯罪學](../Page/犯罪學.md "wikilink")
  - [智慧犯罪](../Page/智慧犯罪.md "wikilink")

### 研究範疇

  - [自然語言處理](../Page/自然語言處理.md "wikilink")（NLP; Natural Language
    Processing）
  - [知識表現](../Page/知識表現.md "wikilink")（Knowledge Representation）
  - [智能搜索](../Page/智能搜索.md "wikilink")（Intelligent Search）
  - [推理](../Page/推理.md "wikilink")
  - [規劃](../Page/規劃.md "wikilink")（Planning）
  - [機器學習](../Page/機器學習.md "wikilink")（Machine Learning）
  - [增強式學習](../Page/增強式學習.md "wikilink")（Reinforcement Learning）
  - [知識獲取](../Page/知識獲取.md "wikilink")
  - [感知問題](../Page/感知問題.md "wikilink")
  - [模式識別](../Page/模式識別.md "wikilink")
  - [邏輯程序設計](../Page/邏輯程序設計.md "wikilink")
  - [軟計算](../Page/軟計算.md "wikilink")（Soft Computing）
  - [不精確和不確定的管理](../Page/不精確和不確定的管理.md "wikilink")
  - [人工生命](../Page/人工生命.md "wikilink")（Artificial Life）
  - [人工神經網路](../Page/人工神經網路.md "wikilink")（Artificial Neural Network）
  - [複雜系統](../Page/複雜系統.md "wikilink")
  - [遺傳算法](../Page/遺傳算法.md "wikilink")
  - [資料挖掘](../Page/資料挖掘.md "wikilink")（Data Mining）
  - [模糊控制](../Page/模糊控制.md "wikilink")

## 應用領域

  - [智能控制](../Page/智能控制.md "wikilink")
  - [機器人學](../Page/機器人學.md "wikilink")
  - [自動化技術](../Page/自動化技術.md "wikilink")
  - [語言和圖像理解](../Page/語言和圖像理解.md "wikilink")
  - [遺傳編程](../Page/遺傳編程.md "wikilink")
  - [法學資訊系統](../Page/法學資訊系統.md "wikilink")
  - [下棋](../Page/下棋.md "wikilink")
  - [醫學領域](../Page/醫學領域.md "wikilink")

## 相關

  - [人類滅絕](../Page/人類滅絕.md "wikilink")

## 參看

  - [人类自愿灭绝运动](../Page/人类自愿灭绝运动.md "wikilink")
  - [無條件基本收入](../Page/無條件基本收入.md "wikilink")

## 參見

  - [自動駕駛](../Page/自動駕駛.md "wikilink")
  - [人工生命](../Page/人工生命.md "wikilink")
  - [人工智慧哲學](../Page/人工智能哲學.md "wikilink")
  - [認知神經科學](../Page/認知神經科學.md "wikilink")
  - [电脑围棋](../Page/电脑围棋.md "wikilink")
  - [艾倫·圖靈](../Page/艾倫·圖靈.md "wikilink")
  - [恐怖谷理論](../Page/恐怖谷理論.md "wikilink")
  - [電子世界爭霸戰](../Page/電子世界爭霸戰.md "wikilink")
  - [計算機科學](../Page/計算機科學.md "wikilink")
  - [計算機科學課程列表](../Page/計算機科學課程列表.md "wikilink")
  - [認知科學](../Page/認知科學.md "wikilink")
  - [意識](../Page/意識.md "wikilink")
  - [希爾勒的](../Page/約翰·希爾勒.md "wikilink")[中文房間](../Page/中文房間.md "wikilink")
  - [語義學](../Page/語義學.md "wikilink")
  - [技术奇异点](../Page/技术奇异点.md "wikilink")
  - [集体智慧](../Page/集体智慧.md "wikilink")
  - [控制論](../Page/控制論.md "wikilink")
  - [心理學](../Page/心理學.md "wikilink")
  - [生物化学计算机](../Page/生物化学计算机.md "wikilink")（例：[人脑](../Page/人脑.md "wikilink")）
  - [東Robo君](../Page/東Robo君.md "wikilink")（東ロボくん）
  - [國際人工智能聯合會議](../Page/國際人工智能聯合會議.md "wikilink")
  - [网络本体语言](../Page/网络本体语言.md "wikilink")（OWL）
  - [遊戲樹](../Page/遊戲樹.md "wikilink")
  - [量子计算机](../Page/量子计算机.md "wikilink")
  - [自動駕駛汽車](../Page/自動駕駛汽車.md "wikilink")

## 参考文献

### 引用

### 来源

  - 中文書

<!-- end list -->

  - [李開復](../Page/李開復.md "wikilink")、[王詠剛](../Page/王詠剛.md "wikilink")；《人工智慧來了》

<!-- end list -->

  - 教材

<!-- end list -->

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
<!-- end list -->

  - 人工智能历史

<!-- end list -->

  -
  -
  -
<!-- end list -->

  - 其他

<!-- end list -->

  -
  - [BibTex](http://dblp.uni-trier.de/rec/bibtex/conf/iwann/Aleksander95)
    [Internet
    Archive](https://web.archive.org/web/19970302014628/http://www.ee.ic.ac.uk/research/neural/publications/iwann.html)

  - .

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - .

  -
  -
  -
  -
  -
  - .

  -
  -
  -
  -
  - .

  -
  -
  -
  -
  -
  -
  -
  -
  -
  - .

  - , Presidential Address to the [Association for the Advancement of
    Artificial Intelligence](../Page/美国人工智能协会.md "wikilink").

  -
  -
  -
  -
  -
  - [Andreas Kaplan; Michael Haenlein (2018) Siri, Siri in my Hand,
    who's the Fairest in the Land? On the Interpretations, Illustrations
    and Implications of Artificial Intelligence, Business
    Horizons, 62(1)](https://www.sciencedirect.com/science/article/pii/S0007681318301393)

  -
  -
  -
  -
  -
  -
  -
  -
  -
## 扩-{展}-阅读

  - TechCast Article Series, John Sagi, [Framing
    Consciousness](https://web.archive.org/web/20120724125829/http://www.techcast.org/Upload/PDFs/634146249446122137_Consciousness-Sagifinalversion.pdf)

  - [Boden, Margaret](../Page/Boden,_Margaret.md "wikilink"), Mind As
    Machine, [Oxford University Press](../Page/牛津大學出版社.md "wikilink"),
    2006

  - Johnston, John (2008) "The Allure of Machinic Life: Cybernetics,
    Artificial Life, and the New AI", MIT Press

  - Myers, Courtney Boyd ed. (2009). [The AI
    Report](http://www.forbes.com/2009/06/22/singularity-robots-computers-opinions-contributors-artificial-intelligence-09_land.html).
    Forbes June 2009

  -
  - Sun, R. & Bookman, L. (eds.), *Computational Architectures:
    Integrating Neural and Symbolic Processes*. Kluwer Academic
    Publishers, Needham, MA. 1994.

## 外部連結

  - [What Is
    AI?](https://web.archive.org/web/20151118212402/http://www-formal.stanford.edu/jmc/whatisai/whatisai.html)—An
    introduction to artificial intelligence by AI founder [John
    McCarthy](../Page/约翰·麦卡锡.md "wikilink").

  -
  - [AITopics](https://web.archive.org/web/20120630042400/http://aaai.org/AITopics/)—A
    large directory of links and other resources maintained by the
    [Association for the Advancement of Artificial
    Intelligence](../Page/美国人工智能协会.md "wikilink"), the leading
    organization of academic AI researchers.

  - [Artificial Intelligence Discussion
    group](https://www.researchgate.net/group/Artificial_Intelligence)

  - 机器人智能机器人智能

  - [研學論壇](http://bbs.matwav.com/)關於人工智能，模式識別，科學交流的學術論壇

  - [中國人工智能網－人工智能|模式識別|數字圖像處理](http://www.chinaai.org)

  - [AI Depot](http://ai-depot.com/)—community discussion, news, and
    articles

  - [Loebner Prize
    website](http://www.loebner.net/Prizef/loebner-prize.html)

  - [Game AI](http://aigamedev.com/)—計算機遊戲開發者的AI資源

  - [Kurzweil CyberArt
    Technologies](http://www.kurzweilcyberart.com/)—關於人工智能藝術的網站，裡面有著名的人工智能繪畫程序AARON

  - [關於人工智能，專家系統prolog語言全介紹的wiki網站](https://web.archive.org/web/20100904002104/http://cdtzx.swiki.net/)

  - [中華民國人工智慧學會](http://www.taai.org.tw/)—以促進中華民國人工智慧及相關領域之研究、發展、應用與交流為宗旨的民間組織。

  - [MostAI](https://archive.is/20070427014339/http://www.mostai.com/)—關於人工智能的網站，AI
    Fans交流平台

  - [智能程序自进化概念](http://blog.csdn.net/liron71/article/details/8242670)

  - [進化論](http://numerentur.org/evolucion-de-la-inteligencia-artificial/)

{{-}}

[人工智能](../Category/人工智能.md "wikilink")
[Category:控制论](../Category/控制论.md "wikilink")
[Category:形式科学](../Category/形式科学.md "wikilink")
[Category:技術與社會](../Category/技術與社會.md "wikilink")
[Category:计算神经科学](../Category/计算神经科学.md "wikilink")
[Category:新兴技术](../Category/新兴技术.md "wikilink")
[Category:计算机科学中未解决的问题](../Category/计算机科学中未解决的问题.md "wikilink")

1.  [人工智能「搶飯碗」 2020年會計師將被AI取代](https://topick.hket.com/article/1916960/人工智能「搶飯碗」%E3%80%802020年會計師將被AI取代)

2.
3.
4.
5.
6.  [Andreas Kaplan; Michael Haenlein (2019) Siri, Siri in my Hand,
    who's the Fairest in the Land? On the Interpretations, Illustrations
    and Implications of Artificial Intelligence, Business
    Horizons, 62(1), 15-25](https://www.sciencedirect.com/science/article/pii/S0007681318301393)

7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.

25.

26.

27.

28.

29.

30.

31.

32.

33. [中時-馬雲談AI的風險](http://www.chinatimes.com/realtimenews/20170622002310-260412)

34.

35. 写道："Simply put, there is wide disagreement in the field about what
    AI is all about" .

36.
37.
38.
39. <http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.38.8384&rep=rep1&type=pdf>

40.

41.
42.
43. The most dramatic case of sub-symbolic AI being pushed into the
    background was the devastating critique of
    [perceptrons](../Page/感知器.md "wikilink") by [Marvin
    Minsky](../Page/马文·闵斯基.md "wikilink") and [Seymour
    Papert](../Page/西摩爾·派普特.md "wikilink") in 1969. See [History of
    AI](../Page/人工智能史.md "wikilink"), [AI
    winter](../Page/人工智慧低谷.md "wikilink"), or .

44.
45.
46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57.
58.
59. Pat Langley, ["The changing science of machine
    learning"](http://www.springerlink.com/content/j067h855n8223338/),
    *Machine Learning*, Volume 82, Number 3, 275–279,

60.
61.