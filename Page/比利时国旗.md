**[比利时国旗](../Page/比利时.md "wikilink")**（、、）是由[黑](../Page/黑.md "wikilink")、[黄](../Page/黄.md "wikilink")、[红三条竖条组成的](../Page/红.md "wikilink")[三色旗](../Page/三色旗.md "wikilink")；三條垂直的设计取自[法国国旗](../Page/法国国旗.md "wikilink")，而它的颜色则是取自[布拉班侯爵](../Page/比利时历史.md "wikilink")（Brabant）徽章的颜色。掛起時，黑色必須靠在旗桿一側。

正式的國旗比例是罕有的13:15，這種配置很少見。相反地，在大多數場合上使用的是常見的2:3或相近比例的國旗，連大多數政府機關與行政機構也是使用這種國旗\[1\]
。

顔色意为紋章，為黑色盾牌和紅色爪子和舌頭的黃色獅子。

这面旗帜在1831年1月23日被正式采纳为比利时国旗，它最早源自独立战争时期比利时人抗击[荷兰统治时所使用的战旗](../Page/荷兰.md "wikilink")。

[Gravensteen_copy.jpg](https://zh.wikipedia.org/wiki/File:Gravensteen_copy.jpg "fig:Gravensteen_copy.jpg")

### 比利時歷史旗幟

Flag of the Brabantine Revolution.svg|比利時合眾國國旗，又稱之為比利時聯合省 Flag of the
Netherlands.svg|[尼德蘭聯合王國國旗](../Page/荷蘭國旗.md "wikilink") Flag of
France.svg|[法國占領時代的國旗](../Page/法國.md "wikilink") Flag of Belgium
(1830).svg| [比利時革命革命旗](../Page/比利時革命.md "wikilink")（1830）

## 歷史上的國旗

在[查理大帝死後](../Page/查理大帝.md "wikilink")，現今的比利時領土（除[佛蘭德伯國之外](../Page/佛蘭德伯國.md "wikilink")）變成的一部分，其國旗是由水平的兩條紅色中間夾白色組成\[2\]。這片領土後來傳到西班牙手中，在[神聖羅馬帝國的查理五世加冕之後](../Page/查理五世_\(神聖羅馬帝國\).md "wikilink")，國旗上加入了西班牙的黃色與紅色。從十六世紀一直到十八世紀末，比利時的顏色一直維持紅色、白色與黃色\[3\]。有時，國旗的白色部分會有紅色的[勃根地十字旗](../Page/勃根地十字旗.md "wikilink")\[4\]。

在奧地利統治期間，曾嘗試過若干種不同的國旗，直到奧地利皇帝強制使用為止。[布魯塞爾反對這項措施](../Page/布魯塞爾.md "wikilink")，並跟隨法國的例子，紅黃黑的開始出現，這些是[布拉班特公國的顏色](../Page/布拉班特公國.md "wikilink")\[5\]。這些顏色於是對應到、與[盧森堡省的紅色獅子](../Page/盧森堡省.md "wikilink")，[布拉班特公國的黃色獅子](../Page/布拉班特公國.md "wikilink")，以及[佛蘭德伯國與](../Page/佛蘭德伯國.md "wikilink")[那慕爾省的黑色獅子](../Page/那慕爾省.md "wikilink")\[6\]。

## 目前國旗的獨立與採用

1830年8月26日，[皇家鑄幣局劇院暴動的隔天](../Page/皇家鑄幣局劇院.md "wikilink")，[比利時革命開始](../Page/比利時革命.md "wikilink")，[布魯塞爾市政廳掛上](../Page/布魯塞爾市政廳.md "wikilink")[法國國旗](../Page/法國國旗.md "wikilink")。貿然替換掉附近紡織店縫製的紅黃黑水平三色旗，與使用的旗幟相似\[7\]。結果，第193條規定比利時國旗的顏色是「紅色、黃色與黑色」，順序與上述的正式國旗不同\[8\]。
1831年1月23日，國旗的條紋從水平改成垂直，10月12日國旗來到了現今的樣式，黑色位於旗桿側。

## 設計與規範

比利時協議的官方指南說明國旗高2.6公尺、寬3公尺\[9\]，因此比例是13:15。每一條條紋佔旗幟的三分之一寬。

| 顏色格式                                     | 黑色 | 黃色        | 紅色 |
| ---------------------------------------- | -- | --------- | -- |
| [彩通](../Page/彩通.md "wikilink")\[10\]     |    | Black     |    |
| [CMYK](../Page/CMYK.md "wikilink")\[11\] |    | 0-0-0-100 |    |
| [RGB](../Page/RGB.md "wikilink")\[12\]   |    | 0-0-0     |    |
| [十六進位](../Page/網頁顏色.md "wikilink")       |    | \#000000  |    |

## 各種樣式

RoyalPalaceBrusselsFlag.jpg|[布魯塞爾王宮與](../Page/布魯塞爾王宮.md "wikilink")[拉肯王家城堡上的國旗](../Page/拉肯王家城堡.md "wikilink")，顯示國王在比利時或在國外。比例是少見的4:3，使得旗幟的高度超過寬度。
Naval Jack of Belgium.svg|比利時海上用旗、縱橫比1:1

### 國旗與民用旗

國旗的法定比例是罕有的13:15，且很少見。比例相近於2:3的旗幟在大多數場合上被使用，連大多數政府機關與行政機構也是使用這種國旗\[13\]。罕有的13:15比例可能源自於19世紀外交部的一道指示，根據該指示，正式國旗是2.6公尺高，3公尺寬\[14\]。

比例2:3的旗幟技術上屬於[民用旗](../Page/民用旗.md "wikilink")，是給在海上行駛的商船使用的旗幟\[15\]。

### 軍艦旗

比利時的[軍艦旗是以白色為底](../Page/軍艦旗.md "wikilink")，上有[聖安得烈十字呈現的](../Page/聖安得烈十字.md "wikilink")[國家代表三色](../Page/國家代表色.md "wikilink")，以及在頂部有黑色皇冠在交叉大砲上，底部是黑色船錨。這面旗幟在1950年被製造出來，就在重新建立的不久之後，在此之前是[第二次世界大戰時英國](../Page/第二次世界大戰.md "wikilink")[皇家海軍的一部分](../Page/皇家海軍.md "wikilink")，並讓人聯想起皇家海軍的[白船旗](../Page/白船旗.md "wikilink")\[16\]。

比利時也有正式的[海上用旗](../Page/海上用旗.md "wikilink")，與國旗相同，不過比例為1:1，為一正方形\[17\]。

### 皇家標準及皇室宮廷使用的旗幟

[比利時皇家標準是現任國王的個人標準](../Page/比利時旗幟列表.md "wikilink")，現任國王為[菲利普](../Page/菲利普_\(比利時國王\).md "wikilink")，特色是他的[花押字](../Page/花押字.md "wikilink")「F」（[荷蘭語的](../Page/荷蘭語.md "wikilink")「Filip」），以及在四個角落交叉的「P」。此皇家標準與過去君王相差不大\[18\]。

值得注意的是，在[布魯塞爾王宮與](../Page/布魯塞爾王宮.md "wikilink")[拉肯王家城堡上的國旗使用的不是上述任何的比例](../Page/拉肯王家城堡.md "wikilink")。其比例是非正規的4:3，使得它高度超過寬度\[19\]。條紋仍然是垂直，此比例解釋是為了美觀的考量，因為宮殿很大，旗幟因此是從遠低於其高度的地方往上看，由於[透視縮短](../Page/透視投影.md "wikilink")，這會讓旗幟看起來比較正常\[20\]。

當國王在比利時時，旗幟升起於宮殿上，不一定代表國王在宮殿中。當國王在其他國家進行國事訪問或在比利時境外度假時，旗幟便降下\[21\]。此規則有些許例外，但一般來說，旗幟的升起與否，可以作為國王是否在境內的一個合理可靠的參考。

## 協議

[Belgian_Senate,_Brussels.jpg](https://zh.wikipedia.org/wiki/File:Belgian_Senate,_Brussels.jpg "fig:Belgian_Senate,_Brussels.jpg")是少數使用正式13:15國旗的建築物\]\]
因為比利時是採行[聯邦制](../Page/聯邦制.md "wikilink")，比利時國旗與各[行政區劃旗幟的位階原則上相同](../Page/比利時行政區劃.md "wikilink")\[22\]。然而，當旗幟被升起、降落或在遊行中使用時，國旗優先於其他所有旗幟\[23\]。

優先順序如下：\[24\]

1.  比利時國旗

2.  各[行政區劃之旗幟](../Page/比利時行政區劃.md "wikilink")

3.  [歐盟旗幟](../Page/歐盟旗幟.md "wikilink")

4.  之旗幟，若多於一面，照當地語言之字母順序

5.  各[城市之旗幟](../Page/比利時城市列表.md "wikilink")

若有[國家元首來訪](../Page/國家元首.md "wikilink")，該國國旗會設在第二優先，其他旗幟下降一階\[25\]。

## 參考資料

[B](../Category/各国国旗.md "wikilink")
[Category:比利时国家象征](../Category/比利时国家象征.md "wikilink")
[Category:比利时旗帜](../Category/比利时旗帜.md "wikilink")
[Category:垂直三色旗](../Category/垂直三色旗.md "wikilink")

1.  See for example the Belgian Federal Government's website, where they
    do not display the official proportions of the national flag:

2.

3.
4.
5.
6.
7.
8.
9.
10.
11.
12. Converted from CMYK using [an online colour
    converter](http://web.forret.com/tools/color.asp).

13.
14.

15.

16.

17.
18.

19.
20.
21.
22.
23.
24.
25.