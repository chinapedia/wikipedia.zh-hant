[Maria,_Bopha_and_Saomai_2006-08-07_0435Z.jpg](https://zh.wikipedia.org/wiki/File:Maria,_Bopha_and_Saomai_2006-08-07_0435Z.jpg "fig:Maria,_Bopha_and_Saomai_2006-08-07_0435Z.jpg")（[颱風桑美](../Page/颱風桑美_\(2006年\).md "wikilink")）已增強為颱風\]\]

**颱風**（，[香港天文台縮寫](../Page/香港天文台.md "wikilink")**T.**；/**たいふう**/**taifū**；）是[赤道以北](../Page/赤道.md "wikilink")，[國際換日線以西](../Page/國際換日線.md "wikilink")，[亞太國家或地區對](../Page/亞太地區.md "wikilink")[熱帶氣旋的一個分級](../Page/熱帶氣旋.md "wikilink")。在[氣象學上](../Page/氣象學.md "wikilink")，按[世界氣象組織定義](../Page/世界氣象組織.md "wikilink")，熱帶氣旋中心持續風速達到[12級](../Page/蒲福風級.md "wikilink")（即64[節或以上](../Page/節_\(單位\).md "wikilink")、32.7[m/s或以上](../Page/公尺每秒.md "wikilink")，又或者118[km/hr或以上](../Page/公里每小時.md "wikilink")）稱為**[颶風](../Page/颶風.md "wikilink")**（**Hurricane**）或其他在地近義字\[1\]。西北太平洋地區採用之近義字乃颱風。世界氣象組織及日本氣象廳均以此為熱帶氣旋的最高級別，但部份氣象部門會按需要而設立更高級別，如中國中央氣象台及香港天文台之[強颱風](../Page/強颱風.md "wikilink")、[超強颱風](../Page/超強颱風.md "wikilink")，中華民國中央氣象局之[強烈颱風](../Page/強烈颱風.md "wikilink")，以及美國聯合颱風警報中心的[超級颱風](../Page/超級颱風.md "wikilink")。

廣義上，「颱風」這個詞並非一種熱帶氣旋強度。在[臺灣](../Page/臺灣.md "wikilink")、[日本等地](../Page/日本.md "wikilink")，將中心持續風速每秒17.2米或以上的[熱帶氣旋](../Page/熱帶氣旋.md "wikilink")（包括世界氣象組織定義中的[熱帶風暴](../Page/熱帶風暴.md "wikilink")、[強烈熱帶風暴和颱風](../Page/強烈熱帶風暴.md "wikilink")）均稱颱風。在非正式場合，「颱風」甚至直接泛指[熱帶氣旋本身](../Page/熱帶氣旋.md "wikilink")。當西北太平洋的熱帶氣旋達到熱帶風暴的強度，[區域專責氣象中心](../Page/區域專責氣象中心.md "wikilink")（）[日本氣象廳會對其編號及命名](../Page/日本氣象廳.md "wikilink")，名稱由世界氣象組織[颱風委員會的](../Page/颱風委員會.md "wikilink")14個國家和地區提供。

但在不同海洋上也各自有地區性的名稱，例如北太平洋西部稱為「颱風」、北大西洋稱為「颶風」、北印度洋稱為「氣旋風暴」。

據[美國海軍的](../Page/美國海軍.md "wikilink")[聯合颱風警報中心統計](../Page/聯合颱風警報中心.md "wikilink")，1959年至2004年間西北太平洋及[南海海域的颱風發生的個數與月份](../Page/南海.md "wikilink")，平均每年有26.5個颱風生成，出現最多颱風的月份是8月，其次是7月和9月。

## 名称由来

希臘神話其中名為[泰風](../Page/泰風.md "wikilink")（，又譯为堤丰或堤福俄斯）的[泰坦](../Page/泰坦.md "wikilink")，是象徵風暴的邪恶巨人。該詞在希臘語中義為「暴風」或「冒煙者」。堤豐也象徵惡風。《神譜》說它戰敗後，從身上生出無數股狂颱，專門滋害往來的海船。特指印度洋的風暴。

又有說該詞源於粵語「大風」者，因为粤语中“大风”发音为“daai6
fung1”，該詞彙經波斯語、阿拉伯語、葡萄牙語，最終传至英语成typhoon，而荷蘭語則是Tyfoon。

但據清王士禛《香祖筆記》，「臺灣風信與他海殊異，風大而烈者為颶，又甚者為颱。颶倏發倏止，颱常連日夜不止。正、二、三、四月發者為颶，五、六、七、八月發者為颱。」可見今日的「颱風」一詞，源出彼時的「颱」字。

較強的熱帶氣旋中，發生於北太平洋西部及南中國海者稱為「颱風（Typhoon）」；於大西洋西部、加勒比海、墨西哥灣和北太平洋東部者稱為「颶風（Hurricane）」；在印度洋上則可簡稱為「氣旋（Cyclone）」。

## 生成

台风发源于热带海面，那里温度高，大量的海水被蒸发到了空中，形成一个低气压中心。随着气压的变化和地球自身的运动，流入的空气也旋转起来，形成一个逆时针旋转的空气漩涡，这就是热带气旋。只要气温不下降，这个热带气旋就会越来越强大，最后形成了台风。

### 生成的动力

### 生成的条件

### 生成的地点

## 结构

## 路徑

[Pacific_typhoon_tracks_1980-2005.jpg](https://zh.wikipedia.org/wiki/File:Pacific_typhoon_tracks_1980-2005.jpg "fig:Pacific_typhoon_tracks_1980-2005.jpg")熱帶氣旋路徑圖，右邊的直線是[國際日期變更線](../Page/國際日期變更線.md "wikilink")\]\]
颱風的主要生成地區為西北[太平洋及](../Page/太平洋.md "wikilink")[南海](../Page/南中國海.md "wikilink")。當中以南海生成的熱帶氣旋數目較少。太平洋上的熱帶氣旋基本上全年都會生成，而以七至十月次數最為頻密。熱帶氣旋生成的位置分佈與季節有關，在冬、春季較為偏南，夏天和初秋較為偏北。

熱帶氣旋生成後的移動路徑主要受[副熱帶高氣壓](../Page/副熱帶高氣壓.md "wikilink")（副高）外圍氣流影響，所以副高的位置和範圍基本上決定了熱帶氣旋的路徑，其移動路徑大約可分為：

### 西進型

熱帶氣旋在[菲律賓以東海域](../Page/菲律賓.md "wikilink")，菲律賓以西海域（即南海）形成後，向西移動，並逐漸稍為偏北。於不同[緯度生成的熱帶氣旋所經過的地區不一](../Page/緯度.md "wikilink")。緯度較低時會橫過菲律賓後進入南海，最後在[中國](../Page/中國.md "wikilink")[廣東](../Page/廣東.md "wikilink")、[海南](../Page/海南.md "wikilink")，或[越南登陸](../Page/越南.md "wikilink")。緯度較高時可以經過[巴士海峽](../Page/巴士海峽.md "wikilink")，影響[臺灣](../Page/臺灣.md "wikilink")，然後在中國的廣東、[福建登陸](../Page/福建.md "wikilink")。由於冬、春兩季，副高位處較南，所以每年11月至5月的颱風生成緯度較低，路徑亦會偏南，一般只在北緯16度以南進入南中國海，最後在越南登陸，間中亦影響[泰國](../Page/泰國.md "wikilink")，甚至進入[孟加拉灣](../Page/孟加拉灣.md "wikilink")，例如[2013年的](../Page/2013年太平洋颱風季.md "wikilink")[颱風百-{}-合](../Page/颱風百合_\(2013年\).md "wikilink")。

### 西北型

路徑和西进型接近，但向北分量增加。西北型颱風可以橫過臺灣島或穿越巴士海峽、之後在中國大陸廣東及福建沿岸登陸；亦可能經過[琉球群島](../Page/琉球群島.md "wikilink")，在中國大陸[浙江](../Page/浙江.md "wikilink")、[江蘇一帶沿海登陸](../Page/江蘇.md "wikilink")。這種路徑的颱風多數出現於每年盛夏七至八月間，例如2013年的[颱風菲特](../Page/颱風菲特_\(2013年\).md "wikilink")。

### 轉向型

熱帶氣旋在菲律賓以東或[關島附近形成後](../Page/關島.md "wikilink")，先向西北偏西移動，至北緯15至30度附近轉向西北，最後轉向北和東北，成一拋物線。在11月至5月，多數轉向型熱帶氣旋在東經130度以東的太平洋上轉向北，較少登陸東亞陸地，主要影響太平洋上的島嶼。6月和10至11月的轉向型熱帶氣旋亦多數在海上轉向。7至8月的熱帶氣旋在較北、較西的地方轉向，途徑可以影響[臺灣海峽北部和](../Page/臺灣海峽.md "wikilink")[東海沿岸地區](../Page/東中國海.md "wikilink")，轉向後可以影響[朝鮮半島和](../Page/朝鮮半島.md "wikilink")[日本等地](../Page/日本.md "wikilink")，例如2013年的[颱風范斯高](../Page/颱風范斯高_\(2013年\).md "wikilink")。

颱風轉的方向是決定於其位置，位於北半球的颱風會作逆時針方向轉動，位於南半球的颱風會作順時針方向轉動。\[2\]

### 迷走型

部份熱帶氣旋因外圍引導氣流不明，或受其他天氣系統影響（如[藤原效應或](../Page/藤原效應.md "wikilink")[季候風](../Page/季候風.md "wikilink")），路徑出現打轉、停滯等，例如[2012年的](../Page/2012年太平洋颱風季.md "wikilink")[颱風天秤](../Page/颱風天秤_\(2012年\).md "wikilink")、2013年的[颱風羅莎](../Page/颱風羅莎_\(2013年\).md "wikilink")、[2016年的](../Page/2016年太平洋颱風季.md "wikilink")[颱風-{zh:獅子山;zh-cn:狮子山;zh-hk:獅子山;zh-mo:獅子山;zh-tw:獅子山}-及](../Page/颱風獅子山_\(2016年\).md "wikilink")[2017年的](../Page/2017年太平洋颱風季.md "wikilink")[颱風奥鹿](../Page/颱風奥鹿_\(2017年\).md "wikilink")。

## 強度分級

## 命名及編號

因為海洋上可能同時出現多個熱帶氣旋。在西北太平洋，几乎每两年都会出现一次3个热带气旋同时出现的“三旋共舞”现象。最高纪录是1960年8月23日，[贝丝](../Page/1960年太平洋台风季.md "wikilink")、[卡门](../Page/1960年太平洋台风季.md "wikilink")、[黛拉](../Page/1960年太平洋台风季.md "wikilink")、[艾琳](../Page/1960年太平洋颱風季.md "wikilink")、[费依等](../Page/1960年太平洋台风季.md "wikilink")5个台风同时出现而形成的“五旋共舞”\[3\]。由于当时这5个台风构成了一个类似倒置的奥运五环的形状且当年举办了[罗马奥运会](../Page/1960年夏季奥林匹克运动会.md "wikilink")，这个现象也被称为“五环台风”\[4\]。

[美國軍方於](../Page/美國.md "wikilink")[關島上設置的](../Page/關島.md "wikilink")[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")（現已移至[夏威夷](../Page/夏威夷.md "wikilink")），在[二次大戰習慣對熱帶氣旋給予名字](../Page/二次大戰.md "wikilink")，方便識別。最初的名字全為女性，後來在1979年加入男性名字。從1947年至1999年，西北太平洋及[南海區域的](../Page/南海.md "wikilink")[熱帶氣旋非正式地採用上述名字](../Page/熱帶氣旋.md "wikilink")。

早在20世紀初至中期，[中國及](../Page/中國.md "wikilink")[日本已自行為區內的熱帶氣旋編配一個](../Page/日本.md "wikilink")4位數字編號，編號首2位為年份，後2位為該年順序號。例如0312，即2003年第12號熱帶氣旋。而[美國海軍則為整個西北太平洋內的熱帶氣旋編配一個兩位數字編號](../Page/美國海軍.md "wikilink")，其後並改成兩位元數字加上英文字母****。

為減少混亂，日本在1981年獲委託為每個西北太平洋及南海區域內的達到熱帶風暴強度的熱帶氣旋編配一個國際編號，但容許其他地區繼續自行給予編號。自此，在大部分國際發佈中，發佈機構會把國際編號放在括號內（JTWC除外）。但是，各氣象機構有時對熱帶氣旋的編號會有差別，主要是因為其對熱帶氣旋強度的評估有所不同。例如在[2006年風季](../Page/2006年太平洋颱風季.md "wikilink")，[中國氣象局曾對一個未被](../Page/中國氣象局.md "wikilink")[日本氣象廳命名的風暴](../Page/日本氣象廳.md "wikilink")（中國氣象局的0614）編號，因此在餘下的風季，前者的編號都比後者的多出一個\[5\]\[6\]。

由2000年開始，西北太平洋的熱帶氣旋命名表由世界氣象組織[颱風委員會](../Page/颱風委員會.md "wikilink")（[英語](../Page/英語.md "wikilink")：）制訂。共有五份命名表分別由14個委員國各提供兩個名字組成，名字會由所提供國家的英文國名順序使用。不同於大西洋及東北太平洋，-{循}-環使用（即用完140個後名稱，回到第一個重新開始）。[日本氣象廳會同時根據這一套新名單為這些熱帶氣旋命名](../Page/日本氣象廳.md "wikilink")。這些名字及編號除了用於為國際[航空及](../Page/航空.md "wikilink")[航海界發放的預測和警報外](../Page/航海.md "wikilink")，亦是向國際媒體發放熱帶氣旋消息時採用的規範名稱。

中國大陸、香港和澳门是颱風委員會的会员，中國氣象局、香港天文臺及澳門氣象局使用三家商議一致的中文譯名。\[7\]\[8\]臺灣不是委员会会员，使用另行翻譯的中文译名。另外，儘管台灣每年皆會受熱帶氣旋侵襲，但由於[聯合國大會2758號決議](../Page/聯合國大會2758號決議.md "wikilink")，所以並非世界氣象組織颱風委員會的委員，不僅無法提供颱風名稱，亦無法提出除名要求。

當熱帶氣旋在某地區造成嚴重破壞，該委員國可要求將其除名。為該熱帶氣旋起名的國家會再提一個名字作替補。例如中國大陸及香港會由市民提名，再選出若干優勝名字，提交世界氣象組織確認選擇其中一個名字。\[9\]

[菲律賓大氣地球物理和天文服務管理局未必會使用世界氣象組織的颱風命名](../Page/菲律賓大氣地球物理和天文服務管理局.md "wikilink")，例如2013年造成災難性傷亡的[颱風海燕](../Page/颱風海燕_\(2013年\).md "wikilink")，在菲律賓的命名叫尤蘭沓（Yolanda），和世界氣象組織一樣，如果造成嚴重傷亡則將被除名。

### 世界氣象組織命名表

| 提供國家／地區                                  | 名稱                                     |
| ---------------------------------------- | -------------------------------------- |
| [柬埔寨](../Page/柬埔寨.md "wikilink")         | [達維](../Page/颱風達維.md "wikilink")       |
| [中華人民共和國](../Page/中華人民共和國.md "wikilink") | [海葵](../Page/颱風海葵.md "wikilink")       |
| [朝鮮](../Page/朝鮮.md "wikilink")           | [鴻雁](../Page/颱風鴻雁.md "wikilink")       |
| [香港](../Page/香港.md "wikilink")           | [鴛鴦](../Page/颱風鴛鴦.md "wikilink")       |
| [日本](../Page/日本.md "wikilink")           | [小狗](../Page/颱風天枰.md "wikilink")       |
| [寮國](../Page/寮國.md "wikilink")           | [布拉萬](../Page/颱風布拉萬.md "wikilink")     |
| [澳門](../Page/澳門.md "wikilink")           | [三巴](../Page/颱風珍珠.md "wikilink")       |
| [馬來西亞](../Page/馬來西亞.md "wikilink")       | [杰拉華](../Page/颱風杰拉華.md "wikilink")     |
| [密克羅尼西亞](../Page/密克羅尼西亞聯邦.md "wikilink") | [艾雲尼](../Page/颱風艾雲尼.md "wikilink")     |
| [菲律賓](../Page/菲律賓.md "wikilink")         | [馬力斯](../Page/颱風馬力斯.md "wikilink")     |
| [韓國](../Page/韓國.md "wikilink")           | [格美](../Page/颱風格美.md "wikilink")       |
| [泰國](../Page/泰國.md "wikilink")           | [派比安](../Page/颱風派比安.md "wikilink")     |
| [美國](../Page/美國.md "wikilink")           | [瑪莉亞](../Page/颱風瑪莉亞.md "wikilink")     |
| [越南](../Page/越南.md "wikilink")           | [山神](../Page/颱風桑美.md "wikilink")       |
| [柬埔寨](../Page/柬埔寨.md "wikilink")         | [安比](../Page/颱風安比.md "wikilink")       |
| [中華人民共和國](../Page/中華人民共和國.md "wikilink") | [悟空](../Page/颱風悟空.md "wikilink")       |
| [朝鮮](../Page/朝鮮.md "wikilink")           | [雲雀](../Page/颱風雲雀.md "wikilink")       |
| [香港](../Page/香港.md "wikilink")           | [珊珊](../Page/颱風珊珊.md "wikilink")       |
| [日本](../Page/日本.md "wikilink")           | [摩羯](../Page/颱風摩羯.md "wikilink")       |
| [寮國](../Page/寮國.md "wikilink")           | [麗琵](../Page/颱風麗琵.md "wikilink")       |
| [澳門](../Page/澳門.md "wikilink")           | [貝碧嘉](../Page/颱風貝碧嘉.md "wikilink")     |
| [馬來西亞](../Page/馬來西亞.md "wikilink")       | **[溫比亞](../Page/颱風溫比亞.md "wikilink")** |
| [密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")   | [蘇力](../Page/颱風蘇力.md "wikilink")       |
| [菲律賓](../Page/菲律賓.md "wikilink")         | [西馬侖](../Page/颱風西馬侖.md "wikilink")     |
| [韓國](../Page/韓國.md "wikilink")           | [飛燕](../Page/颱風飛燕.md "wikilink")       |
| [泰國](../Page/泰國.md "wikilink")           | **[山竹](../Page/颱風山竹.md "wikilink")**   |
| [美國](../Page/美國.md "wikilink")           | [百里嘉](../Page/颱風百里嘉.md "wikilink")     |
| [越南](../Page/越南.md "wikilink")           | [潭美](../Page/颱風潭美.md "wikilink")       |

  - **粗體字**表示該名稱已遭永久退役，新名稱有待公佈。

### 颱風命名中英對照表

<table>
<thead>
<tr class="header">
<th><p>提供國家／地區</p></th>
<th><p>颱風名稱中英標準翻譯對照表<br />
（中國大陸、香港和澳門譯名/中華民國譯名）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>柬埔寨</p></td>
<td><p>-{達維/丹瑞}-<br />
Damrey</p></td>
</tr>
<tr class="even">
<td><p>中國大陸</p></td>
<td><p>-{海葵}-<br />
Haikui</p></td>
</tr>
<tr class="odd">
<td><p>朝鮮</p></td>
<td><p>-{鴻雁}-<br />
Kirogi</p></td>
</tr>
<tr class="even">
<td><p>香港</p></td>
<td><p>-{鴛鴦}-<br />
Yun-yeung</p></td>
</tr>
<tr class="odd">
<td><p>日本</p></td>
<td><p>-{小狗}-<br />
Koinu</p></td>
</tr>
<tr class="even">
<td><p>寮國</p></td>
<td><p>-{布拉萬}-<br />
Bolaven</p></td>
</tr>
<tr class="odd">
<td><p>澳門</p></td>
<td><p>-{三巴}-<br />
Sanba</p></td>
</tr>
<tr class="even">
<td><p>馬來西亞</p></td>
<td><p>-{杰拉華/鯉魚}-<br />
Jelawat</p></td>
</tr>
<tr class="odd">
<td><p>米克羅尼西亞</p></td>
<td><p>-{艾雲尼/艾維尼}-<br />
Ewiniar</p></td>
</tr>
<tr class="even">
<td><p>菲律賓</p></td>
<td><p>-{馬力斯}-<br />
Maliksi</p></td>
</tr>
<tr class="odd">
<td><p>韓國</p></td>
<td><p>-{格美/凱米}-<br />
Gaemi</p></td>
</tr>
<tr class="even">
<td><p>泰國</p></td>
<td><p>-{派比安/巴比侖}-<br />
Prapiroon</p></td>
</tr>
<tr class="odd">
<td><p>美國</p></td>
<td><p>-{瑪莉亞}-<br />
Maria</p></td>
</tr>
<tr class="even">
<td><p>越南</p></td>
<td><p>-{山神}-<br />
Son Tinh</p></td>
</tr>
<tr class="odd">
<td><p>柬埔寨</p></td>
<td><p>-{安比}-<br />
Ampil</p></td>
</tr>
<tr class="even">
<td><p>中國大陸</p></td>
<td><p>-{悟空}-<br />
Wukong</p></td>
</tr>
<tr class="odd">
<td><p>朝鮮</p></td>
<td><p>-{雲雀}-<br />
Jongdari</p></td>
</tr>
<tr class="even">
<td><p>香港</p></td>
<td><p>-{珊珊}-<br />
Shanshan</p></td>
</tr>
<tr class="odd">
<td><p>日本</p></td>
<td><p>-{摩羯}-<br />
Yagi</p></td>
</tr>
<tr class="even">
<td><p>寮國</p></td>
<td><p>-{麗琵}-<br />
Leepi</p></td>
</tr>
<tr class="odd">
<td><p>澳門</p></td>
<td><p>-{貝碧嘉/貝碧佳}-<br />
Bebinca</p></td>
</tr>
<tr class="even">
<td><p>馬來西亞</p></td>
<td><p><em>-{未定}-</em></p></td>
</tr>
<tr class="odd">
<td><p>米克羅尼西亞</p></td>
<td><p>-{蘇力}-<br />
Soulik</p></td>
</tr>
<tr class="even">
<td><p>菲律賓</p></td>
<td><p>-{西馬侖/西馬隆}-<br />
Cimaron</p></td>
</tr>
<tr class="odd">
<td><p>韓國</p></td>
<td><p>-{飛燕/燕子}-<br />
Jebi</p></td>
</tr>
<tr class="even">
<td><p>泰國</p></td>
<td><p><em>-{未定}-</em></p></td>
</tr>
<tr class="odd">
<td><p>美國</p></td>
<td><p>-{百里嘉}-<br />
Barijat</p></td>
</tr>
<tr class="even">
<td><p>越南</p></td>
<td><p>-{潭美}-<br />
Trami</p></td>
</tr>
</tbody>
</table>

  - 如果某個颱風名字在兩地有不同譯名，將會以以下方式顯示：*中國大陸、香港和澳門譯名/中華民國譯名*

### 已停用的颱風命名

## 颱風預警

颱風預警是[熱帶氣旋盛行地區](../Page/熱帶氣旋.md "wikilink")，於風暴可能侵襲期間，由各地專責機構發出的預告、警告。目的在於通知當地居民及民防組織，採取適當的預防措施，如加固房屋門窗、加強排水作為或預防性撤離。這些警告涉及警告範圍內可能遭受的災害，而不是單純重複熱帶氣旋的預測路徑及強度，對於保障生命及財產安全尤為重要。

## 影響

## 相關

  - [熱帶氣旋](../Page/熱帶氣旋.md "wikilink")
  - [龍捲風](../Page/龍捲風.md "wikilink")
  - [颶風](../Page/颶風.md "wikilink")
  - [熱帶氣旋強度分析法](../Page/熱帶氣旋強度分析法.md "wikilink")
  - [香港熱帶氣旋警告信號](../Page/香港熱帶氣旋警告信號.md "wikilink")
  - [澳門熱帶氣旋警告信號](../Page/澳門熱帶氣旋警告信號.md "wikilink")
  - [中央氣象局颱風警報](../Page/中央氣象局颱風警報.md "wikilink")
  - [蒲福風級](../Page/蒲福風級.md "wikilink")

## 註解

## 外部連結

  - [臺灣颱風資訊中心全球資訊網](http://typhoon.ws/)
  - [台灣颱風論壇](http://twtybbs.com/)
  - [國際氣象組織](http://severe.worldweather.org/tc/wnp/acronyms.html#TS)
  - [香港天文臺：西北太平洋及南海熱帶氣旋名字(2015)](http://www.weather.gov.hk/informtc/sound/tcname2015_uc.htm)

**熱帶氣旋分級**

  - [西北太平洋熱帶氣旋移動之研究報告](http://hkcoc.weather.com.hk/trending.htm)
  - [熱帶氣旋字典](http://hkcoc.weather.com.hk/cycdict/index.htm)

[Category:按强度分类的热带气旋](../Category/按强度分类的热带气旋.md "wikilink")
[Category:香港天文台的熱帶氣旋等級](../Category/香港天文台的熱帶氣旋等級.md "wikilink")
[Category:热带气旋气象学](../Category/热带气旋气象学.md "wikilink")
[Category:台风](../Category/台风.md "wikilink")

1.  世界氣象組織 [CMM
    (WMO-No.471)](http://cawcr.gov.au/publications/BMRC_archive/tcguide/ch1/ch1_3.htm)

2.  黃釗俊（2001）．"颱風"，國立自然科學博物館，簡訊，90年出版163期
3.
4.
5.  [日本氣象廳](../Page/日本氣象廳.md "wikilink")，[2006年熱帶氣旋路徑圖](http://www.data.kishou.go.jp/yohou/typhoon/route_map/bstv2006.html)
    ，2006年日本的編號只有23個
6.  泉州颱風網，[根據中國氣象局編成的颱風資料](http://www.qzqxw.com/qztfwnew.asp)，2006年中華人民共和國的編號有24個
7.  [香港天文臺](../Page/香港天文臺.md "wikilink")[颱風名字中文譯名](http://www.hko.gov.hk/informtc/sound/tcname2006c.htm)
8.  [交通部中央氣象局](../Page/交通部中央氣象局.md "wikilink")，[氣象常識—颱風](http://www.cwb.gov.tw/V5/education/knowledge/Data/typhoon/ch1/007.htm)

9.  [香港天文臺](../Page/香港天文臺.md "wikilink")，[熱帶氣旋命名比賽結果](http://www.hko.gov.hk/wxinfo/news/2005/pre1124c.htm)