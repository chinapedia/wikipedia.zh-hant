**太平**（556年九月—557年十月）是[南朝梁政權梁敬帝](../Page/南朝梁.md "wikilink")[蕭方智的](../Page/蕭方智.md "wikilink")[年号](../Page/年号.md "wikilink")，共计近1年。

## 大事记

## 出生

## 逝世

## 纪年

| 太平                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 556年                           | 557年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[太平年號的政權](../Page/太平.md "wikilink")
  - 同期存在的其他政权年号
      - [天保](../Page/天保_\(高洋\).md "wikilink")（550年五月—559年十二月）：[北齊政权北齊文宣帝](../Page/北齊.md "wikilink")[高洋年号](../Page/高洋.md "wikilink")
      - [大定](../Page/大定_\(萧詧\).md "wikilink")（555年正月—562年正月）：[西梁政權梁宣帝](../Page/西梁.md "wikilink")[蕭詧的年号](../Page/蕭詧.md "wikilink")
      - [建昌](../Page/建昌_\(麴寶茂\).md "wikilink")（555年—560年）：[高昌政权](../Page/高昌.md "wikilink")[麴寶茂年号](../Page/麴寶茂.md "wikilink")
      - [開國](../Page/開國.md "wikilink")（551年—568年）：[新羅](../Page/新羅.md "wikilink")[真興王的年號](../Page/新羅真興王.md "wikilink")

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南梁年号](../Category/南梁年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:550年代中国政治](../Category/550年代中国政治.md "wikilink")
[Category:556年](../Category/556年.md "wikilink")
[Category:557年](../Category/557年.md "wikilink")