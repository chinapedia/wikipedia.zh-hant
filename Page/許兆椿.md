**許兆椿**（），[中國](../Page/中國.md "wikilink")[清朝官員](../Page/清朝.md "wikilink")，字茂堂，号秋岩，[湖北云梦人](../Page/湖北.md "wikilink")。

乾隆三十七年（1772年）中进士，選[庶吉士](../Page/庶吉士.md "wikilink")，[散館授](../Page/散館.md "wikilink")[编修](../Page/编修.md "wikilink")，是《[四库全书](../Page/四库全书.md "wikilink")》编纂者之一。改福建道監察御史，歷任[松江府知府](../Page/松江府.md "wikilink")、[江寧府知府等職](../Page/江寧府.md "wikilink")，升[漕运总督](../Page/漕运总督.md "wikilink")，後改任刑部侍郎、浙江巡抚等职。嘉庆十四年四月二十三，由仓场侍郎调任[廣西巡撫](../Page/廣西巡撫.md "wikilink")。嘉庆十四年十二月初六（1810年1月10日），调任[漕运总督](../Page/漕运总督.md "wikilink")。\[1\]。有《秋水阁诗集》。

## 參考文獻

  - [清史稿](../Page/清史稿.md "wikilink")
  - [許兆椿](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%B3%5C)

## 參考

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝監察御史](../Category/清朝監察御史.md "wikilink")
[Category:清朝松江府知府](../Category/清朝松江府知府.md "wikilink")
[Category:清朝江寧府知府](../Category/清朝江寧府知府.md "wikilink")
[Category:清朝漕運總督](../Category/清朝漕運總督.md "wikilink")
[Category:清朝貴州巡撫](../Category/清朝貴州巡撫.md "wikilink")
[Category:清朝浙江巡撫](../Category/清朝浙江巡撫.md "wikilink")
[Category:雲夢人](../Category/雲夢人.md "wikilink")
[Z兆](../Category/許姓.md "wikilink")
[Category:清朝廣西巡撫](../Category/清朝廣西巡撫.md "wikilink")

1.