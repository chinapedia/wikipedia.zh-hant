**山田耕筰**（），日本[古典音乐](../Page/古典音乐.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[指挥家](../Page/指挥家.md "wikilink")。

## 生平

作为日本[明治维新后不久出生的音乐家](../Page/明治维新.md "wikilink")，山田耕筰是日本最早的西洋[古典音乐作曲家之一](../Page/古典音乐.md "wikilink")。他出身于一个下级[武士家庭](../Page/武士.md "wikilink")，少年时就学于[东京音乐学校](../Page/东京艺术大学.md "wikilink")，后又于1910年至1913年在[柏林留学](../Page/柏林.md "wikilink")，曾师从[马克斯·布鲁赫](../Page/马克斯·布鲁赫.md "wikilink")。归国后致力于古典音乐的普及工作，包括创办了日本最早的专业交响乐团[日本交响乐协会](../Page/日本交响乐协会.md "wikilink")（[NHK交响乐团的前身](../Page/NHK交响乐团.md "wikilink")）。作为指挥家，他指挥了大量世界著名作品的日本首演。

山田耕筰名字“耕筰”的[罗马字写法原为](../Page/日语罗马字.md "wikilink")**Kōsaku**，后来在德国留学时，人们由于这个名字的读音与意大利语的“”（“什么”“东西”）以及德语的“”（“牛”）接近而取笑他，于是他将之改为**Kôsçak**。

## 作品

山田耕筰是一位产量很大的作曲家，作品总量大约有1600部，但是不少作品都在[第二次世界大战中被毁掉](../Page/第二次世界大战.md "wikilink")。他创作了日本最早的交响曲《胜鬥与和平》。他的歌剧《黑船》则是日本最受欢迎的歌剧之一。他还创作了大量童谣、校歌、军歌等。其中充满怀旧田园气息的童谣《[红蜻蜓](../Page/紅蜻蜓_\(童謠\).md "wikilink")》无可争议地成为山田最著名的作品。山田的音乐风格总体上是德奥后[浪漫主义和日本民族风格的结合](../Page/浪漫主义音乐.md "wikilink")。

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")
  - [滿洲國國歌](../Page/滿洲國國歌.md "wikilink")

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:20世纪作曲家](../Category/20世纪作曲家.md "wikilink")
[Category:東京藝術大學校友](../Category/東京藝術大學校友.md "wikilink")
[Category:文化勳章獲得者](../Category/文化勳章獲得者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")