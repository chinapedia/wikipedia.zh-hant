**特例市**（）為[日本曾有的](../Page/日本.md "wikilink")[城市](../Page/城市.md "wikilink")[自治制度之一](../Page/地方自治.md "wikilink")，自2000年開始實施，可擁有較一般的[市更多原本屬於](../Page/市.md "wikilink")[都道府縣的權限](../Page/都道府縣.md "wikilink")，但權限少於[中核市](../Page/中核市.md "wikilink")。在2015年4月1日起凍結申請許可前，當一個市的[人口超過](../Page/人口.md "wikilink")20萬人，並經市[議會及所屬](../Page/議會.md "wikilink")[都道府縣議會決議通過後可取得資格](../Page/都道府縣.md "wikilink")。

由於特例市擁有的自治權限與中核市差異不大，在匯集地方政府的意見後，特例市制度在2015年4月1日施行的新版《》遭到廢除，人口達到20萬的城市可直接申請成為中核市，未升格的特例市仍可繼續維持原有的自治權限，稱為「**施行時特例市**」（）。另做為特例，在特例市制度廢除後五年內，即2020年4月1日前，人口未達到20萬的特例市亦可申請成為中核市。\[1\]

## 權限

特例市取自原屬都道府縣的權限包括：\[2\]

  - 都市更新及開發
  - 制定屋外廣告物相關規範
  - 制定環保相關標準及規範。

## 特例市列表

日本現有31個施行時特例市：

<table>
<thead>
<tr class="header">
<th><p>地方</p></th>
<th><p>都道府縣</p></th>
<th><p>施行時特例市</p></th>
<th><p>升格日</p></th>
<th><p>註記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>style="text-align: center; | <a href="../Page/東北地方.md" title="wikilink">東北地方</a></p></td>
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Yamagata,_Yamagata.svg" title="fig:Flag_of_Yamagata,_Yamagata.svg">Flag_of_Yamagata,_Yamagata.svg</a> <strong><a href="../Page/山形市.md" title="wikilink">山形市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td><p><a href="../Page/都道府縣廳所在地.md" title="wikilink">縣廳所在地</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關東地方.md" title="wikilink">關東地方</a></p></td>
<td><p><a href="../Page/茨城縣.md" title="wikilink">茨城縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Mito,_Ibaraki.svg" title="fig:Flag_of_Mito,_Ibaraki.svg">Flag_of_Mito,_Ibaraki.svg</a> <strong><a href="../Page/水戶市.md" title="wikilink">水戶市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td><p>縣廳所在地</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Tsukuba_Ibaraki.JPG" title="fig:Flag_of_Tsukuba_Ibaraki.JPG">Flag_of_Tsukuba_Ibaraki.JPG</a> <strong><a href="../Page/筑波市.md" title="wikilink">筑波市</a></strong></p></td>
<td><p>2007年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/群馬縣.md" title="wikilink">群馬縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Isesaki_Gunma.JPG" title="fig:Flag_of_Isesaki_Gunma.JPG">Flag_of_Isesaki_Gunma.JPG</a> <strong><a href="../Page/伊勢崎市.md" title="wikilink">伊勢崎市</a></strong></p></td>
<td><p>2007年4月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ota,_Gunma.svg" title="fig:Flag_of_Ota,_Gunma.svg">Flag_of_Ota,_Gunma.svg</a> <strong><a href="../Page/太田市.md" title="wikilink">太田市</a></strong></p></td>
<td><p>2007年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/埼玉縣.md" title="wikilink">埼玉縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Tokorozawa,_Saitama.svg" title="fig:Flag_of_Tokorozawa,_Saitama.svg">Flag_of_Tokorozawa,_Saitama.svg</a> <strong><a href="../Page/所澤市.md" title="wikilink">所澤市</a></strong></p></td>
<td><p>2002年4月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Soka,_Saitama.svg" title="fig:Flag_of_Soka,_Saitama.svg">Flag_of_Soka,_Saitama.svg</a> <strong><a href="../Page/草加市.md" title="wikilink">草加市</a></strong></p></td>
<td><p>2004年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kasukabe,_Saitama.svg" title="fig:Flag_of_Kasukabe,_Saitama.svg">Flag_of_Kasukabe,_Saitama.svg</a> <strong><a href="../Page/春日部市.md" title="wikilink">春日部市</a></strong></p></td>
<td><p>2008年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kumagaya,_Saitama.svg" title="fig:Flag_of_Kumagaya,_Saitama.svg">Flag_of_Kumagaya,_Saitama.svg</a> <strong><a href="../Page/熊谷市.md" title="wikilink">熊谷市</a></strong></p></td>
<td><p>2009年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>rowspan="5" nowrap | <a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Odawara,_Kanagawa.svg" title="fig:Flag_of_Odawara,_Kanagawa.svg">Flag_of_Odawara,_Kanagawa.svg</a> <strong><a href="../Page/小田原市.md" title="wikilink">小田原市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Yamato,_Kanagawa.svg" title="fig:Flag_of_Yamato,_Kanagawa.svg">Flag_of_Yamato,_Kanagawa.svg</a> <strong><a href="../Page/大和市.md" title="wikilink">大和市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Hiratsuka,_Kanagawa.svg" title="fig:Flag_of_Hiratsuka,_Kanagawa.svg">Flag_of_Hiratsuka,_Kanagawa.svg</a> <strong><a href="../Page/平塚市.md" title="wikilink">平塚市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Atsugi,_Kanagawa.svg" title="fig:Flag_of_Atsugi,_Kanagawa.svg">Flag_of_Atsugi,_Kanagawa.svg</a> <strong><a href="../Page/厚木市.md" title="wikilink">厚木市</a></strong></p></td>
<td><p>2002年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Chigasaki,_Kanagawa.svg" title="fig:Flag_of_Chigasaki,_Kanagawa.svg">Flag_of_Chigasaki,_Kanagawa.svg</a> <strong><a href="../Page/茅崎市.md" title="wikilink">茅崎市</a></strong></p></td>
<td><p>2003年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中部地方.md" title="wikilink">中部地方</a></p></td>
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Joetsu,_Niigata.svg" title="fig:Flag_of_Joetsu,_Niigata.svg">Flag_of_Joetsu,_Niigata.svg</a> <strong><a href="../Page/上越市.md" title="wikilink">上越市</a></strong></p></td>
<td><p>2007年4月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Nagaoka,_Niigata.svg" title="fig:Flag_of_Nagaoka,_Niigata.svg">Flag_of_Nagaoka,_Niigata.svg</a> <strong><a href="../Page/長岡市.md" title="wikilink">長岡市</a></strong></p></td>
<td><p>2007年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Fukui,_Fukui.svg" title="fig:Flag_of_Fukui,_Fukui.svg">Flag_of_Fukui,_Fukui.svg</a> <strong><a href="../Page/福井市.md" title="wikilink">福井市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td><p>縣廳所在地</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山梨縣.md" title="wikilink">山梨縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kofu,_Yamanashi.svg" title="fig:Flag_of_Kofu,_Yamanashi.svg">Flag_of_Kofu,_Yamanashi.svg</a> <strong><a href="../Page/甲府市.md" title="wikilink">甲府市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td><p>縣廳所在地<br />
2017年10月1日統計資料為19萬人，是人口最少的特例市。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長野縣.md" title="wikilink">長野縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Matsumoto,_Nagano.svg" title="fig:Flag_of_Matsumoto,_Nagano.svg">Flag_of_Matsumoto,_Nagano.svg</a> <strong><a href="../Page/松本市.md" title="wikilink">松本市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靜岡縣.md" title="wikilink">靜岡縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Numazu,_Shizuoka.svg" title="fig:Flag_of_Numazu,_Shizuoka.svg">Flag_of_Numazu,_Shizuoka.svg</a> <strong><a href="../Page/沼津市.md" title="wikilink">沼津市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Fuji,_Shizuoka.svg" title="fig:Flag_of_Fuji,_Shizuoka.svg">Flag_of_Fuji,_Shizuoka.svg</a> <strong><a href="../Page/富士市.md" title="wikilink">富士市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛知縣.md" title="wikilink">愛知縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kasugai,_Aichi.svg" title="fig:Flag_of_Kasugai,_Aichi.svg">Flag_of_Kasugai,_Aichi.svg</a> <strong><a href="../Page/春日井市.md" title="wikilink">春日井市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ichinomiya,_Aichi.svg" title="fig:Flag_of_Ichinomiya,_Aichi.svg">Flag_of_Ichinomiya,_Aichi.svg</a> <strong><a href="../Page/一宮市.md" title="wikilink">一宮市</a></strong></p></td>
<td><p>2002年4月1日</p></td>
<td><p>2017年10月1日統計資料為38萬人，是人口最多的特例市。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/近畿地方.md" title="wikilink">近畿地方</a></p></td>
<td><p><a href="../Page/三重縣.md" title="wikilink">三重縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Yokkaichi,_Mie.svg" title="fig:Flag_of_Yokkaichi,_Mie.svg">Flag_of_Yokkaichi,_Mie.svg</a> <strong><a href="../Page/四日市市.md" title="wikilink">四日市市</a></strong></p></td>
<td><p>2000年11月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Suita,_Osaka.svg" title="fig:Flag_of_Suita,_Osaka.svg">Flag_of_Suita,_Osaka.svg</a> <strong><a href="../Page/吹田市.md" title="wikilink">吹田市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ibaraki,_Osaka.svg" title="fig:Flag_of_Ibaraki,_Osaka.svg">Flag_of_Ibaraki,_Osaka.svg</a> <strong><a href="../Page/茨木市.md" title="wikilink">茨木市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Neyagawa,_Osaka.svg" title="fig:Flag_of_Neyagawa,_Osaka.svg">Flag_of_Neyagawa,_Osaka.svg</a> <strong><a href="../Page/寢屋川市.md" title="wikilink">寢屋川市</a></strong></p></td>
<td><p>2001年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kishiwada,_Osaka.svg" title="fig:Flag_of_Kishiwada,_Osaka.svg">Flag_of_Kishiwada,_Osaka.svg</a> <strong><a href="../Page/岸和田市.md" title="wikilink">岸和田市</a></strong></p></td>
<td><p>2002年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/兵庫縣.md" title="wikilink">兵庫縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kakogawa,_Hyogo.svg" title="fig:Flag_of_Kakogawa,_Hyogo.svg">Flag_of_Kakogawa,_Hyogo.svg</a> <strong><a href="../Page/加古川市.md" title="wikilink">加古川市</a></strong></p></td>
<td><p>2002年4月1日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Takarazuka,_Hyogo.svg" title="fig:Flag_of_Takarazuka,_Hyogo.svg">Flag_of_Takarazuka,_Hyogo.svg</a> <strong><a href="../Page/寶塚市.md" title="wikilink">寶塚市</a></strong></p></td>
<td><p>2003年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九州_(日本).md" title="wikilink">九州地方</a></p></td>
<td><p><a href="../Page/佐賀縣.md" title="wikilink">佐賀縣</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Saga,_Saga.svg" title="fig:Flag_of_Saga,_Saga.svg">Flag_of_Saga,_Saga.svg</a><em>' <a href="../Page/佐賀市.md" title="wikilink">佐賀市</a></em>'</p></td>
<td><p>2014年4月1日</p></td>
<td><p>縣廳所在地</p></td>
</tr>
</tbody>
</table>

### 過去曾經是特例市的城市

| 地方                                                                                                                                                                           | 都道府縣                                                                                                                                                                                     | 城市                                                                                                                                                                                        | 成為特例市日期    | 解除特例市日期                                   | 理由及註記                                                                                                                          |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- | ----------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| style="text-align: center; | [北海道](../Page/北海道.md "wikilink")                                                                                                                | 北海道                                                                                                                                                                                      | [Flag_of_Hakodate,_Hokkaido.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hakodate,_Hokkaido.svg "fig:Flag_of_Hakodate,_Hokkaido.svg") [函館市](../Page/函館市.md "wikilink")              | 2000年11月1日 | 2005年10月1日                                | 升格為[中核市](../Page/中核市.md "wikilink")。                                                                                           |
| rowspan=2 style="text-align: center; | 東北地方                                                                                                                                  | [青森縣](../Page/青森縣.md "wikilink")                                                                                                                                                         | [Flag_of_Hachinohe,_Aomori.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hachinohe,_Aomori.svg "fig:Flag_of_Hachinohe,_Aomori.svg") [八戶市](../Page/八戶市.md "wikilink")                 | 2001年10月1日 | 2017年1月1日                                 | 升格為中核市。                                                                                                                        |
| [岩手縣](../Page/岩手縣.md "wikilink")                                                                                                                                             | [Flag_of_Morioka,_Iwate.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Morioka,_Iwate.svg "fig:Flag_of_Morioka,_Iwate.svg") [盛岡市](../Page/盛岡市.md "wikilink")                         | 2000年11月1日                                                                                                                                                                                | 2008年4月1日  | 升格為中核市。                                   |                                                                                                                                |
| 關東地方                                                                                                                                                                         | [群馬縣](../Page/群馬縣.md "wikilink")                                                                                                                                                         | [Flag_of_Maebashi,_Gunma.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Maebashi,_Gunma.svg "fig:Flag_of_Maebashi,_Gunma.svg") [前橋市](../Page/前橋市.md "wikilink")                       | 2001年4月1日  | 2009年4月1日                                 | 升格為中核市。                                                                                                                        |
| [Flag_of_Takasaki,_Gunma.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Takasaki,_Gunma.svg "fig:Flag_of_Takasaki,_Gunma.svg") [高崎市](../Page/高崎市.md "wikilink")          | 2001年4月1日                                                                                                                                                                                | 2011年4月1日                                                                                                                                                                                 | 升格為中核市。    |                                           |                                                                                                                                |
| [埼玉縣](../Page/埼玉縣.md "wikilink")                                                                                                                                             | [Flag_of_Koshigaya,_Saitama.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Koshigaya,_Saitama.svg "fig:Flag_of_Koshigaya,_Saitama.svg") [越谷市](../Page/越谷市.md "wikilink")             | 2003年4月1日                                                                                                                                                                                 | 2015年4月1日  | 升格為中核市。                                   |                                                                                                                                |
| [Flag_of_Kawaguchi,_Saitama.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kawaguchi,_Saitama.svg "fig:Flag_of_Kawaguchi,_Saitama.svg") [川口市](../Page/川口市.md "wikilink") | 2001年4月1日                                                                                                                                                                                | 2018年4月1日                                                                                                                                                                                 | 升格為中核市。    |                                           |                                                                                                                                |
| style="text-align: center; | 中部地方                                                                                                                                            | [靜岡縣](../Page/靜岡縣.md "wikilink")                                                                                                                                                         | [Flag_of_Shimizu_City,_Shizuoka.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Shimizu_City,_Shizuoka.svg "fig:Flag_of_Shimizu_City,_Shizuoka.svg") [清水市](../Page/清水市.md "wikilink") | 2001年4月1日  | 2003年4月1日                                 | 與[靜岡市](../Page/靜岡市.md "wikilink")[合併新的靜岡市](../Page/市町村合併.md "wikilink")，並成為中核市，目前靜岡市已成為[政令指定都市](../Page/政令指定都市.md "wikilink")。 |
| 近畿地方                                                                                                                                                                         | [滋賀縣](../Page/滋賀縣.md "wikilink")                                                                                                                                                         | [Flag_of_Otsu,_Shiga.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Otsu,_Shiga.svg "fig:Flag_of_Otsu,_Shiga.svg") [大津市](../Page/大津市.md "wikilink")                                   | 2001年4月1日  | 2009年4月1日                                 | 升格為中核市。                                                                                                                        |
| [大阪府](../Page/大阪府.md "wikilink")                                                                                                                                             | [Flag_of_Toyonaka,_Osaka.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Toyonaka,_Osaka.svg "fig:Flag_of_Toyonaka,_Osaka.svg") [豐中市](../Page/豐中市.md "wikilink")                      | 2001年4月1日                                                                                                                                                                                 | 2012年4月1日  | 升格為中核市。                                   |                                                                                                                                |
| [Flag_of_Hirakata,_Osaka.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hirakata,_Osaka.svg "fig:Flag_of_Hirakata,_Osaka.svg") [枚方市](../Page/枚方市.md "wikilink")          | 2001年4月1日                                                                                                                                                                                | 2014年4月1日                                                                                                                                                                                 | 升格為中核市。    |                                           |                                                                                                                                |
| [Flag_of_Yao,_Osaka.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Yao,_Osaka.svg "fig:Flag_of_Yao,_Osaka.svg") [八尾市](../Page/八尾市.md "wikilink")                         | 2001年4月1日                                                                                                                                                                                | 2018年4月1日                                                                                                                                                                                 | 升格為中核市。    |                                           |                                                                                                                                |
| [兵庫縣](../Page/兵庫縣.md "wikilink")                                                                                                                                             | [Flag_of_Amagasaki,_Hyogo.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Amagasaki,_Hyogo.svg "fig:Flag_of_Amagasaki,_Hyogo.svg") [尼崎市](../Page/尼崎市.md "wikilink")                   | 2001年4月1日                                                                                                                                                                                 | 2009年4月1日  | 升格為中核市。                                   |                                                                                                                                |
| [Flag_of_Akashi,_Hyogo.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Akashi,_Hyogo.svg "fig:Flag_of_Akashi,_Hyogo.svg") [明石市](../Page/明石市.md "wikilink")                | 2002年4月1日                                                                                                                                                                                | 2018年4月1日                                                                                                                                                                                 | 升格為中核市。    |                                           |                                                                                                                                |
| 中國地方                                                                                                                                                                         | [鳥取縣](../Page/鳥取縣.md "wikilink")                                                                                                                                                         | [Flag_of_Tottori,_Tottori.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Tottori,_Tottori.svg "fig:Flag_of_Tottori,_Tottori.svg") [鳥取市](../Page/鳥取市.md "wikilink")                    | 2005年10月1日 | 2018年4月1日                                 | 升格為中核市。                                                                                                                        |
| [島根縣](../Page/島根縣.md "wikilink")                                                                                                                                             | [Flag_of_Matsue,_Shimane.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Matsue,_Shimane.svg "fig:Flag_of_Matsue,_Shimane.svg") [松江市](../Page/松江市.md "wikilink")                      | 2012年4月1日                                                                                                                                                                                 | 2018年4月1日  | 升格為中核市。                                   |                                                                                                                                |
| [廣島縣](../Page/廣島縣.md "wikilink")                                                                                                                                             | [Flag_of_Kure,_Hiroshima.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kure,_Hiroshima.svg "fig:Flag_of_Kure,_Hiroshima.svg") [吳市](../Page/吳市.md "wikilink")                        | 2000年11月1日                                                                                                                                                                                | 2016年4月1日  | 升格為中核市。                                   |                                                                                                                                |
| [山口縣](../Page/山口縣.md "wikilink")                                                                                                                                             | [Flag_of_Shimonoseki,_Yamaguchi.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Shimonoseki,_Yamaguchi.svg "fig:Flag_of_Shimonoseki,_Yamaguchi.svg") [下關市](../Page/下關市.md "wikilink") | 2002年4月1日                                                                                                                                                                                 | 2005年2月13日 | 與菊川町、豐田町、豐浦町、豐北町合併為新成立的下關市，新下關市繼續被指定為特例市。 |                                                                                                                                |
| 2005年2月13日                                                                                                                                                                   | 2005年10月1日                                                                                                                                                                               | 升格為中核市。                                                                                                                                                                                   |            |                                           |                                                                                                                                |
| rowspan=2 style="text-align: center; | 九州地方                                                                                                                                  | [福岡縣](../Page/福岡縣.md "wikilink")                                                                                                                                                         | [Flag_of_Kurume,_Fukuoka.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kurume,_Fukuoka.svg "fig:Flag_of_Kurume,_Fukuoka.svg") [久留米市](../Page/久留米市.md "wikilink")                     | 2001年4月1日  | 2008年4月1日                                 | 升格為中核市。                                                                                                                        |
| [長崎縣](../Page/長崎縣.md "wikilink")                                                                                                                                             | [Flag_of_Sasebo,_Nagasaki.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sasebo,_Nagasaki.svg "fig:Flag_of_Sasebo,_Nagasaki.svg") [佐世保市](../Page/佐世保市.md "wikilink")                 | 2001年4月1日                                                                                                                                                                                 | 2016年4月1日  | 升格為中核市。                                   |                                                                                                                                |

## 參考資料

## 相關條目

  - [特別區](../Page/特別區.md "wikilink")
  - [政令指定都市](../Page/政令指定都市.md "wikilink")
  - [中核市](../Page/中核市.md "wikilink")

## 外部連結

  - [日本總務省 - 特例市](http://www.soumu.go.jp/cyukaku/tokurei.html)

[\*](../Category/特例市.md "wikilink") [\*](../Category/日本城市.md "wikilink")
[\*](../Category/日本行政區劃.md "wikilink")

1.  [日本總務省 - 中核市・施行時特例市](http://www.soumu.go.jp/cyukaku/)
2.  日本總務省 - 特例市 <http://www.soumu.go.jp/cyukaku/tokurei.html>