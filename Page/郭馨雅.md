**郭馨雅**為[台灣女性](../Page/台灣.md "wikilink")[配音員](../Page/配音員.md "wikilink")。

## 人物介紹

  - 為女聲合唱團「台北女聲合唱團」的成員之一。

## 配音作品

  - 主要角色名稱以**粗體**顯示。
  - 以下皆為國語配音。

### 台灣動畫

| 播映年份 | 作品名稱                                         | 配演角色    | 首播平台                           | 備註    |
| ---- | -------------------------------------------- | ------- | ------------------------------ | ----- |
| 2010 | [小貓巴克里](../Page/小貓巴克里.md "wikilink")         | **巴克里** | [公視](../Page/公視.md "wikilink") |       |
| 2011 | [山豬 飛鼠 撒可努](../Page/山豬_飛鼠_撒可努.md "wikilink") |         | [公視](../Page/公視.md "wikilink") |       |
| 2014 | [觀測站少年](../Page/觀測站少年.md "wikilink")         | 漢寶      | [公視](../Page/公視.md "wikilink") | \[1\] |
|      |                                              |         |                                |       |

### 台灣動畫電影

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/阿卡的冒險：光子秘密.md" title="wikilink">阿卡的冒險：光子秘密</a></p></td>
<td><p>穿山甲<br />
電視節目主持人</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 中國動畫

  - 《[閃電衝線](../Page/閃電衝線.md "wikilink")》：**李越銘**
  - 《閃電衝線三》：**宮本次郎、金成慧**
  - 《藍貓》：**藍貓**

### 中國電影

  - 《[桃姐](../Page/桃姐.md "wikilink")》：梅姑、金姨之女、辦公室秘書※台灣上映之國台語版

### 日本動畫

  - 《[火影忍者](../Page/火影忍者.md "wikilink")》：**[我愛羅](../Page/我愛羅.md "wikilink")**、**[祭](../Page/山中佐井.md "wikilink")**、[赤砂蠍](../Page/赤砂蠍.md "wikilink")（本體）、靜音、御手洗紅豆、[大蛇丸](../Page/大蛇丸.md "wikilink")（少年期）、菖蒲、輕、野原凜、二尾又旅
  - 《[銀魂](../Page/銀魂_\(動畫\).md "wikilink")》：**定春**、**猿飛菖蒲**、**柳生九兵衛**、**月詠**、**小玉**、陸奧、結野水晶、沖田三葉、花野咲、長谷川夏、葛葉、阿音、錦幾松、阿房、花子、來島真多子、巳厘野道滿（幼年）、阿國凱薩琳（第1集）、
  - 《[李洛克的青春全力忍傳](../Page/李洛克的青春全力忍傳.md "wikilink")》：祭、我愛羅、靜音、御手洗紅豆、赤砂蠍
  - 《[十二國記](../Page/十二國記.md "wikilink")》：大木鈴
  - 《[飛行船的冒險](../Page/飛行船的冒險.md "wikilink")》：小威廉、沙夏
  - 《[蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")》：羽佐間翔子、小楯衛、遠見弓子、赫斯特·蓋洛普
  - 《[嬉皮笑園](../Page/嬉皮笑園.md "wikilink")》：**片桐姬子**、芹澤茜、柏木優奈、美蒂亞、五十嵐美由紀
  - 《[捍衛者21](../Page/捍衛者21.md "wikilink")》：太刀川里香
  - 《[烏龍派出所](../Page/烏龍派出所.md "wikilink")》：**麻里愛**、清正奈緒子、擬寶珠纏、茱蒂、微笑宿舍管理員、角田廣美、純平、早乙女沙織、兩津友內、山口桃江
  - 《[我愛美樂蒂](../Page/奇幻魔法Melody.md "wikilink")》：**夢野歌**、小刺蝟哈利、旋律、留美、小栗鼠、啾米、美紀的媽媽、碓井幸、裕樹、教子、夏川夏美（第一季）、香取惠的媽媽、上野彩、千佳老師
  - 《[獸王星](../Page/獸王星.md "wikilink")》：卡利姆、阿澄
  - 《[彩雲國物語](../Page/彩雲國物語.md "wikilink")》：**紅秀麗**、縹珠翠、縹英姬、柴凜
  - 《[救難小英雄](../Page/救難小英雄_\(日昇動畫\).md "wikilink")》：**遙鈴**、**傑伊**、美波健、瑪莉教官
  - 《[怪傑佐羅力](../Page/怪俠佐羅利.md "wikilink")》（前譯怪俠索羅力）：**伊豬豬**
  - 《[坦克王](../Page/坦克王.md "wikilink")》：馬後炮
  - 《[藍龍](../Page/藍龍.md "wikilink")》：**克露克**、**瑪魯瑪洛**、辛西亞、洛妲雷絲、蘇、琳達、達奈爾
  - 《[高機動交響曲GPO](../Page/高機動交響曲GPO.md "wikilink")》：橫山亞美（白之章）、山口葉月（白之章）、齋藤奈津子（綠之章）、伯爵夫人（綠之章）、辻野友美（青之章）
  - 《[恐龍王](../Page/恐龍王.md "wikilink")》：蘇菲亞
  - 《[羅密歐的藍天](../Page/羅密歐的藍天.md "wikilink")》：丹特、尼基塔、米婆婆※[TVBS-G版本](../Page/TVBS-G.md "wikilink")
  - 《[楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")》：**明石薰**、小鹿圭子
  - 《[韋駄天翔](../Page/韋駄天翔.md "wikilink")》：**坂卷驅**、小雪
  - 《[閃電十一人](../Page/閃電十一人.md "wikilink")》：**圓堂守**、少林寺步、音無春奈、浦部莉香
  - 《[戰鬥陀螺
    鋼鐵奇兵](../Page/戰鬥陀螺_鋼鐵奇兵.md "wikilink")》：**鋼銀河**、**天童遊**、**小丸**（ZEROG）、波佐間光、雙道寒、熊手熊太、李赤雲、紅蓮、隆、和樹、茉提、吉古森、安索（4D）、多萊（4D）、歐燕
  - 《[麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")》：**神崎麗美**、**野村朋子**、上原杏子、村井樹里亞、嘉手納南風※[Channel
    \[V](../Page/Channel_V.md "wikilink")\]/[衛視電影台版本](../Page/衛視電影台.md "wikilink")
  - 《[狼與辛香料](../Page/狼與辛香料.md "wikilink")》：阿瑪提、狄安·魯本斯
  - 《[瀨戶的花嫁](../Page/瀨戶的花嫁_\(漫畫\).md "wikilink")》：**江戶前留奈**
  - 《[天才麻將少女](../Page/咲-Saki-.md "wikilink")》：**片岡優希**、龍門淵透華、池田華菜、東橫桃子
  - 《[鬼影投手](../Page/鬼影投手.md "wikilink")》：全部女性角色
  - 《[獵人（新版）](../Page/Hunter_×_Hunter.md "wikilink")》：**[小傑·富力士](../Page/小傑·富力士.md "wikilink")**、柯特·揍敵客、妮翁·諾斯拉、派克諾妲、波東果·拉波依、阿路加·揍敵客
  - 《[抓狂一族](../Page/抓狂一族.md "wikilink")》：**大澤木小鐵**※[東森電影台版本](../Page/東森電影台.md "wikilink")
  - 《[激戰！彈珠人](../Page/激戰！彈珠人.md "wikilink")》：**龍崎翔**
  - 《[旋風管家：雙眼只有你](../Page/旋風管家.md "wikilink")》：**瑪莉亞**、桂雪路、西澤步、橘-{亘}-、愛澤咲夜
  - 《[旋風管家：Cuties](../Page/旋風管家.md "wikilink")》：**瑪莉亞**、桂雪路、西澤步、橘-{亘}-、愛澤咲夜
  - 《[LINE OFFLINE](../Page/LINE.md "wikilink")》：**兔兔**
  - 《[天國少女](../Page/天國少女.md "wikilink")》：娜潔拉、山手由依
  - 《[噬血狂襲](../Page/噬血狂襲.md "wikilink")》：**藍羽淺蔥**、**葉瀨夏音**、寂靜破除者、緣堂緣、奧可塔薇亞
  - 《[鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")》：小白、阿香

### 歐美動畫

  - 《[少年忍者龜](../Page/忍者龜.md "wikilink")》：艾波·歐尼爾※[衛視中文台版本](../Page/衛視中文台.md "wikilink")
  - 《閃閃小超人》：**泰迪**、閃媽、艾老師

### 韓國動畫

  - 《[雙胞胎姐妹花](../Page/雙胞胎姐妹花.md "wikilink")》：**宋曉蘭**

### 韓劇

  - 《十二月的熱帶夜》：**吳永欣**
  - 《麻辣女人》：**金恩英**
  - 《[搞笑一家親](../Page/不可阻挡的High_Kick.md "wikilink")》：**朴海美**、金潤珠、敬和
  - 《[灰姑娘的姐姐](../Page/灰姑娘的姐姐.md "wikilink")》：**具孝善**、韓廷佑（童年）
  - 《[兩個妻子](../Page/兩個妻子.md "wikilink")》：**韓芝淑**、姜瀚星、張英子
  - 《愛你千萬次》：**朴愛朗**、池女士
  - 《慾望之火》：南愛莉、楊仁淑

### 日本動畫電影

  - 《[火影忍者疾風傳劇場版](../Page/火影忍者疾風傳劇場版.md "wikilink")》：**紫苑**
  - 《[火影忍者疾風傳劇場版 牽絆](../Page/火影忍者疾風傳劇場版_牽絆.md "wikilink")》：祭、靜音
  - 《[火影忍者疾風傳劇場版
    火意志的繼承者](../Page/火影忍者疾風傳劇場版_火意志的繼承者.md "wikilink")》：我愛羅、祭、靜音
  - 《[火影忍者疾風傳劇場版
    失落之塔](../Page/火影忍者疾風傳劇場版_失落之塔.md "wikilink")》：旗木卡卡西（少年期）、祭、靜音
  - 《[火影忍者劇場版 忍者之路](../Page/火影忍者劇場版_忍者之路.md "wikilink")》：祭、靜音、春野芽吹
  - 《[火影忍者劇場版：最終章](../Page/火影忍者劇場版：最終章.md "wikilink")》：祭、我愛羅
  - 《[火影忍者劇場版：慕留人](../Page/火影忍者劇場版：慕留人.md "wikilink")》：祭、我愛羅
  - 《[塔麻可吉 DOKIDOKI\!星球大暴走\!?](../Page/塔麻可吉.md "wikilink")》：**蒲公英**、花花吉
  - 《[銀魂劇場版
    新譯紅櫻篇](../Page/銀魂劇場版_新譯紅櫻篇.md "wikilink")》：**定春**、猿飛菖蒲、柳生九兵衛、來島又子
  - 《[銀魂完結篇
    永遠的萬事屋](../Page/銀魂劇場版_完結篇_永遠的萬事屋.md "wikilink")》：**定春**、猿飛菖蒲、柳生九兵衛、月詠、蛋蛋(小玉)小姐
  - 《[烏龍派出所特別篇 UFO大逆襲龍捲風大作戰](../Page/烏龍派出所.md "wikilink")》：美娜

### 歐美動畫電影

  - 《[一千零二夜](../Page/一千零二夜.md "wikilink")》：精靈仙子
  - 《[芭比與魔幻飛馬](../Page/芭比與魔幻飛馬.md "wikilink")》：貝瑞卡
  - 《[嘰哩咕歷險記](../Page/嘰哩咕歷險記.md "wikilink")》：**嘰哩咕**※[公視版本](../Page/公共電視台.md "wikilink")
  - 《[嘰哩咕與魔女](../Page/嘰哩咕與魔女.md "wikilink")》：**嘰哩咕**※[公視版本](../Page/公共電視台.md "wikilink")
  - 《[蚊子單車冒險](../Page/蚊子單車冒險.md "wikilink")》：甲蟲米米、毛毛蟲、凱特、小愛皇后、波麗公主、朵米拉皇后

### 海外電影

  - 《[鬼來電完結篇](../Page/鬼來電.md "wikilink")》：**惠美理**
  - 《[寶貝阿丹](../Page/寶貝阿丹.md "wikilink")》：湯利太太、女警
  - 《[曼谷保鏢](../Page/曼谷保鏢.md "wikilink")》：克姆洛之妻
  - 《[賊婆百分百](../Page/賊婆百分百.md "wikilink")》：**朱英珠**、在恩
  - 《[鬼嚇八](../Page/鬼嚇八.md "wikilink")》：塚本美保、瀧澤由佳里之母、綾乃
  - 《[85度瘋狂之愛](../Page/85度瘋狂之愛.md "wikilink")》：**金語貞／尹貞**
  - 《[東京大惡鬥](../Page/東京大惡鬥.md "wikilink")》：奈緒、美南
  - 《[勁辣紅巨人](../Page/勁辣紅巨人.md "wikilink")》：杜奇雅
  - 《[戀愛情結](../Page/戀愛情結.md "wikilink")》：**小泉理沙**
  - 《[仁太坊輕津三味線](../Page/仁太坊輕津三味線.md "wikilink")》：阿松嬸、留吉（幼年）

### 海外遊戲

  - 《[劍靈](../Page/劍靈.md "wikilink")（Blade & Soul）》：**坎魔燈**

## 歌曲演唱

  - 小貓巴克里
      -
        電視動畫《[小貓巴克里](../Page/小貓巴克里.md "wikilink")》主題曲
  - 綠色璇律
      -
        電視動畫《[小貓巴克里](../Page/小貓巴克里.md "wikilink")》「科普小教室」片尾曲

## 參考資料

[G](../Category/台灣女性配音員.md "wikilink") [X](../Category/郭姓.md "wikilink")

1.  [《觀測站少年》幕後花絮<首部曲> –
    聲音演員選角與表演](http://www.studio2talks.com/archives/6757)