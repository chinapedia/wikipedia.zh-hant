**古罗马年表**按照[时间先后顺序](../Page/时间.md "wikilink")，列举[古罗马重要历史事件](../Page/古罗马.md "wikilink")。时间跨度从古罗马建立至[拜占庭帝国最后试图收复罗马](../Page/拜占庭帝国.md "wikilink")。

## 前8世纪至前6世纪

  - [前753年](../Page/前753年.md "wikilink") －
    相传，[罗穆卢斯和瑞摩斯在这一年建立](../Page/罗穆卢斯和瑞摩斯.md "wikilink")[罗马市](../Page/罗马市.md "wikilink")，标志**[罗马王政时代](../Page/罗马王政时代.md "wikilink")**的开始（[前509年结束](../Page/前509年.md "wikilink")），[罗穆卢斯成为第一任国王](../Page/罗穆卢斯.md "wikilink")。
  - 前753年/[前715年](../Page/前715年.md "wikilink") － 国王罗穆卢斯在位。
  - 前715年/[前673年](../Page/前673年.md "wikilink") －
    国王[努玛·庞皮留斯](../Page/努玛·庞皮留斯.md "wikilink")（[努马·庞皮里乌斯](../Page/努马·庞皮里乌斯.md "wikilink")）在位，创建了[元老院和](../Page/元老院.md "wikilink")[祭司团](../Page/祭司团.md "wikilink")。
  - [前672年](../Page/前672年.md "wikilink")/[前641年](../Page/前641年.md "wikilink")
    －
    国王[托里斯·奥斯蒂吕斯](../Page/托里斯·奥斯蒂吕斯.md "wikilink")（[圖路斯·荷提里烏斯](../Page/圖路斯·荷提里烏斯.md "wikilink")）在位。
  - 前641年/[前616年](../Page/前616年.md "wikilink") －
    国王[安库斯·玛尔提乌斯](../Page/安库斯·玛尔提乌斯.md "wikilink")（[安古斯·馬奇路斯](../Page/安古斯·馬奇路斯.md "wikilink")）在位。
  - 前616年/[前575年](../Page/前575年.md "wikilink") －
    国王[卢修斯·塔克文·布里斯库](../Page/卢修斯·塔克文·布里斯库.md "wikilink")（Lucius
    Tarquinius Priscus）在位。
  - 前575年/[前535年](../Page/前535年.md "wikilink") －
    国王[塞尔维乌斯·图利乌斯](../Page/塞尔维乌斯·图利乌斯.md "wikilink")（[塞爾維烏斯·圖利烏斯](../Page/塞爾維烏斯·圖利烏斯.md "wikilink")）在位。
  - 前535年/[前509年](../Page/前509年.md "wikilink") －
    国王[卢修斯·塔克文·苏佩布](../Page/卢修斯·塔克文·苏佩布.md "wikilink")（[卢基乌斯·塔奎尼乌斯·苏培布斯](../Page/卢基乌斯·塔奎尼乌斯·苏培布斯.md "wikilink")）在位。
  - [前509年](../Page/前509年.md "wikilink") －
    国王[卢修斯·塔克文·苏佩布被驱逐](../Page/卢修斯·塔克文·苏佩布.md "wikilink")，**[罗马共和国](../Page/罗马共和国.md "wikilink")**建立（[前27年结束](../Page/前27年.md "wikilink")）。

## 前3世纪

  - [前283年](../Page/前283年.md "wikilink") －
    击败[伊特鲁里亚人和柏依人于](../Page/伊特鲁里亚人.md "wikilink")[瓦迪莫湖之战](../Page/瓦迪莫湖之战.md "wikilink")
  - [前280年](../Page/前280年.md "wikilink")/[前275年](../Page/前275年.md "wikilink")
    －
    与[伊庇鲁斯国王](../Page/伊庇鲁斯.md "wikilink")[皮洛士的战争](../Page/皮洛士.md "wikilink")。
  - [前267年](../Page/前267年.md "wikilink") －
    [财务官的人数从](../Page/财务官.md "wikilink")4升至6人。
  - [前264年](../Page/前264年.md "wikilink")/[前241年](../Page/前241年.md "wikilink")
    －
    [第一次布匿战争对抗](../Page/第一次布匿战争.md "wikilink")[迦太基](../Page/迦太基.md "wikilink")，战事主要在[西西里岛及附近海域进行](../Page/西西里岛.md "wikilink")。
  - [前242年](../Page/前242年.md "wikilink") －
    创立[裁判官](../Page/裁判官.md "wikilink")（praetor）。
  - [前241年](../Page/前241年.md "wikilink") －
    罗马和迦太基签定和约。西西里为罗马所有。并且迦太基割让“西西里与意大利之间的岛屿”，不包括[撒丁岛和](../Page/撒丁岛.md "wikilink")[科西嘉岛](../Page/科西嘉岛.md "wikilink")。迦太基赔偿罗马3200他连特。
  - [前238年](../Page/前238年.md "wikilink") －
    [佣兵战争中的迦太基形势危急](../Page/佣兵战争.md "wikilink")。罗马接受迦太基的撒丁岛守军于去年提出的投降要求，进占撒丁岛。
  - [前237年](../Page/前237年.md "wikilink") －
    迦太基胜利结束佣兵战争。要求罗马归还撒丁岛。罗马以武力威胁。精疲力尽的迦太基被迫屈服。罗马正式吞并撒丁岛和科西嘉岛。
  - [前227年](../Page/前227年.md "wikilink") －
    财务官的人数从6升至8人；大法官人数从2升至4人。初次设置行省：西西里行省和撒丁行省（包括科西嘉岛）。
  - [前224年](../Page/前224年.md "wikilink") －
    击败入侵的[高卢人](../Page/高卢人.md "wikilink")
  - [前223年](../Page/前223年.md "wikilink") －
    击败[山南高卢人](../Page/山南高卢人.md "wikilink")（Cisalpine
    Gaul）
  - [前218年](../Page/前218年.md "wikilink")/[前201年](../Page/前201年.md "wikilink")
    －
    [第二次布匿战争对抗](../Page/第二次布匿战争.md "wikilink")[迦太基](../Page/迦太基.md "wikilink")
  - [前216年](../Page/前216年.md "wikilink") －
    [汉尼拔在](../Page/汉尼拔.md "wikilink")[坎尼战役中给罗马造成了灾难](../Page/坎尼战役.md "wikilink")
  - [前214年](../Page/前214年.md "wikilink")/[前205年](../Page/前205年.md "wikilink")
    －
    [第一次马其顿战争](../Page/第一次马其顿战争.md "wikilink")。战事在希腊半岛进行，罗马主要以其希腊盟友与马其顿作战。前205年的和约规定恢复战前状态。
  - [前213年](../Page/前213年.md "wikilink")/[前211年](../Page/前211年.md "wikilink")
    － 包围并进占[叙拉古](../Page/叙拉古.md "wikilink")
  - [前204年](../Page/前204年.md "wikilink")/[前202年](../Page/前202年.md "wikilink")
    － [大西庇阿](../Page/大西庇阿.md "wikilink")（Scipio
    Africanus，意为阿非利加的征服者）侵入阿非利加；汉尼拔被召回，于公元前202年[扎马战役败于西庇阿之手](../Page/扎马战役.md "wikilink")
  - [前202年](../Page/前202年.md "wikilink")/[前196年](../Page/前196年.md "wikilink")
    － [第二次马其顿战争](../Page/第二次马其顿战争.md "wikilink")，罗马胜利

## 前2世纪

  - [前197年](../Page/前197年.md "wikilink") －
      - 远、近西班牙成为罗马行省
      - 财务官的人数从8升至12人；大法官人数从4升至6人
  - [前192年](../Page/前192年.md "wikilink")--[前189年](../Page/前189年.md "wikilink")
    － 对[塞琉西王朝的叙利亚战争](../Page/塞琉西王朝.md "wikilink")
  - [前172年](../Page/前172年.md "wikilink")/[前167年](../Page/前167年.md "wikilink")
    － [第三次马其顿战争](../Page/第三次马其顿战争.md "wikilink"), 罗马得胜
  - [前154年](../Page/前154年.md "wikilink")/[前138年](../Page/前138年.md "wikilink")
    － 对吕西坦尼亚的战争
  - [前149年](../Page/前149年.md "wikilink")/[前146年](../Page/前146年.md "wikilink")
    －
    [第三次布匿战争对抗](../Page/第三次布匿战争.md "wikilink")[迦太基](../Page/迦太基.md "wikilink")
  - [前149年](../Page/前149年.md "wikilink")/[前148年](../Page/前148年.md "wikilink")
    － [第四次马其顿战争](../Page/第四次马其顿战争.md "wikilink")
  - [前146年](../Page/前146年.md "wikilink") －
    [西庇阿·伊密连那斯](../Page/西庇阿·伊密连那斯.md "wikilink")（Scipio
    Aemilianus）以[迦太基与](../Page/迦太基.md "wikilink")[科林斯城的毁灭结束了](../Page/科林斯.md "wikilink")[布匿战争和](../Page/布匿战争.md "wikilink")[马其顿战争](../Page/马其顿战争.md "wikilink")；
    [马其顿和](../Page/马其顿.md "wikilink")[阿非利加行省的建立](../Page/阿非利加.md "wikilink")
  - [前133年](../Page/前133年.md "wikilink") －
    保民官[提比略·革拉古因颁布土地法案被杀害](../Page/提比略·革拉古.md "wikilink")
  - [前121年](../Page/前121年.md "wikilink") －
      - 罗马征服并建立[山外高卢](../Page/山外高卢.md "wikilink")（Transalpine
        Gaul）行省，以确保到西班牙通路的安全
      - 元老院颁布首部[Senatus consultum de re publica
        defenda以平息由保民官](../Page/List_of_Roman_laws.md "wikilink")[盖约·革拉古引起的暴动威胁](../Page/盖约·革拉古.md "wikilink")
  - [前112年](../Page/前112年.md "wikilink")/[前106年](../Page/前106年.md "wikilink")
    －
    对[努米底亚国王](../Page/努米底亚.md "wikilink")[朱古达](../Page/朱古达.md "wikilink")（Jughurta）的战争，以[马略](../Page/马略.md "wikilink")（Marius）的胜利告终
  - [前105年](../Page/前105年.md "wikilink") －
    [西姆布赖](../Page/西姆布赖.md "wikilink")（Cimbri）人入侵，重创罗马军团于[battle
    of Arausio](../Page/battle_of_Arausio.md "wikilink")
  - [前104年](../Page/前104年.md "wikilink")/[前100年](../Page/前100年.md "wikilink")
    - [盖乌斯·马略](../Page/盖乌斯·马略.md "wikilink")（Gaius Marius） 被选连任五年的执政官
  - [前102年](../Page/前102年.md "wikilink") -
    马略击败[条顿人](../Page/条顿.md "wikilink")（Teutons）于[Battle
    of Aquae Sextae](../Page/Battle_of_Aquae_Sextae.md "wikilink")
  - [前101年](../Page/前101年.md "wikilink") -
    马略和[昆塔斯·琉喜阿斯·卡塔拉斯](../Page/昆塔斯·琉喜阿斯·卡塔拉斯.md "wikilink")（Quintus
    Lutatius Catulus）击败西姆布赖人于the [Battle of
    Vercellae](../Page/Battle_of_Vercellae.md "wikilink")

## 前1世纪

  - [前91年](../Page/前91年.md "wikilink")/[前88年](../Page/前88年.md "wikilink")
    － [同盟战争](../Page/同盟者戰爭_\(前91到88年\).md "wikilink")（Social
    wars），末次意大利同盟国对罗马的叛乱
  - [前88年](../Page/前88年.md "wikilink")
    [苏拉统军攻入罗马](../Page/苏拉.md "wikilink")
  - [前88年](../Page/前88年.md "wikilink")/[前85年](../Page/前85年.md "wikilink")
    －
    对[本都国王](../Page/本都.md "wikilink")[米特里達梯六世的](../Page/米特里達梯六世.md "wikilink")[第一次米特里達梯战争](../Page/第一次米特里達梯战争.md "wikilink")
  - [前83年](../Page/前83年.md "wikilink")/[前82年](../Page/前82年.md "wikilink")
    －
    苏拉与罗马民主党间的第一次内战；苏拉获胜，实行独裁统治；废黜独裁官一职（[前70年恢复](../Page/前70年.md "wikilink")）
  - [前83年](../Page/前83年.md "wikilink")/[前82年](../Page/前82年.md "wikilink")
    －
    [第二次米特里達梯战争](../Page/第二次米特里達梯战争.md "wikilink")；苏拉回到罗马，任[独裁官](../Page/独裁官.md "wikilink")
  - [前82年](../Page/前82年.md "wikilink")/[前72年](../Page/前72年.md "wikilink")
    － [塞多留](../Page/塞多留.md "wikilink")（Sertorius），最后的马略党人，在西班牙延续内战
  - [前74年](../Page/前74年.md "wikilink")/[前66年](../Page/前66年.md "wikilink")
    －
    [第三次米特里達梯战争以](../Page/第三次米特里達梯战争.md "wikilink")[庞培](../Page/格涅乌斯·庞贝.md "wikilink")（Pompey）最终获胜
  - [前73年](../Page/前73年.md "wikilink")/[前71年](../Page/前71年.md "wikilink")
    －
    [斯巴达克斯起义](../Page/斯巴达克斯起义.md "wikilink")，最终被[克拉苏镇压](../Page/克拉苏.md "wikilink")
  - [前67年](../Page/前67年.md "wikilink") － 庞培清剿地中海海盗
  - [前63年](../Page/前63年.md "wikilink") －
      - 耶路撒冷陷落
      - [西塞罗任执政官](../Page/西塞罗.md "wikilink")；粉碎平民派领袖[喀提林企图夺权的](../Page/喀提林.md "wikilink")[喀提林的阴谋](../Page/喀提林的阴谋.md "wikilink")
  - [前59年](../Page/前59年.md "wikilink")/[前54年](../Page/前54年.md "wikilink")
    －
    [前三头政治](../Page/前三头.md "wikilink")；恺撒、庞培和[克拉苏缔结秘密同盟](../Page/克拉苏.md "wikilink")
  - [前58年](../Page/前58年.md "wikilink")/[前50年](../Page/前50年.md "wikilink")
    － 恺撒发动[高卢战争攻取外高卢](../Page/高卢战争.md "wikilink")
  - [前54年](../Page/前54年.md "wikilink")/[前53年](../Page/前53年.md "wikilink")
    － 首次对[帕提亚帝国作战](../Page/帕提亚帝国.md "wikilink")，克拉苏败死
  - [前44年](../Page/前44年.md "wikilink") －
    [恺撒遇刺身亡](../Page/恺撒.md "wikilink")

## 1世纪

  - [6年](../Page/6年.md "wikilink") －
    [犹大变成罗马的一个行省](../Page/犹大.md "wikilink")。
  - [37年](../Page/37年.md "wikilink")
    －**[卡-{里}-古拉](../Page/卡里古拉.md "wikilink")**成为皇帝
  - [41年](../Page/41年.md "wikilink")
    －**[克劳狄](../Page/克劳狄.md "wikilink")**成为皇帝
  - [54年](../Page/54年.md "wikilink")
    －**[尼禄](../Page/尼禄.md "wikilink")**成为皇帝
  - [79年](../Page/79年.md "wikilink") －
      - **[提圖斯](../Page/提圖斯.md "wikilink")**成为皇帝
      - [8月24日](../Page/8月24日.md "wikilink")，[维苏威火山爆发毁灭了](../Page/维苏威火山.md "wikilink")[庞贝城和](../Page/庞贝城.md "wikilink")[赫库兰尼姆](../Page/赫库兰尼姆.md "wikilink")
  - [81年](../Page/81年.md "wikilink") －
    **[多米田](../Page/多米田王.md "wikilink")**（又译作[图密善](../Page/图密善.md "wikilink")）成为皇帝
  - [96年](../Page/96年.md "wikilink") －
    [多米田王被元老院派人刺殺](../Page/多米田王.md "wikilink")，[夫拉維王朝結束](../Page/夫拉維王朝.md "wikilink")。元老院任命[涅尔瓦继任](../Page/涅尔瓦.md "wikilink")，开始了[五賢帝時代](../Page/五賢帝時代.md "wikilink")。
  - [98年](../Page/98年.md "wikilink") －
    **[图拉真](../Page/图拉真.md "wikilink")**成为皇帝

## 2世纪

  - [117年](../Page/117年.md "wikilink") －
    **[哈德良](../Page/哈德良.md "wikilink")**成为皇帝
  - [122年](../Page/122年.md "wikilink") －
    [哈德良长城开始建造](../Page/哈德良长城.md "wikilink")
  - [138年](../Page/138年.md "wikilink") －
    **[安敦宁·毕尤](../Page/安敦宁·毕尤.md "wikilink")**成为皇帝
  - [140年](../Page/140年.md "wikilink")/[143年](../Page/143年.md "wikilink")
    －
    安敦宁·毕尤攻占[苏格兰](../Page/苏格兰.md "wikilink")；[安敦宁长城开始建造](../Page/安多宁长城.md "wikilink")
  - [161年](../Page/161年.md "wikilink") －
    **[马尔库斯·奥列里乌斯](../Page/马尔库斯·奥列里乌斯.md "wikilink")**成为皇帝
  - [180年](../Page/180年.md "wikilink") －
    **[康茂德](../Page/康茂德.md "wikilink")**成为皇帝

## 3世纪

  - [211年](../Page/211年.md "wikilink") －
    **[卡拉卡拉](../Page/卡拉卡拉.md "wikilink")**成为皇帝
  - [217年](../Page/217年.md "wikilink") － 卡拉卡拉被殺;
    **[馬克里努斯](../Page/馬克里努斯.md "wikilink")**成为皇帝
  - [222年](../Page/222年.md "wikilink") －
    **[亞歷山大·塞维鲁](../Page/亞歷山大·塞维鲁.md "wikilink")**成为皇帝

## 4世纪

  - [330年](../Page/330年.md "wikilink") －
    **[君士坦丁一世](../Page/君士坦丁一世.md "wikilink")**以[君士坦丁堡为首都](../Page/君士坦丁堡.md "wikilink")

## 參見

  - [古羅馬](../Page/古羅馬.md "wikilink")

  - [羅馬歷史](../Page/羅馬歷史.md "wikilink")

  -
[Category:年表](../Category/年表.md "wikilink")
[Category:古罗马](../Category/古罗马.md "wikilink")