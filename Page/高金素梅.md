**高金素梅**（，[音譯](../Page/音譯.md "wikilink")：**吉娃斯·阿麗**；），本名**金素梅**，出生于[台湾](../Page/台湾.md "wikilink")[台中市](../Page/台中市.md "wikilink")；現任[立法委員](../Page/立法委員.md "wikilink")，曾為演員、歌手。

## 經歷

  - 1984年，[華視節目](../Page/中華電視公司.md "wikilink")《[大精彩](../Page/大精彩.md "wikilink")》出道，步入歌壇。
  - 1998年，檢出罹患[肝癌](../Page/肝癌.md "wikilink")，退出影藝圈。
  - 1999年，切除肝癌腫瘤，擔任癌症病患關懷大使。
  - 2000年，第5屆中國[長春電影節最佳女主角獎提名](../Page/長春電影節.md "wikilink")《[囍宴](../Page/囍宴_\(電影\).md "wikilink")》。
  - 2001年，轉戰政壇，以無黨籍身份當選山地原住民選區[立法委員](../Page/立法委員.md "wikilink")。
  - 2002年，加入[無黨籍立法院黨團運作](../Page/無黨籍.md "wikilink")。
  - 2004年，以第二高票連任立委，之後在[無黨團結聯盟成立後加入該黨黨團](../Page/無黨團結聯盟.md "wikilink")。
  - 2005年，獲得[鳳凰衛視網友評選風範大國民](../Page/鳳凰衛視.md "wikilink")2005年度十大人物「為民族討靈的高金素梅」。
  - 2005年，考取[北京](../Page/北京.md "wikilink")[中央民族大學民族學系](../Page/中央民族大學.md "wikilink")。
  - 2008年，山地原住民立委席次縮減，首度以無盟籍參選，以選區第三高票連任立委。
  - 2008年，赴[中國大陸參加](../Page/中國大陸.md "wikilink")[北京奧運開幕表演](../Page/北京奧運.md "wikilink")。
  - 2009年，在北京獲中共中央總書記[胡錦濤接見](../Page/胡錦濤.md "wikilink")。
  - 2011年，從[中央民族大學民族學系畢業](../Page/中央民族大學.md "wikilink")，取得[學士](../Page/學士.md "wikilink")[學位](../Page/學位.md "wikilink")。
  - 2012年，再度以第二高票當選連任，運作近十年的無盟黨團因主席[林炳坤連任失利遭到註銷](../Page/林炳坤.md "wikilink")，未加入任何黨團。
  - 2015年2月24日，加入立院新聯盟政團運作\[1\]，成員包含[親民黨](../Page/親民黨.md "wikilink")、[民國黨立法委員](../Page/民國黨.md "wikilink")\[2\]。
  - 2015年8月31日，以「反靖國神社代表」身分參加[北京政府舉行的](../Page/中華人民共和國政府.md "wikilink")[抗戰紀念活動](../Page/九三閱兵.md "wikilink")\[3\]。
  - 2016年1月16日，首度以選區第一高票連任立委，隨後加入親民黨立院黨團運作\[4\]。
  - 2016年2月1日，參與[立法院副院長選舉](../Page/立法院副院長.md "wikilink")，僅獲4票，未能當選。
  - 2018年5月4日，[司法院](../Page/司法院.md "wikilink")[大法官會議決議](../Page/大法官會議.md "wikilink")，以該案聲請人中，有一位立委於二、三讀均未參與表決，或在三讀表決時，聲請人中只有二十九人表示反對，因此未達「立委總額三分之一」於「行使職權」發生疑義之要件，因此不受理由國、親、無盟共三十八位立委提出的[前瞻條例與預算的釋憲聲請案](../Page/前瞻條例.md "wikilink")；\[5\]。

## 政治選舉經歷

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>年度</strong></p></td>
<td style="text-align: center;"><p><strong>選舉屆數</strong></p></td>
<td style="text-align: center;"><p><strong>所屬政黨</strong></p></td>
<td><p>選區</p></td>
<td><p><strong>得票率</strong></p></td>
<td><p><strong>名次</strong></p></td>
<td><p><strong>當選標記</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2001年</p></td>
<td style="text-align: center;"><p><a href="../Page/2001年中華民國立法委員選舉.md" title="wikilink">第五屆立法委員選舉</a></p></td>
<td style="text-align: center;"><p>→<br />
</p></td>
<td><p>山地原住民選區</p></td>
<td><p>10.42%</p></td>
<td><p>3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2004年</p></td>
<td style="text-align: center;"><p><a href="../Page/2004年中華民國立法委員選舉.md" title="wikilink">第六屆立法委員選舉</a></p></td>
<td style="text-align: center;"></td>
<td><p>山地原住民選區</p></td>
<td><p>19.96%</p></td>
<td><p>2</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2008年</p></td>
<td style="text-align: center;"><p><a href="../Page/2008年中華民國立法委員選舉.md" title="wikilink">第七屆立法委員選舉</a></p></td>
<td style="text-align: center;"></td>
<td><p>山地原住民選區</p></td>
<td><p>23.72%</p></td>
<td><p>3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2012年</p></td>
<td style="text-align: center;"><p><a href="../Page/2012年中華民國立法委員選舉.md" title="wikilink">第八屆立法委員選舉</a></p></td>
<td style="text-align: center;"></td>
<td><p>山地原住民選區</p></td>
<td><p>25.29%</p></td>
<td><p>2</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
<td><p>2015年起加入「立院新聯盟」政團運作</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2016年</p></td>
<td style="text-align: center;"><p><a href="../Page/2016年中華民國立法委員選舉.md" title="wikilink">第九屆立法委員選舉</a></p></td>
<td style="text-align: center;"></td>
<td><p>山地原住民選區</p></td>
<td><p>24.51%</p></td>
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
<td><p>加入黨團運作</p></td>
</tr>
</tbody>
</table>

## 相關事件

她積極主張要求[日本政府將台灣](../Page/日本政府.md "wikilink")「[高砂族](../Page/高砂族.md "wikilink")」（[原住民](../Page/台灣原住民.md "wikilink")）當年被徵人員[高砂義勇隊陣亡祖靈遷出](../Page/高砂義勇隊.md "wikilink")[靖國神社](../Page/靖國神社.md "wikilink")。

多次為此到[東京](../Page/東京.md "wikilink")[靖國神社與](../Page/靖國神社.md "wikilink")[紐約](../Page/紐約.md "wikilink")[聯合國總部抗議](../Page/聯合國總部.md "wikilink")，並向日本法院提出控告[小泉首相參拜違憲](../Page/小泉純一郎.md "wikilink")，並要求日本政府與靖國神社將台灣原住民從中除名。

  - 2005年9月30日，[大阪高等法院二審宣判原告敗訴但認定小泉首相違反日本憲法](../Page/大阪高等法院.md "wikilink")20條\[6\]。
  - 2009年8月11日，率領台灣原住民「還我祖靈隊」赴日本[靖國神社](../Page/靖國神社.md "wikilink")。
  - 2009年8月20日，[中共中央總書記](../Page/中共中央總書記.md "wikilink")[胡錦濤接見高金素梅](../Page/胡錦濤.md "wikilink")，稱贊其為臺灣原住民進行的鬥爭。\[7\]胡錦濤會見時說：「台灣少數民族同胞是中華民族大家庭的重要成員，長期以來，為抵禦外來侵略，捍衛民族尊嚴，進行了不屈不撓的鬥爭。」\[8\]
  - 2016年6月14日，質詢行政院長[林全時](../Page/林全.md "wikilink")，提及「[大豹社事件](../Page/大豹社事件.md "wikilink")」歷史，要求政府歸還[原住民被奪的土地](../Page/原住民.md "wikilink")、興建「大豹社事件紀念碑」，[轉型正義也應納入原住民](../Page/轉型正義.md "wikilink")，這段質詢影片在網路上爆紅，不到一周就有超過80萬人瀏覽\[9\]\[10\]\[11\]。
  - 2016年8月1日，帶領[泰雅族人及學者](../Page/泰雅族.md "wikilink")，重返[大豹社事件歷史現場告慰祖靈](../Page/大豹社事件.md "wikilink")，表示未來將在大豹[日軍忠魂碑附近](../Page/日軍.md "wikilink")，建立大豹社紀念碑，讓後代更加了解歷史真相，並宣告建碑行動正式開始\[12\]\[13\]。
  - 2017年9月15日，台大原本同意要租場地給「中國新歌聲－上海台北音樂節」主辦單位，後來傳出台大取消出借，因為已經將活動的票發送給族人，因此助理向國會聯絡人瞭解狀況，若沒有如期舉行，要提早告訴族人不要北上，以免白跑一趟，被質疑施壓台大\[14\]\[15\]\[16\]。
  - 2017年9月29日，施政總質詢時，給[行政院長](../Page/行政院長.md "wikilink")[賴清德上一堂原住民文化課](../Page/賴清德.md "wikilink")，大談原住民在文化傳承中的努力，並批評[教育部將文言文棄之敝屣](../Page/教育部.md "wikilink")\[17\]\[18\]，最後則呼籲「要和平相處」，並送給[賴清德一套原住民族傳統歌謠與書籍](../Page/賴清德.md "wikilink")\[19\]。[立法院長](../Page/立法院長.md "wikilink")[蘇嘉全於質詢結束後](../Page/蘇嘉全.md "wikilink")，則對高金素梅說:「不能厚此薄彼，他也要一份」\[20\]。

## 爭議

1.  2009年[莫拉克風災期間](../Page/莫拉克風災.md "wikilink")，高金素梅以「台灣少數民族代表團」團長名義至大陸訪問，還會了中共中央總書記[胡錦濤](../Page/胡錦濤.md "wikilink")，高金素梅說道：「胡錦濤總書記在第一時間給了我們這麼大的溫暖的擁抱，我們要把這溫暖帶回去給我們的族人」。\[21\]事後拿新臺幣一億元捐款回臺，不分藍綠，遭各界強烈質疑：[民進黨立委](../Page/民進黨.md "wikilink")[潘孟安](../Page/潘孟安.md "wikilink")，認為高金素梅不在台灣救災，卻跑到北京將原住民「賣身換錢」，形同中國[統戰馬前卒](../Page/統戰.md "wikilink")，而[陳瑩則認為此鉅額款項沒有完善監督機制](../Page/陳瑩.md "wikilink")，質疑是不是真的花在災民身上？還是到了選舉才花？\[22\]
    ；
    [國民黨籍原住民立委](../Page/國民黨.md "wikilink")[孔文吉即公開質疑高金素梅拿這些錢到原住民部落發放](../Page/孔文吉.md "wikilink")，是為選舉綁樁，高金素梅則表示，如何發放是由自救委員會審核，並強調所有具領人在網路上都有公布，但經記者上高金的「祖靈之邦」網站查證，只有發放時間、地點、村莊名與戶數、金額等，並沒有高金所說的具領人名單\[23\]
    。台灣公益團體自律聯盟秘書長[張宏林則指出](../Page/張宏林.md "wikilink")，高金素梅此舉處於模糊地帶\[24\]。
2.  2015年高金素梅連續六次被列為立委問政待觀察名單，被點名是吊車尾名單第一名\[25\]\[26\]
3.  2017年9月24日，[台大濺血事件立委高金素梅遭指](../Page/2017年中國新歌聲上海·臺北音樂節抗議事件.md "wikilink")「關切」台大出借場地給中國選秀節目「中國新歌聲」，台大經協調後同意出借場地。\[27\]\[28\]\[29\]



## 感情生活

在高金素梅的人生經歷中，還有著不少的感情故事，其中堪稱經典的是與[何家勁之間的一段情](../Page/何家勁.md "wikilink")。高金素梅拍攝的第一部電視劇《[不了情](../Page/不了情_\(1989年電視劇\).md "wikilink")》，就是與何家勁合作的。

2009年，[壹週刊報導](../Page/壹週刊_\(台灣\).md "wikilink")，高金素梅與[親民黨籍前](../Page/親民黨.md "wikilink")[臺北縣副縣長](../Page/新北市.md "wikilink")[李鴻源傳出戀情](../Page/李鴻源.md "wikilink")，後來兩人高調交往，李為此請辭副縣長。

## 影視作品

### 電影

| 年份    | 片名                                                                           | 角色  |
| ----- | ---------------------------------------------------------------------------- | --- |
| 1988年 | 《[江湖接班人](../Page/江湖接班人.md "wikilink")》                                       | 楊麗玲 |
| 1991年 | 《[先生騙鬼](../Page/先生騙鬼.md "wikilink")》                                         |     |
| 1993年 | 《[將邪神劍](../Page/將邪神劍.md "wikilink")》                                         |     |
| 1993年 | 《[囍宴](../Page/囍宴_\(電影\).md "wikilink")》（[李安導演所執導](../Page/李安.md "wikilink")） | 顧威威 |
| 1994年 | 《[梅珍](../Page/梅珍_\(電影\).md "wikilink")》                                      |     |
| 1999年 | 《[女湯](../Page/女湯.md "wikilink")》                                             |     |

### 電視剧

| 年份       | 頻道                             | 劇名                                               | 角色  |
| -------- | ------------------------------ | ------------------------------------------------ | --- |
| 1986年11月 | [中視](../Page/中視.md "wikilink") | 《挑伕》                                             |     |
| 1989年4月  | [華視](../Page/華視.md "wikilink") | 《[不了情](../Page/不了情_\(1989年電視劇\).md "wikilink")》  |     |
| 1990年    | [華視](../Page/華視.md "wikilink") | 《[六個夢](../Page/六個夢.md "wikilink")》（《婉君》）         |     |
| 1990年    | [華視](../Page/華視.md "wikilink") | 《[六個夢](../Page/六個夢.md "wikilink")》（《三朵花》）        |     |
| 1990年12月 | [華視](../Page/華視.md "wikilink") | 《[愛](../Page/愛_\(華視電視劇\).md "wikilink")》         |     |
| 1992年2月  | [華視](../Page/華視.md "wikilink") | 《[緣](../Page/緣_\(華視電視劇\).md "wikilink")》         |     |
| 1992年    | [華視](../Page/華視.md "wikilink") | 《[意難忘](../Page/意難忘\(1992年電視劇\).md "wikilink")》   | 趙文娟 |
| 1993年5月  | [華視](../Page/華視.md "wikilink") | 《秦俑》                                             |     |
| 1999年4月  | [華視](../Page/華視.md "wikilink") | 《[啞巴與新娘](../Page/啞巴與新娘_\(台灣電視劇\).md "wikilink")》 |     |
|          | |《丈夫不見了》                       |                                                  |     |

### 歌曲專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1982年</p></td>
<td style="text-align: left;"><p>山上的野薑花</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1984年</p></td>
<td style="text-align: left;"><p>想想我記得我</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>1988年</p></td>
<td style="text-align: left;"><p>背叛風情</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1990年</p></td>
<td style="text-align: left;"><p>卸粧</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>1992年</p></td>
<td style="text-align: left;"><p>多情痴心</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1994年</p></td>
<td style="text-align: left;"><p>潮</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2001年</p></td>
<td style="text-align: left;"><p>如初之心．丰采祝福精選</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>|鑽石佳人</p></td>
<td style="text-align: left;"><p>國語</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 相關書籍

| 年份    | 書名                      | 出版社 | ISBN            |
| ----- | ----------------------- | --- | --------------- |
| 2005年 | 《你願意聽我的聲音嗎：胡忠信、高金素梅對談錄》 | 智庫  | ISBN 9867264223 |

### 寫真集

| 年份    | 書名     | 出版社                                | 攝影師 |
| ----- | ------ | ---------------------------------- | --- |
| 1991年 | 金素梅寫真集 | [皇冠文化](../Page/皇冠文化.md "wikilink") | 陳福堂 |

## 参考文献

### 引用

### 来源

  - [高金素梅：我们带着悲伤而来
    带着希望而归](https://web.archive.org/web/20090827034625/http://news.xinhuanet.com/tw/2009-08/21/content_11923383.htm)

## 外部連結

  -
  -
  -
{{-}}

[Category:第9屆中華民國立法委員](../Category/第9屆中華民國立法委員.md "wikilink")
[Category:第8屆中華民國立法委員](../Category/第8屆中華民國立法委員.md "wikilink")
[Category:第7屆中華民國立法委員](../Category/第7屆中華民國立法委員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:臺灣原住民族民意代表](../Category/臺灣原住民族民意代表.md "wikilink")
[Category:臺灣女性政治人物](../Category/臺灣女性政治人物.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[Category:中央民族大学校友](../Category/中央民族大学校友.md "wikilink")
[Category:台灣原住民演員](../Category/台灣原住民演員.md "wikilink")
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:安徽裔台灣人](../Category/安徽裔台灣人.md "wikilink")
[Category:臺灣滿族](../Category/臺灣滿族.md "wikilink")
[Category:泰雅族人](../Category/泰雅族人.md "wikilink")
[Category:臺灣原住民混血兒](../Category/臺灣原住民混血兒.md "wikilink")
[Category:愛新覺羅氏](../Category/愛新覺羅氏.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  [胡錦濤接見高金素梅以表揚抗日活動](http://news.backchina.com/2009/8/20/gb2312_53513.html)

9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22. [高金拿中共捐款 立委痛批統戰馬前卒](http://news.ltn.com.tw/news/focus/paper/328956)
    自由時報 2009/08/21
23. [藍委質疑高金 拿中國賑災款綁樁](http://news.ltn.com.tw/news/focus/paper/342563)
    自由時報 2009/10/13
24. [高金素梅收近億捐款惹爭議　自律聯盟：處於模糊地帶](https://www.nownews.com/news/20090823/849430)
    NOW NEWS今日新聞 2009/8/23
25.  公民監督國會聯盟|accessdate=2018-12-17|work=www.ccw.org.tw}}
26.
    ETtoday新聞雲|accessdate=2018-12-17|last=[https://www.facebook.com/ETtoday|work=www.ettoday.net|language=zh-Hant](https://www.facebook.com/ETtoday%7Cwork=www.ettoday.net%7Clanguage=zh-Hant)}}
27.
28.
29.