**塔尔卡流浪者体育社会俱乐部**（**Club Social de Deportes Rangers de
Talca**）是[智利足球俱乐部](../Page/智利.md "wikilink")，位于[塔尔卡](../Page/塔尔卡.md "wikilink")。俱乐部成立于1902年11月2日，现为[智利足球乙级联赛球队](../Page/智利足球乙级联赛.md "wikilink")。俱乐部的主体育场为菲斯卡体育场（Estadio
Fiscal），有大约17,000个座位。

## 队史

球队的队名流浪者是由俱乐部的创始人之一的[英国人胡安](../Page/英国人.md "wikilink")·格林斯垂特（Juan
Greenstret）所起的，

最初球队队色为红黑竖条衫。球衣色的来源据一些第一批进入球队的球员回忆是来自塔尔卡一家渔业公司的盾形徽章。

## 球队荣誉

  - [智利足球甲级联赛](../Page/智利足球甲级联赛.md "wikilink") 冠军: 1988年, 1993年, 1997年

## 知名球员

  -  [文森特·坎塔托瑞](../Page/文森特·坎塔托瑞.md "wikilink")

  - [西尔维奥·费尔南德斯·多斯·桑托斯](../Page/西尔维奥·费尔南德斯·多斯·桑托斯.md "wikilink")

  - [罗本·戈麦斯](../Page/罗本·戈麦斯.md "wikilink")

  - [安赫里多·拉普纳](../Page/安赫里多·拉普纳.md "wikilink")

  - [奥马尔·西沃里·马叶亚](../Page/奥马尔·西沃里·马叶亚.md "wikilink")

## 外部链接

  - [Rangers Talca Website](http://www.rangers.cl)

[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")