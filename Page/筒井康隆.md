**筒井康隆**（，）是[日本的小說家](../Page/日本.md "wikilink")、[科幻作家和](../Page/科幻.md "wikilink")[演員](../Page/演員.md "wikilink")，隸屬於[堀製作株式會社](../Page/Horipro.md "wikilink")。筒井康隆出生於[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")，早期以科幻、[推理小說聞名於世](../Page/推理.md "wikilink")，作品多帶有[無厘頭的諷喻成分](../Page/無厘頭.md "wikilink")。筒井康隆曾經多次榮獲[星雲獎與](../Page/星雲獎_\(日本\).md "wikilink")[紫綬勳章](../Page/紫綬勳章.md "wikilink")，與[小松左京](../Page/小松左京.md "wikilink")、[星新一並稱](../Page/星新一.md "wikilink")「科幻小說御三家」。

## 作家

1934年，筒井康隆於[大阪府](../Page/大阪府.md "wikilink")[大阪市北堀江出生](../Page/大阪市.md "wikilink")，雙親是筒井嘉隆與八重。筒井康隆早期則宣稱自己出生於[船場](../Page/船場.md "wikilink")。1941年，筒井康隆進入南田邊国民学校就讀，小学時期喜愛[江戶川亂步作品](../Page/江戶川亂步.md "wikilink")。1944年、筒井康隆轉學至[吹田市千里山千里第二国民学校](../Page/吹田市.md "wikilink")。因為筒井康隆智力測驗成績優異，於是在[第二次世界大戰結束後於特別教室接受教育](../Page/第二次世界大戰.md "wikilink")\[1\]。1947年，筒井康隆進入大阪市立東第一中学校（現在為大阪市立東中学校）就讀。1948年，筒井康隆參與兒童劇團「子熊座」，開始對演戲產生興趣。1950年，筒井康隆進入大阪府立春日丘高等学校就讀，並擔任演劇部部長。筒井康隆此時非常喜愛[德國](../Page/德國.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[阿圖爾·叔本華的作品](../Page/阿圖爾·叔本華.md "wikilink")\[2\]。1952年4月，筒井康隆進入[同志社大学](../Page/同志社大学.md "wikilink")[文学部](../Page/文学.md "wikilink")[心理学科就讀](../Page/心理学.md "wikilink")\[3\]。1957年，筒井康隆於大学畢業。1959年出版科幻小說同人誌《NULL》，文章受[江戶川亂步注意而步入文學界](../Page/江戶川亂步.md "wikilink")\[4\]。

1962年，筒井康隆作品《往無機世界》（）於《科幻雜誌》獲選早川科幻小說獎，小松左京、半村良也一同獲獎。1963年，筒井康隆於《科幻雜誌》増刊號以「老虎狗」（）之名初登場。1964年、筒井康隆參加「日本科幻小說作家俱樂部」。1965年、筒井康隆與光子結婚，之後前往東京成為専業作家。同年10月，筒井康隆首部作品「東海道戰爭」出版。

1970年，筒井康隆獲得第1屆星雲賞。筒井康隆從1968年開始，三度落選[直木賞](../Page/直木賞.md "wikilink")（1967年《越南觀光公社》、1968年《非洲爆彈》、1972年《家族八景》）。1972年4月，筒井康隆從東京移居[神戶市垂水區](../Page/神戶市.md "wikilink")。1980年，筒井康隆擔任日本科幻小說作家俱樂部事務局長，創設日本SF大獎。

1990年代，筒井康隆出版《文学部唯野教授》、《清晨的加斯巴》（獲得1992年日本SF大賞）等作品，引起許多話題。筒井康隆在連載《塗口紅的殘像》、《文学部唯野教授》時因胃穿孔入院，並因此接觸到[德國](../Page/德國.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[馬丁·海德格爾的思想](../Page/馬丁·海德格爾.md "wikilink")，對於之後的作品產生許多影響。

1993年、[角川書店發行的高中國語教科書收錄筒井康隆作品](../Page/角川書店.md "wikilink")《無人警察》，其有關[癲癇描述受到日本癲癇協會抗議](../Page/癲癇.md "wikilink")。之後筒井康隆接到許多騷擾電話與信件\[5\]。角川書店之後擅自刪除《無人警察》、筒井康隆於是在月刊誌《流言的真相》發表不再寫作的宣言。1995年11月，筒井康隆於[新潮社復出](../Page/新潮社.md "wikilink")\[6\]，開始連載作品。1997年，筒井康隆獲得法蘭西藝術與文學勳章。2002年，筒井康隆獲得[紫綬褒章](../Page/紫綬褒章.md "wikilink")。2010年，筒井康隆獲得[菊池寬賞](../Page/菊池寬賞.md "wikilink")。2012年7月13日至2013年3月13日、筒井康隆於[朝日新聞連載小説](../Page/朝日新聞.md "wikilink")《聖痕》。\[7\]2013年、筒井康隆成為日本科幻小說作家俱樂部名誉会員\[8\]。

## 演員

1993年，筒井康隆宣布不再寫作後，因為收入減少，開始增加演出電視劇、[電影與](../Page/電影.md "wikilink")[廣告的頻率](../Page/廣告.md "wikilink")。1997年，筒井康隆與[堀製作株式會社簽下契約](../Page/Horipro.md "wikilink")。1999年，筒井康隆與[蜷川幸雄共同演出](../Page/蜷川幸雄.md "wikilink")[契訶夫的戲劇](../Page/契訶夫.md "wikilink")《[海鷗](../Page/海鷗.md "wikilink")》。2000年、2001年，筒井康隆與[蜷川幸雄](../Page/蜷川幸雄.md "wikilink")、[藤原龍也共同演出](../Page/藤原龍也.md "wikilink")[三島由紀夫的戲劇](../Page/三島由紀夫.md "wikilink")「弱法師」（「近代能樂集」）。
2005年朝日電視台將筒井康隆原作之「富豪刑事」，改編為電視劇，筒井康隆亦客串一角「瀨崎龍平」。

## 家族

筒井康隆父親為動物生態学者、[大阪市立自然史博物館初代館長筒井嘉隆](../Page/大阪市立自然史博物館.md "wikilink")，兒子為[畫家](../Page/畫家.md "wikilink")[筒井伸輔](../Page/筒井伸輔.md "wikilink")。

## 作品列表

### 小說

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>原文書名</p></th>
<th><p>中文譯名</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1965年</p></td>
<td></td>
<td><p>東海道戰爭</p></td>
<td><p>出道處女作，描寫東京與<a href="../Page/大阪.md" title="wikilink">大阪之間的</a><a href="../Page/戰爭.md" title="wikilink">戰爭</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>48億的妄想</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1966年</p></td>
<td></td>
<td><p><a href="../Page/穿越時空的少女.md" title="wikilink">-{zh-hans:穿越时空的少女;zh-hk:穿越時空的少女;zh-tw:穿越時空的少女;}-</a></p></td>
<td><p>曾改編為電影及電視劇。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>越南觀光公社</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1968年</p></td>
<td></td>
<td><p>非洲爆彈／非洲炸彈</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>熱鬧的未來</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1969年</p></td>
<td></td>
<td><p>筒井順慶</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>靈長類南進</p></td>
<td><p>描寫一場因誤會引起的核戰後，殘存人類的種種醜態，獲得第1回星雲賞（日本長編部門）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1971年</p></td>
<td></td>
<td><p>亡命者與追逐者的森巴舞</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1972年</p></td>
<td></td>
<td><p>家族八景</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>城市異人──搞怪十八人組／俗物圖鑑</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1973年</p></td>
<td></td>
<td><p>要瘋到什麼程度，還得看有多少錢</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/日本以外全部沉沒.md" title="wikilink">日本以外全部沉沒</a></p></td>
<td><p>獲得第5回星雲賞（日本短編部門）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1974年</p></td>
<td></td>
<td><p>關於我的謠言</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>黑血狂奔:耶斯克列曼度之謎</p></td>
<td><p>獲得第6回星雲賞（日本長編部門）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td></td>
<td><p>又见七濑</p></td>
<td><p>獲得第7回星雲賞（日本長編部門）</p></td>
</tr>
<tr class="odd">
<td><p>1976年</p></td>
<td></td>
<td><p>變身群島</p></td>
<td><p>獲得第8回星雲賞（日本短編部門）</p></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td></td>
<td><p>戀母情節的愛人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td></td>
<td><p>男人們的畫</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/富豪刑事.md" title="wikilink">富豪刑事</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td></td>
<td><p>助跑</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td></td>
<td><p>虛人</p></td>
<td><p>為一部超乎現實的形而上作品，獲得第9回泉鏡花文学賞</p></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td></td>
<td><p>虛航船團</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td></td>
<td><p>碳烤教授</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1986年</p></td>
<td></td>
<td><p><a href="../Page/拉哥斯.md" title="wikilink">拉哥斯之旅</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td></td>
<td><p>夢中的木坂分歧點</p></td>
<td><p>獲得第23回谷崎潤一郎賞</p></td>
</tr>
<tr class="odd">
<td><p>最後の喫煙者</p></td>
<td><p><a href="../Page/最後的吸菸者.md" title="wikilink">最後的吸菸者</a></p></td>
<td><p>曾改編為電視劇及廣播劇。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td></td>
<td><p>藥菜飯店</p></td>
<td><p>描述一間以料理治病的神秘中華料理店</p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td></td>
<td></td>
<td><p>獲得第16回川端康成文学賞</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>塗口紅的殘像</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td></td>
<td><p>文學部唯野教授</p></td>
<td><p>鑽石個性獎（）</p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td></td>
<td><p>清晨的加斯巴／晨間小精靈</p></td>
<td><p>獲得第13回<a href="../Page/日本SF大賞.md" title="wikilink">日本SF大賞</a></p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td></td>
<td><p><a href="../Page/盜夢偵探.md" title="wikilink">盜夢偵探</a>／帕普莉卡</p></td>
<td><p>曾改編為<a href="../Page/電影.md" title="wikilink">電影</a></p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td></td>
<td><p>笑犬樓風景</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td></td>
<td><p>邪眼鳥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td></td>
<td><p>敵</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td></td>
<td><p>爺爺</p></td>
<td><p>獲得第51回讀賣文学賞小説賞 （2000年）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td></td>
<td><p>魚籃觀音記</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>細菌人間</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td></td>
<td><p>天狗的匿名信</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>小朋友的智慧</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td></td>
<td><p>聖痕</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

<references />

## 外部連結

  - [筒井康隆官方網站](http://www.jali.or.jp/tti/)

[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:紫綬褒章獲得者](../Category/紫綬褒章獲得者.md "wikilink")
[Category:日本推理小說作家](../Category/日本推理小說作家.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:同志社大學校友](../Category/同志社大學校友.md "wikilink")

1.  [「筒井康隆作家生活五十周年記念～現代語裏辞典ライブ」2010.11.23　18:00～20:20　東京カルチャーカルチャー](http://d.hatena.ne.jp/flow2005/20101124/p1)
2.  筒井「漂流　本から本へ」P74～76
3.  遠藤瓔子「青山「ロブロイ」物語」p.114（世界文化社、1987年）
4.  [時報悅讀網:爺爺(AI0062)](http://www.readingtimes.com.tw/ReadingTimes/ProductPage.aspx?gp=productdetail&cid=mcac\(SellItems\)&id=AI0062&p=excerpt&exid=30181)
5.  「筒井康隆スピーキング」p.365（出帆新社、1996年）
6.  [江上茂](../Page/山中央.md "wikilink")「差別用語を見直す」p.232-236
7.  筒井ブログ「笑犬楼大通り」より「偽文士日碌」2009年3月1日
8.  「日本SF短篇50(1)」早川書房