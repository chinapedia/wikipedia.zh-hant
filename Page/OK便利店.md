[Circle_K_logo.svg](https://zh.wikipedia.org/wiki/File:Circle_K_logo.svg "fig:Circle_K_logo.svg")

**OK便利店**（**Circle
K**）是一家成立於[美國德州](../Page/美國.md "wikilink")[艾爾帕索的](../Page/艾爾帕索_\(德克薩斯州\).md "wikilink")1951年[連鎖式](../Page/連鎖店.md "wikilink")[便利商店集團](../Page/便利商店.md "wikilink")，透過[特許經營方式經營](../Page/特許經營.md "wikilink")，商標原由[康菲石油公司持有](../Page/康菲石油公司.md "wikilink")，後出售予Alimentation
Couche-Tard。在[北美洲及](../Page/北美洲.md "wikilink")[歐洲皆有分店](../Page/歐洲.md "wikilink")。

Circle K在美國一部份地區還附設[加油站](../Page/加油站.md "wikilink")。Circle
K在[亞洲地區](../Page/亞洲.md "wikilink")，例如[香港及](../Page/香港.md "wikilink")[澳門亦設有連鎖店](../Page/澳門.md "wikilink")。

## 各地的OK便利店

### 香港及廣東省OK便利店

[Eastern_shops_in_Wong_Chuk_Hang_Station_(2).jpg](https://zh.wikipedia.org/wiki/File:Eastern_shops_in_Wong_Chuk_Hang_Station_\(2\).jpg "fig:Eastern_shops_in_Wong_Chuk_Hang_Station_(2).jpg")分店\]\]
[Choi_Ming_Shopping_Centre_Convenience_stores.jpg](https://zh.wikipedia.org/wiki/File:Choi_Ming_Shopping_Centre_Convenience_stores.jpg "fig:Choi_Ming_Shopping_Centre_Convenience_stores.jpg")[彩明商場分店](../Page/彩明商場.md "wikilink")\]\]
[Circle_K_on_East_Point_Road.jpg](https://zh.wikipedia.org/wiki/File:Circle_K_on_East_Point_Road.jpg "fig:Circle_K_on_East_Point_Road.jpg")[東角道旗艦店](../Page/東角道.md "wikilink")\]\]
香港及[中國的OK便利店由](../Page/中國.md "wikilink")[利豐屬下之](../Page/利豐.md "wikilink")[利亞零售有限公司](../Page/利亞零售有限公司.md "wikilink")（）及其附屬公司（統稱CRA集團）經營，於1985年在香港開設第一間OK便利店。OK便利店是香港第二大連鎖便利店，主要銷售[食品](../Page/食品.md "wikilink")、[日用品及](../Page/日用品.md "wikilink")[雜誌等貨品](../Page/雜誌.md "wikilink")。有別於其主要競爭對手[7-11](../Page/7-11.md "wikilink")，普遍店舖面積比[7-11大](../Page/7-11.md "wikilink")。

CRA集團於香港共有335家（2019年）Circle
K連鎖便利店\[1\]、[廣州市則有](../Page/廣州市.md "wikilink")71間便利店，[珠海市亦有](../Page/珠海市.md "wikilink")12間。廣州首間OK便利店於2002年11月以合營企業的方式投入服務，由**利亞華南便利店有限公司**負責業務營運，以此正式進入廣州市場，獲廣州市政府列為該年度十二個重大項目之一。廣州店以「好知味」、「Hot
& In」獨家品牌推出一系列新鮮出爐麵包及即磨即制的精選飲品。

#### 事件

2017年

2017年9月，藝術家程展緯【成功爭取】OK便利店店員　可自由選擇是否戴帽。
程展緯發起「全港反思便利店做乜鬼要店員戴帽工作」運動，希望令人反思相關指引是否恰當。在其Facebook帖文，指收到OK便利店前線員工的「制服指引」照片。指引提到，由於每人的髮型有不同差異，OK員工可以自由選擇是否戴帽。而在麵包廊及美食吧工作的員工，為確保衞生，就要繼續佩戴帽及口罩。

OK便利店確認，相關指引屬公司發出

2017年8月，有OK便利店員工向《蘋果》投訴工作時要長期「罰企」。職工盟零售、商業及成衣業總工會指，雖不少分店已獲派圓形鐵凳，但稱不上是工作凳，不方便員工工作，直斥OK便利店「好唔OK」。有店員說，全日僅坐到15分鐘。
該會6月19日至7月27日期間巡視70間OK便利店，約83%反映不是每名收銀員都有工作凳可坐，該會上月20日至27日再派人訪問員工，有店員回覆指公司無說明員工是否可在工作期間坐在圓凳上，亦有人反映坐下後鐵凳高度不方便工作。

2016年

2016年1月，[香港](../Page/香港.md "wikilink")[薄扶林](../Page/薄扶林.md "wikilink")[華富邨OK便利店出售的即食](../Page/華富邨.md "wikilink")「超級[熱狗包](../Page/熱狗.md "wikilink")」被發現高達208隻[螞蟻](../Page/螞蟻.md "wikilink")，被[食環署票控一項](../Page/食環署.md "wikilink")「食物的品質與購買人所要求不符」罪，OK便利店有限公司在[東區法院認罪](../Page/東區法院.md "wikilink")，罰款3,500港元。公司代表求情稱，接到投訴後已加強環境清潔，亦曾拆開機器檢查，無發現有螞蟻。OK便利店之前有6項同樣控罪的[案底](../Page/案底.md "wikilink")，上一次被罰款5,000港元，涉案分店則屬初犯。\[2\]

2016年8月香港OK便利店於推出「OK齊齊印」電子顧客管理平台，從推出至年底有近500,000名會員參與。

2016年8月位於旺角登打士街的OK便利店被指，容許6厘米乘4厘米的碎片從高處墜下，致途人受傷。
OK便利店有限公司今在九龍城法院承認容許物體自高處墜下罪，被判罰款8500港元。

2016年8月3日凌晨約3時56分，油塘油麗邨商場ok便利店發生劫案。當時兩名女職員分別在櫃枱附近叠報紙及在儲物室內工作，突然兩名年約20歲，以口罩遮面，及戴上勞工手套的賊人闖入，一人跳入櫃枱內企圖打開收銀機。其中姓黃﹝41歲﹞正在叠報紙的女職員，上前喝止，另一名賊人即伸手欲按住她嘴巴，雙方發生糾纏。
此時，在儲物室的女職員，從閉路電視目睹事件，不動聲息離開儲物室，直走出大門高叫打劫。賊人聞訊知事敗，即奪門逃走，全程逗留約一分鐘，便利店沒有損失。事後職員報警，惟警員在附近兜截沒有發現

### 澳門OK便利店

[澳門的OK便利店由](../Page/澳門.md "wikilink")**澳門OK便利店有限公司**負責業務營運，公司由利豐屬下之[利亞零售有限公司與](../Page/利亞零售有限公司.md "wikilink")[澳門工藝有限公司合資成立的](../Page/澳門工藝有限公司.md "wikilink")\[3\]。首家位於[皇朝區](../Page/外港新填海區.md "wikilink")[科英布拉街之OK便利店於](../Page/科英布拉街.md "wikilink")2005年3月15日正式營業，最高峰時間在澳門有21家OK便利店\[4\]。

澳門OK便利店於2008年成為[澳門通的合作伙伴](../Page/澳門通.md "wikilink")，成為澳門首間接受澳門通消費及增值服務的便利店。而OK便利店亦曾與澳門通推出一系列優惠及推廣活動。2009年，[大西洋銀行在部分OK便利店裝設自動櫃員機](../Page/大西洋銀行.md "wikilink")，成為澳門首間設有ATM自動櫃員機的便利店\[5\]。部份澳門OK便利店提供由[澳門電訊提供的無線寬頻](../Page/澳門電訊.md "wikilink")（[Wi-Fi](../Page/Wi-Fi.md "wikilink")）服務，是澳門首家提供此服務的便利店。\[6\]

2010年3月26日，[澳門國際機場與OK便利店合作](../Page/澳門國際機場.md "wikilink")，在澳門國際機場離境大堂（1樓）開設分店。成為澳門國際機場內首家24小時便利店\[7\]。

2010年6月11日，就網上關於澳門8間OK便利店將結業的傳聞，《[澳門日報](../Page/澳門日報.md "wikilink")》向OK便利店查詢，但一直未獲任何形式的回覆，\[8\]6月13日，澳門有多間OK便利店同時以「內部裝修」為由，暫停營業。數星期至數月後，有2至3間分店重新營業。

### 越南

[越南控股](../Page/越南控股.md "wikilink")（現[中國微電子](../Page/中國微電子.md "wikilink")，同時出售非核心業務）旗下附屬公司於2008年，獲得25年[越南經營OK便利店獨家專利權](../Page/越南.md "wikilink")。目前由Circle
K Vietnam 公司營運。

## 已解約的OK便利店

### 來來超商

[OK_Taiwan.svg](https://zh.wikipedia.org/wiki/File:OK_Taiwan.svg "fig:OK_Taiwan.svg")
 [台灣的Circle](../Page/台灣.md "wikilink")
K原是由[豐群企業集團在](../Page/豐群企業集團.md "wikilink")1988年9月與美商Circle
K合資成立的，公司總部目前位於台北市信義區，初期除在台北開店外，還採取在基隆地區開設分店，以發展「鄉村包圍都市」的開店策略，且均為直營店，到1998年才開始發展加盟事業。直至2007年為止的統計，全台灣有近830家分店。

目前已經與美國總部解約，改稱仍為「OK超商」，中文公司名稱則從解約前的「富群超商股份有限公司」改為「來來超商股份有限公司」，也修改[企業識別標誌](../Page/企業識別.md "wikilink")。

### 日本

Circle K於1980年在名古屋市開設日本第一間店鋪，後來Circle
K和另一連鎖超商「」在2004年合併為「」，公司合併但維持原有的兩個[品牌](../Page/品牌.md "wikilink")，全盛時期的2005年兩個品牌在日本全國共有接近6300間分店。Circle
K主要於中部地區，Sunkus就主要位於關東地區。

之後，[FamilyMart於](../Page/FamilyMart.md "wikilink")2016年收購Circle K
Sunkus，於同年9月1日起生效，Circle K與Sunkus的門市開始逐步轉用「FamilyMart」品牌；最後一間日本Circle
K門市（位於愛知縣）於2018年11月29日結束營業，標誌著日本的Circle K同Sunkus便利店正式走入歷史。\[9\]

## 參見

  - [連鎖店](../Page/連鎖店.md "wikilink")
  - [便利商店](../Page/便利商店.md "wikilink")

## 参考资料

## 外部連結

  - [OK便利店總公司](https://www.circlek.com/)

  - [C利亞零售](http://www.cr-asia.com/)

  - [香港OK便利店](http://www.circlek.hk/)

  - [广州OK便利店](http://www.circlek.cn/)

  -
[O](../Category/便利商店.md "wikilink") [O](../Category/美国零售商.md "wikilink")
[O](../Category/總部在美國的跨國公司.md "wikilink")
[Category:利豐](../Category/利豐.md "wikilink")
[Category:1951年成立的公司](../Category/1951年成立的公司.md "wikilink")
[Category:美國連鎖速食店](../Category/美國連鎖速食店.md "wikilink")

1.  [利亞零售2010第一季業績報告](http://www.hkexnews.hk/listedco/listconews/gem/20100513/GLN20100513043.pdf)
2.
3.
4.
5.  <http://www.bnu.com.mo/ch/personal_banking/payment_services/CircleKATM2008.html>
6.
7.
8.
9.