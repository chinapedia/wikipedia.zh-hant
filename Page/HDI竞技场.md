**HDI竞技场**（）是一座位於[德國](../Page/德國.md "wikilink")[下萨克森首府](../Page/下萨克森.md "wikilink")[漢諾威的足球場](../Page/漢諾威.md "wikilink")，球場由1959年起成為[-{zh-hans:汉诺威96;
zh-hant:漢諾威96;}-的主場](../Page/汉诺威96足球俱乐部.md "wikilink")，可容納48,933人。

## 歷史

HDI竞技场原名為**下薩克森體育場**，建於1954年，採用[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")[漢諾威大量的建築瓦礫建造地基](../Page/漢諾威.md "wikilink")，當時容量達到86,000名觀眾。[漢諾威96於](../Page/汉诺威96足球俱乐部.md "wikilink")2002年獲財務機構AWD冠名[贊助五年](../Page/贊助.md "wikilink")，於2007年再續約五年至2012年6月。在[2006年世界盃足球賽成為十二個比賽場館之一](../Page/2006年世界盃足球賽.md "wikilink")，比賽期間稱為「漢諾威世界盃球場」（），舉行四場分組初賽及一場次輪淘汰賽。

## 外部連結

  - [BBC世界盃球場：漢諾威](http://news.bbc.co.uk/sport1/hi/football/world_cup_2006/venues/4459048.stm)

[Category:德國足球場](../Category/德國足球場.md "wikilink")
[Category:1974年世界盃足球場](../Category/1974年世界盃足球場.md "wikilink")
[Category:2006年世界盃足球場](../Category/2006年世界盃足球場.md "wikilink")