**中国**的**自然保护区**，根据《中华人民共和国自然保护区条例》，是指对有代表性的自然生态系统、珍稀濒危野生动植物物种的天然集中分布区、有特殊意义的自然遗迹等保护对象所在的陆地、陆地水体或者海域，依法划出一定面积予以特殊保护和管理的区域。自然保护区分为**国家级自然保护区**和地方各级自然保护区，其中在国内外有典型意义、在科学上有重大国际影响或者有特殊科学研究价值的自然保护区，列为国家级自然保护区，报国务院批准建立。

中国现有474处国家级自然保护区。

## 批建时间

以下是国务院批复（或因有相关批示而可被视为）同意建立国家级自然保护区的日期：

  - 1956年6月30日：鼎湖山计一处；
  - 1975年3月20日：卧龙、蜂桶寨计2处；
  - 1978年3月1日：花坪计一处；
  - 1978年12月15日：九寨沟、马边大风顶、美姑大风顶、佛坪、白水江今作5处计；
  - 1979年7月3日：武夷山计一处；
  - 1980年1月17日：山旺古生物化石计一处；
  - 1980年3月16日：南滚河计一处；
  - 1980年8月6日：蛇岛、老铁山计一处；
  - 1980年9月11日：㟖岗计一处；
  - 1984年10月18日：蓟县中、上元古界地层剖面计一处；
  - 1985年3月5日：阿尔金山计一处；
  - 1986年7月9日：长白山等20处；
  - 1987年4月18日：扎龙计一处；
  - 1988年5月9日：雾灵山等25处；
  - 1990年9月30日：昌黎黄金海岸等5处；
  - 1992年10月27日：古海岸与湿地等16处；
  - 1994年4月5日：牡丹峰等13处；
  - 1995年11月6日：八仙山等3处；
  - 1995年12月22日：长青计一处；
  - 1996年11月29日：大兴安岭汗马等7处；
  - 1997年12月8日：芦芽山等18处；
  - 1998年8月18日：围场红松洼等12处；
  - 2000年4月4日：白音敖包等18处；
  - 2001年6月16日：大黑山等16处；
  - 2002年7月2日：泥河湾等17处；
  - 2003年1月24日：额济纳胡杨林等9处；
  - 2003年6月6日：衡水湖等29处；
  - 2005年7月23日：柳江盆地地质遗迹等17处；
  - 2006年2月11日：五鹿山等22处；
  - 2007年4月6日：塞罕坝等19处；
  - 2008年1月14日：百花山等19处；
  - 2009年9月18日：松花江三湖等16处；
  - 2011年4月16日：驼梁等16处；
  - 2012年1月21日：青崖寨等28处；
  - 2013年6月4日：大黑山等21处；
  - 2013年12月25日：灵空山等23处；
  - 2014年12月5日：毕拉河等21处；
  - 2016年5月2日：楼子山等18处；
  - 2017年7月4日：盘中等17处；
  - 2018年2月8日：五花顶等6处；
  - 2018年5月31日：太宽河等5处。

注：蜂桶寨、马边大风顶、九寨沟、美姑大风顶于1994年7月28日确认为国家级；南滚河于1995年2月21日确认为国家级；鼎湖山于1998年8月27日确认为国家级；山旺古生物化石于1999年10月18日确认为国家级。

## 华北地区

  - [北京](../Page/北京.md "wikilink")
      - [松山国家级自然保护区](../Page/松山国家级自然保护区.md "wikilink")
      - [百花山国家级自然保护区](../Page/百花山.md "wikilink")

<!-- end list -->

  - [天津](../Page/天津.md "wikilink")
      - [蓟县中、上元古界地层剖面国家级自然保护区](../Page/天津蓟县中上元古界国家级自然保护区.md "wikilink")
      - [古海岸与湿地国家级自然保护区](../Page/天津古海岸与湿地国家级自然保护区.md "wikilink")
      - [八仙山国家级自然保护区](../Page/八仙山_\(天津\).md "wikilink")

<!-- end list -->

  - [河北](../Page/河北.md "wikilink")
      - [雾灵山国家级自然保护区](../Page/雾灵山.md "wikilink")
      - [昌黎黄金海岸国家级自然保护区](../Page/昌黎黄金海岸.md "wikilink")
      - [围场红松洼国家级自然保护区](../Page/围场红松洼.md "wikilink")
      - [泥河湾国家级自然保护区](../Page/泥河湾.md "wikilink")
      - [小五台山国家级自然保护区](../Page/小五台山.md "wikilink")
      - [衡水湖国家级自然保护区](../Page/衡水湖.md "wikilink")
      - [大海坨国家级自然保护区](../Page/大海坨.md "wikilink")
      - [柳江盆地地质遗迹国家级自然保护区](../Page/柳江盆地地质遗迹.md "wikilink")
      - [塞罕坝国家级自然保护区](../Page/塞罕坝.md "wikilink")
      - [滦河上游国家级自然保护区](../Page/滦河上游.md "wikilink")
      - [茅荆坝国家级自然保护区](../Page/茅荆坝.md "wikilink")
      - [驼梁国家级自然保护区](../Page/驼梁.md "wikilink")
      - [青崖寨国家级自然保护区](../Page/青崖寨.md "wikilink")

<!-- end list -->

  - [山西](../Page/山西.md "wikilink")
      - [庞泉沟国家级自然保护区](../Page/庞泉沟.md "wikilink")
      - [历山国家级自然保护区](../Page/历山.md "wikilink")
      - [芦芽山国家级自然保护区](../Page/芦芽山.md "wikilink")
      - [阳城莽河猕猴国家级自然保护区](../Page/阳城莽河猕猴.md "wikilink")
      - [五鹿山国家级自然保护区](../Page/五鹿山.md "wikilink")
      - [黑茶山国家级自然保护区](../Page/黑茶山.md "wikilink")
      - [灵空山国家级自然保护区](../Page/灵空山.md "wikilink")
      - [太宽河国家级自然保护区](../Page/太宽河.md "wikilink")

<!-- end list -->

  - [内蒙古](../Page/内蒙古.md "wikilink")
      - [大青沟国家级自然保护区](../Page/大青沟.md "wikilink")
      - [内蒙古贺兰山国家级自然保护区](../Page/贺兰山.md "wikilink")
      - [呼伦湖国家级自然保护区](../Page/呼伦湖.md "wikilink")
      - [科尔沁国家级自然保护区](../Page/科尔沁.md "wikilink")
      - [大兴安岭汗马国家级自然保护区](../Page/大兴安岭汗马.md "wikilink")
      - [锡林郭勒草原国家级自然保护区](../Page/锡林郭勒草原.md "wikilink")
      - [达里诺尔国家级自然保护区](../Page/达里诺尔.md "wikilink")
      - [西鄂尔多斯国家级自然保护区](../Page/西鄂尔多斯.md "wikilink")
      - [白音熬包国家级自然保护区](../Page/白音熬包.md "wikilink")
      - [赛罕乌拉国家级自然保护区](../Page/赛罕乌拉.md "wikilink")
      - [大黑山国家级自然保护区](../Page/大黑山.md "wikilink")
      - [乌拉特梭梭林－蒙古野驴国家级自然保护区](../Page/乌拉特梭梭林－蒙古野驴.md "wikilink")
      - [鄂尔多斯遗鸥国家级自然保护区](../Page/鄂尔多斯遗鸥.md "wikilink")
      - [辉河国家级自然保护区](../Page/辉河.md "wikilink")
      - [图牧吉国家级自然保护区](../Page/图牧吉.md "wikilink")
      - [额济纳胡杨林国家级自然保护区](../Page/额济纳胡杨林.md "wikilink")
      - [红花尔基樟子松林国家级自然保护区](../Page/红花尔基樟子松林.md "wikilink")
      - [黑里河国家级自然保护区](../Page/黑里河.md "wikilink")
      - [阿鲁科尔沁草原国家级自然保护区](../Page/阿鲁科尔沁草原.md "wikilink")
      - [哈腾套海国家级自然保护区](../Page/哈腾套海.md "wikilink")
      - [额尔古纳国家级自然保护区](../Page/额尔古纳.md "wikilink")
      - [鄂托克恐龙遗迹化石国家级自然保护区](../Page/鄂托克恐龙遗迹化石.md "wikilink")
      - [大青山国家级自然保护区](../Page/大青山.md "wikilink")
      - [高格斯台罕乌拉国家级自然保护区](../Page/高格斯台罕乌拉.md "wikilink")
      - [古日格斯台国家级自然保护区](../Page/古日格斯台.md "wikilink")
      - [罕山国家级自然保护区](../Page/罕山.md "wikilink")
      - [青山国家级自然保护区](../Page/青山国家级自然保护区.md "wikilink")
      - [毕拉河国家级自然保护区](../Page/毕拉河.md "wikilink")
      - [乌兰坝国家级自然保护区](../Page/乌兰坝.md "wikilink")

## 东北地区

  - [辽宁](../Page/辽宁.md "wikilink")
      - [蛇岛](../Page/蛇岛.md "wikilink")、[老铁山国家级自然保护区](../Page/老铁山.md "wikilink")
      - [医巫闾山国家级自然保护区](../Page/医巫闾山.md "wikilink")
      - [白石砬子国家级自然保护区](../Page/白石砬子.md "wikilink")
      - [辽河口国家级自然保护区](../Page/辽河口.md "wikilink")
      - [仙人洞国家级自然保护区](../Page/仙人洞.md "wikilink")
      - [大连斑海豹国家级自然保护区](../Page/大连斑海豹.md "wikilink")
      - [丹东鸭绿江口滨海湿地国家级自然保护区](../Page/丹东鸭绿江口滨海湿地.md "wikilink")
      - [北票鸟化石国家级自然保护区](../Page/北票鸟化石国家级自然保护区.md "wikilink")
      - [桓仁老秃顶子国家级自然保护区](../Page/桓仁老秃顶子.md "wikilink")
      - [成山头海滨地貌国家级自然保护区](../Page/成山头海滨地貌.md "wikilink")
      - [努鲁儿虎山国家级自然保护区](../Page/努鲁儿虎山.md "wikilink")
      - [海棠山国家级自然保护区](../Page/海棠山.md "wikilink")
      - [白狼山国家级自然保护区](../Page/白狼山.md "wikilink")
      - [章古台国家级自然保护区](../Page/章古台.md "wikilink")
      - [大黑山国家级自然保护区](../Page/大黑山.md "wikilink")
      - [葫芦岛虹螺山国家级自然保护区](../Page/葫芦岛虹螺山.md "wikilink")
      - [青龙河国家级自然保护区](../Page/青龙河.md "wikilink")
      - [楼子山国家级自然保护区](../Page/楼子山.md "wikilink")
      - [五花顶国家级自然保护区](../Page/五花顶.md "wikilink")

<!-- end list -->

  - [吉林](../Page/吉林.md "wikilink")
      - [长白山国家级自然保护区](../Page/长白山.md "wikilink")
      - [向海国家级自然保护区](../Page/向海.md "wikilink")
      - [伊通火山群国家级自然保护区](../Page/伊通火山群.md "wikilink")
      - [莫莫格国家级自然保护区](../Page/莫莫格.md "wikilink")
      - [天佛指山国家级自然保护区](../Page/天佛指山.md "wikilink")
      - [鸭绿江上游国家级自然保护区](../Page/鸭绿江上游.md "wikilink")
      - [龙湾国家级自然保护区](../Page/龙湾.md "wikilink")
      - [大布苏国家级自然保护区](../Page/大布苏.md "wikilink")
      - [珲春东北虎国家级自然保护区](../Page/珲春东北虎.md "wikilink")
      - [查干湖国家级自然保护区](../Page/查干湖.md "wikilink")
      - [雁鸣湖国家级自然保护区](../Page/雁鸣湖.md "wikilink")
      - [松花江三湖国家级自然保护区](../Page/松花江三湖.md "wikilink")
      - [哈泥国家级自然保护区](../Page/哈泥.md "wikilink")
      - [波罗湖国家级自然保护区](../Page/波罗湖.md "wikilink")
      - [靖宇国家级自然保护区](../Page/靖宇.md "wikilink")
      - [黄泥河国家级自然保护区](../Page/黄泥河.md "wikilink")
      - [汪清国家级自然保护区](../Page/汪清.md "wikilink")
      - [白山原麝国家级自然保护区](../Page/白山原麝.md "wikilink")
      - [四平山门中生代火山国家级自然保护区](../Page/四平山门中生代火山.md "wikilink")
      - [集安国家级自然保护区](../Page/集安.md "wikilink")
      - [通化石湖国家级自然保护区](../Page/通化石湖.md "wikilink")
      - [园池湿地国家级自然保护区](../Page/园池湿地.md "wikilink")
      - [头道松花江上游国家级自然保护区](../Page/头道松花江上游.md "wikilink")
      - [甑峰岭国家级自然保护区](../Page/甑峰岭.md "wikilink")

<!-- end list -->

  - [黑龙江](../Page/黑龙江.md "wikilink")
      - [扎龙国家级自然保护区](../Page/扎龙.md "wikilink")
      - [丰林国家级自然保护区](../Page/丰林.md "wikilink")
      - [呼中国家级自然保护区](../Page/呼中.md "wikilink")
      - [牡丹峰国家级自然保护区](../Page/牡丹峰.md "wikilink")
      - [兴凯湖国家级自然保护区](../Page/兴凯湖.md "wikilink")
      - [五大连池国家级自然保护区](../Page/五大连池.md "wikilink")
      - [洪河国家级自然保护区](../Page/洪河.md "wikilink")
      - [凉水国家级自然保护区](../Page/凉水.md "wikilink")
      - [饶河东北黑蜂国家级自然保护区](../Page/饶河东北黑蜂.md "wikilink")
      - [三江国家级自然保护区](../Page/三江.md "wikilink")
      - [宝清七星河国家级自然保护区](../Page/宝清七星河.md "wikilink")
      - [挠力河国家级自然保护区](../Page/挠力河.md "wikilink")
      - [南瓮河国家级自然保护区](../Page/南瓮河.md "wikilink")
      - [八岔岛国家级自然保护区](../Page/八岔岛.md "wikilink")
      - [凤凰山国家级自然保护区](../Page/凤凰山.md "wikilink")
      - [乌伊岭国家级自然保护区](../Page/乌伊岭.md "wikilink")
      - [胜山国家级自然保护区](../Page/胜山.md "wikilink")
      - [珍宝岛湿地国家级自然保护区](../Page/珍宝岛.md "wikilink")
      - [红星湿地国家级自然保护区](../Page/红星湿地.md "wikilink")
      - [双河国家级自然保护区](../Page/双河.md "wikilink")
      - [东方红国家级自然保护区](../Page/东方红林业局.md "wikilink")
      - [大沾河湿地国家级自然保护区](../Page/大沾河湿地.md "wikilink")
      - [穆棱东北红豆杉国家级自然保护区](../Page/穆棱东北红豆杉.md "wikilink")
      - [新青白头鹤国家级自然保护区](../Page/新青白头鹤.md "wikilink")
      - [绰纳河国家级自然保护区](../Page/绰纳河.md "wikilink")
      - [多布库尔国家级自然保护区](../Page/多布库尔.md "wikilink")
      - [友好国家级自然保护区](../Page/友好.md "wikilink")
      - [小北湖国家级自然保护区](../Page/小北湖.md "wikilink")
      - [三环泡国家级自然保护区](../Page/三环泡.md "wikilink")
      - [乌裕尔河国家级自然保护区](../Page/乌裕尔河.md "wikilink")
      - [中央站黑嘴松鸡国家级自然保护区](../Page/中央站黑嘴松鸡.md "wikilink")
      - [茅兰沟国家级自然保护区](../Page/茅兰沟.md "wikilink")
      - [明水国家级自然保护区](../Page/明水.md "wikilink")
      - [太平沟国家级自然保护区](../Page/太平沟.md "wikilink")
      - [老爷岭东北虎国家级自然保护区](../Page/老爷岭.md "wikilink")
      - [大峡谷国家级自然保护区](../Page/黑龙江大峡谷.md "wikilink")
      - [北极村国家级自然保护区](../Page/北极村.md "wikilink")
      - [公别拉河国家级自然保护区](../Page/公别拉河.md "wikilink")
      - [碧水中华秋沙鸭国家级自然保护区](../Page/碧水中华秋沙鸭.md "wikilink")
      - [翠北湿地国家级自然保护区](../Page/翠北湿地.md "wikilink")
      - [盘中国家级自然保护区](../Page/盘中.md "wikilink")
      - [平顶山国家级自然保护区](../Page/平顶山国家级自然保护区.md "wikilink")
      - [乌马河紫貂国家级自然保护区](../Page/乌马河紫貂国家级自然保护区.md "wikilink")
      - [岭峰国家级自然保护区](../Page/岭峰.md "wikilink")
      - [黑瞎子岛国家级自然保护区](../Page/黑瞎子岛.md "wikilink")
      - [七星砬子东北虎国家级自然保护区](../Page/七星砬子东北虎国家级自然保护区.md "wikilink")
      - [仙洞山梅花鹿国家级自然保护区](../Page/仙洞山梅花鹿国家级自然保护区.md "wikilink")
      - [朗乡国家级自然保护区](../Page/朗乡.md "wikilink")
      - [细鳞河国家级自然保护区](../Page/细鳞河.md "wikilink")

## 华东地区

  - [上海](../Page/上海.md "wikilink")
      - [九段沙湿地国家级自然保护区](../Page/九段沙湿地.md "wikilink")
      - [崇明东滩鸟类国家级自然保护区](../Page/东滩.md "wikilink")

<!-- end list -->

  - [江苏](../Page/江苏.md "wikilink")
      - [盐城沿海滩涂珍禽国家级自然保护区](../Page/盐城沿海滩涂珍禽.md "wikilink")
      - [大丰麋鹿国家级自然保护区](../Page/大丰麋鹿国家级自然保护区.md "wikilink")
      - [泗洪洪泽湖湿地国家级自然保护区](../Page/泗洪洪泽湖湿地.md "wikilink")

<!-- end list -->

  - [浙江](../Page/浙江.md "wikilink")
      - [天目山国家级自然保护区](../Page/天目山.md "wikilink")
      - [南麂列岛海洋国家级自然保护区](../Page/南麂列岛.md "wikilink")
      - [凤阳山－百山祖国家级自然保护区](../Page/凤阳山－百山祖.md "wikilink")
      - [乌岩岭国家级自然保护区](../Page/乌岩岭.md "wikilink")
      - [临安清凉峰国家级自然保护区](../Page/临安清凉峰国家级自然保护区.md "wikilink")
      - [古田山国家级自然保护区](../Page/古田山.md "wikilink")
      - [大盘山国家级自然保护区](../Page/大盘山.md "wikilink")
      - [九龙山国家级自然保护区](../Page/九龙山_\(浙江\).md "wikilink")
      - [长兴地质遗迹国家级自然保护区](../Page/长兴地质遗迹.md "wikilink")
      - [象山韭山列岛国家级自然保护区](../Page/韭山列岛.md "wikilink")
      - [安吉小鲵国家级自然保护区](../Page/安吉小鲵国家级自然保护区.md "wikilink")

<!-- end list -->

  - [安徽](../Page/安徽.md "wikilink")

<!-- end list -->

  -   - [扬子鳄国家级自然保护区](../Page/扬子鳄.md "wikilink")
      - [古牛绛国家级自然保护区](../Page/古牛绛.md "wikilink")
      - [鹞落坪国家级自然保护区](../Page/鹞落坪.md "wikilink")
      - [升金湖国家级自然保护区](../Page/升金湖.md "wikilink")
      - [金寨天马国家级自然保护区](../Page/金寨天马.md "wikilink")
      - [铜陵淡水豚国家级自然保护区](../Page/铜陵淡水豚.md "wikilink")
      - [清凉峰国家级自然保护区](../Page/清凉峰国家级自然保护区.md "wikilink")
      - [古井园国家级自然保护区](../Page/古井园.md "wikilink")

<!-- end list -->

  - [福建](../Page/福建.md "wikilink")
      - [武夷山国家级自然保护区](../Page/武夷山.md "wikilink")
      - [梅花山国家级自然保护区](../Page/梅花山.md "wikilink")
      - [深沪湾海底古森林遗迹国家级自然保护区](../Page/深沪湾海底古森林遗迹.md "wikilink")
      - [将乐龙栖山国家级自然保护区](../Page/将乐龙栖山.md "wikilink")
      - [厦门珍稀海洋物种国家级自然保护区](../Page/厦门珍稀海洋物种国家级自然保护区.md "wikilink")
      - [虎伯寮国家级自然保护区](../Page/虎伯寮.md "wikilink")
      - [梁野山国家级自然保护区](../Page/梁野山.md "wikilink")
      - [天宝岩国家级自然保护区](../Page/天宝岩.md "wikilink")
      - [漳江口红树林国家级自然保护区](../Page/漳江口红树林.md "wikilink")
      - [戴云山国家级自然保护区](../Page/戴云山.md "wikilink")
      - [闽江源国家级自然保护区](../Page/闽江源.md "wikilink")
      - [君子峰国家级自然保护区](../Page/君子峰.md "wikilink")
      - [雄江黄楮林国家级自然保护区](../Page/雄江黄楮林.md "wikilink")
      - [闽江河口湿地国家级自然保护区](../Page/闽江河口湿地.md "wikilink")
      - [茫荡山国家级自然保护区](../Page/茫荡山.md "wikilink")
      - [汀江源国家级自然保护区](../Page/汀江源.md "wikilink")
      - [峨嵋峰国家级自然保护区](../Page/峨嵋峰.md "wikilink")

<!-- end list -->

  - [江西](../Page/江西.md "wikilink")
      - [鄱阳湖候鸟国家级自然保护区](../Page/鄱阳湖候鸟.md "wikilink")
      - [井冈山国家级自然保护区](../Page/井冈山.md "wikilink")
      - [桃红岭梅花鹿国家级自然保护区](../Page/桃红岭梅花鹿.md "wikilink")
      - [江西武夷山国家级自然保护区](../Page/江西武夷山.md "wikilink")
      - [九连山国家级自然保护区](../Page/九连山.md "wikilink")
      - [官山国家级自然保护区](../Page/官山.md "wikilink")
      - [鄱阳湖南矶湿地国家级自然保护区](../Page/鄱阳湖南矶湿地.md "wikilink")
      - [马头山国家级自然保护区](../Page/马头山.md "wikilink")
      - [九岭山国家级自然保护区](../Page/九岭山.md "wikilink")
      - [齐云山国家级自然保护区](../Page/齐云山（崇义县）.md "wikilink")
      - [阳际峰国家级自然保护区](../Page/阳际峰.md "wikilink")
      - [赣江源国家级自然保护区](../Page/赣江源.md "wikilink")
      - [庐山国家级自然保护区](../Page/庐山.md "wikilink")
      - [铜钹山国家级自然保护区](../Page/铜钹山.md "wikilink")
      - [婺源森林鸟类国家级自然保护区](../Page/婺源森林鸟类.md "wikilink")
      - [南风面国家级自然保护区](../Page/南风面.md "wikilink")

<!-- end list -->

  - [山东](../Page/山东.md "wikilink")
      - [山旺古生物化石国家级自然保护区](../Page/山旺古生物化石.md "wikilink")
      - [长岛国家级自然保护区](../Page/长山列岛.md "wikilink")
      - [黄河三角洲国家级自然保护区](../Page/黄河三角洲.md "wikilink")
      - [马山国家级自然保护区](../Page/马山.md "wikilink")
      - [滨州贝壳堤岛与湿地国家级自然保护区](../Page/滨州贝壳堤岛与湿地.md "wikilink")
      - [荣成大天鹅国家级自然保护区](../Page/荣成大天鹅.md "wikilink")
      - [昆嵛山国家级自然保护区](../Page/昆嵛山.md "wikilink")

## 中南地区

  - [河南](../Page/河南.md "wikilink")
      - [鸡公山国家级自然保护区](../Page/鸡公山.md "wikilink")
      - [宝天曼国家级自然保护区](../Page/宝天曼.md "wikilink")
      - [新乡黄河湿地鸟类国家级自然保护区](../Page/新乡黄河湿地鸟类.md "wikilink")
      - [伏牛山国家级自然保护区](../Page/伏牛山.md "wikilink")
      - [焦作太行山猕猴国家级自然保护区](../Page/焦作太行山猕猴.md "wikilink")
      - [董寨国家级自然保护区](../Page/董寨.md "wikilink")
      - [南阳恐龙蛋化石群国家级自然保护区](../Page/南阳恐龙蛋化石群.md "wikilink")
      - [黄河湿地国家级自然保护区](../Page/黄河湿地.md "wikilink")
      - [连康山国家级自然保护区](../Page/连康山.md "wikilink")
      - [小秦岭国家级自然保护区](../Page/小秦岭.md "wikilink")
      - [丹江湿地国家级自然保护区](../Page/丹江湿地.md "wikilink")
      - [大别山国家级自然保护区](../Page/商城大别山.md "wikilink")
      - [高乐山国家级自然保护区](../Page/高乐山.md "wikilink")

<!-- end list -->

  - [湖北](../Page/湖北.md "wikilink")
      - [神农架国家级自然保护区](../Page/神农架.md "wikilink")
      - [长江新螺段白鱀豚国家级自然保护区](../Page/长江新螺段白鱀豚.md "wikilink")
      - [长江天鹅洲白鱀豚国家级自然保护区](../Page/长江天鹅洲白鱀豚.md "wikilink")
      - [石首糜鹿国家级自然保护区](../Page/石首糜鹿.md "wikilink")
      - [五峰后河国家级自然保护区](../Page/五峰后河.md "wikilink")
      - [青龙山恐龙蛋化石群国家级自然保护区](../Page/青龙山恐龙蛋化石群.md "wikilink")
      - [星斗山国家级自然保护区](../Page/星斗山.md "wikilink")
      - [九宫山国家级自然保护区](../Page/九宫山.md "wikilink")
      - [七姊妹山国家级自然保护区](../Page/七姊妹山.md "wikilink")
      - [龙感湖国家级自然保护区](../Page/龙感湖.md "wikilink")
      - [赛武当国家级自然保护区](../Page/赛武当.md "wikilink")
      - [木林子国家级自然保护区](../Page/木林子.md "wikilink")
      - [咸丰忠建河大鲵国家级自然保护区](../Page/咸丰忠建河大鲵.md "wikilink")
      - [堵河源国家级自然保护区](../Page/堵河源.md "wikilink")
      - [十八里长峡国家级自然保护区](../Page/十八里长峡.md "wikilink")
      - [洪湖国家级自然保护区](../Page/洪湖.md "wikilink")
      - [南河国家级自然保护区](../Page/谷城南河.md "wikilink")
      - [大别山国家级自然保护区](../Page/黄冈大别山.md "wikilink")
      - [巴东金丝猴国家级自然保护区](../Page/巴东金丝猴.md "wikilink")
      - [长阳崩尖子国家级自然保护区](../Page/长阳崩尖子.md "wikilink")
      - [大老岭国家级自然保护区](../Page/大老岭.md "wikilink")
      - [五道峡国家级自然保护区](../Page/五道峡.md "wikilink")

<!-- end list -->

  - [湖南](../Page/湖南.md "wikilink")
      - [八大公山国家级自然保护区](../Page/八大公山.md "wikilink")
      - [东洞庭湖国家级自然保护区](../Page/东洞庭湖.md "wikilink")
      - [壶瓶山国家级自然保护区](../Page/壶瓶山.md "wikilink")
      - [莽山国家级自然保护区](../Page/莽山.md "wikilink")
      - [张家界大鲵国家级自然保护区](../Page/张家界大鲵.md "wikilink")
      - [永州都庞岭国家级自然保护区](../Page/永州都庞岭.md "wikilink")
      - [小溪国家级自然保护区](../Page/小溪.md "wikilink")
      - [炎陵桃源洞国家级自然保护区](../Page/炎陵桃源洞.md "wikilink")
      - [黄桑国家级自然保护区](../Page/黄桑.md "wikilink")
      - [乌云界国家级自然保护区](../Page/乌云界.md "wikilink")
      - [鹰嘴界国家级自然保护区](../Page/鹰嘴界.md "wikilink")
      - [南岳衡山国家级自然保护区](../Page/南岳衡山.md "wikilink")
      - [借母溪国家级自然保护区](../Page/借母溪.md "wikilink")
      - [八面山国家级自然保护区](../Page/八面山.md "wikilink")
      - [阳明山国家级自然保护区](../Page/阳明山_\(双牌\).md "wikilink")
      - [六步溪国家级自然保护区](../Page/六步溪.md "wikilink")
      - [舜皇山国家级自然保护区](../Page/舜皇山.md "wikilink")
      - [高望界国家级自然保护区](../Page/高望界.md "wikilink")
      - [东安舜皇山国家级自然保护区](../Page/舜皇山.md "wikilink")
      - [白云山国家级自然保护区](../Page/保靖白云山.md "wikilink")
      - [西洞庭湖国家级自然保护区](../Page/西洞庭湖.md "wikilink")
      - [九嶷山国家级自然保护区](../Page/九嶷山.md "wikilink")
      - [金童山国家级自然保护区](../Page/金童山.md "wikilink")

<!-- end list -->

  - [广东](../Page/广东.md "wikilink")
      - [鼎湖山国家级自然保护区](../Page/鼎湖山.md "wikilink")
      - [内伶仃岛－福田国家级自然保护区](../Page/内伶仃岛－福田.md "wikilink")
      - [车八岭国家级自然保护区](../Page/车八岭.md "wikilink")
      - [惠东港口海龟国家级自然保护区](../Page/惠东港口海龟.md "wikilink")
      - [南岭国家级自然保护区](../Page/南岭.md "wikilink")
      - [丹霞山国家级自然保护区](../Page/丹霞山.md "wikilink")
      - [湛江红树林国家级自然保护区](../Page/湛江红树林.md "wikilink")
      - [象头山国家级自然保护区](../Page/象头山.md "wikilink")
      - [珠江口中华白海豚国家级自然保护区](../Page/珠江口中华白海豚.md "wikilink")
      - [徐闻珊瑚礁国家级自然保护区](../Page/徐闻珊瑚礁.md "wikilink")
      - [雷州珍稀海洋生物国家级自然保护区](../Page/雷州珍稀海洋生物.md "wikilink")
      - [石门台国家级自然保护区](../Page/石门台.md "wikilink")
      - [南澎列岛国家级自然保护区](../Page/南澎列岛.md "wikilink")
      - [罗坑鳄蜥国家级自然保护区](../Page/罗坑鳄蜥.md "wikilink")
      - [云开山国家级自然保护区](../Page/云开山.md "wikilink")

<!-- end list -->

  - [广西](../Page/广西.md "wikilink")
      - [花坪国家级自然保护区](../Page/花坪.md "wikilink")
      - [㟖岗国家级自然保护区](../Page/㟖岗.md "wikilink")
      - [山口红树林生态国家级自然保护区](../Page/山口红树林生态国家级自然保护区.md "wikilink")
      - [合浦营盘港－英罗港儒艮国家级自然保护区](../Page/合浦营盘港－英罗港儒艮国家级自然保护区.md "wikilink")
      - [防城金花茶国家级自然保护区](../Page/防城金花茶.md "wikilink")
      - [木论国家级自然保护区](../Page/木论.md "wikilink")
      - [大瑶山国家级自然保护区](../Page/大瑶山.md "wikilink")
      - [北仑河口国家级自然保护区](../Page/北仑河口.md "wikilink")
      - [大明山国家级自然保护区](../Page/大明山.md "wikilink")
      - [猫儿山国家级自然保护区](../Page/猫儿山.md "wikilink")
      - [十万大山国家级自然保护区](../Page/十万大山.md "wikilink")
      - [千家洞国家级自然保护区](../Page/千家洞.md "wikilink")
      - [岑王老山国家级自然保护区](../Page/岑王老山.md "wikilink")
      - [九万山国家级自然保护区](../Page/九万山.md "wikilink")
      - [金钟山黑颈长尾雉国家级自然保护区](../Page/金钟山黑颈长尾雉.md "wikilink")
      - [雅长兰科植物国家级自然保护区](../Page/雅长兰科植物.md "wikilink")
      - [崇左白头叶猴国家级自然保护区](../Page/崇左白头叶猴.md "wikilink")
      - [大桂山鳄蜥国家级自然保护区](../Page/大桂山鳄蜥.md "wikilink")
      - [邦亮长臂猿国家级自然保护区](../Page/邦亮长臂猿.md "wikilink")
      - [恩城国家级自然保护区](../Page/恩城.md "wikilink")
      - [元宝山国家级自然保护区](../Page/元宝山.md "wikilink")
      - [七冲国家级自然保护区](../Page/七冲.md "wikilink")
      - [银竹老山资源冷杉国家级自然保护区](../Page/银竹老山资源冷杉.md "wikilink")

<!-- end list -->

  - [海南](../Page/海南.md "wikilink")
      - [东寨港国家级自然保护区](../Page/东寨港.md "wikilink")
      - [大田国家级自然保护区](../Page/大田.md "wikilink")
      - [坝王岭国家级自然保护区](../Page/坝王岭.md "wikilink")
      - [大洲岛海洋生态国家级自然保护区](../Page/大洲岛海洋生态.md "wikilink")
      - [三亚珊瑚礁国家级自然保护区](../Page/三亚珊瑚礁.md "wikilink")
      - [尖峰岭国家级自然保护区](../Page/尖峰岭.md "wikilink")
      - [铜鼓岭国家级自然保护区](../Page/铜鼓岭.md "wikilink")
      - [五指山国家级自然保护区](../Page/五指山.md "wikilink")
      - [吊罗山国家级自然保护区](../Page/吊罗山.md "wikilink")
      - [鹦哥岭国家级自然保护区](../Page/鹦哥岭.md "wikilink")

## 西南地区

  - [重庆](../Page/重庆.md "wikilink")
      - [金佛山国家级自然保护区](../Page/金佛山.md "wikilink")
      - [缙云山国家级自然保护区](../Page/缙云山.md "wikilink")
      - [大巴山国家级自然保护区](../Page/大巴山.md "wikilink")
      - [长江上游珍稀、特有鱼类国家级自然保护区](../Page/长江上游珍稀、特有鱼类.md "wikilink") \*
      - [雪宝山国家级自然保护区](../Page/雪宝山.md "wikilink")
      - [阴条岭国家级自然保护区](../Page/阴条岭.md "wikilink")
      - [五里坡国家级自然保护区](../Page/五里坡.md "wikilink")

<!-- end list -->

  - [四川](../Page/四川.md "wikilink")
      - [卧龙国家级自然保护区](../Page/卧龙.md "wikilink")
      - [蜂桶寨国家级自然保护区](../Page/蜂桶寨.md "wikilink")
      - [九寨沟国家级自然保护区](../Page/九寨沟.md "wikilink")
      - [马边大风顶国家级自然保护区](../Page/马边大风顶.md "wikilink")
      - [美姑大风顶国家级自然保护区](../Page/美姑大风顶.md "wikilink")
      - [唐家河国家级自然保护区](../Page/唐家河.md "wikilink")
      - [小金四姑娘山国家级自然保护区](../Page/四姑娘山.md "wikilink")
      - [攀枝花苏铁国家级自然保护区](../Page/攀枝花苏铁.md "wikilink")
      - [贡嘎山国家级自然保护区](../Page/贡嘎山.md "wikilink")
      - [龙溪－虹口国家级自然保护区](../Page/龙溪－虹口.md "wikilink")
      - [若尔盖湿地国家级自然保护区](../Page/若尔盖湿地.md "wikilink")
      - [长江上游珍稀、特有鱼类国家级自然保护区](../Page/长江上游珍稀、特有鱼类.md "wikilink") \*
      - [亞丁国家级自然保护区](../Page/亞丁_\(稻城\).md "wikilink")
      - [王朗国家级自然保护区](../Page/王朗.md "wikilink")
      - [白水河国家级自然保护区](../Page/白水河.md "wikilink")
      - [察青松多白唇鹿国家级自然保护区](../Page/察青松多白唇鹿.md "wikilink")
      - [长宁竹海国家级自然保护区](../Page/长宁竹海.md "wikilink")
      - [画稿溪国家级自然保护区](../Page/画稿溪.md "wikilink")
      - [米仓山国家级自然保护区](../Page/米仓山.md "wikilink")
      - [雪宝顶国家级自然保护区](../Page/雪宝顶.md "wikilink")
      - [花萼山国家级自然保护区](../Page/花萼山.md "wikilink")
      - [海子山国家级自然保护区](../Page/海子山.md "wikilink")
      - [长沙贡玛国家级自然保护区](../Page/长沙贡玛.md "wikilink")
      - [老君山国家级自然保护区](../Page/老君山.md "wikilink")
      - [诺水河珍稀水生动物国家级自然保护区](../Page/诺水河珍稀水生动物.md "wikilink")
      - [黑竹沟国家级自然保护区](../Page/黑竹沟.md "wikilink")
      - [格西沟国家级自然保护区](../Page/格西沟.md "wikilink")
      - [小寨子沟国家级自然保护区](../Page/小寨子沟.md "wikilink")
      - [栗子坪国家级自然保护区](../Page/栗子坪.md "wikilink")
      - [千佛山国家级自然保护区](../Page/绵阳千佛山.md "wikilink")
      - [白河国家级自然保护区](../Page/白河国家级自然保护区.md "wikilink")
      - [南莫且湿地国家级自然保护区](../Page/南莫且湿地.md "wikilink")

<!-- end list -->

  - [贵州](../Page/贵州.md "wikilink")
      - [梵净山国家级自然保护区](../Page/梵净山.md "wikilink")
      - [茂兰国家级自然保护区](../Page/貴州茂蘭國家級自然保護區.md "wikilink")
      - [威宁草海国家级自然保护区](../Page/草海.md "wikilink")
      - [赤水桫椤国家级自然保护区](../Page/赤水桫椤国家级自然保护区.md "wikilink")
      - [习水中亚热带常绿阔叶林国家级自然保护区](../Page/贵州习水中亚热带常绿阔叶林国家级自然保护区.md "wikilink")
      - [雷公山国家级自然保护区](../Page/雷公山.md "wikilink")
      - [麻阳河国家级自然保护区](../Page/麻陽河國家級自然保護區.md "wikilink")
      - [长江上游珍稀、特有鱼类国家级自然保护区](../Page/长江上游珍稀、特有鱼类.md "wikilink") \*
      - [宽阔水国家级自然保护区](../Page/貴州寬闊水國家級自然保護區.md "wikilink")
      - [佛顶山国家级自然保护区](../Page/佛顶山.md "wikilink")
      - [大沙河国家级自然保护区](../Page/大沙河国家级自然保护区.md "wikilink")

<!-- end list -->

  - [云南](../Page/云南.md "wikilink")
      - [南滚河国家级自然保护区](../Page/南滚河国家级自然保护区.md "wikilink")
      - [西双版纳国家级自然保护区](../Page/西双版纳.md "wikilink")
      - [高黎贡山国家级自然保护区](../Page/高黎贡山.md "wikilink")
      - [白马雪山国家级自然保护区](../Page/白马雪山.md "wikilink")
      - [哀牢山国家级自然保护区](../Page/哀牢山.md "wikilink")
      - [苍山洱海国家级自然保护区](../Page/苍山洱海.md "wikilink")
      - [西双版纳纳版河流域国家级自然保护区](../Page/西双版纳纳版河流域.md "wikilink")
      - [无量山国家级自然保护区](../Page/无量山.md "wikilink")
      - [金平分水岭国家级自然保护区](../Page/金平分水岭.md "wikilink")
      - [大围山国家级自然保护区](../Page/大围山_\(云南省\).md "wikilink")
      - [大山包黑颈鹤国家级自然保护区](../Page/大山包.md "wikilink")
      - [黄连山国家级自然保护区](../Page/黄连山.md "wikilink")
      - [文山国家级自然保护区](../Page/文山壯族苗族自治州.md "wikilink")
      - [长江上游珍稀、特有鱼类国家级自然保护区](../Page/长江上游珍稀、特有鱼类.md "wikilink") \*
      - [药山国家级自然保护区](../Page/药山.md "wikilink")
      - [会泽黑颈鹤国家级自然保护区](../Page/会泽黑颈鹤.md "wikilink")
      - [永德大雪山国家级自然保护区](../Page/永德大雪山.md "wikilink")
      - [轿子山国家级自然保护区](../Page/轿子山.md "wikilink")
      - [云龙天池国家级自然保护区](../Page/云龙天池.md "wikilink")
      - [元江国家级自然保护区](../Page/元江.md "wikilink")
      - [乌蒙山国家级自然保护区](../Page/乌蒙山.md "wikilink")

<!-- end list -->

  - [西藏](../Page/西藏.md "wikilink")
      - [雅鲁藏布大峡谷国家级自然保护区](../Page/雅鲁藏布大峡谷.md "wikilink")
      - [珠穆朗玛峰国家级自然保护区](../Page/珠穆朗玛峰国家级自然保护区.md "wikilink")
      - [羌塘国家级自然保护区](../Page/羌塘国家级自然保护区.md "wikilink")
      - [察隅慈巴沟国家级自然保护区](../Page/察隅慈巴沟.md "wikilink")
      - [芒康滇金丝猴国家级自然保护区](../Page/芒康滇金丝猴.md "wikilink")
      - [色林错国家级自然保护区](../Page/色林错.md "wikilink")
      - [雅鲁藏布江中游河谷黑颈鹤国家级自然保护区](../Page/雅鲁藏布江中游河谷黑颈鹤.md "wikilink")
      - [拉鲁湿地国家级自然保护区](../Page/拉鲁湿地.md "wikilink")
      - [类乌齐马鹿国家级自然保护区](../Page/类乌齐马鹿.md "wikilink")
      - [麦地卡湿地国家级自然保护区](../Page/麦地卡湿地.md "wikilink")
      - [玛旁雍错湿地国家级自然保护区](../Page/玛旁雍错.md "wikilink")

## 西北地区

  - [陕西](../Page/陕西.md "wikilink")
      - [佛坪国家级自然保护区](../Page/佛坪.md "wikilink")
      - [太白山国家级自然保护区](../Page/太白山.md "wikilink")
      - [周至国家级自然保护区](../Page/周至.md "wikilink")
      - [牛背梁国家级自然保护区](../Page/牛背梁.md "wikilink")
      - [长青国家级自然保护区](../Page/長青自然保護區.md "wikilink")
      - [汉中朱鹮国家级自然保护区](../Page/汉中朱鹮国家级自然保护区.md "wikilink")
      - [子午岭国家级自然保护区](../Page/子午岭.md "wikilink")
      - [化龙山国家级自然保护区](../Page/化龙山.md "wikilink")
      - [天华山国家级自然保护区](../Page/天华山.md "wikilink")
      - [青木川国家级自然保护区](../Page/青木川.md "wikilink")
      - [桑园国家级自然保护区](../Page/桑园.md "wikilink")
      - [陇县秦岭细鳞鲑国家级自然保护区](../Page/陇县秦岭细鳞鲑.md "wikilink")
      - [延安黄龙山褐马鸡国家级自然保护区](../Page/黄龙山.md "wikilink")
      - [米仓山国家级自然保护区](../Page/米仓山.md "wikilink")
      - [韩城黄龙山褐马鸡国家级自然保护区](../Page/韩城黄龙山褐马鸡.md "wikilink")
      - [太白湑水河珍稀水生生物国家级自然保护区](../Page/太白湑水河珍稀水生生物.md "wikilink")
      - [紫柏山国家级自然保护区](../Page/紫柏山.md "wikilink")
      - [略阳珍稀水生动物国家级自然保护区](../Page/略阳珍稀水生动物.md "wikilink")
      - [黄柏塬国家级自然保护区](../Page/黄柏塬.md "wikilink")
      - [平河梁国家级自然保护区](../Page/平河梁.md "wikilink")
      - [老县城国家级自然保护区](../Page/老县城.md "wikilink")
      - [观音山国家级自然保护区](../Page/观音山.md "wikilink")
      - [丹凤武关河珍稀水生动物国家级自然保护区](../Page/丹凤武关河珍稀水生动物.md "wikilink")
      - [黑河珍稀水生野生动物国家级自然保护区](../Page/黑河珍稀水生野生动物.md "wikilink")
      - [摩天岭国家级自然保护区](../Page/摩天岭国家级自然保护区.md "wikilink")
      - [红碱淖国家级自然保护区](../Page/红碱淖.md "wikilink")

<!-- end list -->

  - [甘肃](../Page/甘肃.md "wikilink")
      - [白水江国家级自然保护区](../Page/白水江.md "wikilink")
      - [兴隆山国家级自然保护区](../Page/兴隆山.md "wikilink")
      - [祁连山国家级自然保护区](../Page/祁连山.md "wikilink")
      - [安西极旱荒漠国家级自然保护区](../Page/安西极旱荒漠.md "wikilink")
      - [尕海－则岔国家级自然保护区](../Page/尕海－则岔.md "wikilink")
      - [民勤连古城国家级自然保护区](../Page/民勤连古城.md "wikilink")
      - [莲花山国家级自然保护区](../Page/莲花山.md "wikilink")
      - [敦煌西湖国家级自然保护区](../Page/敦煌西湖.md "wikilink")
      - [太统－崆峒山国家级自然保护区](../Page/太统－崆峒山.md "wikilink")
      - [连城国家级自然保护区](../Page/连城（甘肃）.md "wikilink")
      - [小陇山国家级自然保护区](../Page/小陇山.md "wikilink")
      - [盐池湾国家级自然保护区](../Page/盐池湾.md "wikilink")
      - [安南坝野骆驼国家级自然保护区](../Page/安南坝野骆驼.md "wikilink")
      - [洮河国家级自然保护区](../Page/洮河.md "wikilink")
      - [敦煌阳关国家级自然保护区](../Page/敦煌阳关.md "wikilink")
      - [张掖黑河湿地国家级自然保护区](../Page/黑河湿地.md "wikilink")
      - [太子山国家级自然保护区](../Page/太子山.md "wikilink")
      - [漳县珍稀水生动物国家级自然保护区](../Page/漳县珍稀水生动物.md "wikilink")
      - [黄河首曲国家级自然保护区](../Page/黄河首曲.md "wikilink")
      - [秦州珍稀水生野生动物国家级自然保护区](../Page/秦州珍稀水生野生动物.md "wikilink")
      - [多儿国家级自然保护区](../Page/多儿.md "wikilink")

<!-- end list -->

  - [青海](../Page/青海.md "wikilink")
      - [隆宝国家级自然保护区](../Page/隆宝.md "wikilink")
      - [青海湖国家级自然保护区](../Page/青海湖.md "wikilink")
      - [可可西里国家级自然保护区](../Page/可可西里.md "wikilink")
      - [循化孟达国家级自然保护区](../Page/循化孟达.md "wikilink")
      - [三江源国家级自然保护区](../Page/三江源.md "wikilink")
      - [柴达木梭梭林国家级自然保护区](../Page/柴达木梭梭林.md "wikilink")
      - [大通北川河源区国家级自然保护区](../Page/大通北川河源区.md "wikilink")

<!-- end list -->

  - [宁夏](../Page/宁夏.md "wikilink")
      - [贺兰山国家级自然保护区](../Page/贺兰山.md "wikilink")
      - [六盘山国家级自然保护区](../Page/六盘山.md "wikilink")
      - [沙坡头国家级自然保护区](../Page/沙坡头.md "wikilink")
      - [灵武白芨滩国家级自然保护区](../Page/灵武白芨滩.md "wikilink")
      - [罗山国家级自然保护区](../Page/罗山.md "wikilink")
      - [哈巴湖国家级自然保护区](../Page/哈巴湖.md "wikilink")
      - [云雾山国家级自然保护区](../Page/云雾山.md "wikilink")
      - [火石寨丹霞地貌国家级自然保护区](../Page/火石寨丹霞地貌.md "wikilink")
      - [南华山国家级自然保护区](../Page/海原南华山.md "wikilink")

<!-- end list -->

  - [新疆](../Page/新疆.md "wikilink")
      - [阿尔金山国家级自然保护区](../Page/阿尔金山国家级自然保护区.md "wikilink")
      - [哈纳斯国家级自然保护区](../Page/哈纳斯.md "wikilink")
      - [巴音布鲁克国家级自然保护区](../Page/巴音布鲁克.md "wikilink")
      - [西天山国家级自然保护区](../Page/西天山.md "wikilink")
      - [甘家湖梭梭林国家级自然保护区](../Page/甘家湖梭梭林.md "wikilink")
      - [托木尔峰国家级自然保护区](../Page/托木尔峰.md "wikilink")
      - [罗布泊野骆驼国家级自然保护区](../Page/罗布泊野骆驼.md "wikilink")
      - [塔里木胡杨国家级自然保护区](../Page/塔里木胡杨.md "wikilink")
      - [艾比湖湿地国家级自然保护区](../Page/艾比湖湿地.md "wikilink")
      - [布尔根河狸国家级自然保护区](../Page/布尔根河狸.md "wikilink")
      - [巴尔鲁克山国家级自然保护区](../Page/巴尔鲁克山.md "wikilink")
      - [霍城四爪陆龟国家级自然保护区](../Page/霍城四爪陆龟.md "wikilink")
      - [伊犁小叶白蜡国家级自然保护区](../Page/伊犁小叶白蜡.md "wikilink")
      - [阿勒泰科克苏湿地国家级自然保护区](../Page/科克苏湿地.md "wikilink")
      - [温泉新疆北鲵国家级自然保护区](../Page/温泉新疆北鲵国家级自然保护区.md "wikilink")

注：标“\*”者，为跨省（区、市）国家级自然保护区。

## 参考文献

## 外部链接

  - [中国国家级自然保护区网](https://web.archive.org/web/20160407094506/http://www.nre.cn/)
  - [中国自然保护区资源平台](http://www.papc.cn)

## 参见

  - [中國生物圈保護區網路](../Page/中國生物圈保護區網路.md "wikilink")
  - [中国国家公园](../Page/中国国家公园.md "wikilink")

{{-}}

[中国国家级自然保护区](../Category/中国国家级自然保护区.md "wikilink")
[Category:中华人民共和国地理列表](../Category/中华人民共和国地理列表.md "wikilink")
[Category:中文地名索引](../Category/中文地名索引.md "wikilink")