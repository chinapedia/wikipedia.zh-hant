**瓜氨酸**(citrulline)是一種α[氨基酸](../Page/氨基酸.md "wikilink")，名字是由首先抽取出瓜氨酸的[西瓜而來](../Page/西瓜.md "wikilink")。

## 合成

瓜氨酸是從[鳥氨酸及](../Page/鳥氨酸.md "wikilink")[胺基甲醯磷酸鹽在](../Page/胺基甲醯磷酸鹽.md "wikilink")[尿素循環中生成](../Page/尿素循環.md "wikilink")，或是透過[一氧化氮合酶](../Page/一氧化氮合酶.md "wikilink")（NOS）催化生成[精氨酸的副產物](../Page/精氨酸.md "wikilink")。首先，精氨酸會被[氧化為N](../Page/氧化.md "wikilink")-羥基-精氨酸，再行氧化成瓜氨酸並釋出[一氧化氮](../Page/一氧化氮.md "wikilink")。

## 功用

當瓜氨酸仍然是[胺基酸時](../Page/胺基酸.md "wikilink")，由於它不是為[DNA而編碼](../Page/DNA.md "wikilink")，所以是不會在[蛋白質生物合成中嵌入](../Page/蛋白質生物合成.md "wikilink")[蛋白質內](../Page/蛋白質.md "wikilink")，不過仍然有部份蛋白質含有瓜氨酸。這些瓜氨酸是由[胜肽精胺酸亞氨酶](../Page/胜肽精胺酸亞氨酶.md "wikilink")（PAD）所產生，在[瓜氨化的過程中將](../Page/瓜氨化.md "wikilink")[精氨酸轉化為瓜氨酸](../Page/精氨酸.md "wikilink")。一般含有瓜氨酸的蛋白質包括有[人腦髓鞘鹼性蛋白](../Page/人腦髓鞘鹼性蛋白.md "wikilink")（MBP）、[聚角蛋白微絲蛋白及一些](../Page/聚角蛋白微絲蛋白.md "wikilink")[組蛋白](../Page/組蛋白.md "wikilink")。如[纖維素及](../Page/纖維素.md "wikilink")[波形蛋白能從](../Page/波形蛋白.md "wikilink")[細胞死亡及組織](../Page/細胞.md "wikilink")[發炎中被瓜氨化](../Page/炎症.md "wikilink")。

患有[類風濕性關節炎的病人](../Page/類風濕性關節炎.md "wikilink")（約80%）會發展一套免疫反應對抗帶有瓜氨酸的蛋白質。雖然這種反應機制的起因不明，但察覺抗體可以幫助這類病的診斷。

## 相關條目

  - [瓜氨酸血症](../Page/瓜氨酸血症.md "wikilink")

[分類:α-胺基酸](../Page/分類:α-胺基酸.md "wikilink")
[分類:脲](../Page/分類:脲.md "wikilink")
[分類:尿素循环](../Page/分類:尿素循环.md "wikilink")