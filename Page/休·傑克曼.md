**休·麥可·傑克曼**（，）\[1\]是一名[澳洲](../Page/澳洲.md "wikilink")[演員](../Page/演員.md "wikilink")、[歌手及](../Page/歌手.md "wikilink")[製片人](../Page/製片人.md "wikilink")。他在「[X戰警系列電影](../Page/X戰警系列電影.md "wikilink")」中長期飾演「[金鋼狼](../Page/金鋼狼.md "wikilink")」一角而廣為人知，他在2012年電影《[悲慘世界](../Page/悲慘世界_\(2012年電影\).md "wikilink")》裡的演出，使其首次获得[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")[最佳男主角獎提名](../Page/奧斯卡最佳男主角獎.md "wikilink")，並赢得[金球獎最佳音樂及喜劇類電影男主角](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")。

## 生平

### 早年

休·傑克曼1968年出生於[澳州](../Page/澳州.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")。1987年在英國Uppingham
High
School當體育助教。\[2\]1991年畢業於[雪梨科技大學傳播學系](../Page/雪梨科技大學.md "wikilink")；1994年畢業於[伊迪斯科文大學的](../Page/伊迪斯科文大學.md "wikilink")[西澳表演藝術學院](../Page/西澳表演藝術學院.md "wikilink")（師從其創院院長Geoff
Gibbs）\[3\]。

### 演藝事業

傑克曼在各類電影中的角色獲得國際認可。於[百老匯劇院](../Page/百老匯劇院.md "wikilink")，傑克曼因他在《》中的表現而榮獲[東尼獎](../Page/東尼獎.md "wikilink")。他曾擔任過四次東尼獎的主持人，並在其中一次贏得了[艾美獎](../Page/艾美獎.md "wikilink")。傑克曼還於在2009年2月22日舉行的[第81届奥斯卡金像奖上擔任主持人](../Page/第81届奥斯卡金像奖.md "wikilink")。他在[第81届奥斯卡金像奖擔當](../Page/第81届奥斯卡金像奖.md "wikilink")[主持人](../Page/主持人.md "wikilink")，大獲好評。特別在頒獎禮的表演節目"Musical
is back"，與[碧昂絲·諾利斯](../Page/碧昂絲·諾利斯.md "wikilink")（Beyoncé
Knowles）合作，其[歌唱及](../Page/歌唱.md "wikilink")[舞蹈各項演出均受讚賞](../Page/舞蹈.md "wikilink")。

他在2012年的[音乐剧](../Page/音乐剧.md "wikilink")[电影](../Page/电影.md "wikilink")《[悲惨世界](../Page/悲惨世界_\(2012年电影\).md "wikilink")》饰演的[尚萬強一角获得许多好评](../Page/尚萬強.md "wikilink")，并为他赢得[第70届金球奖](../Page/第70届金球奖.md "wikilink")[最佳音樂及喜劇類電影男主角獎](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")，以及获得[第66届英国电影学院奖](../Page/第66届英国电影学院奖.md "wikilink")[最佳男主角提名和](../Page/英國電影學院獎最佳男主角.md "wikilink")[第85届奥斯卡金像奖](../Page/第85届奥斯卡金像奖.md "wikilink")[最佳男主角獎提名](../Page/奧斯卡最佳男主角獎.md "wikilink")。

2013年9月他被[西班牙](../Page/西班牙.md "wikilink")[圣塞巴斯蒂安电影节授予终身成就奖](../Page/圣塞巴斯蒂安电影节.md "wikilink")。\[4\]

## 私人生活

[Hugh_Jackman_with_wife_Deborra-Lee_Furness_in_India.jpg](https://zh.wikipedia.org/wiki/File:Hugh_Jackman_with_wife_Deborra-Lee_Furness_in_India.jpg "fig:Hugh_Jackman_with_wife_Deborra-Lee_Furness_in_India.jpg")[孟买国际机场](../Page/孟买国际机场.md "wikilink")\]\]
1996年4月11日他與比自己年長13歲的結婚，黛博拉兩度流產\[5\]後領養了兩名孩子，分別是兒子Oscar
Maximillian（2000年5月15日出生）及女兒Ava Eliot（2005年7月10日出生）。

## 作品列表

### 電影

<table>
<thead>
<tr class="header">
<th><p>標題</p></th>
<th><p>年份</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
<th><p>參考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《》</p></td>
<td><p>1999</p></td>
<td><p>Wace</p></td>
<td></td>
<td><p>[6]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>1999</p></td>
<td><p>Jack Willis</p></td>
<td></td>
<td><p>[7]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/X戰警_(電影).md" title="wikilink">X戰警</a>》</p></td>
<td><p>2000</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td></td>
<td><p>[8]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/心花怒放_(電影).md" title="wikilink">心花怒放</a>》</p></td>
<td><p>2001</p></td>
<td><p>Eddie</p></td>
<td></td>
<td><p>[9]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/劍魚_(電影).md" title="wikilink">劍魚</a>》</p></td>
<td><p>2001</p></td>
<td><p>Stanley Jobson</p></td>
<td></td>
<td><p>[10]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/穿越時空愛上你.md" title="wikilink">穿越時空愛上你</a>》</p></td>
<td><p>2001</p></td>
<td><p>Leopold</p></td>
<td></td>
<td><p>[11]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/X戰警2.md" title="wikilink">X戰警2</a>》</p></td>
<td><p>2003</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td></td>
<td><p>[12]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>2004</p></td>
<td><p>Eric Ringer</p></td>
<td></td>
<td><p>[13]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/凡赫辛_(電影).md" title="wikilink">凡赫辛</a>》</p></td>
<td><p>2004</p></td>
<td><p>Gabriel Van Helsing</p></td>
<td></td>
<td><p>[14]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>2004</p></td>
<td><p>Gabriel Van Helsing</p></td>
<td><p>配音</p></td>
<td><p>[15]</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>2005</p></td>
<td><p>Roger</p></td>
<td><p>片段：「Standing Room Only」</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/快樂腳.md" title="wikilink">快樂腳</a>》</p></td>
<td><p>2006</p></td>
<td><p>Memphis</p></td>
<td><p>配音</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/鼠國流浪記.md" title="wikilink">鼠國流浪記</a>》</p></td>
<td><p>2006</p></td>
<td><p>Roddy</p></td>
<td><p>配音</p></td>
<td><p>[18]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/頂尖對決.md" title="wikilink">頂尖對決</a>》</p></td>
<td><p>2006</p></td>
<td><p>Robert Angier</p></td>
<td></td>
<td><p>[19]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/真愛永恆.md" title="wikilink">真愛永恆</a>》</p></td>
<td><p>2006</p></td>
<td><p>Tomas／Tommy／Tom Creo</p></td>
<td></td>
<td><p>[20]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>2006</p></td>
<td><p>Peter Lyman</p></td>
<td></td>
<td><p>[21]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/X戰警：最後戰役.md" title="wikilink">X戰警：最後戰役</a>》</p></td>
<td><p>2006</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td></td>
<td><p>[22]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>2008</p></td>
<td><p>Wyatt Bose</p></td>
<td><p>兼監製</p></td>
<td><p>[23]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/澳大利亞_(電影).md" title="wikilink">澳大利亞</a>》</p></td>
<td><p>2008</p></td>
<td><p>放牧人</p></td>
<td></td>
<td><p>[24]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>2008</p></td>
<td><p>旁白</p></td>
<td><p>紀錄片</p></td>
<td><p>[25]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/X戰警：金鋼狼.md" title="wikilink">X戰警：金鋼狼</a>》</p></td>
<td><p>2009</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">詹姆士·豪利特／羅根／金鋼狼</a></p></td>
<td><p>兼監製</p></td>
<td><p>[26]<br />
[27]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/X戰警：第一戰.md" title="wikilink">X戰警：第一戰</a>》</p></td>
<td><p>2011</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td><p>未掛名客串</p></td>
<td><p>[28]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/雪花與秘扇.md" title="wikilink">雪花與秘扇</a>》</p></td>
<td><p>2011</p></td>
<td><p>Arthur</p></td>
<td></td>
<td><p>[29]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/鋼鐵擂台.md" title="wikilink">鋼鐵擂台</a>》</p></td>
<td><p>2011</p></td>
<td><p>Charlie Kenton</p></td>
<td></td>
<td><p>[30]</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p>2012</p></td>
<td><p>Boyd Bolton</p></td>
<td></td>
<td><p>[31]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/捍衛聯盟.md" title="wikilink">捍衛聯盟</a>》</p></td>
<td><p>2012</p></td>
<td><p>Bunnymund／The Easter Bunny</p></td>
<td><p>配音</p></td>
<td><p>[32]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/悲慘世界_(2012年電影).md" title="wikilink">悲慘世界</a>》</p></td>
<td><p>2012</p></td>
<td><p><a href="../Page/尚萬強.md" title="wikilink">尚萬強</a></p></td>
<td></td>
<td><p>[33]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/激愛543.md" title="wikilink">激愛543</a>》</p></td>
<td><p>2013</p></td>
<td><p>Davis</p></td>
<td></td>
<td><p>[34]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/金鋼狼：武士之戰.md" title="wikilink">金鋼狼：武士之戰</a>》</p></td>
<td><p>2013</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td><p>兼監製</p></td>
<td><p>[35]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/私法爭鋒.md" title="wikilink">私法爭鋒</a>》</p></td>
<td><p>2013</p></td>
<td><p>Keller Dover</p></td>
<td></td>
<td><p>[36]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/X戰警：未來昔日.md" title="wikilink">X戰警：未來昔日</a>》</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td></td>
<td><p>[37]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/博物館驚魂夜3.md" title="wikilink">博物館驚魂夜3</a>》</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/亞瑟王.md" title="wikilink">亞瑟王</a>／他自己</p></td>
<td><p>未掛名客串</p></td>
<td><p>[38]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/成人世界.md" title="wikilink">成人世界</a>》</p></td>
<td><p>2015</p></td>
<td><p>Vincent Moore</p></td>
<td></td>
<td><p>[39]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/我們的故事未完待續.md" title="wikilink">我們的故事未完待續</a>》</p></td>
<td><p>2015</p></td>
<td><p>他自己</p></td>
<td><p>僅聲音</p></td>
<td><p>[40]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/潘恩：航向夢幻島.md" title="wikilink">潘恩：航向夢幻島</a>》</p></td>
<td><p>2015</p></td>
<td><p><a href="../Page/黑鬍子.md" title="wikilink">黑鬍子</a></p></td>
<td></td>
<td><p>[41]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/飛躍奇蹟.md" title="wikilink">飛躍奇蹟</a>》</p></td>
<td><p>2016</p></td>
<td><p>Bronson Peary</p></td>
<td></td>
<td><p>[42]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/X戰警：天啟.md" title="wikilink">X戰警：天啟</a>》</p></td>
<td><p>2016</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／X武器／金鋼狼</a></p></td>
<td><p>未掛名客串</p></td>
<td><p>[43]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/羅根_(電影).md" title="wikilink">羅根</a>》</p></td>
<td><p>2017</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">詹姆士·豪利特／羅根／金鋼狼／X-24</a></p></td>
<td></td>
<td><p>[44]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/大娛樂家_(電影).md" title="wikilink">大娛樂家</a>》</p></td>
<td><p>2017</p></td>
<td><p><a href="../Page/費尼爾司·泰勒·巴納姆.md" title="wikilink">菲尼阿斯·巴納姆</a></p></td>
<td></td>
<td><p>[45]</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/死侍2.md" title="wikilink">死侍2</a>》</p></td>
<td><p>2018</p></td>
<td><p><a href="../Page/金鋼狼.md" title="wikilink">羅根／金鋼狼</a></p></td>
<td><p>客串</p></td>
<td><p>[46]</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/爆料世代.md" title="wikilink">爆料世代</a>》</p></td>
<td><p>2018</p></td>
<td></td>
<td></td>
<td><p>[47]</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p>2019</p></td>
<td><p>Sir Lionel Frost</p></td>
<td><p>配音</p></td>
<td><p>[48]</p></td>
</tr>
</tbody>
</table>

### 電視劇

| 標題                                               | 年份   | 角色                                           | 附註                                                                                                |
| ------------------------------------------------ | ---- | -------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| 《》                                               | 1994 | Charles "Chicka" McCray                      | 單集：「Win, Lose and Draw」                                                                           |
| 《》                                               | 1995 | Kevin Jones\[49\]                            | 主要演員；10集                                                                                          |
| 《》                                               | 1995 | Brady Jackson                                | 單集：「Just Desserts」                                                                                |
| 《》                                               | 1996 | Duncan Jones                                 | 5集                                                                                                |
| 《[週六夜現場](../Page/週六夜現場.md "wikilink")》           | 2001 | 他自己／主持人                                      | 單集：「Hugh Jackman／[Mick Jagger](../Page/米克·傑格.md "wikilink")」                                      |
| 《》                                               | 2003 | 他自己／主持人\[50\]                                | 電視特別節目                                                                                            |
| 《》                                               | 2004 | 他自己／主持人\[51\]                                | 電視特別節目                                                                                            |
| 《》                                               | 2005 | 他自己／主持人\[52\]                                | 電視特別節目                                                                                            |
| 《》                                               | 2007 | Nicky Fontana\[53\]                          | 兼執行製片人；單集：「試播集」                                                                                   |
| 《[第81屆奧斯卡金像獎](../Page/第81屆奧斯卡金像獎.md "wikilink")》 | 2009 | 他自己／主持人\[54\]                                | 電視特別節目                                                                                            |
| 《[芝麻街](../Page/芝麻街.md "wikilink")》               | 2010 | 他自己                                          | 單集：「Tribute to Number Seven」                                                                      |
| 《[WWE Raw](../Page/WWE_Raw.md "wikilink")》       | 2011 | 他自己／主持人                                      | 單集：「955」                                                                                          |
| 《[週六夜現場](../Page/週六夜現場.md "wikilink")》           | 2011 | [丹尼爾·雷德克里夫](../Page/丹尼爾·雷德克里夫.md "wikilink") | 單集：「[Ben Stiller](../Page/班·史提勒.md "wikilink")／[Foster the People](../Page/擁抱人群樂團.md "wikilink")」 |
| 《[頂級跑車秀](../Page/頂級跑車秀.md "wikilink")》           | 2013 | 他自己                                          | 單集：「」                                                                                             |
| 《》                                               | 2013 | 他自己／主持人                                      | 電視特別節目                                                                                            |
| 《[WWE Raw](../Page/WWE_Raw.md "wikilink")》       | 2014 | 他自己／主持人                                      | 單集：「1,091」                                                                                        |
| 《》                                               | 2014 | 他自己／主持人                                      | 電視特別節目                                                                                            |
|                                                  |      |                                              |                                                                                                   |

## 参考资料

## 外部連結

  -
  -
  -
  -
  -
  -
  -
  - [休·傑克曼個人新浪微博](http://weibo.com/hughjackman)

  - ['A Steady Rain' Rules Broadway – opening night
    blog](http://www.broadway.tv/broadway-features-reviews/a-steady-rain-rules-broadway)
    at [broadway.tv](http://www.broadway.tv)

  - [Jackman, Hugh](http://nla.gov.au/nla.party-1446737) National
    Library of Australia, *Trove, People and Organisation* record for
    Hugh Jackman

[Category:悉尼科技大学校友](../Category/悉尼科技大学校友.md "wikilink")
[Category:伊迪斯科文大學校友](../Category/伊迪斯科文大學校友.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:澳大利亞男演員](../Category/澳大利亞男演員.md "wikilink")
[Category:金球奖最佳音乐或喜剧男主角奖得主](../Category/金球奖最佳音乐或喜剧男主角奖得主.md "wikilink")
[Category:雪梨人](../Category/雪梨人.md "wikilink")
[Category:東尼獎得主](../Category/東尼獎得主.md "wikilink")
[Category:土星獎獲得者](../Category/土星獎獲得者.md "wikilink")
[Category:澳大利亞電影演員](../Category/澳大利亞電影演員.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:艾美獎獲獎者](../Category/艾美獎獲獎者.md "wikilink")
[Category:澳大利亞舞台演員](../Category/澳大利亞舞台演員.md "wikilink")

1.
2.
3.  [Jackman back as Boy From
    WAAPA](http://www.thewest.com.au/default.aspx?MenuID=77&ContentID=8095)
4.  [北美票房：近期资讯汇总及下周预告](http://ent.sina.com.cn/m/f/2013-09-30/ba4017790.shtml)
5.
6.
7.
8.
9.   Roger
    Ebert|url=[http://www.rogerebert.com/reviews/someone-like-you-2001|accessdate=2016-12-12|work=Chicago](http://www.rogerebert.com/reviews/someone-like-you-2001%7Caccessdate=2016-12-12%7Cwork=Chicago)
    Sun-Times|date=2001-03-30}}
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.  Roger
    Ebert|url=[http://www.rogerebert.com/reviews/snow-flower-and-the-secret-fan-2011|accessdate=2016-12-13|work=Chicago](http://www.rogerebert.com/reviews/snow-flower-and-the-secret-fan-2011%7Caccessdate=2016-12-13%7Cwork=Chicago)
    Sun-Times|date=2011-07-20}}
30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49. "Biography Today 2010, p. 87"
50.
51. Gans, Andrew and Hernandez, Ernio. ["Tony Host Jackman Nabs an Emmy;
    Danner, Newman and Alexander Win,
    Too"](http://www.playbill.com/news/article/95157-Tony-Host-Jackman-Nabs-an-Emmy-Danner-Newman-and-Alexander-Win-Too)
    playbill.com, September 19, 2005
52.
53.
54.