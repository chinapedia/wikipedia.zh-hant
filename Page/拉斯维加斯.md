Las Vegas Springs Preserve | photo2b = Two Towers Las
Vegas.jpg{{\!}}Stratosphere Tower | photo3a ={{\!}}World Market Center |
photo3b = Lou Ruovo Center for Brain Health.jpg{{\!}}Lou Ruvo Center for
Brain Health | photo4a = Clark County Government Center.png{{\!}}Clark
County Government Center | spacing = 2 | position = center |
color_border = white | color = white | size = 266 | foot_montage =
由最頂順時針方向：天際線、[同溫層酒店](../Page/同溫層酒店.md "wikilink")、、、、 }} |
imagesize = 300px | image_caption = | image_flag = Flag of Las Vegas,
Nevada.svg | image_seal = Las_Vegas_seal.jpg | image_map = Clark
County Nevada Incorporated Areas Las Vegas highlighted.svg | mapsize =
250px | map_caption =
拉斯維加斯於[克拉克縣的位置](../Page/克拉克縣_\(內華達州\).md "wikilink")
| pushpin_map = USA | pushpin_map_caption = 拉斯維加斯於美國的位置 |
subdivision_type = 國家 | subdivision_name = 美國 | subdivision_type1 =
[美国州份](../Page/美国州份.md "wikilink") | subdivision_name1 =
[内华达州](../Page/内华达州.md "wikilink") | subdivision_type2 =
[州行政區劃](../Page/内华达州行政区划.md "wikilink") | subdivision_name2 =
[克拉克縣](../Page/克拉克縣_\(內華達州\).md "wikilink") | government_type =
[議會-經理制政府](../Page/議會-經理制政府.md "wikilink") |leader_title =
[市长](../Page/市长.md "wikilink") |leader_name =
（[無黨籍](../Page/無黨籍.md "wikilink")） |leader_title1 =
|leader_name1 =
[史蒂夫·羅斯](../Page/Steve_Ross_\(politician\).md "wikilink")（[民主党](../Page/民主党_\(美国\).md "wikilink")）
|leader_title2 =  |leader_name2 = Betsy Fretwell | area_magnitude = 1
E9 | established_title = 建立 | established_date = 1905年5月15日 |
established_title1 = 建制 | established_date1 = 1911年3月16日 |
area_total_sq_mi = 135.8 | area_land_sq_mi = 135.8 |
area_water_sq_mi = 0.05 | population_as_of =
[2010年美國人口普查](../Page/2010年美國人口普查.md "wikilink") |
population_footnotes = \[1\] | population_total = 583756 |
population_urban = 2,211,315 | population_metro = 2,248,390 |
population_blank1_title =  | population_blank1 = 2,362,015 |
population_note = | population_density_sq_mi = auto | timezone =
[PST](../Page/太平洋时区.md "wikilink") | utc_offset = −8 | timezone_DST =
[PDT](../Page/太平洋時區.md "wikilink") | utc_offset_DST = −7 |
postal_code_type = [美國郵區編號](../Page/美國郵區編號.md "wikilink") |
postal_code = | area_code =  | coordinates =  | elevation_ft = 2001 |
elevation_m = 610 | website =  | blank_name = [聯邦資料
處理標準](../Page/聯邦資料處理標準.md "wikilink") | blank_info = 32-40000 |
blank1_name = [地名信息系统](../Page/地名信息系统.md "wikilink") | blank1_info =
0847388 | footnotes = |pop_est_as_of = 2017 |pop_est_footnotes =
\[2\] |population_est = 648,224 |population_density_km2 = auto
|unit_pref = Imperial }}
**拉斯維加斯**（）是[美國](../Page/美國.md "wikilink")[內華達州人口最多的](../Page/內華達州.md "wikilink")[城市](../Page/城市.md "wikilink")，也是内華達州[克拉克縣的](../Page/克拉克縣_\(內華達州\).md "wikilink")[縣治](../Page/縣治.md "wikilink")，有着以[赌博业为中心庞大的旅遊](../Page/赌博业.md "wikilink")、購物、度假产业，是世界知名的度假地之一。2005年人口普查局的官方數字為59萬1536人。2005年，此城的都會區人口為191萬2654人\[3\]。

## 历史

拉斯維加斯建于1854年，由当时在美国西部的[摩门教徒建成](../Page/摩门教.md "wikilink")，後來摩门教徒迁走，美国政府使其变成一个兵站。“拉斯維加斯”的名称是[西班牙人起的](../Page/西班牙.md "wikilink")，意思是“肥沃的山谷”，因为拉斯維加斯是周围荒凉的[沙漠和半沙漠地带唯一有泉水的](../Page/沙漠.md "wikilink")[绿洲](../Page/绿洲.md "wikilink")，由于有泉水，逐渐成为来往公路的驿站和铁路的中转站。它开埠于1905年。[内华达州发现金银矿后](../Page/内华达州.md "wikilink")，大量淘金者涌入，拉斯維加斯开始繁荣，但如同西部各采矿城镇一样，一旦矿被采光就会被抛弃。在1931年[經濟大蕭條时期](../Page/經濟大蕭條.md "wikilink")，为了度过经济难关，[内华达州议会通过了赌博合法的议案](../Page/内华达州.md "wikilink")，拉斯維加斯成为一个[赌城](../Page/赌城.md "wikilink")，从此迅速崛起。同年兴建[胡佛水坝](../Page/胡佛水坝.md "wikilink")，工程于1935年完成。

赌城拉斯维加斯在很长时间被[黑手党控制](../Page/黑手党.md "wikilink")。1960年代末期亿万富翁[霍华德·休斯从](../Page/霍华德·休斯.md "wikilink")[黑手党手中收购了很多资产](../Page/黑手党.md "wikilink")，使拉斯维加斯摆脱了[黑手党的控制](../Page/黑手党.md "wikilink")。

## 经济

拉斯維加斯的主要经济支柱是[博彩业](../Page/賭博.md "wikilink")，由于赌场是个淘金碗，美国各地的大亨纷纷向拉斯維加斯投资興建赌场，甚至[日本的富豪](../Page/日本.md "wikilink")、[阿拉伯的王子](../Page/阿拉伯.md "wikilink")、著名[演员均来投资](../Page/演员.md "wikilink")。博彩业带动了旅游业、娱乐业的发展。为了使经济稳定发展，赌业公会实行严格的自律机制，对投资人进行严格的审查，对各赌场进行严格的监督；一旦发现问题，當事人將永遠從允許在當地經營赌业的名單中除名。为了吸引遊客，社会治安治理良好，对中了头奖的人，如果需要，由两名警察将其全程护送到在美国任何地方的家中。

雖然内华达州是美国唯一[性交易合法的州](../Page/性交易.md "wikilink")，但惟独[拉斯維加斯所在的](../Page/拉斯維加斯.md "wikilink")[克拉克县以方便维持社会治安為由禁止性交易](../Page/克拉克县.md "wikilink")。各个赌场的設計以金碧辉煌、奇形怪状的建筑物來吸引游客，所有赌场24小时开业。机场的班机通往世界各地，任何私人飞机都可以在拉斯維加斯降落。虽然后来内华达州的其他城市如[雷諾也发展赌博业](../Page/雷諾_\(內華達州\).md "wikilink")，但拉斯維加斯憑著交通便利和接近大城市[洛杉磯不遠距離的地理優勢](../Page/洛杉磯.md "wikilink")，仍然佔據着美國賭博業的頭把交椅。

拉斯維加斯蓬勃的賭博業帶動起了當地娛樂事業的發展，各國著名的歌舞團體及世界知名影星和歌星都以能登上拉斯維加斯的華麗大舞台而感到自豪，並因此吸引了眾多遊客進場欣賞其演出。1990年甚至[唐人街也在拉斯維加斯落户](../Page/唐人街.md "wikilink")，很快成為[亞洲裔美國人的聚集地](../Page/亞洲裔美國人.md "wikilink")，因為這些關係拉斯維加斯成為美國發展最迅速的城市之一，且當地賭博事業為該地區重要的經濟支柱。

拉斯維加斯的賭場主要集中在兩個區域。[拉斯維加斯舊城](../Page/拉斯維加斯舊城.md "wikilink")（Las Vegas
Downtown）是該市初期的發展原點，雖然曾經一度沒落，但近年來仍試圖透過許多街廓再造工作吸引觀光人潮回流；[拉斯維加斯大道](../Page/拉斯維加斯大道.md "wikilink")（Las
Vegas Strip）則是最新開幕的賭場與飯店匯聚之處，堪稱是賭城的門面。

## 地理

拉斯維加斯地处内华达州被荒凉的沙漠和半沙漠地带包围的山谷地区，雨量很少，冬季和夏季的溫差相當大。受[焚風](../Page/焚風.md "wikilink")[欽諾克風影響](../Page/欽諾克風.md "wikilink")，夏季極之酷热，最高溫度可達攝氏50度；冬季寒冷多风沙，偶然的暴雨会导致洪水。總面積293平方公里。

根据1981至2010年在麦卡伦国际机场的数据，日最低气温以下的平均日数为17
天，日最高气温低于的年均日数为9.5天；日高温以上的日数年均有155天，以上的有43天。\[4\]最冷月（12月）均温，极端最低气温（1963年1月13日）。\[5\]最热月（7月）均温，极端最高气温（上次出现于2013年6月30日）。\[6\]年均降水量约，年极端最少降水量为（1953年），最多为（1941年）。\[7\]无霜期平均有301天（2月5日至12月2日）。\[8\]雪为罕见，而只有24%的冬季出现可测量的降雪；冬季极端最多积累降雪量为（1948–49年）。\[9\]

## 交通

  - 自行開車

<!-- end list -->

  - 15號公路往東北，通往[猶他州](../Page/猶他州.md "wikilink")[聖喬治](../Page/圣乔治_\(犹他州\).md "wikilink")、[鹽湖城](../Page/鹽湖城.md "wikilink")。
  - 15號公路往西南，通往[加州](../Page/加州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")、[聖地亚哥](../Page/圣迭戈.md "wikilink")。
  - 93號公路往東南，通往[亞利桑那州](../Page/亞利桑那州.md "wikilink")[鳳凰城](../Page/鳳凰城.md "wikilink")。

<!-- end list -->

  - 巴士

<!-- end list -->

  - 是一套公共交通系統，提供巴士服務，包含整個拉斯維加斯、Henderson、北拉斯維加斯、以及山谷外圍的其他郊區。

<!-- end list -->

  - 拉斯维加斯-[麥卡倫國際機場](../Page/麥卡倫國際機場.md "wikilink")

## 人口

拉斯維加斯平均人口密度为每平方公里8547人，其中[白人](../Page/白人.md "wikilink")34.7%，墨西哥裔33.6%,
[黑人](../Page/黑人.md "wikilink")17.1%，[亞裔人](../Page/亞裔.md "wikilink")10.3%，[印第安人和阿拉斯加當地人](../Page/印第安人.md "wikilink")1.2%，除了在唐人街经商的黄种人外，在赌场中的发牌员有很多亞洲人。

## 教育

小学和中学公共教育主要位于克拉克县学区，该学区的人口位居全国第五。

## 文化

[The_Smith_Center_for_the_Performing_Arts_&_DISCOVERY_Children's_Museum.jpg](https://zh.wikipedia.org/wiki/File:The_Smith_Center_for_the_Performing_Arts_&_DISCOVERY_Children's_Museum.jpg "fig:The_Smith_Center_for_the_Performing_Arts_&_DISCOVERY_Children's_Museum.jpg")
這個城市有幾個博物館，包括、[黑幫博物館](../Page/黑幫博物館.md "wikilink")、、兒童探索博物館、內華達州博物館和老拉斯維加斯堡壘州立公園。

## 娱乐

拉斯维加斯有68个公园。拉斯维加斯总共有四个高尔夫球场：天使公园高尔夫俱乐部，沙漠之松高尔夫俱乐部，杜兰戈山高尔夫俱乐部和拉斯维加斯市高尔夫球场。它还有123個遊樂場，23个垒球场，10个美式足球场，44个足球场，10个dog
parks，6个社区中心，4个老人中心，109个冰鞋公园，6个游泳池等。

## 图片

<File:USA> LasVegas
Bellagio.jpg|[百樂宮酒店與](../Page/百樂宮酒店.md "wikilink")[凱薩宮酒店的夜景](../Page/凱薩宮酒店.md "wikilink")
<File:Las> Vegas strip.jpg|拉斯維加斯2003年鸟瞰 <File:Las> Vegas
Strip2.jpg|拉斯維加斯2007年鸟瞰
[File:NYNY2.jpg|賭城](File:NYNY2.jpg%7C賭城)[紐約-紐約酒店](../Page/紐約-紐約酒店.md "wikilink")

## 姊妹市

拉斯維加斯有若干姊妹市\[10\]。

  - [安山](../Page/安山.md "wikilink")

  - [濟州](../Page/濟州.md "wikilink")

  - [葫蘆島](../Page/葫蘆島市.md "wikilink")

  -
  - [佩爾尼克](../Page/佩爾尼克.md "wikilink")

  - [普吉](../Page/普吉.md "wikilink")

  -
  - [棉堡](../Page/棉堡.md "wikilink")

  -
  - [朱尼耶](../Page/朱尼耶.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [市政府网站](http://www.lasvegasnevada.gov)
  - [拉斯维加斯华人社区网](http://www.91vegas.com)
  - [赌城华人生活论坛](http://www.91vegas.net)
  - [拉斯维加斯华人论坛](http://www.chineseinvegas.com)
  - [拉斯维加斯旅游景点](http://www.lulutrip.com/scene/view/id-13)
  - [旅人行腳-拉斯維加斯](http://tw.traveleredge.com/USNP/CITY/Las_Vegas/)
  - [拉斯维加斯机场](http://www.mccarran.com)
  - [拉斯維加斯旅遊攻略](https://www.ifunvegas.com)

[Category:内华达州城市](../Category/内华达州城市.md "wikilink")
[拉斯維加斯](../Category/拉斯維加斯.md "wikilink")

1.
2.
3.  [Demographics](http://www.lasvegasnevada.gov/FactsStatistics/demographics.htm)

4.
5.
6.
7.
8.
9.
10. "[Online Directory: Nevada,
    USA](http://www.sister-cities.org/icrc/directory/usa/NV) ." *[Sister
    Cities International, Inc.](http://www.sister-cities.org/).*
    Retrieved on 2007年3月17日.