**川崎重工業株式會社**，簡稱**川崎重工**，是以[重工業作為主要業務的](../Page/重工業.md "wikilink")[日本](../Page/日本.md "wikilink")[企業](../Page/企业.md "wikilink")，業務範圍涵蓋[航空](../Page/航空.md "wikilink")、[太空](../Page/外层空间.md "wikilink")、[鐵路車輛](../Page/鐵路車輛.md "wikilink")、[摩托車](../Page/摩托車.md "wikilink")、[船舶](../Page/船.md "wikilink")、[機械以及各式各樣的設備](../Page/机械.md "wikilink")。

## 歷史

[Kobe_Crystal_Tower_(2006.10.22).jpg](https://zh.wikipedia.org/wiki/File:Kobe_Crystal_Tower_\(2006.10.22\).jpg "fig:Kobe_Crystal_Tower_(2006.10.22).jpg")
[川崎重工業株式会社_播磨工場P7225952.jpg](https://zh.wikipedia.org/wiki/File:川崎重工業株式会社_播磨工場P7225952.jpg "fig:川崎重工業株式会社_播磨工場P7225952.jpg")
[Kawasaki_Shipbuilding_Co._(headquarters_1).jpg](https://zh.wikipedia.org/wiki/File:Kawasaki_Shipbuilding_Co._\(headquarters_1\).jpg "fig:Kawasaki_Shipbuilding_Co._(headquarters_1).jpg")
川崎重工起家於[明治維新](../Page/明治維新.md "wikilink")。1878年[川崎正藏在](../Page/川崎正藏.md "wikilink")[大藏省的帮助下建立了川崎築地造船所](../Page/大藏省.md "wikilink")，1896年更名为，这就是川崎重工業的前身。至[大正時代的](../Page/大正時代.md "wikilink")[第一次世界大戰期間已有蓬勃的造船業](../Page/第一次世界大戰.md "wikilink")，經歷[昭和時代的](../Page/昭和時代.md "wikilink")[二戰](../Page/二戰.md "wikilink")、而戰後的日本經濟發展急劇增長以至日本近代史及工業史，川崎重工仍為日本軍工企業中的老字號之一。

川崎公司成立后借助政治关系，迅速成为日本最重要的重工业企业之一，1906年向日本军队交付了第一艘国产[潜艇](../Page/潜艇.md "wikilink")，并于当年建造了第一台国产蒸汽机车，1939年公司更名为川崎重工業株式会社。在二战期间该公司還向日本军队提供了飞燕战斗机、五式战斗机、一式运输机等軍事裝備，並建造著名的[榛名号战列舰以及](../Page/榛名号战列舰.md "wikilink")[加賀號航空母艦](../Page/加賀號航空母艦.md "wikilink")。在二战结束后，川崎重工仍然保持重要地位，業務的範圍在航空、太空、造船、铁路、发动机、摩托车、机器人等领域，代表了日本科技先进水準。

## 现状

川崎重工目前注册资本1043.28亿日圓，销售额为8899.63亿日圓，集团员工30653人，川崎重工集团下辖主要有车辆公司、航空宇宙公司、燃气轮机、机械公司、通用机公司、船舶公司等部门，产品涵盖海陆空各个领域，对军工訂單的程度只有低于10%，但是在航太部门最主要還是依靠军工訂單才得以维持。目前川崎重工是日本军工企業的重要成员之一，仅次于[三菱重工](../Page/三菱重工.md "wikilink")，是日本自卫队飞机和潜艇的主要製造廠商。

## 航空

### 第二次世界大戰结束前

#### 日本陸軍

  - [八八式偵察機](../Page/八八式偵察機.md "wikilink")

  - 八八式輕型轟炸機

  - [九二式戰鬥機](../Page/九二式戰鬥機.md "wikilink")

  - [九五式戰鬥機](../Page/九五式戰鬥機.md "wikilink")()

  - [九八式輕型俯衝轟炸機](../Page/九八式俯衝轟炸機.md "wikilink")（）

  - [九九式雙發輕轟炸機](../Page/九九式雙發輕轟炸機.md "wikilink")（）

  - （）

  - [二式複座戰鬥機](../Page/二式複座戰鬥機.md "wikilink")「屠龍」（）

  - [三式戰鬥機](../Page/三式戰鬥機.md "wikilink")「飛燕」（）

  - [五式戰鬥機](../Page/五式戰鬥機.md "wikilink")

  -
  - 特許生產

      - 乙式一型偵察機（）
      - 八七式重型轟炸機（Do.N）

#### 民用

  - 特許生產
      - 多尼爾Komet（彗星）

      - 多尼爾Do B「Merkur（水星）」

      - 「Wal（鯨魚）」

### 第二次世界大戰结束後

#### 自衛隊

  - [XC-2運輸機](../Page/XC-2.md "wikilink")
  - [C-1運輸機](../Page/C-1運輸機.md "wikilink")
  - [T-4教練機](../Page/T-4教練機.md "wikilink")
  - [OH-1](../Page/川崎OH-1.md "wikilink")
  - 特許生產
      - [T-33A](../Page/T-33教練機.md "wikilink")

      - [P-2J](../Page/P-2海王星巡邏機.md "wikilink")

      - [P-3C](../Page/P-3獵戶座海上巡邏機.md "wikilink")

      -
      - [CH-46 Sea Knight](../Page/CH-46海骑士直升机.md "wikilink")

      - [OH-6J/D](../Page/OH-6A直升機.md "wikilink")

      - [CH-47支奴干](../Page/CH-47支奴干.md "wikilink")

      - [AgustaWestland EH101](../Page/奧古斯塔偉士蘭AW101灰背隼直升機.md "wikilink")
  - 分工生產
      - [F-2戰鬥機](../Page/F-2戰鬥機.md "wikilink")

#### 民用

  - 分工生產
      - [H-2A運載火箭](../Page/H-2A運載火箭.md "wikilink")（運載部分）
      - [YS-11](../Page/YS-11.md "wikilink")
      - [波音767](../Page/波音767.md "wikilink")（共同開發）
      - [波音777](../Page/波音777.md "wikilink")（共同開發）
      - [波音787](../Page/波音787.md "wikilink")（主翼、中胴結合部及中央翼）
      - [空中巴士A321](../Page/空中客车A320#A321.md "wikilink")（中部胴体外皮）
      - [巴西航空工业](../Page/巴西航空工业公司.md "wikilink")[ERJ170](../Page/巴西航空工業E系列.md "wikilink")（協助開發）
      - 巴西航空工业ERJ175（協助開發）
      - 巴西航空工业ERJ190-100/200（協助開發）
      - 巴西航空工业ERJ195（協助開發）

### 出口及持續生產中的車輛

[2007KawasakiNinjaZX6R-001.jpg](https://zh.wikipedia.org/wiki/File:2007KawasakiNinjaZX6R-001.jpg "fig:2007KawasakiNinjaZX6R-001.jpg")
[Kawasaki_Versys_red.jpg](https://zh.wikipedia.org/wiki/File:Kawasaki_Versys_red.jpg "fig:Kawasaki_Versys_red.jpg")
[2010_Kawasaki_Concours_14_at_the_2009_Seattle_International_Motorcycle_Show_1.jpg](https://zh.wikipedia.org/wiki/File:2010_Kawasaki_Concours_14_at_the_2009_Seattle_International_Motorcycle_Show_1.jpg "fig:2010_Kawasaki_Concours_14_at_the_2009_Seattle_International_Motorcycle_Show_1.jpg")

  - KDX50/200
  - KX100/500
  - J300
  - KLX125/300R/250/400R
  - KAZE HIT/NEW
  - KLR 250/650
  - KAZE125
  - GTO
  - BOSS
  - KRR-SSR
  - ZX130
  - KLE500
  - VERSYS X-300 TOURER
  - VERSYS 650
  - W800
  - ER-6n/f
  - Versys
  - zephyr400
  - zephyr550
  - zephyr750
  - zephyr1100
  - zephyrx
  - ZR-7S
  - 1000GTR
  - 1400GTR

**VULCAN車系**（美式嘻皮車種）

  - VULCAN650
  - VN800
  - VN900
  - VN1500
  - VN1600
  - VN2000

**Z車系**

  - Z125
  - Z250/SL

<!-- end list -->

  - Z300
  - Z400
  - Z650
  - Z800
  - Z900/RS/RS CAFE
  - Z1000/R EDITION
  - ZZR600
  - ZZR1200
  - ZZR1400/ABS (ZX-14)
  - ZRX1200R/1100

**Ninja車系**(仿賽/跑車/旅跑)

  - Ninja ZX-250/R/SL
  - Ninja ZX-300R
  - Ninja 400
  - Ninja ZX-6R/RR
  - Ninja 650
  - Ninja ZX-10R
  - Ninja ZX-14R
  - Ninja 1000
  - Ninja H2
  - Ninja H2R
  - Ninja H2 SX SE

## 船舶

創立以來骨幹部門2002年再度分社化****，於2010年由川崎重工吸收合併。有[神戶船廠](../Page/神戶.md "wikilink")（主要生产军品）、[坂出船廠](../Page/坂出.md "wikilink")（民用）。

### 主要產品

  - 第二次世界大戰前（）
      - [榛名號戰艦](../Page/榛名號戰艦.md "wikilink")
      - [伊勢號戰艦](../Page/伊勢號戰艦.md "wikilink")
      - [飛鷹號航空母艦](../Page/飛鷹號航空母艦.md "wikilink")（原：出雲丸）
      - [加賀號航空母艦](../Page/加賀號航空母艦.md "wikilink")
      - [瑞鶴號航空母艦](../Page/瑞鶴號航空母艦.md "wikilink")
      - [大鳳號航空母艦](../Page/大鳳號航空母艦.md "wikilink")
      - [生駒號航空母艦](../Page/生駒號航空母艦.md "wikilink")（未完工）

<!-- end list -->

  - 第二次世界大戰後（）
      - [波音Jetfoil
        929](../Page/波音Jetfoil_929.md "wikilink")（噴射水翼船，[波音將此系列的生產專利轉讓予川崎重工](../Page/波音.md "wikilink")）
      - [水上電單車](../Page/水上電單車.md "wikilink")（）
      - [:汐潮級潛艇的SS](../Page/:汐潮級潛艇.md "wikilink")-574、SS-576、SS-578、SS-580、SS-582号艇
      - [:春潮級潛艇的SS](../Page/:春潮級潛艇.md "wikilink")-584、SS-586、TSS-3607（原SS-588）號艇
      - [:亲潮级潜艇的SS](../Page/:亲潮级潜艇.md "wikilink")-590、SS-592、SS-594、SS-596、SS-598、SS-600号艇
      - [:蒼龍級潛艇的SS](../Page/:蒼龍級潛艇.md "wikilink")-502、SS-504号艇
      - [貨櫃船](../Page/貨櫃船.md "wikilink")、[散貨船](../Page/散貨船.md "wikilink")、[滚装船](../Page/滚装船.md "wikilink")、[液化天然氣運輸船LNG等](../Page/液化天然氣運輸船LNG.md "wikilink")

## 鐵路車輛

當公司名稱還是川崎造船所的時代，於1906年開始生產鐵路車輛，包含客车与蒸汽机车。在昭和初期私鐵開始購入被稱為「川造型（川造形）」的特色鋼製車。1928年鐵路車輛部門獨立為川崎車輛，1969年與其他分社合併為一體的川崎重工業。

### JR

  - [新幹線](../Page/新幹線.md "wikilink")：[800系以外各型](../Page/新幹線800系電聯車.md "wikilink")[電聯車](../Page/電聯車.md "wikilink")（同時有製造800系的[轉向架](../Page/轉向架.md "wikilink")）
  - [國鐵](../Page/日本國有鐵道.md "wikilink")、JR在來線：
      - [日本國鐵103系電力動車組](../Page/日本國鐵103系電力動車組.md "wikilink")
      - [JR西日本125系電力動車組](../Page/JR西日本125系電力動車組.md "wikilink")（全車輛）
      - [日本國鐵211系電力動車組](../Page/日本國鐵211系電力動車組.md "wikilink")
      - [JR東日本E217系電力動車組](../Page/JR東日本E217系電力動車組.md "wikilink")
      - [JR西日本223系電力動車組](../Page/JR西日本223系電力動車組.md "wikilink")
      - [JR東日本E231系電力動車組](../Page/JR東日本E231系電力動車組.md "wikilink")
      - [JR東海383系電力動車組](../Page/JR東海383系電力動車組.md "wikilink")
      - [JR東日本E531系電力動車組](../Page/JR東日本E531系電力動車組.md "wikilink")
      - [JR東日本651系電力動車組](../Page/JR東日本651系電力動車組.md "wikilink")（全車輛）
      - [JR西日本681系電力動車組](../Page/JR西日本681系電力動車組.md "wikilink")
      - [JR西日本683系電力動車組](../Page/JR西日本683系電力動車組.md "wikilink")
      - [JR東日本701系電力動車組](../Page/JR東日本701系電力動車組.md "wikilink")
      - [JR東日本E721系電力動車組](../Page/JR東日本E721系電力動車組.md "wikilink")
      - [JR北海道731系電力動車組等](../Page/JR北海道731系電力動車組.md "wikilink")

### 大手私鐵

  - [小田急電鐵](../Page/小田急電鐵.md "wikilink")（[ロマンスカー](../Page/小田急ロマンスカー.md "wikilink")、單軌線車輛也製造）
  - **[京阪電氣鐵道](../Page/京阪電氣鐵道.md "wikilink")**（石山坂本線的600・700形除外）
  - **[京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")**：[2100形電車等](../Page/京急2100形電車.md "wikilink")
  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")（但是[08系電車除外](../Page/營團08系電車.md "wikilink")。[03系是只頭款車](../Page/營團03系電車.md "wikilink")）
  - **[西日本鐵道](../Page/西日本鐵道.md "wikilink")**：([600形以後所有車輛](../Page/西鐵600形電車_\(鐵道・2代\).md "wikilink"))
  - [阪神電氣鐵道](../Page/阪神電氣鐵道.md "wikilink")：[9000系電車](../Page/阪神9000系電車.md "wikilink")（所有車輛）、[5500系](../Page/阪神5500系電車.md "wikilink")（一部分）

### 中小私鐵、第三方鐵路

  - [泉北高速鐵道](../Page/大阪府都市開發泉北高速鐵道線.md "wikilink")
  - **[神戶電鐵](../Page/神戶電鐵.md "wikilink")**（所有車輛）
  - [埼玉高速鐵道](../Page/埼玉高速鐵道.md "wikilink")（所有車輛）
  - [埼玉新都市交通](../Page/埼玉新都市交通.md "wikilink")
  - **[山陽電氣鐵道](../Page/山陽電氣鐵道.md "wikilink")**（所有車輛）
  - **[首都圈新都市鐵道](../Page/首都圈新都市鐵道.md "wikilink")**：[TX-1000系電車](../Page/首都圈新都市鐵道TX-1000系電車.md "wikilink")（所有車輛）
  - [東京臨海高速鐵道](../Page/東京臨海高速鐵道.md "wikilink")（所有車輛）
  - [北神急行電鐵](../Page/北神急行電鐵.md "wikilink")（所有車輛）

[ST_SN5000_20061102_001.jpg](https://zh.wikipedia.org/wiki/File:ST_SN5000_20061102_001.jpg "fig:ST_SN5000_20061102_001.jpg")5000型電車。攝於南北線，南平岸車站,
2006年11月.\]\]

### 各市交通局

  - **[札幌市交通局](../Page/札幌市交通局.md "wikilink")**：地下鐵（所有車輛）、路面電車（一部分）
  - [仙台市交通局](../Page/仙台市交通局.md "wikilink")
  - [東京都交通局](../Page/東京都交通局.md "wikilink")（但是12-000系和10-300形除外）
  - [橫濱市交通局](../Page/橫濱市交通局.md "wikilink")：[10000形](../Page/橫濱市交通局10000形電車.md "wikilink")（所有車輛）
  - [大阪市交通局](../Page/大阪市交通局.md "wikilink")
  - [神戶市交通局](../Page/神戶市交通局.md "wikilink")（所有車輛）
  - [福岡市交通局](../Page/福岡市交通局.md "wikilink")（[1000系的第](../Page/福岡市交通局1000系電車.md "wikilink")09～15編成）

### 單軌電車、中運量系統（新交通）

  - [大阪單軌電車](../Page/大阪高速鐵道.md "wikilink")
  - [沖繩都市單軌電車](../Page/沖繩都市單軌電車.md "wikilink")（8\~12編組）
  - **[神戶新交通](../Page/神戶新交通.md "wikilink")**（全部車輛）
  - [北九州單軌電車](../Page/北九州高速鐵道.md "wikilink")（7\~10編組）
  - [多摩都市單軌電車](../Page/多摩都市單軌電車.md "wikilink")
  - 姬路市營單軌電車
  - [廣島高速交通](../Page/廣島高速交通.md "wikilink")

### 海外輸出

[C371_approaching_Platform_2,_CKS_Meml_Hall_20110225.jpg](https://zh.wikipedia.org/wiki/File:C371_approaching_Platform_2,_CKS_Meml_Hall_20110225.jpg "fig:C371_approaching_Platform_2,_CKS_Meml_Hall_20110225.jpg")的川崎重工製列車——371型\]\]
[325test.jpg](https://zh.wikipedia.org/wiki/File:325test.jpg "fig:325test.jpg")西鐵綫8卡川崎重工製SP1900型列車（D325/D326）\]\]

  - 新加坡陸運局
      - [C151型電車](../Page/新加坡地鐵C151電力動車組.md "wikilink")
      - C751B型電車
  - [台灣](../Page/台灣.md "wikilink")[鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")
      - TP32850型[三等普通客車](../Page/普通車.md "wikilink")
      - TPK32850型三等普通客守車
  - [台北捷運](../Page/台北捷運.md "wikilink")
      - [301型電車](../Page/台北捷運301型電車.md "wikilink")（由子公司美国铁路联合机车集团URC最后组装）

      -
      - [381型電車](../Page/台北捷運381型電車.md "wikilink")
  - [台灣高速鐵路](../Page/台灣高速鐵路.md "wikilink")
      - [700T型](../Page/台灣高鐵700T型電聯車.md "wikilink")[新幹線](../Page/新幹線.md "wikilink")[電聯車](../Page/電聯車.md "wikilink")（部分）
  - [桃園捷運](../Page/桃園捷運.md "wikilink")
      - [替代=桃園機場捷運直達車車廂](https://zh.wikipedia.org/wiki/File:桃園機場捷運直達車車廂.jpg "fig:替代=桃園機場捷運直達車車廂")[直達車](../Page/桃園捷運機場線直達車.md "wikilink")（Express）
      - [普通車](../Page/桃園捷運普通車.md "wikilink")（Commuter）
  - [中國鐵路總公司](../Page/中國鐵路總公司.md "wikilink")
      - [6K型电力机车](../Page/中国铁路6K型电力机车.md "wikilink")
      - [CRH2型电力动车组](../Page/和谐号CRH2型电力动车组.md "wikilink")（6組为散件形式付运、3组为日本完成，其余列车由[中国南车四方](../Page/中车四方.md "wikilink")-川崎重工生产）
  - [紐約市交通局](../Page/紐約市公共運輸局.md "wikilink")
  - [九廣鐵路公司](../Page/九廣鐵路公司.md "wikilink")（已經全數租予[港鐵公司](../Page/港鐵公司.md "wikilink")）
      - [輕鐵第2代電動列車](../Page/輕鐵川崎輕軌車輛.md "wikilink")（1071-1090，1201-1210）
      - [港鐵伊藤忠近畿川崎列車](../Page/港鐵伊藤忠近畿川崎列車.md "wikilink")（[港鐵](../Page/港鐵.md "wikilink")[西鐵綫其中](../Page/西鐵綫.md "wikilink")10列列車）
  - [港鐵公司](../Page/港鐵公司.md "wikilink")
      - [港鐵市區綫願景列車](../Page/港鐵市區綫願景列車.md "wikilink")（原先由[中車四方與川崎重工業合營競投生產合約](../Page/中車青島四方機車車輛.md "wikilink")，目前只由中車生產，但電子及動力系統仍由日本廠商提供）
  - 麻薩諸塞港灣運輸局
  - 巴拿馬運河廳
  - [台中捷運](../Page/台中捷運.md "wikilink")（[綠線其中](../Page/臺中捷運綠線.md "wikilink")9列列車）

## 工程機械

  - [压路机](../Page/压路机.md "wikilink")
  - [装载机](../Page/装载机.md "wikilink")
  - [自卸卡車](../Page/矿山自卸车.md "wikilink")
  - [推雪機](../Page/推雪機.md "wikilink")
  - [旋轉掃雪機](../Page/旋轉掃雪機.md "wikilink")
  - [全断面隧道掘进机](../Page/全断面隧道掘进机.md "wikilink")

## 關連項目

  - [神戶勝利船](../Page/神戶勝利船.md "wikilink")（原：川崎製鐵水島足球隊）

## 参见

川崎重工前副社长[高桥铁郎参与](../Page/高桥铁郎.md "wikilink")《[新历史教科书](../Page/新历史教科书.md "wikilink")》编纂会。2005年四月

## 外部連結

  - [川崎重工](http://global.kawasaki.com.cn/)
  - [Kawasaki Motors Taiwan 台崎重車](http://www.tw-kawasaki.com/)
  - [Kawasaki Group Channel -
    YouTube](https://www.youtube.com/user/KawasakiGroupChannel)

[川崎重工業](../Category/川崎重工業.md "wikilink")
[Category:日本軍事工業](../Category/日本軍事工業.md "wikilink")
[Category:日本機車公司](../Category/日本機車公司.md "wikilink")
[Category:日本飛機公司](../Category/日本飛機公司.md "wikilink")
[Category:飛行器發動機製造商](../Category/飛行器發動機製造商.md "wikilink")
[Category:日本軍工廠](../Category/日本軍工廠.md "wikilink")
[Category:日本鐵路車輛製造商](../Category/日本鐵路車輛製造商.md "wikilink")
[Category:日本造船廠](../Category/日本造船廠.md "wikilink")
[Category:第一勸銀集團](../Category/第一勸銀集團.md "wikilink")
[Category:1896年成立的公司](../Category/1896年成立的公司.md "wikilink")
[Category:兵庫縣公司](../Category/兵庫縣公司.md "wikilink") [Category:中央區
(神戶市)](../Category/中央區_\(神戶市\).md "wikilink")
[Category:1896年日本建立](../Category/1896年日本建立.md "wikilink")