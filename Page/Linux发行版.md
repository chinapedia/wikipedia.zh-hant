**Linux 发行版**（，也被叫做**GNU/Linux
发行版**），為一般使用者預先整合好的[Linux](../Page/Linux.md "wikilink")[作業系統及各種應用軟體](../Page/作業系統.md "wikilink")。一般使用者不需要重新[編譯](../Page/編譯.md "wikilink")，在直接安裝之後，只需要小幅度更改設定就可以使用，通常以[软件包管理系统來進行應用軟體的管理](../Page/软件包管理系统.md "wikilink")。Linux发行版通常包含了包括[桌面环境](../Page/桌面环境.md "wikilink")、[办公套件](../Page/办公套件.md "wikilink")、[媒体播放器](../Page/媒体播放器.md "wikilink")、[数据库等应用软件](../Page/数据库.md "wikilink")。这些[操作系统通常由Linux内核](../Page/操作系统.md "wikilink")、以及来自GNU计划的大量的[函式库](../Page/函式库.md "wikilink")，和基于[X
Window的图形界面](../Page/X_Window.md "wikilink")。有些发行版考虑到容量大小而没有预装 X
Window，而使用更加轻量级的软件，如：[busybox](../Page/busybox.md "wikilink"),
[uclibc](../Page/uclibc.md "wikilink") 或
[dietlibc](../Page/dietlibc.md "wikilink")。现在有超过300个Linux发行版([Linux发行版列表](../Page/Linux发行版列表.md "wikilink"))。大部分都正处于活跃的开发中，不断地改进。

由于大多数软件包是[自由软件和](../Page/自由软件.md "wikilink")[开源软件](../Page/开源软件.md "wikilink")，所以Linux发行版的形式多种多样——从功能齐全的桌面系统以及服务器系统到小型系统
(通常在[嵌入式设备](../Page/嵌入式.md "wikilink")，或者启动[软盘](../Page/软盘.md "wikilink"))。除了一些定制软件
(如安装和配置工具)，发行版通常只是将特定的应用软件安装在一堆[函式库和内核上](../Page/函式库.md "wikilink")，以满足特定使用者的需求。

这些发行版可以分为商业发行版，比如[Ubuntu](../Page/Ubuntu.md "wikilink")（[Canonical公司](../Page/Canonical公司.md "wikilink")）、[Fedora](../Page/Fedora.md "wikilink")（[Red
Hat](../Page/Red_Hat.md "wikilink")）、[openSUSE](../Page/openSUSE.md "wikilink")（[Novell](../Page/Novell.md "wikilink")）和[Mandriva
Linux](../Page/Mandriva_Linux.md "wikilink")；和社区发行版，它们由自由软件社区提供支持，如[Debian和](../Page/Debian.md "wikilink")[Gentoo](../Page/Gentoo_Linux.md "wikilink")；也有发行版既不是商业发行版也不是社区发行版，如[Slackware](../Page/Slackware.md "wikilink")。

## 历史

[Linux_Distribution_Timeline.svg](https://zh.wikipedia.org/wiki/File:Linux_Distribution_Timeline.svg "fig:Linux_Distribution_Timeline.svg")

早期的Linux发行版包括：

  - H J Lu的 "Boot-root"，2个磁盘搭配一个内核以及极少的工具；
  - [MCC Interim
    Linux](../Page/MCC_Interim_Linux.md "wikilink")，它在1992年2月通过[英国](../Page/英国.md "wikilink")[曼彻斯特大学的](../Page/曼彻斯特大学.md "wikilink")[FTP服务器向公众提供下载](../Page/FTP.md "wikilink")；
  - TAMU, 几乎同时由[Texas A\&M
    University的个人创造出来的](../Page/Texas_A&M_University.md "wikilink")
  - SLS ([Softlanding Linux
    System](../Page/Softlanding_Linux_System.md "wikilink"))；
  - [Yggdrasil
    Linux/GNU/X](../Page/Yggdrasil_Linux/GNU/X.md "wikilink")，这是第一个基于CD-ROM的Linux发行版。

SLS并没有得到很好的维护，所以[Patrick
Volkerding在](../Page/Patrick_Volkerding.md "wikilink")1993年7月16日发布了一个基于SLS的发行版，叫做[Slackware](../Page/Slackware.md "wikilink")。\[1\]
这是到现在仍然在发展的最老的发行版。

用户开始被从[DOS和](../Page/DOS.md "wikilink")[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")，[苹果机上的](../Page/苹果机.md "wikilink")[Mac
OS和](../Page/Mac_OS.md "wikilink")[专有的](../Page/专有.md "wikilink")[Unix版本吸引到自由的](../Page/Unix.md "wikilink")[Linux](../Page/Linux.md "wikilink")。最早的使用者来自熟悉Unix的公司和学校，它们喜欢Linux的自由、低价、以及开放的[源代码](../Page/源代码.md "wikilink")。

最初的发行版只是为了方便，但是现在即使Unix和Linux的专家也在使用Linux发行版。现在，Linux在服务器市场比桌面市场更流行的多，主要用于[网络和](../Page/网络.md "wikilink")[数据库服务器](../Page/数据库.md "wikilink")
(*参见* [LAMP](../Page/LAMP.md "wikilink"))。

## 组成

一个典型的Linux桌面发行版包括一个[Linux
内核](../Page/Linux_内核.md "wikilink")，来自[GNU的工具和库](../Page/GNU.md "wikilink")，和附加的软件、文档，还有一个窗口系统，[窗口管理器](../Page/窗口管理器.md "wikilink")，和一个[桌面环境](../Page/桌面环境.md "wikilink")。大部分包括的软件是[自由软件](../Page/自由软件.md "wikilink")/[开源软件](../Page/开源软件.md "wikilink")，它们同时以二进制可执行文件和[源代码形式发布](../Page/源代码.md "wikilink")，只要用户愿意，还允许修改和重新[编译源代码](../Page/编译.md "wikilink")。还有一些可能是专有软件而不提供源代码。

许多发行版像其他现代操作系统一样提供了一个安装系统*[定制](../Page/定制.md "wikilink")*的发行版像[Gentoo
Linux](../Page/Gentoo_Linux.md "wikilink"),
[T2](../Page/T2_SDE.md "wikilink") 和 [Linux From
Scratch](../Page/Linux_From_Scratch.md "wikilink")
提供所有软件的源代码以及最基本的内核、编译器、定制工具、安装工具等的可执行文件。安装程序针对使用者不同的[CPU编译成不同的文件](../Page/CPU.md "wikilink")。

### 软件包管理系统

发行版通常被分割成**软件包**。每个软件包都包含了一个特定的软件或服务。比如说处理[PNG图像格式的](../Page/PNG.md "wikilink")[库](../Page/库.md "wikilink")，一套字体，或者一个[浏览器](../Page/浏览器.md "wikilink")。

软件包通常是已[编译的](../Page/编译.md "wikilink")[机器码](../Page/机器码.md "wikilink")，并且由[软件包管理器安装和卸载](../Page/软件包管理器.md "wikilink")。每一个软件包都包含为包管理器提供的元数据，比如描述和版本，以及"[依赖](../Page/依赖.md "wikilink")"。包管理器能够使用这些元数据提供搜索功能、将软件包自动更新到最新版本，以及自动解决依赖。

虽然Linux发行版通常提供比其他专有操作系统多得多的软件包，但为了满足自己的特殊需求，管理员经常想要安装发行版中没有的软件。比如一个比发行版中更新版本的软件，或者其他作为发行版中提供的软件的替代品
(*例如*想要用[KDE替换](../Page/KDE.md "wikilink")[GNOME](../Page/GNOME.md "wikilink"))。如果软件包只提供源代码，那么就需要在本地进行编译，这就需要安装有编译器。但是如果软件是在本地安装的，本地系统的状态可能将会于软件包管理器数据库中的不一致，如果这样的话，软件包管理器将不能对这个软件包自动更新而需要由管理员手工操作。

发行版都会默认安装一些软件包，比如[操作系统内核和系统的重要组成部分](../Page/操作系统内核.md "wikilink")。有些发行版甚至能在安装时选择预装的软件包。这将使安装变得更复杂，特别是对于新手而言。对于特殊需求，软件包都要通过仔细地配置。为了使软件包之间的合作，或者为了安全，管理员经常会重新配置软件包。

最常见的软件包格式包括：[Debian的](../Page/Debian.md "wikilink")[DEB](../Page/DEB.md "wikilink")，以及[Fedora](../Page/Fedora.md "wikilink")
([Red Hat](../Page/Red_Hat.md "wikilink"))
的[RPM等](../Page/RPM.md "wikilink")。

## 类型和发展趋势

广义地说， Linux发行版可能是：

  - 商业或非商业的;
  - 给企业或家庭使用的;
  - 服务器，台式机或嵌入式设备专用的;
  - 针对普通用户或高级用户;
  - 为一般用途或特殊功能的机器定制的，例如防火墙，网络路由器和计算机集群;
  - 甚至是为特定的硬件和计算机架构设计的;
  - 针对特定的用户群体，例如国际化和本地化，或加入许多音乐制作或科学计算软件包。
  - 不同配置的安全性，可用性，便携性，或全面性
  - 支持不同类型的硬件

Linux发行版的多样性是由于不同用户和厂商的技术、哲学和用途差异。在宽松的自由软件许可证下，任何有足够的知识和兴趣的用户可以自定义现有的发行版，以适应自己的需要。

### Live CD

**[Live
CD](../Page/Live_CD.md "wikilink")**不需要安装而直接可以从可移动介质启动，它通常被用来演示和安装Linux。第一个Live
CD发行版是Knoppix，现在几乎所有主流Linux发行版的安装光盘都是Live CD，还有使用DVD作为介质的Live
DVD和装在U盘的Live USB。

### 流行的发行版

#### 基於Dpkg (Debian系)

商業發行版

  - [Ubuntu](../Page/Ubuntu.md "wikilink")，一个非常流行的桌面发行版，由[Canonical维护](../Page/Canonical.md "wikilink")。

社群發行版

  - [Debian](../Page/Debian.md "wikilink")，一个强烈信奉自由软件，并由志愿者维护的系统。
  - [Kubuntu](../Page/Kubuntu.md "wikilink"),
    使用[KDE桌面的](../Page/KDE.md "wikilink")[Ubuntu](../Page/Ubuntu.md "wikilink")。
  - [Linux
    Mint](../Page/Linux_Mint.md "wikilink")，从[Ubuntu衍生并与Ubuntu兼容的系统](../Page/Ubuntu.md "wikilink")。
  - [Knoppix](../Page/Knoppix.md "wikilink")，第一个[Live
    CD发行版](../Page/Live_CD.md "wikilink")，可以从[可移动介质运行](../Page/可移动介质.md "wikilink")，Debian的衍生版。
  - [OpenGEU](../Page/OpenGEU.md "wikilink")，Ubuntu的衍生版。
  - [Elementary
    OS](../Page/elementary_\(操作系统\).md "wikilink")：基於[Ubuntu](../Page/Ubuntu.md "wikilink")，介面酷似[Mac
    OS X](../Page/OS_X.md "wikilink")。
  - [gOS和其他](../Page/gOS.md "wikilink")[上网本用的系统](../Page/上网本.md "wikilink")。

#### 基於RPM (Red Hat系)

商業發行版

  - [Red Hat Enterprise
    Linux](../Page/Red_Hat_Enterprise_Linux.md "wikilink")，[Fedora的商业版](../Page/Fedora.md "wikilink")，由[Red
    Hat维护和提供技术支持](../Page/Red_Hat.md "wikilink")。
  - [openSUSE](../Page/openSUSE.md "wikilink")，最初由Slackware分离出来，现在由[Novell维护](../Page/Novell.md "wikilink")。

社群發行版

  - [Fedora](../Page/Fedora.md "wikilink")，是[Red
    Hat的社区版](../Page/Red_Hat.md "wikilink")，会经常引入新特性进行测试。
  - [PCLinuxOS](../Page/PCLinuxOS.md "wikilink")，[Mandriva的衍生版本](../Page/Mandriva_Linux.md "wikilink")，由社区维护的非常流行的发行版。
  - [CentOS](../Page/CentOS.md "wikilink")，从[Red
    Hat发展而来的发行版](../Page/Red_Hat.md "wikilink")，由[志愿者维护](../Page/志愿者.md "wikilink")，旨在提供开源的，并与Red
    Hat 100%兼容的系统。
  - [Mageia](../Page/Mageia.md "wikilink")，从[Mandriva发展而来的发行版](../Page/Mandriva_Linux.md "wikilink")。

#### 基于其他包格式

  - [ArchLinux](../Page/ArchLinux.md "wikilink")，一个基于[KISS](../Page/KISS.md "wikilink")（Keep
    It Simple and Stupid）的[滾動更新的操作系统](../Page/滾動更新.md "wikilink")。
  - [Chakra](../Page/Chakra_GNU/Linux.md "wikilink")，一個從[ArchLinux衍生出來](../Page/ArchLinux.md "wikilink")，只使用KDE桌面的[半滾動更新發行版](../Page/滾動更新.md "wikilink")。
  - [Gentoo](../Page/Gentoo_Linux.md "wikilink")，一个面向高级用户的发行版，所有软件的源代码需要自行编译。
  - [Slackware](../Page/Slackware.md "wikilink")，最早的发行版之一，1993年建立，由[Patrick
    J. Volkerding维护](../Page/Patrick_J._Volkerding.md "wikilink")。

在[DistroWatch网站可以看到很多发行版的排名和信息](../Page/DistroWatch.md "wikilink")。

## 常见发行版的截图

<File:Johndebian7.png>|[Debian
GNU/Linux](../Page/Debian_GNU/Linux.md "wikilink") 7.0 <File:Fedora> 20
GNOME.png|[Fedora](../Page/Fedora.md "wikilink") 20
Image:Gentoo12.0.jpg|[Gentoo Linux](../Page/Gentoo_Linux.md "wikilink")
12.0 Image:Mandriva Linux.png|[Mandriva
Linux](../Page/Mandriva_Linux.md "wikilink") 2009.0
<File:OpenSUSE12.2-KDE.png>|[OpenSUSE](../Page/OpenSUSE.md "wikilink")
12.2 Image:Slackware.png|[Slackware](../Page/Slackware.md "wikilink") 13
<File:Cinnamon> 1.6.7 Menu.png|[Linux
Mint](../Page/Linux_Mint.md "wikilink") 14 <File:Ubuntu> Desktop 12.10
Screenshot.png|[Ubuntu](../Page/Ubuntu.md "wikilink") 13.04
<File:PuppyLinux533> slacko de.png|[Puppy
Linux](../Page/Puppy_Linux.md "wikilink")5.3.3 <File:Sabayon>
Linux-5.2-GNOME.png|[Sabayon Linux](../Page/Sabayon_Linux.md "wikilink")
5.2 <File:Knoppix> 7.0.1.png|[KNOPPIX](../Page/KNOPPIX.md "wikilink")
7.0.1

## 参考文献

## 外部链接

  - [Distrowatch](https://distrowatch.com/)
  - [Distribution
    Reviews](http://www.linuxquestions.org/reviews/index.php)
  - [Hardware support by Linux
    distribution](https://web.archive.org/web/20090513105836/http://hardware4linux.info/)
  - [Linux Distribution
    Chooser](https://web.archive.org/web/20090523090806/http://www.zegeniestudios.net/ldc/)
    by [Zegenie Studios](../Page/Zegenie_Studios.md "wikilink")
  - [The Linux Mirror
    Project](https://web.archive.org/web/20090603033755/http://tlm-project.org/)
    Download Linux Distributions over BitTorrent
  - [GNU/Linux distro timeline](http://futurist.se/gldt/)
  - [OSMSG](http://www.osmsg.com/)

## 参见

  - [Linux发行版列表](../Page/Linux发行版列表.md "wikilink")
  - [Linux发行版比较](../Page/Linux发行版比较.md "wikilink")
  - [Linux的採用](../Page/Linux的採用.md "wikilink")
  - [Cygwin](../Page/Cygwin.md "wikilink")

{{-}}

[Linux發行版](../Category/Linux發行版.md "wikilink")

1.  [The Slackware Linux Project: Slackware Release
    Announcement](http://www.slackware.com/announce/1.0.php)