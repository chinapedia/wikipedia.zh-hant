下面列出了世界上主要的[宗教](../Page/宗教.md "wikilink")、[教派](../Page/教派.md "wikilink")。

## [亞伯拉罕諸教](../Page/亞伯拉罕諸教.md "wikilink")

  - [犹太教](../Page/犹太教.md "wikilink")

### [基督宗教](../Page/基督宗教.md "wikilink")

  - [原始基督教](../Page/原始基督教.md "wikilink")
  - [亚美尼亚教会](../Page/亚美尼亚教会.md "wikilink")
  - [二性连结派](../Page/聂斯脱里派.md "wikilink")
      - [東方亚述教会](../Page/東方亚述教会.md "wikilink")，即[中国](../Page/中国.md "wikilink")[唐代](../Page/唐代.md "wikilink")[景教](../Page/景教.md "wikilink")。
  - [一性论派](../Page/一性论派.md "wikilink")
      - [科普特教会](../Page/科普特教会.md "wikilink")
  - [天主教](../Page/天主教.md "wikilink")
      - [拉丁礼](../Page/拉丁礼.md "wikilink")
          - [奥古斯丁派](../Page/奥古斯丁派.md "wikilink")
          - [本尼狄克派](../Page/本尼狄克派.md "wikilink")（本笃会）
              - [克吕尼修会](../Page/克吕尼修会.md "wikilink")
          - [耶稣会](../Page/耶稣会.md "wikilink")
          - [慈幼會](../Page/慈幼會.md "wikilink")
          - [方濟會](../Page/方濟會.md "wikilink")
          - [多明我会](../Page/多明我会.md "wikilink")
          - [方济各会](../Page/方济各会.md "wikilink")
      - [东方礼](../Page/东仪天主教会.md "wikilink")
  - [东正教](../Page/东正教.md "wikilink")
      - [俄罗斯正教会](../Page/俄罗斯正教会.md "wikilink")
      - [希腊正教会](../Page/希腊正教会.md "wikilink")

<!-- end list -->

  - [新教派](../Page/基督新教.md "wikilink")
      - [路德宗](../Page/路德宗.md "wikilink")（信义宗）
      - [安立甘宗](../Page/安立甘宗.md "wikilink")（圣公宗）
      - [归正宗](../Page/归正宗.md "wikilink")（加尔文派）
      - [贵格会](../Page/贵格会.md "wikilink")（教友派、公谊会）
      - [公理宗](../Page/公理宗.md "wikilink")
      - [浸会](../Page/浸会.md "wikilink")（浸礼会、浸信会）
      - [长老宗](../Page/长老宗.md "wikilink")
      - [卫斯理宗](../Page/卫斯理宗.md "wikilink")（监理宗）
      - [基督复临派](../Page/基督复临派.md "wikilink")
      - [虔敬派](../Page/虔敬派.md "wikilink")
      - [阿明尼乌派](../Page/阿明尼乌派.md "wikilink")（抗辩派）
      - [重浸派](../Page/重浸派.md "wikilink")（重浸宗）
      - [普救派](../Page/普救派.md "wikilink")
      - [真耶稣教会](../Page/真耶稣教会.md "wikilink")
  - [諾斯底主義](../Page/諾斯底主義.md "wikilink")
  - [玫瑰十字會](../Page/玫瑰十字會.md "wikilink")
  - [五旬节会](../Page/五旬节会.md "wikilink")（五旬宗）
  - [阿里乌派](../Page/阿里乌派.md "wikilink")
  - [冉森派](../Page/冉森派.md "wikilink")
  - [安息会](../Page/安息会.md "wikilink")
  - [基督教科學會](../Page/基督教科學會.md "wikilink")
  - [摩门教](../Page/摩门教.md "wikilink")
  - [耶和华见证人](../Page/耶和华见证人.md "wikilink")
  - [召會](../Page/召會.md "wikilink")
  - [拜上帝教](../Page/拜上帝教.md "wikilink")，[中国的](../Page/中国.md "wikilink")[洪秀全建立](../Page/洪秀全.md "wikilink")。

### [伊斯兰教](../Page/伊斯兰教.md "wikilink")

  -   - [苏非派](../Page/苏非派.md "wikilink")
      - [尼札尔派](../Page/尼札尔派.md "wikilink")
      - [白山派](../Page/白山派.md "wikilink")
      - [什叶派](../Page/什叶派.md "wikilink")
          - [伊斯玛仪派](../Page/伊斯玛仪派.md "wikilink")
              - [德鲁兹派](../Page/德鲁兹派.md "wikilink")
          - [栽德派](../Page/栽德派.md "wikilink")
          - [十二伊玛目派](../Page/十二伊玛目派.md "wikilink")
      - [艾巴德派](../Page/艾巴德派.md "wikilink")（一般认为它属于[哈瓦利吉派](../Page/哈瓦利吉派.md "wikilink")）
          - [易巴德派](../Page/易巴德派.md "wikilink")
      - [哈瓦利吉派](../Page/哈瓦利吉派.md "wikilink")
      - [逊尼派](../Page/逊尼派.md "wikilink")
      - [穆尔太齐赖派](../Page/穆尔太齐赖派.md "wikilink")
      - [莱瓦菲祖派](../Page/莱瓦菲祖派.md "wikilink")
      - [穆尔吉埃派](../Page/穆尔吉埃派.md "wikilink")
      - [瓦哈比派](../Page/瓦哈比派.md "wikilink")

### [巴比教](../Page/巴比教.md "wikilink")

### [巴哈伊信仰](../Page/巴哈伊信仰.md "wikilink")

## 東亞

### [道教](../Page/道教.md "wikilink")

  - [全真道](../Page/全真道.md "wikilink")
      - [楼观派](../Page/楼观派.md "wikilink")
      - [龙门派](../Page/龙门派.md "wikilink")
      - [遇仙派](../Page/遇仙派.md "wikilink")
  - [太平道](../Page/太平道_\(宗教\).md "wikilink")
  - [五斗米道](../Page/五斗米道.md "wikilink")
  - [正一道](../Page/正一道.md "wikilink")
      - [天师道](../Page/天师道.md "wikilink")
      - [上清派](../Page/上清派.md "wikilink")
      - [灵宝派](../Page/灵宝派.md "wikilink")
      - [太一道](../Page/太一道.md "wikilink")
  - [真大道教](../Page/真大道教.md "wikilink")
      - 天宝宫派
      - 玉虚宫派
  - [净明道](../Page/净明道.md "wikilink")
  - [茅山派](../Page/茅山派.md "wikilink")
  - [青龍教](../Page/青龍教.md "wikilink")
  - [武当派](../Page/武当派.md "wikilink")
  - [黃大仙信仰](../Page/黃初平.md "wikilink")
  - [王爺信仰](../Page/王爺信仰.md "wikilink")
  - [越南道教](../Page/越南道教.md "wikilink")

### [无为教](../Page/无为教.md "wikilink")

  - [在理教](../Page/在理教.md "wikilink")

### [神道](../Page/神道.md "wikilink")

  - [神社神道](../Page/神社神道.md "wikilink")
  - [教派神道](../Page/教派神道.md "wikilink")
  - [民俗神道](../Page/民俗神道.md "wikilink")

<!-- end list -->

  - [琉球神道](../Page/琉球神道.md "wikilink")

<!-- end list -->

  - [天理教](../Page/天理教.md "wikilink")

### [儒教](../Page/儒教_\(宗教\).md "wikilink")

  - [孔教](../Page/孔教.md "wikilink")
      - 孔圣会
      - 孔圣堂
      - 孔教学院

<!-- end list -->

  - [關公信仰](../Page/關羽.md "wikilink")
  - [城隍爺信仰](../Page/城隍.md "wikilink")
  - [三太子信仰](../Page/哪吒.md "wikilink")
  - [媽祖信仰](../Page/媽祖.md "wikilink")
  - [川主信仰](../Page/川主.md "wikilink")
  - [中國傳統宗法性宗教](../Page/中國傳統宗法性宗教.md "wikilink")\[1\]\[2\]
  - [天祖教](../Page/天祖教.md "wikilink")

### [天理教 (中国)](../Page/天理教_\(中国\).md "wikilink")

### [天道教](../Page/天道教.md "wikilink")

## 印度-伊朗宗教

  - [印度教](../Page/印度教.md "wikilink")
      - [锡克教](../Page/锡克教.md "wikilink")
      - [虔诚派](../Page/虔诚派.md "wikilink")

<!-- end list -->

  - [婆罗门教](../Page/婆罗门教.md "wikilink")

<!-- end list -->

  - [佛教](../Page/佛教.md "wikilink")
      - [格魯派](../Page/格魯派.md "wikilink")
      - [薩迦派](../Page/薩迦派.md "wikilink")
      - [寧瑪派](../Page/寧瑪派.md "wikilink")
      - [噶舉派](../Page/噶舉派.md "wikilink")
      - [大寺派](../Page/大寺派.md "wikilink")
      - [無畏山派](../Page/無畏山派.md "wikilink")
      - [祗陀林派](../Page/祗陀林派.md "wikilink")
      - [中觀學派](../Page/中觀派.md "wikilink")
      - [天台宗](../Page/天臺宗.md "wikilink")
      - [南山宗](../Page/南山宗.md "wikilink")
      - [淨土宗](../Page/淨土宗.md "wikilink")
      - [法相宗](../Page/法相宗.md "wikilink")
      - [真言宗](../Page/真言宗.md "wikilink")
      - [佛心宗](../Page/佛心宗.md "wikilink")
      - [涅槃宗](../Page/涅槃宗.md "wikilink")
      - [華嚴宗](../Page/華嚴宗.md "wikilink")
      - [曹洞宗](../Page/曹洞宗.md "wikilink")
      - [臨濟宗](../Page/臨濟宗.md "wikilink")
      - [黃檗宗](../Page/黃檗宗.md "wikilink")

<!-- end list -->

  - [耆那教](../Page/耆那教.md "wikilink")

<!-- end list -->

  - [瑣羅亞斯德教](../Page/瑣羅亞斯德教.md "wikilink")（又稱祆教、拜火教）
  - [摩尼教](../Page/摩尼教.md "wikilink")

## 中华少数民族[宗教](../Page/原始宗教.md "wikilink")

  - 白族宗教
  - 布朗族宗教
  - 布依族宗教
  - 瑶族宗教
  - 彝族宗教
  - 壮族宗教
  - 德昂族宗教
  - 侗族宗教
  - 独龙族宗教
  - 高山族宗教
  - [仡佬族宗教](../Page/仡佬族宗教.md "wikilink")
  - 哈尼族宗教
  - 基诺族宗教
  - 景颇族宗教
  - 拉祜族宗教
  - 黎族宗教
  - 傈僳族宗教
  - 珞巴族宗教
  - 毛南族宗教
  - 苗族宗教
  - [仫佬族宗教](../Page/仫佬族宗教.md "wikilink")
  - 纳西族宗教
  - 怒族宗教
  - 普米族宗教
  - 羌族宗教
  - 畲族宗教
  - 水族宗教
  - 土家族宗教
  - 佤族宗教
  - 满族萨满教
  - [蒙古族萨满教](../Page/腾格里.md "wikilink")
  - 中亚萨满教
  - 西伯利亚萨满教
  - [本教](../Page/本教.md "wikilink")

## 大洋洲原始宗教

  - [美拉尼西亚宗教](../Page/美拉尼西亚宗教.md "wikilink")
  - [波利尼西亚宗教](../Page/波利尼西亚宗教.md "wikilink")

## 中东原始宗教

  - [美索不达米亚宗教](../Page/美索不达米亚宗教.md "wikilink")
  - [迦南宗教](../Page/迦南宗教.md "wikilink")
  - [赫梯宗教](../Page/赫梯宗教.md "wikilink")
  - [腓尼基宗教](../Page/腓尼基宗教.md "wikilink")

## 美洲原始宗教

  - [印加宗教](../Page/印加宗教.md "wikilink")
  - [阿兹特克宗教](../Page/阿兹特克宗教.md "wikilink")
  - [玛雅宗教](../Page/玛雅宗教.md "wikilink")

## 欧洲原始宗教

  - [日耳曼宗教](../Page/北欧神话.md "wikilink") 
  - 乌拉尔宗教 
  - [罗马宗教](../Page/古羅馬宗教.md "wikilink")
  - [希腊宗教](../Page/古希臘宗教.md "wikilink")

## 非洲原始宗教

  - [埃及宗教](../Page/古埃及宗教.md "wikilink")
  - [伏都教](../Page/伏都教.md "wikilink")

## [新興宗教](../Page/新興宗教.md "wikilink")

  - [中功](../Page/中功.md "wikilink")
  - [人民圣殿教](../Page/人民圣殿教.md "wikilink")
  - [世界末日教派](../Page/世界末日教派.md "wikilink")
  - [山達基](../Page/山達基.md "wikilink")
  - [灵灵教](../Page/灵灵教.md "wikilink")
  - [撒旦教](../Page/撒旦教.md "wikilink")

## [虛構宗教](../Page/虛構宗教.md "wikilink")

  - [飞行面条怪物信仰](../Page/飞行面条怪物信仰.md "wikilink")

## 参考资料

[Category:宗教相關列表](../Category/宗教相關列表.md "wikilink")

1.  2013-01-20《世纪大讲堂》 宗教与中国社会
2.  《中国宗法性传统宗教试探》 牟钟鉴