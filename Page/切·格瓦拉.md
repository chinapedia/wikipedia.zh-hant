**切·格瓦拉**（；1928年6月14日\[1\]－1967年10月9日），本名**埃內斯托·-{zh-hans:格瓦拉;zh-hk:古華拉;zh-tw:格瓦拉;}-**（），昵称**-{zh-hans:切;zh-hk:哲;zh-tw:切;}-**（或），出生于[阿根廷](../Page/阿根廷.md "wikilink")。他是[古巴革命的核心人物之一](../Page/古巴革命.md "wikilink")，[社会主义古巴](../Page/古巴共和国.md "wikilink")、[古巴革命武装力量和](../Page/古巴革命武装力量.md "wikilink")[古巴共產黨的主要缔造者及领导人](../Page/古巴共產黨.md "wikilink")，著名的國際[共产主义](../Page/共产主义.md "wikilink")[革命家](../Page/革命家.md "wikilink")、軍事理論家、[政治家](../Page/政治家.md "wikilink")、醫生、作家、[游擊隊领袖](../Page/游擊隊.md "wikilink")。

身為年輕的[醫學系學生](../Page/醫學系.md "wikilink")，格瓦拉趁著一股熱情騎上-{zh-hans:摩托車;zh-hk:電單車;zh-tw:機車;}-，遊歷了整個[拉丁美洲](../Page/拉丁美洲.md "wikilink")，並因親眼目睹了[貧窮的無所不在而深感震撼](../Page/貧窮.md "wikilink")\[2\]。他在這些旅行中的所見所聞，使他斷定各地根深蒂固的[社會不平等是](../Page/社會不平等.md "wikilink")[國家壟斷資本主義](../Page/國家壟斷資本主義.md "wikilink")、[新殖民主義與](../Page/新殖民主義.md "wikilink")[帝國主義的結果](../Page/帝國主義.md "wikilink")，唯一的補救方法便是進行[世界革命](../Page/世界革命.md "wikilink")\[3\]。這個信仰激勵他介入了[瓜地馬拉在總統](../Page/瓜地馬拉.md "wikilink")[哈科沃·阿本斯·古斯曼統治下的社會改革](../Page/哈科沃·阿本斯·古斯曼.md "wikilink")；阿本斯最終于1954年瓜地馬拉政變，在[美國中情局暗中策動下被推翻](../Page/美國中情局.md "wikilink")，結束了瓜地馬拉當時不同凡響的意識形態。後來，格瓦拉在[墨西哥城先后結識了](../Page/墨西哥城.md "wikilink")[勞爾·卡斯特罗與](../Page/勞爾·卡斯特罗.md "wikilink")[菲德爾·卡斯特罗](../Page/菲德爾·卡斯特罗.md "wikilink")，加入他們的[七二六運動](../Page/七二六運動.md "wikilink")，懷抱著推翻親[美的](../Page/美國.md "wikilink")[獨裁者](../Page/獨裁者.md "wikilink")[富爾亨西奧·巴蒂斯塔的理想](../Page/富爾亨西奧·巴蒂斯塔.md "wikilink")、乘著[格拉瑪號登陆古巴](../Page/格拉瑪號.md "wikilink")\[4\]。

格瓦拉不久便在[起義者間展露頭角](../Page/起義.md "wikilink")，晉升為副指揮官，並在為期二年、成功推翻巴蒂斯塔政權的游擊戰役中扮演關鍵的角色。\[5\]在1959年[古巴革命成功後](../Page/古巴革命.md "wikilink")，他被授予古巴公民身份，擔任古巴政府高级领导人，格瓦拉在新政府擔任了數個要角。包括再審[革命法庭期間被列為戰犯者的申訴與](../Page/革命法庭.md "wikilink")[行刑隊](../Page/槍斃.md "wikilink")\[6\]、以工業部長身分實施土地改革、擔任國家銀行行長及古巴軍隊教學主任之職，並以宣揚古巴[社會主義的外交家之名橫越地球](../Page/社會主義.md "wikilink")。而格瓦拉所訓練的民兵部隊也在[豬灣入侵擊退了美軍](../Page/豬灣事件.md "wikilink")\[7\]，並為古巴帶來了[蘇聯](../Page/蘇聯.md "wikilink")[核武](../Page/核武器.md "wikilink")[彈道飛彈](../Page/彈道飛彈.md "wikilink")，其隨後在1962年引發了[古巴飛彈危機](../Page/古巴飛彈危機.md "wikilink")\[8\]。另外，他也是文筆鋒利、創作豐富的作家與日記作者，著有影響深遠的军事理论著作《[游击战](../Page/游击战_\(书\).md "wikilink")》，及關於他年轻时漫游美洲的旅行日记《[摩托日记](../Page/摩托日记.md "wikilink")》。格瓦拉於1965年離開古巴，到[刚果
(金)](../Page/刚果_\(金\).md "wikilink")、[玻利维亚进行反对](../Page/玻利维亚.md "wikilink")[帝国主义的游击战争](../Page/帝国主义.md "wikilink")。

他离开古巴的原因有不同的解释，主流看法稱他是自願離开古巴继续革命，也有观点认为他是被迫离开，因为卡斯特罗与切·格瓦拉在对[中苏交恶的看法上存在分歧](../Page/中苏交恶.md "wikilink")\[9\]。總之，他離開职务后便依序前往非洲[剛果-金沙薩及](../Page/剛果民主共和國.md "wikilink")[玻利維亞進行革命](../Page/玻利維亞.md "wikilink")，前者最終失敗，而格瓦拉到了玻利維亞後，被當地由[美國中央情報局協助的軍隊逮捕](../Page/美國中央情報局.md "wikilink")，並遭處決。\[10\]自-{zh-hans:切;zh-hk:哲;zh-tw:切;}-·格瓦拉死後，他的肖像已成為[反主流文化的普遍象徵](../Page/反主流文化.md "wikilink")、全球[流行文化的標誌](../Page/流行文化中的切·格瓦拉.md "wikilink")\[11\]，同時他本人也成为了[国际共产主义运动中的英雄和世界左翼運動的象徵](../Page/国际共产主义运动.md "wikilink")。

格瓦拉常現身於傳記、回憶錄、文章、紀錄片、歌曲及電影的題材，其共同想像普遍呈現兩極化，有理想主義色彩的人會認為他是為改革而生的戰士，主流保守派則認為他只是不顧一切的魯莽反抗者。《[時代](../Page/時代_\(雜誌\).md "wikilink")》雜誌將格瓦拉選入二十世紀百大影響力人物\[12\]，並登上[阿尔韦托·科尔达為他拍攝](../Page/阿尔韦托·科尔达.md "wikilink")、命名為《[英勇的游擊隊員](../Page/英勇的游擊隊員.md "wikilink")》的照片，被美譽為「世上最知名的照片」。\[13\]

## 早年

[Chefamily.jpg](https://zh.wikipedia.org/wiki/File:Chefamily.jpg "fig:Chefamily.jpg")
切·格瓦拉出生于阿根廷[罗萨里奥](../Page/罗萨里奥.md "wikilink")，是这个[西班牙和](../Page/西班牙.md "wikilink")[爱尔兰裔家庭的长子](../Page/爱尔兰.md "wikilink")。（有説是巴斯克裔和愛爾蘭裔的混血兒）他出生证明上的日期是1928年6月14日，但一些资料认为他实际上出生于5月14日。他的出生证明被故意修改，以掩饰他母亲在结婚时已怀孕的事实。切·格瓦拉父亲埃内斯托·格瓦拉·林奇的家族已在阿根廷生活了12代，是一个声誉卓著的家族。他的祖先帕特里克·林奇1715年出生于爱尔兰，后经西班牙转辗来到阿根廷，在18世纪末，他已成为了[巴拉那河地区的总督](../Page/巴拉那河.md "wikilink")。而他母亲塞莉亚·德·拉·塞尔纳·略萨的家族也已在阿根廷生活了7代，同样也是贵族家庭，祖先[约瑟·德·拉·塞尔纳曾是西班牙最后一任驻](../Page/约瑟·德·拉·塞尔纳.md "wikilink")[秘鲁总督](../Page/秘鲁.md "wikilink")。切·格瓦拉的父母于1927年结婚。\[14\]

切·格瓦拉父亲在传记《[我的儿子，切](../Page/我的儿子，切.md "wikilink")》中写到：

[CheG1951.jpg](https://zh.wikipedia.org/wiki/File:CheG1951.jpg "fig:CheG1951.jpg")
从此切·格瓦拉便患上了严重的[哮喘病](../Page/哮喘.md "wikilink")。受这个有着一定左翼思想的上层家庭（尤其是作为[阿根廷共产党党员的姨父母](../Page/阿根廷共产党.md "wikilink")）的影响，切·格瓦拉从小便对政治十分热衷。虽然患有哮喘，但切·格瓦拉十分热爱体育运动。1948年，他进入[布宜诺斯艾利斯大学学习医学](../Page/布宜诺斯艾利斯大学.md "wikilink")，并于1953年3月顺利完成了学业。

### 遊歷

學生時期的切·格瓦拉时常利用假期在[拉丁美洲周遊](../Page/拉丁美洲.md "wikilink")。1950年1、2月暑假时，他遊歷了阿根廷北部的12个省，走过了约4000多公里的路程。1951年，他在自己的好友药剂师[阿爾貝托·格拉納多的建议下](../Page/阿爾貝托·格拉納多.md "wikilink")，决定休学1年环遊整个[南美洲](../Page/南美洲.md "wikilink")。他们的交通工具是一辆1939年产的Norton摩托车。他们于1951年12月29日出发，决定的线路为：沿着[安第斯山脉穿越整个南美洲](../Page/安第斯山脉.md "wikilink")，经阿根廷、[智利](../Page/智利.md "wikilink")、[秘鲁](../Page/秘鲁.md "wikilink")、[哥伦比亚](../Page/哥伦比亚.md "wikilink")，到达[委内瑞拉](../Page/委内瑞拉.md "wikilink")。在路途的中间他们的[摩托车坏掉了](../Page/摩托车.md "wikilink")。切·格瓦拉还在[秘鲁的一个](../Page/秘鲁.md "wikilink")[麻风病人村作了几个月的义工](../Page/麻风病.md "wikilink")。

在这次旅行中，切·格瓦拉开始真正了解拉丁美洲的贫穷与苦难，他的[国际主义思想也在这次旅行中渐渐定型](../Page/国际主义.md "wikilink")，他开始认为拉美各个独立的国家其实是一个拥有共同的文化和经济利益的整体，倘若革命则需要国际合作。离家8个月后，1952年9月，切·格瓦拉乘飞机回到了阿根廷，全家人都去机场迎接他。在他此时的一篇日记中他写到：

[Che_Guevara_-_2do_Viaje_-_1953-55.png](https://zh.wikipedia.org/wiki/File:Che_Guevara_-_2do_Viaje_-_1953-55.png "fig:Che_Guevara_-_2do_Viaje_-_1953-55.png")
切·格瓦拉在这次旅行中所写的[日记后来被成册出版](../Page/摩托日记.md "wikilink")，依此巴西导演[沃尔特·萨勒斯](../Page/沃尔特·萨勒斯.md "wikilink")（Walter
Salles）2004年拍摄了电影《[摩托日记](../Page/摩托日记_\(电影\).md "wikilink")》。切·格瓦拉开始拼命复习，在1953年6月1日，他正式成为医生。他本可成为一位受人尊敬的医生，可是，这次旅行彻底改变了切·格瓦拉。

## 踏上危地馬拉

由于当时[庇隆政府在阿根廷实行独裁统治](../Page/胡安·庇隆.md "wikilink")，担心儿子被征用做军医的母亲让格瓦拉逃离阿根廷。1953年7月7日，格瓦拉开始了他的第二次拉美之旅。在[玻利维亚经历了一次革命之后](../Page/玻利维亚.md "wikilink")，格瓦拉从[厄瓜多爾前往](../Page/厄瓜多爾.md "wikilink")[危地马拉](../Page/危地马拉.md "wikilink")。途经[哥斯達黎加时](../Page/哥斯达黎加.md "wikilink")，这个当时拉美唯一的民主国家深深打动了格瓦拉。

1953年12月24日，格瓦拉到达了危地马拉。当时危地马拉正处于年轻的左翼总统[阿本斯的领导下](../Page/阿本斯.md "wikilink")，进行着一系列改革，尤其是土地改革，矛头直指美国[联合果品公司](../Page/联合果品公司.md "wikilink")。在危地马拉，他和一些[危地马拉劳动党党员结为朋友](../Page/危地马拉劳动党.md "wikilink")，并得到了他知名的绰号“-{zh-hans:切;zh-hk:哲;zh-tw:切;}-
”（Che），“Che”是一个西班牙语的感叹词，在阿根廷和南美的一些地区被广泛使用，是人打招呼和表示惊讶的常用语，也有“朋友”的意思，其他西班牙語系國家的人喜歡暱稱阿根廷人為（Che），是"兄弟"的意思。

1954年3月28日，美国中央情报局在[洪都拉斯成立了一枝由危地马拉军官](../Page/洪都拉斯.md "wikilink")[阿马斯领导的雇佣军](../Page/阿马斯.md "wikilink")，阿本斯政权很快被推翻，阿马斯成为危地马拉总统，开始对左翼人士进行残酷镇压，几个月之内约9000人被捕或杀害。从此，格瓦拉坚定了自己的[共产主义信仰](../Page/共产主义.md "wikilink")，认为共产主义是解决目前拉美种种困难的唯一途径。随后，他前往[墨西哥避难](../Page/墨西哥.md "wikilink")，并在此结识了流亡的古巴革命者[劳尔·卡斯特罗](../Page/劳尔·卡斯特罗.md "wikilink")。不久，劳尔·卡斯特罗又将他引荐给了自己的哥哥、[七二六运动领导人](../Page/七二六运动.md "wikilink")[菲德尔·卡斯特罗](../Page/菲德尔·卡斯特罗.md "wikilink")。

## 古巴革命

[Che_on_Mule_in_Las_Villas_Nov_1958.jpg](https://zh.wikipedia.org/wiki/File:Che_on_Mule_in_Las_Villas_Nov_1958.jpg "fig:Che_on_Mule_in_Las_Villas_Nov_1958.jpg")
1955年，格瓦拉與[菲德爾·卡斯楚在](../Page/菲德爾·卡斯楚.md "wikilink")[墨西哥城相遇](../Page/墨西哥城.md "wikilink")，当时[菲德爾·卡斯楚與](../Page/菲德爾·卡斯楚.md "wikilink")[勞爾·卡斯楚兄弟正为重返](../Page/勞爾·卡斯楚.md "wikilink")[古巴进行武装斗争并推翻](../Page/古巴.md "wikilink")[巴蒂斯塔独裁政权而进行准备](../Page/弗尔亨西奥·巴蒂斯塔·萨尔迪瓦.md "wikilink")。格瓦拉迅速加入了卡斯特罗组织的名为“[七二六运动](../Page/七二六运动.md "wikilink")”（以一次失败的革命：[蒙卡达事件的日期命名](../Page/攻打蒙卡达兵营行动.md "wikilink")）的军事组织。1956年11月25日，“七二六运动”的82名战士挤在“[格拉玛号](../Page/格拉玛号.md "wikilink")”小游艇上，从墨西哥[韦拉克鲁斯州的](../Page/韦拉克鲁斯州.md "wikilink")[图克斯潘出发](../Page/图克斯潘.md "wikilink")，驶向古巴。

1956年12月2日，比计划推迟了两天，他们在古巴南部的[奥连特省的一片沼泽地登陆](../Page/奥连特省.md "wikilink")，遭到巴蒂斯塔的军队的袭击，只有12人在这次袭击中幸存。格瓦拉，作为军队的医生，在一次战斗中，当面前一个是药箱，另一个是子弹箱时，他扛起了子弹箱。从这一刻开始，格瓦拉彻底从医生转变为了一名战士。

剩余的游击队战士，在[马埃斯特腊山中安顿下来](../Page/马埃斯特腊山.md "wikilink")，并使革命队伍逐渐壮大，得到了一些农民及工人的支持。在战斗中，格瓦拉憑著超人的勇气及毅力、出色的战斗技巧和对敌人冷酷无情而得到了越来越多人的赏识，包括卡斯特罗。他很快成为了卡斯特罗最得力和信赖的助手。到1958年初游击队员约有280人，在经历了一系列战斗之后，到12月27日，革命军拥有了8000平方公里土地和50万公民支持。1959年1月2日，革命军成功占领古巴首都[哈瓦那](../Page/哈瓦那.md "wikilink")，巴蒂斯塔出逃。这段经历后被格瓦拉写入于1963年出版的《古巴革命战争回忆录》。

## 古巴任职

[缩略图](https://zh.wikipedia.org/wiki/File:Jânio_Quadros_condecora_Ernesto_Guevara_com_a_Ordem_do_Cruzeiro_do_Sul.jpg "fig:缩略图")
战争结束后，古巴新政府成立並授予格瓦拉“古巴公民”的身份。1959年5月22日，格瓦拉同自己的第一任秘鲁裔妻子[伊尔达·加德亚](../Page/伊尔达·加德亚.md "wikilink")（Hilda
Gadea）离婚，他们唯一的女儿由格瓦拉抚养。6月，格瓦拉同参与了古巴革命且与自己志同道合的[阿莱伊达·马奇](../Page/阿莱伊达·马奇.md "wikilink")（Aleida
March）结婚，之后他们共育有4个子女。

[Che_Guevara.jpg](https://zh.wikipedia.org/wiki/File:Che_Guevara.jpg "fig:Che_Guevara.jpg")

首先，格瓦拉被任命为[卡瓦尼亚堡军事监狱的检察长](../Page/卡瓦尼亚堡.md "wikilink")，负责对巴蒂斯塔时代的[战犯](../Page/战犯.md "wikilink")（主要是政客和警察）进行审問和处理，一些资料认为格瓦拉一共刑决了156人，但一般推测，人数应该高达600人。1959年10月，并不懂经济学的格瓦拉被总理[菲德尔·卡斯特罗任命为国家银行总裁](../Page/菲德尔·卡斯特罗.md "wikilink")，开始对古巴经济体系进行[社会主义改造](../Page/社会主义.md "wikilink")，将企业收归国有，并实行了土地改革。1961年，格瓦拉又被卡斯特罗任命为工业部长。

格瓦拉帮助卡斯特罗在古巴建立了社会主义制度，在古巴遭到美国经济封锁后，格瓦拉与[苏联签定了贸易协定](../Page/苏联.md "wikilink")。在这段时间内，他也因为其对美国的强硬态度而逐渐于西方闻名。在[古巴导弹危机中](../Page/古巴导弹危机.md "wikilink")，他是1962年赴[莫斯科谈判的古巴代表团的成员之一](../Page/莫斯科.md "wikilink")，并最终签署了苏联在古巴部署[核武器的计划](../Page/核武器.md "wikilink")。格瓦拉认为，安置苏联的导弹将捍卫古巴独立，使古巴免于遭受美国的侵略。

1964年12月，格瓦拉代表古巴出席[联合国第19次大会](../Page/联合国第19次大会.md "wikilink")，之后相继访问了[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")、[刚果民主共和国等](../Page/刚果民主共和国.md "wikilink")8个非洲国家和[中华人民共和国](../Page/中华人民共和国.md "wikilink")。当1965年3月14日回到古巴后，他与卡斯特罗在诸如对苏关系、援助第三世界革命等问题上的分歧日趋严重。不久他辞去了自己的职务，在给卡斯特罗写的告别信中：“他对单一的苏联模式感到不解和失望，对社会主义的前途感到忧虑，因为他发现不少的革命者都是在豪华的汽车裡、在漂亮的女秘书的怀抱裡丧失了往日的锐气。所以，为保持革命者的完美形象，他只能选择战斗，选择一个凤凰涅槃式的壮美结局”，为防止个人行为对古巴的不利，放弃了古巴公民身份。4月1日他乘飞机离开了古巴，前往刚果。

在古巴担任高官期间，格瓦拉抵制[官僚主义](../Page/官僚主义.md "wikilink")，生活节俭，并且拒绝给自己增加薪水。他从没上过夜总会，没有看过电影，也没去过海滩。一次在苏联一位官员家裡做客时，当那位官员拿出極昂贵的瓷器餐具来招待格瓦拉时，格瓦拉对主人说：“真是讽刺，我这个土包子怎么配使用这么高级的餐具？”同时格瓦拉周末还积极参加义务劳动，比如在甘蔗地或工厂裡劳动。

## 出走剛果

[1965-4_1965年_古巴切·格瓦拉访问中国，邓小平接见.jpg](https://zh.wikipedia.org/wiki/File:1965-4_1965年_古巴切·格瓦拉访问中国，邓小平接见.jpg "fig:1965-4_1965年_古巴切·格瓦拉访问中国，邓小平接见.jpg")接见\]\]
1965年4月23日，格瓦拉从[坦桑尼亚穿越](../Page/坦桑尼亚.md "wikilink")[坦噶尼喀湖](../Page/坦噶尼喀湖.md "wikilink")，前往[刚果](../Page/刚果民主共和国.md "wikilink")。一些资料指出，在之前他同卡斯楚的一次秘密会谈中，格瓦拉说服了卡斯楚支持这次行动。在最初他得到了当时刚果游击队领导人[洛朗-德西雷·卡比拉的协助](../Page/洛朗-德西雷·卡比拉.md "wikilink")，但不久格瓦拉拒绝了他的帮助，认为其是完全无意义的，并写到：“没有什么能让我相信他是一个现在的人”。

格瓦拉向刚果起义军队传授游击战术，他的计划是利用刚果坦噶尼喀湖西岸的解放区作为基地，训练刚果及周边国家的革命武装。此时格瓦拉已经37岁，而且并没有受過正規軍事訓練的經驗（他的[哮喘使他免于在阿根廷服兵役](../Page/哮喘.md "wikilink")），他的战斗经验大都来自古巴革命。

與剛果政府軍一道的美國中情局人員，此時正全程監控格瓦拉部隊的對外通信，以便于在格瓦拉的游擊隊來襲前能先發制人、截斷其補給線。格瓦拉在此役所期許的是能夠向當地的[辛巴人灌輸古巴共產主义革命思想及游擊戰術](../Page/辛巴人.md "wikilink")，將他們訓練成一批驍勇善戰的游擊隊。事後格瓦拉在他的《剛果日記》裡回憶，當地人組織起來的烏合之眾愚笨、漫無紀律、內部紛爭不休是導致這次起義失敗的主要原因。同年，在非洲叢林吃足了7個月的苦頭之後，病弱的格瓦拉沮喪地與他剩存的古巴戰友離開剛果（有6個伙伴沒能活著離開）。格瓦拉一度考慮將受傷士兵送回古巴，自己留在剛果叢林裡戰到最後一刻，用生命為革命豎立典範。不過，在幾次徘徊後，格瓦拉经不住同志們勸說，同他們一起逃離了刚果。

離開剛果的格瓦拉並沒有因此回到他熟悉的古巴。在卡斯楚公佈的格瓦拉道別信裡，格瓦拉宣称他將切斷與古巴的一切聯繫，投身於世界其他角落的革命運動。為此，格瓦拉深覺在道義上他不应回古巴。接下來的六個月裡，格瓦拉極其低調的遊走於坦桑尼亚首都[達累斯薩拉姆](../Page/達累斯薩拉姆.md "wikilink")、[布拉格以及](../Page/布拉格.md "wikilink")[東德](../Page/東德.md "wikilink")。這段時期，格瓦拉除了記載他在剛果的經過外，還開始起草兩本書，準備對經濟學及哲學加以論述。在卡斯楚获悉格瓦拉的下落後，極力要求他的老同志回到古巴。格瓦拉则明确地聲明，除非是為了在[拉丁美洲國家进行革命活动](../Page/拉丁美洲.md "wikilink")，因地利之便，他會在絕對機密的情況下回到古巴進行籌備工作外，他將不再踏上這片土地。

## 玻利維亞

1966年至1967年間，格瓦拉開始在[玻利維亞帶領](../Page/玻利維亞.md "wikilink")[游擊隊員進行](../Page/游擊隊.md "wikilink")[革命活動](../Page/革命.md "wikilink")。當地的[玻利维亚共产党成员購買了尼阿卡瓦苏的密林地區移交給格瓦拉用作訓練區域](../Page/玻利维亚共产党.md "wikilink")。格瓦拉及其[古巴伴隨亦編改了一些游擊隊員軍隊方式](../Page/古巴.md "wikilink")。格瓦拉的组织大约有50名游擊隊員，命名为“[民族解放军](../Page/民族解放军_\(玻利维亚\).md "wikilink")”（史称“尼阿卡瓦苏游击队”），有著精良裝備。早期他们在險峻的[卡米里山區與玻利維亞正規軍作戰取得了不少胜利](../Page/卡米里.md "wikilink")，然而在9月，正規軍卻設法想消滅两個游擊隊小組，并殺害了一個游擊隊領導。

玻利維亞總統[雷内·巴里恩托斯得知格瓦拉的存在後](../Page/雷内·巴里恩托斯.md "wikilink")，扬言要殺死他。他下令玻利維亞軍隊四處搜尋格瓦拉和他的追隨者。希望帶起革命的格瓦拉對玻利維亞的錯誤判斷令他後來慘敗。他只準備應付國家軍政府及其訓練和裝備皆差的軍隊，但卻沒有顧慮到玻利維亞身後的美國。

當美國政府得知他的革命活動地點後，很快便派出了[中央情报局人員進入玻利維亞幫助其政府剿滅游擊隊](../Page/中央情报局.md "wikilink")。因為美國的援助，玻利維亞軍隊由[美國陸軍](../Page/美國陸軍.md "wikilink")[特種部隊顧問訓練](../Page/特種部隊.md "wikilink")，當中更包括了一支以應付叢林戰而組織的別動隊。而格瓦拉在游擊當中卻得不到地方分離分子及[玻利維亞共產黨的預期協助](../Page/玻利維亞共產黨.md "wikilink")。這時，用來與古巴聯絡的兩台短波發射機損壞令他無法寄發消息到[哈瓦那](../Page/哈瓦那.md "wikilink")，游擊隊員用來為從哈瓦那發出的短波解碼的錄音機亦在渡河中亦丟失了，這令格瓦拉完全地被隔離。格瓦拉一廂情願的所謂革命最終不但沒有得到人民甚至玻利維亞共產黨的支持反而使民心倒向政府一方，玻利維亞政府的軍事行動在美國的援助之下節節勝利，令格瓦拉的形勢顯得十分不妙。

## 结局

1967年10月在玻利維亞[拉伊格拉](../Page/拉伊格拉.md "wikilink")，格瓦拉游擊隊中的一個逃兵向玻利維亞特種部隊透露了格瓦拉游擊隊的營地。10月8日，當格瓦拉在拉伊格拉附近帶領巡邏，特種部隊包圍了營地並且捉住了他。他在他的腿受傷後投降。他被捕獲時身份仍然不為人所知。當巴里恩托斯知道他被擒馬上命令處死他。格瓦拉被囚在一個破落的校舍一夜，后被审问，但其拒絕回答任何问题。其中，审讯者问，你现在在想什么？格瓦拉回答：“我在想，革命是不朽的。”

9日下午，他手被綁在板上，被一個抽中籤的玻利維亞軍隊中陸軍[中士马里奥](../Page/中士.md "wikilink")·特兰射殺。有人認為那個中士是向格瓦拉的面和喉頭開槍；而受到廣泛認同的說法則是，他開槍射擊格瓦拉的雙腿，令他的面孔完整以便證明身份，並假裝是作戰的創傷以隱瞞他被槍斃的事實。子弹首先击中了格瓦拉的手臂和腿部，他痛苦的倒地，并咬住他的一个手腕以避免因痛苦而大喊，特兰随后开枪多次，于下午1点10分打中了格瓦拉的胸部，这是致命的一枪。格瓦拉一共被射中9枪。5次射中腿部，1次射中右肩和手臂，1次射中胸部，最后1枪射中喉部。切·格瓦拉在他的死亡之前曾向那中士說過：「我知道你要在這裡殺我。開槍吧！懦夫，你只不過是殺了一個人」。他的屍體被直昇機送到了一個地方醫院和展示予傳媒。那時被拍攝照片成為了一個傳奇，很多人頌揚他為[聖人](../Page/聖人.md "wikilink")。在一名軍醫切斷了他的手之後，玻利維亞的陸軍將校將格瓦拉的屍首轉運去一個秘密地方，並拒絕透露他的遺骸是否已被掩埋或[火化](../Page/火化.md "wikilink")。

然而負責處死切·格瓦拉的士官2007年接受[古巴醫生提供的免費醫療而治好白內障重見光明](../Page/古巴.md "wikilink")，諷刺的是這免費醫療是切·格瓦拉生前所推廣的，這士官透露他槍殺切·格瓦拉時先打腿部和腹部，然後本身也是醫師的切·格瓦拉仍忍痛自己指著胸口要他射擊，十分英勇。

玻利維亞搜捕切·格瓦拉的[中央情報局情報部門長官菲利克斯](../Page/中央情報局.md "wikilink")·罗德里格斯在聽說切·格瓦拉捕獲的消息後將消息經由在南美各國中央情报局駐地然後才傳回至中央情报局在[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")[兰利的總部](../Page/兰利_\(弗吉尼亚\).md "wikilink")，將他的死訊公布天下這做法，最主要的目的便是令游擊隊放棄幻想。在處決後，罗德里格斯還取了切·格瓦拉的[勞力士手錶和幾樣私人物品](../Page/勞力士.md "wikilink")，在往後日子裏經常驕傲地展示在記者面前。\[15\]

其他游擊隊員被拘捕后也被審問，其中有一位年輕的[法國](../Page/法國.md "wikilink")[馬克思主義作家](../Page/馬克思主義.md "wikilink")、卡斯特罗的親密朋友[雷吉斯·德布雷](../Page/雷吉斯·德布雷.md "wikilink")。1967年4月，政府勢力捉了德布雷，並且指責他與游擊隊合作。德布雷聲稱他只是記者，並且透露切·格瓦拉早期神奇消失的幾年是去帶領游擊隊。玻利維亞在10月上旬開始對德布雷的審判，在當時成為了一個國際事件。當局在10月11日正式報告了切·格瓦拉在10月9日被殺。10月15日卡斯特罗宣告古巴全國哀悼三天。切·格瓦拉之死被認為代表了當時[社會主義革命運動思想遍及至](../Page/社會主義.md "wikilink")[拉丁美洲和其他](../Page/拉丁美洲.md "wikilink")[第三世界國家](../Page/第三世界國家.md "wikilink")。

1997年，切·格瓦拉的無手身體骸骨在巴耶格兰德被掘出，由[DNA辨認吻合](../Page/DNA.md "wikilink")，並運返[古巴](../Page/古巴.md "wikilink")。在1997年10月17日，他的遺體以頂級軍事榮譽安葬在[聖克拉拉一個修造的陵墓](../Page/聖克拉拉.md "wikilink")，以紀念他在三十九年前贏取了聖克拉拉[古巴革命的決戰](../Page/古巴革命.md "wikilink")。

## 玻利維亞日記

玻利維亞日記記述了游擊隊在玻利維亞革命活動。第一篇日記寫於1966年11月7日，是他到來之後在尼阿卡瓦苏農場寫的，最後一篇寫於1967年10月7日，是他被擒的前一天。日記敘述了游擊隊怎樣開始運作以及被政府軍發現，解釋切·格瓦拉怎樣決定劃分二個縱隊但隨後無法重建聯繫，並且描述他們的整體失敗。它記錄了切·格瓦拉和[玻利維亞共產黨之間的不和導致切](../Page/玻利維亞共產黨.md "wikilink")·格瓦拉游擊隊士兵比最初期望少。日記上敘述了切·格瓦拉游擊隊因語言障礙問題而難以由地方民眾招募新兵。革命活動亦隨著切·格瓦拉變得越來越不適而式微。他的[哮喘在死前曾經大幅惡化](../Page/哮喘.md "wikilink")。玻利維亞日記由《Ramparts》雜誌迅速及未加修飾地翻譯後在世界上流通。他的革命挚友，時任古巴总理[菲德尔·卡斯特罗亦曾介入翻譯](../Page/菲德尔·卡斯特罗.md "wikilink")。

## 英雄

切·格瓦拉死后，随着他的尸体的照片的传播，切·格瓦拉的事迹也开始广泛为人所知。全球范围内发生了抗议将其杀害的示威，同时出现了许多颂扬他，和记录他生平以及死亡的文学作品。即便是一些对切·格瓦拉共产主义理想嗤之以鼻的自由人士也对其自我牺牲精神表达了由衷的钦佩。他之所以被广大西方年青人与其他革命者区别对待，原因就在于他为了全世界的革命事业而毅然放弃舒适的家境。当他在古巴大权在握时，他又为了自己的理想放弃了高官厚禄，重返革命战场，并战斗直至牺牲。

特别是在60年代晚期，在中东和西方的年轻人中，他成为一个[公众偶像化的革命的象征和左翼政治理想的代名词](../Page/大众文化.md "wikilink")。[阿尔贝托·科尔达在](../Page/阿尔贝托·科尔达.md "wikilink")1960年为切·格瓦拉拍摄的相片《[英勇的游擊隊員](../Page/英勇的游擊隊員.md "wikilink")》迅速成为20世纪最知名的图片之一而；这幅切·格瓦拉的人像，也被简化并复制成为许多商品（比如T恤衫、海报和棒球帽）上的图案。切·格瓦拉的声望甚至延伸到了舞台上，在[蒂姆·莱斯和](../Page/蒂姆·莱斯.md "wikilink")[安德魯·洛伊·韋伯的音乐剧](../Page/安德魯·洛伊·韋伯.md "wikilink")《[贝隆夫人](../Page/贝隆夫人.md "wikilink")》中他成为了旁白者。该音乐剧讲述了切·格瓦拉由于[胡安·贝隆的受贿和专制](../Page/胡安·贝隆.md "wikilink")，而对贝隆夫人和她的丈夫感到失望。这个旁白者的角色是虚构的，因为切·格瓦拉与贝隆夫人并不是同一时代的人物，而且他一生中唯一一次与埃娃·贝隆有关的事情是他在孩童时代曾经给贝隆夫人写过信，信中向贝隆夫人索取一辆[吉普车](../Page/吉普车.md "wikilink")。

[Che_Guevara_-_Grab_in_Santa_Clara,_Kuba.jpg](https://zh.wikipedia.org/wiki/File:Che_Guevara_-_Grab_in_Santa_Clara,_Kuba.jpg "fig:Che_Guevara_-_Grab_in_Santa_Clara,_Kuba.jpg")

切·格瓦拉的遗体，同其他六个一同在玻利维亚战斗的同志的遗体一道，于1997年被安置于一个叫埃内斯托·格瓦拉司令广场（Plaza
Comandante Ernesto
Guevara）特别的陵墓之中。该陵墓位于古巴[圣克拉拉](../Page/圣克拉拉.md "wikilink")，2004年，大约205,832人参观了切·格瓦拉的陵墓，其中127,597人是外国人，包括来自美国、阿根廷、加拿大、英国、德国、意大利等国的游客。该处陈列了切·格瓦拉写给卡斯特罗的道别信（信中，切·格瓦拉宣称他将切断与古巴的一切联系，投身于世界其他角落的革命运动）的原稿。

[法國](../Page/法國.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[让-保罗·萨特稱許切](../Page/让-保罗·萨特.md "wikilink")·格瓦拉是“我們時代的完人”，切·格瓦拉的支持者认为，切·格瓦拉被证明是继拉美独立运动的领导者[西蒙·玻利瓦尔之后](../Page/西蒙·玻利瓦尔.md "wikilink")，拉丁美洲最伟大的思想家和革命家。

## “圣埃内斯托”

切·格瓦拉几乎是从被处决那一刻之后就被人神化成了[圣人一般的人物](../Page/圣人.md "wikilink")。为切·格瓦拉清理尸体的玻利维亚护士苏珊娜·奥西纳伽（Susana
Osinaga）日后曾回忆当地人都惊讶的发现死不瞑目的格瓦拉“就像[耶稣一样](../Page/耶稣.md "wikilink")，有着坚毅的眼睛、胡须、长髮”。《[纽约客](../Page/纽约客.md "wikilink")》记者乔恩·安德森（Jon
Lee Anderson）在他的传记《切·格瓦拉：革命的一生》（*Che Guevara: A Revolutionary
Life*）中也记录到当时这种“格瓦拉与耶稣神似”看法的传播速度之快，以至于虔诚天主教的当地妇女及医院修女纷纷前来收集格瓦拉的头发做护身符。墨西哥学者[豪尔赫·卡斯塔涅达](../Page/豪尔赫·卡斯塔涅达.md "wikilink")（Jorge
G. Castañeda）在《同志：切·格瓦拉的生与死》（*Compañero: The Life and Death of Che
Guevara*）中则描述格瓦拉死后“如基督一般的形象”看起来“就好像死去的格瓦拉在注视着杀他的凶手并宽恕了他们，同时俯视着世界宣示着为理想而死的他已经超脱了痛苦”。

切·格瓦拉死后11天，美国记者[以撒多·史東通过比较指出格瓦拉](../Page/以撒多·史東.md "wikilink")“有着发红发卷的胡须，看起来就像[法翁与](../Page/法翁.md "wikilink")[主日学耶稣画像的结合](../Page/主日学.md "wikilink")”。德国画家/剧作家[彼得·魏斯也指出死后照片中的格瓦拉就像](../Page/彼得·魏斯.md "wikilink")“被从十字架上放下来的基督”。大卫·昆泽（David
Kunzle）也在《切·格瓦拉：偶像、神话和启示》（*Che Guevara: Icon, Myth, and
Message*）一书中把格瓦拉死前最后一张照片——被俘后双手捆绑于身前站立在CIA特工头目费雷克斯·罗德里格斯（Félix
Rodríguez）旁边示众的照相，比喻成了基督教名画“瞧！这个人”（Ecce
Homo，源于[庞提乌斯·彼拉多对被双手捆绑](../Page/本丢·彼拉多.md "wikilink")、即将被钉上十字架的耶稣的称呼）中的情景。昆泽还进一步评论：“基督的事迹很容易在切（格瓦拉）的一生中找到相似点。两人都是医者——基督是用[神迹治病](../Page/耶稣的神迹.md "wikilink")，切则是受过（正规）培训的医生，而且即便在战斗中仍在济世。两人都对贫困人群的[麻风病特殊关怀](../Page/麻风病.md "wikilink")，就像《摩托车日记》中描写的那样。像切一样，耶稣也是[平等主义者](../Page/平等原则.md "wikilink")，从少私有多共享这方面来说算得上是[共产主义者](../Page/共产主义者.md "wikilink")，而他的[徒弟们也被指示天下为公](../Page/圣徒.md "wikilink")。两人都是严格的纪律信奉者，都主张他人离开家庭亲友和优势特权去加入他们，去牺牲舒适生活甚至自己的生命。”

1968年八月，同格瓦拉一同在玻利维亚被捕的法国学者[雷吉斯·德布雷](../Page/雷吉斯·德布雷.md "wikilink")（Jules
Régis Debray）在狱中接受美国《壁垒》（Ramparts）杂志记者玛琳·奈德尔（Marlene
Nadle）采访时也做出了同样的比较，形容身为[无神论者的格瓦拉是](../Page/无神论者.md "wikilink")“没有超然信仰的神秘主义者、不信上帝的圣人”，并且评价：“切（格瓦拉）是现代的基督，但是我认为他的受难更残酷。2000年前的基督死时得以直面他的上帝，但是切却知道世上本无神，而且死后什么都留不下。”在电影《[切·格瓦拉](../Page/切·格瓦拉_\(电影\).md "wikilink")》中扮演格瓦拉本人的[波多黎各演员本尼西奥](../Page/波多黎各.md "wikilink")·德尔·托罗（Benicio
del
Toro）声称：“我认为切（格瓦拉）有着毅力和品德……身居下风、与不公正作战、为被遗忘的人出面都对他感触很大。在某种方面就像耶稣一样——只不过耶稣会转过另一面脸（让人打），切则不会。”英国记者伊莎贝尔·希尔顿（Isabel
Nancy Hilton）则在2007年《新政治家》（New
Statesman）杂志中说道：“切（格瓦拉）的魅力是情感上的。他在玻利维亚相对年轻的身死把他塑造成了世俗的基督——背负世上的罪孽并为了受压迫者献出生命。他的记忆不朽于受压迫者之间，他的形象继续鼓舞着变革的希望和反抗的坚毅，没有被他失败所削弱，反而得到了增强。基督也在人间失败了，而像基督一样，切的死传达了通过鼓舞人心达到救赎的希望。”

格瓦拉死后30年，西方记者开始返回玻利维亚报导对格瓦拉的纪念活动，从而发现格瓦拉已经被当地农户供奉为“圣埃尼斯托”（San Ernesto de
La
Higuera）。格瓦拉死后40年，记者则发现格瓦拉的画像经常被当地人与耶稣、[圣母玛利亚和](../Page/圣母玛利亚.md "wikilink")[约翰保罗二世挂在一起](../Page/约翰保罗二世.md "wikilink")。[路透社专栏记者克里斯托弗](../Page/路透社.md "wikilink")·洛普（Christopher
Roper）也注意到“在玻利维亚，切的尸体被和[圣若翰洗者相提并论](../Page/圣若翰洗者.md "wikilink")”。《[洛杉矶时报](../Page/洛杉矶时报.md "wikilink")》也报导许多当地乡下人遇到麻烦时会念着格瓦拉的名号祈祷，据说“百呼百应”，甚至有人声称有治愈残疾的功效。关于当地人对格瓦拉崇拜的消息于2006年被Isabel
Santos制作成电影，并获得第五届国际人权电影节最佳短纪实片奖。英国《[卫报](../Page/卫报.md "wikilink")》2007年报導，在Vallegrande镇用来把格瓦拉尸体向全球媒体示众的洗衣房如今已经变成了一处“朝圣地”，四周的墙壁上刻满了慕名而来的人留下的字句。在摆放格瓦拉尸体的桌子上方则被用大字刻上了“被怀念者永垂不朽”的标语。而在当地则有“切之诅咒”的传闻，原因是与格瓦拉之死有关的玻利维亚军政人员中已有六人凶死，其中雷内·巴里恩托斯（René
Barrientos）总统死于直升机坠毁，而负责逮捕格瓦拉的格雷·普拉多（Gary Prado）将军则因为自己的枪枝走火击中脊柱致残。

## 批评

虽然切·格瓦拉被许多人视为英雄，但他的反对者们在他的遗产中发现了他们眼中切·格瓦拉一生中不那么光彩的部分，他们认为切·格瓦拉热衷于处死古巴革命的反对者，是不折不扣的劊子手，披著共產主義外衣的法西斯。一些切·格瓦拉的著作被作为这种狂热的证据，其中的一些被Alvaro
Vargas Llosa（他的众多坚决反对者中的一个）所引用。比如，在切·格瓦拉的*Message to the
Tricontinental*一文中，他写道：“仇恨是斗争的一个要素，对敌人刻骨的仇恨能够让一个人超越他的生理极限，成为一个有效率的，暴力的，有选择性的，冷血的杀戮机器”。\[16\]

美国小報《[太阳报](../Page/太阳报.md "wikilink")》作家Williams
Myers给切·格瓦拉贴上了一个“反社会的暴徒（sociopathic
thug）”的标签。其它一些美国报纸的批评家也有同样的评价。这些批评家声称切·格瓦拉本人应该为古巴监狱中数百人受酷刑和被处死，以及他领导的古巴革命武装控制或造访过的地区許多农民被谋杀之事负责。他们也相信切·格瓦拉是一个拙劣的战术家，而不是一个革命天才，他从没取得过一场有记录的战斗的胜利。一些批评家认为切·格瓦拉在阿根廷读医学院时是失败的，没有证据表明他真的获得了医学学位。

而对格瓦拉及[格瓦拉主义的革命方式批评来自于已故中国总理](../Page/格瓦拉主义.md "wikilink")[周恩来](../Page/周恩来.md "wikilink")，他在与时任中共中央对外联络部部长的耿飚的谈话中指出：格瓦拉是“盲动主义者”，他“脱离群众，不要党的领导”，在古巴获得偶然性胜利后没有认真进行总结，就跑出去盲目地推销经验。试图“不依靠长期坚持武装斗争，不建立农村根据地，不走以农村包围城市的道路，来逐步取得胜利”，而是“不管有无条件，以为只要放一把火就可以烧起来，这完全是冒险主义和拼命主义”，“主张到处点火。我们说“星星之火”是可以燎原的，但必须那个地方有了燎原之势，才能燎原。”结果给革命造成了重大的损失，害人害己。格瓦拉的英雄形象，很大一部分是由于青年的偶像崇拜心理和知识分子开始时“分辨不清”给抬举出来的。\[17\]

## 相关作品

《古巴战士》（），由日本[SNK于](../Page/SNK.md "wikilink")1987年制作的[街机动作射击游戏](../Page/街机.md "wikilink")，1988年移植到[任天堂](../Page/任天堂.md "wikilink")[红白机](../Page/红白机.md "wikilink")。该作以[古巴革命为背景](../Page/古巴革命.md "wikilink")，玩家可以操作1P的切·格瓦拉和2P的[菲德尔·卡斯特罗](../Page/菲德尔·卡斯特罗.md "wikilink")，最终目标是打倒[富尔亨西奥·巴蒂斯塔](../Page/富尔亨西奥·巴蒂斯塔.md "wikilink")。

《[摩托日记](../Page/摩托日记_\(电影\).md "wikilink")》，主演：罗德里戈·戴拉·塞纳（Rodrigo De la
Serna）、盖尔·加西亚·埃布尔纳（Gael Garcia Bernal）、米娅·梅斯特罗（Mia
Maestro）。导演：[沃尔特·萨雷斯](../Page/沃尔特·萨雷斯.md "wikilink")（Walter
Salles），2004年上映。

《切·格瓦拉》（上、下两集，Che: Part One）和（Che: Part
Two），导演：[斯蒂芬·索德伯格](../Page/斯蒂芬·索德伯格.md "wikilink")，主演：[本尼西奥·德尔·托罗](../Page/本尼西奥·德尔·托罗.md "wikilink")，罗德里格·桑托罗（Rodrigo
Santoro）。该片获得2008年第61届[戛纳电影节最佳男主角奖](../Page/戛纳电影节.md "wikilink")（[本尼西奥·德尔·托罗饰演切](../Page/本尼西奥·德尔·托罗.md "wikilink")·格瓦拉），并获得金棕榈奖提名。

《[直到永远，指挥官](../Page/直到永远，指挥官.md "wikilink")》和《写给切的信》（Carta al
Che）是最有影响的赞颂切·格瓦拉的歌曲，由古巴[创作歌手](../Page/创作歌手.md "wikilink")[卡洛斯·普埃布拉写成](../Page/卡洛斯·普埃布拉.md "wikilink")，其中对前者的翻唱更是跨越了国界和多种音乐风格，最流行的版本由[法国歌手](../Page/法国.md "wikilink")[娜塔莉·卡东演唱](../Page/娜塔莉·卡东.md "wikilink")。

台灣搖滾樂團《[五月天](../Page/五月天.md "wikilink")》的歌曲《
[摩托車日記](http://www.youtube.com/watch?v=z5Cg0XzGg10)
》歌詞跟切·格瓦拉有關聯，歌曲收錄在《[為爱而生](../Page/為爱而生.md "wikilink")》。

## 參见

  - [格瓦拉主義](../Page/格瓦拉主義.md "wikilink")
  - [英勇的游擊隊員](../Page/英勇的游擊隊員.md "wikilink")
  - [切·格瓦拉 (电影)](../Page/切·格瓦拉_\(电影\).md "wikilink")

## 外鏈

[關於切·格瓦拉的照片](https://www.marxists.org/chinese/albums/guevara/index.htm)

## 註釋

## 參考資料

  - <cite id=refAlekseev1984> Alekseev, Aleksandr（1984年10月）. "Cuba
    después del triunfo de la revolución"（"Cuba after the triumph of
    the revolution"）. Moscow: *America Latina*.</cite>
  - <cite id=refAlmudevar2007> [Almudevar,
    Lola](../Page/Lola_Almudevar.md "wikilink")（2007年10月9日）. "[Bolivia
    marks capture, execution of 'Che' Guevara 40 years
    ago](http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2007/10/09/MNVASLK4R.DTL&feed=rss.news)".
    *San Francisco Chronicle*.</cite>
  - <cite id=refAnderson1997>[Anderson, Jon
    Lee](../Page/Jon_Lee_Anderson.md "wikilink")（1997）. *Che Guevara: A
    Revolutionary Life*. New York: Grove Press. ISBN
    978-0-8021-1600-0.</cite>
  - <cite id=refBamford2002> [Bamford,
    James](../Page/James_Bamford.md "wikilink")（2002）. *Body of Secrets:
    Anatomy of the Ultra-Secret National Security Agency*（Reprint
    edition）. New York: Anchor Books. ISBN 978-0-385-49908-8.</cite>
  - <cite id=refBBCNews2001a> BBC News（2001年1月17日）. "[Profile: Laurent
    Kabila](http://news.bbc.co.uk/2/hi/africa/1121068.stm)". Accessed
    April 10, 2008.</cite>
  - <cite id=refBBCNews2001b> [BBC
    News](../Page/BBC_News.md "wikilink")（2001年5月26日）. *[Che Guevara
    photographer
    dies](http://news.bbc.co.uk/1/hi/world/americas/1352650.stm)*.
    Accessed January 4, 2006.</cite>
  - <cite id=refBBCNews2007> BBC News（2007年10月9日）. "[Cuba pays tribute
    to Che Guevara](http://news.bbc.co.uk/2/hi/americas/7033880.stm)".
    *BBC News, International version*.</cite>
  - <cite id=refBeaubien2009></cite>
  - <cite id=refBenBella1997> Ben Bella, Ahmed（1997年10月）. "[Che as I
    knew him](http://mondediplo.com/1997/10/che)". *Le Monde
    diplomatique*. mondediplo.com. Accessed February 28, 2008.</cite>
  - <cite id=refBockman1984> Bockman, USMC Major Larry James（1984年4月1日）.
    *[The Spirit of Moncada: Fidel Castro's Rise to
    Power 1953-1959](http://www.globalsecurity.org/military/library/report/1984/BLJ.htm)*.
    United States: Marine Corps Command and Staff College.</cite>
  - <cite id=refCasey2009></cite>
  - <cite id=refCastaneda1998> Castañeda, Jorge G (1998). *Che Guevara:
    Compañero*. New York: Random House. ISBN 978-0-679-75940-9.</cite>
  - <cite id=refCastro1972> [Castro,
    Fidel](../Page/Fidel_Castro.md "wikilink")（editors Bonachea, Rolando
    E. and Nelson P. Valdés; 1972）. *Revolutionary Struggle 1947–1958*.
    Cambridge, Massachusetts and London: MIT Press. ISBN
    978-0-262-02065-7.</cite>
  - <cite id=refCubanArchives> Cuban Information Archives. "[La Coubre
    explodes in
    Havana 1960](http://cuban-exile.com/doc_151-175/doc0166.html)".
    Accessed February 26, 2006; pictures can be seen at Cuban site
    [fotospl.com](http://www.fotospl.com/Default.aspx?Class=23&Epig=001~01&PA=18).</cite>
  - <cite id=refDePalma2006> DePalma, Anthony (2006). *The Man Who
    Invented Fidel: Castro, Cuba, and Herbert L. Matthews of the New
    York Times*. New York: Public Affairs. ISBN
    978-1-58648-332-6.</cite>
  - <cite id=refDorfman1999> [Dorfman,
    Ariel](../Page/Ariel_Dorfman.md "wikilink")（1999年6月14日）. *[Time 100:
    Che
    Guevara](http://www.time.com/time/time100/heroes/profile/guevara01.html)*.
    Time Inc.</cite>
  - <cite id=refDorschner1980> Dorschner, John and Roberto Fabricio
    (1980). *The Winds of December: The Cuban Revolution of 1958*. New
    York: Coward, McCann & Geoghegen. ISBN 978-0-698-10993-3.</cite>
  - <cite id=refDRivera2005> [D'Rivera,
    Paquito](../Page/Paquito_D'Rivera.md "wikilink")（March 25, 2005）.
    "[Open letter to Carlos Santana by Paquito
    D'Rivera](http://www.findarticles.com/p/articles/mi_m0FXV/is_4_15/ai_n13801099)".
    *Latin Beat Magazine*. Accessed June 18, 2006.</cite>
  - <cite id=refDumur1964> Dumur, Jean (interviewer)（1964）.
    *[L'interview de Che
    Guevara](http://www.youtube.com/watch?v=y5X0L_SPgoE)*（Video clip;
    9:43）.</cite>
  - <cite id=refGalvez1999> Gálvez, William (1999). *Che in Africa: Che
    Guevara's Congo Diary*. Melbourne: Ocean Press, 1999. ISBN
    978-1-876175-08-5.</cite>
  - <cite id=refTreto1991> Gómez Treto, Raúl（1991年春季）. "[Thirty Years of
    Cuban Revolutionary Penal
    Law](http://www.jstor.org/stable/2633612)". *Latin American
    Perspectives* **18（2）**, Cuban Views on the Revolution.
    114-125.</cite>
  - <cite id=refGott2004></cite>
  - <cite id=refGott2005> Gott, Richard（2005年8月11日）. "[Bolivia on the
    Day of the Death of Che
    Guevara](https://web.archive.org/web/20051126071737/http://www.mindfully.org/Reform/2005/Che-Guevara-Gott11aug05.htm)".
    *Le Monde diplomatique*. Accessed February 26, 2006.</cite>
  - <cite id=refGrant2007> Grant, Will（2007年10月8日）. "[CIA man recounts
    Che Guevara's
    death](http://news.bbc.co.uk/2/hi/americas/7027619.stm)". *BBC
    News*. Accessed February 29, 2008.</cite>
  - <cite id=refGuevara1995> Guevara, Ernesto "Che"（1995）. *Motorcycle
    Diaries*. London: Verso Books.</cite>
  - <cite id=refGuevara1996> Guevara, Ernesto "Che"（editor Waters, Mary
    Alice）（1996）. *Episodes of the Cuban Revolutionary War 1956–1958*.
    New York: Pathfinder. ISBN 978-0-87348-824-2.</cite>
  - <cite id=refGuevara1965> Guevara, Ernesto "Che"（1965）. "Che
    Guevara's Farewell Letter".</cite>
  - <cite id=refGuevara1967a> Guevara, Ernesto "Che"（1967a）. "English
    Translation of Complete Text of his *Message to the
    Tricontinental*"</cite>
  - <cite id=refGuevara1967b> Guevara, Ernesto "Che"（1967b）. *"Diario
    (Bolivia)"*. Written 1966–1967. </cite>
  - <cite id=refGuevara1969> Guevara, Ernesto "Che"（editors Bonachea,
    Rolando E. and Nelson P. Valdés; 1969）. *Che: Selected Works of
    Ernesto Guevara*, Cambridge, Massachusetts: MIT Press. ISBN
    978-0-262-52016-4</cite>
  - <cite id=refGuevara2009></cite>
  - <cite id=refGuevara1972> Guevara, Ernesto "Che"（1972）. *Pasajes de
    la guerra revolucionaria*.</cite>
  - <cite id=refGuevara2000> Guevara, Ernesto "Che"（translated from the
    Spanish by Patrick Camiller; 2000）. *The African Dream*. New York:
    Grove Publishers. ISBN 978-0-8021-3834-7.</cite>
  - <cite id=refGuevara2005> Guevara, Ernesto "Che"（2005）. "[Socialism
    and man in
    Cuba](http://www.marxists.org/archive/guevara/1965/03/man-socialism.htm)"（First
    published March 12, 1965 as "From Algiers, for Marcha. The Cuban
    Revolution Today"）. *Che Guevara Reader*.（1997）. Ocean Press. ISBN
    978-1-875284-93-1</cite>
  - <cite id=refGuevaraLynch2000> Guevara Lynch, Ernesto (2000). *Aquí
    va un soldado de América*. Barcelona: Plaza y Janés Editores, S.A.
    ISBN 978-84-01-01327-0.</cite>
  - <cite id=refHall2004> Hall, Kevin (2004). "[In Bolivia, Push for Che
    Tourism Follows Locals'
    Reverence](https://web.archive.org/web/20071121023354/http://www.commondreams.org/headlines04/0817-07.htm)".
    *Common Dreams*. commondreams.org. 2008年9月15日查閱.</cite>
  - <cite id=refHaney2005> Haney, Rich (2005). *Celia Sánchez: The
    Legend of Cuba's Revolutionary Heart*. New York: Algora Pub. ISBN
    978-0-87586-395-5.</cite>
  - <cite id=refHari2007> [Hari,
    Johann](../Page/Johann_Hari.md "wikilink")（2007年10月6日）. "[Johann
    Hari: Should Che be an icon?
    No](http://www.independent.co.uk/opinion/commentators/johann-hari/johann-hari-should-che-be-an-icon-no-394336.html)".
    *[The Independent](../Page/The_Independent.md "wikilink")*.</cite>
  - <cite id=refHart2004> Hart, Joseph (2004). *Che: The Life, Death,
    and Afterlife of a Revolutionary*. New York: Thunder's Mouth Press.
    ISBN 978-1-56025-519-2.</cite>
  - <cite id=refIgnacio2007> Ramonez, Ignacio (2007). Translated by
    Andrew Hurley. *Fidel Castro: My Life* London: Penguin Books. ISBN
    978-0-14-102626-8</cite>
  - <cite id=refIrelandsOwn2000> Ireland's Own（2000年8月12日）. *[From Cuba
    to Congo, Dream to Disaster for Che
    Guevara](http://irelandsown.net/Che2.html)*. Accessed January 11,
    2006.</cite>
  - <cite id=refKellner1989></cite>
  - <cite id=refKornbluh1997> [Kornbluh,
    Peter](../Page/Peter_Kornbluh.md "wikilink")（1997）. *[Electronic
    Briefing Book
    No. 5](http://www.gwu.edu/~nsarchiv/NSAEBB/NSAEBB5/index.html)*.
    National Security Archive. 2007年3月25日查閱.</cite>
  - <cite id=refLacey2007a> Lacey, Mark（2007年10月26日）. "[Lone Bidder Buys
    Strands of Che's Hair at U.S.
    Auction](http://www.nytimes.com/2007/10/26/world/americas/26che.html?_r=2&oref=slogin&oref=slogin)".
    *New York Times*.</cite>
  - <cite id=refLacey2007b> Lacey, Mark（2007年10月9日）. "[A Revolutionary
    Icon, and Now, a
    Bikini](http://www.nytimes.com/2007/10/09/world/americas/09che.html?_r=1&oref=slogin)".
    *The New York Times*.</cite>
  - <cite id=refLago>Lago, Armando M（2005年9月）. "". *Cuba: the Human Cost
    of Social Revolution*.（Manuscript pending publication.）Summit, New
    Jersey: Free Society Project. </cite>
  - <cite id="refLavretsky1976"></cite>
  - <cite id=refMcLaren2000></cite>
  - <cite id=refMittleman1981> Mittleman, James H (1981).
    *Underdevelopment and the Transition to Socialism – Mozambique and
    Tanzania*. New York: Academic Press. ISBN 978-0-12-500660-6</cite>
  - <cite id=refMoynihan2006> Moynihan, Michael. "Neutering Sartre at
    Dagens Nyheter". *Stockholm Spectator*. Accessed February 26,
    2006.</cite>
  - <cite id=refMurray2007>[Murray,
    Edmundo](../Page/Edmundo_Murray.md "wikilink")（November-December
    2005）. "[Guevara, Ernesto
    \[Che\]（1928–1967）](http://www.irlandeses.org/dilab_guevarae.htm)".
    *Irish Migration Studies in Latin
    America*（www.irlandeses.org）.</cite>
  - <cite id=refNiess2007> Che Guevara, by Frank Niess, Haus Publishers
    Ltd, 2007, ISBN 978-1-904341-99-4 </cite>
  - <cite id=refNiwata2007>Niwata, Manabu, Mainichi
    correspondent（October 14, 2007）. *[Aide reveals Che Guevara's
    secret trip to
    Hiroshima](https://web.archive.org/web/20090215141505/http://www.hdrjapan.com/japan/japan-news/aide-reveals-che-guevara's-secret-trip-to-hiroshima/)*.
    HDR Japan.</cite>
  - <cite id=refOHagan2004> O'Hagan, Sean（2004年7月11日）. "[Just a pretty
    face?](http://observer.guardian.co.uk/review/story/0,6903,1258340,00.html)".
    *[The Guardian](../Page/The_Guardian.md "wikilink")*.
    2006年10月25日查閱.</cite>
  - <cite id=refRadioCadena2006> Radio Cadena Agramonte, "[Ataque al
    cuartel del
    Bayamo](https://web.archive.org/web/20090418032227/http://www.cadenagramonte.cubaweb.cu/historia/cuartel_bayamo.asp)".
    2006年2月25日查閱.</cite>
  - <cite id=refRamirez1997> </cite>
  - <cite id=refIgnacio2007> Ramonez, Ignacio (2007). Translated by
    Andrew Hurley. *Fidel Castro: My Life* London: Penguin Books. ISBN
    978-0-14-102626-8</cite>
  - <cite id=refRatner1997></cite>
  - <cite id=refRodriguez1989>[Rodriguez, Félix
    I.](../Page/Félix_Rodríguez_\(Central_Intelligence_Agency\).md "wikilink")
    and John Weisman (1989). *Shadow Warrior/the CIA Hero of a Hundred
    Unknown Battles*. New York: Simon & Schuster. ISBN
    978-0-671-66721-4.</cite>
  - <cite id=refRyan1998> Ryan, Henry Butterfield (1998). *The Fall of
    Che Guevara: A Story of Soldiers, Spies, and Diplomats*. New York:
    Oxford University Press. ISBN 978-0-19-511879-7.</cite>
  - <cite id=refSandison1996></cite>
  - <cite id=refSchipani2007> Schipani, Andres（September 23, 2007）.
    "[The Final Triumph of Saint
    Che](http://www.guardian.co.uk/world/2007/sep/23/theobserver.worldnews)".
    *The Observer*.（Reporting from La Higuera.）</cite>
  - <cite id=refSelvage1985> Selvage, Major Donald R. – USMC（1985年4月1日）.
    *[Che Guevara in
    Bolivia](http://www.globalsecurity.org/military/library/report/1985/SDR.htm)*.
    Globalsecurity.org. 2006年1月5日查閱.</cite>
  - <cite id=refSinclair1968/06></cite>
  - <cite id=refSnow2007> Snow, Anita（2007年10月8日）. "[Castro Pays Homage
    to Che
    Guevara](http://abcnews.go.com/International/wireStory?id=3703557)".
    *ABC News*.</cite>
  - <cite id=refTaibo1999></cite>
  - <cite id=refTime1970> Time Magazine（1970年10月12日）. "[Che: A Myth
    Embalmed in a Matrix of
    Ignorance](http://www.time.com/time/printout/0,8816,942333,00.html)".</cite>
  - <cite id=refCastrosbrain1960> Time Magazine cover story（1960年8月8日）.
    "[Castro's
    Brain](http://www.time.com/time/printout/0,8816,869742,00.html)".</cite>
  - <cite id=refUSArmy1967> U.S. Army（1967年4月28日）. *[Memorandum of
    Understanding Concerning the Activation, Organization and Training
    of the 2d Ranger Battalion – Bolivian
    Army](http://www.gwu.edu/~nsarchiv/NSAEBB/NSAEBB5/che14_1.htm)*.
    2006年6月19日查閱.</cite>
  - <cite id=refUSDeptState2008> U.S. Department of State. *[Foreign
    Relations,
    Guatemala, 1952–1954](http://www.state.gov/r/pa/ho/frus/ike/guat/20179.htm)*.
    Office of Electronic Information, Bureau of Public Affairs.
    2008年2月29日查閱ˋ.</cite>
  - <cite id=refLlosa2005> [Vargas Llosa,
    Alvaro](../Page/Álvaro_Vargas_Llosa.md "wikilink")（2005年7月11日）.
    "[The Killing Machine: Che Guevara, from Communist Firebrand to
    Capitalist
    Brand](http://www.independent.org/newsroom/article.asp?id=1535)".
    *The Independent Institute*. 2006年11月10日查閱.</cite>
  - <cite id=refPeoplesWeekly2004> "World Combined Sources"（2004年10月2日）.
    "[Che Guevara remains a hero to
    Cubans](http://www.pww.org/article/view/5880/1/234/)". *People's
    Weekly World*.</cite>
  - <cite id=refWright2000></cite>

## 外部链接

  - [Che, Guía y
    Ejemplo:](https://web.archive.org/web/20110910062142/http://www.sancristobal.cult.cu/sitios/Che/Index.htm)
    [相片](https://web.archive.org/web/20060520072317/http://www.sancristobal.cult.cu/sitios/che/Galeria1.HTM)、[歌曲（mp3）](https://web.archive.org/web/20060427043409/http://www.sancristobal.cult.cu/sitios/Che/canciones.htm)、[影像](https://web.archive.org/web/20060426072003/http://www.sancristobal.cult.cu/sitios/Che/Videos.HTM)
  - [切格瓦拉論游擊戰 -
    Yahoo奇摩新聞](https://tw.news.yahoo.com/%E5%88%87%E6%A0%BC%E7%93%A6%E6%8B%89%E8%AB%96%E6%B8%B8%E6%93%8A%E6%88%B0-160944669.html)
  - [Ernesto Che Guevara](http://chehasta.narod.ru/)
  - [捷·古華拉：游擊戰法](http://www.marxists.org/chinese/Guevara/marxist.org-chinese-che-1964.htm)
  - [切格瓦拉論游擊戰，木馬文化事業股份有限公司, 2012](http://www.cp1897.com.hk/product_info.php?BookId=9789866200793)
  - [台灣立報：切格瓦拉論游擊戰2012年12月21日](http://tw.news.yahoo.com/%E5%88%87%E6%A0%BC%E7%93%A6%E6%8B%89%E8%AB%96%E6%B8%B8%E6%93%8A%E6%88%B0-160944669.html)

{{-}}

[切·格瓦拉](../Category/切·格瓦拉.md "wikilink")
[Category:冷戰人物](../Category/冷戰人物.md "wikilink")
[Category:馬克思主義理論家](../Category/馬克思主義理論家.md "wikilink")
[Category:古巴政治人物](../Category/古巴政治人物.md "wikilink")
[Category:曾入獄的領袖](../Category/曾入獄的領袖.md "wikilink")
[Category:共产主义者](../Category/共产主义者.md "wikilink")
[Category:無神論者](../Category/無神論者.md "wikilink")
[Category:古巴革命家](../Category/古巴革命家.md "wikilink")
[Category:阿根廷醫生](../Category/阿根廷醫生.md "wikilink")
[Category:阿根廷作家](../Category/阿根廷作家.md "wikilink")
[Category:布宜諾斯艾利斯大學校友](../Category/布宜諾斯艾利斯大學校友.md "wikilink")
[Category:被美國處決者](../Category/被美國處決者.md "wikilink")
[Category:被玻利維亞處決者](../Category/被玻利維亞處決者.md "wikilink")
[Category:被處決的阿根廷人](../Category/被處決的阿根廷人.md "wikilink")
[Category:愛爾蘭裔阿根廷人](../Category/愛爾蘭裔阿根廷人.md "wikilink")
[Category:西班牙裔阿根廷人](../Category/西班牙裔阿根廷人.md "wikilink")
[Category:革命理論家](../Category/革命理論家.md "wikilink")
[Category:古巴共产党党员](../Category/古巴共产党党员.md "wikilink")
[Category:罗萨里奥人](../Category/罗萨里奥人.md "wikilink")

1.  紀錄在格瓦拉[出生證明上的日期是](../Page/:File:Ernesto_Guevara_Acta_de_Nacimiento.jpg.md "wikilink")1928年6月14日，但另一份第三方資料（胡利亞·康斯坦拉，引用自[喬恩·李·安德森](../Page/喬恩·李·安德森.md "wikilink")），卻主張格瓦拉其實出生於當年5月14日。康斯坦拉聲稱曾有一名身分不明的占星家告訴她，-{zh-hans:切;zh-hk:哲;zh-tw:切;}-的母親西莉亞·德·拉·瑟娜（Celia
    de la Serna）在與父親埃內斯托·格瓦拉·林奇（Ernesto Guevara
    Lynch）結婚時已懷孕，於是在出生證明上將出生日期偽造為下個月後的同一日以掩饰。（[Anderson
    1997](../Page/#refAnderson1997.md "wikilink")，pp. 3，769。）
2.  [On Revolutionary
    Medicine](http://www.marxists.org/archive/guevara/1960/08/19.htm)
    Speech by Che Guevara to the Cuban Militia on August 19, 1960
3.  [At the Afro-Asian Conference in
    Algeria](http://www.marxists.org/archive/guevara/1965/02/24.htm) A
    speech by Che Guevara to the Second Economic Seminar of Afro-Asian
    Solidarity in Algiers, Algeria on February 24, 1965
4.  [Beaubien, NPR Audio Report,
    2009](../Page/#refBeaubien2009.md "wikilink"), 00:09-00:13
5.  *"[Castro's Brain](../Page/#refCastrosbrain1960.md "wikilink")"*
    1960.
6.  [Taibo 1999](../Page/#refTaibo1999.md "wikilink"), p. 267.
7.  [Kellner 1989](../Page/#refKellner1989.md "wikilink"), p. 69-70.
8.  [Anderson 1997](../Page/#refAnderson1997.md "wikilink"), p. 526-530.
9.  [Che Guevara's Final Verdict on the Soviet
    Economy](http://www.globalresearch.ca/index.php?context=va&aid=9315)
    by John Riddell, *Centre for Research on Globalization*, June 13,
    2008.
10. [Ryan 1998](../Page/#refRyan1998.md "wikilink"), p. 4
11. [Casey 2009](../Page/#refCasey2009.md "wikilink"), p. 128.
12. [Dorfman 1999](../Page/#refDorfman1999.md "wikilink").
13. Maryland Institute of Art, referenced at [BBC News May 26,
    2001](../Page/#refBBCNews2001b.md "wikilink")
14.
15.
16. [不值得膜拜的切·格瓦拉](http://view.news.qq.com/zt2012/qgwl/index.htm)
17. 《周恩来<在外事工作会议上的讲话>》1971年5月31日