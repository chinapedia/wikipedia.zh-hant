**甘木車站**（）是位於[日本](../Page/日本.md "wikilink")[九州](../Page/九州.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink")[朝倉市甘木](../Page/朝倉市.md "wikilink")、屬於[甘木鐵道與](../Page/甘木鐵道.md "wikilink")[西日本鐵道](../Page/西日本鐵道.md "wikilink")（西鐵）的[鐵路車站](../Page/鐵路車站.md "wikilink")。雖然名稱相同，但兩家公司的同名車站並不是真的相連在一起，而是保持了約100公尺左右的距離。甘木車站是[甘木鐵道甘木線與](../Page/甘木鐵道甘木線.md "wikilink")[西鐵甘木線的終點站與轉車點](../Page/西鐵甘木線.md "wikilink")，其中甘木鐵道所屬的甘木車站同時也是該公司總社的所在地，而西鐵甘木車站則是該公司現存最古老的車站站房，建於1948年（[昭和](../Page/昭和.md "wikilink")23年）。

## 車站構造

甘木鐵道甘木車站採[平面車站設計](../Page/平面車站.md "wikilink")，配置有兩座[島式月台共三條乘車線](../Page/島式月台.md "wikilink")。

西鐵甘木車站部分，同樣也是平面車站，設置有島式月台一座與兩條乘車線。

## 历史

  - 1921年12月8日 - 配合西日本鐵道甘木線的通車，西鐵甘木車站啟用。
  - 1939年4月28日 - [日本國鐵甘木線通車](../Page/日本國鐵.md "wikilink")，國鐵甘木車站啟用。
  - 1986年4月1日 -
    原屬國鐵的甘木線轉移由新成立的[第三部門鐵路公司甘木鐵道接手營運](../Page/第三部門.md "wikilink")，更名為甘木鐵道甘木線。

## 相鄰車站

  - 甘木鐵道

    甘木線

      -
        [高田](../Page/高田站_\(福岡縣\).md "wikilink")－**甘木**

  - 西日本鐵道

    甘木線

      -

        －**甘木**

## 相關條目

  - [日本鐵路車站列表](../Page/日本鐵路車站列表.md "wikilink")

[Magi](../Category/日本鐵路車站_A.md "wikilink")
[Category:福岡縣鐵路車站](../Category/福岡縣鐵路車站.md "wikilink")
[Category:甘木鐵道車站](../Category/甘木鐵道車站.md "wikilink") [Category:甘木線車站
(西日本鐵道)](../Category/甘木線車站_\(西日本鐵道\).md "wikilink")
[Category:1921年启用的铁路车站](../Category/1921年启用的铁路车站.md "wikilink")
[Category:朝倉市](../Category/朝倉市.md "wikilink")
[Category:日本國有鐵道廢站](../Category/日本國有鐵道廢站.md "wikilink")