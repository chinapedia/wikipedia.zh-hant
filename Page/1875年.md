## 大事记

  - “伤兵救援国际委员会”正式更名为[国际红十字会](../Page/国际红十字会.md "wikilink")。
  - 年僅四歲的載湉登上帝位，年號[光緒](../Page/光緒.md "wikilink")。
  - [亞歷山大·格拉漢姆·貝爾發明](../Page/亞歷山大·格拉漢姆·貝爾.md "wikilink")[電話](../Page/电话.md "wikilink")。
  - 美國[紐約藝術學生聯盟成立](../Page/紐約藝術學生聯盟.md "wikilink")
  - [1月30日](../Page/1月30日.md "wikilink")——[法蘭西第三共和國通過瓦隆憲法修正案](../Page/法蘭西第三共和國.md "wikilink")。
  - [5月3日](../Page/5月3日.md "wikilink")——[左宗棠奉命收复](../Page/左宗棠.md "wikilink")[新疆](../Page/新疆维吾尔自治区.md "wikilink")。
  - 日本政府頒布《[苗字必稱令](../Page/苗字必稱令.md "wikilink")》，規定全體國民必須有[姓](../Page/姓.md "wikilink")，一直沒有姓的平民百姓，開始陸續給自己取姓。
  - 1875年历史宪法。 最终确立了资产阶级共和制.

## 出生

  - [1月2日](../Page/1月2日.md "wikilink")（清[同治十三年十一月二十五日](../Page/同治帝.md "wikilink")）——[沈钧儒](../Page/沈钧儒.md "wikilink")，中國学者。（[1963年逝世](../Page/1963年.md "wikilink")）
  - [2月4日](../Page/2月4日.md "wikilink")——[路德維希·普朗特](../Page/路德維希·普朗特.md "wikilink")，德國力學家。（[1953年逝世](../Page/1953年.md "wikilink")）
  - [2月21日](../Page/2月21日.md "wikilink")——[雅娜·卡爾曼特](../Page/雅娜·卡爾芒.md "wikilink")，有紀錄以來最長壽的女性和人。（[1997年逝世](../Page/1997年.md "wikilink")）
  - [4月23日](../Page/4月23日.md "wikilink")——[上村松園](../Page/上村松園.md "wikilink")，[日本](../Page/日本.md "wikilink")[膠彩畫女画家](../Page/膠彩畫.md "wikilink")。（[1949年逝世](../Page/1949年.md "wikilink")）
  - [11月8日](../Page/11月8日.md "wikilink")——[秋瑾](../Page/秋瑾.md "wikilink")，[中國女革命家](../Page/中国.md "wikilink")。（[1907年逝世](../Page/1907年.md "wikilink")）
  - [陈天华](../Page/陈天华.md "wikilink")，[中国近代革命家](../Page/中国.md "wikilink")。（[1905年逝世](../Page/1905年.md "wikilink")）

## 逝世

  - [1月12日](../Page/1月12日.md "wikilink")——[載淳](../Page/同治帝.md "wikilink")，清穆宗[同治帝](../Page/同治帝.md "wikilink")
  - [1月20日](../Page/1月20日.md "wikilink")——[讓-弗朗索瓦·米勒](../Page/讓-弗朗索瓦·米勒.md "wikilink")，法國[巴比松派畫家](../Page/巴比松派.md "wikilink")。
  - [2月17日](../Page/2月17日.md "wikilink")——[弗里德里希·阿格兰德](../Page/弗里德里希·阿格兰德.md "wikilink")，普鲁士天文学家，编辑出版了[波恩星表](../Page/波恩星表.md "wikilink")
  - [2月22日](../Page/2月22日.md "wikilink")——[讓-巴蒂斯·卡米耶·柯洛](../Page/讓-巴蒂斯·卡米耶·柯洛.md "wikilink")，法國著名的[巴比松派](../Page/巴比松派.md "wikilink")[畫家](../Page/畫家.md "wikilink")。
  - [6月3日](../Page/6月3日.md "wikilink")——[比才](../Page/乔治·比才.md "wikilink")，[法國](../Page/法国.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")
  - [8月4日](../Page/8月4日.md "wikilink")——[安徒生](../Page/漢斯·克里斯蒂安·安徒生.md "wikilink")，[丹麦](../Page/丹麦.md "wikilink")[童话作家](../Page/童話.md "wikilink")（[1805年出生](../Page/1805年.md "wikilink")）
  - [9月9日](../Page/9月9日.md "wikilink")——[義律](../Page/查理·义律.md "wikilink")，[英國](../Page/英国.md "wikilink")[駐華商務總監](../Page/英國駐華商務總監.md "wikilink")。（[1801年出生](../Page/1801年.md "wikilink")）

[\*](../Category/1875年.md "wikilink")
[5年](../Category/1870年代.md "wikilink")
[7](../Category/19世纪各年.md "wikilink")