**科姆兰·马利**（[法语](../Page/法语.md "wikilink")：****，），[多哥政治家](../Page/多哥.md "wikilink")，2007年12月起任[总理](../Page/多哥总理.md "wikilink")。

马利是多哥执政党[多哥人民联盟的中央委员](../Page/多哥人民联盟.md "wikilink")，曾任[瓦瓦省和](../Page/瓦瓦省.md "wikilink")[海湾省省长](../Page/海湾省.md "wikilink")，\[1\]2006年9月获任命为[亚沃维·阿博伊博政府的城镇和城市规划部长](../Page/亚沃维·阿博伊博.md "wikilink")，并任该职位至2007年9月。\[2\]\[3\]2007年马利在[高原区](../Page/高原区_\(多哥\).md "wikilink")[阿穆省当选国会议员](../Page/阿穆省.md "wikilink")，\[4\]阿博伊博总理请辞后，[总统](../Page/多哥总统.md "wikilink")[福雷·纳辛贝在同年](../Page/福雷·纳辛贝.md "wikilink")12月3日任命其为新任总理。\[5\]

## 参考资料

[Category:多哥政治人物](../Category/多哥政治人物.md "wikilink")
[Category:多哥总理](../Category/多哥总理.md "wikilink")

1.

2.
3.

4.

5.