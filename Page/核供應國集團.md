[Nuclear_suppliers_group_membership_map.svg](https://zh.wikipedia.org/wiki/File:Nuclear_suppliers_group_membership_map.svg "fig:Nuclear_suppliers_group_membership_map.svg")

**核供應國集團**（**Nuclear Suppliers
Group**，NSG）為一跨國組織，成立於1974年，希望透過保護、監管、限制出口及轉運等方式，管控可能與[核武相關的原物料及技術](../Page/核武.md "wikilink")，以遏止核武在全球散佈。
该集团通过“核转让准则”及“与核有关的两用设备、材料、软件和相关技术的转让准则”实施出口控制，进口国需要接受[国际原子能机构全面保障监督作为核出口条件](../Page/国际原子能机构.md "wikilink")，严格控制敏感核物项及技术\[1\]的出口；每年召开一次全体会议。\[2\]。

## 历史

2004年5月27日,在[瑞典](../Page/瑞典.md "wikilink")[哥德堡召开的核供应国集团](../Page/哥德堡.md "wikilink")（NSG）全会决定，接纳[中華人民共和國](../Page/中華人民共和國.md "wikilink")、[立陶宛](../Page/立陶宛.md "wikilink")、[爱沙尼亚和](../Page/爱沙尼亚.md "wikilink")[马耳他为新成员](../Page/马耳他.md "wikilink")。

## 成員國

，核供应国集团共有48个成员，包括：\[3\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 参考文献

## 外部連結

  - [核供應國集團](http://www.nuclearsuppliersgroup.org/)

[Category:國際組織](../Category/國際組織.md "wikilink")
[Category:核武器管制](../Category/核武器管制.md "wikilink")
[Category:1974年建立的组织](../Category/1974年建立的组织.md "wikilink")

1.  如后处理、[铀浓缩和](../Page/铀浓缩.md "wikilink")[重水生产](../Page/重水.md "wikilink")
2.
3.  [1](http://www.nuclearsuppliersgroup.org/en/participants1) NUCLEAR
    SUPPLIERS GROUP.