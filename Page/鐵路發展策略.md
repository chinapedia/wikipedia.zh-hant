《**鐵路發展策略**》（）是[香港為未來](../Page/香港.md "wikilink")[鐵路發展的一份文件](../Page/鐵路.md "wikilink")，由當時的運輸科（與現時[決策局的職能對應的是](../Page/決策局.md "wikilink")[運輸及房屋局](../Page/運輸及房屋局.md "wikilink")）在1994年提出的。

在這份文件提出前，與[香港鐵路運輸相關的文件分別有](../Page/香港鐵路運輸.md "wikilink")：分別於1976年及1989年提出的《[香港整體運輸研究](../Page/香港整體運輸研究.md "wikilink")》與《[香港第二次整體運輸研究](../Page/香港第二次整體運輸研究.md "wikilink")》；於1993年提出的《[香港鐵路發展研究](../Page/香港鐵路發展研究.md "wikilink")》。

在這份文件提出後，與[香港鐵路運輸相關的文件分別有](../Page/香港鐵路運輸.md "wikilink")：於2000年5月與2014年9月提出的《[鐵路發展策略2000](../Page/#鐵路發展策略2000.md "wikilink")》與《[鐵路發展策略2014](../Page/#鐵路發展策略2014.md "wikilink")》
。

## 1993年香港鐵路發展研究

於1993年4月，政府委託顧問進行《**香港鐵路發展研究**》。報告中所建議的新鐵路方案，包括：

### 已經通車

#### 西部走廊鐵路

西部走廊連接[新界西與](../Page/新界西.md "wikilink")[九龍](../Page/九龍.md "wikilink")，分為三部份：港口鐵路線、長途客運線及近郊客運線。整個項目被建議優先興建，預計耗資231億港元，2003年落成啟用。

  - 已通車的近郊客運線第一期（即現[港鐵](../Page/港鐵.md "wikilink")[西鐵綫](../Page/西鐵綫.md "wikilink")[屯門站至](../Page/屯門站_\(西鐵綫\).md "wikilink")[南昌站](../Page/南昌站_\(香港\).md "wikilink")）

#### 將軍澳支線

將軍澳支線是配合[將軍澳新市鎮的項目](../Page/將軍澳新市鎮.md "wikilink")，建議於2002年落成啟用，造價84億港元。路線由[觀塘綫的](../Page/觀塘綫.md "wikilink")[藍田站開始](../Page/藍田站.md "wikilink")，沿途設[調景嶺站](../Page/調景嶺站.md "wikilink")、[將軍澳站](../Page/將軍澳站.md "wikilink")、[坑口站及](../Page/坑口站_\(香港\).md "wikilink")[寶林站](../Page/寶琳站.md "wikilink")（其後改為從北角站開始，以寶琳或康城為終點站）。建議還包括在2011年後，由[藍田站西延該線](../Page/藍田站.md "wikilink")，經[東南九龍填海區南部](../Page/東南九龍.md "wikilink")，接駁[馬頭角站](../Page/馬頭角站.md "wikilink")。

  - 已通車的將軍澳支線（將軍澳綫）

#### 馬鞍山至大圍鐵路及尖沙咀支線

馬鞍山至大圍鐵路及九廣鐵路尖沙咀支線，於1993年的研究報告中仍屬東九龍線的一部份。到《鐵路發展策略》發表時，為減省成本，才將這兩個項目優先興建，並交由[九鐵公司興建和營運](../Page/九鐵公司.md "wikilink")。

馬鞍山至大圍鐵路（及後稱為馬鞍山鐵路），由[大圍站開始](../Page/大圍站.md "wikilink")，經[沙田頭站](../Page/車公廟站_\(香港\).md "wikilink")、[沙角街站](../Page/沙田圍站.md "wikilink")、[第一城站](../Page/第一城站.md "wikilink")、[石門站](../Page/石門站_\(香港\).md "wikilink")、[恆安站](../Page/恆安站.md "wikilink")，以[馬鞍山站為終點站](../Page/馬鞍山站_\(香港\).md "wikilink")，為一[中型鐵路系統](../Page/中型鐵路系統.md "wikilink")。

九廣鐵路尖沙咀支線，由[紅磡站延長到](../Page/紅磡站.md "wikilink")[尖沙咀東站](../Page/尖東站.md "wikilink")。長遠則預留延長到[西九龍站的空間](../Page/九龍站.md "wikilink")，與[機場鐵路交匯](../Page/機場快綫.md "wikilink")。

  - 已通車的馬鞍山至大圍鐵路（馬鞍山綫）

<!-- end list -->

  - 已通車的九鐵尖沙咀支線（即現港鐵[西鐵綫](../Page/西鐵綫.md "wikilink")[紅磡站至](../Page/紅磡站.md "wikilink")[尖東站](../Page/尖東站.md "wikilink")）

#### 西鐵尖沙咀至西九龍段

[九廣鐵路尖沙咀至西九龍段](../Page/九廣鐵路.md "wikilink")，因政府決定優先發展九廣鐵路尖沙咀支線而取而代之。定線由[尖沙咀東站經](../Page/尖東站.md "wikilink")[九龍角站往](../Page/九龍角.md "wikilink")[西九龍站](../Page/九龍站.md "wikilink")，接駁[機場鐵路](../Page/機場鐵路.md "wikilink")，屬於長遠方案。

  - 已通車的九鐵尖沙咀至西九龍段（即現港鐵[西鐵綫](../Page/西鐵綫.md "wikilink")[南昌站至](../Page/南昌站_\(香港\).md "wikilink")[尖東站](../Page/尖東站.md "wikilink")）

#### 西港島線

已於28/12/2014通車的[西港島綫屬於長遠方案](../Page/港島綫西延.md "wikilink")，由[上環站延伸至](../Page/上環站.md "wikilink")[青洲填海區](../Page/青洲_\(中西區\).md "wikilink")，中途經[屈地街站及](../Page/大學站_\(香港島\).md "wikilink")[堅尼地城站](../Page/堅尼地城站.md "wikilink")（稱為「上環－青洲線」）。該線的興建須視乎青洲填海區的發展而定。更長遠的方案中，該線由青洲接駁[陰澳站](../Page/欣澳站.md "wikilink")（稱為「陰澳－青洲線」）。

  - 已通車的西港島線（即現港鐵[港島綫](../Page/港島綫.md "wikilink")[上環站至](../Page/上環站.md "wikilink")[堅尼地城站](../Page/堅尼地城站.md "wikilink")）

#### 南港島綫

南港島綫是一個[中型鐵路系統](../Page/中型鐵路.md "wikilink")，定線分為縱線及橫線。縱線在香港島設[太古廣場站](../Page/太古廣場.md "wikilink")（[金鐘南部](../Page/金鐘.md "wikilink")），再與[北港島綫的](../Page/北港島綫.md "wikilink")[會展中心站交匯](../Page/會展站.md "wikilink")，太古廣場站以南則為[田灣站](../Page/田灣站.md "wikilink")、[鴨脷洲西站及](../Page/海怡半島站.md "wikilink")[鴨脷洲東站](../Page/利東站.md "wikilink")。橫線則設[鋼線灣站](../Page/數碼港站.md "wikilink")、[華富站](../Page/華富站.md "wikilink")、[華貴站](../Page/華貴.md "wikilink")、[田灣站](../Page/田灣站.md "wikilink")、[香港仔站](../Page/香港仔站.md "wikilink")、[黃竹坑站及](../Page/黃竹坑站.md "wikilink")[海洋公園站](../Page/海洋公園站_\(香港\).md "wikilink")。雖然並非列入長遠方案，但該線被認為並無逼切需要興建，列為低優先次序（2016年後落成），而造價則估計約58億港元。

後來，經過2000年代的進一步規劃，南海島綫被分成兩段。當中東段在金鐘出發，然後途經[海洋公園站](../Page/海洋公園站_\(香港\).md "wikilink")、[黃竹坑站及](../Page/黃竹坑站.md "wikilink")[利東站](../Page/利東站.md "wikilink")，最後以[海怡半島作總站](../Page/海怡半島.md "wikilink")。其在2011年動工，2016年通車，並直接以[南港島綫稱呼](../Page/南港島綫.md "wikilink")。而西段則由[香港大學站出發](../Page/香港大學站.md "wikilink")，途經[瑪麗醫院站](../Page/瑪麗醫院站.md "wikilink")、[數碼港站](../Page/數碼港站.md "wikilink")、[華富站](../Page/華富站.md "wikilink")、[田灣站及](../Page/田灣站.md "wikilink")[香港仔站](../Page/香港仔站.md "wikilink")，在[黃竹坑站作總站](../Page/黃竹坑站.md "wikilink")，跟東段交匯。初步建議預計在2021至2026年落實。

### 已經擱置或者取消

#### 港口鐵路線

建議中的港口鐵路線將由[羅湖站經](../Page/羅湖站_\(香港\).md "wikilink")[錦田](../Page/錦田.md "wikilink")，穿越隧道到[荃灣](../Page/荃灣.md "wikilink")，接駁[葵涌貨櫃碼頭](../Page/葵涌貨櫃碼頭.md "wikilink")8號碼頭。該線建議於2001年落成，預期啟用時每日處理貨運量為1,450個貨櫃，到2006年則升至2,500個。此外，該線在錦田一帶設置預留位置，長遠接駁[大嶼山港口鐵路線](../Page/大嶼山港口鐵路線.md "wikilink")。

#### 長途客運線

建議中的長途客運線主要是為了分流[九廣鐵路及](../Page/九廣鐵路.md "wikilink")[羅湖管制站的出入境人流](../Page/羅湖管制站.md "wikilink")。其直通車服務的終點站擬設於[西九龍填海區近](../Page/西九龍填海區.md "wikilink")[長沙灣一帶](../Page/長沙灣.md "wikilink")（即今[南昌站位置](../Page/南昌站_\(香港\).md "wikilink")）。在[落馬洲支線管制站建成前](../Page/落馬洲支線管制站.md "wikilink")，該線會接駁到[羅湖站](../Page/羅湖站_\(香港\).md "wikilink")，而當口岸建成時，九廣鐵路亦會從[上水站分出](../Page/上水站.md "wikilink")[支線連接落馬洲](../Page/落馬洲支線.md "wikilink")。

#### 近郊客運線

建議中的近郊客運線將由[荔景站開始](../Page/荔景站.md "wikilink")，經[海濱花園站](../Page/海濱花園.md "wikilink")、[荃灣西站](../Page/荃灣西站.md "wikilink")、[荃景花園站](../Page/荃景花園.md "wikilink")（[荃灣綫亦會延長至此以便乘客轉乘](../Page/荃灣綫.md "wikilink")），經隧道往[錦田站後](../Page/錦上路站.md "wikilink")，分為兩條支線：其中一條經[元朗東站](../Page/元朗站_\(西鐵綫\).md "wikilink")、[朗屏站](../Page/朗屏站.md "wikilink")，以[天水圍站為終點站](../Page/天水圍站_\(西鐵綫\).md "wikilink")；另一條則經[錦繡花園站至](../Page/錦繡花園.md "wikilink")[落馬洲站](../Page/落馬洲站.md "wikilink")。

當局亦曾研究過經荃灣以西海岸往屯門，以及經[赤鱲角往](../Page/赤鱲角.md "wikilink")[屯門的路線方案](../Page/屯門.md "wikilink")，但因無法與長途客運線及港口鐵路線共用路軌，加上建造費用明顯較多，所以不作考慮。

#### 西部外走廊

西部外走廊屬於西部走廊的延伸，連接[香港島](../Page/香港島.md "wikilink")、[大嶼山](../Page/大嶼山.md "wikilink")、[屯門及](../Page/屯門.md "wikilink")[天水圍](../Page/天水圍.md "wikilink")，可分為三個部份：

  - 天水圍－屯門線：該段由西部走廊近郊客運線終點站[天水圍站南延](../Page/天水圍站_\(西鐵綫\).md "wikilink")，經[洪水橋站](../Page/洪水橋站_\(西鐵綫\).md "wikilink")、[屯門北站及](../Page/兆康站_\(西鐵綫\).md "wikilink")[屯門站](../Page/屯門站_\(西鐵綫\).md "wikilink")
    (即現西鐵綫[屯門至](../Page/屯門站_\(西鐵綫\).md "wikilink")[天水圍段](../Page/天水圍站_\(西鐵綫\).md "wikilink"))。
  - 屯門－[陰澳線](../Page/陰澳.md "wikilink")：該段以[屯門站為起點](../Page/屯門站_\(西鐵綫\).md "wikilink")，經[掃管笏站及沉管隧道](../Page/掃管笏.md "wikilink")，接駁[機場鐵路](../Page/大嶼山機場鐵路.md "wikilink")[陰澳站](../Page/欣澳站.md "wikilink")。
  - 陰澳－[青洲線](../Page/青洲_\(中西區\).md "wikilink")：該段由上述路線的[陰澳站南延](../Page/陰澳站.md "wikilink")，經[扒頭鼓站接駁](../Page/扒頭鼓站.md "wikilink")[西港島綫青洲站](../Page/港島綫西延.md "wikilink")。

#### 通往大嶼山的港口鐵路線

大嶼山港口鐵路線是配合大嶼山擬建貨櫃碼頭的計劃。路線由西部走廊港口鐵路線的[羅湖站開始](../Page/羅湖站_\(香港\).md "wikilink")，在[元朗](../Page/元朗.md "wikilink")[坳頭一帶分支](../Page/坳頭.md "wikilink")，經隧道到達[大欖一帶](../Page/大欖.md "wikilink")，再與西部外走廊屯門－陰澳線共用過海隧道，以在大嶼山[竹篙灣](../Page/竹篙灣.md "wikilink")、[扒頭鼓填海至](../Page/扒頭鼓.md "wikilink")[交椅洲的新貨櫃碼頭為終點站](../Page/交椅洲.md "wikilink")。

#### 穿越中九龍東西鐵路

穿越[中九龍的東西鐵路](../Page/中九龍.md "wikilink")（
）在《香港鐵路發展研究》未有提及，在《鐵路發展策略》才作出這個建議。該線定線大致會將[東南九龍填海區經九龍中部與](../Page/東南九龍.md "wikilink")[西九龍填海區連接](../Page/西九龍.md "wikilink")，其中一個可能的定線為[將軍澳支線之西延線](../Page/將軍澳綫.md "wikilink")「藍田－馬頭角線」進一步西延，經[旺角站往大角咀站](../Page/旺角站.md "wikilink")（今[奧運站](../Page/奧運站.md "wikilink")）

## 準備興建

### 北港島線

準備興建的北港島綫是一個[中型鐵路系統](../Page/中型鐵路.md "wikilink")，由[香港站經](../Page/香港站.md "wikilink")[添馬站](../Page/添馬站.md "wikilink")、[會展中心站及](../Page/會展站.md "wikilink")[維園站連接](../Page/維園站.md "wikilink")[天后站](../Page/天后站.md "wikilink")，連接之前南港島線時的方案。後來，改由[香港站經](../Page/香港站.md "wikilink")[添馬站](../Page/添馬站.md "wikilink")、[會展站及](../Page/會展站.md "wikilink")[銅鑼灣北站連接](../Page/銅鑼灣北站.md "wikilink")[北角站](../Page/北角站.md "wikilink")，把東涌綫和將軍澳綫相遇。初步預計將於2021年動工，2026年落成通車。

### 北環綫

  - 準備興建的近郊客運線第二期（北環綫）

## 興建中

### 第四條過海鐵路

第四條過海鐵路在《香港鐵路發展研究》未有提及，在《鐵路發展策略》才作出這個建議。該段鐵路將連接[紅磡至](../Page/紅磡.md "wikilink")[灣仔一帶](../Page/灣仔.md "wikilink")。

### 大圍至市區鐵路

大圍至[市區的鐵路](../Page/香港市區.md "wikilink")，即沙田至中環綫的沙田（大圍站）至紅磡段。

東九龍綫在1993年的《香港鐵路發展研究》及1994年的《鐵路發展策略》有較明顯的分別。在1993年，東九龍綫是一條由[馬鞍山經](../Page/馬鞍山.md "wikilink")[東九龍往](../Page/東九龍.md "wikilink")[西九龍站的](../Page/九龍站.md "wikilink")[中型鐵路系統](../Page/中型鐵路系統.md "wikilink")，預計2011年落成通車，造價257億港元。但在1994年，東九龍綫的首尾兩段分拆出馬鞍山至大圍鐵路及九廣鐵路尖沙咀支線優先興建。

剩下來的路段，包括[大圍站至](../Page/大圍站.md "wikilink")[尖沙咀東站一段](../Page/尖東站.md "wikilink")（中途設[顯徑站](../Page/顯徑站.md "wikilink")、[鑽石山站](../Page/鑽石山站.md "wikilink")、[九龍灣填海區北站](../Page/啟德站.md "wikilink")、九龍灣填海區南站、[馬頭角站](../Page/馬頭角站.md "wikilink")），以及[尖沙咀東站至](../Page/尖東站.md "wikilink")[西九龍站一段](../Page/柯士甸站.md "wikilink")（中途設[九龍角站](../Page/九龍角.md "wikilink")），隨著九廣鐵路尖沙咀支線的興建，於《鐵路發展策略》中被撥歸九廣鐵路長遠延長方案。

## 1998年香港第二次鐵路發展研究

在1998年3月，政府委託顧問進行《**香港第二次鐵路發展研究**》。報告中所建議的新鐵路方案，包括：

1.  [北港島綫](../Page/北港島綫.md "wikilink")
2.  [東九龍綫](../Page/東九龍綫_\(1970年方案\)#鐵路項目分拆及組合（九十年代）.md "wikilink")
3.  [第四條過海鐵路線](../Page/香港第四條過海鐵路.md "wikilink")
4.  [大圍至鑽石山線](../Page/東西綫_\(香港\).md "wikilink")
5.  [九龍南環線](../Page/九龍南綫.md "wikilink")
6.  [北環線](../Page/北環綫.md "wikilink")
7.  [西港島綫](../Page/港島綫西延.md "wikilink")
8.  [區域快線](../Page/廣深港高速鐵路#香港段.md "wikilink")
9.  [港口鐵路線](../Page/港口鐵路線.md "wikilink")

而《鐵路發展策略2000》是政府根據該研究的結果而制定的鐵路網絡擴展計畫。

## 鐵路發展策略2000

《鐵路發展策略2000》是香港[運輸局在](../Page/運輸局.md "wikilink")2000年5月提出的鐵路發展文件，報告中所建議的新鐵路方案包括：

  - 已經擱置或者取消
      - [小西灣綫](../Page/小西灣綫.md "wikilink")
      - [北環綫](../Page/北環綫.md "wikilink")
      - 港口鐵路線

<!-- end list -->

  - 興建中
      - [沙田至中環綫](../Page/沙田至中環綫.md "wikilink")

<!-- end list -->

  - 已經通車
      - [九龍南環線](../Page/九龍南綫.md "wikilink")
      - [西港島線](../Page/西港島線.md "wikilink")
      - [南港島綫](../Page/南港島綫.md "wikilink")
      - [區域快線](../Page/广深港高速铁路.md "wikilink")

報告中提及的長遠方案包括：

  - 鐵路運輸相關的文件分別有：1976年及1989年提出的《香港整體運輸研究》與《香港第二次整體運輸研究》；1993年提出的《香港鐵路發展研究》以及2000年5月提出的《鐵路發展策略2000》。
  - [第五條過海鐵路](../Page/第五條過海鐵路.md "wikilink")
  - [西部外走廊](../Page/西部外走廊.md "wikilink")
  - [后海灣線](../Page/后海灣線.md "wikilink")
  - [赤鱲角線](../Page/赤鱲角線.md "wikilink")
  - [東西九龍線](../Page/東西九龍線.md "wikilink")

### 檢討及修訂《鐵路發展策略2000》

運輸及房屋局於2010年11月提交[立法會鐵路事宜委員會的文件](../Page/立法會.md "wikilink")\[1\]提出檢討及修訂鐵路發展策略2000，以配合[香港社會最新的發展及規劃因素的變更](../Page/香港社會.md "wikilink")，應付直至2031年的鐵路運輸需求。文件亦提及香港未來會繼續以鐵路為公共運輸的骨幹，以減低[環境污染及對土地的需求](../Page/環境污染.md "wikilink")，以維持運輸系統的[可延續發展](../Page/可延續發展.md "wikilink")。

## 鐵路發展策略2014

《[鐵路發展策略2014](../Page/鐵路發展策略2014.md "wikilink")》為《鐵路發展策略2000》的更新版本，由[運輸及房屋局在](../Page/運輸及房屋局.md "wikilink")2014年9月17日提出，為香港鐵路網絡直至2031年的未來擴展制訂發展框架。報告以2011年3月至2014年1月期間進行的《鐵路發展策略2000》之檢討及修訂（「我們未來的鐵路」）的顧問研究及公眾諮詢結果為基礎，所建議的新鐵路方案包括：

[Hk-rds2014-wikipedia.png](https://zh.wikipedia.org/wiki/File:Hk-rds2014-wikipedia.png "fig:Hk-rds2014-wikipedia.png")

  - 建議鐵路網絡項目
      - [北環綫及](../Page/北環綫.md "wikilink")[古洞站](../Page/古洞站.md "wikilink")
      - [洪水橋站](../Page/洪水橋站.md "wikilink")
      - [東涌西延綫](../Page/東涌西延綫.md "wikilink")
      - [屯門南延綫](../Page/屯門南延綫.md "wikilink")
      - [東九龍綫](../Page/東九龍綫_\(2014年方案\).md "wikilink")（[寶琳站至](../Page/寶琳站.md "wikilink")[鑽石山站](../Page/鑽石山站.md "wikilink")）
      - [南港島綫（西段）](../Page/南港島綫（西段）.md "wikilink")
      - [北港島綫](../Page/北港島綫.md "wikilink")

<!-- end list -->

  - 已經擱置或者取消
      - [港深西部快速軌道](../Page/港深西部快速軌道.md "wikilink")
      - 屯門至荃灣沿海鐵路（[屯荃鐵路](../Page/屯荃鐵路.md "wikilink")）
      - [小西灣綫](../Page/小西灣綫.md "wikilink")

## 參考資料

  - 《鐵路發展研究公眾諮詢文件》，香港政府運輸科，1993年4月
  - 《[鐵路發展策略](http://www.ourfuturerailway.hk/doc/Railway_Development_Strategy\(1994\)_Chi.pdf)》，香港政府運輸科，1994年
  - 《[鐵路發展策略2000](http://www.thb.gov.hk/tc/psp/publications/transport/publications/rds.pdf)》，香港運輸局

## 參見

  - [《鐵路發展策略》](http://www.ourfuturerailway.hk/doc/Railway_Development_Strategy\(1994\)_Chi.pdf)

  - [《鐵路發展策略2000》](http://www.thb.gov.hk/tc/psp/publications/transport/publications/rds.pdf)

  - [《鐵路發展策略2014》](http://www.thb.gov.hk/tc/psp/publications/transport/publications/rds2014.pdf)

[1994](../Category/香港鐵路發展策略.md "wikilink")

1.