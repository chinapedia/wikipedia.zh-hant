**同治帝**（），名**载淳**（），[爱新觉罗氏](../Page/爱新觉罗.md "wikilink")
，是[清朝第十位](../Page/清朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")，也是[清兵入关后的第八位皇帝](../Page/清兵入关.md "wikilink")，1861年11月11日至1875年1月12日在位，[年號](../Page/年號.md "wikilink")「[同治](../Page/同治.md "wikilink")」。[西藏方面尊為](../Page/清朝治藏历史.md "wikilink")「**[文殊皇帝](../Page/文殊皇帝.md "wikilink")**」。

同治帝是[咸丰帝长子](../Page/咸丰帝.md "wikilink")，5歲（[虛歲六歲](../Page/虛歲.md "wikilink")）登基，原設年號為「[祺祥](../Page/祺祥.md "wikilink")」，隨後不及一年發生[辛酉政變](../Page/辛酉政變.md "wikilink")，最終由其[嫡母](../Page/嫡母.md "wikilink")[慈安太后與生母](../Page/慈安太后.md "wikilink")[慈禧太后共同](../Page/慈禧太后.md "wikilink")[垂簾聽政](../Page/垂簾聽政.md "wikilink")（史稱「[兩宮聽政](../Page/兩宮聽政.md "wikilink")」），並改設年號為「同治」。1875年驾崩，時年僅18[週歲](../Page/週歲.md "wikilink")。死後[廟號](../Page/廟號.md "wikilink")**穆宗**（），諡號**毅皇帝**（），葬于[清東陵中的](../Page/清東陵.md "wikilink")[惠陵](../Page/清惠陵.md "wikilink")。

## 生平

### 幼年

[咸豐六年三月二十三日](../Page/咸豐_\(年号\).md "wikilink")，生於[儲秀宮](../Page/儲秀宮.md "wikilink")，生母[叶赫那拉氏懿嫔](../Page/慈禧太后.md "wikilink")，即后来的慈禧太后。八年，其异母弟弟[憫郡王早夭](../Page/憫郡王.md "wikilink")，其后载淳一直是咸丰帝唯一存活的儿子。咸丰十一年，载淳开始上学，由[编修](../Page/编修.md "wikilink")[李鸿藻授读](../Page/李鸿藻.md "wikilink")。

### 登基、大婚

咸丰十一年七月，咸丰帝[弥留之际](../Page/弥留.md "wikilink")，立皇长子载淳为[皇太子](../Page/皇太子.md "wikilink")，任命[肃顺等八人赞襄政务](../Page/肃顺.md "wikilink")，稱[顧命八大臣](../Page/顧命八大臣.md "wikilink")。咸丰帝死後，载淳成为继任皇帝。嫡母[皇后钮祜禄氏和生母](../Page/慈安太后.md "wikilink")[懿贵妃那拉氏并尊为](../Page/慈禧太后.md "wikilink")[皇太后](../Page/皇太后.md "wikilink")。此时，[顧命八大臣主政](../Page/顧命八大臣.md "wikilink")，他们为载淳选定[年號](../Page/年號.md "wikilink")——[祺祥](../Page/祺祥.md "wikilink")。不久，两宫皇太后與他的叔叔们——[恭親王](../Page/恭親王.md "wikilink")[奕訢](../Page/奕訢.md "wikilink")、醇郡王[奕譞等人共同發動](../Page/奕譞.md "wikilink")[辛酉政变](../Page/辛酉政变.md "wikilink")，扳倒八大臣。

咸丰十一年九月，上两宫太后[徽号](../Page/徽号.md "wikilink")，称[慈安](../Page/慈安太后.md "wikilink")、[慈禧](../Page/慈禧太后.md "wikilink")。冬十月庚申，下诏改年号祺祥为[同治](../Page/同治.md "wikilink")，以誌“同歸于治”、“君臣同治”、“同于[顺治](../Page/顺治帝.md "wikilink")”（出自《[春秋](../Page/春秋_\(史书\).md "wikilink")》，或譯“母子同治天下”）的[垂簾聽政](../Page/垂簾聽政.md "wikilink")。甲子，载淳在北京[紫禁城](../Page/紫禁城.md "wikilink")[太和殿登基](../Page/太和殿.md "wikilink")，颁诏天下，以第二年为同治元年，故称同治帝。十一月乙酉朔，[嫡母](../Page/嫡母.md "wikilink")[慈安太后](../Page/慈安太后.md "wikilink")、生母[慈禧太后在](../Page/慈禧太后.md "wikilink")[养心殿正式](../Page/养心殿.md "wikilink")[垂帘听政](../Page/垂帘听政.md "wikilink")。登基时，同治帝年仅五岁，故其后一直由慈安太后、慈禧太后垂帘听政，史称[兩宮聽政](../Page/兩宮聽政.md "wikilink")。

同治元年春正月，同治帝在[慈宁宫率王公大臣向两宫太后行礼](../Page/慈宁宫.md "wikilink")，自己则在[乾清宫受贺](../Page/乾清宫.md "wikilink")，此后每年亦如此。二月乙卯，[懿旨同治帝在](../Page/懿旨.md "wikilink")[弘德殿入学读书](../Page/弘德殿.md "wikilink")，[祁寯藻](../Page/祁寯藻.md "wikilink")、[翁心存授读](../Page/翁心存.md "wikilink")。

[同治八年](../Page/同治.md "wikilink")，[清朝政府就已开始为同治帝大婚作准备](../Page/清朝政府.md "wikilink")。三月己亥，懿旨，大婚典礼，力崇节俭。此前，两位幼年继位的清帝——[顺治帝](../Page/顺治帝.md "wikilink")、[康熙帝均在十四岁](../Page/康熙帝.md "wikilink")（虚岁）完成[大婚](../Page/清帝大婚.md "wikilink")，并[亲政](../Page/亲政.md "wikilink")。此时的同治帝已到达他们的年龄，但完成婚礼则是三年后。

同治十一年（1872年）初，清宫[选秀](../Page/八旗选秀.md "wikilink")，从秀女中为同治帝选定一-{后}-四妃。九月十五日（10月16日），举行大婚典礼，正式迎娶[皇后阿鲁特氏](../Page/孝哲毅皇后.md "wikilink")。

### 短暂的亲政及“[同治中興](../Page/同治中興.md "wikilink")”

同治十二年春正月乙巳，两宫太后以亲政届期，颁布[懿旨](../Page/懿旨.md "wikilink")，鼓励同治帝“祇承家法，讲求用人行政，毋荒典学”。勉廷臣及中外臣工“公忠尽职，宏济艰难”。丙午，同治帝[亲政](../Page/亲政.md "wikilink")，下诏“恪遵慈训，敬天法祖，勤政爱民”。

親政时，同治帝年方十八岁。在位期間，[歐洲](../Page/歐洲.md "wikilink")[列強未有入侵](../Page/列強.md "wikilink")，而[太平天國亦已經被消滅](../Page/太平天國.md "wikilink")，清室亦興辦[洋務](../Page/洋務運動.md "wikilink")，頗有發憤圖強之心。此段時期被稱為[同治中興](../Page/同治中興.md "wikilink")。

### 逝世

同治十三年十月己亥，因同治帝有病，命[李鸿藻代阅](../Page/李鸿藻.md "wikilink")[奏章](../Page/奏章.md "wikilink")。十一月，命[恭亲王](../Page/恭亲王.md "wikilink")[奕訢处理批答](../Page/奕訢.md "wikilink")[清文](../Page/清文.md "wikilink")[摺件](../Page/摺件.md "wikilink")。己酉，命内外奏折呈两宫太后披览。十二月初五日（1875年1月12日），同治帝崩於皇宮[養心殿](../Page/養心殿.md "wikilink")，年仅18岁，为清朝[寿命最短的皇帝](../Page/寿命.md "wikilink")。同治无后，慈禧即挑出咸丰之弟[奕譞之子](../Page/奕譞.md "wikilink")[载湉入嗣](../Page/载湉.md "wikilink")[大宗为](../Page/宗法.md "wikilink")[帝](../Page/皇帝.md "wikilink")，是为[德宗](../Page/清德宗.md "wikilink")（光绪帝）。光绪帝亦无子而逝，以[溥仪继承帝位](../Page/溥仪.md "wikilink")，[兼祧两房](../Page/兼祧.md "wikilink")。

據正史記載，同治帝是死於[天花](../Page/天花.md "wikilink")。相同紀錄亦出現於《[翁同龢日記](../Page/翁同龢日記.md "wikilink")》，說同治帝得了天花，導致毒熱內陷，最終“走馬牙疳”而死。

但在民間傳说稱同治死于[梅毒](../Page/梅毒.md "wikilink")。或说同治帝婚后獨宿[乾清宮](../Page/乾清宮.md "wikilink")，在[內監和寵臣](../Page/內監.md "wikilink")[載澂引導下經常](../Page/載澂.md "wikilink")[微服私行](../Page/微服私行.md "wikilink")，常到[崇文門外的酒肆](../Page/崇文門.md "wikilink")、戲館及花巷尋花問柳。野史記載：“伶人小六如、春眉，娼小鳳輩，皆邀幸。”又有人推薦他一些[黃色小說](../Page/黃色小說.md "wikilink")，“小說淫詞，祕戲圖冊，帝益沉迷”\[1\]。據《[清宮遺聞](../Page/清宮遺聞.md "wikilink")》記載，“同治到私娼處，致染梅毒”。而《[清朝野史大观](../Page/清朝野史大观.md "wikilink")》卷一《清宫遗闻》中说：“孝哲后，崇绮之女，端庄贞静，美而有德，帝甚爱之，以格于慈禧之威，不能相款洽，慈禧又强其爱所不爱之妃（指凤秀之女[淑慎皇贵妃](../Page/淑慎皇贵妃.md "wikilink")），帝遂于家庭无乐趣矣，乃出而纵淫，……专觅内城之私卖淫者取乐焉。……久之毒发，始犹不觉，继而见于面，盎于背。”“太医知为淫毒，而不敢言，遂以治痘药治之，不效”。1923年[蕭一山的](../Page/蕭一山.md "wikilink")《[清代通史](../Page/清代通史.md "wikilink")》再三強調了同治帝是死於梅毒。台灣作家[高陽长篇巨著](../Page/高陽_\(作家\).md "wikilink")《[慈禧全传](../Page/慈禧全传.md "wikilink")》认定是梅毒\[2\]。御醫[李德立的曾孫](../Page/李德立_\(医生\).md "wikilink")[李鎮和](../Page/李鎮.md "wikilink")[李志綏分別撰文稱](../Page/李志綏.md "wikilink")，祖上口傳秘聞，同治帝死于梅毒。慈禧聽到李德立的診斷結果之後，強迫他宣布是天花。李鎮表示“同治梅毒溃烂后，流脓不止，奇臭难闻，曾祖父（李德立）每日必须亲自为他清洗敷药，一个多月来受到强烈恶臭刺激，从此失去了嗅觉”\[3\]。也有人認為是先患天花未癒而又染上梅毒，或先患梅毒而又染上天花，二疾並發而亡\[4\]。

## 評價

## 大事年表

  - 清咸豐六年三月二十三日，載淳在北京紫禁城儲秀宮出生。
  - 清咸豐十一年七月，咸豐帝去世，年僅六歲的載淳登基，依照咸豐帝遺詔，由肅順等八位大臣輔政。九月兩宮太后與恭親王[奕訢發動](../Page/奕訢.md "wikilink")「[辛酉政變](../Page/辛酉政變.md "wikilink")」，八大臣等被奕訢與慈禧奪權。
  - 清同治三年六月，清軍攻陷[太平天國首都](../Page/太平天國.md "wikilink")[天京](../Page/天京.md "wikilink")。
  - 清同治四年四月，科爾沁親王[僧格林沁為](../Page/僧格林沁.md "wikilink")[捻軍所殺](../Page/捻軍.md "wikilink")。
  - 清同治六年十二月，東捻軍被平定。
  - 清同治七年七月，西捻軍主力被平定。
  - 清同治九年七月，[兩江總督](../Page/兩江總督.md "wikilink")[馬新貽被刺殺](../Page/馬新貽.md "wikilink")。
  - 清同治十一年九月，迎娶皇后阿魯特氏（[孝哲毅皇后](../Page/孝哲毅皇后.md "wikilink")）。
  - 清同治十二年正月，親政，同年[同治陝甘回亂及雲南回亂大致平定](../Page/同治陝甘回亂.md "wikilink")。
  - 清同治十三年六月，臺灣[牡丹社事件爆發](../Page/牡丹社事件.md "wikilink")，與日本在北京訂立[北京專約](../Page/北京專約.md "wikilink")。
  - 清同治十三年九月，發生震驚中外的[楊乃武與小白菜案](../Page/楊乃武與小白菜.md "wikilink")。
  - 清同治十三年十二月，同治帝崩，得年19歲。

## 家族

<center>

</center>

### \-{后}-妃

  - [孝哲毅皇后](../Page/孝哲毅皇后.md "wikilink")，阿魯特氏，[崇绮女](../Page/崇绮.md "wikilink")，[赛尚阿孙女](../Page/赛尚阿.md "wikilink")，東太后表外甥女，愛新覺羅端華與鈕祜祿氏
    (東太后之姑) 之外孫女。同治十一年九月十四册为皇后。同治帝死后封为嘉顺皇后，不久崩。
  - [淑慎皇贵妃](../Page/淑慎皇贵妃.md "wikilink")，富察氏，疑是慈禧太后母族。穆宗立皇-{后}-，同日封慧妃。进皇贵妃。德宗即位，以两太后命，封为敦宜皇贵妃。进敦宜荣庆皇贵妃。光绪三十年，薨。谥曰淑慎皇贵妃。
  - [莊和皇貴妃](../Page/莊和皇貴妃.md "wikilink")，阿魯特氏，大学士赛尚阿女，孝哲毅皇后姑也。事穆宗，为珣嫔，进妃。光绪间，进贵妃。宣统皇帝尊为皇考珣皇贵妃。孝定景皇后崩未逾月，妃薨。谥曰莊和皇贵妃。
  - [敬懿皇贵妃](../Page/敬懿皇贵妃.md "wikilink")，赫舍里氏。事穆宗，自嫔进妃。光绪间，进贵妃。宣统间，累进尊封。
  - [榮惠皇貴妃](../Page/榮惠皇貴妃.md "wikilink")，西林覺羅氏。事穆宗，自贵人进嫔。光绪间，进妃。宣统间，累进尊封。

### 嗣子

  - [溥仪](../Page/溥仪.md "wikilink")，同治帝的堂侄，[过继于同治帝](../Page/过继.md "wikilink")，同时兼承[光绪帝之](../Page/光绪帝.md "wikilink")[祧](../Page/祧.md "wikilink")，一人祧两房\[5\]。

## 影视形象

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 注釋

## 参考文献

## 外部链接

  -
{{-}}       |-  |-

[Category:清朝皇帝](../Category/清朝皇帝.md "wikilink")
[清穆宗](../Category/清穆宗.md "wikilink")
[1](../Category/清文宗皇子.md "wikilink")
[Category:墓穴遭搗毀者](../Category/墓穴遭搗毀者.md "wikilink")
[Category:死於天花的人](../Category/死於天花的人.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:愛新覺羅氏](../Category/愛新覺羅氏.md "wikilink")

1.  引自：[閻崇年](../Page/閻崇年.md "wikilink")，《正說清朝十二帝》
2.  《[慈禧全传](../Page/慈禧全传.md "wikilink")》第二部《玉座珠廉》（下）的《恶疾初起》一节中
3.
4.  [閻崇年](../Page/閻崇年.md "wikilink")，《[正說清朝十二帝](../Page/正說清朝十二帝.md "wikilink")》
5.  《[清史稿](../Page/清史稿.md "wikilink")·本纪二十五·宣统皇帝本纪》：“三十四年冬十月壬申，德宗疾大渐，太皇太后命教养宫内。癸酉，德宗崩，奉太皇太后懿旨，入承大统，为嗣皇帝，嗣穆宗，兼承大行皇帝之祧，时年三岁。”