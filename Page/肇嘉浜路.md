[Zhaojiabang_Rd.jpg](https://zh.wikipedia.org/wiki/File:Zhaojiabang_Rd.jpg "fig:Zhaojiabang_Rd.jpg")

**肇嘉浜路**，[上海市](../Page/上海市.md "wikilink")[徐汇区主要交通干道之一](../Page/徐汇区.md "wikilink")，东起[瑞金二路接](../Page/瑞金二路.md "wikilink")[徐家汇路](../Page/徐家汇路.md "wikilink")，西至[漕溪北路](../Page/漕溪北路.md "wikilink")、[衡山路](../Page/衡山路_\(上海\).md "wikilink")、[华山路口接](../Page/华山路_\(上海\).md "wikilink")[虹桥路](../Page/虹桥路.md "wikilink")，路宽63米，中有宽21米的绿化带，并有步行小径，是上海著名的景观道路，南侧道路长2946米，北侧道路长2986米。

肇嘉浜 (肇嘉滨)
原是上海地区一条东西走向的通航河流，原河道从[黄浦江上溯进入大东门](../Page/黄浦江.md "wikilink")，穿越[上海县城](../Page/上海县城.md "wikilink")，出老西门，斜向西南至斜桥，向西经过[卢家湾](../Page/卢家湾.md "wikilink")、[打浦桥](../Page/打浦桥街道#打浦桥.md "wikilink")、徐家汇，可以向西通往松江府城。20世纪初，上海县城以东部分填筑成白渡路，城内部分填筑成肇嘉路（今复兴东路一部分）。以后，肇嘉浜已经缩减到打浦桥[日晖港以西到](../Page/日晖港.md "wikilink")[徐家汇的河道](../Page/徐家汇.md "wikilink")，作为[上海法租界和华界的分界线](../Page/上海法租界.md "wikilink")，路北有法租界修筑的徐家汇路，路南是中国政府修筑的斜徐路，均从斜桥直抵徐家汇。浜边形成棚户区，环境极差。

1954年，[上海市人民政府开始治理](../Page/上海市人民政府.md "wikilink")[肇嘉浜](../Page/肇嘉浜.md "wikilink")。经过3年治理，肇嘉浜全线填平，两侧的徐家汇路和斜徐路合并，成为上海市南部地区东西向交通大动脉，原河边居民1704户迁入[漕溪新村](../Page/漕溪新村.md "wikilink")，耗资754万元人民币。

肇嘉浜路修筑以后，造成日晖港以东到斜桥之间残存的徐家汇路和斜徐路名不副实的情形：既不靠近[徐家汇](../Page/徐家汇.md "wikilink")，也不直接通达徐家汇。

## 机构

  - [上海市高级人民法院](../Page/上海市高级人民法院.md "wikilink") 肇嘉浜路308号（襄阳南路口东北角）

## 交汇道路

  - [瑞金二路](../Page/瑞金二路.md "wikilink")
  - [陕西南路](../Page/陕西南路_\(上海\).md "wikilink")
  - [大木桥路](../Page/大木桥路.md "wikilink")
  - [襄阳南路](../Page/襄阳南路_\(上海\).md "wikilink")
  - [太原路](../Page/太原路_\(上海\).md "wikilink")
  - [小木桥路](../Page/小木桥路.md "wikilink")
  - [岳阳路](../Page/岳阳路_\(上海\).md "wikilink")
  - [枫林路](../Page/枫林路.md "wikilink")
  - [乌鲁木齐南路](../Page/乌鲁木齐南路.md "wikilink")
  - [东安路](../Page/东安路_\(上海\).md "wikilink")
  - [高安路](../Page/高安路_\(上海\).md "wikilink")
  - [吴兴路](../Page/吴兴路.md "wikilink")
  - [宛平路](../Page/宛平路.md "wikilink")-[宛平南路](../Page/宛平南路.md "wikilink")
  - [天平路](../Page/天平路.md "wikilink")
  - [天鈅桥路](../Page/天鈅桥路.md "wikilink")
  - [漕溪北路](../Page/漕溪北路.md "wikilink")-[衡山路](../Page/衡山路_\(上海\).md "wikilink")-[华山路](../Page/华山路_\(上海\).md "wikilink")

{{-}}

[Z](../Category/上海道路.md "wikilink")