**十津川村**（）是[日本](../Page/日本.md "wikilink")[奈良縣最南端的一個](../Page/奈良縣.md "wikilink")[村級行政區](../Page/村.md "wikilink")。除了日本未實際統治的[北方四島](../Page/北方四島.md "wikilink")（南千島群島）設有包括[留別村在內](../Page/留別村.md "wikilink")、僅有名義上的建制但無實際管轄的村級行政區之外，十津川村是日本實際上面積最大的村，面積672.35平方公里。另外也是奈良縣内[實質公債費比率最低的市町村](../Page/實質公債費比率.md "wikilink")。

## 相關條目

  - [天誅組](../Page/天誅組.md "wikilink")
  - [近江屋事件](../Page/近江屋事件.md "wikilink")

[Category:日本地理之最](../Category/日本地理之最.md "wikilink")