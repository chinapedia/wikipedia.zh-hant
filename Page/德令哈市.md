**德令哈市**（[蒙文](../Page/蒙文.md "wikilink")：，），蒙古语全称“阿里腾德令哈”（），意为“金色的世界”\[1\]。德令哈市位于中国[青海省北部](../Page/青海省.md "wikilink")，是[海西蒙古族藏族自治州州府所在地](../Page/海西蒙古族藏族自治州.md "wikilink")，是全州政治、教育、科技、文化中心，也是海西东部经济区中心。辖三镇一乡三个街道办事处，面积2.76万平方公里，市区海拔2980米，2006年人口为7万，共有蒙古、藏、回、撒拉、土、汉等19个民族，蒙古族为主体少数民族。

以工业为主，由名为“巴音河”的河流分为河东、河西两个街道，河东为州委所在地较为繁华，河西以农业为主。

## 历史

13世纪，[蒙古族崛起](../Page/蒙古族.md "wikilink")，灭金亡宋，建立了[元朝](../Page/元朝.md "wikilink")，德令哈亦被置于元朝统治之下，为[宣政院所属](../Page/宣政院.md "wikilink")[吐蕃等处宣慰使司都元帅府辖区](../Page/吐蕃等处宣慰使司都元帅府.md "wikilink")。之后，明正德五年（1512年）和明崇祯九年（1636年），蒙古族两度进入柴达木地区。现居住在德令哈地区的蒙古族为厄鲁特蒙古禾硕特部（西蒙古）首领顾始汗之后裔。

1955年设德令哈工作委员会，1958年撤销工委设德令哈县（驻巴音河），1962年撤县并入乌兰县。1988年4月19日从乌兰县析置德令哈市（县级），辖德令哈镇和乌兰县的怀他头拉、戈壁、郭里木、宗务隆、畜集五个乡。2005年撤销戈壁乡，设立柯鲁柯镇；撤销宗务隆乡。

## 地理

德令哈市位于柴达木盆地东北边缘，地域辽阔，地形复杂，山、川、盆、湖、泊、绿洲、戈壁、荒漠兼有，资源富集，土地肥沃，光热充足。这里是青海省生态绿洲农牧业的重点地区，是柴达木优质绒山羊的繁育基地。境内发现的矿产资源有16个品种、65个矿床，其中已探明的石灰石储量在7亿吨以上，可利用价值较高。境内还有丰富的野牦牛、野驴、雪豹、麝、岩羊、黄羊、雪鸡和沙棘、枸杞、锁阳等驰名中外的野生动植物资源。河湖面积2067平方公里，市区以南30公里的尕海湖，蕴藏有丰富的天然卤虫资源。

距市区以西40公里的可鲁克湖，盛产草鱼、鲤鱼、鲫鱼、闸蟹、虾等水产。

### 气候

## 行政区划

下辖3个[街道办事处](../Page/街道办事处.md "wikilink")、3个[镇](../Page/镇.md "wikilink")、1个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

[河西街道](../Page/河西街道_\(德令哈市\).md "wikilink")、[河东街道](../Page/河东街道_\(德令哈市\).md "wikilink")、[火车站街道](../Page/火车站街道_\(德令哈市\).md "wikilink")、[尕海镇](../Page/尕海镇.md "wikilink")、[怀头他拉镇](../Page/怀头他拉镇.md "wikilink")、[柯鲁柯镇](../Page/柯鲁柯镇.md "wikilink")、[蓄集乡](../Page/蓄集乡.md "wikilink")。

## 旅游

德令哈有多处奇特的旅游景点，古柏蓊翳的柏树山，碧波粼粼的黑石山水库，神秘的“外星人遗址”，古老的怀头他拉岩画，一咸一淡、生态各异、遥相连接、神韵悠长的“褡裢湖”──
托素湖和可鲁克湖特色旅游区吸引着来自四面八方的游客。

## 军事

中国在德令哈部署了10到20枚[东风-21中程弹道导弹](../Page/东风-21中程弹道导弹.md "wikilink")。

## 参考资料

[德令哈市](../Category/德令哈市.md "wikilink")
[Category:海西州县市](../Category/海西州县市.md "wikilink")
[Category:青海省县级市](../Category/青海省县级市.md "wikilink")

1.  德令哈概况 <http://www.qh.xinhuanet.com/dlh/g.htm>