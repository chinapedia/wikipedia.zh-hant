**李燕**（英文：****，
），本名**李秋燕**，暱稱**李小燕**，為台劇當家花旦，[臺灣](../Page/臺灣.md "wikilink")[女演員](../Page/女演員.md "wikilink")，[瑞芳高工畢業](../Page/國立瑞芳高級工業職業學校.md "wikilink")。原為電子公司採購，後進入[民視演員訓練班而踏入演藝圈](../Page/民視.md "wikilink")，和[高宇蓁為同期學員](../Page/高宇蓁.md "wikilink")。

## 個人經歷

2013年9月30日，與[彰化縣議會議長](../Page/彰化縣議會.md "wikilink")[謝典霖登記結婚](../Page/謝典霖.md "wikilink")\[1\]，於2015年3月協議離婚\[2\]。

2018年1月9日，在三立八點檔《[一家人](../Page/一家人.md "wikilink")》殺青酒會時透漏有心儀對象，疑似為同劇男演員[趙駿亞](../Page/趙駿亞.md "wikilink")，趙駿亞經紀人表示兩人目前確實交往中\[3\]。

2018年9月9日，推出籌備近兩年之自創保養品牌《KOCSKIN克麗詩黛》，同時為該品牌產品代言人\[4\]，9月22日舉辦新品發表粉絲見面會<ref>[李燕自創保養品　議長前夫豪氣買百瓶對尬趙駿亞](https://tw.appledaily.com/new/realtime/20180922/1434615)，《蘋果日報

`》，2018/09/22。`</ref>`。`

## 作品

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>年 份</strong></p></td>
<td><p><strong>頻 道</strong></p></td>
<td><p><strong>劇 名</strong></p></td>
<td><p><strong>飾 演</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/GOOD_TV.md" title="wikilink">GOOD TV</a></p></td>
<td><p>《<a href="../Page/鑰匙兒.md" title="wikilink">鑰匙兒</a>》</p></td>
<td><p>明柔</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/後山日先照.md" title="wikilink">後山日先照</a>》</p></td>
<td><p>陳耕蘭</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/今天不上班.md" title="wikilink">今天不上班</a>》</p></td>
<td><p>葉佳蓮</p></td>
<td><p>女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八大電視台.md" title="wikilink">八大電視台</a></p></td>
<td><p>《<a href="../Page/第一劇場.md" title="wikilink">第一劇場</a>－<a href="../Page/相思夢.md" title="wikilink">相思夢</a>》</p></td>
<td><p>許明秀</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/戲說台灣.md" title="wikilink">戲說台灣</a>－<a href="../Page/豆油間的地基主.md" title="wikilink">豆油間的地基主</a>》</p></td>
<td><p>素玉<br />
秀玲</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/家_(公視電視劇).md" title="wikilink">家</a>》</p></td>
<td><p>林月女</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/八大電視台.md" title="wikilink">八大電視台</a></p></td>
<td><p>《<a href="../Page/第一劇場.md" title="wikilink">第一劇場</a>－<a href="../Page/千里情緣.md" title="wikilink">千里情緣</a>》</p></td>
<td><p>素鳳</p></td>
<td><p>女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台視.md" title="wikilink">台視</a></p></td>
<td><p>《<a href="../Page/再見阿郎_(電視劇).md" title="wikilink">再見阿郎</a>》</p></td>
<td><p>年轻美枝</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/戲說台灣.md" title="wikilink">戲說台灣</a>－<a href="../Page/祖師爺收煞.md" title="wikilink">祖師爺收煞</a>》</p></td>
<td><p>秋華</p></td>
<td><p>女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/天地有情.md" title="wikilink">天地有情</a>》</p></td>
<td><p>陳喬安</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/台灣龍捲風.md" title="wikilink">台灣龍捲風</a>》</p></td>
<td><p>陸小芬</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/金色摩天輪.md" title="wikilink">金色摩天輪</a>》</p></td>
<td><p>沈君君<br />
沈亮君</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/公視.md" title="wikilink">公視</a></p></td>
<td><p>《<a href="../Page/出外人生.md" title="wikilink">出外人生</a>》</p></td>
<td><p>蔡秋滿</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八大電視台.md" title="wikilink">八大電視台</a></p></td>
<td><p>《<a href="../Page/燃燒天堂.md" title="wikilink">燃燒天堂</a>》</p></td>
<td><p>李明雁</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/台視.md" title="wikilink">台視</a></p></td>
<td><p>《<a href="../Page/神機妙算劉伯溫.md" title="wikilink">神機妙算劉伯溫</a>》</p></td>
<td><p>雲丹檀風</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三立台灣台.md" title="wikilink">三立台灣台</a></p></td>
<td><p>《<a href="../Page/天下第一味.md" title="wikilink">天下第一味</a>》</p></td>
<td><p>堂本紀香</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/真情滿天下.md" title="wikilink">真情滿天下</a>》</p></td>
<td><p>關葦葦</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/天下父母心.md" title="wikilink">天下父母心</a>》</p></td>
<td><p>蔡有慧</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/戲說台灣.md" title="wikilink">戲說台灣</a>－<a href="../Page/布袋和尚_(2011年電視劇).md" title="wikilink">布袋和尚</a>》</p></td>
<td><p>林秀<br />
廖玉君</p></td>
<td><p>女主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/家和萬事興_(2010年電視劇).md" title="wikilink">家和萬事興</a>》</p></td>
<td><p>方小菲</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/牽手_(2011年電視劇).md" title="wikilink">牽手</a>》</p></td>
<td><p>江詩詩<br />
陸小凡</p></td>
<td><p>第一代第一女主角 （江詩詩）（與<a href="../Page/陳珮騏.md" title="wikilink">陳珮騏</a>、<a href="../Page/連靜雯.md" title="wikilink">連靜雯</a> ）<br />
第二代第一女主角 （陸小凡） |-2013年 白蛇外傳 第一女主角</p></td>
<td><p>2013年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白蛇外傳.md" title="wikilink">《白蛇外傳》</a></p></td>
<td><p>白素貞</p></td>
<td><p>第一女主角</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>《<a href="../Page/逆光真愛.md" title="wikilink">逆光真愛</a>》</p></td>
<td><p>蔡雅真</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p>《<a href="../Page/甘味人生.md" title="wikilink">甘味人生</a>》</p></td>
<td><p>陳怡萱<br />
江姜好</p></td>
<td><p>第一代第二女主角<br />
第二代第一女主角 （江姜好）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p>《<a href="../Page/一家人.md" title="wikilink">一家人</a>》</p></td>
<td><p>葉曉秋</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p>《<a href="../Page/金家好媳婦.md" title="wikilink">金家好媳婦</a>》</p></td>
<td><p>史天娜</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/炮仔聲_(台灣電視劇).md" title="wikilink">炮仔聲</a>》</p></td>
<td><p>吴家芸</p></td>
<td><p>第女一主角</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 代言廣告

  - 《MILDSKIN麥德絲肌》醫學美容保養品
  - 《KOCSKIN克麗詩黛》保養品牌
  - 《拉斯維加斯娛樂城》線上遊戲\[5\]

## 外部連結

  -
  -
  -
  -
## 參考資料

[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:新北市立瑞芳高級工業職業學校校友](../Category/新北市立瑞芳高級工業職業學校校友.md "wikilink")
[Category:新北市人](../Category/新北市人.md "wikilink")
[Yan燕](../Category/李姓.md "wikilink")

1.
2.
3.  {{ Cite news|title =《一家人》愛不到，李燕、趙駿亞被拍到十指緊扣逛街，經紀人證實兩人在一起。|publisher =
    中時電子報 | date = 2018-4-17 | url =
    <http://www.chinatimes.com/realtimenews/20180425004577-260404>
    |author=施品瑜}}

4.  [李燕懷胎兩年生出小孩！進《金家》成了男友趙駿亞「妹妹」](http://www.chinatimes.com/realtimenews/20180824004244-260404)，《中時電子報》，2018/8/24
    。

5.  [李燕代言遊戲扮演火辣賭后　玩賓果、刮刮樂拚5千億獎金！](https://game.ettoday.net/article/1322311.htm)，《[ETtoday新聞雲](../Page/ETtoday新聞雲.md "wikilink")》，2018/12/21。