**格林森林流浪足球俱乐部**（）是[英格兰的足球俱乐部](../Page/英格兰.md "wikilink")，位于[格洛斯特郡](../Page/格洛斯特郡.md "wikilink")[奈尔斯沃思](../Page/奈尔斯沃思.md "wikilink")，现属于[英格蘭足球聯賽系統第五級的](../Page/英格蘭足球聯賽系統.md "wikilink")[英格蘭足球議會全國聯賽](../Page/英格蘭足球議會全國聯賽.md "wikilink")。

## 球队历史

  - **1968年－69年**：球队建立，參加[格洛斯特郡联赛](../Page/格洛斯特郡联赛.md "wikilink")（）
  - **1975年－76年**：加盟[希腊足球联赛超级联赛](../Page/英格兰希腊足球联赛.md "wikilink")（）
  - **1981年－82年**：**希腊足球联赛冠军**。**[英格兰足总瓶冠军](../Page/英格兰足总瓶.md "wikilink")**；晋级[南部联赛米德兰区](../Page/英格蘭足球南部聯賽.md "wikilink")
  - **1989年**：球队更名为斯特劳足球俱乐部（）
  - **1992年**：队名更回格林森林流浪
  - **1995年－96年**：转为联赛南部组球队
  - **1996年－97年**：**南部联赛南部组冠军**；晋级南部联赛超级组
  - **1997年－98年**：**南部联赛冠军**；晋级全国联赛
  - **1998年－99年**：[英格兰足总锦标亚军](../Page/英格兰足总锦标.md "wikilink")
  - **2000年－01年**：英格兰足总锦标亚军

所知道的是[巴塞罗那的队徽设计灵感来源于格林森林流浪](../Page/巴塞罗那足球俱乐部.md "wikilink")。

## 俱乐部纪录

  - 最佳联赛排位：全国联赛（第5级）第8名，2007/08年；
  - 最佳[英格兰足总杯表现](../Page/英格兰足总杯.md "wikilink")：第3轮，2008/09年、2009/10年；
  - 最佳[英格兰足总锦标表现](../Page/英格兰足总锦标.md "wikilink")：亚军，1998/99年、2000/01年；
  - 最佳[英格兰足总瓶表现](../Page/英格兰足总瓶.md "wikilink")：冠军，1981/82年；
  - 最佳[英格蘭聯賽錦標表现](../Page/英格蘭聯賽錦標.md "wikilink")：第1轮，2003/04年；
  - 最佳[英格蘭足球議會聯賽盃表現](../Page/英格蘭足球議會聯賽盃.md "wikilink")：亞軍，2008/09年；

## 荣誉

  - **[英格兰足总瓶](../Page/英格兰足总瓶.md "wikilink")** 优胜者 1982年
  - **[英格蘭足球南部聯賽](../Page/英格蘭足球南部聯賽.md "wikilink") 超级组** 冠军 1998年
  - **[英格蘭足球南部聯賽](../Page/英格蘭足球南部聯賽.md "wikilink") 南部组** 冠军 1997年
  - **[希腊足球联赛](../Page/英格兰希腊足球联赛.md "wikilink")** 冠军 1982年

## 資料來源

  - [足球歷史資料庫](http://www.fchd.info/FORESTGR.HTM)

## 外部链接

  - [Official Site](http://www.forestgreenroversfc.com/)
  - [Forest Green Rovers Mad](http://www.forestgreenrovers-mad.co.uk)
  - [Forest Green Rovers on
    Myspace](http://www.myspace.com/forestgreenrovers)

[F](../Category/英格兰足球俱乐部.md "wikilink")
[Category:1890年建立的足球俱樂部](../Category/1890年建立的足球俱樂部.md "wikilink")