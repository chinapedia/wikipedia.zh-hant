**第五權**是一個由[西班牙](../Page/西班牙.md "wikilink")[記者](../Page/記者.md "wikilink")[伊格納西奧·拉莫內特所創的詞語](../Page/伊格納西奧·拉莫內特.md "wikilink")，作為[孟德斯鳩](../Page/孟德斯鳩.md "wikilink")[三權分立及第四權大眾傳播媒介的延續](../Page/三權分立.md "wikilink")。第五權一般可指[經濟體系或](../Page/經濟體系.md "wikilink")[互聯網](../Page/互聯網.md "wikilink")。

## 互聯網

第五權的第二個意思指由互聯網或網民所組成、對社會甚至政府的網絡監察，其代表了不被「較狹窄、單向領域（one-way
scope）的第四權傳播媒體」所包含的一種新社會大眾媒體。它被考慮為權力的第五個分支，而且更是唯一被社會自己所操控、並不被國家予以一定調節的權力分支。

根據Ramonet，網民共同組成了辯論與民主行動一個強而有力的發動機。隨著全球化，21世紀擁有最終把溝通和資訊帶給全人類的潛質\[1\]。[時代雜誌](../Page/時代雜誌.md "wikilink")2006年度[風雲人物](../Page/時代年度風雲人物.md "wikilink")「[你](../Page/你_\(时代年度风云人物\).md "wikilink")」正帶著相同訊息\[2\]。

2008年部落客世界\[3\]籌委會與政策創議中心於2008年5月1日聯辦《邁向公民社會》（Towards A Civil
Society）研討會，討論[部落格在推動公民社會發展和營造論述的角色](../Page/部落格.md "wikilink")，會中[部落格便被喻為第五權](../Page/部落格.md "wikilink")\[4\]\[5\]。

互聯網思想的一個重要問題是，大部分當中的資訊是完全無價值的。因此過濾資訊的傳統媒體依然扮演著一個重要角色。

### 維基解密和茉莉花革命

2006年成立的《[維基解密](../Page/維基解密.md "wikilink")》接受理匿名者送交的密件，由志願團隊查核可信度後公布，幾年之間震撼全球。2010年，它公開了美國國務院和駐外使館聯繫的電報內容，其中部分電文直指[突尼西亞總統班阿里家族與黑道掛勾](../Page/突尼西亞.md "wikilink")、第一夫人貪腐，引爆突國民眾怒火。其後失業青年自焚的事件，終於觸發民眾的怒火臨界點，突尼西亞人民以[臉書](../Page/臉書.md "wikilink")、[Twitter等網絡工具發動抗爭](../Page/Twitter.md "wikilink")，掀起席捲中東和北非的[茉莉花革命](../Page/茉莉花革命.md "wikilink")，導致了突尼西亞、[埃及和](../Page/埃及.md "wikilink")[利比亞等國家政權的倒台](../Page/利比亞.md "wikilink")。

### 中國大陸的人肉搜索

自2000年代開始，互聯網在中國大陸扮演著越來越重要的角色。很多在現實社會發生的不公平事件，如官僚枉法、富二代欺壓普通民眾，也被網民放在網上[人肉搜索](../Page/人肉搜索.md "wikilink")，發生[網絡爆紅現象](../Page/網絡爆紅現象.md "wikilink")，如[我爸是李剛案](../Page/河北大學「10·16」交通肇事案.md "wikilink")、[杭州飆車案](../Page/杭州飆車案.md "wikilink")、[林嘉祥事件](../Page/林嘉祥事件.md "wikilink")、[李天一事件和](../Page/李冠丰.md "wikilink")[郭美美事件](../Page/郭美美事件.md "wikilink")["史上最牛记者"颜秉光事件等](../Page/"史上最牛记者"颜秉光事件.md "wikilink")，都曾在網上瘋傳一時，並影響了主人公在現實中的生活。

## 參考條目

  - [Political media](../Page/Political_media.md "wikilink")
  - [Web 2.0](../Page/Web_2.0.md "wikilink")
  - [網民](../Page/網民.md "wikilink")
  - [網絡監察](../Page/網絡監察.md "wikilink")
  - [人肉搜索](../Page/人肉搜索.md "wikilink")
  - [維護公民社會價值，反對宗教右翼霸權大遊行](../Page/維護公民社會價值，反對宗教右翼霸權大遊行.md "wikilink")
  - [防火長城](../Page/防火長城.md "wikilink")

## 相關條目

  - [第四權](../Page/第四權.md "wikilink")

## 參考文獻

<references/>

## 外部連結

  -
  - [Ignacio
    RAMONET（2003）　第五權　法國世界外交論衡月刊中文版](http://cn.mondediplo.com/article59.html)

[DWQ](../Category/政治權力.md "wikilink")
[Category:互聯網](../Category/互聯網.md "wikilink")

1.  See Ramonet’s essay (in Spanish) at  and an interview with him (in
    English) at

2.

3.  [Bloggers' Universe
    Malaysia，BUM2008](http://bum2008-desiderata.blogspot.com/)

4.
5.  部落客推動社會發展 五一研討會商第五權　2008/04/10 獨立新聞在線