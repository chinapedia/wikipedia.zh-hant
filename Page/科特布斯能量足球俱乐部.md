[Stadion_der_Freundschaft.jpg](https://zh.wikipedia.org/wiki/File:Stadion_der_Freundschaft.jpg "fig:Stadion_der_Freundschaft.jpg")
**科特布斯能量足球俱乐部**（****）是一家位於[德國東部](../Page/德國.md "wikilink")[布蘭登堡](../Page/布蘭登堡.md "wikilink")[盧薩蒂亞地區的](../Page/盧薩蒂亞.md "wikilink")[科特布斯市的足球會](../Page/科特布斯.md "wikilink")，現時在[德國丙組聯賽中作賽](../Page/德丙.md "wikilink")。

## 歷史

### 創會時期

科特布斯前身為成立於1963年科特布斯能量體育會（），當時正值[東德時代](../Page/東德.md "wikilink")，基於政治原因長期性影響體育及足球俱樂部的發展，球會成立初時獲得一支鄰近東德球會「」批准委派大批球員加入球隊。

### 東德時代

科特布斯在[二次大戰後的](../Page/第二次世界大戰.md "wikilink")1949年以「*Franz Mehring
Marga*」的名義再次出現，1950年更名為「*BSG Aktivist Brieske-Ost*」，1954年再更名為「*SC
Aktivist
Brieske-Senftenberg*」，在前東德的上級聯賽中角逐，位列中游份子。到六十年代早期才直插降級至第四級聯賽Cottbus
Berzirksliga。1963年整隊球員被送往「*SC Energie Cottbus*」。

六十年代中期由前東德政權安排科特布斯重組，足球部門脫離體育會而沿用舊名稱，由本地煤礦贊助的原體育會則另起爐灶創立「*BSG von Bodo
Krautz*」。1966年球隊再次更名為「*BSG Energie*」。

### 兩德統一

當1990年[兩德統一時球隊更名為](../Page/兩德統一.md "wikilink")**能量足球俱乐部**（**）。

一直處於東德甲組尾乙組頭的科特布斯，成為少數於統一後獲得成功的前東德足球俱乐部。在統一後加入第三級地區聯賽作賽五年後，科特布斯於1997年獲得升級[乙組](../Page/德乙.md "wikilink")，更於2000年升上[甲組](../Page/德甲.md "wikilink")，在甲組逗留三年後降級重回乙組。降級後的首季科特布斯僅以得失球差被[-{zh-hans:美因茨05;zh-hk:緬恩斯;zh-tw:美因茲05;}-壓過而失去即時回升德甲的機會](../Page/美因茨05足球俱乐部.md "wikilink")。最終於2005-06年球季獲得再次升級德甲，成為此時期前東德球隊在頂級聯賽的唯一代表。

## 主場球場

**友谊体育场**（）是位於[德國東部](../Page/德國.md "wikilink")[科特布斯的](../Page/科特布斯.md "wikilink")[運動場](../Page/運動場.md "wikilink")，建於1930年，可容22,500名觀眾，主要用於[足球比賽](../Page/英式足球.md "wikilink")，自1970年代末期開始成為科特布斯足球俱樂部的主場球場。

1997年設置汎光燈照明；2003/04年季初東看台落成啟用；2007年夏季拆卸北看台重建，趕及於2007/08年球季完工，可容8,000觀眾，而東看台則改為全坐席可容9,102人，使球場總容量達到22,746人。重建完成後於2008年3月5日進行首場比賽對[拜仁慕尼黑](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。2007/08年球季結束後重建南看台，於2008/09年季初完工，可容5,559名觀眾，使球場容量減少218席，總容量現時為22,528人。

## 球隊冷知識

  - 2001年4月，科特布斯成為[德國甲組聯賽首支委派](../Page/德甲.md "wikilink")11位正選全部屬於外援球員上陣的球會。
  - 中国足球运动员[邵佳一曾于](../Page/邵佳一.md "wikilink")2006年至2011年在此效力。

## 參考資料

## 外部連結

  - [ 官方網站](http://www.fcenergie.de)
  - [ The Abseits Guide to German
    Soccer](http://www.abseits-soccer.com/clubs/cottbus.html)

[C](../Category/德國足球俱樂部.md "wikilink")
[Category:1966年建立的足球俱樂部](../Category/1966年建立的足球俱樂部.md "wikilink")