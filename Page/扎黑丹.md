[Municipality_Building.jpg](https://zh.wikipedia.org/wiki/File:Municipality_Building.jpg "fig:Municipality_Building.jpg")

**扎黑丹**（[波斯语](../Page/波斯语.md "wikilink")：****；[俾路支语](../Page/俾路支语.md "wikilink")：****）位于[伊朗东部邻近](../Page/伊朗.md "wikilink")[巴基斯坦边境处](../Page/巴基斯坦.md "wikilink")，是[锡斯坦-俾路支斯坦省的省会](../Page/锡斯坦-俾路支斯坦省.md "wikilink")，2016年人口约为58.7万。该市名称”Zahedan“是[阿拉伯语词汇](../Page/阿拉伯语.md "wikilink")”Zahed“的复数，意思是“虔诚的，神圣的”。

## 地理

扎黑丹位于[巴基斯坦和](../Page/巴基斯坦.md "wikilink")[阿富汗附近](../Page/阿富汗.md "wikilink")，在三国交界点以南约41公里，海拔高度为1,352米，距离伊朗首都[德黑兰](../Page/德黑兰.md "wikilink")1,605千米。扎黑丹位于[俾路支地区](../Page/俾路支斯坦.md "wikilink")。

根据[柯本气候分类法](../Page/柯本气候分类法.md "wikilink")，扎黑丹气候类型为[热带沙漠气候](../Page/热带沙漠气候.md "wikilink")，夏季炎热，冬季凉爽。扎黑丹[降水量很小](../Page/降水量.md "wikilink")，而且降水一般集中在冬季。

## 教育

[University_of_Sistan_and_Baluchestan.JPG](https://zh.wikipedia.org/wiki/File:University_of_Sistan_and_Baluchestan.JPG "fig:University_of_Sistan_and_Baluchestan.JPG")

扎黑丹拥有扎黑丹伊斯兰阿萨德大学\[1\]、扎黑丹医科大学\[2\]和等高校。扎黑丹还拥有多所[逊尼派神学院](../Page/逊尼派.md "wikilink")。

## 参考来源

[Z](../Category/伊朗城市.md "wikilink")

1.  [<http://www.iauzah.ac.ir>](http://www.iauzah.ac.ir/)
2.  [<http://www.zaums.ac.ir>](http://www.zaums.ac.ir/)