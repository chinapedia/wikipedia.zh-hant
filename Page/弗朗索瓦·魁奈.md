[François_Quesnay.jpg](https://zh.wikipedia.org/wiki/File:François_Quesnay.jpg "fig:François_Quesnay.jpg")
[Quesnay_-_Tableau_economique,_1965_-_5891137.tif](https://zh.wikipedia.org/wiki/File:Quesnay_-_Tableau_economique,_1965_-_5891137.tif "fig:Quesnay_-_Tableau_economique,_1965_-_5891137.tif")
**弗朗索瓦·魁奈**（François
Quesnay，），[法國](../Page/法國.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")，[重农主义的領袖](../Page/重农主义.md "wikilink")、[政治經濟學體系的先驅](../Page/政治經濟學.md "wikilink")。被称为“欧洲的[孔夫子](../Page/孔夫子.md "wikilink")”\[1\]。

魁奈早年以行医为生，后在任[路易十五宮廷御醫時开始研究](../Page/路易十五.md "wikilink")[經濟學](../Page/經濟學.md "wikilink")。

1758年魁奈写出著名的《[經濟表](../Page/經濟表.md "wikilink")》，他用圖表來說明社會各經濟階級和部門的相互關係，以及在它們之間支付的流通，並提出了[經濟平衡的假說](../Page/經濟平衡.md "wikilink")。

他提倡[自由放任經濟政策](../Page/自由放任.md "wikilink")(该思想——甚或名字——很可能来自[中国](../Page/中国.md "wikilink")[无为哲学的启发](../Page/无为_\(道家\).md "wikilink")\[2\]\[3\]）。

魁奈对中国的經濟有所研究，曾著有《中华帝国的专制制度》。

魁奈更提出“纯产品学说”，他认为物质才是财富，唯有[农业才能使财富增加](../Page/农业.md "wikilink")，而[工业只能改变财富的形态](../Page/工业.md "wikilink")，不能增加财富的数量，[服务业更不能增加财富的数量](../Page/服务业.md "wikilink")。並進一步提出“土地单一税理论”，也就是說既然农业创造出纯产品，所以他主张只对土地的收入征缴税收。

後來的[李克洲认为](../Page/李克洲.md "wikilink")，魁奈的经济思想，来源于他所处的时代。为此，[李克洲對魁奈提出](../Page/李克洲.md "wikilink")“纯产品学说”的解釋為：「魁奈时代的法国，农业中已经产生资本主义生产关系，而在工业中，资本主义生产关系还没有发展起来，还是由小业主性质的生产经营占据主导地位。」

可見因為魁奈所處的农业社會環境，當時的確存在“纯产品”（利润）這種現象，但是在工业社會中，這種觀念就不存在了。

因此[马克思在](../Page/马克思.md "wikilink")《[剩余价值理论](../Page/剩余价值.md "wikilink")》中指出：“在重农学派本身得出的结论中，对土地所有权的表面上的推崇，也就变成了对土地所有权的经济上的否定和对[资本主义生产的肯定](../Page/资本主义.md "wikilink")”。

因此，在农业中，存在“纯产品”（利润），而在工业中，就不存在“纯产品”（利润）。但魁奈由于没有所有权决定价值的思想，因此他错误地认为，“纯产品”来自自然的恩惠。

雖然時空背景如此，但是他很[亞當斯密的尊重](../Page/亞當斯密.md "wikilink")。

亞當斯密曾在1764年至1766年間和他的弟子一同遊覽歐洲，大多是在法國，在那裡斯密也認識了許多知識份子的精英。

魁奈身為重農主義學派的領導人，斯密極為尊重他的理論。但是亞當斯密在1776年出版《[國富論](../Page/國富論.md "wikilink")》一書，就否定了重農主義學派對於土地的重視，相反的，斯密認為勞動才是最重要的，而勞動分工將能大量的提升生產效率。

## 參考文獻

[Q](../Category/法国经济学家.md "wikilink")
[Category:法国东方学家](../Category/法国东方学家.md "wikilink")

1.
2.  [1
    Source](http://www.lse.ac.uk/collections/economicHistory/GEHN/GEHNPDF/WorkingPaper12CG.pdf)
3.  "The Eastern Origins of Western Civilization", John M. Hobson, p.196