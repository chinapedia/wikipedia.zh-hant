[Omayyad_mosque.jpg](https://zh.wikipedia.org/wiki/File:Omayyad_mosque.jpg "fig:Omayyad_mosque.jpg")

**倭马亚王朝**（），或譯為**伍麦叶王朝**、**奧米雅王朝**、**奧瑪雅王朝**，在[古中國](../Page/古中國.md "wikilink")《[旧唐书](../Page/旧唐书.md "wikilink")》中被稱為**白衣大食**，是由[倭马亚家族统治的](../Page/倭马亚家族.md "wikilink")[哈里发国](../Page/哈里发国.md "wikilink")，是[阿拉伯帝国的第一个世袭王朝](../Page/阿拉伯帝国.md "wikilink")。在[伊斯兰教最初的四位](../Page/伊斯兰教.md "wikilink")[哈里发](../Page/哈里发.md "wikilink")（即所谓“纯洁的哈里发”或“[正统哈里发](../Page/正统哈里发.md "wikilink")”）的执政结束之后，由阿拉伯帝国的[叙利亚总督](../Page/叙利亚.md "wikilink")[穆阿维叶](../Page/穆阿维叶一世.md "wikilink")（即后来的哈里发**穆阿维叶一世**）建立。公元661年至750年期間，该王朝是[穆斯林世界的主要统治者](../Page/穆斯林世界.md "wikilink")。

## 起源

倭马亚家族是[麦加贵族](../Page/麦加.md "wikilink")[古来氏族中](../Page/古来氏族.md "wikilink")12个支系中最强盛的一支，为4世纪时古来氏族部落首领[库赛伊的长子](../Page/库赛伊.md "wikilink")[阿卜杜勒·马纳夫的后代](../Page/阿卜杜勒·马纳夫.md "wikilink")。至于“倭马亚”一名，则得自于阿卜杜勒·马纳夫的后代[倭马亚·伊本·阿卜杜勒·沙姆斯的名字](../Page/倭马亚·伊本·阿卜杜勒·沙姆斯.md "wikilink")。[先知](../Page/先知.md "wikilink")[穆罕默德传教时期](../Page/穆罕默德.md "wikilink")，倭马亚家族首领[阿布·苏富扬是麦加贵族的代表](../Page/阿布·苏富扬.md "wikilink")，以坚决反对穆罕默德闻名，曾迫使穆罕默德迁居[麦地那](../Page/麦地那.md "wikilink")（[希吉来](../Page/希吉来.md "wikilink")）。但同时，倭马亚家族的另一位重要成员[奥斯曼·伊本·阿凡却是穆罕默德最初的追随者和最亲密的战友之一](../Page/奥斯曼·伊本·阿凡.md "wikilink")。

## 建立

由于穆罕默德生前並未指示其继承人的产生方式，[伊斯兰教世界在哈里发的人选问题上不久即发生分裂](../Page/伊斯兰教世界.md "wikilink")。倭马亚家族的奥斯曼·伊本·阿凡于644年成为[哈里发](../Page/哈里发.md "wikilink")，他大力扶持本家族成员在帝国境内担任要职，引起很多人的不满。倭马亚家族在奥斯曼时代势力大为扩张，尽管许多人记得他们是先知创教初期最凶恶的对手。656年奥斯曼·伊本·阿凡遇刺，奥斯曼的侄子叙利亚总督穆阿维叶反对先知的堂弟和女婿[阿里·伊本·艾比·塔里卜在找到杀害奥斯曼](../Page/阿里·伊本·艾比·塔里卜.md "wikilink")·伊本·阿凡的凶手前继任哈里发，从而引起大规模绥芬之役。

穆阿维叶系穆罕默德传教时代的宿敌[阿布·苏富扬的儿子](../Page/阿布·苏富扬.md "wikilink")，在630年麦加被穆罕默德占领后与父亲一起皈依伊斯兰教。他于633年参加伊斯兰军队对[叙利亚的征服之战](../Page/叙利亚.md "wikilink")，战后成为[大马士革总督](../Page/大马士革.md "wikilink")。后来穆阿维叶在叔父奥斯曼任哈里发时代获得对整个叙利亚的统治权，他完全把叙利亚当作自己的私人领地来经营。穆阿维叶在657年的[隋芬战役中依靠](../Page/隋芬战役.md "wikilink")“[神裁](../Page/神裁.md "wikilink")”的方式战胜阿里，并在阿里（最后一位“纯洁的哈里发”）于661年被刺杀后压服反对者，成为哈里发。679年，[穆阿维叶一世宣布其子](../Page/穆阿维叶一世.md "wikilink")[叶齐德一世为哈里发继承人](../Page/叶齐德一世.md "wikilink")，从而将哈里发的选举制破坏。从此[阿拉伯帝国成为一个由世袭王朝统治的军事帝国](../Page/阿拉伯帝国.md "wikilink")。

## 与反对者的战争

倭马亚王朝的建立伴随着其最大的反对者伊斯兰教[什叶派的兴起](../Page/什叶派.md "wikilink")。什叶派不承认倭马亚王朝的哈里发地位为合法，坚持哈里发一职只能从先知女婿阿里的后代中产生。由于阿里的长子[哈桑主动放弃哈里发地位并于穆阿维叶一世统治时期被毒死](../Page/哈桑·本·阿里·本·阿比·塔利卜.md "wikilink")，什叶派转而支持阿里的次子[侯赛因成为哈里发](../Page/伊玛目侯赛因.md "wikilink")。侯赛因在682年（叶齐德一世统治时期）于[库法附近被倭马亚王朝军队杀害](../Page/库法.md "wikilink")，使什叶派极为震怒。[阿卜杜拉·伊本·祖拜尔](../Page/阿卜杜拉·伊本·祖拜尔.md "wikilink")（穆罕默德最初的信徒和战友[祖拜尔之子](../Page/祖拜尔.md "wikilink")）强烈反对倭马亚王朝，他实际上占领了阿拉伯帝国境内的很多地区，并于[汉志](../Page/汉志.md "wikilink")（**希贾兹**）建立了自己的政权。在哈里发[阿卜杜勒·马利克时代](../Page/阿卜杜勒·马利克.md "wikilink")，倭马亚王朝才消灭了麦加的什叶派政权。在整个倭马亚王朝时期，政府与什叶派一直发生战斗，但什叶派从未被彻底剪除。而且什叶派最后协助[阿拔斯王朝终结了倭马亚家族的统治](../Page/阿拔斯王朝.md "wikilink")。

倭马亚王朝的另一支主要反对力量是伊斯兰教[哈瓦利吉派](../Page/哈瓦利吉派.md "wikilink")，该派主要是由隋芬之战后因反对阿里屈从于神裁结果而脱离阿里阵营的士兵和信徒构成。

## 对外扩张

倭马亚王朝时代，阿拉伯帝国的对外征服达到了另一个高峰。他們的疆域最廣闊之時，東至[中亞和印度](../Page/中亞.md "wikilink")、西至[伊比利亞半島](../Page/伊比利亞半島.md "wikilink")，領有整個南地中海沿岸。在[蒙古帝國興起前](../Page/蒙古帝國.md "wikilink")，沒有一個帝國的疆域比倭马亚王朝廣闊。

穆阿维叶一世进攻[东罗马帝国的计划因东罗马人的秘密武器](../Page/东罗马帝国.md "wikilink")“[希腊火](../Page/希腊火.md "wikilink")”而慘敗，但这位统治者创建了阿拉伯人的第一支[海军](../Page/海军.md "wikilink")。[哈贾杰·本·优素福在阿卜杜勒](../Page/哈贾杰·本·优素福.md "wikilink")·马利克时代率领阿拉伯军队向[中亚挺进](../Page/中亚.md "wikilink")，而阿拉伯人的宗教最终永远地征服了这一地区。哈贾杰·本·优素福的侄子[穆罕默德·伊本·卡希姆则南下攻入](../Page/穆罕默德·伊本·卡希姆.md "wikilink")[印度](../Page/印度.md "wikilink")，迅速征服了[信德和](../Page/信德.md "wikilink")[旁遮普地区](../Page/旁遮普.md "wikilink")，标志着伊斯兰教从此在印度扎根。向東被[突骑施抵抗](../Page/突骑施.md "wikilink")。在公元700年倭马亚王朝统治着2140万人的居民。

对[欧洲地区的进攻也随即展开](../Page/欧洲.md "wikilink")。711年，倭马亚王朝的[埃及总督](../Page/埃及.md "wikilink")[穆萨·伊本·努塞尔手下的](../Page/穆萨·伊本·努塞尔.md "wikilink")[摩尔人将领](../Page/摩尔人.md "wikilink")[塔里克·伊本·齊亞德率领穆斯林军队渡过](../Page/塔里克·伊本·齊亞德.md "wikilink")[直布罗陀海峡](../Page/直布罗陀海峡.md "wikilink")（该海峡以塔里克本人的名字命名）进入[伊比利亚半岛](../Page/伊比利亚半岛.md "wikilink")，不久穆萨·伊本·努塞尔本人率领更多军队赶到进而灭亡[西哥特王国](../Page/西哥特王国.md "wikilink")。两位将军攻占[伊比利亚半岛三分之二的领土](../Page/伊比利亚半岛.md "wikilink")。但是，732年从西班牙进入[法兰克王国的一支伊斯兰杂牌军在](../Page/法兰克王国.md "wikilink")[普瓦捷被](../Page/普瓦捷.md "wikilink")[墨洛温王朝的](../Page/墨洛温王朝.md "wikilink")[宫相](../Page/宫相.md "wikilink")[铁锤查理击败后](../Page/铁锤查理.md "wikilink")(在[高加索地区也被](../Page/高加索地区.md "wikilink")[可薩汗國抵抗](../Page/可薩汗國.md "wikilink"))，阿拉伯人实际上放弃了对欧洲的扩张。到750年，倭马亚王朝统治着3400万人的居民。

## 文化建设

倭马亚王朝时代建立了伊斯兰教几座最重要的[清真寺](../Page/清真寺.md "wikilink")，包括[耶路撒冷的](../Page/耶路撒冷.md "wikilink")[圆顶清真寺和](../Page/圆顶清真寺.md "wikilink")[阿克萨清真寺](../Page/阿克萨清真寺.md "wikilink")，以及[大马士革的](../Page/大马士革.md "wikilink")[倭马亚大寺](../Page/倭马亚大寺.md "wikilink")。

阿卜杜勒·马利克任哈里发时代，发行了阿拉伯人自己的第一种金币[第纳尔](../Page/第纳尔.md "wikilink")，並在全国流通。直至今日，不少阿拉伯國家的貨幣名稱仍然叫作“第納爾”，由此可見影響之深遠。

## 灭亡

由于与[什叶派和](../Page/什叶派.md "wikilink")[哈瓦利吉派的持续冲突](../Page/哈瓦利吉派.md "wikilink")，倭马亚王朝的统治长年陷于不稳定的情况之中。两派都采取暴力手段抵抗[倭马亚王朝的镇压行动](../Page/倭马亚王朝.md "wikilink")，以至数位倭马亚王朝哈里发死于刺客之手，而且几乎所有倭马亚王朝哈里发的在位时间都极短，可說是国内的政治敌对葬送了倭马亚王朝。[穆罕默德的叔父](../Page/穆罕默德.md "wikilink")[阿拔斯·伊本·阿卜杜勒·穆塔里卜的后代](../Page/阿拔斯·伊本·阿卜杜勒·穆塔里卜.md "wikilink")[阿布·阿拔斯-萨法赫利用什叶派与哈瓦利吉派暴动之机](../Page/阿布·阿拔斯-萨法赫.md "wikilink")，借助波斯人[阿布·穆斯里姆的军事力量最终推翻了倭马亚王朝](../Page/阿布·穆斯里姆.md "wikilink")，所有倭马亚家族成员不久都遭屠杀。

## 西班牙的[后倭马亚王朝](../Page/后倭马亚王朝.md "wikilink")

在阿布·阿拔斯（其外号“萨法赫”意为屠夫）对倭马亚家族的屠杀中，有一名幸存者[阿卜杜勒·拉赫曼](../Page/阿卜杜勒·拉赫曼一世.md "wikilink")（即日后的[埃米尔](../Page/埃米尔.md "wikilink")**阿卜杜勒·拉赫曼一世**）逃至西班牙，并在那里建立了自己的政权。该政权在阿拉伯帝国的倭马亚王朝崩溃之后长期以[科尔多瓦为中心统治伊比利亚半岛广大地区](../Page/科尔多瓦_\(西班牙\).md "wikilink")，成为欧洲最重要的伊斯兰教政权。

## 倭马亚王朝统治者列表

<table>
<thead>
<tr class="header">
<th><table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>哈里发</p></td>
</tr>
<tr class="even">
<td><p>661年 - 750年</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>align ="center" style="color: white; height: 30px; background: red no-repeat scroll top left;"|名号</p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/穆阿维叶一世.md" title="wikilink">穆阿维叶一世</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/叶齐德一世.md" title="wikilink">叶齐德一世</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穆阿维叶二世.md" title="wikilink">穆阿维叶二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/马尔万一世.md" title="wikilink">马尔万一世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;|<a href="../Page/阿卜杜勒-马利克·本·马尔万·本·哈卡姆.md" title="wikilink">{{Nowrap</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/瓦利德一世.md" title="wikilink">瓦利德一世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;|<a href="../Page/苏莱曼_(哈里发).md" title="wikilink">苏莱曼</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/奥马尔二世.md" title="wikilink">奥马尔二世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;|<a href="../Page/叶齐德二世.md" title="wikilink">叶齐德二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/希沙姆_(哈里发).md" title="wikilink">希沙姆</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;|<a href="../Page/瓦利德二世.md" title="wikilink">瓦利德二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/叶齐德三世.md" title="wikilink">叶齐德三世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;|<a href="../Page/易卜拉欣_(哈里发).md" title="wikilink">易卜拉欣</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/马尔万二世.md" title="wikilink">马尔万二世</a></p></td>
</tr>
</tbody>
</table></th>
<th><table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>科尔多瓦埃米尔</p></td>
</tr>
<tr class="even">
<td><p>756年 - 929年</p></td>
</tr>
<tr class="odd">
<td><p>align ="center" style="color: white; height: 30px; background: red no-repeat scroll top left;"|名号</p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜勒·拉赫曼一世.md" title="wikilink">阿卜杜勒·拉赫曼一世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/希沙姆一世.md" title="wikilink">希沙姆一世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/哈卡姆一世.md" title="wikilink">哈卡姆一世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜勒·拉赫曼二世.md" title="wikilink">阿卜杜勒·拉赫曼二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/穆罕默德一世_(科尔多瓦).md" title="wikilink">穆罕默德一世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/蒙齐尔.md" title="wikilink">蒙齐尔</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜拉·伊本·穆罕默德.md" title="wikilink">阿卜杜拉·伊本·穆罕默德</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜勒·拉赫曼三世.md" title="wikilink">阿卜杜勒·拉赫曼三世</a></p></td>
</tr>
</tbody>
</table></th>
<th><table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>科尔多瓦哈里发</p></td>
</tr>
<tr class="even">
<td><p>929年 - 1031年</p></td>
</tr>
<tr class="odd">
<td><p>align ="center" style="color: white; height: 30px; background: navy no-repeat scroll top left;"|名号</p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜勒·拉赫曼三世.md" title="wikilink">阿卜杜勒·拉赫曼三世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/哈卡姆二世.md" title="wikilink">哈卡姆二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/希沙姆二世.md" title="wikilink">希沙姆二世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/穆罕默德二世_(科尔多瓦).md" title="wikilink">穆罕默德二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/苏莱曼_(科尔多瓦).md" title="wikilink">苏莱曼</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/穆罕默德二世_(科尔多瓦).md" title="wikilink">穆罕默德二世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;|<a href="../Page/希沙姆二世.md" title="wikilink">希沙姆二世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/苏莱曼_(科尔多瓦).md" title="wikilink">苏莱曼</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/阿里·伊本·哈穆德·纳西尔.md" title="wikilink">阿里·伊本·哈穆德·纳西尔</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜勒·拉赫曼四世.md" title="wikilink">阿卜杜勒·拉赫曼四世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/卡西姆·马蒙.md" title="wikilink">卡西姆·马蒙</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/叶海亚·穆塔里.md" title="wikilink">叶海亚·穆塔里</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/阿卜杜勒·拉赫曼五世.md" title="wikilink">阿卜杜勒·拉赫曼五世</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/穆罕默德三世_(科尔多瓦).md" title="wikilink">穆罕默德三世</a></p></td>
</tr>
<tr class="even">
<td><p>align =center; height: 30px;| <a href="../Page/叶海亚·穆塔里.md" title="wikilink">叶海亚·穆塔里</a></p></td>
</tr>
<tr class="odd">
<td><p>align =center; height: 30px;| <a href="../Page/希沙姆三世.md" title="wikilink">希沙姆三世</a></p></td>
</tr>
</tbody>
</table></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

## 世系图

<center>

</center>

<noinclude>

## 参考文献

## 参见

  - [倭马亚家族](../Page/倭马亚家族.md "wikilink")：[倭马亚家族成员列表](../Page/倭马亚家族成员列表.md "wikilink")
  - [哈里发国](../Page/哈里发国.md "wikilink")：[阿拉伯帝国](../Page/阿拉伯帝国.md "wikilink")
  - [伊斯兰教](../Page/伊斯兰教.md "wikilink")：[穆斯林的征服](../Page/穆斯林的征服.md "wikilink")

{{-}}

[Category:阿拉伯帝国](../Category/阿拉伯帝国.md "wikilink")
[倭马亚王朝](../Category/倭马亚王朝.md "wikilink")
[Category:朝代](../Category/朝代.md "wikilink")
[Category:伊斯兰教历史](../Category/伊斯兰教历史.md "wikilink")
[Category:中东历史](../Category/中东历史.md "wikilink")
[Category:已不存在的帝國](../Category/已不存在的帝國.md "wikilink")
[Category:哈里发国](../Category/哈里发国.md "wikilink")