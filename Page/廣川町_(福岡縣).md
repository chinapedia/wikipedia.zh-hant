**廣川町**（）是位于[福岡縣南部的一個](../Page/福岡縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[八女郡](../Page/八女郡.md "wikilink")。

在福岡縣的規劃中，原本期望廣川町可以與相鄰的[八女市及八女郡其他町村進行合併](../Page/八女市.md "wikilink")，但當地多數居民卻是較偏向與[久留米市合併](../Page/久留米市.md "wikilink")，但也未能在町議會獲得通過；因此至今廣川町並未參予任何合併。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[上妻郡上廣川村](../Page/上妻郡.md "wikilink")、中廣川村、下廣川村。
  - 1948年4月1日：橫山村（現已合併為八女市）的部分地區被併入上廣川村。
  - 1955年4月1日：上廣川村與中廣川村合併為**廣川町**。
  - 1955年12月1日：下廣川村的部分地區被併入廣川町，其餘地區則被併入[三潴郡](../Page/三潴郡.md "wikilink")[筑邦町](../Page/筑邦町.md "wikilink")（現已被併入久留米市）。\[1\]

## 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1896年2月26日</p></th>
<th><p>1896年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>上妻郡下廣川村</p></td>
<td><p>八女郡下廣川村</p></td>
<td><p>1955年12月1日<br />
併入筑後市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1955年12月1日<br />
併入三瀦郡筑邦町</p></td>
<td><p>併入<a href="../Page/久留米市.md" title="wikilink">久留米市</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1955年12月1日<br />
併入廣川町</p></td>
<td><p>廣川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>上妻郡中廣川村</p></td>
<td><p>八女郡中廣川村</p></td>
<td><p>1955年4月1日<br />
廣川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上妻郡上廣川村</p></td>
<td><p>八女郡上廣川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>上妻郡横山村</p></td>
<td><p>八女郡橫山村</p></td>
<td><p>1948年4月1日<br />
併入上廣川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>横山村</p></td>
<td><p>1958年3月31日<br />
合併為上陽町</p></td>
<td><p>2006年10月1日<br />
併入八女市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1958年7月1日<br />
併入北川內村</p></td>
<td><p>1953年10月1日<br />
北川內町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

過去曾有[西日本鐵路](../Page/西日本鐵路.md "wikilink")[福島線通過轄區](../Page/福島線.md "wikilink")，但已於1958年停止營運。

### 道路

  - 高速道路

<!-- end list -->

  - [九州自動車道](../Page/九州自動車道.md "wikilink")：[廣川IC](../Page/廣川IC_\(福岡縣\).md "wikilink")
    - [廣川SA](../Page/廣川SA.md "wikilink")

## 姊妹、友好都市

### 海外

  - [沧浪区](../Page/沧浪区.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")[蘇州市](../Page/蘇州市.md "wikilink")）友好提携

## 參考資料

## 外部連結

  - [廣川町觀光協會](https://web.archive.org/web/20100304183030/http://www.mfj.co.jp/hirokawa/)

  - [廣川町商工會](http://www.hirosho.org/)

<!-- end list -->

1.