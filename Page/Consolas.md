**Consolas**是一套[等宽的字体](../Page/等宽字体.md "wikilink")，属[無襯線字體](../Page/無襯線字體.md "wikilink")，由[Lucas
de
Groot设计](../Page/Lucas_de_Groot.md "wikilink")。这个字体使用了[微軟的](../Page/微軟.md "wikilink")[ClearType](../Page/ClearType.md "wikilink")
[字型平滑技術](../Page/字型平滑.md "wikilink")，並隨同[Windows
Vista](../Page/Windows_Vista.md "wikilink")、[Office
2007及](../Page/Microsoft_Office_2007.md "wikilink")[Microsoft Visual
Studio中發行](../Page/Microsoft_Visual_Studio.md "wikilink")，或可在微軟的網站下載。在Windows
Vista的6套新字体中，Consolas近似於旧版Windows中的2款内置字型：[Lucida
Console與](../Page/Lucida_Console.md "wikilink")[Courier
New](../Page/Courier_New.md "wikilink")，主要是設計做為[代码的顯示字型之用](../Page/代码.md "wikilink")，特別之處是它的「0」字加入了一斜撇，以方便與字母「O」分辨。

在Consolas之前，代码的顯示字体大多為Courier
New或其他等宽的字型，字型的柔邊（反鋸齒）效果則依個人喜好選擇開啟或關閉，然而Consolas是專為柔邊效果而設計的字体，特別是為了搭配微軟的[ClearType技術](../Page/ClearType.md "wikilink")，如果不開啟ClearType，Consolas的顯示效果會打大折扣。另外，ClearType技術還需要搭配[液晶顯示器才會有最佳表現](../Page/液晶顯示器.md "wikilink")。

## 特色

## 程式碼編寫的應用

傳統上，程序员在Windows環境底下，一般都會使用[Courier
New或其他近似的固定字元寬度字型來顯示程式碼](../Page/Courier_New.md "wikilink")。通常用來編寫程式碼的程式都會讓編程員選擇用來顯示程式碼的字體。由於Courier
New的字型比較肥大，使每個畫面或頁面所能顯示的程式碼大為減少。Consolas除了能夠在較少的空間顯示更多的內容，它的清晰字型亦使程序员能夠更快捷的分辨每一個文字。

### 範例

以下為在一般顯示屏下，用Consolas或Courier
New來顯示程式碼的分別。由於Consolas支援ClearType，所以在[LCD顯示屏可以利用](../Page/LCD顯示屏.md "wikilink")[次畫素優視技術來使字體更清晰](../Page/次畫素優視技術.md "wikilink")。

  - 以下為一段使用了Consolas來顯示，並啟動了ClearType技術的[C++/CLI程式碼](../Page/C++/CLI.md "wikilink")：

<!-- end list -->

  - 以下為用作比較的同一段程式碼，但使用了傳統的Courier New字型：

## 外部連結

  -
  -
  - [Microsoft ClearType Font
    Collection](http://www.microsoft.com/typography/ClearTypeFonts.mspx)
    at Microsoft Typography

  - [Microsoft ClearType Font
    Collection](http://www.ascendercorp.com/ctfonts.html) at Ascender
    Corporation

  - [Download Consolas
    Font](http://www.microsoft.com/downloads/details.aspx?familyid=048DC840-14E1-467D-8DCA-19D2A8FD7485&displaylang=en)
    Download Microsoft PowerPoint Viewer 2007 which includes the
    ClearType Collection

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:等宽字体](../Category/等宽字体.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")