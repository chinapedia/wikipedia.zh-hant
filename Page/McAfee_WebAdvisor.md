**McAfee
SiteAdvisor**是一个评估[网站安全性的组织](../Page/网站.md "wikilink")，也指其所提供的服務，該組織已目前已经收入了近一百万个网站，適用的瀏覽器包括[Internet
Explorer及](../Page/Internet_Explorer.md "wikilink")[Mozilla
Firefox等主流瀏覽器](../Page/Mozilla_Firefox.md "wikilink")，該組織於2006年4月5日，被[McAfee公司正式收购](../Page/McAfee.md "wikilink")，成为McAfee的全资子公司，稱之為McAfee
SiteAdvisor。現在已改名為McAfee Web Advisor。

## 概要

McAfee SiteAdvisor是透過安裝在[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")、[Google
Chrome或](../Page/Google_Chrome.md "wikilink")[Mozilla
Firefox上](../Page/Mozilla_Firefox.md "wikilink")，針對瀏覽的網站進行測試，並且將測試網站分為四級。分別是：

  - [<File:McAfee>
    Green.gif](https://zh.wikipedia.org/wiki/File:McAfee_Green.gif "fig:File:McAfee Green.gif")代表該網站經過McAfee
    SiteAdvisor測試，沒有發現任何重大可疑或破壞的行為
  - [<File:McAfee>
    Yellow.gif](https://zh.wikipedia.org/wiki/File:McAfee_Yellow.gif "fig:File:McAfee Yellow.gif")代表該網站經過McAfee
    SiteAdvisor測試，發現網站進行一些可疑的行為
  - [<File:McAfee>
    Red.gif](https://zh.wikipedia.org/wiki/File:McAfee_Red.gif "fig:File:McAfee Red.gif")代表該網站經過McAfee
    SiteAdvisor測試，發現網站進行間諜、破壞等不良行為
  - 灰色代表該網站尚未經過McAfee SiteAdvisor測試

## SiteAdvisor Plus

[Sa_plus.gif](https://zh.wikipedia.org/wiki/File:Sa_plus.gif "fig:Sa_plus.gif")
McAfee提供另一種需要付費的SiteAdvisor Plus給需要更強大保護的網路使用者。

## 參見

  - [McAfee](../Page/McAfee.md "wikilink")

## 外部連結

  - [McAfee
    SiteAdvisor](https://web.archive.org/web/20141012233952/http://www.siteadvisor.com/)

[Category:信誉管理](../Category/信誉管理.md "wikilink")
[Category:免费软件](../Category/免费软件.md "wikilink")
[Category:Firefox 附加组件](../Category/Firefox_附加组件.md "wikilink")
[Category:安全软件](../Category/安全软件.md "wikilink")