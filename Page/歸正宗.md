**歸正宗**（）也称**加尔文宗**、**更正宗**、**改革宗**，是[基督新教的](../Page/基督新教.md "wikilink")[宗派之一](../Page/教派.md "wikilink")。

狭义的归正宗（Continental Reformed
church）即[欧洲的](../Page/欧洲.md "wikilink")[加爾文主義教会](../Page/加爾文主義.md "wikilink")，廣義的歸正宗（Reformed
church）还包含[清教](../Page/清教.md "wikilink")、[結盟宗](../Page/結盟宗.md "wikilink")、[長老會及](../Page/長老宗.md "wikilink")[公理会等所有信奉加尔文主义的教會](../Page/公理会.md "wikilink")。歐洲大陸的歸正宗早期興盛於瑞士與荷蘭，長老會和公理會則早期興盛於蘇格蘭、英格蘭與美國。长老会与狭义的归正宗教会在教会治理上都采用[长老制](../Page/长老制.md "wikilink")，但公理会采用[会众制](../Page/會眾制.md "wikilink")。

若用來指一整套[信仰生活與價值觀](../Page/信仰.md "wikilink")，稱為归正宗信仰（Reformed
Faith）。如果是指[神學](../Page/神學.md "wikilink")，是指[加尔文主义](../Page/加爾文主義.md "wikilink")（或稱歸正神学、改革宗神學）。上述含義源頭都來自[宗教改革家](../Page/宗教改革.md "wikilink")[約翰·加爾文](../Page/讓·喀爾文.md "wikilink")。

其著名國際組織有[普世改革宗教會協會](../Page/普世改革宗教會協會.md "wikilink")（The World Communion
of Reformed Churches）。

## 参考文献

1.  William Edgar, *Truth in All Its Glory: Commending the Reformed
    Faith*, P and R Publishing Company.

## 外部链接

  - [改革宗神學院](http://www.crts.edu/)
  - [印尼歸正福音教會](../Page/:歸正福音教會.md "wikilink")
  - [吉隆坡歸正福音教會](http://www.klrec.net/)
  - [臺北歸正福音教會](http://www.rectp.org/)
  - [香港歸正福音教會](http://www.rechk.org)
  - [主流改革宗学者视频集](http://www.tudou.com/playlist/reformed)
  - [归正神学书目集](http://zuoshihezijide.blog.sohu.com/135713131.html)
  - [台北归正改革宗翻译社（出版社）书目](http://13571023.blog.hexun.com/72569802_d.html)

[歸正宗](../Category/歸正宗.md "wikilink")
[Category:基督教教会与教派](../Category/基督教教会与教派.md "wikilink")
[Category:宗教改革](../Category/宗教改革.md "wikilink")