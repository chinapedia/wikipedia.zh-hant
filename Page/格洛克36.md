**格洛克36**（Glock
36）綽號北極蛇是由[奧地利](../Page/奧地利.md "wikilink")[格洛克公司推出的瘦身袖珍型](../Page/格洛克.md "wikilink")[半自動手槍](../Page/半自動手槍.md "wikilink")，發射[.45
ACP口徑彈藥](../Page/.45_ACP.md "wikilink")。

## 設計

格洛克36是格洛克公司第一種的瘦身手槍型號，[機匣](../Page/機匣.md "wikilink")、[套筒及](../Page/手槍套筒.md "wikilink")[槍管的尺寸與其他型號不同](../Page/槍管.md "wikilink")，為了保持握把的闊度而改用了只有6發容量的單排彈匣，彈匣底座加厚以便於握持，因此無法與同口徑型號互換。

格洛克36擁有非常緊湊的外型，是[美國國內很流行的小型](../Page/美國.md "wikilink")[自衛手槍](../Page/自衛.md "wikilink")，亦是便衣探員及私人保安等常用的「後備手槍」。目前美國民間市場售價為$519至$709[美元](../Page/美元.md "wikilink")。

## 參考

  - [.45 ACP](../Page/.45_ACP.md "wikilink")
  - [格洛克手槍](../Page/格洛克手槍.md "wikilink")

## 資料來源

## 外部链接

  - —[格洛克官方主頁](http://www.glock.com/)

  - —[Tactical-Life.com—Massad Ayoob: 8 Subcompact Glocks Perfect For
    Backup
    Duty](http://www.tactical-life.com/firearms/8-subcompact-glocks-backup/#ppma-g36)

  - —[D Boy Gun
    World（GLOCK 36）](http://firearmsworld.net/austria/glock/36/36.htm)

[en:Glock 36](../Page/en:Glock_36.md "wikilink")

[Category:格洛克手槍](../Category/格洛克手槍.md "wikilink")
[Category:袖珍手槍](../Category/袖珍手槍.md "wikilink") [Category:.45
ACP口徑槍械](../Category/.45_ACP口徑槍械.md "wikilink")