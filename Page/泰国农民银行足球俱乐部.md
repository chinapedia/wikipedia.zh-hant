**泰国农民银行足球俱乐部**（**สโมสรฟุตบอลธนาคารกสิกรไทย**；**Thai Farmers Bank
FC**）是已经解散了的[泰国足球俱乐部](../Page/泰国.md "wikilink")，俱乐部属于[泰华农民银行](../Page/泰華農民銀行.md "wikilink")（Kasikorn
Bank，原农民银行）。

球队是1994-95赛季[亚洲冠军联赛的冠军球队](../Page/亚洲冠军联赛.md "wikilink")。

## 球队荣誉

  - **[亞洲聯賽冠軍盃](../Page/亞洲聯賽冠軍盃.md "wikilink")**: 2次

1994, 1995

  - **[泰国足球联赛](../Page/泰国足球联赛.md "wikilink")**: 4次

1990, 1992, 1993, 1995

  - **[女王杯](../Page/女王杯.md "wikilink")**: 4次

1994, 1995, 1996, 1997

  - **[亚非俱乐部锦标赛](../Page/亚非俱乐部锦标赛.md "wikilink")**: 1

1994

[Thai Farmers](../Category/泰国足球俱乐部.md "wikilink")