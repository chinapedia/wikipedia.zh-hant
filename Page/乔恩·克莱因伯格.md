[Jon_Kleinberg.jpg](https://zh.wikipedia.org/wiki/File:Jon_Kleinberg.jpg "fig:Jon_Kleinberg.jpg")
**乔恩·克莱因伯格**（，）是美国-{zh-cn:计算机科学;
zh-tw:電腦科学;}-家，[康奈尔大学](../Page/康奈尔大学.md "wikilink")-{zh-cn:计算机科学;
zh-tw:電腦科学;}-教授，2006年获得[国际数学联盟颁发的](../Page/国际数学联盟.md "wikilink")[内万林纳奖](../Page/内万林纳奖.md "wikilink")。学生昵称他为“反叛王”（rebel
King, Kleinberg的同字母异序词）。

克莱因伯格以解决重要而且实际的问题并能够从中发现深刻的数学思想而著称。他的研究跨越了从-{zh-cn:计算机网络;
zh-tw:電腦網路;}-由到[-{zh-cn:数据挖掘;
zh-tw:資料探勘;}-到生物结构比对等诸多领域](../Page/数据挖掘.md "wikilink")。他最为人称道的成就是“[小世界理论](../Page/小世界理论.md "wikilink")”和[万维网搜索算法](../Page/万维网.md "wikilink")。他设计了[HITS算法](../Page/HITS算法.md "wikilink")，该算法的相关研究工作启发了[Google的](../Page/Google.md "wikilink")[PageRank算法的诞生](../Page/PageRank.md "wikilink")。

克莱因伯格在1971年10月出生於[波士頓](../Page/波士頓.md "wikilink")，1993年本科毕业于康奈尔大学，1996年在[麻省理工学院获得博士学位](../Page/麻省理工学院.md "wikilink")，论文题目为“Approximation
Algorithms for Disjoint Paths Problems”，导师Michel
Goemans。1995年－1997年在IBM研究院做研究。目前的研究兴趣是-{zh-cn:网络与信息;
zh-tw:網路與資訊;}-组合结构的数学分析与建模。

他与Éva Tardos合著的算法教材《Algorithm
Design》（Addison-Wesley出版，清华大学出版社出版了影印版《算法设计》）获得了很高评价。此外，他发表的许多论文引用數眾多，影响深遠。

## 外部链接

  - [克莱因伯格在康奈尔的主页](http://www.cs.cornell.edu/home/kleinber/)
  - [克莱因伯格在麦克阿瑟基金会网站上的简历](https://web.archive.org/web/20051001064359/http://www.macfound.org/programs/fel/fellows/kleinberg_jon.htm)

[Category:在世人物](../Category/在世人物.md "wikilink")
[K](../Category/计算机科学家.md "wikilink")
[K](../Category/康乃爾大學校友.md "wikilink")
[K](../Category/麻省理工學院校友.md "wikilink")
[Category:内万林纳奖获得者](../Category/内万林纳奖获得者.md "wikilink")