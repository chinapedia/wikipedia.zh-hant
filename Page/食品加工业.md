[Production_of_cheese_1.jpg](https://zh.wikipedia.org/wiki/File:Production_of_cheese_1.jpg "fig:Production_of_cheese_1.jpg")\]\]
**食品加工业**是將[食物透過物理或化學途徑轉化為其他形態的](../Page/食物.md "wikilink")[食物產業](../Page/食物產業.md "wikilink")，旨於令食材更易包裝及烹飪。典形的食物加工包括[切碎](../Page/切碎.md "wikilink")、[浸漬](../Page/浸漬.md "wikilink")、[液化](../Page/液化.md "wikilink")、[乳化](../Page/乳化.md "wikilink")、[烹調](../Page/烹調.md "wikilink")（例如煮、烤、煎、燒）、[醃製](../Page/醃製.md "wikilink")，甚至[巴士德消毒法或其他](../Page/巴士德消毒法.md "wikilink")[食物防腐](../Page/食物防腐.md "wikilink")、[罐裝食品和](../Page/罐裝.md "wikilink")[包裝方法](../Page/包裝.md "wikilink")。切片、冷凍、乾化等初級加工法亦包括在內。

## 歷史

食品加工可以追溯至史前時代。當時已有粗糙的加工方法，例如[發酵](../Page/發酵.md "wikilink")、[曬乾](../Page/曬乾.md "wikilink")、以[鹽作保存食物](../Page/鹽.md "wikilink")，以及各種烹飪方法（[烘烤](../Page/烘烤.md "wikilink")、[煙燻](../Page/煙燻.md "wikilink")、[蒸](../Page/蒸.md "wikilink")）。這些基本的食物處理都牽涉到改變食物中[酶的結構](../Page/酶.md "wikilink")，而能夠防止微生物所致的快速腐壞。在出現罐頭之前，以鹽保存食物尤其在士兵和水手之間常見。這些方法在[古埃及](../Page/古埃及.md "wikilink")、[新巴比倫](../Page/新巴比倫.md "wikilink")、[古希臘及](../Page/古希臘.md "wikilink")[羅馬等古文明中有所記載](../Page/羅馬.md "wikilink")，考古後亦發現歐洲、北美洲、南美洲和亞洲都有相關證據。在[工業革命前食物加工方法一直被沿用](../Page/工業革命.md "wikilink")，變化不大。[餡餅在工業革命前已經出現](../Page/餡餅.md "wikilink")，但在其前後都被認為是加工食物的一種。

現代的食物加工在十九至二十世紀開始發展以滿足軍事需要。1809年，[尼古拉·阿佩爾為](../Page/尼古拉·阿佩爾.md "wikilink")[法國士兵發明了](../Page/法國.md "wikilink")[氣密式食物保存法](../Page/氣密式食物保存法.md "wikilink")，為1810年[罐頭被發明奠定基礎](../Page/罐頭.md "wikilink")。起初罐頭相對昂貴，並因用鉛製而對人體有害；但後來改進後罐頭就被世界各地所採用。而[巴士德消毒法則為紅酒](../Page/巴士德消毒法.md "wikilink")、啤酒及牛奶提供了保存方法。

於二十世紀間，[二戰](../Page/二戰.md "wikilink")、[太空競賽和](../Page/太空競賽.md "wikilink")[消費主義都令食物加工得以發展](../Page/消費主義.md "wikilink")，例如[噴霧乾燥](../Page/噴霧乾燥.md "wikilink")、[蒸發器](../Page/蒸發器.md "wikilink")、[濃縮果汁](../Page/濃縮果汁.md "wikilink")、[冷凍乾燥](../Page/冷凍乾燥.md "wikilink")、[代糖](../Page/代糖.md "wikilink")、[食用色素及](../Page/食用色素.md "wikilink")[苯甲酸鈉等人造防腐劑](../Page/苯甲酸鈉.md "wikilink")。二十世紀後期亦有[即食湯](../Page/即食湯.md "wikilink")、[MRE口糧等發明](../Page/MRE口糧.md "wikilink")。

由於一般大眾追求方便，西歐和北美於二十世紀後期在食物加工上急速發展。相關公司以中產母親作為銷售目標，創造了[急凍飯盒和濃縮果汁等產品](../Page/急凍飯盒.md "wikilink")。

## 優點和缺點

### 優點

食物加工的優點包括去除毒素、防腐、易於推廣和運輸、穩定的質素等等。

### 缺點

任何食物加工都會導致營養流失，例如以熱力加工會破壞[維他命C](../Page/維他命C.md "wikilink")。因此罐裝水果較新鮮水果所含的維他命C低。

## 参考文献

## 参见

  - [现代食品工程](../Page/现代食品工程.md "wikilink")
  - [燒臘工場](../Page/燒臘.md "wikilink")
  - [食物保存法](../Page/食物保存法.md "wikilink")
  - [流變學](../Page/流變學.md "wikilink")
  - [膳食補充劑](../Page/膳食補充劑.md "wikilink")

[de:Lebensmittelindustrie\#Industrielle
Lebensmittelverarbeitung](../Page/de:Lebensmittelindustrie#Industrielle_Lebensmittelverarbeitung.md "wikilink")

[Category:产业](../Category/产业.md "wikilink")
[食品加工业](../Category/食品加工业.md "wikilink")