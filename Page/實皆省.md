**實皆省**
（）是[緬甸的一個省](../Page/緬甸.md "wikilink")，位於該國西北部。面積93,704.5平方公里，人口5,325,347人\[1\]。首府[实皆](../Page/实皆.md "wikilink")。下分8縣。

## 行政區劃

實皆省轄下8縣、34鎮\[2\]、198村組以及多個村莊。

### 縣

  - [坎迪縣](../Page/坎迪縣.md "wikilink")（ခန္တီးခရိုင်）
  - [吉靈廟縣](../Page/吉靈廟縣.md "wikilink")（ကလေးခရိုင်）
  - [杰沙縣](../Page/杰沙縣.md "wikilink")（ကသာခရိုင်）
  - [茂叻縣](../Page/茂叻縣.md "wikilink")（မော်လိုက်ခရိုင်）
  - [蒙育瓦縣](../Page/蒙育瓦縣.md "wikilink")（မုံရွာခရိုင်）
  - [實皆縣](../Page/實皆縣.md "wikilink")（စစ်ကိုင်းခရိုင်）
  - [瑞波縣](../Page/瑞波縣.md "wikilink")（ရွှေဘိုခရိုင်）
  - [德穆縣](../Page/德穆縣.md "wikilink")（တမူးခရိုင်）

### 鎮

實皆省轄下的鎮有\[3\] ：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/阿亞道鎮.md" title="wikilink">阿亞道鎮</a>（Ayadaw Township）</li>
<li><a href="../Page/班茂鎮.md" title="wikilink">班茂鎮</a>（Banmauk Township）</li>
<li><a href="../Page/布德林鎮.md" title="wikilink">布德林鎮</a>（Budalin Township）</li>
<li><a href="../Page/羌烏鎮.md" title="wikilink">羌烏鎮</a>（Chaung-U Township）</li>
<li><a href="../Page/坎迪鎮.md" title="wikilink">坎迪鎮</a>（Hkamti Township）</li>
<li><a href="../Page/霍馬林鎮.md" title="wikilink">霍馬林鎮</a>（Homalin Township）</li>
<li><a href="../Page/因多鎮.md" title="wikilink">因多鎮</a>（Indaw Township）</li>
<li><a href="../Page/吉靈廟鎮.md" title="wikilink">吉靈廟鎮</a>（Kale Township）</li>
<li><a href="../Page/葛禮瓦鎮.md" title="wikilink">葛禮瓦鎮</a>（Kalewa Township）</li>
<li><a href="../Page/甘勃盧鎮.md" title="wikilink">甘勃盧鎮</a>（Kanbalu Township）</li>
<li><a href="../Page/卡尼鎮.md" title="wikilink">卡尼鎮</a>（Kani Township）</li>
<li><a href="../Page/杰沙鎮.md" title="wikilink">杰沙鎮</a>（Katha Township）</li>
<li><a href="../Page/高林鎮.md" title="wikilink">高林鎮</a>（Kawlin Township）</li>
<li><a href="../Page/欽烏鎮.md" title="wikilink">欽烏鎮</a>（Khin-U Township）</li>
<li><a href="../Page/均拉鎮.md" title="wikilink">均拉鎮</a>（Kyunhla Township）</li>
<li><a href="../Page/茂叻鎮.md" title="wikilink">茂叻鎮</a>（Mawlaik Township）</li>
<li><a href="../Page/敏貢鎮.md" title="wikilink">敏貢鎮</a>（Mingin Township）</li>
</ul></td>
<td><ul>
<li><a href="../Page/蒙育瓦鎮.md" title="wikilink">蒙育瓦鎮</a>（Monywa Township）</li>
<li><a href="../Page/苗鎮.md" title="wikilink">苗鎮</a>（Myaung Township）</li>
<li><a href="../Page/敏務鎮.md" title="wikilink">敏務鎮</a>（Myinmu Township）</li>
<li><a href="../Page/勃萊鎮.md" title="wikilink">勃萊鎮</a>（Pale Township）</li>
<li><a href="../Page/龐賓鎮.md" title="wikilink">龐賓鎮</a>（Paungbyin Township）</li>
<li><a href="../Page/平梨鋪鎮.md" title="wikilink">平梨鋪鎮</a>（Pinlebu Township）</li>
<li><a href="../Page/實皆鎮.md" title="wikilink">實皆鎮</a>（Sagaing Township）</li>
<li><a href="../Page/薩林基鎮.md" title="wikilink">薩林基鎮</a>（Salingyi Township）</li>
<li><a href="../Page/瑞波鎮.md" title="wikilink">瑞波鎮</a>（Shwebo Township）</li>
<li><a href="../Page/迪貝因鎮.md" title="wikilink">迪貝因鎮</a>（Tabayin Township）</li>
<li><a href="../Page/德穆鎮.md" title="wikilink">德穆鎮</a>（Tamu Township）</li>
<li><a href="../Page/丹西鎮.md" title="wikilink">丹西鎮</a>（Taze Township）</li>
<li><a href="../Page/提堅鎮.md" title="wikilink">提堅鎮</a>（Tigyaing Township）</li>
<li><a href="../Page/韦莱镇.md" title="wikilink">韦莱镇</a>（Wetlet Township）</li>
<li><a href="../Page/文多鎮.md" title="wikilink">文多鎮</a>（Wuntho Township）</li>
<li><a href="../Page/耶烏鎮.md" title="wikilink">耶烏鎮</a>（Ye-U Township）</li>
<li><a href="../Page/因馬賓鎮.md" title="wikilink">因馬賓鎮</a>（Yinmabin Township）</li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

{{-}}

[M](../Category/緬甸省份.md "wikilink") [實皆省](../Category/實皆省.md "wikilink")

1.
2.  ["Myanmar States/Divisions & Townships Overview
    Map"](http://www.burmalibrary.org/docs6/MIMU001_A3_SD%20&%20Township%20Overview.pdf)
    Myanmar Information Management Unit (MIMU)

3.