[Dapanji_urumqi.jpg](https://zh.wikipedia.org/wiki/File:Dapanji_urumqi.jpg "fig:Dapanji_urumqi.jpg")
**大盘鸡**（，[拉丁维文](../Page/拉丁维文.md "wikilink")：）是主要用[鸡肉](../Page/鸡.md "wikilink")、[馬鈴薯](../Page/馬鈴薯.md "wikilink")、[辣椒制作的一道料理](../Page/辣椒.md "wikilink")。1990年代左右诞生于新疆，归类于[新疆菜](../Page/新疆菜.md "wikilink")。衍生出[大盘肚](../Page/大盘肚.md "wikilink")，[大盘鱼](../Page/大盘鱼.md "wikilink")，大盘鸭，大盘鹅、大盘排骨等菜色，使大盘鸡成为新疆大盘菜系列的代表\[1\]。亦是中国的清真餐厅出售的代表菜色。

与大盘鸡搭配食用的主食为“皮带面”，意思是形容这种面条像皮带一样宽。通常，在吃大盘鸡的时候，餐厅会赠送一份或者两份宽面条。这取决于客人点的是“中盘鸡”还是“大盘鸡”。还可以要额外的宽面条，但这通常要多付一些钱。面条通常是要拌着大盘鸡一起吃。

## 历史

大盘鸡的来源有多种说法。在新疆当地，最为知名品牌的是柴窝堡大盘鸡（柴窝堡辣子鸡）和沙湾大盘鸡。通常认为大盘鸡起源于这两地，亦认为大盘鸡最初来源于湖南[辣子鸡](../Page/辣子鸡.md "wikilink")\[2\]。与沙湾相关的说法是，1990年前后在[沙湾县的长途汽车饭店里四川厨师为过路司机赶路而产生的一道新疆菜](../Page/沙湾县.md "wikilink")，川菜的鸡和北方面食一起上。

与大盘鸡相关的一些民间[传说](../Page/传说.md "wikilink")： \<blockquote style="padding:
0.5em 0.8em; border: 1px solid blue;
{{\#ifeq:民国初年，一位姓张的四川厨师为躲避战争，前往新疆[沙湾县居住](../Page/沙湾县.md "wikilink")。发明大盘鸡\[3\]。或说，张厨师在新疆312国道旁开饭馆，以卖炒面、拌面为主。

后来汽车司机到他的饭馆吃饭，说[炒面](../Page/炒面.md "wikilink")、[拌面太乾](../Page/拌面.md "wikilink")，要一份[辣子鸡](../Page/辣子鸡.md "wikilink")，多放汤、和面条拌在一起。最终这种食品被称为大盘鸡。|fix|height:
120px; overflow: auto;}}{{\#if:|height: ; overflow: auto;}}"\>}}

</blockquote>

## 制作方法

将处理乾净的鸡剁成小块，馬鈴薯去皮切片。调料可以有[盐](../Page/盐.md "wikilink")、[味精](../Page/味精.md "wikilink")、[白糖](../Page/白糖.md "wikilink")、[薑](../Page/薑.md "wikilink")、[蒜](../Page/蒜.md "wikilink")、[葱](../Page/葱.md "wikilink")、[花椒](../Page/花椒.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")，根据口味自选。据说制作时放入啤酒能使鸡肉更嫩。

## 备注

## 参考

[Category:新疆食品](../Category/新疆食品.md "wikilink")
[Category:雞肉料理](../Category/雞肉料理.md "wikilink")

1.

2.
3.