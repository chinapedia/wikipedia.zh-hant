[Tsukasa_Hojo_20100702_Japan_Expo_1.jpg](https://zh.wikipedia.org/wiki/File:Tsukasa_Hojo_20100702_Japan_Expo_1.jpg "fig:Tsukasa_Hojo_20100702_Japan_Expo_1.jpg")的簽名會。\]\]

**北条司**（筆名，本名為**北-{條}-司**，），出生於[日本](../Page/日本.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink")[小倉市](../Page/小倉市.md "wikilink")（现[北九州市](../Page/北九州市.md "wikilink")），是一位主要擅長現代寫實故事的[日本漫畫家](../Page/日本漫画家列表.md "wikilink")，最著名的代表作為《[貓眼三姐妹](../Page/貓眼三姐妹.md "wikilink")》（Cat's
Eye）與《[城市獵人](../Page/城市獵人.md "wikilink")》（City Hunter）。

## 簡歷

  - 1979年時，獲選[集英社舉辦的第](../Page/集英社.md "wikilink")18回[手塚賞入圍獎](../Page/手塚賞.md "wikilink")，作品為《太空天使》()。同期的得獎者還有[藤原KAMUI](../Page/藤原KAMUI.md "wikilink")(，《[勇者鬥惡龍](../Page/勇者鬥惡龍_\(漫畫\).md "wikilink")》)與[野部利雄](../Page/野部利雄.md "wikilink")(《[擂台一片天](../Page/擂台一片天.md "wikilink")》，日文原名：《》)

<!-- end list -->

  - 1980年時，以《[貓眼三姐妹](../Page/貓眼三姐妹.md "wikilink")》一作正式出道，開始長達數年的連載創作。

<!-- end list -->

  - 1981年毕业于[九州产业大学艺术系服装设计专业](../Page/九州产业大学.md "wikilink")。凭长篇[作品](../Page/作品.md "wikilink")《猫眼三姐妹》成名，其后推出的《城市猎人》将其[漫画事业推向高峰](../Page/漫画.md "wikilink")。

<!-- end list -->

  - 《城市猎人》结束后的几年里以创作中短篇漫画为主。《阳光少女》、全彩短篇“SPLASH\!系列”等[作品都是在这一阶段诞生](../Page/作品.md "wikilink")。

<!-- end list -->

  - 1996年，为[性别倒错为题材的长篇](../Page/性别倒错.md "wikilink")[作品](../Page/作品.md "wikilink")《[非常家庭](../Page/非常家庭.md "wikilink")》在[青年漫画杂志](../Page/青年漫画杂志.md "wikilink")《ALL-MAN》上推出，[作品以夸张诙谐的手法分析了时下敏感的](../Page/作品.md "wikilink")[社会问题](../Page/社会.md "wikilink")。其间还用真人照片和电脑绘画结合，制作了全彩漫画短篇集《幸福的人》。

<!-- end list -->

  - 2001年，《城市獵人》續集作品《[天使心](../Page/天使心_\(北条司\).md "wikilink")》(*Angel
    Heart*)在青年漫画周刊《Comic Bunch》5月创刊号上开始连载，作品沿用了《城市猎人》中的部分人物。

## 作品

  - 《我是男子漢》（）：1980年，短篇[作品](../Page/作品.md "wikilink")。
  - 《三級刑事》（）：1981年，短篇[作品](../Page/作品.md "wikilink")。
  - 《[貓眼三姐妹](../Page/貓眼三姐妹.md "wikilink")》（Cat's Eye）：1981年至1985年間連載。
  - 《太空天使》（）：1982年，以1979年時得獎作為基礎修改後刊出的短篇[作品](../Page/作品.md "wikilink")。
  - 《城市獵人 - XYZ》（City Hunter -
    XYZ）：1983年，後來成為長篇連載故事的短篇[作品](../Page/作品.md "wikilink")。
  - 《城市猎人 - 双刃剑》（City Hunter - ）：1984年，試驗性短篇的第二集，也確定了《城市獵人》系列的連載。
  - 《[城市獵人](../Page/城市獵人.md "wikilink")》（City
    Hunter）：1985年至1991年間連載，堪稱是北条司個人最受歡迎的系列作品。
  - 《貓咪報恩記》（）：1986年，短篇作品。
  - 《天使的禮物》（）：1988年，短篇作品。
  - 《計程車駕駛》（Taxi Driver）：1990年，短篇作品。
  - 《家庭劇場》（）：1992年，短篇作品合集。
  - 《少女的季節-夏日之夢》（）：1992年，短篇作品。
  - 《櫻花盛開時》（）：1993年，短篇作品。
  - 《-{zh-cn:阳光少女; zh-tw:豔陽少女;}-》（）：1993年至1994年間的中篇連載。
  - 《RASH\!\!》：1994年至1995年間的中篇連載。
  - 《青空之盡頭-少年們的戰場》（）：1995年，短篇作品。
  - 《少年們的夏天\~珍妮的樂章\~》（\~Melody of Jenny\~）：1995年，短篇作品。
  - 《美國夢》（American Dream）：1995年，短篇[作品](../Page/作品.md "wikilink")。
  - 《[非常家庭](../Page/非常家庭.md "wikilink")》（F.Compo）：1996年至2000年長篇連載。
  - 《鹦鹉-幸福的人》（Parrot）：1997年6月，漫畫與電腦繪圖作品的短篇合集。
  - 《[天使心](../Page/天使心_\(漫畫\).md "wikilink")》（Angel
    Heart）：由2001年連載至2010年，長篇漫畫作品，以《城市獵人》主要人物為基礎設定，故事內容與其為平行時空的新作。
  - 《天使心 2nd
    Season》（）：由2010年12月至2017年5月25日連載的長篇，故事延續《天使心》，主要是為了配合連載的雜誌變更所作的切換。
  - 《》：《Cat's Eye》的續集，由北条司擔任原作，作畫由（）負責。

## 外部連結

  - [北条 司個人官方網站](http://www.hojo-tsukasa.com/)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:日本企業家](../Category/日本企業家.md "wikilink")
[Category:北九州市出身人物](../Category/北九州市出身人物.md "wikilink")
[Category:九州產業大學校友](../Category/九州產業大學校友.md "wikilink")
[\*](../Category/北条司.md "wikilink")