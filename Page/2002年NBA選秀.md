[YaoMingonoffense2.jpg](https://zh.wikipedia.org/wiki/File:YaoMingonoffense2.jpg "fig:YaoMingonoffense2.jpg")
**2002年[NBA选秀](../Page/NBA选秀.md "wikilink")**于2002年6月26日在纽约市[麦迪逊广场花园举行](../Page/麦迪逊广场花园.md "wikilink")。

这一结束后[阿瑪爾·斯塔德邁爾被选为这一年的NBA最佳新秀](../Page/阿瑪爾·斯塔德邁爾.md "wikilink")。

## 圖例

|          |      |      |    |     |     |
| -------- | ---- | ---- | -- | --- | --- |
| **场上位置** | PG   | SG   | C  | PF  | SF  |
| 位置       | 控球后卫 | 得分后卫 | 中锋 | 大前锋 | 小前锋 |

## 第一轮

<table>
<colgroup>
<col style="width: -5%" />
<col style="width: 25%" />
<col style="width: 20%" />
<col style="width: 35%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>顺位</p></th>
<th><p>球员</p></th>
<th><p>国籍</p></th>
<th><p>所属球队</p></th>
<th><p>学校／前俱乐部</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/姚明.md" title="wikilink">姚明</a> (C)</p></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a></p></td>
<td><p><a href="../Page/上海大鯊魚籃球俱樂部.md" title="wikilink">上海大鯊魚籃球俱樂部</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/傑伊·威廉斯.md" title="wikilink">傑伊·威廉斯</a>（PG）</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/杜克大学.md" title="wikilink">杜克大学</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/迈克·邓利维.md" title="wikilink">迈克·邓利维</a> (SF)</p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p><a href="../Page/杜克大学.md" title="wikilink">杜克大学</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/德鲁·古登.md" title="wikilink">德鲁·古登</a> （PF）</p></td>
<td></td>
<td><p><a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a></p></td>
<td><p><a href="../Page/堪薩斯大学.md" title="wikilink">堪薩斯大学</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/尼科洛兹·茨基蒂什维利.md" title="wikilink">尼科洛兹·茨基蒂什维利</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a></p></td>
<td><p>特維索貝納通（義大利聯賽）</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>德胡安·華格納（PG）</p></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p>孟菲斯大學</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/內內.md" title="wikilink">內內</a> (PF)</p></td>
<td></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p>華斯高（巴西聯賽）</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/克里斯·威爾考克斯.md" title="wikilink">克里斯·威爾考克斯</a> (C)</p></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
<td><p><a href="../Page/馬里蘭大學.md" title="wikilink">馬里蘭大學</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/阿瑪爾·斯塔德邁爾.md" title="wikilink">阿瑪爾·斯塔德邁爾</a> (PF)</p></td>
<td></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td><p>塞普勒斯克里克高中</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/卡隆·巴特勒.md" title="wikilink">卡隆·巴特勒</a>(SF)</p></td>
<td></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></p></td>
<td><p>康乃狄克大學</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>賈瑞德·傑瑞斯(SF)</p></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td><p>印第安納大學</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/梅爾文·伊萊.md" title="wikilink">梅爾文·伊萊</a> (PF)</p></td>
<td></td>
<td><p><a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a></p></td>
<td><p><a href="../Page/加利福尼亞州立大學.md" title="wikilink">加利福尼亞州立大學</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>馬可斯·西斯利普(SF)</p></td>
<td></td>
<td><p><a href="../Page/密爾沃基公鹿.md" title="wikilink">密爾沃基公鹿</a></p></td>
<td><p>田納西大學</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/弗雷德·瓊斯.md" title="wikilink">弗雷德·瓊斯</a> (SG)</p></td>
<td></td>
<td><p><a href="../Page/印第安納遛馬.md" title="wikilink">印第安納遛馬</a></p></td>
<td><p>奧倫治大學</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>波斯簡·纳克巴(SF)</p></td>
<td></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a></p></td>
<td><p>特維索貝納通（義大利聯賽）</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>伊日·维爾斯科 (SG)</p></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td><p>Union Olimpija</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>胡安·迪臣 (PG)</p></td>
<td></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td><p>馬里蘭大學</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>柯蒂斯·伯夏特(C)</p></td>
<td></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></p></td>
<td><p><a href="../Page/史丹佛大學.md" title="wikilink">史丹佛大學</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>萊恩·亨弗萊 (PF)</p></td>
<td></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td><p><a href="../Page/聖母院書院.md" title="wikilink">聖母院書院</a></p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>卡里姆·洛許(SG)</p></td>
<td></td>
<td><p><a href="../Page/多倫多猛龍.md" title="wikilink">多倫多猛龍</a></p></td>
<td><p><a href="../Page/密蘇里大學.md" title="wikilink">密蘇里大學</a></p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>昆廷‧伍茲 (SF)</p></td>
<td></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a></p></td>
<td><p>NE Mississippi CC</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>凱西‧賈可布森 (SG)</p></td>
<td></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a></p></td>
<td><p><a href="../Page/史丹佛大學.md" title="wikilink">史丹佛大學</a></p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p><a href="../Page/泰夏安·普林斯.md" title="wikilink">泰夏安·普林斯</a> (SF)</p></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td><p><a href="../Page/肯塔基大學.md" title="wikilink">肯塔基大學</a></p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>內納德·克里斯提奇 (C)</p></td>
<td></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a></p></td>
<td><p>貝爾格勒游擊（歐洲聯賽）</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>法蘭克·威廉士 (PG)</p></td>
<td></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a></p></td>
<td><p>　伊利諾州大學</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>約翰·塞蒙斯(SG)</p></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td><p>　邁阿密大學</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>克里斯·傑富瑞 (SF)</p></td>
<td></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p><a href="../Page/加利福尼亞州立大學.md" title="wikilink">加利福尼亞州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>丹·迪考 (PG)</p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td><p>貢薩格大學</p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>史提夫·羅根 (PG)</p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p>辛辛那堤大學</p></td>
</tr>
</tbody>
</table>

## 第二轮

<table>
<colgroup>
<col style="width: -5%" />
<col style="width: 25%" />
<col style="width: 20%" />
<col style="width: 35%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>顺位</p></th>
<th><p>球员</p></th>
<th><p>国籍</p></th>
<th><p>所属球队</p></th>
<th><p>学校／前俱乐部</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>30</p></td>
<td><p>洛傑·梅森 (SG)</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/維吉尼亞聯邦大學.md" title="wikilink">維吉尼亞聯邦大學</a></p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p>羅伯特·阿契伯德 (PF)</p></td>
<td></td>
<td><p><a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a></p></td>
<td><p>伊利諾州大學</p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p>文森特·加丹尼亞 (SF)</p></td>
<td></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a></p></td>
<td><p>田納西大學</p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td><p>丹·加祖里奇 (C)</p></td>
<td></td>
<td><p><a href="../Page/密爾沃基公鹿.md" title="wikilink">密爾沃基公鹿</a></p></td>
<td><p><a href="../Page/加州大學洛杉磯分校.md" title="wikilink">加州大學洛杉磯分校</a></p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td><p><a href="../Page/卡洛斯·布澤爾.md" title="wikilink">卡洛斯·布澤爾</a> (PF)</p></td>
<td></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a></p></td>
<td><p><a href="../Page/杜克大学.md" title="wikilink">杜克大学</a></p></td>
</tr>
<tr class="even">
<td><p>35</p></td>
<td><p>米洛斯·伏加尼奇 (PG)</p></td>
<td></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a></p></td>
<td><p>貝爾格勒游擊（歐洲聯賽）</p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td><p>大衛·安德森 (C)</p></td>
<td></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p>維魯斯博洛尼亞（義大利聯賽）</p></td>
</tr>
<tr class="even">
<td><p>37</p></td>
<td><p>提托·馬多克斯 (PG)</p></td>
<td></td>
<td><p><a href="../Page/休斯頓火箭.md" title="wikilink">休斯頓火箭</a></p></td>
<td><p><a href="../Page/加利福尼亞州立大學.md" title="wikilink">加利福尼亞州立大學</a></p></td>
</tr>
<tr class="odd">
<td><p>38</p></td>
<td><p><a href="../Page/Tito_Maddox.md" title="wikilink">Tito Maddox</a>（PG）</p></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a><small>（来自<a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a>）</small></p></td>
<td><p><a href="../Page/加州州立大学弗雷斯诺分校.md" title="wikilink">加州州立大学弗雷斯诺分校</a>（二年级）</p></td>
</tr>
<tr class="even">
<td><p>39</p></td>
<td><p><a href="../Page/Rod_Grizzard.md" title="wikilink">Rod Grizzard</a>（SG）</p></td>
<td></td>
<td><p><a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a><small>（来自<a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a>，经手<a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a>)</small></p></td>
<td><p><a href="../Page/阿拉巴马大学.md" title="wikilink">阿拉巴马大学</a>（三年级）</p></td>
</tr>
<tr class="odd">
<td><p>40</p></td>
<td><p><a href="../Page/Juan_Carlos_Navarro_(basketball).md" title="wikilink">Juan Carlos Navarro (basketball)</a>（PG）</p></td>
<td></td>
<td><p><a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a></p></td>
<td><p><a href="../Page/巴塞罗那篮球俱乐部.md" title="wikilink">FC Barcelona</a><small>(<a href="../Page/西班牙.md" title="wikilink">西班牙</a>)</small> 1980</p></td>
</tr>
<tr class="even">
<td><p>41</p></td>
<td><p><a href="../Page/马里奥·卡苏.md" title="wikilink">马里奥·卡苏</a>（C）</p></td>
<td></td>
<td><p><a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a></p></td>
<td><p><a href="../Page/Deutsche_Bank_Skyliners.md" title="wikilink">Opel Skyliners</a><small>(<a href="../Page/德国.md" title="wikilink">德国</a>)</small> 1980</p></td>
</tr>
<tr class="odd">
<td><p>42</p></td>
<td><p><a href="../Page/羅納德·穆雷.md" title="wikilink">羅納德·穆雷</a> (SG)</p></td>
<td></td>
<td><p><a href="../Page/密爾沃基公鹿.md" title="wikilink">密爾沃基公鹿</a></p></td>
<td><p>Shaw University</p></td>
</tr>
<tr class="even">
<td><p>43</p></td>
<td><p><a href="../Page/Jason_Jennings_(basketball_player).md" title="wikilink">Jason Jennings (basketball player)</a>（C）</p></td>
<td></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a><small>（来自<a href="../Page/多伦多猛龙.md" title="wikilink">多伦多猛龙</a>，经手<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>）</small></p></td>
<td><p><a href="../Page/阿肯色州立大学.md" title="wikilink">阿肯色州立大学</a>（四年级）</p></td>
</tr>
<tr class="odd">
<td><p>44</p></td>
<td><p><a href="../Page/罗尼·巴克斯特.md" title="wikilink">罗尼·巴克斯特</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><small>（来自<a href="../Page/印第安纳步行者.md" title="wikilink">印第安纳步行者</a>）</small></p></td>
<td><p><a href="../Page/马里兰大学.md" title="wikilink">马里兰大学</a>（四年级）</p></td>
</tr>
<tr class="even">
<td><p>45</p></td>
<td><p><a href="../Page/Sam_Clancy,_Jr..md" title="wikilink">Sam Clancy, Jr.</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/费城76人.md" title="wikilink">费城76人</a></p></td>
<td><p><a href="../Page/南加州大学.md" title="wikilink">南加州大学</a>（四年级）</p></td>
</tr>
<tr class="odd">
<td><p>46</p></td>
<td><p><a href="../Page/马特·巴恩斯.md" title="wikilink">马特·巴恩斯</a>（SF）</p></td>
<td></td>
<td><p><a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a><small>（来自<a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a>）</small></p></td>
<td><p><a href="../Page/加州大学洛杉矶分校.md" title="wikilink">加州大学洛杉矶分校</a>（四年级）</p></td>
</tr>
<tr class="even">
<td><p>47</p></td>
<td><p><a href="../Page/Jamal_Sampson.md" title="wikilink">Jamal Sampson</a>（C）</p></td>
<td></td>
<td><p><a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a><small>（交易到<a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a>）</small></p></td>
<td><p><a href="../Page/加州大学伯克利分校.md" title="wikilink">加州大学伯克利分校</a>（一年级）</p></td>
</tr>
<tr class="odd">
<td><p>48</p></td>
<td><p><a href="../Page/Chris_Owens_(basketball_player).md" title="wikilink">Chris Owens (basketball player)</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/密尔沃基雄鹿.md" title="wikilink">密尔沃基雄鹿</a><small>（来自<a href="../Page/新奥尔良鹈鹕.md" title="wikilink">新奥尔良黄蜂</a>，交易到<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>）</small></p></td>
<td><p><a href="../Page/得克萨斯大学.md" title="wikilink">得克萨斯大学</a>（四年级）</p></td>
</tr>
<tr class="even">
<td><p>49</p></td>
<td><p><a href="../Page/Peter_Fehse.md" title="wikilink">Peter Fehse</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/西雅图超音速.md" title="wikilink">西雅图超音速</a></p></td>
<td><p><a href="../Page/SV_Halle.md" title="wikilink">Halle</a><small>（德国）</small> 1983</p></td>
</tr>
<tr class="odd">
<td><p>50</p></td>
<td><p><a href="../Page/Darius_Songaila.md" title="wikilink">Darius Songaila</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
<td><p><a href="../Page/维克弗斯特大学.md" title="wikilink">维克弗斯特大学</a>（四年级）</p></td>
</tr>
<tr class="even">
<td><p>51</p></td>
<td><p><a href="../Page/Federico_Kammerichs.md" title="wikilink">Federico Kammerichs</a>（SF）</p></td>
<td></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a></p></td>
<td><p><a href="../Page/Ourense_Baloncesto.md" title="wikilink">Ourense</a><small>（西班牙）</small> 1980</p></td>
</tr>
<tr class="odd">
<td><p>52</p></td>
<td><p><a href="../Page/Marcus_Taylor.md" title="wikilink">Marcus Taylor</a>（PG）</p></td>
<td></td>
<td><p><a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a></p></td>
<td><p><a href="../Page/密歇根州立大学.md" title="wikilink">密歇根州立大学</a>（二年级）</p></td>
</tr>
<tr class="even">
<td><p>53</p></td>
<td><p><a href="../Page/拉苏尔·巴特勒.md" title="wikilink">拉苏尔·巴特勒</a>（SF）</p></td>
<td></td>
<td><p><a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a><small>（来自<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>，经手<a href="../Page/多伦多猛龙.md" title="wikilink">多伦多猛龙和</a><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a>）</small></p></td>
<td><p><a href="../Page/拉塞尔大学.md" title="wikilink">拉塞尔大学</a>（四年级）</p></td>
</tr>
<tr class="odd">
<td><p>54</p></td>
<td><p><a href="../Page/Tamar_Slay.md" title="wikilink">Tamar Slay</a>（SG）</p></td>
<td></td>
<td><p><a href="../Page/布鲁克林篮网.md" title="wikilink">新泽西篮网</a></p></td>
<td><p><a href="../Page/马歇尔大学.md" title="wikilink">马歇尔大学</a>（四年级）</p></td>
</tr>
<tr class="even">
<td><p>55</p></td>
<td><p><a href="../Page/Mladen_Šekularac.md" title="wikilink">Mladen Šekularac</a>（SG）</p></td>
<td></td>
<td><p><a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a></p></td>
<td><p><a href="../Page/KK_FMP.md" title="wikilink">FMP Železnik</a><small>（<a href="../Page/Sinalco_Superleague.md" title="wikilink">Sinalco Superleague和</a><a href="../Page/NLB_League.md" title="wikilink">Adriatic League</a>）</small> 1981</p></td>
</tr>
<tr class="odd">
<td><p>56</p></td>
<td><p><a href="../Page/路易斯·斯科拉.md" title="wikilink">路易斯·斯科拉</a> (PF)</p></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a></p></td>
<td><p>塔烏陶瓷俱樂部（西班牙聯賽）</p></td>
</tr>
<tr class="even">
<td><p>57</p></td>
<td><p><a href="../Page/Randy_Holcomb.md" title="wikilink">Randy Holcomb</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a><small>（交易到<a href="../Page/费城76人.md" title="wikilink">费城76人</a>）</small></p></td>
<td><p><a href="../Page/圣迭戈州立大学.md" title="wikilink">圣迭戈州立大学</a>（四年级）</p></td>
</tr>
<tr class="odd">
<td><p>58</p></td>
<td><p><a href="../Page/Corsley_Edwards.md" title="wikilink">Corsley Edwards</a>（PF）</p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td><p><a href="../Page/中央康涅狄格州立大学.md" title="wikilink">中央康涅狄格州立大学</a>（四年级）</p></td>
</tr>
</tbody>
</table>

## 参考资料

[Category:NBA选秀](../Category/NBA选秀.md "wikilink")