**绿藻纲**（學名：）在生物分类学上是[绿藻门中的一个纲](../Page/绿藻门.md "wikilink")。

## 分類

，綠藻綱包括下列各個目：

  - 目 [Acrosiphoniales](../Page/Acrosiphoniales.md "wikilink")
  - 目 [Caulerpales](../Page/Caulerpales.md "wikilink")
  - 目 [Chaetopeltidales](../Page/Chaetopeltidales.md "wikilink")
  - [胶毛藻目](../Page/胶毛藻目.md "wikilink") Chaetophorales
  - [衣藻目](../Page/衣藻目.md "wikilink") Chlamydomonadales
  - 目 [Chlorocystidales](../Page/Chlorocystidales.md "wikilink")
  - 目 [Chlorophyceae](../Page/Chlorophyceae.md "wikilink") incertae
    sedis
  - 目 [Codiales](../Page/Codiales.md "wikilink")
  - [杜氏藻目](../Page/杜氏藻目.md "wikilink") Dunaliellales (e.g.
    *[Dunaliella](../Page/Dunaliella.md "wikilink")*)
  - [鞘藻目](../Page/鞘藻目.md "wikilink") Oedogoniales (e.g.
    *[Oedogonium](../Page/Oedogonium.md "wikilink")*)
  - 目 [Phaeophilales](../Page/Phaeophilales.md "wikilink")
  - 目 [Sphaeropleales](../Page/Sphaeropleales.md "wikilink")
  - [四胞藻目](../Page/四胞藻目.md "wikilink") Tetrasporales (e.g.
    *[Tetraspora](../Page/Tetraspora.md "wikilink")*)
  - [团藻目](../Page/团藻目.md "wikilink") Volvocales (e.g.
    *[Volvox](../Page/Volvox.md "wikilink")*,
    *[Chlamydomonas](../Page/Chlamydomonas.md "wikilink")*)

<!-- end list -->

  - 異名

<!-- end list -->

  - [绿球藻目](../Page/绿球藻目.md "wikilink") Chlorococcales
    被認為是[Chlamydomonadales的異名](../Page/Chlamydomonadales.md "wikilink")；
  - 目 [Microsporales](../Page/Microsporales.md "wikilink") accepted as
    Sphaeropleales
  - 目 [Protococcales](../Page/Protococcales.md "wikilink") accepted as
    Volvocales

### 過往成員

以下為過往屬绿藻纲的分類單元：

  - [溪菜目](../Page/溪菜目.md "wikilink")
    Prasiolales：今屬[共球藻綱](../Page/共球藻綱.md "wikilink")
  - [管藻目](../Page/管藻目.md "wikilink") Siphonales
  - [双星藻目](../Page/双星藻目.md "wikilink") Zygnematales

## 參看

  - [支序分類學](../Page/支序分類學.md "wikilink")
  - [共球藻綱](../Page/共球藻綱.md "wikilink")

## 參考文獻

## 外部連結

  - [AlgaeBase](http://www.algaebase.org/browse/taxonomy/?id=4355&-session=abv4:4E5E144B1b45e25996Ihq2483A24)

[绿藻纲](../Category/绿藻纲.md "wikilink")