**昌吉回族自治州**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[新疆维吾尔自治区下辖的](../Page/新疆维吾尔自治区.md "wikilink")[自治州](../Page/自治州.md "wikilink")，位于新疆中部偏北。东与[哈密市相接](../Page/哈密市.md "wikilink")，西连石河子市，南与[吐鲁番市](../Page/吐鲁番市.md "wikilink")、巴音郭楞蒙古自治州接壤，北邻塔城、[阿勒泰地区](../Page/阿勒泰地区.md "wikilink")，东北与蒙古国毗邻。从东西北三面环抱首府乌鲁木齐市。\[1\]

南境有终年积雪的[天山](../Page/天山.md "wikilink")，有广大的森林和牧场；北部[准噶尔盆地草原广阔](../Page/准噶尔盆地.md "wikilink")。昌吉州[汉族人口比例约](../Page/汉族.md "wikilink")75%，[回族人口比例约](../Page/回族.md "wikilink")10%。

## 历史沿革

清[康熙三十六年](../Page/康熙.md "wikilink")（1697年），康熙帝灭噶尔丹部，州境随归清朝。[乾隆二十七年](../Page/乾隆.md "wikilink")（1762年），州境属[伊犁将军管辖](../Page/伊犁将军.md "wikilink")，后乾隆在迪化设镇迪道，辖州境大部，光绪十年（1884年），新疆建省，昌吉州大部归[迪化府](../Page/迪化府.md "wikilink")。
1954年7月8日成立昌吉回族自治区，辖昌吉、乌鲁木齐、米泉三县；1955年3月改为昌吉回族自治州。1959年将[乌鲁木齐县划归乌鲁木齐市](../Page/乌鲁木齐县.md "wikilink")。
2007年，将昌吉回族自治州米泉市并入乌鲁木齐市，撤销米泉市。\[2\]

## 政治

### 现任领导

<table>
<caption>昌吉回族自治州四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党昌吉回族自治州委员会.md" title="wikilink">中国共产党<br />
昌吉回族自治州委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/昌吉回族自治州人民代表大会.md" title="wikilink">昌吉回族自治州人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/昌吉回族自治州人民政府.md" title="wikilink">昌吉回族自治州人民政府</a><br />
<br />
州长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议昌吉回族自治州委员会.md" title="wikilink">中国人民政治协商会议<br />
昌吉回族自治州委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/沙尔合提·阿汗.md" title="wikilink">沙尔合提·阿汗</a>[3]</p></td>
<td><p><a href="../Page/马建新.md" title="wikilink">马建新</a>[4]</p></td>
<td><p><a href="../Page/张承义.md" title="wikilink">张承义</a></p></td>
<td><p><a href="../Page/李铁明.md" title="wikilink">李铁明</a>[5]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/哈萨克族.md" title="wikilink">哈萨克族</a></p></td>
<td><p><a href="../Page/回族.md" title="wikilink">回族</a></p></td>
<td><p>回族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/新疆维吾尔自治区.md" title="wikilink">新疆维吾尔自治区</a><a href="../Page/福海县.md" title="wikilink">福海县</a></p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a><a href="../Page/凤县.md" title="wikilink">凤县</a></p></td>
<td><p>新疆维吾尔自治区<a href="../Page/吉木萨尔县.md" title="wikilink">吉木萨尔县</a></p></td>
<td><p>新疆维吾尔自治区<a href="../Page/呼图壁县.md" title="wikilink">呼图壁县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年3月</p></td>
<td><p>2018年1月</p></td>
<td><p>2018年1月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

下辖2个[县级市](../Page/县级市.md "wikilink")、4个[县](../Page/县_\(中华人民共和国\).md "wikilink")、1个[自治县](../Page/自治县.md "wikilink")。

  - 县级市：[昌吉市](../Page/昌吉市.md "wikilink")、[阜康市](../Page/阜康市.md "wikilink")
  - 县：[呼图壁县](../Page/呼图壁县.md "wikilink")、[玛纳斯县](../Page/玛纳斯县.md "wikilink")、[奇台县](../Page/奇台县.md "wikilink")、[吉木萨尔县](../Page/吉木萨尔县.md "wikilink")
  - 自治县：[木垒哈萨克自治县](../Page/木垒哈萨克自治县.md "wikilink")

除正式行政区划外，昌吉州还设立以下经济功能区，赋予部分县级行政管理权限：[准东经济技术开发区](../Page/准东经济技术开发区.md "wikilink")（[国家级](../Page/国家级经济技术开发区.md "wikilink")）、[昌吉国家高新技术产业开发区](../Page/昌吉国家高新技术产业开发区.md "wikilink")（[国家级](../Page/国家级高新技术产业开发区.md "wikilink")）、昌吉国家农业科技园区。

<table>
<thead>
<tr class="header">
<th><p><strong>昌吉回族自治州行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[6]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>652300</p></td>
</tr>
<tr class="odd">
<td><p>652301</p></td>
</tr>
<tr class="even">
<td><p>652302</p></td>
</tr>
<tr class="odd">
<td><p>652323</p></td>
</tr>
<tr class="even">
<td><p>652324</p></td>
</tr>
<tr class="odd">
<td><p>652325</p></td>
</tr>
<tr class="even">
<td><p>652327</p></td>
</tr>
<tr class="odd">
<td><p>652328</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>昌吉回族自治州各市（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="odd">
<td><p>昌吉回族自治州</p></td>
<td><p>1428587</p></td>
<td><p>100</p></td>
</tr>
<tr class="even">
<td><p>昌吉市</p></td>
<td><p>426253</p></td>
<td><p>29.84</p></td>
</tr>
<tr class="odd">
<td><p>阜康市</p></td>
<td><p>165006</p></td>
<td><p>11.55</p></td>
</tr>
<tr class="even">
<td><p>呼图壁县</p></td>
<td><p>210201</p></td>
<td><p>14.71</p></td>
</tr>
<tr class="odd">
<td><p>玛纳斯县</p></td>
<td><p>237558</p></td>
<td><p>16.63</p></td>
</tr>
<tr class="even">
<td><p>奇台县</p></td>
<td><p>210566</p></td>
<td><p>14.74</p></td>
</tr>
<tr class="odd">
<td><p>吉木萨尔县</p></td>
<td><p>113284</p></td>
<td><p>7.93</p></td>
</tr>
<tr class="even">
<td><p>木垒哈萨克自治县</p></td>
<td><p>65719</p></td>
<td><p>4.60</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全州[常住人口为](../Page/常住人口.md "wikilink")1428587人\[9\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加96658人，增长7.27%。年平均增长率为7.0‰。其中，男性人口为757038人，占52.99%；女性人口为671549人，占47.01%。总人口性别比（以女性为100）为112.73。0－14岁人口为220531人，占15.44%；15－59岁人口为1025654人，占71.79%；60岁及以上人口为182402人，占12.77%，其中65岁及以上人口为127796人，占8.95%。

### 民族

常住人口中，[汉族人口为](../Page/汉族.md "wikilink")1075852人，占75.31%；各[少数民族人口为](../Page/少数民族.md "wikilink")352735人，占24.69%。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [哈萨克族](../Page/哈萨克族.md "wikilink") | [维吾尔族](../Page/维吾尔族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [东乡族](../Page/东乡族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [乌孜別克族](../Page/乌孜別克族.md "wikilink") | [藏族](../Page/藏族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | ---------------------------------- | ---------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | ------------------------------------ | ------------------------------ | -------------------------------- | ---- |
| 人口数          | 1075852                        | 136013                         | 133286                             | 63606                              | 5214                             | 3176                             | 2264                           | 1727                                 | 1502                           | 1171                             | 4776 |
| 占总人口比例（%）    | 75.31                          | 9.52                           | 9.33                               | 4.45                               | 0.36                             | 0.22                             | 0.16                           | 0.12                                 | 0.11                           | 0.08                             | 0.33 |
| 占少数民族人口比例（%） | \---                           | 38.56                          | 37.79                              | 18.03                              | 1.48                             | 0.90                             | 0.64                           | 0.49                                 | 0.43                           | 0.33                             | 1.35 |

**昌吉回族自治州民族构成（2010年11月）**\[10\]

## 参见

  - [庭州](../Page/庭州.md "wikilink")

## 注释

## 参考文献

{{-}}

[昌吉](../Category/昌吉.md "wikilink") [新](../Category/回族自治州.md "wikilink")
[Category:新疆自治州](../Category/新疆自治州.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.