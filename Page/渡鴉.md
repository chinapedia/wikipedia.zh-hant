**渡鴉**（[學名](../Page/學名.md "wikilink")：**）是一種全身[黑色的大型](../Page/黑色.md "wikilink")[雀形目](../Page/雀形目.md "wikilink")[鴉屬](../Page/鴉屬.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，俗称为胖头鸟。分佈於[北半球](../Page/北半球.md "wikilink")，是所有[鴉科中分佈最廣泛的一類](../Page/鴉科.md "wikilink")。目前已知有八個[亞種](../Page/亞種.md "wikilink")，不同亞種之間在外表上只有些許不同，但在[遺傳學上差異則較明顯](../Page/遺傳學.md "wikilink")。

渡鴉是最大的兩種[烏鴉之一](../Page/烏鴉.md "wikilink")，可能與[厚嘴渡鴉](../Page/厚嘴渡鴉.md "wikilink")（*C.
crassirostris*）同是雀形目中最重的鳥類。成熟的渡鴉長約56-69厘米，最重紀錄為0.69到1.63公斤。野外渡鴉一般有10到15年的壽命，但曾有達40歲的紀錄。幼鳥成群活動，之後則與伴侶共同生活，每對伴侶皆有各自的領域。

渡鴉與[人類共存了超過一千年](../Page/人類.md "wikilink")，在某些地區的繁殖頗為成功，甚至被認為是瘟疫。牠們繁殖的速度之快在於其[雜食的特性](../Page/雜食性.md "wikilink")
。渡鴉在覓食方面相當靈敏且投機。牠們的主要糧食是[腐肉](../Page/腐肉.md "wikilink")、[昆蟲](../Page/昆蟲.md "wikilink")、食物殘渣、[穀物](../Page/穀物.md "wikilink")、[漿果](../Page/漿果.md "wikilink")、水果及小型[動物](../Page/動物.md "wikilink")。

渡鴉有一些著名的問題解決技巧，居住在人類社區的渡鴉能聰明的利用各種資源，眷養情況下亦可理解人類的指令，因此被認為有著高度[智力](../Page/智力.md "wikilink")。多個[世紀以來](../Page/世紀.md "wikilink")，牠們出現在[神話](../Page/神話.md "wikilink")、[民間故事](../Page/民間故事.md "wikilink")、[藝術及](../Page/藝術.md "wikilink")[文學中](../Page/文學.md "wikilink")。渡鴉在一些文化裡被尊崇為[神或神明](../Page/神.md "wikilink")，包括[斯堪的納維亞半島](../Page/斯堪的納維亞半島.md "wikilink")、古[愛爾蘭與](../Page/愛爾蘭.md "wikilink")[威爾斯](../Page/威爾斯.md "wikilink")、[不丹及](../Page/不丹.md "wikilink")[北美洲西北岸](../Page/北美洲.md "wikilink")\[1\]。

## 分類學

渡鴉是眾多由[卡爾·林奈於](../Page/卡爾·林奈.md "wikilink")18世紀所描述的[物種之一](../Page/物種.md "wikilink")。\[2\]
牠是[鴉屬的](../Page/鴉屬.md "wikilink")[模式種](../Page/模式種.md "wikilink")，鴉屬的[學名](../Page/學名.md "wikilink")（*Corvus*）是源於[拉丁文](../Page/拉丁文.md "wikilink")「渡鴉」的意思。\[3\]
而牠的[種小名](../Page/種小名.md "wikilink")*corax*，是來自[古希臘文的](../Page/古希臘文.md "wikilink")「」，意思亦是「渡鴉」或「[烏鴉](../Page/烏鴉.md "wikilink")」。\[4\]「渡鴉」這個詞，也用來稱呼鴉屬內的其他物種，但牠們往往未必與渡鴉有密切關聯。而有些如[澳洲渡鴉與](../Page/澳洲渡鴉.md "wikilink")[林渡鴉](../Page/林渡鴉.md "wikilink")，就明顯與其他[澳洲的渡鴉有著關聯](../Page/澳洲.md "wikilink")。\[5\]
現在，原來的渡鴉被稱為普通渡鴉或北方渡鴉。\[6\]

渡鴉的[英文是](../Page/英文.md "wikilink")「raven」，與很多古[日耳曼語都相似](../Page/日耳曼語.md "wikilink")；[古英語的渡鴉是](../Page/古英語.md "wikilink")「」，[古諾爾斯語及](../Page/古諾爾斯語.md "wikilink")[古高地德語分別是](../Page/古高地德語.md "wikilink")「」及「*(h)raban*」\[7\]，所有這些詞都是從[原始日耳曼語的](../Page/原始日耳曼語.md "wikilink")「*\*khrabanas*」而來。\[8\]
一種古[蘇格蘭語就以](../Page/蘇格蘭語.md "wikilink")「*corby*」或「*corbie*」，[法文有類似的](../Page/法文.md "wikilink")「*corbeau*」，來表示渡鴉及[小嘴烏鴉](../Page/小嘴烏鴉.md "wikilink")。\[9\]

### 分類

渡鴉的近親是[非洲的](../Page/非洲.md "wikilink")[褐頸渡鴉](../Page/褐頸渡鴉.md "wikilink")（*C.
ruficollis*）及[染色鴉](../Page/染色鴉.md "wikilink")（*C.
albus*），及[北美洲西南部的](../Page/北美洲.md "wikilink")[白頸渡鴉](../Page/白頸渡鴉.md "wikilink")（*C.
cryptoleucus*）。\[10\] 現時已知渡鴉共有八個[亞種](../Page/亞種.md "wikilink")：

  - *C. c.
    corax*：指名亞種，出沒於[歐洲](../Page/歐洲.md "wikilink")，東臨[貝加爾湖](../Page/貝加爾湖.md "wikilink")，南部達[高加索地區及](../Page/高加索.md "wikilink")[伊朗北部](../Page/伊朗.md "wikilink")。牠的喙較短及彎曲。
  - *C. c.
    varius*：出沒於[冰島及](../Page/冰島.md "wikilink")[法羅群島](../Page/法羅群島.md "wikilink")。牠比*C.
    c. corax*及*C. c.
    principalis*色澤較暗，為中型大小，頸部[羽毛的底部呈白色](../Page/羽毛.md "wikilink")。

[Krummi_1.jpg](https://zh.wikipedia.org/wiki/File:Krummi_1.jpg "fig:Krummi_1.jpg")[塞爾蒂亞納上空飛行的渡鴉](../Page/塞爾蒂亞納.md "wikilink")。\]\]

  - *C. c.
    subcorax*：出沒於[希臘](../Page/希臘.md "wikilink")，東臨[印度西北部](../Page/印度.md "wikilink")、[中亞及](../Page/中亞.md "wikilink")[中國西部](../Page/中國.md "wikilink")（但不包括[喜瑪拉雅山區](../Page/喜瑪拉雅山.md "wikilink")）。牠比指名亞種較大，但頸羽則較短。牠的羽毛接近全黑，唯頸部及胸部的羽毛呈像褐頸渡鴉的褐色，當羽毛磨損時這會更為顯注。雖然牠們的頸羽底部會有些變化，但一般都是白色的。
  - *C. c.
    tingitanus*：出沒於[北非及](../Page/北非.md "wikilink")[加那利群島](../Page/加那利群島.md "wikilink")。這是最小的亞種，頸羽最短及全身羽毛都有油性光澤。牠們的喙很短，但明顯的堅硬及彎曲。當磨損時，頭部及身體羽毛會呈現出深褐色。
  - [青藏渡鴉](../Page/青藏渡鴉.md "wikilink") *C. c.
    tibetanus*：出沒於喜瑪拉雅山。牠們是最大型及最有光澤的亞種，而頸羽亦是最長的。牠的喙很大，但不及*C.
    c. principalis*的喙顯眼，而頸羽的底部呈灰色。
  - [東北渡鴉](../Page/東北渡鴉.md "wikilink") *C. c.
    kamtschaticus*：出沒於[亞洲東北部](../Page/亞洲.md "wikilink")，與指名亞種一同生活於貝加爾湖地區。牠是中等體型，介乎於*C.
    c. principalis*及*C. c. corax*之間，比指名亞種有更大及更厚的喙。
  - *C. c.
    principalis*：出沒於[北美洲北部及](../Page/北美洲.md "wikilink")[格陵蘭](../Page/格陵蘭.md "wikilink")。牠們有著很大的體型及最大的喙，羽毛有強烈的光澤，頸羽則很豐滿。
  - *C. c.
    sinuatus*：出沒於[美國的中南部及](../Page/美國.md "wikilink")[中美洲](../Page/中美洲.md "wikilink")。牠們的體型細小，喙比*C.
    c. principalis*較細及窄。

### 演化歷史

渡鴉在[舊大陸中](../Page/舊大陸.md "wikilink")[演化](../Page/演化.md "wikilink")，並橫跨[白令陸橋前往](../Page/白令陸橋.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。\[11\]
最近關於全世界渡鴉[DNA的](../Page/DNA.md "wikilink")[遺傳學研究顯示](../Page/遺傳學.md "wikilink")，這些鳥類至少可分為兩個分支：一個是出沒於[美國西南部的](../Page/美國.md "wikilink")[加利福尼亞分支](../Page/加利福尼亞.md "wikilink")；另一個則是其他[北半球地區的](../Page/北半球.md "wikilink")[全北區分支](../Page/全北區.md "wikilink")。兩分支的外表看來相似，但遺傳學上的差異顯示，牠們在約二百萬年前就已經出現分歧。\[12\]

這個研究結果顯示美國其餘地區的渡鴉與在[歐洲及](../Page/歐洲.md "wikilink")[亞洲的渡鴉是近親](../Page/亞洲.md "wikilink")，較加利福尼亞州分支更為接近，而加利福尼亞州分支就較全北區分支與[白頸渡鴉](../Page/白頸渡鴉.md "wikilink")（*C.
cryptoleucus*）更為接近。\[13\]
全北區分支的渡鴉則較加利福尼亞州分支與[染色鴉接近](../Page/染色鴉.md "wikilink")。\[14\]
故此，渡鴉被認為是[併系群](../Page/併系群.md "wikilink")。\[15\]

這個發現的解釋是渡鴉在加利福尼亞州已住上了最少二百萬年，並在[冰河時期與牠在歐洲及](../Page/冰河時期.md "wikilink")[亞洲的親屬分開](../Page/亞洲.md "wikilink")。一百萬年前，加利福尼亞州分支的一類演化成新的[物種](../Page/物種.md "wikilink")：白頸渡鴉。其他全北區分支的成員則是後期（可能與[人類同期](../Page/人類.md "wikilink")）從亞洲遷徙而至的。\[16\]

另一項關於渡鴉[線粒體DNA的研究顯示](../Page/線粒體DNA.md "wikilink")，*C. c.
tingitanus*這個[亞種與其他全北區分支有著遺傳上的差異](../Page/亞種.md "wikilink")。此亞種只在[北非及](../Page/北非.md "wikilink")[加那利群島出沒](../Page/加那利群島.md "wikilink")。研究亦顯示*C.
c. tingitanus*不會與其他亞種混種繁殖。\[17\]

## 特征

成熟的渡鴉約56-69厘米長，雙翼展開長115-130厘米。紀錄中的體重為0.69-1.63公斤\[18\]，是[雀形目中最重的](../Page/雀形目.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。在較冷的地區，如[喜瑪拉雅山及](../Page/喜瑪拉雅山.md "wikilink")[格陵蘭](../Page/格陵蘭.md "wikilink")，渡鴉的體型的喙都是較大的；而在暖和的地區的渡鴉體型及喙的比例都較小。\[19\]
渡鴉的喙很大及輕微彎曲，尾巴明顯分層，[羽毛大部分都是黑色有光澤](../Page/羽毛.md "wikilink")，[虹膜深褐色](../Page/虹膜.md "wikilink")。頸羽是長及尖的，而底脚多是淡褐灰色。幼鳥的羽毛相似但較沉，而虹膜是藍灰色的。\[20\]

渡鴉與其親屬[烏鴉有所不同](../Page/烏鴉.md "wikilink")，除了更為龐大的體型外，渡鴉有較大及重的喙、既長且蓬鬆毛的喉部羽毛、楔形的尾羽等，\[21\]
渡鴉還有著獨特的深沉叫聲，資深的聆聽者可以聽出與其他[鴉屬的分別](../Page/鴉屬.md "wikilink")。牠們的詞彙亦很廣泛，包括有高的敲打聲、刺耳的聲音、低沉的嘎嘎聲及接近[音樂的響聲](../Page/音樂.md "wikilink")。\[22\]

渡鴉可以很長壽的，尤其是在籠子中或受保護的環境下。在[倫敦塔中就有多於](../Page/倫敦塔.md "wikilink")40歲的渡鴉。\[23\]
在野外的壽命則較短，一般只有10-15年的壽命。被[繫放的渡鴉最長壽命有](../Page/繫放.md "wikilink")13歲。\[24\]

## 分佈及棲息地

[Corvus_corax_jouveniles.jpeg](https://zh.wikipedia.org/wiki/File:Corvus_corax_jouveniles.jpeg "fig:Corvus_corax_jouveniles.jpeg")上的兩隻幼渡鴉\]\]
渡鴉可以在不同的氣候下生存，而事實上牠們是[鴉屬中分佈最廣的](../Page/鴉屬.md "wikilink")[物種](../Page/物種.md "wikilink")。\[25\]\[26\]
牠們分佈在整個[全北區](../Page/全北區.md "wikilink")，由[北美洲的](../Page/北美洲.md "wikilink")[北極與](../Page/北極.md "wikilink")[溫帶棲息地及](../Page/溫帶.md "wikilink")[歐亞大陸](../Page/歐亞大陸.md "wikilink")，至[北非的](../Page/北非.md "wikilink")[沙漠及](../Page/沙漠.md "wikilink")[太平洋的島嶼](../Page/太平洋.md "wikilink")。在[不列顛群島](../Page/不列顛群島.md "wikilink")，牠們普遍在[蘇格蘭](../Page/蘇格蘭.md "wikilink")、[英格蘭北部及](../Page/英格蘭.md "wikilink")[愛爾蘭西部生活](../Page/愛爾蘭.md "wikilink")。\[27\]
在[西藏](../Page/西藏.md "wikilink")，牠們可以生活在海拔5,000米，及在高達6,350米的[珠穆朗瑪峰上](../Page/珠穆朗瑪峰.md "wikilink")。\[28\]

除了在北極的棲息地\[29\]，牠們一般都會全年留在其地方生活。幼鴉則可能會在局部分散。\[30\]

在[法羅群島](../Page/法羅群島.md "wikilink")，曾存在著一種已滅絕的渡鴉色彩形態，稱為[染色渡鴉](../Page/染色渡鴉.md "wikilink")。\[31\]

大部分渡鴉喜歡有著一大片草原的樹林地區，或是海岸區域，供牠們築巢及攝食。在一些人口稠密的地區，如[美國的](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")，牠們會利用豐富食物供應的優勢，來繁殖大量的下一代。\[32\]

## 行為

渡鴉經常是成雙成對的，而有些年輕的渡鴉會組成群族。在渡鴉之間會經常出現爭吵的情況，但牠們都是熱愛家庭的。\[33\]

### 飲食

[Common_raves_landfill.jpg](https://zh.wikipedia.org/wiki/File:Common_raves_landfill.jpg "fig:Common_raves_landfill.jpg")
渡鴉是[雜食性的及高度機會主義](../Page/雜食性.md "wikilink")。牠們的飲食會就不同地區、季節及收穫所變更。\[34\]
例如，那些在[阿拉斯加](../Page/阿拉斯加.md "wikilink")[北灣](../Page/北灣_\(安大略省\).md "wikilink")[凍土層覓食的渡鴉會透過捕獵來補充牠們一半所需的](../Page/凍土層.md "wikilink")[能量](../Page/能量.md "wikilink")，主要是[田鼠](../Page/田鼠.md "wikilink")，而另一半則會是吃如[馴鹿及](../Page/馴鹿.md "wikilink")[岩雷鳥的腐肉來補充](../Page/岩雷鳥.md "wikilink")。\[35\]

在一些地方，牠們主要是吃腐肉及連帶的[蛆及](../Page/蛆.md "wikilink")[埋葬蟲科](../Page/埋葬蟲科.md "wikilink")。\[36\][植物包括](../Page/植物.md "wikilink")[穀物](../Page/穀物.md "wikilink")、[草莓及](../Page/草莓.md "wikilink")[水果都會是牠們的食糧](../Page/水果.md "wikilink")。牠們會捕獵[無脊椎動物](../Page/無脊椎動物.md "wikilink")、[兩棲動物](../Page/兩棲動物.md "wikilink")、[爬行動物](../Page/爬行動物.md "wikilink")、細小的[哺乳動物及](../Page/哺乳動物.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。\[37\]
渡鴉亦會吃動物[糞便內未經](../Page/糞便.md "wikilink")[消化的部分](../Page/消化.md "wikilink")，及[人類的食物殘渣](../Page/人類.md "wikilink")。牠們會儲存起過多的食物，尤其是包含[脂肪的食物](../Page/脂肪.md "wikilink")，並且會收藏在其他渡鴉看不到的地方。\[38\]
牠們會搶走其他動物，例如[北極狐的獵物](../Page/北極狐.md "wikilink")。\[39\]
牠們亦會在[冬天跟蹤其他](../Page/冬天.md "wikilink")[犬科](../Page/犬科.md "wikilink")，例如[狼](../Page/狼.md "wikilink")，去偷竊其腐肉。\[40\]

在接近人類食物殘渣的地方築巢的渡鴉，其飲食會有較多食物殘渣；在路邊築巢的渡鴉會吃被車輛殺死的脊椎動物，而在偏遠地區築巢的則會吃較多[節肢動物及植物](../Page/節肢動物.md "wikilink")。在以人類垃圾為食物來源的渡鴉會較易餵養。\[41\]
相反，於1984年至1986年有關渡鴉的研究中，發現在[愛德荷州西南部的渡鴉以穀物為其主要食糧](../Page/愛德荷州.md "wikilink")，還有細小的哺乳動物、[草蜢](../Page/草蜢.md "wikilink")、[牛的腐肉及鳥類](../Page/牛.md "wikilink")。\[42\]

幼鳥其中一種行為是招攬，主導的渡鴉會以一連串的叫聲來呼喚其他渡鴉到豐富食物的地方，如腐肉。有指這種行為可以幫助幼鳥凌駕在成年渡鴉之上，避免牠們在吃腐肉期間被驅趕。\[43\]
另一個解釋指牠們是互相合作來分享大型腐肉的資料，因為這些腐肉對於數隻渡鴉來說是太多。\[44\]

### 繁殖

[Ravens_nest_Lastef.jpg](https://zh.wikipedia.org/wiki/File:Ravens_nest_Lastef.jpg "fig:Ravens_nest_Lastef.jpg")
[Corvus_corax_tingitanus_MHNT_232_HdB_Djebel_Messaad_Algerie.jpg](https://zh.wikipedia.org/wiki/File:Corvus_corax_tingitanus_MHNT_232_HdB_Djebel_Messaad_Algerie.jpg "fig:Corvus_corax_tingitanus_MHNT_232_HdB_Djebel_Messaad_Algerie.jpg")
渡鴉很小時就會開始成家立室，但多在兩至三年內都維持獨居。牠們吸引異性的方法是表演一些空中雜技、表現[智慧及提供食物的能力等](../Page/智慧.md "wikilink")。牠們一旦結合後，就會開始築巢，並多會在同一地點終老。\[45\]
渡鴉中亦有出現偷情的情況，雄性渡鴉會在妻子離開後造訪雌性的巢。\[46\]

渡鴉會在築巢及生育前建立自己的疆域，並會主動保衛其疆界及食物資源。疆域的大小視乎食物資源的密度來決定。\[47\]
巢是深碗狀，並由樹枝及細枝組成的，內層是樹根、泥土及樹皮，並且以軟質物料，如[鹿毛皮所包圍](../Page/鹿.md "wikilink")。巢一般會在大樹上或懸崖，或有時在舊建築物或柱子上。\[48\]

雌性每次會生下3-7顆青綠色及褪色斑點的蛋。\[49\]
孵化期約為18-21日，只由雌性所孵。雄性會站立保護幼鳥，而不會實際孵化牠們。\[50\]
幼鳥的哺育約35-42日，並由雙親所餵養。牠們會與雙親生活達6個月。\[51\]

在大部分渡鴉分佈的地區，牠們會在2月初生蛋。在較寒冷的氣候中則會較遲，如在[格陵蘭及](../Page/格陵蘭.md "wikilink")[西藏會在](../Page/西藏.md "wikilink")4月。在[巴基斯坦的渡鴉則會在](../Page/巴基斯坦.md "wikilink")12月生蛋。\[52\]

### 發聲

[Raven_croak.jpg](https://zh.wikipedia.org/wiki/File:Raven_croak.jpg "fig:Raven_croak.jpg")的說話。\]\]
就像其他的[鴉科](../Page/鴉科.md "wikilink")，渡鴉可以模仿環境的[聲音](../Page/聲音.md "wikilink")，包括[人類的說話](../Page/人類.md "wikilink")。牠們的發音很廣闊，而這一直也是[鳥類學家的研究興趣](../Page/鳥類學家.md "wikilink")。於1960年代初，有研究替渡鴉錄音及拍照。\[53\]
共有15-30類的渡鴉發音被錄下來，大部分都是用作社交的。這些叫聲包括有警報、追蹤及打鬥的叫聲。非語音的聲音包括有翼的嘯音及喙的咬聲。雌性比雄性會有更多的拍打聲及卡嗒聲。如果失去了伴侶，另一方會模仿伴侶的聲音來呼喚對方。\[54\]

### 智慧

渡鴉在[鳥類中是屬於](../Page/鳥類.md "wikilink")[腦部最大的一類](../Page/腦部.md "wikilink")。就像其他的[鴉科](../Page/鴉科.md "wikilink")，牠們被指有一些顯著解決問題的事蹟，令人相信鳥類也有高等的[智慧](../Page/智慧.md "wikilink")。但是，[科學家卻就牠們如模仿及洞察力等的](../Page/科學家.md "wikilink")[認知過程並不確定](../Page/認知.md "wikilink")。\[55\]

有一個實驗曾設計來評估渡鴉的洞察力及解決問題的能力。這個實驗是將一塊肉用繩子掛在高台上，如果渡鴉為了要吃那塊肉，就必須要站在高台上，拉高繩子一段短的時間，並繞圈子縮短繩子。五隻渡鴉中有四隻最終成功，且在不成功與可靠行徑之間，並沒有任何明顯的[試錯學習](../Page/試錯.md "wikilink")。\[56\]

亦有觀察渡鴉利用其他[動物來協助處理事件](../Page/動物.md "wikilink")，如呼叫[狼及](../Page/狼.md "wikilink")[野狗等前往動物死屍的地方](../Page/野狗.md "wikilink")。這些[犬科替其撕開腐肉](../Page/犬科.md "wikilink")，協助牠們更易於食用。\[57\]
牠們會觀察其他渡鴉收藏食物，牢記這些收藏的地點，並從而偷取食物。這類盜賊的行徑在渡鴉中很普遍，故牠們會從食物來源飛往更遠的地方來收藏食物。\[58\]
牠們亦被觀察到會假裝收藏食物，以混淆追蹤者。\[59\]

渡鴉偷取且收藏如光滑的小圓石、金屬及[高爾夫球等亮閃閃物體的習性廣為人知](../Page/高爾夫球.md "wikilink")。對於此行為其中一種理論是為了吸引其他渡鴉。\[60\]
其他研究則指出幼崽對所有新事物都很好奇，而成鳥則保留了被光亮及圓形的物體所吸引，因這些物體很像蛋使牠們有收集這些物品的嗜好。成熟的渡鴉在不尋常的情況下會失去其興趣，並會新事物產生恐懼。\[61\]

近年[生物學家開始識別哪些](../Page/生物學家.md "wikilink")[鳥種會進行遊戲](../Page/鳥類.md "wikilink")。幼年的渡鴉是最愛玩耍的鳥類之一。生物學家觀察到牠們會從雪堤滑下，似乎完全只是為了好玩。牠們甚至會跟其他[物種玩](../Page/物種.md "wikilink")，例如與狼或[狗玩捉迷藏](../Page/狗.md "wikilink")，亦有觀察到類似為了玩樂而偷拔其他動物尾巴的毛的現象(tail
pulling)。\[62\] 渡鴉亦會作出特技表演，如翻圈飛行（翻跟斗飛）。\[63\]

## 與人類的關係

### 保育管理

渡鴉分佈很廣，且沒有[滅絕的危險](../Page/滅絕.md "wikilink")。在某一些地區，牠們的數量因失去了棲息地及被直接迫害而有所減少；但在另外一些地區，牠們的數量卻有大幅度的增長，並成為了[害獸](../Page/害獸.md "wikilink")。渡鴉可以對農作物，如[堅果及](../Page/堅果.md "wikilink")[穀物造成破壞](../Page/穀物.md "wikilink")，或是傷害[家畜](../Page/家畜.md "wikilink")，如啄食新生[羊或](../Page/羊.md "wikilink")[牛的](../Page/牛.md "wikilink")[眼睛等](../Page/眼睛.md "wikilink")。\[64\]

在[莫哈維沙漠西部](../Page/莫哈維沙漠.md "wikilink")，[人類的定居及土地發展在](../Page/人類.md "wikilink")25年間引致渡鴉的數目估計有16倍的增幅。城鎮、填海、污水處理及人工湖開創了食物及水源。渡鴉亦在柱子上及裝飾樹上築巢，並被高速公路上的屍體所吸引。渡鴉在莫哈維沙漠急劇的增加，引起了對[瀕危的](../Page/瀕危.md "wikilink")[沙漠地鼠龜的威脅](../Page/沙漠地鼠龜.md "wikilink")。渡鴉會捕獵只有軟殼及行動緩慢的幼龜。\[65\]
控制渡鴉的計劃，包括有射殺及捕捉渡鴉、減少在填海中暴露的垃圾。\[66\]
在[芬蘭自](../Page/芬蘭.md "wikilink")18世紀中至1923年都會獎勵獵殺渡鴉。\[67\]
在[阿拉斯加亦有出現排擠的情況](../Page/阿拉斯加.md "wikilink")，渡鴉數目的增長危害了[小絨鴨](../Page/小絨鴨.md "wikilink")（*Polysticta
stelleri*）的數量。\[68\]

### 文化

[Bill_Reid_raven.jpg](https://zh.wikipedia.org/wiki/File:Bill_Reid_raven.jpg "fig:Bill_Reid_raven.jpg")。\]\]
在[北半球的地區](../Page/北半球.md "wikilink")，及[人類的歷史](../Page/人類.md "wikilink")，渡鴉都是力量的標記及在[神話和](../Page/神話.md "wikilink")[民俗學的主題](../Page/民俗學.md "wikilink")。牠一直被認為是不詳的預兆及與[死亡掛鉤](../Page/死亡.md "wikilink")。由於渡鴉會吃因戰爭及處刑被殺的人類屍體腐肉，牠們被認為與死亡及亡魂有所關連。在[瑞典](../Page/瑞典.md "wikilink")，渡鴉被認為是被殺之人的鬼魂；在[德國](../Page/德國.md "wikilink")，牠們則是被咒詛之人的靈魂。\[69\]

很多[北美洲](../Page/北美洲.md "wikilink")[太平洋西北地區的原住民尊崇渡鴉為神明](../Page/太平洋西北地區.md "wikilink")。在[特林吉特人及](../Page/特林吉特人.md "wikilink")[海達族文化中](../Page/海達族.md "wikilink")，渡鴉同是騙子及創造者的神。[維京人相信在](../Page/維京人.md "wikilink")[奧丁肩膀上的渡鴉](../Page/奧丁.md "wikilink")[福金及霧尼會觀看及聆聽世界上的所有](../Page/福金及霧尼.md "wikilink")\[70\]，而[北歐的](../Page/北歐.md "wikilink")[奧克尼伯爵](../Page/奧克尼伯爵.md "wikilink")\[71\]、[英格蘭](../Page/英格蘭.md "wikilink")、[挪威及](../Page/挪威.md "wikilink")[丹麥的](../Page/丹麥.md "wikilink")[克努特大帝](../Page/克努特大帝.md "wikilink")\[72\]
及[哈拉爾三世](../Page/哈拉爾三世.md "wikilink")\[73\]
都帶有渡鴉的標記。在《[聖經](../Page/聖經.md "wikilink")》[舊約中亦有幾處提及渡鴉](../Page/舊約.md "wikilink")，而渡鴉亦是[不丹神話中](../Page/不丹.md "wikilink")[大黑天的行者](../Page/大黑天.md "wikilink")。\[74\]

對於居住在[不列顛群島的](../Page/不列顛群島.md "wikilink")[凱爾特人來說](../Page/凱爾特人.md "wikilink")，渡鴉正是他們的象徵。而在[愛爾蘭神話中](../Page/愛爾蘭神話.md "wikilink")，女神[摩利甘在英雄](../Page/摩利甘.md "wikilink")[庫胡林死後以一隻大渡鴉的形象降落在他的肩膀上](../Page/庫胡林.md "wikilink")。\[75\]
在[威爾斯神話中](../Page/威爾斯神話.md "wikilink")，這種鳥據說跟[威爾斯巨神](../Page/威爾斯.md "wikilink")[布蘭有關](../Page/布蘭.md "wikilink")，而其名正有「渡鴉」的意思。根據《[馬比諾吉昂](../Page/馬比諾吉昂.md "wikilink")》，布蘭的頭正是葬在[倫敦](../Page/倫敦.md "wikilink")，作為對抗入侵的護身符。\[76\]
期後有傳說指只要[倫敦塔上有渡鴉的存在](../Page/倫敦塔.md "wikilink")，[英格蘭就不會給戰敗予外來的入侵者](../Page/英格蘭.md "wikilink")。雖然一般都認為這是古時的信念，但有研究發現這個傳說在19世紀並未有出現，故有可能是從布蘭的傳說所製造出來的。事實上，過往塔上長期也沒有渡鴉，而是在[第二次世界大戰中重新引入的](../Page/第二次世界大戰.md "wikilink")。現時政府為了保障或吸引遊客，而在塔內飼養了幾隻渡鴉。\[77\]
這些渡鴉的其中一隻翼上的主要[羽毛會被定期剪去的](../Page/羽毛.md "wikilink")，以確保牠們不能離開。

除了在傳統的神話及民俗學上，渡鴉在現代的著作中亦經常出現，就如[威廉·莎士比亞的著作](../Page/威廉·莎士比亞.md "wikilink")，及[愛倫·坡的](../Page/愛倫·坡.md "wikilink")《烏鴉》。現代文學中亦可見到渡鴉，如[查爾斯·狄更斯](../Page/查爾斯·狄更斯.md "wikilink")\[78\]、[約翰·羅納德·瑞爾·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")\[79\]、[史蒂芬·金](../Page/史蒂芬·金.md "wikilink")《[最後的槍客](../Page/最後的槍客.md "wikilink")》\[80\]
及[瓊安·艾肯](../Page/瓊安·艾肯.md "wikilink")\[81\]\[82\]\[83\]\[84\]，與其他。渡鴉亦在曾成為神明的地區被用作象徵，如不丹的[國鳥](../Page/國鳥.md "wikilink")\[85\]、[育空地區的官方鳥](../Page/育空.md "wikilink")\[86\]、[萌島軍隊的服裝](../Page/萌島.md "wikilink")\[87\]
及[美式足球球隊](../Page/美式足球.md "wikilink")[巴尔的摩乌鸦的徽章](../Page/巴尔的摩乌鸦.md "wikilink")。

[暴雪娱乐出品的](../Page/暴雪娱乐.md "wikilink")[即时战略游戏](../Page/即时战略游戏.md "wikilink")[星际争霸2中](../Page/星际争霸2.md "wikilink")，[人类的全方位工作机被命名为](../Page/人族_\(星海爭霸\).md "wikilink")“渡鸦”。\[88\]

## 參考

## 外部連結

  - [PBS Nature:
    Ravens](http://www.pbs.org/wnet/nature/ravens/index.html)
  - [RSPB: Raven](http://www.rspb.org.uk/birds/guide/r/raven/index.asp)
  - [Caring for ravens as
    pets](http://www.restarea1mile.com/ravens.html)
  - [Beringia South Raven Research](http://bswy.org/)
  - [BirdLife Species
    Factsheet](http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=5797&m=0)
  - [Cornell Laboratory of Ornithology Species
    Account](http://www.birds.cornell.edu/AllAboutBirds/BirdGuide/Common_Raven.html)
  - [Common Raven videos on the Internet Bird
    Collection](http://ibc.hbw.com/ibc/phtml/especie.phtml?idEspecie=8311)
  - [ARKive
    images](https://web.archive.org/web/20070509035938/http://www.arkive.org/species/ARK/birds/Corvus_corax/)

### 聲音連結

  - [渡鴉的叫聲](http://www.naturesongs.com/tyrrcert.html#cora)

[CC](../Category/IUCN無危物種.md "wikilink")
[corax](../Category/鴉屬.md "wikilink")
[CC](../Category/國鳥.md "wikilink")
[Category:能言鳥類](../Category/能言鳥類.md "wikilink")

1.

2.

3.

4.

5.

6.

7.  "Raven". *Oxford English Dictionary* (2nd edition). (1989). Ed. J.
    Simpson, E. Weiner (eds). Oxford: Clarendon Press. ISBN
    978-0-19-861186-8.

8.

9.
10.
11.

12.
13.

14.

15.
16.
17.

18.

19.
20.
21.
22.

23.
24.

25.
26.

27.

28.
29.

30.
31.

32.
33.
34.

35.

36.

37.

38.
39.

40.

41.

42.

43.

44.

45.

46. Heinrich, B. (1999). *Mind of the Raven: Investigations and
    Adventures with Wolf-Birds* pp 119-120. New York: Cliff Street
    Books. ISBN 978-0-06-093063-9

47.
48.

49.
50.

51.
52.
53.
54.
55.
56.

57.

58.

59.
60.
61.

62.
63.
64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80. King, Stephen (1976). *The Gunslinger.* ISBN 978-0-8488-0780-1

81.

82.

83.

84.

85.
86.

87.

88.