**最伟大的100名英国人**（）是2002年[英国广播公司举办的一个票选活动](../Page/英国广播公司.md "wikilink")。

這項活動舉辦後引起了西方其他國家媒體的爭相模仿。

## 名單

1.  [溫斯頓·丘吉爾爵士](../Page/溫斯頓·丘吉爾.md "wikilink")（1874年－1965年）：[第二次世界大戰的](../Page/第二次世界大戰.md "wikilink")[英國首相](../Page/英國首相.md "wikilink")

2.  [伊桑巴德·金德姆·布魯內爾](../Page/伊桑巴德·金德姆·布魯內爾.md "wikilink")（1806年－1859年）：[工程師](../Page/工程師.md "wikilink")、[大西部鐵路的創建者](../Page/大西部鐵路.md "wikilink")

3.  [威爾士王妃戴安娜](../Page/戴安娜王妃.md "wikilink")（1961年－1997年）：[威爾士親王查爾斯殿下的第一任妻子](../Page/查爾斯_\(威爾士親王\).md "wikilink")（1981年－1996年）、[威廉王子和](../Page/威廉王子_\(威爾斯\).md "wikilink")[哈利王子的母親](../Page/哈利王子_\(威爾斯\).md "wikilink")。

4.  [查爾斯·羅伯特·達爾文](../Page/查爾斯·羅伯特·達爾文.md "wikilink")（1809年－1882年）：自然學家、[自然選擇與](../Page/自然選擇.md "wikilink")[演化論的創立者](../Page/演化論.md "wikilink")、《[物種起源](../Page/物種起源.md "wikilink")》作者

5.  [威廉·莎士比亞](../Page/威廉·莎士比亞.md "wikilink")（1564年－1616年）：[詩人](../Page/詩人.md "wikilink")、[劇作家](../Page/劇作家.md "wikilink")，被認為是最偉大的英語作家

6.  [艾薩克·牛頓爵士](../Page/艾薩克·牛頓.md "wikilink")（1643年－1727年）：物理學家

7.  [-{女王}-伊麗莎白一世](../Page/伊麗莎白一世_\(英格蘭\).md "wikilink")（1533年－1603年）

8.  [約翰·藍儂](../Page/約翰·藍儂.md "wikilink")（1940年－1980年）：歌手、[披頭四樂團成員](../Page/披頭四樂團.md "wikilink")

9.  [霍雷肖·納爾遜，第一代納爾遜子爵](../Page/霍雷肖·納爾遜.md "wikilink")（1758年－1805年）：[海軍司令](../Page/海軍.md "wikilink")，在[特拉法加海戰中擊敗法国西班牙联合舰队](../Page/特拉法加海戰.md "wikilink")

10. [奧利弗·克倫威爾](../Page/奧利弗·克倫威爾.md "wikilink")（1599年－1658年）：[護國公](../Page/護國公.md "wikilink")

11. [歐內斯特·沙克爾頓爵士](../Page/歐內斯特·沙克爾頓.md "wikilink")（1874年－1922年）：[南極探險家](../Page/南極.md "wikilink")

12. [詹姆斯·庫克船長](../Page/詹姆斯·庫克.md "wikilink")（1728年－1779年）：探險家

13. [羅伯特·貝登堡，第一代貝登堡男爵](../Page/羅伯特·貝登堡.md "wikilink")（1857年－1941年）:[童子軍的創建者](../Page/童子軍.md "wikilink")

14. [阿佛列大帝](../Page/阿佛列大帝.md "wikilink")（849年－899年）：[威塞克斯國王](../Page/威塞克斯.md "wikilink")，反抗丹麥人入侵的領導者

15. [阿瑟·韋爾斯利，第一代威靈頓公爵](../Page/阿瑟·韋爾斯利，第一代威靈頓公爵.md "wikilink")（1769年－1852年）：軍事家、政治家

16. [瑪格麗特·柴契爾](../Page/瑪格麗特·柴契爾.md "wikilink")（1925年－2013年）：第一位女性首相

17. （1942年－）：演員

18. [維多利亞-{女王}-](../Page/維多利亞女王.md "wikilink")（1819年－1901年）

19. [保羅·麥卡特尼爵士](../Page/保羅·麥卡特尼.md "wikilink")（1942年－）：歌手、[披頭士樂隊成員](../Page/披頭士樂隊.md "wikilink")

20. [亞歷山大·弗萊明爵士](../Page/亞歷山大·弗萊明.md "wikilink")（1881年－1955年）：生物化學家、[青黴素發明者](../Page/青黴素.md "wikilink")、1945年[諾貝爾生理學或醫學獎得主](../Page/諾貝爾生理學或醫學獎.md "wikilink")

21. [艾倫·圖林](../Page/艾倫·圖林.md "wikilink")（1912年－1954年）：[計算機先驅](../Page/計算機.md "wikilink")

22. [邁克爾·法拉第](../Page/麥可·法拉第.md "wikilink")（1791年－1867年）：科學家

23. [歐文·格林杜爾](../Page/歐文·格林杜爾.md "wikilink")（1359年－1416年）：[威爾士親王](../Page/威爾士親王.md "wikilink")

24. [-{女王}-伊麗莎白二世](../Page/伊麗莎白二世.md "wikilink")（1926年－）：英女王

25. [斯蒂芬·霍金](../Page/斯蒂芬·霍金.md "wikilink")（1942年－2018年）：理論物理學家

26. [廷岱勒](../Page/廷岱勒.md "wikilink")（1494年？－1536年）：英語《[聖經](../Page/聖經.md "wikilink")》翻譯者

27. [埃米琳·潘克赫斯特](../Page/埃米琳·潘克赫斯特.md "wikilink")（1858年－1928年）：女權推動者

28. [威廉·威爾伯福斯](../Page/威廉·威爾伯福斯.md "wikilink")（1759年－1833年）：人道主義者

29. [大衛·鮑伊](../Page/大衛·鮑伊.md "wikilink")（1947年－2016年）：歌手

30. [蓋伊·福克斯](../Page/蓋伊·福克斯.md "wikilink")（1570年－1606年）：革命者

31. （1917年－1992年）：飛行員、慈善家

32. [埃里克·莫克姆](../Page/埃里克·莫克姆.md "wikilink")（1926年－1984年）：喜劇演員

33. [大衛·貝克漢姆](../Page/大衛·貝克漢姆.md "wikilink")（1975年－）：足球運動員

34. [托馬斯·潘恩](../Page/托馬斯·潘恩.md "wikilink")（1737年－1809年）：政治家、思想家

35. [鮑狄西婭](../Page/鮑狄西婭.md "wikilink")（?－60年？）：古不列顛人的領袖，領導人民反抗[羅馬帝國的入侵](../Page/羅馬帝國.md "wikilink")

36. [史蒂芬·雷德格雷夫爵士](../Page/史蒂芬·雷德格雷夫.md "wikilink")（1962年－）：奧運[划艇選手](../Page/划艇.md "wikilink")

37. [聖托馬斯·莫爾](../Page/托馬斯·莫爾.md "wikilink")（1478年－1535年）：英國[律師](../Page/律師.md "wikilink")、[政治家](../Page/政治家.md "wikilink")、思想家、天主教[聖人](../Page/基督教聖人.md "wikilink")

38. [威廉·布萊克](../Page/威廉·布萊克.md "wikilink")（1757年－1827年）：[作家和](../Page/作家.md "wikilink")[畫家](../Page/畫家.md "wikilink")

39. [約翰·哈里森](../Page/約翰·哈里森.md "wikilink")（1693年－1776年）：[鐘錶的發明者](../Page/鐘錶.md "wikilink")

40. [亨利八世](../Page/亨利八世.md "wikilink")（1491年－1547年）：[都鐸王朝的第二位國王](../Page/都鐸王朝.md "wikilink")

41. [查爾斯·狄更斯](../Page/查爾斯·狄更斯.md "wikilink")（1812年－1870年）：作家

42. [弗蘭克·惠特爾爵士](../Page/弗蘭克·惠特爾.md "wikilink")（1907年－1996年）：[噴氣式發動機的發明者](../Page/噴氣式發動機.md "wikilink")

43. [約翰·皮爾](../Page/約翰·皮爾.md "wikilink")（1939年－2004年）：[廣播人員](../Page/廣播.md "wikilink")

44. [約翰·羅傑·貝爾德](../Page/約翰·羅傑·貝爾德.md "wikilink")（1888年－1946年）：電視機發明者

45. [安奈林·贝文](../Page/安奈林·贝文.md "wikilink")（1897年－1960年）：[政治家](../Page/政治家.md "wikilink")

46. [喬治男孩](../Page/喬治男孩.md "wikilink")（1961年－）：音樂家、[文化俱樂部成員](../Page/文化俱樂部.md "wikilink")

47. [道格拉斯·柏德爵士](../Page/道格拉斯·柏德.md "wikilink")（1910年－1982年）：[飛行員及](../Page/飛行員.md "wikilink")[慈善家](../Page/慈善家.md "wikilink")

48. [威廉·華萊士爵士](../Page/威廉·華萊士.md "wikilink")（1270年？－1305年）：[蘇格蘭民族英雄](../Page/蘇格蘭.md "wikilink")

49. [法蘭西斯·德瑞克爵士](../Page/法蘭西斯·德瑞克.md "wikilink")（1540年？－1595年）：著名的[海盜](../Page/海盜.md "wikilink")，領導英國海軍擊敗[西班牙](../Page/西班牙.md "wikilink")[無敵艦隊的指揮官之一](../Page/無敵艦隊.md "wikilink")

50. [約翰·衛斯理](../Page/約翰·衛斯理.md "wikilink")（1703年－1791年）：[衛理公會領袖](../Page/衛理公會.md "wikilink")

51. [亞瑟王](../Page/亞瑟王.md "wikilink")：傳說中的[凱爾特人的國王](../Page/凱爾特人.md "wikilink")

52. [弗羅倫斯·南丁格爾](../Page/弗羅倫斯·南丁格爾.md "wikilink")（1820年－1910年）：[護士與慈善事業推動者](../Page/護士.md "wikilink")

53. [托馬斯·愛德華·勞倫斯](../Page/托馬斯·愛德華·勞倫斯.md "wikilink")（1888年－1935年，即“阿拉伯的勞倫斯”）：[第一次世界大戰中的英國情報人員和軍人](../Page/第一次世界大戰.md "wikilink")

54. [羅伯特·斯科特](../Page/羅伯特·斯科特.md "wikilink")（1868年－1912年）：極地探險家，在到達南極點的探險中犧牲

55. [以諾·鮑威爾](../Page/以諾·鮑威爾.md "wikilink")（1912年－1998年），政治家

56. [克里夫·李察爵士](../Page/克里夫·李察.md "wikilink")（1940年－）：音樂家

57. [亞歷山大·格拉漢姆·貝爾](../Page/亞歷山大·格拉漢姆·貝爾.md "wikilink")（1847年－1922年）：電話機發明者

58. [佛萊迪·摩克瑞](../Page/佛萊迪·摩克瑞.md "wikilink")（1946年－1991年）：音樂家、[皇后樂隊成員](../Page/皇后樂隊.md "wikilink")

59. [朱莉·安德魯斯女爵士](../Page/朱莉·安德魯斯.md "wikilink")（1935年－）：女演員、歌手

60. [愛德華·埃爾加爵士](../Page/愛德華·埃爾加.md "wikilink")（1857年－1934年）：作曲家

61. [伊麗莎白王太后](../Page/伊麗莎白·鮑斯-萊昂.md "wikilink")（1900年－2002年）

62. [喬治·哈里森](../Page/喬治·哈里森.md "wikilink")（1943年－2001年）：披頭士樂隊成員

63. [大衛·愛登堡爵士](../Page/大衛·愛登堡.md "wikilink")（1926年－）：科學家、[英國廣播公司電視節目主持及製作人](../Page/英國廣播公司電視節目.md "wikilink")

64. [詹姆斯·康諾利](../Page/詹姆斯·康諾利.md "wikilink")（1868年－1916年）：[愛爾蘭革命家](../Page/愛爾蘭.md "wikilink")

65. [喬治·斯蒂芬森](../Page/喬治·斯蒂芬森.md "wikilink")（1781年－1848年）：鐵路之父

66. [查理·卓別林爵士](../Page/查理·卓別林.md "wikilink")（1889年－1977年）：喜劇演員

67. [托尼·布萊爾](../Page/托尼·布萊爾.md "wikilink")（1953年－）：聯合王國首相（1997-2007）

68. [威廉·卡克斯顿](../Page/威廉·卡克斯顿.md "wikilink")（1420年？－1492年？）：英國最早的出版家

69. [-{zh-hans:鮑比·摩爾;
    zh-hant:波比·摩亞;}-](../Page/鮑比·摩爾.md "wikilink")（1941年－1993年）：[足球員](../Page/足球.md "wikilink")

70. [簡·奧斯丁](../Page/簡·奧斯丁.md "wikilink")（1775年－1817年）：女作家

71. [卜維廉](../Page/卜維廉.md "wikilink")（1829年－1912年）：[救世軍創辦人](../Page/救世軍.md "wikilink")

72. [亨利五世](../Page/亨利五世_\(英格蘭\).md "wikilink")（1387年－1422年）：[英格蘭國王](../Page/英格蘭.md "wikilink")，在[百年戰爭中大勝法軍](../Page/百年戰爭.md "wikilink")

73. [阿萊斯特·克勞利](../Page/阿萊斯特·克勞利.md "wikilink")（1875年－1947年）：神秘主義者

74. [羅伯特一世](../Page/羅伯特一世.md "wikilink")（1274年－1329年）：蘇格蘭國王，在[班諾克本戰役中保衛了](../Page/班諾克本.md "wikilink")[蘇格蘭的獨立](../Page/蘇格蘭.md "wikilink")

75. [鮑勃·格爾多夫](../Page/鮑勃·格爾多夫.md "wikilink")（1951年－）：愛爾蘭音樂家、[新城之鼠成員](../Page/新城之鼠.md "wikilink")

76. [無名戰士](../Page/無名戰士.md "wikilink")：[第一次世界大戰陣亡而無法辨認的士兵](../Page/第一次世界大戰.md "wikilink")

77. [羅比·威廉斯](../Page/羅比·威廉斯.md "wikilink")（1974年－）：歌手

78. [愛德華·詹納](../Page/愛德華·詹納.md "wikilink")（1749年－1823年）：免疫學之父

79. [戴維·勞合·喬治](../Page/戴維·勞合·喬治.md "wikilink")（1863年－1945年）：首相（1916-1922）

80. [查爾斯·巴貝奇](../Page/查爾斯·巴貝奇.md "wikilink")（1791年－1871年）：[數學家及](../Page/數學家.md "wikilink")[電腦學先鋒](../Page/電腦.md "wikilink")

81. [杰弗裡·喬叟](../Page/喬叟.md "wikilink")（1340年？－1400年？）：詩人

82. [理查三世](../Page/理查三世_\(英格蘭\).md "wikilink")（1452年－1485年）：國王

83. [喬安·凱瑟琳·羅琳](../Page/喬安·凱瑟琳·羅琳.md "wikilink")（1965年－）：[哈利波特小說作者](../Page/哈利波特.md "wikilink")

84. [詹姆斯·瓦特](../Page/詹姆斯·瓦特.md "wikilink")（1736年－1819年）：[蒸汽機開發者](../Page/蒸汽機.md "wikilink")

85. [李察·布蘭遜爵士](../Page/李察·布蘭遜.md "wikilink")（1950年－）：[商人及](../Page/商人.md "wikilink")[探險家](../Page/探險家.md "wikilink")，[維珍集團創辦人](../Page/維珍.md "wikilink")

86. [博諾](../Page/博諾.md "wikilink")（1960年－）：愛爾蘭歌手、[U2成員](../Page/U2樂團.md "wikilink")

87. [約翰·裡頓](../Page/約翰·裡頓.md "wikilink")（1956年－）：歌手、[性手槍成員](../Page/性手槍.md "wikilink")

88. [伯納德·蒙哥馬利，第一代阿拉曼的蒙哥馬利子爵](../Page/伯納德·蒙哥馬利_\(第一代阿拉曼的蒙哥馬利子爵\).md "wikilink")（1887年－1976年）：二次大戰將領

89. [唐納德·坎貝爾](../Page/唐納德·坎貝爾.md "wikilink")（1921年－1967年）：汽艇選手

90. [亨利二世](../Page/亨利二世_\(英格蘭\).md "wikilink")（1133年－1189年）：國王，[金雀花王朝的建立者](../Page/金雀花王朝.md "wikilink")

91. [詹姆斯·克拉克·麥克斯韋](../Page/詹姆斯·克拉克·麥克斯韋.md "wikilink")（1831年－1879年）：[物理學家](../Page/物理.md "wikilink")

92. [約翰·羅納德·魯埃爾·托爾金](../Page/約翰·羅納德·魯埃爾·托爾金.md "wikilink")（1892年－1973年）：[語言學家](../Page/語言學家.md "wikilink")、[作家](../Page/作家.md "wikilink")，《魔戒》作者

93. [沃爾特·雷利爵士](../Page/沃爾特·雷利.md "wikilink")（1552年－1618年）：探險家，從美洲帶回[煙草](../Page/煙草.md "wikilink")

94. [愛德華一世](../Page/愛德華一世_\(英格蘭\).md "wikilink")（1239年－1307年）：英格蘭國王

95. （1887年－1979年）：[航空](../Page/航空.md "wikilink")[科技先驅](../Page/科技.md "wikilink")

96. [理查·波頓](../Page/理查·波頓.md "wikilink")（1925年－1984年）：演員

97. [托尼·本恩](../Page/托尼·本恩.md "wikilink")（1925年－2014年）：政治家

98. [大衛·利文斯通](../Page/大衛·利文斯通.md "wikilink")（1813年－1873年）：傳教士，考察非洲內陸的探險家

99. [蒂姆·伯納斯-李爵士](../Page/蒂姆·伯納斯-李.md "wikilink")（1955年－）：[全球資訊網的發明者](../Page/全球資訊網.md "wikilink")

100. （1880年－1958年）：[計劃生育的推動者](../Page/計劃生育.md "wikilink")

## 類似活動

  - 英国[第四台举办的](../Page/第四台_\(英國\).md "wikilink")“”（100 Worst Britons）。

  - 德國[德國電視二台電視台舉辦了名為](../Page/德國電視二台.md "wikilink")「[最伟大的德国人](../Page/最伟大的德国人.md "wikilink")」（Unsere
    Besten）的票選。

  - [加拿大廣播公司於](../Page/加拿大廣播公司.md "wikilink")2004年舉辦了「[最偉大的加拿大人](../Page/最偉大的加拿大人.md "wikilink")」（The
    Greatest Canadian）票選。

  - 荷蘭KRO電台舉辦了「[最偉大的荷蘭人](../Page/最偉大的荷蘭人.md "wikilink")」（De Grootste
    Nederlander）票選。

  - 美國[發現頻道於](../Page/發現頻道.md "wikilink")2005年5月舉辦了「[最偉大的美國人](../Page/最偉大的美國人.md "wikilink")」（The
    Greatest American）票選

  - [南非廣播公司舉辦了](../Page/南非廣播公司.md "wikilink")「[最偉大的南非人](../Page/最偉大的南非人.md "wikilink")」（SABC3's
    Great South Africans）票選。

  - [芬兰广播公司舉辦了](../Page/芬兰广播公司.md "wikilink")「[偉大的芬蘭人](../Page/偉大的芬蘭人.md "wikilink")」(*Great
    Finns*).\[1\]

  - 法國[法国电视二台電視台舉辦了](../Page/法国电视二台.md "wikilink")「[最偉大的法國人](../Page/最偉大的法國人.md "wikilink")」（Le
    Plus Grand Français）票選。

  - 比利時電視臺曾舉辦「[最偉大的比利時人](../Page/最偉大的比利時人.md "wikilink")」（De Grootste
    Belg） 票選.

  - 捷克在2005年6月播出「[最偉大的捷克人](../Page/最偉大的捷克人.md "wikilink")」（*Největší
    Čech*）。

  - 是在2003-2004年進行的線民意調查的結果。

  - 保加利亞的「[偉大的保加利亞人](../Page/偉大的保加利亞人.md "wikilink")」 (*Великите
    българи*)在2007年2月結束活動。

  - 在西班牙，[天线3於](../Page/天线3.md "wikilink")2007年5月22日選出現任國王[胡安·卡洛斯一世為](../Page/胡安·卡洛斯一世.md "wikilink")「[歷史上最重要的西班牙人](../Page/歷史上最重要的西班牙人.md "wikilink")」（El
    Español De La Historia）第一名。\[2\]

  - [新西蘭](../Page/新西蘭.md "wikilink")於2005年播出了「新西蘭100大創造歷史者」（New
    Zealand's Top 100 History Makers）

  - 希臘於2009年1月播出「偉大的希臘人」 節目（*Megaloi Ellines*, *Great Greeks*）

## 參考文獻

## 外部链接

  - [英国广播公司“最伟大的100名英国人”网站](http://www.bbc.co.uk/history/programmes/greatbritons.shtml)

[Category:英国人](../Category/英国人.md "wikilink")
[Category:最伟大人物](../Category/最伟大人物.md "wikilink")
[Category:英國廣播公司電視節目](../Category/英國廣播公司電視節目.md "wikilink")

1.  [Suuret Suomalaiset](http://www.yle.fi/suuretsuomalaiset/)
2.