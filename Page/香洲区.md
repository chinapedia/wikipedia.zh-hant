**香洲区**是[中国大陸](../Page/中国大陸.md "wikilink")[广东省](../Page/广东省.md "wikilink")[珠海市](../Page/珠海市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于[广东省中南部](../Page/广东省.md "wikilink")，[珠江出海口西岸](../Page/珠江.md "wikilink")（個別島嶼在[香港](../Page/香港.md "wikilink")[離島區及港島](../Page/離島區.md "wikilink")[南區對出水域的](../Page/南區.md "wikilink")[萬山群島](../Page/萬山群島.md "wikilink")），是珠海市政府所在地，是珠海市[政治](../Page/政治.md "wikilink")、[经济](../Page/经济.md "wikilink")、[文化](../Page/文化.md "wikilink")、[交通和](../Page/交通.md "wikilink")[金融中心](../Page/金融.md "wikilink")。总面积为300[平方公里](../Page/平方公里.md "wikilink")（此应为[横琴新区设立前](../Page/横琴新区.md "wikilink")、不包括唐家湾和[万山海洋开发试验区在内的旧香洲区管辖区面积](../Page/万山海洋开发试验区.md "wikilink")），全区常住人口89.26万，其中户籍人口59万。

2012年，香洲區實現地區生產總值806.62億元，同比增長6.5%；現代服務業佔服務業的比重為58.2%；高新技術工業產業產值占規模以上工業總產值的19.4%；三次產業比例為0.1:41.9:58.0。榮獲全國基本普及九年義務教育和基本掃除青壯年文盲工作先進地區、廣東省雙擁模範區、廣東省城鄉居民社會養老保險示範縣區和廣東省土地礦產衛片執法檢查二等獎，順利通過國家生態區考核驗收。

## 行政区划

香洲区下辖8个[街道](../Page/街道_\(行政区划\).md "wikilink")：[拱北](../Page/拱北街道.md "wikilink")、[吉大](../Page/吉大街道.md "wikilink")、[翠香](../Page/翠香街道.md "wikilink")、[狮山](../Page/狮山街道_\(珠海市\).md "wikilink")、[香湾](../Page/香湾街道.md "wikilink")、[梅华](../Page/梅华街道.md "wikilink")、[前山](../Page/前山街道.md "wikilink")、[湾仔](../Page/湾仔街道.md "wikilink")，6个镇：[唐家湾镇](../Page/唐家湾镇.md "wikilink")、[桂山镇](../Page/桂山镇_\(珠海市\).md "wikilink")、[担杆镇](../Page/担杆镇.md "wikilink")、[万山镇](../Page/万山镇_\(珠海市\).md "wikilink")、[横琴镇](../Page/横琴镇.md "wikilink")、[南屏镇](../Page/南屏镇_\(珠海市\).md "wikilink")。

## 參看

  - [九洲港](../Page/九洲港.md "wikilink")

## 参考文献

## 外部链接

  - [地理坐标](../Page/地理坐标.md "wikilink")：
  - [香洲区政府公众网](http://www.zhxz.gov.cn/)

{{-}}

[Category:珠海市辖区](../Category/珠海市辖区.md "wikilink")
[香洲区](../Category/香洲区.md "wikilink")