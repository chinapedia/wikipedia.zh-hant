**自強號列車**，簡稱**自強號**，是[臺灣鐵路管理局所轄車輛等級中等級最高的對號客車車種](../Page/臺灣鐵路管理局.md "wikilink")，也是該局四種收費等級標準中的最高收費級別。

## 簡介

1978年4月25日臺鐵於臺灣[西部幹線鐵路行駛的高級列車](../Page/西部幹線.md "wikilink")「[台鐵觀光號](../Page/台鐵觀光號.md "wikilink")」，因車輛老舊而停駛。除了由1970年開行的[莒光號列車接替外](../Page/莒光號列車.md "wikilink")，就是由[英國製的](../Page/英國.md "wikilink")[EMU100接替](../Page/臺鐵EMU100型電聯車.md "wikilink")。

1978年8月15日由[EMU100行駛的](../Page/台鐵EMU100型電聯車.md "wikilink")「自強號」正式上路。當年以含稅的每公里1.32元[新臺幣的費率](../Page/新臺幣.md "wikilink")，行駛於縱貫線[臺北到](../Page/臺北車站.md "wikilink")[臺中間](../Page/臺中車站.md "wikilink")（經[海線](../Page/海線.md "wikilink")），並隨[電氣化工程完工路段推進](../Page/鐵路電氣化.md "wikilink")，改經[山線延駛](../Page/山線.md "wikilink")[嘉義及](../Page/嘉義車站.md "wikilink")[臺南](../Page/臺南車站.md "wikilink")。西部幹線電氣化完成後行駛於[臺北至](../Page/臺北車站.md "wikilink")[高雄的縱貫線](../Page/高雄車站.md "wikilink")。

1980年代後除了首度投入營運的[EMU100](../Page/台鐵EMU100型電聯車.md "wikilink")（1979年）之外，臺灣鐵路管理局陸續將[DR2800](../Page/台鐵DR2800型柴聯車.md "wikilink")、[DR2900](../Page/台鐵DR2900型柴聯車.md "wikilink")、[EMU200](../Page/台鐵EMU200型電聯車.md "wikilink")（1987至2004年間）、[EMU300](../Page/台鐵EMU300型電聯車.md "wikilink")、[DR3000](../Page/台鐵DR3000型柴聯車.md "wikilink")、[E1000](../Page/E1000型推拉式電車.md "wikilink")、[DR3100](../Page/台鐵DR3100型柴聯車.md "wikilink")、[TEMU1000](../Page/台鐵TEMU1000型電聯車.md "wikilink")、[TEMU2000](../Page/台鐵TEMU2000型電聯車.md "wikilink")、[EMU1200等等不同車型的鐵路客車編入自強號車種](../Page/臺鐵EMU1200型電聯車.md "wikilink")，因此臺鐵自強號呈現多種車型參與營運的情形，與[莒光號連掛的商務車廂也收取自強號票價](../Page/莒光號.md "wikilink")。

現行的自強號分成3種型態的車：

1.  速度快、傾斜式、不賣站票的[太魯閣號和](../Page/太魯閣列車.md "wikilink")[普悠瑪號](../Page/普悠瑪列車.md "wikilink")，皆為8輛編組。
2.  速度較慢、比較舊的推拉式自強號(PP)、電聯車自強號EMU系列，在一般情況下，PP通常為12輛編組，EMU通常為3組9輛編組，少數西部班次為4組12輛。
3.  柴聯自強號[DR2800](../Page/台鐵DR2800型柴聯車.md "wikilink")～[DR3100](../Page/台鐵DR3100型柴聯車.md "wikilink")，依照使用情況的不同，有6輛、9輛、12輛三種編組。

同一種車種和票價，卻有三種不同的列車與服務品質，會造成公平性的問題，所以臺鐵打算在2014年中臺東線鐵路電氣化通車之後，調整北迴線、臺東線的自強號票價，實施差別費率。

行駛於北迴線的普悠瑪、太魯閣號自強號列車，票價將與PP、柴聯自強號做出區隔，普悠瑪、太魯閣號將漲價5%，但舊柴聯自強號降價10%，希望以調整票價，讓不同等級的自強號達到旅客分流的效果，不再一窩蜂搶搭普悠瑪及太魯閣號。

## 營運

[EMU100_Tze-Chiang_Limited_Express_light_box_20070103.jpg](https://zh.wikipedia.org/wiki/File:EMU100_Tze-Chiang_Limited_Express_light_box_20070103.jpg "fig:EMU100_Tze-Chiang_Limited_Express_light_box_20070103.jpg")
臺鐵自強號雖有不同車型，除某些特例外，費率都是相同的。莒光號的商務車廂也收此種票價。依照2014年的票價是每公里新臺幣2.27元為計算單位，臺北到（西部幹線以南）高雄是843元、到[屏東是](../Page/屏東車站.md "wikilink")891元；臺北到（北迴幹線以東）[花蓮是](../Page/花蓮車站.md "wikilink")440元、到[臺東是](../Page/臺東車站.md "wikilink")783元。

自強號雖定位於城際長途列車，但多卡通、定期票也能搭乘此車種，因此也是被臺灣民眾用來[通勤的車種之一](../Page/通勤.md "wikilink")。

自強號停站數少、車程所耗時間少，因此最受臺灣旅客喜歡。即使不是[區間車](../Page/臺鐵區間車.md "wikilink")，自強號的每年搭乘總人次卻佔臺鐵搭乘人次的兩成。

自強號會因停靠站多寡而影響行駛時間，以下是以行車時間的長短計時，人潮擁擠時則行車時間不在此限。

縱貫線臺北－高雄的行車時間最短及最長\[1\]分別為：

  - 《臺北→高雄》
      - 3小時33分（127次）、3小時36分(111次）[普悠瑪號](../Page/普悠瑪號.md "wikilink")
      - 4小時10分（143次）超級直達[自強號](../Page/自強號.md "wikilink")
      - 5小時10分（145次）
  - 《高雄→臺北》
      - 3小時36分（136次）、3小時33分（110次）[普悠瑪號](../Page/普悠瑪號.md "wikilink")
      - 5小時05分（112次）

縱貫線臺北－屏東的行車時間最短及最長分別為：

  - 《臺北→屏東》
      - 3小時50分（127次） 普悠瑪號（停靠站:南港.松山.台北.板橋.新竹.台中.台南.高雄.屏東.潮州）
      - 4小時32分（143次）
        逢周五及連續假日前一日行駛（停靠站:七堵.汐止.松山.台北.板橋.台中.彰化.嘉義.台南.高雄.鳳山.屏東.潮州）
      - 5小時34分（103次）
  - 《屏東→臺北》
      - 3小時51分（110次） 普悠瑪號(停靠站:潮州.屏東.高雄.台南.台中.新竹.板橋.台北.松山.南港）
      - 5小時30分（112次）

[北迴線臺北](../Page/北迴線.md "wikilink")—花蓮的行車時間最短及最長分別為：

  - 《臺北→花蓮》
      - 2小時（408、426次）太魯閣號（停靠站:樹林.板橋.台北.松山.花蓮.玉里.台東(408次終點).知本(426次終點)）
      - 3小時06分（246次）
  - 《花蓮→臺北》
      - 2小時（421、441次）太魯閣號(停靠站:知本(441次起點).台東(421次起點).玉里.花蓮.松山.台北.板橋.樹林)
      - 3小時13分（407次）

[東幹線臺北](../Page/東部幹線.md "wikilink")－臺東的行車時間最短及最長分別為：

  - 《臺北→臺東》
      - 3小時30分（408、426次）太魯閣號
      - 4小時24分（418次）
  - 《臺東→臺北》
      - 3小時30分（421、441次）太魯閣號
      - 5小時58分（407次）

南迴線高雄—臺東的行車時間最短及最長分別為：

  - 《高雄→臺東》
      - 2小時07分（303次）
      - 2小時55分（313次）
  - 《臺東→高雄》
      - 2小時13分（324次）
      - 3小時00分（306次）
  - 1991年7月6日至1992年3月31日間曾於少數班次實施商務車廂，但因賣座狀況不佳而草草結束。部分莒光號商務車（BCK）以自強號票價收費

## 車型

[Double_Tzu-Chang.JPG](https://zh.wikipedia.org/wiki/File:Double_Tzu-Chang.JPG "fig:Double_Tzu-Chang.JPG")
[First_TimeTable_of_TRA_EMU100.gif](https://zh.wikipedia.org/wiki/File:First_TimeTable_of_TRA_EMU100.gif "fig:First_TimeTable_of_TRA_EMU100.gif")

至目前為止，自強號共以下列車型行駛：

  - [推拉式自強號](../Page/推拉式自強號.md "wikilink")：1996年9月開始營運。亦稱推拉式電車(Push-Pull,
    P.P.)，動力車主體由[南非聯邦鐵路客貨車](../Page/聯邦鐵路客貨車公司.md "wikilink")（Union
    Carriage & Wagon
    Company，UCW）製，電機系統為法國[阿爾斯通](../Page/阿爾斯通.md "wikilink")(Alstom)製。兩台機車同步運轉，加上拖車（[南韓](../Page/南韓.md "wikilink")[現代重工製車體](../Page/現代集團.md "wikilink")，臺灣[唐榮重工負責內裝](../Page/唐榮鐵工廠.md "wikilink")）後被稱為推拉式電車組。自強號現行主力車型，主要行駛[西部幹線](../Page/西部幹線.md "wikilink")、東部幹線區間。不過因轉向架品質與車廂輕量化之緣故，高速行駛時會有抖動共振的現象；後雖陸續經過更新避震彈簧來解決，仍無法完全解除抖動情況。

<!-- end list -->

  - [電聯車](../Page/臺鐵電聯車.md "wikilink")
      - [EMU1200型](../Page/台鐵EMU1200型電聯車.md "wikilink")：2002年開始營運。[台灣車輛製](../Page/台灣車輛.md "wikilink")，主要行駛於[縱貫線
        (南段)及](../Page/縱貫線_\(南段\).md "wikilink")[屏東線](../Page/屏東線.md "wikilink")，是因應[EMU200型設備老舊以及各種缺失改造而來的](../Page/台鐵EMU200型電聯車.md "wikilink")，由於車況差且數量稀少，所以常用推拉式自強號代替行駛。2016年改點後僅行駛在[彰化與](../Page/彰化車站.md "wikilink")[潮州之間](../Page/潮州車站.md "wikilink")。
      - [EMU300型](../Page/台鐵EMU300型電聯車.md "wikilink")：1989年開始營運。[義大利](../Page/義大利.md "wikilink")
        [米蘭工業製造](../Page/米蘭工業製造公司.md "wikilink")（Societa Costruzioni
        Industriali Milano，SOCIMI）製，主要行駛於[縱貫線
        (北段)](../Page/縱貫線_\(北段\).md "wikilink")、[海線及](../Page/海岸線_\(臺鐵\).md "wikilink")[縱貫線
        (南段)](../Page/縱貫線_\(南段\).md "wikilink")，現因原廠倒閉而逐漸減少行駛次數及里程。雖然設計營運時速為130公里，但在[轉向架更新後已無法達到](../Page/轉向架.md "wikilink")，2016年改點後最南僅行駛至[斗南](../Page/斗南車站.md "wikilink")。

<!-- end list -->

  - [傾斜式列車](../Page/傾斜式列車.md "wikilink")，本車種限有劃位的旅客始得搭乘
      - [TEMU1000型](../Page/台鐵TEMU1000型電聯車.md "wikilink")：2007年開始營運。日本[日立製](../Page/日立.md "wikilink")，傾斜式列車，以「[太魯閣號](../Page/太魯閣號.md "wikilink")」列車名稱營運，2007年首航。主要行駛[東部幹線區間](../Page/東部幹線.md "wikilink")，2016年10月改點後西部幹線最南僅行駛到[員林](../Page/員林車站.md "wikilink")。
      - [TEMU2000型](../Page/台鐵TEMU2000型電聯車.md "wikilink")：2013年開始營運。日本[日本車輛製](../Page/日本車輛製造.md "wikilink")，傾斜式列車，以「[普悠瑪號](../Page/普悠瑪號.md "wikilink")」列車名稱營運，2013年首航，2013年春節投入正式營運，主要行駛東部幹線區間，2016年改點後西部幹線最南行駛到[潮州](../Page/潮州車站.md "wikilink")。

<!-- end list -->

  - [柴聯車](../Page/臺鐵柴聯車.md "wikilink")
      - [DR2800型](../Page/台鐵DR2800型柴聯車.md "wikilink")：1982年6月開始營運。日本[東急車輛製](../Page/東急車輛製造.md "wikilink")（電系為日立），亦稱為DR2800柴聯車。
      - [DR2900型](../Page/台鐵DR2900型柴聯車.md "wikilink")：1986年12月開始營運。日本[日立製作所製](../Page/日立.md "wikilink")，亦稱為DR2900柴聯車。
      - [DR3000型](../Page/台鐵DR3000型柴聯車.md "wikilink")：1990年5月開始營運。日本[日立製作所製](../Page/日立.md "wikilink")，亦稱為DR3000柴聯車。
      - [DR3100型](../Page/台鐵DR3100型柴聯車.md "wikilink")：1998年8月開始營運。日本[日本車輛製第一組列車](../Page/日本車輛製造.md "wikilink")（電系為日立），其餘第2組至第10組在技術移轉下由[臺灣車輛製造](../Page/臺灣車輛.md "wikilink")，亦稱為DR3100柴聯車。

<!-- end list -->

  -
    上述DR2800、DR3100柴聯車配屬於花蓮機務段，而DR2900、DR3000柴聯車則配屬於臺東機務分段與高雄機務段（潮州=枋寮區間車，新左營=枋寮區間快），主要行駛於[東部幹線與](../Page/東部幹線.md "wikilink")[南迴線](../Page/南迴線.md "wikilink")，柴聯自強號在臺鐵自強號中故障率也是最低的。

2019年1月，[日立製作所取得台鐵](../Page/日立製作所.md "wikilink")600輛城際型電聯車採購案訂單，預定2021-2025年將陸續引進50列12輛編成之新型城際電聯車，屆時將陸續替換現行[傾斜式列車以外的自強號車型](../Page/傾斜式列車.md "wikilink")，成為未來自強號的主力營運車款。

## 停靠站

  - 主要起點終點站：

基隆、七堵、樹林、新左營、屏東、潮州、花蓮、臺東。

  - 主要停靠站：

汐止、松山、台北、板橋、桃園、中壢、新竹、竹南、苗栗、豐原、臺中、大甲、沙鹿、彰化、員林、斗六、嘉義、新營、臺南、高雄、鳳山、潮州、枋寮、知本、宜蘭、羅東、瑞穗、玉里、池上、關山。

  - 次要停靠站：

八堵、南港、後龍、通霄、苑裡、清水、田中、斗南、善化、永康、岡山、九曲堂、南州、林邊、瑞芳、雙溪、頭城、礁溪、蘇澳新、南澳、新城(太魯閣)、吉安、志學、壽豐、鳳林、光復、富里、鹿野、大武、金崙、太麻里

  - 三要停靠站：

后里、二水、民雄、福隆、和平

  - 少數停靠站:

萬華(204次\<6208逢週日行駛\>、272次)、湖口(177次、272次)、竹北(147次、272次)、大肚(109次)、大林(170次)、貢寮(272次)、富源(407次、328次)、瀧溪(304次、313次、326次)、康樂(313次、326次\<逢週五、日行駛\>)、林榮新光(412次、222次(週日6222)、431次、225次(6225次))、楊梅(272次)

## 名稱

[Tze_Chiang_Express_Edmondson_Ticket.jpg](https://zh.wikipedia.org/wiki/File:Tze_Chiang_Express_Edmondson_Ticket.jpg "fig:Tze_Chiang_Express_Edmondson_Ticket.jpg")；較早期的以橫式印刷，被車票收集者稱為「橫自強」\[2\]。\]\]
自強號在時刻表或票證名稱上，曾有「電特快」、「自強特快」等別稱。臺鐵於1976年委託中華民國民意測驗協會，以內部預定好的四十餘個名字及「對在電化工程完成後,與莒光號相同之優等電車,試擬命名,以那個最適當之反應」問題來隨機訪問旅客。這些名字中包含自強號、中正號、總統號、無敵號、知恥號、神州號、反攻號、東海號、平安號、禮義號、人權號、勝利號、自由號、復神號、踏實號、四維號等等。調查結果為自強號(1,115)、勝利號(987)及自由號(823)的票數各占三分之一。臺鐵把此結果呈報給[臺灣省政府](../Page/臺灣省政府.md "wikilink")，經當時[臺灣省政府主席](../Page/臺灣省政府主席.md "wikilink")[謝東閔裁奪後](../Page/謝東閔.md "wikilink")，該新型客車定名為自強號，希望這批車能「莊敬自強，處變不驚」且能「自強不息」\[3\]\[4\]。

2010年前，臺鐵官方資料把自強號譯成Tzu-Chiang，不過民間卻普遍譯成Tze-Chiang，少數車站亦有參照漢語拼音譯為Zi-qiang。惟該譯法已統一，例如統計年報之各項資訊已翻譯成Tze-Chiang\[5\]\[6\]。

## 缺失

依據台鐵內部的統計，2006年4月至7月，在[復興號](../Page/復興號列車.md "wikilink")、[莒光號](../Page/莒光號列車.md "wikilink")、自強號等三種列車之中，自強號的準點率都是最低\[7\]。

由於有兩種行駛條件、兩種電化自強號，造成一種票價三種服務(DR.PP.TEMU)的現象，但問題已解決，DR系列（以DR2800～DR3100列車開行之自強號）週一～週四票價97折，普悠瑪號、太魯閣號漲價5%(西線普悠瑪110、111、127、136次以原價計算)。

## 攝影集

### 電氣化車型

<File:TRA_EMU100_at_Sike_Station.jpg>|[EMU100自強號](../Page/台鐵EMU100型電聯車.md "wikilink")（[汐科車站](../Page/汐科車站.md "wikilink")）
<File:Taiwan> Railway Administration EMU 206 Banqiao
1989.jpg|[EMU200型自強號在舊](../Page/台鐵EMU200型電聯車.md "wikilink")[板橋車站](../Page/板橋車站_\(臺灣\).md "wikilink")（尚未地下化）
<File:TRA> EMU1200 at Taichung
Station.jpg|[EMU1200自強號](../Page/台鐵EMU1200型電聯車.md "wikilink")（[舊臺中車站](../Page/舊臺中車站.md "wikilink")）
<File:TRA> EMU300
01.JPG|[EMU300自強號](../Page/台鐵EMU300型電聯車.md "wikilink")（[彰化車站](../Page/彰化車站.md "wikilink")）
<File:EMU1000> passing LinFongYing
Station.jpg|[E1000型推拉式自強號](../Page/推拉式自強號.md "wikilink")（[柳營車站](../Page/柳營車站.md "wikilink")）
<File:Taroko> waiting another one at Toucheng
station.jpg|[TEMU1000型自強號](../Page/台鐵TEMU1000型電聯車.md "wikilink")，別名太魯閣號（[頭城車站](../Page/頭城車站.md "wikilink")）
<File:TRA> TED2003
20130525.jpg|[TEMU2000型新自強號](../Page/台鐵TEMU2000型電聯車.md "wikilink")，別名普悠瑪號（[花蓮車站](../Page/花蓮車站.md "wikilink")）

### 非電氣化車型

<File:TRA> DR2800
01.JPG|[DR2800自強號](../Page/台鐵DR2800型柴聯車.md "wikilink")（花蓮車站）
<File:TRA> DR2900 DR3000
01.JPG|[DR2900](../Page/台鐵DR2900型柴聯車.md "wikilink")／[DR3000自強號](../Page/台鐵DR3000型柴聯車.md "wikilink")（花蓮車站）
<File:TRA> DR3100
01.JPG|[DR3100型自強號](../Page/台鐵DR3100型柴聯車.md "wikilink")（花蓮車站）

## 備註

<references />

## 相關條目

  - [普快車](../Page/普快車.md "wikilink")
  - [冷氣平快](../Page/冷氣平快.md "wikilink")
  - [復興號](../Page/復興號列車.md "wikilink")
  - [莒光號](../Page/莒光號列車.md "wikilink")
  - [太魯閣號](../Page/太魯閣列車.md "wikilink")
  - [普悠瑪號](../Page/普悠瑪列車.md "wikilink")
  - [臺鐵電聯車](../Page/臺鐵電聯車.md "wikilink")
  - [推拉式自強號](../Page/推拉式自強號.md "wikilink")

[列](../Category/臺灣鐵路管理局.md "wikilink")
[Category:台灣鐵路列車](../Category/台灣鐵路列車.md "wikilink")

1.  交通部鐵路管理局2018年10月14日最新頒布的參考數據
2.
3.
4.
5.  臺鐵：《臺灣鐵路統計年報》，2010年
6.  臺鐵：《臺灣鐵路統計年報》，2011年
7.