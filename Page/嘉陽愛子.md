**嘉陽愛子**（）,
是一名[日本女性](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。[神奈川縣出身](../Page/神奈川縣.md "wikilink")，身高154cm，血型A型。所屬公司為[FITONE和](../Page/FITONE.md "wikilink")[艾迴唱片](../Page/艾迴唱片.md "wikilink")。有「あいぴー」的綽號。

## 人物介紹

  - 出道前曾參加「[早安少女組](../Page/早安少女組.md "wikilink")。LOVE
    Audition21」，並去到最後一回選拔。
  - 聲線偏向「[萌聲](../Page/萌.md "wikilink")」，也有擔任過一些[配音員工作](../Page/配音員.md "wikilink")。
  - 說話用詞被認為和[小倉優子差不多](../Page/小倉優子.md "wikilink")。
  - 略懂[魔術](../Page/魔術.md "wikilink")。
  - [SPHERE
    LEAGUE所舉辦的女性](../Page/SPHERE_LEAGUE.md "wikilink")[藝人](../Page/藝人.md "wikilink")[室內足球隊伍之一的](../Page/室內足球.md "wikilink")「TEAM
    dream」成員。

## 音樂作品

### 單曲

1.  **瞳の中の迷宮**（c/w：solitude）
      -
        2003年12月10日
        [動畫](../Page/日本動畫.md "wikilink")「[暗與帽子與書的旅人](../Page/暗與帽子與書的旅人.md "wikilink")」片頭曲
2.  **愛してね♥もっと**（c/w：Eyes）
      -
        2004年4月14日
        動畫「[天上天下](../Page/天上天下.md "wikilink")」片尾曲、電視劇「[エコエコアザラク](../Page/エコエコアザラク.md "wikilink")～眼～」片尾曲
3.  **Fantasy**
      -
        2004年11月17日
4.  **いえないコトバ**
      -
        2004年11月25日
        動畫「[サムライガン](../Page/サムライガン.md "wikilink")」片尾曲
5.  **ring\!Ring\!\!RING\!\!\!**
      -
        2004年12月1日
6.  **traveller**
      -
        2004年12月8日
7.  **こころの惑星～Little planets～**（c/w：STEP BACK IN TIME）
      -
        2005年5月25日
        動畫「[植木的法則](../Page/植木的法則.md "wikilink")」第一首片尾曲
8.  **彼女はゴキゲンななめ**（c/w：さすらいの天使）
      -
        2005年7月27日
9.  **Hold on to love**（c/w：Callin'～僕を呼ぶ声～）
      -
        2006年1月25日
        [網路遊戲](../Page/網路遊戲.md "wikilink")「[伊希歐之夢](../Page/伊希歐之夢.md "wikilink")」主題曲
10. **☆HOME MADE STAR☆～嘉陽愛子のテーマ～**（c/w：Orange road）
      -
        2006年8月23日
        網路遊戲「伊希歐之夢」
11. **cosmic cosmetics**（c/w：約束の樹の下で…／Bonus Track：愛してね♥もっと[Animelo
    Summer Live](../Page/Animelo_Summer_Live.md "wikilink") 2006
    -OUTRIDE- version）
      -
        2006年11月15日
12. **勇気のチカラ**（c/w：Destiny～未来という名の物語～）
      -
        2007年4月4日発売
        電視遊戲「[史上最強弟子兼一](../Page/史上最強弟子兼一.md "wikilink")～激闘！ラグナレク八拳豪～」片頭曲、片尾曲

### 專輯

1.  **Dolce**
      -
        2006年3月1日
        瞳の中の迷宮／愛してね♥もっと／Fantasy／いえないコトバ／ring\!Ring\!\!RING\!\!\!／traveller／こころの惑星～Little
        planets～／彼女はゴキゲンななめ／Hold on to love／緑の季節／さすらいの天使／夕暮れはラブ・ソング

### DVD

  - THE SHORT STORY OF エコエコアザラク～眼～

<!-- end list -->

  -
    2004年5月26日

<!-- end list -->

  - エコエコアザラク～眼～ ディレクターズカット DVD-BOX

<!-- end list -->

  -
    2004年5月26日

<!-- end list -->

  - 愛子・歌謡博

<!-- end list -->

  -
    2005年7月27日

### 其他

  - OUTRIDE（「[Animelo Summer
    Live](../Page/Animelo_Summer_Live.md "wikilink") 2006」主題曲）

## 主要參與作品

### 電視演出

  - [シブスタ](../Page/シブスタ.md "wikilink")
  - [暗與帽子與書的旅人](../Page/暗與帽子與書的旅人.md "wikilink")（レイラ）
  - アイドル刑事（[TBS](../Page/TBS.md "wikilink")）
  - [Girl's BOX](../Page/Girl's_BOX.md "wikilink")「サンタの涙」（アイコ役）
    (2005年12月，[BS-i](../Page/BS-i.md "wikilink"))

### 廣播

  - 嘉陽愛子・集合mode（[アニスパ](../Page/アニスパ.md "wikilink")／[文化放送](../Page/文化放送.md "wikilink")＆[BSQR489](../Page/BSQR489.md "wikilink")）
  - 嘉陽愛子・放課後mode→おかえりmode（文化放送）
  - ゴチャ2

### 演唱會

  - [Animelo Summer Live](../Page/Animelo_Summer_Live.md "wikilink")
    2006 -OUTRIDE-

### 書籍

  - Pretty Private

## 外部連結

  - [Aiko Kayo Official
    Site](https://web.archive.org/web/20070121155538/http://www.avexnet.or.jp/kayo/)

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本女性偶像](../Category/日本女性偶像.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")
[Category:愛貝克思集團藝人](../Category/愛貝克思集團藝人.md "wikilink")