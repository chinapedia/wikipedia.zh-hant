**HR**、**Hr**或**hr**可以指：

  - ，電視動畫系列

  - 高風險（High Risk），部分[信用評級系統中最低的評級](../Page/信用評級.md "wikilink")

  - [人力資源](../Page/人力資源.md "wikilink")，在管理學的領域

  - [漢普頓錨地](../Page/漢普頓錨地.md "wikilink")（Hampton Roads），美國維珍尼亞州東南部的一個地區

  - [哈里亞納邦](../Page/哈里亞納邦.md "wikilink")（[IN-HR](../Page/ISO_3166-2:IN.md "wikilink")），印度北部的一個邦

  - [歐盟外交和安全政策高級代表](../Page/歐盟外交和安全政策高級代表.md "wikilink")（High
    Representative of the Union for Foreign Affairs and Security Policy）

  - [.hr](../Page/.hr.md "wikilink")，克羅地亞（）國家及地區頂級域（ccTLD）的域名

  - [美國眾議院](../Page/美國眾議院.md "wikilink")（United States House of
    Representatives）及其提出的法案

  - （Half
    Rate；縮寫：HR或GSM-HR），1990年代早期發展出來的[GSM語音編碼](../Page/全球移动通信系统#语音编码.md "wikilink")

  - [心率](../Page/心率.md "wikilink")（Heart Rate），心臟每分鐘跳動的次數

  - [赫羅圖](../Page/赫羅圖.md "wikilink")（Hertzsprung–Russell diagram；簡稱：H-R
    diagram或HRD），研究恆星演化的重要工具

  - [同源重組](../Page/同源重組.md "wikilink")（Homologous
    recombination），在遺傳學，指兩股具有相似序列的DNA的重新排列，使遺傳物質發生交換

  - [小時](../Page/小時.md "wikilink")（**h**ou**r**，縮寫：hr或h）

  - [HR](../Page/星表#耶魯亮星表\(BS,_BSC,_HR\).md "wikilink")，代表**H**arvard
    observatory, **R**evised photometry的星表符號

  - <code>

    <hr/>

    </code>，[HTML元素](../Page/HTML元素.md "wikilink")，用以製造橫線或分隔線

  - 人類遺骸，亦即[屍體](../Page/屍體.md "wikilink")（human remains），縮寫HR為殯儀館等實體所用

  - [全壘打](../Page/全壘打.md "wikilink")（home run），棒球術語

  - 澳洲車廠[霍頓汽車在](../Page/霍頓汽車.md "wikilink")1960年代的汽車型號

  - [哈佛恒星测光表修订版](../Page/哈佛恒星测光表修订版.md "wikilink")（Harvard Revised
    Photometry），[耶鲁亮星星表的前身](../Page/耶鲁亮星星表.md "wikilink")

  - [HR (组合)是日本的偶像组合](../Page/HR_\(组合\).md "wikilink")

  - [HR (情景喜剧)](../Page/HR_\(情景喜剧\).md "wikilink")，日本电视剧

  - hR，[六方晶系中Rhombohedral的](../Page/六方晶系.md "wikilink")[皮尔逊符号](../Page/皮尔逊符号.md "wikilink")