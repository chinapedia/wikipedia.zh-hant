**美国海军特种作战研究大队**（[英文](../Page/英文.md "wikilink")：**United States Naval
Special Warfare Development
Group**，[缩写](../Page/缩写.md "wikilink")：****，常用[縮寫](../Page/縮寫.md "wikilink")：****）前身為**美国海军海豹突击队第六分队**（簡稱**海豹六隊**；[英文](../Page/英文.md "wikilink")：**US
Seal Team
Six**，[缩写](../Page/缩写.md "wikilink")：**ST6**），是[美国海军的](../Page/美国海军.md "wikilink")[反恐](../Page/反恐.md "wikilink")[特种部队](../Page/特种部队.md "wikilink")，隶属于[美国](../Page/美国.md "wikilink")[联合特种作战司令部](../Page/联合特种作战司令部.md "wikilink")[美国海军特种作战司令部](../Page/美国海军特种作战司令部.md "wikilink")，於戰爭时则由[联合特种作战司令部指挥](../Page/联合特种作战司令部.md "wikilink")，其主要責任為蒐集情報、執行[反恐怖主義行動](../Page/反恐怖主義.md "wikilink")，以及進行團中的科技研發以至管理等等。與[三角洲部隊同為美國特種作戰部隊中級別最高的第一級部隊](../Page/三角洲部隊.md "wikilink")（Tier
1 Operator）。

美国海军特种作战研究大队通过[联合特种作战司令部与](../Page/联合特种作战司令部.md "wikilink")[中央情报局合作进行在國內外的](../Page/中央情报局.md "wikilink")[隐蔽行动](../Page/隐蔽行动.md "wikilink")。其近年來參與過的其中一項著名的行動，是於2011年5月，此部隊[擊斃賓拉登](../Page/奥萨马·本·拉登之死.md "wikilink")。此外，海军特种作战研究大队此名称于2010年后不再使用，新名称至今仍然為保密。

## 历史

[DEVGRU_soldiers_protecting_Hamid_Karzai.jpg](https://zh.wikipedia.org/wiki/File:DEVGRU_soldiers_protecting_Hamid_Karzai.jpg "fig:DEVGRU_soldiers_protecting_Hamid_Karzai.jpg")
1980年隨著[美國](../Page/美國.md "wikilink")[联合特种作战司令部一同成立](../Page/联合特种作战司令部.md "wikilink")，海豹突击队第六分队的成立可追溯于1980年[鷹爪行動的失敗](../Page/鷹爪行動.md "wikilink")，當時試圖营救受困在美國驻伊朗大使館的52名人质。在此之前，海豹部隊就已經開始展開反恐行動，包括位於西海岸的海豹一队**埃克排**（**Echo
Platoon**），不過海豹部隊位於東海岸的二队則是挑了兩排更進一步的成立了**第六机动小组**（**MOB SIX**，**Mobility
Six**），而這隊伍也成了預期的海軍方案專門替反恐行動負責，並且持續訓練下去。

海豹六隊成立於1980年10月，也就是鷹爪行動失敗的六個月過後，一開始只是作為軍人為戰爭預備的先行訓練單位，而在這時期只有兩支海豹隊伍存在並且執行任務，海豹六队於1981年開始正式官方運作。

1987年，海豹六隊擁有了正式名稱海軍特種作戰研究大隊（[英文](../Page/英文.md "wikilink")：**Naval
Special Warfare Development
Group**，[縮寫](../Page/縮寫.md "wikilink")：**DEVGRU**），關於名稱為何變換存有多種說法，從運作防衛到隊伍名聲，所有说法都指向了海豹六隊的创建人[理查德·馬辛柯](../Page/理查德·馬辛柯.md "wikilink")。儘管如此，海豹部隊第六分隊仍然是其最流傳及至今仍然被公眾稱呼的名稱。

[美国东部时间](../Page/美国东部时间.md "wikilink")2011年5月1日，此部隊在[巴基斯坦殺死涉嫌策劃](../Page/巴基斯坦.md "wikilink")[九一一事件而被](../Page/九一一事件.md "wikilink")[美國政府](../Page/美國政府.md "wikilink")[通緝多年的](../Page/通緝.md "wikilink")[基地組織](../Page/基地組織.md "wikilink")[領導人](../Page/領導人.md "wikilink")[本拉登](../Page/本拉登.md "wikilink")。

## 裝備

下列為不完全的裝備紀錄。

### 槍械

#### [手槍](../Page/手槍.md "wikilink")

  - [SIG P226](../Page/SIG_P226手槍.md "wikilink")（正被格洛克19取代）
  - [SIG P228](../Page/SIG_P228手槍.md "wikilink")（已撤裝）
  - [SIG P239](../Page/SIG_P239手槍.md "wikilink")
  - [格洛克19](../Page/格洛克19.md "wikilink")
  - [HK45CT](../Page/HK45手槍.md "wikilink")
  - [HK P11](../Page/HK_P11手槍.md "wikilink")（已撤裝）

#### [衝鋒槍](../Page/衝鋒槍.md "wikilink")

  - [HK MP5SD、MP5N](../Page/HK_MP5衝鋒槍.md "wikilink")（正被HK MP7取代）
  - [HK MP7A1](../Page/HK_MP7衝鋒槍.md "wikilink")

#### [步槍](../Page/步槍.md "wikilink")

  - [HK416](../Page/HK416突擊步槍.md "wikilink")
  - [HK417](../Page/HK417自動步槍.md "wikilink")
  - [M4A1](../Page/M4A1卡賓槍.md "wikilink")（正被HK416及其他AR-15改型取代）
  - [Mk 18 Mod 0 / Mk 18 Mod 1](../Page/CQBR.md "wikilink")
  - [AK系列](../Page/AK-47突擊步槍.md "wikilink")（用於訓練及隱蔽行動）

#### [狙擊步槍](../Page/狙擊步槍.md "wikilink")

  - [Mk 12 Mod 0/Mod 1](../Page/Mk_12特別用途步槍.md "wikilink")（已撤裝）
  - [Mk 13 Mod
    5](../Page/Mk_13狙击步枪.md "wikilink")：采用[AICS枪托的](../Page/精密國際AW狙擊步槍.md "wikilink")[雷明顿700狙击步枪](../Page/雷明登700步枪.md "wikilink")
  - [Mk 11 Mod 0](../Page/SR-25狙擊步槍.md "wikilink")
  - [Mk 15 Mod 0](../Page/TAC-50狙擊步槍.md "wikilink")
  - [巴雷特M107](../Page/巴雷特M82狙擊步槍.md "wikilink")

#### [機槍](../Page/機槍.md "wikilink")

  - [Mk 46 Mod 0 / Mod 1](../Page/M249班用自動武器.md "wikilink")
  - [Mk 43 Mod 0 / Mod 1](../Page/M60通用機槍.md "wikilink")
  - [Mk 48 Mod 0 / Mod 1](../Page/Mk_48輕量化機槍.md "wikilink")

### [霰彈槍](../Page/霰彈槍.md "wikilink")

  - [M1014](../Page/伯奈利M4_Super_90半自動霰彈槍.md "wikilink")
  - [M870](../Page/雷明登870泵動式霰彈槍.md "wikilink")
  - [Masterkey](../Page/Masterkey槍管下掛式霰彈槍.md "wikilink")

### [榴彈發射器](../Page/榴彈發射器.md "wikilink")

  - [M79](../Page/M79榴彈發射器.md "wikilink")（截短槍身型）
  - [M203](../Page/M203榴彈發射器.md "wikilink")（下掛在[M4卡賓槍](../Page/M4卡賓槍.md "wikilink")[護木下](../Page/護木.md "wikilink")）
  - [M320](../Page/M320榴彈發射器.md "wikilink")（下掛在[M4卡賓槍或](../Page/M4卡賓槍.md "wikilink")[HK416突擊步槍](../Page/HK416突擊步槍.md "wikilink")[護木下](../Page/護木.md "wikilink")，亦可單獨使用）
  - [Mk 19](../Page/Mk_19自動榴彈發射器.md "wikilink")
  - [HK GMG](../Page/HK_GMG自動榴彈發射器.md "wikilink")

### 反坦克武器

  - [M136 AT-4](../Page/AT4反坦克火箭筒.md "wikilink")
  - [SMAW](../Page/肩射多用途攻擊武器.md "wikilink")
  - [FGM-148標槍飛彈](../Page/FGM-148標槍飛彈.md "wikilink")

### 其他

  - 防彈盔（目前主要使用Ops-Core FAST Ballistic、Crye Precision
    Airframe以及MICH-2000系列等）
  - 戰術背心（LBT 6094、Crye Precision JPC、Eagle Industries MMAC等多種）
  - 迷彩軍服（目前主要使用Crye Precision
    [Multicam](../Page/Multicam.md "wikilink")、AOR-1及AOR-2）
  - 戰術槍套（包括：Safariland 6004）
  - 抗噪音耳機
  - 戰術靴
  - 手套（包括Mechanix M-PACT系列）
  - 通信設備

## 重大事件

2011年8月6日，22名海豹突击队官兵所乘坐的[CH-47直升機在](../Page/CH-47直升機.md "wikilink")[阿富汗中部](../Page/阿富汗.md "wikilink")[瓦尔达克省赛义德阿巴德地区执行清剿](../Page/瓦尔达克省.md "wikilink")[塔利班的军事行动中坠毁](../Page/塔利班.md "wikilink")，包括机组人员在内，造成至少31名美军官兵以及另外7名阿富汗军人丧生，致命伤极可能是一枚塔利班武装人员发射的[火箭推进榴弹](../Page/火箭推进榴弹.md "wikilink")。《[纽约时报](../Page/纽约时报.md "wikilink")》称，机上38人中包括22名海豹突击队特种兵，其中20人隶属于第六分队。英国《[每日邮报](../Page/每日邮报.md "wikilink")》则援引一名美军情报官员的话报道，本次阵亡官兵未参与[击毙賓拉登的军事行动](../Page/奥萨马·本·拉登之死.md "wikilink")。除了海豹部队的官兵外，机上美军还包括空中管制员与机组成员，据信其中有3名空军航管人员，1只军犬和它的训练员，以及1位文职通译和机组员。\[1\]\[2\]这起坠机事件是[2001年阿富汗战争爆发以来](../Page/阿富汗戰爭_\(2001年\).md "wikilink")，驻阿富汗国际部队死亡人数最多的单一事件。

## 流行文化

  - DEVGRU也有出現在遊戲《[-{zh-cn:反恐精英;zh-tw:絕對武力;}-](../Page/絕對武力.md "wikilink")》系列、《[榮譽勳章](../Page/榮譽勳章_\(2010年遊戲\).md "wikilink")》、《[-{zh-cn:荣誉勋章：战士;zh-tw:榮譽勳章：鐵血悍將;}-](../Page/榮譽勳章：鐵血悍將.md "wikilink")》及《[-{zh-cn:使命召唤：黑色行动II;zh-tw:決勝時刻：黑色行動II;}-](../Page/決勝時刻：黑色行動II.md "wikilink")》當中。

<!-- end list -->

  - 2012年上映的[電影](../Page/電影.md "wikilink")《[-{zh-cn:猎杀本·拉登;zh-hk:追擊拉登行動;zh-tw:00:30凌晨密令;}-](../Page/00:30凌晨密令.md "wikilink")》（Zero
    Dark Thirty）亦有記載DEVGRU參與[海神之矛行動時的情況](../Page/海神之矛行動.md "wikilink")。

<!-- end list -->

  - 由[快桅阿拉巴馬號劫持案改編而成的](../Page/快桅阿拉巴馬號劫持案.md "wikilink")2013年[電影](../Page/電影.md "wikilink")《[-{zh-cn:菲利普斯船长;zh-hk:盜海狙擊;zh-tw:怒海劫;}-](../Page/怒海劫.md "wikilink")》中重演了當時DEVGRU的隊員跟[索馬里海盗談判](../Page/索馬里海盗.md "wikilink")，最終擊斃救生艇内三名海盗並救回菲利普斯的整個過程。

<!-- end list -->

  - 《[艱難一日：海豹六隊擊斃本·拉登行動親歷](../Page/艱難一日：海豹六隊擊斃本·拉登行動親歷.md "wikilink")》（No
    Easy Day: The Firsthand Account of the Mission That Killed Osama bin
    Laden）一書中記載了DEVGRU參與海神之矛行動時許多不為人知的細節。

<!-- end list -->

  - 美國電視劇《》（Six）和《[海豹突擊隊](../Page/海豹突擊隊_\(電視劇\).md "wikilink")》（SEAL
    Team）以DEVGRU為主題。

## 参考文献

## 參見

  - [海豹部隊](../Page/海豹部隊.md "wikilink")
  - [陸軍特戰司令部](../Page/陸軍特戰司令部.md "wikilink")
  - [海軍特戰司令部](../Page/海軍特戰司令部.md "wikilink")

[Category:美國海軍單位與編制](../Category/美國海軍單位與編制.md "wikilink")
[Category:美國特種部隊](../Category/美國特種部隊.md "wikilink")
[Category:海軍特種部隊](../Category/海軍特種部隊.md "wikilink")
[Category:反恐單位](../Category/反恐單位.md "wikilink")
[Category:伊拉克戰爭中的軍隊編制](../Category/伊拉克戰爭中的軍隊編制.md "wikilink")

1.
2.