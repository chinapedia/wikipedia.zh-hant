**异丙醇钛**、**四异丙醇钛**是[钛](../Page/钛.md "wikilink")(IV)的[异丙醇](../Page/异丙醇.md "wikilink")[盐](../Page/醇盐.md "wikilink")，[化学式为Ti](../Page/化学式.md "wikilink"){OCH(CH<sub>3</sub>)<sub>2</sub>}<sub>4</sub>，用于[有机合成和](../Page/有机合成.md "wikilink")[材料科学中](../Page/材料科学.md "wikilink")。

异丙醇钛结构复杂。晶态时，异丙醇钛为四聚体，分子式为Ti<sub>4</sub>(OCH<sub>3</sub>)<sub>16</sub>。\[1\]非极性溶剂中不聚合，\[2\]为四面体型[反磁性分子](../Page/反磁性.md "wikilink")。

## 制备

异丙醇钛可由[四氯化钛和](../Page/四氯化钛.md "wikilink")[异丙醇反应制得](../Page/异丙醇.md "wikilink")，副产物为[氯化氢](../Page/氯化氢.md "wikilink")：

  -
    [TiCl<sub>4</sub>](../Page/四氯化钛.md "wikilink") + 4
    (CH<sub>3</sub>)<sub>2</sub>CHOH →
    Ti{OCH(CH<sub>3</sub>)<sub>2</sub>}<sub>4</sub> + 4 HCl

## 性质

异丙醇钛水解生成[二氧化钛](../Page/二氧化钛.md "wikilink")：

  -
    Ti{OCH(CH<sub>3</sub>)<sub>2</sub>}<sub>4</sub> + 2 H<sub>2</sub>O →
    TiO<sub>2</sub> + 4 (CH<sub>3</sub>)<sub>2</sub>CHOH

该反应是[溶胶-凝胶法合成含TiO](../Page/溶胶-凝胶.md "wikilink")<sub>2</sub>材料的基础。产物的特性由添加剂（如[乙酸](../Page/乙酸.md "wikilink")）、加水的量以及混合速度决定
\[3\]。

异丙醇钛用作[Sharpless不对称环氧化反应中的](../Page/Sharpless不对称环氧化反应.md "wikilink")[路易斯酸](../Page/路易斯酸.md "wikilink")、[Kulinkovich反应制备](../Page/Kulinkovich反应.md "wikilink")[三元环的催化剂](../Page/环丙烷.md "wikilink")，以及合成前手性[硫醚立体选择性氧化时所用的异丙醇钛衍生物催化剂](../Page/硫醚.md "wikilink")。\[4\]

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:醇盐](../Category/醇盐.md "wikilink")
[Category:钛化合物](../Category/钛化合物.md "wikilink")

1.  Wright D. A.; Williams, D. A. “The Crystal and Molecular Structure
    of Titanium Tetramethoxide” Acta Crystallographica 1968, Volume B24,
    pages 1107-1114.
2.  Bradley, D. C.; Mehrotra, R.; Rothwell, I.; Singh, A. “Alkoxo and
    Aryloxo Derivatives of Metals” Academic Press, San Diego, 2001. ISBN
    0121241408.
3.
4.  S. H. Zhao, O. Samuel, [H. B.
    Kagan](../Page/Henri_B._Kagan.md "wikilink") “Enantioelective
    Oxidation of a Sulfide:: (S)-(-)-Methyl p-Tolyl Sulfoxide” Organic
    Syntheses, Collected Volume 8, p.464
    (1993).http://www.orgsyn.org/orgsyn/pdfs/CV8P0464.pdf