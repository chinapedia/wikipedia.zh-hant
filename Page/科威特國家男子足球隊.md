**科威特國家足球隊**是[科威特的男子](../Page/科威特.md "wikilink")[足球代表隊](../Page/足球.md "wikilink")，由[科威特足球協會負責管轄](../Page/科威特足球協會.md "wikilink")。

## 歷史

科威特國家足球隊曾經進入過一次[世界盃足球賽會內賽](../Page/世界盃足球賽.md "wikilink")。在[西班牙主辦的](../Page/西班牙.md "wikilink")[1982年世界盃足球賽](../Page/1982年世界盃足球賽.md "wikilink")，他們成功在外圍賽第二輪壓倒[中國和](../Page/中國國家足球隊.md "wikilink")[沙地阿拉伯](../Page/沙地阿拉伯國家足球隊.md "wikilink")，以次輪首名與次名的[紐西蘭晉身世界盃決賽周](../Page/紐西蘭國家足球隊.md "wikilink")。在1982年世界盃決賽周中，科威特首圈分組賽被編排和[捷克斯洛伐克](../Page/捷克國家足球隊.md "wikilink")、[英格蘭和](../Page/英格蘭國家足球隊.md "wikilink")[法國同組](../Page/法國國家足球隊.md "wikilink")，科威特以1-1賽和捷克斯洛伐克，但是分別以1-4和0-1敗給法國和英格蘭，首圈出局。在與法國隊的比賽中，法國隊在臨完場前領先3-1，當時法國球員Maxime
Bossis在沒有科威特球員上前攔截的情況下為法國隊攻入第四球，但科威特球員指他們聽到裁判結束比賽的哨音，才沒有阻止法國隊的進攻。[蘇聯籍裁判](../Page/蘇聯.md "wikilink")[Stupar
Miroslav表示沒有鳴笛完場](../Page/Stupar_Miroslav.md "wikilink")，本來打算判法國的入球有效，科威特隊選手和職員馬上向裁判抗議，科威特足球協會主席[Fahad
Al-Ahmed Al-Jaber
Al-Sabah](../Page/Fahad_Al-Ahmed_Al-Jaber_Al-Sabah.md "wikilink")[王子甚至跳入場內](../Page/王子.md "wikilink")。後來裁判當場改判這個入球無效，比賽結果為法國勝3-1。隨後國際足球總會更改了判決，判定法國第4個入球有效，賽果也改為法國勝4-1，並對跳入球場內抗議的Fahad
Al-Ahmed Al-Jaber Al-Sabah王子判罰8000[英磅](../Page/英磅.md "wikilink")。

不過自1982年世界杯之後，科威特至今仍未再次晉級世界杯決賽周。在[2010年](../Page/2010年世界盃外圍賽.md "wikilink")、[2014年及](../Page/2014年世界盃外圍賽.md "wikilink")[2018年世界盃外圍賽](../Page/2018年世界盃外圍賽.md "wikilink")，科威特更連續三屆在首輪分組賽出局。

科威特在上世紀七、八十年代在[亞洲杯足球賽也有傑出的表現](../Page/亞洲杯足球賽.md "wikilink")，他們在[1972年首次打進決賽周](../Page/1972年亞洲盃足球賽.md "wikilink")，之後便在[1976](../Page/1976年亞洲盃足球賽.md "wikilink")、[1980](../Page/1980年亞洲盃足球賽.md "wikilink")、[1984和](../Page/1984年亞洲盃足球賽.md "wikilink")[1996的比賽進入前四強](../Page/1996年亞洲盃足球賽.md "wikilink")，在1976和1980的比賽均打入決賽，並在1980年的決賽，以3-0擊敗[南韓贏得首個亞洲杯冠軍](../Page/南韓國家足球隊.md "wikilink")。可惜近年科威特在亞洲杯表現一般，[2007年早在外圍賽已被淘汰](../Page/2007年亞洲杯足球賽.md "wikilink")，[2011年及](../Page/2011年亞洲杯足球賽.md "wikilink")[2015年雖打入決賽周](../Page/2015年亞洲杯足球賽.md "wikilink")，但皆在分組初賽三戰全敗小組末席出局。

科威特曾經於2000年亞洲杯外圍賽以20-0大破[不丹](../Page/不丹國家足球隊.md "wikilink")，打破當時國際賽最大比數的紀錄，直到翌年[澳大利亞以](../Page/澳大利亞國家足球隊.md "wikilink")31-0大破[美屬薩摩亞才將紀錄取代](../Page/美屬薩摩亞足球隊.md "wikilink")。同時科威特曾經參加[1980年莫斯科奧運會](../Page/1980年夏季奧林匹克運動會.md "wikilink")，該國的青年隊也參與[1992年巴塞隆納奧運會和](../Page/1992年夏季奧林匹克運動會.md "wikilink")[2000年雪梨奧運會](../Page/2000年夏季奧林匹克運動會.md "wikilink")。

### 停權

科威特足球隊曾多次因科威特政府被指干涉科威特足協運作，而被國際足協禁賽。2007年10月30日，科威特足協因為被指政府干涉，被國際足協取消會籍個多星期。2008年10月24日，科威特足協因未能及時進行新一屆的選舉，再次被國際足協取消會籍兩個月。科威特足協會籍被取消期間，科威特國家足球隊及科威特聯賽球隊，皆被禁止參加國際賽。

2015年10月16日，因國際足協不承認科威特通過的最新體育法，科威特足協會籍再次被取消，當時正值2018年世界杯亞洲區第二圈外圍賽進行期間，科威特國家足球隊亦因此被禁止繼續參賽。同時科威特國家足球隊的世界排名亦因整整一年沒有進行任何國際賽，至2016年10月下滑至211會員國中的第167名，甚至被2016年5月3日才加入國際足協的[科索沃所超越](../Page/科索沃國家足球隊.md "wikilink")。

2017年1月，因國際足協尚未恢復科威特足協的會籍，科威特國家足球隊喪失[2019年亞洲盃外圍賽第三圈賽事的參賽資格](../Page/2019年亞洲盃資格賽第三輪.md "wikilink")。

2017年12月6日，科威特通過符合國際足協要求的新體育法，國際足協隨即恢復科威特足協會籍，科威特國家足球隊可參與國際賽事。因球隊超過兩年缺席所有國際賽，國際排名已跌至第189名。

## 世界盃成績

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/世界盃足球賽.md" title="wikilink">世界盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
</tr>
</tbody>
</table>

## 亞洲盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>總排名</p></th>
<th><p>場次</p></th>
<th><p>勝</p></th>
<th><p>和*</p></th>
<th><p>負</p></th>
<th><p>得球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1956年亞洲盃足球賽.md" title="wikilink">1956年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年亞洲盃足球賽.md" title="wikilink">1960年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年亞洲盃足球賽.md" title="wikilink">1964年</a></p></td>
<td><p><em>並未參加</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年亞洲盃足球賽.md" title="wikilink">1968年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年亞洲盃足球賽.md" title="wikilink">1972年</a></p></td>
<td><p>第一圈</p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年亞洲盃足球賽.md" title="wikilink">1976年</a></p></td>
<td><p>亞軍</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年亞洲盃足球賽.md" title="wikilink">1980年</a></p></td>
<td><p>冠軍</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>13</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984年亞洲盃足球賽.md" title="wikilink">1984年</a></p></td>
<td><p>季軍</p></td>
<td><p>3</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年亞洲盃足球賽.md" title="wikilink">1988年</a></p></td>
<td><p>第一圈</p></td>
<td><p>7</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992年亞洲盃足球賽.md" title="wikilink">1992年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1996年亞洲盃足球賽.md" title="wikilink">1996年</a></p></td>
<td><p>殿軍</p></td>
<td><p>4</p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>9</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年亞洲盃足球賽.md" title="wikilink">2000年</a></p></td>
<td><p>八強</p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年亞洲盃足球賽.md" title="wikilink">2004年</a></p></td>
<td><p>第一圈</p></td>
<td><p>10</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年亞洲盃足球賽.md" title="wikilink">2007年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年亞洲盃足球賽.md" title="wikilink">2011年</a></p></td>
<td><p>第一圈</p></td>
<td><p>14</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015年亞洲盃足球賽.md" title="wikilink">2015年</a></p></td>
<td><p>第一圈</p></td>
<td><p>15</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2019年亞洲盃足球賽.md" title="wikilink">2019年</a></p></td>
<td><p>被禁止參賽</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><strong>總結</strong></p></td>
<td><p>10/16</p></td>
<td><p>-</p></td>
<td><p>42</p></td>
<td><p>15</p></td>
<td><p>10</p></td>
<td><p>17</p></td>
<td><p>47</p></td>
<td><p>51</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [科威特足球協會官方網站](http://www.kfa.org.kw/)

[Category:科威特足球](../Category/科威特足球.md "wikilink")
[Category:亞洲足球代表隊](../Category/亞洲足球代表隊.md "wikilink")
[Kuwait](../Category/亚洲杯足球赛冠军队伍.md "wikilink")