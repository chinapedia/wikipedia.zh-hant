**虎門鎮**為[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[东莞市下辖之一个](../Page/东莞市.md "wikilink")[鎮](../Page/行政建制鎮.md "wikilink")\[1\]，座落东莞市西南部、[珠江口东岸](../Page/珠江口.md "wikilink")，虎门鎮面积为178.5[平方公里](../Page/平方公里.md "wikilink")，虎门镇下辖30个[社区居民委员会](../Page/社区居民委员会.md "wikilink")，居住人口約65万人，其中[户籍人口约](../Page/户籍.md "wikilink")12.4万人，外來人口近45万人。虎门是[林则徐](../Page/林则徐.md "wikilink")[销烟的地方](../Page/虎門銷煙.md "wikilink")，也是[鸦片战争的重要战场](../Page/鸦片战争.md "wikilink")。1843年，《[虎门条约](../Page/虎门条约.md "wikilink")》在这里签订。

虎门镇虽然是一个[鎮](../Page/行政建制鎮.md "wikilink")，但无论经济实力还是常住人口都已达到内陆之[中等城市的标准](../Page/中华人民共和国城市建制.md "wikilink")。
2015年，中国[国家发改委印发](../Page/国家发改委.md "wikilink")《国家新型城镇化综合试点方案》，[广州](../Page/广州.md "wikilink")、[东莞等地列入试点范围](../Page/东莞.md "wikilink")。其中东莞将试点虎门镇和[长安镇](../Page/长安镇_\(东莞市\).md "wikilink")[撤镇设市](../Page/撤镇设市.md "wikilink")，赋予两市县级管理权限，但不配齐[四套班子](../Page/四套班子.md "wikilink")，不设[街道办](../Page/街道办.md "wikilink")。

## 历史

虎门本义指[珠江从](../Page/珠江.md "wikilink")[狮子洋通往](../Page/狮子洋.md "wikilink")[伶仃洋的出海口](../Page/伶仃洋.md "wikilink")，即[虎门水道](../Page/虎门水道.md "wikilink")，后多泛指虎门水道及其两岸的沿江陆地，其地先后由[番禺县](../Page/番禺县.md "wikilink")、宝安县、东莞县辖。后东岸归东莞县，西岸则归番禺县。虎门炮台、虎门销烟处亦位于东岸，虎门一词逐渐专指虎门水道东岸。

早於1637年，[英国](../Page/英国.md "wikilink")[查理一世派遣威德尔率領五艘商船到达](../Page/查理一世_\(英格蘭\).md "wikilink")[虎门亚娘鞋](../Page/虎门.md "wikilink")（Anunghai）停泊，並提出貿易要求，但被[明思宗拒絕](../Page/明思宗.md "wikilink")。\[2\]

1636年9月19日，威德尔在虎门地区将三艘中国帆船并一个市镇焚毁，抢夺30头猪。21日又攻占并炸毁虎门亚娘鞋炮台，焚毁了大帆船一艘，因觉得不好同中国打交道，便将船队驶往[澳门](../Page/澳门.md "wikilink")，请求[葡萄牙人出面调解](../Page/葡萄牙.md "wikilink")。\[3\]

11月22日英商在[广州答应中国的要求](../Page/广州.md "wikilink")，赔偿白银2,800两。30日威德尔向中国官员提交了一份保证完成贸易后即行离去的文书，对虎门事件表示歉意。据此，广州官员令其贸易后尽快离境不予追究。\[4\]。

[香港开埠后](../Page/香港开埠.md "wikilink")，太平（太平墟）正好位于广州至香港的中间，遂逐渐繁荣起来，并设太平镇。而虎门一词又常指太平与虎门水道之间的区域。

1958年10月1日，虎门、北栅、长安、沙田、太平等乡镇，合并成立虎门人民公社。其后，太平、长安、沙田等地先后自虎门分出，虎门公社（虎门乡）成为太平镇周的农村区域。1985年虎门乡与太平镇合并设虎门镇，驻太平。1998年新湾镇并入虎门镇。\[5\]

## 地理

虎门镇位于东莞市西南部，北邻[沙田鎮](../Page/沙田鎮_\(東莞市\).md "wikilink")、[厚街镇](../Page/厚街镇.md "wikilink")，东临[长安镇](../Page/长安镇.md "wikilink")，西隔[虎门水道与](../Page/虎门水道.md "wikilink")[广州市](../Page/广州市.md "wikilink")[南沙区](../Page/南沙区.md "wikilink")[南沙街道相邻](../Page/南沙街道.md "wikilink")，之间有[虎门大桥相通](../Page/虎门大桥.md "wikilink")，南部为珠江口[交椅湾](../Page/交椅湾.md "wikilink")。

## 交通

### 公路

  - [广深高速公路](../Page/广深高速公路.md "wikilink")、[107国道](../Page/107国道.md "wikilink")、[256省道](../Page/256省道.md "wikilink")、[广深沿江高速公路](../Page/广深沿江高速公路.md "wikilink")

### 铁路

  - [广深港高速铁路](../Page/广深港高速铁路.md "wikilink")、[穗莞深城際轨道交通](../Page/穗莞深城際轨道交通.md "wikilink")、[东莞轨道交通2号线](../Page/东莞轨道交通2号线.md "wikilink")、[深茂铁路](../Page/深茂铁路.md "wikilink")

### 港口

  - [虎门港](../Page/虎门港.md "wikilink")

### 航空

  - [深圳宝安国际机场位于虎门镇东南方](../Page/深圳宝安国际机场.md "wikilink")30公里处。\[6\]

## 经济

虎门以服装业为支柱产业，是中国经济实力最强的镇之一，被誉为“中国第一镇”。
[缩略图](https://zh.wikipedia.org/wiki/File:Kwang-jeou_Hu-man_courageous_battle_poster.jpg "fig:缩略图")
2009年虎门逆势发展，GDP达243.76亿元，增长13.5%。 2011年GDP347.38亿元，继续排名东莞市32个镇街第一名。

## 旅遊景點

  - [虎门炮台旧址](../Page/虎门炮台.md "wikilink")
  - [威远炮台](../Page/威远炮台.md "wikilink")
  - [沙角炮台](../Page/沙角炮台.md "wikilink")
  - [虎门广場](../Page/虎门广場.md "wikilink")
  - [虎門大橋](../Page/虎門大橋.md "wikilink")
  - [虎门公园](../Page/虎门公园.md "wikilink")
  - [鸦片战争博物馆](../Page/鸦片战争博物馆.md "wikilink")
  - [海战博物馆](../Page/海战博物馆.md "wikilink")
  - [荔荫园](../Page/荔荫园.md "wikilink")（[蔣光鼐的故居](../Page/蔣光鼐.md "wikilink")）
  - [逆水流龟村堡](../Page/逆水流龟村堡.md "wikilink")
  - [虎门石洞森林公园](../Page/虎门石洞森林公园.md "wikilink")
  - [黄河商业中心](../Page/黄河商业中心.md "wikilink")

## 教育

  - [东莞市虎门中学](../Page/东莞市虎门中学.md "wikilink")
  - [东莞市威遠職業高級中學](../Page/东莞市威遠職業高級中學.md "wikilink")
  - [虎门外語学校](../Page/虎门外語学校.md "wikilink")

## 医疗

  - 东莞市虎门医院
  - [东莞市太平人民医院](../Page/东莞市太平人民医院.md "wikilink")

## 發電廠

  - [沙角發電廠](../Page/沙角發電廠.md "wikilink")

## 名人

  - [蔣光鼐](../Page/蔣光鼐.md "wikilink")
  - [王匡](../Page/王匡.md "wikilink")
  - [王煜初](../Page/王煜初.md "wikilink")
  - [王寵惠](../Page/王寵惠.md "wikilink")
  - [王寵佑](../Page/王寵佑.md "wikilink")
  - [王吉民](../Page/王吉民.md "wikilink")
  - [王應榆](../Page/王應榆.md "wikilink")
  - [盧子樞](../Page/盧子樞.md "wikilink")
  - [盧禮屏](../Page/盧禮屏.md "wikilink")
  - [盧惠卿](../Page/盧惠卿.md "wikilink")
  - [何儒](../Page/何儒.md "wikilink")
  - [陳益](../Page/陳益.md "wikilink")
  - [蘇聰](../Page/蘇聰.md "wikilink")

## 参见

  - [镇级市](../Page/镇级市.md "wikilink")

## 行政区划

虎门镇下辖以下地区：\[7\]

。

## 参考资料

[Category:东莞建制镇](../Category/东莞建制镇.md "wikilink")

1.

2.

3.
4.

5.  [大事记](http://www.humen.gov.cn/rshumen/zj-st0121.asp)

6.

7.