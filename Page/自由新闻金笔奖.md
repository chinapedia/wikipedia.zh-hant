**自由新闻金笔奖**（）是一項在世界[新闻业中享有声誉的奖项](../Page/新闻业.md "wikilink")。此獎項创立于1961年，每年由[世界报业协会颁发给个人或组织](../Page/世界报业协会.md "wikilink")。此獎項旨在表彰「以文字或行動的方式捍卫和促进[新闻自由的個人](../Page/新闻自由.md "wikilink")、團體或機構」的杰出贡献\[1\]。獎項的其中一個目標是「把公眾關注的焦點轉到專制政府打壓記者上」，並向受到一定程度迫害的記者提供保護。\[2\]

此獎項每年頒發一次，頒獎典禮在世界報業大會和世界編輯論壇的開幕儀式中舉辦。

## 概述

每年6月，一至三個人、團體或機構會在[德國](../Page/德國.md "wikilink")[達姆施塔特的世界報業大會和世界編輯論壇開幕儀式中獲頒此獎項](../Page/達姆施塔特.md "wikilink")\[3\]。首位自由新闻金笔奖的得獎者為1961年獲獎的[土耳其記者和作家](../Page/土耳其.md "wikilink")\[4\]。於2013年，第49屆自由新闻金笔奖頒獎典禮舉辦成功，[丹图昂博士獲頒此獎項](../Page/丹图昂.md "wikilink")。\[5\]\[6\]。自由新闻金笔奖亦曾頒予新聞機構。截至2013年，共有4間機構獲頒此獎項。這些機構分別是1997年獲獎的《[我們的鬥爭](../Page/我們的鬥爭.md "wikilink")》、《》和《》\[7\]，以及2003年獲獎的\[8\]。另外，自由新闻金笔奖亦曾授予一群人和機構。於1969年，獲頒此獎項，代表整個[捷克斯洛伐克的新聞工作者均獲頒此獎項](../Page/捷克斯洛伐克.md "wikilink")。可是，捷克斯洛伐克的媒體於22年後的1991年，即[冷戰結束後](../Page/冷戰.md "wikilink")，才能夠正式派遣代表接受獎項。\[9\]

有時候，被監禁的獲獎者會於另一個頒獎典禮上獲獎。其中的例子包括中國的[高瑜和](../Page/高瑜.md "wikilink")[师涛](../Page/师涛.md "wikilink")。高瑜於1995年獲獎，但她在一年前已被逮捕入獄。於1999年，好以保外就醫的名義獲釋，才能獲頒自由新闻金笔奖。\[10\]而师涛則於2007年獲獎，但他在2004年被逮捕，並於翌年入獄。他於2013年8月23日獲釋，才能獲頒自由新闻金笔奖。\[11\]\[12\]

自由新闻金笔奖的其中兩位獲獎者均為追授。他們包括於1978年逝世的法國記者（1979年）\[13\]和於1987年遭暗殺的[巴勒斯坦諷刺漫畫家](../Page/巴勒斯坦.md "wikilink")[納吉·阿里](../Page/納吉·阿里.md "wikilink")（1988年）\[14\]\[15\]這兩位得獎者都是在死後一年獲頒此獎項。

[中華人民共和國和](../Page/中華人民共和國.md "wikilink")[緬甸擁有最多自由新闻金笔奖得主](../Page/緬甸.md "wikilink")，各有四位。中華人民共和國的得主為[戴晴](../Page/戴晴.md "wikilink")、高瑜、師濤和[李长青](../Page/李长青_\(记者\).md "wikilink")，而緬甸的得主則為、[温丁](../Page/温丁.md "wikilink")、和[丹图昂](../Page/丹图昂.md "wikilink")。\[16\]

## 历年获奖者

[Dai_Qing.jpg](https://zh.wikipedia.org/wiki/File:Dai_Qing.jpg "fig:Dai_Qing.jpg")\]\]
[Pius_Njawe.jpg](https://zh.wikipedia.org/wiki/File:Pius_Njawe.jpg "fig:Pius_Njawe.jpg")\]\]
[Gao_yu_VOA.jpg](https://zh.wikipedia.org/wiki/File:Gao_yu_VOA.jpg "fig:Gao_yu_VOA.jpg")\]\]
[Win_Tin.jpg](https://zh.wikipedia.org/wiki/File:Win_Tin.jpg "fig:Win_Tin.jpg")\]\]
[Akbar_Ganji_in_Chicago.jpg](https://zh.wikipedia.org/wiki/File:Akbar_Ganji_in_Chicago.jpg "fig:Akbar_Ganji_in_Chicago.jpg")\]\]
[Anabel_Hernandez.jpg](https://zh.wikipedia.org/wiki/File:Anabel_Hernandez.jpg "fig:Anabel_Hernandez.jpg")

以下為自由新闻金笔奖的得獎者列表。\[17\]

<table>
<caption>註釋</caption>
<tbody>
<tr class="odd">
<td></td>
<td><p>追授</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得獎者</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1961</p></td>
<td></td>
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a></p></td>
</tr>
<tr class="even">
<td><p>1963</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Burma_(1948-1974).svg" title="fig:Flag_of_Burma_(1948-1974).svg">Flag_of_Burma_(1948-1974).svg</a> <a href="../Page/緬甸.md" title="wikilink">緬甸</a></p></td>
</tr>
<tr class="odd">
<td><p>1964</p></td>
<td><p><a href="../Page/加百列·馬科索.md" title="wikilink">加百列·馬科索</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Congo-Léopoldville_(1963-1966).svg" title="fig:Flag_of_Congo-Léopoldville_(1963-1966).svg">Flag_of_Congo-Léopoldville_(1963-1966).svg</a> <a href="../Page/剛果民主共和國.md" title="wikilink">剛果民主共和國</a></p></td>
</tr>
<tr class="even">
<td><p>1965</p></td>
<td><p><a href="../Page/埃斯蒙德·威克雷梅斯.md" title="wikilink">埃斯蒙德·威克雷梅斯</a></p></td>
<td><p><a href="../Page/斯里蘭卡.md" title="wikilink">斯里蘭卡</a></p></td>
</tr>
<tr class="odd">
<td><p>1966</p></td>
<td></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p>1967</p></td>
<td><p><a href="../Page/穆赫塔爾·盧比斯.md" title="wikilink">穆赫塔爾·盧比斯</a></p></td>
<td><p><a href="../Page/印尼.md" title="wikilink">印尼</a></p></td>
</tr>
<tr class="odd">
<td><p>1968</p></td>
<td></td>
<td><p><a href="../Page/希臘.md" title="wikilink">希臘</a></p></td>
</tr>
<tr class="even">
<td><p>1969</p></td>
<td></td>
<td><p><a href="../Page/捷克斯洛伐克.md" title="wikilink">捷克斯洛伐克</a></p></td>
</tr>
<tr class="odd">
<td><p>1970</p></td>
<td></td>
<td><p><a href="../Page/阿根廷.md" title="wikilink">阿根廷</a></p></td>
</tr>
<tr class="even">
<td><p>1972</p></td>
<td><p><a href="../Page/羽贝尔·波夫梅西.md" title="wikilink">羽贝尔·波夫梅西</a></p></td>
<td><p><a href="../Page/法國.md" title="wikilink">法國</a></p></td>
</tr>
<tr class="odd">
<td><p>1973</p></td>
<td><p><a href="../Page/安東·貝茨.md" title="wikilink">安東·貝茨</a></p></td>
<td><p><a href="../Page/德國.md" title="wikilink">德國</a></p></td>
</tr>
<tr class="even">
<td><p>1974</p></td>
<td><p><a href="../Page/胡里奧·德·梅斯基塔·內托.md" title="wikilink">胡里奧·德·梅斯基塔·內托</a></p></td>
<td><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
</tr>
<tr class="odd">
<td><p>1975</p></td>
<td><p><a href="../Page/金相文.md" title="wikilink">金相文</a></p></td>
<td><p><a href="../Page/南韓.md" title="wikilink">南韓</a></p></td>
</tr>
<tr class="even">
<td><p>1976</p></td>
<td><p><a href="../Page/勞爾·雷戈.md" title="wikilink">勞爾·雷戈</a></p></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a></p></td>
</tr>
<tr class="odd">
<td><p>1977</p></td>
<td><p><a href="../Page/羅伯特·黑·利雷.md" title="wikilink">羅伯特·黑·利雷</a></p></td>
<td><p><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a></p></td>
</tr>
<tr class="even">
<td><p>1978</p></td>
<td><p>、<a href="../Page/珀西·戈博扎.md" title="wikilink">珀西·戈博扎</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_South_Africa_(1928-1994).svg" title="fig:Flag_of_South_Africa_(1928-1994).svg">Flag_of_South_Africa_(1928-1994).svg</a> <a href="../Page/南非.md" title="wikilink">南非</a></p></td>
</tr>
<tr class="odd">
<td><p>1979</p></td>
<td><p></p></td>
<td><p><a href="../Page/法國.md" title="wikilink">法國</a></p></td>
</tr>
<tr class="even">
<td><p>1980</p></td>
<td></td>
<td><p><a href="../Page/阿根廷.md" title="wikilink">阿根廷</a></p></td>
</tr>
<tr class="odd">
<td><p>1981</p></td>
<td><p><a href="../Page/何塞·哈維爾·烏蘭加.md" title="wikilink">何塞·哈維爾·烏蘭加</a></p></td>
<td><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a></p></td>
</tr>
<tr class="even">
<td><p>1982</p></td>
<td><p><a href="../Page/華金·查莫羅·巴里奧斯.md" title="wikilink">華金·查莫羅·巴里奧斯</a></p></td>
<td><p><a href="../Page/尼加拉瓜.md" title="wikilink">尼加拉瓜</a></p></td>
</tr>
<tr class="odd">
<td><p>1985</p></td>
<td><p><a href="../Page/華金·羅塞斯.md" title="wikilink">華金·羅塞斯</a></p></td>
<td><p><a href="../Page/菲律賓.md" title="wikilink">菲律賓</a></p></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td><p><a href="../Page/安東尼·赫迪.md" title="wikilink">安東尼·赫迪</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_South_Africa_(1928-1994).svg" title="fig:Flag_of_South_Africa_(1928-1994).svg">Flag_of_South_Africa_(1928-1994).svg</a> <a href="../Page/南非.md" title="wikilink">南非</a></p></td>
</tr>
<tr class="odd">
<td><p>1987</p></td>
<td><p><a href="../Page/胡安·巴勃羅·卡德納斯.md" title="wikilink">胡安·巴勃羅·卡德納斯</a></p></td>
<td><p><a href="../Page/智利.md" title="wikilink">智利</a></p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/納吉·阿里.md" title="wikilink">納吉·阿里</a></p></td>
<td><p><a href="../Page/巴勒斯坦.md" title="wikilink">巴勒斯坦</a></p></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/謝爾蓋·格里戈良茨.md" title="wikilink">謝爾蓋·格里戈良茨</a></p></td>
<td><p><a href="../Page/蘇聯.md" title="wikilink">蘇聯</a></p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td></td>
<td><p><a href="../Page/哥倫比亞.md" title="wikilink">哥倫比亞</a></p></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td></td>
<td><p><a href="../Page/肯尼亞.md" title="wikilink">肯尼亞</a></p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/戴晴.md" title="wikilink">戴晴</a></p></td>
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/皮烏斯·恩佐.md" title="wikilink">皮烏斯·恩佐</a></p></td>
<td><p><a href="../Page/喀麥隆.md" title="wikilink">喀麥隆</a></p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/奧馬爾·貝爾侯切.md" title="wikilink">奧馬爾·貝爾侯切</a></p></td>
<td><p><a href="../Page/阿爾及利亞.md" title="wikilink">阿爾及利亞</a></p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/高瑜.md" title="wikilink">高瑜</a></p></td>
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/因達米羅·雷斯塔諾·迪亞斯.md" title="wikilink">因達米羅·雷斯塔諾·迪亞斯</a></p></td>
<td><p><a href="../Page/古巴.md" title="wikilink">古巴</a></p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p>《<a href="../Page/我們的鬥爭.md" title="wikilink">我們的鬥爭</a>》、《》、《》</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Serbia_(1992-2004).svg" title="fig:Flag_of_Serbia_(1992-2004).svg">Flag_of_Serbia_(1992-2004).svg</a> <a href="../Page/南斯拉夫聯邦共和國.md" title="wikilink">南斯拉夫聯邦共和國</a>、<a href="../Page/克羅地亞.md" title="wikilink">克羅地亞</a>、<a href="../Page/波斯尼亞和黑塞哥維那.md" title="wikilink">波斯尼亞和黑塞哥維那</a></p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/段曰活.md" title="wikilink">段曰活</a></p></td>
<td><p><a href="../Page/越南.md" title="wikilink">越南</a></p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td></td>
<td><p><a href="../Page/伊朗.md" title="wikilink">伊朗</a></p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/尼扎爾·納尤福.md" title="wikilink">尼扎爾·納尤福</a></p></td>
<td><p><a href="../Page/敘利亞.md" title="wikilink">敘利亞</a></p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/温丁.md" title="wikilink">温丁</a>、</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Myanmar_(1974-2010).svg" title="fig:Flag_of_Myanmar_(1974-2010).svg">Flag_of_Myanmar_(1974-2010).svg</a> <a href="../Page/緬甸.md" title="wikilink">緬甸</a></p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td></td>
<td><p><a href="../Page/津巴布韋.md" title="wikilink">津巴布韋</a></p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td></td>
<td><p><a href="../Page/白俄羅斯.md" title="wikilink">白俄羅斯</a></p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/魯斯蘭·沙裡波夫.md" title="wikilink">魯斯蘭·沙裡波夫</a></p></td>
<td><p><a href="../Page/烏茲別克斯坦.md" title="wikilink">烏茲別克斯坦</a></p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td></td>
<td><p><a href="../Page/蘇丹.md" title="wikilink">蘇丹</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/阿克巴尔·甘吉.md" title="wikilink">阿克巴尔·甘吉</a></p></td>
<td><p><a href="../Page/伊朗.md" title="wikilink">伊朗</a></p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/师涛.md" title="wikilink">师涛</a></p></td>
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/李長青_(記者).md" title="wikilink">李长青</a></p></td>
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/納詹姆·塞提.md" title="wikilink">納詹姆·塞提</a></p></td>
<td><p><a href="../Page/巴基斯坦.md" title="wikilink">巴基斯坦</a></p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/艾哈迈德·扎伊达巴迪.md" title="wikilink">艾哈迈德·扎伊达巴迪</a></p></td>
<td><p><a href="../Page/伊朗.md" title="wikilink">伊朗</a></p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td></td>
<td><p><a href="../Page/厄立特里亞.md" title="wikilink">厄立特里亞</a>、<a href="../Page/瑞典.md" title="wikilink">瑞典</a></p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td></td>
<td><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a></p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/丹图昂.md" title="wikilink">丹图昂</a></p></td>
<td><p><a href="../Page/緬甸.md" title="wikilink">緬甸</a></p></td>
</tr>
</tbody>
</table>

## 參見

  - [国际新闻自由奖](../Page/国际新闻自由奖.md "wikilink")
  - [新聞自由指數](../Page/新聞自由指數.md "wikilink")
  - [自由指數列表](../Page/自由指數列表.md "wikilink")

## 參考文獻

## 外部链接

  - [自由新闻金笔奖官方網站](http://www.wan-ifra.org/microsites/golden-pen-of-freedom)

[Z](../Category/新闻奖项.md "wikilink")
[Category:1961年建立](../Category/1961年建立.md "wikilink")
[\*](../Category/自由新聞金筆獎得主.md "wikilink")

1.

2.  [About the Golden Pen of
    Freedom](http://www.wan-ifra.org/articles/2011/03/24/about-the-golden-pen-of-freedom)

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13. *Histoire générale de la presse française*. De 1940 à 1968, t. IV,
    [PUF](../Page/Presses_universitaires_de_France.md "wikilink")

14.

15. Kreitmeyr, Nadine (2012). *Der Nahostkonflikt durch die Augen
    Hanzalas. Stereotypische Vorstellungen im Schaffen des
    Karikaturisten Naji al-'Ali*. Berlin, Klaus Schwarz. ISBN
    978-3-87997-402-3

16.
17.