**西追·迪哥里**（），是[英國作家](../Page/英國.md "wikilink")[喬安·凱瑟琳·羅琳的兒童](../Page/喬安·凱瑟琳·羅琳.md "wikilink")[奇幻小說](../Page/奇幻小說.md "wikilink")《[哈利波特](../Page/哈利波特.md "wikilink")》系列中的人物\[1\]\[2\]。[赫夫帕夫級長](../Page/赫夫帕夫.md "wikilink")、[魁地奇代表隊新隊長與搜捕手](../Page/魁地奇.md "wikilink")，是十分優秀的魁地奇球員。第四集升上六年級時脫穎而出成為[三巫鬥法大賽的](../Page/三巫鬥法大賽.md "wikilink")[霍格華茲代表鬥士](../Page/霍格華茲.md "wikilink")，最後在[佛地魔命令下被](../Page/佛地魔.md "wikilink")[蟲尾殺害](../Page/蟲尾.md "wikilink")。

## 名字由来

“西追”（cedric）這個名字取自於[凯尔特语chieftain](../Page/凯尔特语.md "wikilink")，意思是“领导者”。

而「迪哥里」（Diggory）可能是取自於奇幻文學系列《[納尼亞傳奇](../Page/納尼亞傳奇.md "wikilink")》人物（Digory
Kirke）的名字\[3\]。

## 容貌及個性

小說裡敘述西追是一个非常英俊、长着高鼻梁、黑头发、灰瞳孔、充满理智、具有才华、有着优雅风度的男生。同时也是一名优秀、执拗且富有正义感的学生。人们称赞他有着“人们对于霍格華茲的学生期望中的一切。”

## 其他資料

  - [魔杖](../Page/魔杖.md "wikilink")：十二又四分之一英寸长，芯是[独角兽尾毛](../Page/独角兽.md "wikilink")，黄芩木。
  - [星座](../Page/星座.md "wikilink")：[天秤座](../Page/天秤座.md "wikilink")
  - 擅长：[魁地奇](../Page/魁地奇.md "wikilink")，[变形学](../Page/变形学.md "wikilink")
  - 亲人：[阿默·迪戈里](../Page/阿默·迪戈里.md "wikilink")（父亲） 和母亲
  - 朋友：[秋·张](../Page/秋·张.md "wikilink")（女友）、哈利波特（三巫鬥法大賽盟友）

## 主要出场经历

第三集《》参加魁地奇比赛与[葛兰芬多对决](../Page/葛來分多.md "wikilink")，因突如其来的[催狂魔使哈利从扫帚上掉下来](../Page/催狂魔.md "wikilink")，讓他抓住金探子而赢得比赛。这后来还成为父亲夸耀儿子的资本。

第四集《》開頭與父親、[妙麗](../Page/妙麗·格蘭傑.md "wikilink")、哈利及[衛斯理家觀賞魁地奇世界盃](../Page/衛斯理家族.md "wikilink")，回到學校後被火杯選中代表霍格華茲参加三巫鬥法大賽，当哈利成为第四名勇士时，别的学院学生都因误解哈利，認為他使用卑鄙手段而佩戴“支持西追迪哥里/波特大爛人”徽章，他劝朋友们摘下来並叫哈利不要放在心上，顯見他的善良。

当哈利將第一關内容告诉自己后，為求感謝而把如何得知第二關内容的方法转告给他，以求公平竞争。

在第三關中，他眼看第一个夺得奖杯，却不幸被巨蜘蛛缠上，当哈利帮他摆脱后，认为哈利才是最终获得者，于是他“用了全部的毅力”懇求哈利拿下奖杯，放弃了“赫夫帕夫数百年来未曾得到过的荣誉”。最後兩人決定共同拿起奖杯，但奖杯却被偷换成[港口钥](../Page/港口钥.md "wikilink")，将他们帶到小漢果頓，在此等候已久的[佛地魔命令蟲尾](../Page/佛地魔.md "wikilink")「把多餘的人殺掉」，蟲尾用索命咒將西追残忍的杀害，成为哈利与佛地魔斗争的牺牲品。

隨後佛地魔與哈利對戰過程中雙方[魔杖出現奇特反應](../Page/魔杖.md "wikilink")，所有被他殺掉的人的靈魂都顯現出來，此時西追請求哈利將他的屍體帶回父母身邊，最後全校為他默哀舉辦喪禮。

第五集《》中，因[魔法部不承认佛地魔已复活的事实](../Page/魔法部.md "wikilink")，於是把西追的死说成是一场意外事故，又由于三巫鬥法大賽的迷宫内与小漢果頓墓地都没有目击者，因此魔法世界無論如何也难以相信西追惨遭伏地魔毒手这一事实，但是到最後卻被哈利所領導的鄧不利多軍把事實翻盤，讓魔法部不得不正視黑魔王復活的這件事\!

這是哈利首次亲眼目睹死亡（母親之死不算在親身經歷之中），因此能看见騎士墜鬼馬。

## 邓不利多的评价

霍格華茲[校長](../Page/校長.md "wikilink")[邓不利多在学期末的离校宴会上对西追深深表示哀悼之意](../Page/邓不利多.md "wikilink")，並向全體學生訓示：“西追充分体现了赫夫帕夫所特有的品质，是值得大家学习的楷模。”“他是一位善良、忠诚的朋友，崇尚公平竞争，他的死使大家受到了震撼，无论认识他与否。”“请永远记住西追，当你们不得不在正道和捷径之间作出选择时，请不要忘记一个正直、善良、勇敢的男孩所遭遇到的。”

同時也以這段訓示呼籲在場其他友校師生不分國籍種族，無論如何都要謹記西追的死，並團結一致抵抗佛地魔。

## 註釋

## 參考資料

[da:Bipersoner i Harry Potter-universet\#Cedric
Diggory](../Page/da:Bipersoner_i_Harry_Potter-universet#Cedric_Diggory.md "wikilink")
[de:Figuren der Harry-Potter-Romane\#Cedric
Diggory](../Page/de:Figuren_der_Harry-Potter-Romane#Cedric_Diggory.md "wikilink")
[en:List of supporting Harry Potter characters\#Cedric
Diggory](../Page/en:List_of_supporting_Harry_Potter_characters#Cedric_Diggory.md "wikilink")
[es:Anexo:Personajes secundarios de Harry Potter\#Cedric
Diggory](../Page/es:Anexo:Personajes_secundarios_de_Harry_Potter#Cedric_Diggory.md "wikilink")
[he:הארי פוטר - דמויות משנה\#תלמידים
בהוגוורטס](../Page/he:הארי_פוטר_-_דמויות_משנה#תלמידים_בהוגוורטס.md "wikilink")
[hu:Roxforti diákok\#Cedric
Diggory](../Page/hu:Roxforti_diákok#Cedric_Diggory.md "wikilink")
[no:Elever i Håsblås\#Fredrik
Djervell](../Page/no:Elever_i_Håsblås#Fredrik_Djervell.md "wikilink")
[pl:Puchoni\#Cedrik
Diggory](../Page/pl:Puchoni#Cedrik_Diggory.md "wikilink") [ru:Седрик
Диггори](../Page/ru:Седрик_Диггори.md "wikilink") [tr:Yardımcı Harry
Potter karakterleri listesi\#Cedric
Diggory](../Page/tr:Yardımcı_Harry_Potter_karakterleri_listesi#Cedric_Diggory.md "wikilink")
[vi:Danh sách nhân vật phụ trong truyện Harry Potter\#Cedric
Diggory](../Page/vi:Danh_sách_nhân_vật_phụ_trong_truyện_Harry_Potter#Cedric_Diggory.md "wikilink")

[Category:哈利波特人物](../Category/哈利波特人物.md "wikilink")
[Category:虛構魔法師](../Category/虛構魔法師.md "wikilink")

1.
2.
3.