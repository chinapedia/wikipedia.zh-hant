**津山市**（）是位於[岡山縣北部的](../Page/岡山縣.md "wikilink")[城市](../Page/城市.md "wikilink")，為岡山縣第三大城，也是[美作地方的中心都市](../Page/美作地方.md "wikilink")。

轄區以[津山盆地為主](../Page/津山盆地.md "wikilink")，西側、北側是[中國山地](../Page/中國山地.md "wikilink")，東側為[美作台地](../Page/美作台地.md "wikilink")，南側為[吉備高原](../Page/吉備高原.md "wikilink")。

## 歷史

從8世紀[美作國從](../Page/美作國.md "wikilink")[備前國分出後](../Page/備前國.md "wikilink")，成為美作國的[國府所在地](../Page/國府.md "wikilink")。[江戶時代設置](../Page/江戶時代.md "wikilink")[津山藩並建築](../Page/津山藩.md "wikilink")[津山城後](../Page/津山城.md "wikilink")，成為津山藩的[城下町](../Page/城下町.md "wikilink")。\[1\]

1871年[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，最初為[津山縣的縣政府所在地](../Page/津山縣.md "wikilink")，但五年後即被併入[岡山縣](../Page/岡山縣.md "wikilink")。

### 年表

  - 1889年6月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [西西條郡](../Page/西西條郡.md "wikilink")：二宮村、院庄村。
      - [西北條郡](../Page/西北條郡.md "wikilink")：**津山町**、西苫田村、一宮村、田邑村 。
      - [東南條郡](../Page/東南條郡.md "wikilink")：[津山東町](../Page/津山東町_\(岡山縣東南條郡\).md "wikilink")、林田村、高野村、東苫田村、東一宮村。
      - [東北條郡](../Page/東北條郡.md "wikilink")：神庭村、高倉村、高田村、[阿波村](../Page/阿波村.md "wikilink")、加茂村、西加茂村、東加茂村、上加茂村
        。
      - [勝北郡](../Page/勝北郡.md "wikilink")：新野村、廣戶村、勝加茂村、廣野村、瀧尾村。
      - [勝南郡](../Page/勝南郡.md "wikilink")：高取村、大崎村、河邊村 。
      - [久米北條郡](../Page/久米北條郡.md "wikilink")：久米村、大井東村、大井西村、大倭村、倭文東村、倭文中村。
      - [久米南條郡](../Page/久米南條郡.md "wikilink")：福岡村、佐良山村。
  - 1900年4月1日：
      - 津山東町被併入津山町。
      - 西西條郡、西北條郡、東南條郡、東北條郡合併為[苫田郡](../Page/苫田郡.md "wikilink")。
      - 勝北郡和勝南郡合併為[勝田郡](../Page/勝田郡.md "wikilink")。
      - 久米北條郡和久米南條郡合併為[久米郡](../Page/久米郡.md "wikilink")。
  - 1901年4月1日：佐良山村的大谷地區被併入福岡村。
  - 1923年4月1日：林田村改制並改名為[津山東町](../Page/津山東町_\(岡山縣苫田郡\).md "wikilink")。
  - 1924年7月1日：加茂村改制為[加茂町](../Page/加茂町_\(岡山縣\).md "wikilink")。
  - 1929年2月11日：津山町、津山東町、西苫田村、二宮村、院-{}-庄村和福岡村[合併為](../Page/市町村合併.md "wikilink")**津山市**。
  - 1940年9月1日：倭文東村和倭文中村合併為倭文村。
  - 1941年2月11日：佐良山村和東苫田村被併入津山市。\[2\]（1町24村）
  - 1942年5月27日：加茂町、西加茂村、東加茂村合併為新設置的加茂町。
  - 1943年11月3日：大井東村和大倭村合併為大東村。
  - 1951年1月1日：加茂町的舊加茂町地區分出獨立設置為[新加茂町](../Page/新加茂町.md "wikilink")。
  - 1951年4月1日　一宮村和東一宮村合併為新設置的一宮村。
  - 1952年8月1日：大井西村和大東村合併為[大井町](../Page/大井町_\(岡山縣\).md "wikilink")。
  - 1954年3月31日：高取村和勝田郡[勝間田町](../Page/勝間田町.md "wikilink")、植月村、古吉野村、吉野村的部分地區合併為[勝央町](../Page/勝央町.md "wikilink")。
  - 1954年4月1日：新加茂町、加茂町、上加茂村合併為新設置的加茂町。
  - 1954年7月1日：廣野村、瀧尾村、大崎村、河邊村、一宮村、田邑村、高野村、神庭村、高倉村、高田村被併入津山市。
  - 1955年1月1日：
      - 大井町、久米村、倭文村合併為[久米町](../Page/久米町.md "wikilink")。
      - 新野村、廣戶村、勝加茂村合併為[勝北町](../Page/勝北町.md "wikilink")
  - 1955年4月1日：勝北町的楢地區被併入津山市
  - 1955年6月1日：勝央町的池原、堂尾地區被併入津山市
  - 2005年2月28日：久米町、勝北町、加茂町、阿波村被併入津山市。\[3\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年<br />
6月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>津山町</p></td>
<td><p>1929年2月11日<br />
津山市</p></td>
<td><p>津山市</p></td>
<td><p>津山市</p></td>
<td><p>津山市</p></td>
<td><p>津山市</p></td>
</tr>
<tr class="even">
<td><p>津山東町</p></td>
<td><p>1900年4月1日<br />
併入津山町。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西苫田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>二宮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>院庄村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>林田村</p></td>
<td><p>1923年4月1日<br />
改名為津山東町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佐良山村</p></td>
<td><p>1901年4月1日<br />
併入福岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>佐良山村</p></td>
<td><p>1941年2月11日<br />
併入津山市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東苫田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高野村</p></td>
<td><p>1954年7月1日<br />
併入津山市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東一宮村</p></td>
<td><p>1951年4月1日<br />
一宮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>一宮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>田邑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神庭村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高倉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大崎村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>河邊村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>廣野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>瀧尾村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>勝加茂村</p></td>
<td><p>1955年1月1日<br />
勝北町</p></td>
<td><p>1955年4月1日<br />
併入津山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>新野村</p></td>
<td><p>勝北町</p></td>
<td><p>2005年2月28日<br />
併入津山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>廣戶村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>加茂村</p></td>
<td><p>1924年7月1日<br />
加茂町</p></td>
<td><p>1942年5月27日<br />
加茂町</p></td>
<td><p>1951年1月1日<br />
新加茂町</p></td>
<td><p>1954年4月1日<br />
加茂町</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>西加茂村</p></td>
<td><p>加茂町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東加茂村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>上加茂村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>阿波村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>久米村</p></td>
<td><p>1955年1月1日<br />
久米町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大井西村</p></td>
<td><p>1952年8月1日<br />
大井町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大井東村</p></td>
<td><p>1943年11月3日<br />
大東村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大倭村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>倭文東村</p></td>
<td><p>1940年4月1日<br />
倭文村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>倭文中村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [姬新線](../Page/姬新線.md "wikilink")：（←[勝央町](../Page/勝央町.md "wikilink")）
        - [美作大崎車站](../Page/美作大崎車站.md "wikilink") -
        [東津山車站](../Page/東津山車站.md "wikilink") -
        [津山車站](../Page/津山車站.md "wikilink") -
        [院庄車站](../Page/院庄車站.md "wikilink") -
        [美作千代車站](../Page/美作千代車站.md "wikilink") -
        [坪井車站](../Page/坪井車站.md "wikilink") -
        （[真庭市](../Page/真庭市.md "wikilink")→）
      - [因美線](../Page/因美線.md "wikilink")：（←[智頭町](../Page/智頭町.md "wikilink")）
        - [美作河井車站](../Page/美作河井車站.md "wikilink") -
        [知和車站](../Page/知和車站.md "wikilink") -
        [美作加茂車站](../Page/美作加茂車站.md "wikilink") -
        [三浦車站](../Page/三浦車站.md "wikilink") -
        [美作瀧尾車站](../Page/美作瀧尾車站.md "wikilink") -
        [高野車站](../Page/高野車站_\(岡山縣\).md "wikilink") - 東津山車站
      - [津山線](../Page/津山線.md "wikilink")：（←[美咲町](../Page/美咲町.md "wikilink")）
        - [佐良山車站](../Page/佐良山車站.md "wikilink") -
        [津山口車站](../Page/津山口車站.md "wikilink") - 津山車站

|                                                                                                                                          |                                                                                                                                                                            |
| ---------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [JRW-Higashi-TsuyamaStation.jpg](https://zh.wikipedia.org/wiki/File:JRW-Higashi-TsuyamaStation.jpg "fig:JRW-Higashi-TsuyamaStation.jpg") | [Tsuyama_eki01.JPG](https://zh.wikipedia.org/wiki/File:Tsuyama_eki01.JPG "fig:Tsuyama_eki01.JPG")                                                                         |
| [JRW_innosho_sta.jpg](https://zh.wikipedia.org/wiki/File:JRW_innosho_sta.jpg "fig:JRW_innosho_sta.jpg")                                | [JRW_imbi_line_Mimasaka-kawai_station.jpg](https://zh.wikipedia.org/wiki/File:JRW_imbi_line_Mimasaka-kawai_station.jpg "fig:JRW_imbi_line_Mimasaka-kawai_station.jpg") |
| [Chiwa_sta_enclosure.jpg](https://zh.wikipedia.org/wiki/File:Chiwa_sta_enclosure.jpg "fig:Chiwa_sta_enclosure.jpg")                    | [JRW_takano_sta.jpg](https://zh.wikipedia.org/wiki/File:JRW_takano_sta.jpg "fig:JRW_takano_sta.jpg")                                                                     |

### 道路

  - [高速道路](../Page/高速道路.md "wikilink")

<!-- end list -->

  - [中國自動車道](../Page/中國自動車道.md "wikilink")：[津山交流道](../Page/津山交流道.md "wikilink")
    - [二宮休息區](../Page/二宮休息區.md "wikilink") -
    [院庄交流道](../Page/院庄交流道.md "wikilink")

## 觀光資源

[Tsuyama_Castle02n3200.jpg](https://zh.wikipedia.org/wiki/File:Tsuyama_Castle02n3200.jpg "fig:Tsuyama_Castle02n3200.jpg")
[Tsuyama_jokamachi_-01.jpg](https://zh.wikipedia.org/wiki/File:Tsuyama_jokamachi_-01.jpg "fig:Tsuyama_jokamachi_-01.jpg")

### 景點

  - 鶴山公園（[津山城遺跡](../Page/津山城.md "wikilink")）
  - [眾樂園](../Page/眾樂園.md "wikilink")（舊津山藩別邸庭園）
  - 城東町街道保存地区（津山洋學資料館、箕作阮甫舊宅、舊梶村家住宅、作州城東屋敷、河野美術館）
  - 田町舊武家屋敷遺跡
  - [中山神社](../Page/中山神社.md "wikilink")（[美作國](../Page/美作國.md "wikilink")[一宮](../Page/一宮.md "wikilink")）

### 祭典

  - [福力荒神社大祭](../Page/福力荒神社大祭.md "wikilink")（每年[舊曆](../Page/舊暦.md "wikilink")1月1日～1月3日）
  - 津山櫻花祭（每年4月1日～15日）
  - 中山神社田植祭（每年4月29日）
  - 津山納涼魂語祭（每年8月7日～8日）
  - 津山祭（每年10月第3和第4個週末）

## 教育

[Okayama_Prefectural_Tsuyama_High_school01n4272.jpg](https://zh.wikipedia.org/wiki/File:Okayama_Prefectural_Tsuyama_High_school01n4272.jpg "fig:Okayama_Prefectural_Tsuyama_High_school01n4272.jpg")

  - 大學

<!-- end list -->

  - [美作大學](../Page/美作大學.md "wikilink")
  - 美作大學短期大學部

<!-- end list -->

  - 高等專門學校

<!-- end list -->

  - [津山工業高等專門學校](../Page/津山工業高等專門學校.md "wikilink")

<!-- end list -->

  - 高等學校

<!-- end list -->

  - [岡山縣立津山高等學校](../Page/岡山縣立津山高等學校.md "wikilink")
  - [岡山縣立津山東高等學校](../Page/岡山縣立津山東高等學校.md "wikilink")
  - [岡山縣立津山工業高等學校](../Page/岡山縣立津山工業高等學校.md "wikilink")
  - [岡山縣立津山商業高等學校](../Page/岡山縣立津山商業高等學校.md "wikilink")
  - [岡山縣作陽高等學校](../Page/岡山縣作陽高等學校.md "wikilink")
  - [岡山縣美作高等學校](../Page/岡山縣美作高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [平良市](../Page/平良市.md "wikilink")（[沖繩縣](../Page/沖繩縣.md "wikilink")），已於2005年合併為[宮古島市](../Page/宮古島市.md "wikilink")
  - [出雲市](../Page/出雲市.md "wikilink")（[島根縣](../Page/島根縣.md "wikilink")）
  - [諫早市](../Page/諫早市.md "wikilink")（[長崎縣](../Page/長崎縣.md "wikilink")）
  - [飯田市](../Page/飯田市.md "wikilink")（[長野縣](../Page/長野縣.md "wikilink")）
  - [土庄町](../Page/土庄町.md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")）
  - [兼山町](../Page/兼山町.md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")），已於2005年被併入[可兒市](../Page/可兒市.md "wikilink")

### 海外

  - [圣菲](../Page/圣菲_\(新墨西哥州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[新墨西哥州](../Page/新墨西哥州.md "wikilink")）

## 本地出身之名人

  - [箕作阮甫](../Page/箕作阮甫.md "wikilink")：[蘭學者](../Page/蘭學者.md "wikilink")
  - [平沼淑郎](../Page/平沼淑郎.md "wikilink")：[教育者](../Page/教育者.md "wikilink")、前[早稻田大學校長](../Page/早稻田大學.md "wikilink")
  - [平沼騏一郎](../Page/平沼騏一郎.md "wikilink")：前[內閣總理大臣](../Page/內閣總理大臣_\(日本\).md "wikilink")
  - [美土路昌一](../Page/美土路昌一.md "wikilink")：第一任[全日本空輸社長](../Page/全日本空輸.md "wikilink")、前[朝日新聞社社長](../Page/朝日新聞社.md "wikilink")
  - [棟田博](../Page/棟田博.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [西東三鬼](../Page/西東三鬼.md "wikilink")：[俳人](../Page/俳人.md "wikilink")
  - [稻葉浩志](../Page/稻葉浩志.md "wikilink")：[歌手](../Page/歌手.md "wikilink")，在搖滾組合[B'z裡擔任](../Page/B'z.md "wikilink")[主唱](../Page/主唱.md "wikilink")、[作詞](../Page/作詞.md "wikilink")。在個人[SOLO活動中則擔任](../Page/SOLO.md "wikilink")[作曲](../Page/作曲.md "wikilink")、吉他彈奏和音樂製作人。
  - [八代駿](../Page/八代駿.md "wikilink")：演員、配音原
  - [重松清](../Page/重松清.md "wikilink")：作家
  - [高橋信二](../Page/高橋信二.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [生田善子](../Page/生田善子.md "wikilink")：配音員
  - [小田切讓](../Page/小田切讓.md "wikilink")：演員、模特兒

## 相關條目

  - [津山事件](../Page/津山事件.md "wikilink")

## 參考資料

## 外部連結

  - [津山市觀光協會](http://www.tsuyamakan.jp/)

  - [津山市商店街聯盟](https://web.archive.org/web/20120421062544/http://www.optic.or.jp/lapimall/)

  - [津山商工會議所](https://web.archive.org/web/20120109064957/http://www.tvt.ne.jp/~kaigisho/)

<!-- end list -->

1.
2.
3.