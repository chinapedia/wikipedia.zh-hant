**伍珀塔尔**（Wuppertal）是[德国](../Page/德国.md "wikilink")[北莱茵-威斯特法伦州山城地区的一座大城市](../Page/北莱茵-威斯特法伦州.md "wikilink")。在地理位置上，它位于“[莱茵](../Page/莱茵.md "wikilink")－[鲁尔](../Page/鲁尔.md "wikilink")”城市群的几乎正中央与“鲁尔工业区”的南部，[杜塞尔多夫区的邻市](../Page/杜塞尔多夫.md "wikilink")（约西部30[公里](../Page/公里.md "wikilink")），距离[科隆东北向约](../Page/科隆.md "wikilink")40[公里](../Page/公里.md "wikilink")，距离[埃森东南向约](../Page/埃森.md "wikilink")23[公里](../Page/公里.md "wikilink")。伍珀塔尔有约360,000[人口](../Page/人口.md "wikilink")，是一座中型的大城市，该州的十个最大城市之一。

伍珀塔尔市在1929年8月1日由艾伯费尔德（Elberfeld，自1883年来的大城市）与巴门城（Barmen，自1884年来的大城市）及所辖的城区Cronenberg，Ronsdorf，Vohwinkel与东部的Beyenburg一起建立，当时叫巴门－艾伯费尔德。1930年结合民意改名为伍珀塔尔（Wuppertal）。在德文中，Wupper是城市所在地的一条河的名字，tal是山谷的意思。

## 地理

[Wuppertal_7.18769E_51.23313N.jpg](https://zh.wikipedia.org/wiki/File:Wuppertal_7.18769E_51.23313N.jpg "fig:Wuppertal_7.18769E_51.23313N.jpg")
伍珀塔尔座落于[伍珀河畔](../Page/伍珀河.md "wikilink")。因为城区高低差别大，这里有很多的台阶与陡峭的街道，这样伍珀塔尔成为德国有最多公共台阶的城市。城市周长约94.5[公里](../Page/公里.md "wikilink")，[伍珀河流经市区段约为](../Page/伍珀河.md "wikilink")33.9[公里](../Page/公里.md "wikilink")。城市最高点叫Lichtscheid（海拔350[m](../Page/公尺.md "wikilink")），城市最低点位于[伍珀河畔的Müngsten](../Page/伍珀河.md "wikilink")（海拔100[m](../Page/公尺.md "wikilink")）。

## 历史

最早是在公元前1000年有人进入了这个地区。2003年，人们在艾伯费尔德的Deweerthschen
Garten中两处地点发现了三米长的铁器时代的陶器碎片。今日的伍珀塔尔地区，也就是当时的那一群城区，出现于约[一世纪](../Page/一世纪.md "wikilink")。还早在后来的“[鲁尔工业区](../Page/鲁尔.md "wikilink")”发展起来之前，巴门－艾伯费尔德在19世纪末期已经是[德意志帝国最大的经济中心](../Page/德意志帝国.md "wikilink")。很多交通运输路线穿过城区，如：山城铁路线或者更多的矿之路。今天的到[哈根市的联邦七号大道](../Page/哈根.md "wikilink")（Bundesstraße
7）在当时是[普鲁士最固定的要道之一](../Page/普鲁士.md "wikilink")。

到巴门－艾伯费尔德的铁路工程在很早的时候就连接了[杜塞尔多夫](../Page/杜塞尔多夫.md "wikilink")，[科隆与](../Page/科隆.md "wikilink")[哈根](../Page/哈根.md "wikilink")。很多地方还都有接向该城区的各自的路线。这里曾经还负载很大的原材料运输量，并为运送货物与产品到全世界起到很重要的作用。巴门－艾伯费尔德铁路网是当时德意志帝国四大铁路网之一，这个铁路网从西南向从[哈根到](../Page/哈根.md "wikilink")[杜塞尔多夫](../Page/杜塞尔多夫.md "wikilink")，南北向从[埃森到](../Page/埃森.md "wikilink")[韦莫基辛](../Page/韦莫基辛.md "wikilink")（Wermelskirchen）。

在1900年左右，小型[铁制手工物品与](../Page/铁.md "wikilink")[纺织工业及引人睹目的商业为这两个城市带来了强大的增长](../Page/纺织.md "wikilink")。1929年8月1日，巴门城与艾伯费尔德合并。

1934年，[基督教下的反对者们在巴门城信仰会议上聚在了一起](../Page/基督教.md "wikilink")，为了反对[阿道夫·希特勒](../Page/阿道夫·希特勒.md "wikilink")。在[卡尔·巴特的思想下](../Page/卡尔·巴特.md "wikilink")，改革教会，信义宗，联合教会建立的[宣信会并完成了](../Page/宣信会.md "wikilink")[巴门城宣言](../Page/巴门城宣言.md "wikilink")（Barmer
Erklärung），这是教会拒绝[纳粹统治的一个重要文件](../Page/纳粹.md "wikilink")。

在[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，伍珀塔尔市受到盟军两次袭击，数千人死亡。鉴于其[工业基础](../Page/工业.md "wikilink")，市民们有了一个很好的开端对城市进行重建。[纺织工业直到](../Page/纺织.md "wikilink")1970年代一直是该市的经济基础，纺织业市场逐渐全球化，于是纺织工业在伍珀塔尔已没有重要意义。

1975年，伍珀塔尔进行了地域重构，有了今天的区域面积。

### 宗教

宗教在伍珀塔尔社会曾经远超过了城市的边界，虔诚教派与宗别非常多样化。如Ronsdorf整个城区曾经是由各种其他宗教人群组成。

今日的伍珀塔尔地区从最开始就属于[科隆](../Page/科隆.md "wikilink")[主教](../Page/主教.md "wikilink")[教区](../Page/教区.md "wikilink")。

1566年，Peter
Loh将改革宗教会的宗教改革带到了伍珀塔尔。1625年到1657年，[西班牙驻军禁止了](../Page/西班牙.md "wikilink")[新教的礼拜活动](../Page/新教.md "wikilink")。1690年起，该市还有过[路德教会](../Page/路德教会.md "wikilink")，1847年起，有过一个[荷兰](../Page/荷兰.md "wikilink")[改革教会](../Page/改革教会.md "wikilink")。艾伯费尔德在被[普鲁士接管后](../Page/普鲁士.md "wikilink")，这里的的[新教与](../Page/新教.md "wikilink")[路德教会均被](../Page/路德教会.md "wikilink")[杜塞尔多夫的](../Page/杜塞尔多夫.md "wikilink")[教会法庭所纳入](../Page/教会法庭.md "wikilink")，接下来是纳入了[科隆的](../Page/科隆.md "wikilink")，最后于1822年纳入了Kolbenz，当时[莱茵地方教堂](../Page/莱茵.md "wikilink")（今天的Evangelische
Kirche im Rheinland）所在地。

在巴门城，在被多次殖民后，[宗教改革于](../Page/宗教改革.md "wikilink")16世纪被引入。

在今日伍珀塔尔的其他城区，大多也是于16世纪有了改革宗教会的宗教改革。后来有过[路德教会](../Page/路德教会.md "wikilink")。但在如Beyenburg与Vohwinkel，[宗教改革与](../Page/宗教改革.md "wikilink")[路德教会却出现得相对较晚](../Page/路德教会.md "wikilink")，分别是1854年与1886年。

在[国家社会主义时期](../Page/国家社会主义.md "wikilink")，伍珀塔尔的巴门城因为那场于1934年5月29日到31日的宗教会议而出了名。这里诞生的《[巴门城宣言](../Page/巴门城宣言.md "wikilink")》（德：Barmer
Theologische Erklärung；英：The Barmen declaration）成为新教的信仰基础。

## 政治

早在1444年，艾伯费尔德就有了城市基本法以及相应的一名市长（Bürgermeister），一个市议会，一名治安官与陪审团。1610年起由一位[爵士阶层的长官带领一年一度](../Page/爵士.md "wikilink")5月1日的选举，从而产生市长，参议员等同等级的职员。1807年，市政府成为[法国模式](../Page/法国.md "wikilink")，1845年成为[莱茵地区模式](../Page/莱茵.md "wikilink")，1857年[莱茵城市模式](../Page/莱茵.md "wikilink")。城市最高的职位是市长（Oberbürgermeister）。

巴冕城在1808年有了市基本法与相应的一名最高领导人与另外两名相应等级职员与20名市议员。该市于1809年由一名Maire（法文：市长）执政，后来有了市长（Bürgermeister）。

在两城区合并成为巴冕－艾伯费尔德（1930年起称伍珀塔尔）之后，城市由之前的巴冕城市长执政。在[国家社会主义时期](../Page/国家社会主义.md "wikilink")，市长由[德国](../Page/德国.md "wikilink")[纳粹党取代](../Page/纳粹党.md "wikilink")。[第二次世界大战结束后](../Page/第二次世界大战.md "wikilink")，市长由[英占领区军政府重新指定并于](../Page/英.md "wikilink")1946年在该市依照[英国模式建立了公共宪法](../Page/英国.md "wikilink")。接下来有了一个由民众选出的「市议会」。市议会在一开始从其中间选出市长作为名义上的城市主席与代表。1946年起，由市议会同样选出了专任的城市领导人，作为市政府的领导者。1996年后，就只有专职市长这一职务了，他是市议会的主席者，也是市政府与该市的代表。后来的市长由民众直接选举。

## 文化与景点

[Wuppertal_-_Neumarkt_03_ies.jpg](https://zh.wikipedia.org/wiki/File:Wuppertal_-_Neumarkt_03_ies.jpg "fig:Wuppertal_-_Neumarkt_03_ies.jpg")
[EngelsHaus+Statue.JPG](https://zh.wikipedia.org/wiki/File:EngelsHaus+Statue.JPG "fig:EngelsHaus+Statue.JPG")
令这座城市在世界知名的是[伍珀塔爾空鐵](../Page/伍珀塔爾空鐵.md "wikilink")。

其他的景点有：享有知名度的「海德博物馆」（Von-der-Heydt-Museum）、「山城有轨电车博物馆」（das Bergische
Straßenbahnmuseum）、钟表博物馆（das
Uhrenmuseum）、「弗里德里希·恩格斯」故居（Friedrich-Engels-Haus，[科学社会主义的联合创始人](../Page/科学社会主义.md "wikilink")[弗里德里希·恩格斯的出生地](../Page/弗里德里希·恩格斯.md "wikilink")）、酿酒坊（das
Brauhaus）、[动物园](../Page/动物园.md "wikilink")（der Zoo
Wuppertal）、伍珀塔尔城市大厅（die Stadthalle
Wuppertal，[欧洲最佳声效及最具建筑美观性的](../Page/欧洲.md "wikilink")[音乐礼堂之一](../Page/音乐.md "wikilink")）。伍珀塔尔一共有4,500座以上[历史建筑](../Page/历史建筑.md "wikilink")，位居该联邦州第二。

### 戏剧与音乐

[Wuppertal_Schauspielhaus_2005.jpg](https://zh.wikipedia.org/wiki/File:Wuppertal_Schauspielhaus_2005.jpg "fig:Wuppertal_Schauspielhaus_2005.jpg")
属于伍珀塔尔剧团的伍珀塔尔歌剧院于1907年在当时的巴门城区修建，这座剧院在[二战中被严重破坏](../Page/二战.md "wikilink")，在1956年得到了重建，2003年起因为整修而关闭，预见将于2008年/2009年重新开放。艾伯费尔德的伍珀塔尔演出中心（das
Schauspielhaus）建于1966年，这里除了[歌剧与](../Page/歌剧.md "wikilink")[戏剧](../Page/戏剧.md "wikilink")，还有世界知名的[先锋舞蹈](../Page/先锋舞蹈.md "wikilink")：[皮娜·鮑什](../Page/皮娜·鮑什.md "wikilink")「伍珀塔尔舞蹈劇場」（Tanztheater
Wuppertal Pina Bausch）。

### 博物馆

「海德博物馆」（Von-der-Heydt-Museum）的特别收藏主要是伍珀塔尔19/20世纪工业时期的私人馈赠。这里还保护与收藏了20世纪早期的杰出艺术品。

早期工业博物馆（Das Museum für Frühindustrialisierung）

Fuhlrott-Museum

Völkerkundemuseum

钟表博物馆Uhrenmuseum

圣经博物馆Bibelmuseum

山地博物馆列车die Bergischen Museumsbahnen

老犹太教礼拜堂Die Begegnungsstätte Alte Synagoge

### 悬挂列车

[Schwebebahn_ueber_Strasse.jpg](https://zh.wikipedia.org/wiki/File:Schwebebahn_ueber_Strasse.jpg "fig:Schwebebahn_ueber_Strasse.jpg")
伍珀塔尔尤其以1901年起开放的[悬挂式單軌鐵路](../Page/悬挂式單軌鐵路.md "wikilink")[伍珀塔尔空铁而闻名](../Page/伍珀塔尔空铁.md "wikilink")。列车轨道长13.3[公里](../Page/公里.md "wikilink")。其支撑架于2004年被延伸同时保持了历史工艺，同时其车站进行了现代化改建。这样，伍泊塔尔的悬挂列车成为了有着超过100年历史但又非常现代安全与快速的短途交通系统，日载客量超过70,000人。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/以色列.md" title="wikilink">以色列</a><a href="../Page/贝尔谢巴.md" title="wikilink">贝尔谢巴</a></p></li>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/柏林.md" title="wikilink">柏林</a></p></li>
<li><p><a href="../Page/斯洛伐克.md" title="wikilink">斯洛伐克</a><a href="../Page/科希策.md" title="wikilink">科希策</a></p></li>
</ul></td>
<td></td>
<td><ul>
<li><p><a href="../Page/波兰.md" title="wikilink">波兰</a><a href="../Page/莱格尼察.md" title="wikilink">莱格尼察</a></p></li>
<li><p><a href="../Page/俄羅斯.md" title="wikilink">俄羅斯</a><a href="../Page/叶卡捷琳堡.md" title="wikilink">叶卡捷琳堡</a></p></li>
<li><p><a href="../Page/尼加拉瓜.md" title="wikilink">尼加拉瓜</a><a href="../Page/马塔加尔帕.md" title="wikilink">马塔加尔帕</a></p></li>
<li><p><a href="../Page/法国.md" title="wikilink">法国</a><a href="../Page/圣艾蒂安.md" title="wikilink">圣艾蒂安</a></p></li>
</ul></td>
<td></td>
<td><ul>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/什未林.md" title="wikilink">什未林</a></p></li>
<li><p><a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/南泰恩赛德.md" title="wikilink">南泰恩赛德</a></p></li>
<li><p><a href="../Page/中国.md" title="wikilink">中国</a><a href="../Page/新乡市.md" title="wikilink">新乡市</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [Bezirksjugendräte Wuppertal](http://www.bjr-wuppertal.de/)
  - [Website der Stadt Wuppertal](http://www.wuppertal.de/)
  - [Wuppertaler Denkmalliste](http://www.baudenkmale-wuppertal.de/)
  - \[<https://web.archive.org/web/20010721225042/http://www.schwebebahn.com/>
    Fotos und Infos über Wuppertal und der Schwebebahn
    *（Deutsch/English）\]*
  - [Die Wuppertaler Schwebebahn](http://www.schwebebahn.net)
  - [Wuppertal von seiner besten Seite - Sehenswürdigkeiten und vieles
    mehr...](https://web.archive.org/web/20060112090951/http://mankel.free.fr/Wuppertal/index.htm)
  - [Informationen zum Schienenverkehr in und um
    Wuppertal](https://web.archive.org/web/20170607205426/http://bahnen-im-bergischen.de/)
  - [Der Wuppertaler Zoo und das angrenzende Wohngebiet (Privater
    Link)](http://www.zoo-wuppertal.net)
  - [Barmer Bilderbogen mit 15 Seiten voller
    Farbfotos](http://www.Barmer-Bilderbogen.de)
  - [Die offizielle Seite des Wuppertaler
    Zoos](http://www.zoo-wuppertal.de)
  - [Wupperfoto ist eine private Foto Community für Wuppertal und
    Umgebung](http://www.wupperfoto.de)
  - <http://www.von-der-heydt-museum.de/> Von-der-Heydt-Museum\]
  - <https://web.archive.org/web/20041013192531/http://www.wuppbrau.de/home/home.html>
    Brauhaus\]
  - <http://www.stadthalle-wuppertal.de> historische Stadthalle\]

[伍珀塔尔](../Category/伍珀塔尔.md "wikilink")
[W](../Category/北莱茵-威斯特法伦州市镇.md "wikilink")