**縣道152號
公館－名間**，是位於台灣中部的縣道公路，西起[彰化縣](../Page/彰化縣.md "wikilink")[大城鄉](../Page/大城鄉.md "wikilink")，東至[南投縣](../Page/南投縣.md "wikilink")[名間鄉](../Page/名間鄉.md "wikilink")，全長共計48.2公里（公路總局資料）。

## 行經行政區域

  - [彰化縣](../Page/彰化縣.md "wikilink")

<!-- end list -->

  - [大城鄉](../Page/大城鄉.md "wikilink")：西港0.0k（**起點**）→公館0.5k（叉路）→大城3.5k（叉路）
  - [竹塘鄉](../Page/竹塘鄉_\(台灣\).md "wikilink")：內新厝11.9k（共線起點）→竹塘15.1k（共線終點）
  - [埤頭鄉](../Page/埤頭鄉.md "wikilink")：路口厝18.6k（叉路）
  - [溪州鄉](../Page/溪州鄉.md "wikilink")：溪洲22.6k（叉路）→陸軍路
  - [田中鎮](../Page/田中鎮.md "wikilink")：四塊厝
  - [二水鄉](../Page/二水鄉.md "wikilink")：二水36.6k（叉路）→源泉38.9k（左叉路）

<!-- end list -->

  - [南投縣](../Page/南投縣.md "wikilink")

<!-- end list -->

  - [名間鄉](../Page/名間鄉.md "wikilink")：名間48.2k（**終點**，叉路）

### 已解編路段

  - [南投縣](../Page/南投縣.md "wikilink")

<!-- end list -->

  - [名間鄉](../Page/名間鄉.md "wikilink")：名間48.2k（共線起點）→濁水48.9k（共線終點，共線起點）→綠隧西口49.9k（共線終點）
  - [集集鎮](../Page/集集鎮.md "wikilink")：綠隧東口55.0k→林尾55.711k（接）

## 沿線風景

### 已解編路段

[Jiji_Green_Tunnel_West_Entrance.JPG](https://zh.wikipedia.org/wiki/File:Jiji_Green_Tunnel_West_Entrance.JPG "fig:Jiji_Green_Tunnel_West_Entrance.JPG")
[Ji_Ji_Tunnel,_County_Road_152_(Taiwan).jpg](https://zh.wikipedia.org/wiki/File:Ji_Ji_Tunnel,_County_Road_152_\(Taiwan\).jpg "fig:Ji_Ji_Tunnel,_County_Road_152_(Taiwan).jpg")

  - 集集綠色隧道
  - [集集線鐵路](../Page/集集線.md "wikilink")[濁水車站](../Page/濁水車站.md "wikilink")
  - 集集線鐵路[源泉車站](../Page/源泉車站.md "wikilink")
  - 集集線鐵路[龍泉車站](../Page/龍泉車站.md "wikilink")

## 交流道

  - [TW_PHW61.svg](https://zh.wikipedia.org/wiki/File:TW_PHW61.svg "fig:TW_PHW61.svg")[大城交流道](../Page/大城交流道.md "wikilink")（現僅通南入北出）

## 參見

## 相關連結

[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:彰化縣道路](../Category/彰化縣道路.md "wikilink")
[Category:南投縣道路](../Category/南投縣道路.md "wikilink")