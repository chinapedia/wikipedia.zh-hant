**新界五大氏族**或**新界五大族**或**新界五大家族**是指[新界原居民中](../Page/新界原居民.md "wikilink")[錦田](../Page/錦田.md "wikilink")[鄧氏](../Page/鄧姓.md "wikilink")、[新田](../Page/新田.md "wikilink")[文氏](../Page/文姓.md "wikilink")、[上水](../Page/上水.md "wikilink")[廖氏](../Page/廖姓.md "wikilink")、[上水](../Page/上水.md "wikilink")[侯氏及](../Page/侯姓.md "wikilink")[粉嶺](../Page/粉嶺.md "wikilink")[彭氏](../Page/彭姓.md "wikilink")。

他們都在[宋](../Page/宋朝.md "wikilink")[明期間](../Page/明朝.md "wikilink")，即清朝以前移居現時[香港的](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")，至今發展至新界多個地方定居。期間五族各自在根據地內建築[圍村](../Page/圍村.md "wikilink")、[祠堂](../Page/祠堂.md "wikilink")、[書室和](../Page/書室.md "wikilink")[廟宇等](../Page/廟宇.md "wikilink")，並在交通要道成立墟市，逐漸發展成大族。其後[香港政府在新界登記地權](../Page/香港殖民地時期#香港政府.md "wikilink")，發覺五族名下之土地甚多，故此稱他們為「五大族」（The
Five Great Clans）。·

## 錦田鄧氏

[TangAncestralHall01.jpg](https://zh.wikipedia.org/wiki/File:TangAncestralHall01.jpg "fig:TangAncestralHall01.jpg")為香港最大的祠堂\]\]

[鄧族祖先](../Page/香港新界鄧氏.md "wikilink")[江西](../Page/江西.md "wikilink")[吉安因在五族中最早居住新界](../Page/吉安市.md "wikilink")，是新界五大族之首。先祖[鄧漢黻四世孫](../Page/鄧漢黻.md "wikilink")**[鄧符協](../Page/鄧符協.md "wikilink")**於[北宋初年入](../Page/北宋.md "wikilink")[元朗](../Page/元朗區.md "wikilink")[錦田](../Page/錦田.md "wikilink")\[1\]，至今新界鄧族子孫後代人數多達十萬人。鄧族五大房中有兩房仍居今日新界，其餘各房已遷返內地。其中元禎房聚居元朗[屏山](../Page/屏山_\(香港\).md "wikilink")\[2\]，而元亮房則繼續聚居錦田。其後，元亮一房更分支到[元朗的](../Page/元朗區.md "wikilink")[廈村](../Page/廈村.md "wikilink")、[輞井](../Page/輞井.md "wikilink")、[屯門的](../Page/屯門區.md "wikilink")[紫田村](../Page/紫田村.md "wikilink")、[粉嶺的](../Page/粉嶺.md "wikilink")[龍躍頭](../Page/龍躍頭.md "wikilink")、[萊洞及](../Page/萊洞.md "wikilink")[大埔的](../Page/大埔區.md "wikilink")[大埔頭一帶](../Page/大埔頭.md "wikilink")\[3\]，為新界鄧族主要一支。鄧族自宋時遷入新界後，人口日增，建有宗祠、書室，並於鄰近地域建立墟市。當中[屏山市](../Page/屏山市.md "wikilink")、[錦田市](../Page/錦田市.md "wikilink")、[廈村市](../Page/廈村市.md "wikilink")、[元朗墟和](../Page/元朗墟.md "wikilink")[大埔舊墟等墟市](../Page/大埔舊墟.md "wikilink")，皆鄧族於[清代時建立](../Page/清代.md "wikilink")。鄧族對新界地區的發展，貢獻最大\[4\]。

與鄧族有關[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")︰

  - 錦田︰[廣瑜鄧公祠](../Page/廣瑜鄧公祠.md "wikilink")、[二帝書院](../Page/二帝書院.md "wikilink")
  - 屏山︰[達德公所](../Page/達德公所.md "wikilink")、鄧氏宗祠、[愈喬二公祠](../Page/愈喬二公祠.md "wikilink")、[聚星樓](../Page/聚星樓.md "wikilink")、[仁敦岡書室](../Page/仁敦岡書室.md "wikilink")
  - 廈村︰[鄧氏宗祠](../Page/廈村鄧氏宗祠.md "wikilink")、[楊侯宮](../Page/廈村楊侯宮.md "wikilink")
  - 龍躍頭︰[松嶺鄧公祠](../Page/松嶺鄧公祠.md "wikilink")、[老圍](../Page/老圍_\(龍躍頭\).md "wikilink")、[覲龍圍](../Page/覲龍圍.md "wikilink")、[麻笏圍門樓](../Page/麻笏圍.md "wikilink")、天后宮
  - 大埔頭︰[敬羅家塾](../Page/敬羅家塾.md "wikilink")

[File:廣瑜鄧公祠.JPG|錦田廣瑜鄧公祠](File:廣瑜鄧公祠.JPG%7C錦田廣瑜鄧公祠)
[File:HK_TsuiSingLauPagoda.JPG|屏山聚星樓](File:HK_TsuiSingLauPagoda.JPG%7C屏山聚星樓)
[File:HK_YeungHauTemple_HaTsuen.JPG|廈村楊侯宮](File:HK_YeungHauTemple_HaTsuen.JPG%7C廈村楊侯宮)
<File:LungYeukTau> LoWai Outside.jpg |龍躍頭老圍 King Law Ka Shuk
1.jpg|大埔頭敬羅家塾

## 新田文氏

[Man_Tin_Cheung_Park_(Yuen_Long,_Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Man_Tin_Cheung_Park_\(Yuen_Long,_Hong_Kong\).jpg "fig:Man_Tin_Cheung_Park_(Yuen_Long,_Hong_Kong).jpg")為粵港文氏共同興建\]\]

[文族祖籍江西吉安](../Page/香港新界文氏.md "wikilink")，為新界第二大宗族。其先祖**文天瑞**為[南宋抗](../Page/南宋.md "wikilink")[元英雄](../Page/元朝.md "wikilink")[文天祥堂弟](../Page/文天祥.md "wikilink")，於南宋末年逃避元兵移居[東莞](../Page/東莞.md "wikilink")。五世孫文蔭遷至大埔[泰亨立村](../Page/泰亨.md "wikilink")，人數共700餘人，分佈於[祠堂村](../Page/祠堂村_\(泰亨鄉\).md "wikilink")、[中心圍及](../Page/中心圍.md "wikilink")[灰沙圍三村](../Page/灰沙圍.md "wikilink")\[5\]；七世孫文世歌則遷至元朗[新田立村](../Page/新田_\(香港\).md "wikilink")，人數共5000餘人，分佈於[仁壽圍](../Page/仁壽圍.md "wikilink")、[石湖圍](../Page/石湖圍.md "wikilink")、[東鎮圍](../Page/東鎮圍.md "wikilink")、[安龍村](../Page/安龍村.md "wikilink")、[永平村](../Page/永平村.md "wikilink")、[蕃田村](../Page/蕃田村.md "wikilink")、[新龍村](../Page/新龍村.md "wikilink")、[青龍村和](../Page/青龍村.md "wikilink")[洲頭村等九條村落](../Page/洲頭村.md "wikilink")。此外，亦有族人聚居於[壆圍及](../Page/壆圍.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[沙螺灣等地](../Page/沙螺灣.md "wikilink")\[6\]。另外，文族亦為[太和市](../Page/大埔墟.md "wikilink")（即現今所稱的大埔墟）的創建宗族之一。
\[7\]

與文族有關香港法定古蹟:

  - 新田：[麟峯文公祠](../Page/麟峯文公祠.md "wikilink")、[大夫第](../Page/新田大夫第.md "wikilink")
  - 大埔：[大埔碗窰窰址](../Page/大埔碗窰窰址.md "wikilink")

[File:HK_ManLunFungAncestralHall.JPG|麟峯](File:HK_ManLunFungAncestralHall.JPG%7C麟峯)[文公祠](../Page/文公祠.md "wikilink")
<File:Tai> Fu Tai Mansion HK.jpg|新田大夫第

## 上水廖氏

[HK_WaiNoiTsuen2.JPG](https://zh.wikipedia.org/wiki/File:HK_WaiNoiTsuen2.JPG "fig:HK_WaiNoiTsuen2.JPG")

[廖族原籍](../Page/上水廖氏.md "wikilink")[福建](../Page/福建.md "wikilink")[永定](../Page/永定區_\(龍巖市\).md "wikilink")。先祖**廖仲傑**於元末初居屯門，再遷[福田](../Page/福田.md "wikilink")，至元代末年三遷[雙魚境內](../Page/雙魚河.md "wikilink")\[8\]。廖仲傑之子自玉為上水鄉開基祖，上水鄉一帶原有簡氏村民，相傳廖族看中該地後，通過假扮鬼怪嚇走簡氏族人\[9\]，使簡氏遷居[松柏塱](../Page/松柏塱.md "wikilink")，而廖氏則落居梧桐河畔現址，故廖氏至今仍會在村祭儀式中感謝簡氏讓地之恩。\[10\]現時上水鄉主要分三大房，第四房移居[沙田](../Page/沙田區.md "wikilink")[烏溪沙](../Page/烏溪沙.md "wikilink")[落禾沙村](../Page/落禾沙村.md "wikilink")。由於廖族人口日眾，於上水鄉建有[圍內村](../Page/圍內村.md "wikilink")、[門口村](../Page/門口村.md "wikilink")、[莆上村](../Page/莆上村.md "wikilink")、[大元村](../Page/大元村.md "wikilink")、[中心村](../Page/中心村.md "wikilink")、[上北村](../Page/上北村.md "wikilink")、[下北村](../Page/下北村.md "wikilink")、[興仁村及](../Page/興仁村.md "wikilink")[文閣村等九條村落](../Page/文閣村.md "wikilink")，人數共4000餘人。上水一帶原爲更早建村的侯氏所領導，侯氏亦曾建立墟市，但至清代廖氏已取代侯氏成爲上水的領導村族，並於上水鄉南方建立[石湖墟](../Page/石湖墟.md "wikilink")\[11\]，因石湖墟爲香港境內最靠近新安縣城[深圳墟的墟市](../Page/深圳墟.md "wikilink")，成爲重要的貨物流散地，也是新界北部主要的農產市場，使廖族賺取巨額財富，廖族極盛時所擁土地遠至香港島的[掃桿埔](../Page/掃桿埔.md "wikilink")。上水廖氏對教育的投入在新界五大族中尤爲突出，近100年來在北區先後出資建立了三所中學、三所小學和一所幼稚園，其中還包括全香港校園面積第二大的[鳳溪第一中學](../Page/鳳溪第一中學.md "wikilink")。

與廖族有關香港法定古蹟︰

  - 上水︰[廖萬石堂](../Page/廖萬石堂.md "wikilink")

[File:LiuManShekTongAncestralHall.jpg|上水鄉廖萬石堂](File:LiuManShekTongAncestralHall.jpg%7C上水鄉廖萬石堂)

## 上水侯氏

侯族原籍[廣東](../Page/廣東.md "wikilink")[番禺](../Page/番禺.md "wikilink")，其先祖侯五郎為北宋進士，而六世孫**侯卓峰**則為[河上鄉侯族始祖](../Page/河上鄉.md "wikilink")\[12\]。侯卓峰生六子，其中前四房世居河上鄉。第五房後人遷居[金錢](../Page/金錢_\(香港\).md "wikilink")、[燕崗](../Page/燕崗.md "wikilink")。第六房後人遷至[丙崗](../Page/丙崗.md "wikilink")。侯族在上水一帶人數近2000人，聚居地集中在上水西部。侯族爲上水最早發跡的大族，曾於上水建立隔圳墟和天岡墟兩個墟市，但其勢於清代衰落，爲較晚遷居到梧桐河畔的廖族取代其於上水的領導地位。1899年，英軍接收新界時，丙崗侯氏聯合鄰近姓族反抗。到1908年，[港英政府及](../Page/港英政府.md "wikilink")[香港哥爾夫球會開始與侯族爲首的村民進行談判](../Page/香港哥爾夫球會.md "wikilink")，最終得到村民同意於金錢村與丙崗村之間的侯氏祖墳山地一帶興建粉嶺哥爾夫球場，故侯族在新界境內地位甚高\[13\]。

[File:SheungShui_HauKuShekAncestralHall.jpg|河上鄉](File:SheungShui_HauKuShekAncestralHall.jpg%7C河上鄉)[居石侯公祠](../Page/居石侯公祠.md "wikilink")

## 粉嶺彭氏

彭族原籍[江西](../Page/江西.md "wikilink")[宜春](../Page/宜春.md "wikilink")，始祖**彭桂**，南宋時攜子遷居[粉嶺](../Page/粉嶺.md "wikilink")[龍躍頭](../Page/龍躍頭.md "wikilink")，彭族至[元朝末年因人丁單薄](../Page/元朝末年.md "wikilink")，受外地遷來的鄧季琇一族欺壓\[14\]，移居[粉嶺樓一帶](../Page/粉嶺樓.md "wikilink")，而龍躍頭則變成龍躍頭鄧氏的聚居地。由於人口繁衍，部份族人遷居鄰近的[粉嶺圍立村](../Page/粉嶺圍.md "wikilink")，建立[正圍](../Page/正圍.md "wikilink")、[北邊村及](../Page/北邊村.md "wikilink")[南邊村三條村落](../Page/南邊村.md "wikilink")。此外，部份族人亦定居[蕉徑](../Page/蕉徑.md "wikilink")[彭屋](../Page/彭屋.md "wikilink")、[上水](../Page/上水.md "wikilink")[掃管埔村及大埔](../Page/掃管埔村.md "wikilink")[汀角村等地](../Page/汀角村.md "wikilink")\[15\]，人數約4000人，亦有記載其族人曾於清朝年間於[香港島](../Page/香港島.md "wikilink")[掃桿埔一帶建立箒管莆村](../Page/掃桿埔.md "wikilink")。\[16\]自英國租借新界後，由於彭氏定居地位處[九廣鐵路旁邊](../Page/九廣鐵路.md "wikilink")，而港英政府更於當地先後修築[粉嶺站](../Page/粉嶺站.md "wikilink")、[沙頭角支線和](../Page/沙頭角支線.md "wikilink")[沙頭角公路](../Page/沙頭角公路.md "wikilink")，使彭族迅速崛起。1940年代末，爲了抗衡由[上水廖氏控制的](../Page/上水圍.md "wikilink")[石湖墟定價不公的現象](../Page/石湖墟.md "wikilink")，彭族聯合沙頭角、打鼓嶺和大埔一帶的村落於粉嶺樓東北面建立現代墟市[聯和墟](../Page/聯和墟.md "wikilink")\[17\]。至1970年代港英政府發展[粉嶺／上水新市鎮](../Page/粉嶺／上水新市鎮.md "wikilink")，彭族定居地位處新市鎮的中心點，使彭族得以出售大量土地而迅速致富。

<File:Fanling> Wai 2012.jpg|粉嶺圍
[File:PangAncestralHall_Front.jpg|粉嶺圍](File:PangAncestralHall_Front.jpg%7C粉嶺圍)[彭氏宗祠](../Page/彭氏宗祠_\(粉嶺圍\).md "wikilink")

## 新界五大族之間的維繫

周王二院是一所維繫新界五大族的建築物，原為報德祠，位於[上水](../Page/上水.md "wikilink")[石湖墟](../Page/石湖墟.md "wikilink")。當年由雙魚區鄧、侯、彭、廖、文各族集資興建，以答謝兩廣總督周有德及廣東巡撫王來任上奏清政府請求復界。現今周王二院已是有限公司，分為七股：（一）上水鄉廖萬石堂；（二）上水鄉廖允升堂；（三）粉嶺圍彭大德堂；（四）龍躍頭鄧萃雲堂；（五）泰亨鄉文公眾堂、大埔頭鄉鄧眾興堂；（六）河上鄉、燕崗、丙崗、金錢村侯族；（七）新田鄉文惇裕堂\[18\]。

## 其他氏族

其實除了這五大族以外，新界各地亦有多个清朝建立以前到达的氏族。\[19\]
宋朝末到达：[蒲崗村的](../Page/蒲崗村.md "wikilink")[林姓](../Page/林姓.md "wikilink")、
元朝到达：屯門的[陶姓](../Page/陶姓.md "wikilink")、[衙前圍村的](../Page/衙前圍村.md "wikilink")[吳姓](../Page/吳姓.md "wikilink")、
明朝到达：[龍躍頭的](../Page/龍躍頭.md "wikilink")[温姓](../Page/温姓.md "wikilink")、[鹿頸的](../Page/鹿頸.md "wikilink")[朱姓](../Page/朱姓.md "wikilink")、[大浪西灣的](../Page/大浪西灣.md "wikilink")[黎姓](../Page/黎姓.md "wikilink")、[石壁](../Page/石壁_\(香港\).md "wikilink")（[石碧新村](../Page/石碧新村.md "wikilink")）的[徐姓等](../Page/徐姓.md "wikilink")。
[復界后回港的氏族有](../Page/遷界令.md "wikilink")[白沙澳的](../Page/白沙澳.md "wikilink")[翁姓](../Page/翁姓.md "wikilink")、[大嶼山的](../Page/大嶼山.md "wikilink")[何姓](../Page/何姓.md "wikilink")、[塔門的](../Page/塔門.md "wikilink")[蓝姓](../Page/蓝姓.md "wikilink")。此外，[乾隆年间有居住於](../Page/乾隆.md "wikilink")[長洲的](../Page/長洲_\(香港\).md "wikilink")[黄姓](../Page/黄姓.md "wikilink")。

## 參考資料

  - 《香港古代史》修訂版，蕭國健 著，[中華書局](../Page/中華書局.md "wikilink")，ISBN
    962-8885-51-0
  - 《新界五大家族》，蕭國健 著，現代教育研究社，ISBN 962-11-1996-0
  - [《百載鑪峰》1982年第二集〈早期居民：祠堂〉](http://app4.rthk.hk/special/rthkmemory/details/hk-footprints/101)

### 引用

[Category:香港家族](../Category/香港家族.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")
[Category:中国五大](../Category/中国五大.md "wikilink")

1.  [口頭傳統和表現形式 -
    宗族口述傳說（錦田鄧氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_ref=LPEC03&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqomd1aN6oKBNd)
2.  [口頭傳統和表現形式 -
    宗族口述傳說（屏山鄧氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_ref=LPEC03&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqolCMaKF62XfP)
3.  [口頭傳統和表現形式 -
    宗族口述傳說（龍躍頭鄧氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_ref=LPEC03&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqou%2FQTX8aJT7B)
4.  [香港的先民宗族](http://www.nanchens.com/hkcs/hkcs0501.htm)
5.  [口頭傳統和表現形式 -
    宗族口述傳說（泰亨文氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_ref=LPEC03&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqovVq%2FQJ1W7Gd)
6.  [口頭傳統和表現形式 -
    宗族口述傳說（新田文氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_ref=LPEC03&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqooA96qE2Gb9d)
7.  [大步墟市爭今昔](http://www.pof.org.hk/new/s244_content.php?id=76)
8.  \[<https://sc.lcsd.gov.hk/b5/mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_actual_q=(+(+allTermsMandatory>:(true)+OR+fulltext:(%E4%B8%8A%E6%B0%B4)+OR++all_dc.title:(%E4%B8%8A%E6%B0%B4)+OR+all_dc.creator:(%E4%B8%8A%E6%B0%B4)+OR+all_dc.contributor:(%E4%B8%8A%E6%B0%B4)+OR+all_dc.subject:(%E4%B8%8A%E6%B0%B4)+OR+all_dc.description:(%E4%B8%8A%E6%B0%B4)))&_coverpage_WAR_mmisportalportlet_sort_field=score\&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqonCTCs5OwDxt&_coverpage_WAR_mmisportalportlet_sort_order=desc&_coverpage_WAR_mmisportalportlet_o=0&_coverpage_WAR_mmisportalportlet_hsf=%E4%B8%8A%E6%B0%B4
    口頭傳統和表現形式 - 宗族口述傳說（上水廖氏）\]
9.  周樹佳：《香港傳說》
10. [鄭澄、梁浩宜、蔡俊傑：《香港新界上水廖氏春祭考察報告》](http://nansha.schina.ust.hk/Article_DB/sites/default/files/pubs/news-075.03.pdf)
11. [追溯香港人的根](http://www.liberalstudies.hk/blog/ls_blog.php?id=2475)
12. [口頭傳統和表現形式 -
    宗族口述傳說（河上鄉侯氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?_coverpage_WAR_mmisportalportlet_ref=LPEC03&p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqotrcMjR6aqaN)
13. [香港的先民宗族](http://www.nanchens.com/hkcs/hkcs0501.htm)
14. 《香港地區史研究之三：粉嶺》，陳國成主編，三聯書店(香港)出版，2006年
15. [口頭傳統和表現形式 -
    宗族口述傳說（粉嶺彭氏）](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqotfh2ZGN6uZC)
16. 《新安縣志》卷二十三<藝文>中所收錄由新安知縣段巘生於雍正二年（1724年）所寫的〈創建文岡書院社學社田記〉中有記載此事。
17. [聯和墟市說粉嶺](http://www.pof.org.hk/nobarrier/nobarrier_place_content.php?id=82)
18. [五大族組織周王二院](http://paper.wenweipo.com/2003/01/19/HK0301190114.htm)
19. 《百載鑪峰》1982年第二集〈早期居民：祠堂〉