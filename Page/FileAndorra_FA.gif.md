| 描述摘要 | 安道爾足球總會會徽                                                                                                                                                                                                                                                                               |
| ---- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 來源   | <http://hqfl.dk/> (英文維基轉載)                                                                                                                                                                                                                                                              |
| 日期   | 2007年6月8日上傳                                                                                                                                                                                                                                                                             |
| 作者   | <http://hqfl.dk/>                                                                                                                                                                                                                                                                       |
| 許可   | We offer you everything we have free of charge\! We have only one demand: give credits to us\! We have spent literally thousands of hours creating these logos, but as long as you give credits to us, you can use our logos as much as you want and how you want. (英文維基轉載) {{\#switch: |

## 许可协议

[ar:ملف:Andorra FA.gif](../Page/ar:ملف:Andorra_FA.gif.md "wikilink")
[he:קובץ:Andorra FA.gif](../Page/he:קובץ:Andorra_FA.gif.md "wikilink")
[lt:Vaizdas:Andorra
FA.gif](../Page/lt:Vaizdas:Andorra_FA.gif.md "wikilink")
[mk:Податотека:Andorra
FA.gif](../Page/mk:Податотека:Andorra_FA.gif.md "wikilink")
[ro:Fişier:Andorra
FA.gif](../Page/ro:Fişier:Andorra_FA.gif.md "wikilink")
[sr:Слика:Andorra
FA.gif](../Page/sr:Слика:Andorra_FA.gif.md "wikilink")
[tr:Dosya:Andorra FA.gif](../Page/tr:Dosya:Andorra_FA.gif.md "wikilink")
[uk:Файл:Andorra FA.gif](../Page/uk:Файл:Andorra_FA.gif.md "wikilink")
[vi:Tập tin:Andorra
FA.gif](../Page/vi:Tập_tin:Andorra_FA.gif.md "wikilink")