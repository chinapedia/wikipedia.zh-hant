**异亮氨酸**（Isoleucine、簡寫：三字母: Ile；一字母:
I）是二十種基本[胺基酸的其中一種](../Page/胺基酸.md "wikilink")，幾乎在所有蛋白質的結構裡都存在著。其化學組成和[亮氨酸完全一樣](../Page/亮氨酸.md "wikilink")，但原子连接/排列顺序不同，因此与[亮氨酸有不同的性質](../Page/亮氨酸.md "wikilink")。异亮氨酸屬於[疏水性胺基酸](../Page/疏水性.md "wikilink")。

异亮氨酸有兩個[對掌中心](../Page/對掌性.md "wikilink")，所以有四種[立體異構物和兩個L](../Page/立體異構現象.md "wikilink")-异亮氨酸的[非對映體](../Page/非對映體.md "wikilink")。但無論如何，自然所存在的异亮氨酸只有一種類型，即L-异亮氨酸。

[營養學上](../Page/營養學.md "wikilink")，异亮氨酸是人類的[必需胺基酸](../Page/必需胺基酸.md "wikilink")，人体无法合成异亮氨酸，只能通过体外摄取。异亮氨酸的豐富來源有：蛋、雞、豬肉、羊肉、豆、大豆、白乾酪、牛奶、腰果、穀物。

## 異構體

<table>
<thead>
<tr class="header">
<th><p>colspan=8| <strong>異亮氨酸的形式</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/俗名.md" title="wikilink">俗名</a></strong>:</p></td>
</tr>
<tr class="even">
<td><p><strong>同義詞</strong>:</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/PubChem.md" title="wikilink">PubChem</a></strong>:</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/欧盟委员会编号.md" title="wikilink">欧盟委员会编号</a></strong>:</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/CAS号.md" title="wikilink">CAS号</a></strong>:</p></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:L-Isoleucin_-_L-Isoleucine.svg" title="fig:L-Isoleucin_-_L-Isoleucine.svg">L-Isoleucin_-_L-Isoleucine.svg</a> <a href="https://zh.wikipedia.org/wiki/File:D-isoleucine.svg" title="fig:D-isoleucine.svg">D-isoleucine.svg</a><br />
</p></td>
</tr>
<tr class="even">
<td><p><small>L</small>-isoleucine (2<em>S</em>,3<em>S</em>) and <small>D</small>-isoleucine (2<em>R</em>,3<em>R</em>)<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:L-alloisoleucine.svg" title="fig:L-alloisoleucine.svg">L-alloisoleucine.svg</a> <a href="https://zh.wikipedia.org/wiki/File:D-alloisoleucine.svg" title="fig:D-alloisoleucine.svg">D-alloisoleucine.svg</a><br />
</p></td>
</tr>
<tr class="even">
<td><p><small>L</small>-<em>allo</em>-isoleucine (2<em>S</em>,3<em>R</em>) and <small>D</small>-<em>allo</em>-isoleucine (2<em>R</em>,3<em>S</em>)</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [Isoleucine and valine
    biosynthesis](http://www.chem.qmul.ac.uk/iubmb/enzyme/reaction/AminoAcid/IleVal.html)
  - [Computational Chemistry
    Wiki](https://web.archive.org/web/20061002175119/http://www.compchemwiki.org/index.php?title=Isoleucine)

[Category:蛋白氨基酸](../Category/蛋白氨基酸.md "wikilink")
[Category:生糖氨基酸](../Category/生糖氨基酸.md "wikilink")
[Category:生酮氨基酸](../Category/生酮氨基酸.md "wikilink")
[Category:支链氨基酸](../Category/支链氨基酸.md "wikilink")
[Category:必需氨基酸](../Category/必需氨基酸.md "wikilink")