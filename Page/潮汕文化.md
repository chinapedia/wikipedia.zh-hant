[Chaozhoufenguo.jpg](https://zh.wikipedia.org/wiki/File:Chaozhoufenguo.jpg "fig:Chaozhoufenguo.jpg")\]\]

潮州文化，是指粤东[潮汕地区](../Page/潮汕.md "wikilink")[潮州民系所创造的的文化](../Page/潮州民系.md "wikilink")，包括方言、戏剧音乐、舞蹈、工艺、民俗、建筑、人文、饮食等方面。如[潮州话](../Page/潮州话.md "wikilink")、[潮剧](../Page/潮剧.md "wikilink")、[潮州音乐](../Page/潮州音乐.md "wikilink")、[潮州歌册](../Page/潮州歌册.md "wikilink")、[英歌](../Page/英歌.md "wikilink")、[潮绣](../Page/潮绣.md "wikilink")、[潮州木雕](../Page/潮州木雕.md "wikilink")、[潮汕善堂](../Page/潮汕善堂.md "wikilink")、[潮州民居](../Page/潮州民居.md "wikilink")、[潮州工夫茶](../Page/潮州工夫茶.md "wikilink")、[潮州菜等](../Page/潮州菜.md "wikilink")。

## 传统衣饰

  - [水布](../Page/水布.md "wikilink")
  - [潮汕木屐](../Page/潮汕木屐.md "wikilink")
  - [红头船](../Page/红头船.md "wikilink")

## 戏剧音乐

[HK_CWB_Moreton_Terrace_摩頓台_112th_Yu_Lan_Festival_Chiu_Chow_Opera_潮劇_天子奇緣.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_Moreton_Terrace_摩頓台_112th_Yu_Lan_Festival_Chiu_Chow_Opera_潮劇_天子奇緣.JPG "fig:HK_CWB_Moreton_Terrace_摩頓台_112th_Yu_Lan_Festival_Chiu_Chow_Opera_潮劇_天子奇緣.JPG")

  - [潮剧](../Page/潮剧.md "wikilink")
  - [潮州音乐](../Page/潮州音乐.md "wikilink")：潮州大锣鼓、潮阳笛套、潮州弦诗乐、潮州细乐、潮州庙堂音乐、潮州汉调音乐
  - [潮州铁枝木偶戏](../Page/潮州铁枝木偶戏.md "wikilink"):
  - [潮州歌册](../Page/潮州歌册.md "wikilink")

## 工艺项目

  - [潮绣](../Page/潮绣.md "wikilink")
  - [潮州木雕](../Page/潮州木雕.md "wikilink")
  - [潮州石雕](../Page/潮州石雕.md "wikilink")
  - [潮州抽纱](../Page/潮州抽纱.md "wikilink")
  - [潮州剪纸](../Page/潮州剪纸.md "wikilink")
  - [潮州陶瓷](../Page/潮州陶瓷.md "wikilink")
  - [潮州嵌瓷](../Page/潮州嵌瓷.md "wikilink")
  - [潮州花灯](../Page/潮州花灯.md "wikilink")

## 民俗項目

潮汕地区民俗繁多，且保留着很多中原地区已消失的节日。除了传统节日外，还有以下：

  - [营老爷](../Page/营老爷.md "wikilink")，[春节的游神活动](../Page/春节.md "wikilink")，各地有不同的活动形式和时间。
  - [出花园](../Page/出花园.md "wikilink")，潮汕部分地区的成人礼。
  - [人节食](../Page/人节.md "wikilink")[七菜羹](../Page/七菜羹.md "wikilink")
  - [伯公生](../Page/土地神.md "wikilink")，农历二月初二是[伯公诞辰](../Page/土地公.md "wikilink")。会在伯公庙前搭台演戏（或放映[潮剧](../Page/潮剧.md "wikilink")）连演三晚。
  - [五谷母生](../Page/神農大帝.md "wikilink")，潮州人在农历六月十五慶祝神農誕辰。
  - [施孤](../Page/施孤.md "wikilink")，即[中元节](../Page/中元节.md "wikilink")，或称[盂兰节](../Page/盂兰节.md "wikilink")。
  - 中秋[烧塔](../Page/烧塔.md "wikilink")

## 舞蹈项目

  - [英歌](../Page/英歌.md "wikilink")
  - [雙鵝舞](../Page/雙鵝舞.md "wikilink")
  - [鯉魚舞](../Page/鯉魚舞.md "wikilink")
  - [澄海蜈蚣舞](../Page/澄海蜈蚣舞.md "wikilink")
  - [饶平平布马舞](../Page/饶平平布马舞.md "wikilink")

## 潮州民居

  - [下山虎](../Page/下山虎.md "wikilink")
  - [四点金](../Page/四点金.md "wikilink")
  - [驷马拖车](../Page/驷马拖车.md "wikilink")
  - [百凤朝阳](../Page/百凤朝阳.md "wikilink")

## 饮食文化

  - [潮州菜](../Page/潮州菜.md "wikilink")
  - [潮州打冷](../Page/潮州打冷.md "wikilink")
  - [潮州糜](../Page/潮州糜.md "wikilink")
  - [潮州粿](../Page/潮州粿.md "wikilink")
  - [潮州小食](../Page/潮州小食.md "wikilink")
  - [潮州工夫茶](../Page/潮州工夫茶.md "wikilink")

## 华侨文化

  - [侨批](../Page/侨批.md "wikilink")

## 民间信仰

  - [土地公](../Page/土地公.md "wikilink")
  - [大峰祖师](../Page/大峰祖师.md "wikilink")
  - [三山国王](../Page/三山国王.md "wikilink")
  - [关公](../Page/关公.md "wikilink")
  - [妈祖](../Page/妈祖.md "wikilink")
  - [珍珠娘娘](../Page/珍珠娘娘.md "wikilink")
  - [双忠古庙](../Page/双忠古庙.md "wikilink")

## 慈善文化

  - [潮汕善堂](../Page/潮汕善堂.md "wikilink")

## 参考文献

## 外部链接

  - [潮学网](https://web.archive.org/web/20060928183426/http://www.chxw.net/)
  - [汕头大学图书馆潮汕特藏网](http://cstc.lib.stu.edu.cn)
  - [潮汕文史馆](http://www.douban.com/group/diojiu/)

[潮汕文化](../Category/潮汕文化.md "wikilink")