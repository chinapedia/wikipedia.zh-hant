[UNTK_SHEUNG_YAT_HSE.JPG](https://zh.wikipedia.org/wiki/File:UNTK_SHEUNG_YAT_HSE.JPG "fig:UNTK_SHEUNG_YAT_HSE.JPG")
[UNTK_PHASE13.JPG](https://zh.wikipedia.org/wiki/File:UNTK_PHASE13.JPG "fig:UNTK_PHASE13.JPG")
[Ngau_Tau_Kok_Chinese_Tea_Restaurant.jpg](https://zh.wikipedia.org/wiki/File:Ngau_Tau_Kok_Chinese_Tea_Restaurant.jpg "fig:Ngau_Tau_Kok_Chinese_Tea_Restaurant.jpg")為藍本的文物展覽區，設有可供休息的座位，並掛有名為「牛頭角茶餐廳」的招牌。\]\]
[Lower_Ngau_Tau_Kok_Estate_2012_part6.JPG](https://zh.wikipedia.org/wiki/File:Lower_Ngau_Tau_Kok_Estate_2012_part6.JPG "fig:Lower_Ngau_Tau_Kok_Estate_2012_part6.JPG")
**牛頭角上邨**（）是[香港的](../Page/香港.md "wikilink")[公共屋邨之一](../Page/公共屋邨.md "wikilink")，位於[牛頭角中部](../Page/牛頭角.md "wikilink")，由房屋署總建築師（3）設計，保華建築有限公司承建，並由[香港房屋委員會負責屋邨管理](../Page/香港房屋委員會.md "wikilink")。牛頭角上邨第二期和第三期獲得2007年香港環保建築協會
[香港環保建築大獎](https://web.archive.org/web/20090921003216/http://www.hk-beam.org.hk/fileLibrary/HKBeamAutumn_06_2.pdf)及2010年優質建築大獎。

## 歷史及簡介

重建前的牛頭角上邨為前[政府廉租屋邨](../Page/政府廉租屋邨.md "wikilink")，原稱**牛頭角政府廉租屋邨**，共有九座政府廉租屋大廈，分別於1967年至1968年間興建。1973年，為配合房屋委員會成立，而改稱為牛頭角上邨；而當時位於該邨隔鄰的「牛頭角新區（[徙置區](../Page/徙置區.md "wikilink")）」則改稱為[牛頭角下邨](../Page/牛頭角下邨.md "wikilink")。按牛頭角政府廉租屋邨的興建計劃，分為「甲區」和「乙區」，後來落成的第1-5座位於甲區，而第9-12座則位於乙區。

由於當時第6-8座的原興建選址是在現[花園大廈喜鵲樓一帶或者是牛下一區](../Page/花園大廈.md "wikilink")，可能後來那些地批了給房協又或者興建了徙置區，政府沒有興建第6-8座。
另一個說法是當年第6-8座的選址後來改為山坡上兩所小學的校舍。

## 重建

根據政府的「整體重建計劃」，早於二十世紀九十年代初已計劃把牛頭角上邨重建，計劃分為「甲區」（舊第1-5座）和「乙區」（舊第9-12座）。最終「乙區」的第9-12座於1998年底完成清拆，原址於2000年展開地基工程，但因為工程展開後發現地底岩石存有問題，需要額外加固地基。其後重建成為常逸樓、常滿樓及常悅樓，名為「牛頭角上邨第一期」。並於2002年夏季開始入伙，讓「甲區」餘下未獲搬遷的住戶優先選擇。而「[甲區](../Page/甲區.md "wikilink")」的所有住戶遷出後，舊第1至5座則於2003年年底完成清拆，地基工程2006年已展開，共有6幢大樓，高40層，接收[牛頭角下邨](../Page/牛頭角下邨.md "wikilink")（二區）的拆建戶。新居已於2009年6月起陸續入-{伙}-，興建上邨的預算成本約13億元（以當時造價計算）。

因應近年[屏風樓問題](../Page/屏風樓.md "wikilink")，牛頭角上邨重建計劃是首個採用[微氣候研究工程](../Page/微氣候.md "wikilink")。此外，為增加居民的歸屬感，房署於2003年起便透過各大小工作坊及簡介會，讓居民參與上邨的規劃。房署建築師王國興指，透過工作坊與居民共同商討後，提出以[茶餐廳為藍本設計文物展示區](../Page/茶餐廳.md "wikilink")，並從下邨收集到的三個貨櫃舊物中，選出部分擺放於展示區，當中包括雕花鐵閘、招牌及傢俬等（其後[得寶花園商場牛下新城亦採用同類概念](../Page/得寶花園.md "wikilink")）。

房署也因應居民的意見，稍為改動了上邨的[設計](../Page/設計.md "wikilink")，例如將原本以扶手電梯連接地下至平台，改為以升降機代替，並將通向各座住宅的斜路改為平路，方便[長者及](../Page/長者.md "wikilink")[傷殘人士出入](../Page/傷殘.md "wikilink")。至於原本偏離大廈出入口的座椅，亦重新放置於各大廈出入口附近，方便長者聚集休憩。另外，為了方便傷健人士與長者，各住宅樓層的公共走廊都加設了扶手。\[1\]

## 現時樓宇

### 重建後的牛頭角上邨

牛頭角上邨重建第一期，兩座和諧一型（選擇六）公屋及單向設計大廈合共3座約20-40層高大廈。

**牛頭角上邨重建第二及三期**是房屋署首批由新和諧式過渡至[非標準設計大廈的公屋](../Page/非標準設計大廈.md "wikilink")。合共6座約40層高大廈，提供4584個單位。前房屋署總建築師伍灼宜表示，大廈佈局是透過微氣候分析地盤日照和風向來決定，6座大廈分成兩組排列興建，分別享受山景和海景，並留下通風廊加強通風。邨內設有兒童遊樂場和長者健身區，照顧不同人口。

<table>
<thead>
<tr class="header">
<th><p>樓宇名稱（座號）</p></th>
<th><p>樓宇類型</p></th>
<th><p>落成年份</p></th>
<th><p>所屬重建計劃</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>常滿樓（第1期第1座）</p></td>
<td><p>和諧一型</p></td>
<td><p>2002</p></td>
<td><p>第一期</p></td>
</tr>
<tr class="even">
<td><p>常逸樓（第1期第2座）</p></td>
<td><p>和諧一型連新和諧附翼第四型</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>常悅樓（第1期SAB座）</p></td>
<td><p>單向設計大廈</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>常興樓（第2期第1座）</p></td>
<td><p>特別設計新和諧式大廈<br />
（<a href="../Page/非標準設計大廈.md" title="wikilink">非標準式設計</a>）[2]</p></td>
<td><p>2009</p></td>
<td><p>第二期</p></td>
</tr>
<tr class="odd">
<td><p>常盛樓（第2期第2座）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>常富樓（第2期第3座）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>常榮樓（第3期第4座）</p></td>
<td><p>第三期</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>常康樓（第3期第5座）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>常泰樓（第3期第6座）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 前代樓宇

| 重建前的牛頭角上邨 |
| --------- |
| 樓宇座號      |
| 第1座       |
| 第2座       |
| 第3座       |
| 第4座       |
| 第5座       |
| 第9座       |
| 第10座      |
| 第11座      |
| 第12座      |

## 教育及福利設施

### 幼稚園

  - [平安福音堂幼稚園（牛頭角）](http://www.peck.edu.hk/ngautaukok/)（2003年創辦）（位於常滿樓地下）

### 小學

  - [聖公會基顯小學](../Page/聖公會基顯小學.md "wikilink")（1968年創辦）
  - [香港道教聯合會雲泉學校](http://www.hktawts.edu.hk)（1969年創辦）
  - [閩僑小學](http://www.mkaps.edu.hk)（1969年創辦）

### 綜合家庭服務中心

  - 社會福利署牛頭角綜合家庭服務中心（位於常悅樓3樓平台）

### 綜合青少年服務中心

  - [循道衛理觀塘社會服務處牛頭角青少年綜合服務中心](https://sub.ktmss.org.hk/youth/ntkit/info)（位於牛頭角上邨中央廣場）

[Upper_Ngau_Tau_Kok_Estate_Intergrated_Children_&_Youth_Services_Centre.jpg](https://zh.wikipedia.org/wiki/File:Upper_Ngau_Tau_Kok_Estate_Intergrated_Children_&_Youth_Services_Centre.jpg "fig:Upper_Ngau_Tau_Kok_Estate_Intergrated_Children_&_Youth_Services_Centre.jpg")

### 長者鄰舍中心

  - [竹園區神召會牛頭角長者鄰舍中心](http://www.pchk.org.hk/ntk)（1989年創辦）（位於常盛樓及常富樓地下）

## 前代學校

  - [牛頭角天主教小學](../Page/牛頭角天主教小學.md "wikilink")（已遷往[樂華邨並改名為](../Page/樂華邨.md "wikilink")[樂華天主教小學](../Page/樂華天主教小學.md "wikilink")）

## 商場

[Upper_Ngau_Tau_Kok_Estate_Entry_Plaza_View1.jpg](https://zh.wikipedia.org/wiki/File:Upper_Ngau_Tau_Kok_Estate_Entry_Plaza_View1.jpg "fig:Upper_Ngau_Tau_Kok_Estate_Entry_Plaza_View1.jpg")
牛頭角上邨商場為單層式建築、座落於牛頭角上邨第二期地下、[牛頭角道與](../Page/牛頭角道.md "wikilink")[安德道交界](../Page/安德道.md "wikilink")，已於2009年7月底落成，同年9月正式啟用。該商場一共有6個舖位，提供總商用面積約1,100平方米。包括有一部匯豐銀行自助櫃員機、[美心MX快餐](../Page/美心MX.md "wikilink")、7-11便利店、西藥房、牛頭角街坊福利會、西醫診所和佔地約608平方米的[U購超級市場](../Page/U購.md "wikilink")，供應居民日常所需。此外，商場內並設有循道衛理牛頭角青少年綜合服務中心。

[Upper_Ngau_Tau_Kok_Estate_Garden_Overview_2009.jpg](https://zh.wikipedia.org/wiki/File:Upper_Ngau_Tau_Kok_Estate_Garden_Overview_2009.jpg "fig:Upper_Ngau_Tau_Kok_Estate_Garden_Overview_2009.jpg")
[Lower_Ngau_Tau_Kok_Estate_2012_part8.JPG](https://zh.wikipedia.org/wiki/File:Lower_Ngau_Tau_Kok_Estate_2012_part8.JPG "fig:Lower_Ngau_Tau_Kok_Estate_2012_part8.JPG")行人天橋\]\]

## 相片集

<File:NEW> UPPER NGAU TAU KOK EST
BOARD.JPG|取代位於[安善道的舊牛頭角上邨邨牌的新式邨牌](../Page/安善道.md "wikilink")（2009年5月）
<File:UPPER> NGAU TAU KOK EST
BOARD.JPG|已消失的[安善道前代牛頭角上邨邨牌](../Page/安善道.md "wikilink")（2008年7月）
<File:UNTK> SHEUNG YUET HSE OT.JPG|牛頭角上邨常悅樓外部（2008年11月） <File:UNTK>
SHEUNG YUET HSE IN.JPG|牛頭角上邨常悅樓內部（2008年11月） <File:Upper> Ngau Tau Kok
Estate Phase One 2006.jpg|遠眺牛頭角上邨，可見重建中的新樓宇（2006年8月） <File:Upper> NTK
Estate Overview 20070710.jpg|現有及重建中的牛頭角上邨新廈（2007年7月）
[File:UNTK_070827_P2.JPG|重建中的牛頭角上邨新廈，圖中為牛頭角上邨第二期（2007年8月](File:UNTK_070827_P2.JPG%7C重建中的牛頭角上邨新廈，圖中為牛頭角上邨第二期（2007年8月)）
<File:Upper> Ngau Tau Kok Estate 200712.jpg|重建中的牛頭角上邨新廈（2007年12月）
<File:Upper> Ngau Tau Kok Estate
200807.jpg|重建中的牛頭角上邨新廈，最左前方的新廈為常泰樓。（2008年7月）
<File:UNTK>
CWR.JPG|於[佐敦谷平山](../Page/佐敦谷.md "wikilink")（[彩榮路](../Page/彩榮路.md "wikilink")）遠眺牛頭角。上方為牛頭角上邨第一至第二期，下方為即將拆卸的[牛頭角下邨二區](../Page/牛頭角下邨.md "wikilink")（2008年9月）
<File:UP> NGAU TAU KOK EST.jpg|完成重建的牛頭角上邨，右方為牛頭角上邨第二期 <File:U> Ngau Tau
Kok Estate 2008.JPG|正式竣工的牛頭角上邨新廈（2008年10月） <File:UNTK>
PHASE2.JPG|牛頭角上邨第二期外觀（2008年11月） <File:UNTK>
PHASE3.JPG|牛頭角上邨第三期外觀（2008年11月） <File:Upper> Ngau Tau Kok Estate
Playground 1.jpg|牛頭角上邨第三期遊樂場 <File:Ngau> Tau Kok Estate 2013 08
part2.JPG|連接下邨與上邨的行人天橋

## 外景場地

  - 牛頭角上邨清拆前，曾被[香港電台借用作拍攝電視劇](../Page/香港電台.md "wikilink")《[執法群英](../Page/執法群英.md "wikilink")》-半獸人調査案件所在大廈內的部分。\[3\]
  - 牛頭角上邨第二及第三期在興建時，曾被借用作拍攝電視劇《[廉政行動2007](../Page/廉政行動2007.md "wikilink")》單元二「沙丘城堡」短樁居屋建築地盤部分。\[4\]
  - [香港電台電視節目](../Page/香港電台.md "wikilink")《流金頌》第八集《落葉歸根》\[5\]
  - [牛下女高音](../Page/牛下女高音.md "wikilink")2018年拍攝

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/牛頭角站.md" title="wikilink">牛頭角站B</a>2出入口、<a href="../Page/九龍灣站.md" title="wikilink">九龍灣站A出入口</a></li>
</ul>
<dl>
<dt><a href="../Page/牛頭角道.md" title="wikilink">牛頭角道</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港公共小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>觀塘至<a href="../Page/青山道.md" title="wikilink">青山道線</a> (上午服務)[6]</li>
<li>觀塘/<a href="../Page/黃大仙.md" title="wikilink">黃大仙至青山道線</a> (通宵服務)[7]</li>
<li>觀塘至<a href="../Page/美孚.md" title="wikilink">美孚線</a>[8]</li>
<li>觀塘至<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣</a>/<a href="../Page/佐敦道.md" title="wikilink">佐敦道線</a> (24小時服務)[9]</li>
<li>觀塘至<a href="../Page/旺角.md" title="wikilink">旺角線</a> (24小時服務)[10]</li>
<li>牛頭角至旺角線[11]</li>
<li>牛頭角至佐敦道線 (上午服務)[12]</li>
<li><a href="../Page/紅磡.md" title="wikilink">紅磡至觀塘線</a> (24小時服務)[13]</li>
<li>觀塘至香港仔線 (下午服務)[14]</li>
<li>觀塘至西環線 (上午服務)[15]</li>
<li>銅鑼灣至觀塘線 (通宵服務)[16]</li>
<li>灣仔至觀塘線 (通宵服務)[17]</li>
</ul>
<dl>
<dt><a href="../Page/觀塘道.md" title="wikilink">觀塘道</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 相關條目

  - [牛頭角下邨](../Page/牛頭角下邨.md "wikilink")

## 注釋

<references />

## 外部連結

  - [香港房屋委員會牛頭角上邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2788)
  - [香港房屋委員會牛頭角上邨商場資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=3&id=11222)
  - [香港地方: 政府廉租屋](http://www.hk-place.com/view.php?id=204)
  - [香港地方: 討論文庫: 牛頭角上下邨](http://www.hk-place.com/db.php?post=d005010)
  - [香港地方:
    牛頭角上邨第9-12座樓宇平面圖](http://www.hk-place.com/database/i005023b.jpg)
  - [彩盈俱樂部牛上專區 -
    牛頭角上邨居民臨時聚腳地](http://cyclub.happyhongkong.com/forumdisplay.php?fid=162)

[Category:牛頭角](../Category/牛頭角.md "wikilink")
[Category:2009年完工建築物](../Category/2009年完工建築物.md "wikilink")
[Category:2003年完工建築物](../Category/2003年完工建築物.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  <http://www.info.gov.hk/gia/general/200704/08/P200704040285.htm>
2.  備註：第二期及第三期的大廈雖是非標準，但單位外型與新和諧式大廈相似
3.  [YouTube：執法群英 -
    半獸人(2003)](http://www.youtube.com/watch?v=9JllSY4c62Q)
4.  [廉政行動2007單元二](../Page/廉政行動2007.md "wikilink")：「沙丘城堡」內容([1](http://www.ichannel.icac.hk/tc/categorylist.aspx?video=887))
5.  [YouTube：流金頌 第八集 : 落葉歸根](http://www.youtube.com/watch?v=xrPodMmRbPA)
6.  [觀塘協和街 — 青山道香港紗廠](http://www.16seats.net/chi/rmb/r_k02.html)
7.  [觀塘及黃大仙 — 青山道](http://www.16seats.net/chi/rmb/r_k22.html)
8.  [觀塘協和街 — 美孚](http://www.16seats.net/chi/rmb/r_k64.html)
9.  [觀塘同仁街 — 佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
10. [觀塘同仁街 — 旺角先達廣場／奧運站](http://www.16seats.net/chi/rmb/r_k31.html)
11. [牛頭角站 — 旺角登打士街](http://www.c16seats.net/chi/rmb/r_k24.html)
12. [牛頭角站 — 佐敦道吳松街](http://www.16seats.net/chi/rmb/r_k56.html)
13. [紅磡差館里 ＞ 牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_k15.html)
14. [香港仔湖北街 — 觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
15. [觀塘宜安街 ＞ 西環卑路乍街, 西環修打蘭街 ＞
    觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
16. [銅鑼灣鵝頸橋 — 牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
17. [中環／灣仔 ＞ 牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)