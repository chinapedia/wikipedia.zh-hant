**帕爾杜比采州**（），是[捷克的一個](../Page/捷克.md "wikilink")[州](../Page/捷克行政區劃.md "wikilink")，歷史上屬於[波希米亞的東部和](../Page/波希米亞.md "wikilink")[摩拉維亞西北部的一部分](../Page/摩拉維亞.md "wikilink")。面積4,519
平方公里，人口506,024（2006年）。首府[帕爾杜比采](../Page/帕爾杜比采.md "wikilink")。下分五區。

[\*](../Category/帕尔杜比采州.md "wikilink")
[P](../Category/捷克州份.md "wikilink")
[Category:波希米亞](../Category/波希米亞.md "wikilink")
[Category:摩拉維亞](../Category/摩拉維亞.md "wikilink")