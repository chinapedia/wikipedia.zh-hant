**菲律賓眾議院大樓爆炸案**發生于2007年11月13日晚間8點，在[菲律賓國會](../Page/菲律賓國會.md "wikilink")（[Batasang
Pambansa](../Page/:en:Batasang_Pambansa.md "wikilink")）大樓的南翼，即眾議院大樓發生爆炸。該大樓位於[奎松市東北邊](../Page/奎松市.md "wikilink")。目擊者指稱，爆炸時地面會震動，而且天花板的一部分塌下來，建築物的外牆塊散於周圍\[1\]。爆炸产生的强烈震波摧毁了大堂的部分天花板，爆炸引发的大火还烧毁了部分停在楼外的汽车。\[2\]

眾議會[議長](../Page/菲律賓眾議會議長.md "wikilink")（[Speaker](../Page/:en:Speaker_of_the_Philippine_House_of_Representatives.md "wikilink")）[何塞·德貝內西亞](../Page/何塞·德貝內西亞.md "wikilink")（[Jose
de Venecia,
Jr.](../Page/:en:Jose_de_Venecia,_Jr..md "wikilink")）在接受廣播訪問時指出「在國會大樓南翼有炸彈爆炸」。因為剛剛散會，所以當爆炸發生時許多眾議員仍然在大樓內，他們當時還沒有離開。爆炸之後，他們被要求不要離開大樓。\[3\]

爆炸時，代表[菲律賓全國婦女組織聯盟](../Page/菲律賓全國婦女組織聯盟.md "wikilink")（[GABRIELA](../Page/:en:GABRIELA.md "wikilink")）政黨的[露斯維敏塔·伊拉甘](../Page/露斯維敏塔·伊拉甘.md "wikilink")（Luzviminda
Ilagan）眾議員司機馬提雅爾·達度（Marcial
Taldo）現場立即死亡，而[露斯維敏塔·伊拉甘](../Page/露斯維敏塔·伊拉甘.md "wikilink")（Luzviminda
Ilagan）眾議員、代表[東內格羅省](../Page/東內格羅省.md "wikilink")（[Negros
Oriental](../Page/:en:Negros_Oriental.md "wikilink")）的[亨利·特維斯](../Page/亨利·特維斯.md "wikilink")（[Pryde
Henry
Teves](../Page/:en:Pryde_Henry_Teves.md "wikilink")）眾議員，以及另外兩位眾議員則是受傷\[4\]。達度的屍體在伊拉甘的座車中被發現。

[议长](../Page/菲律賓眾議會議長.md "wikilink")[德贝内西亚在](../Page/何塞·德貝內西亞.md "wikilink")[菲律宾总统](../Page/菲律宾总统.md "wikilink")[阿罗约命令](../Page/阿罗约.md "wikilink")[菲律賓國家警察](../Page/菲律賓國家警察.md "wikilink")（[Philippine
National
Police](../Page/:en:Philippine_National_Police.md "wikilink")）总长[阿維利諾·拉松](../Page/阿維利諾·拉松.md "wikilink")（Avelino
Razon）亲自监督排查之前便下令对整个大楼进行一次彻底的排查，確定大樓內沒有其他的炸彈。\[5\]

眾議員[瓦哈卜·阿克巴爾在爆炸中受到重傷](../Page/瓦哈卜·阿克巴爾.md "wikilink")，送至醫院之後不治，享年47歲。\[6\]眾議員[亨利·特維斯](../Page/亨利·特維斯.md "wikilink")（[Pryde
Henry
Teves](../Page/:en:Pryde_Henry_Teves.md "wikilink")）的中耳遭受嚴重的傷害，可能會導致他失去聽覺。更早的報告敘述他的腿需要截肢，但是已經被醫生的診斷駁回。醫生診斷認為只要小範圍的截除手術就可以使議員的腳復原。

此次袭击疑似是针对议员[瓦哈卜·阿克巴尔的刺杀事件](../Page/瓦哈卜·阿克巴尔.md "wikilink")，因为他事前已经多次遭到多个组织的恐吓。\[7\]

## 眾議會本週議程

## 爆炸

## 罹難者

  - 眾議員[瓦哈卜·阿克巴爾](../Page/瓦哈卜·阿克巴爾.md "wikilink")，代表[巴西蘭德省](../Page/巴西蘭德省.md "wikilink")（[Basilan](../Page/:en:Basilan.md "wikilink")）的巴西蘭德選區（[Legislative
    district of
    Basilan](../Page/:en:Legislative_district_of_Basilan.md "wikilink")）\[8\]
  - Junaskiri Hayudini，[瓦哈卜·阿克巴爾的國會助理](../Page/瓦哈卜·阿克巴爾.md "wikilink")
  - 馬提雅爾·達度（Marcial
    Taldo），[菲律賓全國婦女組織聯盟](../Page/菲律賓全國婦女組織聯盟.md "wikilink")（[GABRIELA](../Page/:en:GABRIELA.md "wikilink")）政黨代表[露斯維敏塔·伊拉甘](../Page/露斯維敏塔·伊拉甘.md "wikilink")（Luzviminda
    Ilagan）眾議員的司機\[9\]
  - Maan Gale Bustalino，眾議員[亨利·特維斯](../Page/亨利·特維斯.md "wikilink")（[Pryde
    Henry Teves](../Page/:en:Pryde_Henry_Teves.md "wikilink")）的國會助理

## 傷者

  - 眾議員[亨利·特維斯](../Page/亨利·特維斯.md "wikilink")（[Pryde Henry
    Teves](../Page/:en:Pryde_Henry_Teves.md "wikilink")），代表[東內格羅省](../Page/東內格羅省.md "wikilink")（[Negros
    Oriental](../Page/:en:Negros_Oriental.md "wikilink")）第三選區
  - 眾議員[露斯維敏塔·伊拉甘](../Page/露斯維敏塔·伊拉甘.md "wikilink")（Luzviminda
    Ilagan），代表[菲律賓全國婦女組織聯盟](../Page/菲律賓全國婦女組織聯盟.md "wikilink")（[GABRIELA](../Page/:en:GABRIELA.md "wikilink")）

另有近十人受傷。

## 菲律賓國家警察措施

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

[Category:2007年爆炸案](../Category/2007年爆炸案.md "wikilink")
[Category:2007年恐怖活动](../Category/2007年恐怖活动.md "wikilink")
[Category:菲律賓恐怖活動](../Category/菲律賓恐怖活動.md "wikilink")
[Category:菲律賓謀殺案](../Category/菲律賓謀殺案.md "wikilink")
[Category:菲律宾爆炸案](../Category/菲律宾爆炸案.md "wikilink")
[Category:菲律宾众议院](../Category/菲律宾众议院.md "wikilink")
[Category:菲律宾政治事件](../Category/菲律宾政治事件.md "wikilink")
[Category:立法機構襲擊事件](../Category/立法機構襲擊事件.md "wikilink")
[Category:2007年11月](../Category/2007年11月.md "wikilink")

1.  [Blast rocks Philippine
    congress](http://news.bbc.co.uk/2/hi/asia-pacific/7092600.stm)
    BBCNews.com

2.  [菲律宾众议院大爆炸2人死9伤](http://news.bbc.co.uk/chinese/simp/hi/newsid_7090000/newsid_7092600/7092623.stm)
    BBC 中文网

3.  [Speaker says House blast that killed driver a
    bomb](http://newsinfo.inquirer.net/breakingnews/nation/view_article.php?article_id=100700)

4.
5.
6.  [Rep. Wahab Akbar dies in hospital after Batasan
    blast](http://www.gmanews.tv/story/68563/Rep-Wahab-Akbar-dies-in-hospital-after-Batasan-blast)
    GMANews.tv

7.  [菲律宾国会发生爆炸
    疑似为行刺](http://cn.reuters.com/article/asiaNews/idCNChina-170820071113)
    路透中文网

8.  [Rep. Wahab Akbar dies in hospital after Batasan
    blast](http://www.gmanews.tv/story/68563/Rep-Wahab-Akbar-dies-in-hospital-after-Batasan-blast)
    GMANews.tv

9.