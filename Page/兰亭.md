**兰亭古迹**是中国浙江[绍兴的重要文化古迹和旅游胜地](../Page/绍兴.md "wikilink")，位于[绍兴城区西南](../Page/绍兴.md "wikilink")13公里的[兰渚山麓](../Page/兰渚.md "wikilink")。[东晋](../Page/东晋.md "wikilink")[永和九年](../Page/永和_\(晋穆帝\).md "wikilink")（353年）三月初三，[王羲之与](../Page/王羲之.md "wikilink")[谢安](../Page/谢安.md "wikilink")、[王献之等](../Page/王献之.md "wikilink")40多位名士在此举办修禊集会，王羲之“微醉之中，振笔直遂”，写下了著名的《[兰亭集序](../Page/兰亭集序.md "wikilink")》。

## 主要景点

  - **兰亭**御题碑，书“兰亭”二字，為[康熙所题](../Page/康熙.md "wikilink")。御题碑在文革時受到破壞，但後來得到復修，所以题碑有部分殘缺。
  - **鹅池**，相传为[王羲之养](../Page/王羲之.md "wikilink")[鹅的地方](../Page/鹅.md "wikilink")，池旁竖立刻“鹅池”二字石碑，相传为王羲之父子合写而成，但二字却宛如浑然天成，丝毫没有两人所写的笔意。
  - **流觞亭**，前为一条曲折的小溪，传当年王羲之就是在溪旁以独有的“[曲水流觞](../Page/曲水流觞.md "wikilink")”的方式与众友饮酒赋诗，为[蘭亭集會](../Page/蘭亭集會.md "wikilink")，并写下了流芳千古的书作《[兰亭序](../Page/兰亭序.md "wikilink")》。
  - 康乾御题碑，该碑为海内外仅有的在阴阳两面分别刻有[乾隆和](../Page/乾隆.md "wikilink")[康熙御题字的石碑](../Page/康熙.md "wikilink")。
  - **右军祠**，内有一泓墨池，传因当年王羲之习书后在此洗笔，年长日久致使池水尽黑。祠内大厅供奉王羲之像并陈列各种版本的《兰亭序》，当为历次晋圣活动的主要场所。
  - **兰亭书法博物馆**，内陈列名家书法作品。

## 碑刻

[绍兴兰亭古迹公园除有与王羲之有关的文物及仿古建筑外](../Page/绍兴.md "wikilink")，尚保存多种珍贵碑刻，包括；

  - [宋仁宗临兰亭序碑](../Page/宋仁宗.md "wikilink")
  - 宋[朱熹兰亭序碑](../Page/朱熹.md "wikilink")
  - 元[赵孟頫临兰亭序碑](../Page/赵孟頫.md "wikilink")
  - 明[王铎](../Page/王铎.md "wikilink")（1592年-1652年）临兰亭序碑
  - 清[康熙帝御笔兰亭集序碑](../Page/康熙帝.md "wikilink")
  - 清[乾隆帝御笔隆兰亭序帖](../Page/乾隆帝.md "wikilink")
  - 清[郑板桥临兰亭序碑](../Page/郑板桥.md "wikilink")
  - 沈尹默临兰亭序碑

Image:SongRenZong
Lanting.JPG|[宋仁宗御笔](../Page/宋仁宗.md "wikilink")[兰亭集序碑](../Page/兰亭集序.md "wikilink")
Image:Zhuxi Lanting.JPG|宋[朱熹兰亭集序碑](../Page/朱熹.md "wikilink")
Image:Kangxi Lanting tablet.JPG|康熙御笔兰亭集序碑 Image:Emperor Qianlong
Lanting.JPG|乾隆御笔兰亭集序帖 Image:Zhao Mengfu Lanting.JPG|趙孟頫兰亭集序碑 Image:Zheng
Banqiao Lanting.JPG|[郑板桥兰亭集序碑](../Page/郑板桥.md "wikilink")

## 参考

  - [绍兴兰亭](http://www.china.com.cn/chinese/zhuanti/gdyl/562400.htm) 中国网

## 相关条目

  - [王羲之](../Page/王羲之.md "wikilink")
  - 《[兰亭集序](../Page/兰亭集序.md "wikilink")》

[Category:绍兴文化](../Category/绍兴文化.md "wikilink")
[Category:绍兴旅游](../Category/绍兴旅游.md "wikilink")
[Category:柯桥区](../Category/柯桥区.md "wikilink")
[Category:王羲之](../Category/王羲之.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")