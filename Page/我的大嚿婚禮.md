《**我的大嚿婚禮**》（），於2002年提名奧斯卡金像獎的[浪漫喜劇電影](../Page/浪漫喜劇電影.md "wikilink")，由[妮雅·瓦達蘿絲編劇與主演](../Page/妮雅·瓦達蘿絲.md "wikilink")，由[喬爾·瑞克執導](../Page/喬爾·瑞克.md "wikilink")。是[美國](../Page/美國.md "wikilink")2002年上映的電影中票房最高的第五名，241,438,208元[美金](../Page/美金.md "wikilink")，而且也是歷史上票房最高的浪漫喜劇。這也是北美每週票房中從來沒有成為第一名的賣座電影。2003年，該片被提名為[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")[最佳原創劇本](../Page/最佳原創劇本.md "wikilink")。

## 情節

該片講述作為一名[希臘裔美國人的女主角如何衝破重重困難](../Page/希臘.md "wikilink")，與他的非希臘裔心上人完婚。本片的主題除了講述兩地文化的差異以外，亦有隱晦的提及民族間的和解。為了融入這個希臘家庭，這位男朋友亦作了各種各樣的忍讓：除了學習各種希臘文化以外，更在各方面做得務求比一個希臘人還要更希臘。對於其他人來說，他不過是女主角的男朋友，一個和一般希臘人無異的人。

## 卡司

  - [妮雅·瓦達蘿絲](../Page/妮雅·瓦達蘿絲.md "wikilink") as Fotoula "Toula"
    Portokalos
  - [約翰·寇貝特](../Page/約翰·寇貝特.md "wikilink") as Ian Miller
  - [麥可·康世坦丁](../Page/麥可·康世坦丁.md "wikilink") as Kostas "Gus" Portokalos
  - [蕾妮·卡珊](../Page/蕾妮·卡珊.md "wikilink") as Maria Portokalos
  - [Andrea Martin](../Page/Andrea_Martin.md "wikilink") as Aunt Voula
  - [Stavroula Logothettis](../Page/Stavroula_Logothettis.md "wikilink")
    as Athena Portokalos
  - [Louis Mandylor](../Page/Louis_Mandylor.md "wikilink") as Nick
    Portokalos
  - [Gia Carides](../Page/Gia_Carides.md "wikilink") as Cousin Nikki
  - [Joey Fatone](../Page/Joey_Fatone.md "wikilink") as Cousin Angelo
  - [Bruce Gray](../Page/Bruce_Gray.md "wikilink") as Rodney Miller
  - [Fiona Reid](../Page/Fiona_Reid.md "wikilink") as Harriet Miller
  - [Arielle Sugarman](../Page/Arielle_Sugarman.md "wikilink") as Paris
    Miller
  - [Jayne Eastwood](../Page/Jayne_Eastwood.md "wikilink") as Mrs. White

## 參考資料

  -
  -
  -
  -
  -
[Category:2002年電影](../Category/2002年電影.md "wikilink")
[Category:美國喜劇片](../Category/美國喜劇片.md "wikilink")
[Category:芝加哥背景電影](../Category/芝加哥背景電影.md "wikilink")
[Category:婚禮相關電影](../Category/婚禮相關電影.md "wikilink")
[Category:異族戀題材電影](../Category/異族戀題材電影.md "wikilink")