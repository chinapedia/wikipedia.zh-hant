**洪棄生**（），原名**攀桂**，字**月樵**，[臺灣清末](../Page/臺灣.md "wikilink")、[日治時期重要文學家](../Page/日治時期.md "wikilink")。[彰化](../Page/彰化縣.md "wikilink")[鹿港人](../Page/鹿港鎮.md "wikilink")，[臺灣割讓後改名](../Page/臺灣割讓.md "wikilink")**洪繻**，字**棄生**，
[以字行](../Page/以字行.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[光緒十年](../Page/光緒.md "wikilink")（1884年）以第一名入學[生員](../Page/生員.md "wikilink")，光緒廿一年（1895年）[馬關條約後](../Page/馬關條約.md "wikilink")，絕意仕進，不再[科舉](../Page/科舉.md "wikilink")，遂潛心於詩詞、古文，把[臺灣歷史與社會刻劃於詩文中](../Page/臺灣歷史.md "wikilink")，發揮了不屈不撓的[反殖民精神](../Page/反殖民.md "wikilink")。[楊雲萍評洪](../Page/楊雲萍.md "wikilink")：「近代學人之中，博聞篤學，抱樸守貞，儼然有古大師之風。」著有《寄鶴齋詩話》、《寄鶴齋文臠》、《臺灣戰紀》、《中東戰紀》等，著作甚豐，其〈鹿港乘桴記〉被選入[臺灣](../Page/臺灣.md "wikilink")[高中國文文言文教材](../Page/高中.md "wikilink")。

洪在世時，堅持衣髮如舊。將原屬[滿人的](../Page/滿人.md "wikilink")[薙髮留辮](../Page/剃髮令.md "wikilink")，作為其認同[中華文化與](../Page/中華文化.md "wikilink")[反日的作法](../Page/反日.md "wikilink")。後來雖遭日本警察強行剪去辮髮，仍留下「我生跼蹐何不辰，垂老乃為斷髮民，披髮欲向中華去，海天水黑波粼粼」、「長嘆無天可避秦，中華遠海總蒙塵，本為海島埋頭客，更變伊川披髮人。」等詩作。

其子為作家[洪炎秋](../Page/洪炎秋.md "wikilink")，以[散文名世](../Page/散文.md "wikilink")\[1\]，在[二二八事變時被誣指](../Page/二二八事變.md "wikilink")「鼓動暴動，陰謀叛國」，後獲平反。

注：其詩，為日本作家[佐藤春夫所稱道](../Page/佐藤春夫.md "wikilink")。略如：“…連讀都讀不來的人，固然無法論其詩，不過從此片麟隻句而言，其高踏的世外人的高邁氣魄，讓我覺得如同讀到用漢字寫成的波特萊爾詩集之類的感覺。”

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [臺灣古典文學瑰寶—洪棄生](http://proxy.yphs.tp.edu.tw/~ypi/hpclasssample/5learning/5_5/5_5_11.htm)
  - [果子離\>台灣的不合作主義者洪棄生](https://web.archive.org/web/20160304074214/http://2007.taiwangoodlife.org/story/20070409/125)
  - [流離累索：洪棄生與羅大佑](http://blog.roodo.com/giff/archives/2208536.html)
  - [洪棄生_百度百科](http://baike.baidu.com/view/6230990.htm)

[Category:台灣清治時期人物](../Category/台灣清治時期人物.md "wikilink")
[Category:台灣日治時期作家](../Category/台灣日治時期作家.md "wikilink")
[Category:台灣儒學學者](../Category/台灣儒學學者.md "wikilink")
[Category:鹿港人](../Category/鹿港人.md "wikilink")
[Qi棄](../Category/洪姓.md "wikilink")

1.  [葉石濤](../Page/葉石濤.md "wikilink")《[台灣文學史綱](../Page/台灣文學史綱.md "wikilink")》