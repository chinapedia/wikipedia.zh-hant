《**財經新聞**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")[新聞及資訊部製作的財經新聞報道節目](../Page/無綫新聞.md "wikilink")，由[翡翠台播出](../Page/翡翠台.md "wikilink")，於[1987年](../Page/1987年.md "wikilink")[3月9日正式開始啟播](../Page/3月9日.md "wikilink")，節目形式與《[今日財經](../Page/無綫電視今日財經.md "wikilink")》相近。主要講述當日[香港以及](../Page/香港.md "wikilink")[國際詳細重點財經新聞](../Page/國際.md "wikilink")。

## 節目名稱

《財經新聞》在的名稱原為《財經消息》，[1993年](../Page/1993年.md "wikilink")[6月14日起](../Page/6月14日.md "wikilink")，改稱《財經新聞》。

## 播映安排

[1987年](../Page/1987年.md "wikilink")[3月9日啟播開始](../Page/3月9日.md "wikilink")，曾安排在《[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")》後，於星期一至五18:50播出，而背景音樂由開播起一直採用至今（並且與《[今日財經](../Page/無綫電視今日財經.md "wikilink")》及[明珠台](../Page/明珠台.md "wikilink")《[財經消息](../Page/無綫電視財經消息.md "wikilink")》一致）。

[1989年](../Page/1989年.md "wikilink")[10月9日起的播放時間改為](../Page/10月9日.md "wikilink")18:20。

[1993年](../Page/1993年.md "wikilink")[6月14日起](../Page/6月14日.md "wikilink")，改名為《**財經新聞**》後，播放時間改為18:15。

[1995年](../Page/1995年.md "wikilink")[9月18日起的播放時間改為](../Page/9月18日.md "wikilink")17:55，並將背景音樂聲調略為調高，沿用至今。

[1996年](../Page/1996年.md "wikilink")[2月26日起](../Page/2月26日.md "wikilink")，改為17:50播出。

[2002年](../Page/2002年.md "wikilink")[9月23日起](../Page/9月23日.md "wikilink")，改為18:15播出。

[2003年](../Page/2003年.md "wikilink")[5月5日起](../Page/5月5日.md "wikilink")，的播放時間再度改為17:50，並維持至今。

[2008年](../Page/2008年.md "wikilink")[7月14日起](../Page/7月14日.md "wikilink")，《財經新聞》節目改用[16:9技術製作](../Page/16:9.md "wikilink")。

[2009年](../Page/2009年.md "wikilink")[8月17日起](../Page/8月17日.md "wikilink")，為配合新增的《[七點新聞報道](../Page/七點新聞報道.md "wikilink")》，[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[互動新聞台及](../Page/互動新聞台.md "wikilink")[TVB新聞台於星期一至五](../Page/TVB新聞台.md "wikilink")19:20重播《財經新聞》，但由主播口述有關外幣匯價等即時性消息則被刪剪，並改播由資訊系統自動產生的即時金融圖文資訊。

[2012年](../Page/2012年.md "wikilink")[3月12日起](../Page/3月12日.md "wikilink")，[互動新聞台及](../Page/互動新聞台.md "wikilink")[TVB新聞台取消與](../Page/TVB新聞台.md "wikilink")[高清翡翠台聯播](../Page/高清翡翠台.md "wikilink")《財經新聞》。

若星期一至五當日為世界各主要國家的休市日，《財經新聞》將會暫停播映，而《[七點新聞報道](../Page/七點新聞報道.md "wikilink")》亦會相應延長其播映時間，原時段亦會改為播放《[超級勁歌推介](../Page/超級勁歌推介.md "wikilink")》或最近播放電視劇的主題曲的MV。

[2015年](../Page/2015年.md "wikilink")[9月14日起](../Page/9月14日.md "wikilink")，因《[七點新聞報道](../Page/七點新聞報道.md "wikilink")》取消播映，《財經新聞》亦不再於[高清翡翠台播出](../Page/高清翡翠台.md "wikilink")。

[2017年](../Page/2017年.md "wikilink")[8月15日起](../Page/8月15日.md "wikilink")，因應[無綫財經台的啟播](../Page/無綫財經·資訊台.md "wikilink")，該節目的廠景改為[無綫財經台的全新財經節目的錄影廠](../Page/無綫財經·資訊台.md "wikilink")。

## 主播

現時為《財經新聞》主播的有：[陳啟樂](../Page/陳啟樂.md "wikilink")、[李寶善](../Page/李寶善.md "wikilink")、[翁家揚](../Page/翁家揚.md "wikilink")、[李穎琳](../Page/李穎琳.md "wikilink")

曾擔任《財經新聞》主播的有：[伍嘉文](../Page/伍嘉文.md "wikilink")、[楊卓華](../Page/楊卓華.md "wikilink")、[陶俊民](../Page/陶俊民.md "wikilink")、[李嘉惠](../Page/李嘉惠.md "wikilink")、

已離職《財經新聞》主播的有：[龔偉怡](../Page/龔偉怡.md "wikilink")、[劉慧萍](../Page/劉慧萍.md "wikilink")、[鍾秀怡](../Page/鍾秀怡.md "wikilink")、[周雪君](../Page/周雪君.md "wikilink")、[范巧茹](../Page/范巧茹.md "wikilink")、[林小珍](../Page/林小珍.md "wikilink")、[陳曉蓉](../Page/陳曉蓉.md "wikilink")、[冼潤棠](../Page/冼潤棠.md "wikilink")、[關懿婷](../Page/關懿婷.md "wikilink")、[陳聞賢](../Page/陳聞賢.md "wikilink")、[葉啟忠](../Page/葉啟忠.md "wikilink")、[陳立志](../Page/陳立志.md "wikilink")、[羅志輝](../Page/羅志輝.md "wikilink")、[陳子斌](../Page/陳子斌.md "wikilink")、[邱喜耀](../Page/邱喜耀.md "wikilink")、[謝瑞華](../Page/謝瑞華.md "wikilink")、[伍健強](../Page/伍健強.md "wikilink")、[陳正犖](../Page/陳正犖.md "wikilink")、[潘禮瑤](../Page/潘禮瑤.md "wikilink")、[曾熙雯](../Page/曾熙雯.md "wikilink")

## 參見

## 外部連結

  - [無綫電視網站 - 財經新聞](http://programme.tvb.com/news/financialnews)

[財](../Category/無綫新聞節目.md "wikilink")
[Category:財經節目](../Category/財經節目.md "wikilink")