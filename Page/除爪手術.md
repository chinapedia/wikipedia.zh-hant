**除爪手術**或稱**去爪**，是以[外科手術方式將](../Page/外科手術.md "wikilink")[動物的爪子去除](../Page/動物.md "wikilink")，被施以手術的對象多半是[家貓](../Page/家貓.md "wikilink")，偶爾也有可能是其他動物像是[馬戲團的](../Page/馬戲團.md "wikilink")[獅子或](../Page/獅子.md "wikilink")[熊](../Page/熊.md "wikilink")。這種手術的過程包含截斷該動物全部或部分的趾骨末端，相當於切除[人類](../Page/人類.md "wikilink")[手指的第一個指骨](../Page/手指.md "wikilink")\[1\]。除爪手術在許多[西方國家被認為是](../Page/西方國家.md "wikilink")[虐待動物的行為](../Page/虐待動物.md "wikilink")。\[2\]

## 獸醫觀點

過程極不人道\[3\]，恢復過程長且痛苦，動物癒後的心裡有負面的影響\[4\]。

在一次對320位獸醫的調查中顯示有34.8%的貓有長期負面的健康影響\[5\]\[6\]
。最近的一項長期研究也顯示33%的貓在術後發展出困擾飼主的負面行為問題，18%的貓在術後開始或增加咬人的習慣\[7\]。一些不同單位做的研究也有類似的結論\[8\]\[9\]\[10\]

**醫療用途**

切斷趾骨末端在一些案例中是必要的，像是範圍限定在趾骨末端的[炎症](../Page/炎症.md "wikilink")、[腫瘤](../Page/腫瘤.md "wikilink")、持續且嚴重的[傳染病以及壞疽](../Page/傳染病.md "wikilink")。通常會將手術的範圍限定在受影響的腳爪，避開[健康的腳爪](../Page/健康.md "wikilink")。

**非醫療用途**

除爪手術被某些飼主被作為一種避免[家貓傷害](../Page/家貓.md "wikilink")[人類和其](../Page/人類.md "wikilink")[財產的預防措施](../Page/財產.md "wikilink")。

**方法**

如介紹所陳述，除爪手術本來就包含了截斷[手指的第一節趾骨](../Page/手指.md "wikilink")。
以[醫療為目的的除爪手術通常將](../Page/醫療.md "wikilink")[手術限定在受影響的趾節](../Page/手術.md "wikilink")，而非醫療目的的除爪手術則通常會被施以在將前腳全部的趾爪，然而有一些獸醫也會因為非醫療理由將後腳腳爪施以除爪手術。
目前在相關的[外科技術或剪斷](../Page/外科.md "wikilink")[工具使用](../Page/工具.md "wikilink")，[手術後止痛劑的用法或其他的追蹤照料](../Page/手術.md "wikilink")，最佳[年齡或接受手術的](../Page/年齡.md "wikilink")[貓其他特性等](../Page/貓.md "wikilink")，都沒有[標準作法存在](../Page/標準.md "wikilink")。

## 其他替代方法

家貓可以使用**爪套**\[11\]來取代不人道的非醫療用途除爪手術。

## 法律地位

除爪手術在[北美少數的州有法令禁止](../Page/北美.md "wikilink")，不同的州法令也有所不同。對野生動物的非醫療用途除爪手術在2007年被美國聯邦法令禁止\[12\]。
許多[歐洲國家禁止或相當限制地執行](../Page/歐洲.md "wikilink")，像是[澳洲](../Page/澳洲.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[以色列](../Page/以色列.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[日本以及](../Page/日本.md "wikilink")[土耳其](../Page/土耳其.md "wikilink")。

## 參考文獻

<div class="references-small">

<references />

</div>

## 相關條目

  - [動物權利](../Page/動物權利.md "wikilink")
  - [虐待動物](../Page/虐待動物.md "wikilink")
  - [貓抓板](../Page/貓抓板.md "wikilink")
  - [獸醫](../Page/獸醫.md "wikilink")

## 外部連結

[C](../Category/獸醫學.md "wikilink") [C](../Category/貓.md "wikilink")
[O](../Category/動物福利.md "wikilink")

1.
2.

3.  [手術過程](http://www.pattayadailynews.com/en/2010/05/13/the-painful-declawing-of-cats/)


4.  Tobias KS. Feline onychectomy at a teaching institution\] a
    retrospective study of 163 cases. Vet Surg 1994; 23:274-280.

5.  Landsberg GM. Declawing is controversial but saves pets. A
    veterinarian survey. *Vet Forum* 1991;8:66-67.

6.  Martinez SA, Hauptmann J, Walshaw R. Comparing two techniques for
    onychectomy in cats and two adhesives for wound closure. *Vet Med*
    1993; 88:516-525.

7.  Yeon SC, Flanders JA, Scarlett JM, et al. Attitudes of owners
    regarding tendonectomy and onychectomy in cats. *J Am Vet Med
    Assoc.* 2001;218:43-47.

8.  Bennett M, Houpt KA, Erb HN. Effects of declawing on feline
    behavior. *Comp Anim Pract.* 1988;2:7-12.

9.  Landsberg GM. Cat owners' attitudes toward declawing. *Anthrozoos*.
    1991;4:192-197.

10. Patronek, GJ, Glickman LT, Beck AM, et al. Risk factors for
    relinquishment of cats to an animal shelter. *J Am Vet Med Assoc.*
    1996;209:582–588.

11. [爪套](http://www.softpaws.com/exclusivecolors.html)

12. [美國聯邦法令禁止](http://www.aphis.usda.gov/ac/publications/declaw_tooth.pdf)