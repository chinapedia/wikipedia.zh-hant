《**My Cup of
T**》為[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[傅穎首張個人EP](../Page/傅穎.md "wikilink")，於2007年7月18日在[香港發行](../Page/香港.md "wikilink")，第二版於2007年9月7日發行，首批第二版更附送「限量版
Theresa x Capdase NDSL 水晶保護盒」。

[傅穎憑這張EP於IFPI香港唱片銷量大獎](../Page/傅穎.md "wikilink")2007獲得最暢銷本地女新人獎。

## 曲目

[Category:傅穎專輯](../Category/傅穎專輯.md "wikilink")
[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")