**天南星科**（学名：）是[單子葉](../Page/單子葉植物.md "wikilink")[開花植物中的一](../Page/開花植物.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，有107[屬](../Page/屬_\(生物\).md "wikilink")、3700個以上的[物種](../Page/物種.md "wikilink")，主要分佈在[新世界的熱帶地區](../Page/新世界.md "wikilink")，但也有些分佈於[舊世界的熱帶及溫帶地區](../Page/舊世界.md "wikilink")。[中國有](../Page/中國.md "wikilink")35屬約206種，主要分佈在南方各地。[臺灣產](../Page/臺灣.md "wikilink")16屬40種，有4種記錄為雜草\[1\]。由[APG最近的基因研究顯示](../Page/APG.md "wikilink")，原先獨立歸於[浮萍科的植物](../Page/浮萍科.md "wikilink")，也應歸類於天南星科之中。

[火鶴花與](../Page/火鶴花.md "wikilink")[馬蹄蓮是本科有名的植物](../Page/馬蹄蓮.md "wikilink")，常被栽培供觀賞用。[粗肋草](../Page/粗肋草.md "wikilink")、[觀音蓮](../Page/觀音蓮.md "wikilink")、[彩葉芋](../Page/彩葉芋.md "wikilink")、[黛粉葉](../Page/花葉萬年青.md "wikilink")、[黃金葛](../Page/黃金葛.md "wikilink")、[龜背芋](../Page/龜背芋.md "wikilink")、[蔓綠絨](../Page/蔓綠絨.md "wikilink")、[白鶴芋](../Page/白鶴芋.md "wikilink")、[合果芋](../Page/合果芋.md "wikilink")、[千年芋](../Page/千年芋.md "wikilink")、[美鐵芋等耐陰性強的](../Page/美鐵芋.md "wikilink")[觀葉植物可以做室內植物栽培](../Page/觀葉植物.md "wikilink")。[芋頭](../Page/芋.md "wikilink")、[蒟蒻](../Page/蒟蒻.md "wikilink")（-{魔芋}-）地下[塊莖可以加工做成](../Page/塊莖.md "wikilink")[蒟蒻](../Page/蒟蒻.md "wikilink")，可以食用。[半夏](../Page/半夏.md "wikilink")、[天南星](../Page/天南星.md "wikilink")、[千年健](../Page/千年健.md "wikilink")、[犁頭尖可做藥用](../Page/犁頭尖.md "wikilink")。[水榕](../Page/水榕.md "wikilink")、[隱棒花](../Page/隱棒花.md "wikilink")、[水芭蕉](../Page/水芭蕉.md "wikilink")、[大萍等水生植物可做水族造景](../Page/大萍.md "wikilink")。[巨花魔芋是全世界最高的](../Page/巨花魔芋.md "wikilink")[花](../Page/花.md "wikilink")，其[花序是植物界中最長的花序](../Page/花序.md "wikilink")。[姑婆芋](../Page/姑婆芋_\(植物\).md "wikilink")（[海芋屬](../Page/海芋屬.md "wikilink")）是一種很有名的野生植物，其植株具毒性。

## 形态

[草本](../Page/草本.md "wikilink")，常有辛辣味或乳状液汁，有块状或延长的地下茎，间有木质而呈攀援状，或以气根附生于他物上，少数浮生水中。

叶子多基生，如茎生则互生为2列或作螺旋状排列，全缘或分裂，常呈戟形或箭形，基部有一膜质鞘。

花极小，常有强烈臭味，排列于[肉穗花序上](../Page/肉穗花序.md "wikilink")，外围以佛焰苞，花两性全相似或单性同株，雌花在花序的下部，雄花在花序的上部，介于两者之间的常为中性花，花被在两性花中常具存，裂片4－6枚，鳞片状或合生为杯状，在单性花中缺，[雄蕊一至多数](../Page/雄蕊.md "wikilink")，[子房上位](../Page/子房.md "wikilink")，由一至数心皮合成，每室有[胚珠一至数枚](../Page/胚珠.md "wikilink")。

[浆果密集于肉穗花序上](../Page/浆果.md "wikilink")。

[种子一至数枚埋藏在浆汁果肉中](../Page/种子.md "wikilink")，有各种不同的外种皮。

## 繁衍

種子缺少[胚乳](../Page/胚乳.md "wikilink")，在非相似原生地，可藉由[鱗莖珠芽](../Page/鱗莖.md "wikilink")（bulbil）行「[無性生殖](../Page/無性生殖.md "wikilink")」，以進行繁殖。\[2\]

## 分类

天南星科属于[泽泻目](../Page/泽泻目.md "wikilink")，在演化史上，天南星科是泽泻目已知各科中最早分化出去的一支。

### 内部分类

## 屬

### 天南星亞科

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/長柄萬年青屬.md" title="wikilink">長柄萬年青屬</a> <em>Aglaodorum</em></li>
<li><a href="../Page/粗肋草屬.md" title="wikilink">粗肋草屬</a> <em>Aglaonema</em>（<a href="../Page/廣東萬年青.md" title="wikilink">廣東萬年青</a>）</li>
<li><a href="../Page/海芋屬.md" title="wikilink">-{zh-hk:海芋屬; zh-tw:姑婆芋屬;zh-cn:海芋属;}-</a> <em>Alocasia</em>（<a href="../Page/海芋.md" title="wikilink">海芋</a>、<a href="../Page/姑婆芋.md" title="wikilink">姑婆芋</a>、<a href="../Page/觀音蓮.md" title="wikilink">觀音蓮</a>）</li>
<li><a href="../Page/魔芋屬.md" title="wikilink">魔芋屬</a> <em>Amorphophallus</em>（<a href="../Page/蒟蒻.md" title="wikilink">蒟蒻</a>、<a href="../Page/巨花魔芋.md" title="wikilink">巨花魔芋</a>）</li>
<li><em>Ambrosina</em></li>
<li><em>Anchomanes</em></li>
<li><a href="../Page/水榕屬.md" title="wikilink">水榕屬</a> <em>Anubias</em></li>
<li><a href="../Page/柳葉榕屬.md" title="wikilink">柳葉榕屬</a> <em>Aridarum</em></li>
<li><em>Ariopsis</em></li>
<li><a href="../Page/天南星屬.md" title="wikilink">天南星屬</a> <em>Arisaema</em></li>
<li><a href="../Page/鼠尾南星屬.md" title="wikilink">鼠尾南星屬</a> <em>Arisarum</em></li>
<li><em>Arophyton</em></li>
<li><a href="../Page/疆南星屬.md" title="wikilink">疆南星屬</a> <em>Arum</em></li>
<li><em>Asterostigma</em></li>
<li><a href="../Page/雙芋屬.md" title="wikilink">雙芋屬</a> <em>Biarum</em></li>
<li><em>Bognera</em></li>
<li><a href="../Page/辣椒榕屬.md" title="wikilink">辣椒榕屬</a> <em>Bucephalandra</em></li>
<li><a href="../Page/彩葉芋屬.md" title="wikilink">彩葉芋屬</a> <em>Caladium</em>（<a href="../Page/花葉芋.md" title="wikilink">花葉芋</a>）</li>
<li><a href="../Page/水芋屬.md" title="wikilink">水芋屬</a> <em>Calla</em></li>
<li><em>Callopsis</em></li>
<li><em>Carlephyton</em></li>
<li><em>Cercestis</em></li>
<li><em>Chlorospatha</em></li>
<li><em>Colletogyne</em></li>
<li><a href="../Page/芋屬.md" title="wikilink">-{zh-cn:芋属;zh-tw:紫芋屬;zh-hk:芋屬;}-</a> <em>Colocasia</em>（<a href="../Page/芋.md" title="wikilink">芋</a>）</li>
<li><a href="../Page/隱棒花屬.md" title="wikilink">隱棒花屬</a> <em>Cryptocoryne</em>（<a href="../Page/椒草.md" title="wikilink">椒草</a>）</li>
<li><em>Culcasia</em></li>
<li><a href="../Page/黛粉葉屬.md" title="wikilink">黛粉葉屬</a> <em>Dieffenbachia</em>（<a href="../Page/花葉萬年青.md" title="wikilink">花葉萬年青</a>）</li>
<li><a href="../Page/紫花海芋屬.md" title="wikilink">紫花海芋屬</a> <em>Dracunculus</em></li>
<li><a href="../Page/黑花天南星屬.md" title="wikilink">黑花天南星屬</a> <em>Eminium</em></li>
<li><em>Filarum</em></li>
<li><em>Furtadoa</em></li>
<li><em>Gearum</em></li>
<li><em>Gorgonidium</em></li>
<li><a href="../Page/細柄芋屬.md" title="wikilink">細柄芋屬</a> <em>Hapaline</em></li>
<li><a href="../Page/白星海芋屬.md" title="wikilink">白星海芋屬</a> <em>Helicodiceros</em></li>
</ul></td>
<td><ul>
<li><em>Heteroaridarum</em></li>
<li><a href="../Page/扁葉芋屬.md" title="wikilink">扁葉芋屬</a> <em>Homalomena</em>（<a href="../Page/千年健.md" title="wikilink">千年健</a>、<a href="../Page/春雪芋.md" title="wikilink">春雪芋</a> ）</li>
<li><em>Hottarum</em></li>
<li><em>Jasarum</em></li>
<li><a href="../Page/芭蕉草屬.md" title="wikilink">芭蕉草屬</a> <em>Lagenandra</em></li>
<li><em>Mangonia</em></li>
<li><em>Montrichardia</em></li>
<li><em>Nephthytis</em></li>
<li><a href="../Page/箭葉芋屬.md" title="wikilink">箭葉芋屬</a> <em>Peltandra</em></li>
<li><a href="../Page/蔓綠絨屬.md" title="wikilink">蔓綠絨屬</a> <em>Philodendron</em>（<a href="../Page/喜林芋.md" title="wikilink">喜林芋</a>）</li>
<li><em>Phymatarum</em></li>
<li><a href="../Page/半夏屬.md" title="wikilink">半夏屬</a> <em>Pinellia</em></li>
<li><a href="../Page/芋榕屬.md" title="wikilink">芋榕屬</a> <em>Piptospatha</em></li>
<li><a href="../Page/大薸属.md" title="wikilink">大薸属</a> <em>Pistia</em></li>
<li><em>Protarum</em></li>
<li><a href="../Page/假龍蓮屬.md" title="wikilink">假龍蓮屬</a> <em>Pseudodracontium</em></li>
<li><em>Pseudohydrosme</em></li>
<li><a href="../Page/假蕪萍屬.md" title="wikilink">假蕪萍屬</a> <em>Pseudowolffia</em></li>
<li><a href="../Page/目賊芋屬.md" title="wikilink">目賊芋屬</a> <em>Remusatia</em>（<a href="../Page/岩芋.md" title="wikilink">岩芋</a>）</li>
<li><a href="../Page/斑龍芋屬.md" title="wikilink">斑龍芋屬</a> <em>Sauromatum</em></li>
<li><em>Scaphispatha</em></li>
<li><a href="../Page/落檐屬.md" title="wikilink">落檐屬</a> <em>Schismatoglottis</em>（<a href="../Page/電光芋.md" title="wikilink">電光芋</a>）</li>
<li><em>Spathantheum</em></li>
<li><em>Spathicarpa</em></li>
<li><a href="../Page/泉七屬.md" title="wikilink">泉七屬</a> <em>Steudnera</em></li>
<li><a href="../Page/地寶芋屬.md" title="wikilink">地寶芋屬</a> <em>Stylochaeton</em></li>
<li><a href="../Page/毒蟲芋屬.md" title="wikilink">毒蟲芋屬</a> <em>Synandrospadix</em></li>
<li><a href="../Page/合果芋屬.md" title="wikilink">合果芋屬</a> <em>Syngonium</em></li>
<li><em>Taccarum</em></li>
<li><em>Theriophonum</em></li>
<li><a href="../Page/土半夏屬.md" title="wikilink">土半夏屬</a> <em>Typhonium</em>（<a href="../Page/土半夏.md" title="wikilink">土半夏</a>）</li>
<li><em>Typhonodorum</em></li>
<li><a href="../Page/匍彩芋屬.md" title="wikilink">匍彩芋屬</a><em>Ulearum</em></li>
<li><a href="../Page/千年芋屬.md" title="wikilink">千年芋屬</a> <em>Xanthosoma</em></li>
<li><a href="../Page/馬蹄蓮屬.md" title="wikilink">馬蹄蓮屬</a> <em>Zantedeschia</em>（<a href="../Page/馬蹄蓮.md" title="wikilink">馬蹄蓮</a>、<a href="../Page/彩色海芋.md" title="wikilink">彩色海芋</a>）</li>
<li><em>Zomicarpa</em></li>
<li><em>Zomicarpella</em></li>
</ul></td>
</tr>
</tbody>
</table>

### 麻菖蒲亚科

  - [麻菖蒲属](../Page/麻菖蒲属.md "wikilink")
    *Gymnostachys*（[麻菖蒲](../Page/麻菖蒲.md "wikilink")）

### 棘芋亞科

  - *Anaphyllopsis*
  - *Anaphyllum*
  - [曲籽芋屬](../Page/曲籽芋屬.md "wikilink") *Cyrtosperma*
  - *Dracontioides*
  - [龍蓮屬](../Page/龍蓮屬.md "wikilink") *Dracontium*
  - [刺芋屬](../Page/刺芋屬.md "wikilink") *Lasia*
  - [擬棘芋屬](../Page/擬棘芋屬.md "wikilink") *Lasimorpha*
  - *Podolasia*
  - *Pycnospatha*
  - *Urospatha*

### 浮萍亞科

  - [紫萍屬](../Page/紫萍屬.md "wikilink") *Landoltia*
  - [浮萍屬](../Page/浮萍屬.md "wikilink") *Lemna*
  - [水萍屬](../Page/水萍屬.md "wikilink") *Spirodela*
  - [無根萍屬](../Page/無根萍屬.md "wikilink") *Wolffia*
  - [扁無根萍屬](../Page/扁無根萍屬.md "wikilink") *Wolffiella*

### 龜背芋亞科

  - *Alloschemone*
  - [雷公連屬](../Page/雷公連屬.md "wikilink") *Amydrium*
  - [上樹南星屬](../Page/上樹南星屬.md "wikilink") *Anadendrum*
  - [拎樹藤屬](../Page/拎樹藤屬.md "wikilink")
    *Epipremnum*（[黃金葛](../Page/黃金葛.md "wikilink")）
  - *Heteropsis*
  - *Holochlamys*
  - [龜背竹屬](../Page/龜背竹屬.md "wikilink") *Monstera*
  - [崖角藤屬](../Page/崖角藤屬.md "wikilink")
    *Rhaphidophora*（[針房藤](../Page/針房藤.md "wikilink")）
  - [紅苞芋屬](../Page/紅苞芋屬.md "wikilink") *Rhodospatha*
  - [藤芋屬](../Page/藤芋屬.md "wikilink")
    *Scindapsus*（[星點藤](../Page/星點藤.md "wikilink")）
  - [白鶴芋屬](../Page/白鶴芋屬.md "wikilink") *Spathiphyllum*
  - *Stenospermation*

### 金棒芋亞科

  - [水芭蕉屬](../Page/水芭蕉屬.md "wikilink") *Lysichiton*
  - [金棒芋屬](../Page/金棒芋屬.md "wikilink") *Orontium*
  - [臭菘屬](../Page/臭菘屬.md "wikilink") *Symplocarpus*

### 柚葉藤亞科

  - [花燭屬](../Page/花燭屬.md "wikilink")
    *Anthurium*（[火鶴花](../Page/火鶴花.md "wikilink")）
  - [梗石柑屬](../Page/梗石柑屬.md "wikilink") *Pedicellarum*
  - [假柚葉藤屬](../Page/假柚葉藤屬.md "wikilink")
    *Pothoidium*（[假石柑](../Page/假石柑.md "wikilink")）
  - [柚葉藤屬](../Page/石柑屬.md "wikilink")
    *Pothos*（[石柑](../Page/石柑.md "wikilink")）

### 美鐵芋亞科

  - [曲苞芋屬](../Page/曲苞芋屬.md "wikilink") *Gonatopus*
  - [美鐵芋屬](../Page/美鐵芋屬.md "wikilink") *Zamioculcas*

## 参考文献

## 外部連結

  - [臺灣維管束植物簡誌（天南星科）](https://web.archive.org/web/20050418094523/http://163.29.26.176/species/vascular/5/193.htm)
  - [Flora of China
    (Araceae)](https://web.archive.org/web/20050326090313/http://flora.huh.harvard.edu/china/mss/volume23/Araceae-MO_coauthoring.htm)
  - [天南星科
    Araceae](http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=family_latin=Araceae)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[Category:天南星目](../Category/天南星目.md "wikilink")
[\*](../Category/天南星科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.
2.