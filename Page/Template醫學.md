{{\#invoke:Sidebar|collapsible | name = 醫學 | image =
![Medistub.svg](Medistub.svg "Medistub.svg") | above =
<big>**[醫學](醫學.md "wikilink")**</big> | imagestyle =
background:\#ECECCF;padding:0; | abovestyle = background:\#ECECCF; |
content1style = padding:0;border:none; | content1 =

<table class="collapsible collapsed" style="width:100%; border-collapse: collapse;text-align: center;">

<tr>

<th style="background-color:#c8c8c8;padding:0.2em;padding-left:2.75em;padding-bottom:0;">

[基礎醫學](基礎醫學.md "wikilink")

</th>

</tr>

<tr>

<td class="plainlist">

[人體解剖學](人體解剖學.md "wikilink") - [人體生理學](人體生理學.md "wikilink")
[組織學](組織學.md "wikilink") - [胚胎學](胚胎學.md "wikilink")
[人體寄生蟲學](人體寄生蟲學.md "wikilink") - [免疫學](免疫學.md "wikilink")
[病理學](病理學.md "wikilink") - [病理生理學](病理生理學.md "wikilink")
[細胞學](細胞學.md "wikilink") - [營養學](營養學.md "wikilink")
[流行病學](流行病學.md "wikilink") -
[藥理學](藥理學.md "wikilink") - [毒理學](毒理學.md "wikilink")

</td>

</tr>

<tr>

<td class="plainlist">

其他基礎學科參見[生物學模板](:template:生物學頁腳.md "wikilink")

</td>

</tr>

</table>

|listtitlestyle=height:1.95em; | list2title = [內科學](內科學.md "wikilink") |
list2 =

<div class="plainlist">

  - [耳鼻喉科學](耳鼻喉科學.md "wikilink") - [眼科學](眼科學.md "wikilink")
  - [心臟病學](心臟病學.md "wikilink") - [胸腔醫學](胸腔醫學.md "wikilink")
  - [消化病學](消化病學.md "wikilink") [胃腸病學](胃腸病學.md "wikilink")
  - [腎臟學](腎臟科.md "wikilink") - [肝臟學](肝臟學.md "wikilink")
  - [血液病學](血液病學.md "wikilink") - [神經病學](神經病學.md "wikilink")
  - [傳染病學](傳染病學.md "wikilink") - [腫瘤學](腫瘤學.md "wikilink")
  - [內分泌學](內分泌學.md "wikilink") - [風濕病學](風濕病學.md "wikilink")
  - [兒童醫學](兒童醫學.md "wikilink") - [老人醫學](老人醫學.md "wikilink")
  - [皮膚病學](皮膚病學.md "wikilink") - [神經眼科學](神經眼科學.md "wikilink")

</div>

| list3title = [外科學](外科學.md "wikilink") | list3 =

<div class="plainlist">

  - [普通外科學](普通外科學.md "wikilink") - [神經外科學](神經外科.md "wikilink")
  - [胸腔外科學](胸腔外科學.md "wikilink") - [心臟外科學](心臟外科.md "wikilink")
  - [肝膽外科學](肝膽外科學.md "wikilink") - [胃腸外科學](胃腸外科學.md "wikilink")
  - [泌尿外科學](泌尿外科.md "wikilink") - [血管外科學](血管外科學.md "wikilink")
  - [器官移植學](器官移植.md "wikilink") - [腫瘤外科學](腫瘤外科學.md "wikilink")
  - [骨科學](骨科學.md "wikilink") - [整型外科學](整型外科學.md "wikilink")
  - [小兒外科學](小兒外科學.md "wikilink")

</div>

| list4title = 其他[醫學專科](醫學專科.md "wikilink") | list4 =

<div class="plainlist">

  - [婦產科學](婦產科學.md "wikilink")（[婦科學](婦科學.md "wikilink") -
    [產科學](產科學.md "wikilink")）
  - [牙醫學](牙醫學.md "wikilink") - [口腔醫學](口腔醫學.md "wikilink") -
    [解剖學](解剖學.md "wikilink")
  - [預防醫學](預防醫學.md "wikilink") - [臨床醫學](臨床醫學.md "wikilink")
  - [急診學](急診學.md "wikilink") - [麻醉學](麻醉學.md "wikilink")
  - [安寧緩和醫學](安寧緩和醫學.md "wikilink") - [精神醫學](精神醫學.md "wikilink")
  - [環境及職業病學](環境及職業病學.md "wikilink") - [復健醫學](復健醫學.md "wikilink")
  - [綜合醫學](綜合醫學.md "wikilink") - [家庭醫學](家庭醫學.md "wikilink")
  - [旅遊醫學](旅遊醫學.md "wikilink")

</div>

| list5title = [替代醫學](替代醫學.md "wikilink") | list5 =

<div class="plainlist">

  - [信仰療法](信仰療法.md "wikilink") - [順勢療法](順勢療法.md "wikilink")
  - [傳統醫學](傳統醫學.md "wikilink") - [草藥學](草藥學.md "wikilink")
  - [自然醫學](自然醫學.md "wikilink") - [脊骨神經醫學](脊骨神經醫學.md "wikilink")
  - [整骨療法](整骨療法.md "wikilink")（骨病醫學）

</div>

| list6title = 診斷醫學 | list6 =

<div class="plainlist">

  - [檢驗醫學](檢驗醫學.md "wikilink") - [影像醫學](醫學影像.md "wikilink")
  - [病理醫學](病理醫學.md "wikilink") - [神經生理學](神經生理學.md "wikilink")
  - [核醫學](核醫學.md "wikilink")

</div>

| list7title = 相關學科 | list7 =

<div class="plainlist">

  - [心理學](心理學.md "wikilink") - [法醫學](法醫學.md "wikilink")
  - [生物數學](生物數學.md "wikilink") - [醫學信息學](醫學信息學.md "wikilink")
  - [社會醫學](社會醫學.md "wikilink") - [生物醫學工程](生物醫學工程.md "wikilink")

</div>

| list8title = 醫事人員 | list8 =

<div class="plainlist">

  - [醫師](醫師.md "wikilink") - [中醫師](中醫師.md "wikilink") -
    [藥師](藥劑師.md "wikilink")
  - [牙醫師](牙醫師.md "wikilink") - [牙體技術師](牙體技術師.md "wikilink")(生)
  - [護理師](護理師.md "wikilink") - [助產師](助產師.md "wikilink") -
    [呼吸治療師](呼吸治療師.md "wikilink")
  - [醫事檢驗師](醫事檢驗師.md "wikilink") - [醫事放射師](醫事放射師.md "wikilink") -
    [驗光師](驗光師.md "wikilink")(生)
  - [物理治療師](物理治療師.md "wikilink") - [職能治療師](職能治療師.md "wikilink")
  - [語言治療師](言語治療.md "wikilink") - [聽力師](聽力學.md "wikilink")
  - [心理師](心理師.md "wikilink")（諮商/臨床） - [營養師](營養師.md "wikilink")

</div>

| list9title = 醫療行為 | list9 =

<div class="plainlist">

  - [藥物治療](藥物治療.md "wikilink") - [外科手術](外科手術.md "wikilink")
  - [化學療法](化學療法.md "wikilink") - [心理治療](心理治療.md "wikilink")
  - [物理治療](物理治療.md "wikilink") - [職能治療](職能治療.md "wikilink")
  - [語言治療](言語治療.md "wikilink") - [聽力治療](聽力學.md "wikilink")

</div>

| list10title = [健康議題](健康.md "wikilink") | list10 =

<div class="plainlist">

  - [症狀](症狀.md "wikilink") - [症候群](症候群.md "wikilink")
  - [亞健康](亞健康.md "wikilink") - [老化](老化.md "wikilink")
  - [病人安全](病人安全.md "wikilink")

</div>

| list11title = 主題與專題 | list11 =

<div class="plainlist">

  - [醫學主題](Portal:醫學.md "wikilink") - [心理學主題](Portal:心理學.md "wikilink")
  - [新陳代謝主題](Portal:新陳代謝.md "wikilink")

-----

  - [醫學專題](Wikipedia:醫學專題.md "wikilink") -
    [藥學專題](Wikipedia:藥學專題.md "wikilink")
  - [藥物專題](Wikipedia:藥物專題.md "wikilink")

</div>

| list12title = 其他 | list12 =

<div class="plainlist">

  - [生物與醫學詞彙譯名表](Wikipedia:生物與醫學詞彙譯名表.md "wikilink")
  - [Wikipedia:醫學專題/資源](Wikipedia:醫學專題/資源.md "wikilink")
  - [Wikipedia:醫學專題/工具](Wikipedia:醫學專題/工具.md "wikilink")
  - [醫學史](醫學史.md "wikilink") [醫學倫理學](醫學倫理學.md "wikilink")
  - [醫學警告模板](Template:Medical.md "wikilink")

</div>

}}<noinclude>

</noinclude>

[\*](../Category/醫學.md "wikilink")
[醫學導航模板](../Category/醫學導航模板.md "wikilink")