**盩厔教区（拉丁文：Dioecesis
Ceucevensis）**是[天主教在中国](../Page/天主教.md "wikilink")[陕西省中部设立的一个](../Page/陕西省.md "wikilink")[教区](../Page/教区.md "wikilink")，位于陕西省渭河平原中部。辖周至县、户县、杨凌区(属西安市)、眉县、扶风县(属宝鸡市)、武功县、兴平市(属咸阳市)五县一市一区。现有教友六万多人，主教一位，神父五十七位，圣堂152座，圣地2处：户县圣母山和眉县跑窝十字山。

## 历史

1932年6月17日，罗马教廷将[西安府代牧区一分为五](../Page/西安教区.md "wikilink")，成立**盩厔监牧区**，由国籍主教负责。

1951年5月10日，盩厔监牧区升格为**盩厔教区**。

1950年，盩厔监牧区有天主教徒23,397人，占当地总人口（92万人）的2.5%，人数居陕西省各教区之首。共有神甫24人，其中教区神甫22人，修会神甫2人，堂口91处。\[1\]

## 教堂

  - [圣母无玷圣心主教座堂](../Page/周至主教座堂.md "wikilink")

## 历任主教

  - 盩厔宗座监牧
      - 司铎[張指南](../Page/張指南.md "wikilink") Fr. John Zhang Zhi-nan
        (Tchang)（1932年6月14日－1940年）
      - 司铎[高正一](../Page/高正一.md "wikilink") Fr. John Gao Zheng-yi
        (Kao)（1941年5月30日－1951年）

<!-- end list -->

  - 盩厔主教
      - 主教[李伯漁](../Page/李伯漁.md "wikilink") Bishop Louis Li
        Bo-yu（1951年5月10日－1980年2月8日）
      - 主教[范玉飞](../Page/范玉飞.md "wikilink") Bishop Paulus Fan
        Yu-fei（1982年－1995年4月5日）未得到官方承认
      - 主教[杨广彦](../Page/杨广彦.md "wikilink") Bishop Alphonsus Yang
        Guang-yan（1995年12月17日－2004年9月4日）
      - 主教[吴钦敬](../Page/吴钦敬.md "wikilink") Bishop Joseph Wu
        Qin-jing（2005年10月19日－）2015年7月10日得到官方承认，遭软禁

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [和谐网](http://www.cnzcatholic.org/index.asp)
  - [周至教区福传网](http://zzcat.lingd.net/)

{{-}}

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:陕西天主教](../Category/陕西天主教.md "wikilink")

1.  [盩厔教区](http://www.catholic-hierarchy.org/diocese/dchwc.html)