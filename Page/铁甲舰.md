[Monitorvirginia.jpg](https://zh.wikipedia.org/wiki/File:Monitorvirginia.jpg "fig:Monitorvirginia.jpg")[維吉尼亞/梅里馬克號](../Page/維吉尼亞號.md "wikilink")（CSS
*Virginia/Merrimac*）（左）對抗[美國](../Page/美國海軍.md "wikilink")[莫尼特號](../Page/莫尼特號.md "wikilink")（USS
*Monitor*），於1862年[漢普頓錨地海戰](../Page/漢普頓錨地海戰.md "wikilink")。\]\]
[USS_Olympia_art_NH_91881-KN.jpg](https://zh.wikipedia.org/wiki/File:USS_Olympia_art_NH_91881-KN.jpg "fig:USS_Olympia_art_NH_91881-KN.jpg")之馬尼拉海戰鐵甲艦\]\]

**鐵甲艦**又譯**裝甲艦**(Ironclad
warship)，是19世紀下半葉早期的一種[蒸汽式](../Page/蒸汽機.md "wikilink")[軍艦](../Page/軍艦.md "wikilink")，外覆有堅硬的[鐵或](../Page/鐵.md "wikilink")[鋼製](../Page/鋼.md "wikilink")[裝甲](../Page/裝甲.md "wikilink")\[1\]。由於木造軍艦無力抵禦[炮彈的轟炸](../Page/炮彈.md "wikilink")，鐵甲艦便應運而生。1859年11月，由[法國海軍領銜的全球第一艘主力鐵甲戰艦](../Page/法國海軍.md "wikilink")[光榮號](../Page/法國戰艦光榮號.md "wikilink")（*La
Gloire*）首度啟航\[2\]。[英國皇家海軍自](../Page/英國皇家海軍.md "wikilink")1856年起籌劃發展裝甲戰艦，1857年順利擬定裝甲[護衛艦草稿圖](../Page/護衛艦.md "wikilink")，但1859年初皇家海軍選擇打造二艘鐵甲[巡防艦](../Page/巡防艦.md "wikilink")，並於1861年決議朝全裝甲艦隊發展。在[美國內戰期間](../Page/美國內戰.md "wikilink")[鐵甲艦首度交鋒後](../Page/漢普頓錨地海戰.md "wikilink")，帆船時代無裝甲（木質船殼）的[戰列艦已明顯居於弱勢](../Page/戰列艦.md "wikilink")，鐵甲艦便取代之，躍身成為水戰最強大的船艦。此類型的船艦後來甚至主宰了美國內戰\[3\]。

鐵甲艦可用作多種用途，包括公海的[戰鬥艦](../Page/戰鬥艦.md "wikilink")、海防艦以及遠程[巡洋艦等](../Page/巡洋艦.md "wikilink")。十九世紀晚期軍艦設計的急遽發展，使鐵甲艦從木造船身、仍須靠[揚帆以填補蒸汽機運作不足的輪船](../Page/帆.md "wikilink")，搖身一變成為鋼製、設有砲台的戰鬥艦與巡洋艦，已接近二十世紀近代艦艇的水準。此轉變的持續演進，應歸功於重型艦炮的研製（1880年代的鐵甲艦已架有重型砲械可於航海時作戰）、越為先進精密的蒸汽機，以及冶金技術的日新月異，使得鋼鐵造船變為可行。

鐵甲艦時代的快速發展，意味著許多船隻完工之際便面臨過時與淘汰，以及海戰兵法正是突飛猛進的時候。許多新造的鐵甲艦都裝有[撞擊裝置](../Page/軍艦撞擊裝置.md "wikilink")（衝角）或[魚雷](../Page/魚雷.md "wikilink")，眾多軍艦設計師視之為海戰時的關鍵武器。鐵甲艦時代並沒有明確的結束界線，但約莫1890年代末時「鐵甲艦」一詞已經極少為人所用。新型船艦有着更强大的能力多称为戰鬥艦或[裝甲巡洋艦](../Page/裝甲巡洋艦.md "wikilink")。

## 蒸氣動力

19世纪中叶後，随着[科学技术和](../Page/科学技术.md "wikilink")[造船](../Page/造船.md "wikilink")[工业的发展](../Page/工业.md "wikilink")，风帆战列舰逐渐让位给[蒸汽战舰](../Page/蒸汽.md "wikilink")。1850年，[法国建造出](../Page/法国.md "wikilink")[世界第一艘以](../Page/世界.md "wikilink")[蒸汽机为辅助动力装置的战列舰](../Page/蒸汽机.md "wikilink")——“[拿破仑号](../Page/拿破仑号.md "wikilink")”([Le
Napoléon](../Page/:en:French_ship_Napoléon_\(1850\).md "wikilink"))，它成为[海军蒸汽动力战列舰的先驱](../Page/海军.md "wikilink")。1853年至1856年的[克里米亚战争](../Page/克里米亚战争.md "wikilink")，奠定了蒸汽[装甲战舰在近代](../Page/装甲.md "wikilink")[海军舰队中举足轻重的统治地位](../Page/海军.md "wikilink")。

1859年，法国建造了5630吨的“光荣”号战列舰（Gloire）。1860年，[英国建造了](../Page/英国.md "wikilink")9137吨的“勇士”号战列舰（HMS
Warrior）。这两艘军舰外-{面}-包覆[铁质](../Page/铁.md "wikilink")[装甲](../Page/装甲.md "wikilink")，被视作世界上最初的两艘蒸汽[装甲舰](../Page/装甲.md "wikilink")。[美国](../Page/美国.md "wikilink")[南北战争期间](../Page/南北战争.md "wikilink")，[美国北方海军小型](../Page/美国.md "wikilink")[装甲炮舰莫尼特号](../Page/装甲.md "wikilink")（USS
Monitor）首次采用了封闭的回旋式炮塔。它与[南方邦联海军](../Page/美国南方邦联.md "wikilink")[弗吉尼亚号](../Page/弗吉尼亚.md "wikilink")[装甲舰](../Page/装甲.md "wikilink")（CSS
Virginia）之间发生了首次近代意义上的海上炮战——1862年的[汉普敦海战](../Page/汉普敦海战.md "wikilink")。

1873年，英国建成“蹂躏”号战列舰([HMS
Devastation](../Page/:en:HMS_Devastation_\(1871\).md "wikilink"))，该舰已废除使用风帆的传统，成为[世界](../Page/世界.md "wikilink")[海军史上第一艘纯](../Page/海军.md "wikilink")[蒸汽动力战列舰](../Page/蒸汽.md "wikilink")。到19世纪70年代，世界各海军强国的蒸汽装甲战列舰已达到较高的水平。蒸汽机不仅为军舰提供了推进动力，而且蒸汽还被用于操纵舵系统、锚泊系统、转动装甲系统、装填弹药、抽水及升降舰载小艇等。大型蒸汽装甲战列舰的排水量达到8000至9000吨，推进功率达到6000至8000匹马力。

<center>

[File:LaGloireModel.jpg|光榮號鐵甲艦(1859年)](File:LaGloireModel.jpg%7C光榮號鐵甲艦\(1859年\))
[File:HMSWarrior.JPG|勇士號鐵甲艦(1860年)](File:HMSWarrior.JPG%7C勇士號鐵甲艦\(1860年\))
<File:Uss> Cairo h61568.jpg|開羅號鐵甲艦(1861年) <File:Redoutable>
1876.png|可畏號鐵甲艦(1876年)

</center>

## 經典戰役

  - [南北戰爭](../Page/南北戰爭.md "wikilink")
  - [中法戰爭](../Page/中法戰爭.md "wikilink")
  - [甲午戰爭](../Page/甲午戰爭.md "wikilink")

## 現今

現今許多鐵甲艦都受到整修、維護，作為博物館館藏船。

  - [勇士號](../Page/勇士号铁甲舰.md "wikilink")（HMS
    *Warrior*）原為[英國皇家海軍軍艦](../Page/英國皇家海軍.md "wikilink")，現經徹底維修，收藏於英國[樸茨茅斯的博物館](../Page/樸茨茅斯.md "wikilink")。
  - [開羅號](../Page/開羅號.md "wikilink")（USS
    *Cairo*）屬[美國海軍](../Page/美國海軍.md "wikilink")，現展出於[密西西比州](../Page/密西西比州.md "wikilink")[維克斯堡](../Page/維克斯堡.md "wikilink")。
  - [諾斯洛普·格魯門公司在](../Page/諾斯洛普·格魯門公司.md "wikilink")[紐波特紐斯依原尺寸仿製了](../Page/紐波特紐斯.md "wikilink")[莫尼特號](../Page/莫尼特號.md "wikilink")（USS
    *Monitor*）。複製品於2005年2月動工，二個月後完成。
  - [巴佛艦](../Page/巴佛艦.md "wikilink")（HNLMS
    *Buffel*）現展於[鹿特丹](../Page/鹿特丹.md "wikilink")[海事博物館](../Page/海事博物館.md "wikilink")。
  - [蠍子號](../Page/蠍子號.md "wikilink")（HNLMS
    *Schorpioen*）現為[登海爾德的館藏船](../Page/登海爾德.md "wikilink")。

## 参考文献

### 引用

### 書目

  - Eugène M. Koleśnik, Roger Chesneau, N. J. M. Campbell. *Conway's All
    the World's Fighting Ships 1860–1905*. 英國康威海事出版社 (Conway Maritime
    Press), 1979. ISBN 978-0-8317-0302-8.

  - Archibald, EHH (1984). *The Fighting Ship in the Royal Navy
    1897–1984*. Blandford. ISBN 978-0-7137-1348-0.

  - Ballard, George, *The Black Battlefleet*. 美國航海學會出版社 (Naval Institute
    Press), 1980. ISBN 978-0-87021-924-5.

  - Baxter, James Phinney III (1933), *The Introduction of the Ironclad
    Warship*, 哈佛大學出版社, 1933.

  - Beeler, John, *Birth of the Battleship: British Capital Ship Design
    1870–1881*. Caxton, 倫敦, 2003. ISBN 978-1-84067-534-4

  - [Brown, DK](../Page/David_K_Brown.md "wikilink") (2003). *Warrior to
    Dreadnought: Warship Development 1860–1905*. Caxton Editions. ISBN
    978-1-84067-529-0.

  -
  - Canney, Donald L *The Old Steam Navy, The Ironclads, 1842–1885*.
    Naval Institute Press, 1993

  -
  - Hill, Richard. War at Sea in the Ironclad Age, ISBN
    978-0-304-35273-9.

  - Jenschura Jung & Mickel, *Warships of the Imperial Japanese Navy
    1869–1946*, ISBN 978-0-85368-151-9

  - Kennedy, Paul M. *The Rise and Fall of British Naval Mastery*.
    Macmillan, London, 1983. ISBN 978-0-333-35094-2.

  - [安德魯·蘭伯特](../Page/安德魯·蘭伯特.md "wikilink")(Andrew Lambert)
    *Battleships in Transition: The Creation of the Steam Battlefleet
    1815–1860*. Conway Maritime Press, London, 1984. ISBN
    978-0-85177-315-5.

  - Lyon, David and Winfield, Rif: *The Sail and Steam Navy List,
    1815–1889*, Chatham Publishing, 2004. ISBN 978-1-86176-032-6.

  - Noel, Gerard et al., *The Gun, Ram and Torpedo, Manoeuvres and
    tactics of a Naval Battle of the Present Day*, 2nd Edition, pub.
    Griffin 1885.

  - Northrop Grumman Newport News, [Northrop Grumman Employees
    Reconstruct History with USS Monitor
    Replica](https://web.archive.org/web/20070219052656/http://www.nn.northropgrumman.com/news/2005/050226_news.html).
    2007-05-21查閱.

  - [愛德華·詹姆斯·里德](../Page/愛德華·詹姆斯·里德.md "wikilink") (Edward James Reed)
    *Our Ironclad Ships, their Qualities, Performance and Cost*. John
    Murray, 1869.

  - Sondhaus, Lawrence. *Naval Warfare 1815–1914*. 洛特列治 (Routledge), 倫敦,
    2001. ISBN 978-0-415-21478-0.

  - Sandler, Stanley. *Emergence of the Modern Capital Ship*,
    [特拉華州](../Page/特拉華州.md "wikilink")[紐瓦克](../Page/紐瓦克.md "wikilink"),
    [聯合大學出版](../Page/聯合大學出版.md "wikilink"), 1979.

## 外部連結

  - [铁壁铧嘴海鹘](https://web.archive.org/web/20060616112403/http://dev2.cns.org.hk/0813/html/0813c13/0813c13.html)

## 參見

  - [大艦巨砲主義](../Page/大艦巨砲主義.md "wikilink")
  - [龜甲船](../Page/龜甲船.md "wikilink")

{{-}}

[Category:军舰类型](../Category/军舰类型.md "wikilink")
[铁甲舰](../Category/铁甲舰.md "wikilink")
[Category:海军装甲](../Category/海军装甲.md "wikilink")

1.  Hill, Richard. 《鐵甲艦年代的海上戰爭》(*War at Sea in the Ironclad Age*) ISBN
    978-0-304-35273-9; p. 17.
2.  Sondhaus, Lawrence. 《海戰1815–1914》(*Naval Warfare 1815–1914*) ISBN
    978-0-415-21478-0, pp. 73–4.
3.  Sondhaus, p. 86.