[Chakras.jpg](https://zh.wikipedia.org/wiki/File:Chakras.jpg "fig:Chakras.jpg")
**查克拉**（ ，音*chakra 察喀拉*， **khor
lo**），字根源自「圓」、「輪子」，意譯為-{**脈輪**}-或**氣卦**，粵語譯作**-{卓羅}-**，在[印度](../Page/印度.md "wikilink")[瑜伽的觀念中是指分布於](../Page/瑜伽.md "wikilink")[人體各部位的](../Page/人體.md "wikilink")[能量](../Page/能量.md "wikilink")[中樞](../Page/中樞.md "wikilink")，尤其是指從尾骨到頭頂排列於身體中軸者。

脈輪主要是由[中脈](../Page/中脈.md "wikilink")、[左脈及](../Page/左脈.md "wikilink")[右脈纏繞所形成](../Page/右脈.md "wikilink")。

## 印度哲學

[印度哲學認為查克拉存在於身體中](../Page/印度哲學.md "wikilink")，同時掌管身心運作。身體方面與各[器官功能有關](../Page/器官.md "wikilink")；在心理方面則對情感及精神方面都有影響。其與[色彩有密切的關係](../Page/色彩.md "wikilink")，由下而上分別對應[彩虹的七種色彩及白色](../Page/彩虹.md "wikilink")，並衍生出[色彩療法](../Page/色彩療法.md "wikilink")。意即在生活中活用色彩能量（光）對身體及心靈進行自然療法，給予身心安適感而回歸平衡的健康狀態。

## 查克拉比較表

下表列出最常被提及的七個查克拉：

| 符號                                                                                             | 中文名稱                                                               | 梵語名稱           | 位置                               | 色彩                               | 音頻    | 身體器官                                                                                                                                                                                        | 心靈意義                                                                                                                                                            |
| ---------------------------------------------------------------------------------------------- | ------------------------------------------------------------------ | -------------- | -------------------------------- | -------------------------------- | ----- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [<File:Chakra07.png>](https://zh.wikipedia.org/wiki/File:Chakra07.png "fig:File:Chakra07.png") | [頂輪](../Page/頂輪.md "wikilink")                                     | सहस्रार ()     | [頭頂](../Page/頭.md "wikilink")    | [雪青色](../Page/雪青色.md "wikilink") | 963Hz | [大腦](../Page/大腦.md "wikilink")、[肌肉](../Page/肌肉.md "wikilink")、[皮膚](../Page/皮膚.md "wikilink")、 [腦下垂體](../Page/腦下垂體.md "wikilink") / (松果體)                                                    | [精神性](../Page/精神.md "wikilink")、[統合](../Page/統合.md "wikilink")、[靈性](../Page/靈性.md "wikilink")                                                                   |
| [<File:Chakra06.png>](https://zh.wikipedia.org/wiki/File:Chakra06.png "fig:File:Chakra06.png") | [眉心輪](../Page/眉心輪.md "wikilink")（[第三眼](../Page/第三眼.md "wikilink")） | आज्ञा ()       | [眉心](../Page/眉.md "wikilink")    | [藍色](../Page/藍色.md "wikilink")   | 852Hz | [小腦](../Page/小腦.md "wikilink")、[眼](../Page/眼.md "wikilink")、[鼻](../Page/鼻.md "wikilink")、[耳](../Page/耳.md "wikilink")、 [松果體](../Page/松果體.md "wikilink") / (腦下垂體)                            | [洞察力](../Page/洞察力.md "wikilink")、[創意](../Page/創意.md "wikilink")、[視覺](../Page/視覺.md "wikilink")、[直覺](../Page/直覺.md "wikilink")、[實相化能力](../Page/念力.md "wikilink") |
| [<File:Chakra05.png>](https://zh.wikipedia.org/wiki/File:Chakra05.png "fig:File:Chakra05.png") | [喉輪](../Page/喉輪.md "wikilink")                                     | विशुद्ध ()     | [喉部](../Page/喉部.md "wikilink")   | [青色](../Page/青色.md "wikilink")   | 714Hz | [喉嚨](../Page/喉嚨.md "wikilink")、[氣管](../Page/氣管.md "wikilink")、[食道](../Page/食道.md "wikilink")、[口腔](../Page/口腔.md "wikilink")、[牙齒](../Page/牙齒.md "wikilink")、[甲狀腺](../Page/甲狀腺.md "wikilink") | [溝通](../Page/溝通.md "wikilink")、[靈感](../Page/靈感.md "wikilink")、[人際關係](../Page/人際關係.md "wikilink")、表達真實自我                                                         |
| [<File:Chakra04.png>](https://zh.wikipedia.org/wiki/File:Chakra04.png "fig:File:Chakra04.png") | [心輪](../Page/心輪.md "wikilink")                                     | अनाहत ()       | [胸部](../Page/胸部.md "wikilink")   | [綠色](../Page/綠色.md "wikilink")   | 639Hz | [心臟](../Page/心臟.md "wikilink")、[橫膈膜](../Page/橫膈膜.md "wikilink")、[肺](../Page/肺.md "wikilink")、[循環系統](../Page/循環系統.md "wikilink")、[胸腺](../Page/胸腺.md "wikilink")                              | [愛](../Page/愛.md "wikilink")、[包容](../Page/包容.md "wikilink")、[原諒](../Page/原諒.md "wikilink")、[情感](../Page/情感.md "wikilink")、[治癒](../Page/治癒.md "wikilink")、心靈富足感  |
| [<File:Chakra03.png>](https://zh.wikipedia.org/wiki/File:Chakra03.png "fig:File:Chakra03.png") | [太陽神經叢](../Page/太陽神經叢.md "wikilink")                               | मणिपूर ()      | [肚臍上方](../Page/肚臍.md "wikilink") | [黃色](../Page/黃色.md "wikilink")   | 528Hz | [胃](../Page/胃.md "wikilink")、[脾](../Page/脾.md "wikilink")、[肝](../Page/肝.md "wikilink")、[胰](../Page/胰.md "wikilink")、[神經系統](../Page/神經系統.md "wikilink")                                      | [自信](../Page/自信.md "wikilink")、[社會性](../Page/社會性.md "wikilink")、[理性](../Page/理性.md "wikilink")、[知識](../Page/知識.md "wikilink")、行動力、物質富足感                         |
| [<File:Chakra02.png>](https://zh.wikipedia.org/wiki/File:Chakra02.png "fig:File:Chakra02.png") | [生殖輪](../Page/生殖輪.md "wikilink")                                   | स्वाधिष्ठान () | [生殖器](../Page/生殖器.md "wikilink") | [橙色](../Page/橙色.md "wikilink")   | 417Hz | [腸](../Page/腸.md "wikilink")、[腎](../Page/腎.md "wikilink")、[腰部](../Page/腰部.md "wikilink")、[生殖器](../Page/生殖器.md "wikilink")、[內分泌系統](../Page/內分泌系統.md "wikilink")                              | [感官](../Page/感官.md "wikilink")、[性愛](../Page/性愛.md "wikilink")、[快樂](../Page/快樂.md "wikilink")、[生命力](../Page/生命力.md "wikilink")、[創造力](../Page/創造力.md "wikilink")  |
| [<File:Chakra01.png>](https://zh.wikipedia.org/wiki/File:Chakra01.png "fig:File:Chakra01.png") | [海底輪](../Page/海底輪.md "wikilink")                                   | मूलाधार ()     | [會陰](../Page/會陰.md "wikilink")   | [紅色](../Page/紅色.md "wikilink")   | 396Hz | [骨](../Page/骨.md "wikilink")、[足](../Page/足.md "wikilink")、[脊髓](../Page/脊髓.md "wikilink")、[直腸](../Page/直腸.md "wikilink")、[免疫系統](../Page/免疫系統.md "wikilink")                                  | [生存能量](../Page/生存.md "wikilink")、[安全感](../Page/安全感.md "wikilink")、全體的基本                                                                                         |

## 參見

  - [气场](../Page/气场.md "wikilink")
  - [活力論](../Page/活力論.md "wikilink")
  - [氣 (印度醫學)](../Page/氣_\(印度醫學\).md "wikilink")
  - [氣 (西藏醫學)](../Page/氣_\(西藏醫學\).md "wikilink")
  - [脈 (印度醫學)](../Page/脈_\(印度醫學\).md "wikilink")

## 外部連結

[Category:脈輪](../Category/脈輪.md "wikilink")
[Category:顏色](../Category/顏色.md "wikilink")
[Category:印度教哲學概念](../Category/印度教哲學概念.md "wikilink")
[Category:印度文化](../Category/印度文化.md "wikilink")
[Category:冥想](../Category/冥想.md "wikilink")
[Category:新紀元運動](../Category/新紀元運動.md "wikilink")
[Category:梵语词汇](../Category/梵语词汇.md "wikilink")
[Category:活力论](../Category/活力论.md "wikilink")