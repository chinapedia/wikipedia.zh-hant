**齊宣王**（，約50歲)，本名**田辟疆**，[戰國時代](../Page/戰國時代.md "wikilink")[齊國國君](../Page/齊國.md "wikilink")，[齊威王之子](../Page/齊威王.md "wikilink")。

前314年，[燕國](../Page/燕國.md "wikilink")[燕王噲](../Page/燕王噲.md "wikilink")[禪讓](../Page/禪讓.md "wikilink")[王位給](../Page/王位.md "wikilink")[宰相](../Page/宰相.md "wikilink")[子之](../Page/子之.md "wikilink")，[太子平被迫起兵奪回權力](../Page/太子平.md "wikilink")，反而被殺。齊宣王乘此大亂，派[匡章率軍](../Page/匡章.md "wikilink")[攻破燕國](../Page/齊破燕之戰.md "wikilink")，燕王噲被殺，宰相子之逃亡，后被齊人抓住做成肉醬。此時齊宣王向[孟子請教稱霸天下的方法](../Page/孟子.md "wikilink")，孟子因勢利導，游說齊宣王棄霸道而行王道，但是齐宣王没有听从，反而军纪败坏，掠夺民财，導致燕人叛亂，不久齊軍就在[赵](../Page/赵.md "wikilink")、[魏](../Page/魏.md "wikilink")、[韩](../Page/韩.md "wikilink")、[楚](../Page/楚.md "wikilink")、[秦等国的压力下被迫撤军](../Page/秦.md "wikilink")，而燕人則共立公子職，是为[燕昭王](../Page/燕昭王.md "wikilink")，齊宣王感嘆：「吾甚慚於孟子。」

前312年，齐宣王杀王后，后娶[钟离春为](../Page/钟离春.md "wikilink")-{zh-hans:后; zh-hant:后;
zh-cn:后; zh-tw:后; zh-hk:后; zh-sg:后}-。相傳钟离春是一位樣貌奇醜的女子。

齊宣王愛聽吹[竽](../Page/竽.md "wikilink")，要三百人合奏，南郭先生不會吹竽，混進竽[樂隊裡](../Page/樂隊.md "wikilink")，一直到齊宣王去世，都沒被拆穿；[齊愍王立後](../Page/齊愍王.md "wikilink")，喜歡單獨演奏，處士趕緊逃亡，這是[成語](../Page/成語.md "wikilink")「濫竽充數」的典故由來。

## 生平

## 家庭

  - 父母

<!-- end list -->

  - [齊威王田因齊](../Page/齊威王.md "wikilink")

<!-- end list -->

  - 兄弟

<!-- end list -->

  - [靖郭君田嬰](../Page/靖郭君.md "wikilink")
  - [郊師](../Page/郊師.md "wikilink")（《《戰國策·齊策·靖郭君謂齊王》》）

<!-- end list -->

  - 妻妾

<!-- end list -->

  - [齊宣王后](../Page/齊宣王后.md "wikilink")(？-前312年)（《史記索隱·田世家》引《紀年》，陳逢衡《竹書紀年集證》認為是齊宣王之母）
  - [鍾離春](../Page/鍾離春.md "wikilink")（《列女傳·辯通傳·鍾離春》）

<!-- end list -->

  - 子女

<!-- end list -->

  - [齊湣王田地](../Page/齊湣王.md "wikilink")
  - [田通](../Page/田通.md "wikilink")\[1\]

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 齊宣王                            | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前319年                          | 前318年                          | 前317年                          | 前316年                          | 前315年                          | 前314年                          | 前313年                          | 前312年                          | 前311年                          | 前310年                          |
| [干支](../Page/干支.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") |
| 齊宣王                            | 11年                            | 12年                            | 13年                            | 14年                            | 15年                            | 16年                            | 17年                            | 18年                            | 19年                            |                                |
| 西元                             | 前309年                          | 前308年                          | 前307年                          | 前306年                          | 前305年                          | 前304年                          | 前303年                          | 前302年                          | 前301年                          |                                |
| [干支](../Page/干支.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") |                                |

</div>

</div>

## 現代題材

以齊宣王作为主角的电视剧：

  - 《[東西宮略](../Page/東西宮略.md "wikilink")》-
    香港TVB2012年作品，[郭晋安](../Page/郭晋安.md "wikilink")（[齐宣王](../Page/齐宣王.md "wikilink")）与[陳法拉](../Page/陳法拉.md "wikilink")（鍾無艷）主演

[Category:齊國君主](../Category/齊國君主.md "wikilink")

1.  《新唐书·宰相世系表》：田完裔孫齊宣王少子通，字季達，封於平原般縣陸鄉，即陸終故地，因以氏焉。