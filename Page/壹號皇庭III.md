《**壹號皇庭III**》（[英文](../Page/英語.md "wikilink")：），[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台時裝法律](../Page/翡翠台.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，於1994年10月10日首播，共20集，監製[鄧特希](../Page/鄧特希.md "wikilink")。逢星期一至五播出。\[1\]\[2\]\[3\]\[4\]

## 各集主題

| 集數 | 單元主題 |
| -- | ---- |
| 1  | 去勢   |
| 2  | 絕路   |
| 3  | 殺妻   |
| 4  | 神打   |
| 5  | 犧牲   |
| 6  | 烙印   |
| 7  | 代價   |
| 8  | 極刑   |
| 9  | 失常   |
| 10 | 殉情   |
| 11 | 歧視   |
| 12 | 懲罰   |
| 13 | 迷姦   |
| 14 | 疏忽   |
| 15 | 情痴   |
| 16 | 男妓   |
| 17 | 串謀   |
| 18 | 討債   |
| 19 | 報復   |
| 20 | 無助   |

## 演員表

### 主要演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a></p></td>
<td><p>余在春</p></td>
<td><p>Ben<br />
大律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳秀雯.md" title="wikilink">陳秀雯</a></p></td>
<td><p>丁　柔</p></td>
<td><p>Michelle<br />
高級检控官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陶大宇.md" title="wikilink">陶大宇</a></p></td>
<td><p>江承宇</p></td>
<td><p>Michael<br />
師爺<br />
江李頌雲長子, 江承宙之兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉美娟.md" title="wikilink">劉美娟</a></p></td>
<td><p>江承宙</p></td>
<td><p>Helen<br />
大律師<br />
江李頌雲幼女, 江承宇之妹</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/駱應鈞.md" title="wikilink">駱應鈞</a></p></td>
<td><p>周文彬</p></td>
<td><p>彬Sir<br />
警長</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇永康.md" title="wikilink">蘇永康</a></p></td>
<td><p>周志輝</p></td>
<td><p>Raymond<br />
律師</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔣志光.md" title="wikilink">蔣志光</a></p></td>
<td><p>周少聰</p></td>
<td><p>Eric<br />
大律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘇杏璇.md" title="wikilink">蘇杏璇</a></p></td>
<td><p>江李頌雲</p></td>
<td><p>Lora<br />
御用大律師<br />
江承宙, 江承宇之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林漪娸.md" title="wikilink">林漪娸</a></p></td>
<td><p>林學宜</p></td>
<td><p>Doris<br />
督察</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳梅馨.md" title="wikilink">陳梅馨</a></p></td>
<td><p>關　靜</p></td>
<td><p>Jessica<br />
检控官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林保怡.md" title="wikilink">林保怡</a></p></td>
<td><p>李世宏</p></td>
<td><p>Joe<br />
大律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁婉靜.md" title="wikilink">梁婉靜</a></p></td>
<td><p>湯芷琪</p></td>
<td><p>Susan<br />
室內設計師<br />
第二輯湯芷珊妹</p></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江希文.md" title="wikilink">江希文</a></p></td>
<td><p>方曦彤</p></td>
<td><p>Grace<br />
律師<br />
關靜表妹</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳國敬.md" title="wikilink">吳國敬</a></p></td>
<td><p>朱家康</p></td>
<td><p>Ken<br />
律師行員工</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/袁鳳瑛.md" title="wikilink">袁鳳瑛</a></p></td>
<td><p>王雪怡</p></td>
<td><p>Shirley<br />
強姦案受害人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張延.md" title="wikilink">張　延</a></p></td>
<td><p>林淑嫻</p></td>
<td><p>Eva<br />
護士</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭政鴻.md" title="wikilink">郭政鴻</a></p></td>
<td><p>王　安</p></td>
<td><p>安仔<br />
探員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林尚武.md" title="wikilink">林尚武</a></p></td>
<td><p>鮑思全</p></td>
<td><p>George<br />
醫生</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a></p></td>
<td><p>謝俊傑</p></td>
<td><p>Dicky<br />
男公關<br />
湯芷琪初戀情人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洪朝豐.md" title="wikilink">洪朝豐</a></p></td>
<td><p>張家賢</p></td>
<td><p>Jeff</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韓馬利.md" title="wikilink">韓馬利</a></p></td>
<td><p>鄧麗芳</p></td>
<td><p>阿芳<br />
鮑思全前妻</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a></p></td>
<td><p>John</p></td>
<td><p>王雪怡朋友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李龍基.md" title="wikilink">李龍基</a></p></td>
<td><p>羅偉林</p></td>
<td><p>Philip<br />
湯芷琪男朋友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/談佩珊.md" title="wikilink">談佩珊</a></p></td>
<td><p>羅太太</p></td>
<td><p>羅偉林太太</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/譚淑梅.md" title="wikilink">譚淑梅</a></p></td>
<td><p>劉妙玲</p></td>
<td><p>Ann<br />
湯芷琪朋友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林文森.md" title="wikilink">林文森</a></p></td>
<td><p>May</p></td>
<td><p>王雪怡朋友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何嘉麗_(演員).md" title="wikilink">何嘉麗</a></p></td>
<td><p>Amy</p></td>
<td><p>丁柔朋友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文潔雲.md" title="wikilink">文潔雲</a></p></td>
<td><p>丁柔表嫂</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周逸鳳.md" title="wikilink">周逸鳳</a></p></td>
<td><p>Annie</p></td>
<td><p>江承宙朋友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馮素波.md" title="wikilink">馮素波</a></p></td>
<td><p>英　姐</p></td>
<td><p>江家傭人<br />
（第一輯由<a href="../Page/梁愛.md" title="wikilink">梁愛飾演</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁健平.md" title="wikilink">梁健平</a></p></td>
<td><p>Paul</p></td>
<td><p>檢控官</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭卓樺.md" title="wikilink">郭卓樺</a></p></td>
<td><p>超</p></td>
<td><p>探員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李煌生.md" title="wikilink">李煌生</a></p></td>
<td><p>強</p></td>
<td><p>探員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林嘉麗.md" title="wikilink">林嘉麗</a></p></td>
<td><p>嫦</p></td>
<td><p>探員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/譚一清.md" title="wikilink">譚一清</a></p></td>
<td><p>王醫生</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何璧堅.md" title="wikilink">何璧堅</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳中堅.md" title="wikilink">陳中堅</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸應康.md" title="wikilink">陸應康</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃成想.md" title="wikilink">黃成想</a></p></td>
<td><p>法　官</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張海勤.md" title="wikilink">張海勤</a></p></td>
<td><p>法庭職員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅雪玲.md" title="wikilink">羅雪玲</a></p></td>
<td><p>法庭職員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麥嘉倫.md" title="wikilink">麥嘉倫</a></p></td>
<td><p>柔助手<br />
／檢察助理<br />
／靜助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳燕航.md" title="wikilink">陳燕航</a></p></td>
<td><p>秘　書<br />
／Kitty</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/溫雙燕.md" title="wikilink">溫雙燕</a></p></td>
<td><p>周　太</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>郭律師</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葉鎮華.md" title="wikilink">葉鎮華</a></p></td>
<td><p>郭助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳曉雲.md" title="wikilink">陳曉雲</a></p></td>
<td><p>Ada</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃建峰.md" title="wikilink">黃建峰</a></p></td>
<td><p>鑑証科職員</p></td>
<td></td>
</tr>
</tbody>
</table>

## 案件內容

#### 一、去勢

|                                         |        |           |
| --------------------------------------- | ------ | --------- |
| **演員**                                  | **角色** | **暱稱/身份** |
| [王維德](../Page/王維德.md "wikilink")        | 李立光    |           |
| [陳潔儀](../Page/陳潔儀_\(香港\).md "wikilink") | 黃美君    |           |
|                                         |        |           |

|           |
| :-------: |
| **第一次審訊** |
|  **被告**   |
|  **罪名**   |
|  **被害人**  |
|  **檢控官**  |
| **辯方律師**  |
| **控方證人**  |
| **辯方證人**  |
|  **結果**   |

|           |
| :-------: |
| **第二次審訊** |
|  **被告**   |
|  **罪名**   |
|  **被害人**  |
|  **檢控官**  |
| **辯方律師**  |
| **控方證人**  |
| **辯方證人**  |
|  **結果**   |

#### 二、絕路

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥皓為.md" title="wikilink">麥皓為</a></p></td>
<td><p>余永栢</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭君寧.md" title="wikilink">鄭君寧</a></p></td>
<td><p>區耀祥</p></td>
<td><p>軍裝警員<br />
死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃振寧.md" title="wikilink">黃振寧</a></p></td>
<td><p>鄭永基</p></td>
<td><p>軍裝警員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔣文端.md" title="wikilink">蔣文端</a></p></td>
<td><p>李月嫦</p></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 余永栢 |
| **罪名**   |     |
| **被害人**  | 區耀祥 |
| **檢控官**  | 丁柔  |
| **辯方律師** |     |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 三、殺妻

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a></p></td>
<td><p>楊　炳</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李詩韻(93年港姐參加者).md" title="wikilink">李詩韻</a></p></td>
<td><p>趙美嫦</p></td>
<td><p>楊炳妻子<br />
死者</p></td>
</tr>
</tbody>
</table>

|          |        |
| -------- | ------ |
| **被告**   | 楊炳     |
| **罪名**   | 謀殺     |
| **被害人**  | 趙美嫦及情夫 |
| **檢控官**  | 江承宙    |
| **辯方律師** | 李世宏    |
| **控方證人** |        |
| **辯方證人** |        |
| **結果**   |        |

#### 四、神打

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭雷.md" title="wikilink">鄭　雷</a></p></td>
<td><p>黃志仁</p></td>
<td><p>黄师傅<br />
被告<br />
神打師傅<br />
先天无极护体神功传人<br />
用刀示范神打过程中砍徒弟大动脉导致后者死亡</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湯俊明.md" title="wikilink">湯俊明</a></p></td>
<td><p>阿　超</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溫雙燕.md" title="wikilink">溫雙燕</a></p></td>
<td><p>周　太</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳寶靈.md" title="wikilink">陳寶靈</a></p></td>
<td><p>陪審員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>周師傅</p></td>
<td><p>气功师傅<br />
证人</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 黃志仁 |
| **罪名**   |     |
| **被害人**  |     |
| **檢控官**  | 丁柔  |
| **辯方律師** | 周少聰 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 五、犧牲

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎萱.md" title="wikilink">黎　萱</a></p></td>
<td><p>何玉梅</p></td>
<td><p>鮑思全医生的患癌病人<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾令茵.md" title="wikilink">曾令茵</a></p></td>
<td><p>阿　斯</p></td>
<td><p>全名吳美斯<br />
夜總會公關<br />
死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何潔珊.md" title="wikilink">何潔珊</a></p></td>
<td><p>CID甲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/胡耀鋒.md" title="wikilink">胡耀鋒</a></p></td>
<td><p>恆　仔</p></td>
<td><p>何玉梅孙子</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李衛民.md" title="wikilink">李衛民</a></p></td>
<td><p>阿　志</p></td>
<td><p>何玉梅兒子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陸麗燕.md" title="wikilink">陸麗燕</a></p></td>
<td><p>阿　婷</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃清榕.md" title="wikilink">黃清榕</a></p></td>
<td><p>阿　萍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳安瑩.md" title="wikilink">陳安瑩</a></p></td>
<td><p>梁小姐</p></td>
<td><p>夜總會公關<br />
死者室友/同事<br />
证人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王翠玉.md" title="wikilink">王翠玉</a></p></td>
<td><p>女　甲</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 何玉梅  |
| **罪名**   |      |
| **被害人**  | 吳美斯  |
| **檢控官**  | 余在春  |
| **辯方律師** | 江李頌雲 |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

#### 六、烙印

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林家棟.md" title="wikilink">林家棟</a></p></td>
<td><p>陳志達</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/袁鳳瑛.md" title="wikilink">袁鳳瑛</a></p></td>
<td><p>王雪怡</p></td>
<td><p>Shirley<br />
受害人<br />
被陳志達強姦</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉煒全.md" title="wikilink">劉煒全</a></p></td>
<td><p>Peter</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡雲霞.md" title="wikilink">蔡雲霞</a></p></td>
<td><p>怡同事</p></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 陳志達 |
| **罪名**   |     |
| **被害人**  | 王雪怡 |
| **檢控官**  | 關靜  |
| **辯方律師** | 周少聰 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 七、代價

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏萍.md" title="wikilink">夏　萍</a></p></td>
<td><p>劉　太</p></td>
<td><p>全名劉林桂蘭<br />
阿成媽媽<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉文俊.md" title="wikilink">劉文俊</a></p></td>
<td><p>盧冠邦</p></td>
<td><p>死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅君左.md" title="wikilink">羅君左</a></p></td>
<td><p>阿　成</p></td>
<td><p>弱智人士</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 劉林桂蘭 |
| **罪名**   |      |
| **被害人**  | 盧冠邦  |
| **檢控官**  | Paul |
| **辯方律師** | 江承宙  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

#### 八、極刑

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/博君.md" title="wikilink">博　君</a></p></td>
<td><p>陳　彪</p></td>
<td><p>謀殺曾氏夫婦真兇</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李成昌.md" title="wikilink">李成昌</a></p></td>
<td><p>何國勇</p></td>
<td><p>被告</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎漢持.md" title="wikilink">黎漢持</a></p></td>
<td><p>馬禮信</p></td>
<td><p>神父<br />
馬玉貞之兄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳寶儀.md" title="wikilink">陳寶儀</a></p></td>
<td><p>馬玉貞</p></td>
<td><p>修女<br />
馬禮信之妹</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 何國勇  |
| **罪名**   |      |
| **被害人**  | 曾氏夫婦 |
| **檢控官**  | 關靜   |
| **辯方律師** | 余在春  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

#### 九、失常

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林嘉華.md" title="wikilink">林嘉華</a></p></td>
<td><p>程大志</p></td>
<td><p>警察<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳紹華.md" title="wikilink">陳紹華</a></p></td>
<td><p>張志雄</p></td>
<td><p>死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉桂芳.md" title="wikilink">劉桂芳</a></p></td>
<td><p>雄　妻</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凌漢.md" title="wikilink">凌　漢</a></p></td>
<td><p>管理員</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 程大志 |
| **罪名**   |     |
| **被害人**  | 張志雄 |
| **檢控官**  | 丁柔  |
| **辯方律師** | 李世宏 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十、殉情

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [李文標](../Page/李文標.md "wikilink") | 黃　健    | 被告        |
| [劉美珊](../Page/劉美珊.md "wikilink") | 周小欣    | 受害人       |
| [曹　濟](../Page/曹濟.md "wikilink")  | 欣　父    |           |
| [廖麗麗](../Page/廖麗麗.md "wikilink") | 欣　母    |           |
| [唐葳娜](../Page/唐葳娜.md "wikilink") | 劉看護    |           |
|                                  |        |           |

|          |     |
| -------- | --- |
| **被告**   | 黃　健 |
| **罪名**   |     |
| **被害人**  | 周小欣 |
| **檢控官**  | 江承宙 |
| **辯方律師** | 李世宏 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十一、歧視

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [黎秀英](../Page/黎秀英.md "wikilink") | 趙雪蓮    | 被告        |
| [林映輝](../Page/林映輝.md "wikilink") | 周小南    | 方曦彤同學     |
|                                  |        |           |
|                                  |        |           |

|          |     |
| -------- | --- |
| **被告**   | 趙雪蓮 |
| **罪名**   |     |
| **被害人**  | 周少威 |
| **檢控官**  | 丁柔  |
| **辯方律師** | 余在春 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十二、懲罰

|                                  |          |           |
| -------------------------------- | -------- | --------- |
| **演員**                           | **角色**   | **暱稱/身份** |
| [黃天鐸](../Page/黃天鐸.md "wikilink") | 羅校長      | 被告        |
| [李欣曉](../Page/李欣曉.md "wikilink") | Miss Lee |           |
| [余慕蓮](../Page/余慕蓮.md "wikilink") | 文仔母      |           |
|                                  |          |           |

|          |
| :------: |
| **死因裁判** |
| **被害人**  |
|  **律師**  |
|  **證人**  |
|  **結果**  |

|          |     |
| -------- | --- |
| **被告**   | 羅校長 |
| **罪名**   |     |
| **被害人**  | 陳偉文 |
| **檢控官**  | 關靜  |
| **辯方律師** | 李世宏 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十三、迷姦

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/薛純基.md" title="wikilink">薛純基</a></p></td>
<td><p>張培昌</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅維.md" title="wikilink">羅　維</a></p></td>
<td><p>郭世文</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳曉雲.md" title="wikilink">陳曉雲</a></p></td>
<td><p>Ada</p></td>
<td><p>全名莊子敏<br />
死者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/呂劍光.md" title="wikilink">呂劍光</a></p></td>
<td><p>Ada父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孫季卿.md" title="wikilink">孫季卿</a></p></td>
<td><p>管理員</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧英敏.md" title="wikilink">鄧英敏</a></p></td>
<td><p>謝律師</p></td>
<td><p>四名被告辯護律師</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亦羚.md" title="wikilink">亦　羚</a></p></td>
<td><p>Angel</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |        |
| -------- | ------ |
| **被告**   | 張培昌等四人 |
| **罪名**   |        |
| **被害人**  | 莊子敏    |
| **檢控官**  | 余在春    |
| **辯方律師** | 謝律師    |
| **控方證人** |        |
| **辯方證人** |        |
| **結果**   |        |

#### 十四、疏忽

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/方傑_(演員).md" title="wikilink">方　傑</a></p></td>
<td><p>曾永成</p></td>
<td><p>醫生<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虞天偉.md" title="wikilink">虞天偉</a></p></td>
<td><p>基　父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>郭律師</p></td>
<td><p>辯方律師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伍文生.md" title="wikilink">伍文生</a></p></td>
<td><p>聰助手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張俊華_(演員).md" title="wikilink">張俊華</a></p></td>
<td><p>郭助手</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |
| :------: |
| **死因裁判** |
| **被害人**  |
|  **律師**  |
|  **證人**  |
|  **結果**  |

|          |     |
| -------- | --- |
| **被告**   | 曾永成 |
| **罪名**   |     |
| **被害人**  | 鄭永基 |
| **檢控官**  | 周少聰 |
| **辯方律師** | 郭律師 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十五、情痴

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱偉達.md" title="wikilink">朱偉達</a></p></td>
<td><p>趙家強</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戴志偉.md" title="wikilink">戴志偉</a></p></td>
<td><p>程志偉</p></td>
<td><p>Alex<br />
被告</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/容嘉勵.md" title="wikilink">容嘉勵</a></p></td>
<td><p>林慧菁</p></td>
<td><p>死者</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/于楓.md" title="wikilink">-{于}-　楓</a></p></td>
<td><p>菁　母</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳文偉.md" title="wikilink">吳文偉</a></p></td>
<td><p>Christ</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |     |
| -------- | --- | --- |
| **被告**   | 趙家強 | 程志偉 |
| **罪名**   |     |     |
| **被害人**  | 林慧菁 |     |
| **檢控官**  | 丁柔  |     |
| **辯方律師** | 江承宙 |     |
| **控方證人** |     |     |
| **辯方證人** |     |     |
| **結果**   |     |     |

#### 十六、男妓

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴少民.md" title="wikilink">戴少民</a></p></td>
<td><p>謝天良</p></td>
<td><p>被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/许思敏.md" title="wikilink">许思敏</a></p></td>
<td><p>沈天丽</p></td>
<td><p>死者<br />
慈善機構總理</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 謝天良  |
| **罪名**   |      |
| **被害人**  | 沈天麗  |
| **檢控官**  | Paul |
| **辯方律師** | 余在春  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

#### 十七、串謀

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [沈　威](../Page/沈威.md "wikilink")  | 白承天    | 被告        |
| [楊得時](../Page/楊得時.md "wikilink") | 關大昌    |           |
| [沈寶思](../Page/沈寶思.md "wikilink") | 林啟豪    |           |
| [葉振聲](../Page/葉振聲.md "wikilink") | 李國威    |           |
| [戴少民](../Page/戴少民.md "wikilink") | 謝天良    |           |
| [鄭家生](../Page/鄭家生.md "wikilink") | 徐明偉    | 死者        |
| [楊瑞麟](../Page/楊瑞麟.md "wikilink") | 律師甲    | 三名被告辯護律師  |
| [張漢斌](../Page/張漢斌.md "wikilink") | 師　爺    |           |
| [鄧汝超](../Page/鄧汝超.md "wikilink") | 阿　正    |           |
|                                  |        |           |

|          |        |
| -------- | ------ |
| **被告**   | 白承天等三人 |
| **罪名**   |        |
| **被害人**  | 徐明偉    |
| **檢控官**  | 江承宙    |
| **辯方律師** | 律師甲    |
| **控方證人** |        |
| **辯方證人** |        |
| **結果**   |        |

#### 十八、討債

|                                  |        |           |
| -------------------------------- | ------ | --------- |
| **演員**                           | **角色** | **暱稱/身份** |
| [李麗麗](../Page/李麗麗.md "wikilink") | 譚少蘭    | 被告        |
| [何金靈](../Page/何金靈.md "wikilink") | 呂志剛    | 死者        |
| [陳勉良](../Page/陳勉良.md "wikilink") | 何　祥    |           |
| [吳列華](../Page/吳列華.md "wikilink") | 宏秘書    |           |
|                                  |        |           |

|          |     |
| -------- | --- |
| **被告**   | 譚少蘭 |
| **罪名**   |     |
| **被害人**  | 呂志剛 |
| **檢控官**  | 丁柔  |
| **辯方律師** | 李世宏 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 十九、报复

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a></p></td>
<td><p>謝俊傑</p></td>
<td><p>Dicky<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/譚淑梅.md" title="wikilink">譚淑梅</a></p></td>
<td><p>劉妙玲</p></td>
<td><p>Ann<br />
死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>蘇律師</p></td>
<td><p>辯方律師</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |     |
| -------- | --- |
| **被告**   | 謝俊傑 |
| **罪名**   |     |
| **被害人**  | 劉妙玲 |
| **檢控官**  | 關靜  |
| **辯方律師** | 蘇律師 |
| **控方證人** |     |
| **辯方證人** |     |
| **結果**   |     |

#### 二十、无助

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/身份</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁婉靜.md" title="wikilink">梁婉靜</a></p></td>
<td><p>湯芷琪</p></td>
<td><p>Susan<br />
被告</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a></p></td>
<td><p>謝俊傑</p></td>
<td><p>Dicky<br />
死者</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴耀明.md" title="wikilink">戴耀明</a></p></td>
<td><p>Paul助手</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

|          |      |
| -------- | ---- |
| **被告**   | 湯芷琪  |
| **罪名**   |      |
| **被害人**  | 謝俊傑  |
| **檢控官**  | Paul |
| **辯方律師** | 余在春  |
| **控方證人** |      |
| **辯方證人** |      |
| **結果**   |      |

## 主题音乐及插曲

  - 主題音樂：《[Your Latest Trick](../Page/Your_Latest_Trick.md "wikilink")》
      - SAXO-PHONE：[Phil Rombaoa](../Page/Phil_Rombaoa.md "wikilink")
      - 作曲：[Mark Knopfler](../Page/Mark_Knopfler.md "wikilink") /
        [羅雁武](../Page/羅雁武.md "wikilink")

<!-- end list -->

  - 插曲《燈火欄柵處》
      - 作曲：[黃尚偉](../Page/黃尚偉.md "wikilink")
      - 填詞：[梁芷珊](../Page/梁芷珊.md "wikilink")
      - 主唱：[蘇永康](../Page/蘇永康.md "wikilink")

<!-- end list -->

  - 插曲《別要愛得太難過》
      - 作曲：[花比傲](../Page/花比傲.md "wikilink")
      - 填詞：[林夕](../Page/林夕.md "wikilink")
      - 主唱：[袁鳳瑛](../Page/袁鳳瑛.md "wikilink")

## 外部連結

  - [無綫電視官方網頁 -
    壹號皇庭III](https://web.archive.org/web/20080917055940/http://tvcity.tvb.com/drama/just3/index.htm)
  - [《壹號皇庭III》 GOTV
    第1集重溫](https://web.archive.org/web/20140222162829/http://gotv.tvb.com/programme/103486/156414/)

## 參考

## 電視節目的變遷

[Category:1994年無綫電視劇集](../Category/1994年無綫電視劇集.md "wikilink")
[Category:無綫電視1990年代背景劇集](../Category/無綫電視1990年代背景劇集.md "wikilink")

1.  [](http://mooddiary.pixnet.net/blog/post/32693582-%E5%A3%B9%E8%99%9F%E7%9A%87%E5%BA%AD%E3%80%81%E5%A3%B9%E8%99%9F%E7%9A%87%E5%BA%ADii%E3%80%81%E5%A3%B9%E8%99%9F%E7%9A%87%E5%BA%ADiii%E3%80%81%E5%A3%B9%E8%99%9F%E7%9A%87)
2.  [File of Justice III 壹 號 皇 庭
    III](http://tvcity.tvb.com/drama/just3/index.htm)
3.  [【《壹號皇庭》女星搖籃】 宣萱陳慧珊榮升花旦
    Sammi王菲雄霸樂壇](https://hk.entertainment.appledaily.com/enews/realtime/article/20161006/55736986)
4.  [【一首歌】林保怡《和你的每一天》講分手傷痛：我仍然留住記憶](https://www.hk01.com/%E9%9F%B3%E6%A8%82/95575/-%E4%B8%80%E9%A6%96%E6%AD%8C-%E6%9E%97%E4%BF%9D%E6%80%A1-%E5%92%8C%E4%BD%A0%E7%9A%84%E6%AF%8F%E4%B8%80%E5%A4%A9-%E8%AC%9B%E5%88%86%E6%89%8B%E5%82%B7%E7%97%9B-%E6%88%91%E4%BB%8D%E7%84%B6%E7%95%99%E4%BD%8F%E8%A8%98%E6%86%B6)