在[數學中的](../Page/數學.md "wikilink")[代數幾何與](../Page/代數幾何.md "wikilink")[數論領域](../Page/數論.md "wikilink")，**志村簇**是一類特殊的[代數簇](../Page/代數簇.md "wikilink")，可視之為[模曲線在高維度的類推](../Page/模曲線.md "wikilink")。粗略地說，志村簇乃是[埃爾米特對稱空間對某個](../Page/埃爾米特對稱空間.md "wikilink")[代數群之](../Page/代數群.md "wikilink")[同餘子群的商](../Page/同餘子群.md "wikilink")；最簡單的例子是上半平面對
\(\mathrm{SL}_2(\Z)\) 的商。一維的志村簇有時也被稱為**志村曲線**。

[志村五郎在](../Page/志村五郎.md "wikilink")1960年代研究了上述商空間的緊化，其目的在推廣[複乘法理論及互逆律](../Page/複乘法理論.md "wikilink")\[1\]；在此需要的基本結果是
Baily-Borel
定理（1966）\[2\]。此後，人們也發現志村簇是某類[霍奇結構的](../Page/霍奇結構.md "wikilink")[模空間](../Page/模空間.md "wikilink")。

## 典範模型

按照定義，志村簇本身僅是一個[複流形](../Page/複流形.md "wikilink")。志村五郎證明了每個志村簇都可以定義在一個唯一確定的[數域](../Page/數域.md "wikilink")
\(K\) 上，由此也可解釋志村簇與數論問題的關聯。這個結果是志村五郎陳述其互逆律的出發點。

## 在郎蘭茲綱領中的角色

志村簇在[郎蘭茲綱領扮演重要地位](../Page/郎蘭茲綱領.md "wikilink")。根據郎蘭茲的猜想，對任一定義在數域 \(K\)
上的代數簇
\(X\)，其[哈瑟-韋伊ζ函數將會來自一個](../Page/哈瑟-韋伊ζ函數.md "wikilink")[自守表示](../Page/自守表示.md "wikilink")。至今已知的結果全是
\(X\) 為志村簇的情形。

在這個方向上，一個指導性的結果是
**Eichler-志村同餘關係**：此結果保證了模曲線的哈瑟-韋伊ζ函數可表成源自[模形式的L函數之積](../Page/模形式.md "wikilink")，其中每個模型式的權都是二，並具有明確的表示式。事實上，志村五郎發展其理論的動機就是推廣這個結果。

## 文獻

  - James Arthur (Editor), David Ellwood (Editor), Robert Kottwitz
    (Editor) [*Harmonic Analysis, the Trace Formula, and Shimura
    Varieties*](http://www.claymath.org/publications/Harmonic_Analysis/):
    Proceedings of the Clay Mathematics Institute, 2003 Summer School,
    the Fields Institute, (Clay Mathematics Proceedings,) ISBN
    082183844X

  - Deligne, Pierre; Milne, James S.; Ogus, Arthur; Shih, Kuang-yen
    *Hodge cycles, motives, and Shimura varieties.* Lecture Notes in
    Mathematics, 900. Springer-Verlag, Berlin-New York, 1982. ii+414 pp.
    ISBN 3-540-11174-3

  - Deligne, Pierre *Variétés de Shimura: interprétation modulaire, et
    techniques de construction de modèles canoniques.* Automorphic
    forms, representations and L-functions (Proc. Sympos. Pure Math.,
    Oregon State Univ., Corvallis, Ore., 1977), Part 2, pp. 247--289,
    Proc. Sympos. Pure Math., XXXIII, Amer. Math. Soc., Providence,
    R.I., 1979.

  - Deligne, Pierre *Travaux de Shimura.* Séminaire Bourbaki, 23ème
    année (1970/71), Exp. No. 389, pp. 123--165. Lecture Notes in
    Math., Vol. 244, Springer, Berlin, 1971.

  - J. Milne, *Shimura varieties and motives* U. Jannsen (ed.) S.
    Kleiman (ed.) J.-P. Serre (ed.) , Motives , Proc. Symp. Pure Math. ,
    55: 2 , Amer. Math. Soc. (1994) pp. 447–523

  -
  - J. S. Milne [Introduction to Shimura
    varieties](http://www.claymath.org/publications/Harmonic_Analysis/chapter2.pdf),
    chapter 2 of the book edited by Arthur, Ellwood, and Kottwitz (2003)

  - Shimura, Goro, *The Collected Works of Goro Shimura* (2003), five
    volumes

## 註記

## 外部連結

  -
[Z](../Category/代數幾何.md "wikilink")
[Category:自守形式](../Category/自守形式.md "wikilink")

1.  見其著作 *Introduction to Arithmetic Theory of Automorphic Functions*。
2.  Baily,W.L., Borel,A.: *Compactication of arithmetic quotients of
    bounded symmetric domains*, Ann. Math.84 (1966), 442 - 528.