**药物效应动力学**（ (PD)
），简称**药效学**，是[药理学的一个分支](../Page/药理学.md "wikilink")，主要研究**[药物作用](../Page/药物.md "wikilink")(action)**与**药理效应(effects)**（即**药物对机体的作用**及**[作用机制](../Page/作用机制.md "wikilink")(mechanism
of
action)**\[1\]）、[治疗效果和](../Page/治疗效果.md "wikilink")[不良反应](../Page/不良反应.md "wikilink")。

## 研究范围

  - 研究**[药物的作用](../Page/药物的作用.md "wikilink")**和**[药理效应](../Page/药理效应.md "wikilink")**。
  - 研究**药物作用的两重性**（**治疗作用**和**不良反应**）。
  - 研究*药物的量效关系**（**剂量与量反应/质反应的关系*'）。
  - 对**药物药物安全性**评价。

## 註釋

## 外部連結

  - Vijay. (2003) [Predictive software for drug design and
    development](http://www.ingentaconnect.com/content/adis/pdr/2003/00000001/00000003/art00002).
    Pharmaceutical Development and Regulation 1 ((3)), 159-168.
  - Werner, E., [In silico multicellular systems biology and minimal
    genomes](https://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=14678738&query_hl=3),
    DDT vol 8, no 24, pp 1121–1127, Dec 2003. (Introduces the concepts
    MCPD and Net-MCPD)
  - Dr. David W. A. Bourne, OU College of Pharmacy [Pharmacokinetic and
    Pharmacodynamic Resources](http://www.boomer.org/pkin/).
  - [Introduction to Pharmacokinetics and
    Pharmacodynamics](https://www.ashp.org/-/media/store%20files/p2418-sample-chapter-1.pdf)

[药效学](../Category/药效学.md "wikilink")
[Category:藥學](../Category/藥學.md "wikilink")
[Category:药物化学](../Category/药物化学.md "wikilink")

1.  [MBA智庫百科](http://wiki.mbalib.com/zh-tw/药理效应)