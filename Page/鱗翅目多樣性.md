此一列表標示著[鱗翅目的多樣性](../Page/鱗翅目.md "wikilink")，標明出每個總科和科的屬和種的估計個數。

|                                    |                                         |                                         |                                   |
| ---------------------------------- | --------------------------------------- | --------------------------------------- | --------------------------------- |
| **總科**                             | **[科](../Page/科_\(生物\).md "wikilink")** | **[屬](../Page/屬_\(生物\).md "wikilink")** | **[種](../Page/物種.md "wikilink")** |
| 小翅蛾總科                              | [小翅蛾科](../Page/小翅蛾科.md "wikilink")      | 6                                       | 182                               |
| 具殼杉蛾總科                             | [貝殼杉蛾科](../Page/貝殼杉蛾科.md "wikilink")    | 1                                       | 2                                 |
| 異石蛾總科                              | [異石蛾科](../Page/異石蛾科.md "wikilink")      | 1                                       | 9                                 |
| 毛頂蛾總科                              | [毛頂蛾科](../Page/毛頂蛾科.md "wikilink")      | 6                                       | 24                                |
| 棘蛾總科                               | [刺翅蛾科](../Page/刺翅蛾科.md "wikilink")      | 2                                       | 5                                 |
| 冠蛾總科                               | [冠蛾科](../Page/冠蛾科.md "wikilink")        | 1                                       | 6                                 |
| 卵翅蛾總科                              | [卵翅蛾科](../Page/卵翅蛾科.md "wikilink")      | 3                                       | 10                                |
| 擬菜蛾總科                              | [擬菜蛾科](../Page/擬菜蛾科.md "wikilink")      | 1                                       | 14                                |
| 蝙蝠蛾總科                              |                                         | 61                                      | 544                               |
|                                    | [小蝠蛾科](../Page/小蝠蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [蝙蝠蛾科](../Page/蝙蝠蛾科.md "wikilink")      | 50                                      | 500                               |
|                                    | [新蝠蛾科](../Page/新蝠蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [古蝠蛾科](../Page/古蝠蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [原蝠蛾科](../Page/原蝠蛾科.md "wikilink")      | ?                                       | ?                                 |
| 微蛾總科                               |                                         | 17                                      | 902                               |
|                                    | [微蛾科](../Page/微蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [隆角蛾科](../Page/隆角蛾科.md "wikilink")      | ?                                       | ?                                 |
| 曲蛾總科                               |                                         | 72                                      | 598                               |
|                                    | [長角蛾科](../Page/長角蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [嬰蛾科](../Page/嬰蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [毛翅蛾科](../Page/毛翅蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [日蛾科](../Page/日蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [曲蛾科](../Page/曲蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [絲蘭蛾科](../Page/絲蘭蛾科.md "wikilink")      | 12                                      | 80                                |
| 始蛾總科                               | [始蛾科](../Page/始蛾科.md "wikilink")        | 7                                       | 60                                |
| 帝氏蛾總科                              | [帝氏蛾科](../Page/帝氏蛾科.md "wikilink")      | 1                                       | 80                                |
| 偽螟蛾總科                              | [擬網蛾科](../Page/擬網蛾科.md "wikilink")      | 2                                       | 4                                 |
| 穀蛾總科                               |                                         | 406                                     | 4,352                             |
|                                    | [毛蛾科](../Page/毛蛾科.md "wikilink")        | ?                                       | 300                               |
|                                    | [顯陽蛾科](../Page/顯陽蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [綿蛾科](../Page/綿蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [始蓑蛾科](../Page/始蓑蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [避債蛾科](../Page/避債蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [穀蛾科](../Page/穀蛾科.md "wikilink")        | 320                                     | 3,000                             |
| 細蛾總科                               |                                         | 90                                      | 2,315                             |
|                                    | [折角蛾科](../Page/折角蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [道格拉斯蛾科](../Page/道格拉斯蛾科.md "wikilink")  | ?                                       | ?                                 |
|                                    | [細蛾科](../Page/細蛾科.md "wikilink")        | 75                                      | 2,000                             |
|                                    | [羅氏蛾科](../Page/羅氏蛾科.md "wikilink")      | ?                                       | ?                                 |
| 巢蛾總科                               |                                         | 83                                      | 1,578                             |
|                                    | [粗頂蛾科](../Page/粗頂蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [巴氏蛾科](../Page/巴氏蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [雕蛾科](../Page/雕蛾科.md "wikilink")        | ?                                       | 384                               |
|                                    | [日逐蛾科](../Page/日逐蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [潛莖蛾科](../Page/潛莖蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [菜蛾科](../Page/菜蛾科.md "wikilink")        | ?                                       | 100                               |
|                                    | [巢蛾科](../Page/巢蛾科.md "wikilink")        | ?                                       | 600                               |
|                                    | [冠翅蛾科](../Page/冠翅蛾科.md "wikilink")      | ?                                       | ?                                 |
| 麥蛾總科                               |                                         | 1,390                                   | 15,966                            |
|                                    | [椰蛾科](../Page/椰蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [蛙蛾科](../Page/蛙蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [粗角蛾科](../Page/粗角蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [鞘蛾科](../Page/鞘蛾科.md "wikilink")        | 43                                      | 1,418                             |
|                                    | [花翼蛾科](../Page/花翼蛾科.md "wikilink")      | 106                                     | 1,628                             |
|                                    | [小微蛾科](../Page/小微蛾科.md "wikilink")      | 165                                     | 3,270                             |
|                                    | [草蛾科](../Page/草蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [麥蛾科](../Page/麥蛾科.md "wikilink")        | 507                                     | 4,530                             |
|                                    | [叢鬚蛾科](../Page/叢鬚蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [曲角蛾科](../Page/曲角蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [梯翅蛾科](../Page/梯翅蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [柳葉菜蛾科](../Page/柳葉菜蛾科.md "wikilink")    | ?                                       | ?                                 |
|                                    | [織蛾科](../Page/織蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [槍蛾科](../Page/槍蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [絹蛾科](../Page/絹蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [腹毛蛾科](../Page/腹毛蛾科.md "wikilink")      | ?                                       | ?                                 |
| 短翅穀蛾總科                             | [短翅穀蛾科](../Page/短翅穀蛾科.md "wikilink")    | 3                                       | 17                                |
| 斑蛾總科                               |                                         | 98                                      | 2,700                             |
|                                    | [艾蛾科](../Page/艾蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [韁蛾科](../Page/韁蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [蟻巢蛾科](../Page/蟻巢蛾科.md "wikilink")      | 1                                       | 5                                 |
|                                    | [亮蛾科](../Page/亮蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [蟬寄蛾科](../Page/蟬寄蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [擬簑蛾科](../Page/擬簑蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [帶翅蛾科](../Page/帶翅蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [擬斑蛾科](../Page/擬斑蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [刺蛾科](../Page/刺蛾科.md "wikilink")        | ?                                       | 1,100                             |
|                                    | [絨蛾科](../Page/絨蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [短體蛾科](../Page/短體蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [斑蛾科](../Page/斑蛾科.md "wikilink")        | ?                                       | ?                                 |
| 木蠹蛾總科                              |                                         | 114                                     | 676                               |
|                                    | [木蠹蛾科](../Page/木蠹蛾科.md "wikilink")      | 113                                     | 670                               |
|                                    | [銀斑蠹蛾科](../Page/銀斑蠹蛾科.md "wikilink")    | 1                                       | ?                                 |
| 透翅蛾總科                              |                                         | 149                                     | 1,360                             |
|                                    | [短翅蛾科](../Page/短翅蛾科.md "wikilink")      | ?                                       | 150                               |
|                                    | [蝶蛾科](../Page/蝶蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [透翅蛾科](../Page/透翅蛾科.md "wikilink")      | 149                                     | 1,362                             |
| 擬捲葉蛾總科                             | [擬捲葉蛾科](../Page/擬捲葉蛾科.md "wikilink")    | 12                                      | 405                               |
| 捲葉蛾總科                              | [捲葉蛾科](../Page/捲葉蛾科.md "wikilink")      | 755                                     | 6,338                             |
| 長腹蛾總科                              | [長腹蛾科](../Page/長腹蛾科.md "wikilink")      | 3                                       | 60                                |
| 史氏蛾總科                              | [史氏蛾科](../Page/史氏蛾科.md "wikilink")      | 2                                       | 5                                 |
| 貂蛾總科                               | [貂蛾科](../Page/貂蛾科.md "wikilink")        | 5                                       | 83                                |
| 多翼蛾總科                              |                                         | 15                                      | 158                               |
|                                    | [多翼蛾科](../Page/多翼蛾科.md "wikilink")      | ?                                       | 130                               |
|                                    | [擬穀蛾科](../Page/擬穀蛾科.md "wikilink")      | ?                                       | ?                                 |
| 鳥羽蛾總科                              | [鳥羽蛾科](../Page/鳥羽蛾科.md "wikilink")      | 73                                      | 986                               |
| 華蛾總科                               | [華蛾科](../Page/華蛾科.md "wikilink")        | 1                                       | 2                                 |
| 蜂蛾總科                               | [蜂蛾科](../Page/蜂蛾科.md "wikilink")        | 6                                       | 246                               |
| 曲鬚蛾總科                              |                                         | 29                                      | 318                               |
|                                    | [果蛀蛾科](../Page/果蛀蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [曲鬚蛾科](../Page/曲鬚蛾科.md "wikilink")      | ?                                       | ?                                 |
| 駝蛾總科                               | [駝蛾科](../Page/駝蛾科.md "wikilink")        | 2                                       | 18                                |
| 螟蛾總科                               |                                         | 215                                     | 17,786                            |
|                                    | [草螟科](../Page/草螟科.md "wikilink")        | ?                                       | 11,630                            |
|                                    | [螟蛾科](../Page/螟蛾科.md "wikilink")        | ?                                       | 6,150                             |
| 網蛾總科                               | [網蛾科](../Page/網蛾科.md "wikilink")        | 12                                      | 1,220                             |
| 米馬隆蛾總科                             | [米馬隆蛾科](../Page/米馬隆蛾科.md "wikilink")    | 30                                      | 200                               |
| 枯葉蛾總科                              |                                         | 158                                     | 1,575                             |
|                                    | [澳洲蠶蛾科](../Page/澳洲蠶蛾科.md "wikilink")    | ?                                       | ?                                 |
|                                    | [枯葉蛾科](../Page/枯葉蛾科.md "wikilink")      | 150                                     | 1,500                             |
| 蠶蛾總科                               |                                         | 466                                     | 3,425                             |
|                                    | [蠶蛾科](../Page/蠶蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [水臘蛾科](../Page/水臘蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [卡西蛾科](../Page/卡西蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [樺蛾科](../Page/樺蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [帶蛾科](../Page/帶蛾科.md "wikilink")        | ?                                       | ?                                 |
|                                    | [北方帶蛾科](../Page/北方帶蛾科.md "wikilink")    | ?                                       | ?                                 |
|                                    | [忍冬蛾科](../Page/忍冬蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [天蠶蛾科](../Page/天蠶蛾科.md "wikilink")      | 165                                     | 1,480                             |
|                                    | [天蛾科](../Page/天蛾科.md "wikilink")        | 200                                     | 1,200                             |
| 地中海蛾總科                             | [地中海蛾科](../Page/地中海蛾科.md "wikilink")    | 2                                       | 6                                 |
| 錨紋蛾總科                              | [錨紋蛾科](../Page/錨紋蛾科.md "wikilink")      | 8                                       | 60                                |
| 絲角蝶總科                              | [絲角蝶科](../Page/絲角蝶科.md "wikilink")      | 1                                       | 40                                |
| [鳳蝶總科](../Page/鳳蝶總科.md "wikilink") | [弄蝶科](../Page/弄蝶科.md "wikilink")        | 550                                     | 3,500                             |
|                                    | [灰蝶科](../Page/灰蝶科.md "wikilink")        | 566                                     | 4,698                             |
|                                    | [蛺蝶科](../Page/蛺蝶科.md "wikilink")        | 633                                     | 5,698                             |
|                                    | [鳳蝶科](../Page/鳳蝶科.md "wikilink")        | 26                                      | 605                               |
|                                    | [粉蝶科](../Page/粉蝶科.md "wikilink")        | 74                                      | 1,051                             |
|                                    | [蜆蝶科](../Page/蜆蝶科.md "wikilink")        | 140                                     | 1,250                             |
| 鉤蛾總科                               |                                         | 129                                     | 675                               |
|                                    | [鳳蛾科](../Page/鳳蛾科.md "wikilink")        | 123                                     | 665                               |
|                                    | [鉤蛾科](../Page/鉤蛾科.md "wikilink")        | ?                                       | ?                                 |
| 尺蛾總科                               |                                         | 2,060                                   | 21,740                            |
|                                    | [尺蛾科](../Page/尺蛾科.md "wikilink")        | ?                                       | 26,000                            |
|                                    | [偽燕蛾科](../Page/偽燕蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [燕蛾科](../Page/燕蛾科.md "wikilink")        | 90                                      | 700                               |
| 夜蛾總科                               |                                         | 7,250                                   | 70,000                            |
|                                    | [燈蛾科](../Page/燈蛾科.md "wikilink")        | ?                                       | 11,000                            |
|                                    | [墨西哥舟蛾科](../Page/墨西哥舟蛾科.md "wikilink")  | ?                                       | ?                                 |
|                                    | [毒蛾科](../Page/毒蛾科.md "wikilink")        | 360                                     | 2,500                             |
|                                    | [夜蛾科](../Page/夜蛾科.md "wikilink")        | 4,200                                   | 35,000                            |
|                                    | [瘤蛾科](../Page/瘤蛾科.md "wikilink")        | 308                                     | 1,400                             |
|                                    | [舟蛾科](../Page/舟蛾科.md "wikilink")        | ?                                       | 3,500                             |
|                                    | [澳舟蛾科](../Page/澳舟蛾科.md "wikilink")      | ?                                       | ?                                 |
|                                    | [隆蛾科](../Page/隆蛾科.md "wikilink")        | ?                                       | ?                                 |

## 參考文獻

  - Kristensen, N.P. (Ed.). 1999. Lepidoptera, Moths and Butterflies.
    Volume 1: Evolution, Systematics, and Biogeography. *Handbuch der
    Zoologie. Eine Naturgeschichte der Stämme des Tierreiches / Handbook
    of Zoology. A Natural History of the phyla of the Animal Kingdom.*
    Band / Volume IV Arthropoda: Insecta Teilband / Part 35: 491 pp.
    Walter de Gruyter, Berlin, New York. ISBN 3-11-017077-9

[Category:鱗翅目](../Category/鱗翅目.md "wikilink")