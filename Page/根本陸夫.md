**根本陸夫**（1926年11月20日－1999年4月30日）為[日本的](../Page/日本.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[日本職棒](../Page/日本職棒.md "wikilink")[大阪近鐵野牛](../Page/大阪近鐵野牛.md "wikilink")，退休之後曾經擔任過[日本職棒](../Page/日本職棒.md "wikilink")[廣島東洋鯉魚](../Page/廣島東洋鯉魚.md "wikilink")、[福岡大榮鷹](../Page/福岡大榮鷹.md "wikilink")、[西武獅的總教練](../Page/西武獅.md "wikilink")。

## 經歷

  - [日本職棒](../Page/日本職棒.md "wikilink")[大阪近鐵野牛隊](../Page/大阪近鐵野牛.md "wikilink")（1952年－1957年）
  - [日本職棒](../Page/日本職棒.md "wikilink")[廣島東洋鯉魚隊總教練](../Page/廣島東洋鯉魚.md "wikilink")（1968年－1972年）
  - [日本職棒](../Page/日本職棒.md "wikilink")[西武獅隊總教練](../Page/西武獅.md "wikilink")（1978年－1981年）
  - [日本職棒](../Page/日本職棒.md "wikilink")[福岡大榮鷹隊總教練](../Page/福岡大榮鷹.md "wikilink")（1993年－1994年）

## 職棒生涯成績

### 球員時期

### 監督時期

| 年度    | 球隊                                     | 順位 | 出賽  | 勝場 | 敗場 | [勝率](../Page/勝率.md "wikilink") |
| ----- | -------------------------------------- | -- | --- | -- | -- | ------------------------------ |
| 1968年 | [廣島東洋鯉魚](../Page/廣島東洋鯉魚.md "wikilink") | 3  | 134 | 68 | 62 | 0.523                          |
| 1969年 | [廣島東洋鯉魚](../Page/廣島東洋鯉魚.md "wikilink") | 6  | 130 | 56 | 70 | 0.444                          |
| 1970年 | [廣島東洋鯉魚](../Page/廣島東洋鯉魚.md "wikilink") | 4  | 130 | 62 | 60 | 0.508                          |
| 1971年 | [廣島東洋鯉魚](../Page/廣島東洋鯉魚.md "wikilink") | 4  | 130 | 63 | 61 | 0.508                          |
| 1972年 | [廣島東洋鯉魚](../Page/廣島東洋鯉魚.md "wikilink") | 6  | 130 | 49 | 75 | 0.395                          |
| 1978年 | [西武獅](../Page/西武獅.md "wikilink")       | 5  | 130 | 51 | 67 | 0.432                          |
| 1979年 | [西武獅](../Page/西武獅.md "wikilink")       | 6  | 130 | 45 | 73 | 0.381                          |
| 1980年 | [西武獅](../Page/西武獅.md "wikilink")       | 4  | 130 | 62 | 64 | 0.496                          |
| 1981年 | [西武獅](../Page/西武獅.md "wikilink")       | 4  | 130 | 61 | 61 | 0.500                          |
| 1993年 | [福岡大榮鷹](../Page/福岡大榮鷹.md "wikilink")   | 6  | 130 | 45 | 80 | 0.360                          |
| 1994年 | [福岡大榮鷹](../Page/福岡大榮鷹.md "wikilink")   | 4  | 130 | 69 | 60 | 0.534                          |

## 外部連結

[Category:1926年出生](../Category/1926年出生.md "wikilink")
[Category:1999年逝世](../Category/1999年逝世.md "wikilink")
[Category:日本棒球選手](../Category/日本棒球選手.md "wikilink")
[Category:西武獅隊總教練](../Category/西武獅隊總教練.md "wikilink")
[Category:大阪近鐵野牛隊球員](../Category/大阪近鐵野牛隊球員.md "wikilink")
[Category:福岡軟體銀行鷹總教練](../Category/福岡軟體銀行鷹總教練.md "wikilink")
[Category:廣島東洋鯉魚隊總教練](../Category/廣島東洋鯉魚隊總教練.md "wikilink")
[Category:日本野球殿堂成員](../Category/日本野球殿堂成員.md "wikilink")
[Category:茨城縣出身人物](../Category/茨城縣出身人物.md "wikilink")
[Category:日本正教徒](../Category/日本正教徒.md "wikilink")